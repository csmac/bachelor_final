1
00:00:01,006 --> 00:00:04,106
<i>- - Addic7ed.com - -</i>

2
00:00:12,199 --> 00:00:16,467
[title music]

3
00:01:06,024 --> 00:01:09,236
<i>[Arusian King] The
monster fell from the sky!</i>

4
00:01:09,319 --> 00:01:14,741
It was an epic battle,
but Voltron was victorious!

5
00:01:18,245 --> 00:01:23,125
No. I said, "Voltron was victorious!"

6
00:01:23,208 --> 00:01:24,668
[exclaiming]

7
00:01:24,751 --> 00:01:26,920
[Arusians cheer]

8
00:01:28,213 --> 00:01:32,426
Thank you, Your Majesty,
for that wonderful production.

9
00:01:32,509 --> 00:01:35,012
It saddens me
that we must leave tomorrow,

10
00:01:35,095 --> 00:01:37,389
but we must continue
our battle with Zarkon

11
00:01:37,472 --> 00:01:39,391
and spread peace
throughout the universe.

12
00:01:39,474 --> 00:01:40,642
Your Highness...

13
00:01:40,726 --> 00:01:42,894
please accept this gift.

14
00:01:42,978 --> 00:01:46,273
This will allow you to contact us
anytime you need help.

15
00:01:47,566 --> 00:01:51,278
Arus will be the first planet
in the Voltron Alliance.

16
00:01:51,361 --> 00:01:52,571
Hoorah!

17
00:01:52,654 --> 00:01:55,657
[festive, traditional music playing]
[Arusians cheer]

18
00:01:56,074 --> 00:01:58,869
- We ought to get something like that.
- Like what?

19
00:01:58,952 --> 00:02:01,872
You know, like, a cheer.
Like, a team cheer that we do.

20
00:02:01,955 --> 00:02:04,708
Mm-hmm. Yeah, okay.
How about, uh...

21
00:02:04,791 --> 00:02:07,669
"I say Vol" and you say "Tron." Vol!

22
00:02:09,212 --> 00:02:11,757
Uh... Voltron?

23
00:02:12,633 --> 00:02:15,260
No! No, no, no.
The cheer includes the instructions.

24
00:02:15,344 --> 00:02:17,012
I say "Vol" and you say...

25
00:02:18,513 --> 00:02:19,806
Voltron?

26
00:02:21,391 --> 00:02:22,768
We'll work on it.

27
00:02:28,065 --> 00:02:29,941
Coran, what is this?

28
00:02:30,025 --> 00:02:32,527
Oh, this is Nunvill,
the nectar of the gods.

29
00:02:32,611 --> 00:02:34,696
It tastes like hot dog water and feet.

30
00:02:34,780 --> 00:02:37,199
Yeah, makes
a wonderful hair tonic as well.

31
00:02:38,200 --> 00:02:40,160
[retches]

32
00:02:41,995 --> 00:02:43,955
Not feeling well? Try some Nunvill.

33
00:02:44,039 --> 00:02:46,124
Settles the stomach
and brightens your smile.

34
00:02:46,208 --> 00:02:49,795
I'm not sure we should be letting everyone
wander in and out of the castle like this.

35
00:02:49,878 --> 00:02:51,254
It doesn't seem safe.

36
00:02:51,338 --> 00:02:55,759
Oh, these Arusians won't
hurt anything... much.

37
00:02:56,218 --> 00:02:57,886
[dishware shattering]

38
00:02:57,969 --> 00:03:00,347
Besides, it's only fair to let them
see the inside of a castle

39
00:03:00,430 --> 00:03:02,307
that's been sitting
on their planet for so long.

40
00:03:02,391 --> 00:03:05,018
But who knows
when Zarkon will attack again?

41
00:03:06,186 --> 00:03:08,772
I'm going to do a perimeter check,
just in case.

42
00:03:18,907 --> 00:03:22,869
Commander Sendak, the sentries are
in position around the Arusian village.

43
00:03:22,953 --> 00:03:24,746
Luck is on our side.

44
00:03:24,830 --> 00:03:28,792
Look, the Castle defenses are down.
The door is wide open.

45
00:03:28,875 --> 00:03:31,128
With all these Arusians coming in and out,

46
00:03:31,211 --> 00:03:34,214
it should be nothing
for you to infiltrate.

47
00:03:34,297 --> 00:03:36,174
[Haxus] I may not have to. Look.

48
00:03:36,258 --> 00:03:39,720
The small one has a Galra drone
they've repurposed.

49
00:03:39,803 --> 00:03:42,806
If I can just get close enough
to clone its signature code,

50
00:03:42,889 --> 00:03:45,726
I can send our bomb drone in undetected.

51
00:03:45,809 --> 00:03:48,645
I knew you would not disappoint me, Haxus.

52
00:03:57,112 --> 00:04:00,282
Look at them,
the new Paladins of Voltron.

53
00:04:00,365 --> 00:04:03,118
The fate of the universe
is on their shoulders.

54
00:04:03,201 --> 00:04:04,828
[groaning]

55
00:04:04,911 --> 00:04:06,788
[screaming] My eyes!

56
00:04:06,872 --> 00:04:08,749
[both laugh]

57
00:04:10,041 --> 00:04:12,627
I must portray strength,
so no one can tell how

58
00:04:12,628 --> 00:04:15,213
concerned I am about
the fate of our mission.

59
00:04:15,297 --> 00:04:17,299
[mouse squeaking]

60
00:04:17,382 --> 00:04:19,301
Let's keep that a secret.

61
00:04:19,384 --> 00:04:22,888
[squeaking]
Who else has secrets?

62
00:04:25,515 --> 00:04:28,226
Hunk tried to eat what?
[chuckles]

63
00:04:28,310 --> 00:04:31,813
That is rather amusing.
What other secrets?

64
00:04:35,776 --> 00:04:39,196
[squeaking]
That seems like Lance.

65
00:04:43,033 --> 00:04:44,785
Pidge is a what?

66
00:04:45,994 --> 00:04:47,704
Hmm...

67
00:04:55,754 --> 00:04:57,130
[sniffs, groans]

68
00:04:58,298 --> 00:05:00,175
Are you sure?

69
00:05:00,258 --> 00:05:02,302
I'm getting to the bottom of this.

70
00:05:03,345 --> 00:05:05,764
I guess we should get used
to this space juice.

71
00:05:05,847 --> 00:05:08,016
Who knows
when we'll get back home again?

72
00:05:08,099 --> 00:05:10,227
Yeah, if ever.

73
00:05:10,310 --> 00:05:11,436
What do you mean?

74
00:05:11,520 --> 00:05:14,356
I mean, if this Zarkon guy has
been ruling for 10,000 years,

75
00:05:14,439 --> 00:05:16,566
how long do you think
it will take for us to fix it?

76
00:05:16,650 --> 00:05:17,984
You know, if we live.

77
00:05:18,068 --> 00:05:20,362
Right. That.

78
00:05:20,445 --> 00:05:23,532
Hey, what do you think the chances are
of us landing on a nacho planet?

79
00:05:24,157 --> 00:05:26,952
Well, there's only one planet
with Veradera Beach,

80
00:05:27,035 --> 00:05:32,582
pizza shack looking over the water
and the garlic knots and my mom's hugs...

81
00:05:32,666 --> 00:05:34,125
[sniffles]

82
00:05:34,209 --> 00:05:38,338
I'm sorry. I think this Nunvill
is getting to me. I gotta go.

83
00:05:39,339 --> 00:05:42,008
[sniffling, crying]

84
00:05:44,678 --> 00:05:48,557
So, Pidge, we haven't had
a chance to really talk.

85
00:05:48,640 --> 00:05:49,933
Tell me about yourself.

86
00:05:50,016 --> 00:05:51,226
Hmm...

87
00:05:51,309 --> 00:05:54,396
Well, I like peanut butter,
and I like peanut butter cookies,

88
00:05:54,479 --> 00:05:56,064
but I hate peanuts.

89
00:05:56,147 --> 00:05:57,232
They're so dry.

90
00:05:57,315 --> 00:06:01,319
Also, I sweat a lot. I mean, in general,
unrelated to the peanuts.

91
00:06:02,070 --> 00:06:06,199
I suppose I was thinking of something
a little more personal.

92
00:06:07,075 --> 00:06:09,828
We have a lot in common.

93
00:06:09,911 --> 00:06:11,288
Like what?

94
00:06:11,413 --> 00:06:15,875
Oh, well, both of us had our fathers
taken away by Zarkon.

95
00:06:15,959 --> 00:06:17,752
Yeah, but I'm going to get mine back.

96
00:06:17,836 --> 00:06:19,087
[gasps]

97
00:06:19,170 --> 00:06:23,592
- I'm sorry. I really didn't mean to...
- No, I understand.

98
00:06:23,675 --> 00:06:26,595
I just want you to know
that you can confide in me.

99
00:06:26,678 --> 00:06:30,390
If there's anything
you ever want to talk about...

100
00:06:32,100 --> 00:06:34,227
- Anything.
- Huh?

101
00:06:35,520 --> 00:06:40,525
- Okay. I do have something to tell you.
- I had a feeling. What is it?

102
00:06:41,526 --> 00:06:43,278
I'm leaving Team Voltron.

103
00:06:43,361 --> 00:06:45,822
[inhales]
Wait, what?

104
00:06:45,905 --> 00:06:48,033
I decrypted the information
from the Galra ship

105
00:06:48,116 --> 00:06:51,119
about where my family might be,
or at least where they were.

106
00:06:51,202 --> 00:06:53,330
I've made up my mind.
I'm leaving tonight.

107
00:06:53,413 --> 00:06:56,458
Pidge, you can't.
You're one of five paladins.

108
00:06:56,541 --> 00:06:59,002
You have a sacred trust
to defend the universe.

109
00:06:59,085 --> 00:07:01,588
My first priority is to find my family!

110
00:07:01,671 --> 00:07:03,590
I thought you of all people
would understand.

111
00:07:03,673 --> 00:07:06,134
If you had a chance to get
your father back, wouldn't you?

112
00:07:08,803 --> 00:07:10,764
I'm sorry.

113
00:07:10,847 --> 00:07:12,682
I should go tell everyone else.

114
00:07:21,232 --> 00:07:22,651
[Pidge] Shiro?

115
00:07:22,734 --> 00:07:24,277
I need to talk to you.

116
00:07:36,539 --> 00:07:38,041
[Haxus] Signature code cloned.

117
00:07:47,968 --> 00:07:49,219
Bomb activated.

118
00:08:07,654 --> 00:08:09,447
[Coran] Mind if I join you?

119
00:08:11,282 --> 00:08:13,743
How far away from Earth
do you think we are, Coran?

120
00:08:13,827 --> 00:08:15,036
Let's take a look.

121
00:08:17,414 --> 00:08:18,999
Earth is over here.

122
00:08:19,082 --> 00:08:20,750
And we're all...

123
00:08:23,169 --> 00:08:26,506
the way over...

124
00:08:26,589 --> 00:08:29,092
You ever notice how far the planets
are from each other, Coran?

125
00:08:29,175 --> 00:08:31,928
Yes. Haven't you been paying attention?

126
00:08:32,012 --> 00:08:36,057
Yeah, but I mean, like,
they're really, really far away.

127
00:08:36,141 --> 00:08:39,728
Like, say, Earth.
It's so far, I can't even see it.

128
00:08:39,811 --> 00:08:44,566
The... The blue oceans,
the white clouds, green grass...

129
00:08:44,649 --> 00:08:46,860
I... I can't see any of it.

130
00:08:46,943 --> 00:08:50,363
You miss Earth. I understand.
I miss Altea.

131
00:08:50,447 --> 00:08:52,532
I know we're supposed
to be brave paladins

132
00:08:52,615 --> 00:08:54,367
and Defenders of the Universe
or whatever,

133
00:08:54,451 --> 00:08:57,746
but, honestly, I just want to go home.

134
00:08:57,829 --> 00:09:00,206
If I could go home, I would.

135
00:09:00,290 --> 00:09:03,084
[Lance] I miss rain and
splashing in puddles.

136
00:09:03,168 --> 00:09:06,171
- Rain?
- Yeah. It's water that falls from the sky.

137
00:09:06,254 --> 00:09:07,839
[Coran] Oh, we had that on Altea.

138
00:09:07,922 --> 00:09:12,010
Only, it wasn't water, more like rocks.
Razor-sharp and boiling-hot.

139
00:09:12,093 --> 00:09:14,095
Oh, they could knock a hole
right in your head.

140
00:09:14,179 --> 00:09:16,556
- [Lance] Sounds fun.
- [Coran] Yeah.

141
00:09:30,612 --> 00:09:32,030
Pidge, no.

142
00:09:32,113 --> 00:09:33,907
The download
from the Galra ship was enough

143
00:09:33,990 --> 00:09:36,618
to at least get me in the right direction
to start my search.

144
00:09:36,701 --> 00:09:38,953
- I have a pod all ready to go.
- You can't leave.

145
00:09:39,037 --> 00:09:44,084
- You can't tell me what to do!
- If you leave, we can't form Voltron.

146
00:09:44,167 --> 00:09:46,711
And that means we can't defend
the universe against Zarkon.

147
00:09:46,795 --> 00:09:49,005
You're not the only one with a family.

148
00:09:49,089 --> 00:09:53,802
All these Arusians have families.
Everyone in the universe has families.

149
00:09:53,885 --> 00:09:55,512
Yeah, I have a family.
They live on Earth.

150
00:09:55,595 --> 00:09:58,056
I want to be with them.
Is that, like, a thing that can happen?

151
00:09:58,139 --> 00:10:00,934
- You want to leave, too?
- Of course I do!

152
00:10:01,017 --> 00:10:03,061
Look, Voltron is super-cool,
don't get me wrong,

153
00:10:03,144 --> 00:10:07,023
but I never signed up for a lifetime
in space, fighting aliens.

154
00:10:07,107 --> 00:10:09,359
You're putting the lives of two people

155
00:10:09,442 --> 00:10:12,028
over the lives of everyone else
in the entire galaxy!

156
00:10:12,112 --> 00:10:14,614
Keith, that's not how a team works.

157
00:10:14,697 --> 00:10:17,617
People have to want to be a part of it.
They can't be forced.

158
00:10:19,661 --> 00:10:22,122
If you want to leave,
we won't try to stop you.

159
00:10:22,205 --> 00:10:25,875
But, please, just think
about what you're doing.

160
00:10:28,628 --> 00:10:33,633
I'm sorry. You're going to have to find
someone else to pilot the Green Lion.

161
00:10:33,716 --> 00:10:35,677
[Rover squeaking]

162
00:10:35,760 --> 00:10:38,721
I can't believe it.
This team is falling apart.

163
00:10:38,805 --> 00:10:40,890
How will we ever form Voltron?

164
00:10:47,397 --> 00:10:48,481
Hey, Rover.

165
00:10:52,277 --> 00:10:54,696
Wait. Where's Pidge?

166
00:10:54,779 --> 00:10:56,239
[beeping]

167
00:10:56,322 --> 00:10:57,949
[gasps]

168
00:10:58,032 --> 00:10:59,409
Coran, look out!
[screams]

169
00:10:59,492 --> 00:11:00,910
[Arusians scream]

170
00:11:00,994 --> 00:11:03,872
[Hunk whimpering]

171
00:11:03,955 --> 00:11:05,081
Huh?

172
00:11:06,291 --> 00:11:07,792
[screams]

173
00:11:08,543 --> 00:11:09,878
[screams]

174
00:11:14,299 --> 00:11:16,009
[powering down]

175
00:11:23,850 --> 00:11:25,184
[metal clattering]

176
00:11:26,185 --> 00:11:29,063
[groaning]

177
00:11:29,147 --> 00:11:32,150
[coughing]

178
00:11:32,233 --> 00:11:35,320
- What happened?
- [groans] I'm not sure.

179
00:11:38,865 --> 00:11:40,408
[gasps]
The crystal!

180
00:11:41,659 --> 00:11:42,660
Lance!

181
00:11:43,703 --> 00:11:46,372
Lance? Lance!
[groans]

182
00:11:47,665 --> 00:11:49,751
We have to get Lance to the infirmary!

183
00:11:49,834 --> 00:11:52,378
Without the crystal,
the Castle has no power.

184
00:11:54,797 --> 00:11:56,049
He doesn't look good.

185
00:11:56,132 --> 00:12:00,345
Lion warriors, our village
is under attack! We need help!

186
00:12:00,428 --> 00:12:02,931
- Let's get to the lions!
- You can't.

187
00:12:03,014 --> 00:12:06,142
They're sealed in their hangars.
There's no way to get them out.

188
00:12:06,225 --> 00:12:08,895
- We're defenseless.
- Will you not help us?

189
00:12:08,978 --> 00:12:12,774
We'll help you. We just...
[Lance groans]

190
00:12:13,816 --> 00:12:15,151
This is bad.

191
00:12:15,234 --> 00:12:17,654
We have to get a new crystal
to get the Castle working again.

192
00:12:17,737 --> 00:12:19,572
But, to get a new crystal,
we need a ship.

193
00:12:19,656 --> 00:12:22,992
The pod I was loading, we can use that!
I left the bay door open.

194
00:12:23,076 --> 00:12:26,162
I can use the scanner in the pod
to see if there's a Balmera nearby.

195
00:12:26,245 --> 00:12:29,415
Hunk, you come with me. I'll need
someone big to help me carry the crystal.

196
00:12:29,499 --> 00:12:30,583
A Balmera?

197
00:12:30,667 --> 00:12:33,378
It's where the crystals come from.
I'll tell you about it on the way!

198
00:12:34,045 --> 00:12:36,047
I'll go see what's happening
at the Arusian village.

199
00:12:36,130 --> 00:12:39,342
I'll go with you, Keith.
I brought this on the poor Arusians.

200
00:12:39,425 --> 00:12:42,095
I'll tend to Lance
and stand watch over the Castle.

201
00:12:42,178 --> 00:12:44,347
[groaning]

202
00:12:48,518 --> 00:12:50,979
It's our first bit of luck.
There's a source not too far.

203
00:12:51,062 --> 00:12:53,272
We won't need a wormhole
to get there, thankfully.

204
00:12:55,817 --> 00:12:57,986
I made some modifications to the shuttle.

205
00:12:58,069 --> 00:13:00,905
The first change is a cloaking device
that I reverse-engineered

206
00:13:00,989 --> 00:13:03,366
from the invisible walls
on the training deck.

207
00:13:03,449 --> 00:13:06,619
The second is a tank of booster fuel
that I mounted on the fuel line.

208
00:13:06,703 --> 00:13:09,789
Using that during flight would turn
the whole pod into a bomb!

209
00:13:09,872 --> 00:13:11,124
[whimpering]

210
00:13:11,207 --> 00:13:14,252
Okay. Maybe you shouldn't use
that modification after all.

211
00:13:15,169 --> 00:13:17,672
- [Hunk] We ready to hit it?
- Right. Let's go.

212
00:13:23,970 --> 00:13:24,971
Good luck.

213
00:13:36,315 --> 00:13:39,485
Hang on, buddy.
Help is on the way.

214
00:13:50,705 --> 00:13:51,789
Sendak.

215
00:14:06,554 --> 00:14:08,347
Stand aside.

216
00:14:09,015 --> 00:14:11,350
No. You're not getting in.

217
00:14:11,434 --> 00:14:13,561
Yes, I am.

218
00:14:16,189 --> 00:14:19,108
[grunts, screams]

219
00:14:19,984 --> 00:14:22,320
[grunts, groans]

220
00:14:24,030 --> 00:14:25,615
[grunts]

221
00:14:30,078 --> 00:14:31,496
[yells]

222
00:14:42,673 --> 00:14:44,092
[yells]

223
00:14:44,175 --> 00:14:46,260
[grunts]

224
00:14:48,554 --> 00:14:53,434
I see you spent some time with the druids.
They do love to experiment.

225
00:14:54,060 --> 00:14:56,104
Too bad you didn't get the latest model.

226
00:14:57,271 --> 00:14:58,648
[grunts]

227
00:15:00,483 --> 00:15:03,569
[grunting]

228
00:15:03,903 --> 00:15:05,488
[grunts]

229
00:15:19,043 --> 00:15:21,254
[yelling]

230
00:15:23,631 --> 00:15:25,967
[breathing heavily]

231
00:15:28,261 --> 00:15:30,513
[Haxus] Let him go or
your friend won't make it!

232
00:15:31,597 --> 00:15:32,515
[groans]

233
00:15:46,279 --> 00:15:48,448
Voltron is ours.

234
00:15:52,493 --> 00:15:53,786
Oh, no.

235
00:15:58,666 --> 00:16:01,627
- What's happening?
- Look! Attackers!

236
00:16:04,547 --> 00:16:06,048
I'll go in for a closer look.

237
00:16:06,132 --> 00:16:07,633
- Stay here with them.
- Keith!

238
00:16:09,218 --> 00:16:10,261
[grunts]

239
00:16:26,444 --> 00:16:27,278
What?

240
00:16:30,781 --> 00:16:33,409
Oh, no. They tricked us!

241
00:16:33,493 --> 00:16:38,164
<i>[over comm] It was just a diversion to
separate us and thin the Castle defenses!</i>

242
00:16:40,625 --> 00:16:42,210
[Sendak] Power up the Castle.

243
00:17:00,561 --> 00:17:03,231
The lions are all in their bays.

244
00:17:03,314 --> 00:17:07,443
Raising particle barrier.
Begin launch sequence.

245
00:17:12,448 --> 00:17:13,908
[both grunt]

246
00:17:13,991 --> 00:17:16,494
We're too late. No!

247
00:17:16,577 --> 00:17:20,373
They have control of the Castle.
They're taking Voltron!

248
00:17:22,959 --> 00:17:25,169
Make contact with Emperor Zarkon.

249
00:17:31,050 --> 00:17:32,176
<i>Sendak.</i>

250
00:17:32,260 --> 00:17:33,844
My mission is complete.

251
00:17:33,928 --> 00:17:37,932
I've captured the Altean Castle,
along with all of the Voltron lions.

252
00:17:38,015 --> 00:17:40,142
I am currently preparing for launch

253
00:17:40,226 --> 00:17:42,895
and will be delivering them all
to you shortly.

254
00:17:42,979 --> 00:17:48,943
<i>This news is most pleasing.
You have done your duty. Vrepit Sa!</i>

255
00:17:49,026 --> 00:17:50,987
<i>Vrepit Sa!</i>

256
00:17:51,112 --> 00:17:55,157
- Haxus, ready the Castle for takeoff.
- Yes, Commander.

257
00:17:59,078 --> 00:18:00,955
- Can we break through the barrier?
- No.

258
00:18:01,038 --> 00:18:03,624
And whoever has taken the Castle
has a crystal,

259
00:18:03,708 --> 00:18:06,127
which means they'll be able
to fly the ship.

260
00:18:06,210 --> 00:18:07,545
We have to stop them.

261
00:18:08,629 --> 00:18:11,882
<i>- How are we going to do that?
- [Pidge] Keith, can you hear me?</i>

262
00:18:11,966 --> 00:18:14,594
Pidge, is that you? Where are you?

263
00:18:14,677 --> 00:18:16,178
I'm inside the Castle.

264
00:18:16,262 --> 00:18:18,931
Sendak has taken over
and he's preparing for launch.

265
00:18:19,015 --> 00:18:20,516
He's got Lance and Shiro.

266
00:18:21,183 --> 00:18:23,853
Pidge, listen. If they've started
the launch sequence,

267
00:18:23,936 --> 00:18:27,231
then we don't have much time
before liftoff to stop it.

268
00:18:27,315 --> 00:18:28,649
What do I have to do?

269
00:18:28,733 --> 00:18:31,736
You have to get down
to the main engine control panel

270
00:18:31,819 --> 00:18:36,073
and disconnect the primary turbine
from the central energy chamber.

271
00:18:36,157 --> 00:18:39,952
<i>If you can do that, Sendak will have
to reset the whole system.</i>

272
00:18:40,036 --> 00:18:42,663
<i>That might give us enough time
to find a way to stop them.</i>

273
00:18:47,376 --> 00:18:49,545
<i>[Hunk] Is this the Balmera planet
with the crystals?</i>

274
00:18:49,629 --> 00:18:52,256
It's not a planet.
Balmera are ancient animals.

275
00:18:52,340 --> 00:18:54,050
Petrified, but still alive.

276
00:18:54,842 --> 00:18:59,263
<i>Their bodies naturally create the crystals
that help power mini Altean ships.</i>

277
00:18:59,347 --> 00:19:02,308
I often accompanied my grandfather
to visit these majestic creatures

278
00:19:02,391 --> 00:19:04,352
when he was building the Castle of Lions.

279
00:19:04,435 --> 00:19:07,980
I'll never forget the first time
I saw the sparkling surface of a Balmera.

280
00:19:08,064 --> 00:19:09,482
You're in for quite a treat.

281
00:19:12,652 --> 00:19:15,696
Oh, no. This is horrifying.

282
00:19:15,780 --> 00:19:18,366
<i>The Galra have turned this
into a mining colony!</i>

283
00:19:18,449 --> 00:19:22,286
<i>They're completely destroying it!
They have no regard for the poor creature!</i>

284
00:19:24,580 --> 00:19:27,166
[alarm blaring]
[Hunk] Uh-oh. We've been spotted.

285
00:19:27,792 --> 00:19:29,585
<i>[male voice] Hailing unidentified craft.</i>

286
00:19:29,669 --> 00:19:33,005
<i>State your ship ID, entry code,
and landing destination.</i>

287
00:19:33,089 --> 00:19:37,259
- Oh, no. What do we do?
- Just stay calm.

288
00:19:37,343 --> 00:19:38,928
[into radio]
We don't really need to land.

289
00:19:39,011 --> 00:19:41,597
Uh, we're just looking around,
if that's okay.

290
00:19:41,681 --> 00:19:43,933
<i>[male voice] Unidentified
craft, land immediately</i>

291
00:19:44,016 --> 00:19:46,185
<i>and prepare to be boarded.</i>

292
00:19:46,268 --> 00:19:50,064
Okay. Thank you.
See you down below.

293
00:19:53,401 --> 00:19:54,694
[screaming]

294
00:20:03,202 --> 00:20:04,704
<i>[Coran] Not done yet.
Hang on!</i>

295
00:20:04,787 --> 00:20:07,039
[Hunk screaming]

296
00:20:07,123 --> 00:20:08,958
[screaming continues]

297
00:20:11,669 --> 00:20:13,462
[screaming continues]

298
00:20:14,296 --> 00:20:16,215
[Hunk screaming]

299
00:20:16,298 --> 00:20:17,967
<i>Coran! Coran!</i>

300
00:20:18,592 --> 00:20:20,219
[screaming]

301
00:20:22,263 --> 00:20:24,432
How deep is this thing?

302
00:20:24,515 --> 00:20:25,975
<i>[Hunk] Oh, no, no, no!</i>

303
00:20:27,101 --> 00:20:29,061
[both screaming]

304
00:20:33,482 --> 00:20:35,943
[groaning]

305
00:20:37,111 --> 00:20:39,613
[groaning]

306
00:20:39,697 --> 00:20:41,157
[gasps]

307
00:20:43,325 --> 00:20:44,618
[grunts]

308
00:20:51,333 --> 00:20:54,879
[Hunk] Coran, what lives
at the bottom of these mines?

309
00:20:59,633 --> 00:21:03,471
[grunting]

310
00:21:04,138 --> 00:21:05,723
Here, Rover.

311
00:21:05,806 --> 00:21:08,517
[Rover squeaking]

312
00:21:08,601 --> 00:21:10,394
Okay, Rover, here we go.

313
00:21:16,108 --> 00:21:18,986
Run main cluster activation sequence.

314
00:21:19,069 --> 00:21:22,531
Activation sequence initiated.
Powering up for launch.

315
00:21:24,492 --> 00:21:27,161
Okay, Allura, I'm near the turbine.
I think it's started.

316
00:21:27,244 --> 00:21:28,871
Then you'll have to hurry.

317
00:21:28,954 --> 00:21:31,540
Cross the catwalk
to the main column in the center.

318
00:21:34,877 --> 00:21:37,338
<i>- Okay.
- Now, open the hatch.</i>

319
00:21:37,421 --> 00:21:41,425
<i>Find the central computer control hub
and enter the following sequence...</i>

320
00:21:41,509 --> 00:21:45,554
Wait, wait, wait, wait! Which one is it?
All the labels are in Altean!

321
00:21:45,638 --> 00:21:48,015
Commander Sendak,
we are ready for launch.

322
00:21:54,313 --> 00:21:58,567
I can't tell which one it is!
Allura? Allura?

323
00:22:04,323 --> 00:22:05,282
Uh...

324
00:22:05,366 --> 00:22:09,745
I've lost connection with Pidge!
Oh, no! It's taking off!

325
00:22:11,330 --> 00:22:12,665
[growls]

326
00:22:13,916 --> 00:22:15,543
Uh...

327
00:22:17,253 --> 00:22:18,254
Whatever.

328
00:22:19,421 --> 00:22:22,466
[screams, groans]

329
00:22:22,550 --> 00:22:23,551
Huh?

330
00:22:29,890 --> 00:22:31,350
[groans]

331
00:22:31,433 --> 00:22:34,103
[chuckles]
[Rover beeping]

332
00:22:34,937 --> 00:22:36,438
The main engine just shorted out!

333
00:22:38,607 --> 00:22:40,818
[computer beeping]

334
00:22:43,904 --> 00:22:48,701
We have a saboteur.
Find him and take him out.

335
00:22:51,389 --> 00:22:54,238
<i>sync & corr. by f1nc0
- - Addic7ed.com - -</i>

