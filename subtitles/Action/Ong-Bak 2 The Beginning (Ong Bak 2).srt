﻿1
00:02:08,560 --> 00:02:12,485
Narrator:
In 1431 A.D.,

2
00:02:12,640 --> 00:02:17,771
in the reign
of King Borommaracha Thirat II,

3
00:02:17,920 --> 00:02:20,082
the Kingdom of Ayothaya

4
00:02:20,280 --> 00:02:23,887
has seized the territory
of Sukhothai

5
00:02:24,080 --> 00:02:28,449
and has expanded its power
across the highland

6
00:02:28,600 --> 00:02:31,001
to the East.

7
00:02:31,160 --> 00:02:34,369
After a seven-month siege,

8
00:02:34,520 --> 00:02:39,242
Ayothaya captured the divine city.

9
00:02:39,400 --> 00:02:43,644
One year
and three months later...

10
00:02:46,080 --> 00:02:48,321
(thunder rumbling )

11
00:03:20,800 --> 00:03:22,882
(whinnies)

12
00:03:29,960 --> 00:03:31,291
( shouting )

13
00:04:12,160 --> 00:04:14,322
Take care of yourself,
young master.

14
00:04:25,080 --> 00:04:26,366
Shoot!

15
00:04:30,280 --> 00:04:32,851
( labored breathing )

16
00:04:47,720 --> 00:04:49,882
( thunder crashing )

17
00:05:05,560 --> 00:05:07,961
Get the boy.

18
00:05:27,080 --> 00:05:29,242
( Birds chirping )

19
00:05:46,440 --> 00:05:48,488
Hey! Stop!

20
00:06:07,120 --> 00:06:09,885
( Shouting )

21
00:07:01,160 --> 00:07:02,321
(growling )

22
00:07:27,880 --> 00:07:30,247
Hey!
Come down quickly!

23
00:07:30,400 --> 00:07:32,448
Hurry UP-

24
00:07:32,600 --> 00:07:35,729
Hey, come down!

25
00:07:35,880 --> 00:07:39,123
Not going, are you?

26
00:07:40,920 --> 00:07:44,527
Let me go! Let go!

27
00:07:50,920 --> 00:07:52,445
Let go of me!

28
00:07:52,600 --> 00:07:53,647
Get up here.

29
00:07:53,800 --> 00:07:55,450
Let go of me!

30
00:07:55,600 --> 00:07:58,285
- Little troublemaker, huh?
- I said, let go of me!

31
00:07:59,280 --> 00:08:02,090
( Screams )

32
00:08:02,240 --> 00:08:04,607
Make sure he's the next one.

33
00:08:06,320 --> 00:08:10,006
- Let me go.
- Get up. Go.

34
00:08:10,160 --> 00:08:12,242
Go.

35
00:08:39,520 --> 00:08:41,841
( Chanting )

36
00:08:43,520 --> 00:08:45,807
( breathing heavily )

37
00:09:04,680 --> 00:09:06,921
( laughs )

38
00:09:08,920 --> 00:09:11,321
He's the next fighter.

39
00:09:11,480 --> 00:09:13,801
( laughing )

40
00:09:27,120 --> 00:09:29,691
I will give you a chance, kid.

41
00:09:29,880 --> 00:09:33,601
Let's see if you can
make it out alive.

42
00:09:35,920 --> 00:09:37,524
(yelling)

43
00:09:50,800 --> 00:09:53,007
( screaming )

44
00:10:06,520 --> 00:10:09,524
- ( gasping )
- ( chanting )

45
00:10:16,040 --> 00:10:18,088
Begin.

46
00:10:21,040 --> 00:10:22,041
( <i>Gasps</i> )

47
00:10:50,480 --> 00:10:52,482
( roaring )

48
00:11:17,080 --> 00:11:18,764
( screams )

49
00:11:35,160 --> 00:11:37,481
(grunts)

50
00:11:43,880 --> 00:11:45,962
- ( roars )
- ( screams )

51
00:11:49,440 --> 00:11:50,805
( groans )

52
00:12:16,840 --> 00:12:19,161
( panting )

53
00:12:21,720 --> 00:12:26,442
Your life is
in your own hands, kid.

54
00:13:41,560 --> 00:13:43,562
( Panting )

55
00:14:20,280 --> 00:14:21,167
Tien.

56
00:14:21,320 --> 00:14:22,765
Young master.
Get away.

57
00:14:22,960 --> 00:14:24,166
It's because of you.

58
00:14:24,320 --> 00:14:25,526
Traitor.

59
00:14:25,680 --> 00:14:27,444
I want to be a great warrior
like my father.

60
00:14:27,600 --> 00:14:28,886
How long will you be away?

61
00:14:29,040 --> 00:14:31,441
- I hate you.
- Give him to me.

62
00:14:31,640 --> 00:14:33,449
Father...

63
00:14:33,640 --> 00:14:35,961
(thunder rumbling )

64
00:14:39,280 --> 00:14:42,124
(groaning )

65
00:15:21,200 --> 00:15:24,044
Is this the boy?

66
00:15:24,200 --> 00:15:27,841
That's right, Father Sengpa.

67
00:15:28,000 --> 00:15:30,571
The way he looked

68
00:15:30,720 --> 00:15:32,927
when he was fighting,

69
00:15:33,080 --> 00:15:35,526
reminded me of someone.

70
00:16:01,280 --> 00:16:04,329
Even the spirits
don't dare touch him.

71
00:16:05,480 --> 00:16:07,528
He has a powerful destiny.

72
00:16:12,960 --> 00:16:15,611
Whenever he holds a weapon

73
00:16:15,760 --> 00:16:18,001
in his hands,

74
00:16:18,200 --> 00:16:20,567
he can conquer the world.

75
00:16:25,040 --> 00:16:27,281
I'm Chernang,

76
00:16:27,440 --> 00:16:30,887
the king of the outlaws
here at Garuda Wing Cliff.

77
00:16:31,040 --> 00:16:35,170
I was impressed
by your courageous fight

78
00:16:35,320 --> 00:16:37,971
with the crocodile.

79
00:16:38,120 --> 00:16:42,967
If you want to train in the way
of weapons and martial arts,

80
00:16:43,120 --> 00:16:47,011
you can stay here.

81
00:16:47,160 --> 00:16:49,401
But if you don't,

82
00:16:49,560 --> 00:16:53,770
I will not stand in your way.

83
00:17:22,440 --> 00:17:24,920
( Explosion )

84
00:17:25,080 --> 00:17:26,684
( laughs )

85
00:18:54,880 --> 00:18:57,008
Chernang:
Unite your body and soul.

86
00:18:57,160 --> 00:18:59,731
Fuse the rules of fighting.

87
00:18:59,880 --> 00:19:02,167
Set your mind
on the path of power.

88
00:19:02,320 --> 00:19:04,800
That is what you have
to accomplish.

89
00:19:15,440 --> 00:19:19,001
Each weapon has its own strengths
and weaknesses.

90
00:19:19,160 --> 00:19:20,810
Short or long.

91
00:19:20,960 --> 00:19:22,485
Sharp or blunt.

92
00:19:22,680 --> 00:19:24,045
Soft or hard.

93
00:19:24,200 --> 00:19:26,248
Heavy or light.

94
00:19:38,320 --> 00:19:41,642
Weapons are the tangible form
of power.

95
00:19:41,800 --> 00:19:45,646
Anyone who can fuse
his body and soul with them

96
00:19:45,800 --> 00:19:50,806
shall possess the greatest
power in the land.

97
00:19:51,000 --> 00:19:53,651
The world will bow down
to your strength.

98
00:20:12,440 --> 00:20:13,726
I am Chernang,

99
00:20:13,880 --> 00:20:16,486
the outlaw king
at Garuda Wing Cliff.

100
00:20:16,640 --> 00:20:19,689
I've given you a new life.

101
00:20:21,440 --> 00:20:25,843
From now on,
always remember...

102
00:20:26,000 --> 00:20:30,767
you are one of us,
the outlaws of Garuda Wing Cliff.

103
00:21:21,680 --> 00:21:23,330
(grunts)

104
00:21:23,480 --> 00:21:24,845
( trumpets )

105
00:21:28,040 --> 00:21:30,168
( trumpeting )

106
00:22:23,600 --> 00:22:25,728
( bellows )

107
00:22:38,600 --> 00:22:40,648
( bellows )

108
00:22:57,200 --> 00:22:59,282
( bellows )

109
00:23:07,400 --> 00:23:09,368
( shouts )

110
00:23:30,000 --> 00:23:32,162
( bellows )

111
00:23:52,560 --> 00:23:54,961
( trumpeting )

112
00:24:12,160 --> 00:24:17,769
The test of agility and wit.

113
00:24:17,920 --> 00:24:20,890
Tien has passed.

114
00:24:21,040 --> 00:24:27,241
Next, is the test of martial arts.

115
00:25:44,320 --> 00:25:45,651
(grunts)

116
00:26:22,760 --> 00:26:24,603
( rings )

117
00:26:42,600 --> 00:26:44,364
( shouts )

118
00:27:24,480 --> 00:27:26,562
(grunting )

119
00:29:15,200 --> 00:29:16,690
( shouting )

120
00:30:34,520 --> 00:30:37,091
( screams )

121
00:30:37,240 --> 00:30:38,924
( laughs )

122
00:30:42,240 --> 00:30:45,084
That's what I'm talking about,
my son!

123
00:30:56,480 --> 00:31:01,122
Chernang: This place will test
the power of your mind.

124
00:31:06,680 --> 00:31:08,728
Woman:
Help me.

125
00:31:10,760 --> 00:31:12,524
Let me go.

126
00:31:15,200 --> 00:31:17,282
Help me.

127
00:31:22,280 --> 00:31:24,362
Help

128
00:31:34,920 --> 00:31:36,968
Help me.

129
00:31:38,200 --> 00:31:40,646
Don't be afraid.

130
00:31:40,800 --> 00:31:42,689
I'm here to help you.

131
00:31:42,880 --> 00:31:44,370
I'll get you out of here.

132
00:31:44,520 --> 00:31:46,648
Come with me.

133
00:31:48,360 --> 00:31:50,249
I'll help you.

134
00:31:50,400 --> 00:31:55,042
Don't be afraid.

135
00:32:05,560 --> 00:32:07,164
( Hisses )

136
00:32:08,160 --> 00:32:09,844
(growling )

137
00:32:23,920 --> 00:32:25,285
( hisses )

138
00:32:31,720 --> 00:32:33,165
( growls )

139
00:32:38,160 --> 00:32:40,049
(growling )

140
00:33:13,520 --> 00:33:17,411
Your life depends on yourself.

141
00:33:44,800 --> 00:33:48,361
To kill someone,
to be merciless,

142
00:33:48,520 --> 00:33:53,845
is the last thing
a leader must learn.

143
00:34:01,120 --> 00:34:04,090
Father Sengpa:
My people, my children!

144
00:34:05,600 --> 00:34:10,128
The spirits in the sky

145
00:34:10,280 --> 00:34:16,686
have written
that the great black-tusk

146
00:34:16,840 --> 00:34:21,368
shall become the king
of all elephants.

147
00:34:21,520 --> 00:34:23,966
This sacred animal

148
00:34:24,160 --> 00:34:27,084
will bring prosperity

149
00:34:27,240 --> 00:34:29,686
to this community.

150
00:34:29,880 --> 00:34:30,881
( Cheering )

151
00:34:31,040 --> 00:34:33,088
Hooray!

152
00:34:38,720 --> 00:34:40,563
Now...

153
00:34:40,760 --> 00:34:42,922
I, Chernang,

154
00:34:43,080 --> 00:34:45,208
hereby proclaim that

155
00:34:45,360 --> 00:34:47,362
from now on,

156
00:34:47,560 --> 00:34:50,291
in all the raids,

157
00:34:50,480 --> 00:34:52,562
Tien

158
00:34:52,720 --> 00:34:54,643
will replace me as the leader.

159
00:34:54,800 --> 00:34:55,847
( Cheering )

160
00:34:56,000 --> 00:34:58,446
Hooray!

161
00:35:17,440 --> 00:35:19,568
( Cheering )

162
00:35:55,840 --> 00:35:57,569
( horses whinny )

163
00:36:07,600 --> 00:36:09,648
( shouting )

164
00:37:26,000 --> 00:37:27,843
( roaring )

165
00:38:03,980 --> 00:38:05,927
Why haven't you taught me
the art of weaponry?

166
00:38:05,929 --> 00:38:09,356
Young Tien:
I don't want to study the dancing arts.

167
00:38:09,400 --> 00:38:11,129
I don't want to go.

168
00:38:11,280 --> 00:38:13,089
I want to stay with you.

169
00:38:13,240 --> 00:38:17,211
You have to study so you'll
grow up to be a great man.

170
00:38:17,360 --> 00:38:19,010
Believe me.

171
00:38:36,280 --> 00:38:39,204
(thunder rumbling )

172
00:38:47,120 --> 00:38:49,168
Pay respect to Master Bua.

173
00:38:49,320 --> 00:38:51,209
You've traveled so far.

174
00:38:51,360 --> 00:38:53,442
Let's go inside.

175
00:39:00,440 --> 00:39:02,522
It's been more than three months

176
00:39:02,680 --> 00:39:05,968
since Ayothaya took control
of the vessel states.

177
00:39:08,000 --> 00:39:13,325
The situation in the capital
is still in disorder.

178
00:39:13,480 --> 00:39:18,520
But the Crown Prince
is behaving impetuously.

179
00:39:18,680 --> 00:39:22,571
He has ordered me
to suppress resistances

180
00:39:22,720 --> 00:39:26,486
in the protectorate territories.

181
00:39:26,640 --> 00:39:30,281
Why is Ayothaya in such a rush
to expand its power?

182
00:39:30,440 --> 00:39:33,489
Why didn't Lord Rajasena,
chief of the royal army,

183
00:39:33,680 --> 00:39:35,842
oppose the Crown Prince?

184
00:39:38,400 --> 00:39:40,926
Everything

185
00:39:41,080 --> 00:39:44,129
was Lord Rajasena's idea.

186
00:39:46,080 --> 00:39:48,651
Power is terrifying.

187
00:39:48,800 --> 00:39:52,521
But those who abuse power
are even more terrifying.

188
00:39:52,680 --> 00:39:54,808
This land will burn.

189
00:39:54,960 --> 00:39:59,045
People in every corner will suffer.

190
00:39:59,200 --> 00:40:02,204
In this time of uncertainty,

191
00:40:04,400 --> 00:40:07,051
there is only you

192
00:40:07,200 --> 00:40:09,680
in this distant land

193
00:40:09,880 --> 00:40:12,087
to whom I can entrust

194
00:40:12,240 --> 00:40:14,322
my son's life.

195
00:40:14,480 --> 00:40:17,404
Master Bua,

196
00:40:17,600 --> 00:40:20,888
I feel like my heart
has been pulled out.

197
00:40:23,200 --> 00:40:25,521
May you take pity on me

198
00:40:25,680 --> 00:40:29,127
and take care of him.

199
00:40:32,360 --> 00:40:34,886
Master Bua:
I will try my best.

200
00:40:35,040 --> 00:40:38,647
You take care
of yourselves as well.

201
00:40:38,800 --> 00:40:41,724
Muen Sri. Luang Krai.

202
00:40:41,880 --> 00:40:43,723
Take us back.

203
00:40:44,920 --> 00:40:47,082
Yes, sir.

204
00:41:22,000 --> 00:41:25,686
My name is Pim.
I'm an orphan.

205
00:41:25,840 --> 00:41:29,731
Master Bua has raised me
since I was a baby.

206
00:41:29,920 --> 00:41:32,082
How about you?

207
00:41:32,240 --> 00:41:34,971
My father is Lord Sihadecho,

208
00:41:35,120 --> 00:41:39,250
one of the four commanders
of the East.

209
00:41:39,400 --> 00:41:44,804
I told my father I want to study
weapons and martial arts.

210
00:41:44,960 --> 00:41:47,327
I want to be a great warrior
like my father.

211
00:41:47,520 --> 00:41:49,284
I don't understand

212
00:41:49,480 --> 00:41:52,643
why he sent me here
to study dancing.

213
00:41:52,800 --> 00:41:54,962
How can I fight anyone then?

214
00:42:08,160 --> 00:42:10,128
( Imitating flute )

215
00:42:13,520 --> 00:42:16,126
Hoarse flute.

216
00:42:24,680 --> 00:42:26,648
( Rattling )

217
00:42:42,640 --> 00:42:44,404
Hey!

218
00:42:56,200 --> 00:42:58,726
Ooh, itchy.
Scratching your balls too?

219
00:42:58,880 --> 00:43:00,882
No, I'm not itchy.
Why are you scratching?

220
00:43:01,040 --> 00:43:02,883
I have it too. The same.

221
00:43:04,320 --> 00:43:05,560
Throw it up.

222
00:43:09,880 --> 00:43:12,645
Oh, throwing it up too.

223
00:43:15,600 --> 00:43:17,489
Turn around,

224
00:43:18,600 --> 00:43:22,571
around, around.

225
00:43:24,400 --> 00:43:26,641
He's gone.

226
00:43:26,800 --> 00:43:28,165
Oh, he's really gone.

227
00:43:30,400 --> 00:43:31,481
Gone.

228
00:43:39,200 --> 00:43:41,043
( Sizzling )

229
00:43:47,800 --> 00:43:49,325
Tien,

230
00:43:49,520 --> 00:43:51,807
do you think being
a master of weapons

231
00:43:51,960 --> 00:43:54,964
can bring peace to the land?

232
00:43:57,120 --> 00:43:59,327
I think

233
00:43:59,480 --> 00:44:02,962
that weapons can help me
protect myself,

234
00:44:03,160 --> 00:44:06,482
assist righteous people,
and fight treacherous people.

235
00:44:06,640 --> 00:44:10,042
That should contribute
to bringing about peace.

236
00:44:10,200 --> 00:44:13,568
Well said.

237
00:44:15,000 --> 00:44:16,286
Tien,

238
00:44:16,440 --> 00:44:19,762
there are various kinds of weapons
in the world.

239
00:44:21,160 --> 00:44:23,561
Which ones
do you wish to study

240
00:44:23,760 --> 00:44:25,728
so you can help your homeland?

241
00:44:28,280 --> 00:44:30,760
( Chuckles )

242
00:44:30,920 --> 00:44:33,287
You don't have to answer me
right now.

243
00:45:10,680 --> 00:45:12,011
Hey, take it.

244
00:45:18,720 --> 00:45:20,688
( laughs )

245
00:45:27,560 --> 00:45:29,722
(gongs)

246
00:45:39,320 --> 00:45:42,961
Let the slave trade begin.

247
00:45:44,000 --> 00:45:46,207
(wailing )

248
00:45:55,240 --> 00:45:57,720
Hey, come here.

249
00:45:57,880 --> 00:45:59,211
Let me go.

250
00:45:59,400 --> 00:46:00,845
Mother!

251
00:46:01,000 --> 00:46:03,526
- Hey, come here.
- Let me go.

252
00:46:03,680 --> 00:46:06,923
My son! My son!

253
00:46:09,760 --> 00:46:11,250
Go!

254
00:46:11,400 --> 00:46:16,088
My son! My son!

255
00:46:16,240 --> 00:46:19,164
Mother! Mother!

256
00:46:27,040 --> 00:46:29,088
Mother!

257
00:46:30,520 --> 00:46:31,760
( Groans )

258
00:46:32,880 --> 00:46:35,042
(whimpering )

259
00:46:38,560 --> 00:46:40,801
( screaming )

260
00:46:59,800 --> 00:47:02,121
Who the hell?

261
00:47:03,400 --> 00:47:05,607
Was it you?

262
00:47:07,800 --> 00:47:09,928
Was it you?

263
00:47:23,240 --> 00:47:25,402
You want to drink?

264
00:47:27,040 --> 00:47:29,486
Drink it!

265
00:47:31,360 --> 00:47:32,486
( laughing )

266
00:47:32,640 --> 00:47:34,563
(grunts)

267
00:48:01,840 --> 00:48:04,286
( screaming )

268
00:49:18,760 --> 00:49:20,125
( groans )

269
00:49:38,320 --> 00:49:40,322
( laughs )

270
00:49:44,640 --> 00:49:46,563
( shouts )

271
00:50:01,120 --> 00:50:02,963
( snapping )

272
00:50:26,040 --> 00:50:27,405
(whimpering )

273
00:51:09,280 --> 00:51:10,645
( screaming )

274
00:51:21,040 --> 00:51:24,044
(whimpering )

275
00:51:31,000 --> 00:51:34,925
Please spare my life.
I don't want to die.

276
00:51:35,080 --> 00:51:36,923
I have money. I have slaves.

277
00:51:37,080 --> 00:51:38,491
Whatever you want,
you can have it.

278
00:51:38,640 --> 00:51:40,722
Good.

279
00:51:40,880 --> 00:51:42,689
I'll give you a chance.

280
00:51:48,400 --> 00:51:51,370
The same one you gave me...

281
00:52:02,880 --> 00:52:04,120
Throw him to the crocodiles!

282
00:52:04,280 --> 00:52:06,601
Huh? No!

283
00:52:08,400 --> 00:52:11,449
I don't want to die.

284
00:52:12,440 --> 00:52:14,727
I don't want to die.

285
00:52:14,880 --> 00:52:17,451
(wailing )

286
00:52:19,600 --> 00:52:21,250
Release all the slaves.

287
00:52:27,320 --> 00:52:29,322
( Sobbing )

288
00:52:40,800 --> 00:52:44,566
You saved my life.
Thank you so much.

289
00:52:59,800 --> 00:53:01,848
( Cheering )

290
00:53:02,000 --> 00:53:04,480
( drums playing )

291
00:53:38,680 --> 00:53:42,401
Pim: Tien, how long
will you be away?

292
00:53:42,560 --> 00:53:45,484
Are you really leaving, Tien?

293
00:53:45,640 --> 00:53:48,962
Who will play with me
if you're gone?

294
00:53:51,560 --> 00:53:56,726
I won't let you go, Tien!
Tien, don't go!

295
00:53:58,200 --> 00:54:00,202
Don't you worry.

296
00:54:02,360 --> 00:54:05,330
I'll be back soon.

297
00:54:05,480 --> 00:54:08,484
Hurry, young master.
We don't have much time.

298
00:54:13,120 --> 00:54:15,168
I have to go.

299
00:54:24,520 --> 00:54:26,284
Tien.

300
00:54:31,400 --> 00:54:33,368
( Rattles )

301
00:55:10,720 --> 00:55:13,291
Why are we in such a hurry?

302
00:55:19,440 --> 00:55:22,762
Luang Krai! Where are you taking
the young master?

303
00:55:26,000 --> 00:55:27,650
How dare you betray
the commander?

304
00:55:27,840 --> 00:55:30,286
Traitor!

305
00:55:40,240 --> 00:55:42,925
( Shouts )

306
00:55:48,920 --> 00:55:51,446
Run, young master.

307
00:56:13,120 --> 00:56:15,122
( Shouting )

308
00:56:23,720 --> 00:56:24,721
(woman sobbing )

309
00:56:24,880 --> 00:56:26,723
You!

310
00:56:26,880 --> 00:56:30,521
Let me go!

311
00:56:58,880 --> 00:57:02,930
Let me go now.
I need to see my husband!

312
00:57:11,760 --> 00:57:14,411
Don't do this to me!
Let us go!

313
00:57:28,960 --> 00:57:32,009
My husband!

314
00:57:35,880 --> 00:57:38,360
(woman wailing )

315
00:57:42,720 --> 00:57:43,960
( laughs )

316
00:57:46,240 --> 00:57:49,084
Let me go.

317
00:58:06,520 --> 00:58:08,045
( Muffled screams )

318
00:58:16,120 --> 00:58:20,091
I'll send you
to serve your master in hell.

319
00:58:22,280 --> 00:58:24,806
Rajasena.
You bastard traitor!

320
00:58:24,960 --> 00:58:26,371
Kill him.

321
00:58:31,360 --> 00:58:33,442
( Shouting )

322
00:58:42,240 --> 00:58:44,288
( laughs )

323
00:58:53,800 --> 00:58:55,609
Father...

324
00:58:58,680 --> 00:59:00,728
Get him.

325
00:59:09,640 --> 00:59:11,881
Take care of yourself,
young master.

326
00:59:23,120 --> 00:59:24,690
Shoot!

327
00:59:29,600 --> 00:59:31,682
( labored breathing )

328
00:59:49,120 --> 00:59:51,521
Get the boy.

329
01:00:25,280 --> 01:00:27,282
So, you have made up
your mind.

330
01:00:31,680 --> 01:00:34,684
The day I met you
at the crocodile pit,

331
01:00:36,840 --> 01:00:39,923
you reminded me
of someone.

332
01:00:41,000 --> 01:00:45,289
That someone was me,
when I was young.

333
01:00:46,440 --> 01:00:49,250
That day...

334
01:00:49,400 --> 01:00:52,609
your eyes were sun-parched.

335
01:00:52,760 --> 01:00:56,242
But they were shining brightly
with flames of anger.

336
01:00:57,640 --> 01:00:59,608
Even today,

337
01:00:59,800 --> 01:01:03,600
those flames are still shining
in your eyes.

338
01:01:12,600 --> 01:01:13,840
Actually,

339
01:01:14,000 --> 01:01:17,971
I was going to make you
the Outlaw King today.

340
01:01:19,360 --> 01:01:22,728
But since you have
something on your mind,

341
01:01:22,880 --> 01:01:25,565
you shall go and finish it first.

342
01:01:28,600 --> 01:01:30,568
Inn:

343
01:01:30,720 --> 01:01:33,246
and everyone
at Garuda Wing Cliff...

344
01:01:36,280 --> 01:01:38,851
will be waiting for your return.

345
01:03:02,560 --> 01:03:04,961
( Horse whinnies )

346
01:03:20,720 --> 01:03:22,927
( drums beating )

347
01:03:54,120 --> 01:03:57,283
Honorable governors,

348
01:03:57,440 --> 01:03:59,363
ministers, royal advisors,

349
01:03:59,560 --> 01:04:01,927
and commanders...

350
01:04:02,080 --> 01:04:05,641
now is an auspicious time
destined by heaven.

351
01:04:05,800 --> 01:04:11,648
As the first monarch
of the Garuda Deva dynasty,

352
01:04:13,040 --> 01:04:17,921
I shall commence the holy ritual

353
01:04:18,120 --> 01:04:20,771
to pay homage

354
01:04:20,960 --> 01:04:23,770
to the divine monument.

355
01:04:23,960 --> 01:04:27,521
It shall eternally stand

356
01:04:27,680 --> 01:04:32,720
as the symbol of power
of the Garuda Deva dynasty.

357
01:08:11,960 --> 01:08:14,281
( Drums beating faster)

358
01:11:13,280 --> 01:11:15,203
( screaming )

359
01:11:32,840 --> 01:11:34,842
( groans )

360
01:11:35,000 --> 01:11:37,571
This is for my mother.

361
01:11:52,520 --> 01:11:53,965
Guards!

362
01:11:54,160 --> 01:11:56,731
This sword is for my father,

363
01:11:56,880 --> 01:11:58,803
Lord Sihadecho.

364
01:12:02,440 --> 01:12:03,441
( Groans )

365
01:12:03,600 --> 01:12:05,284
Guards!

366
01:13:00,800 --> 01:13:02,723
( Screaming )

367
01:13:13,480 --> 01:13:15,528
( shouting )

368
01:13:39,040 --> 01:13:41,042
Father Chernang!

369
01:13:41,200 --> 01:13:43,362
I have returned.

370
01:14:23,880 --> 01:14:25,882
( Shouting )

371
01:15:41,440 --> 01:15:42,885
( <i>gasps</i> )

372
01:17:10,080 --> 01:17:12,560
( panting )

373
01:19:49,920 --> 01:19:51,809
( screams )

374
01:20:01,160 --> 01:20:03,162
( groans )

375
01:20:20,200 --> 01:20:22,089
(coughs)

376
01:20:41,720 --> 01:20:44,769
( men shouting )

377
01:20:44,920 --> 01:20:46,809
(yelling)

378
01:21:05,400 --> 01:21:07,607
( shouting )

379
01:23:01,400 --> 01:23:03,402
( panting )

380
01:23:06,560 --> 01:23:08,483
( bellows )

381
01:23:27,280 --> 01:23:30,011
( rumbling )

382
01:25:03,920 --> 01:25:06,241
( Caws )

383
01:25:30,560 --> 01:25:32,130
( screams )

384
01:25:40,560 --> 01:25:41,686
( Caws )

385
01:25:53,120 --> 01:25:54,804
( Caws )

386
01:26:29,440 --> 01:26:31,841
( bellows )

387
01:27:26,080 --> 01:27:28,731
( panting )

388
01:27:58,720 --> 01:28:01,929
My blood is not
for your sword to drink.

389
01:28:06,440 --> 01:28:09,250
You don't have to waste
your energy, my guards.

390
01:28:10,760 --> 01:28:13,411
I have someone
who has been waiting

391
01:28:13,600 --> 01:28:16,331
to kill you.

392
01:28:17,920 --> 01:28:19,524
( laughs )

393
01:28:23,120 --> 01:28:26,169
Now, the clan of Sihadecho
has been wiped out.

394
01:28:27,680 --> 01:28:30,843
The only one left is his son.

395
01:28:31,880 --> 01:28:33,962
He is standing right there.

396
01:28:36,480 --> 01:28:40,405
Now, you shall hurry to finish

397
01:28:40,560 --> 01:28:43,564
what you still owe me.

398
01:29:02,080 --> 01:29:05,163
Father.

399
01:29:08,320 --> 01:29:11,051
A king shall never go back
on his word.

400
01:29:11,240 --> 01:29:15,040
Your Majesty
must keep your promise

401
01:29:16,520 --> 01:29:20,002
and spare the lives of my people

402
01:29:22,760 --> 01:29:25,411
whether I live or die.

403
01:29:28,920 --> 01:29:32,242
The man in black in front of you

404
01:29:32,400 --> 01:29:34,402
is the one who killed your father.

405
01:29:35,800 --> 01:29:37,962
Kill him.

406
01:29:40,000 --> 01:29:41,525
Kill him...

407
01:29:41,680 --> 01:29:44,206
...if you can.

408
01:29:44,360 --> 01:29:48,365
( laughing )

409
01:30:18,800 --> 01:30:20,211
Rebel!

410
01:30:20,360 --> 01:30:21,885
Your clan deserves to be
exterminated.

411
01:30:29,160 --> 01:30:31,208
Had I known
you were one of them,

412
01:30:31,400 --> 01:30:33,402
I would have finished you off
long ago.

413
01:30:35,880 --> 01:30:37,530
( Shouting )

414
01:30:43,000 --> 01:30:46,243
I was the one who slashed
your father's throat.

415
01:30:52,680 --> 01:30:54,523
( Shouts )

416
01:31:17,360 --> 01:31:19,806
Let me repay your father's life

417
01:31:19,960 --> 01:31:22,281
with mine,

418
01:31:22,440 --> 01:31:24,761
my dear son.

419
01:32:09,680 --> 01:32:12,251
Chernang.

420
01:32:18,640 --> 01:32:20,483
Take him away.

421
01:32:23,760 --> 01:32:26,969
But don't let him die too easily.

422
01:32:28,600 --> 01:32:31,524
Torture him.

423
01:32:31,680 --> 01:32:35,401
Put him through
the kind of pain he deserves.

424
01:32:35,560 --> 01:32:37,847
Make sure he suffers

425
01:32:38,000 --> 01:32:40,651
until his last breath.

426
01:32:45,080 --> 01:32:47,208
Narrator:
Tien's life has come to an end

427
01:32:47,360 --> 01:32:49,488
because of his cursed karma.

428
01:32:49,640 --> 01:32:53,611
But if we could unite
the power of our minds,

429
01:32:53,760 --> 01:32:55,967
our strength of faith

430
01:32:56,120 --> 01:32:59,203
might be able to prolong his life

431
01:32:59,360 --> 01:33:04,526
and guide his tormented soul
back to his body.

432
01:33:04,551 --> 01:33:07,551
SubRip: HighCode

