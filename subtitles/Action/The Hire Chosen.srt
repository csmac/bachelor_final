1
00:00:05,516 --> 00:00:15,392
<font color="#3399CC">Subtitles by </font><font color="ffffff">MemoryOnSmells</font>
<font color="#3399CC">http://UKsubtitles.ru.</font>

2
00:01:33,167 --> 00:01:35,709
I'm thirsty.

3
00:01:38,710 --> 00:01:47,710
- Untranslated -

4
00:01:50,141 --> 00:01:51,141
Jacob...

5
00:01:52,999 --> 00:01:55,999
Jacob, you should sit down.
You're still in shock.

6
00:02:03,999 --> 00:02:06,417
I just killed someone.

7
00:02:08,000 --> 00:02:10,333
He would've killed you.

8
00:02:10,334 --> 00:02:14,291
And me and your brother.

9
00:02:14,292 --> 00:02:16,999
You didn't have a choice.

10
00:02:20,292 --> 00:02:22,751
I took a life.

11
00:02:40,167 --> 00:02:43,292
You didn't have a choice.

12
00:02:56,584 --> 00:02:58,999
Give me your phone.

13
00:02:59,000 --> 00:03:00,998
Why?

14
00:03:00,999 --> 00:03:02,791
Call the police.

15
00:03:02,792 --> 00:03:05,249
Jake, you sure
that's a good idea?

16
00:03:05,250 --> 00:03:07,750
That's what you do
when you kill someone.

17
00:03:09,749 --> 00:03:10,749
Screw that!

18
00:03:11,918 --> 00:03:14,998
They could
put you away for that.

19
00:03:14,999 --> 00:03:16,292
It was self defense.

20
00:03:18,834 --> 00:03:21,082
You bashed the guy's head in.

21
00:03:21,083 --> 00:03:23,501
Like, a lot.

22
00:03:26,083 --> 00:03:29,208
Police. Open up.
What the shit?

23
00:03:29,209 --> 00:03:31,999
How'd they get here so fast?
Quiet.

24
00:03:35,834 --> 00:03:39,918
Jake. Jake, wait. Wait. Jake.

25
00:03:42,999 --> 00:03:45,250
Fuck.

26
00:03:46,584 --> 00:03:48,791
What are we gonna do?

27
00:03:48,792 --> 00:03:51,667
Fire escape.
We can go out the fire escape.

28
00:03:51,709 --> 00:03:52,998
Then what?

29
00:03:52,999 --> 00:03:54,374
I don't know.

30
00:03:54,375 --> 00:03:56,291
Jake, you can't go to jail.
I won't.

31
00:03:56,292 --> 00:03:57,791
I can't end up in foster care.

32
00:03:57,792 --> 00:03:59,750
Okay, okay.

33
00:03:59,751 --> 00:04:01,998
It's gonna be fine.
It's gonna be all right.

34
00:04:01,999 --> 00:04:03,875
I'm not going anywhere.

35
00:04:03,876 --> 00:04:06,082
You're not going anywhere.

36
00:04:06,083 --> 00:04:07,584
You just gotta trust me.

37
00:04:09,626 --> 00:04:11,250
Last warning!

38
00:04:30,667 --> 00:04:32,542
Listen...

39
00:04:42,000 --> 00:04:44,500
He broke into our home.

40
00:04:44,501 --> 00:04:48,291
He put a gun
to my brother's head.

41
00:04:48,292 --> 00:04:50,375
Are you even listening?

42
00:05:06,999 --> 00:05:09,417
In case you need to reach me.

43
00:05:19,792 --> 00:05:21,918
Is that it?

44
00:05:44,209 --> 00:05:46,584
Did that just happen?

45
00:05:50,999 --> 00:05:53,998
Those were real police, right?

46
00:05:53,999 --> 00:05:57,208
Those were real EMTs.

47
00:05:57,209 --> 00:06:00,083
I work with those guys.

48
00:06:04,000 --> 00:06:07,083
Are they a part of this?

49
00:06:10,999 --> 00:06:13,042
Is everyone a part of this?

50
00:06:15,417 --> 00:06:18,708
Are you okay?
Yeah, I'm just dizzy.

51
00:06:18,709 --> 00:06:20,999
You're gonna need another shot.

52
00:06:42,834 --> 00:06:45,082
Remember that convertible
we had?

53
00:06:45,083 --> 00:06:48,500
The one we took to Yosemite.

54
00:06:48,501 --> 00:06:50,791
Top wouldn't go up.

55
00:06:50,792 --> 00:06:52,791
Driving through the park
at night.

56
00:06:52,792 --> 00:06:55,167
Freezing our asses off.

57
00:06:55,209 --> 00:06:58,833
We drove through
the entire park.

58
00:06:58,834 --> 00:07:00,708
Yeah, we had to go through
the entire thing.

59
00:07:00,709 --> 00:07:04,708
God, that ranger
getting us lost in there.

60
00:07:04,709 --> 00:07:07,042
If I find that guy,
I can kill him.

61
00:07:28,334 --> 00:07:32,459
I can't stop thinking
about his face.

62
00:07:39,292 --> 00:07:41,209
I know.

63
00:07:48,167 --> 00:07:51,083
It's just...
I know.

64
00:08:05,751 --> 00:08:07,834
Almost done.

65
00:08:18,999 --> 00:08:20,834
Look at me.

66
00:08:24,459 --> 00:08:28,998
Okay. One more shot tomorrow,
and you should be good to go.

67
00:08:28,999 --> 00:08:31,998
You know, maybe you should
just leave it here.

68
00:08:31,999 --> 00:08:34,292
You know,
you should get out of here.

69
00:08:38,792 --> 00:08:40,999
Do you have any idea what
you're getting yourself into?

70
00:08:41,000 --> 00:08:42,167
Do you?

71
00:08:48,626 --> 00:08:52,834
I really don't want
to be alone right now.

72
00:08:56,999 --> 00:08:59,166
Top dresser drawer.

73
00:08:59,167 --> 00:09:01,999
Take whatever fits.

74
00:09:04,042 --> 00:09:06,500
You can shower first
if you want.

75
00:09:06,501 --> 00:09:08,458
No, go ahead.

76
00:09:08,459 --> 00:09:11,375
I have to... I have to call work.

77
00:09:32,375 --> 00:09:34,583
Okay. I can do that.

78
00:09:34,584 --> 00:09:35,999
I'll text you after.

79
00:10:42,167 --> 00:10:43,334
Sorry.

80
00:12:30,309 --> 00:12:31,309
Be right back.

81
00:12:40,667 --> 00:12:42,708
What's going on?

82
00:12:42,709 --> 00:12:43,999
We're here, honey.

83
00:12:54,542 --> 00:12:57,626
You okay?
Yeah.

84
00:13:33,999 --> 00:13:35,999
We're home.

85
00:14:05,167 --> 00:14:08,999
Raj said the guy
had a generator, so...

86
00:14:11,542 --> 00:14:12,667
Hey, this'll work, right?

87
00:14:14,375 --> 00:14:15,542
Yeah.

88
00:14:23,078 --> 00:14:23,918
Who is he?

89
00:14:28,250 --> 00:14:31,082
It's okay now.

90
00:14:31,083 --> 00:14:34,792
Doesn't look okay.

91
00:14:34,834 --> 00:14:36,834
He won't come near me.

92
00:14:38,125 --> 00:14:40,833
He won't have to.

93
00:14:40,834 --> 00:14:42,999
You'll go back.

94
00:14:48,999 --> 00:14:52,166
My mom tried to leave,
like, a dozen times.

95
00:14:52,167 --> 00:14:53,334
I was...

96
00:14:55,999 --> 00:14:58,208
I was a teenager.

97
00:14:58,209 --> 00:15:02,708
King was like 5, 6.

98
00:15:02,709 --> 00:15:04,998
My folks would argue.

99
00:15:04,999 --> 00:15:11,249
My dad would have his drinks.

100
00:15:11,250 --> 00:15:12,999
And they'd start in.

101
00:15:14,459 --> 00:15:17,500
I always knew
when it was coming.

102
00:15:17,501 --> 00:15:22,833
I mean, they'd always argue,
but when he got that way...

103
00:15:22,834 --> 00:15:26,833
I always knew what was coming.

104
00:15:26,834 --> 00:15:28,250
So I'd grab King...

105
00:15:30,792 --> 00:15:32,626
I'd take him into the garage.

106
00:15:38,250 --> 00:15:39,751
We had this, uh...

107
00:15:41,667 --> 00:15:43,918
This old PlayStation.

108
00:15:47,209 --> 00:15:48,998
Just turn the volume up
real loud

109
00:15:48,999 --> 00:15:51,375
so he wouldn't hear
what was going on.

110
00:15:53,876 --> 00:15:55,666
You know what's funny?

111
00:15:55,667 --> 00:15:57,041
We'd be out in the garage

112
00:15:57,042 --> 00:16:00,709
just beating the shit
out of people on TV.

113
00:16:15,959 --> 00:16:20,751
Bruises would heal,
and she would come back.

114
00:16:26,918 --> 00:16:28,999
She always came back.

115
00:16:51,209 --> 00:16:52,584
King?

116
00:16:56,375 --> 00:16:58,666
You okay?

117
00:16:58,667 --> 00:16:59,999
Mm-hm.

118
00:17:13,000 --> 00:17:15,167
You sure?

119
00:17:17,751 --> 00:17:19,249
We can talk about this
if you want.

120
00:17:19,250 --> 00:17:22,459
I'm fine.
Swear to God.

121
00:17:31,999 --> 00:17:33,249
What's this?

122
00:17:33,250 --> 00:17:36,708
Avery's, aren't they?

123
00:17:36,709 --> 00:17:38,999
No, Avery drives a Ford.

124
00:17:55,667 --> 00:17:57,250
Be right back.

125
00:19:30,709 --> 00:19:31,509
You know...

126
00:19:35,934 --> 00:19:37,134
if you sell this car,

127
00:19:38,999 --> 00:19:41,501
halfway to your surgery.

128
00:19:43,999 --> 00:19:46,541
Who knows what they'll give me
after this box?

129
00:19:46,542 --> 00:19:48,999
How do you know you're
going to get anything?

130
00:19:52,375 --> 00:19:54,334
I don't.

131
00:19:55,751 --> 00:19:57,542
Just open it.

132
00:20:00,228 --> 00:20:08,228
<font color="#3399CC">Subtitles by </font><font color="ffffff">MemoryOnSmells</font>
<font color="#3399CC">http://UKsubtitles.ru.</font>

