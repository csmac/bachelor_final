{412}{483}( whistling, cheering )
{649}{713}( bell rings )
{871}{930}- Man #1 : Way to go, champ!|- Whoa.
{930}{961}Neutral corner.
{961}{1039}One, two, three, four...
{1039}{1082}five - all right,|all right, let's go!
{1082}{1122}Man #2:|You got him, champ!
{1122}{1169}Man #3:|Let's go, Eric!
{1401}{1434}( bell sounds )
{1468}{1531}Break.|Take him to his corner.
{1733}{1772}TV commentator:|The champ is really awesome.
{1772}{1834}No one has even lasted five|rounds in his last eight fights.
{1834}{1881}He's definitely|''The Eliminator.''
{1881}{1956}Watch this, kid.|He's going down this round.
{1956}{1993}( bell sounds )
{1993}{2036}Man #4:|You can do it, champ!
{2036}{2086}Let's fight.
{2491}{2542}That's it! lt's over!
{2675}{2763}Announcer: The winner in 38|seconds of the fourth round,
{2763}{2847}and still heavyweight champion|of the world...
{2847}{2936}Eric ''The Eliminator'' Sloane!
{2936}{2980}( cheering )
{3112}{3169}Hey, champ, how come|you don't use a trainer anymore?
{3169}{3218}l got my kid brother|in my corner for now.
{3218}{3278}He says he's gonna be a vet.|l say he's gonna be a lawyer
{3278}{3334}or l'm gonna kick his butt.
{3334}{3402}Now that you've beaten everyone|there is to beat, what's next?
{3402}{3462}To take on the world's best.|Show 'em who's boss.
{3462}{3509}- Reporter #2: What about Thailand?|- What about it?
{3509}{3591}l understand kickboxing was invented|there and they're the best at it.
{3591}{3625}Oh, really?
{3625}{3694}Kid, book us a flight|to Taiwan-- first.
{3694}{3746}- Bangkok.|- ( reporters laugh )
{3746}{3814}Bangkok, Taiwan, Tokyo--|hey, what's the difference?
{3814}{3860}l'm kicking ass|wherever l go.
{4523}{4567}Standing alone
{4567}{4627}l'm holding my own
{4627}{4691}l'm gonna show the world|l'm the one
{4727}{4780}l don't care what you think
{4780}{4836}l could kill you if you blink
{4836}{4910}No pretenders left|when l'm done
{4969}{5074}Doesn't matter|where l go now
{5074}{5158}You know|what they say about me
{5158}{5227}Everybody loves a winner
{5260}{5332}Everyone can see that l am
{5371}{5469}Everybody follows|the leader, baby
{5469}{5538}Cruisin' down|the streets of Siam
{5781}{5866}Everybody follows the leader
{5866}{5979}Everybody follows the man...
{6481}{6588}Doesn't matter where l go now
{6588}{6671}You know|what they say about me
{6671}{6734}Everybody loves a winner
{6774}{6834}Everyone can see that l am
{6879}{6979}Everybody follows|the leader, baby
{6979}{7046}Cruisin' down|the streets of Siam.
{7492}{7538}See ya, kid.
{7538}{7615}Just don't forget. You're fighting|their champ next week.
{7615}{7676}l know.|l'm ready, don't worry.
{7676}{7729}- Watch your wallet.|- She doesn't want my wallet.
{7729}{7787}She wants to make it|with the champ.
{7988}{8056}Eric: You sure had it easy|with Mom in Europe.
{8056}{8107}Dad never let up.
{8107}{8152}lf l wasn't the best|in every sport l played,
{8152}{8216}he'd call me a loser.
{8216}{8271}He was always on my case.
{8271}{8324}But he made me strong.
{8363}{8468}You know, Eric--|Mom, she was a special lady, too.
{8468}{8515}She wanted me to learn|three languages,
{8515}{8595}to take ballet|before l learned karate.
{8635}{8674}l really miss her.
{8713}{8771}l wish they would have been there|to see me win the championship.
{8817}{8883}Well, big brother,|they could never get along.
{8883}{8936}But, l'm glad we do.
{8936}{9003}They'd be proud.
{9003}{9096}Yeah, you're right, kid.
{9096}{9152}Kurt! Come on.
{9613}{9649}Damn it, kid!
{9649}{9703}How many times|do l have to tell you?
{9703}{9756}You don't win fights|with that ''tip-tap'' shit.
{9756}{9809}Power-- power kicks.
{9809}{9867}Now, take your gloves off,|and let's go hit the pads.
{9867}{9903}Come on.
{10207}{10243}That's power, kid.
{10335}{10373}Whoa!
{10373}{10417}Take it easy.
{10417}{10486}Gotta learn, kid.|Come on.
{10568}{10657}( crowd chatter )
{10809}{10864}Hi, welcome to Bangkok.
{10864}{10905}Thanks.
{10905}{11017}Mr. Sloane, how do you feel|about fighting our champ, Tong Po?
{11017}{11063}Piece of cake.
{11063}{11134}Reporter #3:|Do you think you're going to win?
{11635}{11673}( whistles )
{11938}{11994}Where's the goddamn ice|in this dump?
{11994}{12052}- Go get me some, huh?|- Okay.
{12097}{12150}Hey, how much time we got?
{12177}{12231}About 30 thirty minutes.
{12271}{12303}Eric...
{12303}{12363}you've never fought this style before.|lt's so different.
{12363}{12429}Hey, a punch is a punch.|A kick's a kick.
{12429}{12487}We came to fight.|Get me some ice, will you?
{12487}{12535}Let's go.
{12621}{12712}( loud banging echoes )
{14027}{14065}Eric--
{14065}{14108}Where the hell you been?|Gimme the ice.
{14293}{14333}Eric, don't do it.
{14333}{14396}l saw the look in his eyes.|This guy's crazy.
{14396}{14438}He's fucking crazy.
{14438}{14501}Good, the crowd will love it.|Just like Rocco in Atlanta.
{14501}{14557}You don't understand.
{14557}{14596}No-- you don't understand, kid.
{14596}{14642}l'm the best.
{14642}{14697}This is just another chance|for me to prove it.
{14697}{14768}So watch and learn.|lt's all in the legs.
{14768}{14818}The legs? Legs?
{14818}{14887}This guy was kicking|one of these with his fucking legs.
{14914}{14968}And the plaster|was falling down.
{14968}{15039}So what?|Can he move like me?
{15113}{15165}No. We got him.
{15224}{15288}Eric, please.|Don't do this one.
{15288}{15343}- l've got a bad feeling about this one.|- Hey, hey.
{15343}{15399}- Trust me, don't fight this guy.|- Hey.
{15399}{15445}- Trust me.|- Hey!
{15445}{15518}l brought you here to work my corner.|Now, what's your problem?
{15518}{15590}You're either with me,|or go the fuck home.
{15739}{15776}Hey...
{15804}{15847}we're family.
{15847}{15911}Let's kick some ass.|Come on.
{16037}{16079}Okay, let's go, champ.
{16124}{16208}( music and crowd chatter )
{16431}{16470}( cheering swells )
{17749}{17786}( bell sounds )
{18089}{18119}Eric!
{18154}{18208}( counting )
{18799}{18830}No!
{19039}{19090}- No!|- ( bell sounds )
{19090}{19136}( yells )
{19643}{19684}Fuckin' music's driving me crazy.
{19684}{19731}Let's forget about the music|right now.
{19731}{19796}Did you see that?|He caught me with an elbow.
{19796}{19870}That's the way they fight here.|l told you.
{19870}{19919}The motherfucker|wants to street fight...
{19919}{19970}- l'll show him.|- Let's cancel it.
{19970}{20021}- No.|- Let's cancel, please.
{20021}{20050}No!
{20166}{20194}( bell sounds )
{20451}{20493}No-oo!
{20701}{20738}( yells )
{20828}{20875}Asshole!
{20875}{20928}Asshole!|You can't do that!
{21035}{21070}Jesus Christ,|get off of me.
{21070}{21114}You know you can give Americans|a bad rap?
{21331}{21389}He'd just as soon kill you|as look at you, kid.
{21389}{21431}That's my brother!
{21459}{21494}Hey--|l'm just trying to help you.
{21494}{21532}Take us to a hospital!
{21532}{21593}There's two more fights.
{22115}{22150}Hey, where are you going?
{22195}{22251}Hey, where's|the goddamn hospital?
{22271}{22312}Hey, you hear me?
{22312}{22357}Kurt...
{22459}{22513}Don't move.
{22540}{22573}Taxi!
{22590}{22623}( screeching )
{22623}{22654}Hospital.
{22654}{22699}l want to go to a hospital.
{22770}{22814}l wanna--
{22814}{22851}hospital! Hospital!
{22979}{23016}Yeah, yeah, yeah, yeah...
{23035}{23108}Asshole would have probably|taken you to a whorehouse.
{23108}{23175}He looks bad.|Let's move him.
{23248}{23291}lt's cool.|l'll take him to a good hospital.
{23583}{23637}All right, out, out, out.|Go on, we'll party later.
{23637}{23686}Go on, both of you.|Out.
{23686}{23735}Bitches never get enough|of my stuff.
{23735}{23796}By the way, my name's Taylor.|Army, Special Forces.
{23796}{23829}Retired--|sort of.
{23829}{23887}Hang on, my man. You're riding|the fastest wheels in Bangkok.
{24387}{24434}Can l see him now?
{24434}{24490}l'm sorry. The doctor says|come back tomorrow.
{24490}{24534}Tomorrow?
{24534}{24622}Yes. We're doing all we can.|lt's going to be a long night.
{24622}{24689}You may as well|get some sleep.
{24689}{24744}Sleep?|l can't sleep.
{24744}{24801}Please try.|lt's better for you.
{24855}{24904}Better for me...
{25439}{25506}l'm sorry.|There's nothing more l can do.
{25506}{25576}Your brother is paralyzed.
{25677}{25726}What do you mean, paralyzed?
{25726}{25807}The spinal cord was damaged|around the tenth vertebrae.
{25839}{25907}l'm afraid he can never|walk again.
{25997}{26060}You're going to make him|walk again.
{26083}{26120}- You're going to--|- Easy, easy...
{26120}{26150}Yeah, okay, okay...
{26150}{26178}He's going to|make him walk again.
{26178}{26235}Take it easy,|take it easy.
{26235}{26321}- He's going to make him walk again.|- Easy, easy, easy.
{26343}{26384}He's fortunate...
{26384}{26437}even to survive.
{26465}{26563}He'll be in the hospital at least|three months before he can be moved.
{26563}{26634}l'm very sorry.|l'm very sorry.
{26659}{26704}Doctor: l'm sorry.
{26899}{26967}( sobbing )
{27259}{27303}l want him...
{27359}{27422}l want Tong Po.
{27422}{27478}Forget about it.
{27478}{27531}l'm not signing|your death warrant, too.
{27630}{27681}Just tell me where he is.
{27681}{27713}Goddamn it, no!
{27713}{27760}l don't want|any fucking part of it.
{27760}{27855}Go home. Wait your three months,|and go home where you're safe.
{27911}{27982}l don't want your blood on my hands.|l got enough already.
{28046}{28112}He's going to pay for this.|You hear me?!
{28211}{28303}l'll find him.|l don't need your help.
{28303}{28365}The only way you'll find him|is inside the ring.
{28365}{28403}And you are not good enough!
{28403}{28499}No matter where,|l'll get him.
{28499}{28535}You don't understand.
{28535}{28613}You go after him outside the ring|and we're dead-- all of us.
{28613}{28667}He's too valuable to them.
{28667}{28782}The only way is inside the ring.|And like l said...
{28782}{28849}you ain't good enough!
{28907}{28936}We'll see.
{28936}{28986}Okay?
{29023}{29063}We'll see.
{29175}{29221}We'll see.
{29571}{29646}Excuse me, sir,|we have to go.
{29706}{29765}Please, sir, please.
{30299}{30380}Sometimes it seems|so hopeless
{30380}{30407}All alone
{30546}{30588}Nowhere to turn
{30588}{30711}So far away from home
{30793}{30903}lt's up to me,|l won't be denied
{30903}{30963}lt's better to lose
{30963}{31110}Than never have tried|to fight for love
{31110}{31197}With all my heart
{31295}{31355}Fight for love
{31355}{31436}lgnite the spark
{31539}{31682}Sometimes the world feels|like it's closing in
{31778}{31950}Everything stops,|don't know where to begin.
{32433}{32480}( whistle blows )
{32503}{32556}- Hello.|- May l help you?
{32556}{32606}Yes. l want to try|Muay-Thai.
{32606}{32657}- Oh, really?|- Yes.
{32657}{32702}l want him.
{32702}{32756}l want Tong Po.|l want to fight him.
{32756}{32827}- You?|- Yes.
{32964}{33021}( laughter and jeering )
{33235}{33359}l'll gather my strength|to carry on
{33359}{33457}lt's up to me,|l've got to be strong
{33457}{33629}To fight for love|with all my heart.
{33671}{33722}What do you want?
{33722}{33786}You're really serious|about this Muay-Thai shit?
{33786}{33826}What do you think?
{33826}{33878}l think you're out|of your mind.
{33878}{33975}And if l can't stop you, l might know|somebody who will train you.
{33975}{34031}- Someone?|- Maybe.
{34057}{34092}Who?
{34092}{34172}There's only one person crazy enough|to put you in the ring with Tong Po.
{34216}{34300}Name's Xian.|Lives out in the sticks.
{34300}{34326}Will he do it?
{34326}{34399}l don't have any fuckin' idea.|That's your problem.
{34424}{34463}l just know|how to find him.
{34463}{34514}Let's go.
{34514}{34548}And what if he doesn't take you?
{34548}{34595}He will. Let's go.|Now.
{34595}{34666}Now? Fuck now.|We're going drinking now.
{34666}{34715}l don't want to drink.
{34715}{34786}lf you want me to take you tomorrow,|we go drinking tonight.
{34786}{34831}That's the deal.
{34903}{34935}Deal.
{34960}{34998}So, what's it going to be?
{34998}{35062}''The Pink Pussy,''|or ''The Kitten Candy Club,'' huh?
{35104}{35159}l don't suppose we can just|go back to the hotel?
{35159}{35232}Fuck, no! Relax.
{35232}{35314}l know a nice,|mellow place close by.
{35314}{35361}( rock music blaring )
{35498}{35535}Whoo!
{35535}{35586}Oooh, baby, baby,|do it, do it!
{35586}{35621}Oooh, hurt me,|hurt me, hurt me!
{35621}{35668}( laughing )
{35668}{35702}Come on.
{35702}{35755}This is your idea|of a mellow place?
{35755}{35826}Oh, yeah. l don't go|to raunchy clubs anymore.
{35992}{36049}You should see|how she opens a soda bottle.
{36222}{36283}Come on, sit, sit, sit.|Come on.
{36455}{36506}Bring me my usual.|What do you want to drink?
{36506}{36584}- Water.|- Bring him a Perrier.
{36706}{36779}l just couldn't see|no reason to go back.
{36779}{36852}Wasn't nothin' there for me,|so l stayed.
{36852}{36938}l'm better off here,|less of a danger to anybody.
{36938}{36987}Why?|What do you do?
{36987}{37063}Guns, grenades, mines,|armored cars, tanks...
{37063}{37152}whatever l can get my hands on.|Don't worry, you're safe with me.
{37152}{37213}l'm a survivor.
{37213}{37302}High livin', slick talkin',|fast walkin', cool drinkin' dude.
{37347}{37440}l just like to enjoy|life's many good pleasures.
{37495}{37548}Let me tell you|about Tong Po's boss.
{37548}{37590}Freddy Li...
{37590}{37671}that's one bad motherfucker.
{37671}{37746}lt's a different culture here.|A different world.
{37746}{37816}You got to come|to terms with that,
{37816}{37894}or you won't last a heartbeat.
{37937}{37978}Why are you helping me?
{38031}{38078}l had a buddy in 'Nam...
{38127}{38170}like an older brother.
{38170}{38219}He taught me everything...
{38219}{38294}how to survive|and come out alive.
{38360}{38410}Well, he needed me once.
{38447}{38498}l wasn't there.
{38498}{38549}l could have saved him.
{38588}{38636}But l froze.
{38776}{38876}Anyway, l saw a kid|wanting to help his brother.
{38876}{38923}And here we are.
{39067}{39103}Thanks.
{39220}{39307}( music playing on radio )|Don't let anybody count you out
{39307}{39411}Whenever you feel|like crying
{39411}{39495}That's when you got|to keep on trying, yeah
{39495}{39599}You got to roll|with the punches
{39599}{39731}Don't let nothin'|keep you down for long
{39731}{39796}Roll with the punches
{39796}{39890}Got to get up,|got to be strong
{39928}{39987}Roll with the punches...
{40112}{40147}Here we are, my man.
{40147}{40223}Shangri-La|meets ''Alice in Wonderland.''
{40251}{40295}Kurt: What is this?
{40295}{40349}Taylor: lt's Xian Land.
{40349}{40392}l'm telling you, man,
{40392}{40458}he's weirder than|a three-headed cat. Good luck.
{40458}{40491}You're leaving?
{40491}{40532}Yeah, l got some business|up the road.
{40532}{40570}You're welcome|to come along.
{40570}{40617}No, l'll take my chances here.
{40617}{40663}All right, l'll be back|in a few hours.
{40663}{40750}By the way,|don't you provoke him, you hear?
{40750}{40805}( laughing )
{40805}{40864}( van drives off )
{40977}{41025}''Don't provoke him''?
{41084}{41112}( yelling )
{41277}{41319}May l help you?
{41319}{41387}Yes. Let me down.
{41387}{41466}- No, l cannot.|- Why?
{41466}{41523}Don't know who you are,|what you want.
{41523}{41587}l want down.
{41587}{41654}And l want you to teach me|Muay-Thai.
{41654}{41675}Really?
{41722}{41782}But you are American.
{41782}{41800}So?
{41833}{41882}Americans have swelled heads,
{41882}{41954}especially when hanging|upside down for too long.
{41954}{42001}( chuckling )
{42001}{42043}Very funny.
{42043}{42099}Can you cut me down,|please?
{42099}{42173}Heard about American champion|last week.
{42173}{42234}Oh, very swelled head.
{42234}{42296}Lost badly in fight.
{42296}{42395}That was my brother.|And now he cannot walk.
{42550}{42600}Wait. Wait.
{42600}{42651}Yo-- hey, wait.
{42915}{43000}Revenge is a dangerous motive.
{43052}{43109}lt's also a powerful one.
{43185}{43231}Don't step there.
{43370}{43433}So why you think|l teach you Muay-Thai?
{43433}{43559}Taylor-- he said you're the only one|who might train me.
{43559}{43645}Winston Taylor?
{43645}{43676}You know him?
{43676}{43742}Yeah. Funny guy.
{43742}{43760}Crazy.
{43807}{43864}He said the same stuff|about you.
{43915}{43978}- You fight before?|- Yeah.
{43978}{44059}- You good?|- Yeah, l'm good.
{44059}{44146}- Good defense?|- Yeah.
{44283}{44390}Must train different in America.|Your defense stinks.
{44448}{44486}Teach me.
{44539}{44589}Don't know who you are.
{44589}{44630}Maybe next year.
{44630}{44699}No, l must train now.
{44699}{44743}Why?
{44743}{44806}My brother, remember?
{45091}{45159}Take this path.|Go to the village.
{45159}{45234}At Mylee's store, get groceries.
{45234}{45286}Groceries?
{45286}{45360}Grocery--|rice, flour, honey.
{45360}{45436}- What for?|- For lunch.
{45436}{45503}Catching people in rope|make me hungry.
{45503}{45543}What about training?
{45676}{45791}Never make decision|on empty stomach.
{46093}{46178}- Ki Ki.|- ( dog growls )
{47645}{47688}( playful squeals )
{48508}{48574}ls this the Mylee store?
{48574}{48650}Yes. l'm Mylee.|May l help you?
{48650}{48749}Yeah. l was sent by Mr. Xian|to get his groceries.
{48781}{48854}- Uncle Xian sent you?|- He's your uncle?
{48854}{48910}Yes, but, how do you|know him?
{48910}{48990}l asked him|to teach me Muay-Thai.
{48990}{49078}( laughs ) And Uncle Xian|is going to teach you?
{49078}{49177}My uncle does not teach|anyone anymore.
{49207}{49305}Oh, too many people|come before.
{49305}{49377}Now, he prefers to be alone.
{49411}{49466}Let me get his things.
{49507}{49554}Where are you from?
{49554}{49620}l was born in Belgium,|but now l live in Los Angeles.
{49667}{49754}Why you come to Thailand?
{49754}{49827}My brother came here|to fight Tong Po.
{49883}{49982}He got hurt.|That's why l'm here.
{50007}{50075}You're going to fight Tong Po?
{50075}{50146}Yes. That's why|l wanted to see your uncle.
{50146}{50214}Tong Po very mean man.
{50214}{50267}Very dangerous.
{50267}{50354}He run this province|for Freddy Li.
{50354}{50382}What do you mean?
{50382}{50475}We must all pay,|or there is trouble.
{50475}{50509}Get the police.
{50509}{50623}Freddy Li pay police|not to interfere.
{50623}{50685}Please, you must say nothing.
{50685}{50770}l cannot lose my store.
{50770}{50828}lt is all l have.
{50926}{50996}Please, do not interfere.
{50996}{51065}Hey, don't let these jerks|take your money.
{51065}{51119}l must pay.|lt is all right.
{51119}{51163}No, it's not.
{51190}{51233}Hey, leave her the money.
{51834}{51903}That was very stupid!
{51928}{51962}What do you mean?
{51962}{52006}l get in big trouble now.
{52006}{52054}But, l did it.|You didn't do it.
{52054}{52132}Freddy Li won't care.
{52132}{52206}And look what you did|to my store!
{52242}{52305}What am l supposed to do?|Let them take your money?
{52305}{52349}Yes!
{52349}{52418}l'm going to talk|to Uncle Xian about this.
{52492}{52536}Mylee: l don't understand it.
{52976}{53007}Did you get my things?
{53007}{53065}Yeah, l got everything.
{53065}{53121}- Good.|- ( car horn beeping )
{53121}{53172}Hey, my man,|what's happening?
{53172}{53233}You staying, or coming,|or what? What's the story?
{53233}{53276}You will stay a while.
{53276}{53331}We have many things|to discuss.
{53399}{53420}Taylor.
{53470}{53541}l mean, who wants|to stay here, anyway?
{53541}{53630}Listen, l'll be back in a few days|to pick you up to see Eric.
{53630}{53669}And...
{53669}{53758}you're gonna live to regret this.|Mark my word.
{53758}{53809}Not a chance.
{53809}{53846}Come inside.
{53846}{53871}Bye.
{54163}{54240}Change clothes.
{55876}{55914}( screeching )
{55979}{56020}Breathe.
{56020}{56104}Store here.
{56104}{56187}Breathe out when hit.
{56215}{56262}( exhales )
{56333}{56398}Breathe in.
{56398}{56490}( grunts ) Protect.
{56521}{56625}lf you can do both|at the same time...
{56625}{56674}it's good.
{57265}{57307}( exhales )
{57437}{57523}- Ready to protect?|- Uh-huh.
{57553}{57588}( grunts )
{57623}{57669}Guess not.
{57746}{57782}Kurt.
{57973}{58027}This will make run faster.
{58027}{58100}This? How?
{58127}{58174}( barking )
{58286}{58319}He did that?
{58319}{58386}Yes.|Can you believe it?
{58386}{58430}Good for him.
{58430}{58483}''Good''?
{58511}{58581}My store was a mess.
{58581}{58676}l worked for days|cleaning it up.
{58676}{58737}But Freddy Li's goons|are also a mess.
{58778}{58865}Something l want to do for you|a long time ago.
{58865}{58950}But if l do,|then l put you in danger.
{58950}{59050}American can get away|with it, so it's good.
{59050}{59155}Yes, but what if he doesn't|get away with it?
{59208}{59255}l worry for him.
{59255}{59314}He will learn my way.
{59777}{59875}- You're late for dinner.|- But l was supposed to get...
{60016}{60084}Look, l'm sorry|if l caused you any trouble.
{60120}{60188}lt's okay.|Do you like the food?
{60188}{60234}Excellent.
{60234}{60282}My niece has many talents.
{60282}{60363}Good to help training, too.|You'll see.
{60363}{60446}l'm sure.|l can't wait.
{60490}{60538}Eat your food.
{60642}{60773}Our parents divorced when Eric and l|were both in grade school.
{60773}{60893}Mom raised me in Europe,|and Dad raised Eric in America.
{60893}{60955}ln some ways|we're different.
{60955}{61040}But we're brothers.
{61040}{61115}And now it's all the family|we've got.
{61115}{61185}You are very fortunate.
{61185}{61275}At least the both of you|had the love of one parent.
{61275}{61357}l never knew mine.
{61357}{61472}My entire family was taken|when l was just an infant.
{61472}{61589}But fate has also provided me|with Uncle Xian
{61589}{61659}so l am happy.
{61687}{61761}Uncle Xian,|he's very different.
{61761}{61784}Yes, and very wise.
{61891}{62004}But if you stay with him, he will|make you the best you can be.
{62059}{62110}And you're going to help?
{62166}{62220}l used to help|my Uncle Xian.
{62251}{62334}l'm going to need|all the help l can get.
{62411}{62455}Must you fight him?
{62455}{62510}l have to.
{62654}{62707}You're so beautiful.
{63038}{63100}l have to go now.
{63115}{63173}( scooter engine starts )
{63484}{63534}What is this?
{63534}{63658}This is Stone City,|where many ancient warriors come.
{63683}{63769}While you train here, listen.
{63807}{63865}Listen to what?
{63865}{63977}Just listen.|With your mind, your heart,
{63977}{64029}your whole being.
{64061}{64176}You must learn to be faster|than any punch or kick.
{64176}{64241}That way, won't get hit.
{64737}{64828}Low kick, move legs,|okay? Okay.
{65991}{66032}( muscles straining )
{66220}{66273}( groaning )
{66439}{66484}( groaning louder )
{66858}{66915}( shuddering and groaning )
{67175}{67240}( light melody playing )
{67525}{67581}ls that enough?
{68250}{68291}( grunts )
{68879}{68930}Winner, palm tree.
{69584}{69629}( growling )
{69820}{69872}The winner, Tong Po.
{69872}{69915}( cheering )
{71530}{71590}Kick the tree.
{71817}{71863}That's it.
{71891}{71978}Take your bag|and leave my house.
{71978}{72062}What?|What's going on?
{72062}{72106}You don't want training.
{72106}{72187}Hey, you want me|to break my leg?
{72305}{72380}Your brother, remember?
{72483}{72540}You want this?
{72563}{72593}What about this?
{72939}{72977}( cracks )
{73011}{73063}( groaning and gasping )
{75697}{75744}Ready to protect?
{75744}{75781}Yeah.
{75870}{75900}( grunts )
{75991}{76030}l am okay.
{76338}{76356}Oh!
{76477}{76546}Don't step there.
{76583}{76630}Mylee: Good timing.
{76630}{76734}l go make lunch?|Then maybe we take a walk?
{76734}{76795}( speaking in Thai )
{76915}{76951}What did he say?
{76951}{77025}He said, your training|not finished today.
{77025}{77095}After lunch, he take you|to a special place.
{77095}{77149}A special place?
{77508}{77571}( R&B ballad playing )
{78225}{78277}What am l supposed|to learn here?
{78277}{78355}Learn later.|First, relax.
{78571}{78629}( gasping )
{78656}{78703}H-hot.
{79015}{79083}lt's good stuff.|What is this?
{79083}{79155}Translated, ''Kiss of Death.''
{79155}{79234}- You like?|- Oh, yeah, it's good stuff.
{79276}{79347}- You good dancer?|- Dancing?
{79347}{79417}American dancing--|disco.
{79417}{79485}Oh, yeah, dancing.|l-l'm good, why?
{79485}{79579}l'd like to see you dance.|Observe your balance.
{79703}{79742}Here? Really?
{79803}{79873}( disco-style music plays )
{80009}{80070}Ah-ah-ha-ha
{80136}{80186}lt's all right
{80593}{80690}l'm feeling so good today
{80690}{80788}Ain't nobody|standin' in my way
{80788}{80892}Tomorrow,|gonna be a price to pay
{80892}{80964}l'm feeling so good today
{81001}{81083}There's a mountain|that l have to climb
{81083}{81183}lt's so high,|and l'm so behind
{81183}{81233}Now, get on up
{81233}{81285}Move on out
{81285}{81324}Ain't no place
{81324}{81383}For having doubt
{81383}{81477}l'm feeling|so good today
{81477}{81582}Ain't nobody|standing in my way
{81582}{81686}Tomorrow, gonna be|a price to pay
{81686}{81766}l'm feeling so good today
{81800}{81886}There's a river|that l have to cross
{81886}{81981}From this side|l got to get across
{81981}{82087}l won't bend,|l won't break
{82087}{82176}Right now,|l'm gonna celebrate
{82176}{82276}l'm feeling|so good today
{82276}{82374}Ain't nobody|standing in my way
{82374}{82466}Tomorrow, gonna be|a price to pay
{82466}{82551}l'm feeling|so good to--
{82551}{82623}( record skips )|So good today
{82623}{82713}Ain't nobody|standing in my way
{82713}{82797}Tomorrow, gonna be|a price to pay
{82797}{82904}And l'm feeling|so good today
{82904}{82957}- Xian!|-  There's a mountain
{82957}{83010}That l have to climb
{83010}{83110}lt's so high,|and l'm so behind
{83110}{83155}-  Got to get on up|- Xian!
{83155}{83210}And move on out
{83210}{83307}Ain't no place|for having doubt...
{83307}{83391}- ( music fades out )|- Xian?
{83391}{83471}Brother lose very badly|to Tong Po.
{83471}{83521}But this brother different.
{83521}{83605}You see how he embarrassed|your young fighters inside?
{83605}{83673}Why not set up fight|with your good fighters?
{83673}{83740}You will not be disappointed.
{83740}{83765}Hmm.
{83830}{83883}Xian?
{83925}{84016}What happened?|Why those guys try to hurt me?
{84016}{84070}- Did they succeed?|- No.
{84070}{84111}Good.
{84111}{84208}Why did you leave, and why|were they so pissed at me?
{84208}{84292}Because l tell them,|you say they no good fighters.
{84292}{84359}And that their mothers|have sex with mules.
{84359}{84396}You what?
{84396}{84467}Ohh, make them very angry.|Fight hard.
{84467}{84509}Good training.
{84561}{84662}And now, you drive me|in the pedicab.
{84801}{84832}Go.
{84961}{85008}He looks pretty mean to me.
{85092}{85130}And tough.
{85130}{85206}Thanks, Taylor.|What about Xian?
{85206}{85258}What does he have|to tell me?
{85258}{85320}( speaking in Thai )
{85320}{85376}Roughly translated...
{85376}{85435}''Kick the motherfucker's ass.''
{85435}{85526}- ( bell sounds )|- ( cheering )
{86773}{86814}Yes!
{87081}{87135}( softly )|Nuck Soo Kow.
{87172}{87238}( louder )|Nuck Soo Kow.
{87238}{87284}Nuck Soo Kow.
{87284}{87325}Crowd:|Nuck Soo Kow.
{87325}{87389}( chanting )|Nuck Soo Kow...
{87389}{87510}Nuck Soo Kow, Nuck Soo Kow...
{87510}{87550}( crowd chanting )|Nuck Soo Kow...
{87550}{87630}Nuck Soo Kow, Nuck Soo Kow...
{87630}{87731}( chanting continues )
{87902}{87950}What are they saying?
{87950}{88028}Nuck Soo Kow-- ''White Warrior.''
{88201}{88252}l want Tong Po.
{88303}{88354}Give me Tong Po!
{89123}{89186}( Mylee giggling )
{89263}{89323}Hey, Eric!
{89323}{89356}They let you out early?
{89356}{89430}l told them they'd be sorry|if they didn't. l'd raise hell.
{89501}{89570}Congratulations, l heard|about your fight last night.
{89570}{89615}l wish you were there.
{89615}{89692}They call me Nuck Soo Kow,|''The White Warrior.''
{89692}{89735}- Really?|- Yeah.
{89735}{89800}Let's talk more about that later.|Right now, l'm starved.
{89800}{89843}You got any breakfast|out here in the boonies?
{89843}{89926}Are you kidding? The best.|l'll go make some right now.
{89926}{89970}Wait, wait. l'll help.|l ain't cooked in ages.
{89970}{90034}l'll make my specialty.|''Jungle Survivor Quiche.''
{90113}{90152}Mylee:|What's ''Jungle Survivor Quiche''?
{90152}{90194}Taylor: lt's cobra heads.
{90194}{90234}Morning, Xian.
{90234}{90274}See you inside.
{91115}{91175}Good morning, Eric.
{91175}{91193}Hi.
{91225}{91264}ls Kurt around?
{91264}{91362}He's training on his own.|Be back later.
{91479}{91544}( metallic clanging,|flutes playing )
{91684}{91734}( grunting )
{92359}{92418}( metallic clanging,|flutes playing )
{92533}{92633}( sounds of combat )
{92971}{93046}( thunderclap )
{94091}{94127}( gasps )
{94438}{94489}( thunderclap )
{95279}{95361}So, Ki Ki, what do you think?|l'm ready, or what?
{95361}{95408}( barks )
{95438}{95507}Yes. Thanks.|Thanks a lot.
{95507}{95543}- Kurt?|- Huh?
{95543}{95574}Let's talk.
{95574}{95655}One day, that dog|is going to believe in me.
{95655}{95703}What's up?
{95730}{95787}l don't want you|to fight him.
{95787}{95824}What?
{95824}{95872}l don't want you|to fight Tong Po.
{95872}{95918}You know what l'm saying.
{95918}{96000}Eric, he put you|in that wheelchair for life.
{96000}{96082}That's right. And l don't want you|in one alongside me.
{96082}{96146}Give it up.|Let's go home.
{96178}{96219}l should have listened|to you.
{96269}{96322}And now it's your turn|to listen to me.
{96322}{96391}No, look at me.
{96391}{96458}- l can beat him.|- That's what l thought.
{96458}{96550}l trained Muay-Thai with the old man.|lt's different.
{96550}{96591}Tong Po is a killer, Kurt.
{96591}{96615}Wake up.
{96658}{96714}He's not gonna let you|out of that ring alive.
{96739}{96782}Eric...
{96782}{96865}l'm in the best shape|of my life.
{96865}{96960}l'm a better fighter|than l ever was before.
{96960}{97015}Than you ever were.
{97058}{97104}l can beat Tong Po.
{97104}{97227}l'll win that fight|for you and me.
{97227}{97298}Kurt?|Kurt, come quick!
{97331}{97374}Let's go.
{97554}{97599}What's going on?
{97599}{97648}A message from Freddy Li.
{97648}{97696}Why is he dressed like that?
{97696}{97761}He's wearing the traditional|robes of a messenger
{97761}{97831}sent to bring news of a fight.
{97831}{97888}The ancient way.
{97888}{97944}The ancient way?
{98161}{98214}( speaking Thai )
{98295}{98379}He says you will fight|the old ways.
{98379}{98526}Hands wrapped in hemp|and resin, dipped in broken glass.
{98526}{98562}Bullshit!
{98998}{99046}Kurt.
{99046}{99104}l have to go.
{99104}{99140}Wait a goddamn minute!
{99140}{99201}You're not going to fight|that asshole like that!
{99201}{99246}Kurt!|Listen to me!
{99246}{99322}l'll kick your ass|all over the place!
{99322}{99371}Kurt!|Listen to me!
{99371}{99402}Damn!
{99815}{99867}Xian's voice:|This is Stone City...
{99867}{99963}where many|ancient warriors come.
{99963}{100018}While you train here...
{100018}{100050}listen.
{100815}{100857}Tao Liu.
{100857}{100949}Master warlord, l come|with good news and request.
{100949}{100990}Speak.
{100990}{101108}l have arranged a fight between|Tong Po and American challenger.
{101108}{101153}( laughing )
{101203}{101266}He has been trained|by Xian Chow.
{101298}{101352}Xian Chow?
{101352}{101397}The American will not win.
{101397}{101530}l promise you.|They will fight the ancient way...
{101530}{101634}in the old underground tomb.
{101634}{101678}What is your request?
{101678}{101813}l want to borrow $1 million|to bet on Tong Po.
{101899}{101986}l trust you will not fail me.
{102024}{102082}lt's all being arranged.
{102134}{102194}( struggling )
{102465}{102483}No!
{102628}{102646}No!
{102699}{102771}( whimpering )
{102937}{102980}Ohh!
{103115}{103176}( sobbing )
{103176}{103212}- ( ripping )|- No!
{103212}{103266}( chatter )
{103891}{103930}Tong Po.
{103930}{103993}( sobbing )
{104522}{104586}( hawk screeches )
{105139}{105187}( screeches )
{106070}{106115}( growling )
{106155}{106195}Get 'em, Ki Ki!
{106446}{106486}( whimper )
{106729}{106782}Eric, where are ya, buddy?|We gotta get goin'.
{106782}{106850}l got held up|on some business, and--
{107465}{107523}What the hell happened?
{107523}{107560}Freddy Li.
{107560}{107611}- You were here?|- No.
{107611}{107663}Do you think l would let them|take him if l was?
{107663}{107742}You've done it before.
{107818}{107894}Taylor, Ki Ki's still alive.|Take us to town.
{107894}{107955}To the doctor.
{107955}{108010}Please.
{108069}{108120}l'll get the van.
{108120}{108207}They want to make sure|you not win.
{108350}{108404}- You can find him.|- lf you're smart...
{108404}{108470}you'll do whatever they tell you|to do or they'll kill both of ya.
{108470}{108538}Come on, Winston,|help me.
{108571}{108659}l ain't crossing Freddy Li.
{108715}{108806}Okay, go.|Be a chickenshit all your life.
{108806}{108863}Like l said before...
{108863}{108926}l don't need your help.
{109291}{109358}- You must help Kurt.|- l'm out of it.
{109358}{109407}Listen to me.
{109441}{109485}Tong Po...
{109544}{109592}Tong Po raped me.
{109671}{109732}But l did not tell Kurt.
{109732}{109820}His mind must be clear|for the fight.
{109820}{109891}So, you see...
{109891}{109964}you must help him find Eric.
{109964}{110026}( engine starts )
{110457}{110515}( rattling )
{110591}{110649}( chatter )
{110992}{111059}Man: Tong Po! Tong Po!
{111059}{111170}( crowd chanting )|Tong Po! Tong Po!
{112582}{112626}( coughs )
{113286}{113330}Xian?
{113355}{113406}Where is he?
{114431}{114503}( crowd cheers )
{114937}{114978}Hey...
{114978}{115020}you want brother alive?
{115020}{115073}My brother--
{115106}{115171}Make crowd happy.
{115171}{115283}Let Tong Po punish you|all the rounds.
{115315}{115395}You must last to the end,
{115395}{115482}or brother die very slowly.
{115482}{115600}You understand, huh?
{115600}{115665}Huh?|( laughing )
{116177}{116214}( bangs )
{116803}{116844}( crowd cheers )
{119004}{119073}- ( gongs )|- ( applause )
{119528}{119580}( grunting )
{120591}{120674}( cheering and chanting )
{121349}{121383}( tires screeching )
{121987}{122023}( cracking )
{122141}{122199}( gongs )
{122199}{122245}( applause )
{122734}{122784}Go on-- kill him!
{122827}{122867}( automatic gunfire )
{123735}{123814}l was waiting for you, Winston.|Why so late?
{123814}{123860}Sorry it took so long.
{123860}{123934}Traffic was a mess.|There's a big fight in town.
{124194}{124242}( screaming )
{124295}{124355}Thanks.|Now, we're even.
{124355}{124410}Now, Ki Ki even, too.
{124806}{124857}( groaning )
{126478}{126529}( gongs )
{126605}{126658}You bleed like Mylee.
{126705}{126780}Mylee... good fuck.
{126780}{126818}No-oo!
{127166}{127221}Tell me.
{127221}{127302}Tell me the truth.
{127302}{127348}Tell me!
{127348}{127397}( laughing )
{127459}{127532}Kurt... Kurt...
{127532}{127587}( bangs )
{127587}{127648}( sobbing )|l love you.
{128105}{128151}( whistling )
{128227}{128311}Nuck Soo Kow!|Nuck Soo Kow!
{128311}{128346}Nuck Soo Kow!
{128346}{128438}( crowd chanting )|Nuck Soo Kow! Nuck Soo Kow!
{128608}{128677}Nuck Soo Kow!|Get 'em, Kurt!
{128730}{128778}( screeching )
{128852}{128918}Mylee, cut these ropes.
{128918}{128980}- What?|- Just cut them.
{128980}{129023}( fabric tearing )
{129183}{129240}- He's coming.|- Don't worry.
{129395}{129434}Yeah!
{130047}{130090}Yeah!
{131370}{131411}Ah-ha, ah-ha, ah-ha.
{131436}{131498}Come on, get up!
{131498}{131550}Get him!
{131759}{131794}( howling )
{132268}{132311}Yes!
{132815}{132856}Yeah!
{133662}{133709}( howls )
{133738}{133783}Yes.
{134027}{134087}{Y:i}Never surrender!
{134190}{134234}Taylor: That's the way, baby.
{134310}{134359}Good fight, my man.
{134359}{134461}- Yeah, all right!|- Eric: Yeah!
{134507}{134599}{Y:i}You were born a fighter
{134599}{134678}{Y:i}ln the blood, a mighty warrior
{134678}{134771}{Y:i}Driven by desire
{134771}{134853}{Y:i}Glory calls,|it's waiting for you
{134853}{134981}{Y:i}When they try|to break you down, you can take it
{134981}{135041}{Y:i}But that don't shake you
{135041}{135111}{Y:i}When your back's|against the wall
{135111}{135187}{Y:i}The thrill of the fight's|got you standing tall
{135187}{135242}{Y:i}Never surrender
{135242}{135297}{Y:i}Never say die
{135297}{135362}{Y:i}You've got the heart|of a hero
{135362}{135420}{Y:i}Never surrender
{135420}{135473}{Y:i}The will to survive
{135473}{135557}{Y:i}You're standing strong|in the eye of the storm
{135557}{135622}{Y:i}But something keeps|pushing you on
{135622}{135682}{Y:i}To never surrender
{135718}{135764}{Y:i}Never surrender
{135837}{135921}{Y:i}There's a burning passion
{135921}{136013}{Y:i}Deep inside,|a silent power
{136013}{136101}{Y:i}With a quick reaction
{136101}{136190}{Y:i}Lightning strikes,|you're fighting for honor
{136190}{136309}{Y:i}Winners have a price to pay,|you can taste it
{136309}{136368}{Y:i}But that don't change it
{136368}{136438}{Y:i}You're not there|to take the fall
{136438}{136511}{Y:i}You fight till the end|and you take it all
{136511}{136574}{Y:i}Never surrender
{136574}{136621}{Y:i}Never say die
{136621}{136687}{Y:i}You've got the heart|of a hero
{136687}{136747}{Y:i}Never surrender
{136747}{136794}{Y:i}Keep it alive
{136794}{136877}{Y:i}You're standing strong|in the eye of the storm
{136877}{136948}{Y:i}But something keeps|pushing you on
{136948}{137010}{Y:i}To never surrender
{137042}{137104}{Y:i}Never surrender
{137153}{137229}{Y:i}You'll never stop|till you're number one
{137229}{137319}{Y:i}lt's only a matter of time
{137319}{137408}{Y:i}You'll never give up,|you'll never run
{137408}{137508}{Y:i}You're layin' your life|on the line
{137508}{137613}{Y:i}-  Never surrender|-  Never surrender
{137613}{137704}{Y:i}Never say die
{137704}{137786}{Y:i}Don't stop,|never surrender
{137786}{137859}{Y:i}The will to survive
{137859}{137950}{Y:i}-  Never surrender|-  Don't stop, never surrender
{137950}{138031}{Y:i}Yeah, yeah, yeah, yeah!
{138058}{138141}{Y:i}Don't stop,|never surrender
{138141}{138234}{Y:i}Never say die
{138234}{138317}{Y:i}Don't stop,|never surrender
{138317}{138384}{Y:i}Got the will to survive
{138384}{138483}{Y:i}-  Never surrender|-  Don't stop, never surrender
{138483}{138566}{Y:i}Yeah, yeah, yeah, yeah!
{139655}{139740}{Y:i}( song fades out )
