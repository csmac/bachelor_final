645
00:00:27,308 --> 00:00:29,298
Abbot, is he doing it well?

646
00:00:32,148 --> 00:00:34,138
Depending of the mind which, in its discretion that the development

647
00:00:37,847 --> 00:00:39,837
Very self in

648
00:00:41,617 --> 00:00:43,607
Mediation through martial arts.  In his heart, 
he is beyond the need for revenge or hatred now

649
00:00:47,056 --> 00:00:49,046
I don't know martial arts,
I don't understand these things

650
00:00:50,456 --> 00:00:52,456
Martial arts is meditation, cooking is also meditation

651
00:00:55,136 --> 00:00:57,065
The theory is roughly the same

652
00:00:58,565 --> 00:01:00,555
WuDao, people need a lot of experiences and practice

653
00:01:04,375 --> 00:01:06,364
I've said it many times

654
00:01:06,574 --> 00:01:08,564
Leave Shaolin, and go see the world

655
00:01:10,374 --> 00:01:12,374
All I know is how to cook

656
00:01:12,684 --> 00:01:13,904
Out there, I'm of no use

657
00:01:14,414 --> 00:01:16,403
I'm not going

658
00:01:16,783 --> 00:01:17,473
A piece of gold, a pile of dirt

659
00:01:19,283 --> 00:01:21,273
Which one is useful?

660
00:01:21,623 --> 00:01:22,983
I pick the piece of gold

661
00:01:23,323 --> 00:01:24,413
And if I gave you a seed?

662
00:01:25,992 --> 00:01:27,982
Don't sell yourself short

663
00:01:30,092 --> 00:01:32,092
Everyone is useful in their own way

664
00:02:07,098 --> 00:02:09,088
CaoMan, you want us to help the foreigners build the railway

665
00:02:11,498 --> 00:02:13,487
But don't forget, Mang Mountain 
is the burial plot for our ancestors

666
00:02:15,667 --> 00:02:17,657
Disturbing the dirt will destroy the fengshui

667
00:02:19,407 --> 00:02:21,397
I will not agree

668
00:02:24,006 --> 00:02:26,006
If you are really going to do it, I have nothing to say

669
00:02:27,416 --> 00:02:29,406
I'm just worried that the foreigners are playing us

670
00:02:30,346 --> 00:02:32,346
and making deals under the table

671
00:02:36,685 --> 00:02:38,685
CaoMan, your betrayal of Hou,

672
00:02:41,695 --> 00:02:43,684
We don't care

673
00:02:43,994 --> 00:02:45,084
But he previously said that 
building the railway is just a cover

674
00:02:46,194 --> 00:02:48,184
With the intention to take our land

675
00:02:48,804 --> 00:02:50,794
OK, enough talking

676
00:02:51,304 --> 00:02:53,293
HeNan province's interests are not for one man to decide

677
00:02:54,803 --> 00:02:56,793
But looking at CaoMan's attitude, it seems he's already
made an agreement with the foreigners

678
00:02:59,013 --> 00:03:01,003
There was no intention to hear us out

679
00:03:03,412 --> 00:03:05,402
Leave

680
00:03:33,769 --> 00:03:35,769
Listen well.  Going against me

681
00:03:38,449 --> 00:03:40,438
has no advantage

682
00:03:41,348 --> 00:03:43,338
They are fortunate

683
00:03:43,678 --> 00:03:45,678
But their family members

684
00:03:46,048 --> 00:03:48,038
Will die much more horrible deaths

685
00:03:48,558 --> 00:03:50,547
Being my friend or my enemy

686
00:03:52,757 --> 00:03:54,747
is for you to decide

687
00:03:59,926 --> 00:04:01,916
Have a think,

688
00:04:02,296 --> 00:04:03,426
If there is money to be made, no--one loses out

689
00:04:15,215 --> 00:04:17,205
How come he still hasn't returned?

690
00:04:22,554 --> 00:04:24,544
Auntie, stop crying, OK?

691
00:04:28,224 --> 00:04:30,213
Crying won�t bring him back any sooner

692
00:04:30,953 --> 00:04:32,953
What's happened?

693
00:04:33,323 --> 00:04:34,883
Our DaNiu has been missing for over 20 days now

694
00:04:38,163 --> 00:04:40,152
What should I do?

695
00:04:40,862 --> 00:04:42,862
CaoMen builds the railway

696
00:04:43,232 --> 00:04:44,532
We all rely on him

697
00:04:54,381 --> 00:04:56,371
Work quickly

698
00:04:57,011 --> 00:04:59,010
You hear?

699
00:05:00,320 --> 00:05:02,310
Quickly, quickly

700
00:05:32,647 --> 00:05:34,637
Who?

701
00:05:34,817 --> 00:05:36,807
What are you doing?

702
00:05:39,016 --> 00:05:41,006
Commander Hou

703
00:05:41,526 --> 00:05:43,516
Why did you kill this many people?

704
00:05:43,926 --> 00:05:45,916
It was Commander Cao's orders

705
00:05:47,355 --> 00:05:49,355
Leave

706
00:05:49,765 --> 00:05:51,755
Leave

707
00:05:52,135 --> 00:05:53,655
Hou is alive, Hou is alive

708
00:06:06,573 --> 00:06:08,563
Leave

709
00:06:13,413 --> 00:06:15,403
CaoMan said build the railway

710
00:06:15,782 --> 00:06:17,772
Who knew he was really digging for artefacts for the foreigners

711
00:06:19,092 --> 00:06:21,082
Every one we found, we gave to them

712
00:06:22,992 --> 00:06:24,982
In exchange for rifles and cannons

713
00:06:25,731 --> 00:06:27,721
He was worried it might leak it out, 
so he killed all the witnesses

714
00:06:43,140 --> 00:06:45,129
CaoMen still has many workers locked up

715
00:06:45,509 --> 00:06:46,999
After work tomorrow, they're all going to be killed

716
00:06:52,119 --> 00:06:54,109
I'm sorry

717
00:06:55,618 --> 00:06:57,558
Why don't you go home?

718
00:06:57,858 --> 00:06:59,258
Quick, we're going

719
00:07:47,903 --> 00:07:49,893
A Niu, A Niu ...

720
00:07:52,803 --> 00:07:54,792
Go have a look

721
00:07:56,042 --> 00:07:57,972
What is this?

722
00:08:41,178 --> 00:08:43,177
???

723
00:08:45,317 --> 00:08:47,307
???

724
00:08:51,696 --> 00:08:53,686
???

725
00:08:54,926 --> 00:08:56,916
???

726
00:08:59,536 --> 00:09:01,525
???

727
00:09:04,065 --> 00:09:06,065
Enough

728
00:09:07,435 --> 00:09:09,435
???

729
00:09:12,644 --> 00:09:14,634
ShenNan

730
00:09:14,944 --> 00:09:15,774
The use of any specific misconduct

731
00:09:16,684 --> 00:09:18,674
How can my daughter be dead?

732
00:09:20,253 --> 00:09:22,243
Purity more numerous data

733
00:09:30,862 --> 00:09:32,852
Learning to let go of everything

734
00:09:37,302 --> 00:09:39,292
Will you understand how to face things

735
00:09:40,771 --> 00:09:42,761
From today, the third refuge in self--protection

736
00:09:44,811 --> 00:09:46,801
???

737
00:09:47,411 --> 00:09:49,400
???

738
00:09:50,080 --> 00:09:52,070
???

739
00:09:54,010 --> 00:09:56,010
From now, your Buddhist name is JingJue

740
00:10:00,219 --> 00:10:02,209
amitabha!

741
00:10:03,759 --> 00:10:05,749
amitabha!

742
00:10:39,768 --> 00:10:41,758
Brothers... Brothers..

743
00:11:01,686 --> 00:11:03,676
It's CaoMan; I'm going to stop him

744
00:11:04,456 --> 00:11:06,446
Come back

745
00:11:06,786 --> 00:11:07,985
Sit

746
00:11:08,355 --> 00:11:09,685
Be cool minded

747
00:11:11,955 --> 00:11:13,945
He's come at a good time

748
00:11:15,465 --> 00:11:17,454
In a while, I'll delay him

749
00:11:18,194 --> 00:11:20,184
After sunset, go to Cao Fort

750
00:11:21,564 --> 00:11:23,554
Rescue them

751
00:11:23,904 --> 00:11:24,764
You go

752
00:11:25,104 --> 00:11:26,233
It's suicide either way

753
00:11:26,303 --> 00:11:28,293
If I can exchange my life to save that many

754
00:11:29,703 --> 00:11:31,703
So what if I die

755
00:11:32,813 --> 00:11:34,803
JingJue, you must be careful

756
00:11:59,930 --> 00:12:01,930
Halt

757
00:12:23,258 --> 00:12:25,247
Don't move

758
00:12:31,727 --> 00:12:33,717
amitabha!

759
00:12:51,845 --> 00:12:53,834
Please put down your weapons

760
00:12:54,754 --> 00:12:56,744
Also create extreme evil, all the evil results

761
00:13:10,133 --> 00:13:12,123
That was from my daughter

762
00:13:12,833 --> 00:13:14,822
amitabha!

763
00:13:15,132 --> 00:13:17,122
Still my brother indeed

764
00:13:18,572 --> 00:13:20,562
I knew you wouldn't die that easily

765
00:13:23,041 --> 00:13:25,031
Brother, I think of you terribly

766
00:13:28,151 --> 00:13:30,141
Thinking of me?

767
00:13:30,881 --> 00:13:32,870
If I don't die, you can't sleep, right?

768
00:13:33,380 --> 00:13:35,380
Don't be ridiculous

769
00:13:35,890 --> 00:13:37,880
Nothing of the sort

770
00:13:40,860 --> 00:13:42,849
Don't pretend

771
00:13:43,589 --> 00:13:45,589
You're afraid I will take you down

772
00:13:58,308 --> 00:14:00,298
JingJue's actions are to buy us time

773
00:14:01,308 --> 00:14:03,297
So we can save lives

774
00:14:03,677 --> 00:14:04,667
Your impulsiveness will ruin the plan

775
00:14:05,017 --> 00:14:06,207
Understand?

776
00:14:17,086 --> 00:14:19,086
You taught me

777
00:14:19,196 --> 00:14:21,185
When you're at an advantage, and you stay your hand
The person who dies is you

778
00:14:25,135 --> 00:14:27,065
I remember

779
00:14:32,934 --> 00:14:34,934
Tell your troops to back down, I'll go with you

780
00:14:54,762 --> 00:14:56,752
String the Abbot up

781
00:14:56,792 --> 00:14:58,782
Do anything rash, and I'll send him to heaven

782
00:14:59,762 --> 00:15:01,751
It's OK, all of you backing off

783
00:15:13,370 --> 00:15:15,370
Do it

784
00:15:31,127 --> 00:15:32,117
True beauty

785
00:15:34,158 --> 00:15:35,148
You must deliver it before sunrise to ZhongZhou province

786
00:15:37,367 --> 00:15:38,347
Relax

787
00:15:38,567 --> 00:15:39,557
With Cao's troops, no one will dare block our way

788
00:15:43,037 --> 00:15:45,027
Careful

789
00:15:46,636 --> 00:15:48,626
Move it, move it

790
00:16:07,254 --> 00:16:09,244
Get down, you

791
00:16:11,264 --> 00:16:13,254
Quickly

792
00:16:13,664 --> 00:16:15,653
Hurry

793
00:16:21,603 --> 00:16:23,593
Brother, this is better than before right?

794
00:16:27,202 --> 00:16:29,202
Bringing me here

795
00:16:30,242 --> 00:16:32,232
wasn't just for me to praise you, was it?

796
00:16:34,411 --> 00:16:36,401
Of course not

797
00:16:36,451 --> 00:16:38,441
I was wrong.  Please forgive me

798
00:16:40,621 --> 00:16:42,611
I hope that Brother can once again take me under his wing

799
00:16:44,820 --> 00:16:46,810
Conquer the world, and achieve great things

800
00:16:49,430 --> 00:16:51,400
Brother, look

801
00:16:51,430 --> 00:16:53,419
This was all for you

802
00:16:54,129 --> 00:16:56,119
This royal seat is yours

803
00:16:56,469 --> 00:16:57,869
These imperial robes are also yours

804
00:17:05,538 --> 00:17:07,528
Brother

805
00:17:08,548 --> 00:17:10,538
Is money and power that important to you?

806
00:17:16,887 --> 00:17:18,877
Listen

807
00:17:19,387 --> 00:17:21,377
Don't be like me

808
00:17:26,026 --> 00:17:28,016
False compassion

809
00:17:29,726 --> 00:17:31,726
I was messing with you

810
00:17:34,305 --> 00:17:36,295
Let see how you play

811
00:17:36,905 --> 00:17:38,895
I have something I want to give to you

812
00:17:39,205 --> 00:17:40,295
Bring her out

813
00:18:16,441 --> 00:18:18,411
I found your wife

814
00:18:18,441 --> 00:18:20,431
And kept her alive so you could be reunited

815
00:19:18,894 --> 00:19:20,884
You've suffered

816
00:19:25,304 --> 00:19:27,294
I used to think I was very impressive

817
00:19:29,043 --> 00:19:31,033
and able to care for you

818
00:19:33,243 --> 00:19:35,233
But I ended up doing you harm

819
00:19:56,661 --> 00:19:58,660
Ready

820
00:20:00,570 --> 00:20:02,560
Don't shoot

821
00:20:04,970 --> 00:20:06,959
Steady!!

822
00:20:10,409 --> 00:20:12,399
Fire

823
00:20:14,819 --> 00:20:16,808
Fire

824
00:20:16,978 --> 00:20:18,978
Guards stay, the rest follow me

825
00:20:23,788 --> 00:20:25,778
GO

826
00:20:34,797 --> 00:20:36,786
Please help us...

827
00:20:43,136 --> 00:20:45,136
You men, go see what's happening out front

828
00:21:02,554 --> 00:21:04,544
The artefacts are being stolen, chase them

829
00:21:16,272 --> 00:21:18,262
Get off

830
00:22:01,648 --> 00:22:03,637
Hou

831
00:22:03,947 --> 00:22:04,967
Let go of her

832
00:22:05,847 --> 00:22:07,837
Let go of her

833
00:22:08,187 --> 00:22:09,047
Brother

834
00:22:09,387 --> 00:22:10,577
You're crying

835
00:22:11,357 --> 00:22:13,346
Let her go

836
00:22:13,656 --> 00:22:14,846
Even your death

837
00:22:15,556 --> 00:22:17,546
won't satisfy me

838
00:22:18,656 --> 00:22:20,646
You watched your daughter die

839
00:22:20,866 --> 00:22:22,855
Now I want you to watch your wife die

840
00:22:24,495 --> 00:22:26,485
Then I can be happy

841
00:22:33,504 --> 00:22:35,494
Let her go you animal

842
00:22:35,874 --> 00:22:37,174
Let her go

843
00:22:43,983 --> 00:22:45,973
Auntie is so beautiful.  What a shame

844
00:22:47,383 --> 00:22:49,373
You're a monk

845
00:22:59,932 --> 00:23:01,921
CaoMan, stop it

846
00:23:09,371 --> 00:23:11,360
Commander, the monks have stolen the artefacts

847
00:23:12,140 --> 00:23:14,130
and set free the workers

848
00:23:27,459 --> 00:23:29,449
You deceived me

849
00:23:30,888 --> 00:23:32,878
Shoot him!

850
00:24:02,715 --> 00:24:04,705
JingJue, go

851
00:24:08,695 --> 00:24:10,684
Go

852
00:24:46,121 --> 00:24:48,111
Go

853
00:24:54,770 --> 00:24:56,700
1st Master

854
00:25:18,117 --> 00:25:20,107
Go

855
00:25:24,027 --> 00:25:26,017
Go go

856
00:25:26,397 --> 00:25:27,886
Go

857
00:25:34,906 --> 00:25:36,895
1st Master

858
00:25:37,235 --> 00:25:38,395
go

859
00:25:38,705 --> 00:25:40,295
Run away

860
00:25:40,335 --> 00:25:42,335
Christian Brothers University

861
00:25:51,514 --> 00:25:53,504
Christian Brothers University ...

862
00:25:55,354 --> 00:25:57,283
Are you not listening to my orders now?
Leave

863
00:26:03,663 --> 00:26:05,653
Let's go

864
00:26:05,863 --> 00:26:07,852
Go, now let's take her child to run away

865
00:27:53,413 --> 00:27:55,403
What are you doing?

866
00:27:55,773 --> 00:27:56,973
Go back

867
00:28:00,283 --> 00:28:02,272
Abbot

868
00:28:02,622 --> 00:28:04,082
Go back

869
00:28:10,292 --> 00:28:12,281
This

870
00:28:12,631 --> 00:28:13,181
Little monk, don't force me

871
00:28:13,491 --> 00:28:14,931
I just want you to go back inside

872
00:28:15,131 --> 00:28:17,121
I don't want to kill children

873
00:28:35,849 --> 00:28:37,839
Sir, please open your compassion

874
00:28:37,879 --> 00:28:39,869
and let us save the Abbot

875
00:28:40,249 --> 00:28:41,578
You will gain wealth and happiness

876
00:28:48,488 --> 00:28:50,487
Sir, Abbot always says

877
00:28:50,557 --> 00:28:52,547
A brave man knows when to change

878
00:28:53,167 --> 00:28:55,157
Let it be.  Go back home

879
00:28:55,667 --> 00:28:57,657
I'm sure your family is waiting for you

880
00:29:14,015 --> 00:29:16,005
What is this nonsense?

881
00:29:16,315 --> 00:29:17,215
You want to resist?  Catch them

882
00:29:35,063 --> 00:29:37,063
amitabha!

883
00:29:44,042 --> 00:29:46,032
Don't fight, I don't know Kung Fu

884
00:29:46,182 --> 00:29:48,172
amitabha!

885
00:30:04,990 --> 00:30:06,980
No more fighting

886
00:30:07,360 --> 00:30:08,059
No ... more ... fighting

887
00:30:22,808 --> 00:30:24,798
I'll fight you with my life

888
00:30:41,226 --> 00:30:43,216
Abbott

889
00:30:43,526 --> 00:30:44,556
Are you OK?

890
00:30:53,035 --> 00:30:55,025
Uncle Master, treat him like you're cooking a meal

891
00:31:12,793 --> 00:31:14,723
Uncle Master, fight him with your noodle making skill

892
00:31:41,010 --> 00:31:43,000
Brother, good!

893
00:31:43,120 --> 00:31:45,109
Abbott... Abbott..

894
00:31:45,319 --> 00:31:47,309
Are you alright?

895
00:31:48,319 --> 00:31:50,309
Let's go back

896
00:31:50,619 --> 00:31:52,019
Okay, go!

897
00:32:12,737 --> 00:32:14,676
It's them

898
00:32:19,986 --> 00:32:21,976
Hu Kwan..

899
00:32:25,985 --> 00:32:27,975
Mum

900
00:32:33,594 --> 00:32:35,584
Father

901
00:32:53,212 --> 00:32:55,202
JingNeng's sacrifice was not in vain

902
00:32:56,712 --> 00:32:58,702
You all, no need to feel guilty

903
00:33:18,600 --> 00:33:20,590
Ladies and gentlemen, please ready and pack your things

904
00:33:23,469 --> 00:33:25,459
We are leaving Shaolin

905
00:33:43,027 --> 00:33:45,017
I know you are scared

906
00:33:46,597 --> 00:33:48,587
I am not

907
00:33:49,497 --> 00:33:51,486
Loved ones gone, families lost

908
00:33:54,596 --> 00:33:56,586
Who wouldn't be scared?

909
00:34:02,005 --> 00:34:03,995
These days

910
00:34:04,905 --> 00:34:06,895
I feel just like you

911
00:34:10,045 --> 00:34:12,034
Shaolin is a place of happiness

912
00:34:13,254 --> 00:34:15,244
And it is our home

913
00:34:16,924 --> 00:34:18,914
It is possible that

914
00:34:20,024 --> 00:34:22,013
Shaolin will end today

915
00:34:23,763 --> 00:34:25,753
But the spirit of Shaolin

916
00:34:26,663 --> 00:34:28,653
Every flower, grass

917
00:34:29,663 --> 00:34:31,652
brick and plate

918
00:34:32,732 --> 00:34:34,722
will stay in our hearts

919
00:34:35,632 --> 00:34:37,632
The spirit of Shaolin

920
00:34:39,242 --> 00:34:41,231
taught me perseverance and dignity

921
00:35:38,615 --> 00:35:40,605
I have something to ask of you

922
00:35:40,915 --> 00:35:41,945
What is it?

923
00:35:44,325 --> 00:35:46,315
For you to lead them out of here

924
00:35:46,655 --> 00:35:48,214
No no no

925
00:35:48,554 --> 00:35:49,354
I've never stepped out of Shaolin

926
00:35:49,424 --> 00:35:51,414
No no no, I'm scared

927
00:35:53,034 --> 00:35:55,024
You're scared

928
00:35:55,364 --> 00:35:56,734
Look at them

929
00:35:56,834 --> 00:35:58,823
They need you

930
00:35:59,933 --> 00:36:01,923
They are more scared than you

931
00:36:05,073 --> 00:36:07,062
I, I am the leader now?

932
00:36:13,982 --> 00:36:15,972
Yes..

933
00:36:16,751 --> 00:36:18,741
Ok! I'll lead!

934
00:36:53,618 --> 00:36:55,607
Move quickly

935
00:36:55,917 --> 00:36:56,907
We must safely escort them to the mountains

936
00:36:57,917 --> 00:36:59,917
JingHai, you go first with the Abbot

937
00:37:00,157 --> 00:37:02,147
JingKong, you lead, I'll follow soon

938
00:37:02,327 --> 00:37:04,317
We go!!

939
00:37:05,666 --> 00:37:07,656
I'll give them my all

940
00:38:27,298 --> 00:38:29,288
Go quickly

941
00:38:52,795 --> 00:38:54,785
You fool, you want to destroy Shaolin?

942
00:38:55,825 --> 00:38:57,815
I'll destroy you

943
00:40:05,568 --> 00:40:07,558
CaoMan, wake up to yourself

944
00:40:10,707 --> 00:40:12,697
You must know!
What you have far exceeds what you will ever need

945
00:40:16,517 --> 00:40:18,507
Let it go.  Do not pursue this violent path

946
00:40:23,516 --> 00:40:25,516
We fight wars; it's always been like this

947
00:40:27,796 --> 00:40:29,785
Don't be like me

948
00:40:31,395 --> 00:40:33,385
Realizing my mistakes after they were done

949
00:40:38,035 --> 00:40:40,024
It's time to stop

950
00:40:40,404 --> 00:40:41,834
Can we not fight?

951
00:40:41,874 --> 00:40:43,864
What do you think?

952
00:41:55,467 --> 00:41:57,456
I lose

953
00:42:22,994 --> 00:42:24,984
They took my treasure.

954
00:42:27,963 --> 00:42:29,953
We need to cut the loses

955
00:42:31,273 --> 00:42:33,263
We cannot allow the news of what has happend to leak out

956
00:42:35,372 --> 00:42:37,362
Kill every one in there!!

957
00:42:38,212 --> 00:42:40,202
Fire!

958
00:43:23,678 --> 00:43:25,677
Shaolin Temple..

959
00:43:52,245 --> 00:43:54,234
Shaolin Temple, should not be humiliated

960
00:44:08,323 --> 00:44:10,313
My god is compassionate

961
00:44:34,050 --> 00:44:36,040
Madam, you can't leave, it's dangerous

962
00:44:36,350 --> 00:44:37,840
Abbot, my child is missing

963
00:44:38,180 --> 00:44:39,740
My child is missing

964
00:44:40,250 --> 00:44:42,239
Mum

965
00:44:57,068 --> 00:44:59,058
Brothers, Brothers

966
00:45:01,167 --> 00:45:03,157
Come on, brothers

967
00:45:05,277 --> 00:45:07,267
Brother

968
00:45:14,386 --> 00:45:16,376
Brother ... A Hai ....

969
00:45:16,586 --> 00:45:18,576
Brother

970
00:45:23,925 --> 00:45:25,915
amitabha!

971
00:45:30,094 --> 00:45:32,084
Brothers, Brothers

972
00:45:32,434 --> 00:45:33,484
Brother

973
00:45:33,834 --> 00:45:34,924
Brothers ...

974
00:45:35,834 --> 00:45:37,824
I'll stay; you take them to a safe place

975
00:45:38,104 --> 00:45:40,093
Abbot

976
00:47:06,436 --> 00:47:07,435
It's time you woke up

977
00:47:39,272 --> 00:47:41,262
Don't let go

978
00:48:16,338 --> 00:48:18,328
amitabha!

979
00:48:24,878 --> 00:48:26,867
Psychological and drug

980
00:48:28,447 --> 00:48:30,437
To repent

981
00:49:37,680 --> 00:49:39,670
Why?

981
00:54:09,180 --> 00:54:20,670
Brother, The Temple is no more..
Never mind, the Temple forever in our hearts, can't be destroyed

982
00:54:58,180 --> 00:55:03,670
Let him accompany you

983
00:55:18,680 --> 00:55:19,870
Do you really don't want to go?


984
00:55:24,680 --> 00:55:25,870
CaoMan has came

984
00:55:27,180 --> 00:55:29,470
His fault is my fault too.

986
00:55:31,680 --> 00:55:33,870
Now I want to help him.

987
00:55:37,680 --> 00:55:39,870
You are really different now.

987
00:55:44,280 --> 00:55:45,870
I know..

988
00:55:50,280 --> 00:55:52,870
We really have no chance to live together.

989
00:55:54,280 --> 00:55:55,670
But..

990
00:55:59,280 --> 00:56:02,870
I will stay here waiting for you

991
00:56:12,280 --> 00:56:15,870
<i>Subtitle by Kizoku</i>