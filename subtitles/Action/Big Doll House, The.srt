﻿1
00:00:01,250 --> 00:00:04,050
(The following content may not be suitable for viewers under 15.)

2
00:00:04,050 --> 00:00:05,950
(Viewer discretion is advised.)

3
00:00:06,595 --> 00:00:08,255
(Episode 10)

4
00:00:27,001 --> 00:00:29,212
I'll go study abroad.

5
00:00:38,047 --> 00:00:39,486
Thank you, Se Yun.

6
00:00:40,416 --> 00:00:41,486
Thank you...

7
00:00:43,956 --> 00:00:45,086
so much.

8
00:00:45,727 --> 00:00:47,357
Why are you thanking me?

9
00:00:47,857 --> 00:00:49,227
I'm the one who's thankful.

10
00:00:50,066 --> 00:00:52,797
I get to study abroad with your hard-earned money.

11
00:00:57,766 --> 00:00:58,937
I'm sorry,

12
00:01:00,177 --> 00:01:01,276
Se Yun.

13
00:01:03,177 --> 00:01:04,307
I'm sorry.

14
00:01:19,426 --> 00:01:22,456
So you do have something you want.

15
00:01:26,866 --> 00:01:27,896
Yes.

16
00:01:32,506 --> 00:01:33,736
Hong Se Yun.

17
00:01:36,247 --> 00:01:37,777
I need Hong Se Yun.

18
00:01:45,187 --> 00:01:46,357
Grandpa.

19
00:01:47,256 --> 00:01:49,087
You won't abandon me, will you?

20
00:01:49,786 --> 00:01:53,297
Why would I abandon you?

21
00:01:54,197 --> 00:01:56,866
You're my only blood relative in the entire world.

22
00:02:00,937 --> 00:02:02,667
I won't do it again.

23
00:02:02,907 --> 00:02:04,236
I won't do anything bad.

24
00:02:05,006 --> 00:02:06,637
If you let me have Se Yun,

25
00:02:06,937 --> 00:02:09,376
I can fix what I did wrong.

26
00:02:10,707 --> 00:02:12,076
So please,

27
00:02:14,777 --> 00:02:16,346
don't abandon me.

28
00:02:24,457 --> 00:02:25,687
My child.

29
00:02:46,446 --> 00:02:48,717
- Mom. - Yes?

30
00:02:58,427 --> 00:03:00,356
Why are you crying?

31
00:03:03,596 --> 00:03:05,126
When I was little,

32
00:03:06,267 --> 00:03:07,767
when it was...

33
00:03:08,267 --> 00:03:10,437
just you and me,

34
00:03:13,707 --> 00:03:16,207
I used to get scared whenever the sun set.

35
00:03:17,876 --> 00:03:20,717
I worried that you wouldn't come home from work,

36
00:03:21,946 --> 00:03:23,286
and that you wouldn't...

37
00:03:23,816 --> 00:03:26,386
pick me up from daycare.

38
00:03:29,256 --> 00:03:31,386
Do you still remember things from when you were five?

39
00:03:33,856 --> 00:03:36,566
I always told you to sleep first if I was late,

40
00:03:37,096 --> 00:03:40,397
but you always cried, holding your knees to your chest...

41
00:03:41,096 --> 00:03:43,237
whenever I was late.

42
00:03:45,666 --> 00:03:47,976
I felt like if you left me,

43
00:03:48,677 --> 00:03:50,677
the sun wouldn't rise,

44
00:03:51,046 --> 00:03:52,606
I couldn't breathe,

45
00:03:52,946 --> 00:03:55,446
and the entire world would end.

46
00:03:56,886 --> 00:03:58,416
You felt the same, right?

47
00:03:59,416 --> 00:04:01,286
Like you couldn't live without me. Right?

48
00:04:05,957 --> 00:04:07,696
But if I go study abroad,

49
00:04:08,796 --> 00:04:10,397
you'll be all alone.

50
00:04:12,096 --> 00:04:14,566
You'll take down the laundry all alone like this,

51
00:04:15,897 --> 00:04:17,767
and return to a house without me.

52
00:04:19,736 --> 00:04:21,736
I won't be able to hug you like this either.

53
00:04:25,476 --> 00:04:27,017
You and I...

54
00:04:27,616 --> 00:04:30,387
aren't the only ones in the world anymore.

55
00:04:30,846 --> 00:04:32,017
Even if...

56
00:04:32,916 --> 00:04:34,387
I send you far away,

57
00:04:34,686 --> 00:04:37,457
I can lean on your dad, Chul Soo,

58
00:04:37,527 --> 00:04:39,796
Sun Hee, and Gang Hee.

59
00:04:40,096 --> 00:04:41,657
You're the one I'm worried about.

60
00:04:42,796 --> 00:04:44,127
You'll be alone...

61
00:04:45,467 --> 00:04:47,137
in that faraway land.

62
00:04:48,967 --> 00:04:50,036
Don't worry,

63
00:04:50,967 --> 00:04:54,007
because I'll take your face,

64
00:04:54,476 --> 00:04:57,207
your scent and your voice...

65
00:04:58,046 --> 00:04:59,046
with me...

66
00:05:00,017 --> 00:05:01,676
right here.

67
00:05:03,046 --> 00:05:06,286
I can take you out whenever I miss you.

68
00:05:20,767 --> 00:05:23,907
Okay. That's right. Good.

69
00:05:24,967 --> 00:05:26,236
I'll take that.

70
00:05:26,707 --> 00:05:28,007
Thank you.

71
00:05:29,236 --> 00:05:32,246
One, two, three, four, five, six, seven...

72
00:05:32,246 --> 00:05:35,116
- Hello! - Mom!

73
00:05:35,277 --> 00:05:38,846
Dad! Mother's here! So is Se Yun!

74
00:05:42,186 --> 00:05:44,286
How did you come? You didn't even call.

75
00:05:49,157 --> 00:05:51,527
I've decided to hold off on sending Gyung Hye...

76
00:05:52,467 --> 00:05:53,796
to Hawaii.

77
00:05:56,366 --> 00:05:58,736
Mr. Chairman. She cut up clothes at a shop...

78
00:05:58,736 --> 00:06:00,377
and even physically fought with a shop employee.

79
00:06:00,536 --> 00:06:02,877
People will complain about her abusing her status.

80
00:06:03,777 --> 00:06:06,517
We must send her to Hawaii to avoid the storm...

81
00:06:07,416 --> 00:06:08,577
Mr. Lee...

82
00:06:09,817 --> 00:06:11,546
will take care of it.

83
00:06:12,246 --> 00:06:15,157
- I'll do that. - If that was possible,

84
00:06:15,786 --> 00:06:17,887
he would've prevented them from occurring in the first place.

85
00:06:19,286 --> 00:06:23,096
Mr. Lee. My wife is secretly receiving psychiatric treatment.

86
00:06:23,697 --> 00:06:25,897
Your duty is to make sure she doesn't make a sound...

87
00:06:25,897 --> 00:06:27,296
outside of the house,

88
00:06:27,837 --> 00:06:29,296
but you let things get this out of hand.

89
00:06:32,777 --> 00:06:36,277
It isn't only to avoid the rumours, sir.

90
00:06:36,606 --> 00:06:40,046
It would be of great help to her treatment to let her...

91
00:06:40,176 --> 00:06:42,176
rest peacefully in a new environment.

92
00:06:42,346 --> 00:06:45,686
Sir. You must send my wife to Hawaii.

93
00:06:45,686 --> 00:06:46,986
Mr. Jang.

94
00:06:48,616 --> 00:06:49,827
Have faith in Gyung Hye.

95
00:06:53,257 --> 00:06:54,556
Dr. Kim.

96
00:06:55,397 --> 00:06:57,627
Work harder on her treatment...

97
00:06:58,027 --> 00:07:00,067
so that she can overcome her illness herself.

98
00:07:01,166 --> 00:07:02,267
Yes, sir.

99
00:07:03,067 --> 00:07:04,606
I'll do my best.

100
00:07:08,577 --> 00:07:11,647
I'll be more attentive to her, sir.

101
00:07:31,726 --> 00:07:34,197
Average people fear you.

102
00:07:34,236 --> 00:07:35,637
You're in bad shape.

103
00:07:35,637 --> 00:07:37,236
Get it together.

104
00:07:37,236 --> 00:07:38,767
Don't sit there like that.

105
00:07:38,767 --> 00:07:40,936
You look openly insane. It's frightening.

106
00:07:46,217 --> 00:07:47,717
Apologize for what you said.

107
00:07:47,717 --> 00:07:49,176
- Let go. - Apologize!

108
00:07:49,217 --> 00:07:52,546
Let go. Let go!

109
00:07:54,786 --> 00:07:56,056
That's right.

110
00:08:10,436 --> 00:08:11,967
I'll get rid of everyone...

111
00:08:16,147 --> 00:08:17,407
coming at me.

112
00:08:19,577 --> 00:08:20,717
I'm going to live.

113
00:08:38,767 --> 00:08:40,336
Gyung Hye asked me...

114
00:08:40,397 --> 00:08:42,597
to hire Hong Se Yun as her personal shopper.

115
00:08:45,637 --> 00:08:47,436
She plans to reduce...

116
00:08:47,936 --> 00:08:50,046
the impact of the designer shop incidents.

117
00:08:51,576 --> 00:08:54,377
If Gyung Hye and Hong Se Yun...

118
00:08:55,076 --> 00:08:57,387
knew each other well enough that Gyung Hye would hire her...

119
00:08:57,387 --> 00:08:59,086
as her personal shopper,

120
00:08:59,987 --> 00:09:02,686
people wouldn't think Gyung Hye was abusing her status.

121
00:09:03,556 --> 00:09:07,257
Although they had a physical altercation,

122
00:09:07,826 --> 00:09:09,426
it will be buried as...

123
00:09:10,027 --> 00:09:11,997
a minor argument, a misunderstanding,

124
00:09:13,036 --> 00:09:14,637
and a mistake.

125
00:09:14,966 --> 00:09:17,007
Do you plan to do as Ms. Gyung Hye wants?

126
00:09:23,206 --> 00:09:25,576
Play, turtle

127
00:09:25,706 --> 00:09:28,117
You look happy when you play

128
00:09:28,277 --> 00:09:30,816
You and you

129
00:09:30,946 --> 00:09:32,987
Everyone plays

130
00:09:32,987 --> 00:09:35,286
- Good job. - You were great!

131
00:09:35,286 --> 00:09:37,556
- You did great. - Nice.

132
00:09:37,556 --> 00:09:38,727
He's better than you.

133
00:09:38,727 --> 00:09:40,997
I should get going.

134
00:09:41,597 --> 00:09:43,566
What's the rush?

135
00:09:43,727 --> 00:09:45,296
Gang Hee will be here soon.

136
00:09:45,367 --> 00:09:47,267
Wait until she comes.

137
00:09:47,767 --> 00:09:51,036
Mom. Can't you sleep at home tonight...

138
00:09:51,206 --> 00:09:52,907
and go early tomorrow morning?

139
00:09:53,036 --> 00:09:55,777
I told my boss that I'd be right back.

140
00:09:55,806 --> 00:09:57,607
What are they, night goblins?

141
00:09:57,806 --> 00:10:00,377
It's not like you need to cook or clean in the middle of the night.

142
00:10:00,617 --> 00:10:02,716
Mom, just stay over.

143
00:10:02,747 --> 00:10:05,316
Woong, you ask her.

144
00:10:05,617 --> 00:10:07,686
Don't go. Please?

145
00:10:07,857 --> 00:10:10,527
Gosh. I'll see you on the weekend.

146
00:10:10,887 --> 00:10:14,196
Why are you acting like you'll never see me again?

147
00:10:15,056 --> 00:10:17,367
I have something important to tell you...

148
00:10:17,566 --> 00:10:19,737
and the family this weekend,

149
00:10:19,767 --> 00:10:22,196
so tell Gang Hee to stay home.

150
00:10:24,066 --> 00:10:25,137
Okay.

151
00:10:25,436 --> 00:10:27,836
Mom. What do you need to tell us?

152
00:10:30,507 --> 00:10:33,676
You'll have to wait. I want to tell everyone together.

153
00:10:34,216 --> 00:10:36,446
Wait. I'll go with you and get you a cab.

154
00:10:36,546 --> 00:10:38,546
I don't need a cab. I can take the bus.

155
00:10:38,586 --> 00:10:40,786
- Stay. You'll only slow me down. - No, no.

156
00:10:40,786 --> 00:10:42,586
- I'll walk you. - See you this weekend.

157
00:10:42,586 --> 00:10:44,027
- Clean up. - Okay.

158
00:10:44,027 --> 00:10:46,456
- Get back safely, Mom! - Bye!

159
00:10:56,936 --> 00:10:59,267
I said I'd take you. You're so stubborn.

160
00:11:00,706 --> 00:11:03,347
It's still cold out.

161
00:11:04,806 --> 00:11:07,377
Ask Gang Hee to dye your hair for you.

162
00:11:07,477 --> 00:11:09,747
Do it for me when you're home this weekend.

163
00:11:10,286 --> 00:11:13,517
Gang Hee is very careless.

164
00:11:13,757 --> 00:11:16,686
She misses spots all over the place.

165
00:11:20,227 --> 00:11:23,326
I'll cut your hair too, while I'm at it.

166
00:11:28,296 --> 00:11:31,637
I really like hearing you laugh like this.

167
00:11:34,377 --> 00:11:36,147
Do you remember when Gang Hee was born?

168
00:11:37,377 --> 00:11:39,377
You lost your retirement fight...

169
00:11:39,377 --> 00:11:42,076
and ran to the hospital all banged up,

170
00:11:43,017 --> 00:11:44,686
but as you held Gang Hee,

171
00:11:44,887 --> 00:11:46,857
you smiled like you were the happiest man on earth.

172
00:11:47,786 --> 00:11:50,826
I felt like Gang Hee made you and Se Yun...

173
00:11:51,426 --> 00:11:53,597
really my family,

174
00:11:54,597 --> 00:11:56,227
so I adored her.

175
00:12:12,946 --> 00:12:16,017
I'll bring Ms. Gyung Hye's food to her room.

176
00:12:17,786 --> 00:12:19,117
I'll eat here.

177
00:12:19,347 --> 00:12:21,686
Okay. Have a seat.

178
00:12:24,286 --> 00:12:25,286
Miss.

179
00:12:27,097 --> 00:12:28,326
Water kimchi.

180
00:12:29,966 --> 00:12:31,127
Yes, Miss.

181
00:12:31,627 --> 00:12:34,237
I'd like dark chocolate for dessert.

182
00:12:34,497 --> 00:12:36,436
I hear it's good for reducing stress.

183
00:12:37,336 --> 00:12:38,536
I understand.

184
00:12:40,407 --> 00:12:41,706
You look good.

185
00:12:42,137 --> 00:12:45,407
I'll eat well, sleep well, and smile often.

186
00:12:45,907 --> 00:12:47,247
I'll keep doing that.

187
00:12:52,987 --> 00:12:55,757
The VIP ultra-luxury goods project.

188
00:12:57,657 --> 00:12:59,997
- Yes, sir. - It's not bad.

189
00:13:01,527 --> 00:13:04,326
I'll call a meeting with the executives soon.

190
00:13:04,397 --> 00:13:05,627
Prepare a presentation.

191
00:13:07,966 --> 00:13:09,066
Thank you, sir.

192
00:13:18,806 --> 00:13:22,247
You walked this path to work for 10 whole years.

193
00:13:23,786 --> 00:13:25,546
Work is hard, isn't it?

194
00:13:26,657 --> 00:13:27,657
Dad.

195
00:13:28,186 --> 00:13:30,357
You don't have to worry about what happened yesterday...

196
00:13:30,527 --> 00:13:32,186
at the shop.

197
00:13:34,796 --> 00:13:36,867
I didn't tell your mom.

198
00:13:37,666 --> 00:13:39,966
Good. If she finds out,

199
00:13:39,997 --> 00:13:42,237
it'll just be yet another person who is upset.

200
00:13:45,036 --> 00:13:47,676
Se Yun, are you really okay?

201
00:13:48,277 --> 00:13:50,007
You must've been very upset.

202
00:13:50,877 --> 00:13:53,847
Dad, I'm really okay.

203
00:13:54,117 --> 00:13:56,416
Plus, it was worth it.

204
00:13:56,847 --> 00:13:59,117
I got to have your special agujjim.

205
00:14:02,556 --> 00:14:05,027
- Go in, Dad. - Okay.

206
00:14:08,296 --> 00:14:09,296
Okay.

207
00:14:15,637 --> 00:14:17,036
6 months docked pay.

208
00:14:17,166 --> 00:14:18,507
Demotion.

209
00:14:18,607 --> 00:14:21,206
Write a formal apology...

210
00:14:21,206 --> 00:14:23,446
explaining exactly what it was...

211
00:14:23,576 --> 00:14:24,806
that you did wrong.

212
00:14:25,407 --> 00:14:27,546
Don't type it. Write it by hand.

213
00:14:27,747 --> 00:14:29,847
You're getting off easy only because you were...

214
00:14:29,887 --> 00:14:31,716
the best employee for three years.

215
00:14:32,647 --> 00:14:34,487
Most importantly,

216
00:14:34,857 --> 00:14:36,627
go to Ms. Eun Gyung Hye...

217
00:14:37,056 --> 00:14:38,686
and get her to sign your apology.

218
00:14:39,056 --> 00:14:40,657
I'm saying, get her to forgive you.

219
00:14:40,727 --> 00:14:42,556
I think Friday will do.

220
00:14:42,556 --> 00:14:44,227
Why do you need so much time?

221
00:14:44,466 --> 00:14:46,936
- Go to her today and... - By Friday,

222
00:14:47,066 --> 00:14:49,267
I'll pass on the list of clients that I handled,

223
00:14:49,296 --> 00:14:51,007
their preferences, sizes,

224
00:14:51,066 --> 00:14:54,107
favourite brands and other notables...

225
00:14:54,206 --> 00:14:56,507
to my successor.

226
00:14:56,576 --> 00:14:57,576
What...

227
00:14:58,347 --> 00:14:59,716
What did you just say?

228
00:15:01,377 --> 00:15:04,086
I said to go to the client and apologize, but what?

229
00:15:04,286 --> 00:15:05,586
Your successor?

230
00:15:05,617 --> 00:15:07,657
I'm the one who should receive an apology,

231
00:15:07,657 --> 00:15:08,887
not Eun Gyung Hye.

232
00:15:10,456 --> 00:15:11,586
So, what's this?

233
00:15:12,286 --> 00:15:14,357
Are you saying you quit?

234
00:15:14,426 --> 00:15:17,566
Yes. I'll pass on everything to my successor...

235
00:15:17,666 --> 00:15:20,036
by this Friday, my last day.

236
00:15:20,867 --> 00:15:24,306
I'll do that since this had been my bread and butter for five years.

237
00:15:25,066 --> 00:15:28,107
I hope you'll treat me with respect during my remaining days...

238
00:15:28,377 --> 00:15:29,377
as well.

239
00:15:31,277 --> 00:15:32,306
What...

240
00:15:37,716 --> 00:15:40,357
We didn't ship Gyung Hye off to Hawaii.

241
00:15:40,757 --> 00:15:42,316
Things just got more complicated.

242
00:15:43,627 --> 00:15:46,897
If there's a problem, there is always a solution.

243
00:15:48,296 --> 00:15:50,627
You need to come up with that solution quickly.

244
00:15:52,566 --> 00:15:55,397
I thought he said he'd call an executives meeting soon.

245
00:15:57,666 --> 00:16:00,107
To discuss the ultra-luxury goods project...

246
00:16:03,277 --> 00:16:05,306
If all the core executives complain,

247
00:16:05,676 --> 00:16:07,477
Chairman Eun's hands will be tied.

248
00:16:11,216 --> 00:16:12,517
Isn't that too dangerous?

249
00:16:13,487 --> 00:16:15,286
It may hurt the company.

250
00:16:17,056 --> 00:16:19,456
Chairman Eun is still strong.

251
00:16:19,956 --> 00:16:21,527
He'll block the bomb we set off...

252
00:16:21,597 --> 00:16:23,897
before the company is harmed.

253
00:16:25,296 --> 00:16:27,166
Gyung Hye's succession...

254
00:16:27,566 --> 00:16:29,436
is all that will be destroyed.

255
00:16:34,936 --> 00:16:37,877
I thought you'd withdraw her shopping deposit,

256
00:16:37,977 --> 00:16:39,877
but you doubled it instead.

257
00:16:39,977 --> 00:16:42,247
Ms. Eun Gyung Hye, our VVIP,

258
00:16:42,347 --> 00:16:44,546
is incredible magnanimous...

259
00:16:44,546 --> 00:16:46,757
- and generous... - I hope the employees...

260
00:16:47,316 --> 00:16:49,086
will not talk about what happened...

261
00:16:49,286 --> 00:16:50,657
that day.

262
00:16:51,257 --> 00:16:54,257
You do not need to worry about that at all.

263
00:16:54,397 --> 00:16:55,727
Hong Se Yun said...

264
00:16:55,757 --> 00:16:58,426
she'd resign just earlier today.

265
00:16:58,666 --> 00:17:00,637
Once she's gone from the shop,

266
00:17:00,696 --> 00:17:02,237
the incident will be forgotten.

267
00:17:02,666 --> 00:17:03,736
Don't worry.

268
00:17:03,807 --> 00:17:05,537
Hong Se Yun is resigning?

269
00:17:05,867 --> 00:17:06,936
Yes.

270
00:17:07,276 --> 00:17:10,107
So, please put in a good word...

271
00:17:10,107 --> 00:17:12,206
with Ms. Eun Gyung Hye.

272
00:17:12,377 --> 00:17:14,516
She won't have to worry about...

273
00:17:14,746 --> 00:17:16,787
such unpleasant things happening here again.

274
00:17:22,216 --> 00:17:23,226
Se Yun.

275
00:17:24,287 --> 00:17:26,256
When is your last day here?

276
00:17:28,827 --> 00:17:30,627
I heard you were quitting.

277
00:17:30,726 --> 00:17:32,696
Is it because of what happened with Ms. Gyung Hye?

278
00:17:35,797 --> 00:17:37,706
Not everything in the world happens...

279
00:17:38,166 --> 00:17:39,936
because of Eun Gyung Hye.

280
00:17:41,637 --> 00:17:42,776
If you'll excuse me...

281
00:17:52,587 --> 00:17:54,416
Do you have some time now?

282
00:17:56,087 --> 00:17:57,486
It won't take long.

283
00:17:57,827 --> 00:17:59,726
Sorry, but I have plans.

284
00:18:02,397 --> 00:18:03,397
Shin Hyuk.

285
00:18:07,097 --> 00:18:08,097
You must be hungry.

286
00:18:08,466 --> 00:18:11,407
- What should we have? - Let's have noodles.

287
00:18:26,486 --> 00:18:28,416
- Eat. - Thanks.

288
00:18:37,526 --> 00:18:38,526
Is it good?

289
00:18:40,337 --> 00:18:42,266
Did you have a workshop nearby?

290
00:18:42,637 --> 00:18:44,137
I wondered what was up...

291
00:18:44,607 --> 00:18:45,807
when you suddenly asked me to lunch.

292
00:18:47,407 --> 00:18:49,176
From what I learned at the workshop today,

293
00:18:49,647 --> 00:18:50,746
if we're lucky,

294
00:18:51,506 --> 00:18:54,476
I think we can get them to reopen Kkot Nim's case.

295
00:18:55,547 --> 00:18:57,486
That's great. Good luck.

296
00:18:58,746 --> 00:19:00,416
Don't give me lip service.

297
00:19:00,587 --> 00:19:02,357
Take me to the movies.

298
00:19:03,456 --> 00:19:06,057
- The movies? - I'll pay for lunch.

299
00:19:06,756 --> 00:19:08,597
That's not all. When we get to the theatre,

300
00:19:08,857 --> 00:19:10,996
I'll pay for the popcorn, squid, soda,

301
00:19:11,097 --> 00:19:12,367
everything.

302
00:19:15,936 --> 00:19:18,037
I'll pass on everything to my successor...

303
00:19:18,107 --> 00:19:20,506
by this Friday, my last day.

304
00:19:20,837 --> 00:19:23,307
Then let's go on Friday.

305
00:19:23,476 --> 00:19:26,176
Friday? Wouldn't Saturday or Sunday be better?

306
00:19:27,016 --> 00:19:28,347
Friday is good.

307
00:19:28,817 --> 00:19:32,117
I want to eat yummy food and watch a movie...

308
00:19:32,486 --> 00:19:33,486
that day.

309
00:19:38,986 --> 00:19:41,597
(Letter of Resignation)

310
00:19:45,827 --> 00:19:48,637
I got yelled at a lot by the higher ups.

311
00:19:48,797 --> 00:19:51,367
Look at this. I look like a panda bear.

312
00:19:52,706 --> 00:19:54,506
You caused the problem,

313
00:19:54,676 --> 00:19:56,077
but rather than fixing it,

314
00:19:56,746 --> 00:19:58,877
you're just quitting.

315
00:19:59,276 --> 00:20:01,347
You know you're being mean to me, right?

316
00:20:01,817 --> 00:20:04,887
So, don't go around badmouthing me...

317
00:20:05,016 --> 00:20:07,387
or anything, okay?

318
00:20:08,117 --> 00:20:10,426
I can't thank you for everything.

319
00:20:10,726 --> 00:20:11,756
But still,

320
00:20:13,057 --> 00:20:14,327
you worked hard.

321
00:20:20,666 --> 00:20:21,666
What is this?

322
00:20:21,936 --> 00:20:23,766
Let's go, Se Yun.

323
00:20:28,877 --> 00:20:30,107
(Letter of Resignation)

324
00:20:35,176 --> 00:20:36,347
(Letter of Resignation)

325
00:20:36,446 --> 00:20:37,516
(I quit because you treat us like garbage.)

326
00:20:38,287 --> 00:20:39,317
Why that...

327
00:20:44,887 --> 00:20:46,397
(I quit because I don't want to get cancer here.)

328
00:20:47,397 --> 00:20:50,127
Hey! Why you...

329
00:20:54,266 --> 00:20:55,367
Enjoy.

330
00:20:59,537 --> 00:21:01,077
Why are you putting on so many sesame seeds?

331
00:21:01,377 --> 00:21:03,647
Because I can do whatever I want.

332
00:21:04,307 --> 00:21:05,476
Now that I gave her...

333
00:21:05,476 --> 00:21:07,476
the letters of resignation I wrote over the past three years,

334
00:21:07,617 --> 00:21:09,686
I feel like I pooped after being unable to for a month.

335
00:21:10,246 --> 00:21:11,587
It feels so good.

336
00:21:12,317 --> 00:21:14,216
Gross. We're eating right now.

337
00:21:15,317 --> 00:21:16,327
Let's eat.

338
00:21:25,397 --> 00:21:26,637
This is good.

339
00:21:27,297 --> 00:21:28,466
It's awesome.

340
00:21:34,137 --> 00:21:35,506
- Young Ae. - Yes?

341
00:21:36,307 --> 00:21:38,377
Can you really just quit like this?

342
00:21:38,746 --> 00:21:39,877
Because of me...

343
00:21:39,946 --> 00:21:43,787
Ever since high school, you were the needle, and I, the thread.

344
00:21:44,716 --> 00:21:45,946
That's true.

345
00:21:46,756 --> 00:21:50,186
Designer goods make me sick now.

346
00:21:50,686 --> 00:21:52,256
I'm going to change my field...

347
00:21:52,387 --> 00:21:54,597
to something more for commoners and the general public.

348
00:21:55,897 --> 00:21:59,297
Somewhere where I don't have to see arrogant jerks with money.

349
00:22:03,067 --> 00:22:04,506
- Young Ae. - What?

350
00:22:06,706 --> 00:22:08,676
- Sorry. - Why?

351
00:22:11,047 --> 00:22:12,307
I'm going abroad to study.

352
00:22:13,617 --> 00:22:14,617
You are?

353
00:22:15,946 --> 00:22:17,587
It turned out that way.

354
00:22:17,817 --> 00:22:20,617
My family wants me to pursue my dream before it's too late.

355
00:22:21,857 --> 00:22:23,887
- Do you mean it? - Yes.

356
00:22:25,226 --> 00:22:28,496
You need to find a new job, but I...

357
00:22:29,357 --> 00:22:30,397
I'm sorry.

358
00:22:35,837 --> 00:22:38,067
Why are you sorry?

359
00:22:38,407 --> 00:22:41,206
You really wanted to become a fashion designer.

360
00:22:41,936 --> 00:22:43,047
Congratulations.

361
00:22:44,407 --> 00:22:46,446
Brat. Thank you.

362
00:22:46,946 --> 00:22:47,946
For what?

363
00:22:47,946 --> 00:22:50,887
You're letting me have a fashion designer friend.

364
00:22:57,226 --> 00:23:00,226
- I'm so happy for you. - Congratulations.

365
00:23:01,057 --> 00:23:03,526
Designer friend. Cheers.

366
00:23:03,666 --> 00:23:05,696
- What... Just eat. - Okay.

367
00:23:05,696 --> 00:23:06,736
He must be drunk.

368
00:23:17,577 --> 00:23:19,877
Miss, have some tea.

369
00:23:22,547 --> 00:23:24,117
Why is my grandfather...

370
00:23:24,587 --> 00:23:26,047
taking so long to decide?

371
00:23:26,557 --> 00:23:28,486
What are you waiting for him to decide?

372
00:23:31,186 --> 00:23:33,256
I said I wanted a personal shopper.

373
00:23:34,827 --> 00:23:36,526
A personal shopper?

374
00:23:37,597 --> 00:23:40,567
A personal assistant to help you shop?

375
00:23:40,736 --> 00:23:41,736
Yes.

376
00:23:42,397 --> 00:23:44,067
I recommended someone.

377
00:23:44,666 --> 00:23:46,307
Whom did you recommend?

378
00:23:47,236 --> 00:23:48,407
Hong Se Yun.

379
00:23:51,676 --> 00:23:53,246
I want her.

380
00:24:00,486 --> 00:24:01,716
Why someone...

381
00:24:03,426 --> 00:24:05,956
you had issues with?

382
00:24:07,226 --> 00:24:09,026
I want to teach her something.

383
00:24:09,827 --> 00:24:12,627
- "Teach her"? - Just something.

384
00:24:27,776 --> 00:24:29,787
He should've received the report by now.

385
00:24:31,416 --> 00:24:32,756
It's too quiet.

386
00:24:38,357 --> 00:24:39,996
The core executives...

387
00:24:40,657 --> 00:24:44,466
received a report stating Gyung Hye is secretly...

388
00:24:45,226 --> 00:24:47,037
receiving psychiatric treatment...

389
00:24:47,736 --> 00:24:50,236
along with photographs.

390
00:24:50,837 --> 00:24:53,307
Yes, sir. It was sent from a burner phone...

391
00:24:53,537 --> 00:24:56,307
early this morning to all the core executives.

392
00:24:57,647 --> 00:24:58,976
When is the executives meeting?

393
00:24:59,847 --> 00:25:01,077
It is 10 days from now.

394
00:25:01,446 --> 00:25:02,986
The picture...

395
00:25:03,387 --> 00:25:05,317
must be completed flawlessly...

396
00:25:05,986 --> 00:25:07,516
before that meeting.

397
00:25:13,157 --> 00:25:14,327
(Resume: Hong Se Yun)

398
00:25:14,327 --> 00:25:16,327
In order for the picture to be completed,

399
00:25:17,567 --> 00:25:19,127
we need Hong Se Yun.

400
00:25:20,867 --> 00:25:22,537
Gyung Hye was right.

401
00:25:22,837 --> 00:25:26,407
There is something about her that you need to know.

402
00:25:40,016 --> 00:25:41,087
Bring Hong Se Yun...

403
00:25:42,157 --> 00:25:43,387
to me right now.

404
00:25:47,956 --> 00:25:49,827
The person you have reached...

405
00:25:51,367 --> 00:25:53,966
Your belly is big.

406
00:25:53,966 --> 00:25:56,236
You ate a lot, didn't you?

407
00:25:57,407 --> 00:25:59,877
How did you know I ate a lot?

408
00:26:01,137 --> 00:26:03,307
Do you have a baby in there?

409
00:26:04,307 --> 00:26:07,446
- It was so much fun. - I haven't watched a movie in ages.

410
00:26:07,516 --> 00:26:10,516
People need culture in their lives. Don't you agree?

411
00:26:12,287 --> 00:26:13,287
(Missed calls)

412
00:26:14,456 --> 00:26:16,057
(Mr. Lee Jae Joon)

413
00:26:18,426 --> 00:26:20,726
- You're popular. - I guess so.

414
00:26:26,137 --> 00:26:28,537
Hello? I received a missed call...

415
00:26:35,407 --> 00:26:36,446
Don't be late.

416
00:26:40,547 --> 00:26:42,647
What is it? Is something wrong?

417
00:26:43,887 --> 00:26:44,887
Se Yun.

418
00:27:41,676 --> 00:27:42,946
Don't be nervous.

419
00:27:43,377 --> 00:27:46,147
I plan to forgive you for what happened.

420
00:27:57,256 --> 00:27:58,787
I came for an apology.

421
00:28:09,436 --> 00:28:10,506
Did you...

422
00:28:12,607 --> 00:28:15,307
just say "apology"?

423
00:28:18,147 --> 00:28:19,516
If you weren't going to apology,

424
00:28:20,077 --> 00:28:21,716
why did you call me here?

425
00:28:22,676 --> 00:28:24,587
Don't do it, if it's too hard for you.

426
00:28:24,716 --> 00:28:28,486
Your apology isn't all that important to me...

427
00:28:29,157 --> 00:28:30,857
or to my dad.

428
00:28:33,557 --> 00:28:34,827
Do you realize...

429
00:28:36,567 --> 00:28:40,397
what great opportunity you're passing up?

430
00:28:40,537 --> 00:28:43,607
Remember what you said when we ate here before?

431
00:28:44,936 --> 00:28:46,837
You said a bloody hunting dog...

432
00:28:47,377 --> 00:28:49,206
survived because of his love for food.

433
00:28:50,847 --> 00:28:52,176
For the dog, it was food,

434
00:28:53,476 --> 00:28:55,147
but for me, it's my family.

435
00:28:56,916 --> 00:28:58,547
So, Eun Gyung Hye,

436
00:28:59,287 --> 00:29:01,087
don't mistreat people.

437
00:29:01,157 --> 00:29:02,557
"Mistreat"?

438
00:29:03,956 --> 00:29:06,327
You're the one mistreating me.

439
00:29:07,526 --> 00:29:09,226
How dare you talk to me that way?

440
00:29:16,666 --> 00:29:19,506
I didn't come to fight you.

441
00:29:20,206 --> 00:29:21,236
You can't fight...

442
00:29:21,837 --> 00:29:23,807
with someone who doesn't understand you.

443
00:29:25,206 --> 00:29:26,276
Shut it.

444
00:29:26,276 --> 00:29:28,676
Do you think I'm afraid of you?

445
00:29:29,847 --> 00:29:31,216
I feel...

446
00:29:32,887 --> 00:29:34,287
sorry for you.

447
00:29:35,916 --> 00:29:37,557
What do you have?

448
00:29:38,256 --> 00:29:39,256
Money?

449
00:29:40,557 --> 00:29:42,597
It looks like that is all you have.

450
00:29:59,617 --> 00:30:01,147
That's why I want to teach you...

451
00:30:02,347 --> 00:30:04,446
how that one thing that I have...

452
00:30:05,347 --> 00:30:08,157
can drag you into the sewer.

453
00:30:08,716 --> 00:30:09,986
Try me.

454
00:30:12,186 --> 00:30:13,256
I'll teach you...

455
00:30:14,256 --> 00:30:17,627
that there are people you cannot order around.

456
00:30:18,526 --> 00:30:20,766
I'll make sure you understand.

457
00:30:35,246 --> 00:30:36,486
Hong Se Yun.

458
00:30:39,617 --> 00:30:41,117
How dare you...

459
00:30:59,236 --> 00:31:01,236
How refreshing.

460
00:31:08,617 --> 00:31:11,916
(Mr. Lee Jae Joon: Please call me.)

461
00:31:19,526 --> 00:31:22,357
Hong Se Yun came to the house to see Ms. Gyung Hye once.

462
00:31:23,166 --> 00:31:26,026
Ms. Geum knew that the shop worker with whom she had the altercation...

463
00:31:26,537 --> 00:31:28,097
was her daughter.

464
00:31:34,407 --> 00:31:35,877
(Mother: Geum Young Sook)

465
00:31:48,087 --> 00:31:49,117
(Hong Pil Mok)

466
00:32:08,236 --> 00:32:09,236
Se Yun.

467
00:32:10,847 --> 00:32:12,107
Mom.

468
00:32:14,117 --> 00:32:17,686
It's still cold out. You should dress more warmly.

469
00:32:17,787 --> 00:32:18,787
Se Yun.

470
00:32:29,127 --> 00:32:30,196
Ms. Geum.

471
00:32:33,597 --> 00:32:34,736
Mr. Lee?

472
00:33:11,537 --> 00:33:15,276
(Mysterious Personal Shopper)

473
00:33:15,407 --> 00:33:18,147
You work for the owner of Winners Group?

474
00:33:18,147 --> 00:33:19,676
What are you doing?

475
00:33:19,676 --> 00:33:21,016
What are you thinking?

476
00:33:21,016 --> 00:33:22,946
We have to send Se Yun abroad to study.

477
00:33:22,986 --> 00:33:25,547
- She's going to study abroad? - Is that true?

478
00:33:25,587 --> 00:33:26,787
Thanks to you,

479
00:33:26,787 --> 00:33:29,016
- I had a great experience. - Please help me.

480
00:33:29,016 --> 00:33:30,557
You said you'd help.

481
00:33:30,557 --> 00:33:32,486
Schedule a meeting with Hong Se Yun.

482
00:33:32,486 --> 00:33:34,357
I'll do whatever it takes to stop it.

483
00:33:34,357 --> 00:33:36,797
I will not let her step foot in this house.

