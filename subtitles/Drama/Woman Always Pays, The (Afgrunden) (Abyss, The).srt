﻿1
00:00:00,000 --> 00:00:05,140
Dramatorrent.com

2
00:00:06,925 --> 00:00:08,255
Come with me.

3
00:00:19,655 --> 00:00:21,455
- Don't answer that. - Sorry?

4
00:00:21,855 --> 00:00:23,555
Don't answer any calls.

5
00:00:33,525 --> 00:00:34,695
What are you doing?

6
00:00:34,825 --> 00:00:37,025
What else? You're mine,

7
00:00:37,295 --> 00:00:38,295
so I'm marking you.

8
00:00:39,125 --> 00:00:41,355
We're dating from today.

9
00:00:47,395 --> 00:00:48,425
Hey.

10
00:00:49,295 --> 00:00:50,555
How long were you there?

11
00:00:50,895 --> 00:00:52,655
I'm going to tell Yuh Ri.

12
00:00:53,225 --> 00:00:55,155
Hey. Hey!

13
00:00:58,225 --> 00:00:59,755
(Mr. Son)

14
00:01:01,355 --> 00:01:03,195
- Hello? - Moo Yul, are you home?

15
00:01:04,225 --> 00:01:05,725
Is Yuh Ri home?

16
00:01:06,195 --> 00:01:08,595
No. She isn't back from school yet.

17
00:01:09,295 --> 00:01:11,625
Why are so many people looking for her today?

18
00:01:12,725 --> 00:01:13,825
What do you mean?

19
00:01:14,695 --> 00:01:16,065
Was someone looking for her?

20
00:01:16,155 --> 00:01:19,255
Some wealthy looking lady came by earlier and...

21
00:01:20,955 --> 00:01:21,955
Hello?

22
00:01:22,925 --> 00:01:23,955
Mr. Son?

23
00:01:39,955 --> 00:01:42,655
Joo Ho, your mom and I...

24
00:01:43,295 --> 00:01:44,955
grew up in the same orphanage.

25
00:01:46,425 --> 00:01:48,925
We endured the sorrows and loneliness of not having parents...

26
00:01:49,955 --> 00:01:52,525
by being there for each other.

27
00:01:53,995 --> 00:01:55,025
That's why...

28
00:01:55,625 --> 00:01:57,555
you don't seem like a stranger...

29
00:01:57,895 --> 00:01:59,655
since you're Joo Ho's daughter.

30
00:02:00,355 --> 00:02:01,495
I had no idea.

31
00:02:01,855 --> 00:02:03,955
My father never said.

32
00:02:05,555 --> 00:02:06,555
Yuh Ri.

33
00:02:07,225 --> 00:02:08,795
I have a favor to ask.

34
00:02:09,525 --> 00:02:10,525
You are...

35
00:02:11,225 --> 00:02:13,395
the only person in the world who can do it for me.

36
00:02:14,465 --> 00:02:15,625
Can you do it?

37
00:02:19,825 --> 00:02:22,055
Sure. Tell me what it is.

38
00:02:24,125 --> 00:02:25,125
Yuh Ri.

39
00:02:25,995 --> 00:02:27,195
My son Hae Sung...

40
00:02:29,525 --> 00:02:30,855
Get out right now!

41
00:02:31,195 --> 00:02:33,195
Father, how did you...

42
00:02:35,095 --> 00:02:37,655
I'm sorry I couldn't take your call earlier.

43
00:02:37,695 --> 00:02:39,995
- What happened was... - Get out right this instant!

44
00:02:40,295 --> 00:02:41,325
Get out!

45
00:02:43,655 --> 00:02:46,425
Go straight home and wait for me.

46
00:02:47,755 --> 00:02:49,955
But Ms. Hong wanted to talk...

47
00:02:49,955 --> 00:02:51,295
Didn't you hear me?

48
00:02:54,895 --> 00:02:55,895
Okay.

49
00:03:04,855 --> 00:03:06,525
What do you think you're doing?

50
00:03:07,395 --> 00:03:09,195
How dare you take Yuh Ri?

51
00:03:09,195 --> 00:03:10,295
What else could I do?

52
00:03:10,555 --> 00:03:13,995
What parent wouldn't go crazy when her child is dying before her eyes?

53
00:03:14,225 --> 00:03:15,495
How much did you tell her?

54
00:03:16,655 --> 00:03:17,655
Don't tell me...

55
00:03:18,325 --> 00:03:20,195
you told her everything.

56
00:03:20,255 --> 00:03:21,295
I couldn't.

57
00:03:22,255 --> 00:03:23,625
I was about to.

58
00:03:23,625 --> 00:03:25,625
I wanted to beg, but I couldn't.

59
00:03:28,225 --> 00:03:29,255
Ji Won.

60
00:03:30,295 --> 00:03:31,555
Listen to me.

61
00:03:32,625 --> 00:03:33,625
I...

62
00:03:35,225 --> 00:03:36,555
can't help you.

63
00:03:37,525 --> 00:03:40,195
I can't hurt my child in order to help yours.

64
00:03:40,825 --> 00:03:43,025
I was your slave for 10 years.

65
00:03:43,055 --> 00:03:44,425
But not my child!

66
00:03:45,625 --> 00:03:46,625
So...

67
00:03:47,495 --> 00:03:49,055
don't touch my baby.

68
00:03:50,025 --> 00:03:52,125
Those 10 years weren't for free!

69
00:03:52,455 --> 00:03:54,525
Your family was able to survive thanks to that.

70
00:03:54,895 --> 00:03:56,425
You should repay us.

71
00:03:56,425 --> 00:03:57,925
You should show gratitude!

72
00:04:00,255 --> 00:04:01,795
Now, you sound like yourself.

73
00:04:03,055 --> 00:04:05,555
You're finally showing your true colors.

74
00:04:08,425 --> 00:04:09,425
You're right.

75
00:04:10,755 --> 00:04:13,125
I survived on my salary that you and your husband paid me.

76
00:04:14,055 --> 00:04:16,325
With that, I clothed Yuh Ri,

77
00:04:16,355 --> 00:04:18,895
fed her, and was able to send her to school.

78
00:04:18,895 --> 00:04:20,625
For that, I'm forever grateful.

79
00:04:21,725 --> 00:04:23,125
As the chairman's driver,

80
00:04:23,125 --> 00:04:25,425
I was his puppet, overseeing his personal life,

81
00:04:25,425 --> 00:04:27,225
doing any disgusting task he asked of me,

82
00:04:27,265 --> 00:04:29,325
and sometimes voluntarily obeying him.

83
00:04:31,925 --> 00:04:33,295
In order raise Yuh Ri...

84
00:04:34,095 --> 00:04:35,525
properly.

85
00:04:36,125 --> 00:04:38,455
- Joo Ho. - Don't say my name.

86
00:04:40,995 --> 00:04:42,525
You never once...

87
00:04:43,295 --> 00:04:45,455
sincerely considered me your friend.

88
00:04:56,555 --> 00:04:57,925
I won't give up.

89
00:04:58,695 --> 00:04:59,695
I'm...

90
00:05:00,195 --> 00:05:01,355
Hae Sung's mom.

91
00:05:02,265 --> 00:05:03,795
I won't give up.

92
00:05:13,595 --> 00:05:14,595
Yuh Ri.

93
00:05:16,265 --> 00:05:17,295
Hey.

94
00:05:17,725 --> 00:05:18,925
Shouldn't you be tutoring right now?

95
00:05:20,225 --> 00:05:23,225
So many people were looking for you, but you couldn't be reached.

96
00:05:23,525 --> 00:05:24,855
I was worried, so I rescheduled.

97
00:05:26,225 --> 00:05:27,325
Are you okay?

98
00:05:27,925 --> 00:05:29,755
Yes, I'm fine.

99
00:05:32,725 --> 00:05:33,725
Yuh Ri!

100
00:05:35,125 --> 00:05:36,925
Guess what? My brother...

101
00:05:37,025 --> 00:05:39,395
and another girl...

102
00:05:39,495 --> 00:05:42,025
Yul Mae, you've grown so tall.

103
00:05:42,525 --> 00:05:44,455
I'd better work hard...

104
00:05:44,625 --> 00:05:47,325
and make you grow more.

105
00:05:47,655 --> 00:05:48,695
Come with me.

106
00:05:58,555 --> 00:05:59,555
Father.

107
00:06:10,525 --> 00:06:11,955
Listen to me.

108
00:06:13,525 --> 00:06:14,825
Don't meet with anyone...

109
00:06:15,425 --> 00:06:17,055
from Wid anymore.

110
00:06:18,425 --> 00:06:20,125
Don't answer any calls.

111
00:06:20,825 --> 00:06:23,255
Not Hae Joo, Ms. Hong, or the chairman.

112
00:06:23,295 --> 00:06:24,295
Anyone.

113
00:06:24,995 --> 00:06:27,125
What's wrong? Did something happen?

114
00:06:27,955 --> 00:06:30,525
- What's with everyone today? - Do you understand me?

115
00:06:35,025 --> 00:06:36,025
Yes.

116
00:07:00,625 --> 00:07:03,295
Hae Sung has stabilized,

117
00:07:03,455 --> 00:07:05,625
but you should get the donor's guardian's consent...

118
00:07:05,625 --> 00:07:07,025
as soon as possible.

119
00:07:08,955 --> 00:07:10,555
What's for dinner?

120
00:07:11,455 --> 00:07:12,955
I'm craving meat.

121
00:07:13,555 --> 00:07:14,925
Maybe it's because I'm in a good mood.

122
00:07:15,725 --> 00:07:17,025
Should we go for steak?

123
00:07:19,125 --> 00:07:21,455
- What? - You're in a good mood?

124
00:07:22,525 --> 00:07:23,625
You're craving meat?

125
00:07:24,555 --> 00:07:27,725
How can you think about food when Hae Sung's lying there like that?

126
00:07:28,095 --> 00:07:29,095
Mom.

127
00:07:29,125 --> 00:07:31,395
Your only sibling is barely hanging on,

128
00:07:31,395 --> 00:07:32,925
fighting for his life.

129
00:07:33,095 --> 00:07:36,025
How can you eat right now?

130
00:07:37,755 --> 00:07:40,655
Should I starve because I feel bad for him?

131
00:07:40,825 --> 00:07:42,055
Should I not breathe either?

132
00:07:42,125 --> 00:07:43,855
Should we all die together?

133
00:07:43,895 --> 00:07:44,925
Hae Joo!

134
00:07:45,425 --> 00:07:48,455
Ever since he got sick, I'm invisible here.

135
00:07:48,595 --> 00:07:50,725
You and Dad care only about Hae Sung.

136
00:07:51,655 --> 00:07:53,655
Have you given me any attention recently?

137
00:07:54,425 --> 00:07:57,195
Why are you taking it out on me? Like yesterday too.

138
00:07:57,325 --> 00:07:59,425
You humiliated me and slapped me in front of Yuh Ri!

139
00:08:00,195 --> 00:08:02,755
Is Hae Sung your only child? What about me?

140
00:08:05,625 --> 00:08:06,625
Hey...

141
00:08:23,825 --> 00:08:25,595
I needed to get on Yuh Ri's good side...

142
00:08:26,255 --> 00:08:29,255
in order for Hae Sung to get the transplant, so I wasn't thinking.

143
00:08:30,025 --> 00:08:32,325
- Don't get the wrong idea. - Don't you ever...

144
00:08:32,795 --> 00:08:34,925
touch Hae Joo again. If you do,

145
00:08:35,795 --> 00:08:37,095
I won't forgive you.

146
00:08:38,865 --> 00:08:40,525
Are you suspecting me?

147
00:08:40,825 --> 00:08:42,795
Do you think I hit her because I'm her stepmom?

148
00:08:44,755 --> 00:08:46,655
If you do, I'm hurt.

149
00:08:46,925 --> 00:08:49,325
You know how I've raised her until now.

150
00:08:50,865 --> 00:08:52,325
After my baby died...

151
00:08:52,655 --> 00:08:55,625
without ever feeling a mom's embrace,

152
00:08:55,955 --> 00:08:57,195
Hae Joo...

153
00:08:57,795 --> 00:09:01,095
entered my heart a few days later.

154
00:09:10,025 --> 00:09:13,025
Mr. Koo. I've brought the documents you asked for.

155
00:09:18,655 --> 00:09:21,455
Your ex-wife, the minister's daughter, left you and your baby...

156
00:09:21,825 --> 00:09:24,625
because your mother wasn't your father's wife but someone...

157
00:09:24,695 --> 00:09:26,825
he brought in to give him a child as his wife was barren.

158
00:09:27,455 --> 00:09:29,325
That's when I first met Hae Joo.

159
00:09:45,595 --> 00:09:48,725
Even when you proposed to me, I knew...

160
00:09:49,125 --> 00:09:50,895
that what you needed wasn't me,

161
00:09:50,895 --> 00:09:52,555
but a mom for Hae Joo.

162
00:09:53,495 --> 00:09:55,055
But I did my best...

163
00:09:55,255 --> 00:09:57,125
to you and to her.

164
00:09:58,325 --> 00:09:59,365
And yet,

165
00:09:59,795 --> 00:10:03,125
you're accusing me because I hit her once?

166
00:10:03,755 --> 00:10:06,555
While my son Hae Sung, who took us 10 years to have,

167
00:10:06,625 --> 00:10:08,295
is fighting to stay alive...

168
00:10:08,525 --> 00:10:11,125
before even starting school?

169
00:10:11,125 --> 00:10:12,255
That's enough!

170
00:10:12,955 --> 00:10:14,555
You're overly sensitive.

171
00:10:17,695 --> 00:10:18,695
There's something...

172
00:10:19,695 --> 00:10:20,955
I want to confirm.

173
00:10:23,025 --> 00:10:25,865
That baby that you had before marrying me.

174
00:10:29,125 --> 00:10:31,365
Is that baby really dead?

175
00:10:32,725 --> 00:10:34,895
If not, I would've found my baby somehow.

176
00:10:35,795 --> 00:10:39,225
I refuse to watch another one of my children die again.

177
00:10:39,795 --> 00:10:41,125
So I'll do anything...

178
00:10:41,195 --> 00:10:43,595
in order to save Hae Sung.

179
00:10:43,995 --> 00:10:46,255
So you find a way to convince Mr. Son,

180
00:10:46,455 --> 00:10:48,395
regardless of the means.

181
00:10:55,495 --> 00:10:57,295
(Police Station)

182
00:10:57,295 --> 00:10:58,295
Wait.

183
00:10:59,495 --> 00:11:00,495
Detective.

184
00:11:00,755 --> 00:11:02,825
I wasn't trying to eat and run.

185
00:11:03,455 --> 00:11:05,095
I forgot to get cash.

186
00:11:05,555 --> 00:11:08,895
I just returned to Korea, so I was just out of it.

187
00:11:08,925 --> 00:11:10,395
Detective. Look.

188
00:11:12,895 --> 00:11:15,195
See? I have a credit card.

189
00:11:15,655 --> 00:11:18,055
I wasn't trying to run.

190
00:11:18,055 --> 00:11:21,355
Then you should've used that to pay. Why didn't you?

191
00:11:22,395 --> 00:11:24,125
You should be ashamed.

192
00:11:24,325 --> 00:11:26,655
How much did you eat all by yourself?

193
00:11:27,095 --> 00:11:28,325
You look normal enough.

194
00:11:29,055 --> 00:11:31,125
I have my reasons.

195
00:11:32,825 --> 00:11:34,725
Hello. Thank you for your hard work.

196
00:11:34,895 --> 00:11:36,795
I just received a call...

197
00:11:36,925 --> 00:11:38,355
that Koo Do Chi was here.

198
00:11:40,395 --> 00:11:41,395
Mr. Son.

199
00:11:43,195 --> 00:11:44,255
- Mr. Son. - Do Chi!

200
00:11:45,295 --> 00:11:47,855
- Do Chi. - Mr. Son.

201
00:11:48,925 --> 00:11:52,925
Wow. You've grown so much.

202
00:11:53,555 --> 00:11:56,095
It feels like only yesterday...

203
00:11:56,325 --> 00:11:59,755
that you were crying and insisting you didn't want to go to the US.

204
00:12:01,125 --> 00:12:02,195
I know.

205
00:12:02,255 --> 00:12:04,525
That runny nosed, whining crybaby...

206
00:12:05,325 --> 00:12:06,555
grew up already.

207
00:12:09,125 --> 00:12:10,455
I'm sorry about back then...

208
00:12:11,655 --> 00:12:13,295
for not being able to help.

209
00:12:14,995 --> 00:12:17,825
That always bothered me.

210
00:12:19,325 --> 00:12:21,455
It's so good to see you now.

211
00:12:23,625 --> 00:12:26,255
That's right. Does the chairman know you're back?

212
00:12:27,895 --> 00:12:28,925
No.

213
00:12:30,795 --> 00:12:32,095
When I heard about Hae Sung,

214
00:12:33,095 --> 00:12:35,625
I came to Korea to try to help as his uncle,

215
00:12:38,495 --> 00:12:39,655
but he says there's no need.

216
00:12:42,255 --> 00:12:43,525
He said he found a donor.

217
00:12:46,855 --> 00:12:48,095
But still,

218
00:12:49,195 --> 00:12:51,125
I got tested secretly just in case.

219
00:12:53,725 --> 00:12:55,325
That's right. Just a minute.

220
00:13:01,895 --> 00:13:02,925
I found it.

221
00:13:04,455 --> 00:13:06,355
Ta-da. Congratulations.

222
00:13:08,825 --> 00:13:10,755
Happy early birthday.

223
00:13:12,295 --> 00:13:14,555
You and I have the same birthday.

224
00:13:16,295 --> 00:13:17,295
Here.

225
00:13:19,995 --> 00:13:21,195
Gosh.

226
00:13:22,725 --> 00:13:24,755
I never would've expected this.

227
00:13:25,455 --> 00:13:27,125
I'm so thankful.

228
00:13:28,725 --> 00:13:29,725
I don't have...

229
00:13:30,595 --> 00:13:31,695
anything for you.

230
00:13:32,355 --> 00:13:34,395
You're thankful, right? In that case,

231
00:13:36,055 --> 00:13:37,095
rather than a gift,

232
00:13:37,825 --> 00:13:39,095
lend me some money.

233
00:13:40,195 --> 00:13:41,225
What?

234
00:13:42,325 --> 00:13:43,755
Well, I...

235
00:13:43,925 --> 00:13:45,555
need to go to a hotel,

236
00:13:46,895 --> 00:13:50,055
but my brother will know right away if I use my credit card.

237
00:13:51,795 --> 00:13:52,955
That means...

238
00:13:53,825 --> 00:13:55,225
I'll be on the next flight out.

239
00:14:00,895 --> 00:14:02,055
How's your hand?

240
00:14:03,555 --> 00:14:04,995
Did you take your medicine?

241
00:14:05,355 --> 00:14:08,325
Yes. They said it'll heal soon. Don't worry.

242
00:14:09,125 --> 00:14:11,555
You're not hurt anywhere else?

243
00:14:13,555 --> 00:14:14,625
I'm fine.

244
00:14:19,395 --> 00:14:21,525
Let's get surgery once you start college.

245
00:14:22,495 --> 00:14:23,625
It looks bad.

246
00:14:25,825 --> 00:14:28,025
I've had it since I was a baby, so I'm used to it.

247
00:14:28,025 --> 00:14:29,795
I don't even notice it.

248
00:14:30,955 --> 00:14:33,725
But how did I get this scar?

249
00:14:36,895 --> 00:14:38,255
That was...

250
00:14:40,425 --> 00:14:42,355
It was my mistake.

251
00:14:44,055 --> 00:14:45,095
Sorry...

252
00:14:45,725 --> 00:14:48,395
for not giving you a pretty, flawless body.

253
00:14:49,595 --> 00:14:50,655
There you go again.

254
00:14:50,695 --> 00:14:52,895
A scar like this is nothing.

255
00:14:54,495 --> 00:14:55,495
That's right.

256
00:14:55,925 --> 00:14:58,195
You know that painting you asked for last time?

257
00:14:58,495 --> 00:15:00,495
I just need to redo the finishing touches.

258
00:15:01,325 --> 00:15:03,795
But whom is it for?

259
00:15:04,295 --> 00:15:06,955
Someone I want to cheer on,

260
00:15:07,455 --> 00:15:09,455
saying the tide will come in someday...

261
00:15:09,755 --> 00:15:11,825
just like that painting.

262
00:15:44,455 --> 00:15:45,455
Enough.

263
00:15:46,495 --> 00:15:48,625
Please disappear, Mother.

264
00:15:54,495 --> 00:15:55,855
You need to disappear...

265
00:15:56,995 --> 00:15:58,125
for me to live.

266
00:16:03,425 --> 00:16:06,225
You and Do Chi need to be gone...

267
00:16:07,255 --> 00:16:08,255
so that I...

268
00:16:09,095 --> 00:16:10,695
can become the owner of Wid.

269
00:16:30,395 --> 00:16:31,395
Do Chi.

270
00:16:57,895 --> 00:17:00,325
Do Young!

271
00:17:01,425 --> 00:17:02,955
I'm sorry.

272
00:17:03,295 --> 00:17:04,325
I won't do it.

273
00:17:04,565 --> 00:17:06,955
I'll never do it again!

274
00:17:07,725 --> 00:17:08,995
Do Young!

275
00:17:09,255 --> 00:17:12,825
I won't pee in my pants again.

276
00:17:13,395 --> 00:17:15,855
So please let me see my mommy!

277
00:17:16,425 --> 00:17:19,655
I want to go to my mommy's funeral too.

278
00:17:19,825 --> 00:17:22,655
Do Young!

279
00:17:25,255 --> 00:17:26,955
Do Young!

280
00:17:27,695 --> 00:17:29,995
I want to go to the funeral.

281
00:17:46,895 --> 00:17:48,095
I won't eat.

282
00:17:48,295 --> 00:17:49,995
Take that out!

283
00:17:50,125 --> 00:17:52,225
I'm sorry.

284
00:17:53,065 --> 00:17:55,395
I should've been more understanding.

285
00:17:57,195 --> 00:17:59,455
Please don't do this. I can't handle it.

286
00:17:59,795 --> 00:18:01,495
Can't you stop punishing me?

287
00:18:03,225 --> 00:18:05,925
I'll do anything you want.

288
00:18:10,425 --> 00:18:11,525
Do you mean it?

289
00:18:12,355 --> 00:18:15,065
You'll do anything I want?

290
00:18:23,325 --> 00:18:26,455
Which one is prettier?

291
00:18:26,725 --> 00:18:30,855
Let's ask my father

292
00:18:33,895 --> 00:18:35,225
- Knew it. - Knew it.

293
00:18:35,425 --> 00:18:36,425
We're so alike.

294
00:18:36,955 --> 00:18:39,125
Wrap it nicely, please.

295
00:18:39,455 --> 00:18:41,855
My future father-in-law will be wearing these.

296
00:18:44,065 --> 00:18:45,855
- Are you going to the studio? - Yes.

297
00:18:46,325 --> 00:18:48,025
I need to finish off a painting.

298
00:18:48,255 --> 00:18:50,255
You're meeting your new tutoring student?

299
00:18:50,495 --> 00:18:53,995
Yes. The professor strongly recommended me.

300
00:18:54,095 --> 00:18:55,855
They're paying double my usual rate.

301
00:18:56,955 --> 00:18:59,895
Plus, they asked for me by name.

302
00:19:00,625 --> 00:19:03,855
You know I'm outstanding.

303
00:19:05,195 --> 00:19:07,495
Naturally. You're my boyfriend after all.

304
00:19:07,795 --> 00:19:09,255
Hey. Hae Joo.

305
00:19:10,525 --> 00:19:12,295
How did you find me?

306
00:19:12,565 --> 00:19:14,495
- Were you stalking us? - What about you?

307
00:19:14,995 --> 00:19:16,355
Are you a puppy dog?

308
00:19:16,995 --> 00:19:19,325
Why are you wagging your tail at my boyfriend?

309
00:19:19,725 --> 00:19:23,065
Scram before I grab a pair of scissors and cut off your tail.

310
00:19:23,795 --> 00:19:25,455
What? A dog?

311
00:19:26,295 --> 00:19:27,565
Boyfriend?

312
00:19:31,325 --> 00:19:32,355
Hae Joo.

313
00:19:32,695 --> 00:19:35,425
If you're going insane, at least maintain your dignity.

314
00:19:35,655 --> 00:19:37,395
Or wear a sign saying,

315
00:19:37,395 --> 00:19:40,125
"I'm crazy," so that I can avoid you.

316
00:19:41,495 --> 00:19:43,355
Moo Yul. Let's go.

317
00:19:46,455 --> 00:19:47,625
Didn't you know?

318
00:19:48,395 --> 00:19:50,525
We've kissed already.

319
00:19:53,855 --> 00:19:54,855
Is that true?

320
00:19:54,995 --> 00:19:58,225
It was against my will. She just...

321
00:20:04,125 --> 00:20:05,125
Hae Joo.

322
00:20:05,855 --> 00:20:06,855
Watch.

323
00:20:08,755 --> 00:20:10,125
Let's kiss.

324
00:20:12,455 --> 00:20:14,225
- Don't do it! - What are you doing?

325
00:20:24,095 --> 00:20:26,695
Hey! Stop that right now!

326
00:20:43,595 --> 00:20:44,595
All done.

327
00:20:46,255 --> 00:20:47,525
Perfect.

328
00:20:52,125 --> 00:20:53,825
Resist all you want, Moo Yul.

329
00:20:54,325 --> 00:20:55,995
You're mine regardless.

330
00:20:58,225 --> 00:20:59,255
One,

331
00:20:59,855 --> 00:21:01,855
two, three.

332
00:21:08,325 --> 00:21:10,925
Hello, I'm Kim Moo Yul, sophomore at Hangook University,

333
00:21:10,955 --> 00:21:13,225
here on Professor Jang's recommendation.

334
00:21:16,595 --> 00:21:17,825
We've met before.

335
00:21:19,125 --> 00:21:21,065
My daughter insisted on you,

336
00:21:21,065 --> 00:21:23,125
so I called Professor Jang myself.

337
00:21:24,225 --> 00:21:25,225
Pardon?

338
00:21:28,695 --> 00:21:30,455
Hi, First Love.

339
00:21:31,995 --> 00:21:32,995
You...

340
00:21:39,065 --> 00:21:40,095
Don't go.

341
00:21:42,655 --> 00:21:45,125
Don't play games like this again. It's offensive.

342
00:21:45,355 --> 00:21:47,895
Games? Who says I'm playing games?

343
00:21:48,725 --> 00:21:50,325
I'm drawn to you.

344
00:21:50,625 --> 00:21:51,995
How is this a game?

345
00:21:53,425 --> 00:21:55,755
I like Yuh Ri,

346
00:21:55,995 --> 00:21:57,355
and she likes me too.

347
00:21:57,995 --> 00:21:58,995
So?

348
00:21:59,825 --> 00:22:01,595
You look down on us, don't you?

349
00:22:02,355 --> 00:22:04,295
You think you can step on us,

350
00:22:04,655 --> 00:22:07,295
- disrespect us and insult us. - No.

351
00:22:08,025 --> 00:22:09,325
I don't know about Yuh Ri,

352
00:22:09,895 --> 00:22:11,655
but I don't look down on you at all.

353
00:22:12,525 --> 00:22:16,125
So take a good look at me, Moo Yul.

354
00:22:17,195 --> 00:22:18,255
What?

355
00:22:18,395 --> 00:22:20,195
Look at who's standing before you,

356
00:22:20,625 --> 00:22:22,925
and what I have behind me.

357
00:22:25,125 --> 00:22:27,995
The students at your school, who are the top 0.1 percent,

358
00:22:28,525 --> 00:22:30,255
strive to get a job at Wid.

359
00:22:30,555 --> 00:22:32,325
Wid Construction. Wid Commerce.

360
00:22:32,395 --> 00:22:33,755
Even Wid Foods.

361
00:22:35,055 --> 00:22:37,825
I'm the Wid's only daughter.

362
00:22:38,755 --> 00:22:40,625
So grab the opportunity while you can.

363
00:22:41,225 --> 00:22:43,255
Your life will change in an instant.

364
00:22:51,225 --> 00:22:52,225
You...

365
00:22:53,195 --> 00:22:54,195
Are you intrigued now?

366
00:22:55,655 --> 00:22:57,125
really are the worst.

367
00:22:58,095 --> 00:22:59,795
Forget the tutoring.

368
00:23:20,955 --> 00:23:22,355
- Doctor. - Hello.

369
00:23:22,695 --> 00:23:25,525
The results are out for the person who requested to be tested.

370
00:23:26,425 --> 00:23:28,925
The results? Who...

371
00:23:36,495 --> 00:23:37,495
Get up.

372
00:23:43,595 --> 00:23:44,595
Do Young.

373
00:23:46,225 --> 00:23:47,495
How are you here?

374
00:23:50,295 --> 00:23:51,425
The test results are out.

375
00:23:51,895 --> 00:23:53,725
You are not a match for the bone marrow transplant.

376
00:23:55,095 --> 00:23:57,125
You've completed your business here, so go back.

377
00:23:58,825 --> 00:23:59,855
Wait a second.

378
00:24:02,555 --> 00:24:04,555
I don't want to get physical again,

379
00:24:04,655 --> 00:24:05,955
so leave quietly.

380
00:24:09,425 --> 00:24:10,695
Do Young.

381
00:24:13,125 --> 00:24:14,525
I want to live in Korea.

382
00:24:15,855 --> 00:24:17,095
I want to live here with you,

383
00:24:17,195 --> 00:24:18,425
Ji Won, Hae Joo, and Hae Sung.

384
00:24:19,125 --> 00:24:21,595
In the land where Father and Mother are buried.

385
00:24:23,895 --> 00:24:25,295
Even if I go to the US,

386
00:24:25,625 --> 00:24:27,195
there's nothing I can do.

387
00:24:29,255 --> 00:24:31,495
Shut it and do as I say. Go back!

388
00:24:43,055 --> 00:24:44,055
Ta-da.

389
00:24:44,595 --> 00:24:47,455
Happy birthday, Father.

390
00:24:47,895 --> 00:24:48,955
This is your gift.

391
00:24:49,795 --> 00:24:51,795
The one and only pair of sneakers in the world...

392
00:24:51,825 --> 00:24:54,325
with a painting from the artist Son Yuh Ri.

393
00:24:57,255 --> 00:24:58,625
Thank you.

394
00:25:00,025 --> 00:25:01,555
Try them on.

395
00:25:01,855 --> 00:25:03,955
You and Moo Yul wear the same size,

396
00:25:04,125 --> 00:25:05,355
so he helped me.

397
00:25:11,295 --> 00:25:13,355
They fit perfectly and they're comfortable.

398
00:25:14,195 --> 00:25:16,325
Naturally. I picked them out after all.

399
00:25:18,595 --> 00:25:20,495
- Happy birthday, Mr. Son. - What?

400
00:25:20,495 --> 00:25:22,625
It's not much. It's a belt.

401
00:25:23,695 --> 00:25:26,255
- Gosh. - Happy birthday.

402
00:25:26,255 --> 00:25:27,725
You too?

403
00:25:29,225 --> 00:25:30,695
Thank you.

404
00:25:30,695 --> 00:25:32,225
You didn't forget, did you?

405
00:25:32,255 --> 00:25:33,655
We're having a birthday party tonight.

406
00:25:33,995 --> 00:25:36,995
Come home early. No drinking allowed.

407
00:25:36,995 --> 00:25:38,555
Goodness. Fine.

408
00:25:39,095 --> 00:25:41,595
I invited one other person.

409
00:25:41,955 --> 00:25:42,955
Is that okay?

410
00:25:43,025 --> 00:25:45,495
The more the merrier.

411
00:25:46,055 --> 00:25:48,395
Is it the person the painting is for?

412
00:25:48,855 --> 00:25:51,595
Yes. I'll see you later.

413
00:25:52,295 --> 00:25:53,595
- Bye. - Bye.

414
00:25:53,725 --> 00:25:56,255
(Letter of Resignation)

415
00:25:58,355 --> 00:25:59,355
What's this?

416
00:26:00,725 --> 00:26:01,955
I'm resigning.

417
00:26:03,355 --> 00:26:04,525
I'm leaving.

418
00:26:05,795 --> 00:26:07,395
Leaving? Where to?

419
00:26:08,955 --> 00:26:11,255
I've decided to move already,

420
00:26:11,355 --> 00:26:12,755
so I've put the house on sale.

421
00:26:13,395 --> 00:26:14,495
I've decided...

422
00:26:15,425 --> 00:26:18,295
to take Yuh Ri and live somewhere no one knows us.

423
00:26:20,125 --> 00:26:21,195
Mr. Son.

424
00:26:21,725 --> 00:26:23,425
How could you do this to us?

425
00:26:23,855 --> 00:26:26,255
Will you really shun Hae Sung?

426
00:26:27,755 --> 00:26:28,955
I'm sorry.

427
00:26:29,755 --> 00:26:32,455
I won't forget your mercy in helping our family...

428
00:26:33,295 --> 00:26:34,655
eat and survive.

429
00:26:34,855 --> 00:26:37,425
You're a father too, aren't you?

430
00:26:37,495 --> 00:26:39,755
You should know how we feel better than anyone.

431
00:26:41,295 --> 00:26:44,225
I'll give you anything you want whether it's money or anything else.

432
00:26:44,355 --> 00:26:45,555
How much do you want?

433
00:26:45,955 --> 00:26:47,755
Just say the amount.

434
00:26:47,855 --> 00:26:49,655
I'll make sure your family is set for three generations.

435
00:26:51,425 --> 00:26:52,495
I won't use Yuh Ri...

436
00:26:53,755 --> 00:26:55,255
to make deals.

437
00:26:55,425 --> 00:26:56,495
Mr. Son!

438
00:26:57,595 --> 00:26:58,795
As a father,

439
00:26:59,525 --> 00:27:01,725
let me say one last thing.

440
00:27:03,925 --> 00:27:04,925
The truth is,

441
00:27:05,895 --> 00:27:07,125
my wife and I...

442
00:27:10,355 --> 00:27:12,755
adopted Yuh Ri.

443
00:27:14,325 --> 00:27:16,395
From the day we brought her home,

444
00:27:17,855 --> 00:27:20,425
I never once thought she wasn't my child.

445
00:27:21,655 --> 00:27:25,195
However, I can't tell her to give a part of her body to someone else.

446
00:27:25,995 --> 00:27:28,125
I can't harm a single hair on her body.

447
00:27:30,595 --> 00:27:31,595
The fact...

448
00:27:32,325 --> 00:27:33,595
that she suffered so much...

449
00:27:35,125 --> 00:27:37,455
emotionally and physically because of her loser father...

450
00:27:38,395 --> 00:27:39,595
is bad enough.

451
00:27:43,195 --> 00:27:45,355
In exchange, I'll find Ms. Hong's child...

452
00:27:46,395 --> 00:27:48,725
as promised before I leave.

453
00:27:51,495 --> 00:27:52,525
Don't bother.

454
00:27:53,995 --> 00:27:54,995
Pardon?

455
00:27:55,655 --> 00:27:58,055
I confirmed with Ji Won.

456
00:27:59,225 --> 00:28:00,325
If the child was alive,

457
00:28:01,725 --> 00:28:04,895
I wanted to see if the child was a bone marrow match with Hae Sung,

458
00:28:07,495 --> 00:28:10,125
but she said she's sure the baby died.

459
00:28:11,325 --> 00:28:12,355
Then...

460
00:28:13,295 --> 00:28:15,025
you looked for that child...

461
00:28:15,795 --> 00:28:17,425
only for Hae Sung...

462
00:28:30,125 --> 00:28:31,125
Hello?

463
00:28:32,695 --> 00:28:35,125
Yes, I'm Son Joo Ho.

464
00:28:35,225 --> 00:28:36,295
Who's this?

465
00:28:38,225 --> 00:28:40,395
I heard the baby died for certain.

466
00:28:42,225 --> 00:28:46,295
It wasn't bad enough that she gave that baby a burn scar.

467
00:28:46,895 --> 00:28:49,895
She left the baby on the cold floor in the dead of winter...

468
00:28:49,955 --> 00:28:51,425
which killed the baby.

469
00:28:51,795 --> 00:28:54,655
So she wailed, punching herself in the chest.

470
00:28:54,855 --> 00:28:56,595
A burn scar?

471
00:28:58,795 --> 00:29:01,625
The baby had a burn scar?

472
00:29:02,555 --> 00:29:05,295
She dozed off while making formula...

473
00:29:05,455 --> 00:29:07,095
and dropped hot water.

474
00:29:07,995 --> 00:29:10,395
The baby's skin was so fragile...

475
00:29:10,725 --> 00:29:13,425
that it created a scar along the neckline...

476
00:29:13,655 --> 00:29:15,695
or shoulder.

477
00:29:16,525 --> 00:29:19,395
She was so upset because of that.

478
00:29:22,925 --> 00:29:24,055
A scar.

479
00:29:26,725 --> 00:29:29,395
But how did I get this scar?

480
00:29:31,095 --> 00:29:34,425
Do you know where she abandoned that baby?

481
00:29:35,595 --> 00:29:38,525
She said it was where she grew up.

482
00:29:40,525 --> 00:29:43,495
Love... Love Orphanage?

483
00:29:44,425 --> 00:29:46,255
That's right. Love Orphanage.

484
00:29:47,425 --> 00:29:48,425
Love...

485
00:30:09,695 --> 00:30:11,595
Hong Ji Won?

486
00:30:17,425 --> 00:30:18,425
It couldn't be.

487
00:30:29,595 --> 00:30:30,725
Hello?

488
00:30:31,125 --> 00:30:34,095
Is that Love Orphanage? Yes.

489
00:30:34,395 --> 00:30:36,955
Where's the director, Mr. Cho Ki Chul?

490
00:30:39,755 --> 00:30:42,495
Mr. Son quit and is moving away?

491
00:30:43,055 --> 00:30:44,655
What were you doing?

492
00:30:45,395 --> 00:30:47,125
Why are you letting him just leave?

493
00:30:47,195 --> 00:30:49,695
You should've broken his legs if necessary to keep him here!

494
00:30:53,495 --> 00:30:55,925
What do I do?

495
00:30:57,795 --> 00:30:59,955
He wouldn't run off like this, would he?

496
00:31:04,725 --> 00:31:07,225
I'll give Mr. Son the building in Nonhyeon-dong.

497
00:31:07,525 --> 00:31:10,755
I'll give him money or whatever else will work.

498
00:31:11,555 --> 00:31:13,225
If that still doesn't work,

499
00:31:14,125 --> 00:31:16,425
I'll break his neck and bring Yuh Ri.

500
00:31:17,955 --> 00:31:19,125
That's right.

501
00:31:19,625 --> 00:31:22,525
Two babies were left at the orphanage that day.

502
00:31:23,125 --> 00:31:24,625
Two babies?

503
00:31:25,255 --> 00:31:27,925
It was the 10th anniversary...

504
00:31:28,495 --> 00:31:31,425
of my becoming director of Love Orphanage,

505
00:31:31,825 --> 00:31:33,355
so I remember clearly.

506
00:31:34,195 --> 00:31:35,955
One of the babies...

507
00:31:36,555 --> 00:31:37,955
froze to death.

508
00:31:38,625 --> 00:31:41,495
Did that baby that died...

509
00:31:41,825 --> 00:31:43,425
have a burn scar?

510
00:31:43,655 --> 00:31:44,855
A burn scar?

511
00:31:46,925 --> 00:31:48,455
I didn't see any.

512
00:31:49,125 --> 00:31:51,625
Anyway, the dead baby's mother...

513
00:31:51,695 --> 00:31:53,955
showed up a few days later...

514
00:31:54,055 --> 00:31:55,655
and cried so much.

515
00:31:56,025 --> 00:31:57,595
The baby's mother came?

516
00:31:58,625 --> 00:32:00,555
Wait. Then...

517
00:32:01,355 --> 00:32:04,825
Ji Won is not the mother of the baby that froze to death?

518
00:32:05,355 --> 00:32:08,425
I don't know the details either,

519
00:32:09,125 --> 00:32:11,495
but I think Ji Won mistakenly thought...

520
00:32:11,755 --> 00:32:14,125
the baby that died was hers.

521
00:32:14,495 --> 00:32:15,625
Later on,

522
00:32:15,955 --> 00:32:19,495
when the real mother showed up, I wanted to clear up the mistake,

523
00:32:20,755 --> 00:32:22,255
but I couldn't reach her.

524
00:32:25,425 --> 00:32:28,695
Then I heard that she got married.

525
00:32:29,795 --> 00:32:31,855
She married into some rich family, I think.

526
00:32:33,625 --> 00:32:36,895
She didn't even know whose baby it was anyway.

527
00:32:37,655 --> 00:32:40,355
I didn't want her to think about it, so I didn't tell her.

528
00:32:41,295 --> 00:32:42,425
Even if she knew,

529
00:32:43,295 --> 00:32:45,125
nothing would've changed anyway.

530
00:32:45,395 --> 00:32:46,525
Mr. Cho.

531
00:32:48,655 --> 00:32:49,695
Just a minute.

532
00:32:58,225 --> 00:32:59,255
That means...

533
00:33:01,955 --> 00:33:03,295
Yuh Ri really is...

534
00:33:07,025 --> 00:33:08,125
Father!

535
00:33:08,955 --> 00:33:11,495
I brought your favorite rice-cake cake.

536
00:33:16,195 --> 00:33:17,525
Is he not home yet?

537
00:33:27,395 --> 00:33:28,655
Who are you?

538
00:33:34,055 --> 00:33:35,795
Madam Hong.

539
00:33:41,055 --> 00:33:42,125
Where's your father?

540
00:33:44,895 --> 00:33:47,825
He hasn't come home yet.

541
00:33:51,055 --> 00:33:53,095
Are you home alone?

542
00:33:54,925 --> 00:33:55,925
Pardon?

543
00:33:56,025 --> 00:33:57,555
Are you all alone?

544
00:34:15,965 --> 00:34:19,055
(Unknown Woman)

545
00:34:19,125 --> 00:34:20,855
- You need to know something. - I don't want to hear it.

546
00:34:21,095 --> 00:34:23,125
Mr. Son isn't picking up. Is he running late?

547
00:34:27,525 --> 00:34:28,965
Yuh Ri is your daughter.

548
00:34:29,425 --> 00:34:32,255
Your daughter that you abandoned at Love Orphanage!

