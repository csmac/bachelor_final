1
00:00:21,920 --> 00:00:28,996
Director:
Screenplay:

2
00:00:29,160 --> 00:00:36,157
Art Direction:

3
00:03:35,520 --> 00:03:41,277
"Do you know, Father -
what happens only on one day
all over the world?"

4
00:08:41,960 --> 00:08:47,830
STOCKINGS

5
00:16:08,480 --> 00:16:12,359
"I insist on an immediate search!"

6
00:16:50,480 --> 00:16:58,160
"... in any case I'm not as easily
swayed by a pair of beautiful eyes,
as my father is!"

7
00:19:10,080 --> 00:19:14,870
"...how could someone
come up with such an idea?"

8
00:19:21,280 --> 00:19:25,068
"I was so desperate and in need - 

9
00:19:31,480 --> 00:19:36,270
...and I once read it in the
newspaper -"

10
00:19:53,120 --> 00:19:59,832
"We are not victims yet officer -
let her run on!"

11
00:20:07,320 --> 00:20:12,110
"As constable I must pursue
the matter further!"

12
00:21:24,760 --> 00:21:31,472
"-there you have the difference
between old pros and an amateur!"

13
00:22:37,840 --> 00:22:39,751
"...a handkerchief!"

14
00:23:19,920 --> 00:23:26,632
"Nave mercy on me - I swear,
it was my first time -"

15
00:23:43,520 --> 00:23:50,232
"I was behind with the rent -
I should be thrown out tomorrow -
onto the streets..."

16
00:23:57,560 --> 00:24:02,350
...and I have such
fear of the streets!"

17
00:24:37,520 --> 00:24:40,080
POLICE STATION

18
00:24:44,200 --> 00:24:50,958
"No, no - let me at
least get my papers..."

19
00:24:54,840 --> 00:24:59,755
"...it's just near here:
Kirchstrasse 11 ...

20
00:25:03,560 --> 00:25:07,394
...you can come up there with me!"

21
00:26:56,160 --> 00:26:59,994
"None of this belongs to me
anymore...

22
00:27:03,680 --> 00:27:07,514
...tomorrow I must be out of here!"

23
00:27:37,600 --> 00:27:39,591
"I am so bad..."

24
00:27:41,160 --> 00:27:44,994
...please, get me a cognac...!"

25
00:28:40,600 --> 00:28:44,434
"Would you also like one..."

26
00:29:07,240 --> 00:29:11,119
"You wanted to get your papers.
Please do so!"

27
00:32:29,920 --> 00:32:34,710
"The excitement was
too much for me... Officer -

28
00:32:37,880 --> 00:32:39,791
...I feel so ill!"

29
00:33:13,200 --> 00:33:18,115
"I will let the ambulance
take you away!"

30
00:34:44,240 --> 00:34:47,118
"You please me� you really do!"

31
00:36:50,120 --> 00:36:53,954
"Come in, your supper
is waiting, my boy!"

32
00:37:07,040 --> 00:37:10,953
"l was with friends
having a beer - "

33
00:38:31,120 --> 00:38:34,954
"What's going on, boy -
is something the matter?

34
00:38:40,160 --> 00:38:43,038
"Is the street work perhaps
too tough for you?"

35
00:41:00,040 --> 00:41:06,354
Identification papers
Albert Holk

36
00:43:01,240 --> 00:43:06,030
"Consul General, here is your
passport and plane ticket to Berlin!"

37
00:49:14,560 --> 00:49:17,393
"There's a letter for you from
Paris - from him!"

38
00:49:51,240 --> 00:49:53,390
"My dearest Else..."

39
00:52:32,840 --> 00:52:37,630
"Let the boy sleep, whilst he
has a free Sunday!"

40
00:56:43,280 --> 00:56:48,070
"l have brought you back
your present, Miss..."

41
00:57:01,400 --> 00:57:05,279
"...or do you believe
you can pay me off?"

42
00:57:15,280 --> 00:57:20,070
"- perhaps because I didn't
take you to where you belong!"

43
00:57:46,280 --> 00:57:49,158
"So was that not meant -"

44
00:58:17,960 --> 00:58:21,794
"I was until yesterday evening
a dutiful constable -

45
00:58:26,440 --> 00:58:28,351
...and now -"

46
01:01:31,880 --> 01:01:35,668
"You must stay with me -
forever - as my wife -"

47
01:02:53,280 --> 01:02:58,070
"Do you even realise what you've
said? - policeman and thief -"

48
01:03:26,680 --> 01:03:33,392
"Thief? Desperate people may
do wrong but that doesn't
make them bad!"

49
01:04:20,360 --> 01:04:26,151
"Go, go! It's better for us both -
l won't see you again!"

50
01:05:09,280 --> 01:05:12,158
"- Won�t you go?"

51
01:05:39,040 --> 01:05:42,874
"Do you truly believe that
l need to thieve?"

52
01:06:21,040 --> 01:06:23,873
"Is this need?!"

53
01:06:33,200 --> 01:06:35,998
"Is this need?!"

54
01:07:06,360 --> 01:07:12,117
"- If the officer still wishes to marry
me � please�"

55
01:08:45,960 --> 01:08:50,750
"Don't give up on me! 
Don't let me down!!"

56
01:09:30,720 --> 01:09:32,631
"Good evening!"

57
01:14:34,360 --> 01:14:40,151
"Leave it mother, our boy knows
what he's doing - he should be
allowed to have some fun!"

58
01:16:13,240 --> 01:16:17,199
"- I have - killed - a person!!"

59
01:19:44,040 --> 01:19:45,951
"Mother -

60
01:19:49,280 --> 01:19:52,158
"...the law's the law!"

61
01:22:50,720 --> 01:22:55,510
"She will testify - it was self-defence -
she will testify!!"

62
01:23:59,720 --> 01:24:03,599
"My name is Else Kramer and l live at
Kirchstrasse 11 ."

63
01:24:17,840 --> 01:24:22,709
"l want to tell you everything: l was at
Bergen the jewellers and..."

64
01:26:04,400 --> 01:26:12,159
"The HoIks can go for the time being,
however it is the authority's decree
that...

65
01:26:15,840 --> 01:26:18,718
...the woman be taken into custody!"

66
01:28:37,920 --> 01:28:40,832
"I'm waiting, dearest one -"

67
01:29:47,000 --> 01:29:55,157
Film Restoration:
Martin Koerber

68
01:29:56,600 --> 01:30:03,950
Intertitles Translation:
Jonathan Lomax
Digital retouching, Restoration,
Remastering and Subtitling:
IML Digital Media Melbourne
IML Digital Media 2005
