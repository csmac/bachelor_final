1
00:00:01,260 --> 00:00:03,296
We're expecting you in Paris.

2
00:00:04,260 --> 00:00:05,170
for the contracts.

3
00:00:11,740 --> 00:00:13,856
We're ready for the meeting.

4
00:00:14,060 --> 00:00:15,937
I'm coming.
Close the door.

5
00:00:30,020 --> 00:00:31,009
I don't know

6
00:00:31,740 --> 00:00:33,537
what's happening with the line.

7
00:00:33,740 --> 00:00:35,412
I can't hear you.

8
00:00:36,300 --> 00:00:37,255
Can you hear me?

9
00:00:42,700 --> 00:00:43,610
I can't hear you.

10
00:00:49,100 --> 00:00:50,419
I'll call you back.

11
00:00:54,460 --> 00:00:56,769
Ok, I'll call you back.

12
00:00:59,940 --> 00:01:02,659
Everything ok?
- Yes, sure.

13
00:01:02,860 --> 00:01:03,576
Where are you going?

14
00:01:16,740 --> 00:01:18,810
See you later then.
- Ok.

15
00:01:26,780 --> 00:01:29,931
Hi.
How is it going here?

16
00:01:30,900 --> 00:01:32,379
Fine.

17
00:01:33,460 --> 00:01:34,893
There's something wrong with your tie.

18
00:01:36,780 --> 00:01:38,213
See you later.

19
00:01:41,820 --> 00:01:42,889
Hello, Sandrine.

20
00:01:43,220 --> 00:01:44,972
Everything ok?
- And you?

21
00:01:45,180 --> 00:01:48,058
How are things with Delacroix?
- Fine.

22
00:01:48,500 --> 00:01:50,730
Will we see each other tonight?

23
00:01:52,420 --> 00:01:54,888
Yes, see you tonight.
- See you then.

24
00:02:03,700 --> 00:02:05,338
Miss...

25
00:02:06,500 --> 00:02:08,377
I need you right away.

26
00:02:48,300 --> 00:02:50,291
A shame I could never
afford that.

27
00:03:09,980 --> 00:03:11,618
Do you want to see my stockings?

28
00:03:20,420 --> 00:03:21,694
Stop it.

29
00:03:22,980 --> 00:03:24,698
You're crazy.

30
00:04:06,260 --> 00:04:07,329
Taxi!

31
00:04:30,020 --> 00:04:33,649
2 months ago I couldn't have known
you'd make me discover life...

32
00:04:34,540 --> 00:04:36,132
at the age of 50.

33
00:04:38,820 --> 00:04:42,369
Every second without you hurts.

34
00:04:42,580 --> 00:04:46,493
Please don't get attached to me.
- It's too late.

35
00:04:47,620 --> 00:04:49,770
It can't last between us...

36
00:04:50,980 --> 00:04:53,699
but I can't think of that
or I start to panic.

37
00:04:55,500 --> 00:04:57,889
The direct line.
Must be your wife.

38
00:05:02,500 --> 00:05:03,296
Hello?

39
00:05:04,420 --> 00:05:06,297
Oh, it's you.

40
00:05:07,140 --> 00:05:10,894
I must have given the loser
a short moment of happiness.

41
00:05:12,420 --> 00:05:13,853
He told me all these

42
00:05:14,060 --> 00:05:17,336
sweet little things that showed
he's in love.

43
00:05:17,540 --> 00:05:21,328
I never experienced it myself.
I started to envy him for it.

44
00:05:21,540 --> 00:05:25,613
I pretended I loved him,
as if I came with him.

45
00:05:25,820 --> 00:05:30,211
Everything. And in the end
I had nothing at all.

46
00:05:30,420 --> 00:05:33,378
Except Nathalie, but she was
no knight in shining armour.

47
00:05:33,900 --> 00:05:36,334
That's why I wanted Christophe.

48
00:05:38,500 --> 00:05:42,209
Do you know the Barnay family well?
- Especially the father.

49
00:05:46,660 --> 00:05:49,572
He's a self-made man,
old school.

50
00:05:49,780 --> 00:05:51,930
With a strong sense of honour

51
00:05:52,140 --> 00:05:55,337
and responsibility.
A dinosaur.

52
00:05:55,860 --> 00:06:00,172
We founded the company together
and overcame a lot of problems.

53
00:06:00,380 --> 00:06:02,894
That created a bond of mutual trust.

54
00:06:03,660 --> 00:06:06,936
I've often been invited to his chateau.
- Chateau?

55
00:06:08,180 --> 00:06:10,853
Can't you take me one day?

56
00:06:11,420 --> 00:06:13,217
You know that's impossible.

57
00:06:15,500 --> 00:06:16,819
And Christophe?

58
00:06:17,140 --> 00:06:20,496
He got the most beautiful women
of the company.

59
00:06:20,700 --> 00:06:21,769
Tell me...

60
00:06:22,820 --> 00:06:26,130
Now that you're here, I'm jealous.
- Silly.

61
00:06:26,460 --> 00:06:30,658
No, really. I'm very afraid of him.
And of his father.

62
00:06:31,620 --> 00:06:35,772
He wants me to keep a discrete eye on him.
- Why?

63
00:06:36,100 --> 00:06:37,818
It goes back more than 20 years.

64
00:06:38,860 --> 00:06:42,216
The old man had to work hard
in his new company.

65
00:06:42,420 --> 00:06:44,934
One day he had to go to Africa
for 2 months.

66
00:06:45,940 --> 00:06:50,695
Little Christophe stayed with his mother
and his sister, who was a baby still.

67
00:06:50,900 --> 00:06:55,530
They had no staff yet, back then.
The mother died suddenly.

68
00:06:56,820 --> 00:06:59,095
A heart attack. Totally unexpected.

69
00:07:00,100 --> 00:07:04,298
Little Christophe was unshaken and
stayed with his dead mother

70
00:07:05,180 --> 00:07:08,172
for almost two weeks,
without telling anybody.

71
00:07:08,380 --> 00:07:10,655
Not even his father over the phone.

72
00:07:11,420 --> 00:07:13,980
The body started to decompose.

73
00:07:14,180 --> 00:07:17,217
The boy only moved
to feed his little sister.

74
00:07:17,820 --> 00:07:19,048
How old was he?

75
00:07:19,380 --> 00:07:20,529
Nine.

76
00:07:21,340 --> 00:07:24,252
There were no consequences,
he was doing fine even.

77
00:07:25,740 --> 00:07:28,459
Except, he wasn't afraid
of anything or anybody anymore.

78
00:07:28,660 --> 00:07:31,652
He said: "We're just a piece of wood."

79
00:07:31,860 --> 00:07:34,738
He kept saying he wanted
to attack the sun,

80
00:07:34,940 --> 00:07:38,250
take it away from the earth
or set the world to fire with it.

81
00:07:38,460 --> 00:07:41,293
he also said: "That would be criminal."

82
00:07:41,500 --> 00:07:45,459
Or "Would that make
me as evil as God?"

83
00:07:45,820 --> 00:07:47,219
Boy talk.

84
00:07:47,420 --> 00:07:50,856
Except he still said it when he was 19
and when he was 25.

85
00:07:51,660 --> 00:07:55,733
And he defied death,
in the air, with his car.

86
00:07:55,940 --> 00:07:58,408
He manipulated people for fun

87
00:07:59,420 --> 00:08:03,379
and he hung out with dangerous types,
to see whether they would kill him.

88
00:08:04,100 --> 00:08:05,931
He pushed women to suicide.

89
00:08:06,140 --> 00:08:08,574
Gossip.
- No.

90
00:08:08,900 --> 00:08:11,255
He seduced a number of employees.

91
00:08:11,900 --> 00:08:15,256
They say he tortured
and hurt them to the point

92
00:08:15,460 --> 00:08:17,769
that two of them poured petrol over themselves

93
00:08:18,380 --> 00:08:19,972
and set themselves on fire.

94
00:08:20,340 --> 00:08:22,649
They buried alive in front of him.

95
00:08:23,380 --> 00:08:25,098
Do you think he's mad?

96
00:08:25,700 --> 00:08:27,611
He knows perfectly well what he's doing.

97
00:08:27,980 --> 00:08:30,369
It's the women who are crazy.

98
00:08:30,820 --> 00:08:34,779
Crazy about him. Especially if they know
about their predecessors.

99
00:08:35,860 --> 00:08:37,532
I wanted to fire him

100
00:08:37,860 --> 00:08:40,579
or at least put him out of action.
But that's impossible.

101
00:08:40,780 --> 00:08:45,092
His father feels so guilty
he forgives him for everything.

102
00:08:45,300 --> 00:08:49,339
Maybe, after his death, he'll give me
some control over his son.

103
00:08:49,540 --> 00:08:51,098
That would be good.

104
00:08:51,300 --> 00:08:54,212
Thanks to his relations,
Christophe's untouchable.

105
00:08:54,580 --> 00:08:58,289
If he had the power,
he'd be terrifying.

106
00:08:59,700 --> 00:09:02,498
I've been wanting to tell you for a while...

107
00:09:02,700 --> 00:09:05,578
Why don't you get a replacement
for Mrs. Mercier?

108
00:09:07,100 --> 00:09:09,489
But I'm constantly drowning in work.

109
00:09:09,700 --> 00:09:12,692
They'd be on to us soon
and they'd start gossiping.

110
00:09:12,900 --> 00:09:15,698
Not at all.
I have a friend who'd be suitable.

111
00:09:16,100 --> 00:09:17,419
Please don't insist.

112
00:10:24,060 --> 00:10:26,699
leave me alone.
Don't touch me.

113
00:10:32,140 --> 00:10:32,936
Give it.

114
00:10:36,540 --> 00:10:37,814
You didn't want to take the whole bottle, did you?

115
00:10:41,060 --> 00:10:42,539
What's wrong?

116
00:10:44,300 --> 00:10:47,133
Please... tell me.

117
00:10:48,420 --> 00:10:49,614
Come on.

118
00:11:03,020 --> 00:11:04,692
You don't want to tell me?

119
00:11:07,340 --> 00:11:09,376
You should talk about it. It relieves.

120
00:11:12,020 --> 00:11:14,739
You act like a girl in love
who was rejected.

121
00:11:18,300 --> 00:11:20,609
Was it a man who wasn't available?

122
00:11:21,380 --> 00:11:22,893
We're all the same.

123
00:11:24,300 --> 00:11:25,938
You can't do anything for me.

124
00:11:27,020 --> 00:11:28,738
It's nothing anyway.

125
00:11:29,540 --> 00:11:30,939
I was just dreaming.

126
00:11:32,340 --> 00:11:34,012
It'll pass.

127
00:11:38,660 --> 00:11:40,696
All that counts...

128
00:11:42,220 --> 00:11:45,053
is that we pursue our plan.

129
00:11:45,780 --> 00:11:47,452
You've got Delacroix.

130
00:11:47,860 --> 00:11:51,330
Convince him to replace Mrs. Mercier
with me.

131
00:11:52,740 --> 00:11:54,856
But never forget my advice.

132
00:11:56,780 --> 00:11:59,772
Don't fall in love,
stick with comedy.

133
00:11:59,980 --> 00:12:01,459
That doesn't hurt as much.

134
00:13:02,260 --> 00:13:05,093
Yes?
- Mr. Delacroix? The personnel department.

135
00:13:05,300 --> 00:13:09,293
Miss Tessier reported in sick
and will be absent for at least a week.

136
00:13:09,500 --> 00:13:12,412
What's wrong with her?
- Stress from over fatigue.

137
00:15:20,460 --> 00:15:22,052
Mr. Delacroix?

138
00:15:24,900 --> 00:15:27,619
Mr. Delacroix,
the signatures please.

139
00:15:28,100 --> 00:15:29,818
Come back in an hour.

140
00:15:30,020 --> 00:15:33,057
It's urgent, sir.
- Please.

141
00:15:57,300 --> 00:15:58,528
Come in.

142
00:16:04,940 --> 00:16:06,737
I'd rather talk to you alone.

143
00:16:06,940 --> 00:16:09,977
Why?
Nathalie can hear everything.

144
00:16:18,940 --> 00:16:21,215
Are you in love with her?
- No.

145
00:16:23,540 --> 00:16:27,135
What then?
- We both felt like it.

146
00:16:27,780 --> 00:16:29,452
Isn't it the same with you?

147
00:16:30,380 --> 00:16:33,611
That was not the same.
- It was.

148
00:16:38,180 --> 00:16:39,533
Why did you do that to me?

149
00:16:42,620 --> 00:16:45,088
I didn't want to see you again,
but it's not possible.

150
00:16:47,820 --> 00:16:49,697
I wanted to kill you the other day.

151
00:16:51,180 --> 00:16:52,818
I was in such pain I wanted to strangle you.

152
00:16:53,740 --> 00:16:55,651
I'm not your daughter or your wife.

153
00:16:58,820 --> 00:17:00,219
But I am your slave.

154
00:17:04,820 --> 00:17:06,776
I'm in the office 13 hours a day.

155
00:17:07,100 --> 00:17:09,216
Most evenings I'm with you.

156
00:17:09,900 --> 00:17:12,653
And also on Saturdays and Sundays now.

157
00:17:14,260 --> 00:17:15,852
And I have to be beautiful.

158
00:17:17,780 --> 00:17:21,295
Beauty parlour, shopping,
the laundry on Saturdays.

159
00:17:22,060 --> 00:17:24,016
But you refuse to help me.

160
00:17:24,740 --> 00:17:26,571
Do you want me to leave my wife?

161
00:17:28,260 --> 00:17:31,172
Do you want us to live together?
- No.

162
00:17:32,100 --> 00:17:34,853
I only want a bit less work.

163
00:17:35,060 --> 00:17:37,779
And for discretion's sake,
I want it to be Nathalie.

164
00:17:38,860 --> 00:17:41,818
And I look the other way
when you two make love in the office?

165
00:17:48,340 --> 00:17:49,773
I'm free.

166
00:17:55,860 --> 00:17:57,657
When will you come to work again?

167
00:17:59,620 --> 00:18:01,178
Maybe never.

168
00:18:01,580 --> 00:18:04,492
What does it depend on?
- On you.

169
00:18:22,540 --> 00:18:24,053
Sandrine?

170
00:18:24,940 --> 00:18:28,410
I've been selfish.
When can your friend start?

171
00:20:00,340 --> 00:20:02,774
The man entered hell.

172
00:20:41,180 --> 00:20:45,617
Beat it, sister.
I'll see you at home tonight.

173
00:21:06,500 --> 00:21:08,331
Come along, please.

174
00:21:09,620 --> 00:21:12,578
Please wait here ladies.
Mr. Delacroix.

175
00:21:30,020 --> 00:21:31,248
Sit down.

176
00:21:47,660 --> 00:21:49,776
True enjoyment is rare.

177
00:21:50,820 --> 00:21:53,493
Many women or people
will never know it.

178
00:21:54,460 --> 00:21:55,449
It's a drug.

179
00:21:56,660 --> 00:21:59,891
A she-devil that grabs you
and then keeps escaping.

180
00:22:00,100 --> 00:22:01,738
You'll only live for her.

181
00:22:03,100 --> 00:22:04,453
Not?

182
00:22:06,260 --> 00:22:09,536
Your relationship with your wife and family
is none of my business.

183
00:22:10,740 --> 00:22:12,810
In a way, I envy you.

184
00:22:14,660 --> 00:22:17,015
Except, you work for the Barnay company.

185
00:22:17,340 --> 00:22:22,095
You carry a huge responsibility.
Suppose everybody acted like you do.

186
00:22:23,300 --> 00:22:28,499
The company would be one big fucking palace.
It wouldn't function anymore.

187
00:22:29,140 --> 00:22:30,129
Unfortunately.

188
00:22:32,140 --> 00:22:34,096
You should have been an example.

189
00:22:35,860 --> 00:22:38,977
Am I saying things
you disagree with?

190
00:22:42,020 --> 00:22:43,294
Well then.

191
00:22:47,540 --> 00:22:49,849
This document states
what you did

192
00:22:50,180 --> 00:22:52,455
with your two subordinates.
Sign it.

193
00:22:52,660 --> 00:22:57,097
You also accept your dismissal
as a result of grave professional misconduct,

194
00:22:57,740 --> 00:22:59,139
so without severance pay.

195
00:23:00,300 --> 00:23:03,337
I'm being sacked.
- No.

196
00:23:03,980 --> 00:23:06,210
I'll keep my mouth shut about the affair.

197
00:23:07,020 --> 00:23:08,897
I'm placing you under surveillance.

198
00:23:09,940 --> 00:23:13,330
Under surveillance of an assistant
of my choice, of course.

199
00:23:14,540 --> 00:23:15,734
Right.

200
00:23:16,540 --> 00:23:18,212
I'm expecting your signature.

201
00:23:33,340 --> 00:23:34,329
Thank you.

202
00:23:55,380 --> 00:23:56,779
Poor loser.

203
00:23:57,300 --> 00:23:58,369
What are you doing to him?

204
00:23:59,340 --> 00:24:00,819
Did he pressure you?

205
00:24:01,420 --> 00:24:04,856
Did he abuse his authority?
- No.

206
00:24:06,220 --> 00:24:09,417
So you don't choose hypocrisy
as your defence.

207
00:24:09,620 --> 00:24:10,848
Should I have?

208
00:24:11,220 --> 00:24:15,179
Useful and socially acceptable vices
soon become virtues.

209
00:24:17,260 --> 00:24:18,852
We appreciate the same truths.

210
00:24:23,300 --> 00:24:27,691
First I thought he wanted to show off
his wealth. I thought it was vulgar.

211
00:24:28,620 --> 00:24:31,657
But he was handsome.
I longed for him

212
00:24:31,980 --> 00:24:33,493
and could forgive him anything.

213
00:24:33,700 --> 00:24:34,894
Money.

214
00:24:35,980 --> 00:24:38,335
It rules our dreams and desires...

215
00:24:40,140 --> 00:24:41,573
determines the size of our prisons.

216
00:24:44,300 --> 00:24:48,373
See ladies, just like you, I'm
going to have a taste of true freedom.

217
00:24:49,700 --> 00:24:52,168
Dare to play with the social order...

218
00:24:53,140 --> 00:24:55,335
whether it's just or not

219
00:24:55,540 --> 00:24:58,213
or of divine or human origin.

220
00:24:58,420 --> 00:25:00,138
I reject the rules...

221
00:25:01,060 --> 00:25:02,459
the straitjacket.

222
00:25:03,100 --> 00:25:04,852
I propose to you a game.

223
00:25:06,260 --> 00:25:09,730
Nathalie, you go work with Delacroix again.
- Do you know each other?

224
00:25:10,060 --> 00:25:11,095
We're lovers.

225
00:25:13,300 --> 00:25:14,528
Well, sometimes.

226
00:25:15,620 --> 00:25:17,531
Sandrine, I know all the employees.

227
00:25:17,860 --> 00:25:21,489
I have a confidential file
about everybody.

228
00:25:21,700 --> 00:25:25,488
And I'm often looking for distraction
one way or another

229
00:25:25,820 --> 00:25:27,333
with one person or the next.

230
00:25:28,380 --> 00:25:32,737
Nathalie has slowly replaced Delacroix.
Ok, not officially.

231
00:25:33,100 --> 00:25:35,375
He's still my technical advisor.

232
00:25:36,060 --> 00:25:37,379
And, in secret, she's

233
00:25:37,980 --> 00:25:39,174
my eyes,

234
00:25:39,620 --> 00:25:41,133
my ears,

235
00:25:41,740 --> 00:25:44,379
my mouth.
- Also with me?

236
00:25:45,020 --> 00:25:47,090
Miss Sandrine Tessier...

237
00:25:48,260 --> 00:25:49,693
it's my honour to ask for your hand...

238
00:25:52,380 --> 00:25:54,769
for a temporary commitment.
A game.

239
00:25:55,500 --> 00:25:56,615
What kind of game?

240
00:25:56,940 --> 00:26:00,535
Maybe the game of chance
or love.

241
00:26:01,020 --> 00:26:02,055
A new form of entertainment.

242
00:26:02,500 --> 00:26:03,933
Who will say?

243
00:26:04,660 --> 00:26:08,539
For just a few months.
A game with a useful side to it.

244
00:26:10,700 --> 00:26:12,531
My father will die soon.

245
00:26:13,900 --> 00:26:15,731
And he's still hesitant

246
00:26:15,940 --> 00:26:19,376
to trust me with his entire
empire.

247
00:26:20,500 --> 00:26:26,018
He's heard rumours
saying I'm too whimsical,

248
00:26:26,780 --> 00:26:30,978
too careless with people to be entrusted
with a large employee base.

249
00:26:31,420 --> 00:26:33,092
But I want that power.

250
00:26:34,180 --> 00:26:37,968
Because debauchery isn't possible
without money and protection.

251
00:26:38,820 --> 00:26:41,937
"Depending whether thou
art mighty or poor..."

252
00:26:43,620 --> 00:26:44,848
I disabled Delacroix.

253
00:26:45,420 --> 00:26:47,888
My potential rival and guard dog.

254
00:26:48,580 --> 00:26:50,059
But I'm not taking any risks.

255
00:26:51,420 --> 00:26:55,129
Before my father dies, I want to
make him think I gained respectability

256
00:26:55,340 --> 00:26:58,332
and seriousness as
a result of love.

257
00:26:58,700 --> 00:27:02,010
This, of course, is a deal
you'll accept...

258
00:27:02,820 --> 00:27:06,529
like the donkey, lured by the carrot
and threatened by the stick.

259
00:27:06,740 --> 00:27:07,729
And what's the stick?

260
00:27:08,060 --> 00:27:11,257
Immediate dismissal
and a public scandal.

261
00:27:12,260 --> 00:27:14,330
And the carrot: I marry you.

262
00:27:14,540 --> 00:27:16,690
for the law and for the church.

263
00:27:17,700 --> 00:27:20,134
I insist on the church and on
community of property.

264
00:27:20,620 --> 00:27:23,896
My father could be suspicious.
And not without reason.

265
00:27:25,860 --> 00:27:30,411
The religious side
plus participation in my fortune.

266
00:27:30,620 --> 00:27:34,135
will be a firm foundation of our
short lived amour fou.

267
00:27:35,220 --> 00:27:38,610
Of course, in a couple of months
we'll divorce.

268
00:27:38,820 --> 00:27:41,539
and you'll receive a generous allowance.

269
00:27:41,740 --> 00:27:43,219
But we remain married in the eyes of the church.

270
00:27:44,780 --> 00:27:48,136
I'll patiently await God's punishment.

271
00:27:52,420 --> 00:27:53,773
Why me and not Nathalie?

272
00:27:55,100 --> 00:27:59,252
Nathalie's more useful at the office,
to keep an eye on Delacroix.

273
00:27:59,740 --> 00:28:03,016
And you're a likelier candidate
as the young woman

274
00:28:03,340 --> 00:28:07,094
from a working class background,
innocent, refreshing and lively.

275
00:28:07,740 --> 00:28:09,856
Good for my image
as a dynamic employer.

276
00:28:11,300 --> 00:28:13,450
Shall we start our game now?

277
00:28:15,020 --> 00:28:16,692
You go to the ladies' bathrooms...

278
00:28:17,540 --> 00:28:20,008
you get into a cubicle


279
00:28:21,260 --> 00:28:24,491
and you start making love.
I'll join you in 5 minutes.

280
00:28:28,100 --> 00:28:29,010
Wait for me before finishing.

281
00:29:49,140 --> 00:29:51,290
And, Sandrine,
do you accept my game?

282
00:29:53,220 --> 00:29:56,895
Will you continue your brilliant social rise
or end it?

283
00:30:15,340 --> 00:30:17,729
Then, for the first time
I came with a man.

284
00:30:18,260 --> 00:30:22,492
But I'm sure
he hadn't reached his climax yet.

285
00:30:40,540 --> 00:30:42,496
Have you been lovers for long?

286
00:30:43,180 --> 00:30:45,296
From when we started at Barnay's?

287
00:30:47,300 --> 00:30:50,133
He knew things
that could only come from you.

288
00:30:51,660 --> 00:30:55,778
He knew were he could catch us
with poor Delacroix.

289
00:30:57,180 --> 00:31:00,217
Your mysterious lover
was Christophe.

290
00:31:01,300 --> 00:31:04,372
Now I understand why you
warned me about him.

291
00:31:05,340 --> 00:31:06,409
Shall I tell you something?

292
00:31:07,380 --> 00:31:09,894
While you were manipulating Delacroix.

293
00:31:10,420 --> 00:31:12,854
Christophe was manipulating me.

294
00:31:13,220 --> 00:31:15,495
I thought I'd made him
fall in love.

295
00:31:16,700 --> 00:31:19,009
How could I be that stupid?

296
00:31:19,460 --> 00:31:22,213
Even though I wasn't naive anymore.

297
00:31:23,220 --> 00:31:28,692
Well, he's stronger than you,
than me, then the two of us together.

298
00:31:29,260 --> 00:31:30,693
He's had so many women,

299
00:31:31,020 --> 00:31:33,454
so many people,
in bed or somewhere else

300
00:31:33,660 --> 00:31:35,491
and in each possible combination

301
00:31:35,820 --> 00:31:40,371
that love nor any other form of normal
sexuality can hold his interest.

302
00:31:41,060 --> 00:31:43,813
And the tragic thing is that he loves enjoyment.

303
00:31:44,860 --> 00:31:46,339
Do you know why he needs you?

304
00:31:46,900 --> 00:31:51,849
Like he said: for the enjoyment of
playing with others on a large scale.

305
00:31:52,180 --> 00:31:56,856
He already pushed women to commit suicide,
he'll be able to destroy many more.

306
00:31:58,340 --> 00:31:59,819
And I cheat...

307
00:32:01,060 --> 00:32:02,652
I lie to everybody.

308
00:32:03,700 --> 00:32:05,930
He leads me in everything.

309
00:32:10,220 --> 00:32:12,939
I actually behave like a slave.

310
00:32:14,860 --> 00:32:17,932
A slave who even enjoys it,
out of love.

311
00:32:19,340 --> 00:32:21,729
You're talking nonsense.
Like a crazy woman.

312
00:32:24,260 --> 00:32:26,728
Listen to what the crazy woman
reads in your little head.

313
00:32:28,100 --> 00:32:30,170
You secretly hope he loves you.

314
00:32:30,900 --> 00:32:33,209
Sometimes you even believe it.

315
00:32:33,860 --> 00:32:35,498
You think that, at the very worst,

316
00:32:36,140 --> 00:32:40,213
you keep your chances open
because you stay in the picture.

317
00:32:41,020 --> 00:32:44,171
Well, you're dreaming, girl.

318
00:32:44,460 --> 00:32:47,338
When you're no longer required and all used up,

319
00:32:47,820 --> 00:32:50,414
he'll dump you like an old rag.

320
00:32:50,740 --> 00:32:52,970
Why make such a scene about
a staged wedding?

321
00:32:53,180 --> 00:32:54,898
I never had anything in my life.

322
00:32:55,660 --> 00:32:58,732
Nothing and nobody.

323
00:32:58,940 --> 00:33:01,408
You don't love him.
He's mine.

324
00:33:01,620 --> 00:33:03,815
I won't let you or anybody else
take him from me.

325
00:33:18,260 --> 00:33:19,978
Christophe had transferred me.

326
00:33:20,180 --> 00:33:22,978
I now had my own office
at the Champs Elys�es.

327
00:33:25,780 --> 00:33:26,974
Sandrine?

328
00:33:30,820 --> 00:33:32,617
Can I talk to you?

329
00:33:36,660 --> 00:33:38,093
Are you ok?

330
00:33:43,900 --> 00:33:47,290
I wanted to tell you
that everything's over between us.

331
00:33:47,500 --> 00:33:49,456
And it's better that way.

332
00:33:49,660 --> 00:33:52,697
But out of everything that happened,
I'll remember one thing.

333
00:33:53,300 --> 00:33:56,849
You gave me some of the only moments
of real happiness in my life,

334
00:33:57,380 --> 00:33:59,098
of intense passion,

335
00:33:59,660 --> 00:34:01,093
of pain,

336
00:34:01,420 --> 00:34:03,331
but also of great tenderness.

337
00:34:04,780 --> 00:34:07,169
I will always keep you in my heart.

338
00:34:08,660 --> 00:34:12,812
That's what I wanted to say.
The rest doesn't matter.

339
00:34:13,100 --> 00:34:16,297
Take good care of yourself, Sandrine.

340
00:34:16,860 --> 00:34:18,134
Goodbye.

341
00:34:25,540 --> 00:34:27,656
This man wasn't after revenge.

342
00:34:28,380 --> 00:34:31,338
I only realised later that he loved me...

343
00:34:32,700 --> 00:34:35,134
with an intensity I couldn't comprehend,

344
00:34:35,340 --> 00:34:38,332
without expecting anything in return,
not even my presence.

345
00:34:40,420 --> 00:34:43,856
I should have been very touched,
but back then I only thought of him as a weak man.

346
00:34:45,980 --> 00:34:50,258
Besides, I was pining for a phone call
from Christophe. And then...

347
00:34:53,780 --> 00:34:58,774
Make yourself very beautiful tonight.
Charlotte will pick you up at midnight.

348
00:35:10,860 --> 00:35:14,250
You'll become my sister-in-law.
The only one before God.

349
00:35:14,580 --> 00:35:18,050
For a very short time.
- A day, a month, a year.

350
00:35:18,420 --> 00:35:20,650
What's that in the face of eternity?

351
00:35:22,700 --> 00:35:23,769
My new sister.

352
00:35:24,820 --> 00:35:25,855
Welcome among us.

353
00:35:29,220 --> 00:35:31,654
I only ask one thing of you.
Be who you are.

354
00:35:33,700 --> 00:35:36,009
We're born in a world of lies.

355
00:35:37,020 --> 00:35:39,011
Be nice to us,
genuine...

356
00:35:40,940 --> 00:35:42,089
and never lie to us.

357
00:35:44,340 --> 00:35:46,854
Christophe and I
always speak the truth.

358
00:35:47,340 --> 00:35:51,413
Especially when the truth hurts
or burns.

359
00:35:53,100 --> 00:35:57,651
Let the party begin.
And may we all enjoy ourselves.

360
00:36:08,980 --> 00:36:10,493
Charlotte, get up.

361
00:36:15,220 --> 00:36:16,858
Sandrine, you too.

362
00:36:57,300 --> 00:36:59,450
Sandrine, stand against her back.

363
00:37:02,580 --> 00:37:04,377
Caress her neck.

364
00:37:08,540 --> 00:37:09,768
Her arms.

365
00:37:14,300 --> 00:37:15,619
Her breasts.

366
00:37:19,740 --> 00:37:22,573
Everything I do to you,
you should do to Charlotte.

367
00:37:25,340 --> 00:37:26,932
I open your dress.

368
00:37:34,020 --> 00:37:35,612
I take it off.

369
00:37:43,620 --> 00:37:45,258
I caress your breasts...

370
00:37:50,540 --> 00:37:52,019
your belly...

371
00:37:58,220 --> 00:38:00,051
the inside of your thighs.

372
00:39:18,380 --> 00:39:20,177
How dare you spoil my pleasure?

373
00:39:21,500 --> 00:39:24,617
Why do you choose them?
I can still pleasure you the way you taught me.

374
00:39:25,060 --> 00:39:26,778
Why do you reject me?

375
00:39:29,220 --> 00:39:30,255
Where did you get the key?

376
00:39:34,420 --> 00:39:37,730
I'm from another race than all the women
you seduced and rejected.

377
00:39:38,860 --> 00:39:41,090
I'll never commit suicide.
I won't give you the satisfaction.

378
00:39:43,380 --> 00:39:44,574
Never.

379
00:39:46,460 --> 00:39:48,018
But you're nothing anymore.

380
00:39:49,020 --> 00:39:50,976
Tomorrow, the lock will be changed.

381
00:39:52,900 --> 00:39:56,529
Today, I see you for the last time.
And give back that key.

382
00:39:56,740 --> 00:39:58,298
Come and get it.

383
00:39:59,700 --> 00:40:01,099
I repeat.

384
00:40:01,540 --> 00:40:04,498
I'm stronger than all those you rejected.
- Give it back!

385
00:40:04,820 --> 00:40:05,730
Come and get it.

386
00:40:08,460 --> 00:40:09,813
Hit me.

387
00:40:10,020 --> 00:40:12,932
You taught me how pain
can lead to pleasure.

388
00:40:29,620 --> 00:40:31,292
I never want to see you again.

389
00:40:34,420 --> 00:40:36,570
Can't you see I'm suffering?

390
00:40:37,140 --> 00:40:38,050
That means you're alive.

391
00:40:40,140 --> 00:40:41,334
Unfortunately.

392
00:40:49,140 --> 00:40:50,892
But creatures like you are like dogs.

393
00:40:51,580 --> 00:40:53,855
Unfortunately, I need you at the office.

394
00:40:54,660 --> 00:40:57,618
But as soon as my father dies,
you disappear.

395
00:40:58,820 --> 00:41:00,412
For me you already don't exist anymore.

396
00:41:05,340 --> 00:41:08,889
I'll give you a new party
that's worthy of you. I promise.

397
00:41:16,100 --> 00:41:18,853
I can still help him
and protect him.

398
00:41:19,060 --> 00:41:22,530
You'll only be hated more.

399
00:41:22,740 --> 00:41:24,173
Which means he loves me.

400
00:41:28,300 --> 00:41:30,370
I have no more pride or dignity.

401
00:41:30,700 --> 00:41:32,053
I've got nothing left.

402
00:41:33,180 --> 00:41:35,455
Give me the money
you owe me.

403
00:41:36,420 --> 00:41:39,014
The next day
I went to get my savings.

404
00:41:39,220 --> 00:41:42,098
I gave her everything I had,
which wasn't much.

405
00:41:43,580 --> 00:41:45,969
She took the notes
and sat on the ground.

406
00:41:46,180 --> 00:41:48,978
She counted them en counted them again.

407
00:41:51,100 --> 00:41:52,818
I no longer existed.

408
00:41:53,380 --> 00:41:56,338
She sat there
on the floor of her apartment,

409
00:41:56,540 --> 00:41:58,770
looking almost ugly
with her eyes closed tight.

410
00:42:00,020 --> 00:42:03,456
She counted and recounted difficult things
using her fingers.

411
00:42:05,180 --> 00:42:06,772
I was never more touched by her than then.

412
00:42:09,300 --> 00:42:13,293
For the first time, without wanting it,
I had the heart of a 15 year old girl.

413
00:42:13,860 --> 00:42:17,739
I couldn't believe it.
My husband was so beautiful.

414
00:42:17,940 --> 00:42:20,170
And I was wearing my white wedding dress.

415
00:42:20,380 --> 00:42:23,736
Christophe had chosen the tactics
of the surprise wedding.

416
00:42:24,460 --> 00:42:27,452
I was only going to be introduced
to father Barnay the next day.

417
00:42:27,660 --> 00:42:29,298
And then there was Nathalie.

418
00:42:29,940 --> 00:42:32,374
Christophe had gotten her
a restraining order.

419
00:42:33,900 --> 00:42:36,289
When I came outside, I suddenly saw her.

420
00:42:38,460 --> 00:42:40,416
Our eyes met.

421
00:42:40,740 --> 00:42:42,139
She hated me.

422
00:42:48,140 --> 00:42:51,291
Admire this beautiful symbol
of our worldly lives.

423
00:43:05,940 --> 00:43:07,737
"And I am the death of everything

424
00:43:08,380 --> 00:43:11,019
and I am the birth of everything.

425
00:43:11,220 --> 00:43:14,815
Word and memory,
stability and mercifulness

426
00:43:15,020 --> 00:43:17,215
and the silence of the secret things."

427
00:44:25,060 --> 00:44:28,018
Sir, your father just passed away.

428
00:44:35,500 --> 00:44:37,092
Our father is no longer alive.

429
00:44:39,180 --> 00:44:40,295
He just passed away.

430
00:44:44,860 --> 00:44:47,772
He left me as only president
of the group.

431
00:44:48,380 --> 00:44:50,450
Isn't that a beautiful coincidence?

432
00:44:51,580 --> 00:44:52,899
The president is dead!

433
00:44:53,740 --> 00:44:57,289
Long live the president.
And tonight

434
00:44:57,620 --> 00:45:00,453
more than  all other nights:
Love each other.

435
00:45:08,780 --> 00:45:09,690
Oh, my sister...

436
00:45:12,620 --> 00:45:15,088
"You cry for people
you shouldn't cry for.

437
00:45:16,220 --> 00:45:19,610
A wise man doesn't mourn
the living or the dead,

438
00:45:20,220 --> 00:45:21,938
since we have never been,

439
00:45:22,140 --> 00:45:27,009
me... nor you...
nor these kings...

440
00:45:28,140 --> 00:45:30,017
and never shall we
cease to be."

441
00:45:43,540 --> 00:45:46,771
Finally we enter
the kingdom of princes,

442
00:45:46,980 --> 00:45:49,733
pharaohs and gods.

443
00:47:00,300 --> 00:47:02,177
Please, stop!

444
00:47:36,140 --> 00:47:38,574
You had no right to treat me this way.

445
00:47:38,940 --> 00:47:42,410
Am I more cruel than life
and creation?

446
00:47:44,420 --> 00:47:46,809
More cruel then the Creator,
if He exists?

447
00:47:48,300 --> 00:47:51,975
Have you watched the violence of death
on the faces of the creatures?

448
00:47:52,860 --> 00:47:55,420
Alive we are like chained gods...

449
00:47:56,180 --> 00:48:00,139
most of the time too cowardly
to taste real freedom.

450
00:48:00,980 --> 00:48:02,857
Vanity of vanities.

451
00:48:03,900 --> 00:48:05,811
Only death is the big cleansing.

452
00:48:07,300 --> 00:48:11,213
You might have helped me
to skilfully plan

453
00:48:11,420 --> 00:48:13,650
the fate of man.

454
00:48:14,180 --> 00:48:16,410
You could even have brought me the sun.

455
00:48:16,820 --> 00:48:18,333
A shame.

456
00:48:37,980 --> 00:48:41,177
Once the divorce is settled,
you're free.

457
00:48:42,060 --> 00:48:43,812
It will take a couple of weeks.

458
00:49:00,420 --> 00:49:01,739
You again?

459
00:49:02,580 --> 00:49:03,854
Leave us alone.

460
00:49:09,140 --> 00:49:10,573
Look at me.

461
00:49:32,660 --> 00:49:35,857
What's the use?
You don't exist anymore.

462
00:50:01,540 --> 00:50:03,610
No, don't do that.

463
00:50:06,100 --> 00:50:08,091
I'd see you again up there.

464
00:50:10,380 --> 00:50:13,213
To aggravate us, me,

465
00:50:14,460 --> 00:50:17,213
the devil and all my other fellow players.

466
00:50:17,900 --> 00:50:21,529
To endure you until eternity...

467
00:50:21,860 --> 00:50:23,054
How horrible.

468
00:52:21,020 --> 00:52:26,094
The police came.
Nathalie was cuffed and taken away.

469
00:52:27,260 --> 00:52:29,376
She never answered the question
everybody asked:

470
00:52:29,700 --> 00:52:31,292
Why?

471
00:52:32,820 --> 00:52:34,572
The media got on to the case,

472
00:52:34,780 --> 00:52:37,294
but the essential truth
remained carefully hidden.

473
00:52:38,340 --> 00:52:42,777
Nathalie was sentenced to years in jail
and the case was forgotten.

474
00:52:44,580 --> 00:52:47,777
Married in community of property
and widowed so quickly,

475
00:52:48,540 --> 00:52:52,931
I automatically became the heiress
of my legal husband.

476
00:52:53,540 --> 00:52:57,579
With Charlotte I ruled the empire
here brother had wanted so badly,

477
00:52:57,780 --> 00:53:00,931
with the expert assistance of Delacroix,

478
00:53:01,140 --> 00:53:04,177
who proved to be a discrete
and loyal employee.

479
00:53:04,380 --> 00:53:05,972
Life went on.

480
00:53:07,020 --> 00:53:09,454
After her sentence, Nathalie married

481
00:53:10,500 --> 00:53:11,899
her prison guard.

482
00:53:12,900 --> 00:53:16,051
Of the three of us, she's the only one
who really married.

483
00:53:18,460 --> 00:53:21,258
Years later,
I happened to see her.

484
00:53:23,380 --> 00:53:24,779
There she was...

485
00:53:25,380 --> 00:53:26,733
with her husband and child.

486
00:53:30,140 --> 00:53:31,255
They all looked nice.

487
00:53:34,700 --> 00:53:36,531
After a while we approached
each other.

488
00:53:37,860 --> 00:53:39,339
We looked at each other.

489
00:53:39,940 --> 00:53:40,975
In silence.

490
00:53:44,500 --> 00:53:45,933
I only saw her.

491
00:53:50,380 --> 00:53:52,416
Then we had to go our ways.

492
00:53:53,540 --> 00:53:55,451
We kept looking at each other...

493
00:53:56,580 --> 00:53:58,855
knowing it was the last time.

494
00:54:00,660 --> 00:54:02,696
Then she bent over to me

495
00:54:02,980 --> 00:54:04,652
and asked for forgiveness.

496
00:54:06,300 --> 00:54:07,415
Me.

497
00:54:09,580 --> 00:54:11,775
We looked at each other one last time

498
00:54:12,380 --> 00:54:13,529
and then I kissed her.

499
00:54:33,580 --> 00:54:35,889
Then she walked on,

500
00:54:36,620 --> 00:54:38,178
with her husband and child

501
00:54:38,860 --> 00:54:39,929
and disappeared.

502
00:54:41,540 --> 00:54:42,495
Forever.

503
00:54:58,460 --> 00:55:00,371
And for the rest of my life,
every day that went by,

504
00:55:00,580 --> 00:55:02,571
I'd think about her
at least once.

