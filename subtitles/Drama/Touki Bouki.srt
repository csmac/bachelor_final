1
00:06:04,000 --> 00:06:06,506
NICE. THE RIVIERA AND CORSICA

2
00:07:59,576 --> 00:08:02,427
Allah is great.

3
00:08:09,122 --> 00:08:11,487
Allah is great.

4
00:08:14,106 --> 00:08:17,135
Allah is great.

5
00:08:17,242 --> 00:08:19,986
All praise is due to Allah.
Lord of the World.

6
00:08:20,099 --> 00:08:22,914
The beneficent. Merciful
master of the day of judgment.

7
00:08:23,026 --> 00:08:26,020
Thee we serve and
thee we beseech for help.

8
00:08:26,126 --> 00:08:30,997
Keep us on the right path.

9
00:08:50,308 --> 00:08:52,387
Shit.

10
00:08:55,918 --> 00:08:57,902
Don't forget your change.

11
00:08:58,009 --> 00:09:00,301
- Greet your mother for me.
- I won't forget.

12
00:09:00,866 --> 00:09:03,575
- Peace with you.
- And to you.

13
00:09:05,743 --> 00:09:07,894
My tomatoes are all over the place.

14
00:09:08,008 --> 00:09:09,707
Help yourself. Neighbor.

15
00:09:09,820 --> 00:09:11,970
The postman's hiding something from me.

16
00:09:12,085 --> 00:09:15,946
No letters from France.
My son doesn't write.

17
00:09:16,510 --> 00:09:19,016
If you'd listened to me.
You'd never have let him go there.

18
00:09:19,124 --> 00:09:21,179
France? Nothing good comes of it.

19
00:09:21,284 --> 00:09:23,684
You better go to a marabout
to make him come back.

20
00:09:23,792 --> 00:09:25,598
Those kids never come back from France.

21
00:09:25,709 --> 00:09:28,001
Or they bring back
white women with their diseases.

22
00:09:28,114 --> 00:09:29,884
My son would never do that.

23
00:09:29,994 --> 00:09:31,942
- I've got to go.
- He's just there to study.

24
00:09:32,051 --> 00:09:33,642
I'll pay when my husband returns.

25
00:09:35,779 --> 00:09:38,178
My ass! You'll pay now!

26
00:09:38,288 --> 00:09:40,830
Give it back.
When are you gonna pay?

27
00:09:40,936 --> 00:09:43,371
We're tired of this.

28
00:09:43,479 --> 00:09:45,534
Let her go.

29
00:09:46,685 --> 00:09:49,085
Wait. I'll pay now.

30
00:09:49,193 --> 00:09:52,187
- You're a stubborn girl.
- Can't leave without paying.

31
00:09:52,295 --> 00:09:55,324
Your mom and I are good friends.
This has never happened before.

32
00:09:55,430 --> 00:09:57,901
Tell Mory to meet me
at the university.

33
00:09:58,009 --> 00:10:01,941
What's he riding today.
A motorcycle or an ox?

34
00:10:02,051 --> 00:10:04,071
Get out of here with your shabby clothes.

35
00:10:04,176 --> 00:10:08,834
You say your a student.
But your head's full of garbage.

36
00:10:09,298 --> 00:10:13,159
That Mory's no good.

37
00:10:13,271 --> 00:10:17,548
He's got no class. No shame.
Wears shabby clothes.

38
00:10:17,661 --> 00:10:19,573
Comes by on that motorcycle.

39
00:10:19,681 --> 00:10:21,522
You go off with that skull on it
like a freak.

40
00:10:22,086 --> 00:10:24,937
Keep going and he'll find you
at that university of yours.

41
00:10:25,048 --> 00:10:27,899
That university's just a dump.

42
00:11:39,125 --> 00:11:41,976
If you want to get your water.
You better listen up.

43
00:11:54,212 --> 00:11:56,541
Everyone wait their turn.

44
00:11:58,010 --> 00:12:00,932
Help her with that.

45
00:12:04,038 --> 00:12:06,437
Okay. Here we go.

46
00:12:09,579 --> 00:12:14,141
Kids. If you've got nothing to do here.
Get out of the way.

47
00:12:35,850 --> 00:12:37,549
Stop it!

48
00:13:43,935 --> 00:13:48,045
Hey. Comrade. Will the revolution
wait much longer for you?

49
00:13:53,830 --> 00:13:57,061
All these chicks
think about is screwing.

50
00:14:52,681 --> 00:14:55,532
Hey! Think this is a circus?

51
00:14:56,200 --> 00:14:59,574
I just came to pick up Anta.

52
00:14:59,685 --> 00:15:01,217
I get it.

53
00:15:01,322 --> 00:15:05,432
It's because of you that
she's always late to our meetings.

54
00:15:14,911 --> 00:15:18,036
I get it! Come to spy on us?

55
00:15:23,691 --> 00:15:25,046
Kill him!

56
00:15:29,232 --> 00:15:31,109
Come on.

57
00:18:50,802 --> 00:18:53,131
Hey. Baby.

58
00:18:53,241 --> 00:18:57,447
Hands off that student rabble!

59
00:19:01,987 --> 00:19:04,137
Know where Mory is. Aunt Oumi?

60
00:19:04,251 --> 00:19:09,538
Don't know. Probably puking up
the rice he owes me.

61
00:19:10,872 --> 00:19:14,518
- That's your problem.
- You haven't heard?

62
00:19:14,635 --> 00:19:17,557
That son of a bitch jumped off
the cliff to get away from me.

63
00:19:23,903 --> 00:19:26,647
I hope he winds up in hell.

64
00:24:59,934 --> 00:25:03,653
There's a boat leaving tomorrow.
Let's take it.

65
00:25:05,126 --> 00:25:11,007
Yeah? You got the bread?
Better pay the sorceress for her rice.

66
00:25:11,119 --> 00:25:16,477
We'll just split
and send her a check later on.

67
00:25:16,590 --> 00:25:18,289
Then what?

68
00:25:18,402 --> 00:25:23,237
No hassle.
It's an easy hustle.

69
00:25:26,590 --> 00:25:31,877
All we need is a bit of ready cash.
Throw it around.

70
00:25:32,792 --> 00:25:35,465
Tip the right guys.

71
00:25:35,579 --> 00:25:37,634
Play it like we're loaded.

72
00:25:37,740 --> 00:25:40,935
Hang out where the money is.

73
00:25:41,050 --> 00:25:42,749
France. Here we come!

74
00:25:43,664 --> 00:25:48,262
Paris. The gateway to paradise.

75
00:25:48,367 --> 00:25:51,111
And when we come back.
They'll call me Mr. Mory.

76
00:25:51,224 --> 00:25:55,714
And you can walk all over
those Red Cross broads

77
00:25:55,823 --> 00:25:58,817
grown fat on the drought.

78
00:25:58,924 --> 00:26:03,415
They'll make you their president.

79
00:26:07,321 --> 00:26:12,050
Ingrate! That's how
you pay for my rice?

80
00:26:12,165 --> 00:26:15,645
Bum! Parasite!
Good-for-nothing!

81
00:26:15,754 --> 00:26:18,083
I'll sick my marabouts on you!

82
00:26:18,193 --> 00:26:20,343
You'll crawl back crying!

83
00:26:20,458 --> 00:26:23,654
I'll get your ass yet!

84
00:26:23,769 --> 00:26:26,442
Just you wait!

85
00:26:38,228 --> 00:26:41,044
<i>Paris, Paris, Paris</i>

86
00:26:42,026 --> 00:26:45,542
<i>It's paradise on earth</i>

87
00:29:06,313 --> 00:29:08,333
- Look at this!
- What?

88
00:29:12,725 --> 00:29:14,531
Some gris-gris.

89
00:29:14,641 --> 00:29:16,791
Don't touch them.

90
00:29:18,299 --> 00:29:20,556
- Who says?
- My grandmother.

91
00:29:22,689 --> 00:29:24,768
Who told her?

92
00:29:32,237 --> 00:29:34,292
Look.

93
00:29:36,976 --> 00:29:38,508
There's all sorts here.

94
00:29:43,457 --> 00:29:45,678
One for broken hearts...

95
00:29:47,987 --> 00:29:50,042
one for syphilis...

96
00:29:52,377 --> 00:29:53,909
and this one's for good luck.

97
00:29:54,014 --> 00:29:56,069
It's the key to wealth.

98
00:30:24,015 --> 00:30:26,236
<i>If you get this one, you lose.</i>

99
00:30:30,182 --> 00:30:32,059
<i>Can I see this one?</i>

100
00:30:32,168 --> 00:30:34,045
<i>That one loses.</i>

101
00:30:39,729 --> 00:30:41,748
<i>Okay, let's go.</i>

102
00:30:45,443 --> 00:30:48,567
<i>Black wins, red loses.</i>

103
00:30:49,032 --> 00:30:52,371
<i>Try your luck.
This one loses. That one loses.</i>

104
00:30:53,458 --> 00:30:56,618
<i>Turn it over.</i>

105
00:30:57,186 --> 00:31:00,251
<i>The black one.
You win.</i>

106
00:31:00,356 --> 00:31:02,233
<i>You haven't played. Your turn.</i>

107
00:31:02,760 --> 00:31:06,312
You get this ace. You win.

108
00:31:07,394 --> 00:31:10,910
You pick this queen. You lose.
You pick that queen. You lose.

109
00:31:11,646 --> 00:31:13,796
- I'm in for ten.
- Ten.

110
00:31:13,911 --> 00:31:16,512
Pick the ace.

111
00:31:16,628 --> 00:31:19,099
- What?
- The ace.

112
00:31:24,607 --> 00:31:29,335
If you don't want the money. Stop playing.
If you get the ace. You win.

113
00:31:29,450 --> 00:31:33,763
Get this one. You lose.
I shuffle. And that's it.

114
00:31:35,200 --> 00:31:39,441
You want to play? Let's go.
Just pick the ace and you win.

115
00:31:40,252 --> 00:31:43,662
Just like I said.
Pick this guy. You lose.

116
00:31:44,608 --> 00:31:49,134
I shuffle. And that's it. Pick.

117
00:31:50,148 --> 00:31:52,369
One thousand!

118
00:31:52,483 --> 00:31:54,359
Small bets only.

119
00:31:54,469 --> 00:31:57,142
- One thousand!
- What. You find a wallet?

120
00:31:58,754 --> 00:32:00,286
Have a seat.

121
00:32:00,949 --> 00:32:03,527
Let's see what I've got in here.

122
00:32:05,095 --> 00:32:06,830
Here's a thousand.

123
00:32:10,078 --> 00:32:11,990
If you pick this one. You win.

124
00:32:12,970 --> 00:32:15,679
This one. You lose.
This one. You lose.

125
00:32:15,793 --> 00:32:17,324
I shuffle. And that's it.

126
00:32:18,058 --> 00:32:19,828
Pick one!

127
00:32:27,535 --> 00:32:29,067
You lose. Buddy!

128
00:32:29,660 --> 00:32:31,359
You lose!

129
00:32:31,472 --> 00:32:33,207
Where's your thousand?

130
00:32:33,318 --> 00:32:34,815
Hey! Stop him!

131
00:32:35,445 --> 00:32:37,144
Thief!

132
00:33:46,385 --> 00:33:48,405
Give me a cigarette. Brother.

133
00:34:41,682 --> 00:34:47,849
What. Those assholes
think I stole a thousand?

134
00:34:50,637 --> 00:34:53,274
They haven't seen anything yet.

135
00:34:54,017 --> 00:34:56,274
Just you wait.

136
00:34:57,885 --> 00:34:59,025
Right.

137
00:34:59,976 --> 00:35:02,649
You realize
you're sitting on a treasure?

138
00:35:02,763 --> 00:35:03,796
What?

139
00:35:03,913 --> 00:35:07,464
This arena makes loads
of money on Sundays.

140
00:35:09,069 --> 00:35:12,621
- Get it?
- Yeah.

141
00:35:17,363 --> 00:35:20,428
Long live the wrestlers.

142
00:36:01,613 --> 00:36:04,773
<i>This is a great day for Senegal.</i>

143
00:36:05,342 --> 00:36:10,557
<i>Long-standing ties
bind France and Senegal.</i>

144
00:36:10,673 --> 00:36:15,853
<i>Our greatest champions
are risking their titles today</i>

145
00:36:15,969 --> 00:36:18,024
<i>with the noble aim ofhelping us
make our contribution</i>

146
00:36:18,129 --> 00:36:20,208
<i>towards the construction
of the General De Gaulle Memorial.</i>

147
00:36:20,324 --> 00:36:22,023
<i>This is a big event.</i>

148
00:36:22,136 --> 00:36:26,900
<i>All of the elders, men, women
and children are here today.</i>

149
00:36:27,015 --> 00:36:29,415
<i>All the important people are here.</i>

150
00:39:20,954 --> 00:39:24,470
Hey. Isn't he that same guy?

151
00:39:24,577 --> 00:39:27,844
You're right. That's him.

152
00:39:27,957 --> 00:39:32,686
He won't be stopping anyone today.
He's got a whole pack of cigarettes.

153
00:39:35,797 --> 00:39:43,245
Which trunk do you think the money is in?

154
00:39:43,846 --> 00:39:47,220
- The green one on top.
- The green one?

155
00:39:47,331 --> 00:39:50,942
No. The blue one on the bottom.
That's why they put the others on top.

156
00:39:51,059 --> 00:39:53,660
- I'd bet on the green one.
- Don't you contradict me.

157
00:39:53,776 --> 00:39:56,662
I'm the boss man here.

158
00:39:56,773 --> 00:39:59,933
It's the blue one.
Let's grab it and go.

159
00:40:03,323 --> 00:40:06,840
<i>Our greatest champions
are risking their titles today</i>

160
00:40:06,947 --> 00:40:10,428
<i>with the noble aim ofhelping us
make our contribution</i>

161
00:40:10,536 --> 00:40:13,078
<i>towards the construction
of the General De Gaulle Memorial.</i>

162
00:40:13,184 --> 00:40:17,842
<i>Good old De Gaulle.
His memorial can wait.</i>

163
00:40:17,958 --> 00:40:21,403
<i>This beats everything.
Paris, here I come!</i>

164
00:40:21,511 --> 00:40:24,992
<i>You street-sweeping niggers,
you city nigger.</i>

165
00:40:25,101 --> 00:40:28,712
<i>I'm not one of you.
Can't mess with me, 'cause I'll be in Paris.</i>

166
00:40:35,100 --> 00:40:40,637
<i>The Eiffel Tower, Notre Dame,
the Champs-Elys�es, the L 'Arc de Triomphe.</i>

167
00:40:40,746 --> 00:40:43,668
<i>I'm the one.</i>

168
00:40:49,805 --> 00:40:54,854
<i>If this happens,
I'll be in with the big boys.</i>

169
00:40:54,961 --> 00:40:59,904
<i>These guys here don't know
who they're messing with.</i>

170
00:42:36,252 --> 00:42:40,184
What a surprise. Brother!
You. A cop?

171
00:42:40,747 --> 00:42:43,456
You know the other night
right after you left.

172
00:42:43,569 --> 00:42:46,146
The place was raided.

173
00:42:46,251 --> 00:42:50,220
I only got away because
I was outside pissing.

174
00:42:50,328 --> 00:42:53,667
Scram! We've never met.
Don't mess up my career.

175
00:42:55,869 --> 00:42:57,746
Smart ass.

176
00:43:32,838 --> 00:43:35,796
Miss. As much as
I welcome your company.

177
00:43:35,904 --> 00:43:39,207
Which of these fine houses is yours?

178
00:43:40,120 --> 00:43:45,299
<i>Drive on. We're going to my second home.
My maison de campagne.</i>

179
00:43:56,601 --> 00:43:59,417
<i>This doesn't look so good.
All those cats.</i>

180
00:43:59,528 --> 00:44:02,202
<i>Are you sure this is the right way?</i>

181
00:44:02,315 --> 00:44:05,999
<i>Miss, how could you live here?
This place is creepy.</i>

182
00:44:07,785 --> 00:44:10,981
<i>I'll drop you off
and go on my way.</i>

183
00:44:11,096 --> 00:44:13,008
<i>This place gives me the creeps.</i>

184
00:45:21,689 --> 00:45:23,839
Don't you have a husband?

185
00:45:23,954 --> 00:45:27,185
You must be brave to live here.

186
00:45:28,309 --> 00:45:30,780
God help us.

187
00:45:30,888 --> 00:45:32,729
God save us from places like this.

188
00:46:00,540 --> 00:46:04,508
This is a bad place.

189
00:46:05,836 --> 00:46:08,580
Dear God.

190
00:46:11,132 --> 00:46:15,694
Aren't you from Senegal?
Only Europeans could live like this.

191
00:48:27,962 --> 00:48:30,849
Hey. Iook at that wreck.

192
00:48:36,186 --> 00:48:38,265
If I had tools. I'd fix it.

193
00:48:38,381 --> 00:48:41,196
And we'd split for Paris.

194
00:48:42,283 --> 00:48:46,394
Stop dreaming about that.
Not even an angel could do it.

195
00:49:16,430 --> 00:49:18,686
I got it!

196
00:49:19,949 --> 00:49:22,550
You see all the way down there?

197
00:49:22,667 --> 00:49:24,199
You turn right.

198
00:49:24,304 --> 00:49:27,263
And there's this huge guy
who lives over there.

199
00:49:27,371 --> 00:49:30,472
He's got a nice house.
I met him at Mapenda's place.

200
00:49:30,576 --> 00:49:34,437
A real good one.
Let's go pay him a visit.

201
00:49:34,549 --> 00:49:37,923
But before we do that.

202
00:49:38,033 --> 00:49:39,910
I've got to go take a crap.

203
00:49:40,890 --> 00:49:43,290
You're the man.
Whatever you say.

204
00:50:08,626 --> 00:50:10,325
Oh. It's Mory.

205
00:50:11,064 --> 00:50:12,763
Go on!

206
00:50:17,302 --> 00:50:19,452
Sit here. Mory.

207
00:50:22,458 --> 00:50:25,417
You go join the women. Go on.

208
00:50:32,180 --> 00:50:38,062
<i>Thejoy oflove</i>

209
00:50:38,730 --> 00:50:44,017
<i>Lasts but a moment</i>

210
00:50:45,491 --> 00:50:49,696
<i>But heartbreak</i>

211
00:50:49,811 --> 00:50:57,046
<i>Lasts a lifetime</i>

212
00:51:16,292 --> 00:51:20,426
<i>I abandoned everything</i>

213
00:51:20,542 --> 00:51:27,611
<i>For the fair Sylvie</i>

214
00:51:29,741 --> 00:51:33,531
<i>Yet she abandoned me</i>

215
00:51:33,644 --> 00:51:39,810
<i>And found another</i>

216
00:51:56,710 --> 00:52:00,679
<i>Thejoy oflove</i>

217
00:52:05,177 --> 00:52:07,197
Mory. Have a seat.

218
00:52:07,303 --> 00:52:11,616
Make yourself at home.
This is my villa.

219
00:52:11,728 --> 00:52:15,933
Whatever you wish is yours.

220
00:52:16,049 --> 00:52:20,183
You'll be the most gorgeous
at dinner tonight.

221
00:52:20,299 --> 00:52:23,982
And just in time to drink
at the wellspring of happiness.

222
00:52:32,216 --> 00:52:37,123
Mory. As you know.
I'm not jealous.

223
00:52:37,233 --> 00:52:41,166
But who's that short-haired chick?

224
00:52:42,076 --> 00:52:46,911
A broad's got better things to do
than hang around here.

225
00:52:48,522 --> 00:52:51,124
Undress! I'll rub your back.

226
00:52:52,739 --> 00:52:54,996
The water's not right.

227
00:52:56,572 --> 00:53:00,183
Put your body and soul
in your friend Charlie's care.

228
00:53:00,300 --> 00:53:03,460
They call me Charlie the cook.

229
00:53:04,238 --> 00:53:05,972
That boy you just saw.

230
00:53:06,084 --> 00:53:09,006
He's my number four boy.

231
00:53:11,415 --> 00:53:13,600
When he arrived.

232
00:53:13,714 --> 00:53:17,053
He was skinny as a rat.

233
00:53:19,952 --> 00:53:23,147
Look at him now.

234
00:53:23,262 --> 00:53:25,697
I take care of him.
Give him massages.

235
00:53:25,806 --> 00:53:28,871
Cook for him.
Make him happy.

236
00:53:43,889 --> 00:53:46,526
So you want to go to France?

237
00:53:49,673 --> 00:53:52,073
Sweetheart. France
isn't what she used to be.

238
00:53:53,610 --> 00:53:56,569
When I was there.
That's when France was great.

239
00:53:56,676 --> 00:54:00,572
Champs-Elys�es. Pigalle.
Montparnasse.

240
00:54:03,018 --> 00:54:05,310
Mory. The water's great.

241
00:54:08,489 --> 00:54:12,730
Mory. How's your back?

242
00:54:13,576 --> 00:54:15,904
Let me wash it for you.

243
00:54:17,026 --> 00:54:19,045
I'll show you who I am.

244
00:54:27,967 --> 00:54:30,046
The water's nice.

245
00:55:07,096 --> 00:55:11,171
I'll show you what I'm like.

246
00:55:12,949 --> 00:55:15,623
The water's nice.

247
00:55:21,939 --> 00:55:23,638
Come here.

248
00:56:05,981 --> 00:56:08,310
The master says you're to take us.

249
00:56:09,221 --> 00:56:11,133
Hurry up.

250
00:58:11,662 --> 00:58:14,133
<i>Here comes a griot
from the M'bai family</i>

251
00:58:14,240 --> 00:58:17,995
<i>Today you'll know the man
You'll know the best praise singer</i>

252
00:58:20,303 --> 00:58:23,915
<i>I won't bow to any wrestler</i>

253
00:58:24,032 --> 00:58:26,502
<i>God has granted
this wrestler the power</i>

254
00:58:27,446 --> 00:58:30,677
<i>God has granted him the power</i>

255
00:58:32,952 --> 00:58:37,585
<i>One attacked my right side
I beat him down</i>

256
00:58:39,153 --> 00:58:41,446
<i>Another attacked my left side
I beat him down</i>

257
00:58:42,881 --> 00:58:45,103
<i>Another one came from behind
I beat him down</i>

258
00:58:46,436 --> 00:58:48,836
<i>Here comes a griot
from the M'bai family</i>

259
00:58:49,920 --> 00:58:52,176
<i>Another came from the front
I beat him down</i>

260
00:58:53,439 --> 00:58:55,768
<i>If I do battle
I have to win</i>

261
00:59:01,245 --> 00:59:04,963
<i>Griot M'bai
You shine like gold</i>

262
00:59:05,077 --> 00:59:10,435
<i>The others are only silver</i>

263
00:59:11,175 --> 00:59:14,964
<i>The country's champions
all crumble under my hands</i>

264
00:59:15,530 --> 00:59:18,310
<i>Lady of the blue mouth
Your champion is here</i>

265
00:59:18,805 --> 00:59:21,656
<i>Lady of the blue mouth
Your champion is here</i>

266
00:59:29,015 --> 00:59:34,658
<i>The drummer comes out
to my house in the morning</i>

267
01:02:48,494 --> 01:02:51,619
<i>Hey, here's Mory
The prodigal son is back</i>

268
01:02:51,734 --> 01:02:55,595
<i>The prince doesn't forget his subjects</i>

269
01:02:55,707 --> 01:02:58,487
<i>Your father rode across the savannas</i>

270
01:02:59,226 --> 01:03:05,107
<i>Mory, you can hold your head up high</i>

271
01:03:14,139 --> 01:03:17,299
<i>Your mother watered the ocean</i>

272
01:03:17,414 --> 01:03:20,609
<i>Shame will never come because of you</i>

273
01:03:20,724 --> 01:03:23,991
<i>The vultures, the hyenas won't succeed</i>

274
01:03:24,105 --> 01:03:28,001
<i>A feast then for the prodigal son</i>

275
01:03:30,062 --> 01:03:33,127
<i>Here I am before you
I'll be with you to the end</i>

276
01:03:33,233 --> 01:03:34,932
<i>Let the drums beat</i>

277
01:03:50,516 --> 01:03:54,650
<i>Mory, you remain our chosen,
our favorite friend</i>

278
01:03:58,251 --> 01:04:01,173
<i>Mory, you remain our chosen,
our favorite friend</i>

279
01:05:00,063 --> 01:05:05,183
<i>Mory, you, Mory of our hearts</i>

280
01:05:06,649 --> 01:05:15,037
<i>Mory, son of the savanna
Mory, answer me</i>

281
01:05:16,266 --> 01:05:21,173
<i>Tell me, Mory
Say yes, Mory</i>

282
01:05:22,154 --> 01:05:26,122
<i>The prince said yes to me
It won't surprise you</i>

283
01:05:26,231 --> 01:05:30,544
<i>The sky is clear now
Mory said yes to me</i>

284
01:06:24,419 --> 01:06:27,793
Hello? Central Police Station?

285
01:06:30,448 --> 01:06:33,643
Is Sergeant Blanchet there?

286
01:06:34,872 --> 01:06:36,404
He's out?

287
01:06:36,510 --> 01:06:39,705
How about Mr. Thioye?

288
01:06:42,085 --> 01:06:45,352
He's out?
Is anybody in?

289
01:06:46,790 --> 01:06:49,605
Give me the division commander. Please.

290
01:06:49,716 --> 01:06:51,248
Oh. He's in.

291
01:06:51,354 --> 01:06:54,799
Tell him it's Charlie. Yes.

292
01:06:56,441 --> 01:07:02,393
Is that you. Mambety?
How are you?

293
01:07:02,504 --> 01:07:07,719
Inspector Mambety.
You're an ingrate.

294
01:07:07,835 --> 01:07:12,147
You made me a promise.
And then you disappeared.

295
01:07:12,260 --> 01:07:15,907
Promises. Promises.

296
01:07:16,023 --> 01:07:20,406
But that's not what I'm calling about.
It's business.

297
01:07:21,145 --> 01:07:23,200
I'm calling...

298
01:07:23,305 --> 01:07:25,562
because somebody ripped me off.

299
01:07:26,859 --> 01:07:30,447
He wanted to go to Europe.
He wanted money. He came to me.

300
01:07:31,702 --> 01:07:36,015
I wanted to help him out.
You know me. A regular Pygmalion.

301
01:07:36,128 --> 01:07:38,705
You know. I always help the kids.

302
01:07:38,811 --> 01:07:41,697
He came in. I was taking a shower.

303
01:07:41,808 --> 01:07:43,958
And he stole all my clothes.

304
01:07:45,291 --> 01:07:46,467
Then left!

305
01:07:46,581 --> 01:07:49,254
Anyway. He's easy to recognize.

306
01:07:49,368 --> 01:07:54,417
He's tall. Messy hair.
Iooks like a hippie.

307
01:07:54,525 --> 01:07:58,873
He's with a girl
who's got messed up hair too.

308
01:07:58,985 --> 01:08:01,313
They stole one of my cars.

309
01:08:01,424 --> 01:08:06,188
The all-American classic
with stars and stripes.

310
01:08:07,104 --> 01:08:09,705
I don't remember the licence number.

311
01:08:09,821 --> 01:08:11,068
No.

312
01:08:11,668 --> 01:08:15,149
But they're very hungry.

313
01:08:15,257 --> 01:08:20,092
I'm sure you can find them at a restaurant
or a travel agency

314
01:08:20,205 --> 01:08:21,832
because they love money.

315
01:08:21,947 --> 01:08:24,418
Don't look-
Yes.

316
01:08:24,525 --> 01:08:27,102
He wants to leave.

317
01:08:27,208 --> 01:08:28,740
I'm counting on you.

318
01:08:28,846 --> 01:08:31,246
Okay. I'll make a statement.

319
01:08:31,355 --> 01:08:34,658
Why don't you come over
to take it down. Sweetheart?

320
01:08:34,769 --> 01:08:38,488
I got some first-class whiskey.

321
01:08:39,055 --> 01:08:40,967
You'll come? Okay.

322
01:08:41,076 --> 01:08:43,332
See you tonight.

323
01:09:34,561 --> 01:09:35,737
Here you are. Miss.

324
01:09:35,850 --> 01:09:39,295
I hope you'll have a lovely trip.
Please call again.

325
01:09:41,320 --> 01:09:45,431
Say. Miss.
Haven't I seen you somewhere?

326
01:09:45,537 --> 01:09:48,079
Must have been New York.

327
01:10:10,868 --> 01:10:14,942
PORT OF DAKAR
NO ENTRYWITHOUT AUTHORIZATION

328
01:10:32,854 --> 01:10:34,553
<i>This city's rough.</i>

329
01:10:35,676 --> 01:10:38,836
<i>"No Entry'"signs to the right.</i>

330
01:10:38,952 --> 01:10:41,946
<i>"No Entry'"signs to the left.</i>

331
01:10:42,053 --> 01:10:44,797
<i>A man with a club on every corner.</i>

332
01:10:44,910 --> 01:10:47,654
<i>All because of too many debts.</i>

333
01:10:50,381 --> 01:10:54,943
<i>But I've got to go.
Debt never killed anybody.</i>

334
01:10:55,886 --> 01:10:59,854
<i>Honestly, debt never killed anybody.
Let's go.</i>

335
01:12:07,420 --> 01:12:09,119
Could you take me to the port?

336
01:12:09,232 --> 01:12:12,986
The guy's been waiting for me
at the corner all day 'cause I owe him.

337
01:12:13,099 --> 01:12:15,807
Look. It's Margot.
You're always on some scam.

338
01:12:16,723 --> 01:12:19,052
Drive on. Chauffeur.

339
01:13:47,804 --> 01:13:53,649
<i>Thejoy oflove</i>

340
01:13:54,563 --> 01:13:59,648
<i>Lasts but a moment</i>

341
01:14:01,148 --> 01:14:05,283
<i>But heartbreak</i>

342
01:14:05,400 --> 01:14:11,803
<i>Lasts a lifetime</i>

343
01:14:27,700 --> 01:14:36,158
<i>The Ancerville
will sail at 4:00 p.m.</i>

344
01:14:49,999 --> 01:14:53,610
Mao Tse-tung is soft.
What China needs -

345
01:14:53,728 --> 01:14:57,803
I told my pupils on the first day.

346
01:14:57,909 --> 01:15:02,472
"Your job is to kick out
the neocolonialists who I represent."

347
01:15:02,578 --> 01:15:05,429
We've taught in Dakar for seven years.

348
01:15:05,540 --> 01:15:10,719
The Italian communist
party is too extreme.

349
01:15:10,836 --> 01:15:14,971
We never left Dakar.
What is there in Senegal?

350
01:15:15,086 --> 01:15:18,496
Barren. Intellectually as well.

351
01:15:18,607 --> 01:15:22,776
Our salary is three times
that of their teachers.

352
01:15:23,519 --> 01:15:26,441
But they don't eat as we do.

353
01:15:26,550 --> 01:15:28,462
They're unrefined.

354
01:15:28,571 --> 01:15:32,432
And what would we
buy here. Masks?

355
01:15:33,624 --> 01:15:39,469
African art is a joke made up
by journalists in need of copy.

356
01:15:39,582 --> 01:15:43,062
We bank half our annual salary.

357
01:15:43,171 --> 01:15:44,727
Not that we stint ourselves.

358
01:15:44,844 --> 01:15:48,812
America needs austerity
to improve its economy.

359
01:15:48,921 --> 01:15:52,330
I invested a decent sum...

360
01:15:53,380 --> 01:15:56,301
in a bona fide business in Nice.

361
01:15:56,411 --> 01:16:02,613
When we left this morning.
Our house boy threatened to leave

362
01:16:02,718 --> 01:16:07,553
unless he had a 1.500-CFA raise.

363
01:16:07,666 --> 01:16:09,198
After all we've done for him!

364
01:16:09,304 --> 01:16:11,668
They're just big children.

365
01:16:11,777 --> 01:16:13,476
They have no heart.

366
01:17:00,419 --> 01:17:04,589
<i>I abandoned everything</i>

367
01:17:04,705 --> 01:17:08,744
<i>For the fair Sylvie</i>

368
01:17:13,904 --> 01:17:17,207
<i>She abandoned me</i>

369
01:17:17,772 --> 01:17:20,516
<i>And found another</i>

370
01:17:31,012 --> 01:17:36,025
<i>Mr. Diop is requested
to see the captain at once.</i>

371
01:24:26,556 --> 01:24:28,088
Is he still alive?

372
01:24:32,549 --> 01:24:34,212
He's losing blood.

373
01:24:34,326 --> 01:24:36,167
Looks really bad.

374
01:24:43,490 --> 01:24:48,016
Did you know it?
It was a handsome beast.

