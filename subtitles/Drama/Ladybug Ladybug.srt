﻿1
00:00:01,919 --> 00:00:03,674
In the daytime, I'm Marinette.

2
00:00:04,146 --> 00:00:05,878
Just a normal girl with a normal life.

3
00:00:06,423 --> 00:00:09,320
But there's something about me that no one knows yet,

4
00:00:10,087 --> 00:00:11,094
'Cause I have a secret.

5
00:00:11,127 --> 00:00:13,019
♪ Miraculous Ladybug!

6
00:00:13,256 --> 00:00:14,346
♪ Miraculous!

7
00:00:14,521 --> 00:00:15,569
♪ Simply the best!

8
00:00:15,648 --> 00:00:18,515
♪ Up to the test when things go wrong!

9
00:00:18,579 --> 00:00:20,856
♪ Miraculous, the luckiest!

10
00:00:20,947 --> 00:00:24,340
♪ The power of love, always so strong!

11
00:00:24,407 --> 00:00:28,264
♪ Miraculous!

12
00:00:30,463 --> 00:00:35,066
♪ Merry Christmas to all!

13
00:00:37,436 --> 00:00:39,714
♪ Dad, Mom, I'm gonna help you at the bakery.

14
00:00:39,739 --> 00:00:42,189
♪ To give the Yule logs to all of my friends.

15
00:00:42,214 --> 00:00:44,798
♪ Alya, Merry Christmas to your whole family.

16
00:00:44,823 --> 00:00:47,142
♪ Thank you Marinette, Merry Christmas as well.

17
00:00:47,167 --> 00:00:49,310
♪ Alix and her dad, Merry Christmas to you both.

18
00:00:49,335 --> 00:00:52,023
♪ Lively hearts, Marinette, all of our best wishes.

19
00:00:52,048 --> 00:00:54,303
♪ Rose and Juleka, it's for you, happy holidays.

20
00:00:54,328 --> 00:00:55,746
♪ Merry Christmas too.

21
00:00:55,771 --> 00:00:56,979
♪ Merry Christmas Marinette.

22
00:00:57,004 --> 00:00:59,624
♪ Merry Christmas to you, Manon and Nadia.

23
00:00:59,649 --> 00:01:02,189
♪ Merry Christmas Nino and Sabrina.

24
00:01:02,214 --> 00:01:03,626
♪ Merry Chri-

25
00:01:05,135 --> 00:01:06,928
You want my picture?

26
00:01:07,269 --> 00:01:09,269
Marinette, it's Christmas.

27
00:01:11,258 --> 00:01:13,398
♪ Merry Christmas, Chloe...

28
00:01:13,423 --> 00:01:14,902
Can you repeat?

29
00:01:14,927 --> 00:01:16,650
I didn't hear.

30
00:01:16,842 --> 00:01:20,059
♪ Merry Christmas, Chloe.

31
00:01:21,468 --> 00:01:23,468
Chloe, Christmas Truce.

32
00:01:24,814 --> 00:01:27,703
♪ Merry Christmas, Marinette.

33
00:01:28,181 --> 00:01:30,168
But I hate you.

34
00:01:30,540 --> 00:01:36,934
♪ Merry Christmas to all.

35
00:01:43,775 --> 00:01:45,020
Adrien's bodyguard?

36
00:01:45,133 --> 00:01:45,816
The gift.

37
00:01:45,841 --> 00:01:47,370
Be right back.

38
00:01:49,779 --> 00:01:50,855
I can't believe it.

39
00:01:50,880 --> 00:01:52,473
The gift.  Where did I put it?

40
00:01:52,498 --> 00:01:53,861
It can't be real.

41
00:01:53,886 --> 00:01:54,773
Here.

42
00:01:55,910 --> 00:01:57,503
Thank you, Tikki.

43
00:02:01,836 --> 00:02:03,117
Sir!  Wait!

44
00:02:03,232 --> 00:02:04,934
Please, it's-it's, um...

45
00:02:04,959 --> 00:02:06,771
A gift for Adrien.

46
00:02:06,867 --> 00:02:09,511
Can you wish him a Merry Christmas from me?

47
00:02:09,535 --> 00:02:11,884
I mean er, to you too.
By the way, Merry Christmas.

48
00:02:11,909 --> 00:02:14,355
Sir, from-from me...

49
00:02:21,838 --> 00:02:24,289
I hope Adrien will have a Merry Christmas.

50
00:02:32,233 --> 00:02:33,551
What do you think, Nathalie?

51
00:02:33,576 --> 00:02:35,576
You think he'll like all these decorations?

52
00:02:38,448 --> 00:02:39,718
Why isn't he coming?

53
00:02:39,742 --> 00:02:40,689
Did you call him?

54
00:02:40,714 --> 00:02:42,148
Maybe he needs...

55
00:02:42,173 --> 00:02:43,029
some time.

56
00:02:46,258 --> 00:02:47,622
What's the point of all this?

57
00:02:47,646 --> 00:02:48,447
He's not coming.

58
00:02:51,826 --> 00:02:52,628
That's nice.

59
00:02:54,944 --> 00:02:57,267
I wish you both a Merry Christmas.

60
00:03:01,711 --> 00:03:02,416
Yes?

61
00:03:03,979 --> 00:03:06,541
I can imagine how hard it must be, sir.

62
00:03:06,566 --> 00:03:09,319
But consider that this is
Adrien's first Christmas without his mother.

63
00:03:09,343 --> 00:03:10,095
I sincerely think-

64
00:03:10,120 --> 00:03:11,286
I know, you're right.

65
00:03:11,354 --> 00:03:12,303
I will talk to him.

66
00:03:12,328 --> 00:03:14,328
Give me just a moment, I'm coming.

67
00:03:20,827 --> 00:03:22,827
Even on Christmas,
he only thinks about himself.

68
00:03:23,208 --> 00:03:25,595
I want to be done with this blasted day.

69
00:03:25,620 --> 00:03:26,871
I hate holidays.

70
00:03:27,021 --> 00:03:28,670
Plagg, transform me.

71
00:03:47,440 --> 00:03:50,454
♪ The city lights up, yes it's Christmas in Paris.

72
00:03:50,478 --> 00:03:52,742
♪ But I'm all alone in the night...

73
00:03:53,527 --> 00:03:58,635
♪ Family reunions and dazzling gifts,
♪ a Chat Noir all alone tonight.

74
00:03:58,888 --> 00:04:02,188
♪ I too am dreaming of warmth and tenderness.

75
00:04:02,212 --> 00:04:04,568
♪ But I roam alone like a cat, in the night.

76
00:04:04,718 --> 00:04:08,019
♪ I'm a poor kitty, why does no one pity me?

77
00:04:08,044 --> 00:04:10,562
♪ Chat Noir's feeling so lonely tonight.

78
00:04:10,981 --> 00:04:13,293
♪ Chat Noir's feeling so lonely tonight.

79
00:04:13,656 --> 00:04:17,144
♪ If I stray, if I get lost, what does that matter?

80
00:04:17,221 --> 00:04:19,221
♪ After all, I'm just a cat in the night.

81
00:04:19,245 --> 00:04:22,863
♪ Your tree despairs me, I'll blast it to ashes.

82
00:04:22,888 --> 00:04:25,134
♪ I'm the vengeful cat of the night.

83
00:04:25,561 --> 00:04:28,023
♪ I'm the vengeful cat of the night.

84
00:04:29,021 --> 00:04:32,001
♪ Cataclysm!

85
00:04:34,715 --> 00:04:35,530
I can't...

86
00:04:35,856 --> 00:04:36,934
I can't...

87
00:04:41,085 --> 00:04:44,906
♪ Being alone in this cold
♪ doesn't do me any good.

88
00:04:45,136 --> 00:04:50,068
♪ Brooding over my anger,
♪ it didn't do anything.

89
00:04:50,116 --> 00:04:54,183
♪ I'd like to go away,
♪ get back to my home.

90
00:04:54,255 --> 00:04:59,087
♪ It'll be better tomorrow,
♪ I know it.

91
00:04:59,112 --> 00:05:00,224
Let's go back home.

92
00:05:00,579 --> 00:05:02,346
Plagg, detransform me.

93
00:05:04,528 --> 00:05:05,261
Plagg?

94
00:05:05,505 --> 00:05:06,451
Plagg!

95
00:05:07,530 --> 00:05:12,430
♪ I'd like to help you,
♪ yes, help you transform.

96
00:05:12,455 --> 00:05:16,141
♪ But take a look at me,
♪ I can't help it.

97
00:05:16,458 --> 00:05:20,997
♪ I'm tired, my stomach is starving.

98
00:05:21,021 --> 00:05:25,219
♪ I'm exhausted,
♪ I can't go on anymore.

99
00:05:25,655 --> 00:05:27,267
Plagg!  What have I done?

100
00:05:27,291 --> 00:05:28,720
Wait, wait, I'll help you.

101
00:05:47,206 --> 00:05:49,182
"Merry Christmas,
signed Marinette."

102
00:05:49,368 --> 00:05:50,673
She's so nice...

103
00:05:50,776 --> 00:05:53,054
We're gonna get you back up
on your paws, Plagg.

104
00:05:54,457 --> 00:05:55,298
You hear that?

105
00:05:59,354 --> 00:06:00,678
Merry Christmas, Plagg.

106
00:06:00,739 --> 00:06:02,358
Merry Christmas, Adrien.

107
00:06:08,013 --> 00:06:08,902
Adrien?

108
00:06:10,323 --> 00:06:11,519
Adrien?

109
00:06:12,147 --> 00:06:14,519
Adrien!

110
00:06:15,834 --> 00:06:18,269
Find him immediately,
immediately!

111
00:06:21,125 --> 00:06:21,823
Good evening, I..

112
00:06:21,862 --> 00:06:23,385
Yes, thank you,
Merry Christmas to you too.

113
00:06:23,425 --> 00:06:25,281
No ma'am, he's not at my place.

114
00:06:25,305 --> 00:06:26,820
Adrien isn't home?

115
00:06:26,845 --> 00:06:28,541
Maybe he was kidnapped.

116
00:06:28,573 --> 00:06:31,751
Don't worry Sabrina,
we're gonna find your friend quickly.

117
00:06:31,866 --> 00:06:34,818
A kidnapping,
maybe that's a little exaggerated.

118
00:06:34,968 --> 00:06:38,183
Let's go check the neighborhood
anyway, presents can wait.

119
00:06:38,219 --> 00:06:39,230
You coming,
Marinette?

120
00:06:39,255 --> 00:06:40,730
Erm, I don't feel to good...

121
00:06:40,755 --> 00:06:44,092
I'm just gonna wait for you guys here
and if I have news, I'll call you, okay?

122
00:06:45,123 --> 00:06:46,269
We have to find Adrien.

123
00:06:46,308 --> 00:06:46,868
Quick!

124
00:06:47,491 --> 00:06:49,528
Tikki, transform me.

125
00:07:10,955 --> 00:07:12,821
Don't worry Plagg,
I'll find you food.

126
00:07:12,877 --> 00:07:14,646
I can't promise you Camembert, but...

127
00:07:15,776 --> 00:07:17,538
No need to throw a tantrum,

128
00:07:17,563 --> 00:07:20,425
Santa has a gift for every kid.

129
00:07:20,569 --> 00:07:21,895
Your presents are lame.

130
00:07:21,920 --> 00:07:23,328
And your beard is ugly.

131
00:07:23,353 --> 00:07:25,310
Hey, easy kiddo.

132
00:07:27,992 --> 00:07:30,063
I'm sure you're not even
the real Santa Claus.

133
00:07:30,088 --> 00:07:32,273
Hey, that's enough,
what's gotten into you, kids?

134
00:07:32,298 --> 00:07:34,249
Aren't you ashamed
to lash out on Santa Claus?

135
00:07:34,960 --> 00:07:36,420
This young man is right.

136
00:07:36,445 --> 00:07:37,926
Apologize right now.

137
00:07:37,995 --> 00:07:39,725
Sorry, Santa Claus...

138
00:07:39,750 --> 00:07:41,321
It's not a big deal, kids.

139
00:07:41,346 --> 00:07:43,250
Have a Merry Christmas anyway.

140
00:07:44,140 --> 00:07:45,815
Sorry sir, is everything alright?

141
00:07:45,839 --> 00:07:48,692
Don't worry,
I'm an old tough Santa Claus.

142
00:07:49,072 --> 00:07:51,428
But, and you?
What are you doing out a this time?

143
00:07:51,452 --> 00:07:52,293
Did you get lost?

144
00:07:54,634 --> 00:07:57,108
Dressed like this, you must be cold.

145
00:07:57,132 --> 00:07:58,601
Do you want a cup of
hot chocolate?

146
00:07:58,625 --> 00:07:59,744
I have some left.

147
00:07:59,900 --> 00:08:01,190
With pleasure, sir.

148
00:08:01,362 --> 00:08:02,626
There's nothing better.

149
00:08:03,078 --> 00:08:05,078
Except maybe, a piece of camembert...

150
00:08:05,102 --> 00:08:08,539
You're lucky, kid, I have some left from my lunch sandwich.

151
00:08:08,705 --> 00:08:11,321
I've never had one with such a strong smell.

152
00:08:12,201 --> 00:08:13,629
This cheese is perfect.

153
00:08:14,002 --> 00:08:15,384
To each their own.

154
00:08:18,619 --> 00:08:20,159
You're not lost?

155
00:08:22,475 --> 00:08:24,552
 It's my first Christmas
without my mother, sir.

156
00:08:24,837 --> 00:08:25,648
My father, he...

157
00:08:26,130 --> 00:08:28,023
He doesn't know how to deal with it.

158
00:08:28,048 --> 00:08:29,059
I understand, you know.

159
00:08:29,603 --> 00:08:31,713
But your father must be
wondering where you are.

160
00:08:31,847 --> 00:08:33,847
And worrying a lot about you.

161
00:08:34,565 --> 00:08:36,748
Now that you got some fresh air
and thought a bit...

162
00:08:36,839 --> 00:08:39,053
Don't you think it'd be good
to go back home?

163
00:08:39,568 --> 00:08:40,529
You're right sir.

164
00:08:40,554 --> 00:08:44,346
That's good.  A family reunited under
the tree, that's the spirit of Christmas.

165
00:08:44,450 --> 00:08:46,212
I'll drop you off by sledge.

166
00:08:50,108 --> 00:08:51,552
Come here, Plagg.

167
00:08:52,519 --> 00:08:53,916
Alright, but wear this.

168
00:08:53,941 --> 00:08:56,099
The friend who gave it to me
would be happy for you to have it.

169
00:08:56,210 --> 00:08:57,321
A gift?

170
00:08:57,346 --> 00:08:58,142
For me?

171
00:08:58,285 --> 00:09:00,203
Everybody has the right
to have a Christmas present.

172
00:09:00,593 --> 00:09:02,418
Thank you, my boy.

173
00:09:22,291 --> 00:09:25,889
♪ There's only one power
♪ that can do so much damage,

174
00:09:25,914 --> 00:09:29,587
♪ Chat Noir's cataclysm,
it's for sure.

175
00:09:29,628 --> 00:09:33,353
♪ And this Christmas card,
♪ it didn't fall from the sky,

176
00:09:33,508 --> 00:09:36,975
♪ but from the gift
♪ I gave to Adrien.

177
00:09:37,348 --> 00:09:40,762
♪ Adrien disappears and
♪ then Chat Noir appears.

178
00:09:40,787 --> 00:09:44,353
♪ He must be trying
♪ to protect him.

179
00:09:44,417 --> 00:09:48,337
♪ The only explanation
♪ to these questions,

180
00:09:48,362 --> 00:09:51,935
♪ is that a supervillain
♪ wants to harm Adrien.

181
00:09:51,998 --> 00:09:55,555
♪ He can count on me,
♪ I'll get him out of there,

182
00:09:55,659 --> 00:09:59,526
♪ For he's the one
♪ I secretly love.

183
00:09:59,551 --> 00:10:03,032
♪ I'll do all I can,
♪ to help you the best I can.

184
00:10:03,088 --> 00:10:06,929
♪ You're the one I
♪ secretly love.

185
00:10:06,954 --> 00:10:10,731
♪ Even if I have to hide,
♪ I'll be by your side.

186
00:10:10,795 --> 00:10:14,256
♪ You're the one
♪ I secretly love.

187
00:10:14,281 --> 00:10:18,107
♪ But what would you do,
♪ if you were to know the truth?

188
00:10:18,132 --> 00:10:24,055
♪ That's why I love you, secretly...

189
00:10:24,080 --> 00:10:26,921
Who could even get akumatized
on Christmas Eve?

190
00:10:28,640 --> 00:10:30,068
Footprints...

191
00:10:34,953 --> 00:10:37,411
And you, sir?  Where are you going to
celebrate Christmas?

192
00:10:37,467 --> 00:10:39,689
Me?  Well, everywhere in Paris.

193
00:10:39,714 --> 00:10:41,111
The entire world, even.

194
00:10:42,568 --> 00:10:43,830
You're all alone, huh?

195
00:10:44,556 --> 00:10:46,254
Listen, here's what we're gonna do.

196
00:10:46,279 --> 00:10:48,298
Get me back and
stay at our place for dinner.

197
00:10:48,323 --> 00:10:50,677
Help me bring back the
Christmas spirit, sir.

198
00:10:50,772 --> 00:10:53,539
Well, Okay, but I won't be able to stay long.

199
00:10:53,564 --> 00:10:55,588
It's that I have a lot of work
on Christmas Eve.

200
00:10:55,613 --> 00:10:57,035
Thanks, Santa.

201
00:11:04,505 --> 00:11:05,894
Adrien?
It's you?

202
00:11:06,171 --> 00:11:07,040
And you, who are you?

203
00:11:07,065 --> 00:11:08,223
Well, it's obvious, no?

204
00:11:08,249 --> 00:11:09,617
I'm Santa Claus.

205
00:11:09,642 --> 00:11:10,578
Of course.

206
00:11:10,603 --> 00:11:11,929
And me, I'm the Easter Bunny.

207
00:11:11,954 --> 00:11:14,045
Aren't you ashamed to bother us
on Christmas Eve?

208
00:11:14,133 --> 00:11:15,666
Get lost or I'll send by bodyguard.

209
00:11:15,691 --> 00:11:16,905
Father, you're mistaken.

210
00:11:16,930 --> 00:11:17,952
No, he's right.

211
00:11:17,977 --> 00:11:19,294
Your father saw through his plans.

212
00:11:19,319 --> 00:11:21,676
This man is a supervillain
under the Papillon's hold.

213
00:11:21,701 --> 00:11:22,700
Wh-What?

214
00:11:22,725 --> 00:11:24,640
You've all lost your minds.

215
00:11:25,740 --> 00:11:26,796
No, Ladybug.

216
00:11:26,821 --> 00:11:28,811
Stop!  This man isn't akumatized.

217
00:11:28,836 --> 00:11:30,604
I know how to recognize an akumatized.

218
00:11:30,629 --> 00:11:31,961
Trust me.

219
00:11:34,358 --> 00:11:35,830
Go back home, it's safer.

220
00:11:35,855 --> 00:11:37,267
No, wait.

221
00:11:37,884 --> 00:11:39,199
Adrien.
Plagg.

222
00:11:42,453 --> 00:11:44,190
Go ponies, hup hup!

223
00:11:47,780 --> 00:11:50,817
Are you crazy,
what's gotten in to you?

224
00:11:51,874 --> 00:11:56,638
Without realizing it, you just gave me
a wonderful Christmas present, Ladybug.

225
00:11:56,663 --> 00:11:59,239
A poor innocent wrongly accused.

226
00:11:59,264 --> 00:12:01,378
And the spirit of a scorned holiday.

227
00:12:05,286 --> 00:12:07,649
It's all my akuma needs...

228
00:12:07,674 --> 00:12:10,154
...to darken this heart.

229
00:12:16,745 --> 00:12:18,008
Adrien?  Where are you going?

230
00:12:18,033 --> 00:12:19,294
To my room, of course.

231
00:12:19,319 --> 00:12:21,186
Since my father doesn't want to celebrate Christmas.

232
00:12:21,211 --> 00:12:22,077
You're wrong.

233
00:12:22,102 --> 00:12:24,581
Your father came to see you,
but you were out.

234
00:12:24,661 --> 00:12:26,154
I'll tell him you're waiting for him.

235
00:12:26,179 --> 00:12:29,424
But this time, we'll make sure
you stay in your room.

236
00:12:32,353 --> 00:12:32,957
You-

237
00:12:33,053 --> 00:12:34,880
You aren't a supervillain?

238
00:12:34,905 --> 00:12:36,199
Of course not.

239
00:12:36,224 --> 00:12:39,089
You read too many superhero stories, my dear.

240
00:12:39,204 --> 00:12:40,355
I'm sorry,

241
00:12:40,529 --> 00:12:41,503
I really thought-

242
00:12:41,528 --> 00:12:42,725
Don't touch me.

243
00:12:42,750 --> 00:12:44,336
You've done enough as it is.

244
00:12:52,914 --> 00:12:55,924
No one respects
the spirit of Christmas now days.

245
00:12:55,949 --> 00:12:57,465
They don't believe in me anymore.

246
00:13:01,437 --> 00:13:03,278
I believe in you.

247
00:13:03,561 --> 00:13:06,392
My dear Bother Christmas,
I am the Papillon.

248
00:13:06,417 --> 00:13:09,386
They wrongly accused you of
being a supervillain.

249
00:13:09,411 --> 00:13:12,032
In that case,
that's what you'll be from now on.

250
00:13:12,057 --> 00:13:15,133
In return, since I was good all year,

251
00:13:15,158 --> 00:13:17,990
I'll ask for not one, but two presents.

252
00:13:18,015 --> 00:13:20,951
Ladybug and Chat Noir's Miraculouses.

253
00:13:21,104 --> 00:13:23,152
And you'll have them.

254
00:13:23,177 --> 00:13:26,053
Merry Christmas to you, Papillon.

255
00:13:33,678 --> 00:13:36,179
Ladybug!

256
00:13:39,431 --> 00:13:43,366
♪ You think I'm going to accept
♪ you mocking me?

257
00:13:45,176 --> 00:13:50,462
♪ You think I'm going to let you
♪ accuse me like that?

258
00:13:51,896 --> 00:13:54,798
♪ I am Bother Christmas.

259
00:13:55,212 --> 00:13:57,627
♪ Wonderfully cruel.

260
00:13:57,703 --> 00:14:00,239
♪ I'm gonna scare you stiff.

261
00:14:00,897 --> 00:14:05,239
♪ It'll be an awful Christmas for all.

262
00:14:18,393 --> 00:14:19,507
Santa Claus?

263
00:14:19,532 --> 00:14:21,134
Not quite.

264
00:14:21,158 --> 00:14:24,150
♪ I am Bother Christmas.

265
00:14:24,370 --> 00:14:26,688
♪ Wonderfully cruel.

266
00:14:26,712 --> 00:14:29,248
♪ I'm gonna scare you stiff.

267
00:14:29,754 --> 00:14:34,096
♪ It'll be an awful Christmas for all.

268
00:14:36,127 --> 00:14:37,411
Don't worry, Adrien.

269
00:14:37,436 --> 00:14:39,768
I won't do anything to you.
I owe you.

270
00:14:39,875 --> 00:14:42,050
You gave me a gift
and I won't forget it.

271
00:14:42,075 --> 00:14:44,388
On the contrary,
I'll avenge you, even.

272
00:14:44,425 --> 00:14:46,075
Avenge us both.

273
00:14:46,100 --> 00:14:47,067
No, wait!

274
00:14:47,356 --> 00:14:49,974
It'll be the revenge Christmas.

275
00:14:54,292 --> 00:14:55,990
Plagg, transform me.

276
00:14:57,044 --> 00:14:59,969
It looks like Santa Claus came by,
my dear Chloe.

277
00:15:01,757 --> 00:15:04,125
I wish you all an awful Christmas.

278
00:15:04,971 --> 00:15:07,963
♪ I am Bother Christmas.

279
00:15:08,016 --> 00:15:10,334
♪ Wonderfully cruel.
Hey, my presents!

280
00:15:10,525 --> 00:15:13,061
♪ I'm gonna scare you stiff.

281
00:15:13,567 --> 00:15:17,909
♪ It'll be an awful Christmas for all.

282
00:15:24,051 --> 00:15:27,108
I wish you all an awful Christmas!

283
00:15:33,063 --> 00:15:35,182
It's you and me, Bother Christmas.

284
00:15:35,207 --> 00:15:36,860
You and us, My Lady.

285
00:15:36,885 --> 00:15:37,820
Chat Noir?

286
00:15:37,845 --> 00:15:38,771
Where were you?

287
00:15:38,796 --> 00:15:39,990
And what happened to Adrien?

288
00:15:40,015 --> 00:15:40,787
It's...

289
00:15:40,812 --> 00:15:42,010
It's a long story.

290
00:15:42,035 --> 00:15:43,867
Cats have their little secrets too.

291
00:15:47,613 --> 00:15:50,222
Who's gonna deliver the kids their presents
to night, Bother Christmas?

292
00:15:50,247 --> 00:15:52,072
All that is ancient history.

293
00:15:52,097 --> 00:15:54,094
Farewell presents and Christmas spirit.

294
00:15:54,119 --> 00:15:56,194
You should have been nice.

295
00:15:56,671 --> 00:15:57,983
I was nice all year.

296
00:15:58,008 --> 00:15:59,469
Obviously not enough.

297
00:16:02,994 --> 00:16:03,692
I got you.

298
00:16:08,108 --> 00:16:11,227
How about a little ride in the sledge,
little elves?

299
00:16:12,368 --> 00:16:13,664
Hang on tight!

300
00:16:13,898 --> 00:16:15,705
Here we go!

301
00:16:20,236 --> 00:16:22,470
It's not like that he'll get his sledge license.

302
00:16:26,346 --> 00:16:29,188
It's time to open your presents, kids.

303
00:16:30,658 --> 00:16:31,587
I'll take care of the sledge.

304
00:16:31,612 --> 00:16:32,747
I'll leave Santa to you.

305
00:16:47,032 --> 00:16:48,790
Whoa, easy there.

306
00:16:54,217 --> 00:16:55,112
Thank you so much.

307
00:16:56,173 --> 00:16:57,066
Ladybug!

308
00:16:57,230 --> 00:16:58,836
It's happening again.

309
00:17:08,296 --> 00:17:09,947
Ladybug?  Chat Noir?

310
00:17:09,972 --> 00:17:11,391
That's one Christmas scoop.

311
00:17:11,416 --> 00:17:13,164
A little word for my Ladyblog?

312
00:17:13,189 --> 00:17:15,571
Oh, erm, no no, it's not what it looks like.

313
00:17:18,739 --> 00:17:19,707
No, wait.

314
00:17:19,732 --> 00:17:20,564
I have another idea.

315
00:17:21,147 --> 00:17:22,449
Lucky Charm!

316
00:17:28,928 --> 00:17:29,636
Oh, top notch.

317
00:17:29,661 --> 00:17:31,374
With that, for sure,
it's in the box.

318
00:17:33,455 --> 00:17:35,637
I'm gonna need tape, scissors,
that kind of stuff.

319
00:17:35,662 --> 00:17:36,566
You have that here?

320
00:17:36,974 --> 00:17:37,609
Up there.

321
00:17:41,658 --> 00:17:42,690
Here too.

322
00:17:46,182 --> 00:17:46,722
Here.

323
00:17:49,054 --> 00:17:50,237
My Christmas list.

324
00:17:50,261 --> 00:17:52,581
Run by the Dupain-Cheng bakery
and bring me this.

325
00:17:52,606 --> 00:17:53,971
We'll meet at the Eiffel Tower.

326
00:17:53,996 --> 00:17:54,949
I'll explain everything there.

327
00:17:54,974 --> 00:17:57,611
I've always dreamed of being your Santa.

328
00:17:59,492 --> 00:18:00,540
Don't worry.

329
00:18:00,565 --> 00:18:02,024
Everything will go back to before.

330
00:18:03,385 --> 00:18:04,978
Good luck, Ladybells.

331
00:18:23,769 --> 00:18:24,634
It's me?

332
00:18:24,880 --> 00:18:27,451
♪ Bother Christmas,
♪ this time it's settled.

333
00:18:27,475 --> 00:18:29,769
♪ We give up, you won.

334
00:18:29,793 --> 00:18:32,577
♪ No one to come rescue us,

335
00:18:32,602 --> 00:18:35,175
♪ we give you our Miraculouses.

336
00:18:35,364 --> 00:18:37,658
♪ But before that.

337
00:18:37,683 --> 00:18:40,397
♪ We have a gift for you.

338
00:18:40,507 --> 00:18:46,245
♪ For, on Christmas Eve,
♪ even you, you have a right to it.

339
00:18:46,560 --> 00:18:47,631
A present?

340
00:18:47,751 --> 00:18:48,715
For me?

341
00:18:48,740 --> 00:18:49,671
Don't listen to him.

342
00:18:49,696 --> 00:18:51,357
It has to be a trap.

343
00:18:51,381 --> 00:18:54,105
♪ You scare us so much.

344
00:18:54,129 --> 00:18:56,819
♪ You're so cruel.

345
00:18:56,843 --> 00:19:02,622
♪ But we all have the right to a present
♪ on Christmas Eve.

346
00:19:09,240 --> 00:19:10,407
Merry Christmas!

347
00:19:14,188 --> 00:19:15,600
Cataclysm!

348
00:19:28,564 --> 00:19:29,921
Merry Christmas, My Lady.

349
00:19:29,946 --> 00:19:31,228
Thank you, Kitty.

350
00:19:34,365 --> 00:19:36,697
You've done enough harm, little Akuma.

351
00:19:40,271 --> 00:19:41,557
I'm freeing you from evil.

352
00:19:46,259 --> 00:19:46,918
Gotcha.

353
00:19:47,895 --> 00:19:49,499
Bye-bye, little butterfly.

354
00:19:50,168 --> 00:19:52,019
Miraculous Ladybug.

355
00:20:04,681 --> 00:20:05,649
Good job.

356
00:20:06,439 --> 00:20:07,348
Ladybug.

357
00:20:07,373 --> 00:20:08,380
Chat Noir.

358
00:20:08,543 --> 00:20:10,764
you can very well have a Merry Christmas.

359
00:20:10,789 --> 00:20:14,470
We'll see next year who
will have the best presents.

360
00:20:19,042 --> 00:20:19,772
Adrien.

361
00:20:20,272 --> 00:20:22,693
I can't accept you disappearing too.

362
00:20:22,902 --> 00:20:24,930
I couldn't bear losing you.

363
00:20:31,008 --> 00:20:32,445
You have visitors.

364
00:20:36,434 --> 00:20:39,529
I took the liberty of informing them
that Adrien was safe and sound, sir.

365
00:20:39,554 --> 00:20:41,138
They were worried for their friend.

366
00:20:43,874 --> 00:20:46,644
Please, Father, it's... it's Christmas spirit.

367
00:20:47,323 --> 00:20:48,061
Very well.

368
00:20:48,086 --> 00:20:49,101
Come in.

369
00:20:52,276 --> 00:20:53,664
Merry Christmas, Adrien.

370
00:20:54,995 --> 00:20:59,752
♪ Merry Christmas to all.

371
00:21:00,408 --> 00:21:01,758
Merry Christmas, mom.

