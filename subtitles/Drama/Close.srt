1
00:02:17,998 --> 00:02:19,695
You know the code?

2
00:02:24,271 --> 00:02:26,311
Nope, they don't open up.

3
00:02:30,919 --> 00:02:32,590
Give back my bag!

4
00:02:35,178 --> 00:02:36,563
Wait a moment!

5
00:02:39,889 --> 00:02:41,956
Give back the fucking bag!

6
00:02:43,883 --> 00:02:44,958
Come on!

7
00:02:46,900 --> 00:02:49,033
Give already back the bag!

8
00:03:06,907 --> 00:03:09,041
But who do you wanna talk to?

9
00:03:10,554 --> 00:03:11,983
You may guess.

10
00:03:13,624 --> 00:03:17,143
Don't take that tone.
Well, guess now where I am.

11
00:03:18,460 --> 00:03:20,918
Seriously, I want my things back.

12
00:03:21,038 --> 00:03:22,634
Who doesn't want that?

13
00:03:23,268 --> 00:03:24,999
I'll set the cops on you!

14
00:03:25,119 --> 00:03:28,156
Wait. What do you mean,
"I'll set the cops on you"?

15
00:03:28,276 --> 00:03:31,877
Listen now. The first step is:
Let your phone get blocked.

16
00:03:31,997 --> 00:03:34,102
That's sick, you're a jerk.

17
00:04:16,574 --> 00:04:17,535
Finally.

18
00:04:18,812 --> 00:04:21,437
Who are you actually?
Come on, open up.

19
00:04:25,292 --> 00:04:29,810
Hi Paul, new girlfriend?
- Fuck off, go find another meadow!

20
00:09:01,172 --> 00:09:04,432
Go to the bathroom.
You're dripping all over.

21
00:11:40,289 --> 00:11:42,170
My mother's bad again.

22
00:11:44,008 --> 00:11:47,308
I was with her the whole night.
I just came in now.

23
00:11:52,322 --> 00:11:54,076
Don't be angry, please.

24
00:11:57,164 --> 00:11:58,591
And how was it?

25
00:12:21,398 --> 00:12:23,403
Best wishes to your mom.

26
00:12:32,777 --> 00:12:34,972
How's your name?
- Guess.

27
00:12:37,222 --> 00:12:38,588
You shall guess.

28
00:12:39,322 --> 00:12:41,143
Steffen.
- Nope.

29
00:12:42,818 --> 00:12:45,614
Steffen, Thomas.

30
00:12:48,282 --> 00:12:49,354
More.

31
00:12:59,735 --> 00:13:02,354
Tom.
- With "Z".

32
00:13:04,514 --> 00:13:07,770
"Z". Zorro.
- Right, that's my name.

33
00:13:09,625 --> 00:13:11,615
"Z".
- Yes, Zorro.

34
00:13:20,174 --> 00:13:22,943
Tom. I believe your name's Tom.

35
00:13:40,219 --> 00:13:41,690
Open the door.

36
00:14:00,510 --> 00:14:02,743
Can you please
take it with you.

37
00:16:45,757 --> 00:16:47,489
Good evening, Anna.

38
00:16:48,429 --> 00:16:50,505
Has anything happened?
- No.

39
00:16:51,173 --> 00:16:53,443
Just a routine check.
Sit.

40
00:16:53,563 --> 00:16:54,592
Sit!

41
00:16:57,929 --> 00:17:00,499
We didn't meet for a while.
- No.

42
00:17:01,404 --> 00:17:04,702
So I thought I'd have a look.

43
00:17:06,531 --> 00:17:09,485
They don't get you out of here, right?

44
00:17:12,385 --> 00:17:15,542
You're still not afraid,
all alone here?

45
00:17:16,591 --> 00:17:17,728
Not me.

46
00:17:19,903 --> 00:17:24,661
The flat in the backyard of our place
is still available, I asked again.

47
00:17:26,515 --> 00:17:27,675
Thank you.

48
00:17:30,189 --> 00:17:33,084
Well,
don't let them push you around.

49
00:17:35,693 --> 00:17:38,353
I better take this with me, okay?

50
00:17:41,755 --> 00:17:45,004
Have a nice evening, Anna.
Come on. Heel!

51
00:19:27,933 --> 00:19:29,529
Did I wake you up?

52
00:19:43,082 --> 00:19:44,447
Good night.

53
00:23:05,348 --> 00:23:06,556
Well, and now?!

54
00:23:08,628 --> 00:23:11,289
Come down here!
Someone's going mad here.

55
00:24:27,216 --> 00:24:29,172
Hello, Anna isn't at home.

56
00:24:33,158 --> 00:24:35,261
I'm downstairs at your door.

57
00:24:38,508 --> 00:24:40,027
It's really annoying,

58
00:24:40,147 --> 00:24:44,789
I fix a date to look at that flat and
meet up there and you don't come.

59
00:24:46,947 --> 00:24:49,153
Is it about your mother again?

60
00:24:52,197 --> 00:24:53,345
Shit!

61
00:25:03,020 --> 00:25:04,673
You're okay, Anna?

62
00:25:08,428 --> 00:25:10,833
You must write the letter.

63
00:25:14,590 --> 00:25:16,713
Otherwise they'll get you out.

64
00:25:30,156 --> 00:25:32,936
Here's the number: 482 42 472.

65
00:25:36,145 --> 00:25:40,163
Look at the flat, okay?
And then call me back.

66
00:26:30,280 --> 00:26:33,405
Hey, you're Susanne, right?
- Anything happened?

67
00:26:33,525 --> 00:26:35,022
Anna isn't at home.

68
00:26:36,175 --> 00:26:37,720
We had an appointment.

69
00:26:37,840 --> 00:26:41,514
She's at her mother's for a few days.
She wasn't well again.

70
00:26:43,642 --> 00:26:46,689
Patrick.
She'll tell you eventually.

71
00:26:47,656 --> 00:26:50,458
At the moment she's busy.
Understandable, right?

72
00:26:50,578 --> 00:26:52,454
Can I take a message?

73
00:26:52,698 --> 00:26:53,591
No.

74
00:26:58,079 --> 00:27:00,275
Ciao Susanne. It was nice.

75
00:27:33,165 --> 00:27:34,918
I know you're there.

76
00:27:43,145 --> 00:27:46,411
You're hiding, right?
And Susanne doesn't get it.

77
00:27:50,073 --> 00:27:53,422
I've understood.
You don't want to go there.

78
00:27:58,213 --> 00:28:02,455
It's really good I'm here.
If only for your mail.

79
00:28:06,509 --> 00:28:08,654
Well that clinic looks cool.

80
00:28:09,791 --> 00:28:12,223
From your ill mother.
From Madeira.

81
00:28:14,221 --> 00:28:16,964
And here, bill, reminder.

82
00:28:17,911 --> 00:28:20,165
House administration.
AOL.

83
00:28:23,449 --> 00:28:27,268
Inkasso. Bank, credit card cancelled.

84
00:28:38,362 --> 00:28:42,390
Dear Anna, all the best
for a new year in your life...

85
00:28:42,872 --> 00:28:47,452
I hope you'll visit me soon.
Good luck, darling. Your grandma Mimi.

86
00:28:52,175 --> 00:28:53,406
20 Euro.

87
00:29:12,092 --> 00:29:13,285
Open up.

88
00:29:56,939 --> 00:29:59,208
Why don't you open the door?

89
00:30:19,480 --> 00:30:21,069
Want to eat something?

90
00:30:22,360 --> 00:30:24,202
I've been shopping.

91
00:30:36,967 --> 00:30:38,571
Been shopping?

92
00:30:40,462 --> 00:30:43,175
Just before with Susanne, right?

93
00:30:57,307 --> 00:30:59,754
It doesn't work with everybody!

94
00:31:07,171 --> 00:31:10,698
What will you do when
your mother gets really bad...

95
00:33:47,593 --> 00:33:49,533
Mrs. Meissner!

96
00:34:13,566 --> 00:34:16,125
Property Administration

97
00:34:39,330 --> 00:34:41,458
Anna's not at home...

98
00:34:57,320 --> 00:34:59,626
Anna's not at home...

99
00:37:14,070 --> 00:37:15,447
Who's there?

100
00:37:16,647 --> 00:37:17,651
Guess.

101
00:37:21,311 --> 00:37:23,811
Won't you let me in?
It's late already.

102
00:37:24,893 --> 00:37:26,531
How's the code?

103
00:37:40,700 --> 00:37:42,439
Where are my things?

104
00:37:46,553 --> 00:37:49,600
And the rest?
- Let me in first.

105
00:37:50,513 --> 00:37:53,517
I don't even know your name?
- Guess.

106
00:37:57,182 --> 00:37:58,237
Anton.

107
00:37:59,069 --> 00:38:00,583
Right, Anton.

108
00:38:03,204 --> 00:38:05,805
Buy me a beer
and you get your phone.

109
00:40:08,930 --> 00:40:10,275
Have a light?

110
00:40:24,731 --> 00:40:25,847
Keep it.

111
00:40:30,758 --> 00:40:32,797
Didn't he say keep it?

112
00:40:32,917 --> 00:40:36,022
Do we know each other?
'Cause you're informal.

113
00:40:36,142 --> 00:40:37,264
Leave him.

114
00:40:58,121 --> 00:41:01,972
If you don't stop now...
- Don't get provoked by him.

115
00:41:03,255 --> 00:41:07,054
Who knows what's wrong with him?
It's enough now, okay?

116
00:41:08,191 --> 00:41:09,646
Enough now!

117
00:44:41,855 --> 00:44:43,498
Don't move.

118
00:51:08,826 --> 00:51:10,777
You like it?

119
00:51:15,136 --> 00:51:16,920
It hurts.

120
00:51:22,701 --> 00:51:26,364
Go to the apotek and get something.
- Soon.

121
00:51:28,375 --> 00:51:31,336
I'm hungry too.
That soup doesn't help.

122
00:51:35,940 --> 00:51:37,146
I'm dizzy.

123
00:51:39,900 --> 00:51:43,364
You're dizzy?
Come on now, I can't even get up.

124
00:51:50,435 --> 00:51:53,122
I want you to get me some pills.

125
00:51:55,804 --> 00:51:58,402
Can't I rest a little more?

126
00:51:58,672 --> 00:52:00,232
Well, alright.

127
00:53:31,829 --> 00:53:35,617
It suits well for Anna that
I'm lying here, right?

128
00:53:43,742 --> 00:53:48,532
Is it really so difficult to go to
the closestW apotek? - Come on!

129
00:54:16,581 --> 00:54:18,697
Susanne's coming tomorrow.

130
00:54:22,823 --> 00:54:26,517
She's bringing films for us.
Anything you want to watch?

131
00:54:31,933 --> 00:54:35,187
That's annoying. Get lost.
I'm not your lapdog.

132
00:54:41,470 --> 00:54:46,072
Get lost, neurotic bitch! Don't
touch me with your yellow fingers!

133
00:56:41,041 --> 00:56:43,070
What are you spying here?

134
00:56:43,451 --> 00:56:44,735
You're sick.

135
00:57:07,578 --> 00:57:09,394
Come on, get lost.

136
00:57:12,133 --> 00:57:13,980
Come on.

137
00:57:19,611 --> 00:57:22,606
Now you can see...

138
00:58:00,359 --> 00:58:02,300
Untie me at once.

139
00:58:05,838 --> 00:58:08,415
Untie me!

140
00:58:35,275 --> 00:58:38,583
Get off the bonds!

141
00:58:40,935 --> 00:58:44,577
You shall get off the bonds,
stupid cow!

142
00:59:10,864 --> 00:59:12,795
Sorry, I didn't mean it.

143
00:59:14,469 --> 00:59:18,924
You can breath through the nose.
Don't panic, okay?

144
01:00:33,507 --> 01:00:35,113
Don't move so much.

145
01:01:19,850 --> 01:01:21,929
How shall this go on?

146
01:01:29,890 --> 01:01:32,599
You're sick. Completely schizo.
- Really?

147
01:01:34,545 --> 01:01:36,348
Well, you must know it.

148
01:01:43,247 --> 01:01:44,336
Wanna smoke?

149
01:02:34,889 --> 01:02:36,845
Have you been to the doctor?

150
01:02:37,894 --> 01:02:41,755
No, it's just migraine.
- It looks awful downstairs.

151
01:02:43,268 --> 01:02:45,532
Someone has emptied the rubbish.

152
01:02:45,652 --> 01:02:48,437
They really want to gross you out, huh?

153
01:02:50,839 --> 01:02:52,430
Finally it's finished.

154
01:02:53,041 --> 01:02:56,811
Have you talked with Sacha?
- Susanne, please not now...

155
01:03:05,214 --> 01:03:06,686
Don't be angry.

156
01:03:08,673 --> 01:03:10,885
Thank you for the stuff.
Here.

157
01:03:12,409 --> 01:03:15,309
I'm freezing.
I'd like to lay down again.

158
01:03:16,939 --> 01:03:19,609
Go to bed. I'll make you a tea.

159
01:03:21,134 --> 01:03:23,237
Tomorrow it'll be better.

160
01:03:23,357 --> 01:03:25,246
When's your next tour?

161
01:03:25,366 --> 01:03:28,169
Monday I'm in Istanbul with Ren�.

162
01:03:32,216 --> 01:03:33,708
Kind regards.

163
01:03:34,021 --> 01:03:36,666
I think he still has a crush on me.

164
01:03:36,786 --> 01:03:38,529
It's more than a year ago.

165
01:03:38,649 --> 01:03:42,148
I don't care about it right now.
Can't you leave me alone today?

166
01:03:42,268 --> 01:03:44,234
Any noise is painful for me.

167
01:03:48,499 --> 01:03:50,453
You must go to the doctor.

168
01:03:54,352 --> 01:03:55,906
Birthday present?

169
01:03:58,923 --> 01:04:02,048
It felt strange
when I met him on the stairs.

170
01:04:06,521 --> 01:04:08,832
How is he?
- Leave me alone.

171
01:04:16,497 --> 01:04:19,701
Anna,
the excavators will start in 3 days.

172
01:04:35,282 --> 01:04:36,566
You aren't well.

173
01:04:37,858 --> 01:04:39,998
Let's talk another time.
Okay?

174
01:04:44,513 --> 01:04:46,080
I'll get my lamp.

175
01:04:48,927 --> 01:04:51,097
I wanna go back to bed.

176
01:04:55,742 --> 01:04:58,467
What's going on now?
Come on.

177
01:05:00,299 --> 01:05:02,043
I want back in bed now.

178
01:05:02,522 --> 01:05:03,928
Let go, Anna!

179
01:05:10,536 --> 01:05:13,907
Are you crazy?
- Everything's fine.

180
01:05:15,430 --> 01:05:17,537
Why are you lying, huh?

181
01:05:18,009 --> 01:05:22,095
You're lying to all of us.
Your mother isn't ill actually.

182
01:05:24,703 --> 01:05:26,373
Go away, please.

183
01:05:33,079 --> 01:05:35,239
Anna, I'm your friend!

184
01:05:40,945 --> 01:05:43,831
What shall I do, Anna!
- You shall go away!

185
01:05:55,843 --> 01:05:57,719
Am I just hysterical?

186
01:06:01,703 --> 01:06:03,307
Will you call me?

187
01:13:51,127 --> 01:13:52,568
Get off my back!

188
01:13:57,231 --> 01:13:58,651
You shall leave!

189
01:14:32,616 --> 01:14:34,000
Come on. Open up.

190
01:14:44,796 --> 01:14:47,033
You shall open the door!
- No.

191
01:15:21,768 --> 01:15:23,634
Now we'll go out!
- No!

192
01:15:39,305 --> 01:15:40,358
Come on.

193
01:15:41,696 --> 01:15:42,949
Here we go.

194
01:16:46,054 --> 01:16:47,504
Come on now.

195
01:16:48,850 --> 01:16:50,037
You're sick.

196
01:16:57,999 --> 01:16:59,643
Let go, you swine!

197
01:17:02,405 --> 01:17:04,475
You're even sicker than I am.

198
01:17:21,384 --> 01:17:23,146
Then just die up there!

199
01:19:38,683 --> 01:19:39,667
Get out.

200
01:19:44,439 --> 01:19:45,449
Get out!

201
01:23:18,935 --> 01:23:22,599
Subtitles:
The Huge Animal From The North

