0
00:00:45,000 --> 00:01:00,100
Original Sub by Cycles

1
00:01:25,000 --> 00:01:27,800
The fear in her eyes and
the knife in the chest

2
00:01:28,100 --> 00:01:30,400
That's my last memory of my mother

3
00:01:30,800 --> 00:01:33,200
That's why I had to go
to prison for four years

4
00:01:33,200 --> 00:01:34,900
Even though she survived it

5
00:01:35,700 --> 00:01:37,700
After that it went good for a while

6
00:01:37,700 --> 00:01:39,600
Until I met that 70 year old

7
00:01:39,900 --> 00:01:42,000
"Oh my god" were her last words

8
00:01:42,000 --> 00:01:43,400
Then she was dead

9
00:01:43,500 --> 00:01:45,800
I didn't know her and I didn't steal anything

10
00:01:46,000 --> 00:01:48,000
That damned idea was suddenly there

11
00:01:48,000 --> 00:01:49,300
I just had to

12
00:01:49,500 --> 00:01:51,100
I couldn't explain it

13
00:01:51,200 --> 00:01:53,700
So I went to prison for 10 years

14
00:01:56,300 --> 00:01:59,400
Then the psychologists got interested
in my inner life

15
00:02:02,600 --> 00:02:06,300
When they asked me about my dreams,
I told them about flowers

16
00:02:06,300 --> 00:02:07,200
Flowers

17
00:02:07,400 --> 00:02:08,700
Always just flowers

18
00:02:09,400 --> 00:02:12,800
Apart from that, I could fantasize
in my mind whatever I wanted

19
00:02:12,900 --> 00:02:15,800
It helped me control this urge to...

20
00:02:16,700 --> 00:02:18,800
Just let it all out

21
00:02:38,900 --> 00:02:42,700
Prison exists so one
can better oneself

22
00:02:42,900 --> 00:02:48,200
But that urge to torture a human,
that's one thing I never could get rid of

23
00:03:34,800 --> 00:03:36,400
If you sum everything up

24
00:03:36,600 --> 00:03:40,100
Then I've been behind bars more than
half of my life

25
00:03:40,400 --> 00:03:44,300
Only, I never commited a crime
out of pure joy

26
00:03:44,500 --> 00:03:47,900
There was always a little something,
that was added to it

27
00:04:43,000 --> 00:04:46,100
I knew, It will happen again

28
00:04:46,300 --> 00:04:47,600
It has to

29
00:04:47,900 --> 00:04:50,300
But this time they won't get me

30
00:05:00,700 --> 00:05:02,600
Nobody came to pick me up

31
00:05:02,800 --> 00:05:05,200
No family, Nothing

32
00:05:05,700 --> 00:05:07,800
I would've been too excited anyway

33
00:05:08,400 --> 00:05:11,300
I couldn't sleep the whole last night

34
00:05:13,300 --> 00:05:14,800
Finally free

35
00:05:23,000 --> 00:05:25,400
I already had a concrete plan

36
00:05:25,500 --> 00:05:27,300
The details were all fixed

37
00:05:27,900 --> 00:05:31,500
Now it was only neccesary to find somebody

38
00:05:31,900 --> 00:05:34,100
But I didn't know where I was

39
00:05:34,300 --> 00:05:36,800
I've lived in this town for 10 years

40
00:05:37,000 --> 00:05:41,200
Only, behind bars you can't get
to know a city

41
00:05:41,400 --> 00:05:44,700
My first question was: Where to?

42
00:05:44,900 --> 00:05:48,300
I thought: Where can you meet somebody?

43
00:05:48,900 --> 00:05:52,500
So I went to the first Caf� that was open

44
00:06:41,800 --> 00:06:44,300
There were two attractive girls
sitting in there

45
00:06:44,800 --> 00:06:46,700
They were still pretty young

46
00:06:47,100 --> 00:06:49,000
But that would've worked

47
00:07:07,700 --> 00:07:11,100
I felt a bit awkward, dressed as I was

48
00:07:12,100 --> 00:07:15,500
But after 10 years of prison,
you can't be dressed modern

49
00:07:18,100 --> 00:07:20,000
They stared at me

50
00:07:24,500 --> 00:07:26,500
I was on it immediately

51
00:07:36,400 --> 00:07:38,700
They literally provoked me

52
00:07:41,500 --> 00:07:44,300
My fantasy immediately began to go wild

53
00:08:13,200 --> 00:08:16,200
I wondered how I could approach them

54
00:08:16,500 --> 00:08:18,600
That excited me enormously

55
00:08:19,100 --> 00:08:22,200
I immediately imagined the greatest things

56
00:08:25,600 --> 00:08:28,100
But it would've caused too much
attention in the Caf�

57
00:08:28,200 --> 00:08:30,100
Everyone would have heard us

58
00:08:30,200 --> 00:08:32,700
That wasn't a good opportunity

59
00:09:14,700 --> 00:09:18,700
It was absolutely neccesary to find
a quiet place

60
00:09:20,100 --> 00:09:22,600
and everything would've gone on well

61
00:09:24,400 --> 00:09:26,700
Then a coincidence helped me

62
00:09:50,600 --> 00:09:52,200
- Where to?

63
00:09:52,500 --> 00:09:54,500
Straigt ahead, please

64
00:09:56,800 --> 00:09:59,300
The opportunity in the Taxi was great

65
00:09:59,600 --> 00:10:03,600
It wouldn't be a robbery,
I wasn't interested in money

66
00:10:11,300 --> 00:10:15,500
That woman reminded me of my
first girlfriend, Anna-Marie

67
00:10:18,200 --> 00:10:20,200
A real whore

68
00:10:22,200 --> 00:10:25,800
I was 14 at the time, and she was
significantly older than me

69
00:10:26,400 --> 00:10:28,400
She nearly could've been my mother

70
00:10:29,500 --> 00:10:32,000
We sat on a bench in the park

71
00:10:32,600 --> 00:10:34,800
I had to remove a shoe

72
00:10:35,000 --> 00:10:37,900
She took the shoe away from me...
- What are you doing?

73
00:10:38,700 --> 00:10:41,200
she only would return it to me

74
00:10:41,300 --> 00:10:44,100
...if I gave her a kiss
- What are you doing there?

75
00:10:44,100 --> 00:10:47,000
Anna-Marie then took me home with her

76
00:10:47,100 --> 00:10:48,700
She seduced me

77
00:10:48,900 --> 00:10:51,800
That's when I was able to get active
for the first time

78
00:10:52,400 --> 00:10:55,800
First I tied her up, then sealed
her mouth shut...

79
00:10:56,100 --> 00:10:57,800
Then I hit her

80
00:10:58,300 --> 00:11:00,900
With a belt and later with a real whip

81
00:11:02,000 --> 00:11:03,800
She couldn't get enough

82
00:11:04,300 --> 00:11:06,500
I really enjoyed it

83
00:11:06,800 --> 00:11:10,600
I became Anna-Marie's slave,
and it really excited me

84
00:11:12,100 --> 00:11:13,900
Hey What are you doing !!

85
00:13:17,700 --> 00:13:19,800
Nobody had followed me

86
00:13:20,800 --> 00:13:24,900
Where to and how long I'd run, I don't know

87
00:13:25,700 --> 00:13:27,800
I didn't know that area

88
00:13:28,100 --> 00:13:30,000
I'd never been there

89
00:15:02,200 --> 00:15:04,600
The house seemed desolate to me

90
00:15:05,400 --> 00:15:08,100
I wasn't sure if somebody lived in there

91
00:15:35,500 --> 00:15:37,700
I noticed no one

92
00:15:37,900 --> 00:15:39,700
and didn't hear anything

93
00:15:45,900 --> 00:15:48,200
No human soul around

94
00:15:53,000 --> 00:15:56,100
I thought, that's a paradise for me

95
00:16:12,600 --> 00:16:15,400
The house was well protected
by the park and the trees

96
00:16:15,700 --> 00:16:18,200
I didn't have to be afraid of neighbours here

97
00:16:18,900 --> 00:16:21,000
The locatin was perfect

98
00:16:21,500 --> 00:16:23,700
No danger of being spotted here

99
00:18:33,100 --> 00:18:35,200
I could hardly breathe

100
00:18:37,300 --> 00:18:39,100
I was incredibly excited

101
00:18:47,200 --> 00:18:49,900
The opportunity to meet someone in here...

102
00:18:49,900 --> 00:18:51,800
nearly made me go mad

103
00:18:57,700 --> 00:19:01,300
I thought I couldn't take it much longer

104
00:19:45,000 --> 00:19:48,000
I was sure someone lived in here

105
00:19:50,500 --> 00:19:51,900
My victims

106
00:19:54,600 --> 00:19:56,400
I was afraid

107
00:20:00,300 --> 00:20:01,900
Daddy?

108
00:20:04,400 --> 00:20:05,800
Not of the law

109
00:20:07,000 --> 00:20:12,600
I was in a state of mind that excluded
every kind of logic

110
00:20:16,100 --> 00:20:18,500
I was afraid of myself

111
00:20:25,000 --> 00:20:27,600
Already earlier in my life,
I had been in great fear

112
00:20:28,600 --> 00:20:32,400
For example, as a child it was impossible for
me to be alone in a room

113
00:20:40,800 --> 00:20:46,800
My grandmother once locked me alone in a
dark room for a whole night

114
00:20:48,200 --> 00:20:50,600
I thought she was trying to kill me

115
00:21:00,700 --> 00:21:02,800
That I'd die of fear

116
00:21:07,800 --> 00:21:10,100
The only thing that helped me
were those sounds

117
00:21:42,400 --> 00:21:44,100
What luck I had

118
00:21:44,400 --> 00:21:46,000
What luck I had

119
00:21:46,200 --> 00:21:48,300
This house was perfect for me

120
00:21:52,100 --> 00:21:54,400
My plan would work

121
00:24:52,500 --> 00:24:53,600
Mommy

122
00:24:53,900 --> 00:24:55,500
Daddy is here

123
00:24:55,500 --> 00:24:58,800
Don't be stupid, he's long been dead

124
00:25:34,600 --> 00:25:37,900
As a child, I always had to fear for my life

125
00:25:38,800 --> 00:25:42,200
First my mother threw out my father

126
00:25:47,400 --> 00:25:50,300
Then she lived with another man

127
00:25:50,900 --> 00:25:52,400
My stepfather

128
00:25:52,800 --> 00:25:55,200
And she tried to kill me

129
00:25:56,100 --> 00:25:58,300
She wrapped me in wet diapers

130
00:25:58,400 --> 00:26:00,200
and put me in front of
the open window

131
00:26:01,000 --> 00:26:04,500
So I should get cold at the window
and freeze to death

132
00:26:11,000 --> 00:26:12,500
Sylvia!

133
00:26:20,900 --> 00:26:21,900
Sylvia...

134
00:26:23,300 --> 00:26:24,400
Sylvia...

135
00:26:42,100 --> 00:26:45,100
I was never safe with my mother

136
00:26:46,500 --> 00:26:48,500
She hated boys

137
00:26:51,700 --> 00:26:56,600
For my mother, I was only an attempt
to get a girl

138
00:26:57,500 --> 00:26:59,700
She didn't want a boy like me

139
00:27:00,700 --> 00:27:04,800
She developed a real infatuation
for my sister

140
00:27:16,600 --> 00:27:18,700
I didn't exist for her

141
00:27:19,400 --> 00:27:23,100
My sister could do with me
whatever she wanted

142
00:27:36,500 --> 00:27:39,100
My mother always just laughed

143
00:27:41,100 --> 00:27:44,700
But one time, I caught my sister alone

144
00:27:45,600 --> 00:27:47,900
I really gave it to her then

145
00:27:48,300 --> 00:27:50,400
I wanted to hit her until she was dead

146
00:27:50,500 --> 00:27:54,200
In her eyes I saw that she was afraid of me

147
00:27:54,100 --> 00:27:56,700
That excited me enormously

148
00:28:02,300 --> 00:28:07,200
Because my mother didn't want me,
I grew up at my grandmother's

149
00:28:09,000 --> 00:28:13,000
But she always just said that she
had to be ashamed of me

150
00:28:13,300 --> 00:28:15,700
That I was a shame for the whole family

151
00:28:16,600 --> 00:28:18,700
Because I was an illegitimate child

152
00:28:28,500 --> 00:28:30,400
She was incredibly religious

153
00:28:31,000 --> 00:28:34,400
...and to purge the guilt,
she sent me to a convent

154
00:28:37,400 --> 00:28:39,300
I didn't want to

155
00:28:43,900 --> 00:28:46,100
But I had to become a priest

156
00:29:23,400 --> 00:29:26,200
We students lived in that convent

157
00:29:27,000 --> 00:29:30,600
...and the nuns and priests had a farm there

158
00:29:31,300 --> 00:29:33,500
There were all kinds of animals there

159
00:29:33,900 --> 00:29:37,900
One afternoon, I secretly snuck into the barn

160
00:29:38,100 --> 00:29:42,900
...and used a knife until the blood gushed

161
00:29:43,900 --> 00:29:45,400
It was a pig

162
00:29:45,900 --> 00:29:52,000
It was afraid of me and squeaked so loudly
that the nuns came and caught me

163
00:30:18,000 --> 00:30:21,100
After this incident I had to leave
the convent

164
00:30:23,500 --> 00:30:27,200
For my grandmother, a world collapsed

165
00:30:27,500 --> 00:30:29,600
She was so religious...

166
00:30:30,400 --> 00:30:32,800
Now I was no longer allowed to become a priest

167
00:31:51,700 --> 00:31:54,900
After the incident at the convent,
my mother said

168
00:31:55,500 --> 00:31:58,200
that my family had to be afraid of me

169
00:32:00,300 --> 00:32:02,900
For her, I now was a monster

170
00:32:05,900 --> 00:32:09,700
She punished me by not letting
me back to my grandmother

171
00:32:14,400 --> 00:32:18,700
That was because my stepfather decided
to take over my upbringing

172
00:32:21,300 --> 00:32:23,100
But he hated me

173
00:32:24,700 --> 00:32:27,000
He was always good to my sister

174
00:32:28,600 --> 00:32:30,900
But He couldn't stand me

175
00:32:31,000 --> 00:32:33,800
She was never treated badly

176
00:32:35,100 --> 00:32:39,400
One time, he hit me so hard, that I had
to crawl on the floor

177
00:32:40,200 --> 00:32:44,500
My mother and my sister just stood there
and laughed at me

178
00:34:47,000 --> 00:34:49,800
I wanted to take revenge on my stepfather

179
00:34:51,700 --> 00:34:54,400
I wanted to kill him, out of rage

180
00:34:55,400 --> 00:34:56,700
Daddy

181
00:34:56,800 --> 00:34:58,600
But I was too weak

182
00:34:58,800 --> 00:35:00,500
So I had to run away from home

183
00:35:00,700 --> 00:35:02,700
I slept in the park

184
00:35:03,700 --> 00:35:06,000
There I observed a swan

185
00:35:10,100 --> 00:35:12,200
First I strangled it

186
00:35:12,600 --> 00:35:14,600
Then I cut off its head

187
00:35:17,700 --> 00:35:22,100
It was expelled from the flock and
didn't go back with them in the water

188
00:35:22,600 --> 00:35:25,800
It lived all by itself in a corner
of the park

189
00:35:26,700 --> 00:35:28,600
It liked humans very much

190
00:35:28,900 --> 00:35:30,900
But it was afraid of me

191
00:35:32,100 --> 00:35:36,000
I imagined, that I was taking my revenge then

192
00:36:30,600 --> 00:36:32,600
Everything went wrong

193
00:36:36,300 --> 00:36:38,000
Everything happened to fast

194
00:36:41,200 --> 00:36:44,300
I had it imagined totally differently

195
00:36:44,400 --> 00:36:46,600
Much much more dramatic

196
00:36:50,100 --> 00:36:53,700
At least now I wanted to stick to my plan

197
00:36:54,200 --> 00:36:57,800
First the mother should visit her dead son

198
00:36:58,400 --> 00:37:00,500
She should observe very carefully

199
00:37:00,500 --> 00:37:02,900
...how he no longer could move

200
00:37:03,600 --> 00:37:05,800
That would teach her some respect

201
00:37:09,300 --> 00:37:13,000
But...she wasn't moving anymore herself

202
00:37:18,900 --> 00:37:22,200
I thought she was playing a trick on me

203
00:37:22,500 --> 00:37:25,500
Like a fox that pretends to be dead

204
00:37:28,800 --> 00:37:31,800
I really thought she was pretending

205
00:37:36,400 --> 00:37:41,500
Then I noticed, there was really
something wrong with her

206
00:37:47,900 --> 00:37:50,600
Nobody can act that well

207
00:37:51,000 --> 00:37:53,200
That's impossible

208
00:37:54,800 --> 00:37:57,500
But she shouldn't be unconscious

209
00:37:58,200 --> 00:38:00,800
She had to experience every phase
very thoroughly

210
00:38:10,900 --> 00:38:13,400
I had lost control

211
00:38:17,000 --> 00:38:18,500
A catastrophe!

212
00:38:19,700 --> 00:38:21,600
That couldn't happen to me

213
00:38:21,800 --> 00:38:22,900
What are you doing?

214
00:38:24,300 --> 00:38:25,800
Mother is ill

215
00:38:27,100 --> 00:38:28,600
She needs her medicine...

216
00:38:29,900 --> 00:38:31,500
In the kitchen

217
00:38:42,100 --> 00:38:44,600
- She could help me
- Please

218
00:39:02,900 --> 00:39:06,200
The old one had to stay concscious
no matter what

219
00:39:06,900 --> 00:39:09,000
I wanted to strangle her after all

220
00:39:09,700 --> 00:39:14,900
And the daughter should watch
how her mother slowly died

221
00:39:15,300 --> 00:39:18,600
She should see what it meant to die

222
00:39:37,200 --> 00:39:39,900
The young one tried to get me

223
00:39:40,200 --> 00:39:44,500
Either she liked me that much or she
was just trying to rescue herself

224
00:39:44,700 --> 00:39:48,300
But that didn't matter, she just
didn't know it yet

225
00:39:54,900 --> 00:39:57,800
I had planned something very special for her

226
00:39:58,000 --> 00:40:01,200
She would take the most, that was obvious

227
00:40:02,000 --> 00:40:05,500
The thing with her is going to be
very special

228
00:40:06,400 --> 00:40:09,500
In the kitchen I had already noticed
various tools

229
00:40:10,900 --> 00:40:13,600
With her, I'll do it in the most
precise manner

230
00:40:13,700 --> 00:40:17,600
Exactly what I had always imagined

231
00:40:22,900 --> 00:40:25,500
But first I had to wake up the old one

232
00:40:36,900 --> 00:40:39,700
She had to gain consciousness again

233
00:40:48,700 --> 00:40:52,200
After all, she was supposed to
whimper and be afraid of me

234
00:40:57,200 --> 00:41:01,200
Then the daughter would have to watch
how she would die

235
00:41:11,800 --> 00:41:14,200
She can't give up

236
00:41:15,800 --> 00:41:17,400
Just like that

237
00:41:19,300 --> 00:41:21,400
I finished her

238
00:41:31,300 --> 00:41:34,400
Now only the girl was left to me

239
00:42:01,700 --> 00:42:02,600
You!

240
00:42:06,500 --> 00:42:07,300
You

241
00:42:26,400 --> 00:42:27,100
You

242
00:43:00,300 --> 00:43:01,200
You

243
00:43:14,600 --> 00:43:17,100
You... You

244
00:50:31,600 --> 00:50:35,300
I didn't want to leave the corpses in
the house like that

245
00:53:25,600 --> 00:53:30,500
I decided to take the family with me

246
00:53:38,400 --> 00:53:42,100
The thought that I could have the corpses
with me all the time

247
00:53:42,100 --> 00:53:44,600
...excited me tremendously

248
00:53:59,800 --> 00:54:02,600
In no case would I leave without them

249
00:54:03,500 --> 00:54:07,200
I had my plans with the corpses

250
00:54:30,500 --> 00:54:35,600
I was determined that this all was only
the beginning

251
00:54:36,600 --> 00:54:40,300
I wanted to live out my fantasies

252
00:54:40,600 --> 00:54:44,800
At that moment, I didn't care where that
would lead

253
00:54:45,600 --> 00:54:47,900
I didn't think about it at all

254
00:54:48,200 --> 00:54:51,200
I wanted to get new victims
as soon as possible

255
00:54:51,600 --> 00:54:53,700
I was crazy about it

256
00:54:54,400 --> 00:54:57,500
I made a new plan

257
00:55:02,200 --> 00:55:05,400
I wanted to show the corpses to
the new victims

258
00:55:05,800 --> 00:55:10,100
I was convinced that I could really scare
them with that

259
00:55:15,600 --> 00:55:20,800
I had the idea that I would lock the new
victims together with the corpses

260
00:55:25,300 --> 00:55:28,200
That would scare them enormously

261
00:55:28,800 --> 00:55:31,600
Some would surely die of fear

262
00:55:32,100 --> 00:55:36,200
They couldn't stand being tortured that way

263
00:55:41,800 --> 00:55:44,700
And then I would finish them off

264
01:03:29,800 --> 01:03:32,600
After the act I felt very well

265
01:03:33,300 --> 01:03:35,300
My head felt lighter

266
01:03:37,000 --> 01:03:39,600
Also my whole body felt lighter

267
01:03:39,900 --> 01:03:43,100
I thought that everything I did was
easier now

268
01:03:43,900 --> 01:03:45,800
As if I was floating

269
01:03:53,600 --> 01:03:58,100
I was in a bright, yes even cheerful mood

270
01:04:06,000 --> 01:04:08,700
Of course I knew that this state of mind...

271
01:04:08,900 --> 01:04:11,700
...this relief, wouldn't last long

272
01:04:12,200 --> 01:04:15,000
That the urge would return

273
01:06:10,000 --> 01:06:11,300
Hey!

274
01:06:11,500 --> 01:06:12,900
Hey!

275
01:06:13,000 --> 01:06:15,400
Hey, get out of there!

276
01:06:15,700 --> 01:06:18,400
Get out of there already!

277
01:06:20,600 --> 01:06:22,100
Hey!

278
01:06:23,900 --> 01:06:25,700
Come on!

279
01:08:16,300 --> 01:08:19,200
I can't pity the victims

280
01:08:19,800 --> 01:08:21,400
I wanted to continue to murder

281
01:08:32,800 --> 01:08:35,300
I already had a perfect plan

282
01:09:34,800 --> 01:09:37,500
The two girls were sitting there again

283
01:09:38,300 --> 01:09:40,100
I was on it immediately

284
01:09:53,100 --> 01:09:56,500
My fantasy immediately began to go wild

285
01:10:06,100 --> 01:10:08,000
It was irresistible

286
01:10:08,700 --> 01:10:10,200
Just fantastic

287
01:10:20,900 --> 01:10:22,800
I thought...

288
01:10:24,100 --> 01:10:26,300
How could I approach them?

289
01:10:31,100 --> 01:10:32,600
Then I decided...

290
01:10:32,800 --> 01:10:34,900
I'll take them all

291
01:10:44,000 --> 01:10:47,100
I'll take all four of them
right now, right here

292
01:12:16,900 --> 01:12:18,500
Show me your registration

293
01:12:20,000 --> 01:12:22,200
I don't have anything to do it with!

294
01:12:26,900 --> 01:12:28,300
The registration!

295
01:12:28,600 --> 01:12:32,100
I got the car from someone else,
I don't know how that happened

296
01:12:32,000 --> 01:12:34,400
- I don't care, show me your registration!
- I...

297
01:12:34,400 --> 01:12:35,900
What now?

298
01:12:36,300 --> 01:12:41,200
- Do you have a registration or don't you?
- It's not my fault, that's not my car...

299
01:12:43,000 --> 01:12:45,300
I don't care, show me your registration

300
01:12:45,500 --> 01:12:47,700
The moment was very thrilling

301
01:12:48,600 --> 01:12:50,700
It really excited me

302
01:12:51,000 --> 01:12:54,500
- But I don't have anything to do with it!
- Open up the trunk!

303
01:12:54,700 --> 01:12:57,500
Now everybody's going to be scared to death

304
01:13:00,900 --> 01:13:03,300
Now everybody is afraid of me

305
01:13:03,600 --> 01:13:05,400
I'm famous

306
01:13:06,000 --> 01:13:08,200
It's a pity about those girls, though...

307
01:13:08,300 --> 01:13:10,600
That would've been a nice tango

308
01:13:15,000 --> 01:13:18,200
Excerpt from the psychiatric opinion

309
01:13:18,700 --> 01:13:22,000
The patient planned  and commited
the crime consciously

310
01:13:22,000 --> 01:13:26,800
to gain physical satisfaction

311
01:13:27,200 --> 01:13:30,700
His personality is liable and perverse

312
01:13:30,900 --> 01:13:35,100
The worst mental disorder is a pronounced
sadism

313
01:13:35,300 --> 01:13:38,900
However mental illness wasn't the diagnoses

314
01:13:39,400 --> 01:13:41,800
That by hurting somebody and by
using violence...

315
01:13:41,900 --> 01:13:45,900
on other people, he could get
sexually excited

316
01:13:46,000 --> 01:13:49,300
this was known to the patient since
early childhood

317
01:13:50,000 --> 01:13:53,100
His relation to society is severely disturbed

318
01:13:53,300 --> 01:13:59,500
This is because as a baby he was
abandoned by the mother

319
01:13:59,500 --> 01:14:03,800
and later would never know a stable family

320
01:14:04,000 --> 01:14:06,700
The patient is one hundred percent
responsible

321
01:14:07,000 --> 01:14:11,300
but one cannot speak of guilt in
the common sense with this case

322
01:14:11,500 --> 01:14:15,200
The prospect of punishment didn't stop
him from murdering

323
01:14:16,200 --> 01:14:18,700
Sentence on behalf of the people:

324
01:14:19,000 --> 01:14:21,600
Imprisonment for life

