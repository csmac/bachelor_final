﻿1
00:00:21,771 --> 00:00:27,235
<i>Lullaby and goodnight</i>

2
00:00:27,318 --> 00:00:33,449
<i>With roses bedight</i>

3
00:00:33,533 --> 00:00:38,371
<i>With lilies o'er spread</i>

4
00:00:39,080 --> 00:00:45,295
<i>ls baby's wee bed</i>

5
00:00:45,378 --> 00:00:51,759
<i>Tomorrow morn, if God deems</i>

6
00:00:51,843 --> 00:00:57,682
<i>You will wake from your dreams</i>

7
00:00:57,765 --> 00:01:03,938
<i>Tomorrow morn, if God deems</i>

8
00:01:04,022 --> 00:01:10,987
<i>You will wake from your dreams</i>

9
00:01:13,698 --> 00:01:15,283
Good night.

10
00:02:39,951 --> 00:02:41,077
You're it!

11
00:02:41,160 --> 00:02:44,622
One, two, three, four...

12
00:02:44,706 --> 00:02:45,581
Five!

13
00:03:57,987 --> 00:03:59,197
Lukas?

14
00:05:17,984 --> 00:05:20,778
11,12,

15
00:05:20,862 --> 00:05:23,531
13,14,

16
00:05:24,156 --> 00:05:27,410
15,16,

17
00:05:27,493 --> 00:05:31,163
17,18,

18
00:05:31,247 --> 00:05:34,250
19, 20...

19
00:05:34,333 --> 00:05:35,418
Lukas?

20
00:05:50,433 --> 00:05:51,642
Lukas?

21
00:06:07,825 --> 00:06:13,706
GOODNIGHT MOMMY

22
00:06:46,489 --> 00:06:47,990
- Mom?
- Mom?

23
00:07:09,053 --> 00:07:10,346
Mom?

24
00:07:31,242 --> 00:07:33,202
That's a fine hello.

25
00:07:44,547 --> 00:07:46,382
Look at your clothes.

26
00:07:47,717 --> 00:07:50,928
Filthy. Take them off right now.

27
00:07:52,388 --> 00:07:53,556
Not here!

28
00:07:54,390 --> 00:07:58,394
Undress by the washing machine.
And then take a shower.

29
00:07:59,770 --> 00:08:00,980
Move it!

30
00:08:43,981 --> 00:08:45,274
There you are.

31
00:08:45,441 --> 00:08:46,609
Thanks.

32
00:08:53,991 --> 00:08:56,118
Lukas wants some too.

33
00:08:59,371 --> 00:09:02,833
Then he can ask me himself.

34
00:09:04,668 --> 00:09:07,379
You only made supper for me.

35
00:09:08,214 --> 00:09:09,590
You know why.

36
00:09:24,647 --> 00:09:26,941
You should apologize.

37
00:09:37,868 --> 00:09:38,869
Okay.

38
00:09:44,458 --> 00:09:46,710
- Am I a person?
- No.

39
00:09:47,503 --> 00:09:49,713
- Am I a thing?
- Yes.

40
00:09:50,089 --> 00:09:52,508
- Am I a cell phone?
- No.

41
00:09:55,094 --> 00:09:57,513
- Am I a pair of pants?
- No.

42
00:09:57,721 --> 00:09:59,849
- Can you wear me?
- No.

43
00:10:00,015 --> 00:10:02,351
- Can you eat me?
- No.

44
00:10:04,353 --> 00:10:07,314
- Do you need me every day?
- Yes.

45
00:10:16,532 --> 00:10:19,034
- Am I hair?
- No.

46
00:10:19,451 --> 00:10:20,870
It's a thing.

47
00:10:21,954 --> 00:10:24,123
That's needed in daily life.

48
00:10:25,332 --> 00:10:27,626
You use it almost every day.

49
00:10:28,085 --> 00:10:29,461
Do I?

50
00:10:31,797 --> 00:10:33,716
Not by yourself, but...

51
00:10:35,968 --> 00:10:38,262
- A car?
- Yes, right.

52
00:10:39,471 --> 00:10:41,182
You got it.

53
00:10:58,991 --> 00:10:59,992
Okay.

54
00:11:00,492 --> 00:11:03,454
- Am I an animal?
- No.

55
00:11:03,787 --> 00:11:06,207
- Am I a thing?
- No.

56
00:11:06,874 --> 00:11:08,292
Am I a person?

57
00:11:08,542 --> 00:11:09,877
Yes.

58
00:11:10,753 --> 00:11:13,464
- Am I a man?
- No.

59
00:11:14,465 --> 00:11:16,133
- Am I a woman?
- Yes.

60
00:11:16,217 --> 00:11:18,052
- A grown-up?
- Yes.

61
00:11:19,637 --> 00:11:21,347
Am I still alive?

62
00:11:21,555 --> 00:11:22,640
Yes.

63
00:11:25,059 --> 00:11:26,477
Am I on TV?

64
00:11:26,727 --> 00:11:27,811
Yes.

65
00:11:28,062 --> 00:11:29,146
Am I...

66
00:11:30,189 --> 00:11:31,232
a TV hostess?

67
00:11:31,815 --> 00:11:32,900
Yes.

68
00:11:35,861 --> 00:11:38,530
- Am I on German TV?
- No.

69
00:11:39,073 --> 00:11:40,950
Austrian.

70
00:11:41,200 --> 00:11:42,826
Am I famous?

71
00:11:46,580 --> 00:11:48,457
Well, sort of.

72
00:11:50,834 --> 00:11:52,294
Barbara Karlich?

73
00:11:53,671 --> 00:11:54,755
No.

74
00:12:03,806 --> 00:12:04,807
Um...

75
00:12:06,558 --> 00:12:07,851
A clue?

76
00:12:09,979 --> 00:12:12,189
You like animals.

77
00:12:17,111 --> 00:12:18,988
You like animals.

78
00:12:24,368 --> 00:12:25,577
Come on!

79
00:12:26,620 --> 00:12:28,372
You like animals.

80
00:12:31,125 --> 00:12:32,668
Another clue.

81
00:12:35,713 --> 00:12:38,257
You have two kids.

82
00:12:40,634 --> 00:12:44,513
How do I know who has two kids?
Do I know the person?

83
00:12:44,596 --> 00:12:45,723
Yes.

84
00:14:37,084 --> 00:14:38,210
Lukas!

85
00:15:18,709 --> 00:15:19,668
That's enough.

86
00:15:19,751 --> 00:15:20,752
Hey!

87
00:15:21,170 --> 00:15:22,337
That's it.

88
00:15:22,463 --> 00:15:24,298
- Come on!
- Mom!

89
00:15:24,465 --> 00:15:25,966
I have something to say.

90
00:15:26,550 --> 00:15:27,968
We were playing!

91
00:15:28,177 --> 00:15:29,303
I don't care.

92
00:15:29,470 --> 00:15:31,013
Dad lets us play.

93
00:15:31,180 --> 00:15:33,599
I don't care what your dad does.

94
00:15:34,475 --> 00:15:36,351
This is important.

95
00:15:36,560 --> 00:15:38,687
I want you to look at me.

96
00:15:41,148 --> 00:15:43,567
- So start already.
- Excuse me?

97
00:15:43,817 --> 00:15:44,985
Nothing.

98
00:15:47,446 --> 00:15:49,490
The doctor said I need rest.

99
00:15:50,115 --> 00:15:53,494
And I expect your support.
There'll be some new rules.

100
00:15:54,995 --> 00:15:57,498
I want absolute quiet in the house.

101
00:15:57,789 --> 00:16:00,083
I have to sleep. If it's important,

102
00:16:00,334 --> 00:16:01,627
knock.

103
00:16:02,836 --> 00:16:04,046
And no visitors.

104
00:16:04,171 --> 00:16:05,631
If anyone asks,

105
00:16:05,797 --> 00:16:07,299
Mom is ill.

106
00:16:07,716 --> 00:16:10,219
We'll keep the blinds closed.

107
00:16:10,469 --> 00:16:12,262
I have to avoid sunlight.

108
00:16:12,387 --> 00:16:15,182
You'll play only in the garden, quietly.

109
00:16:15,349 --> 00:16:17,059
Bring nothing inside.

110
00:16:17,184 --> 00:16:19,520
No branches, no animals...

111
00:16:19,728 --> 00:16:21,104
What are you doing?

112
00:16:22,689 --> 00:16:24,399
We'll start over.

113
00:16:24,900 --> 00:16:25,984
Right?

114
00:16:26,109 --> 00:16:29,071
We need to hold this family together.

115
00:16:29,363 --> 00:16:31,990
One from me and one from Lukas.

116
00:16:32,491 --> 00:16:33,909
Very nice.

117
00:16:36,161 --> 00:16:38,997
I'm serious about what I just said.

118
00:16:39,206 --> 00:16:41,375
We won't forget, right?

119
00:16:42,125 --> 00:16:43,335
Good.

120
00:16:43,877 --> 00:16:45,754
It's bedtime now.

121
00:16:50,175 --> 00:16:51,385
Good night.

122
00:17:01,270 --> 00:17:03,689
- She's so different.
- Well...

123
00:17:04,022 --> 00:17:05,857
It's just the operation.

124
00:17:06,608 --> 00:17:08,193
You think?

125
00:17:08,694 --> 00:17:09,861
Yeah.

126
00:17:10,070 --> 00:17:11,488
I don't know.

127
00:17:11,655 --> 00:17:13,991
How would you feel

128
00:17:14,199 --> 00:17:18,704
if you'd had an operation like that?

129
00:17:19,997 --> 00:17:21,373
I don't know.

130
00:17:21,748 --> 00:17:24,209
She s not like our mom.

131
00:17:25,168 --> 00:17:27,129
Play it again.

132
00:17:36,388 --> 00:17:39,391
<i>I'm giving you lots and lots of kisses.</i>

133
00:17:41,101 --> 00:17:43,478
<i>I can't wait until I'm back.</i>

134
00:17:43,812 --> 00:17:48,108
<i>Pull the covers up and close your eyes.</i>

135
00:17:49,234 --> 00:17:52,696
<i>Can you count the stars that brightly</i>

136
00:17:53,155 --> 00:17:56,366
<i>Twinkle in the midnight sky?</i>

137
00:17:57,117 --> 00:18:00,829
<i>Can you count the clouds, so lightly</i>

138
00:18:01,288 --> 00:18:04,625
<i>O'er the meadows floating by?</i>

139
00:18:05,167 --> 00:18:09,004
<i>God, the Lord, doth mark their number</i>

140
00:18:09,296 --> 00:18:13,258
<i>With His eyes that never slumber</i>

141
00:18:13,675 --> 00:18:17,387
<i>You He sees and you He loves</i>

142
00:18:17,971 --> 00:18:22,017
<i>You He sees and you He loves</i>

143
00:18:23,101 --> 00:18:24,686
<i>Good night.</i>

144
00:21:40,382 --> 00:21:42,300
Get Mom.

145
00:22:29,681 --> 00:22:31,057
Mom?

146
00:23:14,726 --> 00:23:16,311
Lukas?

147
00:23:40,919 --> 00:23:42,045
Lukas?

148
00:24:05,777 --> 00:24:08,363
- Hello?
- Hi, good morning.

149
00:24:09,406 --> 00:24:13,493
You sure placed a killer order.
So much, all at once.

150
00:24:13,702 --> 00:24:16,538
- Someone having a party?
- No.

151
00:24:17,956 --> 00:24:20,041
Who's all this pizza for?

152
00:24:20,458 --> 00:24:23,545
Pepperoni, pepperoni?
Is that your favorite?

153
00:24:25,588 --> 00:24:28,091
Pepperoni pizza. You must love it.

154
00:24:28,925 --> 00:24:29,968
Yeah.

155
00:24:30,135 --> 00:24:32,303
Bought enough for a year.

156
00:24:32,721 --> 00:24:35,640
There we go, it's all in there.

157
00:29:04,617 --> 00:29:06,244
<i>Open up.</i>

158
00:29:09,247 --> 00:29:10,665
What in hell...

159
00:29:12,333 --> 00:29:13,918
Since when do we lock doors?

160
00:29:14,002 --> 00:29:15,253
Sorry.

161
00:30:04,677 --> 00:30:07,305
Why is there a lighter in here?

162
00:30:08,014 --> 00:30:10,058
I wanted to burn some books.

163
00:30:10,767 --> 00:30:13,269
Don't act smart.

164
00:30:26,532 --> 00:30:27,575
Mama!

165
00:30:28,242 --> 00:30:29,327
Get off!

166
00:30:29,410 --> 00:30:30,453
Mama!

167
00:30:31,537 --> 00:30:32,789
Let him go!

168
00:35:47,436 --> 00:35:51,357
OUR WEDDING

169
00:37:01,260 --> 00:37:03,429
INGRID ULRICH REAL ESTATE -
METTLER HOUSE

170
00:37:03,512 --> 00:37:10,394
FOR SALE: STYLISH RETREAT,
PRICE ON REQUEST

171
00:39:13,475 --> 00:39:14,476
Lukas!

172
00:39:18,564 --> 00:39:19,565
Leo?

173
00:39:20,232 --> 00:39:22,359
<i>Time for your chores!</i>

174
00:39:35,664 --> 00:39:37,082
Leo?

175
00:40:05,861 --> 00:40:07,488
<i>I've other worries.</i>

176
00:40:07,655 --> 00:40:09,406
<i>I won't play along anymore.</i>

177
00:40:09,490 --> 00:40:13,744
<i>I have to make him stop.
He has to face it.</i>

178
00:40:15,204 --> 00:40:16,705
<i>Wait a second, sorry.</i>

179
00:40:48,070 --> 00:40:49,697
Not here either.

180
00:40:52,032 --> 00:40:53,242
Leo?

181
00:40:57,913 --> 00:40:59,415
Leo?

182
00:41:05,754 --> 00:41:07,881
Lukas, look.

183
00:41:14,179 --> 00:41:15,973
What's wrong with him?

184
00:41:18,767 --> 00:41:19,935
Leo?

185
00:41:26,066 --> 00:41:29,069
I'm sure it was Mom.

186
00:43:05,124 --> 00:43:06,291
Well?

187
00:43:11,755 --> 00:43:12,798
Fine.

188
00:43:17,678 --> 00:43:18,804
Are you crazy?

189
00:43:20,597 --> 00:43:21,849
Stop that.

190
00:43:23,517 --> 00:43:24,643
What's going on?

191
00:43:25,561 --> 00:43:26,562
What is going on?

192
00:43:26,854 --> 00:43:28,730
- We want our mom back.
- What?

193
00:43:29,106 --> 00:43:30,816
We want our mom back.

194
00:43:32,276 --> 00:43:33,819
Have you gone crazy?

195
00:43:35,445 --> 00:43:37,489
Are you out of your mind?

196
00:43:38,532 --> 00:43:40,784
Clean this up and go to your room!

197
00:43:41,743 --> 00:43:43,287
You're not our mom.

198
00:43:48,083 --> 00:43:49,835
To your room!

199
00:43:50,085 --> 00:43:51,712
Show us your birthmark.

200
00:43:51,920 --> 00:43:54,214
- Enough!
- Show us your birthmark.

201
00:43:54,423 --> 00:43:55,716
I've had it.

202
00:43:57,509 --> 00:43:59,178
Anything else, maybe?

203
00:44:17,112 --> 00:44:20,449
<i>I want you to say 10 times
that I'm your mother.</i>

204
00:44:21,283 --> 00:44:22,868
- Open up!
<i>- Say it.</i>

205
00:44:23,202 --> 00:44:24,578
<i>- You're my mom.
- Properly.</i>

206
00:44:24,703 --> 00:44:26,246
<i>You're my mom.</i>

207
00:44:26,330 --> 00:44:27,581
<i>Look at me.</i>

208
00:44:27,706 --> 00:44:29,958
<i>You're my mom, you're my mom...</i>

209
00:44:30,292 --> 00:44:31,793
<i>Louder.</i>

210
00:44:31,919 --> 00:44:33,879
<i>You're my mom.</i>

211
00:44:33,962 --> 00:44:36,506
<i>You're my mom.
You're my mom.</i>

212
00:44:38,550 --> 00:44:40,135
<i>Stop this foolishness!</i>

213
00:44:40,260 --> 00:44:42,930
<i>I'm not playing along anymore.
Understand?</i>

214
00:44:43,138 --> 00:44:45,766
<i>There'll be only one breakfast
and one set of clothes.</i>

215
00:44:45,891 --> 00:44:49,186
<i>And promise me not to talk
to your brother!</i>

216
00:44:49,269 --> 00:44:50,229
<i>No.</i>

217
00:44:51,355 --> 00:44:52,773
<i>Promise me.</i>

218
00:44:55,734 --> 00:44:58,987
<i>I'm going to get so mad!
Promise me!</i>

219
00:45:01,740 --> 00:45:04,034
<i>Give me your cell. Now!</i>

220
00:45:13,585 --> 00:45:15,087
Stay in your room!

221
00:45:31,687 --> 00:45:34,439
She wants to tear us apart.

222
00:46:15,897 --> 00:46:16,732
Does that hurt?

223
00:46:18,358 --> 00:46:19,901
- Does that hurt?
- No.

224
00:46:21,320 --> 00:46:22,154
Does that hurt?

225
00:48:03,338 --> 00:48:05,298
Mom, please come back.

226
00:48:07,384 --> 00:48:09,428
I'll do anything you want.

227
00:48:10,470 --> 00:48:12,305
Please come back.

228
00:48:16,560 --> 00:48:20,021
All I wish for is that
you'll come back to us.

229
00:48:55,891 --> 00:48:57,017
Elias?

230
00:51:34,966 --> 00:51:37,010
Trim around your ear.

231
00:51:37,093 --> 00:51:37,927
Here?

232
00:51:38,011 --> 00:51:39,012
Yeah.

233
00:51:47,854 --> 00:51:49,022
Better?

234
00:51:49,105 --> 00:51:50,106
Yeah.

235
00:51:52,233 --> 00:51:53,777
Now we look the same.

236
00:51:54,110 --> 00:51:57,238
Now she won't be able to tell us apart.

237
00:52:16,091 --> 00:52:18,176
<i>It's me. Open the door.</i>

238
00:52:21,262 --> 00:52:22,722
<i>Are we friends?</i>

239
00:52:23,848 --> 00:52:25,266
<i>I'm not mad anymore.</i>

240
00:52:27,102 --> 00:52:28,603
<i>Please open the door.</i>

241
00:52:55,547 --> 00:52:56,631
Well?

242
00:53:10,645 --> 00:53:12,147
Are we friends again?

243
00:53:28,288 --> 00:53:29,873
I have something.

244
00:53:35,462 --> 00:53:36,546
Thanks.

245
00:53:37,338 --> 00:53:38,381
Thanks.

246
00:53:39,090 --> 00:53:41,009
- Great, huh?
- Yes.

247
00:53:42,469 --> 00:53:45,722
Can we go outside and try it?

248
00:53:56,900 --> 00:53:58,693
Hey! Hey!

249
00:56:55,954 --> 00:56:57,121
Yes?

250
00:57:02,669 --> 00:57:03,670
Um...

251
00:57:07,423 --> 00:57:08,466
Yes?

252
00:57:12,804 --> 00:57:14,222
What is it?

253
00:57:14,847 --> 00:57:16,015
Can you help us?

254
00:57:16,182 --> 00:57:17,183
Yes.

255
00:57:22,063 --> 00:57:23,398
What's it about?

256
00:57:26,234 --> 00:57:29,320
- Are you the priest?
- No, I'm the sexton.

257
00:57:31,197 --> 00:57:32,615
Where's the priest?

258
00:57:33,825 --> 00:57:35,284
He's not here.

259
00:57:39,122 --> 00:57:41,833
- Can we call him?
- Yeah.

260
00:57:44,293 --> 00:57:46,671
Sure you can.

261
00:58:10,403 --> 00:58:14,490
Can you come inside
the police station with us?

262
00:58:15,950 --> 00:58:18,244
And talk to the policeman

263
00:58:18,327 --> 00:58:19,871
and explain it to him?

264
00:58:20,204 --> 00:58:21,914
Yeah, no problem.

265
00:59:16,803 --> 00:59:17,804
Hey!

266
00:59:26,979 --> 00:59:28,356
Open up!

267
00:59:36,781 --> 00:59:39,367
Come out of there! Come out!

268
00:59:50,461 --> 00:59:51,546
Thank you.

269
00:59:58,886 --> 01:00:01,764
Would you like to explain?

270
01:00:07,186 --> 01:00:09,397
It was all a bit much.

271
01:00:11,149 --> 01:00:13,025
The accident. The separation.

272
01:03:49,700 --> 01:03:51,369
What's going on?

273
01:03:53,954 --> 01:03:55,581
Huh?

274
01:03:56,248 --> 01:03:57,541
What's going on?

275
01:03:59,752 --> 01:04:01,504
Where is our mom?

276
01:04:05,549 --> 01:04:07,760
I'm going insane. Insane.

277
01:04:11,514 --> 01:04:13,265
How do I get up?

278
01:04:14,266 --> 01:04:15,393
You don't.

279
01:04:19,647 --> 01:04:21,357
Where is our mom?

280
01:04:24,068 --> 01:04:27,196
Lukas said, "Where is our mom?"

281
01:04:29,281 --> 01:04:30,699
Untie me now.

282
01:04:31,492 --> 01:04:32,618
No.

283
01:04:32,952 --> 01:04:34,328
It hurts. Untie me.

284
01:04:34,412 --> 01:04:35,579
No.

285
01:04:38,290 --> 01:04:40,167
Tell us where our mom is.

286
01:04:40,418 --> 01:04:41,794
I'm your mom!

287
01:04:45,673 --> 01:04:46,799
No.

288
01:04:58,394 --> 01:04:59,895
Who's she?

289
01:05:07,278 --> 01:05:08,654
Where are the scissors?

290
01:05:08,863 --> 01:05:09,864
Who is she?

291
01:05:17,788 --> 01:05:19,248
A friend.

292
01:05:20,541 --> 01:05:22,376
We always dressed the same.

293
01:05:26,297 --> 01:05:28,382
Who is she really?

294
01:05:33,220 --> 01:05:34,555
Answer.

295
01:05:37,683 --> 01:05:38,976
Please answer.

296
01:06:16,347 --> 01:06:18,432
<i>Hi, I'm from Vienna.</i>

297
01:06:19,350 --> 01:06:22,895
<i>What should I say?
It's my first time doing this.</i>

298
01:06:24,563 --> 01:06:26,690
<i>I like to cook, watch movies.</i>

299
01:06:27,274 --> 01:06:29,485
<i>But I also enjoy spending
the evening watching TV.</i>

300
01:06:31,445 --> 01:06:32,738
Look.

301
01:06:45,167 --> 01:06:46,210
Go away!

302
01:06:48,170 --> 01:06:49,463
Help me.

303
01:06:57,012 --> 01:06:58,305
Stop it!

304
01:06:59,682 --> 01:07:00,849
Stop!

305
01:07:01,892 --> 01:07:04,603
- I don't see it.
- What are you doing?

306
01:07:04,687 --> 01:07:05,896
Take another look.

307
01:07:06,772 --> 01:07:09,775
What is this? What is this?

308
01:07:10,234 --> 01:07:13,696
- In the video your eyes are brown.
- What?

309
01:07:14,780 --> 01:07:18,576
They're contacts, Christ!
They're in the bathroom.

310
01:07:36,927 --> 01:07:38,887
Why are you lying to me?

311
01:07:44,643 --> 01:07:47,563
I thought we agreed not to believe her.

312
01:07:47,813 --> 01:07:50,107
So? I changed my mind.

313
01:07:50,482 --> 01:07:52,776
Do you believe her, you retard?

314
01:07:52,985 --> 01:07:54,445
You're the retard!

315
01:08:19,053 --> 01:08:20,387
<i>Help!</i>

316
01:08:27,227 --> 01:08:29,229
Where are the damn scissors?

317
01:08:30,189 --> 01:08:32,858
Do you understand? Get the scissors!

318
01:08:44,244 --> 01:08:45,496
Lukas?

319
01:08:53,045 --> 01:08:54,129
Elias...

320
01:08:56,131 --> 01:08:57,257
Lukas?

321
01:08:59,718 --> 01:09:02,012
Sit beside me. Come on.

322
01:09:03,013 --> 01:09:05,015
Come, Elias. Sit here.

323
01:09:08,560 --> 01:09:10,604
Come sit beside me, Elias.

324
01:09:14,650 --> 01:09:17,319
Come on, sit down. So we can talk.

325
01:09:19,738 --> 01:09:20,864
Look.

326
01:09:26,662 --> 01:09:27,830
Elias...

327
01:09:29,665 --> 01:09:31,875
You know I'm your mom.

328
01:09:47,850 --> 01:09:49,476
I can't untie myself.

329
01:09:49,727 --> 01:09:55,274
Just go to the bathroom,
get the scissors and cut me loose.

330
01:10:00,070 --> 01:10:02,948
Nothing bad has happened. Cut me loose.

331
01:10:05,784 --> 01:10:09,413
<i>I want you to say 10 times,
"I won't listen to my brother."</i>

332
01:10:09,830 --> 01:10:12,332
<i>I won't listen to my brother.
I won't...</i>

333
01:10:12,583 --> 01:10:13,917
<i>Louder, please.</i>

334
01:10:14,042 --> 01:10:17,296
<i>I won't listen to my brother.
I won't listen...</i>

335
01:10:17,463 --> 01:10:18,922
<i>I said louder.</i>

336
01:10:20,591 --> 01:10:22,718
Our mom wouldn't do that.

337
01:10:22,843 --> 01:10:23,969
<i>More clearly.</i>

338
01:10:24,970 --> 01:10:26,054
Elias...

339
01:10:28,599 --> 01:10:29,933
I'm sorry.

340
01:10:30,267 --> 01:10:31,518
<i>Again.</i>

341
01:10:31,977 --> 01:10:33,771
I'm sorry, Elias.

342
01:10:44,615 --> 01:10:45,908
Come now.

343
01:10:46,909 --> 01:10:48,744
Please. Cut me loose.

344
01:10:56,752 --> 01:10:58,378
What happened to your nose?

345
01:10:59,379 --> 01:11:01,715
Nothing. Nothing serious.

346
01:11:02,549 --> 01:11:04,051
There, you see?

347
01:11:08,138 --> 01:11:09,765
You'll cut me loose

348
01:11:10,307 --> 01:11:12,142
and I'll make you breakfast.

349
01:11:13,018 --> 01:11:15,354
Cut my feet loose, they hurt.

350
01:11:21,276 --> 01:11:22,361
Lukas?

351
01:11:23,862 --> 01:11:26,532
I know it's hard, but we'll make it.

352
01:11:27,199 --> 01:11:29,660
Come, just cut my feet loose.

353
01:11:34,706 --> 01:11:38,001
Come on, it'll take a sec.
I'll make breakfast.

354
01:11:42,673 --> 01:11:43,966
Just cut it.

355
01:11:46,760 --> 01:11:47,928
That's it.

356
01:11:52,224 --> 01:11:53,642
What are you doing?

357
01:12:03,026 --> 01:12:04,069
There.

358
01:12:06,363 --> 01:12:07,364
What?

359
01:12:12,202 --> 01:12:14,204
What is it?

360
01:12:17,708 --> 01:12:18,709
Stop.

361
01:12:21,295 --> 01:12:23,714
They had to remove it in hospital.

362
01:12:24,715 --> 01:12:26,341
They're dangerous.

363
01:12:27,175 --> 01:12:28,510
Liar!

364
01:12:30,304 --> 01:12:31,722
They had to. They had no...

365
01:12:32,264 --> 01:12:34,683
Admit that it's a lie.

366
01:12:36,602 --> 01:12:38,896
Just cut me loose, please.

367
01:12:49,406 --> 01:12:50,449
<i>Stop.</i>

368
01:12:51,033 --> 01:12:52,075
<i>Stop!</i>

369
01:13:18,810 --> 01:13:20,812
Tell us where our mom is.

370
01:13:26,985 --> 01:13:29,071
Tell us where our mom is!

371
01:15:33,820 --> 01:15:35,072
It's open.

372
01:15:35,906 --> 01:15:38,200
Hello, Red Cross!

373
01:15:40,952 --> 01:15:41,995
Hello?

374
01:15:43,163 --> 01:15:44,915
Red Cross. Care to make a donation?

375
01:15:46,208 --> 01:15:47,751
Is anybody home?

376
01:15:51,379 --> 01:15:52,881
I'll have a look.

377
01:15:56,301 --> 01:15:57,135
Hello?

378
01:15:58,136 --> 01:15:59,221
Red Cross.

379
01:16:03,058 --> 01:16:04,351
Nobody's there.

380
01:16:08,313 --> 01:16:11,441
It was unlocked, someone must be home.

381
01:16:17,656 --> 01:16:18,824
Hello?

382
01:16:21,743 --> 01:16:23,203
Maybe they're upstairs.

383
01:16:24,746 --> 01:16:26,289
Hello?

384
01:16:30,085 --> 01:16:31,503
Red Cross.

385
01:16:34,798 --> 01:16:36,550
- Hello?
- Hello!

386
01:16:37,050 --> 01:16:38,135
Hello.

387
01:16:43,640 --> 01:16:44,975
Is your mom home?

388
01:16:46,017 --> 01:16:47,519
No, she's not home.

389
01:16:47,727 --> 01:16:50,021
Not upstairs? We heard something.

390
01:16:50,272 --> 01:16:52,190
No, that's our dog.

391
01:16:52,524 --> 01:16:54,192
- She's not home?
- No.

392
01:16:54,484 --> 01:16:55,652
It's our dog.

393
01:16:55,902 --> 01:16:57,070
Uh-huh.

394
01:16:57,696 --> 01:16:59,906
When will Mom be back?

395
01:17:04,870 --> 01:17:05,871
Um...

396
01:17:05,996 --> 01:17:07,664
She'll be home soon.

397
01:17:09,082 --> 01:17:10,333
Can we wait?

398
01:17:10,959 --> 01:17:12,335
Where should we wait?

399
01:17:23,388 --> 01:17:27,350
Hand me the list.
I'll see how much we've collected.

400
01:17:27,475 --> 01:17:29,603
- The money too?
- Yeah, I'll count it.

401
01:17:29,686 --> 01:17:30,687
Okay.

402
01:17:31,146 --> 01:17:33,773
- Really bad handwriting.
- Yeah.

403
01:17:33,982 --> 01:17:37,027
You'd swear a doctor had written it.

404
01:17:37,152 --> 01:17:39,029
It's barely legible.

405
01:17:40,780 --> 01:17:42,949
Looking forward to school?

406
01:17:43,366 --> 01:17:45,368
- Well...
- It starts soon.

407
01:17:50,165 --> 01:17:53,585
- Where did you go to school before?
- In Vienna.

408
01:17:56,004 --> 01:17:58,381
A lot more going on there.

409
01:18:03,929 --> 01:18:06,681
We've already collected a tidy sum.

410
01:18:06,806 --> 01:18:09,809
If somebody else gives as much as him,

411
01:18:10,060 --> 01:18:12,687
we'll have done pretty well.

412
01:18:15,065 --> 01:18:17,984
We'll see how much more we get.

413
01:18:18,401 --> 01:18:20,820
What do you do all day?

414
01:18:21,112 --> 01:18:22,572
Not much.

415
01:18:32,999 --> 01:18:34,960
I have to go to the toilet.

416
01:19:04,406 --> 01:19:05,615
- Uh-huh.
- Uh-huh.

417
01:19:05,740 --> 01:19:07,284
Is your mom back?

418
01:19:07,534 --> 01:19:08,785
Not yet.

419
01:19:09,119 --> 01:19:12,205
Is that your own money?

420
01:19:12,414 --> 01:19:13,540
No.

421
01:19:16,918 --> 01:19:21,464
- Will your mom be OK with this?
- Yes, I'm sure she will.

422
01:19:22,632 --> 01:19:24,718
She always gives that much.

423
01:19:26,886 --> 01:19:29,222
We don't want any trouble.

424
01:19:29,306 --> 01:19:30,974
You realize that...

425
01:19:32,392 --> 01:19:36,104
We usually don't
accept money from children.

426
01:19:36,229 --> 01:19:38,148
That's a lot of money.

427
01:19:38,273 --> 01:19:40,150
No, we're allowed.

428
01:19:40,984 --> 01:19:43,570
Well, it is a nice house. Just look.

429
01:19:43,695 --> 01:19:46,323
I guess they can afford it.

430
01:19:52,329 --> 01:19:54,664
As a thank-you for the donation,

431
01:19:55,290 --> 01:19:56,750
here's a Band-Aid.

432
01:20:02,172 --> 01:20:03,340
Help!

433
01:20:04,841 --> 01:20:07,844
<i>Help! Help!</i>

434
01:20:08,011 --> 01:20:10,972
Help! Help!

435
01:20:11,264 --> 01:20:16,227
<i>Help! Help! Help!</i>

436
01:20:19,564 --> 01:20:22,484
<i>Help! Help!</i>

437
01:20:27,864 --> 01:20:31,493
<i>Help! Help!</i>

438
01:20:31,576 --> 01:20:33,203
Help!

439
01:20:33,286 --> 01:20:35,246
Stop! Stop!

440
01:23:41,391 --> 01:23:44,561
Please prove you're our mom.

441
01:23:54,988 --> 01:23:58,032
Ask her what my favorite song is.

442
01:23:59,242 --> 01:24:01,452
What's Lukas' favorite song?

443
01:24:11,588 --> 01:24:13,506
<i>Lullaby and Good Night?</i>

444
01:24:46,748 --> 01:24:47,874
Don't!

445
01:25:04,307 --> 01:25:08,811
<i>Can you count the stars that brightly</i>

446
01:25:09,312 --> 01:25:13,066
<i>Twinkle in the midnight sky?</i>

447
01:25:14,025 --> 01:25:18,196
<i>Can you count the clouds, so lightly</i>

448
01:25:18,821 --> 01:25:22,700
<i>O'er the meadows floating by?</i>

449
01:25:23,451 --> 01:25:27,789
<i>God, the Lord, doth mark their number</i>

450
01:25:28,122 --> 01:25:32,085
<i>With His eyes that never slumber</i>

451
01:25:32,585 --> 01:25:36,214
<i>You He sees and you He loves</i>

452
01:25:36,714 --> 01:25:39,967
<i>You He sees and you He loves</i>

453
01:26:01,864 --> 01:26:03,908
Where's our mom?

454
01:27:07,597 --> 01:27:09,766
Don't you know when you
have to pee?

455
01:27:11,642 --> 01:27:13,686
We're not changing them.

456
01:27:15,480 --> 01:27:16,564
Hello?

457
01:27:21,194 --> 01:27:22,570
Untie me.

458
01:28:21,212 --> 01:28:22,922
Now change the bed.

459
01:28:23,339 --> 01:28:24,966
Take off the sheets.

460
01:28:27,635 --> 01:28:29,846
Come on, take them off.

461
01:30:36,389 --> 01:30:37,515
Elias?

462
01:30:38,599 --> 01:30:40,184
Where's our mom?

463
01:30:42,019 --> 01:30:43,604
Elias, come here.

464
01:30:45,064 --> 01:30:46,691
Where is she?

465
01:30:53,239 --> 01:30:54,323
Fine.

466
01:30:56,367 --> 01:30:58,703
Elias, let's make a deal.

467
01:31:00,121 --> 01:31:01,789
I'll play along again.

468
01:31:03,332 --> 01:31:05,501
I'll talk to Lukas again.

469
01:31:06,919 --> 01:31:08,713
Lukas will be alive.

470
01:31:14,093 --> 01:31:16,178
I'll make you both breakfast,

471
01:31:16,429 --> 01:31:20,057
put out your clothes.
We'll do things, like before.

472
01:31:20,182 --> 01:31:22,351
But you have to believe me

473
01:31:22,935 --> 01:31:24,687
that I'm your mom.

474
01:31:27,982 --> 01:31:30,151
You won't really do that.

475
01:31:35,448 --> 01:31:36,866
I promise.

476
01:31:38,826 --> 01:31:39,952
Elias...

477
01:31:41,871 --> 01:31:44,749
It's not your fault Lukas died.

478
01:31:46,792 --> 01:31:49,545
The accident wasn't your fault.

479
01:31:51,464 --> 01:31:53,716
Do you really believe her?

480
01:32:02,391 --> 01:32:03,434
She's lying.

481
01:32:09,815 --> 01:32:12,526
Let her prove she's our mom.

482
01:32:19,116 --> 01:32:20,743
What am I doing?

483
01:32:21,869 --> 01:32:23,788
What's Lukas doing?

484
01:32:29,001 --> 01:32:31,087
But I can't see him!

485
01:32:31,420 --> 01:32:33,297
What's he doing?

486
01:32:39,261 --> 01:32:40,596
Elias...

487
01:32:44,141 --> 01:32:46,227
Mom would know that.

488
01:32:47,770 --> 01:32:49,605
Mom could see him.

489
01:32:51,899 --> 01:32:54,110
She'd know what he's doing.

490
01:33:04,120 --> 01:33:05,621
No, Elias. Don't!

491
01:33:07,748 --> 01:33:09,125
Elias, don't!

492
01:33:09,792 --> 01:33:11,502
Elias, do you hear me?

493
01:33:12,628 --> 01:33:13,629
Elias!

494
01:33:16,423 --> 01:33:20,136
Elias! Elias! Elias! Elias!

495
01:33:20,219 --> 01:33:23,139
Elias! Elias!

496
01:33:24,181 --> 01:33:25,015
Elias!

497
01:33:25,099 --> 01:33:27,184
Help! Help!

498
01:33:27,935 --> 01:33:33,190
Elias! Elias! Elias! Elias!

499
01:33:33,274 --> 01:33:36,402
Elias!

500
01:35:29,515 --> 01:35:33,519
<i>God, the Lord, doth mark their number</i>

501
01:35:33,602 --> 01:35:37,690
<i>With His eyes that never slumber</i>

502
01:35:37,773 --> 01:35:42,027
<i>You He sees and you He loves</i>

503
01:35:42,111 --> 01:35:46,282
<i>You He sees and you He loves</i>

