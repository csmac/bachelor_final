1
00:17:11,447 --> 00:17:13,531
You should be practicing.

166
00:17:13,615 --> 00:17:14,699
I am practicing.

167
00:17:15,409 --> 00:17:17,285
What you're doing you won't need these.

168
00:17:17,411 --> 00:17:18,453
What's your problem?

174
00:18:13,342 --> 00:18:14,675
Yeah!

339
00:34:17,847 --> 00:34:19,389
What do you learn here?

340
00:34:19,474 --> 00:34:23,351
No Weakness! No Pain! No Mercy!

341
00:34:24,687 --> 00:34:26,229
What do you learn here?

342
00:34:26,314 --> 00:34:30,358
No Weakness! No Pain! No Mercy!

343
00:34:31,235 --> 00:34:32,277
Begin!

374
00:37:03,554 --> 00:37:06,056
<i>Hi, guys. Gather around here.</i>

375
00:37:06,307 --> 00:37:09,476
<i>As the emperor was considered to be divine,</i>

376
00:37:09,560 --> 00:37:12,229
<i>no mere mortal was permitted access.</i>

377
00:37:12,313 --> 00:37:13,855
<i>Hence the name</i>

378
00:37:13,940 --> 00:37:15,690
<i>the Forbidden City.</i>

383
00:38:36,397 --> 00:38:41,234
<i>The floors of the palace were reinforced
with brick 15 layers deep</i>

384
00:38:41,319 --> 00:38:44,154
<i>to prevent anyone from tunneling in.</i>

385
00:38:44,238 --> 00:38:47,449
<i>Even if an outsider managed to get in...</i>

391
00:43:57,551 --> 00:43:59,218
He's had enough.

392
00:44:00,054 --> 00:44:01,888
Don't you remember what we learned?

393
00:44:02,890 --> 00:44:04,807
No weakness! No pain!

394
00:44:05,643 --> 00:44:06,726
No mercy!

395
00:44:06,852 --> 00:44:08,227
Cheng, stop!

396
00:44:26,747 --> 00:44:27,872
Go home.

400
00:47:31,431 --> 00:47:35,101
You think only with your eyes,
so you are easy to fool.

402
00:47:41,233 --> 00:47:43,150
Ancient Chinese healing.

405
00:47:49,157 --> 00:47:51,576
When fighting angry, blind men,

406
00:47:52,327 --> 00:47:54,287
best to just stay out of the way.

415
00:48:19,938 --> 00:48:22,815
Best fights are the ones we avoid.

419
00:48:33,660 --> 00:48:39,665
Kung fu is for knowledge, defense.
Not to make war, but create peace.

424
00:48:52,512 --> 00:48:54,680
Good idea.

427
00:49:02,022 --> 00:49:03,731
You'll get beat up anyway.

431
00:49:16,995 --> 00:49:18,663
For you. Bad idea for me.

437
00:50:32,946 --> 00:50:34,113
What are you doing?

438
00:50:34,614 --> 00:50:35,823
Why did you stop?

439
00:50:37,284 --> 00:50:38,325
Finish!

440
00:50:43,999 --> 00:50:45,041
Come here!

442
00:51:11,485 --> 00:51:14,403
We do not stop when our enemy is down.

443
00:51:15,155 --> 00:51:16,489
No mercy...

444
00:51:18,033 --> 00:51:19,742
No mercy in the studio...

445
00:51:19,826 --> 00:51:21,118
No mercy in competition...

446
00:51:21,203 --> 00:51:22,995
No mercy in life!

447
00:51:25,499 --> 00:51:27,500
Our enemies deserve pain.

448
00:51:33,840 --> 00:51:35,049
What are you looking at?

449
00:51:36,218 --> 00:51:37,676
He's the one that attacked me.

450
00:51:51,858 --> 00:51:53,526
We are here to make peace.

451
00:51:55,445 --> 00:51:58,030
Let your little thing mind himself.

452
00:52:00,367 --> 00:52:02,034
One to one, no problem...

453
00:52:02,119 --> 00:52:05,704
...six to one, too much to ask of anyone.

454
00:52:06,540 --> 00:52:07,706
I see...

455
00:52:08,542 --> 00:52:10,209
Prepare for match.

457
00:52:19,136 --> 00:52:21,387
We are not here to fight.

458
00:52:26,226 --> 00:52:30,354
You attack my students
and disrespect my studio?!

459
00:52:30,438 --> 00:52:32,398
You want to leave? Not so easy!

460
00:52:35,235 --> 00:52:36,569
Master Li...

461
00:52:40,907 --> 00:52:42,908
You both came here.

462
00:52:44,119 --> 00:52:47,746
One of you fights now.

463
00:52:52,961 --> 00:52:55,296
The boy will fight there.

464
00:53:09,352 --> 00:53:11,770
We accept your challenge.

465
00:53:13,523 --> 00:53:18,319
Please instruct your students
to leave my boy alone to train.

466
00:53:20,030 --> 00:53:21,155
Attention!

467
00:53:24,659 --> 00:53:26,994
From now on, this little thing
is to be left alone...

468
00:53:27,662 --> 00:53:29,538
...until the tournament.

469
00:53:29,623 --> 00:53:30,873
Understood?

470
00:53:38,465 --> 00:53:41,467
If he does not show up for the competition...

471
00:53:42,135 --> 00:53:46,639
...I will bring pain to him and to you.

473
00:54:01,488 --> 00:54:03,280
There's good news and bad news.

474
00:54:03,365 --> 00:54:05,407
Good news is
they promise to leave you alone.

476
00:54:08,078 --> 00:54:09,662
While you prepare.

478
00:54:13,416 --> 00:54:14,458
Tournament.

479
00:54:14,918 --> 00:54:17,211
You'll fight them all one-on-one.

484
00:54:32,143 --> 00:54:33,769
That's not kung fu.

485
00:54:33,853 --> 00:54:35,896
That's a bad man
teach them very bad things.

487
00:54:43,196 --> 00:54:44,738
More good news.

488
00:54:44,823 --> 00:54:47,157
I will teach you real kung fu.

521
00:57:40,665 --> 00:57:42,082
Pick up your jacket.

525
00:57:55,972 --> 00:57:57,014
Hang it up.

528
00:58:01,311 --> 00:58:02,603
Take it down.

530
00:58:07,275 --> 00:58:08,525
Take it down.

531
00:58:12,530 --> 00:58:13,739
Put it on.

532
00:58:19,412 --> 00:58:20,454
Take it off.

533
00:58:21,664 --> 00:58:24,082
Take it off.

534
00:58:24,167 --> 00:58:26,502
Take it off.

535
00:58:30,798 --> 00:58:31,965
Hang it up.

536
00:58:36,763 --> 00:58:37,846
Take it down.

537
00:58:39,015 --> 00:58:40,098
Put it on the ground.

538
00:58:42,769 --> 00:58:43,852
Pick it up.

539
00:58:46,397 --> 00:58:47,689
Hang up.

540
00:58:48,900 --> 00:58:50,442
Take it down.

541
00:58:53,613 --> 00:58:54,988
Put it on.

542
00:58:56,032 --> 00:58:57,533
Take it off.

543
00:58:59,285 --> 00:59:00,410
Put it on the ground.

544
00:59:01,913 --> 00:59:03,330
Pick it up.

545
00:59:04,874 --> 00:59:06,291
Hang it up.

546
00:59:07,877 --> 00:59:09,378
Take it down.

547
00:59:10,296 --> 00:59:11,755
Put it on.

548
00:59:13,174 --> 00:59:14,633
Take it off.

549
00:59:15,802 --> 00:59:17,261
Hang it up.

552
00:59:31,818 --> 00:59:33,443
No street parking.

568
01:00:59,405 --> 01:01:01,615
Are you going to the Qi Xi Festival?

570
01:01:11,459 --> 01:01:12,793
Go to the Shadow Theater.

576
01:01:27,100 --> 01:01:28,934
Tomorrow. Show starts at 7:00.

578
01:01:34,232 --> 01:01:35,399
See you tomorrow.

580
01:01:37,568 --> 01:01:38,902
Festival.

586
01:02:15,148 --> 01:02:16,189
Where's your jacket?

590
01:02:24,532 --> 01:02:26,700
Yes. All the way back to where you hide it.

594
01:03:02,737 --> 01:03:03,904
Something's missing.

596
01:03:14,332 --> 01:03:16,458
You forgot this.

597
01:03:18,127 --> 01:03:19,169
Attitude.

598
01:03:21,631 --> 01:03:22,798
Jacket off.

600
01:03:27,470 --> 01:03:28,804
Attitude.

601
01:03:29,472 --> 01:03:31,473
Yes. That's it.

604
01:03:42,235 --> 01:03:43,318
Put it on the ground.

615
01:04:13,015 --> 01:04:15,559
I don't go.

621
01:04:47,091 --> 01:04:49,259
Chinese Valentine's Day.

643
01:07:02,435 --> 01:07:04,519
Xiao Dre making progress.

645
01:07:06,689 --> 01:07:08,523
Yeah. Chinese for "little."

647
01:07:15,114 --> 01:07:17,282
Including Xiao Dre?

650
01:07:31,464 --> 01:07:36,092
It's a story of a goddess
and the boy she loves.

651
01:07:37,303 --> 01:07:38,970
I love this story.

652
01:07:42,141 --> 01:07:45,477
Her mother does not approve of him.

653
01:07:45,561 --> 01:07:49,105
So she cuts a wide river in the sky

654
01:07:49,940 --> 01:07:51,983
to separate them forever.

655
01:07:53,069 --> 01:07:54,110
But once a year,

656
01:07:55,321 --> 01:07:57,614
all the birds in the sky

657
01:07:58,574 --> 01:08:02,535
feel sad for them and they form a bridge

658
01:08:04,955 --> 01:08:11,044
so the two of them
may be together for a single night.

664
01:08:35,986 --> 01:08:40,365
And I promise to cheer bigger
than anyone else when you win.

666
01:09:51,562 --> 01:09:53,396
Did I say, "Come in?"

683
01:11:31,704 --> 01:11:32,704
Come here.

684
01:11:41,213 --> 01:11:42,213
Jacket on.

685
01:11:42,339 --> 01:11:44,549
Jacket on.

686
01:11:54,393 --> 01:11:55,393
Jacket on.

687
01:11:56,395 --> 01:11:58,730
Jacket on.

688
01:12:06,071 --> 01:12:07,405
Be strong.

689
01:12:08,741 --> 01:12:09,907
Jacket on.

690
01:12:11,702 --> 01:12:12,869
Firm.

691
01:12:14,371 --> 01:12:15,413
Jacket off.

692
01:12:18,375 --> 01:12:21,461
Remember, always strong.

693
01:12:23,297 --> 01:12:24,797
Jacket off.

694
01:12:29,094 --> 01:12:30,303
Strong.

695
01:12:31,430 --> 01:12:32,472
Left foot back.

696
01:12:33,140 --> 01:12:36,642
Right foot back. Left foot back!
Pick up your jacket!

697
01:12:38,437 --> 01:12:40,229
Focus!

698
01:12:40,314 --> 01:12:41,898
Always concentrate.

699
01:12:42,232 --> 01:12:44,901
Left back.
Right foot back! Pick up your jacket!

700
01:12:46,153 --> 01:12:47,236
Stay.

701
01:12:50,491 --> 01:12:52,116
Pick up your jacket.

702
01:12:56,914 --> 01:12:59,290
Be strong. Hang it up.

703
01:13:00,459 --> 01:13:01,751
Hang it up.

704
01:13:02,586 --> 01:13:04,337
Hang up. And attitude.

705
01:13:06,131 --> 01:13:07,173
Strike.

706
01:13:08,592 --> 01:13:10,468
Hang up, and attitude.

707
01:13:11,136 --> 01:13:12,678
Harder! Harder!

708
01:13:16,350 --> 01:13:17,600
Good.

709
01:13:18,477 --> 01:13:19,977
But no face.

710
01:13:21,021 --> 01:13:22,188
Jacket off!

711
01:13:44,670 --> 01:13:48,172
Kung fu lives in everything we do, Xiao Dre.

712
01:13:49,091 --> 01:13:53,094
It lives in how we put on a jacket,
how we take off the jacket.

713
01:13:54,721 --> 01:13:57,348
And lives in how we treat people.

714
01:14:00,102 --> 01:14:03,354
Everything is kung fu.

716
01:14:50,486 --> 01:14:52,653
I don't drive the Scirocco.

720
01:15:06,502 --> 01:15:08,252
Please be quiet.

724
01:15:15,302 --> 01:15:18,095
Do you always ask this many questions?

726
01:15:21,934 --> 01:15:23,434
So, what are we learning today?

727
01:15:26,813 --> 01:15:27,939
Chi.

728
01:15:29,483 --> 01:15:32,109
Internal energy.

729
01:15:32,653 --> 01:15:34,946
The essence of life.

730
01:15:35,697 --> 01:15:39,825
It moves inside of us,
it flows through our bodies.

731
01:15:39,952 --> 01:15:42,328
Give us power from within.

738
01:16:34,881 --> 01:16:37,425
Everything good about me was born here.

739
01:16:38,343 --> 01:16:40,928
Xiao Dre, you can leave your backpack
and skateboard here.

741
01:16:43,348 --> 01:16:45,349
We journey to the top of the mountain.

742
01:16:45,892 --> 01:16:47,685
Drink from the Dragon Well.

752
01:17:50,582 --> 01:17:52,583
Water on top of the mountain.

753
01:20:10,597 --> 01:20:11,639
Xiao Dre.

754
01:20:22,734 --> 01:20:24,443
The journey is complete.

757
01:20:37,541 --> 01:20:39,166
I stood here

758
01:20:39,626 --> 01:20:42,211
with my father when I was your age.

759
01:20:43,004 --> 01:20:45,798
He told me it's magic kung fu water.

760
01:20:56,101 --> 01:20:59,228
You drink, and nothing can defeat you.

767
01:21:29,676 --> 01:21:31,927
You did not watch closely enough, Xiao Dre.

768
01:21:32,971 --> 01:21:35,139
The snake was copying the woman.

771
01:21:48,945 --> 01:21:50,154
Look.

772
01:21:50,822 --> 01:21:52,239
What do you see?

776
01:21:59,122 --> 01:22:00,915
Now what do you see?

778
01:22:02,918 --> 01:22:05,711
The woman was like still water.

779
01:22:06,421 --> 01:22:08,422
Quiet, calm.

780
01:22:09,049 --> 01:22:10,758
In here and in here.

781
01:22:11,551 --> 01:22:17,306
So, the snake reflects her action,
like still water.

784
01:22:28,151 --> 01:22:32,696
Being still and doing nothing
are two very different things.

787
01:22:44,668 --> 01:22:48,420
There's only one person
you need to learn how to control.

789
01:22:54,803 --> 01:22:56,637
Empty your mind.

790
01:22:56,721 --> 01:23:00,557
Flow with my movement.
Connect to the energy around you.

792
01:23:04,020 --> 01:23:06,230
Cobra takes a lifetime.

793
01:23:06,481 --> 01:23:07,982
Requires great focus.

796
01:23:15,198 --> 01:23:17,908
Your focus needs more focus.

807
01:25:08,645 --> 01:25:09,812
Up.

809
01:25:21,783 --> 01:25:23,117
Anticipation.

811
01:25:26,496 --> 01:25:27,871
Don't see it, feel it.

814
01:25:36,464 --> 01:25:37,881
What?

815
01:25:44,180 --> 01:25:46,098
Concentrate. Okay?

817
01:25:47,934 --> 01:25:49,601
Turn. Kick.

821
01:26:19,132 --> 01:26:22,009
Xiao Dre, we are not training tomorrow.

823
01:26:29,517 --> 01:26:34,521
Means, "Too much something is not good. "
You train a lot. You need rest.

832
01:27:08,306 --> 01:27:10,766
My audition is tomorrow at 6:00.

834
01:27:13,186 --> 01:27:14,686
Yes, of course.

836
01:27:18,066 --> 01:27:20,234
What if they don't pick me?

842
01:27:40,713 --> 01:27:42,547
I have to practice.

849
01:28:03,736 --> 01:28:04,820
Where are we going?

851
01:28:06,572 --> 01:28:08,532
I have to go! No!

860
01:29:35,787 --> 01:29:39,331
<i>You're a dance machine.
That was pretty good.</i>

862
01:29:42,126 --> 01:29:44,127
<i>Here comes Round 2.</i>

863
01:30:36,639 --> 01:30:38,557
<i>You're hot!</i>

867
01:31:06,461 --> 01:31:09,504
The audition, it was changed to today.

868
01:31:09,589 --> 01:31:11,339
Something happened.

869
01:31:11,591 --> 01:31:13,383
It's in 20 minutes.

870
01:31:13,676 --> 01:31:16,011
My father is coming to get me.

878
01:35:05,282 --> 01:35:07,075
We can no longer be friends.

879
01:35:08,911 --> 01:35:10,745
You are bad for my life.

880
01:35:25,094 --> 01:35:26,261
See you at the tournament.

881
01:35:28,305 --> 01:35:30,265
Don't be late for that.

883
01:36:24,195 --> 01:36:25,862
We no train today.

885
01:36:44,173 --> 01:36:45,882
It's June 8th.

887
01:38:19,685 --> 01:38:21,603
His name was Gong Gong.

888
01:38:23,939 --> 01:38:26,107
Xiao Dre, how old are you?

890
01:38:30,029 --> 01:38:31,613
He was 10.

891
01:38:33,782 --> 01:38:35,533
He was so beautiful.

892
01:38:39,496 --> 01:38:41,289
Her name was Zhang.

893
01:38:43,459 --> 01:38:45,168
She was a singer.

894
01:38:49,340 --> 01:38:51,007
Not professional.

895
01:38:53,552 --> 01:38:55,678
She sang only for me.

897
01:39:44,728 --> 01:39:46,729
It was a steep hill.

898
01:39:47,940 --> 01:39:49,607
Lots of rain.

899
01:39:51,735 --> 01:39:53,569
The car just...

900
01:39:56,740 --> 01:39:58,241
I was driving.

901
01:39:59,910 --> 01:40:02,078
We argue about something.

902
01:40:04,248 --> 01:40:05,957
I was so angry.

903
01:40:06,917 --> 01:40:08,543
I lost control.

904
01:40:09,795 --> 01:40:11,754
I try to remember.

905
01:40:13,424 --> 01:40:16,426
I cannot remember what we argue about.

906
01:40:18,387 --> 01:40:20,930
I hope it was something important.

907
01:40:24,560 --> 01:40:26,060
Every year,

908
01:40:27,229 --> 01:40:28,813
I fix the car.

909
01:40:30,232 --> 01:40:31,941
Still fix nothing.

910
01:44:19,336 --> 01:44:20,503
Punch.

911
01:44:21,380 --> 01:44:22,505
Good.

913
01:45:54,473 --> 01:45:55,931
Look at me.

914
01:45:56,016 --> 01:45:57,183
Down!

915
01:46:15,786 --> 01:46:16,786
Faster!

916
01:46:17,954 --> 01:46:18,954
One more.

917
01:46:23,543 --> 01:46:24,835
Come here.

918
01:47:16,763 --> 01:47:19,348
Win or lose, doesn't matter.

919
01:47:20,517 --> 01:47:22,017
Fight hard.

920
01:47:23,854 --> 01:47:26,188
Earn respect. Boys leave you alone.

921
01:47:32,195 --> 01:47:34,029
Have present for you.

927
01:47:53,091 --> 01:47:56,218
You have taught me
a very important lesson, Xiao Dre.

928
01:47:58,472 --> 01:48:00,598
Life will knock us down.

929
01:48:01,433 --> 01:48:05,394
But we can choose
whether or not to get back up.

935
01:48:52,692 --> 01:48:54,527
Sir...

936
01:48:55,153 --> 01:48:56,820
...my name is Dre Parker.

937
01:48:57,948 --> 01:48:59,949
My actions have brought...

938
01:49:00,200 --> 01:49:04,286
...dishonor to your family.

939
01:49:05,121 --> 01:49:09,625
Your daughter
has been a great friend to me.

940
01:49:10,835 --> 01:49:13,295
And from her, I have learned...

941
01:49:13,964 --> 01:49:15,631
...that a true friend...

942
01:49:16,216 --> 01:49:19,134
...is a person who makes your life better.

943
01:49:19,469 --> 01:49:23,889
But, if you give me a second chance...

944
01:49:25,141 --> 01:49:28,978
...I promise that I will be...

945
01:49:29,813 --> 01:49:35,234
...the best friend your daughter has ever had.

947
01:49:56,715 --> 01:50:01,677
My daughter told me that she made
a promise to be at your tournament.

948
01:50:02,262 --> 01:50:05,848
In our family, we do not break our promises.

949
01:50:10,020 --> 01:50:11,520
Good luck.

950
01:50:23,199 --> 01:50:24,450
Rock and roll.

953
01:50:51,227 --> 01:50:53,604
Let the tournament begin!

954
01:51:26,763 --> 01:51:28,097
We're up.

955
01:51:30,809 --> 01:51:32,768
Rulebook.

956
01:51:33,436 --> 01:51:35,938
Of course I know the rules.

957
01:51:36,356 --> 01:51:38,857
Simple. You hit him, don't let him hit you.

960
01:51:45,448 --> 01:51:46,782
Two points to win.

961
01:51:47,492 --> 01:51:51,120
Go. Hit him two times.
Focus, focus. Come on.

964
01:52:21,735 --> 01:52:23,944
That's a warning.
You cannot run out of the mat.

965
01:52:24,028 --> 01:52:25,654
Next time, you lose a point.

966
01:52:25,989 --> 01:52:27,823
Go. Hey, be strong.

968
01:53:04,360 --> 01:53:06,028
Good job. Next time, no face.

970
01:53:08,114 --> 01:53:09,448
Next point, winner.

974
01:54:57,432 --> 01:54:58,557
Go.

975
01:56:15,134 --> 01:56:19,137
Dre Parker advances to the semi-finals!

976
01:56:19,973 --> 01:56:23,308
From now on, semi-final matches.
Three points to win.

978
01:56:26,104 --> 01:56:27,813
Be kind of hot if you focus.

980
01:56:39,659 --> 01:56:43,745
And now the first semi-final of the evening...

981
01:58:59,590 --> 01:59:03,009
Next semi-final
introducing Fighting Dragon, Liang...

982
01:59:05,888 --> 01:59:08,140
...and the surprise finalist, Dre Parker.

983
01:59:21,279 --> 01:59:22,988
But, I can beat him.

984
01:59:23,072 --> 01:59:24,781
I do not want him beaten.

985
01:59:24,866 --> 01:59:25,949
I want him broken.

986
01:59:32,957 --> 01:59:34,791
Empty your mind.

987
01:59:34,876 --> 01:59:36,084
Focus.

988
02:00:43,736 --> 02:00:45,237
You're disqualified!

989
02:00:50,785 --> 02:00:52,911
Be still. Xiao Dre.

990
02:00:53,496 --> 02:00:54,829
You okay?

991
02:01:08,552 --> 02:01:10,095
Don't move. Don't move.

992
02:01:19,063 --> 02:01:20,480
He's done.

993
02:01:21,857 --> 02:01:23,441
I'm sorry, you should not continue.

994
02:01:24,068 --> 02:01:26,820
You've brought honor to your family.

995
02:01:29,782 --> 02:01:31,950
Doctor says you did great.

998
02:01:49,260 --> 02:01:52,387
Dre Parker has 2 minutes
to return to the match.

999
02:01:52,638 --> 02:01:54,723
If he is unable to return...

1000
02:01:54,807 --> 02:01:59,352
...the Fighting Dragons win by default.

1004
02:02:21,709 --> 02:02:24,169
Win or lose doesn't matter, Xiao Dre.

1006
02:02:32,636 --> 02:02:36,014
Yes. I think you had a good chance.

1008
02:02:44,815 --> 02:02:47,150
You don't need to fight anymore.

1009
02:02:47,234 --> 02:02:49,152
You have proven everything
you need to prove.

1015
02:03:09,048 --> 02:03:13,176
Because I cannot watch you
get hurt anymore.

1018
02:03:23,396 --> 02:03:25,772
Just tell me, Xiao Dre, why?

1019
02:03:26,399 --> 02:03:28,483
Why you need to
go back out there so badly?

1024
02:03:57,263 --> 02:04:01,975
Since Dre Parker has not
returned to the ring...

1025
02:04:22,538 --> 02:04:25,123
Dre Parker will fight!

1026
02:06:18,487 --> 02:06:19,696
Xiao Dre.

1028
02:08:23,862 --> 02:08:24,904
You okay?

1030
02:08:30,202 --> 02:08:31,577
I want you...

1031
02:08:32,663 --> 02:08:35,206
...to break his leg.

1032
02:08:37,000 --> 02:08:38,751
No mercy.

1033
02:09:53,494 --> 02:09:56,996
The score is tied, the next point wins!

1034
02:10:04,213 --> 02:10:06,798
Can you continue?

1036
02:12:02,664 --> 02:12:03,915
Finish him!

1037
02:12:34,696 --> 02:12:36,280
You won!

1040
02:13:35,340 --> 02:13:36,382
Good.

