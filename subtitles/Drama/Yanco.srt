﻿1
00:00:00,166 --> 00:00:03,766
<i>Servando Gonsalez presents.</i>

2
00:00:25,116 --> 00:00:28,216
"Yanco."

3
00:11:09,266 --> 00:11:11,834
Here's the little jerk again!

4
00:15:53,118 --> 00:15:55,615
Juan!

5
00:17:33,918 --> 00:17:38,487
Demon, leave this boy.

6
00:19:18,023 --> 00:19:21,391
Juanito Kuitlauak.

7
00:19:21,426 --> 00:19:23,393
Present, teacher.

8
00:19:33,421 --> 00:19:34,738
Irene Medina.

9
00:19:34,806 --> 00:19:37,123
Present, professor.

10
00:19:47,535 --> 00:19:49,553
Esteban San Migel.

11
00:19:49,754 --> 00:19:51,922
Present, prof!

12
00:19:58,062 --> 00:19:59,629
Refugio Pena.

13
00:19:59,697 --> 00:20:01,665
Present, teacher.

14
00:20:16,214 --> 00:20:18,281
Concepcion Jimenez.

15
00:20:18,316 --> 00:20:20,210
Present, teacher.

16
00:20:29,494 --> 00:20:30,861
Manuel Rodriguez.

17
00:20:30,995 --> 00:20:33,320
Present, professor.

18
00:20:42,674 --> 00:20:43,940
Teodoro Feria.

19
00:20:43,975 --> 00:20:45,725
Present.

20
00:20:54,385 --> 00:20:55,628
Justo Medina.

21
00:20:55,679 --> 00:20:57,404
Present, teacher.

22
00:20:59,807 --> 00:21:02,028
Hey, all right! Ah!

23
00:21:15,068 --> 00:21:16,568
Stand up.

24
00:21:24,515 --> 00:21:32,255
♪ In each encounter I find a brother... ♪

25
00:21:32,357 --> 00:21:38,829
♪ Who shares my love of my country. ♪

26
00:21:40,665 --> 00:21:47,337
♪ In each encounter I find a brother... ♪

27
00:21:47,572 --> 00:21:54,544
♪ Who shares my love of my country. ♪

28
00:25:46,745 --> 00:25:49,179
I want some chocolate!

29
00:25:59,696 --> 00:26:02,201
Did you see where my son went?

30
00:26:03,595 --> 00:26:06,708
♪ The viper, from the sea, the sea... ♪

31
00:26:06,758 --> 00:26:09,027
♪ How could it happen? ♪

32
00:26:09,968 --> 00:26:13,725
♪ Some stay behind and some run ahead. ♪

33
00:30:52,991 --> 00:30:56,660
- Have you seen my son?
- Sorry, no.

34
00:33:30,258 --> 00:33:33,846
- Have you seen a boy about this tall go by?
- No.

35
00:34:28,132 --> 00:34:29,576
No?

36
00:34:31,297 --> 00:34:34,206
He's there with the old grandfather.

37
00:43:06,651 --> 00:43:08,719
Excuse me, friend, have you seen Juanito?

38
00:43:08,820 --> 00:43:10,383
No, sir.

39
00:45:09,273 --> 00:45:11,341
We've tried aromatic herbs, dead chickens.

40
00:45:11,642 --> 00:45:13,610
It's not possible in front of boy.

41
00:45:13,911 --> 00:45:15,545
Let's go, doctor.

42
00:45:15,713 --> 00:45:17,347
Right now.

43
00:45:17,548 --> 00:45:19,516
Come on.

44
00:45:26,724 --> 00:45:29,542
- Right here, doctor. Go on, go on.
- Well, let's see.

45
00:45:33,564 --> 00:45:35,532
And you, old ladies...

46
00:45:40,304 --> 00:45:42,039
Go cure some dogs.

47
00:45:42,240 --> 00:45:44,207
Push off.

48
00:45:56,654 --> 00:45:58,622
He's the devil, girls!

49
00:46:15,239 --> 00:46:17,857
Lamb of God, who takest
away the sins of the world...

50
00:46:17,875 --> 00:46:19,693
Forgive us our sins, Lord.

51
00:46:19,777 --> 00:46:22,462
Lamb of God, who takest
away the sins of the world...

52
00:46:22,496 --> 00:46:23,847
Hear us, Lord!

53
00:46:23,915 --> 00:46:26,550
Lamb of God, who takest
away the sins of the world...

54
00:46:27,552 --> 00:46:29,519
Have mercy on him and on us.

55
00:46:30,154 --> 00:46:32,122
Our Father, who art in heaven...

56
00:46:43,301 --> 00:46:47,270
- In the name of the Father.
- In the name of the Father.

57
00:47:22,407 --> 00:47:24,874
Hail Mary, full of grace,
the Lord is with thee...

58
00:47:24,908 --> 00:47:26,426
blessed are you among women...

59
00:47:26,544 --> 00:47:28,712
and blessed is the fruit
of thy womb, Jesus.

60
00:47:36,387 --> 00:47:38,855
Hail Mary, full of grace,
the Lord is with thee...

61
00:47:38,889 --> 00:47:40,407
blessed are you among women...

62
00:47:40,458 --> 00:47:42,626
and blessed is the fruit
of thy womb, Jesus.

63
00:47:43,795 --> 00:47:46,212
Holy Mary, Mother of
God, pray for us sinners...

64
00:49:48,402 --> 00:49:49,570
Mama!

65
00:49:49,954 --> 00:49:51,922
Juancito!

66
00:57:26,044 --> 00:57:28,011
Butter, I have good butter.

67
00:58:43,087 --> 00:58:45,055
Sir, sir!

68
00:58:49,544 --> 00:58:52,763
No. I'd like the violin instead.

69
00:58:55,700 --> 00:58:57,667
No, no.

70
00:59:12,427 --> 00:59:13,805
No.

71
01:00:31,262 --> 01:00:34,280
Already taken? Why should
I remove the round?

72
01:00:42,507 --> 01:00:44,474
Well, all right... thanks, marshal.

73
01:00:52,717 --> 01:00:55,385
I'm going, because I have
to go to my father's house.

74
01:00:55,687 --> 01:00:57,654
Don Arcadio, good night.

75
01:00:58,022 --> 01:00:59,589
Good night, sir. Take care, eh?

76
01:00:59,607 --> 01:01:01,425
Sure, Don Arcadio.

77
01:01:24,122 --> 01:01:26,093
<i>Sign: "Shop".</i>

78
01:14:49,020 --> 01:14:51,188
Open up, it's already too late!

79
01:14:53,157 --> 01:14:55,125
Come on, Don Arcadio.

80
01:15:02,166 --> 01:15:03,734
Come on, Don Arcadio.

81
01:15:05,136 --> 01:15:07,104
It's very late.

82
01:15:08,306 --> 01:15:11,073
Let's see. What do you want? Sugar?

83
01:15:11,109 --> 01:15:13,026
What do you want?

84
01:18:01,813 --> 01:18:03,630
I picked up the basket...

85
01:18:03,715 --> 01:18:05,832
and looked for it...

86
01:18:06,417 --> 01:18:08,785
but the violin wasn't there.

87
01:18:11,756 --> 01:18:13,990
The violin appears and disappears.

88
01:18:16,894 --> 01:18:18,862
Devilish!

89
01:18:59,303 --> 01:19:01,271
Right, guys!

90
01:19:01,639 --> 01:19:03,974
What we need is oil.

91
01:19:07,679 --> 01:19:09,646
Welcome.

92
01:20:46,643 --> 01:20:48,311
What's going on?

93
01:21:03,261 --> 01:21:05,328
He nicked the violin!

94
01:21:15,489 --> 01:21:17,657
Don't leave me alone...

95
01:21:17,809 --> 01:21:19,843
Wait for me!

96
01:21:20,227 --> 01:21:22,345
You call yourselves men!

97
01:21:55,947 --> 01:21:57,914
Help! Help!

98
01:22:06,223 --> 01:22:07,941
Just look.

99
01:22:15,468 --> 01:22:19,636
Pedro, Pedro!

100
01:22:22,523 --> 01:22:24,441
Where are you?

101
01:22:26,127 --> 01:22:28,245
Pedro!

102
01:25:16,031 --> 01:25:19,699
No need to fret, just calm down.

103
01:31:02,102 --> 01:31:06,294
Ricardo Ancona as Juanito

104
01:31:06,822 --> 01:31:10,634
Jesus Medina as Old Man

105
01:31:10,810 --> 01:31:15,437
Maria Bustamante as Maria.

106
01:31:21,100 --> 01:31:26,116
Idea and essay by
Servando Gonzalez.

107
01:31:26,907 --> 01:31:33,071
Totally filmed in Mixquic village
Nahuatl vernacular.

108
01:31:33,889 --> 01:31:36,389
Cinematography by
Alex Phillips Jr.

109
01:31:37,042 --> 01:31:40,064
Film Editing by
Fernando Martinez.

110
01:31:40,107 --> 01:31:43,385
Original Music by
Gustavo Cesar Carrion.

111
01:31:43,493 --> 01:31:46,845
Screenplay by Jesus Marin.

112
01:31:46,921 --> 01:31:51,376
Executive producer
Miguel Gonzalez.

113
01:31:52,454 --> 01:31:58,889
Sound editor - Raul Portillo Re-recording mixer - Galdino Samperio
Special Effects by Juan Munoz Ravelo Titles by Nicolas Rueda Jr.

114
01:32:03,603 --> 01:32:05,643
Production company: Producciones
Yanco (Mexico) 1961

