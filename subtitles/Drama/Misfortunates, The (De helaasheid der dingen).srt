1
00:00:07,651 --> 00:00:09,581
Episode 3

2
00:00:09,582 --> 00:00:17,797
"Die Fahne hoch" by Horst Wessel

3
00:00:09,582 --> 00:00:17,797
{\a6}[Hymn of the Nazi Party from 1930~1945]

4
00:00:28,966 --> 00:00:35,913
{\a6}<i>"Marschieren im Geist in unseren Reihen mit"</i>

5
00:00:28,966 --> 00:00:35,913
<i>March in spirit within our ranks</i>

6
00:00:36,039 --> 00:00:43,230
{\a6}<i>"Die Stra��e frei den braunen Bataillonen"</i>

7
00:00:36,039 --> 00:00:43,230
<i>free the way for the brown battalions</i>

8
00:00:43,334 --> 00:00:50,745
{\a6}<i>"Die Stra��e frei dem Sturmabteilungsmann"</i>

9
00:00:43,334 --> 00:00:50,745
<i>free the way for the stormtroopers</i>

10
00:00:54,426 --> 00:00:57,018
<i>- Suam! Suam!
- Suam.</i>

11
00:00:58,400 --> 00:01:00,002
- Suam!
- Suam.

12
00:01:00,151 --> 00:01:04,285
- Suam.
- Suam!

13
00:01:18,200 --> 00:01:19,698
What have you done?

14
00:01:19,812 --> 00:01:25,036
I just shot your cat,
because he killed our hen!

15
00:01:28,243 --> 00:01:31,425
This is Suam.

16
00:01:31,757 --> 00:01:36,625
He carried my cousin's name,
and was like family to me.

17
00:01:36,712 --> 00:01:39,256
It's nothing but a damn cat!

18
00:01:39,370 --> 00:01:42,086
And what are you?

19
00:01:53,173 --> 00:01:56,572
You damn Asians...

20
00:02:11,954 --> 00:02:14,284
Doctor Li.

21
00:02:14,388 --> 00:02:17,554
I'll apologize on his behalf.

22
00:02:18,029 --> 00:02:21,739
<i>People have changed too much.</i>

23
00:02:27,429 --> 00:02:29,699
But don't let it put you down,

24
00:02:29,700 --> 00:02:33,647
not everyone is taking this lying down.

25
00:02:36,560 --> 00:02:39,597
This was in the mailbox.

26
00:03:02,780 --> 00:03:06,979
"There is nothing as shameful as silently
enduring a regime's undignified..."

27
00:03:06,980 --> 00:03:10,290
and despicable delusion
without any resistance."

28
00:03:12,814 --> 00:03:16,027
""The world is like a mysterious vessel,
impossible for man to control."

29
00:03:16,028 --> 00:03:18,223
"Anyone trying to control
it will destroy it all,"

30
00:03:18,224 --> 00:03:22,344
"those trying to grab it
with their hands will lose it." Lao-Tse*

31
00:03:18,536 --> 00:03:22,344
{\a6}[*a.k.a. Laozi]

32
00:03:39,583 --> 00:03:43,745
You're not involved in this, are you?

33
00:03:56,085 --> 00:04:01,892
{\a6}[*Confucian philosopher from
the Warring States Period]

34
00:03:56,085 --> 00:04:01,892
Xun Zi* once said, knowing
is better than seeing,

35
00:04:01,893 --> 00:04:07,134
and that acting is
better than knowing.

36
00:04:07,256 --> 00:04:10,578
But he also said this,

37
00:04:11,096 --> 00:04:16,400
that if you erect a straight tree,
you're only hoping for its shadow to bend.

38
00:04:16,401 --> 00:04:22,161
Trying to act despite
knowing you will fail is foolish.

39
00:04:22,278 --> 00:04:25,571
Your lives could be in danger.

40
00:04:25,679 --> 00:04:27,799
Yes, it's dangerous.

41
00:04:28,875 --> 00:04:32,027
But someone must do it.

42
00:04:32,688 --> 00:04:35,765
Didn't you come
to Germany for the same?

43
00:04:35,766 --> 00:04:38,871
- Because you were one of those people?
- But you have a family...

44
00:04:38,872 --> 00:04:42,199
and responsibilities towards them.

45
00:04:42,200 --> 00:04:47,954
Know what I was thinking,
when I crossed the Yalu?

46
00:04:49,040 --> 00:04:54,993
Seeing my mother's
face as I was leaving,

47
00:04:54,994 --> 00:05:02,077
I wished I never took part in
the resistance in the first place.

48
00:05:04,323 --> 00:05:10,053
If you could go back in time,
would you have done otherwise?

49
00:05:14,431 --> 00:05:16,449
See?

50
00:05:21,419 --> 00:05:23,777
[*Fellow students!*]

51
00:05:48,302 --> 00:05:52,802
[*Fellow students! German people
were shaken by our defeat in Stalingrad*...]

52
00:05:48,302 --> 00:05:56,302
{\a6}[*Battle of Stalingrad, 1.7mil dead,
and one of Germany's worst defeats]

53
00:07:41,197 --> 00:07:43,962
Doctor Li.
You have a guest.

54
00:07:52,340 --> 00:07:55,330
How do you do?
My name is Eva,

55
00:07:55,331 --> 00:07:57,905
I'm majoring in Asian studies.

56
00:07:58,046 --> 00:08:01,507
I've read all your
work, Seonsaengnim*.

57
00:07:58,046 --> 00:08:01,507
{\a6}[*deferential for esteemed elder/professor]

58
00:08:03,090 --> 00:08:07,156
Seonsaengnim,
take me as your pupil.

59
00:08:08,201 --> 00:08:10,790
You already speak my language,

60
00:08:10,900 --> 00:08:12,855
what else would I teach you?

61
00:08:12,856 --> 00:08:18,387
I want to learn all of Korea's
culture and customs.

62
00:08:18,922 --> 00:08:25,490
I want to go and see for
myself, whether Korea is...

63
00:08:25,525 --> 00:08:28,200
as beautiful as you depicted it.

64
00:08:41,696 --> 00:08:43,679
Then, did you?

65
00:08:43,804 --> 00:08:46,612
Are you part of the White Rose, too?

66
00:08:46,718 --> 00:08:48,387
No.

67
00:08:48,495 --> 00:08:53,849
I just wanted to tell people how
many men lost their lives in this war.

68
00:08:54,174 --> 00:08:56,328
What you did was too dangerous.

69
00:08:56,442 --> 00:09:00,402
Then, why did you help me out?

70
00:09:03,426 --> 00:09:08,089
Wasn't it because you
think what we're doing is right?

71
00:09:09,950 --> 00:09:13,037
Reading your works,

72
00:09:13,148 --> 00:09:23,100
I felt a sense of peace about you,
so much I wanted to meet you in person.

73
00:09:25,407 --> 00:09:30,419
Seonsaengnim, please
take me as your pupil.

74
00:09:43,522 --> 00:09:47,121
Your calligraphy is a mess.

75
00:09:49,336 --> 00:09:51,527
You will have to
learn it a little more.

76
00:11:16,644 --> 00:11:22,960
This chocolate was sent from
abroad by one of Eva's friends.

77
00:11:23,071 --> 00:11:26,921
Ahh... precious chocolate.

78
00:11:29,500 --> 00:11:32,447
Remember how long ago
we had something like this?

79
00:11:34,911 --> 00:11:39,184
Just eating something so
precious would be a waste.

80
00:11:39,558 --> 00:11:41,248
Don't you think, Mirok?

81
00:12:13,576 --> 00:12:14,729
Quick, quick!

82
00:12:36,114 --> 00:12:37,237
Six, six!

83
00:12:39,796 --> 00:12:42,323
Yes, yes... okay.

84
00:12:43,758 --> 00:12:44,819
Six! Six!

85
00:12:46,630 --> 00:12:48,416
<i>Ahh... no.</i>

86
00:12:52,933 --> 00:12:54,665
<i>Six!
Good.. good.</i>

87
00:12:57,301 --> 00:12:58,442
Ahhh... five.

88
00:13:01,354 --> 00:13:02,106
Quick, quick.

89
00:13:09,646 --> 00:13:11,236
Six!

90
00:13:32,245 --> 00:13:33,651
<i>Quick, do it quickly!</i>

91
00:13:33,652 --> 00:13:35,573
<i>Six, six!</i>

92
00:13:40,224 --> 00:13:43,070
Six! Six.
One six.

93
00:13:44,937 --> 00:13:46,222
That's cheating!

94
00:13:46,378 --> 00:13:49,735
And why?
Hurry up and give me the chocolate.

95
00:13:50,278 --> 00:13:53,979
How do you know it was really a six?

96
00:13:58,078 --> 00:14:00,811
I saw it, it was a six.

97
00:14:00,932 --> 00:14:02,557
It fell off, so it doesn't count.

98
00:14:02,668 --> 00:14:05,940
Don't you think this is too much?
I haven't touched that chocolate once!

99
00:14:05,941 --> 00:14:09,714
- Anyway, do it once again.
- This is really unfair!

100
00:14:09,854 --> 00:14:13,198
This is how.... just...
the game works!

101
00:15:00,840 --> 00:15:03,911
It was a nice wake up call for them.

102
00:15:05,379 --> 00:15:08,684
That simple game
made them lose their smile.

103
00:15:09,047 --> 00:15:13,707
But hunger, like war,
makes your forget reason and dignity.

104
00:15:20,777 --> 00:15:25,141
Doctor Li....
I will soon be arrested.

105
00:15:26,779 --> 00:15:30,977
The reason I ignored you last time
was not to involve you in this.

106
00:15:31,133 --> 00:15:34,258
If they arrest you now,
it will mean death!

107
00:15:35,098 --> 00:15:38,324
When I started this, I
knew I would have nowhere to run.

108
00:15:38,325 --> 00:15:40,540
But if they arrest me, people
will learn what we're doing.

109
00:15:40,541 --> 00:15:43,350
I believe many more people
will sympathize with our cause.

110
00:15:43,351 --> 00:15:47,193
- Professor...
- This is how I'll show my love for this country.

111
00:15:47,299 --> 00:15:50,262
But I hope you will help
Germany in other ways.

112
00:15:52,084 --> 00:15:55,327
But...
But how can I...

113
00:15:55,454 --> 00:16:00,215
You have the power to surprise
people, like you did earlier.

114
00:16:02,088 --> 00:16:05,874
Our people have lost their sense of human
dignity because of this war.

115
00:16:05,985 --> 00:16:12,649
Help Germany regain that dignity,
for itself and everyone else.

116
00:17:37,936 --> 00:17:41,120
Are you bringing that
to Huber's family again?

117
00:17:41,451 --> 00:17:46,828
Haven't you heard they're arresting
everyone who tries to help them?

118
00:17:47,597 --> 00:17:51,587
You don't need to worry about me, Lina.

119
00:17:51,699 --> 00:17:54,694
Don't worry?
How can I not?

120
00:17:56,022 --> 00:18:01,602
We can survive
with just a little food,

121
00:18:01,711 --> 00:18:10,919
but those good people will not
last a single day, if they lose their hope.

122
00:18:19,294 --> 00:18:22,178
I know how hard it must be for you.

123
00:18:22,616 --> 00:18:27,123
I feel sorry for not helping you more.

124
00:18:27,467 --> 00:18:28,685
Look, Doctor Li!

125
00:18:28,686 --> 00:18:36,112
Just by coming here, a place abandoned
by everyone after he was arrested,

126
00:18:37,908 --> 00:18:39,650
I'm already thankful.

127
00:18:40,401 --> 00:18:43,318
You must be strong.

128
00:18:45,128 --> 00:18:47,825
Unless a miracle happens by next week,

129
00:18:47,943 --> 00:18:50,715
my poor kids will lose their father.

130
00:19:31,317 --> 00:19:33,502
Hey, you there!

131
00:19:35,295 --> 00:19:37,327
What is inside this bag?

132
00:19:43,152 --> 00:19:45,258
Tao Te Ching*

133
00:19:43,152 --> 00:19:45,258
{\a6}[Laozi's essential Taoist text,
6th Century BC]

134
00:19:45,434 --> 00:19:46,569
What is this?

135
00:19:47,335 --> 00:19:51,145
It's a book about
classic oriental philosophy.

136
00:19:51,529 --> 00:19:53,806
And you need this why, exactly?

137
00:19:53,916 --> 00:19:56,630
Aren't you trying to
diffuse disturbing ideology?

138
00:19:56,742 --> 00:19:58,611
<i>Doctor Li.</i>

139
00:19:59,501 --> 00:20:00,889
What are you doing here?

140
00:20:00,983 --> 00:20:03,244
- Do you know this man?
- Yes.

141
00:20:03,604 --> 00:20:08,627
He's my teacher, here to
teach me Chinese characters.

142
00:20:09,397 --> 00:20:13,500
Ahh, here it is. I've been
looking for this for a while,

143
00:20:13,501 --> 00:20:15,522
I didn't know you had it.

144
00:20:15,662 --> 00:20:16,598
If you pardon me.

145
00:20:16,692 --> 00:20:18,876
- Is this yours?
- Yes.

146
00:20:19,095 --> 00:20:20,826
Read it for me, then.

147
00:20:28,907 --> 00:20:33,400
{\a6}"Taesang, Hajiyuji"

148
00:20:28,907 --> 00:20:33,400
"The best form of authority is one
where people acknowledge their ruler"

149
00:20:33,509 --> 00:20:35,537
{\a6}"Gicha, Chiniyeji"

150
00:20:33,509 --> 00:20:35,660
"one where the ruler loves
and is close to his people"

151
00:20:35,662 --> 00:20:40,170
{\a6}"Gicha Wiji, Gicha Moji"

152
00:20:35,662 --> 00:20:40,170
"one where a ruler fears his people,
but also makes them despise him"

153
00:20:41,855 --> 00:20:43,478
And what does this all mean?

154
00:20:43,759 --> 00:20:47,382
That the best form of authority
is akin to that of our Fuhrer Adolf Hitler,

155
00:20:47,491 --> 00:20:51,138
and his strong autocracy leading us all.

156
00:20:52,744 --> 00:20:57,501
We're gathering texts like this
to boost loyalty for the Fuhrer and the party.

157
00:20:57,600 --> 00:20:58,968
Heil Hitler.

158
00:21:01,995 --> 00:21:03,274
Heil Hitler.

159
00:21:16,347 --> 00:21:19,830
I think this might be useful to you.

160
00:21:22,946 --> 00:21:27,189
After the execution of Professor Huber,
anyone connected to him,

161
00:21:29,373 --> 00:21:34,197
or even those who tried to
help him are all being arrested.

162
00:21:34,323 --> 00:21:37,532
It's best just to avoid
all this while we can.

163
00:21:37,533 --> 00:21:40,603
While you're here with me
in Switzerland at this congress,

164
00:21:40,604 --> 00:21:43,725
this White Rose scandal will quiet down.

165
00:21:44,372 --> 00:21:46,483
<i>They disgraced the LMU,
we must get them all and kill them.</i>

166
00:21:46,484 --> 00:21:48,170
They'll learn that anyone trying
to oppose our great leader...

167
00:21:48,171 --> 00:21:51,984
is nothing but a
traitor to the German Empire.

168
00:21:52,082 --> 00:21:53,842
And who is this Hitler?

169
00:21:53,955 --> 00:21:56,400
You don't even know
the name of our great Fuhrer?

170
00:21:56,529 --> 00:21:57,445
No.

171
00:21:57,570 --> 00:21:59,504
Where do you come from?

172
00:21:59,645 --> 00:22:00,663
Germany.

173
00:22:00,768 --> 00:22:03,529
Are you making fun
of our great Fuhrer?

174
00:22:05,279 --> 00:22:08,754
Gentlemen, please.
Don't misunderstand.

175
00:22:08,864 --> 00:22:14,917
This man came here from Asia
for an holiday, so he doesn't know.

176
00:22:15,822 --> 00:22:17,587
Let's go.

177
00:22:26,507 --> 00:22:28,926
Milady.

178
00:22:29,020 --> 00:22:31,890
Don't take all this personal.

179
00:22:31,891 --> 00:22:36,929
I'm just doing this for our country.
Don't you agree?

180
00:22:37,038 --> 00:22:38,988
Move!

181
00:22:39,258 --> 00:22:40,735
Let's go!

182
00:23:41,997 --> 00:23:43,713
That morning, I received news from
my faraway hometown.

183
00:23:43,714 --> 00:23:46,521
My older sister wrote that my mother
passed away, after suffering for a few days.

184
00:23:46,650 --> 00:23:48,393
End

185
00:24:09,577 --> 00:24:12,775
(Thomas) Mann
Buddenbrooks

186
00:24:12,885 --> 00:24:16,148
Let's write that letter now, Seonsaengnim.

187
00:24:19,125 --> 00:24:24,694
Piper only publishes novels
from the most illustrious writers.

188
00:24:24,866 --> 00:24:28,832
Would they possibly publish something
from a nameless writer like me,

189
00:24:28,937 --> 00:24:33,852
not to mention having interest
in any story from the Far East?

190
00:24:33,960 --> 00:24:37,694
I'm sure they will publish it.

191
00:24:38,973 --> 00:24:40,451
Come on, Seonsaengnim.

192
00:24:49,079 --> 00:24:56,025
<i>My first novel is a simple
account of the experiences...</i>

193
00:24:56,026 --> 00:25:03,073
<i>I went through in my childhood.</i>

194
00:25:03,074 --> 00:25:07,250
<i>It purely depicts what happened,</i>

195
00:25:07,251 --> 00:25:14,633
<i>and attempts to represent the
human psyche of East Asian men.</i>

196
00:25:16,395 --> 00:25:21,830
<i>It neither tries to put the
spotlight on their ideal tendencies,</i>

197
00:25:21,831 --> 00:25:30,750
<i>nor attempts to spread
any pretentious ethical musings.</i>

198
00:25:32,526 --> 00:25:36,108
<i>I merely wrote about
my nostalgia for things lost,</i>

199
00:25:36,109 --> 00:25:44,225
<i>and the transience of life.</i>

200
00:25:47,657 --> 00:25:52,876
<i>By reading this book, I wish my German friends
will be able to reminisce about...</i>

201
00:25:53,559 --> 00:26:04,232
<i>the happy memories they made,
however brief they might be.</i>

202
00:27:40,551 --> 00:27:44,078
Japan will soon capitulate.

203
00:27:44,187 --> 00:27:47,198
Your homeland will
soon be free, Doctor Li.

204
00:27:52,880 --> 00:27:55,998
<i>Milady!
Milady!</i>

205
00:27:56,122 --> 00:27:59,320
It's all over!
We're finally done with it!

206
00:27:59,834 --> 00:28:01,098
Done with what?

207
00:28:01,223 --> 00:28:03,079
We were liberated!

208
00:28:03,188 --> 00:28:06,979
Down at the village, people
gathered in the streets to celebrate,

209
00:28:06,980 --> 00:28:10,664
and those Japanese fools are running
away like there was no tomorrow.

210
00:28:10,976 --> 00:28:14,810
Now your husband can come back.

211
00:28:14,919 --> 00:28:17,868
Is this a dream, or what?

212
00:28:21,487 --> 00:28:23,500
I brought this fool to you, Milady!

213
00:28:23,501 --> 00:28:27,177
You fool, how much longer
do you think you could last...

214
00:28:27,178 --> 00:28:29,431
playing a pawn to those Japanese?

215
00:28:30,882 --> 00:28:34,919
<i>Milady, what should we do with him?</i>

216
00:28:35,543 --> 00:28:37,306
Let him go.

217
00:28:38,881 --> 00:28:40,552
<i>What?</i>

218
00:29:07,465 --> 00:29:09,119
<i>Mister Li.</i>

219
00:29:12,655 --> 00:29:14,434
A letter for you.

220
00:29:17,241 --> 00:29:19,632
Seonsaengnim,
hurry up and open it!

221
00:29:32,804 --> 00:29:34,823
Is something wrong?

222
00:29:35,170 --> 00:29:40,002
My novel is going to be published.

223
00:29:40,173 --> 00:29:42,077
Ahh... my God!

224
00:29:42,202 --> 00:29:44,823
I knew it!
Congratulations.

225
00:30:51,120 --> 00:30:53,504
If it weren't for you...

226
00:30:53,633 --> 00:30:56,796
none of this would have been possible.

227
00:30:59,686 --> 00:31:02,679
Then, I have a favor.

228
00:31:03,076 --> 00:31:05,813
Give me a name in Hangeul.

229
00:31:07,156 --> 00:31:12,395
I want you to give
me a name, Seonsaengnim.

230
00:31:19,137 --> 00:31:24,267
"Ae*"

231
00:31:19,137 --> 00:31:24,267
{\a6}[*Ae = Love]

232
00:31:26,063 --> 00:31:27,661
Ae...

233
00:31:30,960 --> 00:31:33,489
Pa*...

234
00:31:30,960 --> 00:31:33,489
{\a6}[*Aepa = Wave of Love]

235
00:31:34,251 --> 00:31:40,610
Does this really mean
what I'm thinking?

236
00:31:45,741 --> 00:31:47,534
Doctor Li.

237
00:31:47,649 --> 00:31:50,113
I have wonderful news for you.

238
00:31:50,219 --> 00:31:56,335
Your novel will be featured in
our German school textbooks.

239
00:31:57,252 --> 00:32:02,214
From now on, all German
children will learn about your hometown.

240
00:32:02,921 --> 00:32:04,334
Seonsaengnim!

241
00:32:15,257 --> 00:32:18,285
{\a6}<i>[Li Bai's "Homesick on a Silent Night"]
Sang Jeon Myeong Wol Gwang</i>

242
00:32:15,257 --> 00:32:18,285
"Before my bed you shed,
O lustrous moonlight"

243
00:32:18,410 --> 00:32:21,548
{\a6}"Eui Shi Ji Sang Sang"

244
00:32:18,410 --> 00:32:21,548
"Like frost caressing
the ground, I doubt "

245
00:32:21,655 --> 00:32:24,951
{\a6}"Geo Du Mang Myeong Wol"

246
00:32:21,655 --> 00:32:24,951
"You capture my eyes,
moon of many feelings bright"

247
00:32:25,085 --> 00:32:29,625
{\a6}"Jeo Du Sa Go Hyang"

248
00:32:25,085 --> 00:32:29,625
"And down I look,
my yearning gushing out"

249
00:32:31,500 --> 00:32:35,026
<i>The liberation our
country found itself in...</i>

250
00:32:35,027 --> 00:32:38,474
<i>doesn't feel like liberation at all.</i>

251
00:32:38,625 --> 00:32:41,859
<i>Only the South is having elections,</i>

252
00:32:41,996 --> 00:32:46,526
<i>and, if this keeps up, I worry our
country might be divided in two.</i>

253
00:32:47,008 --> 00:32:49,423
<i>Doctor Lee.</i>

254
00:32:49,549 --> 00:32:52,448
<i>Before it's too late,
shouldn't you return home,</i>

255
00:32:52,605 --> 00:32:55,617
<i>and do whatever you
can for your country?</i>

256
00:33:11,814 --> 00:33:13,228
<i>Seonsaengnim.</i>

257
00:33:13,805 --> 00:33:16,300
Your novel is becoming
more and more popular.

258
00:33:16,301 --> 00:33:19,452
And more students are
majoring in Asian studies.

259
00:33:33,121 --> 00:33:35,162
Is anything wrong?

260
00:33:35,306 --> 00:33:38,671
I received a letter
from Doctor Kim Ji Won.

261
00:33:38,872 --> 00:33:42,462
He wonders why I don't
try to help my homeland.

262
00:33:43,538 --> 00:33:46,051
<i>He's asking me to return to Korea.</i>

263
00:34:29,438 --> 00:34:32,506
If you leave that door open,

264
00:34:33,163 --> 00:34:35,657
will he return home?

265
00:34:35,783 --> 00:34:40,575
Leave that open for a thousand years,
and you still will see nothing.

266
00:34:40,697 --> 00:34:46,859
For a few decades every morning,
how could you not miss a single day?

267
00:34:47,986 --> 00:34:52,211
Only seeing that
open door drives me crazy.

268
00:35:11,726 --> 00:35:15,391
You heartless man.

269
00:35:15,515 --> 00:35:21,026
Will she be able to see
you again before it's too late?

270
00:36:15,409 --> 00:36:20,779
Are you still thinking about home?

271
00:36:28,186 --> 00:36:30,538
Hoffman Miniatures

272
00:36:57,052 --> 00:37:01,478
I tried to build it in
accordance with your design.

273
00:37:01,604 --> 00:37:06,424
It's just as I remembered it,
exactly the same.

274
00:37:07,098 --> 00:37:08,892
Thank you very much.

275
00:37:11,290 --> 00:37:13,900
Thank you for
showing me your house.

276
00:37:14,020 --> 00:37:17,546
In this part of the house
live the men of the family,

277
00:37:17,671 --> 00:37:21,104
and this is where the women stay.

278
00:37:21,228 --> 00:37:25,440
Ahhh... so this is
how home looked like.

279
00:37:25,598 --> 00:37:28,065
I'd like to see this place myself.

280
00:37:28,186 --> 00:37:34,396
This way is the room where
Suam and I stole all that honey.

281
00:37:35,018 --> 00:37:37,036
You don't look good.

282
00:37:37,139 --> 00:37:39,671
It just my stomach,
nothing to worry about.

283
00:37:39,809 --> 00:37:44,581
No wonder, you're skipping
meals all the time these days.

284
00:37:49,258 --> 00:37:51,470
Calm down, please.

285
00:37:51,595 --> 00:37:54,466
You'll only receive 40 mark per person,

286
00:37:54,588 --> 00:37:56,052
and even after the currency reform*,

287
00:37:54,588 --> 00:37:56,052
{\a6}[*June 1948, after the division.
Ostmark were used in the DDR]

288
00:37:56,053 --> 00:37:58,604
you will be able to exchange all your
Reichsmark into Deutsche Mark.

289
00:37:58,605 --> 00:38:03,439
They're making beggars out of all
of us with this exchange rate!

290
00:38:03,440 --> 00:38:06,384
<i>This is no money,
it's just a piece of paper!</i>

291
00:38:06,385 --> 00:38:09,646
The Soviet Union blocked Berlin,
so if a war breaks out,

292
00:38:09,647 --> 00:38:11,394
what will all these pieces of paper count?

293
00:38:11,514 --> 00:38:13,058
Let's just burn them all!

294
00:38:22,982 --> 00:38:28,742
The Reichsmarks I gave you
aren't worth a penny anymore.

295
00:38:28,848 --> 00:38:32,578
So you came here to pay
me again with the new currency?

296
00:38:32,683 --> 00:38:40,629
You recreated my precious hometown,
it's not something money can buy.

297
00:38:43,651 --> 00:38:45,818
I'd really like to visit
this hometown of yours,

298
00:38:45,943 --> 00:38:49,282
if it produces
honest people such as you.

299
00:39:14,229 --> 00:39:17,207
I knew you'd come here.

300
00:39:22,884 --> 00:39:28,448
<i>When I crossed the Amnok,
going from east to west,</i>

301
00:39:28,615 --> 00:39:32,111
<i>I dreamed of finding a new world.</i>

302
00:39:32,112 --> 00:39:34,811
<i>But, when I escaped Japanese
tyranny and came here,</i>

303
00:39:35,000 --> 00:39:39,082
<i>what I found wasn't
the ideal place I dreamed of,</i>

304
00:39:39,145 --> 00:39:42,488
<i>but just another dictatorship.</i>

305
00:39:42,779 --> 00:39:45,896
Are you going to return home?

306
00:39:46,131 --> 00:39:49,302
<i>Return?</i>

307
00:39:50,206 --> 00:39:52,659
<i>I don't know.</i>

308
00:39:55,426 --> 00:39:58,256
<i>If I cross the Amnok once again,</i>

309
00:39:58,257 --> 00:40:00,671
<i>will my hometown be there?</i>

310
00:40:02,541 --> 00:40:08,833
<i>The place I was born in, that
land populating my dreams.</i>

311
00:40:09,032 --> 00:40:12,358
<i>That is now just a chimera
existing inside my novel,</i>

312
00:40:12,485 --> 00:40:16,225
<i>a place I will no longer
find wherever I go.</i>

313
00:40:17,424 --> 00:40:20,405
You wrote about the
hometown you were born in,

314
00:40:20,544 --> 00:40:23,913
so now you must write about
your newfound hometown.

315
00:40:25,814 --> 00:40:29,060
I'm talking about Germany.

316
00:40:35,891 --> 00:40:38,895
Your first book
was a tremendous success.

317
00:40:39,895 --> 00:40:41,158
Doctor Li,

318
00:40:41,367 --> 00:40:45,203
let's publish a sequel as soon as possible.

319
00:40:45,328 --> 00:40:49,613
I'd like to talk about that later.

320
00:40:49,740 --> 00:40:51,422
No problem.

321
00:40:51,550 --> 00:40:54,450
But don't make us wait too long.

322
00:41:00,254 --> 00:41:02,016
Is everything all right?

323
00:41:02,158 --> 00:41:04,763
Why have you waited so long to come?

324
00:41:04,871 --> 00:41:08,415
You must have realized yourself
something was not right.

325
00:41:09,647 --> 00:41:11,879
How serious is it?

326
00:41:13,575 --> 00:41:16,653
It's cancer,
in a critical stage.

327
00:41:16,690 --> 00:41:18,430
You must go through
surgery immediately.

328
00:41:18,438 --> 00:41:20,116
Immediately?

329
00:41:21,843 --> 00:41:23,808
Please, give me some time,

330
00:41:23,940 --> 00:41:31,704
so I can prepare myself for this.

331
00:43:40,225 --> 00:43:44,208
Good afternoon, Mrs. Seyler.
I brought you the corrected manuscript.

332
00:43:45,985 --> 00:43:50,573
Doctor Li has left.
You didn't know?

333
00:44:10,231 --> 00:44:15,036
{\a6}<i>Insaengdocheojihasa</i>

334
00:44:10,231 --> 00:44:15,036
<i>What could life resemble?</i>

335
00:44:15,190 --> 00:44:20,994
{\a6}<i>Eungsabihongdabseolli</i>

336
00:44:15,190 --> 00:44:20,994
<i>Like a wild geese's
footprints in the snow.</i>

337
00:44:21,137 --> 00:44:26,267
{\a6}<i>Isanguyeonyujijo</i>

338
00:44:21,137 --> 00:44:26,267
<i>Although only a caress
of fate leaves them behind,</i>

339
00:44:26,406 --> 00:44:29,688
{\a6}<i>Hongbinabugyedongseo</i>

340
00:44:26,406 --> 00:44:29,688
<i>As the wild geese
flies away, unknowing.</i>

341
00:45:01,357 --> 00:45:03,192
Seonsaengnim!

342
00:45:15,943 --> 00:45:20,580
Where there is life,
there is death.

343
00:45:21,936 --> 00:45:25,929
Where would you find someone
as happy as me?

344
00:45:26,053 --> 00:45:29,504
Someone with two hometowns.

345
00:45:29,630 --> 00:45:35,371
Who could receive
as much love as I did?

346
00:45:36,389 --> 00:45:38,908
Seonsaengnim...

347
00:46:16,454 --> 00:46:18,505
He says he's coming back, right?

348
00:46:18,656 --> 00:46:20,778
When will he be back?

349
00:46:21,544 --> 00:46:28,484
{\a6}Munhwegeumjikchicheosa

350
00:46:21,544 --> 00:46:28,484
<i>Your soul drenched my
heart in that sightly silk.</i>

351
00:46:28,646 --> 00:46:34,522
<i>{\a6}DanjeolEunjeongbulhakchi</i>

352
00:46:28,646 --> 00:46:34,522
<i>Love's extinction, never did I
learn such foolish words.</i>

353
00:46:35,103 --> 00:46:41,947
<i>{\a6}Unusaetanjongyubyeol</i>

354
00:46:35,103 --> 00:46:41,947
<i>I grieve my loss,
despite choosing to part.</i>

355
00:46:43,226 --> 00:46:49,560
<i>{\a6}Bunshinomunimsieui</i>

356
00:46:43,226 --> 00:46:49,560
<i>I had the doltish suspicion,
wondering whether you would be angry.</i>

357
00:46:51,058 --> 00:46:55,010
Ehh... always making me wait.
What is he saying?

358
00:46:55,178 --> 00:46:58,879
Is he coming back or not?
Uh?

359
00:47:01,019 --> 00:47:03,646
He probably...

360
00:47:04,645 --> 00:47:07,045
will not come back.

361
00:47:07,172 --> 00:47:08,514
What?

362
00:47:11,478 --> 00:47:15,905
He talked of the past,
trying to comfort me.

363
00:47:17,132 --> 00:47:25,282
There must be
something wrong with him.

364
00:47:50,189 --> 00:47:59,267
Now...
that river...

365
00:48:04,647 --> 00:48:06,717
What's wrong?

366
00:48:06,844 --> 00:48:10,169
I feel so anxious.

367
00:48:26,912 --> 00:48:28,661
<i>Mirok.</i>

368
00:48:28,783 --> 00:48:31,985
Why are you outside?
The wind is cold.

369
00:48:32,777 --> 00:48:35,055
It feels refreshing outside.

370
00:48:46,130 --> 00:48:47,720
Mirok.

371
00:48:50,717 --> 00:48:55,147
I am all right.
Don't cry, please.

372
00:48:59,078 --> 00:49:01,032
Mother...

373
00:49:05,692 --> 00:49:07,894
I want to go home.

374
00:49:07,895 --> 00:49:13,345
I want to spend my last days at home.

375
00:49:14,339 --> 00:49:17,850
Why do you say that, Mirok?

376
00:49:18,036 --> 00:49:21,667
You must be strong.

377
00:49:25,486 --> 00:49:27,751
Is there anything you wish to eat?

378
00:49:27,893 --> 00:49:34,292
I'd love to eat
Korean rice one last time.

379
00:50:11,150 --> 00:50:16,080
This clock didn't stop,

380
00:50:16,283 --> 00:50:20,450
but looks like my time is finally up.

381
00:50:20,822 --> 00:50:26,673
Just like your book Der Yalu Fliesst
will always populate our memories,

382
00:50:27,207 --> 00:50:30,869
you will always have a
place inside our hearts.

383
00:50:32,319 --> 00:50:36,767
Germany has become your new hometown,

384
00:50:36,878 --> 00:50:42,152
and Korea has become
the home of peace for us.

385
00:50:42,153 --> 00:50:47,670
My father gave me this clock.

386
00:50:55,598 --> 00:50:57,598
Mirok.

387
00:51:10,961 --> 00:51:13,894
Your rice is coming.

388
00:51:14,988 --> 00:51:17,949
Mother...

389
00:51:24,471 --> 00:51:26,861
Seonsaengnim.

390
00:51:27,513 --> 00:51:38,354
<i>Until the East Sea's waters and Mt. Baekdu...</i>

391
00:51:27,513 --> 00:51:38,354
{\a6}"Donghaemul-gwa Baekdusan-I"
[Korean Hymn, Aegukga]

392
00:51:39,478 --> 00:51:47,879
<i>{\a6}"Mareugo Daltorok"</i>

393
00:51:39,478 --> 00:51:47,879
"are dry and worn away"

394
00:51:47,880 --> 00:51:57,262
<i>{\a6}"Haneunim-I bouhasa"</i>

395
00:51:48,880 --> 00:51:57,262
"May God protect and preserve us"

396
00:51:57,388 --> 00:52:04,488
{\a6}<i>"UriNara Manse"</i>

397
00:51:57,388 --> 00:52:04,488
<i>"and bless our nation"</i>

398
00:52:04,625 --> 00:52:11,604
{\a6}<i>"Mugunghwa samcheolli"</i>

399
00:52:04,625 --> 00:52:11,604
<i>"3,000 Li of beautiful
rivers and valleys..."</i>

400
00:52:11,724 --> 00:52:17,218
{\a6}<i>"Hwaryeogangsan"</i>

401
00:52:11,724 --> 00:52:17,218
<i>"filled with
Roses of Sharon"</i>

402
00:52:18,279 --> 00:52:25,846
{\a6}<i>"Daehansaram Daehan-Euro"</i>

403
00:52:18,279 --> 00:52:25,846
<i>"May our great Korean people
live the Korean way"</i>

404
00:52:26,953 --> 00:52:33,569
{\a6}<i>"Gir-I Bojeonhase"</i>

405
00:52:26,953 --> 00:52:33,569
<i>"and protect our path"</i>

406
00:53:10,279 --> 00:53:16,203
<i>We are all here today to celebrate
his academic achievements,</i>

407
00:53:16,355 --> 00:53:22,554
his cultured ways, and his
endless love for teaching;

408
00:53:22,694 --> 00:53:28,835
<i>his genius as a writer,
and his wisdom as a philosopher.</i>

409
00:53:28,976 --> 00:53:32,000
But before all those
qualities which marked him,

410
00:53:32,001 --> 00:53:35,284
to celebrate the man, Mirok Li.

411
00:53:35,672 --> 00:53:41,128
<i>The people who had the pleasure
to meet this slim and subtle man,</i>

412
00:53:41,129 --> 00:53:47,181
<i>with his quiet and reserved demeanor,
must have felt the warm embrace...</i>

413
00:53:47,182 --> 00:53:51,455
<i>of his ways surrounding them.</i>

414
00:53:51,819 --> 00:53:57,177
Mirok Li gave everyone
the confidence to believe in one's means,

415
00:53:57,278 --> 00:54:02,283
and had the gift of giving us the courage
to act according to our ideals.

416
00:54:03,352 --> 00:54:08,235
By meeting this man and the way
he interacted with people,

417
00:54:08,236 --> 00:54:13,186
we discovered that the joy
of life can only prosper,

418
00:54:13,187 --> 00:54:18,673
when your life is based on honesty
and generosity for your fellow men.

419
00:54:18,937 --> 00:54:25,862
<i>Mirok Li, as a writer, a scholar
and a human being,</i>

420
00:54:25,863 --> 00:54:30,202
played a bridge of understanding
between East and West.

421
00:54:32,008 --> 00:54:38,187
<i>Through his life and works,
we discovered a hometown of peace...</i>

422
00:54:38,316 --> 00:54:43,178
<i>which transcended race or continents.</i>

423
00:54:43,219 --> 00:54:48,753
<i>That spirit connects and reconciles
every single thing on this earth,</i>

424
00:54:48,754 --> 00:54:54,616
<i>and it's the spirit involving
and leading all of our lives.</i>

425
00:54:55,584 --> 00:55:02,555
<i>He found this hometown not
for himself, but for all of us,</i>

426
00:55:04,011 --> 00:55:10,574
<i>and left us this wonderful gift.</i>

427
00:55:11,454 --> 00:55:15,272
<i>Let us stop shedding
any more tears today,</i>

428
00:55:15,273 --> 00:55:19,901
<i>for Mirok Li was
able to find his hometown.</i>

429
00:55:20,042 --> 00:55:27,257
<i>Let's all partake in all the
peace joining and surrounding him.</i>

430
00:55:28,531 --> 00:55:33,274
<i>Our crying would mean belittling
the immortal legacy he created...</i>

431
00:55:33,275 --> 00:55:36,833
<i>while living among us.</i>

432
00:55:37,279 --> 00:55:42,052
<i>So, just like his Der Yalu Fliesst
remains in our memories,</i>

433
00:55:42,198 --> 00:55:50,809
<i>his life will not end here, and will
continue flowing inside our hearts.</i>

434
00:55:57,387 --> 00:56:00,887
Why two letters at once?

435
00:56:13,854 --> 00:56:16,918
What is he writing, this time?

436
00:56:17,309 --> 00:56:21,387
It's a letter from the people
who gave him hospitality.

437
00:56:21,779 --> 00:56:24,517
She asks me to send her some rice.

438
00:56:24,518 --> 00:56:27,295
Says he wants to eat Korean rice.

439
00:56:27,465 --> 00:56:28,401
Ahh... really.

440
00:56:28,402 --> 00:56:33,510
If he came here himself,
he would be already tired of eating rice.

441
00:56:46,108 --> 00:56:47,557
This...

442
00:56:49,632 --> 00:56:53,034
Does this mean he's dead?

443
00:56:53,834 --> 00:56:55,904
<i>How can he?</i>

444
00:56:56,046 --> 00:56:58,339
<i>How can you die?</i>

445
00:56:59,356 --> 00:57:01,582
<i>You heartless man.</i>

446
00:57:01,722 --> 00:57:07,349
How could he die there, so
far away from his hometown?

447
00:57:08,692 --> 00:57:12,148
You left like in a dream...

448
00:57:14,832 --> 00:57:19,274
<i>and you go without a last word?</i>

449
00:57:57,484 --> 00:58:00,768
<i>Where are you, my dear?</i>

450
00:58:01,020 --> 00:58:04,596
<i>Are you crossing the river?</i>

451
00:58:05,709 --> 00:58:14,682
<i>Are you coming back to your hometown,
your home, by my side?</i>

452
00:58:33,259 --> 00:58:36,752
Where are you?

453
00:58:37,788 --> 00:58:41,120
Did you go to that place?

454
00:58:42,524 --> 00:58:47,125
<i>That place you dreamed of?</i>

455
00:58:49,251 --> 00:58:54,336
The place which only
existed in your works?

456
00:58:57,581 --> 00:59:03,739
Did you go back there?

457
00:59:17,300 --> 00:59:21,100
Main Translator & Timer: MisterX

458
00:59:21,101 --> 00:59:23,101
Timing QC: selvaspeedy

459
00:59:23,102 --> 00:59:26,102
Editor/QC: szhoang

460
00:59:26,103 --> 00:59:29,103
Coordinators: mily2, ay_link

461
00:59:29,906 --> 00:59:34,930
Produced by
Bayerisches Fernsehen and SBS

462
00:59:35,689 --> 00:59:39,248
Directed by Lee Jong Han

463
00:59:39,672 --> 00:59:43,636
Written by Lee Hye Seon

464
00:59:44,675 --> 00:59:48,561
Based Upon
Der Yalu Fliesst by Mirok Li

