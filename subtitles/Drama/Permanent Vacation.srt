1
00:00:01,000 --> 00:00:04,000
Downloaded From www.AllSubs.org

2
00:01:05,520 --> 00:01:11,320
English subtitles transcribed by
Father Christmas

3
00:04:32,287 --> 00:04:34,152
My name is
Aloysious Christopher Parker

4
00:04:34,728 --> 00:04:37,464
and, if I ever have a son,
he'll be Charles Christopher Parker.

5
00:04:38,424 --> 00:04:40,160
Just like Charlie Parker.

6
00:04:41,649 --> 00:04:43,289
The people I know just call me Allie,

7
00:04:44,137 --> 00:04:46,201
and this is my story  --  or part of it.

8
00:04:48,609 --> 00:04:50,505
I don't expect it to explain all that much,

9
00:04:51,878 --> 00:04:56,103
but what's a story anyway,
except one of those connect the dots drawings

10
00:04:55,522 --> 00:04:59,426
that in the end forms a picture of something.

11
00:05:00,291 --> 00:05:01,313
That's really all this is.

12
00:05:05,033 --> 00:05:06,441
That's how things work for me.
I go from this place, this person

13
00:05:08,369 --> 00:05:08,666
to that place or person.

14
00:05:09,785 --> 00:05:12,425
And, you know,
it doesn't really make that much difference.

15
00:05:15,705 --> 00:05:21,209
I've known all different kinds of people.
Hung out with them, lived with them,

16
00:05:22,921 --> 00:05:24,065
watched them act things out in their own little ways.

17
00:05:24,530 --> 00:05:25,105
And to me...

18
00:05:28,169 --> 00:05:30,881
To me, those people I've
known are like a series of rooms,

19
00:05:31,561 --> 00:05:34,154
just like all the places where I've spent time.

20
00:05:36,265 --> 00:05:38,537
You walk in for the first time
curious about this new room --

21
00:05:38,969 --> 00:05:28,000
the lamp, TV, whatever.

22
00:05:44,712 --> 00:05:46,976
And then, after a while,
the newness is gone,

23
00:05:47,585 --> 00:05:47,857
completely.

24
00:05:48,649 --> 00:05:50,716
And then there's this kind of dread,

25
00:05:51,329 --> 00:05:53,394
kind of creeping dread.

26
00:05:54,097 --> 00:05:56,513
You probably don't even
know what I'm talking about.

27
00:05:57,209 --> 00:05:57,473
But anyway

28
00:05:59,081 --> 00:06:02,418
I guess the point of all this is that
after a while, something tells you,

29
00:06:04,057 --> 00:06:04,345
some voice speaks to you,

30
00:06:06,105 --> 00:06:07,817
and that's it.  Time to split.

31
00:06:08,970 --> 00:06:09,226
Go someplace else.

32
00:06:11,194 --> 00:06:12,689
People are going to be basically the same.

33
00:06:13,449 --> 00:06:16,617
Maybe use some different kind of refrigerator or toilet

34
00:06:17,217 --> 00:06:17,449
or something.

35
00:06:18,609 --> 00:06:19,513
But this thing tells you,

36
00:06:21,033 --> 00:06:21,849
and you have to start to drift.

37
00:06:23,049 --> 00:06:25,489
You may not even want to go,
but things will inform you.

38
00:06:28,688 --> 00:06:31,441
So here I am now in a place where I don't
even understand their language.

39
00:06:33,234 --> 00:06:35,769
But, you know, strangers are still always just strangers.

40
00:06:37,233 --> 00:06:39,705
And the story, this part of
the story, well,

41
00:06:41,569 --> 00:06:42,281
it's how I got from there to here.

42
00:06:43,004 --> 00:06:45,427
Or maybe I should say from here to here.

43
00:07:32,461 --> 00:07:34,997
Where have you been?

44
00:07:37,437 --> 00:07:37,677
I haven't seen you since Thursday.

45
00:07:44,173 --> 00:07:44,397
Walking.

46
00:07:46,965 --> 00:07:47,669
Just walking around.

47
00:07:49,533 --> 00:07:50,853
I can't seem to sleep at night.

48
00:07:52,685 --> 00:07:53,085
Not in this city.

49
00:08:10,565 --> 00:08:11,197
Doesn't seem like you sleep at all.

50
00:08:12,351 --> 00:08:14,285
Well, I have my dreams while I'm awake.

51
00:11:40,449 --> 00:11:43,232
You know, sometimes I think
I should just live fast

52
00:11:43,737 --> 00:11:44,225
and die young.

53
00:11:48,313 --> 00:11:51,370
And go in a 3-piece white
suit like Charlie Parker.

54
00:11:52,993 --> 00:11:53,368
Not bad, huh?

55
00:13:30,921 --> 00:13:33,440
She has dropped a roll of
paper from her breast.

56
00:13:34,504 --> 00:13:35,720
A stranger picks it up,

57
00:13:36,192 --> 00:13:37,712
shuts himself in his room all night,

58
00:13:38,624 --> 00:13:39,968
and reads the manuscript,

59
00:13:42,201 --> 00:13:43,713
which contains the following:

60
00:13:46,393 --> 00:13:48,409
When she ventured out with her silk net

61
00:13:49,032 --> 00:13:50,552
on the end of a russ,

62
00:13:51,008 --> 00:13:53,025
chasing a wild, free hummingbird...

63
00:13:56,984 --> 00:13:59,792
send me one and I, in return,
will wreath a garland

64
00:14:00,224 --> 00:14:03,049
of violets, mint, and geraniums.

65
00:14:09,525 --> 00:14:11,300
I was not present at the event

66
00:14:12,028 --> 00:14:14,188
of which my daughter's
death was the result.

67
00:14:15,148 --> 00:14:15,660
If I had been,

68
00:14:16,540 --> 00:14:19,725
I would have defended that
angel at the cost of my blood.

69
00:14:20,501 --> 00:14:22,909
Maldoror was passing with his bulldog.

70
00:14:23,541 --> 00:14:26,541
He sees a young girl sleeping
in the shade of a plain tree.

71
00:14:28,213 --> 00:14:30,884
At first, he took her for a rogue.

72
00:14:33,980 --> 00:14:35,460
It is impossible to say which

73
00:14:35,884 --> 00:14:38,165
came first to his mind --
the sight of this young girl

74
00:14:38,997 --> 00:14:40,884
or the resolution which followed.

75
00:14:43,085 --> 00:14:44,877
He undresses rapidly like a man

76
00:14:45,357 --> 00:14:46,716
who knows what he is going to do.

77
00:14:52,188 --> 00:14:54,917
He opens the angular claws of the
steel hydra;

78
00:14:55,580 --> 00:14:57,924
and armed with a scalpel
of the same kind,

79
00:14:58,485 --> 00:15:02,277
seeing that the green of the grass
had not yet disappeared beneath

80
00:15:03,061 --> 00:15:05,500
all the blood which had been shed,

81
00:15:06,324 --> 00:15:08,860
he prepares, without planning, to dig
his knife

82
00:15:09,654 --> 00:15:11,796
courageously into the unfortunate child.

83
00:15:15,221 --> 00:15:16,701
Wide and hoe, he pulls out

84
00:15:17,589 --> 00:15:18,141
one after one...

85
00:15:21,206 --> 00:15:22,387
... corpses sleep again in the shade.

86
00:15:24,764 --> 00:15:27,420
Pig-snouted brutishness

87
00:15:27,959 --> 00:15:29,759
covered him with its protective wings

88
00:15:30,287 --> 00:15:31,975
and cast loving glances at him.

89
00:15:47,063 --> 00:15:48,343
I'm tired of this book. You can have it.

90
00:15:54,855 --> 00:15:55,239
I'm finished with it.

91
00:15:58,581 --> 00:15:59,605
I'm tired of being alone.

92
00:16:03,005 --> 00:16:04,245
Everyone is alone.

93
00:16:09,309 --> 00:16:10,581
That's why I just drift, you know.

94
00:16:12,953 --> 00:16:13,974
People think it's crazy.

95
00:16:19,237 --> 00:16:22,373
But it's better to think that you're not
alone when, you know, you're drifting,

96
00:16:22,941 --> 00:16:23,654
even though you are.

97
00:16:24,333 --> 00:16:27,175
Instead of just knowing
that you're alone all the time.

98
00:16:32,229 --> 00:16:33,494
Some people, you know, they

99
00:16:34,860 --> 00:16:37,517
they can distract themselves with ambitions

100
00:16:37,981 --> 00:16:41,237
and motivation to work, you know,

101
00:16:41,981 --> 00:16:43,989
but it's not for me.

102
00:16:54,519 --> 00:16:56,895
They think people like myself
are crazy.

103
00:16:57,863 --> 00:16:58,279
Everyone does.

104
00:16:59,847 --> 00:17:01,119
'cause of the way I live, you know.

105
00:17:05,767 --> 00:17:08,784
I don't know. I guess you can say it's
reckless.

106
00:17:11,216 --> 00:17:12,622
But it's the only way for me, you know.

107
00:17:16,335 --> 00:17:19,007
My mother was like those kind of
people,

108
00:17:19,543 --> 00:17:21,735
telling me it's bad to be like that.

109
00:17:23,327 --> 00:17:25,327
And she ended up crazy herself,
you know --

110
00:17:26,592 --> 00:17:28,958
after my father was gone.

111
00:17:35,087 --> 00:17:37,399
But I don't care anymore, you know.
I don't want to think about it.

112
00:17:40,359 --> 00:17:43,911
I know that when I get the feeling,
you know, the drift is going to take me.

113
00:19:26,352 --> 00:19:27,265
I have this feeling...

114
00:19:30,800 --> 00:19:33,984
inside my head that's been
haunting me for a long time.

115
00:19:43,424 --> 00:19:44,224
Up until now.

116
00:19:46,712 --> 00:19:47,864
Because I've decided to...

117
00:19:52,177 --> 00:19:54,929
go visit someone I haven't seen in a long time.

118
00:19:59,466 --> 00:20:01,496
I'm going to go back and see my mother.

119
00:20:06,785 --> 00:20:08,617
I haven't seen her in over a year.

120
00:20:11,409 --> 00:20:12,147
She's in an institution.

121
00:20:21,033 --> 00:20:23,713
But first I'm going to go back to
where I was born,

122
00:20:24,369 --> 00:20:26,313
the building that my mother
and father lived in.

123
00:20:28,961 --> 00:20:30,353
It was blown up during the war.

124
00:20:31,849 --> 00:20:33,505
I'm going to walk through the rubble there.

125
00:20:35,922 --> 00:20:38,017
And just look at it one more time.

126
00:20:41,057 --> 00:20:43,121
Look at how the building is all bombed out.

127
00:20:48,433 --> 00:20:50,233
Walk through the rubble

128
00:20:50,728 --> 00:20:52,584
of the building where I was born.

129
00:21:00,354 --> 00:21:01,929
What are you talking about?

130
00:21:02,953 --> 00:21:03,281
What war?

131
00:21:08,369 --> 00:21:10,465
The building was blown up during the war.

132
00:21:14,753 --> 00:21:15,385
Blown up by who?

133
00:21:20,017 --> 00:21:20,609
The Chinese.

134
00:25:01,602 --> 00:25:02,402
Incoming round!

135
00:25:03,340 --> 00:25:04,483
What's the matter?

136
00:25:06,036 --> 00:25:08,067
Hey, that's an enemy plane!
Get on the ground!

137
00:25:11,242 --> 00:25:12,194
Take it easy, man.

138
00:25:13,583 --> 00:25:15,643
They're not even planes;
they're choppers.

139
00:25:17,723 --> 00:25:18,995
The Cong doesn't have choppers.

140
00:25:22,411 --> 00:25:23,548
Come on, man.  Take it easy.

141
00:25:24,412 --> 00:25:24,755
Get up.

142
00:25:27,146 --> 00:25:28,699
You see? Those are American.

143
00:25:48,551 --> 00:25:49,782
You want a cigarette?
Yeah.

144
00:26:13,819 --> 00:26:14,827
Hey, you want to hear a funny story?

145
00:26:19,251 --> 00:26:22,147
The other day, I was walking over by, uh,

146
00:26:24,290 --> 00:26:25,954
where I used to live, you know,
a long time ago.

147
00:26:28,098 --> 00:26:32,635
And...right in front of my house
was this big car.

148
00:26:33,555 --> 00:26:34,155
Really shiny.

149
00:26:35,259 --> 00:26:38,290
You know, one of these old cars, two tone,

150
00:26:39,914 --> 00:26:42,026
and very shiny car, lots of chrome, you know...

151
00:26:45,010 --> 00:26:46,706
Really beautiful,
like a bubble car.

152
00:26:47,330 --> 00:26:49,075
Very big and round, you known, like that.

153
00:26:51,244 --> 00:26:54,883
It was the most beautiful car I ever saw.
And I wanted a car just like it.

154
00:26:56,291 --> 00:26:56,906
It was really bad.

155
00:26:58,866 --> 00:27:00,802
And then today, you know, I woke up,

156
00:27:01,610 --> 00:27:02,338
and I couldn't believe it.

157
00:27:02,722 --> 00:27:04,594
I walked out of my house,
where I live now,

158
00:27:05,684 --> 00:27:08,435
and the same car -- I saw it.
The same exact car.

159
00:27:09,787 --> 00:27:10,827
Wow, I really flipped, man.

160
00:27:11,940 --> 00:27:14,202
It lit up like a bomb, this car,
you know.

161
00:27:14,731 --> 00:27:15,274
It was so great.

162
00:27:17,234 --> 00:27:18,554
Really like a "Batmobile."

163
00:27:21,083 --> 00:27:23,754
Like a gangster car.  Just like that.

164
00:27:25,403 --> 00:27:25,827
It was bad.

165
00:27:29,987 --> 00:27:30,795
So what do you say to that?

166
00:27:33,234 --> 00:27:33,842
I live back there.

167
00:27:35,443 --> 00:27:38,298
Oh, yeah?  Humm.  Some place to live.

168
00:27:39,787 --> 00:27:40,818
Should get out of here, man.

169
00:27:41,347 --> 00:27:43,339
Should get away from here --
go someplace different.

170
00:27:48,426 --> 00:27:49,714
That's what I'm going to do.

171
00:27:58,630 --> 00:27:59,074
Take it easy.

172
00:28:03,739 --> 00:28:04,699
I'm going to get out of here, too.

173
00:28:25,072 --> 00:28:27,641
Come on now.  Don't be scared.
Let's go.  Come on.

174
00:28:33,961 --> 00:28:35,553
You can walk right.  Come on.

175
00:29:14,204 --> 00:29:15,973
Mrs. Parker's your mother?  Um-hum.

176
00:29:32,549 --> 00:29:33,645
This is the room.  Thank you.

177
00:29:50,044 --> 00:29:52,917
Mom, it's me,
Aloysious Christopher Parker.

178
00:30:08,469 --> 00:30:10,101
Mom, it's me.  Your son.

179
00:30:23,792 --> 00:30:25,937
Mom, it's me, Allie.
I'm here to see you.

180
00:30:31,274 --> 00:30:32,874
My son's here to see me.

181
00:30:37,594 --> 00:30:39,394
How are you?  Have you been alright?

182
00:30:41,630 --> 00:30:42,414
Do you feel alright here?

183
00:30:54,669 --> 00:30:56,854
I know who you are.
I know you are my son.

184
00:31:01,414 --> 00:31:04,325
I know because the thighs aren't yours.
And they don't belong to you.

185
00:31:05,877 --> 00:31:07,501
They were taken out
of your father's head.

186
00:31:21,317 --> 00:31:22,493
Uh, mom, you're crazy.

187
00:31:46,311 --> 00:31:47,702
I can hear the planes sometimes.

188
00:31:48,142 --> 00:31:50,438
I haven't heard them since the
war when we were bombed.

189
00:31:51,734 --> 00:31:52,782
Is there another war going on?

190
00:32:05,158 --> 00:32:07,094
Excuse me, ladies.
It's time for your medication.

191
00:32:08,438 --> 00:32:11,590
Would you mind leaving the room
for a few moments,
and then you can come right back.

192
00:32:14,527 --> 00:32:15,127
Yeah, sure.

193
00:32:21,463 --> 00:32:22,767
Is there another war?

194
00:34:41,543 --> 00:34:43,103
[Speaking in Spanish...]

195
00:35:36,460 --> 00:35:37,132
Are you alright?

196
00:35:54,292 --> 00:35:55,068
What are you singing?

197
00:36:03,564 --> 00:35:44,080
I want you to leave.

198
00:36:06,034 --> 00:36:06,330
What?

199
00:36:06,714 --> 00:36:07,050
Go!

200
00:36:08,537 --> 00:36:09,361
Get out of here.

201
00:36:10,401 --> 00:36:12,345
Why?  I just wanted to
know what you are saying.

202
00:36:14,282 --> 00:36:14,770
[Speaking Spanish]

203
00:36:18,202 --> 00:36:20,050
You got to... you got to be quiet.

204
00:36:25,151 --> 00:36:26,192
Get out!  Get out!

205
00:36:27,488 --> 00:36:28,185
All right.  All right.

206
00:39:33,195 --> 00:39:34,501
Hi, I'd like to buy some popcorn.

207
00:39:37,011 --> 00:39:38,011
Uh, 75 cents.

208
00:40:19,916 --> 00:40:21,075
Do you think I would like this movie?

209
00:40:23,563 --> 00:40:24,507
The Quinn Eskimo film?

210
00:40:25,891 --> 00:40:29,332
Um, well, the only parts that I remember
were, uh, the first part where,

211
00:40:30,020 --> 00:40:32,044
uh, he's eating -- they're eating --
maggots and

212
00:40:32,933 --> 00:40:36,580
the second part where the old lady tells
the pregnant daughter, uh,

213
00:40:37,027 --> 00:40:40,011
that the first-born, if it's a boy, um,

214
00:40:40,955 --> 00:40:43,569
they rub it with blubber for good luck.

215
00:40:44,268 --> 00:40:46,179
And, the second, if it's a girl, uh,

216
00:40:47,068 --> 00:40:49,147
They stuffed snow in it's mouth and kill it.

217
00:40:56,939 --> 00:40:57,443
Uh-huh.

218
00:41:31,899 --> 00:41:33,538
Hey, have you ever heard of the Doppler effect?

219
00:41:36,355 --> 00:41:37,059
The Doppler effect.

220
00:41:38,643 --> 00:41:39,852
Yeah, yeah, I've heard of it.

221
00:41:41,491 --> 00:41:42,307
Yeah, sure, it's, uh...

222
00:41:46,138 --> 00:41:48,906
I know it's... it's, it's when Saturn
changes, when it moves.

223
00:41:49,371 --> 00:41:52,042
Yeah, that's the Doppler effect.

224
00:41:53,722 --> 00:41:54,066
That's right.

225
00:41:55,979 --> 00:41:57,619
That's the name of my joke.

226
00:41:58,770 --> 00:41:59,330
The Doppler effect.

227
00:42:02,738 --> 00:42:03,306
The Doppler effect.

228
00:42:05,146 --> 00:42:06,162
You, see, back...

229
00:42:08,314 --> 00:42:10,018
Back in the 50s there was this guy

230
00:42:11,306 --> 00:42:11,786
who played a sax.

231
00:42:12,802 --> 00:42:14,643
He played a mean sax.  Oh, yeah!

232
00:42:15,970 --> 00:42:17,649
But he was out of his time.

233
00:42:19,434 --> 00:42:21,130
His sound was different, way out,

234
00:42:21,578 --> 00:42:23,394
too advanced for all those others.

235
00:42:24,483 --> 00:42:26,234
He couldn't get no work 'cause

236
00:42:26,890 --> 00:42:31,451
he didn't want to, he didn't want to
conform to the traditional style of playing.

237
00:42:32,795 --> 00:42:33,491
Times was rough for him.

238
00:42:34,578 --> 00:42:37,011
So his friends got together and they said:

239
00:42:37,402 --> 00:42:40,434
"Look, Ned, you ought to go on over to Europe...

240
00:42:41,082 --> 00:42:42,890
...see, over there they play a different sound."

241
00:42:43,546 --> 00:42:45,419
"You know, they... you fit in.
You'll be able to play."

242
00:42:46,762 --> 00:42:48,898
And they, uh,  got together some money

243
00:42:49,459 --> 00:42:50,667
and bought a ticket to Paris.

244
00:42:51,970 --> 00:42:53,970
So he goes over there, and when he gets over there

245
00:42:55,018 --> 00:42:56,138
-- huh, it's the same old thing.

246
00:42:56,738 --> 00:42:58,017
His sound is too advanced for them.

247
00:42:58,681 --> 00:43:00,010
Nobody know where he coming from.

248
00:43:01,210 --> 00:43:03,050
And he couldn't get no job playing

249
00:43:03,461 --> 00:43:05,030
'cause he...he still doesn't want to

250
00:43:05,030 --> 00:43:08,461
conform to the traditional style of playing at the time.

251
00:43:09,365 --> 00:43:12,541
So he gets real depressed, gets upset.

252
00:43:13,205 --> 00:43:14,637
And he's broke.  He ain't got no money.

253
00:43:15,149 --> 00:43:17,013
So he's got to go out in the street to start playing.

254
00:43:18,313 --> 00:43:21,045
He plays all day, and he gets into junk.

255
00:43:22,110 --> 00:43:22,894
All kinds of stuff.

256
00:43:23,989 --> 00:43:26,310
And it's just too much for him.  He's not making it.

257
00:43:27,109 --> 00:43:27,965
So one day he decides

258
00:43:28,525 --> 00:43:31,389
to climb up some stairs and go up on a roof

259
00:43:31,918 --> 00:43:33,118
and jump off. End it all.

260
00:43:37,877 --> 00:43:40,341
He up there, and it's a... it's a grey day.

261
00:43:41,077 --> 00:43:43,341
Everything is grey.  The sky is all grey.

262
00:43:44,517 --> 00:43:45,917
And, uh, looking down,

263
00:43:47,229 --> 00:43:48,517
then he looked up.  And,

264
00:43:48,973 --> 00:43:50,229
just as he was about ready to jump,

265
00:43:51,230 --> 00:43:52,118
skies open up,

266
00:43:53,213 --> 00:43:56,117
and there's this ray of light

267
00:43:57,901 --> 00:43:58,205
hit him

268
00:43:59,125 --> 00:43:59,781
like a spotlight.

269
00:44:03,829 --> 00:44:04,613
For no reason at all, he just...

270
00:44:05,285 --> 00:44:07,790
he picked up his horn,

271
00:44:08,238 --> 00:44:10,142
and started playing "Somewhere Over the Rainbow."

272
00:44:29,431 --> 00:44:30,455
And that's as far as he got.

273
00:44:31,815 --> 00:44:32,863
He couldn't remember no more.

274
00:44:33,199 --> 00:44:34,711
He just kept on playing that same

275
00:44:35,303 --> 00:44:36,191
song over and over and over again...

276
00:44:37,048 --> 00:44:38,200
He couldn't remember no further.

277
00:44:40,159 --> 00:44:40,543
Just couldn't.

278
00:44:41,049 --> 00:44:43,311
Then, all of a sudden, people... people all around

279
00:44:43,825 --> 00:44:45,759
gathered around, and started
looking up at him.

280
00:44:46,159 --> 00:44:47,711
And the... And the police
come up to the roof,

281
00:44:48,431 --> 00:44:49,719
sneaking up on him from the back.

282
00:44:50,445 --> 00:44:52,280
And he's still trying to remember the next phrase.

283
00:44:54,247 --> 00:44:57,655
He turn around and look and see the police coming and he

284
00:44:58,039 --> 00:44:58,447
jumps down!

285
00:45:00,240 --> 00:45:00,991
And there he is down there.

286
00:45:01,471 --> 00:45:02,591
He ain't dead from the fall.

287
00:45:04,407 --> 00:45:05,015
And then he was conscious.

288
00:45:05,383 --> 00:45:08,511
He heard in the distance the sound of an ambulance coming.

289
00:45:09,951 --> 00:45:10,360
And, uh...

290
00:45:11,031 --> 00:45:11,520
he was going...

291
00:45:29,827 --> 00:45:30,587
The Doppler effect!

292
00:46:13,587 --> 00:46:14,182
What do you want to hear, kid?

293
00:46:15,809 --> 00:46:18,709
I don't care as long as it's vibrating

294
00:46:19,215 --> 00:46:20,421
and dug downtown.

295
00:46:22,710 --> 00:46:23,534
Man, what a sax!

296
00:50:31,200 --> 00:50:34,640
This gun is my legislative gun!

297
00:51:09,601 --> 00:51:10,424
Now I get it.

298
00:51:11,064 --> 00:51:15,200
It's "This gun is my legislative branch."

299
00:51:16,225 --> 00:51:17,417
Now, that's good.  That's good.

300
00:53:33,308 --> 00:53:36,688
Hey, could you put this in the box for me?

301
00:53:41,381 --> 00:53:42,237
Come on.  Give me a break.

302
00:53:45,709 --> 00:53:46,439
What do I look like?  A mailman?

303
00:54:08,036 --> 00:54:09,308
Fucking Christ!

304
00:54:11,168 --> 00:54:14,063
I don't believe it.  That dude just stole my car!

305
00:54:15,295 --> 00:54:15,728
Did you see him?

306
00:54:17,688 --> 00:54:18,192
That's my car!

307
00:54:19,743 --> 00:54:22,152
That's my fucking car!  Oh my God!

308
00:54:24,240 --> 00:54:26,984
That dude was wild style!  Oh, no, man!

309
00:54:27,528 --> 00:54:30,159
You better get your ass out of here
before he snags that up, too!

310
00:55:21,638 --> 00:55:22,117
Is this the car?

311
00:55:22,878 --> 00:55:23,886
Yup.  This's it.

312
00:55:28,517 --> 00:55:28,942
What's your name again?

313
00:55:30,221 --> 00:55:31,702
Allie.  Aloysious.

314
00:55:31,737 --> 00:55:15,960
Man, what do you want?

315
00:56:30,342 --> 00:56:31,187
Well?

316
00:56:34,284 --> 00:56:35,435
I'll give you 800 dollars for it.

317
00:56:41,923 --> 00:56:42,339
800?

318
00:56:45,131 --> 00:56:48,068
Ah, it's worth more than that.  I could get more than that for it.

319
00:56:48,587 --> 00:56:49,163
Go ahead.

320
00:56:54,435 --> 00:56:55,363
800 is all you can give me?

321
00:56:59,755 --> 00:57:00,507
I need the money.  Okay.

322
00:57:21,600 --> 00:57:21,951
Okay.

323
01:03:23,864 --> 01:03:24,624
Going on the boat, too?

324
01:03:25,736 --> 01:03:26,216
No, I just got here.

325
01:03:28,216 --> 01:03:28,632
Where you from?

326
01:03:29,623 --> 01:03:30,263
From France, from Paris.

327
01:03:32,824 --> 01:03:33,199
You born there?

328
01:03:34,528 --> 01:03:36,288
Yeah, I was born there.   Were you born in New York?

329
01:03:38,304 --> 01:03:39,160
Yeah, but I'm leaving.

330
01:03:40,576 --> 01:03:41,360
Yeah, I had to leave, too.

331
01:03:42,736 --> 01:03:43,048
How come?

332
01:03:44,392 --> 01:03:47,047
See, I had a lot of trouble, so I have to get out of Paris.

333
01:03:48,847 --> 01:03:49,856
And now my friends are crying.

334
01:03:51,703 --> 01:03:52,383
See, I never cry.

335
01:03:53,912 --> 01:03:56,128
'Cause I know when things change, I have to go somewhere.

336
01:03:57,888 --> 01:04:00,384
And now I think that New York is going to be Babylon for me.

337
01:04:02,176 --> 01:04:03,040
Well, that's where I've got to live.

338
01:04:07,519 --> 01:04:08,800
Think I would like it in Paris?

339
01:04:10,456 --> 01:04:11,784
Yeah, Paris wouldn't be a Babylon.

340
01:04:12,897 --> 01:04:13,256
Yeah.

341
01:04:21,089 --> 01:04:22,559
I just got a tattoo the other day.

342
01:04:23,871 --> 01:04:24,231
Oh, yeah?

343
01:04:24,959 --> 01:04:27,303
I kind of wanted to get one before I went away.

344
01:04:28,192 --> 01:04:29,744
I got a tattoo here last year.

345
01:04:30,848 --> 01:04:31,224
What's it say?

346
01:04:32,160 --> 01:04:32,560
It means "mommy."

347
01:04:36,081 --> 01:04:38,000
Oh, you have a diamond shape?
Very good.

348
01:04:47,768 --> 01:04:49,064
I got to go.   It's my boat.

349
01:04:49,960 --> 01:04:50,504
I got to go, too.

350
01:05:51,933 --> 01:05:54,788
I was thinking about the note I
left her when I got on the boat.

351
01:05:56,245 --> 01:05:58,597
But how can you explain something
like this to someone?

352
01:06:00,253 --> 01:06:02,957
I'm just not the kind of person that
settles into anything.

353
01:06:04,437 --> 01:06:05,437
I don't think I ever will be.

354
01:06:06,957 --> 01:06:09,652
Isn't really anything left to
explain that can be.

355
01:06:10,244 --> 01:06:12,445
And that's what I was trying to
explain in the first place.

356
01:06:14,036 --> 01:06:14,750
Just not like that.

357
01:06:15,621 --> 01:06:17,861
I don't want a job, or a house, or taxes

358
01:06:18,645 --> 01:06:20,973
although I wouldn't mind a car, but... I don't know.

359
01:06:21,749 --> 01:06:24,677
Now that I'm away, I wish I was back there

360
01:06:25,069 --> 01:06:26,509
more than even when I was there.

361
01:06:27,644 --> 01:06:30,445
Let's just say I'm a certain kind of tourist...

362
01:06:33,141 --> 01:06:36,373
A tourist that's on a... permanent vacation.

363
01:06:36,455 --> 01:06:06,400
Subtitles re-edit by riotgrrll

