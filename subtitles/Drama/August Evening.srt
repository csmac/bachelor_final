1
00:03:27,473 --> 00:03:29,941
You need a new flea collar.

2
00:03:31,511 --> 00:03:32,876
How was work?

3
00:03:33,513 --> 00:03:35,037
Boss wants the rent.

4
00:03:35,381 --> 00:03:37,246
Dinner's almost ready.

5
00:03:53,566 --> 00:03:55,534
Stop eating, Jaime!

6
00:03:57,337 --> 00:04:00,067
Or else I'll feed your plate to my turtle.

7
00:05:21,187 --> 00:05:22,882
Put your index finger here...

8
00:06:46,339 --> 00:06:48,273
It's your birthday.

9
00:06:48,741 --> 00:06:50,208
Next week.

10
00:07:59,378 --> 00:08:02,279
Sorry, love. I get dizzy too easily.

11
00:08:04,083 --> 00:08:05,778
Come dance with me.

12
00:08:07,720 --> 00:08:09,711
Your dad won't mind.

13
00:08:10,690 --> 00:08:12,658
He's not my dad.

14
00:08:13,459 --> 00:08:17,862
Thanks, but we're
about to leave, so...

15
00:08:19,565 --> 00:08:22,864
Okay then, y'all have a good night.

16
00:08:27,406 --> 00:08:33,811
What's wrong with you, that boy
was fine. We're just sitting here.

17
00:08:36,415 --> 00:08:38,317
You have some other boy in mind?

18
00:08:38,317 --> 00:08:39,511
Of course not.

19
00:08:40,520 --> 00:08:42,545
I want you to get out there.

20
00:08:45,725 --> 00:08:49,525
It's almost four years
since we lost Manuel, honey.

21
00:08:51,797 --> 00:08:55,358
It's time you moved on.

22
00:08:55,902 --> 00:08:57,961
I don't want to talk about it.

23
00:09:00,706 --> 00:09:07,111
Anyway I'd rather stay home Saturday nights
and watch "Sabado Gigante" with Jaime.

24
00:09:07,580 --> 00:09:08,681
Don't bother the girl.

25
00:09:08,681 --> 00:09:12,742
No, I think we know some
good candidates for her.

26
00:09:14,086 --> 00:09:15,451
Candidates?

27
00:09:16,689 --> 00:09:19,852
Just let me know the day
before the wedding and I'll show up.

28
00:09:24,697 --> 00:09:29,657
How bitter she is. You know my sister, Juana.
How bitter she is.

29
00:09:31,237 --> 00:09:36,573
Women shouldn't
grow old by themselves.

30
00:09:39,278 --> 00:09:41,610
I won't let that happen to you.

31
00:09:44,050 --> 00:09:45,540
I'll move out soon.

32
00:09:47,820 --> 00:09:49,515
Don't be so quiet.

33
00:09:50,489 --> 00:09:53,356
You always get quiet when you're upset.

34
00:10:20,586 --> 00:10:22,451
Any better yet?

35
00:10:23,055 --> 00:10:24,682
I'm fine.

36
00:10:29,729 --> 00:10:31,597
I'll stay.

37
00:10:31,597 --> 00:10:35,124
No. Go, go. You'll be late.

38
00:10:39,605 --> 00:10:41,505
I'll take care of her.

39
00:10:44,377 --> 00:10:48,080
Get her out of the house tomorrow
so I can make her a birthday cake.

40
00:10:48,080 --> 00:10:50,344
What are you whispering about?

41
00:10:50,916 --> 00:10:53,680
Lupe's saying how bad you stink.

42
00:11:08,801 --> 00:11:11,668
Dad, are you okay?

43
00:11:12,505 --> 00:11:14,974
Jason's sorry he couldn't be here.

44
00:11:14,974 --> 00:11:18,808
He's in Tampa again for work and
there's no way he would make it in time.

45
00:11:21,580 --> 00:11:25,918
Dad, what are you doing?
I would have paid for the funeral home.

46
00:11:25,918 --> 00:11:28,284
This isn't right. It's disrespectful.

47
00:11:29,922 --> 00:11:33,585
I didn't want to give her to strangers.

48
00:11:34,260 --> 00:11:36,285
Mom deserves better.

49
00:11:37,296 --> 00:11:41,790
Where's Victor?
He lives closer, he should be here by now.

50
00:11:58,784 --> 00:12:01,651
I can't believe neither of
our families could come.

51
00:12:03,222 --> 00:12:05,417
I'm just glad you're here.

52
00:12:07,626 --> 00:12:11,153
It must have been three years
since we've all been together.

53
00:12:12,064 --> 00:12:14,692
It hasn't been that long.

54
00:12:18,938 --> 00:12:22,465
How's work and your new house?

55
00:12:24,777 --> 00:12:27,268
Good. You know...

56
00:12:28,714 --> 00:12:31,205
Your mother was proud of you.

57
00:12:32,118 --> 00:12:32,914
Dad.

58
00:12:40,059 --> 00:12:41,492
Hey everyone.

59
00:12:44,830 --> 00:12:46,132
Juana, thank you so much.

60
00:12:46,132 --> 00:12:47,933
Well, somebody's gotta
take care of you two now.

61
00:12:47,933 --> 00:12:49,264
Where's my sister?

62
00:12:54,006 --> 00:12:55,701
She looks peaceful.

63
00:13:19,932 --> 00:13:21,490
I would have gotten that for you.

64
00:13:28,274 --> 00:13:29,935
How's my dad doing?

65
00:13:31,310 --> 00:13:32,868
It's hard to say.

66
00:13:38,918 --> 00:13:40,647
What'd she bring?

67
00:13:41,320 --> 00:13:44,448
If it's as good as it smells,
we'll all end up in the hospital.

68
00:13:44,957 --> 00:13:46,826
It's the thought that counts.

69
00:13:46,826 --> 00:13:48,760
I don't even think my mom liked her.

70
00:13:55,067 --> 00:13:57,331
I remember when she used to wear this.

71
00:14:00,739 --> 00:14:05,699
She never even wore this one.
Wasteful. I'll wear it.

72
00:14:06,045 --> 00:14:10,072
I couldn't really use any of
this stuff. It's sort-of late 70's.

73
00:14:10,549 --> 00:14:11,650
Would you like something to drink?

74
00:14:11,650 --> 00:14:12,480
No, thanks.

75
00:14:13,953 --> 00:14:16,683
I guess this would
look nice with something blue.

76
00:14:18,724 --> 00:14:22,660
Did someone already go
through this? I thought she had more.

77
00:15:33,332 --> 00:15:34,856
What are you doing now at BanTek?

78
00:15:36,969 --> 00:15:38,766
You know what LAN networks are?

79
00:15:40,973 --> 00:15:44,243
I set those up for
schools and small businesses.

80
00:15:44,243 --> 00:15:45,574
Is it busy?

81
00:15:48,614 --> 00:15:51,082
Yeah, things are good. It's okay.

82
00:15:51,717 --> 00:15:55,813
I'm pretty hectic, too.
It's that time of year.

83
00:15:56,689 --> 00:15:59,021
We should probably
get back soon, right?

84
00:16:00,225 --> 00:16:02,716
Maybe we'll beat traffic.

85
00:16:54,179 --> 00:16:55,510
There you are.

86
00:17:08,961 --> 00:17:11,521
I remember when
she gave you that hat.

87
00:17:13,432 --> 00:17:15,491
You didn't like it at first.

88
00:17:19,238 --> 00:17:21,729
This was Maria's
favorite time of day.

89
00:17:24,009 --> 00:17:25,203
It's beautiful,

90
00:17:26,745 --> 00:17:28,975
and I never noticed.

91
00:19:03,442 --> 00:19:04,431
What's up?

92
00:19:05,744 --> 00:19:06,802
Are you Jaime?

93
00:19:10,382 --> 00:19:13,442
Connie said I
should wait for you.

94
00:19:35,908 --> 00:19:38,502
It's cool that you
can retire at your age.

95
00:19:39,077 --> 00:19:40,772
My grandfather's still working.

96
00:19:45,884 --> 00:19:48,011
Pull this tight. Okay, you try it.

97
00:20:28,160 --> 00:20:31,926
I've changed my mind, about
what Maria was talking about.

98
00:20:37,336 --> 00:20:41,830
It's time you stopped living with
me and found a new husband.

99
00:20:46,845 --> 00:20:49,211
Where the hell did that come from?

100
00:20:50,082 --> 00:20:53,677
I'm serious.
It's what Maria wanted.

101
00:20:55,454 --> 00:20:59,049
I want you to remarry. Before it's too late.

102
00:21:03,061 --> 00:21:07,157
So you're going to remarry, too?

103
00:21:21,179 --> 00:21:23,807
It's late. You've got work tomorrow.

104
00:21:45,137 --> 00:21:46,832
Where's Connie?

105
00:21:47,706 --> 00:21:49,139
It's past seven.

106
00:21:54,446 --> 00:21:55,743
I have to walk.

107
00:22:00,252 --> 00:22:02,117
He's over in Shiner this week.

108
00:22:02,354 --> 00:22:03,582
You'll be late.

109
00:22:05,390 --> 00:22:07,153
Probably so.

110
00:23:16,795 --> 00:23:19,389
- How was work?
- Fine.

111
00:23:19,731 --> 00:23:20,959
Are you hungry?

112
00:24:30,735 --> 00:24:34,227
Then where have you been
going every day this week?

113
00:24:36,241 --> 00:24:38,300
Looking for other work.

114
00:24:39,411 --> 00:24:40,935
But there's nothing.

115
00:24:42,214 --> 00:24:46,412
And apartments want
the first and last month

116
00:24:46,952 --> 00:24:48,647
before we move in.

117
00:24:49,888 --> 00:24:52,118
We'll find a way to stay here.

118
00:24:53,158 --> 00:24:54,921
I don't think so.

119
00:24:56,795 --> 00:24:58,319
What about Juana?

120
00:25:05,103 --> 00:25:06,866
I'll go to San Antonio.

121
00:25:07,672 --> 00:25:09,333
You'll stay with Eva.

122
00:25:10,141 --> 00:25:11,042
No,

123
00:25:11,042 --> 00:25:12,644
we'll stick together.

124
00:25:12,644 --> 00:25:14,339
Vl'll find a way.

125
00:25:29,794 --> 00:25:32,058
I've never seen
this photo of Manuel.

126
00:25:34,933 --> 00:25:36,992
My son always wore that same shirt.

127
00:26:33,224 --> 00:26:35,021
It's been a long time.

128
00:26:37,696 --> 00:26:41,826
It'll be good to see the
life Victor's built for himself.

129
00:26:43,635 --> 00:26:45,068
A new house.

130
00:27:15,266 --> 00:27:17,166
Are we still in San Antonio?

131
00:27:31,049 --> 00:27:32,539
Let me get that.

132
00:27:33,518 --> 00:27:35,076
Thank you so much, Andrea.

133
00:27:36,521 --> 00:27:38,989
You two can stay
in the boys' room.

134
00:27:42,594 --> 00:27:44,858
Sorry it's not much.

135
00:27:49,534 --> 00:27:52,103
We'd planned on
a three bedroom, but

136
00:27:52,103 --> 00:27:54,094
it's difficult
with only one income.

137
00:27:56,408 --> 00:27:59,044
I didn't want to talk
about it at the funeral,

138
00:27:59,044 --> 00:28:01,706
but BanTek's
had some cutbacks.

139
00:28:03,281 --> 00:28:05,442
They'll try to hire
me back in the Spring.

140
00:28:06,818 --> 00:28:08,410
What are you doing now?

141
00:28:11,289 --> 00:28:13,280
He fixes air-conditioners.

142
00:28:13,958 --> 00:28:15,323
It's not just that.

143
00:28:16,428 --> 00:28:17,952
Part-time.

144
00:28:22,434 --> 00:28:24,902
We can move this stuff over.

145
00:28:28,573 --> 00:28:30,438
Gabe's so cute.

146
00:28:31,309 --> 00:28:33,504
Cute's one way to look at it.

147
00:28:38,616 --> 00:28:40,174
Who's Matthew?

148
00:28:41,720 --> 00:28:44,522
Stop running and get
in here. One! Two!

149
00:28:44,522 --> 00:28:46,224
Matthew's our youngest.

150
00:28:46,224 --> 00:28:47,418
Including the cat.

151
00:28:54,199 --> 00:28:56,360
Gabe, do you remember your granddad?

152
00:29:02,040 --> 00:29:04,508
Hey, let's go out to dinner okay?

153
00:29:05,443 --> 00:29:07,377
What about Tomatillo's?

154
00:29:20,692 --> 00:29:22,387
I'll pay you back by Tuesday.

155
00:29:47,585 --> 00:29:50,110
I meant to bring
Matthew down to you and Mom,

156
00:29:50,755 --> 00:29:52,154
but...

157
00:29:53,792 --> 00:29:55,760
And this job thing is just temporary.

158
00:29:55,760 --> 00:29:57,193
Don't make excuses.

159
00:29:59,130 --> 00:30:00,757
Listen, Victor...

160
00:30:02,367 --> 00:30:03,527
Dad...

161
00:30:07,839 --> 00:30:09,874
We're gonna be late Victor!

162
00:30:09,874 --> 00:30:11,171
Come on. Hook this.

163
00:30:13,411 --> 00:30:15,313
Hey, I just invited Luis.

164
00:30:15,313 --> 00:30:18,416
You'll like him, he's your age.
He lives a few doors down.

165
00:30:18,416 --> 00:30:21,579
She needs to
meet more young men.

166
00:30:22,654 --> 00:30:25,356
Let us know if you like him and we'll
invite him back to the house afterward.

167
00:30:25,356 --> 00:30:27,187
Just make a signal or something.

168
00:30:27,592 --> 00:30:28,923
Don't start.

169
00:30:29,494 --> 00:30:32,224
If you think he's cute, just
say you want more jalapenos.

170
00:30:33,064 --> 00:30:33,792
Yeah.

171
00:30:34,666 --> 00:30:35,928
I'm not going.

172
00:30:38,203 --> 00:30:40,330
You can't be too picky, honey.

173
00:30:40,839 --> 00:30:44,570
You aren't exactly a
spring chicken yourself.

174
00:30:47,712 --> 00:30:49,314
The sitter's here, let's go.

175
00:30:49,314 --> 00:30:51,282
Victor, go put something decent on.

176
00:30:51,282 --> 00:30:52,617
Hurry hurry.

177
00:30:52,617 --> 00:30:54,244
Move it Victor!

178
00:31:22,547 --> 00:31:25,277
You can use a
fork for that if you want.

179
00:31:30,154 --> 00:31:32,588
Have you become that crass too?

180
00:31:33,258 --> 00:31:34,657
Probably.

181
00:31:38,696 --> 00:31:41,062
It looks like finger
food to me, anyway.

182
00:31:45,970 --> 00:31:49,462
Luis, did you know Lupe's from the
same part of Oaxaca as you?

183
00:31:51,242 --> 00:31:52,903
El Coyul.

184
00:31:55,613 --> 00:31:57,949
I used to go
there with my cousins.

185
00:31:57,949 --> 00:31:59,439
It's beautiful.

186
00:31:59,751 --> 00:32:01,844
You know Rio Seco?

187
00:32:05,490 --> 00:32:07,958
I wish I could go
back to visit, in Spring.

188
00:32:08,326 --> 00:32:10,061
There's so much space

189
00:32:10,061 --> 00:32:11,392
not like here.

190
00:32:12,030 --> 00:32:13,861
How would you rate this pork?

191
00:32:15,667 --> 00:32:16,793
It's good,

192
00:32:17,902 --> 00:32:19,529
it's pretty good.

193
00:32:22,106 --> 00:32:23,835
I'm a butcher.

194
00:32:24,976 --> 00:32:26,273
I work at Reyes'.

195
00:32:26,978 --> 00:32:29,947
I used to work at Randall's,
but it's too corporate.

196
00:32:31,349 --> 00:32:34,419
They don't care if you
know what you're doing,

197
00:32:34,419 --> 00:32:37,522
or if you take the time to
learn what the customers want.

198
00:32:37,522 --> 00:32:38,682
I can't handle that.

199
00:32:39,724 --> 00:32:41,089
So I quit.

200
00:32:45,163 --> 00:32:48,155
So you're sort of
a rebel butcher then.

201
00:32:49,600 --> 00:32:51,192
Well...

202
00:32:57,842 --> 00:32:58,910
Lupe,

203
00:32:58,910 --> 00:33:01,401
you want some
more jalapenos?

204
00:33:03,548 --> 00:33:05,482
No thanks.

205
00:33:07,285 --> 00:33:10,846
But it tastes better
with jalapenos. Come'on.

206
00:33:13,358 --> 00:33:15,690
Yeah, they're good.

207
00:33:17,228 --> 00:33:19,423
Sounds like you guys
want more for yourselves.

208
00:33:20,465 --> 00:33:22,592
Here, you can share these.

209
00:33:54,932 --> 00:33:59,130
That's funny you guys
are from the same place.

210
00:34:00,671 --> 00:34:03,196
It's really not that close.

211
00:34:03,474 --> 00:34:08,343
Well, I'll have to bring you some
of the best cuts of meat sometime.

212
00:34:12,050 --> 00:34:14,314
For the whole family.

213
00:34:35,006 --> 00:34:37,372
I think we can all squeeze in.

214
00:34:38,242 --> 00:34:42,144
I'm claustrophobic.
Can you catch the next one?

215
00:35:21,619 --> 00:35:23,187
Hey, don't crush him.

216
00:35:23,187 --> 00:35:24,017
Sorry.

217
00:35:34,365 --> 00:35:38,961
I like your father and Lupe, but they
just hopped on a bus without thinking.

218
00:35:41,405 --> 00:35:43,703
He's going to find a job.

219
00:35:45,843 --> 00:35:48,175
But how long is that going to take?

220
00:35:51,749 --> 00:35:55,583
And what the hell are you
doing spending $150 on dinner?

221
00:37:37,054 --> 00:37:39,557
Can you spare some change, man?

222
00:37:39,557 --> 00:37:40,925
Your dog come in?

223
00:37:40,925 --> 00:37:44,228
Let me show you how to bet.
Come on, we'll split the winnings.

224
00:37:44,228 --> 00:37:45,496
Sorry, man.

225
00:37:45,496 --> 00:37:47,231
Man, I should kick your ass.

226
00:37:47,231 --> 00:37:49,131
Hey asshole, I just-

227
00:37:51,469 --> 00:37:55,462
Jaime? Jaime! Look at this bastard.

228
00:38:00,444 --> 00:38:01,911
Salazar?

229
00:38:08,786 --> 00:38:10,955
What are you doing here?

230
00:38:10,955 --> 00:38:14,015
I thought you
went back to Tamaulipas!

231
00:38:15,359 --> 00:38:17,088
I didn't recognize you.

232
00:38:17,595 --> 00:38:18,863
You look good.

233
00:38:18,863 --> 00:38:20,956
No, I went down
but came back up in '92.

234
00:38:21,499 --> 00:38:23,296
Now I'm visiting my son.

235
00:38:24,235 --> 00:38:26,066
Man, that's great.

236
00:38:27,238 --> 00:38:30,696
My son won't spend thirty minutes with me.

237
00:38:35,413 --> 00:38:38,749
He and his wife still living with you?

238
00:38:38,749 --> 00:38:42,253
No, man.
I couldn't take that shit no more.

239
00:38:42,253 --> 00:38:44,118
I got my own place.

240
00:38:47,925 --> 00:38:49,927
You still see Camarillo?

241
00:38:49,927 --> 00:38:51,451
How is he?

242
00:38:56,133 --> 00:38:57,964
He's dead.

243
00:38:58,569 --> 00:39:00,867
Hung himself a few years back.

244
00:39:02,206 --> 00:39:05,232
He got old fast
after his wife died.

245
00:39:07,812 --> 00:39:09,280
Time flies.

246
00:39:09,280 --> 00:39:12,772
Yeah, time flies man. It really flies.

247
00:39:15,119 --> 00:39:17,383
So who do you like in the fifth?

248
00:39:21,325 --> 00:39:23,122
No, I have to get back.

249
00:39:25,029 --> 00:39:27,431
But we should get together soon.

250
00:39:27,431 --> 00:39:30,161
Yeah, have a few drinks.

251
00:39:30,668 --> 00:39:33,432
Okay, I'll see you around.

252
00:39:35,373 --> 00:39:37,603
It was good to see you.

253
00:40:26,524 --> 00:40:28,116
Good evening.

254
00:40:28,559 --> 00:40:31,926
I brought you a few
of the best cuts we had today.

255
00:40:32,730 --> 00:40:34,925
You didn't have to do that, really.

256
00:40:36,700 --> 00:40:41,069
Well, the real reason I came by was you'd
left your coin purse at the restaurant.

257
00:40:41,605 --> 00:40:43,505
I found it in the parking lot.

258
00:40:45,643 --> 00:40:47,406
That's not mine, Luis.

259
00:40:49,046 --> 00:40:50,513
Really?

260
00:41:01,192 --> 00:41:06,152
Well... would you like to take a walk,
or talk or something.

261
00:41:06,964 --> 00:41:10,334
Actually, Jaime's been sick in bed all day.

262
00:41:10,334 --> 00:41:14,668
I don't know if it's something he ate,
but I'm sort of taking care of him.

263
00:41:15,539 --> 00:41:17,063
Hey Luis! Hello!

264
00:41:19,109 --> 00:41:22,078
Jaime! Get back in bed.
What's wrong with you!

265
00:41:24,482 --> 00:41:27,417
He's never going to get
better if he doesn't rest.

266
00:41:27,985 --> 00:41:30,977
Oh, okay. I hope you
feel better, Mr. Esparza.

267
00:41:31,489 --> 00:41:33,389
I'm feeling better already.

268
00:41:35,059 --> 00:41:38,051
Okay, good night.

269
00:43:28,205 --> 00:43:30,274
I'll have two asadas, please.

270
00:43:30,274 --> 00:43:31,775
You want flour tortillas? Or...

271
00:43:31,775 --> 00:43:32,707
Flour.

272
00:43:37,047 --> 00:43:38,382
There a lot of onions?

273
00:43:38,382 --> 00:43:39,371
No.

274
00:43:44,121 --> 00:43:46,090
Which part of Mexico are you from?

275
00:43:46,090 --> 00:43:47,717
I'm from here, man.

276
00:43:57,635 --> 00:43:58,795
Look...

277
00:44:01,505 --> 00:44:05,271
I'm sorry for wasting your money.

278
00:44:08,812 --> 00:44:10,837
I know you won't say it,

279
00:44:12,383 --> 00:44:14,078
but I can feel it.

280
00:44:19,890 --> 00:44:21,118
Maybe.

281
00:44:24,294 --> 00:44:31,996
But why didn't you bring
your mother her own grandson.

282
00:44:34,905 --> 00:44:36,167
It's one thing

283
00:44:36,607 --> 00:44:38,802
if you didn't want to visit.

284
00:44:41,245 --> 00:44:43,008
But she never even knew he existed.

285
00:44:46,283 --> 00:44:48,979
That's not the way we raised you
to treat family.

286
00:44:52,122 --> 00:44:54,958
And if you lose one job, you can't just...

287
00:44:54,958 --> 00:44:56,687
I never gave up.

288
00:44:58,128 --> 00:45:00,596
It's just not that simple.

289
00:45:05,269 --> 00:45:07,032
But I'll try harder.

290
00:45:13,677 --> 00:45:17,081
You just need to try everything you can.

291
00:45:17,081 --> 00:45:19,481
Yeah, I know.

292
00:45:25,389 --> 00:45:30,122
Hey we shouldn't just sit around
the city while you're here.

293
00:45:31,595 --> 00:45:33,864
We should go
up to Lake Calaveras,

294
00:45:33,864 --> 00:45:36,355
just the two of us.

295
00:45:37,901 --> 00:45:39,994
I'd like that.

296
00:45:46,477 --> 00:45:49,002
But I've got that...

297
00:45:52,082 --> 00:45:54,175
We'll figure something out.

298
00:46:27,951 --> 00:46:30,020
Is dinner almost ready?

299
00:46:30,020 --> 00:46:31,044
Almost.

300
00:46:41,732 --> 00:46:44,462
You've been letting them
watch TV in Spanish?

301
00:46:52,576 --> 00:46:53,838
Where's Gabe?

302
00:46:54,244 --> 00:46:55,746
He's playing out front.

303
00:46:55,746 --> 00:46:57,509
No he's not, I just drove in.

304
00:46:59,082 --> 00:47:00,777
Have you seen Gabe?

305
00:47:01,218 --> 00:47:03,846
He's probably destroying
something out back.

306
00:47:04,488 --> 00:47:05,785
No he's not.

307
00:47:23,774 --> 00:47:25,674
Did he say he was going somewhere?

308
00:47:27,711 --> 00:47:30,077
He got angry
because he wanted money, but...

309
00:47:30,280 --> 00:47:32,145
You're supposed to be watching them.

310
00:47:32,482 --> 00:47:36,282
If you're going to stay here you
have to take some responsibility.

311
00:47:40,991 --> 00:47:42,356
Hi, Maria. How's it going?

312
00:47:43,026 --> 00:47:44,118
No, it's fine.

313
00:47:44,628 --> 00:47:46,425
Have you seen Gabe?

314
00:47:48,799 --> 00:47:52,326
No? Okay, thanks. No, it's fine. Bye.

315
00:47:54,171 --> 00:47:55,772
Well, I've got to go look for him.

316
00:47:55,772 --> 00:47:56,807
What's going on?

317
00:47:56,807 --> 00:47:58,240
Gabe's run away.

318
00:47:58,675 --> 00:48:00,110
He was here an hour ago.

319
00:48:00,110 --> 00:48:01,545
Well an hour is a long time.

320
00:48:01,545 --> 00:48:03,146
Let's not get worked up.

321
00:48:03,146 --> 00:48:06,775
I'll take the car. Jaime, do you think you
can watch Matthew, for just a little while?

322
00:48:11,922 --> 00:48:13,116
Sorry about that.

323
00:48:14,157 --> 00:48:15,681
Come on.

324
00:48:25,235 --> 00:48:27,863
You won't run away
when you grow up?

325
00:48:58,769 --> 00:49:02,728
I can't believe I let this happen.
Andrea seemed pretty upset.

326
00:49:03,840 --> 00:49:07,469
She's always like that. I'm sure he's fine.

327
00:49:24,428 --> 00:49:25,520
What's up?

328
00:49:26,630 --> 00:49:28,359
Hey, it's the guy you rejected.

329
00:49:29,433 --> 00:49:31,833
Andrea is convinced Gabe ran away.

330
00:49:32,636 --> 00:49:35,730
I'll help. It'll be faster on my bike.

331
00:49:36,039 --> 00:49:37,597
Sure, but it's no big deal.

332
00:49:38,008 --> 00:49:41,000
Actually, I'm not really sure how he looks.

333
00:49:43,714 --> 00:49:46,012
Then Lupe can go with you.

334
00:49:47,951 --> 00:49:49,453
Well, if...

335
00:49:49,453 --> 00:49:51,114
He's wearing a red shirt.

336
00:49:52,889 --> 00:49:55,585
Go on. This is your fault, remember.

337
00:50:05,102 --> 00:50:06,592
Hold on.

338
00:50:50,213 --> 00:50:54,051
He wanted money. I think he
said something about rides,

339
00:50:54,051 --> 00:50:56,611
but we didn't know
what he was talking about.

340
00:51:57,781 --> 00:52:00,484
Gabe's fine. He's back home.

341
00:52:00,484 --> 00:52:03,453
He was here but didn't
have any money so he went back.

342
00:52:05,922 --> 00:52:07,389
It was no trouble.

343
00:52:08,692 --> 00:52:10,421
I guess we can go home now.

344
00:52:46,429 --> 00:52:48,522
What do you miss most about El Coyul?

345
00:52:51,201 --> 00:52:53,999
Watermelons. Long summers.

346
00:52:55,105 --> 00:52:56,402
I remember.

347
00:52:59,276 --> 00:53:02,040
I'm surprised I never saw
you, it's such a small town.

348
00:53:03,346 --> 00:53:06,543
Maybe you did and we didn't know it.

349
00:53:08,451 --> 00:53:10,351
You ever think about going back?

350
00:53:11,021 --> 00:53:13,581
Things were difficult there, too.

351
00:53:15,825 --> 00:53:17,850
You used to live with
Jaime, and Victor's mother?

352
00:53:18,929 --> 00:53:19,793
Maria.

353
00:53:22,732 --> 00:53:24,757
My brother hates his mother-in-law.

354
00:53:25,502 --> 00:53:29,666
It wasn't like that. We were close.

355
00:53:31,942 --> 00:53:36,902
I haven't seen my own mother in
six years, and I never knew my dad.

356
00:53:41,618 --> 00:53:47,113
When I left Coyul, my mother held
a ceremony for me, like a kind of funeral,

357
00:53:50,727 --> 00:53:54,390
because she knew we'd probably
never see each other again.

358
00:53:57,901 --> 00:54:01,428
I wish I'd told Maria how
important she was to me...

359
00:54:05,141 --> 00:54:12,638
When I was fifteen, I got so angry at my
Mom I poured salt on all her rose bushes.

360
00:54:18,588 --> 00:54:20,283
She died later that summer.

361
00:54:23,593 --> 00:54:29,554
She used to work on those bushes for
hours, on her hands and knees in the dirt.

362
00:54:35,105 --> 00:54:36,367
It's late.

363
00:54:57,327 --> 00:54:59,227
You don't have to cook breakfast.

364
00:55:05,368 --> 00:55:06,870
I was going to make it.

365
00:55:06,870 --> 00:55:08,098
Don't bother.

366
00:55:14,878 --> 00:55:17,574
Are we going to work on
your math when you get home?

367
00:55:18,815 --> 00:55:21,113
I asked Maria to pick up the kids today.

368
00:55:27,557 --> 00:55:29,855
You don't need us to look after them?

369
00:55:31,361 --> 00:55:33,124
It's easier this way.

370
00:56:05,495 --> 00:56:07,360
They don't want us here.

371
00:56:12,769 --> 00:56:15,067
You shouldn't have to live like this anyway.

372
00:56:16,272 --> 00:56:20,106
It's time you moved out on your own.

373
00:56:22,412 --> 00:56:26,508
Just try to sleep. I'll figure something out.

374
00:56:47,303 --> 00:56:52,263
Sorry for the mess. I usually keep
things neat, but it was short notice.

375
00:56:52,776 --> 00:56:56,279
You can have the guest
bedroom, and Lupe can stay in

376
00:56:56,279 --> 00:56:58,338
what will hopefully be
the baby's room someday.

377
00:57:00,750 --> 00:57:01,918
Make yourself at home.

378
00:57:01,918 --> 00:57:03,681
I'll get us something to drink.

379
00:57:18,134 --> 00:57:19,294
That was too much!

380
00:57:19,602 --> 00:57:24,505
It was so crowded and that
house couldn't keep the bugs out.

381
00:57:25,775 --> 00:57:27,242
How many people were there?

382
00:57:28,011 --> 00:57:32,277
It was Jaime, Maria,
Victor, Alice, Me, and Manuel.

383
00:57:33,550 --> 00:57:37,884
So... what's this I hear about
you and Victor's neighbor?

384
00:57:39,456 --> 00:57:42,220
Probably from the biggest liar.

385
00:57:43,726 --> 00:57:47,218
I don't know this guy,
but you should get out there.

386
00:57:48,097 --> 00:57:49,496
You're not getting any younger.

387
00:57:49,933 --> 00:57:53,002
You have to grab the good
ones before they're all taken.

388
00:57:53,002 --> 00:57:53,900
It's nothing.

389
00:57:54,471 --> 00:57:57,235
Besides, everybody
gets divorced these days.

390
00:57:57,807 --> 00:58:00,571
So I can find someone on
their second time around.

391
00:58:01,077 --> 00:58:02,635
That's how Alice found me.

392
00:58:07,383 --> 00:58:09,352
Well she's lucky she did.

393
00:58:09,352 --> 00:58:11,411
And you're lucky I did, too.

394
00:58:12,889 --> 00:58:15,187
You used to complain
I was dating a white guy,

395
00:58:16,125 --> 00:58:19,219
but you're not complaining now
that you need a place to stay.

396
00:59:07,210 --> 00:59:09,201
No. There's no way.

397
00:59:09,946 --> 00:59:11,114
You'll like it.

398
00:59:11,114 --> 00:59:13,582
I don't gamble. It's just dirty.

399
00:59:27,630 --> 00:59:28,756
Go!

400
00:59:31,701 --> 00:59:33,635
We could've had a trifecta.

401
00:59:35,038 --> 00:59:37,268
You're not reading the backgrounds!

402
00:59:37,974 --> 00:59:40,408
Come'on, who do you like in the third?

403
00:59:40,910 --> 00:59:43,146
I don't actually gamble.

404
00:59:43,146 --> 00:59:45,444
Honestly, it's just... dirty.

405
00:59:51,287 --> 00:59:52,811
It's still dirty.

406
00:59:53,323 --> 00:59:55,587
I've only come here
maybe two or three times.

407
00:59:58,094 --> 00:59:59,561
Hey Luis.

408
01:00:01,064 --> 01:00:02,224
Hi Antonio.

409
01:00:03,132 --> 01:00:04,190
Two dollars?

410
01:00:44,741 --> 01:00:47,608
You seemed stuck-up the first
time we met... but I knew it was an act.

411
01:00:50,680 --> 01:00:51,942
It's strange,

412
01:00:53,216 --> 01:00:56,777
we wouldn't have seen each
other again if that punk hadn't run away.

413
01:01:02,692 --> 01:01:06,219
We wouldn't have met if
Jaime hadn't grown too old for his job,

414
01:01:08,831 --> 01:01:11,095
and if Maria were still living.

415
01:01:28,084 --> 01:01:30,052
Sometimes I don't feel like I'm living.

416
01:01:33,456 --> 01:01:35,185
I never take risks.

417
01:01:37,960 --> 01:01:40,428
Don't you ever want
to do something different?

418
01:01:42,231 --> 01:01:47,897
Do you ever think of moving out
on your own or with someone else?

419
01:01:52,542 --> 01:01:54,009
Sometimes.

420
01:01:55,611 --> 01:01:57,306
But Jaime needs me.

421
01:02:00,349 --> 01:02:04,046
People talk like I'll become
an old spinster, but I don't care.

422
01:02:07,156 --> 01:02:11,388
You'll still like me
if I'm an old maid, right?

423
01:02:45,828 --> 01:02:47,693
Look at the tadpoles.

424
01:02:49,766 --> 01:02:51,256
I can't lose that.

425
01:02:54,637 --> 01:02:55,695
I can see it.

426
01:02:59,909 --> 01:03:00,671
Hold on.

427
01:03:21,230 --> 01:03:24,324
It's beautiful.
Where'd you get it?

428
01:03:24,534 --> 01:03:25,694
From Manuel.

429
01:03:27,770 --> 01:03:29,499
My late husband.

430
01:03:47,190 --> 01:03:48,680
What do you think about El Paso?

431
01:03:51,828 --> 01:03:54,262
I might move there if my
cousin gets me a better job.

432
01:03:57,333 --> 01:03:59,135
You should come-

433
01:03:59,135 --> 01:04:00,363
Luis, don't...

434
01:04:07,076 --> 01:04:08,270
Okay.

435
01:04:10,246 --> 01:04:11,736
I guess it's late.

436
01:04:13,549 --> 01:04:15,176
You know how I feel about you...

437
01:04:16,686 --> 01:04:18,017
I won't ask anymore.

438
01:04:21,390 --> 01:04:26,089
If you want to see me again you can call me.

439
01:04:30,333 --> 01:04:32,824
I don't want to go back to Alice's yet.

440
01:05:22,351 --> 01:05:24,683
Hey. We were worried about you.

441
01:05:25,454 --> 01:05:29,857
Sorry, I didn't think it would be a problem.

442
01:05:30,660 --> 01:05:33,288
Well, it's just inappropriate, really.

443
01:05:34,830 --> 01:05:37,697
Tell her, Dad. She needs to be careful.

