﻿1
00:00:06,356 --> 00:00:07,990
(Episode 36)

2
00:00:07,991 --> 00:00:09,393
Gosh, it hurts.

3
00:00:28,678 --> 00:00:30,013
Hey, what are you doing?

4
00:00:32,182 --> 00:00:35,185
You absolutely have to stay away
from work until you fully recover.

5
00:00:35,552 --> 00:00:37,721
I'm really okay. We should
have a meeting now.

6
00:00:37,954 --> 00:00:39,922
The doctor said that you shouldn't
be working now.

7
00:00:39,923 --> 00:00:41,334
You got discharged on that condition.

8
00:00:41,358 --> 00:00:43,926
I'm really fine. I can take
care of myself...

9
00:00:43,927 --> 00:00:46,430
I will look after you. As your
co-worker, that is.

10
00:00:48,532 --> 00:00:50,534
- No, Bong Hee.
- No, Mr. No.

11
00:00:53,036 --> 00:00:55,214
I'll feel guilty if you don't let me take
care of you.

12
00:00:55,238 --> 00:00:58,842
I'll keep beating myself up thinking that
you got stabbed instead of me.

13
00:00:59,076 --> 00:01:00,077
Shall I do that?

14
00:01:11,721 --> 00:01:14,624
Eat the porridge first, then take
your medication with water.

15
00:01:17,794 --> 00:01:18,795
Hey, I...

16
00:01:19,529 --> 00:01:20,530
I don't like porridge.

17
00:01:56,032 --> 00:01:57,700
I'll... I'll help you.

18
00:01:57,701 --> 00:01:59,201
No, it's okay.

19
00:01:59,202 --> 00:02:00,402
No, it must be hard for you.

20
00:02:00,403 --> 00:02:01,972
No, it's okay. I can do it myself.

21
00:02:11,047 --> 00:02:12,482
- Does it sting?
- It's okay.

22
00:02:21,525 --> 00:02:22,792
You're tickling me.

23
00:02:23,927 --> 00:02:24,928
Am I?

24
00:02:33,103 --> 00:02:34,783
I'm really struggling to hold myself back.

25
00:02:41,745 --> 00:02:42,812
Hey.

26
00:02:44,080 --> 00:02:45,182
I'll do it.

27
00:02:47,617 --> 00:02:48,618
Okay.

28
00:03:04,901 --> 00:03:08,304
Nothing is for certain because
it happened too long ago.

29
00:03:08,305 --> 00:03:09,306
However,

30
00:03:09,539 --> 00:03:12,059
there's a possibility that it was
an accidental fire, not arson.

31
00:03:12,442 --> 00:03:14,322
That's the best conclusion
we can arrive at now.

32
00:03:19,916 --> 00:03:20,984
How am I...

33
00:03:23,019 --> 00:03:25,455
supposed to bring all this up to Bong Hee?

34
00:03:44,874 --> 00:03:46,543
There's something I have to tell you.

35
00:03:49,412 --> 00:03:51,348
Can you make some time for
me after work today?

36
00:03:54,184 --> 00:03:55,652
Sure, I will.

37
00:04:26,816 --> 00:04:30,120
The reason of the fire still
remains to be unknown.

38
00:04:31,721 --> 00:04:33,290
But one thing I'm certain of...

39
00:04:35,892 --> 00:04:37,827
is that your father...

40
00:04:40,030 --> 00:04:41,531
was not an arsonist...

41
00:04:44,467 --> 00:04:45,702
that killed...

42
00:04:48,805 --> 00:04:49,806
my parents.

43
00:04:52,275 --> 00:04:54,311
Actually, it was the opposite.

44
00:04:57,213 --> 00:04:58,315
He saved my life...

45
00:04:59,816 --> 00:05:00,984
and passed away...

46
00:05:03,920 --> 00:05:05,322
while trying to rescue my parents.

47
00:05:22,339 --> 00:05:23,373
Then...

48
00:05:23,907 --> 00:05:25,175
why...

49
00:05:26,609 --> 00:05:27,711
was my dad...

50
00:05:28,311 --> 00:05:29,979
accused of killing them?

51
00:05:31,715 --> 00:05:33,249
Why on earth did that happen?

52
00:05:46,229 --> 00:05:47,330
It was because of me.

53
00:05:48,231 --> 00:05:49,299
Sorry?

54
00:05:53,436 --> 00:05:54,513
I beg your pardon?

55
00:05:54,537 --> 00:05:55,572
I...

56
00:06:02,612 --> 00:06:04,252
made a mistake while giving my statement.

57
00:06:06,549 --> 00:06:08,985
I said that your dad is the culprit.

58
00:06:10,186 --> 00:06:12,622
I gave wrong information in my statement.

59
00:06:15,558 --> 00:06:16,593
Why?

60
00:06:17,193 --> 00:06:18,328
Why did you do that?

61
00:06:20,630 --> 00:06:21,631
Answer me.

62
00:06:21,998 --> 00:06:23,633
Tell me why you did such a thing.

63
00:06:24,701 --> 00:06:26,335
Whatever I say will be an excuse.

64
00:06:26,336 --> 00:06:28,214
Then go ahead and give me your excuse.

65
00:06:28,238 --> 00:06:29,773
Say something, please.

66
00:06:30,140 --> 00:06:33,076
Do whatever you can to try
to make me understand.

67
00:06:36,813 --> 00:06:38,014
Can you recognize him?

68
00:06:43,086 --> 00:06:44,154
This is the man...

69
00:06:44,621 --> 00:06:45,755
who killed your dad...

70
00:06:46,256 --> 00:06:47,991
and mom.

71
00:06:48,425 --> 00:06:49,426
Okay?

72
00:06:51,628 --> 00:06:52,629
I think...

73
00:06:53,997 --> 00:06:55,932
I had lost my memory temporarily...

74
00:06:56,900 --> 00:06:58,735
due to the shock from the incident...

75
00:07:00,069 --> 00:07:01,171
back then.

76
00:07:02,906 --> 00:07:04,474
The District Attorney, Jang Moo Young,

77
00:07:04,808 --> 00:07:07,277
who wrongly charged your dad...

78
00:07:07,544 --> 00:07:09,612
had to conceal his mistake.

79
00:07:10,413 --> 00:07:11,414
Hence,

80
00:07:11,815 --> 00:07:13,750
he probably blamed him for the fire...

81
00:07:14,818 --> 00:07:16,419
because he was found on the scene.

82
00:07:17,320 --> 00:07:20,290
The prosecution... Back
then, the prosecution...

83
00:07:20,723 --> 00:07:23,893
was even more unforgiving toward
mistakes than they are now.

84
00:07:27,997 --> 00:07:28,998
No, but...

85
00:07:32,035 --> 00:07:33,570
It was that man.

86
00:07:34,103 --> 00:07:36,606
He's the culprit.

87
00:07:40,143 --> 00:07:43,112
Whatever excuse I give, it is ultimately...

88
00:07:48,284 --> 00:07:49,519
all my fault.

89
00:09:10,500 --> 00:09:11,601
I must have...

90
00:09:13,102 --> 00:09:15,338
gotten way ahead of myself.

91
00:09:18,074 --> 00:09:19,375
He's not even calling me.

92
00:09:20,810 --> 00:09:22,545
Ji Eun Hyuk, you jerk.

93
00:09:31,854 --> 00:09:33,022
Hey, you're home.

94
00:09:42,799 --> 00:09:43,800
Hey.

95
00:09:45,068 --> 00:09:46,301
Did you cry?

96
00:09:46,302 --> 00:09:50,607
Darn it. Pretend you
didn't see that at least.

97
00:09:53,042 --> 00:09:56,980
Why are you about to cry too?

98
00:10:07,991 --> 00:10:10,893
Hey, I'm okay.

99
00:10:12,061 --> 00:10:14,430
You don't have to cry for me.
I'm not that sad.

100
00:10:17,133 --> 00:10:21,371
Hey, I'm really fine.

101
00:10:36,619 --> 00:10:38,788
- Hey.
- Hey, don't cry.

102
00:10:39,922 --> 00:10:41,824
- Don't cry.
- Don't cry.

103
00:10:47,830 --> 00:10:50,299
Eun Hyuk, you've been nice to me lately.

104
00:10:52,435 --> 00:10:54,315
It's all because of meeting
you coincidentally.

105
00:10:55,204 --> 00:10:57,940
Besides, who knew I had
to carry all these files?

106
00:10:59,442 --> 00:11:03,046
By the way, you are going to
read these files again at home?

107
00:11:04,180 --> 00:11:05,940
Don't you think you're
overworking yourself?

108
00:11:07,517 --> 00:11:08,551
Eun Hyuk.

109
00:11:09,786 --> 00:11:10,787
Yes?

110
00:11:12,689 --> 00:11:14,657
Is it really a coincidence?

111
00:11:15,692 --> 00:11:16,726
Sorry?

112
00:11:17,527 --> 00:11:19,929
You always help me whenever
I'm having a tough time.

113
00:11:21,631 --> 00:11:24,000
Is this really a coincidence?

114
00:11:26,135 --> 00:11:28,271
Yes, it is.

115
00:11:30,473 --> 00:11:31,474
I see.

116
00:11:32,608 --> 00:11:33,843
I got it then.

117
00:11:34,677 --> 00:11:37,647
I could have gotten the wrong idea.

118
00:11:40,283 --> 00:11:43,152
We are just friends. We
are just good friends,

119
00:11:44,287 --> 00:11:45,321
aren't we?

120
00:11:51,894 --> 00:11:53,229
Yes, we are.

121
00:12:25,461 --> 00:12:26,729
I did the right thing.

122
00:12:30,333 --> 00:12:31,434
Yes, I did.

123
00:12:35,738 --> 00:12:36,773
I did well.

124
00:13:18,047 --> 00:13:19,715
(Sunho District Prosecutor's Office)

125
00:13:20,416 --> 00:13:21,818
What brings you here?

126
00:13:23,519 --> 00:13:26,556
You don't have to show it so
obviously that you hate me.

127
00:13:26,989 --> 00:13:31,394
I didn't exactly come
here because I like you.

128
00:13:31,561 --> 00:13:32,828
Then you should leave.

129
00:13:32,829 --> 00:13:35,069
Should I go without telling
you the purpose of my visit?

130
00:13:35,798 --> 00:13:37,332
I should at least tell you why.

131
00:13:37,333 --> 00:13:39,035
Then go on.

132
00:13:40,236 --> 00:13:41,304
I...

133
00:13:43,172 --> 00:13:45,975
am going to tell you some things
that you won't like to hear...

134
00:13:46,609 --> 00:13:48,511
and that will hurt your feelings.

135
00:13:49,679 --> 00:13:51,379
Your son, Hee Jun.

136
00:13:51,380 --> 00:13:53,982
Leave if you are going to talk nonsense.

137
00:13:53,983 --> 00:13:55,751
You have it all wrong.

138
00:13:57,053 --> 00:13:59,388
- Bong Hee isn't the culprit.
- Young Hee.

139
00:13:59,488 --> 00:14:03,025
You know that guy who lost his memory?

140
00:14:03,492 --> 00:14:06,328
His name is Jung Hyun Soo. He's your guy.

141
00:14:06,329 --> 00:14:07,530
What are you saying?

142
00:14:07,630 --> 00:14:11,434
You are ignorant, think
you're always right,

143
00:14:11,701 --> 00:14:13,903
and never listen to others.

144
00:14:14,337 --> 00:14:16,505
But you aren't stupid.

145
00:14:18,040 --> 00:14:19,508
Think carefully.

146
00:14:19,976 --> 00:14:22,645
If you look at all the
circumstantial evidence,

147
00:14:22,812 --> 00:14:25,081
you will come to realize Jung
Hyun Soo is the culprit.

148
00:14:25,948 --> 00:14:28,784
So look over the case carefully...

149
00:14:29,986 --> 00:14:33,022
and stop harassing Ji Wook...

150
00:14:34,190 --> 00:14:35,591
and Bong Hee.

151
00:14:37,493 --> 00:14:39,395
Ji Wook is born to be a prosecutor.

152
00:14:40,897 --> 00:14:42,698
Reinstate him.

153
00:15:04,754 --> 00:15:05,754
(Incident Report)

154
00:15:05,755 --> 00:15:07,515
(Jang Hee Jun Murder
Case at Supa Apartment)

155
00:15:07,790 --> 00:15:10,893
(Water Tank Murder Case at Supa Apartment)

156
00:15:30,513 --> 00:15:32,615
I was going to visit you.

157
00:15:34,350 --> 00:15:35,751
Look how I ran into you.

158
00:15:35,985 --> 00:15:37,386
You are here to see me?

159
00:15:37,753 --> 00:15:38,754
Yes.

160
00:15:39,989 --> 00:15:42,758
I was going to tell you that...

161
00:15:43,559 --> 00:15:44,794
you should lead a better life.

162
00:15:44,961 --> 00:15:45,995
What?

163
00:15:46,228 --> 00:15:47,830
My dad was...

164
00:15:48,998 --> 00:15:51,734
Eun Man Soo.

165
00:15:55,504 --> 00:15:56,605
You are...

166
00:15:57,573 --> 00:15:59,275
Eun Man Soo's daughter?

167
00:16:00,710 --> 00:16:01,711
Yes.

168
00:16:02,345 --> 00:16:05,781
He's the scapegoat who you used
to cover up your little mistake.

169
00:16:06,782 --> 00:16:08,650
I'm his daughter.

170
00:16:08,651 --> 00:16:10,519
- That was...
- Just listen.

171
00:16:12,989 --> 00:16:14,390
My dad...

172
00:16:15,858 --> 00:16:17,693
was never an arsonist.

173
00:16:19,095 --> 00:16:20,129
I...

174
00:16:21,330 --> 00:16:23,699
am not the murderer who killed your son.

175
00:16:25,101 --> 00:16:28,403
But you think you are always right...

176
00:16:28,404 --> 00:16:31,107
and that you are never wrong.

177
00:16:31,941 --> 00:16:34,744
On top of that, you even fabricated
evidence and persecuted him.

178
00:16:37,213 --> 00:16:40,583
Have you thought about how
the family you ruined lived?

179
00:16:42,018 --> 00:16:44,120
Or how my mother lived?

180
00:16:45,054 --> 00:16:47,957
Do you know how hard my life was
because of what you did to me?

181
00:16:49,558 --> 00:16:52,828
You don't know, do you? You
were never interested anyway.

182
00:16:57,133 --> 00:16:59,101
Let me tell you again.

183
00:17:00,803 --> 00:17:02,204
My dad...

184
00:17:03,539 --> 00:17:06,609
was an innocent man who did nothing wrong.

185
00:17:09,745 --> 00:17:10,846
It was you...

186
00:17:11,847 --> 00:17:13,749
who made a mistake.

187
00:17:50,186 --> 00:17:52,521
(Attorney No Ji Wook)

188
00:18:36,999 --> 00:18:39,902
No, don't. There's no need. I'm fine.

189
00:18:42,938 --> 00:18:43,973
Sit down.

190
00:18:45,141 --> 00:18:46,675
- Bong Hee.
- Mr. No.

191
00:18:47,476 --> 00:18:49,078
Just listen to me, please.

192
00:18:50,446 --> 00:18:51,480
Aren't you...

193
00:18:52,882 --> 00:18:54,150
sorry?

194
00:19:00,689 --> 00:19:01,690
Yes.

195
00:19:02,992 --> 00:19:04,827
I don't know how to act around you.

196
00:19:05,427 --> 00:19:09,198
I know that the DA's mistake
is bigger than yours.

197
00:19:10,199 --> 00:19:12,635
That's that. Anyway,

198
00:19:13,836 --> 00:19:15,938
it is true that you did
make the false statement...

199
00:19:17,039 --> 00:19:18,140
against my dad.

200
00:19:21,810 --> 00:19:22,845
You're right.

201
00:19:25,581 --> 00:19:27,816
So wait for my decision.

202
00:19:28,184 --> 00:19:30,119
Whether I should forgive you,

203
00:19:30,352 --> 00:19:31,921
or I should just let things be.

204
00:19:34,657 --> 00:19:35,791
Please sit down.

205
00:19:36,926 --> 00:19:39,128
No, it's really okay.

206
00:19:39,328 --> 00:19:42,198
I can do it myself. Don't worry about it.

207
00:19:45,034 --> 00:19:46,101
All right, then.

208
00:19:49,271 --> 00:19:50,272
Wait.

209
00:20:00,916 --> 00:20:01,917
Thanks...

210
00:20:02,952 --> 00:20:04,086
for coming back.

211
00:20:07,323 --> 00:20:08,457
I didn't come back for good.

212
00:20:09,959 --> 00:20:11,879
I'm just trying to keep
my feelings from work...

213
00:20:12,928 --> 00:20:14,968
and giving myself time to
sort through my thoughts.

214
00:20:17,299 --> 00:20:18,767
You're right, but...

215
00:20:20,436 --> 00:20:22,538
this is more than enough for me.

216
00:20:33,949 --> 00:20:35,017
Just listen to me.

217
00:20:35,484 --> 00:20:37,919
They can settle right away.

218
00:20:37,920 --> 00:20:39,554
Gosh, that's not how it works.

219
00:20:39,555 --> 00:20:41,322
Are you sure you have
decades of experience?

220
00:20:41,323 --> 00:20:45,503
Goodness, that's how things used
to be done back in the days.

221
00:20:45,527 --> 00:20:47,228
Can we please get this meeting done?

222
00:20:47,229 --> 00:20:49,264
Yes, please. That's what
I've wanted to say.

223
00:20:49,265 --> 00:20:51,033
Ms. Lack of Evidence, shut it.

224
00:20:51,267 --> 00:20:52,401
Gosh, seriously!

225
00:20:52,935 --> 00:20:54,470
Let's focus on the meeting, please!

226
00:20:54,837 --> 00:20:56,438
You guys are so loud.

227
00:20:56,572 --> 00:20:57,673
Hey, don't record this.

228
00:20:57,940 --> 00:20:59,207
You're distracting me.

229
00:20:59,208 --> 00:21:02,143
Hey, you should've told us
that you were filming us.

230
00:21:02,144 --> 00:21:03,545
Good. Okay.

231
00:21:05,648 --> 00:21:06,815
I look pretty good.

232
00:21:09,518 --> 00:21:10,586
Okay.

233
00:21:13,455 --> 00:21:15,923
Can we please get this meeting done?

234
00:21:15,924 --> 00:21:17,860
Yes, please. That's what
I've wanted to say.

235
00:21:20,329 --> 00:21:22,131
Hey, shut it.

236
00:21:23,432 --> 00:21:24,533
(Smart selection)

237
00:21:30,406 --> 00:21:32,775
Gosh, seriously! Let's focus
on the meeting, please!

238
00:21:33,108 --> 00:21:34,410
Hey, don't record this.

239
00:21:53,329 --> 00:21:54,330
Can you...

240
00:21:56,031 --> 00:21:57,099
recognize me?

241
00:22:05,040 --> 00:22:06,240
Your memory must've come back.

242
00:22:10,012 --> 00:22:11,380
Yes, thanks to you.

243
00:22:12,047 --> 00:22:13,415
Then you must know this too.

244
00:22:13,949 --> 00:22:16,285
There's no more loophole
for you to escape through.

245
00:22:17,319 --> 00:22:19,354
You're charged with two
counts of attempted murders,

246
00:22:19,355 --> 00:22:20,515
including what you did to me.

247
00:22:21,223 --> 00:22:24,193
And I'll be sure to prove that you
killed all those people. So...

248
00:22:25,060 --> 00:22:26,495
you better come clean, you scum.

249
00:22:27,329 --> 00:22:29,397
The system in this country is merciful...

250
00:22:29,398 --> 00:22:30,632
toward those who confess...

251
00:22:30,733 --> 00:22:32,301
and repent their wrongdoings.

252
00:22:32,601 --> 00:22:35,670
But personally, I don't
think scums like you...

253
00:22:35,671 --> 00:22:38,006
deserve any mercy.

254
00:22:42,010 --> 00:22:45,290
What was that about? Are
you not totally there yet?

255
00:22:45,314 --> 00:22:47,149
Is that it? Do you not get what's going on?

256
00:22:47,616 --> 00:22:50,152
No, no. That's not it.

257
00:22:51,220 --> 00:22:52,821
This is just a speculation,

258
00:22:53,288 --> 00:22:55,557
but someone might still be on my side.

259
00:22:55,791 --> 00:22:57,059
That's what it was about.

260
00:23:16,745 --> 00:23:18,547
(Sunho District Prosecutor's Office)

261
00:23:25,888 --> 00:23:27,556
So you've finally decided to talk?

262
00:23:30,225 --> 00:23:31,293
Mr. Jung.

263
00:23:39,601 --> 00:23:40,903
First off,

264
00:23:43,505 --> 00:23:44,807
I killed them all.

265
00:23:48,844 --> 00:23:50,345
Whom shall I start with?

266
00:23:51,613 --> 00:23:52,815
Whom shall I talk about first?

267
00:23:54,683 --> 00:23:57,585
We first met through the Yang Jin Woo case,

268
00:23:57,586 --> 00:23:58,786
so shall I start with Jin Woo?

269
00:23:59,054 --> 00:24:00,422
No, no. I take that back.

270
00:24:00,656 --> 00:24:03,425
Supa Apartment. I should
start with Supa Apartment.

271
00:24:03,725 --> 00:24:07,128
You've managed to confirm the
identity of only one body...

272
00:24:07,129 --> 00:24:08,609
found in the water tank there, right?

273
00:24:09,131 --> 00:24:10,651
Shall I tell you who the other one is?

274
00:24:12,034 --> 00:24:13,702
There's no reason for me to say no.

275
00:24:15,671 --> 00:24:16,672
All right.

276
00:24:18,106 --> 00:24:19,818
His name is Sung Jae Hyun.

277
00:24:19,842 --> 00:24:21,509
He and Jin Woo went to
the same high school.

278
00:24:21,510 --> 00:24:22,611
Go ahead and check it.

279
00:24:24,813 --> 00:24:26,180
- Is that right?
- I'm not done yet.

280
00:24:26,181 --> 00:24:27,416
How dare you cut me off?

281
00:24:32,421 --> 00:24:33,789
There was one more.

282
00:24:36,225 --> 00:24:37,626
Was his name Jang Hee Jun?

283
00:24:40,229 --> 00:24:41,330
Jang Hee Jun?

284
00:24:43,532 --> 00:24:45,932
I'm pretty sure it was Jang Hee Jun.
Yes, that was his name.

285
00:24:48,370 --> 00:24:50,172
Well... Initially,

286
00:24:50,339 --> 00:24:52,374
I had no intention of killing him.

287
00:24:52,741 --> 00:24:54,910
I was working on my stuff on the rooftop,

288
00:24:55,043 --> 00:24:56,377
and someone witnessed it.

289
00:24:56,378 --> 00:24:58,013
That was Eun Bong Hee.

290
00:24:58,413 --> 00:25:00,782
So I went there to kill her,

291
00:25:00,949 --> 00:25:03,452
but I found a drunk guy instead of her.

292
00:25:04,686 --> 00:25:07,422
I ended up stabbing him with a knife,

293
00:25:08,790 --> 00:25:10,359
and he collapsed while crying.

294
00:25:13,729 --> 00:25:14,830
"Dad."

295
00:25:17,032 --> 00:25:18,333
"Dad, save me."

296
00:25:18,734 --> 00:25:20,135
"Dad, help me."

297
00:25:22,538 --> 00:25:23,605
"I'm terrified."

298
00:25:24,406 --> 00:25:25,646
He was saying things like that.

299
00:25:26,975 --> 00:25:28,310
And he kept crying.

300
00:25:29,211 --> 00:25:32,113
Gosh, that guy was being such a
crybaby, and I couldn't stand it.

301
00:25:32,114 --> 00:25:34,116
So I stabbed him again and killed him.

302
00:25:39,788 --> 00:25:40,889
That's what happened.

303
00:26:46,088 --> 00:26:48,957
Hey, Ji Hye. I'm on my
way to your office now.

304
00:26:49,691 --> 00:26:50,926
Yes, okay.

305
00:26:57,499 --> 00:26:58,533
I'm sorry.

306
00:27:12,280 --> 00:27:14,516
I won't accept your apology.

307
00:27:38,740 --> 00:27:39,808
Come with me for a second.

308
00:27:40,709 --> 00:27:41,743
Why?

309
00:28:51,680 --> 00:28:54,182
I'm calling from the HR department
at the Ministry of Justice.

310
00:28:54,516 --> 00:28:55,950
I'd like to let you know that you have...

311
00:28:55,951 --> 00:28:58,191
passed the recruitment exam
for experienced prosecutors.

312
00:29:05,794 --> 00:29:07,228
(Ji Chang Wook's "101
Reasons Why I Like You"...)

313
00:29:07,229 --> 00:29:08,630
(is available on music websites.)

314
00:29:40,395 --> 00:29:42,931
(Epilogue)

315
00:30:26,775 --> 00:30:31,480
(Love in Trouble)

