1
00:04:25,414 --> 00:04:26,449
Who are you looking for?

2
00:04:28,134 --> 00:04:30,364
My mother.

3
00:04:45,054 --> 00:04:47,568
Here... the good stuff.

4
00:04:49,334 --> 00:04:50,562
Hello.

5
00:04:56,854 --> 00:04:57,684
Come in.

6
00:05:14,294 --> 00:05:15,090
This is your sister.

7
00:05:20,854 --> 00:05:22,128
Hug each other.

8
00:05:49,534 --> 00:05:50,887
Go pour some drink for the men!

9
00:06:03,654 --> 00:06:06,293
There's no room for you
to stay here. lt's not possible.

10
00:06:07,414 --> 00:06:10,133
No problem... no problem at all.

11
00:06:15,534 --> 00:06:19,368
ls my father's hut still standing?

12
00:06:20,374 --> 00:06:21,363
lt is.

13
00:06:23,014 --> 00:06:25,403
How long are you planning to stay?

14
00:06:25,614 --> 00:06:26,524
For good.

15
00:10:16,494 --> 00:10:18,371
Give me a shot.
l don't have any money.

16
00:10:18,854 --> 00:10:20,492
l don't have any money.

17
00:10:21,574 --> 00:10:22,609
This needs to be filled up.

18
00:10:24,814 --> 00:10:25,883
With what?

19
00:10:27,094 --> 00:10:28,686
Brandy.

20
00:10:29,534 --> 00:10:31,206
You're cold out there, aren't you?

21
00:10:33,854 --> 00:10:36,493
What kind of brandy? Does it matter?

22
00:10:37,334 --> 00:10:38,130
No.

23
00:10:38,414 --> 00:10:40,006
G<i>ive me a shot.</i>

24
00:10:44,694 --> 00:10:46,844
<i>I'II pay for everything tomorrow...</i>

25
00:10:47,414 --> 00:10:48,972
G<i>ive me a shot.</i>

26
00:10:51,454 --> 00:10:54,366
<i>I'II give you a fish... this big!</i>

27
00:10:55,574 --> 00:10:57,166
G<i>ive me a shot.</i>

28
00:10:58,614 --> 00:10:59,808
l'll pay for it.

29
00:11:00,054 --> 00:11:01,407
l'll pay for ever<i>yt</i>hing.

30
00:11:01,854 --> 00:11:04,414
Give me a shot.

31
00:11:04,854 --> 00:11:08,369
Look, money! l'll buy the whole bar.

32
00:11:08,614 --> 00:11:11,447
Look, you see this money?
l'll buy the whole bar.

33
00:11:11,934 --> 00:11:13,572
l'll buy the whole bar.

34
00:11:14,014 --> 00:11:18,963
Give me another shot for this money.

35
00:11:24,574 --> 00:11:27,327
- l only have big notes.
- That's fine.

36
00:11:28,534 --> 00:11:30,411
Later, then.

37
00:11:31,534 --> 00:11:32,569
Thanks.

38
00:11:34,694 --> 00:11:36,491
See you.

39
00:11:37,574 --> 00:11:39,883
Wait! Be careful,
the brandy is strong!

40
00:12:20,614 --> 00:12:21,763
He's mine.

41
00:12:23,854 --> 00:12:25,207
How did he get here?

42
00:12:28,494 --> 00:12:29,563
l don't know.

43
00:12:31,374 --> 00:12:33,729
Our mother's lover hid him.

44
00:12:38,654 --> 00:12:39,882
Hey...

45
00:12:43,134 --> 00:12:44,453
...come on.

46
00:12:52,894 --> 00:12:54,725
Give me your plate.

47
00:12:58,654 --> 00:12:59,769
You're not eating?

48
00:13:00,334 --> 00:13:01,369
Not now.

49
00:13:13,734 --> 00:13:15,804
Once, l didn't eat for a year.

50
00:13:18,614 --> 00:13:19,569
Why?

51
00:13:20,614 --> 00:13:22,206
l wasn't hungry.

52
00:13:28,654 --> 00:13:30,246
But you're a good cook.

53
00:13:32,134 --> 00:13:33,283
Thanks.

54
00:13:45,614 --> 00:13:46,842
Are you married?

55
00:13:50,534 --> 00:13:51,330
No.

56
00:13:55,654 --> 00:13:56,803
And children?

57
00:14:04,654 --> 00:14:05,803
No.

58
00:16:37,614 --> 00:16:38,649
Where have you been?

59
00:16:40,654 --> 00:16:41,973
ls it any of your business?

60
00:16:43,774 --> 00:16:45,002
No, of course not.

61
00:16:58,174 --> 00:16:59,607
What do you need timber for?

62
00:17:01,614 --> 00:17:03,013
l just need it.

63
00:17:06,814 --> 00:17:08,088
This is very expensive timber.

64
00:17:10,654 --> 00:17:12,326
l've already made my selection.

65
00:17:12,854 --> 00:17:14,207
ls this enough?

66
00:17:42,374 --> 00:17:43,170
lt's enough.

67
00:17:46,894 --> 00:17:48,293
You're pretty loaded.

68
00:17:50,254 --> 00:17:51,687
You don't come home.

69
00:17:52,174 --> 00:17:53,209
Your mother and l can't sleep.

70
00:17:53,614 --> 00:17:56,174
But who cares right?
You're fine, right?

71
00:18:00,054 --> 00:18:01,248
You're a grown-up.

72
00:18:01,934 --> 00:18:04,528
lf you were my daughter,
l'd hit you.

73
00:18:05,294 --> 00:18:06,568
Did she sleep at your place?

74
00:18:07,654 --> 00:18:09,884
You talked her into it, didn't you?

75
00:18:12,734 --> 00:18:14,452
Well, l'll deliver the timber...

76
00:18:16,254 --> 00:18:18,006
...if the permit comes.

77
00:18:18,414 --> 00:18:19,972
lt'll take a few months.

78
00:18:20,614 --> 00:18:23,287
lt will be safe here,
no one will steal it.

79
00:18:24,334 --> 00:18:25,926
Don't count on any other boat...

80
00:18:27,854 --> 00:18:29,207
...or you can go back

81
00:18:29,814 --> 00:18:31,293
to where you came from

82
00:20:31,574 --> 00:20:32,802
A drink?

83
00:20:34,294 --> 00:20:37,366
A beer, please.

84
00:20:57,254 --> 00:20:58,767
Pour some for me, too.

85
00:21:03,974 --> 00:21:06,408
You need a permit
to transport timber.

86
00:21:10,654 --> 00:21:12,610
He's building a house.

87
00:21:15,694 --> 00:21:17,844
So, you just do whatever you like?

88
00:21:26,654 --> 00:21:28,565
You need a permit
for the house as well.

89
00:22:25,414 --> 00:22:27,052
Don't tidy up around here!

90
00:22:37,094 --> 00:22:38,493
Get out!

91
00:23:02,574 --> 00:23:03,927
You're not going anywhere!

92
00:23:04,974 --> 00:23:06,930
Let her go.

93
00:23:12,094 --> 00:23:13,607
She's your daughter.

94
00:23:16,814 --> 00:23:18,691
Continue.

95
00:26:22,614 --> 00:26:24,252
Can l borrow your sweater?

96
00:27:36,854 --> 00:27:38,207
Do you want me to make a fire?

97
00:27:41,254 --> 00:27:45,213
No. lt's colder when it dies out.

98
00:27:47,614 --> 00:27:48,808
That's not true.

99
00:27:49,734 --> 00:27:51,406
That's what Dad told me.

100
00:28:19,694 --> 00:28:21,207
You're gonna stay up?

101
00:28:24,334 --> 00:28:25,403
Yes.

102
00:28:30,774 --> 00:28:32,969
What's this silver cup?

103
00:28:34,774 --> 00:28:37,083
l don't want to talk about it.

104
00:28:45,614 --> 00:28:47,127
Good night, then.

105
00:29:55,734 --> 00:29:57,292
Wake up.

106
00:30:31,934 --> 00:30:33,526
Will you hold it?

107
00:30:58,774 --> 00:31:01,334
Has your hair always been that long?

108
00:31:02,974 --> 00:31:04,168
l don't know.

109
00:31:05,614 --> 00:31:07,093
lt's in your eyes.

110
00:31:08,574 --> 00:31:10,849
You have a handsome face,
you should fix it.

111
00:31:36,774 --> 00:31:38,127
Let's pull it closer.

112
00:31:38,774 --> 00:31:40,685
Wait! Hold on a moment.

113
00:31:43,174 --> 00:31:44,607
This way.

114
00:31:48,774 --> 00:31:51,766
Hold on for a little moment.

115
00:31:54,454 --> 00:31:56,046
- Shall l stand on it?
- Ha?

116
00:31:56,134 --> 00:31:57,806
Shall l stand on it?

117
00:31:58,334 --> 00:32:00,131
No need.

118
00:32:03,654 --> 00:32:05,485
Then give me a hammer.

119
00:32:06,854 --> 00:32:08,765
Here you are.
- And a nail.

120
00:33:52,614 --> 00:33:53,933
Hi.

121
00:33:55,094 --> 00:33:56,049
Hello.

122
00:33:56,134 --> 00:33:57,283
Come.

123
00:33:57,934 --> 00:33:59,925
Come my dear, put this on.

124
00:34:01,814 --> 00:34:03,611
The old drunk died.

125
00:34:03,694 --> 00:34:06,333
She shouldn't be late.

126
00:34:11,614 --> 00:34:14,208
We had to make a damn
big detour because of you.

127
00:34:24,574 --> 00:34:28,249
ls the palace coming along?

128
00:34:28,694 --> 00:34:30,286
lt's getting there.

129
00:34:31,974 --> 00:34:33,612
l have to go.

130
00:34:36,814 --> 00:34:38,532
l won't be gone long.

131
00:34:51,174 --> 00:34:53,324
We're leaving.

132
00:39:18,574 --> 00:39:21,008
You are such a stupid little girl.

133
00:39:32,774 --> 00:39:36,289
Where are you going, huh?

134
00:39:36,574 --> 00:39:38,087
She's leaving, don't you see?

135
00:39:38,254 --> 00:39:40,688
- You're letting her go?
- She's all packed. She's leaving.

136
00:39:40,894 --> 00:39:44,967
- To her brother.
- She's a complete idiot.

137
00:39:45,174 --> 00:39:47,449
You can't talk sense to her.
She's leaving.

138
00:39:47,694 --> 00:39:49,571
- To her brother, is she?
- To her brother, so what?

139
00:39:49,934 --> 00:39:52,528
- They're going to live together?
- They're brother and sister.

140
00:39:52,694 --> 00:39:55,447
- And you think it's appropriate?
- Yes, l do.

141
00:39:55,614 --> 00:39:58,970
- You do? - Stay out of this,
she's my daughter.

142
00:40:00,614 --> 00:40:03,333
Will you wash yourself
in front of him?

143
00:40:05,774 --> 00:40:07,844
How do you think they'll sleep...

144
00:40:08,894 --> 00:40:09,929
like brother and sister?

145
00:40:10,014 --> 00:40:11,447
Stop it!

146
00:40:11,574 --> 00:40:13,007
Like pigs!

147
00:40:14,094 --> 00:40:15,493
You're not going anywhere!

148
00:41:18,734 --> 00:41:20,406
Come back.

149
00:48:19,454 --> 00:48:22,207
Hurry up, l don't want to
spend the summer here!

150
00:48:23,534 --> 00:48:25,092
Unload ever<i>yt</i>hing.

151
00:48:30,694 --> 00:48:33,128
l brought the things
my niece asked for...

152
00:48:33,294 --> 00:48:36,650
nets, fuel, axe, saw, hammer.

153
00:48:38,614 --> 00:48:40,206
lt's gonna be a nice, big house.

154
00:48:40,454 --> 00:48:41,887
How will you cover it?

155
00:48:42,334 --> 00:48:43,813
l don't know yet.

156
00:48:48,094 --> 00:48:50,085
Can you do it alone?

157
00:48:50,334 --> 00:48:52,404
Will you pulley it?

158
00:48:58,774 --> 00:49:00,526
Will all these be windows?

159
00:49:04,174 --> 00:49:07,610
Cover this area well,
the sea breeze will come in here.

160
00:49:08,654 --> 00:49:10,212
And where is my niece?

161
00:49:11,654 --> 00:49:14,851
- She's sick. She's sleeping.
- Has she been sick long?

162
00:49:15,774 --> 00:49:18,732
- A few days now.
- What's wrong with her?

163
00:49:19,774 --> 00:49:21,492
l don't know much about
these things.

164
00:49:21,694 --> 00:49:23,332
Why didn't you let me know?

165
00:49:24,734 --> 00:49:27,851
- She didn't want me to.
- You'll be needing me.

166
00:49:33,614 --> 00:49:37,209
Move it! What are you waiting for?
You want me to do it, don't you?

167
00:49:44,814 --> 00:49:50,207
So many damn frogs. They seem to
have flourished this year.

168
00:49:50,694 --> 00:49:52,764
There weren't so many last year.

169
00:49:53,734 --> 00:49:56,851
- Thanks for the brandy.
- You'll find it useful.

170
00:49:57,694 --> 00:50:00,527
- Thanks for the net, too.
- You'll find that useful too.

171
00:50:04,774 --> 00:50:08,733
Cast off! What are you waiting for?
Let's get out of here!

172
00:52:45,614 --> 00:52:47,332
You're good at this.

173
00:52:48,614 --> 00:52:50,206
When it's needed.

174
00:53:06,774 --> 00:53:13,088
At the zoo, where l used to work
a girl came...

175
00:53:17,574 --> 00:53:21,010
...she was thrown out of the house
because she got pregnant.

176
00:53:22,694 --> 00:53:29,406
l had to nurse her.
The baby was stillborn.

177
00:53:47,694 --> 00:53:49,446
But she disappeared.

178
00:53:55,694 --> 00:53:58,083
And l haven't seen her since.

179
00:54:06,774 --> 00:54:10,210
And the silver cup?
- lt's hers.

180
00:54:38,694 --> 00:54:40,252
No more.

181
00:54:42,654 --> 00:54:44,804
Can you go? l want to sleep.

182
00:55:57,614 --> 00:55:58,888
l'm hungry.

183
00:56:51,414 --> 00:56:52,733
Here.

184
00:56:53,454 --> 00:56:55,251
You're not eating?

185
00:56:55,614 --> 00:56:57,445
We only have one.

186
00:57:03,374 --> 00:57:04,363
Thanks.

187
00:57:40,094 --> 00:57:41,573
Do you want half?

188
00:57:43,134 --> 00:57:44,772
No, you eat it.

189
00:57:56,934 --> 00:57:58,765
Will we sleep here tonight?

190
00:58:04,254 --> 00:58:05,733
You'll catch a cold.

191
00:58:33,614 --> 00:58:34,763
What?

192
01:04:12,334 --> 01:04:14,484
Do you know them?
- Yes.

193
01:06:06,294 --> 01:06:09,411
Push a bit more. Come on, push.
Get up there now.

194
01:06:16,374 --> 01:06:18,649
Careful, careful, a little more.

195
01:06:29,974 --> 01:06:30,963
Hold it!

196
01:06:35,294 --> 01:06:36,329
Take it slowly.

197
01:06:39,854 --> 01:06:41,082
Hand me some nails.

198
01:08:02,134 --> 01:08:05,365
- You like it? - Yeah, it's gonna be
alright. Thanks.

199
01:08:26,454 --> 01:08:28,922
ls your sister going
to live with you?

200
01:08:31,414 --> 01:08:33,450
Sure, there's nothing
wrong with that.

201
01:08:34,094 --> 01:08:36,813
Many people won't like it, you know.

202
01:11:16,454 --> 01:11:19,332
l'll go check the nets.
- Hurry.

203
01:14:20,574 --> 01:14:22,485
Wipe the tables.

204
01:14:31,094 --> 01:14:32,447
Are you celebrating?

205
01:14:33,294 --> 01:14:34,807
What are you celebrating?

206
01:14:41,974 --> 01:14:43,851
Have you moved in already?

207
01:14:44,774 --> 01:14:46,366
lt's not ready yet.

208
01:14:54,134 --> 01:14:56,170
We caught a lot of fish.

209
01:14:56,814 --> 01:14:59,931
l'd like to invite you for dinner.

210
01:15:01,134 --> 01:15:05,764
Son, you know
we won't go over there.

211
01:15:06,574 --> 01:15:08,610
You understand that, don't you?

212
01:15:12,134 --> 01:15:14,568
Can you fill these up, anyway?

213
01:17:01,454 --> 01:17:04,173
Many people came.
- Yeah.

214
01:17:11,654 --> 01:17:13,451
Help me.

215
01:18:20,574 --> 01:18:22,530
You are a good wife!

216
01:19:52,534 --> 01:19:53,933
Nice house.

217
01:19:55,494 --> 01:19:56,973
ls he building it for you?

218
01:19:57,614 --> 01:20:00,412
l help him out.
- With what?

219
01:20:01,414 --> 01:20:04,372
- Building.
- Really? And you will live here?

220
01:20:05,494 --> 01:20:06,483
Yes.

221
01:20:08,214 --> 01:20:09,613
lt's gonna be nice, right?

222
01:20:10,694 --> 01:20:11,490
Yes.

223
01:20:11,694 --> 01:20:13,525
- Do you like being with him?
- Yes.

224
01:20:13,654 --> 01:20:14,928
Yes?
- Aha.

225
01:20:18,454 --> 01:20:20,410
- Give her some drink.
- l don't want any.

226
01:20:20,614 --> 01:20:22,093
Yes you do. Drink!
- No, l don't.

227
01:20:22,334 --> 01:20:23,289
Here.

228
01:20:24,974 --> 01:20:26,327
- What, you don't want any?
- No, l don't.

229
01:20:27,054 --> 01:20:29,363
- A minute ago you did!
- l'm not thirsty.

230
01:20:30,414 --> 01:20:31,733
No? Give her some drink!

231
01:20:33,214 --> 01:20:34,442
Drink!
- Drink! What now?

232
01:20:37,094 --> 01:20:38,925
Why aren't you drinking? The poor
man's drink isn't good enough?

233
01:20:42,534 --> 01:20:44,809
- And his food? Do you want to eat?
- No.

234
01:20:46,254 --> 01:20:47,846
Eat with us.

235
01:20:48,534 --> 01:20:49,728
Give her your watermelon!

236
01:20:51,614 --> 01:20:52,967
Hold it!
Hold it already!

237
01:20:53,534 --> 01:20:54,444
Hold it, and eat!

238
01:20:56,814 --> 01:20:58,213
Pour some drink in it!

239
01:21:01,534 --> 01:21:03,570
Eat!
- l'm not hungry.

240
01:21:04,494 --> 01:21:05,563
Eat!
- l don't want to.

241
01:21:05,694 --> 01:21:07,412
No?

242
01:21:11,614 --> 01:21:13,366
Eat up!

243
01:21:14,654 --> 01:21:16,406
Eat up!

244
01:21:19,654 --> 01:21:21,531
You don't want it?

245
01:21:21,814 --> 01:21:23,884
The poor man's food
isn't good enough, either?

246
01:21:24,574 --> 01:21:26,212
No drink, no food?

247
01:21:26,614 --> 01:21:28,411
Shove it down!

248
01:21:31,534 --> 01:21:33,126
Shove it down!

249
01:21:33,814 --> 01:21:34,803
Shove it down!

250
01:21:35,934 --> 01:21:37,367
We brought it for you!

251
01:21:38,574 --> 01:21:40,451
You want to offend us?

252
01:21:53,854 --> 01:21:55,606
Eat that melon!

253
01:21:58,534 --> 01:21:59,808
Eat up!

254
01:22:03,614 --> 01:22:06,651
ls it tasty? Say it's tasty!

255
01:22:14,614 --> 01:22:15,763
Eat up! Eat up! Eat up!

256
01:22:23,574 --> 01:22:25,246
Say you're a whore!

257
01:22:27,134 --> 01:22:28,613
Say that you're a whore!

258
01:22:29,134 --> 01:22:30,567
Say that you're a whore!

259
01:22:31,294 --> 01:22:32,966
Eat, and say you're a whore!

260
01:22:33,654 --> 01:22:34,973
Eat, and say you're a whore!

261
01:22:35,334 --> 01:22:36,369
Eat up! Eat up!

262
01:22:57,174 --> 01:22:58,653
Sober up, whore!

263
01:23:20,254 --> 01:23:21,323
<i>No!</i>

264
01:23:24,174 --> 01:23:25,607
<i>Leave me aIone!</i>

265
01:23:31,614 --> 01:23:33,570
Don't let him by.

266
01:23:36,494 --> 01:23:38,485
What do you want? Go back.

267
01:23:47,534 --> 01:23:48,284
Knife him!

