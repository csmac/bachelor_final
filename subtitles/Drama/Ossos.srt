﻿1
00:01:09,636 --> 00:01:13,037
BONES

2
00:04:02,609 --> 00:04:04,702
It's Clotilde. Good morning.

3
00:05:38,371 --> 00:05:39,963
Bastard.

4
00:06:36,362 --> 00:06:37,852
I came alone.

5
00:09:48,688 --> 00:09:50,781
You think he can see yet?

6
00:09:51,324 --> 00:09:53,292
He knows you already, Tina.

7
00:09:55,828 --> 00:09:57,796
Go on.
Everyone wants to see him.

8
00:24:50,155 --> 00:24:52,646
Something for the baby...

9
00:25:13,178 --> 00:25:16,170
A little something
for the baby, ma'am?

10
00:26:11,336 --> 00:26:13,201
Something for the baby...

11
00:26:35,360 --> 00:26:37,419
I haven't eaten in three days.

12
00:27:02,420 --> 00:27:05,014
Milk for the baby
and a sandwich for you.

13
00:27:17,602 --> 00:27:19,092
Nurse!

14
00:27:21,473 --> 00:27:22,963
Nurse!

15
00:30:08,973 --> 00:30:10,600
Are you the father?

16
00:30:28,927 --> 00:30:30,622
What's wrong with the baby?

17
00:30:36,901 --> 00:30:39,392
You should ask
Nurse Eduarda Gomes.

18
00:31:05,230 --> 00:31:06,925
Where's the baby?

19
00:32:19,904 --> 00:32:21,895
It's nothing. It's over.

20
00:32:23,007 --> 00:32:24,838
It was just a scare.

21
00:32:26,311 --> 00:32:28,575
Would you have a cigarette?

22
00:32:35,019 --> 00:32:36,543
You can't smoke here.

23
00:32:39,791 --> 00:32:41,418
It's my fault.

24
00:32:43,928 --> 00:32:45,862
I tried to help, but...

25
00:33:08,186 --> 00:33:10,051
The pediatrician's coming
tomorrow.

26
00:33:10,221 --> 00:33:13,281
They'll run some tests,
and then he can go home.

27
00:33:13,458 --> 00:33:15,653
Does he have to stay
in the hospital?

28
00:33:15,827 --> 00:33:17,795
Yes, for tonight.

29
00:33:35,113 --> 00:33:37,104
I'm not leaving without him.

30
00:33:40,551 --> 00:33:42,314
Don't worry. I'll be here.

31
00:33:42,487 --> 00:33:44,455
I was leaving, but I'll stay.

32
00:33:46,391 --> 00:33:48,222
I didn't know his name.

33
00:33:48,393 --> 00:33:51,328
I put down "Zé."
It was all I could think of.

34
00:33:53,264 --> 00:33:55,698
I didn't know your last name.
I put down mine.

35
00:33:55,933 --> 00:33:58,925
I said it was bad milk.
It'll be fine.

36
00:34:02,507 --> 00:34:04,134
Are you hungry?

37
00:34:17,321 --> 00:34:18,948
You shouldn't bother.

38
00:34:27,932 --> 00:34:29,365
Eat slowly.

39
00:34:29,534 --> 00:34:31,263
My stomach hurts.

40
00:34:32,703 --> 00:34:34,864
Is it true you haven't eaten
in three days?

41
00:34:35,039 --> 00:34:37,439
No real meals, but I do eat.

42
00:34:40,344 --> 00:34:43,677
- Shouldn't you make a phone call?
- No need.

43
00:34:44,415 --> 00:34:46,508
If it was me, I'd be worried.

44
00:34:48,186 --> 00:34:49,847
Want me to call?

45
00:35:05,236 --> 00:35:07,033
Maybe you should be going.

46
00:35:07,405 --> 00:35:09,373
My husband's due back soon.

47
00:35:11,209 --> 00:35:14,303
If something happens to the baby,
it'll be your fault.

48
00:35:40,104 --> 00:35:42,572
You can't go in.
Father doesn't want you to.

49
00:35:58,156 --> 00:36:01,387
Your body wants this lovely thing.

50
00:36:07,632 --> 00:36:09,793
What's wrong?

51
00:36:20,545 --> 00:36:23,742
Let me smell
your warm body.

52
00:36:25,216 --> 00:36:27,446
Don't you like it?

53
00:38:18,596 --> 00:38:21,087
I didn't ask Eduarda.
I don't want to know.

54
00:38:25,936 --> 00:38:27,699
I'll look the other way.

55
00:38:32,576 --> 00:38:34,271
Don't come back here.

56
00:39:52,623 --> 00:39:54,420
Is it your only child?

57
00:40:03,067 --> 00:40:04,432
What about the mother?

58
00:40:09,573 --> 00:40:11,507
She doesn't care.

59
00:40:17,648 --> 00:40:19,616
Don't you live with her?

60
00:40:24,822 --> 00:40:26,881
You could have helped her.

61
00:44:09,079 --> 00:44:10,512
Clotilde!

62
00:44:14,451 --> 00:44:15,918
Listen to me!

63
00:44:17,855 --> 00:44:19,880
Your children haven't eaten.

64
00:44:23,727 --> 00:44:26,560
Am I supposed to wait
all night for you?

65
00:45:10,040 --> 00:45:12,099
The evil eye is upon you.

66
00:45:12,910 --> 00:45:15,003
It's over now.

67
00:45:15,179 --> 00:45:17,647
You must pass it on to me.

68
00:46:41,398 --> 00:46:43,332
Let me go now. I'm all right.

69
00:48:05,115 --> 00:48:08,243
<i>Open up.</i>
<i>I know you're in there.</i>

70
00:48:32,743 --> 00:48:34,210
Go away.

71
00:49:05,442 --> 00:49:07,273
You want to kill her?

72
00:49:07,778 --> 00:49:09,575
I don't want anything.

73
00:49:12,249 --> 00:49:14,410
You like it here?

74
00:49:14,851 --> 00:49:16,409
Nice and comfy?

75
00:49:17,254 --> 00:49:19,245
Tina does as she pleases.

76
00:49:21,224 --> 00:49:25,126
She can't eat or sleep.
She keeps seeing the baby.

77
00:49:26,463 --> 00:49:28,431
The baby's fine where it is.

78
00:49:28,799 --> 00:49:30,391
Without its mother?

79
00:49:38,041 --> 00:49:39,508
Dead.

80
00:49:55,625 --> 00:49:57,388
She doesn't believe that.

81
00:49:59,296 --> 00:50:01,287
She can't forget.

82
00:50:03,633 --> 00:50:05,362
You're both the same.

83
00:50:20,851 --> 00:50:22,478
This is Clotilde.

84
00:50:23,253 --> 00:50:26,780
I'm sorry. I was just leaving.
I was his neighbor.

85
00:50:26,957 --> 00:50:30,120
Never mind. It's all right.
I'm going out.

86
00:50:31,194 --> 00:50:34,595
Emergency is packed.
A train jumped the tracks.

87
00:50:35,232 --> 00:50:37,200
I just came to let you know.

88
00:50:38,602 --> 00:50:40,570
I don't know when I'll be back.

89
00:50:41,304 --> 00:50:43,431
There's food if you get hungry.

90
00:50:45,642 --> 00:50:49,305
People are still trapped in the cars.
What can you do?

91
00:50:54,418 --> 00:50:57,751
If anything happens to Tina,
I'll kill you.

92
00:51:56,780 --> 00:51:59,010
Tina, it's Clotilde.

93
00:53:21,364 --> 00:53:23,332
It's Clotilde. Good morning.

94
00:54:04,774 --> 00:54:07,038
Want to help me
with the dishes?

95
01:00:16,512 --> 01:00:17,945
Maura!

96
01:00:33,830 --> 01:00:35,764
<i>I'm a cleaning lady.</i>

97
01:00:36,933 --> 01:00:38,833
<i>I'm a cleaning lady.</i>

98
01:00:41,638 --> 01:00:43,572
<i>I work days.</i>

99
01:00:45,508 --> 01:00:47,408
<i>I work days.</i>

100
01:00:53,516 --> 01:00:56,383
<i>Is Nurse Eduarda Gomes in?</i>

101
01:00:57,720 --> 01:01:01,053
<i>Hello.</i>
<i>Nurse Eduarda Gomes?</i>

102
01:01:03,893 --> 01:01:05,588
<i>I'm Clotilde.</i>

103
01:01:06,929 --> 01:01:08,726
<i>I know. I remember you.</i>

104
01:01:09,332 --> 01:01:12,267
<i>I'm a cleaning lady.</i>
<i>I need work.</i>

105
01:01:13,336 --> 01:01:16,100
<i>If you need a hand at home,</i>

106
01:01:16,706 --> 01:01:19,368
<i>I can cook and do laundry.</i>

107
01:01:20,376 --> 01:01:24,335
<i>A couple of hours.</i>
<i>Monday mornings or afternoons.</i>

108
01:01:26,282 --> 01:01:30,082
<i>Mondays and Fridays,</i>
<i>from 9.;00 to noon?</i>

109
01:01:31,788 --> 01:01:33,346
<i>Thank you.</i>

110
01:02:42,325 --> 01:02:44,350
Waiting for me, Tina?

111
01:02:47,396 --> 01:02:49,364
Where'd your belly go?

112
01:02:51,467 --> 01:02:54,027
Lost in the road,
or gone to the moon?

113
01:03:27,503 --> 01:03:29,971
What are you doing
to Clotilde?

114
01:03:30,373 --> 01:03:35,174
I live between her legs,
but I'll live between yours, if you like.

115
01:04:04,273 --> 01:04:07,299
I've been waiting.
What took you so long?

116
01:04:09,478 --> 01:04:11,673
I always take the 6:30 bus.

117
01:04:12,114 --> 01:04:16,016
If you're not here by 6:30,
I start thinking bad things.

118
01:04:16,552 --> 01:04:18,144
Like what?

119
01:04:18,321 --> 01:04:21,950
Like you had an accident
and got crushed under a train.

120
01:04:24,060 --> 01:04:26,460
That's stuff you watch
on television.

121
01:04:27,063 --> 01:04:29,190
You're gonna have plenty of work.

122
01:04:29,365 --> 01:04:32,926
In a lovely apartment,
working for a very nice lady.

123
01:04:35,104 --> 01:04:36,594
It'll be great.

124
01:05:16,946 --> 01:05:19,346
<i>Turn it off.</i>

125
01:05:29,725 --> 01:05:31,488
What's wrong with you?

126
01:05:33,329 --> 01:05:34,921
You too?

127
01:05:35,264 --> 01:05:37,232
I'm not afraid of you.

128
01:05:37,800 --> 01:05:40,064
You want to rob me? Kill me?

129
01:05:52,081 --> 01:05:54,276
I never asked
for anything for myself.

130
01:05:54,850 --> 01:05:56,943
I wish we could go back.

131
01:05:57,486 --> 01:05:59,477
You and the baby
mean a lot to me.

132
01:05:59,655 --> 01:06:02,624
What do you care?
Did you even notice he's not here?

133
01:06:06,762 --> 01:06:09,595
What did you do with him?
Where is he?

134
01:06:10,766 --> 01:06:12,495
You didn't want to buy him.

135
01:06:15,771 --> 01:06:19,036
I'll give you the money
and find someone to look after him.

136
01:06:19,608 --> 01:06:21,838
Your neighbor Clotilde.

137
01:06:22,244 --> 01:06:23,939
I already found someone.

138
01:07:05,755 --> 01:07:07,222
It's Tina.

139
01:07:18,234 --> 01:07:19,963
Clotilde sent me.

140
01:07:22,204 --> 01:07:23,933
She's sick.

141
01:08:19,562 --> 01:08:21,052
"Clotilde...

142
01:08:21,530 --> 01:08:24,624
please vacuum
the living room and bedroom.

143
01:08:24,800 --> 01:08:29,396
The clothes... in the...
washing machine

144
01:08:30,739 --> 01:08:32,639
need to be hung out.

145
01:08:32,808 --> 01:08:34,673
Give the kitchen a good scrub.

146
01:08:34,844 --> 01:08:38,371
Here's for three hours...
1,800 escudos.

147
01:08:38,547 --> 01:08:40,742
Thanks, Eduarda."

148
01:09:45,848 --> 01:09:47,281
Go away.

149
01:09:47,550 --> 01:09:49,575
I'm tired and want to sleep.

150
01:09:50,653 --> 01:09:52,678
Could I stay with you a while?

151
01:10:12,975 --> 01:10:14,465
You got money?

152
01:10:14,643 --> 01:10:17,237
Not to do anything.
Just to sleep a bit.

153
01:10:44,873 --> 01:10:46,306
Breathe!

154
01:10:50,980 --> 01:10:52,345
Breathe!

155
01:11:17,906 --> 01:11:19,396
Does it burn?

156
01:11:22,077 --> 01:11:24,841
No. I can see fine.

157
01:11:25,848 --> 01:11:28,817
Light hurts.
It's the nerve coming around.

158
01:11:30,953 --> 01:11:32,750
It doesn't hurt.

159
01:11:37,059 --> 01:11:38,526
Can you see?

160
01:11:39,662 --> 01:11:41,186
Now it hurts.

161
01:11:43,866 --> 01:11:46,198
You feel nauseous,
like throwing up?

162
01:11:50,773 --> 01:11:52,434
Does your chest hurt?

163
01:11:54,410 --> 01:11:56,037
Nothing hurts?

164
01:12:03,652 --> 01:12:05,381
No, I'm fine.

165
01:12:13,495 --> 01:12:15,326
What's your name?

166
01:12:21,170 --> 01:12:23,263
Clotilde sent me.

167
01:12:25,574 --> 01:12:27,303
She couldn't come.

168
01:12:29,278 --> 01:12:30,745
She's sick.

169
01:12:58,040 --> 01:13:00,167
Doesn't he have a mother?

170
01:13:01,410 --> 01:13:03,275
She looks like you.

171
01:13:04,179 --> 01:13:06,306
Is that why you're after me?

172
01:13:10,352 --> 01:13:14,083
Do I remind you of her?
Turns you on, does it?

173
01:13:17,192 --> 01:13:20,958
Take him home to her and come back.
I won't charge extra.

174
01:13:23,766 --> 01:13:25,290
I won't run away.

175
01:13:26,535 --> 01:13:28,264
Maybe you could buy him.

176
01:13:29,805 --> 01:13:31,534
You're selling him?

177
01:13:33,375 --> 01:13:36,902
Idiot. Sell him to me?

178
01:13:41,650 --> 01:13:43,345
He likes you.

179
01:13:47,089 --> 01:13:48,613
They're cold.

180
01:14:23,125 --> 01:14:24,592
Estrela de Africa.

181
01:14:35,137 --> 01:14:37,162
You must still feel weak.

182
01:14:44,980 --> 01:14:46,914
You shouldn't go like this.

183
01:14:49,017 --> 01:14:51,417
You could stay
at my place tonight.

184
01:14:57,626 --> 01:14:59,594
Here's for your three hours...

185
01:15:01,797 --> 01:15:03,424
and a little extra.

186
01:15:05,501 --> 01:15:07,230
I have a bus pass.

187
01:15:54,783 --> 01:15:56,580
Are you waiting for Tina?

188
01:16:21,276 --> 01:16:22,766
That's not her.

189
01:16:32,354 --> 01:16:33,821
That isn't either.

190
01:16:38,527 --> 01:16:39,994
Or that either.

191
01:16:42,998 --> 01:16:45,091
Maybe she isn't coming back.

192
01:17:03,719 --> 01:17:07,280
If you like, I'll keep him,
but I don't like you.

193
01:17:10,025 --> 01:17:12,994
I'll keep him, but I don't want
to ever see you again.

194
01:17:13,161 --> 01:17:15,629
And I don't want you
to ever see him again.

195
01:17:18,266 --> 01:17:19,858
Yes or no?

196
01:17:31,013 --> 01:17:32,947
So now you want to buy him?

197
01:17:36,652 --> 01:17:38,779
Where would you get the money?

198
01:18:06,782 --> 01:18:08,249
Yes or no?

199
01:19:09,811 --> 01:19:11,438
You know where Tina lives?

200
01:21:04,726 --> 01:21:06,626
Give her a kiss.

201
01:21:09,397 --> 01:21:11,865
Give her a kiss.
She remembers you.

202
01:21:23,578 --> 01:21:25,569
We don't need anything.

203
01:22:59,374 --> 01:23:02,104
- Clotilde.
- Clotilde isn't here.

204
01:23:02,277 --> 01:23:05,337
Tell her I'm not letting her
in the house tonight.

205
01:23:06,414 --> 01:23:07,881
You hear me, Clotilde?

206
01:23:08,049 --> 01:23:10,950
I know. The moon says
you should sleep alone tonight.

207
01:23:12,921 --> 01:23:14,946
Tell her to take her kids too.

208
01:23:15,123 --> 01:23:18,456
What good is she
if she can't feed them?

209
01:23:23,498 --> 01:23:25,557
Clotilde cleaned house for me.

210
01:23:27,535 --> 01:23:31,596
She must have left it a real mess,
if it's like ours.

211
01:23:47,222 --> 01:23:49,122
I'm very fond of Clotilde.

212
01:23:56,564 --> 01:23:59,328
I'd like to keep her on.

213
01:23:59,601 --> 01:24:02,297
She's all yours.
You're welcome to her.

214
01:24:03,271 --> 01:24:04,670
For all I care...

215
01:24:08,843 --> 01:24:11,004
She even sleeps at Tina's now.

216
01:24:14,049 --> 01:24:15,778
I'm fond of Tina.

217
01:24:18,653 --> 01:24:21,622
You know,
you even look a bit like...

218
01:24:23,391 --> 01:24:25,382
You remind me of Tina.

219
01:24:26,995 --> 01:24:28,519
Don't you?

220
01:24:42,477 --> 01:24:44,035
It's the eyes.

221
01:24:46,347 --> 01:24:48,315
And she's skinny like you.

222
01:24:51,486 --> 01:24:53,283
Tina's very sick.

223
01:24:53,788 --> 01:24:56,279
That's not why you look alike.

224
01:24:57,559 --> 01:24:59,527
You're not sick like her.

225
01:25:04,032 --> 01:25:07,468
A person sick like that
looks like no one else.

226
01:25:13,575 --> 01:25:15,839
Like no one else.

227
01:25:30,592 --> 01:25:32,457
<i>This is our home.</i>

228
01:25:33,628 --> 01:25:35,220
<i>Isn't Clotilde coming?</i>

229
01:25:35,396 --> 01:25:37,887
<i>Forget Tina and Clotilde.</i>

230
01:25:38,066 --> 01:25:40,864
<i>Those two bitches</i>
<i>are a waste of time.</i>

231
01:25:42,537 --> 01:25:46,098
<i>You're skinny,</i>
<i>but you still turn me on.</i>

232
01:25:48,877 --> 01:25:51,141
<i>What's your name?</i>

233
01:25:52,180 --> 01:25:54,045
<i>Eduarda.</i>

234
01:26:04,659 --> 01:26:07,184
You can't go in.
Father doesn't want you to.

235
01:27:30,011 --> 01:27:31,979
Tina, I'll go alone today.

236
01:28:34,542 --> 01:28:37,010
It's Clotilde. Good morning.

237
01:30:37,565 --> 01:30:40,090
Her child should be mine.

