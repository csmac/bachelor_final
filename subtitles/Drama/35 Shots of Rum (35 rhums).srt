1
00:02:09,820 --> 00:02:14,180
Thirty-Five Shots

2
00:06:23,860 --> 00:06:25,220
I'll take this one.

3
00:07:41,940 --> 00:07:43,620
You smell like cigarettes.

4
00:07:45,900 --> 00:07:47,380
I smoked again.

5
00:08:05,100 --> 00:08:07,980
There's tomato and lettuce.
Want a salad?

6
00:08:08,540 --> 00:08:11,220
Sure, if you want. That's fine.

7
00:08:30,500 --> 00:08:31,780
It's nice!

8
00:08:33,860 --> 00:08:35,740
It's great you remembered.

9
00:09:40,660 --> 00:09:41,940
It's ready.

10
00:09:44,500 --> 00:09:45,940
I'll hurry!

11
00:10:11,140 --> 00:10:12,460
Just right.

12
00:10:35,660 --> 00:10:36,820
Some more?

13
00:10:37,540 --> 00:10:38,540
Okay?

14
00:10:46,060 --> 00:10:47,700
Thank you, Daddy.

15
00:12:34,620 --> 00:12:36,700
Fucking pains in the ass!

16
00:12:44,860 --> 00:12:46,220
Hey, daddy-o.

17
00:12:46,700 --> 00:12:47,980
I got back late.

18
00:12:51,740 --> 00:12:53,140
Move it, fatso.

19
00:13:05,820 --> 00:13:06,740
Coming!

20
00:14:48,980 --> 00:14:50,700
Today's not my day.

21
00:14:51,140 --> 00:14:54,540
Couldn't someone need
to go to the airport?

22
00:14:55,700 --> 00:14:58,300
Nope. Short, local rides.

23
00:14:58,580 --> 00:15:00,420
Taxi drivers always complain.

24
00:15:00,580 --> 00:15:02,860
If you're not happy, change jobs!

25
00:15:03,020 --> 00:15:05,820
It's what's called "flexibility."

26
00:15:06,140 --> 00:15:09,580
Not at all, sir.
I love my job, believe me.

27
00:15:10,180 --> 00:15:11,900
Never the same thing.

28
00:15:13,220 --> 00:15:15,180
I'm happy behind the wheel.

29
00:15:16,940 --> 00:15:18,860
No boss breathing down my neck.

30
00:15:19,220 --> 00:15:21,500
I meet interesting people.

31
00:15:21,940 --> 00:15:23,700
Okay, whatever...

32
00:15:24,740 --> 00:15:25,940
Keep the change.

33
00:15:27,020 --> 00:15:28,620
Want my card, just in case?

34
00:15:28,780 --> 00:15:30,940
You never know
when you need a driver.

35
00:15:35,140 --> 00:15:37,620
- Have a nice day.
- You too.

36
00:15:42,700 --> 00:15:46,780
Explain yourself. You say
either too much or not enough.

37
00:15:47,500 --> 00:15:48,500
What I mean is...

38
00:15:48,660 --> 00:15:52,860
debt is a way
of dominating the Global South.

39
00:15:53,700 --> 00:15:58,100
Creditors have the privilege
of deciding the rules of the game.

40
00:15:58,220 --> 00:15:59,540
Rules...

41
00:16:00,780 --> 00:16:03,700
used to manage
international debt load

42
00:16:03,820 --> 00:16:06,180
and imposed on countries in debt.

43
00:16:08,780 --> 00:16:11,820
I don't think
we can ignore Stiglitz.

44
00:16:13,540 --> 00:16:16,540
You say it
as if it's totally self-evident...

45
00:16:17,780 --> 00:16:19,740
It's a little pedantic.

46
00:16:19,860 --> 00:16:21,700
Annoying for your classmates.

47
00:16:21,860 --> 00:16:26,460
Unlike you, they didn't have
to prepare today's class.

48
00:16:26,580 --> 00:16:29,660
If you could be less pedantic,
it would be nice.

49
00:16:34,540 --> 00:16:37,180
Yesterday on the radio
someone from an NGO

50
00:16:37,660 --> 00:16:40,100
was discussing
the power of the weak.

51
00:16:40,220 --> 00:16:42,780
But you're saying there's no hope.

52
00:16:42,820 --> 00:16:45,060
You're not here to hope or despair,

53
00:16:45,620 --> 00:16:47,820
but to develop critical faculties,

54
00:16:47,980 --> 00:16:50,620
rhetorical skills,
analytical frameworks.

55
00:16:50,780 --> 00:16:52,500
Conceptualize your ideas.

56
00:16:52,740 --> 00:16:55,380
That's what we're trying to achieve.

57
00:16:55,500 --> 00:16:57,460
Josephine, answer.

58
00:16:59,740 --> 00:17:01,740
It's neither right nor wrong.

59
00:17:01,860 --> 00:17:03,500
You're begging the question.

60
00:17:03,620 --> 00:17:07,540
Debt can be discussed
without getting all emotional.

61
00:17:08,780 --> 00:17:13,060
It should be discussed
precisely, rigorously, technically.

62
00:17:13,820 --> 00:17:15,420
Just technically?

63
00:17:15,580 --> 00:17:16,780
I disagree.

64
00:17:16,940 --> 00:17:19,980
Stiglitz, Brenton Wood
and the IMF are fine.

65
00:17:20,100 --> 00:17:23,620
But debts are relative.
Which debt do we mean?

66
00:17:23,780 --> 00:17:26,420
The Global South
still owes the North.

67
00:17:26,540 --> 00:17:27,860
But I have a question.

68
00:17:28,380 --> 00:17:31,660
Why not demand restitution
for the slave trade?

69
00:17:31,700 --> 00:17:34,780
Debt, the IMF, is always us.
Always the same.

70
00:17:34,940 --> 00:17:37,060
Blacks get flushed down the drain.

71
00:17:37,660 --> 00:17:39,660
I think the problem is systemic.

72
00:17:41,900 --> 00:17:43,820
The system must be changed.

73
00:17:45,180 --> 00:17:46,980
As Frantz Fanon says...

74
00:17:48,820 --> 00:17:51,140
You should read him. It can't hurt.

75
00:17:52,740 --> 00:17:56,820
When we revolt,
it's not for a particular culture.

76
00:17:57,660 --> 00:18:01,180
We revolt simply...
because, for many reasons...

77
00:18:01,660 --> 00:18:03,220
we can no longer breathe.

78
00:19:55,300 --> 00:19:57,140
You can't fall apart now.

79
00:19:59,020 --> 00:20:00,580
Not in front of everyone.

80
00:20:07,020 --> 00:20:10,260
Good luck, Ren�. Happy retirement.
You deserve it.

81
00:20:10,420 --> 00:20:11,620
Let's hear it for Ren�!

82
00:20:13,100 --> 00:20:14,420
Here's to you!

83
00:20:18,580 --> 00:20:20,300
From the girls.

84
00:20:33,420 --> 00:20:35,260
And now from your brothers!

85
00:20:37,500 --> 00:20:39,260
We picked it out ourselves.

86
00:21:00,420 --> 00:21:01,540
We're not done.

87
00:21:15,500 --> 00:21:18,220
All your music at your fingertips.

88
00:21:25,820 --> 00:21:28,260
You can listen to FM Tropical.

89
00:21:30,340 --> 00:21:31,340
To Ren�!

90
00:21:36,260 --> 00:21:37,220
Cheers!

91
00:21:42,980 --> 00:21:44,420
My friends...

92
00:21:44,980 --> 00:21:46,860
Thank you all for being here.

93
00:21:50,700 --> 00:21:52,540
I've waited so long for this.

94
00:21:55,260 --> 00:21:57,420
It's a deliverance, you know.

95
00:22:09,980 --> 00:22:10,940
You okay?

96
00:22:17,700 --> 00:22:19,620
Tonight I feel like...

97
00:22:22,500 --> 00:22:23,780
I have wings.

98
00:22:27,780 --> 00:22:29,340
- To you!
- Another round.

99
00:22:29,580 --> 00:22:30,700
To all of you!

100
00:22:30,860 --> 00:22:32,460
Another round!

101
00:22:41,860 --> 00:22:43,300
I've had enough.

102
00:22:47,620 --> 00:22:49,220
Down the hatch!

103
00:22:49,780 --> 00:22:52,220
14, 15...

104
00:22:57,260 --> 00:22:58,220
19.

105
00:22:58,620 --> 00:23:01,220
Your turn, Lionel. Go for it.

106
00:23:03,260 --> 00:23:05,300
The time's right for the 35 shots.

107
00:23:06,300 --> 00:23:09,620
What's this thing... about 35 shots?

108
00:23:14,140 --> 00:23:15,540
It's an old story.

109
00:23:16,740 --> 00:23:18,380
But I won't drink them.

110
00:23:19,900 --> 00:23:21,380
Not tonight.

111
00:23:51,740 --> 00:23:53,140
My stop.

112
00:23:55,100 --> 00:23:56,300
See you tomorrow.

113
00:24:18,540 --> 00:24:19,820
What were you saying?

114
00:24:20,860 --> 00:24:21,900
Surrendering...

115
00:24:24,860 --> 00:24:28,700
Surrendering to this condition
is what's so hard.

116
00:24:34,860 --> 00:24:36,820
I'd like to have died young.

117
00:24:37,460 --> 00:24:39,460
But I'm at the age I'm at.

118
00:24:42,700 --> 00:24:44,700
And healthy as an ox.

119
00:24:46,380 --> 00:24:48,660
I'll die at 100, at this rate.

120
00:25:00,300 --> 00:25:02,340
I don't have this life in me.

121
00:25:05,860 --> 00:25:07,700
The subway and all that...

122
00:25:12,540 --> 00:25:14,700
It hit me unarmed and unprepared.

123
00:26:27,340 --> 00:26:29,900
- Were you playing soccer?
- Yes, Detective.

124
00:26:32,420 --> 00:26:34,740
Your dad's not back. No lights on.

125
00:26:34,860 --> 00:26:36,580
He had the party for Ren�.

126
00:26:37,180 --> 00:26:38,820
He'll probably stay late.

127
00:26:47,300 --> 00:26:49,340
Wort you come over for a bite?

128
00:26:49,460 --> 00:26:51,500
Thanks, Gab. I can't tonight.

129
00:26:51,660 --> 00:26:53,620
There's yesterday's pork roast.

130
00:26:53,740 --> 00:26:56,460
I have exams. I can't stay up late.

131
00:26:58,580 --> 00:27:00,580
It was easier when you were a kid.

132
00:27:01,740 --> 00:27:03,860
You'd fall asleep on the table,

133
00:27:03,900 --> 00:27:05,900
then I'd take you into my bed.

134
00:28:01,420 --> 00:28:04,420
I'll buy my ticket tomorrow.
You two coming?

135
00:28:04,780 --> 00:28:06,460
I don't know. Any good?

136
00:28:06,580 --> 00:28:09,100
If we wait too long,
it'll sell out.

137
00:28:09,540 --> 00:28:11,940
I was taking my shower.
Excuse me...

138
00:28:12,220 --> 00:28:14,660
- Your dad will want to.
- For sure!

139
00:28:16,820 --> 00:28:17,820
Good night.

140
00:28:34,340 --> 00:28:35,260
Shit.

141
00:28:56,740 --> 00:28:59,300
<i>To all residents:
Please be reminded</i>

142
00:28:59,420 --> 00:29:02,300
<i>bikes belong in the shed.
Thanks in advance.</i>

143
00:29:07,860 --> 00:29:10,220
Don't wait up for him all night!

144
00:30:54,220 --> 00:30:55,860
You look sort of sloshed.

145
00:31:02,340 --> 00:31:04,340
Fighting fire with fire.

146
00:32:26,860 --> 00:32:27,940
Come closer.

147
00:32:39,740 --> 00:32:41,620
There's something I want.

148
00:32:43,380 --> 00:32:45,780
Don't feel I need
to be looked after.

149
00:32:51,740 --> 00:32:53,740
I want you to think of yourself.

150
00:32:53,860 --> 00:32:55,820
You're getting rid of me.

151
00:32:56,900 --> 00:32:58,380
You don't love me anymore.

152
00:33:12,780 --> 00:33:14,460
Don't be sad.

153
00:33:17,620 --> 00:33:18,820
I love you.

154
00:33:22,540 --> 00:33:23,580
Daddy...

155
00:33:42,300 --> 00:33:43,660
Just feel free.

156
00:34:16,660 --> 00:34:17,540
Jo!

157
00:34:18,540 --> 00:34:19,700
I'm coming!

158
00:34:46,980 --> 00:34:48,980
- Faster?
- If you want.

159
00:35:02,820 --> 00:35:04,180
Nice and warm.

160
00:35:05,900 --> 00:35:07,340
Stupid ass!

161
00:35:24,860 --> 00:35:26,260
You're crazy!

162
00:35:26,740 --> 00:35:28,100
Sicko!

163
00:36:20,500 --> 00:36:21,940
I'm out of milk.

164
00:36:22,260 --> 00:36:23,660
Not a problem.

165
00:36:35,780 --> 00:36:37,300
Where to, this time?

166
00:36:37,940 --> 00:36:40,660
If you really care,
come along and you'll see.

167
00:37:09,380 --> 00:37:11,260
Didn't you say
you'd sell everything?

168
00:37:14,580 --> 00:37:16,900
It's not suffocating, living here?

169
00:37:18,420 --> 00:37:20,540
It's all that's left of my parents.

170
00:37:21,620 --> 00:37:24,460
A table, some furniture.
What the hell do I care?

171
00:37:29,900 --> 00:37:32,820
Like with this apartment.
I don't get attached.

172
00:37:38,660 --> 00:37:40,300
What's holding you back?

173
00:37:42,460 --> 00:37:44,500
I don't know.
I must have my reasons.

174
00:38:21,820 --> 00:38:23,140
Here are the keys.

175
00:38:23,900 --> 00:38:25,940
I bet you haven't eaten a thing.

176
00:38:26,940 --> 00:38:29,100
You redid the place?

177
00:38:29,260 --> 00:38:32,220
It was Jo. She repainted.

178
00:38:33,740 --> 00:38:34,940
It's nice.

179
00:38:38,380 --> 00:38:40,220
Give him a plate.

180
00:38:41,140 --> 00:38:42,900
I'll stay for a few minutes.

181
00:38:53,020 --> 00:38:54,980
Taking your car or the subway?

182
00:38:56,940 --> 00:38:58,420
I'll take my car.

183
00:38:58,940 --> 00:39:00,700
I'll leave it in the lot.

184
00:39:27,500 --> 00:39:28,740
No� is something!

185
00:39:28,900 --> 00:39:32,060
His old furniture,
his old cat, his trips...

186
00:39:33,500 --> 00:39:35,220
You think he's happy?

187
00:39:35,860 --> 00:39:39,020
- I think he's lonely.
- Don't overdo it.

188
00:39:42,140 --> 00:39:43,260
He has us.

189
00:39:54,100 --> 00:39:55,460
We have everything here.

190
00:39:56,780 --> 00:39:58,540
Why go looking elsewhere?

191
00:40:15,540 --> 00:40:16,860
Lionel!

192
00:41:32,460 --> 00:41:34,820
Happy to have the place to yourself?

193
00:41:43,620 --> 00:41:46,940
<i>Welcome to the Free Zone</i>

194
00:41:53,140 --> 00:41:55,500
<i>Anthropology on Strike</i>

195
00:42:07,820 --> 00:42:09,500
Move... Don't just stand there.

196
00:42:10,020 --> 00:42:11,140
It's blocked!

197
00:42:13,260 --> 00:42:14,820
Let us through!

198
00:44:01,020 --> 00:44:03,500
<i>Working late. Jo.</i>

199
00:44:29,420 --> 00:44:31,260
Lina, Th�o...

200
00:44:32,220 --> 00:44:33,420
My colleagues.

201
00:44:51,420 --> 00:44:53,540
You're quiet. Upset I'm here?

202
00:44:54,300 --> 00:44:55,580
The less you know,

203
00:44:56,220 --> 00:44:57,540
the better I feel.

204
00:45:08,180 --> 00:45:09,900
Your friend's here.

205
00:45:11,860 --> 00:45:13,140
Hey, rascal.

206
00:45:13,940 --> 00:45:15,380
Remember Gabrielle?

207
00:45:18,140 --> 00:45:20,260
I never forget a beautiful woman.

208
00:45:21,500 --> 00:45:23,500
Let me buy you a drink.

209
00:45:23,980 --> 00:45:26,460
No thanks, Ren�. I'm on the job.

210
00:45:27,140 --> 00:45:29,100
I thought you left the city.

211
00:45:30,980 --> 00:45:31,980
For where?

212
00:45:32,140 --> 00:45:34,300
I don't know. Far away somewhere.

213
00:45:37,580 --> 00:45:40,820
It's best that I go.
I'll leave you.

214
00:45:43,100 --> 00:45:44,420
So long, Gabrielle.

215
00:45:46,860 --> 00:45:48,220
Take care.

216
00:45:54,500 --> 00:45:56,020
Got any plans?

217
00:45:57,380 --> 00:45:58,340
Tons.

218
00:46:12,820 --> 00:46:14,220
You lent it to me.

219
00:46:24,980 --> 00:46:26,500
I forgot all about it.

220
00:46:41,100 --> 00:46:43,060
I'm not well. I'll walk.

221
00:48:00,300 --> 00:48:02,220
They're closing Anthropology.

222
00:48:03,260 --> 00:48:05,380
They claim it serves no purpose.

223
00:48:08,140 --> 00:48:09,460
My name's Ruben.

224
00:48:10,820 --> 00:48:12,060
Josephine.

225
00:48:16,980 --> 00:48:18,100
See you soon.

226
00:48:20,580 --> 00:48:21,940
Coming to the concert?

227
00:48:23,020 --> 00:48:24,580
I don't know, maybe.

228
00:48:37,900 --> 00:48:39,060
Waiting for me?

229
00:48:39,300 --> 00:48:41,260
I don't like these night hours.

230
00:48:42,220 --> 00:48:44,620
I worry when you come home so late.

231
00:48:46,700 --> 00:48:48,580
You should quit this job.

232
00:49:04,940 --> 00:49:06,860
I enjoy riding like this.

233
00:49:11,860 --> 00:49:12,860
Me too.

234
00:49:35,740 --> 00:49:36,980
Why do you do this?

235
00:49:38,500 --> 00:49:40,500
I don't know. I don't mind.

236
00:49:42,980 --> 00:49:44,740
You can't wear wrinkled shirts.

237
00:49:44,900 --> 00:49:47,420
It bothers me. I prefer to do it.

238
00:49:47,700 --> 00:49:49,740
- Is it the right one?
- I'll do it.

239
00:49:49,900 --> 00:49:51,700
Okay, do it then.

240
00:49:51,900 --> 00:49:53,180
And what's that thing?

241
00:49:57,900 --> 00:50:00,900
What's he up to?
He knows the traffic's bad.

242
00:50:02,140 --> 00:50:03,900
Hurry up, Lionel!

243
00:50:04,020 --> 00:50:05,460
We're ready and waiting.

244
00:50:06,220 --> 00:50:07,180
I'm coming.

245
00:50:17,740 --> 00:50:20,140
You like to be last.
You can't help it.

246
00:50:20,300 --> 00:50:21,500
Got your wallet?

247
00:50:23,300 --> 00:50:25,740
We're not late.
No� isn't even here.

248
00:50:31,020 --> 00:50:34,540
I got tickets for the concert.
I thought you'd like it.

249
00:50:37,380 --> 00:50:39,340
In fact I'm already going.

250
00:50:41,220 --> 00:50:42,860
We're about to leave.

251
00:50:45,500 --> 00:50:47,940
Wait for No�. I'll get the car.

252
00:50:56,380 --> 00:50:58,780
I have to run. I'm double parked.

253
00:50:58,940 --> 00:51:00,380
See you at the concert, maybe.

254
00:51:02,380 --> 00:51:03,780
What's his name?

255
00:51:04,500 --> 00:51:06,780
Ruben. A classmate of mine.

256
00:51:10,500 --> 00:51:11,900
Am I running behind?

257
00:51:40,060 --> 00:51:41,380
Lionel, take the tickets.

258
00:51:41,540 --> 00:51:44,500
I'll drop you off
and park in the lot.

259
00:51:49,540 --> 00:51:51,100
We're not even near.

260
00:52:21,140 --> 00:52:23,940
We haven't gone out
as a family in ages.

261
00:52:28,460 --> 00:52:31,020
Can you lower the music a little?

262
00:52:32,260 --> 00:52:33,220
We can't talk.

263
00:53:21,780 --> 00:53:24,460
- Hold on!
- Gabrielle, get behind the wheel!

264
00:53:25,460 --> 00:53:26,860
I can't believe this!

265
00:53:27,020 --> 00:53:28,540
It's no big deal.

266
00:53:28,900 --> 00:53:30,300
It'll be fine. Turn!

267
00:53:30,460 --> 00:53:33,020
Be careful! Not so fast!

268
00:53:33,140 --> 00:53:34,980
- Calm down!
- I am calm!

269
00:53:35,100 --> 00:53:37,420
- You push too hard!
- Turn left.

270
00:53:39,900 --> 00:53:42,180
I'm stranded with my friends!

271
00:53:42,300 --> 00:53:44,460
- Don't shout!
- Don't scream!

272
00:53:45,780 --> 00:53:49,780
No way!
I don't want it stripped tonight!

273
00:53:51,100 --> 00:53:52,140
What?

274
00:53:52,780 --> 00:53:54,740
You'll send a tow truck?

275
00:53:57,140 --> 00:53:58,980
Rue Papin...

276
00:54:02,700 --> 00:54:04,020
He can't hear a thing!

277
00:54:04,900 --> 00:54:06,220
How long?

278
00:54:07,900 --> 00:54:09,380
I see...

279
00:54:09,700 --> 00:54:11,700
I don't have a choice.

280
00:54:12,260 --> 00:54:13,820
We'll be waiting.

281
00:54:16,900 --> 00:54:18,140
Goodbye!

282
00:54:18,740 --> 00:54:20,940
- Cool it!
- You have to scream?

283
00:54:21,100 --> 00:54:23,100
We couldn't hear each other.

284
00:54:23,340 --> 00:54:24,940
Should I look at the motor?

285
00:54:26,100 --> 00:54:27,980
- You know motors?
- Sure.

286
00:54:28,180 --> 00:54:29,340
I'll take a look.

287
00:54:29,460 --> 00:54:31,900
You have to be
a computer specialist!

288
00:54:32,020 --> 00:54:33,420
It's up to you!

289
00:54:34,020 --> 00:54:34,900
Shit.

290
00:54:35,420 --> 00:54:36,900
I squashed your flowers.

291
00:54:39,300 --> 00:54:40,940
There's an envelope inside.

292
00:54:41,940 --> 00:54:44,060
Must be the florist's card.

293
00:54:54,180 --> 00:54:56,140
Don't look so glum.

294
00:54:56,420 --> 00:54:58,300
We'll cheer each other up.

295
00:54:59,540 --> 00:55:00,780
Will you be okay?

296
00:55:05,860 --> 00:55:08,980
Closing time.
I'll stay open 5 minutes for you.

297
00:55:09,420 --> 00:55:11,700
- A tow truck's coming.
- Last round.

298
00:55:11,860 --> 00:55:13,900
Last one, then it's finished.

299
00:55:17,780 --> 00:55:19,180
No, I'm closing.

300
00:55:19,900 --> 00:55:21,100
Sorry, it's time.

301
00:55:21,500 --> 00:55:23,380
Five minutes is enough.

302
00:55:23,500 --> 00:55:24,820
When it's time, it's time.

303
00:55:42,860 --> 00:55:44,180
Can you drop us off?

304
00:55:44,300 --> 00:55:46,860
I can't.
There's no room in the cabin.

305
00:55:48,780 --> 00:55:51,460
I think we can forget
about the concert.

306
00:55:51,860 --> 00:55:53,300
We'll take cabs home.

307
00:55:53,420 --> 00:55:56,140
- This is too sad.
- What else can we do?

308
00:55:56,300 --> 00:55:58,060
Let's go have a drink somewhere.

309
00:55:58,380 --> 00:55:59,780
I'll call a colleague.

310
00:56:10,820 --> 00:56:13,260
Pierre?
Can you send me a car?

311
00:56:13,420 --> 00:56:14,860
Yeah, I'm stuck here.

312
00:56:25,060 --> 00:56:26,860
No, we're closed.

313
00:56:28,020 --> 00:56:29,780
Come on, please!

314
00:56:37,100 --> 00:56:39,540
- This is nice of you.
- Come in.

315
00:56:40,980 --> 00:56:42,980
Thanks, you're very kind.

316
00:56:43,460 --> 00:56:44,780
We're cold.

317
00:56:45,020 --> 00:56:48,260
- What happened?
- First the car broke down.

318
00:56:48,420 --> 00:56:51,100
A tow truck came.
We missed our concert.

319
00:56:51,260 --> 00:56:53,380
We don't feel like going home
like this.

320
00:56:54,540 --> 00:56:56,940
Stop acting up.
He's my nephew.

321
00:57:06,580 --> 00:57:08,500
So you ended up opening?

322
00:57:09,460 --> 00:57:12,260
Have a seat.
I'll warm something up.

323
00:57:15,300 --> 00:57:16,780
I'll be right with you.

324
00:57:45,100 --> 00:57:46,260
Yes, Pierrot!

325
00:57:48,100 --> 00:57:49,620
You'll pick us up?

326
00:57:51,820 --> 00:57:53,780
I think it's the transmission.

327
00:57:55,620 --> 00:57:57,460
It's going to be too late.

328
00:57:57,500 --> 00:57:59,220
Forget it, Gabrielle.

329
00:57:59,740 --> 00:58:01,300
We're fine here.

330
00:58:01,900 --> 00:58:04,420
Pierrot, change of plans.
We're staying.

331
00:58:06,900 --> 00:58:09,340
I'm not sure I can work on Monday.

332
00:58:58,300 --> 00:58:59,980
You're not dancing?

333
00:59:00,100 --> 00:59:01,780
So they all stare at us?

334
00:59:13,820 --> 00:59:15,020
You're something.

335
01:05:09,420 --> 01:05:10,980
I'm out of milk.

336
01:05:39,020 --> 01:05:40,540
What's wrong?

337
01:05:41,780 --> 01:05:43,140
It's my cat.

338
01:05:46,820 --> 01:05:49,900
He died.
At 17, in his sleep.

339
01:05:51,500 --> 01:05:53,940
I'd say it's a good way to go.

340
01:06:03,900 --> 01:06:05,540
In a trash bag?

341
01:06:07,180 --> 01:06:08,620
That's awful.

342
01:06:11,100 --> 01:06:13,660
No frills. He's dead and done for.

343
01:06:36,620 --> 01:06:38,940
I think I'm going to leave here.

344
01:06:39,100 --> 01:06:41,220
I was offered a job overseas.

345
01:06:42,260 --> 01:06:44,860
Now that my cat's dead,
I'll take it.

346
01:06:46,980 --> 01:06:48,980
It's in Gabon and pays well.

347
01:06:49,340 --> 01:06:51,860
I'll sell the apartment
and that's it.

348
01:06:52,020 --> 01:06:54,180
You'll ditch us and go away?

349
01:06:55,900 --> 01:06:57,220
Why not?

350
01:06:58,500 --> 01:07:01,500
You always tell me
how it's ugly and old here.

351
01:07:04,060 --> 01:07:05,500
Are you okay?

352
01:07:05,620 --> 01:07:07,620
I have a headache. Any aspirin?

353
01:08:22,020 --> 01:08:24,020
Why are you doing housework?

354
01:08:26,820 --> 01:08:28,700
Isn't it clean enough?

355
01:09:04,940 --> 01:09:07,820
Rifling through everything
for no reason!

356
01:09:08,660 --> 01:09:11,820
Why throw that stuff away?
Just to piss me off?

357
01:09:11,940 --> 01:09:13,700
It's filthy here!

358
01:09:14,220 --> 01:09:16,020
We need to empty it all out.

359
01:09:16,660 --> 01:09:19,300
This makes no sense!
Think of yourself!

360
01:09:22,460 --> 01:09:23,780
No�'s leaving!

361
01:09:24,580 --> 01:09:27,620
I bet you already know
but I just found out.

362
01:09:28,900 --> 01:09:30,780
The Congo, someplace like that.

363
01:09:31,820 --> 01:09:33,500
No, I didn't know.

364
01:09:36,180 --> 01:09:37,780
I'm leaving too.

365
01:09:38,100 --> 01:09:39,940
I can do the same as him.

366
01:09:41,860 --> 01:09:43,620
Everyone's leaving.

367
01:09:44,980 --> 01:09:46,660
Don't be silly.

368
01:09:47,780 --> 01:09:50,620
We'll do as we please.
As we always have.

369
01:09:51,140 --> 01:09:53,660
- Nothing will change.
- Yes! Everything will.

370
01:10:00,900 --> 01:10:03,780
<i>Lionel, I miss you.
I miss you so much.</i>

371
01:10:04,060 --> 01:10:06,260
<i>Josephine finally fell asleep.</i>

372
01:10:06,700 --> 01:10:09,060
<i>She looks like you when she sleeps.</i>

373
01:10:09,340 --> 01:10:11,260
<i>It makes me love her even more.</i>

374
01:10:11,900 --> 01:10:14,860
<i>Please let me live
by your side every day.</i>

375
01:10:15,780 --> 01:10:17,260
<i>She'll be my daughter too.</i>

376
01:10:17,300 --> 01:10:19,300
<i>My love, my rascal...</i>

377
01:10:27,580 --> 01:10:30,540
FOR SALE

378
01:11:02,980 --> 01:11:05,020
You have something to tell me?

379
01:11:50,940 --> 01:11:53,060
I didn't think I'd miss it.

380
01:11:55,380 --> 01:11:57,980
Stop by whenever you want.
I enjoy it.

381
01:12:00,140 --> 01:12:03,100
When it's over, it's over.
You have to leave.

382
01:12:05,220 --> 01:12:07,700
A clean break... It's better.

383
01:12:16,820 --> 01:12:18,300
You make me feel better.

384
01:12:24,500 --> 01:12:26,580
You're not much of a talker.

385
01:12:31,100 --> 01:12:32,860
You never say a thing.

386
01:12:45,180 --> 01:12:48,020
When I get dark thoughts,
I think of my daughter.

387
01:14:06,860 --> 01:14:08,020
Fuck.

388
01:14:12,500 --> 01:14:16,140
<i>Service is interrupted
due to a technical incident.</i>

389
01:15:21,020 --> 01:15:22,580
Fuck, Ren�...

390
01:17:48,780 --> 01:17:50,220
I was hungry.

391
01:17:51,100 --> 01:17:52,220
So was I.

392
01:17:56,100 --> 01:17:57,740
It's very good, as usual.

393
01:20:47,740 --> 01:20:50,060
We see too little of each other.

394
01:20:53,500 --> 01:20:55,380
That's the way it is now.

395
01:20:57,100 --> 01:21:00,060
We all lead such withdrawn lives.

396
01:21:01,100 --> 01:21:03,300
Everyone in his corner.

397
01:21:04,180 --> 01:21:06,020
Every man for himself.

398
01:21:11,220 --> 01:21:13,420
Would you like some more coffee?

399
01:21:13,580 --> 01:21:15,500
Yes I would, ma'am.

400
01:21:16,420 --> 01:21:19,620
You don't have to be so formal!

401
01:21:19,740 --> 01:21:21,380
Yes, then.

402
01:21:24,220 --> 01:21:27,100
You must have been driving
all night.

403
01:21:29,100 --> 01:21:30,180
Melanie...

404
01:21:32,140 --> 01:21:33,420
No, thank you.

405
01:21:33,740 --> 01:21:36,140
You can stay for lunch.

406
01:21:38,380 --> 01:21:40,540
Or even spend the night.

407
01:21:42,580 --> 01:21:46,260
Thanks, but we want
to get back on the road.

408
01:21:46,660 --> 01:21:48,620
Maybe some other time.

409
01:21:49,300 --> 01:21:50,780
Some other time...

410
01:21:56,660 --> 01:22:00,500
Your mother said she fell in love
with a guy in Paris.

411
01:22:02,300 --> 01:22:04,540
I asked her: "Is he cute?"

412
01:22:06,820 --> 01:22:10,060
She looked me straight in the eyes
and said:

413
01:22:11,060 --> 01:22:12,700
"You're going to like him."

414
01:22:17,340 --> 01:22:22,820
I taught her to swim.
She was scared of the water.

415
01:22:24,180 --> 01:22:26,140
We're all scared of it.

416
01:22:27,420 --> 01:22:30,260
I'm also scared of that sea.

417
01:22:31,700 --> 01:22:33,540
So vast, so wide...

418
01:22:36,140 --> 01:22:38,740
And when you scream,
no one hears you.

419
01:22:40,700 --> 01:22:42,060
Lionel...

420
01:22:44,740 --> 01:22:47,380
do you remember that time...

421
01:22:48,460 --> 01:22:51,300
when we all went swimming together?

422
01:22:52,420 --> 01:22:54,820
We basked in the sun.

423
01:22:56,620 --> 01:22:58,660
Lost in the dunes.

424
01:23:00,380 --> 01:23:02,100
Good wine.

425
01:23:03,780 --> 01:23:06,140
Not always that good.

426
01:23:10,380 --> 01:23:12,140
You don't remember?

427
01:23:19,180 --> 01:23:21,260
Why don't we drink some wine?

428
01:23:22,060 --> 01:23:25,460
A little glass of wine now.
Why not?

429
01:23:36,620 --> 01:23:37,860
Sometimes it seems

430
01:23:39,420 --> 01:23:42,300
the whole world
is scared of suffering.

431
01:23:43,740 --> 01:23:45,660
Everyone wants

432
01:23:46,140 --> 01:23:48,700
either total stress or...

433
01:23:49,300 --> 01:23:52,820
some peace
in their happy little lives.

434
01:23:54,780 --> 01:23:57,540
But not us, not us!

435
01:23:58,340 --> 01:24:00,820
We're strong, aren't we?

436
01:24:02,780 --> 01:24:04,620
Aren't we, Lionel?

437
01:24:07,060 --> 01:24:08,300
Still so silent.

438
01:24:11,420 --> 01:24:14,940
I'm glad Josephine gets by in German.
It makes me happy.

439
01:24:17,620 --> 01:24:19,300
Just like your dad!

440
01:27:36,140 --> 01:27:37,740
I like being here with you.

441
01:27:47,500 --> 01:27:49,380
We could live like this forever.

442
01:28:13,380 --> 01:28:14,380
Let me in.

443
01:28:15,220 --> 01:28:18,180
- Who'll do her hair?
- She's doing fine alone.

444
01:28:18,500 --> 01:28:20,620
Take all this, at least.

445
01:28:23,340 --> 01:28:24,660
See you later.

446
01:30:34,180 --> 01:30:35,820
Your mother's necklace.

447
01:30:45,860 --> 01:30:47,380
Pretty, isn't it?

448
01:31:32,540 --> 01:31:34,060
28.

449
01:31:36,060 --> 01:31:37,500
29.

450
01:31:39,140 --> 01:31:40,460
30.

451
01:31:42,780 --> 01:31:44,380
It's underway.

452
01:31:45,580 --> 01:31:47,100
This annoys me.

453
01:31:47,420 --> 01:31:50,060
This 35 rum thing,
did you invent it?

454
01:31:51,540 --> 01:31:53,100
Maybe.

455
01:31:54,180 --> 01:31:55,420
31.

456
01:32:06,660 --> 01:32:08,140
32.

457
01:32:12,020 --> 01:32:13,500
A moment like this

458
01:32:14,820 --> 01:32:16,620
only happens once.

459
01:32:19,260 --> 01:32:21,340
So tonight, 35 shots.

460
01:32:25,460 --> 01:32:26,740
33.

461
01:32:30,860 --> 01:32:32,420
You're on the right track.

462
01:32:35,900 --> 01:32:38,140
Gabrielle, you're not going to cry?

463
01:32:46,060 --> 01:32:47,580
34.

464
01:36:31,180 --> 01:36:35,380
Subtitles by Andrew Litvack
DVD Subtitling: CNST, Montreal

