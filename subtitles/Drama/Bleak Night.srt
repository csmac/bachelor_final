1
00:01:15,975 --> 00:01:17,374
Fucker!

2
00:01:47,407 --> 00:01:52,640
Bleak Night

3
00:02:21,040 --> 00:02:22,473
Charge!

4
00:02:32,919 --> 00:02:35,581
Becky, come join us.

5
00:02:35,755 --> 00:02:37,848
- Just leave him.
- What sup?

6
00:02:43,396 --> 00:02:44,863
That's a fall!

7
00:02:44,864 --> 00:02:47,162
Is not, not, not!

8
00:02:53,907 --> 00:02:57,399
Yo, Becky. Come here.

9
00:03:03,883 --> 00:03:05,373
What's with him?

10
00:03:05,852 --> 00:03:07,843
Just let him be.

11
00:03:15,728 --> 00:03:18,253
- What do you mean?
- You know what.

12
00:03:18,731 --> 00:03:19,993
Know what?

13
00:03:21,201 --> 00:03:22,930
What's with you two?

14
00:03:23,303 --> 00:03:24,702
It's nothing.

15
00:03:26,773 --> 00:03:27,967
Talk to me.

16
00:03:28,441 --> 00:03:30,068
Really.

17
00:03:30,877 --> 00:03:32,344
Stop worrying.

18
00:03:32,779 --> 00:03:34,770
Why can't you talk?

19
00:03:35,415 --> 00:03:38,111
Seriously. It's nothing.

20
00:04:28,001 --> 00:04:30,970
Closed till Feb 8th due to
personal reasons

21
00:07:15,368 --> 00:07:22,137
Wanted to meet you in person
and ask about...

22
00:07:23,409 --> 00:07:25,969
No, there's no problem.

23
00:07:27,046 --> 00:07:29,674
I just stopped by the school.

24
00:07:31,484 --> 00:07:36,114
The admin office gave me
your number.

25
00:07:40,293 --> 00:07:42,022
Sure. I'm alright.

26
00:07:43,062 --> 00:07:45,929
Yes, I'll just...

27
00:07:46,599 --> 00:07:47,497
Right.

28
00:07:49,202 --> 00:07:50,260
Thank you.

29
00:08:57,837 --> 00:09:01,000
- Why are you blowing me off?
- What now?

30
00:09:05,811 --> 00:09:06,641
Give it back.

31
00:09:07,280 --> 00:09:08,247
Come get it.

32
00:09:20,393 --> 00:09:21,690
Come pick it up later.

33
00:10:46,278 --> 00:10:48,712
- Just pick it up and shove it.
- Under here.

34
00:10:50,216 --> 00:10:52,084
- Over here.
- Just give it to me.

35
00:10:52,085 --> 00:10:53,813
Guys, Becky's here.

36
00:10:58,591 --> 00:10:59,683
Warm yourself by the fire.

37
00:11:03,062 --> 00:11:11,094
You're late!
Don't keep me waiting.

38
00:11:11,504 --> 00:11:12,402
Sorry.

39
00:11:13,839 --> 00:11:16,706
I missed you buddy.

40
00:11:17,677 --> 00:11:20,510
Don't be late again, alright?

41
00:11:22,448 --> 00:11:23,608
Your backpack's been smoked.

42
00:11:27,787 --> 00:11:30,255
It's a joke.

43
00:11:30,956 --> 00:11:34,323
You think I'd do
that to a friend?

44
00:11:35,461 --> 00:11:36,689
Where's my backpack?

45
00:11:37,296 --> 00:11:38,957
It's at Lego's.

46
00:11:40,466 --> 00:11:43,458
This fucker wanted to burn it,
but Lego stopped.

47
00:11:44,236 --> 00:11:46,329
- It is smokin'
- Right under here.

48
00:11:46,772 --> 00:11:49,434
- There's history book.
- Where? Where?

49
00:11:53,279 --> 00:11:57,375
You good, Becky?

50
00:11:57,917 --> 00:11:58,975
What is?

51
00:12:00,086 --> 00:12:04,819
Are you having fun?

52
00:12:05,491 --> 00:12:06,515
Nah...

53
00:12:07,426 --> 00:12:11,385
I got witnesses say you are.

54
00:12:12,431 --> 00:12:16,629
Eyeball one, two, three, four

55
00:12:18,337 --> 00:12:24,367
Glasses count for four,
so eight, nine, ten...

56
00:12:25,945 --> 00:12:27,572
You're laughing again.

57
00:12:28,581 --> 00:12:29,809
I'm not.

58
00:12:30,249 --> 00:12:33,241
Do I look funny to you?

59
00:12:34,453 --> 00:12:36,944
Like a fucking comedian?

60
00:12:38,290 --> 00:12:41,626
- Bitch, just stop.
- Give it back.

61
00:12:41,627 --> 00:12:44,653
Stop fucking laughing.

62
00:12:51,437 --> 00:12:55,237
Is this funny?

63
00:13:03,215 --> 00:13:05,581
Sir, please come in.

64
00:13:45,524 --> 00:13:47,549
Please, have some.

65
00:13:54,834 --> 00:13:56,392
You came to the funeral?

66
00:13:57,203 --> 00:13:58,898
Yes, to the funeral cortege.

67
00:14:01,407 --> 00:14:07,812
Frankly, I can't remember
who all came.

68
00:14:08,514 --> 00:14:10,141
You look a bit familiar.

69
00:14:10,282 --> 00:14:14,082
It's OK.
Many kids came that day.

70
00:14:14,587 --> 00:14:17,021
So, how did you get my number?

71
00:14:18,824 --> 00:14:22,193
Your teacher gave me
some phone numbers.

72
00:14:22,194 --> 00:14:28,155
Kids said you were
good friends with Ki-tae.

73
00:14:33,939 --> 00:14:38,603
I'm not here to intimidate you.
Just wanted to chat.

74
00:14:39,545 --> 00:14:40,773
Sure...

75
00:16:04,964 --> 00:16:07,933
I'm sorry to bother you.

76
00:16:08,033 --> 00:16:09,660
It's alright.

77
00:16:11,003 --> 00:16:15,201
It's your senior year.
You must be busy studying.

78
00:16:15,874 --> 00:16:17,307
I'm alright.

79
00:16:19,511 --> 00:16:21,138
When do you start school?

80
00:16:21,480 --> 00:16:24,449
Next week.

81
00:16:28,787 --> 00:16:34,851
I found many pictures of
you and Ki-tae together.

82
00:16:35,527 --> 00:16:36,858
I see.

83
00:16:38,063 --> 00:16:41,760
There was another boy,
but I'm not sure who it is.

84
00:16:43,168 --> 00:16:44,658
It's probably Dong-yoon.

85
00:16:48,741 --> 00:16:50,106
Dong-yoon.

86
00:16:50,576 --> 00:16:51,600
Yes.

87
00:16:56,815 --> 00:16:59,875
- Awesome.
- Fucking change.

88
00:17:00,119 --> 00:17:01,051
Homerun!

89
00:17:05,991 --> 00:17:06,855
Last one!

90
00:17:07,259 --> 00:17:08,487
Hurry up.

91
00:17:08,727 --> 00:17:10,820
If it's out, we have to change.

92
00:17:19,338 --> 00:17:20,202
I'm tired.

93
00:17:21,840 --> 00:17:24,832
Last one.
Rock, scissors...

94
00:17:25,411 --> 00:17:26,241
Bitch!

95
00:17:30,082 --> 00:17:32,778
Hurry up. Run.

96
00:17:57,342 --> 00:18:00,038
- Shit, that hurts.
- Watch out.

97
00:18:01,747 --> 00:18:05,342
Let's just buy another ball.

98
00:18:05,584 --> 00:18:08,985
No way, you retard,
Ki-tae loves that ball.

99
00:18:09,755 --> 00:18:12,349
What's in that ball?

100
00:18:14,393 --> 00:18:16,054
- It's expensive?
- I don't know.

101
00:18:17,296 --> 00:18:18,194
Did you look here?

102
00:18:19,998 --> 00:18:21,966
Guys! I found it.

103
00:18:23,335 --> 00:18:24,199
Yeah?

104
00:18:26,271 --> 00:18:27,602
Let's go chill.

105
00:18:32,911 --> 00:18:34,378
You like Bo-kyung that much?

106
00:18:35,414 --> 00:18:37,848
What? No, I don't.

107
00:18:38,383 --> 00:18:39,873
Dumbass, you like her.

108
00:18:40,853 --> 00:18:42,887
- No, I don't.
- It's written on your face.

109
00:18:42,888 --> 00:18:45,123
How about Wolmi Island this weekend?

110
00:18:45,124 --> 00:18:47,024
With Bo-kyung and other girls.

111
00:18:47,025 --> 00:18:48,049
3-on-3?

112
00:18:48,961 --> 00:18:50,087
With Se-jung too?

113
00:18:50,262 --> 00:18:54,790
It's definitely a GO!

114
00:18:54,900 --> 00:18:56,424
How about it, Becky?

115
00:18:57,703 --> 00:18:58,897
Yeah, whatever.

116
00:18:59,004 --> 00:19:00,438
You suck.

117
00:19:00,439 --> 00:19:01,701
What?

118
00:19:02,040 --> 00:19:05,601
- He thinks he's so cool.
- Dumbass.

119
00:19:06,645 --> 00:19:07,907
Do I look it?

120
00:19:08,547 --> 00:19:10,674
Yes, you fucking do.

121
00:19:11,250 --> 00:19:16,020
Okay, I'll go.
Dude, my head hurts.

122
00:19:16,021 --> 00:19:18,216
You little shit.

123
00:19:19,158 --> 00:19:21,388
That hurts.
I said I'll go.

124
00:19:27,499 --> 00:19:29,865
You sure Se-jung's coming?

125
00:19:30,135 --> 00:19:33,204
How many times did I say yes?

126
00:19:33,205 --> 00:19:34,729
If not, I'm not going.

127
00:19:34,840 --> 00:19:36,340
You're acting weird.

128
00:19:36,341 --> 00:19:38,002
I can ask.

129
00:19:38,143 --> 00:19:39,678
You're only going
for your own sake.

130
00:19:39,679 --> 00:19:41,873
No way, bitch.

131
00:19:42,014 --> 00:19:45,216
I'm trying to hook up
Becky and Bo-kyung.

132
00:19:45,217 --> 00:19:48,186
What did I do?
He's going because of Se-jung.

133
00:19:48,187 --> 00:19:49,882
So what?

134
00:19:50,122 --> 00:19:52,283
You're the one always complaining.

135
00:19:52,791 --> 00:19:54,392
Now, you're coming out.

136
00:19:54,393 --> 00:19:55,553
No, I'm not.

137
00:19:55,861 --> 00:19:58,091
Besides, why can't I get
with Se-jung?

138
00:19:58,564 --> 00:20:02,364
Huh? Bo-kyung, Se-jung...

139
00:20:04,970 --> 00:20:06,961
Fucking dumbass

140
00:20:09,675 --> 00:20:11,676
Be more aggressive.

141
00:20:11,677 --> 00:20:14,212
You're gonna blow your chance.

142
00:20:14,213 --> 00:20:15,339
Fine.

143
00:20:15,714 --> 00:20:21,277
Look at Dong-yoon.
He's just going at it.

144
00:20:21,620 --> 00:20:23,588
You think I don't wanna?

145
00:20:23,889 --> 00:20:26,949
Shut up and listen.

146
00:20:27,659 --> 00:20:29,957
Picture her here. Gaze at her.

147
00:20:30,229 --> 00:20:35,189
- Then once she sees you, look away.
- That's aggressive?

148
00:20:35,801 --> 00:20:39,170
- That's aggressive!
- How's that aggressive?

149
00:20:39,171 --> 00:20:42,473
- Just ask Dong-yoon.
- You suck.

150
00:20:42,474 --> 00:20:44,976
If you did half the shit he did,

151
00:20:44,977 --> 00:20:46,278
- You could have had her already.
- You suck.

152
00:20:46,279 --> 00:20:47,370
Hurry up.

153
00:20:47,579 --> 00:20:48,910
Okay.

154
00:20:49,448 --> 00:20:51,177
Yo, hurry up.

155
00:20:55,187 --> 00:20:56,387
I told you so.

156
00:20:56,388 --> 00:20:57,556
He's acting strange.

157
00:20:57,557 --> 00:20:58,923
Just like that.

158
00:20:58,924 --> 00:21:01,051
- Said look into her eyes.
- Move your ass.

159
00:21:02,461 --> 00:21:03,562
Go take care of his dumbass.

160
00:21:03,563 --> 00:21:04,862
What did he say?

161
00:21:04,863 --> 00:21:08,026
I don't know.
He said to look into her eyes.

162
00:21:08,166 --> 00:21:10,566
Don't you get it?

163
00:21:11,003 --> 00:21:13,403
I'm describing exactly
what he does.

164
00:22:29,181 --> 00:22:32,116
What do you want-la?

165
00:22:38,023 --> 00:22:40,856
Hurry up-leh.

166
00:22:41,626 --> 00:22:45,528
- Feels like we flew here.
- We're really in China.

167
00:22:46,465 --> 00:22:49,025
I bet everyone speaks Chinese here.

168
00:22:49,134 --> 00:22:50,726
- How about lunch?
- I can speak a little Chinese.

169
00:22:52,437 --> 00:22:55,539
- Wo ai ni. Zai Zien.
- Wo ai ni, my ass.

170
00:22:55,540 --> 00:23:00,144
- Zai Zien
- Ni hao ma.

171
00:23:00,145 --> 00:23:00,812
Here's another.

172
00:23:00,813 --> 00:23:03,007
Ni chi fan le ma.

173
00:23:06,651 --> 00:23:08,552
You cussed. You're bad.

174
00:23:08,553 --> 00:23:10,922
- No I didn't. It's Chinese.
- You cussed. You're bad.

175
00:23:10,923 --> 00:23:12,480
It's not cursing.

176
00:23:14,960 --> 00:23:17,895
Where we going?
I'm hungry.

177
00:23:18,497 --> 00:23:19,623
Let's go here.

178
00:23:25,904 --> 00:23:27,271
Oh! I know who it is.

179
00:23:27,272 --> 00:23:31,971
I saw him carrying bunch of flowers.

180
00:23:32,277 --> 00:23:38,649
- Wearing this apron.
- That faggot.

181
00:23:38,650 --> 00:23:39,850
Why?

182
00:23:39,851 --> 00:23:42,081
It was a super cute apron.

183
00:23:44,055 --> 00:23:46,489
- This is spicy.
- Drink this.

184
00:23:46,758 --> 00:23:48,259
What did he eat?

185
00:23:48,260 --> 00:23:52,062
- You two look cute.
- Asshole.

186
00:23:52,063 --> 00:23:55,499
No we don't.
I would've done the same for you.

187
00:23:57,269 --> 00:24:01,172
- You two lovebirds.
- Shut the hell up.

188
00:24:01,173 --> 00:24:04,006
Ki-tae, have some of this.

189
00:24:13,418 --> 00:24:16,353
- Is it good?
- Yeah.

190
00:24:19,391 --> 00:24:21,985
Isn't this far too?
Have some of this too.

191
00:24:26,097 --> 00:24:28,463
- What are you doing?
- What?

192
00:24:30,502 --> 00:24:31,662
What are you doing?

193
00:24:31,903 --> 00:24:34,337
You were far from the dumplings.

194
00:24:35,106 --> 00:24:36,801
You said you didn't
want your noodles.

195
00:24:38,743 --> 00:24:44,113
Are you trying to make
Hee-june jealous?

196
00:24:44,249 --> 00:24:45,477
What?

197
00:24:46,084 --> 00:24:48,075
You are, aren't you?

198
00:24:48,553 --> 00:24:57,161
Stop with your bitching.
The food's pretty good.

199
00:24:57,162 --> 00:25:01,189
- It is great.
- Have some.

200
00:25:01,299 --> 00:25:03,096
What do you know about dumplings?

201
00:25:03,301 --> 00:25:04,700
Pretty good.

202
00:25:05,303 --> 00:25:08,670
- I hate when you joke around.
- The food still sucks.

203
00:25:09,107 --> 00:25:11,940
She's into you.

204
00:25:12,043 --> 00:25:13,476
Good luck. She's pretty.

205
00:25:13,578 --> 00:25:16,513
Dude, stop.

206
00:25:18,083 --> 00:25:22,884
Let's all go to your place.

207
00:25:23,488 --> 00:25:26,357
- And do what?
- Just hang.

208
00:25:26,358 --> 00:25:28,053
Are you two dating?

209
00:25:30,629 --> 00:25:34,895
Are ya? Are ya?

210
00:25:58,356 --> 00:26:00,722
I've never been.

211
00:26:01,826 --> 00:26:06,820
I think he was uncomfortable
bringing friends home.

212
00:26:09,067 --> 00:26:13,070
He came to my house
and Dong-yoon's a lot.

213
00:26:13,071 --> 00:26:14,538
My mom knows Ki-tae too.

214
00:26:14,873 --> 00:26:16,773
- Really?
- Yes.

215
00:26:17,943 --> 00:26:21,811
It's not only Ki-tae.

216
00:26:21,947 --> 00:26:26,680
All kids act differently
when they're around parents.

217
00:26:28,420 --> 00:26:34,757
He wasn't in any trouble at school?

218
00:26:35,093 --> 00:26:37,061
No, at least not before I moved.

219
00:26:40,332 --> 00:26:46,601
Frankly, I don't know why
Jae-ho gave you my number.

220
00:26:48,073 --> 00:26:51,600
I moved weeks before
what happened to Ki-tae.

221
00:26:51,943 --> 00:26:55,470
So, I don't know anything.

222
00:26:58,683 --> 00:27:02,813
So you haven't seen him
since you moved?

223
00:27:03,254 --> 00:27:06,189
He came by once to see me.

224
00:27:06,491 --> 00:27:09,892
Couple of days after I moved.

225
00:27:10,462 --> 00:27:14,558
We said our hellos and
I haven't seen him since.

226
00:27:14,833 --> 00:27:19,236
You didn't notice
anything strange?

227
00:27:26,945 --> 00:27:30,312
I guess he came to say goodbye.

228
00:27:30,915 --> 00:27:33,782
He seemed alright.

229
00:27:39,257 --> 00:27:43,660
What about the other boy
in the picture?

230
00:27:44,329 --> 00:27:46,354
- You mean Dong-yoon?
- Yes, him.

231
00:27:47,532 --> 00:27:49,124
Did he come to the funeral?

232
00:27:50,135 --> 00:27:51,397
No, he didn't.

233
00:27:52,103 --> 00:27:54,162
I thought they were close.

234
00:27:55,273 --> 00:27:58,142
Well, he didn't come.

235
00:27:58,143 --> 00:28:00,111
I haven't talked to him in a while.

236
00:28:04,382 --> 00:28:06,714
Can you tell me
a little about him?

237
00:28:07,519 --> 00:28:08,679
About Dong-yoon?

238
00:28:11,556 --> 00:28:16,789
They were good friends
since junior high.

239
00:28:17,996 --> 00:28:23,400
I met Ki-tae in high school.

240
00:28:24,335 --> 00:28:26,860
Was Dong-yoon in the same classes?

241
00:28:27,172 --> 00:28:30,630
No, he was in different classes.

242
00:28:32,977 --> 00:28:38,745
Ki-tae and Dong-yoon were special.

243
00:28:39,517 --> 00:28:40,984
What do you mean special?

244
00:28:41,920 --> 00:28:44,047
I'm a high school friend.

245
00:28:45,724 --> 00:28:49,160
They were best friends
since junior high.

246
00:28:49,994 --> 00:28:52,656
I could never be as close...
Not like those two.

247
00:29:34,739 --> 00:29:37,674
- As long as you don't pick me.
- Ki-tae's so good.

248
00:29:38,977 --> 00:29:41,138
No, don't get that.

249
00:29:41,980 --> 00:29:44,915
Dumbass keeps taking it.

250
00:29:48,453 --> 00:29:51,183
Why? I think he's it.

251
00:29:55,126 --> 00:29:57,993
You better choose well.

252
00:30:21,452 --> 00:30:22,885
This is boring.

253
00:30:23,321 --> 00:30:24,583
Why don't you go play?

254
00:30:26,324 --> 00:30:27,256
You want some?

255
00:30:28,393 --> 00:30:30,122
When are your parents coming home?

256
00:30:31,396 --> 00:30:32,693
Maybe tomorrow.

257
00:30:33,498 --> 00:30:35,261
Then I'm gonna crash at your place.

258
00:30:37,936 --> 00:30:39,426
Alright?

259
00:30:39,637 --> 00:30:40,569
Fine.

260
00:30:41,206 --> 00:30:43,037
You want me to go?

261
00:30:43,641 --> 00:30:44,665
No, it's fine.

262
00:30:47,078 --> 00:30:49,546
Go talk to Bo-kyung or something.

263
00:30:53,852 --> 00:30:57,253
Why are you so quite?
What's wrong, yo?

264
00:31:00,658 --> 00:31:04,788
What were you two talking about?

265
00:31:06,231 --> 00:31:08,256
What do you mean?

266
00:31:09,133 --> 00:31:13,160
Earlier in the room,
I saw you and Bo-kyung talking.

267
00:31:16,708 --> 00:31:17,800
You saw?

268
00:31:18,309 --> 00:31:19,606
Why else would I ask?

269
00:31:22,814 --> 00:31:24,281
It wasn't anything.

270
00:31:29,854 --> 00:31:31,788
You don't think something happened.

271
00:31:32,090 --> 00:31:33,284
Nah...

272
00:31:33,591 --> 00:31:36,193
You got it all wrong.
Stop that.

273
00:31:36,194 --> 00:31:37,627
Alright.

274
00:31:39,864 --> 00:31:40,956
That's strange...

275
00:31:43,701 --> 00:31:45,293
A guilty conscience
needs no accuser.

276
00:31:56,180 --> 00:32:03,518
- What do you mean?
- What?

277
00:32:03,788 --> 00:32:05,756
What do you mean by that?

278
00:32:05,890 --> 00:32:07,255
It's nothing.

279
00:32:09,093 --> 00:32:13,530
- You need to stop.
- Don't touch my head.

280
00:32:15,600 --> 00:32:23,029
Becky, you've grown up so fast.

281
00:32:23,107 --> 00:32:25,302
Don't talk to me like that.

282
00:32:26,110 --> 00:32:29,671
I'm not your bitch.

283
00:32:31,716 --> 00:32:37,780
Dude, I was joking.

284
00:32:42,360 --> 00:32:43,725
Jae-ho, come out this way.

285
00:32:44,329 --> 00:32:45,557
He's caught again.

286
00:32:48,599 --> 00:32:50,533
He's caught again.

287
00:32:59,143 --> 00:33:01,168
Get him.

288
00:33:03,081 --> 00:33:05,982
- Jae-ho does flower arrangement?
- Yeah, At his mom's flower shop.

289
00:33:07,385 --> 00:33:08,909
I'm trying to be a good son
Assholes!

290
00:33:10,855 --> 00:33:13,551
That's better than
your mom telling you to study.

291
00:33:13,858 --> 00:33:16,520
I fucking work every weekend.

292
00:33:16,761 --> 00:33:19,491
- You don't have private tutoring.
- I'd rather do that.

293
00:33:20,698 --> 00:33:22,188
How much do you get paid?

294
00:33:24,102 --> 00:33:25,702
- Two bucks.
- Two bucks?

295
00:33:25,703 --> 00:33:26,670
Damn.

296
00:33:27,372 --> 00:33:28,872
Ask for a 50 cent raise.

297
00:33:28,873 --> 00:33:30,941
You're like those
underpaid foreign workers.

298
00:33:30,942 --> 00:33:33,433
It's about being a good son.

299
00:33:34,278 --> 00:33:36,947
I'd rather get paid two bucks
and hang out with my mom.

300
00:33:36,948 --> 00:33:39,439
You and your flowers.
So cute!

301
00:33:39,784 --> 00:33:42,719
Shut the hell up.
I do look hot carrying flowers.

302
00:33:42,720 --> 00:33:45,555
Guess what fucking happened
yesterday.

303
00:33:45,556 --> 00:33:46,623
What?

304
00:33:46,624 --> 00:33:48,759
- That underpass place.
- The underpass?

305
00:33:48,760 --> 00:33:53,094
What's with the junior high asses
smoking cigarettes?

306
00:33:53,431 --> 00:33:55,332
With bitches on the side.

307
00:33:55,333 --> 00:33:57,995
For real?
You left them alone?

308
00:33:58,336 --> 00:34:02,898
I was glaring at them.
Then these fuckers gave me looks.

309
00:34:03,274 --> 00:34:04,798
They gave you dirty looks?

310
00:34:05,877 --> 00:34:07,344
They're begging to die.

311
00:34:07,745 --> 00:34:12,182
There were about seven,
not counting the girls.

312
00:34:12,717 --> 00:34:19,953
I was gonna go,
but it was a big group.

313
00:34:20,892 --> 00:34:27,161
So I turned away,
but then I got pissed.

314
00:34:28,766 --> 00:34:30,859
- Let's get them.
- Go?

315
00:34:31,002 --> 00:34:32,993
- Where?
- So what happened?

316
00:34:36,541 --> 00:34:38,338
You should've called us.

317
00:34:39,143 --> 00:34:41,805
- You should've called me.
- So what happened?

318
00:34:44,982 --> 00:34:48,440
- You didn't lose, right?
- Junior ass is my specialty.

319
00:34:48,553 --> 00:34:49,986
Yo, talk.

320
00:34:50,755 --> 00:34:53,123
I need to go shit.

321
00:34:53,124 --> 00:34:54,352
Finish the story.

322
00:34:57,528 --> 00:34:58,790
What up?

323
00:34:58,930 --> 00:35:00,693
- Maybe he got his ass kicked.
- He's full of shit.

324
00:35:05,803 --> 00:35:06,827
Where you going?

325
00:35:21,552 --> 00:35:22,780
Come stand here.

326
00:35:26,457 --> 00:35:27,515
What's up?

327
00:35:32,864 --> 00:35:34,354
Why did you do that?

328
00:35:36,467 --> 00:35:37,593
What?

329
00:35:40,505 --> 00:35:42,632
You two were doing that thing.

330
00:35:44,909 --> 00:35:46,137
What do you mean?

331
00:35:46,777 --> 00:35:49,245
Giving funny looks to each other.

332
00:35:52,083 --> 00:35:56,144
You think I'm a retard?

333
00:36:03,261 --> 00:36:05,286
You better tell now
or I'm gonna kick your ass.

334
00:36:12,803 --> 00:36:15,328
Hurry up, fucker!

335
00:36:17,742 --> 00:36:20,040
We were talking earlier.

336
00:36:21,012 --> 00:36:22,274
About?

337
00:36:25,650 --> 00:36:28,949
How you always get quiet
when we talk about parents.

338
00:36:32,156 --> 00:36:34,954
You suddenly stop talking
then you switch subjects.

339
00:36:36,327 --> 00:36:41,526
You did it again today,
and it reminded us.

340
00:36:44,135 --> 00:36:47,434
That's why you two were laughing?
At me?

341
00:36:48,906 --> 00:36:51,641
I'd never laugh at you.

342
00:36:51,642 --> 00:36:54,270
That's why you two gave
funny looks?

343
00:36:56,847 --> 00:36:59,111
Look at me.

344
00:37:03,287 --> 00:37:04,345
I'm sorry.

345
00:37:10,194 --> 00:37:11,388
Look at me.

346
00:37:13,231 --> 00:37:14,664
Start talking.

347
00:37:16,033 --> 00:37:19,799
- We weren't laughing at you.
- Fucking tell me.

348
00:37:21,906 --> 00:37:24,773
We looked at each other,
but we weren't laughing at you.

349
00:37:31,515 --> 00:37:32,914
Why would I do that?

350
00:37:47,231 --> 00:37:48,633
Are you gonna see Se-jung later?

351
00:37:48,634 --> 00:37:49,725
No.

352
00:37:51,335 --> 00:37:56,773
The CD, Lego gave you.
Are you gonna watch it with her?

353
00:37:57,141 --> 00:37:59,905
Dumbass.
I didn't get anything.

354
00:38:00,278 --> 00:38:02,712
- I saw you.
- I was pretending.

355
00:38:03,547 --> 00:38:06,414
You're so into her.

356
00:38:06,717 --> 00:38:07,952
- Sure am.
- I'm jealous now.

357
00:38:07,953 --> 00:38:11,187
Wanna go play baseball?

358
00:38:11,188 --> 00:38:12,023
Go play it with Se-jung.

359
00:38:12,024 --> 00:38:14,821
Go to hell.
Ki-tae, let's go play baseball.

360
00:38:15,760 --> 00:38:17,125
Let's go play baseball.

361
00:38:17,361 --> 00:38:18,295
Go. Go!

362
00:38:18,296 --> 00:38:19,262
Let go.

363
00:38:19,263 --> 00:38:20,252
What?

364
00:38:22,333 --> 00:38:23,231
What?

365
00:38:24,335 --> 00:38:26,132
I'm just gonna say one thing.

366
00:38:26,237 --> 00:38:27,804
- Me?
- Becky.

367
00:38:27,805 --> 00:38:28,863
Me?

368
00:38:36,947 --> 00:38:41,441
When you go home, your mom makes dinner
and tells you to study. Right?

369
00:38:42,019 --> 00:38:43,577
What's up?

370
00:38:48,059 --> 00:38:49,993
When I go home,
I cook my own dinner.

371
00:38:51,796 --> 00:38:54,026
Sometimes, if I get lucky,
I get to see my old man and say hello.

372
00:38:57,001 --> 00:39:02,598
I'm always late to school.
I get pissed that no one woke me up.

373
00:39:05,042 --> 00:39:08,239
But you see, my mom's gone.

374
00:39:10,114 --> 00:39:11,348
OK. I get it.

375
00:39:11,349 --> 00:39:13,010
There's no one.

376
00:39:16,954 --> 00:39:24,725
That's about
all I can say about my parents.

377
00:39:28,332 --> 00:39:32,996
Are you satisfied?
Are you?

378
00:39:40,044 --> 00:39:41,671
I'm gonna split.

379
00:39:44,448 --> 00:39:47,212
Ki-tae.

380
00:39:54,859 --> 00:39:56,053
OK.

381
00:39:59,363 --> 00:40:02,389
- I was just asking to play baseball.
- Don't know.

382
00:40:04,335 --> 00:40:06,098
What do you mean you don't know?

383
00:40:06,871 --> 00:40:08,304
I don't.

384
00:41:00,157 --> 00:41:01,385
You're still here.

385
00:41:02,526 --> 00:41:04,153
You came out earlier than I thought.

386
00:41:04,829 --> 00:41:08,162
I couldn't concentrate
so I'm going home.

387
00:41:08,799 --> 00:41:09,857
Is it because of me?

388
00:41:10,000 --> 00:41:12,594
No. Is something wrong?

389
00:41:13,504 --> 00:41:19,773
The number you gave me.

390
00:41:20,244 --> 00:41:21,836
Dong-yoon's cell is turned off.

391
00:41:22,313 --> 00:41:26,682
I haven't talked to him lately, either.

392
00:41:29,787 --> 00:41:36,317
I'm sorry to bother you,
but could you help me?

393
00:41:38,562 --> 00:41:42,165
I'm not sure who to call.

394
00:41:42,166 --> 00:41:45,932
Maybe, you have some friends,
who still talk to Dong-yoon.

395
00:41:50,007 --> 00:41:51,702
Please?

396
00:41:55,913 --> 00:41:59,474
I was wondering
if you still talked to him.

397
00:42:05,756 --> 00:42:07,451
I have his home phone too.

398
00:42:07,958 --> 00:42:15,797
OK, let me just ask for a favor.

399
00:42:16,767 --> 00:42:21,295
If Dong-yoon calls, give me a call.

400
00:43:33,477 --> 00:43:35,536
- Give me your left hand.
- Light me up.

401
00:43:35,846 --> 00:43:40,681
Give me your left hand.
Now, my hand smells.

402
00:43:40,985 --> 00:43:44,318
- I dropped my cigarette.
- They're gonna check me too.

403
00:43:45,289 --> 00:43:46,221
Light me up.

404
00:43:48,692 --> 00:43:50,216
Becky, keep a lookout.

405
00:43:54,198 --> 00:43:56,928
Are you deaf?
I said, go keep a lookout.

406
00:43:57,267 --> 00:43:58,393
What?

407
00:43:59,303 --> 00:44:01,066
You don't even smoke.

408
00:44:06,610 --> 00:44:10,944
Dude, it's just a joke.

409
00:44:12,349 --> 00:44:13,475
Fine.

410
00:44:22,092 --> 00:44:24,458
Don't get pissed.

411
00:44:24,695 --> 00:44:26,162
I get it.

412
00:44:27,731 --> 00:44:30,825
- Relax. I'm sorry.
- I said, I got it.

413
00:44:33,137 --> 00:44:36,300
- OK. I'm really sorry.
- Fine.

414
00:44:39,043 --> 00:44:40,203
I'm gonna go in now.

415
00:44:41,712 --> 00:44:43,407
Fuck!

416
00:44:44,982 --> 00:44:46,472
I told you I'm sorry.

417
00:44:47,251 --> 00:44:48,548
Like I said, I got it.

418
00:44:48,652 --> 00:44:52,053
I said I'm sorry.

419
00:44:54,725 --> 00:44:59,492
Stop looking at me like that.

420
00:45:00,397 --> 00:45:02,729
I said stop frowning.
Just stop.

421
00:45:05,102 --> 00:45:09,630
Becky. Look at me, shitface.

422
00:45:09,973 --> 00:45:11,497
Let go of me.

423
00:45:12,910 --> 00:45:17,438
You're gonna be like that?

424
00:45:18,415 --> 00:45:20,349
Look at me, bitch.

425
00:45:59,823 --> 00:46:01,222
He's not picking up.

426
00:46:02,559 --> 00:46:03,457
Yeah?

427
00:46:07,364 --> 00:46:08,991
Maybe, Becky's in trouble.

428
00:46:10,634 --> 00:46:11,862
Nah, he's fine.

429
00:46:26,917 --> 00:46:28,680
Think you're smarter than me?

430
00:46:30,554 --> 00:46:33,148
- Growing up so fast.
- Fucking fast.

431
00:46:35,425 --> 00:46:36,392
Yo, Becky!

432
00:46:49,573 --> 00:46:56,445
This is a type of
traditional Korean poem.

433
00:46:57,214 --> 00:47:04,143
Characteristics of poem are?

434
00:47:05,656 --> 00:47:11,151
From the end of Goryeo dynasty.

435
00:47:11,695 --> 00:47:18,931
It's a good test question.
Highlight.

436
00:47:33,450 --> 00:47:35,145
Why are you napping all day?

437
00:47:43,093 --> 00:47:44,117
Yo, Becky.

438
00:48:10,120 --> 00:48:12,213
Raise your fucking head.

