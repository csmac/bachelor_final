﻿1
00:00:13,100 --> 00:00:14,490
Play dumb.

2
00:00:14,910 --> 00:00:16,140
That's right...

3
00:00:16,140 --> 00:00:19,250
He likes Gon, so he'll bail us out.

4
00:00:20,840 --> 00:00:21,880
Idiot!

5
00:00:24,310 --> 00:00:26,360
Oh... Oops.

6
00:00:26,360 --> 00:00:27,530
What is it?

7
00:00:27,530 --> 00:00:29,250
You know someone here?

8
00:00:29,600 --> 00:00:31,760
Ah! No...

9
00:00:34,630 --> 00:00:36,090
It's that girl!

10
00:00:36,090 --> 00:00:38,640
Oh? You know them, Shizuku?

11
00:00:39,250 --> 00:00:41,210
Nope, never seen them before.

12
00:00:41,210 --> 00:00:41,890
Huh?

13
00:00:42,220 --> 00:00:44,510
Oh, I remember...

14
00:00:44,510 --> 00:00:46,650
It's the arm wrestling kid.

15
00:00:46,650 --> 00:00:48,490
Who was that?

16
00:00:48,490 --> 00:00:52,550
Two days ago, you lost to 
that kid at arm wrestling.

17
00:00:52,550 --> 00:00:54,530
Lost? I lost?

18
00:00:55,130 --> 00:00:56,230
That's a lie.

19
00:00:56,230 --> 00:00:58,780
I would never lose to a kid.

20
00:00:59,150 --> 00:01:02,760
Well, you were using your right hand...

21
00:01:02,760 --> 00:01:05,040
Why? I'm left-handed.

22
00:01:05,370 --> 00:01:06,540
Don't bother.

23
00:01:06,540 --> 00:01:10,360
Once Shizuku's forgotten something, 
she'll never remember.

24
00:01:10,360 --> 00:01:13,280
Never mind... I was mistaken.

25
00:01:13,280 --> 00:01:14,170
Thought so.

26
00:01:14,500 --> 00:01:15,690
Oh?

27
00:01:15,690 --> 00:01:17,900
You actually beat Shizuku?

28
00:01:17,900 --> 00:01:18,800
Yeah.

29
00:01:18,800 --> 00:01:21,930
Never would've imagined she 
was a Troupe member.

30
00:01:22,600 --> 00:01:25,270
Okay, I'll challenge you.

31
00:02:46,020 --> 00:02:51,060
Ally x And x Sword

32
00:02:53,050 --> 00:02:54,270
One more time.

33
00:02:57,890 --> 00:02:59,280
Ready...

34
00:02:59,670 --> 00:03:00,530
Go.

35
00:03:13,720 --> 00:03:15,380
One more time.

36
00:03:18,960 --> 00:03:20,380
Ready...

37
00:03:21,260 --> 00:03:21,950
Go.

38
00:03:27,110 --> 00:03:28,060
Hey...

39
00:03:28,640 --> 00:03:30,720
When it comes to arm wrestling,

40
00:03:30,720 --> 00:03:32,860
what's my rank among the spiders?

41
00:03:34,240 --> 00:03:36,860
Seventh or eighth?

42
00:03:36,860 --> 00:03:39,900
You're not weak, but you're 
not strong, either.

43
00:03:40,480 --> 00:03:45,120
And the strongest was this guy called Uvogin.

44
00:03:45,690 --> 00:03:48,910
But apparently, he was 
killed by the chain user.

45
00:03:49,490 --> 00:03:52,150
We already said we don't know who that is!

46
00:03:52,150 --> 00:03:53,050
Kid!

47
00:03:55,020 --> 00:03:57,250
If you speak without permission again,

48
00:03:58,720 --> 00:04:00,550
I'll kill you.

49
00:04:04,110 --> 00:04:05,610
One more time.

50
00:04:06,710 --> 00:04:08,060
Ready...

51
00:04:08,820 --> 00:04:09,820
Go.

52
00:04:13,590 --> 00:04:15,580
He was an Enhancer.

53
00:04:16,190 --> 00:04:19,530
Simple-minded and straightforward, 
he loved a good, all-out fight.

54
00:04:20,080 --> 00:04:23,310
But he was fussy about time.

55
00:04:23,310 --> 00:04:25,410
He'd get into fights

56
00:04:25,410 --> 00:04:28,120
with Franklin and me when we were late.

57
00:04:28,120 --> 00:04:32,290
He'd beat the crap out of me 
in an unarmed brawl...

58
00:04:33,040 --> 00:04:35,550
I've known him since before 
the Troupe was founded.

59
00:04:36,410 --> 00:04:38,290
I know him better than anyone.

60
00:04:39,820 --> 00:04:41,010
He would never...

61
00:04:41,780 --> 00:04:45,490
Uvo would never lose in a fight!

62
00:04:45,920 --> 00:04:48,950
He must've been caught in some dirty trap!

63
00:04:50,740 --> 00:04:53,060
I will make his killer pay.

64
00:04:54,390 --> 00:04:57,180
I'll find him, no matter 
how many I have to kill.

65
00:04:58,100 --> 00:05:02,250
The chain user bears a 
strong grudge against us.

66
00:05:02,250 --> 00:05:05,980
The Mafia's Nostrade Family 
hired him recently.

67
00:05:06,640 --> 00:05:08,010
A grudge against the Troupe?

68
00:05:08,410 --> 00:05:09,990
Recently hired...

69
00:05:11,410 --> 00:05:15,030
You might not know him directly, 
but you may have heard stories of him!

70
00:05:15,410 --> 00:05:17,270
Think hard...

71
00:05:17,270 --> 00:05:20,340
If you have any guesses, spill them!

72
00:05:20,760 --> 00:05:22,100
I don't know anything.

73
00:05:22,580 --> 00:05:26,340
But even if I did, I would never tell you!

74
00:05:26,930 --> 00:05:29,950
I thought you were a bunch 
of heartless monsters,

75
00:05:31,130 --> 00:05:33,400
but you'll mourn the death 
of one of your own?

76
00:05:35,590 --> 00:05:37,810
Why couldn't you spare...

77
00:05:37,810 --> 00:05:41,230
Spare a fraction of that grief...

78
00:05:41,230 --> 00:05:45,190
for the people you've killed?!

79
00:05:55,910 --> 00:05:58,210
You're getting too cocky.

80
00:06:00,210 --> 00:06:01,170
Gon!

81
00:06:09,000 --> 00:06:10,930
Another step, and I'll cut you up.

82
00:06:11,340 --> 00:06:13,260
Answer the question.

83
00:06:13,790 --> 00:06:15,680
Do you know who the chain user is?

84
00:06:16,040 --> 00:06:19,940
I already said I have nothing to tell you!

85
00:06:22,420 --> 00:06:23,360
Feitan...

86
00:06:25,060 --> 00:06:25,970
Stop.

87
00:06:26,360 --> 00:06:27,820
Stop what?

88
00:06:28,820 --> 00:06:30,820
Stop what you're about to do.

89
00:06:31,170 --> 00:06:34,540
You know what I'm about to do?

90
00:06:35,290 --> 00:06:36,830
You're going to break his arm, right?

91
00:06:37,290 --> 00:06:39,170
I'd start with a finger...

92
00:06:39,170 --> 00:06:40,830
Peel off a nail.

93
00:06:41,340 --> 00:06:42,990
Doesn't matter where you'd start.

94
00:06:42,990 --> 00:06:44,210
Just stop!

95
00:06:44,650 --> 00:06:46,520
Why are you giving me orders?

96
00:06:46,520 --> 00:06:48,420
I have no reason to listen to you.

97
00:06:58,250 --> 00:07:00,980
Hey, quit it, Nobunaga.

98
00:07:01,530 --> 00:07:03,680
Have you forgotten the rules?

99
00:07:04,170 --> 00:07:06,900
Troupe members aren't allowed 
to start serious fights.

100
00:07:06,900 --> 00:07:08,370
I know!

101
00:07:08,890 --> 00:07:10,570
If there's a dispute, we flip a coin.

102
00:07:15,330 --> 00:07:16,220
Tails.

103
00:07:16,740 --> 00:07:17,830
Heads!

104
00:07:22,380 --> 00:07:23,510
It's heads.

105
00:07:23,910 --> 00:07:24,840
Let him go.

106
00:07:29,360 --> 00:07:31,580
So, what do we do with them?

107
00:07:32,100 --> 00:07:34,800
They haven't told us anything 
about the chain user yet.

108
00:07:35,230 --> 00:07:38,090
If they don't know anything, 
we can let them go.

109
00:07:38,580 --> 00:07:39,930
Well, Pakunoda?

110
00:07:40,490 --> 00:07:42,940
I checked on the trip here.

111
00:07:43,540 --> 00:07:46,320
They really don't know anything.

112
00:07:47,500 --> 00:07:48,400
You're sure?

113
00:07:48,690 --> 00:07:49,320
Yes.

114
00:07:50,070 --> 00:07:52,570
They have no memories of the chain user.

115
00:07:54,240 --> 00:07:56,860
Seems your instinct was off this time.

116
00:07:56,860 --> 00:07:58,580
That's odd...

117
00:07:58,580 --> 00:08:02,150
Well, if Pakunoda says so, it must be true.

118
00:08:12,790 --> 00:08:13,930
Hey, Gon!

119
00:08:13,930 --> 00:08:16,180
Did they do anything to you 
on the way over here?

120
00:08:16,180 --> 00:08:18,890
No, they just asked me a few questions.

121
00:08:19,390 --> 00:08:20,220
Thought so...

122
00:08:20,890 --> 00:08:24,200
Then they learned what they wanted 
without our realizing.

123
00:08:24,200 --> 00:08:26,110
What was it? And how?

124
00:08:26,840 --> 00:08:31,610
Sounds like they really trust 
Pakunoda's judgment.

125
00:08:32,400 --> 00:08:35,260
She must possess a skill more 
convincing than mere instinct.

126
00:08:36,540 --> 00:08:39,520
They have no memories of the chain user.

127
00:08:40,010 --> 00:08:40,870
Memories?

128
00:08:41,840 --> 00:08:43,520
How about you?

129
00:08:43,520 --> 00:08:44,500
Any guesses?

130
00:08:45,320 --> 00:08:47,180
The power to scan someone's memory!

131
00:08:47,180 --> 00:08:48,740
By touching them?

132
00:08:50,320 --> 00:08:52,290
That would explain everything.

133
00:08:52,290 --> 00:08:53,720
And we're in trouble.

134
00:08:54,810 --> 00:08:58,750
When she scanned us before, I had 
no idea who the chain user was.

135
00:08:59,400 --> 00:09:02,270
But now, I have an image in my head.

136
00:09:03,780 --> 00:09:06,270
If she checks again...

137
00:09:06,790 --> 00:09:08,900
If they have nothing to do 
with the chain user,

138
00:09:08,900 --> 00:09:10,780
we can let them go?

139
00:09:11,280 --> 00:09:14,280
Yeah, there's no point 
in keeping them here.

140
00:09:14,710 --> 00:09:17,340
No, we can't be sure they're 
completely unrelated.

141
00:09:17,870 --> 00:09:20,490
It's possible they're being used.

142
00:09:21,110 --> 00:09:24,750
If the chain user typically conceals his chains,

143
00:09:25,300 --> 00:09:29,250
they wouldn't realize he was the chain user.

144
00:09:29,700 --> 00:09:33,760
We shouldn't release them until they 
tell us who they're working for.

145
00:09:34,200 --> 00:09:35,970
Assuming they're working for someone else,

146
00:09:35,970 --> 00:09:38,010
it wouldn't be the chain user.

147
00:09:38,010 --> 00:09:40,260
Because he operates alone.

148
00:09:40,810 --> 00:09:43,320
Then you agree with Nobunaga.

149
00:09:43,320 --> 00:09:45,690
Rather than use a couple of kids,

150
00:09:45,690 --> 00:09:49,520
we can find the information we need 
through the Nostrade Family.

151
00:09:49,980 --> 00:09:53,630
Since we know the chain user's a member.

152
00:09:54,100 --> 00:09:55,280
That's true.

153
00:09:55,900 --> 00:09:58,300
Our target is the chain user.

154
00:09:59,060 --> 00:10:01,080
We should ignore everyone else.

155
00:10:01,700 --> 00:10:03,050
There you go.

156
00:10:03,050 --> 00:10:04,040
You kids are lucky.

157
00:10:05,980 --> 00:10:07,290
No, not yet.

158
00:10:09,930 --> 00:10:11,790
I won't let him leave.

159
00:10:13,000 --> 00:10:14,420
Kid...

160
00:10:14,420 --> 00:10:15,690
Join the spiders.

161
00:10:16,510 --> 00:10:18,300
Team up with me.

162
00:10:18,670 --> 00:10:19,860
No way.

163
00:10:19,860 --> 00:10:23,810
I'd rather die than join you guys!

164
00:10:25,600 --> 00:10:27,880
He really hates us...

165
00:10:27,880 --> 00:10:29,810
You're an Enhancer, right?

166
00:10:30,670 --> 00:10:32,060
What if I am?!

167
00:10:32,490 --> 00:10:34,380
I knew it!

168
00:10:41,140 --> 00:10:42,130
Hey!

169
00:10:42,130 --> 00:10:45,080
We're keeping them here 
until the boss gets back.

170
00:10:45,800 --> 00:10:47,630
I'm going to recommend we recruit them.

171
00:10:47,630 --> 00:10:49,040
Are you serious?

172
00:10:49,040 --> 00:10:51,330
The boss will never agree to that.

173
00:10:52,590 --> 00:10:54,100
Well, it's your call.

174
00:10:54,560 --> 00:10:56,090
But you have to watch them.

175
00:10:57,110 --> 00:10:59,250
You can't blame us if they escape.

176
00:11:04,530 --> 00:11:07,190
He reminds you of Uvo?

177
00:11:07,190 --> 00:11:10,170
Yeah, he wears his emotions on his sleeve.

178
00:11:10,170 --> 00:11:13,060
And when he's angry, 
he forgets about consequences.

179
00:11:13,470 --> 00:11:15,050
Most of all,

180
00:11:15,050 --> 00:11:18,570
he's strongest when fighting 
for someone else.

181
00:11:19,490 --> 00:11:22,960
He did like to fight alone.

182
00:11:22,960 --> 00:11:24,360
But he was strongest

183
00:11:24,360 --> 00:11:27,830
when he'd team up with 
Nobunaga to fight a horde.

184
00:11:28,380 --> 00:11:30,280
Uvo never admitted it,

185
00:11:30,280 --> 00:11:33,930
but he preferred having 
someone to protect.

186
00:11:33,930 --> 00:11:36,890
Then, he was stronger with 
someone holding him back.

187
00:11:36,890 --> 00:11:39,090
Well, I guess you could say that.

188
00:11:39,360 --> 00:11:42,130
Shizuku is always so uncharitable...

189
00:11:42,130 --> 00:11:45,940
On the other hand, Nobunaga's 
skills are superior for duels.

190
00:11:45,940 --> 00:11:48,850
You could even say that they're 
really only effective one-on-one.

191
00:11:49,670 --> 00:11:53,590
Whenever they had assignments to fight 
together, they always complained.

192
00:11:53,590 --> 00:11:55,820
But in truth, they seemed to enjoy it.

193
00:11:55,820 --> 00:11:56,360
Okay,

194
00:11:56,770 --> 00:11:58,680
that's enough reminiscing.

195
00:11:58,680 --> 00:12:00,110
Let's get down to business.

196
00:12:00,430 --> 00:12:02,970
Here's a list of Nostrade Family members

197
00:12:02,970 --> 00:12:05,620
taken from the Hunter website.

198
00:12:06,110 --> 00:12:08,680
These five are particularly important.

199
00:12:08,680 --> 00:12:12,560
They serve as bodyguards for 
the daughter of the boss.

200
00:12:12,560 --> 00:12:14,630
And they're the ones who captured Uvo.

201
00:12:15,450 --> 00:12:19,450
But Uvo said that the chain 
user wasn't on this list.

202
00:12:19,960 --> 00:12:22,120
Since we don't know the 
chain user's appearance,

203
00:12:22,120 --> 00:12:23,880
we'll have to find someone who does.

204
00:12:24,180 --> 00:12:26,580
Oh, that's the guy I killed.

205
00:12:26,580 --> 00:12:28,890
Cross out the top left guy.

206
00:12:29,320 --> 00:12:31,330
Move around in pairs,

207
00:12:31,330 --> 00:12:34,370
and search for the people on that list.

208
00:12:35,430 --> 00:12:39,410
Then we'll meet back here 
at 10 PM. That's all.

209
00:12:40,830 --> 00:12:41,650
Wait.

210
00:12:42,220 --> 00:12:44,060
Nobunaga's staying here, right?

211
00:12:44,060 --> 00:12:45,030
What should I do?

212
00:12:46,120 --> 00:12:48,530
You can team up with whoever's left.

213
00:13:00,900 --> 00:13:02,170
Ow...

214
00:13:02,740 --> 00:13:03,720
I was a fool.

215
00:13:04,220 --> 00:13:06,100
If I'd made a move,

216
00:13:06,100 --> 00:13:09,120
Hisoka wouldn't have hesitated to kill me.

217
00:13:09,990 --> 00:13:11,310
That murderous aura...

218
00:13:11,640 --> 00:13:12,640
I was frozen in place.

219
00:13:13,560 --> 00:13:16,500
If Gon had suffered a lethal blow,

220
00:13:17,030 --> 00:13:19,670
would I have been able to move?

221
00:13:20,130 --> 00:13:21,570
That's impossible.

222
00:13:23,080 --> 00:13:28,320
Your only concern when you meet someone 
is whether you should kill them.

223
00:13:28,590 --> 00:13:29,580
You're wrong...

224
00:13:29,900 --> 00:13:32,490
"Never fight an enemy you can't beat."

225
00:13:32,830 --> 00:13:35,380
I drilled that into you...

226
00:13:35,870 --> 00:13:36,710
You're wrong!

227
00:13:43,670 --> 00:13:45,340
Scary...

228
00:13:46,020 --> 00:13:48,130
You look like you want to kill me.

229
00:13:49,550 --> 00:13:51,220
Let me warn you first.

230
00:13:51,220 --> 00:13:54,730
Step into range, and I'll cut you down.

231
00:13:57,070 --> 00:13:58,230
Killua!

232
00:14:32,550 --> 00:14:34,550
I stand no chance against him.

233
00:14:35,040 --> 00:14:37,270
I have to think of a way to escape...

234
00:14:38,500 --> 00:14:39,800
No windows.

235
00:14:40,790 --> 00:14:42,240
One exit.

236
00:14:42,920 --> 00:14:46,780
How can we get him to move away?

237
00:14:50,570 --> 00:14:53,280
Is Leorio okay?

238
00:14:53,740 --> 00:14:56,930
I hope he was able to find Zepile-san.

239
00:14:58,120 --> 00:14:59,920
Killua, are you okay?

240
00:15:00,930 --> 00:15:01,920
Yeah.

241
00:15:07,710 --> 00:15:08,690
Hey,

242
00:15:08,690 --> 00:15:13,340
Zepile-san taught us welding, exposing...

243
00:15:13,340 --> 00:15:14,860
What else was there?

244
00:15:15,530 --> 00:15:16,430
I forgot.

245
00:15:16,820 --> 00:15:19,260
Side-stomping, maybe?

246
00:15:19,260 --> 00:15:21,510
I don't think that was it...

247
00:15:21,510 --> 00:15:22,160
Gon...

248
00:15:24,000 --> 00:15:25,570
I'll act as a decoy.

249
00:15:26,000 --> 00:15:28,690
Use that chance to escape.

250
00:15:29,840 --> 00:15:31,200
What are you talking about?

251
00:15:31,480 --> 00:15:32,730
Seriously.

252
00:15:33,130 --> 00:15:34,330
Forget it.

253
00:15:34,990 --> 00:15:38,610
You're smart enough to know 
how much stronger I am.

254
00:15:39,100 --> 00:15:40,830
There won't be any chances.

255
00:15:41,350 --> 00:15:43,330
I already know that!

256
00:15:44,160 --> 00:15:47,060
This room only has one exit 
and nowhere to hide.

257
00:15:47,440 --> 00:15:49,570
He can see everything we're doing.

258
00:15:49,570 --> 00:15:53,090
We're facing someone who can watch 
us without creating an opening.

259
00:15:54,030 --> 00:15:55,990
He's probably an iaido master.

260
00:15:55,990 --> 00:15:59,570
With his shodachi, his range 
is at least two times mine.

261
00:16:00,240 --> 00:16:03,350
If I take one step too close, 
he'll strike a fatal blow.

262
00:16:04,060 --> 00:16:05,740
But that's the point!

263
00:16:06,120 --> 00:16:08,190
You can't do it.

264
00:16:09,240 --> 00:16:10,420
Shut up!

265
00:16:11,510 --> 00:16:13,820
I won't know until I try!

266
00:16:16,920 --> 00:16:19,330
Killua, what are you thinking?

267
00:16:19,760 --> 00:16:23,080
I'll stop his shodachi, even if it kills me.

268
00:16:23,080 --> 00:16:25,140
Use that chance to run away!

269
00:16:27,940 --> 00:16:28,590
Huh?

270
00:16:29,030 --> 00:16:30,640
What was that for?!

271
00:16:30,640 --> 00:16:32,320
Don't be so selfish!

272
00:16:33,420 --> 00:16:36,170
Don't talk about dying like it's nothing!

273
00:16:36,170 --> 00:16:37,250
What?!

274
00:16:37,250 --> 00:16:39,350
You did the same thing earlier!

275
00:16:39,350 --> 00:16:40,600
I'm allowed to do it!

276
00:16:40,930 --> 00:16:42,480
But you aren't!

277
00:16:42,910 --> 00:16:43,980
Huh?

278
00:16:46,860 --> 00:16:49,860
You can't use logic against an Enhancer...

279
00:16:50,440 --> 00:16:54,330
We can't escape unless we're 
prepared to die, stupid!

280
00:16:54,330 --> 00:16:56,860
You have no idea what I was thinking...

281
00:16:56,860 --> 00:16:58,120
Who's the selfish one?!

282
00:16:58,120 --> 00:17:00,490
Yes, I've no idea... 
Because I'm stupid!

283
00:17:02,880 --> 00:17:05,120
You kids are hilarious!

284
00:17:05,120 --> 00:17:07,520
Hey, I'm not gonna hurt you or anything.

285
00:17:07,520 --> 00:17:09,310
Just behave yourselves.

286
00:17:09,310 --> 00:17:11,260
I know you're serious.

287
00:17:11,760 --> 00:17:13,270
Don't throw your lives away.

288
00:17:14,640 --> 00:17:16,710
Wait until the boss gets back.

289
00:17:17,160 --> 00:17:20,760
If he won't give his approval, you can leave.

290
00:17:22,050 --> 00:17:24,330
But if you try to escape, I'll kill you.

291
00:17:25,140 --> 00:17:26,990
Don't make me draw my sword.

292
00:17:27,470 --> 00:17:29,650
If I pull it out, you'll die...

293
00:17:32,650 --> 00:17:34,240
I remember now!

294
00:17:34,240 --> 00:17:36,360
It was side-stepping!

295
00:17:36,360 --> 00:17:38,160
Welding, exposing...

296
00:17:38,770 --> 00:17:40,660
And the last one was side-stepping!

297
00:17:43,950 --> 00:17:45,350
Do you remember now?

298
00:17:45,350 --> 00:17:47,540
Yeah, I sure do!

299
00:17:47,880 --> 00:17:48,850
See?

300
00:17:48,850 --> 00:17:51,550
How could we forget something so simple?

301
00:17:51,890 --> 00:17:52,610
Huh?

302
00:17:54,260 --> 00:17:56,000
Now that we've figured that out...

303
00:17:56,720 --> 00:17:57,550
Let's go!

304
00:17:57,900 --> 00:17:59,090
Are you serious?

305
00:17:59,900 --> 00:18:01,310
You're going to die.

306
00:18:01,310 --> 00:18:04,200
I'm not skilled or merciful enough

307
00:18:04,200 --> 00:18:06,380
to control my strength.

308
00:18:09,850 --> 00:18:10,810
Fools!

309
00:18:15,660 --> 00:18:17,740
If the exit is blocked...

310
00:18:18,120 --> 00:18:20,150
Make your own!

311
00:18:21,180 --> 00:18:22,260
The wall...

312
00:18:22,880 --> 00:18:25,450
Tsk, which one do I chase?

313
00:18:25,450 --> 00:18:26,660
We're on the fifth floor.

314
00:18:27,240 --> 00:18:29,630
The kid with slanted eyes 
is already close to the stairs.

315
00:18:30,400 --> 00:18:31,380
So the black-haired kid!

316
00:18:33,470 --> 00:18:37,010
Now he has nowhere to run.

317
00:18:38,360 --> 00:18:39,390
Am I an idiot?

318
00:18:40,110 --> 00:18:42,760
They can break through walls to escape!

319
00:18:45,240 --> 00:18:46,500
The slanted-eyed kid is below.

320
00:18:46,990 --> 00:18:48,770
I'll have to follow the black-haired one.

321
00:18:52,820 --> 00:18:53,770
Where is he?

322
00:18:54,380 --> 00:18:56,420
The walls are intact...

323
00:18:56,420 --> 00:18:57,400
So is the door.

324
00:18:57,400 --> 00:18:59,280
Killua, are you there?

325
00:19:01,000 --> 00:19:02,780
Yeah, I'm here!

326
00:19:03,540 --> 00:19:04,820
That's it!

327
00:19:04,820 --> 00:19:07,320
That last hole was a decoy!

328
00:19:07,320 --> 00:19:11,320
He broke it before hiding in the nearby room,

329
00:19:11,320 --> 00:19:12,780
{\fad(1,501)}waiting for me to pass,

330
00:19:13,360 --> 00:19:14,800
before stepping into the hallway.

331
00:19:15,230 --> 00:19:17,800
We can beat him together!

332
00:19:22,060 --> 00:19:25,060
<i>His presence disappeared...</i> Zetsu?

333
00:19:25,660 --> 00:19:28,560
Does the idiot want to fight me?

334
00:19:29,330 --> 00:19:31,980
<i>An advanced application of</i> Ten <i>and</i> Ren.

335
00:19:34,340 --> 00:19:35,270
En!

336
00:19:35,020 --> 00:19:36,070
En

337
00:19:36,070 --> 00:19:38,780
Normally, you shroud your body in aura

338
00:19:38,780 --> 00:19:41,760
that extends a few 
millimeters from your body.

339
00:19:41,760 --> 00:19:44,830
<i>But with</i> En<i>, you extend that 
aura as far as necessary.</i>

340
00:19:45,700 --> 00:19:48,590
<i>You can sense anyone who 
steps inside your</i> En.

341
00:19:48,590 --> 00:19:51,530
Inside it, you can even count 
the leaves as they fall.

342
00:19:53,780 --> 00:19:56,710
You can conceal your presence and approach,

343
00:19:56,710 --> 00:19:59,150
never making a sound.

344
00:19:59,150 --> 00:20:02,220
<i>But step within my </i>En, 
<i>and I'll swing my sword.</i>

345
00:20:03,300 --> 00:20:05,220
Come to me.

346
00:20:07,190 --> 00:20:08,220
Stupid.

347
00:20:08,220 --> 00:20:11,680
Man, I wanted to beat him up.

348
00:20:11,680 --> 00:20:13,320
That was impossible.

349
00:20:13,320 --> 00:20:15,230
He'd have fought you off and killed you.

350
00:20:15,470 --> 00:20:17,190
One hundred percent guaranteed?

351
00:20:17,190 --> 00:20:18,980
We don't have a chance

352
00:20:19,160 --> 00:20:21,020
only knowing the basics of <i>Nen</i>.

353
00:20:23,280 --> 00:20:24,320
What is it?

354
00:20:24,320 --> 00:20:26,250
You finally sound like yourself again.

355
00:20:26,720 --> 00:20:28,820
It's my job to say crazy stuff.

356
00:20:28,820 --> 00:20:31,720
Your job is to stay cool and stop me.

357
00:20:32,610 --> 00:20:34,390
So I'm counting on you!

358
00:20:34,390 --> 00:20:35,750
You're so selfish.

359
00:20:36,390 --> 00:20:39,020
Well? What do we do next?

360
00:20:39,020 --> 00:20:41,260
That depends on what you want to do.

361
00:20:41,570 --> 00:20:44,020
I want to defeat those guys.

362
00:20:44,020 --> 00:20:47,640
Well, that matches our original objective.

363
00:20:48,340 --> 00:20:52,640
But we'll have to improve our <i>Nen</i> to do that.

364
00:20:52,640 --> 00:20:55,960
<i>Nen</i> has the potential to give us the power

365
00:20:55,960 --> 00:20:57,610
we'd need to stand a chance.

366
00:20:59,920 --> 00:21:04,150
And the fastest way to learn more 
would be to ask Kurapika.

367
00:21:04,150 --> 00:21:06,100
Huh? Why?

368
00:21:06,100 --> 00:21:10,390
Because Kurapika is the chain user 
who defeated one of them.

369
00:21:10,390 --> 00:21:11,290
Huh?

370
00:21:11,290 --> 00:21:13,300
You didn't realize it?

371
00:21:13,300 --> 00:21:15,170
Is that really true?

372
00:21:15,680 --> 00:21:17,170
I'm pretty confident.

373
00:21:17,170 --> 00:21:19,920
If it's true, we have a chance.

374
00:21:20,980 --> 00:21:24,170
Because we learned <i>Nen</i> around 
the same time Kurapika did.

375
00:21:24,630 --> 00:21:26,410
We need to learn the techniques

376
00:21:26,410 --> 00:21:28,050
appropriate to our abilities.

377
00:21:28,730 --> 00:21:30,310
And most importantly,

378
00:21:30,310 --> 00:21:33,680
we need the strength to fight the Troupe.

379
00:21:33,680 --> 00:21:37,930
And Kurapika is the key to 
finding those answers?

380
00:21:37,980 --> 00:21:38,610
Yeah.

381
00:21:44,100 --> 00:21:47,210
At the same time, a new storm brews

382
00:21:47,210 --> 00:21:50,280
at the heart of Yorknew City.

383
00:23:01,110 --> 00:23:06,150
Brush Your Teeth. 
Unripe Fruit

384
00:23:01,110 --> 00:23:06,320
Gon and Killua's Hunterpedia

385
00:23:01,400 --> 00:23:02,950
Coming up, we have Gon

386
00:23:02,950 --> 00:23:03,650
and Killua's

387
00:23:03,650 --> 00:23:05,300
Hunterpedia!

388
00:23:06,630 --> 00:23:07,530
It's Gon

389
00:23:07,530 --> 00:23:08,360
and Killua's

390
00:23:08,360 --> 00:23:09,610
Hunterpedia!

391
00:23:08,610 --> 00:23:20,250
Gon and Killua's Hunterpedia

392
00:23:09,610 --> 00:23:11,610
Today, we introduce Franklin,

393
00:23:11,010 --> 00:23:12,700
A Phantom Troupe member.

394
00:23:11,610 --> 00:23:12,640
a member of the Phantom Troupe.

395
00:23:12,640 --> 00:23:14,010
He's an Emitter.

396
00:23:12,700 --> 00:23:14,370
An Emitter.

397
00:23:14,010 --> 00:23:16,690
His attack Double Machine Gun fires <i>Nen</i> bullets.

398
00:23:14,370 --> 00:23:20,250
Transforms hands into 
machine guns that fire 
<i>Nen</i> bullets.

399
00:23:16,690 --> 00:23:19,310
Retool your fingers for an extra punch!

400
00:23:27,460 --> 00:23:29,790
Next time: A x Brutal x Battlefield.

401
00:23:29,790 --> 00:23:30,750
Out.

402
00:23:30,750 --> 00:23:31,500
Out?

403
00:23:31,500 --> 00:23:32,720
Outfield!

404
00:23:32,720 --> 00:23:33,720
Infield!

