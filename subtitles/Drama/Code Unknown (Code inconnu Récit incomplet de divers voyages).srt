﻿1
00:02:45,415 --> 00:02:47,624
My God, forgive me.

2
00:06:48,699 --> 00:06:52,994
Teacher, all four are dead.

3
00:06:53,871 --> 00:06:56,957
The Sénéchaux and the
Grand Master himself.

4
00:06:57,125 --> 00:07:00,210
Then I assume
you have the location.

5
00:07:00,378 --> 00:07:03,505
Confirmed by all.

6
00:07:03,673 --> 00:07:06,091
Independently.

7
00:07:06,843 --> 00:07:11,179
I had feared the Priory's penchant
for secrecy might prevail.

8
00:07:14,725 --> 00:07:18,854
The prospect of death
is strong motivation.

9
00:07:22,275 --> 00:07:24,693
It is here.

10
00:07:24,944 --> 00:07:27,362
In Paris, Teacher.

11
00:07:28,322 --> 00:07:33,785
It hides beneath the Rose
in Saint-Sulpice.

12
00:07:34,287 --> 00:07:37,038
You will go forth, Silas.

13
00:08:49,987 --> 00:08:53,448
I chastise my body.

14
00:14:06,178 --> 00:14:09,889
Silas has succeeded.
The legend is true.

15
00:14:10,182 --> 00:14:12,100
It hides beneath the Rose.

16
00:14:12,393 --> 00:14:15,436
My part of our bargain
is nearly fulfilled.

17
00:14:15,938 --> 00:14:19,023
I meet the council in an hour.

18
00:14:19,275 --> 00:14:23,069
I will have your money tonight,
Teacher.

19
00:15:50,366 --> 00:15:53,868
Precisely.

20
00:15:56,580 --> 00:15:58,706
Officer Neveu.

21
00:16:00,542 --> 00:16:01,584
This is not the time.

22
00:16:01,752 --> 00:16:03,878
I received the crime-scene JPEGs
at headquarters...

23
00:16:04,046 --> 00:16:05,880
...and I've deciphered the code.

24
00:16:06,215 --> 00:16:07,799
It's a Fibonacci sequence.

25
00:16:07,967 --> 00:16:10,385
That's the code Saunière
left on the floor.

26
00:16:10,719 --> 00:16:12,845
Headquarters sent me
to explain, captain.

27
00:16:14,473 --> 00:16:15,974
The numbers are out of order.

28
00:16:52,344 --> 00:16:54,554
Hello, you've reached
the home of Sophie Neveu.

29
00:17:40,476 --> 00:17:42,018
Church of Saint-Sulpice.

30
00:17:42,269 --> 00:17:43,895
Good evening, Sister.

31
00:17:44,897 --> 00:17:48,691
I need you to show someone
our church tonight.

32
00:17:49,109 --> 00:17:50,526
Of course, Father.

33
00:17:51,070 --> 00:17:52,779
But so late?

34
00:17:53,989 --> 00:17:55,031
Wouldn't tomorrow...?

35
00:17:55,199 --> 00:17:59,952
This is a request from an
important bishop of Opus Dei.

36
00:18:00,662 --> 00:18:03,122
It would be my pleasure.

37
00:20:38,487 --> 00:20:41,280
He's still in there?
What's he doing?

38
00:23:11,306 --> 00:23:14,141
Saunière was reading his book.

39
00:23:14,518 --> 00:23:16,477
"Blood trail"

40
00:23:18,397 --> 00:23:20,231
Excuse me, captain.

41
00:23:20,399 --> 00:23:24,485
Crypto called.
They identified the code.

42
00:23:24,653 --> 00:23:26,487
Neveu told us already.

43
00:23:26,655 --> 00:23:30,491
I should have fired her on the spot,
barging in like that.

44
00:23:30,659 --> 00:23:32,618
Yes.

45
00:23:32,786 --> 00:23:35,329
Except they didn't send Neveu.

46
00:23:37,749 --> 00:23:38,916
What?

47
00:23:39,084 --> 00:23:42,503
Captain, look at this.

48
00:23:44,172 --> 00:23:45,464
He jumped!

49
00:23:45,841 --> 00:23:46,924
Shit.

50
00:23:49,219 --> 00:23:52,346
He's moving again. And fast.

51
00:23:52,931 --> 00:23:54,723
He must be in a car.

52
00:23:56,143 --> 00:24:00,354
He's going south
on Pont du CarrouSel.

53
00:24:09,030 --> 00:24:10,364
Bastard.

54
00:27:53,672 --> 00:27:58,634
Look at this. He must have
thrown it from the window.

55
00:27:58,968 --> 00:28:00,594
Smart to hit the truck.

56
00:28:00,970 --> 00:28:04,515
What, you admire him now?

57
00:28:06,101 --> 00:28:08,227
We're stupid.
Who did we leave at the museum?

58
00:28:08,395 --> 00:28:11,146
Ledoux? Get him on the radio!

59
00:30:05,637 --> 00:30:07,513
Stay where you are!

60
00:30:09,307 --> 00:30:10,849
Let go of the painting...

61
00:30:11,017 --> 00:30:12,810
...and put it on the ground.

62
00:30:14,896 --> 00:30:18,982
No. You put your gun down.

63
00:30:19,150 --> 00:30:21,276
Now!

64
00:30:21,444 --> 00:30:23,070
Or else I will destroy the painting.

65
00:30:25,907 --> 00:30:27,407
Hurry up!

66
00:30:27,575 --> 00:30:29,409
Slide your gun to me.

67
00:30:32,247 --> 00:30:33,747
Be careful.

68
00:30:33,915 --> 00:30:35,874
I never really liked this painting.

69
00:34:10,715 --> 00:34:12,966
Christ, give me strength.

70
00:34:25,521 --> 00:34:29,524
You are a ghost.

71
00:34:31,903 --> 00:34:34,154
Christ, give me strength.

72
00:36:09,083 --> 00:36:11,710
Stealing in a house of God!

73
00:36:26,058 --> 00:36:28,059
You are an angel.

74
00:36:30,521 --> 00:36:32,772
Christ, give me strength.

75
00:38:24,468 --> 00:38:27,971
Gray Smart Car. Black roof.

76
00:38:28,139 --> 00:38:31,725
Corner of Denain Boulevard
at the train station.

77
00:38:31,892 --> 00:38:33,143
How could you let them get away?

78
00:38:33,311 --> 00:38:36,271
Why not just shoot the damn painting?
Imbecile!

79
00:38:36,439 --> 00:38:39,316
You should put his cigar
out in your hand!

80
00:38:39,483 --> 00:38:40,859
Get out of here!

81
00:38:42,862 --> 00:38:48,616
They found Neveu's car
abandoned at the train station.

82
00:38:49,118 --> 00:38:54,039
And two tickets to Brussels paid for
with Langdon's credit card.

83
00:38:54,248 --> 00:38:55,749
A decoy, I'm sure.

84
00:38:55,916 --> 00:38:58,376
All the same,
send an officer to the station.

85
00:38:58,586 --> 00:39:01,254
Question all the taxi drivers.
I'll put this on the wire.

86
00:39:01,422 --> 00:39:04,174
Interpol? We're not sure he's guilty.

87
00:39:04,592 --> 00:39:08,386
I know he's guilty. Beyond a doubt.

88
00:39:08,596 --> 00:39:11,514
Robert Langdon is guilty.

89
00:40:42,231 --> 00:40:43,565
Police.

90
00:40:44,984 --> 00:40:46,317
What do you want?

91
00:41:01,125 --> 00:41:03,209
Fifty euros for all your stuff.

92
00:41:05,421 --> 00:41:07,547
Go and get something to eat.

93
00:45:03,075 --> 00:45:04,659
This is Jacques Saunière.

94
00:45:05,035 --> 00:45:07,870
Please leave a message
after the tone.

95
00:45:08,580 --> 00:45:10,957
Please, Monsieur Saunière,
pick up the phone.

96
00:45:11,208 --> 00:45:12,709
This is Sandrine Bieil.

97
00:45:12,960 --> 00:45:15,211
I have called the list.

98
00:45:15,629 --> 00:45:17,547
I fear the other guardians are dead.

99
00:45:18,132 --> 00:45:20,049
The lie has been told.

100
00:45:20,384 --> 00:45:22,301
The floor panel has been broken.

101
00:45:22,511 --> 00:45:25,972
Please, monsieur, pick up the phone.
I beg you.

102
00:46:34,082 --> 00:46:36,793
Come, you saints of God.

103
00:46:37,711 --> 00:46:40,379
Hasten, angels of the Lord.

104
00:46:41,089 --> 00:46:43,883
To receive her soul.

105
00:46:44,301 --> 00:46:48,888
And bring her to the sight
of the Almighty.

106
00:46:52,810 --> 00:46:57,438
In the name of the Father,
the Son, and the Holy Spirit.

107
00:47:33,684 --> 00:47:35,101
May Christ be with you.

108
00:47:35,269 --> 00:47:37,061
And also with you.

109
00:50:53,800 --> 00:50:56,343
Two prostitutes identified
Langdon and Neveu...

110
00:50:56,511 --> 00:50:58,763
...getting into a taxi
in the Bois de Boulogne.

111
00:51:47,479 --> 00:51:49,271
How may I help you?

112
00:51:55,153 --> 00:51:57,446
The door to the right, please.

113
00:52:54,963 --> 00:52:57,631
Good evening. I am André Vernet,
the night manager.

114
00:52:59,134 --> 00:53:02,428
I take it this is your first visit
to our establishment?

115
00:56:51,658 --> 00:56:54,284
Good evening, sir. Police.

116
00:56:54,702 --> 00:56:58,831
I just drive from here to Zurich.
Not French, English?

117
00:58:19,078 --> 00:58:22,789
I firmly resolve, with the help of
thy grace, to confess my sins...

118
00:58:37,013 --> 00:58:39,598
...to do penance
and to amend my life.

119
00:58:40,016 --> 00:58:41,058
Amen.

120
00:59:01,287 --> 00:59:03,664
I chastise my body.

121
01:01:56,129 --> 01:01:58,004
Feeling better, Sophie?

122
01:06:52,258 --> 01:06:55,093
It seems you're not a driver at all.

123
01:06:56,929 --> 01:07:01,725
Apparently, you lost your tongue
along with your truck.

124
01:07:04,937 --> 01:07:09,149
Aiding and abetting
two murder suspects.

125
01:07:09,316 --> 01:07:11,693
That carries jail time.

126
01:07:12,278 --> 01:07:14,070
Speak to my lawyer.

127
01:07:15,740 --> 01:07:22,746
All this confusion, violence,
vanishing property...

128
01:07:22,913 --> 01:07:29,127
It might get around that your bank is
less than ideal, don't you think?

129
01:07:31,213 --> 01:07:33,923
You think you're in pain now,
André Vernet?

130
01:07:34,300 --> 01:07:38,303
My cause is worth your life.
Understand?

131
01:07:42,641 --> 01:07:44,267
What do you want?

132
01:07:44,560 --> 01:07:49,814
Your truck carries a homing device.
Activate it.

133
01:10:04,617 --> 01:10:07,285
The truck's signal is coming online.

134
01:10:07,453 --> 01:10:08,912
It's about time.

135
01:10:12,625 --> 01:10:14,292
Locked on and tracking, sir.

136
01:10:17,296 --> 01:10:20,506
Very good. Tell Collet not to move in
until I get there.

137
01:10:21,050 --> 01:10:25,595
Attention! All of Collet's units
to Château Villette.

138
01:10:26,472 --> 01:10:29,807
The suspects Neveu and Langdon
are likely at that location.

139
01:11:53,225 --> 01:11:57,061
It's an honor to welcome you...

140
01:11:57,646 --> 01:11:59,731
...even though it's late.

141
01:12:40,105 --> 01:12:42,315
Château Villette. Yes.

142
01:29:38,456 --> 01:29:40,165
Fache says to wait, so I wait.

143
01:29:40,416 --> 01:29:44,503
What's Fache thinking?
The truck is here. They're inside.

144
01:32:32,713 --> 01:32:35,131
Rip the gate down.

145
01:34:34,001 --> 01:34:35,377
Shit.

146
01:36:51,305 --> 01:36:53,890
What the hell do you mean,
you lost them? Collet.

147
01:36:55,976 --> 01:36:57,435
You're the one who lost them.

148
01:36:57,812 --> 01:36:59,354
You control every step
of this investigation.

149
01:36:59,522 --> 01:37:01,940
You don't let anybody breathe.

150
01:37:02,441 --> 01:37:05,151
You're acting like
you lost your mind.

151
01:37:08,531 --> 01:37:11,407
What is it with these two birds?

152
01:37:14,995 --> 01:37:17,872
Interpol just registered a new
flight plan from Le Bourget.

153
01:37:18,749 --> 01:37:22,001
Stay out of my way on this, Collet.

154
01:42:58,922 --> 01:43:00,715
I need the flight plan.

155
01:43:01,008 --> 01:43:02,049
Ten minutes.

156
01:43:03,010 --> 01:43:05,386
I asked you to get it for me.

157
01:43:06,513 --> 01:43:08,431
I'm on break.

158
01:43:09,892 --> 01:43:11,475
Come back in 10 minutes.

159
01:43:13,478 --> 01:43:15,646
My nose! My nose!

160
01:43:16,231 --> 01:43:17,607
The flight plan, please.

161
01:43:17,774 --> 01:43:18,816
You asshole!

162
01:46:55,951 --> 01:47:00,079
I suppose this is a new technique
for investigations.

163
01:47:00,247 --> 01:47:05,376
I've lost them.
They flew to Switzerland.

164
01:47:05,627 --> 01:47:07,837
No extradition.

165
01:47:10,590 --> 01:47:16,887
The controller filed charges.
Ari was on dispatch. He called me.

166
01:47:20,976 --> 01:47:24,061
What's going on, Bezu?

167
01:47:27,065 --> 01:47:29,942
You know that I am Opus Dei?

168
01:47:30,318 --> 01:47:31,861
Yes.

169
01:47:37,159 --> 01:47:40,703
A bishop of my order called me.

170
01:47:40,996 --> 01:47:43,122
He said a killer
came to him in confession.

171
01:47:43,290 --> 01:47:46,041
His name was Robert Langdon.

172
01:47:47,210 --> 01:47:50,379
He said I couldn't imagine
the evil in this man's heart.

173
01:47:50,547 --> 01:47:53,257
That he would keep killing.

174
01:47:53,633 --> 01:47:56,093
He said I had to stop him.

175
01:47:58,138 --> 01:48:01,974
The bishop broke his vows
to tell me this.

176
01:48:02,142 --> 01:48:05,561
He charged me
to stop Robert Langdon.

177
01:48:08,523 --> 01:48:13,277
Tell me, Collet, who have I failed?

178
01:48:14,070 --> 01:48:15,905
The bishop?

179
01:48:17,199 --> 01:48:19,283
God himself?

180
01:48:27,918 --> 01:48:31,045
They've changed their flight plan
to London.

181
01:48:36,927 --> 01:48:41,430
Go on, Fache.
I'll take care of the controller.

182
01:48:41,598 --> 01:48:46,727
Maybe he needs a few extra euros.

183
01:48:47,604 --> 01:48:51,357
Just tell me next time.

184
01:52:18,690 --> 01:52:21,275
Do you have the bonds, bishop?

185
01:52:21,484 --> 01:52:22,735
Yes, I do, Teacher.

186
01:52:22,902 --> 01:52:26,238
I have chosen an Opus Dei
residence for the exchange.

187
01:52:26,406 --> 01:52:27,865
I am honored.

188
01:52:28,116 --> 01:52:32,953
By the time you get to London,
I will have the Grail.

189
01:59:04,470 --> 01:59:06,179
Lieutenant!

190
01:59:06,347 --> 01:59:07,931
One flag off the prints.

191
01:59:11,602 --> 01:59:13,770
Remy Legaludec.

192
01:59:13,938 --> 01:59:15,522
Petty theft.

193
01:59:17,233 --> 01:59:21,027
Skipped out on a hospital bill
for a tracheotomy.

194
01:59:22,905 --> 01:59:24,739
Peanut allergy.

195
01:59:26,159 --> 01:59:27,576
Like Marie.

196
01:59:27,743 --> 01:59:29,828
This way. Marie?

197
01:59:29,996 --> 01:59:32,247
My daughter.

198
01:59:32,415 --> 01:59:34,040
Over here.

199
01:59:40,715 --> 01:59:41,798
Where?

200
01:59:41,966 --> 01:59:43,258
Up there.

201
02:00:00,610 --> 02:00:02,277
What's all that?

202
02:00:02,445 --> 02:00:04,821
Very advanced surveillance.

203
02:00:09,535 --> 02:00:10,869
It's a listening post.

204
02:00:20,296 --> 02:00:22,505
Get me Fache.

205
02:00:22,673 --> 02:00:27,636
Four of these names are
the men killed last night.

206
02:01:13,516 --> 02:01:15,517
I understand.

207
02:10:09,551 --> 02:10:11,594
I am a ghost.

208
02:19:07,005 --> 02:19:08,589
Leave us, please.

209
02:25:13,538 --> 02:25:15,330
Come along, Sophie.

210
02:31:26,035 --> 02:31:28,328
Sophie, where are you, princess?

211
02:31:45,221 --> 02:31:46,930
I told you, no.

212
02:31:49,350 --> 02:31:50,850
But why can't I?

213
02:31:56,982 --> 02:31:58,817
They're dead. Dead and buried.

214
02:31:59,318 --> 02:32:00,985
Never look for them, Sophie.

215
02:32:01,237 --> 02:32:02,445
Promise me.

216
02:32:03,906 --> 02:32:05,031
Swear it!

217
02:32:05,741 --> 02:32:06,866
Swear it to me!

