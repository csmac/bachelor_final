{415}{482}- Stretcher!|- Let's check it out.
{486}{531}All right, mate.|You're with us.
{536}{573}All right. Stretcher!
{613}{646}Gently.
{648}{686}All right, up!
{715}{788}That was a good day's work,|McAndrew.
{790}{827}That was a good day's work.
{829}{890}Get away from me.
{892}{951}Will you not look me in the eye|when I'm speaking to you?
{1017}{1111}I know how to look at people|without blinking as well.
{1199}{1262}In all my godforsaken life,|I've never known...
{1264}{1345}what it was like to want|to kill somebody until now.
{1405}{1480}You're a brave man,Joe,|a brave man.
{1551}{1611}Gerard Conlon, back on the threes.
{1611}{1700}- Stand your ground.|- Hawkins, returning to his cell.
{1702}{1772}Benjamin Bailey, back to him cell!
{1774}{1826}Burns, returning to his cell.
{1828}{1901}7445, Casey, back to his cell.
{1903}{1967}6309,Johnston, level three.
{1969}{2039}Regis, 113,|going up to number two.
{2477}{2542}- Back to your cell now!|- Keep moving.
{2567}{2615}Barker was maimed for life...
{2617}{2679}and Joe was sent|to another prison.
{2681}{2729}He's in solitary confinement|somewhere.
{2731}{2794}We've had no news of him|since then.
{2796}{2852}The new chief screw|had the yard painted...
{2854}{2902}and I was back|walking in circles again.
{2904}{2946}Back to the cells.
{3013}{3071}Come on. Move.
{3761}{3836}I'd like to help you out|with the campaign, Da...
{3866}{3939}if that's all right with you.
{3941}{3981}Do you mean it?
{3986}{4022}Aye.
{4045}{4091}That's great.
{4093}{4180}What I need from you is|the whole story, your whole story...
{4186}{4248}in as much detail|as you can remember.
{4250}{4340}And I need you to write it down.|It's really important.
{4499}{4544}I can't do this.
{4591}{4642}I can't write this, Da.
{4644}{4704}Could your son|not give you a hand?
{4706}{4774}He's up in his cell writing away.
{5073}{5121}Tape recorder.
{5123}{5170}You're a good talker.
{5172}{5211}Talk.
{5251}{5300}The strange thing is--
{5327}{5386}the strange thing is sometimes|I think he's a little jealous...
{5386}{5451}of me taking over the campaign|and that there.
{5453}{5531}Going up and down the stairs|to meet you.
{5590}{5636}that he could be jealous.
{5711}{5811}Anyhow, the main thing is|toget the case reopened...
{5811}{5903}because I don't know how long|he can survive in here.
{6323}{6422}- I'm tired of this.|- Come on. We have to do this.
{6424}{6462}You behave yourself.
{6553}{6598}Get your head down.
{6780}{6861}When I was a wee lad, I used to wonder|what you were doing under the towel.
{6863}{6949}One day when you and Ma were out,|I got a big bowl of  boiling water...
{6951}{7036}whacked a big drop of Vick|and put the towel over my head.
{7036}{7128}I sat there trying to figure out what it|was about 'cause nothing was happening.
{7130}{7178}I figured you|must've been drinking it.
{7180}{7228}So I stuck my tongue in it.|Do you remember that?
{7321}{7370}How could I forget?
{7391}{7463}And your tongue|swelled up like a football.
{7490}{7543}Had to rush you to the hospital.
{7545}{7599}First time you'd stopped|talking in your life.
{7689}{7725}Give us that.
{7753}{7815}I'll do your chest for you, Da.
{7995}{8044}Was I always bad, was I?
{8076}{8112}Not always.
{8114}{8186}I don't deserve to spend|the rest of  my life in here, do I?
{8211}{8273}All they've done|is block out the light.
{8275}{8323}They can't block out|the light in here.
{8325}{8370}Listen.
{8372}{8413}Every night...
{8415}{8476}I take your mother's|hand in mine.
{8478}{8580}We go out the front door,|into Cyprus Street...
{8586}{8665}down to Falls Road,|up the Antrim Road...
{8702}{8752}to Cave Hill.
{8754}{8795}We look back down...
{8855}{8906}on poor, troubled Belfast.
{8991}{9061}I've been doing that every night...
{9061}{9106}for five years now.
{9161}{9213}As if I never left your mother.
{9461}{9525}What I remember most|about my childhood is...
{9527}{9577}holding your hand.
{9579}{9643}My wee hand in your big hand.
{9645}{9711}And the smell of tobacco.|I remember that...
{9711}{9791}I could smell the tobacco|of the palm of your hand.
{9876}{9972}When I want to feel happy, I try|to remember the smell of tobacco.
{10065}{10105}Hold my hand.
{10111}{10156}Get the fuck--
{10186}{10238}Don't go sentimental on me now.
{10273}{10367}Don't be upset, Da. Look,|I'll hold your hand if you like.
{10431}{10475}I'm going to die.
{10506}{10554}Don't be saying that.
{10577}{10613}I'm scared.
{10667}{10717}You've no reason to be scared.
{10719}{10836}Don't you be comforting me when I can|see the truth staring me in the face.
{10838}{10930}I'm scared I'm gonna die here|among strangers.
{10936}{10969}You're not fucking dying.
{10971}{11063}Can I not say a thing|without you fucking contradicting me?
{11098}{11227}I'm scared to leave|your mother behind.
{11277}{11361}Look, you are not going to die,|all right, Da?
{11416}{11506}Even if you do, sure I can|look after Ma all right.
{11511}{11588}You think I'd leave|Sarah in your care?
{11626}{11668}What do you mean?
{11670}{11769}You haven't the maturity to take care|of yourself, let alone your mother.
{11944}{12010}I haven't much time between|appointments, Mrs Peirce.
{12010}{12065}- How can I help you?|- Thankyou.
{12067}{12160}I'm the solicitor for the Conlons,|Chief Inspector.
{12160}{12254}Giuseppe Conlon is critically ill,|as you may know.
{12260}{12316}I've petitioned before the court|for his compassionate parole.
{12318}{12360}They want your clearance.
{12387}{12435}That'll be difficult, Mrs Peirce.
{12435}{12520}These people have committed|horrific crimes.
{12522}{12580}Society demands|that they serve their time.
{12610}{12668}But they didn't do it,|Chief Inspector.
{12670}{12710}Says who?
{12710}{12768}Say the real bombers.
{12770}{12836}They told you they did it,|Mr Dixon.
{12838}{12902}Gerry Conlon told me he did it,|Mrs Peirce.
{12904}{12969}These people are liars.
{12971}{13035}They're liars for a cause.|That's the worst kind.
{13036}{13092}But he's dying.|Giuseppe's dying.
{13094}{13170}A lot of  people are dying.|It's a dirty war.
{13285}{13335}Well, I'll see what I can do.
{13335}{13386}Is this your family, Mr. Dixon?
{13416}{13465}That's my wife and my son.
{13467}{13528}You have another appointment.
{13530}{13563}Yes.
{13597}{13674}I'll see you again, Mr. Dixon.
{14574}{14621}Are you all right, Da?
{14961}{15016}Are you all right?
{15086}{15122}Wake up, Da.
{15148}{15230}Can you hear me?|Can you hear me, Giuseppe?
{15260}{15305}Come on, dear God in heaven,|don't do this to me.
{15310}{15342}Come on. Wake up.
{15344}{15400}Wake up! Fuck's sake, come on!
{15402}{15491}There you go. Thank you.|You're all right, Da.
{15493}{15542}I'm going to get some help.
{15544}{15610}Number 73! Number 73!
{15610}{15674}Please come quickly!|My father's sick!
{15676}{15716}Benbay, Giuseppe's taken bad!
{15718}{15760}Put your arms around me.
{15760}{15838}I'm getting you out of  bed.|Put your arms around me.
{15910}{15976}- Open the fucking door!|- He's number 73!
{15978}{16022}- Open the fucking door!|- Giuseppe!
{16052}{16119}Fuck, he can't breathe.|He can't fucking breathe!
{16140}{16227}He can't breathe!|Look, my father's fucking dying!
{16229}{16304}- He needs oxygen!|- Give him a fucking break!
{16310}{16378}Open the fucking gate!|I'm right here, Da.
{16380}{16428}Get him in here!
{16430}{16490}What the fuck is going on?
{16600}{16640}Hammersmith Hospital now.
{16642}{16686}I want to go with him.
{16717}{16795}I've got to get clearance first.|It's out of  my hands.
{16797}{16852}You're gonna be all right, Da!
{16854}{16935}- I'll be with you as soon as I can.|- Take him back to his cell.
{17074}{17185}Leave him alone, you bastards!|Just let him be!
{17185}{17260}Stay strong, Gerry!|Stay strong, mate!
{17260}{17310}Keep your chin up, mate!
{17310}{17353}We're with you, Gerry!
{17355}{17410}We're with you all the way, mate!
{17737}{17777}He's still awake.
{18150}{18210}Your father passed away|an hour ago.
{18237}{18277}Thank you very much.
{18340}{18385}I'm sorry.
{19171}{19221}Giuseppe is dead, man!
{19245}{19297}They kill Giuseppe!
{19441}{19477}Gerry, man!
{19603}{19648}They killed Giuseppe!
{20863}{20935}Well, I think they ought to take|the word ''compassion''...
{20936}{20985}out of the English dictionary.
{21062}{21114}They fouled the ball, Gareth.
{21188}{21280}They fouled the fucking ball,|and they're as guilty as sin.
{21310}{21385}Believe me, if there's one thing|I know about, it's guilt.
{21419}{21511}Keep looking 'em in the eye,|and it's gonna reveal itself.
{21513}{21565}You have to keep up the pressure.
{21641}{21690}Do what you have to do.
{21742}{21810}Free the Four!|Free the Four!
{22288}{22355}Those were meant to be here|three weeks ago.
{22435}{22521}"The Parade of  Innocence"|What do you think?
{22523}{22585}Thousands of people|lined the streets of Dublin...
{22585}{22625}London and Liverpool today|in demonstrations...
{22627}{22678}demanding the release|of the Guild ford Four.
{22680}{22760}- Questions have been raised--|- Say good night to Daddy.
{22760}{22821}- Good night, Dad.|- Good night, son.
{22823}{22869}Free the Four!|Free the Four!
{23193}{23244}This was a mistake.
{23268}{23325}Why don't we let her|see the files?
{23327}{23385}What harm could it do?
{23385}{23450}- Mr. Dixon!|- Yes?
{23452}{23500}Don't you get tired of this?
{23502}{23571}This is a court order that I be allowed|to see the Giuseppe Conlon case files.
{23667}{23736}- Good morning.|- There are a few rules to be observed.
{23779}{23837}Here's a complete list of |the Conlon, Giuseppe case files...
{23839}{23887}the only files you'll need to see.
{23889}{23945}I'll go to the file drawer|and bring it out.
{23947}{24015}You take a page at a time,|read it and then return it.
{24017}{24087}Sorry. Is there a problem,|Mr Dixon?
{24089}{24136}Problem?|Not at all, Mrs. Peirce.
{24138}{24225}Our chief archivist, MrJenkins,|is here to assist you.
{24250}{24311}If you want to make a photocopy,|I alone will do it.
{24313}{24384}Use this pen at all times|for any notes.
{24386}{24475}If you deface any document, we can|trace it through the ink in this pen.
{24477}{24592}There are national security issues|involved here, Mrs. Peirce.
{24594}{24643}We wouldn't want|police intelligence files...
{24645}{24726}leaked to the IRA now, would we?
{24792}{24837}Conlon, Giuseppe file.
{24839}{24887}Yes, sir.
{25209}{25271}Leave my kit on the bus,|'cause I'm not staying in your jail.
{25273}{25365}So, our new VIP prisoner.
{25367}{25402}Welcome to Scotland.
{25404}{25459}I'm an innocent man.|My father was an innocent man.
{25460}{25564}He died in one of yourjails.|There's nothing you can do to hurt me.
{25566}{25614}They've moved me to a Scottish|jail to break my confidence.
{25616}{25701}- Put him in solitary.|- This is a peaceful protest.
{25703}{25742}We'll teach you manners.
{25744}{25788}Better men than you|have tried that already.
{25790}{25853}I have not seen my client|for two months.
{25859}{25946}He's been moved to Scotland, which has|impeded my investigation into his case.
{25948}{26040}So I'm filing this motion|to gain proper access--
{26042}{26084}Free the Four!
{26479}{26522}Here, thanks a lot.|Good day.
{26524}{26565}Thanks, thanks, thanks.
{27009}{27090}Sorry. Could you copy that|for me, please?
{27114}{27168}Thank you.
{27404}{27478}I just want my mother to be happy.
{27513}{27592}I'd like herto know...
{27594}{27686}that Giuseppe talked about her|everyday of his life.
{27748}{27797}He missed her terribly.
{27920}{27993}It's strange to be in a cell|without him.
{28059}{28124}I can't seem to get his face|out of my mind.
{28126}{28174}Everywhere I look, I see him.
{28359}{28462}Strange what time does|when you're in prison.
{28518}{28614}Like, you can bestaring|at the wall.
{28616}{28677}Drip, drip, drip.
{28735}{28770}It takes an eternity.
{28772}{28854}And then you blink,|and three years have gone by.
{28979}{29034}I mean, what l'm trying to--
{29034}{29110}I don't know what the fuck|l'm trying to say.
{29170}{29244}But I can't forget|what they did to my family.
{29288}{29326}I just can't forget.
{30552}{30586}Morning.
{30618}{30684}- Where's Jenkins?|- He telephoned in sick.
{30685}{30743}I'm afraid you're going|to have to come back tomorrow.
{30745}{30814}I can't. I'm in court tomorrow.|There's too much to get through.
{30816}{30853}I can't help you.
{30859}{30903}Please. I've got the court order.
{30909}{30965}You saw what Mr. Jenkins did.|He just brings me the files.
{30967}{31029}I take the notes and give them|back to you. Please.
{31034}{31099}- What name is it?|- Conlon.
{31101}{31177}- All right.|- Thanks. Thank you very much. Cheers.
{31325}{31368}What name was that again?
{31410}{31464}We've got two Conlon boxes.
{31466}{31560}Is it Giuseppe Conlon|or Gerard Conlon?
{31611}{31647}Gerard.
{31834}{31884}Well, that's for starters.
{32434}{32466}Fuck 'em!
{32535}{32571}It's good news.
{32600}{32684}We're talking about|a piece of evidence...
{32686}{32771}that says they knew all along,|that they let my father die in prison?
{32773}{32847}Would you mind telling me|what's good about that?
{32849}{32897}We'll get them in court.
{32899}{32970}''We'll get them in court''?|Will you catch yourself on?
{32972}{33049}They've kept us in prison for 1 5 years.|They can keep us in for another 1 5.
{33051}{33099}This is the fucking|government, Gareth!
{33101}{33177}It's the fucking government!|What are they gonna say?
{33179}{33234}''We're sorry about that''?
{33263}{33337}''Made a wee bit of a mistake,|but you can be on your way now''?
{33339}{33393}What are they gonna say?|''Sorry we killed your da''?
{33395}{33468}''Sorry we fucked|your fucking life to hell''?
{33904}{33978}I am not putting my mother|through hell again.
{34004}{34048}Are you afraid of court?
{34050}{34109}I just don't wanna be|humiliated again.
{34637}{34676}I swear by Almighty God...
{34678}{34763}that the evidence I shall give|shall be the truth, the whole truth...
{34765}{34814}and nothing but the truth.
{34989}{35098}Mr Dixon, do you know|these young people...
{35100}{35159}known as the Guildford Four?
{35225}{35260}Yes, I do.
{35262}{35340}Do you know how long|they have spent in jail?
{35366}{35401}Fifteen years.
{35470}{35514}Do you know Annie Maguire...
{35538}{35618}who served her 14 years|without remission?
{35644}{35715}Do you know her son Vincent|who served five years?
{35717}{35786}Her son Patrick who served four?|Do you know her husband...
{35788}{35862}Paddy Maguire, who served 12 years?
{35889}{35987}Carole Richardson was 17|when she went to jail, Mr Dixon.
{35989}{36035}Now she is 32.
{36083}{36133}Do you know Carole Richardson?
{36133}{36240}- What is the point of this?|- Yes, come to the point.
{36397}{36483}Do you know who this is, Mr Dixon?
{36578}{36623}No, I don't.
{36625}{36670}Well, then would you be so kind...
{36672}{36758}as to read this statement|that you took from him...
{36758}{36833}on the third of  November, 1974?
{36835}{36928}A statement, My Lord, which|vindicates all of these people...
{36933}{37003}-all these innocent people.|-I need to see a copy of this statement.
{37008}{37094}Either that man or his superior|or his superior's superior...
{37096}{37173}ordered that these people|be used as scapegoats...
{37175}{37253}by a nation|that was baying for blood...
{37258}{37327}in return for the innocent blood|spilled on the streets of  Guildford!
{37333}{37408}- You got your blood, Mr Dixon!|- She is making a political speech.
{37408}{37497}You got the blood of  Giuseppe Conlon|and the lifeblood of  Carole Richardson!
{37499}{37577}You got 15 years of  blood and sweat|and pain from my client...
{37583}{37650}whose only crime|was that he was Irish...
{37652}{37718}and foolish, and he was in|the wrong place at the wrong time!
{37720}{37796}Mrs. Peirce, I will have you|removed from the court.
{37948}{38013}And one of your colleagues,|My Lord...
{38015}{38092}who sat where you sit now said,|''It is a pity...
{38094}{38158}you were not charged|with treason to the Crown--
{38158}{38251}a charge that carries a penalty|of death by hanging--
{38253}{38334}a sentence I would have no difficulty|in passing in this case.''
{38336}{38409}Mrs. Peirce, I am trying|to read this document.
{38411}{38492}I will not tell you again|to be silent...
{38494}{38583}or you will be removed|from the court.
{39633}{39689}My Lord, this document...
{39691}{39770}brings the entire British|legal system into disrepute.
{39772}{39836}My Lord, this is new evidence.
{39838}{39889}It is shocking new evidence.
{39891}{39933}My Lord, this evidence...
{39933}{40013}was not submitted at the trial|that is under appeal.
{40015}{40108}That, I believe, is the point|Mrs Peirce is trying to make.
{40109}{40176}- Proceed, Mrs Peirce.|- My Lord, I demand a recess.
{40178}{40278}There will be no demands made|in my court.
{40283}{40316}Stand back.
{40433}{40497}This alibi for Gerry Conlon...
{40499}{40543}was taken by Mr Dixon...
{40545}{40626}one month after|Gerry Conlon was arrested.
{40628}{40733}This note was attached to it|when I found it in police files.
{40733}{40817}It reads, ''Not to be shown|to the defence.''
{40966}{41043}I have one question|to ask you, Mr Dixon:
{41045}{41099}Why was the alibi for Gerry Conlon...
{41101}{41208}who was charged with the murder|of five innocent people...
{41210}{41261}kept from the defence?
{41458}{41508}- Give us an answer!|- Answer the question!
{41527}{41575}Silence!
{41577}{41653}- It's about time!|- Order in the court!
{41688}{41758}My Lord, I would like|to approach the bench.
{41759}{41799}This is most irregular.
{41801}{41868}Yes. I am aware of that, My Lord.
{41890}{41937}Very well.
{42323}{42387}Mr. Dixon, you may stand down.
{42426}{42509}This is the man|who should be under arrest!
{42511}{42566}Be silent, Mrs Peirce!
{42568}{42626}- This court is now in recess.|- Guards, get ready.
{42628}{42688}Don't try and make me|the fall guy...
{42690}{42764}for the whole|British legal establishment.
{42866}{42918}If I accept this mercy deal--
{42920}{42968}They have a flipping cheek,|of ering me mercy.
{42970}{43018}They should be begging|for mercy themselves.
{43020}{43062}- You ready, Mr Conlon?|- Aye.
{43064}{43116}I know the difference|between right and wrong.
{43118}{43183}The truth has to come out.|They may not want to hear it...
{43183}{43233}but there's people outside|who'll listen.
{43233}{43297}-Just think about it, will you?|- All right.
{43299}{43353}Aye. Give us three of them.
{43746}{43786}Silence!
{43852}{43918}In the matter of  HerMajesty...
{43920}{43990}versus GerardPatrick Conlon...
{43992}{44065}the case is hereby dismissed.
{44183}{44238}My husband died in your prison|an innocent man!
{44240}{44323}I'm going out the front door.|I'll see you outside.
{44325}{44376}- This way.|- I'm going out the front door.
{44378}{44410}What about Giuseppe Conlon?
{44412}{44483}Your Honour,|he was an innocent man!
{44550}{44594}Mr. Conlon, that's not a good idea.
{44596}{44640}Use the back,|for security reasons.
{44642}{44694}I'm a free man,|and I'm going out the front door.
{44722}{44810}In the matter of |Her Majesty versus Paul Hill...
{44812}{44903}the case is hereby dismissed.
{45073}{45178}Leave me alone!|I'm going out the front door with Gerry!
{45236}{45361}Iln the matterof |Her Majesty versus Patrick Armstrong...
{45363}{45413}the case is hereby dismissed.
{45467}{45583}In the matterof |Her Majesty versus Carole Richardson...
{45584}{45671}the case is hereby dismissed.
{45673}{45711}I'm an innocent man!
{45713}{45809}I spent 15 years in prison|for something I didn't do!
{45811}{45859}I watched my father die...
{45861}{45933}in a British prison|for something he didn't do!
{45934}{46008}And this government|still says he's guilty!
{46010}{46052}I want to tell them...
{46058}{46133}that until my father|is proved innocent...
{46134}{46236}until all the people involved|in this case are proved innocent...
{46238}{46301}until the guilty ones|are brought to justice...
{46303}{46358}I will fight on...
{46357}{46448}in the name of  my father|and of the truth!
