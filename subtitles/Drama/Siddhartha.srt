1
00:03:57,300 --> 00:04:01,500
Well, you had your bath.
Aren't you going to the temple?

2
00:04:02,100 --> 00:04:03,500
I am.

3
00:04:29,921 --> 00:04:33,121
It's so peaceful here.

4
00:04:33,122 --> 00:04:35,233
Hmm.

5
00:04:35,234 --> 00:04:38,359
It's all right if you are my father's age.

6
00:04:38,360 --> 00:04:39,744
But...

7
00:04:39,745 --> 00:04:42,945
But if you are young.

8
00:04:45,612 --> 00:04:48,812
I want to be a sadhu.

9
00:04:49,166 --> 00:04:52,107
I want to be free.

10
00:04:52,108 --> 00:04:55,316
I want to be wild.

11
00:04:55,848 --> 00:04:58,800
I...I do want to be a sadhu.

12
00:04:59,000 --> 00:05:02,000
But how are you going to tell your father?

13
00:05:02,001 --> 00:05:03,500
I don't know.

14
00:05:04,756 --> 00:05:06,899
But I'll speak to him tonight.

15
00:05:06,900 --> 00:05:08,500
He'll be very angry.

16
00:05:09,400 --> 00:05:11,705
I don't think so.
No?

17
00:05:12,002 --> 00:05:14,371
I don't think he'll be angry.

18
00:05:14,372 --> 00:05:21,768
I shall ask him to give me permission
to do what I want to do.

19
00:05:21,824 --> 00:05:26,350
You know there's something
very tranquil about your father.

20
00:05:26,360 --> 00:05:30,900
The way he moves down everyday
to the Ganges for a bath.

21
00:05:31,600 --> 00:05:34,350
There's something nice about it.

22
00:05:34,619 --> 00:05:38,444
It's not nice. It's the same.

23
00:05:38,713 --> 00:05:41,913
Everything's the same, Govinda.

24
00:05:43,529 --> 00:05:47,452
The river, the temples, the mountains,

25
00:05:47,453 --> 00:05:51,858
the people, the rocks, the sand, the steps.

26
00:05:51,859 --> 00:05:55,059
Everything's the same.

27
00:07:25,200 --> 00:07:28,909
My son. Oh!, my son.

28
00:07:37,000 --> 00:07:41,482
Will you go and stand day and night,
waiting for my permission?

29
00:07:41,483 --> 00:07:42,767
I will stand and wait.

30
00:07:42,768 --> 00:07:45,644
You will grow tired, Siddhartha.
Will I grow tired?

31
00:07:45,645 --> 00:07:47,907
You will fall asleep.
Will I fall asleep?

32
00:07:47,908 --> 00:07:50,328
Do you want to die?
Do I want to die?

33
00:07:50,329 --> 00:07:54,529
Would you die rather than obey your father?

34
00:08:04,917 --> 00:08:08,000
I shall never disobey my father.

35
00:08:08,001 --> 00:08:10,380
Please give me your blessing.

36
00:08:10,401 --> 00:08:14,200
Will you leave this idea to become a sadhu?

37
00:08:15,000 --> 00:08:20,000
Siddhartha shall do what his father says.

38
00:08:20,100 --> 00:08:25,600
All right, I cannot stop you.
You are a man.

39
00:08:26,000 --> 00:08:29,600
Go into the forest.
If you find truth,

40
00:08:29,601 --> 00:08:32,400
come back, and tell me about it.

41
00:08:32,600 --> 00:08:34,745
And if you find nothing,

42
00:08:34,746 --> 00:08:37,946
Come back anyway, my son.

43
00:08:38,113 --> 00:08:42,350
Like the river, everything returns.

44
00:09:31,000 --> 00:09:34,000
You've come?
I have come.

45
00:11:46,000 --> 00:11:48,250
What do you think, Govinda?

46
00:11:48,350 --> 00:11:50,250
Are we any further?

47
00:11:50,300 --> 00:11:53,064
Have we reached our goal?

48
00:11:53,065 --> 00:11:56,671
We learn so much
and we're still learning.

49
00:11:56,672 --> 00:12:01,261
You, Siddhartha, will become a great sadhu
because you've learned everything so quickly.

50
00:12:01,262 --> 00:12:04,950
The old sadhus were saying
that one day you'll be a holy man.

51
00:13:16,582 --> 00:13:20,806
What is meditation?
What if I can leave my body?

52
00:13:21,238 --> 00:13:24,735
What is holding of the breath?
What is fasting?

53
00:13:24,768 --> 00:13:29,300
It's an escape, Govinda.
It's an escape from the torments of life.

54
00:13:29,350 --> 00:13:32,800
We, sadhus are escapists.

55
00:13:32,850 --> 00:13:36,343
The man in the village has the same trip.
when he takes his drug,

56
00:13:36,344 --> 00:13:39,435
which is a few bowl of wine.
And then he falls asleep.

57
00:13:39,436 --> 00:13:42,620
And he no longer feels himself
or the pains of life.

58
00:13:42,621 --> 00:13:46,810
What's so difference from what he's doing
and what we are doing?

59
00:13:46,820 --> 00:13:50,950
I don't know why you speak like this, Siddhartha.
Because you're not a farmer

60
00:13:50,951 --> 00:13:55,729
And you're not a drunkard, and you know very
well that a drinker doesn't escape from anything.

61
00:13:55,730 --> 00:13:59,092
He only gets a short rest.
And when he returns,

62
00:13:59,093 --> 00:14:01,431
he finds everything the same as before.

63
00:14:01,432 --> 00:14:05,448
He's not any wiser,
He has not gained any knowledge.

64
00:14:05,449 --> 00:14:07,409
Neither has he climbed any higher.

65
00:14:09,156 --> 00:14:11,658
Well I don't know, Govinda...

66
00:14:11,659 --> 00:14:13,649
I'm not a drunkard.

67
00:14:13,650 --> 00:14:19,100
But I'm...I'm tired of all this,
these exercises in the meditation.

68
00:14:19,150 --> 00:14:24,600
I'm still as removed from wisdom and
salvation as I was im my father's house.

69
00:14:28,903 --> 00:14:31,464
How old do you think is our oldest sadhu,

70
00:14:31,465 --> 00:14:33,719
our worthy teacher there?

71
00:14:33,720 --> 00:14:37,250
I don't know.
Maybe he's sixty?

72
00:14:37,260 --> 00:14:40,721
Sixty,
and he has not attained Nirvana.

73
00:14:41,092 --> 00:14:44,212
He will be seventy, he will be eighty.

74
00:14:44,255 --> 00:14:47,680
And try he might, he can exercise,
meditate, fast,

75
00:14:47,681 --> 00:14:50,946
But he will never attain Nirvana.

76
00:14:51,311 --> 00:14:56,394
He will never get eternal peace of mind.
It's all tricks Govinda.

77
00:14:56,395 --> 00:15:00,086
We are deceiving ourselves.
It's all tricks.

78
00:15:55,600 --> 00:15:59,862
The essential thing, the way,
we do not know.

79
00:15:59,863 --> 00:16:04,060
Oh, Govinda, I'm thirsty,
I'm thirsty, Govinda.

80
00:16:04,062 --> 00:16:06,700
For years we've been on this path
with the sadhus.

81
00:16:06,710 --> 00:16:11,400
For years we've been in search of knowledge.
For years we've been asking questions.

82
00:16:11,855 --> 00:16:15,000
And there's been no.
No answer from God.

83
00:16:16,407 --> 00:16:19,000
Now you want to go
and hear the Buddha?

84
00:16:19,002 --> 00:16:20,680
Why? don't you want to go?

85
00:16:20,681 --> 00:16:22,140
Remember what I told you?

86
00:16:22,275 --> 00:16:24,073
I no longer believe in...

87
00:16:24,074 --> 00:16:27,274
learning or in teaching.

88
00:16:29,056 --> 00:16:31,456
But I will go with you.

89
00:17:15,527 --> 00:17:20,127
What I have to teach
is not a relegion at all.

90
00:17:20,128 --> 00:17:22,327
For I do not talk about substance

91
00:17:22,328 --> 00:17:25,110
or soul, or even God.

92
00:17:25,111 --> 00:17:28,800
Nor do I ask any of my followers to have faith

93
00:17:28,805 --> 00:17:32,227
Seek grace, or worship me.

94
00:17:33,029 --> 00:17:37,829
This life is full of pain,
and this world is full of suffering,

95
00:17:37,853 --> 00:17:45,200
The cause of suffering is craving,
self love and attachment.

96
00:17:45,245 --> 00:17:52,395
The remedy for suffering
is the systematic destruction of craving,

97
00:17:52,628 --> 00:17:59,100
Nirvana, we must think of
as an enlightenment of the conscious mind.

98
00:17:59,107 --> 00:18:03,000
Control of yourself.
And then, concentration

99
00:18:03,001 --> 00:18:09,805
and meditation.
These should lead to wisdom.

100
00:18:37,500 --> 00:18:39,000
Illustrious one.

101
00:18:39,001 --> 00:18:43,438
I wish to be accepted in your order.

102
00:18:59,910 --> 00:19:03,646
Siddhartha! I've been accepted.

103
00:19:04,033 --> 00:19:07,233
What are you waiting for?
Are you not goin' to join?

104
00:19:07,564 --> 00:19:10,120
You've taken a step, Govinda.

105
00:19:10,130 --> 00:19:13,300
You've chosen your path.

106
00:19:13,350 --> 00:19:17,750
Govinda, you've always
followed one step behind me.

107
00:19:17,850 --> 00:19:23,270
And I've wondered
if Govinda will ever take a step on his own.

108
00:19:23,275 --> 00:19:25,800
Well now you have.

109
00:19:25,810 --> 00:19:28,434
Go along it my friend.

110
00:19:28,435 --> 00:19:30,391
Go along it to the end.

111
00:19:30,392 --> 00:19:34,667
But what's wrong with this teaching? Why?
Why aren't you goin' to join the Buddha?

112
00:19:34,668 --> 00:19:37,700
There's....nothing wrong
with the Buddha's teachings.

113
00:19:37,710 --> 00:19:43,056
It's just that
I must go my own way.

114
00:19:44,391 --> 00:19:46,885
Tomorrow, Govinda...

115
00:19:46,886 --> 00:19:50,086
I must leave you.

116
00:19:50,180 --> 00:19:53,380
Siddhartha!

117
00:19:55,289 --> 00:19:57,765
Govinda!

118
00:19:57,766 --> 00:20:00,966
Govinda!

119
00:20:05,089 --> 00:20:08,065
Be at peace, Govinda.

120
00:20:08,066 --> 00:20:11,266
Be at peace.

121
00:20:19,600 --> 00:20:22,580
Forgive me Illustrious One.

122
00:20:22,581 --> 00:20:24,331
May I speak?

123
00:20:24,332 --> 00:20:25,382
As you wish.

124
00:20:26,718 --> 00:20:31,265
Yesterday, my friend and I had
the honor of hearing you speak.

125
00:20:32,921 --> 00:20:34,076
Now..

126
00:20:34,149 --> 00:20:40,813
My friend has sworn allegiance to you,
and left me.

127
00:20:40,814 --> 00:20:43,088
Now I'm alone.

128
00:20:43,433 --> 00:20:45,255
Unable to follow you,

129
00:20:45,256 --> 00:20:47,554
or any teacher.

130
00:20:49,061 --> 00:20:55,042
You said yesterday that,
that life was a chain unbroken.

131
00:20:55,043 --> 00:20:58,252
Linked together by cause and effect.

132
00:20:58,767 --> 00:21:02,634
For me,
cause and effect are not enough.

133
00:21:03,099 --> 00:21:07,999
It is in my opinion that
everything is whole, unified, complete.

134
00:21:08,000 --> 00:21:12,152
Opinions may be beautiful, ugly, or clever.

135
00:21:12,153 --> 00:21:17,903
It is not my goal to explain this world
to those who are thirsty for knowledge.

136
00:21:17,918 --> 00:21:24,157
It is salvation from suffering.
This is all the Buddha teaches, nothing more.

137
00:21:24,158 --> 00:21:28,135
You can never tell me the secret of
what you yourself expierienced,

138
00:21:28,136 --> 00:21:30,496
when you achieved enlightenment.

139
00:21:30,497 --> 00:21:36,141
That is why I'm goin' on my way.
Not to find a better teacher.

140
00:21:36,152 --> 00:21:39,793
but, but to leave behind all teachers,

141
00:21:39,794 --> 00:21:43,079
And to find the way alone or die.

142
00:21:43,080 --> 00:21:46,679
You are clever, young sadhu.
Perhaps too clever.

143
00:21:46,680 --> 00:21:49,931
Be on your guard against cleverness.

144
00:21:56,764 --> 00:21:59,964
But I will never forget this day.

145
00:22:02,160 --> 00:22:04,443
The Buddha has taken away my friend.

146
00:22:04,444 --> 00:22:09,392
My friend Govinda, who believed in me,
and now beleives in Him.

147
00:22:09,393 --> 00:22:14,206
He's taken away my friend who was my shadow,
and is now His shadow.

148
00:22:14,554 --> 00:22:18,625
But He has given me Siddhartha,
He has given me myself.

149
00:22:18,635 --> 00:22:22,683
That's the only man
who I will lower my eyes for.

150
00:22:22,716 --> 00:22:26,296
No other teaching will attract me
as this man's teaching has.

151
00:22:27,218 --> 00:22:31,541
I wanted to know the book of the world
and the book of my own nature.

152
00:22:31,969 --> 00:22:34,303
I missed the sign.

153
00:22:34,304 --> 00:22:37,301
I thought the world of
appearances was the truth.

154
00:22:37,434 --> 00:22:38,612
But I was wrong.

155
00:22:39,233 --> 00:22:43,602
It's my own eyes and my own mouth
that I could not understand.

156
00:22:43,603 --> 00:22:45,553
I could not understand myself.

157
00:22:45,554 --> 00:22:47,100
Anyway that's all over.

158
00:22:47,523 --> 00:22:50,983
I've awakened. I'm awake.

159
00:22:50,984 --> 00:22:53,961
I've been born today.

160
00:24:35,961 --> 00:24:39,339
You were very tired.
Do you feel better now?

161
00:24:40,648 --> 00:24:42,599
Drink this.

162
00:24:43,441 --> 00:24:44,641
Thank you.

163
00:24:48,678 --> 00:24:53,231
Come. It's time to cross the river.

164
00:25:00,011 --> 00:25:04,721
It is a beautiful river.
Yes, oh yes. It is a beautiful river.

165
00:25:04,722 --> 00:25:08,855
I love it. I love everything about it, everything.

166
00:25:08,856 --> 00:25:11,595
I often listen to it, and gaze into it.

167
00:25:11,596 --> 00:25:13,843
And always I learn something from it.

168
00:25:14,607 --> 00:25:17,608
There is much to be learned from the river.

169
00:25:37,546 --> 00:25:41,041
I'm without a home.
I cannot pay you.

170
00:25:41,042 --> 00:25:45,538
Well I can see that.
But I didn't expect anything from you.

171
00:25:45,539 --> 00:25:49,164
You can pay me some other time.
We will meet again.

172
00:25:49,165 --> 00:25:52,745
Everything returns.
Do you think so?

173
00:25:52,750 --> 00:25:53,859
Yes, I think so.

174
00:25:53,860 --> 00:25:58,049
I have learned from the river
that everything returns.

175
00:26:11,298 --> 00:26:13,097
May I have a drink of water?

176
00:27:18,536 --> 00:27:19,742
Who is she?

177
00:27:19,743 --> 00:27:21,511
Kamala a cortes�.

178
00:28:21,636 --> 00:28:24,785
Did you not stand outside yesterday
and greet me?

179
00:28:24,924 --> 00:28:26,124
Yes.

180
00:28:27,129 --> 00:28:28,731
I thought you saw me.

181
00:28:30,074 --> 00:28:35,334
Three years ago I was Siddhartha,
the Brahmin's son who left his home

182
00:28:35,335 --> 00:28:37,433
to become a sadhu.

183
00:28:37,434 --> 00:28:39,013
However that was yesterday.

184
00:28:39,014 --> 00:28:42,469
Today I have entered a new path
that leads to your garden.

185
00:28:42,470 --> 00:28:44,989
I should never lower my eyes before yours.

186
00:28:44,990 --> 00:28:47,470
Not even your fan can hide the beauty.

187
00:28:47,471 --> 00:28:50,410
Is that all you've come
to tell me, young Brahamin?

188
00:28:50,414 --> 00:28:52,003
I've come to say...

189
00:28:52,004 --> 00:28:55,101
that you are all the things that will outlive me.

190
00:28:55,150 --> 00:29:01,300
That you, Kamala, will be all the beauty
that will be in the shadow we leave.

191
00:29:01,727 --> 00:29:07,000
You will be my first love, my only love.

192
00:29:15,000 --> 00:29:20,890
Well, this is the first time that a sadhu
has come from the woods with a desire to love me.

193
00:29:20,891 --> 00:29:24,600
I have never seen a sadhu talk like this before.

194
00:29:24,601 --> 00:29:28,233
I learned a lot today
when I got rid of my beard.

195
00:29:28,234 --> 00:29:31,985
Look! I have oiled and combed my hair.

196
00:29:31,986 --> 00:29:34,280
I'm already beginning to learn from you.

197
00:29:34,343 --> 00:29:38,997
Many young men come to me
and they all have fine clothes,

198
00:29:38,998 --> 00:29:43,637
fine shoes, scent in their hair
and money in their pockets.

199
00:29:43,722 --> 00:29:45,922
That is how young men come to me.

200
00:29:45,923 --> 00:29:49,811
You can't just be some...
beggar from the woods.

201
00:29:49,875 --> 00:29:52,844
Don't you understand?
But...

202
00:29:53,085 --> 00:29:54,405
How can I be rich?

203
00:29:54,406 --> 00:29:55,985
How can I be worldly?

204
00:29:55,986 --> 00:29:59,996
Oh!...Many people want to know about that.

205
00:29:59,997 --> 00:30:03,010
Tell me what can you do?

206
00:30:03,011 --> 00:30:07,878
I can think.
I can wait. I can fast.

207
00:30:08,946 --> 00:30:11,341
Nothing else?

208
00:30:11,342 --> 00:30:13,441
I can recite a poem for a kiss.

209
00:30:13,442 --> 00:30:17,291
Will you give me a kiss for a poem?

210
00:30:19,549 --> 00:30:21,502
Um..hmm...

211
00:30:35,499 --> 00:30:38,015
What about the poem?

212
00:30:39,016 --> 00:30:42,450
Tell me. Can't you do anything else

213
00:30:42,451 --> 00:30:46,500
other than think, fast, and compose poetry?

214
00:30:46,555 --> 00:30:49,000
I can do spells.

215
00:30:51,000 --> 00:30:54,892
I know how to do incantations.
I read the scriptures.

216
00:30:54,893 --> 00:30:56,442
You can read and write?

217
00:30:57,100 --> 00:31:00,150
Well!
I think I can find a job for you.

218
00:31:05,080 --> 00:31:08,490
There is another visitor for you.
It's Kamaswami.

219
00:31:08,500 --> 00:31:11,600
I'm sorry you have to go.
There is another visit for me.

220
00:31:11,605 --> 00:31:14,580
But, perhaps I will
see you tomorrow.

221
00:31:36,533 --> 00:31:38,303
I've found a job for you.

222
00:31:38,304 --> 00:31:39,620
Oh yes?

223
00:31:39,700 --> 00:31:44,000
It's with a rich merchant, Kamaswami.

224
00:31:44,200 --> 00:31:46,339
You are lucky, Siddhartha.

225
00:31:46,350 --> 00:31:49,000
One door after another
is being opened to you.

226
00:31:49,500 --> 00:31:52,100
Tell me.
How does it happen?

227
00:31:52,101 --> 00:31:54,400
You have a special charm?

228
00:31:54,401 --> 00:31:59,900
I told you yesterday
that I can think, wait and fast.

229
00:31:59,901 --> 00:32:02,101
But you didn't consider that important.

230
00:32:03,600 --> 00:32:05,160
You will see, Kamala,

231
00:32:05,161 --> 00:32:08,900
that this sadhu from the forest
can do things that other man can't.

232
00:32:09,050 --> 00:32:12,500
I've come to you...
to learn about love.

233
00:32:13,690 --> 00:32:20,700
From the first glance I knew
that you would be my teacher, my guru.

234
00:32:23,200 --> 00:32:27,300
But...what if I said no?

235
00:32:27,310 --> 00:32:29,250
But you haven't, Kamala.

236
00:32:29,260 --> 00:32:32,000
Because you've recognized
that I am like a stone,

237
00:32:32,001 --> 00:32:33,850
Thrown into the water.

238
00:32:33,851 --> 00:32:38,340
A stone that finds its way
quickly into the depths.

239
00:32:38,345 --> 00:32:42,200
In the same way, when I have a goal,
I do nothing.

240
00:32:42,250 --> 00:32:47,330
I wait, I think, I fast.

241
00:33:01,931 --> 00:33:05,195
I have been told that you are a Brahmin.

242
00:33:05,196 --> 00:33:07,600
A man with some knowledge of writing.

243
00:33:07,601 --> 00:33:10,700
That you seek service with a merchant.

244
00:33:10,710 --> 00:33:13,660
Are you in need, Brahmin, that you seek service?

245
00:33:13,661 --> 00:33:15,520
No! I'm not in need.

246
00:33:15,521 --> 00:33:17,930
I have never been in need.

247
00:33:17,931 --> 00:33:20,300
I have lived for many years in the forest.

248
00:33:20,301 --> 00:33:22,050
I've been with the sadhus.

249
00:33:22,055 --> 00:33:25,400
How is that..that you are not in need?

250
00:33:25,401 --> 00:33:28,400
Are not all Sahus
completely without possessions?

251
00:33:28,401 --> 00:33:30,790
I have no possessions if that's what you mean.

252
00:33:30,791 --> 00:33:36,000
I certainly am without possessions of my own free will. 
So I'm not in need.

253
00:33:36,432 --> 00:33:38,890
How do you live without possessions?

254
00:33:38,900 --> 00:33:40,850
I have never thought of that.

255
00:33:40,855 --> 00:33:43,700
I lived for years in the forest without possessions.

256
00:33:43,710 --> 00:33:45,850
I've never thought on what I should live.

257
00:33:45,855 --> 00:33:50,400
Then you have lived on the possessions of others.
You're a parasite then. Eh?

258
00:33:50,450 --> 00:33:54,000
A merchant also lives on
the possessions of others, doesn't he?

259
00:33:54,001 --> 00:33:56,550
A merchant doesn't take from others for nothing.

260
00:33:56,810 --> 00:33:58,460
He gives goods in return.

261
00:33:59,428 --> 00:34:02,750
That is the way of life,
everyone takes and everyone gives.

262
00:34:02,751 --> 00:34:05,200
Very well. If you're without possessions,

263
00:34:05,201 --> 00:34:07,200
how can you give?
and what can you give?

264
00:34:07,201 --> 00:34:09,800
Everyone gives what he can.
The soldier gives strength.

265
00:34:09,801 --> 00:34:12,765
The merchant goods, the fisherman fish,
The framer rice.

266
00:34:12,766 --> 00:34:16,880
Alright! You're clever. But what've
you learned that you can give?

267
00:34:16,881 --> 00:34:20,900
I can think.
I can wait. I can fast.

268
00:34:20,940 --> 00:34:23,000
So what? Of what use is that?

269
00:34:23,001 --> 00:34:27,300
It is of great value. If a man can live on nothing,
he doesn't have to work.

270
00:34:27,301 --> 00:34:32,350
He can wait calmly for another job.
Perhaps, perhaps, Brahman.

271
00:34:40,800 --> 00:34:42,800
Can you read this?

272
00:34:45,200 --> 00:34:48,200
Twenty bales of cotton, fifty bales of silk,

273
00:34:48,210 --> 00:34:52,000
payment in three weeks.
Excellent.

274
00:34:52,350 --> 00:34:54,560
Can you write something for me?

275
00:35:10,900 --> 00:35:14,255
Writing is good. Thinking is better.

276
00:35:14,260 --> 00:35:18,100
Cleverness is good. Patience is better.

277
00:35:18,200 --> 00:35:21,400
Excellent. Excellent.

278
00:36:36,001 --> 00:36:38,400
Have you ever made love to a woman?

279
00:36:38,600 --> 00:36:40,000
No.

280
00:36:41,750 --> 00:36:44,830
Have you ever... kissed a woman?

281
00:36:44,850 --> 00:36:46,300
No.

282
00:36:51,500 --> 00:36:54,800
Have you ever... known a woman?

283
00:36:55,800 --> 00:36:57,300
No.

284
00:36:59,200 --> 00:37:01,000
Do you want to?

285
00:37:01,900 --> 00:37:03,500
Very much.

286
00:37:05,107 --> 00:37:06,830
Kiss me.

287
00:37:17,500 --> 00:37:19,400
That's all?

288
00:37:20,600 --> 00:37:22,120
I'll teach you.

289
00:39:31,767 --> 00:39:34,967
You must never leave me, Siddhartha.

290
00:39:37,033 --> 00:39:40,233
Never.

291
00:41:20,292 --> 00:41:24,362
Put it over there! If this keeps up,
I'll give you a third of the business.

292
00:42:55,808 --> 00:43:00,079
Did you have a good crop? Thank you very much.
We had a good crop this time.

293
00:43:00,080 --> 00:43:03,995
But I'm sorry, you're just too late.

294
00:43:03,996 --> 00:43:08,198
Never mind. I'll stay and cerebrate with you
since you've sold the harvest.

295
00:43:08,462 --> 00:43:11,843
And maybe next year, I shall come
in time to buy your crop.

296
00:43:11,844 --> 00:43:15,044
Thank you. Thank you, sir.

297
00:43:35,579 --> 00:43:37,779
So, you've come back.

298
00:43:37,780 --> 00:43:40,123
How much rice have you brought?

299
00:43:40,124 --> 00:43:42,311
Nothing.

300
00:43:42,312 --> 00:43:43,554
What do you mean?

301
00:43:43,555 --> 00:43:46,755
You came back with nothing?

302
00:43:48,184 --> 00:43:54,305
I don't understand you. Where is your mind?
You're a child. A child!

303
00:43:54,306 --> 00:43:58,672
Let me ask you one thing my friend,
were you traveling just for pleasure?

304
00:43:58,673 --> 00:44:03,515
Certainly, I always travel for pleasure.
I have friends, friends everywhere.

305
00:44:03,518 --> 00:44:07,394
You see, I spent a lot of time with people.
I learned a lot.

306
00:44:07,556 --> 00:44:12,254
I did not hurt them. They did not hurt me.

307
00:44:12,255 --> 00:44:16,597
I can return. And if I'll go back
they'll be glad to see me.

308
00:44:16,623 --> 00:44:21,000
I did not display any bad feelings.
So don't be angry my friend.

309
00:44:21,001 --> 00:44:25,345
If you think that I, Siddhartha, will do you
any harm, then say good-bye and I'll leave.

310
00:44:25,346 --> 00:44:30,160
Come on my friend. The first sign of 
a good business is friendship.

311
00:44:34,000 --> 00:44:38,380
I feed you. I do everything for you.

312
00:44:39,000 --> 00:44:41,856
I don't understand your attitude.

313
00:44:43,957 --> 00:44:47,292
I mean, you could be more thankful.

314
00:44:47,909 --> 00:44:51,694
You know I've taught you everything.
Please my friend.

315
00:44:51,695 --> 00:44:53,936
Don't make jokes like that.

316
00:44:53,937 --> 00:44:57,602
I have learned from you
how much a basket of fish costs.

317
00:44:57,603 --> 00:44:59,951
How much one can claim for lending monies.

318
00:44:59,952 --> 00:45:03,147
That is the extent of your knowledge Kamaswami.

319
00:45:03,148 --> 00:45:06,348
But I did not learn how to think from you.

320
00:45:06,367 --> 00:45:09,567
It is better if you learn that from me.

321
00:45:53,880 --> 00:45:58,439
You are like me, you know.
You are different from other people.

322
00:45:59,135 --> 00:46:01,363
You are Kamala and no one else.

323
00:46:01,364 --> 00:46:07,885
And within you there is a stillness and a santuary
to which you can retreat at any time and be yourself.

324
00:46:07,886 --> 00:46:09,500
Just as I can.

325
00:46:11,000 --> 00:46:14,480
Do you realize how...how few people can do that?

326
00:46:14,481 --> 00:46:19,362
Not everyone is clever, Siddhartha.

327
00:46:19,363 --> 00:46:22,171
It has nothing to do with that.

328
00:46:22,172 --> 00:46:24,696
Kamaswami is clever.

329
00:46:24,697 --> 00:46:28,741
Yet he has no place, no santuary.

330
00:48:10,935 --> 00:48:14,135
You're the best lover I've ever had.

331
00:48:15,602 --> 00:48:18,346
You're more strong,

332
00:48:18,347 --> 00:48:21,011
more supple,

333
00:48:21,012 --> 00:48:24,212
more willing than all the others.

334
00:49:15,842 --> 00:49:19,042
You don't...really love me.

335
00:49:19,574 --> 00:49:23,497
You love no one. Isn't that the truth?

336
00:49:24,488 --> 00:49:26,708
Maybe I am like you.

337
00:49:26,709 --> 00:49:29,331
You cannot love either.

338
00:49:29,332 --> 00:49:32,663
Otherwise how could you practice love as an art?

339
00:49:33,775 --> 00:49:37,180
No, Kamala, we cannot love.

340
00:49:37,181 --> 00:49:41,551
Ordinary people can. That's their secret.

341
00:50:04,010 --> 00:50:07,210
What a strange life you lead.

342
00:50:07,242 --> 00:50:12,312
You who were once a seeker,
a man in search of his true self.

343
00:50:12,344 --> 00:50:15,544
See how your life is passing you by.

344
00:51:30,662 --> 00:51:33,862
Ah, it's all mine!

345
00:51:56,669 --> 00:51:59,869
No water?

346
00:51:59,986 --> 00:52:02,951
No water!

347
00:52:02,952 --> 00:52:06,152
Go get some!

348
00:52:25,627 --> 00:52:28,827
Tell me more about the Buddha.

349
00:52:29,898 --> 00:52:33,098
How did he smile?

350
00:52:37,739 --> 00:52:41,566
One day I will become his follower.

351
00:52:43,543 --> 00:52:48,005
I will give up my pleasure garden,

352
00:52:48,006 --> 00:53:00,800
and take refuge in his teachings.

353
00:53:12,445 --> 00:53:16,254
What's happened to me?
What have I become?

354
00:53:16,651 --> 00:53:19,851
Another Kamaswami.

355
00:53:20,601 --> 00:53:23,266
What have I, what have I given up my freedom for?

356
00:53:23,267 --> 00:53:27,155
For this? For all the food on my table?

357
00:53:28,918 --> 00:53:33,184
I feel sick.
Sick of this world that I've created.

358
00:53:34,830 --> 00:53:38,030
I must leave this cage!

359
00:53:39,188 --> 00:53:43,722
My house is useless...
and my life in the town is finished.

360
00:56:18,819 --> 00:56:21,824
I do not think you would like this work, sir.

361
00:56:21,825 --> 00:56:24,616
It is not for somebody dressed like you.

362
00:56:24,617 --> 00:56:27,495
Why judge me by my clothes?

363
00:56:27,496 --> 00:56:29,119
I have no money.

364
00:56:29,120 --> 00:56:32,477
I have only these clothes
to pay you for taking me across.

365
00:56:32,490 --> 00:56:36,385
Are you joking with me, sir?
No, my friend, I'm not joking.

366
00:56:36,746 --> 00:56:42,015
A long time ago you took me
across the river for no payment.

367
00:56:42,016 --> 00:56:45,452
So please do it today
and take my clothes instead.

368
00:56:45,453 --> 00:56:48,823
And where would you go
without your clothes? Where?

369
00:56:50,037 --> 00:56:53,237
I would prefer not to go any further.

370
00:56:53,668 --> 00:57:00,073
I would prefer it. If you would give me some
old clothes and keep me here as your apprentice.

371
00:57:00,169 --> 00:57:02,473
I recognize you.

372
00:57:02,474 --> 00:57:05,098
You once slept in my hut.

373
00:57:05,099 --> 00:57:09,495
That was a long time ago.
I took you across the river.

374
00:57:09,934 --> 00:57:13,134
I remember you face, but not your name.

375
00:57:13,868 --> 00:57:18,355
My name is Siddhartha.
I was a sadhu when you last saw me.

376
00:57:19,159 --> 00:57:23,859
Well, you're welcome, Siddhartha.
My name is Vasudeva.

377
00:57:23,860 --> 00:57:27,060
My home is your home.

378
00:57:28,159 --> 00:57:31,359
Thank you.

379
00:57:35,387 --> 00:57:39,033
You know, Siddhartha?
I had a wife once.

380
00:57:39,065 --> 00:57:43,629
She lives in this hut. She slept on that bed.

381
00:57:54,796 --> 00:58:00,013
Siddhartha, this river is everywhere,
everywhere at the same time.

382
00:58:00,044 --> 00:58:03,267
It is at its source and at the mouth.

383
00:58:03,299 --> 00:58:07,139
It's in the waterfall and at the ferry.

384
00:58:08,819 --> 00:58:13,206
It's at the ebb, in the ocean,
 in the mountains.

385
00:58:13,207 --> 00:58:17,560
It is everywhere.
The river only lives in the present.

386
00:58:17,854 --> 00:58:20,661
We too must learn to live in the present.

387
00:58:20,662 --> 00:58:23,956
Our great folly is to feel obliged to conquer time.

388
00:58:23,986 --> 00:58:28,056
It is this endless running after money, riches, 
power, wealth.

389
00:58:28,088 --> 00:58:34,171
This feeling that there isn't enough time.
That obscures the truth.

390
00:59:37,104 --> 00:59:39,322
Siddhartha...

391
00:59:39,323 --> 00:59:41,966
They are all going to see the Lord Buddha.

392
00:59:41,967 --> 00:59:44,434
He will not live long. 

393
00:59:44,500 --> 00:59:48,150
He is close to enlightenment,
 to Nirvana.

394
01:00:01,300 --> 01:00:03,484
Are you tired?
Yes.

395
01:00:30,900 --> 01:00:31,950
Come on.

396
01:00:39,304 --> 01:00:42,504
Ma!

397
01:00:45,480 --> 01:00:48,680
Ma! Ma! Ma!

398
01:00:49,936 --> 01:00:53,136
Help! Help! Why don't you help?
Help, quick!

399
01:00:53,568 --> 01:00:56,768
Why don't you help?

400
01:00:58,228 --> 01:01:01,428
Ma! Ma!

401
01:01:57,366 --> 01:02:00,017
Siddhartha...
Umh..

402
01:02:00,018 --> 01:02:03,218
I recognize you.

403
01:02:04,829 --> 01:02:07,514
Where is our son?

404
01:02:07,515 --> 01:02:11,481
Don't worry, he is here.

405
01:02:17,875 --> 01:02:21,127
We have grown old, my dear one.

406
01:02:27,221 --> 01:02:30,421
Once again

407
01:02:31,040 --> 01:02:35,048
come to me in my garden

408
01:02:35,080 --> 01:02:37,899
without clothes

409
01:02:37,900 --> 01:02:41,795
and with dusty feet.

410
01:02:49,217 --> 01:02:51,895
Your eyes...

411
01:02:51,896 --> 01:02:55,096
...your eyes are like his, Siddhartha.

412
01:02:56,714 --> 01:03:00,261
I have also grown older.
Did you recognize me?

413
01:03:00,262 --> 01:03:07,550
Yes, Kamala, I recognize you.

414
01:03:08,518 --> 01:03:11,730
Did you recognize him too?

415
01:03:20,140 --> 01:03:25,027
Have you...have you attained it?

416
01:03:25,493 --> 01:03:29,240
Have you found peace?

417
01:03:31,619 --> 01:03:34,477
Yes...

418
01:03:34,478 --> 01:03:37,678
I see it.

419
01:03:49,529 --> 01:03:52,729
I will also find peace.

420
01:04:02,069 --> 01:04:05,269
Kamala...

421
01:04:06,886 --> 01:04:10,086
You have already found peace.

422
01:07:50,548 --> 01:07:55,063
You have not slept, Siddhartha.
No, Vasudeva.

423
01:07:55,064 --> 01:07:58,105
I sat here and listened to the river.

424
01:07:58,106 --> 01:08:00,154
It has taught me a great deal.

425
01:08:00,155 --> 01:08:02,202
It has filled me with good thoughts.

426
01:08:02,203 --> 01:08:07,397
You have suffered, Siddhartha. But I see
that sadness has not entered your heart.

427
01:08:07,420 --> 01:08:12,046
Why shoud I be sad?
My son has been given to me.

428
01:08:24,931 --> 01:08:28,131
Son!

429
01:08:28,712 --> 01:08:31,912
Get me some water, son!.

430
01:08:37,411 --> 01:08:40,611
I'm not your servant!

431
01:08:48,803 --> 01:08:52,003
Why don't you beat me?

432
01:08:53,390 --> 01:08:56,590
You can't beat me?

433
01:08:56,929 --> 01:09:00,129
You're afraid to beat me.

434
01:09:00,818 --> 01:09:04,018
You're not even a man!

435
01:09:04,467 --> 01:09:08,641
I hate you. You're not my father!

436
01:09:43,423 --> 01:09:49,881
Your son does not want to be followed.
I must follow him. He's only a child.

437
01:09:49,882 --> 01:09:53,786
He can't go through the forest alone.
He'll come to some harm.

438
01:09:53,787 --> 01:09:57,656
He's not a child. He is a strong boy.
He will find his way to the town.

439
01:09:57,657 --> 01:10:01,005
No, Vasudeva. I must follow him.

440
01:10:36,100 --> 01:10:39,300
Hurry up! All right.

441
01:10:46,963 --> 01:10:50,163
I think I can get you out.

442
01:10:51,359 --> 01:10:54,559
Yeah!, out!

443
01:10:54,560 --> 01:10:58,087
I don't want to play with you.
Why? Just because I win?

444
01:10:58,088 --> 01:11:01,833
No, go home.
Why? I don't have to go home.

445
01:11:23,464 --> 01:11:27,049
Siddhartha...

446
01:11:31,015 --> 01:11:34,215
Vasudeva...
Siddhartha...

447
01:13:32,258 --> 01:13:36,680
Siddhartha, we have traveled
a long way on this river.

448
01:13:36,681 --> 01:13:40,818
I am old now.
I can't work anymore.

449
01:13:40,912 --> 01:13:44,112
Now it's time for me to go.

450
01:13:51,784 --> 01:13:55,951
But we will meet again, and remember...
Nothing remains the same.

451
01:13:55,982 --> 01:14:00,010
Everything changes...

452
01:14:00,011 --> 01:14:03,450
...and everything returns.

453
01:15:10,899 --> 01:15:14,099
Can you take across the river?

454
01:15:15,609 --> 01:15:19,394
Where are you going?
What are you seeking?

455
01:15:19,395 --> 01:15:22,866
I've been seeking for the right path all my life.

456
01:15:23,660 --> 01:15:28,458
Wll, may be you have looked for too much
too long, dear friend, Govinda.

457
01:15:29,903 --> 01:15:33,103
Siddhartha!
Govinda.

458
01:15:35,458 --> 01:15:37,870
My dear friend

459
01:15:37,871 --> 01:15:39,391
Govinda...

460
01:15:39,392 --> 01:15:43,392
The trouble with goals is that
one becomes obsessed with the goal.

461
01:15:43,702 --> 01:15:48,807
When you say you're seeking,
it means there is something to find.

462
01:15:48,808 --> 01:15:54,205
But the real freedom is the realization
 that there are no goals.

463
01:15:54,206 --> 01:15:57,406
There's only the now.

464
01:15:57,437 --> 01:16:04,280
What happened yesterday has gone.
What will happen tomorrow, we will never know.

465
01:16:04,310 --> 01:16:09,183
So we must live in the present.
Like the river.

466
01:16:11,184 --> 01:16:16,123
Everything returns.
Nothing remains the same.

467
01:16:17,088 --> 01:16:18,746
Govinda...

468
01:16:18,747 --> 01:16:23,653
Never is a man completely a saint
or completely a sinner.

469
01:16:23,684 --> 01:16:29,151
The Buddha is in the robber
and in the prostitute.

470
01:16:29,183 --> 01:16:33,222
God is everywhere.
Gos is everywhere?

471
01:16:36,704 --> 01:16:39,904
Umh! Do you see this stone?

472
01:16:40,288 --> 01:16:47,094
This stone, in a certain length of time,
will become soil.

473
01:16:47,096 --> 01:16:54,520
And from the soil it will become a plant,
maybe an animal or a man.

474
01:16:54,521 --> 01:16:59,345
Now, before I realized all this things,
I used to say, "This is a stone,

475
01:16:59,346 --> 01:17:01,374
 it does not have any value".

476
01:17:02,750 --> 01:17:08,850
Now...now I think,
this stone is not just a stone.

477
01:17:08,851 --> 01:17:14,600
It is an animal. It is God.
It is the Buddha.

478
01:17:14,602 --> 01:17:16,500
And in the cycle of change,

479
01:17:16,501 --> 01:17:21,350
it can become a man and spirit.

480
01:17:21,382 --> 01:17:27,259
It can have importance.
Because it has already long been everything.

481
01:17:31,125 --> 01:17:34,099
I love the stone.

482
01:17:34,100 --> 01:17:37,666
I love it. Just because it is a stone.

483
01:17:44,969 --> 01:17:47,052
Govinda!

484
01:17:47,053 --> 01:17:50,253
I can love without words.

485
01:17:50,291 --> 01:17:53,491
That's why I don't believe in teachers.

486
01:17:54,547 --> 01:17:56,187
The river..

487
01:17:56,188 --> 01:17:59,388
the river is the best teacher.

488
01:18:00,843 --> 01:18:02,989
Do you remember the Buddha?

489
01:18:02,990 --> 01:18:05,997
Yes, I remember the Buddha.

490
01:18:05,998 --> 01:18:10,650
And all those we have loved
and who have died before us.

491
01:18:10,670 --> 01:18:16,151
And I have come to recognize that we must...
we must forget searching.

492
01:18:16,152 --> 01:18:19,148
Tell me something that can help me.

493
01:18:19,149 --> 01:18:21,976
My path is hard.

494
01:18:21,977 --> 01:18:27,180
Tell me something I can understand.
Help me, Siddhartha.

495
01:18:28,965 --> 01:18:31,139
Stop searching.

496
01:18:31,140 --> 01:18:33,256
Stop worrying.

497
01:18:33,257 --> 01:18:36,677
And learn to give love.

498
01:18:37,245 --> 01:18:40,874
Stay and work with me by the river, Govinda.

499
01:19:03,056 --> 01:19:06,958
Be at peace, Govinda. Be at peace.

500
01:19:16,008 --> 01:19:19,558
Repair and Synchronization by
Easy Subtitles Synchronizer 1.0.0.0

