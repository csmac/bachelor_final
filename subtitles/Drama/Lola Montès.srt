0
00:00:20,527 --> 00:00:24,486
We shall not cease from exploration
And the end of all our exploring

1
00:00:24,927 --> 00:00:29,079
Will be to arrive where we started
And know the place for the first time.

2
00:00:29,527 --> 00:00:30,357
T.S. Eliot

3
00:00:30,807 --> 00:00:34,356
After the game is before the game.
S. Herberger

4
00:01:48,367 --> 00:01:50,278
<i>Man...</i>

5
00:01:50,727 --> 00:01:54,436
<i>... probably the most mysterious</i>
<i>species on our planet.</i>

6
00:01:54,847 --> 00:01:57,486
<i>A mystery of unanswered questions...</i>

7
00:01:57,927 --> 00:02:01,602
<i>Who are we? Where do we come from?</i>

8
00:02:01,687 --> 00:02:06,442
<i>Where are we going? How do we</i>
<i>know. What we think we know?</i>

9
00:02:07,847 --> 00:02:10,680
<i>Why do we believe anything at all?</i>

10
00:02:11,447 --> 00:02:14,837
<i>Countless questions</i>
<i>In search of an answer.</i>

11
00:02:15,247 --> 00:02:20,002
<i>An answer that will give</i>
<i>rise to a new question...</i>

12
00:02:20,087 --> 00:02:23,636
<i>... and the next answer will give</i>
<i>rise to the next question and so on...</i>

13
00:02:24,047 --> 00:02:25,799
<i>... and so on.</i>

14
00:02:27,047 --> 00:02:31,404
<i>But, In the end, Isn 't It</i>
<i>always the same question?</i>

15
00:02:31,887 --> 00:02:35,960
<i>And always the same answer?</i>

16
00:02:37,767 --> 00:02:41,362
The ball is round.
The game lasts 90 minutes.

17
00:02:41,767 --> 00:02:44,964
That's a fact. Everything
else is pure theory.

18
00:02:46,767 --> 00:02:47,438
Here we go!

19
00:02:53,727 --> 00:02:55,922
RUN LOLA RUN

20
00:04:38,047 --> 00:04:39,366
- Manni?
- Lola?

21
00:04:39,767 --> 00:04:41,598
What's up? Where are you?

22
00:04:42,047 --> 00:04:44,322
Where were you, damn it?

23
00:04:44,727 --> 00:04:45,955
I got there too late.

24
00:04:46,367 --> 00:04:49,006
But why today?
You're always on time! Always!

25
00:04:49,367 --> 00:04:52,439
My moped got ripped off.
It doesn't matter.

26
00:04:52,527 --> 00:04:53,562
Yes, it matters!

27
00:04:54,367 --> 00:04:55,686
What's wrong?

28
00:04:56,567 --> 00:04:59,525
<i>It wasn 't my fault, Manni.</i>
<i>I went to get some cigarettes.</i>

29
00:05:01,047 --> 00:05:02,560
<i>I can 't believe how fast the guy was.</i>

30
00:05:03,727 --> 00:05:07,037
<i>There was nothing I could do.</i>
<i>He was gone before I got outside.</i>

31
00:05:07,127 --> 00:05:08,446
<i>I even took a taxi.</i>

32
00:05:08,847 --> 00:05:11,805
<i>That dumb-ass drove east. They've</i>
<i>got a Grunewald St. There too.</i>

33
00:05:11,887 --> 00:05:14,037
<i>When I noticed, It was too late.</i>

34
00:05:14,447 --> 00:05:18,406
<i>I was so screwed up because</i>
<i>of our moped, I didn 't realize.</i>

35
00:05:18,487 --> 00:05:20,955
<i>- Doesn 't matter. - And when</i>
<i>I got there, you were gone.</i>

36
00:05:21,727 --> 00:05:24,366
<i>- It doesn 't matter now.</i>
<i>I'm done for. - But why?</i>

37
00:05:25,927 --> 00:05:30,318
Help me, Lola! I don't know what to do.

38
00:05:30,847 --> 00:05:34,317
You weren't there, and I messed
it up. I blew it. I'm such a jerk!

39
00:05:35,927 --> 00:05:38,600
<i>Just calm down. Now, what happened?</i>

40
00:05:39,047 --> 00:05:42,164
<i>Just tell me what happened, okay?</i>

41
00:05:42,247 --> 00:05:44,602
Lola, he's gonna kill me. I'm gonna die.

42
00:05:45,407 --> 00:05:48,126
Stop it! You're scaring me. What's up?

43
00:05:48,207 --> 00:05:51,563
<i>- Did you get caught? - No, but</i>
<i>that would have been something.</i>

44
00:05:52,327 --> 00:05:56,445
<i>Everything went really great. We drove</i>
<i>the cars there, and those guys came.</i>

45
00:05:57,087 --> 00:05:59,362
<i>They paid and that was it. Easy as pie.</i>

46
00:05:59,887 --> 00:06:02,401
<i>I got waved across the border...</i>

47
00:06:02,807 --> 00:06:05,037
<i>... and then theydropped me off out there.</i>

48
00:06:05,407 --> 00:06:08,843
<i>I went to see that cyclops,</i>
<i>and he was finished In no time.</i>

49
00:06:08,927 --> 00:06:11,202
<i>And everything was on time,</i>
<i>ex cept for you.</i>

50
00:06:12,127 --> 00:06:13,401
<i>You woeroe n't t hnoeroe</i>

51
00:06:16,047 --> 00:06:19,039
- Then what?
- Nothing. Not even a phone booth.

52
00:06:19,567 --> 00:06:23,799
<i>I couldn 't even call a taxi,</i>
<i>so I walked to the subway station</i>

53
00:06:24,127 --> 00:06:26,004
<i>On the train, there was this bum...</i>

54
00:06:26,407 --> 00:06:29,479
<i>... who somehow fell down. Suddenly,</i>
<i>these Inspectors showed up.</i>

55
00:06:29,887 --> 00:06:33,038
<i>And I got out like always.</i>
<i>An old reflex.</i>

56
00:06:44,207 --> 00:06:44,957
The bag!

57
00:06:45,327 --> 00:06:45,918
The bag!

58
00:07:18,127 --> 00:07:19,196
The bag!

59
00:07:19,927 --> 00:07:23,442
I'm such a fucking amateur!
A dumb-ass!

60
00:07:23,847 --> 00:07:27,920
<i>This could only happen to me! If you'd</i>
<i>picked me up, It wouldn 't have happened.</i>

61
00:07:28,407 --> 00:07:31,046
<i>I was so out of it.</i>
<i>You're always on time!</i>

62
00:07:31,527 --> 00:07:33,882
<i>- Didn 't you call the next station? - Sure.</i>

63
00:07:34,367 --> 00:07:37,439
<i>But It was too late. The</i>
<i>fucking bag was already gone.</i>

64
00:07:37,527 --> 00:07:39,643
<i>And that bum has it.</i>
<i>That plastic-bag freak!</i>

65
00:07:40,047 --> 00:07:42,959
<i>He 's on a plane to Florida or Hawaii...</i>

66
00:07:43,327 --> 00:07:46,717
<i>... or Canada or Hong</i>
<i>Kongor Bermuda or whatever.</i>

67
00:07:50,207 --> 00:07:51,242
<i>What about Ronnie?</i>

68
00:07:51,847 --> 00:07:53,838
<i>- He 'll kill me. - You gotta tell hlm.</i>

69
00:07:53,927 --> 00:07:54,803
<i>Forget it!</i>

70
00:07:54,887 --> 00:07:56,718
- Why? - He won't believe a single word!

71
00:07:57,247 --> 00:08:00,842
<i>I kept a carton of cigarettes</i>
<i>once. He noticed right away.</i>

72
00:08:01,527 --> 00:08:03,722
<i>He doesn 't believe anybody.</i>

73
00:08:04,127 --> 00:08:07,437
The job with those Mercedes
was a kind of test.

74
00:08:08,167 --> 00:08:11,603
To see if he could trust me. Shit.

75
00:08:12,367 --> 00:08:14,403
How much was there in the bag?

76
00:08:15,367 --> 00:08:17,562
- 100.000
- What?

77
00:08:18,847 --> 00:08:21,042
100.000. A test.

78
00:08:21,447 --> 00:08:22,323
Shit.

79
00:08:22,727 --> 00:08:26,356
<i>See! I knew you wouldn 't come up</i>
<i>with any Ideas. I always said...</i>

80
00:08:26,767 --> 00:08:29,327
...someday you wouldn't know what
to do. Not when you die. Sooner!

81
00:08:30,527 --> 00:08:34,805
You said, <i>'</i>'Love can do everything.<i>'</i>'
So find 100.000 marks in 20 minutes!

82
00:08:36,527 --> 00:08:40,964
At 12 by the water tower around
the corner, Ronnie said. In 20 minutes.

83
00:08:41,047 --> 00:08:42,366
Run away, Manni!

84
00:08:42,767 --> 00:08:43,677
- No.
- <i>Why not?</i>

85
00:08:44,087 --> 00:08:46,840
Nobody escapes Ronnie.

86
00:08:46,927 --> 00:08:47,996
I'll go with you.

87
00:08:48,407 --> 00:08:51,524
When Ronnie gets here in 20 minutes
I'll be a goner.

88
00:08:51,607 --> 00:08:52,801
- Stop it, Manni!
- What for?

89
00:08:53,207 --> 00:08:55,801
You can't get me 100 grand either!

90
00:08:55,887 --> 00:08:59,766
He'll rub me out, and all that's
left of me will be 100.000 ashes...

91
00:09:00,207 --> 00:09:04,519
...floating down the Spree to the sea.
No more Manni! You can't do a thing!

92
00:09:04,927 --> 00:09:06,280
Shut up!

93
00:09:15,927 --> 00:09:16,677
Listen.

94
00:09:17,207 --> 00:09:21,564
Wait for me. I'll help you. Stay put,
I'll be there in 20 minutes.

95
00:09:21,647 --> 00:09:23,842
<i>- Okay?</i>
<i>- You gonna pawn all your jewelry?</i>

96
00:09:23,927 --> 00:09:24,723
Where are you?

97
00:09:25,807 --> 00:09:29,482
In a phone booth downtown
in front of the Spiral Bar.

98
00:09:30,447 --> 00:09:32,915
Okay, stay where you are.
I'll come up withsomething.

99
00:09:33,407 --> 00:09:34,556
In 20 minutes, okay?

100
00:09:36,727 --> 00:09:39,002
What the hell. I'll go over to
Bolle and get that 100 grand.

101
00:09:39,407 --> 00:09:40,920
C ut it out!

102
00:09:41,007 --> 00:09:44,204
Ronnie said they make 200 grand a day
so they must have 100 grand by noon.

103
00:09:44,607 --> 00:09:48,043
<i>You're nuts! Don 't do a thing!</i>
<i>Just stay In that damned booth!</i>

104
00:09:48,447 --> 00:09:50,961
- That's it. I'll rob the store.
- Have you lost it?

105
00:09:51,327 --> 00:09:53,636
Don't do a thing! Stay put, I'm coming!

106
00:09:54,247 --> 00:09:55,077
Then what?

107
00:09:55,487 --> 00:09:56,602
I'll think of something!

108
00:09:57,007 --> 00:09:59,726
In 20 minutes I'll be dead
unless I steal the money!

109
00:10:00,127 --> 00:10:01,685
- No, wait!
- What for?

110
00:10:02,087 --> 00:10:04,476
I'll get the money somehow.

111
00:10:07,727 --> 00:10:09,445
I'm going there at 12
if you aren't here.

112
00:10:35,767 --> 00:10:37,883
Okay. Who?

113
00:10:37,967 --> 00:10:39,320
Who?

114
00:11:14,367 --> 00:11:15,197
Dad!

115
00:11:23,247 --> 00:11:25,807
Lola, are you going shopping?
I need shampoo!

116
00:11:28,607 --> 00:11:31,804
Of course, I knew that
Sagittariuswas your ascendant.

117
00:11:31,887 --> 00:11:36,642
Sure, the more I think about it.
I don't know.

118
00:11:37,767 --> 00:11:40,600
Yes, but you're married too.

119
00:12:34,047 --> 00:12:35,924
Watch where you're going!

120
00:12:38,807 --> 00:12:40,081
Bitch!

121
00:13:43,207 --> 00:13:44,799
It's worst at night.

122
00:13:46,047 --> 00:13:48,197
I wake up and can't fall asleep again.

123
00:13:48,927 --> 00:13:50,565
And then I'm afraid.

124
00:13:51,687 --> 00:13:53,643
Me! Afraid of the dark!

125
00:13:54,487 --> 00:13:58,400
I've never been afraid of the dark.
I've never even been afraid before.

126
00:14:00,207 --> 00:14:01,925
But I think about us...

127
00:14:05,167 --> 00:14:08,796
...and I just think
it'll keep going on like this.

128
00:14:09,247 --> 00:14:11,363
That you wouldn't dare...

129
00:14:21,207 --> 00:14:23,641
And then I ask myself
what I'm doing here.

130
00:14:24,047 --> 00:14:28,757
How long will this go on?
The secrecy, all this damned lying?

131
00:14:29,847 --> 00:14:34,045
Should I grow old, waiting for a man
who won't stand by me?

132
00:15:10,767 --> 00:15:12,325
Hey, you need a bike?

133
00:15:14,167 --> 00:15:15,998
50 marks, as good as new.

134
00:15:17,087 --> 00:15:18,042
No!

135
00:15:21,087 --> 00:15:21,917
AND THEN

136
00:16:07,527 --> 00:16:09,643
Hey man! I know that!

137
00:16:10,367 --> 00:16:12,676
It's not my fault it's so much money!

138
00:16:17,727 --> 00:16:20,525
500? What am I supposed to do
with 500 marks?

139
00:16:36,047 --> 00:16:37,082
Thanks.

140
00:16:48,487 --> 00:16:49,636
Your phone card!

141
00:17:09,727 --> 00:17:12,685
I have to go. Meyer will be
here in a minute.

142
00:17:17,047 --> 00:17:19,003
Ca n we nm eet ate r on?

143
00:17:20,767 --> 00:17:22,519
- Do you love me?
- What?

144
00:17:23,327 --> 00:17:24,726
Do you love me?

145
00:17:25,607 --> 00:17:27,279
Why are you asking me now?

146
00:17:27,727 --> 00:17:29,365
Do you love me?

147
00:17:30,047 --> 00:17:31,958
Yes, damn it!

148
00:17:32,967 --> 00:17:33,922
Then decide.

149
00:17:35,207 --> 00:17:36,401
Not now.

150
00:17:37,127 --> 00:17:38,958
You have to decide sometime.

151
00:17:39,327 --> 00:17:41,887
But why now, here, at once?

152
00:17:48,127 --> 00:17:49,719
Because I'm pregnant.

153
00:17:59,167 --> 00:18:00,919
Look who's here!

154
00:18:01,327 --> 00:18:05,002
Our little princess, Lola!
What a rare pleasure.

155
00:18:06,007 --> 00:18:07,918
- Why the rush?
- Please, let me in.

156
00:18:08,367 --> 00:18:12,042
Little Miss wants to see Big Daddy?
Sure thing.

157
00:18:23,927 --> 00:18:24,325
Sorry.

158
00:18:27,287 --> 00:18:28,083
AND THEN

159
00:18:35,607 --> 00:18:39,600
Tell me, do you want to
have a baby with me?

160
00:18:44,087 --> 00:18:44,997
Yes.

161
00:18:49,167 --> 00:18:49,997
Lola?

162
00:18:50,407 --> 00:18:51,362
Dad.

163
00:18:51,967 --> 00:18:53,366
Your daughter?

164
00:18:59,647 --> 00:19:02,445
I'm Jutta Hansen
from the board of directors.

165
00:19:02,527 --> 00:19:06,918
- Sorry to interrupt. It's urgent.
- It's okay. I was just about to...

166
00:19:08,887 --> 00:19:10,400
What are you doing here?

167
00:19:10,487 --> 00:19:11,715
What are you doing here?

168
00:19:16,367 --> 00:19:17,277
Nothing.

169
00:19:25,807 --> 00:19:26,523
Listen...

170
00:19:27,607 --> 00:19:29,996
...if I told you I needed your help...

171
00:19:30,087 --> 00:19:34,478
...more than I ever have before and
you're the only one who can help me...

172
00:19:34,887 --> 00:19:36,559
...would you help me?
- You look terrible.

173
00:19:36,967 --> 00:19:38,400
- Would you help me?
- What happened?

174
00:19:38,847 --> 00:19:41,202
I can't explain now.
Would you help me?

175
00:19:41,607 --> 00:19:42,323
With what, damn it?!

176
00:19:43,967 --> 00:19:45,525
I need 100.000 marks immediately.

177
00:19:46,167 --> 00:19:46,804
What?

178
00:19:47,207 --> 00:19:50,517
I need it within
the next five minutes or else...

179
00:19:50,927 --> 00:19:53,077
- Or else what?
- Something terrible will happen!

180
00:19:54,087 --> 00:19:56,317
I don't understand.
What's going on here today?

181
00:19:56,727 --> 00:19:59,446
Please, Dad! You've got to help me! Please!

182
00:19:59,527 --> 00:20:02,439
- I don't have 100.000 marks.
- What about my life insurance?

183
00:20:03,047 --> 00:20:05,515
What about it?
It isn't worth 100.000

184
00:20:05,927 --> 00:20:08,964
Please, Dad, I'm serious.
This isn't a joke!

185
00:20:09,327 --> 00:20:11,636
Yes, it is, Lola!
You can't be serious!

186
00:20:12,087 --> 00:20:14,317
- If you don't help me, he'll die!
- Who'll die?

187
00:20:14,727 --> 00:20:16,718
- Manni!
- Manni? Who's Manni?

188
00:20:17,127 --> 00:20:18,799
My boyfriend, for over a year!

189
00:20:19,207 --> 00:20:21,084
I don't even know him.
Why will he die?

190
00:20:21,487 --> 00:20:22,522
It doesn't matter!

191
00:20:30,207 --> 00:20:32,801
Okay. Come with me.

192
00:20:41,647 --> 00:20:43,524
- You're going to help me?
- Of course.

193
00:20:43,927 --> 00:20:46,282
Go home and go to bed.

194
00:20:46,847 --> 00:20:51,477
And tell your mother I won't be
home today or tomorrow or ever again.

195
00:20:51,567 --> 00:20:55,321
I'm leaving you guys
and I'm marrying another woman.

196
00:20:55,727 --> 00:21:00,403
We're going to have kids. I'll try to be
happy because she only wants me.

197
00:21:00,487 --> 00:21:03,684
I'm so sick of hearing
<i>'</i>'All you do is work.<i>'</i>'

198
00:21:04,127 --> 00:21:05,606
<i>'</i>'You always play the boss!<i>'</i>'

199
00:21:05,687 --> 00:21:09,202
Maybe I do, but so what!
You guys have no idea!

200
00:21:09,607 --> 00:21:12,838
I'm so tired of being
blamed for everything!

201
00:21:12,927 --> 00:21:16,476
Sure, Daddy's money is good enough
right? Well, not anymore!

202
00:21:16,567 --> 00:21:17,636
Not anymore.

203
00:21:20,847 --> 00:21:24,203
Besides, I'd never have
fathered a freak like you.

204
00:21:24,607 --> 00:21:26,996
- But you did, you jerk!
- No, I didn't!

205
00:21:30,567 --> 00:21:32,205
You're not mine.

206
00:21:40,047 --> 00:21:40,365
Now you know.

207
00:21:43,407 --> 00:21:46,604
The guy who sired you left
before you were even born.

208
00:21:49,087 --> 00:21:49,325
Throw her out, please.

209
00:21:52,767 --> 00:21:54,405
Conm e on! Get h e r out of h e re!

210
00:22:26,447 --> 00:22:28,597
Well, we all have our bad days.

211
00:22:31,727 --> 00:22:33,001
See you around.

212
00:22:45,847 --> 00:22:47,075
My dear.

213
00:22:47,687 --> 00:22:48,756
What's wrong?

214
00:22:50,247 --> 00:22:51,316
Do you have the time?

215
00:23:32,447 --> 00:23:33,562
This afternoon?

216
00:23:36,167 --> 00:23:37,202
All right.

217
00:23:38,607 --> 00:23:39,835
No, forget it. It's okay.

218
00:23:41,047 --> 00:23:41,877
Yeah.

219
00:25:11,727 --> 00:25:12,762
<i>Wait!</i>

220
00:25:14,167 --> 00:25:15,316
<i>Don 't do it!</i>

221
00:25:16,287 --> 00:25:17,515
<i>Manni, please!</i>

222
00:25:18,087 --> 00:25:18,325
<i>Wait!</i>

223
00:25:20,167 --> 00:25:21,441
<i>Wait for me!</i>

224
00:25:22,407 --> 00:25:23,999
<i>I'll be right there.</i>

225
00:25:24,647 --> 00:25:25,716
<i>Please.</i>

226
00:25:26,647 --> 00:25:28,000
<i>Manni, please...</i>

227
00:25:29,047 --> 00:25:30,321
<i>... wait!</i>

228
00:26:13,927 --> 00:26:16,805
Lie down on the floor. On the floor!

229
00:26:17,407 --> 00:26:18,726
Lie down on the floor.

230
00:26:24,447 --> 00:26:26,403
Hands behind your head, and keep quiet!

231
00:26:27,767 --> 00:26:28,324
Hands behind your head, and keep quiet!

232
00:26:29,607 --> 00:26:30,437
Open the registers!

233
00:26:31,127 --> 00:26:33,846
Open all the cash registers!
Open them!

234
00:26:36,607 --> 00:26:37,676
Open them, and lie down!

235
00:26:39,047 --> 00:26:41,003
I'll shoot anyone who bugs me!

236
00:26:42,487 --> 00:26:44,205
I'll shoot anyone who bugs me!

237
00:26:50,047 --> 00:26:51,366
Lola, where were you?

238
00:26:52,007 --> 00:26:53,440
I couldn't get here faster.

239
00:26:54,127 --> 00:26:55,037
Will you help me?

240
00:26:55,767 --> 00:26:57,086
Can't we just get out of here?

241
00:26:58,007 --> 00:26:59,804
Not anymore. You see the shit I'm in.

242
00:27:01,887 --> 00:27:03,764
Why didn't you wait for me?

243
00:27:04,167 --> 00:27:06,203
I did. You got here too late.

244
00:27:07,727 --> 00:27:09,080
So, are you with me?

245
00:27:09,447 --> 00:27:11,881
Put your hands up! Hurry up!

246
00:27:16,887 --> 00:27:17,683
Conm e on!

247
00:27:21,887 --> 00:27:22,922
Drop your gun!

248
00:27:23,527 --> 00:27:24,562
Hurry up!

249
00:27:31,207 --> 00:27:31,798
Don't move.

250
00:27:33,247 --> 00:27:34,362
The safety is on.

251
00:27:35,007 --> 00:27:37,077
- How does it work?
- The little lever on the side.

252
00:27:37,567 --> 00:27:40,684
Kid, you don't know how to use...

253
00:27:44,687 --> 00:27:45,324
Don't move.

254
00:27:48,927 --> 00:27:50,042
Conm e h e re

255
00:27:55,007 --> 00:27:56,838
Cover me. I'll grab the cash.

256
00:28:09,927 --> 00:28:11,804
Hurry, before the cops come.

257
00:29:03,207 --> 00:29:04,242
Don't move!

258
00:30:36,367 --> 00:30:37,686
Do you love me?

259
00:30:40,687 --> 00:30:41,915
Sure, I do.

260
00:30:45,207 --> 00:30:47,038
How can you be sure?

261
00:30:48,527 --> 00:30:51,519
I don't know. I just am.

262
00:30:52,527 --> 00:30:54,438
I could be some other girl.

263
00:30:57,327 --> 00:30:58,555
Why not?

264
00:31:00,367 --> 00:31:03,325
- Because you're the best.
- The best what?

265
00:31:03,727 --> 00:31:05,206
The best girl.

266
00:31:06,807 --> 00:31:08,365
In the whole world?

267
00:31:09,687 --> 00:31:10,722
Sure.

268
00:31:11,847 --> 00:31:12,359
How do you know?

269
00:31:14,647 --> 00:31:16,319
I just know.

270
00:31:18,287 --> 00:31:19,686
You think so.

271
00:31:21,847 --> 00:31:23,485
Okay, I think so.

272
00:31:24,327 --> 00:31:26,283
- You see?
- What?

273
00:31:27,007 --> 00:31:27,996
You aren't sure.

274
00:31:29,127 --> 00:31:31,402
Are you nuts, or what?

275
00:31:37,087 --> 00:31:39,282
What if you never met me?

276
00:31:40,767 --> 00:31:42,246
What do you mean?

277
00:31:44,527 --> 00:31:47,041
You'd be saying the same
thing to someone else.

278
00:31:47,127 --> 00:31:51,006
- Okay, if you don't want to hear it...
- I don't want to hear anything.

279
00:31:51,087 --> 00:31:52,520
I want to know how you feel.

280
00:31:53,167 --> 00:31:54,520
Okay.

281
00:31:55,607 --> 00:31:56,881
My feelings say...

282
00:31:59,247 --> 00:32:00,316
...you're the best.

283
00:32:01,487 --> 00:32:04,604
Your feelings?
Who is <i>'</i>'your feelings<i>'</i>' anyway?

284
00:32:05,007 --> 00:32:06,281
It's me.

285
00:32:09,367 --> 00:32:10,277
My heart.

286
00:32:10,687 --> 00:32:12,962
Your heart says
<i>'</i>' Hi, Manni. She's the one. <i>'</i>'

287
00:32:14,687 --> 00:32:15,517
Exactly.

288
00:32:15,927 --> 00:32:19,078
And you say, <i>'</i>' Thanks for the info.
Talk to you soon. <i>'</i>'

289
00:32:19,487 --> 00:32:20,442
Exactly.

290
00:32:21,807 --> 00:32:24,082
And you do whatever your heart says?

291
00:32:24,487 --> 00:32:27,160
Well, it doesn't really <i>'</i>'say<i>'</i>' anything.

292
00:32:27,247 --> 00:32:28,885
I don't know...

293
00:32:30,727 --> 00:32:31,318
...it just feels.

294
00:32:33,127 --> 00:32:34,924
So what does it feel now?

295
00:32:35,327 --> 00:32:38,285
That someone's asking
rather stupid questions.

296
00:32:39,287 --> 00:32:41,517
You aren't taking me seriously.

297
00:32:41,927 --> 00:32:43,519
Lola, what's wrong?

298
00:32:44,647 --> 00:32:46,285
Do you want to...

299
00:32:47,327 --> 00:32:48,442
...leave me?

300
00:32:51,127 --> 00:32:52,480
I don't know.

301
00:32:57,007 --> 00:32:59,282
I think I have to make a decision.

302
00:33:11,087 --> 00:33:12,679
But I don't want to...

303
00:33:13,967 --> 00:33:15,480
I don't want to leave.

304
00:33:22,487 --> 00:33:23,237
Stop!

305
00:33:29,727 --> 00:33:32,480
Lola, are you going shopping?
I need shampoo.

306
00:33:35,087 --> 00:33:38,079
Of course, I knew that
Sagittariuswas your ascendant.

307
00:33:38,447 --> 00:33:42,326
Sure, the more I think about it.
I don't know.

308
00:33:44,247 --> 00:33:46,477
Yes, but you're married too.

309
00:34:51,727 --> 00:34:52,477
Excuse me!

310
00:34:52,887 --> 00:34:54,684
Watch it, you stupid cow!

311
00:34:56,487 --> 00:34:57,920
Fucking bitch!

312
00:34:58,407 --> 00:34:59,237
AND THEN

313
00:36:04,047 --> 00:36:05,036
Hey, you need a bike?

314
00:36:06,287 --> 00:36:07,083
No.

315
00:36:07,167 --> 00:36:09,635
- 50 marks, as good as new.
- But it's stolen!

316
00:36:23,727 --> 00:36:26,002
500? What am I supposed to do
with 500 marks?

317
00:37:04,367 --> 00:37:06,483
Tell me, do you want to
have a baby with me?

318
00:37:12,087 --> 00:37:12,963
Yes.

319
00:37:23,567 --> 00:37:25,478
Even if it isn't yours?

320
00:37:30,967 --> 00:37:33,003
Look who's here! Lola, our...

321
00:37:33,407 --> 00:37:34,556
I'm in a hurry, please!

322
00:37:36,727 --> 00:37:37,762
Please let me in!

323
00:37:38,167 --> 00:37:41,079
Courtesy and composure are
the queen's jewels.

324
00:37:41,447 --> 00:37:42,436
Let me in, please.

325
00:37:42,527 --> 00:37:45,405
A little anger is good for the heart...

326
00:37:45,807 --> 00:37:48,196
...the circulation, the skin.

327
00:38:10,407 --> 00:38:12,079
What kind of love is that?!

328
00:38:12,567 --> 00:38:15,525
- If you hadn't neglected me...
- I have a family!

329
00:38:15,927 --> 00:38:19,886
I can't leave a sick wife and three
kids just to please her highness.

330
00:38:20,287 --> 00:38:22,005
Your wife is drunk
from morning to night!

331
00:38:22,447 --> 00:38:23,721
So what!

332
00:38:23,807 --> 00:38:24,557
What do you know?

333
00:38:25,407 --> 00:38:27,523
Can't you see you're interrupting?

334
00:38:32,367 --> 00:38:33,402
Hi, Dad.

335
00:38:34,327 --> 00:38:35,806
Why didn't you knock?

336
00:38:37,407 --> 00:38:38,635
What's going on?

337
00:38:39,047 --> 00:38:39,877
What do you want?

338
00:38:40,607 --> 00:38:41,960
Is she your daughter?

339
00:38:42,607 --> 00:38:43,517
Shut the door.

340
00:38:48,927 --> 00:38:49,996
What do you want?

341
00:38:50,447 --> 00:38:51,323
I need your help.

342
00:38:52,527 --> 00:38:54,916
- You see that I'm busy.
- There's no other way.

343
00:38:55,327 --> 00:38:58,319
Damn it! Why did you
have to show up here now?!

344
00:38:58,727 --> 00:39:00,604
I have a problem.
Do you understand?

345
00:39:04,647 --> 00:39:05,716
Who's that slut?

346
00:39:06,127 --> 00:39:07,196
That's none of your business!

347
00:39:16,327 --> 00:39:18,158
Lola, leave.

348
00:39:18,247 --> 00:39:19,600
Go home.

349
00:39:20,007 --> 00:39:21,884
Leave me alone.

350
00:39:22,807 --> 00:39:23,876
I can't!

351
00:39:24,447 --> 00:39:25,197
Why not?

352
00:39:26,487 --> 00:39:27,397
I need money.

353
00:39:28,807 --> 00:39:30,126
Then get a job.

354
00:39:30,207 --> 00:39:33,085
I will, but I need the money now!

355
00:39:33,927 --> 00:39:37,806
Okay, just so you get out of here.
How much?

356
00:39:38,207 --> 00:39:40,198
No, I need lots more.

357
00:39:40,607 --> 00:39:41,642
What do you mean, <i>'</i>'lots more<i>'</i>'?

358
00:39:42,287 --> 00:39:44,084
A whole lot more!

359
00:39:44,727 --> 00:39:48,561
Aren't you ashamed barging in like
this? Can't you see we're busy?

360
00:39:48,647 --> 00:39:50,717
I don't give a shit, you stupid cow!
I have other problems.

361
00:39:53,487 --> 00:39:54,556
Don't you dare...

362
00:40:23,047 --> 00:40:23,877
What?

363
00:40:40,687 --> 00:40:43,326
What's going on?

364
00:40:44,927 --> 00:40:47,361
It just isn't your day today.
Doesn't matter.

365
00:40:47,847 --> 00:40:49,678
You can't have everything.

366
00:41:26,007 --> 00:41:27,406
You're coming with me.

367
00:41:29,687 --> 00:41:31,245
Have you lost your mind?

368
00:41:31,727 --> 00:41:34,958
- You think you can do anything you like?
- Shut up! You go first.

369
00:41:35,487 --> 00:41:36,636
Kid...

370
00:41:37,607 --> 00:41:39,882
...you don't know how to
use that thing.

371
00:41:42,047 --> 00:41:43,480
Let me make a suggestion.

372
00:42:05,127 --> 00:42:06,196
Stay back.

373
00:42:09,807 --> 00:42:11,798
- Listen, we can talk this over.
- Fuck off!

374
00:42:16,127 --> 00:42:16,957
AND THEN

375
00:42:25,607 --> 00:42:26,403
Shit!

376
00:42:26,807 --> 00:42:27,956
The combination.

377
00:42:32,087 --> 00:42:32,997
Quiet!

378
00:42:39,087 --> 00:42:41,203
What the heck are you doing?

379
00:43:03,207 --> 00:43:07,041
Lola, there are cameras everywhere.
You'll never get out of here.

380
00:43:07,727 --> 00:43:09,399
The police will be here in a second.

381
00:43:09,807 --> 00:43:12,196
You said they always come too late.

382
00:43:13,127 --> 00:43:15,482
Bag it. 100.000

383
00:43:25,647 --> 00:43:27,080
Come on, kid.

384
00:43:28,447 --> 00:43:30,278
Leave your daddy alone.

385
00:43:31,287 --> 00:43:32,925
You don't want to
hurt anyone, do you?

386
00:43:34,087 --> 00:43:35,236
I don't know.

387
00:43:52,367 --> 00:43:53,880
- I don't have it.
- Why not?

388
00:43:55,607 --> 00:43:57,404
There's only 88.000

389
00:43:58,807 --> 00:44:01,844
I'll have to get the
rest from downstairs.

390
00:44:03,687 --> 00:44:04,676
Okay, get going!

391
00:44:06,167 --> 00:44:06,838
Go, go!

392
00:44:08,447 --> 00:44:09,675
Hurry up, man!

393
00:45:04,967 --> 00:45:06,241
100.000

394
00:45:09,927 --> 00:45:10,916
In there.

395
00:45:38,327 --> 00:45:39,396
Bye, Dad.

396
00:45:59,847 --> 00:46:00,677
I don't believe it.

397
00:46:22,487 --> 00:46:24,079
You want to get killed, kid?

398
00:46:29,727 --> 00:46:33,083
Attention! This is the police!

399
00:46:33,527 --> 00:46:36,803
The building is surrounded.
Come out with your hands up.

400
00:46:38,847 --> 00:46:40,519
What a ruckus!

401
00:46:40,927 --> 00:46:41,803
Do you have the time?

402
00:47:13,727 --> 00:47:15,285
Get out of the way!

403
00:47:17,127 --> 00:47:18,879
Give me a lift, will you?

404
00:47:19,287 --> 00:47:21,676
- Are you nuts?
- Wait!

405
00:48:25,927 --> 00:48:28,566
<i>Wait, Manni! I'm coming.</i>

406
00:48:28,967 --> 00:48:30,195
<i>I'm almost there...</i>

407
00:48:30,687 --> 00:48:31,722
<i>Wait!</i>

408
00:48:33,887 --> 00:48:36,526
<i>I'm gonna make it!</i>

409
00:50:33,207 --> 00:50:35,277
What would you do if I died?

410
00:50:36,327 --> 00:50:38,557
- I wouldn't let you die.
- Yeah, but...

411
00:50:39,927 --> 00:50:42,236
...what if I were fatally ill?

412
00:50:42,647 --> 00:50:44,683
- I'd find a way.
- What if I were in a coma...

413
00:50:45,087 --> 00:50:47,760
...and the doctor said
<i>'</i>'One more day<i>'</i>'?

414
00:50:48,167 --> 00:50:50,317
I'd throw you into the ocean.
Shock therapy.

415
00:50:50,727 --> 00:50:52,718
Okay, but what if I died anyway?

416
00:50:55,767 --> 00:50:57,803
- What do you want to hear?
- Come on, tell me.

417
00:50:59,047 --> 00:51:01,766
I'd go to the isle of R�gen
and cast your ashes to the wind.

418
00:51:01,847 --> 00:51:02,324
And then?

419
00:51:04,687 --> 00:51:06,803
I don't know. It's a stupid question.

420
00:51:07,207 --> 00:51:08,686
I know what you'd do.

421
00:51:09,887 --> 00:51:12,003
- You'd forget me.
- No!

422
00:51:12,407 --> 00:51:15,080
Sure, you would.
What else could you do?

423
00:51:16,127 --> 00:51:18,322
Sure, you'd mourn for a few weeks.

424
00:51:18,727 --> 00:51:22,606
It wouldn't be so bad.
Everybody's real compassionate.

425
00:51:23,327 --> 00:51:27,286
And everything's so incredibly sad,
and everyone feels sorry for you.

426
00:51:28,527 --> 00:51:30,722
You can show everyonehow strong you are.

427
00:51:31,127 --> 00:51:33,163
<i>'</i>'What a great woman, <i>'</i>' they'll say.

428
00:51:33,247 --> 00:51:36,444
<i>'</i>'She's pulling herself together
instead of crying all day. <i>'</i>'

429
00:51:38,687 --> 00:51:41,804
And suddenly, this really nice
guy with green eyes shows up.

430
00:51:42,207 --> 00:51:44,846
And he's super sensitive
and listens to you all day.

431
00:51:45,247 --> 00:51:47,203
And you can talk his ear off.

432
00:51:48,007 --> 00:51:50,521
And you can tell him how tough
things are for you...

433
00:51:50,927 --> 00:51:55,478
...that you have to look after yourself
and you don't know what'll happen...

434
00:51:57,887 --> 00:52:01,596
Then you'd hop onto his lap and cross me
off your list. That's how it goes.

435
00:52:04,927 --> 00:52:05,723
What?

436
00:52:06,127 --> 00:52:07,879
You're not dead yet.

437
00:52:17,927 --> 00:52:18,882
No?

438
00:52:33,847 --> 00:52:36,486
Lola, are you going shopping?
I need shampoo.

439
00:52:39,207 --> 00:52:41,323
Of course, I knew that
Sagittariuswas your ascendant.

440
00:52:42,527 --> 00:52:47,282
Sure, the more I think about it.
I don't know.

441
00:52:48,367 --> 00:52:50,756
Yes, but you're married too.

442
00:53:39,647 --> 00:53:40,477
AND THEN

443
00:54:37,727 --> 00:54:40,525
- Watch out!
- Sorry.

444
00:54:40,607 --> 00:54:42,518
I bet you're sorry!

445
00:55:01,687 --> 00:55:03,200
Fries and a sausage.

446
00:55:03,607 --> 00:55:04,357
Give me another one.

447
00:55:09,727 --> 00:55:11,240
I'll be damned.

448
00:55:12,207 --> 00:55:14,084
Life's really crazy
sometimes isn't it?

449
00:55:17,287 --> 00:55:19,278
Come on, kid.
I'll buy you a drink.

450
00:55:22,527 --> 00:55:23,357
You need a bike?

451
00:55:25,687 --> 00:55:27,723
A special price. 70 marks.

452
00:55:45,927 --> 00:55:46,837
Mr. Meyer?

453
00:55:47,247 --> 00:55:48,965
Is everything okay?

454
00:56:17,527 --> 00:56:20,883
Tell me, do you want to
have a baby with me?

455
00:56:24,447 --> 00:56:25,482
Yes.

456
00:56:29,527 --> 00:56:30,755
<i>Mr. Meyer Is here.</i>

457
00:56:32,407 --> 00:56:34,079
- There's something else.
- Not now.

458
00:56:34,487 --> 00:56:36,717
<i>Mr. Meyer asked If he should</i>
<i>parkor If you're coming out?</i>

459
00:56:37,127 --> 00:56:38,446
I'm coming.

460
00:56:39,087 --> 00:56:41,726
This is the nicest present
you could give me.

461
00:56:48,407 --> 00:56:49,681
I'll see you later?

462
00:57:05,647 --> 00:57:06,716
Hi there.

463
00:57:13,847 --> 00:57:15,565
- Hi there, Ms. J�ger.
- Hello.

464
00:58:03,887 --> 00:58:05,957
- Nice to see you.
- Hello, Mr. Meyer.

465
00:58:21,207 --> 00:58:23,004
Wait!

466
00:58:24,847 --> 00:58:28,920
You know what?
I just had a strange encounter.

467
00:58:29,527 --> 00:58:30,323
Yes, with your daughter!

468
00:58:31,527 --> 00:58:33,438
- With whom?
- Your daughter.

469
00:58:37,327 --> 00:58:39,318
Shit! Shit!

470
00:58:54,527 --> 00:58:55,357
You've come at last, dear.

471
00:59:25,247 --> 00:59:26,202
Thanks.

472
00:59:26,687 --> 00:59:27,756
Wait.

473
00:59:55,367 --> 00:59:57,722
So I decided, it'd be best
for me not to have kids.

474
00:59:58,127 --> 01:00:00,277
I work so muchthat
they'd never see me.

475
01:00:05,487 --> 01:00:06,397
Watch out!

476
01:00:38,447 --> 01:00:39,800
<i>What can I do?</i>

477
01:00:40,887 --> 01:00:42,320
<i>What can I do?</i>

478
01:00:44,287 --> 01:00:45,845
<i>What can I do?</i>

479
01:00:55,607 --> 01:00:56,722
<i>Come on.</i>

480
01:00:59,327 --> 01:01:00,442
<i>Help me.</i>

481
01:01:02,247 --> 01:01:03,316
<i>Please.</i>

482
01:01:05,487 --> 01:01:07,045
<i>Just this once.</i>

483
01:01:09,327 --> 01:01:11,079
<i>I'll just keep running, okay?</i>

484
01:01:16,727 --> 01:01:17,955
<i>I'm waiting.</i>

485
01:01:19,687 --> 01:01:20,915
<i>I'm waiting.</i>

486
01:01:22,247 --> 01:01:23,475
<i>I'm waiting.</i>

487
01:01:25,047 --> 01:01:26,196
<i>I'm waiting.</i>

488
01:01:34,087 --> 01:01:35,884
You wanna get killed?

489
01:02:08,607 --> 01:02:10,837
- How does this work?
- You buy chips...

490
01:02:10,927 --> 01:02:12,201
...and gamble them away.

491
01:02:16,047 --> 01:02:17,924
You can't go in there like that.

492
01:02:18,007 --> 01:02:19,042
I have to.

493
01:02:20,527 --> 01:02:23,246
99 marks 20. What kind of chips?

494
01:02:24,127 --> 01:02:24,877
One for 100.

495
01:02:26,167 --> 01:02:27,441
You're short.

496
01:02:28,327 --> 01:02:29,521
Please.

497
01:03:03,607 --> 01:03:05,882
Place your bets, please.

498
01:03:13,287 --> 01:03:15,005
Your bets, please.

499
01:03:25,127 --> 01:03:27,038
No more bets.

500
01:03:34,727 --> 01:03:37,525
Twenty, black, even, pass.
No series.

501
01:03:42,527 --> 01:03:44,916
1.500 on black.

502
01:03:51,287 --> 01:03:53,847
100 and 400.

503
01:03:57,247 --> 01:04:00,956
3.500 for 100 marks on 20.

504
01:04:05,927 --> 01:04:07,804
Here you are. 3.500.

505
01:04:21,007 --> 01:04:21,962
Come with me, please.

506
01:04:27,007 --> 01:04:28,440
J u st one nm ore ga nm e

507
01:04:37,127 --> 01:04:38,606
Please place your bets.

508
01:05:42,607 --> 01:05:46,043
Twenty, black, even, pass.

509
01:05:52,527 --> 01:05:53,801
Please hurry.

510
01:05:53,887 --> 01:05:55,957
I need a plastic bag.

511
01:06:46,207 --> 01:06:47,242
Stop!

512
01:07:05,807 --> 01:07:07,001
That's mine!

513
01:07:07,087 --> 01:07:08,406
I know!

514
01:07:20,287 --> 01:07:21,515
I'm sorry.

515
01:07:22,407 --> 01:07:23,317
What about me?

516
01:07:28,327 --> 01:07:30,204
At least give me that.

517
01:08:36,367 --> 01:08:37,402
What're you doing here?

518
01:09:01,287 --> 01:09:02,561
I'll stay with him.

519
01:12:04,927 --> 01:12:06,599
What happened to you?

520
01:12:08,927 --> 01:12:10,280
Did you run here?

521
01:12:12,087 --> 01:12:14,681
Don't worry. Everything's okay.
Come on.

522
01:12:23,167 --> 01:12:29,163
What's in the bag?

