1
00:00:00,609 --> 00:00:03,140
Well, guys, we made some
good progress today.

2
00:00:03,165 --> 00:00:04,861
And on top of that,
we taught Ed how to leave

3
00:00:04,886 --> 00:00:06,633
an outgoing message on his cell phone.

4
00:00:06,658 --> 00:00:09,242
- Let me see if I got this right.
- (Beeps)

5
00:00:09,267 --> 00:00:11,828
<i>Ed: Hi. This is Ed. Please leave a...</i>

6
00:00:11,853 --> 00:00:15,289
<i>- Patrick: Talk louder.
- Ed: I'm talking loud enough.</i>

7
00:00:15,314 --> 00:00:17,018
<i>Lacey: This is boring.</i>

8
00:00:17,043 --> 00:00:18,960
<i>Charlie: Lacey, just let him do it.</i>

9
00:00:18,985 --> 00:00:21,070
<i>Nolan: Want me to make it sounds
like you're at the beach?</i>

10
00:00:21,095 --> 00:00:23,065
<i>Ca-caw, ca-caw.</i>

11
00:00:24,052 --> 00:00:26,078
<i>Ed: Will you all just
shut the hell up?</i>

12
00:00:26,103 --> 00:00:28,007
(beeps)

13
00:00:29,174 --> 00:00:30,674
That's good enough.

14
00:00:32,039 --> 00:00:34,140
All right, everybody, see you Thursday.

15
00:00:34,992 --> 00:00:37,406
Hey, Charlie,
I know I'm behind on my payments.

16
00:00:37,431 --> 00:00:38,617
I'll have them next week.

17
00:00:38,642 --> 00:00:42,164
I found this site online where you can
rent out your apartment to strangers.

18
00:00:42,189 --> 00:00:45,161
Nolan, is paying me really worth

19
00:00:45,186 --> 00:00:47,929
leaving your apartment
in the hands of some potential lunatic?

20
00:00:48,321 --> 00:00:49,821
I think so.

21
00:00:50,000 --> 00:00:51,500
Me, too.

22
00:00:53,616 --> 00:00:56,087
If somebody else
is sleeping in your house,

23
00:00:56,112 --> 00:00:57,612
where are you gonna sleep?

24
00:00:58,171 --> 00:00:59,974
This is not an offer.

25
00:01:00,108 --> 00:01:02,479
I shouldn't have brought it up.
I'll see y'all.

26
00:01:04,655 --> 00:01:06,391
So where are you gonna sleep?

27
00:01:06,416 --> 00:01:07,916
You know how I'm a limo driver?

28
00:01:07,931 --> 00:01:09,476
I have this regular client.

29
00:01:09,501 --> 00:01:12,673
A rich guy with a really nice
guest house with nobody living in it.

30
00:01:12,698 --> 00:01:15,343
Well, he suggested I live in my limo.

31
00:01:16,788 --> 00:01:19,078
Limos are not made for living in.

32
00:01:19,103 --> 00:01:22,578
They're made for flashing people
right before you puke and pass out.

33
00:01:23,797 --> 00:01:25,297
You can't do this, Nolan.

34
00:01:25,298 --> 00:01:27,343
- Where are you gonna shower?
- Not an issue.

35
00:01:27,368 --> 00:01:29,960
The sub letter's only staying eight
nights, so I'll be right on schedule.

36
00:01:30,709 --> 00:01:32,209
I stand corrected.

37
00:01:32,382 --> 00:01:34,304
And a little further back.

38
00:01:37,871 --> 00:01:39,608
- Oh, hey, Jen.
- Hey, Charlie.

39
00:01:39,633 --> 00:01:41,179
I got great news.

40
00:01:41,204 --> 00:01:43,117
I'm dating a divorce attorney.

41
00:01:43,142 --> 00:01:46,085
You might recognize him
from the bus stop bench up the street.

42
00:01:46,281 --> 00:01:47,905
He sleeps on a bench?

43
00:01:47,930 --> 00:01:49,609
No, that's where his ad is.

44
00:01:49,634 --> 00:01:53,382
But you can't see it because there's
usually somebody sleeping in front of it.

45
00:01:53,796 --> 00:01:56,015
Well, congrats. That is great news.

46
00:01:56,173 --> 00:01:58,312
And I've got even better news.

47
00:01:58,337 --> 00:02:00,531
He says you owe me a crap load of money.

48
00:02:01,969 --> 00:02:04,656
Jen, we go through this every two years.

49
00:02:04,681 --> 00:02:06,757
I gave you everything
your lawyer asked for.

50
00:02:06,782 --> 00:02:08,653
You got more money
to live on every month than I do.

51
00:02:08,678 --> 00:02:10,187
Well, it doesn't seem that way.

52
00:02:10,212 --> 00:02:13,952
You make terrible decisions
and invest in terrible businesses.

53
00:02:13,977 --> 00:02:16,109
Remember Share-a-keet?

54
00:02:16,134 --> 00:02:19,734
Hey, there's a big market
for people who don't want the burden

55
00:02:19,759 --> 00:02:21,968
of caring for a parakeet full time.

56
00:02:23,245 --> 00:02:26,406
Yeah, but the birds got so stressed
out moving from house to house,

57
00:02:26,431 --> 00:02:29,905
they tried to kill themselves by banging
their heads against their little mirrors.

58
00:02:30,373 --> 00:02:32,359
Look, Charlie, say whatever you want,

59
00:02:32,384 --> 00:02:36,078
but my lawyer boyfriend thinks you have
more money than you let on.

60
00:02:36,080 --> 00:02:37,796
And you're gonna trust this guy?

61
00:02:37,798 --> 00:02:39,328
I've seen his picture on that bench.

62
00:02:39,330 --> 00:02:41,281
He's wearing a crown and
giving a thumbs-up.

63
00:02:42,562 --> 00:02:43,867
Whatever.

64
00:02:43,869 --> 00:02:46,507
And by the way, that
business would've worked.

65
00:02:46,509 --> 00:02:48,195
I just picked the wrong animal.

66
00:02:48,197 --> 00:02:50,906
- It should've been Share-a-dog.
- Yeah, that would've been much better.

67
00:02:50,908 --> 00:02:53,382
Hey, son, you know that puppy
that you love so much?

68
00:02:53,384 --> 00:02:56,671
Well, the month's up. Time to
give him back to the Johnsons.

69
00:02:57,377 --> 00:02:59,671
Hey, if you stop crying, I'll
give you a suicidal parakeet.

70
00:03:01,249 --> 00:03:03,249
S02E73
"Charlie and Sean's Twisted Sister"

71
00:03:03,250 --> 00:03:05,250
<font color="#ec14bd">Sync & corrections by solfieri</font>
<font color="#ec14bd">www.addic7ed.com</font>

72
00:03:05,948 --> 00:03:07,702
Thanks for dropping me off, Sean.

73
00:03:07,704 --> 00:03:09,373
I had a wonderful time last night.

74
00:03:09,375 --> 00:03:10,703
Yeah, it was amazing.

75
00:03:10,919 --> 00:03:13,022
I told you you'd like "Frozen."

76
00:03:13,093 --> 00:03:15,028
It wasn't half bad.

77
00:03:15,030 --> 00:03:18,164
I mean, you know, what I saw
of it from between your legs.

78
00:03:19,647 --> 00:03:20,976
See you tonight.

79
00:03:22,207 --> 00:03:26,492
Oh, by the way, tonight we're sexing it up
to my movie... "Reservoir Dogs."

80
00:03:26,494 --> 00:03:28,242
Oh, is that about dogs

81
00:03:28,244 --> 00:03:30,480
who are trying to find their
way home from a reservoir?

82
00:03:31,275 --> 00:03:32,726
Yeah. Yeah, it is.

83
00:03:33,386 --> 00:03:35,289
One's missing an ear. It's a whole thing.

84
00:03:40,376 --> 00:03:43,548
- Warden.
- Hello, Jordan. ls Charlie here?

85
00:03:43,550 --> 00:03:45,492
No, he's a little late.

86
00:03:45,675 --> 00:03:48,062
Look, I know you're upset with him.

87
00:03:48,064 --> 00:03:49,500
Damn right I am.

88
00:03:49,502 --> 00:03:53,226
Charlie told everybody about a mistake
I regretfully made when I was younger.

89
00:03:53,228 --> 00:03:56,906
And now I'm going to make his life a
living hell until I can get him fired.

90
00:03:56,908 --> 00:03:58,671
What's the big deal?

91
00:03:58,988 --> 00:04:00,835
You did a porn.

92
00:04:01,331 --> 00:04:03,187
It humanizes you.

93
00:04:03,189 --> 00:04:05,265
It's like in magazines

94
00:04:05,267 --> 00:04:07,369
when celebrities are walking a dog

95
00:04:07,371 --> 00:04:10,243
or pumping their own gas or...

96
00:04:10,245 --> 00:04:13,328
Licking chocolate syrup off their
best friend's boobs on the Internet?

97
00:04:14,509 --> 00:04:16,009
Yeah.

98
00:04:16,011 --> 00:04:18,226
"Wardens, they're just like us."

99
00:04:21,826 --> 00:04:24,163
- Warden.
- I just came by to tell you

100
00:04:24,165 --> 00:04:26,296
we're stepping up security
for all department heads

101
00:04:26,298 --> 00:04:28,301
involved in prisoner psychology.

102
00:04:28,463 --> 00:04:30,365
But that's just me.

103
00:04:30,367 --> 00:04:33,796
Oh, then I guess you'll be the only one
getting a cavity search every day.

104
00:04:34,851 --> 00:04:36,328
Look, I understand that you're mad,

105
00:04:36,330 --> 00:04:37,830
- but we can talk about...
- Every day.

106
00:04:39,758 --> 00:04:41,829
Bye, Warden. Thank you.

107
00:04:43,454 --> 00:04:45,359
Wow, you're so up her butt,

108
00:04:45,361 --> 00:04:48,375
if they gave her a cavity search,
they'd find you.

109
00:04:49,211 --> 00:04:51,031
Hey, can I ask you something?

110
00:04:51,209 --> 00:04:52,968
Sean and I were talking last night

111
00:04:52,970 --> 00:04:56,648
and the subject of maybe
moving in together came up.

112
00:04:56,650 --> 00:04:58,752
And a part of me, really wants to,
give it a shot,

113
00:04:58,754 --> 00:05:01,937
but Sean's just been a womanizer for
so long, I'm not sure he can change.

114
00:05:02,285 --> 00:05:04,718
Hey, I was a womanizer when I got married,

115
00:05:04,720 --> 00:05:06,929
but my wife took a leap of faith with me.

116
00:05:06,931 --> 00:05:08,164
And then you cheated on her

117
00:05:08,166 --> 00:05:10,094
with a woman from every
city in the country.

118
00:05:10,096 --> 00:05:13,167
That is not true. I never slept
with anybody in Pittsburgh.

119
00:05:13,586 --> 00:05:15,757
But, God, that woman was hot.

120
00:05:18,348 --> 00:05:20,718
Man, I am so ready for
this week to be over.

121
00:05:20,720 --> 00:05:22,593
Yeah, they still checking your tonsils

122
00:05:22,595 --> 00:05:24,406
from the wrong end at security?

123
00:05:25,359 --> 00:05:28,906
Yeah, and they just switched out
the little guard with a big guard.

124
00:05:29,505 --> 00:05:31,810
Man, I really miss that little guard.

125
00:05:34,735 --> 00:05:37,439
Oh, and Jen and her lawyer boyfriend

126
00:05:37,441 --> 00:05:39,765
- are digging around, for more money.
- Damn.

127
00:05:40,075 --> 00:05:43,014
You know, Jordan and I have been talking
about her moving into my place.

128
00:05:43,016 --> 00:05:44,968
When you and Jen were
first living together,

129
00:05:44,970 --> 00:05:47,062
did you ever think that it
would end up like this?

130
00:05:47,064 --> 00:05:49,301
No, no, no. When Jen and
I moved in together,

131
00:05:49,303 --> 00:05:50,828
I thought it was gonna last forever.

132
00:05:51,025 --> 00:05:54,085
But this other girl I was seeing,
she knew it was gonna go bad.

133
00:05:54,989 --> 00:05:57,171
The other girls always know, don't they?

134
00:05:57,422 --> 00:05:58,750
It's creepy.

135
00:06:00,183 --> 00:06:01,718
Speaking of other girls,

136
00:06:01,897 --> 00:06:04,534
do you see the hot brunette
sitting behind you?

137
00:06:04,738 --> 00:06:07,179
- She's been checking me out all night.
- You should do it.

138
00:06:07,364 --> 00:06:09,312
Oh, hey, if you really wanna turn her on,

139
00:06:09,314 --> 00:06:11,082
you should talk about your ex-wife.

140
00:06:11,084 --> 00:06:13,406
And her lawyer. And all the money stuff.

141
00:06:14,854 --> 00:06:16,140
That's perfect.

142
00:06:16,142 --> 00:06:17,367
Girls love baggage.

143
00:06:22,223 --> 00:06:24,059
- Hey there.
- Hi.

144
00:06:24,231 --> 00:06:27,168
Can I buy you a drink?
Unless you're an alcoholic.

145
00:06:27,170 --> 00:06:29,867
- In which case, can I buy you two drinks?
- (laughs)

146
00:06:30,039 --> 00:06:31,811
- I'm Maggie.
- Charlie.

147
00:06:32,073 --> 00:06:34,053
Are you with someone or can I join you?

148
00:06:34,055 --> 00:06:35,476
Please, have a seat.

149
00:06:35,478 --> 00:06:37,601
Just in town on business by myself.

150
00:06:38,084 --> 00:06:40,620
Let me guess, professional stunt woman.

151
00:06:41,129 --> 00:06:42,968
Actually, I work for Hickory Farms.

152
00:06:42,970 --> 00:06:44,265
Missed it by one.

153
00:06:45,827 --> 00:06:47,797
Hey, folks, there's a white Toyota Camry

154
00:06:47,799 --> 00:06:49,600
blocking the service entrance.

155
00:06:49,990 --> 00:06:52,195
- Crap, that's me. Excuse me.
- Sure.

156
00:06:52,197 --> 00:06:55,523
As long as I have your
attention, uh, I'm single again,

157
00:06:55,525 --> 00:06:58,203
so if there are any men here over 45,

158
00:06:58,602 --> 00:07:00,638
could you please leave

159
00:07:00,640 --> 00:07:04,380
so it's easier for me to see the
guys here who are between 25 and 30?

160
00:07:06,310 --> 00:07:08,585
(cell phone vibrating)

161
00:07:09,472 --> 00:07:11,708
Hey. How's it going with this girl?

162
00:07:11,710 --> 00:07:13,312
'Cause I'm thinking of taking off.

163
00:07:13,506 --> 00:07:15,542
Well, I thought it was going great

164
00:07:15,544 --> 00:07:17,044
until I saw this.

165
00:07:19,043 --> 00:07:22,000
"Just checking in. Did you
tail him to the bar yet?

166
00:07:22,002 --> 00:07:24,343
Let me know the second
you dig up anything."

167
00:07:24,611 --> 00:07:26,812
I'm think she's a private
investigator and she's tailing me.

168
00:07:26,814 --> 00:07:29,554
- Jen must've hired her.
- Why would Jen do that?

169
00:07:29,648 --> 00:07:32,318
She's trying to find all the money
I'm hiding that I don't have.

170
00:07:32,320 --> 00:07:34,320
That's why she's been
staring at me all night.

171
00:07:34,322 --> 00:07:37,273
I mean, I know that I'm easy on the eyes,
but sometimes you gotta take a break.

172
00:07:42,764 --> 00:07:43,877
(knocks on window)

173
00:07:44,567 --> 00:07:46,937
Coming.
Coming.

174
00:07:54,966 --> 00:07:57,453
Lacey, uh, come on in.

175
00:07:57,663 --> 00:07:59,195
Sorry about the mess.

176
00:07:59,197 --> 00:08:02,156
I had to take a bunch of
frat guys to a strip club

177
00:08:02,158 --> 00:08:03,859
and I am way behind on my housework.

178
00:08:05,108 --> 00:08:08,062
The guy you rented your
place to is loud as hell.

179
00:08:08,064 --> 00:08:10,046
He screams all night long.

180
00:08:10,048 --> 00:08:12,210
Gary? Yeah, he's an actor.

181
00:08:12,212 --> 00:08:14,679
He's auditioning for a
role in a horror film.

182
00:08:14,681 --> 00:08:16,062
I don't think he's the killer, though.

183
00:08:16,064 --> 00:08:18,500
Unless he's horrified
by what he's doing.

184
00:08:19,252 --> 00:08:22,718
I can't sleep. I can't think.
I can't do anything.

185
00:08:22,843 --> 00:08:25,445
Well, you could stay here.

186
00:08:25,804 --> 00:08:28,341
It's quiet and there's plenty of room.

187
00:08:28,343 --> 00:08:29,878
I'm not gonna sleep in this limo with you.

188
00:08:29,880 --> 00:08:32,483
- There's no privacy.
- What are you talking about?

189
00:08:32,485 --> 00:08:33,754
You can take the master bedroom

190
00:08:33,756 --> 00:08:36,070
and I'll just sleep on the
couch in the media room.

191
00:08:37,026 --> 00:08:39,109
It's all the same room.

192
00:08:41,224 --> 00:08:43,059
Fine, I'll stay.

193
00:08:43,061 --> 00:08:44,406
Great.

194
00:08:44,729 --> 00:08:46,835
Oh, and if you have any delicates,

195
00:08:46,837 --> 00:08:48,351
I'm doing another load.

196
00:08:49,637 --> 00:08:51,070
That's a champagne bucket.

197
00:08:51,072 --> 00:08:54,273
Yeah, not until I pick up Mr. and Mrs.
Feinstein tonight at 8:00.

198
00:08:54,275 --> 00:08:55,775
They're going to the opera.

199
00:08:59,612 --> 00:09:04,156
Dude, what is your problem,
driving like that?

200
00:09:04,158 --> 00:09:06,585
Hey, that is how you lose somebody
who is tailing you, all right?

201
00:09:06,587 --> 00:09:08,991
You cut across a church parking
lot, double back in the alley

202
00:09:08,993 --> 00:09:11,976
while they're sitting at a stoplight
somewhere wondering what the hell happened.

203
00:09:13,720 --> 00:09:15,351
She just parked down the street.

204
00:09:16,717 --> 00:09:17,913
How did she find us?

205
00:09:17,915 --> 00:09:19,850
Because she was hired by Jen, you idiot.

206
00:09:19,852 --> 00:09:21,257
Jen knows where I live.

207
00:09:21,259 --> 00:09:23,265
You ruined a church carnival for nothing.

208
00:09:24,612 --> 00:09:26,843
I told Jen to come over. I'm gonna
give her a piece of my mind.

209
00:09:26,845 --> 00:09:28,647
How long do you think that P.I.
is gonna wait out there?

210
00:09:28,649 --> 00:09:30,720
For all I know, she could
be there all night!

211
00:09:33,316 --> 00:09:35,320
You don't have to knock. I can see you.

212
00:09:37,413 --> 00:09:38,843
What's up?

213
00:09:38,845 --> 00:09:41,007
- Oh, hey, Sean.
- Hey, Jen.

214
00:09:41,009 --> 00:09:42,612
So I hear you're seeing Jordan.

215
00:09:42,614 --> 00:09:45,084
- How's that going?
- Yeah, great.

216
00:09:45,367 --> 00:09:47,539
I know what you're thinking,
but you know what?

217
00:09:47,541 --> 00:09:49,632
I've done a lot of growing up
since you and I were together.

218
00:09:49,634 --> 00:09:51,960
Oh, so mentally, you're, like, 12 now?

219
00:09:52,579 --> 00:09:54,968
Ouch. Sizzle. Dis. Burn.

220
00:09:55,701 --> 00:09:57,242
So what's going on, Charlie?

221
00:09:57,244 --> 00:09:58,515
What am I doing here?

222
00:09:58,517 --> 00:10:01,804
I know that your lawyer boyfriend
put a lot of ideas in your head,

223
00:10:01,806 --> 00:10:03,674
but having me followed by a P.I.

224
00:10:03,676 --> 00:10:06,213
to find all my hidden money
is way over the line.

225
00:10:06,215 --> 00:10:09,220
First of all, I'm not having
you followed by a P.I.

226
00:10:09,222 --> 00:10:10,824
And second of all, did you just admit

227
00:10:10,826 --> 00:10:12,895
that you have hidden money?

228
00:10:13,228 --> 00:10:14,562
Yes, yes.

229
00:10:14,564 --> 00:10:16,534
You nudge the giant spoon to the left

230
00:10:16,536 --> 00:10:19,796
which spins the refrigerator around,
revealing a secret stairwell

231
00:10:19,798 --> 00:10:21,765
leading down to my money cellar.

232
00:10:24,566 --> 00:10:26,066
What are you doing?

233
00:10:26,068 --> 00:10:28,605
Nothing. It was crooked.

234
00:10:29,058 --> 00:10:30,558
Don't look at me anymore.

235
00:10:31,843 --> 00:10:33,812
Oh, come on, Jen, just admit it.

236
00:10:34,656 --> 00:10:36,468
The P.I. is right out there in her car.

237
00:10:36,470 --> 00:10:38,250
- I could just ask her.
- Go right ahead.

238
00:10:38,252 --> 00:10:39,654
I didn't hire anybody.

239
00:10:39,656 --> 00:10:42,054
I'm not even seeing the lawyer anymore.

240
00:10:42,056 --> 00:10:45,085
After I left your house, I kind
of saw him in a different light.

241
00:10:45,087 --> 00:10:47,959
What, you realized what a
money-grubbing lowlife he is?

242
00:10:48,422 --> 00:10:51,054
That and when I came home,
he was wearing my stuff.

243
00:10:52,290 --> 00:10:53,790
Okay, okay, fine.

244
00:10:53,792 --> 00:10:56,671
But if you didn't hire the
P.I., then who the hell did?

245
00:10:56,824 --> 00:10:58,468
I'm gonna figure this out.
I'm good at these things.

246
00:10:58,470 --> 00:11:00,306
Charlie, do me a favor...
put on a pot of coffee.

247
00:11:00,308 --> 00:11:03,101
I'm gonna need some index
cards and some Scotch tape.

248
00:11:03,400 --> 00:11:05,187
Okay, who hates you right now?

249
00:11:05,189 --> 00:11:07,273
Oh, crap. It's the Warden.

250
00:11:07,275 --> 00:11:09,812
Thanks, Jen. You just sucked
all the life out of that one.

251
00:11:10,618 --> 00:11:12,734
Why would the Warden hire a P.I.?

252
00:11:12,736 --> 00:11:15,984
That is the real question.
This is gonna take some time.

253
00:11:15,986 --> 00:11:17,855
Because she wants to
catch me doing something

254
00:11:17,857 --> 00:11:19,659
that will violate my
contract and get me fired.

255
00:11:19,661 --> 00:11:21,945
Damn it. You didn't even give me a chance.

256
00:11:23,174 --> 00:11:25,911
Now I gotta figure out how
to turn this on the Warden.

257
00:11:25,913 --> 00:11:28,117
Yes. Now, three ways...

258
00:11:28,119 --> 00:11:29,920
- Three ways.
- I've got it.

259
00:11:29,922 --> 00:11:31,493
Son of a bitch.

260
00:11:34,324 --> 00:11:35,824
I wish I had something juicy to say,

261
00:11:35,826 --> 00:11:37,728
but he's just hanging
out with his neighbor.

262
00:11:37,830 --> 00:11:39,285
Neighbor's cute, though.

263
00:11:39,287 --> 00:11:42,239
I hired you to follow Sean.
What do you got?

264
00:11:42,779 --> 00:11:44,160
There's really nothing to report.

265
00:11:44,162 --> 00:11:46,239
I haven't even seen Sean
look at another woman.

266
00:11:46,400 --> 00:11:48,895
Okay, I just really need to know

267
00:11:48,897 --> 00:11:52,067
that I can trust this guy
enough to move in with him.

268
00:11:52,069 --> 00:11:54,973
I mean, the last guy that I
moved in with was my ex-husband

269
00:11:55,270 --> 00:11:57,489
and he ended up having an
affair with my sister.

270
00:11:58,588 --> 00:12:00,424
- Oh, my God!
- Thank you.

271
00:12:00,426 --> 00:12:01,567
You get it.

272
00:12:02,614 --> 00:12:03,831
Gotta go.

273
00:12:05,396 --> 00:12:07,566
- You scared the hell outta me.
- I just wanna know

274
00:12:07,568 --> 00:12:11,840
what a woman who works for Hickory Farms
is doing sitting outside my house.

275
00:12:11,842 --> 00:12:14,043
Unless you're going
door-to-door selling cheese.

276
00:12:15,124 --> 00:12:16,625
No, that's tomorrow.

277
00:12:16,957 --> 00:12:19,164
I'm just mapping out my route tonight.

278
00:12:19,429 --> 00:12:21,996
Do you live on this street?
(gasps) Do you like sharp cheddar?

279
00:12:22,823 --> 00:12:26,162
Look, I know you're a private
investigator and who you work for.

280
00:12:26,164 --> 00:12:27,760
I know you're not allowed
to say who it is,

281
00:12:27,762 --> 00:12:30,831
- but we both know.
- How'd you find out?

282
00:12:30,833 --> 00:12:33,572
Because I work very closely
with her and she's crazy.

283
00:12:35,178 --> 00:12:36,408
So here's the deal.

284
00:12:36,410 --> 00:12:38,658
I want you to stop working for
her and start working for me.

285
00:12:38,660 --> 00:12:40,557
I can't do that. That's unethical.

286
00:12:40,559 --> 00:12:42,486
I'll pay you double what
she's paying you right now.

287
00:12:42,488 --> 00:12:44,947
I am a P.I. It's not like I
took an oath or anything.

288
00:12:44,949 --> 00:12:46,455
What do you want me to do?

289
00:12:47,641 --> 00:12:49,743
I want you to dig up any
kind of dirt you can.

290
00:12:49,745 --> 00:12:51,908
The sleazier, the better.
You start tonight.

291
00:12:52,535 --> 00:12:54,033
That's too bad.

292
00:12:54,324 --> 00:12:56,424
I was thinking maybe we could go out,

293
00:12:56,426 --> 00:12:58,796
have some drinks, see what happens.

294
00:12:59,072 --> 00:13:01,338
I can't do that if I'm working for you.

295
00:13:01,340 --> 00:13:03,844
That is why I want you to start tomorrow.

296
00:13:07,155 --> 00:13:08,837
(knocks on window)

297
00:13:09,814 --> 00:13:12,432
Oh, sure. Just sit there.
I guess I'll get it.

298
00:13:13,866 --> 00:13:15,576
Don't forget to ask who it is.

299
00:13:15,578 --> 00:13:17,581
There's a giant window right here.

300
00:13:17,583 --> 00:13:19,053
Yeah, but it's tinted.

301
00:13:19,055 --> 00:13:21,358
- You can see out.
- Just ask.

302
00:13:21,360 --> 00:13:24,062
- It's Patrick and Ed.
- Ask!

303
00:13:26,017 --> 00:13:28,487
<i>- Who is it?
- Ed: Ed and Patrick.</i>

304
00:13:28,489 --> 00:13:31,103
Oh! Let 'em in!

305
00:13:32,714 --> 00:13:35,919
Hey, guys. Come in.
What a nice surprise.

306
00:13:37,588 --> 00:13:39,531
We thought we'd drop by

307
00:13:39,533 --> 00:13:42,080
and see how you guys are doing

308
00:13:42,550 --> 00:13:45,986
and drop off a little
house-warming present.

309
00:13:45,988 --> 00:13:48,224
Patrick: It's kind of traditional.

310
00:13:48,421 --> 00:13:52,168
Salt, so your life always has flavor.

311
00:13:52,170 --> 00:13:54,374
Bread, so you'll never know hunger.

312
00:13:54,376 --> 00:13:58,049
And motor oil because you
live in a damn limo.

313
00:13:58,812 --> 00:14:00,478
Ed!

314
00:14:00,849 --> 00:14:03,223
And motor oil 'cause you
live in a damn limo.

315
00:14:04,744 --> 00:14:07,334
Lacey, we have company. Maybe
you want to put something out?

316
00:14:07,336 --> 00:14:09,707
I would have loved to if you
hadn't eaten everything

317
00:14:09,709 --> 00:14:11,548
while you were playing
video games all day.

318
00:14:11,550 --> 00:14:14,578
Hey, I get one day off. I
want to come home and relax.

319
00:14:14,580 --> 00:14:16,590
So what do you think, I don't work?

320
00:14:16,592 --> 00:14:19,256
That keeping this car
nice for you isn't work?!

321
00:14:19,258 --> 00:14:23,094
Today, I dustbusted your old
Doritos out of the upholstery.

322
00:14:23,096 --> 00:14:24,676
I wasn't on vacation.

323
00:14:26,393 --> 00:14:29,061
Well, thanks for having us.

324
00:14:29,063 --> 00:14:31,936
- We'll be going now.
- You're not going anywhere!

325
00:14:34,906 --> 00:14:37,850
Nolan, why don't you
run down to the store

326
00:14:37,852 --> 00:14:39,320
and get our guests some snacks?

327
00:14:39,322 --> 00:14:41,759
And while you're at it, why don't
you grab a bottle of vodka?

328
00:14:41,761 --> 00:14:44,600
There it is! There's the real reason!

329
00:14:46,281 --> 00:14:48,383
What are you saying, you ass?

330
00:14:48,385 --> 00:14:50,455
I'm not gonna say it in
front of the company.

331
00:14:50,457 --> 00:14:52,611
- Just say it.
- Yes, please, please, just say it.

332
00:14:53,936 --> 00:14:55,905
You drink every night!

333
00:14:55,907 --> 00:14:57,630
You're damn right I do!

334
00:14:57,632 --> 00:15:00,521
I live in constant fear
of this car being towed

335
00:15:00,523 --> 00:15:02,659
because you don't know how to
read the damn parking signs.

336
00:15:02,661 --> 00:15:05,517
There's confusing!
They make them that way on purpose!

337
00:15:06,883 --> 00:15:10,011
I'm going out. Don't wait up for me.

338
00:15:13,743 --> 00:15:15,278
(door slams)

339
00:15:15,489 --> 00:15:16,554
My God, it's like

340
00:15:16,556 --> 00:15:18,810
"Who's Afraid of Virginia
Woolf" on wheels.

341
00:15:19,988 --> 00:15:23,341
She'll be back. She always comes back.

342
00:15:25,289 --> 00:15:28,175
Lacey!

343
00:15:28,177 --> 00:15:30,011
So now we're doing "Streetcar."

344
00:15:34,304 --> 00:15:37,074
- Hey.
- Hey. So what you got?

345
00:15:37,076 --> 00:15:38,341
I have tons of shots of her

346
00:15:38,343 --> 00:15:40,636
stealing hundreds of dollars
worth of office supplies.

347
00:15:40,638 --> 00:15:42,111
Oh, that's great.

348
00:15:42,113 --> 00:15:45,353
I can't wait to take this bitch down.
She thinks she's so high and mighty.

349
00:15:45,692 --> 00:15:46,964
Really?

350
00:15:46,966 --> 00:15:48,829
She seems like a chipper
little blonde to me.

351
00:15:48,831 --> 00:15:50,667
No, no, no, no. Do not
underestimate this woman.

352
00:15:50,669 --> 00:15:52,247
She's a killer.

353
00:15:53,033 --> 00:15:55,203
Seriously? Her?

354
00:15:55,205 --> 00:15:58,977
Trust me, she makes me submit
to a cavity search every day.

355
00:15:58,979 --> 00:16:01,183
Really? Why do you put up with that?

356
00:16:01,185 --> 00:16:04,327
- What else can I do?
- Just say no?

357
00:16:04,329 --> 00:16:05,714
I wish I could.

358
00:16:05,716 --> 00:16:07,645
It's the only way she'll
let me come to work.

359
00:16:09,321 --> 00:16:10,985
Listen, now that you're
done working for me,

360
00:16:10,987 --> 00:16:13,065
maybe we can hang out together again?

361
00:16:13,409 --> 00:16:14,540
Yeah, you know,

362
00:16:14,542 --> 00:16:16,688
your life seems a little
complicated right now.

363
00:16:16,690 --> 00:16:19,305
Why don't you sort things
out and then call me?

364
00:16:24,760 --> 00:16:26,963
Hello, Charlie. I'm
glad I caught you here.

365
00:16:26,965 --> 00:16:29,432
Well, there she is. I was just
about to come down to your office.

366
00:16:29,434 --> 00:16:32,723
- I have something, to show you.
- Me, too.

367
00:16:32,725 --> 00:16:34,294
Here's your new parking pass.

368
00:16:34,296 --> 00:16:36,688
I changed your spot so
you could be closer...

369
00:16:36,690 --> 00:16:38,241
to your house.

370
00:16:38,836 --> 00:16:40,380
I can't believe I ever liked you.

371
00:16:40,382 --> 00:16:43,007
- I can't believe, I ever slept with you.
- Get in line, missy.

372
00:16:47,432 --> 00:16:49,902
This little chess game is about to end.

373
00:16:50,157 --> 00:16:52,127
I think after you see these pictures,

374
00:16:52,129 --> 00:16:54,164
I'll be getting two things back...

375
00:16:54,166 --> 00:16:55,595
my parking space

376
00:16:55,597 --> 00:16:57,267
and the dignity of my butt.

377
00:16:59,409 --> 00:17:02,333
So feast your eyes on this.

378
00:17:03,235 --> 00:17:06,796
A picture of Jordan putting one
of our laser printers in her car?

379
00:17:06,935 --> 00:17:08,435
What?

380
00:17:11,435 --> 00:17:13,238
These are all pictures of Jordan.

381
00:17:13,240 --> 00:17:17,179
Yeah, and she's stealing a lot
of stuff from the prison.

382
00:17:17,181 --> 00:17:20,119
Boy, now I get to fire
your business partner.

383
00:17:20,733 --> 00:17:22,369
Thank you for this.

384
00:17:24,161 --> 00:17:27,500
- Warden, how you doing?
- I am awesome. You're fired.

385
00:17:28,438 --> 00:17:30,272
What the hell happened?

386
00:17:30,274 --> 00:17:33,251
I am still trying to figure it out.

387
00:17:33,302 --> 00:17:35,337
But here's what I do know,

388
00:17:35,339 --> 00:17:36,942
there's a P.I. out there

389
00:17:36,944 --> 00:17:39,286
who thinks you and I are into
some really freaky stuff.

390
00:17:43,000 --> 00:17:45,165
- (door opens)
- Nolan: Lacey, I'm home.

391
00:17:45,167 --> 00:17:48,075
I'm gonna make you the
best dinner you've ever...

392
00:17:50,048 --> 00:17:51,649
What's going on?

393
00:17:52,134 --> 00:17:54,938
I was hoping to be gone
before you got back.

394
00:17:54,940 --> 00:17:56,608
I'm leaving, Nolan.

395
00:17:57,819 --> 00:18:00,456
Oh, come on. You're just upset
'cause we're not getting along

396
00:18:00,458 --> 00:18:03,389
and we can't stand up
straight or eat hot food.

397
00:18:04,484 --> 00:18:06,246
I don't want to discuss it.

398
00:18:07,901 --> 00:18:09,804
I left you a note in the media room.

399
00:18:12,139 --> 00:18:14,159
I can't crawl past you.

400
00:18:14,161 --> 00:18:16,860
- Can you just tell me what it says?
- Yeah.

401
00:18:16,862 --> 00:18:19,349
Well, first it says that
by the time you read this,

402
00:18:19,351 --> 00:18:21,121
I'll be gone, so forget that.

403
00:18:22,783 --> 00:18:27,418
And then I told you that we
moved in together too quickly

404
00:18:27,420 --> 00:18:29,056
and it's just not gonna work out."

405
00:18:29,330 --> 00:18:32,742
So you're just gonna throw away the past
two and a half days because of what?

406
00:18:32,913 --> 00:18:35,699
30 or 40 horribly viscous fights?

407
00:18:38,301 --> 00:18:41,001
Every relationship has its ups and downs.

408
00:18:41,003 --> 00:18:44,528
Look, Nolan, this might have worked
out if things were different.

409
00:18:44,530 --> 00:18:47,202
Like if we didn't live in a car.

410
00:18:48,954 --> 00:18:51,659
But we do, so it's over.

411
00:18:53,415 --> 00:18:55,599
But we did have some
good times, didn't we?

412
00:18:59,250 --> 00:19:01,213
Of course we did.

413
00:19:01,709 --> 00:19:05,130
Like when we opened up the
sunroof and looked at the stars.

414
00:19:05,132 --> 00:19:06,537
Yeah.

415
00:19:06,728 --> 00:19:09,371
And then that homeless
guy tried to crawl in

416
00:19:09,373 --> 00:19:11,670
and I thought it was a zombie attack.

417
00:19:12,551 --> 00:19:15,766
But then you pepper sprayed him
and he ran down the street screaming...

418
00:19:15,768 --> 00:19:17,470
Both: "My eyes! My eyes!"

419
00:19:25,129 --> 00:19:26,716
That was fun.

420
00:19:26,718 --> 00:19:28,320
Yeah, yeah, it was.

421
00:19:30,784 --> 00:19:34,656
So I guess I'm just gonna
go back to my apartment.

422
00:19:35,567 --> 00:19:37,933
- Are you gonna stay?
- No.

423
00:19:39,188 --> 00:19:41,047
Too many memories.

424
00:19:44,497 --> 00:19:46,772
Did you talk to the Warden?
What did she say?

425
00:19:48,236 --> 00:19:51,426
I cannot believe you were actually
stealing office supplies.

426
00:19:52,557 --> 00:19:55,332
Look, it got outta hand. I feel terrible.

427
00:19:55,334 --> 00:19:57,989
I will pay for everything.
I just want my job back.

428
00:19:57,991 --> 00:20:00,812
Well, you got it.

429
00:20:00,814 --> 00:20:02,314
- Really?
- Yes.

430
00:20:05,590 --> 00:20:08,602
I found some loopholes in the system.

431
00:20:08,604 --> 00:20:11,442
There's a couple of situations
where you're not allowed to fire

432
00:20:11,444 --> 00:20:14,285
a state employee, and you're
in one of those situations.

433
00:20:14,287 --> 00:20:16,027
What's the loophole?

434
00:20:16,340 --> 00:20:17,869
You're a hardcore heroin addict.

435
00:20:19,126 --> 00:20:21,519
- What?!
- The Warden totally bought it.

436
00:20:21,521 --> 00:20:23,932
She said it explains your stealing
and the fact that you're

437
00:20:23,934 --> 00:20:25,997
radically unbearable to be around.

438
00:20:26,869 --> 00:20:28,597
That is crazy.

439
00:20:28,599 --> 00:20:30,885
I just stole some stupid office supplies.

440
00:20:30,887 --> 00:20:31,934
Well, you had to.

441
00:20:31,936 --> 00:20:35,338
It took a lot of paperclips to pay
for your ride on the white horse.

442
00:20:35,922 --> 00:20:38,128
I can't believe this.

443
00:20:38,130 --> 00:20:40,464
Look, none of this would've happened

444
00:20:40,466 --> 00:20:42,838
if hadn't hired a P.I. to follow Sean.

445
00:20:43,739 --> 00:20:45,773
I'm sorry.

446
00:20:46,109 --> 00:20:49,523
All that talk of moving in
together just freaked me out.

447
00:20:50,039 --> 00:20:51,945
Well, here's some good news,

448
00:20:51,947 --> 00:20:54,616
you can put moving in with Sean on
hold and think about it some more.

449
00:20:54,618 --> 00:20:55,640
Why?

450
00:20:55,642 --> 00:20:57,224
Because you're gonna
spend the next two weeks

451
00:20:57,226 --> 00:20:58,826
in government mandated rehab.

452
00:20:58,828 --> 00:21:00,562
Are you serious?

453
00:21:00,564 --> 00:21:02,619
Relax, it'll be like a vacation.

454
00:21:02,621 --> 00:21:06,090
You know that fancy rehab place in
Malibu where all the celebrities go?

455
00:21:06,092 --> 00:21:08,861
- Yeah.
- Yeah, well, it's about 30 miles past that.

456
00:21:10,733 --> 00:21:13,001
Oh, and if I were you,
I'd cut off your hair

457
00:21:13,003 --> 00:21:14,751
before somebody else does it for you.

458
00:21:15,153 --> 00:21:17,407
<font color="#ec14bd">Sync & corrections by solfieri</font>
<font color="#ec14bd">www.addic7ed.com</font>

