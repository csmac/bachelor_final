1
00:00:01,750 --> 00:00:09,750
TARTUFFE Adapted from Moli�res by
CARL MAYER Directed by F.W.MURNAU.

2
00:00:40,390 --> 00:00:46,494
"Great is the number of
hypocrites on earth..."

3
00:00:46,495 --> 00:00:50,881
"and many are the forms
in which they appear...!"

4
00:00:50,882 --> 00:00:55,790
"Many a time we unsuspectingly
sit next to them...!"

5
00:00:55,791 --> 00:00:58,322
"For see...!"

6
00:03:01,111 --> 00:03:06,647
"Perhaps, Your Honour would write
the letter to the Notary today...?"

7
00:03:11,347 --> 00:03:14,519
"The letter to the Notary..."

8
00:03:46,899 --> 00:03:54,899
My dear Notary! I have learned from information gathered by my faithul old house-keeper that
my grandson is leading a most unworthy and dissipated life - he having become an actor a...

9
00:04:12,892 --> 00:04:20,892
actor against my will. - I therefore have decided to leave my
entire fortune to the good old lady who attends to my household.

10
00:05:22,272 --> 00:05:25,034
"Now for the letter-box!"

11
00:06:38,644 --> 00:06:42,169
"ring to your heart's
content..."

12
00:08:25,171 --> 00:08:28,129
"Your grandson! The actor!"

13
00:08:39,617 --> 00:08:41,687
"Grandfather!"

14
00:08:44,225 --> 00:08:46,661
"Out with you!"

15
00:08:48,156 --> 00:08:50,194
"But, Granddad!"

16
00:08:52,195 --> 00:08:54,131
"Get out, I say!"

17
00:09:03,864 --> 00:09:09,035
"Someone's been handing you
too much 'soft soap'..."

18
00:09:10,290 --> 00:09:11,733
"Get out, I say!"

19
00:09:21,186 --> 00:09:26,159
"Perhaps the young gentleman would
call again some other day!"

20
00:09:45,111 --> 00:09:53,111
"You, who witnessed this scene, may rest assured
that I shall not give in without a struggle."

21
00:09:54,808 --> 00:09:58,808
"I shall come back and release my
grandfather from this humbug."

22
00:10:31,259 --> 00:10:33,687
"The Touring Cinema!
The grand Touring Cinema!"

23
00:10:41,431 --> 00:10:43,830
"We want no cinema!"

24
00:10:45,973 --> 00:10:48,405
"But, fair lady...!"

25
00:10:56,055 --> 00:10:58,290
"Fair lady..."

26
00:11:01,623 --> 00:11:03,963
"What will you show...?"

27
00:11:05,855 --> 00:11:09,291
"A story of Saints
and Sinners..."

28
00:11:10,933 --> 00:11:13,058
"Wait a minute!"

29
00:11:26,213 --> 00:11:29,688
"Pictures! Pictures!
What nonsence!"

30
00:11:40,804 --> 00:11:47,004
"Whereas, as a rule, day-in, day-out, I
only consider my master's well-being!"

31
00:11:56,705 --> 00:11:58,950
"I shall come!"

32
00:13:32,290 --> 00:13:35,952
"I shall be back in a minute."

33
00:14:59,074 --> 00:15:01,655
"The title of the comedy..."

34
00:15:03,647 --> 00:15:09,457
"Tartuffe - or the play of Mr. Orgon
and his well beloved friend."

35
00:15:17,246 --> 00:15:19,890
"Persons in the play."

36
00:15:38,312 --> 00:15:42,341
A play of all times
and all realms.

37
00:15:45,446 --> 00:15:48,449
"Voila, the play can begin!"

38
00:16:03,008 --> 00:16:07,553
"Elmira is expecting her husband
back from a long journey."

39
00:16:58,558 --> 00:17:01,727
"The carriage is in sight."

40
00:17:11,771 --> 00:17:14,718
"Dorine, I am so happy!"

41
00:17:33,998 --> 00:17:38,544
"Elmire, if you knew
how happy I am!"

42
00:17:41,384 --> 00:17:43,714
"Then kiss me!"

43
00:17:45,592 --> 00:17:50,753
"To kiss is to sin -
so teaches my friend Tartuffe."

44
00:17:52,744 --> 00:17:56,642
"Tartuffe - who is
your friend Tartuffe?"

45
00:17:58,436 --> 00:18:03,798
"A saintly man... I hurried on to
prepare all for his reception."

46
00:19:25,538 --> 00:19:33,046
"Hullo, Pierre, Jean, Jacques! Clear out all this
rubbish. Mr. Tartuffe does not approve of luxuries!"

47
00:19:45,510 --> 00:19:48,558
"I do not recognize him."

48
00:19:49,897 --> 00:19:53,034
"It's like a religious mania."

49
00:19:57,942 --> 00:20:04,386
"I'll bring him to his sences -
I'll give him a piece of my mind."

50
00:20:58,098 --> 00:21:04,874
There is a quaint looking fellow waiting
outside the house. He says he is expected.

51
00:21:09,981 --> 00:21:14,024
"You fool! And you didn't
ask him in?"

52
00:21:22,216 --> 00:21:29,481
"For heaven's sake put the lights out.
Tartuffe does not approve of extravagance."

53
00:22:05,172 --> 00:22:08,851
"What an alarming
looking fellow!"

54
00:24:18,335 --> 00:24:21,977
"Hullo - Jacques - Jean -
Pierre, Camille!"

55
00:24:35,184 --> 00:24:40,542
"Tartuffe does not approve
of so many servants, and so..."

56
00:24:43,489 --> 00:24:46,046
"you shall leave..."

57
00:24:47,810 --> 00:24:50,215
"and you..."

58
00:24:55,239 --> 00:24:57,676
"and you..."

59
00:25:05,181 --> 00:25:07,866
"go - all of you...!"

60
00:25:22,508 --> 00:25:30,245
"Oh, dear Sir, for twenty years have I
been in your service. Don't send me away!"

61
00:25:32,723 --> 00:25:34,760
"You shall stay..."

62
00:25:37,517 --> 00:25:40,595
"to wait on Mr. Tartuffe."

63
00:25:54,920 --> 00:26:02,920
"In the morning, Mr. Orgon personally sets
the breakfast table for Mr. Tartuffe."

64
00:27:44,599 --> 00:27:47,667
"First our morning prayer..."

65
00:27:49,407 --> 00:27:52,409
"Forgive me, my brother!"

66
00:29:42,764 --> 00:29:45,247
"Sinful frivolity!"

67
00:30:03,950 --> 00:30:07,100
"I just found the master's
pocket book...!"

68
00:30:11,415 --> 00:30:16,593
Reverves? From Mr. Orgon 500 Shillings
for the poor. 500... Tartuffe.

69
00:30:18,555 --> 00:30:22,133
Reverves? From Mr. Orgon 700 Shillings
for the poor. 700... Tartuffe.

70
00:30:28,970 --> 00:30:31,770
Reverves? From Mr. Orgon 2000 Shillings
for the poor. 2000... Tartuffe.

71
00:30:32,734 --> 00:30:35,593
"Where is this Tartuffe?"

72
00:31:05,289 --> 00:31:07,941
"Orgon..."

73
00:31:07,942 --> 00:31:10,060
"Dearest!"

74
00:31:20,760 --> 00:31:24,990
"Orgon, I must talk to you.
Please be sensible."

75
00:31:25,837 --> 00:31:28,753
"Gently, you'll wake him..."

76
00:31:33,751 --> 00:31:36,169
"Good heavens, Orgon..."

77
00:31:43,559 --> 00:31:47,491
"how unkempt and
neglected you are!"

78
00:32:08,773 --> 00:32:16,773
About the vanity of earthly things. The love of your wife and other persons must
be reset to nothing compared with the love you owe me, your holy friend Tartuffe.

79
00:32:24,839 --> 00:32:28,694
"This fellow is imposing
on you!"

80
00:32:33,984 --> 00:32:37,303
"Wait till you know him..."

81
00:32:42,824 --> 00:32:46,922
"and you will look
up to him as I do..."

82
00:32:51,796 --> 00:32:55,100
"Think how happy we were!"

83
00:33:00,167 --> 00:33:02,619
"I love you!"

84
00:33:07,786 --> 00:33:10,160
"I love you!"

85
00:33:20,900 --> 00:33:24,310
"There, now you've
disturbed him!"

86
00:33:39,924 --> 00:33:43,740
"My brother, I fear
you are cold!"

87
00:34:25,571 --> 00:34:31,032
"Very well... I will try to get
to know your friend Tartuffe."

88
00:34:33,039 --> 00:34:35,627
"Send him to me."

89
00:34:41,147 --> 00:34:47,066
"Oh, convince her and convert
her to your way of thinking!"

90
00:36:23,555 --> 00:36:28,349
"If you are a true friend
of my husband's..."

91
00:36:36,296 --> 00:36:41,240
"then quit this house
and go away from here."

92
00:36:44,562 --> 00:36:49,671
"You are forcing my husband
into religious fanaticism."

93
00:37:35,926 --> 00:37:38,219
"Dear lady..."

94
00:37:40,607 --> 00:37:43,793
"Heaven sent me to you..."

95
00:37:46,504 --> 00:37:50,126
"In that case... pray...
Forgive me!"

96
00:38:19,102 --> 00:38:23,683
"The impudent fellow!
Call my husband immediately!"

97
00:38:43,574 --> 00:38:47,518
"I won't have my friend
slandered!"

98
00:38:49,009 --> 00:38:53,153
"and if I were to
give you proofs...?"

99
00:39:11,140 --> 00:39:18,467
"I will put him to the test and you...
without being seen... shall be my witness."

100
00:39:25,744 --> 00:39:30,627
"Right... make whatever
arrangements necessary!"

101
00:40:09,251 --> 00:40:13,721
"His blind credulity forces
me to take this step."

102
00:41:39,059 --> 00:41:47,059
Dear Mr. Tartuffe! I beg your pardon that I brote off our conversation in this way. I should like to
talk over one or two things with you. Will you take a cup of tea with me this afternoon? Mrs. Elmire.

103
00:42:31,907 --> 00:42:37,162
"I accept your suggestion merely
in order to convince you!"

104
00:42:40,576 --> 00:42:45,738
"Very well, we will see
how he stands the test."

105
00:43:09,387 --> 00:43:11,729
"Here he comes."

106
00:43:17,277 --> 00:43:21,773
"Oh, that you would but
believe my words..."

107
00:43:23,762 --> 00:43:26,985
"Spare me this masquerade!"

108
00:43:30,817 --> 00:43:33,096
"I believe in him...!"

109
00:45:37,139 --> 00:45:40,040
"I wish to... confess to you..."

110
00:45:48,278 --> 00:45:52,842
"why I asked you to leave
us this morning..."

111
00:45:54,862 --> 00:46:00,635
"I fear... I too might fall under
the spell of your personality."

112
00:46:12,637 --> 00:46:14,757
"I fear... I love you!"

113
00:47:31,506 --> 00:47:33,968
"I felt a draft!"

114
00:47:55,946 --> 00:47:59,386
"Is it a sin to love
you... a saint?"

115
00:49:04,175 --> 00:49:07,713
"I have listened to
your confession."

116
00:49:18,607 --> 00:49:21,272
"I shall pray for you!"

117
00:49:34,418 --> 00:49:36,919
"Now, do you believe in him?"

118
00:49:39,663 --> 00:49:42,940
"Yes, but he must never know..."

119
00:49:44,288 --> 00:49:47,751
"that I induced you to listen."

120
00:49:59,346 --> 00:50:04,519
"Oh, my Lord! Have mercy
on the unhappy woman!"

121
00:50:48,779 --> 00:50:55,116
"There is no other way... I must
risk all to save my husband."

122
00:52:22,443 --> 00:52:26,684
"I must see you... alone!"

123
00:52:34,425 --> 00:52:37,810
"Send Dorine to bed
early tonight."

124
00:53:18,935 --> 00:53:26,935
"Oh, my Lord, give me the strength to rescue my
husband from the clutches of this hypocrite!"

125
00:53:42,947 --> 00:53:49,482
When Dorine went to bed...

126
00:55:45,797 --> 00:55:50,971
I herewith bequeath to my faithful
friend, Tartuffe, my entire fortune...

127
00:56:16,578 --> 00:56:23,107
"I implore you... ask no questions,
oh my master, but come with me!"

128
00:57:03,413 --> 00:57:06,569
"Look! That you may be cured!"

129
00:58:26,729 --> 00:58:32,032
"I am so afraid my
husband might come in."

130
00:58:35,928 --> 00:58:42,463
"No fear! He is obediently writing his
last will and testament in my favor."

131
00:58:55,614 --> 00:58:58,298
"Are we not sinning?"

132
00:59:02,125 --> 00:59:05,965
"Who sins in secret,
does not sin!"

133
00:59:09,173 --> 00:59:11,928
"You say that... a Saint?"

134
00:59:13,759 --> 00:59:16,013
"I... a Saint...?"

135
00:59:20,492 --> 00:59:23,851
"Now, are you convinced, Orgon?"

136
01:00:06,954 --> 01:00:13,883
"Oh, my Lord, I thank you for helping
me to regain my husband's love!"

137
01:00:23,123 --> 01:00:28,167
"From this time forth, all
hypocrites are called Tartuffe!"

138
01:00:29,471 --> 01:00:33,060
"Often they sit beside you..."

139
01:00:35,015 --> 01:00:37,899
"without your knowing it..."

140
01:00:51,191 --> 01:00:58,373
"You are stealthily trying to obtain
possession of my grandfather's money."

141
01:01:00,034 --> 01:01:02,048
"Stealthily...?"

142
01:01:18,090 --> 01:01:21,020
Poison!

143
01:01:34,243 --> 01:01:37,205
"A slow but sure poison..."

144
01:01:42,377 --> 01:01:46,878
"Your role is played out...
Go pack your boxes."

145
01:01:59,177 --> 01:02:01,135
"Tartuffe!"

146
01:02:05,540 --> 01:02:07,739
"Tartuffe!"

147
01:02:12,725 --> 01:02:17,899
"Was I blind? How could
I trust that awful person?"

148
01:02:20,555 --> 01:02:25,750
"Dear Grandfather... none of us
is proof against hypocrites."

149
01:02:28,521 --> 01:02:30,994
"And therefore you..."

150
01:02:30,995 --> 01:02:33,450
"do you know..."

151
01:02:33,451 --> 01:02:36,714
"who is sitting beside you?"

152
01:02:37,750 --> 01:02:38,750
THE END

