1
00:00:00,000 --> 00:00:04,880
"STRIKE", the first film
of prominent director

2
00:00:05,080 --> 00:00:09,560
Sergei Eisenstein,
has been released in 1925.

3
00:00:09,760 --> 00:00:11,960
It played a considerable part

4
00:00:12,160 --> 00:00:15,520
in the development

5
00:00:15,920 --> 00:00:20,040
of the art of cinema.

6
00:00:21,680 --> 00:00:24,280
A Production of the
First State Film Factory.

7
00:00:24,480 --> 00:00:27,440
The film has been restored
in Gorki studios in 1969.

8
00:00:28,000 --> 00:00:32,920
STRIKE

9
00:00:33,120 --> 00:00:38,203
Scenario PROLETKULT Under
the Editorship of V. Pletnykov

10
00:00:41,963 --> 00:00:46,763
Performed by the First
Workers' Theatre of Proletkult

11
00:00:48,083 --> 00:00:50,603
Directed by
Sergei Eisenstein

12
00:00:50,803 --> 00:00:54,843
Camera
Eduard Tisse

13
00:00:56,403 --> 00:00:59,723
Sets
V. Rakhals

14
00:01:01,123 --> 00:01:05,723
"The strength of the working
class is organization.

15
00:01:05,923 --> 00:01:10,563
Without organization of the masses,
the proletariat is nothing.

16
00:01:10,763 --> 00:01:15,123
Organized, it is everything.
Being organized means unity of action,

17
00:01:15,363 --> 00:01:19,803
the unity of pratical activity."
Lenin, 1907

18
00:01:20,483 --> 00:01:26,603
PART ONE :
All is calm at the factory

19
00:01:49,403 --> 00:01:50,803
All is calm at the factory,

20
00:01:51,003 --> 00:01:52,767
BUT...

21
00:02:04,447 --> 00:02:06,327
There is trouble brewing.

22
00:03:18,007 --> 00:03:19,927
They hurry to...

23
00:03:32,810 --> 00:03:34,650
The Director

24
00:04:03,770 --> 00:04:05,450
Reporting

25
00:04:10,210 --> 00:04:11,850
He laid down the law.

26
00:04:37,210 --> 00:04:38,730
Through the hierarchy.

27
00:05:21,613 --> 00:05:25,533
"Get me the files
on the factory district."

28
00:05:42,373 --> 00:05:46,893
Information of
"Private Agents".

29
00:06:00,053 --> 00:06:03,777
List of Agents who spy:

30
00:06:04,017 --> 00:06:08,537
The Monkey, The Quiet One,
The Patriarch, The Fellow-Countryman,

31
00:06:08,777 --> 00:06:13,337
Zoya, Bulldog, The Fox, The Taylor,
The Shepherd, The Owl, The Fly-by-night.

32
00:06:26,977 --> 00:06:28,617
Preparations.

33
00:06:55,017 --> 00:06:56,577
"The Fox"

34
00:07:04,057 --> 00:07:05,897
Alms for the Blind.

35
00:07:31,417 --> 00:07:32,777
"The Owl"

36
00:07:43,940 --> 00:07:46,900
To the meeting place.

37
00:08:02,340 --> 00:08:03,860
"The Monkey"

38
00:08:58,623 --> 00:09:00,263
"The Bulldog"

39
00:09:32,863 --> 00:09:34,863
A group of activists.

40
00:10:07,103 --> 00:10:10,663
"They've pushed us into a corner,
so we've got to strike."

41
00:10:13,867 --> 00:10:16,867
"With our people..."

42
00:10:22,267 --> 00:10:27,267
"It would be better if the
people didn't go out on strike."

43
00:10:32,507 --> 00:10:34,147
Preparations.

44
00:10:46,667 --> 00:10:48,307
"Agitate everywhere."

45
00:10:49,707 --> 00:10:53,627
"The Owl" is in a tight spot.

46
00:11:02,347 --> 00:11:03,547
A spy.

47
00:11:26,227 --> 00:11:29,187
Secret meetings.

48
00:12:08,790 --> 00:12:10,990
They're stirring up the people.

49
00:12:54,670 --> 00:12:56,790
Accordion accompaniment.

50
00:13:48,753 --> 00:13:50,713
"You've become even more
insolent, you scum."

51
00:13:59,673 --> 00:14:02,633
Meanwhile,
in the underground...

52
00:14:08,713 --> 00:14:11,433
CALLS TO STRIKE

53
00:14:17,273 --> 00:14:19,593
The boys are taking action.

54
00:15:01,197 --> 00:15:05,677
"Enough of patience. Let's go out
on strike, comrades, into the struggle."

55
00:15:13,557 --> 00:15:16,557
PART TWO
A REASON TO STRIKE

56
00:15:23,597 --> 00:15:25,237
The micrometer is stolen.

57
00:15:46,397 --> 00:15:49,037
The price of the micrometer
is 25 rubles.

58
00:15:59,477 --> 00:16:04,040
A loss of three weeks pay.

59
00:16:04,960 --> 00:16:07,920
To the office to report the loss.

60
00:16:28,360 --> 00:16:32,280
"We haven't had any thieves before."

61
00:16:46,120 --> 00:16:47,440
The Manager.

62
00:17:04,480 --> 00:17:05,760
"Thief!"

63
00:17:21,683 --> 00:17:22,723
Thief...

64
00:18:39,087 --> 00:18:43,727
Comrades, the boss
accused me of the theft.

65
00:18:43,967 --> 00:18:48,647
I'm not guilty but I can't prove it.
I cannot leave the factory with

66
00:18:48,887 --> 00:18:53,567
the stigma of a thief. So I have
decided to put an end to myself.

67
00:18:53,807 --> 00:18:58,527
Goodbye and remember I'm not guilty.
Yakov Strongen."

68
00:19:12,167 --> 00:19:13,807
"Break it up!"

69
00:19:51,367 --> 00:19:53,367
"Stop work!"

70
00:20:15,130 --> 00:20:16,650
The master craftsmen.

71
00:21:19,850 --> 00:21:21,610
"Blow the whistle!"

72
00:21:25,690 --> 00:21:26,930
"Go to hell!"

73
00:22:11,493 --> 00:22:13,093
For the whistle.

74
00:22:43,137 --> 00:22:45,457
To the Old Foundry.

75
00:22:59,337 --> 00:23:00,657
The assembly.

76
00:23:10,937 --> 00:23:13,897
The Old Foundry resists.

77
00:23:24,057 --> 00:23:26,977
"C'mon, lads, let's take it!"

78
00:24:41,420 --> 00:24:44,140
"To the office, comrades!"

79
00:25:16,500 --> 00:25:19,180
"Don't open the gates!

80
00:25:19,420 --> 00:25:22,660
Let no one in or out."

81
00:25:28,140 --> 00:25:29,900
The Tribune of Labor.

82
00:25:30,900 --> 00:25:32,540
In the office.

83
00:25:44,503 --> 00:25:48,183
"Without our labor
the furnaces will go out,

84
00:25:48,423 --> 00:25:50,583
the machines will stop,
the plant will die."

85
00:25:55,303 --> 00:25:57,943
"We have the power,
we are the power,

86
00:25:58,183 --> 00:26:01,703
if we are united to struggle
against the capital."

87
00:26:08,743 --> 00:26:12,023
A "carriage" for the administration.

88
00:28:29,907 --> 00:28:33,990
"Why didn't you warn us
about all this?!"

89
00:29:18,790 --> 00:29:21,670
PART THREE
THE PLANT STOOD STOCK-STILL

90
00:29:39,350 --> 00:29:41,310
The first days.

91
00:29:47,633 --> 00:29:49,913
"Get up for work."

92
00:30:17,993 --> 00:30:20,273
Copying their fathers.

93
00:31:42,517 --> 00:31:44,797
Orders pour in...

94
00:31:56,797 --> 00:31:59,037
Everything on which their
thrones rest

95
00:31:59,317 --> 00:32:01,237
is made by the worker's hands.

96
00:32:39,277 --> 00:32:41,237
"Is that on strike too?!"

97
00:32:46,480 --> 00:32:48,800
Formulating demands.

98
00:33:06,760 --> 00:33:11,320
"We demand an eight hour work day."

99
00:33:14,640 --> 00:33:18,560
"...Fair treatment
by the management."

100
00:33:22,680 --> 00:33:26,600
"A 30% increase in wages."

101
00:33:30,160 --> 00:33:31,520
At the police station.

102
00:34:02,603 --> 00:34:06,323
"There are no cowards
or traitors among us.

103
00:34:06,563 --> 00:34:08,163
We'll defend our demands

104
00:34:08,403 --> 00:34:11,923
to the end."

105
00:34:16,243 --> 00:34:17,523
The stockholders.

106
00:34:23,323 --> 00:34:24,523
"I'll read them."

107
00:34:39,723 --> 00:34:43,683
"A 6 hour work day for minors."

108
00:34:56,763 --> 00:35:02,923
"They're demanding
a 30% wage increase."

109
00:35:06,363 --> 00:35:11,563
"It's shocking. They've
brought the factory into politics."

110
00:35:18,807 --> 00:35:20,447
"This is impudence."

111
00:35:37,447 --> 00:35:39,727
"And now, gentlemen..."

112
00:36:10,207 --> 00:36:11,527
Police!

113
00:36:51,327 --> 00:36:52,447
Sit down!

114
00:37:05,090 --> 00:37:07,410
"Oh, that isn't all..."

115
00:37:21,850 --> 00:37:24,810
"You press down hard and
you get...juice."

116
00:37:50,450 --> 00:37:51,810
The demands.

117
00:37:54,130 --> 00:37:57,770
"The administration, having

118
00:37:58,010 --> 00:38:01,250
considered the workers' demands
with the utmost care..."

119
00:38:24,733 --> 00:38:26,093
"Clean it up."

120
00:38:47,693 --> 00:38:49,973
A nice reply.

121
00:38:53,933 --> 00:38:58,813
And late into the night they
studied the workers' welfare.

122
00:39:25,137 --> 00:39:29,497
PART FOUR
THE STRIKE DRAGS ON

123
00:39:37,097 --> 00:39:39,777
Notice
Food store closed for repairs

124
00:39:43,497 --> 00:39:44,777
"He's hungry."

125
00:40:47,297 --> 00:40:48,457
To the flea market.

126
00:42:26,423 --> 00:42:29,703
The tobacco pouch is empty.

127
00:42:47,903 --> 00:42:50,183
"Want my dinner, Daddy."

128
00:43:07,383 --> 00:43:09,503
On the prowl.

129
00:45:33,830 --> 00:45:35,790
The Owl sees by day as well.

130
00:45:41,910 --> 00:45:45,110
"Having reviewed the workers'
demands with the utmost care,

131
00:45:45,350 --> 00:45:47,470
the administration
considers them impractical

132
00:45:47,670 --> 00:45:49,630
for the following reasons:

133
00:45:49,870 --> 00:45:53,550
1. The eight hour work day
is completely illegal

134
00:45:53,790 --> 00:45:56,830
and does not depend
upon the administration.

135
00:45:57,110 --> 00:46:00,150
2) The wage increase...

136
00:46:55,073 --> 00:46:57,353
At a prearranged place.

137
00:47:12,753 --> 00:47:16,393
A civil manner from the
administration is recommended

138
00:47:16,673 --> 00:47:20,033
only if the workers
submit unconditionally.

139
00:47:29,713 --> 00:47:31,033
In the evening.

140
00:47:58,077 --> 00:47:59,157
In the night.

141
00:49:20,197 --> 00:49:21,077
"Beat him!"

142
00:50:27,920 --> 00:50:28,800
"Beat him!"

143
00:50:39,403 --> 00:50:41,363
The next night.

144
00:51:25,923 --> 00:51:30,523
"Article 102 : 4 to 6 years
of hard labor... or..."

145
00:52:03,767 --> 00:52:04,847
Towards morning.

146
00:52:32,407 --> 00:52:34,087
"They've brought him around."

147
00:52:42,887 --> 00:52:45,207
"The leader.. and dangerous.".

148
00:52:57,287 --> 00:52:59,567
They are discussing the refusal.

149
00:53:10,727 --> 00:53:14,007
"Who is for continuing the strike?"

150
00:53:17,807 --> 00:53:19,087
"Against?"

151
00:53:21,327 --> 00:53:24,607
"A minority for work."

152
00:53:40,810 --> 00:53:41,930
(An arrest warrant)

153
00:53:57,250 --> 00:54:00,610
PART FIVE
THE PROVOCATION TO DISASTER

154
00:54:15,610 --> 00:54:18,570
The Czarist police are not squeamish.

155
00:54:21,010 --> 00:54:23,090
"I'll announce you immediately."

156
00:54:44,410 --> 00:54:45,490
"The King"

157
00:55:53,373 --> 00:55:55,333
A shady deal.

158
00:56:29,497 --> 00:56:32,737
"My realms are limitless.".

159
00:56:35,257 --> 00:56:37,577
The Kadushkino Cemetery.

160
00:56:56,377 --> 00:56:57,257
Tough sorts.

161
00:57:04,097 --> 00:57:07,377
"I need five
unscrupulous men."

162
00:57:09,377 --> 00:57:11,177
They're ALL without conscience!

163
00:58:05,900 --> 00:58:06,860
Down to "business".

164
00:58:17,860 --> 00:58:22,420
The Monkey works in wholesale,
the Owl in retail.

165
00:59:30,663 --> 00:59:32,263
Pass it on.

166
00:59:43,503 --> 00:59:45,103
The war council.

167
00:59:47,143 --> 00:59:47,943
Liquor store.

168
01:00:02,063 --> 01:00:03,183
From a meeting.

169
01:00:04,823 --> 01:00:05,863
At "work".

170
01:00:45,867 --> 01:00:47,387
Worming their way in...

171
01:01:04,467 --> 01:01:05,347
SMASH IT!

172
01:01:18,707 --> 01:01:21,667
The "King's" boys go into action.

173
01:01:40,667 --> 01:01:43,427
Press the button.
Wait for the firemen.

174
01:02:03,990 --> 01:02:07,270
"It's provocation.
Call out the firemen."

175
01:02:14,590 --> 01:02:20,750
Stop gossiping and guard the fire
alarm or they will foil the plan.

176
01:02:56,150 --> 01:02:58,430
"Comrades, follow me!"

177
01:03:10,473 --> 01:03:13,753
"They want to buy us
with vodka!

178
01:03:14,033 --> 01:03:16,393
Don't be provoked!"

179
01:03:34,873 --> 01:03:38,913
"Provocateurs are here
working with the police."

180
01:03:39,193 --> 01:03:42,913
"Everybody go home,
Comrades!"

181
01:04:16,353 --> 01:04:17,593
It's been foiled...

182
01:04:39,557 --> 01:04:40,877
"Follow me!"

183
01:05:17,317 --> 01:05:19,037
"Fools!"

184
01:05:43,317 --> 01:05:44,637
A dead end.

185
01:05:58,597 --> 01:06:00,597
Struggling for a way out.

186
01:06:10,080 --> 01:06:11,600
They have broken through.

187
01:06:35,080 --> 01:06:36,440
"He got away."

188
01:06:55,040 --> 01:06:57,000
"Flush out" the ringleaders!

189
01:07:07,800 --> 01:07:09,720
"You scum! Your own people..."

190
01:07:39,123 --> 01:07:40,283
They've caught him.

191
01:07:59,603 --> 01:08:02,363
PART SIX
LIQUIDATION

192
01:08:09,803 --> 01:08:11,923
In the factory district

193
01:08:12,163 --> 01:08:16,363
a State Liquor Store was smashed,
looted and burned by the strikers.

194
01:08:16,603 --> 01:08:19,923
The crowd was dispersed
with water hoses.

195
01:08:20,163 --> 01:08:24,243
The situation is tense.
There is fear of further unrest.

196
01:08:24,483 --> 01:08:28,283
Troops have been sent to the
district by order of the Governor.

197
01:08:36,807 --> 01:08:38,487
The First Strike

198
01:08:50,207 --> 01:08:52,207
"DIS-PER-SE!"

199
01:09:40,487 --> 01:09:43,447
"Comrades, help!"

200
01:10:45,050 --> 01:10:48,330
"To the forge, comrades,
to the sledge hammers."

201
01:12:00,093 --> 01:12:01,773
"Go to your apartments."

202
01:13:17,817 --> 01:13:19,177
They have broken in.

203
01:14:38,660 --> 01:14:40,140
Wild animals.

204
01:15:01,940 --> 01:15:05,860
"You can't put them all in prison.
Our boys will hold out.".

205
01:15:13,340 --> 01:15:14,460
"The boys..."

206
01:15:26,060 --> 01:15:28,180
"Your little district..."

207
01:15:29,500 --> 01:15:30,460
"...is already..."

208
01:15:41,983 --> 01:15:45,183
"But you, young man, could maybe
join us... we could help you.

209
01:15:45,423 --> 01:15:48,183
What do you say?"

210
01:16:09,663 --> 01:16:11,103
"Back to my cell!"

211
01:16:16,183 --> 01:16:17,263
The carnage.

212
01:17:05,067 --> 01:17:06,387
The defeat.

213
01:17:47,587 --> 01:17:51,667
And like bloody unforgettable scars
on the body of the proletariat

214
01:17:51,907 --> 01:17:56,347
lay the wounds of Lena,
Talka, Zlataust, Yaro,

215
01:17:56,587 --> 01:18:01,707
Slavl, Tsaritsin and Kosteroma.

216
01:18:02,347 --> 01:18:03,227
REMEMBER!

217
01:18:03,467 --> 01:18:05,187
PROLETARIANS!