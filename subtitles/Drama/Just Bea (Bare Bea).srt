﻿1
00:00:00,266 --> 00:00:02,701
♪ Da, da, da-ba-da, da, da,
ba-da-ba-da-ba ♪

2
00:00:02,736 --> 00:00:04,216
♪ da, da, da-ba-da, da, da ♪
let's go!

3
00:00:04,270 --> 00:00:06,138
♪ We'll be there ♪

4
00:00:06,172 --> 00:00:08,574
♪ a wink and a smile
and a great, old time ♪

5
00:00:08,641 --> 00:00:11,010
♪ yeah, we'll be there ♪

6
00:00:11,044 --> 00:00:13,078
♪ wherever we are,
there's fun to be found ♪

7
00:00:13,113 --> 00:00:15,180
♪ we'll be there
when you turn that corner ♪

8
00:00:15,215 --> 00:00:16,281
♪ when you jump out the bush ♪

9
00:00:16,316 --> 00:00:18,283
♪ with a big bear smile ♪

10
00:00:18,318 --> 00:00:19,451
♪ we'll be there ♪

11
00:00:25,358 --> 00:00:26,558
Okay, here it is.

12
00:00:26,593 --> 00:00:28,160
The date is June 24th.

13
00:00:29,529 --> 00:00:32,564
I've found
what I believe to be a cave.

14
00:00:32,599 --> 00:00:36,035
Oh.
This furniture looks handmade.

15
00:00:36,069 --> 00:00:38,103
Aw, dang.
Oh. There.

16
00:00:38,171 --> 00:00:39,505
Nobody will notice.

17
00:00:39,539 --> 00:00:41,206
Hmm. Porridge maybe.

18
00:00:42,642 --> 00:00:43,809
Yep, that's porridge.

19
00:00:43,843 --> 00:00:45,711
Oh, hey, I've read that... oh!

20
00:00:45,745 --> 00:00:48,280
We were all freaked out,
you know? I can't really...

21
00:00:48,314 --> 00:00:50,182
Hey, what the...
My chair!

22
00:00:50,216 --> 00:00:52,317
My porridge! Who are you?
How'd you get in here?

23
00:00:52,385 --> 00:00:55,020
Stay back! Aah!

24
00:00:55,055 --> 00:00:57,056
All right, all right.
Turn on the light. Okay!

25
00:00:57,123 --> 00:00:58,657
Ow!

26
00:00:58,691 --> 00:01:00,492
All right, we got you,
you little rascal.

27
00:01:00,527 --> 00:01:01,794
What are you doing in our cave?

28
00:01:01,861 --> 00:01:03,562
Oh, wow.
So, this is your cave?

29
00:01:03,596 --> 00:01:04,663
Perfect!
Mind games, eh?

30
00:01:04,697 --> 00:01:06,465
I know your type.

31
00:01:06,499 --> 00:01:09,034
Now tell us why you were filming
in here and touching our...

32
00:01:09,069 --> 00:01:10,702
So it's true
what the scientists say...

33
00:01:10,737 --> 00:01:12,671
Your cavities are atrocious.

34
00:01:12,705 --> 00:01:15,207
Would you say that
your poor dental hygiene

35
00:01:15,241 --> 00:01:17,109
contributes
to your surly nature?

36
00:01:17,177 --> 00:01:18,677
I don't know
what "surly," means,

37
00:01:18,711 --> 00:01:20,145
but I'm gonna pretend
it means "radical."

38
00:01:20,180 --> 00:01:21,413
So thank you.

39
00:01:21,448 --> 00:01:23,182
But that wasn't
the answer we need.

40
00:01:23,216 --> 00:01:26,151
Wah! Oh, no!

41
00:01:26,186 --> 00:01:28,320
Sorry. I can't find it. It's too
dark in here. Maybe it's over...

42
00:01:28,354 --> 00:01:29,621
Aah!
Ohh! So bright!

43
00:01:29,656 --> 00:01:31,090
How are we supposed
to be cool with you

44
00:01:31,124 --> 00:01:32,224
throwing
our mood lighting around?

45
00:01:32,292 --> 00:01:33,452
I'm sorry!
It was an accident!

46
00:01:33,460 --> 00:01:34,726
My paws are sweaty!
Hey, now.

47
00:01:34,761 --> 00:01:36,195
What are you writing there?
Let me see.

48
00:01:36,229 --> 00:01:37,463
Highly suspicious, I'd say.

49
00:01:37,497 --> 00:01:39,231
Yeah,
this is super-invasive.

50
00:01:39,265 --> 00:01:40,599
Ice bear has too many secrets.

51
00:01:40,633 --> 00:01:42,501
It isn't invasive.
Really.

52
00:01:42,535 --> 00:01:44,203
I'm doing research
on local bears.

53
00:01:44,237 --> 00:01:45,637
It's an assignment for my class.

54
00:01:45,672 --> 00:01:47,306
Class for what...
Kindergarten?

55
00:01:47,340 --> 00:01:49,675
Up top.

56
00:01:49,709 --> 00:01:51,310
Actually, it's a college course.

57
00:01:51,344 --> 00:01:52,578
I skipped a couple of grades.

58
00:01:52,612 --> 00:01:53,879
So the joke's on you.

59
00:01:53,913 --> 00:01:56,381
Well, the proof...
Is in the pudding.

60
00:01:56,416 --> 00:01:57,850
We got your backpack.

61
00:01:57,884 --> 00:01:59,318
I'm gonna mess with your stuff

62
00:01:59,352 --> 00:02:00,853
and get answers
at the same time.

63
00:02:00,920 --> 00:02:03,155
Oh-ho!
What's all this?

64
00:02:03,189 --> 00:02:04,823
I'm eating your stuff!

65
00:02:04,858 --> 00:02:07,192
How's it feel?

66
00:02:07,227 --> 00:02:08,861
Whoa.

67
00:02:08,928 --> 00:02:11,196
Aww. Is this you?

68
00:02:11,231 --> 00:02:12,464
"Chlu..."
"Cha-low-ee."

69
00:02:12,499 --> 00:02:14,133
"Chloh..."
"Ch..."

70
00:02:14,167 --> 00:02:15,801
Ice bear has
never seen a smaller human.

71
00:02:15,869 --> 00:02:17,569
Stop! Give me that!

72
00:02:17,604 --> 00:02:18,837
And spit those out!
Gah!

73
00:02:18,872 --> 00:02:20,305
You're not even
eating them right.

74
00:02:20,340 --> 00:02:21,640
Here you go.

75
00:02:21,708 --> 00:02:24,476
They're a little spicy.

76
00:02:25,945 --> 00:02:29,581
Oh, my gosh, you guys.
These are so good.

77
00:02:29,616 --> 00:02:31,350
Can... can we
have another?

78
00:02:31,352 --> 00:02:32,551
Sure!
Whoo!

79
00:02:32,585 --> 00:02:34,786
Actually, I vecan we
opior gu. Have another?

80
00:02:34,821 --> 00:02:36,655
Would you mind ii around for a t

81
00:02:36,689 --> 00:02:38,423
to study your behavior?

82
00:02:38,458 --> 00:02:40,492
I'll bring
you as many gummies as you want.

83
00:02:40,527 --> 00:02:42,327
Deal!

84
00:02:47,433 --> 00:02:48,700
Should I be doing something?

85
00:02:48,735 --> 00:02:52,171
Nope. Just be normal.

86
00:02:52,205 --> 00:02:54,206
♪ Hey, hey, look to the sky ♪

87
00:02:54,274 --> 00:02:55,807
♪ the sun is bright ♪

88
00:02:55,842 --> 00:02:58,343
♪ there's not a cloud in view ♪

89
00:02:58,378 --> 00:03:02,548
♪ not a cloud in view ♪

90
00:03:02,582 --> 00:03:05,217
♪ I'm thinking we make
the most of the sunshine ♪

91
00:03:06,586 --> 00:03:09,121
♪ "What do you say?" Says me ♪

92
00:03:09,155 --> 00:03:11,723
♪ ooh, doo-doo, doo-doo ♪

93
00:03:11,791 --> 00:03:15,460
♪ don't want a push ♪

94
00:03:15,495 --> 00:03:18,063
♪ no gloom in these streets ♪

95
00:03:18,097 --> 00:03:20,632
♪ I just saw
a guy with purple hat ♪

96
00:03:20,667 --> 00:03:25,637
♪ And the elements
will be like a bind ♪

97
00:03:25,672 --> 00:03:27,506
♪ yo, rain cloud, chill ♪

98
00:03:29,108 --> 00:03:31,743
♪ Stay away ♪

99
00:03:31,778 --> 00:03:36,481
♪ not in the mood
for your fluff, okay? ♪

100
00:03:36,516 --> 00:03:39,151
♪ Come back in two weeks ♪

101
00:03:39,185 --> 00:03:42,087
♪ or two years or maybe never ♪

102
00:03:42,121 --> 00:03:46,358
♪ 'Cause I just want it bright
for now, forever ♪

103
00:03:46,426 --> 00:03:48,660
♪ ahh, whoo-hoo ♪

104
00:03:49,896 --> 00:03:52,097
♪ Doo-doo-doo, doo-doo-doo ♪

105
00:03:52,131 --> 00:03:54,600
♪ doo-doo-doo, doo-doo-doo-doo ♪

106
00:03:54,634 --> 00:03:59,738
♪ doo-doo-doo, doo-doo-doo,
doo-doo ♪

107
00:03:59,772 --> 00:04:02,407
♪ doo-doo-doo, doo-doo-doo ♪

108
00:04:02,475 --> 00:04:05,444
♪ doo-doo-doo, doo-doo-doo-doo ♪

109
00:04:05,478 --> 00:04:06,918
♪ doo-doo-doo, doo-doo-doo,
doo-doo ♪

110
00:04:10,516 --> 00:04:12,351
♪ Rain cloud, chill ♪

111
00:04:12,385 --> 00:04:16,521
♪ stay away for a while ♪

112
00:04:16,556 --> 00:04:18,190
Hey, friend. How's it going?
You need anything?

113
00:04:18,224 --> 00:04:19,524
Are you almost done?
When can I see?

114
00:04:19,592 --> 00:04:20,792
Hang on.
Oh, yeah.

115
00:04:20,860 --> 00:04:22,194
Cool, cool, cool.
You got it.

116
00:04:22,228 --> 00:04:24,329
Done!

117
00:04:24,364 --> 00:04:25,364
Done! Done! Done!

118
00:04:25,431 --> 00:04:26,798
Ice bear believed in you.

119
00:04:26,833 --> 00:04:28,734
Um, now that you're done,
you wouldn't happen

120
00:04:28,768 --> 00:04:31,637
to have anything spicy for us,
would you?

121
00:04:31,671 --> 00:04:35,540
Spicy? I don't think I know
what you're talking about.

122
00:04:35,575 --> 00:04:37,442
But you... you said...

123
00:04:37,477 --> 00:04:38,777
The thing.

124
00:04:38,811 --> 00:04:40,212
Just kidding!

125
00:04:40,246 --> 00:04:41,713
I got loads!

126
00:04:41,748 --> 00:04:43,215
Go ahead!

127
00:04:44,817 --> 00:04:47,719
We worked so hard for this.

128
00:04:49,489 --> 00:04:51,189
Oh, no!
The last bus!

129
00:04:51,224 --> 00:04:52,457
I've got to go!

130
00:04:52,492 --> 00:04:54,826
Sorry for dashing!
I'll see you guys!

131
00:04:57,597 --> 00:04:58,830
Well, that was fun.

132
00:04:58,865 --> 00:05:00,899
Now time to enjoy our spoils.

133
00:05:00,967 --> 00:05:02,634
Huh? Oh, no.

134
00:05:02,669 --> 00:05:05,904
Chloe's laptop... she needs this.
Hmm.

135
00:05:05,938 --> 00:05:10,475
Well... Now that it's here,
a little peek can't hurt.

136
00:05:10,510 --> 00:05:12,144
Here it is... the assignment.

137
00:05:12,178 --> 00:05:14,446
Let's full-screen.
Next slide.

138
00:05:14,480 --> 00:05:17,783
"Large and needy beasts
of the nor..."

139
00:05:17,817 --> 00:05:20,352
wha... ugh. That's
a horrible photo.

140
00:05:20,386 --> 00:05:21,687
"Powerful stench"?

141
00:05:21,754 --> 00:05:22,921
It's an alluring musk!

142
00:05:22,955 --> 00:05:24,523
Ice bear
smells like clean babies.

143
00:05:24,557 --> 00:05:26,725
We don't make people
uncomfortable.

144
00:05:26,759 --> 00:05:28,260
Cool. It's me.

145
00:05:28,328 --> 00:05:30,228
Grizz, most of this stuff
isn't very nice.

146
00:05:30,263 --> 00:05:31,496
I mean... what?!

147
00:05:31,531 --> 00:05:33,332
My diet
is more balanced than that.

148
00:05:33,366 --> 00:05:35,167
I mean, it's just more lies!

149
00:05:35,234 --> 00:05:37,302
Ice bear
settled that out of court.

150
00:05:37,337 --> 00:05:40,939
What's this? "The bear
will do most anything for food."

151
00:05:40,973 --> 00:05:42,641
"Anything for food"?

152
00:05:42,709 --> 00:05:44,743
That's just not true.

153
00:05:47,347 --> 00:05:48,580
I don't understand.

154
00:05:48,614 --> 00:05:50,549
This is just such
a misrepresentation.

155
00:05:50,583 --> 00:05:51,817
Yeah.
That's not us at all.

156
00:05:54,253 --> 00:05:55,253
Look at this.

157
00:05:55,288 --> 00:05:56,955
"Left my laptop."

158
00:05:56,989 --> 00:05:59,891
"Could you bring it
by the college for me tomorrow?"

159
00:05:59,926 --> 00:06:01,326
Gasping face.

160
00:06:01,361 --> 00:06:03,729
Tomorrow. Huh.

161
00:06:07,600 --> 00:06:09,368
Oh, I'll bring it by tomorrow!

162
00:06:09,402 --> 00:06:11,737
"Yes," winky, winky, winky.

163
00:06:11,771 --> 00:06:13,238
This is our chance, bros,

164
00:06:13,272 --> 00:06:14,639
to let the people know
who we really are.

165
00:06:14,674 --> 00:06:16,875
Chloe won't mind
if we make a couple adjustments.

166
00:06:16,909 --> 00:06:18,543
Break out the sugary drinks.

167
00:06:18,611 --> 00:06:20,645
We have a project to write!

168
00:06:28,855 --> 00:06:32,290
Chloe.
Hey, psst! Hey, Chloe!

169
00:06:32,325 --> 00:06:33,825
We're over here.
You guys made it!

170
00:06:33,860 --> 00:06:35,994
Of course! Here's your lappy,
safe and sound.

171
00:06:36,028 --> 00:06:37,796
Awesome.
Thank you so much.

172
00:06:37,830 --> 00:06:39,631
I got to run,
but I'll see you later.

173
00:06:39,665 --> 00:06:41,700
Later, indeed.

174
00:06:43,703 --> 00:06:45,771
Is there any more room
down there?

175
00:06:45,805 --> 00:06:47,906
All right, everyone.

176
00:06:47,974 --> 00:06:49,341
We're going to start now.

177
00:06:49,375 --> 00:06:50,942
Will our first presenter
come forward?

178
00:06:50,977 --> 00:06:52,778
Chloe? Chloe?

179
00:06:52,812 --> 00:06:54,012
Huh? Oh, yes.

180
00:06:54,046 --> 00:06:56,815
Uh...
Oh, excuse me, uh...

181
00:06:56,849 --> 00:06:58,450
Coming through.

182
00:07:09,929 --> 00:07:11,396
I'd like to introduce to you all

183
00:07:11,431 --> 00:07:13,899
the topic of my report...
Bears!

184
00:07:13,933 --> 00:07:16,301
I took the time
to locate and study

185
00:07:16,335 --> 00:07:18,837
three bear specimens of
the northern California region

186
00:07:18,871 --> 00:07:21,706
and have compiled my facts
on them to share with you today.

187
00:07:21,741 --> 00:07:23,942
Let's start with the next slide.

188
00:07:23,976 --> 00:07:27,779
<i>You thought
you knew about bears...</i>

189
00:07:27,814 --> 00:07:30,649
<i>You don't!</i>

190
00:07:30,683 --> 00:07:33,385
<i>Nothing can re-create
the majesty...</i>

191
00:07:33,419 --> 00:07:35,987
<i>of a bear overlooking his realm.</i>

192
00:07:36,022 --> 00:07:38,590
<i>More often than not,</i>

193
00:07:38,658 --> 00:07:42,661
<i>bears can be found saving
the human race from themselves.</i>

194
00:07:42,695 --> 00:07:45,430
<i>Bears are also the cleanest
of this planet's creatures.</i>

195
00:07:45,465 --> 00:07:47,999
<i>They bathe an astonishing
three times a month!</i>

196
00:07:48,034 --> 00:07:52,804
<i>Bears have evolutionized to be
masters of all kinds of combat.</i>

197
00:07:54,574 --> 00:07:56,541
<i>Bears have more than eight abs,</i>

198
00:07:56,576 --> 00:07:59,845
<i>with new abs
being discovered every day.</i>

199
00:07:59,879 --> 00:08:02,914
<i>Bears have lots of feelings,
and that's a beautiful thing.</i>

200
00:08:02,949 --> 00:08:04,449
<i>Bears are smart.</i>

201
00:08:04,517 --> 00:08:05,984
<i>Congratulations, bear.</i>

202
00:08:06,018 --> 00:08:08,820
<i>In conclusion, majestic, action,</i>

203
00:08:08,888 --> 00:08:10,956
<i>hygiene, Brazilian jiu-jitsu,</i>

204
00:08:10,990 --> 00:08:14,759
<i>fire, magic,
sensitivity, competence.</i>

205
00:08:21,467 --> 00:08:24,636
I, um... I promise that wa...
It wasn't what...

206
00:08:24,670 --> 00:08:27,038
Chloe, I think
it's very apparent to me

207
00:08:27,073 --> 00:08:28,840
and everyone else
in this classroom

208
00:08:28,875 --> 00:08:31,076
that you put no actual research
into this project.

209
00:08:31,143 --> 00:08:33,011
I really did do the research.
I...

210
00:08:33,045 --> 00:08:36,081
Nothing in your report was
based on any scientific fact.

211
00:08:36,115 --> 00:08:38,083
Why's she so angry?
That was all true.

212
00:08:38,117 --> 00:08:39,584
Pardon me, student.

213
00:08:39,619 --> 00:08:41,086
What did you think
about that abs slide?

214
00:08:41,153 --> 00:08:42,687
Very cool and accurate, right?

215
00:08:47,627 --> 00:08:51,596
I have no choice but
to issue you an incomplete.

216
00:08:51,631 --> 00:08:53,431
I thought you were
more mature than this.

217
00:08:53,466 --> 00:08:56,468
Please take your seat.

218
00:08:56,502 --> 00:08:58,003
Wait!

219
00:08:58,037 --> 00:09:00,405
Your honor, please,
we have a confession to make.

220
00:09:00,439 --> 00:09:03,808
We are not actually the handsome
rapscallions you see before you.

221
00:09:03,843 --> 00:09:07,112
We are, in fact... Bear... bea...

222
00:09:10,416 --> 00:09:12,984
Bears.

223
00:09:14,053 --> 00:09:15,554
We're Chloe's friends,

224
00:09:15,621 --> 00:09:17,489
and we... excuse me...
Are also... sorry...

225
00:09:17,523 --> 00:09:20,592
The ones who wrote that report.

226
00:09:20,626 --> 00:09:22,494
She really did do research.

227
00:09:22,528 --> 00:09:24,663
We just wanted
to make it more exciting.

228
00:09:24,730 --> 00:09:26,665
I'm really sorry.
I'm sorry, too.

229
00:09:26,732 --> 00:09:27,832
We were such fools.

230
00:09:27,867 --> 00:09:28,867
Ice bear feels shame.

231
00:09:28,935 --> 00:09:31,002
Pardon me.

232
00:09:31,037 --> 00:09:33,838
Um, now that you guys are up
there, would you mind telling me

233
00:09:33,873 --> 00:09:36,608
whether it's true that bears
have horrible dental hygiene?

234
00:09:36,642 --> 00:09:39,744
I also wanted to know...
Is there an evolutionary benefit

235
00:09:39,779 --> 00:09:41,613
for bears
having such big behinds?

236
00:09:41,681 --> 00:09:43,014
Are bears dogs?

237
00:09:43,049 --> 00:09:46,518
Um, those are
some very nice, uh,

238
00:09:46,586 --> 00:09:48,119
revealing questions
you got there.

239
00:09:48,154 --> 00:09:50,922
Uh, I don't know if we...

240
00:09:54,260 --> 00:09:55,760
All right, let's do this.

241
00:09:55,795 --> 00:09:59,030
One, we are not dogs,
"b," our butts look awesome,

242
00:09:59,065 --> 00:10:01,800
and "c," my dentist says
my teeth are getting better.

243
00:10:01,834 --> 00:10:05,737
Keep those questions coming.
We're answering all of them!

244
00:10:05,771 --> 00:10:09,808
Hey, that's pretty cool.

245
00:10:39,271 --> 00:10:40,639
Good job in there.

246
00:10:40,673 --> 00:10:42,574
Yeah, that was great.

247
00:10:44,076 --> 00:10:46,811
Hey, Chloe, we're gonna watch

248
00:10:46,846 --> 00:10:48,580
this new TV show
back at our place.

249
00:10:48,614 --> 00:10:50,649
You want to come?
I don't know, you guys.

250
00:10:50,683 --> 00:10:52,283
I have another test
to study for.

251
00:10:52,318 --> 00:10:53,852
Aw.
Oh.

252
00:10:53,886 --> 00:10:55,920
Oh, all right, then.

253
00:10:55,955 --> 00:10:57,022
Yay!

254
00:10:57,056 --> 00:10:58,256
Also, bring more gummies!

255
00:10:58,290 --> 00:10:59,891
Ice bear will share
his secrets...

256
00:10:59,925 --> 00:11:01,760
About other bears mostly.

257
00:11:01,794 --> 00:11:06,531
<i>Wah-wah wah-wah-wah wah-wah
wah-wah-wah wah-wah-wah!</i>

