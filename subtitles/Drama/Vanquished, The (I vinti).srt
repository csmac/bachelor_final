1
00:00:00,700 --> 00:00:02,660
Stop! Stop!

2
00:00:10,340 --> 00:00:12,300
Customs, go!

3
00:01:21,620 --> 00:01:23,580
- Let me through.
- No.

4
00:01:24,660 --> 00:01:26,700
- Let me through!
- No.

5
00:01:41,580 --> 00:01:43,540
Go!

6
00:04:44,540 --> 00:04:46,500
Eugenio, Eugenio.

7
00:04:46,820 --> 00:04:48,780
At your service, madam.

8
00:04:49,940 --> 00:04:53,420
Did my son say where he was
going? Did someone 'phone him?

9
00:04:53,420 --> 00:04:56,260
- Yes, Mr. Olivieri.
- Who's that?

10
00:04:56,260 --> 00:05:00,500
- I don't know, but he often 'phones.
- What's his first name?

11
00:05:00,500 --> 00:05:04,580
Calm down, it's not
the first time he's been late.

12
00:05:04,580 --> 00:05:07,980
- Such terrible things happen!
- What are you thinking of!

13
00:05:07,980 --> 00:05:12,180
Bianca, if you want we can go to the
police station, to the hospitals.

14
00:05:12,180 --> 00:05:14,820
- It's never happened before!
- It's happening today.

15
00:05:14,820 --> 00:05:17,820
Have you discovered anything
about the girl in the photo?

16
00:05:17,820 --> 00:05:23,620
No. A certain Mr. Olivieri called
him. And how do we find him?

17
00:05:23,620 --> 00:05:26,620
How come you don't know your
sors friends?

18
00:05:26,620 --> 00:05:30,380
- They're probably university friends.
- I see, I'll go to the police station.

19
00:05:30,380 --> 00:05:35,460
- I'll come with you. - No, I'll call
you later after speaking to the inspector.

20
00:07:15,100 --> 00:07:17,060
Don't you feel well?

21
00:07:27,900 --> 00:07:30,140
Do you want me to call someone?

22
00:07:51,540 --> 00:07:54,860
He isn't in hospital,
that's good news, isn't it?

23
00:07:54,940 --> 00:07:58,420
Don't worry, the inspector
thinks the same as me.

24
00:07:58,420 --> 00:08:02,700
Anyway he'll send someone
to the house. Yes, two officers.

25
00:08:03,140 --> 00:08:05,180
No, it's just a formality.

26
00:08:05,940 --> 00:08:09,780
So you can explain things better
and give them all the names.

27
00:08:09,780 --> 00:08:11,460
The girl.

28
00:08:11,460 --> 00:08:13,580
The inspector was asking me

29
00:08:14,260 --> 00:08:17,500
if Claudio had had
any disappointments.

30
00:08:18,060 --> 00:08:21,060
Yes, if he's argued
with that girl.

31
00:08:22,100 --> 00:08:24,740
No, inspector,
I said so too,

32
00:08:24,740 --> 00:08:28,500
my son wouldn't do anything
stupid for some disappointment.

33
00:08:28,500 --> 00:08:32,620
We've never met the young lady
personally, but it's just the same.

34
00:08:32,900 --> 00:08:38,580
I'll just pop into the office and
then I'll come straight home, okay.

35
00:08:39,740 --> 00:08:42,780
- Thank you for your kindness.
- Not at all.

36
00:08:42,780 --> 00:08:47,500
I'm happy to have been of help
to a friend of the Honourable Valeri.

37
00:08:48,180 --> 00:08:50,140
Guard!

38
00:10:11,420 --> 00:10:14,860
You usually wait a day or two,
he could have left Rome.

39
00:10:14,860 --> 00:10:17,500
My son would have let us know.

40
00:10:17,500 --> 00:10:20,620
Has he had any quarrels
with his father?

41
00:10:20,620 --> 00:10:23,580
My husband told the inspector,
nothing's happened.

42
00:10:23,580 --> 00:10:29,020
- Whose letters are these?
- They're from school mates, friends.

43
00:10:29,820 --> 00:10:32,900
I don't understand why
you're examining these papers.

44
00:10:32,900 --> 00:10:36,060
Claudio must have had
an accident.

45
00:10:38,100 --> 00:10:41,020
- Does your son do any deals?
- What nonsense!

46
00:10:41,020 --> 00:10:45,860
- He's just a lad, he's in the first
year of university. - Thank you, madam.

47
00:11:27,060 --> 00:11:29,020
You, come here.

48
00:11:37,740 --> 00:11:41,540
- Where were you when you heard
the shots? - In the factory.

49
00:11:41,540 --> 00:11:43,980
- Who was with you?
- The others.

50
00:11:44,340 --> 00:11:46,460
- What time was it?
- About 5 o'clock.

51
00:11:46,460 --> 00:11:48,780
- I've just come from the Valmauro's.
- Later.

52
00:11:48,780 --> 00:11:53,500
That youngster, Claudio Valmauro...
Iook what I've found.

53
00:12:27,340 --> 00:12:31,820
Play something more pathetic,
Marina needs some atmosphere.

54
00:12:32,220 --> 00:12:36,260
Guys, let's telephone Claudio,
otherwise we'll be ruined.

55
00:12:38,380 --> 00:12:42,460
- Fools!
- Why, isn't it true?

56
00:12:42,460 --> 00:12:44,700
How funny you are!

57
00:12:45,500 --> 00:12:48,820
- But really, why hasn't he come?
- He'll have found some blonde.

58
00:12:48,820 --> 00:12:52,140
Worse for him if he doesn't come.
I don't care.

59
00:12:55,700 --> 00:12:59,460
- What's the matter? - If you
want me to console you, I'm here.

60
00:13:00,060 --> 00:13:03,900
- Here he is, calm down.
- He's playing the existentialist!

61
00:13:04,740 --> 00:13:06,700
Claudio!

62
00:13:07,140 --> 00:13:11,140
Your mother's been bombarding me with
'phone calls since this morning!

63
00:13:11,140 --> 00:13:13,100
She started at 8.

64
00:13:15,420 --> 00:13:19,460
- What did she say? - Your parents
went to the police station.

65
00:13:19,460 --> 00:13:21,500
Where were you last night?

66
00:13:22,380 --> 00:13:24,700
What's that face for?

67
00:13:24,700 --> 00:13:29,260
- Can we go outside a moment?
- You want me to leave everyone like this?

68
00:13:32,020 --> 00:13:35,020
Guys, make yourselves
at home.

69
00:13:38,220 --> 00:13:40,860
- What's the matter?
- Nothing.

70
00:13:40,860 --> 00:13:43,500
- Let's go.
- This way.

71
00:13:55,540 --> 00:13:58,100
Marina Locatelli, on the third floor.

72
00:13:58,100 --> 00:14:01,300
- Is she at home?
- Yes, she's having a party.

73
00:14:02,140 --> 00:14:04,100
I'll go with you.

74
00:14:14,700 --> 00:14:17,980
They're all excuses,
what do you suppose has happened?

75
00:14:18,540 --> 00:14:20,700
I'm getting really angry!

76
00:14:20,700 --> 00:14:24,140
Please, do me the favour
of not involving me in this.

77
00:14:24,140 --> 00:14:28,260
When your mother phoned
I told her, what a voice she had!

78
00:14:30,260 --> 00:14:33,020
Do you think I'm wrong? Say so!

79
00:14:33,460 --> 00:14:36,380
You say you come to see me, and
instead you're playing around.

80
00:14:36,380 --> 00:14:39,940
You're looking bad,
and you're drunk as well.

81
00:14:39,940 --> 00:14:43,100
- Stop it, Marina.
- You stop it! I'm fed up!

82
00:14:43,100 --> 00:14:46,580
I don't care if you've got others
girls! Do what you want!

83
00:14:46,580 --> 00:14:49,020
But without a lot of drama
or pathetic scenes.

84
00:14:49,020 --> 00:14:51,860
- You should say things to my face.
- Please, Marina!

85
00:14:54,340 --> 00:14:56,300
There isn't anyone else.

86
00:14:57,980 --> 00:14:59,940
No one.

87
00:15:12,900 --> 00:15:15,620
I didn't think I cared
so much about you.

88
00:15:15,620 --> 00:15:19,900
I didn't betray you, I'd have told you.
But there's one thing you don't know.

89
00:15:19,900 --> 00:15:24,860
- Just one. - One? I don't know anything
about you, not even if you care about me.

90
00:15:24,860 --> 00:15:26,700
It's the first time
you've ever told me.

91
00:15:26,700 --> 00:15:31,660
I must go away. It'd be nice
to go away together.

92
00:15:31,660 --> 00:15:33,660
To see far away places,

93
00:15:33,660 --> 00:15:37,980
leave when and if you want
and return when you like.

94
00:15:39,140 --> 00:15:43,460
Life would be great like that.
It's the only way to be happy.

95
00:15:44,180 --> 00:15:47,300
You need money, lots and immediately.

96
00:15:49,100 --> 00:15:54,980
Because I want to live life
while I'm young. At 20!

97
00:15:56,420 --> 00:15:58,820
Not when I'm old.

98
00:15:59,340 --> 00:16:02,500
- Do you understand?
- What are you saying?

99
00:16:04,420 --> 00:16:06,500
What's the matter with you?

100
00:16:07,140 --> 00:16:10,580
Have you argued with your parents?
It happens, you'll make it up.

101
00:16:10,580 --> 00:16:12,940
They give you everything you want.

102
00:16:12,940 --> 00:16:17,020
Do you think my father gives me
the money I've got?

103
00:16:17,020 --> 00:16:21,580
I earn it myself!
It's not difficult, you know.

104
00:16:21,580 --> 00:16:23,540
I was "doing well".

105
00:16:25,820 --> 00:16:27,780
Very well!

106
00:16:28,220 --> 00:16:32,460
Another few months and I'd have
asked you to come with me.

107
00:16:32,460 --> 00:16:37,820
I want to save myself anyway, you see?
I don't feel guilty!

108
00:16:38,500 --> 00:16:42,060
Nobody has the right to
take my freedom away!

109
00:16:42,060 --> 00:16:44,500
Should I give myself up?

110
00:16:48,540 --> 00:16:51,100
Spend years in prison.

111
00:16:52,540 --> 00:16:56,660
Why give everything up?
I'd never be resigned to it.

112
00:16:56,660 --> 00:17:00,020
It drives me mad just
thinking about it.

113
00:17:00,020 --> 00:17:05,100
Give up my youth!
I've done everything to enjoy it!

114
00:17:05,260 --> 00:17:08,300
I still think I've
got this right.

115
00:17:08,300 --> 00:17:11,140
Even now that I've killed a man!

116
00:17:12,900 --> 00:17:16,660
Yes, Marina, I've killed a man.

117
00:17:18,500 --> 00:17:21,820
Are you mad?
What are you talking about?

118
00:17:25,060 --> 00:17:28,660
Let's go away, maybe we'll never
see each other again.

119
00:17:28,660 --> 00:17:30,620
Claudio!

120
00:17:30,980 --> 00:17:34,700
I'm sorry. Do me a favour,
let's get away from here.

121
00:17:36,740 --> 00:17:41,740
You won't be compromised.
You know nothing, understand?

122
00:17:42,740 --> 00:17:44,700
You don't know anything.

123
00:18:18,220 --> 00:18:20,180
It's not true, Claudio.

124
00:18:21,220 --> 00:18:23,180
It's not possible.

125
00:18:30,660 --> 00:18:35,180
It's true, Marina. I shouldn't
have told you anything.

126
00:18:35,820 --> 00:18:40,140
But I needed to tell someone.
I don't feel any remorse, you know?

127
00:18:40,700 --> 00:18:43,340
It's as if it hadrt happened to me.

128
00:18:43,340 --> 00:18:46,180
If only I didn't feel so ill!

129
00:18:50,500 --> 00:18:52,860
It happened when I fell.

130
00:18:54,180 --> 00:18:56,620
What's the matter,
what's happened to me?

131
00:19:03,940 --> 00:19:07,300
- How do you feel?
- I don't know, I feel ill.

132
00:19:10,140 --> 00:19:12,100
Tell me how you feel.

133
00:19:18,500 --> 00:19:22,740
It could be serious,
an internal wound.

134
00:19:29,100 --> 00:19:31,060
Claudio.

135
00:19:34,740 --> 00:19:36,700
Let's go to the doctor.

136
00:19:39,700 --> 00:19:41,740
What does it matter now?

137
00:19:43,420 --> 00:19:46,700
I'll take you to my doctor,
don't be afraid.

138
00:20:06,900 --> 00:20:10,500
- Where are you going? - Dr. Bonanni.
- Go to the emergency ward.

139
00:20:24,660 --> 00:20:27,500
Wait a minute, I'll be right back.

140
00:20:37,460 --> 00:20:41,500
- Can I help? - Dr. Bonanni,
he should be on the emergency ward.

141
00:20:41,940 --> 00:20:45,100
- Who are you?
- Tell him its Miss Marina.

142
00:20:49,100 --> 00:20:55,140
Tell Dr. Bonanni that
Miss Marina is waiting to see him.

143
00:20:55,140 --> 00:20:57,580
- He'll be right here.
- Thank you.

144
00:22:36,340 --> 00:22:38,300
It's him.

145
00:22:51,500 --> 00:22:54,940
- Madam, your son has come home.
- Where is he? - In his room.

146
00:22:54,940 --> 00:22:59,660
How is he?
Claudio! He's come back!

147
00:22:59,660 --> 00:23:02,660
- Please, Bianca,
let me. - No, me!

148
00:23:02,660 --> 00:23:05,980
Calm down, this is something
I must deal with.

149
00:23:06,780 --> 00:23:08,940
Good evening, inspector.

150
00:23:10,020 --> 00:23:15,300
My sors come home. I'm sorry
to have inconvenienced you.

151
00:23:15,300 --> 00:23:19,380
I must ask your son to come
with us to the police station.

152
00:23:19,980 --> 00:23:21,940
To the police station?

153
00:23:25,420 --> 00:23:28,300
Call a doctor, quick!

154
00:23:38,580 --> 00:23:40,860
"Commendatore",
what's the doctor's number?

155
00:23:40,860 --> 00:23:45,540
It's unbelievable,
you bring up a son day by day,

156
00:23:45,540 --> 00:23:47,420
he's close to you,

157
00:23:47,420 --> 00:23:51,460
and suddenly you find yourself
face to face with a stranger, a rogue!

158
00:23:51,460 --> 00:23:53,860
Giulio!

159
00:23:56,180 --> 00:23:58,140
Giulio!

160
00:24:06,500 --> 00:24:08,460
What have they done to you?

161
00:24:09,660 --> 00:24:11,620
What have they done to you?

162
00:24:12,100 --> 00:24:14,980
Claudio, what have they done to you?

163
00:24:15,060 --> 00:24:20,300
Claudio, what have they done to you?

164
01:00:33,860 --> 01:00:37,980
If only one of our children,
listening to our three stories,

165
01:00:37,980 --> 01:00:39,780
feels healed
of the morbid fascination

166
01:00:39,780 --> 01:00:43,060
which cultivates within him
too many false images,

167
01:00:43,060 --> 01:00:45,980
if anyone, perceiving the unveiled
face of egoism,

168
01:00:45,980 --> 01:00:49,380
feels faith in human solidarity
reviving within him,

169
01:00:49,380 --> 01:00:53,140
our work
won't have been in vain.

