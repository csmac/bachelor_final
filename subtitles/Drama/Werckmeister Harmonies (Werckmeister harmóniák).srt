1
00:01:51,208 --> 00:01:52,482
hey kid!

2
00:01:52,688 --> 00:01:54,280
what are you doing here?

3
00:01:57,168 --> 00:01:58,567
who are you?

4
00:01:58,968 --> 00:02:00,447
j�nos Valuska.

5
00:02:07,288 --> 00:02:09,040
what do you want here?

6
00:02:10,808 --> 00:02:12,082
I'm...

7
00:02:13,808 --> 00:02:16,322
I'm on an errand, I'm the...

8
00:02:17,728 --> 00:02:19,320
Mr Eszter told me to...

9
00:02:19,608 --> 00:02:21,485
- Mr Eszter...
- Aha.

10
00:02:22,168 --> 00:02:24,602
...who on Auntie T�nde's request...

11
00:02:26,688 --> 00:02:28,406
- Drink this.
- just that...

12
00:02:30,008 --> 00:02:30,997
Let's hear it.

13
00:02:31,208 --> 00:02:35,167
he was requested to go
and Auntie T�nde when she arrives...

14
00:02:35,368 --> 00:02:38,997
Your attention please!
Our show is over for today.

15
00:02:40,008 --> 00:02:41,441
For technical reasons

16
00:02:41,648 --> 00:02:44,446
the appearance of the Prince
has been cancelled.

17
00:02:45,568 --> 00:02:49,402
All the best then until our ticket
office opens tomorrow morning.

18
00:02:49,608 --> 00:02:51,519
I thank you for your attention,

19
00:02:52,048 --> 00:02:54,801
and commend our company again to you.

20
00:02:55,368 --> 00:02:57,165
So, please go home.

21
00:03:01,088 --> 00:03:02,077
Go!

22
00:04:59,728 --> 00:05:02,162
Filthy gang! No class!

23
00:05:02,888 --> 00:05:04,241
Swine!

24
00:05:07,768 --> 00:05:10,407
Dirty swine! Rabble!

25
00:05:11,248 --> 00:05:12,601
Rogues!

26
00:05:14,208 --> 00:05:15,846
Good evening Auntie T�nde.

27
00:05:20,328 --> 00:05:22,558
Uncle Gyuri sent me
to tell Auntie T�nde

28
00:05:22,768 --> 00:05:25,328
that all that you asked
has been arranged,

29
00:05:25,528 --> 00:05:28,486
and the organising of the whole
thing is under way,

30
00:05:28,728 --> 00:05:32,323
so I'm to bring your case here
this evening, right away.

31
00:05:32,608 --> 00:05:34,883
That doesn't concern us now, j�nos.

32
00:05:37,768 --> 00:05:40,282
Are they ripping the square apart?

33
00:05:40,528 --> 00:05:42,120
Or setting it on fire?

34
00:05:43,128 --> 00:05:46,677
The whale, the great sensation
of the century has arrived,

35
00:05:46,888 --> 00:05:49,004
a gigantic, wonderful whale, and...

36
00:05:49,208 --> 00:05:50,436
wait a moment.

37
00:05:50,968 --> 00:05:54,677
The whole squad advances! Attack!

38
00:05:56,528 --> 00:05:57,802
Bastards!

39
00:06:00,168 --> 00:06:04,366
Show no mercy!
Shoot right into them.

40
00:06:04,568 --> 00:06:07,002
I've called for the tanks.

41
00:06:10,608 --> 00:06:11,723
Don't!

42
00:06:12,928 --> 00:06:14,202
Don't.

43
00:06:14,688 --> 00:06:17,566
- what are you trying to do?
- Lie down and go to sleep.

44
00:06:17,808 --> 00:06:20,003
Stop it! Get away from me!

45
00:06:20,768 --> 00:06:22,998
No time for sleeping, resting!

46
00:06:23,688 --> 00:06:26,805
- Out of the question!
- That's enough! Go and lie down!

47
00:06:27,008 --> 00:06:28,282
Nowwait!

48
00:06:28,768 --> 00:06:29,917
just wait!

49
00:06:30,128 --> 00:06:32,358
Pull yourself together,
act like a man!

50
00:06:32,568 --> 00:06:33,603
Stay!

51
00:06:34,568 --> 00:06:36,206
Stay. Don't go!

52
00:06:39,648 --> 00:06:40,797
T�nde!

53
00:06:47,488 --> 00:06:49,922
I have something important
I want you to do.

54
00:06:50,128 --> 00:06:54,201
Go to the police chief's home
and put his children to bed.

55
00:06:54,528 --> 00:06:56,598
Tell them he's going to be late,

56
00:06:56,808 --> 00:07:00,721
then go to the main square,
take a look around,

57
00:07:01,648 --> 00:07:05,561
observe how many there are,
who is talking to who and what about,

58
00:07:05,808 --> 00:07:08,276
then come back here
and tell us everything.

59
00:07:08,488 --> 00:07:12,197
Delighted to oblige, Auntie T�nde.
I'll go and take a look around.

60
00:07:12,448 --> 00:07:15,008
I'll gladly observe
and tell you everything,

61
00:07:15,248 --> 00:07:17,523
who is talking to who, then.

62
00:07:18,248 --> 00:07:20,159
I've just come from there,

63
00:07:20,368 --> 00:07:24,122
I'd like to find out
just what this strange secret is.

64
00:07:24,328 --> 00:07:26,046
- Off you go.
- Goodbye.

65
00:07:52,928 --> 00:07:58,082
Now my little witch,
do you knowwho wrote this piece?

66
00:08:02,608 --> 00:08:03,961
Do you like it?

67
00:08:05,208 --> 00:08:06,277
just dance!

68
00:08:18,128 --> 00:08:19,163
just dance!

69
00:08:38,288 --> 00:08:39,357
Do you like it?

70
00:09:43,808 --> 00:09:45,924
Off to bed with you!

71
00:09:46,768 --> 00:09:49,521
- I don't want to go to bed!
- Off to bed!

72
00:09:50,528 --> 00:09:52,200
I'm not going to bed!

73
00:09:53,528 --> 00:09:55,280
- Go and get into bed!
- No!

74
00:10:07,328 --> 00:10:09,159
That was just a pat.

75
00:10:16,888 --> 00:10:18,844
I didn't really hit you.

76
00:10:27,648 --> 00:10:29,400
I'll be hard on you.

77
00:10:40,048 --> 00:10:41,800
I'll be hard on you.

78
00:10:48,528 --> 00:10:50,246
Go to bed both of you!

79
00:10:50,568 --> 00:10:51,967
I don't want to.

80
00:11:26,008 --> 00:11:27,600
I'll be hard on you.

81
00:11:30,368 --> 00:11:32,199
I'll be hard on you.

82
00:11:39,288 --> 00:11:40,926
I'll be hard on you.

83
00:11:42,968 --> 00:11:46,881
I'll be hard on you.
I'll be hard on you.

84
00:11:48,168 --> 00:11:49,920
I'll be hard on you.

85
00:11:53,008 --> 00:11:56,478
I'll be hard on you.
I'll be hard on you.

86
00:11:58,488 --> 00:12:01,446
I'll be hard on you.
I'll be hard on you.

87
00:12:05,048 --> 00:12:07,039
I'll be hard on you.

88
00:12:15,048 --> 00:12:18,404
I'll be hard on you.
I'll be hard on you.

89
00:12:48,648 --> 00:12:50,445
Good evening Uncle Lajos.

90
00:12:50,648 --> 00:12:53,640
how's it going...
what are you doing here j�nos?

91
00:12:54,528 --> 00:12:55,881
Auntie T�nde sent me.

92
00:12:56,688 --> 00:12:59,327
She said to go to the market square,

93
00:12:59,848 --> 00:13:02,840
and take a look around
to see what was happening

94
00:13:03,088 --> 00:13:07,081
and tell her about everything, who's
doing what, who's talking to who.

95
00:13:07,288 --> 00:13:10,360
Don't go there j�nos,
this is not for us...

96
00:13:11,368 --> 00:13:13,643
They'll have to get the army in.

97
00:13:14,128 --> 00:13:16,084
There's nothing we can do now.

98
00:13:18,408 --> 00:13:19,636
I'll watch out.

99
00:13:20,848 --> 00:13:22,759
Be careful, j�nos.

100
00:13:24,088 --> 00:13:25,885
Goodbye, Uncle Lajos.

101
00:14:00,128 --> 00:14:01,447
Good evening.

102
00:16:00,568 --> 00:16:01,717
Sorry.

103
00:16:03,848 --> 00:16:05,201
The whale...

104
00:16:06,048 --> 00:16:08,243
Can't I see the whale now?

105
00:16:09,648 --> 00:16:10,922
Or in the morning?

106
00:16:12,328 --> 00:16:13,920
Fuck off out of here.

107
00:17:44,448 --> 00:17:46,564
See how much trouble you've caused!

108
00:17:49,368 --> 00:17:52,963
Even though you haven't been able
to harm anyone for a long time.

109
00:17:59,328 --> 00:18:02,684
...and not from the whole,
as the Director imagines.

110
00:18:03,248 --> 00:18:06,797
I don't imagine anything.
But I know

111
00:18:07,008 --> 00:18:10,045
that if he doesn't calm them,
but fires them up,

112
00:18:10,288 --> 00:18:13,485
then the whole thing is going
to start all over again!

113
00:18:13,848 --> 00:18:17,966
And tell him this too,
that I take his impudence badly.

114
00:18:18,728 --> 00:18:20,161
- Translate that!
- Why?!

115
00:18:20,368 --> 00:18:23,838
So that it'll get through
that infantile skull of his

116
00:18:24,048 --> 00:18:27,324
that he cannot go out, that you
aren't going to take him out,

117
00:18:27,528 --> 00:18:30,964
you're not going to pick him up
and translate for him.

118
00:18:31,168 --> 00:18:32,567
But he sticks to me!

119
00:18:33,408 --> 00:18:36,684
I won't stand for his stirring up
the rabble with his lies.

120
00:18:37,128 --> 00:18:40,120
T ell him that we have finished now
once and for all.

121
00:18:40,328 --> 00:18:43,400
he never wished to go anywhere.
The Director takes him.

122
00:18:43,608 --> 00:18:49,046
I engaged him to show himself,
and not to stir and incite.

123
00:18:49,448 --> 00:18:52,520
I won't let him out
and let him speak. Translate that.

124
00:18:52,768 --> 00:18:54,087
No point.

125
00:18:54,288 --> 00:18:56,848
No one can stop him
whatever they do.

126
00:18:57,208 --> 00:19:01,440
I won't stand for his tearing
the town apart with these bandits.

127
00:19:01,648 --> 00:19:05,004
I am not willing to risk
the good name of the company.

128
00:19:05,968 --> 00:19:08,163
The last time
was positively the last.

129
00:19:08,768 --> 00:19:09,883
No way!

130
00:19:10,408 --> 00:19:11,363
Ridiculous!

131
00:19:11,568 --> 00:19:15,402
he doesn't recognise any superior
authority. Nothing of that kind.

132
00:19:15,608 --> 00:19:18,725
In the eyes of his followers
out there, he is the Prince.

133
00:19:18,968 --> 00:19:22,005
No ordinary force can hold him,
he has magnetic power!

134
00:19:22,208 --> 00:19:25,962
his magnetic power is a
disfigurement! he is an aberration!

135
00:19:26,168 --> 00:19:28,762
A born freak who knows nothing
about anything!

136
00:19:28,968 --> 00:19:32,404
The title of Prince I bestowed on him
for commercial reasons.

137
00:19:32,608 --> 00:19:35,441
Tell him that I invented him.

138
00:19:36,568 --> 00:19:38,524
his followers are waiting outside!

139
00:19:38,728 --> 00:19:41,481
They are losing patience.
To them he is the Prince!

140
00:19:41,688 --> 00:19:43,246
- Then he is fired!
- Fine!

141
00:19:43,448 --> 00:19:47,327
From now, he seeks independence.
he's taking me with him.

142
00:19:47,528 --> 00:19:49,120
- You!
- Me.

143
00:19:50,048 --> 00:19:52,801
I shall do what he wants,
because he means money.

144
00:19:53,008 --> 00:19:55,841
You are poor.
The Prince means money for you.

145
00:19:56,048 --> 00:19:57,766
will you stop poncing about!

146
00:20:03,568 --> 00:20:05,877
why don't we try
to arrange something?

147
00:20:06,288 --> 00:20:07,641
Tell him...

148
00:20:08,648 --> 00:20:11,116
that I'll let him out,
on one condition.

149
00:20:11,328 --> 00:20:14,923
That he doesn't open his mouth,
not even a single word,

150
00:20:15,128 --> 00:20:17,688
and he is to be as silent
as the grave.

151
00:20:17,888 --> 00:20:19,037
Tell him that.

152
00:20:28,768 --> 00:20:31,236
The Director cannot lay down
any rules!

153
00:20:32,248 --> 00:20:36,287
The Director gets the money,
the Prince gets his followers.

154
00:20:36,488 --> 00:20:40,322
There's no point in arguing!
The Prince alone sees the whole.

155
00:20:40,568 --> 00:20:42,524
And the whole is nothing.

156
00:20:43,528 --> 00:20:46,088
Completely in ruins.

157
00:20:46,688 --> 00:20:49,646
what they build
and what they will build,

158
00:20:49,848 --> 00:20:53,523
what they do and what they will do,
is delusion and lies.

159
00:20:53,728 --> 00:20:56,879
Under construction,
everything is only half complete.

160
00:20:57,088 --> 00:20:58,521
In ruins, all is complete.

161
00:20:58,728 --> 00:21:02,687
what they think is ridiculous.
They think because they are afraid.

162
00:21:03,008 --> 00:21:04,999
And he who is afraid knows nothing.

163
00:21:05,208 --> 00:21:07,164
The Director doesn't understand

164
00:21:07,368 --> 00:21:11,077
that his followers are not afraid
and do understand him.

165
00:21:11,368 --> 00:21:14,326
his followers are going
to make ruins of everything!

166
00:21:15,288 --> 00:21:16,767
Confused drivel!

167
00:21:18,128 --> 00:21:21,279
Tell that to the rabble
and not to me.

168
00:21:21,488 --> 00:21:25,242
I won't listen to this any more.
I'll wash my hands of him,

169
00:21:25,488 --> 00:21:28,048
I won't take responsibility
for what he does.

170
00:21:28,248 --> 00:21:32,161
You gentlemen
from this moment on are free.

171
00:21:32,848 --> 00:21:35,043
You can do what you want.

172
00:21:35,248 --> 00:21:37,125
If they destroy the towns,

173
00:21:37,328 --> 00:21:40,286
after a while, there will be
nowhere left for him to go.

174
00:21:40,568 --> 00:21:41,762
Does he know that?

175
00:21:49,168 --> 00:21:52,956
His followers do, tell him.
There is disillusion in everything.

176
00:21:53,168 --> 00:21:55,045
They don't understand why.

177
00:21:55,248 --> 00:21:57,284
But the Prince knows full well

178
00:21:57,488 --> 00:22:00,764
that it is because the whole
is nothing. They are finished.

179
00:22:00,968 --> 00:22:02,686
We crush them with our fury!

180
00:22:03,608 --> 00:22:06,645
We punish them!
We will be pitiless!

181
00:22:07,448 --> 00:22:09,006
The day has come!

182
00:22:12,248 --> 00:22:14,284
There will be nothing left!

183
00:22:15,888 --> 00:22:17,367
Fury overcomes all!

184
00:22:18,448 --> 00:22:20,245
Their silver and gold...

185
00:22:21,688 --> 00:22:23,246
cannot protect them!

186
00:22:23,968 --> 00:22:26,402
We'll take possession
of their houses!

187
00:22:26,608 --> 00:22:28,997
Terror is here! Massacre!

188
00:22:31,128 --> 00:22:33,084
Show no mercy! Slaughter!

189
00:23:35,608 --> 00:23:37,200
Arson. Slaughter!

190
00:39:12,088 --> 00:39:17,799
When the clamour died down,

191
00:39:19,208 --> 00:39:24,077
the Prince said this:

192
00:39:25,488 --> 00:39:28,685
"What they build

193
00:39:29,808 --> 00:39:36,725
"and what they will build,

194
00:39:37,448 --> 00:39:39,882
"what they do

195
00:39:41,008 --> 00:39:48,323
"and what they will do,

196
00:39:49,928 --> 00:39:56,845
"is delusion and lies.

197
00:39:58,448 --> 00:40:02,600
"What they think

198
00:40:02,848 --> 00:40:09,037
"and what they will think,

199
00:40:10,328 --> 00:40:12,683
"is ridiculous.

200
00:40:13,128 --> 00:40:20,125
"They think because they are afraid.

201
00:40:20,688 --> 00:40:29,801
"And he who is afraid,
knows nothing. "

202
00:40:30,648 --> 00:40:38,521
He says he likes it

203
00:40:39,168 --> 00:40:46,165
when things fall apart.

204
00:40:47,088 --> 00:40:49,727
"There is construction
in all ruins... "

205
00:40:49,928 --> 00:40:53,159
"A single emotion for destruction,
implacable, deadly.

206
00:40:53,368 --> 00:40:56,883
"we didn't find the real object
of our abhorrence and despair,

207
00:40:57,088 --> 00:41:01,047
"so we rushed at everything we came
across with wilder and wilder fury.

208
00:41:01,248 --> 00:41:04,604
"we destroyed the shops,
threw out and trampled everything

209
00:41:04,808 --> 00:41:07,720
"that was movable
and what we couldn't move

210
00:41:07,928 --> 00:41:12,240
"we broke up with slats
of shutters and iron bars.

211
00:41:12,448 --> 00:41:16,077
"we turned over cars in the streets,

212
00:41:16,288 --> 00:41:19,041
"tore off the miserable sign-boards,

213
00:41:19,248 --> 00:41:21,523
"destroyed the telephone centre

214
00:41:21,728 --> 00:41:23,958
"because we saw the lights inside,

215
00:41:25,248 --> 00:41:28,923
"and we had the two post office girls

216
00:41:29,128 --> 00:41:31,039
"and we left only

217
00:41:31,248 --> 00:41:35,127
"when they had fainted
and like two used rags,

218
00:41:35,408 --> 00:41:37,763
"lifeless,
hands clasped between knees,

219
00:41:37,968 --> 00:41:41,563
"hunched over,
they slipped off the bloody table..."

220
00:42:48,928 --> 00:42:50,680
- hello there.
- hello...

221
00:42:52,048 --> 00:42:53,686
- Doing OK?
- Fine.

222
00:42:53,928 --> 00:42:55,281
just a moment...

223
00:43:00,848 --> 00:43:02,361
Allow me to introduce

224
00:43:03,968 --> 00:43:05,799
Mrs Eszter.

225
00:43:07,248 --> 00:43:08,727
Privilege, ma'am.

226
00:43:19,448 --> 00:43:20,597
Ma'am.

227
00:43:21,808 --> 00:43:24,322
Is this where
you were thinking of?

228
00:43:28,288 --> 00:43:29,437
here.

229
00:43:33,408 --> 00:43:35,399
And if here?

230
00:43:37,848 --> 00:43:39,839
Possible, but not really.

231
00:43:43,128 --> 00:43:44,243
And...

232
00:43:45,208 --> 00:43:47,517
what about here?

233
00:43:54,688 --> 00:43:55,677
here!

234
00:43:59,248 --> 00:44:02,797
Then will you join us, ma'am?
In the car?

235
00:44:04,088 --> 00:44:06,363
Yes. I'm coming.

236
00:44:10,248 --> 00:44:11,727
Follow us.

237
00:45:04,448 --> 00:45:08,407
FANTASTlC:.
ThE wORLD'S LARGEST GlANT whALE!

238
00:45:08,608 --> 00:45:13,284
AND OThER wONDERS OF NATURE!
GUEST STAR: ThE PRlNCE

239
00:46:32,328 --> 00:46:34,125
Uncle Lajos...

240
00:47:53,168 --> 00:47:55,045
- how's j�nos?
- Good morning.

241
00:47:55,248 --> 00:47:57,398
You didn't see my Lajos?

242
00:47:59,928 --> 00:48:02,567
There's been no sign of him
since last night.

243
00:48:04,248 --> 00:48:06,318
I saw him in the square
last night.

244
00:48:06,528 --> 00:48:09,361
I'm sure he got mixed up
somehow in that ruction.

245
00:48:09,568 --> 00:48:11,718
he always has to be
where he shouldn't.

246
00:48:11,928 --> 00:48:15,807
he'll never have any sense. he's
always poking his nose in everywhere.

247
00:48:16,008 --> 00:48:17,680
So are you, j�nos.

248
00:48:17,888 --> 00:48:19,241
So are you.

249
00:48:19,768 --> 00:48:21,645
They're looking for you as well.

250
00:48:21,848 --> 00:48:25,079
They came here
and I saw your name on the list.

251
00:48:25,288 --> 00:48:27,802
You'll hang for it in the end, j�nos.

252
00:48:30,488 --> 00:48:33,798
- I haven't done anything.
- That means nothing to them.

253
00:48:34,008 --> 00:48:36,158
They recognise neither man nor god.

254
00:48:36,368 --> 00:48:39,041
Get away from the town
as soon as you can.

255
00:48:39,288 --> 00:48:42,724
head for the rail tracks.
They're not being guarded.

256
00:48:42,928 --> 00:48:46,125
Try and hide somewhere,
and we'll see what happens.

257
00:48:46,528 --> 00:48:47,802
Got that?

258
00:48:50,728 --> 00:48:52,127
Yes, Aunt harrer.

259
00:48:52,408 --> 00:48:56,083
If you see my Lajos,
tell him to come immediately.

260
00:48:57,048 --> 00:49:00,358
It's horrible here and
he's wandering about out there.

261
00:49:00,568 --> 00:49:02,399
And I've so much to do here.

262
00:49:04,208 --> 00:49:05,641
Goodbye Aunt harrer.

263
00:49:06,208 --> 00:49:08,164
Take care of yourself.

264
00:49:32,408 --> 00:49:37,641
LAjOS hARRER jr.
BOOT AND ShOEMAKER

265
00:53:58,008 --> 00:54:00,568
I've made a place for you.

266
00:54:03,088 --> 00:54:05,363
You'll see it if you come out.

267
00:54:08,368 --> 00:54:10,598
I'm living in the summer kitchen.

268
00:54:12,168 --> 00:54:14,124
Because that woman...

269
00:54:14,328 --> 00:54:18,207
and the police chief have taken it
on themselves to move in

270
00:54:18,408 --> 00:54:20,763
and have taken the whole place over.

271
00:54:23,968 --> 00:54:27,358
But we'll get by all right
with the summer kitchen outside.

272
00:54:29,648 --> 00:54:33,721
The sofa with the green blanket
will be yours.

273
00:54:35,568 --> 00:54:37,559
In the top part of the cupboard

274
00:54:37,928 --> 00:54:39,964
I'll make some space for you.

275
00:54:43,248 --> 00:54:45,557
The windows will have to be done.

276
00:54:45,928 --> 00:54:47,520
There are draughts

277
00:54:47,728 --> 00:54:49,241
and it's cold.

278
00:54:51,648 --> 00:54:54,560
I'm sleeping there
for the second day now.

279
00:54:56,448 --> 00:54:58,916
And, thank God, out there

280
00:54:59,448 --> 00:55:01,404
you can't hear a thing.

281
00:55:02,488 --> 00:55:04,240
Absolute silence.

282
00:55:08,648 --> 00:55:10,718
I've retuned the piano.

283
00:55:11,728 --> 00:55:14,959
Now, once again,
it's like any other.

284
00:55:16,608 --> 00:55:19,327
Once again,
you can play anything on it.

285
00:55:22,248 --> 00:55:24,204
And if we run short of money

286
00:55:24,808 --> 00:55:27,561
it's much easier this way.

287
00:55:33,608 --> 00:55:37,601
I took my grey overcoat
over to �rgyel�n.

288
00:55:38,928 --> 00:55:42,238
he promised he'd tailor it for you.

289
00:55:44,688 --> 00:55:46,519
whenever you come out

290
00:55:47,648 --> 00:55:49,366
it will be ready.

291
00:56:01,288 --> 00:56:03,006
Nothing counts.

292
00:56:05,208 --> 00:56:07,199
Nothing counts at all.

