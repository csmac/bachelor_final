1
00:03:25,373 --> 00:03:30,461
<i>There were seven girls on
The Albatross</i>

2
00:03:32,338 --> 00:03:37,635
<i>But it's you who left
this sadness in my head</i>

3
00:03:39,095 --> 00:03:44,017
<i>You were seventeen
when we were dating</i>

4
00:03:45,852 --> 00:03:51,065
<i>It was in Augustów
that I first laid eyes on you</i>

5
00:03:52,734 --> 00:03:57,906
<i>Your name was Beata</i>

6
00:03:59,574 --> 00:04:04,412
<i>It's a beautiful name, my love</i>

7
00:04:10,919 --> 00:04:15,173
<i>Help us come ashore</i>

8
00:04:15,590 --> 00:04:18,384
<i>There's no need to fear</i>

9
00:04:18,927 --> 00:04:21,846
<i>We won't eat you, my dear</i>

10
00:04:22,430 --> 00:04:25,767
<i>Eat you, eat you...</i>

11
00:05:31,666 --> 00:05:38,840
THE LURE

12
00:06:41,736 --> 00:06:43,738
What are you doing?

13
00:06:56,292 --> 00:06:58,211
Hi there.

14
00:07:04,300 --> 00:07:08,096
- What's that smell?
- Beats me, boss. I got a cold.

15
00:07:08,262 --> 00:07:10,932
Then stay home
and don't spread it around.

16
00:07:56,352 --> 00:07:58,646
Mrs. Rockets, what's that smell?

17
00:07:59,147 --> 00:08:01,983
We're cooking in here, boss.

18
00:09:06,881 --> 00:09:09,717
Actually they're a friend's kids.

19
00:09:09,842 --> 00:09:11,844
And this is a nightclub

20
00:09:11,969 --> 00:09:14,972
with a liquor license
and adult entertainment.

21
00:09:15,223 --> 00:09:21,062
And this room is
for authorized staff only.

22
00:09:21,938 --> 00:09:24,482
Beat it, you two.

23
00:09:25,733 --> 00:09:27,235
Fine.

24
00:09:36,118 --> 00:09:37,370
Excuse me, boss.

25
00:09:39,247 --> 00:09:40,748
Here you go.

26
00:09:45,753 --> 00:09:47,546
Take your clothes off.

27
00:10:04,814 --> 00:10:06,399
Nothing.

28
00:10:06,524 --> 00:10:08,025
Sit down.

29
00:10:14,782 --> 00:10:16,867
Smooth as Barbie dolls.

30
00:10:56,782 --> 00:11:00,244
This is great!

31
00:11:00,995 --> 00:11:04,457
I have to say
you really got me this time.

32
00:11:05,708 --> 00:11:07,335
Are those real?

33
00:11:08,294 --> 00:11:10,087
Check for yourself.

34
00:11:11,339 --> 00:11:13,424
Sure they won't bite?

35
00:11:13,549 --> 00:11:15,634
They're still young.

36
00:11:16,177 --> 00:11:18,179
So what do you suggest?

37
00:11:20,556 --> 00:11:22,850
The slit in the tail?

38
00:11:27,229 --> 00:11:29,565
Where's it supposed to be?

39
00:11:41,702 --> 00:11:43,704
There it is!

40
00:11:44,747 --> 00:11:47,124
Thing is, they smell fishy.

41
00:11:49,377 --> 00:11:51,170
It's not that bad.

42
00:12:10,523 --> 00:12:13,901
And if they dry out and change back,

43
00:12:14,276 --> 00:12:17,071
just splash some water on them.

44
00:12:20,491 --> 00:12:24,662
No offense, but I was wondering...

45
00:12:25,413 --> 00:12:28,374
where'd you learn to speak Polish?

46
00:12:28,916 --> 00:12:31,669
On the beach in Bulgaria.

47
00:12:32,586 --> 00:12:34,588
That's great.

48
00:12:35,756 --> 00:12:37,299
Just great.

49
00:12:40,928 --> 00:12:45,099
Give it up for Miss Muffet
and "The Banana Song"!

50
00:13:12,376 --> 00:13:14,837
<i>Swinging in the sun</i>

51
00:13:16,380 --> 00:13:18,382
<i>And thinking lazy thoughts</i>

52
00:13:23,596 --> 00:13:26,140
<i>There's a noise up in the sky</i>

53
00:13:27,600 --> 00:13:30,394
<i>I slowly raise my eyes</i>

54
00:13:35,649 --> 00:13:39,820
<i>I see a jet full of businesspeople</i>

55
00:13:40,988 --> 00:13:44,825
<i>Packed to capacity</i>

56
00:13:45,326 --> 00:13:49,914
<i>While down here
there's just a carefree song</i>

57
00:13:50,164 --> 00:13:52,625
<i>And the lapping of the sea</i>

58
00:13:55,878 --> 00:13:57,630
<i>It's the banana song</i>

59
00:14:22,863 --> 00:14:25,407
It'll be all right...

60
00:14:30,454 --> 00:14:33,832
I just splashed water on them.
I didn't hurt them.

61
00:14:43,092 --> 00:14:44,552
Hold on.

62
00:15:30,848 --> 00:15:32,808
- Breadstick?
- Please!

63
00:15:33,851 --> 00:15:35,394
What are your names?

64
00:15:35,728 --> 00:15:37,813
- I'm Golden.
- And I'm Silver.

65
00:15:37,938 --> 00:15:40,316
"Imgolden," "Andimsilver."

66
00:15:40,441 --> 00:15:42,401
Very nice.

67
00:15:43,110 --> 00:15:46,363
They can do
backup vocals and strip.

68
00:15:46,488 --> 00:15:48,490
Here's an advance.

69
00:15:48,616 --> 00:15:51,785
Now get them
something decent to wear.

70
00:16:07,301 --> 00:16:09,386
<i>I'm new to the city</i>

71
00:16:09,762 --> 00:16:13,766
<i>I wanted to put
my best foot forward</i>

72
00:16:13,891 --> 00:16:20,105
<i>Change what I can change
and get their attention</i>

73
00:16:20,272 --> 00:16:23,192
<i>A mention, a nod
I could be soaring</i>

74
00:16:23,400 --> 00:16:26,362
<i>But life's gross and boring</i>

75
00:16:26,654 --> 00:16:31,742
<i>I walk through the city
Cars honking, polluting</i>

76
00:16:32,785 --> 00:16:35,204
<i>New sights all around me</i>

77
00:16:35,954 --> 00:16:38,999
<i>The lights, sounds, and faces
never cease to astound me</i>

78
00:16:39,166 --> 00:16:41,418
<i>Bright shiny neon</i>

79
00:16:42,544 --> 00:16:45,673
<i>Is saying this is
the best street to be on</i>

80
00:16:45,839 --> 00:16:49,051
<i>Hustle and bustle
Buses, people, and cars</i>

81
00:16:49,176 --> 00:16:52,221
<i>So exciting we're not leaving
We like it here</i>

82
00:16:58,686 --> 00:17:03,691
<i>The city will tell us
what it is we lack</i>

83
00:17:06,276 --> 00:17:09,655
<i>The city will tell us
what is it we lack</i>

84
00:17:12,116 --> 00:17:17,246
<i>We've chosen to live here
We want things to be clear</i>

85
00:17:17,538 --> 00:17:20,791
<i>The city will tell us</i>

86
00:17:21,166 --> 00:17:23,377
<i>What is it we lack</i>

87
00:17:24,503 --> 00:17:30,426
<i>There's not a dull minute
Lots of people live in it</i>

88
00:17:30,634 --> 00:17:33,846
<i>The tastes, smells, and pleasures</i>

89
00:17:33,971 --> 00:17:36,890
<i>The garbage, the treasures</i>

90
00:17:37,224 --> 00:17:42,855
<i>Pigeons and sirens
Horns and pylons</i>

91
00:17:43,480 --> 00:17:48,152
<i>Streets hot and exciting
Parks cool and inviting</i>

92
00:17:50,112 --> 00:17:52,781
<i>The city will tell us</i>

93
00:17:53,115 --> 00:17:55,117
<i>What it is we lack</i>

94
00:17:57,244 --> 00:18:01,457
<i>It knows for a fact
what it is we lack</i>

95
00:18:03,584 --> 00:18:05,711
<i>We've chosen to live here</i>

96
00:18:05,836 --> 00:18:08,881
<i>We want things to be clear</i>

97
00:18:16,305 --> 00:18:21,101
<i>Hands made filthy by dirty deeds</i>

98
00:18:22,895 --> 00:18:28,150
<i>Can't stand to watch it go to seed</i>

99
00:19:05,437 --> 00:19:09,358
<i>Want to hang out here a while
before swimming to America?</i>

100
00:19:15,364 --> 00:19:17,282
<i>Sure.</i>

101
00:19:22,704 --> 00:19:24,248
<i>Silver...</i>

102
00:19:24,915 --> 00:19:27,334
<i>what if you fall in love?</i>

103
00:19:30,504 --> 00:19:31,755
<i>Don't be silly.</i>

104
00:19:33,465 --> 00:19:34,758
<i>Would you eat him?</i>

105
00:19:34,925 --> 00:19:36,844
<i>Girls...</i>

106
00:19:37,219 --> 00:19:41,306
I want you to cover up a bit
when you're singing.

107
00:19:42,224 --> 00:19:43,684
Why that?

108
00:19:44,268 --> 00:19:47,145
Give 'em something
to look forward to.

109
00:19:47,688 --> 00:19:50,691
Remember: Give it 100%...

110
00:19:51,358 --> 00:19:53,527
But not 200% or 300%.

111
00:20:15,215 --> 00:20:18,886
- Knock 'em dead, Krysia.
- Nervous, girls?

112
00:20:22,431 --> 00:20:25,392
Just have fun
and let the rest happen.

113
00:20:25,642 --> 00:20:27,227
Follow me.

114
00:20:29,980 --> 00:20:32,065
Don't touch me.

115
00:20:32,733 --> 00:20:35,485
The next act is Figs 'n' Dates...

116
00:20:36,111 --> 00:20:39,865
featuring two sisters...

117
00:20:40,824 --> 00:20:43,410
two little mermaids.

118
00:20:49,416 --> 00:20:53,295
Let's hear it
for Silver and Golden!

119
00:20:57,007 --> 00:20:58,508
Nice, huh?

120
00:21:18,153 --> 00:21:20,614
<i>You were my heart's beating</i>

121
00:21:23,200 --> 00:21:25,452
<i>My life and spring so fleeting</i>

122
00:21:28,038 --> 00:21:30,457
<i>My dreams before and after</i>

123
00:21:33,126 --> 00:21:35,587
<i>My wind, my wine, my laughter</i>

124
00:21:37,714 --> 00:21:41,969
<i>After midnight in my town
the streetcars all get lost</i>

125
00:21:42,719 --> 00:21:46,932
<i>The night is run by demons
and the wires all get crossed</i>

126
00:21:47,766 --> 00:21:52,396
<i>Infernal powers fiddle with the routes
It's all in limbo</i>

127
00:21:52,688 --> 00:21:57,609
<i>And for some reason all the lines
end up outside your window</i>

128
00:21:58,151 --> 00:22:00,487
<i>You were my heart's beating</i>

129
00:22:03,323 --> 00:22:05,492
<i>My life and spring so fleeting</i>

130
00:22:08,120 --> 00:22:10,330
<i>My dreams before and after</i>

131
00:22:13,208 --> 00:22:15,627
<i>My wind, my wine, my laughter</i>

132
00:22:18,213 --> 00:22:22,843
<i>Someone asked me how you are
How's it going</i>

133
00:22:23,135 --> 00:22:28,306
<i>Someone with whom you're at war
Out there, longing</i>

134
00:22:28,432 --> 00:22:33,103
<i>Someone rushes
through the night on a streetcar</i>

135
00:22:33,228 --> 00:22:37,941
<i>They think they know the score
but I'm already at your door</i>

136
00:22:58,420 --> 00:23:00,630
<i>You were my heart's beating</i>

137
00:23:03,300 --> 00:23:05,635
<i>My life and spring so fleeting</i>

138
00:23:08,221 --> 00:23:10,348
<i>My dreams before and after</i>

139
00:23:13,268 --> 00:23:15,645
<i>My wind, my wine, my laughter</i>

140
00:23:30,035 --> 00:23:31,828
Girls...

141
00:23:32,579 --> 00:23:34,539
I'm so proud of you.

142
00:23:36,166 --> 00:23:38,877
My pretty girls.

143
00:23:40,295 --> 00:23:41,838
Jesus...

144
00:24:30,470 --> 00:24:33,473
Where did you disappear to?

145
00:24:45,318 --> 00:24:46,820
Golden!

146
00:24:47,487 --> 00:24:51,074
- Who me?
- No, Santa Claus.

147
00:25:32,324 --> 00:25:36,703
<i>It's been a long time
since I've felt this lonesome</i>

148
00:25:36,912 --> 00:25:41,041
<i>It can't be the weather
though it's pretty loathsome</i>

149
00:25:41,208 --> 00:25:47,547
<i>At night I get cravings
that aren't quite wholesome</i>

150
00:25:49,925 --> 00:25:54,262
<i>It's been a long time
since I've felt this lonesome</i>

151
00:25:54,554 --> 00:25:58,433
<i>It can't be the weather
though it's pretty loathsome</i>

152
00:25:58,558 --> 00:26:05,273
<i>At night I get cravings
that aren't quite wholesome</i>

153
00:26:07,150 --> 00:26:11,404
<i>Because I'm sad and so are you</i>

154
00:26:11,821 --> 00:26:15,533
<i>Now he's sad 'cause we're sad too</i>

155
00:26:15,825 --> 00:26:19,955
<i>You're all sad and they're all sad</i>

156
00:26:20,288 --> 00:26:24,251
<i>We're all gloomy as hell</i>

157
00:26:27,003 --> 00:26:31,007
<i>Bid farewell to cheerful smiles</i>

158
00:26:31,633 --> 00:26:35,262
<i>Dark thoughts eat me up alive</i>

159
00:26:35,637 --> 00:26:42,560
<i>I'm a fly facing winter
heavy, black, and sleepy</i>

160
00:26:44,354 --> 00:26:48,316
<i>Because I'm sad and so are you</i>

161
00:26:48,775 --> 00:26:52,779
<i>Now he's sad 'cause we're sad too</i>

162
00:26:52,988 --> 00:26:57,284
<i>You're all sad and they're all sad</i>

163
00:26:57,450 --> 00:27:00,745
<i>We're all gloomy as hell</i>

164
00:27:59,804 --> 00:28:01,723
Champagne.

165
00:28:23,495 --> 00:28:25,497
Hey, Silver!

166
00:28:26,206 --> 00:28:28,041
What the fuck?

167
00:28:28,208 --> 00:28:31,753
The name's Golden.
I'll be right back.

168
00:28:34,964 --> 00:28:37,759
You know I'm hung up on you.

169
00:28:42,555 --> 00:28:44,224
I love you...

170
00:28:44,891 --> 00:28:46,726
but don't tell anyone.

171
00:29:12,752 --> 00:29:14,212
Golden?

172
00:29:31,771 --> 00:29:33,273
Golden!

173
00:29:40,655 --> 00:29:43,241
Open up, Golden. I give up.

174
00:29:43,491 --> 00:29:45,118
You win.

175
00:30:16,024 --> 00:30:17,484
Silver.

176
00:30:19,611 --> 00:30:23,281
- Have you seen my sister?
- She didn't come this way.

177
00:30:28,328 --> 00:30:30,330
Have a smoke, child.

178
00:30:32,165 --> 00:30:33,625
Go on.

179
00:30:36,211 --> 00:30:38,296
I've never smoked before.

180
00:30:38,838 --> 00:30:40,673
That's not good.

181
00:30:44,594 --> 00:30:46,679
You want to have a pussy, huh?

182
00:30:52,852 --> 00:30:54,896
I'm shaking all over.

183
00:30:55,438 --> 00:30:57,982
"I'm such a shy thing."

184
00:31:03,238 --> 00:31:06,032
You know it'll make you
lose your voice?

185
00:32:33,536 --> 00:32:35,788
I have to tell you something.

186
00:32:38,041 --> 00:32:40,877
I really went crazy.
You were amazing today.

187
00:32:49,469 --> 00:32:51,596
I'm the one who's crazy.

188
00:33:30,968 --> 00:33:32,512
Put it in.

189
00:33:46,526 --> 00:33:47,777
Silver...

190
00:33:49,195 --> 00:33:51,322
You want me to be a girl?

191
00:33:54,992 --> 00:33:56,828
Don't be angry...

192
00:33:58,121 --> 00:34:02,125
but to me you'll always be
a fish, an animal.

193
00:34:03,626 --> 00:34:06,796
I can't do this,
as much as I'd like to.

194
00:34:31,738 --> 00:34:33,656
Darling Mietek...

195
00:34:34,824 --> 00:34:37,577
you'll be the world's
first four-handed bass player.

196
00:34:41,122 --> 00:34:42,749
Kiss me first.

197
00:35:07,940 --> 00:35:10,651
<i>Come watch over me closely</i>

198
00:35:11,360 --> 00:35:14,113
<i>Follow my every movement</i>

199
00:35:14,489 --> 00:35:19,118
<i>Take note of my tics and habits
Memorize all my gestures</i>

200
00:35:21,621 --> 00:35:24,624
<i>Be there at the right moment</i>

201
00:35:25,124 --> 00:35:28,294
<i>Tell me when something seems false</i>

202
00:35:28,753 --> 00:35:33,800
<i>Put your hand deep inside me
and drag me onto the shore</i>

203
00:35:35,468 --> 00:35:41,307
<i>If you do, I'll convert</i>

204
00:35:42,558 --> 00:35:48,147
<i>To a life not bound up with virtue</i>

205
00:35:49,315 --> 00:35:54,070
<i>Regain my former composure</i>

206
00:35:55,488 --> 00:36:00,409
<i>Learn all your secrets anew</i>

207
00:36:02,495 --> 00:36:04,872
<i>Come watch over me closely</i>

208
00:36:05,164 --> 00:36:07,333
<i>Follow my every movement</i>

209
00:36:07,667 --> 00:36:11,879
<i>Take note of my tics and habits
Memorize all my gestures</i>

210
00:36:13,005 --> 00:36:15,466
<i>Be there at the right moment</i>

211
00:36:15,800 --> 00:36:18,261
<i>Tell me when something seems false</i>

212
00:36:18,511 --> 00:36:23,182
<i>Put your hand deep inside me
and drag me onto the shore</i>

213
00:36:23,474 --> 00:36:27,854
<i>And if there's any time left</i>

214
00:36:28,437 --> 00:36:32,984
<i>I'll have another try</i>

215
00:36:34,360 --> 00:36:39,073
<i>At catching you off guard</i>

216
00:36:39,198 --> 00:36:43,703
<i>In any way I can</i>

217
00:37:27,872 --> 00:37:30,499
<i>That which endures forever</i>

218
00:37:33,085 --> 00:37:35,922
<i>Inscribed in our memories</i>

219
00:37:38,507 --> 00:37:40,927
<i>That which endures forever</i>

220
00:37:43,763 --> 00:37:46,349
<i>Inscribed in our memories</i>

221
00:37:50,102 --> 00:37:54,315
There you are! Come in.

222
00:37:54,440 --> 00:37:56,859
Next you'll be growing a tail!

223
00:37:56,984 --> 00:37:58,778
Want some herring?

224
00:38:13,167 --> 00:38:14,752
It was a cow.

225
00:38:24,470 --> 00:38:28,474
Good evening, ladies and gentlemen.

226
00:38:29,141 --> 00:38:33,312
Mermaids make their way
up from the abyss to the seabed.

227
00:38:33,479 --> 00:38:37,775
Then they use it for leverage
and swim to the surface.

228
00:38:38,234 --> 00:38:41,988
I give you... The Lure!

229
00:39:04,677 --> 00:39:07,221
<i>You come in and look me over</i>

230
00:39:09,432 --> 00:39:11,684
<i>You're picky and you're choosy</i>

231
00:39:14,186 --> 00:39:16,230
<i>They all want to be with you</i>

232
00:39:18,441 --> 00:39:21,485
<i>Every day of the week</i>

233
00:39:23,195 --> 00:39:27,908
<i>A wink like a fish scale
flashing in the depths</i>

234
00:39:32,246 --> 00:39:36,125
<i>So many albums
we could have danced to</i>

235
00:39:41,422 --> 00:39:43,799
<i>You choose a vowel A-A-A-A</i>

236
00:39:46,052 --> 00:39:49,055
<i>I'll have it played</i>

237
00:39:50,723 --> 00:39:53,142
<i>Letters spinning quicker</i>

238
00:39:55,061 --> 00:39:58,397
<i>Someone spiked our liquor</i>

239
00:39:59,648 --> 00:40:01,776
<i>Let go for a second</i>

240
00:40:01,901 --> 00:40:03,861
<i>Let love flow inside you</i>

241
00:40:09,033 --> 00:40:13,412
<i>Discover the flavor
of all that's denied you</i>

242
00:40:18,000 --> 00:40:20,252
<i>Holy moly</i>

243
00:40:20,419 --> 00:40:22,505
<i>Bitter tastes can be delicious</i>

244
00:40:22,630 --> 00:40:24,632
<i>So you're lonely</i>

245
00:40:24,757 --> 00:40:27,093
<i>Now I know that love is vicious</i>

246
00:40:27,218 --> 00:40:29,595
<i>Holy moly</i>

247
00:40:29,720 --> 00:40:31,764
<i>Bitter tastes can be delicious</i>

248
00:40:31,889 --> 00:40:33,891
<i>So you're lonely</i>

249
00:40:34,016 --> 00:40:36,435
<i>Now I know that love is vicious</i>

250
00:41:03,671 --> 00:41:07,800
<i>They promised frutti di mare</i>

251
00:41:08,092 --> 00:41:12,888
<i>Now we're feeling sorry</i>

252
00:41:13,139 --> 00:41:16,976
<i>I'm angry as hell</i>

253
00:41:17,351 --> 00:41:23,482
<i>Picking at love's cracked-up shells</i>

254
00:41:26,735 --> 00:41:30,531
<i>One last piece of flesh</i>

255
00:41:30,990 --> 00:41:35,202
<i>Lands on my plate</i>

256
00:41:35,452 --> 00:41:39,832
<i>Charred like some scrap</i>

257
00:41:40,374 --> 00:41:44,670
<i>No one wants to wrap</i>

258
00:42:05,983 --> 00:42:07,693
The head of entertainment.

259
00:42:07,943 --> 00:42:10,571
I loved your act!

260
00:42:10,696 --> 00:42:12,740
You were sensational!

261
00:42:45,356 --> 00:42:47,733
The fiends are on the scene.

262
00:43:12,258 --> 00:43:15,511
Don't push your luck
or you'll get a broken arm.

263
00:43:23,978 --> 00:43:27,481
You are my blood.
I have horns under my flesh.

264
00:43:30,776 --> 00:43:33,654
A fisherman broke this one off.

265
00:43:34,905 --> 00:43:37,116
I tore this one out myself.

266
00:43:47,835 --> 00:43:51,088
You're so beautiful,
and you have a great fish tail.

267
00:43:53,757 --> 00:43:56,051
And an amazing voice.

268
00:43:57,928 --> 00:44:00,222
Want to sing with my band?

269
00:44:11,817 --> 00:44:13,694
<i>Keep an eye on her.</i>

270
00:44:17,740 --> 00:44:21,869
<i>If she falls in love with a human
and he marries someone else...</i>

271
00:44:24,913 --> 00:44:28,751
<i>she'll turn into sea foam
before the night is out.</i>

272
00:44:50,564 --> 00:44:52,107
Police.

273
00:44:53,233 --> 00:44:54,860
Good evening.

274
00:44:55,903 --> 00:44:57,696
What's "police"?

275
00:45:00,532 --> 00:45:04,119
I can put the squeeze on you
so hard you'll gasp for breath.

276
00:45:13,045 --> 00:45:16,423
You ate a citizen
who was out on the town.

277
00:45:19,343 --> 00:45:21,261
I know nothing about that.

278
00:45:24,098 --> 00:45:26,850
I wasn't born yesterday.

279
00:45:29,520 --> 00:45:33,774
<i>Leaves are falling
like they've gone crazy</i>

280
00:45:34,066 --> 00:45:37,736
<i>Ravens are pecking at nuts</i>

281
00:45:39,029 --> 00:45:43,742
<i>The wind raises goose bumps
on the bare ankles</i>

282
00:45:43,951 --> 00:45:47,329
<i>Of girls walking home
from their classes</i>

283
00:45:48,038 --> 00:45:51,834
<i>The wind feels freezing
on my bare skin</i>

284
00:45:52,126 --> 00:45:55,712
<i>I just need
some warmth and comfort</i>

285
00:45:56,338 --> 00:45:59,967
<i>Leaves are falling
like they've gone crazy</i>

286
00:46:00,217 --> 00:46:03,554
<i>Ravens are pecking at nuts</i>

287
00:46:05,431 --> 00:46:08,559
<i>Leaves are falling
like they've gone crazy</i>

288
00:46:08,684 --> 00:46:12,271
<i>Ravens are pecking at nuts</i>

289
00:46:12,896 --> 00:46:16,525
<i>My breath condenses
in the storm of the senses</i>

290
00:46:16,859 --> 00:46:20,612
<i>Anxiety tugs at my elbow</i>

291
00:46:20,946 --> 00:46:24,867
<i>Frost and dampness
are now in motion</i>

292
00:46:25,409 --> 00:46:28,370
<i>Bodies swell with emotion</i>

293
00:46:29,455 --> 00:46:33,041
<i>Leaves are falling
like they've gone crazy</i>

294
00:46:33,250 --> 00:46:36,962
<i>Ravens are pecking at nuts</i>

295
00:46:51,101 --> 00:46:54,396
<i>Oil on your palms
Le! me hold them</i>

296
00:46:54,605 --> 00:46:58,108
<i>Babe, I'll take you to Disneyland</i>

297
00:46:58,275 --> 00:47:01,737
<i>We'll watch Paris burn together</i>

298
00:47:02,112 --> 00:47:04,531
<i>See fire spread in Rome</i>

299
00:47:04,823 --> 00:47:06,700
<i>Or in me</i>

300
00:47:12,998 --> 00:47:16,627
<i>You have your doubts about going out
I had a talk with Daddy</i>

301
00:47:16,752 --> 00:47:19,796
<i>He has no buts regarding sluts</i>

302
00:47:20,172 --> 00:47:23,550
<i>So what if you blew your debut</i>

303
00:47:23,842 --> 00:47:26,595
<i>Next time you're bound to get lucky</i>

304
00:47:27,554 --> 00:47:30,516
<i>Karma's like that, it can dog you</i>

305
00:47:30,974 --> 00:47:33,352
<i>The limelight tastes sour</i>

306
00:47:34,978 --> 00:47:38,398
<i>You have your doubts about going out
I had a talk with Daddy</i>

307
00:47:38,524 --> 00:47:41,360
<i>He has no buts regarding sluts</i>

308
00:47:41,944 --> 00:47:45,614
<i>Oil on your palms
Le! me hold them</i>

309
00:47:45,739 --> 00:47:49,034
<i>Babe, I'll take you to Disneyland</i>

310
00:47:49,243 --> 00:47:52,704
<i>We'll watch Paris burn together</i>

311
00:47:53,080 --> 00:47:56,124
<i>See fire spread in Rome</i>

312
00:49:33,639 --> 00:49:36,058
How come you reek of fish?

313
00:49:41,146 --> 00:49:43,106
I ate some herring.

314
00:49:44,066 --> 00:49:45,734
With your fingers?

315
00:49:46,735 --> 00:49:47,903
Krysia...

316
00:49:48,236 --> 00:49:53,367
Go and bone
those fucking fish of yours!

317
00:50:23,271 --> 00:50:25,107
I worry about you.

318
00:50:26,108 --> 00:50:27,651
Stop it.

319
00:50:32,406 --> 00:50:34,574
<i>Nothing in the obituaries.</i>

320
00:50:34,700 --> 00:50:36,785
<i>- Looking for someone in particular?
- You!</i>

321
00:50:37,202 --> 00:50:41,832
<i>This one goes out from an admiring
drummer to a semi-obscure singer.</i>

322
00:50:41,957 --> 00:50:44,501
<i>Bill Chase with "Cronus."</i>

323
00:50:44,626 --> 00:50:47,045
- I don't believe this.
- Stop it.

324
00:52:12,005 --> 00:52:13,256
<i>Good evening.</i>

325
00:52:13,965 --> 00:52:17,803
<i>We open tonight's program
with an exceptional case</i>

326
00:52:17,928 --> 00:52:20,222
<i>that occurred last week,</i>

327
00:52:20,347 --> 00:52:24,226
<i>when a Warsaw beach became
the scene of a gruesome murder.</i>

328
00:52:25,143 --> 00:52:28,313
<i>The victim's throat was ripped open
and his heart torn out.</i>

329
00:52:28,480 --> 00:52:30,607
<i>Missing body parts
have not been found.</i>

330
00:52:30,732 --> 00:52:34,694
<i>Foxes and dogs have been ruled out.</i>

331
00:52:38,198 --> 00:52:40,909
<i>Witnesses are asked to come forward.</i>

332
00:52:41,660 --> 00:52:45,872
<i>The only tracks on the scene are
of a human and something reptilian.</i>

333
00:52:45,997 --> 00:52:50,168
<i>If any of our viewers
has any information,</i>

334
00:52:50,293 --> 00:52:53,046
<i>please call our studios</i>

335
00:52:53,171 --> 00:52:55,632
<i>at 43-10-53...</i>

336
00:52:55,757 --> 00:52:58,844
<i>or contact
your nearest police station...</i>

337
00:53:02,305 --> 00:53:03,598
Fuck!

338
00:53:05,016 --> 00:53:07,769
How come we never play
volleyball together?

339
00:53:08,019 --> 00:53:12,816
We never roller-skate
or ice-skate or ride scooters.

340
00:53:12,941 --> 00:53:15,193
We can't go
to restaurants or bars.

341
00:53:15,360 --> 00:53:18,029
We work,
but we don't get any money.

342
00:53:19,197 --> 00:53:21,241
You're still kids.

343
00:53:21,533 --> 00:53:23,910
Kids can't have everything they want.

344
00:53:24,035 --> 00:53:26,204
- Brats.
- Why?

345
00:53:34,170 --> 00:53:36,089
I'll smack her, I swear.

346
00:54:25,138 --> 00:54:29,309
<i>One jellyfish, two jellyfishes</i>

347
00:54:29,726 --> 00:54:33,688
<i>I wanna grow up
and get all my wishes</i>

348
00:54:34,105 --> 00:54:37,943
<i>Three jellyfishes, four jellybeanses</i>

349
00:54:38,151 --> 00:54:41,613
<i>We want the plankton
scrubbing our fins</i>

350
00:54:41,821 --> 00:54:44,783
<i>Five jellyfishees, six jellyfishoos</i>

351
00:54:44,991 --> 00:54:47,619
<i>Wanna walk on the water
in my high-heeled shoes</i>

352
00:54:47,744 --> 00:54:49,621
<i>Seven jellyfishes, eight jellybabies</i>

353
00:54:49,788 --> 00:54:51,831
<i>Wanna drown in the water
and stay dead for ages</i>

354
00:54:51,957 --> 00:54:55,001
<i>Nine jellyfishoos, ten jellyfishus</i>

355
00:54:55,126 --> 00:54:57,629
<i>Give me a boyfriend
who isn't amphibious!</i>

356
00:56:28,178 --> 00:56:29,888
Let go of him!

357
00:56:32,932 --> 00:56:34,392
Leave him alone!

358
00:58:56,242 --> 00:59:01,915
<i>Poison</i>

359
00:59:04,959 --> 00:59:11,049
<i>Inside you</i>

360
00:59:14,302 --> 00:59:26,231
<i>That's what the smell is</i>

361
00:59:30,401 --> 00:59:34,530
<i>But is it done, is it done, is it done?</i>

362
01:00:33,881 --> 01:00:39,387
<i>It goes to your head</i>

363
01:00:41,639 --> 01:00:48,521
<i>Noxious and nasty</i>

364
01:00:48,938 --> 01:00:53,026
<i>But is it done, is it done, is it done?</i>

365
01:01:07,999 --> 01:01:14,797
<i>I can't walk this one off</i>

366
01:01:17,008 --> 01:01:22,722
<i>I need my glucose</i>

367
01:01:25,433 --> 01:01:31,522
<i>My hair feels like viscose</i>

368
01:01:34,317 --> 01:01:39,072
<i>The pain's made me morose</i>

369
01:01:41,324 --> 01:01:45,161
<i>But is it done, is it done, is it done?</i>

370
01:03:13,791 --> 01:03:15,710
I want to apologize...

371
01:03:17,253 --> 01:03:19,422
for what happened before.

372
01:03:21,174 --> 01:03:22,675
I'm sorry.

373
01:03:24,635 --> 01:03:26,971
Oh, we're not mad.

374
01:04:55,393 --> 01:04:56,978
Mind if I join you?

375
01:04:59,981 --> 01:05:02,233
Hi.

376
01:05:07,863 --> 01:05:10,783
I'll be getting
real legs and a pussy.

377
01:05:13,578 --> 01:05:16,247
You can have my tail if you want.

378
01:05:17,456 --> 01:05:18,958
You want it?

379
01:05:19,375 --> 01:05:20,960
What for?

380
01:05:22,461 --> 01:05:24,880
I know you want a fish tail.

381
01:05:27,091 --> 01:05:28,634
No, I don't.

382
01:05:52,867 --> 01:05:55,328
You can't talk to him now.

383
01:05:55,453 --> 01:05:57,788
He's getting
psyched up for the gig.

384
01:06:01,250 --> 01:06:03,044
Are you a mermaid?

385
01:06:03,210 --> 01:06:04,837
We'll just see.

386
01:06:29,820 --> 01:06:32,406
<i>The colors keep changing</i>

387
01:06:32,531 --> 01:06:34,659
<i>From green to yellow to red</i>

388
01:06:57,390 --> 01:07:02,561
<i>Someone who never existed
walked out on me</i>

389
01:07:18,994 --> 01:07:21,122
<i>I'm getting up and falling</i>

390
01:07:21,247 --> 01:07:24,458
<i>I'm falling! I'm falling!</i>

391
01:07:24,583 --> 01:07:28,045
<i>Don't ask where I'm going</i>

392
01:07:46,397 --> 01:07:49,233
Triton! Triton! Triton!

393
01:07:55,865 --> 01:07:58,033
You were really...

394
01:07:58,868 --> 01:08:00,911
You need a lot of practice.

395
01:08:56,300 --> 01:09:00,805
<i>Triton said if you out off your tail,
you'll lose your voice.</i>

396
01:09:02,306 --> 01:09:04,725
<i>Nothing happened to him, did it?</i>

397
01:09:07,728 --> 01:09:12,107
<i>Silver, you have to sing.
We have to sing together.</i>

398
01:09:19,782 --> 01:09:22,701
<i>That's a superstition, you moron.</i>

399
01:09:33,003 --> 01:09:37,216
<i>Try to stop me
and you'll never see me again.</i>

400
01:09:39,552 --> 01:09:40,845
<i>Got that?</i>

401
01:10:31,770 --> 01:10:34,565
It's not my birthday.

402
01:10:36,400 --> 01:10:38,402
What can I do for you?

403
01:10:47,661 --> 01:10:52,124
<i>I'm bound to the bed
There's bile in the drip</i>

404
01:10:56,545 --> 01:11:00,633
<i>No veil, just a tube in my vein</i>

405
01:11:04,803 --> 01:11:13,479
<i>I'll be wheeled down the aisle
in my surgical gown</i>

406
01:11:13,604 --> 01:11:18,025
<i>As orderlies roll on their latex</i>

407
01:11:18,275 --> 01:11:24,198
<i>The bedpan's bright gold
The dinner's grown cold</i>

408
01:11:27,117 --> 01:11:32,790
<i>And time flows like sweat
off a midwife's brow</i>

409
01:11:35,542 --> 01:11:41,966
<i>I cheer myself up
with bitter chocolate</i>

410
01:11:44,426 --> 01:11:48,722
<i>Bitter chocolate makes me better</i>

411
01:11:52,309 --> 01:11:59,316
<i>You can always choose to walk out</i>

412
01:12:00,776 --> 01:12:04,196
<i>As long as you sign
the release papers</i>

413
01:12:06,073 --> 01:12:10,285
<i>On your own responsibility</i>

414
01:12:10,786 --> 01:12:16,750
<i>Your own responsibility</i>

415
01:13:24,193 --> 01:13:31,075
<i>You can always choose to walk out</i>

416
01:13:32,951 --> 01:13:36,538
<i>As long as you sign
the release papers</i>

417
01:13:38,415 --> 01:13:43,087
<i>On your own responsibility</i>

418
01:13:53,597 --> 01:13:57,059
That's for you, sis.
To cheer you up.

419
01:14:20,707 --> 01:14:23,460
<i>So much smoke
it makes my mouth dry</i>

420
01:14:23,585 --> 01:14:25,712
<i>Bodies on the dance floor dancing by</i>

421
01:14:26,213 --> 01:14:28,507
<i>Did I catch your eye on purpose?</i>

422
01:14:28,966 --> 01:14:31,468
<i>I'm no rookie but I'm nervous</i>

423
01:14:31,677 --> 01:14:34,429
<i>Spilling smokes
my hands are shaking</i>

424
01:14:34,555 --> 01:14:37,141
<i>Is this the wrong step I'm taking?</i>

425
01:14:37,307 --> 01:14:42,396
<i>As fatal as hara-kiri
my emotions make me silly</i>

426
01:14:42,604 --> 01:14:45,149
<i>Hearts' magnetism pulls me in</i>

427
01:14:45,274 --> 01:14:47,985
<i>Don't want to fall in love again</i>

428
01:14:59,329 --> 01:15:01,707
<i>Just one moment and I'm reeling</i>

429
01:15:02,040 --> 01:15:04,585
<i>You lit the fire of my feelings</i>

430
01:15:04,793 --> 01:15:07,462
<i>On this dance floor I'll dance for you</i>

431
01:15:07,588 --> 01:15:10,007
<i>Cheat on you but still adore you</i>

432
01:15:15,637 --> 01:15:17,931
<i>Hearts' magnetism pulls me in</i>

433
01:15:18,223 --> 01:15:21,185
<i>Don't want to fall in love again</i>

434
01:16:51,608 --> 01:16:53,402
Boss...

435
01:16:55,279 --> 01:16:58,198
Silver could lip-synch.
It'd be great.

436
01:16:58,365 --> 01:17:00,951
In my club?
Over my dead body.

437
01:17:03,912 --> 01:17:07,833
Silver, don't get me wrong.

438
01:17:08,041 --> 01:17:10,252
Everyone loves you.

439
01:17:10,669 --> 01:17:13,672
I love all of you. Lots.

440
01:17:14,548 --> 01:17:19,803
This is hard for me,
but everyone wants duos now.

441
01:17:25,517 --> 01:17:27,769
We'll have Golden go solo.

442
01:17:30,105 --> 01:17:32,024
I don't sing solo.

443
01:17:45,829 --> 01:17:49,124
My whole band's sick.

444
01:17:49,583 --> 01:17:52,085
How are you holding up?
- Fit as a fiddle.

445
01:17:52,252 --> 01:17:55,422
I like that song of yours "The Fly."

446
01:17:55,547 --> 01:17:57,758
That one's by The Lure.

447
01:17:59,343 --> 01:18:01,261
Let's rock it a bit, huh?

448
01:18:04,389 --> 01:18:06,099
What do I play?

449
01:20:02,257 --> 01:20:05,844
<i>Because I'm sad and so are you</i>

450
01:20:06,511 --> 01:20:10,140
<i>Now he's sad 'cause we're sad too</i>

451
01:20:10,265 --> 01:20:13,852
<i>You're all sad and they're all sad</i>

452
01:20:14,394 --> 01:20:17,522
<i>We're all gloomy as hell</i>

453
01:20:19,149 --> 01:20:22,903
<i>Because I'm sad and so are you</i>

454
01:20:23,361 --> 01:20:27,365
<i>Now he's sad 'cause we're sad too</i>

455
01:20:27,491 --> 01:20:30,911
<i>You're all sad and they're all sad</i>

456
01:20:31,119 --> 01:20:35,040
<i>We're all gloomy as hell</i>

457
01:20:58,897 --> 01:21:01,691
Kiss, kiss, kiss!

458
01:21:01,942 --> 01:21:05,487
Let's hear it for the newlyweds! Kiss!

459
01:21:05,612 --> 01:21:08,573
The name of the band is
Ballady I Romanse!

460
01:21:18,667 --> 01:21:21,044
You have to eat him by daybreak.

461
01:21:24,172 --> 01:21:26,049
Chickadee...

462
01:21:29,052 --> 01:21:32,764
He's just a human.
You'll be sea foam if you don't.

463
01:21:34,683 --> 01:21:39,062
We're not human.
We're just on vacation here.

464
01:21:41,231 --> 01:21:43,400
You'll vanish into thin air.

465
01:21:43,525 --> 01:21:45,944
<i>Give me this night</i>

466
01:21:46,903 --> 01:21:49,072
<i>Just this one night</i>

467
01:21:50,407 --> 01:21:52,951
<i>Give me this night</i>

468
01:21:54,035 --> 01:21:56,621
<i>Just this one night</i>

469
01:21:57,747 --> 01:22:00,792
<i>In warm houses people fret</i>

470
01:22:01,293 --> 01:22:04,462
<i>All the stress, too much to cope</i>

471
01:22:04,796 --> 01:22:08,008
<i>But here hands are holding hands</i>

472
01:22:08,383 --> 01:22:11,219
<i>And in every glass there's hope</i>

473
01:22:11,803 --> 01:22:14,806
<i>We don't need that much at all</i>

474
01:22:15,432 --> 01:22:18,268
<i>Just to party till we weep</i>

475
01:22:18,977 --> 01:22:22,105
<i>Surrounded by these spinning walls</i>

476
01:22:22,439 --> 01:22:25,817
<i>And the rain of dancing lights</i>

477
01:22:40,081 --> 01:22:46,755
<i>Enjoy the fun before it's gone</i>

478
01:23:08,360 --> 01:23:11,655
<i>Let's get lost and forget again</i>

479
01:23:11,905 --> 01:23:15,200
<i>All the times when we felt down</i>

480
01:23:15,408 --> 01:23:18,495
<i>There'll be time for all of that</i>

481
01:23:18,620 --> 01:23:22,165
<i>When daylight comes around</i>

482
01:23:30,298 --> 01:23:31,758
Eat him.

483
01:26:49,372 --> 01:26:51,165
What the fuck!

484
01:29:11,973 --> 01:29:19,731
<i>My fingers slowly ease
their grip on your hand</i>

485
01:29:22,859 --> 01:29:30,992
<i>The sludge of despair
sinks to the bottom</i>

486
01:29:33,953 --> 01:29:36,372
<i>One thing is certain</i>

487
01:29:37,999 --> 01:29:46,299
<i>I'll be warmer here than in your arms</i>

488
01:29:48,968 --> 01:29:56,976
<i>I'll be warmer amid these weeds
and old car parts</i>

489
01:30:01,981 --> 01:30:08,112
<i>Than on your knee</i>

490
01:30:10,114 --> 01:30:19,040
<i>The undertow that drags me down</i>

491
01:30:23,044 --> 01:30:30,134
<i>Is gentler than your hands and lips</i>

492
01:30:32,595 --> 01:30:35,348
<i>One thing is certain</i>

493
01:30:36,682 --> 01:30:45,024
<i>I'll be warmer here than in your arms</i>

494
01:30:50,404 --> 01:30:55,868
<i>Now that I'm surrounded
by creatures of the sea</i>

495
01:30:55,993 --> 01:31:03,501
<i>Don't pretend you're sorry</i>

496
01:31:18,808 --> 01:31:21,727
<i>'Cause I'll be better off here</i>

497
01:31:24,397 --> 01:31:27,483
<i>So much warmer here</i>

498
01:31:29,944 --> 01:31:34,198
<i>Amid these old car parts</i>

499
01:31:34,490 --> 01:31:41,205
<i>Than on your knee</i>

