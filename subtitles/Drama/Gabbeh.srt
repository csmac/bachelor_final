1
00:00:14,672 --> 00:00:16,185
www.iranianmovies.org

2
00:00:16,472 --> 00:00:19,065
http://iribmediatrade.ir

3
00:00:24,230 --> 00:00:26,265
GABBEH

4
00:00:26,549 --> 00:00:28,459
A film by Mohsen MAKHMALBAF

5
00:00:29,028 --> 00:00:30,222
Cast:

6
00:00:30,508 --> 00:00:33,101
Abbas SAYAHI, Shaghayegh DJODAT

7
00:00:33,827 --> 00:00:35,977
Hossein MOHARAMI, R. MOHARAMI

8
00:00:36,266 --> 00:00:37,938
Parvaneh GHALANDARI

9
00:00:38,546 --> 00:00:41,742
Sound: Mojtaba MIRTAHMASEBI

10
00:00:42,465 --> 00:00:44,739
Sound mixing: Abbas RASTEGARPOUR

11
00:00:44,984 --> 00:00:46,497
and Behroz SHAHAMAT

12
00:00:47,224 --> 00:00:48,861
Focus pullers:

13
00:00:49,103 --> 00:00:51,173
Shahriar ASADI, Behzad DORANI

14
00:00:52,062 --> 00:00:53,939
Director's assistants:

15
00:00:54,182 --> 00:00:56,251
B. AZIMPOUR, A. ALAGHEBAND

16
00:01:01,660 --> 00:01:04,890
Executive producer:
Mostafa MIRZAKHANI

17
00:01:05,579 --> 00:01:07,694
Producers: Khalil DAROUDCHI

18
00:01:07,978 --> 00:01:09,331
and Khalil MAHMOUDI

19
00:01:09,978 --> 00:01:13,333
Set photographer:
Mohamad AHMADI

20
00:01:14,217 --> 00:01:17,367
Music: Houssein ALIZADEH

21
00:01:18,016 --> 00:01:21,166
Director of photography:
Mahmoud KALARI

22
00:02:31,117 --> 00:02:34,870
You were moaning last night,
saying your feet hurt.

23
00:02:35,516 --> 00:02:37,507
You couldn't sleep.

24
00:02:38,995 --> 00:02:42,191
Let me wash the gabbeh.

25
00:02:42,394 --> 00:02:44,749
Let me do it - I've got boots on.

26
00:02:44,954 --> 00:02:47,547
Pass me the boots.

27
00:02:47,753 --> 00:02:51,222
No, there's no need.

28
00:02:51,432 --> 00:02:55,185
I'll do it. You get the food ready.
I'll wash the gabbeh.

29
00:02:55,391 --> 00:02:58,109
Let me do it.

30
00:02:58,310 --> 00:03:01,347
No, you've got sore feet.

31
00:03:01,549 --> 00:03:04,222
Give me the boots.

32
00:03:04,429 --> 00:03:06,623
No, I won't give them to you.

33
00:03:08,588 --> 00:03:11,977
You see to the food.
I'll see to the gabbeh.

34
00:03:12,187 --> 00:03:15,144
I'm not a woman
who serves in the kitchen!

35
00:03:32,782 --> 00:03:35,215
Will you let me wash it
or won't you?

36
00:03:37,780 --> 00:03:41,135
Little darling, precious darling,

37
00:03:41,859 --> 00:03:45,009
if you don't wash it, who will?

38
00:03:46,738 --> 00:03:50,013
My precious gabbeh,
beautiful gabbeh,

39
00:03:50,657 --> 00:03:52,647
how pretty you are!

40
00:03:53,056 --> 00:03:55,854
Why have you turned blue?

41
00:03:56,256 --> 00:03:59,292
Who is this horseman
who is carrying off this girl?

42
00:03:59,495 --> 00:04:01,962
Tell me who wove you.

43
00:04:04,773 --> 00:04:08,083
You are like the full moon!

44
00:04:22,209 --> 00:04:24,119
What is your name?

45
00:04:24,328 --> 00:04:25,807
Gabbeh.

46
00:04:33,326 --> 00:04:35,203
The water is so clear.

47
00:04:37,605 --> 00:04:39,163
Won't you wash me?

48
00:04:39,405 --> 00:04:42,714
Of course we want to wash you.

49
00:05:01,399 --> 00:05:04,674
May I lean on your young shoulders?

50
00:05:05,358 --> 00:05:08,076
I have no more strength.

51
00:05:11,316 --> 00:05:13,147
Of course you can.

52
00:05:19,314 --> 00:05:22,828
You look familiar.
What's your father's name?

53
00:05:23,273 --> 00:05:25,229
His name is Weave...

54
00:05:26,233 --> 00:05:28,621
Weave of the wool. There he is.

55
00:05:46,747 --> 00:05:48,021
<i>There's Father.</i>

56
00:05:50,826 --> 00:05:52,623
<i>We never stay
in the same place long.</i>

57
00:05:52,826 --> 00:05:56,659
<i>But if we do,
our father forces us to leave.</i>

58
00:05:56,985 --> 00:05:59,942
<i>One day, I fell in love
with a horseman.</i>

59
00:06:00,184 --> 00:06:03,858
<i>He had a strange voice,
as if he were but an illusion.</i>

60
00:06:04,063 --> 00:06:06,940
<i>He followed our tribe
like a shadow to take me away...</i>

61
00:06:09,901 --> 00:06:14,416
Were I younger,
I would ask for your hand.

62
00:06:14,660 --> 00:06:16,696
I think your father

63
00:06:16,980 --> 00:06:18,094
is a kind man.

64
00:06:18,299 --> 00:06:21,893
Appearances can be deceptive.
He has a wicked temper.

65
00:06:22,298 --> 00:06:25,176
So much for your father.
What about your mother?

66
00:06:25,378 --> 00:06:27,447
Is she kind and fair?

67
00:06:28,297 --> 00:06:31,527
No, in our family, we say
my father's bad temper

68
00:06:31,736 --> 00:06:33,930
is due to my mother's ugliness.

69
00:06:34,215 --> 00:06:35,409
There she is.

70
00:06:35,855 --> 00:06:38,812
<i>My mother's name is Sakineh.</i>

71
00:06:39,734 --> 00:06:41,610
<i>I am the eldest daughter.</i>

72
00:06:46,572 --> 00:06:49,847
Pass me the boots
so I can wash the gabbeh.

73
00:06:50,051 --> 00:06:53,122
Your feet are sore. They'll feel
worse if you get them wet.

74
00:06:53,330 --> 00:06:55,525
I'll wash it.

75
00:06:56,850 --> 00:07:00,364
See how beautiful she is.

76
00:07:00,569 --> 00:07:02,877
I am filled with wonder.

77
00:07:04,807 --> 00:07:07,685
As soon as he sees you,
he forgets me.

78
00:07:08,686 --> 00:07:13,805
Miss Gabbeh,
she is jealous of herself.

79
00:07:21,283 --> 00:07:23,239
Little girl, you who are so fair,

80
00:07:23,443 --> 00:07:25,194
do you not have a suitor?

81
00:07:25,442 --> 00:07:27,592
<i>You were my suitor</i>

82
00:07:27,802 --> 00:07:30,031
when you were younger.

83
00:07:32,920 --> 00:07:36,514
How foolish I was!

84
00:07:40,998 --> 00:07:43,876
See how he breaks my heart!

85
00:07:49,836 --> 00:07:51,792
Do you not have a suitor?

86
00:08:05,832 --> 00:08:08,789
Why does his voice
sound like a wolf's?

87
00:08:10,191 --> 00:08:12,340
It's a secret between him and me.

88
00:08:12,790 --> 00:08:15,986
He says: "I can't bear it.
Why don't you come?"

89
00:08:25,667 --> 00:08:27,497
Do you love him?

90
00:08:27,866 --> 00:08:30,300
Why don't you run away with him?

91
00:08:31,465 --> 00:08:34,218
Because of my father. He'd kill me.

92
00:08:34,785 --> 00:08:38,857
If he had done so, I would be free.

93
00:08:48,781 --> 00:08:50,100
He calls. Should I go?

94
00:08:50,701 --> 00:08:53,692
Don't go or your father
will kill you.

95
00:08:54,180 --> 00:08:56,135
Talk to him about it first.

96
00:08:56,419 --> 00:08:58,011
<i>Grandma's ill.</i>

97
00:08:58,219 --> 00:09:00,732
<i>When Uncle returns
he'll take her to a doctor.</i>

98
00:09:00,938 --> 00:09:05,886
<i>Father said I could only marry
when my uncle comes back from town.</i>

99
00:09:06,297 --> 00:09:09,845
<i>But my suitor is becoming impatient.</i>

100
00:09:26,531 --> 00:09:28,647
- Once five?
- Five.

101
00:09:28,851 --> 00:09:30,806
- Two fives?
- Ten.

102
00:09:31,010 --> 00:09:32,887
- Three fives?
- Fifteen.

103
00:09:33,090 --> 00:09:35,000
- Four fives?
- Twenty.

104
00:09:35,209 --> 00:09:37,245
- Five fives?
- Twenty-five.

105
00:09:37,449 --> 00:09:39,279
Remain seated.

106
00:09:39,528 --> 00:09:41,166
Stand up.

107
00:09:42,447 --> 00:09:45,007
May I?

108
00:09:47,166 --> 00:09:49,282
- What is your name?
- Mohsen Jahanpour.

109
00:09:49,486 --> 00:09:50,918
- And you?
- Tahmineh.

110
00:09:51,565 --> 00:09:53,283
What kind of school is this?

111
00:09:53,764 --> 00:09:55,959
- An itinerant one.
- Where's Fars?

112
00:09:56,204 --> 00:09:57,353
In Iran.

113
00:10:15,599 --> 00:10:18,590
Well, well,
a class without a teacher...

114
00:10:18,798 --> 00:10:20,629
Let's make the most of it.

115
00:10:38,993 --> 00:10:41,506
Now repeat after me:

116
00:10:41,712 --> 00:10:44,748
"R.A.N.G."

117
00:10:44,951 --> 00:10:46,350
What does that mean?

118
00:10:46,591 --> 00:10:47,865
Color.

119
00:10:48,071 --> 00:10:49,389
Bravo.

120
00:10:49,590 --> 00:10:51,467
Now tell me

121
00:10:52,429 --> 00:10:54,181
what color that is.

122
00:10:54,389 --> 00:10:55,868
Red.

123
00:10:58,508 --> 00:11:00,657
The red of a poppy.

124
00:11:01,987 --> 00:11:04,500
Now tell me what color that is.

125
00:11:05,546 --> 00:11:07,059
Yellow.

126
00:11:08,865 --> 00:11:12,459
The yellow of a wheat field.

127
00:11:12,824 --> 00:11:14,621
And that?

128
00:11:15,864 --> 00:11:17,581
Blue.

129
00:11:19,143 --> 00:11:23,340
The blue of God's heaven.

130
00:11:24,421 --> 00:11:25,820
And that?

131
00:11:27,461 --> 00:11:29,098
Blue.

132
00:11:32,299 --> 00:11:36,007
The shimmering blue of the seas.

133
00:11:36,658 --> 00:11:38,569
And that?

134
00:11:40,857 --> 00:11:42,256
Yellow.

135
00:11:44,696 --> 00:11:48,051
The yellow of the sun
which lights up the world.

136
00:11:48,775 --> 00:11:50,652
The yellow of the sun

137
00:11:50,855 --> 00:11:53,493
and the blue of the water
make the green of plants.

138
00:11:54,894 --> 00:11:56,292
Green.

139
00:11:57,213 --> 00:11:59,123
Pure green.

140
00:12:00,932 --> 00:12:05,049
Yellow and red
together in the sun...

141
00:13:25,630 --> 00:13:28,508
<i>Spring came,
but my uncle still did not come.</i>

142
00:13:28,710 --> 00:13:32,099
<i>In the spring, the whole tribe
broke camp, except for our clan.</i>

143
00:13:32,309 --> 00:13:35,300
<i>My father was waiting
for my uncle to return</i>

144
00:13:35,508 --> 00:13:37,816
<i>and take Grandma into town.</i>

145
00:13:38,027 --> 00:13:41,462
<i>She was ill,
but did not want to die.</i>

146
00:13:41,666 --> 00:13:44,134
<i>She wanted to see Uncle first,</i>

147
00:13:44,346 --> 00:13:47,576
<i>but he came too late.</i>

148
00:13:47,785 --> 00:13:49,297
<i>Father</i>

149
00:13:49,504 --> 00:13:54,179
<i>buried Grandma
in a lush cemetery.</i>

150
00:14:01,741 --> 00:14:03,413
Hey there!

151
00:14:25,135 --> 00:14:27,808
- Hello, Sakineh. How are you?
- Hello, sir.

152
00:14:28,015 --> 00:14:29,845
Don't you recognize me?

153
00:14:31,854 --> 00:14:33,764
- No.
- You don't recognize me?

154
00:14:35,213 --> 00:14:36,440
And now?

155
00:14:37,972 --> 00:14:39,963
You are my husband's brother.

156
00:14:40,291 --> 00:14:41,770
Good.

157
00:14:41,971 --> 00:14:44,245
Why didn't you come with your wife?

158
00:14:44,850 --> 00:14:47,080
I am still young.
Who would marry me?

159
00:14:47,410 --> 00:14:49,684
Hello, Zinat. How are you?

160
00:14:49,889 --> 00:14:53,244
You recognized me
without my hat on!

161
00:14:54,728 --> 00:14:58,083
Miss Gabbeh, your uncle is here.

162
00:14:58,287 --> 00:15:00,402
The wedding day is nearing.

163
00:15:00,686 --> 00:15:02,563
How are you, Sakineh?

164
00:15:02,846 --> 00:15:05,439
And the children?
How many do you have?

165
00:15:05,645 --> 00:15:08,033
Seven. Come on, children.
Your uncle's here.

166
00:15:08,444 --> 00:15:10,878
Zinat, are you well?

167
00:15:13,083 --> 00:15:15,039
- Hello.
- Hello, Uncle.

168
00:15:19,561 --> 00:15:22,029
How you've grown!

169
00:15:23,960 --> 00:15:28,635
Tell your uncle
to put in a good word for you.

170
00:15:29,359 --> 00:15:32,236
My uncle?
He doesn't even remember me.

171
00:15:34,678 --> 00:15:36,793
He doesn't even know
if he's my uncle

172
00:15:36,997 --> 00:15:39,305
on my mother's or my father's side!

173
00:15:42,476 --> 00:15:43,874
Is this your child?

174
00:15:44,155 --> 00:15:47,066
You have been away so long
you no longer recognize them.

175
00:15:47,314 --> 00:15:50,192
You mock me. I shall show you.

176
00:15:51,393 --> 00:15:53,304
Zinat, go over there.

177
00:15:53,553 --> 00:15:55,349
Sakineh, go the other side.

178
00:15:55,832 --> 00:15:57,504
Soraya, go to your mother.

179
00:16:00,591 --> 00:16:02,740
Fatomeh, go over there.

180
00:16:03,310 --> 00:16:06,984
Reza, you go there too.
Go with Zinat.

181
00:16:07,909 --> 00:16:10,139
You look like my brother.

182
00:16:11,908 --> 00:16:14,262
- So did I get it right?
- No!

183
00:16:14,627 --> 00:16:16,618
Well, where did I go wrong?

184
00:16:18,906 --> 00:16:21,659
<i>Our uncle brought us together
under the tree.</i>

185
00:16:21,866 --> 00:16:24,743
<i>This tree is the symbol
of our family.</i>

186
00:16:25,065 --> 00:16:30,091
<i>A new branch sprouts
with each new birth.</i>

187
00:16:30,503 --> 00:16:33,575
<i>When one of us dies,</i>

188
00:16:33,862 --> 00:16:37,059
<i>a branch is cut off.</i>

189
00:16:37,302 --> 00:16:40,657
<i>Grandmother recognized each child</i>

190
00:16:40,861 --> 00:16:43,328
<i>by the branches of the tree.</i>

191
00:16:47,259 --> 00:16:50,568
See? Uncle didn't ask after me.

192
00:16:51,338 --> 00:16:53,567
He didn't ask where Gabbeh was.

193
00:16:54,697 --> 00:16:56,767
He only came back for his mother.

194
00:16:56,977 --> 00:16:59,695
<i>Where's my mother? Where's Narendj?</i>

195
00:17:01,455 --> 00:17:02,774
Mother!

196
00:17:04,335 --> 00:17:06,450
Mother! Narendj!

197
00:17:07,294 --> 00:17:10,171
Your Abbas is back.

198
00:17:10,453 --> 00:17:12,205
Where are you, Mom?

199
00:17:22,050 --> 00:17:24,564
Too late.
Your guardian angel has passed away.

200
00:17:26,889 --> 00:17:29,607
Too late.
Your guardian angel has passed away.

201
00:17:36,846 --> 00:17:39,565
She wanted to finish this gabbeh
for your wedding

202
00:17:39,966 --> 00:17:41,842
and send it to you in town.

203
00:19:50,452 --> 00:19:52,488
It is time for the wedding.

204
00:19:53,052 --> 00:19:56,248
Alas, not mine. My uncle's.

205
00:19:59,450 --> 00:20:03,601
My father said:
"Your uncle still isn't married.

206
00:20:04,289 --> 00:20:07,485
"It's his turn first,
then it'll be yours."

207
00:20:15,686 --> 00:20:17,438
Congratulations.

208
00:20:17,765 --> 00:20:19,835
Uncle, please get married.

209
00:20:21,884 --> 00:20:25,671
The yellow of the wheat field.

210
00:20:33,042 --> 00:20:35,350
Children, what is that sound?

211
00:20:36,201 --> 00:20:37,838
A sparrow.

212
00:20:43,079 --> 00:20:45,273
And what can you hear over there?

213
00:20:45,598 --> 00:20:46,747
A sparrow.

214
00:20:46,998 --> 00:20:50,785
Old man, let it fly away.
So my uncle gets married quickly.

215
00:21:02,234 --> 00:21:03,508
The sparrow?

216
00:21:03,794 --> 00:21:05,272
The singing bird.

217
00:21:05,473 --> 00:21:07,429
The singing bird.

218
00:21:07,633 --> 00:21:09,827
Thanks to the yellow,

219
00:21:12,471 --> 00:21:15,861
the sparrow has become a canary.

220
00:22:53,566 --> 00:22:55,362
Anyone there?

221
00:22:55,605 --> 00:22:59,995
<i>My uncle dreamed he would meet
his wife near a spring.</i>

222
00:23:00,284 --> 00:23:02,922
<i>A girl who could sing
like a canary.</i>

223
00:23:03,123 --> 00:23:06,160
<i>My father looked for a wife
for my uncle</i>

224
00:23:06,362 --> 00:23:08,239
<i>in each and every clan.</i>

225
00:23:08,562 --> 00:23:10,438
<i>The girls were all pretty,</i>

226
00:23:10,641 --> 00:23:13,758
<i>but none sang like a canary.</i>

227
00:23:13,961 --> 00:23:17,555
<i>Uncle said he would find
his wife near a spring.</i>

228
00:23:18,799 --> 00:23:21,233
<i>My uncle saw this spring
in a dream.</i>

229
00:23:21,439 --> 00:23:23,906
<i>Our tribe stopped at every spring.</i>

230
00:23:24,158 --> 00:23:25,988
<i>But we never met a girl</i>

231
00:23:26,237 --> 00:23:27,795
<i>who sang like a canary.</i>

232
00:23:28,517 --> 00:23:29,586
Where is the spring?

233
00:23:31,476 --> 00:23:33,626
Where you hear the sound of water.

234
00:24:15,905 --> 00:24:17,223
Hello.

235
00:24:17,464 --> 00:24:18,817
Hello.

236
00:24:20,783 --> 00:24:22,694
What is your name?

237
00:24:23,783 --> 00:24:25,818
I am the daughter of Aladad.

238
00:24:35,580 --> 00:24:37,410
I was looking for water,

239
00:24:37,899 --> 00:24:41,652
but I found singing.
What a beautiful poem.

240
00:24:41,898 --> 00:24:44,969
- I'd never heard it before.
- I wrote it last night.

241
00:24:45,217 --> 00:24:48,811
- You're the first to hear it.
- When? Last night?

242
00:24:49,096 --> 00:24:50,814
That's right.

243
00:24:51,016 --> 00:24:52,892
- So you wrote it?
- Of course.

244
00:24:53,095 --> 00:24:55,893
Let me help with the dishes.

245
00:24:56,654 --> 00:24:58,326
Are you a poet?

246
00:24:58,534 --> 00:25:00,808
No, I'm Aladad's daughter.

247
00:25:01,013 --> 00:25:03,686
I'd never heard this poem before.
Tell me it again.

248
00:25:06,852 --> 00:25:09,081
(Turkish dialect)

249
00:25:12,690 --> 00:25:14,920
Bravo. But tell me it in Persian.

250
00:25:16,209 --> 00:25:19,406
<i>Above the spring I stand
Below the spring I stand</i>

251
00:25:19,648 --> 00:25:22,321
<i>The pebble in the spring I am</i>

252
00:25:22,648 --> 00:25:24,877
<i>When my beloved passes by...</i>

253
00:25:26,447 --> 00:25:30,200
<i>The canary he holds I am
I am in three parts...</i>

254
00:25:30,806 --> 00:25:32,363
Did you write it?

255
00:25:32,565 --> 00:25:33,759
Yes.

256
00:25:34,525 --> 00:25:35,752
Bravo.

257
00:25:36,004 --> 00:25:39,076
Who did you write it for?
For your beloved?

258
00:25:39,283 --> 00:25:42,081
No, for myself.
I have no beloved.

259
00:25:43,642 --> 00:25:45,997
Aren't you married?

260
00:25:46,882 --> 00:25:48,200
No.

261
00:25:49,961 --> 00:25:52,076
And why's that?

262
00:25:53,200 --> 00:25:54,474
Well...

263
00:25:57,199 --> 00:25:59,393
Time flies by.
How old are you?

264
00:26:10,835 --> 00:26:13,269
- Have you come from town?
- Yes.

265
00:26:16,634 --> 00:26:19,022
If you had a suitor,
would you get married?

266
00:26:23,272 --> 00:26:27,150
I'll have to wait and see
what fate has in store for me.

267
00:26:28,991 --> 00:26:30,582
Supposing I...

268
00:26:37,429 --> 00:26:39,623
How bold he is!

269
00:26:43,347 --> 00:26:47,942
At his age...
There's no fool like an old fool!

270
00:26:50,625 --> 00:26:52,104
<i>She's stolen his heart.</i>

271
00:26:53,745 --> 00:26:56,463
Quiet.
Let them get married quickly.

272
00:26:58,623 --> 00:27:01,978
If you get angry at me,
what will you do?

273
00:27:02,182 --> 00:27:03,581
Huddle up

274
00:27:03,822 --> 00:27:05,812
and recite poems.

275
00:27:09,820 --> 00:27:11,094
Like what?

276
00:27:11,620 --> 00:27:14,452
<i>I am thirsty, you are pure water</i>

277
00:27:15,219 --> 00:27:17,016
<i>I am thirsty, you are pure water</i>

278
00:27:19,578 --> 00:27:22,535
<i>I am weary, you are energy</i>

279
00:27:23,177 --> 00:27:26,725
<i>I am tired and old and withered</i>

280
00:27:27,536 --> 00:27:30,333
<i>You are youth, you are a bud</i>

281
00:27:30,575 --> 00:27:34,362
<i>I am thirsty, you are pure water</i>

282
00:27:36,973 --> 00:27:41,648
<i>I am weary, you are energy...</i>

283
00:27:43,852 --> 00:27:45,170
Yes...

284
00:27:46,691 --> 00:27:48,329
Yes...

285
00:27:49,450 --> 00:27:50,280
Yes.

286
00:27:50,530 --> 00:27:52,407
What does that mean?

287
00:27:54,609 --> 00:27:58,157
I liked your poem.
I'll marry you.

288
00:28:01,647 --> 00:28:04,115
Where were you?
The child is thirsty.

289
00:28:07,686 --> 00:28:09,960
I'm sorry. Fill this up.

290
00:28:10,165 --> 00:28:12,724
I shall help this young lady.

291
00:28:33,039 --> 00:28:34,836
Be patient.

292
00:28:35,039 --> 00:28:37,791
Just while the water skin's
being filled

293
00:28:37,998 --> 00:28:40,033
It's taking for ever.

294
00:28:40,237 --> 00:28:42,512
No, you're not patient enough.

295
00:28:52,594 --> 00:28:56,666
I was in love once - I understand
your suffering. Let me help.

296
00:29:29,985 --> 00:29:33,340
I asked for the hand
of Aladad's daughter.

297
00:29:33,544 --> 00:29:36,057
And here's some sugar to celebrate.

298
00:32:47,574 --> 00:32:49,371
I must gather the wool

299
00:32:49,574 --> 00:32:52,167
or else my uncle's wedding
will be delayed.

300
00:33:15,087 --> 00:33:17,317
Aladad's daughter,
did you weave this gabbeh?

301
00:33:17,527 --> 00:33:20,404
Of course.
Who else could have done it?

302
00:33:22,885 --> 00:33:24,716
Why is there this horseman?

303
00:33:24,925 --> 00:33:26,755
The bride is taken away
by a horseman.

304
00:33:26,964 --> 00:33:30,512
Yes, but not
if the bridegroom is old.

305
00:33:30,963 --> 00:33:32,794
That's fine for young couples.

306
00:33:34,962 --> 00:33:38,078
<i>I hear the call of old age</i>

307
00:33:38,281 --> 00:33:40,749
<i>when I see my hair is grey...</i>

308
00:33:41,121 --> 00:33:44,715
Come to the wedding celebrations.
Everyone's tired of waiting.

309
00:33:44,960 --> 00:33:48,508
I'm coming. Don't do anything
to make me regret this wedding.

310
00:33:51,198 --> 00:33:53,473
<i>Looking is not seeing</i>

311
00:33:53,678 --> 00:33:56,669
<i>Behind every cradle
hides a tomb</i>

312
00:33:56,877 --> 00:33:59,265
<i>Behind every joy, sadness...</i>

313
00:33:59,516 --> 00:34:02,075
Why did you recite a poem?
Are you angry with me?

314
00:34:02,275 --> 00:34:03,105
No.

315
00:34:03,355 --> 00:34:05,822
I am happy and contented.

316
00:34:06,834 --> 00:34:10,906
<i>But...
Fifty-seven years have gone by</i>

317
00:34:11,193 --> 00:34:13,911
<i>Alas! Gone by so quickly...</i>

318
00:34:18,271 --> 00:34:19,670
Do not be sad.

319
00:34:19,911 --> 00:34:22,629
<i>Although my body grows old</i>

320
00:34:22,870 --> 00:34:25,986
<i>and my hair is turning grey,
my heart is full of desire</i>

321
00:34:26,789 --> 00:34:28,108
<i>and hope</i>

322
00:34:28,309 --> 00:34:31,778
<i>My body is like a cold,
silent dungeon</i>

323
00:34:32,708 --> 00:34:35,426
<i>But my heart
is like a happy child...</i>

324
00:36:09,963 --> 00:36:12,795
<i>My uncle's wedding
is woven into the gabbeh.</i>

325
00:36:13,082 --> 00:36:16,756
<i>Our clan left
my uncle and his wife</i>

326
00:36:16,961 --> 00:36:20,270
<i>by the river bank
to have their honeymoon.</i>

327
00:37:22,704 --> 00:37:26,378
Why are you crying, Miss Gabbeh?

328
00:37:26,943 --> 00:37:29,173
Because I still have to wait.

329
00:37:31,062 --> 00:37:33,257
Father said
Mother must first give birth.

330
00:37:33,461 --> 00:37:35,179
After that, I can get married.

331
00:37:36,541 --> 00:37:41,613
But your father said you could
after your uncle's wedding!

332
00:37:44,139 --> 00:37:46,447
Now he says
after Mother's given birth.

333
00:37:46,938 --> 00:37:49,849
When is she due to give birth?

334
00:37:52,737 --> 00:37:54,806
When we've broken camp,

335
00:37:55,616 --> 00:37:59,210
when we've walked a while,
when we've worked a while,

336
00:38:00,055 --> 00:38:02,443
when we've crossed the river...

337
00:38:50,802 --> 00:38:55,397
<i>The women blew into the water skins
to inflate them.</i>

338
00:38:56,120 --> 00:38:58,759
<i>The men tied them to the rafts</i>

339
00:38:58,960 --> 00:39:02,508
<i>and the boys put
the animals on the rafts</i>

340
00:39:03,039 --> 00:39:05,552
<i>so the water
would not wash them away.</i>

341
00:39:05,798 --> 00:39:07,231
<i>We had a flock of ewes.</i>

342
00:39:07,518 --> 00:39:10,315
<i>My mother worked
harder than the others,</i>

343
00:39:10,517 --> 00:39:13,030
<i>but there was still
no sign of the birth.</i>

344
00:39:39,349 --> 00:39:40,464
<i>We crossed</i>

345
00:39:40,709 --> 00:39:44,303
<i>a freshwater river
and two saltwater rivers...</i>

346
00:39:44,548 --> 00:39:47,300
<i>but still my mother
did not give birth.</i>

347
00:40:20,459 --> 00:40:22,051
Uncle!

348
00:40:24,378 --> 00:40:26,254
The hen's laid an egg!

349
00:40:33,896 --> 00:40:35,487
It's time.

350
00:40:36,735 --> 00:40:38,372
It's time!

351
00:40:39,974 --> 00:40:41,771
Uncle, stop everything.

352
00:40:41,973 --> 00:40:44,168
Stop, I say!

353
00:41:40,519 --> 00:41:41,997
<i>I am a stranger.</i>

354
00:41:42,238 --> 00:41:46,025
Why's that man stopping?
He should go away.

355
00:41:49,876 --> 00:41:52,185
Life is color!

356
00:42:09,191 --> 00:42:11,421
Love is color!

357
00:42:11,671 --> 00:42:13,900
Man is color!

358
00:42:16,349 --> 00:42:18,146
Woman is color!

359
00:42:18,709 --> 00:42:20,664
Love is color!

360
00:42:29,546 --> 00:42:30,820
Child is color!

361
00:42:32,425 --> 00:42:34,336
<i>Love is pain!</i>

362
00:43:00,858 --> 00:43:04,850
You never gave me a child.

363
00:43:05,057 --> 00:43:07,809
I'd like to have a child.

364
00:43:09,416 --> 00:43:11,929
How beautiful!

365
00:43:15,734 --> 00:43:17,247
I'm leaving for good.

366
00:43:18,734 --> 00:43:22,203
I don't give a damn!

367
00:43:26,771 --> 00:43:28,602
The old woman has left.

368
00:43:28,811 --> 00:43:31,802
Do you want to run away with me?

369
00:43:32,010 --> 00:43:33,363
Father would kill us.

370
00:43:33,570 --> 00:43:35,321
You're lying.

371
00:43:35,529 --> 00:43:38,804
God doesn't forgive liars.

372
00:43:39,008 --> 00:43:41,886
Tell me the truth.

373
00:43:42,607 --> 00:43:45,565
Don't lie.

374
00:43:47,646 --> 00:43:50,637
You don't love me. You're lying.

375
00:43:51,085 --> 00:43:53,360
I'm not lying. I love you.

376
00:43:53,965 --> 00:43:56,797
It's not true.

377
00:43:57,004 --> 00:43:59,563
I know you're lying.

378
00:44:00,443 --> 00:44:02,797
Here's a baby. Stop complaining!

379
00:44:03,242 --> 00:44:05,710
I'm sure you're lying.

380
00:44:05,921 --> 00:44:08,674
Your father won't see.

381
00:44:17,119 --> 00:44:20,554
What a beautiful baby he is.

382
00:44:21,238 --> 00:44:24,070
Has he drunk some milk?

383
00:44:24,757 --> 00:44:25,871
No.

384
00:44:26,396 --> 00:44:27,909
He hasn't?

385
00:44:28,396 --> 00:44:31,068
Go get some milk.

386
00:44:43,192 --> 00:44:45,182
Go and milk his mother.

387
00:44:46,591 --> 00:44:48,309
He's hungry.

388
00:44:52,910 --> 00:44:55,662
Give him his mother's milk.

389
00:45:05,346 --> 00:45:07,064
Sakineh, the baby's hungry.

390
00:46:05,291 --> 00:46:08,088
Life is color!

391
00:46:09,850 --> 00:46:11,567
Death is...

392
00:47:07,035 --> 00:47:09,151
<i>You told me to wash the gabbeh</i>

393
00:47:09,395 --> 00:47:12,625
<i>to feel better.
So why are you crying?</i>

394
00:47:13,234 --> 00:47:15,303
Because I still must wait.

395
00:47:16,193 --> 00:47:18,990
My father and my uncle aren't here.

396
00:47:19,832 --> 00:47:21,550
Nor is my mother.

397
00:47:21,872 --> 00:47:25,147
They have taken my sister-in-law
to see a doctor in town.

398
00:47:26,390 --> 00:47:29,143
I am to look after
the children and the animals.

399
00:47:31,069 --> 00:47:33,537
One of the sheep has a chill.

400
00:47:34,228 --> 00:47:36,423
My sister Sholeh has disappeared.

401
00:48:10,259 --> 00:48:11,817
Don't run off.

402
00:48:38,612 --> 00:48:40,364
Mind, you'll fall!

403
00:48:53,128 --> 00:48:56,006
Mom! Uncle! I'm going to fall...

404
00:49:03,926 --> 00:49:05,517
Uncle!

405
00:49:15,643 --> 00:49:16,597
Dad!

406
00:49:17,282 --> 00:49:18,635
Sholeh!

407
00:50:13,468 --> 00:50:15,139
It's my fault.

408
00:53:26,099 --> 00:53:28,817
Why did you stop? Go on!

409
00:54:34,961 --> 00:54:37,793
Why did you stop? Go on, move!

410
00:56:00,499 --> 00:56:03,297
Go the other way.

411
00:59:31,846 --> 00:59:35,679
<i>By day, the girls kept watch
over me, by night, the men did.</i>

412
00:59:35,925 --> 00:59:38,802
<i>I had no chance to run away.</i>

413
00:59:39,004 --> 00:59:41,676
<i>The day Grandmother's gabbeh
was finished,</i>

414
00:59:42,203 --> 00:59:46,753
<i>my uncle told me I should run away.</i>

415
00:59:47,402 --> 00:59:49,790
<i>He said he'd take
my father on one side</i>

416
00:59:50,001 --> 00:59:52,036
<i>so he wouldn't see me leave.</i>

417
00:59:53,000 --> 00:59:56,674
O mother, your gabbeh is finished.

418
00:59:58,799 --> 01:00:01,676
I'd like to sleep on it
and never get up again.

419
01:00:01,918 --> 01:00:05,751
<i>That's when I had a chance
of escape, but I didn't dare.</i>

420
01:00:06,117 --> 01:00:10,029
Why won't you come?
Tell me why!

421
01:00:12,835 --> 01:00:16,270
Why do you hurt me?

422
01:00:16,474 --> 01:00:18,783
Why don't we run away together?

423
01:00:18,994 --> 01:00:23,304
Come on. Don't make me suffer.

424
01:00:24,032 --> 01:00:28,661
Your father isn't here.

425
01:00:32,110 --> 01:00:34,704
You've made me so unhappy.

426
01:00:34,909 --> 01:00:37,662
You've made me lose my mind.

427
01:00:38,429 --> 01:00:41,977
Why won't you listen to me?

428
01:00:42,707 --> 01:00:45,426
Stop making me angry!

429
01:01:11,260 --> 01:01:13,490
Because of you,

430
01:01:13,740 --> 01:01:16,890
I am like a suffering soul.

431
01:01:17,539 --> 01:01:20,006
Why won't you listen to me?

432
01:01:20,298 --> 01:01:21,811
Do you not love me?

433
01:01:22,017 --> 01:01:24,929
You do not love me!

434
01:01:35,614 --> 01:01:39,162
You do not love me.

435
01:01:39,653 --> 01:01:41,484
I brought you some apples.

436
01:01:41,692 --> 01:01:43,808
Why don't you eat them?

437
01:01:48,091 --> 01:01:50,046
You don't love me.

438
01:05:46,230 --> 01:05:47,709
He killed them!

439
01:05:49,349 --> 01:05:50,907
He killed them!

440
01:07:00,491 --> 01:07:02,447
Miss Gabbeh,

441
01:07:02,731 --> 01:07:05,961
will you help me
wash the gabbeh tomorrow?

442
01:07:06,170 --> 01:07:09,161
I can't do it. My feet hurt.

443
01:07:16,967 --> 01:07:19,036
Miss Gabbeh,

444
01:07:19,606 --> 01:07:23,564
do what you want with me,
but don't break my heart.

445
01:07:31,643 --> 01:07:35,032
The old woman has left. Don't lie.

446
01:07:35,322 --> 01:07:39,234
Let's leave together.
How many times must I beg you?

447
01:07:39,521 --> 01:07:42,081
You don't love me.

448
01:07:46,519 --> 01:07:48,794
You're a liar...

449
01:07:51,478 --> 01:07:55,356
You don't love me.
Tell me the truth.

450
01:07:55,557 --> 01:07:57,912
<i>My father didn't really kill us.</i>

451
01:07:58,396 --> 01:08:01,149
<i>It was only a rumor.
He just said it</i>

452
01:08:01,676 --> 01:08:04,314
<i>so that my sisters wouldn't run away</i>

453
01:08:04,555 --> 01:08:07,466
<i>and so that they never answered
the wolf's call.</i>

454
01:08:07,674 --> 01:08:10,471
<i>That is why, for forty years now,</i>

455
01:08:10,833 --> 01:08:15,348
<i>no one has heard
the canary's song near a spring.</i>

456
01:08:27,189 --> 01:08:30,021
Subtitles:
J. Miller and S. Hovsepian

457
01:09:11,658 --> 01:09:16,094
Written, directed and edited by
Mohsen MAKHMALBAF

458
01:09:18,536 --> 01:09:21,049
Subtitles processed by
C.M.C. - Paris

459
01:09:21,575 --> 01:09:23,805
With the participation of the
French Ministry of Culture

460
01:09:24,015 --> 01:09:25,515
and the Ministry of Foreign Affairs

461
01:09:26,000 --> 01:09:27,500
www.irib.ir

462
01:09:28,000 --> 01:09:30,000
www.iranianmovies.org