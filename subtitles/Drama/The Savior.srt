1
00:01:29,584 --> 00:01:31,779
Christian, how are you, son?

2
00:01:32,024 --> 00:01:34,618
Good. You missed mass, Papa.

3
00:01:35,304 --> 00:01:37,260
I don't go for that religious stuff.

4
00:01:37,504 --> 00:01:39,062
Don't you believe in God?

5
00:01:40,703 --> 00:01:43,536
You don't have to go to church
to believe in God.

6
00:01:43,783 --> 00:01:48,174
God is everywhere. He's inside you.
He's in me. He's in Mommy.

7
00:01:48,423 --> 00:01:49,936
Even in bad people?

8
00:01:51,503 --> 00:01:53,653
Yeah, even in bad people.

9
00:01:56,103 --> 00:01:57,058
So what's with you?

10
00:01:57,623 --> 00:01:58,612
Nothing.

11
00:01:59,822 --> 00:02:01,141
Always late!

12
00:02:01,822 --> 00:02:04,131
Ambassador called.
I'm sorry. We can still go.

13
00:02:04,502 --> 00:02:06,140
-Where we going?
-The movies.

14
00:02:06,382 --> 00:02:07,610
The movies?

15
00:02:09,782 --> 00:02:11,420
Which one, huh?

16
00:02:12,022 --> 00:02:15,901
Go get us a paper, and while you're
at it, get yourself a woman.

17
00:02:19,141 --> 00:02:21,052
We'll make it, just like I promised.

18
00:02:21,621 --> 00:02:24,340
You promise him a lot of things.

19
00:02:24,541 --> 00:02:25,656
I have to work.

20
00:02:28,421 --> 00:02:30,093
Let's just have fun, huh?

21
00:02:36,981 --> 00:02:38,698
Here's your newspaper, Papa.

22
00:02:39,180 --> 00:02:41,489
And the woman said
she needed more money.

23
00:02:43,940 --> 00:02:45,373
Hey, Chris!

24
00:02:45,820 --> 00:02:46,935
Peter!

25
00:02:47,740 --> 00:02:48,775
-How are you?
-Good.

26
00:02:49,020 --> 00:02:50,214
Good to see you.

27
00:02:51,540 --> 00:02:52,734
Maria.

28
00:02:53,820 --> 00:02:55,538
So what are you guys up to?

29
00:02:55,740 --> 00:02:57,252
Going to the movies.

30
00:02:59,339 --> 00:03:03,127
Don't get mad now, but I gotta borrow
your father for a couple of hours.

31
00:03:03,419 --> 00:03:06,968
-What?
-We gotta get back. We got an alert.

32
00:03:11,339 --> 00:03:14,570
It's a bomb threat.
Some Muslim fundamentalist group.

33
00:03:14,819 --> 00:03:15,934
Again?

34
00:03:19,578 --> 00:03:20,897
Shit.

35
00:03:21,578 --> 00:03:23,330
Well, colonel's orders.

36
00:03:25,698 --> 00:03:26,528
Behave.

37
00:03:26,778 --> 00:03:27,927
All right.

38
00:03:35,218 --> 00:03:39,574
I'm really sorry I can't take you
to the movies. We'll go tomorrow.

39
00:03:39,817 --> 00:03:42,285
No, you can't do that.
You promised him.

40
00:03:42,497 --> 00:03:44,772
It's an emergency. I have to go.

41
00:03:47,137 --> 00:03:47,933
Why?

42
00:03:48,137 --> 00:03:49,252
I have to.

43
00:03:58,136 --> 00:03:59,330
I'm really sorry.

44
00:04:00,216 --> 00:04:01,854
Take care.
See you later.

45
00:04:16,815 --> 00:04:17,804
Come here.

46
00:04:28,735 --> 00:04:30,771
Maybe you should take a week off.

47
00:04:30,975 --> 00:04:32,124
I could use it.

48
00:04:32,335 --> 00:04:33,768
I know she could use it.

49
00:04:51,134 --> 00:04:52,328
Call for ambulances!

50
00:04:52,574 --> 00:04:53,689
Yes, sir!

51
00:04:57,373 --> 00:04:58,522
Maria!

52
00:04:58,733 --> 00:04:59,848
Maria!

53
00:05:09,133 --> 00:05:11,772
Oh, God! God! God!

54
00:05:12,733 --> 00:05:14,165
Somebody! Help!

55
00:05:14,612 --> 00:05:15,761
Christian!

56
00:05:17,892 --> 00:05:19,564
Help! Somebody! Help!

57
00:05:19,812 --> 00:05:20,688
God!

58
00:05:35,331 --> 00:05:36,320
All right, Josh.

59
00:05:37,011 --> 00:05:39,127
The plane's ready
for transport to Dover.

60
00:05:39,331 --> 00:05:40,241
May we?

61
00:05:46,371 --> 00:05:47,929
We'll get them, Josh.

62
00:05:48,411 --> 00:05:49,810
We'll track them down.

63
00:05:50,971 --> 00:05:52,926
"Track them down"? Bullshit.

64
00:05:53,370 --> 00:05:55,964
Just walk down the street
to the nearest mosque.

65
00:05:56,570 --> 00:05:59,130
Look, Josh. I know this
is a bad time for you.

66
00:05:59,810 --> 00:06:01,368
Let us investigate.

67
00:06:01,610 --> 00:06:04,727
We'll find these people.
They'll be brought to justice.

68
00:06:07,210 --> 00:06:08,928
I'll meet you at the airport.

69
00:06:11,769 --> 00:06:13,088
Carry on, sergeant.

70
00:06:24,489 --> 00:06:25,365
Where you going?

71
00:06:25,609 --> 00:06:26,564
For a walk.

72
00:08:19,883 --> 00:08:22,556
Gentlemen, you lost
your name, your passports...

73
00:08:25,203 --> 00:08:27,796
...your bank accounts,
your right to get married.

74
00:08:28,002 --> 00:08:30,118
But we will give you new name...

75
00:08:30,922 --> 00:08:33,959
...date of birth
and a second chance.

76
00:08:35,962 --> 00:08:37,315
Above all...

77
00:08:37,522 --> 00:08:38,557
...a family.

78
00:08:39,122 --> 00:08:41,397
Don't forget, gentlemen.

79
00:08:42,162 --> 00:08:43,277
A family!

80
00:08:44,282 --> 00:08:46,318
But if any of you doesn't function...

81
00:08:46,522 --> 00:08:49,513
...the whole family does not function.

82
00:08:50,001 --> 00:08:52,754
We fight for no country...

83
00:08:53,041 --> 00:08:54,156
...no flag...

84
00:08:54,881 --> 00:08:56,599
...or political boss.

85
00:08:58,161 --> 00:08:59,833
We fight for honour...

86
00:09:00,561 --> 00:09:02,552
...our fellow soldiers.

87
00:09:02,921 --> 00:09:07,198
We never leave our dead
or wounded behind.

88
00:09:09,800 --> 00:09:10,596
We...

89
00:09:11,120 --> 00:09:13,918
...respect our enemy.

90
00:09:18,000 --> 00:09:19,194
We fight...

91
00:09:20,200 --> 00:09:22,077
...anywhere in the world.

92
00:09:24,400 --> 00:09:26,038
We have no home.

93
00:09:26,799 --> 00:09:31,031
The Foreign Legion
is our home, gentlemen!

94
00:10:01,278 --> 00:10:03,269
Watch it. They look real dangerous.

95
00:10:03,958 --> 00:10:05,151
Scary.

96
00:10:08,477 --> 00:10:10,149
What a fucking job.

97
00:10:10,917 --> 00:10:12,908
I'm glad we're not re-enlisting.

98
00:10:13,117 --> 00:10:15,756
Six years in the Legion.
That's enough.

99
00:10:16,317 --> 00:10:17,591
But what we gonna do?

100
00:10:18,157 --> 00:10:19,590
I mean...

101
00:10:20,317 --> 00:10:23,707
...I'd like to find a war where
I fight for something I believe in.

102
00:10:27,276 --> 00:10:29,107
You wanna hate the guys you pop?

103
00:10:29,316 --> 00:10:31,352
Yeah, it'd be nice for a change.

104
00:11:58,192 --> 00:11:59,784
Look what I found, man.

105
00:12:00,112 --> 00:12:01,465
Good grass.

106
00:12:02,591 --> 00:12:05,628
Let's roll a huge bomber.

107
00:12:07,111 --> 00:12:08,305
Not for me.

108
00:12:09,031 --> 00:12:10,544
Well, I need one.

109
00:12:12,791 --> 00:12:14,349
This war sucks, man.

110
00:12:16,591 --> 00:12:18,149
Stop thinking about it.

111
00:12:20,151 --> 00:12:21,822
I'm getting out of here.

112
00:12:32,590 --> 00:12:35,821
What are you gonna do?
Sell used fucking cars...

113
00:12:36,750 --> 00:12:39,310
...until somebody finds out
who you are?

114
00:12:44,549 --> 00:12:46,107
Open a butcher shop.

115
00:12:49,749 --> 00:12:51,023
Don't do it.

116
00:12:51,669 --> 00:12:52,988
Just don't do it.

117
00:12:54,789 --> 00:12:57,701
Hey, you've done nothing wrong here.

118
00:12:57,949 --> 00:12:59,746
Well, it feels like I did.

119
00:12:59,989 --> 00:13:01,307
I'm getting out.

120
00:13:10,988 --> 00:13:12,706
I'll miss you, man.

121
00:13:53,986 --> 00:13:57,422
Hey, hey, hey, can you hear me?
Can you hear me?

122
00:13:57,786 --> 00:14:00,299
Don't you ever get bored up there?

123
00:14:04,345 --> 00:14:05,744
What do you want?

124
00:14:05,985 --> 00:14:08,453
Haven't you heard, man?
They called a truce.

125
00:14:08,665 --> 00:14:12,101
We have to go back to my village
to pick up prisoners for exchange.

126
00:14:13,705 --> 00:14:14,774
Exchange?

127
00:14:15,265 --> 00:14:16,903
Yeah, man, exchange with Muslims.

128
00:14:17,145 --> 00:14:20,580
They captured some of ours a year ago.
Now they wanna get rid of them.

129
00:14:22,944 --> 00:14:24,138
Give me a minute.

130
00:14:24,464 --> 00:14:26,694
Hurry up, man. Hurry up.

131
00:14:27,304 --> 00:14:31,297
I wanna take a look around here
for rats before we go to exchange.

132
00:14:31,544 --> 00:14:35,503
-I thought there was a truce.
-Don't be so naive, man.

133
00:14:35,744 --> 00:14:40,498
Truce is the best time to chase rats.
Just give me a hand. All right?

134
00:16:44,017 --> 00:16:45,132
Hey, Keith!

135
00:16:46,137 --> 00:16:47,968
Keith, everything okay?

136
00:16:48,617 --> 00:16:50,892
It's me, man. I'm coming.
Don't shoot!

137
00:16:56,376 --> 00:16:57,729
Anything for you?

138
00:17:04,936 --> 00:17:07,689
What's the matter with you?
You became emotional?

139
00:17:07,896 --> 00:17:09,124
Don't like the mess?

140
00:17:10,296 --> 00:17:11,615
Nice legs.

141
00:17:12,376 --> 00:17:14,445
Someone did our job already.

142
00:17:28,975 --> 00:17:31,773
You know what she said?
She said she likes you. You know?

143
00:17:32,295 --> 00:17:35,206
She said she needs a man like you
to make her feel young again.

144
00:17:35,454 --> 00:17:36,967
Let's go, Goran.

145
00:17:41,054 --> 00:17:42,453
Didn't she?

146
00:17:42,734 --> 00:17:44,884
I saw you looking at her, you know.

147
00:17:45,734 --> 00:17:46,803
I saw you.

148
00:17:47,454 --> 00:17:49,684
But I like the younger one.

149
00:17:49,894 --> 00:17:51,566
Like this one.

150
00:17:52,653 --> 00:17:54,484
It's a shame she's dead.

151
00:18:31,051 --> 00:18:32,882
Why did you do that?

152
00:18:36,571 --> 00:18:38,880
She's just an old Muslim bitch.

153
00:18:41,571 --> 00:18:42,924
Let her bleed to death.

154
00:19:20,449 --> 00:19:23,043
Nice and quiet.

155
00:19:25,729 --> 00:19:27,321
A perfect week.

156
00:19:32,248 --> 00:19:33,283
You're French?

157
00:19:34,368 --> 00:19:37,246
American. I lived in France.

158
00:19:38,648 --> 00:19:40,001
You ever see...

159
00:19:40,968 --> 00:19:43,402
...9-0-2-1-0?

160
00:19:44,768 --> 00:19:45,678
What?

161
00:19:45,928 --> 00:19:47,725
Beverly Hills 9-0-2-1-0.

162
00:19:48,288 --> 00:19:49,515
Wacky show.

163
00:19:50,447 --> 00:19:51,402
Take a look.

164
00:19:52,967 --> 00:19:55,606
See this? It's Luke Perry.
You know the guy?

165
00:19:57,967 --> 00:20:01,039
I should, like, grow these....
How do you call these...?

166
00:20:01,567 --> 00:20:02,522
Sideburns?

167
00:20:02,767 --> 00:20:04,280
Yeah, sideburns.

168
00:20:04,687 --> 00:20:06,279
Can you imagine me?

169
00:20:06,527 --> 00:20:09,404
Brand-new partner
with brand-new sideburns.

170
00:20:10,766 --> 00:20:12,882
You ought to go to Hollywood.

171
00:20:14,246 --> 00:20:18,080
I don't think they'd like a Serb
student in Beverly Hills High School.

172
00:20:21,166 --> 00:20:22,394
So lived in France?

173
00:20:25,566 --> 00:20:27,284
Why did you come over here?

174
00:20:32,365 --> 00:20:35,084
Not many Western men here for Serbs,
you know.

175
00:20:35,965 --> 00:20:38,274
Most of them think we are
the bad guys.

176
00:20:39,925 --> 00:20:43,281
It's good to have someone like you
to see what's going on here.

177
00:20:46,645 --> 00:20:48,794
We just fight for our land.
That's all.

178
00:21:20,083 --> 00:21:23,075
What the fuck do you think
you're doing?!

179
00:21:26,882 --> 00:21:29,271
Hold your fire.
We got a kid on the bridge.

180
00:21:29,682 --> 00:21:32,515
Fuck the captain's orders.
We're not gonna shoot kids.

181
00:21:32,882 --> 00:21:35,521
We're not turning into
baby-killers, are we?

182
00:21:38,122 --> 00:21:39,840
What a fucking crazy kid.

183
00:21:41,962 --> 00:21:43,520
It's a girl, man.

184
00:21:46,241 --> 00:21:48,072
She's sure got balls.

185
00:21:56,641 --> 00:21:57,517
Chewing gum?

186
00:22:00,961 --> 00:22:02,360
Don't be afraid.

187
00:22:14,880 --> 00:22:17,952
What the fuck do you think
you're doing? What the fuck?

188
00:22:19,120 --> 00:22:22,510
Is that what you call a truce?
Is that what you call a truce?

189
00:22:22,760 --> 00:22:24,078
Motherfuckers!

190
00:22:24,879 --> 00:22:26,437
They betrayed us!

191
00:22:37,719 --> 00:22:39,471
Where are you going?

192
00:23:11,037 --> 00:23:11,913
See her?

193
00:23:12,957 --> 00:23:16,267
She's the daughter of a
Muslim bastard my father worked for.

194
00:23:16,597 --> 00:23:19,555
Many years worked hard,
but never was paid good.

195
00:23:20,037 --> 00:23:23,073
Then I saw her in prison camp
and I fucked her many times.

196
00:23:23,756 --> 00:23:25,155
You know what she said?

197
00:23:25,356 --> 00:23:29,235
She said I was the best she ever had.
And everybody had her.

198
00:23:29,556 --> 00:23:32,832
-They fuck your women too, you know.
-That's shit!

199
00:23:33,756 --> 00:23:35,314
Our women are stronger.

200
00:23:36,116 --> 00:23:40,029
Most would rather kill themselves
than be fucked by Turk or Croat.

201
00:23:41,236 --> 00:23:43,510
Many good priests,
and we got this one.

202
00:23:49,155 --> 00:23:49,985
Shit.

203
00:23:50,315 --> 00:23:52,988
He preaches, and Goran
does the dirty work.

204
00:24:06,474 --> 00:24:09,750
Check the houses up the hill.
They're sneaky bastards.

205
00:24:49,552 --> 00:24:51,463
Hello, my Serbian brothers!

206
00:24:52,272 --> 00:24:54,228
I got a little present for you!

207
00:26:24,587 --> 00:26:26,862
What the fuck is she doing in the car?

208
00:26:27,107 --> 00:26:28,665
We're gonna take her home.

209
00:26:29,627 --> 00:26:31,743
She tell you where she lives?

210
00:26:33,387 --> 00:26:34,866
I know where she lives.

211
00:26:41,426 --> 00:26:44,338
Did you see the green scarves?
At exchange?

212
00:26:44,986 --> 00:26:47,978
Those are mujahideens,
holy Allah warriors.

213
00:26:48,226 --> 00:26:51,024
We all gonna send them
to their fucking God!

214
00:26:52,186 --> 00:26:54,142
She's been fucking all of them.

215
00:26:54,346 --> 00:26:55,984
-She was raped.
-Raped?

216
00:26:56,825 --> 00:27:00,738
She's a fucking whore with a fucking
Muslim bastard in her belly!

217
00:27:00,945 --> 00:27:03,778
Her family, my neighbours,
good Serbs, great warriors....

218
00:27:04,025 --> 00:27:05,219
She dishonoured them.

219
00:27:05,425 --> 00:27:07,655
Now she wants to get back home.

220
00:27:20,624 --> 00:27:22,137
You know what she said?

221
00:27:22,584 --> 00:27:25,576
She doesn't have to talk to me.
You don't have to talk to me?

222
00:27:25,824 --> 00:27:27,018
Then I'll talk to you!

223
00:27:29,944 --> 00:27:31,172
What are you doing?

224
00:27:31,424 --> 00:27:32,743
I'm talking, okay?

225
00:27:32,944 --> 00:27:35,935
I'm having conversation here.
That okay for you?

226
00:28:05,662 --> 00:28:07,220
Now she wants to talk to me!

227
00:28:11,822 --> 00:28:12,937
What's that?

228
00:28:13,222 --> 00:28:15,257
The little Muslim bastard
is coming out.

229
00:28:15,501 --> 00:28:18,379
Well, I'm not sure about that.
I'm not sure about that!

230
00:28:19,181 --> 00:28:20,091
Goran.

231
00:28:23,781 --> 00:28:26,215
-Enough!
-Shut up! It's not your business!

232
00:28:26,461 --> 00:28:28,258
Come on! I'm waiting!
Come on, baby!

233
00:28:28,501 --> 00:28:29,536
That's enough!

234
00:28:29,741 --> 00:28:32,539
Push the fucking baby!
Come on!

235
00:29:10,739 --> 00:29:12,092
I'm gonna help you.

236
00:29:26,138 --> 00:29:26,968
All right.

237
00:29:27,618 --> 00:29:28,937
I can see the head.

238
00:29:29,178 --> 00:29:30,133
You push.

239
00:29:30,378 --> 00:29:31,857
Just push.

240
00:29:32,977 --> 00:29:33,853
That's it.

241
00:29:34,417 --> 00:29:35,452
Push.

242
00:29:41,497 --> 00:29:42,725
It's almost there.

243
00:29:43,017 --> 00:29:44,006
Push.

244
00:29:48,257 --> 00:29:50,327
Push. Push. It's coming.

245
00:29:50,737 --> 00:29:51,532
Almost.

246
00:29:53,336 --> 00:29:56,009
Keep pushing. Almost.

247
00:30:00,256 --> 00:30:01,291
One more time.

248
00:30:17,815 --> 00:30:18,691
It's a girl.

249
00:30:35,574 --> 00:30:36,450
Breathe.

250
00:32:01,090 --> 00:32:02,284
Don't do that.

251
00:32:05,170 --> 00:32:06,888
You don't want to do that.

252
00:32:21,969 --> 00:32:23,925
I need you to hold this baby.

253
00:32:26,609 --> 00:32:28,041
Can you hold the baby?

254
00:32:33,168 --> 00:32:34,601
You speak English?

255
00:32:43,888 --> 00:32:46,402
Here. You need to hold the baby.

256
00:32:49,407 --> 00:32:50,806
You need to feed it.

257
00:32:51,807 --> 00:32:53,957
You understand? You need to feed it.

258
00:32:58,767 --> 00:33:00,917
I'm just gonna help you, all right?

259
00:33:02,207 --> 00:33:03,765
Just giving you help.

260
00:33:09,206 --> 00:33:11,083
Put it up to your breast.

261
00:33:21,486 --> 00:33:23,522
What's the matter? Are you dry?

262
00:33:24,166 --> 00:33:25,439
You have no milk?

263
00:33:27,205 --> 00:33:29,514
That's all right.
Feed it when you get home.

264
00:33:32,925 --> 00:33:34,916
Can't you at least hold the thing?

265
00:37:06,714 --> 00:37:08,113
Speak English.

266
00:37:11,434 --> 00:37:13,584
Was she a part
of prisoner exchange today?

267
00:37:14,714 --> 00:37:16,511
Yeah. I brought her here.

268
00:37:17,354 --> 00:37:19,628
Where is Goran? I saw his car.

269
00:37:20,473 --> 00:37:21,667
He couldn't come.

270
00:38:11,471 --> 00:38:12,301
You must go.

271
00:38:12,951 --> 00:38:14,589
And take them with you.

272
00:38:16,231 --> 00:38:18,346
I can't take them with me.

273
00:38:18,590 --> 00:38:20,069
They can't stay here.

274
00:39:20,867 --> 00:39:22,664
You have any cousins?

275
00:39:26,067 --> 00:39:27,580
Do you know anybody?

276
00:39:31,667 --> 00:39:33,658
All right, here's what's gonna happen.

277
00:39:34,467 --> 00:39:37,458
I'm gonna take you to
the refugee camp in Bileca.

278
00:39:37,826 --> 00:39:38,781
Bileca?

279
00:39:40,746 --> 00:39:42,657
Somebody there'll take you in.

280
00:39:42,906 --> 00:39:45,101
Somebody will take the kid too.

281
00:39:45,346 --> 00:39:47,462
I'll be done with this bullshit.

282
00:39:51,466 --> 00:39:54,423
I tried to help you, but fuck it.
You know?

283
00:39:55,105 --> 00:39:56,618
Fuck it. I'm done.

284
00:40:04,345 --> 00:40:06,575
Jesus Christ, that's driving me nuts.

285
00:40:06,825 --> 00:40:09,055
Does it make you feel anything?

286
00:40:10,665 --> 00:40:13,304
Think you could stick
a bottle in that kid's mouth...

287
00:40:13,545 --> 00:40:15,853
...so I could get you
where you need to go?

288
00:40:25,624 --> 00:40:27,057
Son of a bitch!

289
00:40:27,304 --> 00:40:29,420
I'll feed the little bitch myself!

290
00:40:35,143 --> 00:40:36,337
I'm coming.

291
00:40:43,423 --> 00:40:46,301
Just quiet down.
Calm down. I'll be right there.

292
00:40:56,342 --> 00:40:57,661
Where's the nipple?

293
00:40:59,702 --> 00:41:01,055
Oh, fuck.

294
00:41:13,021 --> 00:41:15,455
Where's the motherfucking nipple?

295
00:41:40,260 --> 00:41:41,739
You got a better idea?

296
00:41:56,139 --> 00:41:59,051
Be the best use ever
for one of these, though.

297
00:42:04,739 --> 00:42:06,570
We got a surprise for you.

298
00:42:09,899 --> 00:42:10,933
All right.

299
00:42:11,698 --> 00:42:14,815
I'd take it if I were you.
It's all there is.

300
00:42:16,418 --> 00:42:17,328
Come on.

301
00:42:36,417 --> 00:42:39,056
You'd just as soon
see this kid dead, huh?

302
00:42:39,537 --> 00:42:41,334
Can't do it yourself?

303
00:42:43,257 --> 00:42:46,055
Don't worry about it.
You're better off for it.

304
00:42:51,136 --> 00:42:53,252
It's easy to kill somebody.

305
00:42:54,496 --> 00:42:56,851
You just pull the trigger
and it's done.

306
00:42:59,216 --> 00:43:00,729
Living with it.

307
00:43:02,816 --> 00:43:04,613
You gotta harden up for that.

308
00:44:48,930 --> 00:44:50,158
Give me the bottle.

309
00:44:55,130 --> 00:44:56,119
Your mother's a bitch.

310
00:45:07,049 --> 00:45:10,644
But you'll be lucky if you turn out
to be as pretty as she is.

311
00:46:44,084 --> 00:46:45,278
It's blues.

312
00:46:46,244 --> 00:46:47,723
It gets me going.

313
00:46:51,084 --> 00:46:54,554
Low down and dirty
Pay his dues

314
00:46:56,084 --> 00:46:58,154
What's the matter?
Don't like the blues?

315
00:47:02,163 --> 00:47:05,758
Come on, kid.
Can't even the blues shut you up?

316
00:47:28,522 --> 00:47:30,592
I don't suppose you'd change this kid?

317
00:47:32,082 --> 00:47:33,276
Guess not.

318
00:47:53,601 --> 00:47:54,636
Man, you stink.

319
00:47:56,281 --> 00:47:58,841
That's a lot of shit for a little kid.

320
00:48:12,720 --> 00:48:13,709
Fuck it.

321
00:48:33,119 --> 00:48:34,268
It's not very ladylike.

322
00:49:19,316 --> 00:49:21,546
I hope you appreciate this.

323
00:49:44,715 --> 00:49:47,024
What do you think of this new style?

324
00:49:47,475 --> 00:49:49,306
Think they'll like that in Milan?

325
00:51:49,505 --> 00:51:51,814
They must have found Goran's body.

326
00:52:01,145 --> 00:52:03,100
You gonna hold that kid?

327
00:52:26,184 --> 00:52:28,903
This road will probably
take us to Bileca.

328
00:52:32,903 --> 00:52:36,100
Jesus Christ, that kid is
a pain in the ass!

329
00:52:44,063 --> 00:52:46,293
Did you drink any of this?

330
00:52:47,383 --> 00:52:48,896
Son of a bitch!

331
00:52:50,063 --> 00:52:53,100
Now I know where that fucking broad
gets its attitude!

332
00:53:34,101 --> 00:53:36,296
All right, goats. Hello.
Hello.

333
00:53:40,501 --> 00:53:41,297
Come here.

334
00:53:42,181 --> 00:53:43,136
Come here.

335
00:53:48,661 --> 00:53:50,094
You're a billy goat.

336
00:53:50,581 --> 00:53:52,059
I need a billy-ana.

337
00:53:54,580 --> 00:53:56,172
This one right here.
Come on!

338
00:54:04,460 --> 00:54:05,893
You think that's funny?

339
00:54:34,539 --> 00:54:36,894
Don't any of the tits
in this country work?

340
00:56:30,935 --> 00:56:32,084
Wait!

341
00:56:39,534 --> 00:56:40,489
It's not her fault!

342
00:56:50,174 --> 00:56:51,163
Don't.

343
00:56:52,694 --> 00:56:54,173
I shot Goran.

344
00:56:54,734 --> 00:56:55,849
Why?

345
00:56:56,974 --> 00:57:00,807
He was gonna kill her
and the baby like dogs.

346
00:58:12,051 --> 00:58:13,484
It's gonna be all right.

347
00:58:13,691 --> 00:58:17,127
It's all right.
It's just through flesh.

348
00:58:17,371 --> 00:58:20,601
No, it's not good.
You need to stop bleeding.

349
00:58:21,890 --> 00:58:23,164
I knew you spoke English.

350
00:58:23,970 --> 00:58:25,961
Only not so good.

351
00:58:26,450 --> 00:58:28,088
Yeah, right.

352
00:58:28,730 --> 00:58:30,721
Is there first aid in the car?

353
00:58:31,170 --> 00:58:34,401
There's something in my vest.
Can you get it? Bring it.

354
00:58:58,289 --> 00:58:59,927
I'm gonna be all right.

355
00:59:00,129 --> 00:59:02,927
There'll be a medic in Bileca.

356
00:59:03,489 --> 00:59:06,242
We should just keep on going.

357
00:59:08,369 --> 00:59:11,167
You are not good.
You should lay down.

358
00:59:26,328 --> 00:59:28,717
It's okay. I was just trying to....

359
00:59:39,168 --> 00:59:40,886
Why didn't they kill us?

360
00:59:45,527 --> 00:59:47,040
He's my father.

361
00:59:47,967 --> 00:59:49,161
He loves me.

362
00:59:50,367 --> 00:59:51,641
They are my family.

363
00:59:53,127 --> 00:59:54,924
Don't lie to yourself.

364
00:59:55,447 --> 00:59:57,483
There's nothing for you here.

365
01:00:00,167 --> 01:00:01,282
Listen...

366
01:00:03,647 --> 01:00:05,717
...we don't have to go to Bileca.

367
01:00:06,767 --> 01:00:09,326
I could help you
get out of the country.

368
01:00:11,486 --> 01:00:12,999
And go where?

369
01:00:14,046 --> 01:00:15,479
Someplace...

370
01:00:15,686 --> 01:00:16,801
...normal.

371
01:00:22,286 --> 01:00:23,799
You should rest.

372
01:01:09,244 --> 01:01:10,279
Hello?

373
01:01:43,443 --> 01:01:44,671
Stop following me.

374
01:01:47,123 --> 01:01:48,602
Where are you going?

375
01:01:48,963 --> 01:01:50,635
I'm going home.

376
01:01:51,163 --> 01:01:53,074
Stop following me.

377
01:01:54,163 --> 01:01:56,722
-I want to help.
-Why do you need to help me?

378
01:01:57,722 --> 01:02:00,236
-Need?
-Yes. I can see you need that.

379
01:02:03,362 --> 01:02:04,875
I am from here.

380
01:02:05,682 --> 01:02:07,991
Somehow they're right
when they say...

381
01:02:08,722 --> 01:02:10,201
...I was not a good Serb woman.

382
01:02:10,962 --> 01:02:12,793
That's not true.
Listen to me.

383
01:02:13,002 --> 01:02:16,358
What happened to you,
it wasn't your fault.

384
01:02:17,762 --> 01:02:19,036
It was.

385
01:02:19,282 --> 01:02:21,193
I deserve no better.

386
01:02:28,801 --> 01:02:30,075
What about your baby?

387
01:02:31,321 --> 01:02:33,630
This is her home too.

388
01:02:35,281 --> 01:02:37,351
All right, listen. Stop.

389
01:02:39,521 --> 01:02:42,354
Get in the car.
I'll drive you there.

390
01:02:46,361 --> 01:02:47,396
Come on.

391
01:04:50,356 --> 01:04:51,345
Don't.

392
01:04:53,316 --> 01:04:54,305
Baby.

393
01:05:54,714 --> 01:05:56,432
There's too many of them.

394
01:05:57,554 --> 01:05:59,589
-Maybe I could try to--
-No.

395
01:06:00,393 --> 01:06:02,827
There is nothing we can do for them.

396
01:06:06,633 --> 01:06:07,952
We should just go.

397
01:06:24,313 --> 01:06:28,271
I'm taking you to Split.
There's UN. There's Red Cross.

398
01:06:28,472 --> 01:06:30,622
They can help you get out
of the country.

399
01:07:12,711 --> 01:07:14,622
-No gas?
-No gas.

400
01:07:18,551 --> 01:07:19,903
We'll find some.

401
01:07:20,390 --> 01:07:21,459
Come on.

402
01:07:37,110 --> 01:07:40,182
Let's get closer.
We'll wait for the night.

403
01:08:02,749 --> 01:08:04,182
Stop!

404
01:08:04,549 --> 01:08:06,585
Don't! Don't shoot!
He has no gun.

405
01:08:06,829 --> 01:08:08,945
-Okay. Okay.
-It's okay.

406
01:08:11,869 --> 01:08:12,745
What do you want?

407
01:08:50,947 --> 01:08:52,346
She says you need to eat.

408
01:08:53,467 --> 01:08:54,786
You're American?

409
01:08:56,067 --> 01:08:57,500
I used to be.

410
01:08:59,827 --> 01:09:02,295
You fight for the Serbs?

411
01:09:04,547 --> 01:09:07,219
I am Croat. My wife, Serb.

412
01:09:07,746 --> 01:09:10,306
Before the war, no difference.

413
01:09:11,626 --> 01:09:13,662
Now, stupid.

414
01:09:15,506 --> 01:09:17,497
Where are you going?

415
01:09:18,946 --> 01:09:20,220
Split.

416
01:09:21,546 --> 01:09:22,740
Do you have any gas?

417
01:09:22,986 --> 01:09:24,465
Very little.

418
01:09:24,706 --> 01:09:26,697
Army take it all.

419
01:09:27,426 --> 01:09:28,779
We'll walk.

420
01:09:29,666 --> 01:09:30,860
It's a long walk.

421
01:09:31,066 --> 01:09:33,863
Stay here until you are better.

422
01:09:39,865 --> 01:09:41,537
No, we need to go.

423
01:09:42,345 --> 01:09:44,620
You stay tonight.

424
01:10:00,345 --> 01:10:02,733
She asked me if you're my husband.

425
01:10:13,064 --> 01:10:16,056
I don't remember the last time
I slept in a bed.

426
01:10:20,024 --> 01:10:21,093
Vera?

427
01:10:22,824 --> 01:10:25,657
That's your name, right?
That's what your mother called you?

428
01:10:26,904 --> 01:10:27,779
Yes.

429
01:10:39,143 --> 01:10:41,259
I'm sorry about your family.

430
01:10:45,943 --> 01:10:47,217
It's war.

431
01:10:55,942 --> 01:10:57,455
This is beautiful.

432
01:11:07,422 --> 01:11:08,571
You are called Guy?

433
01:11:10,142 --> 01:11:11,780
That's not my real name.

434
01:11:12,942 --> 01:11:14,375
It's Joshua.

435
01:11:16,022 --> 01:11:17,740
Joshua Rose.

436
01:11:18,142 --> 01:11:19,700
You changed your name?

437
01:11:21,542 --> 01:11:22,974
Why?

438
01:11:23,341 --> 01:11:24,615
I had to.

439
01:11:25,941 --> 01:11:27,294
Joshua...

440
01:11:29,461 --> 01:11:30,974
...did some things.

441
01:11:38,541 --> 01:11:39,974
You are a good man.

442
01:12:39,339 --> 01:12:40,294
Vera...

443
01:12:41,619 --> 01:12:43,017
...it's time to go.

444
01:12:46,618 --> 01:12:47,414
This will help.

445
01:12:47,938 --> 01:12:50,452
Serb uniforms are no good
where I'm going.

446
01:12:50,698 --> 01:12:52,177
I have decided.

447
01:12:52,538 --> 01:12:54,017
You take my boat.

448
01:12:55,658 --> 01:12:57,171
I can't take your boat.

449
01:12:57,418 --> 01:12:58,646
You cannot walk.

450
01:12:59,138 --> 01:13:02,016
There is a town across the lake.

451
01:13:02,258 --> 01:13:04,249
Maybe get a bus to Split.

452
01:13:04,458 --> 01:13:07,097
They have buses, even with the war?

453
01:13:07,818 --> 01:13:09,012
Sometimes.

454
01:13:09,417 --> 01:13:11,885
There's so much fighting these days.

455
01:13:12,417 --> 01:13:14,089
Very dangerous.

456
01:13:14,337 --> 01:13:15,929
It's dangerous for you too.

457
01:13:17,817 --> 01:13:21,332
The clothes you wear
belonged to my son.

458
01:13:22,257 --> 01:13:23,895
He's dead.

459
01:13:24,537 --> 01:13:26,653
He also had a son.

460
01:13:26,857 --> 01:13:28,051
Dead too.

461
01:13:28,897 --> 01:13:30,535
Killed, fighting.

462
01:13:31,297 --> 01:13:32,366
For what?

463
01:13:36,217 --> 01:13:39,845
I have nothing more to lose.

464
01:13:40,656 --> 01:13:41,805
Are you ready?

465
01:14:40,934 --> 01:14:42,652
Let's not take any chances.

466
01:14:43,814 --> 01:14:46,044
I'm gonna land the boat further down.

467
01:15:20,653 --> 01:15:22,769
You are bleeding again.

468
01:15:23,333 --> 01:15:25,243
We still have to make the bus.

469
01:15:25,812 --> 01:15:27,689
I think it's better if I go alone.

470
01:15:27,932 --> 01:15:30,651
The wrong people may see
you are wounded.

471
01:15:32,332 --> 01:15:33,447
We both go.

472
01:15:34,132 --> 01:15:36,441
You don't even know
if there is a bus, or when.

473
01:15:36,692 --> 01:15:39,365
What if it doesn't leave for hours
and we have to wait?

474
01:15:39,612 --> 01:15:41,250
It's too dangerous.

475
01:15:41,652 --> 01:15:43,847
-What if it leaves in an hour?
-lt doesn't matter.

476
01:15:44,052 --> 01:15:47,044
You said we shouldn't take
any chances.

477
01:15:48,092 --> 01:15:49,207
Stay here.

478
01:15:50,012 --> 01:15:52,650
I'll look for a bus and come back.

479
01:15:53,731 --> 01:15:55,084
Wait.

480
01:15:58,731 --> 01:16:00,369
Leave the baby with me.

481
01:16:01,891 --> 01:16:03,449
She'll slow you down.

482
01:16:16,931 --> 01:16:21,640
Now, on your way into town,
if you hear a car, get off the road.

483
01:26:02,909 --> 01:26:03,864
Breathe.

484
01:26:07,349 --> 01:26:08,145
Breathe.

485
01:26:13,468 --> 01:26:14,344
Breathe!

486
01:26:18,388 --> 01:26:19,264
Come on!

487
01:26:28,588 --> 01:26:29,464
Come on.

488
01:26:29,868 --> 01:26:31,381
Come on. Breathe!

489
01:28:07,464 --> 01:28:08,613
Split.

490
01:28:12,464 --> 01:28:14,022
I wanna go to Split.

491
01:28:14,264 --> 01:28:16,539
Three hundred Deutschmark.

492
01:28:16,984 --> 01:28:18,258
I have no money.

493
01:28:18,784 --> 01:28:20,536
No money, no ride.

494
01:29:47,860 --> 01:29:48,690
Split.

495
01:30:06,980 --> 01:30:07,856
Excuse me.

496
01:30:08,700 --> 01:30:10,258
Where's the Red Cross?

497
01:30:10,580 --> 01:30:11,933
I don't know.

498
01:30:12,180 --> 01:30:12,930
I know.

499
01:30:13,939 --> 01:30:16,089
It's at the port. It's close.

500
01:31:00,258 --> 01:31:02,977
You just cry, and somebody
will come for you.

501
01:31:07,577 --> 01:31:10,250
I'd like to take you with me
but I can't.

502
01:31:11,977 --> 01:31:13,888
You're better off here.

503
01:31:14,777 --> 01:31:16,654
Somebody will take care of you.

504
01:31:52,456 --> 01:31:54,845
Your mother loved you very much.

505
01:32:00,536 --> 01:32:01,968
I love you too.

506
01:32:17,895 --> 01:32:20,090
You grow up. You be happy.

507
01:34:05,571 --> 01:34:06,970
Excuse me?

508
01:34:11,731 --> 01:34:12,925
Excuse me.

509
01:34:15,850 --> 01:34:17,761
Is this your daughter?

510
01:34:20,570 --> 01:34:21,639
Yes.

511
01:34:22,570 --> 01:34:23,798
She's mine.

512
01:34:24,770 --> 01:34:27,523
There is no reason to give up baby.

513
01:34:30,970 --> 01:34:32,688
You were on the bus.

514
01:34:33,490 --> 01:34:34,400
Yes.

515
01:34:37,450 --> 01:34:38,405
You are hurt?

516
01:34:44,129 --> 01:34:45,926
So I will take you...

517
01:34:46,449 --> 01:34:48,201
...and your baby to hospital, okay?

518
01:34:56,169 --> 01:34:57,443
Okay.

519
01:39:20,559 --> 01:39:21,548
Subtitles Repack by
Ingeborg

