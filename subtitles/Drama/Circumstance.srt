1
00:00:29,120 --> 00:00:33,318
If you could be anywhere in the world,


2
00:00:33,480 --> 00:00:36,517
where would you be?

3
00:00:36,680 --> 00:00:38,591
Anywhere in the world?

4
00:00:38,920 --> 00:00:41,388
Yes, anywhere.

5
00:00:42,520 --> 00:00:45,671
A place where you could sing

6
00:00:45,880 --> 00:00:48,519
and I'll be your manager.

7
00:02:05,560 --> 00:02:08,870
Shahnaz Yoonesi.
Present.

8
00:02:09,840 --> 00:02:13,469
- Zahra Latifpour.
- Present.

9
00:02:15,200 --> 00:02:18,158
- Maryam Hedayat.
- Present.

10
00:02:19,280 --> 00:02:22,078
- Tannaz Hemmati.
- Present.

11
00:02:23,040 --> 00:02:25,759
- Shireen Arshadi.
- Present.

12
00:02:26,400 --> 00:02:29,437
- Tahereh Asghari
- Present.

13
00:02:30,440 --> 00:02:33,398
- Fatemeh shirazi.
- Present.

14
00:02:33,560 --> 00:02:36,358
- Marziyeh zarindoos.
- Present.

15
00:02:36,680 --> 00:02:40,070
- Atafeh Hakimi.
- Present.

16
00:02:41,640 --> 00:02:45,792
Everyone dismissed except
Miss Shireen Arshadi.

17
00:02:45,960 --> 00:02:48,713
Please stay to speak
with the headmaster.

18
00:03:00,520 --> 00:03:02,875
- I'll come with you.
- No.

19
00:03:07,520 --> 00:03:11,559
Sorry, I'm in a hurry.
We have a biology exam today.

20
00:03:11,720 --> 00:03:14,280
I'm her lab partner,
so I'll just wait for her.

21
00:03:14,440 --> 00:03:19,878
You've been busy all month.
I can arrange for plenty
of free time...

22
00:03:20,040 --> 00:03:22,031
At your service, ma'am.


23
00:03:24,600 --> 00:03:28,275
We usually don't let people
like you into our school.

24
00:03:29,960 --> 00:03:31,757
Do you know why?

25
00:03:33,680 --> 00:03:35,113
No, ma'am.

26
00:03:37,000 --> 00:03:39,594
Because their character is questionable.


27
00:03:40,400 --> 00:03:44,951
You're late again with this semester's tuition.
Sorry, Madame.

28
00:03:45,120 --> 00:03:47,793
it's my fault.
I borrowed some money from Shireen, but...


29
00:03:47,960 --> 00:03:51,953
Take it back.
Bring it to my office later.

30
00:03:52,280 --> 00:03:54,191
Get to class.

31
00:04:09,760 --> 00:04:12,797
"Your character is questionable."

32
00:04:35,120 --> 00:04:37,270
Cello suites,

33
00:04:37,560 --> 00:04:38,595
Bach.
More specific.

34
00:04:38,760 --> 00:04:42,878
The cellist, the key, the movement.

35
00:04:49,040 --> 00:04:50,678
Pablo Casals.

36
00:04:50,880 --> 00:04:54,998
Minuet in D Minor.

37
00:04:55,640 --> 00:04:58,791
Cello suites, Bach.

38
00:04:59,880 --> 00:05:01,108
Mehran!

39
00:05:02,240 --> 00:05:05,152
The student has beaten his teacher.

40
00:05:05,320 --> 00:05:07,629
The prodigal son.

41
00:05:07,800 --> 00:05:09,438
Welcome home.

42
00:05:11,160 --> 00:05:12,752
Are you well?

43
00:05:13,960 --> 00:05:16,793
Mom has made one of her special cakes.

44
00:05:16,960 --> 00:05:18,552
God help us.


45
00:05:18,720 --> 00:05:22,349
Eat it for your mother's sake.

46
00:05:42,720 --> 00:05:44,119
Azar!
How about this one?

47
00:06:04,400 --> 00:06:05,879
Mehran.!

48
00:06:07,440 --> 00:06:09,954
Here he is,
as good as new.

49
00:06:28,160 --> 00:06:29,957
You've gotten so thin.

50
00:06:32,480 --> 00:06:34,232
Are you feeling better?

51
00:06:34,480 --> 00:06:39,190
Mehran, I ran into Professor Jamshid,
at the University yesterday.

52
00:06:39,400 --> 00:06:44,428
There might be an opening
in the music department.

53
00:06:45,320 --> 00:06:48,118
Music isn't for me
anymore, Father.

54
00:06:49,400 --> 00:06:50,674
What are you saying?

55
00:06:51,520 --> 00:06:53,078
I'm sorry, Father.

56
00:06:53,280 --> 00:06:56,431
Mehran, you've worked so hard..

57
00:07:38,280 --> 00:07:40,999
- What, are you scared?
- I told my uncle that I'd be home.

58
00:07:41,160 --> 00:07:43,469
Come on, I'll make you
a respectable girl yet.

59
00:07:54,800 --> 00:07:56,233
So, what's your name?

60
00:07:58,040 --> 00:08:01,112
Let's see if you earn
the privilege to know my name.

61
00:08:01,320 --> 00:08:02,548
Vanak Square.

62
00:08:03,320 --> 00:08:05,038
What's your rush?

63
00:08:06,440 --> 00:08:08,237
We're having fun.

64
00:08:10,800 --> 00:08:13,234
Turn it up!

65
00:08:14,640 --> 00:08:16,835
Turn it up!
This song is orgasmic.

66
00:08:46,560 --> 00:08:47,595
Who is it?

67
00:08:47,760 --> 00:08:49,955
We're here for the sewing class.

68
00:10:04,960 --> 00:10:08,157
- Let me introduce you to my friend.
- Salaam  Saalaam.

69
00:10:09,440 --> 00:10:12,796
This is my cousin, Hossein,
who's coming from America.  America!

70
00:10:14,720 --> 00:10:16,631
I'm gonna go dance.

71
00:10:16,880 --> 00:10:18,438
Don't worry.

72
00:10:19,160 --> 00:10:21,310
Tonight, I'm your professor.

73
00:10:31,080 --> 00:10:34,470
Shireen     Ho-bag..
Ho-bag,     shireen.

74
00:10:35,280 --> 00:10:37,157
Protection's in the drawer.

75
00:10:39,160 --> 00:10:41,993
Ho-bag?
Don't worry about it.

76
00:10:42,680 --> 00:10:44,079
I can't hear you.

77
00:10:44,240 --> 00:10:47,550
Let's go somewhere we can talk.

78
00:12:44,560 --> 00:12:46,790
Can I ask you a question?

79
00:12:47,120 --> 00:12:49,475
Does it make you jealous?

80
00:12:51,320 --> 00:12:54,710
- Why would I be jealous?
- Isn't he your boyfriend?

81
00:13:16,840 --> 00:13:19,559
Boy, that's how it's done.

82
00:13:22,200 --> 00:13:25,749
I knew you weren't such a good girl.

83
00:13:52,440 --> 00:13:54,192
Open your mouth.

84
00:14:17,400 --> 00:14:19,152
Hey, check this out.

85
00:14:19,640 --> 00:14:21,596
Beautiful sparkles.

86
00:14:41,000 --> 00:14:43,514
pour moi?.
pour toi       madame.

87
00:15:09,120 --> 00:15:11,315
This isn't necessary anymore.

88
00:15:13,280 --> 00:15:14,508
Dad....

89
00:15:15,000 --> 00:15:17,719
I'm not going to fuck up again.

90
00:15:18,440 --> 00:15:20,237
I'm sorry.

91
00:15:43,480 --> 00:15:45,869
This is your weekly allowance.

92
00:15:46,040 --> 00:15:48,554
I need all your receipts.

93
00:15:56,680 --> 00:15:58,796
Kids..    I have to get to work.

94
00:16:08,080 --> 00:16:09,195
Mehran...

95
00:16:09,360 --> 00:16:13,353
I have to tell you something.
You'll die of laughter.

96
00:16:16,560 --> 00:16:17,788
Hello.

97
00:16:20,800 --> 00:16:24,952
- Mehran, are you OK?
- Leave me be.

98
00:16:25,680 --> 00:16:28,194
Why can't you understand,
I'm better.

99
00:16:29,320 --> 00:16:32,198
I was just....
I know exactly what the fuck 
you were trying to do.

100
00:17:14,240 --> 00:17:16,754
Can you help a brother?

101
00:17:20,920 --> 00:17:23,639
Could you help a brother?

102
00:17:26,120 --> 00:17:27,633
Here, brother.

103
00:17:28,280 --> 00:17:29,838
Take this food.

104
00:17:32,200 --> 00:17:34,156
My wife made it.

105
00:17:36,480 --> 00:17:39,677
If you want help, I know a place
that can help you.

106
00:17:42,440 --> 00:17:45,000
Those addicts won't even leave 
the mosque in peace.

107
00:17:46,560 --> 00:17:49,154
The mosque is a place for everyone.

108
00:17:50,000 --> 00:17:52,036
God is compassionate.

109
00:17:56,880 --> 00:18:00,236
I'm only here
because of God's mercy.

110
00:18:06,240 --> 00:18:10,199
Seeing young believers
like yourself gives one hope.

111
00:18:15,240 --> 00:18:16,673
I'm Mohammad Mehdi.


112
00:18:18,440 --> 00:18:22,399
Your name, sir?...
Pardon me.   My name is Mehran.

113
00:18:24,520 --> 00:18:27,637
So dear Mehran,
what do u do?

114
00:18:28,240 --> 00:18:30,276
What is your profession?

115
00:18:36,400 --> 00:18:41,110
A true Muslim needs to work
and provide for a family.

116
00:18:41,280 --> 00:18:43,635
God has not willed it yet.

117
00:18:44,520 --> 00:18:47,398
Believe in God's plan.
He will provide.

118
00:19:11,360 --> 00:19:12,793
Good morning, mother.

119
00:19:13,280 --> 00:19:14,395
Morning, son.

120
00:19:14,560 --> 00:19:18,633
Shireen, put on some clothes.
You have a suitor.

121
00:19:23,360 --> 00:19:25,396
Yusef, please have a seat.

122
00:19:33,440 --> 00:19:35,431
Shireen! Bring the tea.

123
00:19:52,480 --> 00:19:55,074
Yusef, 
I present my niece, Shireen.

124
00:20:21,000 --> 00:20:24,549
- Did something happen?
- It's nothing.

125
00:20:26,280 --> 00:20:29,317
What a heavy surgery.

126
00:20:29,480 --> 00:20:32,517
My back and feet are killing me.ds.

127
00:20:39,240 --> 00:20:43,119
I'm not that scary.

128
00:20:43,640 --> 00:20:46,677
Tell me if something is wrong, OK?
Mehran...

129
00:20:47,640 --> 00:20:52,350
Tomorrow, we're all going to the seaside.
Be ready at 7 A.M.


130
00:20:53,080 --> 00:20:55,878
- Is that all, Dad?
- That's all.

131
00:21:07,800 --> 00:21:11,759
Seems like yesterday,
they were children.


132
00:21:12,920 --> 00:21:15,798
Seems like yesterday,
we were young.

133
00:23:19,160 --> 00:23:20,878
Little devil.

134
00:23:25,800 --> 00:23:29,110
Dad, let's go swimming.

135
00:23:36,880 --> 00:23:39,917
You're all sweaty.
Go in the water.


136
00:23:41,200 --> 00:23:43,839
One day we can all go in
together.

137
00:24:56,840 --> 00:24:59,798
Mommy.

138
00:25:54,160 --> 00:25:55,957
I'm scared.

139
00:25:58,480 --> 00:26:00,471
It's nothing.

140
00:26:00,680 --> 00:26:03,240
You're just a bit sick.

141
00:26:04,560 --> 00:26:06,198
I love you.

142
00:26:09,400 --> 00:26:10,913
I know.

143
00:26:16,480 --> 00:26:18,710
I want to tell you something.

144
00:26:20,080 --> 00:26:21,718
You don't need to,

145
00:26:22,320 --> 00:26:23,992
I already know.


146
00:26:24,760 --> 00:26:26,352
Really?

147
00:26:30,000 --> 00:26:32,434
Let's run away to Dubai.

148
00:26:33,160 --> 00:26:36,072
You'll sing and
I'll be your manager.

149
00:26:37,560 --> 00:26:39,630
You're delirious.

150
00:26:40,760 --> 00:26:43,718
In Dubai,
anything is possible.

151
00:29:34,800 --> 00:29:37,872
It's cold.
Scared?

152
00:30:43,560 --> 00:30:46,472
- Dad, pull over.
- Are you sick?

153
00:30:47,320 --> 00:30:49,231
It is time for my prayers.

154
00:30:50,360 --> 00:30:52,032
You're joking.

155
00:30:52,560 --> 00:30:54,710
No, it's not a joke.

156
00:31:59,400 --> 00:32:02,949
Atafeh and Mr. Hakimi
will be here soon.

157
00:32:03,160 --> 00:32:05,628
I'll bring tea.

158
00:35:04,000 --> 00:35:07,675
Just wait a second...
This system is hopeless.

159
00:35:07,840 --> 00:35:09,831
Wait, it will get fixed.

160
00:35:12,200 --> 00:35:15,909
1..2..3..
I haven't attached the cord
and you're testing.

161
00:35:16,120 --> 00:35:18,588
Can I help?

162
00:35:19,520 --> 00:35:21,033
With the will of God.


163
00:36:10,920 --> 00:36:12,399
I'm grateful.

164
00:36:41,880 --> 00:36:44,394
How I love this song!

165
00:37:35,440 --> 00:37:37,510
I know it's a bit late.

166
00:37:38,160 --> 00:37:40,674
But I just wanted to say
Happy Birthday.

167
00:37:42,000 --> 00:37:44,753
Thanks.
It was three weeks ago.

168
00:38:10,640 --> 00:38:14,633
One day I'll win it.
The young can dream.

169
00:38:31,160 --> 00:38:33,913
Let's go abroad.

170
00:38:40,800 --> 00:38:43,314
A place we can be free.

171
00:38:55,920 --> 00:38:59,196
We'll go to clubs

172
00:39:00,600 --> 00:39:02,830
We'll have fun.

173
00:39:07,840 --> 00:39:10,274
Sweet!

174
00:39:55,880 --> 00:39:58,474
Sweet!

175
00:40:17,960 --> 00:40:20,235
Do you feel embarrassed?

176
00:40:20,400 --> 00:40:22,038
A little.

177
00:41:35,280 --> 00:41:39,432
- You know what this dork wants to do here?
- Tell us.

178
00:41:39,600 --> 00:41:41,272
Dub "Milk" into Persian.

179
00:41:41,680 --> 00:41:43,830
In the film where Sean Penn is gay?

180
00:41:44,040 --> 00:41:47,999
Milk's non-violent protests
led to a movement.
.

181
00:41:48,160 --> 00:41:50,594
We'd be like Che.

182
00:41:50,760 --> 00:41:54,514
Here they make Che
into a God-fearing man.

183
00:41:56,280 --> 00:42:00,193
But they can't co-opt a gay figure.
Yes, they can.

184
00:42:00,400 --> 00:42:05,076
They're experts.
They'll make Milk lead the anti-gay movement.

185
00:42:05,280 --> 00:42:06,395
Exactly.

186
00:42:07,120 --> 00:42:10,476
Don't you want to
change your circumstances?

187
00:42:13,320 --> 00:42:15,550
President Hossein Obama!

188
00:42:15,720 --> 00:42:18,359
Change has come our way.

189
00:42:19,840 --> 00:42:23,719
Hey, don't make fun of Obama.

190
00:42:25,240 --> 00:42:29,916
What they were doing to the gays
30 years ago,

191
00:42:30,080 --> 00:42:32,674
they're doing to you now.

192
00:42:33,560 --> 00:42:37,997
We'll rock the country with this film.
We'll pour into the streets.

193
00:42:38,160 --> 00:42:39,957
We're united together,

194
00:42:40,120 --> 00:42:43,476
- we are gay together.
- We knew you were gay.

195
00:42:46,680 --> 00:42:50,389
Don't worry, Atie.
I'll take the role of Milk.

196
00:42:50,600 --> 00:42:54,878
You play his lover, Diego Luna.
Does that mean I can fuck you?

197
00:42:55,040 --> 00:42:57,554
- Yes, please.
- Sweet!

198
00:42:57,720 --> 00:43:01,554
This film is not about fucking.
It's about human rights.

199
00:43:01,760 --> 00:43:06,356
Fucking is a human's right.
I swear it's a human's right.

200
00:43:07,080 --> 00:43:08,115
Listen.

201
00:43:08,320 --> 00:43:11,198
We dub "Milk" in the day

202
00:43:11,360 --> 00:43:13,555
And "Sex and the City"
for mainstream subversion.

203
00:43:13,720 --> 00:43:15,711
Isn't that a porn film?

204
00:43:15,880 --> 00:43:19,350
It's always the same with you, Joey.
You're really sick.

205
00:43:19,520 --> 00:43:22,796
"Sex and the City"
will sell like crazy.

206
00:43:23,000 --> 00:43:25,560
And we'll throw "Milk"
on the same DVD.

207
00:43:27,200 --> 00:43:30,715
I want to create serious dialogue.

208
00:43:31,800 --> 00:43:35,429
Sweetie, here anything illegal

209
00:43:35,600 --> 00:43:38,034
becomes politically subversive.

210
00:43:38,640 --> 00:43:42,269
Political acts aren't
that romantic, Atafeh.

211
00:43:42,440 --> 00:43:46,911
With the parents that you had,
subversion and naughtinessis in your blood.

212
00:43:47,120 --> 00:43:50,351
Shut up, Joey.
I can defend myself.

213
00:43:52,600 --> 00:43:54,477
What did I say?

214
00:43:55,920 --> 00:43:57,194
What did I say?

215
00:44:24,440 --> 00:44:26,590
Do you mind if I play music?

216
00:44:26,760 --> 00:44:29,752
No, it's a nice song.

217
00:44:29,960 --> 00:44:32,190
Very relaxing.

218
00:44:34,040 --> 00:44:36,952
- We can go somewhere relaxing.
- Excuse me?

219
00:44:37,120 --> 00:44:38,394
Don't be coy.

220
00:44:38,560 --> 00:44:42,189
- You're mistaken... What do you take me for?
- Who do you think you are?

221
00:44:42,360 --> 00:44:46,319
- Sir, please stop the car.
- Don't think the clothes fool me.


222
00:44:46,480 --> 00:44:50,996
- Stop. I want to get out.
- No matter how you dress
yourself, trash is trash.



223
00:44:53,600 --> 00:44:58,196
How about we go to the police station?
I have some friends there.


224
00:45:04,720 --> 00:45:06,312
- Give me your foot.
- Excuse me?

225
00:45:07,840 --> 00:45:09,956
I just want your foot.

226
00:46:47,480 --> 00:46:49,789
You think I'm hot?

227
00:47:04,960 --> 00:47:06,837
Like it?

228
00:47:07,640 --> 00:47:09,596
Feels good?

229
00:47:21,680 --> 00:47:23,796
Go to hell.

230
00:48:04,480 --> 00:48:05,993
Mehran!

231
00:48:09,000 --> 00:48:11,992
We've missed you, man.

232
00:48:12,560 --> 00:48:15,757
There is a killer afterparty.

233
00:48:39,760 --> 00:48:42,194
Shireen! Get down!

234
00:49:03,520 --> 00:49:05,238
Oh, my God, are you okay?

235
00:49:05,400 --> 00:49:08,153
Hossein, let's dub those scenes.

236
00:49:16,000 --> 00:49:17,877
That was awesome.

237
00:49:18,040 --> 00:49:23,068
Fuck the mothers of all the mullahs
who shit all over this country.

238
00:49:27,640 --> 00:49:29,915
To Hollywood!

239
00:49:37,640 --> 00:49:40,632
Where's your energy? Dance.

240
00:49:44,080 --> 00:49:46,036
Are you OK?

241
00:49:46,200 --> 00:49:48,270
Don't tell me what to do.

242
00:50:03,880 --> 00:50:06,872
Morality Police!
Morality Police!

243
00:50:09,600 --> 00:50:10,555
Hurry!

244
00:50:11,000 --> 00:50:12,115
shireen!

245
00:50:12,880 --> 00:50:14,677
Go! Go!

246
00:50:29,600 --> 00:50:31,795
Thank God you got away too.

247
00:50:52,480 --> 00:50:54,198
I haven't done anything.

248
00:50:54,360 --> 00:50:55,588
Mehran.

249
00:50:56,000 --> 00:50:58,150
Please, tell him.

250
00:51:03,040 --> 00:51:06,635
Don't be afraid. 
We have nothing against you.

251
00:51:09,800 --> 00:51:11,119
Ali, my dear.

252
00:51:16,840 --> 00:51:19,479
You piece of shit.

253
00:51:19,640 --> 00:51:21,073
You worthless trash.

254
00:51:21,440 --> 00:51:23,670
Always the same mistakes.

255
00:52:01,600 --> 00:52:03,636
God is Great!

256
00:52:16,600 --> 00:52:18,591
God is Great!

257
00:52:31,520 --> 00:52:34,876
I have homework to do. 
Let's eat.

258
00:52:37,080 --> 00:52:39,071
Let's wait for your big brother.

259
00:52:46,920 --> 00:52:50,071
Thank you for the feast, Mom.

260
00:52:59,440 --> 00:53:01,556
Is there any more breast?

261
00:53:03,280 --> 00:53:04,793
Wait.

262
00:53:19,920 --> 00:53:20,875
Here you go.

263
00:53:40,280 --> 00:53:41,429
Azar,

264
00:53:43,200 --> 00:53:45,919
Don't you think the boy
has become strange?


265
00:53:47,040 --> 00:53:48,268
No...

266
00:53:48,720 --> 00:53:50,312
not at all.

267
00:53:54,560 --> 00:53:56,949
Didn't you want breast?.

268
00:53:59,480 --> 00:54:01,914
Is it because I used my hands?

269
00:54:03,920 --> 00:54:07,469
What? You think I'm dirty?

270
00:54:09,560 --> 00:54:12,233
I pray, so I know I'm clean.

271
00:54:12,400 --> 00:54:14,709
You, I'm not so sure of.

272
00:54:21,480 --> 00:54:25,473
Oh, you found some..
Thank you, Mommy.

273
00:54:31,240 --> 00:54:32,639
Pig.

274
00:54:57,080 --> 00:54:59,753
I know you're angry.

275
00:55:03,200 --> 00:55:04,872
I'm angry too.

276
00:55:05,080 --> 00:55:08,595
Cut. Joey...    That was good but....

277
00:55:08,960 --> 00:55:13,397
Make his voice more gay.
More gay. Could you do that?

278
00:55:13,560 --> 00:55:15,869
-More gay?
-Yes, please. More gay..

279
00:55:18,000 --> 00:55:19,194
Action!

280
00:55:25,400 --> 00:55:27,994
I know you're angry.

281
00:55:28,680 --> 00:55:30,398
I'm angry too.

282
00:55:30,560 --> 00:55:31,959
OK.... cut.
  

283
00:55:33,120 --> 00:55:34,838
Joey, he's gay.

284
00:55:35,000 --> 00:55:37,355
But not that gay.

285
00:55:37,520 --> 00:55:40,512
OK, OK. Let's do it again.
Action.!

286
00:55:43,280 --> 00:55:46,238
I know you're angry.

287
00:55:50,560 --> 00:55:52,471
I'm angry too.

288
00:55:54,160 --> 00:55:58,870
Let's march the streets of San Francisco
and share our anger.

289
00:57:16,240 --> 00:57:17,673
Yes. Who's your Daddy?

290
00:57:18,120 --> 00:57:19,348
Yeah.

291
00:57:20,400 --> 00:57:22,197
Oh! Say my name.

292
00:57:22,400 --> 00:57:23,913
OK, kids,   cut...    Cut...

293
00:57:24,080 --> 00:57:26,275
Guys,  Cut. Cut! Cut!

294
00:57:26,440 --> 00:57:27,668
Guys,  Cut. Cut! Cut!

295
00:57:27,840 --> 00:57:30,877
Joey, less.
Guys aren't that loud.

296
00:57:31,040 --> 00:57:33,031
Shireen, breathe more.
Exhale.

297
00:57:33,200 --> 00:57:38,115
It's not a Lamaze class.
You're not giving birth.

298
00:57:38,280 --> 00:57:39,679
Yeah. OK. Action!

299
00:57:51,040 --> 00:57:53,793
I love your body.


300
00:57:58,080 --> 00:57:59,718
Yeah, again.

301
00:58:00,640 --> 00:58:02,790
- Your left hand.
- Cut. Cut.

302
00:58:07,120 --> 00:58:08,553
Fuck me.

303
00:58:09,080 --> 00:58:10,911
- Will you fuck me?
- Yeah

304
00:58:11,080 --> 00:58:12,513
- Fuck me.
- Yeah

305
00:58:12,680 --> 00:58:14,591
- Fuck Me
- Yeah

306
00:58:18,560 --> 00:58:20,391
Cut, cut, cut!

307
00:58:22,000 --> 00:58:25,913
OK, Joey, please...let me direct.
There's one director now.

308
00:58:28,320 --> 00:58:30,470
Why are you making donkey sounds?

309
00:58:30,640 --> 00:58:33,359
Ladies here don't make those sounds.

310
00:58:33,520 --> 00:58:36,478
Maybe you just don't know
how to produce them.

311
00:58:59,440 --> 00:59:02,432
From now on,
my name is Samantha.

312
00:59:04,080 --> 00:59:07,038
Tonight, you're going to fuck me.

313
00:59:07,200 --> 00:59:09,998
- Fuck me.
- Fuck me.

314
00:59:10,160 --> 00:59:13,516
Fuck me.
Why won't you fuck me?

315
00:59:13,680 --> 00:59:16,114
- Yeah, baby.
- Yeah, baby.

316
00:59:41,160 --> 00:59:43,037
Get Her... 

317
00:59:43,200 --> 00:59:44,599
Pull over.

318
00:59:54,760 --> 00:59:56,557
Don't be afraid, my child.

319
00:59:57,080 --> 00:59:59,878
You're not responsible
for your parents' mistakes.

320
01:00:00,800 --> 01:00:03,678
Is it true.. 

321
01:00:04,320 --> 01:00:06,754
your parents were writers?

322
01:00:07,040 --> 01:00:09,031
Anti-revolutionary writers?

323
01:00:10,480 --> 01:00:14,314
Face the wall, you whore.
You have no right to look at us.
Understand?

324
01:00:14,480 --> 01:00:16,198
Answer his question.

325
01:00:17,040 --> 01:00:21,397
These tactics might work with your clients,
but they don't work here.

326
01:00:21,560 --> 01:00:23,437
Answer his question.

327
01:00:23,960 --> 01:00:26,474
My parents were professors.

328
01:00:27,320 --> 01:00:30,437
Do you think you can avenge
their deaths by doing this?

329
01:00:32,400 --> 01:00:35,551
Is my brother telling a joke?

330
01:00:35,720 --> 01:00:39,235
Hung from a rope,
you won't be laughing.

331
01:00:58,640 --> 01:01:03,634
You know in these cases
of anti-government activity,

332
01:01:05,120 --> 01:01:09,636
we're forced to do
a complete physical examination.

333
01:01:11,360 --> 01:01:13,715
With what right?
You had no such permission.

334
01:01:13,920 --> 01:01:16,480
We don't need your permission.

335
01:01:16,640 --> 01:01:18,676
Especially you,
who can't control his daughter.

336
01:01:18,840 --> 01:01:20,273
Do you know who you're talking to?

337
01:01:20,440 --> 01:01:24,592
You're not in a position
to play that card.

338
01:01:26,640 --> 01:01:28,232
Please, take a seat.

339
01:01:39,320 --> 01:01:41,072
Why is it

340
01:01:42,120 --> 01:01:46,033
your 16-year-old daughter
is not a virgin, but...

341
01:01:46,200 --> 01:01:48,589
Know your place.

342
01:01:49,440 --> 01:01:51,874
Money brings power, I know..

343
01:01:52,040 --> 01:01:56,397
But you're not responsible enough
to control your own daughter.

344
01:02:16,840 --> 01:02:19,229
I once had a daughter too.

345
01:02:20,320 --> 01:02:22,390
God rest her soul.

346
01:02:23,040 --> 01:02:25,759
She's not dead... She's alive.

347
01:02:27,040 --> 01:02:28,632
She lives on the other side
of the ocean.

348
01:02:31,200 --> 01:02:33,350
But for me, she is dead.

349
01:02:41,320 --> 01:02:44,118
Raising a daughter
is difficult, I know..

350
01:02:44,960 --> 01:02:46,632
It costs so much.

351
01:02:47,400 --> 01:02:48,674
Yes, a lot.

352
01:02:52,000 --> 01:02:54,389
So we understand each other.

353
01:03:04,480 --> 01:03:06,232
Completely.

354
01:03:07,880 --> 01:03:10,872
How much to forget this?

355
01:03:11,920 --> 01:03:14,593
We're all forced to play this game.

356
01:03:16,600 --> 01:03:19,353
You should be ashamed.

357
01:03:22,000 --> 01:03:26,357
You're all garbage, filthy whores..

358
01:03:28,400 --> 01:03:29,913
Answer me.

359
01:03:30,920 --> 01:03:32,399
Answer me.

360
01:03:33,040 --> 01:03:35,235
Are you deaf or dumb?

361
01:03:35,400 --> 01:03:38,278
And you call yourself an actress?

362
01:03:38,440 --> 01:03:43,958
You think, because of who your father is,
you can do whatever you want?

363
01:03:44,120 --> 01:03:45,678
Where is Shireen?

364
01:03:59,760 --> 01:04:04,117
San Francisco 
Firouz

365
01:04:26,760 --> 01:04:28,990
Shireen, do you need a ride home?

366
01:04:29,160 --> 01:04:32,118
My uncle is coming
with the deed to his house.

367
01:04:32,440 --> 01:04:34,078
A generous uncle.

368
01:04:39,920 --> 01:04:43,913
Sister, your parents are here.

369
01:04:44,560 --> 01:04:46,232
Fix your headscarf.
Good girl.

370
01:04:55,440 --> 01:04:57,192
jI'll stay with you,

371
01:04:58,560 --> 01:05:00,312
until your uncle comes.

372
01:05:01,520 --> 01:05:03,238
He is going to kill me.

373
01:05:14,440 --> 01:05:16,317
What's so funny?

374
01:05:20,320 --> 01:05:23,312
You can go.
I took care of everything.

375
01:05:25,520 --> 01:05:26,839
How?

376
01:05:30,640 --> 01:05:32,596
I could do something,..

377
01:05:32,760 --> 01:05:35,638
so it all disappears
from your record.

378
01:05:35,800 --> 01:05:38,951
And you won't even
have to go to court.

379
01:05:47,200 --> 01:05:49,839
My face is burning.

380
01:05:50,000 --> 01:05:52,309
So damn hot in here.

381
01:05:53,000 --> 01:05:55,275
Not as hot as hell.

382
01:06:12,760 --> 01:06:16,355
Atafeh, this time...
I'll forgive you.

383
01:06:17,160 --> 01:06:20,118
We're all human.
We make mistakes.

384
01:06:21,560 --> 01:06:26,953
But if we repeat the same mistakes,

385
01:06:28,560 --> 01:06:32,109
After months of investigation,

386
01:06:32,280 --> 01:06:35,716
the Revolutionary Guards
were able to infiltrate

387
01:06:35,880 --> 01:06:40,317
a secret anti-government
political cell...

388
01:06:40,480 --> 01:06:41,754
Shireen.

389
01:06:43,200 --> 01:06:45,316
Now you must get married.

390
01:06:45,680 --> 01:06:48,433
No more discussion.
That's that.

391
01:06:51,120 --> 01:06:53,634
The leader of the spies
is Hossein Shekarkhar,

392
01:06:53,800 --> 01:06:58,715
Born in Ohio
and a Harvard University student.

393
01:06:58,880 --> 01:07:03,908
Using his dual citizenship as a guise,
he was able to conduct his covert spying operation

394
01:07:04,120 --> 01:07:08,511
sponsored by the American
and lsraeli governments.

395
01:07:08,720 --> 01:07:11,837
This dangerous American spy

396
01:07:12,040 --> 01:07:15,999
is set to stand trial
at the Freedom Courts.

397
01:07:27,320 --> 01:07:30,073
Hurry. A five-hour hike
is no joke.

398
01:07:30,240 --> 01:07:31,673
Yes, Doctor.

399
01:07:42,360 --> 01:07:43,839
Madame Elle.

400
01:07:46,200 --> 01:07:49,078
I have guests today,
I need your help..

401
01:08:00,520 --> 01:08:05,833
-Look at the beautiful blossoms.
-You never used to be this slow!

402
01:08:06,120 --> 01:08:08,918
Respect your parents!

403
01:08:09,200 --> 01:08:12,033
I'll show you.

404
01:08:16,240 --> 01:08:19,391
You think you can beat me?

405
01:08:24,600 --> 01:08:29,276
Let's take a tiny break here..

406
01:08:32,720 --> 01:08:34,472
Like it?

407
01:08:38,480 --> 01:08:40,391
Have some water.

408
01:08:44,080 --> 01:08:47,595
Save it for yourself.

409
01:08:48,440 --> 01:08:50,271
I have plenty.

410
01:08:50,480 --> 01:08:52,630
Don't squander it
on an old man.

411
01:08:52,840 --> 01:08:55,274
Just have some, Daddy.

412
01:08:55,840 --> 01:08:58,991
I used to come here during college.

413
01:08:59,160 --> 01:09:02,277
When we weren't protesting
in the streets,

414
01:09:02,480 --> 01:09:06,632
we would rip off our clothes
and jump in the water.

415
01:09:06,960 --> 01:09:08,791
We'd create such a ruckus.

416
01:09:08,960 --> 01:09:13,636
The park ranger
would chase us with his baton.

417
01:09:13,800 --> 01:09:15,279
"You bastards!"

418
01:09:15,440 --> 01:09:19,831
We would grab our stuff
and run buck naked to our car.


419
01:09:22,520 --> 01:09:25,080
I was once rebellious.

420
01:09:25,280 --> 01:09:28,033
I know what it means to be young.

421
01:09:28,440 --> 01:09:30,112
Are you done?

422
01:09:31,040 --> 01:09:36,512
- God, see how she treats her father?
- What's God got to do with it?

423
01:09:37,280 --> 01:09:38,599
Don't be disrespectful.

424
01:09:38,760 --> 01:09:41,593
You all created this world for us

425
01:09:42,240 --> 01:09:44,435
with that revolution of yours.

426
01:09:44,960 --> 01:09:47,428
Now we're forced to live
under these circumstances.

427
01:09:47,600 --> 01:09:49,909
Have I done so wrong?

428
01:09:51,080 --> 01:09:53,640
I've done all this because of you

429
01:10:09,320 --> 01:10:11,470
What's gotten into you?

430
01:10:12,040 --> 01:10:16,716
You have everything at your disposal.
If you think somewhere else is better, go!


431
01:10:18,560 --> 01:10:21,552
If you're tired, go back.
I'll continue alone.

432
01:10:21,720 --> 01:10:24,518
Rush up, rush down.
Rush up, rush down.

433
01:10:24,680 --> 01:10:27,035
What's the point?

434
01:10:27,200 --> 01:10:30,112
It's exercise,
it's good for your heart.

435
01:10:35,320 --> 01:10:37,834
You know what's good
for my heart?

436
01:10:38,680 --> 01:10:41,148
Being with my daughter.

437
01:10:41,400 --> 01:10:44,039
You're getting sentimental
in your old age.

438
01:10:52,480 --> 01:10:53,879
Ready?

439
01:10:56,040 --> 01:10:57,951
Why don't you go on ahead?

440
01:10:58,720 --> 01:11:01,393
Like you suggested,
I'll go home.

441
01:11:04,920 --> 01:11:06,717
Are you sure, Dad?

442
01:11:11,680 --> 01:11:14,831
Are you sure?
Yes, I am sure..

443
01:11:15,440 --> 01:11:18,273
- At least, take the water.
- No need.

444
01:11:19,160 --> 01:11:21,549
- Daddy.
- Go, Atie.
445
01:12:52,000 --> 01:12:54,070
Why didn't you tell me?

446
01:12:54,840 --> 01:12:57,877
Nothing has happened.
They're here to talk.

447
01:13:32,800 --> 01:13:34,791
I just don't understand.

448
01:13:35,240 --> 01:13:37,913
She's ruining herself.

449
01:13:38,480 --> 01:13:40,357
Mehran, out.

450
01:13:41,040 --> 01:13:43,156
- Out.
- I'm going.

451
01:13:49,920 --> 01:13:53,356
- Why did you get me out?
- Should we have left you there?

452
01:13:53,760 --> 01:13:56,069
You don't even know what it means
to be in a place like that.


453
01:13:56,240 --> 01:13:58,390
You're not a child anymore.

454
01:13:58,560 --> 01:14:01,154
Stop these games..

455
01:14:01,360 --> 01:14:03,635
I didn't raise you like this.

456
01:14:14,800 --> 01:14:17,678
Don't get caught up
in these things.

457
01:14:20,720 --> 01:14:23,792
At least do something of worth.

458
01:14:27,320 --> 01:14:29,038
Sometimes,

459
01:14:29,520 --> 01:14:31,988
we have to accept our reality.

460
01:14:33,760 --> 01:14:34,954
Ateh...

461
01:14:35,720 --> 01:14:38,792
Atafeh, my beautiful little girl.

462
01:14:39,440 --> 01:14:41,476
My sweet baby.

463
01:14:42,080 --> 01:14:44,514
Remember how I used
to sing to you?

464
01:15:17,840 --> 01:15:21,515
With the permission
of my uncle and grandmother,

465
01:15:22,920 --> 01:15:24,148
I accept.

466
01:20:16,960 --> 01:20:18,837
I bought it today.

467
01:20:20,720 --> 01:20:22,278
It's nice.

468
01:20:24,160 --> 01:20:28,438
With the payment from my first contract,
I will build a six-bedroom home, 

469
01:20:28,600 --> 01:20:30,113
for me,

470
01:20:31,160 --> 01:20:32,718
Shireen

471
01:20:33,640 --> 01:20:37,633
and, God willing,
our children.

472
01:20:44,320 --> 01:20:47,869
Azar, I'm going to Vanak
if you want a ride.....

473
01:20:59,560 --> 01:21:00,913
shireen...

474
01:21:01,480 --> 01:21:05,029
can you imagine it?

475
01:21:05,760 --> 01:21:08,115
Imagine here....

476
01:21:08,920 --> 01:21:13,357
huge windows facing the garden.

477
01:21:14,840 --> 01:21:16,796
And here,

478
01:21:18,320 --> 01:21:20,515
we could plant jasmine trees.

479
01:21:22,240 --> 01:21:23,912
Do you like jasmine trees?

480
01:21:28,240 --> 01:21:30,629
- What do you like?
- Mehran...

481
01:21:30,800 --> 01:21:33,360
I haven't talked to Atie
in so long.


482
01:21:34,120 --> 01:21:35,348
Can I go with her?

483
01:21:35,520 --> 01:21:37,750
- Sweetheart, no.
- Why

484
01:21:37,920 --> 01:21:40,229
Atafeh and her friends
got you into trouble.

485
01:21:40,400 --> 01:21:42,595
- But Mehran.
- Let's go.

486
01:23:11,680 --> 01:23:13,557
I can't do this anymore.

487
01:23:14,240 --> 01:23:16,196
Nothing has changed.

488
01:23:17,320 --> 01:23:19,117
I love you.

489
01:23:21,600 --> 01:23:23,750
How can you say that?

490
01:23:40,160 --> 01:23:42,310
Is it better if I were in jail?

491
01:23:42,560 --> 01:23:44,516
Or here with you?

492
01:23:45,800 --> 01:23:48,234
I did this all for you.

493
01:23:56,880 --> 01:24:01,192
Firouz darling,
bring the wine for our guests.

494
01:24:19,240 --> 01:24:21,515
I want to play you something.

495
01:25:12,440 --> 01:25:15,591
I learned this dish at Berkeley.

496
01:25:15,760 --> 01:25:18,228
Very, very special.

497
01:25:20,480 --> 01:25:22,232


498
01:25:22,400 --> 01:25:23,719
OK.

499
01:25:24,360 --> 01:25:28,478
Mehran, Atafeh.
A song for your mother.

500
01:25:28,960 --> 01:25:30,518
No,

501
01:25:30,680 --> 01:25:36,437
Shireen wrote a lovely new song
that she must perform for us.

502
01:25:36,640 --> 01:25:38,312
Sing it for us, my dear.

503
01:25:39,080 --> 01:25:41,799
Shireen is talented, isn't she?

504
01:25:42,520 --> 01:25:45,876
Absolutely,
talented and beautiful.

505
01:25:49,320 --> 01:25:51,550
But I don't like my wife
singing in public.

506
01:25:56,120 --> 01:25:58,475
But it's just us here.

507
01:25:58,720 --> 01:26:01,598
Still, I don't want my wife to sing.

508
01:26:02,720 --> 01:26:04,551
You're ridiculous.

509
01:26:05,360 --> 01:26:08,636
Atafeh shouldn't sing either.

510
01:26:09,240 --> 01:26:10,593
Mehran.

511
01:26:10,960 --> 01:26:13,076
But that's up to you.

512
01:26:13,240 --> 01:26:16,676
You control your daughter,
I'll control my wife.

513
01:26:18,080 --> 01:26:21,152
Have you become a fanatic?
Going to turn us all in tonight?

514
01:26:21,320 --> 01:26:23,117
Iaisse.

515
01:26:23,280 --> 01:26:26,636
It's not a concert,
it's family and friends.


516
01:26:26,840 --> 01:26:29,798
Atafeh and I will sing.

517
01:26:31,480 --> 01:26:32,708
Ok...my darling?.

518
01:29:02,360 --> 01:29:04,590
"l can't do this anymore."

519
01:29:11,400 --> 01:29:13,550
l love you.

520
01:29:13,720 --> 01:29:15,711
Nothing has changed.

521
01:29:18,320 --> 01:29:20,709
"How can you say that?"

522
01:29:26,000 --> 01:29:27,911
"How can you say that?"

523
01:29:28,080 --> 01:29:30,640
ls it better if I were in jail,
or here with you?

524
01:29:30,800 --> 01:29:33,030
I did this all for you.

525
01:30:05,680 --> 01:30:08,194
Why can't I be enough?

526
01:30:09,160 --> 01:30:12,072
Why can't I be enough?

527
01:30:12,440 --> 01:30:13,919
Why?.

528
01:30:18,120 --> 01:30:22,193
Why can't I be enough?

529
01:33:43,760 --> 01:33:45,352
Mr. Tabrizi?

530
01:33:46,200 --> 01:33:48,236
Visa and ticket for Dubai.

531
01:33:48,400 --> 01:33:51,551
Father's permission
stamped in your passport?


532
01:34:06,640 --> 01:34:09,234
Tonight at nine.

533
01:35:03,000 --> 01:35:04,638
Shireen?

534
01:35:07,720 --> 01:35:10,871
What's wrong, Shireen?
He Left....

535
01:35:11,080 --> 01:35:12,433
What's wrong?

536
01:35:13,320 --> 01:35:15,959
Shireen.

537
01:35:17,400 --> 01:35:20,312
What's wrong? We're alone.

538
01:35:20,480 --> 01:35:22,391
We're alone today.

539
01:35:22,920 --> 01:35:24,831
What's wrong with you?

540
01:35:25,680 --> 01:35:27,033
What's wrong?

541
01:35:27,520 --> 01:35:28,794
Are you afraid ?

542
01:35:28,960 --> 01:35:30,712
What are you afraid of?

543
01:35:31,160 --> 01:35:32,718
What is it?

544
01:35:40,160 --> 01:35:42,435
You afraid of this?

545
01:35:43,440 --> 01:35:46,830
Is that it, Shireen,
You afraid of this?

546
01:35:50,120 --> 01:35:54,033
Why? You are afraid of this?

547
01:35:54,280 --> 01:35:57,113
Is that what you are afraid of?

548
01:35:57,520 --> 01:35:59,317
You afraid of this?

549
01:36:00,240 --> 01:36:02,310
Why?

550
01:36:59,440 --> 01:37:01,271
We can leave....

551
01:38:27,960 --> 01:38:31,509
Subtitles By DeUSMAN@Hotmail.com

