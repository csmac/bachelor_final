1
00:04:16,400 --> 00:04:18,789
Move it! We're late!

2
00:04:21,800 --> 00:04:24,792
I can't figure out what you do
and we're late every time

3
00:04:30,520 --> 00:04:32,158
Hello Nikolo!

4
00:04:32,480 --> 00:04:34,038
Hello Maestro!

5
00:04:34,400 --> 00:04:36,755
What kind of angels did you 
paint this time Maestro?

6
00:04:37,040 --> 00:04:38,996
C'mon and have a drink with us!
Our treat!

7
00:04:43,080 --> 00:04:45,230
Down! Down, you wench!

8
00:04:50,320 --> 00:04:53,118
So, you were working all night?
I know this kind of "work".

9
00:04:54,360 --> 00:04:55,793
Hello, highest of Crete!

10
00:04:56,680 --> 00:04:58,193
To our health!

11
00:07:37,040 --> 00:07:38,996
I can buy three paintings at this price!

12
00:09:51,440 --> 00:09:52,793
You won't go?

13
00:09:53,080 --> 00:09:54,718
Everyone in Crete will be there,
but you won't go?

14
00:09:55,000 --> 00:09:57,309
Throw it away! I won't go!

15
00:10:01,120 --> 00:10:04,430
Yes, you will go!

16
00:10:06,080 --> 00:10:08,036
I can't leave you alone.

17
00:10:08,680 --> 00:10:12,753
You will go to the dance.
Nikolos will be with you.

18
00:10:13,680 --> 00:10:16,035
He will have his eyes open.

19
00:10:16,440 --> 00:10:18,351
Da Rimi is up to something.

20
00:10:18,760 --> 00:10:20,637
Listen to your father.

21
00:11:26,880 --> 00:11:28,950
Remember why we came here.

22
00:11:29,280 --> 00:11:32,272
I do, but do you?

23
00:15:25,200 --> 00:15:27,998
This time we won...

24
00:15:28,720 --> 00:15:30,278
...but we lost many

25
00:15:32,320 --> 00:15:35,596
Next time...who knows.

26
00:15:56,000 --> 00:15:58,468
For the dead!

27
00:15:58,840 --> 00:16:00,193
May they fare well!

28
00:16:00,640 --> 00:16:02,278
May they fare well!

29
00:16:19,280 --> 00:16:23,068
He must leave. You will go with him.

30
00:16:25,920 --> 00:16:29,674
From here on...
you will be his guardian.

31
00:16:33,720 --> 00:16:35,073
Play!

32
00:18:37,160 --> 00:18:39,628
He's in a dark mood again

33
00:18:39,960 --> 00:18:42,235
Like the sky in this town: Black

34
00:19:20,800 --> 00:19:22,870
Because life takes you where she wants

35
00:19:23,120 --> 00:19:24,678
your job is to paint

36
00:19:25,000 --> 00:19:27,639
That's what your father says,
and that's what I say too.

37
00:25:07,440 --> 00:25:08,714
What are you doing here?

38
00:25:08,960 --> 00:25:11,349
I escaped from Crete, and I'm looking
for my older brother.

39
00:25:11,680 --> 00:25:12,829
My ship sails at dawn

40
00:25:13,080 --> 00:25:14,593
What's new? Father?

41
00:25:19,040 --> 00:25:20,678
What?

42
00:25:24,840 --> 00:25:26,831
Da Rimi set us up.

43
00:25:27,640 --> 00:25:29,870
At a wedding.
They poisoned the wine.

44
00:25:30,800 --> 00:25:32,392
Everyone's dead.

45
00:26:56,040 --> 00:26:59,589
It's not finished yet.
You have to sign it.

46
00:27:06,440 --> 00:27:08,908
Dominikos Theotokopoulos

47
00:38:31,600 --> 00:38:33,158
The Cretan temperament.

48
00:39:56,960 --> 00:40:01,351
Lord, come to me, please

49
00:40:03,760 --> 00:40:10,279
He is here, for me!

50
00:40:50,960 --> 00:40:53,554
The Lord comes in the middle of the night...

51
00:40:54,040 --> 00:40:56,998
...and your lights will be out, 
foulish virgins.

52
00:40:59,760 --> 00:41:03,673
He's our Lord...
and he is beautiful...

53
00:41:12,400 --> 00:41:15,278
In our days, everyone thinks 
he can talk to God

54
00:41:16,680 --> 00:41:19,319
Finally, they won't be 
needing us anymore

55
00:41:20,080 --> 00:41:21,718
What kind of farce is this?

56
00:41:22,680 --> 00:41:26,116
She is not well, that's obvious,
but she is not a witch.

57
00:41:26,600 --> 00:41:29,558
You are still yound.
That's understandable.

58
00:41:32,840 --> 00:41:34,637
But be careful Guevara.

59
00:41:35,440 --> 00:41:38,955
People in high places
watch your every move.

60
00:41:41,040 --> 00:41:42,598
Don't let them down.

61
00:41:46,160 --> 00:41:48,196
He is here now.

62
00:41:49,520 --> 00:41:51,750
He is everywhere.

63
00:41:55,600 --> 00:41:59,434
Why don't you see him?
Why can't you see him?

64
00:42:07,880 --> 00:42:09,757
What will happen to her?

65
00:42:10,920 --> 00:42:12,558
Well, she...

66
00:42:13,600 --> 00:42:17,878
...will be your first decision.

67
00:48:27,680 --> 00:48:30,877
Beauty!
Is there art without beauty?

68
00:49:10,040 --> 00:49:11,712
I don't like him.

69
00:49:19,320 --> 00:49:21,515
- Are you going for a confession;
- Excuse me, I don't want to be late.

70
00:49:21,800 --> 00:49:24,394
- Have you commited many sins?
- I haven't done anything bad!

71
00:49:24,640 --> 00:49:26,153
Not yet.

72
00:49:26,440 --> 00:49:28,670
But the biggest sin is
to obstruck his work

73
00:49:28,960 --> 00:49:31,349
He doesn't belong to you.
He belongs to God.

74
00:49:31,720 --> 00:49:35,190
You play with fire
senorita De Las Cuevas

75
00:49:35,640 --> 00:49:37,437
- How dare you!
- Listen to me!

76
00:49:38,120 --> 00:49:41,317
He pretends to know everything.
He needs someone to guide him.

77
00:49:41,640 --> 00:49:43,153
And it cannot be you.

78
00:49:43,600 --> 00:49:45,955
He needs to believe in something.

79
00:49:53,400 --> 00:49:55,311
He believes in life!

80
00:49:55,800 --> 00:49:57,074
In life!

81
00:50:09,800 --> 00:50:12,155
Are you the Lord...

82
00:50:12,880 --> 00:50:15,314
...that comes in the middle of the night?

83
00:50:17,760 --> 00:50:19,557
Yes.

84
00:50:21,440 --> 00:50:22,759
���.

85
00:50:31,840 --> 00:50:34,673
We are waiting for your verdict.

86
00:50:40,800 --> 00:50:42,153
To the Pyre!

87
00:50:54,080 --> 00:50:55,877
Nino De Guevara

88
00:50:57,600 --> 00:51:00,831
...welcome, Grand Inquisitor.

89
00:52:49,640 --> 00:52:51,551
- What did you see?
- Nothing.

90
00:52:59,320 --> 00:53:02,198
I shall see her.
Again.

91
00:58:19,360 --> 00:58:21,237
My hand is asleep dad.

92
01:04:00,360 --> 01:04:03,193
You have to help me.

93
01:04:03,760 --> 01:04:07,116
The Inquisition arrested my husband,
the baker Karkadil

94
01:04:07,760 --> 01:04:11,275
He is Greek,
he doesn't speak Spanish

95
01:04:35,560 --> 01:04:38,757
Sweetheart!

96
01:04:41,760 --> 01:04:45,150
Talk to me, talk to me!

97
01:19:01,600 --> 01:19:03,477
Why don't you talk to me?

98
01:19:05,400 --> 01:19:07,675
Why don't you ever talk to me?

