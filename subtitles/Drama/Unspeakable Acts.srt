1
00:02:21,909 --> 00:02:23,408
Coffee?

2
00:02:29,516 --> 00:02:30,883
You know
what you want?

3
00:02:35,255 --> 00:02:36,588
I'll be right back.

4
00:03:14,294 --> 00:03:16,428
Man:
...Libel if it's true.

5
00:03:16,430 --> 00:03:18,463
We've got to tell the client
we can't do this work.

6
00:03:18,465 --> 00:03:21,934
We file this lawsuit,
i swear...

7
00:03:24,438 --> 00:03:26,838
Thank you.
Have nice day.

8
00:03:26,840 --> 00:03:28,407
He's sleeping
with Anita.

9
00:03:28,409 --> 00:03:29,808
No way.
She's not even hot.

10
00:03:29,810 --> 00:03:31,343
Like I care.

11
00:03:33,514 --> 00:03:35,747
Come on, guys.
They traded this prick

12
00:03:35,749 --> 00:03:38,650
for three draft picks,
none of them worth spit.

13
00:03:40,588 --> 00:03:42,588
Sit up straight
and eat your food.

14
00:03:42,590 --> 00:03:45,857
You never listen.
All you do is fool around.

15
00:03:45,859 --> 00:03:48,260
You never take
anything seriously.
Sorry, dad.

16
00:04:13,653 --> 00:04:15,020
Can I help you?

17
00:04:18,491 --> 00:04:20,459
How about you just turn
your ass around

18
00:04:20,461 --> 00:04:21,860
and get the fuck
out of here?

19
00:04:23,796 --> 00:04:25,864
You just back up, there.

20
00:04:55,495 --> 00:04:58,363
Man:
<i> Do I look crazy to you?</i>

21
00:04:58,365 --> 00:05:00,899
<i> Unhinged? Maybe I am.</i>

22
00:05:00,901 --> 00:05:02,868
<i> But before you rush to</i>
<i> judgment,</i>

23
00:05:02,870 --> 00:05:04,536
<i> let's rewind a bit.</i>

24
00:05:25,758 --> 00:05:28,460
Which is why he's gonna choose
our firm to defend him

25
00:05:28,462 --> 00:05:30,462
and not some cut-rate llp

26
00:05:30,464 --> 00:05:32,564
with a radio commercial
and a catchy jingle.

27
00:05:32,566 --> 00:05:35,634
Man:<i> Women speak approximately</i>
<i> 20,000 words a day.</i>

28
00:05:35,636 --> 00:05:38,337
Yeah, you know,
some not guilty.

29
00:05:38,339 --> 00:05:41,406
I want to spend an extra
million or two on that,
all right?

30
00:05:41,408 --> 00:05:44,609
<i> Men, 17,000.</i>

31
00:05:44,611 --> 00:05:46,912
No, no, no.
No, no, no, no, no.

32
00:05:46,914 --> 00:05:49,981
This is not what I think.
This is what it is.

33
00:05:49,983 --> 00:05:52,751
Another victim
of the systemic racism

34
00:05:52,753 --> 00:05:54,720
inherent
in the police force.

35
00:05:54,722 --> 00:05:57,089
Man:<i> I spoke around</i>
<i> 80,000 words a day.</i>

36
00:05:57,091 --> 00:06:00,959
...my two favorite
letters: N-g.

37
00:06:00,961 --> 00:06:02,594
Not guilty.

38
00:06:02,596 --> 00:06:04,963
<i> Words were my weapon</i>
<i> of choice.</i>

39
00:06:06,766 --> 00:06:10,802
<i> But out of those 80,000 words</i>
<i> I spoke a day,</i>

40
00:06:10,804 --> 00:06:13,071
<i> you know how many</i>
<i> are really important?</i>

41
00:06:14,807 --> 00:06:15,941
<i> Three.</i>

42
00:06:15,943 --> 00:06:18,410
I love you.

43
00:06:18,412 --> 00:06:19,678
Come here.

44
00:06:21,848 --> 00:06:24,549
<i> Three words total</i>
<i> out of 80,000.</i>

45
00:06:25,685 --> 00:06:27,519
I love you.

46
00:06:27,521 --> 00:06:29,588
<i> Not a good ratio.</i>

47
00:06:49,108 --> 00:06:51,743
Frank, you were supposed to
be here a half an hour ago.

48
00:06:51,745 --> 00:06:53,445
The place is really
starting to fill up.

49
00:06:53,447 --> 00:06:55,046
I know, I know.
I'm sorry.

50
00:06:55,048 --> 00:06:57,048
I'm heading down
to the garage right now.

51
00:06:57,050 --> 00:06:58,917
<i>Olivia wants</i>
<i>to talk to you.</i>

52
00:07:02,588 --> 00:07:03,555
Hi, daddy.

53
00:07:04,757 --> 00:07:06,725
Hi, sweetheart.

54
00:07:06,727 --> 00:07:08,960
<i> You're coming, right?</i>

55
00:07:08,962 --> 00:07:11,530
I'm on my way.
<i> Good.</i>

56
00:07:11,532 --> 00:07:14,166
'Cause I think you're gonna
love my performance.

57
00:07:14,168 --> 00:07:15,967
<i> Promise you'll be here?</i>

58
00:07:15,969 --> 00:07:17,569
I promise.

59
00:07:17,571 --> 00:07:19,204
<i> I love you.</i>

60
00:07:21,841 --> 00:07:23,642
Shit.

61
00:07:25,945 --> 00:07:27,612
Wait. Wait.

62
00:07:33,820 --> 00:07:35,821
Uh, frank,
you got a minute?

63
00:07:54,540 --> 00:07:57,542
Okay, you've got to sign
these too. We've got to
ship them out tonight.

64
00:07:57,544 --> 00:08:00,846
Luckily there was a case
argued just recently
on the supreme court.

65
00:08:00,848 --> 00:08:03,582
For three reasons
acquittals should have
full effect

66
00:08:03,584 --> 00:08:05,550
under the double
jeopardy clause...

67
00:08:09,155 --> 00:08:12,123
Olivia, you're next.

68
00:08:46,259 --> 00:08:48,727
This is my daddy's
favorite song.

69
00:08:48,729 --> 00:08:50,996
He and I always
sing it together.

70
00:10:00,766 --> 00:10:02,834
<i> Hi, it's sue.</i>
<i> Please leave a message.</i>

71
00:10:02,836 --> 00:10:05,770
Susan. Listen, I'm sorry.

72
00:10:05,772 --> 00:10:08,873
Things just got away
from me at work.

73
00:10:08,875 --> 00:10:10,675
Call me back, please.

74
00:10:10,677 --> 00:10:12,844
I'm sorry.

75
00:11:04,263 --> 00:11:06,731
<i>Hi, it's sue.</i>
<i>Please leave a message.</i>

76
00:11:06,733 --> 00:11:10,402
Sue. Listen, uh...

77
00:11:10,404 --> 00:11:12,871
Listen, I know
you're upset with me,

78
00:11:12,873 --> 00:11:15,373
but please call me back.

79
00:11:15,375 --> 00:11:18,743
It's getting late,
and I'm worried
about you guys, okay?

80
00:12:05,791 --> 00:12:07,759
Mr. valera,
I'm detective lustiger.

81
00:12:07,761 --> 00:12:09,260
Where are they?

82
00:12:09,262 --> 00:12:12,063
Are they okay?
Where are they?

83
00:12:12,065 --> 00:12:13,398
Mr. valera...

84
00:12:14,400 --> 00:12:16,067
Mr. valera...

85
00:12:19,071 --> 00:12:20,338
Mr. valera!

86
00:12:22,075 --> 00:12:24,309
We're still waiting
for the forensic team.

87
00:12:24,311 --> 00:12:27,078
Frank! Frank!

88
00:12:27,080 --> 00:12:29,814
I can't let you contaminate
the crime scene.

89
00:12:29,816 --> 00:12:30,749
Frank!

90
00:12:42,061 --> 00:12:45,330
My baby.
Th-that's my baby.

91
00:12:45,332 --> 00:12:48,900
That's my baby!
Hold him back!

92
00:12:48,902 --> 00:12:51,236
Let me go. Let me go!

93
00:12:52,838 --> 00:12:54,172
Let me go!

94
00:12:57,077 --> 00:12:59,177
Let me go!

95
00:12:59,179 --> 00:13:02,313
Get him back!
Get him back!

96
00:13:11,023 --> 00:13:13,925
The lord is my Shepherd;
I shall not want.

97
00:13:13,927 --> 00:13:16,895
He makes me lie down
in green pastures.

98
00:13:16,897 --> 00:13:19,330
He leads me beside
the still waters.

99
00:13:19,332 --> 00:13:21,232
He restores my soul.

100
00:13:21,234 --> 00:13:23,968
He leads me in the paths
of righteousness

101
00:13:23,970 --> 00:13:26,004
for his name's sake.

102
00:13:26,006 --> 00:13:29,307
Yea, though I walk through
the valley of the shadow
of death,

103
00:13:29,309 --> 00:13:31,242
I will fear no evil,

104
00:13:31,244 --> 00:13:33,278
for you are with me.

105
00:13:42,154 --> 00:13:46,124
Chances are they'll
never catch the people
who killed them.

106
00:13:46,126 --> 00:13:48,326
And even if
they do catch them,

107
00:13:48,328 --> 00:13:50,528
some slick-tongued
defense attorney

108
00:13:50,530 --> 00:13:52,497
will probably
get them off,

109
00:13:52,499 --> 00:13:55,500
some slick-tongued
defense attorney

110
00:13:55,502 --> 00:13:58,303
just like you.

111
00:14:01,073 --> 00:14:04,242
I always kept my tongue quiet
while my daughter was alive,

112
00:14:04,244 --> 00:14:07,545
but I never saw
what she saw in you.

113
00:14:07,547 --> 00:14:10,582
She left her family,
friends behind,

114
00:14:10,584 --> 00:14:13,251
she followed you
across the country,

115
00:14:13,253 --> 00:14:15,587
put your needs
ahead of her own,

116
00:14:15,589 --> 00:14:18,022
but as long as
my daughter was happy,

117
00:14:18,024 --> 00:14:20,458
I was happy.

118
00:14:20,460 --> 00:14:23,094
Now that
my daughter is dead,

119
00:14:23,096 --> 00:14:25,964
and my granddaughter,

120
00:14:25,966 --> 00:14:30,602
I'll tell you
what I think of you,
frank valera:

121
00:14:30,604 --> 00:14:33,605
You're all talk.

122
00:14:33,607 --> 00:14:36,574
You make your living
spinning words

123
00:14:36,576 --> 00:14:39,143
into meanings
that are vague

124
00:14:39,145 --> 00:14:42,413
and hazy and unclear,

125
00:14:42,415 --> 00:14:46,284
all just to exonerate
the scum of the earth.

126
00:14:46,286 --> 00:14:49,120
And from this moment on,

127
00:14:49,122 --> 00:14:52,090
I do not ever
want to hear

128
00:14:52,092 --> 00:14:55,059
your mealy-mouthed
voice again.

129
00:14:55,061 --> 00:14:58,296
Am I understood.

130
00:14:58,298 --> 00:15:01,466
Jeff...
I said,

131
00:15:01,468 --> 00:15:05,904
I never want to hear
your voice again.

132
00:15:07,539 --> 00:15:10,041
A nod will suffice.

133
00:17:52,838 --> 00:17:54,572
Frank:
<i> Without a break in the case,</i>

134
00:17:54,574 --> 00:17:57,075
<i> all the police could do</i>
<i> was talk.</i>

135
00:17:57,077 --> 00:17:59,644
<i> Empty words to appease.</i>
<i> A familiar game.</i>

136
00:17:59,646 --> 00:18:02,580
Your wife's car was
expertly wiped down.

137
00:18:02,582 --> 00:18:05,183
<i> My wife and daughter,</i>
<i> murdered.</i>

138
00:18:05,185 --> 00:18:07,285
<i> Their killer still out there</i>
<i> roaming the street.</i>

139
00:18:07,287 --> 00:18:08,553
Maybe they knew
what they were doing.

140
00:18:08,555 --> 00:18:13,257
<i> And all anyone could do</i>
<i> was... talk.</i>

141
00:18:13,259 --> 00:18:16,661
And there wasn't any evidence
at the crime scene, right?

142
00:18:16,663 --> 00:18:18,729
Found these tiny
gold fibers.

143
00:18:18,731 --> 00:18:20,164
Gold fibers?

144
00:18:22,301 --> 00:18:24,302
I-i don't think
my wife or Olivia

145
00:18:24,304 --> 00:18:27,305
were wearing anything gold
that night, were they?
No.

146
00:18:27,307 --> 00:18:30,575
So then,
that's a lead, right?

147
00:18:30,577 --> 00:18:34,445
Well, I talked to
a tailor downtown.

148
00:18:34,447 --> 00:18:37,648
He said it was
a very common thread.
It's not much of a lead.

149
00:18:40,252 --> 00:18:42,153
And there are still
no witnesses?

150
00:18:42,155 --> 00:18:44,555
None have come forward,
but we're still looking.

151
00:18:44,557 --> 00:18:46,424
That area is
a notorious stomping ground

152
00:18:46,426 --> 00:18:48,326
for the<i> oreversakonya.</i>

153
00:18:48,328 --> 00:18:49,360
What was that?

154
00:18:49,362 --> 00:18:51,829
Russian mafia.

155
00:18:51,831 --> 00:18:55,133
They have a stranglehold
in that area. It's hard
to get anyone to talk.

156
00:18:55,135 --> 00:18:57,368
Would it have been
the Russian mafia

157
00:18:57,370 --> 00:19:00,238
and not, like, robbery
or a carjacking
or something?

158
00:19:00,240 --> 00:19:03,641
You guys never found
my wife's purse.

159
00:19:03,643 --> 00:19:05,510
Pretty violent
for a purse-snatching,

160
00:19:05,512 --> 00:19:07,445
don't you think, frank?

161
00:19:07,447 --> 00:19:10,214
Or carjacking?

162
00:19:10,216 --> 00:19:11,916
Why'd they leave
the car?

163
00:19:11,918 --> 00:19:14,619
I don't know. Maybe something
went terribly wrong

164
00:19:14,621 --> 00:19:16,587
and they had to flee,
i don't know.

165
00:19:18,290 --> 00:19:20,291
We are exhausting
all leads.

166
00:19:20,293 --> 00:19:22,727
You don't need to keep
coming down here, frank.

167
00:19:22,729 --> 00:19:24,328
I'll call you if anything
comes up.

168
00:19:24,330 --> 00:19:26,430
I'll call you even
if something doesn't,

169
00:19:26,432 --> 00:19:28,566
just to keep you informed.

170
00:19:28,568 --> 00:19:31,502
Detective lustiger,

171
00:19:31,504 --> 00:19:33,671
will you please stop
texting, sir?

172
00:19:43,415 --> 00:19:45,616
Can I see the file
on the case?

173
00:19:49,421 --> 00:19:51,422
Chief wants a word
with you, bill.

174
00:19:51,424 --> 00:19:53,658
Tell him I'll be right there.

175
00:19:56,929 --> 00:19:57,962
Please.

176
00:19:59,464 --> 00:20:01,432
No.

177
00:20:01,434 --> 00:20:04,702
It's confidential.
It's police business.

178
00:20:04,704 --> 00:20:07,505
It is my wife
and my daughter.

179
00:20:07,507 --> 00:20:09,640
Frank, I am trying to find out

180
00:20:09,642 --> 00:20:11,842
who did this and to
bring them into justice.

181
00:20:11,844 --> 00:20:14,345
Well, try harder.
Goddamn it!

182
00:20:14,347 --> 00:20:15,780
You have any idea how often

183
00:20:15,782 --> 00:20:17,515
a violent crime is committed
in this country?

184
00:20:17,517 --> 00:20:18,583
No, I...

185
00:20:18,585 --> 00:20:20,918
Every 25.3 seconds,
frank.

186
00:20:20,920 --> 00:20:23,621
That means for the 25.3 minutes
you sat here,

187
00:20:23,623 --> 00:20:25,823
60 violent crimes
have been committed.

188
00:20:25,825 --> 00:20:26,724
Really?

189
00:20:32,231 --> 00:20:33,464
You okay?

190
00:20:35,234 --> 00:20:36,500
Am I okay?

191
00:20:38,270 --> 00:20:40,605
I'll never be okay.

192
00:20:40,607 --> 00:20:42,506
Mr. valera.

193
00:20:45,244 --> 00:20:46,577
We never give up.

194
00:20:48,247 --> 00:20:49,413
Yeah.

195
00:22:34,519 --> 00:22:36,754
Man: Kill him!
Man 2: Tackle him!

196
00:22:51,036 --> 00:22:53,637
Come on! Come on!

197
00:23:05,917 --> 00:23:07,685
Frank:
<i> It became clear to me</i>

198
00:23:07,687 --> 00:23:09,453
<i> that justice</i>
<i> would never be served.</i>

199
00:23:17,763 --> 00:23:20,498
<i> I made my family</i>
<i> a promise,</i>

200
00:23:20,500 --> 00:23:23,000
<i> but my words were as empty</i>
<i> as everyone else's.</i>

201
00:23:26,972 --> 00:23:29,707
<i> I was the guilty one.</i>

202
00:23:55,434 --> 00:23:57,701
Crowd:
Get him! Get him!
Get him! Get him!

203
00:24:16,455 --> 00:24:18,656
<i> I'd found</i>
<i> my form of penance.</i>

204
00:25:28,093 --> 00:25:29,894
What are you doing?

205
00:25:29,896 --> 00:25:31,161
<i> My associates suggested</i>

206
00:25:31,163 --> 00:25:33,597
<i> I take some time off.</i>

207
00:25:33,599 --> 00:25:35,799
<i> "Mental-health days,"</i>

208
00:25:35,801 --> 00:25:37,868
<i> they called it.</i>

209
00:25:37,870 --> 00:25:39,970
<i> Although I know</i>
<i> it was really because</i>

210
00:25:39,972 --> 00:25:43,274
<i> they didn't want me</i>
<i> around anymore.</i>

211
00:25:43,276 --> 00:25:46,277
<i> Frankly,</i>
<i> the feeling was mutual.</i>

212
00:25:50,015 --> 00:25:51,715
I need time.

213
00:25:51,717 --> 00:25:54,251
<i> Defending suspected criminals</i>
<i> was something</i>

214
00:25:54,253 --> 00:25:56,253
<i> I'd lost my taste for.</i>

215
00:26:01,993 --> 00:26:03,727
Break it up! Stop!

216
00:26:36,695 --> 00:26:38,596
I suppose you're wondering,

217
00:26:38,598 --> 00:26:41,332
what is a nice guy like me
doing in a place like this?

218
00:26:41,334 --> 00:26:43,601
Something like that,
yeah.

219
00:26:50,875 --> 00:26:54,011
I promised I'd meet them

220
00:26:54,013 --> 00:26:55,646
at my daughter's talent show
the night they were killed.

221
00:26:59,184 --> 00:27:00,784
I should've been there
for them.

222
00:27:00,786 --> 00:27:04,121
The last message
i ever got from my wife was

223
00:27:04,123 --> 00:27:07,925
a text of a video she took
of a performance I missed.

224
00:27:09,327 --> 00:27:12,630
Yeah.
Gave them my word,

225
00:27:12,632 --> 00:27:14,732
then I broke it.

226
00:27:16,635 --> 00:27:18,202
It's killing me.

227
00:27:21,139 --> 00:27:23,073
Frank, you can't blame yourself
for what happened.

228
00:27:23,075 --> 00:27:25,743
Oh, believe me, I can.

229
00:27:29,047 --> 00:27:31,081
What about you?

230
00:27:31,083 --> 00:27:33,083
You're not in
a police uniform,

231
00:27:33,085 --> 00:27:36,186
which means you're not here
to bust the fights up.

232
00:27:36,188 --> 00:27:38,022
You follow me here?

233
00:27:38,024 --> 00:27:40,791
This job, frank...

234
00:27:40,793 --> 00:27:43,961
It's a fucking
war zone out there.

235
00:27:43,963 --> 00:27:47,364
The way people feel
about cops,

236
00:27:47,366 --> 00:27:50,868
sometimes I just gotta
blow off a little steam,
you know?

237
00:27:52,971 --> 00:27:55,439
Of course, if the department
knew that I sometimes
mixed it up

238
00:27:55,441 --> 00:27:58,342
in the underground
fight circuit, well,

239
00:27:58,344 --> 00:28:00,778
wouldn't be too good
for my career
in law enforcement.

240
00:28:10,021 --> 00:28:13,891
You know, I, uh,
was sorry to hear that, um,

241
00:28:13,893 --> 00:28:17,327
they're going to
re-prioritize your case.

242
00:28:17,329 --> 00:28:19,229
What the hell
are you talking about?

243
00:28:21,466 --> 00:28:24,368
Your case has been
re-prioritized
from active to cold.

244
00:28:24,370 --> 00:28:26,270
I thought you knew.

245
00:28:26,272 --> 00:28:28,939
Wh-why?
Frank, it's been
a long time.

246
00:28:28,941 --> 00:28:31,909
We've got no fresh leads.
I mean, it's hard.

247
00:28:31,911 --> 00:28:34,144
The case is dead?
No, it's not dead,

248
00:28:34,146 --> 00:28:37,081
it's just... cold.

249
00:28:37,083 --> 00:28:40,451
Those are fucking semantics,
and you know it.

250
00:28:40,453 --> 00:28:42,886
This is a way of
twisting words around.

251
00:28:42,888 --> 00:28:44,988
Believe me,
i know about it.

252
00:28:44,990 --> 00:28:47,825
I've made a lot of money
doing that.

253
00:28:53,032 --> 00:28:54,998
Look, I'll stay on top
of lustiger.

254
00:28:55,000 --> 00:28:56,834
You know, try and keep it
in his mind.

255
00:28:59,904 --> 00:29:01,972
Talk is cheap, huh?

256
00:29:16,154 --> 00:29:17,988
Hey, mister.

257
00:29:20,792 --> 00:29:22,493
You looking for...

258
00:29:22,495 --> 00:29:24,795
Company tonight?

259
00:29:26,364 --> 00:29:29,967
I can make you feel good
if you want.

260
00:29:29,969 --> 00:29:32,803
You could do anything.

261
00:29:32,805 --> 00:29:35,005
Hold on, hold on,
hold on, hold on.

262
00:29:37,041 --> 00:29:39,843
What are you doing
out here?

263
00:29:39,845 --> 00:29:42,546
You're too young
to be here.
How old are you?

264
00:29:44,516 --> 00:29:47,151
Answer me.
How old are you?

265
00:29:47,153 --> 00:29:49,353
Thirteen.

266
00:29:49,355 --> 00:29:51,188
Thirteen...

267
00:29:51,190 --> 00:29:52,289
Shit.

268
00:29:57,862 --> 00:30:00,297
Get your ass
over here.

269
00:30:00,299 --> 00:30:02,299
I'm sorry, buddy.

270
00:30:02,301 --> 00:30:04,001
She thought you were
somebody else.

271
00:30:04,003 --> 00:30:05,536
Let's go.

272
00:30:05,538 --> 00:30:08,005
Please, sir. This girl
is someone's daughter.

273
00:30:28,326 --> 00:30:30,294
Frank:
<i> If could have been</i>
<i> listening more closely,</i>

274
00:30:30,296 --> 00:30:32,596
<i> if I would have been</i>
<i> listening at all,</i>

275
00:30:32,598 --> 00:30:35,165
<i> I probably</i>
<i> would have heard it.</i>

276
00:32:11,529 --> 00:32:14,698
Frank:
<i> Finding the book</i>
<i> was a sign.</i>

277
00:32:14,700 --> 00:32:18,535
<i> I had been punishing myself</i>
<i> for their deaths,</i>

278
00:32:18,537 --> 00:32:21,571
<i> but I wasn't the one</i>
<i> who killed them.</i>

279
00:32:21,573 --> 00:32:24,174
<i> I had been punishing</i>
<i> the wrong person.</i>

280
00:34:33,371 --> 00:34:37,574
Meditations<i> was written</i>
<i> nearly 2,000 years ago</i>

281
00:34:37,576 --> 00:34:41,111
<i> and became the basis</i>
<i> for stoicism,</i>

282
00:34:41,113 --> 00:34:44,281
<i> which believed</i>
<i> that the best indication</i>

283
00:34:44,283 --> 00:34:46,416
<i> of an individual's</i>
<i> philosophy</i>

284
00:34:46,418 --> 00:34:48,251
<i> was not what he said</i>

285
00:34:49,353 --> 00:34:52,789
<i> but rather what he did.</i>

286
00:34:52,791 --> 00:34:54,758
<i> Stoics would take</i>
<i> a vow of silence</i>

287
00:34:54,760 --> 00:34:58,295
<i> in order to focus on</i>
<i> a specific task.</i>

288
00:34:58,297 --> 00:35:02,132
<i> It is why I took my own vow</i>
<i> not to be broken</i>

289
00:35:02,134 --> 00:35:05,235
<i> until I avenged my wife</i>
<i> and daughter's murders.</i>

290
00:36:51,842 --> 00:36:54,511
<i> Within days</i>
<i> after I stopped talking,</i>

291
00:36:54,513 --> 00:36:57,747
<i> my sense of hearing</i>
<i> had drastically improved.</i>

292
00:36:57,749 --> 00:37:00,717
<i> You see?</i>
<i> Good things do happen</i>

293
00:37:00,719 --> 00:37:03,553
<i> when you shut the fuck up</i>
<i> for a minute or two.</i>

294
00:39:22,360 --> 00:39:24,728
Hey! Hey.

295
00:39:34,472 --> 00:39:35,939
Nice ride, my man.

296
00:39:38,008 --> 00:39:40,110
But unfortunately
you're in our territory,

297
00:39:40,112 --> 00:39:42,545
so how about you just give me
your wallet and car keys,

298
00:39:42,547 --> 00:39:44,080
and we'll call it even.

299
00:39:47,886 --> 00:39:50,687
<i> Could these be the men</i>
<i> who killed them?</i>

300
00:39:53,057 --> 00:39:55,058
<i> A carjacking</i>
<i> gone terribly wrong.</i>

301
00:40:07,973 --> 00:40:10,407
Come on!

302
00:40:16,648 --> 00:40:19,582
Fucking dumb
piece of shit!

303
00:41:22,980 --> 00:41:24,581
Please...

304
00:41:37,228 --> 00:41:39,062
<i> I wanted to search his face</i>

305
00:41:39,064 --> 00:41:41,698
<i> for the slightest hint</i>
<i> of recognition.</i>

306
00:41:43,601 --> 00:41:46,069
<i> He had no idea</i>
<i> who they were.</i>

307
00:43:06,985 --> 00:43:08,651
Are you okay?

308
00:44:11,015 --> 00:44:12,682
My name is Alma.

309
00:44:15,285 --> 00:44:18,354
I'm an E.R. nurse
over at mercy.

310
00:44:18,356 --> 00:44:21,357
I volunteer at the local
homeless shelter.

311
00:44:22,793 --> 00:44:24,394
A-and you are?

312
00:44:30,067 --> 00:44:32,001
Are you okay?

313
00:44:32,003 --> 00:44:34,671
Do you understand me?

314
00:44:34,673 --> 00:44:36,939
<i> ┐Comprende?</i>
C-can you hear me?

315
00:44:39,209 --> 00:44:43,112
Do you remember anything
about yesterday?

316
00:44:43,114 --> 00:44:45,281
About how you got here?

317
00:44:48,318 --> 00:44:50,953
I was coming home
from the hospital,

318
00:44:50,955 --> 00:44:53,756
and I found you
lying in the road.

319
00:44:53,758 --> 00:44:56,726
So I helped you
over here.

320
00:44:56,728 --> 00:44:58,928
You were really
out of it,

321
00:44:58,930 --> 00:45:02,165
and that's when I discovered

322
00:45:02,167 --> 00:45:04,033
you'd been shot.

323
00:45:04,035 --> 00:45:07,437
Let me check it out. Okay?

324
00:45:07,439 --> 00:45:09,305
You're lucky.

325
00:45:09,307 --> 00:45:11,841
The bullet
only grazed you,

326
00:45:11,843 --> 00:45:14,444
so the wound was
pretty easy to dress.

327
00:45:16,814 --> 00:45:18,014
Hey.

328
00:47:20,871 --> 00:47:23,472
Frank:
<i> Why would Susan roll down</i>
<i> her window in the rain?</i>

329
00:47:23,474 --> 00:47:24,974
<i> Why?</i>

330
00:47:34,318 --> 00:47:35,918
Hi.

331
00:47:38,956 --> 00:47:41,991
You left this
at my house.

332
00:47:46,129 --> 00:47:49,599
Your address is on
your driver's license.

333
00:47:51,168 --> 00:47:53,569
Plus, I have to
re-check your wound.

334
00:48:12,189 --> 00:48:13,556
You were very lucky.

335
00:48:23,467 --> 00:48:25,902
You don't have me fooled,
you know.

336
00:48:25,904 --> 00:48:27,536
I know you can talk.

337
00:48:30,507 --> 00:48:33,609
Last night,
after I bandaged you up,

338
00:48:33,611 --> 00:48:36,045
you talked in your sleep.

339
00:48:37,614 --> 00:48:39,649
And if you can talk
in your sleep,

340
00:48:39,651 --> 00:48:41,550
you can talk
when you are awake.

341
00:48:47,291 --> 00:48:49,091
You kept saying
two names

342
00:48:49,093 --> 00:48:51,494
over and over:

343
00:48:51,496 --> 00:48:53,362
Sue and Olivia.

344
00:49:09,579 --> 00:49:11,447
Your wife and daughter?

345
00:49:24,127 --> 00:49:26,228
I am so sorry.

346
00:50:03,100 --> 00:50:04,700
Was their killer
ever caught?

347
00:50:17,381 --> 00:50:20,149
I know that area
very well.

348
00:50:21,618 --> 00:50:23,753
I drive through it
on my way to work.

349
00:50:31,428 --> 00:50:33,429
Let me know
if you need any help.

350
00:51:09,666 --> 00:51:11,767
Frank:
<i>A transportation security dog.</i>

351
00:51:14,337 --> 00:51:16,472
<i> Trained to sniff out</i>
<i> the smallest of scents</i>

352
00:51:16,474 --> 00:51:18,574
<i> in the most densely</i>
<i> populated areas</i>

353
00:51:18,576 --> 00:51:20,576
<i> with military precision.</i>

354
00:51:27,451 --> 00:51:30,753
<i> I wonder if my new friend</i>
<i> could fill in some blanks.</i>

355
00:52:45,896 --> 00:52:48,364
<i> The homeless encampment</i>
<i> I had found--</i>

356
00:52:48,366 --> 00:52:50,566
<i> if anyone was there</i>
<i> that night,</i>

357
00:52:50,568 --> 00:52:52,935
<i> they would have been</i>
<i> the perfect witness.</i>

358
00:52:52,937 --> 00:52:55,371
<i> Maybe even</i>
<i> the perfect killer.</i>

359
00:54:38,942 --> 00:54:40,943
Alma:
No fever.

360
00:54:40,945 --> 00:54:43,312
You are going to be
okay, honey.

361
00:54:43,314 --> 00:54:46,282
The doctor will come soon.

362
00:54:46,284 --> 00:54:47,783
Woman:
Thank you very much.

363
00:54:54,291 --> 00:54:56,692
You have something for me?

364
00:54:56,694 --> 00:54:58,560
I told you it was done.

365
00:55:01,464 --> 00:55:03,999
Nobody is done.

366
00:55:04,001 --> 00:55:05,934
Not until I say so.

367
00:55:09,806 --> 00:55:11,774
I'll be seeing you soon.

368
00:55:31,494 --> 00:55:34,596
Alma:
Yeah, I know this spot.
It's Mr. shivers' place.

369
00:55:34,598 --> 00:55:37,499
Sometimes on my way home
from the night shift,

370
00:55:37,501 --> 00:55:40,102
I see him around here,

371
00:55:40,104 --> 00:55:41,704
always smoking.

372
00:55:43,673 --> 00:55:47,476
But I've never
interacted with him.

373
00:55:47,478 --> 00:55:49,912
Actually,
i never saw his face.

374
00:55:49,914 --> 00:55:52,414
So I wouldn't be able
to I.D. Him for you.

375
00:55:52,416 --> 00:55:54,083
I'm sorry.

376
00:55:56,619 --> 00:55:59,521
But I heard
he works sometimes

377
00:55:59,523 --> 00:56:01,924
at the local diner
as a fry cook.

378
00:56:04,728 --> 00:56:06,428
We don't know
his real name.

379
00:56:08,064 --> 00:56:10,833
People at the shelter
call him Mr. shivers

380
00:56:10,835 --> 00:56:13,001
because he gives them
the creeps.

381
00:56:15,372 --> 00:56:19,441
They say he has this
really violent attitude.

382
00:56:36,092 --> 00:56:37,559
Thanks, frank.

383
00:56:59,916 --> 00:57:03,152
You and I need to talk.
Where are my drugs?

384
00:57:03,154 --> 00:57:05,020
I don't want to
do this anymore.

385
00:57:05,022 --> 00:57:07,022
Listen.

386
00:57:07,024 --> 00:57:08,791
You will do

387
00:57:08,793 --> 00:57:11,026
as I tell you,
understood?

388
00:57:11,028 --> 00:57:12,628
Yes.
Shut up!

389
00:57:45,829 --> 00:57:47,496
Those guys...

390
00:57:49,065 --> 00:57:51,567
They came into the E.R.

391
00:57:51,569 --> 00:57:53,202
About a year ago...

392
00:57:55,705 --> 00:57:57,706
Forcing me
to pay protection.

393
00:57:59,776 --> 00:58:02,478
Have me steal drugs
for them.

394
00:58:02,480 --> 00:58:04,213
From the hospital,
you know.

395
00:58:04,215 --> 00:58:07,216
Oxy, codeine, fentanyl.

396
00:58:08,818 --> 00:58:10,786
But the other day
i told them I was done.

397
00:58:16,559 --> 00:58:18,660
They're gonna kill me
for this, frank.

398
00:58:20,930 --> 00:58:22,564
What am I going to do?

399
00:59:50,320 --> 00:59:53,322
Nobody ever accused me
of being able to cook.

400
00:59:55,692 --> 00:59:58,894
But I figured
that one breakfast

401
00:59:58,896 --> 01:00:00,996
was the least I could do
to thank you.

402
01:00:09,105 --> 01:00:11,773
You know, I've been
thinking about

403
01:00:11,775 --> 01:00:14,009
the guy you are
looking for,
Mr. shivers.

404
01:00:14,011 --> 01:00:16,945
Like I said,
I've never worked with him

405
01:00:16,947 --> 01:00:18,680
directly at
the shelter, but...

406
01:00:21,884 --> 01:00:24,786
But he had this cough,

407
01:00:24,788 --> 01:00:27,856
a hacking cough
you'd recognize anywhere.

408
01:00:50,713 --> 01:00:53,281
Frank:
<i> Do I still look crazy to you?</i>

409
01:00:53,283 --> 01:00:55,984
<i> Unhinged?</i>
<i> Like a maniac?</i>

410
01:00:55,986 --> 01:00:58,186
<i> All of the above?</i>

411
01:01:00,123 --> 01:01:02,391
<i> Yeah.</i>
<i> You're probably right.</i>

412
01:01:02,393 --> 01:01:04,826
<i> But at least now,</i>
<i> you understand why.</i>

413
01:01:14,137 --> 01:01:16,171
<i> He recognized them.</i>

414
01:01:16,173 --> 01:01:17,406
<i> But...</i>

415
01:01:19,242 --> 01:01:21,376
<i> There was no death</i>
<i> in this man's eyes.</i>

416
01:01:21,378 --> 01:01:23,178
<i> He wasn't the killer.</i>

417
01:01:25,882 --> 01:01:28,316
I saw what happened
that night.

418
01:01:43,132 --> 01:01:45,701
It was a cop.

419
01:01:47,136 --> 01:01:49,137
I was there.

420
01:01:49,139 --> 01:01:51,773
I saw the whole thing.

421
01:02:17,300 --> 01:02:19,101
Olivia:
Mommy!

422
01:02:33,150 --> 01:02:35,050
Mommy! Mommy!

423
01:02:50,066 --> 01:02:52,901
Mr. shivers:<i> He killed them</i>
<i> in the warehouse.</i>

424
01:02:52,903 --> 01:02:55,303
<i> What could I do? Huh?</i>

425
01:02:55,305 --> 01:02:58,540
I've got enough trouble
already.

426
01:03:00,076 --> 01:03:03,979
I don't need no more
trouble with cops.

427
01:03:12,522 --> 01:03:14,890
The purse...

428
01:03:14,892 --> 01:03:16,958
<i> Got left behind.</i>

429
01:03:16,960 --> 01:03:20,929
I took the money from it,
and I got rid of it.

430
01:03:20,931 --> 01:03:23,265
I needed the money, man.

431
01:03:25,802 --> 01:03:28,003
I'm sorry, man.

432
01:03:29,105 --> 01:03:30,472
I'm sorry.

433
01:03:32,342 --> 01:03:34,543
Open that door!

434
01:03:34,545 --> 01:03:37,345
Come on!
Open this door now!

435
01:03:39,149 --> 01:03:40,582
Open this door
right now!

436
01:03:51,261 --> 01:03:54,896
Woman:
I just want to see my son.
Take me to see my son.

437
01:03:56,833 --> 01:03:59,568
Man:
Get your hands off me!

438
01:03:59,570 --> 01:04:03,171
Hey! I didn't do nothing,
goddamn it!

439
01:04:03,173 --> 01:04:05,574
What'd I tell you?
Why don't you just
let me go, man?

440
01:04:05,576 --> 01:04:07,609
Cuff him!
Motherfuckers!

441
01:04:07,611 --> 01:04:09,878
Calm down!
You need some help?

442
01:04:09,880 --> 01:04:12,180
Man: Fuck you!
Policeman:
Calm down!

443
01:04:12,182 --> 01:04:14,049
Policeman 2:
Keep moving!

444
01:04:44,948 --> 01:04:48,016
Frank:
<i> I wanted to narrow</i>
<i> my pool of suspects</i>

445
01:04:48,018 --> 01:04:50,886
<i> by finding out</i>
<i> which officers were on duty</i>

446
01:04:50,888 --> 01:04:53,588
<i> the night shivers</i>
<i> witnessed the killings.</i>

447
01:04:53,590 --> 01:04:57,058
<i> And of those officers,</i>
<i> which one had been assigned</i>

448
01:04:57,060 --> 01:04:59,160
<i> to patrol that</i>
<i> particular district.</i>

449
01:05:06,035 --> 01:05:08,637
<i> I was surprised to discover</i>
<i> a familiar face.</i>

450
01:05:10,239 --> 01:05:11,573
<i> Could it really be him?</i>

451
01:07:20,136 --> 01:07:23,038
Man:
Show him what you got!
Woman: Hit him!

452
01:07:50,733 --> 01:07:54,469
<i> I followed Payton for</i>
<i> seven whole days and nights.</i>

453
01:07:54,471 --> 01:07:56,438
<i> I chased him</i>

454
01:07:56,440 --> 01:07:58,540
<i> as my demons</i>
<i> were chasing me.</i>

455
01:08:01,110 --> 01:08:03,545
<i> I even alternated</i>
<i> between cars,</i>

456
01:08:03,547 --> 01:08:06,214
<i> as to not arouse suspicion.</i>

457
01:08:06,216 --> 01:08:08,583
<i> Night after night,</i>

458
01:08:08,585 --> 01:08:10,685
<i> day after day.</i>

459
01:08:13,355 --> 01:08:16,458
<i> All the while wondering,</i>

460
01:08:16,460 --> 01:08:18,359
<i> if this man truly was the one</i>
<i> who had killed</i>

461
01:08:18,361 --> 01:08:20,395
<i> my wife and daughter,</i>

462
01:08:20,397 --> 01:08:22,497
<i> then why?</i>

463
01:08:24,400 --> 01:08:26,201
<i> So I kept watching...</i>

464
01:08:28,270 --> 01:08:30,271
<i> Trying to find an answer...</i>

465
01:08:31,540 --> 01:08:33,174
<i> A sign...</i>

466
01:08:34,577 --> 01:08:36,211
<i> Or maybe a mistake.</i>

467
01:11:13,702 --> 01:11:16,771
Found these tiny
gold fibers.
Gold fibers?

468
01:11:26,382 --> 01:11:28,750
Yes, officer?

469
01:11:28,752 --> 01:11:31,052
Can you please pull over
on the next road?

470
01:11:43,466 --> 01:11:44,932
Olivia's voice:
<i> Mommy!</i>

471
01:12:58,974 --> 01:13:01,676
Frank:<i> The case is dead?</i>
Strode:<i> It's not dead,</i>
<i>it's just cold.</i>

472
01:13:01,678 --> 01:13:04,545
This is a way of twisting
words around.

473
01:13:04,547 --> 01:13:05,680
I made a lot of money
doing that.

474
01:16:10,198 --> 01:16:12,600
Now you know why
i did it, frank.

475
01:16:15,571 --> 01:16:18,072
Some creep you put
back on the streets...

476
01:16:19,675 --> 01:16:21,576
Killed my little girl.

477
01:16:25,213 --> 01:16:27,281
Twelve years old, frank.

478
01:16:29,251 --> 01:16:31,819
Murdered by a guy

479
01:16:31,821 --> 01:16:34,589
who should have been
behind bars.

480
01:16:52,674 --> 01:16:54,041
This is where
they died.

481
01:16:56,378 --> 01:16:58,713
Your wife was
a fighter, frank.

482
01:17:03,185 --> 01:17:04,986
But your little girl...

483
01:17:08,223 --> 01:17:11,225
Well, she was
screaming,

484
01:17:11,227 --> 01:17:13,861
crying for her daddy

485
01:17:13,863 --> 01:17:15,796
to come and save her.

486
01:17:18,266 --> 01:17:19,800
But you didn't.

487
01:17:22,004 --> 01:17:24,305
Where were you, frank?

488
01:17:26,141 --> 01:17:27,875
Where were you?

489
01:20:37,966 --> 01:20:39,867
Frank:
<i> I wanted nothing more</i>
<i> than to kill</i>

490
01:20:39,869 --> 01:20:41,869
<i> the human monster</i>
<i> before me</i>

491
01:20:45,473 --> 01:20:47,541
<i> but that would make me</i>

492
01:20:47,543 --> 01:20:49,543
<i> no better than him.</i>

493
01:20:51,479 --> 01:20:54,381
<i> Because the greatest stoic</i>
<i> of them all has said,</i>

494
01:20:54,383 --> 01:20:56,350
<i> "the best revenge</i>

495
01:20:56,352 --> 01:20:58,452
<i> is to be unlike your enemy."</i>

496
01:21:50,005 --> 01:21:52,973
Woman:
What do you think
about the verdict?

497
01:21:52,975 --> 01:21:56,277
<i> The evidence against officer</i>
<i> strode was overwhelming.</i>

498
01:21:56,279 --> 01:21:58,579
<i> The department is very happy</i>
<i> with the verdict.</i>

499
01:21:58,581 --> 01:22:01,415
<i>There is nothing worse</i>
<i>than a bad cop.</i>

500
01:22:03,085 --> 01:22:05,119
Man:<i> Detective, were there</i>
<i>other cops involved?</i>

501
01:22:05,121 --> 01:22:07,321
Lustiger:<i> No, there's no</i>
<i>evidence supporting that.</i>

502
01:22:17,632 --> 01:22:20,401
Frank:
<i> Justice had been served.</i>

503
01:22:22,570 --> 01:22:24,505
<i> I had tried</i>
<i> to make things right.</i>

504
01:22:26,574 --> 01:22:29,510
<i> But I knew that nothing I did</i>
<i> would ever bring them back.</i>

505
01:22:34,349 --> 01:22:36,550
<i> And I could never go</i>
<i> into the past</i>

506
01:22:38,153 --> 01:22:41,255
<i> to be there for them,</i>
<i> like I promised.</i>

507
01:22:43,425 --> 01:22:45,526
<i> If I could,</i>

508
01:22:45,528 --> 01:22:48,462
<i> I would tell my daughter</i>
<i> that she was right--</i>

509
01:22:48,464 --> 01:22:51,365
<i> I loved her performance.</i>

510
01:22:52,968 --> 01:22:54,568
<i> And I would tell them both</i>
<i> the words</i>

511
01:22:54,570 --> 01:22:57,237
<i> that still matter</i>
<i> the most,</i>

512
01:22:57,239 --> 01:23:00,708
<i> among the thousands</i>
<i> we speak every day--</i>

513
01:23:00,710 --> 01:23:02,276
I love you.

