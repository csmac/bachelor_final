1
00:00:48,760 --> 00:00:53,072
MY WAY HOME

2
00:00:55,200 --> 00:01:00,194
Khleb means "bread".
Say it.

3
00:01:00,320 --> 00:01:04,029
- Khleb
- No, say it softer.

4
00:01:04,160 --> 00:01:07,038
- Khleb
- Woda means "water".

5
00:01:07,160 --> 00:01:09,276
What does pashlee mean?

6
00:01:10,880 --> 00:01:13,758
Pashlee? It means, "Go to hell!"

7
00:01:20,760 --> 00:01:24,389
Let's go on. Kushaty

8
00:01:26,800 --> 00:01:30,793
- Kushaty Know what that means?
- It means "to eat".

9
00:01:30,920 --> 00:01:33,832
Dai mne kushaty
means "give me food".

10
00:01:33,960 --> 00:01:36,155
Good, very good.

11
00:03:00,760 --> 00:03:04,150
Why hide?
They stopped attacking.

12
00:03:06,120 --> 00:03:08,111
Get changed!

13
00:03:19,640 --> 00:03:22,313
Just put the shirt on.

14
00:03:36,680 --> 00:03:40,468
- Give me food.
- Kushaty

15
00:03:40,600 --> 00:03:43,160
Dai mne kushaty
Is that right?

16
00:03:43,280 --> 00:03:45,236
Yes.

17
00:03:55,360 --> 00:03:58,158
How do you like it?
Will it be OK?

18
00:04:05,240 --> 00:04:07,595
Put on the cap too.

19
00:04:18,720 --> 00:04:23,874
- They're stationed at the road.
- We have to pass them.

20
00:04:24,000 --> 00:04:27,197
The region's full of them.

21
00:04:27,320 --> 00:04:31,029
- Let's take a chance.
- Let's go.

22
00:04:33,320 --> 00:04:37,472
- And you?
- l'm not going.

23
00:04:39,400 --> 00:04:43,234
Idiot! Stay then.

24
00:06:38,240 --> 00:06:40,231
The Cossacks!

25
00:06:48,440 --> 00:06:50,476
Hold them.

26
00:06:53,080 --> 00:06:55,036
Come here.

27
00:06:56,480 --> 00:06:58,471
Over there.

28
00:07:20,280 --> 00:07:23,317
Traitors! Bandits!

29
00:07:26,400 --> 00:07:30,837
We got the last ones.
You go with them too!

30
00:07:31,680 --> 00:07:33,955
Hands up!

31
00:07:36,280 --> 00:07:39,477
Dirty fascists!

32
00:07:50,400 --> 00:07:54,109
Take your clothes off!

33
00:08:01,880 --> 00:08:03,871
Hungarian?

34
00:08:07,680 --> 00:08:12,629
I'm Hungarian too, sir.
They forced me to wear this.

35
00:08:16,160 --> 00:08:18,151
Come here.

36
00:08:19,160 --> 00:08:25,349
Are you Hungarian?
Do you have any documents?

37
00:08:30,120 --> 00:08:34,352
Schoolboy? Student?

38
00:08:37,800 --> 00:08:39,791
Joseph.

39
00:08:41,600 --> 00:08:44,114
How old are you?

40
00:08:45,440 --> 00:08:51,310
Your age! How old are you?

41
00:08:52,960 --> 00:08:54,996
17 years old?

42
00:08:59,760 --> 00:09:02,069
- Done?
- Done.

43
00:09:11,080 --> 00:09:16,393
Come on! Quicker!
Just a little more!

44
00:09:16,520 --> 00:09:19,398
Girls!
The Cossacks are coming!

45
00:09:20,200 --> 00:09:23,431
What's the hurry, gorgeous?

46
00:09:24,600 --> 00:09:27,319
- l'll get you!
- Try!

47
00:09:27,440 --> 00:09:31,399
Get me, Cossack!

48
00:10:01,920 --> 00:10:04,480
Line up in fives!

49
00:10:15,480 --> 00:10:17,994
You go too! Over there!

50
00:10:42,080 --> 00:10:44,230
You all right, old man?

51
00:10:46,200 --> 00:10:48,191
Open your mouth.

52
00:10:51,480 --> 00:10:53,471
Nothing.

53
00:10:59,680 --> 00:11:01,716
Healthy.

54
00:11:05,800 --> 00:11:07,870
He's OK.

55
00:11:36,440 --> 00:11:39,637
That chap will escape.

56
00:11:39,760 --> 00:11:42,797
Come on,
splash about in the water.

57
00:12:01,240 --> 00:12:04,471
Come on, splash!
Come on, jump, splash about!

58
00:12:36,240 --> 00:12:41,837
There's barbed wire
all the way down.
I'll try once again.

59
00:12:51,400 --> 00:12:55,439
Come! That was enough.

60
00:12:55,560 --> 00:12:57,551
Come out!

61
00:12:58,560 --> 00:13:00,790
All of you. Enough!

62
00:13:00,920 --> 00:13:04,071
How many times do I have to say?

63
00:13:08,520 --> 00:13:11,318
Take your things and come on!

64
00:13:11,440 --> 00:13:14,716
Move, move! Come on!

65
00:13:50,520 --> 00:13:52,715
Fourth company, line up!

66
00:13:53,280 --> 00:13:56,590
Line up, you at the back too!

67
00:14:04,000 --> 00:14:08,869
Hell! What are you waiting for?
Line up!

68
00:14:16,200 --> 00:14:18,998
Are you in the fourth company, too?

69
00:14:19,120 --> 00:14:21,350
Stop gawping! Come on!

70
00:14:21,480 --> 00:14:24,358
You too! Get in the line.

71
00:14:29,640 --> 00:14:31,915
Get in the line!

72
00:14:32,040 --> 00:14:34,679
What are you waiting for? Move!

73
00:14:38,240 --> 00:14:41,516
One, two... You too.

74
00:14:41,640 --> 00:14:44,029
Four, five...

75
00:14:47,480 --> 00:14:49,710
I'll take four men to fetch water.

76
00:14:50,680 --> 00:14:53,240
Thirsty, all of you?

77
00:14:56,360 --> 00:14:58,157
One...

78
00:15:01,960 --> 00:15:04,793
Can you lead a horse?
Step out!

79
00:15:23,600 --> 00:15:27,115
- For water?
- Go up to the guard!

80
00:15:35,800 --> 00:15:38,234
Five men to fetch water.

81
00:15:38,760 --> 00:15:39,795
- O���p��i��a��! - Ec���!

82
00:15:43,720 --> 00:15:46,075
Go ahead!

83
00:15:48,240 --> 00:15:50,356
Come on!

84
00:16:23,040 --> 00:16:25,076
Stop!

85
00:16:26,560 --> 00:16:30,917
How did you get here?
Did I select you?

86
00:16:31,040 --> 00:16:35,431
I live quite near.
On our way back, l'll get lost.

87
00:16:35,560 --> 00:16:39,519
- You'll stay!
- Shut one eye, that's all.

88
00:16:39,640 --> 00:16:43,713
- Are you a soldier?
- I used to be. Does it matter?

89
00:16:43,840 --> 00:16:46,877
Shut up! I am the boss here.

90
00:16:49,280 --> 00:16:51,555
Go and draw water!

91
00:17:22,320 --> 00:17:25,073
- Do you have a cigarette?
- No.

92
00:17:44,920 --> 00:17:47,878
Halt!

93
00:19:13,400 --> 00:19:15,391
Carefully.

94
00:19:22,920 --> 00:19:24,558
Stop.

95
00:19:24,680 --> 00:19:27,353
Why are there six of you?

96
00:19:29,120 --> 00:19:31,475
Get in line. Quickly!

97
00:19:34,240 --> 00:19:36,629
Where's the sixth one from?

98
00:19:38,800 --> 00:19:41,314
One, two, three, four, five, six.

99
00:19:43,440 --> 00:19:46,113
From here, go.

100
00:19:47,880 --> 00:19:50,189
What shall I do with you?

101
00:19:50,320 --> 00:19:53,118
Well, go home. Go.

102
00:19:58,520 --> 00:20:00,511
Go home.

103
00:20:05,440 --> 00:20:07,112
Get out!

104
00:20:10,120 --> 00:20:12,111
You are free!

105
00:20:14,440 --> 00:20:17,000
Go to hell!

106
00:20:18,160 --> 00:20:23,473
He means you. Go home! Go!

107
00:22:40,560 --> 00:22:43,791
Get up!

108
00:22:53,120 --> 00:22:56,669
- Are you German?
- Hungarian.

109
00:23:23,800 --> 00:23:26,598
What are you looking at, kraut?

110
00:23:55,120 --> 00:23:59,636
One, two, three.
Go to the truck.

111
00:24:01,040 --> 00:24:07,309
One... two... three. Go!

112
00:24:10,600 --> 00:24:12,909
Wait!

113
00:24:14,840 --> 00:24:17,638
- How old are you?
- 17.

114
00:24:17,760 --> 00:24:22,356
Do you speak Russian? German?

115
00:24:22,480 --> 00:24:25,119
- Do you speak German?
- A little.

116
00:24:27,560 --> 00:24:32,588
What's your profession?
What work do you do?

117
00:24:34,480 --> 00:24:39,554
Can you drive?

118
00:24:39,680 --> 00:24:41,796
I don't understand.

119
00:24:41,920 --> 00:24:43,911
Car? Chauffeur?

120
00:24:44,040 --> 00:24:49,751
- l'm a schoolboy.
- Schoolboy? Get in, schoolboy.

121
00:24:51,360 --> 00:24:53,510
Let's go.

122
00:26:55,480 --> 00:26:57,789
Praise be to our Lord.

123
00:28:22,600 --> 00:28:24,591
- Where is he?
- Go.

124
00:28:25,320 --> 00:28:27,390
Kolya!

125
00:28:35,880 --> 00:28:37,871
Kolya!

126
00:28:39,360 --> 00:28:42,511
Where have you been?
The milk will get sour.

127
00:28:42,640 --> 00:28:46,713
- Where's the milk?
- lt's coming!
Hurry up!

128
00:29:03,600 --> 00:29:06,956
Load them up! Work!

129
00:29:20,800 --> 00:29:23,394
- Here's your parcel!
- Thanks.

130
00:29:23,520 --> 00:29:29,152
- How are the troops doing?
- It won't take long.

131
00:29:33,440 --> 00:29:35,431
Let's go.

132
00:29:40,160 --> 00:29:41,957
You stay.

133
00:29:42,080 --> 00:29:45,390
I left him for you. He'll help you!

134
00:30:29,480 --> 00:30:33,393
Here, eat!

135
00:30:39,400 --> 00:30:44,155
Why are you coming here?
Who called you?

136
00:30:45,120 --> 00:30:47,918
Go away! I told you to go away!

137
00:34:23,040 --> 00:34:26,350
Where are you going?
There are mines there.

138
00:34:28,120 --> 00:34:31,430
Stop! I told you to stop!

139
00:34:35,680 --> 00:34:37,910
Stop or l'll shoot!

140
00:34:41,840 --> 00:34:44,195
B��op��e���c��, �ڧէ�o��!

141
00:35:26,520 --> 00:35:30,513
You'll die there!
Do you want to die?

142
00:35:30,640 --> 00:35:34,599
You fool!
There are mines all round.

143
00:35:35,680 --> 00:35:38,797
Mines, understand?

144
00:35:44,480 --> 00:35:47,472
Read the sign. Mines!

145
00:35:58,320 --> 00:36:00,550
Read the sign.

146
00:36:02,720 --> 00:36:08,272
It says here!
See? There are mines here.

147
00:36:11,240 --> 00:36:13,231
Look.

148
00:36:32,880 --> 00:36:38,000
I'm taking you back.
You're my prisoner of war.

149
00:36:38,120 --> 00:36:41,396
If you run away,
l"ll get into trouble.

150
00:36:42,760 --> 00:36:48,039
If you try to escape, l'll kill you.
Do you understand?

151
00:36:50,680 --> 00:36:52,910
No, you don't.

152
00:36:53,360 --> 00:36:56,272
Let's get down to work.

153
00:36:58,520 --> 00:37:00,511
We're short of churns.

154
00:37:46,400 --> 00:37:51,793
Pour it out...

155
00:37:52,480 --> 00:37:55,199
...onto the ground.

156
00:41:27,360 --> 00:41:31,876
Stop a bit, driver!
Can I take a ride?

157
00:41:32,000 --> 00:41:35,197
Come, get on.

158
00:47:54,760 --> 00:47:58,469
Comrade Commander,
what a lovely dog.

159
00:48:22,040 --> 00:48:26,158
Ta�ߧ���!

160
00:48:26,200 --> 00:48:30,830
Well, student, do you
understand Russian now?

161
00:48:30,960 --> 00:48:33,190
Come here, doggy.

162
00:48:35,680 --> 00:48:39,195
Hey, who's going to work
instead of you?

163
00:48:39,320 --> 00:48:41,993
Come on. Run!

164
00:48:43,680 --> 00:48:46,274
Stop it! Come on!

165
00:48:51,720 --> 00:48:54,792
I've even got a name
for him - Burka.

166
00:48:57,640 --> 00:49:00,029
Come, come!

167
00:49:00,160 --> 00:49:03,152
Leave the dog here, Comrade.
I could use him.

168
00:49:03,280 --> 00:49:05,748
You already have a helper.

169
00:49:08,560 --> 00:49:11,279
Burka!

170
00:49:11,400 --> 00:49:13,595
Come here. Stop!

171
00:49:19,440 --> 00:49:22,193
Come over here!

172
00:49:23,680 --> 00:49:25,716
Give me the dog.

173
00:49:25,840 --> 00:49:28,195
Stop it!

174
00:49:33,080 --> 00:49:35,150
Let's go.

175
00:50:27,200 --> 00:50:29,475
There you go.

176
00:54:52,600 --> 00:54:55,433
Come! Come here!

177
00:54:55,560 --> 00:54:57,915
- What's up?
- There are potatoes here.

178
00:54:58,040 --> 00:55:00,600
Where?

179
00:55:02,440 --> 00:55:05,398
Give me the bucket!

180
00:55:34,400 --> 00:55:37,039
Spit it out!

181
00:55:38,440 --> 00:55:41,352
You want to die now that you're free?

182
00:55:41,480 --> 00:55:45,189
Spit it out! Don't you understand?
Spit it out!

183
00:55:46,800 --> 00:55:48,358
Idiot!

184
00:56:26,720 --> 00:56:30,599
More people tried to escape
from our camp?

185
00:56:30,720 --> 00:56:33,792
Three Poles were kneeling
at the steps of the chapel.

186
00:56:33,920 --> 00:56:37,993
- Poles?
- Yes, but there were others too.

187
00:56:38,120 --> 00:56:42,159
They kneeled down
and tried to escape.

188
00:56:42,280 --> 00:56:44,953
Four of them dug a hole
under the altar.

189
00:56:45,080 --> 00:56:50,552
They got three of them.
One escaped.

190
00:56:53,400 --> 00:56:56,198
- What is it?
- This bucket is ours.

191
00:56:56,320 --> 00:56:58,470
- You're Hungarian?
- Yes.

192
00:56:58,600 --> 00:57:00,716
- Really?
- Yes.

193
00:57:08,040 --> 00:57:12,750
- You rotten NaZi!
- l'm not a NaZi.

194
00:57:12,880 --> 00:57:17,112
- Not any more, of course!
- He is a NaZi. You can see it.

195
00:57:20,240 --> 00:57:23,915
- Leave him alone!
- I will kill you, you bastard!

196
00:57:26,160 --> 00:57:28,720
Let go or l'll fire!

197
00:57:28,840 --> 00:57:31,354
That's not right.

198
00:57:31,480 --> 00:57:33,914
- What's not right?
- lt's not right.

199
00:57:34,040 --> 00:57:36,554
All of you against one, is that fair?

200
00:57:39,240 --> 00:57:42,038
Let's go, don't be scared.

201
00:57:46,280 --> 00:57:48,874
All against one.

202
00:58:04,800 --> 00:58:07,439
- Are you Hungarian?
- Yes.

203
00:58:07,560 --> 00:58:09,915
- A prisoner of war?
- Yes.

204
00:58:12,680 --> 00:58:18,073
- Could you escape?
- Yes.

205
00:58:19,000 --> 00:58:24,358
Follow us.
We'll get a striped coat for you.

206
00:58:25,400 --> 00:58:27,994
Nothing, nothing.

207
00:58:30,200 --> 00:58:35,638
This boy would get into trouble,
if I escaped.

208
00:58:55,760 --> 00:58:56,954
Look, frogs!

209
00:58:57,080 --> 00:58:59,036
Quiet.

210
00:59:02,440 --> 00:59:05,796
There... a hit!

211
00:59:20,400 --> 00:59:23,437
Xo��e��� ��oc��pe�ݧ���?

212
00:59:27,600 --> 00:59:29,591
Got it!

213
00:59:31,640 --> 00:59:35,155
Come on.

214
00:59:37,120 --> 00:59:39,509
Come on, move.

215
00:59:44,960 --> 00:59:47,793
Go to hell!

216
00:59:47,920 --> 00:59:49,717
Go.

217
00:59:52,040 --> 00:59:54,110
Follow me! Come.

218
01:00:06,800 --> 01:00:09,075
Come here.

219
01:00:27,520 --> 01:00:29,556
One... Hold this.

220
01:00:29,680 --> 01:00:32,240
Two, three.

221
01:00:39,040 --> 01:00:41,235
Look, statues.

222
01:00:41,360 --> 01:00:43,316
Statues.

223
01:00:46,680 --> 01:00:49,558
A whole museum.

224
01:00:54,840 --> 01:00:57,354
What's that? A broken head.

225
01:01:00,560 --> 01:01:03,711
Look at the style. Greek.

226
01:01:03,840 --> 01:01:05,831
Pretty, isn't it?

227
01:01:05,960 --> 01:01:08,713
I'll put it back.

228
01:01:08,840 --> 01:01:11,274
It lay here.

229
01:01:13,560 --> 01:01:15,551
Give me your coat.

230
01:01:15,680 --> 01:01:19,355
She's freeZing.

231
01:01:22,840 --> 01:01:24,910
There.

232
01:01:26,160 --> 01:01:28,958
This will be the wreath.

233
01:01:33,440 --> 01:01:35,431
How beautiful!

234
01:01:39,400 --> 01:01:42,392
Let's throw stones through
the window. Will you?

235
01:01:44,000 --> 01:01:45,797
One...

236
01:01:48,320 --> 01:01:51,312
More. Got it?

237
01:01:51,440 --> 01:01:53,476
One more.

238
01:01:54,080 --> 01:01:56,594
One more, come on.

239
01:01:56,720 --> 01:01:58,711
Throw some more.

240
01:02:01,040 --> 01:02:04,271
I was lucky!

241
01:02:11,920 --> 01:02:14,275
Hold it up.

242
01:02:22,200 --> 01:02:24,714
Hold it for me.

243
01:02:24,840 --> 01:02:28,071
I'm going to climb up.

244
01:02:31,000 --> 01:02:33,070
Be careful.

245
01:02:35,640 --> 01:02:37,870
- Hold it!
- Jump!

246
01:02:38,000 --> 01:02:40,992
It breaks easily.

247
01:02:47,200 --> 01:02:50,556
This one's finished.
She gave her soul to God.

248
01:02:52,000 --> 01:02:55,151
Let's go.

249
01:03:04,080 --> 01:03:07,993
Go. Go. Move!

250
01:03:14,600 --> 01:03:18,229
Watch out!
Don't let her back out.

251
01:03:22,720 --> 01:03:25,075
- Come on, go!
- Damn you!

252
01:03:29,240 --> 01:03:31,435
Come on, move!

253
01:03:31,560 --> 01:03:33,551
Go.

254
01:03:41,920 --> 01:03:43,911
What was that?

255
01:04:00,680 --> 01:04:03,797
Stop. I'll go alone.

256
01:04:10,880 --> 01:04:12,916
Be careful. On the right.

257
01:04:19,120 --> 01:04:22,157
Follow my tracks.

258
01:04:22,800 --> 01:04:24,836
To hell with it!

259
01:04:30,480 --> 01:04:32,994
Look here.

260
01:04:40,280 --> 01:04:42,475
Here are their tracks.

261
01:04:50,080 --> 01:04:52,833
- Go carefully over there.
- I can see it.

262
01:05:00,080 --> 01:05:02,275
Come.

263
01:05:02,400 --> 01:05:04,470
Yeah, l'm coming.

264
01:05:05,400 --> 01:05:08,392
We won't be hungry
until the end of the war.

265
01:05:11,120 --> 01:05:13,156
- lt'll be a rich meal.
- Grab it.

266
01:05:13,280 --> 01:05:15,475
Yeah. Come on.

267
01:05:23,200 --> 01:05:25,475
Heavy, isn't it?

268
01:05:33,720 --> 01:05:37,474
Give me a piece.

269
01:05:38,160 --> 01:05:40,549
Delicious, isn't it?

270
01:05:43,000 --> 01:05:45,514
It's cooked in the Georgian way.

271
01:05:47,320 --> 01:05:49,880
I love it.

272
01:06:02,840 --> 01:06:04,910
Just a little bit.

273
01:06:09,080 --> 01:06:11,640
Give me a drink.

274
01:07:08,640 --> 01:07:12,269
What's the matter?
Are you in pain?

275
01:07:13,400 --> 01:07:19,236
My stomach.
It's my wound here.
I got wounded.

276
01:07:21,680 --> 01:07:24,035
The bullet is still in there.

277
01:07:24,880 --> 01:07:27,599
The doctor told me to exercise.

278
01:07:29,440 --> 01:07:35,993
To do this... and l'll be OK.

279
01:07:43,920 --> 01:07:46,229
Can you hear?

280
01:08:05,040 --> 01:08:07,349
Look at them!

281
01:08:10,280 --> 01:08:12,430
Can you see it?

282
01:08:21,440 --> 01:08:23,749
Give it to me.

283
01:08:25,400 --> 01:08:28,358
You're so far away.

284
01:09:28,200 --> 01:09:29,997
Run!

285
01:09:30,120 --> 01:09:32,315
Let's catch up with her!

286
01:09:32,440 --> 01:09:35,557
Come on! Run!

287
01:10:55,560 --> 01:10:59,109
- Look out, he's coming for us!
- Take to the ground!

288
01:11:13,000 --> 01:11:15,798
- He's coming back.
- Look how it turns.

289
01:11:15,920 --> 01:11:17,911
He wants to play.

290
01:11:19,760 --> 01:11:23,150
I told you so.
He's coming back.

291
01:11:24,200 --> 01:11:28,193
- Run!
- Take to the ground.

292
01:11:35,800 --> 01:11:39,395
Where is she?
Over there. Look!

293
01:11:41,520 --> 01:11:44,876
She's too far away.

294
01:11:49,880 --> 01:11:51,871
Come on.

295
01:12:08,080 --> 01:12:10,594
Don't you know your duty?
Where have you been?

296
01:12:10,720 --> 01:12:13,917
- Everything's been OK.
- That's what you think.

297
01:12:14,640 --> 01:12:18,758
Disinfectant.
Do you have a samovar?

298
01:12:18,880 --> 01:12:24,637
What do you want it for?
You'll get clean anyway.

299
01:12:24,760 --> 01:12:31,199
Two handkerchiefs?
- Is that all?
- Stop asking questions.

300
01:12:31,320 --> 01:12:35,108
- Why only two handkerchiefs?
- I don't know.

301
01:12:35,240 --> 01:12:38,038
It's order number 248.

302
01:12:38,160 --> 01:12:41,994
Obey, or do it yourself.

303
01:12:42,120 --> 01:12:45,192
What manners! I work all day long.

304
01:12:45,320 --> 01:12:47,311
You can't do that to us.

305
01:12:47,440 --> 01:12:51,911
- Keep quiet.
- Brother, l'm wounded.

306
01:12:52,040 --> 01:12:55,191
A soldier must obey orders.

307
01:12:55,320 --> 01:12:59,108
- But since l'm wounded...
- Undress. I'm also wounded.

308
01:13:04,680 --> 01:13:06,671
Shall I cover myself with this?

309
01:13:07,600 --> 01:13:10,273
Do that, the rest doesn't matter.

310
01:13:13,360 --> 01:13:16,716
Where are you going?
Go over there.

311
01:13:17,560 --> 01:13:19,835
You can only go there dressed.

312
01:13:19,960 --> 01:13:21,712
Come on.

313
01:13:25,960 --> 01:13:28,110
Get undressed.

314
01:13:30,760 --> 01:13:36,278
Nothing else works...
...only disinfectant.

315
01:13:36,400 --> 01:13:39,836
Are you a girl or a soldier?

316
01:13:39,960 --> 01:13:41,996
What are you doing? Close it.

317
01:13:42,120 --> 01:13:46,398
They're not ready,
and the machine needs to cool down.

318
01:13:46,520 --> 01:13:49,478
Hurry up, boys!

319
01:14:05,040 --> 01:14:08,635
Look at those soldiers!

320
01:14:08,760 --> 01:14:11,274
They make you strip!

321
01:14:12,720 --> 01:14:18,477
Look! Napoleon! Is it ready?

322
01:14:19,760 --> 01:14:23,036
Patience, soldier! Hold on!

323
01:14:30,640 --> 01:14:32,631
Open it!

324
01:14:43,480 --> 01:14:47,553
You've ruined my uniform.

325
01:14:47,680 --> 01:14:49,636
Don't be fussy.

326
01:14:50,600 --> 01:14:52,192
Here...

327
01:14:55,520 --> 01:14:57,556
Let's go.

328
01:14:57,680 --> 01:15:00,911
Nice work.

329
01:15:01,400 --> 01:15:04,278
- Enough, soldier.
- Yes, indeed.

330
01:15:04,400 --> 01:15:10,589
- Stop squealing!
- You've ruined my uniform...

331
01:15:51,960 --> 01:15:54,349
- What's the matter?
- It hurts.

332
01:15:56,080 --> 01:15:58,878
Something's hurting me... inside.

333
01:16:01,040 --> 01:16:03,076
Look... my tongue.

334
01:16:04,400 --> 01:16:06,436
Good.

335
01:16:09,920 --> 01:16:12,559
The Germans got me.

336
01:16:12,680 --> 01:16:15,990
The devil knows how,
but I survived.

337
01:16:16,120 --> 01:16:19,476
And l'm OK,
do you understand?

338
01:16:21,800 --> 01:16:26,749
The doctor said I need
a good diet and exercise.

339
01:16:36,240 --> 01:16:39,949
Don't worry, l'll survive.

340
01:17:06,600 --> 01:17:09,433
Water, hot water.

341
01:17:59,560 --> 01:18:02,233
Come on, quickly.

342
01:18:02,360 --> 01:18:05,397
They are waiting for the milk.

343
01:18:06,720 --> 01:18:09,314
Kolya is ill.

344
01:18:09,440 --> 01:18:12,477
What are you saying?
Hurry up!

345
01:18:12,600 --> 01:18:14,636
We've got a lot of wounded.

346
01:18:14,760 --> 01:18:16,716
Don't you understand?
Kolya is ill.

347
01:18:16,840 --> 01:18:19,513
He didn't understand a word.

348
01:18:24,920 --> 01:18:27,798
- l've got documents.
- I see.

349
01:18:27,920 --> 01:18:29,956
Go home.

350
01:19:03,960 --> 01:19:06,997
- l've got documents.
- l'm Hungarian.

351
01:19:07,120 --> 01:19:11,796
Is there a doctor amongst you?

352
01:19:11,920 --> 01:19:13,751
No.

353
01:19:31,720 --> 01:19:33,995
Do you want some meat?

354
01:19:36,840 --> 01:19:39,912
Thank you.

355
01:19:59,600 --> 01:20:01,670
- Do you live here?
- I do.

356
01:20:15,160 --> 01:20:17,151
- Is he ill?
- He is.

357
01:20:17,280 --> 01:20:20,397
He got wounded,
and now he's ill.

358
01:20:26,080 --> 01:20:28,514
- Is he Russian?
- He is.

359
01:20:33,920 --> 01:20:38,869
What are you waiting for?
Why don't you get lost?

360
01:20:45,000 --> 01:20:47,150
Some more meat?

361
01:20:48,840 --> 01:20:51,149
No, thanks.

362
01:21:03,960 --> 01:21:06,269
Let's go!

363
01:21:15,480 --> 01:21:18,472
Stop!

364
01:21:18,600 --> 01:21:21,194
Stop!

365
01:21:58,760 --> 01:22:01,877
Don't drink this. You're ill.

366
01:23:55,720 --> 01:23:59,998
- We have documents.
- Go. Go.

367
01:24:05,600 --> 01:24:09,673
Hello, Comrade.
Where are you heading?

368
01:24:09,800 --> 01:24:11,870
I've been near VoroneZh
for two years.

369
01:24:12,000 --> 01:24:14,468
I worked there in a factory,
you know.

370
01:24:14,600 --> 01:24:16,830
- I worked...
- Go.

371
01:24:24,320 --> 01:24:26,311
Halt.

372
01:24:28,240 --> 01:24:30,390
Move on.

373
01:24:54,920 --> 01:24:57,070
Halt!

374
01:25:02,600 --> 01:25:04,716
We have documents.

375
01:25:17,720 --> 01:25:19,995
Step back.

376
01:25:20,440 --> 01:25:23,159
Turn away.

377
01:25:26,200 --> 01:25:29,909
He's alone. That's unusual.

378
01:25:30,040 --> 01:25:32,634
Don't show you're afraid.

379
01:25:37,760 --> 01:25:39,796
A doctor?

380
01:25:40,800 --> 01:25:43,234
Is there a doctor here?

381
01:25:56,800 --> 01:25:58,950
Doctor, yes?

382
01:26:02,600 --> 01:26:05,353
Come on.

383
01:26:08,120 --> 01:26:10,953
He's alone.
Take his weapon.

384
01:26:14,080 --> 01:26:16,230
Be careful. He's loaded it.

385
01:26:18,480 --> 01:26:20,994
Let's go!

386
01:26:21,120 --> 01:26:24,271
- Let's go.
- Don't let him do it!

387
01:26:25,080 --> 01:26:27,992
Stay calm.
Probably someone is ill here.

388
01:26:29,400 --> 01:26:31,914
Move!

389
01:26:47,240 --> 01:26:50,676
Move, move!

390
01:26:51,640 --> 01:26:53,631
Move it!

391
01:27:00,400 --> 01:27:02,470
Wait here.

392
01:27:30,880 --> 01:27:33,872
Go back.

393
01:27:34,000 --> 01:27:36,070
Go!

394
01:27:36,200 --> 01:27:38,953
Go!

395
01:31:15,520 --> 01:31:17,750
Be careful by the tracks!

396
01:31:57,280 --> 01:32:03,310
I baked cakes for them,
and meat, too.

397
01:32:05,280 --> 01:32:07,919
They were so nice,
they didn't hurt us.

398
01:32:08,040 --> 01:32:11,919
I made strudel for them.
They said, "Mama, let it be good!"

399
01:32:12,040 --> 01:32:13,996
I said, "You'll gobble it up!"

400
01:32:14,120 --> 01:32:16,076
Tovarish!

401
01:32:16,200 --> 01:32:20,432
There is room here!
Come and sit down!

402
01:32:20,560 --> 01:32:23,677
Sit down.

403
01:32:23,800 --> 01:32:27,395
- Thank you.
- You're Hungarian?

404
01:32:27,520 --> 01:32:32,878
- Yes.
- In this uniform?

405
01:32:33,000 --> 01:32:35,309
I thought you were Russian.

406
01:32:37,120 --> 01:32:39,111
Here.

407
01:32:39,240 --> 01:32:41,196
Eat.

408
01:33:17,080 --> 01:33:20,868
So you are Hungarian?
Well, well...

409
01:33:21,000 --> 01:33:24,356
We've already met, haven't we?

410
01:33:27,760 --> 01:33:29,751
I want to tell you something.

411
01:33:29,880 --> 01:33:32,030
Come! Take his gun.

412
01:33:44,800 --> 01:33:46,597
Look!

413
01:33:46,720 --> 01:33:49,518
Look! The scoundrels!
Don't let them get you!

