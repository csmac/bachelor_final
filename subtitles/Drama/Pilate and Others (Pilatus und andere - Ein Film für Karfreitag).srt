1
00:04:58,013 --> 00:04:59,913
What's your pension number?

2
00:05:05,054 --> 00:05:06,525
What?

3
00:05:06,890 --> 00:05:09,415
I said what's your pension number.

4
00:05:16,532 --> 00:05:17,521
That's ok.

5
00:05:21,973 --> 00:05:23,065
Money order notice

6
00:05:44,428 --> 00:05:47,727
- Nothing.
- But there's the notice.

7
00:05:48,599 --> 00:05:54,595
There is nothing...
see for yourself.

8
00:06:08,486 --> 00:06:09,612
When should I come?

9
00:06:12,157 --> 00:06:13,886
Perhaps when you get another notice.

10
00:06:17,229 --> 00:06:18,355
Hey!

11
00:06:20,232 --> 00:06:21,460
Your notice.

12
00:06:23,803 --> 00:06:24,929
Bloody mess!

13
00:06:48,227 --> 00:06:50,127
- Miss Poland is on.
- I'm studying.

14
00:06:50,598 --> 00:06:52,623
Come and watch.

15
00:07:23,097 --> 00:07:24,189
Great, thanks.

16
00:09:42,308 --> 00:09:48,577
My name is Maugojata.
I'm 24 years old...

17
00:09:51,885 --> 00:09:56,584
Entry number six, Miss...

18
00:09:59,460 --> 00:10:01,064
I'm Katajina.

19
00:10:01,462 --> 00:10:05,558
I'm 20 years old.
I'm graduated from Medical College.

20
00:12:30,683 --> 00:12:31,615
Hello?

21
00:12:33,451 --> 00:12:34,349
Hello?

22
00:12:38,424 --> 00:12:41,052
Enough of this idiocy.
Who is it?

23
00:12:46,899 --> 00:12:48,730
I hear you breathing.
You bastard.

24
00:13:10,091 --> 00:13:11,115
Hello?

25
00:13:12,593 --> 00:13:13,582
I'm sorry.

26
00:14:06,615 --> 00:14:07,604
Milk wasn't delivered.

27
00:14:09,485 --> 00:14:13,282
- I get it only every other day.
- Would you like to deliver milk?

28
00:14:14,222 --> 00:14:17,851
- I haven't time.
- Neither has the milkman.

29
00:14:22,965 --> 00:14:27,860
He sends you his best wishes...
and "SSIA"...

30
00:14:29,072 --> 00:14:30,195
what's that?

31
00:14:30,474 --> 00:14:31,463
A code.

32
00:14:32,876 --> 00:14:37,643
He'll see the world. Pity you
couldn't go with him.

33
00:14:39,017 --> 00:14:40,006
I'm not sorry.

34
00:14:48,859 --> 00:14:50,451
From Marcin.

35
00:14:55,567 --> 00:14:57,535
What will happen to you when
he gets back?

36
00:15:02,139 --> 00:15:05,108
I'd like to see you settled down.

37
00:15:06,810 --> 00:15:11,999
Marcin likes travelling. I'm afraid
he won't stay home for long.

38
00:15:13,284 --> 00:15:17,050
Maybe you could go on
living here.

39
00:16:49,417 --> 00:16:50,753
Gas emergency.

40
00:16:51,085 --> 00:16:53,198
I want to report a gas leak.

41
00:16:53,721 --> 00:16:56,984
- You're sure of it?
- I can smell it and hear it.

42
00:16:57,793 --> 00:16:59,210
- Where's it leaking from?
- The cooker.

43
00:16:59,561 --> 00:17:04,000
- What address?
- Piratowka, flat 376.

44
00:17:05,099 --> 00:17:07,761
We'll be there soon.

45
00:17:29,325 --> 00:17:30,877
Marcin wrote...

46
00:17:31,262 --> 00:17:35,621
he went to the cinema with
an Arab girl.

47
00:17:36,700 --> 00:17:38,361
I wonder what they saw.

48
00:17:39,636 --> 00:17:40,500
Tomek.

49
00:17:43,073 --> 00:17:44,665
Are you seeing somebody?

50
00:17:47,279 --> 00:17:48,041
No.

51
00:17:51,815 --> 00:17:55,376
I'm not sure whether you have
been told this.

52
00:17:56,954 --> 00:18:03,291
Girls only pretend to be casual,
they kiss boys easily...

53
00:18:05,397 --> 00:18:08,525
but in fact...
they like tenderness.

54
00:18:11,403 --> 00:18:15,521
If you'd like to bring
someone here...

55
00:18:16,540 --> 00:18:18,508
you needn't be shy about it.

56
00:18:19,811 --> 00:18:21,005
I haven't anyone.

57
00:20:37,020 --> 00:20:37,987
I'll wait.

58
00:20:40,155 --> 00:20:41,019
How are you?

59
00:20:42,025 --> 00:20:42,957
What do you want?

60
00:20:43,959 --> 00:20:48,025
I heard you need someone
to deliver milk.

61
00:20:49,032 --> 00:20:51,331
Do you want to deliver milk?
How old are you?

62
00:20:51,901 --> 00:20:54,131
- Nineteen.
- A student?

63
00:20:54,737 --> 00:20:57,385
No. I work in our post office.

64
00:20:58,041 --> 00:21:02,401
You realize you must get up at five?

65
00:21:03,481 --> 00:21:05,381
I get up early, anyway.

66
00:22:00,039 --> 00:22:02,098
- Hello?
- The milkman.

67
00:22:10,115 --> 00:22:11,776
You didn't put a bottle out.

68
00:22:12,985 --> 00:22:13,974
That's right.

69
00:23:56,226 --> 00:23:57,318
Son of a bitch!

70
00:25:40,968 --> 00:25:41,696
Tomek.

71
00:25:44,338 --> 00:25:45,134
Tomek.

72
00:25:51,111 --> 00:25:52,043
Not asleep?

73
00:25:53,681 --> 00:25:54,943
Come and sit down.

74
00:26:06,561 --> 00:26:07,721
Is something wrong?

75
00:26:14,903 --> 00:26:16,694
Why do people cry?

76
00:26:17,138 --> 00:26:20,802
Don't you know?
Haven't you ever cried?

77
00:26:21,709 --> 00:26:23,955
Once, long ago.

78
00:26:24,512 --> 00:26:26,309
When you were left?

79
00:26:28,483 --> 00:26:29,780
People cry...

80
00:26:30,286 --> 00:26:34,699
for various reasons.

81
00:26:35,791 --> 00:26:39,294
When someone dies, when they're
left alone...

82
00:26:40,162 --> 00:26:42,114
When they...
can't stand it anymore.

83
00:26:42,598 --> 00:26:44,190
What?

84
00:26:45,301 --> 00:26:48,457
Living. When they're hurt.

85
00:26:49,239 --> 00:26:52,037
Can you do anything about it?

86
00:26:56,012 --> 00:26:59,809
Once Marcin had toothache

87
00:27:00,749 --> 00:27:05,563
he heated the iron and
pressed it here.

88
00:27:06,755 --> 00:27:09,121
He forgot about the tooth.

89
00:29:02,477 --> 00:29:03,492
You before, wasn't it?

90
00:29:03,744 --> 00:29:04,904
So?

91
00:29:05,780 --> 00:29:07,111
Another notice.

92
00:29:17,392 --> 00:29:18,120
Nothing.

93
00:29:19,426 --> 00:29:23,037
- It's the second time.
- Yes, but there's nothing.

94
00:29:23,932 --> 00:29:24,948
A mess, eh?

95
00:29:25,200 --> 00:29:25,948
Really?

96
00:29:26,134 --> 00:29:28,568
Could you ask someone
more senior?

97
00:29:29,804 --> 00:29:30,740
What?

98
00:29:30,973 --> 00:29:33,203
The manager, or someone.

99
00:29:47,757 --> 00:29:48,951
Wait a minute please.

100
00:29:50,025 --> 00:29:50,827
What's the matter?

101
00:29:51,027 --> 00:29:53,459
I don't know who's sent me money, but
this is the second notice I've had.

102
00:29:54,062 --> 00:29:57,828
No money is here.

103
00:29:59,067 --> 00:30:00,297
You got the notice.

104
00:30:00,603 --> 00:30:01,993
But not any money.

105
00:30:02,338 --> 00:30:03,805
Give that notice to me.

106
00:30:08,377 --> 00:30:09,875
Who did you expect money from?

107
00:30:10,247 --> 00:30:12,760
No idea... 24,000...

108
00:30:13,383 --> 00:30:14,800
So how did you know it was due?

109
00:30:15,151 --> 00:30:17,397
I got two notices, didn't I?

110
00:30:17,954 --> 00:30:19,719
But there's no money.

111
00:30:20,157 --> 00:30:22,148
- So why was I sent notices?
- Mr. Bachek!

112
00:30:22,860 --> 00:30:24,157
Mr. Bachek!

113
00:30:25,796 --> 00:30:27,730
What's the matter, sir?

114
00:30:29,933 --> 00:30:31,617
What notices are these?

115
00:30:32,035 --> 00:30:33,159
Well, notices.

116
00:30:33,438 --> 00:30:37,502
- It's your round. Did you issue them?
- No, I write in pencil.

117
00:30:38,509 --> 00:30:39,524
They were in my box.

118
00:30:39,776 --> 00:30:42,666
Not from us.

119
00:30:43,382 --> 00:30:46,617
But they were in my box,
your stamp is on them.

120
00:30:47,418 --> 00:30:48,514
This is a state post office!

121
00:30:48,786 --> 00:30:51,219
If you write your own notices...

122
00:30:51,822 --> 00:30:52,463
it's a police matter.

123
00:30:52,623 --> 00:30:53,906
Right. Give me the notices.

124
00:30:54,225 --> 00:30:57,717
No, they're forgeries.

125
00:30:59,063 --> 00:31:01,088
You wanted to swindle us.

126
00:31:27,392 --> 00:31:28,654
What do you want?

127
00:31:33,499 --> 00:31:36,923
I want to tell you
there was no money.

128
00:31:37,771 --> 00:31:40,137
- What about the notices?
- I sent them.

129
00:31:41,474 --> 00:31:44,335
Why? I don't understand.

130
00:31:45,044 --> 00:31:46,568
I wanted to see you.

131
00:31:47,514 --> 00:31:50,381
You wanted to see me?

132
00:32:00,094 --> 00:32:01,721
You were crying yesterday.

133
00:32:19,180 --> 00:32:20,204
How do you know?

134
00:32:29,225 --> 00:32:30,385
I peeped at you.

135
00:32:31,792 --> 00:32:32,588
What?

136
00:32:34,028 --> 00:32:35,859
I watched you through the window.

137
00:32:41,237 --> 00:32:45,435
Clear off... you meddler!

138
00:35:41,456 --> 00:35:42,218
Hello.

139
00:35:48,097 --> 00:35:49,462
I'll count to three.

140
00:35:50,399 --> 00:35:51,229
One...

141
00:35:52,967 --> 00:35:54,457
- Two...
- Hello.

142
00:35:57,172 --> 00:35:58,036
Are you looking?

143
00:36:02,043 --> 00:36:02,839
Yes.

144
00:36:03,546 --> 00:36:06,174
I've moved the bed, see?

145
00:36:07,116 --> 00:36:07,775
Yes.

146
00:36:08,351 --> 00:36:10,945
Have fun.

147
00:38:19,519 --> 00:38:20,383
Hey!

148
00:38:21,722 --> 00:38:22,620
You shit!

149
00:38:26,927 --> 00:38:28,417
Postman!

150
00:38:36,571 --> 00:38:37,799
Come here!

151
00:38:49,851 --> 00:38:58,953
Come here, you bastard!

152
00:39:27,156 --> 00:39:28,418
You, sweetheart?

153
00:39:33,462 --> 00:39:34,292
Put up your hands.

154
00:39:35,731 --> 00:39:36,698
Don't do it again.

155
00:39:52,282 --> 00:39:55,308
Very unhealthy at your age.

156
00:40:07,064 --> 00:40:08,053
Will you sleep?

157
00:40:52,379 --> 00:40:54,040
I thought it would be you.

158
00:40:55,648 --> 00:40:57,377
Want to come in? No one's here.

159
00:41:01,220 --> 00:41:03,814
Everything in good order?

160
00:41:11,330 --> 00:41:12,763
You look terrific.

161
00:41:16,235 --> 00:41:17,600
Don't you know how to fight?

162
00:41:30,084 --> 00:41:34,180
Why are you peeping at me?

163
00:41:37,257 --> 00:41:38,451
Because I love you.

164
00:41:47,869 --> 00:41:49,097
I really do.

165
00:41:53,107 --> 00:41:55,769
And what do you want?

166
00:41:56,812 --> 00:41:57,972
I don't know.

167
00:41:58,814 --> 00:42:02,409
- Do you want to kiss me?
- No.

168
00:42:03,351 --> 00:42:07,683
Perhaps you want to
make love to me?

169
00:42:08,755 --> 00:42:09,585
No.

170
00:42:11,059 --> 00:42:13,323
Want to go away with me?

171
00:42:14,029 --> 00:42:17,294
- To the lakes, or to Budapest?
- No.

172
00:42:19,969 --> 00:42:21,300
So what do you want?

173
00:42:22,939 --> 00:42:23,997
Nothing.

174
00:42:27,010 --> 00:42:27,999
Nothing?

175
00:42:30,880 --> 00:42:31,778
Yes.

176
00:44:03,209 --> 00:44:07,703
Could I invite you out to a cafe...
ice-cream...

177
00:44:33,207 --> 00:44:34,196
Sorry.

178
00:44:57,865 --> 00:44:59,662
Have you been watching long?

179
00:45:01,636 --> 00:45:02,625
A year.

180
00:45:03,571 --> 00:45:08,225
This morning...

181
00:45:09,378 --> 00:45:10,743
what was it you said?

182
00:45:15,017 --> 00:45:18,009
- I love you.
- There's no such thing.

183
00:45:19,521 --> 00:45:20,419
There is.

184
00:45:23,092 --> 00:45:23,956
No.

185
00:45:28,964 --> 00:45:33,833
Apart from loving me and working in
the post office, what do you do?

186
00:45:36,138 --> 00:45:37,400
I study languages.

187
00:45:38,239 --> 00:45:41,422
- What have you learned?
- Bulgarian.

188
00:45:42,211 --> 00:45:46,238
Bulgarian?
Great, but what for?

189
00:45:47,584 --> 00:45:49,857
We had two Bulgarians in
the orphanage.

190
00:45:50,421 --> 00:45:53,924
Then English, French, Italian.

191
00:45:54,791 --> 00:45:56,656
Now I'm learning Portuguese.

192
00:45:57,994 --> 00:46:00,121
- You're a strange one.
- No, I have a good memory.

193
00:46:01,499 --> 00:46:05,376
I remember
everything from the start.

194
00:46:06,336 --> 00:46:09,601
Even when you were born?

195
00:46:13,511 --> 00:46:16,002
- I think so, sometimes.
- And your parents?

196
00:46:17,449 --> 00:46:18,507
Not them.

197
00:46:20,184 --> 00:46:26,710
Remember a thin boy who used to
visit me in the autumn?

198
00:46:28,325 --> 00:46:32,739
Yes, he brought milk and rolls
and took away parcels.

199
00:46:33,832 --> 00:46:37,791
That's right. He never came back.

200
00:46:42,541 --> 00:46:43,701
I liked him.

201
00:46:45,544 --> 00:46:49,344
So did l, but he left...

202
00:46:50,648 --> 00:46:52,787
for Austria, then Australia.

203
00:46:53,318 --> 00:46:54,335
Australia?

204
00:46:54,587 --> 00:46:55,576
Yes.

205
00:47:05,864 --> 00:47:12,929
I've been removing your letters.
I didn't know they were from him.

206
00:47:14,774 --> 00:47:16,298
I work at the post office.

207
00:47:34,862 --> 00:47:39,033
You besiege me, you bring the milk
you send me false notices...

208
00:47:40,066 --> 00:47:44,264
you send gas workers, you pinch my letters.

209
00:47:51,946 --> 00:47:53,345
But it doesn't really matter.

210
00:47:57,951 --> 00:47:59,384
Haven't you any friends?

211
00:48:01,422 --> 00:48:02,999
One, but he's not here now.

212
00:48:03,390 --> 00:48:04,687
Where is he?

213
00:48:05,527 --> 00:48:07,666
He's with the UN Force in Syria.

214
00:48:08,196 --> 00:48:09,746
I lodge with his mother, opposite.

215
00:48:10,131 --> 00:48:11,173
He's different from me.

216
00:48:11,432 --> 00:48:12,524
I think so.

217
00:48:15,004 --> 00:48:16,767
He peeped at you, too.

218
00:48:20,642 --> 00:48:22,132
What did he tell you?

219
00:48:23,645 --> 00:48:26,944
Before he left he showed me your
window and gave me his opera glasses.

220
00:48:29,584 --> 00:48:32,348
- What did he tell you?
- SSIA.

221
00:48:33,288 --> 00:48:34,277
What's it stand for?

222
00:48:36,825 --> 00:48:37,792
Go ahead.

223
00:48:40,661 --> 00:48:41,650
She...

224
00:48:43,933 --> 00:48:44,797
She?

225
00:48:46,101 --> 00:48:47,068
Quickly!

226
00:48:49,038 --> 00:48:51,257
She spreads it around.

227
00:48:51,807 --> 00:48:53,001
That's okay.

228
00:48:53,877 --> 00:48:55,469
What would you like?

229
00:48:57,814 --> 00:49:00,146
I don't like ice-cream.
But I'll eat.

230
00:49:01,550 --> 00:49:03,211
- I'll have ice-cream.
- It's off.

231
00:49:03,886 --> 00:49:05,217
What, then?

232
00:49:05,956 --> 00:49:07,453
- Tea.
- We've got that.

233
00:49:07,824 --> 00:49:10,384
I'd like red wine.

234
00:49:11,762 --> 00:49:13,957
Two glasses of red wine, and tea.
- Okay.

235
00:49:18,035 --> 00:49:18,933
Give me your hand.

236
00:49:37,589 --> 00:49:38,886
Caress me.

237
00:49:56,074 --> 00:49:56,904
Look there!

238
00:50:03,447 --> 00:50:06,541
They seem to know more about it.

239
00:50:33,113 --> 00:50:33,977
Our bus.

240
00:50:36,115 --> 00:50:36,979
I have a good idea.

241
00:50:37,850 --> 00:50:42,116
If we catch it, you're coming to
my place. If not, no. Agreed?

242
00:50:44,858 --> 00:50:45,620
Let's go!

243
00:51:49,225 --> 00:51:51,216
Such snow-scenes are
embroidered, too.

244
00:52:00,102 --> 00:52:01,296
Do I always do that?

245
00:52:02,303 --> 00:52:03,159
Not that I've seen.

246
00:52:03,372 --> 00:52:06,899
Not everything's routine.

247
00:52:10,312 --> 00:52:11,336
Where did you get that?

248
00:52:12,616 --> 00:52:18,452
It's a souvenir. It's yours now.

249
00:52:27,463 --> 00:52:28,794
I'm not good.

250
00:52:30,868 --> 00:52:32,199
Don't give it to me.

251
00:52:38,242 --> 00:52:41,473
You know I'm not good.

252
00:52:46,618 --> 00:52:47,516
Really.

253
00:52:50,688 --> 00:52:53,248
I don't care. I love you.

254
00:53:11,409 --> 00:53:12,671
What else do you know?

255
00:53:13,679 --> 00:53:16,648
What do you see when one
of them is with me?

256
00:53:21,353 --> 00:53:26,484
It's called... making love.

257
00:53:29,729 --> 00:53:32,755
I don't watch that any more.

258
00:53:33,665 --> 00:53:37,863
It has nothing to do with love.
Tell me what I do.

259
00:53:44,943 --> 00:53:46,570
You undress.

260
00:53:49,516 --> 00:53:53,714
Then you undress them.

261
00:53:55,020 --> 00:53:58,953
You go to bed, or on the floor.

262
00:53:59,927 --> 00:54:01,417
And?

263
00:54:02,463 --> 00:54:03,691
Or in the kitchen.

264
00:54:06,100 --> 00:54:07,829
You close your eyes...

265
00:54:09,804 --> 00:54:16,410
then you lift your hands...
above your head.

266
00:54:18,045 --> 00:54:21,811
Show me how. Go on.

267
00:54:41,636 --> 00:54:44,469
- Have you had a girl?
- No.

268
00:54:46,140 --> 00:54:50,406
And when you watch me
do you do it with yourself?

269
00:54:58,653 --> 00:55:03,590
I used to. Not now.

270
00:55:11,033 --> 00:55:12,159
You know it's sinful.

271
00:55:15,371 --> 00:55:19,171
Now...

272
00:55:22,478 --> 00:55:23,809
I think only about you.

273
00:55:35,258 --> 00:55:37,692
I have nothing underneath
you know, don't you?

274
00:55:41,065 --> 00:55:42,910
When a woman wants a man...

275
00:55:43,367 --> 00:55:48,532
she becomes wet inside.

276
00:55:50,608 --> 00:55:52,303
I'm wet now.

277
00:55:59,217 --> 00:56:02,482
Such delicate hands.

278
00:56:04,089 --> 00:56:05,078
Don't be afraid.

279
00:57:12,525 --> 00:57:13,457
Already?

280
00:57:19,201 --> 00:57:21,897
That's all there is to love.

281
00:57:29,911 --> 00:57:32,573
Wash in the bathroom
there's a towel.

282
01:00:20,988 --> 01:00:23,889
I'm sorry. Please come back.

283
01:03:00,587 --> 01:03:01,576
Go back.

284
01:05:06,250 --> 01:05:07,182
Sorry.

285
01:05:08,218 --> 01:05:09,412
Did I wake you up?

286
01:05:10,154 --> 01:05:11,143
No.

287
01:05:13,791 --> 01:05:16,453
I'm looking for...

288
01:05:18,896 --> 01:05:20,366
He left his coat.

289
01:05:20,731 --> 01:05:23,131
Come in.

290
01:05:49,462 --> 01:05:50,622
Leave it there.

291
01:05:59,438 --> 01:06:00,213
Is he out?

292
01:06:00,405 --> 01:06:01,235
No.

293
01:06:01,973 --> 01:06:05,291
- He's in hospital.
- What happened?

294
01:06:06,113 --> 01:06:12,772
Nothing serious.
He'll be out in a few days.

295
01:06:14,420 --> 01:06:19,368
I'd like to visit him.
He just left my place...

296
01:06:20,593 --> 01:06:21,582
I know.

297
01:06:22,928 --> 01:06:27,922
I think that... I have hurt him.

298
01:06:29,170 --> 01:06:32,940
Don't visit him, he will be back.

299
01:06:33,873 --> 01:06:38,503
What happened?

300
01:06:42,015 --> 01:06:44,108
You'll probably find it funny.

301
01:06:50,224 --> 01:06:51,919
He fell in love with you.

302
01:07:03,304 --> 01:07:07,718
He watched you through this.

303
01:07:08,811 --> 01:07:10,506
Stolen, probably.

304
01:07:11,213 --> 01:07:13,579
He used opera-glasses before.

305
01:07:14,884 --> 01:07:20,634
And he set this alarm-clock
for half-past eight.

306
01:07:22,057 --> 01:07:24,457
- You come home then, don't you?
- About then.

307
01:07:34,604 --> 01:07:36,538
He made a bad choice?

308
01:07:40,174 --> 01:07:41,402
Yes.

309
01:07:43,012 --> 01:07:49,315
I don't need much, but I'm afraid
without someone in the house.

310
01:07:53,756 --> 01:07:54,780
Good night.

311
01:08:00,730 --> 01:08:05,133
- May I phone, to enquire?
- We haven't a telephone.

312
01:08:18,515 --> 01:08:19,504
I'm sorry.

313
01:08:20,950 --> 01:08:22,047
- What is his name?
- Tomek.

314
01:08:22,320 --> 01:08:23,480
Tomek.

315
01:08:29,960 --> 01:08:30,984
Tomek.

316
01:10:04,758 --> 01:10:06,988
Closed.

317
01:10:11,966 --> 01:10:13,490
What is this?

318
01:10:13,868 --> 01:10:16,632
Don't delete anything
without making notes.

319
01:10:52,342 --> 01:10:53,116
Excuse me.

320
01:10:53,308 --> 01:10:56,971
- Number?
- 376.

321
01:10:58,947 --> 01:11:01,142
Nothing.

322
01:11:01,817 --> 01:11:06,982
Do you know what happened to
the young man who worked in your branch?

323
01:11:10,828 --> 01:11:14,059
It seems he cut his wrists out of love.

324
01:11:21,237 --> 01:11:22,548
Do you know his name?

325
01:11:22,873 --> 01:11:26,707
You'll have to ask the manageress.
I don't know.

326
01:12:10,922 --> 01:12:11,911
Hello?

327
01:12:13,992 --> 01:12:15,084
Hello!

328
01:12:16,362 --> 01:12:23,165
Tomek? Say something.

329
01:12:41,387 --> 01:12:42,786
I've looked for you everywhere.

330
01:12:44,824 --> 01:12:50,194
I've looked for you in several
hospitals, to tell you...

331
01:12:53,334 --> 01:12:54,494
you were right.

332
01:12:55,937 --> 01:12:56,631
Tomek.

333
01:12:56,804 --> 01:13:00,501
Do you hear me? You were right.

334
01:13:03,210 --> 01:13:08,842
I don't know what to say to you.
I don't know how...

335
01:13:19,061 --> 01:13:19,836
Hello?

336
01:13:20,029 --> 01:13:21,873
- Maria Magdlena?
- I'm Magda.

337
01:13:22,330 --> 01:13:23,319
I'm Boytush.

338
01:13:23,565 --> 01:13:25,009
Was it you just now?

339
01:13:25,367 --> 01:13:27,265
Yeah, but it didn't hook up.

340
01:13:27,736 --> 01:13:29,420
- Did you hear what I said?
- No.

341
01:13:29,837 --> 01:13:31,236
I have a problem with Viola...

342
01:14:56,694 --> 01:15:00,528
Excuse me, is he back?

343
01:15:02,667 --> 01:15:03,793
Not yet.

344
01:16:35,630 --> 01:16:36,562
Is he back?

345
01:16:38,834 --> 01:16:40,461
Yes.

346
01:17:41,699 --> 01:17:42,961
I'd like to talk...

347
01:17:49,040 --> 01:17:50,360
Just a minute...

348
01:17:50,688 --> 01:17:55,238
Repair and Synchronization by
Easy Subtitles Synchronizer 1.0.0.0

