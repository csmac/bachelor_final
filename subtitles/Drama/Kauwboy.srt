﻿1
00:00:18,892 --> 00:00:21,724
First, there is nothing.
There is nothing.

2
00:00:23,433 --> 00:00:27,261
Nothing at all.
Nothing at all, it is all black.

3
00:00:30,267 --> 00:00:32,720
- But then: Boom.
- That's right.

4
00:00:45,392 --> 00:00:46,801
I'm going to win.

5
00:01:18,933 --> 00:01:21,256
In a film by Boudewijn Koole.

6
00:01:23,808 --> 00:01:30,255
There goes Speedy and behind him
drives the slow snail.

7
00:03:02,683 --> 00:03:04,010
I've got your little one.

8
00:04:31,600 --> 00:04:33,756
You're a tough one.

9
00:04:39,808 --> 00:04:43,719
What would you like to eat?
Mashed potatoes?

10
00:04:45,350 --> 00:04:50,091
I hate it too,
but dad wanted it for dinner.

11
00:04:53,350 --> 00:04:58,884
How about mashed potatoes
with cauliflower?

12
00:05:00,183 --> 00:05:02,055
Do you fancy this?

13
00:05:04,892 --> 00:05:07,806
Just cauliflower, I like that.

14
00:05:10,975 --> 00:05:15,597
I think your mum is really stupid
chucking you out of the nest like that.

15
00:05:15,767 --> 00:05:18,469
You've got to eat, you know.

16
00:05:19,558 --> 00:05:24,760
E, A, T, eat or you'll die.

17
00:05:24,892 --> 00:05:26,681
A drink?

18
00:05:36,475 --> 00:05:38,300
Would you like to see the bathroom?

19
00:05:39,725 --> 00:05:44,299
If you have to go, lift up the seat.
The towels are over there.

20
00:05:51,767 --> 00:05:56,009
That's where mummy used to sleep
and daddy still does.

21
00:05:56,600 --> 00:05:59,551
He doesn't like pets or plants.

22
00:05:59,767 --> 00:06:03,298
He always says:
Animals and plants belong outside.

23
00:06:04,808 --> 00:06:06,764
It's so quiet, isn't it?

24
00:06:09,225 --> 00:06:11,678
Look, this is my room.

25
00:06:13,600 --> 00:06:20,343
That's dad and mum in America.
That's me with dad and mum.

26
00:06:20,517 --> 00:06:24,759
Do you know how we went to America?
By plane.

27
00:06:24,933 --> 00:06:28,844
You can't fly yet, but you'll learn.

28
00:06:30,017 --> 00:06:33,761
That's mummy performing in a band.

29
00:06:34,892 --> 00:06:37,048
In Westland.

30
00:06:37,517 --> 00:06:39,389
That's in South Africa.

31
00:06:39,600 --> 00:06:44,837
We fed the giraffes
out of a little train.

32
00:07:02,392 --> 00:07:06,089
That's mummy, she made it herself.

33
00:07:06,267 --> 00:07:08,506
Do you like it too?

34
00:07:08,683 --> 00:07:12,298
I guess so, because your ear
is up against the speaker.

35
00:07:30,225 --> 00:07:35,724
It's about the stars and the moon
and stuff like that.

36
00:07:37,725 --> 00:07:40,723
I play it every night
before I go to sleep.

37
00:07:47,558 --> 00:07:54,717
I'm glad I found you. You're going
to sleep right next to me, won't you?

38
00:08:07,892 --> 00:08:09,598
Goodnight.

39
00:08:42,725 --> 00:08:46,766
Jackdaw, are you going to eat?
Otherwise you'll die.

40
00:08:48,725 --> 00:08:53,715
Please? If you're going to die
I might as well have left you there.

41
00:08:53,892 --> 00:08:57,340
You're so sweet,
I want to keep you as a pet.

42
00:08:57,475 --> 00:09:01,800
Okay, that's it, come on, eat.

43
00:09:09,892 --> 00:09:11,799
Come on. Please?

44
00:09:36,058 --> 00:09:38,381
Be quiet, or you'll wake dad up.

45
00:10:31,392 --> 00:10:35,089
Hello mum, how are you?

46
00:10:36,350 --> 00:10:40,924
I'm fine and so is dad.
I slept in his bed last night.

47
00:10:42,350 --> 00:10:43,629
It was really nice.

48
00:10:47,558 --> 00:10:49,881
You've got your own bed.

49
00:10:51,142 --> 00:10:53,216
And he let me stay.

50
00:10:54,267 --> 00:10:55,807
Now move.

51
00:11:01,392 --> 00:11:04,259
Well mum, how are things over there?

52
00:11:05,308 --> 00:11:08,508
Is it fun? Do you miss us?

53
00:11:17,100 --> 00:11:18,890
No, just a little bit.

54
00:11:23,100 --> 00:11:30,175
Mum, I've got a surprise for you.
It's for your birthday.

55
00:11:30,350 --> 00:11:33,798
It's black and it's just as big as a...

56
00:11:35,600 --> 00:11:37,721
as the peanut butter lid.

57
00:11:39,392 --> 00:11:41,050
I've got to go, bye.

58
00:12:02,433 --> 00:12:05,052
It's an alligator, not a crocodile.

59
00:12:10,267 --> 00:12:12,837
Mum had crocodile boots like that.

60
00:12:30,850 --> 00:12:32,177
Jojo...

61
00:12:35,142 --> 00:12:38,009
Are we going to fight? Boxing?

62
00:12:39,517 --> 00:12:45,300
You fight like a girl.
No ear-pulling, Jojo.

63
00:12:57,475 --> 00:12:59,715
So I fight like a girl, right?

64
00:13:26,642 --> 00:13:28,051
I've had enough now.

65
00:13:42,058 --> 00:13:43,800
Jojo is free.

66
00:13:43,933 --> 00:13:45,592
Over here.

67
00:13:45,808 --> 00:13:47,633
Keep the ball in front of you.

68
00:13:56,683 --> 00:14:01,673
Well done. Jojo is free.

69
00:14:01,850 --> 00:14:05,429
Well done, come on swim yourself free.

70
00:14:07,100 --> 00:14:09,091
Jairo, take it easy.

71
00:14:27,183 --> 00:14:28,557
Knock it off.

72
00:14:28,683 --> 00:14:31,966
Listen, Jojo. Are you listening?

73
00:14:32,142 --> 00:14:36,882
If you do that again, I'll call your dad
and you're out of the team.

74
00:14:37,058 --> 00:14:39,926
But he started it.
I don't care.

75
00:14:40,975 --> 00:14:45,928
If he kicks you, you come to me.
You don't bite. Shut up.

76
00:14:49,600 --> 00:14:52,349
Thank you. I'll see you next week.

77
00:15:10,808 --> 00:15:12,716
Your little one eats.

78
00:15:15,975 --> 00:15:18,890
Don't worry about your little one,
he's eating.

79
00:15:19,475 --> 00:15:21,430
He's safe with me.

80
00:15:30,892 --> 00:15:33,048
Come on, boy. You've got to eat.

81
00:15:43,433 --> 00:15:46,550
Yes, well done.
Do you want another one?

82
00:15:51,350 --> 00:15:54,680
Well done. Who's a big eater?

83
00:16:18,933 --> 00:16:20,805
There you are, dad.

84
00:16:22,475 --> 00:16:24,466
For you, dad. Just put it down.

85
00:16:24,683 --> 00:16:28,547
Don't you want a drink?
Just put it down.

86
00:16:32,600 --> 00:16:36,925
Look, a boy in my class
found a jackdaw.

87
00:16:37,100 --> 00:16:41,674
And... Well, he tried to put it back.

88
00:16:41,850 --> 00:16:45,796
But then the mother
jackdaw came at him.

89
00:16:46,725 --> 00:16:49,842
And then the chick
fell out of the nest again.

90
00:16:52,392 --> 00:16:54,927
So then he took it home.

91
00:16:56,100 --> 00:17:00,556
His parents let him. If he were your son
would you let him keep it?

92
00:17:00,683 --> 00:17:06,348
No, you always put it back.
Yes, but he tried that.

93
00:17:06,475 --> 00:17:08,715
And then it fell out again.

94
00:17:10,892 --> 00:17:13,925
So what then?
Would you let him keep it?

95
00:17:15,267 --> 00:17:19,343
Pass me that extension thing.
Dad?

96
00:17:19,517 --> 00:17:23,214
Could he keep it?
Animals and plants belong outside.

97
00:17:23,392 --> 00:17:24,849
Okay, dad.

98
00:17:45,600 --> 00:17:47,756
A jackdaw.

99
00:18:02,392 --> 00:18:06,634
Don't feed jackdaws bread and milk
or their feathers will grow weak.

100
00:18:06,850 --> 00:18:12,882
Every two hours, feed them a mix
of cat food and birdseed, or they'll die.

101
00:18:16,308 --> 00:18:20,254
A tame jackdaw can live up to 25 years.

102
00:18:20,433 --> 00:18:27,259
They're monogamous, which means
they always stay with their partner.

103
00:18:29,058 --> 00:18:33,597
Take good care of a jackdaw
and you'll have a friend for life.

104
00:19:00,517 --> 00:19:03,847
Hand underneath thumb behind

105
00:19:04,058 --> 00:19:07,590
shoulders forward
pinch in the ball

106
00:19:07,725 --> 00:19:11,422
throw it far follow it through

107
00:19:11,600 --> 00:19:14,349
and score a goal.

108
00:19:14,558 --> 00:19:20,140
Did you see fatty push me under?
But he couldn't, I threw the ball.

109
00:19:20,308 --> 00:19:25,345
Then I dived under him.
You should have seen his face.

110
00:19:25,433 --> 00:19:28,467
Yes, and then Jairo threw the ball to me.

111
00:19:28,642 --> 00:19:33,382
And I slammed it into the top corner.
Yes, that's the spirit.

112
00:19:33,558 --> 00:19:37,339
I want to see that every time, Jojo.
Are we proud of Jojo?

113
00:19:37,475 --> 00:19:40,010
Yes.
Well, give him the treatment.

114
00:19:59,517 --> 00:20:00,891
Dad, we won.

115
00:20:02,642 --> 00:20:05,675
Everybody...
Bags don't go on tables.

116
00:20:06,725 --> 00:20:12,259
There was a fat boy who pushed me
under but I dived underneath him.

117
00:20:12,392 --> 00:20:16,717
We won 5-3 and I scored all five.

118
00:20:19,350 --> 00:20:23,640
But it was great fun and
everybody lifted me up and...

119
00:20:23,850 --> 00:20:25,805
Go to your room.

120
00:20:27,725 --> 00:20:31,386
- And in the bus they...
- Now.

121
00:21:19,975 --> 00:21:23,056
Don't be afraid.
You're safe here, with me.

122
00:21:47,517 --> 00:21:49,342
It's not too bad, is it?

123
00:22:45,975 --> 00:22:49,755
Hello mum, dad cooked
something really nice.

124
00:22:51,517 --> 00:22:54,847
We had spaghetti in a red sauce.

125
00:22:58,558 --> 00:23:03,429
I won today at water polo.
I scored eight goals.

126
00:23:09,808 --> 00:23:15,639
There's a new girl on the water polo
team, she's called Yenthe.

127
00:23:16,933 --> 00:23:21,886
She always chews blue bubble-gum.
She blows bubbles the size of my head.

128
00:23:25,808 --> 00:23:28,676
No, she's just as special
as any other girl.

129
00:24:23,058 --> 00:24:26,092
Let me have a go. Be my guest.

130
00:24:26,267 --> 00:24:30,675
But be careful because I just burnt
my fingers three times in a row.

131
00:24:33,725 --> 00:24:38,596
See, I can do it.
That's beginner's luck.

132
00:24:40,433 --> 00:24:42,140
Show me again.

133
00:24:46,100 --> 00:24:47,379
See?

134
00:24:51,350 --> 00:24:52,629
You can do it.

135
00:25:04,975 --> 00:25:07,013
Smoke signals.

136
00:25:14,892 --> 00:25:16,598
A smoke signal.

137
00:25:55,017 --> 00:25:58,015
Is it fun?
Would you like to try?

138
00:25:58,142 --> 00:25:59,848
Yes, please.

139
00:26:20,975 --> 00:26:22,468
Where is your mum?

140
00:26:25,225 --> 00:26:28,638
She's on tour in America.

141
00:26:30,600 --> 00:26:34,973
When is she coming back?
I don't know.

142
00:26:37,433 --> 00:26:41,972
It's her birthday in a few days.
I'm going to bake her an apple pie.

143
00:26:50,808 --> 00:26:55,133
- Do you miss her a lot?
- A bit.

144
00:27:05,642 --> 00:27:07,265
Hey, little one.

145
00:27:18,433 --> 00:27:20,803
Have a cuddle with Yenthe.

146
00:27:23,642 --> 00:27:25,549
Come on, cuddle with Yenthe.

147
00:27:26,933 --> 00:27:29,682
Go on then.

148
00:27:33,933 --> 00:27:38,258
Cool animal, isn't it?
Yes, how big do they get?

149
00:27:39,558 --> 00:27:42,640
This one won't grow much more,
I don't think.

150
00:27:48,683 --> 00:27:50,390
He's so sweet, isn't he?

151
00:28:19,433 --> 00:28:23,723
Do you know where the Lada keys are?
Downstairs, on the table?

152
00:28:26,725 --> 00:28:30,138
Are you in the same class as Jojo?
No, water polo team.

153
00:28:33,683 --> 00:28:37,380
What are you doing?
Just sitting.

154
00:28:40,433 --> 00:28:42,673
Behave yourself, now. Yes, dad.

155
00:28:46,850 --> 00:28:50,891
And clean the place up, it smells in here.
Sure dad, I will.

156
00:28:53,433 --> 00:28:57,474
- Bye.
- Bye, dad.

157
00:29:17,475 --> 00:29:20,722
- See you.
- See you tomorrow.

158
00:29:47,142 --> 00:29:49,760
How long have you had him for?
A week.

159
00:29:49,975 --> 00:29:53,590
A week?
That's quite an achievement.

160
00:30:00,933 --> 00:30:04,346
- What's it called?
- Jack.

161
00:30:06,683 --> 00:30:12,383
Do you know what the problem is, Jojo?
It's silly but they always die.

162
00:30:15,308 --> 00:30:18,508
This one is still alive.
In the nest it might survive.

163
00:30:18,683 --> 00:30:21,136
If you keep it, it'll die. Put it back.

164
00:30:21,308 --> 00:30:22,718
No, I won't.

165
00:30:26,350 --> 00:30:29,929
It needs to be scared of cars and dogs.
I'll teach him.

166
00:30:30,100 --> 00:30:33,300
It has to be afraid of you,
of humans, doesn't it?

167
00:30:34,517 --> 00:30:36,591
- Yes.
- Well, there you are.

168
00:30:36,808 --> 00:30:38,799
You heard me, put it back. No.

169
00:30:41,642 --> 00:30:45,552
He'll think you're his mother.
He already thinks that.

170
00:30:47,767 --> 00:30:53,135
You'll get attached to it...
I already am, and he is attached to me.

171
00:31:00,850 --> 00:31:04,429
You heard me, put it back.
That's the end of it.

172
00:31:10,267 --> 00:31:11,676
Dickhead.

173
00:31:53,642 --> 00:31:57,682
Take good care of yourself, little one.
Bye.

174
00:32:48,475 --> 00:32:49,802
Wait a second.

175
00:32:50,850 --> 00:32:53,801
Look at me.

176
00:32:57,267 --> 00:32:58,676
Look at me.

177
00:32:59,725 --> 00:33:03,671
You understand that it's
for your own good, don't you?

178
00:33:07,392 --> 00:33:10,722
Well, do your best.
Score some goals.

179
00:33:11,767 --> 00:33:14,848
See you tomorrow,
we'll do something fun.

180
00:33:23,933 --> 00:33:26,173
I'm not talking to you anymore.

181
00:34:57,350 --> 00:34:58,891
Jack.

182
00:35:09,058 --> 00:35:10,432
Jack.

183
00:35:18,100 --> 00:35:19,557
Jack...

184
00:35:22,725 --> 00:35:26,386
You're still alive, little one.

185
00:35:33,267 --> 00:35:37,640
I have a new place for you,
somewhere dad never goes.

186
00:35:37,850 --> 00:35:42,389
But you have to be quiet
or dad will kill you, and me too.

187
00:35:43,517 --> 00:35:45,424
So, quiet. Yes?

188
00:36:00,308 --> 00:36:01,718
You must be hungry.

189
00:36:12,683 --> 00:36:15,847
This is your new home.
Nice, isn't it?

190
00:36:23,683 --> 00:36:25,141
Look at this.

191
00:37:38,183 --> 00:37:42,888
Are you looking forward
to your birthday? I'm not telling you.

192
00:37:43,058 --> 00:37:48,130
All right, it's black and it flaps about,
but that's all I'm saying.

193
00:37:54,725 --> 00:37:58,422
Yes, it's great fun.
I'll make you a cake.

194
00:38:01,808 --> 00:38:04,759
Daddy hasn't been angry, lately.

195
00:38:08,933 --> 00:38:10,509
No, he's really kind.

196
00:38:11,767 --> 00:38:15,132
Love you too, mum. Kisses, bye.

197
00:38:23,183 --> 00:38:28,848
Look, first you look to the right
and then to the left.

198
00:38:34,017 --> 00:38:39,883
First left, then to the right
and then to the left again.

199
00:38:40,100 --> 00:38:45,765
So, left, right and left again.

200
00:38:46,975 --> 00:38:49,807
Did you get that, little one?
Are you afraid?

201
00:38:56,017 --> 00:38:58,683
Look, a dog. They're very dangerous.

202
00:39:32,517 --> 00:39:35,717
Hello dad, how was work?

203
00:39:35,850 --> 00:39:38,421
I was called up for nothing.
Excuse me.

204
00:39:38,600 --> 00:39:41,764
Shall I bring you a beer
so you can have a rest?

205
00:39:43,017 --> 00:39:44,426
Well, well.

206
00:40:14,100 --> 00:40:18,390
Thank you, what's going on?
Nothing.

207
00:40:45,308 --> 00:40:50,131
Jackdaws live in colonies.
The bravest male is the leader.

208
00:40:51,517 --> 00:40:56,506
He gets the prettiest female
and the best spot to build a nest.

209
00:40:57,933 --> 00:41:02,425
But he has to do something
to get the best spot.

210
00:41:04,683 --> 00:41:11,640
He has to be brave, he's the least
frightened jackdaw, he's tough.

211
00:41:11,850 --> 00:41:16,886
If there is danger,
he's the first one to get in there.

212
00:42:05,517 --> 00:42:07,175
Too bad.

213
00:42:19,267 --> 00:42:20,973
Too bad.

214
00:42:22,642 --> 00:42:27,216
Can you hit it or not?
We'll find out soon enough.

215
00:42:29,892 --> 00:42:32,215
- Would you like to try?
- Sure.

216
00:42:32,433 --> 00:42:35,550
I'm a better marksman than you, anyway.

217
00:42:40,517 --> 00:42:42,424
The butt against your shoulder.

218
00:42:46,683 --> 00:42:48,259
Hold the barrel high.

219
00:42:55,600 --> 00:42:58,681
That was a good one.
Put it back up.

220
00:43:04,058 --> 00:43:05,765
It's got a hole in it.

221
00:43:13,058 --> 00:43:16,140
Are we going to bake a cake?
Don't be ridiculous.

222
00:43:16,308 --> 00:43:17,884
Why not?

223
00:43:18,767 --> 00:43:23,305
Don't even think about it.
I asked you why not?

224
00:43:25,392 --> 00:43:29,717
She isn't here to celebrate her birthday.
A cake won't matter.

225
00:43:32,517 --> 00:43:34,886
Did you hear what I said?

226
00:44:10,350 --> 00:44:13,052
We are going to celebrate it.

227
00:44:33,267 --> 00:44:37,805
Not much use to us, is it?

228
00:44:38,225 --> 00:44:39,801
I'll use washing-up liquid.

229
00:44:44,892 --> 00:44:46,384
Nice and warm.

230
00:44:52,350 --> 00:44:56,011
Look, those are the clothes
that you crapped all over.

231
00:44:56,725 --> 00:44:58,680
And daddy's shirt.

232
00:45:01,100 --> 00:45:02,723
My sock.

233
00:45:09,183 --> 00:45:11,340
Look, here is your little bench.

234
00:45:15,100 --> 00:45:17,256
Look, a light.

235
00:45:39,142 --> 00:45:42,756
When I'm not here
there is something to remember me by.

236
00:45:42,933 --> 00:45:45,766
Jojo, damn it.

237
00:45:51,017 --> 00:45:52,758
Come here, now.

238
00:45:56,142 --> 00:45:57,469
Come here.

239
00:46:03,058 --> 00:46:05,262
I only wanted to wash the clothes.

240
00:46:06,600 --> 00:46:09,432
You only wanted to wash the clothes?

241
00:46:13,725 --> 00:46:16,758
Look at this, come here.

242
00:46:17,558 --> 00:46:22,132
Look at it. Damn it, come here.

243
00:46:24,475 --> 00:46:28,220
What's this then?
Now look at what you've done.

244
00:46:30,475 --> 00:46:32,715
Look at me.

245
00:46:32,933 --> 00:46:37,258
It's ruined, isn't it? All of it.
It's all ruined.

246
00:46:37,433 --> 00:46:41,297
Look at me. What were you thinking?

247
00:46:41,433 --> 00:46:44,515
I wanted to wash the clothes.
And that made you cry?

248
00:46:44,683 --> 00:46:46,639
Do it yourself then.

249
00:47:48,267 --> 00:47:53,386
Stop it, Jojo.
I said: Knock it off.

250
00:47:54,725 --> 00:47:57,806
Look at me. Look at me, I said.

251
00:48:01,642 --> 00:48:05,588
You're coming with me.
Did you hear me? Come on.

252
00:48:08,308 --> 00:48:10,761
You're coming with me.
Come on, lad.

253
00:48:20,433 --> 00:48:22,839
Are you going to tell us what's wrong?

254
00:48:24,767 --> 00:48:27,551
Why were you so angry
with those lockers?

255
00:48:35,392 --> 00:48:39,468
Answer the question.
I don't know why.

256
00:49:07,892 --> 00:49:09,929
Jojo, I...

257
00:49:12,267 --> 00:49:13,807
About yesterday.

258
00:49:17,683 --> 00:49:19,057
I...

259
00:49:54,433 --> 00:49:56,590
You will come back, won't you?

260
00:50:02,850 --> 00:50:04,592
Jack, you can fly.

261
00:51:18,433 --> 00:51:22,676
I taught Jack to fly.
I was sitting in the garden, like this.

262
00:51:22,850 --> 00:51:29,463
Jack was on the shed and when I
called him, he flew straight at me.

263
00:51:29,642 --> 00:51:31,632
- Really?
- Yes.

264
00:51:37,642 --> 00:51:40,842
Can I help you?
Of course you can.

265
00:51:41,017 --> 00:51:43,552
Jojo, Yenthe, get in and swim.

266
00:52:18,267 --> 00:52:19,759
Thank you.

267
00:52:24,808 --> 00:52:26,218
Look.

268
00:52:27,975 --> 00:52:34,766
First, there is nothing.
Nothing at all, it is all black.

269
00:52:35,808 --> 00:52:37,182
And then...

270
00:52:39,433 --> 00:52:41,389
- life came.
- Cool.

271
00:52:46,433 --> 00:52:49,799
And then there is nothing again.
Nothing at all.

272
00:53:01,100 --> 00:53:02,806
Cheers.

273
00:53:10,392 --> 00:53:13,887
- Dad?
- Yes, Jojo?

274
00:53:14,100 --> 00:53:17,217
Can I bake a small cake, please?

275
00:53:24,642 --> 00:53:26,549
Just forget about it.

276
00:53:28,058 --> 00:53:29,385
But dad...

277
00:55:03,642 --> 00:55:04,969
Dad?

278
00:55:11,808 --> 00:55:13,301
Dad?

279
00:56:26,642 --> 00:56:32,508
Jackdaws spend every minute together,
and do hardly a step apart.

280
00:56:32,725 --> 00:56:36,968
In flight they soar
gracefully side by side.

281
00:56:37,183 --> 00:56:40,300
In the fields they sit on a bunch.

282
00:56:40,642 --> 00:56:44,007
In the tree they brush
each other's feathers.

283
00:56:44,933 --> 00:56:51,546
The male gives her any delicacy
he finds, and she accepts it lovingly.

284
00:56:51,808 --> 00:56:56,596
Even if one is ill, the other stays.

285
00:57:05,517 --> 00:57:07,258
Look, I can fly too, Jack.

286
00:57:12,975 --> 00:57:15,049
Come and fly.

287
00:57:22,600 --> 00:57:24,840
- Close your eyes.
- Why?

288
00:57:25,017 --> 00:57:27,968
Because. Just do it.

289
00:57:33,433 --> 00:57:35,839
- Are they closed?
- Yes.

290
00:57:43,058 --> 00:57:44,599
You can open them now.

291
00:57:49,350 --> 00:57:50,724
Nice.

292
00:57:54,808 --> 00:57:56,431
No, you can keep it.

293
00:59:35,600 --> 00:59:37,472
I know about your mum.

294
00:59:39,767 --> 00:59:41,094
What?

295
00:59:43,975 --> 00:59:45,882
That she's dead.

296
01:00:31,392 --> 01:00:32,766
Hello mum.

297
01:04:26,767 --> 01:04:28,757
Jojo, stop it.

298
01:04:41,892 --> 01:04:43,219
She's dead, Jojo.

299
01:05:09,475 --> 01:05:10,884
Stop it.

300
01:05:27,808 --> 01:05:29,515
Dad, don't.

301
01:05:38,225 --> 01:05:40,892
Jojo, get lost.

302
01:05:56,933 --> 01:05:58,260
Daddy.

303
01:06:01,142 --> 01:06:03,808
Dad, let go of him.

304
01:06:09,558 --> 01:06:11,715
- Sod off.
- Chew.

305
01:06:42,892 --> 01:06:47,016
Come back, Jack.

306
01:15:04,392 --> 01:15:05,801
Come here.

307
01:15:09,767 --> 01:15:11,224
I'm sorry...

308
01:15:18,267 --> 01:15:19,510
I'm sorry.

309
01:16:05,100 --> 01:16:07,221
What shall I play for him?

310
01:16:07,850 --> 01:16:09,640
Mummy's song.

311
01:17:09,183 --> 01:17:11,850
You can stroke him if you want.

312
01:18:11,392 --> 01:18:14,176
Should I say something,
like at mum's funeral?

313
01:18:14,350 --> 01:18:15,677
Only if you want to.

314
01:18:19,433 --> 01:18:21,803
It's a real pity.

315
01:20:08,475 --> 01:20:13,381
First, there was nothing.
Nothing at all.

316
01:20:13,558 --> 01:20:18,346
And then?
And then? There was a...

317
01:20:20,975 --> 01:20:26,889
flame. A very large flame.