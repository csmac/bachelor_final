﻿1
00:00:01,000 --> 00:00:04,074
Subtitles downloaded from www.OpenSubtitles.org

2
00:03:01,400 --> 00:03:03,448
Come here, Quero!

3
00:03:12,280 --> 00:03:14,328
Marquitos.

4
00:03:14,400 --> 00:03:15,958
Let's go to swim in the creek.

5
00:03:16,000 --> 00:03:18,048
Come, goat!

6
00:03:23,840 --> 00:03:25,888
Come, all!

7
00:04:05,880 --> 00:04:08,269
Yesterday, the witch went too far.

8
00:04:08,320 --> 00:04:10,368
It isn't painful any longer.

9
00:04:14,120 --> 00:04:16,509
Jump! it's cold but good.

10
00:04:16,520 --> 00:04:20,616
Come on, jump! You haven't touched
water during the winter.

11
00:04:20,640 --> 00:04:22,688
So cold!

12
00:04:26,480 --> 00:04:28,528
It isn't cold anymore.

13
00:04:39,920 --> 00:04:41,968
Come on.

14
00:05:00,440 --> 00:05:02,488
Do you think she is looking us?

15
00:05:02,520 --> 00:05:03,305
Who?

16
00:05:03,400 --> 00:05:04,116
Mother.

17
00:05:04,200 --> 00:05:06,088
Of course, she sees
everything from up there.

18
00:05:06,120 --> 00:05:08,190
How do you know she is up there?

19
00:05:08,240 --> 00:05:10,504
Father Antonio told me.

20
00:05:10,560 --> 00:05:12,858
Who behaves properly on Earth...

21
00:05:12,920 --> 00:05:14,057
... goes direct to heaven.

22
00:05:14,120 --> 00:05:15,701
Then ...

23
00:05:15,760 --> 00:05:17,808
The witch has it complicated.

24
00:05:59,520 --> 00:06:01,568
Oh, the goats!

25
00:06:09,640 --> 00:06:11,688
Eah!

26
00:06:16,000 --> 00:06:18,048
Quero, Quero!

27
00:06:18,080 --> 00:06:19,570
Boy, come here!

28
00:06:19,640 --> 00:06:21,688
Quero, come here!

29
00:06:50,680 --> 00:06:52,728
Benito!

30
00:06:58,400 --> 00:07:00,448
Back!

31
00:07:06,800 --> 00:07:08,848
Go away, then!

32
00:07:11,080 --> 00:07:13,128
Don't be far from me, Quero.

33
00:07:13,160 --> 00:07:15,208
Release him!

34
00:07:17,440 --> 00:07:19,488
Go away!

35
00:07:21,080 --> 00:07:23,128
Away, away!

36
00:07:32,680 --> 00:07:34,728
Are you OK?

37
00:07:35,840 --> 00:07:37,888
And Lucero?

38
00:07:39,400 --> 00:07:41,448
Let's go home.

39
00:08:03,720 --> 00:08:05,768
What the hell did it happen?

40
00:08:06,120 --> 00:08:08,168
Coming here ...

41
00:08:08,240 --> 00:08:11,186
... wolves have attacked
the goats, and ...

42
00:08:11,240 --> 00:08:12,923
and killed five of them.

43
00:08:12,960 --> 00:08:13,858
Five?

44
00:08:13,960 --> 00:08:16,008
Damn Satan!

45
00:08:16,760 --> 00:08:18,808
Where did you leave them?

46
00:08:18,880 --> 00:08:20,780
What are we going to eat now,
the dog?

47
00:08:20,840 --> 00:08:22,569
We could do nothing.

48
00:08:22,640 --> 00:08:24,164
Don't tell me stories.

49
00:08:24,240 --> 00:08:27,268
I have two useless boys who
cannot even take care of goats.

50
00:08:27,360 --> 00:08:28,748
On top, you woke the girl up.

51
00:08:28,840 --> 00:08:30,694
Get lost!

52
00:08:30,760 --> 00:08:32,899
I don't want to see you again!

53
00:08:32,920 --> 00:08:34,103
Away!

54
00:08:34,200 --> 00:08:36,248
Don't hit me!

55
00:09:42,640 --> 00:09:44,688
Father is coming.

56
00:10:04,360 --> 00:10:06,408
Where are the kids?

57
00:10:06,440 --> 00:10:08,761
They should be in hell.

58
00:10:16,800 --> 00:10:19,189
Wolves have killed five goats.

59
00:10:19,920 --> 00:10:21,968
Damn it!

60
00:10:23,680 --> 00:10:26,410
If we don't pay them
to the Patroon ...

61
00:10:26,480 --> 00:10:27,799
... we are going away from here.

62
00:10:27,840 --> 00:10:28,920
And where are we going ...

63
00:10:29,000 --> 00:10:31,560
... if we have no place to die?

64
00:10:31,600 --> 00:10:33,522
Look, Juan ...

65
00:10:33,600 --> 00:10:37,197
... Get the money from somewhere,
because I don't want to leave.

66
00:10:37,280 --> 00:10:39,328
I can not have more debt.

67
00:10:39,400 --> 00:10:42,608
But are you blind?,
we have nothing to eat!

68
00:10:42,640 --> 00:10:44,892
And what the hell can I do?

69
00:10:45,040 --> 00:10:47,088
Take them away ...

70
00:10:47,960 --> 00:10:50,008
they became almost adults.

71
00:10:50,080 --> 00:10:52,264
It is clear that they are not yours!

72
00:11:16,240 --> 00:11:18,288
Boy.

73
00:11:18,360 --> 00:11:19,736
Prepare your bag, we are leaving.

74
00:11:19,750 --> 00:11:20,450
Where?

75
00:11:20,500 --> 00:11:21,200
To Patroon's home.

76
00:11:21,220 --> 00:11:21,920
For what?

77
00:11:22,060 --> 00:11:24,008
Stop asking and hurry up.

78
00:11:26,880 --> 00:11:29,474
OK, but Juanillo comes with me.

79
00:11:30,360 --> 00:11:32,408
No way. He stays.

80
00:11:32,480 --> 00:11:33,970
Without Juanillo, I am not going anywhere.

81
00:11:34,040 --> 00:11:36,099
You are leaving now, boy.

82
00:11:36,160 --> 00:11:38,958
So get ready NOW.

83
00:11:47,680 --> 00:11:50,615
Please, Juanillo,
don't let them to take me away

84
00:11:50,640 --> 00:11:52,688
Come here.

85
00:11:54,120 --> 00:11:56,168
Come on, boy, get ready.

86
00:11:57,400 --> 00:11:59,448
Damn satan!

87
00:12:01,240 --> 00:12:03,288
Stay away from him now.

88
00:12:03,360 --> 00:12:05,180
NO! I don't want to go without Juanillo!

89
00:12:05,240 --> 00:12:06,150
Let him, asshole!

90
00:12:06,200 --> 00:12:06,900
Don't touch me!

91
00:12:06,960 --> 00:12:09,281
Boy, I don't have the whole day.

92
00:12:10,120 --> 00:12:12,168
Marquitos!

93
00:12:12,240 --> 00:12:13,377
Father, let me go!

94
00:12:13,440 --> 00:12:14,140
Stay still now!

95
00:12:14,200 --> 00:12:14,900
Let me go!

96
00:12:14,920 --> 00:12:16,421
Still, baby!

97
00:12:16,480 --> 00:12:18,528
Juanillo!

98
00:12:21,200 --> 00:12:23,248
Come here!

99
00:12:24,240 --> 00:12:26,288
Marquitos!

100
00:12:26,320 --> 00:12:27,833
Juanillo, run!

101
00:12:27,920 --> 00:12:29,956
Father, Father!

102
00:12:30,000 --> 00:12:31,524
Wait!

103
00:12:31,600 --> 00:12:33,033
Don't stop!

104
00:12:33,080 --> 00:12:35,128
Marquitos!

105
00:12:37,160 --> 00:12:39,208
Run, run!

106
00:12:40,880 --> 00:12:42,928
Marquitos!

107
00:13:25,440 --> 00:13:27,488
Come in.

108
00:13:30,760 --> 00:13:33,149
May I ask your permission,
Don Honesto?

109
00:13:33,160 --> 00:13:35,208
What do you want?

110
00:13:36,040 --> 00:13:38,634
I am bringing you my son.

111
00:13:39,640 --> 00:13:41,688
What is your name, boy?

112
00:13:41,760 --> 00:13:42,875
Marquitos.

113
00:13:42,960 --> 00:13:46,236
I've been informed that you
are skilled with goats.

114
00:13:52,480 --> 00:13:54,528
He is very good indeed.

115
00:13:54,560 --> 00:13:56,039
He had grown up among them.

116
00:13:56,120 --> 00:13:58,168
Ceferino.

117
00:13:59,080 --> 00:14:01,128
Ceferino!

118
00:14:05,600 --> 00:14:07,648
Yes, Don Honesto.

119
00:14:07,680 --> 00:14:11,844
Tomorrow morning take this kid
to the old man's place.

120
00:14:12,320 --> 00:14:14,368
As you command, my lord.

121
00:14:14,400 --> 00:14:17,039
Careful man! you are going
to scratch my boots.

122
00:14:17,080 --> 00:14:18,172
Sorry.

123
00:14:18,240 --> 00:14:20,674
Nothing more, Ceferino,
and get my horse ready.

124
00:14:20,720 --> 00:14:22,517
At your feet, Don Honesto.

125
00:14:22,560 --> 00:14:24,608
Then ...

126
00:14:24,680 --> 00:14:27,990
... the issue with the goats
was solved, Don Honesto?

127
00:14:28,000 --> 00:14:30,048
By this time, it is OK.

128
00:14:30,400 --> 00:14:32,789
But the next one you are
going to the streets.

129
00:14:32,800 --> 00:14:34,643
Is it clear?

130
00:14:34,720 --> 00:14:35,607
Yes, my lord.

131
00:14:35,680 --> 00:14:38,205
Lately, wolves have been
very active, and ...

132
00:14:38,240 --> 00:14:40,185
Stop talking of wolves, Pig-mouth ...

133
00:14:40,240 --> 00:14:43,994
... i am doing more than enough alouding
you to use the house and the horse.

134
00:14:44,760 --> 00:14:47,968
God keeps you for many years.

135
00:14:49,120 --> 00:14:51,441
And you already know:
Here I am ...

136
00:14:51,480 --> 00:14:53,528
... to serve you.

137
00:15:04,200 --> 00:15:05,224
Father.

138
00:15:06,320 --> 00:15:08,368
Where are you going?

139
00:15:10,640 --> 00:15:12,176
Home.

140
00:15:12,240 --> 00:15:14,288
And me?

141
00:15:47,760 --> 00:15:49,808
Come, jaca, come.

142
00:16:04,560 --> 00:16:06,608
Doroteo!

143
00:16:08,960 --> 00:16:11,076
Let's go closer.

144
00:16:11,320 --> 00:16:13,368
Come on.

145
00:16:19,920 --> 00:16:21,968
Come on.

146
00:16:29,400 --> 00:16:31,448
Doroteo!

147
00:16:36,280 --> 00:16:38,328
But, Doroteo, animal!

148
00:16:38,680 --> 00:16:40,728
What the hell have you done?

149
00:16:40,760 --> 00:16:42,728
Nothing, Don Ceferino.

150
00:16:42,800 --> 00:16:45,689
Venancia was about to deliver, and ...

151
00:16:45,720 --> 00:16:47,950
... I had to receive the baby.

152
00:16:48,000 --> 00:16:49,627
And?

153
00:16:49,680 --> 00:16:52,069
Well ... we are with no money.

154
00:16:52,120 --> 00:16:56,011
Don't complain,
there are people worse than you.

155
00:16:56,040 --> 00:16:58,088
One thing, don Ceferino:

156
00:16:58,160 --> 00:17:00,788
If you don't mind,
before you go ...

157
00:17:00,800 --> 00:17:02,939
... if you can leave me
some wheat breadcrumbs ...

158
00:17:03,000 --> 00:17:04,900
Don't bother me, Doroteo.

159
00:17:04,960 --> 00:17:07,463
What you have to do...
is fuck a little less ...

160
00:17:07,520 --> 00:17:09,636
... and work a lot more.

161
00:17:10,440 --> 00:17:12,488
Did u get it?

162
00:17:14,680 --> 00:17:16,728
Let's go.

163
00:17:27,200 --> 00:17:29,248
Pizquilla, leave him.

164
00:17:31,120 --> 00:17:34,806
Hopefully, they will be eaten by wolves
in those lonely mountains.

165
00:17:37,160 --> 00:17:39,208
Come on! What do you expect?

166
00:18:14,160 --> 00:18:16,208
Good evening, Don Ceferino.

167
00:18:16,240 --> 00:18:17,434
Good evening, with God.

168
00:18:17,520 --> 00:18:19,101
Why are you so late around here,
my sergeant?

169
00:18:19,160 --> 00:18:21,822
Some pigglets have been stolen,
at Don Martin's farm ...

170
00:18:21,840 --> 00:18:23,205
... And Balilla is the main suspect.

171
00:18:23,280 --> 00:18:25,328
Have you heard anything?

172
00:18:25,400 --> 00:18:28,028
I don't know, but no worries,
if I hear something ...

173
00:18:28,040 --> 00:18:29,041
... I will tell you immedialty.

174
00:18:29,120 --> 00:18:30,838
Perhaps you know that my brother ...

175
00:18:30,920 --> 00:18:32,046
... wanted to finish that asshole?

176
00:18:32,120 --> 00:18:34,168
Besides, Don Martin ...

177
00:18:34,200 --> 00:18:36,134
... is offering a good reward.

178
00:18:36,200 --> 00:18:37,030
How much?

179
00:18:37,120 --> 00:18:39,168
Around 20,000 pesetas.

180
00:18:39,800 --> 00:18:41,848
20,000 pesetas ...

181
00:18:43,520 --> 00:18:45,568
Let's go.

182
00:18:45,600 --> 00:18:47,989
Have a good night and a good service.

183
00:18:48,040 --> 00:18:50,088
With God.

184
00:18:59,240 --> 00:19:01,970
Balilla has become such an asshole.

185
00:19:04,440 --> 00:19:06,488
Have you heard about him?

186
00:19:08,800 --> 00:19:10,848
A bandit.

187
00:19:11,120 --> 00:19:15,147
The biggest motherf... around Despenia-perros.

188
00:19:15,440 --> 00:19:18,785
By the war's end,
he escaped into the bush ...

189
00:19:18,800 --> 00:19:21,872
... and since then he lives
robbing everyone.

190
00:19:21,880 --> 00:19:23,996
Pray god in order to avoid him.

191
00:19:53,960 --> 00:19:56,008
Look, boy.

192
00:19:56,080 --> 00:19:57,957
The Silence Valley.

193
00:19:58,000 --> 00:20:00,867
There are people who
got eggs to live there.

194
00:20:04,200 --> 00:20:06,248
Let's go down.

195
00:20:20,200 --> 00:20:22,248
Come on!

196
00:20:53,720 --> 00:20:55,768
Good afternoon, Don Atanasio.

197
00:21:08,040 --> 00:21:11,453
Life in the countryside keeps you well.

198
00:21:12,720 --> 00:21:15,860
Do you still have the same
bloody mood than ever, eh?

199
00:21:25,280 --> 00:21:27,908
Here I leave you a little of wheat,
to hold the summer.

200
00:21:36,800 --> 00:21:39,940
Eah, I will be back this fall
to take the goats.

201
00:21:44,520 --> 00:21:46,568
And the other sacks?

202
00:21:46,640 --> 00:21:50,804
If you tell me where Balilla hides,
I will give you all of them.

203
00:21:52,760 --> 00:21:54,808
With God.

204
00:21:58,560 --> 00:22:00,608
Ah, and take car of this kid ...

205
00:22:00,640 --> 00:22:04,531
... I brought him
to help you with the goats.

206
00:22:27,720 --> 00:22:29,768
Hang it around your neck.

207
00:22:30,760 --> 00:22:33,354
As many goats cross the fence ...

208
00:22:33,400 --> 00:22:36,119
... many beans you put inside the can.

209
00:22:36,160 --> 00:22:38,208
Understood?

210
00:22:38,320 --> 00:22:40,368
If you get more beans ...

211
00:22:40,400 --> 00:22:41,970
...  you are missing goats.

212
00:22:42,040 --> 00:22:44,065
You have to go and find them.

213
00:22:44,120 --> 00:22:46,168
Yes, sir.

214
00:22:57,760 --> 00:22:59,808
Come inside!

215
00:23:30,800 --> 00:23:32,848
Sir, all are here!

216
00:23:38,120 --> 00:23:40,168
Sir?

217
00:24:29,960 --> 00:24:32,008
Go away!

218
00:24:33,800 --> 00:24:35,848
Away!

219
00:25:36,520 --> 00:25:38,568
Who is there?

220
00:26:03,320 --> 00:26:05,368
You sleep there.

221
00:28:55,920 --> 00:28:57,968
Goat!

222
00:30:12,600 --> 00:30:14,648
Come, miner!

223
00:30:44,920 --> 00:30:46,968
Got you!

224
00:30:49,960 --> 00:30:52,008
Come here, miner.

225
00:31:58,520 --> 00:32:00,568
Boy!

226
00:32:01,960 --> 00:32:04,008
Herd them to the corral.

227
00:32:08,480 --> 00:32:10,528
Hey, goat, don't escape!

228
00:32:25,240 --> 00:32:27,288
Be calm, Lunara.

229
00:32:27,960 --> 00:32:30,008
Baby, come here, to here!

230
00:32:44,320 --> 00:32:46,368
Has it been bitten by a snake?

231
00:32:46,800 --> 00:32:48,848
No, it has a broken leg.

232
00:32:48,920 --> 00:32:50,899
Bring me some sticks ...

233
00:32:50,920 --> 00:32:54,060
... a liitle of Gum Rockrose
and a bit of flax-leaved daphne.

234
00:32:55,880 --> 00:32:58,132
Calm down, Lunara ...

235
00:32:58,520 --> 00:33:02,274
... for the cork oak's debarking
you are going to be jumping already.

236
00:33:03,560 --> 00:33:05,608
Go, Lunara, Lunara...

237
00:33:05,640 --> 00:33:07,688
... be calm.

238
00:33:11,120 --> 00:33:13,168
Hold her from there.

239
00:33:18,720 --> 00:33:21,041
Gum Rockrose is good for hits.

240
00:33:29,400 --> 00:33:31,448
Go, take her to the cave.

241
00:33:52,800 --> 00:33:55,052
Go and bring a little of Rosemary.

242
00:34:42,280 --> 00:34:44,328
Sir, a wolf!

243
00:34:44,400 --> 00:34:46,231
Where?

244
00:34:46,280 --> 00:34:48,328
Out there.

245
00:34:48,600 --> 00:34:50,648
I was eating the rabbit ...

246
00:34:52,080 --> 00:34:54,537
... then the wolf came,
and stole it from my hands.

247
00:34:54,960 --> 00:34:57,008
Lucky enough ...

248
00:34:57,120 --> 00:34:59,168
... that you had the rabbit.

249
00:34:59,240 --> 00:35:01,288
Otherwise, it will get you instead.

250
00:35:01,480 --> 00:35:03,596
You have to be really carefull ...

251
00:35:04,040 --> 00:35:06,088
... with lonely wolves.

252
00:35:20,960 --> 00:35:23,008
Go there.

253
00:35:39,680 --> 00:35:41,728
Now.

254
00:35:41,800 --> 00:35:43,848
Throw it.

255
00:35:50,320 --> 00:35:52,368
Be still there.

256
00:35:53,240 --> 00:35:55,288
Come here.

257
00:35:55,680 --> 00:35:57,728
But never loose its face.

258
00:36:05,920 --> 00:36:07,968
Now you know ...

259
00:36:08,160 --> 00:36:10,754
... how do you gain its trust on you.

260
00:36:12,560 --> 00:36:14,608
Before they attack you...

261
00:36:14,960 --> 00:36:17,008
... the move very fast around you ...

262
00:36:17,360 --> 00:36:19,408
... here, next to you...

263
00:36:19,680 --> 00:36:21,728
... several times ...

264
00:36:21,760 --> 00:36:23,808
...and rub you ...

265
00:36:24,360 --> 00:36:26,408
... in order to make you panic.

266
00:36:27,760 --> 00:36:29,808
And when you start running ...

267
00:36:30,480 --> 00:36:32,528
... they jump to your neck ...

268
00:36:32,560 --> 00:36:34,334
... and it's over.

269
00:36:34,400 --> 00:36:36,721
Did they ever do anything to you?

270
00:36:36,760 --> 00:36:38,876
When I just came the first time ...

271
00:36:40,480 --> 00:36:43,074
... they killed some goats.

272
00:36:43,880 --> 00:36:45,928
But then ...

273
00:36:46,000 --> 00:36:48,798
... I was building their trust,
and now ...

274
00:36:48,840 --> 00:36:50,888
... they respect me.

275
00:36:51,080 --> 00:36:53,128
How did you do it?

276
00:36:53,200 --> 00:36:55,794
Using the same methods I showed you today:

277
00:36:56,200 --> 00:36:58,521
... giving them food little by little.

278
00:37:57,200 --> 00:37:59,248
Place it well.

279
00:38:02,040 --> 00:38:04,088
And you put him inside the hole.

280
00:38:07,840 --> 00:38:09,888
More! There.

281
00:38:48,920 --> 00:38:50,968
The reason is because here ...

282
00:38:51,000 --> 00:38:52,752
... the thrushes land ...

283
00:38:52,840 --> 00:38:54,888
... in the horsehairs.

284
00:39:15,000 --> 00:39:17,389
The lonely wolf
is trying him.

285
00:39:34,120 --> 00:39:36,168
It was a very strange day ...

286
00:39:36,200 --> 00:39:38,452
... when she didn't hit me.

287
00:39:38,920 --> 00:39:40,968
She never loved me.

288
00:39:43,680 --> 00:39:45,728
Neither my brother.

289
00:39:46,840 --> 00:39:48,888
And Dad ...

290
00:39:50,560 --> 00:39:52,608
... never could manage it.

291
00:39:57,240 --> 00:39:59,492
But here with you I am very happy.

292
00:40:07,680 --> 00:40:09,728
Who is there?

293
00:40:13,960 --> 00:40:16,008
There is no danger.

294
00:40:17,000 --> 00:40:19,048
Balilla is here.

295
00:40:24,680 --> 00:40:26,728
Pablito, you have to stay vigilant.

296
00:40:30,560 --> 00:40:32,608
Well, it is as usual:

297
00:40:32,680 --> 00:40:35,820
... Someone stole in Don Samuel's farm ...

298
00:40:35,840 --> 00:40:36,886
... and they killed the guard.

299
00:40:36,960 --> 00:40:41,545
<i>All birds eat wheat,
but the guilty one is always the sparrow...</i>

300
00:40:41,560 --> 00:40:43,608
... shameless people.

301
00:40:43,640 --> 00:40:46,302
All the fucking life
fighting in the name of Justice ...

302
00:40:46,360 --> 00:40:47,850
... and what for?

303
00:40:47,880 --> 00:40:49,780
To end like this?

304
00:40:49,840 --> 00:40:51,853
We became fugitives.

305
00:40:51,920 --> 00:40:54,172
Four days without sleep.

306
00:40:54,440 --> 00:40:56,488
Yesterday Doroteo told us ...

307
00:40:56,520 --> 00:40:59,455
... that there are fifty
Civil Guards looking for us.

308
00:40:59,880 --> 00:41:01,928
Now, I understand that asshole.

309
00:41:02,000 --> 00:41:03,809
Which one?

310
00:41:03,840 --> 00:41:05,569
Ceferino ...

311
00:41:05,640 --> 00:41:07,744
... he had to give me
five sacks of wheat.

312
00:41:07,800 --> 00:41:09,848
And guess what he told me?

313
00:41:10,080 --> 00:41:12,332
That he give me them
only if I tell him...

314
00:41:12,400 --> 00:41:13,765
... where were you hidding.

315
00:41:13,800 --> 00:41:15,848
Bastard!

316
00:41:16,160 --> 00:41:18,276
He really hates me!

317
00:41:18,320 --> 00:41:20,185
Since the Contrapartida
killed his brother...

318
00:41:20,240 --> 00:41:23,858
... he has been blind
and blames us for it.

319
00:41:24,440 --> 00:41:27,238
When I was coming here
with Don Ceferino ...

320
00:41:27,960 --> 00:41:30,895
... we met with some Civil Guards.

321
00:41:31,480 --> 00:41:33,528
They were looking for you.

322
00:41:33,560 --> 00:41:37,451
They say that you stole
some pigglets from Don Martin's farm.

323
00:41:38,520 --> 00:41:40,568
Damn it!

324
00:41:41,520 --> 00:41:43,772
Then thay asked Don Ceferino ...

325
00:41:44,000 --> 00:41:46,048
... if he had information about your location.

326
00:41:46,080 --> 00:41:47,149
And?

327
00:41:47,240 --> 00:41:49,424
They also told us about a reward ...

328
00:41:49,480 --> 00:41:51,528
... for information.

329
00:41:51,800 --> 00:41:54,803
I will stick my knife
very deep on him.

330
00:41:54,920 --> 00:41:56,968
Let's go.

331
00:41:57,000 --> 00:41:59,616
Stay a little bit longer,
to rest well, Balilla.

332
00:41:59,640 --> 00:42:02,757
No. The civil guards
can show up at any time.

333
00:42:02,800 --> 00:42:04,848
Take it.

334
00:42:04,920 --> 00:42:06,968
Mechote, take this with you.

335
00:42:09,000 --> 00:42:11,048
Go carefully.

336
00:42:25,760 --> 00:42:27,808
Atanasio.

337
00:42:28,360 --> 00:42:30,408
And you?

338
00:42:30,480 --> 00:42:31,180
Me what?

339
00:42:31,260 --> 00:42:33,208
Why do you live alone?

340
00:42:33,560 --> 00:42:35,608
Don't you have family?

341
00:42:35,640 --> 00:42:37,688
Not many

342
00:42:38,280 --> 00:42:40,328
My wife and my childen ...

343
00:42:41,040 --> 00:42:43,088
... were killed during the war.

344
00:42:43,120 --> 00:42:45,850
I was in the bush
with the goats ...

345
00:42:47,000 --> 00:42:49,116
... and suddenly, I heard a bomb blast.

346
00:42:49,360 --> 00:42:52,022
I knew something wrong
had happen ...

347
00:42:52,080 --> 00:42:54,605
... so I ran towards my town.

348
00:42:55,480 --> 00:42:58,483
When finally I arrived,
I wanted to die.

349
00:43:00,840 --> 00:43:02,888
Only my Tomas survived ...

350
00:43:02,960 --> 00:43:04,700
... the eldest ...

351
00:43:04,760 --> 00:43:07,558
... who went out
to fight in the streets.

352
00:43:09,240 --> 00:43:11,288
I lost everything.

353
00:43:11,840 --> 00:43:13,888
Everything the most loved.

354
00:43:16,360 --> 00:43:18,408
Then I came to this place.

355
00:43:20,240 --> 00:43:22,288
And here I am, with my sorrow...

356
00:43:22,960 --> 00:43:25,417
... that keep here inside.

357
00:43:42,600 --> 00:43:44,648
My god ...

358
00:43:44,840 --> 00:43:46,888
... they hunted them!

359
00:44:02,480 --> 00:44:04,528
Boy.

360
00:44:04,600 --> 00:44:07,125
Take the goats down .

361
00:44:08,040 --> 00:44:10,770
I am going to stay here,
I don't feel ...

362
00:44:10,880 --> 00:44:12,928
... well.

363
00:44:13,160 --> 00:44:15,208
Do you want a little bit of milk?

364
00:44:15,440 --> 00:44:17,488
No, no, don't bring me anything.

365
00:44:17,760 --> 00:44:19,808
I am a little apathetic.

366
00:44:19,840 --> 00:44:21,888
Well, see you tonight.

367
00:46:18,680 --> 00:46:20,728
Wolfy!

368
00:46:24,000 --> 00:46:26,048
Wolfy!

369
00:46:36,720 --> 00:46:38,768
Wolfy!

370
00:47:18,720 --> 00:47:20,768
Atanasio!

371
00:47:22,560 --> 00:47:24,608
I had such an experience ...

372
00:47:24,680 --> 00:47:26,728
... you won't believe it!

373
00:47:28,360 --> 00:47:30,408
You are too warm.

374
00:47:30,760 --> 00:47:32,808
Yes.

375
00:47:34,040 --> 00:47:37,589
Get the herb for cold,
and boil water with it. Go.

376
00:47:58,440 --> 00:48:00,488
Get well soon ...

377
00:48:00,560 --> 00:48:02,812
... I don't want to leave this place.

378
00:48:42,960 --> 00:48:45,076
Get lost, this is not for you!

379
00:50:04,000 --> 00:50:06,048
Wolfy!

380
00:50:23,760 --> 00:50:25,808
Atanasio!

381
00:50:35,880 --> 00:50:37,928
Atanasio!

382
00:50:39,480 --> 00:50:41,528
Atanasio!

383
00:51:06,120 --> 00:51:08,168
Sir ...

384
00:51:08,240 --> 00:51:10,538
... why did you come up here?

385
00:51:10,560 --> 00:51:12,608
I had too much heat ...

386
00:51:13,800 --> 00:51:16,735
... and here runs a
little bit of fresh air.

387
00:51:16,760 --> 00:51:18,808
Are you still in pain?

388
00:51:19,360 --> 00:51:21,408
Yes.

389
00:51:22,200 --> 00:51:24,248
Boy...

390
00:51:24,280 --> 00:51:26,328
... I am leaving.

391
00:51:26,400 --> 00:51:28,516
Where,
if you can not even walk?

392
00:51:31,280 --> 00:51:33,328
Far away.

393
00:51:34,560 --> 00:51:37,085
Where I want to go long ago.

394
00:51:37,160 --> 00:51:39,503
Then me too,
I am going with you.

395
00:51:39,520 --> 00:51:41,568
What for?

396
00:51:41,640 --> 00:51:43,688
To be with my mother.

397
00:51:49,280 --> 00:51:51,328
Listen:

398
00:51:53,240 --> 00:51:55,288
... If he has survived ...

399
00:51:56,160 --> 00:51:58,208
... and someday ...

400
00:51:58,240 --> 00:52:00,014
... you meet him ...

401
00:52:00,080 --> 00:52:02,128
Who?

402
00:52:02,480 --> 00:52:04,528
Balilla ...

403
00:52:06,640 --> 00:52:08,688
... don't panic ...

404
00:52:10,160 --> 00:52:12,208
... he is my son ...

405
00:52:12,240 --> 00:52:14,288
... my Tomas.

406
00:52:18,440 --> 00:52:20,488
Take this.

407
00:52:26,600 --> 00:52:28,648
It will protect you.

408
00:52:29,160 --> 00:52:31,208
And don't ever aloud ...

409
00:52:32,280 --> 00:52:34,328
... to extinguish the fire.

410
00:58:58,880 --> 00:59:00,928
Come, miner.

411
01:04:42,080 --> 01:04:44,128
Away!

412
01:04:48,640 --> 01:04:50,688
Away!

413
01:04:54,520 --> 01:04:56,568
Away!

414
01:09:36,960 --> 01:09:39,008
Atanasio!

415
01:09:49,600 --> 01:09:51,648
Atanasio!

416
01:09:51,720 --> 01:09:55,542
Where is the bloody old man?

417
01:09:58,880 --> 01:10:02,225
Caragorda, go up there,
and tell me if you see anything.

418
01:10:13,640 --> 01:10:16,985
Atanasio! damn it!
I don't have all day to wait!

419
01:10:19,160 --> 01:10:21,412
Why did you neglect the goats?

420
01:10:33,120 --> 01:10:35,509
Are you playing
hide-and-seek with me?

421
01:11:21,320 --> 01:11:23,368
Hey, boss!

422
01:11:23,440 --> 01:11:25,488
Come to see this.

423
01:11:39,520 --> 01:11:41,568
Damn it!

424
01:11:41,640 --> 01:11:43,892
I already knew that
you had little time.

425
01:11:44,160 --> 01:11:46,685
Let's go now,
do you realise there is no one?

426
01:11:46,720 --> 01:11:49,041
Damn it!
Take the goats out.

427
01:11:50,280 --> 01:11:52,669
You died saying nothing, eh?

428
01:12:43,080 --> 01:12:45,128
- Come here!
- No!

429
01:12:45,160 --> 01:12:47,742
Are you going to tell me
where is Balilla hidding, eh?

430
01:12:47,800 --> 01:12:49,279
- Don't touch me!
- Where is he hidding?

431
01:12:49,360 --> 01:12:50,804
- I don't know!
- Tell me or I will urge on you!

432
01:12:50,840 --> 01:12:52,694
The old man was helping him. Wasn't he?

433
01:12:52,760 --> 01:12:54,614
I know nothing.

434
01:12:54,680 --> 01:12:57,478
Damn your life,
you are going to tell it.

435
01:12:57,520 --> 01:12:59,499
Let me free!

436
01:12:59,560 --> 01:13:01,608
Let me free!

437
01:13:03,400 --> 01:13:05,448
Let me free!

438
01:14:48,800 --> 01:14:50,848
Damn you, boy!

439
01:14:53,720 --> 01:14:56,587
Let's see who
is going to eat who.

440
01:17:23,120 --> 01:17:25,168
I am here, stop shooting.

441
01:17:25,240 --> 01:17:27,367
Don Ceferino,
only one went away.

442
01:17:27,400 --> 01:17:30,062
Relax, that one has a fatal wound.

443
01:17:30,120 --> 01:17:32,509
Caragorda, check the horses.

444
01:17:32,520 --> 01:17:33,771
I am going there, boss.

445
01:17:33,840 --> 01:17:35,888
Come, Manuel.

446
01:17:45,320 --> 01:17:47,368
Hurry up.

447
01:18:05,560 --> 01:18:07,608
Let's go.

448
01:18:16,840 --> 01:18:18,888
Wolfy.

449
01:18:42,440 --> 01:18:44,488
Wolfy?

450
01:20:35,640 --> 01:20:37,688
Take, eat a little bit.

451
01:22:16,520 --> 01:22:18,568
Look what I brought you.

452
01:22:20,640 --> 01:22:22,756
You will be cured with this.

453
01:22:22,800 --> 01:22:24,848
You will see it.

454
01:22:42,000 --> 01:22:44,048
Quiet, Wolfy ...

455
01:22:44,120 --> 01:22:46,236
... nothing bad is going
to happen to you.

456
01:22:46,800 --> 01:22:49,462
You will feel very good ...

457
01:28:57,040 --> 01:28:59,088
Fly.

458
01:32:27,880 --> 01:32:29,928
I am badly wounded.

459
01:32:34,440 --> 01:32:36,488
Take it.

460
01:32:36,520 --> 01:32:38,568
I have three bullets left.

461
01:33:09,560 --> 01:33:11,608
Stay here.

462
01:33:11,680 --> 01:33:13,728
They will follow my trail.

463
01:33:22,000 --> 01:33:24,048
Search dog, search!

464
01:33:25,200 --> 01:33:27,248
He is there, let's go!

465
01:34:37,440 --> 01:34:39,488
Throw the knife away.

466
01:34:39,520 --> 01:34:41,568
Throw it!

467
01:34:46,320 --> 01:34:48,709
Where is Balilla hidding?

468
01:34:50,200 --> 01:34:52,248
Tell me or I kill you.

469
01:34:52,280 --> 01:34:54,669
Where is Balilla hidding?

470
01:34:55,000 --> 01:34:57,048
Where?!

471
01:35:38,520 --> 01:35:40,568
Foolish Balilla!

472
01:35:40,600 --> 01:35:42,921
Soon, you'll meet with them.

473
01:35:48,600 --> 01:35:51,398
As if it he was eaten by the earth.

474
01:35:51,840 --> 01:35:54,638
How the hell could he escape?

475
01:35:54,640 --> 01:35:56,039
I don't get it.

476
01:35:56,120 --> 01:35:58,236
We have searched everywhere.

477
01:35:58,840 --> 01:36:00,888
Ramirez.
- Yes, my sergeant.

478
01:36:00,920 --> 01:36:03,605
Let's look around the Fraile's creek
before dark.

479
01:36:03,640 --> 01:36:05,221
As you wish.

480
01:36:05,320 --> 01:36:07,845
You take care of this feral man.

481
01:36:07,880 --> 01:36:08,824
Don't worry, my Sergeant ...

482
01:36:08,880 --> 01:36:10,643
... before dusk you will
have him in the barracks.

483
01:36:10,720 --> 01:36:12,972
Caragorda, go with them.

484
01:36:13,640 --> 01:36:15,688
Manuel.

485
01:36:15,720 --> 01:36:17,654
You and me are going to Doroteo's hut ...

486
01:36:17,720 --> 01:36:20,427
... he knows a lot more than he says.

487
01:36:20,440 --> 01:36:22,488
Come on.

488
01:36:45,280 --> 01:36:48,147
Listen to me, tomorrow morning
at the sunrise ...

489
01:36:48,200 --> 01:36:52,227
... grab all your things and your family
and get lost from here.

490
01:36:52,280 --> 01:36:54,328
But... why?

491
01:36:54,400 --> 01:36:55,981
Because Don Honesto said so.

492
01:36:56,040 --> 01:36:58,224
But Don Ceferino, in the name of God ...

493
01:36:58,280 --> 01:37:00,328
... I've been here all my life.

494
01:37:00,360 --> 01:37:03,568
My wife and children
are buried here.

495
01:37:03,600 --> 01:37:05,488
How you dare to do this?

496
01:37:05,520 --> 01:37:06,942
They are Don Honesto's commands.

497
01:37:07,040 --> 01:37:09,656
But, there is a way
for me to help you.

498
01:37:09,680 --> 01:37:11,420
Tell me.

499
01:37:11,480 --> 01:37:15,371
You tell me this fucking time
where does Balilla hide.

500
01:37:17,440 --> 01:37:20,443
Don Ceferino
I have told you 1000 times ...

501
01:37:20,480 --> 01:37:23,131
... and I swear by my dead ones,
who are buried outside...

502
01:37:23,160 --> 01:37:24,377
... I don't know where Balilla is.

503
01:37:24,440 --> 01:37:27,034
Start packing then, moron.

504
01:37:31,120 --> 01:37:34,260
I know where you'll find him,
Don Ceferino.

505
01:37:34,280 --> 01:37:38,034
Pizquilla go inside,
this is not your bussiness.

506
01:37:38,480 --> 01:37:40,528
Where, Pizquilla?

507
01:37:44,240 --> 01:37:46,288
Where the hell?!

508
01:37:48,880 --> 01:37:52,020
Several nights
he stays in the One-Eyed's hut.

509
01:37:52,120 --> 01:37:54,850
Where his friends are all buried.

510
01:37:58,320 --> 01:38:00,368
There he is.

511
01:38:00,600 --> 01:38:02,648
Damn it!

512
01:38:06,200 --> 01:38:08,248
Boss, boss!

513
01:38:09,800 --> 01:38:11,848
Do you want me to go with you?

514
01:38:12,200 --> 01:38:15,340
And what the hell do
we do with the feral man?

515
01:38:23,960 --> 01:38:28,602
How do you know that
he stays in the One-Eyed's hut?

516
01:38:30,440 --> 01:38:32,488
I don't ...

517
01:38:32,720 --> 01:38:35,177
... but tonight there is full moon.

518
01:38:36,360 --> 01:38:38,681
and the fog is coming, Father.

519
01:39:21,520 --> 01:39:23,568
Alonso!

520
01:39:30,720 --> 01:39:32,836
Damn her dead ones!

521
01:40:35,000 --> 01:40:37,048
My sergeant!

522
01:40:37,080 --> 01:40:39,105
Where is Don Ceferino?

523
01:40:39,160 --> 01:40:41,151
Don't worry about him, boss.
Caragorda...

524
01:40:41,200 --> 01:40:44,749
... he is already getting drunk in town.

525
01:41:21,400 --> 01:41:23,448
Shut up!

526
01:41:24,000 --> 01:41:27,080
Best watched using Open Subtitles MKV Player
