﻿1
00:16:20,600 --> 00:16:22,443
Welcome to McFarland.

2
00:42:12,240 --> 00:42:13,241
Hello, son.

3
00:42:13,960 --> 00:42:15,086
Say hello to your father.

4
00:48:16,400 --> 00:48:17,780
Are you going to leave again?

5
00:48:17,880 --> 00:48:18,927
This is your fault.

6
00:48:20,360 --> 00:48:21,771
Everything is always my fault.

7
00:48:22,120 --> 00:48:23,326
Be quiet.

8
00:48:23,960 --> 00:48:25,325
You've abandoned us.

9
00:48:25,880 --> 00:48:27,041
Be quiet!

10
01:11:10,400 --> 01:11:11,380
Can you pass me another one, sweetheart?

11
01:11:11,480 --> 01:11:12,527
Yep.

12
01:11:15,840 --> 01:11:17,205
Got it, got it.

13
01:18:10,600 --> 01:18:15,162
Things are going really well for me.
Coach thinks we can make it to finals.

14
01:18:16,120 --> 01:18:17,770
Maybe...

15
01:18:18,760 --> 01:18:20,330
I can get into college.

16
01:18:21,600 --> 01:18:22,601
College?

17
01:18:26,800 --> 01:18:28,404
You know what...

18
01:18:29,640 --> 01:18:32,120
Take your face out of those books.

19
01:18:33,160 --> 01:18:35,606
They're going to ruin your eyes.

20
01:18:43,800 --> 01:18:47,486
Nobody ever needed a book in the fields.

21
01:38:16,800 --> 01:38:17,801
Javi.

22
01:38:25,000 --> 01:38:29,085
My God, Javi,
why won't they leave you in peace?

