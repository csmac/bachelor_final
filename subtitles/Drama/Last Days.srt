{1145}{1250}The only reason we...
{1285}{1300}even...|that it was even...
{1350}{1410}that she... that it was|even mentioned
{1425}{1500}in the... in the first place|is really... it wasn't for us,
{1510}{1530}you know,|like that she...
{1550}{1600}big fucking favor.
{8900}{8950}This fucking...
{8970}{9010}swamp.
{11345}{11400}'Cause I'm afraid...
{11490}{11525}You can't do anything.
{11560}{11610}You can't do anything.
{11630}{11650}I can't.
{11670}{11720}I don't know what I'm...
{11790}{11860}God, it's just...|I don't know
{11890}{11915}Just a...
{11965}{11980}I just...
{17110}{17165}"The gun is|in the bedroom closet."
{17240}{17280}Thanks.
{17515}{17580}Mmm. I do, I do.
{17730}{17800}I do, I do.
{18960}{19000}Damn.
{19550}{19600}Spoon.
{23765}{23786}Hey, Blake
{23859}{23888}Hey, did you|get my messages?
{23930}{23955}Mmm.
{24001}{24081}Ok, I got everybody with me here right now.|I got Trip here,|everybody's here.|- Blakey, Trip here.
{24090}{24165}What's going on at the house anyway?
{24182}{24245}What's all over there,|�what do you guys do all the time?
{24279}{24389}I mean I call, I call, I call,|I finally get through to you, and now you're not even talking to me.|Dude I know you're on the phone
{24442}{24569}Anyway, this tour is like... this hasn't been easy man.|Don has been working his ass off.|-Don rocks man!
{24580}{24672}beating up promoters in Europe|trying to get the money that you want.
{24688}{24742}We all wanna get on this, trying to make it all work.
{24851}{24958}So, if you need to go hide out in the clinic, you can.|Whatever you need. Let's just get this going man.|Give me the... You know...
{24981}{25058}Are you gonna play the tour?|That's what I need to know.|I'm assuming you're gonna play it.
{25085}{25211}We're ready. Everbody here has been working on it.|-It's only 86 days, so it should go pretty fast.
{25275}{25335}I mean, I thought this is what you wanted man,|Isn't this what you wanted?
{25345}{25415}It's gonna be a shame dude, |if you don't make these dates.
{25424}{25519}We're gonna have pissed off agents,|I'm gonna get yelled at. Everybody's gonna...The world's gonna fall apart on this.|You've gotta play the days...
{26020}{26070}Rabbit.
{27105}{27164}There's somebody here. Who is it?
{27211}{27231}It's Blake.
{27350}{27380}...you can.
{27440}{27465}Hm?
{27500}{27560}How are you sir? Thaddeus Thomas,|from Yellow Pages.
{27569}{27614}Who are you?|-Thaddeus Thomas, from the Yellow Pages.
{27648}{27689}I just want to make sure that you're represented in the book|who will be coming out shortly.
{28025}{28053}How's your day so far?
{28127}{28215}Mmm... pretty...|pretty good.
{28190}{28230}Not... not...|not, uh,
{28278}{28310}you know...|another day.
{28390}{28455}Thaddeus Thomas again.|Your name again?|Blake.
{28463}{28501}�Blake?|Nice to meet you Blake.|- Nice to meet you.
{28514}{28589}Ok, great. Well, we have some great opportunities here for you.|in the Yellow Pages.
{28604}{28759}What I would like to do is just ask a couple of questions|about your business.
{28789}{28864}Find out a little bit about what your business needs are.
{28881}{28969}So that this will enable me to build an effective ad
{28992}{29045}for the upcoming edition of the Yellow Pages. Ok?
{29115}{29145}Um, yeah.|-Ok. Great.
{29177}{29332}So, last year...you had...you ran an ad and...|-�I ran an ad last year?
{29342}{29484}Yeah, you ran|an ad last year.|This is for... you had a...|this is the locomotive shop,
{29514}{29604}- and you were selling parts for..|-Oh, yes, yes.
{29655}{29748}Right. And the ad was successful?|Did you get a lot of replies from that ad or..
{29767}{29900}I didn't, um...l...|I think it was...I think it was successful.
{29969}{30157} Okay.- I mean, it's kinda... it's kinda like...|Success is subjective,you know?
{30173}{30212}Right.- It's not...
{30223}{30248}Absolutely.
{30265}{30298}It could be an opinion.
{30311}{30445}Mm-hm, but in terms of people.|Obviously, you obtained|a lot of new customers|as a result...|- Mm-hm.
{30480}{30620}...and sold a lot of parts as well?|Yeah, we...yeah,|we sold quite a lot that year.
{30631}{30776}That's great, that's great. So what we're gonna do here is|simply continue that process.|Mm-hm.
{30806}{31016}So my next question would be, do you plan to |open up another store, another shop, a different location?
{31061}{31104}Well, what do you plan|to do business-wise?
{31124}{31491}I mean, we're just sort of playing it by ear.|We didn't really have any... Mm-hm.|Except, you know...thoughts about opening other...mm...shops.
{31500}{31530}Right. Right.|Yeah.
{31541}{31666}Ok, so about employees.|you might hire more employees?
{31678}{31726}Do you plan to expand your business in any way?
{31788}{31918}Ok?|Yeah, dude.|Just bugs.
{31936}{32070}So based on my records Blake,|you are the sole proprietor here.
{32078}{32226}You don't have partner, correct?|No, I don't have any partners.
{32311}{32394}Well, I'm happy to hear that your ad has been|working successfuly.
{32422}{32529}and we're going to be running the same ad...|one more time. Okay?
{32623}{32688}You should achieve the same level of success as well.
{32717}{32909}And the town of North Port is exploding with business|And a lot of people are moving in from around the country|because of the beauty.
{32931}{32954}Are you ok?
{32985}{33138}The beauty and the elegance of the city|and it's a very good place to do business
{33148}{33203}And you'll be the benefactor of that, right?
{33225}{33346}So what I would like to do, is|set up another meeting with you, okay?
{33356}{33497}Just wanna go over some options.|For different programmes that we have.
{33584}{33625}In the directory. Ok?
{33633}{33676}So for right now what I'm gonna do is|leave my business card with you, all right?
{33738}{33823}I'll be coming back on tuesday at 1:00|Is that time good for you?|- Mm-hm.
{33866}{34020}Ok, I want you to hold onto this right now.|and I'll be here at 1:00
{34073}{34098}next tuesday.
{34129}{34293}And at that point I'll have a copy of your ad.|And I'll discuss some additional programmes.
{34316}{34376}that I feel that you would benefit from. Ok?
{34411}{34443}Ok, very good.
{34474}{34534}All right, so it's been a pleasure, Blake, meeting you,
{34590}{34635}And hope to see you again very soon. Ok?
{34660}{34695}I'll see you next tuesday. Ok?
{34712}{34742}Alright.|Have a good day.
{34751}{34810}All right. You...you have a good day as well.|Ok. Very good.
{34825}{34842}Take care.
{35176}{35240}I'm sorry Blake.|�Blake?
{38576}{38596}Scott-
{38660}{38675}Scott-
{39218}{39254}Hello.|Hello Lukas.
{39264}{39294} No, it's not Luke. It's Scott|- You fucking asshole,
{39325}{39395}- I need to talk to Blake.|- Look, he's the asshole, not me.|- Get him now. I need to talk...
{39403}{39528}- No, Blake is not here|Don't fucking tell me he's not there.|He left rehab.
{39537}{39562}- None of us have seen him.|- This a bunch of bullshit.|Why are you making this so hard? I just need to talk to him.
{39625}{39685}I just need to talk to him.|- Yeah. - Fine, just get him.
{39733}{39772}Look, you just... just need to chill out.
{39787}{39817}Everything's under control here.
{39935}{40055}- Wow.|- Hello, my name's Elder Friberg.|My name's also Elder Friberg.
{40078}{40147}We're from the Church of Jesus Christ|of Latter Day Saints|- just down the road.|- Okay.
{40178}{40217}- How are you doing this morning?|- Good.
{40230}{40363}We were wondering if we could come in|and tell you a little bit about our church,|give you information on our religion.
{40496}{40531}- This is a nice house|- Thanks.
{40539}{40576}You want a drink?|No, thank you.|Don't you, um...the blood of Christ thing?
{40592}{40746}Don't you, um...the blood of Christ thing?|No, we use water instead of wine.
{40777}{40819}- Water for the blood of Christ?|- Yes.
{40882}{41034}The alcohol|in the wine is...|we believe is bad for our bodies, so we don't...
{41043}{41087}drink anything that's bad for our body.
{41115}{41199}Humph.|So have you guys had anything really freaky|happen to you when you go to these houses?
{41218}{41254}- Not really.|- Not normally.
{41302}{41332}And you're here|to convert us?
{41345}{41420}No, we're here just to give you background information|on our religion
{41456}{41530}in hopes that you come|to one of our services.
{41552}{41597}Do you know anything|about our religion?|- No.|- No?
{41617}{41667}All right, I'll give you|some background information.
{41691}{41813}How it started is, uh...|a young boy by a...|at the age of 14,
{41825}{41925}wanted to know which church to join,|because he noticed there were so many.
{41944}{41996}So he knelt down and he prayed,
{42004}{42111}and he said, "As I prayed,|a pillar of light descended upon me
{42142}{42234}and when that pillar reached me,|I saw two figures directly above me.
{42249}{42322}The figure on the right said,|'This is my son, Jesus Christ, hear him'."
{42635}{42804}God sent Jesus to the earth|to be the most pure being|to ever walk the earth.
{42827}{42944}And he was sent to be sacrificed|so that we didn't have to sacrifice lambs
{42953}{43002}to be forgiven.
{43018}{43065}Now, all we have to do is ask with a sincere heart to be forgiven.
{43080}{43142}Back then you couldn't talk to God unless you were pure.
{43157}{43362}So by puri... by killing something pure...|by sacrificing something that was innocent,|you became innocent yourself.
{43372}{43468}- Let me get a...|- Can you...|- can you talk to God now?|- Yes, when we pray.
{43485}{43541}- We've gotta go.|- You gotta go?
{43549}{43608}We have to go. We have|a lot of area to cover today.
{43616}{43701}We want 'em to stay, right guys?|Is there a time we can come back?
{50838}{50900}...when you come out,|because if you truly go...
{50920}{50980}if you truly go inside...
{51000}{51080}you know, if you|actually go there...
{51200}{51230}when you come out,
{51240}{51320}it can be|very frustrating.
{51350}{51380}Mmm.
{51410}{51440}Very...
{51490}{51550}I don't know,|it's just hard...|it's hard to speak
{51580}{51610}with other...
{51620}{51710}because you're speaking|like a different, uh...
{51758}{51788}Thank you|for your time.
{51818}{51932}Um, in the book...|- in the cover, there's scriptures...|- These?
{51956}{52015}Yes, and our phone number|if you have any questions.
{52028}{52050}- Thanks for the book.
{52058}{52106}- Thank you for having us inside.|- Nice to meet you.|- You have a good day.
{52120}{52280}Our number's in there|and if you could read over the scriptures|and then give us a call if you have any questions
{52296}{52408}- All right.|- We have services on Sunday|from... at 9:00.
{52426}{52448}Um, okay.
{52466}{52514}You guys talk to Jesus for real?|Huh? Huh?
{53535}{53585}Then one day he thought the way|to make money...
{53598}{53704}there are a lot of...|a lot of people who had|exotic backgrounds in vaudeville.
{53713}{53806}There was a Chinese magician named Ching Ling Foo|who was doing really well.
{53840}{53945}So Billy Robinson sort of disappeared|and he resurfaced|as a Chinese magician
{53955}{54001}named Chung Ling Soo.
{54021}{54139}He had his hair cut off|and made into a queue...|you know, one of those long queues at the back of his neck.
{54172}{54374}And he uh...|he had himself made up| o look like a Celestial.|And he became incredibly well-known performing...
{54384}{54438}- Was he a prophet?|..in England.|No, he was a magician. He was a stage magician.
{54452}{54498}- Oh.|- He did these really wonderful shows.
{54530}{54623}And the real Chinese magician,|Ching Ling Foo, got|outraged
{54635}{54727}and he tried|o have a competition with him.|There was headlines in the paper,
{54736}{54802}"Soo fools Foo,"|"Foo sues Soo."
{54813}{54925}You know, they had|this amazing rivalry.|Basically, the guy who wasn't|Chinese kind of won the contest,
{54938}{55062}as this great Chinese magician.|The other thing he did...he actually tried|o catch a bullet in his teeth on the stage.
{55085}{55241}- What? - And marksmen would get up.|They'd have a bullet autographed.|I mean, you would nick with your nail an initial into the bullet,
{55250}{55383}and a rifleman|would fire it at him.|And this Billy Robinson dressed up|as Chung Ling Soo would stand...
{55393}{55490}stand on the stage with a plate|in front of his mouth,
{55520}{55632}and the marksman would|fire the bullet.|And Chung Ling Soo would catch|the bullet in his teeth,
{55651}{55774}and then spit the bullet onto the plate.|- You're shitting me.|- And they would check it.
{55784}{55859}And it would be the same mark that was made by the guy in the audience.|- It was an amazing effect.|- Whoa.
{55879}{55922}It goes back|to the 16th century.
{55954}{55990}What was that book called?
{56002}{56057}There was a book about it called|"The Riddle of Chung Ling Soo"
{56074}{56224}by a guy named Will Dexter, but...|- I'll check that out. - The thing that's amazing|is he's performing one day at the Wood Green Empire Theatre,
{56237}{56402}you know, in London in 1918.|And he's doing this stunt...|they shoot him, he drops to the ground
{56430}{56520}and he's dead. He actually dies trying to catch the bullet|- in his mouth.
{56550}{56656}-No! - To this day, there's still all sorts of speculation|about what really happened.
{56668}{56810}"Was it an attempt|to commit suicide?"|He had some rocky relationship|with his wife Dot,
{56828}{56917}who performed with...|under the name of Suee Seen.
{56963}{57046}But the thing I always remembered about the case...|you know, being a P.I. I guess is that
{57057}{57149}the Coroner's report|called it "misadventure."
{57177}{57229}"Death by misadventure."
{57527}{57604}Not bad.|- This is a great castle
{57612}{57644}- Look at this stonework.
{57653}{57707}- Stonehenge.|- Well, not quite, is it?|-Bueno no casi-
{57740}{57867}No, but there's a similar castle down|three miles from here that has a replica.
{57877}{57899}Of Stonehenge?!
{57915}{58057}hey moved|the Salisbury Plain up here?|Just a replica. Was built for like|the princess of Romania, by this guy, Sam Hill Rockefeller.
{58086}{58150}That guy... that auto man|from Arizona... McCulloch
{58161}{58223}had the whole London Bridge|brought out to Arizona.
{58235}{58318}-Yeah, same thing.|-In 1831, there was this guy|who played his chin
{58327}{58397}at the opening of the bridge.|Michael Boai.
{58420}{58549}Played "Ladoiska," by Kreutzer.|The whole overture, man.|Sample that for some music.
{58579}{58651}- Out. - Yeah.|- Hey, man, have you seen Blake?
{58659}{58688}- No, I haven't seen him.
{58707}{58774}'Cause Blackie says|she hasn't seen him for a week.
{58791}{58869}- Who's this guy, man?|- Blackie told me to lead him around,
{58876}{58944}try to find him, 'cause she's worried about him, actually.|Come back later without him.
{58979}{58994}All right, man.
{59070}{59217}Um...We could probably go over to, uh...|Vanessa and Page's house.|He might be sleeping on their couch.
{59258}{59412}- Who are they? - Just some kind of|girlfriends that we all have.
{59422}{59512}Like, hang out kind of...|just mutual friends.
{59525}{59589}So Vanessa and Page are both women?|- Yeah.|- Yeah.
{59890}{59950}For every...
{60020}{60070}for every thought
{60100}{60130}of good...
{60200}{60250}for every thought of good
{60260}{60310}that my death...
{60370}{60400}will benefit.
{60810}{60840}I...
{60930}{60955}Iost...
{60100}{60125}something...
{60150}{60190}on my way.
{60520}{61600}I lost something|on my way...
{61650}{61740}to...|wherever...
{61770}{61820}I am today.
{62090}{62200}I remember...|when...
{62300}{62380}I remember...|when...
{62520}{62650}I remember...|when...
{63700}{63740}- Who's that?|- Yeah, right.
{63773}{63798}- That isn't...|- Hey, girlie. Oh, man.
{63849}{63885}- Who is it?|- That isn't Donovan.|Yeah, it is.
{64058}{64085}Fucking motherfucker.|- I'd like to kick his ass.|- Oh, yeah.
{64141}{64203}- Fucking asshole.|- Scumbag.|Fucking dickhead Don.
{64221}{64268}I cannot believe that wuss.|Who the fuck is that with him?
{64284}{64311}- I have no idea.|- Oh, God.
{65561}{65586}Hey, Blake!
{65731}{65763}Blake!|It's me, Donovan.
{65830}{65860}Blake?
{66330}{66360}Blake?
{75674}{75698}Hey, you.
{75956}{75981}Are you free?
{76082}{76102}Sort of.
{76151}{76188}Free enough to play that guitar?
{76310}{76340}Yeah.
{76350}{76400}I haven't been|playing very much.
{76567}{76601}Have you talked to your daughter, hm?
{76675}{76816}I have been talking to her on the phone.|What do you say to her?
{76905}{77040}I do the voices she likes, you know.
{77100}{77160}I tell her I miss her.
{77230}{77265}Do you say "I'm sorry that I'm a...
{77336}{77378}rock and roll clich�"?
{77450}{77480}Hmm.
{77683}{77718}Are you writing all that down?
{77927}{77961}Do you want something to drink? Something...|No no.
{78025}{78103}I have a car waiting,|and I want you to come with me.
{78156}{78265}I really can't. I got, uh...|I don't know. There's stuff that I gotta get done.
{78330}{78366}You have to come now.
{78382}{78422}You can go. You can get out of here.
{78485}{78510}No one's here.
{78528}{78600}You can go.|It'd be easy
{78661}{78703}If you stay here, you're just gonna...
{80330}{80350}Oh.
{80440}{80480}Shh.
{80691}{80713}It's okay.
{80766}{80786}It's okay.
{81000}{81040}I'm sorry.
{81149}{81183}You're not wearing a bra? Come on.
{81229}{81262}Come on, come on. Mama Mama Mama.
{85700}{85760}Ouch!|You okay?
{86607}{86726}There was this lyric in the bridge that I was trying to figure out,|and I've been having trouble figuring it out
{86760}{86868}'cause it's... like, I want it to be more personal,|and it's just sort of... I'm not getting there.
{86880}{86983}And I was wondering if you could maybe, like,|help me... help me figure it out a little...
{86990}{87060}Leave him the fuck alone and come upstairs with me, all right?
{87090}{87230}Um. I'm sorry I'm talking your ear off.|I didn't ask.
{87260}{87290}You know.
{87573}{87642}Luke... Lukas,|I've gotta get out of here, man.
{87670}{87697}Why? Why?
{87742}{87895}That ranger,|the cop.|He's not a real cop. He's not a ranger.|He's not "Walker, Texas Ranger."
{87924}{88029}He's a private eye.|Well, what do you mean?
{88055}{88110}He's not real,|but she's probably coming back.
{88146}{88190}- Herself?|- Yeah.
{88537}{88624}You fucking bug him|- every time you talk to him about the demo tape|- Really?
{88664}{88734}- I'm not bugging.|He doesn't wanna help you, he just saying that to...
{88750}{88800}to get you|to leave him alone, man.
{88865}{88904}Is that what you think?
{88920}{88966}- You really think that?|- I know that, man.|-Si-
{88984}{89012}- Well, thanks. Thanks a lot, then.|- Sorry.
{89027}{89104}You gotta leave him alone.
{89370}{89437}Forget about the demo.
{90330}{90430}I need some contact with her.|You know, it's not...
{90450}{90550}everyone... I'm being treated like I'm a...|like I'm a...
{90570}{90660}fucking criminal, you know?
{90680}{90750}I don't...I don't like...|I don't wanna|put it in the...
{91460}{91480}Hey, Blake!
{91596}{91657}Blake! It's me, Donovan!
{91710}{91750}Blake?|Blake?
{91820}{91850}Blake?
{91960}{91980}Blake?
{92300}{92350}Blake?
{92548}{92596}You know, I used to work|for the forest service in the summer.
{92628}{92658}Hey, bro?|Blake?
{92874}{92900}Blake!
{92950}{92970}Blake!
{94131}{94209}Wanna shoot some craps?|Look at this.
{94218}{94324}Look how it's starting to crystallize.|This is cellulous nitrate,|same stuff as film stock.
{94333}{94421}This is eventually gonna implode.|Completely crystallize and implode.
{94527}{94558}What do you suppose is over there?
{94592}{94667}Oh, yeah. That over there... that, that's..|that's the greenhouse, where they keep the garden tools and stuff.
{95660}{95690}I'm your boyfriend.
{95720}{95750}No.
{95972}{96002}Oh, Christ.
{96124}{96164}Oh, whatever you want it to be, sir.
{96257}{96355}A package of artificial cheese with your...
{96350}{96400}macaroni and cheese.
{98741}{98838}Hey, Blake. Can I have your attention, man?|There's something I really gotta talk to you about.
{98875}{98953}You know, I've been... I've been stuck here for a week with...|with no apples and...
{98983}{99037}and you know how I got fucked up on that airplane going to Portland?|dosis en el aeropuerto?
{99080}{99175}And I threatened to pee on the... on the stewardess?|- Mm-hm.|- And they diverted the flight?
{99214}{99272}- Hmm.|- I have... I have to go to Utah, man,|and I don't have any money.
{99332}{99397}it's colder than|shit in this house.
{99469}{99602}I was watching football last night,|they have these...they have these...|these jet heaters.
{99617}{99683}They're like jet engines.|este tama�o-
{99692}{99735}They can probably...they could heat this whole place.|- Mm-hm. - I mean,
{99788}{99823}it sounds like a good idea to me.
{99843}{99928}- Hmm. - So I need... I need to go to Utah|and I need, uh...we need a jet heater
{99980}{100020}Hmm.
{100850}{100950}Ouch!|You okay?
{102797}{102914}Blake. Here's that... that demo tape I was...|I was gonna give you.
{102960}{103004}- Mmm.|It's... I recorded it on the four track...|upstairs.
{103083}{103217}But...|there's this one... one of the songs is a song about...
{103251}{103315}this girl...|when I was touring in Japan, and uh...
{103405}{103477}I was... we were... it was the last night of the tour in Tokyo.
{103572}{103732}She was...she gave me this card
{103764}{103857}at the end of...|at the end of the tour. I mean, at the... like, at the after party.
{103901}{103987}And she drew little tits on it and gave me her number,|and I called her that night at 12:00 or something.|And she came right over, and we, like... we had sex.
{104034}{104121}And... and she was like...|her body was unbelievable. She was just so beautiful.
{104149}{104270}I think she was like a star,|like a music and like TV star or something.
{104380}{104562}And like...and so l...|she kept calling me.|Well, so... so we had sex|and it was probably the best sex I ever had in my whole life.
{104602}{104807}And then, uh...then I left the next day back to America|and... and, uh, she kept calling me|and she wanted to come visit me
{104841}{104912}and, uh, I didn't...|I never called her back,
{104930}{104987}and I felt really bad about it.|So I wrote this song... that's one... that's the song...
{104996}{105055}one of the songs on that tape.
{105076}{105175}And there's... there's...|I wanna make it as personal as I can
{105205}{105282}like, I wa... I wanna apologize to her, you know?
{105300}{105420}- Hmm.|- I was sort of...|I actually was wondering if you could help me with some of the lyrics on it
{105454}{105510}'cause there's... there's this one part in the song|on the bridge where...
{105530}{105598}where I don't know exactly how to put what I'm trying to say.
{105618}{105656}It's like I wanna do it more...
{105676}{105726}Leave him the fuck alone and come upstairs with me, all right?
{105843}{105936}Oh.|I'm... I'm sorry.|I'm talking your ear off.
{105992}{106069}I just wasn't thinking, man,|You know.
{106100}{106130}-Mmm.|-See you.
{106174}{106249}I'll listen to it.
{106274}{106304}Listen to it, yeah.|Tell me what you think.
{106550}{106580}Hmm.
{115566}{115616}Hey.|What's going on, man? How are you doing?
{115645}{115669}How are you?
{115759}{115839}L... I missed|you Thursday.
{115882}{116022}We tried to get in touch 'cause l..|I went to this... because...|this, uh... I fucking hate talking here.
{116050}{116145}We went to the Dead... uh, to a Dead show|and it was like at least about 2-300,000 people,
{116150}{116270}but that's not even the... the point is that afterwards|we played "Dungeons and Dragons."
{116330}{116507}We played that "D&D" game with Jerry,|and Jerry was like the best dungeon master.|We captured the winged Pegasus.
{116540}{116700}And his girlfriend...|Suzette or Jacquette, some French name...|she was like... she did these voodoo rituals.
{116720}{116835}She gave us, uh...|Now it's only half a jaw,|but before it was a whole jaw.
{116860}{117000}And... it's something about virility,|but I know you weren't feeling...|you're not feeling that great,
{117051}{117224}so if you want the bottom jaw,|I could give you the bottom jaw, like...|I could give you the jaw... at least.
{117300}{117330}It works.
{122607}{122661}We're coming,|we're coming.
{122660}{122700}Where did Asia go?
{122751}{122773}- Hey, Nicole.|- What?
{122824}{122846}Give me your hand.
{122882}{122991}We were just three... guys.|- No, we're not three guys. - No, we're not|three guys.
{123003}{123057}- We were just two guys and a girl.
{123115}{123172}Going, leaving for no particular reason. 
{123191}{123387}Why... why are we leaving, Scott?|Oh, yeah. Why are we leaving, Scott?
{123330}{123410}- Where are we going? - Nobody knows|- To my place?|- Where we're going.
{123440}{123500}Yep. That's right.
{123900}{124010}Sorry.
{127250}{127323}...of Blake were so excruciatingly painful|that he sometimes wanted to die or kill himself.
{127330}{127430}Do you think this is a contributing thing here?|I mean, do you think he was exaggerating in saying that?
{127468}{127504}- No, I don't think...|- No, I know.
{127510}{127550}I actually spoke to his mother about this.|- She had an identical... 
{127579}{127627} - I understand.|- ...situation uh, at about his age| - I understand. Okay.
{127789}{127814}We're fucked. We're gonna be implicated
{127832}{127889}and tied up in this whole thing.
{127898}{127950}They're gonna accuse us of copping for Blake.
{128043}{128082}And they're gonna say|we were there.
{128158}{128195}Do we need to get out of here?
{128252}{128288}I don't have anything.|I don't have any of my stuff ready.
{128390}{128422}This is giving me the creeps.
{128435}{128472}Do we need anything?|Can we just go?
{128528}{128555}- Let's just get the fuck out of here.|- Let's just go.
{128776}{128813}Where are we gonna go?
{128825}{128863}We'll go to Los Angeles.
{129100}{129140}Fuck.
{129160}{129190}Are you all right?
