﻿1
00:02:19,606 --> 00:02:22,746
- Are you bored?
- No.

2
00:06:16,109 --> 00:06:21,787
CLIMATES

3
00:08:59,539 --> 00:09:02,679
Should we give Arif a call?

4
00:09:16,155 --> 00:09:17,566
Bahar!

5
00:09:21,928 --> 00:09:23,498
Sure.

6
00:09:33,873 --> 00:09:37,218
- Semra, dinner was great. Thank you.
- I hope you enjoyed it.

7
00:09:37,376 --> 00:09:40,619
- How about some coffee?
- How do you take it?

8
00:09:40,780 --> 00:09:44,785
- Medium-sweet for me.
- Me too.

9
00:09:50,189 --> 00:09:54,535
- It must be interesting living here.
- It's kind of boring actually.

10
00:09:54,760 --> 00:09:58,298
- Why are you living here then?
- What else can I do?

11
00:09:58,664 --> 00:10:02,339
The weather's great but that's about it.

12
00:10:03,836 --> 00:10:06,840
If there were a few more people
like us around...

13
00:10:07,006 --> 00:10:10,215
Why don't you come down more often?

14
00:10:10,376 --> 00:10:14,051
Actually this is the first time
we've been away in years.

15
00:10:14,180 --> 00:10:18,026
Bahar has that TV series.
The schedule's always uncertain.

16
00:10:18,150 --> 00:10:20,528
And I have the university as you know.

17
00:10:20,920 --> 00:10:25,062
I'm free in summer
but then Bahar is usually busy.

18
00:10:26,959 --> 00:10:31,169
- What's your job on the series, Bahar?
- Art director.

19
00:10:31,330 --> 00:10:33,936
Is that the one with Ugur Yucel?

20
00:10:34,400 --> 00:10:39,179
No, that's a different series.
There are no big names in ours.

21
00:10:52,084 --> 00:10:54,189
It's getting cold.

22
00:10:54,587 --> 00:10:57,864
- Aren't you cold?
- No, I'm fine.

23
00:10:58,591 --> 00:11:01,868
- Why don't you put your jacket on?
- I told you, I'm fine.

24
00:11:22,882 --> 00:11:25,556
You seem kind of fed up.
What's wrong?

25
00:11:41,067 --> 00:11:43,809
What's going on?

26
00:11:45,404 --> 00:11:47,441
Is there a problem?

27
00:11:48,507 --> 00:11:50,453
Why are you using that tone?

28
00:11:50,576 --> 00:11:53,079
Why do you keep nagging?

29
00:11:53,846 --> 00:11:57,316
If I'm nagging,
it's for your own good, isn't it?

30
00:12:00,486 --> 00:12:03,763
If I was cold,
I'd be wearing the thing anyway.

31
00:12:04,957 --> 00:12:08,996
Can't we go anywhere
without you making problems?

32
00:12:09,161 --> 00:12:11,835
Hey, come on!
Don't go over the top.

33
00:12:17,236 --> 00:12:20,979
Don't worry.
They enjoy seeing us miserable.

34
00:12:22,007 --> 00:12:26,649
- Come on. You wrecked dinner...
- No, it's okay. Don't worry.

35
00:12:53,239 --> 00:12:55,776
Hey, come on, guys!

36
00:12:56,142 --> 00:12:58,349
Just forget it.

37
00:12:58,511 --> 00:13:01,754
So you can hear the waves
all the way up here.

38
00:13:03,649 --> 00:13:05,094
Is that the sea just down there?

39
00:13:05,251 --> 00:13:07,925
Yes, it's right there.

40
00:13:09,221 --> 00:13:12,134
- Just behind those trees?
- Yes.

41
00:13:12,758 --> 00:13:14,738
Kas has got so big.

42
00:13:14,860 --> 00:13:19,707
I came here back in 1977.
There was nothing here then.

43
00:13:19,865 --> 00:13:25,281
It's tourism.
The Germans started coming. So then...

44
00:13:55,601 --> 00:13:57,774
Shall we go for a swim?

45
00:13:58,671 --> 00:13:59,809
What, now?

46
00:13:59,972 --> 00:14:02,578
- Yes.
- You'll freeze.

47
00:14:03,175 --> 00:14:08,682
Are you crazy? For God's sake!
Where do you get these ideas?

48
00:14:09,949 --> 00:14:16,559
- Why not if you want. But...
- Forget it. It's bad for lsa's neck.

49
00:14:22,595 --> 00:14:26,372
It's late.
Let's have our coffee and get going.

50
00:14:27,299 --> 00:14:30,246
Semra, where's the coffee?

51
00:14:32,605 --> 00:14:37,076
- You can stay here if you like.
- No, it's okay. The hotel's fine.

52
00:14:37,243 --> 00:14:40,486
- Are you comfortable there?
- Yes.

53
00:16:03,629 --> 00:16:05,631
I love you.

54
00:17:03,789 --> 00:17:09,330
- What's up? You're covered in sweat.
- I must have fallen asleep.

55
00:17:10,763 --> 00:17:15,508
You shouldn't be sleeping in the sun.
It's really dangerous.

56
00:19:14,853 --> 00:19:16,526
Bahar...

57
00:19:20,459 --> 00:19:23,906
I want to say something,
but please don't get the wrong idea.

58
00:19:40,179 --> 00:19:44,457
Maybe we should go our own ways
for a bit. What do you think?

59
00:19:58,597 --> 00:20:02,875
We'd still be good friends.
It wouldn't change things a lot.

60
00:20:07,139 --> 00:20:11,212
We'd still go out together,
for dinner, to the movies.

61
00:20:24,022 --> 00:20:27,629
We don't have to be friends.
I don't mind.

62
00:20:29,328 --> 00:20:34,607
Come on, Bahar.
Don't be like that for God's sake.

63
00:20:41,273 --> 00:20:44,152
This would be better for you as well.

64
00:20:44,309 --> 00:20:48,382
Even that meaningless "Serap" incident
has screwed you up.

65
00:20:48,547 --> 00:20:51,289
You call it meaningless?

66
00:20:54,286 --> 00:20:57,631
For God's sake,
don't get started on that again.

67
00:21:01,627 --> 00:21:05,473
It's better that way for you as well.
You know it yourself.

68
00:21:09,501 --> 00:21:13,347
You don't have to worry about me.
I'll be fine.

69
00:21:13,505 --> 00:21:18,147
That's what I mean.
You're young. You're attractive...

70
00:21:18,877 --> 00:21:21,756
You can get all the men you want.

71
00:21:29,588 --> 00:21:32,398
Maybe it's true what you said.
Maybe I am too old for you.

72
00:21:32,557 --> 00:21:35,538
The age difference
really has become a problem.

73
00:21:37,829 --> 00:21:40,742
It just isn't working.
We can't seem to get along.

74
00:21:52,911 --> 00:21:57,382
I don't need convincing, you know,
I agree with you.

75
00:21:58,884 --> 00:22:05,199
I'm not trying to convince you,
just pointing out the facts.

76
00:22:06,858 --> 00:22:09,464
You know I'm right.

77
00:23:44,389 --> 00:23:47,199
Are you crazy?
Are you trying to kill us both?

78
00:23:47,359 --> 00:23:49,862
If you're that keen to die,
shall I throw you off?

79
00:23:50,028 --> 00:23:54,807
- Shall I throw you off?
- Get off me!

80
00:24:21,126 --> 00:24:22,867
Come back here.

81
00:24:23,795 --> 00:24:25,672
Bahar!

82
00:24:32,070 --> 00:24:34,277
Come back!

83
00:24:55,894 --> 00:24:59,205
Fucking hell!

84
00:26:12,971 --> 00:26:15,383
Your baggage receipt.

85
00:26:17,075 --> 00:26:18,952
Don't lose it.

86
00:26:21,346 --> 00:26:25,021
Do you have a sweater with you?
It gets really cold at night.

87
00:26:25,684 --> 00:26:27,322
Yes, I do.

88
00:26:28,887 --> 00:26:30,366
Excuse me.

89
00:26:32,691 --> 00:26:35,638
- Okay, then. Have a good trip.
- Thanks.

90
00:26:36,895 --> 00:26:39,967
- I'll call you when I'm back in Istanbul.
- Don't.

91
00:26:40,365 --> 00:26:42,845
- What?
- Don't call.

92
00:28:59,270 --> 00:29:01,216
Okay, people.

93
00:29:02,474 --> 00:29:06,980
I want you to compare
the architecture of these two temples.

94
00:29:09,714 --> 00:29:12,820
You have 15 minutes.
Any questions?

95
00:29:14,886 --> 00:29:17,457
- How are you doing?
- Fine.

96
00:29:22,393 --> 00:29:25,203
- How did the exam go?
- Okay.

97
00:29:26,397 --> 00:29:29,071
Who switched on my computer?

98
00:29:30,335 --> 00:29:32,337
I have no idea.

99
00:29:32,504 --> 00:29:34,916
Probably one of your students.

100
00:29:35,039 --> 00:29:39,488
- Come on! Was it you?
- No. Why would I switch it on?

101
00:29:42,046 --> 00:29:44,822
- Are you leaving?
- Yes, I have to.

102
00:29:45,550 --> 00:29:49,157
I'm going to an art exhibit with Pinar.
Join us if you like.

103
00:29:49,320 --> 00:29:52,426
Thanks, but no.
I want to go through these exam papers.

104
00:29:52,590 --> 00:29:54,570
Can't you do that later?

105
00:29:54,726 --> 00:29:58,697
I don't like taking work home.
I'll get it done here.

106
00:29:58,863 --> 00:30:02,675
I've booked the court for Saturday.
Hopefully that's okay with you.

107
00:30:02,834 --> 00:30:09,251
- What if it rains?
- Always excuses. lf it rains, it rains.

108
00:30:09,407 --> 00:30:12,980
- In that case, we play even if it snows.
- It's a deal.

109
00:30:13,111 --> 00:30:15,523
- No getting out of it.
- No.

110
00:30:16,848 --> 00:30:20,125
- We'll even go for a swim afterwards.
- In the sea?

111
00:30:20,485 --> 00:30:23,557
All right, we'll do that too.

112
00:30:23,688 --> 00:30:25,326
Okay, see you then.

113
00:30:25,490 --> 00:30:28,960
- Let's have coffee before you go.
- I don't have time.

114
00:30:29,127 --> 00:30:32,904
She'll only complain if l'm late.
I should go.

115
00:30:33,064 --> 00:30:35,977
- Okay. Have a good time.
- Thanks.

116
00:31:13,972 --> 00:31:16,452
Isa... Isa!

117
00:31:17,308 --> 00:31:21,120
- Hey, how are you?
- Great.

118
00:31:22,046 --> 00:31:27,018
- What are you up to?
- Same as ever. Work and all that.

119
00:31:27,185 --> 00:31:33,101
- You're still in advertising?
- Yes. They won't let me go.

120
00:31:33,258 --> 00:31:37,206
- Hey, Serap! How are you?
- Fine. And you?

121
00:31:37,328 --> 00:31:41,936
Fine. It's so nice that you're still
together after all this time.

122
00:31:42,100 --> 00:31:44,603
Did you get married?

123
00:31:47,572 --> 00:31:50,849
I have an hour to kill.
Shall we have a drink together?

124
00:31:51,009 --> 00:31:52,818
Let's see.

125
00:31:53,011 --> 00:31:54,854
Sorry. I have to meet someone.

126
00:31:55,013 --> 00:31:58,256
Come on.
We hardly ever run into each other.

127
00:31:58,383 --> 00:32:01,091
- I can't. But let's be in touch.
- Okay.

128
00:32:01,252 --> 00:32:03,528
- Don't forget.
- You never call either.

129
00:32:03,688 --> 00:32:08,694
Neither do you.
We're both as bad as each other.

130
00:32:08,860 --> 00:32:10,032
- How are you doing?
- Okay.

131
00:32:10,194 --> 00:32:13,300
We're just looking
at books and magazines.

132
00:32:13,464 --> 00:32:18,607
- Okay. I'll see you then.
- See you. Take care.

133
00:34:41,979 --> 00:34:44,482
The place has changed.

134
00:34:55,393 --> 00:34:58,897
Why are you looking at me like that?
I can leave, if you want.

135
00:35:00,331 --> 00:35:03,505
How did you know I'd come home alone?

136
00:35:04,869 --> 00:35:08,043
Guven said so, didn't he?
At the bookstore.

137
00:35:11,709 --> 00:35:14,246
And I guess I wanted to try my luck.

138
00:35:18,716 --> 00:35:23,358
- Aren't you going to offer me anything?
- You want something to drink?

139
00:35:23,888 --> 00:35:25,834
What should I have?

140
00:35:27,225 --> 00:35:29,330
Something alcoholic?

141
00:35:31,729 --> 00:35:34,437
Have whatever you want.

142
00:35:35,199 --> 00:35:37,338
Okay, I'll have coffee.

143
00:35:38,703 --> 00:35:40,376
It's coffee then.

144
00:35:40,505 --> 00:35:43,247
Then we'll move on to wine, okay?

145
00:35:44,976 --> 00:35:46,785
Sure.

146
00:37:11,095 --> 00:37:14,133
You haven't been around for a while.

147
00:37:14,298 --> 00:37:16,869
Is Bahar out of town again?

148
00:37:19,070 --> 00:37:21,448
What's it got to do with Bahar?

149
00:37:28,946 --> 00:37:31,256
What are you trying to say?

150
00:37:57,608 --> 00:38:00,350
What are you laughing at
for God's sake?

151
00:38:16,661 --> 00:38:19,141
Okay, okay, calm down!

152
00:38:19,563 --> 00:38:21,565
l am.

153
00:39:00,705 --> 00:39:04,380
- These are stale.
- Don't eat them then.

154
00:39:08,612 --> 00:39:10,785
Open your mouth.

155
00:40:18,849 --> 00:40:23,696
How can you wear those pointed shoes?
They're so ugly.

156
00:40:25,322 --> 00:40:27,427
I like them.

157
00:40:28,392 --> 00:40:29,962
You want it?

158
00:40:30,127 --> 00:40:31,470
- Nothing's wrong with it.
- No!

159
00:40:31,629 --> 00:40:35,406
- Don't be so picky.
- It's been on the floor.

160
00:40:35,566 --> 00:40:40,140
- So? The floor's clean. Go on.
- I told you, I don't want it.

161
00:45:45,843 --> 00:45:49,017
There, it's done.

162
00:46:01,058 --> 00:46:03,595
That's much better.

163
00:46:05,162 --> 00:46:09,474
I was treading on them before.
They're just right now.

164
00:46:09,633 --> 00:46:12,546
- Was there anything else?
- No. Thanks.

165
00:46:12,703 --> 00:46:14,774
You're welcome.

166
00:46:16,273 --> 00:46:18,116
I have to go, Mom. I'm running late.

167
00:46:18,275 --> 00:46:22,018
I'm still working on my thesis.
I have to meet someone.

168
00:46:22,146 --> 00:46:27,061
- You haven't eaten anything.
- I'm not hungry. I can eat at home.

169
00:46:27,584 --> 00:46:34,001
- Drop by now and again.
- I can't but I will try.

170
00:46:34,158 --> 00:46:38,698
If I could just finish my thesis...

171
00:46:39,396 --> 00:46:43,242
You should be thinking
about having children.

172
00:46:43,734 --> 00:46:46,305
Mom, don't keep going on about that.

173
00:46:46,436 --> 00:46:48,882
What does it matter
if I do or don't have children?

174
00:46:49,039 --> 00:46:50,814
- Of course it matters.
- Why?

175
00:46:50,974 --> 00:46:58,222
We're not getting any younger. Who knows
how much longer we'll be around?

176
00:46:58,382 --> 00:47:03,297
- Children add so much to your life.
- But I don't even like kids.

177
00:47:03,787 --> 00:47:07,963
It's different when they're your own.
Just wait and see.

178
00:47:09,359 --> 00:47:10,599
Dad, why are you wearing that hat?

179
00:47:10,727 --> 00:47:14,174
That's a new habit.
He even wears it when guests come over.

180
00:47:14,331 --> 00:47:20,077
This hat has more uses than you think.
For one, it protects the blood vessels.

181
00:47:20,237 --> 00:47:23,309
It makes the blood thinner
so it flows more easily.

182
00:47:23,473 --> 00:47:25,248
Well, don't get too used to it, Dad.

183
00:47:25,375 --> 00:47:30,017
He'll soon need another hat on top
whenever he goes outside.

184
00:47:31,515 --> 00:47:36,328
I keep telling him but he never listens.

185
00:47:37,888 --> 00:47:41,335
It's late. I'd better be going.

186
00:47:41,859 --> 00:47:44,840
Come and visit more often.

187
00:47:50,767 --> 00:47:53,873
- That was in.
- What do you mean? It was out.

188
00:47:54,805 --> 00:47:57,809
- For me, it was on the line.
- Are you blind?

189
00:47:58,642 --> 00:48:03,716
So we'd planned the whole wedding.
Imagine, everything's ready.

190
00:48:04,147 --> 00:48:06,627
We go out for dinner one night.

191
00:48:08,752 --> 00:48:10,663
Then suddenly, what do I see?

192
00:48:10,821 --> 00:48:13,358
A finger wagging in my face.

193
00:48:13,523 --> 00:48:14,866
That was a great image.

194
00:48:15,025 --> 00:48:16,868
Unbelievable!

195
00:48:17,027 --> 00:48:22,636
I said to myself,
just get out while the going's good.

196
00:48:22,799 --> 00:48:26,144
I walked out and that was it.
Really!

197
00:48:26,303 --> 00:48:27,907
I just left her like that.

198
00:49:01,872 --> 00:49:04,284
- That dress looks great on you.
- Really?

199
00:49:04,441 --> 00:49:07,354
- Yes, really.
- I like it a lot too.

200
00:49:07,477 --> 00:49:10,185
You look good in black.

201
00:49:10,881 --> 00:49:12,883
Thank you.

202
00:49:15,485 --> 00:49:18,056
Guven got it for me in London.

203
00:49:18,221 --> 00:49:19,825
Yes.

204
00:49:20,524 --> 00:49:23,266
The bastard has good taste.

205
00:49:26,396 --> 00:49:28,899
Why's he gone to Kazakhstan?

206
00:49:29,833 --> 00:49:34,942
He's setting up an advertising agency
there. With some friends of his.

207
00:49:35,405 --> 00:49:37,510
God! The stuff he gets into...

208
00:49:37,674 --> 00:49:38,812
When does he get back?

209
00:49:38,976 --> 00:49:41,650
- In a week or so.
- That's good.

210
00:49:42,446 --> 00:49:44,722
So you're free for a whole week.

211
00:49:45,849 --> 00:49:47,988
What's Bahar up to?

212
00:49:48,719 --> 00:49:50,460
She's okay.

213
00:49:52,622 --> 00:49:55,432
Does she come back
to Istanbul now and then?

214
00:49:56,626 --> 00:49:59,163
- Where from?
- From the east.

215
00:49:59,796 --> 00:50:01,173
What do you mean "the east"?

216
00:50:01,331 --> 00:50:05,006
I heard she's working
on a TV series there.

217
00:50:05,168 --> 00:50:06,476
Who told you that?

218
00:50:06,603 --> 00:50:12,576
Nevzat. I ran into him the other day
at the opening of the Istanbul Modern.

219
00:50:12,743 --> 00:50:14,154
Who's Nevzat?

220
00:50:14,311 --> 00:50:17,383
The architect, you know.

221
00:50:17,714 --> 00:50:19,921
You mean Nevzat Sayin?

222
00:50:20,150 --> 00:50:23,495
- How does he know?
- I've no idea.

223
00:50:26,523 --> 00:50:29,333
- When's she supposed to have gone?
- I really don't know.

224
00:50:29,493 --> 00:50:31,632
Wait, didn't you know?

225
00:50:32,696 --> 00:50:34,141
No.

226
00:50:36,433 --> 00:50:38,208
Why not?

227
00:50:38,635 --> 00:50:40,478
I just didn't.

228
00:50:40,604 --> 00:50:45,485
- We broke up.
- Come on! I don't believe you.

229
00:50:45,642 --> 00:50:46,985
- Didn't I tell you?
- No.

230
00:50:47,110 --> 00:50:49,852
Weren't you together
when you last came here?

231
00:50:50,013 --> 00:50:52,425
Well, why didn't you say so?

232
00:50:53,216 --> 00:50:54,718
You didn't ask.

233
00:50:54,885 --> 00:50:58,094
How was I supposed to know
you'd split up?

234
00:50:58,822 --> 00:50:59,857
So why did you break up?

235
00:51:00,023 --> 00:51:02,435
It happened ages ago. In Kas.

236
00:51:03,527 --> 00:51:06,838
Back in the summer
when we were in Kas.

237
00:51:09,699 --> 00:51:15,411
Go on! What happened there?
I love hearing about break-ups.

238
00:51:17,974 --> 00:51:20,614
Forget it.
It's a long story.

239
00:51:21,645 --> 00:51:26,151
It's all dust and ashes.
Just like the song.

240
00:51:37,094 --> 00:51:40,871
- Can I have more wine? It's good.
- Sure.

241
00:51:47,704 --> 00:51:50,014
Thanks.

242
00:51:54,010 --> 00:51:57,719
There's been an earthquake somewhere.
But I didn't catch where.

243
00:51:59,649 --> 00:52:02,152
I wonder if it was here in Turkey.

244
00:52:23,607 --> 00:52:26,019
Are you going to watch that?

245
00:52:27,911 --> 00:52:31,484
No, it's Cuneyt Ozdemir's thing but...

246
00:52:32,415 --> 00:52:34,691
Come over here.

247
00:52:53,136 --> 00:52:55,673
The earthquake...

248
00:53:57,567 --> 00:53:59,911
You said she wagged her finger
in your face.

249
00:54:00,070 --> 00:54:06,043
Well, you should see Pinar now.
She's meek as a lamb.

250
00:54:06,209 --> 00:54:09,122
There's no more wagging fingers.

251
00:54:09,479 --> 00:54:11,254
Believe me.

252
00:54:13,316 --> 00:54:18,527
After I walked out that evening
she really got scared.

253
00:54:18,688 --> 00:54:22,534
It's the truth.
She knows where she stands now.

254
00:54:24,227 --> 00:54:27,299
She won't revert to her old ways
when you get married?

255
00:54:27,430 --> 00:54:30,570
No chance of that.
The girl's terrified!

256
00:54:30,700 --> 00:54:36,878
The way I acted that night
she really thought it was all over.

257
00:54:39,743 --> 00:54:42,019
I wouldn't be so sure.

258
00:54:43,446 --> 00:54:46,222
What are you doing
for the winter holiday?

259
00:54:46,383 --> 00:54:51,230
Maybe take a trip to Russia,
but we're not sure yet.

260
00:54:51,388 --> 00:54:53,891
Are you crazy? At this time of year?

261
00:54:54,057 --> 00:55:00,667
I promised Pinar some time ago
so l have to go through with it.

262
00:55:01,631 --> 00:55:04,612
What are you up to?
You have any plans?

263
00:55:04,834 --> 00:55:06,745
Wow! A trip to the sun, huh?

264
00:55:06,903 --> 00:55:09,679
I need some decent weather.

265
00:55:10,974 --> 00:55:13,113
The cold here gets to my bones.

266
00:55:13,276 --> 00:55:16,018
You'll get bored on your own.

267
00:55:17,213 --> 00:55:19,454
Why would I get bored?

268
00:55:20,884 --> 00:55:24,661
I've been dreaming
of a trip on my own for years.

269
00:55:26,856 --> 00:55:30,167
I'm sure to meet someone there anyway.

270
00:55:31,061 --> 00:55:34,042
It sounds good at first,
but the more I think about it...

271
00:55:34,197 --> 00:55:39,977
I find places like that a bit vacuous
and kind of empty.

272
00:55:40,136 --> 00:55:41,615
What places?

273
00:55:41,771 --> 00:55:45,378
Faraway places.
Especially when I'm on my own.

274
00:55:45,542 --> 00:55:49,149
- Other countries.
- But you're going to another country.

275
00:55:49,312 --> 00:55:52,122
But at least I have Pinar with me.

276
00:55:52,282 --> 00:55:55,525
You'll be fighting every day.
Just wait and see.

277
00:55:55,685 --> 00:55:58,689
No, we won't. Not anymore.

278
00:55:59,356 --> 00:56:01,461
Hopefully you're right.

279
00:56:01,725 --> 00:56:07,676
I have a class to go to.
The students will be waiting.

280
00:56:09,099 --> 00:56:11,978
- Are you here after class?
- I'll be around.

281
00:56:12,135 --> 00:56:16,880
- I'll see you later then.
- Have a good class.

282
00:58:16,593 --> 00:58:18,072
- Welcome.
- Thanks.

283
00:58:18,228 --> 00:58:19,536
- Do you have a room?
- Yes.

284
00:58:19,696 --> 00:58:21,903
- How much?
- 20 lira.

285
00:58:30,573 --> 00:58:33,315
- Isn't the lift working?
- No.

286
00:59:50,153 --> 00:59:53,191
- How much is this?
- 25 lira.

287
00:59:59,329 --> 01:00:02,799
- Are there any different tunes?
- Sorry, no.

288
01:05:21,918 --> 01:05:25,889
- I called but your number's changed.
- Yes. It has.

289
01:05:30,660 --> 01:05:33,197
- How long have you been here?
- Four months.

290
01:05:33,329 --> 01:05:36,867
Really? It's been a long time.

291
01:05:37,667 --> 01:05:40,705
Haven't you been back
to Istanbul at all?

292
01:05:43,906 --> 01:05:47,547
- When did you get here?
- Yesterday.

293
01:05:48,277 --> 01:05:54,353
- Where are you staying?
- At the Ortadogu Hotel.

294
01:05:59,322 --> 01:06:02,701
- You look well.
- I am.

295
01:06:03,859 --> 01:06:06,032
You look well too.

296
01:06:07,163 --> 01:06:09,643
I guess I'm okay.

297
01:06:21,410 --> 01:06:24,391
- What photographs did you bring?
- What?

298
01:06:24,580 --> 01:06:26,992
What photographs did you bring?

299
01:06:28,084 --> 01:06:31,327
The ones from Kas.

300
01:06:32,421 --> 01:06:38,997
I though I might run into you here.
That's why I brought them with me.

301
01:06:56,445 --> 01:07:00,154
- I got this for you.
- What is it?

302
01:07:00,316 --> 01:07:02,853
I found it wandering around last night.

303
01:07:12,828 --> 01:07:16,605
- It's nice.
- Smuggled in from Dubai.

304
01:07:28,377 --> 01:07:31,358
There's so much smuggled stuff here.

305
01:07:39,355 --> 01:07:41,266
Yes, Cenk?

306
01:07:42,425 --> 01:07:44,837
I gave them to Zafer.

307
01:07:45,227 --> 01:07:47,366
What do you mean they're not there?

308
01:07:47,530 --> 01:07:50,511
Okay, I'll call him right away. Okay.

309
01:07:51,033 --> 01:07:52,512
Sorry.

310
01:07:52,668 --> 01:07:55,205
- Is there a problem?
- No, not really.

311
01:08:08,017 --> 01:08:13,365
Zafer? Don't you have
the Aga's costume for today?

312
01:08:13,656 --> 01:08:16,034
But you told Cenk you didn't have it.

313
01:08:17,359 --> 01:08:20,340
Can't you get anything done?

314
01:08:22,264 --> 01:08:24,301
When are we leaving?

315
01:08:28,337 --> 01:08:30,977
Okay. Okay then.

316
01:08:33,709 --> 01:08:35,382
Sorry.

317
01:08:38,848 --> 01:08:40,725
Shall I get you another tea?

318
01:08:40,883 --> 01:08:44,353
No, I have to go.
I should leave in a minute.

319
01:08:48,023 --> 01:08:50,264
What happened with your thesis?

320
01:08:51,460 --> 01:08:52,962
I still haven't finished it.

321
01:08:53,129 --> 01:08:56,599
I want to photograph
the ruins at lshakpasha tomorrow.

322
01:08:56,732 --> 01:09:00,077
I thought the photos
would look good in the thesis.

323
01:09:00,269 --> 01:09:03,079
I thought I could drop by on you
while I was here.

324
01:09:03,205 --> 01:09:05,151
Right.

325
01:09:09,578 --> 01:09:13,458
Okay. I'll be going then.

326
01:09:13,616 --> 01:09:16,859
- Will I see you this evening?
- Unlikely. We don't get back 'til late.

327
01:09:17,553 --> 01:09:20,329
That's okay. I'll wait up for you.

328
01:09:21,290 --> 01:09:23,566
How much longer are you here?

329
01:09:25,060 --> 01:09:27,131
I don't know.

330
01:09:28,497 --> 01:09:30,636
- Okay.
- What?

331
01:09:31,000 --> 01:09:34,106
- I'm going.
- Okay.

332
01:09:34,703 --> 01:09:37,707
- Give me a call anyway.
- I won't have time.

333
01:09:37,873 --> 01:09:41,377
- I don't have a spare minute.
- All right then, okay.

334
01:09:41,544 --> 01:09:44,423
- Good luck with your work.
- You too.

335
01:10:48,777 --> 01:10:51,781
- You're crew, aren't you?
- Yes.

336
01:10:51,947 --> 01:10:54,427
- Can you give this to Bahar?
- She's in the van.

337
01:10:54,583 --> 01:10:56,563
- That van?
- Yes.

338
01:10:56,685 --> 01:10:58,995
I'll give it to her if you want.

339
01:11:00,656 --> 01:11:03,398
No. It's okay.
I'll do it myself.

340
01:11:32,688 --> 01:11:34,292
Bahar?

341
01:11:35,391 --> 01:11:37,837
Sweetheart. Why are you crying?

342
01:11:44,300 --> 01:11:45,802
Bahar?

343
01:12:25,808 --> 01:12:29,312
Come on, sweetheart.
Why are you crying?

344
01:12:36,752 --> 01:12:38,698
Why are you here?

345
01:12:44,660 --> 01:12:46,867
To see you, of course.

346
01:12:51,166 --> 01:12:52,975
Sweetheart...

347
01:12:53,635 --> 01:12:57,276
I've really changed.
I'm a different person now.

348
01:12:57,439 --> 01:13:00,909
I know I never made
you happy in the past.

349
01:13:03,645 --> 01:13:05,784
But I've really changed.

350
01:13:05,948 --> 01:13:12,058
I say you quit this job and come back
to Istanbul with me tomorrow.

351
01:13:26,435 --> 01:13:28,540
Sweetheart, stop crying.

352
01:14:01,570 --> 01:14:05,347
Bahar, stop crying for God's sake.

353
01:14:08,610 --> 01:14:14,561
I really feel capable of changing.

354
01:14:17,786 --> 01:14:21,393
I feel like I'm ready
to start a new life now.

355
01:14:22,357 --> 01:14:26,362
You know, to get married, have kids.

356
01:14:27,663 --> 01:14:32,009
Even to leave Istanbul
and move somewhere else.

357
01:14:32,134 --> 01:14:34,011
To turn over--

358
01:14:45,681 --> 01:14:48,628
I feel ready
to give up material things...

359
01:14:59,828 --> 01:15:04,038
I don't want anything more for myself.
I'm done with all that.

360
01:15:06,735 --> 01:15:08,612
I know I can--

361
01:15:14,776 --> 01:15:17,723
I know I can make you happy.

362
01:15:21,483 --> 01:15:24,487
I want to ask you something
but answer me honestly.

363
01:15:25,521 --> 01:15:27,091
Sure.

364
01:15:30,792 --> 01:15:33,363
Ask me whatever you want.

365
01:15:34,930 --> 01:15:38,400
Did you see Serap again
after we broke up?

366
01:15:43,105 --> 01:15:45,381
No, of course l didn't.

367
01:15:58,287 --> 01:16:02,565
- Are we going, Ozcan?
- Pretty soon. The fog's lifting.

368
01:16:08,363 --> 01:16:10,434
Bahar, what do you say?

369
01:16:10,899 --> 01:16:15,507
Forget this job.
Come with me to Istanbul tomorrow.

370
01:16:20,676 --> 01:16:23,589
I'm sorry. It's too late now.

371
01:16:47,202 --> 01:16:49,944
Well, fine then. I'm going.

372
01:17:26,675 --> 01:17:31,215
- Here you are. Have a good trip.
- Thanks.

373
01:17:32,581 --> 01:17:35,687
Make sure you're there an hour early.

374
01:17:35,851 --> 01:17:39,230
- What?
- You have to be there an hour early.

375
01:17:43,892 --> 01:17:46,429
- At the airport?
- At the airport, yes.

376
01:17:46,862 --> 01:17:49,468
- Thanks a lot.
- You're welcome.

377
01:17:50,365 --> 01:17:53,209
Is the road to lshakpasha open?

378
01:19:45,380 --> 01:19:48,190
- Are you done?
- Yes.

379
01:19:52,788 --> 01:19:56,167
What a great view
from down here as well.

380
01:19:58,393 --> 01:20:02,637
I should take a photo from here.
Can you stand in the foreground?

381
01:20:03,198 --> 01:20:06,702
It would be nice to have someone
in the picture for a change.

382
01:20:07,769 --> 01:20:09,476
Come a bit closer.

383
01:20:14,509 --> 01:20:16,420
Is this okay?

384
01:20:17,679 --> 01:20:21,126
That's good but don't smile.

385
01:20:23,819 --> 01:20:25,696
Look serious.

386
01:20:26,254 --> 01:20:28,495
Tilt your head forward a bit.

387
01:20:30,225 --> 01:20:32,330
Look more serious.

388
01:20:38,800 --> 01:20:39,744
Look!

389
01:20:40,402 --> 01:20:44,373
- Nice, isn't it?
- Yes, it's a good one.

390
01:20:45,106 --> 01:20:48,849
Is there any chance
you can send me a copy?

391
01:20:54,015 --> 01:20:55,688
Sure.

392
01:20:56,585 --> 01:21:00,032
Write your address
on a piece of paper then.

393
01:21:13,468 --> 01:21:16,415
I'll write it on the back of this.

394
01:21:18,506 --> 01:21:21,919
How long will it take to get here?

395
01:21:24,079 --> 01:21:26,582
A couple of weeks or so.

396
01:21:27,983 --> 01:21:29,621
What are you going to do with it?

397
01:21:29,784 --> 01:21:32,355
Send it to my girlfriend.

398
01:21:33,922 --> 01:21:36,095
Aren't there any photographers
in Dogubeyazit?

399
01:21:36,258 --> 01:21:39,705
There are,
but they do mostly passport photos.

400
01:21:39,861 --> 01:21:43,070
It's nice to have
a scenic photo for once.

401
01:21:43,565 --> 01:21:47,103
I'm putting my phone number
here as well, just in case...

402
01:21:52,908 --> 01:21:54,581
Here you go.

403
01:21:55,777 --> 01:21:59,691
- Sorry for the trouble but...
- No problem.

404
01:22:01,016 --> 01:22:05,192
- Are we going straight back then?
- That's right.

405
01:29:08,076 --> 01:29:10,852
- Good morning.
- Morning.

406
01:29:11,245 --> 01:29:15,193
- You were up early.
- I couldn't sleep.

407
01:29:17,251 --> 01:29:20,425
- Did you manage to get some sleep?
- Yes, a bit.

408
01:29:25,793 --> 01:29:28,137
I was even dreaming.

409
01:29:32,700 --> 01:29:35,738
You forgot your present that day.

410
01:29:35,903 --> 01:29:39,146
I was going to give it
to you in the van but...

411
01:29:48,483 --> 01:29:51,089
I had such a nice dream.

412
01:29:54,222 --> 01:29:55,826
What about?

413
01:29:56,491 --> 01:30:00,940
It was a beautiful day.
Perfect sunshine.

414
01:30:01,095 --> 01:30:05,771
There were these rolling green meadows.

415
01:30:05,933 --> 01:30:08,470
And I could fly.

416
01:30:09,137 --> 01:30:16,715
I was gliding gently over the meadows.
It was so beautiful.

417
01:30:19,213 --> 01:30:25,687
Then I looked down
and saw a cemetery or something.

418
01:30:26,120 --> 01:30:30,193
I flew gently down towards it.

419
01:30:31,392 --> 01:30:35,169
Suddenly I could see my mother.
She was waving at me.

420
01:30:35,563 --> 01:30:40,603
I thought:
"Wow! She's still alive!"

421
01:30:41,903 --> 01:30:47,819
It was really nice.
It made me so happy.

422
01:30:51,312 --> 01:30:54,225
What time do you have to be on set?

423
01:30:58,553 --> 01:31:00,829
You don't want to be late.

424
01:31:13,501 --> 01:31:15,742
At 9:00.

425
01:31:17,071 --> 01:31:21,349
Let's get going then.
I'll buy you a good breakfast.

426
01:31:22,009 --> 01:31:24,922
I can go from there to the airport.

427
01:31:32,887 --> 01:31:34,389
Okay?

428
01:32:59,173 --> 01:33:03,849
Aze, you're not crying again, are you?
Come on. That's enough now.

429
01:33:05,379 --> 01:33:07,985
I won't stop until my father's avenged.

430
01:33:08,149 --> 01:33:12,928
That's going to happen.
Don't worry. I'm here.

431
01:33:13,087 --> 01:33:17,627
I'm going to avenge your father.
I'm going to stop those tears.

432
01:33:18,492 --> 01:33:20,768
We'll be able to walk with pride again.

433
01:33:20,928 --> 01:33:23,408
Can we cut, please?
There's an aircraft.

434
01:33:23,564 --> 01:33:26,602
Oh God! That's all we needed.

435
01:33:26,901 --> 01:33:28,847
- Tell me when it's gone.
- Okay.

436
01:33:29,003 --> 01:33:30,914
A cigarette, someone!

437
01:33:33,541 --> 01:33:38,718
Guys, what's the problem?
You keep forgetting your lines.

438
01:33:38,879 --> 01:33:40,290
Zeynep,
what happened to the rehearsals?

439
01:33:40,448 --> 01:33:41,449
But they only forgot...

440
01:33:41,616 --> 01:33:46,690
Apo, when you put your rifle down,
don't block it.

441
01:33:46,854 --> 01:33:49,892
- Okay.
- Watch out for that.

442
01:35:57,318 --> 01:36:01,061
For my son, Ayaz

