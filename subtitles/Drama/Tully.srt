﻿1
00:00:10,550 --> 00:00:20,240
NESTED proudly presents

2
00:00:21,480 --> 00:00:23,100
Do you know what a night nanny is?

3
00:00:23,100 --> 00:00:23,720
They take care of the baby at night so Mom and Dad can get some sleep.

4
00:00:26,520 --> 00:00:27,840
I don’t want a stranger in my house.

5
00:00:28,460 --> 00:00:29,420
That's like a Lifetime movie.

6
00:00:29,700 --> 00:00:33,780
Where the nanny tries to kill the family and the mom survives and she has to walk with a cane at the end.

7
00:00:34,280 --> 00:00:35,000
Get over yourself.

8
00:00:46,120 --> 00:00:48,120
Mom, what's wrong with your body?

9
00:00:52,620 --> 00:00:53,120
Hello.

10
00:00:55,560 --> 00:00:56,060
I'm Tully.

11
00:00:57,620 --> 00:00:59,700
I'm here to take care of you.

12
00:01:01,360 --> 00:01:03,160
I'm just not used to people doing things for me.

13
00:01:06,840 --> 00:01:08,240
I hold a baby all day.

14
00:01:08,460 --> 00:01:13,200
And then nighttime rolls around and I'm supposed to switch gears. Like, "Hello, I'm all sexy now."

15
00:01:14,700 --> 00:01:15,200
You're empty.

16
00:01:17,340 --> 00:01:17,840
Yeah.

17
00:01:19,920 --> 00:01:21,060
No, you're empty on this side

18
00:01:25,020 --> 00:01:26,740
Your twenties are great.

19
00:01:28,180 --> 00:01:31,520
But then your thirties come around the corner like a garbage truck at 5:00 a.m.

20
00:01:32,300 --> 00:01:32,800
Girls heal.

21
00:01:33,300 --> 00:01:34,100
No, we don't.

22
00:01:34,580 --> 00:01:37,820
We might look like we're all better, but if you look close, we're covered in concealer.

23
00:01:42,460 --> 00:01:45,960
You're convinced that you're this failure, but you actually made your biggest dream come true.

24
00:01:47,480 --> 00:01:51,920
If you want to run off or something, I get that, because I want to do that too sometimes, but I'm not gonna.

25
00:01:55,880 --> 00:01:57,000
I'm here to help you with everything.

26
00:01:57,740 --> 00:01:59,960
You can't fix the parts without treating the hole.

27
00:02:01,620 --> 00:02:02,120
Yeah.

28
00:02:02,280 --> 00:02:04,300
No one's treated my hole in a really long time.

