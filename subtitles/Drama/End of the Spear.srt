1
00:02:09,729 --> 00:02:14,598
1943 Amazon Basin, Ecuador

2
00:03:41,721 --> 00:03:45,623
Moipa!

3
00:04:21,427 --> 00:04:24,294
Father!

4
00:04:34,841 --> 00:04:37,002
Get Gimade!

5
00:06:48,571 --> 00:06:50,133
Dayumae!

6
00:06:50,243 --> 00:06:52,211
Dayumae!

7
00:07:06,225 --> 00:07:09,319
You went back for the stupid bird?

8
00:07:10,563 --> 00:07:13,930
I went back to put Nemo in the earth...

9
00:07:15,268 --> 00:07:18,760
so her spirit won't wander.

10
00:07:20,473 --> 00:07:23,670
Now I follow my father's carvings.

11
00:07:37,390 --> 00:07:39,881
Father! Father!

12
00:07:39,992 --> 00:07:42,984
I was so afraid you were dead.

13
00:07:46,165 --> 00:07:48,725
Where is Nemo?

14
00:08:04,484 --> 00:08:07,044
Mincayani went back for the bird.

15
00:08:07,153 --> 00:08:11,385
Dayumae went back for the
stupid bird. I say we eat him.

16
00:08:18,764 --> 00:08:21,562
There is no food in this place.

17
00:08:21,667 --> 00:08:25,000
If you die, I'll strangle her...

18
00:08:25,104 --> 00:08:27,095
and put her in the earth with you.

19
00:08:36,015 --> 00:08:39,143
Do you want me in the earth with you?

20
00:08:43,956 --> 00:08:46,254
Mincayani, come here.

21
00:08:51,898 --> 00:08:55,163
Take Dayumae into the forest.

22
00:08:56,469 --> 00:08:59,461
Don't let her mother find her.

23
00:09:01,140 --> 00:09:04,576
Are you leaving us?

24
00:09:04,677 --> 00:09:08,010
I must jump the Great Boa,

25
00:09:24,363 --> 00:09:26,797
Next full moon, we'll go back.

26
00:09:29,735 --> 00:09:32,795
I'll never go back.

27
00:09:59,732 --> 00:10:02,166
- No, Dayumae. They'll kill you.
- I'm dead if I stay.

28
00:10:02,268 --> 00:10:06,500
- They'll eat your flesh.
- Then I'll join my father.

29
00:10:10,443 --> 00:10:12,843
Watch out! Shoot!

30
00:10:12,945 --> 00:10:14,378
Shoot!!

31
00:10:14,480 --> 00:10:16,880
I've only one bullet!

32
00:10:18,551 --> 00:10:20,951
There are others!

33
00:10:29,228 --> 00:10:33,358
1956 Shell Mera, Ecuador

34
00:12:16,569 --> 00:12:19,697
Mincayani,
we need wives to make our drink.

35
00:12:21,006 --> 00:12:23,941
We say take them from the Aenomenani.

36
00:12:24,043 --> 00:12:26,273
What do you say?

36
00:12:30,883 --> 00:12:33,613
I say a jaguar ate this monkey.

38
00:12:33,719 --> 00:12:38,452
Maybe the monkey ate himself.

39
00:12:44,630 --> 00:12:48,589
He thinks the Aenomenani
sent a jaguar spirit.

40
00:13:09,555 --> 00:13:11,455
I will spear this jaguar.

41
00:13:16,095 --> 00:13:19,861
We need women, not jaguar.

42
00:13:19,965 --> 00:13:23,230
We should hunt monkeys or wild pigs.

43
00:13:23,335 --> 00:13:25,269
At least something we can eat.

44
00:14:51,223 --> 00:14:54,750
A very big wood bee.

45
00:16:24,116 --> 00:16:28,712
Mincayani's kissing the jaguar.

46
00:22:07,993 --> 00:22:11,986
Dayumae is alive!

47
00:22:12,097 --> 00:22:15,066
Dayumae is alive!

48
00:22:15,167 --> 00:22:19,536
- Dayumae is alive.
- They've eaten her!

49
00:24:46,551 --> 00:24:50,715
We must spear Moipa quickly.

50
00:25:38,336 --> 00:25:41,464
Not the children!

51
00:25:45,810 --> 00:25:49,940
Who has killed Moipa?

52
00:25:53,251 --> 00:25:55,879
Show yourself. Let me see you.

53
00:26:07,866 --> 00:26:09,891
A boy?

54
00:26:10,001 --> 00:26:14,495
A boy has killed Moipa?

55
00:26:28,019 --> 00:26:30,180
We'll take the women with us.

56
00:26:42,133 --> 00:26:45,330
Moipa doesn't deserve to go into the earth.

57
00:26:45,437 --> 00:26:47,496
He's a warrior just like us!

58
00:26:47,606 --> 00:26:50,268
Let the Great Boa judge him, not Nampa!

59
00:26:54,179 --> 00:26:57,876
This one will make my drink.
What do you say?

60
00:28:19,130 --> 00:28:23,590
No. Don't take it.

61
00:28:23,702 --> 00:28:26,603
No. No, don't take it.

62
00:36:37,395 --> 00:36:39,727
Don't be afraid of us!

63 
00:36:40,965 --> 00:36:42,956
The creator God you call Waengongi...

64
00:36:43,067 --> 00:36:44,864
Waengongi has sent us.

65
00:36:44,969 --> 00:36:47,563
We�re just like you.

66
00:37:20,271 --> 00:37:22,831
The creator God is our friend.

67
00:37:22,940 --> 00:37:25,340
What's he talking about?

68
00:37:27,912 --> 00:37:30,472
How does that one sound like a woman?

69
00:37:31,782 --> 00:37:34,615
Even when drinking?

70
00:37:36,387 --> 00:37:38,287
I'll go to them...

71
00:37:39,824 --> 00:37:41,951
and see if Dayumae lives.

72
00:37:42,059 --> 00:37:44,186
The foreigners are cannibals.

73
00:37:44,295 --> 00:37:46,855
I'll go with my sister.

74
00:37:46,964 --> 00:37:49,489
No, I'll go.

75
00:37:49,600 --> 00:37:51,465
Go home to your second wife...

76
00:37:51,569 --> 00:37:54,663
and stay away from my sister!

77
00:37:56,440 --> 00:37:58,465
Enough!

78
00:37:58,576 --> 00:38:00,510
Let's go hunting.

79
00:38:25,569 --> 00:38:27,696
We're just like you.

80
00:38:35,246 --> 00:38:37,510
We're just like you.

81
00:38:59,337 --> 00:39:01,805
Dayumae is my sister.

82
00:39:01,906 --> 00:39:05,501
Can you show me where she is?

83
00:39:05,609 --> 00:39:09,238
Can you take me in the wood bee?

84
00:39:12,049 --> 00:39:15,951
I'll be very happy if you take me.

85
00:39:49,720 --> 00:39:51,711
Take me to Dayumae.

86
00:39:53,657 --> 00:39:56,251
Take me to Dayumae.

87
00:40:10,741 --> 00:40:13,232
Take me to Dayumae.

88
00:40:47,445 --> 00:40:51,211
Mincayani why are you so small,
and I am so fast?

89
00:41:42,566 --> 00:41:45,091
The wood bee flies fast.

90
00:41:49,006 --> 00:41:51,031
Did you see Dayumae?

91
00:41:51,141 --> 00:41:53,939
No, I saw Mincayani and Kimo like ants.

92
00:42:00,951 --> 00:42:03,715
Why don't you take me to Dayumae?

93
00:42:20,004 --> 00:42:22,131
These foreigners understand nothing.

94
00:42:25,309 --> 00:42:27,470
Gimade, where are you going?

95
00:42:30,114 --> 00:42:35,677
I think you're a very tall woman.

96
00:42:35,786 --> 00:42:38,812
You sing like a beautiful canary.

97
00:43:02,079 --> 00:43:05,515
Why's he alone with my sister?

98
00:43:05,616 --> 00:43:07,516
Where's Maengamo?

99
00:43:10,087 --> 00:43:12,351
Where is she?

100
00:43:12,456 --> 00:43:17,155
The foreigners attacked us. We ran!

101
00:43:17,261 --> 00:43:20,025
But we saw you in the wood bee!

102
00:43:21,565 --> 00:43:23,692
I wanted to see Dayumae...

103
00:43:23,801 --> 00:43:25,530
but they already killed her.

104
00:43:28,172 --> 00:43:30,140
Gimade, is this true?

105
00:43:34,278 --> 00:43:38,476
We didn't see her. She must be dead.

106
00:43:41,218 --> 00:43:43,880
Don't the foreigners always kill us?

107
00:43:43,988 --> 00:43:49,290
Why should we spear each other?
I say we spear them now.

108
00:43:49,393 --> 00:43:51,054
Mincayani, what do you say?

109
00:44:01,939 --> 00:44:05,102
Nenkiwi says they've killed Dayumae!

110
00:44:06,810 --> 00:44:09,176
What else does Nenkiwi say?

111
00:44:09,279 --> 00:44:13,181
I say they ate her flesh.
They can't eat if they're dead.

112
00:44:13,283 --> 00:44:17,743
Nenkiwi's right.
We must spear them!

113
00:50:01,865 --> 00:50:04,163
All the foreigners are coming.

114
00:50:04,267 --> 00:50:07,759
- They search for us?
- No. They searched for the others.

115
00:50:07,871 --> 00:50:09,896
They'll never find them.

116
00:50:10,007 --> 00:50:12,532
The river has hid them.

117
00:51:28,885 --> 00:51:33,822
We've angered all the spirits.