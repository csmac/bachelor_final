﻿1
00:06:26,600 --> 00:06:29,649
That poof is back from abroad.

2
00:06:40,680 --> 00:06:43,081
Want a beer'!

3
00:06:46,120 --> 00:06:48,202
Piss off.

4
00:06:48,360 --> 00:06:50,442
The beer...

5
00:08:14,760 --> 00:08:17,001
Feels really weird.

6
00:08:18,440 --> 00:08:21,330
Like a dream or something.

7
00:08:22,280 --> 00:08:27,241
There was a train. It was crazy.

8
00:08:27,400 --> 00:08:29,971
Boys, coffee's ready.

9
00:08:30,120 --> 00:08:32,691
I was, like, inside it

10
00:08:33,800 --> 00:08:35,802
and there was this bridge...

11
00:08:37,560 --> 00:08:41,485
I was inside the train and...

12
00:08:43,320 --> 00:08:47,245
Then it, like, sank to the bottom.

13
00:08:47,400 --> 00:08:50,131
I just can't explain it.

14
00:08:59,160 --> 00:09:01,731
Then don't explain it.

15
00:09:06,040 --> 00:09:09,487
I've poured your coffee.
It's getting cold.

16
00:09:12,600 --> 00:09:15,001
Want a punch?

17
00:09:16,920 --> 00:09:19,730
Hands straight. Straight!

18
00:09:20,040 --> 00:09:22,930
Keep 'em up. Good.

19
00:09:23,560 --> 00:09:26,291
Straight. Follow through.

20
00:09:26,760 --> 00:09:28,683
That's the way.

21
00:09:32,040 --> 00:09:34,281
Swipe. Good.

22
00:09:36,040 --> 00:09:38,930
Good. That's it.

23
00:09:39,080 --> 00:09:40,844
Double. Double.

24
00:09:44,040 --> 00:09:46,850
Quit the fucking prancing.

25
00:10:04,920 --> 00:10:09,005
If the wind could give me wings

26
00:10:09,160 --> 00:10:12,926
I'd leave the darkening earth

27
00:10:13,480 --> 00:10:17,485
I'd soar into the night sky -

28
00:10:17,720 --> 00:10:21,008
up among the brightening stars

29
00:10:23,640 --> 00:10:27,486
If I were young, guess what I'd do.

30
00:10:27,640 --> 00:10:29,881
But you're not.

31
00:10:31,240 --> 00:10:34,130
You're just as old as you feel.

32
00:10:34,440 --> 00:10:36,044
Fuck that!

33
00:10:36,200 --> 00:10:37,645
Fuck that!

34
00:10:53,160 --> 00:10:55,970
If it's her, I'm not in.
- Me neither.

35
00:11:16,680 --> 00:11:18,842
Hi.

36
00:11:19,880 --> 00:11:22,770
Well?
- Come out.

37
00:11:23,400 --> 00:11:25,402
Fuck.

38
00:11:26,440 --> 00:11:29,410
Where?
-just come out.

39
00:11:45,800 --> 00:11:48,849
Did he try to barge in?
- Fuck that.

40
00:11:49,000 --> 00:11:51,890
He's snooping for Raakel,
asking where I am.

41
00:11:52,040 --> 00:11:54,850
I know Raakel well enough.

42
00:11:55,720 --> 00:11:58,121
That Veera woman rang the other day.

43
00:12:10,280 --> 00:12:12,044
And?

44
00:12:12,200 --> 00:12:15,409
I told her you'd gone,
said you were already inside.

45
00:13:44,040 --> 00:13:47,487
Forty, forty-one, forty-two...

46
00:13:47,640 --> 00:13:50,610
Forty-three, forty-four...

47
00:13:56,520 --> 00:13:58,921
Forty-six, forty-seven...

48
00:14:02,280 --> 00:14:03,884
Fifty.

49
00:14:04,680 --> 00:14:07,490
Fifty-one, fifty-two...

50
00:14:09,960 --> 00:14:12,042
Fifty-three.

51
00:14:12,840 --> 00:14:15,571
Fifty-four, fifty-five...

52
00:14:26,760 --> 00:14:29,809
Two hundred and sixty-three.

53
00:14:32,680 --> 00:14:35,650
Add 14 and you get 277.

54
00:14:41,480 --> 00:14:43,881
Days and nights.

55
00:14:47,400 --> 00:14:50,131
277 days.

56
00:14:54,280 --> 00:14:56,851
That's it.

57
00:14:57,800 --> 00:15:00,201
My whole sentence.

58
00:15:01,640 --> 00:15:04,120
It's nothing.

59
00:15:08,680 --> 00:15:11,411
No point counting it then.

60
00:15:15,080 --> 00:15:17,082
What the fuck?

61
00:15:18,120 --> 00:15:20,361
Windows, days.

62
00:15:21,080 --> 00:15:24,368
What's it going to change?
Nothing.

63
00:15:39,480 --> 00:15:41,801
Go get some beer.

64
00:15:42,920 --> 00:15:45,491
And go to the shop
while you're at it.

65
00:18:46,920 --> 00:18:49,651
Imagine all this burning.

66
00:18:51,560 --> 00:18:54,609
What?
- The whole fucking lot.

67
00:18:54,760 --> 00:18:57,730
Light it here and it'd all go up.

68
00:18:57,960 --> 00:19:01,646
There'd be huge flames
flickering at your neck.

69
00:19:01,800 --> 00:19:05,327
Your neck? -It would've
travelled right round the world.

70
00:19:05,800 --> 00:19:07,723
Right.

71
00:19:08,200 --> 00:19:11,170
Right. Goodbye!
- Thanks!

72
00:19:15,720 --> 00:19:18,041
Crack open a beer.

73
00:19:18,840 --> 00:19:20,922
They're my brother's.

74
00:19:21,080 --> 00:19:25,165
Your brother can give you a beer if
he's got you running around after him.

75
00:19:38,040 --> 00:19:40,361
Give us a sip.

76
00:19:42,520 --> 00:19:45,410
Lick that up.
- Fuck off.

77
00:20:10,120 --> 00:20:14,125
He came home last night.
- Who did'!

78
00:20:14,280 --> 00:20:19,491
That fucking poof from your house.
- Okay. So what'!

79
00:20:46,120 --> 00:20:49,329
What was that?
- What?

80
00:21:13,160 --> 00:21:16,528
Why don't you air the room?
- What took you so long?

81
00:21:16,680 --> 00:21:19,081
Catch.
- Bring it.

82
00:22:06,120 --> 00:22:08,202
Listen.
- Yes?

83
00:22:08,360 --> 00:22:10,283
Lend me a fifty.

84
00:22:12,680 --> 00:22:15,081
Fucking listen.
- Haven't got any.

85
00:22:15,240 --> 00:22:17,163
No, really.

86
00:22:19,800 --> 00:22:21,962
Fetch me a towel.

87
00:22:40,120 --> 00:22:43,249
The guys are going to
a cottage in Pusula.

88
00:22:43,720 --> 00:22:45,961
Swimming and everything.

89
00:22:46,680 --> 00:22:49,331
Empty the water.

90
00:23:05,000 --> 00:23:07,401
Listen.

91
00:23:07,560 --> 00:23:10,450
We can't leave him alone now.

92
00:23:11,000 --> 00:23:14,049
Monday he's going... inside.

93
00:23:14,120 --> 00:23:17,090
He might do something to himself.

94
00:23:21,400 --> 00:23:24,370
Why don't you stay with him?

95
00:23:25,960 --> 00:23:29,009
He never talks to me.
-Or me.

96
00:23:30,280 --> 00:23:33,409
Can't you be a brother to him?
just once?

97
00:23:33,640 --> 00:23:35,244
Fuck it.
- Don't say that.

98
00:23:42,440 --> 00:23:45,011
Then give me a hundred.

99
00:23:46,360 --> 00:23:49,569
How often do I ever get out?

100
00:23:50,280 --> 00:23:54,763
I'll leave you some money,
eat at the gas station or something.

101
00:23:54,920 --> 00:24:00,211
You can go to Pusula tomorrow,
or wherever it is you're going.

102
00:24:06,280 --> 00:24:08,203
Simo. Is that Raakel?

103
00:24:43,880 --> 00:24:46,360
You're fucking fat.

104
00:25:19,480 --> 00:25:22,609
Jesus. Humans are humans

105
00:25:22,760 --> 00:25:27,243
but only when they're young
are they night animals.

106
00:25:30,440 --> 00:25:33,330
One of them's got a towel
wrapped round her head.

107
00:25:33,480 --> 00:25:37,963
This one's an older wanderer
with an exceptionally large head.

108
00:25:38,120 --> 00:25:41,806
I wonder whether she's carrying
all her earthly belongings up there

109
00:25:41,960 --> 00:25:44,281
gold or diamonds.

110
00:25:44,440 --> 00:25:47,011
Or maybe it's just grown.

111
00:25:47,160 --> 00:25:50,687
We don't have access
to someone else's head now...

112
00:25:50,760 --> 00:25:52,922
Do we?

113
00:26:34,920 --> 00:26:38,527
Where's she going?
- She's got a new man.

114
00:26:38,600 --> 00:26:40,682
I don't believe you.

115
00:26:47,720 --> 00:26:50,610
She doesn't need you to believe.

116
00:27:01,160 --> 00:27:04,448
Should go get a beer.
- Let's go.

117
00:27:06,280 --> 00:27:10,126
We'd better go then.
- Well let's go then.

118
00:27:10,280 --> 00:27:14,365
They won't serve you.
- I could wait.

119
00:27:14,600 --> 00:27:17,490
What, outside?
- Yeah.

120
00:27:22,840 --> 00:27:24,842
Try this.

121
00:27:30,040 --> 00:27:34,682
I look religious, right?
-Like a fucking Mormon. Give it here.

122
00:27:36,200 --> 00:27:38,282
Give yourself a parting.

123
00:27:42,920 --> 00:27:44,922
Not like that.

124
00:27:45,080 --> 00:27:47,162
Spike it up.

125
00:27:49,960 --> 00:27:52,850
Great.
Now you look like a sparrow.

126
00:28:02,680 --> 00:28:05,001
That's it.

127
00:28:24,120 --> 00:28:27,249
So, where are we going?

128
00:28:27,400 --> 00:28:29,323
The amusement park?

129
00:28:42,600 --> 00:28:45,001
I understand you.

130
00:29:56,680 --> 00:29:59,081
THE WATCHTOWER
RELIGION AND POLITICS

131
00:30:45,000 --> 00:30:46,764
lie...

132
00:30:47,320 --> 00:30:50,449
D'you think I look
like that blond guy?

133
00:30:50,600 --> 00:30:54,207
What fucking blond guy'!
- You know.

134
00:30:56,920 --> 00:30:59,082
<i>In Deer/mater.</i>

135
00:30:59,240 --> 00:31:03,882
Stayed in town playing,
shooting and that.

136
00:31:04,760 --> 00:31:07,411
Walk on ahead and I'll watch.

137
00:32:04,520 --> 00:32:06,841
They're pretty good-looking.

138
00:32:07,000 --> 00:32:09,162
Nah, broads, all of them.

139
00:32:24,680 --> 00:32:26,762
Two pints. Cold.

140
00:32:26,920 --> 00:32:29,571
They'll be whatever
temperature they are.

141
00:32:32,840 --> 00:32:35,571
Remember,
there are two kinds of women.

142
00:32:35,720 --> 00:32:37,961
Some don't give a fuck
about anything -

143
00:32:38,120 --> 00:32:42,284
and some are always trying to save you.
Mine is the latter type.

144
00:32:42,440 --> 00:32:46,286
You mean Veera?
The one that's always ringing us?

145
00:32:46,440 --> 00:32:48,841
She's always smiling.

146
00:32:49,000 --> 00:32:51,890
When she's upset,
then she really smiles.

147
00:32:52,040 --> 00:32:54,281
And she's always upset.

148
00:32:54,440 --> 00:32:59,571
Screw another woman, she gets upset.
Give her a slap, she gets upset.

149
00:32:59,720 --> 00:33:02,769
And always that same
woeful grin on her face.

150
00:33:02,920 --> 00:33:05,491
Have you, like, hit a woman?

151
00:33:06,280 --> 00:33:07,884
You fucking bet I have.

152
00:33:08,040 --> 00:33:09,644
What did she say?

153
00:33:09,800 --> 00:33:11,245
She enjoyed it.

154
00:33:11,400 --> 00:33:14,051
Women enjoy getting slapped.

155
00:33:14,120 --> 00:33:17,806
You can hit women,
but don't hit men unless you have to.

156
00:33:28,680 --> 00:33:30,921
And how old are you?
- He's old enough.

157
00:33:31,080 --> 00:33:34,607
You'll have ID then?
- Yeah. I'll take 'em both, okay'!

158
00:33:34,760 --> 00:33:36,842
Another time.

159
00:33:43,880 --> 00:33:47,009
You can all fuck yourselves!

160
00:33:56,280 --> 00:33:59,170
Quit gawping at yourself.

161
00:34:23,560 --> 00:34:26,769
Your girlfriend'll probably
turn up there too

162
00:34:26,920 --> 00:34:29,571
wherever it is you're going.

163
00:34:29,800 --> 00:34:32,280
Don't you get it? I'm already there.

164
00:36:01,640 --> 00:36:04,928
Left my fucking cigarettes back there.

165
00:36:11,160 --> 00:36:13,970
Red Smarts. Mum's?

166
00:36:14,120 --> 00:36:16,930
Yeah. From her handbag.

167
00:36:17,480 --> 00:36:19,721
She coughs all the fucking time.

168
00:36:19,880 --> 00:36:22,690
She'll end with cancer soon.

169
00:36:26,360 --> 00:36:29,330
You watch her health like a dog.

170
00:36:30,280 --> 00:36:32,362
Hey, Simo.

171
00:36:34,520 --> 00:36:36,841
You're a good guy.

172
00:36:42,920 --> 00:36:46,208
You're a good guy. I mean it.

173
00:37:38,440 --> 00:37:42,604
You know the only thing in the world
you should be afraid of'!

174
00:37:45,400 --> 00:37:47,482
It's hope.

175
00:37:47,640 --> 00:37:50,928
Understand this. Hope.

176
00:37:51,080 --> 00:37:54,368
If you're free of hope,
you're free of everything.

177
00:37:54,520 --> 00:37:58,605
There's nobody stronger
than someone who's lost all hope.

178
00:37:59,720 --> 00:38:03,247
I don't hope much. Normally.

179
00:38:08,120 --> 00:38:13,160
We're going to see all this shit
blown to the wind, into space.

180
00:38:13,800 --> 00:38:16,531
Not a stone unturned.

181
00:38:19,080 --> 00:38:22,926
Burnt to nothing,
just as they predicted.

182
00:38:24,520 --> 00:38:27,888
For billions of years this piece
of shit has been spinning around

183
00:38:28,040 --> 00:38:31,726
first just steam and mud.
You know all this, right?

184
00:38:33,320 --> 00:38:39,089
Then came jellyfish, fish, lizards,
amoebas, all with the same thought:

185
00:38:39,240 --> 00:38:43,564
move forward whatever the cost
and eat everything in their path.

186
00:38:43,720 --> 00:38:47,725
That's why they developed teeth

187
00:38:47,880 --> 00:38:53,330
so they could dodge bigger animals
and chase after the smaller ones.

188
00:38:54,520 --> 00:38:57,888
This is healthy, remember that.

189
00:38:58,040 --> 00:39:01,647
But then something happened,
and it all went to shit.

190
00:39:01,800 --> 00:39:04,201
Humans appeared on earth.

191
00:39:04,760 --> 00:39:06,922
Humans appeared on earth.

192
00:40:26,440 --> 00:40:29,728
Humans are the only animals
that live in the future.

193
00:40:29,880 --> 00:40:32,360
I won't eat this meat today,
I'll eat it tomorrow.

194
00:40:32,520 --> 00:40:35,091
I won't drink today,
I'll drink at Christmas.

195
00:40:35,240 --> 00:40:38,608
I'll screw my girl in 12 years' time
when I'm ready to be a dad.

196
00:40:38,760 --> 00:40:41,331
I won't live today,
I'll live tomorrow.

197
00:40:41,480 --> 00:40:44,689
But Simo, you see,
there is no tomorrow.

198
00:40:45,640 --> 00:40:49,406
No tomorrow.
Not for humans.

199
00:40:50,600 --> 00:40:54,844
The final act will start soon.
Humans will disappear from the stage.

200
00:41:08,520 --> 00:41:10,761
There is no tomorrow.

201
00:41:10,920 --> 00:41:14,129
When humans are extinct,
there'll be a kingdom of scorpions.

202
00:41:14,280 --> 00:41:16,601
It says so in the scriptures.

203
00:41:16,760 --> 00:41:21,482
Scorpions are the only animals
that can survive radioactive fallout.

204
00:41:25,800 --> 00:41:29,009
Scorpions will eat human corpses.

205
00:41:33,080 --> 00:41:36,209
Gouge out little Simo's eyes

206
00:41:37,880 --> 00:41:40,611
guts bleeding out
in front of the shopping centre.

207
00:41:48,360 --> 00:41:52,206
And when they've had enough,
they stab themselves to death.

208
00:41:52,360 --> 00:41:54,681
It's true, they can.

209
00:41:55,640 --> 00:41:58,928
Ready to kill
themselves at any moment.

210
00:42:00,280 --> 00:42:03,011
Humans understand all right.

211
00:42:04,040 --> 00:42:08,284
They watch their own demise
on TV every night.

212
00:42:08,440 --> 00:42:10,920
Who can commit such a skilful suicide?

213
00:42:13,320 --> 00:42:15,721
You needed a piss, right'!

214
00:42:46,520 --> 00:42:51,731
We could join a brigade
or something.

215
00:42:53,240 --> 00:42:55,720
We could save ourselves.

216
00:42:55,880 --> 00:42:59,248
What are you on about?
- We could save ourselves.

217
00:43:03,720 --> 00:43:07,008
Now fuck off.
Got a bit of a situation here.

218
00:43:14,760 --> 00:43:17,969
And what kind of situation is that?

219
00:43:30,120 --> 00:43:32,771
Is this fucking necessary?

220
00:49:17,960 --> 00:49:20,531
You're back then?

221
00:49:25,160 --> 00:49:27,481
Oh, it's you.

222
00:49:29,000 --> 00:49:30,764
You're fucking drunk.

223
00:49:34,120 --> 00:49:36,361
I'm not drunk.

224
00:49:37,320 --> 00:49:39,482
Simo.

225
00:49:44,280 --> 00:49:46,851
Simo, you see...

226
00:49:49,800 --> 00:49:52,690
Why do you always have to...?

227
00:49:54,120 --> 00:49:56,691
Little, little...

228
00:49:58,760 --> 00:50:01,570
Simo, little Simo.

229
00:50:04,440 --> 00:50:07,410
Mummy's little Simo.

230
00:50:10,440 --> 00:50:15,082
Little Simo. Mummy's big...

231
00:50:17,000 --> 00:50:20,686
Big, little. Little, big.

232
00:50:24,680 --> 00:50:27,411
Little Simo.

233
00:50:28,280 --> 00:50:30,931
Mummy's big...

234
00:50:49,720 --> 00:50:52,041
I see.

235
00:50:54,600 --> 00:50:57,331
Get yourself a glass
and you can have a taste.

236
00:50:58,280 --> 00:51:01,648
Viva España!
- Simo!

237
00:51:01,800 --> 00:51:04,201
'Viva' what?

238
00:51:04,360 --> 00:51:08,524
You know.
All the way from here to Rhodes.

239
00:51:08,680 --> 00:51:11,160
Don't be so childish.

240
00:51:15,320 --> 00:51:17,971
Me? You, more like it!

241
00:51:18,440 --> 00:51:21,489
Fuck, she lies to everyone!

242
00:51:21,640 --> 00:51:25,326
Everybody knows
you're not in fucking Rhodes!

243
00:51:25,480 --> 00:51:30,122
Raakel knows, Jusa laughs at you
and Raakel laughs at you.

244
00:51:30,840 --> 00:51:34,208
You can't afford a fucking holiday!

245
00:51:34,680 --> 00:51:37,490
You just hide here, pissed.

246
00:51:37,640 --> 00:51:41,167
Fuck, you've...
you think you've tricked us all.

247
00:51:44,440 --> 00:51:46,920
I am what I am.

248
00:52:17,240 --> 00:52:21,245
Pretty childish.
It was just a few grams.

249
00:52:21,400 --> 00:52:26,201
It was my thing, my fuck-up.
And I'll do the time.

250
00:52:26,360 --> 00:52:29,967
It was supposed to be a shared thing.

251
00:52:30,120 --> 00:52:32,851
We could have done it together.

252
00:52:33,000 --> 00:52:35,082
Nothing we can do about it now.

253
00:52:35,800 --> 00:52:37,882
Sorry.

254
00:52:38,760 --> 00:52:41,161
He's a detective, this pretty boy.

255
00:52:43,720 --> 00:52:45,961
Sorry, sorry...

256
00:52:47,720 --> 00:52:51,088
Take a rest. For a little while.

257
00:52:52,760 --> 00:52:56,207
Upstairs.
- Oh right.

258
00:52:59,480 --> 00:53:02,211
What's that supposed to mean?

259
00:53:02,920 --> 00:53:05,400
Am I going by myself?

260
00:53:55,560 --> 00:53:57,642
Veera...

261
00:54:54,200 --> 00:54:57,966
Do you like cats?
- Dunno.

262
00:55:00,680 --> 00:55:03,251
I'm sure you do.

263
00:55:03,640 --> 00:55:06,291
You're just embarrassed to say so.

264
00:55:06,680 --> 00:55:09,331
It's difficult at your age.

265
00:55:24,280 --> 00:55:27,250
It's the same thing
right through our culture.

266
00:55:29,000 --> 00:55:32,129
Globalisation requires aggression.

267
00:55:33,160 --> 00:55:35,640
Or what do you think?

268
00:55:35,800 --> 00:55:37,962
Maybe.

269
00:55:43,080 --> 00:55:47,085
You're a good kid, but you're evasive.

270
00:55:50,120 --> 00:55:52,361
Doesn't matter.

271
00:55:53,800 --> 00:55:58,362
Rebellion will become
marginalized and criminalized -

272
00:55:58,520 --> 00:56:02,366
or aestheticised.
D'you understand?

273
00:56:04,360 --> 00:56:06,283
No.

274
00:56:07,160 --> 00:56:12,769
I mean, when you draw an erect penis
on a clean concrete wall

275
00:56:12,920 --> 00:56:16,606
the criminal and
the aesthetic worlds combine.

276
00:56:16,760 --> 00:56:18,922
What penis?

277
00:56:20,520 --> 00:56:22,682
That dick.

278
00:56:23,320 --> 00:56:25,402
Over there, next to the stairwell

279
00:56:25,560 --> 00:56:28,131
I didn't draw it.

280
00:56:31,800 --> 00:56:34,121
Feel like a drink?

281
00:56:34,280 --> 00:56:36,362
Sometimes.

282
00:56:44,200 --> 00:56:46,362
I mean now.

283
00:58:13,000 --> 00:58:15,970
I'm thirsty.
- I can get you something.

284
00:58:24,680 --> 00:58:27,729
Your mum lied,
said you'd already gone... inside.

285
00:58:28,680 --> 00:58:30,762
And you just bedded another bloke.

286
00:58:34,840 --> 00:58:36,683
You look pretty rough.

287
00:58:37,640 --> 00:58:39,881
Thanks, you too.

288
00:58:45,480 --> 00:58:49,326
We won't get through this unless
we sort it. -I'll get through this.

289
00:58:51,560 --> 00:58:53,961
What do you want?
- What do I want?

290
00:58:54,920 --> 00:58:58,367
I want you to get down.
Right here. Trousers down.

291
00:58:59,320 --> 00:59:01,004
What?

292
00:59:01,160 --> 00:59:03,242
So you won't then?

293
00:59:03,480 --> 00:59:06,609
What?
- You won't let me fuck you?

294
00:59:22,280 --> 00:59:25,568
Which way?
- On all fours.

295
00:59:27,880 --> 00:59:30,121
You not coming?
- Soon.

296
00:59:32,040 --> 00:59:34,441
Turn this way.

297
00:59:39,720 --> 00:59:42,121
Arse this way.

298
00:59:42,840 --> 00:59:44,763
What?

299
00:59:44,840 --> 00:59:46,763
Arse this way.

300
01:00:00,440 --> 01:00:02,841
Spread your legs.

301
01:00:04,840 --> 01:00:06,922
Wider.

302
01:00:39,560 --> 01:00:41,483
Okay.

303
01:00:42,040 --> 01:00:44,611
You can get up now.

304
01:01:07,960 --> 01:01:10,440
Beautiful, isn't it?

305
01:01:22,920 --> 01:01:27,881
The mystics try to tell us that
everything in reality is okay.

306
01:01:28,680 --> 01:01:31,411
There's nothing twisted about reality.

307
01:01:31,560 --> 01:01:34,928
The only twisted things
are in our minds.

308
01:01:37,640 --> 01:01:41,964
Events don’t justify
negative emotions.

309
01:01:42,120 --> 01:01:45,010
Life is easy and joyous.

310
01:01:46,760 --> 01:01:51,891
It only mistreats
delusions, ambitions

311
01:01:52,840 --> 01:01:55,571
greed, desires.

312
01:01:57,240 --> 01:02:01,484
Negative things exist in us humans -

313
01:02:01,640 --> 01:02:04,041
but not in reality.

314
01:02:10,760 --> 01:02:14,207
If humans disappeared
from this planet, life would carry on.

315
01:02:14,360 --> 01:02:19,400
Nature would flourish
in all its beauty and aggression.

316
01:02:25,720 --> 01:02:30,442
These negative emotions exist in us,
not in reality.

317
01:02:35,800 --> 01:02:39,407
You're probably wondering
why it's so messy in here.

318
01:02:39,560 --> 01:02:41,881
No, I'm not.

319
01:02:45,960 --> 01:02:48,361
Are you afraid of me?

320
01:03:10,920 --> 01:03:13,969
There are two things in this world:

321
01:03:14,120 --> 01:03:17,090
love and fear.

322
01:03:19,400 --> 01:03:23,724
In this world,
there's only one bad thing -

323
01:03:23,880 --> 01:03:26,042
and that's fear.

324
01:03:26,200 --> 01:03:29,329
And there's only one good thing -

325
01:03:29,480 --> 01:03:31,801
and that's love.

326
01:03:34,200 --> 01:03:38,603
Some people call it freedom, joy

327
01:03:38,760 --> 01:03:42,128
God or whatever.

328
01:03:44,520 --> 01:03:47,091
There isn't a single bad thing -

329
01:03:47,240 --> 01:03:51,564
that can't be opened
without revealing fear.

330
01:03:54,120 --> 01:03:56,441
There's always fear.

331
01:03:57,560 --> 01:04:00,450
And when you unravel that fear

332
01:04:01,400 --> 01:04:03,721
you find ignorance.

333
01:04:17,160 --> 01:04:20,209
There's a line in the Bible that says:

334
01:04:20,360 --> 01:04:24,445
'He will find himself
who first loses himself.'

335
01:04:29,480 --> 01:04:34,611
Losing yourself means
understanding who you really are -

336
01:04:35,240 --> 01:04:38,289
so you don't live
as though you're dreaming.

337
01:04:41,000 --> 01:04:45,085
He who speaks doesn't know, and he who
knows doesn't speak. You say something.

338
01:04:45,960 --> 01:04:48,201
<i>We“...</i>

339
01:04:50,760 --> 01:04:53,889
What's your vision of the future?

340
01:04:57,080 --> 01:04:59,242
Vision...?

341
01:04:59,720 --> 01:05:03,964
I mean, when you close your eyes...

342
01:05:05,640 --> 01:05:07,563
So close your eyes.

343
01:05:08,520 --> 01:05:12,206
What do you see?
- Scorpions.

344
01:05:13,240 --> 01:05:16,528
Scorpions?
- Yes.

345
01:05:18,200 --> 01:05:20,771
That's what I see.

346
01:05:20,920 --> 01:05:22,843
Scorpions.

347
01:05:23,720 --> 01:05:27,247
They're the only creature
that can survive radioactive fallout.

348
01:05:27,400 --> 01:05:30,529
It was written, you know.

349
01:05:30,680 --> 01:05:33,251
What are they doing?

350
01:05:34,920 --> 01:05:37,491
The scorpions.

351
01:05:37,640 --> 01:05:40,849
When humans die out

352
01:05:41,000 --> 01:05:44,527
they'll live in our houses
screwing each other.

353
01:05:44,680 --> 01:05:48,526
And once they get pissed off,
they kill themselves with their tails.

354
01:05:50,120 --> 01:05:52,043
Scorpions.

355
01:05:53,320 --> 01:05:56,051
Apocalyptic visions, eh?

356
01:05:58,840 --> 01:06:03,243
So I suppose I shouldn't even bother?
- I should be going.

357
01:06:03,400 --> 01:06:06,609
No, don't.
I want to show you something.

358
01:06:07,800 --> 01:06:10,451
I've got this idea -

359
01:06:11,240 --> 01:06:14,767
of photographing
youth with a garland.

360
01:06:16,040 --> 01:06:18,441
Just the way it is.

361
01:06:19,800 --> 01:06:23,805
Even when it's ugly, it has a garland.

362
01:06:23,960 --> 01:06:26,850
Naked or clothed

363
01:06:27,960 --> 01:06:30,440
angry, crying.

364
01:06:31,640 --> 01:06:34,450
Vomiting, shitting.

365
01:06:35,960 --> 01:06:40,204
In the kitchen, on the toilet,
watching TV, shooting up.

366
01:06:40,360 --> 01:06:42,601
D'you see?

367
01:06:44,520 --> 01:06:46,841
And I've got this old helmet

368
01:06:47,000 --> 01:06:49,321
found it in the woods.

369
01:06:51,480 --> 01:06:55,246
So I decided to photograph
youth with a helmet.

370
01:07:00,360 --> 01:07:04,046
Like a soldier.
That's what they turn you into.

371
01:07:14,600 --> 01:07:17,001
Let me just...

372
01:07:19,720 --> 01:07:21,961
There we go.

373
01:07:39,480 --> 01:07:42,609
I'll just move you a little.

374
01:07:44,680 --> 01:07:47,809
Against the white... Good.

375
01:08:14,440 --> 01:08:16,442
Good.

376
01:08:24,520 --> 01:08:27,091
Take off your shirt.

377
01:08:27,720 --> 01:08:29,324
Fucking hell.

378
01:08:32,280 --> 01:08:34,760
Fucking poof.

379
01:08:34,920 --> 01:08:38,129
Let's not start that old stuff again.
just let me...

380
01:08:43,560 --> 01:08:46,609
it can't be all that silly. Let me...

381
01:08:46,760 --> 01:08:48,364
Stay there.

382
01:08:51,560 --> 01:08:53,244
It can't be that silly. Let me...

383
01:12:33,400 --> 01:12:35,880
I warned you.

384
01:14:51,640 --> 01:14:54,769
Jesus. A morning like this.

385
01:14:54,920 --> 01:14:58,686
Like after a sauna, isn't it.

386
01:14:59,400 --> 01:15:01,641
For the soul, I mean.

387
01:15:02,760 --> 01:15:04,762
Oh yes.

388
01:15:05,560 --> 01:15:07,483
Oh yes.

389
01:15:10,600 --> 01:15:13,410
There's this old woman

390
01:15:13,560 --> 01:15:15,483
wandering the streets at night.

391
01:15:15,800 --> 01:15:19,885
She's got a towel
wrapped round her head.

392
01:15:21,480 --> 01:15:26,202
And beneath that towel,
she has an exceptionally large head.

393
01:15:29,560 --> 01:15:33,246
I wonder what she's carrying around?

394
01:15:33,400 --> 01:15:36,131
Gold, diamonds, share deeds

395
01:15:36,280 --> 01:15:40,444
all wrapped up inside that towel.

396
01:15:43,880 --> 01:15:47,089
What if she's just very wise?

397
01:15:47,240 --> 01:15:52,849
We don't have access
to someone else's head now, do we?

398
01:15:54,920 --> 01:15:58,208
So... Where are you going?

399
01:15:59,560 --> 01:16:03,406
To Pusula, a friend's place...

400
01:16:04,200 --> 01:16:06,123
To Pusula.

401
01:16:13,320 --> 01:16:16,927
You're running away,
now we're talking man to man.

402
01:16:17,080 --> 01:16:19,401
Running away.

403
01:16:22,840 --> 01:16:26,367
I've got to go.
- Running away from mummy.

404
01:16:27,800 --> 01:16:30,280
Saw it straight away.

405
01:16:30,440 --> 01:16:32,681
I can tell.

406
01:16:34,760 --> 01:16:37,889
You've done your bad deeds,
looking for glory.

407
01:16:38,040 --> 01:16:41,328
A man's deeds, and now you're running.

408
01:16:49,160 --> 01:16:52,050
Don't you ever do that again.

409
01:16:58,920 --> 01:17:03,562
If you had any sense,
you'd run back to mummy.

410
01:17:03,720 --> 01:17:06,121
But you don't.

411
01:19:03,320 --> 01:19:06,847
Where's your shirt?
- Fuck.

412
01:19:07,720 --> 01:19:10,530
What's the matter?
- I'm cold.

413
01:19:17,080 --> 01:19:19,242
Simo...

414
01:19:21,000 --> 01:19:24,686
Leave him.
- But something's not right.

415
01:19:25,800 --> 01:19:28,929
I'll make some coffee
and fry you an egg.

416
01:19:29,320 --> 01:19:32,767
I'll add some sausage.
It'll be more filling.

417
01:20:12,680 --> 01:20:15,490
You're not ill, are you?
- Don't!

418
01:20:25,640 --> 01:20:29,167
Good God. There's police outside.

419
01:20:29,320 --> 01:20:32,688
Is it because of that man?
The one Simo knew?

420
01:20:32,840 --> 01:20:34,922
I didn't know him.
- Has he died?

421
01:20:35,080 --> 01:20:37,811
Yes.
- Topped himself?

422
01:20:38,600 --> 01:20:42,366
The scorpions.
- What fucking scorpions?

423
01:20:45,640 --> 01:20:48,041
What you said yesterday...

424
01:20:48,840 --> 01:20:52,128
They'll come and eat human corpses.

425
01:20:52,280 --> 01:20:54,442
You said.

426
01:20:55,320 --> 01:20:59,245
Have I been talking about scorpions?
- Well, that they're coming

427
01:20:59,480 --> 01:21:02,290
and everything will change.

428
01:21:04,680 --> 01:21:07,729
So nothing really matters.

429
01:21:08,680 --> 01:21:11,411
Humans don't matter.

430
01:21:13,080 --> 01:21:16,129
Humans will die out.

431
01:21:16,280 --> 01:21:17,805
What the hell?

432
01:21:17,960 --> 01:21:21,328
Because the scorpions are coming.

433
01:21:22,120 --> 01:21:25,169
And the end is coming

434
01:21:25,320 --> 01:21:29,962
and if you still hope,
you can never be free.

435
01:21:35,240 --> 01:21:37,481
I don't get it.

436
01:21:58,200 --> 01:22:00,441
Simo...