1
00:03:50,200 --> 00:03:52,077
Ru�a, this is my niece Vera.

2
00:03:53,920 --> 00:03:55,273
She's a hard-working girl.

3
00:03:55,440 --> 00:03:57,396
She could replace Lidia.

4
00:03:57,600 --> 00:04:00,353
I decide who we hire here, Mila.

5
00:04:01,720 --> 00:04:03,995
Hurry up, we've got a lot to do.

6
00:04:29,760 --> 00:04:31,034
There's one too many.

7
00:04:31,520 --> 00:04:32,748
It's a gift.

8
00:04:33,520 --> 00:04:35,351
I don't need any gifts.

9
00:04:36,280 --> 00:04:37,380
Thank you.

10
00:05:29,560 --> 00:05:30,660
8.30, please.

11
00:05:32,720 --> 00:05:33,820
Thank you.

12
00:05:40,800 --> 00:05:41,900
Bon app�tit.

13
00:05:52,040 --> 00:05:53,519
Is the butcher sick again?

14
00:05:54,160 --> 00:05:55,275
15.50.

15
00:06:00,920 --> 00:06:03,309
Too much meat is bad for you anyway.

16
00:06:03,920 --> 00:06:05,020
Thank you.

17
00:06:08,720 --> 00:06:10,870
Slovenia played badly last night.

18
00:06:11,040 --> 00:06:12,439
That's not true.

19
00:06:12,720 --> 00:06:14,278
You've never understood
anything about football.

20
00:06:14,480 --> 00:06:16,277
They played terribly
and that's that!

21
00:06:16,720 --> 00:06:17,820
That was offside.

22
00:06:17,840 --> 00:06:19,193
You're blind.

23
00:06:19,360 --> 00:06:21,715
Weren't you at the eye doctor's last week?

24
00:06:24,600 --> 00:06:25,828
There we go!

25
00:06:26,000 --> 00:06:27,274
They're going to score.

26
00:06:27,520 --> 00:06:29,397
Goal!

27
00:06:34,000 --> 00:06:35,797
Time to go, gentlemen.

28
00:07:12,760 --> 00:07:13,860
<i>Out of the 7 inhabitants...</i>

29
00:07:13,920 --> 00:07:15,751
<i>who were all asleep...</i>

30
00:07:15,920 --> 00:07:17,831
<i>only the little girl survived.</i>

31
00:07:19,120 --> 00:07:21,793
<i>Here is the traffic news on Radio 24...</i>

32
00:07:37,440 --> 00:07:38,540
Okay.

33
00:07:39,120 --> 00:07:40,235
You'll easily make your way from here.

34
00:07:41,240 --> 00:07:42,340
Thanks.

35
00:08:42,880 --> 00:08:44,438
Can you spare any change?

36
00:08:44,640 --> 00:08:45,740
Spare any change?

37
00:08:56,400 --> 00:08:58,038
Spare any change, please?

38
00:09:05,200 --> 00:09:07,156
Cool, thanks a lot.

39
00:09:10,000 --> 00:09:11,991
Excuse me, you got a cigarette?

40
00:09:17,680 --> 00:09:18,780
Turkish?

41
00:09:19,080 --> 00:09:20,180
Bosnian.

42
00:09:20,960 --> 00:09:22,060
Thank you very much.

43
00:09:23,760 --> 00:09:25,079
What are you up to tonight?

44
00:09:32,240 --> 00:09:35,118
Were you in Sarajevo
during the siege?

45
00:09:36,360 --> 00:09:37,460
Yes.

46
00:09:39,480 --> 00:09:40,833
It was shit.

47
00:09:44,920 --> 00:09:46,751
I was young at the time.

48
00:09:47,760 --> 00:09:49,079
My grandma says...

49
00:09:49,240 --> 00:09:51,708
I always stopped...

50
00:09:51,920 --> 00:09:53,797
to look at the dead bodies.

51
00:09:56,040 --> 00:09:58,315
It's really tough, a war like that!

52
00:11:40,280 --> 00:11:41,380
Damn!

53
00:11:48,920 --> 00:11:50,353
What were you looking at?

54
00:12:04,600 --> 00:12:06,192
I'll be fine in a minute.

55
00:12:06,400 --> 00:12:07,879
It doesn't look good.

56
00:12:13,440 --> 00:12:14,668
14.50.

57
00:12:22,200 --> 00:12:23,300
Mila!

58
00:12:27,080 --> 00:12:28,180
Just a minute.

59
00:12:28,240 --> 00:12:29,355
I'll take care of it.

60
00:12:36,200 --> 00:12:37,300
Here you are.

61
00:12:44,600 --> 00:12:45,874
Wait.

62
00:12:48,560 --> 00:12:49,660
Here.

63
00:12:50,600 --> 00:12:51,700
For your work.

64
00:12:52,160 --> 00:12:53,388
I just wanted to help.

65
00:12:55,960 --> 00:12:57,234
I could use...

66
00:12:58,320 --> 00:13:00,151
someone like you.

67
00:13:01,080 --> 00:13:03,389
No thanks,
I'm not from here.

68
00:15:05,040 --> 00:15:06,519
And then I finished...

69
00:15:07,760 --> 00:15:09,398
my studies in New York.

70
00:15:09,600 --> 00:15:11,955
- In New York?
- Yes.

71
00:15:13,920 --> 00:15:17,117
My brother and I
always wanted to go there.

72
00:15:20,440 --> 00:15:21,540
So?

73
00:15:23,080 --> 00:15:24,229
Why didn't you go?

74
00:15:26,240 --> 00:15:28,754
He killed himself after the war.

75
00:15:33,000 --> 00:15:34,100
Cheers.

76
00:15:35,560 --> 00:15:38,028
�ivjeli means "to life!"

77
00:15:39,200 --> 00:15:40,300
To life!

78
00:15:52,320 --> 00:15:53,420
Do you live alone?

79
00:15:55,800 --> 00:15:56,900
I think...

80
00:15:57,800 --> 00:15:58,994
I'd better get home.

81
00:16:01,600 --> 00:16:02,953
When can we meet again?

82
00:16:04,360 --> 00:16:06,874
I'm sorry, really, sorry.

83
00:16:22,880 --> 00:16:23,980
Good girl!

84
00:16:27,520 --> 00:16:28,620
Heel.

85
00:16:33,600 --> 00:16:34,700
Damn.

86
00:16:36,640 --> 00:16:38,278
What do you want?

87
00:16:38,560 --> 00:16:39,660
Get lost.

88
00:16:40,200 --> 00:16:41,300
Leave me alone.

89
00:17:05,280 --> 00:17:07,999
I can't tell you anything new
about your condition.

90
00:17:13,840 --> 00:17:16,957
You're better
but the symptoms will come back.

91
00:17:17,160 --> 00:17:19,151
- Ms. Hanovic.
- Tanovic!

92
00:17:22,560 --> 00:17:25,711
You have to get yourself
to a hospital.

93
00:17:26,400 --> 00:17:28,470
In Sarajevo they gave me pills.

94
00:17:29,840 --> 00:17:32,593
Yes, but that's not enough
to treat your condition.

95
00:17:37,520 --> 00:17:39,158
Is there anyone who could be a...

96
00:17:39,360 --> 00:17:41,715
bone marrow donor for you?

97
00:17:47,320 --> 00:17:48,753
Do you have any siblings?

98
00:18:27,840 --> 00:18:29,558
I'm alive!

99
00:18:51,120 --> 00:18:53,190
How long
have you been working here?

100
00:18:53,360 --> 00:18:54,460
From the very beginning.

101
00:18:56,360 --> 00:18:59,079
We've been through a lot together,
me and Ru�a.

102
00:19:01,400 --> 00:19:03,152
But I'm not staying much longer.

103
00:19:04,080 --> 00:19:05,718
We're going back home
to Croatia next year.

104
00:19:07,520 --> 00:19:08,748
Look, it's on the coast.

105
00:19:09,600 --> 00:19:11,113
It'll be lovely... one day.

106
00:19:12,960 --> 00:19:14,837
My Ante is building it.

107
00:19:15,080 --> 00:19:17,753
But he's hurt his foot and can't work.

108
00:19:20,440 --> 00:19:21,919
Don't bother.

109
00:19:22,160 --> 00:19:24,151
She won't appreciate it.

110
00:19:24,320 --> 00:19:26,197
It looks prettier this way.

111
00:19:28,160 --> 00:19:30,116
Watch out Fredi!

112
00:19:31,760 --> 00:19:33,637
Where did you learn German?

113
00:19:34,280 --> 00:19:36,191
In Sarajevo.
At school.

114
00:19:36,440 --> 00:19:37,668
During the war?

115
00:19:37,920 --> 00:19:39,020
So?

116
00:19:40,920 --> 00:19:43,229
I don't understand you.

117
00:19:43,480 --> 00:19:45,596
What are you doing in this canteen?

118
00:19:46,280 --> 00:19:47,380
Making money...

119
00:19:48,080 --> 00:19:50,196
and anyway, I won't stay long.

120
00:19:50,560 --> 00:19:52,755
That's what we all said!

121
00:19:54,680 --> 00:19:58,070
And you young people
travel for fun these days.

122
00:19:59,200 --> 00:20:02,351
We left home to earn money.

123
00:20:03,280 --> 00:20:04,633
You're spoiled.

124
00:20:14,320 --> 00:20:15,719
Finished?

125
00:20:16,600 --> 00:20:18,113
Are you new here?

126
00:20:18,440 --> 00:20:19,540
Looks like it.

127
00:20:19,680 --> 00:20:20,795
Where are you from?

128
00:20:21,960 --> 00:20:23,109
Bosnia.

129
00:20:31,680 --> 00:20:32,999
Ana! Come here.

130
00:20:37,360 --> 00:20:39,828
You're here to work not chat.

131
00:20:40,440 --> 00:20:41,668
That goes for everyone.

132
00:20:42,640 --> 00:20:43,740
OK.

133
00:20:44,000 --> 00:20:46,116
Why do you speak German to me, Ru�a?

134
00:20:46,960 --> 00:20:48,678
You're from Belgrade, aren't you?

135
00:20:49,120 --> 00:20:50,633
Number two, please.

136
00:20:53,360 --> 00:20:54,460
18, here you are.

137
00:20:54,480 --> 00:20:55,629
Hey you, come over here!

138
00:21:00,240 --> 00:21:01,389
Does this belong to one of you?

139
00:21:01,640 --> 00:21:02,740
Thank you.

140
00:21:03,080 --> 00:21:04,308
Where do you come from?

141
00:21:04,520 --> 00:21:05,620
Sarajevo.

142
00:21:05,680 --> 00:21:06,999
How long have you been here?

143
00:21:07,200 --> 00:21:08,300
A few days.

144
00:21:08,560 --> 00:21:10,073
See, she's only just arrived and speaks...

145
00:21:10,280 --> 00:21:12,236
better German than you do after 40 years.

146
00:21:12,640 --> 00:21:13,789
What's life there like these days?

147
00:21:18,200 --> 00:21:19,952
Go and see for yourselves!

148
00:21:46,800 --> 00:21:47,915
Where shall I put it?

149
00:21:54,960 --> 00:21:57,269
You don't need to bring me my food.

150
00:21:59,360 --> 00:22:01,351
Leave it in the kitchen in future.

151
00:22:02,040 --> 00:22:04,395
See you tomorrow then.

152
00:22:05,120 --> 00:22:06,394
Have a good night.

153
00:22:06,600 --> 00:22:08,795
Are you going to sort out your papers?

154
00:22:10,440 --> 00:22:11,953
I have to register you.

155
00:22:13,920 --> 00:22:15,148
I'm not sure...

156
00:22:16,000 --> 00:22:17,513
how long I'll stay.

157
00:23:08,320 --> 00:23:10,550
I met Pero today.

158
00:23:11,560 --> 00:23:13,835
The workers have been booked
to do the roof.

159
00:23:14,920 --> 00:23:16,020
When for?

160
00:23:16,240 --> 00:23:17,832
When we're there.

161
00:23:19,480 --> 00:23:21,710
I need to keep an eye on things...

162
00:23:23,160 --> 00:23:24,239
I thought we wouldn't...

163
00:23:24,240 --> 00:23:26,231
be able to go this year because of your leg.

164
00:23:26,520 --> 00:23:28,397
Do you think I can just leave it

165
00:23:28,600 --> 00:23:29,794
to the workers?

166
00:23:34,720 --> 00:23:35,820
So once again Mila has to cook...

167
00:23:36,040 --> 00:23:37,140
and clean up after everybody.

168
00:23:37,320 --> 00:23:38,420
When will I get a chance to relax?

169
00:23:38,760 --> 00:23:40,318
Calm down dear,

170
00:23:41,200 --> 00:23:43,236
we need to get our house finished.

171
00:23:43,760 --> 00:23:45,193
Where can we go back to otherwise?

172
00:24:18,760 --> 00:24:22,275
The younger one's thinking
about coming with us.

173
00:24:23,560 --> 00:24:26,120
But Miro will have
a family of his own soon.

174
00:24:28,080 --> 00:24:31,117
You can't just get up and go
once there's a family.

175
00:24:34,120 --> 00:24:35,220
Ru�a doesn't want us
to throw those away.

176
00:24:35,800 --> 00:24:37,711
Rinse them and chop them up with the rest.

177
00:24:44,160 --> 00:24:45,593
I didn't forget.

178
00:25:00,520 --> 00:25:02,238
Hey, you're bleeding!

179
00:25:11,240 --> 00:25:13,231
She's got a nosebleed.

180
00:25:13,400 --> 00:25:14,500
Go and wash your hands!

181
00:25:22,600 --> 00:25:24,033
I've seen it all before,

182
00:25:25,200 --> 00:25:26,349
birthdays.

183
00:25:26,600 --> 00:25:27,679
What do you mean?

184
00:25:27,680 --> 00:25:29,716
- Is it her birthday?
- Yeah.

185
00:25:30,440 --> 00:25:31,540
Aren't we going to celebrate?

186
00:25:31,680 --> 00:25:33,716
Fr�ulein and celebrate!?

187
00:25:33,880 --> 00:25:35,871
What if we throw a party for Ru�a?

188
00:25:36,280 --> 00:25:37,599
Now you've completely lost it!

189
00:25:37,800 --> 00:25:39,756
She might like it.

190
00:25:40,200 --> 00:25:41,300
Forget it!

191
00:25:42,160 --> 00:25:43,559
She'll fire us.

192
00:25:44,760 --> 00:25:46,273
You first.

193
00:25:59,200 --> 00:26:01,077
What the hell's going on?

194
00:26:04,440 --> 00:26:06,078
Get out.

195
00:26:07,360 --> 00:26:09,555
Now, out.

196
00:26:10,080 --> 00:26:12,230
Happy Birthday, Ru�a.

197
00:26:12,760 --> 00:26:14,591
Today we're celebrating Ru�a.

198
00:26:59,800 --> 00:27:00,900
Ru�a!

199
00:27:49,160 --> 00:27:51,230
Do you want to dance, Ru�a?

200
00:28:11,280 --> 00:28:14,113
Look at her, the little Bosnian.

201
00:28:14,520 --> 00:28:16,476
She isn't afraid of anything.

202
00:28:16,800 --> 00:28:18,836
Come on, let's dance!
Come on.

203
00:28:19,000 --> 00:28:21,070
- Franz, I'm too old.
- Don't be silly!

204
00:30:41,880 --> 00:30:43,029
Stay with me.

205
00:31:49,920 --> 00:31:53,151
Morning, beautiful.

206
00:31:55,840 --> 00:31:57,478
I overslept.

207
00:32:02,160 --> 00:32:05,596
It certainly took a lot of wine and music...

208
00:32:05,760 --> 00:32:08,035
to finally let me stay over.

209
00:32:08,240 --> 00:32:09,593
Don't you have to go to work?

210
00:32:09,840 --> 00:32:11,478
We can go together.

211
00:32:11,680 --> 00:32:12,759
I'll hurry up.

212
00:32:12,760 --> 00:32:14,352
I have to leave now.

213
00:32:15,440 --> 00:32:16,540
Make sure you lock the door properly.

214
00:32:30,520 --> 00:32:31,620
You're late.

215
00:32:32,440 --> 00:32:34,351
Here, we work the same after parties.

216
00:32:47,280 --> 00:32:48,759
Tired after all the dancing?

217
00:32:51,800 --> 00:32:52,994
And 10.

218
00:33:00,200 --> 00:33:01,349
How are you?

219
00:33:03,280 --> 00:33:04,380
7.30.

220
00:33:45,120 --> 00:33:46,220
What are you doing here?

221
00:33:47,440 --> 00:33:49,271
I missed you.

222
00:34:07,000 --> 00:34:08,638
I can't just sell you...

223
00:34:08,840 --> 00:34:10,512
a prescription drug.

224
00:34:10,720 --> 00:34:12,551
It's not allowed in this country.

225
00:34:12,920 --> 00:34:15,275
I've taken it before,
it's nothing special.

226
00:34:15,520 --> 00:34:17,192
You've come to the wrong place.

227
00:34:17,440 --> 00:34:18,668
Go and see a doctor.

228
00:34:51,600 --> 00:34:53,477
8.30, please.

229
00:34:57,960 --> 00:34:59,791
Thanks. Bye.

230
00:35:14,240 --> 00:35:16,117
What are you still doing here?

231
00:35:18,280 --> 00:35:19,554
I'm working.

232
00:35:19,920 --> 00:35:21,751
Finish, I'm locking up.

233
00:36:26,280 --> 00:36:28,111
Do you have a brother or a sister?

234
00:36:32,560 --> 00:36:33,754
A brother.

235
00:36:37,400 --> 00:36:38,628
Where does he live?

236
00:36:40,520 --> 00:36:41,620
In Belgrade.

237
00:36:45,320 --> 00:36:46,799
Do you see each other often?

238
00:37:30,560 --> 00:37:32,278
He still plays the piano every day.

239
00:37:32,480 --> 00:37:35,711
His repertoire covers everything
from jazz to Chopin.

240
00:37:36,000 --> 00:37:37,100
Question:

241
00:37:37,120 --> 00:37:40,590
What was Chopin's first name?

242
00:37:43,800 --> 00:37:45,074
He doesn't even know!

243
00:37:48,560 --> 00:37:49,993
Come on, say something!

244
00:37:50,160 --> 00:37:51,479
- So?
- Henry Chopin.

245
00:37:51,680 --> 00:37:54,990
- Henry Chopin.
- Wolfgang, Sebastian.

246
00:37:58,640 --> 00:38:00,278
This question goes
to you too, Mr. Remund.

247
00:38:00,480 --> 00:38:02,789
His name was Frederic Chopin.

248
00:40:40,880 --> 00:40:42,757
Did you find the towels?

249
00:40:50,000 --> 00:40:51,100
Is everything okay?

250
00:40:53,040 --> 00:40:55,395
You remind me of my brother Adis!

251
00:40:59,000 --> 00:41:01,514
These are his milk teeth.

252
00:41:13,520 --> 00:41:14,953
Are you falling sick?

253
00:41:16,720 --> 00:41:18,199
If you need anything,

254
00:41:18,440 --> 00:41:19,759
I've got everything:

255
00:41:21,080 --> 00:41:22,308
Vitamin C,

256
00:41:23,800 --> 00:41:27,429
Aspirin, throat lozenges.

257
00:41:29,320 --> 00:41:30,514
Help yourself.

258
00:41:34,000 --> 00:41:35,718
I'm a hypochondriac.

259
00:41:59,600 --> 00:42:01,238
Where do you work?

260
00:42:06,640 --> 00:42:07,755
Can I have your phone number?

261
00:42:10,000 --> 00:42:12,195
Thanks for looking after me.

262
00:42:13,080 --> 00:42:14,911
I'd like to look after you again.

263
00:42:23,200 --> 00:42:24,300
No sugar.

264
00:42:55,200 --> 00:42:58,033
240.

265
00:43:01,320 --> 00:43:03,311
200, 300.

266
00:43:11,160 --> 00:43:12,260
Come in.

267
00:43:19,360 --> 00:43:21,351
Time flies, doesn't it?

268
00:43:21,760 --> 00:43:22,860
Thank you.

269
00:43:24,160 --> 00:43:25,239
I'm off then.

270
00:43:25,240 --> 00:43:26,468
Don't you want to count it?

271
00:43:26,880 --> 00:43:28,154
I'm sure it's fine.

272
00:43:28,840 --> 00:43:31,400
If there's one thing you're good at,
it's counting.

273
00:43:42,680 --> 00:43:44,796
You're doing a great job.

274
00:43:45,880 --> 00:43:46,980
You've adjusted quickly.

275
00:43:49,480 --> 00:43:50,993
Well. Adjusted.

276
00:44:01,480 --> 00:44:03,118
I want to go to the mountains.

277
00:44:55,600 --> 00:44:56,828
What are you scared of?

278
00:44:57,560 --> 00:44:59,790
Are you afraid
I'll push you or I'll jump?

279
00:45:03,120 --> 00:45:06,078
I don't like that emptiness down there.

280
00:45:14,200 --> 00:45:16,395
Don't you want to know
what the war was like?

281
00:45:17,400 --> 00:45:19,118
Everybody asks me about it.

282
00:45:19,760 --> 00:45:20,875
Well, only if...

283
00:45:21,080 --> 00:45:23,071
you want to tell me something about it?

284
00:45:24,560 --> 00:45:25,913
Not really.

285
00:45:53,000 --> 00:45:54,831
Are you afraid of dying Ru�a?

286
00:45:57,720 --> 00:45:59,472
What kind of a question is that?

287
00:46:01,200 --> 00:46:03,475
It's the only thing I'm scared of!

288
00:46:04,200 --> 00:46:06,077
How silly!

289
00:46:07,120 --> 00:46:08,951
At your age!

290
00:46:09,680 --> 00:46:11,511
It's beautiful up here.

291
00:46:14,680 --> 00:46:16,238
I wonder what it looks like in summer.

292
00:46:19,160 --> 00:46:20,673
There's no snow.

293
00:46:28,920 --> 00:46:30,638
Ana, Ana, no.

294
00:47:17,360 --> 00:47:18,998
A hot punch, please.

295
00:47:19,240 --> 00:47:20,878
And for your daughter?

296
00:47:22,800 --> 00:47:24,995
- The same.
- Two punches.

297
00:47:28,760 --> 00:47:30,637
I ordered you a punch.

298
00:47:31,680 --> 00:47:32,780
Thank you.

299
00:47:34,720 --> 00:47:37,188
- How old are you?
- Guess.

300
00:47:40,720 --> 00:47:41,820
Twenty...

301
00:47:44,680 --> 00:47:45,780
...two.

302
00:47:45,880 --> 00:47:47,108
Not bad.

303
00:47:51,880 --> 00:47:52,995
- Cheers.
- Thank you.

304
00:47:56,000 --> 00:47:57,433
I was 22, too...

305
00:47:57,680 --> 00:47:59,352
when I came from Yugoslavia.

306
00:48:02,840 --> 00:48:05,115
Nobody calls it Yugoslavia anymore.

307
00:48:14,800 --> 00:48:16,233
Do you miss it?

308
00:48:19,400 --> 00:48:20,500
No.

309
00:48:23,520 --> 00:48:24,919
Do you miss Bosnia?

310
00:48:31,200 --> 00:48:33,475
When were you there last?

311
00:48:35,640 --> 00:48:37,471
When my mother died.

312
00:48:42,520 --> 00:48:43,748
You know...

313
00:48:45,800 --> 00:48:47,358
I've been here...

314
00:48:48,640 --> 00:48:50,437
for almost 25 years.

315
00:48:52,160 --> 00:48:54,116
A lot of people left the country back then...

316
00:48:55,160 --> 00:48:57,230
Even though we didn't have a bad life.

317
00:48:58,080 --> 00:49:00,753
But I wanted to live better...

318
00:49:01,440 --> 00:49:04,238
to be more independent.

319
00:49:05,520 --> 00:49:07,431
I was ambitious,

320
00:49:07,680 --> 00:49:08,999
had plans...

321
00:49:12,600 --> 00:49:14,192
and I was in love.

322
00:49:19,160 --> 00:49:20,260
And?

323
00:49:24,760 --> 00:49:26,398
He was supposed to come, too.

324
00:49:28,280 --> 00:49:29,380
But...

325
00:49:31,320 --> 00:49:32,469
...he didn't.

326
00:49:38,480 --> 00:49:41,870
- Cheers.
- Cheers.

327
00:49:49,680 --> 00:49:51,955
Didn't you ever want to go back?

328
00:49:52,840 --> 00:49:53,940
No.

329
00:49:57,800 --> 00:49:59,472
But sometimes...

330
00:50:00,960 --> 00:50:02,552
I wake up in the middle of the night...

331
00:50:03,880 --> 00:50:05,029
and I'm scared.

332
00:50:09,760 --> 00:50:12,479
I don't know where I want to be buried.

333
00:50:20,080 --> 00:50:22,116
What difference does it make
when you're dead!

334
00:50:30,120 --> 00:50:31,220
Let's go for a drink!

335
00:50:31,240 --> 00:50:33,151
I have to go home.

336
00:50:36,640 --> 00:50:37,740
OK.

337
00:50:40,200 --> 00:50:41,519
See you tomorrow then.

338
00:50:43,800 --> 00:50:44,915
Where do you live?

339
00:50:45,880 --> 00:50:46,980
With friends.

340
00:51:01,280 --> 00:51:02,918
The key to the canteen.

341
00:51:03,800 --> 00:51:05,472
Make sure you lock up!

342
00:51:06,760 --> 00:51:07,860
Thank you.

343
00:52:04,480 --> 00:52:06,198
Maybe I...

344
00:52:06,400 --> 00:52:07,799
could come along...

345
00:52:08,800 --> 00:52:10,119
fishing after all.

346
00:52:12,000 --> 00:52:13,991
But I've already booked!

347
00:52:30,960 --> 00:52:33,190
I was down in Croatia recently Ante.

348
00:52:33,360 --> 00:52:35,635
Life has gotten expensive there.

349
00:52:35,840 --> 00:52:37,876
Don't worry,
Ante has it all worked out.

350
00:52:38,080 --> 00:52:40,355
He'll go on
"Who wants to be a millionaire?"

351
00:52:43,240 --> 00:52:44,753
Ante's a genius.

352
00:52:45,320 --> 00:52:48,198
He's sure to get through to the first round.

353
00:52:56,680 --> 00:52:58,272
In one year you'll get your pension...

354
00:52:58,480 --> 00:52:59,993
Then you'll be off home, right?

355
00:53:02,360 --> 00:53:04,954
You'll definitely miss the boys!

356
00:53:05,560 --> 00:53:07,790
It's not like I get to see them often here.

357
00:53:07,960 --> 00:53:10,190
Anyhow, we have each other!
And we can do...

358
00:53:10,400 --> 00:53:12,709
some work on the house if we get bored.

359
00:53:18,920 --> 00:53:20,831
Help yourselves!

360
00:54:38,400 --> 00:54:41,517
Did you think I wouldn't notice?

361
00:54:44,520 --> 00:54:46,351
I'm going to tell Ru�a everything.

362
00:54:54,800 --> 00:54:56,756
Making yourself at home like that!

363
00:54:59,880 --> 00:55:01,279
You should be ashamed of yourself.

364
00:55:10,760 --> 00:55:12,113
You're very ungrateful.

365
00:55:13,720 --> 00:55:15,073
Ru�a took you in...

366
00:55:15,240 --> 00:55:16,753
even though nobody knew you.

367
00:55:17,480 --> 00:55:19,710
Nobody actually knows
what you're doing here.

368
00:55:19,920 --> 00:55:21,273
And what are you doing here, Mila?

369
00:55:21,520 --> 00:55:22,620
Your house is waiting.

370
00:55:49,120 --> 00:55:50,951
It might suit you.

371
00:56:25,400 --> 00:56:26,500
Come with me!

372
00:56:27,320 --> 00:56:28,639
I want to show you a place!

373
00:56:28,800 --> 00:56:30,472
- I can't.
- Do you have plans?

374
00:56:30,680 --> 00:56:32,477
- No, but...
- Come on!

375
00:56:32,640 --> 00:56:34,119
No, wait, I can't.

376
00:56:43,760 --> 00:56:45,034
15 black.

377
00:56:46,240 --> 00:56:47,639
16 yellow chips for you, sir.

378
00:56:52,000 --> 00:56:53,100
For you, madam.

379
00:56:53,240 --> 00:56:54,468
- Let's go.
- Thank you.

380
00:56:56,040 --> 00:56:58,156
People gamble in casinos.
What did you expect?

381
00:56:58,320 --> 00:57:01,517
- Come on, try.
- No way.

382
00:57:03,560 --> 00:57:04,660
Come on!

383
00:57:05,480 --> 00:57:07,471
Go put your money on a row!

384
00:57:20,080 --> 00:57:21,180
Six black.

385
00:57:26,400 --> 00:57:27,500
You've won.

386
00:57:31,200 --> 00:57:33,430
And 13... for you.

387
00:57:44,880 --> 00:57:45,980
Let's go.

388
00:57:46,840 --> 00:57:48,034
Let's go. Stop it!

389
00:57:55,440 --> 00:57:56,540
35.

390
00:58:16,760 --> 00:58:17,860
Ru�a!

391
00:58:26,520 --> 00:58:28,511
You forgot your winnings.

392
00:58:33,680 --> 00:58:34,780
And you?

393
00:58:36,480 --> 00:58:39,313
You just threw your money away.

394
00:58:40,640 --> 00:58:41,740
Just like that.

395
00:58:45,120 --> 00:58:46,473
I don't give a damn about money.

396
00:58:53,040 --> 00:58:54,917
I was like you once.

397
00:58:55,600 --> 00:58:57,431
So carefree,

398
00:58:58,360 --> 00:59:00,476
full of joie de vivre,

399
00:59:00,680 --> 00:59:01,908
an optimist.

400
00:59:07,440 --> 00:59:08,793
You've got no idea how difficult...

401
00:59:09,360 --> 00:59:11,430
it was here at the beginning!

402
00:59:13,280 --> 00:59:15,919
It was lonely, damn lonely.

403
00:59:17,560 --> 00:59:20,358
Every day I wanted to pack my bags...

404
00:59:20,640 --> 00:59:22,631
and disappear,

405
00:59:22,840 --> 00:59:25,479
go back home.

406
00:59:25,720 --> 00:59:27,119
But I didn't,

407
00:59:27,280 --> 00:59:30,590
I stayed.

408
00:59:31,760 --> 00:59:34,035
And I created something.

409
00:59:35,240 --> 00:59:36,719
Something for myself.

410
00:59:37,880 --> 00:59:40,155
Now, I don't need anybody anymore.

411
00:59:40,360 --> 00:59:43,079
I've made it.
Do you understand?

412
00:59:45,720 --> 00:59:47,756
Who are you to tell me about life?

413
00:59:49,640 --> 00:59:51,551
What is it you want to tell me?

414
00:59:52,200 --> 00:59:54,509
That I should live your life?

415
00:59:56,600 --> 00:59:59,068
I'm not like you,
you don't know me!

416
01:00:03,000 --> 01:00:04,399
How dare you talk to me like that?

417
01:00:06,160 --> 01:00:07,260
Ru�a,

418
01:00:10,760 --> 01:00:12,557
I'm scared, really scared.

419
01:00:17,040 --> 01:00:18,268
I have leukemia.

420
01:00:36,400 --> 01:00:38,550
I wasn't expecting you...

421
01:00:44,640 --> 01:00:46,835
What a nice surprise.

422
01:01:06,640 --> 01:01:08,676
Don't you want to get undressed?

423
01:01:34,200 --> 01:01:36,873
I started the inventory lists.

424
01:01:43,360 --> 01:01:44,998
Put them on the table.

425
01:01:51,920 --> 01:01:53,020
What's the matter with Ana?

426
01:01:56,760 --> 01:01:58,876
What do you mean,
what's the matter with Ana?

427
01:01:59,360 --> 01:02:01,920
It was hard work today without her.

428
01:02:03,160 --> 01:02:05,151
I'm getting used to her.

429
01:04:13,240 --> 01:04:14,798
Where's my money?

430
01:04:14,960 --> 01:04:16,060
No idea.

431
01:04:22,200 --> 01:04:24,270
You took my money, just like that.

432
01:04:28,280 --> 01:04:29,679
I had to put down...

433
01:04:30,880 --> 01:04:32,791
a deposit for the roof.

434
01:04:34,520 --> 01:04:35,748
Our roof, Mila.

435
01:04:38,320 --> 01:04:40,515
I was saving up for a new coat.

436
01:04:41,360 --> 01:04:42,588
Calm down love.

437
01:04:43,480 --> 01:04:45,516
We must get that house finished.

438
01:04:45,800 --> 01:04:47,074
Where can we go back to otherwise?

439
01:04:52,360 --> 01:04:54,828
I'm not spending
any more money on that house.

440
01:04:55,800 --> 01:04:57,438
What good is that house to me?

441
01:04:58,680 --> 01:05:00,511
But Mila, it's our dream!

442
01:05:01,400 --> 01:05:03,277
It's no longer my dream.

443
01:05:05,920 --> 01:05:07,751
Our children are here,

444
01:05:09,400 --> 01:05:11,231
my life is here.

445
01:07:15,280 --> 01:07:17,589
Do you know the feeling,
when you think...

446
01:07:19,000 --> 01:07:21,719
you're thirsty and then you realize...

447
01:07:22,400 --> 01:07:23,628
what you're feeling is longing?

448
01:07:34,680 --> 01:07:37,035
Your blood count isn't very good,

449
01:07:37,240 --> 01:07:38,434
but statistically...

450
01:07:38,680 --> 01:07:40,875
people respond well to this treatment,

451
01:07:41,080 --> 01:07:42,593
especially at your age.

452
01:07:46,560 --> 01:07:49,358
You've been treated in Sarajevo before!

453
01:07:49,880 --> 01:07:50,980
Yes.

454
01:07:55,280 --> 01:07:57,840
We'll start chemotherapy tomorrow.

455
01:08:05,080 --> 01:08:06,832
You could of course continue...

456
01:08:07,000 --> 01:08:08,991
the treatment in Sarajevo.

457
01:08:09,160 --> 01:08:10,957
What matters most
is that you start immediately.

458
01:08:31,760 --> 01:08:33,193
You know what Ru�a?

459
01:08:35,040 --> 01:08:36,951
All hospitals smell the same.

460
01:08:37,120 --> 01:08:38,758
But the view is different!

461
01:08:46,440 --> 01:08:47,839
The best view is the one...

462
01:08:48,000 --> 01:08:49,433
you choose for yourself.

463
01:09:09,200 --> 01:09:11,077
Lie down and sleep a little.

464
01:09:20,120 --> 01:09:22,634
Don't worry,
I'm counting it for the third time.

465
01:09:22,800 --> 01:09:24,472
Just to be sure!

466
01:09:24,800 --> 01:09:26,597
What are you doing here?

467
01:09:28,480 --> 01:09:30,277
I'm only doing your job...

468
01:09:32,600 --> 01:09:34,238
because you weren't there.

469
01:09:41,440 --> 01:09:43,271
You could be grateful!

470
01:09:45,880 --> 01:09:48,599
Good night Ru�a.
See you tomorrow.

471
01:09:50,600 --> 01:09:51,700
Mila.

472
01:09:55,960 --> 01:09:57,109
Thank you!

473
01:10:00,600 --> 01:10:01,999
Good night.

