1
00:00:46,600 --> 00:00:48,440
In America...

2
00:00:49,000 --> 00:00:51,960
... there's a burglary
every 1 1 seconds...

3
00:00:53,280 --> 00:00:58,040
...an armed robbery
every 65 seconds...

4
00:00:59,240 --> 00:01:02,960
...a violent crime
every 25 seconds...

5
00:01:04,280 --> 00:01:07,800
...a murder every 24 minutes...

6
00:01:08,920 --> 00:01:12,120
...and 250 rapes a day.

7
00:03:27,760 --> 00:03:29,960
Jerry, don't push it together like that.

8
00:03:30,200 --> 00:03:31,680
Will you just let me do it?

9
00:03:32,560 --> 00:03:34,080
You make me sick!

10
00:04:01,800 --> 00:04:03,080
Watch it!

11
00:04:20,920 --> 00:04:21,800
Hey, pal!

12
00:04:22,040 --> 00:04:23,240
Can I help you?

13
00:05:31,160 --> 00:05:35,520
We are willing to talk.
No one wants to hurt you.

14
00:05:35,760 --> 00:05:37,400
Do you understand?

15
00:05:37,800 --> 00:05:42,240
We are willing to talk.
There's no more need for violence.

16
00:05:43,000 --> 00:05:47,440
There's no way out except by talking.
Do you understand?

17
00:05:47,640 --> 00:05:49,920
No one wants to hurt you.

18
00:05:50,920 --> 00:05:52,520
We want to help.

19
00:05:53,520 --> 00:05:55,840
Please, communicate with us.

20
00:05:57,200 --> 00:05:58,000
You can go.

21
00:06:00,920 --> 00:06:02,040
You can go!

22
00:06:04,120 --> 00:06:05,360
You're free!

23
00:06:08,120 --> 00:06:11,480
No one wants to hurt you.
Do you understand?

24
00:06:14,800 --> 00:06:18,160
-Enough of trying to deal with this maniac!
-Get down!

25
00:06:18,880 --> 00:06:22,200
All we need is more time.
We can get control of the situation.

26
00:06:22,440 --> 00:06:23,960
What control?

27
00:06:34,640 --> 00:06:36,160
Call the Cobra.

28
00:07:28,720 --> 00:07:30,200
-How bad is it?
-It's bad.

29
00:07:30,440 --> 00:07:31,520
Any l.D. on the guy?

30
00:07:31,760 --> 00:07:35,320
Just another asshole who woke up
hating the world. What's happening?

31
00:07:35,600 --> 00:07:39,080
I don't agree with them bringing you
in here. I just want you to know that.

32
00:07:42,080 --> 00:07:43,280
Come on.

33
00:07:44,120 --> 00:07:45,080
Come on!

34
00:07:46,960 --> 00:07:47,920
Move!

35
00:08:33,600 --> 00:08:35,760
Get down, you.
Get down there, man.

36
00:08:36,000 --> 00:08:37,720
Shut up!
You get away from her!

37
00:08:38,680 --> 00:08:39,680
Shut up!

38
00:08:55,800 --> 00:08:58,120
I'll kill you, man.
I'll kill them all!

39
00:08:59,280 --> 00:09:01,200
It's the way of the New World.

40
00:09:01,440 --> 00:09:03,960
So where are those TV cameras?

41
00:09:04,200 --> 00:09:06,800
Hey man, I've got a bomb here.

42
00:09:07,840 --> 00:09:09,680
I'll kill them all!

43
00:09:10,640 --> 00:09:13,400
You bring in the TV,
or I'll kill them all.

44
00:09:13,640 --> 00:09:15,240
You understand?

45
00:09:15,480 --> 00:09:17,680
It's the way of the New World!

46
00:09:30,960 --> 00:09:33,720
What are you waiting for?
Let's go!

47
00:09:35,160 --> 00:09:38,080
Come on over here, I'll kill them.
You got that?

48
00:09:39,000 --> 00:09:41,520
You're all trash, anyway.
You all deserve it.

49
00:09:54,480 --> 00:09:55,440
Dirtbag!

50
00:09:56,920 --> 00:09:58,680
You're a lousy shot.

51
00:09:59,640 --> 00:10:01,680
I don't like lousy shots.

52
00:10:01,920 --> 00:10:04,600
You wasted a kid for nothing.

53
00:10:05,880 --> 00:10:08,280
Now I think it's time to waste you.

54
00:10:16,040 --> 00:10:17,200
Hold it!

55
00:10:17,520 --> 00:10:19,280
Come on, man.

56
00:10:19,520 --> 00:10:20,840
I got a bomb here.

57
00:10:21,080 --> 00:10:22,400
I'll kill her.

58
00:10:22,960 --> 00:10:24,440
I'll blow this place up.

59
00:10:24,880 --> 00:10:25,960
Go ahead.

60
00:10:26,200 --> 00:10:28,000
I don't shop here.

61
00:10:33,280 --> 00:10:35,120
Just relax, amigo.

62
00:10:35,360 --> 00:10:36,560
You want to talk?

63
00:10:36,800 --> 00:10:39,920
We'll talk.
I'm a sucker for good conversation.

64
00:10:40,160 --> 00:10:41,200
I don't want to talk to you.

65
00:10:41,440 --> 00:10:45,120
You bring the television cameras
in here now. Come on, bring it in!

66
00:10:46,320 --> 00:10:47,640
I can't do that.

67
00:10:47,920 --> 00:10:48,920
Why?

68
00:10:50,200 --> 00:10:53,360
I don't deal with psychos.
I put them away.

69
00:10:53,600 --> 00:10:54,920
I ain't no psycho, man.

70
00:10:55,800 --> 00:10:56,720
I'm a hero!

71
00:10:57,360 --> 00:10:59,440
You're looking at a fucking hunter.

72
00:10:59,800 --> 00:11:01,480
I'm a hero of the New World.

73
00:11:03,080 --> 00:11:05,680
You're a disease, and I'm the cure.

74
00:11:07,280 --> 00:11:08,080
Die!

75
00:11:10,960 --> 00:11:11,960
Drop it!

76
00:11:40,880 --> 00:11:42,080
Come on.

77
00:11:51,000 --> 00:11:52,280
You okay, man?

78
00:11:54,800 --> 00:11:56,600
You're the one
who killed the maniac?

79
00:11:57,160 --> 00:11:59,800
Did he say why he did it?
ls it related to the Night Slasher?

80
00:12:00,000 --> 00:12:02,600
The Night Slasher's a different case.
People, please!

81
00:12:02,840 --> 00:12:04,120
Let's have a little room!

82
00:12:07,040 --> 00:12:10,680
Was it necessary for it to end
like this? Did he have to die?

83
00:12:10,920 --> 00:12:13,320
Come on, don't listen to this asshole.

84
00:12:13,680 --> 00:12:15,800
Did you use unnecessary deadly force?

85
00:12:16,040 --> 00:12:17,640
I used everything I had.

86
00:12:17,880 --> 00:12:20,680
Is this a public forum?
We have work to do out here!

87
00:12:20,920 --> 00:12:24,840
We want to know what makes a policeman
a judge and jury. People have rights.

88
00:12:25,080 --> 00:12:27,840
You think a maniac who blew a kid's
heart out should have rights?

89
00:12:28,080 --> 00:12:32,400
No matter what you think, people
are entitled to protection by the law.

90
00:12:35,920 --> 00:12:37,840
You tell that to his family!

91
00:12:42,960 --> 00:12:45,320
All right, the show's over.
Come on, clear this area!

92
00:13:01,000 --> 00:13:02,800
-Move it!
-Fuck you!

93
00:13:10,720 --> 00:13:12,480
What's your problem?

94
00:13:23,160 --> 00:13:26,360
What's your problema, ese ?
You touched my car, man.

95
00:13:30,880 --> 00:13:32,560
That's bad for your health.

96
00:13:32,800 --> 00:13:34,760
What is, pinche ?

97
00:13:36,760 --> 00:13:37,880
Me.

98
00:13:41,880 --> 00:13:42,880
Clean up your act.

99
00:13:49,040 --> 00:13:51,120
Yeah! You're in trouble, man.

100
00:13:52,080 --> 00:13:53,680
You told him, ese.

101
00:15:26,920 --> 00:15:29,680
Tonight, the man known as
the Night Slasher...

102
00:15:29,920 --> 00:15:33,520
...has struck for the 16th time
in just over a month 's time.

103
00:15:33,760 --> 00:15:37,000
The 22-year-old victim was
mutilated with a sharp instrument...

104
00:15:37,240 --> 00:15:40,880
...and seemed to be just as unlikely
a victim as the other 15.

105
00:15:41,120 --> 00:15:45,400
Not much else is known other than that
the Night Slasher preys upon anyone.

106
00:15:45,640 --> 00:15:48,480
Victims have included businessmen,
Asian immigrants...

107
00:15:48,720 --> 00:15:52,480
... the elderly and, in one case,
a sexually assaulted child.

108
00:15:52,720 --> 00:15:55,520
The serial killer has thrown
the city into a panic...

109
00:15:55,760 --> 00:15:59,840
...and up until now, no one
has had a clue to his identity.

110
00:16:00,080 --> 00:16:03,360
The killer has been known
to cra wl through windows...

111
00:16:03,600 --> 00:16:05,640
...and kill the victims in their sleep.

112
00:16:05,840 --> 00:16:08,560
The method of death has been
silent instruments...

113
00:16:08,800 --> 00:16:11,240
...such as cla w hammers,
knives and axes.

114
00:17:25,520 --> 00:17:28,360
Cuts are straight, long, deep,
just like the others.

115
00:17:28,600 --> 00:17:31,600
That brings the count to 1 6, and still
this bastard shows no distinct pattern.

116
00:17:31,840 --> 00:17:33,440
Maybe there's more than one.

117
00:17:33,680 --> 00:17:35,920
Look, this is not your specialty, okay?

118
00:17:36,320 --> 00:17:38,040
This is pure investigative work. . .

119
00:17:38,240 --> 00:17:39,520
. . .not jumping out a window.

120
00:17:39,960 --> 00:17:43,600
-We need to beef up the task force.
-We need interagency cooperation.

121
00:17:43,760 --> 00:17:46,520
Every department wants to be
the hero in this case!

122
00:17:46,760 --> 00:17:49,880
I don't want to be a hero.
I just want to get involved.

123
00:17:54,600 --> 00:17:55,920
If I may, captain. . .?

124
00:17:56,160 --> 00:17:59,160
If we let Cobretti use his tactics,
we're asking for trouble.

125
00:17:59,360 --> 00:18:00,680
What are you saying?

126
00:18:00,880 --> 00:18:04,600
-He should stay on the zombie squad.
-You're telling me where to stay?

127
00:18:04,800 --> 00:18:07,640
It's nothing personal.
We're just different kinds of cops.

128
00:18:07,880 --> 00:18:11,520
Like Monte says, you're a specialist
who does a job nobody wants.

129
00:18:11,760 --> 00:18:14,840
But we can't shake people down.
Internal Affairs will jump on us.

130
00:18:15,040 --> 00:18:16,720
-Then we lost.
-Lost?

131
00:18:16,960 --> 00:18:21,200
As long as we play by these bullshit
rules and the killer doesn't, we lose.

132
00:18:32,840 --> 00:18:35,960
-What do you want to do?
-Nothing we can do, except wait.

133
00:18:36,160 --> 00:18:37,280
For what?

134
00:18:37,960 --> 00:18:40,040
For it to happen again.

135
00:19:14,920 --> 00:19:18,120
I'm, like, really sorry.
Do you think there's, like, any damage?

136
00:19:18,360 --> 00:19:21,640
How could you have hit me?
Have you been drinking or something?

137
00:19:22,320 --> 00:19:23,600
Yes.

138
00:20:04,040 --> 00:20:05,080
Get the license plate!

139
00:20:11,080 --> 00:20:12,080
Come on!

140
00:20:30,040 --> 00:20:31,760
He's daring us to catch him.

141
00:20:32,000 --> 00:20:34,680
-We dig in and find that lead.
-Forget it.

142
00:20:34,920 --> 00:20:36,920
Forget it?
What options do we have?

143
00:20:37,160 --> 00:20:38,520
Call the bastard.

144
00:20:42,520 --> 00:20:44,720
Metro Police. Can I help you?

145
00:21:12,240 --> 00:21:13,120
Gentlemen.

146
00:21:13,560 --> 00:21:14,960
Captain.

147
00:21:15,640 --> 00:21:17,040
I won't waste any time.

148
00:21:17,440 --> 00:21:20,760
You know almost every sicko
in this city. Shake them down.

149
00:21:21,040 --> 00:21:22,880
Do what you have to do
to get a lead.

150
00:21:23,120 --> 00:21:24,400
If I find him?

151
00:21:24,640 --> 00:21:25,920
Do what you do best.

152
00:21:27,160 --> 00:21:29,200
And try not to waste the wrong guy.

153
00:21:29,920 --> 00:21:31,120
But then again. . .

154
00:21:31,560 --> 00:21:33,920
. . .what do you care, right?

155
00:21:37,040 --> 00:21:38,760
You know, when this is over. . .

156
00:21:39,040 --> 00:21:43,760
. . .I'd like to celebrate by
punching a hole in Monte's chest.

157
00:21:46,200 --> 00:21:47,960
You know what the trouble
with you is?

158
00:21:50,200 --> 00:21:51,640
You're too violent.

159
00:21:53,800 --> 00:21:55,160
It's all that sugar you eat.

160
00:21:55,840 --> 00:21:58,520
-This is the first sugar today.
-It's that junk food.

161
00:21:58,760 --> 00:21:59,680
Bullshit!

162
00:21:59,920 --> 00:22:02,480
Try prunes, something natural.
Raisins.

163
00:22:03,160 --> 00:22:06,120
Try fish.
Fish and rice. Very good.

164
00:22:07,000 --> 00:22:08,120
Fish and rice!

165
00:25:20,120 --> 00:25:21,800
-I'll take it from here.
-Sure?

166
00:25:22,040 --> 00:25:24,400
No problem.
Her car's right over there.

167
00:25:24,640 --> 00:25:25,960
You're in good hands.
Good night.

168
00:25:26,200 --> 00:25:30,440
Listen, I still think if you wanted to,
you could be doing a lot bigger layouts.

169
00:25:30,680 --> 00:25:33,000
I mean it's up to you,
but I could help.

170
00:25:33,240 --> 00:25:35,280
I don't want to have
to play these games.

171
00:25:35,520 --> 00:25:38,520
-Games? What games?
-You know what games!

172
00:25:39,160 --> 00:25:41,280
I'd be sick not to want
to sleep with you.

173
00:25:42,120 --> 00:25:44,600
Then don't do it for me.
Do it for your career.

174
00:25:46,920 --> 00:25:48,120
Did you hear something?

175
00:25:50,080 --> 00:25:52,840
It's not like I'm
asking you to marry me.

176
00:26:03,240 --> 00:26:06,080
I see it will take time
to make you change your mind.

177
00:26:06,320 --> 00:26:10,200
-So where do you want to eat?
-Please, Dan, I'm not hungry!

178
00:26:10,720 --> 00:26:12,600
I'm not doing this for me, really.

179
00:26:12,840 --> 00:26:16,560
Look, I'm just trying
to help you find happiness.

180
00:26:17,640 --> 00:26:19,440
I forgot my bag.

181
00:26:20,000 --> 00:26:21,960
You see?
You do need me.

182
00:26:43,680 --> 00:26:46,360
Please! Come on!
My God!

183
00:26:51,800 --> 00:26:53,160
Leave me alone!

184
00:26:54,040 --> 00:26:55,080
They're trying to kill me!

185
00:26:55,320 --> 00:26:56,600
Get back!

186
00:27:01,360 --> 00:27:02,840
Kill her.

187
00:28:12,920 --> 00:28:16,960
Doctor Dorson,
please call nurses ' station 4 West.

188
00:28:17,320 --> 00:28:18,880
How long before that kicks in?

189
00:28:19,120 --> 00:28:20,280
I don't want to sleep.

190
00:28:20,760 --> 00:28:23,680
It won't make you sleep.
It will just calm you down.

191
00:28:24,280 --> 00:28:26,320
Now, I'll be back when you're finished.

192
00:28:29,240 --> 00:28:30,440
Hi.

193
00:28:30,680 --> 00:28:33,760
Is it "Ka-nudsen"? Ka?

194
00:28:34,920 --> 00:28:35,960
That's what I thought.

195
00:28:38,680 --> 00:28:40,120
I am Sergeant Gonzales. . .

196
00:28:40,440 --> 00:28:44,320
. . .and that intense-looking gentleman
behind you is Lieutenant Cobretti.

197
00:28:45,080 --> 00:28:46,960
We're a pair of really nice guys. . .

198
00:28:47,240 --> 00:28:49,560
. . .who are here to ask you
a lot of bad questions.

199
00:28:50,240 --> 00:28:51,240
Is that all right?

200
00:28:55,000 --> 00:28:56,720
You don't look like policemen.

201
00:28:57,480 --> 00:28:58,560
No?

202
00:28:58,800 --> 00:29:01,280
You're kidding?
She must mean you.

203
00:29:01,920 --> 00:29:04,760
No, we are.
We're the real thing.

204
00:29:06,360 --> 00:29:08,960
I don't understand
how this happened to me.

205
00:29:10,120 --> 00:29:13,240
Why don't you take a minute?
Think about it.

206
00:29:15,440 --> 00:29:17,600
-Did you have a fight with anyone?
-No.

207
00:29:17,840 --> 00:29:19,480
You owe money to anybody?

208
00:29:20,440 --> 00:29:22,720
You been around drugs,
people who deal drugs?

209
00:29:23,440 --> 00:29:25,120
Never. No.

210
00:29:27,080 --> 00:29:28,440
That's nice.

211
00:29:30,960 --> 00:29:34,160
So in the last few weeks,
there hasn't been anything?

212
00:29:34,400 --> 00:29:36,160
No threats of any kind?

213
00:29:36,400 --> 00:29:37,560
No.

214
00:29:38,240 --> 00:29:41,080
There was this guy who
scared me a little tonight.

215
00:29:42,400 --> 00:29:43,360
Who?

216
00:29:44,960 --> 00:29:47,080
I don't know who he was.

217
00:29:47,640 --> 00:29:49,600
Some guy broke down by the underpass.

218
00:29:49,840 --> 00:29:51,360
I just drove away.

219
00:29:53,760 --> 00:29:55,200
What time were you there?

220
00:29:56,960 --> 00:29:58,320
Maybe 1 0:00.

221
00:29:59,080 --> 00:30:01,360
He scared you. Why?
What was he doing?

222
00:30:02,360 --> 00:30:04,640
It was just the way he looked at me.

223
00:30:05,160 --> 00:30:07,720
What else did you see?
Did you see anything else?

224
00:30:08,120 --> 00:30:11,480
Yeah, there was another car
in front of them.

225
00:30:13,400 --> 00:30:14,360
Them?

226
00:30:16,000 --> 00:30:17,720
I think there was three of them.

227
00:30:18,200 --> 00:30:20,960
You think you would recognize him
if you saw him again?

228
00:30:21,440 --> 00:30:22,680
The tall one?

229
00:30:22,960 --> 00:30:25,760
Yeah, the one that wants to kill you.

230
00:30:49,640 --> 00:30:51,440
She knows your face.

231
00:30:52,160 --> 00:30:53,760
I know where she is.

232
00:30:59,840 --> 00:31:01,560
Let me get her for you.

233
00:31:08,000 --> 00:31:09,400
She's mine.

234
00:31:13,480 --> 00:31:15,200
You done, Fred?

235
00:31:16,280 --> 00:31:17,360
Cute guy.

236
00:31:19,280 --> 00:31:20,400
So, what do you think?

237
00:31:21,520 --> 00:31:23,120
It looks a little like him.

238
00:31:23,360 --> 00:31:25,360
You can't describe the other two?

239
00:31:26,040 --> 00:31:27,880
No, it was too dark.

240
00:31:28,120 --> 00:31:31,520
-Want to run prints at headquarters?
-I'll take care of it. Thanks.

241
00:31:31,760 --> 00:31:35,280
Tonight, you'll stay here. Tomorrow,
we'll move you to a safe house.

242
00:31:35,680 --> 00:31:37,880
Why do I have to stay around this?
I want to go.

243
00:31:38,160 --> 00:31:40,760
You're the one who can place him
at the crime scene.

244
00:31:41,000 --> 00:31:43,040
Until we get him,
it's got to be that way.

245
00:31:46,920 --> 00:31:48,120
Tony, you hungry?

246
00:31:48,400 --> 00:31:49,360
What do you got?

247
00:31:50,040 --> 00:31:51,920
Something that looks like cheese.

248
00:31:52,200 --> 00:31:54,120
No, there's some cake over there.

249
00:31:54,360 --> 00:31:56,720
-Take the cheese.
-I don't want the cheese.

250
00:31:56,960 --> 00:31:59,680
-I saw some cake.
-You're done with this, aren't you?

251
00:32:00,000 --> 00:32:01,240
Yes.

252
00:32:02,160 --> 00:32:04,640
All right, take the cake.
But save it, okay?

253
00:32:19,800 --> 00:32:22,240
She's going to wreck our New World.

254
00:32:22,480 --> 00:32:24,080
And the dream.

255
00:32:24,800 --> 00:32:26,840
You have to stop her.

256
00:32:33,080 --> 00:32:35,000
Great-looking guy, huh?

257
00:32:35,240 --> 00:32:36,960
Do you have that upside-down?

258
00:32:37,280 --> 00:32:39,440
No, he's just a regular dirtbag.

259
00:32:39,680 --> 00:32:42,840
I want to go to my place
and check this out against my files.

260
00:32:43,080 --> 00:32:45,040
I'll be back in a couple of hours, okay?

261
00:32:45,280 --> 00:32:47,960
There's a real nice-looking lady, huh?
Wasn't she?

262
00:32:48,200 --> 00:32:50,920
-The one upstairs?
-The one upstairs. You remember?

263
00:32:51,160 --> 00:32:53,560
I didn't notice, Gonzales.
I was on the job.

264
00:32:53,800 --> 00:32:56,320
-I was paying attention to business.
-Me too.

265
00:32:56,560 --> 00:32:57,760
Good night, all right?

266
00:32:58,000 --> 00:32:59,000
Fucking liar!

267
00:32:59,240 --> 00:33:01,560
-Watch your mouth. You're in public.
-Of course.

268
00:33:29,080 --> 00:33:30,200
You're a good citizen.

269
00:34:42,080 --> 00:34:44,240
You can't buy a damn thing
that's worth a--

270
00:35:03,000 --> 00:35:05,600
You men are supposed to use
the service elevator.

271
00:35:05,840 --> 00:35:08,280
Next time, use the stairs.

272
00:35:32,840 --> 00:35:36,760
Remember the health code.
Next time take the stairs.

273
00:35:58,200 --> 00:36:00,120
A little late to be mopping up.

274
00:36:02,640 --> 00:36:04,360
You want me to say something?

275
00:36:04,840 --> 00:36:07,640
No, I've got to do my rounds.
I'll tell him.

276
00:36:07,880 --> 00:36:09,360
See you next break.

277
00:36:31,640 --> 00:36:34,400
Hey, I'm in your office.
What do you need me for?

278
00:36:34,680 --> 00:36:35,600
Why are you there?

279
00:36:35,880 --> 00:36:37,840
Headquarters said
you wanted me here.

280
00:36:38,080 --> 00:36:40,160
Get back to the hospital!

281
00:36:40,520 --> 00:36:41,640
Presley!

282
00:39:07,400 --> 00:39:08,280
Pretty hair!

283
00:40:02,360 --> 00:40:04,680
Why are you doing this to me?

284
00:40:07,240 --> 00:40:09,560
Stop this right now!
Stop doing this to me!

285
00:40:23,600 --> 00:40:26,040
Is anybody here?

286
00:40:36,800 --> 00:40:39,200
Please remain calm.

287
00:40:39,480 --> 00:40:42,040
All exits from the hospital
are clearly marked.

288
00:40:43,200 --> 00:40:47,680
Please use the stairs and
proceed in an orderly manner.

289
00:40:47,920 --> 00:40:48,840
Do not run.

290
00:41:11,080 --> 00:41:12,920
You almost got everyone killed.

291
00:41:13,160 --> 00:41:16,520
-Now maybe you'll do it right.
-They've got somebody inside.

292
00:41:16,800 --> 00:41:19,720
Smart accusation, Cobretti.
Can you prove it?

293
00:41:20,960 --> 00:41:22,960
Chief Halliwell wants an answer.

294
00:41:23,880 --> 00:41:24,840
Not yet.

295
00:41:25,080 --> 00:41:26,760
I know what we're dealing with.

296
00:41:27,000 --> 00:41:28,880
Don't make things
harder than they are.

297
00:41:29,160 --> 00:41:31,240
Monte says you've got
a sketch of the suspect.

298
00:41:31,480 --> 00:41:32,760
Is this a game?

299
00:41:33,800 --> 00:41:35,640
He didn't say the magic word.

300
00:41:35,880 --> 00:41:36,960
What magic word?

301
00:41:37,720 --> 00:41:38,760
Please.

302
00:41:41,800 --> 00:41:44,440
All right, enough of this bullshit.
Do you understand?

303
00:41:45,520 --> 00:41:46,800
You bet.

304
00:41:48,480 --> 00:41:49,440
That's it?

305
00:41:50,880 --> 00:41:52,000
That's it.

306
00:41:56,560 --> 00:41:59,840
Cobretti, do you know
you have an attitude problem?

307
00:42:02,240 --> 00:42:04,720
Yeah, but it's just a little one.

308
00:42:14,920 --> 00:42:17,760
-He's going to get me, isn't he?
-No, he won't.

309
00:42:17,960 --> 00:42:21,280
How can you say that?
You said I'll be safe here.

310
00:42:23,120 --> 00:42:24,600
Who is he?

311
00:42:25,280 --> 00:42:26,880
We don't know yet.

312
00:42:28,600 --> 00:42:30,440
Come on, it's moving time.

313
00:42:30,760 --> 00:42:33,360
This is Officer Stalk.
She's assigned to this case.

314
00:42:33,600 --> 00:42:35,160
-So let's go, guys.
-Where?

315
00:42:35,400 --> 00:42:36,960
We're going to a safe house.

316
00:42:37,200 --> 00:42:38,400
Want help with this?

317
00:42:39,480 --> 00:42:41,280
-Want a bite?
-No.

318
00:42:41,560 --> 00:42:43,200
Don't you like health food?

319
00:42:45,760 --> 00:42:48,000
Did you notice
anything out of the ordinary?

320
00:42:48,600 --> 00:42:50,400
Did anybody challenge you?

321
00:42:52,040 --> 00:42:55,520
If you remember anything, let us know.
I appreciate your help.

322
00:42:55,760 --> 00:42:57,720
You've got to tell me
what happened here.

323
00:42:57,960 --> 00:43:01,280
I told you to let my team
handle this, understand?

324
00:43:01,560 --> 00:43:03,760
Of course you don't.
Listen, hotshot.

325
00:43:04,040 --> 00:43:06,360
-We want him just as much as you do.
-Do you?

326
00:43:06,600 --> 00:43:11,160
There were 3 men assigned to that room.
Headquarters called to take 2 off. Why?

327
00:43:11,400 --> 00:43:15,520
-Headquarters didn't do anything.
-No? Then who did?

328
00:43:15,720 --> 00:43:18,200
It's your show.
You find out.

329
00:44:32,960 --> 00:44:34,080
Where's Gonzales?

330
00:44:39,080 --> 00:44:39,960
Buckle up!

331
00:45:59,800 --> 00:46:01,800
He's crazy! He's crazy!

332
00:46:55,000 --> 00:46:55,800
Go!

333
00:47:06,040 --> 00:47:06,840
Watch out!

334
00:48:11,960 --> 00:48:13,560
Get down!

335
00:49:01,840 --> 00:49:04,160
It's so out of control.
Where do I start?

336
00:49:04,400 --> 00:49:05,640
With the legal problems.

337
00:49:05,880 --> 00:49:09,480
Of course we have legal problems.
The city's become one big battlefield.

338
00:49:09,760 --> 00:49:12,720
The public will be screaming
for federal assistance.

339
00:49:13,000 --> 00:49:14,520
I don't want FBI in here!

340
00:49:14,760 --> 00:49:18,360
We've got enough manpower to handle it,
so why can't we control this?

341
00:49:18,640 --> 00:49:22,520
Because we're against an army of
killers. I keep telling you guys that.

342
00:49:22,760 --> 00:49:24,760
-How do you know?
-I know, all right?

343
00:49:25,000 --> 00:49:28,240
-This army theory sounds weak.
-Does it really?

344
00:49:28,480 --> 00:49:32,040
What have you got, besides one scared
woman, to back up anything you say?

345
00:49:32,280 --> 00:49:36,280
And your idea of taking her out of
the city for protection is ridiculous.

346
00:49:36,760 --> 00:49:38,200
The security is here.

347
00:49:38,440 --> 00:49:41,520
Maybe she'll be safer here,
where we can keep an eye on her.

348
00:49:41,760 --> 00:49:43,200
-It won't work.
-Why not?

349
00:49:43,440 --> 00:49:46,320
We're dealing with fanatics
who'll do anything to waste her!

350
00:49:46,560 --> 00:49:48,960
Excuse me, but I've got
to say what I think.

351
00:49:49,240 --> 00:49:52,240
And I think this whole
sorry ordeal is like some. . .

352
00:49:52,480 --> 00:49:54,240
. . .damn sick joke, if you ask me.

353
00:49:54,720 --> 00:49:56,080
Nobody asked you, Monte.

354
00:49:56,320 --> 00:49:58,040
That's just too bad, isn't it?

355
00:49:58,240 --> 00:50:01,880
He doesn't give a rat's ass for
this girl. She's just live bait. . .

356
00:50:02,080 --> 00:50:04,240
. . .so King Cobretti here
can cut a new notch.

357
00:50:04,480 --> 00:50:06,720
You've already caused
a lot of people to die.

358
00:50:06,960 --> 00:50:08,680
How about letting her live?

359
00:50:11,280 --> 00:50:12,120
Enough!

360
00:50:20,880 --> 00:50:22,120
Next time.

361
00:50:22,800 --> 00:50:24,200
He's the psycho!

362
00:50:24,440 --> 00:50:25,760
What do we want to do?

363
00:50:26,200 --> 00:50:29,080
If he thinks those psychos
will follow her, let him go.

364
00:50:29,320 --> 00:50:31,720
At least we got them out of the city.

365
00:50:32,040 --> 00:50:33,440
-You all right?
-Yeah.

366
00:51:05,600 --> 00:51:06,760
Are your men out there?

367
00:51:07,200 --> 00:51:08,440
Yeah, they're out there.

368
00:51:09,480 --> 00:51:12,800
Are we just supposed to drive around
until he tries to kill us again?

369
00:51:13,520 --> 00:51:15,200
No, they won't get you.

370
00:51:15,440 --> 00:51:18,280
You keep saying that.
How do you know that for sure?

371
00:51:19,880 --> 00:51:21,720
You got to have faith.

372
00:51:51,360 --> 00:51:52,680
Can I ask you something?

373
00:51:53,240 --> 00:51:54,320
Sure, what's that?

374
00:51:54,560 --> 00:51:56,800
Why were you fighting
with the other police?

375
00:51:57,120 --> 00:52:00,480
Sometimes they don't buy what I believe.

376
00:52:01,800 --> 00:52:04,120
There's all these
crazy people everywhere.

377
00:52:04,360 --> 00:52:07,600
Why can't the police just
put them away and keep them away?

378
00:52:08,200 --> 00:52:10,000
Hey, tell it to the judge.

379
00:52:10,240 --> 00:52:11,040
What do you mean?

380
00:52:11,960 --> 00:52:14,080
We put them away.
They let them out.

381
00:52:15,200 --> 00:52:16,920
It makes me sick!

382
00:52:18,280 --> 00:52:21,080
Like I said, you got to
tell it to the judge.

383
00:52:33,240 --> 00:52:35,200
Great day for junk food, babe.

384
00:52:35,800 --> 00:52:37,840
Why don't you add meat to your diet?

385
00:52:38,160 --> 00:52:40,960
No, thanks. I hate frog legs.

386
00:52:48,600 --> 00:52:49,800
You want?

387
00:52:50,320 --> 00:52:51,600
Thank you.

388
00:52:51,960 --> 00:52:53,200
Fellow garbage-belly.

389
00:52:59,880 --> 00:53:01,120
So how you doing?

390
00:53:01,360 --> 00:53:02,560
All right. What about you?

391
00:53:02,800 --> 00:53:03,920
Not bad.

392
00:53:05,960 --> 00:53:07,240
Where are we going?

393
00:53:07,480 --> 00:53:11,000
You're going to have to ask Cobra.
He's the expert on this.

394
00:53:12,160 --> 00:53:14,440
He doesn't look like an expert, does he?

395
00:53:14,680 --> 00:53:18,080
I know. He looks like
a fugitive from the '50s. . .

396
00:53:18,320 --> 00:53:20,480
. . .but he sure is great
at catching psychos.

397
00:53:20,720 --> 00:53:22,880
He's the pride of the zombie squad.

398
00:53:23,680 --> 00:53:24,760
What's the zombie squad?

399
00:53:25,320 --> 00:53:27,000
The bottom line.

400
00:53:28,320 --> 00:53:29,280
Want to have some fun?

401
00:53:30,560 --> 00:53:32,600
It's kind of personal. . .

402
00:53:32,840 --> 00:53:34,760
. . .but it might cheer him up.

403
00:53:35,000 --> 00:53:36,480
Walk up to him. . .

404
00:53:36,800 --> 00:53:38,240
. . .and call him. . . .

405
00:53:40,000 --> 00:53:41,680
He likes that.

406
00:53:48,000 --> 00:53:49,440
Hot item.

407
00:53:50,480 --> 00:53:52,160
Did you find out anything new?

408
00:53:52,720 --> 00:53:55,120
Since you were in the bathroom?
No, not really.

409
00:53:56,040 --> 00:53:57,120
I did.

410
00:53:57,360 --> 00:53:58,480
Like what?

411
00:53:58,720 --> 00:54:00,160
Marion Cobretti.

412
00:54:01,000 --> 00:54:02,800
Is that really your name?

413
00:54:03,680 --> 00:54:05,600
Gonzales talks too much, doesn't he?

414
00:54:07,040 --> 00:54:08,560
It's not so bad.

415
00:54:10,760 --> 00:54:12,520
It's kind of tough growing up with.

416
00:54:13,760 --> 00:54:15,080
-I like it.
-Do you?

417
00:54:16,240 --> 00:54:18,680
I always wanted to have
a tougher one myself.

418
00:54:18,920 --> 00:54:20,560
You know, a little harder name.

419
00:54:20,800 --> 00:54:21,960
Like what?

420
00:54:22,760 --> 00:54:23,720
Alice.

421
00:54:27,600 --> 00:54:29,280
We'd better get going.

422
00:54:31,720 --> 00:54:33,120
Sorry, no sale.

423
00:55:31,240 --> 00:55:33,480
Your entree is ready.

424
00:55:35,320 --> 00:55:36,800
It's for the large family.

425
00:55:37,040 --> 00:55:38,800
It sure is.

426
00:55:52,480 --> 00:55:53,800
You have a life preserver?

427
00:55:54,520 --> 00:55:55,520
Why?

428
00:55:55,760 --> 00:55:58,120
Because your french fries
are drowning there.

429
00:56:01,600 --> 00:56:03,280
What is this place?

430
00:56:04,040 --> 00:56:05,800
It's a foundry town.

431
00:56:08,320 --> 00:56:09,680
Tell me something:

432
00:56:09,920 --> 00:56:11,160
Do you ever let go?

433
00:56:12,320 --> 00:56:13,280
Sure.

434
00:56:14,440 --> 00:56:16,000
Really?

435
00:56:16,240 --> 00:56:18,320
So what do you do to relax?

436
00:56:18,560 --> 00:56:20,040
Look for trouble.

437
00:56:21,000 --> 00:56:21,880
No, I mean. . .

438
00:56:22,120 --> 00:56:23,840
. . .do you ever get involved?

439
00:56:24,560 --> 00:56:25,560
With a woman?

440
00:56:26,160 --> 00:56:27,760
Yeah, a woman.

441
00:56:28,120 --> 00:56:30,200
A real woman?
A real live woman?

442
00:56:30,440 --> 00:56:33,080
I don't know.
Now and then. Nothing regular.

443
00:56:33,320 --> 00:56:34,240
Why?

444
00:56:35,200 --> 00:56:37,000
Let's just say that. . .

445
00:56:38,600 --> 00:56:41,080
. . .not many people could
put up with the way I live.

446
00:56:42,120 --> 00:56:43,880
What if you found one?

447
00:56:45,960 --> 00:56:48,680
I'd say she'd have to be
a little crazy.

448
00:57:53,400 --> 00:57:54,720
I've got to go.

449
00:57:59,840 --> 00:58:01,000
You scared me.

450
00:58:01,760 --> 00:58:02,920
Where's Tony?

451
00:58:03,560 --> 00:58:06,600
He's asleep. I was just
checking on things at home.

452
00:58:07,680 --> 00:58:09,600
Why not use the phone in your room?

453
00:58:10,000 --> 00:58:11,000
It's out of order.

454
00:58:11,240 --> 00:58:15,160
-It's getting cold out here, huh?
-Yeah. Better get inside.

455
00:58:15,720 --> 00:58:19,800
Maybe it doesn't mean much,
but I think you're doing a great job.

456
00:58:20,040 --> 00:58:20,960
You too.

457
00:59:36,920 --> 00:59:38,440
Can't you sleep?

458
00:59:39,200 --> 00:59:40,480
Not really.

459
00:59:41,960 --> 00:59:42,880
You should try.

460
00:59:48,880 --> 00:59:51,240
-I can still hear.
-What?

461
00:59:51,880 --> 00:59:53,640
That your eyes are open.

462
00:59:55,080 --> 00:59:56,320
Who can sleep?

463
01:00:00,360 --> 01:00:02,800
Would you come over here, please?

464
01:00:06,280 --> 01:00:07,880
I won't hurt you.

465
01:00:13,000 --> 01:00:14,440
Sure, why not?

466
01:00:18,800 --> 01:00:21,240
Yeah, I guess I'll be pretty safe.

467
01:00:27,000 --> 01:00:29,360
Where will you go
when this is all done?

468
01:00:30,880 --> 01:00:33,320
I don't know. Maybe another case.

469
01:00:34,960 --> 01:00:37,960
We'll see each other some more
when this is over, right?

470
01:00:40,160 --> 01:00:41,920
You think that's a good idea?

471
01:00:42,240 --> 01:00:44,160
I think it might be.

472
01:00:50,080 --> 01:00:52,040
I'll go check the window.

473
01:01:06,800 --> 01:01:09,280
I think it might be a very good idea.

474
01:02:28,200 --> 01:02:29,160
Morning.

475
01:02:29,400 --> 01:02:30,400
Hi.

476
01:02:34,440 --> 01:02:35,560
Where we going today?

477
01:02:35,800 --> 01:02:37,480
Further upstate.

478
01:02:37,800 --> 01:02:40,440
We better check on Stalk,
see why she isn't out here.

479
01:02:40,840 --> 01:02:42,440
Better get in the car.

480
01:02:42,840 --> 01:02:44,480
What's wrong?
You look so mad.

481
01:02:44,960 --> 01:02:46,960
I always look this way
before breakfast.

482
01:02:47,600 --> 01:02:48,840
-She's gone, man.
-What?

483
01:02:49,080 --> 01:02:50,120
Gone.

484
01:02:53,480 --> 01:02:54,280
Get inside.

485
01:02:56,480 --> 01:02:57,720
Get inside!

486
01:03:06,800 --> 01:03:07,880
Never liked that bitch.

487
01:03:08,120 --> 01:03:09,480
Me either.

488
01:03:09,840 --> 01:03:11,240
Get in the house.

489
01:03:14,480 --> 01:03:15,840
Get down over there.

490
01:04:51,760 --> 01:04:52,720
Come on!

491
01:05:22,040 --> 01:05:23,240
Tony!

492
01:06:39,960 --> 01:06:40,880
What do I do?

493
01:06:42,760 --> 01:06:44,080
Go through it!

494
01:06:44,320 --> 01:06:45,200
Keep going!

495
01:07:04,520 --> 01:07:06,120
Come on, let's go!

496
01:07:29,360 --> 01:07:30,880
Go to the foundry.

497
01:11:15,760 --> 01:11:17,880
You have the right to remain silent.

498
01:11:32,880 --> 01:11:33,840
Die, bitch!

499
01:11:47,800 --> 01:11:49,120
Get out of there!

500
01:12:45,200 --> 01:12:47,280
Let's bleed, pig!

501
01:12:49,160 --> 01:12:50,120
Where are you?

502
01:12:55,560 --> 01:12:57,560
I want your eyes, pig!

503
01:12:58,600 --> 01:13:00,080
I want them!

504
01:13:01,120 --> 01:13:02,840
You want to go to hell?

505
01:13:07,560 --> 01:13:09,720
You want to go to hell with me?

506
01:13:10,920 --> 01:13:12,720
It doesn't matter, does it?

507
01:13:13,320 --> 01:13:15,560
We are the hunters.

508
01:13:16,120 --> 01:13:17,400
We kill the weak. . .

509
01:13:17,640 --> 01:13:19,440
. . .so the strong survive.

510
01:13:20,240 --> 01:13:22,320
You can't stop the New World.

511
01:13:22,560 --> 01:13:25,640
Your filthy society will never
get rid of people like us.

512
01:13:26,720 --> 01:13:28,400
It's breeding them.

513
01:13:28,640 --> 01:13:30,280
We are the future!

514
01:13:33,640 --> 01:13:35,280
You're history.

515
01:13:37,960 --> 01:13:40,320
You won't do it, pig.

516
01:13:41,040 --> 01:13:42,640
You won't shoot.

517
01:13:43,560 --> 01:13:46,440
Murder is against the law.

518
01:13:48,400 --> 01:13:50,520
You have to take me in.

519
01:13:51,480 --> 01:13:52,600
If. . .

520
01:13:52,840 --> 01:13:54,200
. . .you can.

521
01:13:56,040 --> 01:13:58,280
Even I have rights. . .

522
01:13:58,800 --> 01:14:00,520
. . .don't l. . .

523
01:14:00,880 --> 01:14:02,240
. . .pig?

524
01:14:03,880 --> 01:14:05,440
Take me in.

525
01:14:06,920 --> 01:14:08,760
They'll say I'm insane.

526
01:14:09,600 --> 01:14:11,320
Won't they?

527
01:14:12,640 --> 01:14:14,880
The court is civilized. . .

528
01:14:15,120 --> 01:14:16,680
. . .isn't it. . .

529
01:14:16,920 --> 01:14:18,120
. . .pig?

530
01:14:18,600 --> 01:14:20,440
But I'm not.

531
01:14:21,480 --> 01:14:23,920
This is where the law stops. . .

532
01:14:24,440 --> 01:14:26,360
. . .and I start. . .

533
01:14:28,840 --> 01:14:30,000
. . .sucker!

534
01:14:52,880 --> 01:14:54,600
Come on, pig.

535
01:17:15,120 --> 01:17:16,360
You okay?

536
01:17:17,480 --> 01:17:18,800
You sure?

537
01:17:19,800 --> 01:17:21,240
Let's get out of here.

538
01:17:33,960 --> 01:17:37,760
Come on, you did your best.
Don't feel guilty.

539
01:17:38,200 --> 01:17:41,560
That's nice of you.
You going to be all right?

540
01:17:41,800 --> 01:17:44,280
I'm going to miss the dance finals.

541
01:17:45,480 --> 01:17:47,240
Listen, can I get you anything?

542
01:17:47,480 --> 01:17:48,640
Yeah.

543
01:17:48,920 --> 01:17:51,120
I would kill for some. . . .

544
01:17:51,640 --> 01:17:52,720
What?

545
01:17:53,200 --> 01:17:54,600
Gummy Bears.

546
01:17:55,080 --> 01:17:56,080
Gummy Bears?

547
01:17:58,960 --> 01:18:00,160
See you at the hospital.

548
01:18:00,400 --> 01:18:03,000
-Keep in touch.
-Don't drive too fast.

549
01:18:11,920 --> 01:18:13,720
You did a hell of a job here.

550
01:18:14,320 --> 01:18:17,320
You ever want a transfer
to something easier. . .

551
01:18:17,560 --> 01:18:18,840
. . .or you need anything. . .

552
01:18:19,080 --> 01:18:20,520
. . .just say the word.

553
01:18:22,840 --> 01:18:25,400
I would like to have my car replaced.

554
01:18:26,240 --> 01:18:29,040
We'd like to,
but it's not in the budget.

555
01:18:31,200 --> 01:18:34,760
See you back at headquarters.
I'll take care of your toy for you.

556
01:18:40,200 --> 01:18:41,920
No hard feelings.

557
01:18:43,000 --> 01:18:45,160
You kind of overdid it around here.

558
01:18:45,600 --> 01:18:47,160
I personally would have. . .

559
01:18:47,400 --> 01:18:50,440
. . .a more subtle solution,
but that's not your style.

560
01:18:51,040 --> 01:18:52,600
No hard feelings.

561
01:19:06,280 --> 01:19:08,560
No hard feelings, pal.

562
01:19:14,280 --> 01:19:15,400
I'll give you a lift.

563
01:19:15,640 --> 01:19:17,960
No, I've got my own
transportation, thanks.

564
01:19:18,440 --> 01:19:19,560
You ready?

565
01:19:19,800 --> 01:19:21,640
Ready, Marion.

566
01:19:22,760 --> 01:19:24,400
Catchy name, isn't it?

