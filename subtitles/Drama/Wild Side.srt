1
00:05:46,247 --> 00:05:48,977
First who makes 10, okay?

2
00:05:49,717 --> 00:05:51,344
- Show me.
- Look me.

3
00:05:51,819 --> 00:05:53,753
One, two...

4
00:05:57,191 --> 00:05:58,556
Ready?

5
00:06:03,664 --> 00:06:06,462
I'm the best! Champion! Winner.

6
00:06:10,404 --> 00:06:11,701
Where are we going?

7
00:06:11,772 --> 00:06:14,332
We don't know, anywhere you want.
Where do you want to go?

8
00:06:14,408 --> 00:06:16,171
What do you want? Let's go.

9
00:06:16,510 --> 00:06:19,308
- Wherever you want!
- Fuck, I'm always deciding.

10
00:06:27,989 --> 00:06:28,978
Let's go.

11
00:06:32,894 --> 00:06:34,259
Get the phone!

12
00:06:37,398 --> 00:06:38,797
Hello.

13
00:06:40,067 --> 00:06:43,901
You got the wrong number. No problem.

14
00:06:44,639 --> 00:06:47,836
- Who was it?
- Some guy asking for a Pierre.

15
00:06:47,909 --> 00:06:50,400
Pierre something-or-other. A wrong number.

16
00:06:55,816 --> 00:06:57,545
- Shit.
- Leave it.

17
00:07:02,189 --> 00:07:04,453
Hello. Yes.

18
00:07:06,994 --> 00:07:08,188
Yes, it's me.

19
00:09:44,819 --> 00:09:46,548
Pierre, come inside!

20
00:09:57,164 --> 00:09:58,563
Pierre, come here.

21
00:10:01,836 --> 00:10:04,430
We won't be seeing Daddy
and Caroline anymore.

22
00:11:01,462 --> 00:11:02,451
Thank you.

23
00:11:06,433 --> 00:11:07,627
- Will this be okay?
- Yes.

24
00:11:07,768 --> 00:11:09,793
- Are you comfortable?
- Yes.

25
00:11:13,574 --> 00:11:17,374
Jesus. You who know where my sister is...

26
00:11:18,646 --> 00:11:20,204
please tell me where she is.

27
00:11:20,481 --> 00:11:22,972
Please, I miss her so much.

28
00:11:23,417 --> 00:11:26,784
I want to see her
at least one more in my life.

29
00:11:29,823 --> 00:11:31,051
Please.

30
00:11:36,797 --> 00:11:39,129
Here, Cesar!

31
00:11:41,435 --> 00:11:42,868
That's a good dog.

32
00:11:44,371 --> 00:11:46,032
He wants to lick you.

33
00:11:46,240 --> 00:11:48,435
Go on, let him lick you.

34
00:11:48,842 --> 00:11:50,241
He likes you.

35
00:11:52,379 --> 00:11:56,213
I think he really likes you! Come on, let's go.

36
00:13:04,818 --> 00:13:06,080
Caroline?

37
00:13:06,887 --> 00:13:08,218
I'm sorry.

38
00:13:39,687 --> 00:13:42,178
Do want me to do it like when we were kids?

39
00:13:42,623 --> 00:13:44,682
Here we go. One for Daddy.

40
00:13:54,335 --> 00:13:55,734
One for Mommy.

41
00:14:01,442 --> 00:14:03,603
- It's not too hot?
- No.

42
00:14:04,845 --> 00:14:06,176
It has carrots.

43
00:14:06,246 --> 00:14:09,909
- Yes. You like it?
- Yes, but I don't want any more.

44
00:14:10,484 --> 00:14:12,008
Come on, just a little more.

45
00:14:13,187 --> 00:14:14,814
- One for Caro.
- I can't.

46
00:14:24,965 --> 00:14:26,455
- Did it go down?
- Yes.

47
00:14:31,105 --> 00:14:33,300
The last one is for Pierre.

48
00:14:48,856 --> 00:14:50,323
Look how skinny you are.

49
00:14:51,592 --> 00:14:54,117
You'll never get better if you don't eat.

50
00:14:55,696 --> 00:14:57,664
You've got to make an effort.

51
00:15:02,136 --> 00:15:03,125
What for?

52
00:15:07,908 --> 00:15:12,004
Bless us, Lord, bless this food,
give bread to those who have none.

53
00:15:12,079 --> 00:15:14,741
In the name of the Father, the Son,
and the Holy Ghost. Amen.

54
00:15:15,048 --> 00:15:16,948
Pierre, pass me the bread.

55
00:15:17,718 --> 00:15:18,707
Thanks.

56
00:15:28,262 --> 00:15:29,786
Bring it closer.

57
00:15:32,866 --> 00:15:34,333
- More?
- Yes.

58
00:15:57,724 --> 00:15:58,713
Talk.

59
00:16:01,295 --> 00:16:02,284
Speak.

60
00:16:12,005 --> 00:16:12,994
I kiss you.

61
00:16:25,385 --> 00:16:26,545
What is this?

62
00:16:27,654 --> 00:16:28,882
Mouth.

63
00:16:29,189 --> 00:16:31,180
- Mouth.
- Yes, mouth.

64
00:16:31,692 --> 00:16:33,489
I like your mouth.

65
00:16:45,105 --> 00:16:46,299
Face.

66
00:16:47,174 --> 00:16:48,266
Face.

67
00:16:48,642 --> 00:16:50,337
Yes, face.

68
00:16:50,577 --> 00:16:51,737
Face.

69
00:16:58,252 --> 00:16:59,241
Hand.

70
00:16:59,786 --> 00:17:00,775
Hand.

71
00:17:10,264 --> 00:17:11,253
I like hand.

72
00:17:16,537 --> 00:17:18,562
- St�phanie.
- St�phanie.

73
00:17:19,740 --> 00:17:21,071
- St�phanie.
- I know.

74
00:17:24,878 --> 00:17:27,142
- St�phanie.
- Mikhail.

75
00:17:28,181 --> 00:17:29,170
Mikhail.

76
00:20:19,686 --> 00:20:21,711
- 50 euros.
- What?

77
00:20:22,989 --> 00:20:25,287
What do you mean, "what"? It's 50 euros.

78
00:20:25,992 --> 00:20:27,721
I owe you 50 euros?

79
00:20:28,729 --> 00:20:30,788
You think I fuck for free?

80
00:20:35,469 --> 00:20:36,834
Is 20 euros all right?

81
00:20:43,877 --> 00:20:45,071
Suck me.

82
00:20:47,013 --> 00:20:48,378
Can't we kiss first?

83
00:20:48,782 --> 00:20:49,976
Suck me, I said.

84
00:20:51,952 --> 00:20:52,941
Go on.

85
00:21:21,615 --> 00:21:23,981
Look at me. Look at me!

86
00:23:34,648 --> 00:23:36,206
Does he make you happy?

87
00:23:37,284 --> 00:23:38,273
Yes.

88
00:23:40,120 --> 00:23:42,953
- Have you known him long?
- No, just a few months.

89
00:23:46,693 --> 00:23:47,682
That's nice.

90
00:23:50,263 --> 00:23:53,790
- What does he do?
- Works in a restaurant, he's a waiter.

91
00:23:56,002 --> 00:23:59,233
- Is that where you met him?
- No, in a nightclub.

92
00:24:00,941 --> 00:24:01,930
Oh, right.

93
00:24:04,945 --> 00:24:08,005
- Can you tell me his name again?
- Mikhail.

94
00:24:11,484 --> 00:24:14,112
- He seems solid, anyway.
- Why "anyway"?

95
00:24:16,323 --> 00:24:18,518
I don't know, just a manner of speaking.

96
00:24:29,436 --> 00:24:31,063
Why won't you look at me?

97
00:24:36,042 --> 00:24:37,236
Look at me.

98
00:24:46,820 --> 00:24:49,311
Still so stubborn, it never got you anywhere.

99
00:25:06,172 --> 00:25:07,730
My little boy.

100
00:25:37,103 --> 00:25:39,094
- Ancel, Jean-Fran�ois.
- Here.

101
00:25:39,339 --> 00:25:41,204
- Albert, Thierry.
- Here.

102
00:25:41,341 --> 00:25:43,309
- Benasseur, Didier.
- Here.

103
00:25:43,576 --> 00:25:45,544
- Bono, C�line.
- Here.

104
00:25:45,845 --> 00:25:47,608
- Carpentier, Fran�oise.
- Here.

105
00:25:48,081 --> 00:25:49,844
- Charlemagne, Bastien.
- Here.

106
00:25:50,450 --> 00:25:52,315
- Caumon, Pierre.
- Here.

107
00:25:52,585 --> 00:25:54,485
- Decroix, Sylvie.
- Here.

108
00:26:25,852 --> 00:26:27,376
You don't understand?

109
00:26:29,089 --> 00:26:31,887
- Russian?
- Yes.

110
00:26:38,798 --> 00:26:39,787
Russkiy.

111
00:26:48,908 --> 00:26:50,705
Can I see your hands?

112
00:27:02,422 --> 00:27:03,753
You strong.

113
00:27:04,924 --> 00:27:05,913
Yes?

114
00:27:06,993 --> 00:27:08,358
My husband.

115
00:27:09,629 --> 00:27:10,960
Do you understand?

116
00:27:11,431 --> 00:27:14,161
My husband. My husband.

117
00:27:14,801 --> 00:27:16,701
Understand? No?

118
00:27:18,905 --> 00:27:20,304
He was strong, too.

119
00:27:27,280 --> 00:27:30,078
You seem like a good man,
I'm glad Pierre found you.

120
00:27:31,351 --> 00:27:33,546
- You don't understand.
- Pierre?

121
00:27:34,187 --> 00:27:35,176
Pierre.

122
00:27:38,057 --> 00:27:39,046
You...

123
00:27:39,359 --> 00:27:40,348
Yes?

124
00:27:42,529 --> 00:27:44,258
...can you sing?

125
00:27:46,199 --> 00:27:48,326
Sing? Kalinka?

126
00:27:48,668 --> 00:27:50,499
- Kalinka!
- Yes.

127
00:27:51,804 --> 00:27:52,793
Yes.

128
00:27:52,972 --> 00:27:55,270
Juniper, Juniper, Juniper

129
00:27:57,143 --> 00:27:59,976
Juniper, Juniper, my Juniper

130
00:28:00,413 --> 00:28:01,641
Louder.

131
00:28:01,881 --> 00:28:05,180
Juniper, Juniper, my Juniper

132
00:28:05,251 --> 00:28:08,152
In the garden there's the raspberry,
my raspberry

133
00:29:14,988 --> 00:29:15,977
Hello!

134
00:29:18,491 --> 00:29:20,686
Excuse me, I'm looking for Marat Babinsky.

135
00:29:26,866 --> 00:29:28,891
Do you know where I can find him?

136
00:29:29,235 --> 00:29:32,136
There's no Babinsky here.

137
00:29:37,744 --> 00:29:39,371
He's my uncle.

138
00:29:39,979 --> 00:29:41,776
I just arrived from Odessa.

139
00:29:42,315 --> 00:29:43,577
Your uncle.

140
00:29:44,784 --> 00:29:48,151
I guess he doesn't like his nephew much.

141
00:29:48,288 --> 00:29:49,277
Why?

142
00:29:49,489 --> 00:29:53,220
Babinsky's been gone for a year now.

143
00:29:54,827 --> 00:29:56,419
You know where he is?

144
00:30:01,301 --> 00:30:03,269
I have no idea.

145
00:30:03,536 --> 00:30:07,267
In this place, people come and go.

146
00:30:08,374 --> 00:30:13,334
No one stays around.
I think he wanted to go to Germany.

147
00:30:14,681 --> 00:30:17,844
He talked about Hamburg.

148
00:30:17,917 --> 00:30:20,351
Do you have an address or phone number?

149
00:30:20,420 --> 00:30:23,719
Sorry, I can't help you.

150
00:31:36,362 --> 00:31:37,351
Hello?

151
00:31:38,598 --> 00:31:41,362
Hello. I can't hear you. Who is this?

152
00:31:43,870 --> 00:31:44,859
Hello?

153
00:32:00,553 --> 00:32:02,714
- Good evening.
- Good evening.

154
00:32:17,503 --> 00:32:19,471
- Hi.
- I'm so happy to see you.

155
00:32:19,539 --> 00:32:21,166
- How are you?
- Fine.

156
00:32:23,509 --> 00:32:24,533
It's so cool!

157
00:32:25,578 --> 00:32:27,239
Shit, there's no one around.

158
00:32:29,015 --> 00:32:30,778
It's mort, dead.

159
00:32:31,017 --> 00:32:32,006
- Dead.
- Yes.

160
00:32:32,185 --> 00:32:34,244
- Yeah, it's quiet.
- What?

161
00:32:34,320 --> 00:32:36,288
- Quiet.
- "Quite"?

162
00:32:36,355 --> 00:32:37,982
No, quiet, it's quiet.

163
00:32:38,124 --> 00:32:40,058
- What's that mean?
- No noise.

164
00:32:40,426 --> 00:32:41,984
No people, there are no people.

165
00:32:43,029 --> 00:32:44,963
No people around.

166
00:32:45,031 --> 00:32:46,498
It's Deadsville.

167
00:32:47,633 --> 00:32:49,601
And St�phanie? Good?

168
00:32:49,669 --> 00:32:51,068
- St�phanie.
- Is she okay?

169
00:32:51,437 --> 00:32:52,961
- She's okay.
- And you?

170
00:32:53,873 --> 00:32:56,307
- You.
- Me. I'm good.

171
00:32:59,278 --> 00:33:02,076
Djamel, is it good, my job?

172
00:33:02,548 --> 00:33:04,880
Your job? Don't worry, I took care of it.

173
00:33:05,885 --> 00:33:07,443
No problem, I told them.

174
00:33:10,623 --> 00:33:12,284
And you? Your job?

175
00:33:13,292 --> 00:33:15,692
My job? Sure, everything's great.

176
00:33:16,362 --> 00:33:18,227
- Really?
- Yeah, sure.

177
00:33:19,932 --> 00:33:21,263
I don't believe you.

178
00:33:23,402 --> 00:33:26,235
- "I don't believe you"?
- Yes, I don't believe you.

179
00:33:26,305 --> 00:33:28,967
Yeah, cool, everyone's cool.

180
00:33:29,175 --> 00:33:31,302
Why you talk: "blah, blah, blah"?

181
00:33:31,611 --> 00:33:32,600
What "blah"?

182
00:33:35,147 --> 00:33:37,308
You should change your life, you know?

183
00:33:37,850 --> 00:33:39,408
- My life?
- Yes.

184
00:33:39,685 --> 00:33:41,243
You want to change my life?

185
00:33:41,721 --> 00:33:44,952
- You change life for me?
- No, I don't want to change your life.

186
00:33:45,057 --> 00:33:48,254
- You change your life.
- You want me to change my life?

187
00:33:48,494 --> 00:33:49,722
- Yes.
- Why?

188
00:33:50,129 --> 00:33:51,790
'Cause it's not normal.

189
00:33:51,864 --> 00:33:53,695
You think your life is normal?

190
00:33:55,268 --> 00:33:57,133
I have normal life.

191
00:34:00,506 --> 00:34:02,770
- I'm working.
- Yeah, right.

192
00:34:07,547 --> 00:34:11,210
Leave me alone.
I'm free, Mikhail, understand? Free.

193
00:34:11,884 --> 00:34:12,873
Free.

194
00:34:13,319 --> 00:34:15,150
Do you understand that? Free!

195
00:34:17,490 --> 00:34:21,358
I want we happy, you and me and St�phanie.

196
00:34:24,564 --> 00:34:26,691
I want we are happy.

197
00:34:29,402 --> 00:34:30,664
Come on.

198
00:34:46,886 --> 00:34:47,910
Are you okay?

199
00:34:50,723 --> 00:34:53,624
l, The Golden Knight, Lord of the Somme...

200
00:34:53,693 --> 00:34:57,288
hereby promise you, my beloved Queen,
love and loyalty.

201
00:34:57,363 --> 00:35:02,323
Before God, I promise to protect you
from all dangers till death do us part.

202
00:35:02,802 --> 00:35:06,898
Noble Knight of the North,
your Queen blesses you...

203
00:35:07,039 --> 00:35:09,064
and promises in return...

204
00:35:09,141 --> 00:35:11,871
the same Loyalty and love.

205
00:35:12,211 --> 00:35:15,203
Let us seal it with a kiss.

206
00:35:26,659 --> 00:35:28,786
Come in, don't be shy.

207
00:35:29,795 --> 00:35:32,195
- Hello, ma'am.
- Hello, sir.

208
00:35:32,598 --> 00:35:34,327
Mom, this is Djamel.

209
00:35:35,668 --> 00:35:37,568
- Did you have a nice trip?
- Yes.

210
00:35:38,504 --> 00:35:40,062
It was fine.

211
00:35:46,812 --> 00:35:48,143
Have you eaten?

212
00:35:50,016 --> 00:35:51,005
No.

213
00:35:51,517 --> 00:35:53,508
- I'll fix you something.
- Okay.

214
00:36:01,494 --> 00:36:03,860
- Can I put my bag down?
- Of course.

215
00:36:12,104 --> 00:36:14,504
I'm glad your father never saw you like this.

216
00:36:19,111 --> 00:36:20,408
Why do you say that?

217
00:36:21,013 --> 00:36:23,004
You know he wouldn't have liked it.

218
00:36:27,119 --> 00:36:28,711
How do you know?

219
00:36:32,658 --> 00:36:35,718
I thought you wouldn't either,
but I was wrong.

220
00:36:36,195 --> 00:36:37,958
Yes, but he's a man.

221
00:36:41,467 --> 00:36:43,697
He was so happy to have a son.

222
00:36:49,275 --> 00:36:52,142
He told everybody right away.

223
00:36:59,385 --> 00:37:01,376
Thank God, he never saw you like this.

224
00:37:06,392 --> 00:37:09,054
- It would have hurt him.
- Don't say that.

225
00:37:12,398 --> 00:37:15,367
Maybe he just would've wanted me
to be happy.

226
00:37:19,338 --> 00:37:20,999
Men don't think that way.

227
00:37:23,776 --> 00:37:25,641
- Did he love me?
- Yes.

228
00:37:47,366 --> 00:37:49,459
Am I hurting your feelings?

229
00:38:10,222 --> 00:38:11,951
It's fucking freezing!

230
00:38:12,458 --> 00:38:15,393
- You'll sleep here. All right?
- And you?

231
00:38:16,162 --> 00:38:18,096
I sleep upstairs.

232
00:38:19,899 --> 00:38:21,992
- And Mikhail?
- Here.

233
00:38:23,736 --> 00:38:26,398
- You mean not together?
- Not here, Djamel.

234
00:38:28,174 --> 00:38:30,074
We could push the beds together.

235
00:38:30,543 --> 00:38:31,532
No.

236
00:38:32,111 --> 00:38:34,341
- Why not?
- Take a wild guess.

237
00:38:34,513 --> 00:38:37,676
- She won't hear anything.
- Yes, she will.

238
00:38:37,950 --> 00:38:42,011
Wait a sec, I didn't come 200 kilometers
to sleep alone.

239
00:38:42,988 --> 00:38:44,922
You'll live, it's only a few days.

240
00:38:44,990 --> 00:38:47,458
Djamel, don't worry.

241
00:39:03,175 --> 00:39:07,771
Okay, show me where I'm supposed
to throw the ball.

242
00:39:07,847 --> 00:39:08,836
Okay.

243
00:39:09,381 --> 00:39:10,678
Go for it, Stella!

244
00:39:10,749 --> 00:39:14,617
- Should I throw it hard?
- Go, champion!

245
00:39:14,687 --> 00:39:18,054
Give it some wrist action.
Shift into high gear.

246
00:39:22,895 --> 00:39:24,157
Very good.

247
00:39:24,396 --> 00:39:26,227
Good, but not far enough.

248
00:39:26,699 --> 00:39:30,100
I'm a girl, I'm weak! The ball is too heavy!

249
00:39:30,169 --> 00:39:31,636
Too far!

250
00:39:32,638 --> 00:39:34,731
Try again, miss.

251
00:39:34,807 --> 00:39:36,297
Okay, she's ready.

252
00:39:45,751 --> 00:39:47,378
She didn't get it.

253
00:39:47,453 --> 00:39:49,614
It's my turn again? What about her?

254
00:39:49,688 --> 00:39:52,657
- Everyone has their own role.
- Okay.

255
00:39:52,725 --> 00:39:55,057
- One person points, like you.
- I see.

256
00:39:55,127 --> 00:39:59,791
- The middle person shoots and points.
- I get it.

257
00:39:59,865 --> 00:40:03,596
The middle one points and shoots.

258
00:40:03,669 --> 00:40:06,797
- Okay, thanks.
- Try to throw it straight.

259
00:40:08,073 --> 00:40:09,404
Thanks, sugar.

260
00:40:24,223 --> 00:40:28,387
Now she gets it! There you go.

261
00:40:28,460 --> 00:40:30,428
You did it!

262
00:41:17,609 --> 00:41:18,940
Moussa, it's Natasha.

263
00:41:20,412 --> 00:41:23,108
Yes, I got your message. Where do you live?

264
00:41:23,882 --> 00:41:26,578
Okay. Within half an hour.
The time it takes to get there.

265
00:41:26,986 --> 00:41:27,975
Okay.

266
00:41:31,323 --> 00:41:33,917
- Good evening. This way.
- Thank you.

267
00:41:45,237 --> 00:41:47,865
- Can I take your coat?
- No, I'll keep it.

268
00:41:48,140 --> 00:41:50,108
I'm still a bit cold, thanks.

269
00:41:50,509 --> 00:41:54,468
- Would you like something to drink?
- Sure, water would be nice.

270
00:41:55,781 --> 00:41:59,547
- Nothing stronger?
- Just a glass of water, thanks.

271
00:42:16,869 --> 00:42:17,858
Thank you.

272
00:42:24,309 --> 00:42:25,298
Thank you.

273
00:43:27,339 --> 00:43:28,363
No!

274
00:43:43,288 --> 00:43:44,277
We won.

275
00:43:54,633 --> 00:43:56,066
It's crazy!

276
00:43:56,602 --> 00:44:00,368
Leva attacks the other phase
with a hightyfide!

277
00:44:04,176 --> 00:44:05,666
Are you okay?

278
00:44:08,947 --> 00:44:09,936
No, I'm not.

279
00:44:11,016 --> 00:44:12,415
What's the matter?

280
00:44:17,789 --> 00:44:18,778
Can I help?

281
00:44:19,358 --> 00:44:21,986
- No, you can't.
- Why can't I?

282
00:44:22,828 --> 00:44:24,193
Because...

283
00:44:26,398 --> 00:44:28,161
I'm just tired, Djamel.

284
00:44:29,001 --> 00:44:31,367
- Tired, you understand?
- No.

285
00:44:33,539 --> 00:44:35,837
It's the way it is, I'm tired.

286
00:44:39,545 --> 00:44:41,035
Do you love me?

287
00:44:43,015 --> 00:44:44,949
Of course I love you.

288
00:44:50,055 --> 00:44:52,250
I mean besides my body, do you love me?

289
00:44:53,458 --> 00:44:56,359
Of course I love you. You know I do.

290
00:45:03,335 --> 00:45:05,735
Then why don't you ask me to stop?

291
00:47:15,634 --> 00:47:17,363
Who is it in the picture?

292
00:47:18,370 --> 00:47:20,531
I love my mother here. She's beautiful.

293
00:47:29,247 --> 00:47:31,272
You know, I miss them. My parents.

294
00:47:32,050 --> 00:47:34,678
- He miss them?
- Miss. I miss them.

295
00:47:34,953 --> 00:47:36,580
- "Miss"?
- Manquer.

296
00:47:40,425 --> 00:47:42,086
Oh, he misses his parents.

297
00:47:44,496 --> 00:47:45,861
But you've got us.

298
00:48:06,618 --> 00:48:08,848
Want me to ask you about your life?

299
00:48:09,121 --> 00:48:11,555
About my life? No, why?

300
00:48:12,958 --> 00:48:15,722
- Are you mad at me?
- Mad at you for what?

301
00:48:17,129 --> 00:48:18,460
For...

302
00:48:20,198 --> 00:48:22,462
- all these years, I never...
- Hold still!

303
00:48:23,735 --> 00:48:25,032
Sorry.

304
00:48:32,611 --> 00:48:36,047
You know, if I never called...

305
00:48:40,619 --> 00:48:44,282
it was because I thought
it was better that way.

306
00:48:46,191 --> 00:48:47,522
That you...

307
00:48:48,393 --> 00:48:49,883
That it was better.

308
00:48:50,429 --> 00:48:52,090
That you wouldn't want to know.

309
00:48:55,467 --> 00:48:57,992
It's been hard all these years...

310
00:48:58,670 --> 00:49:00,501
without your father, without Caroline.

311
00:49:03,241 --> 00:49:04,538
And with you in Paris.

312
00:49:06,144 --> 00:49:07,907
It's been hard on everyone.

313
00:49:12,918 --> 00:49:15,318
- Yes.
- Tip your head back.

314
00:49:21,193 --> 00:49:23,320
But you have your whole life in front of you.

315
00:49:40,178 --> 00:49:42,544
- Hello.
- Hello.

316
00:49:44,950 --> 00:49:45,939
Can I help you?

317
00:49:48,286 --> 00:49:50,083
Hello, Nicolas.

318
00:49:51,490 --> 00:49:54,118
Settle down, I'll be right there.

319
00:49:57,229 --> 00:49:59,925
- You don't recognize me?
- No.

320
00:50:01,132 --> 00:50:02,497
Take a good look.

321
00:50:04,536 --> 00:50:05,935
I've no idea.

322
00:50:06,872 --> 00:50:09,170
It's me, Pierre. Pierre Caumon.

323
00:50:21,219 --> 00:50:24,017
It's been a long time. How long?

324
00:50:24,956 --> 00:50:27,550
- Fifteen, sixteen years?
- Seventeen.

325
00:50:28,827 --> 00:50:32,092
I left when I was 15. Add it up.

326
00:50:33,365 --> 00:50:35,333
What brought you back?

327
00:50:35,767 --> 00:50:39,100
I had to come, for my family.

328
00:50:40,839 --> 00:50:44,002
- I didn't think you'd still be here.
- Yeah, I know.

329
00:50:45,076 --> 00:50:48,170
I got out for a while,
spent five years in Brussels.

330
00:50:49,014 --> 00:50:50,777
Then I came back. I had to.

331
00:50:51,917 --> 00:50:54,283
I'm okay with it. And now I've got my kids.

332
00:50:54,819 --> 00:50:56,252
What are their names?

333
00:50:58,557 --> 00:51:01,219
- Pierre and Daniel.
- Pierre?

334
00:51:01,927 --> 00:51:03,292
That's funny.

335
00:51:08,199 --> 00:51:09,962
I'm glad you came by.

336
00:51:11,169 --> 00:51:14,229
- You really mean it?
- Yeah, I'm happy to see you.

337
00:51:14,873 --> 00:51:16,238
Thanks.

338
00:51:20,512 --> 00:51:22,571
It's strange, but you haven't changed.

339
00:51:22,981 --> 00:51:25,848
- It still seems like you.
- It still is me.

340
00:51:26,751 --> 00:51:28,343
It'll take some getting used to.

341
00:51:33,992 --> 00:51:35,857
Do you live with someone?

342
00:51:40,799 --> 00:51:43,267
- Does he mind?
- No.

343
00:51:46,271 --> 00:51:49,104
- Do you love him?
- Of course, why?

344
00:51:50,075 --> 00:51:51,303
No reason.

345
00:51:59,484 --> 00:52:01,975
Did you think I'd love you forever, Nicolas?

346
00:52:02,287 --> 00:52:03,481
Teach me...

347
00:52:03,755 --> 00:52:06,189
boxing punches and all that.

348
00:52:06,391 --> 00:52:08,188
- The box, you and me.
- Okay.

349
00:52:09,194 --> 00:52:10,559
- Gently.
- Okay.

350
00:52:10,729 --> 00:52:11,718
This is...

351
00:52:12,831 --> 00:52:14,196
Okay, you try. Okay?

352
00:52:14,299 --> 00:52:15,561
- One...
- Go ahead.

353
00:52:15,967 --> 00:52:18,527
One. Oh, yeah! Good! Ducking.

354
00:52:18,603 --> 00:52:20,434
- "Ducking"?
- Yeah. Up.

355
00:52:20,505 --> 00:52:21,494
Yes, ducking.

356
00:52:21,906 --> 00:52:24,932
Aim there, not there. One, two, three.

357
00:52:25,010 --> 00:52:27,911
Yeah, no, you shouldn't turn your head.
It's very important.

358
00:52:27,979 --> 00:52:29,207
Like this?

359
00:52:29,280 --> 00:52:31,043
Oh, yeah, good. Very good.

360
00:52:31,116 --> 00:52:33,346
- I turn this.
- Go. Bang!

361
00:52:34,085 --> 00:52:37,282
He's the... 63 kilos, Djamel!

362
00:52:37,355 --> 00:52:39,949
This is Mikhail Gorbachev, 120 kilos.

363
00:52:40,025 --> 00:52:41,993
- It's no good, not evenly matched.
- Okay, weave.

364
00:52:42,060 --> 00:52:44,085
- Okay, duck.
- Duck.

365
00:52:44,696 --> 00:52:45,993
- Yeah.
- Look, huh?

366
00:52:46,064 --> 00:52:47,827
Gently. Gently.

367
00:52:47,899 --> 00:52:49,730
Gently. Gently.

368
00:52:49,801 --> 00:52:52,668
Let's go. Gently! Mikhail.

369
00:52:52,737 --> 00:52:54,432
There you go. Yeah, good.

370
00:52:54,506 --> 00:52:56,337
Let's go. One... Let's go again.

371
00:54:10,648 --> 00:54:12,115
It hurts!

372
00:54:20,458 --> 00:54:21,618
Come here, you.

373
00:54:22,794 --> 00:54:23,818
Get lost.

374
00:54:54,726 --> 00:54:56,557
He's better looking than me.

375
00:54:56,628 --> 00:54:58,858
- Don't say that.
- That's what you said.

376
00:54:59,397 --> 00:55:00,830
- I didn't.
- You did.

377
00:55:00,899 --> 00:55:02,867
I said you're both handsome.

378
00:55:08,339 --> 00:55:11,240
- So what do you think?
- About what?

379
00:55:12,243 --> 00:55:14,803
I don't know. You, me, him.

380
00:55:15,847 --> 00:55:19,248
- Us three, together.
- Could be nice.

381
00:55:21,386 --> 00:55:22,853
What's wrong?

382
00:55:24,389 --> 00:55:25,856
What's wrong?

383
00:55:27,091 --> 00:55:28,080
Nothing.

384
00:55:29,294 --> 00:55:30,784
Come on, get up.

385
00:55:31,529 --> 00:55:32,962
Come on. Come on.

386
00:55:36,434 --> 00:55:39,062
You must've eaten something bad.

387
00:55:40,405 --> 00:55:42,305
Because of the food.

388
00:55:44,442 --> 00:55:45,568
Maybe.

389
00:55:48,179 --> 00:55:49,339
Maybe not, too.

390
00:55:51,749 --> 00:55:52,738
Maybe not.

391
00:55:54,252 --> 00:55:56,413
Have I shown you my scars?

392
00:55:56,521 --> 00:55:58,045
Have you seen this one?

393
00:55:58,489 --> 00:56:01,014
Do you look, I am accident.

394
00:56:01,659 --> 00:56:02,648
True.

395
00:56:05,730 --> 00:56:06,719
Serious.

396
00:56:07,065 --> 00:56:09,499
- Look.
- I see.

397
00:56:09,701 --> 00:56:10,895
Cicatrice.

398
00:56:11,502 --> 00:56:13,402
Scar. Scar.

399
00:56:13,538 --> 00:56:15,768
- Scar?
- Scar, in English.

400
00:56:15,840 --> 00:56:17,137
This is... I'm...

401
00:56:17,742 --> 00:56:19,505
1, 2, 3, 4, 5, 6...

402
00:56:19,577 --> 00:56:22,478
1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11... Two and ten.

403
00:56:22,547 --> 00:56:23,536
Two and ten.

404
00:56:24,415 --> 00:56:27,509
Twelve years old. Me, I was very little.

405
00:56:27,585 --> 00:56:29,075
Me, little, little.

406
00:56:29,854 --> 00:56:30,843
Moped.

407
00:56:31,489 --> 00:56:35,016
"Douze ans" in English
is "two and ten," yeah?

408
00:56:36,094 --> 00:56:37,118
Two and ten?

409
00:56:37,195 --> 00:56:39,857
- How do you say "ann�es"?
- Honey.

410
00:56:39,931 --> 00:56:42,195
Yeah, honey. Two and ten honey.

411
00:56:42,367 --> 00:56:46,235
- No, 1 2 years.
- I mean, it's the first number.

412
00:56:46,938 --> 00:56:51,841
No, I mean, first time.
First time. I am this moped.

413
00:56:52,644 --> 00:56:54,009
Speak mobylette?

414
00:56:55,280 --> 00:56:58,113
- Bicycle.
- Yes, two wheels.

415
00:56:59,884 --> 00:57:01,818
With my friends.

416
00:57:05,490 --> 00:57:07,583
Accident, you speak?

417
00:57:08,393 --> 00:57:12,124
This is because, this is the veine.

418
00:57:13,331 --> 00:57:16,391
Vein. Le sang. Single.

419
00:57:17,068 --> 00:57:19,901
There was single everywhere. All over.

420
00:57:20,438 --> 00:57:23,339
I was in shorts.

421
00:57:23,775 --> 00:57:24,867
I am in shorts.

422
00:57:25,777 --> 00:57:26,766
Maillot.

423
00:57:28,146 --> 00:57:31,377
You speak? Maillot. Maillot.

424
00:57:34,619 --> 00:57:36,280
Maillot. Maillot.

425
00:57:36,754 --> 00:57:41,384
Like in volleyball.
You speak? Maillot. Football.

426
00:57:42,727 --> 00:57:44,285
- To play football.
- T-shirt.

427
00:57:44,362 --> 00:57:47,354
T-shirt, yes. T-shirt covered.

428
00:57:48,099 --> 00:57:52,126
Single everywhere. Just like my pullover.

429
00:57:52,203 --> 00:57:57,038
Same color, the single, the T-shirt,
the pull and me.

430
00:58:00,011 --> 00:58:02,036
Why did you guys leave the party?

431
00:58:06,517 --> 00:58:10,078
Mikhail wanted to.
I don't think it was his scene.

432
00:58:11,022 --> 00:58:12,614
What did he say?

433
00:58:14,359 --> 00:58:15,348
Nothing.

434
00:58:17,061 --> 00:58:20,155
- Just that he wanted to leave.
- I mean afterwards.

435
00:58:21,532 --> 00:58:23,557
We talked about tons of stuff.

436
00:58:25,970 --> 00:58:27,767
Did you know he was a soldier?

437
00:58:30,608 --> 00:58:32,007
He used to be a soldier.

438
00:58:45,990 --> 00:58:47,719
Did you fuck after?

439
00:58:48,092 --> 00:58:50,526
- Why do you say that?
- Oh, come on.

440
00:59:31,869 --> 00:59:34,337
- Any hot water left?
- Yeah.

441
00:59:37,809 --> 00:59:42,576
- There's no heat?
- No, it doesn't work. It broke down.

442
00:59:43,581 --> 00:59:47,210
- A problem with the wiring.
- It's freezing in here.

443
00:59:47,318 --> 00:59:48,580
Today, yeah.

444
00:59:48,853 --> 00:59:51,583
Yesterday was warmer.

445
00:59:52,122 --> 00:59:53,122
Misha, hold it.

446
00:59:54,523 --> 00:59:56,055
Careful, it's hot.

447
00:59:56,059 --> 00:59:57,583
Fill the glasses.

448
00:59:59,860 --> 01:00:01,987
Olga, the ladle.

449
01:00:00,496 --> 01:00:03,090
- Looks good.
- Where's the meat?

450
01:00:03,833 --> 01:00:05,323
I'm not hungry.

451
01:00:05,668 --> 01:00:06,760
No meat for the boss today.

452
01:00:21,836 --> 01:00:24,930
Everyone's working on
the second building?

453
01:00:53,050 --> 01:00:55,484
Hello?

454
01:00:56,687 --> 01:00:58,882
Who is this? Answer me!

455
01:01:00,191 --> 01:01:01,783
What do you want?

456
01:01:03,594 --> 01:01:05,653
Who's calling?

457
01:01:07,298 --> 01:01:09,698
What is it all about?

458
01:01:12,470 --> 01:01:14,461
Hello? Listen to me.

459
01:01:15,039 --> 01:01:16,028
Mikhail?

460
01:01:17,475 --> 01:01:18,464
Misha.

461
01:01:18,809 --> 01:01:20,743
I'm sure, it's you.

462
01:01:21,045 --> 01:01:24,879
Please, let me know if it's you, say
something. Who is this? What's going on?

463
01:01:35,493 --> 01:01:36,983
Mom, it's me.

464
01:01:38,329 --> 01:01:41,127
- Hello?
- Mishenka. Misha.

465
01:01:42,233 --> 01:01:44,861
- Don't cry, mom.
- Thank God, you're alive!

466
01:01:44,936 --> 01:01:48,736
- Mishenka, where are you?
- Don't cry, mom. Okay?

467
01:01:51,008 --> 01:01:54,239
Don't tell anyone, Mom. Not even Dad.

468
01:01:54,845 --> 01:01:57,006
All right, I won't. We're so tired.

469
01:01:57,582 --> 01:02:02,178
Your dad and I think about you all the time.

470
01:02:02,753 --> 01:02:04,721
Is everything all right?

471
01:02:04,889 --> 01:02:07,357
Don't cry, Mom. I love you.

472
01:02:08,526 --> 01:02:10,016
I'm fine, Mom.

473
01:02:14,432 --> 01:02:18,835
Why did you wait so long to call us?

474
01:02:23,341 --> 01:02:25,036
I'm far away. I'm sorry.

475
01:03:01,879 --> 01:03:04,040
- You got a girl, right?
- Yeah.

476
01:03:04,115 --> 01:03:05,582
Little Samia?

477
01:03:05,750 --> 01:03:09,811
You're still with Samia? Here.
She still lives in the projects?

478
01:03:10,988 --> 01:03:12,751
We do the best we can.

479
01:03:14,692 --> 01:03:17,126
- You take her to the movies?
- Yes.

480
01:03:18,996 --> 01:03:23,365
- You know, Mom's been asking about you.
- Really?

481
01:03:23,434 --> 01:03:27,165
She wants me to stop by the house.
She wants me to stop by.

482
01:03:33,244 --> 01:03:36,236
Keep it. I don't know
where you get your money.

483
01:03:36,747 --> 01:03:39,079
You think I buy your story about the job?

484
01:03:41,285 --> 01:03:43,378
If I lie, it has nothing to do with you.

485
01:03:45,790 --> 01:03:47,121
Do you understand?

486
01:03:53,831 --> 01:03:57,198
The problem between Mom and me
has nothing to do with you.

487
01:03:57,268 --> 01:03:59,133
I know. So why do you lie to me?

488
01:04:08,746 --> 01:04:10,577
I'm sorry.

489
01:05:16,781 --> 01:05:19,181
You've got white hair everywhere.

490
01:05:22,486 --> 01:05:23,475
It's cool.

491
01:05:30,694 --> 01:05:31,854
Do you have kids?

492
01:05:53,584 --> 01:05:56,451
You have to go. My wife will be home soon.

493
01:06:00,024 --> 01:06:02,015
You want to see me again?

494
01:06:03,260 --> 01:06:05,194
I shouldn't get into bad habits.

495
01:06:36,193 --> 01:06:38,286
Come in, if you're so interested.

496
01:07:09,793 --> 01:07:10,782
Take it.

497
01:07:13,464 --> 01:07:14,488
Go on.

498
01:07:30,214 --> 01:07:32,239
I told you. I look awful in this.

499
01:07:36,487 --> 01:07:37,476
Put it on.

500
01:07:39,089 --> 01:07:40,078
Go.

501
01:07:43,227 --> 01:07:44,353
I don't.

502
01:07:47,064 --> 01:07:48,361
Please.

503
01:07:50,968 --> 01:07:51,992
Don't be so formal.

504
01:08:05,849 --> 01:08:07,316
I look horrible.

505
01:08:12,556 --> 01:08:14,581
Now, speak with you male voice.

506
01:08:16,427 --> 01:08:18,224
- Male voice?
- Yes.

507
01:08:23,000 --> 01:08:24,627
This is my real voice.

508
01:08:27,371 --> 01:08:29,305
No, you have another one.

509
01:13:37,981 --> 01:13:39,346
Hello.

510
01:13:40,517 --> 01:13:41,984
Hello.

511
01:13:47,791 --> 01:13:50,351
I've been watching you,
you're really beautiful.

512
01:13:52,396 --> 01:13:53,886
Thanks, that's sweet.

513
01:14:01,638 --> 01:14:04,471
- Come here often?
- Yeah, do you?

514
01:14:04,841 --> 01:14:07,309
First time. I usually go to Kat's.

515
01:14:19,189 --> 01:14:21,248
Actually, I want to ask you something.

516
01:14:24,761 --> 01:14:26,661
Fire away.

517
01:14:33,103 --> 01:14:34,502
I'd like to...

518
01:14:38,208 --> 01:14:39,470
Fuck me, is that it?

519
01:14:42,212 --> 01:14:43,736
Not exactly.

520
01:14:51,989 --> 01:14:55,425
That guy over there with the brown coat.

521
01:14:56,293 --> 01:14:57,988
I'd do it with him.

522
01:14:59,396 --> 01:15:01,956
- Go talk to him.
- No, that's your job.

523
01:15:02,032 --> 01:15:03,863
Better hurry, he's leaving.

524
01:15:35,866 --> 01:15:37,390
Dja, it's me.

525
01:15:38,602 --> 01:15:41,366
I need the apartment. Can you split?

526
01:15:42,672 --> 01:15:45,971
No, a customer. Okay, me, too.

527
01:16:21,144 --> 01:16:23,942
I'd rather you pay now,
that way it's taken care of.

528
01:16:28,618 --> 01:16:29,949
Thank you.

529
01:16:52,008 --> 01:16:53,134
Go ahead.

530
01:17:05,255 --> 01:17:06,244
Don't worry.

531
01:17:13,463 --> 01:17:14,623
Suck his dick.

532
01:17:16,900 --> 01:17:19,596
No kissing. Keep going.

533
01:17:47,063 --> 01:17:48,553
Not like that. Faster.

534
01:17:55,805 --> 01:17:56,794
Like this.

535
01:18:21,531 --> 01:18:22,589
Now, fuck her.

536
01:18:24,167 --> 01:18:25,498
You, get on all fours.

537
01:18:40,517 --> 01:18:42,610
Go, go. Don't be afraid.

538
01:18:43,687 --> 01:18:45,587
She's used to it.

539
01:19:04,374 --> 01:19:05,966
Jerk her off.

540
01:19:31,701 --> 01:19:33,692
Careful, don't come too fast.

541
01:19:47,250 --> 01:19:48,239
Now.

542
01:22:07,757 --> 01:22:08,746
Dominique?

543
01:23:31,174 --> 01:23:33,233
What should I do with these?

544
01:23:33,443 --> 01:23:36,276
Throw it all away. I'm not keeping anything.

545
01:23:43,086 --> 01:23:44,075
Is this you?

546
01:23:45,321 --> 01:23:46,913
For me.

547
01:26:05,261 --> 01:26:08,389
Don't play with the door, Lucas.
Leave it alone. Leave it.

548
01:26:12,735 --> 01:26:15,295
You're bothering people, cut it out.

