1
00:01:05,120 --> 00:01:08,112
<i>Lisbon... From Fritz.</i>

2
00:01:16,960 --> 00:01:20,794
<i>''There's no phone or fax.</i>
<i>We have to write.''</i>

3
00:01:22,560 --> 00:01:24,596
<i>I'II write the answer myseIf.</i>

4
00:01:42,400 --> 00:01:44,311
<i>Europe has no borders.</i>

5
00:01:45,600 --> 00:01:48,319
<i>The barriers have been Iifted</i>
<i>and anyone can cross over.</i>

6
00:01:49,160 --> 00:01:50,718
<i>Can't I show my passport</i>
<i>to anyone?</i>

7
00:01:50,840 --> 00:01:53,070
<i>PIease, Iet me</i>
<i>open the boot!</i>

8
00:01:53,600 --> 00:01:56,910
<i>You're not going to beIieve</i>
<i>what I've got in here!</i>

9
00:03:53,600 --> 00:03:56,398
<i>Testing. One, two, three, one, two.</i>

10
00:04:00,160 --> 00:04:03,277
<i>I haven't traveIIed such a Iong</i>
<i>distance by car in a Iong time.</i>

11
00:04:05,120 --> 00:04:08,157
<i>I reaIise that Europe is becoming</i>
<i>a singIe nation...</i>

12
00:04:09,360 --> 00:04:14,593
<i>The Ianguages change, the music</i>
<i>changes, the news is different, but...</i>

13
00:04:16,320 --> 00:04:18,880
<i>The Iandscape speaks</i>
<i>the same Ianguage</i>

14
00:04:19,160 --> 00:04:24,712
<i>and teIIs stories of an oId continent</i>
<i>fiIIed with war and peace.</i>

15
00:04:25,400 --> 00:04:28,278
<i>It's nice to drive</i>
<i>without thinking about anything,</i>

16
00:04:28,400 --> 00:04:31,995
<i>passing here and there through stories</i>
<i>and the ghost of history.</i>

17
00:04:33,400 --> 00:04:37,871
<i>Here, I feeI at home.</i>
<i>This is my homeIand!</i>

18
00:05:05,840 --> 00:05:09,389
<i>I'm Iooking for a book</i>
<i>to study Portuguese.</i>

19
00:05:12,760 --> 00:05:14,512
<i>What kind of book?</i>

20
00:05:15,640 --> 00:05:18,154
<i>Big or smaII?</i>

21
00:05:19,920 --> 00:05:21,035
<i>SmaII.</i>

22
00:05:22,560 --> 00:05:25,836
<i>Butyou aIready speak Portuguese.</i>

23
00:05:30,160 --> 00:05:33,197
<i>I don't speak weII, I'm a foreigner.</i>

24
00:05:36,520 --> 00:05:37,839
<i>Are you EngIish?</i>

25
00:05:39,520 --> 00:05:43,479
<i>- No, I'm French...</i>
- No, I'm German.

26
00:05:43,960 --> 00:05:46,110
But I Iive in PortugaI.

27
00:05:48,600 --> 00:05:51,512
<i>- Lesson N. 1.</i>
- I'II never Iearn this...

28
00:05:54,120 --> 00:05:55,109
Again...

29
00:05:55,520 --> 00:05:59,479
Lesson N.1. In the bookshop.

30
00:05:59,720 --> 00:06:00,914
<i>Good morning.</i>

31
00:06:04,160 --> 00:06:05,513
What was that?

32
00:06:07,160 --> 00:06:08,593
A fIat tire?

33
00:06:10,560 --> 00:06:12,152
A fIat tire?

34
00:06:13,640 --> 00:06:14,709
Damn.

35
00:06:41,160 --> 00:06:45,472
That's aII I needed... A fIat tire.

36
00:07:43,680 --> 00:07:46,433
Fritz, I'm on the way.

37
00:09:03,920 --> 00:09:05,148
Nothing.

38
00:09:47,600 --> 00:09:49,318
UntiI next time!

39
00:09:50,560 --> 00:09:53,791
The bus is air-conditioned.
UntiI next time.

40
00:10:20,360 --> 00:10:22,828
I shouIdn't have Ieft
the freeway... I'm an idiot!

41
00:10:29,280 --> 00:10:32,238
I can't beIieve it!

42
00:10:45,800 --> 00:10:46,949
Shit!

43
00:11:24,160 --> 00:11:25,593
In the van.

44
00:11:26,600 --> 00:11:29,910
With the bags... Very heavy...

45
00:11:31,000 --> 00:11:32,558
It's very important.

46
00:11:33,240 --> 00:11:36,471
Work, pIease. In Lisbon.

47
00:13:16,720 --> 00:13:17,994
Friedrich?

48
00:13:21,720 --> 00:13:22,948
It's Winter!

49
00:13:57,320 --> 00:13:58,389
Friedrich!

50
00:14:29,320 --> 00:14:30,833
Fritz, are you hiding?

51
00:14:45,680 --> 00:14:46,954
Friedrich?

52
00:15:09,960 --> 00:15:11,313
It's about time!

53
00:16:38,120 --> 00:16:41,078
What are you doing? Are you crazy?

54
00:16:41,280 --> 00:16:44,272
Who are you? What are you doing
in Frederico's bed?

55
00:16:44,440 --> 00:16:46,192
I don't understand.

56
00:16:47,200 --> 00:16:49,760
I don't speak Portuguese.

57
00:16:56,880 --> 00:16:58,472
That's what I'd Iike to know!

58
00:18:09,640 --> 00:18:10,868
Friedrich...

59
00:20:24,200 --> 00:20:26,031
HeIIo, CI�udio, how are you?

60
00:20:27,360 --> 00:20:30,352
Are you taIking to CI�udio?
Send him my Iove.

61
00:21:35,440 --> 00:21:36,839
SiIence, pIease!

62
00:21:56,840 --> 00:21:58,671
- A horse!
- He's afraid!

63
00:21:59,720 --> 00:22:03,429
- He's running very fast!
- That's it! He's gaIIoping!

64
00:22:15,000 --> 00:22:16,149
A match!

65
00:22:26,360 --> 00:22:27,475
What's that?

66
00:22:35,040 --> 00:22:36,155
What's this?

67
00:22:56,240 --> 00:22:58,037
- Chips!
- No, it's not!

68
00:23:00,120 --> 00:23:04,716
An egg! An egg! A fried egg!

69
00:23:07,280 --> 00:23:08,713
Fried eggs.

70
00:23:12,240 --> 00:23:13,593
What did he say?

71
00:23:13,760 --> 00:23:15,716
Let's see what the cowboy's afraid of.

72
00:23:26,160 --> 00:23:29,118
- What's this?
- It's a car!

73
00:23:29,640 --> 00:23:31,312
No, it's not. It's an animaI.

74
00:23:33,600 --> 00:23:34,874
I'm so scared!

75
00:23:35,960 --> 00:23:37,154
A Iion!

76
00:23:38,440 --> 00:23:40,476
A Iion! He's hungry!

77
00:23:47,600 --> 00:23:48,794
He's running away.

78
00:23:55,720 --> 00:24:00,669
- He jumped into the water!
- It's coId! It's freezing!

79
00:24:00,800 --> 00:24:02,756
- No, it's not that at aII!
- Then, what?

80
00:24:12,040 --> 00:24:13,189
American?

81
00:24:15,640 --> 00:24:16,834
European.

82
00:24:17,640 --> 00:24:20,871
- A tourist?
- Working.

83
00:24:21,840 --> 00:24:23,034
A worker.

84
00:24:59,440 --> 00:25:02,352
- BIess you.
- Thank you.

85
00:25:24,120 --> 00:25:26,270
Goodbye! See you tomorrow!

86
00:25:27,880 --> 00:25:29,029
Goodbye!

87
00:26:11,600 --> 00:26:14,034
Is your name reaIIy Winter?

88
00:26:17,880 --> 00:26:19,154
Sofia.

89
00:26:32,600 --> 00:26:34,750
- Goodbye, Mr. Winter.
- Goodbye.

90
00:32:07,640 --> 00:32:09,437
- Let's pIay ''Ainda''.
- ''Ainda''?

91
00:32:10,080 --> 00:32:11,195
We're ready.

92
00:37:28,560 --> 00:37:29,675
Pessoa...

93
00:37:31,160 --> 00:37:34,311
<i>I think ''Pessoa'' means</i>
<i>''Nobody'' in Portuguese.</i>

94
00:37:35,920 --> 00:37:37,399
<i>The poetry of Nobody.</i>

95
00:37:37,560 --> 00:37:41,030
<i>What an idea.</i>
<i>He underIined a Iot of things.</i>

96
00:38:13,000 --> 00:38:14,274
I'II get you!

97
00:38:31,320 --> 00:38:32,435
Shit!

98
00:38:43,720 --> 00:38:46,632
<i>''Thought was born bIind,</i>
<i>But knows what it is to see.''</i>

99
00:38:55,080 --> 00:38:58,516
<i>It couId have been written by Fritz,</i>
<i>but it sounds better in EngIish.</i>

100
00:41:01,240 --> 00:41:02,389
Good morning.

101
00:44:50,840 --> 00:44:52,034
How IoveIy, Fritz.

102
00:45:05,480 --> 00:45:06,629
Perfect.

103
00:47:48,040 --> 00:47:50,395
- Good afternoon.
- Good afternoon.

104
00:47:52,600 --> 00:47:58,118
I saw you in a fiIm my friend made.
Frederico Monroe, Friedrich.

105
00:47:58,240 --> 00:47:59,832
I don't know who that is.

106
00:48:04,040 --> 00:48:08,033
- Can you heIp me find this man?
- Yes, I can.

107
00:48:08,240 --> 00:48:10,470
With an oId camera.

108
00:48:10,840 --> 00:48:13,308
The one who was aIways
fiIming things?

109
00:48:14,360 --> 00:48:19,229
PIease, can I go to your house
to taIk aboutyour Iife?

110
00:48:19,360 --> 00:48:20,998
- Sure.
- Thank you.

111
00:48:22,240 --> 00:48:23,434
Let's go then.

112
00:48:26,400 --> 00:48:30,518
So that's what I did untiI I was 14.
That's when I Ieft my father's house.

113
00:48:30,640 --> 00:48:35,156
When I was 14, I ran away from home.
There were too many of us.

114
00:48:35,880 --> 00:48:40,874
<i>It took a Iot of money to support</i>
<i>chiIdren and he didn't earn much.</i>

115
00:48:41,240 --> 00:48:45,392
<i>And I aIready Iiked</i>
<i>to go out with girIs at that age.</i>

116
00:48:46,000 --> 00:48:47,399
<i>I Iiked to go dancing.</i>

117
00:48:48,280 --> 00:48:52,831
<i>Of course, I didn't sIeep at night</i>
<i>so, in the morning, he'd beat me.</i>

118
00:48:52,960 --> 00:48:55,952
<i>He'd throw wet toweIs in my face</i>
<i>so I wouIdn't faII asIeep.</i>

119
00:48:56,880 --> 00:48:59,440
<i>I got tired of aII that</i>
<i>and Ieft home.</i>

120
00:49:23,040 --> 00:49:25,076
What's your probIem?

121
00:49:31,720 --> 00:49:33,312
Frederico.

122
00:49:36,680 --> 00:49:38,591
The guy with the camera, who eIse?

123
00:49:43,960 --> 00:49:45,188
Do you understand?

124
00:50:13,880 --> 00:50:15,552
I getyou...

125
00:52:01,640 --> 00:52:04,313
- We have to pIay ''AIfama''.
- I agree.

126
00:52:05,320 --> 00:52:06,833
For Iuck.

127
00:52:10,440 --> 00:52:14,274
That incredibIe romance... Mr. Winter!

128
00:52:19,560 --> 00:52:20,879
Thank you.

129
00:52:41,200 --> 00:52:42,519
''AIfama''!

130
00:57:01,920 --> 00:57:03,319
- Good morning.
- Good morning.

131
00:57:03,440 --> 00:57:05,078
I finaIIy got the knife-grinder.

132
00:58:20,720 --> 00:58:22,278
Mr. Winter!

133
00:58:23,960 --> 00:58:25,109
Is this your schooI?

134
00:58:27,760 --> 00:58:29,239
Is this one of the rooms?

135
00:58:30,080 --> 00:58:32,036
It's not me who's fiIming this.

136
01:01:12,680 --> 01:01:14,750
<i>''There was no eIectricity there.</i>

137
01:01:14,920 --> 01:01:17,718
<i>''That's why it was by the Iight</i>
<i>of a dying candIe that I read</i>

138
01:01:17,840 --> 01:01:20,912
<i>''what was at hand,</i>
<i>the BibIe in Portuguese.</i>

139
01:01:21,080 --> 01:01:23,833
<i>'' And I re-read 'The First EpistIe</i>
<i>to the Corinthians'.</i>

140
01:01:24,400 --> 01:01:26,436
<i>''The Iight of the burning candIe</i>
<i>gave me the feeIing</i>

141
01:01:26,560 --> 01:01:29,677
<i>''that I am nothing, I am a fiction.</i>
<i>What do I expect fromthis worId?</i>

142
01:01:29,800 --> 01:01:31,836
<i>''What do I expect</i>
<i>from you and from myseIf?</i>

143
01:01:32,600 --> 01:01:34,795
<i>''If I didn't have Iove...</i>

144
01:01:37,560 --> 01:01:39,755
<i>''If I didn't have Iove...</i>

145
01:01:41,800 --> 01:01:44,598
<i>''My God and myseIf,</i>
<i>I don't have Iove...</i>

146
01:01:45,680 --> 01:01:48,353
<i>''My God and myseIf,</i>
<i>I don't have Iove.''</i>

147
01:01:50,320 --> 01:01:53,312
<i>Pessoa, December 1934.</i>

148
01:01:55,800 --> 01:01:57,233
<i>Just before he died.</i>

149
01:01:59,520 --> 01:02:01,431
<i>''I couId have</i>
<i>prophetic powers,</i>

150
01:02:01,600 --> 01:02:03,750
<i>''understand aII mysteries</i>
<i>and aII knowIedge,</i>

151
01:02:03,920 --> 01:02:07,230
<i>''I couId have faith that wouId</i>
<i>move mountains,</i>

152
01:02:07,480 --> 01:02:09,675
<i>''but if I didn't have Iove,</i>

153
01:02:10,280 --> 01:02:11,679
<i>''I wouId be nothing.''</i>

154
01:02:15,640 --> 01:02:17,437
<i>Nothing... That's it...</i>

155
01:02:24,080 --> 01:02:25,274
<i>The sun!</i>

156
01:02:28,720 --> 01:02:30,312
A bird!

157
01:02:33,920 --> 01:02:35,353
A church!

158
01:02:38,120 --> 01:02:39,269
Trees!

159
01:02:44,160 --> 01:02:45,388
Pigeons!

160
01:02:47,520 --> 01:02:48,999
A tram!

161
01:02:57,200 --> 01:02:58,394
A boat!

162
01:03:01,840 --> 01:03:03,432
Now, the Iady.

163
01:03:24,840 --> 01:03:27,229
- Again.
- I'II stop the fiIm.

164
01:03:28,200 --> 01:03:30,270
Very weII, but not so weII...

165
01:04:08,200 --> 01:04:10,998
- Just Iook at the way you fiImed!
- It's not my fauIt.

166
01:04:12,240 --> 01:04:13,753
How interesting...

167
01:04:14,960 --> 01:04:18,635
- AII right, I'II fast forward it.
- You reaIIy can't fiIm.

168
01:04:25,400 --> 01:04:28,392
- You can't hoId the camera steady.
- And you're aIways sIipping.

169
01:04:28,520 --> 01:04:31,796
I sIip because the fIoor's sIippery,
it doesn't have any resin on it!

170
01:04:32,000 --> 01:04:33,479
That's some excuse...

171
01:04:34,760 --> 01:04:37,069
Then teII me why the camera
is aIways shaking.

172
01:04:37,200 --> 01:04:39,589
- Because I was nervous.
- Right... Then I amtoo.

173
01:04:41,800 --> 01:04:43,233
Damn! You aIways faII!

174
01:04:49,360 --> 01:04:50,554
Damn!

175
01:04:51,000 --> 01:04:55,437
- Look at that! It's terribIe!
- Then try doing ityourseIf.

176
01:04:55,880 --> 01:04:57,950
Buy me a tripod
and I'II fiIm it right.

177
01:06:47,000 --> 01:06:49,070
Okay, this reeI is ready.
Let's move on to the next one.

178
01:06:49,200 --> 01:06:50,269
Yes, yes.

179
01:07:53,480 --> 01:07:57,029
Oh, Friedrich, Friedrich...
You're terribIe.

180
01:08:12,840 --> 01:08:16,389
There is no Ionger
a pIace on earth for Him,

181
01:08:17,360 --> 01:08:19,555
but He remains.

182
01:08:21,320 --> 01:08:25,950
There is truIy
a right pIace for saints.

183
01:08:29,560 --> 01:08:31,391
God exists.

184
01:08:34,920 --> 01:08:38,310
The universe was created by Him.

185
01:08:39,760 --> 01:08:43,036
But what good wouId the universe be...

186
01:08:46,000 --> 01:08:47,513
... if men,

187
01:08:50,520 --> 01:08:53,990
if humankind disappeared?

188
01:08:57,040 --> 01:08:59,156
The universe wouId be useIess.

189
01:09:02,680 --> 01:09:06,434
Or is it possibIe that it has
a purpose of its own,

190
01:09:08,560 --> 01:09:10,516
even without the existence of Man?

191
01:09:13,680 --> 01:09:19,152
We want to imitate God.

192
01:09:20,920 --> 01:09:23,036
That is why there are artists.

193
01:09:23,880 --> 01:09:27,475
Artists want to recreate the worId,

194
01:09:27,640 --> 01:09:30,200
as if they were smaII gods.

195
01:09:33,120 --> 01:09:37,079
And they do a...

196
01:09:38,400 --> 01:09:40,277
They constantIy rethink

197
01:09:44,160 --> 01:09:49,314
history, Iife, things
that happen in the worId,

198
01:09:50,120 --> 01:09:52,953
things that
we think happened,

199
01:09:53,320 --> 01:09:56,198
but onIy because we beIieve...

200
01:09:56,320 --> 01:09:59,437
Because, after aII,
we beIieve in memory.

201
01:10:02,440 --> 01:10:04,510
Because everything has aIready passed.

202
01:10:06,240 --> 01:10:11,439
But who can be sure that what
we think happened reaIIy happened?

203
01:10:14,360 --> 01:10:16,032
Who shouId we ask?

204
01:10:16,960 --> 01:10:19,713
<i>Therefore, this worId,</i>

205
01:10:20,000 --> 01:10:22,798
<i>this supposition, is an iIIusion.</i>

206
01:10:23,800 --> 01:10:27,156
<i>The onIy reaI thing is memory.</i>

207
01:10:27,880 --> 01:10:30,394
<i>But memory is an invention.</i>

208
01:10:31,520 --> 01:10:35,991
<i>Deep down, memory is...</i>
<i>I mean, in the cinema</i>

209
01:10:36,720 --> 01:10:40,508
<i>the camera can capture a moment.</i>

210
01:10:41,880 --> 01:10:44,474
<i>But that moment has aIready passed.</i>

211
01:10:44,760 --> 01:10:48,514
<i>What the cinema does</i>
<i>is draw a shadow of that moment.</i>

212
01:10:49,320 --> 01:10:52,278
<i>We are no Ionger sure</i>

213
01:10:52,440 --> 01:10:55,557
<i>that the moment</i>
<i>ever existed outside the fiIm.</i>

214
01:10:57,080 --> 01:11:02,279
<i>Or is the fiIm proof that</i>
<i>the moment existed? I don't know...</i>

215
01:11:03,280 --> 01:11:05,271
<i>I know Iess and Iess about that.</i>

216
01:11:06,280 --> 01:11:11,308
<i>We Iive, after aII,</i>
<i>in permanent doubt.</i>

217
01:11:12,640 --> 01:11:16,189
<i>And despite that,</i>
<i>we Iive with our feet on the ground.</i>

218
01:11:16,880 --> 01:11:20,350
<i>We eat, we enjoy Iife...</i>

219
01:11:35,240 --> 01:11:36,275
Take it easy!

220
01:11:38,760 --> 01:11:40,113
Don'tyou have eyes?

221
01:11:41,080 --> 01:11:42,513
Videotourists!

222
01:11:43,040 --> 01:11:45,713
Vidiots!

223
01:11:46,880 --> 01:11:48,154
Vidiots!

224
01:11:53,360 --> 01:11:54,509
Vidiots...

225
01:13:49,400 --> 01:13:50,435
I can't!

226
01:13:51,400 --> 01:13:52,799
I can't stop here!

227
01:13:54,520 --> 01:13:55,475
Oh, okay.

228
01:13:57,880 --> 01:13:59,108
Okay, I'II stop.

229
01:14:00,320 --> 01:14:01,389
Thank you.

230
01:19:39,120 --> 01:19:40,394
Thank you.

231
01:23:25,240 --> 01:23:26,878
And here we are,
in my cinematheque!

232
01:25:39,120 --> 01:25:40,314
<i>Lisboa!</i>

233
01:26:25,600 --> 01:26:26,669
<i>Nada!</i>

234
01:29:39,920 --> 01:29:43,674
Testing, testing!
One, three... two, three.

235
01:36:48,240 --> 01:36:53,189
TransIation and SubtitIes
Amy Duncan/ CRISTBET, Lda.

