1
00:00:48,800 --> 00:00:53,522
Something happened in the
forest last summer.

2
00:00:56,680 --> 00:00:58,444
A crime.

3
00:03:18,440 --> 00:03:20,010
No!

4
00:03:42,840 --> 00:03:45,207
I'll help you.

5
00:03:55,640 --> 00:03:57,608
- Don't die.
- Stay with me.

6
00:03:57,800 --> 00:03:59,962
Don't die.

7
00:04:01,280 --> 00:04:03,931
- Don't die.
- Just stay with me.

8
00:04:04,080 --> 00:04:06,162
Don't die.

9
00:04:07,600 --> 00:04:11,002
He was so small and lovely...

10
00:04:11,160 --> 00:04:12,730
William?

11
00:04:17,520 --> 00:04:19,363
William?

12
00:04:27,880 --> 00:04:30,565
Don't die!

13
00:04:34,320 --> 00:04:36,891
Don't die!

14
00:05:11,920 --> 00:05:18,804
ALL THAT MATTERS IS PAST

15
00:05:34,800 --> 00:05:37,770
I don't know what to do with her.

16
00:05:41,600 --> 00:05:43,967
Is she a murderer?

17
00:05:45,000 --> 00:05:47,446
Or is she a victim?

18
00:05:59,760 --> 00:06:02,604
They lived in this cabin.

19
00:06:03,640 --> 00:06:07,690
This is where I met them
the first time.

20
00:06:12,920 --> 00:06:15,082
But their story ...

21
00:06:16,120 --> 00:06:18,327
... began long before.

22
00:06:36,960 --> 00:06:42,410
She says she was the kind of girl
who lived In her own world.

23
00:06:42,600 --> 00:06:44,921
In her own house.

24
00:06:47,760 --> 00:06:53,130
She educated herself by reading
magazines she had found at the dump.

25
00:07:07,360 --> 00:07:11,922
So in that sense she felt
ready to take on life.

26
00:07:28,200 --> 00:07:33,684
Who might she have become if
she hadn't met those two brothers?

27
00:07:34,840 --> 00:07:37,446
William and Ruud.

28
00:07:37,640 --> 00:07:40,769
Two boys, recently arrived from Sweden.

29
00:07:40,920 --> 00:07:42,763
Hi.

30
00:07:43,960 --> 00:07:45,928
Hi.

31
00:07:53,200 --> 00:07:57,046
She says it was love at first sight.

32
00:08:05,400 --> 00:08:10,247
With that love,
the two brothers lost each other.

33
00:09:30,200 --> 00:09:32,043
Hello?

34
00:11:24,720 --> 00:11:29,089
Do not go gentle
into that good night

35
00:11:29,240 --> 00:11:34,007
Grave men, near death,
who see with blinding sight

36
00:11:34,160 --> 00:11:38,882
Blind eyes could blaze like
Meteors and be gay

37
00:11:39,040 --> 00:11:43,284
Rage, rage against the
dying of the fight

38
00:11:45,600 --> 00:11:50,766
And you, my father,
there on the sad height

39
00:11:50,920 --> 00:11:57,007
Curse, bless me now
with your fierce tears, I pray

40
00:11:57,960 --> 00:12:01,965
Do not go gentle
into that good night

41
00:12:02,120 --> 00:12:07,365
Rage, rage against the
dying of the fight

42
00:12:08,400 --> 00:12:11,051
What is this poem about? Yes?

43
00:12:11,200 --> 00:12:15,000
I know he wrote it
when his father died.

44
00:12:15,160 --> 00:12:18,369
It's about rebelling,
fighting against death.

45
00:12:18,560 --> 00:12:22,007
"Don't go gentle
into that good night...

46
00:12:22,160 --> 00:12:25,642
- To fight. Refuse to give in.
- Good.

47
00:12:25,800 --> 00:12:29,441
Could it be about rebelling
in general?

48
00:12:29,640 --> 00:12:31,881
Sure. Why do you say that?

49
00:12:32,040 --> 00:12:36,250
I don't know.
It is a strong appeal to fight.

50
00:12:36,400 --> 00:12:40,121
But he describes
the night as "good," ...

51
00:12:40,280 --> 00:12:45,525
... even though he encourages us
to "not go gently" into it.

52
00:12:45,680 --> 00:12:48,365
Let's listen to it again.

53
00:12:50,560 --> 00:12:52,927
Hi. Do you have a moment?

54
00:12:53,080 --> 00:12:56,004
He says it can't wait.

55
00:12:56,160 --> 00:12:59,687
- And it isn't Espen?
- No.

56
00:13:07,760 --> 00:13:09,888
There you are.

57
00:13:18,880 --> 00:13:21,042
I should have called.

58
00:13:23,040 --> 00:13:26,249
But it was all kind of...

59
00:13:26,400 --> 00:13:30,200
On impulse, so...
I just got off a 17-hour flight.

60
00:13:32,880 --> 00:13:36,362
How about you?
Are you doing all right?

61
00:13:37,400 --> 00:13:39,243
Yes.

62
00:13:43,600 --> 00:13:48,288
- I'm going to the cabin...
- I have a new family.

63
00:13:49,400 --> 00:13:53,405
- Kids?
- I'm a stepmother to two girls.

64
00:13:55,920 --> 00:13:59,049
- Come with me.
- Why have you come here now?

65
00:14:03,000 --> 00:14:05,890
T witnessed the tsunami.

66
00:14:11,280 --> 00:14:14,568
And I have forgiven myself.

67
00:14:18,440 --> 00:14:20,568
It's too late.

68
00:14:24,320 --> 00:14:26,800
It's good to see you.

69
00:14:28,280 --> 00:14:32,251
- Do you think I look bad?
- You look worn out.

70
00:14:33,280 --> 00:14:35,806
I saw you on Facebook.

71
00:14:39,960 --> 00:14:43,328
But wanted to see you face to face.

72
00:14:46,400 --> 00:14:49,847
I have a dentist appointment.

73
00:15:22,520 --> 00:15:25,091
I met William today.

74
00:15:31,000 --> 00:15:33,287
He's back.

75
00:15:34,680 --> 00:15:37,445
He looked pretty tired.

76
00:15:39,680 --> 00:15:42,160
Where did you meet him?

77
00:15:44,040 --> 00:15:47,283
- He showed up at school.
- What did he want?

78
00:15:53,080 --> 00:15:55,606
Are you leaving us?

79
00:17:30,960 --> 00:17:34,806
It's nice to hear your footsteps.

80
00:17:34,960 --> 00:17:37,247
I almost didn't see you.

81
00:17:44,680 --> 00:17:47,160
I bought a car.

82
00:19:56,520 --> 00:19:58,966
- Still lots of salt?
- Yeah.

83
00:19:59,120 --> 00:20:02,966
- And crazy amounts of ketchup?
- Yeah.

84
00:20:11,840 --> 00:20:14,127
Welcome back.

85
00:20:19,800 --> 00:20:21,882
Cheers.

86
00:21:32,080 --> 00:21:34,162
Can I touch you?

87
00:22:31,680 --> 00:22:36,447
And tomorrow,
I'll go into the forest ...

88
00:22:36,640 --> 00:22:41,601
... and we'll pretend to be children
pretending to be children.

89
00:22:41,760 --> 00:22:47,642
And I'll be your mirror.
Because no one loves you like I do.

90
00:22:47,800 --> 00:22:52,567
And no one kisses you like I do.
Returning to life.

91
00:23:17,920 --> 00:23:20,844
Hello, can you hear me?

92
00:24:59,120 --> 00:25:01,122
Hey!

93
00:26:13,080 --> 00:26:15,242
She speaks of a boy ...

94
00:26:15,400 --> 00:26:20,645
... who looked at the others
without being allowed to take part.

95
00:26:35,600 --> 00:26:38,888
And that even back then
they feared him.

96
00:27:05,120 --> 00:27:07,521
What is our connection to evil?

97
00:28:26,600 --> 00:28:31,606
The two dead men and near-dead woman
have not yet been Identified.

98
00:28:31,760 --> 00:28:34,843
The kayaker
who happened upon them ...

99
00:28:35,000 --> 00:28:40,882
... says he didn't see the woman.
or he would never have left her,

100
00:28:41,040 --> 00:28:44,408
So you knew both of the deceased?

101
00:28:54,080 --> 00:28:57,289
Would you like something to drink?

102
00:28:57,440 --> 00:28:59,329
Yes.

103
00:32:01,280 --> 00:32:03,169
Hi.

104
00:32:05,440 --> 00:32:08,569
- I killed a goat.
- Whose is it?

105
00:32:11,800 --> 00:32:14,087
I was up at the farm.

106
00:32:17,240 --> 00:32:19,686
Did he give it to you?

107
00:32:27,520 --> 00:32:30,808
I'll pay him the next time I see him.

108
00:32:31,520 --> 00:32:35,366
- How is he now?
- He sounds OK on the phone.

109
00:32:35,560 --> 00:32:38,291
But I haven't met him.

110
00:32:39,360 --> 00:32:41,886
Poor little thing.

111
00:33:48,320 --> 00:33:51,324
- OK. Are you ready?
- Yes.

112
00:34:22,880 --> 00:34:25,565
I just wanted to say ...

113
00:34:26,760 --> 00:34:31,163
... that I took one of your goats
this morning.

114
00:34:33,200 --> 00:34:35,806
So I... wanted to pay for it.

115
00:34:48,120 --> 00:34:50,726
I didn't recognize you.

116
00:34:53,400 --> 00:34:56,927
- You aren't that pretty anymore.
- I know.

117
00:34:59,160 --> 00:35:03,882
We just wanted to let you know
we're here, and pay you.

118
00:36:17,320 --> 00:36:24,010
He received two blows with a heavy
object. And a bird has nibbled here.

119
00:36:24,160 --> 00:36:27,562
I've seen him before,
but didn't recognize him.

120
00:36:27,720 --> 00:36:32,282
It took him a while to die.
He became paralyzed first.

121
00:36:33,000 --> 00:36:38,723
We found soil in his mouth.
Most likely placed there by someone.

122
00:36:38,880 --> 00:36:43,283
But the cause of death
was the initial brain trauma.

123
00:36:46,800 --> 00:36:50,646
This one lost a lot of blood.

124
00:36:50,800 --> 00:36:54,088
The knife cut his femoral artery.

125
00:36:54,240 --> 00:36:59,929
- They don't look much like brothers.
- But they are brothers.

126
00:37:46,280 --> 00:37:50,683
She says he noticed her
when she was 14.

127
00:37:57,680 --> 00:38:02,925
And that what happened afterward
was her own fault.

128
00:38:30,080 --> 00:38:32,162
Stop!

129
00:38:35,200 --> 00:38:40,001
She says that when you are 14.
you long for what causes pain.

130
00:38:59,160 --> 00:39:02,448
I want to show you something.

131
00:39:05,640 --> 00:39:07,608
Get in.

132
00:40:29,440 --> 00:40:32,728
It doesn't matter that it's raining.

133
00:40:33,560 --> 00:40:37,360
The mother bird keeps the eggs dry.

134
00:40:55,200 --> 00:40:57,521
Can I have one?

135
00:41:01,080 --> 00:41:03,447
If I get a kiss.

136
00:41:43,800 --> 00:41:45,131
Stop!

137
00:42:12,560 --> 00:42:15,040
Take your pants off.

138
00:43:35,120 --> 00:43:38,169
I can fix this place up for you.

139
00:43:38,320 --> 00:43:41,005
Say it in Norwegian.

140
00:43:48,440 --> 00:43:53,002
- You can only learn one language.
- So what?

141
00:43:58,080 --> 00:44:00,651
We can run away, if you want.

142
00:44:03,440 --> 00:44:05,920
Just me and you.

143
00:44:07,080 --> 00:44:09,526
I love you.

144
00:44:39,560 --> 00:44:41,847
I love you.

145
00:44:44,200 --> 00:44:49,843
- Why are you speaking Swedish?
- I don't know.

146
00:44:52,960 --> 00:44:55,361
Give me a kiss.

147
00:45:06,000 --> 00:45:08,844
Take your pants off.

148
00:45:18,080 --> 00:45:20,367
And that.

149
00:47:07,160 --> 00:47:10,243
There's a dead one in there

150
00:48:43,520 --> 00:48:45,522
Ragnhild?

151
00:48:45,680 --> 00:48:49,082
Ragnhild, come take a look at this.

152
00:48:57,680 --> 00:49:00,445
It must be here somewhere.

153
00:49:01,760 --> 00:49:04,604
What is this?

154
00:49:07,320 --> 00:49:11,723
We found something.
Send backup and an ambulance.

155
00:50:05,580 --> 00:50:07,662
My God!

156
00:50:31,740 --> 00:50:36,871
- So this is where you were hiding?
- And there comes the mother.

157
00:50:37,020 --> 00:50:39,148
Hi.

158
00:51:06,340 --> 00:51:08,422
- Hi.
- Hi.

159
00:51:09,620 --> 00:51:14,228
- Want me to open the curtains?
- Yes, please.

160
00:51:23,220 --> 00:51:29,182
We found the child again, hidden
at a farm. Does that make any sense?

161
00:51:30,220 --> 00:51:32,791
Did he kill it?

162
00:51:34,540 --> 00:51:37,066
He? Who do you mean?

163
00:51:54,900 --> 00:51:59,189
Southern Norway has been
through a warm spell...

164
00:51:59,340 --> 00:52:02,230
Now it's good, this placement.

165
00:52:02,380 --> 00:52:06,624
So when you have commercial breaks.
you can watch it all here.

166
00:52:26,300 --> 00:52:28,621
That's good.

167
00:52:33,100 --> 00:52:36,183
It looks like ultrasound.

168
00:52:36,340 --> 00:52:40,584
My wife is pregnant.
It looks like this.

169
00:52:52,500 --> 00:52:56,949
Refugees escaped from
the reception center every week.

170
00:52:57,100 --> 00:53:00,309
As police,
there isn't much we can do.

171
00:53:00,500 --> 00:53:06,746
Tell me if they make any trouble.
I will find jobs for them all.

172
00:53:14,220 --> 00:53:20,182
He must have taken the baby
from outside the reception center.

173
00:53:33,500 --> 00:53:35,980
How does it go again?

174
00:54:14,340 --> 00:54:16,308
Hi!

175
00:55:03,700 --> 00:55:06,624
What a lovely little foot.

176
00:55:09,420 --> 00:55:13,744
So pretty.
Let's put on your little socks.

177
00:55:13,900 --> 00:55:16,710
There. So you don't get cold.

178
00:55:16,860 --> 00:55:19,909
Look how pretty you are now,

179
00:55:22,020 --> 00:55:24,307
so pretty.

180
00:55:33,100 --> 00:55:35,387
Come here.

181
00:55:38,820 --> 00:55:41,187
What do you want?

182
00:56:01,140 --> 00:56:04,861
So... We'll let you know.

183
00:56:05,020 --> 00:56:07,102
Thank you.

184
00:56:14,180 --> 00:56:20,142
He gave her the baby.
And then told me where to find it.

185
00:56:48,260 --> 00:56:50,547
- Hi.
- What did they say?

186
00:56:50,700 --> 00:56:53,101
I didn't report it.

187
00:57:39,220 --> 00:57:42,827
She became pregnant with William.

188
00:58:02,420 --> 00:58:05,663
Ruud! I have to borrow your car!

189
00:58:06,420 --> 00:58:10,391
Ruud!
You have to let me borrow your car!

190
00:58:10,580 --> 00:58:14,187
Ruud, Janne is in labor.
Lend me your car!

191
00:58:14,340 --> 00:58:18,061
She needs to get to the hospital!
Ruud!

192
00:59:58,700 --> 01:00:02,147
They drove around for a few days.

193
01:00:03,060 --> 01:00:06,542
The two of them
and the newborn boy.

194
01:00:09,660 --> 01:00:12,789
He checked into various motels.

195
01:00:14,340 --> 01:00:17,423
She was ill,
and was feeling unwell.

196
01:00:41,580 --> 01:00:46,222
On the fourth day,
she managed to escape from him.

197
01:00:55,940 --> 01:00:58,830
Are you all right?

198
01:01:04,980 --> 01:01:07,426
Are you all right?

199
01:01:30,660 --> 01:01:33,743
Later they lost that child.

200
01:02:02,020 --> 01:02:04,830
He could have killed her.

201
01:02:19,100 --> 01:02:21,023
Janne?

202
01:02:23,420 --> 01:02:28,062
- You realize we can't keep her?
- Yes.

203
01:02:28,220 --> 01:02:31,269
- You do realize that?
- I do.

204
01:02:45,220 --> 01:02:50,431
You're telling me you saw him
and some Chinese woman.

205
01:02:56,980 --> 01:03:02,225
I read that in China they leave
the baby girls in the forest.

206
01:03:03,340 --> 01:03:07,504
- Isn't that so?
- We aren't in China now.

207
01:03:30,900 --> 01:03:32,982
Christ!

208
01:03:45,780 --> 01:03:48,067
Look at him.

209
01:04:11,420 --> 01:04:15,709
So the child was placed in the river
near the cabin?

210
01:04:15,860 --> 01:04:18,147
Yes, she was.

211
01:04:21,180 --> 01:04:26,744
And you didn't bring her here
because you don't have a baby seat?

212
01:04:29,100 --> 01:04:31,626
That's right.

213
01:04:34,860 --> 01:04:37,591
- And we know who did it.
- Really?

214
01:04:37,740 --> 01:04:41,062
William's disturbed brother
can do such things.

215
01:04:41,220 --> 01:04:44,429
I saw him down by the riverbank.

216
01:04:45,980 --> 01:04:50,349
I'd like to reconstruct that
before we question you any further.

217
01:04:50,540 --> 01:04:53,703
Do that. For Gods sake.

218
01:04:54,140 --> 01:04:58,589
You are aware that you're
not allowed to leave town?

219
01:05:18,820 --> 01:05:23,189
Forget about that policewoman.
She's an idiot.

220
01:05:35,900 --> 01:05:39,143
Do you want to go back to the any?

221
01:05:40,180 --> 01:05:42,103
No.

222
01:05:58,300 --> 01:06:02,624
I lay down on the beach,
waited for the tsunami to return.

223
01:06:04,180 --> 01:06:06,706
But it never came.

224
01:06:12,220 --> 01:06:14,791
Was out shopping.

225
01:06:16,060 --> 01:06:19,064
I saw the wave on my way back.

226
01:06:23,540 --> 01:06:26,510
I stopped my car. Got out.

227
01:06:28,780 --> 01:06:31,101
AH I heard was the sound.

228
01:06:34,660 --> 01:06:39,871
When I made it back to where I lived,
everything was gone.

229
01:06:45,060 --> 01:06:47,825
A little girl used to live there.

230
01:06:50,060 --> 01:06:53,667
She wasn't that small.
Ten, maybe.

231
01:06:56,580 --> 01:06:59,629
She used to stand
in front of the mirror.

232
01:06:59,780 --> 01:07:03,262
Making faces, putting on makeup.

233
01:07:05,260 --> 01:07:08,343
She was the first one we found.

234
01:07:10,100 --> 01:07:15,504
Her body was all tangled up
in the branches of a tree.

235
01:07:18,260 --> 01:07:22,424
Her head hung down
like a little... nut.

236
01:07:27,300 --> 01:07:31,385
Later we found her parents.
They were also dead.

237
01:07:31,580 --> 01:07:34,186
Just as well, probably.

238
01:07:42,220 --> 01:07:47,147
The strange thing over there,
was how often I thought of Ruud.

239
01:07:48,700 --> 01:07:52,022
Through all that chaos.

240
01:07:52,180 --> 01:07:55,741
And the silence that followed.

241
01:07:57,060 --> 01:07:59,791
The rage in that wave.

242
01:08:02,100 --> 01:08:04,501
If it was rage.

243
01:08:06,020 --> 01:08:08,626
Made me think of him.

244
01:08:09,660 --> 01:08:13,506
Ruud. My big brother.

245
01:08:14,540 --> 01:08:17,908
- Is that why you came back?
- No.

246
01:08:22,500 --> 01:08:25,583
I came back to be with you.

247
01:09:00,540 --> 01:09:03,191
Can't we just disappear?

248
01:09:54,340 --> 01:09:56,741
Look in the bag.

249
01:10:03,860 --> 01:10:06,147
He isn't gone.

250
01:10:09,900 --> 01:10:14,986
He's in the birds in the sky,
and in the leaves on the trees.

251
01:10:17,300 --> 01:10:21,021
He is in everything you see
here on earth.

252
01:10:22,780 --> 01:10:25,067
Put it on

253
01:10:32,580 --> 01:10:34,867
Don't cry.

254
01:11:47,620 --> 01:11:50,271
It wasn't my fault.

255
01:11:52,740 --> 01:11:54,788
Hey...

256
01:11:55,820 --> 01:11:59,905
Hey, you...
What are you thinking about?

257
01:14:29,380 --> 01:14:34,750
When you reported your child missing,
you came with a man.

258
01:14:34,900 --> 01:14:37,585
Is he the one who locked you up?

259
01:14:37,740 --> 01:14:40,346
He is my friend.

260
01:14:40,540 --> 01:14:44,545
What happened after we
returned the child to you?

261
01:14:45,580 --> 01:14:49,665
Where is he?
Why did he not come back?

262
01:15:07,540 --> 01:15:10,225
Police came with her.

263
01:15:13,100 --> 01:15:15,148
Thank you.

264
01:15:28,540 --> 01:15:31,271
Now they want us to go back.

265
01:15:34,500 --> 01:15:37,344
Can I come for hiding too?

266
01:15:57,580 --> 01:16:01,630
I'll come back and pick up the rest.

267
01:16:04,580 --> 01:16:07,424
I have an extra car.

268
01:16:18,380 --> 01:16:24,342
Right. You three, go down there.
You two, just sit down here, OK?

269
01:19:03,100 --> 01:19:04,306
Come.

270
01:19:18,180 --> 01:19:20,421
Put this on.

271
01:19:48,620 --> 01:19:51,305
You want girlfriend?

272
01:20:06,340 --> 01:20:09,423
I want to live in this room.

273
01:20:09,620 --> 01:20:12,305
It's not so dark.

274
01:20:13,380 --> 01:20:16,190
You can't go in there now.

275
01:20:43,220 --> 01:20:47,111
The nurse said you
Tried to commit suicide.

276
01:20:47,260 --> 01:20:50,230
What do you feel guilty about?

277
01:20:51,860 --> 01:20:55,581
They killed each other, didn't they?

278
01:20:55,740 --> 01:20:59,790
One with a stone,
the other with a knife.

279
01:21:00,820 --> 01:21:03,505
Yes. Something like that.

280
01:21:12,180 --> 01:21:14,706
Thought you might want this.

281
01:25:46,260 --> 01:25:48,342
Hi.

282
01:25:52,580 --> 01:25:55,345
I brought you some food.

283
01:26:03,260 --> 01:26:05,342
Thank you.

284
01:26:10,660 --> 01:26:16,827
They say it will stay warm a few
more weeks, but then it's over.

285
01:26:17,900 --> 01:26:20,949
We'll be long gone by then.

286
01:26:22,860 --> 01:26:25,670
Have you harvested your grain?

287
01:26:26,940 --> 01:26:30,183
I don't do that anymore.
It doesn't pay.

288
01:26:33,420 --> 01:26:36,264
And what about that child?

289
01:26:38,020 --> 01:26:42,582
- The one you sent down the river.
- She lives with me now.

290
01:26:42,740 --> 01:26:45,220
With her mother.

291
01:26:49,180 --> 01:26:52,707
So she's your girlfriend?
The baby's mother?

292
01:28:20,700 --> 01:28:23,180
You're freezing

293
01:29:20,060 --> 01:29:21,744
- No.
- Come.

294
01:29:21,900 --> 01:29:24,665
- Let go of me!
- Come, come.

295
01:29:26,420 --> 01:29:27,865
No!

296
01:29:58,580 --> 01:30:00,423
Stop!

297
01:30:04,340 --> 01:30:06,388
No!

298
01:31:44,540 --> 01:31:47,908
I'll bring more food tomorrow.

299
01:31:52,820 --> 01:31:57,109
And I can bring the baby too,
if you want.

300
01:33:06,820 --> 01:33:09,300
It's warm now.

301
01:33:32,500 --> 01:33:35,583
- Please.
- No, I can't.

302
01:33:35,740 --> 01:33:37,822
Please!

303
01:33:39,740 --> 01:33:41,629
No.

304
01:33:45,540 --> 01:33:47,622
Calm down.

305
01:36:36,340 --> 01:36:38,741
Help me.

306
01:38:35,180 --> 01:38:39,026
- So that's how I look inside.
- Like any adult woman.

307
01:38:39,180 --> 01:38:43,981
It doesn't feel representative.
Can I complain?

308
01:38:52,380 --> 01:38:56,305
I don't know
what to think about her.

309
01:38:56,500 --> 01:38:59,868
About what she has experienced.

310
01:39:06,660 --> 01:39:08,947
Is she a murderer?

311
01:39:11,140 --> 01:39:13,507
Or a victim?

312
01:39:22,700 --> 01:39:26,591
Once she was nothing
but a little girl.

