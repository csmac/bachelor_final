1
00:00:05,915 --> 00:00:08,485
My name is Jean-Claude Van Damme.

2
00:00:08,775 --> 00:00:11,260
I used to be super famous.

3
00:00:12,139 --> 00:00:15,492
Perhaps you remember my first
starring role in "Bloodsport."

4
00:00:16,092 --> 00:00:18,427
It is on television all of the time.

5
00:00:19,388 --> 00:00:20,866
Or maybe you've seen "Timecop,"

6
00:00:21,314 --> 00:00:23,800
which is like "Looper,"
starring Bruce Willis,

7
00:00:24,259 --> 00:00:26,339
but like a million times better.

8
00:00:27,315 --> 00:00:30,523
But this is not a movie.

9
00:00:31,040 --> 00:00:33,510
And that man is not an actor.

10
00:00:33,576 --> 00:00:35,994
He's trying to kill me.

11
00:00:38,681 --> 00:00:40,795
But I'm not worried because...

12
00:00:41,779 --> 00:00:43,084
I have a gift.

13
00:00:44,854 --> 00:00:47,963
I am the master of the splits.

14
00:00:50,325 --> 00:00:52,729
Or at least I used to be.

15
00:00:54,197 --> 00:00:55,197
Fuck.

16
00:01:04,540 --> 00:01:06,171
How did this happen?

17
00:01:08,089 --> 00:01:09,719
How did I get here?

18
00:01:19,455 --> 00:01:23,465
JC, I used your shower
and now I'm all sticky.

19
00:01:23,492 --> 00:01:25,374
It's coconut water.

20
00:01:26,193 --> 00:01:29,063
Your shower uses coconut water?

21
00:01:29,129 --> 00:01:32,829
All of the plumbing uses coconut water.

22
00:01:33,267 --> 00:01:34,897
I don't understand.

23
00:01:34,969 --> 00:01:36,869
Of course not.

24
00:01:36,937 --> 00:01:38,341
You never do.

25
00:01:38,606 --> 00:01:41,506
This is the first time I've been here.

26
00:01:46,313 --> 00:01:47,723
Let me help you get dressed.

27
00:01:47,782 --> 00:01:48,922
Okay.

28
00:03:18,804 --> 00:03:20,334
Hey, excuse me. Excuse me.

29
00:03:21,280 --> 00:03:22,776
My, uh, ramen is dry.

30
00:03:22,841 --> 00:03:24,281
Yeah, man, that's what we do.

31
00:03:24,342 --> 00:03:25,612
Dry ramen.

32
00:03:25,678 --> 00:03:28,618
Uh, can you put some, uh, water on mine?

33
00:03:28,680 --> 00:03:30,241
We actually don't have access to water.

34
00:03:30,640 --> 00:03:33,591
We're a pop up experience,
not a restaurant.

35
00:03:33,618 --> 00:03:35,318
Okay, I don't understand.

36
00:03:35,386 --> 00:03:36,386
Dude.

37
00:03:38,226 --> 00:03:39,482
Oh, hey, no shit.

38
00:03:39,591 --> 00:03:40,901
Where you been, man?

39
00:03:40,958 --> 00:03:42,298
You got anything coming out?

40
00:03:42,359 --> 00:03:43,359
I'm retired.

41
00:03:43,394 --> 00:03:45,094
What about on, uh, DVD?

42
00:03:45,162 --> 00:03:46,162
VOD?

43
00:03:46,197 --> 00:03:48,597
No, no. Real retired.

44
00:03:48,633 --> 00:03:50,737
Not like Nicholas Cage retired.

45
00:03:51,703 --> 00:03:53,603
Oh, fuck, man.

46
00:03:53,605 --> 00:03:55,775
I thought you were Nicholas Cage.

47
00:04:12,623 --> 00:04:14,723
Shooting's about finesse, JC.

48
00:04:15,792 --> 00:04:17,662
You gotta have a soft touch.

49
00:04:17,728 --> 00:04:20,179
Yeah, I'm not exactly
known for my soft touch.

50
00:04:20,974 --> 00:04:22,243
Let's try this.

51
00:04:22,909 --> 00:04:25,219
You told me about your favorite
childhood memory once.

52
00:04:25,278 --> 00:04:26,548
You remember that?

53
00:04:26,580 --> 00:04:28,720
Yeah. The farm.

54
00:04:30,450 --> 00:04:32,424
Your grandfather's emu farm.

55
00:04:33,119 --> 00:04:34,199
Picture it.

56
00:04:36,423 --> 00:04:38,205
It's a beautiful day.

57
00:04:40,320 --> 00:04:42,017
Nowhere for you to be.

58
00:04:43,001 --> 00:04:45,387
Nothing for you to do.

59
00:04:46,266 --> 00:04:48,666
It's just you,

60
00:04:48,735 --> 00:04:50,639
and those big...

61
00:04:51,671 --> 00:04:53,031
majestic...

62
00:04:55,641 --> 00:04:56,741
...birds.

63
00:05:00,445 --> 00:05:01,915
Whoa.

64
00:05:06,251 --> 00:05:07,486
I love you.

65
00:05:07,987 --> 00:05:09,127
I love you.

66
00:05:17,869 --> 00:05:19,097
Vanessa.

67
00:05:22,602 --> 00:05:24,772
- What?
- Shit.

68
00:05:24,837 --> 00:05:26,381
- Hey, Vanessa, I called...
- I'm late for a flight.

69
00:05:26,405 --> 00:05:28,656
I wrote, I tried everything.

70
00:05:29,942 --> 00:05:31,012
I miss you.

71
00:05:31,327 --> 00:05:32,971
What do you want me to say to that?

72
00:05:33,745 --> 00:05:35,845
Let's have dinner tonight.
Let me explain.

73
00:05:35,914 --> 00:05:38,014
I'm flying to Bulgaria in three hours.

74
00:05:38,082 --> 00:05:39,471
For a job?

75
00:05:40,631 --> 00:05:41,654
But I thought you quit.

76
00:05:41,720 --> 00:05:44,790
I didn't quit working, JC.

77
00:05:44,856 --> 00:05:46,626
I just quit working with you.

78
00:05:47,926 --> 00:05:50,126
You take care of yourself, okay?

79
00:05:52,810 --> 00:05:53,970
Vanessa.

80
00:05:57,035 --> 00:05:58,935
Hey, bro, you forgot your dry ramen.

81
00:06:02,440 --> 00:06:03,559
Val Kilmer?

82
00:06:15,085 --> 00:06:16,085
Jane.

83
00:06:16,353 --> 00:06:17,996
It's your favorite client.

84
00:06:18,455 --> 00:06:19,655
I'm back.

85
00:06:22,425 --> 00:06:24,835
- Jane.
- JC.

86
00:06:24,895 --> 00:06:27,705
Goddamn, I got such a hard
on when I heard your voice.

87
00:06:27,765 --> 00:06:29,705
Ooh. Ooh, ooh, ooh.

88
00:06:29,767 --> 00:06:31,444
Hey, you ready to
pop the trunk on this shit?

89
00:06:31,468 --> 00:06:33,228
- Yes.
- Let's go.

90
00:06:33,303 --> 00:06:34,303
Where have you been?

91
00:06:34,337 --> 00:06:35,847
I've been relaxing.

92
00:06:35,906 --> 00:06:39,676
Uh-huh. That explains no
Christmas cards, birthday cards.

93
00:06:39,710 --> 00:06:41,658
Okay, first up.

94
00:06:42,145 --> 00:06:44,955
Action re-imagining of
"Rikki-Tikki-Tavi."

95
00:06:45,316 --> 00:06:46,760
Channing Tatum's playing the mongoose,

96
00:06:46,784 --> 00:06:48,054
you'd be one of the cobras.

97
00:06:48,118 --> 00:06:49,518
This is set up at Paramount.

98
00:06:49,586 --> 00:06:50,696
Jane.

99
00:06:51,989 --> 00:06:55,158
Action re-imaging of
"Anne of Green Gables."

100
00:06:55,224 --> 00:06:56,994
This was on the black list last year.

101
00:06:57,059 --> 00:06:59,529
Kid who wrote it is
a garbage man in Fresno.

102
00:06:59,595 --> 00:07:01,795
Uh, honey, those are not edible.

103
00:07:02,164 --> 00:07:03,403
Oh.

104
00:07:04,967 --> 00:07:06,927
That's at Paramount.

105
00:07:07,003 --> 00:07:11,273
Okay. This isn't Paramount.

106
00:07:11,340 --> 00:07:12,840
- Jane.
- Origin story.

107
00:07:12,909 --> 00:07:14,709
Of the restaurant P.F. Chang's.

108
00:07:14,711 --> 00:07:16,241
But imagined as an action movie.

109
00:07:16,312 --> 00:07:20,152
You'd be Richard Shaw,
sidekick to Phineas Fog Chang.

110
00:07:20,216 --> 00:07:22,126
- Channing Tatum is attached.
- Jane.

111
00:07:22,184 --> 00:07:23,924
Jackie Chan's playing General Tso,

112
00:07:23,986 --> 00:07:26,196
he's rides a giant CG chicken.

113
00:07:26,255 --> 00:07:27,325
This is gonna be great.

114
00:07:27,389 --> 00:07:28,719
Jane!

115
00:07:30,760 --> 00:07:32,502
When I told you I was back,

116
00:07:33,329 --> 00:07:34,858
I did not mean JCVD.

117
00:07:37,733 --> 00:07:40,903
I meant... Johnson.

118
00:07:45,406 --> 00:07:46,836
I see.

119
00:07:50,478 --> 00:07:53,108
Director authorization recognized.

120
00:07:53,181 --> 00:07:55,921
Security protocols engaged.

121
00:07:55,984 --> 00:07:58,324
Johnson has been retired for two years.

122
00:08:01,356 --> 00:08:03,456
That's an eternity in this line of work.

123
00:08:05,360 --> 00:08:06,760
Room secured.

124
00:08:06,795 --> 00:08:10,064
I stay active. I play Frisbee golf.

125
00:08:10,766 --> 00:08:12,626
This isn't a game, Jean-Claude.

126
00:08:12,768 --> 00:08:14,228
It's black ops.

127
00:08:15,770 --> 00:08:17,670
Real black ops, not the game.

128
00:08:17,772 --> 00:08:20,312
I send you out there before
you're ready, people die.

129
00:08:20,374 --> 00:08:22,214
No one's going to die.

130
00:08:22,276 --> 00:08:25,399
Except for all the people
I'm going to kill.

131
00:08:26,239 --> 00:08:29,183
I'll put out feelers, see if there
are any low risk assignments.

132
00:08:29,249 --> 00:08:30,711
I want Bulgaria.

133
00:08:32,023 --> 00:08:33,621
Oh, the job Vanessa's working.

134
00:08:33,788 --> 00:08:36,658
Honey, that ship has sailed.
You had your chance.

135
00:08:36,791 --> 00:08:38,361
I want Bulgaria.

136
00:08:38,425 --> 00:08:39,785
That job is booked.

137
00:08:40,212 --> 00:08:41,600
My guy's already on the ground.

138
00:08:41,661 --> 00:08:42,661
Who is it?

139
00:08:43,030 --> 00:08:45,670
Jones? Thompson?

140
00:08:45,800 --> 00:08:47,800
- Brown.
- Brown.

141
00:08:49,311 --> 00:08:51,144
Brown is a monster.

142
00:08:51,597 --> 00:08:54,705
His, uh, methods are a bit
extreme, but he's good.

143
00:08:55,448 --> 00:08:59,449
Brown is good, but Johnson is better.

144
00:09:01,815 --> 00:09:03,507
I can make the call.

145
00:09:04,117 --> 00:09:06,287
For you, I'll always make the call,

146
00:09:06,352 --> 00:09:09,522
but this isn't just about the
client and you know that.

147
00:09:09,588 --> 00:09:11,188
You need the cover job, too.

148
00:09:11,257 --> 00:09:14,257
And that movie starts
shooting in two days.

149
00:09:14,327 --> 00:09:16,275
Jane, I trust you.

150
00:09:16,930 --> 00:09:18,380
I'm going to pack.

151
00:09:19,098 --> 00:09:22,198
JC, please tell me
this isn't about Vanessa.

152
00:09:24,103 --> 00:09:25,413
JC!

153
00:09:35,614 --> 00:09:37,043
Well, JC, good news.

154
00:09:37,115 --> 00:09:38,783
I worked my magic.

155
00:09:39,184 --> 00:09:42,454
The mission and cover job are yours.

156
00:09:42,775 --> 00:09:45,661
That's right, Bulgaria's a go.

157
00:09:46,077 --> 00:09:49,254
But you're going up against
bloodthirsty drug runners,

158
00:09:49,327 --> 00:09:52,027
so, yeah, I hope you're ready.

159
00:09:52,097 --> 00:09:55,467
Because... ready or not,

160
00:09:55,533 --> 00:09:58,533
Johnson is back.

161
00:09:58,603 --> 00:09:59,763
Hiya.

162
00:10:01,473 --> 00:10:02,473
Oh, shit.

163
00:10:20,491 --> 00:10:21,531
Ta-dah!

164
00:10:22,141 --> 00:10:23,522
Motherfucker.

165
00:10:26,763 --> 00:10:28,833
I can't believe you pulled this.

166
00:10:28,900 --> 00:10:31,310
- Vanessa...
- Don't, don't even.

167
00:10:31,628 --> 00:10:33,909
You know how much this job means to me.

168
00:10:33,938 --> 00:10:35,611
How hard I've worked to get here.

169
00:10:36,173 --> 00:10:37,907
I'm not letting you fuck that up for me.

170
00:10:42,713 --> 00:10:44,473
Get to work or get lost, your call.

171
00:10:55,092 --> 00:10:56,952
Heroin cut with ketamine.

172
00:10:57,028 --> 00:10:59,397
Kids call it HK. Hong Kong.

173
00:10:59,462 --> 00:11:01,931
It's being smuggled in
cans of motor oil.

174
00:11:01,932 --> 00:11:04,962
It's highly addictive and highly lethal.

175
00:11:05,936 --> 00:11:07,106
All right, you're set.

176
00:11:10,140 --> 00:11:11,310
We know the source?

177
00:11:11,374 --> 00:11:13,444
Intel points to
a factory outside of town.

178
00:11:13,509 --> 00:11:15,670
I'm gonna run recon this
morning while you're shooting.

179
00:11:16,163 --> 00:11:17,042
If it checks out,

180
00:11:17,113 --> 00:11:18,790
we'll get you in there
at the end of the week.

181
00:11:18,814 --> 00:11:19,944
End of the week?

182
00:11:20,379 --> 00:11:21,427
I thought it might make sense for you

183
00:11:21,451 --> 00:11:23,951
to spend the week
warming up with stunt work.

184
00:11:23,987 --> 00:11:25,957
Before you have to do the real thing.

185
00:11:26,022 --> 00:11:27,022
You know.

186
00:11:27,423 --> 00:11:29,223
On the off chance
you didn't walk in here

187
00:11:29,292 --> 00:11:31,592
in peak physical condition.

188
00:11:32,155 --> 00:11:34,291
Wait, stunt work.

189
00:11:34,364 --> 00:11:35,764
In this movie?

190
00:11:35,831 --> 00:11:39,061
You haven't even looked at the script.

191
00:11:39,135 --> 00:11:41,004
Vanessa.

192
00:11:42,237 --> 00:11:45,407
- Hey.
- Maybe you've forgotten, JC,

193
00:11:45,474 --> 00:11:48,582
but what you do is fucking dangerous.

194
00:11:48,998 --> 00:11:50,983
You waltz in here unprepared,

195
00:11:51,366 --> 00:11:54,447
it's not just you that's gonna end
up splattered across the field.

196
00:12:04,793 --> 00:12:05,893
Where's Brown?

197
00:12:06,473 --> 00:12:08,565
Last minute personnel change.

198
00:12:08,630 --> 00:12:09,630
This is Johnson.

199
00:12:09,895 --> 00:12:12,223
Johnson, meet Luis.

200
00:12:12,356 --> 00:12:13,201
Where's Marcus?

201
00:12:13,268 --> 00:12:15,908
Retired. For real retired.

202
00:12:16,005 --> 00:12:18,687
Not Jean-Claude Van Damme retired.

203
00:12:19,776 --> 00:12:20,878
Is he qualified?

204
00:12:21,009 --> 00:12:23,739
I was top of my class
at the Paul Mitchell academy.

205
00:12:24,151 --> 00:12:27,111
Plus a Beacon award at
the PBA Beauty Week 2014.

206
00:12:27,455 --> 00:12:28,581
And before that?

207
00:12:33,021 --> 00:12:36,587
I spent nine years as a child
soldier in el cartel de Los Zetas.

208
00:12:37,125 --> 00:12:38,895
- Mr. Van Damme.
- Yep.

209
00:12:39,027 --> 00:12:40,497
They're ready for you now.

210
00:12:47,501 --> 00:12:50,431
We could run away, head out west.

211
00:12:52,173 --> 00:12:54,645
We have more than enough gold left.

212
00:12:55,376 --> 00:12:58,046
No more running. It ends tonight.

213
00:13:05,152 --> 00:13:06,352
He's here.

214
00:13:16,063 --> 00:13:17,463
I love you, Tom Sawyer.

215
00:13:17,530 --> 00:13:19,201
Oh, Huck.

216
00:13:22,069 --> 00:13:23,833
Huckleberry Finn.

217
00:13:24,504 --> 00:13:25,504
Pop.

218
00:13:26,739 --> 00:13:27,739
Daddy's home.

219
00:13:31,278 --> 00:13:34,108
Ahh. There's a new man of the house.

220
00:14:10,716 --> 00:14:11,756
What the fuck!

221
00:14:11,817 --> 00:14:13,336
Cut! Cut.

222
00:14:14,753 --> 00:14:16,223
Okay, reset.

223
00:14:16,288 --> 00:14:18,388
Uh, second team, let's...

224
00:14:18,457 --> 00:14:20,117
Chrissy, baby. I am really sorry.

225
00:14:20,159 --> 00:14:21,489
- Fix it.
- On it.

226
00:14:21,560 --> 00:14:23,129
Okay, uh, yeah, yeah, yeah.

227
00:14:23,130 --> 00:14:25,758
Uh, I know the choreography is tough.

228
00:14:25,830 --> 00:14:27,600
Um, it's kind of my trademark, though.

229
00:14:27,665 --> 00:14:30,135
Yes, I remember from the commercial

230
00:14:30,201 --> 00:14:31,601
with the, uh, dancing hamsters.

231
00:14:31,669 --> 00:14:33,439
- Yeah, exactly.
- I know, it's so...

232
00:14:33,504 --> 00:14:35,585
That's okay, this is what
we're gonna do. Sit down...

233
00:14:39,625 --> 00:14:41,146
How's he looking?

234
00:14:43,214 --> 00:14:44,384
Not good.

235
00:14:45,883 --> 00:14:47,554
What's the story with you two?

236
00:14:51,222 --> 00:14:52,492
We were a team.

237
00:14:53,791 --> 00:14:56,691
I did hair and recon, he...

238
00:14:57,333 --> 00:14:58,848
did what he does,

239
00:14:59,390 --> 00:15:01,202
and eventually,

240
00:15:01,710 --> 00:15:03,525
it's the classic story,

241
00:15:04,168 --> 00:15:07,338
he spinning back heel
kicked his way into my heart.

242
00:15:09,821 --> 00:15:13,884
The chemistry was instant.
It was undeniable.

243
00:15:14,405 --> 00:15:16,554
But we didn't want
to jeopardize the work,

244
00:15:17,013 --> 00:15:18,553
so we tried to fight it.

245
00:15:18,615 --> 00:15:21,945
You know, we shouldn't
be doing this at all.

246
00:15:22,018 --> 00:15:23,518
We should not be...

247
00:15:23,920 --> 00:15:26,757
But every time we came back
together for a mission,

248
00:15:26,823 --> 00:15:29,033
we picked up right
where we had left off,

249
00:15:29,092 --> 00:15:33,632
until finally we realized there
was no point in fighting it.

250
00:15:33,697 --> 00:15:36,327
So we decided to take some time off.

251
00:15:36,772 --> 00:15:38,070
Together.

252
00:15:38,533 --> 00:15:41,043
I went to Bali while he
finished up his press tour

253
00:15:41,104 --> 00:15:42,949
and I waited for him.

254
00:15:45,580 --> 00:15:46,781
But he never showed up.

255
00:15:50,213 --> 00:15:51,587
I went my way.

256
00:15:52,848 --> 00:15:54,518
He went his and...

257
00:15:54,583 --> 00:15:56,853
eventually, he retired altogether.

258
00:15:57,211 --> 00:15:58,427
And now he's back.

259
00:15:59,121 --> 00:16:00,491
Now he's back.

260
00:16:03,492 --> 00:16:04,832
So what's your story?

261
00:16:10,333 --> 00:16:12,103
It's not that interesting.

262
00:16:12,235 --> 00:16:13,365
I'll tell you later.

263
00:16:15,195 --> 00:16:19,507
You're right. With Pop dead...

264
00:16:20,157 --> 00:16:22,846
it would be easy now
to go someplace warm.

265
00:16:23,524 --> 00:16:24,828
We got a problem.

266
00:16:24,890 --> 00:16:27,522
We'll be fine, I just need
to stretch a bit more.

267
00:16:27,582 --> 00:16:29,022
Not you.

268
00:16:29,083 --> 00:16:30,281
It's the factory.

269
00:16:32,889 --> 00:16:35,254
I don't know how, but they
must have gotten tipped off.

270
00:16:35,662 --> 00:16:37,454
They've started moving the drugs out.

271
00:16:37,710 --> 00:16:38,926
How long do we have?

272
00:16:39,189 --> 00:16:40,493
It'll be empty by tonight.

273
00:16:40,821 --> 00:16:43,502
Which means you need to find a
way to change the schedule.

274
00:16:43,565 --> 00:16:47,265
You need to be wrapped by 6, 7 PM
at the latest.

275
00:16:52,108 --> 00:16:53,251
...and, you know, couldn't do it.

276
00:16:53,275 --> 00:16:55,005
Real hamsters is more practical.

277
00:16:55,076 --> 00:16:56,086
- They did good, though.
- Excuse me.

278
00:16:56,110 --> 00:16:57,510
Ah, JC. Perfect.

279
00:16:57,579 --> 00:16:59,089
I want you to meet one of your co-stars,

280
00:16:59,113 --> 00:17:00,284
this is Victor.

281
00:17:01,048 --> 00:17:02,348
Big fan, Mr. Van Damme.

282
00:17:02,417 --> 00:17:04,017
Really excited to be working with you.

283
00:17:04,085 --> 00:17:05,415
- Same here.
- Thank you.

284
00:17:05,487 --> 00:17:07,286
Victor's playing "N" word Jim.

285
00:17:09,556 --> 00:17:10,556
Okay.

286
00:17:11,625 --> 00:17:13,325
Can I talk to you for a second?

287
00:17:13,804 --> 00:17:15,365
Yeah. Yeah.

288
00:17:15,429 --> 00:17:16,599
What's up, babe?

289
00:17:18,088 --> 00:17:20,860
Well, it's about, uh, my choreography.

290
00:17:22,355 --> 00:17:24,235
It doesn't have the right feel.

291
00:17:25,606 --> 00:17:27,723
Okay, so... Okay.

292
00:17:29,310 --> 00:17:31,320
I have TBS, all right?

293
00:17:31,378 --> 00:17:33,748
So I've seen "Kickboxer"
like, a hundy times, right?

294
00:17:33,814 --> 00:17:36,290
I love it just like everybody else.

295
00:17:36,635 --> 00:17:38,316
But that '80s style of fighting,

296
00:17:38,319 --> 00:17:39,689
the style that you're known for,

297
00:17:39,753 --> 00:17:43,023
with the kicking and the
spinning and the splits

298
00:17:43,089 --> 00:17:44,301
in the middle of the s... you know,

299
00:17:44,325 --> 00:17:46,025
with the guys one at time coming...

300
00:17:46,092 --> 00:17:47,121
It's not realistic.

301
00:17:48,361 --> 00:17:49,791
And we all know that now.

302
00:17:50,229 --> 00:17:51,229
You know?

303
00:17:51,931 --> 00:17:53,861
All I'm saying maybe we
can spend half a day...

304
00:17:53,933 --> 00:17:57,063
Oh, no, it's okay. I get
it, I get it, I get it.

305
00:17:57,136 --> 00:17:58,536
I... I hear you.

306
00:17:58,604 --> 00:18:00,974
But hey, we're gonna
roll with the punches.

307
00:18:02,742 --> 00:18:05,141
Pow. Oh.

308
00:18:06,412 --> 00:18:08,478
Roll with the punches.

309
00:18:10,082 --> 00:18:11,782
Camera's set!

310
00:18:11,851 --> 00:18:12,851
Action!

311
00:18:14,053 --> 00:18:15,623
- Cut! Cut!
- JC.

312
00:18:15,688 --> 00:18:17,688
- I'm so, so sorry, man.
- Holy shit.

313
00:18:17,757 --> 00:18:19,435
I totally... I totally thought
you were gonna duck.

314
00:18:19,459 --> 00:18:21,499
Okay, that's a wrap, everyone.

315
00:18:21,561 --> 00:18:23,361
It's simple.

316
00:18:23,363 --> 00:18:25,263
Get in, find the HK,

317
00:18:25,767 --> 00:18:29,165
then all you need to do is
get one of these trackers

318
00:18:29,233 --> 00:18:30,403
on one of their shipments.

319
00:18:31,803 --> 00:18:34,033
Remember, if you blow your cover,

320
00:18:34,105 --> 00:18:37,045
the entire operation
goes down in flames,

321
00:18:37,108 --> 00:18:38,948
so stick to the plan.

322
00:18:45,173 --> 00:18:46,173
Johnson.

323
00:18:46,474 --> 00:18:47,884
Here's your weapon.

324
00:18:47,952 --> 00:18:49,622
I've got my own weapons.

325
00:19:02,834 --> 00:19:03,834
Vanessa.

326
00:19:03,868 --> 00:19:04,868
Give me a sec.

327
00:19:07,972 --> 00:19:09,001
Okay, I got you.

328
00:19:10,408 --> 00:19:11,408
I'm in.

329
00:19:11,942 --> 00:19:15,012
I'll meet you back in
my room in 59 minutes.

330
00:19:15,304 --> 00:19:16,889
Your room?

331
00:19:16,947 --> 00:19:20,417
You know, for mission review
and alibi briefing.

332
00:19:20,484 --> 00:19:21,884
Right, yeah, that location's

333
00:19:21,952 --> 00:19:24,082
not gonna work for me this time around.

334
00:19:27,290 --> 00:19:28,790
I can come to your room.

335
00:19:30,461 --> 00:19:32,131
How about we do that dinner?

336
00:19:32,195 --> 00:19:33,878
Hotel restaurant.

337
00:19:34,432 --> 00:19:35,862
Work only.

338
00:19:38,035 --> 00:19:39,505
Work only.

339
00:19:44,141 --> 00:19:46,241
I'm in uniform.

340
00:19:46,309 --> 00:19:47,839
Headed to the entrance.

341
00:19:49,982 --> 00:19:50,917
Slow it down a sec.

342
00:19:50,980 --> 00:19:52,221
I've got a half a dozen workers

343
00:19:52,248 --> 00:19:53,908
coming up on your position.

344
00:19:57,754 --> 00:19:59,224
There's another group approaching.

345
00:19:59,288 --> 00:20:01,488
I need to see which
direction they're headed.

346
00:20:08,540 --> 00:20:09,667
You're right.

347
00:20:09,964 --> 00:20:13,916
With Pop dead, it would be
a more comfortable life.

348
00:20:14,403 --> 00:20:15,903
Comfortable life.

349
00:20:16,475 --> 00:20:18,372
I don't want to do what's easy, Tom.

350
00:20:18,441 --> 00:20:19,482
I want to do what's right.

351
00:20:19,542 --> 00:20:20,836
What did you say?

352
00:20:21,277 --> 00:20:24,047
Wait. What are you doing?

353
00:20:25,782 --> 00:20:26,782
Nothing.

354
00:20:28,284 --> 00:20:31,306
Please tell me you didn't
bring your script.

355
00:20:33,889 --> 00:20:36,659
You said yourself I need to be prepared.

356
00:20:37,003 --> 00:20:37,926
Being prepared for tomorrow's scenes

357
00:20:37,993 --> 00:20:39,493
isn't gonna do you a lot of good

358
00:20:39,561 --> 00:20:40,731
if you're dead tonight.

359
00:20:40,796 --> 00:20:41,796
Down!

360
00:20:51,573 --> 00:20:54,273
Don't worry. Nobody's
going to lay eyes on me.

361
00:20:54,342 --> 00:20:55,642
Hey!

362
00:20:58,183 --> 00:20:59,799
Philippe. How is it going?

363
00:21:01,015 --> 00:21:02,015
Good.

364
00:21:02,111 --> 00:21:04,523
Good, good. Well, I
don't want to keep you.

365
00:21:09,125 --> 00:21:12,957
Okay. The HK should be
through those double doors.

366
00:21:18,533 --> 00:21:19,733
Hey, Philippe.

367
00:21:22,169 --> 00:21:23,457
What is this?

368
00:21:24,271 --> 00:21:25,571
What's happening here?

369
00:21:25,640 --> 00:21:27,840
Just go with it.

370
00:21:27,908 --> 00:21:29,008
Hey, Philippe.

371
00:21:33,581 --> 00:21:34,616
Hey, Philippe.

372
00:21:35,583 --> 00:21:36,897
Hey, Philippe.

373
00:21:43,224 --> 00:21:44,775
Hey, it's just me, Philippe.

374
00:21:49,029 --> 00:21:50,216
Who are you?

375
00:21:51,131 --> 00:21:54,095
Um... I'm you.

376
00:21:54,668 --> 00:21:57,320
From the future,
and I'm here to warn you

377
00:21:58,271 --> 00:22:01,581
that something bad's going
to happen to this factory.

378
00:22:04,542 --> 00:22:05,843
You should leave.

379
00:22:06,445 --> 00:22:08,598
You're not me from the future.

380
00:22:09,048 --> 00:22:10,478
What?

381
00:22:10,584 --> 00:22:13,424
Like matter cannot occupy like space.

382
00:22:13,486 --> 00:22:14,486
"Timecop."

383
00:22:14,588 --> 00:22:16,058
"Timecop."

384
00:22:16,122 --> 00:22:16,992
Have you seen it?

385
00:22:17,056 --> 00:22:19,126
- Yeah.
- So you know the rules.

386
00:22:19,575 --> 00:22:22,862
Like matter cannot occupy like space.

387
00:22:23,505 --> 00:22:26,070
But, uh, in "Looper" Bruce Willis

388
00:22:26,132 --> 00:22:29,602
and Joseph Gordon-Levitt
touch many times.

389
00:22:29,636 --> 00:22:31,666
If you are really me,

390
00:22:32,058 --> 00:22:35,138
do you think "Looper," it's a
better movie than "Timecop"?

391
00:22:38,644 --> 00:22:40,431
- Yes.
- False.

392
00:22:40,813 --> 00:22:44,083
I think "Timecop"
is way better than "Looper."

393
00:22:52,531 --> 00:22:55,325
You've got excellent taste in films.

394
00:23:18,382 --> 00:23:19,652
Okay, I've got the signal.

395
00:23:19,993 --> 00:23:21,022
Now get out of there.

396
00:23:21,085 --> 00:23:23,455
What, are you worried about me?

397
00:23:24,958 --> 00:23:27,692
Well, you gotta admit,
that was a strange one,

398
00:23:27,759 --> 00:23:29,229
even for you.

399
00:23:29,293 --> 00:23:30,553
But hey...

400
00:23:30,662 --> 00:23:32,186
that's mission success.

401
00:23:32,664 --> 00:23:34,185
I'll meet you at the restaurant in 30.

402
00:23:34,496 --> 00:23:35,938
Just don't stand me up this time.

403
00:23:45,309 --> 00:23:46,440
Oh, shit.

404
00:23:47,178 --> 00:23:48,178
Vanessa.

405
00:23:48,713 --> 00:23:49,813
Vanessa, can you hear me?

406
00:23:49,881 --> 00:23:51,251
I've got to go back.

407
00:23:51,315 --> 00:23:52,345
I dropped my script.

408
00:23:52,416 --> 00:23:54,286
It has my fucking name on it.

409
00:24:03,059 --> 00:24:04,429
Psst.

410
00:24:06,596 --> 00:24:07,996
"And what about us?"

411
00:24:08,064 --> 00:24:10,834
"You promised we'd start
a family of our own."

412
00:24:11,208 --> 00:24:12,741
"The family we never had."

413
00:24:15,240 --> 00:24:17,640
"Huck, we already have."

414
00:24:23,714 --> 00:24:25,214
Jean-Claude Van Damme?

415
00:24:30,721 --> 00:24:31,861
Yeah.

416
00:24:33,724 --> 00:24:34,954
Sergey.

417
00:25:21,235 --> 00:25:22,835
JC.

418
00:25:25,807 --> 00:25:28,917
I love you.

419
00:25:38,386 --> 00:25:39,426
Huh?

420
00:25:48,696 --> 00:25:49,896
Hah.

421
00:25:58,038 --> 00:25:59,508
Me want!

422
00:25:59,572 --> 00:26:00,942
No, no, no. One at a time,

423
00:26:01,008 --> 00:26:02,051
or you run into each other.

424
00:26:02,075 --> 00:26:03,575
Could get confusing. Go!

425
00:26:03,643 --> 00:26:05,113
Go!

426
00:26:37,344 --> 00:26:38,574
Come on!

427
00:27:23,688 --> 00:27:24,689
Excuse me.

428
00:27:25,256 --> 00:27:27,966
- It's Vanessa, right?
- Yeah.

429
00:27:29,394 --> 00:27:30,394
Are you okay?

430
00:27:38,904 --> 00:27:40,321
Vanessa.

431
00:27:41,539 --> 00:27:42,909
Vanessa, please.

432
00:27:45,276 --> 00:27:46,538
I can explain.

433
00:27:49,514 --> 00:27:51,857
JC. Hey, man.

434
00:27:52,609 --> 00:27:55,483
Look, Vanessa wanted me to tell you that

435
00:27:55,553 --> 00:27:57,361
it's not a good time.

436
00:27:58,389 --> 00:27:59,489
Sorry.

437
00:28:22,079 --> 00:28:24,579
- Jane.
- Before you say another word,

438
00:28:24,648 --> 00:28:28,348
yes, Brown is telling people
that he's coming for you,

439
00:28:28,418 --> 00:28:29,718
but I'm on it.

440
00:28:29,786 --> 00:28:31,646
Don't worry.

441
00:28:31,721 --> 00:28:33,321
I'm not calling about Brown.

442
00:28:33,390 --> 00:28:34,990
Oh, no.

443
00:28:35,059 --> 00:28:36,559
Honey, you don't sound good.

444
00:28:36,626 --> 00:28:39,558
Jane, the job is harder
than I remember it.

445
00:28:40,382 --> 00:28:41,990
The movie's shit.

446
00:28:43,266 --> 00:28:45,189
And the woman I love...

447
00:28:46,589 --> 00:28:49,112
is fucking "N" word Jim.

448
00:28:58,881 --> 00:29:00,979
The woman I love.

449
00:29:02,659 --> 00:29:04,443
I should never have come back.