1
00:00:28,187 --> 00:00:34,480
"I was like grass and I was not torn out."
Fernando Pessoa

2
00:03:09,581 --> 00:03:10,981
Mati...

3
00:03:31,182 --> 00:03:32,582
Mati.

4
00:04:00,083 --> 00:04:01,583
Mati.

5
00:04:04,184 --> 00:04:05,684
Don't.

6
00:04:22,327 --> 00:04:25,605
What are you...?
- Stop it!

7
00:04:34,427 --> 00:04:35,958
Stop it!

8
00:04:35,987 --> 00:04:37,464
You stop it!

9
00:04:37,547 --> 00:04:41,078
What are you doing?

10
00:04:41,247 --> 00:04:43,458
Stop it!

11
00:06:53,607 --> 00:06:56,618
See?

12
00:07:24,547 --> 00:07:30,881
AUTUMN BALL

13
00:12:48,327 --> 00:12:51,280
I forgive you everything.

14
00:13:13,667 --> 00:13:15,858
Do you still love Jaan?

15
00:14:02,027 --> 00:14:05,566
It really affects me whenever
I get pulled into an environment...

16
00:14:05,567 --> 00:14:09,566
that hasn't been created by a professional.
Then I realise it's truly great.

17
00:14:09,567 --> 00:14:15,717
How can that be? I feel that
I should be the one to...

18
00:14:16,627 --> 00:14:19,505
I should be...

19
00:14:23,006 --> 00:14:25,406
Well, I think that...

20
00:14:25,407 --> 00:14:29,249
For someone who has never studied it
to suddenly create something so...

21
00:14:29,250 --> 00:14:31,323
Well, true style is inherent...

22
00:14:38,007 --> 00:14:40,043
It's interesting how
they've done the facade.

23
00:14:40,044 --> 00:14:42,225
Is it intentional or a work in progress?

24
00:14:42,226 --> 00:14:45,403
Sure seems like something's in progress.

25
00:15:11,807 --> 00:15:13,322
Great party.

26
00:15:19,187 --> 00:15:21,730
Hey Maurer, do you still...

27
00:15:21,731 --> 00:15:26,431
live in that...
housing estate?

28
00:15:27,147 --> 00:15:29,403
- I do
- Why?

29
00:15:34,227 --> 00:15:37,125
Practicing humility.

30
00:15:45,187 --> 00:15:47,098
But seriously?

31
00:16:49,707 --> 00:16:51,763
You're back early.

32
00:17:03,487 --> 00:17:04,902
I'm so tired.

33
00:17:23,627 --> 00:17:25,176
Of what?

34
00:17:30,027 --> 00:17:33,838
They want everything to be...

35
00:17:33,847 --> 00:17:34,982
so cozy.

36
00:17:36,747 --> 00:17:42,084
Warm and comfy.

37
00:17:44,907 --> 00:17:47,263
Who's they?

38
00:17:52,847 --> 00:17:59,082
I cannot stand my friends and
colleagues anymore.

39
00:18:05,427 --> 00:18:08,585
That cynic cornered me again.

40
00:18:09,807 --> 00:18:14,386
Asked me why we still live in
a housing estate.

41
00:18:15,067 --> 00:18:18,880
Does he really believe...

42
00:18:18,881 --> 00:18:22,101
I'm insincere?

43
00:18:25,527 --> 00:18:28,276
Useless layabout.

44
00:18:36,527 --> 00:18:38,521
But really...

45
00:18:38,527 --> 00:18:41,497
Why do we live here
in Lasnam�e?

46
00:18:41,867 --> 00:18:43,382
What?

47
00:18:44,840 --> 00:18:47,844
I'm sure you could still be a
paragon of integrity...

48
00:18:47,845 --> 00:18:50,699
even if we didn't live in this
wretched neighbourhood.

49
00:18:50,700 --> 00:18:53,523
What are you talking about?

50
00:18:54,327 --> 00:18:58,143
Nobody lives here!

51
00:19:01,907 --> 00:19:04,877
Here, there, there!
Nobody lives here?

52
00:21:55,227 --> 00:21:57,938
I'm taking my lunch break.

53
00:22:00,167 --> 00:22:03,239
Baltic consciousness 2006
Literary conference

54
00:22:14,147 --> 00:22:16,980
Those Latvians and their tantrums.

55
00:22:21,527 --> 00:22:24,297
I don't think I have seen you before.

56
00:22:25,047 --> 00:22:27,903
I only came for the food.

57
00:22:28,127 --> 00:22:34,842
Of course, what else.
The presentations are as dull as ever.

58
00:22:38,667 --> 00:22:42,740
It feels so contrived, the way
Eastern Europeans are always...

59
00:22:42,741 --> 00:22:47,377
trying to construct their identity.
Don't you agree?

60
00:22:47,378 --> 00:22:49,378
I agree.

61
00:22:50,427 --> 00:22:53,640
The West finances any old nonsense...

62
00:22:53,647 --> 00:22:56,705
trying to deal with their
overfed guilty conscience.

63
00:22:56,707 --> 00:23:04,201
But all this financing is simply
a continuation of colonialism.

64
00:23:04,287 --> 00:23:06,765
There is no such thing as
the 'Baltic consciousness'.

65
00:23:06,766 --> 00:23:09,783
There is no Baltic anything!

66
00:23:10,327 --> 00:23:14,185
Giving things names is pure violence.

67
00:23:18,007 --> 00:23:21,277
You are looking at me
like I'm a moron.

68
00:23:23,087 --> 00:23:26,825
You know what I'm saying is true.

69
00:23:48,007 --> 00:23:51,599
- What's your sign?
- Aries.

70
00:23:59,867 --> 00:24:03,059
You don't live at all like a writer.

71
00:24:08,987 --> 00:24:11,323
Have you read any Pessoa?

72
00:24:13,907 --> 00:24:19,720
"I made of myself something
beyond my knowledge...

73
00:24:19,807 --> 00:24:22,785
And what I could make of myself
I failed to do."

74
00:24:22,867 --> 00:24:25,803
No, I haven't read it.
Who's Pessoa?

75
00:24:27,907 --> 00:24:30,218
Who's Pessoa!

76
00:24:36,867 --> 00:24:38,016
You're different.

77
00:24:38,567 --> 00:24:41,817
You are... real.

78
00:24:43,947 --> 00:24:46,403
You're a real person.

79
00:24:48,727 --> 00:24:52,624
I'd like to read something
you've written. Will you let me?

80
00:24:54,327 --> 00:24:56,618
I have nothing to show you.

81
00:24:58,827 --> 00:25:01,163
You are modest as well.

82
00:25:04,947 --> 00:25:07,838
Maybe even a genius?

83
00:25:07,867 --> 00:25:10,837
A quiet hermit noticed by no one...

84
00:25:14,127 --> 00:25:20,782
But after your death your
shabby bachelor flat will reveal...

85
00:25:20,787 --> 00:25:25,126
the best texts in the world...
- I'm a doorman.

86
00:25:28,947 --> 00:25:30,858
I don't understand.

87
00:25:30,907 --> 00:25:33,423
What is there to understand?

88
00:29:19,987 --> 00:29:22,198
Maurer?

89
00:29:29,527 --> 00:29:32,477
I saw a one-armed man today.

90
00:29:35,378 --> 00:29:37,078
Yeah.

91
00:29:41,227 --> 00:29:45,861
I'm saying that I saw a man
with a rotting arm near the store...

92
00:29:45,907 --> 00:29:50,266
drinking cologne
and smelling of piss.

93
00:29:50,867 --> 00:29:52,667
For god's sake.

94
00:29:54,727 --> 00:29:57,677
Do we have to discuss this now?

95
00:30:02,007 --> 00:30:04,598
Dear lord!

96
00:30:14,387 --> 00:30:17,378
Listen, I'm afraid of them.

97
00:30:17,427 --> 00:30:21,515
I'm afraid that I might catch
some horrible contagious disease...

98
00:30:21,520 --> 00:30:24,644
just by walking past them.

99
00:30:30,667 --> 00:30:34,303
Is it awful of me to speak this way?

100
00:30:39,227 --> 00:30:41,738
Is it?

101
00:30:47,047 --> 00:30:49,658
Can I speak that way?

102
00:30:54,007 --> 00:30:56,260
Maurer,
can I even think that way?

103
00:30:56,267 --> 00:30:58,937
Can't we discuss
this in the morning?

104
00:31:06,667 --> 00:31:10,237
Two rooms are just fine.

105
00:31:10,667 --> 00:31:13,658
Three would already be a luxury.

106
00:31:14,527 --> 00:31:18,619
Two rooms with a kitchen.
A bathroom.

107
00:31:20,287 --> 00:31:22,618
Not bad.

108
00:31:24,567 --> 00:31:27,178
Not bad at all.

109
00:31:29,127 --> 00:31:32,460
I'm a lucky guy.

110
00:32:21,287 --> 00:32:25,065
(in a Finnish accent)
I'm done for today.

111
00:32:25,867 --> 00:32:29,025
I'm not doing your clients.

112
00:32:29,087 --> 00:32:31,498
What clients?

113
00:32:31,867 --> 00:32:36,726
I've got no more
appointments for today.

114
00:32:38,087 --> 00:32:40,398
What about the queue?

115
00:32:43,627 --> 00:32:48,305
What queue?

116
00:32:51,007 --> 00:32:52,476
Kaski!

117
00:32:56,967 --> 00:33:02,380
The fact that you're Finnish
doesn't change a thing.

118
00:33:02,727 --> 00:33:05,343
This here is a business.

119
00:33:06,727 --> 00:33:10,219
Success does not come overnight.

120
00:33:11,187 --> 00:33:13,064
You need enthusiasm.

121
00:35:25,566 --> 00:35:27,166
(voice on the phone)
Slut, I want to see my kid...

122
00:35:27,167 --> 00:35:32,080
Don't call here any more!
You hear me? You're drunk!

123
00:36:13,527 --> 00:36:15,442
Well then...

124
00:37:07,427 --> 00:37:10,260
Yes, it seems to be working now.

125
00:37:10,261 --> 00:37:13,861
Yes.
Then how about...

126
00:37:13,947 --> 00:37:18,181
I mean, I wouldn't say no to a drink.

127
00:37:38,967 --> 00:37:45,058
Such a ladies' drink.
Sweet.

128
00:37:53,807 --> 00:37:58,720
Nice flat you've got here.
Lots of space. - Yes.

129
00:37:58,887 --> 00:38:01,598
You live here alone?

130
00:38:01,727 --> 00:38:05,844
I mean with your daughter?
- Yes.

131
00:38:07,967 --> 00:38:13,936
It's not easy, everything is
so expensive these days.

132
00:38:14,047 --> 00:38:16,738
And you, all alone...

133
00:38:17,727 --> 00:38:22,086
And adults have their
needs as well, don't they?

134
00:38:22,767 --> 00:38:24,382
What are you...?

135
00:38:24,567 --> 00:38:28,925
Oh no, no. I'm just saying...

136
00:38:28,926 --> 00:38:31,926
I'm saying I understand.

137
00:38:31,967 --> 00:38:36,805
Those needs can overwhelm you
so that...

138
00:38:36,807 --> 00:38:40,024
And it doesn't get any easier
with age.

139
00:38:40,027 --> 00:38:43,083
Please stop talking!

140
00:38:44,187 --> 00:38:50,422
Listen young lady,
don't act so...

141
00:38:50,807 --> 00:38:58,976
I could... I'd be able to...
with you...

142
00:39:03,767 --> 00:39:07,459
I'm a complete stranger to you.

143
00:39:07,807 --> 00:39:11,479
We can be honest with each other.

144
00:39:12,667 --> 00:39:14,636
There's no reason to...

145
00:39:19,147 --> 00:39:22,819
You have my number.
If there is anything...

146
00:40:42,227 --> 00:40:43,724
Laura!

147
00:40:43,725 --> 00:40:45,984
Wait!

148
00:40:50,747 --> 00:40:52,878
Hey!

149
00:40:56,847 --> 00:40:59,583
I want to see my daughter.

150
00:41:00,807 --> 00:41:02,422
You're drunk.

151
00:41:02,587 --> 00:41:05,198
This is unfair.

152
00:41:06,327 --> 00:41:09,819
I'll have her
taken away from you.

153
00:41:09,847 --> 00:41:13,639
I know about all those men
who come to your place.

154
00:41:13,727 --> 00:41:15,897
Don't think that I don't know.

155
00:41:15,927 --> 00:41:18,204
Slut!

156
00:41:21,087 --> 00:41:23,878
I'll kill you!

157
00:42:27,127 --> 00:42:30,685
Lotta, come inside!
You'll catch cold!

158
00:42:42,527 --> 00:42:45,383
What is this film about?

159
00:42:47,507 --> 00:42:50,877
That woman over there
is the main character.

160
00:42:50,927 --> 00:42:55,961
Her love is a forbidden love.
- What is forbidden love?

161
00:42:56,667 --> 00:43:00,283
It is love that is not permitted.

162
00:44:13,127 --> 00:44:15,783
Have you read any Pessoa?

163
00:44:18,527 --> 00:44:22,522
"I made of myself
something beyond my knowledge...

164
00:44:22,527 --> 00:44:26,264
And what I could make of myself
I failed to do."

165
00:44:29,487 --> 00:44:31,102
I know one too.

166
00:44:33,427 --> 00:44:38,421
A pig and a duck
had a good fuck...

167
00:44:38,467 --> 00:44:40,136
No, wait.

168
00:44:40,227 --> 00:44:49,378
A pig and two ducks
had a good fuck.

169
00:44:49,467 --> 00:44:52,485
It's an old rhyme from M�rjamaa.

170
00:44:52,527 --> 00:44:58,678
I'm sorry...
You're a really cool guy.

171
00:45:00,427 --> 00:45:03,383
No offence. Honestly.

172
00:45:03,867 --> 00:45:06,223
My naughty mouth...
Sorry.

173
00:45:13,547 --> 00:45:15,858
You're a cool guy.

174
00:45:16,987 --> 00:45:19,323
It's nothing personal.

175
00:45:19,367 --> 00:45:24,358
You're a great guy, really.

176
00:45:30,867 --> 00:45:33,603
What a naughty mouth.

177
00:45:36,227 --> 00:45:38,450
I'm really sorry.

178
00:45:38,467 --> 00:45:44,626
If we get a free moment,
we'll pity you.

179
00:45:46,387 --> 00:45:49,478
Sorry.

180
00:46:33,879 --> 00:46:43,379
(in the notebook)
197. Ruth - Libra
198. Kirsi - Gemini...

181
00:46:47,487 --> 00:46:51,382
(last line)
210. Merike - Aquarius

182
00:48:05,607 --> 00:48:10,546
Listen, I was thinking...

183
00:48:10,547 --> 00:48:14,379
If you've got spare time and
have nothing better to do...

184
00:48:14,380 --> 00:48:20,562
could you keep the front
of the restaurant clean?

185
00:48:21,607 --> 00:48:23,718
How do you mean?

186
00:48:23,727 --> 00:48:28,885
I don't know,
with a broom or something.

187
00:48:30,727 --> 00:48:33,218
But I'm a doorman.

188
00:48:34,567 --> 00:48:36,903
Well, we all have our responsibilities.

189
00:48:36,950 --> 00:48:39,685
I'm the manager and your job
is to keep things tidy.

190
00:48:39,687 --> 00:48:42,398
Division of labour.

191
00:48:45,127 --> 00:48:52,405
I was told that cleaning would not
be one of my responsibilities.

192
00:48:52,407 --> 00:49:00,103
So, we seem to have a...
difference of opinions.

193
00:49:00,187 --> 00:49:03,378
And I'm the manager here.

194
00:49:05,647 --> 00:49:08,183
Cleaning is not my job.

195
00:49:09,227 --> 00:49:12,318
Not one of your strong suits?

196
00:49:20,727 --> 00:49:26,486
And where's your jacket?
Where is it?

197
00:49:34,527 --> 00:49:38,799
Employees work.
Reading is for writers.

198
00:54:16,607 --> 00:54:20,679
Hello? - Congratulations,
you lucky bastard - What?

199
00:54:20,687 --> 00:54:23,920
You really don't know?
Everyone else does.

200
00:54:24,187 --> 00:54:26,502
- No, I don't. - You've been awarded
the Cross of Terra Mariana.

201
00:54:26,503 --> 00:54:28,303
And a hundred thousand kroons.

202
00:54:29,647 --> 00:54:31,558
- The Cross, to me?
- Yes, to you.

203
00:54:31,647 --> 00:54:34,438
It was in the paper.

204
00:54:40,467 --> 00:54:44,282
Hey, did you really believe that?

205
00:54:44,567 --> 00:54:46,016
No, I didn't.

206
00:54:46,227 --> 00:54:49,485
I've already called five people.
Everyone believed me!

207
00:54:49,567 --> 00:54:51,558
Fuck off with your stupid pranks!

208
00:54:51,647 --> 00:54:57,640
- Listen, have you heard people saying
that I've gone mad? - No, I haven't!

209
00:56:47,887 --> 00:56:53,125
It's not boredom that ends love.

210
00:56:53,200 --> 00:56:55,226
It's impatience.

211
00:57:00,047 --> 00:57:06,126
The impatience of bodies yearning
to live, yet dying each day.

212
00:57:07,647 --> 00:57:14,640
The impatience of bodies dying
day by day, but yearning to live.

213
00:57:16,127 --> 00:57:20,960
Who can't pass up a single opportunity.

214
00:57:24,987 --> 00:57:27,364
Who wish...

215
00:57:28,887 --> 00:57:33,575
The impatience of bodies yearning
to live but dying each day...

216
00:57:33,607 --> 00:57:36,119
Bodies that can't pass up
a single opportunity

217
00:57:36,167 --> 00:57:43,682
Who wish to make the most of their
limited, diminishing, mediocre lives.

218
00:57:45,527 --> 00:57:47,242
Who can't...

219
00:57:48,227 --> 00:57:54,161
Who wish to make the most of their
limited lives and cannot love anyone...

220
00:57:54,187 --> 00:58:03,979
because everyone else seems limited,
diminishing and mediocre.

221
00:58:04,087 --> 00:58:07,198
Who cannot love anyone...

222
00:58:10,127 --> 00:58:12,583
But it is not boredom...

223
00:59:33,127 --> 00:59:36,825
So poorly printed.

224
00:59:39,527 --> 00:59:44,418
Such a lousy print.
Is it really that much?

225
00:59:46,607 --> 00:59:48,563
Natasha!
- Yes?

226
00:59:48,647 --> 00:59:50,558
How much is "Maaja"?
- Forget it.

227
00:59:50,607 --> 00:59:54,398
- What's "Maaja"?
- The porn magazine!

228
00:59:54,687 --> 00:59:56,678
Doesn't it have a price on it?

229
00:59:56,727 --> 01:00:00,139
It does but the print is so bad,
I can't make it out.

230
01:00:00,327 --> 01:00:03,899
Hold on, I'll find out.
- Forget it.

231
01:00:05,947 --> 01:00:10,359
How much is "Maaja",
that porn mag?

232
01:00:10,747 --> 01:00:16,782
Yes, that porn. It does, but the print
is so bad, impossible to read.

233
01:00:17,127 --> 01:00:20,405
Well, there is,
but it's impossible to read...

234
01:00:20,407 --> 01:00:24,419
Thirty nine kroons.
- Thirty nine?

235
01:00:24,527 --> 01:00:26,518
Thirty nine kroons.

236
01:00:26,607 --> 01:00:30,279
Okay. I aslo read "thirty nine",
but that seemed too expensive.

237
01:00:30,327 --> 01:00:33,497
Thirty nine, it's quite legible.

238
01:03:35,367 --> 01:03:38,016
What's going on here?

239
01:03:38,087 --> 01:03:41,575
What are you doing? Oh my god!

240
01:03:41,647 --> 01:03:45,225
- I was just...
- I know your sort!

241
01:03:45,226 --> 01:03:50,926
You're not going anywhere!
Pervert!

242
01:03:51,467 --> 01:03:55,516
Look everyone!
There goes a pervert!

243
01:03:55,567 --> 01:03:59,416
Where are you going?
Pervert!

244
01:05:01,507 --> 01:05:04,043
Jump in, I'll give you a lift.

245
01:05:37,427 --> 01:05:40,918
It's always red.

246
01:06:04,387 --> 01:06:06,456
Bumpy road.

247
01:06:34,367 --> 01:06:40,983
You must be wondering
what I'm doing.

248
01:06:44,327 --> 01:06:49,215
I've been watching you from afar
for quite a while now.

249
01:06:49,227 --> 01:06:52,183
And I like you.

250
01:06:57,727 --> 01:07:04,615
You seem sad and...

251
01:07:04,687 --> 01:07:07,018
lonely.

252
01:07:10,467 --> 01:07:15,418
You have... class.

253
01:07:28,847 --> 01:07:33,539
Be my wife.
- Why?

254
01:07:40,087 --> 01:07:45,675
My home needs a fairy.

255
01:07:50,567 --> 01:07:56,184
Or do you despise people like me?
- Like what?

256
01:08:15,547 --> 01:08:18,638
Do you at least want to kiss me?

257
01:11:29,327 --> 01:11:30,896
Lotta.

258
01:11:32,647 --> 01:11:37,080
Do you remember
what I told you about strangers?

259
01:11:39,227 --> 01:11:42,485
You should never accept anything
from strange men.

260
01:11:43,647 --> 01:11:46,738
What does 'pervert' mean?

261
01:11:58,066 --> 01:11:59,566
Try to understand...

262
01:11:59,567 --> 01:12:04,040
It is very important
where and how I live!

263
01:12:04,127 --> 01:12:10,917
I'm an architect, not an estate agent.
- I see. You're a maniac.

264
01:13:25,907 --> 01:13:27,122
Don't look.

265
01:13:27,127 --> 01:13:32,419
Some psychopaths don't like it
when you look them in the eye.

266
01:13:36,467 --> 01:13:39,717
Why did that doorman
look at you like that?

267
01:13:40,567 --> 01:13:43,437
I'm afraid the kitchen is closed.

268
01:13:43,647 --> 01:13:46,738
Give me a bottle of brandy.

269
01:14:51,687 --> 01:14:56,899
Waiter! Bring me
a bottle of champagne!

270
01:14:58,727 --> 01:15:03,304
You're a beautiful goddess.

271
01:15:04,507 --> 01:15:07,519
Like Aphrodite rising from the sea.

272
01:15:07,567 --> 01:15:10,278
You know?

273
01:15:10,527 --> 01:15:14,222
No, I don't. Behave!

274
01:15:14,227 --> 01:15:17,380
People are looking.
- What people?

275
01:15:17,427 --> 01:15:19,242
They're robots.

276
01:15:20,647 --> 01:15:25,526
You know, my wife left me...
for my friend.

277
01:16:12,507 --> 01:16:14,376
Let's drink.

278
01:16:17,377 --> 01:16:18,977
Come on, let's drink.

279
01:16:19,047 --> 01:16:20,878
Hello?

280
01:16:21,427 --> 01:16:23,838
Bottoms up.

281
01:16:41,687 --> 01:16:45,975
Don't be so fucking...
conservative.

282
01:16:46,027 --> 01:16:48,538
So reserved.

283
01:16:48,607 --> 01:16:52,143
It's not a beauty pageant.

284
01:16:56,427 --> 01:17:00,976
Come on, let's drink.
Hello?

285
01:17:25,987 --> 01:17:29,720
Why do you always
wear that same dress?

286
01:17:29,747 --> 01:17:33,717
- What?
- Shut up!

287
01:17:37,647 --> 01:17:40,803
I'm bored with that dress.

288
01:17:47,487 --> 01:17:52,279
Your breasts are
too small for that dress.

289
01:18:05,987 --> 01:18:08,723
Hey, right on!

290
01:18:19,607 --> 01:18:22,077
Thank you for the lovely evening.

291
01:19:18,227 --> 01:19:22,639
What's wrong?

292
01:19:44,426 --> 01:19:47,326
I'm sorry.

293
01:19:47,327 --> 01:19:49,927
I soaked your shirt.

294
01:19:54,527 --> 01:19:56,276
I'm sorry.

295
01:20:07,727 --> 01:20:11,360
- Hey, where did that woman go!
- What woman?

296
01:20:11,647 --> 01:20:15,124
Get my coat.

297
01:20:33,947 --> 01:20:36,978
Forgive me.

298
01:20:38,327 --> 01:20:40,818
I don't know what came over me.

299
01:21:07,647 --> 01:21:10,058
Give me the keys.

300
01:21:23,647 --> 01:21:26,525
Let's go for a ride.

301
01:21:26,687 --> 01:21:29,164
I'll drive.

302
01:21:31,465 --> 01:21:33,465
Come on, let's go.

303
01:21:35,647 --> 01:21:40,060
Let's drive, we'll go to my place.
- Please stop it.

304
01:21:40,147 --> 01:21:42,438
Where are you going?

305
01:21:43,667 --> 01:21:46,103
Please don't!

306
01:21:52,887 --> 01:21:55,118
Don't go.

307
01:21:58,567 --> 01:22:03,783
- Bastard!
- I'll total this car! Hear me?

308
01:27:21,527 --> 01:27:23,776
Did you sleep well?

309
01:28:21,847 --> 01:28:23,958
Do you want this?

310
01:28:41,567 --> 01:28:43,623
Very good.

311
01:28:51,227 --> 01:28:53,542
Shit.

312
01:28:53,567 --> 01:28:58,639
My shirt is so ugly.

313
01:29:17,987 --> 01:29:20,784
These are...

314
01:29:20,787 --> 01:29:25,779
These are big ones.

315
01:29:36,607 --> 01:29:38,676
Big ones.

316
01:31:38,227 --> 01:31:39,840
Anton.

317
01:31:39,847 --> 01:31:42,703
Why are you dressed like that?

318
01:31:42,927 --> 01:31:47,986
I got a job. It's impossible
to survive as a sculptor.

319
01:31:56,847 --> 01:32:00,138
Your car is parked wrong.

320
01:32:10,587 --> 01:32:12,843
Three rooms is something else.

321
01:32:12,845 --> 01:32:16,325
23 thousand kroons per square metre.
Pretty sweet, isn't it?

322
01:32:16,327 --> 01:32:18,377
It's safe to say I'm a lucky man.

323
01:32:18,387 --> 01:32:22,325
But you... now that you're alone,
you should sell your apartment.

324
01:32:22,327 --> 01:32:25,925
- I don't know...
- Or find a new woman.

325
01:32:25,927 --> 01:32:28,905
Where? And how?
I just can't do it.

326
01:32:28,927 --> 01:32:32,218
Then you should sell the apartment.

327
01:32:32,607 --> 01:32:35,423
I just don't know
how to approach women.

328
01:32:35,607 --> 01:32:37,885
It's very easy to approach women.

329
01:32:37,887 --> 01:32:40,620
You should find out
when she's ovulating.

330
01:32:40,621 --> 01:32:44,886
Women are extremely
receptive during ovulation.

331
01:32:44,887 --> 01:32:48,880
But of course then they're drawn to
men with angular jaws...

332
01:32:48,885 --> 01:32:52,080
and lots of body hair.

333
01:32:52,927 --> 01:32:55,805
Do you have to stick
these things everywhere?

334
01:32:55,887 --> 01:32:58,118
Selfish bastard!

335
01:33:27,187 --> 01:33:29,981
Asshole, look what you've done!

336
01:33:39,327 --> 01:33:42,843
Ovulation? What ovulation?

337
01:33:42,847 --> 01:33:47,284
- I saw it on the Discovery Channel.
- Discovery?

338
01:33:47,327 --> 01:33:49,538
Fuck off!

339
01:34:09,847 --> 01:34:13,880
I don't think I've ever eaten here.

340
01:34:21,327 --> 01:34:23,783
Exquisite cuisine.

341
01:34:40,887 --> 01:34:44,399
Soon I'll have enough money
to buy two skip containers.

342
01:34:44,427 --> 01:34:45,645
What?

343
01:34:45,647 --> 01:34:48,358
Skip containers.

344
01:34:56,707 --> 01:35:01,825
Skip container rental.
For construction waste and garbage.

345
01:35:01,827 --> 01:35:04,100
First I'll take two
4.5 cubic meter ones.

346
01:35:04,107 --> 01:35:05,876
Later when the money
starts coming in...

347
01:35:05,877 --> 01:35:11,577
I'll take 10, 15, hell, 30 cubics!

348
01:35:11,707 --> 01:35:18,246
For tractors, dump cars,
the whole deal.

349
01:35:19,567 --> 01:35:22,600
Think how much construction
takes place in this town.

350
01:35:22,607 --> 01:35:26,619
Think how much construction waste
that creates.

351
01:35:26,647 --> 01:35:31,125
I'm not going to be
a doorman my whole life.

352
01:35:41,127 --> 01:35:46,318
That's all just great...

353
01:35:48,747 --> 01:35:52,480
But I have to get going now.

354
01:36:25,887 --> 01:36:28,423
Please wait!
Hold on!

355
01:36:31,647 --> 01:36:35,338
What's with you?
What just happened?

356
01:36:35,350 --> 01:36:38,217
Nothing happened.

357
01:36:38,227 --> 01:36:40,863
I just have to go.

358
01:36:40,927 --> 01:36:44,804
But I thought...

359
01:36:44,887 --> 01:36:47,198
We had something.

360
01:36:47,237 --> 01:36:51,020
Don't say you didn't feel it.

361
01:36:51,227 --> 01:36:54,083
Yes, I felt it.

362
01:36:56,367 --> 01:36:58,778
So what's wrong?

363
01:37:03,747 --> 01:37:08,739
Theo, I just really have to go now.

364
01:37:12,987 --> 01:37:15,785
Give me your phone number.

365
01:37:16,467 --> 01:37:19,278
No, I'll call you.

366
01:38:46,927 --> 01:38:49,104
Where have you been?

367
01:38:53,927 --> 01:38:56,785
You know how much construction
waste this city generates?

368
01:38:56,790 --> 01:39:00,319
- I asked you a question.
- I'll ask you one as well.

369
01:39:00,327 --> 01:39:03,423
- You will?
- Yes, I will.

370
01:39:03,427 --> 01:39:05,776
Ask away then.

371
01:39:13,927 --> 01:39:16,905
Let's try to be honest,
just this once

372
01:39:16,927 --> 01:39:19,624
- Honest?
- Yes, truly honest.

373
01:39:19,625 --> 01:39:23,825
Without worrying about appearances.

374
01:39:24,847 --> 01:39:29,617
- Honestly?
- Yes, honestly.

375
01:39:34,027 --> 01:39:35,842
Honestly.

376
01:39:50,467 --> 01:39:53,023
I'm bored with you.

377
01:39:57,027 --> 01:40:02,886
Your dry skin. Your cold eyes.

378
01:40:04,707 --> 01:40:16,225
Your petite bourgeoisie egotism,
which to you seems vibrant and natural...

379
01:40:16,227 --> 01:40:20,799
but to me it looks plain stupid.

380
01:40:23,267 --> 01:40:28,404
I'm bored with
your childish craving for attention.

381
01:40:28,427 --> 01:40:33,425
I'm bored with your...

382
01:40:33,427 --> 01:40:35,783
femininity.

383
01:40:40,927 --> 01:40:44,583
And you think I'm the problem?

384
01:40:44,647 --> 01:40:47,283
I couldn't care less about that.

385
01:40:47,327 --> 01:40:50,821
- Do you know what you are?
- Well?

386
01:40:50,827 --> 01:40:58,361
You're like some Bergman character,
not even human anymore. - Wow.

387
01:40:58,370 --> 01:41:01,265
A tiny man bemoaning his god's silence...

388
01:41:01,267 --> 01:41:06,759
squirming in his own little
black and white world.

389
01:41:08,327 --> 01:41:15,020
Maybe that's...
Yeah.

390
01:41:21,927 --> 01:41:26,860
What are you?
What kind of a man are you?

391
01:41:27,787 --> 01:41:31,945
All shiny surfaces and self-admiration.

392
01:41:57,847 --> 01:42:01,405
That's what I think about
your shiny surfaces.

393
01:45:50,606 --> 01:45:51,506
This is...

394
01:45:51,507 --> 01:45:54,465
This shows a complete
lack of responsibility.

395
01:45:54,467 --> 01:45:58,125
You come to work
whenever you please!

396
01:45:58,127 --> 01:46:02,740
If you disappear one
more time, you're gone!

397
01:46:02,747 --> 01:46:05,458
Is that clear?

398
01:46:05,659 --> 01:46:07,059
Is that clear?

399
01:46:08,427 --> 01:46:11,618
Where do you think
you're going?

400
01:46:43,026 --> 01:46:44,726
Hello.

401
01:46:53,927 --> 01:46:56,860
- Get out!
- Shit!

402
01:46:56,887 --> 01:47:01,975
Come on,
don't treat him like that.

403
01:47:05,447 --> 01:47:09,859
He's a great actor and director.
- How great?

404
01:47:09,927 --> 01:47:13,118
Rather well-known.

405
01:47:31,847 --> 01:47:34,158
What do you direct?

406
01:47:35,127 --> 01:47:40,718
- Romantic comedies.
- Romantic comedies?

407
01:47:58,367 --> 01:48:00,382
Romantic comedies.

408
01:52:02,827 --> 01:52:05,658
I decided to come after all.

409
01:52:07,927 --> 01:52:10,158
Yes.

410
01:52:15,227 --> 01:52:19,218
Do you despise me now?

411
01:52:28,687 --> 01:52:32,599
These buildings cannot
contain the past very long.

412
01:52:32,607 --> 01:52:40,165
Maybe it's because they were
built thinking of the future.

413
01:52:51,887 --> 01:52:54,843
I just wanted to be happy.

414
01:52:54,887 --> 01:52:57,985
I still want to (be happy).

415
01:53:12,647 --> 01:53:15,317
In every single one
of these fucking boxes...

416
01:53:15,318 --> 01:53:19,318
there is a human being
who just wants be happy.

417
01:53:19,367 --> 01:53:29,900
Whole generations of mothers
and sons, fathers and daughters...

418
01:53:29,927 --> 01:53:34,286
spouses, their lovers,
car owners...

419
01:53:35,127 --> 01:53:39,325
All of them want to be happy.

420
01:53:40,847 --> 01:53:51,883
And their lives drag on like
the wind grinding limestone walls.

421
01:53:55,927 --> 01:53:59,405
And what will be left behind?

422
01:54:23,607 --> 01:54:28,086
Maybe you don't believe me,
but I still love you.

423
01:54:40,787 --> 01:54:43,623
I don't believe.

424
01:56:19,324 --> 01:56:26,000
AUTUMN BALL

425
01:56:33,401 --> 01:56:54,401
Subtitles edited and resynced by sigane
subscene.com/members/sigane.aspx

426
01:58:22,607 --> 01:58:25,719
This movie is inspired by the book
"Autumn Ball" by Mati Unt...

427
01:58:25,720 --> 01:58:28,945
and dedicated to all the men
with a gentle heart and a weak liver...

428
01:58:28,947 --> 01:58:32,038
who stand alone
in the night in their underwear.

