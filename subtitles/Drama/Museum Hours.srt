﻿1
00:03:46,311 --> 00:03:49,856
Somehow, it's already my sixth year
working in the museum.

2
00:03:49,939 --> 00:03:53,151
I'm thinking about that as I take my seat,

3
00:03:53,234 --> 00:03:57,071
with the big wooden door at my back
and the little rope in front,

4
00:03:57,155 --> 00:03:59,407
like a fence or gate.

5
00:04:01,993 --> 00:04:05,079
Guarding has its tedium,

6
00:04:05,163 --> 00:04:08,499
but it's not a bad job, not at all.

7
00:04:11,336 --> 00:04:14,881
Before this, I taught woodworking in a vocational school

8
00:04:14,964 --> 00:04:18,092
and listened to the saws buzz all day.

9
00:04:18,801 --> 00:04:22,722
And before that, in my younger days,
I worked in the music business,

10
00:04:22,805 --> 00:04:25,892
though it didn't seem so much
of a business to us at the time.

11
00:04:26,851 --> 00:04:28,853
I managed small bands

12
00:04:28,937 --> 00:04:32,732
and drove the van on many tours, both long and short,

13
00:04:32,815 --> 00:04:37,779
and knew the inside of every smoke-filled
hole from Sheffield to Hamburg to my Vienna,

14
00:04:37,862 --> 00:04:42,325
though there weren't quite as many
holes here as we would have liked.

15
00:04:43,660 --> 00:04:46,371
So off we'd go in the van.

16
00:04:47,205 --> 00:04:49,207
It was good.

17
00:04:49,290 --> 00:04:51,751
Well, mostly it was good.

18
00:04:51,834 --> 00:04:53,670
I like people, you see,

19
00:04:53,753 --> 00:04:56,297
and to be of use...

20
00:04:57,590 --> 00:05:00,510
But I had my share of loud.

21
00:05:00,593 --> 00:05:03,137
So now, I have my share of quiet.

22
00:06:44,655 --> 00:06:47,867
My favorite room is the Bruegel room.

23
00:06:47,950 --> 00:06:51,829
It is, one can say without boasting,
the finest room of Bruegels in the world,

24
00:06:51,913 --> 00:06:55,416
and perhaps the most popular room in the museum,

25
00:06:55,500 --> 00:06:58,044
so there's always much watching out to do.

26
00:06:58,961 --> 00:07:02,924
And when it's quiet
or one tires of looking only at visitors,

27
00:07:03,007 --> 00:07:08,304
one can turn an eye to the paintings and
you will always see something new, I think.

28
00:07:09,847 --> 00:07:13,893
You've heard that before, I'm sure, but it's quite true,

29
00:07:13,976 --> 00:07:15,645
especially when it comes to Bruegel.

30
00:07:19,148 --> 00:07:24,195
Now, some of my co-workers, I'll admit,
might not share my enthusiasm.

31
00:07:24,278 --> 00:07:28,116
Some are students,
with their minds on the night's parties

32
00:07:28,199 --> 00:07:30,368
or the day's exams.

33
00:07:30,451 --> 00:07:34,038
And some, well some have worked here so long

34
00:07:34,122 --> 00:07:38,876
it's hard to say just what they've done with their minds.

35
00:07:38,960 --> 00:07:40,837
But that's their own business,

36
00:07:40,920 --> 00:07:43,381
and we all seem to get along.

37
00:07:46,008 --> 00:07:48,636
For me, the paintings are almost always worth a look

38
00:07:48,719 --> 00:07:52,265
and the Bruegels are worth that and more.

39
00:07:52,390 --> 00:07:56,435
Just the other day I noticed a frying pan

40
00:07:56,519 --> 00:07:59,147
sticking up from a figure's hat

41
00:07:59,230 --> 00:08:01,649
that I'd somehow missed before.
And that led me

42
00:08:01,732 --> 00:08:08,573
to thinking about eggs, and to start a
search for them in different paintings.

43
00:08:09,490 --> 00:08:15,329
And each time I counted one I would go
make my rounds and return to search again,

44
00:08:15,413 --> 00:08:21,085
and before I knew it an hour and a
half had slipped pleasantly by.

45
00:08:27,425 --> 00:08:30,178
Discarded playing cards,

46
00:08:30,261 --> 00:08:33,222
a bone, a broken egg,

47
00:08:34,765 --> 00:08:36,517
a cigarette butt,

48
00:08:36,601 --> 00:08:38,352
a folded note,

49
00:08:38,477 --> 00:08:40,688
a lost glove,

50
00:08:40,813 --> 00:08:42,607
a beer can...

51
00:09:16,307 --> 00:09:20,353
A woman came into the museum
and spent quite a bit of time,

52
00:09:20,478 --> 00:09:25,024
first just sitting, and then looking in the galleries,

53
00:09:25,107 --> 00:09:28,069
none of which is unusual in itself...

54
00:09:28,569 --> 00:09:32,490
But what is it about some people that makes one curious,

55
00:09:32,573 --> 00:09:38,246
while with others one would be just as
happy to never know a thing about them?

56
00:09:45,878 --> 00:09:49,215
Three or four days later, she was back again,

57
00:09:49,298 --> 00:09:54,011
struggling with a map
and looking more than a bit confused.

58
00:14:03,302 --> 00:14:09,725
I did want to help her but I must admit,
I brought up the phone call in part because

59
00:14:09,850 --> 00:14:13,938
it would allow me to confirm if her story was true.

60
00:14:14,313 --> 00:14:18,108
It's sad, but one has to be careful these days.

61
00:14:21,362 --> 00:14:25,324
But she did come back, and she did ask me to call.

62
00:14:25,407 --> 00:14:28,369
And it was true.

63
00:15:04,822 --> 00:15:06,740
She was stranded all right.

64
00:15:06,824 --> 00:15:10,869
But not as stranded as her cousin, Janet,
in a coma of all things.

65
00:15:11,787 --> 00:15:16,417
Anne knew nothing of the city
or how long she should or could stay,

66
00:15:16,500 --> 00:15:20,170
or what to do with herself.

67
00:15:20,546 --> 00:15:22,756
I guess she spent a lot of time wandering about,

68
00:15:22,840 --> 00:15:24,675
but she didn't seem to have much money,

69
00:15:24,842 --> 00:15:26,969
and of course, it was cold.

70
00:15:30,681 --> 00:15:36,312
I got her a pass to the museum.
It was no big deal.

71
00:15:36,395 --> 00:15:40,357
Nor was it any trouble to help out a bit myself.

72
00:15:40,524 --> 00:15:45,279
If I were to wake up lost in Montreal,
where she came from,

73
00:15:45,362 --> 00:15:47,406
it's what I'd hope someone would do for me.

74
00:16:23,859 --> 00:16:26,570
It's the hands.
It's always the hands.

75
00:17:35,889 --> 00:17:39,560
She asked if I'd go to the hospital with her.

76
00:17:39,643 --> 00:17:41,812
"Why not?" I thought.

77
00:28:18,823 --> 00:28:22,702
It felt good to see my city anew,

78
00:28:22,786 --> 00:28:28,041
to go somewhere for no reason other
than to show it to someone else,

79
00:28:28,124 --> 00:28:32,545
and then realize I hadn't been there in years

80
00:28:32,629 --> 00:28:35,507
and actually liked these places.

81
00:28:35,590 --> 00:28:40,011
They usually weren't places I'd show a regular tourist.

82
00:28:40,095 --> 00:28:43,223
They had to be inexpensive, for one thing.

83
00:28:43,765 --> 00:28:47,519
It made me realize how much time
I normally spent sitting at home,

84
00:28:47,644 --> 00:28:50,647
and online, of course.

85
00:40:53,911 --> 00:40:58,040
An art student worked here for a while.

86
00:40:58,207 --> 00:41:01,794
I liked him, he was a punk kid, just as I'd been once.

87
00:41:03,712 --> 00:41:06,715
He thought the museum was a bit ridiculous.

88
00:41:06,882 --> 00:41:11,512
He said when he looked at the paintings,
he mostly just saw money,

89
00:41:11,595 --> 00:41:15,391
or more accurately, things standing in for money.

90
00:41:15,850 --> 00:41:19,311
I guess this was what he'd learned at university.

91
00:41:19,436 --> 00:41:23,607
He said this was clearest in Dutch still lifes

92
00:41:23,691 --> 00:41:28,654
which were essentially just piled-up
possessions of the newly rich of that time.

93
00:41:29,071 --> 00:41:30,311
He said these were no different

94
00:41:30,364 --> 00:41:32,908
than if someone today
were to paint a pile of Rolex watches,

95
00:41:32,992 --> 00:41:35,744
champagne bottles, and flat-screen TVs,

96
00:41:36,912 --> 00:41:41,208
that they were the rap-star
videos of their day.

97
00:41:41,792 --> 00:41:45,754
And he said they were only less subtle versions

98
00:41:45,880 --> 00:41:50,968
of all the other commodities the museum was hoarding,

99
00:41:51,427 --> 00:41:56,682
and this was now just part of the way things
were disguised in the time of Late Capitalism.

100
00:41:56,765 --> 00:42:01,770
He didn't hold it against the museum
personally, but he went on like that.

101
00:42:03,022 --> 00:42:07,818
I asked why he always used the term "Late Capitalism,"

102
00:42:07,943 --> 00:42:11,864
and how people knew it was so late,

103
00:42:11,947 --> 00:42:17,536
and if it wasn't perhaps more troublesome
if what existed now was early.

104
00:42:17,786 --> 00:42:19,872
He knew a lot more than me

105
00:42:19,955 --> 00:42:23,500
but he didn't seem to have an answer for that.

106
00:42:24,627 --> 00:42:27,630
He was also unhappy about the cost of museum admission.

107
00:42:27,713 --> 00:42:32,468
I agreed it would be nicer if it was free,

108
00:42:32,551 --> 00:42:38,390
but he was a big fan of the movies and
I had to remind him they cost as much

109
00:42:38,474 --> 00:42:41,352
and he never complained about that.

110
00:42:42,019 --> 00:42:45,773
"Yeah, you can't win," he said,

111
00:42:45,856 --> 00:42:48,776
"but maybe someday everyone will lose less

112
00:42:48,859 --> 00:42:53,530
"and museums and movies
could both be free."

113
00:42:54,406 --> 00:43:00,496
We got to wondering how museums began.

114
00:43:00,621 --> 00:43:04,416
He looked it up and was pleasantly surprised to report

115
00:43:04,625 --> 00:43:07,920
that because of the French Revolution the Louvre opened

116
00:43:08,003 --> 00:43:12,925
as what is considered to be the one of
the first truly public art museums

117
00:43:13,008 --> 00:43:16,178
with the idea that art should be
accessible to the people,

118
00:43:16,262 --> 00:43:20,849
not just tied up in the private rooms of the rich.

119
00:43:21,767 --> 00:43:25,854
He was a good kid, and I'm sorry he moved on.

120
01:07:30,256 --> 01:07:35,010
I watch the adolescents come through
with the big school groups.

121
01:07:35,094 --> 01:07:39,807
They fool around, ignore their teachers,
send text messages.

122
01:07:39,890 --> 01:07:46,647
Sometimes they compete to be the most bored
and to make fun of the art.

123
01:07:47,189 --> 01:07:52,570
It gets a bit tedious but I know I'd have done the same.

124
01:07:52,653 --> 01:07:56,615
Some of the paintings do get their attention,

125
01:07:56,699 --> 01:08:01,954
Medusa's head of snakes works like a charm,
or rather like some horror film.

126
01:08:02,037 --> 01:08:06,333
In fact we have a strong showing of severed heads here,

127
01:08:06,417 --> 01:08:10,755
the Cranach Judith and Holofernes is especially graphic,

128
01:08:10,838 --> 01:08:13,966
but there are at least five more.

129
01:08:19,680 --> 01:08:24,185
And then of course, once they
actually start looking, there's sex

130
01:08:24,268 --> 01:08:26,771
which is common in the museum, too.

131
01:08:30,983 --> 01:08:34,612
It makes the kids laugh, nervously I guess.

132
01:08:34,695 --> 01:08:37,072
And to be completely honest,

133
01:08:37,156 --> 01:08:41,160
there are a few paintings
that look like cheap porn even to me,

134
01:08:41,243 --> 01:08:44,371
soft-core of course,
but somehow just a bit sleazy.

135
01:08:46,540 --> 01:08:49,627
There's one with a dog at the bottom of the picture

136
01:08:49,710 --> 01:08:52,546
and even the dog looks a bit embarrassed.

137
01:09:01,889 --> 01:09:06,602
But then there's the sculpture with a veiled body

138
01:09:06,685 --> 01:09:08,896
that gets to almost everyone, young or old.

139
01:09:08,979 --> 01:09:14,485
Or that magnificent body,
of a god I think, his penis is missing

140
01:09:14,568 --> 01:09:17,988
as they often are,

141
01:09:18,072 --> 01:09:24,119
but I think that just makes everyone
think about it and miss it.

142
01:09:24,203 --> 01:09:28,666
It's quite a thing,
to be able to watch people's impressions.

143
01:09:28,749 --> 01:09:32,586
And it's as if we, the guards, can be invisible.

144
01:09:32,670 --> 01:09:39,468
I see the one kid on their own, maybe
holding back at the tail of the class.

145
01:09:39,552 --> 01:09:42,012
Perhaps it's a girl, or a boy,

146
01:09:42,096 --> 01:09:45,391
looking furtively at the discus-thrower,

147
01:09:45,474 --> 01:09:47,810
whose ass so tenderly rests on a tree,

148
01:09:47,893 --> 01:09:51,856
and the tree seeming very dead in comparison.

149
01:09:55,734 --> 01:09:58,362
I've seen this happen again and again.

150
01:09:58,445 --> 01:10:03,784
Where else can one look at such a thing,
and without shame,

151
01:10:03,868 --> 01:10:08,706
because after all, one is in a very fine art museum?

152
01:10:08,789 --> 01:10:15,254
Of course, these days they could just go online
and see all the Internet porn they want,

153
01:10:15,337 --> 01:10:20,092
but it's different here, the way it feels for them.

154
01:10:20,175 --> 01:10:23,220
I can't quite put my finger on it, but I know it is.

155
01:11:33,207 --> 01:11:39,213
And on Sundays, for example, there's an
immigrant's party here that we love...

156
01:16:43,433 --> 01:16:49,439
It's a big spectacle to admire
on the huge church grounds.

157
01:16:52,567 --> 01:16:59,324
This big gathering of black birds.

158
01:17:01,201 --> 01:17:03,453
And I like to walk up to the church,

159
01:17:03,537 --> 01:17:10,460
because the church is really unbelievable,

160
01:17:10,544 --> 01:17:14,548
and you go to the back, and walk over the hill

161
01:17:14,631 --> 01:17:17,926
to the Johann-Staud-Gasse,

162
01:17:20,220 --> 01:17:23,974
and it's always a unique view.

163
01:20:18,356 --> 01:20:23,653
The most-asked question is probably
of course, "Where is the bathroom?"

164
01:20:24,571 --> 01:20:29,117
Naturally, we tire of it, though in the
case of the most obnoxious visitors

165
01:20:29,201 --> 01:20:33,455
who ask in the rudest ways, there is the option

166
01:20:33,538 --> 01:20:38,251
of sending them on what we guards call
"the scenic route."

167
01:20:38,919 --> 01:20:43,089
But we all in life have to use the place,
so if they ask politely,

168
01:20:43,173 --> 01:20:46,092
there's no point being bothered.

169
01:20:48,094 --> 01:20:51,348
Price or value is a common question, too,

170
01:20:51,431 --> 01:20:53,558
and not just amongst schoolchildren.

171
01:20:53,642 --> 01:20:56,770
It makes me think about the English word "priceless,"

172
01:20:56,853 --> 01:21:01,066
and wonder if a painting could actually be priceless

173
01:21:01,149 --> 01:21:02,692
in the sense

174
01:21:02,776 --> 01:21:05,612
that no price could be put on it.

175
01:21:05,695 --> 01:21:10,825
And if so, and it was in a museum and not for sale,

176
01:21:10,909 --> 01:21:15,664
if that could mean it was somehow set free
from all such accounting?

177
01:21:16,039 --> 01:21:19,376
Many of the works considered greatest in the museums

178
01:21:19,459 --> 01:21:22,754
were worth little or nothing in their day,

179
01:21:22,837 --> 01:21:27,092
and many of the artists who made them died poor,

180
01:21:27,342 --> 01:21:30,845
and yet they sit side by side with paintings

181
01:21:30,929 --> 01:21:34,808
that were of great renown and sold for fortunes.

182
01:21:35,392 --> 01:21:37,852
Side by side they hang,

183
01:21:37,978 --> 01:21:40,230
and if you weren't told,

184
01:21:40,313 --> 01:21:42,816
would you know which was which?

185
01:27:26,075 --> 01:27:28,036
Johann the Elder.

186
01:28:43,695 --> 01:28:47,115
Only 1.2 meters deep, it's not very deep.

187
01:28:47,198 --> 01:28:51,619
Every day we have to pump out 60,000 liters
so that the lake stays like this,

188
01:28:51,703 --> 01:28:55,081
otherwise the water will rise up to the white line

189
01:28:55,164 --> 01:28:58,710
and then we can't drive the boats here any longer.

190
01:28:59,836 --> 01:29:03,840
The water can go that high, but it is very clean water

191
01:29:03,923 --> 01:29:06,467
and we always keep it at 1.2 meters deep.

192
01:29:41,085 --> 01:29:46,549
And now we're driving 14 meters
underneath the little blue lake.

193
01:32:15,489 --> 01:32:20,369
Hello, my name is Leitner.
I've a missed call from Dr. Winterstein.

194
01:32:21,329 --> 01:32:23,623
Yes, thank you, I'll stay on the line.

195
01:32:36,928 --> 01:32:40,681
Yes, Dr. Winterstein, this is Leitner.
You've called me?

196
01:32:46,646 --> 01:32:48,022
Oh, yes.

197
01:32:52,485 --> 01:32:54,362
Well, should we come?

198
01:33:00,952 --> 01:33:04,330
Thank you. Yes. I'll tell her.

