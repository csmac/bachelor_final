1
00:00:02,270 --> 00:00:05,400
We are celebrating the union
of Zorn, defender of Zephyria,

2
00:00:05,440 --> 00:00:07,770
conqueror of the tribes of Agon,

3
00:00:07,810 --> 00:00:09,940
decapitator of the
dark herdsmen of Grith,

4
00:00:09,980 --> 00:00:11,340
to Edie...

5
00:00:11,380 --> 00:00:12,810
Bennett.

6
00:00:12,850 --> 00:00:16,510
So, by the power bestowed
by the god of war,

7
00:00:16,550 --> 00:00:18,820
I pronounce...
I oppose this union.

8
00:00:23,260 --> 00:00:24,360
Sir pent.

9
00:00:24,390 --> 00:00:28,060
You can't marry... a dead woman.

10
00:00:35,340 --> 00:00:36,646
I'm sorry, honey.
I know I promised not

11
00:00:36,670 --> 00:00:38,070
to disembowel anyone
at our wedding,

12
00:00:38,100 --> 00:00:40,570
but he was about
to explode your head.

13
00:00:40,610 --> 00:00:42,610
I love you, babe.

14
00:00:42,640 --> 00:00:43,740
<i>See?</i>

15
00:00:43,780 --> 00:00:45,210
I'm glad we watched that.

16
00:00:45,250 --> 00:00:48,510
That's exactly how I want
my hair for our ceremony.

17
00:00:48,550 --> 00:00:50,650
So, just saved you from a
murderous snake person

18
00:00:50,680 --> 00:00:51,780
at your own wedding?

19
00:00:51,820 --> 00:00:53,250
Oh, he was just an ex.

20
00:00:53,290 --> 00:00:54,990
We went on, like, three dates.

21
00:00:55,020 --> 00:00:57,120
Four if you count
the initial kidnapping.

22
00:00:57,160 --> 00:01:00,890
You know, I've never seen
you look at me that way.

23
00:01:00,930 --> 00:01:02,790
Would you call this
a French braid?

24
00:01:02,830 --> 00:01:04,560
No, that's a Dutch braid.
Hmm.

25
00:01:04,600 --> 00:01:07,530
I'd like to see Zorn
correctly identify a braid.

26
00:01:07,570 --> 00:01:10,700
I love you, Edie, and, hey,
I love your Dutch braid.

27
00:01:10,740 --> 00:01:11,740
Son of a...

28
00:01:20,280 --> 00:01:21,350
Hey, Alan.

29
00:01:21,380 --> 00:01:23,210
Oh, hey, Layla. What's up?

30
00:01:23,250 --> 00:01:25,380
I'm plant sitting for my aunt.
She's away this week,

31
00:01:25,420 --> 00:01:27,850
and I was just wondering
if maybe you wanted

32
00:01:27,890 --> 00:01:30,050
to come over tomorrow
and hang out.

33
00:01:30,090 --> 00:01:32,350
Totally, yeah.
Plants and no aunts?

34
00:01:32,380 --> 00:01:34,180
That sounds like my kind
of a party.

35
00:01:34,220 --> 00:01:35,850
Cool. Well, I'll text you
the address.

36
00:01:35,890 --> 00:01:37,990
Cool.

37
00:01:38,020 --> 00:01:40,590
Oh, uh, by the way,
it's-it's a beach house, so...

38
00:01:40,630 --> 00:01:41,590
Bring your swimsuit.

39
00:01:41,630 --> 00:01:43,290
Beach house?

40
00:01:43,330 --> 00:01:44,230
See you later.

41
00:01:44,260 --> 00:01:46,400
Yup.

42
00:01:51,700 --> 00:01:54,340
<i>Wait, isn't
Dr. Klorpins in Zephyria?</i>

43
00:01:54,370 --> 00:01:56,586
Yeah, well, that's what he wants
the FBI to think, too...

44
00:01:56,610 --> 00:01:58,210
And the CIA...

45
00:01:58,240 --> 00:01:59,640
And mothers against
drunk driving.

46
00:01:59,680 --> 00:02:00,610
Zorn.

47
00:02:00,650 --> 00:02:01,910
You're looking well.

48
00:02:01,950 --> 00:02:04,010
Someone must be taking
his fish person oil.

49
00:02:04,050 --> 00:02:07,620
Keeps the muscles tight
and the mind focused on eggs.

50
00:02:07,650 --> 00:02:09,350
That is actually why I'm here.

51
00:02:09,390 --> 00:02:11,150
Well, there's a fish guy
in room two.

52
00:02:11,190 --> 00:02:12,720
Go on. Take a suck
on his oil hole.

53
00:02:12,760 --> 00:02:14,620
All right.

54
00:02:14,660 --> 00:02:16,990
And this time maybe I'll let
the fish guy return the favor.

55
00:02:22,100 --> 00:02:24,300
Uh, hey, Dr. Klorpins,
what-what is, uh,

56
00:02:24,340 --> 00:02:25,400
zephyritol?

57
00:02:25,440 --> 00:02:26,770
Oh, it's just a drug

58
00:02:26,800 --> 00:02:28,340
that changes Zephyrian bodies

59
00:02:28,370 --> 00:02:30,510
so that they look
like regular bodies.

60
00:02:30,540 --> 00:02:31,810
And that really works?

61
00:02:31,840 --> 00:02:33,610
Like on any body part?
I don't know,

62
00:02:33,650 --> 00:02:35,480
just like a random example,
what about legs?

63
00:02:35,510 --> 00:02:38,010
Well, does Blake lively
look like she has tentacles

64
00:02:38,050 --> 00:02:39,520
under her skirt?

65
00:02:39,550 --> 00:02:41,750
Then I guess it works.

66
00:02:41,790 --> 00:02:43,690
Hey, uh, do you think
I could have some?

67
00:02:43,720 --> 00:02:46,520
Sure, I got some free samples
here somewhere.

68
00:02:46,560 --> 00:02:47,690
Let's see, scalpel...

69
00:02:47,730 --> 00:02:49,430
Oh, pus kit... ow!

70
00:02:50,392 --> 00:02:54,262
There's got to be a better place
to store old, dirty needles.

71
00:02:54,292 --> 00:02:55,632
Here you go, squirt.

72
00:02:56,732 --> 00:02:57,902
Ooh, all right, well,

73
00:02:57,932 --> 00:02:59,208
just give me my flu shot
and a lollipop,

74
00:02:59,232 --> 00:03:00,592
and let's get
the hell out of here.

75
00:03:07,742 --> 00:03:09,642
Craig. Did you hear that?

76
00:03:09,682 --> 00:03:11,582
Does this look like
the face of someone

77
00:03:11,612 --> 00:03:12,542
who didn't hear that?

78
00:03:12,582 --> 00:03:13,688
You want to go check it out?

79
00:03:13,712 --> 00:03:14,852
Does this feel like

80
00:03:14,882 --> 00:03:15,982
the heartbeat of someone

81
00:03:16,022 --> 00:03:17,852
who wants to go
check something out?

82
00:03:17,882 --> 00:03:19,282
Oh.

83
00:03:19,322 --> 00:03:20,922
Okay, we've got to do something.

84
00:03:20,952 --> 00:03:23,022
Okay, um,
let's just stay in bed,

85
00:03:23,062 --> 00:03:25,592
and if the murderer comes
in here, then I'll-I'll, like,

86
00:03:25,632 --> 00:03:27,292
distract him with a cloud
of feathers,

87
00:03:27,332 --> 00:03:30,262
and you make a run for it.

88
00:03:32,372 --> 00:03:33,332
What are you doing?

89
00:03:33,372 --> 00:03:35,272
Nothing. I just texted Zorn.

90
00:03:35,302 --> 00:03:36,572
Okay, well, fine.

91
00:03:36,602 --> 00:03:38,742
We'll just call Zorn,
let him swoop in,

92
00:03:38,772 --> 00:03:40,612
and I'll just sit here
and be useless.

93
00:03:40,642 --> 00:03:41,672
Ooh.

94
00:03:41,712 --> 00:03:43,212
Well, looks like it was

95
00:03:43,242 --> 00:03:45,382
a branch hitting your window.
Don't worry, though.

96
00:03:45,412 --> 00:03:47,282
I hacked it off,
uprooted the tree,

97
00:03:47,312 --> 00:03:49,458
and salted the earth so nothing
can ever grow there again.

98
00:03:49,482 --> 00:03:51,282
Oh, I can break a stick, Edie.

99
00:03:51,322 --> 00:03:52,922
If I had known
that was the problem,

100
00:03:52,952 --> 00:03:54,722
I would've taken
care of it myself.

101
00:03:54,762 --> 00:03:56,452
Oh, I know you would've,
sweetie.

102
00:03:56,492 --> 00:03:58,222
Hey, just so you know, Craig,

103
00:03:58,262 --> 00:04:00,862
she's lying.
Yeah, well,
just so you know, Zorn,

104
00:04:00,892 --> 00:04:02,932
you could be mean.

105
00:04:09,502 --> 00:04:11,142
I hate to sound
like such a dreamer,

106
00:04:11,172 --> 00:04:13,088
but there's got to be
a better way to shred documents.

107
00:04:13,112 --> 00:04:16,842
Actually, the machine you use
to make your smoothies

108
00:04:16,882 --> 00:04:18,312
is technically a paper...

109
00:04:18,352 --> 00:04:20,282
Todd!
No one likes a know-it-all.

110
00:04:21,252 --> 00:04:22,452
Zorn.

111
00:04:22,482 --> 00:04:24,452
Hey, hey,
didn't you get my memo?

112
00:04:24,482 --> 00:04:29,452
Hey, Blake Erickson,
the CEO of sanitation solutions,

113
00:04:29,492 --> 00:04:31,692
is coming in from Houston
to visit our branch.

114
00:04:31,732 --> 00:04:33,962
So I need everyone,
especially you,

115
00:04:33,992 --> 00:04:35,662
to be on their best behavior.

116
00:04:35,702 --> 00:04:37,362
That means no blasting
blues traveler

117
00:04:37,402 --> 00:04:39,062
"'cause it's 'run-around' time
somewhere."

118
00:04:39,102 --> 00:04:40,632
Geez, relax, man.

119
00:04:40,672 --> 00:04:42,172
You're the most
badass boss I know.

120
00:04:42,202 --> 00:04:43,702
Why are you so worried
about this guy?

121
00:04:43,742 --> 00:04:45,602
'Cause I think
Erickson hates me.

122
00:04:45,642 --> 00:04:46,912
Last time he was here,

123
00:04:46,942 --> 00:04:48,572
I said I'd rather
be dead in a ditch

124
00:04:48,612 --> 00:04:50,782
than alive
watching a tennis match.

125
00:04:50,812 --> 00:04:52,442
Turns out he's a huge tennis fan

126
00:04:52,482 --> 00:04:54,512
and his wife was found
dead in a ditch.

127
00:04:54,552 --> 00:04:56,252
Personally,
I'd rather be dead in a ditch

128
00:04:56,282 --> 00:04:57,482
than at a tennis match.

129
00:04:57,522 --> 00:04:59,952
I agree, but please
don't say that

130
00:04:59,992 --> 00:05:01,922
in front of our CEO
when he gets here.

131
00:05:01,962 --> 00:05:02,922
Okay, well, shut up a second.

132
00:05:02,962 --> 00:05:04,022
Zorn on the horn.

133
00:05:04,062 --> 00:05:05,262
Well, well, well.

134
00:05:05,292 --> 00:05:07,162
No, I'd-I'd love
to help you out,

135
00:05:07,192 --> 00:05:08,462
but I'm super busy

136
00:05:08,502 --> 00:05:09,578
here at work, okay? Whoops.

137
00:05:09,602 --> 00:05:10,762
No, he's not.

138
00:05:10,802 --> 00:05:11,862
Whoever this is,

139
00:05:11,902 --> 00:05:13,802
he'll be right there.

140
00:05:13,832 --> 00:05:14,948
Hey, sorry it took me so long.

141
00:05:14,972 --> 00:05:16,272
Traffic on the freeway

142
00:05:16,302 --> 00:05:18,242
was terribly hard
to walk through on foot.

143
00:05:19,272 --> 00:05:21,712
Zorn, I want you to train me

144
00:05:21,742 --> 00:05:23,172
to be more like you.

145
00:05:23,212 --> 00:05:25,682
Really? You need me
to train you to hate you?

146
00:05:25,712 --> 00:05:29,452
No, I want you to train me
to be a warrior.

147
00:05:29,482 --> 00:05:30,982
If I'm gonna marry Edie,

148
00:05:31,022 --> 00:05:32,352
I need to be able
to protect her.

149
00:05:32,392 --> 00:05:34,522
Being sweet, lovable Craig
isn't enough.

150
00:05:34,552 --> 00:05:36,492
I need to be tough,
lovable Craig.

151
00:05:36,522 --> 00:05:38,762
Alangulon.

152
00:05:38,792 --> 00:05:39,892
The fruit of my loins.

153
00:05:39,932 --> 00:05:42,292
Zorn, I want you to picture Alan

154
00:05:42,332 --> 00:05:44,932
trying to stand up
to a meth-crazed mugger.

155
00:05:44,972 --> 00:05:46,302
All right, cool.

156
00:05:46,332 --> 00:05:47,578
I'll dust off the old
imagination here,

157
00:05:47,602 --> 00:05:48,732
let me see what I got.

158
00:05:48,772 --> 00:05:50,072
Oh, my god!

159
00:05:50,102 --> 00:05:52,172
That junkie tore him to bits
in seconds.

160
00:05:52,212 --> 00:05:55,042
I mean, that was like running
a tomato through a band saw.

161
00:05:55,082 --> 00:05:57,712
Why-why did you make
me picture that?

162
00:05:57,742 --> 00:05:59,142
So you see my point, Zorn.

163
00:05:59,182 --> 00:06:00,152
Fine.

164
00:06:00,182 --> 00:06:02,052
I, Zorn, will make it

165
00:06:02,082 --> 00:06:04,622
my mission to train thee,

166
00:06:04,652 --> 00:06:08,252
Craig, in the ways
of the warrior!

167
00:06:08,292 --> 00:06:10,022
Here you go, Kale salad,

168
00:06:10,062 --> 00:06:12,122
no Parmesan,
extra lemon juice.
Ooh.

169
00:06:12,162 --> 00:06:13,932
B.S.! He'll have a live pig,

170
00:06:13,962 --> 00:06:17,002
and don't you dare help him
kill it when it arrives.

171
00:06:37,882 --> 00:06:39,822
Oh!

172
00:06:39,852 --> 00:06:41,752
Wow.

173
00:06:45,592 --> 00:06:48,262
Wait a second.

174
00:06:50,502 --> 00:06:52,202
Only one.

175
00:06:55,612 --> 00:06:56,612
So, this is the place.

176
00:06:56,642 --> 00:06:57,842
Uh, my aunt can't get pregnant

177
00:06:57,882 --> 00:06:59,712
and, uh, she doesn't
trust animals,

178
00:06:59,752 --> 00:07:02,082
so she says her plants
are her children.

179
00:07:02,122 --> 00:07:03,552
Wow.

180
00:07:03,592 --> 00:07:04,782
Oh, you got aloe.

181
00:07:04,822 --> 00:07:05,952
Yeah, we got aloe,

182
00:07:05,992 --> 00:07:07,852
we got drinks,
we got everything.

183
00:07:07,892 --> 00:07:09,562
Aloe is the best.
Aloe Vera.

184
00:07:09,592 --> 00:07:11,208
You know, a lot of people
say that aloe should not

185
00:07:11,232 --> 00:07:13,268
be applied to exquisite and
elegant legs such as these.

186
00:07:13,292 --> 00:07:14,332
Really?

187
00:07:14,362 --> 00:07:15,638
You mind if I, uh,
crack one of these off?

188
00:07:15,662 --> 00:07:16,962
Hop to it.

189
00:07:17,002 --> 00:07:18,432
Only the best for the best legs.

190
00:07:18,472 --> 00:07:20,772
How often do you apply?

191
00:07:20,802 --> 00:07:23,942
Every day at breakfast I apply
aloe to my real, human legs.

192
00:07:23,972 --> 00:07:26,042
You really do have amazing,
amazing legs.

193
00:07:26,072 --> 00:07:28,512
Oh, you know what?
Speaking of...

194
00:07:28,542 --> 00:07:30,088
These legs aren't
gonna tone themselves.

195
00:07:30,112 --> 00:07:31,542
Mind if I hop on this?

196
00:07:31,582 --> 00:07:32,582
Not at all.

197
00:07:32,612 --> 00:07:33,812
Wow, you know your plants

198
00:07:33,852 --> 00:07:36,182
and you can climb stairs.

199
00:07:36,222 --> 00:07:37,152
Oh, my god.

200
00:07:37,192 --> 00:07:38,482
Oh, yup, I'm good.

201
00:07:38,522 --> 00:07:40,192
Just a little
too much aloe on there.

202
00:07:41,292 --> 00:07:43,162
Zorn, is this really necessary?

203
00:07:43,192 --> 00:07:44,768
All right, the first step
in being a warrior

204
00:07:44,792 --> 00:07:45,992
is looking like a warrior.

205
00:07:46,032 --> 00:07:49,502
Hey, you look, like,
so fierce, man.

206
00:07:49,532 --> 00:07:52,032
Look at that.
I mean, you are no longer

207
00:07:52,072 --> 00:07:53,448
Craig the therapist.
You're more like

208
00:07:53,472 --> 00:07:55,742
Craig the psycho-therapist.

209
00:07:55,772 --> 00:07:57,188
But did you have
to rip the sleeves off?

210
00:07:57,212 --> 00:07:59,012
I just bought this shirt
last week.

211
00:07:59,042 --> 00:08:00,542
Warriors don't have sleeves.

212
00:08:00,582 --> 00:08:02,172
We need our arms free,

213
00:08:02,212 --> 00:08:03,712
you know, for windmill punches

214
00:08:03,752 --> 00:08:05,782
and raising our fists
victoriously, like this.

215
00:08:05,812 --> 00:08:08,712
Ha-ha-ha.
Yeah! Ha-ha.

216
00:08:08,752 --> 00:08:11,022
By the power of
Fullerton university

217
00:08:11,052 --> 00:08:13,992
online, continuing
adult education division,

218
00:08:14,022 --> 00:08:16,392
<i>department of psychology
and music.</i>

219
00:08:16,422 --> 00:08:18,622
<i>You shall not pass.</i>

220
00:08:28,902 --> 00:08:31,602
Zorn.

221
00:08:53,292 --> 00:08:55,092
Yes.

222
00:08:56,262 --> 00:08:58,632
You know, I totally thought

223
00:08:58,672 --> 00:09:01,102
you didn't know how to swim,
but you're actually pretty good.

224
00:09:01,142 --> 00:09:03,272
Well, I take a bath
every Sunday,

225
00:09:03,302 --> 00:09:05,642
so what is that
but a tiny ocean?

226
00:09:09,742 --> 00:09:11,212
Yeah, you should
come back Saturday.

227
00:09:11,252 --> 00:09:12,982
I'm gonna be here all day, so...

228
00:09:13,012 --> 00:09:16,652
Oh, uh, that sounds good.

229
00:09:16,682 --> 00:09:18,752
It's cold.

230
00:09:18,792 --> 00:09:20,792
Oh, you want to, uh,
wear this towel?

231
00:09:20,822 --> 00:09:22,962
Thank you.

232
00:09:22,992 --> 00:09:25,962
Uh... hold on a second.
Let me take that.

233
00:09:25,992 --> 00:09:27,662
Uh...
What?

234
00:09:27,702 --> 00:09:28,902
I just, uh, I just caught a...

235
00:09:28,932 --> 00:09:29,962
Caught a spider, yeah,

236
00:09:30,002 --> 00:09:31,162
between my leg and the towel.

237
00:09:31,202 --> 00:09:33,602
I got to go take care of that.

238
00:09:42,942 --> 00:09:45,042
Oh, Alan, how was
the beach house?

239
00:09:45,082 --> 00:09:46,512
A super magical day.

240
00:09:46,552 --> 00:09:48,552
I don't want to talk about it,
though. Thanks.

241
00:09:48,582 --> 00:09:50,222
Well, if you decide you do

242
00:09:50,252 --> 00:09:51,652
want to talk about it, I'm here.

243
00:09:53,052 --> 00:09:54,652
Come on, honey.
Let's rap.

244
00:09:54,692 --> 00:09:55,962
Kid's still say that, right?

245
00:09:55,992 --> 00:09:57,692
No, we don't, no.

246
00:09:57,732 --> 00:09:59,692
Well, do you want half
of my sandwich?

247
00:09:59,732 --> 00:10:01,392
No, OK... fine, sure,
don't come in.

248
00:10:01,432 --> 00:10:03,162
Just shove it
under the door, okay? Bye.

249
00:10:05,102 --> 00:10:06,732
<i>These numbers are terrible,
Linda.</i>

250
00:10:06,772 --> 00:10:08,902
I know. We've been
having some...

251
00:10:08,942 --> 00:10:10,972
Employee productivity issues.

252
00:10:11,012 --> 00:10:13,472
Todd!
Where have you been?

253
00:10:13,512 --> 00:10:15,358
I need that report on my
desk by the end of the day.

254
00:10:15,382 --> 00:10:16,612
Oh, I'm sorry.

255
00:10:16,642 --> 00:10:17,828
I was just playing some tennis.

256
00:10:17,852 --> 00:10:19,112
It helps me concentrate.

257
00:10:19,152 --> 00:10:20,852
I'm gonna tackle the
report right now.

258
00:10:20,882 --> 00:10:21,982
Tennis, you say?

259
00:10:22,022 --> 00:10:24,022
Oh.

260
00:10:24,052 --> 00:10:25,552
Important client.

261
00:10:25,592 --> 00:10:28,222
I'll be right back.

262
00:10:28,262 --> 00:10:29,962
Hey, Edie, what's up?

263
00:10:29,992 --> 00:10:31,892
Hey, have you seen Zorn?
No.

264
00:10:31,932 --> 00:10:32,932
Why? Everything okay?

265
00:10:32,962 --> 00:10:34,432
Well, I'm worried about Alan.

266
00:10:34,462 --> 00:10:36,202
He had a 40-minute shower cry.

267
00:10:36,232 --> 00:10:37,548
I don't know much
about parenting.

268
00:10:37,572 --> 00:10:38,732
I'm gonna guess gay.

269
00:10:38,772 --> 00:10:40,402
Okay, got to go.

270
00:10:40,432 --> 00:10:43,342
What do you think we can do
about our productivity problem?

271
00:10:43,372 --> 00:10:45,472
Well, I do have some
ideas, actually...

272
00:10:45,512 --> 00:10:47,872
Todd calls his mom
on company time.

273
00:10:49,312 --> 00:10:51,912
<i>Okay, warrior,
let's build up that confidence.</i>

274
00:10:51,952 --> 00:10:54,082
I'm gonna do a power poem.

275
00:10:54,122 --> 00:10:56,622
Capable.

276
00:10:56,652 --> 00:10:59,222
Resourceful.
Acrobatic.

277
00:10:59,252 --> 00:11:01,152
Incorrigible.

278
00:11:01,192 --> 00:11:03,522
Giggly?
Sometimes, yeah.

279
00:11:05,292 --> 00:11:06,262
Craig.

280
00:11:06,292 --> 00:11:07,962
Oh. Zorn.

281
00:11:08,002 --> 00:11:09,462
Do not barge into my bathroom.

282
00:11:09,502 --> 00:11:10,802
Or my bedroom.

283
00:11:10,832 --> 00:11:12,232
Or my dreams.

284
00:11:12,272 --> 00:11:14,132
All right, Craig, you
can become a warrior,

285
00:11:14,172 --> 00:11:16,372
or you can become someone
whose privacy I respect,

286
00:11:16,402 --> 00:11:17,872
but you can't become both.

287
00:11:17,912 --> 00:11:19,412
Zorn, do not slap me again.

288
00:11:19,442 --> 00:11:20,772
Enough is enough.

289
00:11:22,282 --> 00:11:24,212
You stood up to me.

290
00:11:24,252 --> 00:11:25,382
You are now a warrior.

291
00:11:25,412 --> 00:11:26,782
What about learning how
to fight?

292
00:11:26,812 --> 00:11:28,812
Nah, you're never gonna
be good at that, no.

293
00:11:28,852 --> 00:11:30,668
Here's a little tip, though,
just sucker punch people.

294
00:11:30,692 --> 00:11:32,098
Now get dressed
and drive me to work.

295
00:11:32,122 --> 00:11:34,252
Hell no, Zorn.
That's the spirit.

296
00:11:34,292 --> 00:11:36,822
But there is a time and a place,
and I really need a ride.

297
00:11:36,862 --> 00:11:38,622
<i>Has Alan seemed weird to you?</i>

298
00:11:38,662 --> 00:11:40,432
I feel like the clueless mom

299
00:11:40,462 --> 00:11:41,832
in an afterschool special.

300
00:11:41,862 --> 00:11:43,302
What, like he's on drugs?

301
00:11:43,332 --> 00:11:45,602
Edie, come on.
Let's be honest with ourselves.

302
00:11:45,632 --> 00:11:48,572
Alangulon's not cool enough
for anyone to offer him drugs.

303
00:11:48,602 --> 00:11:50,842
<i>No, I saw a thing on 60 minutes</i>

304
00:11:50,872 --> 00:11:52,912
that said even lame kids
can get drugs now.

305
00:11:52,942 --> 00:11:54,412
Edie, I got to go.

306
00:11:54,442 --> 00:11:56,582
<i>Linda.</i>

307
00:11:56,612 --> 00:11:59,182
Have you moved your desk
to work among your lessers?

308
00:11:59,212 --> 00:12:01,482
You truly you are
a man of the people.

309
00:12:01,522 --> 00:12:03,152
Zorn, I got demoted.

310
00:12:03,182 --> 00:12:06,292
Save your sucking up
for your new boss, Todd.
What?

311
00:12:06,322 --> 00:12:07,992
But you're the best boss ever.

312
00:12:08,022 --> 00:12:09,792
How could this happen?

313
00:12:09,822 --> 00:12:11,722
Seriously, I don't understand

314
00:12:11,762 --> 00:12:13,092
corporate employment structure,

315
00:12:13,132 --> 00:12:14,632
so I'm genuinely confused here.

316
00:12:14,662 --> 00:12:16,302
I was already on thin ice
with Erickson,

317
00:12:16,332 --> 00:12:17,948
and then Todd swooped in
and said all the right things

318
00:12:17,972 --> 00:12:20,572
and used all the right
tennis metaphors.

319
00:12:20,602 --> 00:12:22,378
Well, I mean, you know,
look at the bright side.

320
00:12:22,402 --> 00:12:24,348
Now you can sit out here
in cubicles with all of us

321
00:12:24,372 --> 00:12:25,772
and make Linda jokes.

322
00:12:25,812 --> 00:12:27,672
Screw Linda, right, sir?

323
00:12:27,712 --> 00:12:29,482
You don't need
to call me "sir."

324
00:12:29,512 --> 00:12:31,382
I'm not your superior anymore.

325
00:12:31,412 --> 00:12:33,382
Not my superior? Huh.

326
00:12:33,412 --> 00:12:35,182
Yeah. W-wait a second...

327
00:12:35,222 --> 00:12:36,622
Not my superior...

328
00:12:36,652 --> 00:12:38,252
Dresses...

329
00:12:38,292 --> 00:12:40,422
Earrings...

330
00:12:40,452 --> 00:12:43,862
Seeing a gynecologist
for your penis... I...

331
00:12:43,892 --> 00:12:46,892
Oh, my god.

332
00:12:46,932 --> 00:12:48,532
You're a woman!

333
00:12:51,002 --> 00:12:52,302
Please, okay? I just...

334
00:12:52,332 --> 00:12:54,302
I need more zephyritol.
I'm desperate.

335
00:12:54,342 --> 00:12:57,242
Well... there is an old saying

336
00:12:57,272 --> 00:12:59,002
the first taste is free.

337
00:12:59,042 --> 00:13:01,272
After that, bitch better
have my money.

338
00:13:01,312 --> 00:13:03,682
$500 to be exact.
What?

339
00:13:03,712 --> 00:13:06,112
No. Come on.
I need those pills.

340
00:13:06,152 --> 00:13:09,182
I didn't just change my legs,
it changed my entire life.

341
00:13:09,222 --> 00:13:11,368
I finally felt like the Alan
that I've always wanted to be.

342
00:13:11,392 --> 00:13:13,822
You know? Okay, the Alan that
can just walk up to anybody

343
00:13:13,852 --> 00:13:17,092
and say, "inspect me.
You'll find nothing weird."

344
00:13:17,122 --> 00:13:18,322
Just have a heart.

345
00:13:18,362 --> 00:13:19,762
I'm west Zephyrian.

346
00:13:19,792 --> 00:13:21,262
I actually don't have a heart.

347
00:13:21,302 --> 00:13:22,562
There is a ghost in my chest

348
00:13:22,602 --> 00:13:24,702
that scares the blood
around my body.

349
00:13:24,732 --> 00:13:27,032
So give me the money,
or get ready

350
00:13:27,072 --> 00:13:29,442
for some serious
withdrawal symptoms.

351
00:13:29,472 --> 00:13:30,672
Withdrawal?

352
00:13:42,232 --> 00:13:43,468
This site says
I should confront Alan

353
00:13:43,492 --> 00:13:45,192
and demand he tell me
what's going on.

354
00:13:45,232 --> 00:13:46,762
And if he refuses...

355
00:13:46,802 --> 00:13:48,302
I should...

356
00:13:48,332 --> 00:13:51,432
Send him to Bible
reeducation camp.

357
00:13:51,472 --> 00:13:52,832
What do you think, Craig?

358
00:13:52,872 --> 00:13:54,102
Craig.
I'm sorry.

359
00:13:54,142 --> 00:13:55,772
It's just the sandman

360
00:13:55,812 --> 00:13:57,642
has just been on my case today.

361
00:13:57,672 --> 00:13:58,958
Well, maybe you shouldn't have
gotten up to investigate

362
00:13:58,982 --> 00:14:00,512
every little sound last night.

363
00:14:00,542 --> 00:14:02,058
Well, maybe we shouldn't
have an air conditioner

364
00:14:02,082 --> 00:14:05,082
that sounds like a man
strangling Alan.
Alan.

365
00:14:05,112 --> 00:14:06,812
What are you doing?

366
00:14:06,852 --> 00:14:08,982
Oh, uh, me? I'm just
looking for a mint.

367
00:14:09,022 --> 00:14:10,922
Why are you wearing a pig nose?

368
00:14:10,952 --> 00:14:12,652
I'm doing the pigs...
Uh, factories.

369
00:14:12,692 --> 00:14:13,992
Protesting the pig factories.

370
00:14:14,022 --> 00:14:15,362
They are very bad, you know.

371
00:14:15,392 --> 00:14:17,862
Pigs are actually smarter
than dogs, so... poof.

372
00:14:17,892 --> 00:14:21,532
Yeah, I think I saw that on
someone's Facebook wall.

373
00:14:21,562 --> 00:14:24,272
Can I have $500 in cash
to do an art project?

374
00:14:24,302 --> 00:14:25,778
It's a really special thing
I'm working on

375
00:14:25,802 --> 00:14:27,702
where I take five $100 bills

376
00:14:27,742 --> 00:14:29,872
and I arrange them on a piece
of construction paper.

377
00:14:29,912 --> 00:14:32,012
It's a comment on commercialism
and stuff.

378
00:14:32,042 --> 00:14:33,972
Sounds like it's a good idea.

379
00:14:34,012 --> 00:14:35,742
$500 on art?

380
00:14:35,782 --> 00:14:37,312
Fine. I guess
I'll just go get it

381
00:14:37,352 --> 00:14:39,852
from that guy down
at the ravine.

382
00:14:39,882 --> 00:14:42,082
A couple weeks
learning about Jesus

383
00:14:42,122 --> 00:14:44,292
can't be that bad.

384
00:14:45,422 --> 00:14:47,192
Milady.

385
00:14:53,732 --> 00:14:54,962
Milady.

386
00:14:55,002 --> 00:14:56,802
Zorn, just because I'm a woman

387
00:14:56,832 --> 00:14:58,548
doesn't mean you have to stand
up every time I stand up.

388
00:14:58,572 --> 00:15:00,972
Of course, milady.
And please stop
talking to me

389
00:15:01,002 --> 00:15:04,002
like some sad-ass
lute player at ren fair.

390
00:15:04,042 --> 00:15:05,718
Yeah, you're right. I'm sorry.
It's just, you know,

391
00:15:05,742 --> 00:15:07,142
a lot to wrap my head around.

392
00:15:07,182 --> 00:15:09,142
I mean, you were a
woman this whole time?

393
00:15:09,182 --> 00:15:10,852
Wait, wait, wait, wait.
So that time

394
00:15:10,882 --> 00:15:13,082
we came to work dressed
as lady Zorn and she-Linda...

395
00:15:13,122 --> 00:15:14,682
You just showed up in a dress.

396
00:15:14,722 --> 00:15:16,452
We never discussed that
beforehand.

397
00:15:16,492 --> 00:15:19,322
It's just, I...
I feel like I lost

398
00:15:19,362 --> 00:15:21,492
my best friend at work,
I mean, you were my bro.

399
00:15:21,522 --> 00:15:25,032
You know? And now?
No mo' bro... you know?

400
00:15:25,062 --> 00:15:28,832
I'm still the same person
I always was, only now...
You can't vote.

401
00:15:28,872 --> 00:15:31,302
Nothing's changed.

402
00:15:31,332 --> 00:15:33,232
We're still friends, or at least

403
00:15:33,272 --> 00:15:34,802
I hope we can still be friends.

404
00:15:34,842 --> 00:15:37,272
Who else is gonna
help me overthrow Todd?

405
00:15:37,312 --> 00:15:39,572
Yeah, but I bet you
overthrow like a girl.

406
00:15:44,052 --> 00:15:46,512
500 bucks.

407
00:15:46,552 --> 00:15:48,558
Come on, something around here's
got to be worth 500 bucks.

408
00:15:48,582 --> 00:15:50,422
Oh. Laptop.

409
00:15:58,092 --> 00:16:01,202
Fullerton online university!

410
00:16:01,232 --> 00:16:04,802
So you think you can break
into Craig's fiance’s house

411
00:16:04,832 --> 00:16:06,672
where Craig lives rent-free...

412
00:16:06,702 --> 00:16:09,642
And take from Craig's fiance?

413
00:16:09,672 --> 00:16:12,572
Well, you got
another thing coming.
Stop!

414
00:16:12,612 --> 00:16:15,242
It's me.
Craig, what are you doing?

415
00:16:15,282 --> 00:16:17,812
I'm being the guy that you can
count on for everything, Edie.

416
00:16:17,852 --> 00:16:19,712
Oh, my spleen.

417
00:16:19,752 --> 00:16:20,922
Alan.

418
00:16:20,952 --> 00:16:22,322
Whoa!

419
00:16:22,352 --> 00:16:23,952
Oh, my god.

420
00:16:23,992 --> 00:16:26,522
I literally kicked
your ass back to Zephyria.

421
00:16:27,962 --> 00:16:29,108
<i>Well, it looks
like the zephyritol</i>

422
00:16:29,132 --> 00:16:30,462
<i>is starting to wear off.</i>

423
00:16:30,492 --> 00:16:32,068
I mean, who would
have thought that taking

424
00:16:32,092 --> 00:16:34,238
strange drugs from a cave doctor
would have consequences?

425
00:16:34,262 --> 00:16:36,862
Well, we're gonna talk about all
this when your father gets here.

426
00:16:36,902 --> 00:16:38,472
I called him.
He's very worried.

427
00:16:38,502 --> 00:16:39,732
Can I talk to you?

428
00:16:39,772 --> 00:16:40,872
Sure.

429
00:16:40,902 --> 00:16:43,042
You kicked my son's ass, Craig.

430
00:16:43,072 --> 00:16:44,372
Edie, I thought
he was a burglar.

431
00:16:44,412 --> 00:16:46,412
Look, I was just trying
to protect us,

432
00:16:46,442 --> 00:16:48,082
so you don't always
have to call Zorn

433
00:16:48,112 --> 00:16:49,512
at the first sign of trouble.

434
00:16:49,552 --> 00:16:52,182
I call Zorn because he's
a big dumb barbarian

435
00:16:52,212 --> 00:16:54,182
and he's good
at dumb barbarian stuff,

436
00:16:54,222 --> 00:16:57,152
like murder, maiden rescue,
car repair.

437
00:16:57,192 --> 00:16:58,938
I fell in love with you
because you are all the things

438
00:16:58,962 --> 00:17:00,222
that Zorn isn't.

439
00:17:00,262 --> 00:17:02,422
But if I can't do the things
that I do

440
00:17:02,462 --> 00:17:04,192
and the things that Zorn does,

441
00:17:04,232 --> 00:17:07,092
then I'll never be everything
that you need.

442
00:17:07,132 --> 00:17:08,962
You don't need to be everything.

443
00:17:09,002 --> 00:17:10,632
You're sweet and sensitive.

444
00:17:10,672 --> 00:17:12,902
Zorn's just...

445
00:17:12,942 --> 00:17:14,442
Zorn!

446
00:17:18,442 --> 00:17:20,272
Alangulon.
I have come to vanquish

447
00:17:20,312 --> 00:17:23,282
your drug-related issues.

448
00:17:26,452 --> 00:17:28,128
Come on, junkie. Spill it.
Tell me what you're hiding.

449
00:17:28,152 --> 00:17:29,482
Dad, I'm not a junkie.

450
00:17:29,522 --> 00:17:31,168
Ah. That is exactly what
a junkie would say.

451
00:17:31,192 --> 00:17:32,492
I was only taking zephyritol

452
00:17:32,522 --> 00:17:34,092
to make my legs look normal.

453
00:17:34,122 --> 00:17:35,962
I was just worried that
Layla wouldn't like me

454
00:17:35,992 --> 00:17:38,732
because I'm, like,
well, I'm different.

455
00:17:38,762 --> 00:17:40,802
Well, why do you care
if Layla loves you?

456
00:17:40,832 --> 00:17:42,532
She's not the woman
who birthed you.

457
00:17:42,572 --> 00:17:44,632
Edie, please. Alangulon.

458
00:17:44,672 --> 00:17:46,702
I know exactly what you're
going through.

459
00:17:46,742 --> 00:17:48,372
Take a seat, son.

460
00:17:48,412 --> 00:17:50,912
I recently found out
something insane

461
00:17:50,942 --> 00:17:52,372
about someone in my life.

462
00:17:52,412 --> 00:17:54,682
Turns out this guy I was hanging
with was a woman.

463
00:17:54,712 --> 00:17:56,512
A woman, Alangulon.

464
00:17:56,552 --> 00:17:58,052
Just imagine my surprise.

465
00:17:58,082 --> 00:17:59,252
And sure,

466
00:17:59,282 --> 00:18:01,582
at first it was an adjustment,

467
00:18:01,622 --> 00:18:03,082
but then I realized something.

468
00:18:03,122 --> 00:18:05,392
Once you really care
about someone,

469
00:18:05,422 --> 00:18:06,752
that's it.

470
00:18:06,792 --> 00:18:08,922
That feeling on the inside
doesn't change

471
00:18:08,962 --> 00:18:11,162
just because things might
look different on the outside.

472
00:18:11,192 --> 00:18:15,062
Wow, dad. That was
actually really helpful.

473
00:18:15,102 --> 00:18:17,502
Yeah, I've never heard you
say anything that deep.

474
00:18:17,532 --> 00:18:19,372
I didn't know you had it in you.

475
00:18:19,402 --> 00:18:21,472
No, no. I know.
Yeah, it blew my mind, too.

476
00:18:21,502 --> 00:18:24,312
But it turns out that I'm
kick-ass at being sensitive.

477
00:18:24,342 --> 00:18:26,612
Especially when it comes
to my family.
Oh.

478
00:18:29,682 --> 00:18:31,312
Ah, look at us.

479
00:18:31,352 --> 00:18:33,982
Sitting here just like
a real family.

480
00:18:34,022 --> 00:18:37,822
Holding hands
and silently farting.

481
00:18:39,992 --> 00:18:41,792
Hi, Alan.
Hey.

482
00:18:41,832 --> 00:18:42,992
Or did a spider kill you?

483
00:18:43,032 --> 00:18:44,962
Are you Alan's ghost?

484
00:18:44,992 --> 00:18:46,392
I actually came
here to apologize

485
00:18:46,432 --> 00:18:48,762
for how I've been acting
kind of crazy lately.

486
00:18:48,802 --> 00:18:50,402
Did you go on Adderall
or something?

487
00:18:50,432 --> 00:18:52,502
That's what made Shannon
so angry and impulsive

488
00:18:52,542 --> 00:18:54,202
and good at AP Spanish.

489
00:18:54,242 --> 00:18:55,488
I need to show you
something, okay?

490
00:18:55,512 --> 00:18:58,612
Alan. What-what
are you doing?!

491
00:18:58,642 --> 00:19:00,512
These are my legs.

492
00:19:00,542 --> 00:19:02,142
Whoa.

493
00:19:02,182 --> 00:19:04,982
I knew you'd be freaked out.

494
00:19:05,012 --> 00:19:07,052
Well, kind of, only because...

495
00:19:07,082 --> 00:19:08,952
You had different legs,
like, three days ago.

496
00:19:08,992 --> 00:19:10,992
Well, those weren't
my real legs.

497
00:19:11,022 --> 00:19:12,892
I was taking these weird
Zephyrian drugs

498
00:19:12,922 --> 00:19:15,462
to change them and... okay,
look, if you never want

499
00:19:15,492 --> 00:19:17,468
to see me or my freakish legs
ever again, I understand.

500
00:19:17,492 --> 00:19:18,938
That's fine.
Alan, the only thing
that's weirding me out

501
00:19:18,962 --> 00:19:20,278
is that you're standing here
with your pants

502
00:19:20,302 --> 00:19:21,532
around your ankles.

503
00:19:21,562 --> 00:19:24,232
I just feel like I
need to say sorry

504
00:19:24,272 --> 00:19:27,272
for not being honest with you.

505
00:19:27,302 --> 00:19:29,772
I like it when you're yourself.

506
00:19:29,812 --> 00:19:31,742
It's funny.
Oh.

507
00:19:31,772 --> 00:19:33,212
You know I have a-a Twitter

508
00:19:33,242 --> 00:19:35,982
where I post funny things
you say and do.

509
00:19:36,012 --> 00:19:38,252
It has like 450 followers.

510
00:19:38,282 --> 00:19:40,922
Wow. Um, is that like
a... is that a lot?

511
00:19:40,952 --> 00:19:42,882
Is that a little?
I don't really know.

512
00:19:42,922 --> 00:19:44,752
It's respectable.
Oh.

513
00:19:48,732 --> 00:19:51,162
Hey, Craig,

514
00:19:51,192 --> 00:19:53,232
I found those special baby wipes
you like to use

515
00:19:53,262 --> 00:19:55,702
instead of toilet paper.

516
00:19:55,732 --> 00:19:57,902
Ooh, you made muffins.

517
00:19:57,932 --> 00:20:00,302
It smells incredible.

518
00:20:00,342 --> 00:20:02,772
Oh.

519
00:20:02,812 --> 00:20:05,072
<i>My beloved Edie.</i>

520
00:20:05,112 --> 00:20:07,358
<i>By the time you're reading
this, the only thing left of me</i>

521
00:20:07,382 --> 00:20:09,382
<i>will be these good-bye muffins.</i>

522
00:20:09,412 --> 00:20:11,352
<i>They're apple-carrot.</i>

523
00:20:11,382 --> 00:20:14,052
<i>I know you won't admit this,
but it's become clear to me</i>

524
00:20:14,082 --> 00:20:17,082
<i>that you and Zorn are the ones
who should be together.</i>

525
00:20:17,122 --> 00:20:19,722
<i>Whatever reason you left him,</i>

526
00:20:19,762 --> 00:20:21,562
<i>he's now changed
into the husband you deserve</i>

527
00:20:21,592 --> 00:20:23,822
<i>and the protector
this family needs.</i>

528
00:20:23,862 --> 00:20:25,592
<i>It breaks my heart to leave,</i>

529
00:20:25,632 --> 00:20:27,332
<i>but please don't look for me.</i>

530
00:20:27,362 --> 00:20:29,502
<i>I know how to disappear.</i>

531
00:20:29,532 --> 00:20:33,402
<i>P.S. Make sure to individually
wrap these muffins</i>

532
00:20:33,442 --> 00:20:34,972
<i>in paper towels
before freezing them.</i>

533
00:20:35,002 --> 00:20:37,002
<i>It preserves the moistness.</i>

534
00:20:37,042 --> 00:20:39,272
Zorn? Are you in here...

535
00:20:39,312 --> 00:20:41,518
Oh. Hey, Edie. Hey, I just found
out Linda's a woman... twice.

536
00:20:41,542 --> 00:20:42,912
Hey-o. Well, I mean,

537
00:20:42,952 --> 00:20:44,258
technically,
one and a half times.

538
00:20:44,282 --> 00:20:45,642
You just bursting
into my apartment

539
00:20:45,643 --> 00:20:46,643
like that kinda ruins my sexy life.

