1
00:00:55,640 --> 00:00:59,110
1st night

2
00:04:24,960 --> 00:04:27,269
Excuse me, can I make the room?

3
00:08:26,320 --> 00:08:27,150
Lady?

4
00:08:30,680 --> 00:08:31,795
Excuse me?

5
00:08:36,720 --> 00:08:38,517
Are you from here?

6
00:09:01,360 --> 00:09:04,352
2nd night

7
00:09:30,520 --> 00:09:31,919
Good morning.

8
00:09:32,800 --> 00:09:35,268
It's cheap, 5 euros.

9
00:09:36,760 --> 00:09:38,557
The lighter too.

10
00:11:17,160 --> 00:11:17,751
No.

11
00:11:17,920 --> 00:11:19,990
- The belts, they're cheap.
- No.

12
00:13:07,800 --> 00:13:09,028
Grenadine, mint...

13
00:13:09,200 --> 00:13:10,155
Almond milk?

14
00:13:10,320 --> 00:13:11,753
Lemon. Almond milk.

15
00:13:11,920 --> 00:13:13,433
That wouldn't be bad.

16
00:13:13,600 --> 00:13:15,955
How about with gas?

17
00:13:16,120 --> 00:13:18,076
And water with gas, perhaps.

18
00:13:18,800 --> 00:13:20,711
That way we'll taste it.

19
00:13:20,960 --> 00:13:22,154
Yes, yes!

20
00:13:23,120 --> 00:13:24,394
A passion juice.

21
00:13:24,560 --> 00:13:26,278
Don't you prefer pear?

22
00:13:26,440 --> 00:13:27,395
Yes, pear.

23
00:13:28,040 --> 00:13:29,553
One pear juice.

24
00:13:46,040 --> 00:13:48,998
In Sylvie's city

25
00:14:38,560 --> 00:14:39,879
A wallet.

26
00:14:40,720 --> 00:14:41,835
Cheap.

27
00:14:43,760 --> 00:14:45,955
A lighter? A lion?

28
00:14:47,720 --> 00:14:49,312
It's cheap, 5 euros.

29
00:14:49,680 --> 00:14:51,875
It's cheap, 5 euros.

30
00:14:52,920 --> 00:14:55,070
I didn't order that.
I want juice.

31
00:14:55,280 --> 00:14:57,077
This is aggressive.

32
00:14:57,280 --> 00:14:58,998
Isn't sending it back too?

33
00:14:59,160 --> 00:15:00,434
I'd have coffee.

34
00:15:00,600 --> 00:15:01,999
I ordered something else.

35
00:15:02,160 --> 00:15:05,311
Do you realize what
you have ordered?

36
00:15:05,480 --> 00:15:07,118
We've finally decided.

37
00:15:07,280 --> 00:15:10,795
You ordered this, and
I've brought it to you.

38
00:15:10,960 --> 00:15:11,597
No, no.

39
00:15:11,840 --> 00:15:13,239
I have: A juice...

40
00:15:14,880 --> 00:15:15,630
Well...

41
00:15:15,800 --> 00:15:16,596
Yes, well...

42
00:15:16,760 --> 00:15:20,036
Well, leave the coffee,
I think.

43
00:15:33,240 --> 00:15:34,832
Her

44
00:15:43,000 --> 00:15:44,069
them

45
00:17:26,000 --> 00:17:27,513
A passion juice.

46
00:17:28,960 --> 00:17:30,552
No, a beer.

47
00:17:32,440 --> 00:17:33,316
Sorry.

48
00:17:34,880 --> 00:17:35,756
Thanks.

49
00:19:23,920 --> 00:19:24,636
No.

50
00:19:37,360 --> 00:19:38,839
I don't think so.

51
00:19:42,520 --> 00:19:44,511
But I'll think it over.

52
00:20:21,680 --> 00:20:24,990
Give me a coin, please.

53
00:20:26,920 --> 00:20:29,115
Drop dead, you dumb hick!

54
00:42:49,000 --> 00:42:49,989
Sorry.

55
00:42:54,400 --> 00:42:56,675
I picked these this morning.

56
00:42:58,960 --> 00:42:59,915
What?

57
00:43:00,080 --> 00:43:02,071
I picked them this morning.

58
00:43:04,800 --> 00:43:05,755
Thanks.

59
00:43:39,400 --> 00:43:40,992
Want a lighter?

60
00:43:41,480 --> 00:43:43,436
No, thanks. Thank you.

61
00:43:43,920 --> 00:43:46,912
A wallet, 5 euros, 4 euros.

62
00:43:48,080 --> 00:43:49,877
Belts, much cheaper.

63
00:44:01,840 --> 00:44:04,354
Do you have a cigarette, please?

64
00:44:05,680 --> 00:44:06,556
No.

65
00:48:18,800 --> 00:48:19,869
Sylvie.

66
00:48:22,080 --> 00:48:23,229
Sylvie?

67
00:48:25,720 --> 00:48:26,914
What is it?

68
00:48:27,280 --> 00:48:29,077
Don't you remember?

69
00:48:32,360 --> 00:48:33,236
We've met?

70
00:48:33,400 --> 00:48:35,470
"Les Aviateurs", 6 years ago.

71
00:48:35,640 --> 00:48:36,629
What?

72
00:48:36,840 --> 00:48:38,068
"Les Aviateurs".

73
00:48:38,480 --> 00:48:39,310
What's that?

74
00:48:39,480 --> 00:48:42,472
The "Les Aviateurs" bar,
behind the cathedral.

75
00:48:44,280 --> 00:48:45,349
You don't remember?

76
00:48:46,360 --> 00:48:50,069
I still have the map
you drew on a napkin.

77
00:48:51,880 --> 00:48:53,359
You don't remember?

78
00:48:54,280 --> 00:48:55,235
No?

79
00:48:55,600 --> 00:48:56,999
Yes, yes, yes.

80
00:48:58,280 --> 00:49:00,157
The "Les Aviateurs" bar.

81
00:49:02,160 --> 00:49:03,559
Sounds familiar.

82
00:49:04,360 --> 00:49:07,432
You were with 2 friends
from the Conservatory.

83
00:49:10,720 --> 00:49:13,518
The Higher School
of Dramatic Art.

84
00:49:14,000 --> 00:49:15,399
You don't remember?

85
00:49:15,840 --> 00:49:18,035
I'm sorry, I don't understand.

86
00:49:18,280 --> 00:49:21,590
You entered the Conservatory
6 years ago, right?

87
00:49:24,720 --> 00:49:27,518
You're mistaken,
I've been here a year.

88
00:49:31,600 --> 00:49:33,989
But you are Sylvie, aren't you?

89
00:49:34,600 --> 00:49:35,476
No.

90
00:49:37,800 --> 00:49:40,394
But... you're Sylvie, right?

91
00:49:41,560 --> 00:49:42,356
No.

92
00:49:45,000 --> 00:49:45,955
No.

93
00:49:50,240 --> 00:49:52,959
- You aren't Sylvie?
- No, you're mistaken.

94
00:49:57,160 --> 00:49:58,149
What?

95
00:50:04,280 --> 00:50:05,269
Sylvie.

96
00:50:13,200 --> 00:50:14,792
You're mistaken.

97
00:50:30,240 --> 00:50:31,639
What a disaster!

98
00:50:42,880 --> 00:50:44,359
What a disaster!

99
00:50:51,800 --> 00:50:53,518
I made a mistake.

100
00:50:57,720 --> 00:50:59,119
That's okay.

101
00:51:24,520 --> 00:51:26,715
You could have asked me sooner.

102
00:51:28,440 --> 00:51:29,429
What?

103
00:51:30,520 --> 00:51:33,114
It's not nice to follow people.

104
00:51:34,080 --> 00:51:35,308
Yes, I know.

105
00:51:35,480 --> 00:51:37,471
To follow women on the street.

106
00:51:37,640 --> 00:51:38,868
I wasn't sure.

107
00:51:39,040 --> 00:51:41,634
That's why you should have asked.

108
00:51:42,560 --> 00:51:44,755
Actually, I called you Sylvie

109
00:51:46,480 --> 00:51:48,198
and thought you answered.

110
00:51:48,360 --> 00:51:51,193
It's not pleasant to be followed.

111
00:51:52,360 --> 00:51:54,191
So you noticed?

112
00:51:55,000 --> 00:51:56,592
Couldn't you tell?

113
00:51:57,360 --> 00:51:58,156
No.

114
00:52:00,240 --> 00:52:02,231
Were you running from me?

115
00:52:02,640 --> 00:52:03,993
I sure was!

116
00:52:04,160 --> 00:52:07,232
I went round and
round trying to lose you.

117
00:52:09,600 --> 00:52:12,239
That itinerary was my fault?

118
00:52:12,400 --> 00:52:13,879
Yes, your fault.

119
00:52:15,480 --> 00:52:17,277
I was convinced...

120
00:52:19,240 --> 00:52:22,437
Not at first.
But later, I was convinced.

121
00:52:25,280 --> 00:52:28,670
It's unpleasant to be
followed on the street.

122
00:52:28,840 --> 00:52:31,229
- Yes, very...
- I wanted to be sure.

123
00:52:31,400 --> 00:52:33,197
Very, very unpleasant.

124
00:52:33,360 --> 00:52:35,635
It was very long, endless.

125
00:52:36,360 --> 00:52:38,351
I didn't know where to go.

126
00:52:38,960 --> 00:52:40,154
I'm sorry.

127
00:52:42,320 --> 00:52:45,551
And the shop I hid in
was closed.

128
00:52:49,280 --> 00:52:50,918
Didn't you notice?

129
00:52:51,640 --> 00:52:52,516
No.

130
00:52:55,840 --> 00:52:58,229
Since when are you following me?

131
00:52:58,760 --> 00:53:01,115
I saw you at the
Conservatory cafe.

132
00:53:01,280 --> 00:53:02,599
From the cafe?

133
00:53:06,720 --> 00:53:08,950
You followed me from the cafe?

134
00:53:09,800 --> 00:53:10,789
Yes.

135
00:53:11,360 --> 00:53:12,554
I'm sorry.

136
00:53:12,720 --> 00:53:14,119
How awful!

137
00:53:17,200 --> 00:53:18,838
What a disaster!

138
00:54:07,440 --> 00:54:09,396
You're so similar...

139
00:54:11,880 --> 00:54:14,440
Sure, 6 years...

140
00:54:18,400 --> 00:54:19,992
You are younger.

141
00:54:23,280 --> 00:54:26,477
She was younger too,
6 years ago.

142
00:54:33,600 --> 00:54:36,592
Now she is older,
but still young.

143
00:54:37,200 --> 00:54:40,351
Yes... sure.
Less younger...

144
00:55:00,240 --> 00:55:02,037
I'm very sorry, really.

145
00:55:03,680 --> 00:55:05,159
I get off here.

146
00:55:05,880 --> 00:55:06,915
I'm sorry.

147
00:55:07,080 --> 00:55:09,275
I hope you don't get off here.

148
00:55:09,800 --> 00:55:11,279
No, don't worry.

149
00:55:11,760 --> 00:55:13,034
I'm getting off.

150
00:55:13,200 --> 00:55:15,509
- I'm really sorry.
- Forget it.

151
00:55:15,680 --> 00:55:16,510
I feel awful.

152
00:55:22,760 --> 00:55:23,988
I have to get off.

153
00:55:24,160 --> 00:55:25,593
Are you on vacation?

154
00:55:25,760 --> 00:55:26,988
Have a nice trip!

155
00:55:27,760 --> 00:55:29,113
Bye, and forgive me.

156
00:55:29,280 --> 00:55:30,429
That's enough!

157
00:55:30,720 --> 00:55:32,756
I hope you find her.

158
01:05:30,680 --> 01:05:33,558
3rd night

159
01:09:11,400 --> 01:09:13,868
Excuse me,
do you have a cigarette?

