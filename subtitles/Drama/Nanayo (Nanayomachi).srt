1
00:01:09,702 --> 00:01:16,540
<i>NANAYO</i>

2
00:12:07,126 --> 00:12:10,061
Is something wrong?

3
00:12:10,229 --> 00:12:12,595
Will you come with me?

4
00:12:13,032 --> 00:12:14,294
Will you?

5
00:12:15,735 --> 00:12:17,430
You don't speak French?

6
00:12:22,141 --> 00:12:24,507
I'll take you somewhere, OK?

7
00:12:34,820 --> 00:12:37,482
What happened?

8
00:12:53,639 --> 00:12:54,833
Hi, Amari.

9
00:12:55,775 --> 00:12:57,242
Ah, Greg...

10
00:13:01,614 --> 00:13:02,945
Hello.

11
00:13:05,951 --> 00:13:10,547
No, someone attacked her
in the woods.

12
00:13:12,792 --> 00:13:14,225
Mommy!

13
00:13:15,394 --> 00:13:16,486
Poop!

14
00:13:16,662 --> 00:13:19,153
Oh, dear! Come on, then.

15
00:13:30,743 --> 00:13:31,767
You OK?

16
00:13:40,052 --> 00:13:43,681
Would you like
some more tea?

17
00:13:47,226 --> 00:13:50,024
Could we have some more tea?

18
00:13:50,462 --> 00:13:52,589
Mother, give them some tea.

19
00:13:52,832 --> 00:13:54,197
I just did.

20
00:13:57,636 --> 00:13:58,625
Hello, Marwin!

21
00:13:58,804 --> 00:14:00,032
What is it?

22
00:14:09,615 --> 00:14:11,674
What happened?

23
00:14:12,218 --> 00:14:15,449
Did he attack her?
She's scared of him.

24
00:14:27,066 --> 00:14:28,863
Stay and eat.

25
00:14:30,569 --> 00:14:31,695
What's wrong?

26
00:14:31,770 --> 00:14:35,831
Nothing. I was just
passing by in my taxi.

27
00:14:36,008 --> 00:14:38,033
Well, anyway...

28
00:14:38,711 --> 00:14:41,202
Mother, bring Marwin some water.

29
00:14:59,531 --> 00:15:02,227
Would you like to eat?

30
00:15:41,473 --> 00:15:46,069
Would you like a massage?
I'll give you one.

31
00:15:48,580 --> 00:15:49,808
A massage.

32
00:15:51,216 --> 00:15:52,410
Go on...

33
00:15:57,990 --> 00:15:59,821
Lie down.

34
00:23:11,723 --> 00:23:13,816
First, spread the towel.

35
00:23:17,329 --> 00:23:21,629
Then cross the feet,
and place your hands on them.

36
00:23:21,933 --> 00:23:24,060
Press down lightly.

37
00:23:24,870 --> 00:23:26,098
Hey, Saiko...

38
00:23:27,906 --> 00:23:28,930
Come here.

39
00:23:31,376 --> 00:23:32,536
Come on.

40
00:23:33,412 --> 00:23:34,640
Sit down.

41
00:23:34,980 --> 00:23:36,538
Over here.

42
00:23:39,551 --> 00:23:42,714
Have a massage. Go on.

43
00:23:44,489 --> 00:23:48,858
Let Greg practice massage on you.

44
00:23:49,528 --> 00:23:50,688
There.

45
00:23:57,335 --> 00:23:58,302
Good.

46
00:24:19,157 --> 00:24:20,385
No.

47
00:24:22,461 --> 00:24:24,691
Come back to the feet.

48
00:24:24,896 --> 00:24:27,729
Three times, I told you.

49
00:24:27,899 --> 00:24:31,300
Here again.
Press three times on the feet.

50
00:24:31,470 --> 00:24:33,165
OK? Back here.

51
00:24:33,338 --> 00:24:36,466
I'll show you. Watch.

52
00:24:36,641 --> 00:24:37,938
Like this.

53
00:24:41,279 --> 00:24:42,712
Sit up.

54
00:24:46,318 --> 00:24:50,755
Greg, you lie down.
Behave yourself!

55
00:24:52,491 --> 00:24:55,551
I'll show you how to do it.

56
00:24:56,027 --> 00:24:57,289
OK?

57
00:24:58,530 --> 00:25:00,293
Stretch out your legs.

58
00:25:01,533 --> 00:25:03,023
Come here.

59
00:25:03,268 --> 00:25:05,395
Come on, try it.

60
00:25:06,271 --> 00:25:07,431
<i>Me?</i>

61
00:25:29,094 --> 00:25:32,222
First you pray to the spirits...

62
00:25:33,131 --> 00:25:35,793
...of the masters of massage.

63
00:25:36,167 --> 00:25:38,328
OK? Like this.

64
00:25:39,404 --> 00:25:40,837
Good.

65
00:25:41,106 --> 00:25:42,471
Like this.

66
00:25:46,912 --> 00:25:48,004
That's right.

67
00:25:49,481 --> 00:25:50,413
Good.

68
00:25:50,615 --> 00:25:52,276
Now we start.

69
00:25:53,919 --> 00:25:55,284
From the feet.

70
00:25:58,757 --> 00:26:01,920
Feet together, like this.

71
00:26:04,596 --> 00:26:06,826
That's right. Now press.

72
00:26:07,065 --> 00:26:10,057
Like that. Do what I do.

73
00:26:15,674 --> 00:26:20,008
This hand above.
Hold them like this.

74
00:26:21,580 --> 00:26:23,707
Now press down lightly.

75
00:26:25,517 --> 00:26:27,781
Hey, watch the nails!

76
00:26:28,086 --> 00:26:28,950
<i>Are you OK?</i>

77
00:26:31,723 --> 00:26:33,122
What are you doing?!

78
00:26:33,291 --> 00:26:35,350
She dug her nails in me!

79
00:26:36,127 --> 00:26:37,651
<i>What was that for?!</i>

80
00:26:38,229 --> 00:26:39,093
What?

81
00:26:39,431 --> 00:26:41,626
She dug her nails in me!

82
00:26:43,101 --> 00:26:44,659
Show me your nails.

83
00:26:45,370 --> 00:26:48,134
They're pretty long...

84
00:26:48,707 --> 00:26:51,232
That's not good for massage.

85
00:26:54,446 --> 00:26:57,847
Are you OK? Are you hurt?

86
00:26:58,216 --> 00:26:59,615
You got a bump?

87
00:27:05,023 --> 00:27:08,550
Where? There's no bump here.

88
00:27:08,827 --> 00:27:10,488
There's no bump.

89
00:27:10,929 --> 00:27:12,294
No bumps.

90
00:27:12,731 --> 00:27:16,326
But look here,
you scratched me.

91
00:27:16,601 --> 00:27:18,398
Not a mark on you!

92
00:27:23,341 --> 00:27:26,208
To do it right,
you need to concentrate.

93
00:27:26,444 --> 00:27:30,073
They respect massage here.

94
00:27:32,283 --> 00:27:33,716
Do you understand?

95
00:27:39,758 --> 00:27:42,955
It's in the breathing,
and listening.

96
00:27:43,662 --> 00:27:46,756
You don't understand, do you.

97
00:27:47,732 --> 00:27:49,256
<i>I don't understand.</i>

98
00:27:56,241 --> 00:27:57,731
Do you want to learn?

99
00:28:01,746 --> 00:28:03,646
Do you want to?

100
00:28:04,949 --> 00:28:06,177
<i>I don't understand.</i>

101
00:28:06,551 --> 00:28:08,143
Are you going to hurt me again?

102
00:28:11,056 --> 00:28:13,354
You're not angry any more?

103
00:28:16,094 --> 00:28:19,689
When I talk to you,
you're always angry.

104
00:28:19,864 --> 00:28:23,698
When I threw you off me,
you were angry.

105
00:28:24,035 --> 00:28:25,730
Whatever.

106
00:28:25,970 --> 00:28:27,301
The wind's come up.

107
00:28:27,439 --> 00:28:29,464
Yes, you're right.

108
00:28:29,941 --> 00:28:32,501
It's clouded over.

109
00:28:36,047 --> 00:28:37,207
'Water'.

110
00:28:37,382 --> 00:28:38,508
'La pluie'.

111
00:28:38,917 --> 00:28:41,351
'Rain'. 'La pluie'.

112
00:28:45,023 --> 00:28:46,547
Not 'puryui'.

113
00:28:50,929 --> 00:28:51,918
'Pluie'.

114
00:29:02,841 --> 00:29:04,866
'Water'. 'Eau'.

115
00:29:09,814 --> 00:29:10,746
'Mizu'.

116
00:29:24,929 --> 00:29:25,861
'Kanpai'.

117
00:29:26,765 --> 00:29:28,756
<i>You know 'cheers' in Japanese?</i>

118
00:29:29,367 --> 00:29:30,356
What?

119
00:29:31,436 --> 00:29:32,232
'Sant�'.

120
00:29:32,403 --> 00:29:33,495
In Thai?

121
00:29:33,671 --> 00:29:35,104
'Chai yo'.

122
00:29:43,114 --> 00:29:44,672
You too, Toi.

123
00:29:47,886 --> 00:29:48,875
In French?

124
00:29:56,094 --> 00:29:57,823
She's still here.

125
00:30:05,336 --> 00:30:09,670
When you're all playing like that
you look like a family.

126
00:30:09,841 --> 00:30:10,739
You're right.

127
00:30:12,844 --> 00:30:17,076
That was with one.
Now do it with two.

128
00:30:20,151 --> 00:30:22,244
Nope. What if I do one twice?

129
00:30:22,420 --> 00:30:24,012
No. Two at once.

130
00:31:39,898 --> 00:31:41,058
Let it go.

131
00:31:41,232 --> 00:31:43,132
Really?

132
00:31:43,601 --> 00:31:44,829
Let it go?

133
00:33:13,624 --> 00:33:14,591
Toi...

134
00:33:15,593 --> 00:33:16,491
Toi...

135
00:33:19,797 --> 00:33:20,855
<i>'Morning.</i>

136
00:33:23,801 --> 00:33:25,268
<i>'Morning.</i>

137
00:33:31,009 --> 00:33:33,569
<i>My nails are too long</i>
<i>to play with you.</i>

138
00:33:34,178 --> 00:33:35,509
<i>I have to cut them.</i>

139
00:33:41,652 --> 00:33:43,210
<i>It's sparkling.</i>

140
00:33:44,689 --> 00:33:45,951
<i>You do it, too.</i>

141
00:33:54,532 --> 00:33:55,999
<i>It's sparkling.</i>

142
00:34:11,182 --> 00:34:14,948
Stop it! That hurts!

143
00:34:15,119 --> 00:34:16,347
OK?

144
00:34:29,367 --> 00:34:32,825
Go on. You try it.

145
00:34:33,004 --> 00:34:34,198
I know how.

146
00:34:34,539 --> 00:34:35,631
Do it.

147
00:34:38,443 --> 00:34:39,137
Can you?

148
00:34:39,377 --> 00:34:41,902
Sure I can!

149
00:34:55,560 --> 00:34:56,822
He's good.

150
00:34:57,095 --> 00:34:58,858
He sees me every day.

151
00:35:19,750 --> 00:35:21,581
Stretch your legs out.

152
00:35:21,752 --> 00:35:23,982
Let go with your hands.

153
00:35:28,626 --> 00:35:29,820
Down!

154
00:36:10,034 --> 00:36:11,695
Like it?

155
00:38:21,732 --> 00:38:26,135
You know, I always hid
my homosexuality from myself.

156
00:38:26,737 --> 00:38:29,797
And perhaps...

157
00:38:30,775 --> 00:38:33,539
...as I let things take their course...

158
00:38:35,780 --> 00:38:39,181
...I'm starting to find
a link with the world.

159
00:38:41,052 --> 00:38:45,489
In my work,
it's always me. Just me.

160
00:38:45,656 --> 00:38:47,180
I got sick of it.

161
00:38:47,358 --> 00:38:49,553
That's why I came here.

162
00:38:51,195 --> 00:38:55,859
I want to disappear behind an action,
melt into an action.

163
00:38:56,367 --> 00:38:58,232
Concentrate myself.

164
00:38:58,402 --> 00:39:01,200
Feel pain where it is, in others...

165
00:39:01,372 --> 00:39:04,432
...and soothe it.
That's all that interests me now.

166
00:39:07,411 --> 00:39:09,709
That's why I'm here.

167
00:39:14,352 --> 00:39:15,649
See this?

168
00:39:20,091 --> 00:39:21,956
<i>Did your girlfriend give you this?</i>

169
00:39:34,438 --> 00:39:35,462
<i>Who's it from?</i>

170
00:39:43,914 --> 00:39:44,778
<i>What?</i>

171
00:39:51,655 --> 00:39:53,020
I can't take it off.

172
00:39:54,358 --> 00:39:56,758
I always wear it.

173
00:39:58,329 --> 00:40:00,297
Otherwise I'd give it to you.

174
00:40:12,443 --> 00:40:14,172
It's going to rain.

175
00:40:14,478 --> 00:40:15,740
<i>La pluie.</i>

176
00:40:16,414 --> 00:40:17,438
'Lovely'?

177
00:40:21,051 --> 00:40:22,450
<i>Someone dear?</i>

178
00:40:23,421 --> 00:40:25,389
<i>Oh, 'rain '?</i>

179
00:41:15,206 --> 00:41:16,503
I was a soldier.

180
00:41:23,948 --> 00:41:25,882
It was scary.

181
00:41:26,050 --> 00:41:29,713
You're in a house,
talking like this...

182
00:41:30,354 --> 00:41:34,381
Suddenly a bomb
smashes the roof...

183
00:41:40,931 --> 00:41:42,296
<i>'Die'.</i>

184
00:41:44,101 --> 00:41:45,830
<i>'Dangerous'.</i>

185
00:41:47,972 --> 00:41:48,870
<i>'Die'.</i>

186
00:41:49,874 --> 00:41:52,342
'Dangerous'.

187
00:41:52,510 --> 00:41:53,568
'Abunai'.

188
00:41:55,246 --> 00:41:58,807
There's danger everywhere.

189
00:41:58,983 --> 00:42:02,646
Toi might drown.

190
00:42:02,820 --> 00:42:03,946
'Splash'!

191
00:42:08,425 --> 00:42:13,294
Amari might accidentally
cut her wrist cooking...

192
00:42:14,698 --> 00:42:16,791
...and die.

193
00:42:19,036 --> 00:42:20,025
Me...

194
00:42:21,305 --> 00:42:22,363
...I've seen it.

195
00:42:24,375 --> 00:42:27,367
Lots of dead bodies.

196
00:42:29,547 --> 00:42:33,608
People... me, you, him...

197
00:42:34,084 --> 00:42:36,177
...shouldn't kill each other.

198
00:42:36,987 --> 00:42:39,785
We should never do that.

199
00:42:43,160 --> 00:42:44,627
Understand?

200
00:42:46,497 --> 00:42:47,930
Do you?

201
00:42:50,801 --> 00:42:52,200
<i>'Understand?'</i>

202
00:42:55,372 --> 00:42:56,805
<i>More or less.</i>

203
00:42:58,842 --> 00:42:59,900
Thai...

204
00:43:00,744 --> 00:43:03,907
...Japanese, French.

205
00:43:05,149 --> 00:43:06,707
All different.

206
00:43:08,085 --> 00:43:10,952
But we shouldn't kill each other.

207
00:43:11,689 --> 00:43:13,247
We should love.

208
00:43:16,961 --> 00:43:18,986
She doesn't understand.

209
00:43:22,499 --> 00:43:23,864
Understand?

210
00:43:29,573 --> 00:43:31,336
<i>Do you understand?</i>

211
00:43:31,508 --> 00:43:33,135
You speak Japanese?

212
00:43:33,811 --> 00:43:35,301
You speak Japanese?

213
00:43:47,358 --> 00:43:50,327
He wants to meet his father.

214
00:43:50,761 --> 00:43:53,286
I want Toi...

215
00:43:53,964 --> 00:43:55,693
...to meet his father.

216
00:43:59,303 --> 00:44:03,069
What's Japan like?

217
00:44:04,541 --> 00:44:05,940
Beautiful?

218
00:44:10,414 --> 00:44:13,508
<i>Beautiful? What's it like?</i>

219
00:44:14,184 --> 00:44:16,744
<i>What's Japan like?</i>

220
00:44:23,160 --> 00:44:24,422
<i>Japan</i> is...

221
00:44:30,634 --> 00:44:32,226
<i>.... Very peaceful.</i>

222
00:44:39,610 --> 00:44:41,475
<i>We're blessed.</i>

223
00:44:48,118 --> 00:44:49,142
<i>But....</i>

224
00:44:54,825 --> 00:44:58,056
<i>.... We don't have...</i>

225
00:45:01,065 --> 00:45:04,933
<i>.... The warmth...</i>

226
00:45:09,873 --> 00:45:11,864
<i>.... That you all have.</i>

227
00:45:36,533 --> 00:45:38,330
Give me some advice.

228
00:45:38,936 --> 00:45:43,100
I want to put Toi into the temple.
What do you think?

229
00:45:44,308 --> 00:45:48,176
Put him in the temple?
Have you asked him?

230
00:45:48,345 --> 00:45:51,940
Not yet. I'm still thinking.

231
00:45:52,116 --> 00:45:55,142
You'd better ask him first.

232
00:45:55,519 --> 00:45:59,250
Even a loser like me...
I wish I could see my daughter.

233
00:46:02,059 --> 00:46:03,924
Did something happen?

234
00:46:06,563 --> 00:46:08,292
What?

235
00:46:11,001 --> 00:46:13,970
Nothing. I'd just like to see her.

236
00:46:17,441 --> 00:46:18,408
Drink?

237
00:46:18,976 --> 00:46:19,874
There.

238
00:46:25,716 --> 00:46:27,479
<i>I'd like some water.</i>

239
00:46:29,052 --> 00:46:31,486
I wonder where they sell it...

240
00:46:41,198 --> 00:46:42,495
This is good.

241
00:46:43,000 --> 00:46:44,467
Beer?

242
00:46:45,302 --> 00:46:46,326
Can I have this?

243
00:46:46,870 --> 00:46:48,963
All right, Marwin?

244
00:46:49,473 --> 00:46:51,407
Singha Beer?

245
00:46:52,342 --> 00:46:53,434
One can.

246
00:46:55,078 --> 00:46:56,136
Singha?

247
00:47:02,986 --> 00:47:05,887
'Dtao huu'.

248
00:47:08,492 --> 00:47:10,585
Water.

249
00:47:11,728 --> 00:47:12,888
'Dtao huu'.

250
00:47:15,332 --> 00:47:16,458
<i>Tofu.</i>

251
00:47:16,934 --> 00:47:18,868
<i>'Tofu water'?</i>

252
00:47:19,469 --> 00:47:21,460
<i>Like milk.</i>

253
00:47:22,439 --> 00:47:24,168
<i>It's sweet?</i>

254
00:47:30,881 --> 00:47:32,974
What should I say?

255
00:47:41,992 --> 00:47:43,118
Rice crackers.

256
00:47:44,328 --> 00:47:47,126
Do you have any fresh ones?

257
00:47:47,798 --> 00:47:48,992
No?

258
00:47:49,833 --> 00:47:50,925
Too bad.

259
00:47:51,101 --> 00:47:53,763
Let's get four bags.

260
00:47:53,971 --> 00:47:55,563
One's enough.

261
00:48:00,143 --> 00:48:03,010
Let's get something for Toi.

262
00:48:55,465 --> 00:48:57,865
No thanks, girls.

263
00:48:58,035 --> 00:49:00,003
No thanks, I said!

264
00:49:00,170 --> 00:49:02,195
Go away.

265
00:49:04,808 --> 00:49:06,036
Stop it.

266
00:49:06,843 --> 00:49:08,538
Why did you hit her?!

267
00:49:18,689 --> 00:49:20,213
Run!

268
00:49:23,527 --> 00:49:26,325
Hold it, you!

269
00:49:26,496 --> 00:49:27,690
I'm running!

270
00:49:29,099 --> 00:49:29,724
Marwin!

271
00:49:29,900 --> 00:49:30,867
Asshole!

272
00:54:55,825 --> 00:55:00,057
Were you watching?
Come pray with us tomorrow.

273
00:55:00,664 --> 00:55:03,155
<i>Together, tomorrow.</i>

274
00:55:04,734 --> 00:55:07,567
Let's go have breakfast.

275
00:55:40,303 --> 00:55:43,636
Your daddy's Japanese.

276
00:55:47,344 --> 00:55:49,471
You know that, don't you.

277
00:55:51,214 --> 00:55:53,512
But we're in Thailand.

278
00:55:54,117 --> 00:55:56,244
Here you can be a monk.

279
00:56:17,307 --> 00:56:18,274
OK?

280
00:56:26,383 --> 00:56:29,580
Would you like to be a monk?

281
00:56:31,654 --> 00:56:32,780
A monk?

282
00:56:35,258 --> 00:56:36,691
Why?

283
00:56:37,861 --> 00:56:40,022
I want you to.

284
00:56:49,773 --> 00:56:51,331
Could you do that?

285
00:56:52,575 --> 00:56:54,167
You'll live in a temple.

286
00:56:56,279 --> 00:56:58,440
And be a monk.

287
00:56:58,615 --> 00:57:00,014
Always?

288
00:57:01,284 --> 00:57:02,808
Forever?

289
00:57:04,053 --> 00:57:07,284
You can if you want.

290
00:57:07,957 --> 00:57:10,892
Not forever.

291
00:57:11,394 --> 00:57:13,362
Maybe for a while.

292
00:57:18,435 --> 00:57:20,460
Could you live there alone?

293
00:57:25,775 --> 00:57:28,073
I'm a woman,
so I can't go with you.

294
00:57:30,046 --> 00:57:34,483
But the other monks
would be there with you.

295
00:57:35,819 --> 00:57:39,516
In the morning
I'll come with an offering.

296
00:57:40,657 --> 00:57:43,057
In a bowl, just like that one.

297
00:57:43,226 --> 00:57:47,026
You'll accept
your mother's offering.

298
00:57:47,630 --> 00:57:52,329
'Please accept this,' I'll say.

299
00:57:56,172 --> 00:57:58,470
Can I go?

300
00:57:59,008 --> 00:58:00,805
I want down.

301
00:58:04,881 --> 00:58:06,439
OK?

302
00:58:07,984 --> 00:58:08,951
Why?

303
00:58:09,118 --> 00:58:10,608
I'm thirsty.

304
00:58:12,822 --> 00:58:14,346
Can I go?

305
00:58:30,173 --> 00:58:32,141
Did Daddy love you?

306
00:59:05,642 --> 00:59:07,974
Is Japan that way?

307
00:59:10,146 --> 00:59:11,477
That way?

308
00:59:13,816 --> 00:59:16,216
Where I can meet my daddy?

309
00:59:37,273 --> 00:59:39,036
<i>What do you want?!</i>

310
00:59:39,208 --> 00:59:43,508
What kind of father
have you ever been to me?!

311
00:59:44,080 --> 00:59:45,741
You never paid for anything!

312
00:59:45,915 --> 00:59:49,146
Sure I did! I work hard!

313
00:59:49,786 --> 00:59:51,048
It's not enough!

314
00:59:51,220 --> 00:59:54,246
That's no excuse
for selling yourself.

315
00:59:54,424 --> 00:59:57,154
I need to work!

316
00:59:57,327 --> 00:59:59,591
Who was that yesterday?

317
00:59:59,762 --> 01:00:02,697
My boyfriend!

318
01:00:02,865 --> 01:00:06,028
A foreign boyfriend?
You're a whore.

319
01:00:06,202 --> 01:00:09,262
That's not true!

320
01:00:09,772 --> 01:00:13,333
You were all over him!
In my taxi!

321
01:00:13,509 --> 01:00:16,103
That doesn't concern you!

322
01:00:16,279 --> 01:00:16,904
Come on!

323
01:00:17,080 --> 01:00:18,445
No!

324
01:00:18,615 --> 01:00:19,445
We're going.

325
01:00:19,616 --> 01:00:20,048
No!

326
01:00:20,216 --> 01:00:20,978
Come on!

327
01:00:21,150 --> 01:00:22,117
No!

328
01:02:02,552 --> 01:02:03,746
Mother?

329
01:02:04,821 --> 01:02:06,254
Where's Toi?

330
01:02:06,422 --> 01:02:07,889
I haven't seen him.

331
01:02:08,391 --> 01:02:09,915
He's not here.

332
01:02:10,093 --> 01:02:12,755
Marwin, did you take Toi anywhere?

333
01:02:12,929 --> 01:02:14,191
Nope.

334
01:02:14,363 --> 01:02:16,388
Where did you take him?

335
01:02:16,566 --> 01:02:19,364
Me? Don't be stupid!

336
01:02:19,535 --> 01:02:22,402
I can't find him anywhere.

337
01:02:22,572 --> 01:02:24,199
That's my fault?

338
01:02:24,373 --> 01:02:25,362
You talked to him.

339
01:02:25,541 --> 01:02:28,806
A little bit, last night.

340
01:02:28,978 --> 01:02:32,436
Think about how he feels.

341
01:02:32,615 --> 01:02:34,082
What did he say?

342
01:02:34,250 --> 01:02:36,718
That you want him
to be a monk.

343
01:02:36,886 --> 01:02:40,344
He doesn't want to.
I'm against it, too.

344
01:02:40,523 --> 01:02:44,619
So you put ideas in his head!

345
01:02:44,794 --> 01:02:48,252
You're as bad as my daughter!

346
01:02:49,699 --> 01:02:52,998
Are all whores like that?

347
01:02:53,169 --> 01:02:54,568
You bitch!

348
01:02:55,638 --> 01:02:56,570
It's my fault?

349
01:02:56,739 --> 01:02:58,400
How would I know?!

350
01:02:58,808 --> 01:03:00,503
You're dumping the kid?

351
01:03:01,244 --> 01:03:03,644
I can see him at the temple.

352
01:03:03,880 --> 01:03:08,783
He should be with his mother.
He hasn't got a dad.

353
01:03:08,951 --> 01:03:10,418
Why did you even have him?

354
01:03:10,586 --> 01:03:13,180
I wouldn't have...

355
01:03:13,356 --> 01:03:15,187
...if I'd only known!

356
01:03:15,391 --> 01:03:16,153
You're crazy!

357
01:03:17,460 --> 01:03:20,190
What did you just say?!

358
01:03:20,797 --> 01:03:23,925
Don't say that!

359
01:03:25,301 --> 01:03:27,098
You mustn't say that!

360
01:03:27,436 --> 01:03:29,131
You have no right!

361
01:03:29,572 --> 01:03:30,869
Get out!

362
01:03:31,174 --> 01:03:34,200
You can't say things like that!

363
01:03:35,344 --> 01:03:38,507
Now look what you've done!

364
01:03:39,115 --> 01:03:41,515
<i>Amari, what's going on?!</i>

365
01:03:41,684 --> 01:03:43,276
Stop, stop.

366
01:03:43,753 --> 01:03:44,651
Stop!

367
01:03:46,255 --> 01:03:47,722
<i>What did you hit me for?!</i>

368
01:03:49,992 --> 01:03:51,550
He's your son!

369
01:03:52,028 --> 01:03:53,052
You're nuts!

370
01:03:53,296 --> 01:03:54,593
<i>What happened?!</i>

371
01:03:56,833 --> 01:03:58,323
I didn't take him!

372
01:03:58,534 --> 01:03:59,831
<i>What happened?</i>

373
01:04:00,236 --> 01:04:01,498
<i>Stop it!</i>

374
01:04:01,871 --> 01:04:03,338
<i>What's wrong?</i>

375
01:04:09,011 --> 01:04:09,909
Amari?

376
01:04:10,079 --> 01:04:11,512
Where's Toi?!

377
01:04:12,381 --> 01:04:14,281
<i>Toi? What about Toi?</i>

378
01:04:15,418 --> 01:04:16,282
Was it you?

379
01:04:16,452 --> 01:04:19,182
<i>Toi's gone? He's gone?</i>

380
01:04:19,355 --> 01:04:20,947
He's gone!

381
01:04:21,457 --> 01:04:22,515
Give him back!

382
01:04:26,462 --> 01:04:28,089
Bring Toi back!

383
01:04:28,264 --> 01:04:30,255
I didn't take him!

384
01:04:31,467 --> 01:04:32,525
Where is he?!

385
01:04:35,171 --> 01:04:37,139
Don't blame it on her!

386
01:04:37,306 --> 01:04:42,175
Her country's beautiful and rich.
She wouldn't understand.

387
01:04:42,879 --> 01:04:43,868
Saiko!

388
01:04:45,715 --> 01:04:47,774
Have you seen my son?

389
01:04:48,351 --> 01:04:49,750
Have you?

390
01:04:50,453 --> 01:04:52,751
He's been gone all day.

391
01:04:52,922 --> 01:04:54,446
Did you hide him?

392
01:04:55,691 --> 01:04:57,056
Where's Toi?

393
01:04:57,226 --> 01:04:58,488
<i>I don't know!</i>

394
01:04:59,161 --> 01:05:01,391
Why won't anyone tell me?!

395
01:05:04,533 --> 01:05:05,363
Where's Toi?

396
01:05:05,668 --> 01:05:10,367
<i>I don't know! Why is everyone</i>
<i>shouting at me?!</i>

397
01:05:12,775 --> 01:05:16,836
You have to understand.
I want him to be a monk!

398
01:05:17,013 --> 01:05:21,382
Your country
is beautiful and modern.

399
01:05:22,685 --> 01:05:26,382
But I want him to be
rich in spirit!

400
01:05:26,956 --> 01:05:28,116
Not materialistic.

401
01:05:28,391 --> 01:05:33,090
<i>I don't understand!</i>
<i>Thai, French, I don't understand!</i>

402
01:05:33,629 --> 01:05:36,962
<i>Tell me why</i>
<i>everyone's hitting me!</i>

403
01:05:41,237 --> 01:05:44,365
She doesn't understand people.

404
01:05:44,573 --> 01:05:47,133
She's only rich in things.

405
01:05:50,746 --> 01:05:55,547
<i>What's wrong with you?</i>
<i>Have you gone crazy?!</i>

406
01:06:03,426 --> 01:06:05,394
<i>I'm not afraid of you!</i>

407
01:06:05,628 --> 01:06:06,754
Enough!

408
01:06:09,031 --> 01:06:09,998
<i>Ow!</i>

409
01:06:13,769 --> 01:06:14,997
<i>That hurt!</i>

410
01:06:17,039 --> 01:06:19,599
That's enough.

411
01:06:21,510 --> 01:06:22,442
<i>Move.</i>

412
01:06:25,781 --> 01:06:27,646
<i>So Toi's missing?</i>

413
01:06:27,817 --> 01:06:30,217
<i>Why aren't we looking for him.?</i>

414
01:06:30,720 --> 01:06:34,781
<i>Why are we wasting our time</i>
<i>with this?</i>

415
01:06:37,560 --> 01:06:40,654
<i>Shouldn't we be looking for him?</i>

416
01:06:44,767 --> 01:06:45,825
Toi!

417
01:06:46,802 --> 01:06:47,791
Toi!

418
01:07:02,651 --> 01:07:03,208
Go away!

419
01:07:03,386 --> 01:07:05,286
Let's go look for Toi.

420
01:07:05,454 --> 01:07:07,046
Where did you take him?!

421
01:07:07,223 --> 01:07:11,592
We'll split up and look.
You go that way, I'll go this way.

422
01:07:25,941 --> 01:07:29,570
Yo! Have you seen Toi?

423
01:07:30,112 --> 01:07:32,979
A kid with long hair.

424
01:07:33,416 --> 01:07:35,077
Has he been here?

425
01:07:41,857 --> 01:07:43,290
Thanks, anyway.

426
01:08:06,816 --> 01:08:08,909
He wouldn't come this way.

427
01:08:33,642 --> 01:08:36,008
Come back to Mommy!

428
01:08:50,326 --> 01:08:51,623
Toi!

429
01:15:11,073 --> 01:15:12,233
I understand.

430
01:15:18,447 --> 01:15:19,846
<i>You're here!</i>

431
01:15:33,829 --> 01:15:36,559
I think Daddy loves Mommy.

432
01:15:54,883 --> 01:15:56,544
You want to go to Japan?

433
01:15:58,554 --> 01:15:59,316
You do?

434
01:16:04,192 --> 01:16:06,092
So do I.

435
01:16:08,297 --> 01:16:09,821
To Japan.

436
01:16:12,634 --> 01:16:13,999
We'll go.

437
01:16:16,605 --> 01:16:17,572
Look.

438
01:16:18,040 --> 01:16:21,601
Saiko's shoes are all dirty.

439
01:16:21,977 --> 01:16:24,571
Rinse them.

440
01:16:30,886 --> 01:16:34,083
A little bit louder.

441
01:16:34,489 --> 01:16:36,423
1, 2, 3...

442
01:16:36,592 --> 01:16:41,689
<i>'Sleep, sleep, fast asleep...'</i>

443
01:16:42,464 --> 01:16:45,331
<i>'Fast asleep... '</i>

444
01:16:47,102 --> 01:16:49,263
Again.

445
01:16:49,438 --> 01:16:51,406
1, 2, 3...

446
01:16:52,140 --> 01:16:57,772
<i>'Sleep, sleep, fast asleep...'</i>

447
01:16:58,580 --> 01:17:00,411
<i>'Fast asleep... '</i>

448
01:17:06,421 --> 01:17:12,451
<i>'You're a good boy...'</i>

449
01:17:13,061 --> 01:17:17,293
<i>'Go to sleep....'</i>

450
01:17:19,935 --> 01:17:30,675
<i>'Where did Nurse go?'</i>

451
01:17:32,914 --> 01:17:43,119
<i>'She went from the village,</i>
<i>over the mountain...</i>

452
01:17:46,094 --> 01:17:55,833
<i>'What did she bring</i>
<i>as a souvenir?</i>

453
01:17:58,206 --> 01:18:08,013
<i>'A rattle-drum,</i>
<i>and a set of pipes... '</i>

454
01:19:23,291 --> 01:19:24,622
Kiss me.

455
01:19:40,876 --> 01:19:42,070
By your leave...

456
01:25:59,187 --> 01:26:00,211
'Water'?

457
01:26:01,289 --> 01:26:02,415
'La pluie'.

458
01:26:05,360 --> 01:26:06,759
Not 'puryui'.

459
01:26:11,299 --> 01:26:12,288
'Pluie'.

460
01:26:23,111 --> 01:26:24,738
'Eau'.

461
01:26:30,185 --> 01:26:31,083
<i>'Water'.</i>

462
01:26:35,323 --> 01:26:36,620
'Nam'.

463
01:26:37,125 --> 01:26:38,854
'Eau'.

464
01:26:39,027 --> 01:26:39,994
'Mizu'.

465
01:26:45,300 --> 01:26:46,232
'Kanpai'.

466
01:26:47,168 --> 01:26:49,363
<i>You know</i>
<i>'cheers' in Japanese?</i>

467
01:26:49,737 --> 01:26:50,635
What?

468
01:26:51,372 --> 01:26:52,464
'Sant�'.

469
01:26:52,674 --> 01:26:53,902
In Thai?

470
01:26:54,409 --> 01:26:55,535
'Chai yo'.

471
01:27:02,417 --> 01:27:03,543
All these words!

472
01:27:03,718 --> 01:27:05,686
Yeah.

473
01:27:40,688 --> 01:27:41,655
What's 'tasty'?

474
01:27:41,823 --> 01:27:42,949
'Aloi'.

475
01:27:43,758 --> 01:27:45,726
'Aloi' means 'oishii'?

476
01:27:48,496 --> 01:27:49,656
In French?

477
01:27:50,999 --> 01:27:52,227
'C'est bon! '

478
01:28:10,218 --> 01:28:12,652
What's 'thank you'
in Japanese?

479
01:28:12,820 --> 01:28:13,787
In French?

480
01:28:14,055 --> 01:28:15,147
'Merci'.

481
01:28:20,228 --> 01:28:22,753
'Arigato', 'khoop khun kha',
'Thank you'.

482
01:28:50,291 --> 01:28:51,986
'Love' in Thai is 'rak'.

483
01:29:02,103 --> 01:29:04,936
'Rak', 'amour', 'ai'.

