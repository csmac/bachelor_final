1
00:00:25,141 --> 00:00:27,132
Laborers?

2
00:00:42,992 --> 00:00:45,620
A laborer?
You want laborers?

3
00:00:46,629 --> 00:00:47,425
No

4
00:01:21,965 --> 00:01:25,401
- You want laborers, do you?
- How many?

5
00:01:30,807 --> 00:01:32,069
Take two

6
00:03:03,066 --> 00:03:05,057
- Hello, mister.
- Hello

7
00:03:05,301 --> 00:03:06,529
How are you doing?

8
00:03:07,136 --> 00:03:08,501
- What are you doing?
- Playing cars

9
00:04:02,992 --> 00:04:05,552
Know how much a meal costs?

10
00:04:09,098 --> 00:04:10,156
Get to the point

11
00:04:15,338 --> 00:04:17,431
100,000 tomans is nothing!

12
00:04:19,475 --> 00:04:21,443
How will we pay it back?

13
00:04:26,416 --> 00:04:28,680
Right, what are they asking for?

14
00:04:29,986 --> 00:04:31,715
Where can we find title-deeds?

15
00:04:35,191 --> 00:04:37,591
Don't hang up!
Do you want to make a call?

16
00:04:37,794 --> 00:04:38,590
What?

17
00:04:38,795 --> 00:04:40,990
- Do you want to use the phone?
- No

18
00:04:41,698 --> 00:04:44,166
Yes, I'm listening

19
00:05:32,348 --> 00:05:33,212
Where?

20
00:05:33,850 --> 00:05:37,047
Outside the museum?
When?

21
00:05:39,389 --> 00:05:40,913
All right

22
00:05:41,657 --> 00:05:42,988
Good-bye

23
00:05:53,102 --> 00:05:55,297
- I'll drop you off.
- No, I'm working

24
00:05:55,505 --> 00:05:57,200
- I'm going that way.
- I'm working

25
00:05:57,407 --> 00:05:59,671
- Don't act so proud...
- I said I'm working!

26
00:06:18,361 --> 00:06:21,990
Come here.
Just a minute, please

27
00:06:33,476 --> 00:06:36,206
Hello. How are you?

28
00:06:38,181 --> 00:06:41,639
If you have money problems,
I can help you

29
00:06:42,085 --> 00:06:42,949
No

30
00:06:46,389 --> 00:06:48,118
- You don't have money problems?
- No

31
00:06:50,693 --> 00:06:51,682
I can help you

32
00:06:51,894 --> 00:06:53,691
Clear off
or I'll smash your face in!

33
00:06:54,097 --> 00:06:55,291
Get lost!

34
00:08:36,966 --> 00:08:37,955
Hello

35
00:08:38,534 --> 00:08:40,229
- How are you?
- Fine, thanks

36
00:08:40,436 --> 00:08:44,236
What is it?
What are you collecting?

37
00:08:44,640 --> 00:08:46,403
Plastic bags

38
00:08:47,043 --> 00:08:48,032
What for?

39
00:08:48,744 --> 00:08:55,240
I pick them up and liquidate them
near the factory

40
00:08:55,451 --> 00:08:59,615
What happened to your finger?

41
00:09:01,157 --> 00:09:03,091
I cut myself this morning

42
00:09:03,392 --> 00:09:04,882
Here?

43
00:09:06,128 --> 00:09:08,858
Nice shirt!
Where did you get it?

44
00:09:09,232 --> 00:09:11,700
I found it over there
last week

45
00:09:11,901 --> 00:09:15,302
Nice color, it suits you!

46
00:09:15,638 --> 00:09:17,265
Know what it says on it?

47
00:09:28,050 --> 00:09:29,483
Where are you from?

48
00:09:31,520 --> 00:09:35,251
"Pissy, pissy...
Your dick's all sticky"

49
00:09:37,260 --> 00:09:39,854
- Don't mind them.
- They're playing.

50
00:09:40,896 --> 00:09:44,798
- Where are you from?
- Near Lorestan

51
00:09:45,468 --> 00:09:47,561
Not a local then,
from Lorestan

52
00:09:47,803 --> 00:09:51,466
- Are you from Lorestan too?
- You could say that

53
00:09:52,708 --> 00:09:54,232
How much do you make a day?

54
00:09:55,311 --> 00:09:59,008
2OO, 3OO, 4OO...

55
00:10:00,149 --> 00:10:04,210
7OO, 6OO.

56
00:10:04,754 --> 00:10:06,016
What do you do with the money?

57
00:10:07,323 --> 00:10:08,847
I send it to my family

58
00:10:09,325 --> 00:10:11,156
You want to get married?

59
00:10:12,161 --> 00:10:13,890
No, I'm helping my family

60
00:10:14,764 --> 00:10:16,231
You're helping them?

61
00:10:16,932 --> 00:10:20,231
So, would you do something
if I asked you?

62
00:10:21,170 --> 00:10:21,932
A job?

63
00:10:22,171 --> 00:10:24,969
A well-paid job

64
00:10:25,775 --> 00:10:28,073
No, I don't know how...

65
00:10:28,277 --> 00:10:31,041
I just collect plastic bags
and sell them

66
00:10:31,247 --> 00:10:32,805
Why?
Where are you going?

67
00:10:34,750 --> 00:10:39,517
A TASTE OF CHERRY

68
00:10:40,356 --> 00:10:44,452
A film by
Abbas KIAROSTAMI

69
00:10:45,428 --> 00:10:49,660
with
Homayoun ERSHADI

70
00:10:50,733 --> 00:10:53,327
Abdolhosein BAGHERI
Afshin KHORSHID BAKHTARI

71
00:10:53,569 --> 00:10:56,060
Safar Ali MORADI

72
00:10:57,106 --> 00:10:59,506
Mir Hosein NOURI
Ahmad ANSARI

73
00:10:59,709 --> 00:11:03,076
Hamid MASOUMI
Elham IMANI

74
00:11:04,013 --> 00:11:06,880
Assistant cameraman
Farshad BASHIR ZADEH

75
00:11:07,083 --> 00:11:09,881
Sound assistant
Sassan BAGHERPOUR

76
00:11:11,253 --> 00:11:13,847
Cameraman
Alireza ANSARIAN

77
00:11:14,056 --> 00:11:16,524
Titles
Mehdi SAMAKAR

78
00:11:17,426 --> 00:11:19,792
1st assistant director
Hassan YEKTAPANAH

79
00:11:20,029 --> 00:11:22,862
2nd assistant director
Bahman KIAROSTAMI

80
00:11:23,766 --> 00:11:28,203
Editor
Abbas KIAROSTAMI

81
00:11:29,305 --> 00:11:33,969
Sound
Jahangir MIRSHEKARI

82
00:11:34,910 --> 00:11:39,506
Mix
Mohamadreza DELPAK

83
00:11:40,583 --> 00:11:45,680
Photography
Homayoun PAYVAR

84
00:11:46,889 --> 00:11:52,327
Written, produced and directed by
Abbas KIAROSTAMI

85
00:12:07,042 --> 00:12:09,442
- Where are you going?
- To the barracks

86
00:12:09,645 --> 00:12:11,203
Come on, get in

87
00:12:11,781 --> 00:12:12,907
Hello

88
00:12:13,315 --> 00:12:14,282
Hello

89
00:12:17,887 --> 00:12:21,516
Are you well?
Where are you going?

90
00:12:22,258 --> 00:12:25,227
Down there, not very far

91
00:12:29,198 --> 00:12:30,825
The barracks down there?

92
00:12:31,901 --> 00:12:33,732
- Near the reservoir?
- Yes

93
00:12:37,907 --> 00:12:39,397
You look tired

94
00:12:40,242 --> 00:12:41,869
I guess I am

95
00:12:42,077 --> 00:12:44,739
- Are you worn out?
- Yes

96
00:12:45,214 --> 00:12:47,273
A soldier is never tired!

97
00:12:47,483 --> 00:12:51,419
What do you expect?
I've walked from Darabad

98
00:12:52,788 --> 00:12:54,551
- From Darabad?
- Yes

99
00:12:55,925 --> 00:12:57,688
But today's a holiday

100
00:12:58,527 --> 00:13:02,759
I'm on duty tonight
until six in the morning

101
00:13:05,100 --> 00:13:06,465
Then what are you doing?

102
00:13:06,669 --> 00:13:08,728
- Where will you go?
- To my aunt's

103
00:13:11,173 --> 00:13:16,736
She's dead, I stay
with her husband, a janitor

104
00:13:20,850 --> 00:13:22,112
How long ago were you drafted?

105
00:13:29,825 --> 00:13:30,655
Two months

106
00:13:30,860 --> 00:13:33,090
- Still in training?
- Yes

107
00:13:36,165 --> 00:13:37,530
Where are you from?

108
00:13:39,101 --> 00:13:40,295
Kurdistan

109
00:13:41,070 --> 00:13:43,300
- And you're in the army here?
- Yes

110
00:13:49,411 --> 00:13:51,345
Will you stay or go back after?

111
00:13:51,547 --> 00:13:53,344
- No, I'll go back.
- Sorry?

112
00:13:53,549 --> 00:13:55,915
- I'll go back home.
- Back to Kurdistan?

113
00:13:56,352 --> 00:13:57,614
- Yes.
- Good

114
00:14:03,225 --> 00:14:04,715
What did you do
in Kurdistan?

115
00:14:07,596 --> 00:14:08,995
What did you do there?

116
00:14:10,599 --> 00:14:12,430
I was a farmer

117
00:14:16,305 --> 00:14:17,499
A farmer?

118
00:14:20,976 --> 00:14:22,603
Have you studied?

119
00:14:23,979 --> 00:14:27,881
- Were you at school?
- Not for long

120
00:14:28,450 --> 00:14:29,474
Well

121
00:14:32,488 --> 00:14:33,819
You gave up?

122
00:14:37,693 --> 00:14:38,751
Why?

123
00:14:44,967 --> 00:14:46,525
You know how it is

124
00:14:55,511 --> 00:14:57,206
How many people in your family?

125
00:14:58,847 --> 00:14:59,939
Nine

126
00:15:00,149 --> 00:15:01,707
- Nine?
- Yes

127
00:15:02,885 --> 00:15:05,581
- They all work?
- Yes, all of them

128
00:15:08,123 --> 00:15:10,489
- Do you know anyone in Tehran?
- Yes

129
00:15:10,826 --> 00:15:12,020
- Relatives?
- Yes

130
00:15:12,294 --> 00:15:17,129
I have two brothers in Tehran

131
00:15:18,634 --> 00:15:21,501
Why don't you stay with them?

132
00:15:22,504 --> 00:15:26,201
They're married,
they have small houses, children

133
00:15:26,408 --> 00:15:27,067
I can't stay there

134
00:15:27,276 --> 00:15:30,609
Do you have money?
Soldiers get pay

135
00:15:30,813 --> 00:15:34,146
- Money?
- A little... the pay's not good

136
00:15:36,418 --> 00:15:38,147
Is it enough for you?

137
00:15:38,687 --> 00:15:40,416
Oh no, it's not enough

138
00:15:48,530 --> 00:15:50,122
When do you have to be back by?

139
00:15:50,666 --> 00:15:52,065
Six p.m.

140
00:15:53,902 --> 00:15:55,199
It's five now

141
00:15:57,606 --> 00:16:00,234
You're so fond of the place,
you get there

142
00:16:00,442 --> 00:16:02,433
an hour early?

143
00:16:02,645 --> 00:16:05,409
No, not really... no

144
00:16:07,049 --> 00:16:11,247
We've got an hour to kill.
How about a drive?

145
00:16:11,453 --> 00:16:14,286
I may have
a well-paid job to offer you

146
00:16:14,490 --> 00:16:16,458
to supplement your income

147
00:16:16,659 --> 00:16:18,752
I want to be at the barracks
by six

148
00:16:18,927 --> 00:16:22,124
- What?
- I want to be back by six

149
00:16:22,364 --> 00:16:23,922
I'll drive you back

150
00:16:24,166 --> 00:16:26,600
Don't worry, you won't be late

151
00:16:42,317 --> 00:16:45,684
- Shall we go?
- Okay

152
00:16:46,188 --> 00:16:49,089
I'll get you back by six,
don't worry

153
00:17:04,406 --> 00:17:07,807
You say the barracks
aren't much fun?

154
00:17:10,913 --> 00:17:13,541
I had fun
when I did my military service

155
00:17:14,550 --> 00:17:18,213
It was the best time
of my life

156
00:17:19,154 --> 00:17:21,748
I met my closest friends there

157
00:17:21,957 --> 00:17:25,154
Especially in the first
six months

158
00:17:26,528 --> 00:17:31,227
I remember we used to get up
at four in the morning

159
00:17:32,034 --> 00:17:34,366
After breakfast,
we'd shine our boots and

160
00:17:34,570 --> 00:17:38,734
we'd go out on manoeuvers.
The major would join us

161
00:17:38,941 --> 00:17:43,674
There were 4O to 45 men
in the regiment

162
00:17:44,613 --> 00:17:46,581
The major would start counting

163
00:17:49,118 --> 00:17:52,349
He'd tell us to repeat: One

164
00:17:53,856 --> 00:17:54,982
Two

165
00:17:55,557 --> 00:17:56,489
Three

166
00:17:57,593 --> 00:17:58,423
Four

167
00:17:58,627 --> 00:18:01,027
Do you count too?
Is it the same?

168
00:18:04,133 --> 00:18:05,225
Well?

169
00:18:05,801 --> 00:18:06,859
How do you count?

170
00:18:10,472 --> 00:18:11,700
Are you shy?

171
00:18:12,207 --> 00:18:13,674
- Yes.
- What?

172
00:18:18,113 --> 00:18:21,207
Why? Don't you count with
your friends at the barracks?

173
00:18:23,185 --> 00:18:24,174
Yes

174
00:18:28,857 --> 00:18:30,757
So you don't think of me
as a friend

175
00:18:30,959 --> 00:18:32,688
Of course I do, we're friends

176
00:18:37,800 --> 00:18:40,325
No, you act as if
we don't know each other

177
00:18:40,803 --> 00:18:43,328
- No, it's not that.
- Well, then?

178
00:18:43,572 --> 00:18:45,096
It's not that

179
00:18:48,510 --> 00:18:53,038
Listen, this is how we did it:
One

180
00:18:53,682 --> 00:18:55,172
Two

181
00:18:55,551 --> 00:18:56,779
Three

182
00:18:56,985 --> 00:18:58,213
Four

183
00:18:58,420 --> 00:19:01,184
One, two, three, four

184
00:19:01,857 --> 00:19:03,484
One, two, three, four

185
00:19:03,859 --> 00:19:06,191
- Go on, repeat after me: One
- One

186
00:19:06,695 --> 00:19:09,027
two, three

187
00:19:09,231 --> 00:19:11,096
Is that how you do it
at the barracks?!

188
00:19:12,734 --> 00:19:15,532
- Soldiers don't count like that!
- I can't help it

189
00:19:32,688 --> 00:19:36,852
You're taking me a long way!
I need to know what you want

190
00:19:37,960 --> 00:19:39,518
- What you have to do?
- Yes

191
00:19:44,566 --> 00:19:46,659
You know, son

192
00:19:47,569 --> 00:19:49,161
If I were in your shoes,
I wouldn't ask

193
00:19:49,371 --> 00:19:53,000
what the job is,
but rather how good the pay is

194
00:19:53,909 --> 00:19:55,536
For someone like you

195
00:19:55,744 --> 00:19:58,611
the pay's what matters.
A job is a job

196
00:19:58,814 --> 00:20:02,875
It's easy
if you look at it that way

197
00:20:03,785 --> 00:20:06,879
This isn't an ordinary job
but neither is the pay

198
00:20:07,089 --> 00:20:10,388
In ten minutes,
you can earn six months' pay

199
00:20:11,960 --> 00:20:13,621
What's the job?

200
00:20:14,196 --> 00:20:18,132
Forget the job,
it's the pay that matters

201
00:20:18,367 --> 00:20:20,130
You have to tell me what it is!

202
00:20:23,372 --> 00:20:28,469
Listen, when you ask a laborer
to dig foundations

203
00:20:28,677 --> 00:20:33,239
does he ask
if they're for a hospital,

204
00:20:33,448 --> 00:20:36,508
a lunatic asylum or a mosque

205
00:20:36,718 --> 00:20:40,677
or a school? He does his job
and gets his pay

206
00:20:42,491 --> 00:20:44,425
Ever been a laborer?

207
00:20:44,626 --> 00:20:45,593
Yes

208
00:20:45,794 --> 00:20:47,989
Were you told
what you were digging for?

209
00:20:48,897 --> 00:20:51,491
- No, no one told me.
- Why ask me then?

210
00:20:52,801 --> 00:20:54,530
Help me out, I'll pay you

211
00:20:54,770 --> 00:20:58,638
It's not hard, you'll see

212
00:20:59,875 --> 00:21:00,933
Okay?

213
00:21:43,051 --> 00:21:44,450
Where are you going?

214
00:21:45,887 --> 00:21:48,321
I have to be at the barracks
by six

215
00:21:48,757 --> 00:21:51,123
I'll drive you back,
wait a while

216
00:21:56,531 --> 00:21:57,793
I want to get out!

217
00:21:58,834 --> 00:22:00,358
I want to get out!

218
00:22:01,970 --> 00:22:04,302
- Get out?
- Yes

219
00:22:04,506 --> 00:22:05,768
A call of nature?

220
00:22:08,243 --> 00:22:09,767
No, I was thinking
I could go

221
00:22:10,012 --> 00:22:13,106
Why did I ask you to come?
You think I'm nuts?

222
00:22:13,615 --> 00:22:15,207
Do I look nuts?

223
00:22:17,552 --> 00:22:19,747
I was thinking
I ought to get back

224
00:22:20,789 --> 00:22:23,917
I promised I'd get you back
by six and I will

225
00:22:25,093 --> 00:22:26,287
Wait a while

226
00:23:13,508 --> 00:23:14,873
Come on, get out a minute

227
00:23:15,410 --> 00:23:17,742
- I need to get back.
- Get out, I'll explain

228
00:23:32,094 --> 00:23:33,493
You see that hole

229
00:23:35,197 --> 00:23:36,323
That hole there

230
00:23:40,769 --> 00:23:42,168
Now listen carefully

231
00:23:42,370 --> 00:23:46,534
At six in the morning,
come here and call me twice:

232
00:23:46,741 --> 00:23:48,333
"Mr. Badii! Mr. Badii!"

233
00:23:50,378 --> 00:23:54,246
If I reply, take my hand
to help me out of there

234
00:23:54,850 --> 00:23:57,478
There's 2OO, OOO tomans
in the car

235
00:23:57,819 --> 00:23:59,411
Take it and go

236
00:24:02,858 --> 00:24:04,257
If I don't reply,

237
00:24:05,393 --> 00:24:08,556
throw in 2O spadefuls of earth
on top of me

238
00:24:08,964 --> 00:24:10,932
- Then take the money and go.
- Take me back

239
00:24:11,166 --> 00:24:13,600
Keep it. Take me back!

240
00:24:13,835 --> 00:24:15,302
I don't want to cause trouble

241
00:24:15,537 --> 00:24:19,906
- Trouble? What trouble?
- I don't want it. It's late

242
00:24:20,442 --> 00:24:25,277
I want to go back. I don't want
to be involved in all this

243
00:24:27,516 --> 00:24:31,714
You can't throw 2O spadefuls
of earth in that hole?

244
00:24:34,122 --> 00:24:36,022
Right now,
I really need you

245
00:24:36,224 --> 00:24:38,658
otherwise I wouldn't have
begged you

246
00:24:39,961 --> 00:24:42,953
You want me to beg you?
Is that what you want?

247
00:24:43,398 --> 00:24:45,332
No, why would you beg me?

248
00:24:48,803 --> 00:24:50,464
What does "need" mean?

249
00:24:50,672 --> 00:24:54,802
What does "help" mean?
"Help" isn't necessarily paid for,

250
00:24:55,110 --> 00:24:59,069
but I'll give you money,
I'll help you

251
00:24:59,881 --> 00:25:03,146
Don't you need money?
Are you sure?

252
00:25:03,418 --> 00:25:05,045
Of course I do

253
00:25:05,253 --> 00:25:08,313
Well?
Isn't 2OO, OOO enough?

254
00:25:08,523 --> 00:25:11,549
It's not that.
I can't do this for you

255
00:25:13,128 --> 00:25:14,493
It's not a matter of money

256
00:25:18,600 --> 00:25:20,568
You can't throw earth in a hole?

257
00:25:21,136 --> 00:25:24,970
Yes, but not on top of someone,
not on someone's head

258
00:25:30,111 --> 00:25:32,579
You can't throw earth
on someone!

259
00:25:33,782 --> 00:25:36,615
If he was alive,
he'd stand up to respond.

260
00:25:38,053 --> 00:25:41,113
Look, I really need your help.
I'm not mad!

261
00:25:53,835 --> 00:25:58,431
When you throw the earth in,
the man won't be alive,

262
00:25:58,640 --> 00:26:00,198
or he wouldn't be in that hole

263
00:26:01,576 --> 00:26:02,941
Do you understand now?

264
00:26:05,513 --> 00:26:06,741
Do you understand me?

265
00:26:10,118 --> 00:26:10,709
Yes

266
00:26:10,919 --> 00:26:14,650
So if you understand,
get out, come and see

267
00:26:16,291 --> 00:26:20,250
When you come back here
at six tomorrow morning

268
00:26:21,796 --> 00:26:23,286
Is your barracks over there?

269
00:26:23,531 --> 00:26:28,468
From there to here
will only take you 2O minutes

270
00:26:29,170 --> 00:26:35,302
Call my name twice:
"Mr. Badii, Mr. Badii!"

271
00:26:36,144 --> 00:26:38,977
If I reply, you'll take my hand
to help me climb out

272
00:26:40,148 --> 00:26:41,581
You'll get paid

273
00:26:42,217 --> 00:26:44,117
But if I don't reply...

274
00:26:48,390 --> 00:26:49,721
Get out

275
00:27:00,969 --> 00:27:02,300
Get out!

276
00:27:03,471 --> 00:27:05,405
Get out, come and see!

277
00:27:06,675 --> 00:27:10,543
It's God's will I should need you

278
00:27:10,745 --> 00:27:13,646
and you don't want to help!

279
00:27:20,789 --> 00:27:22,347
You don't want to?!

280
00:27:23,525 --> 00:27:26,551
You won't bury me alive!

281
00:27:26,761 --> 00:27:29,355
Right now,
they're burying dozens of people

282
00:27:29,564 --> 00:27:31,896
As we speak, dozens
of dead people are being buried

283
00:27:33,101 --> 00:27:36,229
- You've never seen a gravedigger?
- No, never

284
00:27:37,739 --> 00:27:40,264
I'm not a gravedigger

285
00:27:41,876 --> 00:27:43,104
I don't bury people

286
00:27:45,814 --> 00:27:47,611
I know you're not a gravedigger

287
00:27:48,883 --> 00:27:51,579
If I'd wanted one,
I'd have fetched one

288
00:27:52,887 --> 00:27:54,514
It's you I need

289
00:27:55,657 --> 00:27:57,249
You're like my son

290
00:27:58,927 --> 00:28:00,121
Help me

291
00:28:04,966 --> 00:28:06,365
I have to beg?

292
00:28:07,135 --> 00:28:08,602
Is that what you want?

293
00:28:08,837 --> 00:28:11,101
No, what's the use of begging?

294
00:28:24,786 --> 00:28:27,448
Get out then,
come and have a look

295
00:28:29,491 --> 00:28:30,856
Maybe you'll feel up to it

296
00:28:33,395 --> 00:28:34,987
2O spadefuls of earth

297
00:28:35,764 --> 00:28:38,460
Just 2O,
Each paid 10,000 tomans

298
00:28:41,302 --> 00:28:42,462
Where are you from?

299
00:28:44,139 --> 00:28:46,801
- Kurdistan.
- You're a Kurd!

300
00:28:47,442 --> 00:28:49,410
A Kurd has to be brave

301
00:28:56,017 --> 00:28:59,145
You people have fought
so many wars,

302
00:28:59,487 --> 00:29:04,390
known such suffering...
Your villages have been decimated

303
00:29:07,262 --> 00:29:09,321
You've probably used a gun,
right?

304
00:29:10,932 --> 00:29:14,163
Know what a gun is?
Why you're given one?

305
00:29:15,503 --> 00:29:18,131
So you can kill when you need to

306
00:29:21,209 --> 00:29:23,575
I don't want to give you
a gun to kill me

307
00:29:24,879 --> 00:29:27,439
I'm giving you a spade, a spade

308
00:29:28,349 --> 00:29:29,714
You're a farmer, right?

309
00:29:48,736 --> 00:29:50,533
Just pretend you're farming

310
00:29:53,241 --> 00:29:56,802
and that I'm manure to be spread
at the foot of a tree

311
00:29:57,979 --> 00:29:59,173
Is that hard?

312
00:30:11,926 --> 00:30:14,292
You're destined to use a gun,
not a spade!

313
00:30:14,963 --> 00:30:18,160
You can't even use a spade!

314
00:36:11,018 --> 00:36:12,349
Thank you!

315
00:36:15,056 --> 00:36:16,717
Thank you very much!

316
00:37:29,196 --> 00:37:31,255
- Hello.
- Hello

317
00:37:31,632 --> 00:37:33,532
- How are you?
- Well, thanks

318
00:37:34,635 --> 00:37:37,069
- What's that machine for?
- Making cement

319
00:37:38,272 --> 00:37:41,241
Really?
Why isn't it working?

320
00:37:42,009 --> 00:37:44,807
This place is closed now.
The workers aren't here

321
00:37:47,949 --> 00:37:49,416
So what are you doing here?

322
00:37:49,617 --> 00:37:51,209
I'm the security guard

323
00:37:52,486 --> 00:37:55,683
- Are you alone?
- Yes

324
00:37:57,224 --> 00:37:59,818
All alone... Do you enjoy that?

325
00:38:00,728 --> 00:38:02,218
Come and join me

326
00:38:02,730 --> 00:38:05,096
- I don't want to disturb you.
- Come on up!

327
00:38:18,245 --> 00:38:20,236
- I have to climb this ladder?
- Yes

328
00:38:27,521 --> 00:38:30,012
- How can you climb this?
- I'm used to it

329
00:38:30,825 --> 00:38:32,486
- You're used to it.
- Yes

330
00:38:34,428 --> 00:38:36,487
- Hello.
- Hello

331
00:38:45,439 --> 00:38:47,168
What a nice place

332
00:38:48,676 --> 00:38:51,702
Nice? It's nothing
but earth and dust

333
00:38:53,381 --> 00:38:54,746
You don't think earth's nice?

334
00:38:56,450 --> 00:38:58,281
Earth gives us
all the good things!

335
00:39:00,187 --> 00:39:04,487
So according to you, all good
things return to the earth

336
00:39:05,493 --> 00:39:06,755
I think you're...?

337
00:39:07,561 --> 00:39:09,290
I'm Afghani

338
00:39:10,865 --> 00:39:13,663
- Where from?
- Mazar-e Sharif

339
00:39:15,136 --> 00:39:18,469
That's a strange name.
Whose "Mazar" is it?

340
00:39:19,140 --> 00:39:21,700
It's the tomb of the lmam Ali

341
00:39:21,976 --> 00:39:24,774
It's a place of pilgrimage

342
00:39:25,980 --> 00:39:27,607
Isn't the lmam Ali's tomb
in Najaf?

343
00:39:27,815 --> 00:39:32,582
Yes, but some people think
it's in Mazar-e Sharif

344
00:39:34,055 --> 00:39:35,079
Strange

345
00:39:36,524 --> 00:39:37,786
What are you doing?

346
00:39:37,992 --> 00:39:42,429
I'm making myself an omelette.
It isn't worthy of you

347
00:39:42,797 --> 00:39:44,924
I'll make you some tea

348
00:39:45,166 --> 00:39:46,895
Thank you very much!

349
00:40:35,416 --> 00:40:37,441
Don't you get bored here alone?

350
00:40:37,818 --> 00:40:41,049
I'm used to it.
I'm used to loneliness too

351
00:40:51,065 --> 00:40:53,124
Do you know that man?

352
00:40:53,734 --> 00:40:56,635
Yes, he's a fellow countryman

353
00:40:57,171 --> 00:40:57,933
He's Afghani?

354
00:40:58,205 --> 00:41:03,040
Yes, he's a "seminarist".
He's here for his holidays

355
00:41:04,778 --> 00:41:07,872
A "seminarist"?
He should be in a seminary

356
00:41:08,082 --> 00:41:10,175
He felt lonely,
he came to see me

357
00:41:10,384 --> 00:41:14,445
He's been here
for two or three nights

358
00:41:16,557 --> 00:41:17,785
Is he staying for good?

359
00:41:18,592 --> 00:41:21,390
Of course not... just three nights

360
00:41:24,632 --> 00:41:26,827
I didn't catch what you said

361
00:41:27,535 --> 00:41:30,527
- Is he staying tonight?
- Yes, he's staying

362
00:41:32,206 --> 00:41:34,140
You're not entirely alone then

363
00:41:35,075 --> 00:41:37,669
No, not entirely alone

364
00:41:45,519 --> 00:41:47,885
- Is he Afghani too?
- Yes

365
00:41:48,088 --> 00:41:49,385
So many Afghanis around here!

366
00:41:49,590 --> 00:41:54,357
There've been a lot
since the war in Afghanistan

367
00:41:54,562 --> 00:41:58,293
Between 2 and 3 million Afghanis
live in Iran

368
00:41:59,733 --> 00:42:02,531
With the war here,
why didn't they go back?

369
00:42:03,337 --> 00:42:07,034
The war against Iraq
only concerned the Iranians

370
00:42:07,274 --> 00:42:09,606
But the war at home concerned us

371
00:42:10,277 --> 00:42:12,336
And our war didn't concern you?

372
00:42:13,147 --> 00:42:15,445
You could say your war
troubled us

373
00:42:15,649 --> 00:42:20,643
but the Afghanistan War
was harder... more painful for us

374
00:42:28,963 --> 00:42:30,089
Tell me

375
00:42:31,799 --> 00:42:35,235
today's a holiday,
so why are you here alone?

376
00:42:35,536 --> 00:42:37,470
You feel sad, so do I

377
00:42:37,705 --> 00:42:42,142
Come for a drive. We can
get a change of scene, talk

378
00:42:42,343 --> 00:42:44,470
I'm the security guard here,
I'm in charge

379
00:42:44,678 --> 00:42:47,442
- A guard in this place?
- Yes

380
00:42:47,681 --> 00:42:51,412
But who could carry off
a machine that heavy?

381
00:42:51,652 --> 00:42:54,450
It's a holiday.
No one needs guards. Come on

382
00:42:54,788 --> 00:42:58,019
Let's get some fresh air.
Everything's safe

383
00:42:58,325 --> 00:43:00,384
It's my duty.
We all have our responsibilities

384
00:43:00,594 --> 00:43:04,030
- I can't leave my post.
- You really can't?

385
00:43:06,700 --> 00:43:07,860
No, sorry

386
00:43:08,602 --> 00:43:10,331
I thought
we could get some air

387
00:43:17,044 --> 00:43:20,241
I'll go and see your friend,
the seminarist

388
00:43:20,881 --> 00:43:23,611
Maybe we'll go
for a drive together

389
00:43:25,686 --> 00:43:27,654
I've made some tea

390
00:43:27,855 --> 00:43:31,052
Leave it for later.
Good-bye

391
00:43:36,463 --> 00:43:39,455
- Try going the other way.
- The ladder moves!

392
00:43:39,833 --> 00:43:41,630
It's dangerous! Mend it

393
00:43:43,070 --> 00:43:45,800
You can mend it
by wrapping fuse-wire around it

394
00:44:54,842 --> 00:44:55,934
Are you well?

395
00:44:59,646 --> 00:45:01,546
You're Afghani, aren't you?

396
00:45:02,182 --> 00:45:03,114
Yes

397
00:45:03,417 --> 00:45:04,748
What are you doing?

398
00:45:06,153 --> 00:45:08,314
I've got three days' holiday

399
00:45:09,022 --> 00:45:10,819
I was alone, I felt down

400
00:45:11,225 --> 00:45:15,821
My friend Ahmad was alone too
so I came here to visit him

401
00:45:16,296 --> 00:45:19,390
I meant
what are you doing in Iran?

402
00:45:19,633 --> 00:45:23,296
I study at the Tchizar seminary

403
00:45:26,940 --> 00:45:32,776
Aren't there any seminaries
in Afghanistan?

404
00:45:34,248 --> 00:45:37,877
Yes, there are some.
But there was a war on

405
00:45:40,587 --> 00:45:43,055
And my seminary wasn't
that good

406
00:45:43,424 --> 00:45:47,690
so my father told me
to study in Iran or at Najaf

407
00:45:50,564 --> 00:45:52,293
So I came to study in Iran

408
00:45:58,906 --> 00:46:03,809
What about the fees?
Does your father send you money?

409
00:46:04,578 --> 00:46:07,911
My father isn't that well-off, no

410
00:46:08,115 --> 00:46:12,518
The seminary pays me 2,OOO tomans

411
00:46:14,121 --> 00:46:16,419
During the summer, I work

412
00:46:16,623 --> 00:46:19,786
to build up my savings

413
00:46:20,727 --> 00:46:21,853
What kind of work?

414
00:46:23,263 --> 00:46:25,561
I work as a laborer,
simple work

415
00:46:25,766 --> 00:46:29,862
I didn't know a seminarist
could work as a laborer

416
00:46:31,638 --> 00:46:34,266
When you need to, you work

417
00:46:34,475 --> 00:46:36,909
So if a job comes along,
you take it?

418
00:46:37,644 --> 00:46:38,372
Yes

419
00:46:41,248 --> 00:46:45,947
Aren't you wondering
why I've offered you a ride?

420
00:46:46,787 --> 00:46:47,685
Yes

421
00:46:53,060 --> 00:46:57,997
I know that your duty
is to preach and guide people

422
00:46:58,265 --> 00:47:02,565
But you're young, you have time,
you can do that later

423
00:47:04,104 --> 00:47:06,299
It's your hands that I need

424
00:47:07,708 --> 00:47:09,335
I don't need your tongue

425
00:47:09,843 --> 00:47:11,071
or your mind

426
00:47:13,747 --> 00:47:18,775
I'm lucky that those hands
belong to a true believer

427
00:47:19,786 --> 00:47:26,157
With the patience, endurance
and perseverance that you learn

