﻿1
00:00:00,000 --> 00:00:06,000
<i>Watch and Download App at official site FastDrama.Co to support us</i>

2
00:00:06,854 --> 00:00:09,755
What about my daughter-in-law?

3
00:00:10,055 --> 00:00:11,155
The girl...

4
00:00:12,424 --> 00:00:14,055
that Ji Seok is married to is...

5
00:00:14,055 --> 00:00:15,755
I'll contact you when we get back.

6
00:00:16,294 --> 00:00:19,294
Watch your mouth until then.

7
00:00:19,535 --> 00:00:22,434
What about her?

8
00:00:25,765 --> 00:00:27,834
She's my...

9
00:00:35,215 --> 00:00:37,385
(Tae Ran)

10
00:00:37,385 --> 00:00:39,114
Go Ya's wedding went well.

11
00:00:39,315 --> 00:00:40,855
I thought you should at least see some pictures.

12
00:00:43,825 --> 00:00:45,925
What do you think you're doing?

13
00:00:46,554 --> 00:00:49,095
Continue with what you were saying.

14
00:00:49,294 --> 00:00:52,294
What about the girl that Ji Seok is married to?

15
00:00:55,194 --> 00:00:56,834
I know what this is about.

16
00:00:58,105 --> 00:01:00,005
Na Ra put you up to this.

17
00:01:00,304 --> 00:01:03,145
She wanted you to find out about the girl...

18
00:01:03,145 --> 00:01:04,574
that Ji Seok married.

19
00:01:04,574 --> 00:01:06,474
Well, it's not that.

20
00:01:06,474 --> 00:01:09,474
Go tell her.

21
00:01:09,845 --> 00:01:11,744
Tell her that she's a great girl...

22
00:01:12,285 --> 00:01:15,955
and makes Ji Seok very happy,

23
00:01:15,955 --> 00:01:18,655
so you two can stay out of their lives.

24
00:01:19,425 --> 00:01:20,824
- What? - Make sure...

25
00:01:21,125 --> 00:01:24,125
you tell her that.

26
00:01:28,565 --> 00:01:29,735
Sa Ra.

27
00:01:30,664 --> 00:01:31,864
I mean, Ms. Oh!

28
00:01:32,705 --> 00:01:33,905
Sa Ra.

29
00:01:34,435 --> 00:01:35,875
What's going on?

30
00:01:37,005 --> 00:01:38,774
Sa Ra, Ms. Oh!

31
00:01:40,474 --> 00:01:42,244
This is driving me crazy.

32
00:01:42,244 --> 00:01:45,414
She's my sister-in-law and my in-law. My gosh.

33
00:01:51,084 --> 00:01:52,584
- That way. - I see.

34
00:01:58,024 --> 00:01:59,765
Ji Seok, are you okay?

35
00:01:59,864 --> 00:02:01,095
I'm fine.

36
00:02:01,694 --> 00:02:03,134
You're sweating.

37
00:02:03,765 --> 00:02:06,274
- Does your stomach hurt? - Yes, a bit.

38
00:02:07,005 --> 00:02:08,935
This isn't good. Let's go see a doctor.

39
00:02:08,935 --> 00:02:11,005
I'm fine. I can take it.

40
00:02:11,805 --> 00:02:13,074
Let's go...

41
00:02:14,414 --> 00:02:17,085
Ji Seok. My gosh.

42
00:02:17,085 --> 00:02:18,785
Does it hurt a lot?

43
00:02:19,115 --> 00:02:21,454
What do I do? Ji Seok.

44
00:02:23,625 --> 00:02:26,655
Ji Seok got married today.

45
00:02:27,625 --> 00:02:29,224
Seriously.

46
00:02:29,924 --> 00:02:32,794
I can't believe I didn't know about this.

47
00:02:33,435 --> 00:02:34,835
Darn it.

48
00:02:39,405 --> 00:02:42,905
Goodness, why does she keep calling me?

49
00:02:45,375 --> 00:02:46,745
Yes, what?

50
00:02:46,745 --> 00:02:49,414
I called you so many times. Why didn't you pick up?

51
00:02:49,414 --> 00:02:51,585
I didn't hear it. What is it?

52
00:02:51,615 --> 00:02:53,185
Can you come out now?

53
00:02:57,185 --> 00:02:58,484
Goodness.

54
00:02:59,755 --> 00:03:02,995
The garden is a huge mess.

55
00:03:03,894 --> 00:03:07,794
It's just like how that guy tore my heart out.

56
00:03:09,495 --> 00:03:12,764
Ji Seok, why does it have to be you?

57
00:03:13,234 --> 00:03:16,375
Darn him. He must be crazy.

58
00:03:16,674 --> 00:03:18,674
I can't believe that guy.

59
00:03:45,835 --> 00:03:48,574
- What? - Are you still angry...

60
00:03:48,574 --> 00:03:51,375
- that you weren't invited? - What do you want?

61
00:03:51,845 --> 00:03:54,574
- I'm not in the mood to talk. - Did you see the pictures?

62
00:03:55,845 --> 00:03:59,685
The wedding went very well. I hope you feel better.

63
00:03:59,685 --> 00:04:02,585
It ended well thanks to me.

64
00:04:02,585 --> 00:04:06,324
If it wasn't for me, they wouldn't have gotten married.

65
00:04:06,555 --> 00:04:09,155
- I was going to ruin it. - Don't say that.

66
00:04:09,324 --> 00:04:11,924
Your daughter finally found her happiness.

67
00:04:11,924 --> 00:04:14,565
The least you can do is to give her your blessings.

68
00:04:14,565 --> 00:04:16,665
I'm not in a situation to bless her.

69
00:04:16,964 --> 00:04:19,574
Do you even know who Go Ya is married to?

70
00:04:19,574 --> 00:04:20,735
Why wouldn't I know?

71
00:04:20,735 --> 00:04:23,375
He's from a rich family and has a good job.

72
00:04:23,375 --> 00:04:25,144
He's also handsome.

73
00:04:25,144 --> 00:04:27,144
What do kids these days call guys like him again?

74
00:04:27,514 --> 00:04:29,344
Right, he's like a luxurious sedan.

75
00:04:29,344 --> 00:04:30,915
He's not a beat-up car like you.

76
00:04:30,985 --> 00:04:34,115
- What? How dare you say that? - Don't be so sour.

77
00:04:34,485 --> 00:04:37,055
You saw how happy she is in those pictures.

78
00:04:37,755 --> 00:04:40,694
She smiled so much all day today.

79
00:04:41,225 --> 00:04:43,795
I never knew that she had...

80
00:04:43,795 --> 00:04:45,464
such a great smile.

81
00:04:45,464 --> 00:04:48,094
She could never smile because of what you've done,

82
00:04:48,295 --> 00:04:50,065
but she finally got her smile back.

83
00:04:51,704 --> 00:04:55,375
Just give her your blessings, will you?

84
00:04:58,375 --> 00:05:01,144
You don't even know anything. Hey, Tae Ran.

85
00:05:01,315 --> 00:05:04,245
The guy Go Ya married is Oh Na Ra's nephew.

86
00:05:04,415 --> 00:05:07,985
You're the one who would be the angriest if you found out.

87
00:05:09,925 --> 00:05:10,985
Goodness.

88
00:05:12,255 --> 00:05:14,654
Anyway, what do I do now?

89
00:05:15,795 --> 00:05:18,795
My sister-in-law is about to be my in-law.

90
00:05:21,235 --> 00:05:23,935
What kind of a disrupted family is this?

91
00:05:32,915 --> 00:05:33,944
My goodness.

92
00:05:34,915 --> 00:05:36,384
You dropped this.

93
00:05:44,685 --> 00:05:45,824
It's been a while.

94
00:05:49,125 --> 00:05:50,225
Indeed.

95
00:05:52,995 --> 00:05:55,295
- Are you eating here? - I'm leaving now.

96
00:05:56,034 --> 00:05:58,365
I have someone waiting for me.

97
00:06:22,725 --> 00:06:23,925
Sorry.

98
00:06:24,625 --> 00:06:26,024
- Hello. - I'm late, right?

99
00:06:27,894 --> 00:06:31,065
Couldn't you come a bit earlier?

100
00:06:31,365 --> 00:06:32,565
Why?

101
00:06:33,074 --> 00:06:36,204
Your ex-husband was here until a moment ago.

102
00:06:37,805 --> 00:06:39,975
I know. I saw him outside.

103
00:06:39,975 --> 00:06:41,074
Really?

104
00:06:41,144 --> 00:06:43,714
Then did you see the woman he was with too?

105
00:06:44,384 --> 00:06:47,214
- "Woman"? - He was with a woman.

106
00:06:47,355 --> 00:06:49,555
He couldn't stop smiling.

107
00:06:49,824 --> 00:06:52,485
- They must be together. - Hey.

108
00:06:53,425 --> 00:06:54,954
What's the big deal?

109
00:06:54,954 --> 00:06:56,795
I'm married to another man now.

110
00:06:56,795 --> 00:06:58,524
He should get married too.

111
00:06:59,165 --> 00:07:01,194
I'm sorry it took so long.

112
00:07:01,435 --> 00:07:03,365
I ran into my ex-wife.

113
00:07:03,365 --> 00:07:05,764
I see. What?

114
00:07:06,235 --> 00:07:07,534
Your ex-wife?

115
00:07:08,235 --> 00:07:09,435
Yes.

116
00:07:11,375 --> 00:07:13,574
Are you okay?

117
00:07:13,574 --> 00:07:15,475
You had a hard time getting over her.

118
00:07:15,475 --> 00:07:17,985
I sometimes thought about...

119
00:07:18,384 --> 00:07:19,855
what I should do...

120
00:07:20,055 --> 00:07:22,714
if I ever run into her.

121
00:07:24,125 --> 00:07:26,954
But it was hard to imagine.

122
00:07:27,094 --> 00:07:30,394
Just by imagining it, I couldn't even breathe.

123
00:07:30,394 --> 00:07:31,594
I understand.

124
00:07:32,165 --> 00:07:36,235
I also can't breathe when I see my ex-husband with his new wife.

125
00:07:36,605 --> 00:07:39,235
I feel betrayed because I trusted him.

126
00:07:39,875 --> 00:07:43,005
But it wasn't as painful as I had expected.

127
00:07:43,274 --> 00:07:46,245
Really? That's good.

128
00:07:46,675 --> 00:07:50,745
I think it's because you're here with me.

129
00:07:51,185 --> 00:07:53,654
- What? - Because I knew...

130
00:07:53,654 --> 00:07:56,485
you were waiting for me outside,

131
00:07:56,485 --> 00:07:59,225
I somehow felt at ease.

132
00:08:00,925 --> 00:08:03,824
It's a good thing that you were with me...

133
00:08:03,964 --> 00:08:05,524
when I ran into my ex-wife.

134
00:08:07,935 --> 00:08:09,805
Let's go then.

135
00:08:17,574 --> 00:08:20,444
I'm sorry we had to miss our flight.

136
00:08:20,944 --> 00:08:23,014
Didn't you hear what the doctor said?

137
00:08:23,014 --> 00:08:25,944
You might have collapsed if you had got on board.

138
00:08:26,185 --> 00:08:27,954
I don't want to become a widow.

139
00:08:28,654 --> 00:08:30,454
Stomach cramps aren't that serious.

140
00:08:30,654 --> 00:08:32,054
And I'm okay now.

141
00:08:32,384 --> 00:08:35,695
I feel better after getting a sedative at the emergency room.

142
00:08:35,695 --> 00:08:40,294
Did something stressful happen at the wedding?

143
00:08:40,294 --> 00:08:42,064
- What? - I mean,

144
00:08:42,064 --> 00:08:44,505
that's what the doctor guessed.

145
00:08:45,365 --> 00:08:47,375
I've been worried too.

146
00:08:47,735 --> 00:08:49,904
You looked fine this morning,

147
00:08:49,904 --> 00:08:52,674
but you started to look so troubled right before the wedding.

148
00:08:52,945 --> 00:08:54,375
I did?

149
00:08:55,044 --> 00:08:57,384
Did something happen?

150
00:08:57,884 --> 00:08:59,955
What made you so stiff?

151
00:09:00,684 --> 00:09:02,085
Of course I became stiff.

152
00:09:02,284 --> 00:09:05,024
I've been right next to this beautiful lady all day.

153
00:09:05,325 --> 00:09:08,325
- I'm being serious now. - I'm being serious too.

154
00:09:08,955 --> 00:09:11,024
Let's find another flight tomorrow.

155
00:09:11,024 --> 00:09:13,034
That's absurd. We're not going anywhere.

156
00:09:13,034 --> 00:09:15,465
The doctor said you needed good rest.

157
00:09:15,465 --> 00:09:18,564
I won't be able to rest next to you anyway.

158
00:09:18,564 --> 00:09:20,005
My heart will beat too fast.

159
00:09:20,005 --> 00:09:21,375
Do as I say.

160
00:09:21,735 --> 00:09:23,875
You might feel some pain for the next few days,

161
00:09:23,875 --> 00:09:25,345
and you might get more cramps.

162
00:09:25,445 --> 00:09:26,945
We're staying here.

163
00:09:26,945 --> 00:09:29,884
Are you saying we're spending our honeymoon here?

164
00:09:30,784 --> 00:09:32,784
Why not? It's nice here.

165
00:09:32,784 --> 00:09:35,755
We can see the sky and sea, and you're with me too.

166
00:09:36,924 --> 00:09:39,955
I'm sorry. We'll go on another trip soon.

167
00:09:40,294 --> 00:09:42,524
Let me make up for this time.

168
00:09:43,225 --> 00:09:45,264
- Okay. - Let's go out on the beach.

169
00:09:45,794 --> 00:09:47,934
No. You need some rest.

170
00:09:48,965 --> 00:09:50,105
Come here.

171
00:09:52,105 --> 00:09:53,134
All right.

172
00:09:54,434 --> 00:09:56,075
Get some sleep, okay?

173
00:10:01,014 --> 00:10:02,245
You know what?

174
00:10:02,375 --> 00:10:05,314
I read somewhere that you look like the person...

175
00:10:05,314 --> 00:10:08,355
you loved the most in your past life.

176
00:10:09,684 --> 00:10:13,095
In my next life, I'll probably look like you.

177
00:10:14,355 --> 00:10:15,554
I'll...

178
00:10:18,095 --> 00:10:19,365
look like you then.

179
00:10:19,865 --> 00:10:22,434
Guess what I'll do in three seconds.

180
00:10:23,835 --> 00:10:27,174
3, 2, 1.

181
00:10:47,125 --> 00:10:53,294
How did we miss each other so much?

182
00:10:53,294 --> 00:10:59,134
We've always been so nearby

183
00:11:12,384 --> 00:11:13,684
Go Bong.

184
00:11:22,424 --> 00:11:23,564
Gosh!

185
00:11:24,625 --> 00:11:26,794
Did we sleep together?

186
00:11:26,965 --> 00:11:29,865
No. That's impossible.

187
00:11:30,064 --> 00:11:31,164
You brat.

188
00:11:31,365 --> 00:11:34,375
- What happened last night? - What do you mean?

189
00:11:34,375 --> 00:11:37,005
Do you have to stay out the night before your sister's wedding?

190
00:11:37,105 --> 00:11:38,544
Let me explain.

191
00:11:39,375 --> 00:11:40,975
Explain what?

192
00:11:40,975 --> 00:11:42,414
Where did you sleep last night?

193
00:11:42,414 --> 00:11:43,985
At Yeon Hee's.

194
00:11:43,985 --> 00:11:45,985
I can't believe her.

195
00:11:45,985 --> 00:11:47,654
How could she let you sleep there...

196
00:11:47,654 --> 00:11:49,085
when your sister is getting married the next day?

197
00:11:49,355 --> 00:11:50,485
Well,

198
00:11:51,184 --> 00:11:54,455
it turns out that the man she fell in love with...

199
00:11:54,455 --> 00:11:56,195
is her brother-in-law's brother.

200
00:11:56,365 --> 00:11:59,064
Does she want to get married to him?

201
00:11:59,225 --> 00:12:00,335
She can't?

202
00:12:00,335 --> 00:12:02,695
Of course not. That's absurd.

203
00:12:02,965 --> 00:12:05,264
- Why? - Why?

204
00:12:05,264 --> 00:12:07,835
It becomes too complicated.

205
00:12:07,835 --> 00:12:10,975
It's not like he is the only man in the world.

206
00:12:10,975 --> 00:12:14,014
She's right. Your sister won't be just your sister,

207
00:12:14,014 --> 00:12:16,985
but she'll be your brother-in-law's wife too.

208
00:12:17,184 --> 00:12:18,245
It doesn't end there.

209
00:12:18,245 --> 00:12:20,014
It'll be the same...

210
00:12:20,014 --> 00:12:21,654
on the man's side too.

211
00:12:21,654 --> 00:12:23,784
It's making my head spin.

212
00:12:23,784 --> 00:12:26,995
Tell her to break up with him before her parents collapse.

213
00:12:28,595 --> 00:12:31,024
Mom, would that make you collapse too?

214
00:12:31,164 --> 00:12:32,225
Why would I?

215
00:12:32,225 --> 00:12:33,995
Before that happens,

216
00:12:33,995 --> 00:12:36,735
I'd break my daughter's legs.

217
00:12:37,134 --> 00:12:39,774
By the way, where have you been?

218
00:12:39,774 --> 00:12:42,375
Me? I just wandered around a bit.

219
00:12:43,644 --> 00:12:46,514
Were you feeing empty after Go Ya's wedding?

220
00:12:46,914 --> 00:12:49,014
Why would I feel empty?

221
00:12:49,174 --> 00:12:51,115
She got married to a nice man.

222
00:12:52,044 --> 00:12:53,384
- What's that? - What?

223
00:12:53,914 --> 00:12:55,715
It's nothing.

224
00:12:55,955 --> 00:12:57,485
Then why are you hiding it?

225
00:12:57,485 --> 00:12:59,325
- What is it? Show us. - Show us.

226
00:12:59,325 --> 00:13:01,195
It's really nothing.

227
00:13:01,355 --> 00:13:02,554
What?

228
00:13:02,695 --> 00:13:04,924
What's with her? Is she afraid we'll take it away from her?

229
00:13:04,924 --> 00:13:06,564
She probably did some shopping.

230
00:13:07,294 --> 00:13:09,904
Exactly. It's not like...

231
00:13:09,904 --> 00:13:12,404
she received flowers from a man or anything.

232
00:13:27,715 --> 00:13:29,384
My goodness.

233
00:13:36,894 --> 00:13:37,995
Go Ya.

234
00:13:39,195 --> 00:13:40,634
Be happy.

235
00:13:46,134 --> 00:13:47,235
It's beautiful here.

236
00:13:49,434 --> 00:13:50,605
Do you know this poem?

237
00:13:52,144 --> 00:13:54,375
"I wait for you..."

238
00:13:55,174 --> 00:13:56,985
"not because I want to,"

239
00:13:57,514 --> 00:13:59,115
"but because I have no choice but to do so."

240
00:14:00,755 --> 00:14:03,024
"While waiting for you,"

241
00:14:03,024 --> 00:14:05,384
"the pain makes me put on weight,"

242
00:14:05,884 --> 00:14:08,524
"and the longing makes me grow taller."

243
00:14:09,394 --> 00:14:10,495
You know it.

244
00:14:11,365 --> 00:14:12,524
That's not the only thing I know.

245
00:14:13,495 --> 00:14:15,465
Since I met you,

246
00:14:15,764 --> 00:14:18,335
I've learned what it means to have no choice but to wait for someone.

247
00:14:19,505 --> 00:14:22,475
The pain and longing has made me...

248
00:14:22,975 --> 00:14:24,804
grow too, I think.

249
00:14:25,605 --> 00:14:27,314
Love has got us through it all.

250
00:14:28,715 --> 00:14:29,985
It will from now on too.

251
00:14:30,715 --> 00:14:32,284
No matter what awaits us.

252
00:15:05,445 --> 00:15:06,715
It's been a while.

253
00:15:07,884 --> 00:15:08,955
Indeed.

254
00:15:09,755 --> 00:15:10,985
Right.

255
00:15:10,985 --> 00:15:13,725
He must have known Ji Seok was getting married.

256
00:15:15,455 --> 00:15:17,465
Was he at the wedding?

257
00:15:26,934 --> 00:15:28,005
Tae Ran?

258
00:15:29,575 --> 00:15:32,144
Isn't she Tae Pyung's sister?

259
00:15:34,845 --> 00:15:37,485
Tae Pyung, here are a few more pictures of Go Ya.

260
00:15:38,314 --> 00:15:39,715
Pictures of Go Ya?

261
00:15:40,355 --> 00:15:43,325
Why are you looking at my phone?

262
00:15:43,424 --> 00:15:44,554
You.

263
00:15:44,955 --> 00:15:48,455
Since when were you so close with your sister?

264
00:15:48,755 --> 00:15:50,894
- What? - Why is your sister...

265
00:15:50,894 --> 00:15:53,294
sending you Choi Go Ya's pictures?

266
00:15:53,294 --> 00:15:54,564
Did you see them?

267
00:15:54,564 --> 00:15:56,735
Give it to me. Let me see what she sent you.

268
00:15:56,735 --> 00:15:59,335
- No. - Give it to me.

269
00:15:59,705 --> 00:16:02,105
Fine. One second.

270
00:16:03,144 --> 00:16:04,475
My goodness.

271
00:16:04,975 --> 00:16:08,544
Hey, I pressed the wrong button and deleted everything.

272
00:16:09,274 --> 00:16:12,044
How can you come up with such an obvious lie?

273
00:16:12,414 --> 00:16:14,784
Did you whine to your sister...

274
00:16:14,784 --> 00:16:17,284
and said you missed her?

275
00:16:17,284 --> 00:16:19,524
- No. - Why didn't you...

276
00:16:19,924 --> 00:16:23,064
ask her to send you Woo Yang Sook's pictures too?

277
00:16:23,064 --> 00:16:25,424
That's nonsense. It's not like that.

278
00:16:25,524 --> 00:16:26,894
I don't care about her.

279
00:16:26,894 --> 00:16:29,064
Tae Ran just keeps on sending me these.

280
00:16:29,064 --> 00:16:32,505
Darn it, I hate everything.

281
00:16:32,904 --> 00:16:34,235
Seriously.

282
00:16:36,075 --> 00:16:38,445
My gosh, this is driving me crazy.

283
00:16:40,845 --> 00:16:42,774
(Tae Ran)

284
00:16:47,715 --> 00:16:49,384
Hey! Stop sending me these!

285
00:16:49,725 --> 00:16:51,325
Do you want to see me dead?

286
00:16:51,325 --> 00:16:54,024
My goodness, what's with him?

287
00:16:54,024 --> 00:16:55,755
Why would he act like that when I just sent those pictures...

288
00:16:55,755 --> 00:16:57,764
because he whined about not getting invited?

289
00:17:00,064 --> 00:17:02,264
- Aunt. - What brings you here?

290
00:17:02,695 --> 00:17:04,034
Can I take some kimchi?

291
00:17:04,034 --> 00:17:06,334
I moved out to a studio near my cafe.

292
00:17:06,334 --> 00:17:08,635
- Take some. - Where are you going?

293
00:17:09,705 --> 00:17:11,604
Upstairs. The newlyweds' room.

294
00:17:13,374 --> 00:17:14,415
I want to come too.

295
00:17:14,415 --> 00:17:17,114
Nonsense. They haven't even seen the room yet.

296
00:17:17,114 --> 00:17:18,314
You can't come.

297
00:17:18,314 --> 00:17:19,715
Take your kimchi, and go.

298
00:17:22,415 --> 00:17:23,725
She's so cheap.

299
00:17:28,094 --> 00:17:30,564
- Goodness, stop following me. - One second.

300
00:17:30,564 --> 00:17:32,594
You can't come into the newlyweds' place.

301
00:17:33,395 --> 00:17:36,205
My gosh, why is she so strong?

302
00:17:42,604 --> 00:17:45,745
I know I asked him to bring someone to share the bed with,

303
00:17:46,945 --> 00:17:48,844
but you brought someone like her.

304
00:17:53,814 --> 00:17:55,784
I was feeling complicated enough,

305
00:17:56,155 --> 00:17:59,084
but that guy made it even worse.

306
00:18:05,564 --> 00:18:07,995
Mother, we've arrived well.

307
00:18:07,995 --> 00:18:09,635
We'll lead a happy life.

308
00:18:09,635 --> 00:18:11,935
And next time, let's come here together.

309
00:18:12,264 --> 00:18:13,905
(Go Ya)

310
00:18:13,905 --> 00:18:15,475
She doesn't even mean that.

311
00:18:17,205 --> 00:18:20,145
Fine. At least she's not his daughter.

312
00:18:20,544 --> 00:18:22,885
Who cares if she filed a suit against her dad?

313
00:18:22,885 --> 00:18:24,945
It's not like she's his daughter.

314
00:18:29,655 --> 00:18:30,725
Have some.

315
00:18:34,794 --> 00:18:36,725
- Did you do it? - Why aren't they eating?

316
00:18:37,925 --> 00:18:38,965
Have some.

317
00:18:50,374 --> 00:18:51,445
Okay.

318
00:18:52,245 --> 00:18:53,245
Good.

319
00:19:23,445 --> 00:19:25,344
It's been a difficult journey.

320
00:19:25,344 --> 00:19:27,844
So make sure you two live happily ever after.

321
00:19:28,245 --> 00:19:29,245
- Yes. - Yes.

322
00:19:29,245 --> 00:19:32,784
And don't take each other for granted because you're family.

323
00:19:32,784 --> 00:19:34,955
Always treat each other with respect.

324
00:19:35,514 --> 00:19:37,024
Someone once said this.

325
00:19:37,225 --> 00:19:40,395
"If you think of your wife as a business partner..."

326
00:19:40,395 --> 00:19:41,955
"and not your wife,"

327
00:19:41,955 --> 00:19:44,364
"you can treat her with more respect."

328
00:19:44,995 --> 00:19:46,935
Yes, I'll remember that, Mother.

329
00:19:47,435 --> 00:19:48,895
You too.

330
00:19:49,334 --> 00:19:50,635
Yes, Mom.

331
00:19:52,005 --> 00:19:54,334
So how was the honeymoon?

332
00:19:54,334 --> 00:19:57,274
- Where did you go again? Pa... - Palau.

333
00:19:57,774 --> 00:20:00,274
I heard Milky Way Bay is really beautiful there.

334
00:20:00,574 --> 00:20:01,574
What?

335
00:20:02,374 --> 00:20:04,544
Oh, yes. It was.

336
00:20:04,544 --> 00:20:06,314
- Show us some pictures. - Yes, show us.

337
00:20:06,685 --> 00:20:08,354
Well,

338
00:20:08,655 --> 00:20:12,185
we were really out of it, so we couldn't take any pictures.

339
00:20:14,395 --> 00:20:15,955
Of course.

340
00:20:16,155 --> 00:20:19,395
I'm sure you didn't have any time to go sightseeing.

341
00:20:20,364 --> 00:20:23,534
During a honeymoon trip, all you want to do is...

342
00:20:24,005 --> 00:20:25,064
My goodness.

343
00:20:25,235 --> 00:20:27,274
- Come on, Tae Ran. - Mom!

344
00:20:27,905 --> 00:20:31,745
What? I'm just saying that those are the good times.

345
00:20:32,205 --> 00:20:33,314
My gosh.

346
00:20:35,715 --> 00:20:38,814
As you know, our family is very talkative.

347
00:20:39,014 --> 00:20:41,854
- I love it. - I'm glad then.

348
00:20:42,715 --> 00:20:44,354
They must be hungry.

349
00:20:44,354 --> 00:20:46,524
- San Deul, come get the table. - Okay.

350
00:20:46,524 --> 00:20:49,895
She made some braised ribs for you and Ji Seok.

351
00:20:50,395 --> 00:20:51,564
Thank you, Mother.

352
00:20:54,635 --> 00:20:57,064
- Is this little girl you? - Yes.

353
00:20:58,735 --> 00:21:01,274
- Is this your high school picture? - You can't see this.

354
00:21:01,975 --> 00:21:04,405
- Why not? - This was back when I was...

355
00:21:04,544 --> 00:21:06,475
a bit fat. It's embarrassing.

356
00:21:07,475 --> 00:21:10,284
Okay. I'll close my eyes. You can take that out.

357
00:21:12,854 --> 00:21:14,814
Hey, you can't see that.

358
00:21:15,455 --> 00:21:17,255
What? You look pretty.

359
00:21:17,455 --> 00:21:19,385
You've never been ugly in your life, have you?

360
00:21:19,385 --> 00:21:21,755
How many girls did you hit on with that line?

361
00:21:21,955 --> 00:21:25,294
What do you think of me? I'm not that easy.

362
00:21:25,594 --> 00:21:27,235
I'm very reserved.

363
00:21:27,235 --> 00:21:29,705
Do reserved men say things like, "What's your religion?"

364
00:21:29,705 --> 00:21:31,304
"What do you have to believe..."

365
00:21:31,304 --> 00:21:32,834
"to be so pretty?"

366
00:21:33,235 --> 00:21:34,574
I really wanted to know.

367
00:21:34,705 --> 00:21:37,505
I wondered what you had to believe to be this pretty.

368
00:21:38,304 --> 00:21:39,505
So why don't we...

369
00:21:41,114 --> 00:21:44,185
It's so inappropriate to do that here.

370
00:21:45,415 --> 00:21:47,215
Put those lips away.

371
00:21:48,385 --> 00:21:50,885
- Go Ya. - Hey, Go Woon.

372
00:21:52,784 --> 00:21:55,094
- Goodness. - Go Woon, what is it?

373
00:21:55,395 --> 00:21:57,064
Come out, and have some fruit.

374
00:21:57,965 --> 00:22:00,465
Oh, you can finish doing what you were doing.

375
00:22:03,205 --> 00:22:04,264
My goodness.

376
00:22:12,104 --> 00:22:13,745
We're leaving now.

377
00:22:13,745 --> 00:22:15,745
Okay. I'll pack up some side dishes.

378
00:22:17,385 --> 00:22:18,814
Why did you make so much food?

379
00:22:18,814 --> 00:22:21,354
If I don't do this right, your mother-in-law...

380
00:22:21,354 --> 00:22:23,225
will nitpick over the smallest things again.

381
00:22:23,225 --> 00:22:25,255
She's really quick-tempered.

382
00:22:25,485 --> 00:22:27,324
I'm afraid she'll give you a hard time.

383
00:22:27,455 --> 00:22:29,995
- Don't worry. - I won't.

384
00:22:30,195 --> 00:22:32,895
You aren't just going to let her bully you.

385
00:22:33,364 --> 00:22:35,935
Be confident and stand up for yourself...

386
00:22:35,935 --> 00:22:38,205
just as you did with me.

387
00:22:38,205 --> 00:22:40,304
And if you have something to show off, do that.

388
00:22:40,534 --> 00:22:42,135
You're good at that.

389
00:22:42,135 --> 00:22:45,745
Letting everyone know about something you've done.

390
00:22:46,814 --> 00:22:48,915
- Okay. - And...

391
00:22:49,544 --> 00:22:53,114
if things get too hard, don't sit on it by yourself.

392
00:22:53,114 --> 00:22:55,584
- Come talk to me. - Okay.

393
00:22:56,084 --> 00:22:58,885
Don't forget to care for yourself while caring for others.

394
00:23:02,725 --> 00:23:04,264
I'll visit often.

395
00:23:05,124 --> 00:23:08,334
Okay. That's good news for me.

396
00:23:19,344 --> 00:23:21,344
(Jang Ok Ja's Cooking Class)

397
00:23:21,344 --> 00:23:23,945
- Goodness. - Are you insane?

398
00:23:24,014 --> 00:23:27,314
Yes, you're driving me insane.

399
00:23:27,885 --> 00:23:30,784
I can't believe I didn't know about Ji Seok's wedding.

400
00:23:31,385 --> 00:23:33,024
You're unbelievable.

401
00:23:33,155 --> 00:23:36,225
I was courteous enough to tell you...

402
00:23:36,225 --> 00:23:39,064
that Ji Seok is getting married.

403
00:23:39,534 --> 00:23:41,435
I'm his aunt.

404
00:23:41,435 --> 00:23:44,764
I deserve to be at his wedding.

405
00:23:44,764 --> 00:23:46,274
In your dreams.

406
00:23:47,005 --> 00:23:48,705
You aren't welcome there.

407
00:23:49,435 --> 00:23:51,405
You'll be nothing but a shame.

408
00:23:51,405 --> 00:23:53,245
What did I do so wrong?

409
00:23:53,514 --> 00:23:56,614
I have nothing to hide. I'm confident with myself.

410
00:23:57,314 --> 00:23:58,415
What?

411
00:23:59,114 --> 00:24:00,385
When is Ji Seok coming?

412
00:24:01,584 --> 00:24:02,925
Why do you care?

413
00:24:05,255 --> 00:24:08,094
I'm going to see him again.

414
00:24:08,524 --> 00:24:11,294
Why can't we see each other? Who are you to tell him that?

415
00:24:11,294 --> 00:24:13,395
When you see him,

416
00:24:13,965 --> 00:24:16,034
I hope you tell him...

417
00:24:16,034 --> 00:24:18,165
that you broke someone else's family.

418
00:24:18,165 --> 00:24:19,774
- Sa Ra. - You're confident?

419
00:24:20,005 --> 00:24:21,635
How can you feel so confident?

420
00:24:22,205 --> 00:24:24,475
Do you feel confident about living with a guy like Tae Pyung...

421
00:24:24,475 --> 00:24:26,614
after dumping a great guy like Jae Woong?

422
00:24:26,915 --> 00:24:29,915
Do you not feel ashamed regarding the child...

423
00:24:29,915 --> 00:24:32,344
that Tae Pyung's wife had at that time?

424
00:24:32,344 --> 00:24:35,155
Do you not feel ashamed for breaking their family?

425
00:24:35,155 --> 00:24:36,584
- Sa Ra. - If not those,

426
00:24:37,084 --> 00:24:40,294
do you really feel confident in front of me,

427
00:24:40,725 --> 00:24:42,824
who brought you up like your mom?

428
00:24:44,764 --> 00:24:48,465
When I tried to stop you as you left me,

429
00:24:48,735 --> 00:24:50,165
you told me this.

430
00:24:51,064 --> 00:24:52,874
"Stop being so clingy."

431
00:24:53,574 --> 00:24:56,745
Why are you so clingy now?

432
00:24:58,074 --> 00:25:01,814
You should stop being so clingy about Ji Seok.

433
00:25:05,945 --> 00:25:07,215
Unbelievable.

434
00:25:14,794 --> 00:25:16,324
The person you have reached is not available.

435
00:25:16,324 --> 00:25:18,324
- Please leave a message... - I can't believe this.

436
00:25:21,564 --> 00:25:24,235
Ji Seok, it's me, your aunt.

437
00:25:24,935 --> 00:25:26,935
I need to see you for a minute. Please.

438
00:25:27,405 --> 00:25:29,874
I have something to tell you.

439
00:25:30,505 --> 00:25:31,905
My gosh.

440
00:25:33,645 --> 00:25:35,245
Darn it.

441
00:26:03,774 --> 00:26:04,844
All right.

442
00:26:11,044 --> 00:26:12,445
Mom, where is the cake?

443
00:26:13,655 --> 00:26:16,655
Didn't you say you were going to get a cake?

444
00:26:16,955 --> 00:26:19,824
I dropped and ruined it because of my sister.

445
00:26:20,794 --> 00:26:22,895
- Was Aunt here? - Yes.

446
00:26:23,364 --> 00:26:25,465
She threw a tantrum...

447
00:26:25,465 --> 00:26:27,195
about not being invited to Ji Seok's wedding.

448
00:26:27,695 --> 00:26:30,005
We're welcoming our new family member today.

449
00:26:30,005 --> 00:26:32,665
Exactly. I can't stand her.

450
00:26:33,675 --> 00:26:35,804
Please don't say anything...

451
00:26:35,804 --> 00:26:38,304
in front of Go Ya.

452
00:26:38,574 --> 00:26:41,044
I don't want her to know about Ji Seok's aunt.

453
00:26:41,344 --> 00:26:45,044
And Ji Seok will feel bad if he finds out she was here.

454
00:26:49,354 --> 00:26:50,524
They must be here.

455
00:26:54,155 --> 00:26:57,024
Did you enjoy your honeymoon?

456
00:26:57,364 --> 00:27:00,264
Yes. We had a lot of fun.

457
00:27:00,594 --> 00:27:03,064
Ji Seok, didn't you eat well?

458
00:27:03,505 --> 00:27:05,465
You look so thin.

459
00:27:05,564 --> 00:27:07,274
I was so happy that I didn't need any food.

460
00:27:07,505 --> 00:27:10,975
Gosh, you're making me cringe. Stop it.

461
00:27:10,975 --> 00:27:12,505
Exactly.

462
00:27:12,705 --> 00:27:14,614
What's with him?

463
00:27:14,814 --> 00:27:17,614
What are you talking about? I think it's cute.

464
00:27:19,084 --> 00:27:21,084
I heard your little brother...

465
00:27:21,084 --> 00:27:23,024
is Eun Seok's student.

466
00:27:23,554 --> 00:27:25,324
Yes. It surprised me too.

467
00:27:25,324 --> 00:27:27,294
I had no idea you were Go Woon's teacher.

468
00:27:27,294 --> 00:27:28,425
Neither did I.

469
00:27:28,655 --> 00:27:31,425
Go Woon used to say his eldest sister's future husband...

470
00:27:31,425 --> 00:27:33,495
would be the luckiest person in the world.

471
00:27:33,495 --> 00:27:35,834
I didn't know it'd be my brother.

472
00:27:36,665 --> 00:27:38,264
What are you talking about?

473
00:27:38,705 --> 00:27:40,505
Ji Seok's wife is the one...

474
00:27:40,505 --> 00:27:42,505
who would be the luckiest person in the world.

475
00:27:43,074 --> 00:27:45,245
I'm not saying this because he is my son,

476
00:27:45,245 --> 00:27:47,675
but your husband is an amazing man.

477
00:27:47,675 --> 00:27:51,014
Yes. And I have you to thank for that.

478
00:27:51,745 --> 00:27:53,715
You got my point.

479
00:27:55,054 --> 00:27:59,084
Your mom must feel empty after her sweet daughter's wedding.

480
00:27:59,324 --> 00:28:01,495
That's why I should be a nice son-in-law.

481
00:28:05,824 --> 00:28:07,235
You're making me cringe again.

482
00:28:10,764 --> 00:28:14,505
Hey. Where did my smart son go?

483
00:28:14,505 --> 00:28:17,005
He sounds so silly now.

484
00:28:17,205 --> 00:28:18,304
What do you mean?

485
00:28:18,304 --> 00:28:20,915
It's nice to see him in love.

486
00:28:20,915 --> 00:28:23,514
What? "That's why I should be a nice son-in-law."

487
00:28:25,014 --> 00:28:27,354
I'm the one he should be nice to,

488
00:28:27,354 --> 00:28:29,554
not his wife's mom.

489
00:28:29,554 --> 00:28:31,824
That woman is so vulgar.

490
00:28:31,824 --> 00:28:35,294
Mom, she is your in-law now.

491
00:28:36,425 --> 00:28:37,564
Stay for dinner.

492
00:28:37,564 --> 00:28:40,064
I ate too much already. I'm not that hungry.

493
00:28:40,395 --> 00:28:42,334
I've seen Ji Seok and Go Ya now,

494
00:28:42,334 --> 00:28:43,735
so I should get going.

495
00:28:44,034 --> 00:28:46,205
- Already? - Yes.

496
00:28:46,475 --> 00:28:48,374
Is something bothering you?

497
00:28:48,834 --> 00:28:52,044
No. I'll get going then.

498
00:29:00,084 --> 00:29:01,415
Welcome to our club.

499
00:29:01,514 --> 00:29:02,854
Hello, sir.

500
00:29:03,124 --> 00:29:05,185
Ask for Lady at the entrance.

501
00:29:05,185 --> 00:29:13,864
(Lady)

502
00:29:17,435 --> 00:29:22,774
(Trash)

503
00:29:22,774 --> 00:29:25,245
- I miss you, Trash. - What?

504
00:29:26,975 --> 00:29:29,314
Ask for Trash at the entrance.

505
00:29:30,985 --> 00:29:40,695
(Lemon Balm Diet)

506
00:29:40,695 --> 00:29:42,925
How did I end up here again?

507
00:29:57,675 --> 00:30:05,044
(Slimedge Lemon Balm Diet)

508
00:30:06,514 --> 00:30:08,014
He's not even calling me.

509
00:30:08,385 --> 00:30:11,784
Is he cutting me off because we're in-laws now?

510
00:30:15,195 --> 00:30:16,665
Just a second.

511
00:30:17,725 --> 00:30:19,024
Are we far yet?

512
00:30:20,895 --> 00:30:22,735
You can see now. Surprise.

513
00:30:22,764 --> 00:30:26,064
What do you think? I chose everything in this room myself.

514
00:30:28,304 --> 00:30:29,804
It's like you.

515
00:30:29,945 --> 00:30:31,245
What do you mean?

516
00:30:31,245 --> 00:30:32,844
I like it for no particular reason.

517
00:30:35,114 --> 00:30:38,185
We're starting over here together.

518
00:30:38,544 --> 00:30:41,715
It's finally starting to sink in that we're married.

519
00:30:42,054 --> 00:30:44,485
I'm so happy...

520
00:30:44,485 --> 00:30:46,524
to always have you by my side.

521
00:30:48,324 --> 00:30:50,395
You must be tired after meeting so many people today.

522
00:30:50,395 --> 00:30:52,094
Why don't you take a bath?

523
00:30:52,094 --> 00:30:53,534
I have to go out for a while.

524
00:30:53,764 --> 00:30:55,965
Is it something urgent?

525
00:30:55,965 --> 00:30:59,005
There is something I have to take care of today.

526
00:31:08,475 --> 00:31:11,614
Should I tell her or not?

527
00:31:12,415 --> 00:31:15,385
If she finds out that her nephew is married to my daughter,

528
00:31:15,755 --> 00:31:18,624
she'll faint.

529
00:31:19,624 --> 00:31:22,824
But if she finds out that I've been keeping it from her,

530
00:31:23,864 --> 00:31:27,064
it'll make her faint too.

531
00:31:28,635 --> 00:31:29,764
Cake!

532
00:31:30,135 --> 00:31:32,534
You startled me. What about it?

533
00:31:32,534 --> 00:31:34,175
I'm sure Sa Ra was holding a cake...

534
00:31:34,175 --> 00:31:36,245
when I saw her earlier.

535
00:31:36,645 --> 00:31:37,844
It means...

536
00:31:37,844 --> 00:31:40,645
Ji Seok is coming back from his honeymoon today.

537
00:31:42,645 --> 00:31:44,745
Where are you going?

538
00:31:44,745 --> 00:31:47,814
- I want to see Ji Seok. - Please don't do this.

539
00:31:47,814 --> 00:31:50,385
You might collapse.

540
00:31:50,385 --> 00:31:52,755
- Why? - What if...

541
00:31:52,955 --> 00:31:55,155
you see Ji Seok's wife there?

542
00:31:55,155 --> 00:31:56,564
What about her?

543
00:32:01,935 --> 00:32:03,064
What's wrong?

544
00:32:03,804 --> 00:32:04,905
Hold on.

545
00:32:08,034 --> 00:32:10,344
- Hello. - This is Ji Seok.

546
00:32:11,374 --> 00:32:12,475
Okay.

547
00:32:13,445 --> 00:32:14,614
All right.

548
00:32:17,314 --> 00:32:19,185
What is it?

549
00:32:20,955 --> 00:32:22,084
Hold on.

550
00:32:22,514 --> 00:32:25,425
I'll explain when I come back.

551
00:32:26,155 --> 00:32:27,195
Honey.

552
00:32:27,594 --> 00:32:30,294
Honey. Honey!

553
00:32:33,334 --> 00:32:36,205
What's going on?

554
00:32:53,284 --> 00:32:54,584
You jerk!

555
00:33:14,334 --> 00:33:15,374
(Forever Enemies)

556
00:33:15,374 --> 00:33:17,344
Why can't I see Ji Seok's wife?

557
00:33:17,344 --> 00:33:19,844
I disappointed Mother on our first day here.

558
00:33:19,844 --> 00:33:21,314
I'm looking for someone.

559
00:33:21,314 --> 00:33:23,314
I'm being threatened.

560
00:33:23,314 --> 00:33:25,614
Something doesn't feel right. I feel empty.

561
00:33:25,614 --> 00:33:28,415
Ji Seok got married to Go Ya?

562
00:33:28,415 --> 00:33:29,824
Please don't ask Ji Seok...

563
00:33:29,824 --> 00:33:32,185
to give his kidney to your brother.

564
00:33:32,185 --> 00:33:34,495
I want to be tested for the kidney transplant.

