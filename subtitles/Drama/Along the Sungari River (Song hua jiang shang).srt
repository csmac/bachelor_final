﻿1
00:01:27,130 --> 00:01:31,500
<i>Subtitles brought to you by the Love Chocolate Team @Viki</i>

2
00:01:32,200 --> 00:01:35,000
<i>Episode 79</i>

3
00:01:35,500 --> 00:01:37,400
I promised you

4
00:01:37,400 --> 00:01:39,600
a romantic proposal.

5
00:01:40,200 --> 00:01:42,400
Hence,

6
00:01:42,460 --> 00:01:45,670
I especially chose this ring out.

7
00:01:45,700 --> 00:01:48,000
I never thought that

8
00:01:48,000 --> 00:01:51,100
it would be under a situation like this.

9
00:01:51,100 --> 00:01:54,600
It's okay, it's not important.

10
00:01:54,600 --> 00:01:56,300
It is important.

11
00:01:58,590 --> 00:02:00,880
You are very important to me.

12
00:02:14,000 --> 00:02:18,300
Are you willing to accept this ring?

13
00:02:22,830 --> 00:02:25,710
Are you willing to accept my heart?

14
00:02:27,700 --> 00:02:53,100
<i>Subtitles brought to you by the Love Chocolate Team @Viki</i>

15
00:03:16,050 --> 00:03:20,250
In the name of God, Mr. Fang Jia Hua

16
00:03:20,300 --> 00:03:24,600
and Miss Hong Xi En, I now pronounced you...

17
00:03:26,400 --> 00:03:28,200
man and wife.

18
00:03:53,920 --> 00:03:57,760
<i>Congratulations on finding your eternal happiness.</i>

19
00:03:57,800 --> 00:04:00,500
<i>Congratulations on fulfilling your wish. </i>

20
00:04:00,500 --> 00:04:04,000
<i>Right now it is time for me to find my happiness.</i>

21
00:04:04,000 --> 00:04:08,000
<i>To find someone who will love and 
 cherish me forever</i>

22
00:04:10,700 --> 00:04:20,000
<i>Subtitles brought to you by the Love Chocolate Team @Viki</i>

23
00:04:30,500 --> 00:04:32,300
Mr. Fang.

24
00:04:33,600 --> 00:04:35,000
We are now family.

25
00:04:36,000 --> 00:04:37,600
Close family?

26
00:04:37,600 --> 00:04:39,900
Let me tell you in advance first.

27
00:04:39,900 --> 00:04:45,100
If Chocolate were to bully Xi En, I will discipline him like my own son.

28
00:04:45,100 --> 00:04:48,600
This is necessary. 
 Thank you.

29
00:04:49,500 --> 00:04:51,400
Next time it's you guys.

30
00:04:51,400 --> 00:04:54,100
Not necessarily. You guys might be before us.

31
00:04:55,900 --> 00:04:57,400
Impossible!

32
00:04:58,800 --> 00:05:01,800
Jia Hua. Take good care of Xi En.

33
00:05:01,800 --> 00:05:06,600
After you guys leave you always have to be careful.

34
00:05:06,600 --> 00:05:10,800
Remember to call if there are any matters.

35
00:05:10,800 --> 00:05:13,600
That's not important. They are already adults.

36
00:05:13,600 --> 00:05:16,700
What's important then?

37
00:05:16,700 --> 00:05:20,100
HAVE CHILDREN AS SOON AS POSSIBLE!

38
00:05:22,600 --> 00:05:28,500
Did you hear that? We have to have a son, who knows 
 how to make chocolate.

39
00:05:44,900 --> 00:05:46,600
Boss.

40
00:05:46,600 --> 00:05:48,300
Boss!

41
00:06:08,920 --> 00:06:11,760
He didn't go overseas.

42
00:06:18,580 --> 00:06:24,240
<i>Sorry but the number you have dialed has
 no reply so they cannot be reached. Thank you.</i>

43
00:06:31,700 --> 00:06:35,500
- Xi Hui. What's wrong? 
- Have you guys seen my boss?

44
00:06:35,500 --> 00:06:37,600
No he hasn't come here.

45
00:06:37,600 --> 00:06:38,800
What's the matter?

46
00:06:38,800 --> 00:06:41,800
He lied and said he went abroad for business.

47
00:06:41,800 --> 00:06:48,300
I'm worried that for the sake of protecting us, he may have gone to perform some illegal task for Zhao Hai Feng.

48
00:06:49,630 --> 00:06:51,630
Hurry up and return home.

49
00:06:51,670 --> 00:06:53,290
I'm going to look for Hai Feng and ask him.

50
00:06:53,300 --> 00:06:55,000
How can you? I'm going too!

51
00:06:55,000 --> 00:06:57,400
No. It's too dangerous.

52
00:06:57,400 --> 00:07:00,000
It's because it's dangerous, we should go together.

53
00:07:00,000 --> 00:07:04,400
But Boss won't be happy if you go.

54
00:07:08,000 --> 00:07:11,700
Hence, in any case, if you go, he won't be happy.

55
00:07:11,700 --> 00:07:14,300
If we go, he won't be happy too.

56
00:07:14,300 --> 00:07:17,700
Why don't we do this? Let's all go together.

57
00:07:20,000 --> 00:07:24,300
Then promise me you won't be rash and irrational.

58
00:07:24,300 --> 00:07:26,600
We are just going to ask him some questions ok?

59
00:07:26,600 --> 00:07:28,000
Okay.

60
00:07:40,500 --> 00:07:44,300
What are you doing? Field trip?

61
00:07:44,300 --> 00:07:45,800
Isn't it too late?

62
00:07:45,800 --> 00:07:47,900
Where is Boss?

63
00:07:49,300 --> 00:07:55,500
Lovely, just because your boss isn't here you think you can
become the hero?

64
00:07:55,500 --> 00:07:58,400
Actually it's not enough. Keep training, baby.

65
00:07:58,400 --> 00:08:00,200
Hey!

66
00:08:00,200 --> 00:08:02,000
Wow.

67
00:08:02,000 --> 00:08:04,790
You switch sides so fast.

68
00:08:04,800 --> 00:08:07,800
Stop your crap. Where is my boss?

69
00:08:08,500 --> 00:08:11,600
Do you have THE RIGHT TO SPEAK?

70
00:08:11,600 --> 00:08:14,100
Are you going to tell us or not?

71
00:08:14,100 --> 00:08:18,900
Why are you asking me? Who do I ask then? This is a pool place.

72
00:08:18,900 --> 00:08:22,700
Or you've been in love with me for a while now?

73
00:08:22,750 --> 00:08:25,920
Just another excuse.

74
00:08:26,000 --> 00:08:28,540
Hey! Are you going to tell us or not?

75
00:08:28,580 --> 00:08:30,880
Move back.

76
00:08:39,500 --> 00:08:42,100
Boss is calling you, right?

77
00:08:42,700 --> 00:08:45,700
So what if it is? So what if it's not?

78
00:08:45,700 --> 00:08:48,800
Tell me where he is now! I want to see him!

79
00:08:48,800 --> 00:08:52,500
How would I know?

80
00:08:53,590 --> 00:08:55,670
I don't understand you.

81
00:08:55,670 --> 00:08:59,160
Lu Zheng Ting pushed you away so frantically and asked you not to stick with us

82
00:08:59,200 --> 00:09:01,600
but you choose to dig deeper.

83
00:09:01,600 --> 00:09:04,300
You know you're a busybody?

84
00:09:06,790 --> 00:09:11,000
But honestly speaking I still have to thank you.

85
00:09:11,000 --> 00:09:15,300
If not for you how would he listen to me?

86
00:09:16,400 --> 00:09:18,700
Maybe you think that women are useless.

87
00:09:18,700 --> 00:09:23,800
But just like how men can sacrifice for loyalty, women can also sacrifice for love.

88
00:09:23,800 --> 00:09:27,500
If you let him get harmed, I WON'T LET YOU GO!

89
00:09:27,500 --> 00:09:31,800
So sweet, right, baby?

90
00:09:33,000 --> 00:09:36,500
What can you do?

91
00:09:36,500 --> 00:09:39,700
Not only her, but us too.

92
00:09:39,700 --> 00:09:45,100
If you dare to touch a single hair on his head, 
we won't forgive you.

93
00:09:46,600 --> 00:09:49,800
Did I ask you to move, go back.

94
00:09:50,800 --> 00:09:55,800
Hurry home, little girl. Don't waste Lu Zheng Ting's efforts.

95
00:10:03,900 --> 00:10:12,800
<i> "You have been transferred to the voicemail box. Charges will apply after the 'beep' sound. If you do not wish to..."</i>

96
00:10:20,200 --> 00:10:24,700
Boss it's me, I know Hai Feng just called.

97
00:10:24,700 --> 00:10:29,200
But could you tell me your location. I want to see you.

98
00:10:30,040 --> 00:10:35,420
I know you want to protect us and protect my feelings.

99
00:10:35,500 --> 00:10:40,700
But right now you're making me worried and 
only harming me even more.

100
00:10:41,290 --> 00:10:45,040
Because of me you did a lot of things you didn't 
 want to do.

101
00:10:45,090 --> 00:10:48,130
You even wore the shirt I gave you.

102
00:10:48,180 --> 00:10:53,630
I beg to promise me one thing: let me be by your side ok?

103
00:10:56,100 --> 00:11:01,100
<i> Press 4 to replay. Press 7 to delete..</i>

104
00:11:17,500 --> 00:11:22,600
I just phoned and ask, and they said he's not there.

105
00:11:25,200 --> 00:11:28,700
Where exactly is he?

106
00:11:32,100 --> 00:11:34,500
<i>I think I saw Boss just now.</i>

107
00:11:34,500 --> 00:11:36,200
<i> Didn't he say he went abroad?</i>

108
00:11:36,220 --> 00:11:39,000
<i>But I really saw his car. </i>

109
00:11:39,050 --> 00:11:42,030
<i> There are so many types of car.</i>

110
00:11:42,100 --> 00:11:48,100
<i>Of course, there are lots of the same type of cars out there. Impossible, he has already gone overseas. </i>

111
00:11:49,410 --> 00:11:51,710
All of you, go back first. I will think of a solution.

112
00:11:51,760 --> 00:11:54,250
Hey, Sister Xi Hui!

113
00:11:56,400 --> 00:11:58,200
What should we do?

114
00:12:24,140 --> 00:12:26,090
What are you thinking of?

115
00:12:28,850 --> 00:12:32,270
I'm wondering if I'm dreaming now.

116
00:12:34,030 --> 00:12:35,580
Do you want me

117
00:12:35,580 --> 00:12:39,660
to hit you to make sure that you are not dreaming?

118
00:12:41,250 --> 00:12:44,440
Home violence the first day we got married. That isn't good.

119
00:12:44,440 --> 00:12:46,640
If Father Hong,

120
00:12:49,620 --> 00:12:53,140
finds out, he will be very sad.

121
00:12:53,180 --> 00:12:57,060
My mom would probably feel sad

122
00:12:57,060 --> 00:12:59,300
but my dad

123
00:12:59,300 --> 00:13:01,130
did asked me secretly

124
00:13:01,130 --> 00:13:03,110
if I brought the scissors.

125
00:13:03,110 --> 00:13:07,350
Really? You really brought it along?

126
00:13:07,350 --> 00:13:08,690
What do you think?

127
00:13:08,700 --> 00:13:12,480
Impossible. You wouldn't bear to use it.

128
00:13:12,480 --> 00:13:15,660
My dad just gave me a big bag of things.

129
00:13:15,660 --> 00:13:18,990
What do you think they are for? Let's look.

130
00:13:23,590 --> 00:13:25,800
Computer keyboard.

131
00:13:25,820 --> 00:13:28,160
That's great.

132
00:13:31,930 --> 00:13:34,720
And also a pair of scissors.

133
00:13:34,720 --> 00:13:36,870
Give this to me.

134
00:13:44,870 --> 00:13:47,390
Do you still remember what I said?

135
00:13:48,900 --> 00:13:51,270
Every time you do something wrong,

136
00:13:52,450 --> 00:13:56,210
you have to be punished with 50 push-ups

137
00:13:56,210 --> 00:14:00,020
and kneel thirty minutes on the keyboard.

138
00:14:00,040 --> 00:14:03,920
And also sleep on the sofa for 18 days.

139
00:14:05,910 --> 00:14:07,470
You don't need a tool for push-ups.

140
00:14:07,470 --> 00:14:12,130
We already have the keyboard. The sofa is over there so...

141
00:14:12,130 --> 00:14:14,510
Does it count tonight?

142
00:14:14,510 --> 00:14:17,570
Tonight is our matrimonial night.

143
00:14:17,620 --> 00:14:20,360
There isn't anyone to mess up our night.

144
00:14:21,080 --> 00:14:22,810
There's only the both of us.

145
00:14:25,730 --> 00:14:29,270
Because it's between the two of us,

146
00:14:29,270 --> 00:14:31,860
you better consider this over carefully.

147
00:14:33,270 --> 00:14:35,740
We are already married.

148
00:14:35,740 --> 00:14:38,830
You don't need to be that calculative.

149
00:14:40,200 --> 00:14:43,440
Nowadays, the assets of the parents

150
00:14:43,440 --> 00:14:45,700
can be split.

151
00:14:45,740 --> 00:14:49,710
If this is the case, I should be calculative.

152
00:14:53,360 --> 00:14:56,800
The money that you owed me can be not counted.

153
00:14:59,370 --> 00:15:01,530
But

154
00:15:05,730 --> 00:15:08,110
this debt has to be counted.

155
00:15:10,990 --> 00:15:14,900
The chocolate order.

156
00:15:17,430 --> 00:15:19,570
I want to change products.

157
00:15:21,250 --> 00:15:24,000
The chocolate before was spoiled.

158
00:15:24,730 --> 00:15:26,950
After eating that,

159
00:15:28,080 --> 00:15:30,300
I was so upset.

160
00:15:31,950 --> 00:15:35,180
Hence, my request now

161
00:15:35,180 --> 00:15:38,130
I want a complete

162
00:15:38,130 --> 00:15:40,930
chocolate without any flaws.

163
00:15:49,780 --> 00:15:52,180
Don't worry.

164
00:15:52,250 --> 00:15:55,280
The Chocolate who is in front of you now,

165
00:15:55,330 --> 00:15:57,360
is guaranteed complete,

166
00:15:57,380 --> 00:15:59,420
without any flaws.

167
00:16:01,050 --> 00:16:04,400
- Really? 
- Yes.

168
00:16:04,430 --> 00:16:06,690
Although sometimes,

169
00:16:06,720 --> 00:16:10,060
it may include impurities,

170
00:16:10,080 --> 00:16:12,310
or make you angry,

171
00:16:13,310 --> 00:16:17,460
but I guarantee that I will not leave you again.

172
00:16:17,460 --> 00:16:19,700
I will stay by your side forever.

173
00:16:26,150 --> 00:16:28,360
I swear that.

174
00:16:32,750 --> 00:16:34,870
Okay.

175
00:16:34,910 --> 00:16:38,060
Looking at how sincere you are,

176
00:16:38,100 --> 00:16:40,350
I'll accept it.

177
00:16:42,670 --> 00:16:44,940
But...

178
00:16:46,310 --> 00:16:49,530
if you make me sad,

179
00:16:49,560 --> 00:16:52,790
I would be letting myself down if I don't punish you.

180
00:16:52,790 --> 00:16:54,720
Okay.

181
00:16:59,450 --> 00:17:02,840
If you want to punish me,

182
00:17:02,840 --> 00:17:05,660
just punish me.

183
00:17:19,900 --> 00:17:30,890
<i>Subtitles brought to you by The Love Chocolate Team@Viki</i>

184
00:17:41,920 --> 00:17:45,150
I would be letting myself down if I don't punish you.

185
00:17:45,150 --> 00:17:46,850
Okay.

186
00:17:51,890 --> 00:17:55,260
If you want to punish me,

187
00:17:55,260 --> 00:17:57,640
just punish me.

188
00:18:02,440 --> 00:18:05,850
Okay. Just do according to what I said.

189
00:18:05,860 --> 00:18:09,000
50 push-ups, kneel on the keyboard for 30 mins

190
00:18:09,000 --> 00:18:11,460
and sleep on the sofa for 18 days. Starting now.

191
00:18:11,460 --> 00:18:14,460
Can I not do it?

192
00:18:14,460 --> 00:18:16,800
Is there any other option?

193
00:18:18,300 --> 00:18:20,740
Yes.

194
00:18:20,790 --> 00:18:23,210
Then, you must

195
00:18:23,210 --> 00:18:26,140
devote your lifetime

196
00:18:26,140 --> 00:18:28,880
be with me

197
00:18:28,880 --> 00:18:31,560
and take care of me forever.

198
00:18:31,560 --> 00:18:33,850
Which option do you want?

199
00:18:36,630 --> 00:18:38,740
Of course, I choose this.

200
00:18:40,730 --> 00:18:45,800
What are you doing?

201
00:18:45,800 --> 00:18:47,780
Quieter. What if you wake up the other hotel guests?

202
00:18:47,780 --> 00:18:49,340
I'm on my honeymoon!

203
00:18:49,340 --> 00:18:52,050
Chocolate!

204
00:19:18,650 --> 00:19:20,520
Hong Xi Hui.

205
00:19:20,520 --> 00:19:22,890
Where is Boss?

206
00:19:25,090 --> 00:19:27,430
What are you doing?

207
00:19:27,430 --> 00:19:29,460
You have been acting strange lately?

208
00:19:29,460 --> 00:19:34,030
I didn't see you after the wedding is over. Where did you go?

209
00:19:34,030 --> 00:19:38,610
Dad. You know where Boss is, right?

210
00:19:39,590 --> 00:19:41,800
What are you talking about?

211
00:19:42,740 --> 00:19:44,470
You know that Boss didn't go overseas.

212
00:19:44,470 --> 00:19:48,020
You also know what he's going to do, right?

213
00:19:48,050 --> 00:19:50,510
How would I know? I don't know anything at all.

214
00:19:50,510 --> 00:19:52,020
I'm telling you.

215
00:19:52,030 --> 00:19:54,140
I'm not that close with him.

216
00:19:54,190 --> 00:19:57,950
Dad. Don't help Boss to hide matters from me, okay?

217
00:19:58,000 --> 00:19:59,900
I'm really very worried that something could happen to him.

218
00:19:59,900 --> 00:20:01,510
I have been looking for him the whole night.

219
00:20:01,510 --> 00:20:05,310
I really don't know where else can I find him

220
00:20:08,290 --> 00:20:10,110
What's going on?

221
00:20:10,110 --> 00:20:11,400
Mom!

222
00:20:11,400 --> 00:20:15,090
Xi Hui, be good. Don't cry.

223
00:20:16,130 --> 00:20:18,390
Dear. What's going on?

224
00:20:18,390 --> 00:20:22,380
Why is she crying so early in the morning?

225
00:20:22,380 --> 00:20:25,600
Don't cry. Tell me now.

226
00:20:25,600 --> 00:20:28,950
Aiyoh, dear.

227
00:20:29,000 --> 00:20:32,470
She... Okay, I will talk.

228
00:20:32,470 --> 00:20:37,250
I really only know a bit.

229
00:20:37,250 --> 00:20:40,850
Lu Zheng Ting told me that he and that

230
00:20:40,880 --> 00:20:42,450
police are going to join forces.

231
00:20:42,450 --> 00:20:46,900
He is going undercover to Hai Feng

232
00:20:46,910 --> 00:20:49,640
and together with the police,

233
00:20:49,640 --> 00:20:53,250
catch the bad guys.

234
00:20:53,250 --> 00:20:55,280
I knew it!

235
00:20:55,320 --> 00:20:58,360
Xi Hui!

236
00:20:58,390 --> 00:21:00,980
- Dear, be careful. 
- Don't bother about me.

237
00:21:01,030 --> 00:21:04,380
- Go after her now. 
- Okay.

238
00:21:04,380 --> 00:21:07,330
Xi Hui!

239
00:21:07,330 --> 00:21:09,150
This child...

240
00:21:09,150 --> 00:21:12,470
Xi Hui!

241
00:21:14,240 --> 00:21:16,780
Dear. Go after her. What if she goes missing?

242
00:21:16,780 --> 00:21:18,780
What should we do?

243
00:21:18,780 --> 00:21:21,570
Don't worry. Now that she knows he didn't go overseas,

244
00:21:21,570 --> 00:21:23,310
she will definitely go and look for him.

245
00:21:23,310 --> 00:21:25,890
She will be back soon.

246
00:21:27,680 --> 00:21:28,660
Are you okay?

247
00:21:28,660 --> 00:21:30,690
I'm fine.

248
00:21:41,180 --> 00:21:42,530
You...

249
00:21:43,530 --> 00:21:46,160
I haven't even open my mouth, why are you so noisy?

250
00:21:46,200 --> 00:21:48,180
Yes, Boss.

251
00:21:48,180 --> 00:21:51,560
Hong Xi Hui. You miss me that much?

252
00:21:51,590 --> 00:21:52,590
If not...

253
00:21:52,590 --> 00:21:55,550
Cut your crap. Take me to see Boss!

254
00:21:56,990 --> 00:22:00,140
You are getting even more arrogant.

255
00:22:01,710 --> 00:22:03,800
Why don't you do this? Leave Lu Zheng Ting,

256
00:22:03,800 --> 00:22:06,590
come to me and assist me?

257
00:22:06,610 --> 00:22:08,980
Take me to him.

258
00:22:25,100 --> 00:22:27,790
What I want you to do is very simple.

259
00:22:28,400 --> 00:22:31,340
Tomorrow the boat will come from this direction

260
00:22:31,340 --> 00:22:33,850
and throw the bag later into the sea.

261
00:22:34,600 --> 00:22:36,810
Go into the water and help me to retrieve the bag.

262
00:22:36,810 --> 00:22:39,220
There will be two person to take over.

263
00:22:40,120 --> 00:22:41,430
That's it?

264
00:22:41,430 --> 00:22:43,800
Don't be so boring, okay?

265
00:22:43,810 --> 00:22:46,220
The fish this time is very fat.

266
00:22:46,220 --> 00:22:49,900
I can travel the world after this.

267
00:22:49,900 --> 00:22:51,900
The both us can not keep in contact after that.

268
00:22:51,900 --> 00:22:54,590
Hopefully you will keep your word this time.

269
00:22:54,590 --> 00:22:57,070
Of course.

270
00:22:57,070 --> 00:23:00,450
Oh yes, to express my kindness

271
00:23:00,490 --> 00:23:02,940
I brought someone here.

272
00:23:47,190 --> 00:23:48,950
to express my kindness

273
00:23:48,950 --> 00:23:51,520
I brought someone here.

274
00:24:06,780 --> 00:24:08,280
Hai Feng. You...

275
00:24:08,280 --> 00:24:09,680
It's not me.

276
00:24:09,710 --> 00:24:13,150
She came here voluntarily.

277
00:24:20,690 --> 00:24:21,570
Xi Hui.

278
00:24:21,570 --> 00:24:25,810
I brought her here with sincerity with any damage done to her.

279
00:24:26,680 --> 00:24:29,470
What happens next depends on your performance.

280
00:24:32,980 --> 00:24:35,290
Your girlfriend really has guts.

281
00:24:35,290 --> 00:24:37,120
If you don't want her, I will.

282
00:24:37,120 --> 00:24:38,780
Zhao Hai Feng...

283
00:24:38,780 --> 00:24:40,080
Okay, okay, okay.

284
00:24:40,080 --> 00:24:43,100
I'm only just joking.

285
00:24:53,400 --> 00:24:57,460
Do you know how it feels to have someone suddenly disappear?

286
00:24:57,460 --> 00:25:01,780
Why do you force me to feel this way again and again?

287
00:25:01,780 --> 00:25:04,550
I have no news of you and I can't find you.

288
00:25:04,550 --> 00:25:07,310
You cause to be frantic and scared.

289
00:25:07,330 --> 00:25:09,130
I didn't dare to sleep the whole night!

290
00:25:09,130 --> 00:25:11,710
I have been looking and waiting for you.

291
00:25:11,710 --> 00:25:14,700
I waited till I was scared.

292
00:25:15,600 --> 00:25:18,790
Sorry, Xi Hui.

293
00:25:37,860 --> 00:25:39,650
Xi Hui

294
00:25:52,350 --> 00:25:57,080
I really have no idea what other ways to keep you by my side.

295
00:25:58,290 --> 00:26:00,950
I beg you not leave me again okay?

296
00:26:00,950 --> 00:26:03,440
Stay with me.

297
00:26:30,040 --> 00:26:32,720
The food will be here soon.

298
00:26:40,780 --> 00:26:42,400
So scrumptious.

299
00:26:42,410 --> 00:26:45,990
Did you wonder why there is a kitchen in this suite?

300
00:26:46,040 --> 00:26:48,180
You personally cooked this?

301
00:26:48,210 --> 00:26:50,790
- If not, then. 
- Really? Is it cooked all the way?

302
00:26:50,790 --> 00:26:52,340
Please.

303
00:26:52,340 --> 00:26:55,930
I used a small fire, added it with my fiery passion

304
00:26:55,930 --> 00:26:57,330
and cooked it.

305
00:26:57,330 --> 00:26:58,610
I guarantee that it's cooked

306
00:26:58,650 --> 00:27:01,380
and it's filled with sweetness(love).

307
00:27:01,380 --> 00:27:03,940
It will definitely be very delicious.

308
00:27:10,230 --> 00:27:11,010
What?

309
00:27:11,010 --> 00:27:13,560
Since I did so much for you,

310
00:27:13,560 --> 00:27:16,540
give me a kiss, quick.

311
00:27:21,660 --> 00:27:23,900
Come and give me a kiss.

312
00:27:23,900 --> 00:27:27,600
Stop fooling around. Let's eat.

313
00:27:34,700 --> 00:27:36,580
There is no wine opener.

314
00:27:36,580 --> 00:27:38,200
I seem to have one in my bag.

315
00:27:38,200 --> 00:27:40,850
Thanks.

316
00:27:48,330 --> 00:27:50,220
What's wrong?

317
00:27:52,570 --> 00:27:56,100
Nothing. I just wanted to say that

318
00:27:56,810 --> 00:27:59,200
this plane ticket is for one.

319
00:27:59,240 --> 00:28:01,310
This plane ticket, giving it to someone

320
00:28:01,360 --> 00:28:03,450
or sell it online

321
00:28:03,490 --> 00:28:05,140
is hard.

322
00:28:05,140 --> 00:28:06,900
Why are you selling it?

323
00:28:06,930 --> 00:28:09,220
Use it for yourself.

324
00:28:09,220 --> 00:28:13,060
What are you talking about? We are already married.

325
00:28:13,780 --> 00:28:16,070
I'm Mrs. Fang now.

326
00:28:16,110 --> 00:28:19,760
I have to take care of my husband.

327
00:28:19,760 --> 00:28:21,240
Xi En.

328
00:28:21,240 --> 00:28:24,600
I'm very happy that you married me.

329
00:28:24,600 --> 00:28:27,780
I'm not going to plan your future.

330
00:28:27,800 --> 00:28:30,600
You finally got this opportunity.

331
00:28:30,600 --> 00:28:32,160
You should treasure this

332
00:28:32,160 --> 00:28:34,450
and use it.

333
00:28:35,190 --> 00:28:38,070
But I don't want to be apart from you.

334
00:28:38,770 --> 00:28:41,600
I didn't say that we will be apart.

335
00:28:42,420 --> 00:28:45,490
When you go to Paris to study how to make chocolates,

336
00:28:45,490 --> 00:28:46,870
I will go with you.

337
00:28:46,870 --> 00:28:48,620
Do you mean it?

338
00:28:48,620 --> 00:28:49,760
Don't forget that

339
00:28:49,760 --> 00:28:53,390
both of us originally wanted to take part in the Chocolate competition together.

340
00:28:53,390 --> 00:28:56,120
We both shared the same dream.

341
00:28:59,970 --> 00:29:00,980
What?

342
00:29:00,980 --> 00:29:03,070
I can give it to you now.

343
00:29:04,180 --> 00:29:06,070
What?

344
00:29:25,120 --> 00:29:26,670
Your eyes are very swollen.

345
00:29:26,700 --> 00:29:28,280
Do you need an ice pack to bring it down?

346
00:29:28,280 --> 00:29:30,780
No.

347
00:29:30,780 --> 00:29:32,870
Do I look ugly?

348
00:29:34,530 --> 00:29:36,390
I saw it when you looked even terrible.

349
00:29:36,390 --> 00:29:38,960
For example,

350
00:29:38,960 --> 00:29:41,030
your ugly eating looks.

351
00:29:41,030 --> 00:29:43,740
I was careless.

352
00:29:50,780 --> 00:29:52,490
What's wrong?

353
00:29:52,490 --> 00:29:54,980
Nothing.

354
00:29:58,310 --> 00:30:01,680
This is the second time that we are having a meal together.

355
00:30:01,700 --> 00:30:04,930
It's the first time.

356
00:30:04,940 --> 00:30:08,280
This is our first time that

357
00:30:08,330 --> 00:30:10,450
we are having a meal together.

358
00:30:11,270 --> 00:30:15,310
We will have many such opportunities in the future, right?

359
00:30:16,910 --> 00:30:19,620
Yes. there will be.

360
00:30:19,620 --> 00:30:21,530
Here.

361
00:30:37,820 --> 00:30:39,260
What?

362
00:30:39,260 --> 00:30:41,740
You are really going to Paris?

363
00:30:42,540 --> 00:30:46,230
Dad, what's wrong? Is Paris very far away?

364
00:30:46,230 --> 00:30:49,760
Of course, it's very far away. It's on the other side of the globe.

365
00:30:50,970 --> 00:30:52,480
Dad.

366
00:30:52,530 --> 00:30:54,640
This is a hard-to-come-by opportunity for me.

367
00:30:54,640 --> 00:30:57,870
It would be a pity if I don't go.

368
00:30:57,870 --> 00:31:00,920
You are right but...

369
00:31:00,920 --> 00:31:03,530
it's for two years.

370
00:31:06,430 --> 00:31:08,200
Don't worry.

371
00:31:08,200 --> 00:31:09,990
Chocolate will be going with me.

372
00:31:09,990 --> 00:31:12,590
He will take care of me.

373
00:31:12,590 --> 00:31:15,700
Of course, he has to take care of you. He's your husband. If he doesn't take care of you,

374
00:31:15,700 --> 00:31:17,940
who would?

375
00:31:20,020 --> 00:31:22,730
Chocolate will be with her.

376
00:31:22,730 --> 00:31:24,210
What are you worried about?

377
00:31:24,210 --> 00:31:26,400
You didn't hear what I just said.

378
00:31:26,420 --> 00:31:28,070
It's for two years.

379
00:31:28,120 --> 00:31:32,730
I won't be able to see my Princess for two years.

380
00:31:32,730 --> 00:31:35,580
I can't bear to do that.

381
00:31:37,860 --> 00:31:41,000
Dad, let me tell you.

382
00:31:41,000 --> 00:31:42,400
You don't have to worry.

383
00:31:42,450 --> 00:31:44,580
Technology is very advanced now.

384
00:31:44,580 --> 00:31:46,080
There is video chat.

385
00:31:46,100 --> 00:31:48,400
We can see each other and talk every day.

386
00:31:48,420 --> 00:31:49,730
It's not like I don't know.

387
00:31:49,730 --> 00:31:52,860
But it doesn't feel real.

388
00:31:54,970 --> 00:31:56,930
Xi En and Chocolate

389
00:31:56,960 --> 00:31:59,620
are also working hard for their future.

390
00:31:59,660 --> 00:32:01,760
As parents,

391
00:32:01,760 --> 00:32:03,810
we should support them.

392
00:32:04,770 --> 00:32:06,730
Dad, it's okay.

393
00:32:06,730 --> 00:32:08,530
I will keep you company.

394
00:32:08,580 --> 00:32:12,050
Haru, too. Isn't that so, Haru?

395
00:32:13,040 --> 00:32:15,770
Okay, I know.

396
00:32:15,790 --> 00:32:18,180
This can't be helped.

397
00:32:18,200 --> 00:32:20,300
But Xi En,

398
00:32:20,300 --> 00:32:23,120
You must tell Chocolate

399
00:32:23,120 --> 00:32:25,690
that I said

400
00:32:25,740 --> 00:32:27,650
he must take good care of you.

401
00:32:27,650 --> 00:32:29,980
He must not let you suffer at all.

402
00:32:30,020 --> 00:32:32,110
If not,

403
00:32:32,110 --> 00:32:36,420
tell him that I'm sharpening the scissor every day.

404
00:32:36,420 --> 00:32:38,950
When he comes back, I will cut him.

405
00:32:38,950 --> 00:32:40,490
Okay.

406
00:32:44,660 --> 00:32:46,340
Answer it.

407
00:32:49,130 --> 00:32:50,900
Hello.

408
00:32:50,940 --> 00:32:53,250
Father Hong, it's me.

409
00:32:55,020 --> 00:32:58,780
Mom. I'm not going home today.

410
00:33:01,120 --> 00:33:02,560
Don't worry.

411
00:33:02,610 --> 00:33:05,110
Nothing will happen to Xi Hui.

412
00:33:05,120 --> 00:33:06,320
Okay.

413
00:33:06,320 --> 00:33:07,750
I know.

414
00:33:07,790 --> 00:33:10,440
I won't let Boss do any dangerous tasks.

415
00:33:10,490 --> 00:33:13,600
I will be careful. Thanks, Father Hong.

416
00:33:13,600 --> 00:33:16,000
Okay, I know.

417
00:33:22,030 --> 00:33:24,040
- Do you want juice? 
 - I'll sleep alone.

418
00:33:35,960 --> 00:33:37,450
Dad.

419
00:33:37,500 --> 00:33:39,400
Mom.

420
00:33:39,400 --> 00:33:41,920
What's wrong with you both?

421
00:33:42,560 --> 00:33:45,330
Nothing.

422
00:33:45,330 --> 00:33:47,500
After talking on the phone,

423
00:33:47,530 --> 00:33:50,090
your expressions turned strange. What's wrong?

424
00:33:50,090 --> 00:33:52,160
Did I?

425
00:33:55,920 --> 00:33:58,940
I already told you that it's nothing.

426
00:34:12,990 --> 00:34:14,990
I have cleared the table.

427
00:34:15,010 --> 00:34:16,990
Okay, take a break first.

428
00:34:16,990 --> 00:34:19,050
I will do the dishes.

429
00:34:24,150 --> 00:34:25,950
What's wrong?

430
00:34:25,980 --> 00:34:28,830
I'm afraid that you will disappear.

431
00:34:30,430 --> 00:34:32,630
You are going to watch me doing the dishes?

432
00:34:33,430 --> 00:34:34,820
Why don't I do the dishes...

433
00:34:34,820 --> 00:34:37,240
No need. It's okay.

434
00:34:37,240 --> 00:34:39,320
Don't make it so hard on yourself.

435
00:34:39,360 --> 00:34:42,280
Just treat it as an atonement for my sins

436
00:34:45,870 --> 00:34:48,280
Take a break. I will be done soon.

437
00:35:05,050 --> 00:35:06,660
What's wrong?

438
00:35:06,660 --> 00:35:09,560
I'm just watching over you and not even hindering you.

439
00:35:55,300 --> 00:35:58,000
Jia Hua. You just got married to Xi En

440
00:35:58,000 --> 00:36:02,500
and you are going to live in Paris now. How can I bear to see you go?

441
00:36:02,500 --> 00:36:09,200
Granny. I can't bear to leave too but if we don't go now,
 I'm afraid that Xi En won't get used to the environment there.

442
00:36:11,120 --> 00:36:13,170
Mom.

443
00:36:13,200 --> 00:36:16,100
It's good that they have their own opinions.

444
00:36:16,100 --> 00:36:17,300
But...

445
00:36:17,300 --> 00:36:20,800
Do you have any plans when you go with Xi En?

446
00:36:20,830 --> 00:36:24,750
I'm going to study chocolates with Xi En and at the same time, study retail management.

447
00:36:24,800 --> 00:36:26,600
Retail management?

448
00:36:27,500 --> 00:36:32,100
Are you saying that you will join our company when you are back?

449
00:36:32,100 --> 00:36:34,700
Dad. I only just want to learn new things.

450
00:36:34,700 --> 00:36:37,200
Maybe see if there's any business oportunity

451
00:36:37,200 --> 00:36:40,100
and let Jia Rui consider.

452
00:36:40,160 --> 00:36:44,290
You don't have to worry about Jia Rui and the company.

453
00:36:45,200 --> 00:36:47,120
Okay.

454
00:36:47,160 --> 00:36:51,250
I won't force you again. Go and do whatever you want to do.

455
00:36:51,300 --> 00:36:53,300
Jia Hua.

456
00:36:53,300 --> 00:36:56,400
When you are living overseas, there are many things that are inconvenient for you to buy.

457
00:36:56,400 --> 00:37:00,900
I have prepared some stuff for you to bring over.

458
00:37:00,950 --> 00:37:02,540
Remember that

459
00:37:02,600 --> 00:37:07,400
the both of you must be very careful when you are out there.

460
00:37:07,400 --> 00:37:11,500
You must take special care of Xi En.

461
00:37:12,800 --> 00:37:15,000
Also,

462
00:37:15,000 --> 00:37:17,700
if you lack anything wherever you go,

463
00:37:17,700 --> 00:37:23,100
call home immediately. I will send them over.

464
00:37:24,400 --> 00:37:26,200
I know.

465
00:37:27,400 --> 00:37:29,200
Thanks, mom.

466
00:37:38,200 --> 00:37:40,400
What did you call me?

467
00:37:42,000 --> 00:37:44,300
Pei Long.

468
00:37:44,300 --> 00:37:46,500
Did you hear that?

469
00:37:48,000 --> 00:37:51,100
Jia Hua called me mom.

470
00:37:54,600 --> 00:37:56,500
I'm sorry

471
00:37:57,700 --> 00:38:00,040
for being immature.

472
00:38:00,900 --> 00:38:03,500
In the past, I...

473
00:38:03,500 --> 00:38:05,800
thought that by doing this

474
00:38:05,800 --> 00:38:07,900
it would be better for everyone.

475
00:38:08,700 --> 00:38:10,900
I was wrong.

476
00:38:13,400 --> 00:38:16,600
Please forgive me for giving you a hard time.

477
00:38:31,800 --> 00:38:36,300
Don't cry anymore. You won't look pretty when you cry.

478
00:38:40,300 --> 00:38:44,400
Jia Rui said this too.

479
00:39:21,200 --> 00:39:23,620
You are going overseas.

480
00:39:25,100 --> 00:39:29,700
What about the chocolates for my 13 year old Jia Hua?

481
00:39:30,300 --> 00:39:32,500
I have already sent it to him.

482
00:39:32,500 --> 00:39:34,500
Really?

483
00:39:35,370 --> 00:39:39,540
Was he happy when you gave it to him?

484
00:39:39,600 --> 00:39:42,100
Very happy.

485
00:39:44,100 --> 00:39:48,400
Furthermore, I also met his girlfriend.

486
00:39:48,400 --> 00:39:50,800
Girlfriend?

487
00:39:50,800 --> 00:39:53,500
My Jia Hua is only 13 years old.

488
00:39:53,500 --> 00:39:55,300
He already has a girlfriend?

489
00:39:55,300 --> 00:39:58,900
Then, what does she looks like?

490
00:40:08,540 --> 00:40:11,790
- She looks exactly like Xi En. 
- Xi En?

491
00:40:13,080 --> 00:40:15,250
She must be pretty.

492
00:40:15,300 --> 00:40:17,300
Very pretty.

493
00:40:18,000 --> 00:40:20,200
A good person

494
00:40:20,200 --> 00:40:22,300
but she's fierce.

495
00:40:23,900 --> 00:40:26,600
Would Jia Hua be bullied by her?

496
00:40:28,000 --> 00:40:30,500
Don't worry. She won't do that.

497
00:40:30,500 --> 00:40:34,800
From the look of it, that girl really likes him.

498
00:40:35,600 --> 00:40:38,600
Your son is very good looking.

499
00:40:39,290 --> 00:40:41,500
Of course.

500
00:40:41,500 --> 00:40:43,900
That's good then.

501
00:40:50,100 --> 00:40:54,800
Master, I have to trouble you with the shop and Sister Hao.

502
00:40:54,800 --> 00:40:59,100
Don't worry. Whatever you want to tell me, Chocolate has already told me.

503
00:40:59,100 --> 00:41:01,200
Continue to study well.

504
00:41:01,200 --> 00:41:05,800
Jia Yi and I will be here waiting for you both to come home.

505
00:41:09,100 --> 00:41:12,800
Thank for sending the chocolates to Jia Hua.

506
00:41:13,700 --> 00:41:16,080
I will definitely think of you.

507
00:41:18,600 --> 00:41:20,800
I will think of you too.

508
00:41:24,290 --> 00:41:26,630
Can you hug me for a while?

509
00:41:35,450 --> 00:41:52,250
<i>Subtitles brought to you by the Love Chocolate Team @Viki</i>

510
00:41:54,900 --> 00:41:56,500
Mom.

511
00:41:57,620 --> 00:41:59,660
You must take good care of yourself.

512
00:42:48,500 --> 00:42:50,100
Here's your fruit juice.

513
00:42:50,100 --> 00:42:51,900
I want to drink coffee.

514
00:42:52,900 --> 00:42:54,700
Aren't you afraid that you won't be able to sleep?

515
00:42:54,700 --> 00:42:56,200
I'm not afraid.

516
00:42:56,200 --> 00:43:00,200
It's because I don't want to sleep in case you leave when I sleep.

517
00:43:02,900 --> 00:43:05,600
Not every one can keep awake by drinking coffee.

518
00:43:06,600 --> 00:43:08,200
It's okay for me.

519
00:43:08,200 --> 00:43:13,200
I'm very sensitive to coffee. Just one cup and I can stay awake for the whole night.

520
00:43:13,200 --> 00:43:15,200
Don't worry, I won't.

521
00:43:15,200 --> 00:43:20,100
You always say this. Saying nothing will happen to me.

522
00:43:20,100 --> 00:43:23,500
In the end something always happens.

523
00:43:33,100 --> 00:43:36,200
- Okay.Watch television with me.
- Okay.

524
00:45:14,400 --> 00:45:17,200
<i>Sorry, Xi Hui. </i>

525
00:45:17,200 --> 00:45:19,300
<i>This is the last time. </i>

526
00:45:20,200 --> 00:45:25,500
<i>I don't want you to get involved in
 a world not for you just because of me. </i>

527
00:45:25,500 --> 00:45:27,700
<i>Just say that I'm selfish. </i>

528
00:45:47,660 --> 00:45:49,660
Boss!

529
00:45:54,750 --> 00:45:57,660
All the stuff that you need is already here.

530
00:45:57,700 --> 00:46:00,000
And the things I need are in the ocean.

531
00:46:00,000 --> 00:46:03,000
Retrieve that for me and we will be squared off.

532
00:46:06,600 --> 00:46:09,800
Zhao Hai Feng conned Boss into going into the water.

533
00:46:09,800 --> 00:46:14,200
I'm afraid that his life will be in danger. What should we go?

534
00:46:14,900 --> 00:46:17,000
Brother, where are you?

535
00:46:17,000 --> 00:46:19,800
Brother!

536
00:46:25,400 --> 00:46:45,600
<i>Subtitles brought to you by the Love Chocolate Team @Viki</i>

