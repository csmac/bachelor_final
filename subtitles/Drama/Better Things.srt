1
00:01:05,860 --> 00:01:07,339
<i>"Nothing.</i>

2
00:01:15,940 --> 00:01:17,532
<i>She supposed.</i>

3
00:01:20,380 --> 00:01:22,257
<i>This was real life.</i>

4
00:01:25,140 --> 00:01:27,017
<i>And real life was difficult.</i>

5
00:01:30,580 --> 00:01:32,457
<i>At best.</i>

6
00:01:38,580 --> 00:01:40,457
<i>Hadn't she learnt that...</i>

7
00:01:45,100 --> 00:01:46,977
<i>...when she was a little girl?</i>

8
00:01:50,180 --> 00:01:52,978
<i>Why did she think falling in love...</i>

9
00:01:55,820 --> 00:01:57,697
<i>...would make it easier?</i>

10
00:01:59,380 --> 00:02:01,257
<i>Why did she think...</i>

11
00:02:04,380 --> 00:02:05,449
<i>...falling in love...</i>

12
00:02:07,380 --> 00:02:10,099
<i>...would make it any... easier?"</i>

13
00:02:51,340 --> 00:02:54,571
<i>� ...in ancient time �</i>

14
00:02:55,380 --> 00:03:00,613
<i>� Walk upon England's mountains green? �</i>

15
00:03:01,020 --> 00:03:06,538
<i>� And was the holy Lamb of God... �</i>

16
00:03:06,980 --> 00:03:12,816
<i>� On England's pleasant pastures seen? �</i>

17
00:03:13,260 --> 00:03:18,414
<i>� And did the countenance divine �</i>

18
00:03:19,100 --> 00:03:24,493
<i>� Shine forth upon our clouded hills? �</i>

19
00:03:25,140 --> 00:03:30,737
<i>� And was Jerusalem builded here �</i>

20
00:03:31,500 --> 00:03:38,053
<i>� Among those dark satanic mills? �</i>

21
00:03:39,420 --> 00:03:45,290
� Bring me my bow of burning gold! �

22
00:03:45,780 --> 00:03:51,059
� Bring me my arrows of desire! �

23
00:03:51,460 --> 00:03:57,137
� Bring me my spear: O clouds unfold! �

24
00:03:57,660 --> 00:04:03,530
� Bring me my chariots of fire! �

25
00:04:03,940 --> 00:04:09,617
� I will not cease from mental fight �

26
00:04:10,100 --> 00:04:15,572
� Nor shall my sword sleep in my hand �

27
00:04:16,180 --> 00:04:22,255
� Till we have built Jerusalem �

28
00:04:22,700 --> 00:04:30,175
� In England's green and pleasant land �

29
00:05:13,820 --> 00:05:15,856
What were you talking to Tess's mum about?

30
00:05:16,860 --> 00:05:19,055
She just invited us back to the reception.

31
00:05:28,460 --> 00:05:30,416
And she wanted to know where Rob was.

32
00:05:42,180 --> 00:05:45,013
<i>...and he couldn't explain why,</i>

33
00:05:45,100 --> 00:05:46,772
<i>I couldn't explain why,</i>

34
00:05:46,860 --> 00:05:48,940
<i>I suppose my dad couldn't explain why.</i>

35
00:05:48,940 --> 00:05:51,295
<i>And my grandfather couldn't explain why...</i>

36
00:05:51,420 --> 00:05:53,297
<i>except beg forgiveness.</i>

37
00:05:56,060 --> 00:05:58,449
<i>Tradition doesn't do anything for anybody.</i>

38
00:05:58,540 --> 00:06:00,820
<i>Tradition doesn't put bread on the table.</i>

39
00:06:00,820 --> 00:06:03,698
<i>It doesn't pay the bills,</i>
<i>doesn't pay the mortgage...</i>

40
00:06:21,500 --> 00:06:23,377
Where's your mum and dad gone?

41
00:06:25,460 --> 00:06:27,337
They're on holiday.

42
00:07:32,060 --> 00:07:33,891
I've got to go and see the nurse.

43
00:07:33,980 --> 00:07:35,740
Be back in a bit.

44
00:07:35,740 --> 00:07:37,458
And then we'll finish packing.

45
00:07:37,580 --> 00:07:39,457
OK, Mum?

46
00:08:09,940 --> 00:08:11,817
Are you all right there, Mum?

47
00:08:14,300 --> 00:08:15,779
Steady.

48
00:08:17,180 --> 00:08:19,057
All right?

49
00:08:30,980 --> 00:08:32,493
Gail?

50
00:08:34,300 --> 00:08:36,177
Gail?

51
00:08:51,060 --> 00:08:54,132
Nice to have you back, Nan.
Your bed's been made up for you.

52
00:08:55,180 --> 00:08:58,217
Just pop your bag down there,
I'll bring it in a bit later.

53
00:09:27,740 --> 00:09:29,298
Is it him again, Rach?

54
00:09:30,860 --> 00:09:31,860
Yeah.

55
00:09:31,860 --> 00:09:34,010
Fuck, does Larry always phone this much?

56
00:09:34,740 --> 00:09:36,093
Sometimes.

57
00:09:36,180 --> 00:09:38,860
- He knows you're seeing someone else?
- No.

58
00:09:38,860 --> 00:09:40,980
Maybe you should tell him.

59
00:09:40,980 --> 00:09:42,140
Why?

60
00:09:42,140 --> 00:09:45,337
You know,
just give him a little excuse to hate you.

61
00:09:45,460 --> 00:09:47,020
That'd get rid of him.

62
00:09:47,020 --> 00:09:49,340
- You're gonna have to go.
- Why?

63
00:09:49,340 --> 00:09:50,932
Gonna fuck him?

64
00:10:10,020 --> 00:10:11,897
Who you been trying to phone?

65
00:10:13,100 --> 00:10:14,460
No-one.

66
00:11:06,100 --> 00:11:07,977
How's your Granddad?

67
00:11:08,900 --> 00:11:11,900
He's all right. Coming out soon.

68
00:11:11,900 --> 00:11:13,731
So he's better now, then?

69
00:11:13,820 --> 00:11:15,780
I don't know if he's fully better, like.

70
00:11:15,780 --> 00:11:17,260
I've got to go and pick him up.

71
00:11:17,260 --> 00:11:19,137
Gran asked me.

72
00:11:19,860 --> 00:11:22,010
I had to say yes - I felt a bit guilty, else.

73
00:11:35,300 --> 00:11:38,258
Yeah, I ain't been to see either of them
since he went in.

74
00:11:39,740 --> 00:11:41,935
Yeah, picking him up is the least I can do.

75
00:11:42,900 --> 00:11:44,777
It gets me away from work and all.

76
00:11:52,540 --> 00:11:54,417
How long have they been married?

77
00:11:55,140 --> 00:11:57,017
God. Fuck knows.

78
00:11:57,860 --> 00:11:59,691
Along time.

79
00:14:47,100 --> 00:14:48,852
I've missed you.

80
00:15:15,580 --> 00:15:16,695
Good.

81
00:15:46,740 --> 00:15:48,617
Nice game, Joel.

82
00:16:59,660 --> 00:17:01,537
Saw Tess's mum the other day.

83
00:17:04,580 --> 00:17:06,457
Did she say anything to you?

84
00:17:09,500 --> 00:17:11,377
She didn't see me.

85
00:17:12,860 --> 00:17:14,737
I just sort of hid.

86
00:17:19,580 --> 00:17:21,298
What for?

87
00:17:24,740 --> 00:17:27,652
I didn't want her asking me
why I wasn't at the funeral.

88
00:17:32,140 --> 00:17:34,017
Why didn't you come, Rob?

89
00:17:38,820 --> 00:17:40,936
Just... couldn't face it.

90
00:17:50,140 --> 00:17:52,017
I'd never inject.

91
00:18:13,740 --> 00:18:15,940
How's Rob?

92
00:18:15,940 --> 00:18:17,817
Don't know.

93
00:18:20,540 --> 00:18:22,417
Haven't you seen him?

94
00:18:24,340 --> 00:18:25,773
No.

95
00:18:27,260 --> 00:18:28,932
How was he at the funeral?

96
00:18:32,180 --> 00:18:33,693
He wasn't there.

97
00:18:35,660 --> 00:18:37,139
You didn't tell me.

98
00:18:38,780 --> 00:18:40,452
You didn't go.

99
00:18:42,140 --> 00:18:44,100
Did he love her?

100
00:18:44,100 --> 00:18:45,977
Yeah, he did.

101
00:18:48,060 --> 00:18:49,937
Why didn't you come?

102
00:18:53,300 --> 00:18:54,858
You know why.

103
00:18:55,580 --> 00:18:57,457
I only come back for you now.

104
00:19:03,940 --> 00:19:05,498
Please don't look at them.

105
00:19:12,460 --> 00:19:14,132
I don't mind them.

106
00:19:15,420 --> 00:19:16,819
I do.

107
00:19:21,900 --> 00:19:23,820
Do you think Rob's injecting now?

108
00:19:23,820 --> 00:19:25,697
I don't know.

109
00:19:28,500 --> 00:19:30,377
You know he must be...

110
00:19:31,500 --> 00:19:33,420
...if Tess was.

111
00:19:33,420 --> 00:19:34,489
Maybe.

112
00:19:36,140 --> 00:19:37,493
Promise me.

113
00:19:38,460 --> 00:19:40,098
I know.

114
00:19:40,980 --> 00:19:42,700
I wouldn't.

115
00:19:42,700 --> 00:19:44,577
I saw it with you.

116
00:19:45,580 --> 00:19:47,457
You do love me, don't you?

117
00:19:50,860 --> 00:19:52,373
Yeah.

118
00:20:50,580 --> 00:20:53,180
Are you sure you're not up to it?

119
00:20:53,180 --> 00:20:55,340
- Look, I'm sure.
- Why ever not?

120
00:20:55,340 --> 00:20:57,300
I just don't feel like it today, OK?

121
00:20:57,300 --> 00:20:59,370
OK, fine, if that's how you feel.

122
00:21:00,100 --> 00:21:02,534
Have they got anything at the library for you?

123
00:21:02,620 --> 00:21:05,820
I think there's a few you didn't pick up
last time. Just get those.

124
00:21:05,820 --> 00:21:07,580
And can you get me some fags?

125
00:21:07,580 --> 00:21:09,260
- Have you got any money?
- No.

126
00:21:09,260 --> 00:21:12,297
Well, how do I pay for them?
You still owe me for last week.

127
00:21:12,380 --> 00:21:15,019
Can you just take it out of my disability benefit?

128
00:21:15,140 --> 00:21:18,052
OK, I need to cash that anyway.

129
00:21:18,860 --> 00:21:20,339
Thanks.

130
00:21:21,340 --> 00:21:23,296
I won't be long, just a few hours or so.

131
00:21:23,380 --> 00:21:25,257
Fine.

132
00:21:30,860 --> 00:21:32,820
Can you look after Nan?

133
00:21:32,820 --> 00:21:35,937
You can make her a cup of tea.
Don't keep making her loads.

134
00:21:36,060 --> 00:21:37,900
I won't.

135
00:21:37,900 --> 00:21:39,777
- Bye, then, love.
- Bye, Mum.

136
00:23:48,060 --> 00:23:50,290
70, 80, 85...

137
00:23:50,380 --> 00:23:52,300
88...

138
00:23:52,300 --> 00:23:53,813
and 64.

139
00:28:41,060 --> 00:28:42,175
Rob!

140
00:30:03,380 --> 00:30:05,257
Hello, Granddad.

141
00:30:08,020 --> 00:30:09,897
Thank you for bringing him home.

142
00:31:00,820 --> 00:31:02,697
And I worry.

143
00:31:04,620 --> 00:31:06,497
About me?

144
00:31:07,980 --> 00:31:09,857
And about us.

145
00:31:13,020 --> 00:31:14,897
About you being here without me.

146
00:31:19,420 --> 00:31:21,297
You know I don't do what they do.

147
00:31:21,980 --> 00:31:24,050
Never lie to me, would you?

148
00:31:25,060 --> 00:31:26,493
No.

149
00:31:34,220 --> 00:31:36,893
Please don't ever do anything
to make me leave you.

150
00:31:38,380 --> 00:31:40,257
I wouldn't.

151
00:33:24,980 --> 00:33:27,050
<i>So, are we still seeing each other?</i>

152
00:33:31,380 --> 00:33:32,893
<i>I dunno.</i>

153
00:33:38,820 --> 00:33:40,697
Are you coming to bed?

154
00:33:43,060 --> 00:33:44,095
No.

155
00:33:46,060 --> 00:33:48,260
<i>Wake up.</i>

156
00:33:48,260 --> 00:33:49,739
Not yet.

157
00:33:59,700 --> 00:34:01,577
<i>I'll see you in a minute, OK?</i>

158
00:34:29,700 --> 00:34:30,610
Gail!

159
00:34:33,900 --> 00:34:35,777
It's gone midday, love.

160
00:34:36,980 --> 00:34:38,857
Your lady will be here soon.

161
00:34:40,620 --> 00:34:42,497
Please don't make her wait again.

162
00:34:43,940 --> 00:34:45,453
Can you hear me, Gail?

163
00:35:12,780 --> 00:35:15,692
And the last time you were outside?
What was that like?

164
00:35:18,060 --> 00:35:19,937
I can't explain.

165
00:35:23,020 --> 00:35:24,897
It just felt like...

166
00:35:26,020 --> 00:35:29,057
...I couldn't make any connections
between my thoughts.

167
00:35:32,620 --> 00:35:36,408
Could you explain a little bit more about
your thoughts and what they were like?

168
00:35:38,380 --> 00:35:40,140
I would feel like my body was falling apart...

169
00:35:40,140 --> 00:35:40,856
I would feel like my body was falling apart...

170
00:35:42,860 --> 00:35:46,216
...just... dissolving...

171
00:35:50,060 --> 00:35:53,132
...like I was going to just disappear.

172
00:36:00,340 --> 00:36:02,217
But the new prescription's better?

173
00:36:06,340 --> 00:36:08,535
They seem OK, but they make me sleep more.

174
00:36:09,420 --> 00:36:11,180
Well, that's to be expected.

175
00:36:11,180 --> 00:36:13,694
It's just your body getting used to the dosage.

176
00:36:22,540 --> 00:36:25,259
Is there anything else
you want to talk about, Gail?

177
00:36:29,380 --> 00:36:31,020
I wish you could come more often.

178
00:36:31,020 --> 00:36:31,258
I wish you could come more often.

179
00:36:34,220 --> 00:36:36,097
I'm afraid that er...

180
00:36:37,100 --> 00:36:40,220
...these visits are as regular
as my schedule will allow.

181
00:36:40,220 --> 00:36:43,132
Home visits like this are quite difficult to arrange.

182
00:36:46,860 --> 00:36:48,460
Still, it's progress
that the new medication's working.

183
00:36:48,460 --> 00:36:49,973
Still, it's progress
that the new medication's working.

184
00:37:09,420 --> 00:37:10,780
Rachel!

185
00:37:10,780 --> 00:37:12,657
Get on with it.

186
00:37:27,380 --> 00:37:29,052
Frank!

187
00:37:31,300 --> 00:37:32,972
Frank?

188
00:37:34,460 --> 00:37:36,337
I'm going now.

189
00:37:39,460 --> 00:37:41,530
I'll be back in about an hour.

190
00:37:45,420 --> 00:37:46,739
OK?

191
00:38:31,940 --> 00:38:36,968
<i>� If I got on my knees and I pleaded with you �</i>

192
00:38:38,140 --> 00:38:43,453
<i>� Not to go but to stay in my arms �</i>

193
00:38:44,500 --> 00:38:49,813
<i>� Would you walk out the door �</i>

194
00:38:50,660 --> 00:38:57,133
<i>� Like you did once before? �</i>

195
00:38:57,660 --> 00:39:02,097
<i>� This time be different �</i>

196
00:39:02,980 --> 00:39:05,619
<i>� Please stay �</i>

197
00:39:06,460 --> 00:39:08,337
<i>� Don't go �</i>

198
00:39:09,620 --> 00:39:14,740
<i>� If I call out your name like a prayer �</i>

199
00:39:15,740 --> 00:39:21,053
<i>� Would you leave me alone in �</i>
<i>� my fear? �</i>

200
00:39:22,340 --> 00:39:27,858
<i>� Knowing I need you so �</i>

201
00:39:28,580 --> 00:39:34,928
<i>� Would you still turn and go? �</i>

202
00:39:35,380 --> 00:39:37,769
<i>� This time �</i>
<i>� This time �</i>

203
00:39:38,260 --> 00:39:40,694
<i>� Be different �</i>
<i>� Be different �</i>

204
00:39:41,140 --> 00:39:43,017
<i>� Please stay �</i>

205
00:40:36,060 --> 00:40:37,940
- Oi!
- What?

206
00:40:37,940 --> 00:40:40,090
Why didn't you fucking tell me?

207
00:40:40,820 --> 00:40:42,420
Tell you what, Larry?

208
00:40:42,420 --> 00:40:45,059
You fucking know what-
about that cunt with the bike.

209
00:41:58,540 --> 00:42:00,019
Touch me.

210
00:42:19,380 --> 00:42:21,140
Out the fucking window, dickhead.

211
00:42:21,140 --> 00:42:22,892
My mum smells this shit a mile off, man.

212
00:42:23,900 --> 00:42:25,413
Fuck, I need a piss!

213
00:42:31,780 --> 00:42:33,498
That was a beast.

214
00:42:36,260 --> 00:42:38,220
Told you.

215
00:42:38,220 --> 00:42:40,097
Stop there.

216
00:42:43,380 --> 00:42:44,654
Have I taken quite a bit?

217
00:42:44,740 --> 00:42:46,412
Yeah, put more in that.

218
00:43:16,900 --> 00:43:18,618
Carry on.

219
00:43:31,900 --> 00:43:33,413
You can stop.

220
00:44:13,580 --> 00:44:14,569
Another one?

221
00:44:17,820 --> 00:44:20,140
He looks like he needs one, though.

222
00:44:20,140 --> 00:44:21,619
Pack the fuck out of this one.

223
00:44:26,660 --> 00:44:28,620
He's gonna die, ain't he?

224
00:44:28,620 --> 00:44:31,380
- Oi, man, this is for you, mate.
- No, I'm all right.

225
00:44:31,380 --> 00:44:34,500
- We just made this for you.
- He doesn't fucking want one, Joel.

226
00:44:34,500 --> 00:44:35,728
What?

227
00:44:39,300 --> 00:44:41,177
Cunt by name, cunt by nature.

228
00:44:41,940 --> 00:44:43,817
What the fuck's wrong with you?

229
00:46:59,340 --> 00:47:01,217
<i>...when we love somebody.</i>

230
00:47:02,900 --> 00:47:04,970
<i>Because loving is a painful thing.</i>

231
00:47:06,740 --> 00:47:08,617
<i>That is its nature.</i>

232
00:47:10,380 --> 00:47:15,374
<i>Today, even though we are not sure</i>
<i>that the pain will pass,</i>

233
00:47:16,580 --> 00:47:18,700
<i>it has to be said...</i>

234
00:47:18,700 --> 00:47:20,577
<i>...that our loving is hurting us.</i>

235
00:48:52,340 --> 00:48:55,650
It's a three-month waiting list
for the methadone programme.

236
00:48:55,740 --> 00:48:57,696
I take it you want me to recommend you?

237
00:49:25,220 --> 00:49:27,097
I don't want to come back.

238
00:49:31,540 --> 00:49:33,417
The next time we see each other...

239
00:49:34,620 --> 00:49:36,497
...I don't want it to be here.

240
00:50:29,260 --> 00:50:31,137
Has Sarah gone back?

241
00:50:31,860 --> 00:50:33,737
Yeah, she went back this morning.

242
00:50:35,860 --> 00:50:37,737
How's her course going?

243
00:50:38,700 --> 00:50:40,660
It's all right, I think.

244
00:50:40,660 --> 00:50:42,776
We didn't really talk about it that much.

245
00:50:43,580 --> 00:50:45,457
Nice to see her, though?

246
00:50:45,580 --> 00:50:47,457
Yeah, it was good.

247
00:50:48,180 --> 00:50:49,659
Sound.

248
00:51:00,020 --> 00:51:02,020
Still getting shit off your parents?

249
00:51:02,020 --> 00:51:03,897
What? To get a job, you mean?

250
00:51:04,020 --> 00:51:05,533
Yeah.

251
00:51:06,300 --> 00:51:08,060
No.

252
00:51:08,060 --> 00:51:10,335
They've eased off over the last few weeks.

253
00:51:13,700 --> 00:51:15,577
Probably because of Tess.

254
00:51:27,180 --> 00:51:28,977
How was the funeral?

255
00:51:31,100 --> 00:51:32,977
What do you want to know?

256
00:51:38,380 --> 00:51:40,132
How was Tess's mum?

257
00:51:42,260 --> 00:51:44,220
Yeah, she was all right.

258
00:51:44,220 --> 00:51:46,097
As good as you could expect...

259
00:51:46,940 --> 00:51:48,578
I guess.

260
00:51:50,180 --> 00:51:52,057
She asked about you, Rob.

261
00:51:55,660 --> 00:51:57,537
I think she used to quite like me.

262
00:51:58,940 --> 00:52:02,171
We got on well for a while, you know...
me and Jean.

263
00:52:10,180 --> 00:52:12,057
I saw you there, Rob.

264
00:52:16,660 --> 00:52:18,537
I thought you did.

265
00:52:24,460 --> 00:52:26,337
So why didn't you come in?

266
00:52:27,020 --> 00:52:28,339
Why did you leave?

267
00:52:36,860 --> 00:52:38,339
I'm sorry.

268
00:52:39,700 --> 00:52:41,930
I'm sorry I didn't call before the funeral.

269
00:52:47,140 --> 00:52:49,017
I couldn't have gone in.

270
00:52:52,140 --> 00:52:54,574
They all know what she fucking died of, David.

271
00:52:56,380 --> 00:52:58,257
And I was her boyfriend.

272
00:53:04,340 --> 00:53:06,300
Couldn't handle them all talking,

273
00:53:06,300 --> 00:53:08,177
fucking staring at me.

274
00:53:09,380 --> 00:53:11,257
I was a coward.

275
00:53:11,980 --> 00:53:13,857
I've been a stupid fucking coward.

276
00:53:14,580 --> 00:53:16,252
You haven't.

277
00:53:18,940 --> 00:53:20,817
It's just a fucked up thing.

278
00:53:27,740 --> 00:53:31,096
I never used to think about getting old,
growing up...

279
00:53:32,220 --> 00:53:33,980
...or dying.

280
00:53:33,980 --> 00:53:36,130
I never used to think about those things.

281
00:54:00,460 --> 00:54:02,337
It wasn't your fault, Rob.

282
00:54:05,140 --> 00:54:07,256
- No-one makes someone do that.
- I know.

283
00:54:11,100 --> 00:54:12,977
But I'll never see her face again.

284
00:56:39,020 --> 00:56:41,980
What the fuck's a month?
What? You forget that fucking easy?

285
00:56:41,980 --> 00:56:44,414
I haven't forgot... forgotten anything.

286
00:56:45,500 --> 00:56:47,380
I just want to be on my own at the moment, OK?

287
00:56:47,380 --> 00:56:50,140
- Am I meant to be fucking blind?
<i>-What are you talking about?</i>

288
00:56:50,140 --> 00:56:52,220
you know fucking well what I'm talking about.

289
00:56:52,220 --> 00:56:53,980
<i>Don't play fucking dumb with me.</i>

290
00:56:53,980 --> 00:56:55,652
<i>Why do it in my face?</i>

291
00:56:57,340 --> 00:57:00,060
What's it got to do with you?
We're not seeing each other.

292
00:57:00,060 --> 00:57:02,820
- You're fucking him, aren't you?
<i>- No.</i>

293
00:57:02,820 --> 00:57:04,412
Have you done anything with him?

294
00:57:04,540 --> 00:57:06,656
- Don't do this.
<i>- I'm not doing anything.</i>

295
00:57:06,740 --> 00:57:08,651
<i>You're fucking doing it to me. It's you.</i>

296
00:57:08,740 --> 00:57:11,652
I haven't done anything to hurt you.
You're doing it to yourself.

297
00:57:11,740 --> 00:57:13,100
Doing what?

298
00:57:13,100 --> 00:57:15,460
<i>Texting me and phoning me all the time.</i>

299
00:57:15,460 --> 00:57:17,420
you know I'll answer because I care for you

300
00:57:17,420 --> 00:57:19,100
and then you just hurl abuse at me.

301
00:57:19,100 --> 00:57:20,540
So you don't want me to call you?

302
00:57:20,540 --> 00:57:23,060
<i>Not if you're going to shout "bitch"</i>
<i>at me all the time.</i>

303
00:57:23,060 --> 00:57:24,380
Well, you fucking are.

304
00:57:24,380 --> 00:57:26,700
Then don't phone me.

305
00:57:26,700 --> 00:57:28,860
<i>I hate you so fucking much.</i>

306
00:57:28,860 --> 00:57:31,658
I don't care. You made me stop loving you.

307
00:57:33,060 --> 00:57:35,415
<i>You can't get that this is hard for me too.</i>

308
00:57:36,980 --> 00:57:39,900
<i>I have to let you go but it's making me hurt.</i>

309
00:57:39,900 --> 00:57:41,380
you don't look like you are.

310
00:57:41,380 --> 00:57:43,580
You don't make it easy to show.

311
00:57:43,580 --> 00:57:45,500
What the fuck is that supposed to mean?

312
00:57:45,500 --> 00:57:48,651
<i>- I've got to go.</i>
- No, don't go. What does that mean?

313
00:57:49,500 --> 00:57:52,173
You know what you're like, everyone does.

314
00:57:52,260 --> 00:57:55,058
Always accusing me
of trying to fuck your friends.

315
00:57:55,860 --> 00:57:57,340
Of flirting round them...

316
00:57:57,340 --> 00:57:59,380
<i>You know all that's bullshit.</i>

317
00:57:59,380 --> 00:58:02,133
<i>You just enjoy making me feel like shit.</i>

318
00:58:04,460 --> 00:58:06,337
We're finished. So leave me alone.

319
00:58:07,020 --> 00:58:09,250
I'm going. And don't phone back.

320
00:58:20,060 --> 00:58:22,654
- My mum wants me.
<i>- I don't give a shit.</i>

321
00:58:23,540 --> 00:58:25,178
- I'm going.
- No, don't go.

322
00:58:25,260 --> 00:58:27,376
Don't put that fucking phone down. Talk to me.

323
00:58:28,220 --> 00:58:29,858
Fuck!

324
00:59:54,260 --> 00:59:56,020
I fucking hate her.

325
00:59:56,020 --> 00:59:58,614
I wish we didn't have to score off
such a fucking bitch.

326
01:00:00,300 --> 01:00:01,980
She did say half-six.

327
01:00:01,980 --> 01:00:04,016
I know she did. She's just playing God.

328
01:02:28,700 --> 01:02:30,577
Where are you going, Nan?

329
01:02:33,660 --> 01:02:34,809
Nan?

330
01:02:36,340 --> 01:02:38,140
Nan?

331
01:02:38,140 --> 01:02:40,415
You can't go outside, Nan.

332
01:02:46,940 --> 01:02:50,091
Nan, you're not allowed outside.

333
01:02:52,060 --> 01:02:54,528
Yes, I am.

334
01:02:55,380 --> 01:02:56,654
No, you're not.

335
01:03:05,500 --> 01:03:07,092
Nan...

336
01:03:07,860 --> 01:03:08,929
Please.

337
01:03:12,780 --> 01:03:15,613
Go back inside and I'll make you a cup of tea.

338
01:03:17,220 --> 01:03:19,495
Enough tea.

339
01:03:21,420 --> 01:03:24,298
I want to go out.

340
01:04:40,060 --> 01:04:42,415
Did you like being out?

341
01:04:47,580 --> 01:04:50,890
I thought it was beautiful out there.

342
01:04:52,900 --> 01:04:57,018
The trees always look special
at this time of year.

343
01:05:00,700 --> 01:05:02,975
I prefer summer, mind.

344
01:05:05,700 --> 01:05:08,009
He liked autumn, though.

345
01:05:11,500 --> 01:05:12,853
Who?

346
01:05:17,020 --> 01:05:19,329
He loved this time.

347
01:05:20,380 --> 01:05:23,213
The leaves changing color.

348
01:05:27,460 --> 01:05:29,610
Then, in spring...

349
01:05:31,260 --> 01:05:33,933
...he used to bring me blossom.

350
01:05:36,820 --> 01:05:38,697
Who did?

351
01:05:43,180 --> 01:05:45,057
Michael.

352
01:05:47,860 --> 01:05:51,694
- Who's Michael?
- You are a good girl.

353
01:05:56,500 --> 01:05:58,570
You are, you know?

354
01:06:01,580 --> 01:06:04,378
And this is a good life.

355
01:06:07,780 --> 01:06:10,578
And as long as you don't weaken...

356
01:06:45,340 --> 01:06:47,695
Food's ready!

357
01:07:02,260 --> 01:07:04,012
You've forgiven me?

358
01:07:10,340 --> 01:07:13,820
It was such a long time ago.

359
01:07:13,820 --> 01:07:16,812
Why does it still matter?

360
01:07:17,500 --> 01:07:19,934
Because I can't forget it.

361
01:07:27,380 --> 01:07:29,769
Do you still love me?

362
01:07:35,700 --> 01:07:39,739
Do you think that someone could die
of a broken heart?

363
01:07:41,700 --> 01:07:43,691
What do you mean?

364
01:07:44,940 --> 01:07:49,855
There was this couple in the hospital,
older than us.

365
01:07:52,420 --> 01:07:54,729
They shared a private room.

366
01:07:55,620 --> 01:07:59,977
I only noticed them
because they seemed so close.

367
01:08:01,460 --> 01:08:03,610
She couldn't do much for herself.

368
01:08:06,020 --> 01:08:07,976
He had to care for her.

369
01:08:08,700 --> 01:08:10,736
Feed her and so on.

370
01:08:13,500 --> 01:08:17,334
She passed away
a few days after I arrived.

371
01:08:21,340 --> 01:08:23,490
Didn't see much of him then.

372
01:08:25,540 --> 01:08:29,453
But when I did... he...

373
01:08:31,980 --> 01:08:34,414
...looked different.

374
01:08:38,060 --> 01:08:40,733
He died the night before I came out.

375
01:08:43,100 --> 01:08:47,332
I knew then how much I wanted to come home.

376
01:08:49,260 --> 01:08:52,060
But when I saw you,

377
01:08:52,060 --> 01:08:56,338
something still wouldn't let me touch you.

378
01:09:00,140 --> 01:09:02,256
But I wanted to.

379
01:12:08,260 --> 01:12:10,535
I'll just get you a cup of tea, Nan.

380
01:12:22,100 --> 01:12:23,977
Gail!

381
01:13:08,940 --> 01:13:10,817
- See you later.
- See you.

382
01:13:39,540 --> 01:13:42,179
Have you noticed
he's stopped wearing T-shirts?

383
01:13:44,660 --> 01:13:46,537
Do you think he injects now?

384
01:13:50,820 --> 01:13:52,890
I know he does.

385
01:14:14,220 --> 01:14:16,097
When are your mum and dad coming back?

386
01:14:18,140 --> 01:14:19,778
Tomorrow.

387
01:14:49,060 --> 01:14:51,176
Have you never seen that before?

388
01:14:54,100 --> 01:14:55,977
No.

389
01:15:02,380 --> 01:15:04,257
Keep it.

390
01:15:30,260 --> 01:15:33,411
<i>Hi, this is Sarah. Leave me a message</i>
<i>and I'll get back to you.</i>

391
01:17:51,180 --> 01:17:53,136
I don't remember Granddad.

392
01:17:54,260 --> 01:17:56,137
You wouldn't.

393
01:17:57,660 --> 01:18:00,049
He died when your dad was very young.

394
01:18:01,620 --> 01:18:03,770
He was the youngest, you see.

395
01:18:04,780 --> 01:18:06,816
The youngest of ten.

396
01:18:08,620 --> 01:18:10,497
They had ten kids?

397
01:18:14,260 --> 01:18:18,651
Imagine losing your husband
when you've got ten kids.

398
01:18:21,140 --> 01:18:23,734
She brought them up on her own, you know?

399
01:18:28,900 --> 01:18:31,460
Why didn't she ever re-marry?

400
01:18:35,220 --> 01:18:37,097
Your dad said...

401
01:18:37,220 --> 01:18:39,973
she was only ever interested in one man...

402
01:18:40,900 --> 01:18:44,893
...and when he died,
she didn't want anybody else.

403
01:18:47,100 --> 01:18:50,854
I think she felt she'd already had her family.

404
01:18:52,460 --> 01:18:55,930
Blessed, as she would say.

405
01:19:20,700 --> 01:19:23,339
I'm gonna have to go and see your dad.

406
01:19:25,060 --> 01:19:26,937
Will you come?

407
01:24:21,220 --> 01:24:22,778
<i>"Nothing.</i>

408
01:24:30,860 --> 01:24:32,737
<i>She supposed.</i>

409
01:24:44,220 --> 01:24:46,097
<i>This was real life.</i>

410
01:24:48,180 --> 01:24:50,648
<i>And real life was difficult.</i>

411
01:24:52,700 --> 01:24:54,577
<i>At best.</i>

412
01:24:58,700 --> 01:25:02,659
<i>Hadn't she learnt that,</i>
<i>when she was a little girl?</i>

413
01:25:08,060 --> 01:25:13,009
<i>Why did she think falling in love</i>
<i>would make it easier?</i>

414
01:25:26,660 --> 01:25:30,653
<i>Why did she think... falling in love...</i>

415
01:25:41,740 --> 01:25:44,732
<i>...would make it any... easier?"</i>
