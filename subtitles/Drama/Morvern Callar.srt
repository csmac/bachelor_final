﻿1
00:05:45,454 --> 00:05:47,506
Hello?

2
00:05:49,750 --> 00:05:51,256
Who?

3
00:05:52,336 --> 00:05:54,506
No. No, this is Morvern Callar.

4
00:05:58,175 --> 00:06:00,227
Morvern Callar.

5
00:06:01,845 --> 00:06:06,401
M-O-R-V...

6
00:06:17,069 --> 00:06:18,706
I don't see anybody.

7
00:06:19,946 --> 00:06:21,702
No.

8
00:06:23,200 --> 00:06:25,252
Yes, it's the station.

9
00:06:29,539 --> 00:06:32,469
No, no, I'm not from here, no.
But I've lived here for years.

10
00:06:35,087 --> 00:06:37,471
I was... I was going to make a call.

11
00:06:45,639 --> 00:06:47,442
Oh, don't worry.

12
00:06:48,517 --> 00:06:50,273
I'm sure she'll be fine.

13
00:06:51,853 --> 00:06:53,905
Yeah.

14
00:06:58,068 --> 00:07:00,120
OK, then.

15
00:07:01,113 --> 00:07:02,750
OK, thanks a lot. Bye.

16
00:07:06,201 --> 00:07:07,922
Oh, aye, merry Christmas.

17
00:12:00,454 --> 00:12:02,506
Sorry.

18
00:12:47,334 --> 00:12:50,264
- Where have you been? I'm freezin' to death.
- Sorry.

19
00:12:51,338 --> 00:12:52,845
Where is he, then?

20
00:12:52,923 --> 00:12:55,343
- He's not coming.
- And why not?

21
00:12:56,968 --> 00:12:59,021
He's weird.

22
00:13:00,889 --> 00:13:03,143
What's that?
"Jackie"? Who's Jackie?

23
00:13:03,225 --> 00:13:05,479
- I found it.
- Lucky bitch. It's gold.

24
00:13:09,856 --> 00:13:11,612
What's up wi' you?

25
00:13:12,693 --> 00:13:14,745
Here. Take this and shut up.

26
00:13:37,759 --> 00:13:40,144
OK, boys, this time we'll
play you for Joan of Arc.

27
00:13:42,014 --> 00:13:44,268
Hi, gorgeous.

28
00:13:44,349 --> 00:13:46,401
- Hey, sexy.
- Arsehole.

29
00:13:46,476 --> 00:13:49,359
<i>When are you going to get
a steady boyfriend?</i>

30
00:13:56,653 --> 00:13:58,705
Where's your man the night, then, eh?

31
00:13:58,780 --> 00:14:00,536
At home.

32
00:14:00,615 --> 00:14:02,786
Eh? Don't tempt me.

33
00:14:04,536 --> 00:14:06,126
Hello, Morvern.

34
00:14:06,204 --> 00:14:08,044
Long time no see.

35
00:14:10,125 --> 00:14:12,759
I thought you'd have been over
to visit us by now.

36
00:14:12,836 --> 00:14:14,047
Sorry.

37
00:14:14,129 --> 00:14:16,846
What have you done wi' Dostoevsky tonight?

38
00:14:16,923 --> 00:14:19,641
- He's at home.
- You leave him at home?

39
00:14:19,718 --> 00:14:21,308
In the kitchen.

40
00:14:33,482 --> 00:14:35,238
They'll get their heads kicked in.

41
00:14:46,453 --> 00:14:48,540
Where are you taking us?

42
00:14:51,208 --> 00:14:52,928
The girl's mental.

43
00:14:53,001 --> 00:14:56,264
- Look at her.
- I know.

44
00:17:46,925 --> 00:17:49,179
I told them my leg won't stop shaking

45
00:17:49,261 --> 00:17:53,769
and then they said...
"Do you want to go to the hospital?"

46
00:17:53,849 --> 00:17:57,692
I said, "I don't know what to do
because my leg won't stop shaking."

47
00:17:59,813 --> 00:18:03,111
Is this your house?
It's a really nice house, isn't it?

48
00:18:03,191 --> 00:18:05,243
I like the bath.

49
00:18:12,117 --> 00:18:13,707
Reckon we can get any more?

50
00:18:13,785 --> 00:18:16,170
Do you reckon we can get any more?

51
00:18:16,246 --> 00:18:18,085
Do you reckon we can get any more?

52
00:18:19,166 --> 00:18:20,554
I reckon he's got some.

53
00:18:20,625 --> 00:18:21,966
I reckon he's got some.

54
00:18:22,044 --> 00:18:25,555
I reckon he's got some. I reck-I
reck-I reck-I reckon he's got some.

55
00:18:26,840 --> 00:18:28,810
<i>I broke my nail.</i>

56
00:18:28,884 --> 00:18:30,853
<i>Ask them, they might have seen it.</i>

57
00:18:30,927 --> 00:18:33,976
- Have you seen my nail?
- It's a sparkly blue nail.

58
00:18:34,056 --> 00:18:36,689
Excuse me, have you seen my nail?

59
00:18:36,767 --> 00:18:38,487
Look.

60
00:18:38,560 --> 00:18:40,233
She's lost her nail.

61
00:18:40,312 --> 00:18:42,815
Excuse me, everybody,
but I've lost my nail.

62
00:18:42,898 --> 00:18:45,033
I've fuckin' lost my nail.

63
00:18:45,108 --> 00:18:49,035
No, I think you've got a nice smile.
Don't worry about your smile.

64
00:18:49,112 --> 00:18:53,584
Have you seen my boyfriend?
I've lost my boyfriend.

65
00:18:53,659 --> 00:18:55,498
Have you seen my boyfriend?

66
00:19:07,798 --> 00:19:11,096
...when you can sit on your own
and not really say anything,

67
00:19:11,176 --> 00:19:14,604
I think that's when you know
you really love somebody.

68
00:21:15,092 --> 00:21:17,512
<i>What are you doing out here? It's freezin'.</i>

69
00:21:18,595 --> 00:21:20,232
Oh, shit.

70
00:21:21,765 --> 00:21:23,402
How are you feelin'?

71
00:21:24,851 --> 00:21:26,488
Not too good.

72
00:21:37,155 --> 00:21:39,920
That's the island where
my foster mum is buried.

73
00:21:41,243 --> 00:21:43,164
I'm fucked.

74
00:22:01,304 --> 00:22:03,357
Ooh, I need a pee.

75
00:22:04,683 --> 00:22:07,483
I do. I need it. I need to do a pee.

76
00:22:21,283 --> 00:22:23,584
I'm freezin'. Let's go to yours.

77
00:22:24,703 --> 00:22:27,716
- No, I don't wanna go home.
- I know what you mean.

78
00:22:31,418 --> 00:22:33,423
A merry Christmas, hen.

79
00:22:33,503 --> 00:22:36,089
Happy New Year, Gran. It's New Year.

80
00:22:36,173 --> 00:22:39,554
It's New Year, is it?
Och, I thought it was Christmas.

81
00:22:39,634 --> 00:22:43,431
- I'm only winding you up, Granny.
- Ah, get off me, toerag.

82
00:22:43,513 --> 00:22:46,396
Put the whistling jenny on
and we'll have a cup of tea.

83
00:22:46,475 --> 00:22:47,649
Right, Granny.

84
00:22:52,022 --> 00:22:57,409
A three-letter word meaning um, what is it,
neighbour. Och aye.

85
00:22:57,486 --> 00:22:59,870
What would we say instead of neighbour?

86
00:22:59,946 --> 00:23:01,584
- Pal.
- Pal!

87
00:23:01,657 --> 00:23:05,287
Three-letter word. Pal. That's right.

88
00:23:05,369 --> 00:23:07,421
<i>I know, Mum, but it was snowin'.</i>

89
00:23:10,666 --> 00:23:12,718
<i>Well, Morvern's not got a phone.</i>

90
00:23:16,046 --> 00:23:17,683
<i>Look...</i>

91
00:23:19,925 --> 00:23:21,562
<i>Right.</i>

92
00:23:22,678 --> 00:23:24,730
<i>Right.</i>

93
00:23:28,684 --> 00:23:30,356
<i>Just shut up.</i>

94
00:23:36,149 --> 00:23:39,079
So what did you get me
for Christmas, Granny?

95
00:23:39,152 --> 00:23:42,332
What Christmas? Thought
it was the New Year.

96
00:23:42,406 --> 00:23:44,162
<i>Och, away wi' you,</i>

97
00:23:44,241 --> 00:23:46,709
and have a hot bath, the pair of yous.

98
00:24:00,340 --> 00:24:02,061
What are you gonna tell him?

99
00:24:13,395 --> 00:24:15,317
You can tell him you crashed at mine.

100
00:24:18,608 --> 00:24:20,661
Lanna?

101
00:24:22,279 --> 00:24:23,869
What?

102
00:24:23,947 --> 00:24:25,703
Summat bad's happened.

103
00:24:27,993 --> 00:24:30,045
You're not pregnant, are you?

104
00:24:33,790 --> 00:24:36,756
He's gone. He's left me.

105
00:24:36,835 --> 00:24:39,683
- What are you talking about?
- He's left me.

106
00:24:39,755 --> 00:24:42,091
Oh, he's probably just in one of his moods.

107
00:24:44,259 --> 00:24:45,849
He's not.

108
00:24:45,927 --> 00:24:47,399
He's really gone.

109
00:24:48,513 --> 00:24:50,601
He's never coming back.

110
00:24:50,682 --> 00:24:52,522
He's gone where?

111
00:24:59,691 --> 00:25:01,328
Gone where?

112
00:25:02,861 --> 00:25:04,415
Dunno.

113
00:25:04,488 --> 00:25:06,078
To another country.

114
00:25:06,156 --> 00:25:08,624
Another country?

115
00:25:08,700 --> 00:25:11,120
I don't know what you're talking about.

116
00:25:16,416 --> 00:25:18,468
Is it something to do wi' me?

117
00:25:19,628 --> 00:25:21,680
What?

118
00:25:21,755 --> 00:25:24,140
I'll get some more towels, you look tired.

119
00:28:13,427 --> 00:28:15,895
Are you going deaf or what?
I was buzzin' for ages.

120
00:28:15,971 --> 00:28:17,524
Sorry.

121
00:28:23,395 --> 00:28:26,823
- Merry Christmas, pet.
- Oh, Lanna...

122
00:28:26,898 --> 00:28:28,701
I didn't get you anything.

123
00:28:28,775 --> 00:28:31,623
- I'm sorry.
- Don't be daft. It's no big deal.

124
00:28:31,695 --> 00:28:33,617
Come here.

125
00:28:33,697 --> 00:28:35,500
And don't worry, he'll come back.

126
00:28:35,574 --> 00:28:38,160
<i>Lanna Phimister to bakery section.</i>

127
00:28:38,243 --> 00:28:42,205
Great. I'll meet you at
Menzies at nine, right?

128
00:28:42,289 --> 00:28:45,801
<i>Can Lanna Phimister
please make her way to bakery section?</i>

129
00:29:03,602 --> 00:29:04,990
Don't worry, he'll be back,

130
00:29:05,062 --> 00:29:07,114
<i>tail tucked between his legs.</i>

131
00:29:09,691 --> 00:29:11,992
<i>Loose carrots 69p a pound.</i>

132
00:29:12,069 --> 00:29:14,323
<i>Thank you for shopping at Pennysaver.</i>

133
00:30:09,292 --> 00:30:10,846
Hello.

134
00:30:13,880 --> 00:30:15,932
Just get the snow off my boots.

135
00:30:18,719 --> 00:30:20,059
Are you looking for Lanna?

136
00:30:20,137 --> 00:30:22,473
<i>No. I was kind of wondering how you were.</i>

137
00:35:50,258 --> 00:35:52,477
- What are you doing here?
- Na-naa!

138
00:35:52,552 --> 00:35:54,189
What? What is it?

139
00:35:55,263 --> 00:35:57,351
I booked us both a fortnight at a resort.

140
00:35:57,432 --> 00:35:59,022
You're kidding?!

141
00:36:04,690 --> 00:36:06,944
Creeping Jesus will have
to change your shifts.

142
00:36:07,025 --> 00:36:09,161
- And don't worry about money.
- Oh, my God!

143
00:36:09,236 --> 00:36:11,241
Lynn, I cannae believe it!

144
00:36:11,321 --> 00:36:13,576
Morvern, you are so lovely to me!

145
00:36:15,283 --> 00:36:18,083
Oh, it's all right. It's
your Christmas present.

146
00:36:22,124 --> 00:36:24,710
- Where did you get the money?
- From the bank.

147
00:40:37,129 --> 00:40:39,513
What are we goin' to do when we get there?

148
00:40:39,589 --> 00:40:41,641
Be quiet.

149
00:40:45,429 --> 00:40:48,525
<i>It's an old woman that lives there anyway.</i>

150
00:41:00,652 --> 00:41:03,950
He's left all his stuff, eh?
Even the computer.

151
00:41:05,824 --> 00:41:07,876
<i>It's kinda creepy, innit?</i>

152
00:41:08,493 --> 00:41:11,258
Oh, my head's buzzin' wi' this tab.

153
00:41:11,330 --> 00:41:14,094
- We've gotta put some music on.
- <i>What do you want on?</i>

154
00:41:14,166 --> 00:41:16,218
<i>Some of his stuff.</i>

155
00:41:44,488 --> 00:41:47,869
Did you know that Shadow the shelf-stacker
got the boot from work?

156
00:41:48,950 --> 00:41:50,541
I don't want to talk about work.

157
00:41:50,619 --> 00:41:52,339
I hated him. He's a real perv.

158
00:41:58,960 --> 00:42:01,641
Let's look at some of his
stuff on the computer.

159
00:42:02,964 --> 00:42:06,227
- No.
- Maybe he's wrote something about me.

160
00:42:06,301 --> 00:42:07,974
I don't want to.

161
00:42:08,053 --> 00:42:10,105
You're a pure bore.

162
00:42:14,101 --> 00:42:15,987
Let's do some hooverin'.

163
00:42:22,484 --> 00:42:24,703
Oh, come on, it'll be a brilliant trip.

164
00:42:24,778 --> 00:42:25,870
No.

165
00:42:32,452 --> 00:42:34,504
Do you want to do some bakin', then?

166
00:42:56,309 --> 00:42:58,149
We got to do it right.

167
00:43:09,323 --> 00:43:11,743
Oh, that's brilliant.

168
00:43:11,825 --> 00:43:15,456
- It's pure dead brilliant.
- Pure dead brilliant.

169
00:43:15,537 --> 00:43:18,716
It's no sticking properly
to the wee pasty underneath.

170
00:43:21,168 --> 00:43:23,422
Your hand's shakin'.

171
00:43:39,478 --> 00:43:41,981
Cake. It's a wee cake.

172
00:43:43,106 --> 00:43:44,743
Morvern?

173
00:43:45,817 --> 00:43:47,869
What?

174
00:43:50,822 --> 00:43:52,874
What?

175
00:44:02,834 --> 00:44:03,878
What?

176
00:44:08,674 --> 00:44:10,726
You know we were pals?

177
00:44:11,885 --> 00:44:13,439
What, us?

178
00:44:13,512 --> 00:44:15,351
No. Me and him.

179
00:44:26,316 --> 00:44:27,823
We were good pals.

180
00:44:28,902 --> 00:44:30,954
What do you mean?

181
00:44:33,907 --> 00:44:35,746
You'll be in a massive huff.

182
00:44:54,177 --> 00:44:55,470
Did you go with him, then?

183
00:46:49,001 --> 00:46:52,180
<i>Look, Morvern,
I know how terrible it must feel to you,</i>

184
00:46:52,254 --> 00:46:54,888
<i>but it realty wasn't what you think.</i>

185
00:46:54,965 --> 00:46:57,682
I'll understand
if you don't want me to come on holiday.

186
00:46:58,760 --> 00:47:00,931
I'll stay here and look after the flat.

187
00:47:02,347 --> 00:47:05,230
It's not that I don't want to come.
I'd love to come.

188
00:47:10,856 --> 00:47:13,192
<i>You're my best friend, Morvern.</i>

189
00:47:15,402 --> 00:47:17,454
<i>I'm sorry, really.</i>

190
00:47:20,532 --> 00:47:24,542
I got your letter.
Um... I'm... I'm glad you like the book.

191
00:47:24,619 --> 00:47:28,083
I can't... I can't come and meet you
in London because I'm off to Spain.

192
00:47:29,166 --> 00:47:30,637
So... Oh. Shit.

193
00:47:30,709 --> 00:47:32,761
Um... It's the er...

194
00:47:34,296 --> 00:47:35,684
...Hotel Rozinante.

195
00:47:38,467 --> 00:47:42,097
- <i>Come on! We're missin' the plane!</i>
- Sorry.

196
00:47:42,179 --> 00:47:44,231
Hurry up!

197
00:47:45,307 --> 00:47:47,359
You're lucky to be on the flight.

198
00:48:12,459 --> 00:48:14,013
Oh, wow!

199
00:48:14,086 --> 00:48:16,138
This is great.

200
00:48:18,048 --> 00:48:19,471
<i>Lanna!</i>

201
00:48:20,592 --> 00:48:23,096
- What about the buses?
- We'll get a cab.

202
00:49:35,500 --> 00:49:38,430
- Are the buses here already?
- No, we got a lift.

203
00:49:38,503 --> 00:49:41,089
<i>There is a special bus
laid on for you, you know.</i>

204
00:49:41,173 --> 00:49:42,810
So?

205
00:49:44,551 --> 00:49:45,809
<i>Name?</i>

206
00:49:45,886 --> 00:49:47,558
Morvern Callar.

207
00:49:47,637 --> 00:49:50,022
Oh, by the way, there's a message for you.

208
00:49:54,561 --> 00:49:56,317
- Let me see.
- Give it back.

209
00:49:56,396 --> 00:49:58,781
- "Tom Boddington."
- Give it back.

210
00:49:58,857 --> 00:50:00,993
I said give it back!

211
00:50:01,068 --> 00:50:02,907
There's no need to get nasty.

212
00:50:04,696 --> 00:50:06,748
So who's Tom Boddington, anyway?

213
00:50:07,824 --> 00:50:09,627
Just a guy I met.

214
00:50:09,701 --> 00:50:11,422
Where?

215
00:50:11,495 --> 00:50:14,709
- At the airport.
- At the airport? How does he know you're here?

216
00:50:14,790 --> 00:50:16,546
Cos I told him.

217
00:50:21,213 --> 00:50:23,265
Fab!

218
00:50:24,466 --> 00:50:26,518
This is crackin'.

219
00:50:29,638 --> 00:50:32,817
Lanna Phimister and Mervo Caller?

220
00:50:32,891 --> 00:50:34,730
Morvern Callar.

221
00:50:34,810 --> 00:50:36,447
Marven Caller?

222
00:50:36,520 --> 00:50:39,106
- Morvern Callar!
- Marvell?

223
00:50:39,189 --> 00:50:41,953
Oh, for fuck's sake!

224
00:50:43,694 --> 00:50:45,746
- Good mor...
- Never mind. What is it?

225
00:50:45,821 --> 00:50:47,660
Good morning, Marvell Caller.

226
00:50:47,739 --> 00:50:50,124
<i>Welcome to Hotel Rozinante...</i>

227
00:51:03,755 --> 00:51:06,057
...very pleasant stay.

228
00:51:06,133 --> 00:51:08,185
- OK?
- OK.

229
00:51:47,215 --> 00:51:49,267
You speak English?

230
00:51:51,386 --> 00:51:53,854
- Very quiet.
- Swedish?

231
00:51:53,930 --> 00:51:57,229
<i>Me Dazzer and that's Dave.</i>

232
00:51:59,186 --> 00:52:00,313
Helga.

233
00:52:00,395 --> 00:52:01,819
Ha! Olga.

234
00:52:02,939 --> 00:52:04,577
<i>Olga.</i>

235
00:52:04,649 --> 00:52:06,702
- Olga?
- <i>Olga.</i>

236
00:52:06,777 --> 00:52:08,533
<i>Ol-ga.</i>

237
00:52:08,612 --> 00:52:09,905
- Olga.
- Olga.

238
00:52:09,988 --> 00:52:11,495
<i>German?</i>

239
00:52:12,616 --> 00:52:15,795
So you are German?
I know someone from Germany.

240
00:52:15,869 --> 00:52:17,921
Bloke. Um...

241
00:52:19,247 --> 00:52:21,418
<i>Al... Alwin...</i>

242
00:52:21,500 --> 00:52:23,884
<i>Alwin Küchler from Germany.</i>

243
00:52:23,960 --> 00:52:25,348
<i>Do you know him?</i>

244
00:52:30,550 --> 00:52:32,472
Slappers!

245
00:52:32,552 --> 00:52:35,103
Look at them two over there.

246
00:52:44,231 --> 00:52:46,283
Get off!

247
00:52:54,116 --> 00:52:55,753
I like champagne.

248
00:52:55,826 --> 00:52:57,332
Oh, me too.

249
00:52:57,411 --> 00:53:00,839
Morvern, this is great. You're so good
to me and I really don't deserve it.

250
00:53:00,914 --> 00:53:02,670
- Oh, shut up.
- I mean it.

251
00:53:02,749 --> 00:53:05,715
- You're such a good friend. You're great.
- That's enough now.

252
00:53:07,963 --> 00:53:09,849
And there's plenty of guys who fancy you.

253
00:53:09,923 --> 00:53:12,142
You'll have no problem
finding a new boyfriend.

254
00:53:12,217 --> 00:53:16,144
- Who said I wanted a new boyfriend?
- Like that guy Tom you met at the airport.

255
00:53:16,221 --> 00:53:18,107
Have you phoned Tom? He fancies you.

256
00:53:18,181 --> 00:53:19,771
What are you talking about?

257
00:53:23,770 --> 00:53:25,111
Let's go in.

258
00:53:29,943 --> 00:53:31,995
I'm goin' in.

259
00:53:33,739 --> 00:53:36,622
Eugh. There's beasties
floating about. Look.

260
00:53:46,209 --> 00:53:49,092
People are fuckin' tryin' to sleep up here!

261
00:53:52,341 --> 00:53:55,105
Fuckin' cut it out! I'm
tryin' to get some sleep!

262
00:53:55,177 --> 00:53:57,229
Do you not have children?

263
00:53:59,264 --> 00:54:02,230
Swap your swimsuits! Away you go.

264
00:54:02,309 --> 00:54:05,156
<i>If these cats in the bag
don't get into the spirit of things,</i>

265
00:54:05,228 --> 00:54:08,028
they're going to end up
in the swimming pool!

266
00:54:10,108 --> 00:54:11,911
How are you getting on in there?

267
00:54:12,986 --> 00:54:14,160
Nice one, Tony!

268
00:54:14,237 --> 00:54:17,037
I think they've had long enough.
Drum roll, everybody.

269
00:54:17,115 --> 00:54:19,203
Here we go.

270
00:54:20,327 --> 00:54:22,414
And... Look at that.

271
00:54:22,496 --> 00:54:26,339
<i>Don't they look lovely? Beautiful.
Big round of applause.</i>

272
00:54:41,682 --> 00:54:43,022
Morvey.

273
00:54:43,100 --> 00:54:44,690
Morvey!

274
00:54:46,228 --> 00:54:48,731
<i>- Like my hat?
- Yeah, it's lovely.</i>

275
00:54:48,814 --> 00:54:51,661
<i>I can't stand up, mate. I am mashed!</i>

276
00:54:52,776 --> 00:54:55,161
- He's an ugly cunt.
- <i>Aye, I'm an ugly cunt, like</i>.

277
00:54:56,279 --> 00:54:58,664
- I like your pants.
- Do you like them?

278
00:54:58,740 --> 00:55:00,995
Very nice.

279
00:55:01,076 --> 00:55:03,377
I never take it off. My
hair's a fuckin' mess.

280
00:55:03,453 --> 00:55:05,589
Do you like it like that?

281
00:55:05,664 --> 00:55:07,384
- Does it work?
- That's much better.

282
00:55:07,457 --> 00:55:09,427
It's my hat, greasy.

283
00:55:09,501 --> 00:55:11,921
I don't wash it, you see.

284
00:55:12,004 --> 00:55:13,178
Makes it dry.

285
00:55:13,255 --> 00:55:15,509
We just got a cheap flight, didn't we?

286
00:55:15,590 --> 00:55:18,889
We don't know how long we're going
to be here. We're looking for jobs.

287
00:55:18,969 --> 00:55:21,686
- Do you want a job?
- Nah, you're all right.

288
00:55:21,763 --> 00:55:23,815
I don't want one either.

289
00:55:23,890 --> 00:55:26,571
<i>- What's your name?
- Lanna.</i>

290
00:55:26,643 --> 00:55:28,446
<i>Paul.</i>

291
00:55:28,520 --> 00:55:29,943
<i>- Lanna?
- Lanna, yeah.</i>

292
00:55:30,022 --> 00:55:31,409
<i>- Lara?
- Lanna.</i>

293
00:55:31,481 --> 00:55:32,573
<i>Lanna.</i>

294
00:55:32,649 --> 00:55:35,330
<i>Lanna. As in, like, Lanna the llama?</i>

295
00:55:35,402 --> 00:55:38,700
<i>Fuckin' hell!</i>

296
00:55:38,780 --> 00:55:41,117
<i>It's all right.</i>

297
00:55:41,199 --> 00:55:43,880
<i>Do you... Do you want to come
to Revert with us later?</i>

298
00:55:43,952 --> 00:55:47,665
<i>It's only down the road and it's fucking
good, innit? It's a great club.</i>

299
00:55:47,748 --> 00:55:49,966
It's got two big fuckin' rooms, right.

300
00:55:50,042 --> 00:55:51,881
- 24 hours, man.
- 24 hours.

301
00:55:51,960 --> 00:55:54,096
They close one room and,
like, clean the other.

302
00:55:54,171 --> 00:55:56,757
How can they close one
and clean the other, spacker?

303
00:55:56,840 --> 00:56:00,720
<i>You're doing my fuckin' head in.
You're doing it on purpose to show me up.</i>

304
00:56:00,802 --> 00:56:02,642
I was saying summat.

305
00:56:02,721 --> 00:56:05,438
Oh, Sashy! Our fuckin' mate Sasha, man.

306
00:56:05,515 --> 00:56:08,529
- He's fuckin' off his head.
- He came here for a week...

307
00:56:08,602 --> 00:56:09,646
All right, mate?

308
00:56:09,770 --> 00:56:10,770
Dick.

309
00:56:10,812 --> 00:56:14,027
...found him a month later...

310
00:56:14,107 --> 00:56:15,281
...a month later,

311
00:56:15,359 --> 00:56:19,285
<i>in The Revert, just been livin' on
fuckin' orange juice and burgers, man!</i>

312
00:56:19,363 --> 00:56:21,913
<i>He completely fried his brain.</i>

313
00:56:21,990 --> 00:56:23,746
<i>So are you gonna come with us?</i>

314
00:56:23,825 --> 00:56:25,166
<i>Aye, great.</i>

315
00:56:25,243 --> 00:56:28,043
<i>You ought to.
It's gonna be a fuckin' laugh.</i>

316
00:56:28,121 --> 00:56:31,051
It just goes on and on.
You just fuckin' get smashed.

317
00:56:31,124 --> 00:56:34,885
- You get some fuckin' nuts.
- I'm fuckin' off my nut now, man.

318
00:56:34,961 --> 00:56:36,302
Think they're cute?

319
00:56:36,380 --> 00:56:38,930
But I might go somewhere else.

320
00:56:42,719 --> 00:56:44,475
<i>Where's she going?</i>

321
00:56:44,554 --> 00:56:46,524
<i>Ah, she'll be all right.</i>

322
00:59:02,317 --> 00:59:04,369
<i>No!</i>

323
00:59:16,206 --> 00:59:18,258
<i>No. No.</i>

324
00:59:52,743 --> 00:59:54,333
Are you all right?

325
00:59:56,204 --> 00:59:58,256
My mum's dead.

326
01:00:49,800 --> 01:00:51,935
Will you stay for a while and talk to me?

327
01:01:03,313 --> 01:01:06,944
I'll tell you about my foster
mum's funeral, if you like.

328
01:03:19,366 --> 01:03:21,418
I've gotta go back.

329
01:03:24,329 --> 01:03:26,417
- <i>Who's there?</i>
- It's me.

330
01:03:34,006 --> 01:03:35,808
What time is it?

331
01:03:35,882 --> 01:03:38,303
Half nine.

332
01:03:38,385 --> 01:03:40,437
<i>Where have you been?</i>

333
01:03:44,641 --> 01:03:46,693
<i>We've just taken some E's.</i>

334
01:03:46,768 --> 01:03:48,820
- Hi.
- Hi. Do you want one?

335
01:03:53,233 --> 01:03:54,823
What are you doing with the bags?

336
01:03:54,901 --> 01:03:56,953
Get dressed. We're leaving.

337
01:04:07,205 --> 01:04:09,590
- What are you doing?
- Get dressed. We're leaving.

338
01:04:09,666 --> 01:04:12,430
- Come on.
- <i>Are you sure you don't want a pill?</i>

339
01:04:31,772 --> 01:04:34,322
- What's the matter with you?
- We're going elsewhere.

340
01:04:34,399 --> 01:04:36,120
Where?

341
01:04:36,193 --> 01:04:39,242
What are you lookin' at? Pervert!

342
01:04:39,321 --> 01:04:40,531
Come on!

343
01:04:59,091 --> 01:05:00,847
What the hell is this?!

344
01:06:08,201 --> 01:06:10,253
Mar-way.

345
01:06:23,008 --> 01:06:25,060
Where are we goin'?

346
01:06:29,389 --> 01:06:31,026
Somewhere beautiful.

347
01:06:55,957 --> 01:06:57,713
Oh, my God.

348
01:07:15,310 --> 01:07:16,947
Lanna!

349
01:07:38,166 --> 01:07:40,218
Lanna!

350
01:09:16,765 --> 01:09:18,817
Leave me alone, you old bag!

351
01:09:20,227 --> 01:09:22,279
Lanna!

352
01:09:28,360 --> 01:09:29,748
Vaya, hombre!

353
01:09:30,862 --> 01:09:32,914
Lanna!

354
01:09:37,452 --> 01:09:39,504
Lanna!

355
01:09:41,790 --> 01:09:43,842
<i>Lanna!</i>

356
01:10:01,393 --> 01:10:03,445
Don't like it here. I want to go back.

357
01:10:06,023 --> 01:10:08,075
Where's your case?

358
01:10:10,652 --> 01:10:12,704
I don't know.

359
01:10:16,116 --> 01:10:18,702
And the new bikini and everything!

360
01:10:18,785 --> 01:10:20,541
I can't believe it.

361
01:10:20,620 --> 01:10:22,127
That cost a fortune.

362
01:10:26,585 --> 01:10:27,878
And the pink dress.

363
01:10:28,962 --> 01:10:31,014
Everything.

364
01:10:35,802 --> 01:10:37,890
This is too depressin'.

365
01:10:42,309 --> 01:10:44,148
Wow!

366
01:10:44,227 --> 01:10:46,529
This is amazing.

367
01:10:46,605 --> 01:10:49,570
This? It's the middle of nowhere.

368
01:10:58,408 --> 01:11:00,460
I cannae walk.

369
01:11:02,371 --> 01:11:04,376
Oh, that's a sign.

370
01:11:04,456 --> 01:11:06,342
A sign for a hotel, I bet you.

371
01:11:07,459 --> 01:11:09,511
- Come on.
- There's nothin' up that way.

372
01:11:09,586 --> 01:11:11,757
- Come on, you.
- It's just mountains.

373
01:11:14,007 --> 01:11:15,430
Come on.

374
01:11:15,509 --> 01:11:18,059
Oh, I cannae walk. My feet are killin' me.

375
01:11:56,383 --> 01:11:58,435
Have you got a light?

376
01:12:13,442 --> 01:12:15,281
Somethin' just bit me!

377
01:12:15,360 --> 01:12:16,831
This place is crawlin'.

378
01:12:20,574 --> 01:12:22,413
Are you sure you haven't got a light?

379
01:12:25,287 --> 01:12:29,463
You don't understand, Morvern.
We are lost in the middle of nowhere!

380
01:12:29,541 --> 01:12:30,834
I want to go back.

381
01:12:36,548 --> 01:12:38,387
There's nothin' wrong wi' that place.

382
01:12:38,467 --> 01:12:40,519
I liked it. I was havin' a great time.

383
01:12:51,938 --> 01:12:54,738
- What?
- Did you hear that?

384
01:12:54,816 --> 01:12:57,664
- What?
- There. Look.

385
01:12:59,237 --> 01:13:01,492
There's eyes. Somebody's watching us.

386
01:13:14,211 --> 01:13:18,387
This is all your fault. Why did we have
to go anywhere anyhow? I hate this place.

387
01:13:20,592 --> 01:13:23,273
Stop laughing. It's not funny.

388
01:13:29,059 --> 01:13:30,898
- You hate me, don't you?
- <i>What?</i>

389
01:13:30,977 --> 01:13:33,446
You still fuckin' hate me
because of him, don't you?

390
01:13:33,522 --> 01:13:35,361
What are you talking about?

391
01:13:35,440 --> 01:13:38,489
You think he's left you because of me.
That's what you think.

392
01:13:38,568 --> 01:13:40,122
Shut up, Lanna.

393
01:13:40,195 --> 01:13:42,532
It was just a stupid fuck.
Wasnae even a good one.

394
01:13:42,614 --> 01:13:44,370
You're makin' it into some big deal.

395
01:13:44,449 --> 01:13:46,252
- <i>Shut up, Lanna.</i>
- Just get over it,

396
01:13:46,326 --> 01:13:48,830
- <i>for fuck's sake.</i>
- Shut up. He's dead.

397
01:13:48,912 --> 01:13:50,751
I'm sick of your stupid moods.

398
01:13:52,958 --> 01:13:55,544
<i>I mean, we could have been out clubbin' it.</i>

399
01:13:55,627 --> 01:13:59,056
<i>Instead, we're fuckin' surrounded
by donkeys and cactus.</i>

400
01:14:15,230 --> 01:14:18,861
<i>What's wrong wi' you? What do you want?
What planet are you on?</i>

401
01:14:26,074 --> 01:14:28,162
You're too fuckin' weird.

402
01:14:28,243 --> 01:14:30,331
- Where are you going?
- Away from you.

403
01:14:30,412 --> 01:14:32,464
That's my case.

404
01:16:09,970 --> 01:16:12,022
Jackie.

405
01:16:13,181 --> 01:16:15,233
Jackie.

406
01:16:16,309 --> 01:16:19,109
A que se aparece la niña
de ella que tanto te gusta.

407
01:16:19,187 --> 01:16:20,990
Cémo se llama?

408
01:16:21,064 --> 01:16:22,820
La que canta esa cancién...

409
01:16:27,529 --> 01:16:29,415
<i>La que siempre pone.</i>

410
01:16:30,615 --> 01:16:32,703
Mina Entrompada.

411
01:16:32,784 --> 01:16:38,918
Tu te pareces a la chica
de Mina Entrompada.

412
01:16:38,999 --> 01:16:41,051
<i>La conoces?</i>

413
01:17:21,416 --> 01:17:23,089
Hello?

414
01:17:23,168 --> 01:17:25,173
Hello, can I speak to Tom, please?

415
01:17:26,254 --> 01:17:28,224
Oh, sorry. Tom Boddington.

416
01:17:28,298 --> 01:17:30,220
Yeah.

417
01:17:30,300 --> 01:17:32,222
It's Morvern.

418
01:17:33,303 --> 01:17:35,355
Sorry. Morvern Callar.

419
01:17:41,812 --> 01:17:43,864
Hello. Hi.

420
01:17:45,065 --> 01:17:47,402
Yeah, I'm still in Spain.

421
01:17:47,484 --> 01:17:48,611
Yeah.

422
01:19:20,243 --> 01:19:23,292
One of the best first novels
I've read for a while.

423
01:19:23,372 --> 01:19:26,089
<i>I really loved hearing
such a distinctive female voice.</i>

424
01:19:26,166 --> 01:19:29,797
<i>I loved just each character.
The honesty, I loved.</i>

425
01:19:29,878 --> 01:19:32,559
- Very fresh.
- It's brilliant.

426
01:19:34,132 --> 01:19:36,019
<i>It's a brave piece.</i>

427
01:19:37,094 --> 01:19:41,435
So... how are you finding it here?
Are you having a good time in Spain?

428
01:19:41,515 --> 01:19:43,686
Aye, it's great, yeah.

429
01:19:44,768 --> 01:19:48,399
It's really beautiful
when you get to the quiet places, you know.

430
01:19:48,480 --> 01:19:49,821
I like the ants.

431
01:19:53,360 --> 01:19:57,370
<i>So... tell us something about yourself.
About your background.</i>

432
01:20:00,617 --> 01:20:04,710
Well, I do books, myself,
because it's got a lot to offer me.

433
01:20:04,788 --> 01:20:07,967
I mean, it's much better
than waking up on cold mornings

434
01:20:08,041 --> 01:20:10,722
knowing it's 39 years to go till pension,
you know?

435
01:20:11,795 --> 01:20:16,386
I mean, when you're writing,
you can just... knock off when you want.

436
01:20:17,551 --> 01:20:21,478
Look out the window, smoke a cigarette...

437
01:20:21,555 --> 01:20:23,939
then make a cup of coffee, take a shower...

438
01:20:26,101 --> 01:20:27,275
Is that OK?

439
01:20:27,352 --> 01:20:30,282
- Yeah, no. Sure.
- <i>Yeah, course, course.</i>

440
01:20:30,355 --> 01:20:34,068
Is there anyone on the business side
that we should be talking to specifically?

441
01:20:34,151 --> 01:20:36,156
Someone back in the UK?

442
01:20:36,236 --> 01:20:39,250
<i>Do you have an agent
or someone looking after your book?</i>

443
01:20:39,323 --> 01:20:41,493
Me. Talk to me.

444
01:20:41,575 --> 01:20:44,043
- Directly. Great. Fine.
- <i>That's great.</i>

445
01:20:44,119 --> 01:20:45,163
Well, um...

446
01:20:45,245 --> 01:20:46,668
<i>That's how we like it. No agent.</i>

447
01:20:46,747 --> 01:20:53,177
Since we're talking to you directly,
what did you have in mind deal-wise?

448
01:20:55,464 --> 01:20:57,599
Well, shall I just put something out there?

449
01:20:57,674 --> 01:21:02,265
We were thinking of something
in the region of... 100?

450
01:21:02,346 --> 01:21:05,193
<i>I know it's a bit awkward
talking about money.</i>

451
01:21:05,265 --> 01:21:08,362
Morvern, I'll be direct.
We love the novel. That's why we're here.

452
01:21:08,435 --> 01:21:11,199
We don't just jump on the plane to Spain
at a day's notice

453
01:21:11,271 --> 01:21:15,032
on every unsolicited manuscript
that comes through the door.

454
01:21:15,108 --> 01:21:17,576
But you are a first-time writer,

455
01:21:17,653 --> 01:21:21,200
<i>and we're taking a risk taking you on
and you have to appreciate that.</i>

456
01:21:21,281 --> 01:21:25,541
I can assure you that for a first-time
writer, £100,000 is a really good deal.

457
01:21:28,997 --> 01:21:31,133
<i>Can I go to the toilet?</i>

458
01:21:31,208 --> 01:21:33,260
<i>Of course.</i>

459
01:21:46,473 --> 01:21:48,525
Fucking hell.

460
01:21:57,901 --> 01:21:59,538
<i>So when do I get the money?</i>

461
01:22:00,988 --> 01:22:04,202
Well, you know, these things take
a little bit of time, but soon.

462
01:22:04,282 --> 01:22:07,331
We have to go through lawyers,
paperwork, that sort of thing.

463
01:22:07,411 --> 01:22:09,463
<i>We'll sort it out.</i>

464
01:22:09,538 --> 01:22:11,091
So how long does that take?

465
01:22:11,164 --> 01:22:13,051
If you sign the contract here now,

466
01:22:13,125 --> 01:22:16,008
er... then, you know,
by the time we get back to London,

467
01:22:16,086 --> 01:22:18,720
we could um... get a
cheque to you immediately.

468
01:22:22,759 --> 01:22:27,315
So, Morvern,
are you working on any new material?

469
01:22:27,389 --> 01:22:29,145
Material?

470
01:22:29,224 --> 01:22:32,570
Just give us just a few words.
What are you working on next?

471
01:22:43,322 --> 01:22:45,457
Hey, I'm on holiday.

472
01:22:46,992 --> 01:22:48,831
Oh, come on, Morvern, don't be shy.

473
01:22:49,911 --> 01:22:51,963
What's your next book about?

474
01:22:53,332 --> 01:22:55,384
<i>Sneak preview.</i>

475
01:23:00,047 --> 01:23:01,637
I work in a supermarket.

476
01:23:04,468 --> 01:23:06,058
Supermarket. Love it.

477
01:23:06,136 --> 01:23:07,607
Here, take a picture.

478
01:23:07,679 --> 01:23:09,815
Checkout girl. Say cheese.

479
01:23:15,395 --> 01:23:16,867
<i>Chin-chin.</i>

480
01:23:16,938 --> 01:23:19,619
- Chin-chin.
- Chin-chin.

481
01:23:33,163 --> 01:23:36,794
So, Morvern,
do you really work in a supermarket?

482
01:23:38,794 --> 01:23:40,596
Fruit and veg section.

483
01:25:22,647 --> 01:25:24,368
No, Lanna.

484
01:25:25,442 --> 01:25:27,114
Laa-nna.

485
01:25:28,195 --> 01:25:30,449
Lanna Phimister.

486
01:25:30,530 --> 01:25:32,582
That's right.

487
01:25:39,790 --> 01:25:41,842
Yeah. Aqualand?

488
01:25:43,752 --> 01:25:45,804
Right, right. Um...

489
01:25:47,881 --> 01:25:50,978
No, I'll call back later, thanks.

490
01:25:51,051 --> 01:25:53,815
No. No message. Thank you.

491
01:29:27,184 --> 01:29:28,607
<i>How have you been?</i>

492
01:29:28,685 --> 01:29:32,149
Same old, same old. Back on the early
shift till the end of my days.

493
01:29:33,607 --> 01:29:35,612
So where did you go, then?

494
01:29:35,692 --> 01:29:38,326
Oh, I met this group from Leeds.
Completely mental.

495
01:29:38,403 --> 01:29:40,206
Absolutely mad. Such a good laugh.

496
01:29:42,991 --> 01:29:44,284
Oh, and I met this guy.

497
01:29:44,368 --> 01:29:48,247
One of the guys from Leeds. His name's
Drew. He's coming to see me in a fortnight.

498
01:29:48,330 --> 01:29:49,920
- That's great.
- You'll like him.

499
01:29:49,998 --> 01:29:52,050
He's a good laugh.

500
01:30:02,678 --> 01:30:04,350
Lanna, I'm going again.

501
01:30:07,724 --> 01:30:09,314
What?

502
01:30:09,393 --> 01:30:11,232
Do you want to come?

503
01:30:12,354 --> 01:30:13,825
Don't worry about money.

504
01:30:15,482 --> 01:30:17,819
What are you talkin' about? What money?

505
01:30:17,901 --> 01:30:19,538
It doesn't matter.

506
01:30:21,738 --> 01:30:23,292
And what about work?

507
01:30:23,365 --> 01:30:25,417
Fuck work, Lanna.

508
01:30:26,451 --> 01:30:30,046
- We can go anywhere you like.
- I'm happy here.

509
01:30:30,122 --> 01:30:31,878
- Are you?
- Yeah.

510
01:30:31,957 --> 01:30:34,887
Everyone I know's here.
There's nothing wrong wi' here.

511
01:30:34,960 --> 01:30:37,510
It's the same crapness everywhere,
so stop dreaming.

512
01:30:53,812 --> 01:30:55,864
- Whisky, Morvern.
- Thanks a lot.

513
01:30:55,939 --> 01:30:57,991
- Yours was...?
- Coke.

514
01:30:58,066 --> 01:31:00,118
Thank you.

515
01:31:00,193 --> 01:31:01,949
<i>Ta.</i>

516
01:31:14,583 --> 01:31:16,635
I'm going to the toilet.

