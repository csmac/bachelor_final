1
00:00:57,800 --> 00:01:03,706
Visit Mike Vernon Motors
at 13631 Valley Boulevard...

2
00:01:03,773 --> 00:01:06,435
in Garden Grove.

3
00:01:06,509 --> 00:01:08,943
Now, for our report from
the freeways, to Don Edwards...

4
00:01:09,012 --> 00:01:11,606
and KBMJ's eye in the sky.
Come in, Don.

5
00:01:11,681 --> 00:01:17,484
Things seem to be pretty normal
on our southland freeways...

6
00:01:17,555 --> 00:01:20,991
which is to say they're about filled
to capacity at this hour of the morning.

7
00:01:21,058 --> 00:01:23,652
There's a stalled vehicle
in the number two lane...

8
00:01:23,728 --> 00:01:26,596
of the inbound San Diego Freeway
just south of Mulholland.

9
00:01:26,665 --> 00:01:30,032
We also have a report
of an accident half a mile west...

10
00:01:30,102 --> 00:01:33,071
of the 605 junction
of the Santa Ana Freeway.

11
00:01:33,138 --> 00:01:35,073
Both vehicles are now
up on the center divider.

12
00:01:35,141 --> 00:01:37,837
Traffic is still slow and go
in that area.

13
00:01:37,910 --> 00:01:40,777
Traffic is also heavily congested
on the westbound Ventura Freeway...

14
00:01:40,847 --> 00:01:42,781
from Woodman to Balboa
due to construction.

15
00:01:42,849 --> 00:01:47,446
We understand this condition
will last until 3:00 this afternoon.

16
00:01:47,521 --> 00:01:50,513
Don Edwards in Airwatch 3.
Back to you.

17
00:01:50,591 --> 00:01:53,526
Thanks, Don. The weather
for today, as promised...

18
00:01:53,594 --> 00:01:55,654
is a carbon copy of yesterday's:

19
00:01:55,730 --> 00:01:58,494
low overcast this morning,
clearing by 11:00.

20
00:01:58,566 --> 00:02:01,467
The National Weather Bureau
says the high today will be about 74...

21
00:02:01,536 --> 00:02:03,401
low tonight in the high 50s.

22
00:02:03,471 --> 00:02:05,871
At the beaches temperatures
will stay in the pleasant 70s.

23
00:02:05,940 --> 00:02:08,273
Water temperature
in the 60s.

24
00:02:08,344 --> 00:02:10,778
The APCD predicts light
to moderate eye irritation...

25
00:02:10,846 --> 00:02:13,906
as if you hadn't
already noticed. I have.

26
00:02:13,983 --> 00:02:16,509
You know how much
your dog likes meat?

27
00:02:16,586 --> 00:02:18,884
Well, make sure that's
what he's gett...

28
00:02:21,425 --> 00:02:24,121
...Grocery card double discount value
I mention each week...

29
00:02:24,194 --> 00:02:26,788
is not one of a kind.

30
00:02:26,863 --> 00:02:30,391
It's only one of hundreds of
double discounts always available...

31
00:02:30,468 --> 00:02:32,402
at Grocery Cart.
The giant 49...

32
00:02:34,172 --> 00:02:36,834
...At the first sign of any
hemorrhoidal discomfort...

33
00:02:36,908 --> 00:02:38,843
treatment should begin
at once.

34
00:02:38,911 --> 00:02:41,744
Remarkably successful results
have been obtained...

35
00:02:41,814 --> 00:02:45,341
with a doctor-proven medica...

36
00:02:54,327 --> 00:02:56,420
...5 to 3.

37
00:02:56,496 --> 00:02:58,658
St. Louis, 6,
the Giants, 4.

38
00:02:58,733 --> 00:03:01,031
Milwaukee defeated
the Royals, 4 to 3.

39
00:03:01,102 --> 00:03:03,468
Texas edged
the Yankees, 5 to 4.

40
00:03:03,537 --> 00:03:05,869
Pittsburgh and
the Padres rained out.

41
00:03:05,940 --> 00:03:08,534
The Dodgers and Angels
were both idle.

42
00:03:08,609 --> 00:03:10,976
In golf, Dave Brewer has
a one-stroke lead...

43
00:03:11,046 --> 00:03:13,378
after the first round
of the Toronto Classic.

44
00:03:13,448 --> 00:03:15,712
He shot a 5-under-par 67...

45
00:03:15,784 --> 00:03:18,344
with Lee Trevino
one stroke back at 68.

46
00:03:18,420 --> 00:03:22,619
Three others tied
four strokes off the pace at 71.

47
00:03:22,692 --> 00:03:26,355
Does your car needa new muffler
or new shock absorbers?

48
00:03:32,302 --> 00:03:34,930
You know
about these census forms...

49
00:03:35,005 --> 00:03:38,964
that the Census Bureau has sent
out to us all to fill in, right?

50
00:03:39,042 --> 00:03:41,102
That will be the basis of this.

51
00:03:41,179 --> 00:03:43,477
Census district office.

52
00:03:43,548 --> 00:03:48,212
I'd like some information.
I'm filling out my census form now...

53
00:03:48,286 --> 00:03:50,754
and I have an awful problem.

54
00:03:50,823 --> 00:03:53,621
- Can somebody help me?
- Go ahead.

55
00:03:53,692 --> 00:03:55,785
Thanks. Oh, you're
gonna answer my question?

56
00:03:55,861 --> 00:03:58,989
Oh, good. First of all,
I just wanted to say...

57
00:03:59,064 --> 00:04:01,932
I don't mind being counted
as an American.

58
00:04:02,002 --> 00:04:03,867
I'm one of the silent majority.

59
00:04:03,937 --> 00:04:06,872
But I wish you had made some of
those questions multiple choice.

60
00:04:06,940 --> 00:04:11,844
The question was,
are you the head of the family?

61
00:04:11,913 --> 00:04:15,872
Well, quite frankly,
the day I married that woman...

62
00:04:15,950 --> 00:04:20,319
that, unfortunately, I've been
married to for 25 years...

63
00:04:20,388 --> 00:04:25,884
Well, it's true. I lost the position
as head of the family.

64
00:04:25,961 --> 00:04:28,794
You see, what I do...
I stay at home. I hate working.

65
00:04:28,864 --> 00:04:30,627
I hate going out
and seeing people...

66
00:04:30,699 --> 00:04:33,134
and getting involved
in the rat race and things.

67
00:04:33,203 --> 00:04:36,036
So she works
and I do the housework...

68
00:04:36,105 --> 00:04:39,563
and take care of the babies
and things like that.

69
00:04:39,642 --> 00:04:41,371
And so, I was wondering...
you wanted honest answers.

70
00:04:42,645 --> 00:04:46,844
Now, what I did,
I pencilled in all of the marks...

71
00:04:46,917 --> 00:04:49,579
You wanted marks in these circles
I see in front of me.

72
00:04:49,653 --> 00:04:52,486
I pencilled it in first, but I said,
"No, that's being dishonest.

73
00:04:52,556 --> 00:04:54,422
"I'm really not
the head of the family...

74
00:04:54,493 --> 00:04:56,620
and yet I'm the man
of the family."

75
00:04:56,695 --> 00:04:58,925
Although there are people
who would question that.

76
00:04:58,997 --> 00:05:01,625
But nevertheless,
I was wondering...

77
00:05:01,700 --> 00:05:04,431
how should I answer
that question?

78
00:05:04,504 --> 00:05:07,371
Well, if you don't consider yourself
head of the household...

79
00:05:07,440 --> 00:05:09,670
and you think your wife
occupies that position...

80
00:05:09,742 --> 00:05:12,210
I would suggest you put
your wife's name there.

81
00:05:12,278 --> 00:05:15,646
But that's so embarrassing. What
will you people think when I send it?

82
00:05:15,716 --> 00:05:18,514
- Nobody is even going to know.
- Are you sure?

83
00:05:18,585 --> 00:05:20,519
I'm positive. No one
will know about that.

84
00:05:20,587 --> 00:05:22,521
We won't know you from
Adam's house cat.

85
00:05:22,589 --> 00:05:24,819
But there are people
in my neighborhood...

86
00:05:24,892 --> 00:05:27,384
- They will never see that form.
- Are you sure?

87
00:05:27,462 --> 00:05:30,625
No one in your neighborhood
will ever find out.

88
00:05:30,698 --> 00:05:33,462
I don't want people
to know about that.

89
00:05:33,535 --> 00:05:35,800
I've lived like this
for 15 years...

90
00:05:35,871 --> 00:05:38,237
and I admit,
I wear a house dress.

91
00:05:38,307 --> 00:05:41,834
It's so much easier to pick up
stuff in a house dress...

92
00:05:41,911 --> 00:05:43,640
And, you know, slippers.

93
00:05:43,712 --> 00:05:46,910
- And, quite frankly, I've adopted most
of the ways... - Talk about pollution.

94
00:05:46,983 --> 00:05:51,511
of the average housewife, and I'd
hate for anybody to know that.

95
00:05:51,588 --> 00:05:53,613
Anyway, it's very embarrassing,
so would you...

96
00:05:53,690 --> 00:05:57,991
All information that you include
on that form is confidential.

97
00:05:58,062 --> 00:06:00,155
It's unreasonable
to expect...

98
00:06:15,047 --> 00:06:18,040
...800,000 others.

99
00:06:18,117 --> 00:06:21,814
So it would be all right?
I wanna be honest with you.

100
00:06:21,888 --> 00:06:24,823
I'm really not the head of the family.
She doesn't know I'm calling.

101
00:06:24,891 --> 00:06:28,054
That woman just drives me
up the wall.

102
00:06:28,128 --> 00:06:30,790
I like doing this
just to get even with her.

103
00:06:36,003 --> 00:06:38,564
You know how women are before
you marry them. They're so nice.

104
00:06:38,640 --> 00:06:41,006
And suddenly they become
so aggressive.

105
00:06:41,076 --> 00:06:43,237
I mean, she became
so aggressive afterward.

106
00:06:43,311 --> 00:06:46,542
Just took over everything.
I mean...

107
00:06:46,615 --> 00:06:50,052
I'm afraid of her, you see.
I've been wanting to divorce her.

108
00:06:50,119 --> 00:06:53,054
The first six months of marriage,
I knew I made a mistake.

109
00:06:53,122 --> 00:06:55,647
But no, I got ahold of this first,
before she did...

110
00:06:55,725 --> 00:06:57,750
and I thought,
"Well, I'll answer it."

111
00:06:57,827 --> 00:07:00,592
But I wanted to be honest,
and I'm a man of conscience...

112
00:07:00,664 --> 00:07:03,690
so I wanted to make sure. If I put
down I'm the head of the family...

113
00:07:03,767 --> 00:07:05,792
nobody would know
the difference, right?

114
00:07:05,869 --> 00:07:10,330
I'll put it down...

115
00:07:10,408 --> 00:07:12,740
I really like you.
Are you married?

116
00:07:12,810 --> 00:07:14,971
- No.
- Oh. When you get married...

117
00:07:15,046 --> 00:07:17,537
don't be like
that whippo I married.

118
00:07:17,615 --> 00:07:19,480
Would you?

119
00:07:19,550 --> 00:07:22,315
- I'll try my best not to be.
- All right.

120
00:07:22,388 --> 00:07:24,754
- Thank you so much. It's nice
talking to you. - You're welcome.

121
00:07:24,823 --> 00:07:26,450
- Good-bye.
- Good-bye.

122
00:07:35,935 --> 00:07:37,926
This is Mr. Whittington.

123
00:07:38,004 --> 00:07:40,438
You were talking to a Mr...

124
00:07:40,507 --> 00:07:43,568
Oh, Valenti, the man
who blows bicycle pump...

125
00:07:43,644 --> 00:07:46,010
on the Ted Mack Original
Amateur Hour. Yes.

126
00:07:46,080 --> 00:07:48,014
Yes, well, I've...

127
00:07:48,082 --> 00:07:50,744
I've been trying to get
in touch with Mr. Mack.

128
00:07:50,818 --> 00:07:54,050
Because...
well, I've tried to...

129
00:08:05,835 --> 00:08:09,965
Well, I play music on
something unusual myself.

130
00:08:10,039 --> 00:08:13,203
We're always looking for
original amateur talent.

131
00:08:13,277 --> 00:08:16,508
Mr. Valenti is unusual
for the fact...

132
00:08:16,580 --> 00:08:20,710
that he not only blows
"Oh, Johnny" on the bicycle pump...

133
00:08:20,784 --> 00:08:24,448
but also does nail file
on a high, flat table...

134
00:08:24,522 --> 00:08:28,185
and also plays
the vacuum cleaner nozzle.

135
00:08:28,259 --> 00:08:30,819
"It's a Great Day
for the Irish."

136
00:08:30,895 --> 00:08:33,125
You say you do
something unusual?

137
00:08:33,198 --> 00:08:36,828
- Well, what do you do, may I ask?
- I play meat.

138
00:08:36,902 --> 00:08:38,802
- You what?
- I play meat.

139
00:08:38,871 --> 00:08:41,032
You play meat?

140
00:08:41,107 --> 00:08:45,772
- Yes, meat. You know, beef
and pork? - That's sick, man.

141
00:08:45,846 --> 00:08:48,314
- M-e-a-t, meat?
- Yes.

142
00:08:48,381 --> 00:08:51,214
It may sound a little strange.

143
00:08:51,284 --> 00:08:54,219
Yeah. I don't mean
to hurt your feelings,

144
00:08:54,287 --> 00:08:58,850
but I've never heard of anybody
who plays meat.

145
00:08:58,926 --> 00:09:02,623
- A lot of people think it's strange.
- Yes, I can understand that.

146
00:09:04,098 --> 00:09:06,795
- No offense. Are you serious
about this? - Yes, I've...

147
00:09:06,869 --> 00:09:08,734
worked here as
an assistant butcher...

148
00:09:08,804 --> 00:09:12,604
and I've found in a cold
storage locker there...

149
00:09:53,820 --> 00:09:56,983
Yes, sir. Whatever
you want, I got it.

150
00:09:57,056 --> 00:09:59,491
- What do you want?
- Fill it with Ethyl.

151
00:09:59,560 --> 00:10:01,926
If Ethel don't mind.

152
00:10:35,599 --> 00:10:39,866
- Want me to check under the hood
for you? - Please. Yes.

153
00:10:50,449 --> 00:10:53,077
Looks like you could use
a new radiator hose.

154
00:10:54,786 --> 00:10:58,517
Where have I heard that before?
I'll get one later.

155
00:10:58,590 --> 00:11:02,357
- You're the boss.
- Not in my house, I'm not.

156
00:11:13,908 --> 00:11:15,842
- Attendant?
- Yes, sir?

157
00:11:15,910 --> 00:11:19,676
- You got change for the telephone?
- Yes, sir.

158
00:11:24,686 --> 00:11:26,950
I'll get you the rest later?

159
00:11:27,022 --> 00:11:30,219
Be with you in a minute.

160
00:11:42,605 --> 00:11:47,373
Operator, I'd like to call
659-0716, collect.

161
00:11:48,612 --> 00:11:55,451
Dave Mann.
The number here is 238-2098.

162
00:11:56,721 --> 00:11:58,655
Thank you.

163
00:12:01,893 --> 00:12:03,828
Excuse me.

164
00:12:05,497 --> 00:12:09,627
Honey? It's me.

165
00:12:09,702 --> 00:12:14,538
- What's the matter? Did you have
an accident? - No, nothing like that.

166
00:12:14,607 --> 00:12:18,634
- Well, what happened?
- Well, nothing happened. I just...

167
00:12:18,712 --> 00:12:23,115
I just wanted to apologize.

168
00:12:23,183 --> 00:12:27,917
- You don't have to apologize, Dave.
- I know. I wanted to.

169
00:12:27,989 --> 00:12:30,253
When I left the house this morning,
you were asleep...

170
00:12:30,324 --> 00:12:33,452
so I just wanted to call you up
and tell you that...

171
00:12:33,527 --> 00:12:36,725
I'm sorry about last night.

172
00:12:36,798 --> 00:12:39,062
I don't really
want to talk about it.

173
00:12:39,134 --> 00:12:41,534
Well...

174
00:12:41,603 --> 00:12:43,969
Don't you think
maybe we ought to?

175
00:12:44,039 --> 00:12:46,338
No, because if we
talk about it...

176
00:12:46,409 --> 00:12:49,435
we'll fight, and you wouldn't
want that, would you?

177
00:12:49,512 --> 00:12:51,707
Of course not.

178
00:12:51,781 --> 00:12:54,113
What is that
supposed to mean?

179
00:12:54,183 --> 00:12:56,982
- Oh, never mind.
- Just a minute.

180
00:12:57,054 --> 00:12:59,284
I know what it's
supposed to mean.

181
00:12:59,356 --> 00:13:03,884
It means you think I should go
out and call Steve Henderson up...

182
00:13:03,961 --> 00:13:07,955
- and challenge him to a fistfight
or something. - No, of course not.

183
00:13:08,033 --> 00:13:11,560
But you could have at least
said something to the man.

184
00:13:11,636 --> 00:13:13,570
I mean, after all, he...

185
00:13:15,173 --> 00:13:18,234
practically tried to rape me
in front of the whole party.

186
00:13:18,310 --> 00:13:21,006
- Oh, come on, honey.
- Just forget it.

187
00:13:21,080 --> 00:13:24,481
- You gonna be home by 6:30?
- If Forbes lets me go in time.

188
00:13:24,550 --> 00:13:27,543
Is it that important
that you see him?

189
00:13:27,621 --> 00:13:29,851
He's leaving for Hawaii
in the morning.

190
00:13:29,923 --> 00:13:32,824
The way he's been griping to the front
office, if I don't reach him today...

191
00:13:32,893 --> 00:13:37,455
- I could lose the account. - You said
there'd be no problem getting home on time.

192
00:13:37,531 --> 00:13:40,126
There probably won't be.

193
00:13:40,201 --> 00:13:43,967
It's your mother. God knows
she's not coming to see me.

194
00:13:44,038 --> 00:13:47,838
Honey, I said there probably
won't be a problem.

195
00:13:47,909 --> 00:13:50,811
Well, just be on time, okay?

196
00:13:50,879 --> 00:13:55,407
All right.
Okay, I'll be there.

197
00:14:12,236 --> 00:14:14,170
Here's your card, sir.

198
00:14:15,573 --> 00:14:18,542
Be with you in a second.

199
00:14:20,912 --> 00:14:23,881
- Save them stamps?
- No, thanks.

200
00:14:23,949 --> 00:14:27,180
Good enough.
Come back, now.

201
00:14:27,252 --> 00:14:29,186
Will do.

202
00:15:53,446 --> 00:15:56,143
I gave you the road.
Why don't you take it?

203
00:15:59,653 --> 00:16:01,484
Why don't you go?

204
00:16:34,458 --> 00:16:37,986
Oh, boy, you're beautiful.

205
00:16:53,445 --> 00:16:55,470
I don't believe it.

206
00:16:57,049 --> 00:16:58,984
I don't believe it.

207
00:17:09,830 --> 00:17:13,391
I'm in no mood to play games.
Let's go.

208
00:17:41,765 --> 00:17:45,292
Well, it's about time, Charlie!

209
00:17:54,479 --> 00:17:57,107
My God.

210
00:17:58,650 --> 00:18:00,585
Jesus.

211
00:18:09,929 --> 00:18:14,230
Come on, you miserable fathead.
Get that fat-ass truck out of my way.

212
00:18:22,810 --> 00:18:25,745
Well, I'm never going to
make that appointment now.

213
00:18:46,770 --> 00:18:48,795
You miserable...

214
00:19:02,620 --> 00:19:08,321
Okay. Okay.
You want to play games.

215
00:25:33,815 --> 00:25:35,749
You all right, mister?

216
00:25:39,120 --> 00:25:43,080
Yeah. Yeah, except...

217
00:25:43,159 --> 00:25:46,094
- Oh, my neck.
- You got a whiplash, probably.

218
00:25:46,162 --> 00:25:49,290
Yeah. It's all right.

219
00:25:51,334 --> 00:25:53,963
He's all right.

220
00:25:56,139 --> 00:25:58,073
What happened?

221
00:26:00,277 --> 00:26:04,044
- That truck driver tried to kill me.
- Kill you? Go on!

222
00:26:04,115 --> 00:26:06,583
He chased me
down the mountain at...

223
00:26:06,651 --> 00:26:09,211
at nearly 90 miles an hour,
and I don't...

224
00:26:09,287 --> 00:26:13,122
- don't know what else you'd call it.
- Tried to kill you.

225
00:26:13,192 --> 00:26:17,322
It sure looks like you got
whiplash, all right.

226
00:26:17,396 --> 00:26:19,887
- You got the whiplash, all right.
- Oh, that's okay.

227
00:26:19,965 --> 00:26:21,899
Thank... It's okay.

228
00:26:21,967 --> 00:26:24,528
- Anything I can do for you?
- No, nothing. Thank you.

229
00:26:24,604 --> 00:26:26,663
- That's okay.
- Okay.

230
00:26:32,712 --> 00:26:35,409
Just a little whiplash is all.

231
00:27:28,074 --> 00:27:30,008
What happened
out there, mister?

232
00:27:30,076 --> 00:27:33,807
- Can I use your men's room, please?
- Yeah. Through the door...

233
00:27:33,879 --> 00:27:36,814
on the right,
down the hall...

234
00:27:36,882 --> 00:27:39,317
turn left, second door.

235
00:28:34,813 --> 00:28:39,750
Well, you never know.
You just never know.

236
00:28:39,819 --> 00:28:42,913
You just go along figuring
some things don't change, ever.

237
00:28:42,989 --> 00:28:46,755
Like being able to drive
on a public highway...

238
00:28:46,826 --> 00:28:49,920
without somebody trying
to murder you.

239
00:28:49,996 --> 00:28:53,933
And then one stupid thing
happens.

240
00:28:54,001 --> 00:28:57,596
Twenty, twenty-five minutes
out of your whole life...

241
00:28:57,671 --> 00:29:02,006
and all the ropes that kept you
hangin'in there get cut loose.

242
00:29:02,077 --> 00:29:07,140
And it's like, there you are,
right back in the jungle again.

243
00:29:08,950 --> 00:29:12,717
All right, boy, it was a nightmare,
but it's over now.

244
00:29:14,423 --> 00:29:16,857
It's all over.

245
00:29:37,915 --> 00:29:40,315
- Are you all right?
- Yeah, I'm fine.

246
00:29:40,384 --> 00:29:45,152
- What happened out there?
- Just a slight complication.

247
00:29:45,224 --> 00:29:48,660
Oh? Looked like a big
complication to me.

248
00:32:13,819 --> 00:32:17,152
Well, you ready to order now?

249
00:32:17,223 --> 00:32:19,384
Yes.

250
00:32:19,458 --> 00:32:21,689
Yes, thank you.

251
00:32:25,766 --> 00:32:28,326
Oh, I think I'll...

252
00:32:32,273 --> 00:32:34,332
Just give me...

253
00:32:34,409 --> 00:32:37,572
Why don't you just give me
a Swiss cheese sandwich...

254
00:32:37,645 --> 00:32:41,013
- on rye. R-Y-E.
- Swiss cheese on rye.

255
00:32:41,083 --> 00:32:44,018
- And could I have another
glass of water, please?

256
00:32:44,086 --> 00:32:47,249
- Sure. Another glass of water.
- Oh, miss?

257
00:32:47,323 --> 00:32:50,053
- Do you have an aspirin?
- Aw, your head aches.

258
00:32:50,125 --> 00:32:52,219
Sure, I'll get you some aspirin.

259
00:33:06,510 --> 00:33:09,775
All I did was pass this
stupid rig a couple of times...

260
00:33:09,847 --> 00:33:12,942
and he goes flyin'
off the deep end.

261
00:33:13,018 --> 00:33:15,782
He has to be crazy.

262
00:33:15,854 --> 00:33:20,484
Okay, so he's crazy.
What can I do about it?

263
00:33:20,559 --> 00:33:23,996
Find him a psychiatrist?

264
00:33:24,063 --> 00:33:26,122
Oh, boy.

265
00:34:12,349 --> 00:34:14,749
Now wait a minute.

266
00:34:14,818 --> 00:34:19,381
Now wait just a minute.
All right, now, think.

267
00:34:20,825 --> 00:34:24,693
Okay, he's in here.

268
00:34:24,762 --> 00:34:29,530
Well, that doesn't mean
he intends to continue his... attack.

269
00:34:29,601 --> 00:34:32,468
It is lunchtime...

270
00:34:32,538 --> 00:34:38,171
and Chuck's Cafe may be
the only place to eat for miles around.

271
00:34:38,244 --> 00:34:40,872
Yeah. He probably
eats here all the time.

272
00:34:40,947 --> 00:34:43,780
He was just...
just moving too fast before...

273
00:34:43,850 --> 00:34:47,787
and he had to... slow down,
turn around, that's all.

274
00:34:52,293 --> 00:34:54,227
That's all.

275
00:34:58,934 --> 00:35:01,835
Why didn't I leave right away
when I saw his truck outside?

276
00:35:01,903 --> 00:35:04,929
Then I'd know what
he intends to do.

277
00:35:07,108 --> 00:35:10,704
What if he followed me out,
started after me again?

278
00:35:10,780 --> 00:35:12,714
I'd be right back
where I started.

279
00:35:12,782 --> 00:35:15,342
Even if I got a lead,
he'd overtake me soon enough.

280
00:35:15,418 --> 00:35:18,650
He's got some... some...
some souped-up diesel.

281
00:35:18,722 --> 00:35:21,452
My car's just not
that powerful. I just...

282
00:35:21,525 --> 00:35:24,722
can't drive 80 and 90
miles an hour.

283
00:35:24,795 --> 00:35:28,789
As soon as I stopped concentrating, I'd
go back to 60 or 70 like I always do.

284
00:35:28,867 --> 00:35:30,698
It's a habit.
I can't help it.

285
00:35:30,768 --> 00:35:34,465
I ca.. Take it easy.

286
00:35:35,173 --> 00:35:39,166
Just... take it easy.

287
00:35:39,245 --> 00:35:41,179
Maybe I...

288
00:35:42,715 --> 00:35:46,412
Maybe I should try
some kind of contact.

289
00:35:46,485 --> 00:35:49,249
I'd better do something.

290
00:35:53,827 --> 00:35:55,988
Look, mister, I'm sorry
if I irritated you.

291
00:35:56,062 --> 00:36:00,397
Why don't I buy you a beer
and get this straightened out?

292
00:36:00,468 --> 00:36:02,333
No.

293
00:36:05,206 --> 00:36:08,369
Look, mister, I'm sorry
if I irritated you, but...

294
00:36:08,442 --> 00:36:10,376
let me...

295
00:36:22,191 --> 00:36:27,026
- There you are. Anything else?
- No, thank you.

296
00:36:29,666 --> 00:36:31,657
I'd like some ketchup.

297
00:36:54,427 --> 00:36:56,987
What if I called
the local police?

298
00:36:57,063 --> 00:36:59,759
But then I'd have to
stay here, lose more time.

299
00:36:59,832 --> 00:37:02,801
What if he stayed too? Actually
talked to the police himself?

300
00:37:02,868 --> 00:37:04,598
Naturally,
he'd deny everything.

301
00:37:04,671 --> 00:37:08,664
I've got to prove it. None of
these people would back me up.

302
00:37:08,742 --> 00:37:11,677
The cops'd probably...

303
00:38:26,960 --> 00:38:30,897
Okay... so now what?

304
00:39:28,461 --> 00:39:30,657
Look...

305
00:39:35,236 --> 00:39:39,002
- I want you to cut it out.
- What?

306
00:39:40,776 --> 00:39:43,711
Just cut it out, okay?

307
00:39:47,048 --> 00:39:51,748
- Cut what out?
- Now, come on. I mean...

308
00:39:51,821 --> 00:39:55,848
please, I...
Let's not play games.

309
00:39:55,925 --> 00:39:59,986
- What the hell are you talking about?
- I can call the police.

310
00:40:02,499 --> 00:40:06,993
- The police? - You think that I
won't? You're wrong, mister.

311
00:40:07,070 --> 00:40:09,834
If you think you can
take that truck of yours...

312
00:40:09,907 --> 00:40:15,210
and use it as a murder weapon, killing
people on the highway, well, you're wrong.

313
00:40:15,280 --> 00:40:19,273
- You've got another thing comin'.
- Man, you need help.

314
00:40:20,485 --> 00:40:23,011
Don't you tell me
I need help.

315
00:40:25,958 --> 00:40:28,586
Hey! Hey, come on!

316
00:40:28,661 --> 00:40:30,526
Who the hell
do you think you are...

317
00:40:30,596 --> 00:40:33,794
- knockin' my sandwich outta my
hand? - You wanna fight, get outside.

318
00:40:33,867 --> 00:40:37,701
- I don't wanna fight, I wanna knock
his head off. - You already hit him twice.

319
00:40:37,771 --> 00:40:40,740
What more do you want?
Come on now.

320
00:40:40,807 --> 00:40:43,743
He's sick. Can't you see?
He ain't gonna fight no one.

321
00:40:43,811 --> 00:40:47,577
That creep goes around,
knocks my sandwich outta my hand.

322
00:40:47,648 --> 00:40:49,582
I'll buy you
another sandwich.

323
00:40:49,650 --> 00:40:51,584
- Come on, forget it.
- No, forget it.

324
00:40:51,652 --> 00:40:54,053
Forget it. I don't wanna stay
around here anyway.

325
00:40:54,122 --> 00:40:57,216
Let me buy you
a beer then?

326
00:40:57,292 --> 00:40:59,226
I'll buy you a beer.

327
00:41:03,899 --> 00:41:05,925
Get out of here?

328
00:44:14,041 --> 00:44:17,041
I'm sorry to bother you,
but I could use help.

329
00:44:17,112 --> 00:44:22,542
- What kind of help? - I could use
a push. Damn thing overheated.

330
00:44:22,616 --> 00:44:24,110
Now I can't get her
started again.

331
00:44:24,286 --> 00:44:27,050
- Why didn't you flag down
that truck? - What truck?

332
00:44:27,122 --> 00:44:30,717
- The one that just went by here.
- I didn't notice.

333
00:44:30,792 --> 00:44:33,556
I must have been inside tryin' to
get that piece of junk shakin'.

334
00:44:33,628 --> 00:44:36,223
Would you get off the car?
How about it?

335
00:44:36,299 --> 00:44:40,565
Looks like I'd go
right underneath the bumper.

336
00:44:40,636 --> 00:44:44,163
Hey, get off the car.
You're gonna get hurt there.

337
00:44:44,240 --> 00:44:47,574
Naw, you're all right.
Rodney, get down from there.

338
00:44:47,644 --> 00:44:50,511
You too, Shawn. Come on, everybody!
Back on the bus!

339
00:44:50,581 --> 00:44:53,175
Wait a minute!
Wait, I...

340
00:44:53,250 --> 00:44:57,483
Well, we'll see.

341
00:45:15,274 --> 00:45:17,505
Come on. Come on.

342
00:45:41,270 --> 00:45:45,366
Push!

343
00:46:21,380 --> 00:46:23,371
He can't do it!

344
00:46:23,449 --> 00:46:26,748
You can't do it!
You can't do it!

345
00:46:26,819 --> 00:46:30,415
You can't do it!
You can't do it!

346
00:46:34,595 --> 00:46:38,053
Mr. Pfeiffer,
that guy's stuck too.

347
00:46:43,137 --> 00:46:45,162
You can't do it!

348
00:46:50,913 --> 00:46:54,440
Sorry, mister.
I coulda swore we were okay.

349
00:46:54,516 --> 00:46:58,452
- I told you we were gonna get hung up.
- How 'bout I give you a hand?

350
00:46:58,520 --> 00:47:01,251
Don't, don't,
don't sit on the hood.

351
00:47:01,324 --> 00:47:04,521
That hood'll dent. I told the kids
not to get on the hood.

352
00:47:06,429 --> 00:47:09,523
Just see if you
can bounce it loose...

353
00:47:09,599 --> 00:47:12,034
and I'll...

354
00:47:13,938 --> 00:47:15,872
Just bounce it loose.

355
00:47:54,649 --> 00:47:58,449
Hold it!

356
00:48:13,936 --> 00:48:16,030
Somethin' wrong?

357
00:48:18,542 --> 00:48:21,739
That bastard turned around
and came back.

358
00:48:21,812 --> 00:48:24,542
Is that the truck
you were askin' about?

359
00:48:29,787 --> 00:48:33,223
Get those kids
out of the way.

360
00:48:33,291 --> 00:48:36,056
- What for? - Just get them off
the road and they'll be okay.

361
00:48:36,128 --> 00:48:38,494
They're all right.
They're not on the road.

362
00:48:38,564 --> 00:48:42,330
Get, get back
in the bus. Please!

363
00:48:42,401 --> 00:48:44,164
Come on. Back in the bus!

364
00:48:44,236 --> 00:48:46,262
You're all right
right where you are...

365
00:48:46,339 --> 00:48:48,933
long as you stay off the road.
Come on!

366
00:48:49,008 --> 00:48:52,944
- You're perfectly all right.
- Let's get back in the bus, come on.

367
00:48:53,012 --> 00:48:55,981
Get off of the highway.
There's a truck coming.

368
00:48:56,049 --> 00:48:59,050
You must be
out of your brains!

369
00:48:59,920 --> 00:49:03,321
- Come on! Please!
- Take your hands off these...

370
00:49:03,391 --> 00:49:05,325
- Get back in the bus!
- Get your hands off him!

371
00:49:05,393 --> 00:49:08,261
Now, look, I know
how this must sound,

372
00:49:08,330 --> 00:49:11,265
but that man is crazy.
He's been trying to kill me.

373
00:49:11,333 --> 00:49:14,860
If I had to vote on who's crazy,
it'd be you.

374
00:49:17,407 --> 00:49:21,036
When I bust 'em loose,
pull it back.

375
00:49:31,054 --> 00:49:32,817
Now!

376
00:56:06,387 --> 00:56:09,550
Hi.
Help you, mister?

377
00:56:09,624 --> 00:56:12,559
- You got a telephone?
- Out in back.

378
00:56:12,627 --> 00:56:16,358
- This way?
- Something for your car?

379
00:56:16,431 --> 00:56:19,162
Well, you can...

380
00:56:19,235 --> 00:56:22,693
- put what Ethyl you can get in
the tank. - All righty.

381
00:56:28,078 --> 00:56:31,605
- Would you mind checking those
radiator hoses? - I'll do that.

382
00:56:31,681 --> 00:56:35,412
Take a look at my snakes
if you have time.

383
00:56:43,895 --> 00:56:46,557
Weird place
for a telephone booth.

384
00:56:56,341 --> 00:57:00,005
I'd like to report a truck driver
that's been endangering my life.

385
00:57:00,079 --> 00:57:04,516
- In that case, I'll have to give you the
police, sir. - Right. Give me the police.

386
00:57:04,584 --> 00:57:08,850
- Sir, which department do you want?
- Whichever's closer.

387
00:57:08,922 --> 00:57:13,656
- What number are you calling from?
- This number is 9821.

388
00:57:32,048 --> 00:57:34,448
- What's your name, sir?
- David Mann.

389
00:57:34,517 --> 00:57:37,918
- How do you spell that, please?
- M-A-N-N. That's two "N"s.

390
00:57:40,657 --> 00:57:42,784
I'd like to report...

391
00:57:42,859 --> 00:57:45,555
a truck driver that's
been endangering my life.

392
00:57:45,629 --> 00:57:48,393
- Your name again?
- David Mann.

393
00:58:01,112 --> 00:58:03,013
Why'd he do that?

394
00:58:03,082 --> 00:58:05,676
Why'd he break
my cages up?

395
00:58:35,283 --> 00:58:37,217
Why'd he break
my cages up?

396
00:58:37,285 --> 00:58:39,219
- Call the police!
- With what?

397
00:58:39,287 --> 00:58:41,414
That's the only phone
I've got!

398
00:58:41,490 --> 00:58:45,291
My snakes!
I've got to find my snakes!

399
00:58:52,735 --> 00:58:57,673
Oh, my snakes.
My snakes.

400
01:00:07,983 --> 01:00:11,680
The highway's all yours, Jack.

401
01:00:11,755 --> 01:00:15,213
I'm not budgin'
for at least an hour.

402
01:00:15,292 --> 01:00:18,228
Maybe the police
will pull you in by then.

403
01:00:18,630 --> 01:00:23,260
Maybe they won't, but at least
you'll be far away from me.

404
01:00:25,337 --> 01:00:31,243
"Well, dear, did you
have a nice trip?"

405
01:00:31,310 --> 01:00:36,270
"No. Just the, same old thing."

406
01:00:43,490 --> 01:00:48,325
Well, I won't be seein'
Forbes today, that's for sure.

407
01:07:44,818 --> 01:07:46,615
What's the matter,
car trouble?

408
01:07:46,686 --> 01:07:48,985
In a way, yes.
Will you do me a favor?

409
01:07:49,056 --> 01:07:51,616
- What's that? - Would you stop
at the nearest telephone...

410
01:07:51,692 --> 01:07:53,626
- and call the police?
- Police?

411
01:07:53,694 --> 01:07:56,288
- You see that truck?
- Mister, we don't want any trouble.

412
01:07:56,364 --> 01:07:58,298
- There won't be any trouble.
- Oh, yeah, I see him.

413
01:07:58,366 --> 01:08:01,063
- Just call the police.
- Was there an accident?

414
01:08:01,136 --> 01:08:03,696
All I'm asking you to do
is just make a phone call.

415
01:08:03,772 --> 01:08:06,138
- Jim, step on the pedal.
- My life's in danger!

416
01:08:06,208 --> 01:08:08,301
- I'm sorry, mister...
- Can't you make a lousy call...

417
01:08:08,377 --> 01:08:11,643
- to the police?
- Stop it, mister. You're scaring us.

418
01:08:11,714 --> 01:08:13,978
- Drop me at the nearest station.
- I'll pay you for it!

419
01:08:14,050 --> 01:08:16,746
Jim!

420
01:10:23,491 --> 01:10:27,519
Okay, let's see you
catch me now.

421
01:10:30,733 --> 01:10:32,928
Here we go.

422
01:10:55,026 --> 01:10:58,724
Can't beat me on the grade.
You can't beat me on the grade!

423
01:13:52,954 --> 01:13:54,979
Come on.

424
01:14:33,598 --> 01:14:36,965
How can he go so fast?

425
01:18:08,868 --> 01:18:10,859
The radiator hose!

426
01:18:22,849 --> 01:18:25,511
Oh, no, please! No!

427
01:18:37,932 --> 01:18:40,060
Oh, my God!

428
01:18:48,444 --> 01:18:51,039
Come on, faster!

429
01:19:19,778 --> 01:19:22,247
Oh, my God.

430
01:19:30,490 --> 01:19:34,791
Come on!
Come on.

431
01:19:51,546 --> 01:19:53,481
Please.

432
01:19:55,684 --> 01:19:57,879
Come on.

433
01:20:00,222 --> 01:20:03,283
Come on, car.
Come on, let's go!

434
01:20:03,360 --> 01:20:05,191
Come on!

435
01:20:13,036 --> 01:20:15,801
Where's the summit?

436
01:20:15,873 --> 01:20:18,865
Please. Please.

437
01:20:22,079 --> 01:20:24,913
Come on!

438
01:20:25,918 --> 01:20:28,079
Come on!

439
01:20:34,260 --> 01:20:36,592
There it is.
There it is!

440
01:21:10,800 --> 01:21:13,769
Faster. Faster!

