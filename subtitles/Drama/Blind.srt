1
00:00:52,840 --> 00:00:54,592
<i>Now then! We will begin.</i>

2
00:00:55,360 --> 00:01:00,559
<i>When the story is done maybe
we'll know more than we do now.</i>

3
00:01:36,560 --> 00:01:37,709
<i>Go away!</i>

4
00:01:39,160 --> 00:01:40,752
Don't look at me.

5
00:01:41,800 --> 00:01:45,952
Don't... Don't. I don't want to.

6
00:01:47,960 --> 00:01:51,839
No, I don't want it.
- You don't have any choice.

7
00:01:52,400 --> 00:01:56,951
Clean outside, clean inside.
That's what the priest says.

8
00:02:12,320 --> 00:02:13,389
Ann.

9
00:02:14,240 --> 00:02:15,798
He bit me!

10
00:02:58,320 --> 00:02:59,958
Leave me alone.

11
00:03:06,880 --> 00:03:08,711
You can't look at me.

12
00:03:10,200 --> 00:03:12,031
Don't look. No.

13
00:03:17,080 --> 00:03:17,830
Sshhh.

14
00:03:40,440 --> 00:03:42,351
Sshhh. Ruben.

15
00:03:44,920 --> 00:03:47,229
They can't look. They mustn't.

16
00:03:49,040 --> 00:03:50,439
Now calm down.

17
00:03:52,240 --> 00:03:53,593
Victor won't be long.

18
00:03:53,880 --> 00:03:56,519
No. No. No.

19
00:03:56,680 --> 00:03:58,955
I won't have it. I won't have it.

20
00:03:59,480 --> 00:04:00,993
No Victor any more.

21
00:04:08,880 --> 00:04:12,475
I'll stand up. I shall leave.

22
00:05:49,120 --> 00:05:50,473
My name is Marie.

23
00:05:57,000 --> 00:06:01,152
You have to know that this post
is not easy, madam. My son is....

24
00:06:01,960 --> 00:06:03,552
Perseverance is needed.

25
00:06:04,880 --> 00:06:06,472
You have a lovely voice.

26
00:06:17,240 --> 00:06:18,639
Please follow me.

27
00:06:28,040 --> 00:06:29,075
Ruben?

28
00:06:31,560 --> 00:06:32,595
Ruben?

29
00:06:37,560 --> 00:06:39,869
May I introduce you to someone?

30
00:06:42,960 --> 00:06:45,838
Can I introduce you to someone? Ruben.

31
00:06:46,000 --> 00:06:49,788
She isn't here to wash and dress you.
You have to do that.

32
00:06:49,960 --> 00:06:51,359
Marie reads.

33
00:06:58,480 --> 00:07:00,596
Can I open the curtain?

34
00:07:08,760 --> 00:07:10,034
It would be...
- No.

35
00:07:13,800 --> 00:07:15,552
Ruben, no one will stay.

36
00:08:02,200 --> 00:08:03,952
I'll pay you in advance.

37
00:08:04,560 --> 00:08:06,437
Where's the library?

38
00:08:07,320 --> 00:08:09,675
I'd like to read the books at home.

39
00:08:09,840 --> 00:08:13,310
If you promise me
you'll persevere for a month.

40
00:09:37,680 --> 00:09:38,829
I didn't hear you.

41
00:09:40,520 --> 00:09:41,999
Here, take this one.

42
00:10:56,000 --> 00:10:57,115
You smell nice.

43
00:10:58,840 --> 00:11:01,195
I came to read
and you won't ruin it.

44
00:11:01,360 --> 00:11:04,909
Stop moving and I'll let go.
Stop moving.

45
00:11:09,160 --> 00:11:10,115
Bitch.

46
00:11:11,000 --> 00:11:14,231
If you ever raise a finger to me...

47
00:11:24,840 --> 00:11:25,716
Go away.

48
00:11:27,800 --> 00:11:28,994
You have to go.

49
00:11:41,840 --> 00:11:43,796
Now then! We will begin...

50
00:11:56,320 --> 00:11:58,515
When the story is done...

51
00:12:46,440 --> 00:12:48,829
You don't treat books like that.

52
00:12:55,560 --> 00:12:57,357
Did he behave?

53
00:12:59,320 --> 00:13:00,355
- Yes. -

54
00:13:01,000 --> 00:13:02,433
Tomorrow?

55
00:13:05,080 --> 00:13:08,231
This one's called Marie, isn't she?
- Yes.

56
00:13:09,720 --> 00:13:10,914
And what else?

57
00:13:11,800 --> 00:13:13,358
She didn't say.

58
00:13:14,640 --> 00:13:16,312
She won't stay either.

59
00:13:18,560 --> 00:13:22,189
Don't look. Don't look. Don't look.

60
00:13:28,440 --> 00:13:30,237
You won't get rid of me.

61
00:13:45,320 --> 00:13:47,231
Now then! We will begin.

62
00:13:47,400 --> 00:13:51,916
When the story is done maybe
we'll know more than we do now.

63
00:13:52,720 --> 00:13:54,278
There was a mirror...

64
00:13:54,440 --> 00:13:59,230
...in which everything good and beautiful
dwindled to nothing...

65
00:13:59,400 --> 00:14:04,952
...while everything worthless and ugly
became more conspicuous and uglier.

66
00:14:05,120 --> 00:14:08,430
Only now could one see
how people really looked.

67
00:14:08,600 --> 00:14:12,115
The grinning mirror trembled
with all that violence...

68
00:14:12,280 --> 00:14:16,910
...that it shattered into millions of bits
that rained on the earth.

69
00:14:17,960 --> 00:14:19,791
I could try another book.

70
00:14:26,880 --> 00:14:31,237
If someone got a splinter in their heart,
it turned to ice.

71
00:14:31,360 --> 00:14:32,998
In the big city lived...

72
00:14:34,080 --> 00:14:36,310
I'll get another book.
- Go on.

73
00:14:36,680 --> 00:14:39,353
They weren't siblings,
but loved each.

74
00:14:39,520 --> 00:14:42,557
You already know it.
- You're paid to read. Read.

75
00:14:51,400 --> 00:14:53,789
In the big city lived two children.

76
00:14:53,960 --> 00:14:57,555
They weren't siblings,
but loved each other as much.

77
00:14:57,720 --> 00:15:00,314
His name was Kay and hers was Gerda.

78
00:15:00,520 --> 00:15:02,272
Outside snow fell.

79
00:15:02,440 --> 00:15:05,557
''They're white bees,''
the old grandmother said.

80
00:15:05,720 --> 00:15:08,473
''Do they have a queen bee?'' Kay asked.

81
00:15:09,080 --> 00:15:13,119
''Yes,'' said the grandmother,
the Snow Queen.

82
00:15:13,520 --> 00:15:16,239
She looks inside on winter nights.

83
00:15:16,480 --> 00:15:20,029
Then they freeze as beautifully
as if flowers...

84
00:15:31,120 --> 00:15:33,315
Kay only saw ugliness.

85
00:15:33,520 --> 00:15:35,556
A shard had pierced his eye.

86
00:15:35,720 --> 00:15:37,199
''What ugly roses.''

87
00:15:37,360 --> 00:15:41,797
''Ouch,'' said Kay. ''It's as if
I was stabbed in the heart.''

88
00:15:42,800 --> 00:15:45,917
Kay had a shard in his chest too.

89
00:15:46,120 --> 00:15:48,759
His heart slowly turned to ice.

90
00:15:48,920 --> 00:15:51,229
''Why are you crying?'' called Kay.

91
00:15:51,400 --> 00:15:53,994
''You look so ugly, Gerda.''

92
00:15:54,360 --> 00:15:55,315
Ugly.

93
00:15:56,400 --> 00:15:57,719
Ugly.

94
00:15:58,880 --> 00:16:00,359
You're ugly.

95
00:16:01,200 --> 00:16:02,519
You're so ugly.

96
00:16:02,760 --> 00:16:04,432
Ugly, you are.

97
00:16:06,400 --> 00:16:07,469
You are...

98
00:16:09,640 --> 00:16:10,755
...ugly.

99
00:16:18,520 --> 00:16:20,511
How could I bear such a child?

100
00:16:21,720 --> 00:16:23,517
Everything about you is ugly.

101
00:16:24,240 --> 00:16:26,515
Everything. Stupid look.

102
00:16:27,120 --> 00:16:28,553
Stupid look!

103
00:16:29,400 --> 00:16:33,757
Wear your hair in front of your face
- You don't radiate pride...

104
00:16:33,920 --> 00:16:37,515
No eyebrows, your hair is dull...
- Why are your teeth so ugly?

105
00:16:37,680 --> 00:16:40,797
Bad posture, ugly knees...
- Nothing glistens.

106
00:16:40,960 --> 00:16:44,077
You need a long dress
so we can't see your legs.

107
00:16:44,240 --> 00:16:46,037
You're ugly. Ugly!

108
00:17:06,360 --> 00:17:08,032
Do they ever answer back?

109
00:17:11,480 --> 00:17:14,677
They smell nice.
Smell the leather, the paper.

110
00:17:31,920 --> 00:17:33,592
People stink.

111
00:17:45,960 --> 00:17:47,518
What do you look like?

112
00:17:51,160 --> 00:17:53,116
Is your hair dark or light?

113
00:17:57,600 --> 00:17:58,555
Red.

114
00:18:00,200 --> 00:18:01,269
Red hair.

115
00:18:02,880 --> 00:18:05,553
Yes, and long.

116
00:18:10,440 --> 00:18:11,919
Green or brown eyes?

117
00:18:12,360 --> 00:18:13,634
Green.

118
00:18:15,680 --> 00:18:16,908
And your mouth?

119
00:18:36,560 --> 00:18:39,120
She was beautiful, but made of ice.

120
00:18:39,280 --> 00:18:41,350
Of blinding, glistening ice.

121
00:18:42,160 --> 00:18:44,196
The Snow Queen kissed Kay.

122
00:18:53,080 --> 00:18:57,119
The kiss penetrated to his heart
that was already ice.

123
00:19:00,080 --> 00:19:03,152
It was as if he were dying.
- Anything wrong?

124
00:19:07,720 --> 00:19:12,271
The Ice Queen kissed Kay again
and then he forgot about Gerda.

125
00:19:21,440 --> 00:19:22,793
Do we have to?

126
00:19:23,200 --> 00:19:25,760
Yes, your mother is watching you.

127
00:19:28,040 --> 00:19:29,792
Pretend you like it.

128
00:19:30,520 --> 00:19:33,956
Two days ago we boarded
a ship with negro slave girls...

129
00:19:34,120 --> 00:19:35,917
I don't need fresh air.

130
00:19:36,160 --> 00:19:40,358
As dress, they wore amulets
and little leather breeches.

131
00:19:41,920 --> 00:19:43,990
We bought a few...

132
00:19:44,200 --> 00:19:47,317
Not women, but their breeches.

133
00:19:48,120 --> 00:19:53,194
We negotiated about ostrich feathers
and a little Abyssinian girl...

134
00:19:53,360 --> 00:19:58,514
...to stay aboard longer and
enjoy the spectacle.

135
00:21:46,760 --> 00:21:48,512
What does she look like?

136
00:21:49,520 --> 00:21:50,635
Marie?

137
00:21:51,040 --> 00:21:51,995
- Yes. -

138
00:21:54,600 --> 00:21:55,715
You know...

139
00:21:56,360 --> 00:21:57,839
Does she have red hair?

140
00:22:01,280 --> 00:22:03,510
You could see it that way...

141
00:22:04,040 --> 00:22:05,109
Green eyes?

142
00:22:12,000 --> 00:22:13,399
I believe so.

143
00:22:15,040 --> 00:22:16,632
She's beautiful, isn't she?

144
00:22:16,800 --> 00:22:18,916
Yes, lovely.

145
00:22:20,800 --> 00:22:22,119
I knew it.

146
00:22:44,960 --> 00:22:45,915
Ruben?

147
00:22:47,560 --> 00:22:49,676
This rug has to go. It stinks.

148
00:22:49,880 --> 00:22:51,029
My boy, let me do that.

149
00:22:51,200 --> 00:22:53,031
I can do it.
- Careful...

150
00:22:53,200 --> 00:22:54,189
I told you so.

151
00:24:29,800 --> 00:24:31,711
You have cold hands.

152
00:24:32,040 --> 00:24:32,995
No...

153
00:24:33,320 --> 00:24:35,914
Otherwise I can't see you.
- That's a good thing.

154
00:24:36,080 --> 00:24:37,035
Why?

155
00:24:45,640 --> 00:24:47,073
I said: no.

156
00:24:49,400 --> 00:24:51,118
You're very beautiful.

157
00:25:00,640 --> 00:25:04,792
Did your mother tell you that?
- No, I can see that by myself.

158
00:25:12,840 --> 00:25:14,159
Frost flowers...

159
00:25:16,160 --> 00:25:17,912
Where did you get them?

160
00:25:19,600 --> 00:25:20,919
You smell nice.

161
00:25:21,320 --> 00:25:22,753
You stink...

162
00:25:31,920 --> 00:25:32,875
Marie.

163
00:25:36,120 --> 00:25:38,839
Marie.
Don't leave, Marie.

164
00:25:44,040 --> 00:25:45,393
Go inside.

165
00:25:49,760 --> 00:25:51,034
Go on then.

166
00:27:18,400 --> 00:27:20,994
Can you see anything?
- No, boy.

167
00:27:36,520 --> 00:27:38,715
And now? Can you see anything?

168
00:28:10,880 --> 00:28:14,270
Shall I ask Victor to come?
- No. Please don't.

169
00:28:14,440 --> 00:28:15,668
All right...

170
00:28:21,520 --> 00:28:23,112
Shall I read to you?

171
00:28:26,760 --> 00:28:28,512
I touched her.

172
00:28:28,720 --> 00:28:30,199
That's how you look.

173
00:28:32,800 --> 00:28:34,756
What else did you do?

174
00:28:39,680 --> 00:28:41,636
We'll think of something.

175
00:28:48,040 --> 00:28:50,508
Come on, let's lie in my bed.

176
00:29:02,600 --> 00:29:05,068
Don't you want to know where I was?

177
00:29:05,320 --> 00:29:07,550
You don't like to be touched.

178
00:29:15,880 --> 00:29:17,279
Wash yourself.

179
00:29:22,960 --> 00:29:24,075
Wash.

180
00:29:24,840 --> 00:29:27,673
Don't depend on anyone,
you'll be better off.

181
00:29:32,360 --> 00:29:33,713
I can't see anything.

182
00:29:33,880 --> 00:29:34,949
So what?

183
00:29:35,400 --> 00:29:36,674
Here. Wash.

184
00:29:37,240 --> 00:29:38,958
If you tell a story.

185
00:29:40,720 --> 00:29:42,199
I won't wash you.

186
00:29:42,360 --> 00:29:44,351
No, me wash, you story.

187
00:29:54,680 --> 00:29:55,635
Left...

188
00:29:56,560 --> 00:29:57,834
A little more...

189
00:30:05,200 --> 00:30:06,792
You can look.

190
00:32:06,080 --> 00:32:07,195
Mama?

191
00:32:13,240 --> 00:32:14,639
What happened?

192
00:32:40,720 --> 00:32:41,994
Has Victor left?

193
00:32:44,920 --> 00:32:45,909
No.

194
00:32:47,120 --> 00:32:48,109
Fine.

195
00:32:48,280 --> 00:32:52,273
Ruben, can I look at your eyes
while I'm here?

196
00:32:55,760 --> 00:32:58,797
What if we ask Marie
to come and live here?

197
00:32:59,640 --> 00:33:01,358
She can help Mama too.

198
00:33:02,600 --> 00:33:04,750
Fine. Fine.

199
00:33:05,800 --> 00:33:07,153
I'll ask.

200
00:33:33,040 --> 00:33:36,237
We're going outside.
- Now? Where were you?

201
00:33:58,640 --> 00:33:59,755
Are you all right?

202
00:34:05,040 --> 00:34:07,156
I'm blind with gloves on.

203
00:34:17,400 --> 00:34:20,631
It's just like glass.
- I've seen ice.

204
00:34:48,480 --> 00:34:49,708
Marie.

205
00:34:54,200 --> 00:34:56,589
My name is really Neeltje Marie.

206
00:35:14,120 --> 00:35:15,997
My hands are frozen.

207
00:35:27,400 --> 00:35:30,676
I may ask you to come
and live with us, Marie.

208
00:35:36,320 --> 00:35:38,151
You have to say yes.

209
00:36:07,320 --> 00:36:08,878
You have to say ''yes''.

210
00:36:09,720 --> 00:36:10,675
No!

211
00:36:14,000 --> 00:36:15,069
Yes!
- No.

212
00:36:15,240 --> 00:36:16,275
Yes!

213
00:37:11,400 --> 00:37:13,709
D'you have a suitcase with you?

214
00:37:14,080 --> 00:37:15,274
- Yes. -

215
00:37:21,920 --> 00:37:24,070
I'll show you your room.

216
00:37:25,280 --> 00:37:28,238
If you don't like this room, we have more.

217
00:37:29,800 --> 00:37:31,472
Yes, this is fine.

218
00:39:30,720 --> 00:39:31,948
Ruben?

219
00:39:37,640 --> 00:39:38,755
Ruben?

220
00:39:45,640 --> 00:39:47,278
RUBEN!

221
00:39:49,120 --> 00:39:50,838
I can't sleep.

222
00:39:51,840 --> 00:39:53,398
Come and sit with me.

223
00:40:11,680 --> 00:40:13,193
Do you still love me?

224
00:40:16,480 --> 00:40:17,595
Mama.

225
00:41:15,520 --> 00:41:16,794
I want to see you.

226
00:41:42,520 --> 00:41:43,794
You have even more.

227
00:41:44,960 --> 00:41:46,439
Where did you get them?

228
00:42:08,360 --> 00:42:10,191
You don't stink any more.

229
00:42:19,880 --> 00:42:22,269
You don't look the way I thought.

230
00:42:25,920 --> 00:42:27,478
Much more beautiful.

231
00:46:03,680 --> 00:46:05,193
How old are you?

232
00:46:07,240 --> 00:46:08,798
21 ...

233
00:46:31,360 --> 00:46:34,432
I'm so sorry.
- No, please don't get up.

234
00:46:35,520 --> 00:46:36,669
Here you are.

235
00:46:58,600 --> 00:46:59,919
This is eye liner.

236
00:47:15,160 --> 00:47:17,720
You have performed your task well.

237
00:47:20,560 --> 00:47:22,073
I am grateful to you.

238
00:47:26,080 --> 00:47:28,389
Don't forget you are here to read.

239
00:47:28,560 --> 00:47:29,834
Nothing more.

240
00:47:50,120 --> 00:47:51,235
How are you?

241
00:47:53,000 --> 00:47:55,275
It's more difficult to walk.

242
00:47:56,480 --> 00:47:58,835
My legs have suddenly given up on me.

243
00:47:59,800 --> 00:48:01,836
My fingers too.

244
00:48:02,120 --> 00:48:04,953
I dropped that lovely bottle of yours.

245
00:48:16,320 --> 00:48:19,869
Did you think about what I said?

246
00:48:22,440 --> 00:48:24,032
I don't dare believe it.

247
00:48:24,200 --> 00:48:27,510
Believe me, Catherine,
this life can be changed.

248
00:48:27,680 --> 00:48:32,276
We live in a new era.
We need the courage to act on it.

249
00:48:32,480 --> 00:48:34,198
God, that would be great.

250
00:48:34,360 --> 00:48:37,830
I can write a letter.
He will be able to see.

251
00:49:25,960 --> 00:49:27,712
Do you remember colour?

252
00:49:31,080 --> 00:49:31,990
Yes.

253
00:49:32,760 --> 00:49:36,753
My father practised
when we knew I was going blind.

254
00:49:38,680 --> 00:49:40,432
What does red look like?

255
00:49:45,840 --> 00:49:47,159
Like this.

256
00:49:53,320 --> 00:49:54,719
What will they read?

257
00:50:26,240 --> 00:50:28,037
Where are we going?

258
00:50:28,400 --> 00:50:31,233
Does it matter?
- No.

259
00:50:35,240 --> 00:50:36,593
What about Mama?

260
00:50:40,400 --> 00:50:42,595
You want to stay here forever?

261
00:50:46,440 --> 00:50:49,193
I want to go to Istanbul... and you?

262
00:50:51,560 --> 00:50:54,597
Come on, surprise...
- Surprise?

263
00:50:56,480 --> 00:50:58,675
Shall I shut my eyes?
- Yes.

264
00:51:11,560 --> 00:51:13,039
You can open them now.

265
00:51:18,360 --> 00:51:20,396
What's the surprise, then?

266
00:51:22,760 --> 00:51:24,432
Victor, this is Marie.

267
00:51:24,600 --> 00:51:26,909
Marie, Doctor Verbeeke.

268
00:51:29,200 --> 00:51:31,270
Well you are...
- Ruben...

269
00:51:32,120 --> 00:51:34,429
Victor has very good news.

270
00:52:14,200 --> 00:52:15,918
You live in a lie.

271
00:52:19,080 --> 00:52:21,548
Who's going to tell him the truth?

272
00:52:27,520 --> 00:52:28,839
He sees it...

273
00:52:30,040 --> 00:52:31,871
...and he's the only one.

274
00:52:33,600 --> 00:52:35,989
You mean that love's blind?

275
00:52:56,800 --> 00:52:58,119
Are you afraid?

276
00:52:59,040 --> 00:53:00,109
Nervous.

277
00:53:01,240 --> 00:53:02,355
A little scared.

278
00:53:05,680 --> 00:53:07,272
I'm sure it'll work.

279
00:53:18,600 --> 00:53:20,670
How does the fairytale end?

280
00:53:21,160 --> 00:53:22,434
The Snow Queen?

281
00:53:24,920 --> 00:53:27,229
Happily ever after, of course.

282
00:54:55,720 --> 00:54:57,358
Aren't you cold?

283
00:54:59,040 --> 00:54:59,995
No.

284
00:55:23,480 --> 00:55:24,799
I love you.

285
00:56:08,920 --> 00:56:10,148
Marie.

286
00:56:12,360 --> 00:56:13,315
Marie.

287
00:56:14,480 --> 00:56:15,629
Where are you?

288
00:56:17,040 --> 00:56:17,995
Marie.

289
00:56:20,920 --> 00:56:21,955
Marie.

290
00:57:01,760 --> 00:57:05,230
We shall anaesthetize your eyes
with a new drug.

291
00:57:05,400 --> 00:57:06,753
Cocaine.

292
00:57:10,800 --> 00:57:12,756
You won't feel much.

293
00:57:32,120 --> 00:57:33,473
Is Marie there?

294
00:57:34,800 --> 00:57:36,472
After you, colleague.

295
00:57:36,640 --> 00:57:39,871
My compliments. Truly wonderful.

296
00:57:54,440 --> 00:57:56,078
And now we wait.

297
00:57:56,640 --> 00:57:59,791
But it looks hopeful. Very hopeful.

298
00:58:05,480 --> 00:58:07,277
He bit me.
- Where?

299
00:58:08,120 --> 00:58:09,838
Untie me! Let me out of here.

300
00:58:10,000 --> 00:58:15,074
Don't move. Mr Rietlander. You don't
want to go blind again? Calm down!

301
00:58:15,360 --> 00:58:18,557
I want to see her.
- Later Ruben. Later.

302
00:58:19,360 --> 00:58:22,193
Something's wrong.
- Think of your eyes.

303
00:58:22,360 --> 00:58:23,952
A few more weeks. Come on.

304
00:58:25,720 --> 00:58:28,154
A few more weeks. Think of your eyes.

305
00:58:30,440 --> 00:58:32,556
Quiet. You aren't alone here.

306
00:58:32,720 --> 00:58:33,948
Where is she?

307
00:59:08,640 --> 00:59:09,629
Ruben.

308
00:59:14,120 --> 00:59:15,155
Marie?

309
00:59:16,680 --> 00:59:17,908
Ruben.

310
00:59:19,520 --> 00:59:20,475
- Marie.

311
00:59:27,120 --> 00:59:29,429
Hush now.
- Where were you?

312
00:59:29,600 --> 00:59:30,669
I'm here.

313
00:59:31,480 --> 00:59:32,674
Ruben?

314
00:59:34,400 --> 00:59:35,469
Marie.

315
00:59:36,680 --> 00:59:38,079
Ssshh, quiet.

316
00:59:39,040 --> 00:59:40,075
Marie.

317
00:59:40,960 --> 00:59:44,430
I'm here. Pull yourself together.

318
00:59:48,920 --> 00:59:50,069
Where is she?

319
00:59:51,720 --> 00:59:52,914
I don't know.

320
00:59:53,720 --> 00:59:56,393
Has she gone? Did something happen?

321
01:00:01,440 --> 01:00:04,955
There are so many
beautiful things out there.

322
01:00:07,440 --> 01:00:09,635
And now you'll see them all.

323
01:00:10,760 --> 01:00:13,069
Are they as beautiful as Marie?

324
01:00:17,920 --> 01:00:21,310
It was pretty nasty that she just left.
Wasn't it?

325
01:00:22,240 --> 01:00:24,435
Ruben. Ruben.

326
01:00:25,560 --> 01:00:29,075
Promise me you'll eat again. Ruben.

327
01:00:30,040 --> 01:00:30,677
Ruben...

328
01:00:30,840 --> 01:00:34,230
You have to say who you are.
Who can I say is here?

329
01:00:37,800 --> 01:00:40,678
How is he? Can he see?

330
01:00:42,720 --> 01:00:46,269
It is impolite to keep your hood on, madam.

331
01:00:47,800 --> 01:00:49,119
That's my affair.

332
01:00:51,040 --> 01:00:52,314
Not entirely.

333
01:00:53,640 --> 01:00:58,873
If you don't dare appear in public,
what will that mean for Ruben?

334
01:01:02,160 --> 01:01:04,469
You should be more courageous.

335
01:01:05,400 --> 01:01:07,356
Why are you here, madam?

336
01:01:23,520 --> 01:01:25,158
He is going to see.

337
01:01:30,280 --> 01:01:33,033
You're between 30 and 40 years old.

338
01:01:35,080 --> 01:01:38,959
What future can you possibly offer Ruben?

339
01:01:42,560 --> 01:01:47,509
A colleague of mine has made
a fascinating study of the human soul.

340
01:01:48,080 --> 01:01:52,756
Some scars are forever.
Did you know that?

341
01:02:03,000 --> 01:02:09,030
I'm convinced that modern science will
be able to correct even more in the future.

342
01:02:10,960 --> 01:02:12,996
Let that be a comfort to you.

343
01:02:28,680 --> 01:02:32,195
Look, can you see it? Look at yourself!

344
01:02:43,120 --> 01:02:48,752
...but never managed to put down
the word that he wanted to.

345
01:02:48,960 --> 01:02:50,439
The word eternity.

346
01:02:50,640 --> 01:02:53,757
The Snow Queen had said:
if you can find this figure...

347
01:02:53,920 --> 01:02:55,956
...you'll be your own boss.

348
01:03:20,680 --> 01:03:21,635
Victor.

349
01:03:22,880 --> 01:03:25,348
You have to promise me something.

350
01:03:26,400 --> 01:03:27,958
Ruben is vulnerable.

351
01:03:29,640 --> 01:03:30,595
Be patient.

352
01:03:32,000 --> 01:03:34,753
Show him how beautiful the world is.

353
01:03:35,560 --> 01:03:38,028
Promise me you'll show him that.

354
01:03:40,560 --> 01:03:43,950
He'll see the very best
the world has to offer.

355
01:03:54,680 --> 01:03:56,875
This letter is from Marie.

356
01:03:59,280 --> 01:04:00,759
Ruben, he shall...

357
01:04:03,240 --> 01:04:04,229
Keep it.

358
01:04:06,680 --> 01:04:09,717
Give it to him at a better moment.

359
01:04:29,480 --> 01:04:31,118
Now open them.

360
01:05:26,760 --> 01:05:28,955
I think this is the right lens.

361
01:05:33,640 --> 01:05:35,835
So these are your spectacles.

362
01:07:01,360 --> 01:07:02,918
Mama...

363
01:07:03,200 --> 01:07:04,758
Marie?

364
01:10:07,640 --> 01:10:08,789
Marie?

365
01:10:51,520 --> 01:10:52,999
No last name either?

366
01:10:54,200 --> 01:10:55,155
Tinne.

367
01:11:00,920 --> 01:11:02,399
Marie Tinne.

368
01:11:20,160 --> 01:11:26,190
At a certain point she advertised
for a reader.

369
01:11:28,880 --> 01:11:29,790
And...

370
01:11:34,160 --> 01:11:36,310
That really is all l know.

371
01:11:40,560 --> 01:11:43,518
We'll get through this together, Ruben.

372
01:11:46,680 --> 01:11:50,070
Would you like to use my tap

373
01:11:50,960 --> 01:11:54,873
That hangs below my belly in my lap

374
01:11:56,240 --> 01:12:00,074
Then you can have a holy sup

375
01:12:00,360 --> 01:12:04,319
That splatters into your sweet cup

376
01:12:06,440 --> 01:12:09,352
I'll read you some poetry occasionally.

377
01:12:11,280 --> 01:12:12,679
Are they straight?

378
01:12:14,360 --> 01:12:15,554
Apparently not.

379
01:12:23,600 --> 01:12:25,875
The first woman isn't the best.

380
01:12:26,600 --> 01:12:28,352
She's the first.

381
01:12:30,160 --> 01:12:31,479
Do you know any more?

382
01:12:32,280 --> 01:12:35,590
Learn to look.
Then you'll see for yourself.

383
01:12:35,920 --> 01:12:37,433
What do you know?

384
01:12:37,880 --> 01:12:39,518
No more than you do.

385
01:12:42,600 --> 01:12:44,272
I'll show you something.

386
01:12:50,440 --> 01:12:51,919
Take a look.

387
01:12:52,240 --> 01:12:53,992
Take a good look, Ruben.

388
01:13:06,120 --> 01:13:07,348
Marie?

389
01:13:16,240 --> 01:13:18,071
What's the matter, dear?

390
01:13:18,400 --> 01:13:22,518
I thought you were someone else.
- You're looking for Marie?

391
01:13:22,720 --> 01:13:23,675
Yes.

392
01:13:25,720 --> 01:13:26,675
Come on.

393
01:13:30,960 --> 01:13:31,870
She's here.

394
01:13:38,720 --> 01:13:39,835
He wants Marie.

395
01:13:40,000 --> 01:13:41,319
I'm Marie.
- So am I.

396
01:13:41,880 --> 01:13:43,279
Me too.
I'm Marie too.

397
01:13:44,440 --> 01:13:47,910
We're all Marie.
- Come to Mama, darling...

398
01:13:48,320 --> 01:13:52,233
We're whoever you want.
- Shall I read you something?

399
01:13:52,400 --> 01:13:55,597
Victor said we have to be
really nice to you.

400
01:13:55,800 --> 01:13:58,598
The Snow Queen kissed Kay.
The kiss...

401
01:13:58,760 --> 01:14:00,796
No. No, thank you.

402
01:14:10,240 --> 01:14:12,037
The kiss penetrated...
- No.

403
01:14:14,440 --> 01:14:18,479
The kiss penetrated
to his heart that was already...

404
01:14:21,960 --> 01:14:24,428
...almost a lump of ice...
- Stop!

405
01:14:24,640 --> 01:14:28,235
The call of the grey curlew
does not rise at once...

406
01:14:28,360 --> 01:14:30,555
...but follows a drop in tone.

407
01:14:31,000 --> 01:14:36,358
Peewit. Peewit.

408
01:14:38,440 --> 01:14:39,759
The sequel to...

409
01:15:02,840 --> 01:15:05,513
Kuchuk-Hanem is
a very famous courtesan.

410
01:15:05,680 --> 01:15:07,193
She had just taken a bath.

411
01:15:07,720 --> 01:15:11,633
She stood waiting with a sheep
sprinkled with yellow henna...

412
01:15:11,800 --> 01:15:13,950
...that followed her like a dog.

413
01:15:14,120 --> 01:15:18,796
She's an imperial bitch, well endowed,
enormous eyes and beautiful knees.

414
01:15:18,960 --> 01:15:21,474
Confounded folds in horse flesh.

415
01:22:14,760 --> 01:22:18,878
Excuse me, can you tell me
where to find Andersen's books?

416
01:22:19,040 --> 01:22:21,554
I don't know my way around here, sir.

417
01:23:13,080 --> 01:23:16,868
Excuse me. Books by Andersen.
Where can I find them?

418
01:23:43,360 --> 01:23:44,588
Oh, thank you.

419
01:23:49,000 --> 01:23:50,115
Thank you.

420
01:24:08,000 --> 01:24:09,479
May I ask you something?

421
01:24:11,480 --> 01:24:12,754
Do you know this?

422
01:24:24,720 --> 01:24:26,756
You really should read it.

423
01:24:38,480 --> 01:24:41,517
Would you mind reading
something for me?

424
01:25:03,200 --> 01:25:04,838
He sat quite still...

425
01:25:07,800 --> 01:25:10,997
...stiff and cold
in the empty frigid hall.

426
01:25:12,960 --> 01:25:17,192
She recognised him at once
and threw her arms around him.

427
01:25:18,920 --> 01:25:22,833
Tears rolled down her red cheeks
and feel on his chest.

428
01:25:24,480 --> 01:25:27,438
His icy heart melted. She kissed...

429
01:25:30,280 --> 01:25:31,838
She kissed his eyes...

430
01:25:35,480 --> 01:25:36,799
...he wept...

431
01:25:38,120 --> 01:25:39,792
Come home with me.

432
01:25:42,360 --> 01:25:44,032
She kissed his hands...

433
01:25:45,160 --> 01:25:46,115
...his cheeks...

434
01:25:47,000 --> 01:25:48,672
...and he blushed again

435
01:25:51,920 --> 01:25:54,832
Tears washed the splinter from his eye.

436
01:25:56,480 --> 01:25:57,913
He recognised her.

437
01:26:03,520 --> 01:26:05,636
You thought I was beautiful.

438
01:26:07,000 --> 01:26:08,479
Come home with me.

439
01:26:13,800 --> 01:26:14,755
Look at me.

440
01:26:18,840 --> 01:26:20,114
Look at me.

441
01:26:26,320 --> 01:26:27,389
What do you see?

442
01:26:31,720 --> 01:26:33,199
What do you see now?

443
01:26:35,960 --> 01:26:36,995
Marie.
- No!

444
01:26:38,280 --> 01:26:39,235
That's not true.

445
01:26:39,880 --> 01:26:41,074
Not any more.

446
01:26:42,840 --> 01:26:44,193
Don't do it. Don't.

447
01:26:47,800 --> 01:26:49,119
You left too.

448
01:26:49,280 --> 01:26:50,679
You got eyes.

449
01:27:16,240 --> 01:27:19,312
Come back. You belong with me.

450
01:27:22,160 --> 01:27:23,354
Not any more.

451
01:27:24,120 --> 01:27:25,269
Who says that?

452
01:27:29,360 --> 01:27:30,475
Am I beautiful?

453
01:27:32,200 --> 01:27:33,189
- Yes. -

454
01:27:35,280 --> 01:27:36,554
Don't lie.

455
01:27:41,200 --> 01:27:42,838
I belong with you.

456
01:27:43,000 --> 01:27:45,036
Why won't you believe me?

457
01:27:48,720 --> 01:27:51,029
I don't believe in fairytales.

458
01:27:54,120 --> 01:27:55,633
I can't.

459
01:28:17,200 --> 01:28:18,315
Marie!

460
01:28:33,880 --> 01:28:35,108
Marie!

461
01:29:31,880 --> 01:29:33,359
We thought...

462
01:29:33,960 --> 01:29:36,428
It never seemed the right moment.

463
01:30:05,640 --> 01:30:07,710
She doesn't want to be seen.

464
01:30:43,800 --> 01:30:46,189
<i>My love, my everything...</i>

465
01:30:47,400 --> 01:30:52,349
<i>...when you read this letter, you'll see
how beautiful the world can be.</i>

466
01:30:52,520 --> 01:30:57,150
<i>I never felt more beautiful than when
seen through your hands, love.</i>

467
01:30:57,320 --> 01:30:58,514
<i>My love...</i>

468
01:30:59,200 --> 01:31:01,760
<i>...seek me with your hands and ears.</i>

469
01:31:01,920 --> 01:31:04,992
<i>I am happy to have known
the greatest love.</i>

470
01:31:05,160 --> 01:31:06,434
<i>The purest.</i>

471
01:31:08,760 --> 01:31:10,478
<i>True love is blind.</i>

472
01:31:11,720 --> 01:31:12,835
<i>For eternity.</i>

473
01:31:13,320 --> 01:31:14,594
<i>Marie.</i>

