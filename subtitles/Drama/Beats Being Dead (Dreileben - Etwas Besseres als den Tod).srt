1
00:00:12,770 --> 00:00:15,023
Legend has it that it was
written by the Dark Ones.

2
00:00:15,690 --> 00:00:17,609
Necronomicon ex Mortis,

3
00:00:18,693 --> 00:00:23,615
roughly translated,
"Book of the Dead".

4
00:00:26,784 --> 00:00:31,456
The book served as a passageway
to the evil worlds beyond..

5
00:00:36,717 --> 00:00:38,630
It was written long ago.

6
00:00:40,715 --> 00:00:42,800
When the seas ran red
with blood.

7
00:00:44,719 --> 00:00:47,805
It was this blood that was
used to ink the book.

8
00:00:53,728 --> 00:00:57,815
In the year 1300 AD, the
book disappeared.

9
00:01:14,749 --> 00:01:16,668
"EVIL DEAD II"

10
00:01:32,684 --> 00:01:34,602
So what's this place like?

11
00:01:34,769 --> 00:01:38,523
Well it's a little run-down... but,
uh, it's right up in the mountains.

12
00:01:38,773 --> 00:01:40,692
Are you sure it's deserted?

13
00:01:40,692 --> 00:01:45,780
Oh yeah... I think so.

14
00:02:26,738 --> 00:02:28,656
So what do you think kid?

15
00:02:31,784 --> 00:02:33,661
I love it Ash.

16
00:02:43,755 --> 00:02:45,673
I feel funny about being here.

17
00:02:45,673 --> 00:02:47,592
What if the people who own
the place come home?

18
00:02:47,717 --> 00:02:49,594
They're not gonna come back.

19
00:02:49,761 --> 00:02:51,852
Even if they do we'll
tell them the car

20
00:02:51,878 --> 00:02:53,681
broke down or
something like that.

21
00:02:53,723 --> 00:02:55,058
With your car, they'd
believe it.

22
00:02:56,768 --> 00:02:59,771
Hey, what do you say we have
some champagne hey baby?

23
00:03:00,772 --> 00:03:01,773
Sure.

24
00:03:02,774 --> 00:03:04,729
After all, I'm a
man and you're a

25
00:03:04,755 --> 00:03:07,053
woman, at least last
time I checked....

26
00:03:27,215 --> 00:03:30,468
Hey! There's a... There's a
tape recorder here.

27
00:03:31,719 --> 00:03:32,720
See what's on it.

28
00:03:35,723 --> 00:03:37,024
This is Professor
Raymond Knowby,

29
00:03:39,727 --> 00:03:41,846
Department of Ancient History,
log entry number two.

30
00:03:42,730 --> 00:03:44,914
I believe I have
made a significant

31
00:03:44,940 --> 00:03:46,759
find in the Castle of Candar.

32
00:03:47,735 --> 00:03:50,886
Having journeyed there
with my wife Henrietta, my

33
00:03:50,912 --> 00:03:54,062
daughter Annie and associate
professor Ed Getley.

34
00:03:54,742 --> 00:03:56,996
It was in the rear
chamber of the castle

35
00:03:57,022 --> 00:03:59,439
that we stumbled upon
something remarkable.

36
00:04:00,748 --> 00:04:06,838
Morturom Demonto, the
"Book of the Dead".

37
00:04:08,756 --> 00:04:11,126
My wife and I brought
the book to this

38
00:04:11,152 --> 00:04:13,703
cabin where I could
study it undisturbed.

39
00:04:17,765 --> 00:04:19,684
It was here that I began
the translations.

40
00:04:20,768 --> 00:04:22,687
The book speaks of a
spiritual presence.,

41
00:04:23,771 --> 00:04:26,644
A thing of evil that
roams the forests

42
00:04:26,670 --> 00:04:29,469
and the dark bowers
of man's domain..

43
00:04:30,778 --> 00:04:32,997
It is through the recitation
of the book's passages...

44
00:04:33,698 --> 00:04:39,704
that this dark spirit is given
license to possess the living.

45
00:04:41,789 --> 00:04:46,210
Included here are the phonetic
pronunciations of those passages.

46
00:04:48,713 --> 00:04:52,717
"Cunda astratta montose

47
00:04:54,719 --> 00:04:56,721
eargrets gutt

48
00:04:58,806 --> 00:05:00,051
nos veratoos

49
00:05:01,777 --> 00:05:02,924
canda

50
00:05:05,850 --> 00:05:06,665
amantos

51
00:05:06,691 --> 00:05:07,664
canda".

52
00:09:50,848 --> 00:09:52,809
It's gone. The sun's
driven it away.

53
00:10:03,861 --> 00:10:05,738
Yeah. For now. Gotta blow
out of here for now...

54
00:10:19,877 --> 00:10:21,796
Join us.

55
00:11:17,852 --> 00:11:20,855
Gotta, I gotta get a grip
on myself here..

56
00:14:25,873 --> 00:14:26,958
How'd the expedition go?

57
00:14:26,958 --> 00:14:29,711
Great. I found the pages of
the Book of the Dead.

58
00:14:29,877 --> 00:14:33,548
Yeah, I got your telegram. Thanks.
So what condition are they in?

59
00:14:33,881 --> 00:14:35,800
Take a look.

60
00:14:36,884 --> 00:14:37,885
They haven't aged a
day in 3000 years.

61
00:14:37,885 --> 00:14:39,804
Maybe longer.

62
00:14:40,888 --> 00:14:41,889
When do we begin
the translations?

63
00:14:41,889 --> 00:14:44,642
Tonight. Is everything all
set with my father?

64
00:14:44,892 --> 00:14:47,478
Well, it should be but I haven't
spoken with him in a week.

65
00:14:47,895 --> 00:14:49,814
There's no phone in the cabin.

66
00:14:50,898 --> 00:14:52,817
We'll take my car, it'll take us
about an hour to get there.

67
00:14:55,403 --> 00:14:57,423
Annie you hinted in your
telegram that your father was

68
00:14:57,449 --> 00:14:59,432
onto something in the first
part of his translations.

69
00:14:59,907 --> 00:15:00,908
What has he found in the
Book of the Dead?

70
00:15:00,908 --> 00:15:04,280
Probably nothing.
But just possibly,

71
00:15:04,306 --> 00:15:06,856
a doorway to another world?

72
00:18:32,954 --> 00:18:34,872
Dance with me.

73
00:19:25,006 --> 00:19:26,924
Hello lover.

74
00:19:58,957 --> 00:20:00,875
Workshed.

75
00:20:32,991 --> 00:20:37,412
Even now we have your
darling Linda's soul.

76
00:20:37,996 --> 00:20:40,748
She suffers in torment.

77
00:20:40,915 --> 00:20:45,837
You're going down.
Chainsaw.

78
00:21:36,971 --> 00:21:40,308
Please Ash... please
don't hurt me.

79
00:21:40,934 --> 00:21:44,979
You swore- you swore that
we'd always be together.

80
00:21:46,981 --> 00:21:48,900
I love you.

81
00:21:51,986 --> 00:21:55,073
Yah! Your lover is mine
and now she burns

82
00:21:55,740 --> 00:21:57,825
in Hell.

83
00:24:30,979 --> 00:24:32,897
Oh yeah, alright... OK.

84
00:24:42,073 --> 00:24:43,992
I'm fine... I'm fine.

85
00:24:44,075 --> 00:24:47,662
I don't think so. We just cut up
our girlfriend with a chainsaw.

86
00:24:47,996 --> 00:24:50,748
Does that sound fine?

87
00:25:51,059 --> 00:25:57,732
You bastards!
You dirty bastards!

88
00:26:06,074 --> 00:26:09,077
Give me back my hand!

89
00:26:11,996 --> 00:26:16,167
Give me back my haaaaand!!

90
00:26:54,038 --> 00:26:55,957
Excuse me. Excuse me.

91
00:26:57,041 --> 00:26:58,960
Is this the road to
the Knowby cabin?

92
00:27:00,044 --> 00:27:01,963
That's right.
And you ain't going there.

93
00:27:03,131 --> 00:27:05,049
And why not?

94
00:27:16,060 --> 00:27:18,062
There must be another way in.

95
00:27:18,730 --> 00:27:19,731
There's gotta be another
road or something.

96
00:27:20,064 --> 00:27:21,983
Sure ain't no road.

97
00:27:27,071 --> 00:27:30,992
Why the hell do you want to
go up there for anyway?

98
00:27:35,997 --> 00:27:37,915
None of your business.

99
00:27:44,088 --> 00:27:46,758
Hey! I just remembered.

100
00:27:47,091 --> 00:27:50,011
Why, yeah... that's right.
There is a trail.

101
00:27:50,011 --> 00:27:52,931
You could uh, follow
Bobby Joe and me.

102
00:27:54,098 --> 00:27:56,017
Sounds alright to me.

103
00:27:56,100 --> 00:27:57,101
But it'll cost ya.

104
00:27:57,101 --> 00:27:59,020
How much?

105
00:27:59,103 --> 00:28:04,025
Forty fi...
Hundred bucks.

106
00:28:05,026 --> 00:28:09,948
Tell ya what, you take my bags
and you got a deal.

107
00:28:13,117 --> 00:28:15,036
Sure.

108
00:30:07,065 --> 00:30:10,068
Ah. Ah. That's right.
Who's laughing now?

109
00:30:19,077 --> 00:30:22,080
Who's laughing now?

110
00:31:11,129 --> 00:31:13,047
Jesus H. Christ... thought
all she was talking about

111
00:31:13,631 --> 00:31:15,633
were those two Goddamn
little bags.

112
00:31:26,144 --> 00:31:29,147
Here's your new home.

113
00:31:38,740 --> 00:31:41,743
"B�CS� A FEGYVEREKT�L"

114
00:33:09,163 --> 00:33:10,164
Son of a....

115
00:33:33,104 --> 00:33:35,690
Gotcha didn't I you
little sucker?!

116
00:34:09,140 --> 00:34:11,059
Old double barrel here...

117
00:34:11,142 --> 00:34:16,731
blow your butts to kingdom come.
See if we don't...

118
00:37:15,243 --> 00:37:17,161
You little bastard!

119
00:37:20,248 --> 00:37:22,166
You gonna be alright honey?

120
00:37:22,166 --> 00:37:24,794
I-I don't know.
I-I think so.

121
00:37:31,175 --> 00:37:31,175
You just sit still for a minute.
You know this son of a bitch?

122
00:37:31,175 --> 00:37:33,094
No we thought her father
was going to be here.

123
00:37:33,177 --> 00:37:34,178
That's why we decided -

124
00:37:34,178 --> 00:37:36,097
Oh my God! Where are
my parents?

125
00:37:49,193 --> 00:37:51,112
What the hell did
you do to them?

126
00:37:53,197 --> 00:37:55,116
What the hell did
you do to them?

127
00:37:57,201 --> 00:37:59,120
Annie, come here.

128
00:38:04,167 --> 00:38:06,127
We'll throw him
in there.

129
00:38:10,173 --> 00:38:12,050
Crazy buck's gone
blood seeking.

130
00:38:13,217 --> 00:38:17,055
Wait.
I made a mistake.

131
00:38:23,186 --> 00:38:25,146
Wait. Wait. Wait.

132
00:38:27,231 --> 00:38:28,232
I made a mistake.

133
00:38:28,232 --> 00:38:30,151
Damn right. Blackmail
son of a bitch.

134
00:38:39,243 --> 00:38:41,162
I hope you rot down there.

135
00:38:51,256 --> 00:38:52,173
Oh shit!

136
00:38:52,173 --> 00:38:54,092
I know it hurts baby,
but everything's gonna be fine.

137
00:38:59,180 --> 00:39:00,181
Now in about five
minutes I'm gonna

138
00:39:00,207 --> 00:39:01,207
go fetch the sheriff
and bring him-

139
00:39:02,183 --> 00:39:04,102
Checked all the other rooms.

140
00:39:04,185 --> 00:39:06,104
Folks aren't here.
Maybe they never came.

141
00:39:06,271 --> 00:39:08,189
But these are my
father's things.

142
00:39:11,192 --> 00:39:13,229
It's only been a few hours
since I've translated and

143
00:39:13,255 --> 00:39:15,138
spoken aloud the first of
the demon resurrection

144
00:39:15,280 --> 00:39:18,199
passages from the Book
of the Dead.

145
00:39:19,200 --> 00:39:21,077
Shhh... Listen up.
This is my father's voice.

146
00:39:24,205 --> 00:39:29,127
And now I fear that my wife has become
host to a Candarian Demon.

147
00:39:31,212 --> 00:39:35,133
May God forgive me for what I
have unleashed unto this earth.

148
00:39:36,175 --> 00:39:38,136
Last night Henrietta tried to...

149
00:39:39,721 --> 00:39:40,972
kill me.

150
00:39:43,224 --> 00:39:45,143
It's now October 1st, 4:33 PM.

151
00:39:48,187 --> 00:39:50,148
Henrietta is dead.
I could not bring

152
00:39:52,233 --> 00:39:54,152
myself to dismember her corpse.
But I dragged her down the steps...

153
00:40:01,200 --> 00:40:04,162
and I buried her.
I buried her in the cellar.

154
00:40:05,163 --> 00:40:07,081
God help me,

155
00:40:07,206 --> 00:40:10,251
I buried her in the earthen floor
of the fruit cellar.

156
00:40:18,259 --> 00:40:19,177
What the hell was that?

157
00:40:19,177 --> 00:40:21,095
Somebody's down there with him.

158
00:40:22,180 --> 00:40:23,264
No, can't be.

159
00:40:23,264 --> 00:40:24,182
Let's get the fuck
out of here.

160
00:40:24,182 --> 00:40:33,107
Someone's in my fruit cellar.
Someone with a fresh soul!

161
00:40:35,276 --> 00:40:37,195
Let me out! There's something
down here! Ah!

162
00:40:39,280 --> 00:40:39,280
Let him out!

163
00:40:39,280 --> 00:40:41,199
It's a trick, I know it!

164
00:40:41,199 --> 00:40:43,117
Let him out!!

165
00:40:43,201 --> 00:40:44,285
Move it! Open those
chains up!

166
00:40:44,285 --> 00:40:46,204
Come to me.

167
00:40:48,206 --> 00:40:50,124
Ah! Help! Help!
Help me!

168
00:40:50,208 --> 00:40:52,126
Come to sweet Henrietta.
Hahaha.

169
00:40:59,217 --> 00:41:01,177
Hurry!

170
00:41:02,220 --> 00:41:04,138
Help! Help me please!

171
00:41:06,224 --> 00:41:08,184
I'll swallow your soul.

172
00:41:25,243 --> 00:41:27,161
Do something!

173
00:42:35,271 --> 00:42:37,148
There's something out there.

174
00:42:39,317 --> 00:42:44,238
That... that witch in the
cellar is only part of it.

175
00:42:47,241 --> 00:42:54,749
It lives... out in those
woods, in the dark...

176
00:42:56,334 --> 00:43:03,925
something... something that's
come back from the dead.

177
00:43:04,259 --> 00:43:06,552
Plee! Please let's just get
the hell out of here!

178
00:43:07,262 --> 00:43:09,138
We're going baby. We're
going to get on that trail-

179
00:43:10,181 --> 00:43:12,100
Nobody's going out that door,
not till daylight.

180
00:43:14,269 --> 00:43:16,187
Now you listen to me -

181
00:43:42,255 --> 00:43:43,222
Remember that song Annie?
I used to

182
00:43:43,248 --> 00:43:44,240
sing it to you when
you were a baby.

183
00:43:45,300 --> 00:43:47,218
Mother?

184
00:43:48,261 --> 00:43:50,138
Unlock these chains.
Quickly!

185
00:43:59,314 --> 00:44:00,231
No.

186
00:44:00,231 --> 00:44:04,694
You were born September
2nd, 1962.

187
00:44:05,236 --> 00:44:12,160
I remember it well because
it was snowing. So strange

188
00:44:12,327 --> 00:44:13,286
it would be snowing
in September.

189
00:44:17,332 --> 00:44:19,292
That thing in the cellar
is not my mother.

190
00:44:28,760 --> 00:44:32,764
We are the things that
were and shall be again.

191
00:44:35,266 --> 00:44:36,225
Steps of the Book.

192
00:44:37,268 --> 00:44:39,270
We want what is yours.

193
00:44:40,271 --> 00:44:42,231
Life! Dead by dawn.
Dead by dawn.

194
00:44:42,273 --> 00:44:43,274
Dead by dawn. Dead by dawn.

195
00:44:43,274 --> 00:44:45,193
Dead by dawn.

196
00:44:46,277 --> 00:44:48,196
Dead by dawn.

197
00:45:04,128 --> 00:45:05,171
Let me out.

198
00:45:05,296 --> 00:45:07,298
Thirsty son-of-a-bitch.

199
00:45:18,726 --> 00:45:22,230
Where you going?
Help us you filthy coward!

200
00:45:44,335 --> 00:45:46,254
We live still!

201
00:46:27,378 --> 00:46:29,297
That's funny.

202
00:46:29,297 --> 00:46:31,215
What?

203
00:46:32,300 --> 00:46:34,302
That trail we came in here on?

204
00:46:36,304 --> 00:46:38,556
It just ain't there no more.

205
00:46:40,308 --> 00:46:46,230
Like, like the woods
just swallowed her up.

206
00:47:09,337 --> 00:47:10,672
It's so quiet.

207
00:47:59,304 --> 00:48:04,225
Maybe something trying to force
its way into our world.

208
00:48:41,262 --> 00:48:43,181
Hell no. You're the curious one.

209
00:49:04,369 --> 00:49:09,958
Hey. I'll go with you.

210
00:50:05,346 --> 00:50:09,267
Shit. I told you there weren't
nothing in here no how.

211
00:50:35,376 --> 00:50:37,295
Holy Mother o' Mercy.

212
00:50:38,379 --> 00:50:39,380
Father?

213
00:50:39,380 --> 00:50:41,299
Annie.

214
00:50:41,299 --> 00:50:47,722
There is a dark spirit here
that wants to destroy you.

215
00:50:48,306 --> 00:50:52,393
Your salvation lies there.

216
00:50:59,400 --> 00:51:05,323
In the pages of the book.
Recite the passages.

217
00:51:06,407 --> 00:51:16,918
Dispel the evil. Save my soul.
And your own lives!

218
00:51:27,428 --> 00:51:30,348
Jake. You're holding
my hand too tight.

219
00:51:32,433 --> 00:51:34,352
Baby, I ain't holding your hand.

220
00:51:55,373 --> 00:51:57,292
Hey? Where's Bobby Joe?

221
00:53:50,405 --> 00:53:53,074
Hey? Where the hell is she?

222
00:54:06,421 --> 00:54:08,339
We gotta go out there
and find her.

223
00:54:08,423 --> 00:54:12,093
If she went out in those woods,
you can forget about her.

224
00:54:18,433 --> 00:54:20,351
What's wrong?

225
00:54:21,436 --> 00:54:24,355
Felt like someone just
walked over my grave.

226
00:54:29,360 --> 00:54:31,279
What's that picture?
What is that?

227
00:54:33,448 --> 00:54:38,286
In 1300 AD they called this
man the, ah, hero from the sky.

228
00:54:40,455 --> 00:54:42,373
He was prophesied to have
destroyed the Evil.

229
00:54:44,459 --> 00:54:48,379
Didn't do a very good job...
Can you find it?

230
00:54:50,381 --> 00:54:52,300
Here it is, two passages.

231
00:54:58,389 --> 00:55:00,308
Recitation of this
first passage..

232
00:55:03,478 --> 00:55:07,398
will make this dark spirit
manifest itself in the flesh.

233
00:55:10,401 --> 00:55:12,320
Why the hell would we
want to do that?

234
00:55:12,403 --> 00:55:14,322
Recitation of this second
passage creates

235
00:55:17,408 --> 00:55:19,327
a kind of rift in
time and space.

236
00:55:22,413 --> 00:55:24,332
And the physical
manifestation of this

237
00:55:25,416 --> 00:55:26,417
dark spirit can be forced
back into the rift.

238
00:55:26,417 --> 00:55:28,336
At least that's the best
translation that I can -

239
00:55:31,422 --> 00:55:35,343
That's right. I'm running
the show now.

240
00:55:36,427 --> 00:55:38,846
We're going to go out there in
them woods and look for Bobby Joe.

241
00:55:39,430 --> 00:55:41,349
Once we find her we're getting
the hell out of here.

242
00:55:42,433 --> 00:55:45,687
No you idiot!
You'll kill us all.

243
00:55:47,438 --> 00:55:49,357
She's dead by now.

244
00:55:49,440 --> 00:55:51,359
Don't you understand?

245
00:55:52,443 --> 00:55:55,780
With these pages, at least
we have a chance.

246
00:55:59,450 --> 00:56:01,369
Bunch of mumbo jumbo bullshit.

247
00:56:04,455 --> 00:56:06,374
These pages don't mean squat.

248
00:56:13,464 --> 00:56:16,384
Besides, now you ain't
got no choice.

249
00:56:17,385 --> 00:56:19,304
Now move!

250
00:56:28,479 --> 00:56:30,398
Move.

251
00:56:30,481 --> 00:56:32,400
Look. You're nuts.

252
00:56:34,402 --> 00:56:36,321
I said move!

253
00:56:42,493 --> 00:56:44,412
I'll blow your fucking head off.

254
00:57:31,459 --> 00:57:35,380
Hey. No trail.
Where to now?

255
00:58:00,405 --> 00:58:01,489
You'll get us all killed!

256
00:58:01,489 --> 00:58:03,408
Shut up!

257
00:58:03,491 --> 00:58:04,492
Leave him alone!

258
00:58:04,492 --> 00:58:06,411
Get outta here...

259
00:58:21,426 --> 00:58:23,344
Where are you, girl?

260
00:58:54,459 --> 00:58:56,377
You're next. Annie!

261
01:01:45,463 --> 01:01:47,382
I'm sorry!

262
01:01:48,466 --> 01:01:50,385
Get me another room.
Get the axe.

263
01:01:50,468 --> 01:01:52,387
We'll kill it when
it comes back.

264
01:01:55,473 --> 01:01:57,392
But first, pull this
damn thing out of me!

265
01:02:09,487 --> 01:02:11,406
I can't breathe,
I can't breathe. Hurry!

266
01:02:13,575 --> 01:02:15,493
I'm trying!
I'm trying!

267
01:02:26,504 --> 01:02:30,425
Shut up! Shut up!
Shut up!

268
01:02:56,534 --> 01:02:58,453
Check outside the windows.

269
01:03:01,539 --> 01:03:03,458
Check the windows, he's
probably right out...

270
01:03:04,542 --> 01:03:06,461
Ahhh! Help me!

271
01:03:36,491 --> 01:03:38,409
Oh God! Ahhhhh!

272
01:05:12,503 --> 01:05:15,423
No! No wait!

273
01:05:17,592 --> 01:05:21,512
Listen to me!
I'm alright now.

274
01:05:21,512 --> 01:05:23,431
That thing is gone!

275
01:05:36,527 --> 01:05:38,780
Damn it!
I said I was alright!

276
01:05:39,614 --> 01:05:41,532
Are you listening to me?

277
01:05:43,534 --> 01:05:47,455
You hear what I'm saying?
I'm alright!

278
01:05:51,626 --> 01:05:53,544
I'm alright.

279
01:05:55,546 --> 01:06:00,051
OK, maybe you are.
But for how long?

280
01:06:04,555 --> 01:06:07,475
If we're going to beat this
thing, we need those pages.

281
01:06:11,562 --> 01:06:13,430
Then let's head down into that

282
01:06:13,456 --> 01:06:15,758
cellar and carve
ourselves a witch.

283
01:07:04,616 --> 01:07:06,534
Groovy.

284
01:07:50,578 --> 01:07:52,497
Those pages are down
there somewhere.

285
01:10:56,598 --> 01:11:00,518
"Nos veratos alamemnon conda."

286
01:11:56,574 --> 01:11:57,575
Let's go.

287
01:12:30,692 --> 01:12:32,610
I'll swallow your soul.
I'll swallow your soul.

288
01:13:25,663 --> 01:13:27,582
Hey! I'll swallow your soul!
I'll swallow your soul!

289
01:13:30,668 --> 01:13:32,587
Swallow this.

290
01:15:03,761 --> 01:15:05,680
I only completed the first
of the passages

291
01:15:06,681 --> 01:15:08,641
and that was to make the Evil
a thing of the flesh!

292
01:15:16,691 --> 01:15:18,568
Finish it!

293
01:15:19,652 --> 01:15:21,613
There's still the
second passage.

294
01:15:26,701 --> 01:15:26,701
The one-

295
01:15:26,701 --> 01:15:28,703
the one to open the rift
and send the Evil back!

296
01:15:29,621 --> 01:15:32,582
Well start reciting it!
Now!

297
01:15:45,678 --> 01:15:47,639
Don't look Annie! Finish
the passages! Get rid of it!

298
01:16:09,661 --> 01:16:11,579
No!!!

299
01:17:07,719 --> 01:17:09,596
We've won. We've won.
Victory is ours.

300
01:17:42,754 --> 01:17:44,672
You did it kid.

301
01:18:18,790 --> 01:18:20,792
By God... No!!!!
For God's sake!

302
01:18:20,792 --> 01:18:22,710
How do you stop it?

303
01:19:40,747 --> 01:19:42,624
Slay the beast.
It is a deadite!

304
01:19:43,708 --> 01:19:45,627
Run! Back to the castle!

305
01:20:21,746 --> 01:20:23,665
Hail he who has come
from the skies

306
01:20:24,749 --> 01:20:26,668
to deliver us from the
terrors of the deadites!

307
01:20:28,753 --> 01:20:30,672
Hail! Hail! Hail!

308
01:20:33,758 --> 01:20:35,677
No! No!! No!!! No!!!!!!