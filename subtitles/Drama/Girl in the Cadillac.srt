﻿1
00:00:09,200 --> 00:00:13,200
Midnight's girl

2
00:00:21,794 --> 00:00:23,361
Sera is not there..

3
00:00:30,360 --> 00:00:38,160
The person you are calling is not available. Please leave a 
message after the tone. Extra charges may...

4
00:00:44,150 --> 00:00:46,850
It's probably broken or something...

5
00:00:57,062 --> 00:00:58,162
What is this?

6
00:01:00,387 --> 00:01:01,587
Let's close down the karaoke.

7
00:01:01,588 --> 00:01:02,688
Why?

8
00:01:02,712 --> 00:01:04,212
Aigoo!

9
00:01:06,857 --> 00:01:10,924
Hyung, do we have to turn this place into a storage?

10
00:01:10,926 --> 00:01:12,626
What kind of crap are you saying?

11
00:01:12,650 --> 00:01:14,650
If you regret it so much, then you sell it for me.

12
00:01:14,674 --> 00:01:16,674
Why? Do you feel attached to it?

13
00:01:16,698 --> 00:01:20,198
I have a lot to organize. Quick, put those chairs up.

14
00:01:26,909 --> 00:01:28,409
Mr. President!

15
00:01:28,433 --> 00:01:30,033
Are you closing down?

16
00:01:30,072 --> 00:01:32,905
Yeah, well... it seems so.

17
00:01:33,847 --> 00:01:35,147
I'll buy it.

18
00:01:38,590 --> 00:01:39,590
Really?

19
00:01:39,614 --> 00:01:41,914
It seems like there were customers 
from time to time during night.

20
00:01:46,938 --> 00:01:52,238
Mr. President, then let's go outside and talk more about this in depth.

21
00:01:52,238 --> 00:01:53,638
Sure.

22
00:01:53,638 --> 00:01:55,438
Closing down? Nonsense.

23
00:02:00,162 --> 00:02:02,362
You, don't leave! Wait for me, wait!

24
00:02:03,386 --> 00:02:05,386
Closing down? What did you mean?

25
00:02:06,110 --> 00:02:08,110
Closing down?

26
00:02:32,793 --> 00:02:37,626
♪ So I can wash away my sorrow ♪

27
00:02:40,981 --> 00:02:45,448
♪ So I can add to my joy ♪

28
00:02:48,562 --> 00:02:55,162
♪ So your eyes can shine more brightly ♪

29
00:02:55,186 --> 00:03:01,886
♪ I will always be the tears in your eyes ♪

30
00:03:04,556 --> 00:03:08,556
♪ Because I am so weak ♪

31
00:03:08,580 --> 00:03:12,680
♪ Because my heart is too young ♪

32
00:03:12,704 --> 00:03:20,904
♪ I'm afraid that I cannot even protect you ♪

33
00:03:20,928 --> 00:03:28,328
♪ Because I look so miserable, I did not say anything ♪

34
00:03:28,352 --> 00:03:29,652
Hello?

35
00:03:29,676 --> 00:03:34,176
♪ I will promise you one thing, that I'm here for you ♪

36
00:03:34,406 --> 00:03:37,973
This is my daughter, Sera.

37
00:03:38,597 --> 00:03:43,497
She has just been lying like this because of a car accident.

38
00:03:44,323 --> 00:03:46,123
I've waited 4 years.

39
00:03:51,143 --> 00:03:53,043
It's Sera's handphone.

40
00:03:54,772 --> 00:03:59,539
Its battery should have been drained,

41
00:03:59,959 --> 00:04:02,159
but maybe there was a little left

42
00:04:02,183 --> 00:04:04,183
when I received the call from you.

43
00:04:05,907 --> 00:04:10,307
The first person to call us in four years...

44
00:04:13,883 --> 00:04:14,883
But...

45
00:04:16,607 --> 00:04:18,407
It can't be...

46
00:04:19,263 --> 00:04:20,263
Why?

47
00:04:22,755 --> 00:04:23,855
Nevermind.

48
00:04:24,079 --> 00:04:26,679
I thought you looked like someone I know

49
00:04:26,703 --> 00:04:29,203
but I think he was a lot  more handsome that you.

50
00:04:31,253 --> 00:04:35,120
♪ I will give you ♪

51
00:04:38,202 --> 00:04:41,202
♪ everything I have ♪

52
00:04:45,037 --> 00:04:47,670
♪ Suddenly tonight ♪

53
00:04:51,487 --> 00:04:54,487
♪ I will give you everything ♪

54
00:04:58,010 --> 00:05:02,410
♪ There is nothing that I cannot do ♪

55
00:05:04,634 --> 00:05:06,834
♪ For you, I will ♪

56
00:05:09,784 --> 00:05:13,484
♪ For you, I will ♪
Ah, this really is not working.

57
00:05:13,708 --> 00:05:16,608
Ah, this really isn't working.

58
00:05:22,722 --> 00:05:23,722
Ta da!

59
00:05:29,748 --> 00:05:32,315
Are you doing magic for the talent show?

60
00:05:32,910 --> 00:05:36,010
No, it's a club. Magic club.

61
00:05:36,193 --> 00:05:38,860
Oh, there's such a thing? Freshman?

62
00:05:39,338 --> 00:05:40,338
Yes.

63
00:05:41,615 --> 00:05:46,148
But, a flower is too cliche so why don't you try something else?

64
00:05:47,772 --> 00:05:50,072
Here, I picked this out from a toy coin machine.

65
00:05:51,319 --> 00:05:53,719
A dolphin instead of a flowers.

66
00:06:03,703 --> 00:06:07,336
Ah, the truth is I'm singing as the 12th grade representative tomorrow.

67
00:06:07,360 --> 00:06:09,160
So, I was here to practice...

68
00:06:09,184 --> 00:06:13,684
Can I watch you practice, Jidan oppa?

69
00:06:14,208 --> 00:06:16,208
How do you know my name?

70
00:06:17,665 --> 00:06:18,665
Ahh..

71
00:06:18,881 --> 00:06:21,414
I didn't know because of that...

72
00:06:22,133 --> 00:06:24,733
I'm your fan.

73
00:06:24,757 --> 00:06:26,057
Ah, really?

74
00:06:26,081 --> 00:06:28,681
Yes, since very long time ago.

75
00:06:29,105 --> 00:06:30,605
Really?

76
00:06:32,629 --> 00:06:35,929
Hey, Handsome! What are you doing here?

77
00:06:35,930 --> 00:06:40,330
All the girls from other high schools rushed here to hear you sing.

78
00:06:40,354 --> 00:06:42,354
Come on! Hurry, Hurry!

79
00:06:42,378 --> 00:06:43,878
I've to go.

80
00:06:53,802 --> 00:06:58,269
Jidan oppa, you will be a very good singer.

81
00:07:02,082 --> 00:07:04,249
Sera always said...

82
00:07:05,346 --> 00:07:06,846
..to wait and see.

83
00:07:07,130 --> 00:07:10,863
That, you will be a good singer.

84
00:07:12,535 --> 00:07:15,935
I also wished that you would become a good singer

85
00:07:15,959 --> 00:07:20,559
so, that Sera would be able to see you on TV

86
00:07:22,773 --> 00:07:25,273
But you came so soon.

87
00:07:28,207 --> 00:07:32,807
It's just, there's someone I want to help.

88
00:07:36,094 --> 00:07:40,161
The person she want to help...was me.

89
00:07:40,275 --> 00:07:41,275
Sing once!

90
00:07:41,972 --> 00:07:47,905
I'm sure there is at least one person in this world who is waiting for Gong Ji Dan's song.

91
00:07:47,929 --> 00:07:51,529
That person can be close or far away.

92
00:07:51,831 --> 00:07:54,664
So she came to me...

93
00:07:54,688 --> 00:07:56,688
...and left

94
00:07:58,589 --> 00:08:02,022
In that blind moment, when I sleep.

95
00:08:04,289 --> 00:08:09,556
I'm just finally starting to get to know you..

96
00:08:11,637 --> 00:08:15,204
She stayed in my mind and left.

97
00:08:33,869 --> 00:08:40,036
♪ Even when I am born a thousand times again ♪

98
00:08:40,560 --> 00:08:45,360
♪ there will never be a person like her. ♪

99
00:08:47,314 --> 00:08:54,547
♪ Warming up my sad life ♪

100
00:08:54,571 --> 00:09:00,271
♪ She's a person to be thankful for ♪

101
00:09:00,883 --> 00:09:08,450
♪ For a person like you ♪

102
00:09:08,914 --> 00:09:15,447
♪ I wouldn't mind my heart aching ♪

103
00:09:15,581 --> 00:09:22,081
♪ Even if I can't say the word  "love" ♪

104
00:09:22,105 --> 00:09:30,305
♪ Even if I can only stare at you from far away♪

105
00:09:30,736 --> 00:09:37,803
♪ Because I can give you everything, 
because I can love you ♪

106
00:09:37,827 --> 00:09:45,627
♪ Although I am sad, I am happy ♪

107
00:09:46,929 --> 00:09:52,396
Song is something that can move someone's heart,
and send a message of someone's mind.

108
00:09:52,420 --> 00:09:55,020
It's like magic, don't you think so?

109
00:09:57,512 --> 00:10:02,112
I'm just finally starting to get to know you...

110
00:10:04,100 --> 00:10:10,000
Code blue! Code blue! Second floor, room 201!

111
00:10:16,929 --> 00:10:22,996
♪ Although I don't want anything ♪

112
00:10:23,020 --> 00:10:31,020
♪ Your smile would still make me happy ♪

113
00:10:31,033 --> 00:10:38,800
♪  Love is for giving, just for giving ♪

114
00:10:38,824 --> 00:10:48,924
♪ Although I am sad, I am happy ♪

115
00:11:18,652 --> 00:11:19,652
Hello?

116
00:11:19,676 --> 00:11:21,676
Ouhh, my Gong Ji Dan

117
00:11:21,700 --> 00:11:24,500
You're still rolling around on bed, aren't you?

118
00:11:24,524 --> 00:11:26,324
Ah, what is it?

119
00:11:26,325 --> 00:11:29,025
I'll give you one hour.
Hurry up and come outside. Go~

120
00:11:29,030 --> 00:11:31,030
Hello? Hyung?

121
00:11:32,054 --> 00:11:34,054
Aish!

122
00:11:34,055 --> 00:11:38,522
Oh! Hey! I'm going to Maldives tomorrow!

123
00:11:38,946 --> 00:11:40,946
9 nights, 10 days!

124
00:11:42,125 --> 00:11:45,525
Hey, is my son an average son?

125
00:11:45,549 --> 00:11:49,349
National superstar, Jidan!

126
00:11:51,011 --> 00:11:52,911
and I'm his mother

127
00:11:53,135 --> 00:11:55,135
I'm his sister!

128
00:11:55,659 --> 00:11:57,659
Ah, busy busy!
-Oppa!

129
00:11:57,683 --> 00:11:59,983
What is it?
- Of course I'm busy~

130
00:12:03,307 --> 00:12:05,107
Your autograph please..

131
00:12:07,631 --> 00:12:13,931
Oppa~! Oppa~!

132
00:12:14,022 --> 00:12:17,022
AAAHHHHHH! SHUT UPPPP!!

133
00:12:17,046 --> 00:12:19,046
Please hurry up! You're going to be late!

134
00:12:19,048 --> 00:12:20,648
What with this car?

135
00:12:20,649 --> 00:12:23,949
Ha! I give up on the moneylending business and get one!

136
00:12:23,973 --> 00:12:27,973
This should be enough to serve our superstar!

137
00:12:27,997 --> 00:12:32,897
I'm not a star yet. I'm gonna ride the subway.
Stop overreacting, please!

138
00:12:33,121 --> 00:12:35,121
Hey! Hey!

139
00:12:35,145 --> 00:12:37,345
Hey, don't be late and be here by 2!

140
00:12:39,999 --> 00:12:41,399
He left.

141
00:13:03,943 --> 00:13:09,643
I heard that dolphins can communicate with each other 
even if they're thousands of kilometers away from each other.

142
00:13:09,667 --> 00:13:13,467
Thousands of kilometers...No, more than that.

143
00:13:13,491 --> 00:13:15,491
Sending what your heart feels.

144
00:13:18,288 --> 00:13:27,588
Even if we're far away, my mind and feelings, 
can I really send them on to where you're?

145
00:13:29,712 --> 00:13:31,612
Of course!

146
00:13:33,836 --> 00:13:36,836
Talking to myself making me even hear things.

147
00:13:40,778 --> 00:13:42,645
Why do you talk to yourself?

148
00:13:51,354 --> 00:13:57,687
♪ The person I want to give everything to, ♪
♪ you are my first love ♪

149
00:13:57,711 --> 00:14:05,611
♪ The person that I want to keep in my heart ♪
♪ you are my first love ♪

150
00:14:06,313 --> 00:14:11,313
♪ My heart pit-a-pat, it must be love ♪

151
00:14:11,408 --> 00:14:15,308
♪ What should I do with you? ♪

152
00:14:19,241 --> 00:14:22,741
♪ Inside my mind ♪

153
00:14:23,084 --> 00:14:29,817
♪ is filled with you and I don't know what to do ♪

154
00:14:29,841 --> 00:14:32,741
♪ What should I do? ♪

155
00:14:32,765 --> 00:14:40,265
♪ The person that I want to gve everything to ♪
♪ You are the love I was looking for ♪

156
00:14:40,289 --> 00:14:45,389
♪ My heart pit-a-pat, it's like I'm dreaming ♪

157
00:14:45,813 --> 00:14:47,813
♪ I love you ♪

