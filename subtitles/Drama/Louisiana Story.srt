﻿1
00:00:00,100 --> 00:00:03,200
Subtitles rtm fenixclub.com.

2
00:00:23,700 --> 00:00:27,900
THE LOUISIAN HISTORY

3
00:00:28,000 --> 00:00:32,000
Directed by R. Flaherty.

4
00:00:33,157 --> 00:00:37,259
This film tells of the adventures

5
00:00:38,410 --> 00:00:41,255
cajun boy living.

6
00:00:41,852 --> 00:00:44,938
In the marshland Petit
Anse Bayu in Louisiana.

7
00:03:53,222 --> 00:03:59,545
His name is Alexander
Napoleon Ulysses Latour.

8
00:04:04,259 --> 00:04:07,671
He says that from the
sea swim mermaid.

9
00:04:07,961 --> 00:04:10,318
And their hair is green.

10
00:04:12,988 --> 00:04:15,663
And the boy often sees
bubbles on the surface.

11
00:04:28,929 --> 00:04:30,668
And still there are found.

12
00:04:30,932 --> 00:04:34,217
Long-nosed and red-eyed werewolves.

13
00:04:34,440 --> 00:04:37,131
On clear nights they
gather and dance.

14
00:05:47,845 --> 00:05:51,867
He would not have left the house without
this bag of salt tied to his belt

15
00:06:06,556 --> 00:06:09,861
and something else that
he hides in his bosom.

16
00:10:15,398 --> 00:10:17,309
Are there a lot of alligators here?

17
00:10:18,492 --> 00:10:21,882
Yes, full, however, before there was more.

18
00:10:22,221 --> 00:10:24,933
People after all hunt them,

19
00:10:25,332 --> 00:10:29,536
I myself hunted to sell skins.

20
00:10:30,225 --> 00:10:33,852
"Can they attack?"
Yes, it happens,

21
00:10:34,412 --> 00:10:36,977
if you try to catch them
or behave yourself stupid.

22
00:10:37,802 --> 00:10:41,217
I remember one old man

23
00:10:41,886 --> 00:10:44,724
took a fancy place for
swimming next to the house,

24
00:10:45,147 --> 00:10:46,651
his wife was still washing there.

25
00:10:47,567 --> 00:10:49,496
Well, he decided to swim.

26
00:10:49,905 --> 00:10:51,341
He jumped into the water,

27
00:10:51,621 --> 00:10:55,386
and there his alligator was waiting,

28
00:10:55,813 --> 00:10:57,907
healthy such, feet 8,

29
00:10:58,425 --> 00:11:03,049
that's enough of his alligator
for the throat and nose,

30
00:11:03,318 --> 00:11:05,418
and pulls his nose out with his throat...

31
00:11:05,762 --> 00:11:08,444
He died, and thank God.

32
00:11:08,656 --> 00:11:14,905
He was probably drunk, he often drank,

33
00:11:15,394 --> 00:11:18,403
and when I returned home,

34
00:11:18,644 --> 00:11:20,147
then sang a funny song:

35
00:11:20,365 --> 00:11:22,340
<i>"When you want to eat - I eat,"</i>

36
00:11:22,486 --> 00:11:24,024
<i>and when I want to sing - I drink,</i>

37
00:11:24,298 --> 00:11:26,233
<i>if the whiskey does not kill me,</i>

38
00:11:26,441 --> 00:11:28,075
<i>"Then I will live until I die. "</i>

39
00:11:35,615 --> 00:11:37,641
- Would you like to get coffee?
- Thank you.

40
00:11:38,738 --> 00:11:40,344
- Sugar?
- No thanks.

41
00:11:41,791 --> 00:11:43,335
And I'll put myself a little.

42
00:12:32,078 --> 00:12:33,281
Well, again.

43
00:12:34,092 --> 00:12:37,285
And that, so you can determine
whether there is oil here or not?

44
00:12:37,672 --> 00:12:41,181
Not 100%, but you can, Mr. Latour.

45
00:13:09,699 --> 00:13:13,775
Well, good luck,
you will need it!

46
00:13:17,759 --> 00:13:20,249
- Perhaps, it's time for me.
- Bye.

47
00:13:26,044 --> 00:13:28,757
- Bye.
- Happily, come again.

48
00:13:29,444 --> 00:13:31,357
When again you will be here.

49
00:21:21,750 --> 00:21:23,350
Dad, look!

50
00:23:36,496 --> 00:23:38,055
We have guests...

51
00:23:40,491 --> 00:23:41,491
Hello!

52
00:23:42,740 --> 00:23:44,204
Hello, monsieur.

53
00:23:45,595 --> 00:23:46,898
Where are you from?

54
00:23:48,454 --> 00:23:49,460
From there.

55
00:23:52,624 --> 00:23:54,827
And what is there in your pie?

56
00:24:00,917 --> 00:24:02,142
Wow!

57
00:24:02,402 --> 00:24:05,105
The biggest catfish
I've ever seen.

58
00:24:05,789 --> 00:24:07,878
What kind of bait do you catch?

59
00:24:09,361 --> 00:24:10,538
I'm not bait.

60
00:24:11,899 --> 00:24:14,137
"Not for bait?"
- No.

61
00:24:17,515 --> 00:24:20,334
Look. I'll show you how
to catch a big fish.

62
00:24:36,352 --> 00:24:37,637
And what are you doing here?

63
00:24:38,801 --> 00:24:40,405
What do you think?

64
00:24:41,501 --> 00:24:44,186
Father says that you are just crazy.

65
00:24:47,505 --> 00:24:48,804
Join us!

66
00:25:45,418 --> 00:25:49,792
Probably, out of order.
"Looks like the head has flown off."

67
00:28:07,734 --> 00:28:10,572
Excellent! Have taken!

68
00:30:02,943 --> 00:30:03,943
Come in!

69
00:30:06,623 --> 00:30:07,800
Come in!

70
00:33:51,780 --> 00:33:53,707
Look, I have salt.

71
00:33:56,239 --> 00:33:59,217
Salt? What for?

72
00:33:59,974 --> 00:34:01,443
It's against evil spirits.

73
00:34:02,508 --> 00:34:06,645
You throw salt in them.
and they swim away.

74
00:34:11,339 --> 00:34:13,058
And here I have something else.

75
00:34:14,612 --> 00:34:17,138
"The same for them?"
Yes. It's green.

76
00:34:24,163 --> 00:34:25,782
The best.

77
00:34:26,863 --> 00:34:28,589
Well, you're a cool guy.

78
00:34:29,953 --> 00:34:31,491
So here you are.

79
00:34:32,359 --> 00:34:35,124
I hope it does not bother you very much.

80
00:34:35,536 --> 00:34:36,866
Not at all.

81
00:34:38,587 --> 00:34:40,462
We are just happy.

82
00:34:49,582 --> 00:34:50,718
Come again.

83
00:34:53,614 --> 00:34:55,684
- Till!
- Till.

84
00:36:01,394 --> 00:36:02,604
Sit still, Yo-Yo.

85
00:36:11,300 --> 00:36:13,100
Sit still.

86
00:43:27,800 --> 00:43:29,604
Yo-Yo, where are you?

87
00:43:40,500 --> 00:43:42,400
Yo-yo!

88
00:48:20,023 --> 00:48:22,589
Are you here?

89
00:48:27,777 --> 00:48:29,341
Where are you?

90
00:50:14,333 --> 00:50:15,884
Now. Now we are...

91
00:50:20,657 --> 00:50:21,657
Father, where are you?

92
00:50:25,150 --> 00:50:26,175
Are you here?

93
00:50:30,718 --> 00:50:34,791
Where are you?

94
00:50:47,604 --> 00:50:49,433
Father, he does not have to leave!

95
00:51:24,980 --> 00:51:28,803
He does not have to leave!
Help me get this alligator!

96
00:51:39,898 --> 00:51:41,983
Help me, he's leaving!

97
00:53:06,720 --> 00:53:12,358
"He killed my raccoon."
"Nothing, we'll catch him."

98
00:53:15,823 --> 00:53:17,175
We'll catch!

99
00:53:49,612 --> 00:53:52,180
My God, you are mine! Who caught it?

100
00:53:52,384 --> 00:53:53,955
He was killed by my son.

101
00:53:54,115 --> 00:53:55,658
How, the boy himself?

102
00:53:56,304 --> 00:53:58,814
Yes, myself.

103
00:53:59,450 --> 00:54:01,520
So, the bait was the right thing!

104
00:54:04,607 --> 00:54:06,760
And he did not forget to spit at her.

105
00:54:52,993 --> 00:54:54,156
Hi Tom!

106
00:54:57,693 --> 00:54:59,620
I'm waiting - I can not wait for the fuel

107
00:54:59,984 --> 00:55:01,506
for my engine...

108
00:55:01,882 --> 00:55:03,842
Yes, for God's sake, one
bottle is enough for you?

109
00:55:06,743 --> 00:55:10,930
More than one bottle of this hole you
still will not be able to deflate.

110
00:55:21,430 --> 00:55:23,612
Go down, rest a little.

111
00:57:27,600 --> 00:57:29,500
Rather, the ejection went!

112
00:57:32,900 --> 00:57:34,700
Quickly! Stop the car!

113
00:57:36,700 --> 00:57:38,100
Quick, guys!

114
00:57:58,983 --> 00:58:00,502
Quick, screw the valve!

115
00:58:03,800 --> 00:58:05,302
Come on!

116
01:00:52,159 --> 01:00:53,855
<i>WILDKET WASTE EXPLOSION.</i>

117
01:00:54,537 --> 01:00:56,217
<i>Catastrophe on the first drilling platform</i>

118
01:00:57,159 --> 01:00:59,306
<i>in Petit Anse Baia,</i>

119
01:00:59,985 --> 01:01:01,778
<i>rented from Latour.</i>

120
01:01:02,458 --> 01:01:04,325
<i>As a result of a pressure of 8000 pounds</i>

121
01:01:05,017 --> 01:01:08,008
<i>demolished the stub and
rose from the bottom.</i>

122
01:01:08,680 --> 01:01:10,687
<i>A pillar of gas and salt water.</i>

123
01:01:10,783 --> 01:01:13,005
<i>Half a mile from the
drilling site is closed</i>

124
01:01:13,105 --> 01:01:15,216
<i>due to the danger of igniting the gas.</i>

125
01:01:16,345 --> 01:01:18,239
<i>FROM DRILLING CAN NOT REFUSE.</i>

126
01:01:18,519 --> 01:01:20,183
<i>Yesterday evening at half past 12th</i>

127
01:01:20,433 --> 01:01:22,150
<i>finally managed to stop the gas outflow</i>

128
01:01:22,300 --> 01:01:23,827
<i>on a drilling rig in Petit Anse Bayu.</i>

129
01:01:24,078 --> 01:01:26,437
<i>With this outburst, nothing
could be done for 10 days.</i>

130
01:01:26,600 --> 01:01:28,432
<i>This is the first case of
its kind in the region;</i>

131
01:01:28,456 --> 01:01:30,192
<i>Was without human sacrifice.</i>

132
01:01:30,483 --> 01:01:31,970
<i>Work drilling</i>

133
01:01:32,223 --> 01:01:33,941
<i>are waiting for further orders.</i>

134
01:01:49,873 --> 01:01:51,284
There's nothing more for you to do,

135
01:01:51,573 --> 01:01:54,274
except how to hang around here?

136
01:01:55,751 --> 01:01:57,063
Has he gone mad?

137
01:02:06,261 --> 01:02:09,538
Go home, do you hear me?

138
01:02:11,564 --> 01:02:13,703
There's nothing for you to hang around.

139
01:02:20,813 --> 01:02:25,229
Go home and do something useful,
or something else will happen.

140
01:02:30,190 --> 01:02:33,351
So that I can not see you here anymore,

141
01:02:33,450 --> 01:02:36,851
stay away from this place!

142
01:06:20,735 --> 01:06:23,212
So, so, look who's here.

143
01:06:29,070 --> 01:06:30,587
Where are you from?

144
01:06:34,123 --> 01:06:36,136
- Why did you lose the salt?
- Yes.

145
01:06:38,764 --> 01:06:41,447
Again he was hunting for a crocodile?

146
01:06:42,203 --> 01:06:44,540
I think we ourselves could
be re-qualified as hunters.

147
01:06:45,744 --> 01:06:48,194
Exactly, we would have
an excellent teacher.

148
01:06:50,045 --> 01:06:51,045
Yes.

149
01:06:52,099 --> 01:06:55,445
I think we could use some
salt for this well, right?

150
01:06:56,508 --> 01:06:57,815
Still would.

151
01:06:58,777 --> 01:07:00,358
I understood.

152
01:07:03,193 --> 01:07:05,663
We should do with the borehole
the same as with bait.

153
01:07:06,071 --> 01:07:07,385
I did it already.

154
01:08:02,899 --> 01:08:07,971
You know, these drillers
will be leaving soon.

155
01:08:11,057 --> 01:08:12,439
Yes, they leave from day to day.

156
01:08:14,477 --> 01:08:16,031
No, they will not.

157
01:08:18,440 --> 01:08:19,845
Will not they leave?

158
01:08:23,554 --> 01:08:26,377
And how do you know that
they will not leave?

159
01:08:28,023 --> 01:08:30,312
I know that's all.

160
01:09:34,243 --> 01:09:36,698
<i>On the Wildcat in Petit Anse Bayu</i>

161
01:09:37,019 --> 01:09:39,164
<i>our drillers personally showed,</i>

162
01:09:39,369 --> 01:09:41,228
<i>which means real skill.</i>

163
01:09:41,433 --> 01:09:43,238
<i>They not only managed in 10 days</i>

164
01:09:43,441 --> 01:09:45,248
<i>stop this dangerous release;</i>

165
01:09:45,400 --> 01:09:47,297
<i>By changing the angle, they were able to</i>

166
01:09:47,524 --> 01:09:48,791
<i>bypass the danger zone</i>

167
01:09:49,024 --> 01:09:51,080
<i>and reach a depth of 14032 feet.</i>

168
01:09:51,285 --> 01:09:53,213
<i>Now the usual procedures are coming -</i>

169
01:09:53,578 --> 01:09:55,900
<i>analysis of the quality of the obtained oil</i>

170
01:09:56,100 --> 01:09:58,100
<i>and a temporary plug of the well.</i>

171
01:12:02,662 --> 01:12:04,550
Well, now it's fine.

172
01:12:18,692 --> 01:12:20,875
Try it, Jean!

173
01:12:25,372 --> 01:12:26,484
Yes...

174
01:12:26,864 --> 01:12:28,996
There will now be fuel for your engine.

175
01:12:29,414 --> 01:12:31,702
I did not believe that you would succeed.

176
01:12:32,225 --> 01:12:35,099
And I knew from the beginning, Tom.

177
01:12:38,935 --> 01:12:40,102
True, I knew.

178
01:12:55,011 --> 01:12:59,791
You see how much I brought from the city.

179
01:12:59,969 --> 01:13:02,623
I said it was high
time to go shopping.

180
01:13:03,153 --> 01:13:06,381
So I went, everything,
as you wanted.

181
01:13:06,825 --> 01:13:08,660
Have you bought anything else?

182
01:13:08,866 --> 01:13:11,001
Still, now you'll see everything.

183
01:13:11,578 --> 01:13:14,993
"Take this."
- It?

184
01:13:15,192 --> 01:13:17,389
No, that's it. Give your mother.

185
01:13:25,845 --> 01:13:30,664
What a delight, I long
for this dreamed.

186
01:13:31,132 --> 01:13:33,288
- Like?
- Yes!

187
01:13:38,676 --> 01:13:42,877
"Did you bring me anything?"
No, it did not work.

188
01:13:43,058 --> 01:13:45,272
- Why?
- I wanted to... I'm already going...

189
01:13:45,500 --> 01:13:48,416
But then it began to rain.

190
01:13:48,548 --> 01:13:49,761
And you did not bring anything?

191
01:13:49,785 --> 01:13:51,658
Let me show you something.

192
01:13:51,755 --> 01:13:53,858
This is a wind gun. "A rifle?"

193
01:13:54,225 --> 01:13:55,901
Mom, look, the gun.

194
01:13:56,587 --> 01:13:57,774
- Do I get it?
- Yes.

195
01:14:03,725 --> 01:14:06,301
- It's a great thing!
- Beautiful!

196
01:14:07,391 --> 01:14:10,295
"It's for me, right?"
Of course you do.

197
01:14:12,967 --> 01:14:15,058
Wow!

198
01:14:16,609 --> 01:14:17,945
- Can?
- Yes.

199
01:14:18,645 --> 01:14:20,596
"Can I shoot him?"
- Yes.

200
01:14:21,165 --> 01:14:23,592
- For real?
- Yes!

201
01:14:24,267 --> 01:14:27,558
- Do you like?
- Still would! Such a beautiful gun!

202
01:14:37,180 --> 01:14:41,989
"No, not at home, be careful." Go to the street.
- Can?

203
01:14:42,242 --> 01:14:43,826
- Thank you, I went.
- Come on.

204
01:15:29,133 --> 01:15:30,133
Yo-yo!

205
01:15:52,946 --> 01:15:55,250
<i>Dear friends,</i>

206
01:15:56,043 --> 01:15:58,498
<i>Do you remember that day,</i>

207
01:15:58,711 --> 01:16:01,583
<i>when you sailed here on your boat</i>

208
01:16:01,880 --> 01:16:03,830
<i>and we signed this paper together?</i>

209
01:16:04,105 --> 01:16:06,475
<i>That day changed.</i>

210
01:16:06,807 --> 01:16:08,571
<i>All my life on these swamps,</i>

211
01:16:09,089 --> 01:16:11,038
<i>Now the drilling is taken away.</i>

212
01:16:42,595 --> 01:16:44,625
<i>But we will all ask the Lord,</i>

213
01:16:44,936 --> 01:16:47,671
<i>that the oil you have found,</i>

214
01:16:47,940 --> 01:16:49,532
<i>turned out to be good.</i>

215
01:18:18,580 --> 01:18:20,094
Look, Tom!

216
01:18:44,349 --> 01:18:47,218
END

217
01:19:00,680 --> 01:19:05,100
Scenario Robert and Francis Flaherty.

218
01:19:07,339 --> 01:19:10,920
Producer and director Robert Flaherty

