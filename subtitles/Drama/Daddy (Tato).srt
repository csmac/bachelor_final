﻿1
00:00:00,000 --> 00:00:10,567
<font color="#ec14bd">Sync & corrections by honeybunny</font>
<font color="#ec14bd">www.addic7ed.com</font>

2
00:00:20,012 --> 00:00:21,282
That's disgusting.

3
00:00:22,454 --> 00:00:25,303
- Want some?
- Hit me.

4
00:00:28,402 --> 00:00:29,631
See?

5
00:00:29,751 --> 00:00:31,722
<i>Constantly learning</i>
<i>something new.</i>

6
00:00:31,756 --> 00:00:33,730
Today's lesson:
Always get dressed for work

7
00:00:33,765 --> 00:00:35,637
after you give the baby a bath.

8
00:00:35,671 --> 00:00:36,907
Say good morning to Tucker

9
00:00:36,941 --> 00:00:37,910
and your Uncle Danny.

10
00:00:37,944 --> 00:00:39,916
- Hi, baby.
- ( Babbles )

11
00:00:39,950 --> 00:00:42,821
Okay, so I think
we're officially

12
00:00:42,856 --> 00:00:45,329
out of everything with
the word "baby" in it.

13
00:00:45,931 --> 00:00:46,966
Where's the list?

14
00:00:47,000 --> 00:00:48,869
What list?

15
00:00:48,904 --> 00:00:50,741
The one we should have started

16
00:00:50,776 --> 00:00:52,745
before we ran out
of everything.

17
00:00:55,307 --> 00:00:57,020
Why can't you just ask your mom

18
00:00:57,055 --> 00:00:59,892
- to go pick up some stuff?
- Oh, no no no no no.

19
00:00:59,927 --> 00:01:01,729
There will be no asking
the mom anything.

20
00:01:01,763 --> 00:01:03,632
She already thinks
I'm making a huge mistake,

21
00:01:03,667 --> 00:01:04,734
that I'm too irresponsible.

22
00:01:04,769 --> 00:01:06,537
So I'd like to
get a handle on this

23
00:01:06,572 --> 00:01:07,639
before she hits me with a giant

24
00:01:07,674 --> 00:01:08,975
"I told you so."

25
00:01:09,010 --> 00:01:10,946
Then get ready to be smacked,

26
00:01:10,980 --> 00:01:12,782
because I'm pretty sure
she's on her way over.

27
00:01:13,885 --> 00:01:15,955
What? Here, now?
Why do you think that?

28
00:01:15,989 --> 00:01:18,893
Because when I told her
you decided to keep Emma,

29
00:01:18,927 --> 00:01:20,762
all I heard was
"Oh, dear lord,"

30
00:01:20,797 --> 00:01:21,830
the phone hit the floor

31
00:01:21,864 --> 00:01:22,864
and tires squealing,

32
00:01:22,899 --> 00:01:24,968
so I kind of put
two and two together.

33
00:01:25,002 --> 00:01:26,972
Okay. Uh,
we gotta clean up.

34
00:01:27,006 --> 00:01:28,807
No, change the baby.

35
00:01:28,842 --> 00:01:31,578
No, I just did that.
A schedule.

36
00:01:31,612 --> 00:01:33,581
She's crazy
about having a plan.

37
00:01:33,616 --> 00:01:34,583
Okay, I've got a day shift,

38
00:01:34,617 --> 00:01:35,651
so I can be
home by 6:00.

39
00:01:35,685 --> 00:01:36,886
Sorry, but I'm heading
down to the rink.

40
00:01:36,920 --> 00:01:39,690
And it's emasculation
Saturday with Vanessa.

41
00:01:43,732 --> 00:01:44,899
I think we're sham shopping.

42
00:01:46,903 --> 00:01:49,906
Guys, come on.

43
00:01:49,940 --> 00:01:51,775
I really want to
prove mom wrong.

44
00:01:51,809 --> 00:01:53,878
I want to show her
I can be a good dad.

45
00:01:55,915 --> 00:01:57,082
Where's Emma?

46
00:01:58,085 --> 00:02:00,721
<i>( Theme music playing )</i>

47
00:02:00,755 --> 00:02:03,724
<i>♪ It's amazing</i>
<i>how the unexpected ♪</i>

48
00:02:03,759 --> 00:02:06,762
<i>♪ can take your life</i>
<i>and change direction. ♪</i>

49
00:02:10,702 --> 00:02:12,670
- Emma? Emma?
- Baby baby baby.

50
00:02:12,704 --> 00:02:14,805
- Where's the baby? Where's the baby?
- Emma?

51
00:02:14,840 --> 00:02:15,840
Emma? Found her!

52
00:02:15,874 --> 00:02:16,908
Under the table. Wait,

53
00:02:16,942 --> 00:02:17,976
here is my retainer.

54
00:02:18,010 --> 00:02:20,545
I have been looking
everywhere for this.

55
00:02:20,579 --> 00:02:21,813
Okay.

56
00:02:21,848 --> 00:02:22,948
Be careful.
Grab this end.

57
00:02:22,982 --> 00:02:24,015
All right?
On three.

58
00:02:24,050 --> 00:02:25,751
One, two, three.

59
00:02:25,785 --> 00:02:26,752
( Grunting )

60
00:02:26,786 --> 00:02:27,753
It's okay.

61
00:02:27,788 --> 00:02:29,356
Knock knock, child services.

62
00:02:37,800 --> 00:02:40,603
Yeah, you really should
get used to hearing that.

63
00:02:41,805 --> 00:02:43,873
Just playing a little hide and seek.
( Chuckles )

64
00:02:43,908 --> 00:02:45,843
Oh, there you are, Emma.

65
00:02:45,877 --> 00:02:47,044
You're such a good hider.

66
00:02:47,079 --> 00:02:48,747
Honey, she's not hiding,

67
00:02:48,781 --> 00:02:49,982
she's trying to escape.

68
00:02:51,885 --> 00:02:53,853
But don't you worry,
little Emma.

69
00:02:53,887 --> 00:02:56,857
Your fairy grandmother
is here to save you.

70
00:02:56,891 --> 00:02:58,792
Emma doesn't need to be saved.

71
00:02:58,826 --> 00:02:59,960
I've got this under control.

72
00:02:59,994 --> 00:03:02,730
Once again, it's going
to be up to me

73
00:03:02,765 --> 00:03:03,931
to bail you out.

74
00:03:03,966 --> 00:03:04,932
At least this time,

75
00:03:04,967 --> 00:03:06,968
I don't have to fly to Mexico.

76
00:03:09,672 --> 00:03:11,806
That was a good trip.

77
00:03:11,841 --> 00:03:13,741
You know I hate to say

78
00:03:13,776 --> 00:03:14,943
"I told you so," but--

79
00:03:14,977 --> 00:03:17,812
Mom, there is gonna be
no "I told you so" this time.

80
00:03:17,847 --> 00:03:19,715
You know, unless it's me
saying it to you.

81
00:03:19,749 --> 00:03:21,850
So you're telling me
you've worked out a plan

82
00:03:21,885 --> 00:03:24,687
- to take care of this baby.
- ( Scoffs ) Yeah.

83
00:03:24,721 --> 00:03:25,921
I was just working it out
with the guys.

84
00:03:25,956 --> 00:03:27,656
- Bonnie: <i>Oh.</i>
<i>- Just give me a second.</i>

85
00:03:29,860 --> 00:03:31,561
Please help me.

86
00:03:31,595 --> 00:03:33,029
( Whispering )

87
00:03:36,868 --> 00:03:38,803
Fine. I've got her
this morning.

88
00:03:38,837 --> 00:03:40,871
And I am taking her out
this afternoon.

89
00:03:40,906 --> 00:03:43,608
And I've got her tonight,
which is what we like to call

90
00:03:43,642 --> 00:03:45,043
a <i>plan!</i>

91
00:03:46,612 --> 00:03:48,013
Yeah, hold up there,
chip and Dale.

92
00:03:52,674 --> 00:03:55,576
Just so you know, if anything

93
00:03:55,610 --> 00:03:57,645
happens to my granddaughter,

94
00:03:57,679 --> 00:03:58,813
you're both dead men.

95
00:04:01,316 --> 00:04:03,751
Which is what I like
to call a <i>threat.</i>

96
00:04:08,756 --> 00:04:10,657
All right, Emma.

97
00:04:10,692 --> 00:04:13,627
I think we've got everything.

98
00:04:13,661 --> 00:04:16,729
Do I really need to take
all the baby stuff?

99
00:04:16,764 --> 00:04:18,297
Dude, you're gonna be gone

100
00:04:18,331 --> 00:04:19,731
for four or five hours.

101
00:04:19,766 --> 00:04:21,533
Just take a diaper
and a bottle.

102
00:04:21,567 --> 00:04:22,567
( Chuckles )

103
00:04:25,370 --> 00:04:27,571
<i>( Cell phone ringing )</i>

104
00:04:27,605 --> 00:04:29,506
- ( Beeps )
- Hey, man, what's going on?

105
00:04:29,540 --> 00:04:31,408
This plan is not working.

106
00:04:31,442 --> 00:04:33,476
I'm out of food,
her diaper's history,

107
00:04:33,510 --> 00:04:34,710
and the new one's
gonna blow any minute.

108
00:04:34,745 --> 00:04:36,479
And apparently
there's some new rule

109
00:04:36,513 --> 00:04:38,547
about bringing babies
into the locker room.

110
00:04:38,581 --> 00:04:41,383
Hey, buddy, why don't you put
a towel on or something?

111
00:04:41,417 --> 00:04:42,717
I got a baby over here.

112
00:04:42,751 --> 00:04:45,686
Rude! You gotta
get over here, now.

113
00:04:45,720 --> 00:04:47,688
I-- I can't right now.

114
00:04:47,722 --> 00:04:49,756
Okay, hang on one second.

115
00:04:49,791 --> 00:04:52,525
What? Okay.
I'm on my way. Okay?

116
00:04:53,527 --> 00:04:55,328
( Scoffs )

117
00:04:55,362 --> 00:04:56,796
Why is it always up to Tucker

118
00:04:56,830 --> 00:04:58,730
to save the day?
( Chuckles )

119
00:04:58,765 --> 00:05:00,698
<i>( Cell phone rings )</i>

120
00:05:00,733 --> 00:05:03,501
- Hey, Tuck, what's going on?
- I cannot do this, okay?

121
00:05:03,535 --> 00:05:05,669
I've got a lap full of pee,
and it isn't even mine.

122
00:05:07,772 --> 00:05:09,572
Hey! I could really
use a diaper out here

123
00:05:09,607 --> 00:05:11,341
and I know
one of y'all is holding.

124
00:05:12,576 --> 00:05:14,610
$20 for a diaper, miss.
Please? Baby or senior,

125
00:05:14,645 --> 00:05:16,712
at this point, it really don't matter.
Where you going?

126
00:05:20,316 --> 00:05:22,284
I'm sorry. I came as soon
as I got your message.

127
00:05:22,318 --> 00:05:23,352
Is everything okay?

128
00:05:23,386 --> 00:05:24,619
Ben:
Yeah, she's fine.

129
00:05:24,654 --> 00:05:28,589
Though I should remind you,
complete stranger,

130
00:05:28,624 --> 00:05:31,459
that we're not
a daycare center.

131
00:05:34,429 --> 00:05:35,696
( Whispers ) I'm five minutes
away from getting fired

132
00:05:35,730 --> 00:05:37,397
if anyone finds out
she's really my baby.

133
00:05:38,666 --> 00:05:40,600
I've been telling my boss
the mom's outside smoking

134
00:05:40,635 --> 00:05:42,402
for the last hour.

135
00:05:44,705 --> 00:05:46,505
Double Margarita, rocks.

136
00:05:46,540 --> 00:05:48,741
So, apparently, I'm a bad mom

137
00:05:48,775 --> 00:05:50,476
with a drinking problem.

138
00:05:52,679 --> 00:05:54,379
What the hell.

139
00:05:56,381 --> 00:05:57,515
I know, I know.
I'm sorry.

140
00:05:57,549 --> 00:05:58,782
I just have to get this right.

141
00:05:58,817 --> 00:06:01,318
Don't worry, Emma.
Daddy will figure this out.

142
00:06:04,321 --> 00:06:06,555
And he'll be right back. Can
you watch Emma for a sec?

143
00:06:09,525 --> 00:06:11,392
Wow. Wouldn't
have pegged her

144
00:06:11,427 --> 00:06:12,760
for a parenting expert.

145
00:06:14,363 --> 00:06:16,397
<i>Please?</i>
<i>Her name's Chloe.</i>

146
00:06:16,431 --> 00:06:18,533
She comes in every
Saturday for crab cakes.

147
00:06:18,567 --> 00:06:20,401
And if you've ever had ours,

148
00:06:20,435 --> 00:06:22,737
you'd know that's not why she's
really here. ( Chuckles )

149
00:06:24,506 --> 00:06:26,573
Oh, crapballs.

150
00:06:27,675 --> 00:06:30,343
- Hey, Benji.
- Hey.

151
00:06:30,378 --> 00:06:31,611
Has anyone seen Emma?

152
00:06:33,345 --> 00:06:36,478
( Gasps ) Oh, there she is.

153
00:06:36,512 --> 00:06:39,612
Oh, wow, you're right.
She really is a good hider.

154
00:06:39,646 --> 00:06:40,679
Mom.

155
00:06:40,713 --> 00:06:42,677
I guess I missed out
on the part of the plan

156
00:06:42,712 --> 00:06:44,542
where she hangs out
with the bar snacks.

157
00:06:46,511 --> 00:06:48,711
Oh, no, there's a baby
in my beer nuts.

158
00:06:52,615 --> 00:06:54,649
Something came up, okay?

159
00:06:54,684 --> 00:06:57,419
But like any good plan,
I had a plan B,

160
00:06:57,453 --> 00:06:59,387
also known as Riley.

161
00:07:01,356 --> 00:07:03,457
Words every girl
just lives to hear.

162
00:07:05,727 --> 00:07:08,529
So as you can see, we've got
this all under control.

163
00:07:08,563 --> 00:07:11,598
- Did you want something?
- Yeah, I actually wanted to talk about

164
00:07:11,633 --> 00:07:13,400
that sweet little
angel's future.

165
00:07:13,434 --> 00:07:15,569
I was thinking about
redoing my will--

166
00:07:15,603 --> 00:07:17,570
you know, setting up
a college savings account.

167
00:07:17,605 --> 00:07:20,340
You know, you really
can't be too proactive.

168
00:07:20,374 --> 00:07:22,408
Actually I've been taking
this estate planning class--

169
00:07:22,442 --> 00:07:24,510
Oh my God.

170
00:07:24,545 --> 00:07:27,312
I just had an amazing idea.

171
00:07:30,750 --> 00:07:33,351
That Riley should help you
with your estate planning,

172
00:07:33,385 --> 00:07:35,619
thus saving you money and giving
her some real world experience?

173
00:07:35,653 --> 00:07:37,321
That is an amazing idea.

174
00:07:38,523 --> 00:07:40,591
Why don't you two discuss
that while I go on break?

175
00:07:43,294 --> 00:07:45,561
I actually was gonna say, "Why
don't we order some fried things?"

176
00:07:45,596 --> 00:07:47,730
But I like his idea better.

177
00:07:47,764 --> 00:07:49,431
- Gonna finish that?
- Yeah.

178
00:07:49,465 --> 00:07:50,632
Okay.

179
00:08:04,414 --> 00:08:05,514
All right, she's out.

180
00:08:05,548 --> 00:08:07,249
( Sighs ) There's nothing cuter

181
00:08:07,283 --> 00:08:08,417
than a sleeping baby.

182
00:08:08,451 --> 00:08:09,551
Yeah, it's when they're awake

183
00:08:09,585 --> 00:08:10,652
you gotta watch your back.

184
00:08:12,521 --> 00:08:14,455
Yeah, they suck you in
with their cuteness

185
00:08:14,490 --> 00:08:16,224
and then once
they got you-- bam!

186
00:08:16,258 --> 00:08:18,259
--Fluids are flying everywhere.

187
00:08:21,598 --> 00:08:23,431
Oh. I'm so glad
you're all here.

188
00:08:23,466 --> 00:08:25,300
Uh, this is perfect.

189
00:08:25,334 --> 00:08:27,369
So a little change of plans.

190
00:08:27,403 --> 00:08:28,670
I know it was my turn
with Emma tonight,

191
00:08:28,704 --> 00:08:31,473
but crab cake Chloe
just invited me over

192
00:08:31,507 --> 00:08:33,341
to get to know
each other a little better,

193
00:08:33,375 --> 00:08:34,542
if you know what I mean.

194
00:08:34,577 --> 00:08:36,478
Well, you're
on your own tonight.

195
00:08:36,512 --> 00:08:39,024
- Tough break, dad.
- I hope she likes kids.

196
00:08:41,751 --> 00:08:44,185
So obviously
you don't know what I mean.

197
00:08:47,766 --> 00:08:50,071
Wait, where are you going?

198
00:08:50,311 --> 00:08:51,445
Did you all miss the part

199
00:08:51,479 --> 00:08:53,446
where she said she wants
to get to know me better?

200
00:08:55,282 --> 00:08:57,516
No. But you obviously
missed the part

201
00:08:57,551 --> 00:08:59,251
where we don't care.

202
00:09:02,221 --> 00:09:03,988
And also the part

203
00:09:04,022 --> 00:09:06,188
where I had to
throw out my pants.

204
00:09:08,321 --> 00:09:10,016
<i>Please,</i> guys?

205
00:09:10,136 --> 00:09:13,022
I'll be quick.
Well, not that quick.

206
00:09:14,424 --> 00:09:16,991
Well, if someone hadn't
already committed me

207
00:09:17,026 --> 00:09:18,259
to a fun-filled evening

208
00:09:18,294 --> 00:09:20,194
of helping his mother
with her will...

209
00:09:22,430 --> 00:09:24,364
Sorry about that.

210
00:09:24,399 --> 00:09:27,400
So probably not a good time
to ask for another favor.

211
00:09:27,435 --> 00:09:29,335
But if you could try
to wedge in a word or two

212
00:09:29,370 --> 00:09:31,037
about how great
I'm doing with Emma,

213
00:09:31,071 --> 00:09:32,271
you know, how responsible I am?

214
00:09:32,306 --> 00:09:34,206
So you want me to lie?

215
00:09:36,076 --> 00:09:37,243
Look, you're right.

216
00:09:37,277 --> 00:09:40,012
I said I'll figure
this out and I will.

217
00:09:40,046 --> 00:09:42,281
- I promise.
- Good man.

218
00:09:42,315 --> 00:09:44,049
<i>( Elevator dings )</i>

219
00:09:45,485 --> 00:09:48,353
But you two, not so fast.

220
00:09:48,387 --> 00:09:51,122
You just said you were
gonna figure it out.

221
00:09:51,156 --> 00:09:52,390
And that's what I'm doing.

222
00:09:52,424 --> 00:09:54,158
Come on, guys.
I've been working on Chloe

223
00:09:54,193 --> 00:09:55,459
for, like, five weeks.

224
00:09:55,494 --> 00:09:57,361
Okay, look,
you can probably tell

225
00:09:57,396 --> 00:09:59,297
from the extra hair product

226
00:09:59,331 --> 00:10:01,366
and the fact that
the big guy showered,

227
00:10:03,269 --> 00:10:05,070
that we have plans.

228
00:10:05,104 --> 00:10:06,271
Sorry, little bro.

229
00:10:06,306 --> 00:10:08,173
Work this one out on your own.

230
00:10:08,208 --> 00:10:10,075
I can't believe you're
gonna make me do it.

231
00:10:10,109 --> 00:10:12,244
I'm invoking the bro code.

232
00:10:19,118 --> 00:10:20,252
Are you serious?

233
00:10:20,286 --> 00:10:22,320
June 21st, 2010,

234
00:10:22,354 --> 00:10:24,422
who covered for you when
Vanessa caught you flirting

235
00:10:24,457 --> 00:10:26,391
with that unnaturally
enhanced waitress, huh?

236
00:10:27,427 --> 00:10:29,495
Senior year,
who told Jessica Feltshank

237
00:10:29,529 --> 00:10:31,330
that you were sick in bed when,
in fact, you were having

238
00:10:31,364 --> 00:10:33,365
carnal knowledge of her sister
in our tree house?

239
00:10:35,201 --> 00:10:38,337
Answer to both?
I did.

240
00:10:38,372 --> 00:10:42,075
Fine. Just go.

241
00:10:42,109 --> 00:10:44,344
Yes! Don't wait up.

242
00:10:46,413 --> 00:10:48,448
And remember, she needs
a second bottle around 10:00.

243
00:10:54,121 --> 00:10:55,355
I hate that code.

244
00:10:57,492 --> 00:11:00,126
I loved that tree house.

245
00:11:03,497 --> 00:11:07,067
Oh, do you just believe that after all
these years we would be here together?

246
00:11:07,101 --> 00:11:09,270
Yep. Life is...
♪ funny. ♪

247
00:11:11,306 --> 00:11:12,507
So if you have all the info,

248
00:11:12,541 --> 00:11:14,308
- we should be able--
- Oh, here.

249
00:11:17,245 --> 00:11:20,314
I like to stay organized, so I keep
all my important papers together.

250
00:11:22,184 --> 00:11:24,219
Oh, honestly, Riley,

251
00:11:24,253 --> 00:11:26,221
where did I go wrong?

252
00:11:26,255 --> 00:11:28,523
Wow, so many ways
to go with that.

253
00:11:28,557 --> 00:11:31,125
I meant with Ben.

254
00:11:32,394 --> 00:11:34,128
Well, I think
he turned out all right.

255
00:11:34,163 --> 00:11:35,496
In fact,

256
00:11:35,530 --> 00:11:37,464
he's one of the most
responsible young men

257
00:11:37,499 --> 00:11:40,068
- I've ever met.
- Oh.

258
00:11:40,102 --> 00:11:42,404
I'm sorry, I didn't know we were
drinking before this meeting.

259
00:11:44,140 --> 00:11:47,242
Well, he is sitting
at home on a Saturday night

260
00:11:47,277 --> 00:11:49,278
taking care of his daughter.

261
00:11:49,313 --> 00:11:51,514
That sounds awfully
responsible to me.

262
00:11:52,550 --> 00:11:54,117
Hi.

263
00:11:54,152 --> 00:11:55,519
For you, and I'm
pretty sure it's good

264
00:11:55,553 --> 00:11:58,289
because it's from a place I can't
pronounce and it has a cork.

265
00:11:58,323 --> 00:12:01,125
Nice. I'll get
some glasses.

266
00:12:01,160 --> 00:12:03,261
- All right.
- Make yourself comfortable.

267
00:12:08,200 --> 00:12:09,434
Wow, your place is really...

268
00:12:11,070 --> 00:12:12,204
Efficient.

269
00:12:14,074 --> 00:12:15,441
So is there a separate...

270
00:12:15,475 --> 00:12:17,043
Sleeping area?

271
00:12:17,077 --> 00:12:19,111
<i>The futon flips out into a bed.</i>

272
00:12:19,145 --> 00:12:22,047
<i>I hope you didn't think</i>
<i>I was too forward</i>

273
00:12:22,082 --> 00:12:23,315
<i>by asking you to come over.</i>

274
00:12:23,349 --> 00:12:25,484
Forward is
my favorite direction.

275
00:12:25,518 --> 00:12:27,519
<i>So many guys</i>
<i>get the wrong idea.</i>

276
00:12:27,553 --> 00:12:30,489
<i>They just automatically</i>
<i>assume you want to hook up.</i>

277
00:12:30,523 --> 00:12:32,357
<i>It's so nice</i>
<i>to finally meet a guy</i>

278
00:12:32,391 --> 00:12:33,525
<i>who's not like that.</i>

279
00:12:41,100 --> 00:12:42,267
See, the trouble with Ben is,

280
00:12:42,301 --> 00:12:43,501
he's exactly like his father,

281
00:12:43,536 --> 00:12:45,336
Danny Sr.

282
00:12:45,371 --> 00:12:47,438
( Groans ) Now there was a man

283
00:12:47,473 --> 00:12:50,341
who was clinically allergic
to responsibility.

284
00:12:52,444 --> 00:12:54,345
I liked Mr. Wheeler.

285
00:12:54,379 --> 00:12:56,147
God, he always put on the best

286
00:12:56,181 --> 00:12:58,082
4th of July fireworks displays.

287
00:12:58,117 --> 00:13:01,219
He has no eyebrows, and only
four fingers on his right hand.

288
00:13:04,123 --> 00:13:05,424
I was lucky he could sign

289
00:13:05,459 --> 00:13:07,230
the divorce papers.

290
00:13:07,264 --> 00:13:08,466
( Sighs )

291
00:13:08,500 --> 00:13:11,036
But with a father like that,

292
00:13:11,070 --> 00:13:14,206
it meant that I had to be
the responsible parent.

293
00:13:14,240 --> 00:13:16,341
The role model.

294
00:13:16,375 --> 00:13:18,277
Who's Larry Hoffman?

295
00:13:18,311 --> 00:13:19,344
What? Why? No,
I've never heard of--

296
00:13:19,379 --> 00:13:20,512
No no no, give me that.

297
00:13:20,546 --> 00:13:22,247
- ( Chuckling )
- Give it, give it, give it!

298
00:13:24,183 --> 00:13:26,485
That's funny
that you don't know him,

299
00:13:26,519 --> 00:13:30,088
because it says here
that you two were married

300
00:13:30,123 --> 00:13:33,458
in Vegas when you were 18.

301
00:13:35,394 --> 00:13:37,296
I really am sorry
about your futon.

302
00:13:37,330 --> 00:13:39,064
I certainly don't want
you to get the impression

303
00:13:39,098 --> 00:13:41,366
- that I'm one of those guys.
- My only impression so far

304
00:13:41,401 --> 00:13:44,069
is that you're a horrible liar.

305
00:13:44,103 --> 00:13:45,371
Which, if you think about it

306
00:13:45,405 --> 00:13:47,406
- is a good thing.
- Huh?

307
00:13:47,440 --> 00:13:49,374
I mean, who'd want
to go out with a good liar?

308
00:13:49,409 --> 00:13:51,476
( Chuckles ) Not me.

309
00:13:51,510 --> 00:13:54,479
Definitely something
for the plus column.

310
00:13:55,481 --> 00:13:57,081
Well, if there's
anything I can do

311
00:13:57,116 --> 00:13:59,383
- to help that total--
- There might be

312
00:13:59,417 --> 00:14:01,417
one or two things.

313
00:14:03,353 --> 00:14:05,021
That's not your baby, is it?

314
00:14:05,055 --> 00:14:07,456
What? Oh God, no.
Could you imagine?

315
00:14:07,491 --> 00:14:10,126
Shoot me.
( Chuckles )

316
00:14:10,161 --> 00:14:11,428
It's my sister's.

317
00:14:11,462 --> 00:14:14,064
- Why?
- Oh, no reason.

318
00:14:14,099 --> 00:14:16,100
- She's cute.
- ( Chuckles )

319
00:14:17,236 --> 00:14:18,403
Good night, Emma.

320
00:14:21,207 --> 00:14:22,473
Hey, Tuck, I was thinking.

321
00:14:22,508 --> 00:14:25,143
There's really no reason
we both need to stay in tonight,

322
00:14:25,177 --> 00:14:26,244
so I'm out of here.

323
00:14:31,517 --> 00:14:33,418
So I divorced Larry,

324
00:14:33,452 --> 00:14:36,120
I rushed home
and I married Danny Sr,

325
00:14:36,154 --> 00:14:38,322
which is what I should have
done in the first place.

326
00:14:38,356 --> 00:14:41,459
So, oddly,
if this hadn't happened,

327
00:14:41,493 --> 00:14:44,295
your two greatest accomplishments
never would have been created.

328
00:14:44,329 --> 00:14:46,197
How did you know
about my tattoos?

329
00:14:47,232 --> 00:14:49,433
I'm talking
about Danny and Ben.

330
00:14:49,468 --> 00:14:51,402
Your sons never
would have been born.

331
00:14:51,436 --> 00:14:53,437
That's a really good point,

332
00:14:53,472 --> 00:14:55,172
but maybe next time
you could try it

333
00:14:55,207 --> 00:14:57,074
without the sledgehammer.

334
00:14:57,109 --> 00:14:58,509
Even I can tell

335
00:14:58,544 --> 00:15:01,278
that you're being especially
hard on Ben lately.

336
00:15:02,280 --> 00:15:03,314
I mean, I know he'd hate me

337
00:15:03,348 --> 00:15:05,249
for telling you this,

338
00:15:05,284 --> 00:15:06,417
but he could really
use your help.

339
00:15:06,451 --> 00:15:08,148
Well, I am trying to.

340
00:15:08,183 --> 00:15:10,316
- He won't let me.
- Maybe because it's not worth

341
00:15:10,350 --> 00:15:12,351
the "I told you so"
that always comes with it.

342
00:15:12,385 --> 00:15:14,286
Well, I'm a mother.
It's a package deal.

343
00:15:15,422 --> 00:15:17,422
Well, you didn't
get to be a mom

344
00:15:17,457 --> 00:15:20,224
without making a few
mistakes, Mrs. Hoffman.

345
00:15:22,127 --> 00:15:23,394
So, maybe if you could admit

346
00:15:23,428 --> 00:15:25,129
that you weren't
always so perfect

347
00:15:25,163 --> 00:15:27,197
Ben could admit
that he needs help.

348
00:15:27,232 --> 00:15:29,065
You're just not gonna
let it go, are you?

349
00:15:29,100 --> 00:15:30,500
Not until you go talk to him.

350
00:15:32,136 --> 00:15:33,169
He needs you.

351
00:15:34,238 --> 00:15:36,305
Thanks, Riley.

352
00:15:36,339 --> 00:15:38,140
You know, you've
turned out to be

353
00:15:38,174 --> 00:15:40,308
a pretty smart young lady.

354
00:15:40,342 --> 00:15:42,476
I think you might
just make a go of this.

355
00:15:42,510 --> 00:15:44,411
- Being a lawyer?
- No, keeping the weight off.

356
00:15:44,445 --> 00:15:46,179
You barely touched
the cheese board.

357
00:15:51,518 --> 00:15:53,252
Ben honey, we need to talk.

358
00:15:56,289 --> 00:15:57,489
Ben?

359
00:16:04,195 --> 00:16:06,296
Oh hey, princess.

360
00:16:06,331 --> 00:16:08,165
<i>Oh, you're taking a nap.</i>

361
00:16:10,301 --> 00:16:12,169
Hey, big guy,
there's really no reason

362
00:16:12,204 --> 00:16:14,338
we both need to
stay in tonight, right?

363
00:16:14,372 --> 00:16:16,373
So, um, see you!

364
00:16:21,346 --> 00:16:22,513
Hello?

365
00:16:25,184 --> 00:16:26,384
Is anyone here?

366
00:16:29,154 --> 00:16:30,388
Ben?

367
00:16:30,422 --> 00:16:33,124
Danny?
The other one?

368
00:16:36,062 --> 00:16:37,463
Oh, this is gonna be the mother

369
00:16:37,497 --> 00:16:39,465
of all "I told you so"s.

370
00:16:46,602 --> 00:16:47,835
Is everything okay?

371
00:16:47,870 --> 00:16:49,704
I can't stop
thinking about Emma.

372
00:16:49,738 --> 00:16:50,905
Of course.

373
00:16:50,940 --> 00:16:52,840
Married, gay,
commitment issues--

374
00:16:52,875 --> 00:16:54,675
it's always something.

375
00:16:54,710 --> 00:16:56,139
No no, it's not that.

376
00:16:56,289 --> 00:16:59,191
Emma's my daughter.
I have a daughter.

377
00:16:59,225 --> 00:17:00,751
She was that little girl
at the bar today.

378
00:17:01,075 --> 00:17:02,568
Look, I know
I should have told you.

379
00:17:02,688 --> 00:17:04,228
I guess I was just afraid
that I wouldn't be here

380
00:17:04,263 --> 00:17:06,064
doing this with you
now if I had.

381
00:17:07,200 --> 00:17:09,234
Hey, it's okay.

382
00:17:09,269 --> 00:17:11,103
It's actually another thing

383
00:17:11,137 --> 00:17:12,270
for the plus column.

384
00:17:12,304 --> 00:17:15,142
- Really?
- Really.

385
00:17:17,040 --> 00:17:18,273
Do you want to see her picture?

386
00:17:18,307 --> 00:17:20,475
- Sure.
- ( Chuckles )

387
00:17:22,478 --> 00:17:25,079
I think she rolled over for
the first time this morning.

388
00:17:25,114 --> 00:17:27,114
- ( Chuckles )
- You know, maybe I'll call

389
00:17:27,149 --> 00:17:28,248
and check in
and then I'll be fine.

390
00:17:30,151 --> 00:17:33,052
- ( Rings )
- <i>( Dance music playing )</i>

391
00:17:33,087 --> 00:17:34,320
Danny, where are you?

392
00:17:34,354 --> 00:17:36,388
Hey, man. I just met a
couple of my new teammates

393
00:17:36,422 --> 00:17:38,423
- out for a drink.
- Ben: <i>Where's Emma?</i>

394
00:17:38,458 --> 00:17:41,193
Oh, she's totally conked out.
Tucker has it covered.

395
00:17:41,227 --> 00:17:42,394
( Laughs )

396
00:17:42,428 --> 00:17:44,262
I thought you were--

397
00:17:44,296 --> 00:17:46,430
Never mind.
Two seconds.

398
00:17:49,266 --> 00:17:50,500
Hey, Tuck,
I was just--

399
00:17:50,534 --> 00:17:53,102
<i>( bowling pins clatter )</i>
<i>- Are you bowling?</i>

400
00:17:53,136 --> 00:17:54,403
Where's Emma?!

401
00:17:54,438 --> 00:17:56,439
- Oh. I gotta go.
- ( Beeps )

402
00:17:57,507 --> 00:17:59,441
Emma, it's okay, daddy's here.

403
00:18:04,479 --> 00:18:07,147
Oh my God, Emma, where are you?

404
00:18:07,181 --> 00:18:09,182
Oh my God. No no no.
This can't be happening.

405
00:18:09,216 --> 00:18:12,051
- Oh my God.
- Looking for someone?

406
00:18:13,086 --> 00:18:15,321
Emma.
Thank God you're okay.

407
00:18:15,355 --> 00:18:19,324
I am so so sorry.
( Sighs )

408
00:18:19,359 --> 00:18:21,226
I thought I might
never see you again.

409
00:18:22,428 --> 00:18:25,463
I never want you out
of my sight, ever. Okay?

410
00:18:27,332 --> 00:18:29,066
I'm the worst father
in the world.

411
00:18:33,438 --> 00:18:35,139
Riley, can you take Emma?

412
00:18:39,077 --> 00:18:40,377
Come here, baby.

413
00:18:52,488 --> 00:18:55,423
I'm ready.
Let me have it.

414
00:18:55,457 --> 00:18:58,025
But just know that
nothing you can say to me

415
00:18:58,059 --> 00:19:00,994
can be any worse than what I'm
already saying to myself.

416
00:19:01,996 --> 00:19:04,296
Actually, Ben.

417
00:19:04,331 --> 00:19:06,198
All I wanted to say

418
00:19:06,232 --> 00:19:08,366
is that I'm proud of you.

419
00:19:08,401 --> 00:19:10,135
( Sniffles )

420
00:19:10,169 --> 00:19:13,337
Did you miss the part
where my baby got left alone?

421
00:19:15,440 --> 00:19:18,241
I once left Danny at a
Wal-Mart when he was two.

422
00:19:21,178 --> 00:19:23,379
- What happened?
- He was never seen again.

423
00:19:23,413 --> 00:19:24,480
( Mouths )

424
00:19:29,351 --> 00:19:32,486
My point--
I made mistakes.

425
00:19:34,022 --> 00:19:35,088
But that feeling?

426
00:19:36,090 --> 00:19:37,323
That gut-wrenching,

427
00:19:37,357 --> 00:19:39,158
heart-stopping,

428
00:19:39,192 --> 00:19:41,127
if-anything-
happens-to-my-child-

429
00:19:41,161 --> 00:19:42,328
I-think-I-will-die feeling--

430
00:19:42,362 --> 00:19:44,296
what you just had-- that?

431
00:19:44,330 --> 00:19:48,098
That is what let's you know
that you're a parent.

432
00:19:48,133 --> 00:19:50,267
And, honey, I guarantee
that you will do anything

433
00:19:50,301 --> 00:19:52,369
to make sure that
you never feel that again.

434
00:19:55,239 --> 00:19:57,340
So congratulations,

435
00:19:57,375 --> 00:19:59,375
now you're a dad.

436
00:20:02,112 --> 00:20:04,445
And I think you're
gonna be a pretty great one.

437
00:20:07,249 --> 00:20:08,382
But I don't know
what I'm doing.

438
00:20:08,416 --> 00:20:10,350
I know I said I got this,

439
00:20:10,385 --> 00:20:13,386
but I just <i>so</i> don't got this.

440
00:20:14,454 --> 00:20:17,023
- Emma!
- Well, if it isn't--

441
00:20:17,057 --> 00:20:19,158
me standing here,
talking to myself.

442
00:20:20,494 --> 00:20:22,262
Is she all right?

443
00:20:22,296 --> 00:20:24,531
She's got three boys
fawning over her.

444
00:20:26,100 --> 00:20:27,301
Works for most girls.

445
00:20:29,237 --> 00:20:31,138
Emma, I am so sorry.

446
00:20:31,172 --> 00:20:33,173
This will never happen again.

447
00:20:33,207 --> 00:20:34,374
Ben:
<i>Guys, it's all right.</i>

448
00:20:34,409 --> 00:20:37,244
We all make mistakes.

449
00:20:37,279 --> 00:20:39,079
But I think we could
definitely use some help.

450
00:20:40,449 --> 00:20:42,083
I need you, mom.

451
00:20:46,421 --> 00:20:48,255
Wait for it.

452
00:20:48,289 --> 00:20:49,753
I told you so.

453
00:20:49,873 --> 00:20:54,399
<font color="#ec14bd">Sync & corrections by honeybunny</font>
<font color="#ec14bd">www.addic7ed.com</font>

454
00:20:54,519 --> 00:20:56,543
Baby's at the ready...

455
00:20:57,518 --> 00:20:58,652
Oh, yeah.

456
00:20:58,687 --> 00:21:00,654
In three, two,

457
00:21:00,688 --> 00:21:01,622
one, roll!

458
00:21:01,656 --> 00:21:02,689
Come on, Emma, come here!

459
00:21:02,724 --> 00:21:04,491
- Roll!
- Come on, Emma. Roll to daddy.

460
00:21:04,525 --> 00:21:06,493
- Roll roll roll.
- Dan: This way, lead with your head.

461
00:21:06,527 --> 00:21:08,661
( All chattering )

462
00:21:09,696 --> 00:21:11,531
All: Oh!

463
00:21:11,565 --> 00:21:13,465
Are you kidding? Emma.

464
00:21:13,500 --> 00:21:14,565
Oh.

465
00:21:14,599 --> 00:21:16,664
- Double or nothing?
- You're on.

466
00:21:16,698 --> 00:21:19,298
- ( Laughing )
- Gentlemen, place your bets.

467
00:21:19,333 --> 00:21:20,700
<i>It's time for baby roll.</i>

