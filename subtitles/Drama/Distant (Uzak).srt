﻿1
00:02:45,131 --> 00:02:48,772
DISTANT

2
00:07:04,257 --> 00:07:06,464
Mahmut, it's Mom.

3
00:07:06,592 --> 00:07:09,971
I called earlier,
but you were out.

4
00:07:10,096 --> 00:07:12,599
Take care.

5
00:10:42,875 --> 00:10:44,479
Are you looking for someone?

6
00:10:44,610 --> 00:10:48,057
- Does Mahmut Ozdemir live here?
- Yes, he lives here.

7
00:10:48,180 --> 00:10:51,286
I'm a relative of his
from back home.

8
00:10:51,417 --> 00:10:55,092
- Did you ring the bell?
- There's no answer.

9
00:10:55,221 --> 00:10:57,360
Sometimes the bell shorts out.

10
00:10:57,490 --> 00:11:02,997
- Do you think he went out?
- I didn't see him leave.

11
00:11:05,564 --> 00:11:07,703
Good morning, Kamil.

12
00:11:07,833 --> 00:11:10,109
Could you accept
a package for me?

13
00:11:10,236 --> 00:11:13,649
Sometimes they won't
let me sign for it.

14
00:11:13,773 --> 00:11:17,311
- Please try anyway.
- Of course.

15
00:11:17,443 --> 00:11:19,514
- Kamil.
- Yes?

16
00:11:19,645 --> 00:11:21,750
Go buy the things
on the list.

17
00:11:28,754 --> 00:11:30,358
Right now?

18
00:11:30,489 --> 00:11:33,527
Yes, I've got
some cooking to do.

19
00:11:33,659 --> 00:11:35,502
- All right.
- Hurry up.

20
00:14:23,229 --> 00:14:24,503
Yusuf?

21
00:14:26,031 --> 00:14:29,774
Sorry, I completely forgot
you were coming.

22
00:14:29,902 --> 00:14:31,609
That's okay.

23
00:14:31,737 --> 00:14:32,738
No problem.

24
00:14:32,872 --> 00:14:34,044
What's the news?

25
00:14:34,173 --> 00:14:36,983
Back home, everybody's the same.

26
00:14:37,109 --> 00:14:38,611
Do you still work
at the factory?

27
00:14:40,412 --> 00:14:45,418
The economic crisis
put the factory out of business.

28
00:14:45,551 --> 00:14:47,531
My father was dismissed,
then me.

29
00:14:47,653 --> 00:14:50,964
There's no work at all now.

30
00:14:51,090 --> 00:14:52,364
How many people lost their job?

31
00:14:52,491 --> 00:14:53,435
1,000.

32
00:14:53,559 --> 00:14:57,097
That's a whole town
full of people.

33
00:14:58,864 --> 00:15:00,810
It sure is.

34
00:15:05,170 --> 00:15:06,877
What will you do on the ship?

35
00:15:09,141 --> 00:15:15,717
I'll work as a cabin boy
or a steward.

36
00:15:15,848 --> 00:15:20,058
I'll do whatever job
they've got for me.

37
00:15:20,185 --> 00:15:23,997
Sailors make a lot of money,
money in U.S. dollars.

38
00:15:24,123 --> 00:15:26,933
And you see the world.

39
00:15:27,059 --> 00:15:29,596
There's no economic crisis
at sea.

40
00:15:29,728 --> 00:15:31,969
The dollars make up
for the inflation.

41
00:15:32,097 --> 00:15:34,202
It may not be that easy.

42
00:15:34,333 --> 00:15:39,407
You'll be away on long journeys.

43
00:15:39,538 --> 00:15:42,348
Can you can take
that kind of loneliness?

44
00:15:44,510 --> 00:15:46,183
Have you thought about that?

45
00:15:48,113 --> 00:15:51,754
You know I like to travel.

46
00:15:51,884 --> 00:15:56,094
But the main thing
is to earn money.

47
00:15:56,221 --> 00:16:00,294
That's why I'm here.

48
00:16:00,426 --> 00:16:02,599
I also want to travel!

49
00:16:02,728 --> 00:16:05,470
You've been everywhere,
so why not me?

50
00:16:05,597 --> 00:16:08,908
Every place
ends up looking the same.

51
00:16:09,034 --> 00:16:13,141
How many days
before you find a job?

52
00:16:18,711 --> 00:16:20,349
I don't know exactly.

53
00:16:20,479 --> 00:16:22,015
You said about a week
on the phone.

54
00:16:26,819 --> 00:16:29,425
I guess it'll take a week or so.

55
00:16:31,223 --> 00:16:33,225
Yeah, about a week.

56
00:16:33,359 --> 00:16:38,468
At night, be careful
of the mouse glue strip.

57
00:16:38,597 --> 00:16:42,875
If you step on it,
you'll be stuck there all night.

58
00:16:44,136 --> 00:16:47,242
And don't use the toilet
in the bathroom.

59
00:16:47,373 --> 00:16:49,375
Use the other one.

60
00:16:49,508 --> 00:16:51,715
The one by the entrance?

61
00:16:51,844 --> 00:16:54,324
Yes, and we only smoke
in the kitchen.

62
00:16:54,446 --> 00:16:57,950
- Okay, then, good night.
- Good night.

63
00:22:35,053 --> 00:22:36,862
Come this way.

64
00:22:36,988 --> 00:22:39,491
Hello.

65
00:22:39,624 --> 00:22:44,403
Is this where the freighters
come in?

66
00:22:44,529 --> 00:22:47,806
They dock here,
but the offices are in Karakiiy.

67
00:22:47,933 --> 00:22:49,412
They dock here?

68
00:22:49,534 --> 00:22:52,276
Yes, they do,
but the offices are in Karakiiy.

69
00:22:52,404 --> 00:22:55,613
So I can't get a job
directly on board?

70
00:22:55,741 --> 00:22:57,015
No.

71
00:22:57,142 --> 00:22:59,053
Go to the shipping agents
on Monday.

72
00:22:59,511 --> 00:23:01,013
I'll come.

73
00:23:01,146 --> 00:23:03,126
You set it up.

74
00:23:07,252 --> 00:23:12,258
The thing is,
I'm coming with someone.

75
00:23:12,391 --> 00:23:16,237
No, he is someone
from my hometown.

76
00:23:16,361 --> 00:23:18,602
Will any girls be coming?

77
00:23:21,767 --> 00:23:25,340
Great, okay.

78
00:23:27,773 --> 00:23:29,013
We'll be there.

79
00:23:29,141 --> 00:23:32,850
See you tomorrow.

80
00:23:33,712 --> 00:23:35,316
Hi.

81
00:23:37,449 --> 00:23:39,087
Welcome back.

82
00:23:39,217 --> 00:23:40,389
God, it's cold.

83
00:23:40,519 --> 00:23:42,726
It's so late.
I was getting worried.

84
00:23:42,854 --> 00:23:44,356
God, the snow is bad.

85
00:23:44,489 --> 00:23:48,494
How could you go out
in this weather?

86
00:23:48,627 --> 00:23:50,163
My socks are soaked.

87
00:23:50,295 --> 00:23:53,401
Can I dry them on the radiator?

88
00:23:53,532 --> 00:23:54,510
Yes, you can.

89
00:24:03,141 --> 00:24:05,143
How'd the job hunting go?

90
00:24:05,277 --> 00:24:07,348
Well, I went down to the port.

91
00:24:07,479 --> 00:24:09,186
I'll dry my shoes too.

92
00:24:09,314 --> 00:24:10,486
All right.

93
00:24:10,615 --> 00:24:12,993
It seems you don't find jobs
at the port.

94
00:24:15,053 --> 00:24:18,830
I was told to go to Karakiiy
on Monday.

95
00:24:18,957 --> 00:24:20,732
I walked around Eminonu.

96
00:24:22,761 --> 00:24:25,367
I'd better put on a new pair
of socks.

97
00:24:35,974 --> 00:24:39,717
Money doesn't make you happy
anymore.

98
00:24:39,845 --> 00:24:42,451
And I think you're looking
for your past here.

99
00:24:42,581 --> 00:24:44,254
You're rambling.

100
00:24:44,382 --> 00:24:47,022
Maybe,
but have you already forgotten

101
00:24:47,152 --> 00:24:50,565
our climb
to the summit of Reshko

102
00:24:50,689 --> 00:24:53,863
to get a better shot
of the White Valley?

103
00:24:53,992 --> 00:24:57,201
You used to say then
you'd make films like Tarkovsky.

104
00:24:57,329 --> 00:25:00,867
So why are you trying
to forget those days?

105
00:25:00,999 --> 00:25:04,310
- Photography is finished, man.
- No, it's not.

106
00:25:04,436 --> 00:25:05,506
The mountains too.

107
00:25:05,637 --> 00:25:08,174
Maybe it's you who's done for.

108
00:25:08,306 --> 00:25:10,786
You're announcing your death
before it's happened.

109
00:25:10,909 --> 00:25:14,982
You don't have the right
to bury your ideals

110
00:25:15,113 --> 00:25:17,650
or make everything commercial.

111
00:25:17,782 --> 00:25:22,060
Hey, Arif, where are the chicks?

112
00:25:22,187 --> 00:25:23,564
They all backed out.

113
00:25:23,688 --> 00:25:24,689
What do you mean?

114
00:25:24,823 --> 00:25:27,463
We've reserved the whole Sunday.
Where are they?

115
00:25:27,592 --> 00:25:28,593
Not here.

116
00:25:28,727 --> 00:25:31,469
At the cinema or wherever.
Fuck it.

117
00:25:31,596 --> 00:25:34,634
You're weaseling
out of it again.

118
00:25:34,766 --> 00:25:38,043
Photography or women,
which do you prefer?

119
00:25:38,169 --> 00:25:39,739
I prefer photography.

120
00:25:39,871 --> 00:25:42,613
Come on.
Photography is dead.

121
00:25:44,509 --> 00:25:48,218
This fits here normally,
but it's too loose.

122
00:25:48,346 --> 00:25:52,158
Tell your mother
to wrap some tape here.

123
00:25:52,284 --> 00:25:54,127
Hello.

124
00:25:54,252 --> 00:25:57,233
There's a package.
Shall I get it now?

125
00:25:57,355 --> 00:25:58,698
Please.

126
00:26:10,569 --> 00:26:13,243
If you want, I'll wait for it.

127
00:26:13,371 --> 00:26:14,577
Okay.

128
00:27:30,815 --> 00:27:33,091
For the love of God.

129
00:27:33,218 --> 00:27:36,256
We can't find anything
once we've put it down.

130
00:27:38,223 --> 00:27:39,566
Here it is.

131
00:27:39,691 --> 00:27:41,432
- Thanks.
- Good evening.

132
00:27:41,559 --> 00:27:43,539
Good evening to you too.

133
00:27:43,662 --> 00:27:47,371
There's also a problem
with the dust bag.

134
00:27:47,499 --> 00:27:50,742
Let's go to your house to show
your mother how to attach it.

135
00:28:56,534 --> 00:29:00,607
- I think I'd better go to bed.
- Okay.

136
00:29:10,115 --> 00:29:12,391
Good night.
See you tomorrow.

137
00:29:12,517 --> 00:29:13,825
Good night.

138
00:29:15,153 --> 00:29:17,633
Could you close the door?

139
00:31:32,857 --> 00:31:34,666
Hi, Mom,
what are you up to?

140
00:31:36,027 --> 00:31:37,904
Is your tooth ache better?

141
00:31:40,632 --> 00:31:42,976
Why don't you let
that old tooth-puller, Nuri,

142
00:31:43,101 --> 00:31:44,637
take care of it?

143
00:31:46,537 --> 00:31:48,710
'Cause Mahmut's sleeping.

144
00:31:50,508 --> 00:31:52,249
I am sure Nuri gives credit.

145
00:31:53,678 --> 00:31:56,750
I'll pay it once I earn it.
Tell him.

146
00:31:56,881 --> 00:31:59,225
What are you ashamed of?

147
00:32:08,559 --> 00:32:10,903
Yeah, so I've been around.

148
00:32:11,029 --> 00:32:14,272
I was told to go
to Karakiiy on Monday.

149
00:32:16,968 --> 00:32:19,209
What did Dad do?

150
00:32:22,106 --> 00:32:25,383
There's no chance the factory
will take him back.

151
00:32:25,510 --> 00:32:28,616
He should stop kidding himself.

152
00:32:28,746 --> 00:32:32,159
They always say that.

153
00:32:32,283 --> 00:32:34,593
Anyway, I've got to hang up.

154
00:32:34,719 --> 00:32:40,192
Stop putting off that tooth,
Mom, okay?

155
00:32:40,325 --> 00:32:43,465
I'll call you again.
Good night.

156
00:32:57,208 --> 00:33:00,314
Can I take the magazine
over there?

157
00:33:00,445 --> 00:33:02,447
I'll turn on the light
for a second.

158
00:34:07,712 --> 00:34:10,556
It's late.
Let's turn that off.

159
00:34:15,653 --> 00:34:19,066
- Good night.
- Good night.

160
00:34:43,481 --> 00:34:45,893
Do you know where
the sailors coffeehouse is?

161
00:34:46,017 --> 00:34:47,291
Behind that hilltop.

162
00:35:15,680 --> 00:35:17,751
May I sit here?

163
00:35:38,102 --> 00:35:42,482
At your age,
I thought the same, but listen:

164
00:35:42,607 --> 00:35:45,816
I've worked on the ships
for months now,

165
00:35:45,943 --> 00:35:49,117
and I have nothing.

166
00:35:49,247 --> 00:35:52,194
I never could send
any money home.

167
00:35:52,316 --> 00:35:54,489
l am broke just like Tarzan.

168
00:35:54,619 --> 00:35:56,326
Listen to me.

169
00:35:56,454 --> 00:35:58,661
There's no work
and no money on the ships.

170
00:35:58,789 --> 00:36:00,735
Forget it
while you're still ahead.

171
00:36:00,858 --> 00:36:03,395
It's an empty dream.

172
00:36:58,616 --> 00:36:59,594
Hi.

173
00:36:59,717 --> 00:37:01,594
Come on in, Yusuf.

174
00:37:01,719 --> 00:37:04,996
It's snowing like crazy.

175
00:37:05,122 --> 00:37:07,728
Where were you 'til now?

176
00:37:07,858 --> 00:37:10,998
I was trying to find a job,
you know.

177
00:37:11,128 --> 00:37:13,631
So what happened?

178
00:37:13,764 --> 00:37:18,042
Well, some said
they would call me.

179
00:37:18,169 --> 00:37:21,548
Others told me to come back

180
00:37:21,672 --> 00:37:24,380
or that they'd think about it.

181
00:37:27,245 --> 00:37:28,622
What are you doing?

182
00:37:28,746 --> 00:37:29,520
What can I do?

183
00:37:29,647 --> 00:37:31,058
Surfing around...

184
00:37:31,882 --> 00:37:34,453
ls there a problem with the job?

185
00:37:34,585 --> 00:37:37,156
No, there's no problem.

186
00:37:37,288 --> 00:37:41,065
It looks like they want someone
to act as a guarantor.

187
00:37:41,192 --> 00:37:43,263
No guarantor, no job.

188
00:37:43,394 --> 00:37:47,206
That's easy enough.
You can take care of that.

189
00:37:47,331 --> 00:37:50,778
But I don't know anybody
in Istanbul.

190
00:37:50,901 --> 00:37:52,505
I don't know what to do.

191
00:37:56,407 --> 00:37:58,216
There was something here.

192
00:37:58,342 --> 00:37:59,912
I just can't seem to...

193
00:38:08,419 --> 00:38:10,956
The God-damned thing
has 50 channels,

194
00:38:11,088 --> 00:38:12,658
but there's only shit.

195
00:38:14,992 --> 00:38:17,063
What a rip-off.
Look at that.

196
00:40:05,903 --> 00:40:13,822
I want to call home.
Can I use the phone?

197
00:40:15,112 --> 00:40:16,716
All right.

198
00:40:16,847 --> 00:40:19,157
The phone in the back room.

199
00:40:19,283 --> 00:40:21,923
Okay, go use it.

200
00:40:57,154 --> 00:40:59,065
He refused to give us
any credit?

201
00:40:59,190 --> 00:41:01,830
What a bastard!

202
00:41:07,865 --> 00:41:10,778
When I get back,
I'll punch him real good.

203
00:41:12,870 --> 00:41:18,650
I'll punch out his teeth
one by one.

204
00:41:23,881 --> 00:41:26,020
Anyway, forget about it.

205
00:41:26,150 --> 00:41:28,096
I'll send you some money
as soon as I get a job.

206
00:41:30,287 --> 00:41:34,565
Mahmut's going to Anatolia
to take pictures,

207
00:41:34,692 --> 00:41:38,834
and he says I should
go with him.

208
00:41:38,963 --> 00:41:41,876
If he gives me money,
I'll send it to you.

209
00:41:41,999 --> 00:41:45,708
Then you throw that money
in that bastard's face.

210
00:41:51,041 --> 00:41:53,920
Okay, take care.

211
00:43:36,580 --> 00:43:39,959
That fucking mouse.

212
00:44:34,438 --> 00:44:38,682
"Our agency has no current need
for ship personnel."

213
00:44:43,247 --> 00:44:44,658
Hello.

214
00:44:44,782 --> 00:44:48,457
I'm looking for a job on a ship.

215
00:44:48,585 --> 00:44:49,962
There are no jobs.

216
00:44:50,087 --> 00:44:53,125
All right.
Thanks anyway.

217
00:45:29,226 --> 00:45:30,899
So it's Canada?

218
00:45:31,028 --> 00:45:33,531
Orhan got a very good offer.

219
00:45:33,664 --> 00:45:35,871
We don't want
to miss this opportunity.

220
00:45:39,770 --> 00:45:42,273
When are you leaving?

221
00:45:42,406 --> 00:45:44,386
In two or three weeks.

222
00:45:44,508 --> 00:45:50,584
By New Year, we'll be in a new
country starting a new life.

223
00:45:52,316 --> 00:45:54,262
It's hard.

224
00:45:56,520 --> 00:46:01,401
It's not so hard when you don't
have that much to leave behind.

225
00:46:04,795 --> 00:46:06,832
We spent a lot of money
on doctors.

226
00:46:06,964 --> 00:46:09,069
That's one reason
to sell the house.

227
00:46:12,369 --> 00:46:14,371
We can settle that.

228
00:46:14,505 --> 00:46:16,781
But I keep thinking
about that baby matter.

229
00:46:16,907 --> 00:46:23,586
How can they be sure it happened
because of the abortion you got?

230
00:46:23,714 --> 00:46:27,787
They just say the abortion
is the most probable reason.

231
00:46:27,918 --> 00:46:30,057
I was three months pregnant
if you remember.

232
00:46:32,122 --> 00:46:33,396
I remember.

233
00:46:34,625 --> 00:46:38,232
But we were about
to get divorced.

234
00:46:38,362 --> 00:46:42,037
That's why I didn't want it.

235
00:46:42,166 --> 00:46:43,076
I'm not blaming you.

236
00:46:43,200 --> 00:46:45,544
Really, don't worry about it.

237
00:46:52,976 --> 00:46:55,650
You should see a doctor
in Canada.

238
00:46:55,779 --> 00:46:58,623
I still doubt that one abortion
can cause infertility.

239
00:47:01,318 --> 00:47:03,821
We didn't believe it either
at first.

240
00:47:03,954 --> 00:47:05,695
We went to different doctors.

241
00:47:05,823 --> 00:47:07,666
But that's the way it is.

242
00:47:11,161 --> 00:47:19,161
I've resigned myself, actually,
but it's hard for Orhan.

243
00:47:21,171 --> 00:47:23,173
He really likes children.

244
00:47:28,545 --> 00:47:30,616
I told him we could separate.

245
00:47:32,015 --> 00:47:34,552
He doesn't want to.

246
00:54:54,758 --> 00:54:55,998
It's empty.

247
00:54:56,126 --> 00:54:58,504
Have one of these
sailor cigarettes.

248
00:54:58,628 --> 00:55:00,835
How can you smoke that shit?

249
00:55:28,091 --> 00:55:30,537
There's still sun coming.

250
00:55:31,828 --> 00:55:32,863
Raise it a little.

251
00:55:32,996 --> 00:55:34,339
It seems okay.

252
00:55:35,932 --> 00:55:37,138
Don't you see that?

253
00:55:39,035 --> 00:55:40,343
Now, there it is.

254
00:56:18,842 --> 00:56:20,583
Raise that light a little.

255
00:56:24,114 --> 00:56:25,855
Leave it.

256
00:56:29,686 --> 00:56:32,166
Didn't you see me
do that before?

257
00:57:05,455 --> 00:57:06,763
Why did we stop?

258
00:57:06,890 --> 00:57:08,733
Open that window.

259
00:57:15,999 --> 00:57:19,105
God, what a place to photograph.

260
00:57:23,640 --> 00:57:25,142
It's best to shoot from the top.

261
00:57:25,275 --> 00:57:27,915
Sheep in front
and the lake behind.

262
00:57:28,044 --> 00:57:30,285
Shall I set up the camera now?

263
00:57:36,252 --> 00:57:38,596
Shall I?

264
00:57:41,991 --> 00:57:43,732
Mahmut?

265
00:57:46,429 --> 00:57:47,533
Fuck it.

266
00:57:47,664 --> 00:57:48,870
Why bother?

267
00:57:48,998 --> 00:57:50,033
I'll set it up right now.

268
00:57:50,166 --> 00:57:50,871
Forget it.

269
00:58:12,388 --> 00:58:17,360
Kamil, put it down on the rug.
I'll take it from there.

270
00:58:33,376 --> 00:58:35,549
Mahmut, this is your sister.
Mom is sick.

271
00:58:35,678 --> 00:58:37,180
We're rushing to emergency.

272
00:58:37,313 --> 00:58:38,257
Call me back.

273
00:58:44,387 --> 00:58:46,094
Mahmut, where the hell are you?

274
00:58:46,222 --> 00:58:48,031
Mom's been hospitalized.

275
00:58:48,157 --> 00:58:49,465
She'll be operated on tomorrow.

276
00:58:49,592 --> 00:58:51,003
I'm staying with her.

277
00:58:51,127 --> 00:58:53,437
My kid's suffering
and so is my work.

278
00:58:53,563 --> 00:58:55,133
You could at least come
and help.

279
00:58:55,265 --> 00:58:57,506
How can you refuse
to get a mobile?

280
00:58:57,634 --> 00:58:59,204
You're so irresponsible.

281
00:59:10,847 --> 00:59:13,953
- God, I'm tired.
- Take this.

282
00:59:14,083 --> 00:59:17,223
Isn't it too much?

283
00:59:17,353 --> 00:59:20,129
In that case, give some back.

284
00:59:20,256 --> 00:59:24,204
If you take more photos,
I'd like to come along again.

285
00:59:42,779 --> 00:59:44,383
What's wrong, Mom?

286
00:59:44,514 --> 00:59:45,959
Can't you sleep?

287
00:59:46,082 --> 00:59:50,224
It hurts.
Right here.

288
00:59:52,922 --> 00:59:57,393
Maybe it's just gas.

289
00:59:57,527 --> 01:00:02,374
Do you think
I should walk a bit?

290
01:00:02,498 --> 01:00:05,342
All right, let's give it a try.

291
01:00:08,071 --> 01:00:09,948
Take the serum too.

292
01:00:16,746 --> 01:00:18,919
Come on, now.

293
01:00:19,048 --> 01:00:21,255
Hold the serum up.

294
01:00:21,384 --> 01:00:24,058
Higher, higher.

295
01:00:25,154 --> 01:00:26,531
Or else blood comes.

296
01:00:26,656 --> 01:00:28,966
Easy, now.

297
01:00:32,862 --> 01:00:35,468
It hurts more when I lie down.

298
01:00:46,242 --> 01:00:52,090
Sickness comes
when you least expect it.

299
01:00:52,215 --> 01:00:55,492
And you smoke so much!

300
01:00:55,618 --> 01:00:59,532
I don't understand
why you smoke so much.

301
01:00:59,656 --> 01:01:01,158
I can't stop, Mom.

302
01:01:01,290 --> 01:01:02,564
I've tried...

303
01:01:02,692 --> 01:01:04,535
Yes, you can.

304
01:01:04,661 --> 01:01:08,234
First cut down,
and then give it up altogether.

305
01:03:33,209 --> 01:03:36,554
Just a bit more, sweetie.

306
01:05:06,769 --> 01:05:11,047
I can't find anything
in this house.

307
01:05:13,676 --> 01:05:15,986
I'm closing the door
to keep the noise down.

308
01:06:01,791 --> 01:06:05,034
Yusuf, are you there?

309
01:06:05,161 --> 01:06:06,401
It's Mahmut.

310
01:06:06,529 --> 01:06:10,033
If you're home,
pick up the phone.

311
01:06:13,703 --> 01:06:14,977
Hey, Mahmut.

312
01:06:15,104 --> 01:06:17,550
Where were you?
I've called you all day.

313
01:06:17,673 --> 01:06:19,653
Well you know, I went to...

314
01:06:19,775 --> 01:06:21,482
How's your mom doing?

315
01:06:21,610 --> 01:06:22,884
We brought her home.

316
01:06:23,012 --> 01:06:24,150
She's resting.

317
01:06:24,280 --> 01:06:25,657
Listen...

318
01:06:25,781 --> 01:06:30,287
I've got something going on
at my place tonight.

319
01:06:30,419 --> 01:06:34,390
Can you get lost
until ten or so?

320
01:06:34,523 --> 01:06:37,060
Sure, I'll go to Beyoglu
or around there.

321
01:06:37,193 --> 01:06:38,365
I've got this thing to do.

322
01:06:38,494 --> 01:06:40,974
I got it.

323
01:10:03,832 --> 01:10:06,005
Son of a bitch.

324
01:10:23,052 --> 01:10:24,963
Look at that.

325
01:10:25,087 --> 01:10:27,124
Little prick.

326
01:10:41,537 --> 01:10:44,416
Filthy son of a bitch!

327
01:14:38,941 --> 01:14:41,080
- Is she gone?
- ls who gone?

328
01:14:41,210 --> 01:14:42,621
You know.

329
01:14:42,744 --> 01:14:44,246
Did you smoke
in the living room?

330
01:14:44,379 --> 01:14:45,756
No, I didn't.

331
01:14:45,881 --> 01:14:47,451
Come on, stop lying.

332
01:14:47,583 --> 01:14:49,062
It reeked here when I came in.

333
01:14:49,184 --> 01:14:51,824
And there were ashes
on the floor.

334
01:14:51,954 --> 01:14:54,730
Remember that day
when we smoked together?

335
01:14:54,857 --> 01:14:56,666
I thought you smoked here too.

336
01:14:56,792 --> 01:14:58,100
Listen to me!

337
01:14:58,227 --> 01:15:01,640
When I turn my back,
you take advantage of it.

338
01:15:01,763 --> 01:15:04,642
I often told you to flush
the toilet, for in stance.

339
01:15:04,766 --> 01:15:06,109
No, you didn't.

340
01:15:06,235 --> 01:15:07,771
Do I have to tell you this?

341
01:15:07,903 --> 01:15:10,008
At your age,
haven't you learned yet?

342
01:15:12,541 --> 01:15:15,545
When you're a guest,
you have to be a little careful.

343
01:15:15,677 --> 01:15:19,352
I'm not home for a day,
and you shit everywhere.

344
01:15:19,481 --> 01:15:21,722
I've got lots of worries,

345
01:15:21,850 --> 01:15:24,694
and now I
have to clean your shit?

346
01:15:28,657 --> 01:15:30,034
Listen...

347
01:15:30,158 --> 01:15:31,796
No more smoking
in the kitchen, either.

348
01:15:31,927 --> 01:15:33,497
I've quit.

349
01:15:33,629 --> 01:15:35,108
Okay.

350
01:16:28,050 --> 01:16:29,961
What do you think?

351
01:16:37,960 --> 01:16:39,962
I bought it for my niece.

352
01:16:42,864 --> 01:16:44,969
Isn't it great?

353
01:16:45,100 --> 01:16:46,977
What happened with the ship job?

354
01:16:47,102 --> 01:16:48,979
Nothing definite yet.

355
01:16:49,104 --> 01:16:50,606
I'm waiting for some final word.

356
01:16:50,739 --> 01:16:53,345
When will you know for sure?

357
01:16:53,475 --> 01:16:54,476
I don't know.

358
01:16:54,610 --> 01:16:56,783
I keep running after jobs.

359
01:16:56,912 --> 01:17:00,189
They all say they'll let me know
in a few days.

360
01:17:00,315 --> 01:17:03,023
Will you go back home
if you don't find a job?

361
01:17:03,151 --> 01:17:04,357
No way.

362
01:17:04,486 --> 01:17:06,557
So what the hell will you do?

363
01:17:06,688 --> 01:17:10,101
If I return to the village now,
I'll never get out.

364
01:17:10,225 --> 01:17:14,571
And there's no work
at the factory.

365
01:17:14,696 --> 01:17:16,607
So what are your plans?

366
01:17:23,772 --> 01:17:28,414
Shut the fucking thing off.
I'm asking you something here.

367
01:17:35,017 --> 01:17:38,692
Could you get me a job
at the tile factory?

368
01:17:38,820 --> 01:17:41,266
Sure, they're just waiting
for you.

369
01:17:41,390 --> 01:17:44,667
Don't you think the recession
has hit here too?

370
01:17:44,793 --> 01:17:50,368
Maybe they need a guard
or something like that.

371
01:17:50,499 --> 01:17:52,069
A guard?

372
01:17:52,200 --> 01:17:55,613
Two days alone here,
and look what happened.

373
01:17:55,737 --> 01:17:57,808
You have no credentials.

374
01:17:57,939 --> 01:18:01,386
What work could you do
if they did hire you?

375
01:18:01,510 --> 01:18:05,856
Plant beans,
work as a steward, or what?

376
01:18:05,981 --> 01:18:06,891
Come on, Mahmut.

377
01:18:07,015 --> 01:18:09,427
Don't be so hard on me.

378
01:18:09,551 --> 01:18:11,531
I just want you to ask them.

379
01:18:11,653 --> 01:18:13,599
I'd do the same for you.

380
01:18:13,722 --> 01:18:16,032
I've never asked them
for anything.

381
01:18:16,158 --> 01:18:19,105
This town has changed you.

382
01:18:19,227 --> 01:18:21,207
Listen kid, ever heard of pride?

383
01:18:21,329 --> 01:18:23,104
You can't just throw it
on a garbage heap.

384
01:18:23,231 --> 01:18:28,078
What would you lose by trying?

385
01:18:28,203 --> 01:18:29,648
You don't know shit.

386
01:18:29,771 --> 01:18:31,773
You're running off at the mouth.

387
01:18:31,907 --> 01:18:33,511
You think it's that easy?

388
01:18:33,642 --> 01:18:36,646
I've taken photographs
for them for ten years.

389
01:18:36,778 --> 01:18:40,988
And I didn't even get a discount
for the tiles on the balcony.

390
01:18:41,116 --> 01:18:44,723
You come in from the country
and look for someone with pull.

391
01:18:44,853 --> 01:18:48,426
You don't worry
about learning any skills.

392
01:18:48,557 --> 01:18:52,095
You start looking for a cousin,
an M.P. or whoever.

393
01:18:52,227 --> 01:18:55,071
You want it all on a platter.

394
01:18:55,197 --> 01:18:59,646
I did everything by myself here.

395
01:18:59,768 --> 01:19:02,612
I came to Istanbul
without a cent.

396
01:19:20,922 --> 01:19:23,766
You hop on a bus
without thinking ahead,

397
01:19:23,892 --> 01:19:26,099
and you become a burden.

398
01:19:28,697 --> 01:19:31,610
That fucking mouse
doesn't leave the kitchen.

399
01:19:31,733 --> 01:19:35,180
We get caught,
but the fucker doesn't.

400
01:19:39,875 --> 01:19:42,048
Shut it off!

401
01:21:28,049 --> 01:21:30,188
Do you have
any Sezen Aksu music?

402
01:21:30,318 --> 01:21:31,490
No.

403
01:21:38,226 --> 01:21:41,264
Who's that "Bak"
on all those CDs over there?

404
01:21:55,343 --> 01:21:57,482
What are you looking for?

405
01:22:06,054 --> 01:22:08,534
A watch.
Have you seen it?

406
01:22:08,657 --> 01:22:10,295
What kind?

407
01:22:10,425 --> 01:22:13,372
A silver pocket watch.

408
01:22:13,495 --> 01:22:14,803
No, I haven't seen it.

409
01:22:14,930 --> 01:22:15,738
Are you sure?

410
01:22:15,864 --> 01:22:17,639
I'm positive.

411
01:22:17,766 --> 01:22:19,507
If I had, I would tell you.

412
01:22:19,634 --> 01:22:21,545
Why wouldn't I?

413
01:22:45,560 --> 01:22:48,131
Where did you keep it?

414
01:22:48,263 --> 01:22:50,504
In the middle drawer of my desk.

415
01:22:51,800 --> 01:22:53,746
When did you put it there?

416
01:22:53,868 --> 01:22:56,508
Just a month ago.

417
01:22:56,638 --> 01:22:58,709
I used it as a prop
on my last shoot.

418
01:23:00,041 --> 01:23:02,385
I swear, I haven't seen it.

419
01:23:32,674 --> 01:23:35,814
Maybe you put it
somewhere else...

420
01:23:37,746 --> 01:23:40,750
or gave it to a friend.

421
01:23:41,316 --> 01:23:42,761
Forget about it.

422
01:23:42,884 --> 01:23:44,386
It's not that important.

423
01:23:44,519 --> 01:23:46,123
Of course it's important.

424
01:23:46,254 --> 01:23:48,063
Just drop it, okay?
Fuck it.

425
01:23:55,363 --> 01:23:56,341
Hello?

426
01:23:56,464 --> 01:23:58,910
Mahmut, hi, it's me, Nazan.

427
01:23:59,034 --> 01:24:01,446
Hi, Nazan, how are you?

428
01:24:01,569 --> 01:24:04,641
We're leaving tomorrow,
and I wanted to say good-bye.

429
01:24:04,773 --> 01:24:06,252
Tomorrow?
That's so soon.

430
01:24:06,374 --> 01:24:08,752
Yeah, on the 9:00 plane.

431
01:24:08,877 --> 01:24:12,825
We may never
see each other again.

432
01:24:12,947 --> 01:24:18,989
I wanted to thank you again
for signing the paper.

433
01:24:19,120 --> 01:24:21,930
Forget about it.
It was nothing.

434
01:24:22,057 --> 01:24:24,162
Can you hold for a second?

435
01:24:37,739 --> 01:24:40,720
Okay, we can talk now.

436
01:24:40,842 --> 01:24:46,554
When I left that day,
I had a terrible feeling.

437
01:24:46,681 --> 01:24:48,854
I don't want to leave
feeling like this.

438
01:24:48,983 --> 01:24:53,864
I was somewhat distant towards
you because of my emotions.

439
01:24:53,988 --> 01:24:56,332
But after all these years,
there's no need for that.

440
01:24:56,458 --> 01:24:59,462
It doesn't matter.
Forget it.

441
01:24:59,594 --> 01:25:03,337
Nazan, listen...

442
01:25:09,137 --> 01:25:11,708
I think Orhan's coming.

443
01:25:11,840 --> 01:25:13,148
I have to hang up.

444
01:25:13,274 --> 01:25:14,048
All right.

445
01:25:14,175 --> 01:25:16,849
What were you going to say?

446
01:25:16,978 --> 01:25:18,753
I'll tell you some other time.

447
01:25:21,716 --> 01:25:24,060
Take care.

448
01:25:24,185 --> 01:25:25,323
Okay.

449
01:25:25,453 --> 01:25:26,557
Bye.

450
01:25:26,688 --> 01:25:28,429
Bye.

451
01:26:04,726 --> 01:26:07,366
I swear I never saw it.

452
01:26:07,495 --> 01:26:08,565
Okay, enough!

453
01:26:08,696 --> 01:26:10,266
I didn't even know
where it was.

454
01:26:10,398 --> 01:26:12,105
Stop chewing on it!

455
01:26:12,233 --> 01:26:15,407
I don't want to hear
about it anymore.

456
01:26:15,537 --> 01:26:18,677
God, what a character.

457
01:30:58,219 --> 01:30:59,926
Leave it.

458
01:31:00,054 --> 01:31:02,159
The janitor will take care of it
tomorrow.

459
01:31:02,290 --> 01:31:08,104
It'll be squeaking like this
all night.

460
01:31:08,229 --> 01:31:11,574
What can we do?
Can you handle it?

461
01:31:11,699 --> 01:31:12,939
Yes, I can.

462
01:31:13,067 --> 01:31:15,547
But how can I get it unstuck?

463
01:31:15,670 --> 01:31:17,115
No need to.

464
01:31:17,238 --> 01:31:19,548
Put the whole thing
in a plastic bag.

465
01:31:19,674 --> 01:31:21,915
Close it and throw it out
in the trash.

466
01:31:22,043 --> 01:31:24,023
Alive like that?

467
01:31:24,145 --> 01:31:26,421
What else can we do?

468
01:43:32,606 --> 01:43:35,849
For Ebru.

