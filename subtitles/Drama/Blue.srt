1
00:00:42,519 --> 00:00:45,481
THREE COLORS
BLUE

2
00:02:01,555 --> 00:02:03,636
Come on, Anne.

3
00:02:05,557 --> 00:02:07,638
Get in.

4
00:03:56,608 --> 00:03:58,689
Are you able to talk?

5
00:04:04,611 --> 00:04:06,692
During the... Were you conscious?

6
00:04:11,615 --> 00:04:16,577
I'm sorry to have to inform you...
Do you know?

7
00:04:19,618 --> 00:04:21,699
Your husband died in the accident.

8
00:04:29,623 --> 00:04:34,585
But you weren't conscious
all that time.

9
00:04:35,626 --> 00:04:37,706
Anne?

10
00:04:40,628 --> 00:04:42,709
Yes, your daughter too.

11
00:05:11,642 --> 00:05:13,723
Is someone there? Who did it?

12
00:05:25,648 --> 00:05:26,609
Mr. Leroy?

13
00:05:26,649 --> 00:05:31,611
Please call the guards. Somebody
broke a window on the 1st floor.

14
00:05:37,654 --> 00:05:39,735
I'll look in the garden.

15
00:06:34,680 --> 00:06:36,761
I can't do that.

16
00:06:42,683 --> 00:06:44,764
- I broke the window.
- Don't worry.

17
00:06:49,687 --> 00:06:51,768
We'll replace it.

18
00:06:53,489 --> 00:06:56,770
- I am sorry.
- Don't worry.

19
00:07:53,716 --> 00:07:58,678
- Is it today?
- This afternoon. At 17.00.

20
00:08:01,719 --> 00:08:03,800
Can I do anything for you?

21
00:09:18,754 --> 00:09:23,717
We gathered to pay respects

22
00:09:24,757 --> 00:09:26,838
to the memory of the man

23
00:09:27,759 --> 00:09:29,840
and composer,

24
00:09:31,760 --> 00:09:35,722
known as one of the
most outstanding in the world.

25
00:09:35,762 --> 00:09:37,723
Nobody can accept

26
00:09:37,763 --> 00:09:39,844
that he is gone.

27
00:09:42,765 --> 00:09:47,728
We also mourn his 5 year-old
daughter.

28
00:09:48,768 --> 00:09:50,849
Who died by his side.

29
00:09:54,771 --> 00:09:56,852
Patrice, millions of men
and women

30
00:09:59,773 --> 00:10:04,735
awaited the music you composed

31
00:10:05,776 --> 00:10:07,737
for the Unification of Europe,

32
00:10:07,777 --> 00:10:12,739
which we all hope to
celebrate soon.

33
00:11:05,803 --> 00:11:07,884
Good morning.

34
00:11:18,809 --> 00:11:20,890
Good morning.

35
00:11:22,811 --> 00:11:27,773
- I know you don't want to see me.
- Right.

36
00:11:29,814 --> 00:11:34,776
- Can I come in?
- No.

37
00:11:35,817 --> 00:11:37,778
Julie, I don't want an interview.

38
00:11:37,818 --> 00:11:42,780
- What then?
- I'm writing an article about your husband.

39
00:11:44,821 --> 00:11:48,783
- I need to know something.
- About what?

40
00:11:48,823 --> 00:11:52,785
The concert
for the Unification of Europe.

41
00:11:52,825 --> 00:11:57,787
- It doesn't exist.
- You've changed.

42
00:11:57,827 --> 00:12:01,789
- You were not so rude before.
- Haven't you heard?

43
00:12:01,829 --> 00:12:06,791
I lost in my daughter and
husband in an accident.

44
00:12:07,831 --> 00:12:12,794
Is it true you wrote
your husband's music?

45
00:12:54,853 --> 00:12:56,934
Good morning.

46
00:12:57,854 --> 00:12:59,935
Bernard!

47
00:13:01,856 --> 00:13:05,818
- Good morning madam.
- Did you do what I asked?

48
00:13:05,858 --> 00:13:07,819
- Did you clear the blue room?
- Everything.

49
00:13:07,859 --> 00:13:12,821
Madam, we're all deeply sorry.

50
00:14:33,898 --> 00:14:35,979
Marie?

51
00:14:42,902 --> 00:14:44,983
Why are you crying?

52
00:14:46,904 --> 00:14:48,985
Because you're not.

53
00:14:54,908 --> 00:14:56,989
I keep thinking about them.

54
00:14:57,909 --> 00:14:59,990
I remember everything.

55
00:15:01,911 --> 00:15:03,992
- No thanks.
- Lets sit down.
This could take a while
- I'll be right back

56
00:17:03,966 --> 00:17:06,047
Nobody must know about it.

57
00:17:07,968 --> 00:17:12,930
You will pay for my mother's
rest home for the rest of her life.

58
00:17:13,971 --> 00:17:18,933
You will take care of
Marie and the gardener.

59
00:17:18,973 --> 00:17:21,054
You will sell all our property

60
00:17:21,975 --> 00:17:25,936
and pay all the money
in one account.

61
00:17:25,976 --> 00:17:30,939
- 27-0641-196?
- Yes.

62
00:17:30,979 --> 00:17:33,060
May I ask why?

63
00:17:34,980 --> 00:17:37,061
No.

64
00:17:37,982 --> 00:17:40,063
Would you excuse me.

65
00:17:48,987 --> 00:17:53,949
- But what will you have left?
- My own account.

66
00:20:08,050 --> 00:20:10,131
I haven't started yet.

67
00:20:12,052 --> 00:20:17,014
- I put it aside...
- The day I left?
- Yes

68
00:20:19,055 --> 00:20:24,017
Here it is. I was waiting
to hear from you.

69
00:20:24,057 --> 00:20:26,138
You were right.

70
00:20:35,062 --> 00:20:37,143
A lot of corrections.

71
00:20:39,064 --> 00:20:41,145
No more than usual.

72
00:20:45,067 --> 00:20:47,148
It's beautiful.

73
00:20:49,069 --> 00:20:51,150
I love this chorus.

74
00:23:01,129 --> 00:23:03,210
It's Julie. Do you love me?

75
00:23:05,131 --> 00:23:09,093
- Yes.
- Since when?

76
00:23:09,133 --> 00:23:12,094
Since I started working with
Patrice.

77
00:23:12,134 --> 00:23:17,096
- Do you think of me?
- Yes.

78
00:23:18,137 --> 00:23:19,097
Come if you want to.

79
00:23:19,137 --> 00:23:23,099
- Now?
- Yes. Now. Right away.

80
00:23:23,139 --> 00:23:28,101
- Are you sure?
- Yes.

81
00:24:07,159 --> 00:24:09,240
Olivier?

82
00:24:19,165 --> 00:24:24,127
- I fell.
- Please take it off.

83
00:24:34,171 --> 00:24:36,252
The rest too.

84
00:25:44,203 --> 00:25:49,166
They took everything.
Only the mattress is left.

85
00:26:23,221 --> 00:26:25,302
I appreciate what you did for me.

86
00:26:29,224 --> 00:26:34,186
But you see I'm like others.
I sweat, cough.

87
00:26:34,226 --> 00:26:36,307
I have cavities.

88
00:26:37,227 --> 00:26:42,190
You won't miss me.
You understand that now.

89
00:26:48,232 --> 00:26:50,313
Shut the door when you leave.

90
00:27:07,241 --> 00:27:09,322
Julie!

91
00:28:23,276 --> 00:28:26,237
I don't want any children
in the building.

92
00:28:26,277 --> 00:28:28,358
Let me ask you something.
It may help.

93
00:28:31,279 --> 00:28:35,241
- What do you do?
- Nothing.

94
00:28:35,281 --> 00:28:40,243
- I mean, for a living?
- Nothing.

95
00:28:41,284 --> 00:28:46,246
I have what you are looking for.
But the apartment needs repairing.
It may take a while.

96
00:28:48,287 --> 00:28:53,249
- Can I live there now?
- Sure. It needs a quick paint.

97
00:28:53,289 --> 00:28:57,251
- Can I look see it now?
- Yes.

98
00:28:57,291 --> 00:29:02,253
- What is your name?
- Julie de Courcy, with a "Y".

99
00:29:04,294 --> 00:29:09,257
Sorry, Julie Vignon. I am
going back to my maiden name.

100
00:31:04,349 --> 00:31:09,311
- Coffee and ice cream?
- As usual.

101
00:34:50,452 --> 00:34:52,533
Is anybody there?

102
00:34:58,456 --> 00:35:00,537
Is anybody there?

103
00:38:03,540 --> 00:38:08,702
- I'm sorry for the noise. I've almost finished
- I haven't heard anything.

104
00:38:09,543 --> 00:38:11,624
Can I come in?

105
00:38:12,544 --> 00:38:16,506
I heard you got locked out
last week.

106
00:38:16,546 --> 00:38:19,507
Your husband lent me a blanket.
I spent the night in the stairwell.

107
00:38:19,547 --> 00:38:21,628
I wanted you to sign this.

108
00:38:22,549 --> 00:38:27,511
- What is it?
- Everyone has signed already.

109
00:38:27,551 --> 00:38:32,513
We don't want loose women in our building.
The young woman downstairs...

110
00:38:33,554 --> 00:38:38,516
- I don't want to get involved.
- She is a whore.

111
00:38:40,557 --> 00:38:42,638
I don't care.

112
00:39:56,591 --> 00:40:01,554
You are in perfect health.
Everything is fine.

113
00:40:04,595 --> 00:40:06,676
You are in good spirits.

114
00:40:10,598 --> 00:40:15,560
- Hello, it's Antoine.
- It is for you.

115
00:40:18,601 --> 00:40:20,562
My name is Antoine.
You don't know me...

116
00:40:20,602 --> 00:40:21,563
No I don't. Excuse me.

117
00:40:21,603 --> 00:40:25,565
- Who is it?
- Some boy.

118
00:40:25,605 --> 00:40:29,566
He has been looking for you.
I said he could call.

119
00:40:29,606 --> 00:40:34,569
- Lets meet. It's important.
- Nothing is important.

120
00:40:35,609 --> 00:40:39,571
- It is about an object.
- What?

121
00:40:39,611 --> 00:40:43,692
A necklace with a cross.

122
00:41:05,623 --> 00:41:07,704
I'd forgotten about it.

123
00:41:09,625 --> 00:41:11,706
I found it near the car.

124
00:41:14,627 --> 00:41:19,589
I couldn't keep it.
That's stealing.

125
00:41:21,630 --> 00:41:25,592
If you want to ask me something,
I got there just after...

126
00:41:25,632 --> 00:41:27,713
No.

127
00:41:40,639 --> 00:41:42,720
I'm sorry.

128
00:41:46,642 --> 00:41:49,603
I wanted to return the necklace...

129
00:41:49,643 --> 00:41:54,605
- I wanted to ask you something too.
- Yes?

130
00:41:55,646 --> 00:42:00,608
When I opened the door...
your husband was still alive.

131
00:42:03,649 --> 00:42:05,610
He said...

132
00:42:05,650 --> 00:42:07,611
I don't understand.

133
00:42:07,651 --> 00:42:09,732
He said "Now try coughing."

134
00:42:32,663 --> 00:42:34,743
He was telling us a joke.

135
00:42:35,664 --> 00:42:40,626
It's about a woman who can't stop
coughing. She sees a doctor.

136
00:42:41,667 --> 00:42:46,629
He examined her and gave her a pill.
She swallowed it.

137
00:42:47,669 --> 00:42:49,630
And asked "What was it?"

138
00:42:49,670 --> 00:42:51,751
"The most powerful laxative".

139
00:42:54,673 --> 00:42:57,634
"A laxative for a cough?"

140
00:42:57,674 --> 00:43:02,636
"Yes. Try coughing now."

141
00:43:02,676 --> 00:43:04,757
It made us laugh.

142
00:43:05,678 --> 00:43:10,640
Then the car crashed.

143
00:43:14,682 --> 00:43:19,644
My husband always liked
to repeat the punch line.

144
00:43:19,684 --> 00:43:21,765
You returned it.

145
00:43:24,686 --> 00:43:26,767
It is yours.

146
00:44:41,721 --> 00:44:43,802
Are you sick?

147
00:44:49,725 --> 00:44:51,806
Are you ok?

148
00:45:01,730 --> 00:45:06,693
- You always gotta hold onto something.
- What did you say?

149
00:45:35,746 --> 00:45:37,707
Thank you.

150
00:45:37,747 --> 00:45:41,709
- For what?
- I'm staying.

151
00:45:41,749 --> 00:45:42,709
In order to throw me away,

152
00:45:42,749 --> 00:45:47,711
they had to collect signatures
of all occupants.

153
00:45:49,752 --> 00:45:51,833
Your place is nice.

154
00:46:01,758 --> 00:46:05,720
When I was a little girl,
I had a lamp like this.

155
00:46:05,760 --> 00:46:10,722
I'd stand under it
and stretch out my hand.

156
00:46:10,762 --> 00:46:15,724
I wanted to jump
and touch it.

157
00:46:20,766 --> 00:46:22,847
I forgot all about it.

158
00:46:27,770 --> 00:46:29,730
Where did you find it?

159
00:46:29,771 --> 00:46:31,851
I found it.

160
00:46:33,772 --> 00:46:35,853
Souvenir?

161
00:46:36,774 --> 00:46:38,855
Do you live here alone?

162
00:46:40,776 --> 00:46:42,736
Yes.

163
00:46:42,776 --> 00:46:44,857
I could never spend
a night alone.

164
00:46:49,780 --> 00:46:54,742
Something must have happened.
You are not the type someone dumps.

165
00:47:10,789 --> 00:47:12,870
Forgive me, I talk too much.

166
00:47:22,795 --> 00:47:24,876
Poor guy.

167
00:47:26,796 --> 00:47:29,758
- Who?
- He was asleep there last night.

168
00:47:29,798 --> 00:47:31,879
He's gone now but left the flute.

169
00:47:43,804 --> 00:47:45,885
Julie?

170
00:47:51,808 --> 00:47:53,889
A coffee.

171
00:48:01,812 --> 00:48:03,893
I searched everywhere.

172
00:48:04,814 --> 00:48:06,895
And I found you.

173
00:48:09,816 --> 00:48:11,897
Nobody knows where I live.

174
00:48:12,817 --> 00:48:14,898
It took a few months.
Then I got lucky.

175
00:48:16,819 --> 00:48:21,782
My cleaning lady's daughter
saw you in the area.

176
00:48:26,824 --> 00:48:28,905
I've come here ever since.
I miss you.

177
00:48:35,828 --> 00:48:37,909
Did you run away?

178
00:48:42,831 --> 00:48:44,912
From me?

179
00:49:31,853 --> 00:49:33,934
Do you hear what he is playing?

180
00:49:40,858 --> 00:49:45,820
- It sounds like...
- Yes.

181
00:49:49,862 --> 00:49:54,824
I've seen you. Maybe that will
do for now.

182
00:49:56,865 --> 00:49:58,946
I'll try.

183
00:50:22,877 --> 00:50:27,839
- How do you know this music?
- I invent lots of things. I like to play.

184
00:51:59,921 --> 00:52:02,002
Just a second.

185
00:52:02,722 --> 00:52:04,883
A problem with your place?

186
00:52:04,923 --> 00:52:07,884
No. I would like another one
like this.

187
00:52:07,925 --> 00:52:12,887
- I can manage that but it will take time.
- How long?

188
00:52:12,927 --> 00:52:15,008
2-3 months.

189
00:52:17,929 --> 00:52:20,010
You cut yourself.

190
00:52:22,931 --> 00:52:25,012
My cat scratched me.

191
00:53:42,968 --> 00:53:45,049
Mom?

192
00:53:46,970 --> 00:53:49,051
Marie-France.

193
00:53:51,972 --> 00:53:54,053
It's me. Julie.

194
00:53:58,975 --> 00:54:01,056
Come to me.

195
00:54:11,981 --> 00:54:16,943
I've heard you are dead.
You seem fine.

196
00:54:18,984 --> 00:54:20,945
Very young.

197
00:54:20,985 --> 00:54:25,947
You were always younger,
you look 30 now.

198
00:54:26,988 --> 00:54:31,950
I'm not your sister,
but daughter. I'm 33.

199
00:54:31,990 --> 00:54:33,951
I know.

200
00:54:33,991 --> 00:54:34,951
I was kidding.

201
00:54:34,991 --> 00:54:39,954
I have everything here.
The TV...

202
00:54:41,995 --> 00:54:44,076
I see the entire world.

203
00:54:46,997 --> 00:54:51,959
- Do you watch too?
- No.

204
00:55:00,003 --> 00:55:04,965
Tell me about your house,
husband...

205
00:55:05,005 --> 00:55:07,086
children...

206
00:55:08,007 --> 00:55:12,969
- Or about you?
- Mom...

207
00:55:17,011 --> 00:55:21,973
My husband and daughter are dead.
I have no home anymore.

208
00:55:23,013 --> 00:55:25,094
I heard about it.

209
00:55:34,018 --> 00:55:38,981
I was happy before.
I loved them and they loved me.

210
00:55:43,022 --> 00:55:45,103
Mom, are you listening to me?

211
00:55:48,025 --> 00:55:50,106
I am listening to, Marie-France.

212
00:55:55,028 --> 00:55:57,109
Now I have only one thing
left to do:

213
00:55:58,029 --> 00:56:00,110
Nothing.

214
00:56:03,032 --> 00:56:07,994
I don't want anymore friends,
belongings, love. Those are all traps.

215
00:56:11,035 --> 00:56:13,997
Do you have money, my child?
To get by?

216
00:56:14,037 --> 00:56:16,998
- Yes I have enough.
- That's important.

217
00:56:17,038 --> 00:56:19,119
You can't give up everything.

218
00:56:23,041 --> 00:56:28,003
- Mom?
- Yes?

219
00:56:28,043 --> 00:56:32,005
Was I afraid of mice
during my child days?

220
00:56:32,045 --> 00:56:34,126
You weren't.

221
00:56:35,046 --> 00:56:37,127
Julie was afraid.

222
00:56:38,048 --> 00:56:40,128
I'm afraid now.

223
00:56:45,051 --> 00:56:47,132
Thank you.

224
00:58:26,097 --> 00:58:30,059
- Nice to see you. Come in.
- Can I ask you something?

225
00:58:30,099 --> 00:58:35,061
- My wife is out. Please come in.
- Can you lend me your cat?

226
00:58:36,101 --> 00:58:41,064
- Excuse me?
- Cat. For a few days.

227
00:58:42,104 --> 00:58:47,066
It is not castrated.
It can be aggressive.

228
00:58:56,110 --> 00:58:58,191
I am not sure it likes you.

229
00:59:40,130 --> 00:59:45,093
- What are you doing here?
- I saw you from the bus.

230
00:59:45,133 --> 00:59:47,214
You were running like crazy.
Are you crying?

231
00:59:58,139 --> 01:00:00,100
It is the water.

232
01:00:00,140 --> 01:00:05,102
- You don't wear underwear?
- Never.

233
01:00:11,145 --> 01:00:16,107
I borrowed the cat from the neighbor,
to kill the mice. It had babies.

234
01:00:18,148 --> 01:00:20,229
It's normal, Julie.
Are you afraid to go back?

235
01:00:25,151 --> 01:00:27,232
I'll go clean up.

236
01:00:36,156 --> 01:00:38,237
I'll see you at my place.

237
01:01:05,169 --> 01:01:10,131
It's Lucille. Take a taxi here.
I will pay you back.

238
01:01:11,172 --> 01:01:16,134
- Now? It is late.
- 23.30. You have 25 minutes. It's important.

239
01:01:18,175 --> 01:01:19,136
I can't.

240
01:01:19,176 --> 01:01:22,137
I beg you. I never asked you
for anything. Please come.

241
01:01:22,177 --> 01:01:24,258
Where?

242
01:01:25,178 --> 01:01:28,140
Cita de Midi 3. Near Pigalle.
First door on the left. There's a buzzer.

243
01:01:28,180 --> 01:01:30,261
Say it's for me. You'll come?

244
01:01:49,189 --> 01:01:51,270
I am here for Lucille.

245
01:02:30,208 --> 01:02:32,289
You came.

246
01:02:37,211 --> 01:02:39,292
Forgive me.

247
01:02:42,213 --> 01:02:44,294
I'm sorry.

248
01:02:51,217 --> 01:02:56,180
- Are you angry?
- No.

249
01:02:58,221 --> 01:03:03,183
We are on in 5 minutes.
Get me ready.

250
01:03:05,224 --> 01:03:08,185
After I changed,
I came here for a drink.

251
01:03:08,225 --> 01:03:10,306
I looked around the audience,
for no reason.

252
01:03:13,227 --> 01:03:18,190
In the middle... of the first
row, I saw my father.

253
01:03:22,232 --> 01:03:24,313
He seemed tired.

254
01:03:27,234 --> 01:03:29,195
And sleepy.

255
01:03:29,235 --> 01:03:31,316
But he was starring a girl's ass.

256
01:03:33,237 --> 01:03:37,198
The shithead who let you in
wouldn't make him leave.

257
01:03:37,238 --> 01:03:42,201
He told me, that if he pays,
he has a right to watch.

258
01:03:43,241 --> 01:03:45,202
I didn't know who I could count on.

259
01:03:45,242 --> 01:03:47,323
I was desperate.
I didn't know who to talk to.

260
01:03:51,245 --> 01:03:55,207
- That's why I phoned you.
- And your father?

261
01:03:55,247 --> 01:04:00,209
10 minutes ago, he looked at
his watch and left.

262
01:04:02,250 --> 01:04:07,212
At 23.45 is the last train
to Montpellier.

263
01:04:07,252 --> 01:04:09,333
Why do you do this?

264
01:04:11,254 --> 01:04:13,335
Because I like to.

265
01:04:14,255 --> 01:04:16,336
I think everybody likes to.

266
01:04:22,259 --> 01:04:25,220
- You saved my life.
- I didn't do anything.

267
01:04:25,260 --> 01:04:30,223
- I asked you to come and you did.
- No.

268
01:04:33,264 --> 01:04:35,345
Julie...

269
01:04:36,265 --> 01:04:38,346
Isn't that you?

270
01:04:41,268 --> 01:04:43,228
Lucille, we are on.

271
01:04:43,269 --> 01:04:45,349
It's me.

272
01:04:46,270 --> 01:04:48,351
European Council proposes you

273
01:04:49,271 --> 01:04:54,233
to finish the composition of
Patrice De Courcy.

274
01:04:55,274 --> 01:04:57,355
I agreed. I am working on it now.

275
01:04:58,275 --> 01:05:03,238
I'm trying to understand
Patrice. It's not easy.

276
01:05:03,278 --> 01:05:05,359
Can you tell something

277
01:05:06,279 --> 01:05:11,241
about this score, shown here
for the very first time?

278
01:05:12,282 --> 01:05:15,243
It's the first part of the concert
Patrice composed,

279
01:05:15,283 --> 01:05:17,364
commissioned by
the European Council.

280
01:05:19,285 --> 01:05:24,247
The concert was to be played
just once

281
01:05:24,287 --> 01:05:29,249
by twelve symphonic orchestras
in the 12 EU cities.

282
01:05:29,289 --> 01:05:31,370
Patrice was a secretive man.

283
01:05:34,292 --> 01:05:39,254
Only his wife, Julie, would
understand him.

284
01:05:40,294 --> 01:05:43,256
I tried asking her to participate
in this program, but

285
01:05:43,296 --> 01:05:45,377
she refused.

286
01:05:46,297 --> 01:05:51,259
I assume that these documents
are from your archives.

287
01:05:51,299 --> 01:05:53,380
No, they are not archives.
Who can predict?

288
01:05:55,301 --> 01:05:59,263
They are photos and documents which
I found in his desk at the Conservatory.

289
01:05:59,303 --> 01:06:01,384
His wife didn't want them.

290
01:06:02,304 --> 01:06:07,267
I am not sure
I should show them.

291
01:06:07,307 --> 01:06:12,269
He was a great man. One of the
most important composers of our time.

292
01:06:13,310 --> 01:06:18,272
People like him belong to everybody.

293
01:06:51,327 --> 01:06:53,408
I'll be five minutes.

294
01:06:58,330 --> 01:07:03,292
Where did I put it?
A green calling card.

295
01:07:04,333 --> 01:07:09,295
- Did you watch TV today?
- No.

296
01:07:10,335 --> 01:07:14,297
I found it. Her number at home
and at work.

297
01:07:14,337 --> 01:07:16,418
Why do you need this?

298
01:07:18,339 --> 01:07:23,301
Today, on TV, she showed the
scores I took from you.

299
01:07:29,344 --> 01:07:33,306
After the accident, when
nothing was sure, I made a copy.

300
01:07:33,346 --> 01:07:38,308
When you picked it up,
I knew you'd destroy it.

301
01:07:38,348 --> 01:07:43,310
- I kept the copy and sent it to Strasbourg.
- Why did you do that?

302
01:07:44,351 --> 01:07:49,313
The music is so beautiful.
You can't destroy things like that.

303
01:08:10,363 --> 01:08:12,444
Olivier!

304
01:08:30,372 --> 01:08:32,333
I'm sorry.

305
01:08:32,373 --> 01:08:34,454
It's nothing.

306
01:08:36,375 --> 01:08:40,336
- I heard you're finishing Patrice's concert.
- I thought I could try...

307
01:08:40,376 --> 01:08:44,338
You can't.
You have no right. It won't be the same.

308
01:08:44,378 --> 01:08:49,341
I said I'd try.
I don't know if I'll finish.

309
01:08:49,381 --> 01:08:54,343
I'll tell you why. It was a way...
that's what I thought, to make you cry, run.

310
01:08:57,384 --> 01:08:59,465
The only way of making you say,
"I want" or "I don't want".

311
01:09:02,387 --> 01:09:07,349
- It's not fair.
- You didn't leave me any choice.

312
01:09:13,392 --> 01:09:18,354
- You have no right to.
- You want to see what I've done?

313
01:09:18,394 --> 01:09:20,475
I'm not sure I understood.

314
01:09:23,396 --> 01:09:25,477
If I could play for you...

315
01:09:55,411 --> 01:10:00,373
Do you know what the chorus sings?
I thought Patrice had told you.

316
01:10:01,413 --> 01:10:03,494
In Greek, the rhythm is different.

317
01:10:25,424 --> 01:10:30,387
- Who was that girl?
- What girl?

318
01:10:31,427 --> 01:10:33,508
In the photos on the program.
She was with Patrice.

319
01:10:37,430 --> 01:10:39,511
Didn't you know?

320
01:10:41,432 --> 01:10:43,393
Just tell me, were they together?

321
01:10:43,433 --> 01:10:45,513
Yes.

322
01:10:46,434 --> 01:10:48,515
Since when?

323
01:10:49,435 --> 01:10:51,516
Several years.

324
01:10:57,439 --> 01:11:00,400
- Where does she live?
- Near Montparnasse.

325
01:11:00,940 --> 01:11:05,403
They often met at the courts.

326
01:11:06,443 --> 01:11:08,524
She is a lawyer.
Or she works for one.

327
01:11:10,445 --> 01:11:12,526
What do you want to do?

328
01:11:27,453 --> 01:11:29,534
Meet her.

329
01:11:59,467 --> 01:12:01,548
Excuse me.

330
01:12:38,500 --> 01:12:40,500
What about equality?

331
01:12:40,600 --> 01:12:43,500
Is it because I don't speak French
that the court won't hear my case?

332
01:13:45,515 --> 01:13:47,596
Excuse me.

333
01:13:55,520 --> 01:13:57,601
Yes?

334
01:13:58,521 --> 01:14:03,484
- You were my husband's mistress?
- Yes.

335
01:14:04,524 --> 01:14:06,605
I didn't know. I just found out.

336
01:14:11,527 --> 01:14:15,489
It's a shame.
Now you'll hate him, and me too.

337
01:14:15,529 --> 01:14:17,610
I don't know...

338
01:14:19,531 --> 01:14:21,612
Of course you will.

339
01:14:28,535 --> 01:14:30,616
It is his child?

340
01:14:33,537 --> 01:14:35,618
Yes.

341
01:14:37,539 --> 01:14:39,620
But he didn't know.

342
01:14:40,541 --> 01:14:42,621
I found out after the accident.

343
01:14:45,543 --> 01:14:50,505
I didn't want a child but it happened.
Now I want to keep it.

344
01:14:58,549 --> 01:15:00,630
Do you have a cigarette?

345
01:15:13,556 --> 01:15:15,636
You shouldn't.

346
01:15:22,560 --> 01:15:27,522
Do you want to know
when and where?

347
01:15:27,562 --> 01:15:29,643
No.

348
01:15:30,563 --> 01:15:35,526
- You want to know if he loved me?
- Yes, that was my question.

349
01:15:37,566 --> 01:15:39,647
Why ask, I know he loved you.

350
01:15:42,569 --> 01:15:44,530
Yes.

351
01:15:44,570 --> 01:15:46,651
He loved me.

352
01:15:47,571 --> 01:15:49,652
Julie...

353
01:15:51,573 --> 01:15:53,654
Will you hate me now?

354
01:17:30,618 --> 01:17:32,579
Come in.

355
01:17:32,619 --> 01:17:34,700
What happened?

356
01:17:37,621 --> 01:17:39,702
You met her?

357
01:17:53,628 --> 01:17:57,590
Once you asked me
to take Patrice's papers.

358
01:17:57,630 --> 01:18:01,592
- You didn't want to.
- No, but if I had...

359
01:18:01,632 --> 01:18:06,594
- The photos were among them?
- Yes.

360
01:18:07,635 --> 01:18:11,597
If I had taken them,
I'd have known.

361
01:18:11,637 --> 01:18:14,598
And if I had burned them,
I'd never have known.

362
01:18:14,638 --> 01:18:16,719
That's right.

363
01:18:18,640 --> 01:18:20,721
Maybe it's better this way.

364
01:18:22,642 --> 01:18:26,603
Can you show me
what you've composed?

365
01:18:26,643 --> 01:18:31,606
- These are violins?
- The altos.

366
01:18:37,648 --> 01:18:39,729
And now...

367
01:18:49,654 --> 01:18:54,616
Wait a second. Lighter,
without the percussions.

368
01:18:58,658 --> 01:19:00,739
Lets remove the trumpets.

369
01:19:15,666 --> 01:19:19,628
- Instead of a piano...
- A Flute?

370
01:19:19,668 --> 01:19:21,749
Flute... Start back here.

371
01:19:48,681 --> 01:19:50,762
That's as far as I got.

372
01:19:52,683 --> 01:19:57,645
- And the finale?
- I don't know.

373
01:19:57,685 --> 01:19:59,766
There was a paper.

374
01:20:01,687 --> 01:20:06,649
The counterpoint was supposed
to come back.

375
01:20:11,691 --> 01:20:16,654
He told me, it is memento.
Try weaving it back in.

376
01:20:17,694 --> 01:20:19,775
Van den Budenmayer?

377
01:20:21,696 --> 01:20:26,658
He wanted to allude to him at the
end. You know how much he loved him.

378
01:20:28,299 --> 01:20:31,660
- Are you still in touch with our lawyer?
- Sometimes.

379
01:20:31,700 --> 01:20:34,662
- Did he sell the house yet?
- I don't know.

380
01:20:34,702 --> 01:20:39,664
- I doubt it. He'd have called.
- Tell him not to.

381
01:20:41,705 --> 01:20:45,667
If you can handle all this,
will you show me?

382
01:20:45,707 --> 01:20:47,788
Yes.

383
01:20:52,710 --> 01:20:54,791
There was a mattress inside.

384
01:20:55,711 --> 01:20:57,672
Mr. Olivier bought it.

385
01:20:57,712 --> 01:21:02,675
- I didn't think you would need it.
- That's fine.

386
01:21:07,717 --> 01:21:09,798
Good morning.

387
01:21:11,719 --> 01:21:13,680
Have you been here before?

388
01:21:13,720 --> 01:21:15,801
No. Never.

389
01:21:18,722 --> 01:21:23,684
Upstairs are the bedrooms and the
office. I'll show you them later.

390
01:21:24,725 --> 01:21:29,687
This is the Kitchen.
It's always been like this.

391
01:21:32,728 --> 01:21:37,690
- Is it a boy or a girl?
- Boy.

392
01:21:38,731 --> 01:21:40,812
Have you chosen a name?

393
01:21:41,732 --> 01:21:43,813
Yes.

394
01:21:45,734 --> 01:21:50,696
I thought... he should have
his name and his house.

395
01:21:53,738 --> 01:21:58,700
- Do you still need me?
- No, you can go home. Thank you.

396
01:22:00,741 --> 01:22:04,703
- I knew it.
- What?

397
01:22:04,743 --> 01:22:09,705
- Patrice told me a lot about you.
- Really? Like what?

398
01:22:13,747 --> 01:22:15,828
That you are good.

399
01:22:16,748 --> 01:22:18,829
That's what you want to be.

400
01:22:19,750 --> 01:22:24,712
People can always count on you.
Even me.

401
01:22:31,755 --> 01:22:33,836
I'm sorry.

402
01:23:22,778 --> 01:23:24,739
It's me. I finished.

403
01:23:24,779 --> 01:23:28,741
Come pick it up tomorrow.
Or now, if you're not too tired.

404
01:23:28,781 --> 01:23:32,743
I'm not tired. But I will not
come for the score.

405
01:23:32,783 --> 01:23:34,744
What?

406
01:23:34,784 --> 01:23:39,746
I won't pick it up. I've been thinking it
for a week. This music can be mine.

407
01:23:39,786 --> 01:23:43,748
A little heavy and awkward,
but mine.

408
01:23:43,788 --> 01:23:48,750
Or yours, but everyone
would have to know.

409
01:23:51,792 --> 01:23:53,752
Are you there?

410
01:23:53,792 --> 01:23:55,873
Yes.

411
01:23:57,794 --> 01:23:59,875
You right.

412
01:24:31,810 --> 01:24:33,771
Its me. I wanted to ask you.

413
01:24:33,811 --> 01:24:38,773
You really sleep on the mattress?

414
01:24:38,813 --> 01:24:40,894
Yes.

415
01:24:42,815 --> 01:24:47,777
- You never told me.
- No I didn't.

416
01:24:54,820 --> 01:24:56,901
Do you still love me?

417
01:24:58,822 --> 01:25:00,903
I love you.

418
01:25:03,824 --> 01:25:05,905
Are you alone?

419
01:25:06,826 --> 01:25:08,907
Of course I'm alone.

420
01:25:15,830 --> 01:25:17,911
I am coming.

421
01:25:32,838 --> 01:25:34,919
If I can talk like angel,

422
01:25:38,840 --> 01:25:40,921
And I have no love,

423
01:25:42,842 --> 01:25:44,923
I will be a copper ringing.

424
01:25:46,844 --> 01:25:48,925
If I can prophesy

425
01:25:52,847 --> 01:25:54,928
And if I have no love

426
01:25:58,849 --> 01:26:00,930
I will be nothing.

427
01:26:36,867 --> 01:26:38,948
Love is patient.

428
01:26:42,869 --> 01:26:44,950
Love is kind.

429
01:26:46,871 --> 01:26:48,952
It bears all things.

430
01:26:51,874 --> 01:26:53,955
And knows everything.

431
01:26:57,876 --> 01:26:59,957
Love never ends.

432
01:27:04,879 --> 01:27:06,960
Prophecy - gone.

433
01:27:13,884 --> 01:27:15,965
If languages - silent.

434
01:27:16,885 --> 01:27:18,966
Knowledge - nothing.

435
01:27:22,888 --> 01:27:24,969
Prophecy - gone.

436
01:27:27,890 --> 01:27:29,971
Languages - silent.

437
01:27:33,893 --> 01:27:35,974
And only stays

438
01:27:38,895 --> 01:27:40,976
Faith, hope and love

439
01:27:43,897 --> 01:27:45,978
But the biggest one from this three

440
01:27:49,900 --> 01:27:51,981
is love.

