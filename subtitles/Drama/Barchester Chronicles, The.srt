1
00:00:03,951 --> 00:00:05,976
Some men do.

2
00:00:07,387 --> 00:00:09,912
<i>Just being so far from the armada,</i>

3
00:00:09,990 --> 00:00:15,860
the mind can start to fill
with strange thoughts, doubts.

4
00:00:15,929 --> 00:00:18,864
Don't you ever have doubts, Vaako?

5
00:00:22,302 --> 00:00:24,793
- Doubts?
- About the campaign.

6
00:00:25,806 --> 00:00:27,967
About Lord Marshal.

7
00:00:29,977 --> 00:00:34,346
First and always,
I am a Necromonger commander.

8
00:00:35,415 --> 00:00:40,512
So if you're here to test my loyalty,
you succeed only in testing my patience.

9
00:00:40,587 --> 00:00:44,182
Oh, no.
No, that's not why I'm here at all.

10
00:01:05,612 --> 00:01:08,308
<i>[ Dame Vaako ]</i>
<i>I'm so glad I could</i>
<i>steal you away for a moment.</i>

11
00:01:08,382 --> 00:01:10,782
Doesn't it strike you odd?

12
00:01:10,851 --> 00:01:13,479
<i>Here we have</i>
<i>the current Lord Marshal...</i>

13
00:01:13,554 --> 00:01:17,650
<i>destroying entire societies,</i>
<i>and yet he can't bring himself...</i>

14
00:01:17,724 --> 00:01:21,683
to kill one
stranded Elemental.

15
00:01:21,762 --> 00:01:23,696
Why is that?

16
00:01:24,898 --> 00:01:26,832
You don't pray to our god.

17
00:01:26,900 --> 00:01:29,460
You pray to no god, I hear.

18
00:01:29,536 --> 00:01:32,528
Elementals-- we calculate.

19
00:01:33,607 --> 00:01:35,541
Don't we all.

20
00:01:35,609 --> 00:01:39,511
But now let's have first things first.

21
00:01:39,580 --> 00:01:42,811
What of Riddick?
In truth, I don't know
where he went.

22
00:01:42,883 --> 00:01:45,818
In truth, I'm more interested
in where he came from.

23
00:01:47,955 --> 00:01:50,185
Watch your step.

24
00:01:50,257 --> 00:01:52,953
I've always wondered,

25
00:01:53,026 --> 00:01:55,620
does an air Elemental fly?

26
00:01:55,696 --> 00:01:57,687
<i>Now do me a favor.</i>

27
00:01:57,764 --> 00:02:01,723
Calculate the odds of you
getting off this planet alive...

28
00:02:01,802 --> 00:02:03,793
and now cut them in half.

29
00:02:05,239 --> 00:02:07,173
No, we can't fly.

30
00:02:07,241 --> 00:02:09,709
But we glide very well.

31
00:02:09,776 --> 00:02:12,574
<i>Save your threats, Necromonger.</i>

32
00:02:13,647 --> 00:02:17,208
I would have told you about Riddick
for the asking.

33
00:02:17,284 --> 00:02:21,084
It concerns a foretelling,
a prediction now more than 30 years old.

34
00:02:21,154 --> 00:02:25,454
<i>Ayoung warrior</i>
<i>once consulted a seer.</i>

35
00:02:25,525 --> 00:02:30,588
<i>He was told a child would be born</i>
<i>on the planet Furya--a male child--</i>

36
00:02:30,664 --> 00:02:35,067
<i>who would someday</i>
<i>cause the warrior's downfall.</i>

37
00:02:35,135 --> 00:02:38,730
<i>[ Female Voice ]</i>
<i>Cause his untimed death.</i>

38
00:02:39,740 --> 00:02:44,700
Furya? Furya's a ruined world.
No life to speak of.

39
00:02:44,778 --> 00:02:46,336
For good reason.

40
00:02:46,413 --> 00:02:51,407
This young warrior
mounted an attack on Furya,

41
00:02:51,485 --> 00:02:54,579
killing all young males
he could find,

42
00:02:54,655 --> 00:02:58,751
even strangling some
with their birth cords.

43
00:02:58,825 --> 00:03:01,623
An artful stroke,
wouldn't you say?

44
00:03:03,530 --> 00:03:07,557
So this warrior, the one
who tried to outwit the prediction,
would later become--

45
00:03:07,634 --> 00:03:09,659
That's why he worries.

46
00:03:09,736 --> 00:03:11,727
The Lord Marshal.

47
00:03:13,340 --> 00:03:15,331
Which would make
the man-child--

48
00:03:15,409 --> 00:03:17,877
He worries he missed
killing that child in its crib.

49
00:03:17,944 --> 00:03:19,935
Riddick.

50
00:03:20,013 --> 00:03:22,914
<i>[ Baby Crying ]</i>

51
00:03:36,963 --> 00:03:39,124
<i>That's why it's so vital to him.</i>
Wait.

52
00:03:39,199 --> 00:03:41,599
<i>- It's about a prophecy.</i>
- Wait! Wait!

53
00:03:44,638 --> 00:03:46,629
Are you there?

54
00:03:50,177 --> 00:03:52,577
You do what your lord asks.

55
00:03:52,646 --> 00:03:56,309
You cleanse Riddick for him,
and in doing so...

56
00:03:56,383 --> 00:03:59,784
you prove your undying loyalty.

57
00:03:59,853 --> 00:04:05,485
And perhaps then
he'll finally let down his guard.

58
00:04:48,268 --> 00:04:49,792
<i>[ Man ]</i>
<i>Still here, I see?</i>

59
00:04:49,870 --> 00:04:53,601
I've been here 1 8 years.
See this?

60
00:04:55,675 --> 00:05:01,511
I remember how gorgeous she was.
Well, gorgeous in a certain light.

61
00:05:01,581 --> 00:05:04,482
And now,
for the goddamn death of me,

62
00:05:04,551 --> 00:05:06,678
I cannot remember her name.

63
00:05:06,887 --> 00:05:09,321
[ Man ]
Feeding time!

64
00:05:11,491 --> 00:05:13,618
<i>[ Loud Snarling ]</i>

65
00:05:13,693 --> 00:05:17,686
We're here for the rest
of our unnatural lives.

66
00:05:21,568 --> 00:05:24,002
<i>[ Snarling Continues ]</i>

67
00:05:25,705 --> 00:05:27,400
Here they come!

68
00:05:30,644 --> 00:05:32,635
[ Snarling Continues ]

69
00:05:32,712 --> 00:05:36,580
Whatever you do,
don't make eye contact.

70
00:05:55,836 --> 00:05:58,805
[ Clamoring ]

71
00:06:08,515 --> 00:06:11,780
[ Snarling ]

72
00:06:24,297 --> 00:06:27,232
[ Men Shouting ]

73
00:06:35,742 --> 00:06:37,232
Aaah!

74
00:06:38,879 --> 00:06:41,541
- [ Gasping ]
- [ Growls ]

75
00:06:41,615 --> 00:06:44,243
<i>[ Beasts Chomping ]</i>

76
00:06:49,022 --> 00:06:51,252
[ Snarling ]

77
00:07:40,640 --> 00:07:42,665
[ Purring ]

78
00:07:47,547 --> 00:07:49,572
<i>[ Alarm Buzzing ]</i>

79
00:08:02,329 --> 00:08:04,320
It's an animal thing.

80
00:08:04,531 --> 00:08:07,898
<i>[ Footsteps ]</i>

81
00:08:15,241 --> 00:08:18,836
<i>Check her for me.</i>
<i>She's always got a blade somewhere.</i>

82
00:08:45,572 --> 00:08:48,871
- Hyah!
- [ Groans ]

83
00:09:00,020 --> 00:09:02,181
[ Grunting ]

84
00:09:04,658 --> 00:09:06,523
- [ Grunting ]
- [ Yells ]

85
00:09:08,361 --> 00:09:10,886
[ Grunts ]

86
00:09:12,932 --> 00:09:15,298
[ Growls ]

87
00:09:18,605 --> 00:09:21,597
<i>I don't think</i>
<i>she likes being touched.</i>

88
00:09:23,777 --> 00:09:28,077
I'd take my wounded and go
while you still can.

89
00:09:28,148 --> 00:09:29,775
[ Gasps ]

90
00:09:33,553 --> 00:09:37,614
Is there a name for this
private little world of yours, huh?

91
00:09:37,691 --> 00:09:42,492
What happens there
when we don't just run away?

92
00:09:42,562 --> 00:09:45,030
<i>You'll kill us... with a soup cup?</i>

93
00:09:45,098 --> 00:09:47,430
[ Laughing ]
Tea, actually.

94
00:09:47,500 --> 00:09:49,434
What's that?

95
00:09:52,005 --> 00:09:55,497
I'll kill you with my teacup.

96
00:10:07,253 --> 00:10:10,586
You know the rule. They aren't dead
if they're still on the books.

97
00:10:10,657 --> 00:10:13,820
Mmm.

98
00:10:16,429 --> 00:10:19,193
[ Grunting ]

99
00:10:23,069 --> 00:10:25,401
[ Gasps ]

100
00:10:31,277 --> 00:10:32,767
Come on.

101
00:10:55,635 --> 00:10:57,728
Death by teacup.

102
00:11:01,908 --> 00:11:04,342
Damn. Why didn't I think of that?

103
00:11:04,410 --> 00:11:07,243
<i>I didn't come to play</i>
<i>"Who's the Better Killer?"</i>

104
00:11:07,313 --> 00:11:10,714
But it's my favorite game.
Haven't you heard?

105
00:11:10,784 --> 00:11:13,218
I heard you came lookin' for me.
<i>Is that all?</i>

106
00:11:13,286 --> 00:11:15,948
Then you missed the good part.

107
00:11:16,022 --> 00:11:18,286
Hooked up with some mercs
outta Lupus Five.

108
00:11:18,358 --> 00:11:21,725
<i>Said they'd take me on,</i>
<i>teach me the trade, give me a good cut.</i>

109
00:11:23,029 --> 00:11:24,656
They slaved me out, Riddick.

110
00:11:24,731 --> 00:11:27,859
<i>Do you know what that could do to you</i>
<i>when you're that age?</i>

111
00:11:27,934 --> 00:11:31,529
When you're 1 2 years old?
I told you to stay in New Mecca.

112
00:11:31,604 --> 00:11:34,129
[ Shouts ]
Did you not listen?

113
00:11:34,207 --> 00:11:36,402
I had mercs on my neck.

114
00:11:36,476 --> 00:11:38,910
I'll always have mercs on my neck.

115
00:11:38,978 --> 00:11:42,641
I spent five years on a frozen heap
just to keep 'em away from you.

116
00:11:43,650 --> 00:11:46,585
And you go and sign up
with the same fake badges...

117
00:11:46,653 --> 00:11:48,644
that wanted to cut you up
and use you for bait.

118
00:11:48,721 --> 00:11:53,090
What are you pitching, Riddick?
That you cuttin' out was a good thing?

119
00:11:53,159 --> 00:11:56,595
That you had my ass covered
from halfway across the universe?

120
00:11:56,663 --> 00:11:59,257
You signed with mercs.

121
00:12:03,002 --> 00:12:05,835
There was nobody else around.

122
00:12:09,943 --> 00:12:11,877
<i>[ Door Slams ]</i>

123
00:12:19,786 --> 00:12:22,914
Let's pop the cork.
Get some fresh air.

124
00:12:29,095 --> 00:12:32,360
[ Whirring ]

125
00:12:51,017 --> 00:12:55,579
[ Hissing ]

126
00:13:00,026 --> 00:13:02,119
So they do go topside...

127
00:13:03,529 --> 00:13:06,293
to swap out air.

128
00:13:06,366 --> 00:13:08,800
Interesting.

129
00:13:08,868 --> 00:13:10,597
Who the hell are you?

130
00:13:12,538 --> 00:13:15,939
When it happens, it'll happen fast.

131
00:13:16,009 --> 00:13:19,445
<i>Stay on my leg when I cut fence</i>
<i>or stay here...</i>

132
00:13:19,512 --> 00:13:21,503
for the rest of your natural life.

133
00:13:21,581 --> 00:13:25,449
Nobody outs this place.
Nobody.

134
00:13:31,491 --> 00:13:33,823
<i>[ Kyra ]</i>
<i>He ain't "nobody."</i>

135
00:13:47,874 --> 00:13:52,243
<i>Good news first.</i>
<i>Talked things over with my amigos here.</i>

136
00:13:52,312 --> 00:13:55,543
We'll cut you in for 77 5K.

137
00:13:57,984 --> 00:14:00,043
Well, all right.

138
00:14:00,119 --> 00:14:02,485
What's the bad news?
They close the local whorehouse?

139
00:14:02,555 --> 00:14:05,080
[ Cackles ]

140
00:14:05,158 --> 00:14:08,924
<i>No. The bad news is worse than that.</i>
<i>Much worse.</i>

141
00:14:08,995 --> 00:14:14,023
Our pilot, he saw this.
It crossed a shipping lane.

142
00:14:17,570 --> 00:14:21,062
<i>Any idea what this might be?</i>

143
00:14:24,677 --> 00:14:27,669
Never saw nothin' like it.

144
00:14:27,747 --> 00:14:31,444
<i>This ship charts back</i>
<i>to Helion Prime.</i>

145
00:14:31,517 --> 00:14:35,044
You know, Anatoli's
got a nose for trouble,

146
00:14:35,121 --> 00:14:39,182
<i>and he thinks</i>
<i>trouble follows you here.</i>

147
00:14:41,294 --> 00:14:45,628
<i>Look. We dusted our tracks</i>
<i>and got the hell outta there.</i>

148
00:14:45,698 --> 00:14:48,690
There is no way
we didn't lose them.

149
00:14:49,836 --> 00:14:52,737
<i>"</i>Them"?

150
00:14:58,177 --> 00:15:01,408
This is my prisoner<i>. Mine.</i>

151
00:15:01,481 --> 00:15:03,472
Nobody else's.

152
00:15:03,549 --> 00:15:05,881
And I want my money now.

153
00:15:05,952 --> 00:15:08,045
<i>So...</i>

154
00:15:08,121 --> 00:15:10,555
you stole a prisoner...

155
00:15:10,623 --> 00:15:12,614
from <i>"</i>them"?

156
00:15:18,131 --> 00:15:20,099
[ Yelling ]

157
00:15:22,835 --> 00:15:25,065
<i>[ Gunfire ]</i>

158
00:15:32,879 --> 00:15:34,642
<i>[ Gunfire Continues ]</i>

159
00:16:10,049 --> 00:16:12,347
<i>[ Gunfire Stops ]</i>

160
00:16:19,859 --> 00:16:22,054
[ Growls ]
No.

161
00:16:22,128 --> 00:16:25,222
<i>Riddick. No.!</i>

162
00:16:30,436 --> 00:16:34,429
[ Grunts ]
Shoulda taken the money, Toombs.

163
00:16:37,643 --> 00:16:38,610
[ Grunts ]

164
00:16:43,483 --> 00:16:46,475
[ Electricity Crackling ]

165
00:17:13,312 --> 00:17:17,749
<i>- [ Throws Switch ]</i>
<i>-[ Gate Buzzer Sounds ]</i>

166
00:17:23,356 --> 00:17:25,620
[ Gasping ]

167
00:17:30,396 --> 00:17:32,921
You gonna kill me?

168
00:17:36,502 --> 00:17:39,164
[ Raspy Breathing ]

169
00:17:41,908 --> 00:17:43,899
[ Whirs ]

170
00:18:06,265 --> 00:18:08,460
<i>[ Man Groans ]</i>
Mercs.

171
00:18:09,936 --> 00:18:12,370
Some guards here,
but this can't be all of them.

172
00:18:13,606 --> 00:18:16,905
<i>Check their slots in the back--</i>
<i>and be careful.</i>

173
00:18:16,976 --> 00:18:18,967
<i>[ Riddick ]</i>
<i>Don't bother.</i>

174
00:18:19,045 --> 00:18:21,445
Guards ain't there.

175
00:18:21,514 --> 00:18:24,244
<i>They figured out the Necros</i>
<i>are comin'for me.</i>

176
00:18:24,317 --> 00:18:27,480
Plan was to clean the bank,
ghost the mercs,

177
00:18:27,553 --> 00:18:29,487
break wide through the tunnel.

178
00:18:29,555 --> 00:18:33,286
And then somebody got a lucky shot off
with this rocket launcher here...

179
00:18:33,359 --> 00:18:35,350
and took out the sled.

180
00:18:35,428 --> 00:18:39,558
Guards took off on foot
but rigged the door
so no one could follow.

181
00:18:40,633 --> 00:18:45,229
<i>They'll take the one ship in the hangar</i>
<i>and leave everyone else here to die.</i>

182
00:18:45,304 --> 00:18:48,671
How come you know all this shit?

183
00:18:48,741 --> 00:18:50,766
You weren't even here.

184
00:18:51,777 --> 00:18:53,904
'Cause it was my plan.

185
00:18:59,619 --> 00:19:02,179
I shoulda taken the money.

186
00:19:02,255 --> 00:19:05,588
[ Sighs ]
<i>[ Snarling ]</i>

187
00:19:05,658 --> 00:19:07,922
[ Snarling ]

188
00:19:08,928 --> 00:19:11,158
Riddick!

189
00:19:11,364 --> 00:19:12,490
<i>[ Yelling Echoes ]</i>

190
00:19:12,565 --> 00:19:15,033
<i>[ Whirring ]</i>

191
00:19:21,741 --> 00:19:24,869
<i>[ Man ] What is this?</i>
<i>What is he thinking?</i>

192
00:19:24,944 --> 00:19:27,913
<i>- [ Man #2 ] Once that sun comes up--</i>
- We'll last five minutes.

193
00:19:27,980 --> 00:19:31,939
Five minutes. We ain't gonna last
30 seconds out there.

194
00:19:32,018 --> 00:19:34,009
It'll light you up like a match.

195
00:19:38,591 --> 00:19:42,755
20-mile buffer zone
and 30 klicks to the hangar.

196
00:19:42,828 --> 00:19:44,853
<i>[ Murmuring ]</i>

197
00:19:44,930 --> 00:19:48,195
30 klicks over that terrain?

198
00:19:53,839 --> 00:19:57,070
<i>[ Murmuring Continues ]</i>

199
00:20:00,713 --> 00:20:04,877
It's moving in the right direction.
We could make it.

200
00:20:05,251 --> 00:20:07,947
Stay behind the night,

201
00:20:08,020 --> 00:20:10,318
ahead of the sun.

202
00:20:10,389 --> 00:20:12,721
There's gonna be one speed--

203
00:20:14,827 --> 00:20:16,818
mine.

204
00:20:19,098 --> 00:20:21,259
<i>If you can't keep up, don't step up.</i>

205
00:20:21,334 --> 00:20:23,268
You'll just die.

206
00:21:27,400 --> 00:21:29,163
<i>[ Kyra ]</i>
<i>Riddick?</i>

207
00:21:31,237 --> 00:21:33,228
[ Shouts ]
Riddick?

208
00:21:36,609 --> 00:21:38,600
Riddick?

209
00:21:43,115 --> 00:21:46,642
Go, go! Go on! Go! Climb here!

210
00:21:46,719 --> 00:21:49,381
Climb! Go! Go, go, go!

211
00:22:15,080 --> 00:22:16,911
Boss!

212
00:22:20,753 --> 00:22:22,721
<i>Up there.</i>

213
00:22:22,788 --> 00:22:24,983
- Take a look.
- Because Anatoli says so?

214
00:22:25,057 --> 00:22:28,151
Because his nose says so.

215
00:22:29,161 --> 00:22:31,595
[ Hisses ]

216
00:22:45,444 --> 00:22:48,004
There's nothin' up here.

217
00:22:50,082 --> 00:22:51,515
Oh, shit.

218
00:23:01,293 --> 00:23:03,853
Riddick.

219
00:23:03,929 --> 00:23:07,558
<i>They're heading</i>
<i>right for the volcano fields.</i>
<i>They're going for our ship.!</i>

220
00:23:07,633 --> 00:23:10,227
No chance do they
get to the hangar first.
No chance!

221
00:23:22,014 --> 00:23:24,209
Next one! Next hole!
We'll catch 'em there!

222
00:23:52,144 --> 00:23:54,442
<i>No more run for you.</i>

223
00:23:59,652 --> 00:24:03,088
Where did the big guy go?

224
00:24:08,827 --> 00:24:10,795
- Ohh--
- [ Groans ]

225
00:24:12,331 --> 00:24:14,765
[ Bullets Ricochet ]

226
00:24:35,854 --> 00:24:37,583
Close it!

227
00:24:39,525 --> 00:24:42,494
[ Bullets Ricochet ]

228
00:24:45,864 --> 00:24:47,832
<i>[ Trigger Clicking ]</i>

229
00:24:47,900 --> 00:24:50,334
[ Cries Out ]

230
00:25:09,188 --> 00:25:12,021
<i>What was that?</i>
<i>You don't care if you live or die?</i>

231
00:25:12,091 --> 00:25:15,117
If I kill them first, not really.

232
00:25:16,762 --> 00:25:18,855
Maybe I do.

233
00:25:18,931 --> 00:25:20,922
Keep moving!

234
00:25:36,482 --> 00:25:39,747
[ Grunting ]

235
00:25:51,997 --> 00:25:54,397
[ Grunting ]
[ Grunting ]

236
00:26:18,257 --> 00:26:21,351
<i>[ Riddick ]</i>
<i>Kyra?</i>

237
00:26:21,427 --> 00:26:23,452
- Kyra!
- What?

238
00:26:23,529 --> 00:26:26,191
Get that ass moving!

239
00:26:53,025 --> 00:26:57,394
- [ Rocks Sizzle ]
- [ Groans ]

240
00:26:58,797 --> 00:27:01,061
[ Breathing Heavily ]

241
00:27:03,869 --> 00:27:06,736
[ Shouts ]
Riddick?

242
00:27:11,410 --> 00:27:15,403
Remember what I said
about not caring if I lived or died?

243
00:27:19,084 --> 00:27:21,279
You knew I was kidding, right?

244
00:27:24,556 --> 00:27:26,046
<i>"</i>One speed."

245
00:27:36,435 --> 00:27:38,198
<i>[ Rumbling ]</i>

246
00:27:46,678 --> 00:27:48,543
Temperature differential.

247
00:27:48,614 --> 00:27:51,105
Your rope.

248
00:27:51,183 --> 00:27:53,549
It's too late.
We're never gonna make it.

249
00:27:53,619 --> 00:27:55,712
Your rope! Gimme your rope!

250
00:27:56,855 --> 00:27:58,914
And your water. All of it!

251
00:27:59,958 --> 00:28:03,416
Stay in the shadow of the mountain.
Don't wait for me. Run!

252
00:28:15,340 --> 00:28:17,706
Riddick!

253
00:28:28,754 --> 00:28:30,153
Aaah!

254
00:28:35,327 --> 00:28:39,093
[ Flames Roaring ]

255
00:28:39,164 --> 00:28:41,655
[ Grunts ]

256
00:28:55,414 --> 00:28:59,680
[ Breathing Heavily ]

257
00:29:01,653 --> 00:29:03,587
Where the hell is that hangar?

258
00:29:05,591 --> 00:29:08,526
<i>[ Rocks Sizzling ]</i>

259
00:29:08,594 --> 00:29:12,394
<i>There it is.</i>

260
00:29:12,464 --> 00:29:14,329
[ Grunts ]

261
00:29:14,399 --> 00:29:18,665
- Listen.
<i>- [ Wind Whistling ]</i>

262
00:29:30,115 --> 00:29:33,107
<i>[ Wind Howling ]</i>
[ Engines Thrumming ]

263
00:29:38,056 --> 00:29:41,423
- [ Sniffs ]
- [ Amplified Breathing ]

264
00:29:41,493 --> 00:29:43,927
[ Breathing Heavily ]

265
00:29:43,996 --> 00:29:46,931
Let me guess. Necros.

266
00:29:46,999 --> 00:29:50,196
And a whole lotta Necro firepower.

267
00:29:50,269 --> 00:29:53,568
<i>Shit.! I hate not being the bad guys.</i>

268
00:29:53,639 --> 00:29:56,938
[ Amplified Breathing ]

269
00:29:57,009 --> 00:30:00,001
<i>[ Electronic Clicking ]</i>

270
00:30:01,046 --> 00:30:03,173
[ Electronic Screeching ]

271
00:30:09,087 --> 00:30:12,716
[ Rumbling In Distance ]

272
00:30:21,633 --> 00:30:26,764
I figure we got three minutes
before the sun hits us again,
burns out this whole valley.

273
00:30:26,838 --> 00:30:29,534
- Wait.
- We gonna do this or not?

274
00:30:29,608 --> 00:30:31,508
Just wait.

275
00:30:36,581 --> 00:30:38,742
Ellen.

276
00:30:40,185 --> 00:30:43,154
Her name was Ellen.
I never really forgot.

277
00:30:57,869 --> 00:31:00,360
[ Electronic Screeching ]

278
00:31:00,439 --> 00:31:03,033
Aaah! Boss!

279
00:31:10,782 --> 00:31:14,218
<i>[ Gunfire Continues ]</i>

280
00:31:14,286 --> 00:31:18,154
- Remember that favorite game of yours?
<i>- "</i>Who's the Better Killer?"

281
00:31:18,223 --> 00:31:20,214
<i>[ Gunfire Continues ]</i>

282
00:31:20,292 --> 00:31:24,285
- Let's play.
- Come on!

283
00:31:33,071 --> 00:31:34,936
[ Groans ]

284
00:31:42,581 --> 00:31:44,276
On the right!

285
00:31:48,053 --> 00:31:49,418
[ Yelling ]

286
00:31:51,790 --> 00:31:53,587
Watch out!
Behind you!

287
00:31:53,658 --> 00:31:55,421
<i>Kyra.!</i>

288
00:32:03,735 --> 00:32:06,795
- [ Explosions ]
- [ Shouts ]

289
00:32:51,483 --> 00:32:53,508
Sybar!

290
00:32:59,958 --> 00:33:01,255
[ Screams, Grunts ]

291
00:33:07,699 --> 00:33:10,065
[ Yells ]

292
00:33:12,871 --> 00:33:14,065
[ Screams ]

293
00:33:14,139 --> 00:33:16,937
[ Yells, Grunts ]

294
00:33:21,646 --> 00:33:22,943
[ Growls ]
[ Gasps ]

295
00:33:25,951 --> 00:33:28,579
[ Yells ]

296
00:33:28,653 --> 00:33:30,951
[ Grunts ]

297
00:33:33,158 --> 00:33:35,092
[ Yells ]

298
00:33:35,160 --> 00:33:37,993
[ Grunts, Groans ]

299
00:33:38,063 --> 00:33:40,054
[ Cries Out ]

300
00:33:42,300 --> 00:33:43,961
- [ Shouts ]
- [ Grunts ]

301
00:33:55,046 --> 00:33:56,980
<i>[ Kyra ]</i>
<i>No.!</i>

302
00:34:09,828 --> 00:34:12,592
- [ Yells ]
- [ Grunts ]

303
00:34:15,901 --> 00:34:18,927
[ Grunting ]

304
00:34:19,004 --> 00:34:21,234
Aaah!

305
00:34:28,013 --> 00:34:31,972
[ Gasping, Groaning ]

306
00:34:33,919 --> 00:34:35,546
[ Yells ]

307
00:34:38,924 --> 00:34:41,620
<i>Get up. Get up.!</i>

308
00:34:41,693 --> 00:34:43,661
<i>Please get up.</i>

309
00:34:56,975 --> 00:34:58,636
<i>So,</i>

310
00:35:00,545 --> 00:35:03,343
you can kneel.

311
00:35:20,799 --> 00:35:23,290
<i>[ Woman's Voice, Distorted ]</i>
<i>I think you know now.</i>

312
00:35:24,970 --> 00:35:27,404
I think you know
who tore Furya apart.

313
00:35:30,742 --> 00:35:34,269
This mark carries the anger
of an entire race.

314
00:35:34,346 --> 00:35:36,541
[ Inhales ]

315
00:35:39,618 --> 00:35:41,711
But it's going to hurt.

316
00:36:39,144 --> 00:36:41,135
[ Groaning ]

317
00:36:41,212 --> 00:36:43,203
[ Groans ]

318
00:36:45,483 --> 00:36:48,646
<i>[ Wind Whistling ]</i>
<i>[ Engines Thrumming ]</i>

319
00:36:55,994 --> 00:36:59,327
<i>[ Alarm Siren ]</i>

320
00:37:38,670 --> 00:37:40,661
[ Gasps ]

321
00:37:44,075 --> 00:37:47,067
<i>[ Soft Clatter ]</i>

322
00:37:59,557 --> 00:38:01,787
Kyra?

323
00:38:05,663 --> 00:38:08,598
<i>I was supposed to</i>
<i>deliver a message to you...</i>

324
00:38:10,568 --> 00:38:12,866
if Vaako failed to kill you.

325
00:38:14,773 --> 00:38:17,537
A message from
the Lord Marshal himself.

326
00:38:19,444 --> 00:38:22,936
He tells you to stay away from Helion,
stay away from him,

327
00:38:24,282 --> 00:38:26,409
and in return,
you'll be hunted no more.

328
00:38:33,391 --> 00:38:36,224
But Vaako will most likely
report you as dead.

329
00:38:37,629 --> 00:38:39,893
So this is your chance.

330
00:38:39,964 --> 00:38:43,730
Your chance to do
what no man has ever done.

331
00:38:43,802 --> 00:38:45,793
The girl.

332
00:38:46,805 --> 00:38:49,000
Where will they take her?

333
00:38:49,073 --> 00:38:52,042
[ Gasping ]

334
00:39:05,990 --> 00:39:09,323
We all began... as something else.

335
00:39:13,565 --> 00:39:18,161
I've done... unbelievable things...

336
00:39:19,170 --> 00:39:21,934
in the name of a faith
that was never my own.

337
00:39:23,842 --> 00:39:27,801
And he'll do to her
what he did to me.

338
00:39:34,052 --> 00:39:38,682
The Necromonger in me
warns you not to go back.

339
00:39:40,024 --> 00:39:42,652
But the Furyan in me...

340
00:39:46,197 --> 00:39:47,926
hopes you won't listen.

341
00:39:53,771 --> 00:39:58,435
God knows... I've dreamed of it.

342
00:40:59,637 --> 00:41:02,504
<i>I have lost a Purifier...</i>

343
00:41:03,741 --> 00:41:08,542
<i>but I have gained</i>
<i>a first among commanders.</i>

344
00:41:09,547 --> 00:41:12,175
It is overdue, isn't it--

345
00:41:12,250 --> 00:41:14,946
that we acknowledge
your many accomplishments,

346
00:41:15,019 --> 00:41:17,180
<i>your steady faith,</i>

347
00:41:17,255 --> 00:41:22,784
and above all,
your unflinching loyalty.

348
00:41:30,535 --> 00:41:34,027
Obedience without question.
Loyalty till UnderVerse come.

349
00:41:36,541 --> 00:41:38,532
Well done, Vaako.

350
00:41:39,811 --> 00:41:43,406
This is a day of days.

351
00:42:03,201 --> 00:42:05,726
Look more pleased, Vaako.

352
00:42:07,505 --> 00:42:10,906
You have killed his enemy...

353
00:42:10,975 --> 00:42:12,738
and his suspicions.

354
00:42:12,810 --> 00:42:17,611
- I should have brought back his head.
- You saw him unbreathing.
You saw him dead on the ground.

355
00:42:17,682 --> 00:42:20,674
Riddick was no common breeder.
In a heartbeat he dropped 20 of my men.

356
00:42:20,752 --> 00:42:24,711
All mysteries are not miracles,
not even in this religion.

357
00:42:27,225 --> 00:42:29,819
And if you say it is certain,
then it is certain,

358
00:42:29,894 --> 00:42:33,193
and we've already said it,
haven't we?

359
00:42:35,833 --> 00:42:37,824
We have.

360
00:42:39,904 --> 00:42:41,531
<i>[ Door Opens ]</i>

361
00:42:41,606 --> 00:42:44,131
<i>Now, tell me if it's true.</i>

362
00:42:44,208 --> 00:42:48,440
Tell me the Furyan is gone,
and I can close this campaign
without hearing his bootsteps.

363
00:42:49,947 --> 00:42:51,938
If he is dead,

364
00:42:53,117 --> 00:42:55,142
I sense I'm not far
from the same fate,

365
00:42:56,220 --> 00:42:58,154
being of no further use here.

366
00:42:59,223 --> 00:43:02,283
Shouldn't I tell you
that Riddick is still alive?

367
00:43:02,360 --> 00:43:04,157
Don't try me, Aereon.

368
00:43:04,228 --> 00:43:06,492
I could plow you under
with the rest of Helion Prime.

369
00:43:06,564 --> 00:43:08,896
No one really knows the future.

370
00:43:08,966 --> 00:43:12,766
Then tell me the odds
that Vaako met with success,

371
00:43:12,837 --> 00:43:14,828
that I will now be the one...

372
00:43:14,906 --> 00:43:17,807
who can carry his people across
the threshold into UnderVerse,

373
00:43:17,875 --> 00:43:19,866
where they shall begin true life.

374
00:43:19,944 --> 00:43:22,242
Tell me what I want to hear, Aereon,

375
00:43:22,313 --> 00:43:26,409
and maybe
I'll save your home world... for last.

376
00:43:31,255 --> 00:43:33,746
- The odds are good...
- That?

377
00:43:33,825 --> 00:43:36,385
that you will reach the UnderVerse...

378
00:43:39,597 --> 00:43:41,588
<i>soon.</i>

379
00:44:19,971 --> 00:44:21,768
Ascension protocol!

380
00:44:21,839 --> 00:44:25,536
We still have numbers
out there, Lord Marshal--
sweep teams, recon ships.

381
00:44:25,610 --> 00:44:27,669
They would be hard-pressed
to make it back--

382
00:44:32,483 --> 00:44:36,283
Get my armada off the ground!

383
00:44:39,056 --> 00:44:41,718
<i>[ Engines Thrumming ]</i>
<i>[ Large Machinery Noises ]</i>

384
00:45:09,821 --> 00:45:12,585
[ Chattering ]

385
00:45:16,160 --> 00:45:17,684
[ Heavy Slam ]

386
00:45:24,936 --> 00:45:27,097
- [ Chattering Continues ]
- [ Heavy Slam ]

387
00:45:37,815 --> 00:45:41,376
- You mean on Helion?
- I mean here, on this very ship!

388
00:45:41,452 --> 00:45:45,752
Could you be wrong?
Mind fabricates fear.

389
00:45:45,823 --> 00:45:50,089
Could you be wrong?
Not so wrong as you
when you left him alive!

390
00:45:50,161 --> 00:45:52,095
It's twice a mistake.

391
00:45:52,163 --> 00:45:55,428
Not only your failure,
but now the report of success.

392
00:45:55,499 --> 00:45:59,094
How do we salvage this? How? How?

393
00:45:59,170 --> 00:46:01,730
Lord Marshal's got to be warned.

394
00:46:01,806 --> 00:46:03,740
You will never see the UnderVerse!

395
00:46:05,076 --> 00:46:07,203
He will kill us both
before our due time.

396
00:46:10,348 --> 00:46:12,282
I say give Riddick his chance.

397
00:46:17,121 --> 00:46:19,715
If he is half of what you think,

398
00:46:19,790 --> 00:46:21,815
he can at least wound the Lord Marshal--

399
00:46:21,893 --> 00:46:24,384
and that is when you must act.

400
00:46:24,462 --> 00:46:26,828
Just to take his place?
Just to keep what I kill?

401
00:46:26,898 --> 00:46:29,628
- That is the Necromonger way.
- It is not enough!

402
00:46:29,700 --> 00:46:32,464
Then you do it for the faith!

403
00:46:32,536 --> 00:46:35,266
If he has fear, he has weakness.

404
00:46:35,339 --> 00:46:37,273
If he has weakness, Vaako--

405
00:46:39,310 --> 00:46:41,471
He is unworthy of lordship.

406
00:46:42,480 --> 00:46:44,812
We do it for all Necromongers.

407
00:46:46,317 --> 00:46:48,478
Protect the faith.

408
00:46:48,552 --> 00:46:51,578
This can still be a day of days,

409
00:46:51,656 --> 00:46:53,749
but the timing...

410
00:46:53,824 --> 00:46:56,816
must be flawless.

411
00:47:18,649 --> 00:47:22,176
Final protocol.
Execute on my order.

412
00:47:29,760 --> 00:47:32,752
<i>[ Panicked Chattering ]</i>

413
00:47:38,336 --> 00:47:41,328
<i>[ Engines Thrumming ]</i>

414
00:47:57,088 --> 00:47:59,352
Go inside.

415
00:48:00,458 --> 00:48:02,449
Are they leaving now?

416
00:48:02,526 --> 00:48:05,290
Ziza! Go inside!

417
00:48:45,302 --> 00:48:47,361
[ Gasps ]

418
00:48:57,048 --> 00:48:59,312
<i>We found this Lensor dead.</i>

419
00:49:01,886 --> 00:49:05,344
<i>[ Lord Marshal ]</i>
<i>Show me his last sight.</i>

420
00:49:17,735 --> 00:49:19,430
Commander Toal.

421
00:49:19,503 --> 00:49:21,494
He won't escape twice.

422
00:50:30,841 --> 00:50:33,969
[ Chattering ]

423
00:50:45,623 --> 00:50:47,614
<i>[ Metallic Slicing Noise ]</i>

424
00:50:50,961 --> 00:50:52,952
<i>[ Metallic Slicing Noise ]</i>

425
00:50:55,866 --> 00:50:58,926
[ Metallic Slicing ]

426
00:51:10,714 --> 00:51:12,875
[ Grunts ]

427
00:51:13,884 --> 00:51:17,012
- [ Grunts ]
- Stay your weapons!

428
00:51:18,455 --> 00:51:20,446
He came for me.

429
00:51:47,218 --> 00:51:49,584
<i>[ Lord Marshal ]</i>
<i>Consider this:;</i>

430
00:51:54,458 --> 00:51:58,554
<i>If you fall here now,</i>

431
00:51:58,629 --> 00:52:00,563
you'll never rise.

432
00:52:00,631 --> 00:52:03,759
<i>But if you choose another way--</i>

433
00:52:04,935 --> 00:52:07,096
<i>the Necromonger way--</i>

434
00:52:07,171 --> 00:52:09,696
<i>you'll die in due time...</i>

435
00:52:09,773 --> 00:52:13,504
and rise again in the UnderVerse.

436
00:52:23,153 --> 00:52:25,348
Go to him.

437
00:52:32,863 --> 00:52:34,854
It hurts...

438
00:52:35,866 --> 00:52:38,130
at first.

439
00:52:38,202 --> 00:52:41,797
But after a while,
the pain goes away,
just as they promise.

440
00:52:44,208 --> 00:52:46,733
- Are you with me, Kyra?
- There's a moment...

441
00:52:46,810 --> 00:52:49,210
when you can almost
see the UnderVerse through his eyes.

442
00:52:50,547 --> 00:52:53,311
It makes it sound perfect--

443
00:52:53,384 --> 00:52:55,318
a place where anyone
can start over.

444
00:52:55,386 --> 00:52:57,854
Are you with me, Kyra?

445
00:53:10,401 --> 00:53:12,835
Convert now...

446
00:53:12,903 --> 00:53:15,064
or fall forever.

447
00:53:19,843 --> 00:53:22,107
You killed everything I know.

448
00:53:27,251 --> 00:53:28,445
Vaako.

449
00:53:28,519 --> 00:53:30,487
[ Grunts ]

450
00:53:30,554 --> 00:53:32,818
Not yet.

451
00:53:44,368 --> 00:53:47,337
Been a long time
since I've seen my own blood.

452
00:55:05,616 --> 00:55:08,608
These are his last moments.

453
00:55:25,202 --> 00:55:28,137
[ Grunting ]

454
00:55:28,205 --> 00:55:31,038
Give me your soul.

455
00:55:36,046 --> 00:55:39,482
- Fuck you!
- [ Groans ]

456
00:55:39,550 --> 00:55:41,882
[ Growls ]

457
00:55:43,787 --> 00:55:45,345
[ Grunts ]

458
00:55:50,093 --> 00:55:51,993
[ Yells ]

459
00:56:08,445 --> 00:56:10,743
[ Grunting ]

460
00:56:28,065 --> 00:56:30,090
[ Grunts ]

461
00:56:31,502 --> 00:56:33,970
[ Grunting ]

462
00:56:44,781 --> 00:56:47,045
You're not the one to bring me down.

463
00:56:48,051 --> 00:56:49,484
[ Groans ]

464
00:56:59,062 --> 00:57:01,121
[ Gasps ]

465
00:57:04,234 --> 00:57:05,963
[ Groans ]

466
00:57:06,036 --> 00:57:08,766
Now! Kill the beast
while he's wounded!

467
00:57:19,149 --> 00:57:21,982
[ Exhales Deeply ]

468
00:57:23,353 --> 00:57:26,379
[ Groaning ]

469
00:57:26,456 --> 00:57:29,482
Help me, Vaako. Kill him.

470
00:57:37,134 --> 00:57:38,465
Vaako?

471
00:57:43,140 --> 00:57:45,131
Forgive me.

472
00:57:45,208 --> 00:57:46,903
Flawless.

473
00:58:17,774 --> 00:58:20,242
[ Gasps ]
No.

474
00:58:26,316 --> 00:58:29,717
No-o-o!
[ Echoing ]

475
00:58:29,786 --> 00:58:32,880
Now what would be
the odds of that?

476
00:58:45,869 --> 00:58:48,463
I thought you were dead.

477
00:58:50,707 --> 00:58:53,267
Are you with me, Kyra?

478
00:58:55,345 --> 00:58:57,905
[ Sighs ]
I was always with you.

479
00:59:00,250 --> 00:59:02,241
I was.

480
01:00:25,802 --> 01:00:28,134
You keep what you kill.

