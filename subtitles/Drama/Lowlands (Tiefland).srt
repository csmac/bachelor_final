1
00:00:00,000 --> 00:00:12,100
Leni Riefenstahl  Tiefland

2
00:10:32,600 --> 00:10:34,090
What do you want?

3
00:10:34,780 --> 00:10:38,550
Mr. Mayoral, the stream has been diverted

4
00:10:39,120 --> 00:10:39,880
So what?

5
00:10:40,560 --> 00:10:43,610
It has been dry for one year now. We need water for our fields.

6
00:10:44,350 --> 00:10:45,870
Is that water yours?

7
00:10:46,770 --> 00:10:48,080
It belongs to us all, Sir. It comes from above

8
00:10:51,970 --> 00:10:52,870
So

9
00:10:53,140 --> 00:10:57,030
Mr. Mayoral, we beg you, let the stream flow freely again

10
00:10:57,570 --> 00:11:00,350
No, the Marquis needs that water for his bulls

11
00:12:43,000 --> 00:12:46,790
Be careful. Don't be so nervous

12
00:12:48,320 --> 00:12:50,140
I hope he'll come today 

13
00:12:51,250 --> 00:12:53,870
Should I send someone at the castle to ask?

14
00:12:54,140 --> 00:12:57,950
He'll come today
Try to look a bit more joyful, Amelia

15
00:12:58,080 --> 00:13:00,420
He can get my money also with the way I look now

16
00:13:00,490 --> 00:13:03,010
But he is the Marquis of Roccabruna, don't forget that

17
00:13:04,330 --> 00:13:05,880
The Marquis is a beggar. 

18
00:13:06,630 --> 00:13:08,370
Someone should finally tell him.

19
00:13:08,550 --> 00:13:10,620
Be careful! If not everything will be over.

20
00:13:10,910 --> 00:13:13,800
When I'll be there, the first one I'll throw out is Don Camillo

21
00:13:14,330 --> 00:13:16,020
Today he asked for his commission again

22
00:13:16,890 --> 00:13:19,080
After the wedding! Give him half of his commission now.

23
00:13:20,650 --> 00:13:22,650
It will be a great honor for the family 

24
00:13:22,970 --> 00:13:24,520
when you will be the marchioness

25
00:13:26,960 --> 00:13:29,690
In fact it's too much money for this honor

26
00:13:54,640 --> 00:13:56,560
Mr. Marquis, it's time. 

27
00:13:57,390 --> 00:14:00,120
The mayor has been waiting in vain three times already.

28
00:14:00,450 --> 00:14:01,790
Is there no other solution?

29
00:14:02,120 --> 00:14:03,430
The Mayor of Huesca is a rich man

30
00:14:03,550 --> 00:14:05,410
I don't like that woman

31
00:14:05,790 --> 00:14:09,930
But the Mayor is very rich and Donna Amelia is his only daughter. 
38
00:14:11,756 -- 00:14:15,322
Next week, parts of your debts have to be paid.
And there is no money left.

32
00:14:16,470 --> 00:14:17,720
What about moneylenders?

33
00:14:18,350 --> 00:14:22,690
Impossible not the ones in Barcelona nor those in Madrid. 
And besides, everything here is in pawn.

34
00:14:24,360 --> 00:14:25,800
But why do I have so many debts? 

35
00:14:27,080 --> 00:14:28,430
How is that possible?

36
00:14:28,890 --> 00:14:31,740
If you don't trust my words, 

37
00:14:32,150 --> 00:14:33,870
I can also leave.

38
00:14:34,030 --> 00:14:35,860
Nonsense, Camillo 

39
00:14:38,260 --> 00:14:40,800
But tell me: what about the rent the peasants pay?

40
00:14:41,590 --> 00:14:44,470
Since you had this weir built, the peasants have

41
00:14:44,820 --> 00:14:47,670
no more water. So their harvests are really bad.

42
00:14:48,240 --> 00:14:49,890
I can't let my bulls die. 

43
00:14:50,230 --> 00:14:51,880
The best bulls of the province.

44
00:14:52,050 --> 00:14:54,890
The province? The whole kingdom you mean.

45
00:14:55,220 --> 00:14:59,100
Well then let's go!

46
00:15:32,310 --> 00:15:34,850
Do you like the food Pedro?

47
00:15:35,950 --> 00:15:37,620
Why don't you come more often?

48
00:15:38,280 --> 00:15:39,590
I like to be alone in the mountains

49
00:15:39,840 --> 00:15:41,490
You cannot remain alone all your life.

50
00:15:42,280 --> 00:15:44,280
I really love to be alone

51
00:15:56,080 --> 00:15:58,860
Did you find that pelt? 

52
00:15:59,130 --> 00:16:00,760
Or did your sheep kill the wolf?

53
00:16:01,030 --> 00:16:02,760
Found?

54
00:16:09,390 --> 00:16:11,710
Don't laugh like that, stupid girls. 

55
00:16:12,990 --> 00:16:15,730
Pedro, here's some flour. Do you still have salt?

56
00:16:16,470 --> 00:16:18,140
Then I'll give you some to take with you

57
00:16:18,840 --> 00:16:21,020
Pedro, which one of us all do you like?

58
00:16:22,070 --> 00:16:23,550
None!

59
00:16:25,070 --> 00:16:26,360
Do you already have a girl? 

60
00:16:26,910 --> 00:16:27,910
And how does she look like?

61
00:16:28,520 --> 00:16:29,670
Is she prettier than I?

62
00:16:30,490 --> 00:16:31,490
Yes

63
00:16:47,890 --> 00:16:48,890
Don't leave so quickly Pedro!

64
00:16:53,690 --> 00:16:55,610
Here is the salt
Thanks Josefa

65
00:16:56,490 --> 00:16:58,710
Don't you want to leave that pelt here?

66
00:16:59,070 --> 00:17:02,120
No, I'll come back when Don Sebastian is at home

67
00:17:02,820 --> 00:17:05,710
Bye bye

68
00:17:06,140 --> 00:17:07,080
Bye bye. Come back soon!

69
00:18:04,460 --> 00:18:06,310
Please, the sixty-year-old Malaga wine!

70
00:18:06,690 --> 00:18:07,190
Immediately Sir!

71
00:18:16,790 --> 00:18:18,690
Very old.

72
00:18:19,530 --> 00:18:21,740
Can I ask you for a glass of water?

73
00:18:22,960 --> 00:18:24,710
Water for the Marquis!!!

74
00:18:41,810 --> 00:18:43,610
May I ask you for your arm?

75
00:18:44,160 --> 00:18:45,490
With pleasure

76
00:19:08,080 --> 00:19:10,080
The Marquis is in a good mood I hope?

77
00:19:10,420 --> 00:19:13,280
Of course everything will work out well

78
00:19:13,550 --> 00:19:14,820
It would be a happy marriage

79
00:19:18,610 --> 00:19:20,340
What a lovely night 

80
00:19:26,230 --> 00:19:28,630
But the Marquis seems thinking

81
00:19:30,430 --> 00:19:32,970
Would you share your thoughts with me?

82
00:19:35,720 --> 00:19:37,620
I was just wondering how I could express myself

83
00:19:38,610 --> 00:19:39,980
Oh something special?

84
00:19:40,730 --> 00:19:42,200
Very

85
00:19:42,510 --> 00:19:44,270
I have been thinking about it for days.

86
00:19:46,120 --> 00:19:48,000
I beg you, express yourself.

87
00:19:49,710 --> 00:19:52,310
I am wondering 

88
00:19:52,550 --> 00:19:54,770
how I could enlarge my bull breeding

89
00:19:58,670 --> 00:20:00,640
The bulls your real passion.

90
00:20:01,620 --> 00:20:04,240
Is it profitable?

91
00:20:06,270 --> 00:20:09,330
I don't keep bulls to earn money. 

92
00:20:10,380 --> 00:20:13,610
But I wanted to ask something else, Donna Amelia: 

93
00:20:14,350 --> 00:20:16,290
would you want to become my wife?

94
00:20:17,470 --> 00:20:18,580
Your wife?  

95
00:20:20,770 --> 00:20:23,110
Ah I understand You need money.

96
00:20:24,370 --> 00:20:26,890
Well, I'll think about it.

97
00:20:30,260 --> 00:20:31,560
My child, what happened?

98
00:20:31,860 --> 00:20:32,980
Leave me alone

99
00:20:36,320 --> 00:20:39,840
Sir
My best regards to Donna Amelia

100
00:20:40,260 --> 00:20:41,890
Are you leaving already?

101
00:20:43,770 --> 00:20:44,330
Bonsoir

102
00:20:48,770 --> 00:20:50,030
Music please!

103
00:21:01,150 --> 00:21:02,780
The Marquis will pay for this!!! 

104
00:21:03,250 --> 00:21:05,280
He'll pay for this
But my child calm down.

105
00:21:05,830 --> 00:21:07,920
I'll have him on his knees. 

106
00:21:08,240 --> 00:21:10,960
It will be finished with his arrogance 
when he knows who has his promissory notes

107
00:21:11,260 --> 00:21:12,900
the beggar of Roccabrona

108
00:21:21,870 --> 00:21:24,310
You are laughing, but I'll have to fix it!

109
00:21:24,630 --> 00:21:27,000
Let's go

110
00:23:21,700 --> 00:23:22,710
Wait for me

111
00:23:26,130 --> 00:23:28,350
Don Sebastian!
What do you want?

112
00:23:30,160 --> 00:23:32,870
I'm bringing you a wolf's pelt.

113
00:23:36,650 --> 00:23:40,060
Did you kill the wolf?
yes Sir, I killed it.

114
00:23:45,110 --> 00:23:47,790
I'll give you one Deuro for every wolf you kill

115
00:23:51,880 --> 00:23:53,610
Go back to your sheep.

116
00:25:26,330 --> 00:25:28,930
Come to the castle tonight. She should dance for me.

117
00:27:42,090 --> 00:27:43,000
Wait here!

118
00:28:04,060 --> 00:28:05,800
Welcome in my house

119
00:28:06,350 --> 00:28:08,370
At your service, Sir

120
00:28:08,660 --> 00:28:11,380
Go to the kitchen, there you can eat 

121
00:28:13,790 --> 00:28:15,650
And you come with me.

122
00:28:19,530 --> 00:28:22,990
He little friend, come! 

123
00:28:24,910 --> 00:28:28,580
You cannot expect that you will sit at the same table as them.

124
00:29:01,010 --> 00:29:02,950
Do you like it here?  

125
00:29:06,480 --> 00:29:07,350
What's your name?

126
00:29:09,420 --> 00:29:10,570
Martha

127
00:29:21,420 --> 00:29:22,570
Martha Tell me about yourself Martha.

128
00:29:27,790 --> 00:29:29,490
There is not much to tell, Sir. 

129
00:29:30,230 --> 00:29:32,380
We wander along the roads 

130
00:29:32,630 --> 00:29:35,180
and stop when we find inns, 
then I dance there.

131
00:29:35,800 --> 00:29:37,320
Where did you learn to dance?

132
00:29:37,620 --> 00:29:39,010
I never learnt it

133
00:29:39,930 --> 00:29:41,540
So it's a natural talent. 

134
00:29:43,110 --> 00:29:45,180
And this guy is you lover?
No, Sir

135
00:29:45,720 --> 00:29:48,560
You cannot fool me!
Yet it is the truth.

136
00:29:49,660 --> 00:29:51,940
I'm just happy to be able to dance

137
00:29:52,490 --> 00:29:54,550
But you are not letting me have a drink alone?

138
00:29:58,540 --> 00:29:59,970
Come Drink with me.

139
00:30:06,180 --> 00:30:07,860
To your beauty!

140
00:31:02,450 --> 00:31:05,090
Oh Sir, I know where it is

141
00:31:05,580 --> 00:31:08,550
Yes?  Then get up

142
00:33:42,870 --> 00:33:45,370
Pedro!
Nando!

143
00:33:48,110 --> 00:33:51,280
- I'm glad to be in the mountains again
- Do you come from the low land?

144
00:33:52,090 --> 00:33:55,500
Yes, 

145
00:33:55,840 --> 00:33:58,510
but here, I feel a free man. 

146
00:34:18,650 --> 00:34:20,750
Do you have anything to drink?

147
00:34:21,230 --> 00:34:22,170
I have plenty of milk.

148
00:34:26,050 --> 00:34:28,580
Where are your sheep grazing?
At the lake

149
00:34:31,650 --> 00:34:34,450
- Do you have many lambs?
- Sixteen!

150
00:34:36,490 --> 00:34:40,000
I had twelve but the golden eagle took two

151
00:34:43,490 --> 00:34:44,720
Were you in Roccabrona?

152
00:34:49,980 --> 00:34:51,810
You have changed Pedro 

153
00:34:52,440 --> 00:34:54,320
since I was here last. 

154
00:34:54,920 --> 00:34:56,970
That's a while ago.

155
00:34:59,100 --> 00:35:00,840
Changed, you say 

156
00:35:01,940 --> 00:35:03,130
Yes Nando, 

157
00:35:03,400 --> 00:35:04,990
before I was happy that

158
00:35:05,430 --> 00:35:07,340
the sun was shining only for myself 

159
00:35:08,520 --> 00:35:10,460
and the sloes were scented only for me. 

160
00:35:13,070 --> 00:35:14,880
Now, I would like to share 

161
00:35:16,090 --> 00:35:17,940
everything that is nice

162
00:35:19,450 --> 00:35:20,840
and makes me happy

163
00:35:21,620 --> 00:35:22,940
with a woman.

164
00:35:24,520 --> 00:35:25,890
Do you anything about women?

165
00:35:28,680 --> 00:35:30,270
I never was with a woman

166
00:35:31,150 --> 00:35:33,180
You'll find women only in the lowland. 

167
00:35:33,780 --> 00:35:35,710
And the lowland is bad. 

168
00:35:35,940 --> 00:35:37,940
The women there are possessed by the devil

169
00:35:38,470 --> 00:35:41,710
I know one and I'm even prepared to fight with the devil

170
00:35:42,590 --> 00:35:44,800
Yes, I know one.

171
00:35:46,620 --> 00:35:47,110
Come

172
00:35:48,620 --> 00:35:50,110
Choose something

173
00:35:52,800 --> 00:35:54,090
Make yourself beautiful

174
00:36:17,040 --> 00:36:18,520
That's the way you should look for me! 

175
00:36:30,270 --> 00:36:34,220
You're to good to wanderand dancing in inns.

176
00:36:38,470 --> 00:36:40,810
The old guy, the musician, he's not your father is he?

177
00:36:42,540 --> 00:36:43,920
Does he beat you? 

178
00:36:52,640 --> 00:36:54,330
I won't let you go back to him. 

179
00:36:55,660 --> 00:36:57,470
I want you to stay here. 

180
00:36:57,860 --> 00:36:59,480
Say you want stay 

181
00:37:02,650 --> 00:37:04,290
I love you 

182
00:37:05,190 --> 00:37:07,020
and want you to love me too.

183
00:37:51,190 --> 00:37:54,040
Listen here. You have beaten that girl.

184
00:37:55,860 --> 00:37:57,520
I'll have you whipped out for that. 

185
00:38:05,690 --> 00:38:07,630
When you're still here in an hour, 

186
00:38:08,300 --> 00:38:09,460
the dogs will kill you. 

187
00:38:21,310 --> 00:38:22,390
Go! 

188
00:38:24,810 --> 00:38:25,720
Hurry up!

189
00:39:12,040 --> 00:39:15,580
I can't give you more. Only one bucket per person.

190
00:39:56,240 --> 00:39:58,060
Good day Don Sebastian

191
00:39:58,400 --> 00:40:00,820
- Is everything all right?
- Yes Sir.

192
00:40:56,350 --> 00:40:58,140
You devil!

193
00:41:15,780 --> 00:41:20,330
Here we believe that the devil can take possession of people.

194
00:41:21,960 --> 00:41:25,390
He's good to you, as good as he can be. 

195
00:41:26,810 --> 00:41:29,670
But the peasants call him the wolf.

196
00:41:30,380 --> 00:41:31,930
How could we help the peasants?

197
00:41:33,650 --> 00:41:36,870
You can't, I can't. None of us can.

198
00:41:37,730 --> 00:41:39,020
I'll talk to him.

199
00:41:40,920 --> 00:41:43,800
- He'll beat you
- No, he'll not beat me.

200
00:41:59,050 --> 00:42:00,560
I have a surprise for you. 

201
00:42:04,680 --> 00:42:06,240
I want to give you this. 

202
00:42:10,350 --> 00:42:11,970
Now it's yours.

203
00:42:14,500 --> 00:42:15,970
I have a request

204
00:42:16,530 --> 00:42:17,910
Anything you want!

205
00:42:18,410 --> 00:42:20,080
The peasants have been waiting for hours

206
00:42:20,590 --> 00:42:22,420
I know what they want

207
00:42:33,190 --> 00:42:34,610
Talk to them

208
00:42:37,510 --> 00:42:39,090
That's my request

209
00:42:39,650 --> 00:42:40,890
All right, I'll do it.

210
00:43:00,160 --> 00:43:01,610
Well

211
00:43:04,480 --> 00:43:07,140
Sir, the village is in distress. 

212
00:43:07,500 --> 00:43:08,860
The well has run out. 

213
00:43:09,640 --> 00:43:11,490
We must have water!

214
00:43:11,760 --> 00:43:13,320
Ah! You must

215
00:43:13,820 --> 00:43:18,650
Our cattle die of thirst, the fields are dry. 
The corn is burned. 

216
00:43:19,960 --> 00:43:21,610
If you don't give the water, 

217
00:43:22,010 --> 00:43:24,250
we cannot pay the rent.

218
00:43:25,430 --> 00:43:27,390
Then I'll have chased away

219
00:43:31,020 --> 00:43:31,890
Sir! 

220
00:43:34,190 --> 00:43:36,000
Will you give us the water?

221
00:43:44,810 --> 00:43:45,990
Go

222
00:45:01,580 --> 00:45:02,600
What do you want?

223
00:45:03,560 --> 00:45:04,960
I have to talk to you. 

224
00:45:09,400 --> 00:45:12,040
Take this for the rent. 
I want to help you.

225
00:45:12,960 --> 00:45:13,900
Goodbye.

226
00:45:57,420 --> 00:45:59,700
- Natario, did you fix it?
- Yes.

227
00:46:00,670 --> 00:46:02,530
The millstones are turning again

228
00:46:04,050 --> 00:46:06,680
Yes, the millstones are turning again

229
00:46:08,830 --> 00:46:11,970
Today is our lucky day. 
We can pay the rent. 

230
00:46:16,100 --> 00:46:18,700
That foreign girl gave us this.
Who?

231
00:46:19,610 --> 00:46:21,060
The girl from the castle

232
00:46:24,620 --> 00:46:26,380
Don't you know it belongs to that monster? 

233
00:46:27,210 --> 00:46:28,400
We do not need charity. 

234
00:46:29,120 --> 00:46:31,490
We just want our rights,  do you understand? 

235
00:46:32,670 --> 00:46:33,850
Our rights!

236
00:46:36,180 --> 00:46:39,590
You did not pay attention to what happened, why?

237
00:46:50,750 --> 00:46:51,980
You swine. 

238
00:46:52,260 --> 00:46:54,850
You should watch out! Go! These idiots all the same.

239
00:47:01,650 --> 00:47:02,690
What are you going to do?

240
00:47:35,810 --> 00:47:39,200
Natario! Natario! 

241
00:47:49,300 --> 00:47:50,820
No more water.

242
00:47:51,970 --> 00:47:53,380
The monster!

243
00:48:09,450 --> 00:48:11,950
Donna Martha has a good hart. 

244
00:48:13,140 --> 00:48:15,160
She's very generous with the gift you give her. 

245
00:48:18,070 --> 00:48:20,920
Natario, the miller brought the necklace back.

246
00:48:22,000 --> 00:48:24,520
He said he doesn't need charity.

247
00:48:42,440 --> 00:48:43,700
Martha! 

248
00:48:53,070 --> 00:48:54,650
You are not wearing the necklace. 

249
00:48:55,830 --> 00:48:56,860
Did you lose it?

250
00:48:57,250 --> 00:48:58,020
No.

251
00:48:59,390 --> 00:49:00,770
What did you do with it? 

252
00:49:05,940 --> 00:49:06,860
Answer me!

253
00:49:07,960 --> 00:49:09,390
I gave it to the miller 

254
00:49:09,400 --> 00:49:11,240
so he could pay the rent.

255
00:49:11,460 --> 00:49:12,860
What?!  

256
00:49:20,760 --> 00:49:22,160
Here Put it on.

257
00:49:30,760 --> 00:49:35,100
You have been seen at the weir last night. 
And you own rent to the Marquis, one year of rent. 

258
00:49:36,090 --> 00:49:37,520
Now it's enough. 

259
00:49:39,110 --> 00:49:40,420
You'll be thrown out.

260
00:50:35,410 --> 00:50:36,530
Martha!  

261
00:51:09,760 --> 00:51:11,070
Camillo! 

262
00:51:12,490 --> 00:51:13,610
did you see Donna Martha?

263
00:51:13,900 --> 00:51:15,200
Donna Martha? 

264
00:51:15,460 --> 00:51:17,380
- She left the house 
- When?

265
00:51:17,700 --> 00:51:19,250
A few hours ago.

266
00:51:20,620 --> 00:51:22,120
Ask my men to search for her.

267
00:51:22,870 --> 00:51:24,020
All right.

268
00:51:38,090 --> 00:51:39,400
Any idea where she could be?

269
00:51:40,880 --> 00:51:42,850
Did you beat her?

270
00:52:08,930 --> 00:52:11,990
One day you'll be able to rule, Donna Amelia

271
00:52:14,850 --> 00:52:17,600
First there has to be a wedding.

272
00:52:17,960 --> 00:52:19,680
That's why I came.

273
00:52:20,160 --> 00:52:22,790
Let's see the debts of Aviedo ten thousand.

274
00:52:25,340 --> 00:52:27,300
And another for Aviedio, six thousand.

275
00:52:29,720 --> 00:52:33,100
But sixteen thousand were was already paid 
How much is outstanding?

276
00:52:33,740 --> 00:52:35,890
Here I have..

277
00:52:36,780 --> 00:52:39,390
I know very well.. if it's a lot, it's too much for me.

278
00:52:39,770 --> 00:52:42,460
And here two creditors: both ten thousand

279
00:52:46,010 --> 00:52:47,560
Thank you so much!

280
00:52:47,910 --> 00:52:51,590
And then here.. two, eight, nine thousand

281
00:52:55,230 --> 00:52:57,470
Every time it's more. 

282
00:52:58,880 --> 00:53:01,580
You promised me to get rid of that slut.

283
00:53:02,900 --> 00:53:05,910
But Donna Amelia, please

284
00:53:06,830 --> 00:53:10,470
You keep telling me he'll throw her out like all the others.

285
00:53:11,640 --> 00:53:12,940
I'm still waiting!!! 

286
00:53:14,010 --> 00:53:16,910
Maybe she will become the Marquise of Roccabruna

287
00:53:18,090 --> 00:53:20,190
There is no reason to get upset. She left already.

288
00:53:23,620 --> 00:53:26,550
- Really?
- Yes, I succeeded

289
00:53:27,300 --> 00:53:29,730
You? Maybe he got tired of here?

290
00:53:30,350 --> 00:53:32,740
Mmm with a helping hand

291
00:53:33,730 --> 00:53:35,820
Well, let's go back to business. 

292
00:53:36,720 --> 00:53:38,220
How much is the total?

293
00:53:38,420 --> 00:53:39,880
A grand total of 35.000.

294
00:53:40,650 --> 00:53:43,130
All right. Tomorrow it will be paid. 

295
00:53:43,340 --> 00:53:44,600
But it's the last time

296
00:53:45,120 --> 00:53:47,160
Your words sound hard, Donna Amelia 

297
00:53:48,130 --> 00:53:50,470
50.000 for a Marquisit's...

298
00:53:51,640 --> 00:53:53,500
A bargain

299
00:54:12,380 --> 00:54:17,400
Sir, we found this in the hills at Campo Verde

300
00:54:18,340 --> 00:54:20,080
- Anything else?
- That's all, Sir.

301
00:54:20,600 --> 00:54:22,450
We really looked everywhere.

302
00:54:22,740 --> 00:54:26,260
If you can't find her I'll have you flogged.

303
00:57:33,600 --> 00:57:35,970
Water water 

304
00:57:42,380 --> 00:57:44,950
The peasants the peasants

305
00:57:47,670 --> 00:57:49,810
Calm down 

306
00:57:51,700 --> 00:57:53,670
calm down 

307
00:57:56,940 --> 00:57:58,710
I am with you.

308
00:58:26,240 --> 00:58:28,250
Come here!  

309
00:58:30,870 --> 00:58:33,650
Dona Martha !
Oh she's hurt.

310
00:58:35,270 --> 00:58:36,770
Caramba!

311
00:58:47,840 --> 00:58:51,990
- I'm coming with you
- You stupid boy!

312
00:58:52,460 --> 00:58:54,040
she's from the castle.

313
01:00:54,060 --> 01:00:57,560
- What do you wish?
- Sir, just a few small things.

314
01:01:02,080 --> 01:01:03,450
These are my promised notes.. 

315
01:01:18,920 --> 01:01:20,750
So Maybe next year if the harvest is good.

316
01:01:22,170 --> 01:01:24,640
I'm afraid 

317
01:01:24,830 --> 01:01:26,540
I cannot wait till next year

318
01:01:30,210 --> 01:01:31,210
What does that mean?

319
01:01:32,820 --> 01:01:35,580
I'll have to foreclose

320
01:01:36,130 --> 01:01:37,960
My dear Mayor

321
01:01:40,710 --> 01:01:42,780
I think there is a solution

322
01:01:44,540 --> 01:01:45,440
Of course! 

323
01:01:45,750 --> 01:01:47,530
But the insult my daughter 

324
01:01:48,050 --> 01:01:50,690
has suffered this

325
01:01:51,090 --> 01:01:53,160
must be made good.

326
01:01:53,860 --> 01:01:54,640
So?

327
01:01:54,700 --> 01:01:57,950
You must keep your promise to marry her. 

328
01:01:58,930 --> 01:02:01,740
But first that woman must go!

329
01:02:02,570 --> 01:02:03,560
Out!

330
01:02:04,220 --> 01:02:06,260
- Sir
- Out!

331
01:02:08,800 --> 01:02:11,060
We'll see who throws whom out

332
01:02:15,610 --> 01:02:17,010
Damn... 

333
01:02:25,470 --> 01:02:27,530
Well, any advice? 

334
01:02:28,870 --> 01:02:30,950
You're always so clever.

335
01:02:32,410 --> 01:02:34,620
There is no advice I can give.

336
01:02:40,820 --> 01:02:42,270
Does he mean it?

337
01:02:43,440 --> 01:02:45,740
Yes..

338
01:02:56,410 --> 01:02:58,560
Giving up Martha no way

339
01:03:01,560 --> 01:03:03,220
Don Sebastian...

340
01:03:05,090 --> 01:03:07,920
You don't have to get rid of Donna Martha. 

341
01:03:09,010 --> 01:03:11,940
- There is a solution
- What kind of solution?

342
01:03:12,300 --> 01:03:14,910
Donna Martha could be married off.

343
01:03:15,790 --> 01:03:17,930
Are you crazy?

344
01:03:19,630 --> 01:03:21,140
We'll find her a husband. 

345
01:03:22,210 --> 01:03:25,250
He doesn't have to be a real husband. 

346
01:03:27,910 --> 01:03:31,440
Pay someone from the village to give his name.

347
01:03:36,820 --> 01:03:39,430
Donna Martha will be near to you. 

348
01:03:40,020 --> 01:03:42,220
It will not bother Donna Amelia

349
01:03:42,670 --> 01:03:45,420
and you will be free of debt.

350
01:03:47,450 --> 01:03:49,380
Not bad. 

351
01:03:52,230 --> 01:03:54,340
That's the solution! 

352
01:03:54,730 --> 01:03:56,980
We'll house the young couple 
in the house with the mill. 

353
01:04:00,290 --> 01:04:02,480
It's not too far away.

354
01:04:16,590 --> 01:04:20,250
Does anyone live up there?

355
01:04:20,800 --> 01:04:22,670
Only the shepherd.

356
01:04:23,190 --> 01:04:26,840
Yes, the shepherd.

357
01:04:27,000 --> 01:04:31,160
His man brought you back

358
01:04:32,100 --> 01:04:33,200
Josefa! 

359
01:04:34,160 --> 01:04:35,430
I want to leave

360
01:04:35,930 --> 01:04:39,280
Wherever you go, you can't escape him.

361
01:04:45,510 --> 01:04:47,670
Let us alone. 

362
01:04:47,970 --> 01:04:50,480
I need to talk to Martha. 

363
01:04:57,790 --> 01:04:59,780
Why did you run away? 

364
01:05:00,530 --> 01:05:03,000
Where you afraid? 
There is no reason for that.

365
01:05:04,820 --> 01:05:08,540
Pleas Sir, let me go.

366
01:05:08,990 --> 01:05:12,070
No, we shall stay together forever. Here.

367
01:05:13,160 --> 01:05:16,570
- Forever?
- Yes. 

368
01:05:19,790 --> 01:05:21,750
You think I'm the wealthy master of Roccabruna.

369
01:05:22,210 --> 01:05:24,850
But I absolutely don't own anything, everything is in pawn. 

370
01:05:25,420 --> 01:05:27,520
A rich marriage is my only way out.

371
01:05:29,400 --> 01:05:31,690
You want to marry? 

372
01:05:32,640 --> 01:05:34,180
Then I'll be free.

373
01:05:34,960 --> 01:05:36,410
You will also marry

374
01:05:37,060 --> 01:05:39,300
- I?
- I've already chosen a husband for you.

375
01:05:40,100 --> 01:05:41,460
Oh no!

376
01:05:41,670 --> 01:05:43,120
It's just a mock marriage. 

377
01:05:43,620 --> 01:05:46,920
You'll bear his name, nothing more. 

378
01:05:47,290 --> 01:05:50,430
In reality you will remain my wife.

379
01:05:50,760 --> 01:05:51,650
And that man?

380
01:05:51,930 --> 01:05:53,620
He gets the mill in return.  

381
01:06:01,690 --> 01:06:03,320
I'll never let you go. 

382
01:06:26,310 --> 01:06:29,430
I'll never let you go. 

383
01:06:30,730 --> 01:06:32,460
Never again. 

384
01:06:33,680 --> 01:06:36,430
You hear? 

385
01:06:37,160 --> 01:06:38,590
I need you. 

386
01:06:44,850 --> 01:06:46,270
You hear? 

387
01:06:47,630 --> 01:06:50,720
I'll rather kill you than let you go.

388
01:08:07,880 --> 01:08:09,350
Pedro! I have news for you.

389
01:08:29,120 --> 01:08:30,030
What's the matter?

390
01:08:30,900 --> 01:08:32,640
Don Sebastian told me to come to you. 

391
01:08:34,170 --> 01:08:37,550
He wants you to become a miller.

392
01:08:38,390 --> 01:08:39,470
What? 

393
01:08:39,930 --> 01:08:41,370
In the lowland? 

394
01:08:41,720 --> 01:08:45,310
- A miller?
- Yes. An order of Don Sebastian. 

395
01:08:46,970 --> 01:08:48,450
And you're getting married,

396
01:08:49,130 --> 01:08:52,170
to the woman his men found in your hut.

397
01:08:53,360 --> 01:08:54,910
What??? What did you say? 

398
01:08:56,830 --> 01:08:59,360
The woman who was in my hut?

399
01:08:59,850 --> 01:09:01,460
Yes, you have to marry her

400
01:09:02,160 --> 01:09:04,470
Have you seen her? Do you know her?

401
01:09:05,020 --> 01:09:05,740
No. 

402
01:09:06,300 --> 01:09:10,970
I was told you saved her, that's all. 
As a reward she'll be your wife.

403
01:09:12,060 --> 01:09:14,910
- You, old man.. are you making fun of me?
- Of course not!

404
01:09:15,340 --> 01:09:17,900
Son Sebastian ordered me to tell this to you.

405
01:09:18,420 --> 01:09:20,740
And I must take your place here.

406
01:09:31,790 --> 01:09:33,360
What's happening? 

407
01:09:38,060 --> 01:09:39,380
Are you out of your mind?

408
01:09:44,780 --> 01:09:45,980
You know Nando, 

409
01:09:47,180 --> 01:09:49,570
I never thought I'd see her again. 

410
01:09:51,120 --> 01:09:53,560
I was walking in Roccabruna. 

411
01:09:54,640 --> 01:09:56,920
At the tavern I heard music and then

412
01:09:57,560 --> 01:09:59,610
I saw her for the first time. 

413
01:10:00,710 --> 01:10:02,360
She was dancing. 

414
01:10:04,240 --> 01:10:05,710
She was so lovely Nando.

415
01:10:12,940 --> 01:10:14,360
I forgot everything. 

416
01:10:16,500 --> 01:10:18,020
And now she's to be my wife? 

417
01:10:19,880 --> 01:10:21,930
Our master is very kind. 

418
01:10:22,450 --> 01:10:24,560
How can I thank him?

419
01:10:25,000 --> 01:10:28,770
Pedro, you do not know the lowland. 

420
01:10:29,310 --> 01:10:31,350
People are different there.

421
01:10:32,530 --> 01:10:33,930
Not good.

422
01:10:34,250 --> 01:10:36,610
Nando I'm not afraid. 

423
01:10:37,920 --> 01:10:39,360
Goodbye.

424
01:12:42,530 --> 01:12:43,800
Don't cry 

425
01:12:45,270 --> 01:12:46,870
don't cry

426
01:12:49,680 --> 01:12:51,480
The bridegroom is downstairs

427
01:12:54,040 --> 01:12:55,830
Send him away

428
01:12:56,080 --> 01:12:57,220
Tell him to wait in front of the church

429
01:12:58,270 --> 01:12:59,460
Yes

430
01:13:20,580 --> 01:13:22,220
I'll come to see you tonight 

431
01:13:24,870 --> 01:13:26,460
tonight.

432
01:15:05,840 --> 01:15:06,570
Martha! 

433
01:15:08,150 --> 01:15:11,010
finally you came.

434
01:16:02,940 --> 01:16:05,550
Friend! A toast to love! 

435
01:16:11,410 --> 01:16:13,750
Three cheers for my beautiful bride!

436
01:17:38,100 --> 01:17:39,300
Martha!

437
01:17:45,240 --> 01:17:47,130
My dear Martha 

438
01:18:01,780 --> 01:18:03,380
I'll make some fire. 

439
01:18:41,090 --> 01:18:42,540
Why don't you look at me? 

440
01:18:45,670 --> 01:18:46,850
Come, sit here next to me.

441
01:18:52,060 --> 01:18:53,000
Leave me alone

442
01:18:57,770 --> 01:18:59,610
Leaving you alone? 

443
01:19:07,620 --> 01:19:09,000
Don't touch me.

444
01:19:09,200 --> 01:19:10,590
But what's the matter? 

445
01:19:18,140 --> 01:19:19,770
Don't you like me?

446
01:19:23,610 --> 01:19:24,940
I despise you

447
01:19:25,720 --> 01:19:27,530
You despise me? 

448
01:19:30,610 --> 01:19:31,690
But why?

449
01:19:32,900 --> 01:19:34,360
You took the mill

450
01:19:34,830 --> 01:19:37,440
I preferred my hut!

451
01:19:40,440 --> 01:19:43,020
Why did you marry me then?

452
01:19:46,770 --> 01:19:48,100
Because I love you

453
01:19:48,880 --> 01:19:50,210
Me?

454
01:19:51,090 --> 01:19:52,690
You love me?

455
01:19:54,650 --> 01:19:57,950
Yes.

456
01:20:19,380 --> 01:20:23,630
She's not yours, stupid guy!

457
01:20:35,970 --> 01:20:37,670
- what did you say?
- Let me go.

458
01:20:37,990 --> 01:20:43,990
Come here.

459
01:20:49,080 --> 01:20:50,700
Beg her pardon. He insulted you.

460
01:20:54,120 --> 01:20:55,420
I beg you let him go

461
01:20:56,280 --> 01:20:58,520
I should let him go?

462
01:20:59,490 --> 01:21:00,660
Please, for my sake

463
01:21:01,970 --> 01:21:02,940
If you want

464
01:21:14,230 --> 01:21:17,510
You fool! You fool!

465
01:22:25,840 --> 01:22:30,120
- Oh I was looking for you
- My dearest Amelia, 

466
01:22:30,680 --> 01:22:32,300
I'm sorry but I have to go

467
01:22:32,690 --> 01:22:34,700
and meet someone for my cattle

468
01:22:36,600 --> 01:22:38,860
But I don't like to be alone without you

469
01:23:19,820 --> 01:23:21,480
Pedro.. Pedro Come in. 

470
01:23:27,440 --> 01:23:30,000
Come, don't leave me alone

471
01:23:54,860 --> 01:23:57,830
But didn't you tell me to leave you alone?

472
01:23:59,080 --> 01:24:00,210
Forget it

473
01:24:09,320 --> 01:24:11,430
And that you despise me? 

474
01:24:13,730 --> 01:24:16,760
Do you know something I don't?

475
01:24:18,220 --> 01:24:19,890
Maybe that's why they all laugh at me?

476
01:24:26,510 --> 01:24:29,860
If you love, don't ask me anything

477
01:24:30,770 --> 01:24:32,400
Whom can I ask? You know something. 

478
01:24:33,420 --> 01:24:35,940
Why are they laughing?

479
01:24:36,810 --> 01:24:38,190
Everyone knows something I don't know. 
But now I want to know:

480
01:24:38,910 --> 01:24:40,310
why are they laughing? 

481
01:24:40,770 --> 01:24:41,980
Why am I a fool? Why? Why? 

482
01:24:55,270 --> 01:24:57,180
You Is there someone else?

483
01:25:06,750 --> 01:25:08,640
Yes 

484
01:28:41,690 --> 01:28:44,170
It's our wedding night

485
01:28:44,400 --> 01:28:46,320
- Let me go!
- That's the way I like you best

486
01:29:00,840 --> 01:29:03,940
Try and take my wife if you dare.

487
01:29:04,150 --> 01:29:06,380
Get out; I'm the master here

488
01:29:08,440 --> 01:29:10,240
You are the wolf

489
01:33:57,910 --> 01:33:59,610
FIN

490
01:34:00,259 --> 01:34:02,726
English Subtitles: 
text from http://users.skynet.be/deneulin/Tieflandst.html
timing by Lena Bolshakova (lena_bolshakova@mail.ru)
time marks mostly from French Subtitles by J.B.P. (2007, ptr4fr@yahoo.fr)


