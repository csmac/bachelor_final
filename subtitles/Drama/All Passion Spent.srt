1
00:02:03,923 --> 00:02:09,884
THE PASSION OF JOAN OF ARC

2
00:02:10,563 --> 00:02:16,433
A film by Carl Th. Dreyer

3
00:03:00,980 --> 00:03:03,813
At the Bibliotheque de la
Chambre des Deputes in Paris

4
00:03:03,983 --> 00:03:07,146
resides one of the most
extraordinary documents

5
00:03:07,320 --> 00:03:09,914
in the history of the world:
The record of the trial of

6
00:03:10,089 --> 00:03:13,252
Joan of Arc, the trial
that ended in her death.

7
00:03:23,269 --> 00:03:26,864
The questions of the judges
and Joan's responses

8
00:03:27,040 --> 00:03:30,476
were recorded exactly.

9
00:03:33,379 --> 00:03:37,281
Reading it, we discover
the real Joan...

10
00:03:37,450 --> 00:03:40,442
not in armor,
but simple and human...

11
00:03:40,587 --> 00:03:43,055
a young woman who died
for her country...

12
00:03:46,526 --> 00:03:49,086
and we are witness to an
amazing drama:

13
00:03:49,262 --> 00:03:52,390
A young, pious woman
confronted by a group

14
00:03:52,732 --> 00:03:56,099
of orthodox theologians and
powerful judges.

15
00:05:29,729 --> 00:05:32,960
I swear to tell the truth,
the whole truth...

16
00:05:35,034 --> 00:05:37,332
nothing but the truth...

17
00:05:59,726 --> 00:06:02,820
In France I'm called Joan...

18
00:06:11,904 --> 00:06:14,998
in my village,
I'm called Jeannette.

19
00:06:20,246 --> 00:06:22,237
How old are you?

20
00:06:32,191 --> 00:06:35,183
Nineteen... I think.

21
00:06:44,003 --> 00:06:46,437
Do you know the Lord's Prayer?

22
00:06:51,878 --> 00:06:54,142
Who taught it to you?

23
00:07:15,234 --> 00:07:16,724
My mother.

24
00:07:24,977 --> 00:07:27,241
Will you recite it?

25
00:07:44,163 --> 00:07:46,859
You claim to be sent by God?

26
00:07:58,544 --> 00:08:00,603
To save France...

27
00:08:07,653 --> 00:08:10,053
it's why I was born.

28
00:08:19,732 --> 00:08:22,428
So you think God hates
the English?

29
00:08:37,149 --> 00:08:41,779
I don't know if God loves
or hates the English...

30
00:08:48,094 --> 00:08:52,053
but I do know that the English
will all be chased from France...

31
00:09:13,953 --> 00:09:16,046
except those that die here!

32
00:09:47,587 --> 00:09:52,251
You have said that St. Michael
appeared to you... in what form?

33
00:09:57,863 --> 00:09:59,728
Did he have wings?

34
00:10:03,102 --> 00:10:05,229
Did he wear a crown?

35
00:10:13,245 --> 00:10:15,611
How was he dressed?

36
00:10:32,765 --> 00:10:36,633
How did you know if it was
a man or a woman?

37
00:10:42,642 --> 00:10:44,439
Was he naked?

38
00:10:56,522 --> 00:11:00,083
Do you think God was unable
to clothe him?

39
00:11:08,000 --> 00:11:10,059
Did he have long hair?

40
00:11:18,144 --> 00:11:20,374
Why would he have cut it?

41
00:11:28,087 --> 00:11:31,318
Why do you wear men's clothing?

42
00:11:36,929 --> 00:11:40,490
If we give you women's clothing,
would you wear it?

43
00:11:58,084 --> 00:12:01,315
When the mission that God has
entrusted to me is over...

44
00:12:01,454 --> 00:12:03,684
I will again dress as a woman.

45
00:12:45,831 --> 00:12:49,562
So it is God who orders you
to dress as a man?

46
00:13:01,213 --> 00:13:05,115
And what reward do you
expect from God?

47
00:13:17,997 --> 00:13:19,965
The salvation of my soul.

48
00:13:29,008 --> 00:13:30,976
You blaspheme God.

49
00:13:59,138 --> 00:14:00,730
This is disgraceful.

50
00:14:05,277 --> 00:14:07,404
For me, she is a saint.

51
00:16:45,437 --> 00:16:47,871
Has God made you promises?

52
00:17:06,425 --> 00:17:09,223
That has nothing to do
with this trial.

53
00:17:18,670 --> 00:17:21,400
Shouldn't we let the judges
decide that?

54
00:17:26,945 --> 00:17:30,073
Shall we put this question
to a vote?

55
00:17:56,708 --> 00:17:59,768
So! What has God promised you?

56
00:18:01,713 --> 00:18:05,205
Perhaps that you will be
delivered from prison?

57
00:18:14,560 --> 00:18:16,084
When?

58
00:18:31,877 --> 00:18:33,868
I know neither the day...

59
00:18:39,284 --> 00:18:40,774
nor the hour.

60
00:19:14,386 --> 00:19:17,651
Since she couldn't be made
to confess easily...

61
00:19:17,789 --> 00:19:20,257
we will have to be clever...

62
00:19:31,470 --> 00:19:35,736
Go find a letter bearing the
signature of King Charles.

63
00:19:45,050 --> 00:19:48,213
I will dictate a letter.

64
00:22:34,653 --> 00:22:36,985
I have great sympathy for you!

65
00:23:05,350 --> 00:23:08,751
Do you know the signature
of your King?

66
00:23:23,902 --> 00:23:26,735
I have a letter from him
for you.

67
00:23:45,424 --> 00:23:47,415
I can't read.

68
00:23:52,097 --> 00:23:54,895
To our beloved Joan...

69
00:24:03,074 --> 00:24:07,511
I am preparing to march on Rouen
with a mighty army.

70
00:24:11,450 --> 00:24:13,714
I send you this devoted priest.

71
00:24:13,852 --> 00:24:15,683
Have confidence in him.

72
00:25:04,236 --> 00:25:06,431
Just as Jesus
is the son of God...

73
00:25:06,571 --> 00:25:08,903
you claim to be
the daughter of God?

74
00:25:13,945 --> 00:25:17,472
Will you recite the
Lord's Prayer?

75
00:25:56,788 --> 00:26:00,724
God has told you that you shall
be delivered from prison?

76
00:26:17,142 --> 00:26:19,133
By a great victory!

77
00:27:01,086 --> 00:27:04,613
God has promised you that
you will go to Heaven?

78
00:27:23,341 --> 00:27:25,707
So you are certain of
being saved?

79
00:27:42,527 --> 00:27:45,724
Be careful,
it's a dangerous answer.

80
00:28:12,524 --> 00:28:15,084
Since you are sure of
your salvation...

81
00:28:15,226 --> 00:28:17,694
you have no need of the Church?

82
00:28:41,720 --> 00:28:43,881
Are you in a state of grace?

83
00:29:22,761 --> 00:29:25,855
Respond!
Are you in a state of grace?

84
00:29:45,884 --> 00:29:49,012
If I am, may God keep me there.

85
00:29:56,261 --> 00:29:59,492
If I am not,
may God grant it to me!

86
00:30:25,590 --> 00:30:27,023
Father...

87
00:30:35,400 --> 00:30:38,130
permit me to attend Mass.

88
00:31:16,741 --> 00:31:19,767
Joan, if we permit you
to attend Mass...

89
00:31:23,781 --> 00:31:26,841
will you stop wearing
men's clothes?

90
00:31:53,945 --> 00:31:57,847
So, you would rather dress
as a man than attend Mass?

91
00:32:02,954 --> 00:32:04,945
These shameless clothes...

92
00:32:10,962 --> 00:32:12,827
abominable to God...

93
00:32:29,981 --> 00:32:32,279
You are no daughter of God...

94
00:32:35,219 --> 00:32:37,449
You are Satan's creature!

95
00:32:43,661 --> 00:32:45,993
Go prepare the torture chamber.

96
00:34:42,113 --> 00:34:44,775
She really looks like
a daughter of God, eh?

97
00:35:32,563 --> 00:35:34,861
The torture chamber.

98
00:36:10,601 --> 00:36:12,592
Regard your judges.

99
00:36:21,712 --> 00:36:24,340
Do you not feel that these
learned doctors

100
00:36:24,482 --> 00:36:26,575
are wiser than you?

101
00:36:34,358 --> 00:36:36,826
But God is even wiser!

102
00:36:47,271 --> 00:36:52,038
Listen, Joan, we know that your
visions do not come from God...

103
00:36:56,247 --> 00:36:58,215
but from the devil.

104
00:37:09,727 --> 00:37:13,493
How do you know a good angel
from an evil angel?

105
00:37:22,473 --> 00:37:27,536
You have knelt before Satan,
not St. Michael!

106
00:37:37,788 --> 00:37:39,949
Don't you see
that it's the Devil

107
00:37:40,124 --> 00:37:42,058
who has turned your head...

108
00:37:46,397 --> 00:37:48,297
who has tricked you...

109
00:37:52,003 --> 00:37:53,595
and betrayed you?

110
00:38:07,551 --> 00:38:11,078
I think she's ready to abjure!

111
00:39:04,041 --> 00:39:06,373
The Church opens its arms
to you...

112
00:39:12,883 --> 00:39:15,784
but if you reject it,
the Church will abandon you...

113
00:39:15,920 --> 00:39:17,649
and you will be alone...

114
00:39:21,359 --> 00:39:22,917
alone!

115
00:39:25,763 --> 00:39:27,663
Yes, alone...

116
00:39:32,436 --> 00:39:34,495
alone, with God!

117
00:40:55,719 --> 00:40:58,517
Even if you part my soul
from my body...

118
00:40:58,656 --> 00:41:00,453
I will confess nothing...

119
00:41:05,196 --> 00:41:08,427
And if I do confess,
later I will say

120
00:41:08,566 --> 00:41:10,898
it was forced from me!

121
00:42:27,745 --> 00:42:30,179
Not for anything in the world
do I want her to die

122
00:42:30,314 --> 00:42:31,747
a natural death.

123
00:42:39,356 --> 00:42:40,880
She is very weak.

124
00:42:47,431 --> 00:42:50,491
She has a fever...
we must bleed her.

125
00:42:55,272 --> 00:42:57,832
Be careful, she could end
her own life...

126
00:42:57,975 --> 00:42:59,806
she's very crafty.

127
00:43:54,932 --> 00:43:57,298
Bring the Sacraments.

128
00:44:51,155 --> 00:44:53,453
Is there something
you wish to tell us?

129
00:45:05,602 --> 00:45:08,765
I fear that I am about to die...

130
00:45:16,213 --> 00:45:20,547
and if I do, I ask you to
bury me in sacred ground.

131
00:45:32,830 --> 00:45:35,492
The Church is merciful...

132
00:45:42,573 --> 00:45:45,701
It always welcomes
the misguided lamb.

133
00:46:05,129 --> 00:46:08,223
Joan, we all want what's
best for you.

134
00:46:08,365 --> 00:46:10,799
Look, I have sent
for the Sacraments.

135
00:46:51,141 --> 00:46:53,632
I am a good Christian.

136
00:47:53,337 --> 00:47:57,706
Do you not know it is the
Body of Christ you refuse?

137
00:48:04,882 --> 00:48:09,649
Do you not see that you
outrage God by your obstinacy?

138
00:49:00,804 --> 00:49:03,898
I love and honor God...
with all my heart.

139
00:49:42,012 --> 00:49:45,573
You claim... that I am...
sent by the devil.

140
00:49:55,192 --> 00:49:56,989
It's not true...

141
00:50:00,764 --> 00:50:04,894
to make me suffer,
the devil has sent you...

142
00:50:09,306 --> 00:50:10,739
and you...

143
00:50:13,744 --> 00:50:15,268
and you...

144
00:50:17,881 --> 00:50:19,473
and you...

145
00:50:33,430 --> 00:50:35,489
There is nothing left
to be done...

146
00:50:35,632 --> 00:50:37,259
alert the executioner!

147
00:52:11,928 --> 00:52:15,830
Let us try one last time
to save this lost soul...

148
00:52:40,924 --> 00:52:43,654
It is to you, Joan,
that I speak.

149
00:52:47,898 --> 00:52:51,163
It is to you that I say
your King is a heretic!

150
00:52:55,639 --> 00:52:59,575
My King is the most noble
Christian of all.

151
00:53:12,355 --> 00:53:15,449
The arrogance of this woman
is insane.

152
00:54:22,726 --> 00:54:26,162
France has never seen
such a monster...

153
00:54:38,208 --> 00:54:41,336
I have never wronged anyone.

154
00:54:54,424 --> 00:54:57,985
If you do not sign,
you will be burned alive...

155
00:55:01,498 --> 00:55:04,058
The stake awaits you...

156
00:55:17,781 --> 00:55:22,343
You don't have the right to die.
Your King still needs you.

157
00:55:40,637 --> 00:55:43,697
Joan, sign...
and save your life!

158
00:55:50,814 --> 00:55:53,749
Joan, we have great
sympathy for you.

159
00:56:00,523 --> 00:56:02,423
Sign, Joan!

160
00:57:37,654 --> 00:57:39,986
In the name of the Lord. Amen.

161
00:57:48,331 --> 00:57:51,266
Having recognized your errors,

162
00:57:51,401 --> 00:57:53,835
you shall not be excommunicated.

163
00:58:01,311 --> 00:58:05,407
But as you have greatly sinned,
we condemn you...

164
00:58:14,123 --> 00:58:16,887
to perpetual imprisonment,
to eat the bread of sorrow

165
00:58:17,026 --> 00:58:19,426
and drink the water of anguish.

166
00:58:29,439 --> 00:58:32,203
A good day's work:
You have saved

167
00:58:32,375 --> 00:58:34,434
your life and your soul.

168
00:59:07,977 --> 00:59:10,707
She has only made fools of you!

169
00:59:16,886 --> 00:59:18,683
Long live Joan!

170
01:01:10,767 --> 01:01:13,167
Go find the judges!

171
01:01:16,239 --> 01:01:18,901
I take it back, I have lied...

172
01:01:21,744 --> 01:01:23,439
Hurry!

173
01:02:08,658 --> 01:02:10,888
I have committed a great sin...

174
01:02:34,117 --> 01:02:36,881
I have denied God...
to save my life.

175
01:02:42,625 --> 01:02:45,594
But Joan, you have confessed
before everyone

176
01:02:45,728 --> 01:02:48,288
that the devil misled you.

177
01:03:02,445 --> 01:03:06,006
You still believe you were
sent by God?

178
01:03:24,600 --> 01:03:27,228
Her answer will bring her death.

179
01:04:02,505 --> 01:04:05,531
Everything I said was for
fear of the stake.

180
01:04:15,351 --> 01:04:17,717
Have you anything else
to tell us?

181
01:05:39,368 --> 01:05:42,701
We have come to prepare you
for death.

182
01:05:57,119 --> 01:05:59,587
Now... already?

183
01:06:09,832 --> 01:06:11,322
How am I to die?

184
01:06:16,272 --> 01:06:18,297
At the stake.

185
01:06:29,618 --> 01:06:31,813
I'll bring the last Sacrament.

186
01:06:40,262 --> 01:06:45,222
How can you still believe
that you are sent by God?

187
01:06:50,473 --> 01:06:53,203
His ways are not our ways.

188
01:07:04,253 --> 01:07:06,517
Yes, I am His child.

189
01:07:17,666 --> 01:07:20,066
And the great victory?

190
01:07:29,045 --> 01:07:30,979
...my martyrdom!

191
01:07:36,318 --> 01:07:38,309
...and your deliverance?

192
01:07:58,607 --> 01:08:00,131
...death!

193
01:08:23,099 --> 01:08:25,090
Will you take confession?

194
01:09:34,170 --> 01:09:39,107
May the Body of Our Lord
Jesus Christ keep my soul...

195
01:09:53,856 --> 01:09:57,019
unto life everlasting. Amen.

196
01:11:05,461 --> 01:11:09,921
Be courageous, Joan.
Your last hour approaches.

197
01:12:33,182 --> 01:12:37,118
Dear God, I accept
my death gladly...

198
01:12:55,437 --> 01:12:59,066
but do not let me suffer
too long.

199
01:13:03,979 --> 01:13:07,574
Will I be with You tonight
in Paradise?

200
01:18:07,082 --> 01:18:08,947
Jesus!

201
01:18:17,059 --> 01:18:19,357
You have burned a saint!

202
01:21:16,972 --> 01:21:20,533
The flames sheltered
Joan's soul...

203
01:21:20,675 --> 01:21:23,644
as it rose to heaven -

204
01:21:23,778 --> 01:21:27,111
Joan whose heart has become
the heart of France...

205
01:21:27,249 --> 01:21:30,013
Joan, whose memory
will always be cherished...

206
01:21:30,151 --> 01:21:32,346
by the people of France.

