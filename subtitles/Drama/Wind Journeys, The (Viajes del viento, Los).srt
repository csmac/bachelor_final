﻿1
00:00:08,470 --> 00:00:12,800
You bad guy, you are here.
Give me my cloth!

2
00:00:16,780 --> 00:00:19,340
- What's going on?
- There's something going on.

3
00:00:20,320 --> 00:00:24,050
Give me back my cloth!
Aren't you going to? Give me my cloth!

4
00:00:25,390 --> 00:00:26,820
Why are you doing this.

5
00:00:26,920 --> 00:00:30,120
- He stole my clothes.
- What do you mean?

6
00:00:30,230 --> 00:00:31,620
Move.

7
00:00:36,700 --> 00:00:39,790
Is that tru?

8
00:00:40,970 --> 00:00:42,990
Here, here!

9
00:00:47,080 --> 00:00:48,740
You strange fellow!

10
00:00:49,080 --> 00:00:52,050
How dare you harass women!

11
00:00:52,410 --> 00:00:53,940
Aren't you embarrased as a man?

12
00:00:54,150 --> 00:00:55,080
Original work / Lee Jung Myung

13
00:00:56,420 --> 00:00:58,410
Scenario / Lee Eun Yeong
My wife is

14
00:00:58,550 --> 00:01:01,390
Producer / Jang Tae Yoo, Jin Hyuk
very weak

15
00:01:01,620 --> 00:01:04,560
and she wanted to come out and draw

16
00:01:04,930 --> 00:01:09,950
and I was trying to protect her, is that a problem?

17
00:01:10,330 --> 00:01:11,390
Is it?

18
00:01:14,670 --> 00:01:19,440
No!
He stole my clothes.

19
00:01:19,640 --> 00:01:21,400
He's a man.

20
00:01:23,110 --> 00:01:26,710
It's a she, you might be misunderstading.

21
00:01:27,380 --> 00:01:29,250
Take her cloth off! Then you will know!

22
00:01:29,350 --> 00:01:33,480
Why are you doing this to my weak wife.
Are you alright my darling?

23
00:01:33,860 --> 00:01:37,480
- I am feeling dizzy.
- Oh my, oh my.

24
00:01:37,960 --> 00:01:41,620
I told you not to move too much.

25
00:01:41,830 --> 00:01:44,920
I said I don't need this.
Let's go.

26
00:01:47,900 --> 00:01:48,630
Look at that!

27
00:01:49,900 --> 00:01:51,570
Run, run!

28
00:01:52,940 --> 00:01:54,810
Artist! I will keep this!!

29
00:01:56,410 --> 00:01:57,340
Stop there!!

30
00:02:00,820 --> 00:02:01,800
Oh my god!!

31
00:02:04,020 --> 00:02:09,620
My cloth! My cloth! What shall I do!
That's my new one.

32
00:02:09,860 --> 00:02:12,520
Hey, this is my new cloth too.
Stand up.

33
00:02:18,230 --> 00:02:21,290
You, you are that guy just now.

34
00:02:21,670 --> 00:02:23,140
Hey! Hey! Hey!

35
00:02:34,920 --> 00:02:37,410
- Let's go together, together!
- Hurry up.

36
00:02:42,120 --> 00:02:48,220
Episode 5 Women on swing

37
00:02:54,540 --> 00:02:55,630
Don't look.

38
00:02:55,770 --> 00:02:57,740
I won't, I won't.

39
00:02:58,610 --> 00:03:02,510
Why are you being so shy?
I won't see even if you beg me.

40
00:03:14,320 --> 00:03:18,020
Ah, sir!
You said you won't look!

41
00:03:19,330 --> 00:03:21,990
Why do you think I would look!

42
00:03:23,130 --> 00:03:25,960
Hurry up, the sun will rise soon.

43
00:03:29,170 --> 00:03:30,260
Anyway why are your...

44
00:03:31,040 --> 00:03:31,870
What?

45
00:03:33,680 --> 00:03:38,110
bones so thin?

46
00:03:39,550 --> 00:03:41,070
You look like a woman.

47
00:03:45,790 --> 00:03:47,520
No, no way!

48
00:03:47,620 --> 00:03:53,580
I meant...that you could have got me
when you said you were a woman!

49
00:03:54,630 --> 00:03:59,330
Who will marry a woman like you?

50
00:03:59,470 --> 00:04:02,930
Ah, really?

51
00:04:03,500 --> 00:04:07,440
Who will even look at you
when you are a woman?

52
00:04:07,610 --> 00:04:09,770
What about me?
Hey, what's wrong with me!

53
00:04:21,390 --> 00:04:26,330
I am very curious.
Finish up fast and show me.

54
00:04:31,630 --> 00:04:37,800
The main chracter is not here.

55
00:04:40,780 --> 00:04:44,940
It's until five o'clock tomorrow.
Can you finish it?

56
00:04:52,690 --> 00:04:58,850
Where are you going to draw it?

57
00:05:00,530 --> 00:05:03,960
I have somewhere to go.

58
00:05:05,100 --> 00:05:11,130
Okay! Then go there and fill it
in with what you want.

59
00:05:11,470 --> 00:05:12,670
Yes, sir.

60
00:06:08,830 --> 00:06:14,700
Alright! Good!
Well done.

61
00:06:18,210 --> 00:06:19,800
Keep concentrating!

62
00:06:21,980 --> 00:06:27,780
Don't even try to be lazy because all those papers,

63
00:06:28,250 --> 00:06:30,740
brushes, ink, and colors are made from
the citizens' hard work. Alright?

64
00:06:31,090 --> 00:06:32,020
Yes!

65
00:06:33,820 --> 00:06:34,750
Sir.

66
00:06:35,660 --> 00:06:36,880
Are you here, Yoo choon?

67
00:06:36,990 --> 00:06:38,320
Yes, you can leave now.

68
00:06:38,960 --> 00:06:40,020
Okay, take care of the exam.

69
00:06:57,710 --> 00:07:01,340
I can see some details there.

70
00:07:03,890 --> 00:07:05,150
Good job.

71
00:07:06,220 --> 00:07:07,080
Yes.

72
00:07:19,100 --> 00:07:21,690
Hyo Won, hasn't Yoon Bok arrived yet?

73
00:07:26,740 --> 00:07:31,740
If he doesn't come, you will be the artist, right?

74
00:07:32,910 --> 00:07:33,850
What do you mean?

75
00:07:34,820 --> 00:07:39,580
Aren't I right? Yoon Bok looks like a woman but

76
00:07:39,750 --> 00:07:41,950
still he's the only guy who can compete with you...

77
00:07:42,090 --> 00:07:43,780
Shut your mouth!

78
00:07:44,760 --> 00:07:48,290
Alright, I will, shh, shh...

79
00:08:10,050 --> 00:08:13,710
You were alive! Sir Dan Won!

80
00:08:14,320 --> 00:08:15,760
I told you he is...

81
00:08:15,990 --> 00:08:19,360
Duk Bong! Duk Bong! Stand up.

82
00:08:20,530 --> 00:08:21,260
Duk Bong!

83
00:08:21,360 --> 00:08:25,770
He was such a coward and I had a hard time

84
00:08:25,970 --> 00:08:27,590
bringing him here.

85
00:08:27,700 --> 00:08:29,730
Sit down, sit.

86
00:08:29,970 --> 00:08:31,340
You stay out for a while.

87
00:08:32,010 --> 00:08:35,600
Sit, sit, sit.

88
00:08:38,780 --> 00:08:42,220
Why do you look so bad?
How did you stay?

89
00:08:42,650 --> 00:08:45,590
I wandered around.

90
00:08:47,790 --> 00:08:52,380
You must have had a hard time.
Did you hear about lL Wol party?

91
00:08:53,530 --> 00:08:57,560
Are you going to send him back to Pyeongyang?
It will take time to prepare a horse...

92
00:08:57,770 --> 00:09:00,060
Hey! Stay outside!

93
00:09:01,200 --> 00:09:03,140
I said you need to tell me earlier!

94
00:09:03,300 --> 00:09:05,240
It takes time to get a horse.

95
00:09:05,370 --> 00:09:09,210
Did you hear about lL Wol party?

96
00:09:09,710 --> 00:09:11,230
He is alive.

97
00:09:12,350 --> 00:09:14,040
Alive, who is alive?

98
00:09:15,720 --> 00:09:21,660
I've gathered informations after
the teacher died, and...

99
00:09:28,800 --> 00:09:33,820
What? His kid is alive?

100
00:09:34,070 --> 00:09:42,480
Yes, but I couldn't find where he is.

101
00:09:44,050 --> 00:09:47,450
You mean, the kid is alive?
Are you sure?

102
00:09:47,580 --> 00:09:56,580
Yes, he is.
Sir, I'm afraid.

103
00:09:57,960 --> 00:10:00,450
I am still so afraid.

104
00:10:02,260 --> 00:10:07,460
Don't worry, don't worry.
I will solve this, don't worry.

105
00:10:12,540 --> 00:10:17,770
Myo' is for cat and 'Mo' is for a 70 year old man

106
00:10:18,010 --> 00:10:19,980
and it is pronounced as 'Mao'.

107
00:10:20,680 --> 00:10:24,340
A cat which means 70 years old is looking at
the butterfly which means 80 years old

108
00:10:24,420 --> 00:10:26,940
and this must mean long life.

109
00:10:27,420 --> 00:10:30,390
High official / Kim Si Up
Yes! It is!

110
00:10:31,460 --> 00:10:32,930
Yes it is.

111
00:10:33,860 --> 00:10:37,320
Kim Gwee Joo / Brother of Queen Jung Soon
How is it?

112
00:10:37,530 --> 00:10:40,760
Would you give that picture to Sir Si Up?

113
00:10:42,400 --> 00:10:47,170
I know that you have good taste

114
00:10:48,040 --> 00:10:52,910
but you still couldn't find out who drew it.

115
00:10:53,910 --> 00:11:00,050
It's not important to know the artist's name!

116
00:11:00,450 --> 00:11:02,050
Don't you think so?

117
00:11:02,860 --> 00:11:06,420
Then, cheers for everyhing!

118
00:11:06,860 --> 00:11:07,850
- Yes.
- Yes.

119
00:11:08,160 --> 00:11:10,430
Can I find out the meaning?

120
00:11:12,800 --> 00:11:14,670
Can you?

121
00:11:15,100 --> 00:11:17,660
Kim Jo Nyun / The head of the market

122
00:11:29,850 --> 00:11:34,550
This is a China pink flower.
And it means 'youth'.

123
00:11:35,660 --> 00:11:39,520
And here, at the bottom, it's a violet.

124
00:11:40,290 --> 00:11:46,200
Why would have the artist drawn this
two flowers at once

125
00:11:46,930 --> 00:11:49,630
when the China pink blooms at early spring,
and violet at early summer?

126
00:11:50,640 --> 00:11:54,940
That's because there is a special meaning.

127
00:11:56,010 --> 00:12:00,240
The old people called the violet a 'wish flower'.

128
00:12:00,950 --> 00:12:06,610
And so this picture means that he wants you to

129
00:12:06,790 --> 00:12:10,190
Iive long and be healthy as young people

130
00:12:10,660 --> 00:12:17,500
and also he wants all your dream to come true.

131
00:12:22,100 --> 00:12:23,130
Lastly...

132
00:12:25,210 --> 00:12:32,640
The slanting line of the flower, the cat on the right,
and the butterfly is also acting in concert.

133
00:12:33,350 --> 00:12:35,040
They seem to be drawn

134
00:12:35,120 --> 00:12:37,640
Hwamyorongjupdo/Kim Hong Do
without plans but it's a very skillful work

135
00:12:37,720 --> 00:12:44,390
and the lively picture is for sure
Dan Won's work!

136
00:12:44,530 --> 00:12:48,460
- What?
- Dan Won?

137
00:12:49,600 --> 00:12:51,190
Tear the paper out.

138
00:12:59,670 --> 00:13:03,540
Dan Won? This is Dan Won's picture?

139
00:13:05,080 --> 00:13:09,040
Dan Won?

140
00:13:09,220 --> 00:13:15,920
Then would I give any work to you
as a birthday present

141
00:13:16,390 --> 00:13:20,160
infront of all the high officials?

142
00:13:22,500 --> 00:13:23,900
You have a great taste.

143
00:13:24,000 --> 00:13:25,430
Thank you.

144
00:13:26,400 --> 00:13:30,600
You've allowed me to show my talent today

145
00:13:30,740 --> 00:13:34,640
so would you allow me to pay for tonight's meal?

146
00:13:35,110 --> 00:13:40,340
You speak only beautiful words now.

147
00:13:41,320 --> 00:13:42,650
Don't you think so?

148
00:13:42,850 --> 00:13:44,370
The head of the market...

149
00:13:45,450 --> 00:13:48,420
Is he Kim Jo Nyun, the head of the market.

150
00:13:48,660 --> 00:13:53,390
He's the seller who payed sir Woo Sang.

151
00:13:54,960 --> 00:13:57,450
You look quite like a upper class.

152
00:13:58,070 --> 00:14:02,090
I've prepared this dinner in order to thank you

153
00:14:02,340 --> 00:14:05,100
for ruling the country peacefully and helping me

154
00:14:05,440 --> 00:14:10,430
to sell things well.

155
00:14:10,540 --> 00:14:12,640
So, please enjoy.

156
00:14:13,510 --> 00:14:16,070
Let's enjoy.

157
00:14:45,310 --> 00:14:48,250
Mak Nyun, where's Jung Hyang?

158
00:14:48,550 --> 00:14:52,680
Where is she when all the high officials are here!

159
00:14:52,850 --> 00:14:55,750
Er...special guests are here.

160
00:14:56,620 --> 00:14:58,090
Special guests?

161
00:15:32,490 --> 00:15:37,360
Please come inside this picture.

162
00:15:41,570 --> 00:15:45,440
What do I have to do?
To go in to that picture.

163
00:15:46,410 --> 00:15:56,040
Show me everyhing.
Everyhing that's hidden inside you.

164
00:16:00,920 --> 00:16:14,290
Your heart, spirit, firmness, and the music.

165
00:21:32,890 --> 00:21:36,650
Why do you look absent minded?

166
00:21:39,030 --> 00:21:40,760
I've lost my way for a while.

167
00:21:41,760 --> 00:21:45,460
I'm quite sure that you've seen
something that you desire.

168
00:21:48,070 --> 00:21:50,090
How dare you speak like that?

169
00:21:51,140 --> 00:21:54,770
Then I should apologize.

170
00:21:56,810 --> 00:22:03,940
But, you and I have the eye that others don't have,
don't we?

171
00:22:05,750 --> 00:22:06,580
What is it?

172
00:22:07,890 --> 00:22:12,920
The eye that notices something most valuable.

173
00:22:16,700 --> 00:22:19,100
Inform me anytime.

174
00:22:22,840 --> 00:22:28,300
If you get to have that girl inside the room,

175
00:22:29,310 --> 00:22:35,150
it will be your most flourescent possession.

176
00:22:53,530 --> 00:22:58,840
Now do I get to live...
inside your picture?

177
00:23:02,340 --> 00:23:11,680
Then...if you pass the exam, can l...
Live only inside your heart?

178
00:23:15,260 --> 00:23:19,950
In my heart...

179
00:23:47,450 --> 00:23:53,150
What kind of a man can reject you?

180
00:23:56,600 --> 00:24:08,030
And, what kind of a man act harsh on you?

181
00:24:37,200 --> 00:24:41,070
Jung Jo/22nd King of Josun

182
00:25:01,660 --> 00:25:02,790
My father the King.

183
00:25:05,400 --> 00:25:17,900
Grandfather, please save my father.
Grandfather, please save my father.

184
00:25:18,380 --> 00:25:20,180
Move aside!

185
00:25:21,180 --> 00:25:37,860
Grandfather! Grandfather!
Grandfather! Grandfather!

186
00:26:14,100 --> 00:26:18,660
Please, please wait a little more.

187
00:26:40,090 --> 00:26:43,760
The sleeves look like bended bows

188
00:26:55,180 --> 00:27:02,840
The sleeves look like bended bows

189
00:27:04,350 --> 00:27:11,450
since those feet are rolling in the air.

190
00:28:19,990 --> 00:28:23,220
You must be the artist that was chased out
from the art department.

191
00:28:23,700 --> 00:28:25,290
Shin Yeong Bok / Shin Yoon Bok's elder brother.
Who are you?

192
00:28:26,430 --> 00:28:27,830
Huh Ok / Niece of the senior in color department.
What can we do!

193
00:28:27,970 --> 00:28:32,670
Someone who breaks the mortar will not be able to
come to the color mixing room.

194
00:28:34,010 --> 00:28:34,800
What's wrong with you?

195
00:28:37,980 --> 00:28:42,410
I can fill in a mortar without anyone knowing.

196
00:28:42,650 --> 00:28:46,140
Who are you and why do you have that key?

197
00:28:48,390 --> 00:28:50,550
You are under sir Baek Baek right?

198
00:28:51,930 --> 00:28:57,860
Then didn't you hear from him that
he has a beautiful niece?

199
00:28:59,530 --> 00:29:00,620
I didn't.

200
00:29:03,740 --> 00:29:07,100
That's bad.

201
00:29:09,340 --> 00:29:10,330
Wait.

202
00:29:16,820 --> 00:29:17,510
Why?

203
00:29:18,990 --> 00:29:24,480
How can you fill in the mortar I broke?

204
00:29:32,870 --> 00:29:37,330
I saw a secret and I think I should make one.

205
00:29:38,640 --> 00:29:39,370
A secret?

206
00:29:45,550 --> 00:29:46,670
What are you doing?

207
00:29:48,720 --> 00:29:50,950
We now share a secret.

208
00:29:51,920 --> 00:29:52,610
Let's go.

209
00:30:57,220 --> 00:31:02,420
How come I can't see a dust?
Does someone really live here?

210
00:31:03,020 --> 00:31:05,320
Aren't you abusing Myung too much?

211
00:31:06,190 --> 00:31:08,090
Suh Jing / Kim Hong Do's friend
Do you envy?

212
00:31:09,460 --> 00:31:13,420
If you do, go find a girl.
Don't fuss around here.

213
00:31:15,870 --> 00:31:17,430
Are you making a joke again?

214
00:31:17,600 --> 00:31:18,730
Yoon, you are here.

215
00:31:18,910 --> 00:31:21,600
- Uncle!
- Yes, yes, yes, here!

216
00:31:22,310 --> 00:31:23,170
Did you pluck some flowers?

217
00:31:24,940 --> 00:31:25,810
Are you giving it to me?

218
00:31:27,850 --> 00:31:29,540
- Do you want some radish?
- Is it delicious?

219
00:31:29,720 --> 00:31:30,740
Yes, yes, here.

220
00:31:33,020 --> 00:31:34,650
- It's nice right?
- Yes.

221
00:31:34,790 --> 00:31:35,480
Suh Yoon / Suh Jing's daughter

222
00:31:35,590 --> 00:31:36,280
Do you want more?

223
00:31:50,240 --> 00:31:51,900
Where are you?
Yoon.

224
00:32:55,700 --> 00:33:00,040
Where's this place?

225
00:33:33,440 --> 00:33:34,130
My picture!

226
00:33:39,450 --> 00:33:40,500
My picture!

227
00:33:43,980 --> 00:33:44,920
My picture!

228
00:33:49,760 --> 00:33:53,750
Hello! Anyone there!

229
00:33:54,530 --> 00:33:57,930
Hello! Help me!

230
00:34:02,140 --> 00:34:06,540
Help me! Hello!

231
00:34:13,550 --> 00:34:18,750
Hello! It's inside the well!
Help me!

232
00:34:24,160 --> 00:34:26,890
Hello! I'm inside the well!

233
00:34:29,360 --> 00:34:33,990
Help me! Hello!

234
00:34:40,040 --> 00:34:43,770
Is it true that there's a ghost in the well?

235
00:34:44,080 --> 00:34:48,310
No way.
Let's go...

236
00:35:05,200 --> 00:35:08,330
Inside the well! Look inside the well!

237
00:35:15,140 --> 00:35:16,630
Help me!

238
00:35:41,530 --> 00:35:42,800
What do you want?

239
00:35:43,240 --> 00:35:49,070
I just remembered myself taking the
artist exam...

240
00:35:49,910 --> 00:35:50,900
Goodbye.

241
00:36:03,860 --> 00:36:04,980
Sir, sir Dan Won?

242
00:36:05,590 --> 00:36:07,390
Where have you been this late?

243
00:36:07,690 --> 00:36:09,850
I've ran an errand.

244
00:36:11,430 --> 00:36:12,900
I have a job to complete...

245
00:36:30,950 --> 00:36:32,010
Good job.

246
00:36:39,060 --> 00:36:44,290
Now Dan Won can no longer stay here.

247
00:36:58,280 --> 00:36:59,340
When did he leave?

248
00:37:00,610 --> 00:37:07,610
It's been hours since he left, and he still didn't
arrive at the art department?

249
00:37:08,750 --> 00:37:10,280
Don't you really know?

250
00:37:11,160 --> 00:37:12,280
I don't know.

251
00:37:13,790 --> 00:37:18,420
Would you tell me if I pay for some drinks?

252
00:37:18,860 --> 00:37:22,130
I am really worried too.

253
00:37:33,580 --> 00:37:38,980
Wow, I feel...
I feel so happy because of Soon Shim.

254
00:38:00,540 --> 00:38:02,800
There's not much time left.

255
00:38:14,150 --> 00:38:15,250
Yoon Bok!

256
00:38:42,750 --> 00:38:51,450
Yoon Bok! Yoon Bok!

257
00:38:57,230 --> 00:39:04,660
Sir, sir, sir.

258
00:39:06,510 --> 00:39:11,640
Sir! Here, sir!

259
00:39:16,920 --> 00:39:17,940
Yoon Bok!

260
00:39:19,450 --> 00:39:25,860
Sir, Yoon Bok's here, sir!

261
00:39:34,100 --> 00:39:35,330
Yoon Bok!

262
00:39:35,970 --> 00:39:39,270
Sir, sir.

263
00:39:56,260 --> 00:39:58,160
What are you doing here this late?

264
00:39:58,760 --> 00:40:03,530
Haven't you seen a little student passing by?

265
00:40:03,660 --> 00:40:04,490
I didn't.

266
00:40:04,830 --> 00:40:07,360
You almost surprised me to death.

267
00:40:07,530 --> 00:40:09,900
It's late in the night.
Go to your house!

268
00:40:14,140 --> 00:40:20,050
Here! The well, sir!
It's the well, sir!

269
00:40:36,760 --> 00:40:38,290
Sir!

270
00:40:41,370 --> 00:40:43,340
- lnside the well.
- Is it Yoon Bok?

271
00:40:45,540 --> 00:40:46,770
Sir!!

272
00:40:49,110 --> 00:40:53,910
Hey! Hey!
I found him! Come here!

273
00:40:56,680 --> 00:40:58,450
Sir!

274
00:41:00,290 --> 00:41:01,220
Yoon Bok!

275
00:41:02,020 --> 00:41:03,820
- Pass me that.
- Oh my.

276
00:41:04,760 --> 00:41:06,420
Yoon Bok! Are you alright?

277
00:41:06,590 --> 00:41:08,060
Sir.

278
00:41:10,000 --> 00:41:13,620
Yoon Bok! What are you doing here?
Are you okay?

279
00:41:17,570 --> 00:41:18,760
Where are you going?

280
00:41:19,640 --> 00:41:20,660
Don't do it, don't do it.

281
00:41:24,740 --> 00:41:26,610
Please pass me that bucket.

282
00:41:27,050 --> 00:41:28,810
- Yes.
- Bring it.

283
00:41:47,470 --> 00:41:48,430
Are you alright?

284
00:41:50,140 --> 00:41:51,530
What are you doing here?

285
00:41:51,640 --> 00:41:52,970
Sir.

286
00:41:57,180 --> 00:41:59,700
Yoon Bok, Yoon Bok are you alright?

287
00:42:00,480 --> 00:42:01,210
Yes.

288
00:42:01,380 --> 00:42:03,210
There's no time, hurry up.

289
00:42:08,920 --> 00:42:09,850
Let me see.

290
00:42:24,440 --> 00:42:27,130
It's quite bad, get on my back.

291
00:42:29,040 --> 00:42:30,510
- Sir.
- Now.

292
00:42:30,710 --> 00:42:32,230
- Sir.
- Hurry up.

293
00:42:33,050 --> 00:42:34,170
Sir.

294
00:42:48,030 --> 00:42:49,290
- Get on my back.
- Sir.

295
00:42:49,460 --> 00:42:52,290
- Now.
- Sir.

296
00:43:10,220 --> 00:43:11,150
Now, pull.

297
00:43:22,630 --> 00:43:23,690
Hold on tight.

298
00:43:52,060 --> 00:43:53,250
- Thank you.
- Are you alright?

299
00:43:53,990 --> 00:43:57,620
Thank you, thank you, thank you.

300
00:43:59,530 --> 00:44:01,620
There's no time.
Let's go now.

301
00:44:01,770 --> 00:44:03,100
There's no need.

302
00:44:04,240 --> 00:44:05,130
What?

303
00:44:06,810 --> 00:44:08,240
It's all over.

304
00:44:09,170 --> 00:44:10,070
What do you mean?

305
00:44:13,750 --> 00:44:22,650
Everyhing's gone.
The picture is gone.

306
00:44:32,130 --> 00:44:33,930
Thank you.

307
00:44:35,200 --> 00:44:37,960
- Okay.
- Why did he go in there...

308
00:44:40,110 --> 00:44:43,130
What shall I do? Sir.

309
00:44:50,220 --> 00:44:54,210
Let's do this,
Yoon Bok, let's do this.

310
00:44:57,720 --> 00:45:02,520
Everyhing's gone.
The picture is gone.

311
00:45:09,100 --> 00:45:12,400
What do you see? Huh? Just tell me.

312
00:45:12,740 --> 00:45:18,370
Imagine anything!
A swing, a woman on the swing, the fall.

313
00:45:18,510 --> 00:45:25,540
The sound of the wind, water, rock, tree, smell
Anything's fine, anything.

314
00:45:33,990 --> 00:45:37,290
Now, close your eyes.
Close your eyes.

315
00:45:46,340 --> 00:45:48,310
Anything's fine.
Imagine anything.

316
00:45:49,370 --> 00:45:54,180
What you see, what you hear, taste, smell.

317
00:45:54,550 --> 00:45:58,610
Everyhing's connected so if you can remember one,
all the others will follow.

318
00:45:58,750 --> 00:46:01,650
Think of one thing.
Can you see?

319
00:46:02,960 --> 00:46:04,180
Can you?

320
00:46:05,860 --> 00:46:09,490
Concentrate.
Can you see something? Can you?

321
00:46:40,890 --> 00:46:41,830
Yes.

322
00:46:44,100 --> 00:46:44,860
Good.

323
00:47:22,370 --> 00:47:24,770
Yoon Bok is still not here.

324
00:47:29,910 --> 00:47:33,040
It's a pleasant morning.

325
00:48:04,940 --> 00:48:05,880
Can you do it?

326
00:48:22,060 --> 00:48:23,960
Can you finish it in time?

327
00:48:24,960 --> 00:48:28,460
If he is late, we won't wait even for a second.

328
00:51:06,930 --> 00:51:10,660
Look at that inordinate...
He's a bad fellow!

329
00:51:29,650 --> 00:51:30,740
You there!

330
00:51:31,920 --> 00:51:32,820
Sir!

331
00:51:33,220 --> 00:51:36,090
Your brother was chased out
for a obscene picture

332
00:51:36,720 --> 00:51:39,090
and you are drawing a group of women!

333
00:51:39,860 --> 00:51:43,760
Are you brothers trying contempt the department!
Get out!

334
00:51:44,200 --> 00:51:47,860
Sir! No, sir.
That's not an obscene picture!

335
00:51:48,170 --> 00:51:49,260
Then what is it!

336
00:51:49,840 --> 00:51:55,540
That's women breathing, talking and shouting!

337
00:51:55,740 --> 00:52:01,110
That's women on Dano Day and it's not an
obscene picture!

338
00:52:01,350 --> 00:52:04,250
Talk about it to someone else.

339
00:52:04,350 --> 00:52:07,220
Sir! Sir.

340
00:52:07,350 --> 00:52:08,580
Sir!

341
00:52:11,120 --> 00:52:15,560
If you chase him out,
he won't be able to take the exam.

342
00:52:16,290 --> 00:52:18,820
The King is watching this exam.

343
00:52:19,330 --> 00:52:22,360
It won't be late to judge his picture later!

344
00:52:23,740 --> 00:52:24,860
Don't you think so?

345
00:52:37,850 --> 00:52:40,550
There's only one time bell left.

346
00:52:41,420 --> 00:52:47,420
Afterthat, his fate will be on strict judging!
Do you get it?

347
00:52:58,370 --> 00:52:59,930
Finish it.

348
00:54:11,940 --> 00:54:12,910
The nipples!

349
00:54:49,910 --> 00:54:51,280
The nipples!

350
00:55:05,960 --> 00:55:09,300
What are you doing?
End the picture.

351
00:55:11,400 --> 00:55:15,700
Hurry! Hurry!

352
00:55:59,750 --> 00:56:02,310
- Did you finish?
- Not yet.

353
00:56:31,150 --> 00:56:32,880
Time's up!

354
00:56:33,750 --> 00:56:35,480
Sir, I finished it!

355
00:56:40,960 --> 00:56:41,650
Well done!

356
00:56:44,430 --> 00:56:47,160
Well done!
Why did you make me so nervous?

357
00:56:47,630 --> 00:56:48,960
I payed foryourfood!

358
00:56:49,100 --> 00:56:51,360
You've worked hard.

359
00:57:19,160 --> 00:57:20,430
Well done.

360
00:57:21,300 --> 00:57:22,930
Yes...he did.

361
00:57:24,400 --> 00:57:29,640
But don't forget that this is the art department.

362
00:57:30,810 --> 00:57:34,470
You must know what this means right?

363
00:57:53,830 --> 00:57:55,960
This is quite disturbing.

364
00:57:56,130 --> 00:58:00,090
The women are calm while they are naked

365
00:58:01,170 --> 00:58:03,640
and the look on theirface are dirty.

366
00:58:04,340 --> 00:58:11,440
And the impression of the men watching fits
with sundry goods at the market.

367
00:58:11,820 --> 00:58:14,680
And, look at that color.

368
00:58:16,050 --> 00:58:17,180
What about the color?

369
00:58:18,760 --> 00:58:20,310
There's too much color.

370
00:58:20,430 --> 00:58:25,450
Is there any reason that we
can't use too much color?

371
00:58:26,000 --> 00:58:29,300
How come you are interferring?

372
00:58:30,500 --> 00:58:37,810
You should resist and control the colors,
and if it's too much like this,

373
00:58:38,410 --> 00:58:42,680
don't you think it becomes into a shallow picture?

374
00:58:43,850 --> 00:58:45,210
Why?

375
00:58:45,850 --> 00:58:50,980
A good picture doesn't show
too much of it's existence.

376
00:58:51,920 --> 00:58:54,320
But how's this?

377
00:58:54,730 --> 00:58:57,720
It shows custom and it is trying to attract people

378
00:58:58,630 --> 00:59:02,730
with naked women, and it is also trying to

379
00:59:03,470 --> 00:59:08,000
attract people's attention with many colors.
Don't you think?

380
00:59:08,610 --> 00:59:12,770
This is a shallow and a rude work.

381
00:59:13,280 --> 00:59:14,210
Sir!

382
00:59:14,780 --> 00:59:19,480
Didn't you mean that this picture
moves the heart of people

383
00:59:19,680 --> 00:59:21,910
and pulls out one's soul?

384
00:59:22,090 --> 00:59:25,490
Do you think there's a student who can

385
00:59:25,720 --> 00:59:28,490
move one's heart with a picture?

386
00:59:28,630 --> 00:59:32,220
This work neglects the art department.

387
00:59:32,560 --> 00:59:33,690
Sir!

388
00:59:33,930 --> 00:59:38,270
Don't you remember you can't judge?
Why do you keep talking?

389
00:59:38,370 --> 00:59:42,740
Sir! Aren't you an artist too?

390
00:59:43,110 --> 00:59:46,870
Shin Yoon Bok chose one out of two topics.

391
00:59:47,150 --> 00:59:48,580
Is there anything wrong?

392
00:59:48,680 --> 00:59:51,620
Whatever you say, this picture won't work!

393
00:59:53,050 --> 00:59:55,040
- Doesn't it?
- I'm sorry.

394
00:59:56,150 --> 00:59:57,880
That's a shallow picture...

395
00:59:58,620 --> 01:00:01,560
Dan Won! What do you think you are doing?

396
01:00:02,360 --> 01:00:03,790
Get out!

397
01:00:05,530 --> 01:00:11,130
It's now a real goodbye.
What do I have to do when I miss you?

398
01:00:13,840 --> 01:00:18,300
The art department has gone crazy.
Totally crazy!

399
01:00:19,280 --> 01:00:21,400
It's crazy!

400
01:00:21,780 --> 01:00:25,840
- That, that, that temper!
- It's crazy!

401
01:00:28,990 --> 01:00:32,250
He doesn't fit with the art department.

402
01:00:33,060 --> 01:00:36,750
We have to thank him for leaving.

403
01:00:37,560 --> 01:00:38,690
Don't worry.

404
01:00:51,710 --> 01:00:52,680
They are here.

405
01:00:58,350 --> 01:01:01,940
We will announce the result of the exam.

406
01:01:06,020 --> 01:01:09,550
First, student Jang Hyo Won.

407
01:01:15,370 --> 01:01:22,400
All the 1 1 judges passed him and he has 55, the
highest score.

408
01:01:22,510 --> 01:01:24,670
55/means a perfect score.

409
01:01:30,980 --> 01:01:33,180
Congratulations.

410
01:01:35,090 --> 01:01:35,880
And!

411
01:01:39,390 --> 01:01:43,660
Special entrance! Student, Shin Yoon Bok.

412
01:01:50,770 --> 01:01:51,860
Yoon Bok!

413
01:01:52,140 --> 01:01:53,430
Special entrance?

414
01:01:54,410 --> 01:01:55,700
What des this mean?

415
01:01:55,840 --> 01:02:00,540
This student got judged by 12 judges

416
01:02:01,350 --> 01:02:05,810
and he got one pass in order
to qualifyforthe exam.

417
01:02:16,530 --> 01:02:17,550
Am I again?

418
01:02:19,560 --> 01:02:23,400
How come did he get to be judged by 12 judges?

419
01:02:23,700 --> 01:02:26,300
Who is it?
Who's the 12th judge?

420
01:02:26,800 --> 01:02:27,670
Who is it?

421
01:02:28,210 --> 01:02:31,230
How can he pass Yoon Bok?

422
01:02:35,780 --> 01:02:36,870
He is...

423
01:02:39,250 --> 01:02:43,580
He is the King!

424
01:02:48,630 --> 01:02:51,790
The...King?

425
01:02:51,930 --> 01:02:56,590
Inscrutable are the king's favors.

