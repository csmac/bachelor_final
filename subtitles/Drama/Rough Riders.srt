1
00:00:09,000 --> 00:00:16,000
Subtitle by: Footsoldier
The evil Japanese warlord Lord Godless desires to conquer China. He threatens the General in command to surrender,

2
00:00:16,030 --> 00:00:24,000
takes the emperor captive and subdues a large number of martial arts masters using his special poison.

3
00:00:24,030 --> 00:00:30,000
Among the captives are Cloud who surrendered out of concern for Chu Chu, and the Mou Lam legend Nameless.

4
00:00:30,030 --> 00:00:42,000
Lord Godless gives them two choices, they can either surrender and work under him, or they will be executed. None of the masters accept the offer.

5
00:02:26,080 --> 00:02:28,640
Your Emperor has already handed the throne to my father,

6
00:02:29,040 --> 00:02:30,439
Juewu Shen (Lord Godless)

7
00:02:32,360 --> 00:02:38,799
Those willing to surrender, The gift is the poison antidote

8
00:02:41,720 --> 00:02:46,032
and enjoy the central plains together

9
00:02:46,360 --> 00:02:51,878
Xun zhe chen, Nie zhi wang!

10
00:03:04,520 --> 00:03:09,719
Nameless, surrender your secret 1000 swords skill

11
00:03:10,760 --> 00:03:17,108
and you will conquer the central plains with me

12
00:03:17,400 --> 00:03:24,590
Since hundreds and thousands of years, Central Plains has already survived so many enemy attacks.

13
00:03:24,920 --> 00:03:29,198
Now still do not fall

14
00:03:29,520 --> 00:03:34,958
With the power of you alone, you want to conquer the central plains?

15
00:03:35,280 --> 00:03:38,875
Only in your dreams

16
00:03:50,240 --> 00:03:57,590
Since you rejected this offer, kill!

17
00:04:07,720 --> 00:04:08,914
These are the Ma Xiang poison antidote

18
00:05:37,880 --> 00:05:40,474
Heart!

19
00:05:47,640 --> 00:05:49,835
Know nothing of your limits

20
00:06:51,000 --> 00:06:58,395
Wind and Cloud together, are vulnerable

21
00:08:12,400 --> 00:08:16,552
Today, I will use my immortal body armor

22
00:08:16,840 --> 00:08:21,436
to break the myth of your 1000 swords skill

23
00:10:18,680 --> 00:10:22,559
From now on, I am the myth

24
00:13:21,680 --> 00:13:29,259
FY-II
The Storm Warriors

25
00:14:17,160 --> 00:14:19,355
Master, how can you hurt so badly?

26
00:14:19,640 --> 00:14:23,189
Toxicity had not entirely ceased

27
00:14:23,520 --> 00:14:26,910
He was seriouly injured when confronting the Lord Godless

28
00:14:27,200 --> 00:14:29,589
Based on my current injury

29
00:14:29,920 --> 00:14:32,718
It is hard to recover Within a short time

30
00:14:35,840 --> 00:14:40,311
Brother Wind, Where are you going?

31
00:14:40,640 --> 00:14:44,030
I can't stand people to die because of me

32
00:14:46,280 --> 00:14:48,714
Brother Cloud

33
00:14:51,200 --> 00:14:52,792
Do not act rashly

34
00:14:53,080 --> 00:14:55,833
Otherwise, the sacrifices of the other masters will be useless

35
00:14:56,200 --> 00:15:02,116
Cloud, based on you and Wind potentials

36
00:15:02,480 --> 00:15:06,951
you will need to enhance both of your skills in order to defeat Lord Godless

37
00:15:07,320 --> 00:15:13,350
At this moment, only Piggy King's elder brother, Lord Wicked (xie huang) 

38
00:15:13,720 --> 00:15:17,315
can be skillful enough to defeat the Lord Godless

39
00:15:17,680 --> 00:15:21,878
That guy is strange

40
00:15:22,280 --> 00:15:25,795
he might not be willing to help

41
00:15:35,760 --> 00:15:37,113
Someone is chasing behind

42
00:15:37,400 --> 00:15:42,110
We split into two ways, I am responsible to divert the pursuing soldiers

43
00:15:42,400 --> 00:15:43,992
Brother Piggy King, you bring Wind and Cloud

44
00:15:44,240 --> 00:15:46,595
and go find Brother Xie Huang for help

45
00:15:46,800 --> 00:15:47,550
Ok

46
00:15:51,320 --> 00:15:52,469
Heart

47
00:15:52,760 --> 00:15:56,036
Bring back Nameless

48
00:15:56,320 --> 00:16:00,233
Eradicate others

49
00:16:00,600 --> 00:16:03,478
Those who disobeyed, will die

50
00:17:53,800 --> 00:17:56,473
Only he is capable of defeating Lord Godless

51
00:17:58,440 --> 00:18:00,032
He has strange behaviour

52
00:18:00,320 --> 00:18:01,639
Do not care about anything

53
00:18:01,960 --> 00:18:04,997
Hope that this time, he can help

54
00:18:22,680 --> 00:18:25,672
Len Lao, long time no see

55
00:18:26,000 --> 00:18:30,152
See you working day and night, you shoud take a rest

56
00:18:30,440 --> 00:18:33,955
If anyone is like you, how can there be any fun in life

57
00:18:36,400 --> 00:18:42,748
Lord wicked! I have came to see you, come out and meet

58
00:19:03,240 --> 00:19:05,913
Lord Wicked

59
00:19:06,320 --> 00:19:13,510
Me, wind and with Brother cloud, come under Nameless command,

60
00:19:13,800 --> 00:19:16,872
Hope that Lord Wicked can help and defeat Lord Godless

61
00:19:17,160 --> 00:19:21,199
He occupied the territory of Central Plains and we kill martial art masters and innocents

62
00:19:21,520 --> 00:19:26,913
These are dangerous and hard times, hope you can help

63
00:19:27,280 --> 00:19:31,671
Long have I ever question any world matters

64
00:19:32,000 --> 00:19:33,353
with Nameless

65
00:19:33,680 --> 00:19:35,796
Lord Godless will retreat sooner or later

66
00:19:36,120 --> 00:19:37,155
All of you go

67
00:19:37,480 --> 00:19:42,759
Because Nameless was defeated by Lord Godless, suffering from serious injuries,

68
00:19:43,120 --> 00:19:44,519
therefore we come and find you

69
00:19:44,800 --> 00:19:49,112
Now that Central plains is under the enemies hands, do give some opinions

70
00:20:00,320 --> 00:20:03,710
Brother Cloud

71
00:20:13,400 --> 00:20:18,554
Sir, to show our sincerity

72
00:20:18,880 --> 00:20:20,029
We will kneel here

73
00:20:20,440 --> 00:20:23,238
Until you are willing to offer your help to defeat Lord Godless

74
00:20:23,560 --> 00:20:30,955
Lord wicked, this is enough, stop being incapable of being satisfied

75
00:20:48,560 --> 00:20:52,519
Hey, have one, taste very nice

76
00:20:55,400 --> 00:20:59,791
Brother Wind, Brother Cloud. Both of you haven't been eating for the past few days

77
00:21:00,120 --> 00:21:01,519
Please have a little

78
00:21:04,720 --> 00:21:10,158
It's ok, ZhuZhu. Thanks for caring.

79
00:21:10,440 --> 00:21:13,512
How about Brother Wind?

80
00:21:13,880 --> 00:21:18,158
Lord Wicked, I am Second Dream

81
00:21:18,440 --> 00:21:20,192
Requesting to greet you

82
00:21:42,640 --> 00:21:45,029
Dream, Why did you come here?

83
00:21:47,640 --> 00:21:53,431
Piggy King, such big event

84
00:21:53,720 --> 00:22:00,637
I come, hoping to help Brother Wind and Cloud

85
00:22:00,960 --> 00:22:06,478
Good timing, please help persuade Lord Wicked

86
00:22:15,080 --> 00:22:20,473
Lord Wicked, I am Second Dream, please come out

87
00:22:32,520 --> 00:22:37,719
So, you no longer care about me

88
00:22:38,000 --> 00:22:41,231
Then I follow Brother Wind

89
00:22:41,480 --> 00:22:44,916
Kneel before you until you come out

90
00:22:52,600 --> 00:22:57,276
(Sigh) You have already grown up, how is your father?

91
00:22:57,600 --> 00:23:01,673
Lord Wicked, my father is fine

92
00:23:02,040 --> 00:23:05,749
He even said he wants to have a duel with you

93
00:23:06,000 --> 00:23:09,310
You are still that pure

94
00:23:38,920 --> 00:23:42,390
Lord Wicked, what happened to your hands?

95
00:24:34,160 --> 00:24:36,469
I got curious, and went to learn a demonic skill

96
00:24:36,760 --> 00:24:38,671
Hoping to master it

97
00:24:39,200 --> 00:24:44,832
However, the result is uncontrollable behaviour and had done terrible things

98
00:24:45,160 --> 00:24:54,478
Few years ago, due to fear of this behaviour happening again, i cut down my two arms

99
00:25:00,200 --> 00:25:03,237
Now my skills are not as powerful as before

100
00:25:12,720 --> 00:25:17,032
Learning any other skills cannot be done in a short time

101
00:25:17,280 --> 00:25:20,670
According to the present situation, seems not possible

102
00:25:20,960 --> 00:25:23,758
Lord Wicked, is there another way?

103
00:25:27,480 --> 00:25:31,792
Learn the demonic skill, therefore increases termendous strengths

104
00:25:37,560 --> 00:25:40,677
After learning the skill

105
00:25:40,960 --> 00:25:43,269
it will be dangerous

106
00:25:43,560 --> 00:25:47,917
However, it is the only fastest way to defeat Lord Godless

107
00:25:48,240 --> 00:25:49,958
Who is willing to try?

108
00:25:50,200 --> 00:25:51,428
Me

109
00:25:53,280 --> 00:25:58,149
Lord Wicked, do think carefully, are there any skilled masters?

110
00:25:58,480 --> 00:26:00,550
Would it be better to ask for their help?

111
00:26:01,960 --> 00:26:05,999
Open both of your hands

112
00:26:09,720 --> 00:26:11,631
Cloud

113
00:26:11,920 --> 00:26:13,831
You are too impatient and irrational

114
00:26:14,120 --> 00:26:16,509
Learning demonic skill will only worsen it

115
00:26:16,920 --> 00:26:20,151
Wind

116
00:26:20,400 --> 00:26:22,550
You have a calm heart

117
00:26:22,800 --> 00:26:24,074
Hope that after you learned the skill

118
00:26:24,400 --> 00:26:30,270
There is a chance of survival. Just like me, repent and be saved

119
00:26:47,480 --> 00:26:53,032
Cloud, be patient, wait until i mastered this skill

120
00:26:53,400 --> 00:26:56,039
We will fight Lord Godless together

121
00:26:59,800 --> 00:27:03,554
But there is one thing I want you to promise me

122
00:27:04,120 --> 00:27:08,796
If started to lose consiousness after i learned this demonic skill

123
00:27:09,200 --> 00:27:11,919
Killing innocents

124
00:27:12,320 --> 00:27:17,678
If that occurs, then I have to trouble you.

125
00:27:35,960 --> 00:27:41,273
Brother Cloud, for you

126
00:27:41,640 --> 00:27:43,437
Thank you for your help

127
00:28:27,960 --> 00:28:33,671
I wont bother you anymore

128
00:28:34,200 --> 00:28:35,474
Thanks

129
00:29:11,000 --> 00:29:12,513
Any martial arts

130
00:29:12,800 --> 00:29:16,315
cannot be learned in a short time, even with a superior master

131
00:29:16,760 --> 00:29:19,320
I understand that haste makes waste

132
00:29:22,960 --> 00:29:30,230
Rehabilitation is hard, learning demonic skills are easy but with major side effects

133
00:29:30,560 --> 00:29:34,599
Takes a long time to learn a skill, but lesser time with demonic skill

134
00:29:34,920 --> 00:29:38,037
To enhance life-long skill

135
00:29:38,360 --> 00:29:41,716
Then must wholeheartedly willingly to learn, will you regret?

136
00:29:42,040 --> 00:29:45,191
No, I have already considered this thoroughly

137
00:29:45,560 --> 00:29:51,590
Your blood, must mix with the demonic lake

138
00:29:51,920 --> 00:29:59,634
in order to become a demon

139
00:30:27,840 --> 00:30:33,198
We have finally met

140
00:30:41,720 --> 00:30:43,073
Brother Wind

141
00:30:45,600 --> 00:30:50,151
The letter you wrote me, I have been keeping it with me

142
00:31:00,760 --> 00:31:06,551
I have been cherishing it

143
00:32:55,120 --> 00:32:59,955
Cloud, Lord Nameless has invited you to come

144
00:36:38,200 --> 00:36:41,988
Just now you can counter my ultimate skill

145
00:36:42,320 --> 00:36:48,793
And in a short time, you can turn it into your own use

146
00:36:49,160 --> 00:36:54,359
Sufficient to prove that you have the ability to save this disaster

147
00:36:54,720 --> 00:36:58,554
I intend to pass on to you my skill

148
00:36:58,880 --> 00:37:06,275
I think you misunderstood
I have no ambition to be a hero

149
00:37:06,600 --> 00:37:08,079
I am willing to kneel before Lord Wicked

150
00:37:08,320 --> 00:37:12,154
Only to hope that after learning his demonic skill, i can fight for the sake of myself

151
00:37:23,120 --> 00:37:27,398
I am now left with only one power

152
00:37:31,960 --> 00:37:40,152
In order to defeat Lord Godless, there must be unity between Wind and Cloud

153
00:37:40,600 --> 00:37:44,149
And I feel that this unity

154
00:37:44,480 --> 00:37:48,393
will have a remarkable power and force

155
00:37:48,720 --> 00:37:53,475
and will continue to increase in power

156
00:38:50,800 --> 00:38:54,236
The demonic water

157
00:38:54,480 --> 00:38:56,789
This is the method

158
00:40:54,320 --> 00:40:58,871
On this map, the mentioned holy spine/treasure 

159
00:40:59,200 --> 00:41:03,000
is hidden in the Central Plains of the Dragon empire

160
00:41:03,000 --> 00:41:09,109
within the emperor's palace of a restricted area, a cave

161
00:41:09,440 --> 00:41:12,318
The cave is connected to the emperor's palace

162
00:41:12,720 --> 00:41:15,678
Only the Emperor knows the way

163
00:41:15,960 --> 00:41:20,078
If lost, there is a possibility that we will be lost forever

164
00:41:22,680 --> 00:41:24,272
King is already in our custody

165
00:41:24,560 --> 00:41:26,073
When found the Dragon Tomb

166
00:41:26,400 --> 00:41:28,960
No one can stop us from sharing the central plains

167
00:41:47,800 --> 00:41:53,875
Remember, central plains cannot be shared

168
00:41:54,360 --> 00:41:58,638
Lord Godless, with Nameless

169
00:41:59,000 --> 00:42:02,072
and Wind and Cloud

170
00:42:02,360 --> 00:42:05,193
Rumors that they share extraordinary potential

171
00:42:08,240 --> 00:42:15,032
Nameless has underestimate the power of the Ma xiang poison, it has not been cleared from his body yet 

172
00:42:15,360 --> 00:42:19,717
The forced use of his 1000 swords skill will only kill him

173
00:42:20,040 --> 00:42:25,990
As for Wind and Cloud, I don't even mind about them

174
00:42:28,760 --> 00:42:30,557
But when both unite

175
00:42:43,640 --> 00:42:48,350
The one who has the capability to find the Dragon holy treasure

176
00:42:48,680 --> 00:42:52,878
Can share great treasures with me

177
00:42:52,878 --> 00:42:57,720
Yes, Father

178
00:42:57,720 --> 00:43:02,320
Lord Godless, Nameless is under recovery

179
00:43:02,320 --> 00:43:03,390
Wind and Cloud went to the Mountain

180
00:43:03,760 --> 00:43:06,513
Send Messange to the my comrades

181
00:43:06,760 --> 00:43:07,909
to defeat Nameless

182
00:43:08,160 --> 00:43:12,597
Also, bring my best warrior to the Mountain

183
00:43:12,880 --> 00:43:14,029
Yes lord

184
00:43:18,560 --> 00:43:22,030
Let's go together and find the holy treasure

185
00:43:22,280 --> 00:43:25,192
together with the emperor

186
00:43:28,760 --> 00:43:30,318
My Son

187
00:43:30,560 --> 00:43:32,312
Understand

188
00:45:03,760 --> 00:45:06,228
Bring us to find the holy treasure

189
00:45:06,600 --> 00:45:09,194
My father will guarantee you and your family safety

190
00:45:11,840 --> 00:45:13,193
and enjoy lasting wealth

191
00:45:14,640 --> 00:45:17,234
Wealth I have already obtained

192
00:45:17,520 --> 00:45:20,318
This country has always belong to me

193
00:45:20,640 --> 00:45:24,758
I have been in vain 

194
00:45:25,080 --> 00:45:25,910
Stop your nonsence!

195
00:45:29,600 --> 00:45:33,991
Most people stay here

196
00:45:34,320 --> 00:45:35,992
Others follow me

197
00:45:37,520 --> 00:45:40,239
Walk

198
00:45:40,560 --> 00:45:42,994
Walk

199
00:46:32,120 --> 00:46:34,588
Can't believe at this short period

200
00:46:34,920 --> 00:46:39,436
You can master this skill in such high level

201
00:46:41,520 --> 00:46:43,795
Master, all this effort

202
00:46:44,040 --> 00:46:45,678
All thanks to you

203
00:46:45,960 --> 00:46:48,235
I hope you will name for this new skill of mine

204
00:46:50,080 --> 00:46:54,312
Since the skill you learned is special in this world

205
00:46:54,600 --> 00:46:58,070
I am afraid, not a word can be worthy of it

206
00:47:04,320 --> 00:47:09,030
Or this one ...

207 
00:47:29,720 --> 00:47:36,910
Cloud, next time you will name this skill of yours as this

208
00:47:37,240 --> 00:47:42,314
Master, there are so many odd words in this world

209
00:47:42,600 --> 00:47:46,718
Forgive of my stupidity, but i've never seen this word

210
00:47:47,040 --> 00:47:49,759
This word might not existed in this world

211
00:47:50,040 --> 00:47:54,670
This is my new creation

212
00:47:56,720 --> 00:48:00,679
Neither a cloud nor cloud, Neither a sword nor sword

213
00:48:00,920 --> 00:48:01,955
that this word implies

214
00:48:02,200 --> 00:48:06,398
You can read it as "Tyrants" 

215
00:48:10,160 --> 00:48:12,230
"Tyrants"?

216
00:50:01,640 --> 00:50:04,791
What are you doing? What are you sweeping?

217
00:50:05,120 --> 00:50:06,872
I am enough of you

218
00:50:07,200 --> 00:50:08,792
There's nothing for you to sweep

219
00:50:14,520 --> 00:50:18,752
Do persuade him

220
00:50:19,160 --> 00:50:19,956
He's wasting his time

221
00:50:22,120 --> 00:50:24,190
Be patient

222
00:50:24,520 --> 00:50:27,239
I told you to self-cutivate, have you done that?

223
00:50:32,320 --> 00:50:37,997
Time has passed, Wind has gone to the final stage of his demonic training

224
00:50:38,320 --> 00:50:40,311
Must not be disturbed

225
00:50:40,640 --> 00:50:45,760
Otherwise, he will die

226
00:50:47,560 --> 00:50:54,636
Cloud, when will you be back ...

227
00:54:33,960 --> 00:54:42,470
Cloud, the skill you learned, has not been completed yet

228
00:54:42,760 --> 00:54:47,834
Still unable to express to the tipping point

229
00:54:48,840 --> 00:54:54,472
Once completed, the power is unimaginable

230
00:55:00,800 --> 00:55:06,432
Rush to the Mountain immediately, see how are they

231
00:56:01,040 --> 00:56:02,189
Which hole is the Dragon Tomb?

232
00:57:39,000 --> 00:57:46,793
This Central Plains will not escape from my reign

233
00:58:19,040 --> 00:58:20,393
Sir

234
00:58:22,200 --> 00:58:26,955
What happened? Where is Wind?

235
00:58:27,280 --> 00:58:29,396
In order to save Second Dream, Brother Wind has been forced to come out

236
00:58:29,680 --> 00:58:31,238
without finishing his demonic training

237
00:58:33,600 --> 00:58:36,040
Actually, I am confused

238
00:58:36,040 --> 00:58:38,873
The training has not finished

239
00:58:39,200 --> 00:58:40,997
Those people learning it cannot intefere the process

240
00:58:41,040 --> 00:58:44,476
If intefered at the last minute,

241
00:58:44,720 --> 00:58:46,597
That person will go mad/crazy

242
00:58:46,840 --> 00:58:49,479
and die on the spot

243
00:58:51,440 --> 00:58:54,830
Unless Wind is somewhat different

244
00:58:58,840 --> 00:59:00,990
With the present situation

245
00:59:01,240 --> 00:59:04,312
as long as he can wake up from his uncontrollable and unfinished demonic power

246
00:59:04,560 --> 00:59:07,028
Most likely he will return to the correct path

247
00:59:07,320 --> 00:59:11,598
That means Wind still has a chance

248
00:59:15,240 --> 00:59:18,516
Where is Wind at this moment?

249
00:59:23,440 --> 00:59:28,798
Cloud, you went with Nameless for such a long time, what did you do?

250
00:59:29,080 --> 00:59:31,913
He teached me his ultimate skill

251
00:59:32,240 --> 00:59:35,198
Even guide me to invent my own skill based on his

252
00:59:37,440 --> 00:59:41,115
Please note that the Emperor is under custody by Lord Godless

253
00:59:41,320 --> 00:59:42,355
and went after the Dragon tomb in search for the holy spine

254
00:59:46,920 --> 00:59:48,478
In the Dragon Tomb,

255
00:59:48,760 --> 00:59:51,672
there is a hidden material that according to legend, possession of the material will enhance skill and wisdom

256
00:59:51,920 --> 00:59:53,433
It must not be fallen to Lord Godless possession

257
00:59:53,720 --> 00:59:57,030
Cloud, take haste and stop him

258
00:59:57,320 --> 01:00:00,517
Lord Godless has indestructable body armor

259
01:00:00,800 --> 01:00:03,234
Based on my opinion, his weak spot

260
01:00:03,480 --> 01:00:05,232
is hidden in his strongest defended area

261
01:00:07,680 --> 01:00:09,910
I will remember what you have said

262
01:00:10,240 --> 01:00:14,028
When I have finished, I will return immediately

263
01:00:37,680 --> 01:00:42,515
I think I know where Brother Wind has went

264
01:01:00,520 --> 01:01:04,593
Really magnificent

265
01:01:18,680 --> 01:01:20,955
Where is the Dragon Tomb?

266
01:01:25,320 --> 01:01:27,117
This is the land of the dragon tomb?