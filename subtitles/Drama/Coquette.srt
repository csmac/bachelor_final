1
00:00:10,960 --> 00:00:15,305
A Flirtatious Woman

2
00:00:17,000 --> 00:00:23,074
Subs by Filip�w
Twitter: @Filiipaw

3
00:00:26,360 --> 00:00:27,774
My dear Fran�oise,

4
00:00:27,894 --> 00:00:30,153
You ask me if I'm happy
since my marriage,

5
00:00:30,520 --> 00:00:33,411
yes, very. Jacques
and I get along very well.

6
00:00:34,062 --> 00:00:36,656
But I'm very unhappy now,
I've just cheated on him.

7
00:00:36,842 --> 00:00:40,062
It wasn't on purpose, but I don't
know if I must confess it to him.

8
00:00:40,280 --> 00:00:42,520
I write to ask for your advice.

9
00:00:42,640 --> 00:00:45,107
I'm cruelly punished
for my thoughtlessness.

10
00:00:45,440 --> 00:00:47,880
Must I laugh or cry?
I don't know anymore.

11
00:00:48,000 --> 00:00:50,400
Don't think that I've taken
a lover, no, not at all.

12
00:00:50,760 --> 00:00:53,233
Well, yes, but what could I do?

13
00:00:53,680 --> 00:00:55,613
Here's exactly what happened:

14
00:01:11,440 --> 00:01:13,706
Like each Friday morning,
while leaving the office,

15
00:01:13,760 --> 00:01:17,121
Jacques went to his parents'
while I went home to cook lunch.

16
00:01:35,600 --> 00:01:38,227
Would there be sun, or rain?
How could I know?

17
00:01:38,560 --> 00:01:41,298
The sun was appearing and
disappearing behind the clouds,

18
00:01:41,523 --> 00:01:43,312
and I thought of
taking the streetcar

19
00:01:43,360 --> 00:01:46,710
when my attention was caught by
a woman I saw at her window.

20
00:01:46,920 --> 00:01:49,288
Almost in front of me,
on the other side of the road.

21
00:01:49,840 --> 00:01:51,680
She was nothing extraordinary

22
00:01:51,800 --> 00:01:55,785
but she intrigued me, because
a smile, very light, very quick

23
00:01:56,080 --> 00:01:58,040
regularly animated her features.

24
00:01:58,160 --> 00:01:59,400
And I wondered

25
00:01:59,520 --> 00:02:02,800
who's the target of this
uninterrupted line of smiles?

26
00:02:02,920 --> 00:02:04,280
Suddenly I understood,

27
00:02:04,400 --> 00:02:07,133
she smiled at every man
that passed under her window.

28
00:02:07,240 --> 00:02:11,373
Old ones, ugly ones, handsome ones,
tall ones, men of all kinds.

29
00:02:11,680 --> 00:02:14,837
As soon as she caught their eye,
she made a signal.

30
00:02:15,520 --> 00:02:18,030
But furtive smiles, so sweet,

31
00:02:18,369 --> 00:02:20,517
that they were erased
as soon as they started.

32
00:02:20,600 --> 00:02:22,667
Both full of prudishness
and infamy.

33
00:02:22,760 --> 00:02:26,259
So discreet and cunning, that they
looked innocent at first glance.

34
00:02:26,400 --> 00:02:29,251
And could mean equally,
what a beautiful day,

35
00:02:29,320 --> 00:02:31,552
or, come up quick sweetie.

36
00:02:31,760 --> 00:02:34,516
She would repeat this
tactful act tirelessly.

37
00:02:34,762 --> 00:02:38,219
Indeed, after a few minutes
I saw a man answer that call.

38
00:02:38,400 --> 00:02:41,267
He entered the building.
I couldn't believe my eyes.

39
00:02:41,520 --> 00:02:45,053
I wanted to be sure. And started
to wait in front of her door.

40
00:02:45,633 --> 00:02:47,333
It wasn't long.

41
00:02:47,600 --> 00:02:51,667
Only 15 minutes, and I saw the man
going out, our mermaid open her window

42
00:02:51,920 --> 00:02:53,920
and start the same game again.

43
00:02:54,968 --> 00:02:57,345
Then I went away
astounded, asking myself

44
00:02:58,240 --> 00:03:01,541
to what kind of act
was I the spectator.

45
00:03:02,960 --> 00:03:08,266
To the bottom of my soul, curiosity and
respect were mixed with my indignation.

46
00:03:08,592 --> 00:03:11,559
I was very concerned
about what I'd seen.

47
00:03:11,760 --> 00:03:15,560
And the more I thought, the less I could
stop myself admiring this woman.

48
00:03:15,680 --> 00:03:17,440
For her skill

49
00:03:17,560 --> 00:03:21,310
as well as her gracious way
of practicing this dreadful job.

50
00:04:05,720 --> 00:04:08,179
Then suddenly,
I had a crazy idea.

51
00:04:08,401 --> 00:04:10,960
So absurd that it
frightened me at first.

52
00:04:11,080 --> 00:04:14,278
The idea of doing the same thing.
To throw at the first man I saw,

53
00:04:14,398 --> 00:04:16,920
me too, a smile.
Just one.

54
00:04:17,040 --> 00:04:19,040
And to see what'd happen next.

55
00:04:19,280 --> 00:04:21,920
Believe what you want,
blame me for flirting.

56
00:04:22,040 --> 00:04:26,496
But I was suddenly anxious
to know if I would succeed.

57
00:04:28,000 --> 00:04:30,207
I couldn't get this
thought out of my head.

58
00:04:30,520 --> 00:04:32,653
It came back each time
more threatening.

59
00:04:32,920 --> 00:04:35,097
I absolutely had to do it.

60
00:04:36,424 --> 00:04:39,024
My heart was pounding.
I was ashamed, I confess.

61
00:04:39,720 --> 00:04:42,120
But I was already forgiving
myself for my recklessness.

62
00:04:42,240 --> 00:04:44,288
Think about the game,
as Jacques said.

63
00:04:44,408 --> 00:04:47,016
Women think that everything
they dare is innocent.

64
00:04:48,840 --> 00:04:52,373
I wasn't paying attention.
I was lost in my thoughts.

65
00:04:52,680 --> 00:04:57,200
They were lost in my soul like
clouds drifting apart by the wind

66
00:04:57,320 --> 00:05:00,376
on a greyish background
where the sun was heading.

67
00:05:00,640 --> 00:05:05,070
I realized that all those reflections
had brought me to the Rousseau park.

68
00:05:05,360 --> 00:05:08,050
I don't know how,
and not far from me, by chance,

69
00:05:08,170 --> 00:05:10,320
and you see that
the devil was with me,

70
00:05:10,440 --> 00:05:13,000
I saw a man reading a newspaper.

71
00:05:13,120 --> 00:05:14,920
He didn't look bad.

72
00:05:15,040 --> 00:05:18,755
It didn't matter, the most important thing
was to defeat my cowardice.

73
00:05:48,360 --> 00:05:51,320
Yes, I thought to myself,
the fate of a coquette is so cruel.

74
00:05:51,440 --> 00:05:54,813
The world sentences her to
a freedom that tests their virtue.

75
00:05:54,933 --> 00:05:58,162
Debauching oneself demands a courage
that our time excused too much

76
00:05:58,282 --> 00:06:00,308
in the name of
prudishness and inattention.

77
00:06:00,400 --> 00:06:02,956
Of love, but never of wisdom.

78
00:06:03,160 --> 00:06:08,159
Acting with flirtatiousness was, to accept
the danger of being taken by surprise.

79
00:06:08,720 --> 00:06:11,625
Confident that this adventure
wasn't that interesting

80
00:06:11,840 --> 00:06:14,423
and to hide from my
favorite husband,

81
00:06:14,720 --> 00:06:17,611
in myself, the fact of
making this rash gesture,

82
00:06:17,920 --> 00:06:20,447
the fear of knowing him,
my inexperience with men,

83
00:06:20,680 --> 00:06:22,803
the wish to test my honesty,

84
00:06:23,080 --> 00:06:27,422
everything contributed to adopt
this frivolous yet inexorable act.

85
00:07:26,080 --> 00:07:28,217
I wondered what kind
of smile should I have,

86
00:07:28,440 --> 00:07:30,228
feminine, Iphigenia?

87
00:07:31,000 --> 00:07:34,640
I sat next to him. I was hesitating.
I was shaking.

88
00:07:34,760 --> 00:07:36,627
And then, bravely, I did it.

89
00:07:36,800 --> 00:07:39,600
I smiled at him,
very quickly, very shyly.

90
00:07:39,880 --> 00:07:41,917
Well, it worked first time.

91
00:07:42,120 --> 00:07:44,568
My stranger came closer
while looking at me.

92
00:07:44,840 --> 00:07:46,116
Then I panicked.

93
00:07:46,320 --> 00:07:49,890
I told him that it was a mistake,
a bet with myself.

94
00:07:50,200 --> 00:07:54,320
And I ran away but he chased me.
He wouldn't hear anything.

95
00:07:54,440 --> 00:07:58,000
He looked like was having lots
of fun while I was terrorized.

96
00:07:58,120 --> 00:08:02,094
Panicked, I ran away to escape him.
But he followed me by car.

97
00:08:30,880 --> 00:08:33,259
My only chance was
to arrive home before him.

98
00:08:33,440 --> 00:08:36,640
Impossible, he came behind me
and wanted to kiss me.

99
00:08:36,760 --> 00:08:38,027
I burst into tears.

100
00:08:38,920 --> 00:08:43,030
I begged him to go away, my husband
is coming home any minute I said.

101
00:08:43,240 --> 00:08:44,680
But he got angry.

102
00:08:44,800 --> 00:08:48,319
Stop fooling around dear, he said,
here's 50 francs, undress.

103
00:08:49,880 --> 00:08:54,040
The only way to get rid of this
fool right away, was to give in.

104
00:08:54,160 --> 00:08:56,932
That's what I did,
what else could I do?

105
00:08:58,842 --> 00:09:01,760
Here I am cruelly punished
of my flirtatiousness.

106
00:09:01,880 --> 00:09:05,547
I write to you my dear Fran�oise
to convince you of my innocence.

107
00:09:06,480 --> 00:09:07,995
Answer me quickly.

108
00:09:08,360 --> 00:09:10,422
Your friend, Agn�s.

109
00:09:14,280 --> 00:09:18,203
subs by Filip�w

110
00:09:19,305 --> 00:09:25,425
Twitter: @Filiipaw
