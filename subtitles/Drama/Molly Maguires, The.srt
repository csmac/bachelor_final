1
00:00:02,511 --> 00:00:03,986
Have you lost
some weight?

2
00:00:03,987 --> 00:00:05,801
I have, and thank
you for noticing.

3
00:00:05,802 --> 00:00:06,903
I thought so.

4
00:00:06,904 --> 00:00:08,571
When you were bent
over tying your shoes,

5
00:00:08,572 --> 00:00:10,306
Your wallet didn't
look like it was trying

6
00:00:10,307 --> 00:00:12,641
To bust out
of your ass.

7
00:00:12,642 --> 00:00:15,077
Do you, uh, always check out
my behind when I'm bent over?

8
00:00:15,078 --> 00:00:16,829
Well, I try to look
every other way,

9
00:00:16,830 --> 00:00:18,448
But that thing
is an Imax.

10
00:00:20,150 --> 00:00:22,785
Well, regardless, I feel
better, I got more energy...

11
00:00:22,786 --> 00:00:24,487
I don't know
if you're aware of this,

12
00:00:24,488 --> 00:00:26,989
But for a long time I struggled
with a midday lag.

13
00:00:26,990 --> 00:00:29,675
Well, if you're talking about
that time between noon to 5:00,

14
00:00:29,676 --> 00:00:30,993
When I drive and you sleep?

15
00:00:30,994 --> 00:00:32,929
Yeah, I've noticed.

16
00:00:32,930 --> 00:00:34,330
Well, no more.

17
00:00:34,331 --> 00:00:35,998
And the great thing is,
I'm not dieting.

18
00:00:35,999 --> 00:00:37,016
I eat whatever I want.

19
00:00:37,017 --> 00:00:38,301
It's all about
portion control.

20
00:00:38,302 --> 00:00:40,269
Well, I been preaching
moderation to you for years.

21
00:00:40,270 --> 00:00:41,504
The stomach understood.

22
00:00:41,505 --> 00:00:43,639
The ears weren't
ready to listen.

23
00:00:43,640 --> 00:00:45,475
Morning, gentlemen.

24
00:00:45,476 --> 00:00:46,359
Hey, Samuel.

25
00:00:46,360 --> 00:00:47,510
I'll have the
French toast,

26
00:00:47,511 --> 00:00:49,812
With hash browns and
a large orange juice.

27
00:00:49,813 --> 00:00:50,863
Mm, good choice.

28
00:00:50,864 --> 00:00:52,365
And for you, big and bountiful?

29
00:00:52,366 --> 00:00:55,568
I will have a half
a cup of grape-nuts,

30
00:00:55,569 --> 00:00:56,986
Two inches of bacon,

31
00:00:56,987 --> 00:01:01,624
And one silver-dollar pancake
with a teaspoon of syrup.

32
00:01:01,625 --> 00:01:03,326
Is he making a joke?

33
00:01:04,495 --> 00:01:08,247
I usually find more food
than that under his chair.

34
00:01:08,248 --> 00:01:10,166
Afraid not.

35
00:01:10,167 --> 00:01:11,284
Okay.

36
00:01:11,285 --> 00:01:14,604
Uh, would you like skim
milk on your grape-nuts?

37
00:01:14,605 --> 00:01:17,640
No, whole milk.
Just a whisper.

38
00:01:17,641 --> 00:01:19,475
I like my nuts crunchy.

39
00:01:21,695 --> 00:01:25,548
You are certain my chain
is not being yanked.

40
00:01:25,549 --> 00:01:26,682
Huh.

41
00:01:26,683 --> 00:01:27,984
You understand my suspicions,
though, right?

42
00:01:27,985 --> 00:01:29,268
Just go get my food!

43
00:01:31,405 --> 00:01:32,989
So what you got
going on tonight?

44
00:01:32,990 --> 00:01:34,023
Nothing, really.

45
00:01:34,024 --> 00:01:35,658
I was thinking about
asking molly out again.

46
00:01:35,659 --> 00:01:36,776
Are you crazy?

47
00:01:36,777 --> 00:01:38,578
You can't ask a woman out
on the same day.

48
00:01:38,579 --> 00:01:42,164
Why not?
'cause even if she's got nothing
going on, she's gonna tell you

49
00:01:42,165 --> 00:01:43,833
She's got something going on,
so you don't think she's got

50
00:01:43,834 --> 00:01:45,868
Nothing going on, when you call
her and ask her what's going on.

51
00:01:45,869 --> 00:01:48,838
So should I call her or not?

52
00:01:48,839 --> 00:01:51,040
Sure. You got
nothing going on.

53
00:01:51,041 --> 00:01:53,242
Really? What are
you doing tonight?

54
00:01:53,243 --> 00:01:55,678
Making fondue
with your grandma?

55
00:01:58,098 --> 00:02:01,250
It's chili night.

56
00:02:01,251 --> 00:02:05,354
You know, I was thinking maybe
I'd ask molly to go bowling.

57
00:02:05,355 --> 00:02:06,422
Bowling?

58
00:02:06,423 --> 00:02:08,057
Oh, you dog!

59
00:02:08,058 --> 00:02:10,092
I see the tender trap
you're baiting.

60
00:02:10,093 --> 00:02:12,111
What? It's just
a second date.

61
00:02:12,112 --> 00:02:14,230
I figured it would be a
fun, no-pressure kinda deal.

62
00:02:14,231 --> 00:02:15,598
Uh-huh.

63
00:02:15,599 --> 00:02:18,267
And it has nothing to do with
the fact that when it comes

64
00:02:18,268 --> 00:02:19,986
To bowling, you're a
chunky, white Michael Jordan?

65
00:02:19,987 --> 00:02:22,905
It does not.

66
00:02:22,906 --> 00:02:24,440
Not to mention a sight to behold

67
00:02:24,441 --> 00:02:27,326
In that pink and black shirt
with the matching ball?

68
00:02:27,327 --> 00:02:29,629
You know, I won that ball
in a police league tournament.

69
00:02:29,630 --> 00:02:32,615
I built the ensemble around it.

70
00:02:32,616 --> 00:02:35,034
You smooth-ass Casanova, you.

71
00:02:35,035 --> 00:02:36,335
I'm not a Casanova.

72
00:02:36,336 --> 00:02:37,786
I haven't even kissed
this girl yet.

73
00:02:37,787 --> 00:02:39,005
Forget kissing.

74
00:02:39,006 --> 00:02:42,958
You're gonna be looking
at a seven/ten split.

75
00:02:42,959 --> 00:02:44,627
That schoolteacher ain't
gonna know what hit her,

76
00:02:44,628 --> 00:02:46,178
When she sees you
treat those pins

77
00:02:46,179 --> 00:02:47,396
Like your ten
little bitches.

78
00:02:47,397 --> 00:02:49,231
Well, whether it goes
that far or not,

79
00:02:49,232 --> 00:02:51,100
I just want to show
her a good time.

80
00:02:51,101 --> 00:02:53,152
She lives at home with
her mom and her sister.

81
00:02:53,153 --> 00:02:54,604
I get the feeling
she doesn't have

82
00:02:54,605 --> 00:02:56,022
A lot of excitement
in her life.

83
00:02:56,023 --> 00:02:57,773
Put the
scissors down!
No! No!

84
00:02:57,774 --> 00:02:59,025
I'm gonna cut it all off!

85
00:02:59,026 --> 00:03:00,026
No man is worth it.

86
00:03:00,027 --> 00:03:01,327
He said he loved me.

87
00:03:01,328 --> 00:03:02,445
He's married!

88
00:03:02,446 --> 00:03:05,748
He won't be after
his wife gets my hair!

89
00:03:13,790 --> 00:03:19,790
<font color="#00ffff">-- Sync by Rafael UPD --</font>
<font color="#00ffff">www.addic7ed.com/</font>

90
00:03:23,843 --> 00:03:27,362
Well, I finally got
Sweeney Todd to bed.

91
00:03:27,363 --> 00:03:28,780
How'd you manage that?

92
00:03:28,781 --> 00:03:32,734
I crushed up three xanax and put
it in her teeth whitening tray.

93
00:03:32,735 --> 00:03:35,454
You know, it's all my fault
she's so fragile.

94
00:03:35,455 --> 00:03:37,155
I was too hard on her
growing up.

95
00:03:37,156 --> 00:03:40,576
Too hard? You breastfed
her till she was five.

96
00:03:40,577 --> 00:03:43,161
I would've done the same
for you,

97
00:03:43,162 --> 00:03:46,665
Except you were eating corn
on the cob before you had teeth.

98
00:03:46,666 --> 00:03:48,634
Hello?

99
00:03:48,635 --> 00:03:49,551
Hey, Mike!

100
00:03:49,552 --> 00:03:51,336
Uh, not much.

101
00:03:51,337 --> 00:03:53,722
Hanging with mom and sis.

102
00:03:53,723 --> 00:03:54,723
Tonight?

103
00:03:54,724 --> 00:03:55,924
You can't leave!

104
00:03:55,925 --> 00:03:57,259
Watch me!

105
00:03:58,561 --> 00:04:01,212
Um... Bowling, yeah,
that sounds fun!

106
00:04:01,213 --> 00:04:03,048
Your sister needs us now!

107
00:04:03,049 --> 00:04:04,066
Hang on.

108
00:04:04,067 --> 00:04:05,684
Mom, she's doing this
for attention.

109
00:04:05,685 --> 00:04:09,554
It's the same reason she's
got a webcam in her bathroom.

110
00:04:09,555 --> 00:04:11,490
Fine.

111
00:04:11,491 --> 00:04:14,159
But if you come home
and find the coroner

112
00:04:14,160 --> 00:04:15,560
Snapping a toe tag
on your sister,

113
00:04:15,561 --> 00:04:17,362
Promise me
you won't blame yourself.

114
00:04:17,363 --> 00:04:19,364
Deal.

115
00:04:20,283 --> 00:04:22,084
I love him.

116
00:04:22,085 --> 00:04:24,336
I must go to him.

117
00:04:24,337 --> 00:04:26,204
Oh, damn, it's awake.

118
00:04:26,205 --> 00:04:29,291
Sure, that sounds fun! Great!

119
00:04:29,292 --> 00:04:31,643
7:00, I have to go!

120
00:04:35,247 --> 00:04:37,082
Listen. Why don't I
just bring you

121
00:04:37,083 --> 00:04:38,634
A whole order of french fries,

122
00:04:38,635 --> 00:04:40,135
And you just eat 12 of them?

123
00:04:40,136 --> 00:04:42,688
If I could eat just 12,

124
00:04:42,689 --> 00:04:45,390
This shirt wouldn't look like
it was made in an awning store.

125
00:04:45,391 --> 00:04:48,944
I tried that portion
control diet once.

126
00:04:48,945 --> 00:04:50,078
Yeah? How'd it go?

127
00:04:50,079 --> 00:04:51,279
Gained 14 pounds.

128
00:04:51,280 --> 00:04:54,683
Three frickin' gummi bears
at a time.

129
00:04:54,684 --> 00:04:56,151
Actually, I'm having
wonderful success with it,

130
00:04:56,152 --> 00:04:58,103
And if I stay on track?
I'm thinking next summer,

131
00:04:58,104 --> 00:05:01,239
Shirtless at the beach!

132
00:05:01,240 --> 00:05:02,457
I'll bring the suntan lotion.

133
00:05:02,458 --> 00:05:05,410
Just in case, bring a bucket
and a paint roller.

134
00:05:06,295 --> 00:05:08,664
That is quite an outfit.

135
00:05:08,665 --> 00:05:10,999
I'm guessing you've
done this before.

136
00:05:11,000 --> 00:05:12,467
Well, I'm not
gonna lie to you.

137
00:05:12,468 --> 00:05:14,953
I'm a bit of a legend
in these parts.

138
00:05:14,954 --> 00:05:16,254
Oh, really?

139
00:05:16,255 --> 00:05:17,472
Well, maybe legend's too strong.

140
00:05:17,473 --> 00:05:18,890
Folk hero.

141
00:05:18,891 --> 00:05:21,143
I'm prepared to be impressed.

142
00:05:21,144 --> 00:05:23,562
Me, I just did a little
bowling in college.

143
00:05:23,563 --> 00:05:25,313
Mostly as an excuse to drink.

144
00:05:25,314 --> 00:05:27,649
Yeah, you know,
a lot of people like

145
00:05:27,650 --> 00:05:29,101
To mix alcohol with a sport.

146
00:05:29,102 --> 00:05:31,019
But I don't like
my senses dulled.

147
00:05:31,020 --> 00:05:34,189
Can't put beer goggles
on the eye of the tiger.

148
00:05:34,190 --> 00:05:36,692
Right.
You know, if you're ever
interested in a couple of tips,

149
00:05:36,693 --> 00:05:38,744
I'd be happy to help.
That'd be great.

150
00:05:38,745 --> 00:05:40,078
All right.

151
00:05:40,079 --> 00:05:42,030
First off, there's a lot of
misconceptions about bowling.

152
00:05:42,031 --> 00:05:43,749
It's not just
about brute force.

153
00:05:43,750 --> 00:05:45,867
It's about form,
follow-through, and finesse.

154
00:05:45,868 --> 00:05:47,919
The three fs,
I like to call 'em.

155
00:05:47,920 --> 00:05:49,538
Not to be confused
with the three rs,

156
00:05:49,539 --> 00:05:51,990
Reading, writing and
'rithmatic, which is your forte.

157
00:05:51,991 --> 00:05:54,276
Now, the key to form

158
00:05:54,277 --> 00:05:57,462
Is keeping your
wrist straight, okay?

159
00:05:57,463 --> 00:05:58,663
Key to a
follow-through

160
00:05:58,664 --> 00:06:02,100
Is releasing the ball
in a smooth, gliding motion.

161
00:06:02,101 --> 00:06:03,685
Attaboy, mike!

162
00:06:03,686 --> 00:06:05,937
Ignore him.

163
00:06:05,938 --> 00:06:07,055
What about finesse?

164
00:06:07,056 --> 00:06:10,225
Forget finesse for now,
focus on fun.

165
00:06:11,510 --> 00:06:13,195
Okay.

166
00:06:13,196 --> 00:06:14,563
Hope I don't
embarrass myself

167
00:06:14,564 --> 00:06:16,531
Hey. We're just here
to have a good time.

168
00:06:16,532 --> 00:06:18,150
Nobody's gonna
be embarrassed.

169
00:06:23,122 --> 00:06:27,125
Oh, look! I knocked
them all down!

170
00:06:29,962 --> 00:06:31,763
I'll be darned.

171
00:06:31,764 --> 00:06:33,331
Was my form okay?

172
00:06:33,332 --> 00:06:34,532
Yeah. I mean, uh,

173
00:06:34,533 --> 00:06:36,201
You're still bending
your wrist quite a bit,

174
00:06:36,202 --> 00:06:37,719
But that goes away
with practice.

175
00:06:37,720 --> 00:06:38,870
Could I take
another turn?

176
00:06:38,871 --> 00:06:40,672
Sure.

177
00:06:40,673 --> 00:06:42,874
Maybe I'll get lucky again.

178
00:06:42,875 --> 00:06:45,393
That's a very healthy attitude.

179
00:06:48,765 --> 00:06:51,316
Boo-yah! That's what
I'm talking about!

180
00:06:52,735 --> 00:06:55,854
That's good.
That's real good.

181
00:06:55,855 --> 00:06:58,190
Where's my 12 french fries?

182
00:07:00,743 --> 00:07:03,328
Seriously, though, I don't feel
right about taking your money.

183
00:07:03,329 --> 00:07:04,579
No, no,
I made the bet,

184
00:07:04,580 --> 00:07:05,831
And you won,
fair and square.

185
00:07:05,832 --> 00:07:07,232
Sandbagger.

186
00:07:07,233 --> 00:07:08,233
What?

187
00:07:08,234 --> 00:07:10,085
Nothing.

188
00:07:10,086 --> 00:07:11,536
Hey, Samuel.

189
00:07:11,537 --> 00:07:13,238
This is molly.
Molly, Samuel.

190
00:07:13,239 --> 00:07:14,840
Oh. Pleased to
meet you, Samuel.

191
00:07:14,841 --> 00:07:16,875
The pleasure is mine.

192
00:07:16,876 --> 00:07:18,577
I heard the large man
was seeing someone,

193
00:07:18,578 --> 00:07:21,246
But I, of course, assumed
you were imaginary.

194
00:07:22,581 --> 00:07:25,100
That's funny.

195
00:07:25,101 --> 00:07:26,101
Samuel's from Africa.

196
00:07:26,102 --> 00:07:28,270
He came here to pursue
the American dream

197
00:07:28,271 --> 00:07:30,639
And, apparently,
to bust my balls.

198
00:07:31,858 --> 00:07:33,692
What part of Africa
are you from?

199
00:07:33,693 --> 00:07:36,027
I grew up in Senegal,
but my mother's from France.

200
00:07:36,028 --> 00:07:37,262
Do you speak french?

201
00:07:37,263 --> 00:07:38,446
Fluently.

202
00:07:38,447 --> 00:07:41,366
I also speak Arabic,
Wolof, and a little German.

203
00:07:41,367 --> 00:07:46,604
Apparently, the only language
he doesn't speak is "waiter."

204
00:07:46,605 --> 00:07:49,107
I speak a little
German, too.

205
00:07:49,108 --> 00:07:51,042
Uh, <i>was ist los
mit der hund</i> ?

206
00:07:51,043 --> 00:07:53,161
<i>Nicht viel,
nur chillen.</i>

207
00:07:56,132 --> 00:07:57,799
I asked him,
"what up, dawg""

208
00:07:57,800 --> 00:08:00,335
And he said,
"not much, just chillin'""

209
00:08:02,555 --> 00:08:03,638
That's good.

210
00:08:03,639 --> 00:08:06,508
You working tonight, or
just doing a floor show?

211
00:08:07,560 --> 00:08:08,910
I'm sorry.

212
00:08:08,911 --> 00:08:11,980
He gets grumpy when he
doesn't get his tiny pancake.

213
00:08:13,015 --> 00:08:16,067
So what are you doing
here in the United States?

214
00:08:16,068 --> 00:08:19,104
I'm studying English literature
at the university of Illinois.

215
00:08:19,105 --> 00:08:20,322
Oh, I love English lit.

216
00:08:20,323 --> 00:08:22,657
It's where I fell in love
with Shakespeare.

217
00:08:22,658 --> 00:08:25,577
Ah! Shakespeare.

218
00:08:25,578 --> 00:08:28,313
"love looks not with the
eyes, but with the mind;

219
00:08:28,314 --> 00:08:32,584
And therefore is winged
cupid painted blind."

220
00:08:32,585 --> 00:08:37,589
"nor hath love's mind
of any judgment taste;

221
00:08:37,590 --> 00:08:44,212
"wings and no eyes,
figure unheedy haste."

222
00:08:47,499 --> 00:08:49,167
Ah, screw it.

223
00:08:49,168 --> 00:08:51,336
Whatever's on the
grill, I'm eating.

224
00:08:53,839 --> 00:08:56,024
Big man gets bitchy
when he's hungry.

225
00:08:57,009 --> 00:08:59,844
Keep a snickers bar
in your purse.

226
00:09:03,482 --> 00:09:05,567
So...What's next?

227
00:09:05,568 --> 00:09:07,235
You want to grab
a drink somewhere?

228
00:09:07,236 --> 00:09:08,320
I don't think so.

229
00:09:08,321 --> 00:09:10,071
You sure? You don't
have to worry about

230
00:09:10,072 --> 00:09:11,957
Blurring the eye
of the tiger anymore.

231
00:09:11,958 --> 00:09:14,192
Yep.

232
00:09:14,193 --> 00:09:15,961
Hey, we could just
drive around a little.

233
00:09:15,962 --> 00:09:17,528
It's a really beautiful night.

234
00:09:17,529 --> 00:09:18,663
Well, you're a schoolteacher

235
00:09:18,664 --> 00:09:21,833
You see the beauty.
I see the crime,

236
00:09:21,834 --> 00:09:24,052
The corruption,
and the danger.

237
00:09:24,053 --> 00:09:26,554
So what do you
think's more dangerous:

238
00:09:26,555 --> 00:09:30,675
The coffee bean and tea leaf
or the Gymboree?

239
00:09:32,345 --> 00:09:34,596
That's funny.

240
00:09:36,816 --> 00:09:38,566
You should just go
right here on wells.

241
00:09:38,567 --> 00:09:40,551
It's a quicker way
to get to my house.

242
00:09:40,552 --> 00:09:42,904
'scuse me, I'm a
Chicago police officer.

243
00:09:42,905 --> 00:09:45,073
I know these streets
like the back of my hand.

244
00:09:45,074 --> 00:09:47,892
Okay. Sorry.

245
00:09:47,893 --> 00:09:49,494
I may not know how
to quote Shakespeare,

246
00:09:49,495 --> 00:09:53,949
But I know I can navigate the
"city of the broad shoulders""

247
00:09:53,950 --> 00:09:55,784
Carl Sandburg.

248
00:09:55,785 --> 00:09:57,502
Great Illinois poet.

249
00:09:57,503 --> 00:10:01,506
I know. I love his poems.

250
00:10:01,507 --> 00:10:04,542
It's actually "city
of the <i>big</i> shoulders."

251
00:10:06,078 --> 00:10:08,797
Broad is fine.
I mean, you get it.

252
00:10:13,602 --> 00:10:16,021
Look, mike, if I've done
anything to upset you--

253
00:10:16,022 --> 00:10:17,522
Night.

254
00:10:19,191 --> 00:10:20,425
Thanks!

255
00:10:20,426 --> 00:10:23,662
I had a nice time.
I guess you'll call me, right?

256
00:10:23,663 --> 00:10:24,896
okay.

257
00:10:24,897 --> 00:10:27,532
You bowl like a girl!

258
00:10:30,436 --> 00:10:31,987
I can't live
without him!

259
00:10:31,988 --> 00:10:34,572
I'm getting too old
for this crap.

260
00:10:34,573 --> 00:10:35,991
You and me both.

261
00:10:42,110 --> 00:10:45,112
Or cinemax in my bedroom.

262
00:10:56,407 --> 00:10:57,841
What are you doing here?

263
00:10:58,876 --> 00:11:00,043
Oh, no.

264
00:11:00,044 --> 00:11:01,211
Come here.

265
00:11:05,633 --> 00:11:06,850
What happened?

266
00:11:06,851 --> 00:11:08,635
It was a complete disaster.

267
00:11:08,636 --> 00:11:10,720
Erectile dysfunction?

268
00:11:13,524 --> 00:11:15,809
No. I didn't even
kiss her good night.

269
00:11:15,810 --> 00:11:17,444
So the penis is fine?

270
00:11:17,445 --> 00:11:19,062
As far as I know.

271
00:11:19,063 --> 00:11:21,898
He's been hiding
for the last couple hours.

272
00:11:21,899 --> 00:11:24,151
Did you tie your shoes
and give her the rear view?

273
00:11:24,152 --> 00:11:25,836
'cause I may have
fudged its attractiveness

274
00:11:25,837 --> 00:11:27,204
Out of friendship.

275
00:11:27,205 --> 00:11:29,772
Carlton, who was at the door?

276
00:11:29,773 --> 00:11:30,991
It's just
mike, grandma.

277
00:11:30,992 --> 00:11:32,542
Go back to bed.

278
00:11:32,543 --> 00:11:34,161
Oh, hey, Michael.

279
00:11:34,162 --> 00:11:35,378
How are you, baby?

280
00:11:35,379 --> 00:11:36,580
I'm all right, nana.

281
00:11:36,581 --> 00:11:37,947
How are you?

282
00:11:37,948 --> 00:11:41,835
Oh, I'm right with my savior
and tight with my bookie.

283
00:11:43,638 --> 00:11:46,423
What are you
doing here, sugar?

284
00:11:46,424 --> 00:11:48,141
Carlton told me you had a date.

285
00:11:48,142 --> 00:11:50,227
Uh, I did,
but it ended pretty early.

286
00:11:50,228 --> 00:11:51,845
Erectile dysfunction?

287
00:11:53,264 --> 00:11:55,899
'cause Carlton got some pills
if you need them.

288
00:11:57,185 --> 00:11:58,351
Uh, grandma,
how many times

289
00:11:58,352 --> 00:12:00,604
Have I asked you to
stay out of my room?

290
00:12:00,605 --> 00:12:02,822
Well, when you start
doing your own laundry,

291
00:12:02,823 --> 00:12:04,407
I will stay out of your room.

292
00:12:04,408 --> 00:12:07,277
Until then,
I go where I please.

293
00:12:07,278 --> 00:12:10,480
Pays $200 a month in rent,
and he gonna start

294
00:12:10,481 --> 00:12:12,699
Telling me where I can
and cannot go.

295
00:12:12,700 --> 00:12:16,986
Now, Michael,
what happened with this girl?

296
00:12:16,987 --> 00:12:20,090
I don't know, we just
didn't make a connection.

297
00:12:20,091 --> 00:12:21,091
I see.

298
00:12:21,092 --> 00:12:23,126
You didn't make a connection.

299
00:12:23,127 --> 00:12:25,595
Yeah, you know, our
world views don't jibe.

300
00:12:25,596 --> 00:12:28,331
Your world views don't jibe.

301
00:12:28,332 --> 00:12:29,633
Exactly.

302
00:12:29,634 --> 00:12:32,519
Now, Michael, I'd like
to see you in heaven, too,

303
00:12:32,520 --> 00:12:35,722
But lying to an old lady
ain't gonna get you there.

304
00:12:37,508 --> 00:12:40,644
She beat me
at bowling.

305
00:12:40,645 --> 00:12:42,529
You're kidding me.

306
00:12:42,530 --> 00:12:44,013
And I don't mean
just beat me,

307
00:12:44,014 --> 00:12:45,682
I mean she covered
my ass in pledge

308
00:12:45,683 --> 00:12:48,702
And wiped the
floor with it.

309
00:12:48,703 --> 00:12:51,037
The bowling alley's
your home court advantage.

310
00:12:51,038 --> 00:12:53,790
Only place you got left
to shine is house of pies.

311
00:12:53,791 --> 00:12:55,742
Hush up, black Gilligan.

312
00:13:02,032 --> 00:13:03,466
Continue, Michael.

313
00:13:03,467 --> 00:13:04,751
It's not just that.

314
00:13:04,752 --> 00:13:07,671
She was showing off and
speaking in different languages.

315
00:13:07,672 --> 00:13:10,257
You mean in tongues,
like the Pentecostals?

316
00:13:10,258 --> 00:13:14,377
No, like french and
German and Shakespeare.

317
00:13:14,378 --> 00:13:15,695
Oh, good.

318
00:13:15,696 --> 00:13:17,480
'cause I been
to a tent revival

319
00:13:17,481 --> 00:13:19,683
And that mess
will put you off Jesus.

320
00:13:19,684 --> 00:13:23,486
So if I hear you correctly,

321
00:13:23,487 --> 00:13:26,156
What's upsetting you
is you had a date with

322
00:13:26,157 --> 00:13:30,527
A smart, capable young woman and
it threatened your masculinity.

323
00:13:30,528 --> 00:13:31,528
No!

324
00:13:31,529 --> 00:13:33,663
No.

325
00:13:33,664 --> 00:13:35,732
Oh, Michael.

326
00:13:35,733 --> 00:13:39,953
Why is it you boys get
so intimidated by strong women?

327
00:13:39,954 --> 00:13:42,622
Want us all to just
be dumb and grateful.

328
00:13:42,623 --> 00:13:44,574
No, I don't
want dumb.

329
00:13:44,575 --> 00:13:47,460
I am a fan of grateful.

330
00:13:47,461 --> 00:13:52,215
Please. When's the last time
you laid your eyes on grateful?

331
00:13:55,136 --> 00:13:58,388
Let me tell you
something, Michael.

332
00:13:58,389 --> 00:14:00,523
You got to embrace the beauty

333
00:14:00,524 --> 00:14:03,293
Of a strong,
independent woman.

334
00:14:03,294 --> 00:14:05,478
If it was me,
I'd cut my losses and move on.

335
00:14:05,479 --> 00:14:07,931
I guess we know why
you're in the bathroom alone

336
00:14:07,932 --> 00:14:11,568
On a Friday night, shaving
your head with my lady Schick.

337
00:14:12,787 --> 00:14:17,457
And I pray to god
that's all you were shaving.

338
00:14:18,626 --> 00:14:21,661
Now, Michael, do you have
feelings for this girl?

339
00:14:21,662 --> 00:14:23,630
Yeah, I guess I do.

340
00:14:23,631 --> 00:14:26,716
Well, then you need to stop
behaving like a petulant child,

341
00:14:26,717 --> 00:14:30,003
Go back to her,
tell her what's in your heart

342
00:14:30,004 --> 00:14:31,755
And how you really feel.

343
00:14:31,756 --> 00:14:34,123
Ugh, really?

344
00:14:34,124 --> 00:14:35,124
Take it from me,

345
00:14:35,125 --> 00:14:36,926
You show her that side of you

346
00:14:36,927 --> 00:14:39,962
And she will open up
like a flower.

347
00:14:39,963 --> 00:14:42,982
'cause there ain't nothing
sexier in the world

348
00:14:42,983 --> 00:14:46,820
Than a secure man
being honest with his woman.

349
00:14:46,821 --> 00:14:48,938
Baby, you coming back to bed?

350
00:14:48,939 --> 00:14:50,156
Now, that man

351
00:14:50,157 --> 00:14:52,442
Is honest
as the day is long.

352
00:14:52,443 --> 00:14:54,911
And had he not been honest,
I never would've

353
00:14:54,912 --> 00:14:56,780
Found out he was long.

354
00:15:03,687 --> 00:15:05,372
God, I love your grandma.

355
00:15:07,375 --> 00:15:09,042
You may not believe this now,

356
00:15:09,043 --> 00:15:11,010
But you'll get
over this man.

357
00:15:11,011 --> 00:15:11,994
Yes, you will.

358
00:15:11,995 --> 00:15:13,179
And you'll find
somebody else.

359
00:15:13,180 --> 00:15:15,882
Somebody sweet and
kind and loving...

360
00:15:15,883 --> 00:15:17,016
And single.

361
00:15:17,017 --> 00:15:18,518
I'd try single for the next one.

362
00:15:18,519 --> 00:15:21,638
You know, just to
shake things up a little bit.

363
00:15:21,639 --> 00:15:24,006
Molly, your sister
is hurting.

364
00:15:24,007 --> 00:15:26,059
This is no time to play
Monday morning quarterback.

365
00:15:26,060 --> 00:15:29,195
She's had one of those,
and he was married!

366
00:15:30,197 --> 00:15:31,848
She has been doing this
her whole life.

367
00:15:31,849 --> 00:15:34,451
She gets mixed-up with
emotionally unavailable men

368
00:15:34,452 --> 00:15:36,286
And then wonders
why they broke her heart.

369
00:15:36,287 --> 00:15:39,906
He's not emotionally
unavailable, he's an alcoholic.

370
00:15:39,907 --> 00:15:42,125
There's a difference.

371
00:15:42,126 --> 00:15:43,293
My mistake.

372
00:15:43,294 --> 00:15:44,544
Oh, he's drunk and married.

373
00:15:44,545 --> 00:15:47,914
That's a guy you can count on.

374
00:15:47,915 --> 00:15:51,000
Why are you being
so mean to me?

375
00:15:51,001 --> 00:15:53,503
Is she on a new diet?

376
00:15:53,504 --> 00:15:56,172
Yeah, I'm on the "not swallowing
any more of your crap" diet.

377
00:15:56,173 --> 00:16:00,059
Oh, mom, please, give her
a cookie or something.

378
00:16:00,060 --> 00:16:01,811
Molly, you need
to calm down

379
00:16:01,812 --> 00:16:04,264
And just be supportive
of your sister.

380
00:16:04,265 --> 00:16:05,598
Are you kidding?

381
00:16:05,599 --> 00:16:08,685
That's all I ever do around here
is support my sister.

382
00:16:08,686 --> 00:16:09,903
What about me?

383
00:16:09,904 --> 00:16:11,821
Who supports me
when I'm hurting?

384
00:16:11,822 --> 00:16:12,989
Why are you hurting?

385
00:16:12,990 --> 00:16:14,274
Oh, I don't know.

386
00:16:14,275 --> 00:16:16,860
Maybe because I had a date
with a guy I really liked--

387
00:16:16,861 --> 00:16:19,913
A single guy-- and it was
a complete train wreck.

388
00:16:19,914 --> 00:16:21,063
Oh, well.

389
00:16:21,064 --> 00:16:22,565
You'll be fine.
You're a rock.

390
00:16:22,566 --> 00:16:23,950
I don't want to be
the rock anymore.

391
00:16:23,951 --> 00:16:26,169
Let somebody else be the rock!

392
00:16:26,170 --> 00:16:29,205
I want to be the hot one
that gets taken care of

393
00:16:29,206 --> 00:16:31,624
And everybody wants
to have sex with.

394
00:16:31,625 --> 00:16:34,511
Oh, molly, it's not
as glamorous as it looks.

395
00:16:35,880 --> 00:16:38,047
It's really not.

396
00:16:41,385 --> 00:16:43,303
Molly, I'm really sorry
about how I behaved,

397
00:16:43,304 --> 00:16:45,305
But you got to keep in mind
that I'm a cop.

398
00:16:45,306 --> 00:16:46,856
My life is on the line
every day,

399
00:16:46,857 --> 00:16:48,591
And that can wind a man up
pretty tight.

400
00:16:48,592 --> 00:16:50,109
So sometimes you get
the teddy bear

401
00:16:50,110 --> 00:16:52,395
And sometimes you get
the rattlesnake.

402
00:16:52,396 --> 00:16:54,447
That's good.

403
00:16:54,448 --> 00:16:56,332
Okay, let's do this.

404
00:16:56,333 --> 00:16:57,617
Victoria, what
are you doing

405
00:16:57,618 --> 00:16:58,818
With that hair
spray and lighter?!

406
00:16:58,819 --> 00:16:59,903
Back off!

407
00:16:59,904 --> 00:17:01,020
Molly, code blue!

408
00:17:01,021 --> 00:17:02,856
What the hell
is going on?

409
00:17:02,857 --> 00:17:05,458
I'm gonna set that bastard on
fire and you can't stop me.

410
00:17:06,577 --> 00:17:07,627
Oh, my god!

411
00:17:07,628 --> 00:17:09,162
Put it down!

412
00:17:09,163 --> 00:17:10,129
Step aside, ladies,
I got this.

413
00:17:10,130 --> 00:17:11,164
Mike, what are
you doing here?

414
00:17:11,165 --> 00:17:12,332
Not now.
Let me do my job.

415
00:17:12,333 --> 00:17:14,717
Victoria, give me the hairspray
and the lighter.

416
00:17:14,718 --> 00:17:17,337
No, he's got to pay!

417
00:17:17,338 --> 00:17:19,756
Listen, I understand
you're upset,

418
00:17:19,757 --> 00:17:21,808
But revenge is not the answer.

419
00:17:21,809 --> 00:17:24,293
I loved him,

420
00:17:24,294 --> 00:17:26,646
And he broke my heart!

421
00:17:26,647 --> 00:17:28,898
I understand, sweetie.

422
00:17:28,899 --> 00:17:33,302
But sometimes we all suffer
disappointment and humiliation.

423
00:17:33,303 --> 00:17:37,740
Can't help it if
I'm a good bowler.

424
00:17:38,943 --> 00:17:40,243
You're a great bowler,

425
00:17:40,244 --> 00:17:42,245
And I'll get
back to you on that.

426
00:17:42,246 --> 00:17:46,616
Listen, I know guys act
really stupid sometimes.

427
00:17:46,617 --> 00:17:48,201
I certainly did tonight.

428
00:17:49,620 --> 00:17:52,255
Okay.

429
00:17:52,256 --> 00:17:53,756
But that's not an excuse

430
00:17:53,757 --> 00:17:56,826
To turn some guy's face
into burnt lasagna.

431
00:17:56,827 --> 00:17:58,678
Okay?

432
00:17:58,679 --> 00:18:00,346
What it really is,
is an opportunity

433
00:18:00,347 --> 00:18:03,049
To practice forgiveness.

434
00:18:03,050 --> 00:18:07,103
So why don't you give me the
lighter and we'll talk about it?

435
00:18:08,138 --> 00:18:09,672
Come on, Victoria, please?

436
00:18:11,475 --> 00:18:15,011
That a girl.

437
00:18:15,012 --> 00:18:17,346
Hostage training at the academy.

438
00:18:17,347 --> 00:18:18,698
Okay, now, listen-- oh!

439
00:18:20,851 --> 00:18:23,987
Get back in this house!

440
00:18:23,988 --> 00:18:25,705
That was really sweet.

441
00:18:25,706 --> 00:18:27,790
I'll flush your eyes out
when I get back.

442
00:18:27,791 --> 00:18:29,859
Ah.

443
00:18:29,860 --> 00:18:32,028
That's good, she still likes me.

444
00:18:40,316 --> 00:18:41,500
Yes!

445
00:18:43,286 --> 00:18:45,621
Sorry, I get excited.

446
00:18:47,090 --> 00:18:48,090
It's okay.

447
00:18:48,091 --> 00:18:49,658
You're a wonderful bowler.

448
00:18:49,659 --> 00:18:50,676
Well, in that case,

449
00:18:50,677 --> 00:18:51,743
Boo-yah!

450
00:18:51,744 --> 00:18:54,346
That's how I roll!

451
00:18:54,347 --> 00:18:56,581
All right, all right,
don't push the cute.

452
00:18:57,750 --> 00:18:58,967
Where are we at?

453
00:18:58,968 --> 00:19:01,720
You're up, like, 38
pins, give or take.

454
00:19:01,721 --> 00:19:04,223
Well, you can still
turn things around.

455
00:19:11,114 --> 00:19:12,114
Whatcha doing?

456
00:19:12,115 --> 00:19:13,782
I'm turning
things around.

457
00:19:15,601 --> 00:19:18,120
Attaboy, mike!

458
00:19:18,121 --> 00:19:24,121
<font color="#00ffff">-- Sync by Rafael UPD --</font>
<font color="#00ffff">www.addic7ed.com/</font>

