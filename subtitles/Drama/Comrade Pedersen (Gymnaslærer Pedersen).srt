1
00:00:05,640 --> 00:00:08,241
So, Jon, when did your interest
in Comrade Detective begin?

2
00:00:08,308 --> 00:00:11,445
Oh, when I was doing my
documentary on Stanley Kubrick.

3
00:00:11,512 --> 00:00:13,013
It's called Stanley Kubrick's Boxes.

4
00:00:13,080 --> 00:00:16,016
I actually found a copy
of Comrade Detective

5
00:00:16,083 --> 00:00:17,350
in one of his boxes.

6
00:00:17,417 --> 00:00:19,119
- Come on. No way.
- Yeah, yeah.

7
00:00:19,186 --> 00:00:20,120
Kubrick had a copy.

8
00:00:20,187 --> 00:00:22,022
There's one mention of Comrade Detective

9
00:00:22,089 --> 00:00:26,026
in his diaries, from March 13, 1991.

10
00:00:26,093 --> 00:00:29,029
He wrote, "We watched episode three

11
00:00:29,096 --> 00:00:31,632
of Comrade Detective over dinner."

12
00:00:31,699 --> 00:00:34,401
So, Stanley Kubrick was a fan?

13
00:00:34,468 --> 00:00:36,570
A fan and a student. Yeah.

14
00:00:36,637 --> 00:00:37,971
Wow. That's crazy.

15
00:00:38,038 --> 00:00:40,173
- Should we keep watching?
- Absolutely.

16
00:00:44,678 --> 00:00:46,514
The Father, the Son

17
00:00:46,580 --> 00:00:49,116
and the Holy Ghost.

18
00:00:49,182 --> 00:00:52,920
The Father, the Son and the Holy Ghost.

19
00:00:54,755 --> 00:00:57,558
The Father, the Son and the Holy Ghost.

20
00:00:57,625 --> 00:01:00,494
The Father, the Son

21
00:01:00,561 --> 00:01:01,829
and the Holy Ghost.

22
00:01:01,895 --> 00:01:04,965
The Father, the Son and the Holy Ghost.

23
00:01:05,032 --> 00:01:06,233
Bucharest PD!

24
00:01:06,299 --> 00:01:09,436
Put your hands on your
head, turn around, slowly.

25
00:01:09,503 --> 00:01:10,904
Turn around! Turn around!

26
00:01:10,971 --> 00:01:13,607
Our Father who art in heaven,

27
00:01:13,674 --> 00:01:16,176
hallowed be thy name.

28
00:01:16,243 --> 00:01:17,811
Thy kingdom come...

29
00:01:17,878 --> 00:01:19,980
- The guy's got a knife.
- ... thy will be done...

30
00:01:20,047 --> 00:01:21,214
Drop it, motherfucker. Drop it!

31
00:01:21,281 --> 00:01:22,215
... on Earth as it is in heaven.

32
00:01:22,282 --> 00:01:24,384
Give us this day our daily bread

33
00:01:24,451 --> 00:01:27,420
and forgive us our trespasses...

34
00:01:27,487 --> 00:01:28,889
- Put it down!
- ... as we forgive those

35
00:01:28,956 --> 00:01:30,490
- who trespass against us.
- Don't be a hero.

36
00:01:30,558 --> 00:01:32,059
- And God said...
- He's gonna do it!

37
00:01:32,125 --> 00:01:34,394
- I don't believe it!
- ... let there be blood!

38
00:01:36,029 --> 00:01:37,798
Holy shit!

39
00:01:42,502 --> 00:01:44,437
- Look at this.
- Oh, geez. What the... ?

40
00:01:44,504 --> 00:01:46,106
Look at the...

41
00:01:46,173 --> 00:01:47,808
What the... ?

42
00:02:09,963 --> 00:02:13,433
♪ And I hold them like a jewel ♪

43
00:02:14,968 --> 00:02:17,404
♪ And they are colors ♪

44
00:02:17,470 --> 00:02:20,874
♪ With an ancient fame ♪

45
00:02:20,941 --> 00:02:25,112
♪ Of a brave nation. ♪

46
00:02:26,580 --> 00:02:28,015
Hey there. He's here.

47
00:02:28,081 --> 00:02:31,284
Come... come have a
drink with me, partner.

48
00:02:31,351 --> 00:02:33,086
Constantine!

49
00:02:33,153 --> 00:02:35,789
Give my partner a drink!

50
00:02:35,856 --> 00:02:37,825
You been here all night?

51
00:02:37,891 --> 00:02:40,560
And I'll be here all day.

52
00:02:40,628 --> 00:02:43,897
Now, come on, goat fucker, have a drink.

53
00:02:43,964 --> 00:02:46,166
Nikita would have a drink.

54
00:02:46,233 --> 00:02:49,870
Where is Nikita?

55
00:02:49,937 --> 00:02:52,172
Hmm?

56
00:02:52,239 --> 00:02:54,207
Nikita!

57
00:02:54,274 --> 00:02:56,309
Nikita!

58
00:02:56,376 --> 00:02:57,945
Nikita...

59
00:03:19,199 --> 00:03:22,836
Captain Covaci officially
reopened Nikita's case.

60
00:03:22,903 --> 00:03:25,272
We have a chance to bring
the killer to justice,

61
00:03:25,338 --> 00:03:28,275
but we won't find him at the
bottom of a bottle, Gregor.

62
00:03:30,711 --> 00:03:33,146
I miss him, Joseph.

63
00:03:35,783 --> 00:03:37,384
Yeah, me, too.

64
00:03:37,450 --> 00:03:39,887
But he's dead, and there's no afterlife.

65
00:03:39,953 --> 00:03:41,822
So we'll never see him again.

66
00:03:41,889 --> 00:03:45,759
But he can live on in our good deeds.

67
00:03:46,827 --> 00:03:48,962
You're right.

68
00:03:49,863 --> 00:03:52,332
You're right.

69
00:03:59,740 --> 00:04:01,474
Don't you ever do that again.

70
00:04:01,541 --> 00:04:03,210
Goat fucker.

71
00:04:16,456 --> 00:04:19,592
In the name of the Father and the Son

72
00:04:19,659 --> 00:04:21,194
and the Holy Spirit, amen.

73
00:04:21,261 --> 00:04:23,530
Forgive me, Father, for I have sinned.

74
00:04:23,596 --> 00:04:26,633
It has been one week
since my last confession.

75
00:04:26,700 --> 00:04:29,669
I have committed multiple murders.

76
00:04:29,737 --> 00:04:33,741
Do you accept Jesus
Christ as your savior?

77
00:04:34,775 --> 00:04:36,509
I do.

78
00:04:36,576 --> 00:04:40,480
Good. Now say five Our
Fathers and two Hail Marys.

79
00:04:40,547 --> 00:04:42,515
I will do more than that.

80
00:04:42,582 --> 00:04:45,385
I will build you a
church so big and awesome

81
00:04:45,452 --> 00:04:48,288
that all who enter will
be forced to their knees.

82
00:04:48,355 --> 00:04:51,859
Then Romania will know
the true power of the Lord.

83
00:04:51,925 --> 00:04:54,127
You are a broken vessel,

84
00:04:54,194 --> 00:04:57,865
but I believe you have
been sent by the Almighty.

85
00:04:57,931 --> 00:05:00,067
Bless you, my son.

86
00:05:28,796 --> 00:05:32,966
Eh, he paid his rent on time,
he wasn't loud, kept to himself.

87
00:05:37,704 --> 00:05:39,940
What is all this crap?

88
00:05:40,007 --> 00:05:41,508
Thanks, lady, but we got it from here.

89
00:05:41,574 --> 00:05:44,344
- Okay. Thanks.
- Oh, watch it. I'm spoken for.

90
00:05:44,411 --> 00:05:47,414
What kind of freak lives like this?

91
00:05:47,480 --> 00:05:50,683
I mean, look at this shit.

92
00:05:50,750 --> 00:05:52,319
Check it out.

93
00:05:52,385 --> 00:05:55,455
Bibles. In English.

94
00:05:55,522 --> 00:05:57,190
What do you think?

95
00:05:57,257 --> 00:06:00,593
I think we got some religious
nut who's smuggling in Bibles.

96
00:06:00,660 --> 00:06:03,663
When he got caught, he killed
himself to avoid jail time.

97
00:06:03,730 --> 00:06:04,965
Open and shut.

98
00:06:05,032 --> 00:06:08,401
Open and shut. Hmm.

99
00:06:08,468 --> 00:06:09,803
Hey, are you hungry?

100
00:06:09,870 --> 00:06:11,171
Yeah.

101
00:06:11,238 --> 00:06:13,606
Let's get lunch.

102
00:06:13,673 --> 00:06:15,475
Hmm. Excuse me.

103
00:06:34,294 --> 00:06:35,562
Carry the one...

104
00:06:35,628 --> 00:06:37,530
Okay, very funny.

105
00:06:40,834 --> 00:06:42,535
All right, well,

106
00:06:42,602 --> 00:06:45,272
I just got to send them
in. I'll just take my lunch.

107
00:06:45,338 --> 00:06:49,142
Great, here come these
two fucking assholes.

108
00:06:49,209 --> 00:06:50,844
Oh, I heard you guys
closed a case before lunch.

109
00:06:50,911 --> 00:06:52,079
Congratulations.

110
00:06:52,145 --> 00:06:53,546
Yeah, Marku Miklos.

111
00:06:53,613 --> 00:06:55,916
Religious nut. Slit
his throat this morning.

112
00:06:55,983 --> 00:06:57,184
See?

113
00:06:57,250 --> 00:06:59,519
That's how it's done, fuck-o.

114
00:06:59,586 --> 00:07:03,056
The report said he has a stash
of English-language Bibles.

115
00:07:03,123 --> 00:07:04,992
Nothing about that
seemed strange to you?

116
00:07:05,058 --> 00:07:07,627
So the guy's into creepy Christian shit.

117
00:07:07,694 --> 00:07:09,096
What's strange about that, huh?

118
00:07:09,162 --> 00:07:11,164
Well, in case you
forgot, Nikita was killed

119
00:07:11,231 --> 00:07:13,833
by a man with ties to an
international smuggling ring.

120
00:07:13,901 --> 00:07:16,369
The guy was acting alone, fellas.

121
00:07:16,436 --> 00:07:17,670
Go bark up another tree.

122
00:07:17,737 --> 00:07:20,473
Yeah, some loner sneaking
in Bibles eventually

123
00:07:20,540 --> 00:07:22,943
gets curious and gets hooked
on the same shit he's pushing.

124
00:07:23,010 --> 00:07:24,344
Happens all the time.

125
00:07:24,411 --> 00:07:26,279
Yeah, yeah, no, I'm sure you're right.

126
00:07:26,346 --> 00:07:28,581
- Of course I'm right.
- Yeah.

127
00:07:28,648 --> 00:07:29,382
Hey!

128
00:07:29,449 --> 00:07:30,753
Hey!

129
00:07:30,754 --> 00:07:31,881
- Stop, please!
- You call yourself a detective,

130
00:07:31,885 --> 00:07:33,353
you ginger piece of shit?

131
00:07:33,420 --> 00:07:35,115
- Let go! Let go!
- We questioned his landlady.

132
00:07:35,116 --> 00:07:36,990
He had no friends, kept to
himself, just him and his religion.

133
00:07:37,057 --> 00:07:38,625
The case is closed!

134
00:07:38,691 --> 00:07:40,327
End of story!

135
00:07:40,393 --> 00:07:42,262
- Stop, please!
- Give us the file.

136
00:07:42,329 --> 00:07:43,863
You like jerking off
with your left hand?

137
00:07:43,931 --> 00:07:45,332
No! Give him the fucking file!

138
00:07:47,734 --> 00:07:49,669
- Aah, give him the file!
- Fucker.

139
00:07:56,243 --> 00:07:58,178
Ah, worked at a neighborhood bakery.

140
00:07:58,245 --> 00:07:59,646
Did you question his coworkers?

141
00:07:59,712 --> 00:08:01,048
Fuck you.

142
00:08:01,114 --> 00:08:03,783
Enjoy your soup.

143
00:08:03,850 --> 00:08:06,319
Motherfucker.

144
00:08:14,161 --> 00:08:16,096
Good afternoon, gentlemen.

145
00:08:16,163 --> 00:08:18,431
I'm Detective Baciu and
this is Detective Anghel.

146
00:08:18,498 --> 00:08:20,433
We're looking for
information on Marku Miklos.

147
00:08:30,677 --> 00:08:33,246
Marku Miklos.

148
00:08:33,313 --> 00:08:35,348
Yes.

149
00:08:35,415 --> 00:08:38,986
I was saddened to hear of his passing.

150
00:08:39,052 --> 00:08:41,154
Still, I wasn't surprised.

151
00:08:42,189 --> 00:08:45,292
No? And why's that?

152
00:08:45,358 --> 00:08:47,460
He was a strange young man.

153
00:08:47,527 --> 00:08:48,728
Always praying.

154
00:08:48,795 --> 00:08:50,797
If it rained, he prayed.

155
00:08:50,863 --> 00:08:52,932
If he was late, he prayed.

156
00:08:53,000 --> 00:08:55,102
If he couldn't find his keys, he...

157
00:08:55,168 --> 00:08:56,769
He prayed, right.

158
00:08:56,836 --> 00:08:59,839
And you don't share his
interest in the Christian occult?

159
00:08:59,906 --> 00:09:02,075
Comrades...

160
00:09:02,142 --> 00:09:04,511
I'm just a baker.

161
00:09:04,577 --> 00:09:08,248
And Marku, he was a troubled boy.

162
00:09:08,315 --> 00:09:11,985
He spoke constantly of his faith.

163
00:09:12,052 --> 00:09:14,587
And why exactly didn't
you inform on him?

164
00:09:21,028 --> 00:09:24,031
How about a slice of bread?

165
00:09:28,335 --> 00:09:29,369
Hmm.

166
00:09:32,239 --> 00:09:36,176
Marku spoke often about
transubstantiation.

167
00:09:36,243 --> 00:09:38,345
Are you gents familiar with the term?

168
00:09:38,411 --> 00:09:41,114
Yeah. Transubstantiation.

169
00:09:41,181 --> 00:09:43,416
In countries that are slaves
to the Christian faith,

170
00:09:43,483 --> 00:09:45,718
they eat bread during
ritualized worship.

171
00:09:45,785 --> 00:09:47,054
They believe that the bread

172
00:09:47,120 --> 00:09:49,389
literally turns into the body of Christ.

173
00:09:49,456 --> 00:09:50,557
Correct.

174
00:09:50,623 --> 00:09:52,359
Think about that.

175
00:09:52,425 --> 00:09:54,861
One second, you're chewing on my bread,

176
00:09:54,927 --> 00:09:57,430
and the next moment, that
very same bread is transformed

177
00:09:57,497 --> 00:10:00,100
into the flesh of Jesus Christ.

178
00:10:00,167 --> 00:10:03,002
An absurd superstition.

179
00:10:03,070 --> 00:10:04,804
Let's hope so,

180
00:10:04,871 --> 00:10:10,810
or the two of you would've
just eaten the flesh of a man.

181
00:10:33,066 --> 00:10:35,001
Well, that guy's a fucking weirdo.

182
00:10:35,068 --> 00:10:37,204
Let's pull his file.

183
00:10:40,273 --> 00:10:42,209
His name's Anton Streza.

184
00:10:42,275 --> 00:10:44,077
Arrested eight years ago
for burglary and assault.

185
00:10:44,144 --> 00:10:45,712
Did six years in prison.

186
00:10:45,778 --> 00:10:48,415
Once he was released,
got a job at the bakery.

187
00:10:48,481 --> 00:10:52,319
Kept a low profile ever since.

188
00:10:52,385 --> 00:10:55,255
Gregor.

189
00:10:55,322 --> 00:10:57,624
Look who the arresting officer was.

190
00:11:01,961 --> 00:11:04,764
Detective Nikita Ionescu.

191
00:11:10,703 --> 00:11:13,840
Could six years in prison
drive a man to murder?

192
00:11:13,906 --> 00:11:17,144
Well, it certainly wouldn't
result in friendship.

193
00:11:18,345 --> 00:11:21,848
We should take a closer
look at this Streza.

194
00:11:33,260 --> 00:11:36,296
What's this guy's game?

195
00:11:44,371 --> 00:11:48,608
Farooota, mika,

196
00:11:48,675 --> 00:11:51,578
shooooo,

197
00:11:51,644 --> 00:11:56,716
maliki maka.

198
00:12:02,054 --> 00:12:03,190
Idiot.

199
00:12:09,429 --> 00:12:12,232
The body of Christ. The body of Christ.

200
00:12:13,400 --> 00:12:14,601
The body of Christ.

201
00:12:20,106 --> 00:12:22,074
For thine is the kingdom,

202
00:12:22,141 --> 00:12:24,076
and the power, and the glory,

203
00:12:24,143 --> 00:12:26,413
for ever and ever.

204
00:12:26,479 --> 00:12:27,414
Aah... !

205
00:12:44,497 --> 00:12:46,098
Gregor. Hey, wake up.

206
00:12:46,165 --> 00:12:48,301
What?

207
00:12:48,368 --> 00:12:49,802
He's on the move.

208
00:13:53,065 --> 00:13:56,269
♪ Three colors I know in the world ♪

209
00:13:56,336 --> 00:13:58,805
♪ And I hold them like a jewel ♪

210
00:13:58,871 --> 00:14:01,608
♪ They are colors with an ancient fame ♪

211
00:14:01,674 --> 00:14:04,076
♪ Reminders of our brave nation ♪

212
00:14:04,143 --> 00:14:09,148
♪ Red is the fire that
permeates my heart ♪

213
00:14:09,215 --> 00:14:12,051
♪ Which is full of longing ♪

214
00:14:12,118 --> 00:14:15,922
♪ For holy liberty... ♪

215
00:14:20,527 --> 00:14:23,663
♪ Shall be our future ♪

216
00:14:23,730 --> 00:14:26,666
♪ Always in eternal blossom ♪

217
00:14:26,733 --> 00:14:29,536
♪ And unwithered radiance... ♪

218
00:14:57,330 --> 00:14:59,031
Is he hurt?

219
00:15:17,450 --> 00:15:19,852
Hello, comrade.

220
00:15:26,025 --> 00:15:28,027
You don't think we know where you live?

221
00:15:28,094 --> 00:15:31,230
We got your file, asshole.

222
00:15:31,297 --> 00:15:32,432
Oh...

223
00:15:33,900 --> 00:15:37,704
Gideon Bible, straight from the USA.

224
00:15:37,770 --> 00:15:40,373
Some heavy shit for a baker.

225
00:15:40,440 --> 00:15:42,875
Congratulations, pal,
you're under arrest.

226
00:15:51,451 --> 00:15:54,020
All right, bread boy, what are these?

227
00:15:57,857 --> 00:16:01,327
They're books.

228
00:16:01,394 --> 00:16:05,532
Books that hold the Word of God.

229
00:16:05,598 --> 00:16:07,900
We know what Bibles are, Streza.

230
00:16:07,967 --> 00:16:09,902
It's my favorite work of fiction.

231
00:16:09,969 --> 00:16:12,772
We found these same
Bibles on Marku Miklos.

232
00:16:12,839 --> 00:16:14,507
Where are you getting them?

233
00:16:16,643 --> 00:16:20,279
We prayed for them...

234
00:16:20,346 --> 00:16:22,682
and God answered our prayers.

235
00:16:22,749 --> 00:16:23,916
Hmm.

236
00:16:25,384 --> 00:16:28,588
Marku Miklos committed suicide.

237
00:16:28,655 --> 00:16:30,156
Doesn't that mean he's destined

238
00:16:30,222 --> 00:16:32,825
to spending eternity in hell?

239
00:16:37,930 --> 00:16:41,668
God has a tremendous
capacity for forgiveness,

240
00:16:41,734 --> 00:16:44,336
unless you're a nonbeliever, Detective.

241
00:16:44,403 --> 00:16:47,874
Then you go straight to hell.

242
00:16:50,376 --> 00:16:52,979
How convenient.

243
00:16:53,045 --> 00:16:57,349
So, people smart enough to know
there's no such thing as God,

244
00:16:57,416 --> 00:16:59,385
we're the ones who should be worried.

245
00:16:59,452 --> 00:17:02,555
You cannot prove there is no God.

246
00:17:02,622 --> 00:17:05,124
That's because you
can't prove a negative.

247
00:17:05,191 --> 00:17:09,629
Absence of evidence is
not evidence of absence.

248
00:17:09,696 --> 00:17:12,665
All I know is God is good.

249
00:17:14,033 --> 00:17:14,967
Is he?

250
00:17:16,469 --> 00:17:19,839
Then explain war, rape.

251
00:17:23,510 --> 00:17:26,879
Explain death and suffering.

252
00:17:26,946 --> 00:17:29,482
Explain the horrors
man has had to endure

253
00:17:29,549 --> 00:17:31,383
since the beginning of time.

254
00:17:31,450 --> 00:17:34,020
God works in mysterious ways.

255
00:17:35,588 --> 00:17:38,891
And that's all I'm going to say.

256
00:18:04,150 --> 00:18:05,818
That crazy fuck's not
gonna give us anything.

257
00:18:05,885 --> 00:18:07,754
How do you suppose someone
even gets wrapped up

258
00:18:07,820 --> 00:18:09,989
in all this bullshit?

259
00:18:15,294 --> 00:18:16,863
The universe is complex.

260
00:18:16,929 --> 00:18:18,831
Religion offers simple-minded people

261
00:18:18,898 --> 00:18:20,867
easily digestible answers.

262
00:18:20,933 --> 00:18:22,935
It takes a great deal
of study to understand

263
00:18:23,002 --> 00:18:25,471
how we rotate around
the sun, or to comprehend

264
00:18:25,538 --> 00:18:27,406
the gravitational pull of our orbit.

265
00:18:27,473 --> 00:18:30,209
It's much easier to just be told that...

266
00:18:30,276 --> 00:18:32,378
God brings the sun out in the morning

267
00:18:32,444 --> 00:18:33,680
and puts it away at night.

268
00:18:33,746 --> 00:18:35,748
You make it sound so innocent,

269
00:18:35,815 --> 00:18:38,184
like children who
believe in fairy tales.

270
00:18:38,250 --> 00:18:40,186
Yeah, but don't forget,

271
00:18:40,252 --> 00:18:41,588
every fairy tale has a monster.

272
00:18:49,328 --> 00:18:52,632
What the fuck is she doing here?

273
00:18:54,333 --> 00:18:56,302
Hello, Detectives.

274
00:18:56,368 --> 00:18:59,639
As acting interim ambassador
to the United States,

275
00:18:59,706 --> 00:19:02,141
I'm demanding the
release of Father Streza.

276
00:19:02,208 --> 00:19:04,543
Father Streza?

277
00:19:04,611 --> 00:19:06,445
Yes. He is a priest merely exercising

278
00:19:06,512 --> 00:19:08,781
his fundamental human right
of practicing religion.

279
00:19:08,848 --> 00:19:11,984
Okay, health care is a
fundamental human right.

280
00:19:12,051 --> 00:19:17,123
Believing in an imaginary
God is a sign of insanity.

281
00:19:17,189 --> 00:19:20,192
I have here a letter
signed by world leaders

282
00:19:20,259 --> 00:19:22,829
calling for his release.

283
00:19:26,098 --> 00:19:29,235
England, Canada, Australia, France...

284
00:19:29,301 --> 00:19:30,670
Wales?

285
00:19:30,737 --> 00:19:32,071
You gotta be fucking kidding me

286
00:19:32,138 --> 00:19:34,206
with these bullshit countries.

287
00:19:34,273 --> 00:19:35,507
I'm just doing my job.

288
00:19:35,574 --> 00:19:37,944
And you've done it. Now you can go.

289
00:19:38,010 --> 00:19:41,147
This isn't over, Detectives.

290
00:19:42,782 --> 00:19:45,351
Great, now we got a ticking clock.

291
00:19:45,417 --> 00:19:47,119
They're going to apply
international pressure.

292
00:19:47,186 --> 00:19:50,189
How we gonna get this priest to talk?

293
00:19:50,256 --> 00:19:51,624
You know what, Joseph?

294
00:19:51,691 --> 00:19:53,292
You've been working too hard.

295
00:19:53,359 --> 00:19:54,661
Go home to your family.

296
00:19:54,727 --> 00:19:56,262
What are you planning?

297
00:19:57,664 --> 00:20:00,232
I'm gonna set a herring
to catch a whale.

298
00:20:06,472 --> 00:20:08,207
Good night.

299
00:20:08,274 --> 00:20:09,308
Good night, Roy.

300
00:20:09,375 --> 00:20:11,310
Good night, madam.

301
00:20:13,680 --> 00:20:16,282
Uh...

302
00:20:16,348 --> 00:20:18,350
What are you doing here?

303
00:20:21,120 --> 00:20:22,655
You hungry?

304
00:20:46,178 --> 00:20:49,348
I missed you.

305
00:20:49,415 --> 00:20:51,117
I missed you, my love.

306
00:20:54,620 --> 00:20:56,288
Ooh!

307
00:20:58,390 --> 00:21:00,159
Mmm.

308
00:21:02,294 --> 00:21:05,031
This bean soup with ham hock is so good.

309
00:21:05,097 --> 00:21:06,933
I told you, it's the best in the city.

310
00:21:06,999 --> 00:21:09,501
Bean soup is a delicacy.

311
00:21:09,568 --> 00:21:11,838
When I go home, I'm really
going to miss the food

312
00:21:11,904 --> 00:21:13,239
in this country.

313
00:21:14,907 --> 00:21:17,209
I've never eaten so well.

314
00:21:18,544 --> 00:21:21,347
You miss the United States?

315
00:21:22,581 --> 00:21:24,483
I do.

316
00:21:24,550 --> 00:21:27,386
I mean, I don't miss the poverty

317
00:21:27,453 --> 00:21:32,825
or the crime or the...
racial inequality.

318
00:21:32,892 --> 00:21:36,062
I certainly don't miss AIDS.

319
00:21:36,128 --> 00:21:40,099
Everyone in the U.S. seems to have AIDS.

320
00:21:40,166 --> 00:21:42,668
But America is still the
greatest country in the world.

321
00:21:42,735 --> 00:21:44,636
Hmm.

322
00:21:44,703 --> 00:21:46,072
What? It's true.

323
00:21:46,138 --> 00:21:48,908
I mean, Romania has
some nice things, too.

324
00:21:48,975 --> 00:21:50,476
Like, the literacy rate is amazing.

325
00:21:52,311 --> 00:21:57,283
Everyone is so good-looking
and has such a youthful spirit.

326
00:21:57,349 --> 00:21:59,886
But...

327
00:21:59,952 --> 00:22:02,822
I miss my home.

328
00:22:02,889 --> 00:22:05,424
I miss my country.

329
00:22:05,491 --> 00:22:08,494
I think you're just lonely.

330
00:22:10,797 --> 00:22:13,499
Yes.

331
00:22:13,565 --> 00:22:15,167
Maybe I am.

332
00:22:19,071 --> 00:22:21,373
Why aren't you with someone, Jane?

333
00:22:21,440 --> 00:22:24,110
Why aren't you?

334
00:22:25,744 --> 00:22:28,547
Because life is too short.

335
00:22:32,084 --> 00:22:33,719
Hmm.

336
00:22:33,786 --> 00:22:35,121
You're trouble.

337
00:22:36,688 --> 00:22:37,857
Hmm?

338
00:22:37,924 --> 00:22:42,694
Unfortunately, I'm a good American girl

339
00:22:42,761 --> 00:22:47,366
and I don't sleep with
strange, Eastern European men

340
00:22:47,433 --> 00:22:49,501
on the first date.

341
00:22:49,568 --> 00:22:51,670
You want to bet?

342
00:24:34,573 --> 00:24:38,177
Andrei? Andrei?

343
00:24:38,244 --> 00:24:41,480
What?

344
00:24:41,547 --> 00:24:42,949
Where's your sister?

345
00:24:43,015 --> 00:24:45,918
She made me promise not to tell.

346
00:24:45,985 --> 00:24:50,122
Well, that's a promise
you have to break.

347
00:24:50,189 --> 00:24:53,192
It's your duty to inform
on your sister, Andrei.

348
00:24:53,259 --> 00:24:57,029
And do the right thing.

349
00:24:57,096 --> 00:24:59,498
♪ Are you ready for the sex girls? ♪

350
00:24:59,565 --> 00:25:02,401
♪ The fine, fine, super
special, real sex girls ♪

351
00:25:02,468 --> 00:25:06,305
♪ Are you ready for the best girls? ♪

352
00:25:06,372 --> 00:25:08,507
♪ The knees-showin', knees-
showin' big-breast girls. ♪

353
00:25:15,247 --> 00:25:19,785
I... I... I was just listening
for a second, that's all.

354
00:25:19,851 --> 00:25:22,121
I... I didn't mean... I...

355
00:25:38,270 --> 00:25:40,706
Listen, it's okay,
Flavia, I'm not angry.

356
00:25:40,772 --> 00:25:42,741
You're not?

357
00:25:42,808 --> 00:25:44,943
No.

358
00:25:48,714 --> 00:25:51,083
You're almost a grown woman,

359
00:25:51,150 --> 00:25:53,986
and so I'm gonna speak
to you like an adult.

360
00:25:59,491 --> 00:26:02,328
Flavia, do you know what a whore is?

361
00:26:02,394 --> 00:26:06,798
It's... it's a woman who
sleeps with men for money.

362
00:26:06,865 --> 00:26:08,000
Exactly.

363
00:26:08,067 --> 00:26:09,601
And do you think

364
00:26:09,668 --> 00:26:12,304
that there are little girls
out there that dream of one day

365
00:26:12,371 --> 00:26:14,240
growing up to become whores?

366
00:26:14,306 --> 00:26:16,475
No, Papa, of course not.

367
00:26:17,743 --> 00:26:19,245
No, of course not.

368
00:26:19,311 --> 00:26:23,082
And yet the world is full of them.

369
00:26:23,149 --> 00:26:26,152
The West wants to turn
us all into whores.

370
00:26:26,218 --> 00:26:30,789
And they're devious in their ways.

371
00:26:30,856 --> 00:26:33,692
But this is just music, Papa.

372
00:26:33,759 --> 00:26:36,762
No, Flavia, it's propaganda.

373
00:26:36,828 --> 00:26:41,100
It's propaganda designed
to break down your defenses,

374
00:26:41,167 --> 00:26:43,135
little by little, and they
call it "art" or "music"

375
00:26:43,202 --> 00:26:45,971
or "film" or "literature,"
but make no mistake

376
00:26:46,038 --> 00:26:48,140
what it really is.

377
00:26:48,207 --> 00:26:50,008
What is it really?

378
00:26:50,076 --> 00:26:52,744
A tool for brainwashing.

379
00:26:52,811 --> 00:26:56,115
Every time you listen
to one of these songs,

380
00:26:56,182 --> 00:27:00,052
you are being indoctrinated.

381
00:27:11,497 --> 00:27:15,434
Well, you didn't have to
break the radio, Flavia.

382
00:27:15,501 --> 00:27:17,703
You know, there's a Bartók
concerto on tomorrow.

383
00:27:22,208 --> 00:27:25,611
I've seen terrible things, Flavia.

384
00:27:25,677 --> 00:27:28,680
It's why I'm so protective of you.

385
00:27:28,747 --> 00:27:31,650
I love you, Papa.

386
00:27:34,120 --> 00:27:36,322
I love you, too.

387
00:27:39,158 --> 00:27:42,394
Mmm.

388
00:27:42,461 --> 00:27:45,664
That's one more thing that
I'll miss about this country.

389
00:27:45,731 --> 00:27:50,769
Romanians make love like no one else.

390
00:27:50,836 --> 00:27:53,339
Listen, Jane...

391
00:27:54,706 --> 00:27:59,511
Father Streza happens to
have information that I need.

392
00:27:59,578 --> 00:28:01,947
Can you get him to talk to me?

393
00:28:02,013 --> 00:28:05,284
Gregor, don't ruin the moment.

394
00:28:05,351 --> 00:28:08,187
This man might have killed my partner.

395
00:28:08,254 --> 00:28:10,989
I understand if you have
religious convictions,

396
00:28:11,056 --> 00:28:12,391
but I need your help.

397
00:28:15,227 --> 00:28:16,562
Okay.

398
00:28:20,732 --> 00:28:23,502
Look, I'll be honest.

399
00:28:23,569 --> 00:28:27,706
Some of us in the West don't
actually believe in religion.

400
00:28:27,773 --> 00:28:31,042
We just pretend to in order to
convince a bunch of half-wits

401
00:28:31,109 --> 00:28:33,245
to vote for us.

402
00:28:35,881 --> 00:28:37,816
So, you'll help me then?

403
00:28:40,586 --> 00:28:42,721
I'm a capitalist, Gregor.

404
00:28:44,223 --> 00:28:46,458
If you want something,

405
00:28:46,525 --> 00:28:49,027
you're going to have to work for it.

406
00:28:49,094 --> 00:28:50,862
Really hard.

407
00:29:00,071 --> 00:29:02,441
This is madness.

408
00:29:02,508 --> 00:29:04,810
This is madness. You trust her?

409
00:29:04,876 --> 00:29:07,813
No, but she trusts me.

410
00:29:07,879 --> 00:29:10,182
You slept with her?

411
00:29:10,249 --> 00:29:11,583
Yeah.

412
00:29:15,787 --> 00:29:19,157
Well, detectives, looks
like you're in luck.

413
00:29:23,229 --> 00:29:26,398
Father Streza has agreed
to give you information

414
00:29:26,465 --> 00:29:30,068
in return for his immediate release.

415
00:29:30,135 --> 00:29:31,570
We're authorized to release him

416
00:29:31,637 --> 00:29:33,739
if he does, in fact, give us real info.

417
00:29:40,212 --> 00:29:41,813
Where are you getting your Bibles?

418
00:29:41,880 --> 00:29:45,217
They're the words of God,
and God is everywhere.

419
00:29:46,752 --> 00:29:48,119
I want answers.

420
00:29:48,186 --> 00:29:49,655
We all want answers.

421
00:29:49,721 --> 00:29:53,592
But until you accept Jesus
Christ as your savior,

422
00:29:53,659 --> 00:29:56,795
you're like a blind man
fumbling around in the dark.

423
00:29:56,862 --> 00:29:58,797
So shed some light for me, Father.

424
00:29:58,864 --> 00:30:02,468
Where were you and Marku
getting your Bibles?

425
00:30:02,534 --> 00:30:06,372
There's a man intent on helping
me spread the word of God.

426
00:30:07,439 --> 00:30:09,341
To you, he's a smuggler.

427
00:30:09,408 --> 00:30:11,777
To me, he's an angel,

428
00:30:11,843 --> 00:30:13,211
sent from heaven above.

429
00:30:13,279 --> 00:30:15,481
And does this angel have a name?

430
00:30:21,987 --> 00:30:25,324
He calls himself...

431
00:30:25,391 --> 00:30:27,559
James Brown.

432
00:30:33,399 --> 00:30:35,367
James Brown?

433
00:30:39,237 --> 00:30:40,572
Hmm.

434
00:30:44,209 --> 00:30:45,511
How do we find him?

435
00:30:46,412 --> 00:30:48,914
You don't.

436
00:30:48,980 --> 00:30:51,283
He has people who are devoted to him.

437
00:30:51,350 --> 00:30:54,286
People like Marku
Miklos who keep him safe.

438
00:30:54,353 --> 00:30:58,557
Who would rather slit their
own throats than give him up.

439
00:30:58,624 --> 00:31:02,361
He is the rock on which
I will build my church.

440
00:31:02,428 --> 00:31:05,564
He's coming for you, detectives.

441
00:31:05,631 --> 00:31:07,299
He's coming for both of you.

442
00:31:07,366 --> 00:31:08,434
How the fuck do we find him?

443
00:31:08,500 --> 00:31:09,635
I told you, you can't.

444
00:31:09,701 --> 00:31:11,370
He's a ghost. A holy ghost!

445
00:31:11,437 --> 00:31:13,839
Okay, that's all you get.

446
00:31:13,905 --> 00:31:16,074
Father Streza has been
more than forthcoming.

447
00:31:16,141 --> 00:31:17,843
This is bullshit.

448
00:31:17,909 --> 00:31:19,411
He gave us an alias.

449
00:31:19,478 --> 00:31:20,646
That's all he knows.

450
00:31:20,712 --> 00:31:22,013
And this interview is over.

451
00:31:22,080 --> 00:31:23,515
Like hell it is.

452
00:31:23,582 --> 00:31:26,284
Look, we had a deal,

453
00:31:26,352 --> 00:31:28,286
and Father Streza has
lived up to his end.

454
00:31:28,354 --> 00:31:29,321
Understood?

455
00:31:29,388 --> 00:31:30,956
So, let me make this clear.

456
00:31:31,022 --> 00:31:35,494
Release him now or I will use
the full power of my office

457
00:31:35,561 --> 00:31:37,262
to make sure that the two of you

458
00:31:37,329 --> 00:31:40,198
end up building canals in Constanta.

459
00:31:40,265 --> 00:31:42,368
Got it?

460
00:31:42,434 --> 00:31:43,602
Good.

461
00:31:45,404 --> 00:31:47,673
You're free to go, Father.

462
00:31:47,739 --> 00:31:50,409
These men won't bother you anymore.

463
00:31:50,476 --> 00:31:52,811
May God bless you, comrades.

464
00:31:59,184 --> 00:32:01,687
Hey, Father.

465
00:32:02,888 --> 00:32:05,090
You know why Jesus
wasn't born in the West?

466
00:32:06,558 --> 00:32:08,794
Enlighten me, Detective.

467
00:32:08,860 --> 00:32:12,498
Because they couldn't find
three wise men or a virgin.

468
00:32:13,499 --> 00:32:14,466
That's enough.

469
00:32:14,533 --> 00:32:18,570
I will pray for you, Detective.

470
00:32:32,951 --> 00:32:35,353
Nice work, Jane.

471
00:32:39,525 --> 00:32:41,527
Do you think he bought it?

472
00:34:16,655 --> 00:34:19,625
♪ My love ♪

473
00:34:19,691 --> 00:34:23,161
♪ Tell me what it's all about ♪

474
00:34:26,231 --> 00:34:28,534
♪ You've got something ♪

475
00:34:28,600 --> 00:34:32,538
♪ That I can't live without ♪

476
00:34:35,473 --> 00:34:38,376
♪ Happiness ♪

477
00:34:38,443 --> 00:34:42,848
♪ Is so hard to find ♪

478
00:34:44,683 --> 00:34:46,952
♪ Hey, baby ♪

479
00:34:47,018 --> 00:34:52,558
♪ Tell me what is on your mind ♪

480
00:34:52,624 --> 00:34:55,026
♪ 'Cause I can't wait ♪

481
00:34:55,093 --> 00:34:56,762
♪ Baby, I can't wait ♪

482
00:34:56,828 --> 00:35:00,065
♪ Till you call me on the telephone ♪

483
00:35:02,601 --> 00:35:04,402
♪ I can't wait ♪

484
00:35:04,469 --> 00:35:06,237
♪ Baby, I can't wait ♪

485
00:35:06,304 --> 00:35:09,775
♪ Till we're all alone ♪

486
00:35:09,841 --> 00:35:11,977
♪ I can't wait ♪

487
00:35:38,604 --> 00:35:40,538
♪ You know I love you... ♪

488
00:35:40,606 --> 00:35:42,674
Turn that fucking shit off!

489
00:35:42,741 --> 00:35:45,944
♪ Even when you don't try... ♪

490
00:35:46,011 --> 00:35:48,146
Put your hands on your head!

491
00:35:50,548 --> 00:35:53,218
Comrades, I told you.

492
00:35:53,284 --> 00:35:56,321
What did you tell us?

493
00:35:56,387 --> 00:35:59,925
I told you James Brown
was coming for you.

494
00:35:59,991 --> 00:36:02,360
He's coming for all of us!

495
00:36:07,599 --> 00:36:10,235
Go, go. I'm okay.

496
00:36:10,301 --> 00:36:15,040
♪ You'll never have to say good-bye. ♪

497
00:36:55,213 --> 00:36:57,015
Freeze!

498
00:37:01,552 --> 00:37:02,954
Take off the mask!

499
00:37:04,222 --> 00:37:06,224
Take off the mask!

500
00:37:06,291 --> 00:37:08,493
Now!

501
00:37:08,559 --> 00:37:12,263
I said take off...

502
00:37:22,173 --> 00:37:24,109
Joseph!

503
00:37:29,080 --> 00:37:30,415
Joseph.

504
00:37:30,481 --> 00:37:32,417
No!

505
00:37:54,039 --> 00:37:55,406
Daddy.

506
00:37:56,875 --> 00:37:58,810
How bad is it?

507
00:38:01,713 --> 00:38:03,749
He's in a coma.

508
00:38:03,815 --> 00:38:05,951
They don't know if he'll recover.

509
00:38:12,690 --> 00:38:15,560
Pardon me, Detective.

510
00:38:15,626 --> 00:38:19,931
There's a phone call for you.

511
00:38:38,483 --> 00:38:40,418
Show me.

512
00:38:48,493 --> 00:38:50,295
I'm warning you.

513
00:38:50,361 --> 00:38:52,463
It's not pretty.

514
00:38:52,530 --> 00:38:54,465
Just play it.

515
00:39:25,530 --> 00:39:27,465
Play it again.

516
00:39:40,000 --> 00:39:45,000
- Synced and corrected by chamallow -
- www.addic7ed.com -

