1
00:00:48,590 --> 00:00:50,884
Boys, boys! Stop the fight!

2
00:02:04,874 --> 00:02:06,292
Witch hazel.

3
00:02:07,752 --> 00:02:09,254
Split lip.

4
00:02:12,215 --> 00:02:14,551
Don't smile for a few days.
Next?

5
00:02:17,095 --> 00:02:19,347
Two beefsteaks.

6
00:02:20,015 --> 00:02:21,641
A kick in the pants.

7
00:02:21,850 --> 00:02:23,101
Next?

8
00:02:23,309 --> 00:02:25,603
A loose tooth
and swelled-up knuckles.

9
00:02:25,854 --> 00:02:29,274
It's a wonder you have any teeth left.
Witch hazel, please.

10
00:02:29,524 --> 00:02:32,986
I'm sorry, Milly.
But when that fellow took after Adam...

11
00:02:33,236 --> 00:02:34,821
I understand.

12
00:02:35,071 --> 00:02:37,115
You all did your best, I guess.

13
00:02:38,533 --> 00:02:40,201
Good night, boys.

14
00:02:43,371 --> 00:02:44,998
Good night, Milly.

15
00:02:51,212 --> 00:02:54,591
I feel awful strange-like, Milly.
Here.

16
00:02:56,134 --> 00:02:59,888
If it's what I think ails you,
witch hazel's not gonna help.

17
00:03:05,143 --> 00:03:07,520
Alice is sweet, isn't she?

18
00:03:09,314 --> 00:03:10,648
Good night.

19
00:03:24,287 --> 00:03:25,705
What's the matter with you?

20
00:03:25,955 --> 00:03:28,166
Somebody butt you in the breadbasket?

21
00:03:30,251 --> 00:03:32,462
I felt this way even before the fight.

22
00:03:32,712 --> 00:03:35,048
Ever since I seen Alice.

23
00:03:41,262 --> 00:03:43,723
Do you reckon I could be in love?

24
00:03:45,850 --> 00:03:49,145
Pa used to say love's like measles.
You only get it once.

25
00:03:49,396 --> 00:03:51,940
The older you are, the tougher it goes.

26
00:03:52,190 --> 00:03:55,151
Young fellow like you
ought to get it kind of light.

27
00:04:04,411 --> 00:04:05,954
I guess not.

28
00:04:10,125 --> 00:04:13,003
I don't know as I can answer
your question.

29
00:04:14,212 --> 00:04:18,258
But according to Milly, and she's
had a heap of book-learning...

30
00:04:18,508 --> 00:04:21,678
When you're in love

31
00:04:22,262 --> 00:04:25,181
When you're in love

32
00:04:26,725 --> 00:04:29,519
There is no way on earth...

33
00:04:29,769 --> 00:04:32,230
...to hide it

34
00:04:33,898 --> 00:04:36,901
When you're in love

35
00:04:37,736 --> 00:04:40,280
Really in love

36
00:04:42,240 --> 00:04:45,118
You simply let your heart...

37
00:04:45,368 --> 00:04:47,829
...decide it

38
00:04:49,873 --> 00:04:52,584
How can you tell?

39
00:04:53,877 --> 00:04:56,921
What's in its spell?

40
00:04:58,340 --> 00:04:59,966
How can you tell...

41
00:05:00,216 --> 00:05:03,345
...until you've tried it?

42
00:05:05,972 --> 00:05:08,767
Wait for that kiss...

43
00:05:09,017 --> 00:05:11,519
...you're certain of

44
00:05:14,856 --> 00:05:19,986
And let your heart decide

45
00:05:20,695 --> 00:05:23,531
When you're...

46
00:05:24,115 --> 00:05:28,036
...in love

47
00:05:36,002 --> 00:05:38,296
I guess I got the beginnings.

48
00:05:39,464 --> 00:05:43,301
But them townspeople will never let us
court them gals now.

49
00:05:44,094 --> 00:05:46,846
-Not after today.
-Don't let it fret you.

50
00:05:47,055 --> 00:05:49,808
If you don't get this one,
another will come along.

51
00:05:50,058 --> 00:05:52,477
One woman's pretty much like the next.

52
00:05:52,727 --> 00:05:55,188
Come on, let's go feed the stock.

53
00:06:51,661 --> 00:06:52,912
I'm...

54
00:06:54,497 --> 00:06:58,626
...a lonesome polecat

55
00:07:00,337 --> 00:07:02,464
Lonesome...

56
00:07:02,714 --> 00:07:07,344
...sad and blue

57
00:07:08,678 --> 00:07:13,558
'Cause I ain't got no

58
00:07:14,684 --> 00:07:17,354
Feminine polecat

59
00:07:19,105 --> 00:07:22,442
Vowin' to be true

60
00:07:33,995 --> 00:07:36,748
Can't make no vows

61
00:07:37,749 --> 00:07:41,586
To a herd of cows

62
00:07:48,760 --> 00:07:50,553
I'm...

63
00:07:51,346 --> 00:07:55,475
...a mean old hound dog

64
00:07:57,352 --> 00:08:02,482
Bayin' at the moon

65
00:08:05,610 --> 00:08:09,531
'Cause I ain't got no

66
00:08:11,574 --> 00:08:14,327
Lady friend hound dog

67
00:08:16,121 --> 00:08:20,041
Here to hear my tune

68
00:08:30,927 --> 00:08:33,680
A man can't sleep

69
00:08:34,764 --> 00:08:39,477
When he sleeps with sheep

70
00:08:45,900 --> 00:08:47,527
I'm...

71
00:08:48,570 --> 00:08:52,615
...a little old hoot owl

72
00:08:54,701 --> 00:08:59,831
Hootin' in the trees

73
00:09:02,917 --> 00:09:07,047
'Cause I ain't got no

74
00:09:09,049 --> 00:09:11,593
Little gal owl fowl

75
00:09:13,511 --> 00:09:17,349
Here to shoot the breeze

76
00:09:28,443 --> 00:09:31,863
Can't shoot no breeze

77
00:09:32,364 --> 00:09:36,117
With a bunch of trees

78
00:10:18,785 --> 00:10:20,328
It is beautiful, isn't it?

79
00:10:20,578 --> 00:10:22,122
Not to me.

80
00:10:22,414 --> 00:10:24,457
I've seen too much of it.

81
00:10:25,083 --> 00:10:26,543
I'm getting out.

82
00:10:27,961 --> 00:10:31,297
I'm not spending another
winter here, snowed in for months.

83
00:10:31,589 --> 00:10:33,800
What would we do without you?

84
00:10:34,259 --> 00:10:35,427
We'd miss you so.

85
00:10:35,677 --> 00:10:39,014
There's plenty here
to take care of the farm without me.

86
00:10:51,735 --> 00:10:53,111
Adam!

87
00:10:55,071 --> 00:10:57,198
-Benjamin says he's leaving.
-What?

88
00:10:57,407 --> 00:10:59,451
He says it's the winter,
but it's not.

89
00:10:59,743 --> 00:11:02,412
They're all grieving for their girls.

90
00:11:02,662 --> 00:11:05,373
Why should they grieve?
They hardly saw them once.

91
00:11:06,833 --> 00:11:09,586
Once is all it takes
if it's the right one.

92
00:11:09,836 --> 00:11:13,173
I had such dreams about all of them
living around.

93
00:11:13,423 --> 00:11:18,303
Dozens of children, visiting back
and forth at Christmas and birthdays.

94
00:11:18,595 --> 00:11:20,805
If Benjamin goes,
then another will go.

95
00:11:21,097 --> 00:11:22,349
And another.

96
00:11:24,601 --> 00:11:26,394
I'll talk to him.

97
00:11:43,661 --> 00:11:46,414
If you could just get a look at
yourselves...

98
00:11:46,623 --> 00:11:49,959
You look like
a bunch of lovesick bull calves.

99
00:11:54,214 --> 00:11:57,550
If you're sweet on them,
why don't you do something about it?

100
00:11:57,842 --> 00:11:59,302
Why don't you go marry them?

101
00:11:59,594 --> 00:12:01,888
Sure, "Go marry them," as easy as that!

102
00:12:02,430 --> 00:12:04,808
They wouldn't marry us
in a thousand years.

103
00:12:05,058 --> 00:12:08,561
Do as the Romans did with the
"Sobbing Women" or "Sabine Women"...

104
00:12:08,895 --> 00:12:12,732
...or whatever they called them.
They were in the same fix you're in.

105
00:12:13,024 --> 00:12:17,070
They was opening up new territory,
and women were scarce, like here.

106
00:12:17,570 --> 00:12:19,948
There were these sobbing women
in town.

107
00:12:20,198 --> 00:12:21,658
So what did the Romans do?

108
00:12:21,866 --> 00:12:24,953
They went down there,
and they carried them off.

109
00:12:25,245 --> 00:12:29,624
If you can't do as good as a bunch of
Romans, you're no brothers of mine.

110
00:12:29,916 --> 00:12:33,169
Course, this being Oregon
and God-fearing territory...

111
00:12:33,420 --> 00:12:35,463
...you'd have to capture a parson.

112
00:12:35,714 --> 00:12:39,134
Romans. They the ones I heard
about settled north of here?

113
00:12:39,426 --> 00:12:40,885
No, this was in olden days.

114
00:12:41,136 --> 00:12:42,929
I read about it in Milly's book.

115
00:12:43,138 --> 00:12:45,598
-A book!
-Why, this is history.

116
00:12:45,849 --> 00:12:47,100
This really happened.

117
00:12:47,392 --> 00:12:52,105
Tell you about them sobbin' women
Who lived in the Roman days

118
00:12:52,313 --> 00:12:56,943
It seems that they all went swimmin'
While their men was off to graze

119
00:12:57,193 --> 00:13:01,740
Well a Roman troop was riding by
And saw them in their "me-oh-mys"

120
00:13:02,032 --> 00:13:04,492
So they took them all
Back home to dry

121
00:13:04,743 --> 00:13:07,203
Least that's what Plutarch says

122
00:13:07,495 --> 00:13:09,873
Them women was
Sobbin', sobbin', sobbin'

123
00:13:10,123 --> 00:13:11,666
Fit to be tied

124
00:13:12,250 --> 00:13:16,796
Every muscle was throbbin', throbbin'
From that riotous ride

125
00:13:17,088 --> 00:13:19,966
Seems they cried and kissed
And kissed and cried

126
00:13:20,216 --> 00:13:22,260
All over that Roman countryside

127
00:13:22,510 --> 00:13:24,971
So don't forget that

128
00:13:25,221 --> 00:13:27,265
When you're taking a bride

129
00:13:27,515 --> 00:13:29,726
Sobbin', fit to be tied

130
00:13:29,976 --> 00:13:32,270
From that riotous ride

131
00:13:32,520 --> 00:13:35,357
-So then what happened?
-Gather round, I'll tell you.

132
00:13:35,607 --> 00:13:37,817
They never did return their plunder

133
00:13:38,068 --> 00:13:40,195
The victor gets all the loot

134
00:13:40,445 --> 00:13:42,405
They carried them home by thunder

135
00:13:42,697 --> 00:13:45,075
To rotundas small but cute

136
00:13:45,325 --> 00:13:47,452
And you've never seen,
So they tell me

137
00:13:47,744 --> 00:13:49,954
Such downright domesticity

138
00:13:50,246 --> 00:13:52,207
With a Roman baby on each knee

139
00:13:52,499 --> 00:13:55,251
Named Claudius and Brute

140
00:13:55,585 --> 00:13:57,879
Them women was
Sobbin', sobbin', sobbin'

141
00:13:58,171 --> 00:13:59,964
Passin' them nights

142
00:14:00,256 --> 00:14:03,843
While the Romans was out
Hobnobbin', starting up fights

143
00:14:04,052 --> 00:14:04,886
Is that so?

144
00:14:05,136 --> 00:14:09,265
They kept occupied by sewing lots
Of little old togas...

145
00:14:09,474 --> 00:14:12,894
...for them tots
And sayin', "Someday...

146
00:14:13,186 --> 00:14:15,397
...womenfolks will have rights"

147
00:14:15,689 --> 00:14:17,649
-Passin' all their nights
-Just sewing!

148
00:14:17,941 --> 00:14:21,027
-While the Romans had fights
-Listen to this.

149
00:14:21,319 --> 00:14:23,697
"When the menfolk
Went to fetch them

150
00:14:23,988 --> 00:14:25,949
The women would not be fetched"

151
00:14:26,199 --> 00:14:29,953
Seems when the Romans catch 'em
Their lady friends stay catched

152
00:14:30,829 --> 00:14:33,123
Now, let this be
Because it's true

153
00:14:33,373 --> 00:14:35,333
A lesson to the likes of you

154
00:14:35,625 --> 00:14:37,669
Rough 'em up
Like them there Romans do

155
00:14:37,961 --> 00:14:40,714
Or else they'll think you're tetched

156
00:14:40,964 --> 00:14:43,341
Them women was
Sobbin', sobbin', sobbin'

157
00:14:43,633 --> 00:14:45,427
-Buckets of tears
-Mighty sad!

158
00:14:45,719 --> 00:14:49,305
On account of old dobbin, dobbin
Really rattled their ears

159
00:14:49,556 --> 00:14:53,393
And that ain't all!
Oh, they acted angry and annoyed

160
00:14:53,643 --> 00:14:55,603
But secretly they was overjoyed

161
00:14:55,895 --> 00:14:59,816
You might recall that
When corrallin' your steers

162
00:15:01,192 --> 00:15:02,444
Them poor little dears

163
00:15:02,694 --> 00:15:04,821
Why are you sitting there?
Go get them!

164
00:15:05,947 --> 00:15:08,324
Them women was
Sobbin', sobbin', sobbin'

165
00:15:08,575 --> 00:15:10,535
-Weepin' a ton
-Them sobbin' women

166
00:15:10,827 --> 00:15:13,580
Just remember what
Robin, Robin, Robin Hood...

167
00:15:13,830 --> 00:15:15,540
...would have done

168
00:15:15,790 --> 00:15:18,710
We'll be just like
Them there merry men

169
00:15:18,918 --> 00:15:20,879
And make them all merry once again

170
00:15:21,129 --> 00:15:24,632
And though they'll be
A-sobbin' for a while

171
00:15:25,633 --> 00:15:30,764
We're gonna make them
Sobbin' women smile

172
00:15:44,819 --> 00:15:46,071
Ho there.

173
00:15:47,530 --> 00:15:49,574
Go get them.
Be quiet about it.

174
00:15:50,075 --> 00:15:51,618
I'll wait for you here!

175
00:15:59,167 --> 00:16:00,502
Dan, Eph!

176
00:16:03,296 --> 00:16:05,173
Your two's in here.

177
00:16:07,592 --> 00:16:09,052
They're coming out.

178
00:16:09,552 --> 00:16:11,429
-Good night, Liza.
-Good night.

179
00:16:17,602 --> 00:16:19,145
You got mine.

180
00:16:23,233 --> 00:16:25,110
How about my good-night kiss, Sarah?

181
00:16:25,402 --> 00:16:27,112
Carl, you're so persistent.

182
00:16:27,320 --> 00:16:28,947
Be a sport! Just one kiss.

183
00:16:29,197 --> 00:16:31,741
Well, maybe just a little one.

184
00:16:31,950 --> 00:16:33,326
Close your eyes.

185
00:16:38,456 --> 00:16:39,916
Why, Carl!

186
00:16:40,291 --> 00:16:41,459
Frank!

187
00:16:49,050 --> 00:16:50,427
Attaboy! Get them in.

188
00:16:52,429 --> 00:16:53,805
Come on, Frank!

189
00:16:55,598 --> 00:16:56,433
Where's Gideon?

190
00:17:02,022 --> 00:17:05,692
-I hear the cat outside.
-I'll go, Mrs. Elcott.

191
00:17:06,401 --> 00:17:08,528
Poor Pansy, out on a night like this.

192
00:17:24,919 --> 00:17:26,713
Hurry it up, will you?

193
00:17:33,678 --> 00:17:37,599
-Sounds like Pansy has the croup.
-I'll look out in back.

194
00:17:41,186 --> 00:17:42,812
It's all right, she's here.

195
00:17:57,202 --> 00:17:58,286
Dorcas.

196
00:17:58,870 --> 00:18:00,205
Dorcas!

197
00:18:00,830 --> 00:18:02,707
Pa! Pa, come quick!

198
00:18:04,626 --> 00:18:05,627
Attaboy, Gideon!

199
00:18:21,142 --> 00:18:23,269
-What's keeping Benjamin?
-I don't know.

200
00:18:25,605 --> 00:18:27,399
-Keep them quiet.
-Be quiet.

201
00:18:27,649 --> 00:18:29,275
Let's get out of here now.

202
00:18:29,484 --> 00:18:30,735
Let's go!

203
00:18:43,915 --> 00:18:45,792
-Sarah Kines is gone!
-So is Alice.

204
00:18:46,084 --> 00:18:48,294
-Ruth Jebson too!
-Where's Dorcas?

205
00:18:48,586 --> 00:18:51,464
It was them Pontipees.
All seven of them jumped me!

206
00:18:51,715 --> 00:18:53,842
Come on! We'll get them.

207
00:19:45,101 --> 00:19:47,479
Make them stop yelling.
We're coming to the pass.

208
00:19:47,771 --> 00:19:50,148
Quiet, or we'll have
an avalanche down on us.

209
00:19:55,445 --> 00:19:57,489
Here we go. Hold your breath.

210
00:20:13,171 --> 00:20:14,631
Keep her quiet!

211
00:20:20,553 --> 00:20:22,263
Hurry up,
I hear them coming!

212
00:20:52,752 --> 00:20:54,254
All right, let them go!

213
00:21:07,684 --> 00:21:08,852
There she goes!

214
00:22:31,893 --> 00:22:35,647
On account of old dobbin, dobbin
Really rattled their ears

215
00:22:38,400 --> 00:22:40,193
All right, boys, line them up.

216
00:22:40,443 --> 00:22:41,945
Oh, Milly!

217
00:22:44,114 --> 00:22:45,657
What have you done?

218
00:22:45,865 --> 00:22:48,159
Help us, Milly.
We've been kidnapped!

219
00:22:48,368 --> 00:22:51,871
-Make them take us back.
-You take these girls back at once!

220
00:22:52,122 --> 00:22:53,540
-We can't.
-The avalanche!

221
00:22:53,790 --> 00:22:56,876
They'll just have to wait
till spring, that's all.

222
00:22:57,877 --> 00:22:59,045
It was your idea?

223
00:22:59,337 --> 00:23:03,717
It was more yours. It came out of
that book about them Sabine women.

224
00:23:04,009 --> 00:23:08,138
What kind of men are you?
Are you animals that you'd do this?

225
00:23:08,430 --> 00:23:10,807
Swooping down,
carrying off poor innocents!

226
00:23:11,099 --> 00:23:13,143
They're just young boys in love.

227
00:23:13,435 --> 00:23:16,688
You said you wanted them
married with wives of their own.

228
00:23:16,980 --> 00:23:18,606
I want my mother!

229
00:23:20,233 --> 00:23:21,693
Please don't cry, Alice.

230
00:23:21,901 --> 00:23:25,238
I'm sorry, but it was the only way
I could get to marry you.

231
00:23:27,407 --> 00:23:30,243
-What?
-We forgot to get the parson!

232
00:23:30,702 --> 00:23:33,955
Don't worry. Go on in the kitchen.
Nothing's gonna happen.

233
00:23:34,247 --> 00:23:36,041
Cheer up.
You've got them, right?

234
00:23:36,291 --> 00:23:38,335
Everything's gonna
turn out all right.

235
00:23:38,585 --> 00:23:40,462
This house is for the girls.

236
00:23:40,670 --> 00:23:44,007
You're not gonna set foot in it
as long as they're here.

237
00:23:44,257 --> 00:23:47,594
You'll eat and sleep in the barn
with the other livestock.

238
00:23:47,844 --> 00:23:49,262
Now get out of here.

239
00:23:49,471 --> 00:23:51,097
I'm ashamed of you.

240
00:23:51,389 --> 00:23:53,350
Go ahead, boys. Go on.

241
00:23:54,809 --> 00:23:56,519
-Now, Milly...
-You too.

242
00:23:56,770 --> 00:23:59,856
You're taking this too hard.
Everything will work out fine.

243
00:24:00,148 --> 00:24:02,525
Me and the boys will get
a parson here some way.

244
00:24:02,859 --> 00:24:05,862
Do you think those girls
would marry them now?

245
00:24:06,738 --> 00:24:09,032
You think because
you got a wife so easily...

246
00:24:09,240 --> 00:24:13,161
...because I didn't make you court me,
that that's all there is to it?

247
00:24:13,620 --> 00:24:17,999
I said yes because I fell in love
with you the first time I saw you.

248
00:24:18,833 --> 00:24:21,127
And I thought it was
the same with you too.

249
00:24:21,419 --> 00:24:23,380
You think a wife is
just to cook and clean.

250
00:24:23,880 --> 00:24:27,133
You got no understanding,
you got no feelings!

251
00:24:27,384 --> 00:24:30,095
How could you do a thing like this?

252
00:24:30,512 --> 00:24:32,889
When I think of these girls,
sick with fright...

253
00:24:33,181 --> 00:24:35,725
...and their families
crazed with worry, and...

254
00:24:36,142 --> 00:24:38,269
I can't abide to look at you!

255
00:24:52,575 --> 00:24:56,788
This isn't the way we'd planned
on spending the night. In a barn!

256
00:24:59,791 --> 00:25:02,168
-What are you doing?
-I'm getting out of here.

257
00:25:02,460 --> 00:25:04,337
Going to the cabin
for the winter.

258
00:25:04,587 --> 00:25:07,590
You can't do it.
You can't stay up there alone.

259
00:25:07,841 --> 00:25:09,718
There's supplies for 6 months.

260
00:25:09,968 --> 00:25:12,429
Don't go.
It's us she's mad at.

261
00:25:13,555 --> 00:25:17,392
No, it's more than that.
It's something goes a lot deeper.

262
00:25:17,684 --> 00:25:21,604
Suppose you get sick or break a leg
or something, up there all alone?

263
00:25:21,813 --> 00:25:23,940
The wolves are
mighty bad this year.

264
00:25:24,190 --> 00:25:27,610
The wolves will be good company
after what I've been through.

265
00:25:36,411 --> 00:25:40,332
There's nothing like a hot cup of tea
when you're all tuckered out.

266
00:25:41,791 --> 00:25:44,419
Milly, it's me, Gideon.
I gotta speak to you.

267
00:25:48,673 --> 00:25:50,383
-What is it?
-It's about Adam.

268
00:25:50,633 --> 00:25:54,137
He's going away up into the mountains
to the trapping cabin.

269
00:25:54,763 --> 00:25:57,474
-Can you hear me?
-I hear.

270
00:25:57,849 --> 00:25:59,893
Just speak to him.
Ask him not to go.

271
00:26:00,810 --> 00:26:02,437
Please, Milly.

272
00:26:04,314 --> 00:26:06,524
He can't treat people this way.

273
00:26:29,130 --> 00:26:31,007
Now, let's start here.

274
00:27:11,089 --> 00:27:13,550
What's the matter,
you afraid of a snowball?

275
00:27:20,056 --> 00:27:22,350
Snowballs with rocks in them!

276
00:27:22,559 --> 00:27:24,686
"Them poor little dears...

277
00:27:24,894 --> 00:27:27,564
...sobbing buckets of tears."

278
00:27:30,859 --> 00:27:34,696
"When the sign was given, drawing
their swords and with a shout...

279
00:27:34,904 --> 00:27:37,574
...they ravished away
the daughters of the Sabine."

280
00:27:38,199 --> 00:27:39,659
Who is it?

281
00:27:41,161 --> 00:27:43,038
It's me, Caleb.

282
00:27:44,289 --> 00:27:45,123
What do you want?

283
00:27:46,166 --> 00:27:48,376
It's a mighty cold night.

284
00:27:49,210 --> 00:27:50,920
I need an extra blanket.

285
00:27:53,006 --> 00:27:53,840
Go and get one.

286
00:28:03,516 --> 00:28:06,936
"It continues the custom today
for the bride, not of herself...

287
00:28:07,187 --> 00:28:09,939
...to pass her husband's threshold,
but to be lifted over...

288
00:28:10,190 --> 00:28:13,860
...in memory that the Sabine women
were carried in by violence."

289
00:28:15,153 --> 00:28:16,529
Who is it?

290
00:28:17,072 --> 00:28:18,239
Benjamin.

291
00:28:19,783 --> 00:28:21,326
What do you want?

292
00:28:21,660 --> 00:28:24,287
My leg is feeling poorly.

293
00:28:24,537 --> 00:28:25,705
I need some liniment.

294
00:28:28,124 --> 00:28:30,001
Go ahead, but be quick about it.

295
00:28:43,765 --> 00:28:45,141
"Some say too...

296
00:28:45,392 --> 00:28:49,688
...the custom of parting the bride's
hair with a spear was in token of..."

297
00:28:54,776 --> 00:28:57,654
-I got me a stiff neck.
-That's enough of this!

298
00:28:57,862 --> 00:28:59,239
-We wanted...
-It was like this...

299
00:28:59,489 --> 00:29:01,282
Go to the barn
and go to sleep...

300
00:29:01,574 --> 00:29:03,451
...or I'll bolt the door on you.

301
00:29:14,421 --> 00:29:16,881
"It was in token of their marriages.

302
00:29:17,132 --> 00:29:21,052
It began at first by wars
and acts of hostility...

303
00:29:21,302 --> 00:29:22,679
...and then..."

304
00:29:40,905 --> 00:29:43,450
Doesn't it do anything
but snow up here?

305
00:29:43,700 --> 00:29:46,453
We've had a blizzard every day
for two months.

306
00:29:46,661 --> 00:29:49,581
I'm going crazy,
shut up in this house!

307
00:29:49,831 --> 00:29:52,834
Alice, why don't you
read out loud to us?

308
00:29:53,084 --> 00:29:55,045
We've heard that book three times.

309
00:29:55,295 --> 00:29:58,882
-Go on, read about the Sabine women.
-Please, Alice.

310
00:29:59,132 --> 00:30:00,550
I can't stand it again.

311
00:30:00,800 --> 00:30:02,927
Get a dress from Milly,
and I'll fit it.

312
00:30:03,219 --> 00:30:05,430
Why, when there's no one to see it?

313
00:30:05,722 --> 00:30:09,768
No one? Well, I like that!
We're no one.

314
00:30:10,060 --> 00:30:13,396
-Where's Milly?
-Out in the barn hunting eggs.

315
00:30:13,646 --> 00:30:18,193
I wish I could hunt eggs.
I love to hunt eggs.

316
00:30:18,526 --> 00:30:21,863
Which of the boys slept
in this bed, do you suppose?

317
00:30:22,906 --> 00:30:24,157
Dorcas Galen!

318
00:30:25,408 --> 00:30:27,952
What's the matter?
Didn't you ever think of that?

319
00:30:28,161 --> 00:30:30,872
That you're sleeping
in one of their beds?

320
00:30:31,289 --> 00:30:34,209
I certainly never thought
any such thing.

321
00:30:34,501 --> 00:30:36,127
Liza, Alice...

322
00:30:36,378 --> 00:30:39,005
...come away from
that window this minute!

323
00:30:39,214 --> 00:30:43,218
I think it's disgusting standing there
where they can see you.

324
00:30:43,468 --> 00:30:47,222
You peek out often enough
when you think no one's looking.

325
00:30:47,514 --> 00:30:49,224
-I never!
-I saw you!

326
00:30:49,724 --> 00:30:50,558
So did I!

327
00:30:50,767 --> 00:30:52,727
You take that back, you hear?

328
00:30:53,061 --> 00:30:54,062
Stop it!

329
00:30:54,312 --> 00:30:56,690
Don't you dare say that about Martha!

330
00:30:56,940 --> 00:31:00,694
And what were you doing last night
out at the woodpile?

331
00:31:09,953 --> 00:31:11,997
Girls! Girls!

332
00:31:12,455 --> 00:31:14,249
Stop it this minute!

333
00:31:17,544 --> 00:31:21,589
Please don't squabble.
Not now, when I need you so much.

334
00:31:21,798 --> 00:31:25,301
You see, I'm counting on
all of you to help me...

335
00:31:25,802 --> 00:31:27,762
...because I'm going to have a baby.

336
00:31:29,347 --> 00:31:30,974
Oh, Milly, sit down!

337
00:31:31,182 --> 00:31:34,352
-Are you comfortable?
-You won't lift a finger from now on.

338
00:31:34,644 --> 00:31:38,898
We'll take over everything. We'll do
your chores, cleaning, cooking...

339
00:31:39,149 --> 00:31:40,942
No, you can't.
That'd be silly.

340
00:31:41,234 --> 00:31:43,778
-Where are you going?
-I have things to do.

341
00:31:44,029 --> 00:31:45,739
When's the baby coming?

342
00:31:45,989 --> 00:31:47,240
You have to be patient.

343
00:31:47,490 --> 00:31:48,867
But when?

344
00:31:49,659 --> 00:31:51,369
In the spring.

345
00:31:55,707 --> 00:31:58,501
-I wish it was me.
-Dorcas!

346
00:31:58,710 --> 00:32:00,920
I've always wanted
to be a June bride...

347
00:32:01,546 --> 00:32:05,592
...and have a baby right off.
In the spring, maybe.

348
00:32:07,927 --> 00:32:12,015
Oh, they say when you marry in June

349
00:32:12,223 --> 00:32:16,353
You're a bride all your life

350
00:32:16,603 --> 00:32:20,523
And the bridegroom
Who marries in June

351
00:32:20,815 --> 00:32:24,569
Gets a sweetheart for a wife

352
00:32:25,028 --> 00:32:28,782
Winter weddings can be gay

353
00:32:29,282 --> 00:32:33,411
Like a Christmas holiday

354
00:32:33,662 --> 00:32:37,499
But the June bride hears the song

355
00:32:38,041 --> 00:32:42,045
Of a spring that
Lasts all summer long

356
00:32:42,337 --> 00:32:46,091
By the light of the silvery moon

357
00:32:46,841 --> 00:32:48,551
Home you ride

358
00:32:49,010 --> 00:32:50,887
Side by side

359
00:32:51,179 --> 00:32:54,933
With the echo of Mendelssohn's tune

360
00:32:55,433 --> 00:32:57,310
In your hearts

361
00:32:57,560 --> 00:32:59,187
As you ride

362
00:32:59,813 --> 00:33:03,733
For they say when you marry in June

363
00:33:04,025 --> 00:33:08,154
You will always be a bride

364
00:33:12,075 --> 00:33:13,785
The day a maiden marries

365
00:33:13,994 --> 00:33:17,664
Is a day she carries through the years

366
00:33:17,956 --> 00:33:22,085
The church is full of flowers
Bridal showers are passe

367
00:33:23,503 --> 00:33:25,964
The groom's waiting at the altar

368
00:33:26,214 --> 00:33:28,258
Here comes the bride

369
00:33:28,508 --> 00:33:32,178
They're each promising
To love and obey

370
00:33:33,972 --> 00:33:35,765
Best man is celebrating

371
00:33:36,016 --> 00:33:39,686
Every bridesmaid's waiting
Just to see

372
00:33:39,978 --> 00:33:43,815
Which one of them
Will catch the wedding bouquet

373
00:34:33,490 --> 00:34:35,533
For they say

374
00:34:35,784 --> 00:34:38,703
When you marry in June

375
00:34:39,663 --> 00:34:42,666
You will always

376
00:34:46,002 --> 00:34:48,129
Always

377
00:34:52,133 --> 00:34:54,511
Be a bride

378
00:35:09,109 --> 00:35:12,946
In November, the snow starts to fly

379
00:35:13,321 --> 00:35:14,781
Piling up

380
00:35:15,407 --> 00:35:17,117
Ankle-high

381
00:35:17,575 --> 00:35:21,079
Come December, it's up to your knee

382
00:35:21,579 --> 00:35:25,000
Still, the bride's a bride-to-be

383
00:35:25,750 --> 00:35:29,337
January, higher still

384
00:35:29,963 --> 00:35:33,133
To the parlor windowsill

385
00:35:34,092 --> 00:35:37,512
February finds a drift

386
00:35:38,221 --> 00:35:42,225
And a storm that seems never to lift

387
00:35:42,809 --> 00:35:46,563
March comes in like a lion...
What else?

388
00:35:46,896 --> 00:35:50,817
Still the snow never melts

389
00:35:51,192 --> 00:35:53,320
April showers will come

390
00:35:53,570 --> 00:35:55,196
So they say

391
00:35:55,447 --> 00:35:59,117
But they don't
And it's May

392
00:35:59,659 --> 00:36:03,288
You're about to
Forget the whole thing

393
00:36:11,504 --> 00:36:13,631
All at once

394
00:36:13,965 --> 00:36:17,761
One day it's spring

395
00:36:52,128 --> 00:36:56,091
Oh, the barnyard is busy
In a regular tizzy

396
00:36:56,299 --> 00:37:00,804
And the obvious reason
Is because of the season

397
00:37:01,096 --> 00:37:04,808
Ma Nature's lyrical
With her yearly miracle

398
00:37:05,016 --> 00:37:08,269
Spring, spring, spring

399
00:37:08,812 --> 00:37:12,732
All the hen-folk are hatchin'
While their menfolk are scratchin'

400
00:37:12,982 --> 00:37:17,445
To ensure the survival
Of each brand-new arrival

401
00:37:17,696 --> 00:37:21,449
Each nest is twitterin'
They're all babysitterin'

402
00:37:21,741 --> 00:37:25,078
Spring, spring, spring

403
00:37:25,537 --> 00:37:29,416
It's a beehive
Of budding son and daughter life

404
00:37:29,666 --> 00:37:32,961
Every family has plans in view

405
00:37:33,837 --> 00:37:37,674
Even down in the brook
The underwater life

406
00:37:38,008 --> 00:37:41,261
Is forever blowing bubbles too

407
00:37:42,137 --> 00:37:46,266
Every field wears a bonnet
With some spring daisies on it

408
00:37:46,474 --> 00:37:50,854
Even birds of a feather
Show their clothes off together

409
00:37:51,062 --> 00:37:54,983
Sun's getting shinery
To spotlight the finery

410
00:37:55,233 --> 00:37:58,028
Spring, spring, spring

411
00:37:58,820 --> 00:38:02,490
From his aerie
The eagle with his eagle eye

412
00:38:02,991 --> 00:38:05,994
Gazes down across his eagle beak

413
00:38:07,162 --> 00:38:10,832
And affixin' his lady
With a legal eye

414
00:38:11,207 --> 00:38:14,627
Screams, "Suppose
we set the date this week?"

415
00:38:19,591 --> 00:38:23,720
Yes, sirree, spring discloses
That it's all one supposes

416
00:38:23,970 --> 00:38:28,224
It's a real bed of roses
Waggin' tails, rubbing noses

417
00:38:28,475 --> 00:38:32,228
Each day is Mother's Day
The next is some other's day

418
00:38:32,520 --> 00:38:35,523
When love is king

419
00:38:36,399 --> 00:38:38,109
Frank, Dan, Ben, Caleb!

420
00:38:38,485 --> 00:38:40,945
Everybody!
Milly's having her baby!

421
00:39:08,515 --> 00:39:10,475
Liza, Martha, Ruth, hurry!

422
00:39:10,975 --> 00:39:11,976
Hurry!

423
00:39:39,963 --> 00:39:41,339
I'm an uncle.

424
00:40:45,862 --> 00:40:48,323
Couldn't stand the barn
any longer, huh?

425
00:40:48,823 --> 00:40:52,243
I've come to get you.
To take you back with me.

426
00:40:56,039 --> 00:40:57,916
Well, Milly has a baby.

427
00:40:58,166 --> 00:41:00,377
It come day before yesterday.

428
00:41:02,545 --> 00:41:04,172
I don't believe it.

429
00:41:04,673 --> 00:41:07,676
-It's as true as I'm standing here.
-I don't believe it!

430
00:41:08,009 --> 00:41:10,387
Just one of her tricks
to get me back.

431
00:41:10,595 --> 00:41:13,139
Tricks? You lived with Milly
as man and wife...

432
00:41:13,431 --> 00:41:15,308
...and you don't know
nothing about her.

433
00:41:15,600 --> 00:41:20,230
She's proud and spunky! She'd never
bring herself to ask you back.

434
00:41:20,438 --> 00:41:22,482
She'd be wasting her breath.

435
00:41:23,817 --> 00:41:24,985
I don't figure to go.

436
00:41:25,276 --> 00:41:29,739
Not even to see the baby?
Your own kin? Your own little girl?

437
00:41:29,990 --> 00:41:31,366
A girl?

438
00:41:31,992 --> 00:41:34,619
I might have known she'd have a girl.

439
00:41:35,453 --> 00:41:37,497
I'll be back when the pass is open.

440
00:41:37,706 --> 00:41:40,458
I'll stay away
till the shooting's over.

441
00:41:43,211 --> 00:41:45,005
You're my eldest brother.

442
00:41:45,922 --> 00:41:49,009
And I've always looked up to you,
tried to ape you...

443
00:41:49,718 --> 00:41:51,928
...but today I'm ashamed for you.

444
00:41:53,263 --> 00:41:56,141
I know you can lick me,
lick the tar out of me...

445
00:41:56,391 --> 00:41:59,894
...but I wouldn't be a man
unless I showed you how I felt.

446
00:42:00,854 --> 00:42:01,938
Why, you...

447
00:42:09,696 --> 00:42:10,780
Now get!

448
00:42:25,253 --> 00:42:27,797
The pass is open!
The pass is open!

449
00:42:28,006 --> 00:42:31,343
All able-bodied men gather
at the meeting house!

450
00:42:33,928 --> 00:42:38,558
Bring your rifles, your sides,
your guns, axes, any kind of weapon!

451
00:42:39,142 --> 00:42:41,519
The pass is open!
The pass is open!

452
00:42:42,395 --> 00:42:43,229
Ready, men?

453
00:42:44,230 --> 00:42:45,482
Let's go!

454
00:43:09,923 --> 00:43:11,800
What are you staring at?

455
00:43:12,300 --> 00:43:15,011
I said I'd be back
when the pass was open.

456
00:43:18,139 --> 00:43:20,975
Go upstairs, girls.
Go on. Quickly!

457
00:43:23,937 --> 00:43:25,563
Let's get the guns.

458
00:43:27,691 --> 00:43:29,734
Tell them to hitch up the wagon.

459
00:43:30,568 --> 00:43:33,029
Put plenty of blankets
and straw in it. Go.

460
00:44:07,147 --> 00:44:08,690
What do you call her?

461
00:44:10,150 --> 00:44:14,779
I was thinking of some name
like Hannah or Hagar or Hephzibah...

462
00:44:15,280 --> 00:44:17,907
...picking up
where your mother left off.

463
00:44:19,826 --> 00:44:21,202
Hannah.

464
00:44:25,874 --> 00:44:28,543
I got to thinking
up there in the mountains...

465
00:44:29,127 --> 00:44:31,004
...thinking about the baby...

466
00:44:34,049 --> 00:44:37,886
...about how I'd feel if somebody
sneaked in and carried her off.

467
00:44:39,137 --> 00:44:41,097
I'd string him up
the nearest tree...

468
00:44:41,306 --> 00:44:44,142
...shoot him down like I would
a thieving fox.

469
00:44:47,395 --> 00:44:50,023
I thought about you too
while I was up there.

470
00:44:52,400 --> 00:44:55,070
It seemed like I couldn't
get you off my mind.

471
00:44:58,657 --> 00:45:00,617
When you're in love...

472
00:45:01,701 --> 00:45:03,578
When you're really in love...

473
00:45:18,635 --> 00:45:20,428
The wagon's ready.

474
00:45:20,970 --> 00:45:22,347
All right.

475
00:45:22,972 --> 00:45:25,016
Get the girls.
I'll take them back...

476
00:45:25,225 --> 00:45:26,935
...back to their families.

477
00:45:31,064 --> 00:45:33,191
Adam, you will be careful?

478
00:45:41,116 --> 00:45:42,826
We won't let you take them back.

479
00:45:43,076 --> 00:45:46,079
-We talked it over and we're agreed.
-They ain't going.

480
00:45:46,413 --> 00:45:48,456
They're our girls now.

481
00:45:51,501 --> 00:45:54,754
I'm taking them back.
I'm still head of this family.

482
00:45:56,297 --> 00:45:58,091
I reckon you'll have to show me.

483
00:45:58,383 --> 00:45:59,634
Likewise.

484
00:46:02,345 --> 00:46:05,682
What do I have to do,
beat some sense into your dumb heads?

485
00:46:05,974 --> 00:46:07,851
Taking them back's the only way.

486
00:46:08,601 --> 00:46:11,146
-The whole town's coming here.
-Let them come.

487
00:46:11,354 --> 00:46:13,231
We'll fight them all.

488
00:46:14,941 --> 00:46:17,402
Sure you will.
And who will you be fighting?

489
00:46:17,694 --> 00:46:19,237
The girls' kinfolk.

490
00:46:19,446 --> 00:46:22,407
Someone's bound to get hurt.
A father, a brother.

491
00:46:24,367 --> 00:46:26,745
Think the girls will
marry you after that?

492
00:46:33,752 --> 00:46:35,337
Adam's right.

493
00:46:36,046 --> 00:46:37,839
I stand with Adam.

494
00:46:38,715 --> 00:46:40,508
Well, don't stand too close.

495
00:46:41,676 --> 00:46:43,803
The girls have gone.
They've run away!

496
00:46:44,387 --> 00:46:46,681
Go get them. Get them back.

497
00:46:47,307 --> 00:46:48,683
Come back here!

498
00:46:54,773 --> 00:46:57,442
Dorcas!
Come back. You'll get hurt.

499
00:47:00,779 --> 00:47:03,239
Keep it quiet.
They can hear us from here.

500
00:47:03,490 --> 00:47:05,116
We'll creep up on them.

501
00:47:07,285 --> 00:47:08,536
Please, Alice!

502
00:47:08,745 --> 00:47:12,415
Where are you? Come back, Martha.
Don't run away.

503
00:47:14,834 --> 00:47:17,712
I'm not going back!
I want to stay here with you!

504
00:47:17,962 --> 00:47:20,340
You've got to.
It's the only way.

505
00:47:23,259 --> 00:47:24,427
It's Alice!

506
00:47:24,761 --> 00:47:26,137
Come on, Ruth.

507
00:47:34,813 --> 00:47:36,231
There they are!

508
00:47:50,996 --> 00:47:53,039
Don't. You'll hit Dorcas.

509
00:48:04,050 --> 00:48:05,510
Oh, no...

510
00:48:06,136 --> 00:48:07,679
Not that!

511
00:48:19,941 --> 00:48:20,775
Grab her dress!

512
00:48:39,627 --> 00:48:41,087
Why, Pa!

513
00:48:57,020 --> 00:48:58,188
What will you do?

514
00:48:58,480 --> 00:49:01,316
Hang him with the rest
from the nearest tree.

515
00:49:05,820 --> 00:49:06,905
Just a minute!

516
00:49:07,155 --> 00:49:10,659
Before we go, there's one thing
we've got to know.

517
00:49:10,950 --> 00:49:12,327
Girls! Girls!

518
00:49:12,869 --> 00:49:14,996
Reverend, what is it
you want to know?

519
00:49:15,246 --> 00:49:17,374
We're all fathers here,
and we love you...

520
00:49:17,666 --> 00:49:19,709
...so don't be afraid to answer.

521
00:49:19,918 --> 00:49:23,171
I heard a wee babe crying
in the house. Whose is it?

522
00:49:25,590 --> 00:49:27,300
Don't be afraid to tell.

523
00:49:30,804 --> 00:49:32,055
Mine!

524
00:49:36,810 --> 00:49:39,479
Do you, Alice, Ruth, Martha,
Liza, Sarah, Dorcas...

525
00:49:39,729 --> 00:49:42,607
...take these men to be
your lawfully wedded husbands?

526
00:49:47,570 --> 00:49:48,571
We do.

527
00:49:48,822 --> 00:49:51,992
Do you, Benjamin, Caleb, Daniel,
Ephraim, Frankincense, Gideon...

528
00:49:52,242 --> 00:49:55,120
...take these girls to be
your lawfully wedded wives?

529
00:50:00,208 --> 00:50:01,376
We do.

530
00:50:01,626 --> 00:50:04,295
I now pronounce you men and wives.

