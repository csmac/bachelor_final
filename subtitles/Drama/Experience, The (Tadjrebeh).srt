1
00:00:17,933 --> 00:00:19,731
Previously on
The Girlfriend Experience...

2
00:00:19,732 --> 00:00:21,456
No, you're not getting the point.

3
00:00:21,457 --> 00:00:22,839
No, I think I'm getting the point.

4
00:00:22,840 --> 00:00:24,056
Look, if you don't want
to come, that's fine.

5
00:00:24,057 --> 00:00:25,056
No, I want to come.

6
00:00:25,057 --> 00:00:26,122
I'm just...

7
00:00:26,123 --> 00:00:27,422
curious.

8
00:00:27,423 --> 00:00:29,322
It's just drinks.

9
00:00:29,323 --> 00:00:31,322
Also, he calls me Ashley.

10
00:00:31,323 --> 00:00:32,289
Ashley?

11
00:00:32,290 --> 00:00:34,122
It's my online profile.

12
00:00:34,123 --> 00:00:35,561
You find guys online?

13
00:00:36,075 --> 00:00:37,767
I find everything online.

14
00:00:39,008 --> 00:00:40,307
I get it.

15
00:00:40,308 --> 00:00:42,408
I don't need that.

16
00:00:42,409 --> 00:00:44,808
It's just a gift.

17
00:00:44,809 --> 00:00:46,416
I know what it is.

18
00:00:47,375 --> 00:00:49,308
Should I be Chelsea?

19
00:00:49,309 --> 00:00:51,209
You can be whoever you want to be.

20
00:00:53,482 --> 00:00:56,870
<font color="#ff0000">Sync and corrections by btsix
www.addic7ed.com</font>

21
00:01:47,575 --> 00:01:48,841
How's it going?

22
00:01:48,842 --> 00:01:50,241
Hey.

23
00:01:50,242 --> 00:01:52,291
It's been pretty non-stop.

24
00:01:53,275 --> 00:01:54,914
Let me get this to you now.

25
00:01:55,908 --> 00:01:57,661
You know, some of Stacy's
friends are having

26
00:01:57,662 --> 00:01:59,174
a party over at Logan Square tonight.

27
00:01:59,175 --> 00:02:00,207
You want to come?

28
00:02:00,208 --> 00:02:01,614
Does Stacy want me to come?

29
00:02:01,615 --> 00:02:03,327
Yeah, she's the one who
asked me to invite you.

30
00:02:03,328 --> 00:02:05,027
You should come.

31
00:02:05,028 --> 00:02:06,827
You'd really like her.
She's pretty incredible.

32
00:02:06,828 --> 00:02:08,894
Did you tell her about us?

33
00:02:09,360 --> 00:02:12,054
Not yet. But I'm sure she'll
understand, it's in the past.

34
00:02:13,528 --> 00:02:15,694
Don't be naive.

35
00:02:15,695 --> 00:02:17,961
Cash it soon.

36
00:02:17,962 --> 00:02:19,961
You're so cynical.

37
00:02:19,962 --> 00:02:21,595
See ya.

38
00:02:23,961 --> 00:02:27,960
I want you to amend claim 32.

39
00:02:27,961 --> 00:02:29,499
Hold on.

40
00:02:30,161 --> 00:02:33,027
You've got great organizational skills.

41
00:02:33,028 --> 00:02:34,494
It's all up here.

42
00:02:34,495 --> 00:02:36,461
I know where everything is.

43
00:02:36,462 --> 00:02:40,661
I'm hearing some rumors...

44
00:02:40,662 --> 00:02:41,661
Christine...

45
00:02:41,662 --> 00:02:42,961
Ben Holgrem.

46
00:02:42,962 --> 00:02:44,694
Learn this guy's face.

47
00:02:44,695 --> 00:02:46,161
He's the devil.

48
00:02:46,162 --> 00:02:48,961
My client is suing the
shit out of his client.

49
00:02:48,962 --> 00:02:51,827
I heard XHP is unhappy with you.

50
00:02:51,828 --> 00:02:53,394
Tell Susan to hold my calls.

51
00:03:02,260 --> 00:03:03,560
He wants you to hold his calls.

52
00:03:20,560 --> 00:03:23,959
I mean it's not something that
I want to make into a career,

53
00:03:23,960 --> 00:03:25,326
but I actually enjoy it.

54
00:03:25,327 --> 00:03:28,359
I like meeting new people,
I like having sex.

55
00:03:28,360 --> 00:03:30,960
And I only do it a few times a month.

56
00:03:32,727 --> 00:03:34,359
Do you meet all your clients online?

57
00:03:34,360 --> 00:03:36,093
A few.

58
00:03:36,094 --> 00:03:38,093
This woman, Jacqueline,
vets them for me,

59
00:03:38,094 --> 00:03:40,093
and takes care of the bookings.

60
00:03:40,094 --> 00:03:41,226
Why? Do you want to meet her?

61
00:03:41,227 --> 00:03:44,093
I could introduce you, if you want.

62
00:03:44,094 --> 00:03:46,093
No, just curious.

63
00:03:46,094 --> 00:03:47,960
Okay.

64
00:03:47,961 --> 00:03:49,960
But there's a difference
between being curious

65
00:03:49,961 --> 00:03:52,160
and actually following through.

66
00:03:52,161 --> 00:03:53,760
I'm aware of that.

67
00:03:53,761 --> 00:03:56,760
If you meet Jacqueline, she's
gonna wanna book for you.

68
00:03:56,761 --> 00:03:59,093
You're smart and hot and funny.

69
00:03:59,094 --> 00:04:01,259
You just have to want to do it.

70
00:04:01,260 --> 00:04:03,426
I mean I get off on it.
I like the rush...

71
00:04:03,427 --> 00:04:05,993
All the attention, knowing he wants me.

72
00:04:05,994 --> 00:04:07,660
Then there's the money.

73
00:04:07,661 --> 00:04:08,893
And at the end of the day,

74
00:04:08,894 --> 00:04:10,160
if I don't get along with a client,

75
00:04:10,161 --> 00:04:11,660
I just move on.

76
00:04:11,661 --> 00:04:15,493
All I really have to do is
listen and ask questions.

77
00:04:15,494 --> 00:04:17,260
And fuck?

78
00:04:17,261 --> 00:04:19,026
Yeah.

79
00:04:19,027 --> 00:04:20,493
And fuck.

80
00:04:44,194 --> 00:04:45,293
Here's sixty.

81
00:04:45,294 --> 00:04:46,427
Thanks.

82
00:05:17,027 --> 00:05:19,493
- Hi.
- Hi, come on in.

83
00:05:19,494 --> 00:05:23,226
You're gonna have to play
catch up. We're two deep.

84
00:05:23,227 --> 00:05:25,026
Think I can handle that.

85
00:05:32,360 --> 00:05:34,093
Look who's here?

86
00:05:34,094 --> 00:05:35,093
Chelsea!

87
00:05:35,094 --> 00:05:36,093
Hi.

88
00:05:36,094 --> 00:05:37,326
Hey, glad you came.

89
00:05:37,327 --> 00:05:38,460
Me, too.

90
00:05:41,394 --> 00:05:42,526
You look beautiful.

91
00:05:42,527 --> 00:05:44,359
Thank you.

92
00:05:44,360 --> 00:05:48,093
No, it's interesting because
we have some sizable estates,

93
00:05:48,094 --> 00:05:50,160
and there's no better sport

94
00:05:50,161 --> 00:05:52,393
than watching the wealthy try
to hang onto their money.

95
00:05:52,394 --> 00:05:54,093
Or try to get some

96
00:05:54,094 --> 00:05:55,093
they think they deserve.

97
00:05:55,094 --> 00:05:56,726
It just never gets old.

98
00:05:56,727 --> 00:05:58,393
Really?

99
00:05:58,394 --> 00:06:00,628
So it's not just the same
story over and over again

100
00:06:00,629 --> 00:06:01,626
with different names?

101
00:06:01,627 --> 00:06:02,487
Not yet.

102
00:06:02,488 --> 00:06:04,626
And I've been doing this for 20 years.

103
00:06:04,627 --> 00:06:06,167
Huh.

104
00:06:06,168 --> 00:06:07,694
Do you have a lawyer?

105
00:06:07,695 --> 00:06:09,047
Me? No.

106
00:06:09,048 --> 00:06:11,514
Seriously? You should.

107
00:06:11,515 --> 00:06:13,647
You can be my lawyer.

108
00:06:13,648 --> 00:06:15,648
I would happily be your lawyer,

109
00:06:15,649 --> 00:06:17,514
but then...

110
00:06:17,515 --> 00:06:19,281
this would have to stop.

111
00:06:19,282 --> 00:06:21,581
Why?

112
00:06:21,582 --> 00:06:25,081
We're just starting to
get to know each other.

113
00:06:39,149 --> 00:06:40,548
Keep it.

114
00:06:40,549 --> 00:06:41,614
Welcome.

115
00:06:41,615 --> 00:06:43,114
- Are you checking in?
- Yeah.

116
00:06:43,115 --> 00:06:44,614
- Do you have any luggage?
- No.

117
00:07:17,348 --> 00:07:18,781
Make yourself comfortable.

118
00:08:06,448 --> 00:08:07,381
We're good?

119
00:08:07,382 --> 00:08:09,914
Yeah. Great.

120
00:08:09,915 --> 00:08:10,914
Drink?

121
00:08:10,915 --> 00:08:14,147
Yes, vodka, please...

122
00:08:14,148 --> 00:08:15,515
um, with soda.

123
00:08:40,382 --> 00:08:41,382
Thanks.

124
00:08:55,582 --> 00:08:56,748
Come here.

125
00:09:27,139 --> 00:09:29,338
Do you like that?

126
00:09:29,339 --> 00:09:30,605
Yes.

127
00:09:52,372 --> 00:09:55,071
Yeah, listen, my 12 o'clock's
here now, so I gotta run,

128
00:09:55,072 --> 00:09:57,271
but we'll talk about it later, okay?

129
00:09:57,272 --> 00:09:58,671
Yeah...

130
00:09:58,672 --> 00:10:00,671
We'll sort it out, I'm not worried.

131
00:10:00,672 --> 00:10:03,271
Okay, bye-bye.

132
00:10:03,272 --> 00:10:04,271
Jacqueline.

133
00:10:04,272 --> 00:10:05,271
You are Christine.

134
00:10:05,272 --> 00:10:06,638
I am.

135
00:10:06,639 --> 00:10:09,405
You didn't tell me she was so beautiful!

136
00:10:09,406 --> 00:10:10,905
She's great.

137
00:10:12,705 --> 00:10:15,838
Federal Circuit has a monopoly
on all patent appeals.

138
00:10:15,839 --> 00:10:17,505
It was established by Ronald Reagan and

139
00:10:17,506 --> 00:10:19,271
is hardly met a patent it doesn't like.

140
00:10:19,272 --> 00:10:21,338
If they make an error
interpreting patent code,

141
00:10:21,339 --> 00:10:23,605
then every later judge is
obliged to follow their ruling.

142
00:10:23,606 --> 00:10:27,171
The whole system is a complete mess.

143
00:10:27,172 --> 00:10:28,438
It's so great you know this stuff.

144
00:10:28,439 --> 00:10:29,638
Stuff like law?

145
00:10:29,639 --> 00:10:31,438
Yes.

146
00:10:31,439 --> 00:10:34,338
I do realize it's the result of
years of hard work and school,

147
00:10:34,339 --> 00:10:36,405
and that's your passion.

148
00:10:36,406 --> 00:10:38,237
Well, it's costing me enough.

149
00:10:39,305 --> 00:10:40,846
It'll pay off.

150
00:10:41,439 --> 00:10:43,118
At least from this end.

151
00:10:44,372 --> 00:10:46,605
Now I already have a couple
of exceptional men in mind

152
00:10:46,606 --> 00:10:48,571
who I think would love you.

153
00:10:48,572 --> 00:10:50,005
Do you have any photos?

154
00:10:51,939 --> 00:10:52,905
Okay...

155
00:10:52,906 --> 00:10:54,472
We'll get you some.

156
00:10:56,605 --> 00:10:58,805
Don't worry, no one will
ever see your eyes.

157
00:11:02,705 --> 00:11:04,338
- You ready?
- Yeah.

158
00:11:04,339 --> 00:11:05,338
Where do you want me?

159
00:11:05,339 --> 00:11:06,872
Just up against the curtain.

160
00:11:12,539 --> 00:11:14,109
That's perfect.

161
00:11:15,739 --> 00:11:17,505
You look great.

162
00:11:17,506 --> 00:11:19,905
Angle your chin up a little bit.

163
00:11:19,906 --> 00:11:21,705
Beautiful.

164
00:11:21,706 --> 00:11:24,472
Now just slowly start to
unbutton your blouse.

165
00:11:30,605 --> 00:11:31,805
Perfect.

166
00:11:39,505 --> 00:11:40,738
Beautiful.

167
00:11:44,805 --> 00:11:47,738
Yeah, just chin up just a little bit.

168
00:11:47,739 --> 00:11:48,838
Beautiful.

169
00:11:51,439 --> 00:11:53,372
And turn to face the window.

170
00:11:55,772 --> 00:11:57,470
Great.

171
00:11:57,471 --> 00:11:59,805
Just lift the back of your
skirt up a little bit.

172
00:12:03,471 --> 00:12:04,537
Gorgeous.

173
00:12:07,838 --> 00:12:09,004
Beautiful.

174
00:12:56,838 --> 00:12:58,804
It's whatever you're comfortable with,

175
00:12:58,805 --> 00:13:01,404
and how much you wanna get paid.

176
00:13:01,405 --> 00:13:04,770
I want to make sure that
you're matched properly.

177
00:13:04,771 --> 00:13:06,104
For reasons I will let you figure out,

178
00:13:06,105 --> 00:13:08,371
most of your clients will be white

179
00:13:08,372 --> 00:13:10,361
and approaching middle age.

180
00:13:11,405 --> 00:13:13,071
It should also be enjoyable for you.

181
00:13:13,072 --> 00:13:15,471
I mean the more you enjoy
it, the more they will too.

182
00:13:15,472 --> 00:13:17,004
My big question is...

183
00:13:17,005 --> 00:13:20,604
Why would I go with you
when I can do this myself?

184
00:13:20,605 --> 00:13:22,137
You mean why pay me?

185
00:13:22,138 --> 00:13:23,370
Yeah.

186
00:13:23,371 --> 00:13:25,737
Well, it's just math really.

187
00:13:25,738 --> 00:13:27,870
And quality of life.

188
00:13:27,871 --> 00:13:31,604
The hours that you would spend
finding and then vetting clients

189
00:13:31,605 --> 00:13:35,137
could be spent with
clients, making money.

190
00:13:35,138 --> 00:13:36,604
I'm the infrastructure.

191
00:13:36,605 --> 00:13:39,437
You don't work for me, you work with me.

192
00:13:39,438 --> 00:13:41,570
And if you're unhappy...

193
00:13:41,571 --> 00:13:43,337
you leave.

194
00:13:43,338 --> 00:13:44,737
There's no contract.

195
00:13:46,605 --> 00:13:48,671
I already have one client.

196
00:13:48,672 --> 00:13:49,771
Hmmm...

197
00:13:52,671 --> 00:13:54,070
At least I think I do.

198
00:13:54,071 --> 00:13:57,371
Okay, well, he's all yours.

199
00:13:57,372 --> 00:13:58,771
Can I tell you about Ryan?

200
00:14:00,105 --> 00:14:01,437
He's assertive.

201
00:14:01,438 --> 00:14:04,004
He knows exactly what he likes,

202
00:14:04,005 --> 00:14:06,704
but everyone who's seen
him had a great time.

203
00:14:06,705 --> 00:14:07,904
What are you comfortable with?

204
00:14:07,905 --> 00:14:09,504
Did you and Avery talk about it?

205
00:14:09,505 --> 00:14:11,171
A little. I mean I get the gist of it.

206
00:14:11,172 --> 00:14:12,571
Mm-hmm.

207
00:14:12,572 --> 00:14:13,671
Bareback blow jobs?

208
00:14:13,672 --> 00:14:15,137
Overnights?

209
00:14:15,138 --> 00:14:16,637
Dinner at the Y?

210
00:14:16,638 --> 00:14:17,437
Yeah.

211
00:14:17,438 --> 00:14:18,699
I'm fine with all of that.

212
00:14:19,605 --> 00:14:20,571
Great!

213
00:14:22,738 --> 00:14:23,971
More wine?

214
00:14:37,738 --> 00:14:38,736
Chelsea?

215
00:14:38,737 --> 00:14:40,704
Ryan...

216
00:14:40,705 --> 00:14:41,745
You're late.

217
00:14:42,605 --> 00:14:43,937
I'm always late.

218
00:14:43,938 --> 00:14:45,204
She's older.

219
00:14:45,205 --> 00:14:46,704
Of course she is.

220
00:14:46,705 --> 00:14:48,671
So you have one sister.
What does she do?

221
00:14:48,672 --> 00:14:50,504
She's a math teacher.

222
00:14:50,505 --> 00:14:51,504
What grade?

223
00:14:51,505 --> 00:14:52,671
Middle school.

224
00:14:52,672 --> 00:14:54,537
- Any kids?
- One.

225
00:14:54,538 --> 00:14:57,737
A boy. He's 4. He's got a lisp.

226
00:14:57,738 --> 00:14:58,870
A lisp?

227
00:14:58,871 --> 00:15:00,176
That's cute.

228
00:15:00,177 --> 00:15:01,254
What's his name?

229
00:15:02,837 --> 00:15:03,846
Sean.

230
00:15:05,038 --> 00:15:06,004
Bullshit.

231
00:15:07,871 --> 00:15:08,870
Come on, you gotta rehearse this stuff.

232
00:15:08,871 --> 00:15:10,937
Look, I know you're new,

233
00:15:10,938 --> 00:15:13,104
but Jacqueline should be runnin'
bio drills with you or something.

234
00:15:13,105 --> 00:15:15,004
Okay, she doesn't have a kid.

235
00:15:15,005 --> 00:15:16,370
She could have 10 kids.

236
00:15:16,371 --> 00:15:18,370
She could be a he. I don't care.

237
00:15:22,038 --> 00:15:25,104
I think you should move
closer, "Chelsea".

238
00:15:25,105 --> 00:15:26,938
Okay, "Ryan".

239
00:15:33,703 --> 00:15:34,893
How was your day?

240
00:15:48,336 --> 00:15:49,335
Are you on the pill?

241
00:15:49,336 --> 00:15:50,602
Yes...

242
00:15:50,603 --> 00:15:52,259
But I'd like to use a condom.

243
00:15:53,103 --> 00:15:54,702
Yeah, I don't have any.

244
00:15:54,703 --> 00:15:55,660
I do.

245
00:15:58,703 --> 00:16:00,069
Let's go to the bathroom.

246
00:16:55,203 --> 00:16:56,469
Face me.

247
00:17:05,836 --> 00:17:07,302
Perfect.

248
00:17:07,303 --> 00:17:08,935
Just like that.

249
00:17:08,936 --> 00:17:10,102
Fuck.

250
00:17:25,236 --> 00:17:26,302
Water is included.

251
00:17:26,303 --> 00:17:28,835
Electric would be separate.

252
00:17:28,836 --> 00:17:30,135
Take your time.

253
00:17:38,036 --> 00:17:39,602
I think this place is great for you.

254
00:17:43,903 --> 00:17:45,203
It's too expensive.

255
00:17:47,336 --> 00:17:48,969
I can't afford this.

256
00:17:52,069 --> 00:17:53,748
What if I cover the deposit?

257
00:17:55,469 --> 00:17:57,431
And if you keep working,

258
00:17:57,836 --> 00:17:59,536
it'll figure itself out.

259
00:18:05,602 --> 00:18:09,038
Emery, this is part of the process.

260
00:18:10,036 --> 00:18:12,695
I'm not trying to manipulate you!

261
00:18:13,236 --> 00:18:15,668
I'm sure that when you come down...

262
00:18:15,669 --> 00:18:19,005
Hey, we're doing
everything we fucking can!

263
00:18:20,102 --> 00:18:21,235
Hello?

264
00:18:23,275 --> 00:18:24,275
Shit!

265
00:18:24,640 --> 00:18:26,373
We already gave you a chance.

266
00:18:26,374 --> 00:18:28,039
This is how things move.

267
00:18:28,040 --> 00:18:30,746
Don't fucking lecture
me on how things move!

268
00:18:30,747 --> 00:18:31,553
Three mistrials.

269
00:18:31,554 --> 00:18:33,786
Three fucking mistrials, David.

270
00:18:33,787 --> 00:18:35,554
Explain to me why I need
to hear anything else

271
00:18:35,555 --> 00:18:37,547
out of your mouth other
than you fucked up.

272
00:18:37,548 --> 00:18:39,813
This is not the time to switch counsel.

273
00:18:39,814 --> 00:18:43,288
You will lose if Kersner
sees you have doubts.

274
00:18:43,289 --> 00:18:44,589
Do you get that?

275
00:18:44,590 --> 00:18:46,019
I think what David is trying to say...

276
00:18:46,020 --> 00:18:48,144
I don't need to hear the same
goddamn words in a different tone.

277
00:18:48,145 --> 00:18:50,720
I completely understand where
you are coming from, Emery.

278
00:18:50,721 --> 00:18:52,820
We, or maybe I should
just speak for myself,

279
00:18:52,821 --> 00:18:55,720
I just want to know how to fix this.

280
00:18:55,721 --> 00:18:57,488
How do you see this moving forward?

281
00:18:59,789 --> 00:19:02,287
He didn't say. I don't know.

282
00:19:02,288 --> 00:19:04,120
Maybe his wife's coming to town or

283
00:19:04,121 --> 00:19:06,920
maybe he's just not
that into me anymore.

284
00:19:06,921 --> 00:19:10,020
Either way, I have to pack
up my shit and leave.

285
00:19:10,021 --> 00:19:13,854
It's really not that big a deal.

286
00:19:13,855 --> 00:19:16,654
Just stay with me until
you figure it out.

287
00:19:16,655 --> 00:19:18,722
Maybe Jacqueline can lend you
money. She charges enough.

288
00:19:22,055 --> 00:19:23,920
How much are you paying her?

289
00:19:23,921 --> 00:19:25,654
Same as you...

290
00:19:25,655 --> 00:19:26,922
30%.

291
00:19:29,755 --> 00:19:31,154
Kind of steep, no?

292
00:19:31,155 --> 00:19:33,050
Yeah, but it's pretty standard.

293
00:19:33,722 --> 00:19:35,120
Why, you don't like her?

294
00:19:35,121 --> 00:19:37,220
No, she's great, I just...

295
00:19:37,221 --> 00:19:39,220
30%'s a lot.

296
00:19:39,221 --> 00:19:40,787
That's all.

297
00:19:40,788 --> 00:19:42,545
You can tell me if you don't like her.

298
00:19:43,955 --> 00:19:45,221
I like her.

299
00:19:50,055 --> 00:19:51,787
Don't tell her I said anything.

300
00:19:51,788 --> 00:19:53,122
Of course.

301
00:20:04,888 --> 00:20:07,020
So, you can sleep in here.

302
00:20:07,021 --> 00:20:10,124
Obviously, I haven't slept in
it, but it seems comfortable.

303
00:20:12,822 --> 00:20:16,920
Ahhh, if you need to do
your laundry, it's the...

304
00:20:16,921 --> 00:20:19,220
closet right next to the kitchen.

305
00:20:19,221 --> 00:20:20,388
Yeah.

306
00:20:34,389 --> 00:20:36,221
This place is really nice.

307
00:20:36,222 --> 00:20:38,087
Yeah.

308
00:20:38,088 --> 00:20:41,554
Still getting used to it, but I like it.

309
00:20:41,555 --> 00:20:43,887
How much do you pay for it?

310
00:20:43,888 --> 00:20:46,154
Probably too much.

311
00:20:46,155 --> 00:20:47,921
Don't you have loans to pay off?

312
00:20:47,922 --> 00:20:50,555
Yeah, Jacqueline actually put the
deposit down, so that helps.

313
00:20:51,822 --> 00:20:53,954
So you can take "in calls"?

314
00:20:53,955 --> 00:20:56,154
No, I am paying her back.
She was just helping.

315
00:20:56,155 --> 00:20:57,954
Oh.

316
00:20:57,955 --> 00:20:59,221
Here.

317
00:20:59,222 --> 00:21:01,045
Just pay me back when you have it.

318
00:21:01,046 --> 00:21:02,662
I have credit cards, I'm fine.

319
00:21:03,513 --> 00:21:05,410
Speaking of which, let's go to lunch.

320
00:21:06,479 --> 00:21:08,511
It's on me, it's the least I could do.

321
00:21:08,512 --> 00:21:12,145
We'll have the forest
mushrooms, the kale salad,

322
00:21:12,146 --> 00:21:17,112
the ahi tuna tartare, diver
scallops and two mimosas...

323
00:21:17,113 --> 00:21:18,280
Please.

324
00:21:20,179 --> 00:21:21,612
Thanks.

325
00:21:24,379 --> 00:21:26,511
You deserve it.

326
00:21:26,512 --> 00:21:27,779
We deserve it.

327
00:21:29,979 --> 00:21:31,652
So how is work?

328
00:21:31,653 --> 00:21:33,245
Which work?

329
00:21:33,246 --> 00:21:34,578
I'm currently burning a
candle at three ends,

330
00:21:34,579 --> 00:21:36,511
which I didn't know was possible.

331
00:21:36,512 --> 00:21:38,145
Maybe one should go.

332
00:21:38,146 --> 00:21:40,112
Yeah, maybe.

333
00:21:40,113 --> 00:21:41,645
For now, it's fun.

334
00:21:41,646 --> 00:21:44,758
And have you noticed that
people are fucking insane?

335
00:21:59,312 --> 00:22:00,611
Hello.

336
00:22:00,612 --> 00:22:02,678
Christine. Hi, it's Jacqueline.

337
00:22:02,679 --> 00:22:05,411
Listen, I have someone
I'd love you to meet.

338
00:22:05,412 --> 00:22:06,812
Are you free for lunch on Thursday?

339
00:22:06,813 --> 00:22:08,548
Yeah, I can make that work.

340
00:22:09,813 --> 00:22:11,688
I'm so sorry to hear that.

341
00:22:13,213 --> 00:22:14,678
When did she pass away?

342
00:22:14,679 --> 00:22:16,625
It's been eleven months now.

343
00:22:17,846 --> 00:22:20,345
That's gotta be hard.

344
00:22:20,346 --> 00:22:22,392
I think it's been harder on the kids.

345
00:22:23,379 --> 00:22:24,578
Are you close to them?

346
00:22:24,579 --> 00:22:25,545
I was...

347
00:22:25,546 --> 00:22:27,268
Least I think I was.

348
00:22:29,219 --> 00:22:31,119
But they're adults now.

349
00:22:34,679 --> 00:22:36,412
They've got their own lives.

350
00:22:38,712 --> 00:22:42,711
Why it's so nice to spend time
with someone and just talk.

351
00:22:42,712 --> 00:22:44,645
Especially to a...

352
00:22:44,646 --> 00:22:47,512
to a beautiful young woman.

353
00:22:47,513 --> 00:22:50,078
We can talk whenever you like.

354
00:22:54,479 --> 00:22:55,679
Jacqueline?

355
00:22:57,579 --> 00:23:00,178
Oh, she-she's wonderful.

356
00:23:00,179 --> 00:23:02,412
We're having a really
nice time together.

357
00:23:04,946 --> 00:23:06,379
Declined?

358
00:23:08,446 --> 00:23:09,630
Well, that's surprising.

359
00:23:10,913 --> 00:23:12,312
Yeah.

360
00:23:12,313 --> 00:23:13,321
Hang on.

361
00:23:14,513 --> 00:23:16,380
I'm just gonna use the restroom.

362
00:23:20,779 --> 00:23:22,445
Okay...

363
00:23:27,446 --> 00:23:28,812
2226.

364
00:24:48,441 --> 00:24:49,541
Christine...

365
00:24:52,874 --> 00:24:54,090
What is it?

366
00:24:54,474 --> 00:24:56,164
Was having problems sleeping.

367
00:24:57,441 --> 00:24:59,608
I'm really sorry, I shouldn't
have brought him here.

368
00:25:02,807 --> 00:25:04,640
Can I lie down beside you?

369
00:25:04,641 --> 00:25:05,841
Yeah.

370
00:25:17,707 --> 00:25:19,464
I'm sorry, I should have asked.

371
00:25:20,441 --> 00:25:22,204
I can give you half for letting me.

372
00:25:22,808 --> 00:25:25,736
- I'll give you $500.
- I don't want your money.

373
00:25:26,341 --> 00:25:28,473
Jacqueline won't call me back.

374
00:25:28,474 --> 00:25:31,273
- She won't tell me why.
- Go to sleep.

375
00:25:31,274 --> 00:25:33,406
I have to get up at six in the morning.

376
00:25:33,407 --> 00:25:35,474
Oh really? Fuck, I'm sorry.

377
00:25:51,207 --> 00:25:52,840
You're really warm.

378
00:25:58,174 --> 00:26:00,306
Thank you for being there for me.

379
00:26:00,307 --> 00:26:01,807
Of course.

380
00:26:18,641 --> 00:26:19,974
Kiss me.

381
00:26:37,874 --> 00:26:39,874
<font color="#ff0000">Sync and corrections by btsix
www.addic7ed.com</font>

