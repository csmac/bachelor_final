1
00:00:44,146 --> 00:00:49,118
And on tuesday evening you poured yourself some
brandy, drank the whole glass in one gulp,

2
00:00:50,379 --> 00:00:54,245
closed the cupboard and turned the light off in the living
room, went to the bathroom and brushed your teeth

3
00:00:55,661 --> 00:00:59,025
and in the bedroom you skimmed through your
newspaper and fell asleep
- So?

4
00:00:59,830 --> 00:01:04,255
So what do you mean? I drank my brandy,
turned off the light

5
00:01:06,195 --> 00:01:09,324
brushed my teeth and eventually
fell asleep. So?

6
00:01:10,409 --> 00:01:14,465
Nothing. Only that I was still
sitting in the living room.

7
00:01:34,823 --> 00:01:39,606
Listen, Katja, let's take our time in
the evening and discuss about this.

8
00:01:40,548 --> 00:01:44,698
Now I'm on my way to the client, let us later
have dinner together.
- You are oblivious to me.

9
00:01:45,235 --> 00:01:51,022
You know that this is not true. I was
probably lost in thoughts. I'm sorry.

10
00:01:51,832 --> 00:01:55,402
One may be lost in thoughts but not
completely forget there's another person
living in the same appartement.

11
00:01:55,495 --> 00:01:57,132
I'm really sorry.

12
00:01:57,921 --> 00:02:00,204
And that's not all. On Wednesday,
when Lobenstein enquired about my job,

13
00:02:00,448 --> 00:02:03,501
you couldn't stand it.
You immediately started talking about you.

14
00:02:03,608 --> 00:02:08,521
Every time when it's about me ...
- Damn it, Katja, have you compiled a list of this ...

15
00:02:11,787 --> 00:02:14,771
What have you said?

16
00:02:16,300 --> 00:02:19,253
Katja, let's ...
- What have you just said?

17
00:02:19,786 --> 00:02:23,437
I asked yo if you made a complete list of this.
That's what I've asked you.

18
00:02:25,254 --> 00:02:27,820
Yes, I did. And that's the end of our conversation.

19
00:03:15,396 --> 00:03:16,589
Shit.

20
00:04:54,465 --> 00:04:56,293
So, what do we have here?

21
00:05:08,760 --> 00:05:11,729
Revlon 17 Vixen.

22
00:05:13,047 --> 00:05:15,002
What's so funny about it?

23
00:05:19,831 --> 00:05:22,416
Do you know what BDS stands for?

24
00:05:23,542 --> 00:05:24,956
No, but i'm really anxious to hear it.

25
00:05:25,763 --> 00:05:27,848
Brigade of the dim-witted.

26
00:05:30,308 --> 00:05:32,250
And what does the S stand for?

27
00:05:35,055 --> 00:05:36,651
Ok

28
00:05:42,491 --> 00:05:46,620
Have a nice evening. Bye.

29
00:05:53,362 --> 00:05:56,981
Laura... Excuse me ...
Could I talk to you for a moment?

30
00:05:58,650 --> 00:06:00,162
Alone.

31
00:06:05,940 --> 00:06:08,976
Sorry...  This was not my idea.

32
00:06:17,151 --> 00:06:19,265
Don't you believe me?

33
00:06:19,948 --> 00:06:22,131
I believe you. But I've finished for today.

34
00:06:30,117 --> 00:06:33,803
What did he want?
- He invited me to a meal.

35
00:06:35,289 --> 00:06:37,174
Lunch or dinner?
- Dinner

36
00:06:37,355 --> 00:06:39,776
And after that to dance .... tango.

37
00:07:10,303 --> 00:07:12,240
Laura!

38
00:07:23,594 --> 00:07:25,855
Are you Laura Reiser?

39
00:07:29,477 --> 00:07:33,109
Would you please come with me. Your son
had an accident. He's in the hospital.

40
00:09:03,254 --> 00:09:05,325
You were right.

41
00:09:08,675 --> 00:09:10,790
I did make a list.

42
00:09:26,844 --> 00:09:31,231
Only because I knew I would cry.

43
00:09:32,962 --> 00:09:35,467
And when I cry, I get mixed up.

44
00:09:41,720 --> 00:09:43,929
Katja!

45
00:10:26,283 --> 00:10:29,012
Do you think he'll die?

46
00:10:32,662 --> 00:10:34,984
Rubbish, how do you come up with this idea?

47
00:10:38,839 --> 00:10:40,969
You didn't bring any of his things.

48
00:10:57,245 --> 00:10:59,644
I don't need anything here.

49
00:11:15,939 --> 00:11:19,337
Good morning I'd like a car,

50
00:11:20,532 --> 00:11:22,506
17 Nordhoftra�e, Gerber

51
00:11:23,532 --> 00:11:25,909
All right. Thank you.

52
00:11:29,582 --> 00:11:32,426
Why don't you drive with your car?

53
00:11:33,422 --> 00:11:36,055
It's broken down.
- Serious damage?

54
00:11:36,505 --> 00:11:38,189
The apex seal of the rotary piston.

55
00:11:38,263 --> 00:11:41,450
Then I can drive you.
- You don't need to.

56
00:11:46,360 --> 00:11:49,690
... Apex seal of the rotary piston
- What about it?

57
00:11:50,532 --> 00:11:52,291
Nothing ... it sounds beautiful.

58
00:11:52,851 --> 00:11:55,252
I would like to cancel the taxi for Gerber. 
Thank you.

59
00:12:35,423 --> 00:12:38,760
Shall I pick you up.
- No, you don't need to. I'll take a demonstration car.

60
00:12:44,450 --> 00:12:45,867
What's with your car?
- It's broken down.

61
00:12:46,513 --> 00:12:49,723
The apex seal of the rotary piston
- I understand ...

62
00:12:51,679 --> 00:12:53,659
Shall I tell someone to pick it up?
- No, I'll do it myself.

63
00:12:55,208 --> 00:12:57,182
See you later.

64
00:13:02,750 --> 00:13:04,237
So?
- Well ...

65
00:13:15,206 --> 00:13:18,748
What do you think? Are those people clients?

66
00:13:19,534 --> 00:13:20,208
No idea.

67
00:13:23,006 --> 00:13:25,882
Well, don't look at us ... look down there.

68
00:13:27,329 --> 00:13:30,263
The children's seat, for example ...
is more expensive than the entire car.

69
00:13:30,636 --> 00:13:33,045
Really.
Yeah, really. You can believe me ...

70
00:13:33,699 --> 00:13:36,323
Now they have a child and somewhere someone rich
has become a granny or a grandpa ...

71
00:13:37,307 --> 00:13:39,491
and buys a car for them.

72
00:13:39,171 --> 00:13:41,748
And you shall go downstairs and sell them one.

73
00:13:42,009 --> 00:13:45,097
And please don't start talking about
resales or the engine torque.

74
00:13:45,281 --> 00:13:47,449
They want something that's safe and
healthy for their child.

75
00:13:48,409 --> 00:13:51,258
Do you understand what I mean?
All right, then give it to them.

76
00:13:52,140 --> 00:13:53,330
Good luck!

77
00:14:00,117 --> 00:14:05,248
Do you think he'll manage it?
Yes, I suppose ... we'll see.

78
00:14:10,462 --> 00:14:13,330
And you ... do you manage it?

79
00:14:15,746 --> 00:14:17,785
What?

80
00:14:18,698 --> 00:14:21,525
To be faithful and honest.

81
00:14:22,779 --> 00:14:27,988
It's been a terrible week for Katja. I hope I won't
see my sister suffering like that again.

82
00:14:28,506 --> 00:14:32,424
If you cheat on her, I'll sack you.
Did you understand?

83
00:14:33,626 --> 00:14:35,223
I always understand you, Klaus.

84
00:14:38,062 --> 00:14:40,850
You prefer the combi because you think of
all the things you have to take with you:

85
00:14:42,629 --> 00:14:46,010
Buggy, trike, bobbycar ..

86
00:14:46,659 --> 00:14:50,494
<i>... but I assure you  that you can put all these
trouble-free in a limousine too.</i>

87
00:14:53,920 --> 00:14:58,452
<i>...  I've told you that there are
two essential reasons for ....</i>

88
00:15:01,734 --> 00:15:04,207
<b>Driver flees after running down a child</b>

89
00:15:04,777 --> 00:15:07,051
<i>Hello, could you please put out
your cigarette.</i>

90
00:15:07,936 --> 00:15:09,616
Yes, it's you I mean.

91
00:15:31,381 --> 00:15:34,169
Good afternoon. My name is Philipp Gerber.

92
00:15:35,858 --> 00:15:40,996
Yesterday afternoon I have run down a child
on the L322  between Wolfsburg and Norsteimke.

93
00:15:42,095 --> 00:15:45,027
I commited a hit-and-run.
I should have stopped and check for the boy,

94
00:15:45,713 --> 00:15:51,158
call an ambulance or drive
the boy to the hospital.

95
00:15:52,956 --> 00:15:55,490
... but I didn't do that.
I don't know why ...

96
00:15:57,041 --> 00:15:59,391
... I don't know why ...

97
00:15:59,994 --> 00:16:04,045
Maybe I was thinking of my girlfriend, who was
about to pack her things and leave me.

98
00:16:05,865 --> 00:16:08,886
But I don't know it for sure.

99
00:16:14,794 --> 00:16:18,748
I hope ... I hope that the boy is OK

100
00:16:20,554 --> 00:16:22,758
... I hope that the boy is alright.

101
00:16:24,014 --> 00:16:26,275
I hope that the boy gets well.

102
00:16:30,204 --> 00:16:33,460
Good afternoon. My name is Philipp Gerber.
Yesterday afternoon I have run down a child

103
00:16:34,592 --> 00:16:39,742
on the L322 between Wolfsburg and Norsteimke.

104
00:17:04,487 --> 00:17:08,000
<i>Is that coffee?
- There's a machine up front.</i>

105
00:17:11,748 --> 00:17:15,191
Do you have a cigarette?
- I've quit smoking.

106
00:18:22,944 --> 00:18:27,768
Excuse me, that's my coffee.
- Please.

107
00:18:30,618 --> 00:18:33,841
Do you have a cigarette?
Yes.

108
00:18:39,415 --> 00:18:41,665
Light?

109
00:18:52,568 --> 00:18:54,483
Excuse me, my name is Philipp Gerber ...

110
00:18:55,678 --> 00:18:57,944
<i>Ms. Reiser, come quickly, Paul woke up.</i>

111
00:18:58,000 --> 00:18:59,007
Please hold this.

112
00:19:05,691 --> 00:19:08,376
Is my bicycle broken?

113
00:19:12,521 --> 00:19:15,198
We'll buy another one.

114
00:19:18,103 --> 00:19:21,213
Can I chose one?

115
00:19:23,962 --> 00:19:27,483
Can you drive a bicycle at the seaside?

116
00:19:29,974 --> 00:19:33,388
Yes, on the spots where the sand is moist.

117
00:19:42,888 --> 00:19:46,383
Red car. Ford.

118
00:19:48,588 --> 00:19:52,698
<i>What type. Ask him what type the car was.
-Later.</i>

119
00:19:56,918 --> 00:20:03,576
... the boy has woken up.
The nurse says he'll make it.

120
00:20:06,902 --> 00:20:09,896
... yes he has. A red Ford.

121
00:20:12,536 --> 00:20:15,060
No, he shortly felt asleep.

122
00:20:21,401 --> 00:20:23,258
Shall I stay here?

123
00:20:34,440 --> 00:20:40,032
It's me. I just wanted to hear your voice.
Bye.

124
00:21:21,884 --> 00:21:24,152
For how long have you been staying here?

125
00:21:25,180 --> 00:21:26,902
Why didn't you call me?

126
00:21:28,065 --> 00:21:34,059
You wanted to hear my voice
and I wanted to see you.

127
00:21:36,934 --> 00:21:38,812
Let's go away

128
00:21:39,744 --> 00:21:41,058
Where.

129
00:21:44,195 --> 00:21:47,714
Simply go away. Maybe to an island.

130
00:21:51,084 --> 00:21:55,163
For how long?
- For as long as you want.

131
00:22:04,083 --> 00:22:05,695
Let's get married.

132
00:22:09,687 --> 00:22:11,255
Has something happened?

133
00:22:26,616 --> 00:22:29,212
<i>I can't believe it.</i>

134
00:22:31,854 --> 00:22:34,934
I'm sorry. And when should it happen?

135
00:22:38,761 --> 00:22:41,661
Philipp, could you come for a moment?

136
00:22:48,256 --> 00:22:50,766
Could you please come ...

137
00:22:54,868 --> 00:22:57,127
<i>Why did you do that?</i>

138
00:23:01,918 --> 00:23:03,945
What for?

139
00:23:04,871 --> 00:23:10,257
Do you want a part of the car dealership?
Don't you earn enough money?

140
00:23:10,470 --> 00:23:16,358
Is the commission too low?
Is it the position?

141
00:23:16,943 --> 00:23:20,747
You don't even love her.
You only take advantage of her.

142
00:23:21,043 --> 00:23:24,669
Right. And when the situation becomes dire,
you simply bugger off.

143
00:23:25,719 --> 00:23:28,294
Listen to me! Did you understand it?

144
00:23:30,255 --> 00:23:31,550
Francoise! Philipp, may I introduce to you...

145
00:23:32,842 --> 00:23:36,596
Francoise.
Philipp, sales manager and future brother-in-law.

146
00:23:38,058 --> 00:23:41,202
Enchanted. I have to go.

147
00:23:57,484 --> 00:24:00,135
You look fine.

148
00:24:04,417 --> 00:24:06,654
Why didn't you let someone
from the garage do it?

149
00:24:08,067 --> 00:24:10,977
I think I wanted to prove to myself
that I'm still able to do it.

150
00:24:11,050 --> 00:24:16,035
And are you?
- Yes, I believe ...

151
00:24:19,896 --> 00:24:22,378
I thought it was all about the
apex seal of the rotary piston.

152
00:24:23,418 --> 00:24:26,866
You don't get very far ....

153
00:24:30,186 --> 00:24:34,930
Could you prepare a bath?
- I could use one too.

154
00:26:22,777 --> 00:26:23,291
Excuse me

155
00:26:26,462 --> 00:26:29,090
I'm leaving shortly to bring a few things for Paul.
- Allright.

156
00:26:31,343 --> 00:26:34,843
Could you call me a cab?
- The taxi rank is next to the hospital.

157
00:26:38,522 --> 00:26:42,001
If he wakes up, tell him I'll be right back.
- Of course.

158
00:27:54,655 --> 00:27:57,077
<i>I tried to call you.</i>

159
00:27:57,260 --> 00:28:00,094
<i>I only reached the voice mail.</i>

160
00:28:04,204 --> 00:28:05,879
The battery is empty.

161
00:28:07,762 --> 00:28:09,646
<i>I'm so sorry ...</i>

162
00:28:16,508 --> 00:28:19,266
<i>The gentle swell rinses the bare, gleaming cliffs,</i>

163
00:28:19,332 --> 00:28:22,422
<i> and the mild wind smoothly strikes the skin.</i>

164
00:28:22,937 --> 00:28:25,709
<i>A sunset on Cuba is short, 
but so intense and breathtaking,</i>

165
00:28:25,906 --> 00:28:30,398
<i>that one lingers in the darkness,
oddly relaxed, almost overwhelmed.</i>

166
00:28:31,190 --> 00:28:33,520
That's not a travel guide, that's sex.

167
00:28:34,496 --> 00:28:35,891
Can I see?

168
00:28:47,264 --> 00:28:49,464
That's very beautiful.

169
00:29:22,826 --> 00:29:26,567
<i>When you cannot bear the suffering caused
by losing a beloved person</i>

170
00:29:27,765 --> 00:29:29,967
you may want to depart this life too

171
00:29:31,104 --> 00:29:32,388
<i>There are people who jump from a bridge,</i>

172
00:29:32,882 --> 00:29:36,807
<i>people who throw themselves in front of a car
or of a train, people who plunge into water.</i>

173
00:29:37,514 --> 00:29:40,239
<i>They want to disappear and follow
their beloved into the other world.</i>

174
00:29:41,270 --> 00:29:45,457
But someone like you, who stabs himself four times
with a knife doesn't want to follow anyone.

175
00:29:45,738 --> 00:29:48,067
He wants to punish himself.

176
00:29:48,065 --> 00:29:50,850
You feel guilty and we want to relieve you
from this sense of guilt.

177
00:29:52,400 --> 00:29:54,576
Does jumping into water mean
that you're free of any guilt?

178
00:29:55,723 --> 00:29:59,427
<i>Laura, I have talked to all of them: to his
colleagues, to their mothers, to the teachers,</i>

179
00:29:59,769 --> 00:30:04,311
<i>you are a great mother and don't have
any reason to feel guilty.</i>

180
00:30:11,895 --> 00:30:16,430
On 11. 3. 1996 you are at the councelling center
of the Departement of Social Services

181
00:30:16,693 --> 00:30:19,062
and you want to give your son up for adoption.

182
00:30:19,444 --> 00:30:21,907
He is currently six month old, you specify that
you're unable to mamage it anymore,

183
00:30:22,017 --> 00:30:24,690
you are a single mother and
have no perspective.

184
00:31:02,084 --> 00:31:03,854
Laura, you know well you
can take everything you want.

185
00:31:05,180 --> 00:31:07,014
If you need another blanket,
you can find one in the closet.

186
00:31:07,767 --> 00:31:09,058
Thanks.

187
00:31:18,066 --> 00:31:19,765
Sleep well.

188
00:31:23,214 --> 00:31:25,941
We'll find that bastard.

189
00:31:26,050 --> 00:31:28,099
Sure!

190
00:31:51,462 --> 00:31:52,897
Vera!

191
00:31:57,021 --> 00:31:57,889
Vera!

192
00:32:03,787 --> 00:32:04,722
Vera!

193
00:32:12,101 --> 00:32:14,056
Can I sleep here?

194
00:32:13,716 --> 00:32:14,779
Of course.

195
00:32:45,593 --> 00:32:49,767
A clown, bears, a bottle, a dog,

196
00:32:50,619 --> 00:32:53,669
a glove, a dice, a car,

197
00:32:54,360 --> 00:32:56,725
a frog, another car.

198
00:33:02,258 --> 00:33:03,486
I am here!

199
00:33:12,356 --> 00:33:13,774
And whereto now?

200
00:33:16,440 --> 00:33:18,243
To the sea.

201
00:33:18,925 --> 00:33:20,607
Where's the sea?

202
00:33:22,419 --> 00:33:25,090
I'ts a long, long way ... you have to drive
through five cities,

203
00:33:25,622 --> 00:33:28,501
through three forests.

204
00:33:30,214 --> 00:33:32,718
<i>Shall I bring for you something from there?</i>

205
00:33:33,141 --> 00:33:35,004
A starfish.

206
00:33:38,722 --> 00:33:41,321
And bring one for Laura too.
- I will.

207
00:34:26,381 --> 00:34:28,572
Here it is.

208
00:34:30,032 --> 00:34:31,802
Where you've this one?
- It lay there.

209
00:34:33,238 --> 00:34:36,562
Totally broken.
- We'll get it right.

210
00:34:37,069 --> 00:34:38,951
No you can't, forget it.

211
00:34:39,496 --> 00:34:42,610
Was it in an accident?

212
00:34:43,163 --> 00:34:45,336
Looks like.

213
00:34:45,688 --> 00:34:48,373
But you're looking for a Ford.
This one is an NSU Ro 80.

214
00:34:49,265 --> 00:34:51,801
Throw it back from where
you took it.

215
00:35:05,910 --> 00:35:08,190
Do you think it helps if you'll find him?

216
00:35:15,563 --> 00:35:17,123
Probably not.

217
00:35:26,200 --> 00:35:28,212
Have to go again to work.

218
00:35:30,599 --> 00:35:33,356
Have to take care of Antonia,
cannot take her for weeks with me.

219
00:35:42,440 --> 00:35:45,365
I'll go again home.

220
00:35:48,675 --> 00:35:51,067
That's not what I've meant.
- I know.

221
00:36:11,018 --> 00:36:12,479
That's silly.

222
00:36:14,729 --> 00:36:17,011
I think it's cute.

223
00:37:07,262 --> 00:37:08,568
Phillip!?

224
00:37:40,242 --> 00:37:42,405
<b> Urgent: Seeking witnesses ....</b>

225
00:38:24,026 --> 00:38:26,049
Something has happened ...

226
00:38:27,167 --> 00:38:29,820
<i>it dates back before our
voyage to Cuba.</i>

227
00:38:30,807 --> 00:38:33,562
<i>I thought I've gotten over it ...</i>

228
00:38:33,600 --> 00:38:35,771
Did you wait until
I lay in the tub?

229
00:38:37,481 --> 00:38:38,802
What?

230
00:38:38,857 --> 00:38:42,161
I've read that most men break it off
when their girlfriend is lying in the bathtub.

231
00:38:42,328 --> 00:38:45,605
You cannot answer back as you're naked,
wet and defenseless.

232
00:38:45,796 --> 00:38:51,958
You remain in the bathtub and all seems
more calm, more tolerable, easier.

233
00:38:54,279 --> 00:38:56,998
But it doesn't have anything to do with that.

234
00:38:57,479 --> 00:39:01,510
Philipp, I don't want any confessions.
I don't want to be a rubbish dump.

235
00:39:09,866 --> 00:39:11,467
I'm not interested in what happened.

236
00:39:38,890 --> 00:39:41,111
<i>I'll let some air in.</i>

237
00:40:25,231 --> 00:40:27,944
I think you should start working again.

238
00:40:35,154 --> 00:40:37,164
Believe me, it's better this way.

239
00:41:03,701 --> 00:41:05,329
A great car!

240
00:41:05,282 --> 00:41:08,489
New T�V vehicle inspection certificate,
84.000 km, ATP

241
00:41:09,692 --> 00:41:13,858
moonroof, 4 doors, shipshape
except some small injuries,

242
00:41:14,098 --> 00:41:17,747
What exactly?
- Back bumper needs some bending, some painting..

243
00:41:19,155 --> 00:41:22,122
And the car fender?
- Is all right.

244
00:41:22,777 --> 00:41:26,468
Has it ever been replaced?
- Put your hand inside it.

245
00:41:34,847 --> 00:41:36,879
What do you feel?

246
00:41:38,889 --> 00:41:40,698
It's quite rough.
- You see ...

247
00:41:41,040 --> 00:41:44,949
It's the underbody protection. Had it been
replaced, the surface would have been even.

248
00:41:48,508 --> 00:41:51,110
It's yours for 1200, guarantees included.

249
00:41:52,134 --> 00:41:54,066
Thank you very much.

250
00:41:56,517 --> 00:41:58,298
1100, maybe?

251
00:42:02,891 --> 00:42:06,277
<i>Red, red, red, red, red, red, red ...
- Again</i>

252
00:42:06,847 --> 00:42:11,251
<i>Again? Red, red, red, red, red, red, red ...</i>

253
00:42:13,090 --> 00:42:14,744
<i>What do vampyres drink?
- Red.</i>

254
00:42:15,011 --> 00:42:16,749
<i>What do you donate to the red cross?
- Red.</i>

255
00:42:17,081 --> 00:42:18,501
<i>When are you allowed to cross the street?
- Red.</i>

256
00:42:21,538 --> 00:42:24,763
So you cross the street when the red light is on?
- Oh, shit!

257
00:42:26,794 --> 00:42:28,735
At this moment I would have
sold you a car, Fran�oise.

258
00:42:50,494 --> 00:42:53,915
What did she want?
- She's looking for some red Ford.

259
00:43:05,766 --> 00:43:08,584
Do you come in?
- I'll come in a moment.

260
00:43:10,422 --> 00:43:12,437
Do you have one for me?

261
00:43:21,975 --> 00:43:23,368
What's wrong with you?

262
00:43:25,866 --> 00:43:28,031
I'll come in soon.

263
00:43:29,668 --> 00:43:31,603
I want to go home.

264
00:43:32,808 --> 00:43:34,824
Let's drive.

265
00:43:50,680 --> 00:43:54,108
I only wanted to say that
I've kept your job ...

266
00:43:55,416 --> 00:43:59,363
and I thought that maybe you'll like
to supervise the shelf maintenance.

267
00:44:01,250 --> 00:44:05,545
That's a good position. I believe you're
the right person for this.

268
00:44:09,453 --> 00:44:11,810
I'll  be very glad if
you'd come back.

269
00:44:18,055 --> 00:44:21,542
January is evident. We'll take
the picture from the Web-catalogue.

270
00:44:22,349 --> 00:44:25,539
For february we'll do something
completely different.

271
00:44:26,444 --> 00:44:31,184
Simple, clear, still life.

272
00:44:34,505 --> 00:44:37,802
I find it great.
- Yeah. Continue!

273
00:44:39,320 --> 00:44:41,736
For march I thought we'll
go right into the summer.

274
00:44:42,642 --> 00:44:45,957
Sun, wind, cabriolet,
but no clumsy Cote d'Azur.

275
00:44:46,887 --> 00:44:52,540
With a woman's close-up.  Her hair in the
wind, a fresh face, say goodbye to the winter!

276
00:44:55,618 --> 00:44:58,410
What's wrong with you?
Don't you like it?

277
00:44:59,436 --> 00:45:02,017
I'll be right back.

278
00:45:04,220 --> 00:45:06,733
<i>Did you take dance lessons?
- No.</i>

279
00:45:09,195 --> 00:45:12,977
<i>... and Argentinian tango ...</i>

280
00:45:13,419 --> 00:45:16,416
<i>Of course! Anyone can dance tango.</i>

281
00:45:29,437 --> 00:45:32,318
I can't do it!

282
00:45:40,460 --> 00:45:42,103
I can drive you home.

283
00:45:43,383 --> 00:45:46,436
The bycicle goes into the trunk.
I can flip over the back seat.

284
00:48:42,014 --> 00:48:44,318
I'll drive you to a hospital.

285
00:48:44,990 --> 00:48:47,455
No. no hospital.

286
00:49:02,098 --> 00:49:04,360
I'll drive you home.

287
00:52:04,052 --> 00:52:04,935
<b>Urgent: Seeking witnesses!</b>

288
00:52:04,711 --> 00:52:06,811
<i>What are you doing?</i>

289
00:52:11,259 --> 00:52:13,290
I was looking for some dry things.

290
00:52:19,720 --> 00:52:22,724
You've fainted. I've driven you home.

291
00:52:29,854 --> 00:52:31,759
<i>I made some hot tea.</i>

292
00:52:38,465 --> 00:52:40,825
Please leave.

293
00:52:42,627 --> 00:52:44,955
Of course.

294
00:52:47,455 --> 00:52:51,463
Everything is all right. I only want to be alone.
- Good bye.

295
00:53:16,506 --> 00:53:18,175
Did I wake you up?

296
00:53:19,571 --> 00:53:21,561
I'm sorry.

297
00:53:23,276 --> 00:53:25,088
What's happened to you?

298
00:53:25,406 --> 00:53:26,801
A flat tyre.

299
00:53:28,352 --> 00:53:30,490
There's always something wrong with your cars.

300
00:53:35,249 --> 00:53:37,715
And, how's the calendar?
- Shitty.

301
00:53:39,564 --> 00:53:41,458
Then it was worth it.

302
00:54:12,407 --> 00:54:13,651
Good morning.

303
00:54:17,868 --> 00:54:21,346
I brought your bicycle. Didn't want
to leave it unlocked downstairs.

304
00:54:25,740 --> 00:54:29,313
Would you like a cup of coffee?
- Yes.

305
00:54:33,367 --> 00:54:35,065
You can leave that there,
noone's going to steal it

306
00:54:40,707 --> 00:54:42,929
Good coffee.

307
00:54:49,991 --> 00:54:52,314
I'm glad that nothing bad has happened
to you last night.

308
00:54:57,319 --> 00:54:59,524
I'm glad too.

309
00:55:08,485 --> 00:55:11,591
Do you know what BDS stands for?

310
00:55:17,100 --> 00:55:19,239
Brigade of the dim-witted.

311
00:55:20,666 --> 00:55:23,359
And what does the S stand for?

312
00:55:26,543 --> 00:55:29,953
I have to go.
- I'll drive you.

313
00:55:46,085 --> 00:55:47,898
I'd like to get off here.

314
00:56:14,553 --> 00:56:16,050
Thank you.

315
00:56:17,226 --> 00:56:21,141
When do you finish work?
- At 7. Why?

316
00:56:23,049 --> 00:56:25,868
I'd like to invite you to dinner,
if that's all right to you.

317
00:56:26,698 --> 00:56:29,747
I could come by at 8.

318
00:58:11,156 --> 00:58:13,316
I'm sorry, but I'd rather not go out.

319
00:58:23,246 --> 00:58:26,362
You polished your fingernails.

320
00:58:28,980 --> 00:58:31,281
Right.

321
00:58:37,272 --> 00:58:39,598
How did you enter
the brigade of the dim-witted?

322
00:58:42,968 --> 00:58:44,976
I's a job.

323
00:58:47,013 --> 00:58:49,432
What job did you learn to do?

324
00:58:55,421 --> 00:58:56,943
Typography.

325
00:59:01,702 --> 00:59:03,431
And why don't you work
as a typographer?

326
00:59:08,065 --> 00:59:10,527
There's hardly any work
for a typographer.

327
00:59:18,704 --> 00:59:20,920
For how long haven't you
worked this?

328
00:59:25,457 --> 00:59:27,695
For eight years.

329
00:59:33,205 --> 00:59:37,081
Could you drive me home?
- Sure.

330
00:59:43,318 --> 00:59:47,346
I could find you a job. A good one.

331
00:59:49,812 --> 00:59:52,521
I'd like to help you.

332
00:59:57,324 --> 00:59:59,970
I'd really like to go home.

333
01:00:05,462 --> 01:00:08,992
Nice to see you!
What are you doing here?

334
01:00:10,933 --> 01:00:12,880
Business ... discussions.

335
01:00:13,681 --> 01:00:16,381
I have to ... have a nice evening.

336
01:00:46,537 --> 01:00:48,965
Do you want to drive along for a while?

337
01:00:51,079 --> 01:00:53,242
Simply drive.

338
01:02:11,604 --> 01:02:13,356
Good morning.

339
01:02:15,562 --> 01:02:17,881
I got us breakfast.

340
01:02:19,162 --> 01:02:21,211
Would you like some coffee?

341
01:02:28,513 --> 01:02:31,498
Where are we?
- Near Braunschweig.

342
01:02:41,114 --> 01:02:44,344
What's the time?
- One second ...

343
01:02:48,094 --> 01:02:51,121
Almost 8.
- Shit! I have to go to work!

344
01:02:51,898 --> 01:02:54,055
We'll eat as we drive.

345
01:03:02,941 --> 01:03:05,329
I can't go like this to work.

346
01:03:08,480 --> 01:03:10,869
Then we'll first drive to your place.

347
01:03:18,436 --> 01:03:21,949
In case you're hungry ...
there are some sandwiches in the bag.

348
01:03:29,041 --> 01:03:31,570
They only had chicken.

349
01:03:32,313 --> 01:03:34,605
I hope you like it.

350
01:03:41,506 --> 01:03:43,564
No, thanks, I'm not hungry.

351
01:03:52,598 --> 01:03:54,706
Does it taste good?

352
01:04:23,653 --> 01:04:27,400
How late is it now?
- Shortly after half past.

353
01:05:04,456 --> 01:05:05,717
I have to hurry.

354
01:05:06,036 --> 01:05:09,047
When can I see you again?
- I don't know.

355
01:05:10,387 --> 01:05:12,029
I have to go.

356
01:05:39,020 --> 01:05:40,308
A little late, huh?

357
01:05:42,118 --> 01:05:44,210
I felt asleep in the car.

358
01:05:52,398 --> 01:05:54,764
I've lost a shoe.

359
01:05:57,262 --> 01:06:00,948
Laura, could you please come to my office.
Now.

360
01:06:11,685 --> 01:06:13,414
I was only 20 minutes late.

361
01:06:17,254 --> 01:06:20,426
It's not going to happen again, really.
- No, Laura.

362
01:06:21,296 --> 01:06:23,432
Oh, please.

363
01:06:24,971 --> 01:06:27,432
<i>It's too late.</i>

364
01:06:30,709 --> 01:06:33,053
I'll get it all under control, really.

365
01:06:34,838 --> 01:06:37,130
Yes, I've seen that.

366
01:06:39,258 --> 01:06:41,198
So that's really ...

367
01:06:44,992 --> 01:06:46,661
Please leave now.

368
01:06:50,381 --> 01:06:54,309
And what are the deduction from salary for?
Because I didn't want to kiss you?

369
01:07:01,145 --> 01:07:05,203
We've made an error and paid you
family allowance for 3 extra months.

370
01:07:13,881 --> 01:07:16,148
I can't employ a typographer.
Noone can pay for it.

371
01:07:16,341 --> 01:07:18,651
You'd do me a great favor, that's all.

372
01:07:18,811 --> 01:07:22,127
And the social security contributions,
the protection against dismissal and all that?

373
01:07:22,293 --> 01:07:26,734
I'll pay all that to you.
- Philipp, you're a friend, I'm not happy to take money from you.

374
01:07:26,963 --> 01:07:29,133
Simply tell me how much.

375
01:07:29,368 --> 01:07:32,894
Is all this a coming to terms with the past
or investment in the future?

376
01:07:32,954 --> 01:07:36,695
How much?
- She's beautiful ....?

377
01:07:37,891 --> 01:07:40,753
Please, simply tell me how much.

378
01:07:41,299 --> 01:07:44,940
30.000
- 30.000? For sure, for one year.

379
01:07:45,360 --> 01:07:48,459
Shall I transfer it to a company?
- Cash. You know, the books ...

380
01:07:54,071 --> 01:07:57,810
But she won't know anything, right?
- You look exhausted.

381
01:08:40,289 --> 01:08:42,234
Switch it off!

382
01:08:53,204 --> 01:08:55,281
It was a business dinner.

383
01:08:56,269 --> 01:09:00,552
She's a graphic artist from Oliver's firm
and we discussed some details about the calendar.

384
01:09:03,068 --> 01:09:05,094
Did she drop this detail in your car?

385
01:09:08,016 --> 01:09:10,162
Who is she?

386
01:09:15,161 --> 01:09:17,116
Cinderella.

387
01:09:21,529 --> 01:09:23,511
You're a dirty pig.

388
01:09:26,711 --> 01:09:30,838
I met her before Cuba.
I thought it was nothing serious.

389
01:09:36,541 --> 01:09:38,896
I'm thinking of her night and day.

390
01:10:26,255 --> 01:10:27,441
<i>Philipp!</i>

391
01:10:34,105 --> 01:10:36,608
Klaus would like the car keys back.

392
01:11:44,553 --> 01:11:49,543
... At Audi in Wolfsburg and before that
at BMW in Forchheim.

393
01:11:52,523 --> 01:11:55,936
We can discuss it then.
I'll call you when I arrive.

394
01:11:57,377 --> 01:12:00,160
To you too. Thank you. Good bye.

395
01:14:10,364 --> 01:14:12,709
Good morning.

396
01:14:13,909 --> 01:14:16,416
Did I wake you up?

397
01:14:18,931 --> 01:14:20,699
I'm sorry.

398
01:14:26,031 --> 01:14:29,883
You forgot your shoe in the car.
- Right.

399
01:14:36,197 --> 01:14:38,121
Would you like some coffee?

400
01:14:48,989 --> 01:14:50,884
For how long are you still on probation?

401
01:14:51,511 --> 01:14:53,417
For three weeks.

402
01:14:54,327 --> 01:14:56,645
You'll make it.

403
01:14:58,331 --> 01:15:03,039
I still have to get acquainted with all the software,
but I think it will work out all right.

404
01:15:05,616 --> 01:15:08,158
I would have thanked you ....
- What for?

405
01:15:08,614 --> 01:15:11,144
... but I didn't have your address.

406
01:15:11,655 --> 01:15:15,782
And then I thought there ought to have been
a reason for you not giving it to me.

407
01:15:17,907 --> 01:15:20,603
I've just split up with my wife.

408
01:15:24,961 --> 01:15:27,924
Actually I've come to say good-bye.
I'll leave.

409
01:15:29,179 --> 01:15:32,596
And your work?
- I've handed my notice.

410
01:15:32,994 --> 01:15:36,837
But that's terrible!
- No. Not really.

411
01:15:40,114 --> 01:15:42,479
And where do you want to go to?

412
01:15:42,959 --> 01:15:48,420
To Forchheim. My parents live there.
I also have an offer nearby.

413
01:15:50,280 --> 01:15:52,163
Is that far away?

414
01:15:53,325 --> 01:15:55,243
It's close to N�rnberg.

415
01:16:00,171 --> 01:16:02,639
I'll go then ...
- What's the time?

416
01:16:03,919 --> 01:16:08,721
Three quarters past eight.
- Damn it, how can I ... I'm late again.

417
01:16:10,080 --> 01:16:11,377
I can drive you.

418
01:16:24,185 --> 01:16:26,181
What's wrong?
- No idea.

419
01:16:32,504 --> 01:16:34,181
Also closed.

420
01:16:38,539 --> 01:16:39,837
Holiday.

421
01:16:51,340 --> 01:16:53,635
Would you like to drive somewhere?

422
01:16:55,347 --> 01:16:57,365
Don't you have to leave?

423
01:16:59,122 --> 01:17:00,458
There's time enough.

424
01:18:28,839 --> 01:18:30,238
Laura ...

425
01:22:18,231 --> 01:22:20,723
You did it.

426
01:24:28,668 --> 01:24:30,948
There's been an accident.

427
01:24:32,427 --> 01:24:34,594
Someone severely injured.

428
01:24:39,319 --> 01:24:41,347
Where are we?

429
01:24:43,237 --> 01:24:46,875
On the B 248

430
01:24:48,630 --> 01:24:50,408
close to Salzwedel.

431
01:24:57,191 --> 01:25:01,420
B 248, close to Salzwedel.

