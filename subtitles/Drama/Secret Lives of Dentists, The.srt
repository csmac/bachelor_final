1
00:00:58,002 --> 00:01:01,003
I bought some things Tiffany will need.

2
00:01:01,901 --> 00:01:04,767
They're things she'll need right away.

3
00:01:06,267 --> 00:01:09,634
I bought clothes a size bigger, too,

4
00:01:09,634 --> 00:01:12,100
so she can wear them next year.

5
00:01:13,267 --> 00:01:16,300
Tiffany is prone to colds,

6
00:01:16,003 --> 00:01:19,404
so be sure to put a jacket on her in April.

7
00:01:35,667 --> 00:01:37,671
You're flying out tomorrow, right?

8
00:01:40,004 --> 00:01:41,938
Before you go,

9
00:01:42,334 --> 00:01:45,601
can you let me see her one more time?

10
00:01:50,467 --> 00:01:53,634
I'll get going now.

11
00:01:54,901 --> 00:01:56,033
Areum.

12
00:02:17,801 --> 00:02:20,834
You raise Tiffany.

13
00:02:23,767 --> 00:02:25,801
Really?

14
00:02:25,801 --> 00:02:27,567
Do you mean it?

15
00:02:28,067 --> 00:02:29,073
Tiffany...

16
00:02:30,004 --> 00:02:32,071
I can really raise her?

17
00:02:34,000 --> 00:02:37,133
Go before I change my mind.

18
00:02:38,667 --> 00:02:39,671
Thank you.

19
00:02:41,467 --> 00:02:43,400
Thank you!

20
00:03:13,901 --> 00:03:15,400
What happened?

21
00:03:15,767 --> 00:03:17,601
Phillip...

22
00:03:17,601 --> 00:03:19,604
Phillip said I could take her.

23
00:03:19,901 --> 00:03:20,908
He said I could raise her.

24
00:03:21,601 --> 00:03:22,500
What?

25
00:03:25,334 --> 00:03:26,338
Really?

26
00:03:27,767 --> 00:03:29,776
Tiffany, come here.

27
00:03:30,667 --> 00:03:31,676
How did this happen?

28
00:03:33,601 --> 00:03:35,000
It feels like a dream.

29
00:03:35,734 --> 00:03:37,767
That I...

30
00:03:37,767 --> 00:03:40,734
That I can raise my little girl.

31
00:03:40,734 --> 00:03:42,267
I can't believe it.

32
00:03:43,033 --> 00:03:44,034
Is this for real?

33
00:03:46,868 --> 00:03:48,934
This is great. I'm so happy.

34
00:03:48,934 --> 00:03:51,300
This is wonderful.

35
00:03:51,003 --> 00:03:52,870
Tiffany...

36
00:03:54,003 --> 00:03:55,337
Consent Form to Relinquish Parental Custody Rights

37
00:03:58,534 --> 00:04:01,267
I know the way you love Tiffany

38
00:04:01,267 --> 00:04:03,000
is different from mine,

39
00:04:03,667 --> 00:04:05,801
but I know that it's real.

40
00:04:07,001 --> 00:04:08,934
That's why you underwent

41
00:04:09,033 --> 00:04:11,167
this surgery despite complications.

42
00:04:13,534 --> 00:04:14,767
Thank you.

43
00:04:15,634 --> 00:04:20,500
You did for Tiffany what I couldn't.

44
00:04:21,968 --> 00:04:23,100
I...

45
00:04:23,901 --> 00:04:27,334
I don't regret having met you.

46
00:04:28,467 --> 00:04:30,467
Without you,

47
00:04:30,467 --> 00:04:33,467
I wouldn't have Tiffany.

48
00:04:36,067 --> 00:04:38,634
She is not only my daughter,

49
00:04:38,634 --> 00:04:40,701
but yours, too.

50
00:04:42,567 --> 00:04:46,334
I've been pushing you away too much.

51
00:04:47,801 --> 00:04:51,802
I'm really sorry.

52
00:04:53,901 --> 00:04:59,234
I acknowledge the fact that

53
00:04:59,234 --> 00:05:00,667
you are Tiffany's dad.

54
00:05:01,434 --> 00:05:03,467
So...

55
00:05:05,167 --> 00:05:06,701
Please wake up soon.

56
00:05:07,667 --> 00:05:10,634
I'm sure that's what Tiffany wants, too.

57
00:05:29,868 --> 00:05:32,467
Myeonghwa, no matter

58
00:05:32,467 --> 00:05:34,834
where Tiffany goes, she'll be fine.

59
00:05:34,834 --> 00:05:36,968
So don't worry so much, okay?

60
00:05:36,968 --> 00:05:37,667
Of course.

61
00:05:37,667 --> 00:05:41,133
And America isn't as far as you think.

62
00:05:41,133 --> 00:05:44,400
This goodbye isn't forever, so don't be sad.

63
00:05:45,002 --> 00:05:46,010
Myeonghwa.

64
00:05:47,000 --> 00:05:49,005
If you're tired, you should go to your room.

65
00:05:49,005 --> 00:05:51,339
Yeah, you must be worn out from crying.

66
00:05:51,834 --> 00:05:54,767
Let's go in and lie down.

67
00:05:57,467 --> 00:05:58,901
Areum!

68
00:06:03,601 --> 00:06:05,000
I'm back.

69
00:06:05,634 --> 00:06:08,133
Areum, what happened?

70
00:06:08,133 --> 00:06:09,667
Why is Tiffany...

71
00:06:09,667 --> 00:06:12,934
You couldn't go to Phillip and just came back?

72
00:06:13,567 --> 00:06:16,400
It's for the better. Good going.

73
00:06:17,167 --> 00:06:18,901
That's not it, mom.

74
00:06:20,868 --> 00:06:22,467
Phillip...

75
00:06:22,467 --> 00:06:24,534
Phillip told me to raise Tiffany.

76
00:06:26,734 --> 00:06:28,741
Are you... Are you serious?

77
00:06:29,934 --> 00:06:31,601
Phillip said that?

78
00:06:31,601 --> 00:06:35,767
Then, Phillip changed his mind?

79
00:06:35,767 --> 00:06:37,033
Yes, dad.

80
00:06:39,767 --> 00:06:43,200
Tiffany, come to grandma!

81
00:06:43,002 --> 00:06:45,403
Oh, my darling Tiffany.

82
00:06:46,601 --> 00:06:49,300
Thank you so much for coming back to us.

83
00:06:49,003 --> 00:06:52,937
Thank you, Tiffany!

84
00:06:54,004 --> 00:06:56,405
Thank you! Yes, yes.

85
00:06:58,005 --> 00:07:00,206
You're a sweetheart.

86
00:07:00,701 --> 00:07:04,200
They say life is full of ups and downs,

87
00:07:04,002 --> 00:07:05,011
and it must be true.

88
00:07:10,868 --> 00:07:12,667
What a pretty girl.

89
00:07:13,601 --> 00:07:15,604
What a pretty little girl.

90
00:07:16,767 --> 00:07:18,801
I sent Tiffany to Areum.

91
00:07:22,002 --> 00:07:25,036
You got her to sign the consent form!

92
00:07:26,133 --> 00:07:27,142
What are you saying?

93
00:07:28,033 --> 00:07:29,968
I tore that up.

94
00:07:29,968 --> 00:07:31,534
It means nothing now.

95
00:07:31,534 --> 00:07:32,801
You...

96
00:07:32,801 --> 00:07:35,067
I thought Tiffany would be happier

97
00:07:35,067 --> 00:07:37,033
being with Areum than being with me,

98
00:07:37,033 --> 00:07:38,634
so I decided to do that.

99
00:07:38,634 --> 00:07:42,067
Please respect my wishes, father.

100
00:07:42,067 --> 00:07:44,033
Oh, geez...

101
00:07:44,901 --> 00:07:46,534
Unbelievable...

102
00:07:46,534 --> 00:07:49,541
What about us going to the States?

103
00:07:51,601 --> 00:07:53,467
We'll go as planned.

104
00:07:53,467 --> 00:07:54,834
Let's leave tomorrow.

105
00:08:07,801 --> 00:08:10,367
It must've been a tough day for Tiffany,

106
00:08:10,367 --> 00:08:11,371
but she's still going strong.

107
00:08:11,767 --> 00:08:14,334
Yeah, my family must have worn her out

108
00:08:14,334 --> 00:08:15,343
from all the hugs and kisses,

109
00:08:16,234 --> 00:08:17,238
but she's got energy left.

110
00:08:17,634 --> 00:08:19,601
I can tell she's healthier.

111
00:08:19,601 --> 00:08:20,868
Yeah.

112
00:08:20,868 --> 00:08:24,467
It's been an eventful day for her, too...

113
00:08:26,367 --> 00:08:28,374
From now on, I'll make sure

114
00:08:29,067 --> 00:08:33,133
you never part with Tiffany again.

115
00:08:33,133 --> 00:08:34,801
No matter what happens,

116
00:08:34,801 --> 00:08:35,809
I'll protect the two of you.

117
00:08:37,534 --> 00:08:39,868
If you weren't there for me,

118
00:08:39,868 --> 00:08:44,033
I don't know how I would've gotten through it.

119
00:08:48,968 --> 00:08:51,701
Forget all the difficulties.

120
00:08:51,701 --> 00:08:54,300
Let's focus on being happy from now on.

121
00:08:54,003 --> 00:08:57,037
I'll forget the hardships of the past, too.

122
00:08:57,334 --> 00:08:58,667
From now on,

123
00:08:58,667 --> 00:09:01,300
I'll live only for you and Tiffany.

124
00:09:02,667 --> 00:09:05,334
I'll do that, too.

125
00:09:31,367 --> 00:09:32,368
Who is it?

126
00:09:38,434 --> 00:09:40,033
Are you going to answer it?

127
00:09:41,667 --> 00:09:43,334
There's still time.

128
00:09:43,334 --> 00:09:44,340
Go ahead.

129
00:09:44,934 --> 00:09:46,200
Okay.

130
00:09:46,002 --> 00:09:48,869
Don't disappear like last time.

131
00:09:49,834 --> 00:09:50,901
I won't.

132
00:10:01,868 --> 00:10:03,367
Where are you?

133
00:10:03,367 --> 00:10:04,968
I'm at the airport.

134
00:10:04,968 --> 00:10:06,400
I'm leaving soon.

135
00:10:06,004 --> 00:10:08,771
Can I see you for a second?

136
00:10:24,767 --> 00:10:26,868
You won't get to see Tiffany for a while,

137
00:10:26,868 --> 00:10:29,701
so I thought I'd bring her to see you off.

138
00:10:29,701 --> 00:10:31,767
No need to come all this way.

139
00:10:35,734 --> 00:10:37,400
Hold her.

140
00:10:55,801 --> 00:10:59,234
I'll keep you posted on how she's doing.

141
00:10:59,234 --> 00:11:00,200
And whenever you miss her,

142
00:11:00,002 --> 00:11:02,236
you can video chat with her.

143
00:11:03,868 --> 00:11:05,701
You and I will be too busy.

144
00:11:05,701 --> 00:11:09,000
Just send me some pictures.

145
00:11:09,868 --> 00:11:12,267
I'll want to see her growing up.

146
00:11:13,467 --> 00:11:14,634
Thank you.

147
00:11:18,234 --> 00:11:22,033
I'll raise her well enough for both of us.

148
00:11:23,334 --> 00:11:26,267
So that when you see her in the future,

149
00:11:26,267 --> 00:11:28,968
you'll be proud.

150
00:11:28,968 --> 00:11:32,601
I'll raise her to be a great person.

151
00:11:34,834 --> 00:11:35,839
Cheon Seongun...

152
00:11:37,167 --> 00:11:38,901
He seems like he'll make a good dad.

153
00:11:40,567 --> 00:11:43,100
When Tiffany was sick,

154
00:11:43,001 --> 00:11:45,802
he asked me to help save her.

155
00:11:45,901 --> 00:11:47,434
He even knelt down in front of me.

156
00:11:48,667 --> 00:11:50,534
With dedication like that,

157
00:11:50,534 --> 00:11:52,267
he deserves to be Tiffany's dad.

158
00:11:57,234 --> 00:11:59,968
I could never do that.

159
00:12:03,434 --> 00:12:05,436
I should get going.

160
00:12:10,003 --> 00:12:13,070
You won't be back for a while, right?

161
00:12:14,133 --> 00:12:15,601
Probably not.

162
00:12:16,801 --> 00:12:21,500
Then stay well, wherever you go.

163
00:12:21,005 --> 00:12:23,439
And I wish you and Suji happiness.

164
00:12:24,567 --> 00:12:26,400
Thanks.

165
00:12:26,004 --> 00:12:27,871
I wish you the same with Seongun.

166
00:12:43,004 --> 00:12:45,171
Seongun, there's a lot I should apologize for.

167
00:12:48,801 --> 00:12:51,300
I won't ask you to forgive me,

168
00:12:51,003 --> 00:12:55,237
but please take good care of those two.

169
00:13:00,467 --> 00:13:01,300
Take care.

170
00:13:19,734 --> 00:13:21,100
Tiffany.

171
00:13:21,001 --> 00:13:24,003
Say bye to daddy.

172
00:13:24,003 --> 00:13:25,270
Bye.

173
00:14:12,367 --> 00:14:14,370
Let's get out, ladies.

174
00:14:20,467 --> 00:14:21,470
Are you ready?

175
00:14:23,001 --> 00:14:24,934
Yes, I'm ready.

176
00:14:25,701 --> 00:14:27,534
Want something to calm your nerves?

177
00:14:27,534 --> 00:14:28,968
No, thanks. I'm fine.

178
00:14:31,001 --> 00:14:33,268
Let me have Tiffany.

179
00:14:33,367 --> 00:14:35,000
Here we go!

180
00:14:35,000 --> 00:14:36,567
Let's go inside.

181
00:14:40,334 --> 00:14:42,267
He said he'd come with Areum,

182
00:14:42,267 --> 00:14:44,276
but they're late. Should I call?

183
00:14:45,167 --> 00:14:48,173
No need. I'm sure they'll be here soon.

184
00:14:48,767 --> 00:14:50,000
President Cheon.

185
00:14:50,000 --> 00:14:52,701
You won't oppose Areum this time, right?

186
00:14:52,701 --> 00:14:54,567
What do you mean, oppose her?

187
00:14:54,567 --> 00:14:55,573
I promised Seongun that I'd accept her

188
00:14:56,167 --> 00:14:58,334
if the baby issue was taken care of.

189
00:15:00,901 --> 00:15:02,000
Seongun.

190
00:15:05,534 --> 00:15:07,667
I'm here, father.

191
00:15:07,667 --> 00:15:09,434
Hello, President Cheon.

192
00:15:09,434 --> 00:15:11,438
The baby...

193
00:15:11,834 --> 00:15:14,968
You said Phillip took her to the States.

194
00:15:14,968 --> 00:15:16,534
Well...

195
00:15:16,534 --> 00:15:19,167
Areum regained custody of her.

196
00:15:20,133 --> 00:15:22,567
Regained custody?

197
00:15:22,567 --> 00:15:25,534
Yes, Phillip gave up custody of Tiffany,

198
00:15:25,534 --> 00:15:29,467
so Areum can raise her again.

199
00:15:32,001 --> 00:15:33,034
That's a relief.

200
00:15:33,133 --> 00:15:34,968
That's great, Areum.

201
00:15:37,868 --> 00:15:40,133
President Cheon, let's sit down and talk.

202
00:15:40,133 --> 00:15:41,300
Have a seat.

203
00:15:41,003 --> 00:15:43,004
Don't you dare.

204
00:15:43,004 --> 00:15:44,438
I thought the child was taken care of,

205
00:15:44,834 --> 00:15:46,300
so I said to bring Areum over,

206
00:15:46,003 --> 00:15:48,304
and you bring the child, too?

207
00:15:48,601 --> 00:15:50,534
Are you kidding me right now?

208
00:15:52,667 --> 00:15:53,901
President Cheon.

209
00:15:53,901 --> 00:15:57,067
You might as well accept them now.

210
00:15:57,734 --> 00:15:59,667
Accept them?

211
00:16:01,000 --> 00:16:02,667
I don't even know anymore.

212
00:16:02,667 --> 00:16:03,672
You do whatever you want.

213
00:16:11,003 --> 00:16:13,637
I think he's taken aback.

214
00:16:13,934 --> 00:16:14,940
I'll talk to him later,

215
00:16:15,534 --> 00:16:17,133
so don't worry too much.

216
00:16:17,133 --> 00:16:19,701
Thank you.

217
00:16:19,701 --> 00:16:21,400
Go up to Seongun's room.

218
00:16:21,004 --> 00:16:23,538
I'll bring some fruit.

219
00:16:23,934 --> 00:16:25,367
Thank you.

220
00:16:25,367 --> 00:16:26,901
- Come on. / - Okay.

221
00:16:36,267 --> 00:16:37,271
Sir.

222
00:16:37,667 --> 00:16:38,671
You made Areum feel bad.

223
00:16:39,067 --> 00:16:41,072
How could you go in here like this?

224
00:16:41,567 --> 00:16:43,934
Is Seongun out of his mind?

225
00:16:43,934 --> 00:16:44,937
I made myself clear to him before,

226
00:16:45,234 --> 00:16:47,467
so he should get it right!

227
00:16:47,467 --> 00:16:48,901
Sir.

228
00:16:48,901 --> 00:16:52,033
I know you have a grudge against Phillip,

229
00:16:52,033 --> 00:16:55,034
but the baby did nothing wrong.

230
00:16:55,133 --> 00:16:58,267
And Seongun loves the baby so much.

231
00:17:02,267 --> 00:17:04,701
Mom, grandma's here.

232
00:17:04,701 --> 00:17:06,267
- She is? / - Yup.

233
00:17:06,267 --> 00:17:09,801
She's here with her landlord.

234
00:17:09,801 --> 00:17:11,367
Her landlord?

235
00:17:11,934 --> 00:17:13,567
Why would he come with her?

236
00:17:15,004 --> 00:17:17,171
Are you nervous?

237
00:17:17,567 --> 00:17:19,701
Of course. It's the first time I'm saying hello.

238
00:17:19,701 --> 00:17:21,701
You have no reason to be nervous.

239
00:17:21,701 --> 00:17:23,434
We're just here to inform them.

240
00:17:23,434 --> 00:17:25,467
We're not here to get permission.

241
00:17:25,467 --> 00:17:28,534
Then I'll just trust you on that.

242
00:17:32,267 --> 00:17:34,634
Mom, what are you doing here?

243
00:17:36,033 --> 00:17:37,767
Hello, nice to meet you.

244
00:17:37,767 --> 00:17:39,771
I'm Kim Seongcheol.

245
00:17:40,167 --> 00:17:41,534
Oh, hello.

246
00:17:41,534 --> 00:17:43,701
What brings you here...

247
00:17:43,701 --> 00:17:46,500
This won't be brief,

248
00:17:46,005 --> 00:17:48,439
so we should all sit down. You, too.

249
00:17:53,968 --> 00:17:56,767
Mother, what's going on?

250
00:17:57,567 --> 00:18:01,133
I came with Seongcheol to say something.

251
00:18:01,133 --> 00:18:02,968
To say something?

252
00:18:02,968 --> 00:18:04,367
Listen up.

253
00:18:05,567 --> 00:18:07,968
From now on, Seongcheol and I

254
00:18:07,968 --> 00:18:09,467
will begin a new chapter together.

255
00:18:09,467 --> 00:18:10,470
What?

256
00:18:10,767 --> 00:18:11,868
What...

257
00:18:12,801 --> 00:18:15,868
Mother, are you saying you'll

258
00:18:15,868 --> 00:18:17,801
marry this man or something?

259
00:18:17,801 --> 00:18:19,810
No, we won't go all out like that.

260
00:18:20,701 --> 00:18:22,702
We're just thinking about

261
00:18:22,801 --> 00:18:25,200
moving in together.

262
00:18:25,901 --> 00:18:26,901
What?

263
00:18:27,634 --> 00:18:31,767
But you seem around my age...

264
00:18:31,767 --> 00:18:35,434
And you'll be my father-in-law?

265
00:18:35,434 --> 00:18:37,436
I suppose so.

266
00:18:37,634 --> 00:18:41,067
If you want labels and such, I guess so.

267
00:18:41,968 --> 00:18:43,367
Grandma, unbelievable...

268
00:18:45,167 --> 00:18:47,171
No way. I won't allow it!

269
00:18:47,567 --> 00:18:49,033
Brat.

270
00:18:49,033 --> 00:18:51,868
I don't need your permission.

271
00:18:51,868 --> 00:18:54,801
I'm not asking you to buy us a house.

272
00:18:55,367 --> 00:18:57,901
We won't bother you, so don't worry.

273
00:18:59,367 --> 00:19:00,434
Sir!

274
00:19:01,033 --> 00:19:02,868
Sir, are you alright?

275
00:19:02,868 --> 00:19:05,100
- I don't even know anymore! / - Dohyeong.

276
00:19:05,001 --> 00:19:07,368
Everyone can move in together

277
00:19:07,467 --> 00:19:08,801
or get married or whatever!

278
00:19:08,801 --> 00:19:09,834
What?

279
00:19:12,567 --> 00:19:13,575
This will drive me to an early grave...

280
00:19:16,767 --> 00:19:17,834
We're so busy today.

281
00:19:17,834 --> 00:19:19,835
I feel bad leaving you alone like this.

282
00:19:19,934 --> 00:19:22,267
I asked my friends to come help,

283
00:19:22,267 --> 00:19:23,434
so don't worry and go ahead.

284
00:19:23,434 --> 00:19:26,133
Okay, just for today.

285
00:19:26,133 --> 00:19:27,467
I'm late.

286
00:19:27,467 --> 00:19:28,468
I'll be back. Sorry about this.

287
00:19:28,567 --> 00:19:29,576
Please congratulate them for me.

288
00:19:30,467 --> 00:19:32,200
Okay. Thanks!

289
00:19:33,667 --> 00:19:36,674
You're not in your hanbok yet?

290
00:19:39,000 --> 00:19:40,801
Oh, my gosh!

291
00:19:40,801 --> 00:19:42,100
Pretty, huh?

292
00:19:42,801 --> 00:19:47,067
You can't put on so much today.

293
00:19:47,067 --> 00:19:49,968
Why not? It's pretty.

294
00:19:49,968 --> 00:19:52,801
We're going for a reserved look today.

295
00:19:52,801 --> 00:19:53,868
That's the concept.

296
00:19:53,868 --> 00:19:56,000
Myeonghwa, hold on, okay?

297
00:19:56,000 --> 00:19:58,033
Let me wipe the lipstick off of you.

298
00:19:58,033 --> 00:20:01,500
I'll help you put it on again.

299
00:20:01,005 --> 00:20:03,439
I like being pretty.

300
00:20:03,934 --> 00:20:07,500
Eungmin might come today.

301
00:20:07,005 --> 00:20:08,439
Eungmin?

302
00:20:10,234 --> 00:20:12,868
Yeah, handsome Eungmin.

303
00:20:14,033 --> 00:20:16,267
I don't know if he's coming today.

304
00:20:16,267 --> 00:20:18,534
Myeonghwa, for now...

305
00:20:18,534 --> 00:20:20,267
Let me wipe this off.

306
00:20:20,267 --> 00:20:22,033
For now...

307
00:20:22,033 --> 00:20:24,667
Myeonghwa, say ah.

308
00:20:24,667 --> 00:20:26,100
Oh!

309
00:20:26,701 --> 00:20:29,868
Okay, then you do that. Hold still.

310
00:20:29,868 --> 00:20:31,133
Ah!

311
00:20:32,002 --> 00:20:34,969
Look at you being silly.

312
00:20:35,167 --> 00:20:36,171
You're so adorable.

313
00:20:36,567 --> 00:20:39,500
Hold still.

314
00:20:43,067 --> 00:20:45,500
You look pretty.

315
00:20:45,005 --> 00:20:48,106
We'll put on your hanbok, okay? And wipe this.

316
00:20:49,901 --> 00:20:52,567
I hope he comes today.

317
00:20:52,567 --> 00:20:54,000
Eungmin.

318
00:20:55,868 --> 00:20:57,834
Close your eyes.

319
00:21:04,667 --> 00:21:06,400
Who's this beauty?

320
00:21:06,004 --> 00:21:08,010
Are you an angel from heaven?

321
00:21:09,000 --> 00:21:11,934
I had no idea you could be so elegant.

322
00:21:11,934 --> 00:21:13,968
I like being pretty better.

323
00:21:14,834 --> 00:21:15,840
You do look pretty.

324
00:21:16,434 --> 00:21:19,000
He's right. You look great.

325
00:21:19,000 --> 00:21:20,467
Really?

326
00:21:20,467 --> 00:21:21,534
Really.

327
00:21:22,701 --> 00:21:23,667
You look pretty.

328
00:21:25,601 --> 00:21:28,467
Now that we're ready, let's get going.

329
00:21:30,267 --> 00:21:31,300
Honey.

330
00:21:34,701 --> 00:21:36,701
Seongho, you look great.

331
00:21:36,701 --> 00:21:39,834
Dad, why isn't Mr. Jang here yet?

332
00:21:39,834 --> 00:21:41,267
He's not here yet?

333
00:21:41,267 --> 00:21:42,934
I'll call him.

334
00:21:46,701 --> 00:21:50,100
Oh, geez.

335
00:21:50,001 --> 00:21:51,968
Why are you here so late?

336
00:21:52,067 --> 00:21:54,634
Sir, I'm sorry,

337
00:21:54,634 --> 00:21:57,133
but I don't think I can attend today.

338
00:21:57,133 --> 00:21:58,400
You can't? Why not?

339
00:21:58,004 --> 00:21:59,011
If I go there,

340
00:22:00,001 --> 00:22:01,568
I'll be a laughingstock.

341
00:22:01,667 --> 00:22:02,671
I can hear people's laughter

342
00:22:03,067 --> 00:22:05,200
ringing in my ears already.

343
00:22:05,002 --> 00:22:07,236
What are you saying?

344
00:22:07,434 --> 00:22:09,901
I was in such a rush

345
00:22:09,901 --> 00:22:12,133
that I didn't double-check my clothes...

346
00:22:20,002 --> 00:22:22,011
You are all kinds of trouble.

347
00:22:23,001 --> 00:22:24,735
I thought I told you to lose weight.

348
00:22:24,834 --> 00:22:26,033
I'm sorry, sir.

349
00:22:26,934 --> 00:22:28,968
Get him a pair of my dress pants.

350
00:22:28,968 --> 00:22:30,033
Okay.

351
00:22:33,004 --> 00:22:36,238
Please get the biggest size he has.

352
00:22:48,801 --> 00:22:49,868
You're here.

353
00:22:49,868 --> 00:22:51,033
You got here first.

354
00:22:51,033 --> 00:22:53,067
Haebang, watch your step.

355
00:22:55,601 --> 00:22:57,334
Your feet will hurt standing around.

356
00:22:57,334 --> 00:22:58,901
You should sit down inside.

357
00:22:58,901 --> 00:23:01,634
What took you so long? You said you'd be here.

358
00:23:01,634 --> 00:23:03,767
Don't even ask.

359
00:23:03,767 --> 00:23:07,100
Haebang's fans wouldn't let her go.

360
00:23:07,001 --> 00:23:09,135
We'll be late. Let's go in.

361
00:23:09,234 --> 00:23:12,236
Let's go. Careful.

362
00:23:13,003 --> 00:23:15,011
Bride, Han Areum

363
00:23:17,167 --> 00:23:18,168
Congratulations.

364
00:23:18,868 --> 00:23:21,834
Areum's going to get rich at this rate.

365
00:23:21,834 --> 00:23:23,767
Are you jealous that she's getting married?

366
00:23:23,767 --> 00:23:24,776
Of course.

367
00:23:26,003 --> 00:23:28,970
I wonder when we'll be hearing wedding bells.

368
00:23:29,267 --> 00:23:30,334
Just wait a bit longer.

369
00:23:30,334 --> 00:23:32,868
I'll marry you as soon as I can make money.

370
00:23:34,000 --> 00:23:35,004
You're too sweet.

371
00:23:36,167 --> 00:23:37,868
Here you are, Jinwu.

372
00:23:37,868 --> 00:23:39,234
Hi!

373
00:23:39,234 --> 00:23:40,601
Here's my gift.

374
00:23:41,003 --> 00:23:43,010
I'm giving it to the bride's side,

375
00:23:44,000 --> 00:23:46,000
since the groom's side is loaded.

376
00:23:46,000 --> 00:23:47,004
We should chip in where it's needed.

377
00:23:47,004 --> 00:23:47,012
Thank you.

378
00:23:48,002 --> 00:23:50,770
I hear you're doing really well for yourself.

379
00:23:50,968 --> 00:23:52,200
Jinwu told me.

380
00:23:52,002 --> 00:23:53,703
Thanks to Haebang,

381
00:23:53,901 --> 00:23:56,868
I feel like I've hit the second peak in life.

382
00:23:56,868 --> 00:23:58,801
Anyway, see you inside.

383
00:24:04,634 --> 00:24:07,067
He must be doing super well.

384
00:24:07,067 --> 00:24:09,100
He finally got himself a good singer.

385
00:24:10,634 --> 00:24:11,637
Wow.

386
00:24:11,934 --> 00:24:13,868
I hear the buffet here is amazing.

387
00:24:13,868 --> 00:24:16,634
I hear the steak will melt in your mouth.

388
00:24:16,634 --> 00:24:18,767
Really? Want to go eat right now?

389
00:24:18,767 --> 00:24:20,770
I didn't have breakfast, so I'm starving.

390
00:24:21,067 --> 00:24:24,033
We have to see the bride and groom enter.

391
00:24:24,033 --> 00:24:25,801
- Let's wait. / - Alright.

392
00:24:27,133 --> 00:24:29,300
Hey, kids! What are you doing on my turf?

393
00:24:29,003 --> 00:24:30,011
Wow! You look amazing!

394
00:24:31,001 --> 00:24:33,001
You look like a movie star.

395
00:24:33,001 --> 00:24:34,005
Amazing!

396
00:24:35,033 --> 00:24:38,334
Hey, I was born looking this good.

397
00:24:38,334 --> 00:24:39,968
Go hang out over there.

398
00:24:39,968 --> 00:24:41,033
I can't hang out.

399
00:24:41,033 --> 00:24:42,534
Okay, see you later.

400
00:24:42,534 --> 00:24:44,535
- Congratulations. / - Thanks.

401
00:24:44,634 --> 00:24:44,643
Let's go.

402
00:24:48,567 --> 00:24:49,968
Thank you.

403
00:24:49,968 --> 00:24:51,601
Seongun, congratulations.

404
00:24:52,367 --> 00:24:53,371
Father, you're here.

405
00:24:54,834 --> 00:24:55,843
Stop smiling so much.

406
00:24:57,734 --> 00:24:59,667
Congratulations, Seongun.

407
00:24:59,667 --> 00:25:00,667
Thank you.

408
00:25:03,667 --> 00:25:06,000
Yuni, what are you doing here?

409
00:25:07,267 --> 00:25:09,272
I heard from our friends.

410
00:25:09,767 --> 00:25:10,868
I'm hurt.

411
00:25:10,868 --> 00:25:13,534
How could you not invite me?

412
00:25:13,534 --> 00:25:14,467
Sorry.

413
00:25:15,004 --> 00:25:17,012
You're marrying Areum after all, huh?

414
00:25:18,002 --> 00:25:20,670
- Congratulations. / - Thanks.

415
00:25:20,868 --> 00:25:23,100
Life in Jeju-do must be good.

416
00:25:23,001 --> 00:25:24,702
You've gotten even prettier.

417
00:25:24,801 --> 00:25:26,868
The air is great there.

418
00:25:26,868 --> 00:25:29,067
Come visit me in Jeju-do with Areum.

419
00:25:29,067 --> 00:25:31,334
I'll show you a lot of great places.

420
00:25:31,334 --> 00:25:32,901
Okay, we'll go there.

421
00:25:33,434 --> 00:25:35,435
See you in the wedding hall.

422
00:25:35,534 --> 00:25:36,538
Don't get too nervous.

423
00:25:36,934 --> 00:25:38,367
See you in a bit.

424
00:25:49,133 --> 00:25:50,634
Thank you.

425
00:26:00,033 --> 00:26:03,100
Honey, why...

426
00:26:03,868 --> 00:26:07,000
Why is Shoe Man over there greeting people?

427
00:26:07,968 --> 00:26:11,367
Shoe Man is marrying Areum today.

428
00:26:11,367 --> 00:26:12,375
Oh, my.

429
00:26:13,167 --> 00:26:15,133
Then Shoe Man

430
00:26:15,133 --> 00:26:16,901
will be my son-in-law?

431
00:26:16,901 --> 00:26:20,234
Yes. You like that, don't you?

432
00:26:20,234 --> 00:26:23,167
You like Shoe Man.

433
00:26:23,167 --> 00:26:25,634
I do like him.

434
00:26:27,667 --> 00:26:29,673
Shoe Man got really lucky.

435
00:26:30,267 --> 00:26:32,400
He gets to marry Areum.

436
00:26:46,133 --> 00:26:50,100
Tiffany, you're all dressed up.

437
00:26:50,001 --> 00:26:51,968
Don't you look so pretty?

438
00:26:52,067 --> 00:26:54,834
What a pretty girl.

439
00:26:54,834 --> 00:26:57,200
You like that?

440
00:27:03,734 --> 00:27:05,500
Areum Han!

441
00:27:06,901 --> 00:27:07,908
Louis!

442
00:27:10,734 --> 00:27:11,739
- Congratulations, Areum. / - Congrats.

443
00:27:12,234 --> 00:27:14,400
Thank you so much for coming.

444
00:27:14,004 --> 00:27:16,010
Areum Han, you look really beautiful today.

445
00:27:17,000 --> 00:27:19,934
Her face is glowing! How radiant!

446
00:27:19,934 --> 00:27:23,033
This is how a blushing bride should be.

447
00:27:23,033 --> 00:27:23,934
You look amazing.

448
00:27:23,934 --> 00:27:25,067
I'll give you that.

449
00:27:25,067 --> 00:27:27,072
What brand is the wedding dress?

450
00:27:27,567 --> 00:27:28,572
That's not important.

451
00:27:29,067 --> 00:27:31,067
She'd make anything look luxurious.

452
00:27:31,067 --> 00:27:33,234
Yeah, I mean, who knew

453
00:27:33,234 --> 00:27:35,500
you'd marry the heir of Winners Group?

454
00:27:35,005 --> 00:27:35,013
Yeah.

455
00:27:36,003 --> 00:27:39,370
When did you two get so close?

456
00:27:39,667 --> 00:27:42,300
Let's hear that story another time.

457
00:27:42,003 --> 00:27:44,736
Why don't we take pictures first?

458
00:27:45,033 --> 00:27:46,467
Yes, pictures!

459
00:27:48,234 --> 00:27:50,133
Tiffany, let me hold you!

460
00:27:51,005 --> 00:27:52,010
Look pretty!

461
00:27:53,000 --> 00:27:55,934
One, two, three, cheese!

462
00:27:55,934 --> 00:27:58,701
Tiffany!

463
00:28:02,367 --> 00:28:04,934
We'll get going now. See you in a bit.

464
00:28:07,001 --> 00:28:08,368
How did you...

465
00:28:08,467 --> 00:28:10,701
We came to see Tiffany and everything.

466
00:28:10,701 --> 00:28:12,667
Congratulations.

467
00:28:12,667 --> 00:28:14,601
Thank you.

468
00:28:15,001 --> 00:28:16,635
Tiffany's gotten so big.

469
00:28:16,734 --> 00:28:18,133
Can I hold her?

470
00:28:18,133 --> 00:28:19,400
Of course.

471
00:28:19,004 --> 00:28:20,538
Tiffany, daddy's here.

472
00:28:20,934 --> 00:28:21,937
Let's go to daddy.

473
00:28:23,534 --> 00:28:24,901
Here we go.

474
00:28:25,934 --> 00:28:28,067
She's gotten heavier.

475
00:28:28,067 --> 00:28:29,074
Are you well?

476
00:28:31,701 --> 00:28:34,133
She is, thanks to you.

477
00:28:34,133 --> 00:28:35,834
What about you?

478
00:28:36,567 --> 00:28:38,801
Philip's well, too.

479
00:28:38,801 --> 00:28:40,767
I've been taking good care of him.

480
00:28:41,004 --> 00:28:42,271
I see.

481
00:28:43,001 --> 00:28:44,005
Tiffany.

482
00:28:44,005 --> 00:28:46,106
It's me. Daddy's here.

483
00:28:46,601 --> 00:28:48,934
Daddy's here. Let me see you.

484
00:28:48,934 --> 00:28:50,601
How adorable.

485
00:29:03,234 --> 00:29:04,238
Hello.

486
00:29:05,005 --> 00:29:06,272
Hello, Ms. Oh.

487
00:29:08,534 --> 00:29:11,542
What brings you here?

488
00:29:14,000 --> 00:29:14,968
Pardon?

489
00:29:17,000 --> 00:29:19,868
I'm here because my daughter

490
00:29:19,868 --> 00:29:22,367
is getting married today.

491
00:29:23,901 --> 00:29:24,908
I see...

492
00:29:26,968 --> 00:29:30,400
I'm here because my son's getting married.

493
00:29:30,004 --> 00:29:31,938
Oh, my! Congratulations!

494
00:29:32,334 --> 00:29:35,567
Oh, I see.

495
00:29:36,467 --> 00:29:37,500
Thank you.

496
00:29:38,667 --> 00:29:40,701
Mom, why are you over here?

497
00:29:40,701 --> 00:29:41,710
Your seat is over there.

498
00:29:42,601 --> 00:29:45,534
Jinwu, this man's son

499
00:29:45,534 --> 00:29:48,200
is getting married today, too.

500
00:29:49,868 --> 00:29:51,734
Mom, let's go over to that side.

501
00:29:51,734 --> 00:29:53,267
Okay.

502
00:29:53,267 --> 00:29:54,272
Goodbye now.

503
00:30:04,000 --> 00:30:05,367
Are you not feeling well?

504
00:30:05,367 --> 00:30:07,400
You don't look well.

505
00:30:07,004 --> 00:30:08,338
I'm fine.

506
00:30:09,003 --> 00:30:11,170
Why aren't they starting?

507
00:30:11,467 --> 00:30:12,475
The groom will now enter.

508
00:30:30,567 --> 00:30:32,534
He's so excited about seeing his bride

509
00:30:32,534 --> 00:30:34,200
that he can't stop smiling.

510
00:30:34,002 --> 00:30:37,536
I can tell he's going to be a fool for his wife.

511
00:30:37,734 --> 00:30:39,100
And today's lady of the hour.

512
00:30:39,001 --> 00:30:41,502
The beautiful bride will now enter.

513
00:30:41,601 --> 00:30:43,767
Here comes the bride.

514
00:31:41,667 --> 00:31:45,133
I can't believe I'm the officiator today.

515
00:31:45,834 --> 00:31:46,834
Life leads you

516
00:31:46,834 --> 00:31:49,968
to places you'd never imagine you'd be.

517
00:31:49,968 --> 00:31:51,667
You become family with people

518
00:31:51,667 --> 00:31:54,801
you'd never have imagined.

519
00:31:54,801 --> 00:31:57,367
And we call that destiny.

520
00:31:58,534 --> 00:32:01,701
If you find a kind person to be with,

521
00:32:01,701 --> 00:32:04,200
they say you'll be happy for 30 years.

522
00:32:04,002 --> 00:32:07,004
But if you find a dependable person,

523
00:32:07,004 --> 00:32:08,538
you'll be happy for life.

524
00:32:08,934 --> 00:32:10,100
Honey.

525
00:32:11,000 --> 00:32:14,334
Isn't our Areum so beautiful?

526
00:32:14,334 --> 00:32:15,901
She looks like an angel.

527
00:32:18,701 --> 00:32:21,934
I didn't know our little girl was so pretty.

528
00:32:23,934 --> 00:32:25,300
But honey...

529
00:32:26,004 --> 00:32:28,405
Why do I keep crying?

530
00:32:42,003 --> 00:32:44,404
Upon the green pastures

531
00:32:45,934 --> 00:32:48,467
I'd like to build a lovely house

532
00:32:49,005 --> 00:32:52,638
And live forevermore with my beloved

533
00:32:53,133 --> 00:32:55,801
For 100 years to come

534
00:32:56,634 --> 00:32:59,100
Sow seeds in spring

535
00:33:00,133 --> 00:33:02,701
And watch flowers bloom in summer

536
00:33:03,701 --> 00:33:06,467
In fall, we reap the bounty

537
00:33:07,267 --> 00:33:09,834
And in winter, we're as happy as can be

538
00:33:10,834 --> 00:33:13,840
Tall buildings may boast great heights

539
00:33:14,434 --> 00:33:17,367
And living the trendy life can be nice...

540
00:33:42,267 --> 00:33:44,601
I'll take the picture now.

541
00:33:44,601 --> 00:33:47,300
Ready? One, two...

542
00:33:47,003 --> 00:33:48,237
Wait!

543
00:33:48,534 --> 00:33:49,968
Here comes Tiffany.

544
00:33:50,901 --> 00:33:52,667
Our little Tiffany.

545
00:33:53,367 --> 00:33:54,369
Tiffany.

546
00:33:54,567 --> 00:33:56,400
Tiffany...

547
00:33:59,567 --> 00:34:02,801
How adorable.

548
00:34:02,801 --> 00:34:04,367
Tiffany.

549
00:34:04,367 --> 00:34:05,374
Tiffany, go to daddy.

550
00:34:07,003 --> 00:34:10,304
You look so pretty.

551
00:34:10,601 --> 00:34:11,604
Are you happy?

552
00:34:13,334 --> 00:34:15,343
Okay, I'm really taking the picture now.

553
00:34:16,234 --> 00:34:18,167
One, two...

554
00:34:18,167 --> 00:34:19,169
It's Eungmin!

555
00:35:13,767 --> 00:35:17,634
Thank you for watching "Love & Secret"

