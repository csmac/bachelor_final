1
00:00:07,891 --> 00:00:09,225
(Episode 5)

2
00:00:13,840 --> 00:00:15,730
No way. Look at her outfits.

3
00:00:15,732 --> 00:00:17,275
Do her parents abuse her at home or something?

4
00:00:17,567 --> 00:00:20,345
She would never figure out she's peculiar.

5
00:00:21,438 --> 00:00:23,045
She has no friends.

6
00:00:24,708 --> 00:00:25,975
It seems like she doesn't.

7
00:00:27,611 --> 00:00:29,685
Aren't you friends with Eun Tae Hee?

8
00:00:30,580 --> 00:00:31,915
What is she like?

9
00:00:32,282 --> 00:00:35,450
Well, when my friend, Tae Hee,

10
00:00:35,452 --> 00:00:39,020
sees a guy waiting for her on her usual route...

11
00:00:39,022 --> 00:00:40,395
What's his deal?

12
00:00:40,724 --> 00:00:42,365
She hates that.

13
00:00:44,761 --> 00:00:45,820
If...

14
00:00:45,829 --> 00:00:48,460
I'm in front of your building. I'll wait until you come down.

15
00:00:48,465 --> 00:00:50,135
If a guy waits the whole night...

16
00:00:59,109 --> 00:01:01,485
Is it possible that no guy ever told her that he liked her?

17
00:01:01,745 --> 00:01:04,755
No, Kyung Joo said she was popular even in Germany.

18
00:01:04,948 --> 00:01:08,750
The guy Kyung Joo likes ends up liking Tae Hee.

19
00:01:08,752 --> 00:01:10,595
She said that was the jinx between them.

20
00:01:11,054 --> 00:01:13,720
Luckily, they could stay as best friends...

21
00:01:13,723 --> 00:01:15,465
since Tae Hee was never interested in dating.

22
00:01:16,460 --> 00:01:18,590
She's not interested in dating.

23
00:01:18,595 --> 00:01:22,065
But why did Kyung Joo share everything with you?

24
00:01:23,500 --> 00:01:25,975
So what did you tell her on the bus?

25
00:01:27,838 --> 00:01:29,875
That was you?

26
00:01:30,107 --> 00:01:32,715
Don't you think it's enough to say that we are meant to be?

27
00:01:34,111 --> 00:01:35,885
I'm okay as long as you weren't hurt.

28
00:01:39,683 --> 00:01:42,750
Come on, why did you get off then? You should've followed her.

29
00:01:42,752 --> 00:01:44,155
You said she's kind.

30
00:01:44,921 --> 00:01:47,565
Since I got off without saying much to her,

31
00:01:47,824 --> 00:01:50,495
her guilt will eat her up.

32
00:01:57,467 --> 00:01:58,530
I agree.

33
00:01:58,535 --> 00:02:00,500
She will be less cautious of you when you see her next time.

34
00:02:00,504 --> 00:02:01,745
That's right.

35
00:02:01,938 --> 00:02:03,915
Then are you meeting her next...

36
00:02:04,441 --> 00:02:05,875
at the district library?

37
00:02:06,443 --> 00:02:07,645
Are you going to follow her again?

38
00:02:08,178 --> 00:02:09,445
Like it's a coincidence again?

39
00:02:09,479 --> 00:02:12,640
When coincidences repeat, she will feel like it's fate.

40
00:02:12,649 --> 00:02:15,380
- She'll end up believing it's fate. - Hello.

41
00:02:15,385 --> 00:02:17,495
Why? Because she wants to believe in fate.

42
00:02:19,923 --> 00:02:22,295
Hey, hey. That's so lame.

43
00:02:22,425 --> 00:02:25,820
Embracing her from behind looks only romantic on TV. It's lame.

44
00:02:25,829 --> 00:02:29,365
1 touch is equivalent to 100 words.

45
00:02:29,499 --> 00:02:31,100
45 degrees up from her eyes.

46
00:02:31,101 --> 00:02:32,735
Chiseled jawline.

47
00:02:32,969 --> 00:02:35,345
Oh, man. Make sure you show her that.

48
00:02:46,917 --> 00:02:48,250
Wait. What was her name again?

49
00:02:48,251 --> 00:02:50,880
Eun Tae Hee. Try to remember her name.

50
00:02:50,887 --> 00:02:53,150
Eun Tae Hee. Eun Tae Hee, right.

51
00:02:53,156 --> 00:02:55,220
If our great tempter, Kwon Si Hyun,

52
00:02:55,225 --> 00:02:56,590
could do some method-acting,

53
00:02:56,593 --> 00:02:58,795
his handsome looks would be enough to rule the world.

54
00:02:59,029 --> 00:03:00,205
Right?

55
00:03:01,398 --> 00:03:04,800
Hey, what do you mean I can't act?

56
00:03:04,801 --> 00:03:06,645
I might end up falling for her, you know.

57
00:03:06,670 --> 00:03:08,105
Look in the mirror.

58
00:03:08,138 --> 00:03:11,100
Your eyes are like the mirror Cinderella's stepmother has.

59
00:03:11,107 --> 00:03:14,685
They are full of honesty. I can see right through you.

60
00:03:14,778 --> 00:03:15,885
That's all right.

61
00:03:17,647 --> 00:03:20,125
No woman can look at me in the eye...

62
00:03:20,584 --> 00:03:22,180
- because her heart would flutter. - Cut it out.

63
00:03:22,185 --> 00:03:23,420
Leave her before lunch.

64
00:03:23,420 --> 00:03:25,225
Why? I should have lunch with her to get close.

65
00:03:25,789 --> 00:03:27,350
The best meal to develop relationships...

66
00:03:27,357 --> 00:03:29,820
would be the brunch after sleeping with the girl.

67
00:03:29,826 --> 00:03:32,265
You can't. You look like a fool when you eat.

68
00:03:32,963 --> 00:03:34,305
Then...

69
00:03:37,067 --> 00:03:39,705
How about going day drinking? She can drink as much as...

70
00:03:40,270 --> 00:03:41,870
8 glasses. 8...

71
00:03:41,871 --> 00:03:42,970
8 bottles?

72
00:03:42,973 --> 00:03:46,375
- That's the infinity symbol. - I know. It's the idol group.

73
00:03:47,611 --> 00:03:49,410
Do you not know the term, "infinity"?

74
00:03:49,412 --> 00:03:50,915
The mathematical term, infinity?

75
00:03:52,015 --> 00:03:53,255
Is this what it means?

76
00:03:53,617 --> 00:03:54,985
- Did you know that too? - Me?

77
00:03:58,355 --> 00:04:00,020
I can't believe I'm friends with these fools.

78
00:04:00,023 --> 00:04:02,425
What am I doing here? It's too early.

79
00:04:08,698 --> 00:04:09,805
Okay.

80
00:04:19,876 --> 00:04:22,045
Gosh, it's so bitter. Why do people drink this?

81
00:04:27,384 --> 00:04:28,785
Can you do something about that?

82
00:04:34,824 --> 00:04:36,195
- Hey. - What?

83
00:04:43,133 --> 00:04:44,435
Do you have something to tell me?

84
00:04:45,935 --> 00:04:47,445
If you don't, I'm going to go.

85
00:04:51,975 --> 00:04:53,115
Have...

86
00:04:53,710 --> 00:04:55,145
lunch with me.

87
00:05:08,992 --> 00:05:11,165
I was starving.

88
00:05:11,261 --> 00:05:12,490
You seem to enjoy your sleep.

89
00:05:12,495 --> 00:05:15,735
I couldn't get much sleep because I was moving.

90
00:05:15,765 --> 00:05:17,200
- Do you live around here? - Yes.

91
00:05:17,200 --> 00:05:19,500
- Do you often visit the library? - I think so.

92
00:05:19,502 --> 00:05:22,245
- But you came to see me today. - That's right.

93
00:05:24,374 --> 00:05:25,645
Oh, no.

94
00:05:25,909 --> 00:05:27,215
You got me.

95
00:05:29,913 --> 00:05:32,310
Actually, I came a few more times before.

96
00:05:32,315 --> 00:05:34,585
I thought I could see you here.

97
00:05:34,918 --> 00:05:37,455
When I first saw you, I saw you getting off in front of the library.

98
00:05:39,889 --> 00:05:42,565
You seemed to enjoy your sleep then.

99
00:05:45,028 --> 00:05:47,065
I didn't fall asleep.

100
00:05:52,635 --> 00:05:54,905
I'll show you an easy way to eat it.

101
00:05:56,272 --> 00:05:59,040
First, the bones in its back and the stomach...

102
00:05:59,042 --> 00:06:01,415
Remove the major bones on both ends first.

103
00:06:01,711 --> 00:06:03,145
Then you can enjoy the fish.

104
00:06:03,646 --> 00:06:04,985
Eat up.

105
00:06:05,148 --> 00:06:07,825
It's your favorite. You won't get to eat this often in Germany.

106
00:06:09,786 --> 00:06:11,855
I guess Mom's really not going to come.

107
00:06:11,888 --> 00:06:15,090
You know that she doesn't leave the kiln once she starts baking.

108
00:06:15,091 --> 00:06:16,220
We should try to understand her.

109
00:06:16,226 --> 00:06:18,335
That's exactly my point. Why does she have to bake today?

110
00:06:20,830 --> 00:06:22,065
Forget what I said.

111
00:06:22,799 --> 00:06:24,535
You shouldn't be sorry.

112
00:06:29,038 --> 00:06:30,875
You don't like cutlassfish.

113
00:06:31,207 --> 00:06:32,615
Do you want another dish?

114
00:06:34,010 --> 00:06:35,685
I heard a lot about you.

115
00:06:36,012 --> 00:06:37,455
A popular guy at Joosung High School.

116
00:06:38,047 --> 00:06:41,380
You're a man about town students from other schools come to see.

117
00:06:41,384 --> 00:06:43,380
I can't believe Kyung Joo told you that.

118
00:06:43,386 --> 00:06:45,995
- Come on, eat up. - But she said I must be careful.

119
00:06:46,156 --> 00:06:47,690
You're a troublemaker...

120
00:06:47,690 --> 00:06:50,695
who would do anything if it means you can seduce the girl.

121
00:06:50,860 --> 00:06:52,890
You saw me for the first time on the bus.

122
00:06:52,896 --> 00:06:55,605
You put out your will as your bait which was ridiculous.

123
00:06:59,002 --> 00:07:01,245
Is this how you seduced Kyung Joo's mom?

124
00:07:06,609 --> 00:07:08,815
I didn't have to use these childish tactics to her.

125
00:07:10,680 --> 00:07:12,155
Perhaps, you didn't hear the whole story.

126
00:07:15,118 --> 00:07:16,655
Should I tell you the details?

127
00:07:16,920 --> 00:07:18,455
Details about how we had fun?

128
00:07:19,556 --> 00:07:21,095
No, there's no need.

129
00:07:23,193 --> 00:07:25,660
I don't want to spend another minute with someone...

130
00:07:25,662 --> 00:07:27,105
who toys with someone's heart.

131
00:07:28,364 --> 00:07:30,335
I hope I never see you again.

132
00:07:38,374 --> 00:07:40,215
What's up with that skirt? It's too long.

133
00:07:40,276 --> 00:07:43,270
- It's not your style. - Consider it my combat uniform.

134
00:07:43,279 --> 00:07:44,985
I already know what type of family it must be.

135
00:07:47,016 --> 00:07:50,080
You won't nag your girlfriend because her skirt is too short.

136
00:07:50,086 --> 00:07:51,580
I like that about you.

137
00:07:51,588 --> 00:07:54,425
I ought to sue everyone who whines about women's skirt lengths.

138
00:07:54,557 --> 00:07:56,225
That's a violation of a basic human right.

139
00:07:56,860 --> 00:07:58,965
Wait, I know who will be your girlfriend.

140
00:07:59,863 --> 00:08:01,105
What?

141
00:08:01,431 --> 00:08:02,935
I slept by myself last night.

142
00:08:04,601 --> 00:08:07,545
I'm not talking about how you sleep with women before you befriend them.

143
00:08:09,339 --> 00:08:10,545
What's she saying?

144
00:08:11,307 --> 00:08:13,515
Hey, what is it?

145
00:08:13,877 --> 00:08:14,985
Whom are you talking about?

146
00:08:15,645 --> 00:08:16,845
Am I dating someone?

147
00:08:17,881 --> 00:08:20,185
The crazy Lee Se Joo? Why?

148
00:08:20,283 --> 00:08:22,725
He's so cool because he's crazy.

149
00:08:22,886 --> 00:08:25,495
What's Se Joo's type?

150
00:08:26,322 --> 00:08:27,825
No idea. I don't care...

151
00:08:28,558 --> 00:08:30,120
Well, not that I don't.

152
00:08:30,126 --> 00:08:31,860
Someone who's a bit like you.

153
00:08:31,861 --> 00:08:35,365
You should've just handed the country over, not your friend.

154
00:08:35,832 --> 00:08:37,130
I'm not telling you to date her.

155
00:08:37,133 --> 00:08:39,435
Just take her out once. You have nothing to do anyway.

156
00:08:39,969 --> 00:08:42,605
I'm a busy man. I have a lot of girls to meet.

157
00:08:43,172 --> 00:08:44,770
I'll get you out to the hospital.

158
00:08:44,774 --> 00:08:46,645
Legitimately, through the secretary's office.

159
00:08:47,076 --> 00:08:49,215
- Here it is. - Thank you.

160
00:08:50,914 --> 00:08:52,210
I'm a humanist.

161
00:08:52,215 --> 00:08:53,810
I can love every single human being.

162
00:08:53,816 --> 00:08:56,225
But not Kyung Joo.

163
00:08:57,053 --> 00:09:00,795
First of all, you need to get closer to Hye Jeong's mom to see her.

164
00:09:01,157 --> 00:09:02,425
That's what her family is like.

165
00:09:02,859 --> 00:09:04,795
The most important thing is to be fashionable.

166
00:09:05,028 --> 00:09:09,335
She thinks she's Meryl Streep of "The Devil Wears Prada".

167
00:09:22,145 --> 00:09:23,640
Welcome.

168
00:09:23,646 --> 00:09:26,855
Goodness, you finally came around.

169
00:09:26,983 --> 00:09:28,810
Thank you for inviting me.

170
00:09:28,818 --> 00:09:31,825
My goodness, how beautiful.

171
00:09:59,582 --> 00:10:00,650
Bravo.

172
00:10:00,650 --> 00:10:04,695
Hye Jeong, you picked that up at an amazing speed.

173
00:10:05,455 --> 00:10:06,595
That was great.

174
00:10:07,023 --> 00:10:08,020
Your arm strength, I mean.

175
00:10:08,024 --> 00:10:10,560
You should ask Soo Ji to teach you a lot.

176
00:10:10,560 --> 00:10:13,265
You'll make it if you try just a little harder.

177
00:10:14,030 --> 00:10:16,935
It would be great if you two could see classical concerts together.

178
00:10:17,767 --> 00:10:20,375
Yes, if we have time.

179
00:10:20,837 --> 00:10:24,215
I'm sad that I didn't get to see you play at the Supporters' Night.

180
00:10:24,574 --> 00:10:27,745
Anyway, it looked like you had no idea...

181
00:10:28,011 --> 00:10:30,345
that your mom was getting married.

182
00:10:32,348 --> 00:10:34,685
I was wondering why you didn't ask.

183
00:10:34,917 --> 00:10:37,650
It was a surprise event, so I acted like I didn't know.

184
00:10:37,654 --> 00:10:39,925
My mom is cute like that.

185
00:10:40,757 --> 00:10:44,095
I see. She's such a prankster.

186
00:10:44,394 --> 00:10:47,805
Then may I ask you to play a song?

187
00:10:47,897 --> 00:10:51,435
What a good liar. You didn't even blink an eye.

188
00:10:51,868 --> 00:10:52,975
I'm sorry.

189
00:10:53,102 --> 00:10:55,445
May I play for you next time with my instrument?

190
00:10:55,538 --> 00:10:58,470
I know my mom lets you have all of your ways,

191
00:10:58,474 --> 00:10:59,840
so you must think I'm a joke too.

192
00:10:59,842 --> 00:11:02,870
Of course. It would be more comfortable to use your own.

193
00:11:02,879 --> 00:11:05,515
I sounded very ignorant.

194
00:11:06,249 --> 00:11:09,855
Look at the way she talks back at me with everything I say. What a brat.

195
00:11:10,253 --> 00:11:12,650
There's nothing you can do even if you think I'm a brat.

196
00:11:12,655 --> 00:11:13,925
I'm a part of JK Group.

197
00:11:20,196 --> 00:11:22,535
What are you two doing? Sending telepathies?

198
00:11:24,033 --> 00:11:25,575
Please excuse me.

199
00:11:25,802 --> 00:11:28,075
I was reading.

200
00:11:29,639 --> 00:11:30,970
Have fun.

201
00:11:30,973 --> 00:11:32,415
See you later.

202
00:11:41,517 --> 00:11:42,655
("Secrets in Bed")

203
00:11:44,821 --> 00:11:48,125
Look at the way she says everything she wants while smiling.

204
00:11:49,225 --> 00:11:52,465
She's just like her mom.

205
00:12:12,181 --> 00:12:13,755
I wouldn't take it even if you gave it to me.

206
00:12:14,951 --> 00:12:17,680
- What's wrong? - I'm hopeless at cello, right?

207
00:12:17,687 --> 00:12:19,750
- Yes. - Right?

208
00:12:19,756 --> 00:12:22,690
Then can you tell that to my mom?

209
00:12:22,692 --> 00:12:25,920
Cello strings are strangling me, and pianos pin me down...

210
00:12:25,928 --> 00:12:29,165
in my dreams. It's really killing me.

211
00:12:29,565 --> 00:12:31,505
I'm not your tutor.

212
00:12:32,335 --> 00:12:33,505
Fine.

213
00:12:36,239 --> 00:12:38,175
Why is it so hot in here?

214
00:12:44,647 --> 00:12:45,955
My goodness.

215
00:12:46,849 --> 00:12:48,755
You're so sexy.

216
00:12:51,687 --> 00:12:54,995
I'll see. If she ever asks, I'll try to bring it up.

217
00:12:56,626 --> 00:12:59,890
Right, where's Tle?

218
00:12:59,896 --> 00:13:01,935
You have to say hello.

219
00:13:02,565 --> 00:13:04,100
Tle? Who's Tle?

220
00:13:04,100 --> 00:13:05,835
Where did Tle go?

221
00:13:09,238 --> 00:13:10,670
My gosh, Tle.

222
00:13:10,673 --> 00:13:13,700
You can't sit there and surprise Soo Ji.

223
00:13:13,709 --> 00:13:15,345
Were you surprised?

224
00:13:15,812 --> 00:13:18,980
This is my sister, Turtle. Say hello to Soo Ji.

225
00:13:18,981 --> 00:13:21,980
- Do you want to hold her? - No, it's fine.

226
00:13:21,984 --> 00:13:23,150
Hi, Tle.

227
00:13:23,152 --> 00:13:24,450
She doesn't bite.

228
00:13:24,453 --> 00:13:27,195
Tle, go back to your room.

229
00:13:27,590 --> 00:13:30,050
Soo Ji, do you want to take selfies, then?

230
00:13:30,059 --> 00:13:31,695
Okay, should we?

231
00:13:32,295 --> 00:13:34,165
Come here. Let's take it here.

232
00:13:36,065 --> 00:13:38,335
Soo Ji, do you want to hold this too?

233
00:13:42,238 --> 00:13:46,170
These cupcakes are from the second son of the law firm.

234
00:13:46,175 --> 00:13:48,545
Gosh, thank you.

235
00:13:50,413 --> 00:13:52,485
They look so good.

236
00:13:52,748 --> 00:13:54,750
Your mom said she will thank him.

237
00:13:54,750 --> 00:13:57,055
You don't need to call him.

238
00:13:57,286 --> 00:13:58,425
Okay.

239
00:14:01,858 --> 00:14:05,495
I thought of you when I had this patissier's cupcakes.

240
00:14:05,828 --> 00:14:08,635
I hope this makes a sweet present for you. From Ki Young.

241
00:14:09,866 --> 00:14:11,375
What's with the way you talk?

242
00:14:11,801 --> 00:14:14,130
Do you think this is the 1900s? Are you writing a poem?

243
00:14:14,136 --> 00:14:15,970
What did you say? Aren't you going to eat?

244
00:14:15,972 --> 00:14:17,375
This is really good.

245
00:14:17,406 --> 00:14:18,915
I wanted to take more selfies.

246
00:14:23,379 --> 00:14:24,455
In 1, 2, 3.

247
00:14:25,982 --> 00:14:28,850
After I met her, I kind of felt bad for her.

248
00:14:28,851 --> 00:14:30,650
She only had a turtle to talk to.

249
00:14:30,653 --> 00:14:31,950
So for a second,

250
00:14:31,954 --> 00:14:34,325
I felt bad and thought I'd become like a sister for her.

251
00:14:34,557 --> 00:14:37,920
But my gosh, Ki Young. He sent cute cupcakes.

252
00:14:37,927 --> 00:14:38,990
This is your fourth serving.

253
00:14:38,995 --> 00:14:41,005
Leave me alone. I need the energy to fight.

254
00:14:41,898 --> 00:14:43,630
You can't put up a fight if you get fat.

255
00:14:43,633 --> 00:14:46,260
- Get fat? What? - Pardon?

256
00:14:46,269 --> 00:14:49,205
Who are you to say that my only weapon is my body?

257
00:14:49,672 --> 00:14:52,370
I never knew that you could eat pig skin.

258
00:14:52,375 --> 00:14:54,570
You have respect for women's sexual desires,

259
00:14:54,577 --> 00:14:57,415
skirt lengths, and hairstyle, but you deny their appetite?

260
00:14:57,647 --> 00:14:59,810
I know how to eat pig skin. I love it.

261
00:14:59,815 --> 00:15:03,450
Exactly. I had to set everything up by myself. It was hard.

262
00:15:03,452 --> 00:15:04,555
Great job.

263
00:15:04,754 --> 00:15:07,350
You know that I'm called the Barbeque Master, right?

264
00:15:07,356 --> 00:15:09,465
They pop up when they get cooked, so only I can grill this.

265
00:15:09,558 --> 00:15:11,095
Make sure you dip it in bean flour.

266
00:15:11,761 --> 00:15:13,095
- Gosh. - Darn you.

267
00:15:15,097 --> 00:15:17,760
- I failed. - What now? What is it?

268
00:15:17,767 --> 00:15:20,375
She knows everything including how I met Kyung Joo's mom.

269
00:15:21,003 --> 00:15:24,270
Did Kyung Joo tell her that? That her mom and you did that?

270
00:15:24,273 --> 00:15:26,200
What? No way. She'd be talking bad about her mom.

271
00:15:26,208 --> 00:15:27,415
No idea.

272
00:15:27,877 --> 00:15:30,980
Wait, what was your bet again? I never heard that part.

273
00:15:30,980 --> 00:15:32,210
What? You didn't know?

274
00:15:32,214 --> 00:15:34,355
She didn't want to marry without love, so...

275
00:15:34,850 --> 00:15:36,010
What? What is it?

276
00:15:36,018 --> 00:15:38,250
The loser will go on a hunger strike.

277
00:15:38,254 --> 00:15:40,790
That 1 of the 2 will die of hunger if they get married.

278
00:15:40,790 --> 00:15:43,295
A hunger strike? You?

279
00:15:43,326 --> 00:15:45,660
Keep grilling. He's starving.

280
00:15:45,661 --> 00:15:46,660
Okay, wait.

281
00:15:46,662 --> 00:15:49,130
If you win, you will go on a hunger strike.

282
00:15:49,131 --> 00:15:51,105
If you lose, are you going to starve?

283
00:15:51,434 --> 00:15:53,500
His dad won't budge even if he starved.

284
00:15:53,502 --> 00:15:55,870
I know. He said he'll hand his car over.

285
00:15:55,871 --> 00:15:56,945
What?

286
00:15:57,540 --> 00:15:59,440
Darn it. You can't give up now.

287
00:15:59,442 --> 00:16:00,570
Eat up, and get more energy.

288
00:16:00,576 --> 00:16:02,640
I can bear to see your dad and her mom get married,

289
00:16:02,645 --> 00:16:04,910
but not your car get taken away.

290
00:16:04,914 --> 00:16:06,880
So are you telling me to starve?

291
00:16:06,882 --> 00:16:09,025
You should eat a lot now.

292
00:16:15,958 --> 00:16:17,765
(I hope this makes a sweet present for you. From Ki Young.)

293
00:16:18,427 --> 00:16:19,835
I don't see it in here either.

294
00:16:20,563 --> 00:16:22,535
- Are you looking for something? - What?

295
00:16:22,965 --> 00:16:24,375
Oh, a plane.

296
00:16:25,334 --> 00:16:27,575
- Are those good? - Totally.

297
00:16:29,505 --> 00:16:32,975
You should hurry up and send him a picture saying you got them.

298
00:16:33,009 --> 00:16:35,885
- No. It's too embarrassing. - Look over here.

299
00:16:36,679 --> 00:16:38,315
Show me a smile.

300
00:16:42,985 --> 00:16:46,050
- Ki Young, the cupcakes were good. - So cute.

301
00:16:46,055 --> 00:16:47,195
Thank you.

302
00:16:47,890 --> 00:16:49,465
You're so cute.

303
00:16:51,093 --> 00:16:52,835
I can get 100 of them for you.

304
00:16:56,899 --> 00:16:58,300
(Hye Jeong's mom)

305
00:16:58,300 --> 00:17:01,330
Hye Jeong said she loves the cupcakes.

306
00:17:01,337 --> 00:17:03,545
Thanks for the thought.

307
00:17:07,043 --> 00:17:09,115
Why does it feel like I'm getting tamed?

308
00:17:15,084 --> 00:17:16,155
(Park Hye Jeong)

309
00:17:21,624 --> 00:17:23,865
- There's a taxi. - Okay.

310
00:17:23,926 --> 00:17:26,220
- Please get in. - Goodbye.

311
00:17:26,228 --> 00:17:28,705
Bye. Text me when you get there.

312
00:17:29,165 --> 00:17:31,305
- Hurry up, and go home. - Bye.

313
00:17:48,417 --> 00:17:49,555
Gosh.

314
00:18:05,434 --> 00:18:07,475
Was I too harsh?

315
00:18:09,238 --> 00:18:11,245
It didn't even happen to me.

316
00:18:13,776 --> 00:18:16,715
Shall I give you the details of what we did?

317
00:18:19,415 --> 00:18:20,585
Whatever.

318
00:18:21,283 --> 00:18:23,455
I'll give him his plane or will, whatever it was.

319
00:18:24,787 --> 00:18:26,355
And then I'll end it properly.

320
00:18:34,396 --> 00:18:36,035
(Kwon Si Hyun)

321
00:18:39,969 --> 00:18:42,305
1, 2...

322
00:18:42,338 --> 00:18:43,330
(Eun Tae Hee)

323
00:18:43,339 --> 00:18:44,445
(Kwon Si Hyun)

324
00:18:47,610 --> 00:18:50,215
What was that? Why did he even call?

325
00:18:52,915 --> 00:18:54,925
That should bother her.

326
00:19:09,231 --> 00:19:10,490
Are you going out this afternoon?

327
00:19:10,499 --> 00:19:13,305
Shopping. For something to wear when the new term starts.

328
00:19:14,036 --> 00:19:15,445
Shall I join you?

329
00:19:16,038 --> 00:19:17,205
We can get our hair done.

330
00:19:17,306 --> 00:19:19,945
What for? Aren't you busy?

331
00:19:24,480 --> 00:19:25,855
Whom will you go with?

332
00:19:26,415 --> 00:19:28,585
You know I can't go shopping with anyone.

333
00:19:29,251 --> 00:19:31,055
You've been seeing Hye Jeong.

334
00:19:32,188 --> 00:19:35,150
I find it interesting that you befriended her.

335
00:19:35,157 --> 00:19:36,625
And strange.

336
00:19:38,327 --> 00:19:42,105
It's more interesting and strange that you're dating Si Hyun's dad.

337
00:19:43,065 --> 00:19:45,075
Does he even like you for real?

338
00:19:49,805 --> 00:19:51,075
Whatever.

339
00:19:51,207 --> 00:19:53,715
Get me a new bed. I can't sleep.

340
00:19:56,212 --> 00:19:58,555
- Keep this bed. - I can't sleep.

341
00:20:00,516 --> 00:20:02,655
We'll move into a new house soon.

342
00:20:05,888 --> 00:20:07,395
Bear with it until then.

343
00:20:10,059 --> 00:20:11,365
Thank you.

344
00:20:35,951 --> 00:20:37,025
(Vice Chairman Kwon Seok Woo)

345
00:20:39,388 --> 00:20:40,555
(Vice Chairman Kwon Seok Woo)

346
00:20:44,526 --> 00:20:47,790
I got the foundation's budget for this year.

347
00:20:47,796 --> 00:20:50,635
I think you should clear this.

348
00:20:50,666 --> 00:20:54,700
The proposal on the new medication that Secretary Yoon brought over...

349
00:20:54,703 --> 00:20:56,305
Did you have breakfast?

350
00:20:56,705 --> 00:20:58,775
I only had a cup of coffee.

351
00:20:59,108 --> 00:21:00,315
What?

352
00:21:01,010 --> 00:21:02,685
You talked about work first.

353
00:21:03,579 --> 00:21:04,815
It's upsetting.

354
00:21:06,548 --> 00:21:08,025
Oh, sorry.

355
00:21:08,317 --> 00:21:10,885
Are you free the day you return from your business trip?

356
00:21:11,120 --> 00:21:13,455
Let's have dinner with the kids.

357
00:21:14,723 --> 00:21:16,725
Sure, I'll free my schedule.

358
00:21:17,059 --> 00:21:18,890
Pick a place Si Hyun likes...

359
00:21:18,894 --> 00:21:22,060
Let's meet at the hotel we had the event at.

360
00:21:22,064 --> 00:21:23,235
(Flowers to Seol Young Won were delivered.)

361
00:21:23,299 --> 00:21:24,400
Who sent these?

362
00:21:24,400 --> 00:21:26,275
I don't know. It doesn't say.

363
00:21:27,169 --> 00:21:28,475
They're my favorite flowers.

364
00:21:32,441 --> 00:21:33,975
Was it you?

365
00:21:34,476 --> 00:21:37,285
Fortunately for us, it was not.

366
00:21:38,414 --> 00:21:41,225
I can't believe how disappointed you sounded.

367
00:21:41,950 --> 00:21:43,025
Sorry.

368
00:21:44,787 --> 00:21:45,895
Geum Sil.

369
00:21:46,889 --> 00:21:49,195
Thanks for celebrating Tae Hee's birthday.

370
00:21:49,458 --> 00:21:51,095
I completely forgot.

371
00:21:51,327 --> 00:21:52,635
She found out.

372
00:21:52,761 --> 00:21:54,635
She should've pretended it was from you.

373
00:21:55,431 --> 00:21:57,135
Did she complain about it?

374
00:22:11,880 --> 00:22:13,185
I feel so cold.

375
00:22:29,531 --> 00:22:31,135
(Eun Tae Hee)

376
00:22:31,166 --> 00:22:33,335
I left a missed call,

377
00:22:34,136 --> 00:22:35,905
but she didn't call back?

378
00:22:46,415 --> 00:22:48,285
(Kwon Si Hyun)

379
00:22:48,350 --> 00:22:50,385
1, 2...

380
00:22:54,056 --> 00:22:55,425
What is he doing?

381
00:22:56,558 --> 00:22:57,995
What do you want?

382
00:22:59,361 --> 00:23:01,405
It's such a bother.

383
00:23:07,903 --> 00:23:09,105
Gosh.

384
00:23:12,708 --> 00:23:15,910
I love the makeup and everything you picked out.

385
00:23:15,911 --> 00:23:17,210
I should lose weight.

386
00:23:17,212 --> 00:23:19,580
Don't, Se Joo likes girls who eat well.

387
00:23:19,581 --> 00:23:21,010
At least four portions of meat.

388
00:23:21,016 --> 00:23:22,085
Really?

389
00:23:22,151 --> 00:23:23,285
Ko Kyung Joo.

390
00:23:24,253 --> 00:23:25,925
Don't forget the favor I asked.

391
00:23:25,954 --> 00:23:27,380
I won't.

392
00:23:27,389 --> 00:23:29,550
If it doesn't work out, it's not my fault.

393
00:23:29,558 --> 00:23:30,965
It's not.

394
00:23:31,894 --> 00:23:33,235
He's here.

395
00:23:37,266 --> 00:23:40,130
Gosh, you should've told me.

396
00:23:40,135 --> 00:23:41,405
Good luck.

397
00:23:46,408 --> 00:23:47,775
Hi, Ko Kyung Joo.

398
00:23:48,210 --> 00:23:50,785
Hi. It's been a while.

399
00:23:57,286 --> 00:23:59,195
He said he couldn't stand her.

400
00:24:00,255 --> 00:24:03,125
Oh dear. I have an appointment.

401
00:24:03,325 --> 00:24:04,735
- An appointment? - An appointment?

402
00:24:05,194 --> 00:24:07,890
Se Joo, take Kyung Joo home safely.

403
00:24:07,896 --> 00:24:10,090
You two have similar sounding names.

404
00:24:10,098 --> 00:24:12,035
Ko Kyung Joo, Lee Se Joo.

405
00:24:12,668 --> 00:24:15,245
Go on, Se Joo. Get the door for her.

406
00:24:18,407 --> 00:24:21,370
I wish you could come along. Is it an important appointment?

407
00:24:21,376 --> 00:24:23,115
Yes, totally.

408
00:24:23,412 --> 00:24:24,655
Goodbye.

409
00:24:27,316 --> 00:24:29,585
Don't mess up, or our deal's off.

410
00:24:29,685 --> 00:24:31,125
Keep your word.

411
00:24:32,988 --> 00:24:34,495
Thank you.

412
00:24:40,262 --> 00:24:42,505
Shall we race to your home?

413
00:24:43,265 --> 00:24:44,560
Where do you live?

414
00:24:44,566 --> 00:24:46,635
We don't have to rush.

415
00:24:47,402 --> 00:24:48,675
Let's speed.

416
00:25:04,686 --> 00:25:06,020
Are you having dinner?

417
00:25:06,021 --> 00:25:08,350
No, Kyung Joo went home.

418
00:25:08,357 --> 00:25:09,890
You should've fed her.

419
00:25:09,892 --> 00:25:11,435
My family has a memorial service.

420
00:25:13,428 --> 00:25:14,835
Okay, bye.

421
00:25:18,600 --> 00:25:20,035
(Aunt Mi Soon)

422
00:25:21,103 --> 00:25:22,375
Hello.

423
00:25:24,506 --> 00:25:26,075
I'll eat at home.

424
00:25:27,209 --> 00:25:28,445
Bye.

425
00:25:33,682 --> 00:25:36,250
- What's up? - Nothing. You're leaving?

426
00:25:36,251 --> 00:25:37,785
No, I just got here.

427
00:25:37,819 --> 00:25:39,820
You said you had a memorial service.

428
00:25:39,821 --> 00:25:42,125
So what? I'm starving. What shall we eat?

429
00:25:42,925 --> 00:25:44,425
- Sushi. - Sure.

430
00:25:45,127 --> 00:25:46,720
I just can't believe it.

431
00:25:46,728 --> 00:25:47,890
Guess what she said.

432
00:25:47,896 --> 00:25:50,390
She won't get me a new bed. I have to bear with it.

433
00:25:50,399 --> 00:25:53,100
Just buy whatever bed you want.

434
00:25:53,101 --> 00:25:55,730
You idiot, what do you think she means?

435
00:25:55,737 --> 00:25:58,500
I have to wait until she refurnishes the house.

436
00:25:58,507 --> 00:25:59,900
Let's say they get married.

437
00:25:59,908 --> 00:26:02,915
Would my mom move into your place? She'd live elsewhere.

438
00:26:05,847 --> 00:26:07,710
Since we're all here,

439
00:26:07,716 --> 00:26:09,925
let's strategize. Come along, pretty boy.

440
00:26:12,521 --> 00:26:13,825
Soo Ji.

441
00:26:13,922 --> 00:26:16,165
Did you ask Kyung Joo for her help?

442
00:26:16,425 --> 00:26:17,695
Focus on your own role.

443
00:26:17,826 --> 00:26:21,120
Then they can meet this weekend at...

444
00:26:21,129 --> 00:26:22,860
(Volunteer at nursing home)

445
00:26:22,864 --> 00:26:25,805
- A nursing home? - No. This weekend?

446
00:26:25,867 --> 00:26:28,075
No, that's just wrong.

447
00:26:28,537 --> 00:26:29,845
What's that?

448
00:26:29,972 --> 00:26:35,185
You were born to serve others

449
00:26:35,644 --> 00:26:37,740
Some wives say they had plastic surgery,

450
00:26:37,746 --> 00:26:40,715
and their husbands couldn't tell.

451
00:26:40,949 --> 00:26:44,410
That's because they're husbands, not boyfriends.

452
00:26:44,419 --> 00:26:46,650
You couldn't be bothered to use hair conditioner.

453
00:26:46,655 --> 00:26:49,125
They recognize even the smallest of changes.

454
00:26:49,358 --> 00:26:51,090
That means affection.

455
00:26:51,093 --> 00:26:54,495
Men are surprisingly visual beings.

456
00:26:54,596 --> 00:26:56,865
Where did you read that?

457
00:26:57,032 --> 00:26:59,375
All the theories I read up on until now...

458
00:26:59,501 --> 00:27:02,545
are finally coming in useful.

459
00:27:03,338 --> 00:27:06,915
Let's meet at a club this Saturday. Dress up.

460
00:27:07,943 --> 00:27:09,945
Who is it that you're so pleased?

461
00:27:10,145 --> 00:27:11,485
Choi Soo Ji.

462
00:27:11,546 --> 00:27:14,655
I thought she'd be haughty and cold.

463
00:27:14,916 --> 00:27:16,310
She isn't at all.

464
00:27:16,318 --> 00:27:17,885
Good for you.

465
00:27:17,986 --> 00:27:20,355
You should've become friends before you graduated.

466
00:27:21,323 --> 00:27:23,695
- Are you upset? - No.

467
00:27:23,792 --> 00:27:26,765
Hey, I suddenly feel bad.

468
00:27:26,895 --> 00:27:31,290
We might not be able to meet up as much if I start dating.

469
00:27:31,299 --> 00:27:33,000
Get yourself a boyfriend.

470
00:27:33,001 --> 00:27:34,300
No, thanks.

471
00:27:34,302 --> 00:27:36,075
Why would you talk about that?

472
00:27:37,906 --> 00:27:40,415
Do you remember Kwon Si Hyun?

473
00:27:40,509 --> 00:27:42,385
- Who? - The guy we met.

474
00:27:42,944 --> 00:27:44,840
Right. Yes.

475
00:27:44,846 --> 00:27:46,910
His dad and Soo Ji's mom will marry,

476
00:27:46,915 --> 00:27:49,385
and they'll become step-siblings.

477
00:27:49,484 --> 00:27:50,855
Wouldn't that be weird?

478
00:27:50,952 --> 00:27:52,450
Is that true?

479
00:27:52,454 --> 00:27:55,550
Soo Ji's really worried about him.

480
00:27:55,557 --> 00:27:59,720
Si Hyun had a rough time after his mom died.

481
00:27:59,728 --> 00:28:01,760
He got scolded even more for being immature.

482
00:28:01,763 --> 00:28:03,705
And there were bad rumors about him.

483
00:28:04,166 --> 00:28:07,535
But actually, he's not that bad, you know.

484
00:28:07,569 --> 00:28:08,700
- He's quiet. - Eun Tae Hee.

485
00:28:08,704 --> 00:28:11,200
Nursing home. Wake up. Wake up.

486
00:28:11,206 --> 00:28:12,700
Nursing home. Wake up.

487
00:28:12,708 --> 00:28:14,075
He's gentle.

488
00:28:15,610 --> 00:28:17,115
He's kind.

489
00:28:18,580 --> 00:28:20,115
He's handsome.

490
00:28:20,315 --> 00:28:22,955
He's tall. He's cool.

491
00:28:24,920 --> 00:28:28,150
That's why people misunderstand him as a player,

492
00:28:28,156 --> 00:28:29,265
but that's not true.

493
00:28:29,424 --> 00:28:31,390
He seems to be nice to everyone,

494
00:28:31,393 --> 00:28:34,790
but he's only kind to the girl he likes in real life.

495
00:28:34,796 --> 00:28:36,535
In other words, he's a real man.

496
00:28:38,800 --> 00:28:40,075
You got a haircut.

497
00:28:41,470 --> 00:28:42,645
You look cute.

498
00:29:20,108 --> 00:29:21,645
How long will you follow me?

499
00:29:21,743 --> 00:29:23,285
Why did you call me two days ago?

500
00:29:23,745 --> 00:29:24,940
- I did? - Yes.

501
00:29:24,946 --> 00:29:27,340
You left three missed calls.

502
00:29:27,349 --> 00:29:29,385
You should've called if you wanted to know.

503
00:29:30,018 --> 00:29:32,020
I forgot why I called you in the first place.

504
00:29:32,020 --> 00:29:34,355
Is it that you have nothing to do or that you have a lot of time?

505
00:29:35,991 --> 00:29:37,095
It's both.

506
00:29:39,194 --> 00:29:42,105
Anyway, tell me why you followed me here.

507
00:29:42,197 --> 00:29:44,405
- Because I wanted to see you. - What?

508
00:29:44,900 --> 00:29:46,475
I followed you here because I wanted to see you.

509
00:29:48,336 --> 00:29:50,105
Why did you want to see me?

510
00:29:50,338 --> 00:29:51,675
Because I'm interested in you.

511
00:29:53,608 --> 00:29:54,885
Tae Hee!

512
00:29:55,577 --> 00:29:57,440
- Hello. - Hey.

513
00:29:57,445 --> 00:30:00,040
You said you'd bring a friend. I did not know it'd be your boyfriend.

514
00:30:00,048 --> 00:30:01,115
Sorry?

515
00:30:02,083 --> 00:30:05,495
- No, he's not my boyfriend. - It's so nice to meet you.

516
00:30:05,887 --> 00:30:08,490
This is Father Mateo. This is Ms. Park.

517
00:30:08,490 --> 00:30:10,465
And I'm Columba.

518
00:30:11,459 --> 00:30:12,635
I'm glad you're here.

519
00:30:13,528 --> 00:30:16,630
So am I. I am really happy that you came.

520
00:30:16,631 --> 00:30:18,505
Please join us.

521
00:30:21,403 --> 00:30:23,675
My goodness.

522
00:30:26,441 --> 00:30:28,745
Thank you for coming.

523
00:30:32,013 --> 00:30:33,285
It's all right.

