1
00:02:28,500 --> 00:02:30,020
Where do you think you're going?

2
00:02:30,740 --> 00:02:32,820
To have a look. The guy looks alive.

3
00:02:33,100 --> 00:02:35,660
Don't be ridiculous. Get back in.
We'll call the police.

4
00:02:35,860 --> 00:02:37,780
Take down the number plate then.

5
00:04:05,300 --> 00:04:13,100
Three Monkeys

6
00:05:31,300 --> 00:05:32,300
Yes, boss?

7
00:05:35,900 --> 00:05:36,780
Now?

8
00:05:38,860 --> 00:05:42,580
OK. I'll be right there.

9
00:06:29,420 --> 00:06:31,100
I just called the lawyer.

10
00:06:31,900 --> 00:06:34,740
He says they won't suspect
anything since you're my driver.

11
00:06:35,220 --> 00:06:37,780
Six months maximum. A year at the most.

12
00:06:41,100 --> 00:06:43,860
At least you'll get a lump
sum when you're out.

13
00:06:46,340 --> 00:06:49,940
You know I'd never ask you if I wasn't
running in the elections.

14
00:06:50,500 --> 00:06:53,540
If anyone hears about the accident,
on the eve of the elections...

15
00:06:53,860 --> 00:06:57,460
...my political career will end right there.
You know that.

16
00:06:57,780 --> 00:07:00,340
They're just waiting to
nail me, the bastards.

17
00:07:02,700 --> 00:07:05,660
Your son can get your salary every month.

18
00:07:06,580 --> 00:07:09,260
I'll pay the lump sum in
cash when you get out.

19
00:07:09,620 --> 00:07:10,940
How's that?

20
00:07:13,900 --> 00:07:16,900
Let's not get the banks involved.
Just in case.

21
00:07:18,460 --> 00:07:20,580
- OK?
- OK. No problem.

22
00:07:24,700 --> 00:07:26,140
Shall I give you the key?

23
00:07:27,180 --> 00:07:30,260
It's in the car park. By the
entrance, on the right.

24
00:09:15,940 --> 00:09:19,100
Ismail, get up! You'll miss the train.

25
00:10:48,620 --> 00:10:50,300
You've got a whole year.

26
00:10:50,980 --> 00:10:54,900
This time, you've got to pass
that university entrance exam.

27
00:10:57,940 --> 00:10:59,300
OK?

28
00:11:01,180 --> 00:11:02,180
We'll see.

29
00:11:02,420 --> 00:11:03,940
What do you mean "we'll see"?

30
00:11:05,180 --> 00:11:07,340
Well, we'll see, dad. Don't worry about it.

31
00:11:09,500 --> 00:11:10,860
"We'll see", huh?

32
00:12:07,060 --> 00:12:09,660
What's this? Sleeping in
the middle of the day!

33
00:12:10,540 --> 00:12:11,900
What can I do? I'm bored.

34
00:12:12,380 --> 00:12:15,300
Of course you're bored.
Find yourself something to do.

35
00:12:17,180 --> 00:12:20,420
- I did. But you turned up your nose.
- At what?

36
00:12:20,980 --> 00:12:21,980
Forget it.

37
00:12:22,220 --> 00:12:23,740
Come on! What was it?

38
00:12:24,980 --> 00:12:26,860
Just forget it. As if you don't know..

39
00:12:27,340 --> 00:12:29,420
I don't know. If I did, why would I ask?

40
00:12:31,140 --> 00:12:33,780
- I told you the other day.
- What did you tell me?

41
00:12:34,020 --> 00:12:37,180
About the school run to the creche.
I could do it by car.

42
00:12:37,620 --> 00:12:39,540
What car are you talking about?

43
00:12:42,700 --> 00:12:45,460
Isn't Servet paying a lump
sum when dad gets out?

44
00:12:45,860 --> 00:12:48,100
- Let's ask for an advance.
- So?

45
00:12:50,300 --> 00:12:54,100
So... We can easily find a
car for 5-6 billion lira.

46
00:12:55,060 --> 00:12:56,500
Don't be ridiculous.

47
00:12:56,900 --> 00:12:59,300
What's ridiculous? It's our money in the end.

48
00:13:01,500 --> 00:13:05,540
Fine, but you think your father
would let us ask for a loan?

49
00:13:06,700 --> 00:13:07,740
No.

50
00:13:08,180 --> 00:13:10,260
Stop talking nonsense then.

51
00:13:11,340 --> 00:13:13,540
Let's just find you another job.

52
00:13:13,820 --> 00:13:15,620
Suppose we give him a surprise?

53
00:13:15,860 --> 00:13:20,260
I do nothing without his permission!
He only gets mad at me later.

54
00:13:23,740 --> 00:13:25,820
How is he? Is he OK?

55
00:13:27,460 --> 00:13:29,940
Was he angry you failed the exam again?

56
00:13:30,380 --> 00:13:31,340
No.

57
00:13:33,740 --> 00:13:36,820
When Recai was out of work
he did that job too.

58
00:13:37,300 --> 00:13:38,540
Who's Recai?

59
00:13:41,060 --> 00:13:42,460
A friend of Fedai's.

60
00:13:43,220 --> 00:13:44,740
And who's Fedai?

61
00:13:46,540 --> 00:13:48,260
Sezai's older brother.

62
00:13:49,540 --> 00:13:52,220
Didn't I tell you to keep
away from Sezai and his lot?

63
00:13:53,900 --> 00:13:55,740
You'll get into trouble one day.

64
00:13:56,700 --> 00:13:58,220
What else can I do?

65
00:14:09,460 --> 00:14:11,860
If you want to work, I'll
find you another job.

66
00:14:12,220 --> 00:14:13,380
I'm not interested.

67
00:14:20,620 --> 00:14:22,060
I'm going to lie down for a bit.

68
00:14:22,820 --> 00:14:25,060
Sit down and eat your food.

69
00:14:48,860 --> 00:14:52,660
Remember you mentioned a job at
Metin's place? Is it still going?

70
00:14:54,260 --> 00:14:55,500
That's too bad.

71
00:14:58,300 --> 00:14:59,460
Whatever...

72
00:15:00,060 --> 00:15:01,380
Keep it in mind.

73
00:15:03,580 --> 00:15:07,020
It's not good for the boy to
loaf about all summer.

74
00:15:07,260 --> 00:15:08,980
He could do something useful.

75
00:15:14,740 --> 00:15:15,940
You're right too.

76
00:15:16,620 --> 00:15:18,620
OK. It can't be helped.

77
00:15:23,100 --> 00:15:27,500
<i>With more than 95 percent
of the ballot boxes now open...</i>

78
00:15:27,740 --> 00:15:31,700
<i>...Recep Tayyip Erdogan's AK Party
appears to have won...</i>

79
00:15:31,940 --> 00:15:35,900
<i>...a landslide victory in
this general election.</i>

80
00:15:41,780 --> 00:15:46,340
<i>So AK Party has significantly
increased its share of the vote...</i>

81
00:15:46,580 --> 00:15:49,180
<i>...compared with the previous election.</i>

82
00:15:50,500 --> 00:15:54,380
<i>In other words, the election result is
already a foregone conclusion.</i>

83
00:16:04,540 --> 00:16:05,900
Ismail...

84
00:16:11,940 --> 00:16:13,060
Are you back?

85
00:16:13,780 --> 00:16:14,820
Yes.

86
00:16:18,780 --> 00:16:20,420
Where have you been all this time?

87
00:16:21,300 --> 00:16:22,580
Hanging out.

88
00:16:23,860 --> 00:16:25,420
Hanging out with friends.

89
00:16:26,300 --> 00:16:27,740
Which friends?

90
00:16:28,100 --> 00:16:29,700
Which friends do you think?

91
00:17:38,580 --> 00:17:39,660
Who?

92
00:17:43,380 --> 00:17:44,700
Put him through.

93
00:17:49,820 --> 00:17:51,260
Thanks.

94
00:17:52,900 --> 00:17:54,940
I see you're in a good mood, Oktay.

95
00:17:56,100 --> 00:17:58,020
I'm falling to pieces here.

96
00:18:00,060 --> 00:18:02,740
No, no. There's nothing to get upset about.

97
00:18:03,580 --> 00:18:04,580
Right, but...

98
00:18:07,780 --> 00:18:10,420
Hey, come on. You're exaggerating.

99
00:18:10,900 --> 00:18:13,380
That's not why I sold my house.

100
00:18:13,660 --> 00:18:15,020
Yes, but...

101
00:18:17,820 --> 00:18:22,260
We said we'd sacrifice everything
for the party, didn't we?

102
00:18:23,660 --> 00:18:25,780
That's politics for you. You live and learn.

103
00:18:26,060 --> 00:18:28,020
Win some, lose some...

104
00:18:28,540 --> 00:18:29,500
Yes.

105
00:18:30,900 --> 00:18:33,100
You're outrageous, you know.

106
00:18:33,340 --> 00:18:35,820
You think I'm making a big deal out of it.

107
00:18:36,060 --> 00:18:38,780
I'm not. Not at all.

108
00:18:39,540 --> 00:18:40,660
No.

109
00:18:41,700 --> 00:18:43,180
Well, we'll see.

110
00:18:44,420 --> 00:18:45,380
OK.

111
00:18:45,900 --> 00:18:49,740
Thanks for calling by the way. Goodbye, then.

112
00:18:51,380 --> 00:18:52,780
Cheap bastard!

113
00:19:07,220 --> 00:19:08,700
Fucking politics...

114
00:19:09,260 --> 00:19:11,300
Fuck your fucking politics...

115
00:19:17,780 --> 00:19:19,300
Fucking politics...

116
00:19:21,900 --> 00:19:23,340
I guess I came at a bad time.

117
00:19:23,980 --> 00:19:25,500
I can come back later.

118
00:19:25,740 --> 00:19:26,620
No, no.

119
00:19:28,020 --> 00:19:29,260
It's OK.

120
00:19:30,340 --> 00:19:31,900
Things are just...

121
00:19:34,020 --> 00:19:35,380
...a bit stressful here.

122
00:19:36,420 --> 00:19:38,860
- I'll go then.
- No. Don't get up.

123
00:19:41,380 --> 00:19:42,340
Excuse me.

124
00:19:46,100 --> 00:19:48,820
If that Oktay ever calls
again, don't put him through.

125
00:19:49,580 --> 00:19:50,580
Say I'm not around.

126
00:19:51,780 --> 00:19:54,580
You put through every call
and send in every visitor.

127
00:19:54,820 --> 00:19:56,620
Didn't I warn you about that?

128
00:19:56,860 --> 00:20:00,500
Didn't I tell you... What?
OK, OK. That's enough.

129
00:20:02,980 --> 00:20:04,460
Stupid girl.

130
00:20:17,900 --> 00:20:19,740
Ey�p knows about this, doesn't he?

131
00:20:21,860 --> 00:20:23,220
No, he doesn't.

132
00:20:40,220 --> 00:20:43,700
<i>I hope you love and aren't loved back...</i>

133
00:20:45,220 --> 00:20:48,500
<i>I hope love hurts you like it hurts me...</i>

134
00:20:49,860 --> 00:20:53,580
<i>I hope you yearn and are never reunited...</i>

135
00:20:55,220 --> 00:20:58,340
<i>...like I was never reunited.</i>

136
00:21:00,060 --> 00:21:03,260
<i>I hope your heart is made to melt...</i>

137
00:21:05,500 --> 00:21:07,780
<i>...just like a candle.</i>

138
00:21:10,060 --> 00:21:13,060
<i>I hope despair is always at your door...</i>

139
00:21:15,340 --> 00:21:17,500
<i>...waiting just like a slave.</i>

140
00:21:19,980 --> 00:21:22,980
<i>I hope your heart is stolen away...</i>

141
00:21:25,540 --> 00:21:28,380
<i>...just like wares from a market stall.</i>

142
00:22:29,780 --> 00:22:30,860
Hacer.

143
00:22:32,340 --> 00:22:33,540
Hacer!

144
00:22:34,740 --> 00:22:36,180
Hacer, it's me.

145
00:22:36,900 --> 00:22:37,980
Hello.

146
00:22:38,580 --> 00:22:39,900
Won't you get in?

147
00:22:40,780 --> 00:22:42,580
Let me give you a lift.

148
00:22:42,980 --> 00:22:45,220
No, there's no need. Don't bother.

149
00:22:45,580 --> 00:22:47,580
No, I'm going your way anyway.

150
00:22:47,820 --> 00:22:49,580
The bus will be here soon. Thanks.

151
00:22:49,820 --> 00:22:52,140
Why wait for the bus? Come on.

152
00:22:52,380 --> 00:22:55,500
No, really. My bus will soon be here.

153
00:22:56,220 --> 00:22:57,500
Don't worry about it.

154
00:22:58,660 --> 00:23:00,940
Come on. Look, it's really no trouble.

155
00:23:01,180 --> 00:23:02,620
Hang on there!

156
00:23:04,940 --> 00:23:07,500
Will you excuse me a moment?

157
00:23:14,700 --> 00:23:17,500
I was just talking to the lady.
We're going right now.

158
00:23:20,540 --> 00:23:22,420
Get in for goodness sake.

159
00:23:22,660 --> 00:23:24,660
We're about to get beaten up. Come on.

160
00:23:26,060 --> 00:23:29,340
Didn't you see those thugs?
In the car back there.

161
00:23:38,940 --> 00:23:45,300
Before the election, the fucker never
spoke to me. Excuse me.

162
00:23:45,540 --> 00:23:50,140
Then as soon as I lose, he's on
the phone to humiliate me.

163
00:23:50,380 --> 00:23:54,460
He gives me this crap about
how he likes me so much.

164
00:23:54,980 --> 00:23:56,820
A jerk called Oktay.

165
00:23:57,740 --> 00:24:00,620
You see, politics get
left to idiots like him.

166
00:24:00,860 --> 00:24:03,500
There's no place for guys
like me in politics.

167
00:24:04,940 --> 00:24:06,980
Then he tries to console me.

168
00:24:07,420 --> 00:24:08,500
What the hell is that?

169
00:24:08,740 --> 00:24:11,060
Why should I need consoling?

170
00:24:12,020 --> 00:24:13,820
He says I'm making a big deal out of it.

171
00:24:14,060 --> 00:24:17,460
Why would I? I'm not that kind of guy.

172
00:24:18,020 --> 00:24:20,580
OK, I put a lot into it.

173
00:24:21,620 --> 00:24:24,700
But for me, politics is
about giving not taking.

174
00:24:25,140 --> 00:24:27,420
OK. It didn't work out. Fine.

175
00:24:29,820 --> 00:24:30,580
Sometimes...

176
00:24:31,860 --> 00:24:33,220
I think...

177
00:24:34,580 --> 00:24:36,140
...maybe it's better that way.

178
00:24:38,460 --> 00:24:41,220
I mean. I think it's for the best.

179
00:24:43,900 --> 00:24:46,980
I'm a very emotional man actually. Really.

180
00:24:48,580 --> 00:24:49,940
It'll sound strange to you...

181
00:24:50,740 --> 00:24:54,380
...but I cry at the slightest thing. Like
when someone reads a poem.

182
00:24:54,700 --> 00:24:55,740
Really.

183
00:25:07,900 --> 00:25:09,180
I should get out around here.

184
00:25:09,620 --> 00:25:10,740
We're pretty close.

185
00:25:11,140 --> 00:25:12,460
Whatever you say.

186
00:25:12,780 --> 00:25:14,100
Oh, please...

187
00:25:14,580 --> 00:25:16,540
Let's find a place to stop.

188
00:25:25,180 --> 00:25:27,820
- Let me give you a hand.
- Thanks.

189
00:25:29,620 --> 00:25:31,180
You shouldn't have bothered.

190
00:25:31,620 --> 00:25:32,700
Hacer...

191
00:25:36,220 --> 00:25:37,940
If you have any problem...

192
00:25:38,260 --> 00:25:39,660
...whatever it is...

193
00:25:41,060 --> 00:25:43,100
...don't hesitate to call me, please.

194
00:25:43,420 --> 00:25:44,540
Thanks.

195
00:25:45,260 --> 00:25:46,700
I'd do anything for you.

196
00:27:26,660 --> 00:27:27,380
Well?

197
00:27:35,740 --> 00:27:36,980
It's sorted.

198
00:27:38,580 --> 00:27:40,420
- Meaning?
- He's going to pay.

199
00:27:45,660 --> 00:27:46,860
Good.

200
00:27:51,940 --> 00:27:53,580
But in a week's time, he said.

201
00:27:55,580 --> 00:27:57,540
That's no problem, as long as he pays...

202
00:28:21,460 --> 00:28:23,660
<i>Do you love me that much Kenan?</i>

203
00:28:30,980 --> 00:28:32,540
<i>More than anything else.</i>

204
00:28:34,180 --> 00:28:35,620
Do I have something here?

205
00:28:35,860 --> 00:28:39,220
Is there a scratch under my eye? You see?

206
00:28:42,980 --> 00:28:43,980
On this side.

207
00:28:44,660 --> 00:28:46,060
It's a bit red.

208
00:28:46,620 --> 00:28:47,740
It hurts.

209
00:28:48,220 --> 00:28:50,300
- It hurts?
- It stings.

210
00:28:50,540 --> 00:28:52,860
- Did you play with it?
- Yes.

211
00:28:53,180 --> 00:28:55,100
- That's why.
- I guess so.

212
00:28:56,380 --> 00:28:58,620
Turn around. Let me see the other side.

213
00:28:58,980 --> 00:29:02,100
- Both sides are red.
- OK.

214
00:31:33,940 --> 00:31:35,660
Is it OK if I don't wear a tie?

215
00:32:57,380 --> 00:32:59,380
Your father's waiting for his cigarettes.

216
00:32:59,620 --> 00:33:02,380
I'll get them at the station. Don't worry.

217
00:33:03,380 --> 00:33:07,020
- The pastries are for him only.
- OK. I'll tell him.

218
00:33:07,460 --> 00:33:08,860
Say hello from me.

219
00:33:09,100 --> 00:33:10,820
Wait. Let me get my thing.

220
00:33:12,460 --> 00:33:15,300
- I'll be off in half an hour.
- OK. Bye then.

221
00:34:39,180 --> 00:34:40,420
A ticket to Muratl�.

222
00:34:42,660 --> 00:34:43,580
Return.

223
00:34:50,420 --> 00:34:51,460
Thanks.

224
00:42:19,500 --> 00:42:20,540
Ismail?

225
00:42:21,740 --> 00:42:23,620
What happened? Why didn't you go?

226
00:42:27,780 --> 00:42:29,020
Why didn't you go?

227
00:42:29,540 --> 00:42:32,100
They called to say the
training was cancelled.

228
00:42:32,540 --> 00:42:33,660
Right.

229
00:42:35,780 --> 00:42:38,060
- Why didn't you go?
- Forget it.

230
00:43:14,620 --> 00:43:16,540
What's the matter with you?

231
00:43:18,980 --> 00:43:20,260
Who was here?

232
00:43:21,260 --> 00:43:23,500
Who do you think? Mehtap dropped by.

233
00:43:25,860 --> 00:43:27,300
Does Mehtap smoke?

234
00:43:28,580 --> 00:43:30,300
She gave up but started again.

235
00:43:31,220 --> 00:43:32,300
Really?

236
00:43:34,940 --> 00:43:36,220
What's the big deal?

237
00:43:38,060 --> 00:43:39,580
Don't lie to me.

238
00:43:39,820 --> 00:43:41,740
- Why...
- I saw Servet leaving the house!

239
00:43:41,980 --> 00:43:43,140
What?

240
00:43:46,140 --> 00:43:48,620
I saw the bastard leaving the house.

241
00:43:53,900 --> 00:43:55,460
He just dropped off the money. Don't!

242
00:43:57,260 --> 00:43:59,020
He only stayed a few minutes.

243
00:44:10,820 --> 00:44:13,180
- Don't lie to me.
- I'm not.

244
00:44:13,420 --> 00:44:14,620
Don't lie!

245
00:44:49,700 --> 00:44:50,940
Where are you going?

246
00:46:11,940 --> 00:46:13,820
Why didn't your mother come?

247
00:46:17,540 --> 00:46:19,580
She had something on at the company.

248
00:46:19,860 --> 00:46:21,260
Some training thing...

249
00:46:21,900 --> 00:46:23,260
I don't know.

250
00:46:25,260 --> 00:46:26,660
What training?

251
00:46:26,900 --> 00:46:28,100
No idea.

252
00:46:31,780 --> 00:46:33,700
- Everything all right at home?
- Yes, sure.

253
00:46:35,700 --> 00:46:37,060
Are you doing OK here?

254
00:46:37,940 --> 00:46:39,300
Yes, fine.

255
00:46:44,380 --> 00:46:47,540
- Are you getting my salary?
- Yes.

256
00:46:48,300 --> 00:46:49,500
Is it you who goes?

257
00:46:49,740 --> 00:46:51,820
Yes, I get it from the secretary.

258
00:47:19,180 --> 00:47:21,500
OK, guys. Visiting hours are over.

259
00:47:23,500 --> 00:47:25,100
Ismail, son...

260
00:47:28,980 --> 00:47:30,980
Do we have anyone else?

261
00:47:32,220 --> 00:47:33,380
How do you mean?

262
00:47:38,340 --> 00:47:39,700
Do we have anyone else?

263
00:47:52,860 --> 00:47:54,100
No.

264
00:47:57,820 --> 00:48:00,220
Off you go then. Don't forget what I said.

265
00:48:00,460 --> 00:48:01,380
I won't.

266
00:48:03,740 --> 00:48:05,820
- Ey�p, come on.
- OK.

267
00:48:06,300 --> 00:48:08,220
- Say hello to your mother. OK?
- I will.

268
00:48:08,460 --> 00:48:09,660
Goodbye.

269
00:48:11,340 --> 00:48:12,700
Dad.

270
00:48:14,180 --> 00:48:15,700
You've got something to say?

271
00:48:17,780 --> 00:48:18,700
No.

272
00:48:19,580 --> 00:48:21,580
Goodbye. See you. That's all.

273
00:51:24,940 --> 00:51:26,100
Brother!

274
00:53:01,380 --> 00:53:02,660
Give me the water.

275
00:53:17,220 --> 00:53:18,580
Here, you drive if you want.

276
00:53:19,500 --> 00:53:20,740
No, you drive.

277
00:53:35,940 --> 00:53:37,340
How much did you pay for this?

278
00:53:38,100 --> 00:53:39,060
Guess.

279
00:53:40,140 --> 00:53:42,140
Forget the guessing. How much did you pay?

280
00:53:42,900 --> 00:53:44,460
Five billion lira, a giveaway.

281
00:53:46,340 --> 00:53:48,820
A giveaway?! What does that mean?

282
00:53:49,900 --> 00:53:52,460
When have you ever had five billion?

283
00:53:53,580 --> 00:53:56,940
I've been in jail nine months
and haven't spent 900 million.

284
00:53:58,300 --> 00:54:00,020
You only came three times.

285
00:54:03,980 --> 00:54:05,340
Why didn't you tell me?

286
00:54:05,580 --> 00:54:08,220
I mean, you were spending five billion.

287
00:54:08,460 --> 00:54:11,860
Whose money was it anyway?
Why didn't you tell me?

288
00:54:13,060 --> 00:54:15,780
- We wanted to give you a surprise.
- Fuck your surprise!

289
00:54:16,020 --> 00:54:19,060
Some surprise! What kind of surprise is that?

290
00:54:35,580 --> 00:54:37,100
Who asked for the money?

291
00:54:42,180 --> 00:54:43,940
- I got it.
- What?

292
00:54:46,860 --> 00:54:49,260
Mum asked for it. I told her to.

293
00:54:50,500 --> 00:54:52,340
You or your mother? Make up your mind.

294
00:54:52,580 --> 00:54:53,860
Mum asked for it.

295
00:54:54,100 --> 00:54:55,460
But you said you got it.

296
00:54:55,700 --> 00:54:58,580
I thought you were asking who got the money.

297
00:54:58,980 --> 00:55:01,020
I hassled mum about the money for the car.

298
00:55:01,260 --> 00:55:04,580
She said OK. And she asked for it.

299
00:55:05,940 --> 00:55:08,500
Did she go to the office?

300
00:55:10,180 --> 00:55:12,740
I guess she arranged it on the phone.

301
00:55:43,020 --> 00:55:45,580
Let's visit your brother on the way home.

302
00:55:48,700 --> 00:55:49,900
OK.

303
00:55:51,540 --> 00:55:54,380
The only thing is, I must
be at the creche at 3.30.

304
00:58:58,220 --> 00:59:01,100
<i>I hope you love and aren't loved back...</i>

305
00:59:03,260 --> 00:59:06,540
<i>I hope love hurts you like it hurts me...</i>

306
00:59:07,980 --> 00:59:11,460
<i>I hope you yearn and are never reunited...</i>

307
00:59:13,140 --> 00:59:15,820
<i>...like I was never reunited.</i>

308
00:59:18,100 --> 00:59:21,500
<i>I hope your heart is made to melt...</i>

309
00:59:23,380 --> 00:59:26,060
<i>...just like a candle.</i>

310
00:59:27,860 --> 00:59:31,380
<i>I hope despair is always at your door...</i>

311
00:59:33,380 --> 00:59:36,300
<i>...waiting just like a slave.</i>

312
00:59:37,900 --> 00:59:41,700
<i>I hope your heart is stolen away...</i>

313
00:59:43,220 --> 00:59:45,860
<i>...just like wares from a market stall.</i>

314
00:59:47,900 --> 00:59:51,100
<i>I hope you choke on your passion...</i>

315
01:00:44,820 --> 01:00:46,380
<i>Private number calling</i>

316
01:00:49,860 --> 01:00:53,460
<i>What do you think you were
doing outside my house?</i>

317
01:00:54,020 --> 01:00:55,620
Who were you calling?

318
01:00:57,820 --> 01:00:59,020
Hello?

319
01:02:52,700 --> 01:02:55,380
I hear it's you who asked for the money.

320
01:02:55,740 --> 01:02:56,900
What money?

321
01:02:58,260 --> 01:02:59,700
What money do you think?

322
01:03:02,820 --> 01:03:04,380
Of course it was me.

323
01:03:05,580 --> 01:03:08,700
The boy got so depressed
after failing the exam.

324
01:03:09,140 --> 01:03:11,100
You know nothing about all that.

325
01:03:15,220 --> 01:03:16,900
Why didn't you tell me?

326
01:03:17,820 --> 01:03:21,540
Ismail didn't want me to.
He said you'd never allow it.

327
01:03:22,620 --> 01:03:24,820
We wanted it to be a surprise.

328
01:03:36,500 --> 01:03:38,340
Did you go to the office?

329
01:03:38,860 --> 01:03:39,860
Yes.

330
01:03:46,580 --> 01:03:47,740
What did you say?

331
01:03:48,900 --> 01:03:51,100
What do you think? I explained the situation.

332
01:03:55,380 --> 01:03:57,700
- Did he pay it right away?
- No.

333
01:04:00,580 --> 01:04:01,740
Well?

334
01:04:02,540 --> 01:04:06,140
Well what? He paid it later.
Why the interrogation?

335
01:04:06,420 --> 01:04:07,940
For goodness sake!

336
01:04:16,780 --> 01:04:18,060
I'll kill you!

337
01:06:07,180 --> 01:06:08,260
Hacer.

338
01:06:10,300 --> 01:06:11,620
Did you miss me?

339
01:06:12,060 --> 01:06:12,860
Yes.

340
01:06:13,100 --> 01:06:14,460
It's been nine months.

341
01:06:18,420 --> 01:06:19,820
Is this new?

342
01:06:21,820 --> 01:06:22,980
Like it?

343
01:06:23,420 --> 01:06:25,340
You never used to wear stuff like that.

344
01:06:25,580 --> 01:06:27,700
I thought I'd give you a surprise.

345
01:06:30,340 --> 01:06:32,100
On the bed then!

346
01:06:40,380 --> 01:06:41,300
Come here.

347
01:06:45,100 --> 01:06:46,860
Let's take this off.

348
01:07:16,420 --> 01:07:17,620
Is something wrong?

349
01:07:18,620 --> 01:07:19,580
No.

350
01:08:07,820 --> 01:08:09,380
What's wrong with you?

351
01:08:16,540 --> 01:08:18,500
- Just stop it.
- What's wrong with you?

352
01:08:19,020 --> 01:08:21,300
- What's wrong with you?
- What's wrong with you?

353
01:08:45,500 --> 01:08:47,500
What's wrong with you?

354
01:08:51,420 --> 01:08:52,620
What's wrong with you?

355
01:09:08,980 --> 01:09:10,220
Fucking bitch!

356
01:10:13,100 --> 01:10:14,300
Hello, everybody.

357
01:10:17,740 --> 01:10:20,060
Good to see you. How are you?

358
01:10:20,300 --> 01:10:21,700
Fine. thanks.

359
01:10:23,620 --> 01:10:26,220
Welcome back. Have a seat.

360
01:10:28,860 --> 01:10:31,180
Bayram! Get Ey�p some tea.

361
01:10:35,940 --> 01:10:37,460
Ey�p, how are you?

362
01:10:37,820 --> 01:10:39,580
Fine. How are you lot?

363
01:11:27,220 --> 01:11:28,780
Ey�p, here you go.

364
01:11:32,380 --> 01:11:33,500
Thanks, Bayram.

365
01:11:33,740 --> 01:11:35,700
How are you? We missed you.

366
01:11:37,180 --> 01:11:39,100
Good. thanks. How are you?

367
01:11:39,900 --> 01:11:42,020
Struggling to survive.

368
01:11:48,380 --> 01:11:51,580
You're still living in this place?

369
01:11:52,420 --> 01:11:54,020
Yes. What can I do?

370
01:11:55,500 --> 01:11:57,340
At least I get peace here.

371
01:11:57,620 --> 01:11:59,700
There's no one to nag me.

372
01:12:02,060 --> 01:12:05,780
I stayed with the relatives once.
But they treated me like shit.

373
01:12:08,100 --> 01:12:09,980
I tried a room-share...

374
01:12:10,900 --> 01:12:12,980
...but the other guys were
nothing but trouble.

375
01:12:15,180 --> 01:12:18,580
M�cahit was nice enough to let me stay here.

376
01:12:19,540 --> 01:12:23,780
I help him out and earn enough to get by.

377
01:12:25,660 --> 01:12:29,060
I don't have parents and
it's so far from home.

378
01:12:39,220 --> 01:12:42,020
<i>I want to ask you something,
if you don't mind.</i>

379
01:12:43,500 --> 01:12:44,900
What? Go on.

380
01:12:47,020 --> 01:12:47,940
What's the matter?

381
01:12:53,020 --> 01:12:54,940
What do you want to ask, Ey�p?

382
01:13:07,100 --> 01:13:08,220
Hello?

383
01:13:08,900 --> 01:13:10,300
No, I'm busy.

384
01:13:10,740 --> 01:13:12,180
No, I can't talk now.

385
01:13:13,180 --> 01:13:14,900
OK. I'll call back later.

386
01:13:15,820 --> 01:13:18,460
Say I'm in a meeting, OK? That's all.

387
01:13:19,220 --> 01:13:20,780
OK. Make something up.

388
01:13:22,300 --> 01:13:24,500
As if it's the first time she's lied.

389
01:13:28,380 --> 01:13:30,300
You said you wanted to ask something.

390
01:13:30,620 --> 01:13:32,420
Did you say you had something to ask?

391
01:13:34,660 --> 01:13:35,460
Thanks.

392
01:13:37,940 --> 01:13:40,500
You've subtracted the money
they borrowed, right?

393
01:13:40,820 --> 01:13:43,700
They don't owe me anything, Ey�p.
We're all square.

394
01:13:48,420 --> 01:13:49,420
Why?

395
01:13:49,940 --> 01:13:51,660
After all you've done for me...

396
01:13:52,100 --> 01:13:54,380
I'm not going to quibble over small change.

397
01:15:45,940 --> 01:15:47,580
What the hell were you doing?

398
01:15:50,500 --> 01:15:52,620
What were you doing outside my house?

399
01:15:53,300 --> 01:15:55,420
Why were you making a face?

400
01:15:55,900 --> 01:15:57,580
What are you trying to do?

401
01:15:58,700 --> 01:16:00,620
Why don't you answer my calls?

402
01:16:00,860 --> 01:16:02,420
Didn't we talk about this?

403
01:16:02,740 --> 01:16:04,940
Didn't we say it was over?

404
01:16:05,540 --> 01:16:08,340
Your husband's out of jail.
You say the boy's suspicious.

405
01:16:08,780 --> 01:16:10,540
Isn't that what we agreed?

406
01:16:12,820 --> 01:16:15,220
I can't help it. I miss you.

407
01:16:15,460 --> 01:16:16,980
- What do you mean?
- I miss you.

408
01:16:17,220 --> 01:16:19,500
Forget the whole thing.

409
01:16:20,260 --> 01:16:22,860
Don't drag it out. It's over. OK?

410
01:16:23,700 --> 01:16:27,420
We made a deal. It's over.
OK, I fancied you too.

411
01:16:28,540 --> 01:16:30,740
- But that's all.
- Really? That's all?

412
01:16:30,980 --> 01:16:31,860
- Yes.
- It's over?

413
01:16:32,100 --> 01:16:33,420
- Of course it's over.
- That's it?

414
01:16:34,420 --> 01:16:36,460
From now on. we go our own ways.

415
01:16:36,860 --> 01:16:37,900
It's not that simple.

416
01:16:38,140 --> 01:16:39,300
Don't be ridiculous.

417
01:16:39,540 --> 01:16:41,060
You're my destiny.

418
01:16:41,300 --> 01:16:43,700
Don't give me that shit. I'll kill you.

419
01:16:43,940 --> 01:16:46,580
I'm not letting you go. I won't let you go.

420
01:16:46,820 --> 01:16:48,300
Fucking maniac!

421
01:16:51,740 --> 01:16:53,580
You don't know what I can be like!

422
01:16:54,300 --> 01:16:56,300
I haven't shown you that side of me.

423
01:16:56,580 --> 01:16:57,620
You don't know me either.

424
01:16:57,860 --> 01:16:59,060
I'll kill you!

425
01:16:59,700 --> 01:17:00,980
Get off.

426
01:17:01,540 --> 01:17:02,700
You maniac!

427
01:17:03,940 --> 01:17:06,180
Your husband, your son...
What the hell is this?

428
01:17:06,540 --> 01:17:07,700
You're like a leech!

429
01:17:08,180 --> 01:17:11,100
Are you sick? Are you all maniacs?

430
01:17:13,700 --> 01:17:14,980
Don't come near me!

431
01:17:18,460 --> 01:17:20,660
If I ever see you outside the house again...

432
01:17:20,980 --> 01:17:23,620
If I ever see you again... OK?

433
01:17:28,660 --> 01:17:29,940
Crazy bitch!

434
01:17:32,740 --> 01:17:33,860
Wait there.

435
01:17:34,740 --> 01:17:36,060
Wait there.

436
01:17:37,060 --> 01:17:39,340
You never listen, do you?

437
01:17:42,260 --> 01:17:44,340
Please don't leave me. Please!

438
01:17:44,580 --> 01:17:46,700
Don't leave me!

439
01:17:47,820 --> 01:17:50,300
- Get off!
- Please, I beg you!

440
01:17:50,700 --> 01:17:52,180
Get off, you pest!

441
01:17:54,220 --> 01:17:55,620
God damn you!

442
01:17:59,820 --> 01:18:01,300
For God's sake!

443
01:21:19,300 --> 01:21:20,380
Who is it?

444
01:21:50,460 --> 01:21:52,220
Ey�p �zt�rk. Come in.

445
01:22:03,020 --> 01:22:04,820
Come on. Hurry up.

446
01:22:16,540 --> 01:22:17,900
Yes, Ey�p...

447
01:22:19,860 --> 01:22:22,420
You've been his driver all these years...

448
01:22:23,500 --> 01:22:27,580
So maybe you have some idea
who could have killed him.

449
01:22:32,020 --> 01:22:33,380
I don't have a clue.

450
01:22:34,700 --> 01:22:35,980
I really don't know.

451
01:22:43,780 --> 01:22:47,580
The last text message on
Servet's phone was from your wife.

452
01:22:48,100 --> 01:22:50,020
What do you say to that?

453
01:22:56,500 --> 01:22:57,620
My wife?

454
01:23:04,500 --> 01:23:06,020
Didn't you know?

455
01:23:09,580 --> 01:23:10,540
No.

456
01:23:10,980 --> 01:23:12,180
Are you sure?

457
01:23:12,660 --> 01:23:13,580
Yes.

458
01:23:16,420 --> 01:23:19,420
So you don't know that
they met after that either.

459
01:23:25,780 --> 01:23:27,060
- Ey�p?
- Yes.

460
01:23:27,300 --> 01:23:28,260
Are you sure?

461
01:26:59,660 --> 01:27:01,220
I did it.

462
01:27:11,140 --> 01:27:12,420
Did what?

463
01:27:50,500 --> 01:27:51,780
No!

464
01:31:42,340 --> 01:31:43,780
Did anyone see?

465
01:31:51,820 --> 01:31:52,980
I don't know.

466
01:32:00,580 --> 01:32:01,980
Go and lie down.

467
01:32:03,540 --> 01:32:04,540
Go on.

468
01:32:50,180 --> 01:32:51,620
Where are you going?

469
01:33:04,100 --> 01:33:05,220
Answer me!

470
01:33:05,460 --> 01:33:07,300
Fuck off! Go to bed!

471
01:33:08,420 --> 01:33:10,460
Or throw yourself off there!

472
01:35:05,660 --> 01:35:08,140
Don't be ridiculous. Get off there.

473
01:37:43,020 --> 01:37:44,340
Bayram!

474
01:38:02,380 --> 01:38:06,220
OK, this teahouse is your home.
That place can be too.

475
01:38:07,900 --> 01:38:09,340
What's the difference?

476
01:38:12,220 --> 01:38:13,940
Winter's on its way, Bayram.

477
01:38:16,420 --> 01:38:18,420
It'll be freezing here at night.

478
01:38:22,100 --> 01:38:26,020
There's heating there. It's warm.
Three meals a day...

479
01:38:32,060 --> 01:38:34,060
You're still young, Bayram.

480
01:38:38,780 --> 01:38:41,460
At least you'll get a lump
sum when you're out.

481
01:38:41,700 --> 01:38:43,380
You can set up a business.

482
01:38:44,180 --> 01:38:46,020
Open a teahouse...

483
01:39:03,540 --> 01:39:04,980
What do you say?



9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.moviesubtitles.org</font>
