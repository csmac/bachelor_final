1 
00:00:01,000 --> 00:00:06,000

    Subtitles by DramaFever
   

2 
00:02:20,960 --> 00:02:30,960

    <i>[Love 'til the End of Summer]</i>
   

3 
00:02:30,960 --> 00:02:34,479

    <i>[Final Episode]</i>
   

4 
00:02:36,719 --> 00:02:40,280

    <i>You must rely on yourselves
    to draw a perfect piece of art.</i>
   

5 
00:02:41,177 --> 00:02:43,439

    Sir, do you have any tips for this?
   

6 
00:02:44,199 --> 00:02:46,479

    Follow your heart.
   

7 
00:02:46,479 --> 00:02:47,879

    That's why...
   

8 
00:02:49,052 --> 00:02:54,719

    You must find what
    you believe to be most beautiful
   

9 
00:02:54,719 --> 00:03:01,520

    and then use art to show everybody
    the world you see through your eyes.
   

10 
00:03:01,520 --> 00:03:04,800

    Sir, where does
    your creative inspiration come from?
   

11 
00:03:07,560 --> 00:03:09,460

    That stretch of camphor trees.
   

12 
00:03:24,262 --> 00:03:26,080

    <i>Xiao Si.</i>
   

13 
00:03:26,080 --> 00:03:32,520

    <i>In the past, I always read about
    the confines of prison.</i>
   

14 
00:03:33,560 --> 00:03:37,280

    <i>After coming here, I've found
    it's actually not that cramped.</i>
   

15 
00:03:38,000 --> 00:03:40,159

    <i>The sky is vast.</i>
   

16 
00:03:40,159 --> 00:03:44,199

    <i>I can still see the white clouds
    floating freely by.</i>
   

17 
00:03:44,199 --> 00:03:48,120

    <i>But, I just can't see you.</i>
   

18 
00:03:49,137 --> 00:03:50,479

    <i>Do you know?</i>
   

19 
00:03:51,639 --> 00:03:53,512

    <i>Whenever I think back on
    those days when</i>
   

20 
00:03:53,512 --> 00:03:56,039

    - Let's go!
    <i>- we biked around Qianchuan...</i>
   

21 
00:03:57,400 --> 00:04:01,879

    <i>the countless times we jumped over
    the school fence to go out to play</i>
   

22 
00:04:03,262 --> 00:04:07,560

    <i>the artistic drawings you had
    strewn all over the art studio</i>
   

23 
00:04:09,280 --> 00:04:14,400

    <i>the yawns I made when
    you started jigsaw puzzles at home...</i>
   

24 
00:04:16,095 --> 00:04:17,839

    <i>When I think of these...</i>
   

25 
00:04:19,095 --> 00:04:21,680

    <i>I feel so sad.</i>
   

26 
00:04:21,680 --> 00:04:26,120

    <i>Xiao Si, there is so much
    I want to say to you</i>
   

27 
00:04:26,120 --> 00:04:29,240

    <i>but I can no longer
    find a chance to tell you.</i>
   

28 
00:04:29,240 --> 00:04:31,759

    <i>Within these four walls</i>
   

29 
00:04:31,759 --> 00:04:36,100

    <i>I often watch the sun set
    in the evening by myself.</i>
   

30 
00:04:37,600 --> 00:04:40,879

    <i>The inmates have their own community.</i>
   

31 
00:04:40,879 --> 00:04:43,920

    <i>They exercise together and eat together.</i>
   

32 
00:04:43,920 --> 00:04:47,439

    <i>However, I'm more used to being alone.</i>
   

33 
00:04:48,199 --> 00:04:52,079

    <i>Oftentimes, I tell myself I'm not sad.</i>
   

34 
00:04:52,887 --> 00:04:56,319

    <i>But when I see autumn drawing to an end</i>
   

35 
00:04:56,319 --> 00:04:58,699

    <i>I still feel faint twinges in my heart.</i>
   

36 
00:05:00,040 --> 00:05:03,079

    <i>Do you think there will be a day</i>
   

37 
00:05:03,079 --> 00:05:06,040

    <i>when a miracle suddenly happens?</i>
   

38 
00:05:06,040 --> 00:05:08,057

    <i>The clock has been turned back</i>
   

39 
00:05:08,057 --> 00:05:10,439

    <i>or perhaps, we receive
    a different fate from the start?</i>
   

40 
00:05:11,800 --> 00:05:16,980

    <i>We would once again...
    lie on the yard together.</i>
   

41 
00:05:25,553 --> 00:05:29,720

    <i>Let the grass tickle our necks</i>
   

42 
00:05:29,720 --> 00:05:34,439

    <i>and the scent of the green grass
    lightly lure us to sleep.</i>
   

43 
00:05:36,678 --> 00:05:38,839

    <i>Let the summer sun</i>
   

44 
00:05:38,839 --> 00:05:43,079

    <i>shine on the eyelids
    covering our bloodshot eyes.</i>
   

45 
00:05:45,800 --> 00:05:49,180

    <i>Do you think there will be such a day?</i>
   

46 
00:05:57,759 --> 00:06:01,160

    <i>[Shanghai Detention Center]</i>
   

47 
00:06:06,879 --> 00:06:09,160

    <i>It's been three years, Zhi Ang.</i>
   

48 
00:06:09,160 --> 00:06:11,399

    <i>We will finally see each other again.</i>
   

49 
00:06:15,639 --> 00:06:17,639

    Hello, CEO Wu?
   

50 
00:06:17,639 --> 00:06:20,040

    I'm sorry. I have something
    very important to do today.
   

51 
00:06:20,040 --> 00:06:21,399

    How about another day?
   

52 
00:06:21,399 --> 00:06:22,879

    All right, bye.
   

53 
00:06:28,137 --> 00:06:30,920

    Xiao Si, has my makeup melted?
   

54 
00:06:30,920 --> 00:06:32,319

    No.
   

55 
00:06:34,360 --> 00:06:36,000

    Do I look beautiful today?
   

56 
00:06:36,000 --> 00:06:38,639

    Yes. This is the fifth time
    you've asked today.
   

57 
00:06:38,639 --> 00:06:40,600

    Beautiful, beautiful!
   

58 
00:06:41,120 --> 00:06:42,959

    No, I have to touch up my makeup.
   

59 
00:06:49,040 --> 00:06:50,879

    Does it look all right?
   

60 
00:06:51,879 --> 00:06:53,800

    What do I do? I'm so nervous.
   

61 
00:06:53,800 --> 00:06:57,120

    Lu Zhi Ang has refused our visits
    throughout these three years.
   

62 
00:06:57,120 --> 00:06:59,040

    Would he have fallen in love
    with someone else?
   

63 
00:06:59,040 --> 00:07:01,879

    No. Don't worry.
   

64 
00:07:05,927 --> 00:07:08,560

    When he comes out,
    I'm going to act perfectly
   

65 
00:07:08,560 --> 00:07:10,399

    so he'll see I have improved.
   

66 
00:07:10,399 --> 00:07:13,439

    Stop being so nervous.
    You're making me nervous too.
   

67 
00:07:18,560 --> 00:07:20,019

    How come he's not out yet?
   

68 
00:08:13,680 --> 00:08:15,319

    Lu Zhi Ang, you bastard!
   

69 
00:08:15,319 --> 00:08:17,040

    Why wouldn't you let me see you?
   

70 
00:08:17,040 --> 00:08:18,959

    Why did you make me wait so long for you?
   

71 
00:08:18,959 --> 00:08:23,040

    Do you think my youth has preservatives?
    I'm about to expire!
   

72 
00:08:24,399 --> 00:08:26,319

    Hug me tighter!
   

73 
00:08:54,678 --> 00:08:55,879

    You're out.
   

74 
00:09:00,720 --> 00:09:02,279

    I've waited a long time.
   

75 
00:09:11,070 --> 00:09:13,919

    How come you've lost weight?
    Did you not have enough to eat in there?
   

76 
00:09:20,440 --> 00:09:23,080

    What's with the "ahs" and "ohs"?
    Are you speaking in Morse code?
   

77 
00:09:23,080 --> 00:09:24,559

    Can't you speak properly?
   

78 
00:09:30,927 --> 00:09:34,679

    All right, all right.
    I know you two have a lot to talk about.
   

79 
00:09:36,000 --> 00:09:39,351

    Seeing as I'll be spending my life
    with this person in the future
   

80 
00:09:39,351 --> 00:09:41,799

    and I'll have him to myself
    for much longer than you will
   

81 
00:09:41,799 --> 00:09:43,440

    I'll give you two one more day.
   

82 
00:09:43,440 --> 00:09:46,919

    24 hours. 86,400 seconds.
   

83 
00:09:47,679 --> 00:09:51,519

    I'll let you tell each other everything
    you couldn't say over these years.
   

84 
00:09:51,519 --> 00:09:52,759

    All right?
   

85 
00:09:54,000 --> 00:09:55,279

    Thank you.
   

86 
00:10:16,399 --> 00:10:19,840

    I remember Qianchuan also had
    many walkways like this.
   

87 
00:10:19,840 --> 00:10:22,360

    They were lined with
    camphor trees on both sides.
   

88 
00:10:23,095 --> 00:10:28,879

    The leaves would sway with the wind,
    swishing and rustling.
   

89 
00:10:28,879 --> 00:10:31,480

    It was like the whole city was singing.
   

90 
00:10:33,799 --> 00:10:35,000

    Yes.
   

91 
00:10:48,840 --> 00:10:50,879

    How long has it been since we've biked?
   

92 
00:10:52,345 --> 00:10:54,039

    Four or five years?
   

93 
00:10:55,970 --> 00:10:59,559

    I remember we often raced back then.
   

94 
00:10:59,559 --> 00:11:01,480

    We'd bike from home to school.
   

95 
00:11:01,480 --> 00:11:04,720

    The loser had to draw
    five drawings of a bust sculpture.
   

96 
00:11:05,759 --> 00:11:09,035

    Maybe the creation of my art books
   

97 
00:11:09,035 --> 00:11:11,559

    is thanks in large part to you
    for beating me so many times.
   

98 
00:11:13,481 --> 00:11:15,720

    You've been winning all this time.
   

99 
00:11:23,480 --> 00:11:26,120

    Do you want to bike again?
   

100 
00:12:04,080 --> 00:12:08,559

    <i>If we could turn around
    and sail back up the river of time</i>
   

101 
00:12:08,559 --> 00:12:12,399

    <i>would everything be different?</i>
   

102 
00:12:20,799 --> 00:12:25,220

    Fu Xiao Si, have you ever thought about
    how we'll be 10 years later?
   

103 
00:12:26,136 --> 00:12:28,113

    10 years later?
   

104 
00:12:28,113 --> 00:12:30,519

    That's too far in the future.
    I've never thought about it.
   

105 
00:12:32,120 --> 00:12:35,879

    I think you will have become
    a real artist in 10 years.
   

106 
00:12:35,879 --> 00:12:39,840

    You'll have numerous best-selling works
    and a bunch of women pursuing you.
   

107 
00:12:42,120 --> 00:12:44,799

    That sounds...
   

108 
00:12:44,799 --> 00:12:47,559

    more like your dream future.
   

109 
00:12:49,803 --> 00:12:52,360

    Do you know what my biggest dream is?
   

110 
00:12:52,886 --> 00:12:56,159

    Your biggest dream?
    How many dreams do you have?
   

111 
00:12:56,159 --> 00:12:57,919

    I have many; big and small ones.
   

112 
00:12:57,919 --> 00:12:59,879

    But there's only one that is the biggest.
   

113 
00:13:01,480 --> 00:13:03,000

    Let me hear it.
   

114 
00:13:04,799 --> 00:13:06,120

    Come here.
   

115 
00:13:11,200 --> 00:13:12,340

    I'm not telling you.
   

116 
00:13:15,639 --> 00:13:17,039

    Lu Zhi Ang!
   

117 
00:13:19,799 --> 00:13:20,940

    You...
   

118 
00:13:22,010 --> 00:13:26,240

    <i>Xiao Si, my biggest dream is</i>
   

119 
00:13:26,240 --> 00:13:30,860

    <i>to stay by your side
    and be your guardian forever.</i>
   

120 
00:13:38,440 --> 00:13:39,960

    Fu Xiao Si.
   

121 
00:13:41,636 --> 00:13:45,000

    Is my life over?
   

122 
00:13:46,600 --> 00:13:49,419

    Don't say such things.
    It's just getting started.
   

123 
00:13:51,519 --> 00:13:53,580

    But the way I am now...
   

124 
00:13:54,440 --> 00:13:56,159

    I have nothing left.
   

125 
00:13:57,720 --> 00:13:59,600

    Don't you still have me?
   

126 
00:13:59,600 --> 00:14:01,259

    I'll be your guardian.
   

127 
00:14:07,927 --> 00:14:10,580

    Okay, I'm going to speed up. Sit tight.
   

128 
00:14:25,511 --> 00:14:29,919

    <i>We always fantasize about
    going back in our lives</i>
   

129 
00:14:29,919 --> 00:14:34,080

    <i>and returning to some point in our past
    to change something</i>
   

130 
00:14:34,080 --> 00:14:37,480

    <i>so that everything would
    turn out differently.</i>
   

131 
00:14:38,678 --> 00:14:42,799

    <i>However, life isn't
    a cassette tape full of songs</i>
   

132 
00:14:42,799 --> 00:14:46,240

    <i>which can be rewound to
    the beginning at any time.</i>
   

133 
00:14:46,240 --> 00:14:52,679

    <i>Real life doesn't have
    a fast-forward, rewind, or pause button.</i>
   

134 
00:14:53,440 --> 00:14:57,000

    <i>What has already happened
    will not change.</i>
   

135 
00:14:57,000 --> 00:15:01,600

    <i>The fortunate thing is
    the future that has yet to happen</i>
   

136 
00:15:01,600 --> 00:15:05,179

    <i>is full of unlimited possibilities.</i>
   

137 
00:15:49,559 --> 00:15:50,840

    We're here.
   

138 
00:15:51,919 --> 00:15:53,080

    We're here.
   

139 
00:16:31,970 --> 00:16:33,200

    Lu Zhi Ang.
   

140 
00:16:34,302 --> 00:16:38,559

    Do you still remember three years ago...
   

141 
00:16:40,159 --> 00:16:42,840

    you gave me a cherry blossom necklace?
   

142 
00:16:43,799 --> 00:16:45,840

    You might not know this.
   

143 
00:16:48,600 --> 00:16:50,279

    Actually...
   

144 
00:16:53,010 --> 00:16:54,379

    That necklace...
   

145 
00:16:56,095 --> 00:16:58,159

    also comes in a matching set.
   

146 
00:16:59,886 --> 00:17:00,980

    Yan Mo.
   

147 
00:17:04,386 --> 00:17:08,059

    All this time, I haven't been
    caring enough about you.
   

148 
00:17:09,119 --> 00:17:11,500

    In the beginning,
    I felt you were unreasonable.
   

149 
00:17:12,761 --> 00:17:14,920

    I thought you weren't soft and gentle.
   

150 
00:17:16,303 --> 00:17:19,559

    I thought you spent too much money
    and was too mischievous.
   

151 
00:17:20,303 --> 00:17:21,579

    But...
   

152 
00:17:22,759 --> 00:17:24,259

    over these years...
   

153 
00:17:26,400 --> 00:17:31,099

    Every time I thought back on it,
    I felt like a jerk.
   

154 
00:17:44,119 --> 00:17:45,900

    Do you want to say something?
   

155 
00:17:49,519 --> 00:17:50,980

    I want to say...
   

156 
00:17:52,554 --> 00:17:54,460

    Would you scorn this Lu Zhi Ang
   

157 
00:17:55,880 --> 00:17:57,859

    who was always mean to you in the past
   

158 
00:17:59,759 --> 00:18:02,000

    who now has nothing
   

159 
00:18:05,599 --> 00:18:07,779

    who can only peel crayfish for you
   

160 
00:18:09,480 --> 00:18:13,519

    and make sweet and sour spareribs?
   

161 
00:18:48,178 --> 00:18:50,440

    Will you marry this jerk?
   

162 
00:18:57,839 --> 00:18:59,119

    Lu Zhi Ang.
   

163 
00:19:03,429 --> 00:19:05,279

    Three years was too long.
   

164 
00:19:10,220 --> 00:19:12,480

    I really missed you.
   

165 
00:19:16,636 --> 00:19:18,039

    In the future...
   

166 
00:19:20,480 --> 00:19:22,599

    Don't make me wait anymore.
   

167 
00:20:49,160 --> 00:20:50,599

    It's too big.
   

168 
00:21:04,470 --> 00:21:06,160

    <i>Is everything ready?</i>
   

169 
00:21:08,636 --> 00:21:12,640

    But... it feels a bit weird.
   

170 
00:21:13,400 --> 00:21:14,839

    <i>How so?</i>
   

171 
00:21:16,095 --> 00:21:17,960

    I'm not sure.
   

172 
00:21:17,960 --> 00:21:22,079

    I just feel like I have
    a lump in my throat.
   

173 
00:21:22,079 --> 00:21:25,400

    <i>You're about to leave your home.
    You must hate to leave.</i>
   

174 
00:21:25,400 --> 00:21:28,079

    <i>Yan Mo, I will take responsibility.</i>
   

175 
00:21:41,095 --> 00:21:44,720

    <i>Mo Mo? Mo Mo?
    Have you gone to bed yet?</i>
   

176 
00:21:44,720 --> 00:21:46,319

    Not yet!
   

177 
00:21:46,319 --> 00:21:49,559

    I can't talk. My dad is here. Bye.
   

178 
00:21:54,429 --> 00:21:55,759

    - Yan Da Zhuang.
    - Mo Mo.
   

179 
00:21:57,559 --> 00:21:59,079

    - So--
    - Tomorrow--
   

180 
00:22:09,480 --> 00:22:12,680

    Yan Da Zhuang, why are you
    standing at the door? Come in.
   

181 
00:22:30,119 --> 00:22:35,279

    To be honest, I've never been
    so nervous before in my whole life.
   

182 
00:22:37,595 --> 00:22:38,759

    Yes.
   

183 
00:22:40,079 --> 00:22:46,160

    I managed to come this far
    with Lu Zhi Ang.
   

184 
00:22:46,160 --> 00:22:48,220

    I almost can't believe it.
   

185 
00:22:51,960 --> 00:22:54,680

    I can't believe it either.
   

186 
00:22:55,970 --> 00:23:01,599

    How come the little girl who was
    acting spoiled by my feet just yesterday
   

187 
00:23:02,720 --> 00:23:04,680

    is already getting married tomorrow?
   

188 
00:23:06,880 --> 00:23:09,319

    How come you've grown up so quickly?
   

189 
00:23:15,559 --> 00:23:20,319

    In the future, if you get hungry,
    then come home for dinner.
   

190 
00:23:22,554 --> 00:23:23,880

    Yes, dinner.
   

191 
00:23:27,599 --> 00:23:32,039

    Mo Mo, why don't I
    braid your hair for you?
   

192 
00:23:36,926 --> 00:23:38,880

    I want it Athena-style.
   

193 
00:23:39,819 --> 00:23:43,640

    Okay, I'll braid them any style you want.
   

194 
00:23:46,640 --> 00:23:47,740

    Dad.
   

195 
00:23:48,928 --> 00:23:54,480

    Do you still remember
    the first time you braided my hair?
   

196 
00:23:58,345 --> 00:24:00,440

    At the time, you nearly
    scolded me to death.
   

197 
00:24:00,440 --> 00:24:04,759

    That was the first in my life
    I felt so useless.
   

198 
00:24:06,511 --> 00:24:08,279

    Almost done, Mo Mo.
   

199 
00:24:09,303 --> 00:24:11,359

    Just bear with me a little longer.
   

200 
00:24:11,359 --> 00:24:13,680

    Okay, almost done.
   

201 
00:24:14,178 --> 00:24:16,400

    That hurt! You yanked on my hair!
   

202 
00:24:16,400 --> 00:24:18,279

    I'm sorry, Mo Mo.
   

203 
00:24:18,279 --> 00:24:21,554

    I'll do it over--
    Look, you moved and it fell apart.
   

204 
00:24:21,554 --> 00:24:23,980

    Mo Mo, don't move around, okay?
   

205 
00:24:26,053 --> 00:24:27,400

    Bear with me.
   

206 
00:24:28,240 --> 00:24:31,720

    Do you know you looked like you were
    about to chew me up and spit me out?
   

207 
00:24:33,761 --> 00:24:36,039

    If I didn't have such high demands of you
   

208 
00:24:36,039 --> 00:24:40,720

    would you have learned to perfect
    your skills to such a masterly level?
   

209 
00:24:42,000 --> 00:24:45,319

    So, going by what you say,
    I should be thanking you?
   

210 
00:24:45,319 --> 00:24:47,279

    Of course.
   

211 
00:24:47,279 --> 00:24:49,359

    Listen up.
   

212 
00:24:49,359 --> 00:24:53,960

    This is the only thing your hands are
    good at. You're bad at everything else.
   

213 
00:24:53,960 --> 00:24:55,559

    It's because of your handiwork that
   

214 
00:24:55,559 --> 00:24:58,943

    I was often criticized by
    teachers in kindergarten.
   

215 
00:24:59,804 --> 00:25:01,720

    Are you done yet?
   

216 
00:25:05,480 --> 00:25:09,119

    Daddy, the teacher gave us homework
    to fold a paper frog.
   

217 
00:25:09,119 --> 00:25:10,480

    A frog?
   

218 
00:25:10,480 --> 00:25:12,079

    Daddy's the best at folding frogs.
   

219 
00:25:12,079 --> 00:25:14,440

    All right, let's fold one together.
   

220 
00:25:15,095 --> 00:25:17,880

    All right. Little frog, done!
   

221 
00:25:17,880 --> 00:25:19,799

    Isn't Daddy amazing?
   

222 
00:25:21,804 --> 00:25:25,759

    Daddy, how come the frog
    isn't jumping? Is it dead?
   

223 
00:25:27,200 --> 00:25:28,440

    It's not dead.
   

224 
00:25:28,440 --> 00:25:30,880

    I'll think of a way to
    make it jump. Watch.
   

225 
00:25:33,846 --> 00:25:36,519

    Isn't it jumping now?
   

226 
00:25:39,679 --> 00:25:41,079

    All right, Athena.
   

227 
00:25:42,000 --> 00:25:43,200

    Have a look.
   

228 
00:25:43,759 --> 00:25:45,259

    Let me see.
   

229 
00:25:50,319 --> 00:25:51,839

    Not bad.
   

230 
00:25:52,440 --> 00:25:54,400

    Your skills haven't slipped.
   

231 
00:25:57,119 --> 00:25:59,720

    How did I make you so pretty?
   

232 
00:26:04,804 --> 00:26:08,920

    It's just a bit of a shame.
    I have to unravel the braids soon.
   

233 
00:26:12,160 --> 00:26:13,400

    It's not a shame.
   

234 
00:26:14,636 --> 00:26:16,019

    This is my...
   

235 
00:26:17,761 --> 00:26:19,799

    last time braiding your hair for you.
   

236 
00:26:24,400 --> 00:26:25,579

    Dad.
   

237 
00:26:31,960 --> 00:26:34,039

    Go to sleep soon.
   

238 
00:26:34,039 --> 00:26:38,660

    You have to be
    the most beautiful bride tomorrow.
   

239 
00:26:44,759 --> 00:26:46,240

    I'm going now.
   

240 
00:26:56,480 --> 00:26:58,559

    Oh, right.
   

241 
00:26:58,559 --> 00:27:00,559

    That is for you.
   

242 
00:27:00,559 --> 00:27:04,240

    But open it after the wedding, okay?
   

243 
00:27:05,839 --> 00:27:06,920

    Okay.
   

244 
00:27:08,599 --> 00:27:11,799

    I also have something for you. Hold on.
   

245 
00:27:16,640 --> 00:27:18,799

    This is for you.
   

246 
00:27:18,799 --> 00:27:20,900

    Wait until after the wedding
    to open it, too.
   

247 
00:27:29,960 --> 00:27:31,559

    Okay.
   

248 
00:27:31,559 --> 00:27:33,079

    I'm going now.
   

249 
00:27:33,079 --> 00:27:34,599

    Go to sleep.
   

250 
00:27:38,200 --> 00:27:39,680

    Goodnight, Dad.
   

251 
00:27:58,240 --> 00:28:02,440

    <i>Daddy, I won't call you
    Yan Da Zhuang today.</i>
   

252 
00:28:02,440 --> 00:28:07,279

    <i>I usually only say, "Yan Da Zhuang,
    my card has hit the limit."</i>
   

253 
00:28:07,279 --> 00:28:10,440

    <i>"Yan Da Zhuang, I bought another bag."</i>
   

254 
00:28:10,440 --> 00:28:13,440

    <i>"Yan Da Zhuang,
    Yan Da Zhuang, Yan Da Zhuang!"</i>
   

255 
00:28:14,240 --> 00:28:19,039

    <i>It's been hard on you all these years
    to have such an unruly daughter.</i>
   

256 
00:28:19,759 --> 00:28:22,640

    <i>Now that I'm about to
    be married, I realized</i>
   

257 
00:28:22,640 --> 00:28:25,480

    <i>I seldom stopped to see
    how you were doing.</i>
   

258 
00:28:25,480 --> 00:28:28,240

    <i>This is what I feel most guilty about.</i>
   

259 
00:28:28,240 --> 00:28:31,680

    <i>Daddy, when I was young,
    I was always opposed to</i>
   

260 
00:28:31,680 --> 00:28:33,440

    <i>you finding a stepmother for me.</i>
   

261 
00:28:33,440 --> 00:28:36,160

    <i>You've held yourself back
    for my sake.</i>
   

262 
00:28:36,160 --> 00:28:38,119

    <i>You've done so much for me.</i>
   

263 
00:28:38,119 --> 00:28:40,599

    <i>Now, I finally understand a little bit.</i>
   

264 
00:28:41,480 --> 00:28:44,680

    <i>I know that finding someone you could
    spend the rest of your life with</i>
   

265 
00:28:44,680 --> 00:28:47,119

    <i>is a very blessed thing.</i>
   

266 
00:28:47,119 --> 00:28:51,079

    <i>That's why I hope you can find
    that someone too.</i>
   

267 
00:28:51,079 --> 00:28:53,240

    <i>Stop worrying about what I think.</i>
   

268 
00:28:53,240 --> 00:28:55,640

    <i>Live your life for yourself.</i>
   

269 
00:29:05,386 --> 00:29:08,160

    <i>My one and only beloved baby.</i>
   

270 
00:29:08,160 --> 00:29:13,559

    <i>I still remember you cried
    the whole night on your sixth birthday.</i>
   

271 
00:29:13,559 --> 00:29:16,859

    <i>Every time I recall that,
    I feel my cheeks burning.</i>
   

272 
00:29:17,846 --> 00:29:19,799

    <i>In the past 27 years</i>
   

273 
00:29:19,799 --> 00:29:24,759

    <i>I hope you can forgive me for
    any negligence in caring for you.</i>
   

274 
00:29:24,759 --> 00:29:27,140

    <i>This is my first time being a father.</i>
   

275 
00:29:28,279 --> 00:29:32,160

    <i>Even though I want to say
    I don't feel sad at all</i>
   

276 
00:29:32,160 --> 00:29:34,759

    <i>I can't manage to say the words.</i>
   

277 
00:29:34,759 --> 00:29:37,839

    <i>Seeing how beautiful you look
    in a wedding dress</i>
   

278 
00:29:37,839 --> 00:29:40,920

    <i>I feel as if I am about to lose you.</i>
   

279 
00:29:40,920 --> 00:29:44,759

    <i>Yan Mo, since the moment you were born</i>
   

280 
00:29:44,759 --> 00:29:47,079

    <i>I have been preparing for this day.</i>
   

281 
00:29:47,079 --> 00:29:50,759

    <i>Even so, I feel as if it is all
    happening too soon.</i>
   

282 
00:29:52,200 --> 00:29:54,480

    <i>However, I am proud of you.</i>
   

283 
00:29:54,480 --> 00:29:56,799

    <i>You will inevitably need to
    live your own life.</i>
   

284 
00:29:56,799 --> 00:30:01,400

    <i>I believe my daughter
    will be able to be happy.</i>
   

285 
00:30:02,039 --> 00:30:08,000

    <i>Last of all, having you was
    the greatest accomplishment of my life.</i>
   

286 
00:30:22,261 --> 00:30:23,319

    <i>Mo Mo!</i>
   

287 
00:30:23,319 --> 00:30:25,519

    Mo Mo, Daddy's back--
   

288 
00:30:47,319 --> 00:30:48,640

    Mo Mo.
   

289 
00:30:50,010 --> 00:30:51,200

    Mo Mo?
   

290 
00:30:52,010 --> 00:30:53,440

    Daddy.
   

291 
00:30:54,886 --> 00:30:56,440

    Were you waiting for me?
   

292 
00:30:59,079 --> 00:31:03,640

    I'm sorry. I was just too busy today.
   

293 
00:31:04,386 --> 00:31:05,599

    I'm back late.
   

294 
00:31:06,386 --> 00:31:07,940

    Happy birthday, baby.
   

295 
00:31:09,053 --> 00:31:11,053

    Look at the birthday present
    I have for you.
   

296 
00:31:11,053 --> 00:31:13,599

    Is it a pink princess dress?
   

297 
00:31:32,960 --> 00:31:37,640

    Yan Da Zhuang, why are you
    being sentimental all of a sudden?
   

298 
00:31:37,640 --> 00:31:39,119

    How annoying.
   

299 
00:31:59,839 --> 00:32:01,160

    Are you asleep?
   

300 
00:32:03,303 --> 00:32:04,680

    What is it?
   

301 
00:32:04,680 --> 00:32:07,259

    You have to get up early tomorrow.
    Go to sleep.
   

302 
00:32:08,599 --> 00:32:11,000

    This is my last night being single.
   

303 
00:32:11,000 --> 00:32:12,900

    Aren't you going to chat with me?
   

304 
00:32:15,160 --> 00:32:16,579

    What do you want to talk about?
   

305 
00:32:18,720 --> 00:32:21,559

    Oh, right. I have something fun.
   

306 
00:32:30,119 --> 00:32:31,279

    Do you still remember?
   

307 
00:32:31,279 --> 00:32:34,359

    There was a period when I was really
    into astronomy so you gave this to me.
   

308 
00:32:39,359 --> 00:32:42,579

    I remember your childhood dream
    wasn't to be an artist.
   

309 
00:32:44,720 --> 00:32:48,359

    I wanted to be
    an aerospace engineer like my dad.
   

310 
00:32:48,359 --> 00:32:50,839

    However, my mom
    quickly killed that notion.
   

311 
00:32:52,599 --> 00:32:55,720

    It seems neither of us
    accomplished our initial dreams.
   

312 
00:32:56,595 --> 00:33:00,039

    But, we've both become better people.
   

313 
00:33:05,559 --> 00:33:07,200

    People are weird.
   

314 
00:33:07,200 --> 00:33:09,240

    When we're young, we want to grow up
   

315 
00:33:09,240 --> 00:33:12,279

    but when we're grown up,
    we miss our childhood days.
   

316 
00:33:17,799 --> 00:33:20,779

    It's all in the past. We can't go back.
   

317 
00:33:22,926 --> 00:33:27,480

    We can't go back to the past
    but the future is still up in the air.
   

318 
00:33:31,554 --> 00:33:35,039

    I invited Li Xia to tomorrow's wedding.
   

319 
00:33:38,599 --> 00:33:42,000

    I don't believe this is how
    the two of you will end.
   

320 
00:33:43,440 --> 00:33:46,960

    Do you think she will come tomorrow?
   

321 
00:34:05,220 --> 00:34:09,559

    <i>Li Xia, this world is too big.</i>
   

322 
00:34:09,559 --> 00:34:12,800

    <i>It's so big that my voice can turn hoarse
    from shouting in this sea of people</i>
   

323 
00:34:12,800 --> 00:34:14,880

    <i>and I still won't receive
    a reply from you.</i>
   

324 
00:34:16,302 --> 00:34:18,599

    <i>But sometimes, I think that</i>
   

325 
00:34:18,599 --> 00:34:23,840

    <i>in some corner of the world, you must
    also be doing the same thing I am</i>
   

326 
00:34:23,840 --> 00:34:27,940

    <i>and looking up at the same starry sky.</i>
   

327 
00:34:30,010 --> 00:34:34,000

    <i>I haven't bothered you again
    all these years</i>
   

328 
00:34:34,000 --> 00:34:38,760

    <i>but I have left
    an empty space in my heart</i>
   

329 
00:34:38,760 --> 00:34:43,000

    <i>and I am waiting for us to
    one day meet in due course.</i>
   

330 
00:35:06,400 --> 00:35:09,880

    <i>[Groom: Lu Zhi Ang
    Bride: Yan Mo]</i>
   

331 
00:35:22,320 --> 00:35:24,239

    Are you nervous?
   

332 
00:35:25,000 --> 00:35:27,039

    I'm not nervous.
   

333 
00:35:27,039 --> 00:35:28,340

    As if!
   

334 
00:35:31,119 --> 00:35:33,159

    Are you nervous too?
   

335 
00:35:33,159 --> 00:35:36,519

    Why would I be nervous?
    You're the one getting married, not me.
   

336 
00:35:38,519 --> 00:35:40,900

    Maybe she will come.
   

337 
00:35:51,719 --> 00:35:54,480

    I think they should have two kids.
   

338 
00:35:54,480 --> 00:35:55,719

    Two would be nice.
   

339 
00:35:55,719 --> 00:35:58,400

    A boy and a girl to make a perfect pair.
   

340 
00:35:59,679 --> 00:36:01,039

    That's what I think too.
   

341 
00:36:01,039 --> 00:36:03,119

    Great minds think alike.
   

342 
00:36:09,387 --> 00:36:13,840

    Yu Jian, what's the status with
    you and Qing Tian?
   

343 
00:36:13,840 --> 00:36:16,800

    He's pursued you for so long now.
   

344 
00:36:19,039 --> 00:36:21,800

    We tried but it still didn't work out.
   

345 
00:36:23,679 --> 00:36:28,280

    You'd just rather be alone than
    put up with anyone.
   

346 
00:36:36,639 --> 00:36:37,920

    - Look up there.
    - Jun Bao.
   

347 
00:36:37,920 --> 00:36:40,137

    What is 34 plus 27?
   

348 
00:36:40,137 --> 00:36:41,300

    88.
   

349 
00:36:42,010 --> 00:36:45,599

    It's obvious it was the gym teacher
    who taught you mathematics.
   

350 
00:36:50,387 --> 00:36:53,800

    Aren't these our seniors?
   

351 
00:36:53,800 --> 00:36:55,880

    I still remember that tennis match.
   

352 
00:36:55,880 --> 00:36:59,559

    What was it you said back then?
    If you lost, you'd eat what?
   

353 
00:36:59,559 --> 00:37:01,320

    What were you going to eat?
   

354 
00:37:01,320 --> 00:37:03,840

    I'm now a role model for others.
   

355 
00:37:03,840 --> 00:37:06,440

    That was the greatest embarrassment
    of my life.
   

356 
00:37:06,440 --> 00:37:08,297

    What high places
    are you working in nowadays?
   

357 
00:37:08,297 --> 00:37:10,679

    I'm not fit for high places
    but I won't settle for less.
   

358 
00:37:15,760 --> 00:37:18,579

    Mr. Lu Zhi Ang,
    there's a package for you.
   

359 
00:37:20,079 --> 00:37:21,280

    Thank you.
   

360 
00:37:38,440 --> 00:37:44,719

    <i>Zhi Ang, time is a mighty author.
    It can pen a perfect ending.</i>
   

361 
00:37:44,719 --> 00:37:47,559

    <i>Congratulations and best wishes
    to you and Yan Mo.</i>
   

362 
00:37:47,559 --> 00:37:50,960

    <i>Greetings from afar, Cheng Qi Qi.</i>
   

363 
00:38:12,177 --> 00:38:14,739

    May the groom please enter.
   

364 
00:38:22,239 --> 00:38:24,840

    May the bride please enter.
   

365 
00:39:46,052 --> 00:39:49,199

    Groom and bride, please face each other.
   

366 
00:40:01,469 --> 00:40:06,079

    Mr. Lu Zhi Ang, please make a vow
    in front of all present.
   

367 
00:40:06,079 --> 00:40:11,000

    Do you take Ms. Yan Mo to be your wife?
   

368 
00:40:11,440 --> 00:40:12,679

    I do.
   

369 
00:40:13,554 --> 00:40:18,239

    Ms. Yan Mo, please make a vow
    in front of all present.
   

370 
00:40:18,239 --> 00:40:23,119

    Do you take Mr. Lu Zhi Ang
    to be your husband?
   

371 
00:40:24,512 --> 00:40:26,039

    I do.
   

372 
00:40:39,719 --> 00:40:43,360

    Groom and bride, please exchange rings.
   

373 
00:42:04,840 --> 00:42:05,927

    Give me a second.
   

374 
00:42:05,927 --> 00:42:07,846

    I want the bouquet! Toss it to me!
   

375 
00:42:07,846 --> 00:42:09,559

    I want to get married!
   

376 
00:42:09,559 --> 00:42:11,920

    What's the delay? I want to get married.
   

377 
00:42:13,969 --> 00:42:16,760

    It's my big day today.
    Why do you look so miserable?
   

378 
00:42:16,760 --> 00:42:18,239

    Come catch the bouquet.
   

379 
00:42:18,239 --> 00:42:19,760

    I'll pass.
   

380 
00:42:24,280 --> 00:42:28,521

    Okay, three, two, one!
   

381 
00:43:00,387 --> 00:43:10,219

    Fu Xiao Si...
   

382 
00:43:10,219 --> 00:43:11,469

    Everybody, come take pictures!
   

383 
00:43:11,469 --> 00:43:13,900

    - Time for pictures!
    - Pictures!
   

384 
00:43:23,429 --> 00:43:25,480

    One.
   

385 
00:43:25,480 --> 00:43:27,320

    Two.
   

386 
00:43:28,094 --> 00:43:29,519

    Three!
   

387 
00:44:11,969 --> 00:44:14,320

    <i>There will inevitably be someone
    to take our place</i>
   

388 
00:44:14,320 --> 00:44:18,440

    <i>to carry the torch of youthfulness
    in their time of youth.</i>
   

389 
00:44:18,440 --> 00:44:20,000

    <i>There will inevitably come a day</i>
   

390 
00:44:20,000 --> 00:44:24,639

    <i>when the Qianchuan we know
    will be buried beneath layers by time</i>
   

391 
00:44:24,639 --> 00:44:27,599

    <i>turning into something different
    from our memories.</i>
   

392 
00:44:27,599 --> 00:44:32,360

    <i>All stories eventually come to an end.</i>
   

393 
00:44:33,679 --> 00:44:38,800

    <i>All that is left is
    a continuous line of camphor trees.</i>
   

394 
00:44:38,800 --> 00:44:43,960

    <i>When the annual wet monsoon
    sweeps past these treetops</i>
   

395 
00:44:43,960 --> 00:44:48,480

    <i>they silently deliver
    those secrets held in our hearts.</i>
   

396 
00:45:05,119 --> 00:45:08,519

    <i>Those boys taught me about maturity.
    [Love 'til the End of Summer]</i>
   

397 
00:45:08,519 --> 00:45:11,800

    <i>Those girls taught me about love.
    [Love 'til the End of Summer]</i>
   

398 
00:45:13,760 --> 00:45:18,760

    Subtitles by DramaFever
   

