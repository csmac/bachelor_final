1
00:00:01,000 --> 00:00:06,000
Subtitles by DramaFever

2
00:01:38,000 --> 00:01:40,000
<i>[Episode 48]</i>

3
00:02:02,126 --> 00:02:04,373
All it takes is one stab from Father.

4
00:02:05,813 --> 00:02:11,301
Then I'll be free from feeling
any concern for you and the people.

5
00:02:12,298 --> 00:02:13,790
- Kill him!
- Kill him!

6
00:02:13,790 --> 00:02:16,074
- Your Majesty, kill him!
- Kill him!

7
00:02:16,074 --> 00:02:18,199
- You must kill him!
- Kill him!

8
00:02:18,199 --> 00:02:22,650
- Kill him!
- Kill him!

9
00:02:22,650 --> 00:02:25,908
<i>- Kill her!
- Kill her!</i>

10
00:02:25,908 --> 00:02:27,893
- Kill him!
- Kill him!

11
00:02:27,893 --> 00:02:31,522
<i>- Kill her!
- Kill her!</i>

12
00:02:31,522 --> 00:02:33,745
- Kill him!
- Kill him!

13
00:02:33,745 --> 00:02:36,419
<i>- Kill her!
- Kill her!</i>

14
00:02:36,419 --> 00:02:38,344
- Kill him!
- Kill him!

15
00:02:38,344 --> 00:02:42,409
<i>- Kill her!
- Kill her!</i>

16
00:02:42,409 --> 00:02:43,409
Shut up!

17
00:02:48,243 --> 00:02:49,562
I...

18
00:02:50,591 --> 00:02:53,258
I once did this to your mother.

19
00:02:53,258 --> 00:02:54,867
I know about it.

20
00:02:55,668 --> 00:02:56,668
However...

21
00:02:59,379 --> 00:03:00,741
I was wrong.

22
00:03:02,539 --> 00:03:03,842
Your Majesty!

23
00:03:03,842 --> 00:03:05,412
The nation should come first!

24
00:03:08,116 --> 00:03:09,716
Your Majesty!

25
00:03:09,716 --> 00:03:12,282
The nation should be your priority!

26
00:03:18,377 --> 00:03:19,643
Father.

27
00:03:21,391 --> 00:03:23,223
What did you just say?

28
00:03:24,548 --> 00:03:25,706
I said...

29
00:03:29,794 --> 00:03:31,467
I was wrong.

30
00:03:37,258 --> 00:03:41,679
For the sake of the Duan Empire,
for you and my people

31
00:03:43,044 --> 00:03:45,222
I already killed
my most beloved woman.

32
00:03:46,222 --> 00:03:49,331
How would you have me
place the nation as my priority?

33
00:03:52,157 --> 00:03:53,786
Whenever I assigned you tasks

34
00:03:53,786 --> 00:03:56,877
all of you put
your personal interests first.

35
00:03:56,877 --> 00:03:58,341
I...

36
00:04:01,377 --> 00:04:07,435
I now make a small request of you
for my own sake.

37
00:04:11,038 --> 00:04:14,263
Don't make me kill my own son.

38
00:04:17,014 --> 00:04:18,999
Born as my son!

39
00:04:20,000 --> 00:04:22,266
Our woe and well-being are destined!

40
00:04:22,266 --> 00:04:24,158
Whatever he does...

41
00:04:25,740 --> 00:04:27,365
I will bear the consequences.

42
00:04:50,495 --> 00:04:52,595
Crown Prince Muyun Sheng!

43
00:04:52,595 --> 00:04:54,622
He suggested participating
in political affairs.

44
00:04:54,622 --> 00:04:55,970
I consent to it!

45
00:04:55,970 --> 00:04:58,990
From this day forth,
he shall govern by my side.

46
00:05:06,882 --> 00:05:08,416
Throughout my life...

47
00:05:08,416 --> 00:05:10,880
I've been cautious with
my words and actions.

48
00:05:12,259 --> 00:05:19,127
Now, I'd like to know what
the Great Duan Empire will become

49
00:05:19,127 --> 00:05:25,158
if there's a future emperor
who doesn't obey any of you.

50
00:05:28,567 --> 00:05:32,016
Your Majesty, this must not be done!

51
00:05:37,959 --> 00:05:43,598
I thank Your Highness for this manor.

52
00:05:44,892 --> 00:05:48,920
His Majesty has allowed that monster
to attend the royal court.

53
00:05:48,920 --> 00:05:51,550
That monster can now wield
magical powers.

54
00:05:51,550 --> 00:05:54,797
He's even supported fully by His Majesty.

55
00:05:55,959 --> 00:06:02,439
If I were still young, I'd risk my life
to take him on for Your Highness!

56
00:06:03,925 --> 00:06:05,875
It's such a pity.

57
00:06:05,875 --> 00:06:07,324
I'm old now.

58
00:06:07,324 --> 00:06:14,403
I'm just waiting to see how I can live
until I retire to my hometown.

59
00:06:19,117 --> 00:06:21,115
Just take it anyway.

60
00:06:23,232 --> 00:06:25,572
I'm already useless to Your Highness.

61
00:06:25,572 --> 00:06:29,327
I can't accept payment
for a job I didn't accomplish.

62
00:06:37,858 --> 00:06:40,185
How painful.

63
00:06:40,185 --> 00:06:46,978
You mean to say that Muyun Sheng...
is scarier than I am?

64
00:06:49,425 --> 00:06:52,434
Master Xue...

65
00:06:53,814 --> 00:07:00,237
You're always trying to play so nice,
you can't even get anything done.

66
00:07:08,593 --> 00:07:10,391
Master Xue...

67
00:07:10,391 --> 00:07:14,156
You mustn't blame me.

68
00:07:15,173 --> 00:07:16,173
I...

69
00:07:17,177 --> 00:07:19,218
I'm just angry.

70
00:07:19,218 --> 00:07:21,088
I'm mad at you.

71
00:07:21,088 --> 00:07:24,105
Why haven't you understood the situation?

72
00:07:24,105 --> 00:07:28,670
If you wish to live long,
you have to fight for it.

73
00:07:28,670 --> 00:07:30,735
You can't just hide away.

74
00:07:30,735 --> 00:07:37,620
If you keep on hiding, your own nest
will get taken away in the end.

75
00:08:15,959 --> 00:08:17,886
Your Highness's gift...

76
00:08:19,454 --> 00:08:22,764
I shall hang it up in
the main hall of my residence.

77
00:08:22,764 --> 00:08:29,843
My descendants shall then understand
that Your Highness watches over them.

78
00:08:32,268 --> 00:08:34,020
Look.

79
00:08:34,900 --> 00:08:36,025
Master Xue...

80
00:08:36,025 --> 00:08:38,135
You bled out so much.

81
00:08:38,135 --> 00:08:40,254
You're old now.

82
00:08:40,254 --> 00:08:42,813
You must be careful.

83
00:08:45,044 --> 00:08:47,393
Apply some good medication on it.

84
00:08:47,393 --> 00:08:49,227
This hand...

85
00:08:49,227 --> 00:08:52,159
You have to use it to write reports.

86
00:08:56,986 --> 00:08:58,895
Take care, Master Xue.

87
00:08:58,895 --> 00:09:01,833
Your carriage is waiting outside.

88
00:09:33,225 --> 00:09:34,913
The snow in Han Prefecture...

89
00:09:34,913 --> 00:09:37,181
It's falling heavily?

90
00:09:38,238 --> 00:09:39,940
The snowstorm has caused disasters.

91
00:09:39,940 --> 00:09:42,765
There was news sent from Tian Tuo Strait.

92
00:09:42,765 --> 00:09:45,028
They agree with our suggestions.

93
00:09:45,028 --> 00:09:48,490
They have already sent
a letter to Master Xue.

94
00:09:52,796 --> 00:09:55,789
Suoda Meng is too cunning.

95
00:09:55,789 --> 00:09:58,090
I like him a lot.

96
00:10:00,596 --> 00:10:02,558
This won't be an easy task.

97
00:10:02,558 --> 00:10:05,123
In order to win Suoda Meng over

98
00:10:05,123 --> 00:10:09,518
I even had to trouble
my godfather in Glass Valley.

99
00:10:09,518 --> 00:10:13,999
My godfather ordered the salt merchants
to give Suoda Meng

100
00:10:13,999 --> 00:10:16,764
20 percent off the price.

101
00:10:16,764 --> 00:10:18,224
That was how he sealed the deal.

102
00:10:19,277 --> 00:10:21,777
There are hundreds of thousands
of people in Han Prefecture.

103
00:10:21,777 --> 00:10:25,395
20 percent off the price of salt
is quite a huge loss.

104
00:10:29,187 --> 00:10:35,071
Father, how will you reward me?

105
00:10:37,570 --> 00:10:39,567
What would you like?

106
00:10:40,775 --> 00:10:42,884
I want you to say that I did well.

107
00:10:47,618 --> 00:10:51,303
Come here... and I'll say it to you.

108
00:11:03,216 --> 00:11:05,676
You've been to visit Muyun Sheng?

109
00:11:12,523 --> 00:11:13,730
His Majesty...

110
00:11:15,110 --> 00:11:17,360
That day he was supposed to
visit Wei Ping Residence...

111
00:11:19,464 --> 00:11:21,596
I went to take a look.

112
00:11:24,279 --> 00:11:26,264
You went to take a look?

113
00:11:27,384 --> 00:11:30,662
You even told them that
I had sent you there!

114
00:11:40,221 --> 00:11:43,633
Father, you have spies
in Wei Ping Residence as well?

115
00:11:48,629 --> 00:11:53,835
Since you're so... capable

116
00:11:53,835 --> 00:11:57,057
that you provoked Muyun Sheng
into sabotaging my plan

117
00:11:58,057 --> 00:11:59,057
you...

118
00:12:00,888 --> 00:12:05,712
You should put your ability
to better use again.

119
00:12:23,326 --> 00:12:25,284
Father, what would you like me to do?

120
00:12:27,331 --> 00:12:29,393
Muyun Sheng...

121
00:12:29,393 --> 00:12:31,999
He shouldn't be alive.

122
00:12:32,916 --> 00:12:35,489
<i>Assassinating the crown prince
will get our whole clan killed!</i>

123
00:12:41,871 --> 00:12:45,500
Therefore, you have to be
all the more careful.

124
00:12:47,626 --> 00:12:51,402
I can only feel at ease
if you carry out this task.

125
00:12:53,777 --> 00:12:56,818
We're just trying to cause
trouble in the palace.

126
00:12:56,818 --> 00:13:01,083
Isn't it better to keep him around?

127
00:13:01,083 --> 00:13:03,432
<i>Those who can't be manipulated...</i>

128
00:13:03,432 --> 00:13:07,909
<i>The more capable they are,
the more we can't let them live.</i>

129
00:13:07,909 --> 00:13:11,969
These words were also meant for you.

130
00:13:11,969 --> 00:13:19,885
The sooner you get started on this,
the sooner Muyun Sheng will die

131
00:13:19,885 --> 00:13:25,365
and Muyun He Ge is
released from captivity.

132
00:13:28,239 --> 00:13:32,261
Father, you've been making many visits
during the past year or so.

133
00:13:32,261 --> 00:13:37,913
You hope to see Muyun He Ge set free.

134
00:13:39,798 --> 00:13:41,796
Why exactly is that?

135
00:13:42,379 --> 00:13:43,760
He's useful to me.

136
00:13:43,760 --> 00:13:45,187
What use is he to you?

137
00:13:45,187 --> 00:13:47,690
If you wish to fulfill your dream

138
00:13:47,690 --> 00:13:50,326
you can just choose to support
one of the young princes.

139
00:13:50,326 --> 00:13:52,456
Why does it have to be Muyun He Ge?

140
00:13:54,937 --> 00:13:56,235
It's because...

141
00:13:56,235 --> 00:13:59,461
- the empress--
- The Nanku family was wiped out!

142
00:13:59,461 --> 00:14:03,379
Even the empress only gets to survive
by pretending to be another woman.

143
00:14:03,379 --> 00:14:04,913
What use is she to you?

144
00:14:04,913 --> 00:14:06,826
Father...

145
00:14:08,075 --> 00:14:16,273
I've heard of a rumor about Father
and a lady of the Nanku family.

146
00:14:21,875 --> 00:14:23,880
You don't have to do it.

147
00:14:23,880 --> 00:14:26,782
I can have someone else replace you.

148
00:14:26,782 --> 00:14:28,673
For instance...

149
00:14:28,673 --> 00:14:33,623
your eldest brother has been in
Chiyang County for a long time.

150
00:14:33,623 --> 00:14:35,188
He also told me that--

151
00:14:35,188 --> 00:14:36,514
When do I have to do it?

152
00:14:37,756 --> 00:14:40,166
<i>When should you take action?</i>

153
00:14:40,166 --> 00:14:42,561
It's up to you to decide.

154
00:14:43,583 --> 00:14:45,118
Father...

155
00:14:47,797 --> 00:14:50,109
I have a question for you.

156
00:14:53,336 --> 00:14:56,571
Father, you know that
Muyun Sheng can wield magical powers.

157
00:15:00,447 --> 00:15:02,628
Have you even...

158
00:15:04,889 --> 00:15:07,514
Have you even worried about my safety?

159
00:15:13,273 --> 00:15:14,686
What do you think?

160
00:15:15,783 --> 00:15:23,331
Aren't you so good at provoking others
into doing what you want?

161
00:15:41,395 --> 00:15:43,192
I understand.

162
00:15:44,528 --> 00:15:45,745
Who is it?

163
00:15:58,393 --> 00:16:01,934
It's so late. You're still out
running errands for Prince De?

164
00:16:05,559 --> 00:16:06,794
Come here.

165
00:16:29,813 --> 00:16:32,350
Prince De, don't you ever knock
before entering a room?

166
00:16:33,400 --> 00:16:35,907
That's not what a prince should do.

167
00:16:37,913 --> 00:16:39,687
You were eavesdropping on us?

168
00:16:47,263 --> 00:16:49,361
So what if I was?

169
00:16:49,361 --> 00:16:51,572
So what if I wasn't?

170
00:16:55,735 --> 00:16:59,316
My father has blamed me
for saving your life.

171
00:16:59,316 --> 00:17:03,500
Do you know the effort I had to make
to keep you safe until now?

172
00:17:03,500 --> 00:17:06,037
You're so smart.
Why would you act recklessly?

173
00:17:06,037 --> 00:17:07,940
You truly have no fear of death.

174
00:17:11,001 --> 00:17:13,454
Why would you make such
an effort to keep me safe?

175
00:17:27,371 --> 00:17:28,935
Don't kill me.

176
00:17:28,935 --> 00:17:30,775
Please?

177
00:17:32,680 --> 00:17:36,215
How do I benefit
from offending my father?

178
00:17:39,707 --> 00:17:41,257
Prince De...

179
00:17:42,375 --> 00:17:44,028
In your mind...

180
00:17:45,057 --> 00:17:47,635
do you like me?

181
00:17:53,409 --> 00:17:55,874
If not, why would I have
saved you in the first place?

182
00:17:57,442 --> 00:17:58,923
Prince De, how naughty.

183
00:17:58,923 --> 00:18:01,970
Why won't you say
what I wish to hear?

184
00:18:04,281 --> 00:18:05,828
What do you wish to hear?

185
00:18:05,828 --> 00:18:07,241
I wish to hear you say...

186
00:18:08,480 --> 00:18:10,038
that you like me.

187
00:19:12,367 --> 00:19:13,759
How about now?

188
00:19:14,915 --> 00:19:16,455
Will you say it?

189
00:19:19,335 --> 00:19:22,961
You're doing this just to
make me say that I like you?

190
00:19:22,961 --> 00:19:26,772
I'm just trying to make life
easier for myself.

191
00:19:27,413 --> 00:19:32,055
As long as I get to win
and lead the life I want

192
00:19:34,459 --> 00:19:36,382
I'm willing to do anything.

193
00:19:44,221 --> 00:19:48,545
I don't even want to touch
anyone who doesn't like me.

194
00:19:54,058 --> 00:19:55,750
I know that.

195
00:19:58,078 --> 00:20:02,106
Women who are too clever
are really annoying.

196
00:20:05,738 --> 00:20:07,382
However...

197
00:20:07,882 --> 00:20:12,679
I can't tell from your eyes
that you dislike me.

198
00:20:17,729 --> 00:20:19,422
Get some rest soon.

199
00:20:35,362 --> 00:20:37,035
You can come out now.

200
00:20:50,189 --> 00:20:54,844
Lan Yu'er...
If Muyun De goes to you tonight

201
00:20:54,844 --> 00:20:58,326
it means he's making use
of the fact that you like him.

202
00:20:58,326 --> 00:21:01,395
But he doesn't care much for you.

203
00:21:01,395 --> 00:21:03,288
That is nonsense.

204
00:21:03,288 --> 00:21:05,320
I just saved you.

205
00:21:05,320 --> 00:21:07,673
Shouldn't you thank me?

206
00:21:10,134 --> 00:21:12,049
Thank you, Miss Yue Li.

207
00:21:16,231 --> 00:21:18,304
This isn't how I want you to thank me.

208
00:21:18,304 --> 00:21:20,146
It's useless.

209
00:21:20,146 --> 00:21:23,990
In the future when my life needs saving

210
00:21:25,051 --> 00:21:27,215
don't forget what I did for you today.

211
00:21:29,851 --> 00:21:32,352
Yes, I'll remember.

212
00:21:51,864 --> 00:21:54,509
<i>Prince De...</i>

213
00:21:54,509 --> 00:21:58,905
<i>In your mind... do you like me?</i>

214
00:22:00,153 --> 00:22:02,601
<i>If not, why would I have
saved you in the first place?</i>

215
00:22:24,586 --> 00:22:27,138
<i>If Muyun De goes to you tonight</i>

216
00:22:27,138 --> 00:22:30,559
<i>it means he's making use
of the fact that you like him.</i>

217
00:22:30,559 --> 00:22:33,501
<i>But he doesn't care much for you.</i>

218
00:22:41,265 --> 00:22:42,417
Prince De.

219
00:23:42,410 --> 00:23:44,130
<i>Greetings, Your Majesty.</i>

220
00:23:49,575 --> 00:23:50,851
Yue Li.

221
00:23:53,240 --> 00:23:54,753
Our dream of a lifetime...

222
00:23:56,905 --> 00:23:58,911
It will come true very soon.

223
00:23:58,911 --> 00:24:00,685
Yue Li?

224
00:24:02,781 --> 00:24:04,432
How should you address me, Your Majesty?

225
00:24:09,617 --> 00:24:10,821
Empress.

226
00:24:11,913 --> 00:24:15,679
My... Empress Nanku.

227
00:24:19,640 --> 00:24:21,403
I'll wait here for you.

228
00:24:34,516 --> 00:24:36,397
Prince He Ge...

229
00:24:39,449 --> 00:24:41,288
I'm waiting for you to fetch me.

230
00:24:43,804 --> 00:24:45,554
I'm your empress.

231
00:24:49,451 --> 00:24:51,346
I should be the empress.

232
00:24:56,999 --> 00:24:58,344
<i>Qin Yu Feng.</i>

233
00:25:02,601 --> 00:25:03,976
<i>Qin Yu Feng!</i>

234
00:25:18,493 --> 00:25:20,204
What took you so long?

235
00:25:20,204 --> 00:25:22,608
Do you also bully me
now that I'm a nobody?

236
00:25:23,808 --> 00:25:25,284
Bring me wine.

237
00:25:25,284 --> 00:25:26,777
- I don't have any.
- You run an inn.

238
00:25:26,777 --> 00:25:28,038
How can you not have wine?

239
00:25:28,038 --> 00:25:30,827
Miss, you've often been drunk
during your stay here at my inn.

240
00:25:30,827 --> 00:25:32,566
You made a mess in your room.

241
00:25:32,566 --> 00:25:34,411
We even had to clean it up.

242
00:25:35,972 --> 00:25:37,852
Even you would dare to pick on me?

243
00:25:37,852 --> 00:25:39,940
Miss, you should just
stay alive and well.

244
00:25:39,940 --> 00:25:41,366
Why bother tormenting yourself?

245
00:25:41,366 --> 00:25:44,000
You're making things difficult
for others and yourself.

246
00:26:01,471 --> 00:26:04,236
What is this if it isn't wine?

247
00:26:10,390 --> 00:26:11,784
Prince De.

248
00:26:30,180 --> 00:26:31,941
I'll visit you tomorrow.

249
00:26:34,900 --> 00:26:36,548
Don't hurt Prince Sheng.

250
00:26:38,748 --> 00:26:41,068
You were the one eavesdropping
outside the door?

251
00:26:46,972 --> 00:26:51,574
You're making a deal with me
in order to save Muyun Sheng?

252
00:26:54,648 --> 00:26:56,046
Sort of.

253
00:26:59,375 --> 00:27:01,085
Interesting.

254
00:27:02,961 --> 00:27:05,299
I suddenly find that you're also...

255
00:27:07,285 --> 00:27:08,766
quite interesting.

256
00:27:26,278 --> 00:27:27,281
Only if I do so...

257
00:27:29,229 --> 00:27:31,109
can I get you to like me.

258
00:27:36,470 --> 00:27:38,638
Do you know what cold wine is like?

259
00:27:41,143 --> 00:27:42,523
Cold wine...

260
00:27:42,523 --> 00:27:45,621
It's like they roll their eyes
when they are mocking you.

261
00:27:45,621 --> 00:27:50,105
When you drink it in a gulp,
it's sour and bitter.

262
00:27:50,105 --> 00:27:53,931
But it's too late and
you have to swallow it down.

263
00:27:55,108 --> 00:27:59,013
When it gets to your stomach, it burns.

264
00:28:01,460 --> 00:28:03,753
There's no fun in it at all.

265
00:28:03,753 --> 00:28:05,852
What has that got to do
with rolling eyes at others?

266
00:28:05,852 --> 00:28:08,741
What can't you understand?

267
00:28:08,741 --> 00:28:11,131
I'm telling you to warm up the wine!

268
00:28:17,294 --> 00:28:19,647
Miss, you behave terribly.

269
00:28:24,114 --> 00:28:26,397
Why would I care if
only you get to see me like this?

270
00:28:28,009 --> 00:28:29,425
Who am I?

271
00:28:30,636 --> 00:28:33,906
I'm Nanku Yue Li.

272
00:28:33,906 --> 00:28:38,105
A man like you without a family crest
means nothing to me.

273
00:28:41,287 --> 00:28:44,776
Any man is a man.

274
00:29:12,101 --> 00:29:14,298
You?

275
00:29:17,657 --> 00:29:20,607
What can you do to me?

276
00:29:25,866 --> 00:29:32,229
A man would care if his wife
was once a state prostitute.

277
00:29:35,742 --> 00:29:38,618
Whether or not
she is still labeled as lowly.

278
00:29:44,113 --> 00:29:45,909
I didn't do anything wrong.

279
00:29:47,519 --> 00:29:49,298
I didn't do anything wrong at all.

280
00:29:52,602 --> 00:29:55,444
I was born of a more prestigious
identity than anyone else.

281
00:29:55,444 --> 00:29:57,348
I'm the empress's niece.

282
00:29:57,348 --> 00:29:59,077
I am clever.

283
00:29:59,077 --> 00:30:00,548
I am good looking.

284
00:30:00,548 --> 00:30:02,369
I am hardworking.

285
00:30:02,369 --> 00:30:05,435
I know the propriety and
when I should take a step back.

286
00:30:05,435 --> 00:30:09,612
Didn't the gods bless me with all these
so that I could be the empress?

287
00:30:09,612 --> 00:30:11,565
Who else is more suitable than I am?

288
00:30:11,565 --> 00:30:13,549
This should be mine!

289
00:30:53,612 --> 00:30:55,813
The things you like...

290
00:30:57,326 --> 00:31:01,229
They are placed on the window sill
within your reach.

291
00:31:02,925 --> 00:31:05,516
Wouldn't you reach out for it?

292
00:31:07,590 --> 00:31:09,058
Wouldn't you?

293
00:31:12,109 --> 00:31:15,852
All of you find me calculative.

294
00:31:19,836 --> 00:31:21,718
Everyone dislikes me.

295
00:31:24,345 --> 00:31:26,692
You want me to be down on my luck.

296
00:31:29,824 --> 00:31:32,044
I'm just more honest than any of you.

297
00:31:33,919 --> 00:31:38,542
I dare to strive for what I want.

298
00:31:41,125 --> 00:31:44,866
I dare to strive for the best!

299
00:31:48,519 --> 00:31:49,519
I also know that...

300
00:31:52,334 --> 00:31:55,943
there are consequences to bear
in exchange for it.

301
00:31:57,682 --> 00:32:02,006
I, Nanku Yue Li, would never
rely on anyone for nothing.

302
00:32:03,178 --> 00:32:05,861
Anything I wish for...

303
00:32:07,221 --> 00:32:10,846
I always attain it
with what I'm capable of.

304
00:32:10,846 --> 00:32:14,480
Isn't this what people should do?

305
00:32:23,921 --> 00:32:28,960
Did you get to your position as
the innkeeper of Novoland Inn

306
00:32:28,960 --> 00:32:31,374
just by playing a nice man all your life?

307
00:32:31,374 --> 00:32:35,058
Did you not fight and vie
for this position?

308
00:32:38,969 --> 00:32:40,221
<i>Qin Yu Feng!</i>

309
00:32:43,246 --> 00:32:44,521
Prince De.

310
00:32:44,521 --> 00:32:46,060
You're busy?

311
00:32:48,996 --> 00:32:51,478
I asked you for a favor and yet you--

312
00:32:59,471 --> 00:33:01,634
Good acting skills.

313
00:33:16,008 --> 00:33:17,634
Prince De, is something the matter?

314
00:33:23,163 --> 00:33:24,882
There's been much going on tonight.

315
00:33:24,882 --> 00:33:27,078
Please instruct me, Prince De.

316
00:33:30,646 --> 00:33:32,336
Get someone to catch a thief.

317
00:33:32,336 --> 00:33:33,827
There's a thief?

318
00:33:33,827 --> 00:33:35,440
No.

319
00:33:35,440 --> 00:33:37,308
Bear this in mind.

320
00:33:37,308 --> 00:33:40,558
Be sure to shout a few more times
outside my father's room.

321
00:33:41,624 --> 00:33:44,546
Prepare a corpse tomorrow morning.

322
00:33:45,651 --> 00:33:47,989
Just claim that
the thief was found at night.

323
00:33:47,989 --> 00:33:49,641
He was caught...

324
00:33:50,722 --> 00:33:51,950
then beaten to death.

325
00:33:53,200 --> 00:33:54,200
Yes.

326
00:34:27,643 --> 00:34:29,588
Things I like...

327
00:34:40,641 --> 00:34:43,467
These are the things I like.

328
00:35:02,925 --> 00:35:04,693
Tiger Valor General.

329
00:35:06,150 --> 00:35:07,434
Prince De.

330
00:35:09,215 --> 00:35:10,688
Is Prince Sheng in?

331
00:35:10,688 --> 00:35:12,905
Prince Sheng is heading into
the palace very soon.

332
00:35:12,905 --> 00:35:14,989
I just have a few words to say.

333
00:35:14,989 --> 00:35:16,998
I'll leave right after that.

334
00:35:19,980 --> 00:35:21,244
Your sword.

335
00:35:22,463 --> 00:35:24,400
General, you're being too cautious.

336
00:35:24,400 --> 00:35:26,599
Prince Sheng wields
strong magical powers.

337
00:35:26,599 --> 00:35:27,856
This sword of mine...

338
00:35:27,856 --> 00:35:30,143
It's just used to scare off others.

339
00:35:30,143 --> 00:35:31,572
How can this hurt him?

340
00:35:31,572 --> 00:35:33,483
General, you'll be right by the door.

341
00:35:33,483 --> 00:35:35,885
You can come in anytime.

342
00:35:38,913 --> 00:35:40,382
Prince Sheng.

343
00:35:44,909 --> 00:35:46,617
I need to speak to you in private.

344
00:35:47,867 --> 00:35:49,525
You may go.

345
00:35:50,626 --> 00:35:51,626
- Yes.
- Yes.

346
00:36:23,139 --> 00:36:26,896
You came to visit me early in the morning
just to look at my painting?

347
00:36:28,306 --> 00:36:29,601
Your dragon...

348
00:36:29,601 --> 00:36:31,487
Why didn't you draw any eyes?

349
00:36:31,487 --> 00:36:32,728
Speak.

350
00:36:32,728 --> 00:36:34,230
What brings you here?

351
00:36:36,670 --> 00:36:37,869
My father...

352
00:36:38,951 --> 00:36:40,534
He told me to kill you.

353
00:36:47,235 --> 00:36:48,269
You don't think I will?

354
00:36:48,269 --> 00:36:49,436
I don't.

355
00:36:49,436 --> 00:36:50,536
Why?

356
00:36:52,675 --> 00:36:55,003
You don't seem like an obedient son.

357
00:36:57,601 --> 00:36:59,206
Tell me.

358
00:36:59,206 --> 00:37:01,311
What is it that you wish to ask of me?

359
00:37:13,275 --> 00:37:14,782
What is it that you wish to do?

360
00:37:14,782 --> 00:37:18,501
I wish to find out the darkest secret
at the bottom of everyone's heart.

361
00:37:18,501 --> 00:37:20,298
What then?

362
00:37:20,298 --> 00:37:21,902
I'll take them away.

363
00:37:25,304 --> 00:37:27,492
What matters the most to you?

364
00:37:29,630 --> 00:37:31,257
Women.

365
00:37:31,257 --> 00:37:33,114
You're not telling the truth.

366
00:37:35,512 --> 00:37:37,405
I'm not as fortunate as you are.

367
00:37:37,405 --> 00:37:41,903
You get to love one woman
wholeheartedly for life.

368
00:37:42,559 --> 00:37:44,384
I can't.

369
00:37:44,384 --> 00:37:46,262
I don't know why either.

370
00:37:46,262 --> 00:37:48,992
Whenever the woman I fancy
begins to fall for me

371
00:37:48,992 --> 00:37:50,690
I stop being fond of her.

372
00:37:50,690 --> 00:37:54,545
The harder I court her,
the faster I fall out of love.

373
00:37:54,545 --> 00:37:56,155
Deep down in my mind...

374
00:37:56,155 --> 00:37:58,353
I'm truly lonely.

375
00:37:58,353 --> 00:38:02,637
You were not likable
when you were a child.

376
00:38:03,760 --> 00:38:06,201
You always thought that
others were dumber than you.

377
00:38:06,201 --> 00:38:08,293
You were always mocking others.

378
00:38:08,293 --> 00:38:12,034
You could also easily read
what others had on their minds.

379
00:38:12,034 --> 00:38:14,434
You knew that they said something,
but meant another.

380
00:38:14,434 --> 00:38:16,367
Therefore, you exposed their lies.

381
00:38:16,367 --> 00:38:18,856
You enjoyed seeing others
suffer in embarrassment.

382
00:38:18,856 --> 00:38:25,777
As time went by, neither your parents
nor friends wished to talk to you.

383
00:38:26,349 --> 00:38:28,793
You knew that they didn't like you.

384
00:38:28,793 --> 00:38:31,567
Gradually, you started to
dislike yourself as well.

385
00:38:32,367 --> 00:38:34,829
You could never figure out

386
00:38:34,829 --> 00:38:38,626
whether you were the most incredible
or the most useless person.

387
00:38:38,626 --> 00:38:40,715
That's why you're always angry.

388
00:38:40,715 --> 00:38:42,168
Unusually furious.

389
00:38:42,168 --> 00:38:43,856
Am I right?

390
00:38:48,617 --> 00:38:51,708
I think that I can bare
my heart to you now.

391
00:38:55,456 --> 00:38:57,322
The Duan Empire should come to an end.

392
00:38:59,163 --> 00:39:01,253
People should be killed.

393
00:39:01,253 --> 00:39:06,327
You came to console me because you think
that I've been hurt by the people?

394
00:39:06,327 --> 00:39:08,250
To make use of me?

395
00:39:08,250 --> 00:39:09,684
You were born with such a destiny.

396
00:39:09,684 --> 00:39:11,052
You also have the force to do so.

397
00:39:11,052 --> 00:39:12,815
I've been fighting against this destiny.

398
00:39:12,815 --> 00:39:14,059
Why would you fight against it?

399
00:39:14,059 --> 00:39:16,465
You just don't want to become
the bad person.

400
00:39:16,465 --> 00:39:19,135
Just judging from that,
how is there any difference between us?

401
00:39:19,135 --> 00:39:20,382
There are kind and evil people.

402
00:39:20,382 --> 00:39:21,827
The evil shall pay for their sins.

403
00:39:21,827 --> 00:39:23,442
The good shall live to an old, ripe age.

404
00:39:34,472 --> 00:39:37,059
However, in the Duan Empire nowadays

405
00:39:37,059 --> 00:39:39,543
no one can really
tell kindness from evil.

406
00:39:41,789 --> 00:39:46,893
Have you seen how bees find a new home?

407
00:39:47,623 --> 00:39:49,804
They send out eight worker bees
at the same time

408
00:39:49,804 --> 00:39:51,849
to look for a new home
in all different directions.

409
00:39:52,088 --> 00:39:54,652
After those eight worker bees return

410
00:39:54,652 --> 00:39:57,914
they dance in the form
of the number eight

411
00:39:57,914 --> 00:40:00,856
to inform the swarm of worker bees
of the best spot they have chosen.

412
00:40:00,856 --> 00:40:03,675
The worker bees then send out
swarms of bees to follow them.

413
00:40:03,675 --> 00:40:09,608
In the end, the one with more followers
has the better location.

414
00:40:09,608 --> 00:40:12,215
That's how they find
a new place for their hive.

415
00:40:12,215 --> 00:40:15,735
However, those eight worker bees
don't know where the others went.

416
00:40:15,735 --> 00:40:18,699
How could they know that
they have found the best spot?

417
00:40:18,699 --> 00:40:20,585
They don't actually know.

418
00:40:20,585 --> 00:40:23,163
The other bees don't mind either.

419
00:40:23,163 --> 00:40:25,028
They just choose
the one who dances better

420
00:40:25,028 --> 00:40:26,525
and follow it there.

421
00:40:26,525 --> 00:40:29,304
What accuracy is there?

422
00:40:30,597 --> 00:40:32,396
What if that's because of trust?

423
00:40:33,521 --> 00:40:35,117
They are just lazy.

424
00:40:36,159 --> 00:40:40,088
They don't want to judge for themselves
if the location is good or bad.

425
00:40:40,088 --> 00:40:42,246
They just want to go along
with everyone's opinion

426
00:40:42,246 --> 00:40:44,402
thinking that it will surely
always be correct.

427
00:40:44,402 --> 00:40:48,141
Even if there's a mistake,
it's what the majority has chosen.

428
00:40:48,141 --> 00:40:50,632
It's not the individual's fault.
He's still a good person.

429
00:40:50,632 --> 00:40:52,817
Kick someone when he's down.

430
00:40:53,940 --> 00:40:56,835
But he won't be
the first to kick that man.

431
00:40:56,835 --> 00:40:58,250
Am I right?

432
00:41:00,217 --> 00:41:02,175
So I have to kill everyone?

433
00:41:02,175 --> 00:41:03,639
You're helping them.

434
00:41:05,327 --> 00:41:07,297
Only when they stop being lazy

435
00:41:07,297 --> 00:41:10,682
can they be responsible
for their duty as a person.

436
00:41:10,682 --> 00:41:14,065
They can then become powerful
enough to be responsible.

437
00:41:15,541 --> 00:41:17,813
When every one of the Humans
is equally powerful

438
00:41:17,813 --> 00:41:21,875
only then can Novoland flourish
and be reborn as a new Novoland.

439
00:41:21,875 --> 00:41:23,490
I'm not interested in that.

440
00:41:24,818 --> 00:41:26,231
My father...

441
00:41:26,231 --> 00:41:27,333
Your father..

442
00:41:27,333 --> 00:41:29,063
All the officials in the royal court...

443
00:41:29,063 --> 00:41:30,853
They can all come up with such a theory.

444
00:41:30,853 --> 00:41:33,454
They will all use the excuse of
strengthening the country and people

445
00:41:33,454 --> 00:41:35,003
for their own personal gain.

446
00:41:35,003 --> 00:41:36,666
That is an even greater lie.

447
00:41:36,666 --> 00:41:38,938
I despise them for it.

448
00:41:38,938 --> 00:41:40,539
Your father...

449
00:41:41,246 --> 00:41:42,632
My father...

450
00:41:46,309 --> 00:41:48,063
My father...

451
00:41:50,036 --> 00:41:51,744
They are both too foolish.

452
00:41:52,989 --> 00:41:54,858
I also look down on them.

453
00:41:57,186 --> 00:41:59,003
They...

454
00:42:01,402 --> 00:42:03,458
They are both too narrow-minded.

455
00:42:04,458 --> 00:42:08,333
They only struggle for power
within their own Human tribe.

456
00:42:08,333 --> 00:42:10,333
What's the fun in that?

457
00:42:10,333 --> 00:42:13,318
The battle between the God of Ruins
and the God of Desolation...

458
00:42:13,318 --> 00:42:15,456
That created Novoland.

459
00:42:15,456 --> 00:42:17,394
Apart from the Humans

460
00:42:17,394 --> 00:42:20,369
there are still the Soarers
whose wings grow upon moonlight.

461
00:42:20,369 --> 00:42:22,443
The Wise who are naturally intelligent.

462
00:42:22,443 --> 00:42:24,451
The Oceanides who dwell
both in water and on land.

463
00:42:24,451 --> 00:42:26,693
The strong, but simple Giants,
and then your kind...

464
00:42:26,693 --> 00:42:29,130
The Charmers who are formed
by energy of all creations.

465
00:42:30,681 --> 00:42:35,681
Subtitles by DramaFever

