1
00:02:04,291 --> 00:02:10,214
J E A N N E  D ' A R C

2
00:02:17,930 --> 00:02:23,352
SCENARIO

3
00:03:01,306 --> 00:03:04,142
In the "Chamber of Deputies"library in Paris

4
00:03:04,309 --> 00:03:07,521
found one of the most extraordinary documents

5
00:03:07,688 --> 00:03:10,274
of the history of the world: 
the transcript process of Jeanne ofArc,

6
00:03:10,440 --> 00:03:13,610
The process that accomplished 
her sentence and her death...

7
00:03:23,620 --> 00:03:27,207
...the judges questions 
and the answers ofJeanne

8
00:03:27,416 --> 00:03:30,794
are overseen with great care.

9
00:03:33,755 --> 00:03:37,676
...at the reading we unveil 
Jeanne as she was...

10
00:03:37,801 --> 00:03:40,804
Not with a helmet and armour- 
but simple and human...

11
00:03:40,929 --> 00:03:43,390
a young girl who died for her country...

12
00:03:46,894 --> 00:03:49,479
...and we are witnessing a rare account:

13
00:03:49,605 --> 00:03:52,733
a young girl of faith.

14
00:03:53,108 --> 00:03:56,445
confronted by a cohort of blind 
theologens and corrupt lawyers.

15
00:05:30,080 --> 00:05:33,333
I swear to tell the truth, 
all the truth...

16
00:05:35,377 --> 00:05:37,713
...nothing but the truth...

17
00:06:00,068 --> 00:06:03,197
...In France,they call me Jeanne...

18
00:06:12,247 --> 00:06:15,334
...in my country,they called me Jeannette.

19
00:06:20,631 --> 00:06:22,591
What is your age?

20
00:06:32,559 --> 00:06:35,562
19 years old... as I remember...

21
00:06:44,404 --> 00:06:46,782
Do you know the "Our Father"?

22
00:06:52,204 --> 00:06:54,498
Who taught you this?

23
00:07:15,602 --> 00:07:17,104
...my mother.

24
00:07:25,362 --> 00:07:27,614
Do you want to recite the "Our Father"?

25
00:07:44,548 --> 00:07:47,217
Do you pretend to be sent by God?

26
00:07:58,896 --> 00:08:00,981
...in order to save France...

27
00:08:08,030 --> 00:08:10,449
...it is for this I was born.

28
00:08:20,125 --> 00:08:22,794
Thus you think God hates the English?

29
00:08:37,518 --> 00:08:42,147
Of love or hate that god has
for the English,I know nothing...

30
00:08:48,487 --> 00:08:52,449
...but I know well that the
English will all be hunted in France...

31
00:09:14,346 --> 00:09:16,431
...except those who will die there!

32
00:09:47,963 --> 00:09:52,634
You said saint Michael appeared to you...
... describe him?

33
00:09:58,223 --> 00:10:00,100
Did he have wings?

34
00:10:03,478 --> 00:10:05,564
Did he have a crown?

35
00:10:13,655 --> 00:10:15,991
How was he dressed?

36
00:10:33,133 --> 00:10:37,012
How could you know if it was 
a man or a woman?

37
00:10:43,018 --> 00:10:44,770
Was he naked?

38
00:10:56,907 --> 00:11:00,452
Don.t you think God has 
something to clothe him?

39
00:11:08,377 --> 00:11:10,420
Did he have long hair?

40
00:11:18,512 --> 00:11:20,764
Why would he have them cut?

41
00:11:28,438 --> 00:11:31,692
Why are you dressed like a man?

42
00:11:37,322 --> 00:11:40,868
If we gave you woman's clothing,
would you put it on?

43
00:11:58,510 --> 00:12:01,680
Once my mission from  
God is finished,

44
00:12:01,847 --> 00:12:04,057
then I will wear woman's clothing.

45
00:12:46,183 --> 00:12:49,937
What do you expect as a reward from God?

46
00:13:01,573 --> 00:13:05,494
So it's God that ordered  
you to dress like a man?

47
00:13:18,382 --> 00:13:20,384
The salvation of my soul.

48
00:13:29,393 --> 00:13:31,395
You blasheme God!

49
00:13:59,548 --> 00:14:01,133
It's an insult...

50
00:14:05,637 --> 00:14:07,806
To Me.she is a saint

51
00:16:45,839 --> 00:16:48,258
Did God make you any promises?

52
00:17:06,818 --> 00:17:09,655
That has nothing to do with this trial.

53
00:17:19,039 --> 00:17:21,792
Should we leave the judges to decide?

54
00:17:27,339 --> 00:17:30,467
Do you want to put this question to a vote?

55
00:17:57,119 --> 00:18:00,164
Well! What did God promise you?

56
00:18:02,082 --> 00:18:05,627
Did he tell you,you won't go to prison?

57
00:18:14,970 --> 00:18:16,471
When?

58
00:18:32,279 --> 00:18:34,281
I don't know the day...

59
00:18:39,703 --> 00:18:41,163
...nor the hour

60
00:19:14,780 --> 00:19:18,075
Since she doesn't want 
to admit to anything through kindness,

61
00:19:18,200 --> 00:19:20,661
Then well will get it 
out of her through trickery...

62
00:19:31,880 --> 00:19:36,134
Go look for a letter 
bearing the signature of King Charles.

63
00:19:45,477 --> 00:19:48,605
Write what I am going to dictate.

64
00:22:35,063 --> 00:22:37,399
I pity you!

65
00:23:05,761 --> 00:23:09,181
Do you recognize 
the signature of your King?

66
00:23:24,321 --> 00:23:27,157
I have a letter from him to you!

67
00:23:45,843 --> 00:23:47,845
I don't know how to read.

68
00:23:52,516 --> 00:23:55,310
To our dearest and beloved Jeanne...

69
00:24:03,485 --> 00:24:07,906
I am preparing to march into Rouen
with a great army...

70
00:24:11,910 --> 00:24:14,121
I am sending this dedicated priest.

71
00:24:14,288 --> 00:24:16,123
trust him.

72
00:25:04,630 --> 00:25:06,882
As Jesus is the son of God,

73
00:25:07,007 --> 00:25:09,343
Are you saying you are 
the daughter of God?

74
00:25:14,348 --> 00:25:17,851
Do you want to recite the "Our Father"?

75
00:25:57,182 --> 00:26:01,186
Did God say that you will 
go to prison?

76
00:26:17,578 --> 00:26:19,580
...with a great victory!

77
00:27:01,538 --> 00:27:05,042
Did God promise you would 
go to Heaven?

78
00:27:23,769 --> 00:27:26,146
You are therefore sure to be saved ?

79
00:27:42,955 --> 00:27:46,166
Be careful!It's a serious answer.

80
00:28:12,985 --> 00:28:15,529
Since you are sure of your salvation,

81
00:28:15,696 --> 00:28:18,157
you don't need the church?

82
00:28:42,181 --> 00:28:44,349
Are you in a state of grace?

83
00:29:23,222 --> 00:29:26,308
Well!answer!
Are you in a state of grace?

84
00:29:46,328 --> 00:29:49,456
If I am , God keeps me there ...

85
00:29:56,713 --> 00:29:59,925
If I am not,God puts me there !

86
00:30:26,034 --> 00:30:27,452
My Lord...

87
00:30:35,878 --> 00:30:38,589
...permit me,to hear mass.

88
00:31:17,211 --> 00:31:20,214
Jeanne,If I permit you to 
attend mass...

89
00:31:24,218 --> 00:31:27,346
...will you consent to 
leave your clothes of a man?

90
00:31:54,414 --> 00:31:58,335
You love your men's clothes 
more than hearing mass?

91
00:32:03,382 --> 00:32:05,384
This shamefull suit...

92
00:32:11,431 --> 00:32:13,267
abominable to God...

93
00:32:30,409 --> 00:32:32,703
You are not 
the daughter of God!

94
00:32:35,706 --> 00:32:37,916
...You are Satan's supporter!

95
00:32:44,089 --> 00:32:46,425
Prepare the torture.

96
00:34:42,583 --> 00:34:45,252
She looks like 
the daughter of God.

97
00:35:33,050 --> 00:35:35,344
In the torture chamber.

98
00:36:11,046 --> 00:36:13,048
Face your judges.

99
00:36:22,140 --> 00:36:24,768
Don't you think that these doctors

100
00:36:24,935 --> 00:36:27,062
are wiser than you?

101
00:36:34,778 --> 00:36:37,281
But God is wiser!

102
00:36:47,708 --> 00:36:52,462
Listen Jeanne, we know that 
your revelations don't come from God...

103
00:36:56,717 --> 00:36:58,677
...but from Satan!

104
00:37:10,230 --> 00:37:13,984
How can you tell 
a good angel from a bad angel?

105
00:37:22,951 --> 00:37:27,998
It is before Satan that you knelt,
and not before saint Michael!

106
00:37:38,258 --> 00:37:40,427
You do not see that it is Satan...

107
00:37:40,594 --> 00:37:42,513
...that has turned your head...

108
00:37:46,850 --> 00:37:48,769
...who deceived you...

109
00:37:52,481 --> 00:37:54,066
...and betrayed you?

110
00:38:07,996 --> 00:38:11,500
I think she is ready to 
sign a confession!

111
00:39:04,511 --> 00:39:06,847
The Church is opening it's arms to you...

112
00:39:13,395 --> 00:39:16,273
...if you refuse,
the Church will abandon you...

113
00:39:16,398 --> 00:39:18,108
...and you will be alone!

114
00:39:21,778 --> 00:39:23,405
...alone!

115
00:39:26,283 --> 00:39:28,160
...yes, alone...

116
00:39:32,873 --> 00:39:34,958
...alone with God!

117
00:40:56,165 --> 00:40:59,001
Even if you kill me...

118
00:40:59,168 --> 00:41:00,961
I will renounce nothing.

119
00:41:05,674 --> 00:41:08,886
and if I say something else after,

120
00:41:09,052 --> 00:41:11,388
it is because you forced me !

121
00:42:28,215 --> 00:42:30,634
There is nothing more 
in the world that I want...

122
00:42:30,759 --> 00:42:32,219
than to die a natural death...

123
00:42:39,810 --> 00:42:41,353
She is very weak.

124
00:42:47,943 --> 00:42:50,988
She has the fever...
it is necesary to bleed her.

125
00:42:55,742 --> 00:42:58,287
Be carefull!
She could kill herself...

126
00:42:58,453 --> 00:43:00,330
She is very tricky.

127
00:43:55,469 --> 00:43:57,804
Go get the Sacrements.

128
00:44:51,650 --> 00:44:53,944
Have you nothing to say to us?

129
00:45:06,123 --> 00:45:09,293
I believe I am in danger of death...

130
00:45:16,717 --> 00:45:21,054
...and if I die,I ask you 
to put my body in holy ground.

131
00:45:33,317 --> 00:45:35,986
The Church is mercifull...

132
00:45:43,076 --> 00:45:46,205
She always welcomes 
the lost sheep...

133
00:46:05,641 --> 00:46:08,685
Jeanne, we want what is best for you,

134
00:46:08,852 --> 00:46:11,313
See,I have sent for  
the Holy Sacrement,

135
00:46:51,645 --> 00:46:54,148
I am a good christian.

136
00:47:53,832 --> 00:47:58,170
Don't you know that it is 
the body of Christ that you refuse?

137
00:48:05,427 --> 00:48:10,140
Don't you see that it is 
God that you anger by your stubborness?

138
00:49:01,316 --> 00:49:04,403
I love and honor God... 
with all my heart.

139
00:49:42,524 --> 00:49:46,069
You say...
that I am... 
sent from the devil...

140
00:49:55,704 --> 00:49:57,498
...this is not true...

141
00:50:01,293 --> 00:50:05,422
...it is you who was sent 
from the devil to make me suffer...

142
00:50:09,801 --> 00:50:11,261
....and you...

143
00:50:14,223 --> 00:50:15,807
....and you...

144
00:50:18,435 --> 00:50:19,978
....and you...

145
00:50:33,951 --> 00:50:36,036
There is nothing more to be done...

146
00:50:36,119 --> 00:50:37,788
...go get the executioner!

147
00:52:12,466 --> 00:52:16,345
For the last time let us 
try to save this lost soul...

148
00:52:41,453 --> 00:52:44,164
It is to you, Jeanne,that I speak...

149
00:52:48,418 --> 00:52:51,713
...It is to you that I say 
your King is a heretic!

150
00:52:56,134 --> 00:53:00,138
My King is the most 
noble christian of all christians.

151
00:53:12,901 --> 00:53:15,988
The pride of this woman 
doesn't make any sense...

152
00:54:23,263 --> 00:54:26,725
...never have I seen in France... 
such a monster...

153
00:54:38,737 --> 00:54:41,865
I have never done any wrong to anyone.

154
00:54:54,962 --> 00:54:58,507
If you don't sign,
you will be burned alive...

155
00:55:02,052 --> 00:55:04,596
...See the executioner who waits...

156
00:55:18,318 --> 00:55:22,865
You don't have to die.
Your King still needs your help.

157
00:55:41,133 --> 00:55:44,261
Jeanne, sign...
and save your life!

158
00:55:51,351 --> 00:55:54,271
Jeanne, we pity you

159
00:56:01,069 --> 00:56:02,946
Sign Jeanne!

160
00:57:38,208 --> 00:57:40,502
In the name of the Lord.
Amen.

161
00:57:48,886 --> 00:57:51,805
Since you have recognized 
your errors,

162
00:57:51,972 --> 00:57:54,349
We are abanding the bonds 
of excommunication...

163
00:58:01,857 --> 00:58:05,944
But because you have recklessly sinned,
we are condemning you

164
00:58:14,703 --> 00:58:17,456
to life in prison 
with the bread of pain

165
00:58:17,581 --> 00:58:19,958
and the water of anguish.

166
00:58:29,885 --> 00:58:32,679
You have made a good day...

167
00:58:32,930 --> 00:58:34,973
you have saved your life 
and your soul.

168
00:59:08,549 --> 00:59:11,260
She has only made a mockery of you!

169
00:59:17,432 --> 00:59:19,226
Long Live Jeanne!

170
01:01:11,338 --> 01:01:13,757
Go get the judges...

171
01:01:16,760 --> 01:01:19,429
I repent,
I have lied!

172
01:01:22,266 --> 01:01:23,976
...hurry up!

173
01:02:09,229 --> 01:02:11,440
I have done a great wrong...

174
01:02:34,671 --> 01:02:37,466
...I have renounced God
... to save my life.

175
01:02:43,180 --> 01:02:46,141
But Jeanne...
before the people you recognized...

176
01:02:46,308 --> 01:02:48,852
...that you were inspired by the devil.

177
01:03:03,033 --> 01:03:06,578
You believe always that 
you are the elect of the Lord?

178
01:03:25,180 --> 01:03:27,808
That answer evokes death.

179
01:04:03,051 --> 01:04:06,138
I did not admit it... 
because of my fear of fire.

180
01:04:15,898 --> 01:04:18,233
Have you anything else to say to us?

181
01:05:39,940 --> 01:05:43,277
We have come 
to prepare you for death.

182
01:05:57,666 --> 01:06:00,169
Is it now...already?

183
01:06:10,387 --> 01:06:11,930
What kind of death?

184
01:06:16,810 --> 01:06:18,854
At the stake.

185
01:06:30,199 --> 01:06:32,367
I am going to look for the sacrement.

186
01:06:40,834 --> 01:06:45,797
Tell me,how can you always believe 
that you are sent by God?

187
01:06:51,053 --> 01:06:53,806
His ways are not our ways.

188
01:07:04,858 --> 01:07:07,069
Yes,I am his child.

189
01:07:18,247 --> 01:07:20,624
And the great victory?

190
01:07:29,633 --> 01:07:31,552
...my martyrdom!

191
01:07:36,890 --> 01:07:38,851
...and your deliverance?

192
01:07:59,204 --> 01:08:00,706
...death!

193
01:08:23,645 --> 01:08:25,689
Do you want to confess?

194
01:09:34,758 --> 01:09:39,680
Body of our Lord Jesus Christ 
protector of our spirit...

195
01:09:54,444 --> 01:09:57,573
...in eternal life. Amen.

196
01:11:06,016 --> 01:11:10,521
Be corageous, Jeanne. 
Your last hour is near.

197
01:12:33,770 --> 01:12:37,691
Very gentle God,I accept death 
with all my heart...

198
01:12:56,001 --> 01:12:59,671
...but don't let me suffer too long.

199
01:13:04,551 --> 01:13:08,180
Will I be with you 
this night in paradise?

200
01:18:07,688 --> 01:18:09,565
Jesus!

201
01:18:17,656 --> 01:18:19,950
You have burned a saint!

202
01:21:17,586 --> 01:21:21,173
The protective flames 
engulfed the soul of Jeanne

203
01:21:21,298 --> 01:21:24,301
when she rose up to heaven -

204
01:21:24,426 --> 01:21:27,721
Jeanne,whose heart 
has become the heart of France...

205
01:21:27,888 --> 01:21:30,641
Jeanne, whose memory 
will be honored for all time...

206
01:21:30,766 --> 01:21:32,935
by the people of France.

207
01:21:34,478 --> 01:21:41,902
Translated in English by an 
anonymous Anglophone from Quebec...

208
01:21:42,945 --> 01:21:48,158

special thanks for providing 
this subtitle to:

209
01:21:49,201 --> 01:21:54,414

niewielka cz��:Grojan umaczenie: Zeljko 
(gg#3875359)Odwied� 
www.NAPiSY.info

