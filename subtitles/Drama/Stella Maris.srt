﻿1
00:01:07,300 --> 00:01:12,420
<i>Stella? It's Raggi. Is it a bad time?</i>
- Hell yes!

2
00:01:14,110 --> 00:01:15,700
What do you want?

3
00:01:16,020 --> 00:01:18,620
<i>The man on the phone is a cop named Raggi.</i>

4
00:01:18,620 --> 00:01:24,940
<i>He calls me every time some petty criminal
needs a lawyer.</i>

5
00:01:24,970 --> 00:01:30,180
I got someone who needs a lawyer here.
- Find someone else.

6
00:01:30,180 --> 00:01:34,140
<i>It's an old friend of yours.</i>
- Wait!

7
00:01:39,940 --> 00:01:43,460
<i>Stella? Hello?</i>

8
00:01:47,580 --> 00:01:50,140
Good boy.

9
00:01:50,140 --> 00:01:53,140
<i>Stella? Are you alright?</i>

10
00:01:56,980 --> 00:01:58,580
<i>Hello?</i>

11
00:02:00,220 --> 00:02:04,060
You can't smoke in here.
- Sure.

12
00:02:05,860 --> 00:02:08,780
Who's this friend?
- Sæmi.

13
00:02:08,780 --> 00:02:14,220
<i>Sæmi fucking Jo, one of our most loyal
small criminals.</i>

14
00:02:14,220 --> 00:02:16,460
<i>An old client.</i>

15
00:02:16,660 --> 00:02:24,420
They pay me scraps for people like him.
- He's in for a murder at the Ministry.

16
00:02:24,900 --> 00:02:31,620
In the Prime Minister's office.
- What did you say? Who did he kill?

17
00:02:32,860 --> 00:02:38,140
Stella... you want the case or not?
- I'll be right there.

18
00:02:40,440 --> 00:02:43,220
Leaving already?

19
00:02:45,020 --> 00:02:47,940
Are you sober enough to drive?

20
00:03:37,700 --> 00:03:41,620
<b><i>ENGLISH SUBTITLES BY BARISHNIKOV@KATCR.CO</i></b>

21
00:03:43,550 --> 00:03:46,060
Thanks for the ride.

22
00:03:46,860 --> 00:03:49,260
Can I have your num...

23
00:04:10,660 --> 00:04:13,700
You got me something to eat?

24
00:04:23,460 --> 00:04:28,060
How come you're all dressed up?
Is there a party going on?

25
00:04:28,660 --> 00:04:37,540
Each Ministry has sent a representative. The
PM's assistant doesn't get murdered everyday.

26
00:04:38,380 --> 00:04:41,220
<i>This deserves an explanation.</i>

27
00:04:41,220 --> 00:04:50,580
<i>Halla was PM Sverrir Kristjánsson's assistant. He
and his party have risen to power 5 years ago.</i>

28
00:04:50,680 --> 00:05:00,100
<i>Taking advantage of people's disgust for the previous
government, they won the election by a landslide.</i>

29
00:05:01,220 --> 00:05:06,700
<i>Right away they brought down the crown
and introduced the almighty dollar.</i>

30
00:05:07,100 --> 00:05:12,260
<i>All sort of things were legalized
and several restrictions were abolished.</i>

31
00:05:13,420 --> 00:05:18,100
<i>Relations with China became a priority.</i>

32
00:05:18,100 --> 00:05:23,100
<i>Sverrir sealed Iceland's
biggest deal of all time...</i>

33
00:05:23,100 --> 00:05:28,580
<i>...when he sold to China
the northernmost of Iceland's islands.</i>

34
00:05:28,580 --> 00:05:32,200
<i>This brought huge changes to our society.</i>

35
00:05:32,200 --> 00:05:40,980
<i>Corruption has now reached new levels in this already nepotistic
country where the few get everything at the expense of the most.</i>

36
00:05:40,980 --> 00:05:46,540
<i>Anyway, back to the case.</i>
- She was killed around 3 am.

37
00:05:46,540 --> 00:05:50,980
Why is Sæmi a suspect?
- Various reasons.

38
00:05:51,340 --> 00:05:57,140
He was found lying on top of her
covered in blood.

39
00:05:57,780 --> 00:06:03,540
<i>Cameras show Halla coming in by herself
at about 1 am.</i>

40
00:06:03,900 --> 00:06:09,140
<i>She sends the guard home
and then switches off the cameras.</i>

41
00:06:10,300 --> 00:06:14,780
<i>They're switched back on
for the morning shift...</i>

42
00:06:14,980 --> 00:06:21,780
<i>...showing Sæmi in the PM's office,
covered in blood and lying on Halla's body.</i>

43
00:06:24,740 --> 00:06:27,940
Call me when you're done.

44
00:06:34,500 --> 00:06:38,020
You got something on your neck.

45
00:06:39,820 --> 00:06:41,940
It's not my blood.

46
00:06:41,940 --> 00:06:46,140
<i>He's screwed.</i>
- Am I screwed?

47
00:06:46,340 --> 00:06:51,100
I didn't even know her.
- What were you doing there?

48
00:06:55,860 --> 00:07:01,620
You called her twice the night she died.

49
00:07:03,580 --> 00:07:06,860
Have you got any plausible explanation?

50
00:07:10,620 --> 00:07:17,660
Come on, Sæmi. You know how it works.
Talk to me and I'll help you out of here.

51
00:07:22,660 --> 00:07:30,020
Sometimes I brought her cola.
<i>- By "cola" he means cocaine.</i>

52
00:07:30,420 --> 00:07:35,140
Let's just call it "sugar" from now on.
So you did know her?

53
00:07:35,140 --> 00:07:39,220
It was business,
we never went to the movies or such.

54
00:07:39,220 --> 00:07:44,740
Ok.
How did her blood end up on you?

55
00:07:46,780 --> 00:07:53,300
I came by at 3 because she
needed some... sugar.

56
00:07:54,100 --> 00:07:59,500
<i>I came in through the back door as usual.
Nobody was there.</i>

57
00:07:59,500 --> 00:08:05,100
I got in the room and found her like that.
I freaked out.

58
00:08:05,100 --> 00:08:12,540
I was trying to reanimate her
when the guard came in and knocked me down.

59
00:08:14,500 --> 00:08:18,960
Come on, Sæmi. You can do better than that.
Did she owe you money?

60
00:08:18,960 --> 00:08:21,140
I didn't do it!

61
00:08:21,140 --> 00:08:25,540
My client won't say anything more.

62
00:08:25,540 --> 00:08:31,740
Let's suppose he was there selling sugar...
that doesn't make him a murderer.

63
00:08:31,740 --> 00:08:38,980
Are we questioning him or you?
- Until he's tried, he'll help in the enquiry.

64
00:08:40,900 --> 00:08:42,860
Raggi?

65
00:09:00,700 --> 00:09:06,370
You came on to her, she rejected you
so you attacked her.

66
00:09:06,700 --> 00:09:12,820
"Came on to her"? You're a Mormon and
you're accusing my client of rape?

67
00:09:12,920 --> 00:09:19,820
We found semen. It's being analyzed.
- He's not gonna answer your speculations.

68
00:09:20,100 --> 00:09:24,940
Ask reasonable questions, Mr. Fletcher,
otherwise we're out of here.

69
00:09:25,740 --> 00:09:30,140
Stella, can we talk for a minute?

70
00:09:30,480 --> 00:09:33,480
Not a word until I'm back.

71
00:09:39,140 --> 00:09:45,500
<i>Edda Kristjáns.
Attorney General and PM's sister.</i>

72
00:09:46,180 --> 00:09:49,860
<i>Word is she's the one pulling the strings.</i>

73
00:09:49,860 --> 00:09:51,980
Hello.

74
00:09:51,980 --> 00:09:56,980
I'm afraid you wasted your time.
- Is that so?

75
00:09:56,980 --> 00:10:01,500
Edda Kristjánsdóttir, Sæmi's lawyer.
- Does he know?

76
00:10:01,500 --> 00:10:06,090
Yes, he's my client.
- I'm the only lawyer he's been talking to.

77
00:10:06,090 --> 00:10:10,260
Perhaps you're the one wasting your time.
- I'm sure we can reach an agreement.

78
00:10:10,260 --> 00:10:13,370
I'm not looking to be paid for this case.

79
00:10:13,370 --> 00:10:17,860
You're willing to work pro-bono on this?

80
00:10:17,860 --> 00:10:23,740
I know you and you don't come cheap.
Who's paying the bill?

81
00:10:23,840 --> 00:10:28,420
We can reach an agreement
on the fee if you like.

82
00:10:28,700 --> 00:10:32,460
Thanks but no.

83
00:10:33,860 --> 00:10:38,180
Sæmi is my client.
Find yourself another one.

84
00:10:46,160 --> 00:10:50,580
No one will talk to my client
without my permission.

85
00:10:51,060 --> 00:10:55,160
What happened?
- Nothing. We can continue.

86
00:10:55,160 --> 00:10:57,980
<i>Sæmi's record is not helping him.</i>

87
00:10:57,980 --> 00:11:04,180
<i>I agree with the judge when he says Sæmi looks
as innocent as a German pensioner in Argentina.</i>

88
00:11:04,180 --> 00:11:08,180
...in reclusion until April 19...

89
00:11:27,140 --> 00:11:33,460
How did Sæmi get in?
- She let him in from the back door.

90
00:11:34,060 --> 00:11:39,220
What was Halla doing here that late?
- I imagine she was working.

91
00:11:39,220 --> 00:11:46,140
<i>The crime scene tells me Sæmi is innocent
and Halla wasn't killed here.</i>

92
00:11:46,280 --> 00:11:51,420
<i>The blood stain is very small
compared to the condition of Halla's face.</i>

93
00:11:53,300 --> 00:11:59,980
<i>A young man was arrested for the Ministry
murder. The victim is Halla Gunnarsdottir.</i>

94
00:11:59,980 --> 00:12:02,780
<i>The man is known by the
police for some petty crimes.</i>

95
00:12:02,780 --> 00:12:07,180
<i>A press conference has
been announced for today.</i>

96
00:12:07,180 --> 00:12:10,140
<i>People are shocked by this crime.</i>

97
00:12:10,140 --> 00:12:18,580
<i>They express hate and contempt for my client...
and for the loss of an attractive woman.</i>

98
00:12:21,340 --> 00:12:23,780
I'm devastated.

99
00:12:23,780 --> 00:12:30,460
I can't believe such thing could happen
inside my Ministry...

100
00:12:30,460 --> 00:12:36,860
...and that one of my employees
had to suffer such a horrible death.

101
00:12:36,860 --> 00:12:39,260
<i>So boring.</i>

102
00:12:40,700 --> 00:12:47,660
<i>Why is the PM's sister so determined
in defending the murderer of his assistant?</i>

103
00:12:48,790 --> 00:12:53,740
<i>Police are not gonna tell me anything
that I don't already know.</i>

104
00:12:53,740 --> 00:12:57,860
<i>Except for the gruesome pictures of the body.</i>

105
00:13:27,380 --> 00:13:33,540
<i>In case you're wondering about my minimalism,
I just don't wanna buy shitty furniture.</i>

106
00:13:33,820 --> 00:13:39,300
<i>The perfect living room does exist,
I just haven't found it yet.</i>

107
00:14:00,710 --> 00:14:04,460
<i>But now let's get to know Halla a bit better.</i>

108
00:14:07,180 --> 00:14:13,060
<i>Pretty, long hair,
bright eyes and determined look.</i>

109
00:14:13,060 --> 00:14:17,540
<i>Full, smiling lips.
Knockers men dream about.</i>

110
00:14:17,540 --> 00:14:23,650
<i>Tragedy: loses her parents as a child
and is raised by her aunt in Selfoss.</i>

111
00:14:23,650 --> 00:14:27,540
<i>Stands out in Poli-Sci.
She can draw attention.</i>

112
00:14:27,540 --> 00:14:30,420
<i>Hired by the Ministry 18 months ago.</i>

113
00:14:30,420 --> 00:14:37,300
<i>The other assistant, Haukur Garðarsson,
helped me with my law exams back in school.</i>

114
00:14:44,420 --> 00:14:50,340
<i>Call me if you want info
on the Ministry case.</i>

115
00:14:58,620 --> 00:15:02,020
<i>You don't waste time.</i>
- Who are you?

116
00:15:02,020 --> 00:15:07,900
<i>I heard your client's in trouble.</i>
- Yes and you're very mysterious. What do you want?

117
00:15:07,900 --> 00:15:12,020
<i>If you wanna know more,
come to Hamraborg parking lot.</i>

118
00:15:12,020 --> 00:15:15,940
Why would I go in a parking lot
in the middle of the night?

119
00:15:15,940 --> 00:15:21,260
<i>Police are urged to solve the case.
They're not gonna dig any deeper.</i>

120
00:15:21,260 --> 00:15:24,940
<i>They'll just build a case
against your client.</i>

121
00:15:24,940 --> 00:15:29,740
<i>I can help you.</i>
- Alright. Shall I call you when I'm there?

122
00:15:29,740 --> 00:15:33,020
<i>This number will no longer be active.</i>

123
00:16:31,140 --> 00:16:33,300
Good evening.

124
00:16:35,700 --> 00:16:39,140
I gotta give you this.

125
00:16:40,940 --> 00:16:48,020
You had a more feminine voice on the phone.
- I'm only a messenger.

126
00:16:48,130 --> 00:16:55,900
A couple of tips for you and your client.
- Very kind of you to help poor Sæmi.

127
00:16:55,900 --> 00:17:00,100
Do you work for some charity organization?

128
00:17:00,100 --> 00:17:03,340
Why don't you give this to the police?

129
00:17:03,340 --> 00:17:08,580
They're the ones investigating
the case after all.

130
00:17:09,260 --> 00:17:14,660
That's all I have to say.
Just read that.

131
00:17:31,020 --> 00:17:33,740
<i>The messenger's letter:</i>

132
00:17:33,740 --> 00:17:44,060
<i>not very clear allegations about Halla
organizing the PM's private parties.</i>

133
00:17:45,100 --> 00:17:55,780
<i>Looks like Halla filmed various orgies and kept them
inside a blue bag which she called "life insurance".</i>

134
00:18:10,900 --> 00:18:15,440
<i>Gunna. Technologu guru
and also my landlord.</i>

135
00:18:15,900 --> 00:18:20,300
<i>Daytime, she runs a
marketplace of cat puppets.</i>

136
00:18:20,300 --> 00:18:24,660
<i>Nighttime, she buys all kind
of shit from the internet.</i>

137
00:18:25,020 --> 00:18:29,260
<i>She's a compulsive shopper
with a strange obsession with cats.</i>

138
00:18:29,260 --> 00:18:35,380
<i>But she managed to hack a technology giant
which somehow had crossed her.</i>

139
00:18:36,060 --> 00:18:39,460
Can you trace these plates for me?

140
00:18:48,780 --> 00:18:54,540
It's registered to AVIS.
- Can you see who rent it?

141
00:18:58,400 --> 00:19:04,940
Home Office.
- Is there a list of employees with pictures?

142
00:19:08,660 --> 00:19:11,820
Stop!
It's him.

143
00:19:11,820 --> 00:19:15,540
Þorlákur Pálsson, driver at the Ministry.

144
00:19:15,540 --> 00:19:20,900
Whose driver in particular?
- A woman named Dagbjört.

145
00:19:21,060 --> 00:19:28,700
<i>Of course... the mandatory "gender ratio"
which got Sverrir reelected last year.</i>

146
00:19:28,700 --> 00:19:31,700
Thank you.
Good night.

147
00:19:40,900 --> 00:19:44,220
You know anything about a blue bag?

148
00:19:44,220 --> 00:19:47,300
What blue bag?
- I'm asking you.

149
00:19:47,300 --> 00:19:51,740
I know nothing about that.
- What is it you know, Sæmi?

150
00:19:51,740 --> 00:19:56,040
You're giving me nothing to work with.

151
00:19:56,040 --> 00:20:01,420
How many times do I have to tell you?
I'm not a murderer!

152
00:20:02,060 --> 00:20:06,260
<i>Sæmi knows nothing about the bag,
let alone about Halla.</i>

153
00:20:06,260 --> 00:20:09,260
<i>I gotta get hold of her videos</i>.

154
00:20:09,260 --> 00:20:13,140
Did you get the lab results on the semen?

155
00:20:14,500 --> 00:20:19,260
Yes and no... it's not Sæmi's.
- No? Whose then?

156
00:20:19,260 --> 00:20:25,180
We don't know, it's not on record.
- So does the murderer.

157
00:20:26,000 --> 00:20:29,020
We'll look into that.

158
00:20:29,340 --> 00:20:34,580
She might have had sex earlier that day.

159
00:20:34,580 --> 00:20:38,940
Did you check her apartment?
- Forensics just finished.

160
00:20:38,940 --> 00:20:42,620
What did they find?
- Nothing related to the murder.

161
00:20:42,620 --> 00:20:46,580
Not a blue bag by any chance?

162
00:20:46,860 --> 00:20:52,140
What's that?
- A blue bag. Did it turn up?

163
00:20:52,140 --> 00:20:57,100
No, we weren't the only ones there.

164
00:20:57,100 --> 00:21:00,580
No blue bag then.
- No.

165
00:21:21,860 --> 00:21:26,020
Good morning. Who's next?
- I am.

166
00:21:26,020 --> 00:21:28,180
Name?
- Daniel Scheving.

167
00:21:28,180 --> 00:21:33,510
I'm afraid the appointment has been cancelled.
- I need to see her now!

168
00:21:33,510 --> 00:21:37,940
I'm very sorry.
- I crossed the whole country!

169
00:21:38,200 --> 00:21:41,940
You can schedule another appointment.
- Fuck it...

170
00:21:46,260 --> 00:21:51,460
Hi, I have an appointment.
I represent Daniel Scheving.

171
00:21:51,980 --> 00:21:56,780
He had to leave so he called me.
- She'll be right with you.

172
00:21:56,880 --> 00:22:01,100
That's been the point from the very start.

173
00:22:01,200 --> 00:22:07,180
Yes, we do agree.
I'm well aware of that.

174
00:22:07,980 --> 00:22:11,860
Someone's coming in.
Thanks.

175
00:22:13,300 --> 00:22:17,940
Sorry, Daniel.
We're always very busy here.

176
00:22:21,580 --> 00:22:27,740
You're not Daniel.
- He had to leave so I snuck in.

177
00:22:28,660 --> 00:22:33,940
Thanks for calling me last night
but I thought you'd sooner see me in person.

178
00:22:34,940 --> 00:22:40,500
I've read your letter.
Perhaps you got more to tell me.

179
00:22:41,460 --> 00:22:44,180
I'm listening.

180
00:22:51,620 --> 00:22:56,220
These are very strong allegations.

181
00:22:59,140 --> 00:23:02,100
I know nothing about it.

182
00:23:02,540 --> 00:23:07,180
I'd be careful talking about
this in public though.

183
00:23:07,780 --> 00:23:10,460
You know nothing, do you?

184
00:23:10,560 --> 00:23:20,740
Just a small tip... make sure your messenger
doesn't leave any trace that could lead to you.

185
00:23:25,460 --> 00:23:30,980
Will you show my guest out, please?
Thanks.

186
00:23:33,620 --> 00:23:39,820
I thought we could become friends.
- We'll see.

187
00:23:42,860 --> 00:23:49,340
Until then, I suggest you find the videos
mentioned in the letter.

188
00:23:49,340 --> 00:23:54,180
If it's any help, the blue bag is the key.

189
00:23:54,180 --> 00:23:59,060
What's this bag look like?
- No one knows.

190
00:24:06,980 --> 00:24:09,780
Thanks for the chat.

191
00:24:20,980 --> 00:24:25,180
Gunna, I need a tracker and a blue bag.

192
00:24:25,180 --> 00:24:28,780
Search through all that junk you have.

193
00:24:28,780 --> 00:24:32,220
It's gotta be blue, ok?

194
00:24:32,580 --> 00:24:36,980
I'll meet you on the back of
the Home Office building.

195
00:24:43,180 --> 00:24:47,740
Here.
- What's this? I asked for a tracker.

196
00:24:47,820 --> 00:24:51,620
It's got "Find My Friends" and a GPS.

197
00:24:51,620 --> 00:24:58,220
Stella... it's the XXI century. With one
of those you're basically James Bond.

198
00:24:58,220 --> 00:25:03,540
It should last a couple of days in low battery mode.
- And the bag?

199
00:25:07,860 --> 00:25:11,780
Don't you have anything else?

200
00:25:17,780 --> 00:25:21,100
I wanna see Halla's apartment.

201
00:25:21,420 --> 00:25:25,700
What for?
- So I can do your job for you.

202
00:25:26,700 --> 00:25:30,140
Now?
- Yes, now.

203
00:25:30,140 --> 00:25:32,020
Dammit...

204
00:25:44,660 --> 00:25:47,060
Shit...

205
00:25:49,500 --> 00:25:52,580
It's better if you wait here.

206
00:25:54,500 --> 00:25:57,660
No, no... I go first!

207
00:26:00,060 --> 00:26:01,860
Police!

208
00:26:02,340 --> 00:26:07,540
Who are you? What are you doing here?
- Vacuuming.

209
00:26:07,540 --> 00:26:10,940
Who are you?
- Lena.

210
00:26:13,540 --> 00:26:17,340
How did you get in?
The door was locked.

211
00:26:17,340 --> 00:26:19,500
I used my key.

212
00:26:19,500 --> 00:26:23,680
You can't break into a place
that's been sealed off by the police!

213
00:26:23,680 --> 00:26:27,740
You're the intruders,
this is my house!

214
00:26:27,740 --> 00:26:32,300
Come again?
- Halla was renting. She was my niece.

215
00:26:32,300 --> 00:26:37,640
She can stay then... this isn't a crime scene
so you can't seal it off.

216
00:26:37,640 --> 00:26:43,100
Yes, yes... you got any ID
so I know who you are?

217
00:26:48,660 --> 00:26:50,260
Thanks.

218
00:26:55,020 --> 00:26:58,980
Raggi is harmless... unless
you're wrapped in bacon.

219
00:26:58,980 --> 00:27:04,380
Are you a cop too?
- No, I'm a lawyer.

220
00:27:09,260 --> 00:27:14,060
Did you know Halla well?
- Of course.

221
00:27:14,060 --> 00:27:19,180
I raised her after her parents died.

222
00:27:19,290 --> 00:27:25,860
Did you remain in contact?
- We used to see each other a couple of times a month.

223
00:27:27,140 --> 00:27:32,260
She was still playing with Barbie?
- She collected those.

224
00:27:34,100 --> 00:27:39,740
Her parents used to bring her one back
every time they went abroad.

225
00:27:40,580 --> 00:27:43,180
Until they...

226
00:27:44,300 --> 00:27:48,580
Well... they're reunited now.

227
00:27:48,580 --> 00:27:53,140
<i>Yes... pretty little Barbie.</i>

228
00:27:54,940 --> 00:27:59,540
<i>Auntie doesn't seem to know Halla that well.</i>

229
00:27:59,640 --> 00:28:04,900
I'm glad they caught the
monster who killed her.

230
00:28:06,020 --> 00:28:10,260
Did they?
- Wasn't it him?

231
00:28:10,360 --> 00:28:13,620
Ok, we're good.

232
00:28:14,180 --> 00:28:23,740
You shouldn't stay here anyway.
There's an investigation going on.

233
00:28:23,740 --> 00:28:27,540
I thought you got the murderer.

234
00:28:27,540 --> 00:28:32,420
I hope we did.
- She doesn't seem so sure.

235
00:28:32,420 --> 00:28:36,700
Of course not, she's a lawyer.

236
00:28:37,300 --> 00:28:41,900
She's paid to raise doubt.
<i>- Thanks, Raggi.</i>

237
00:28:45,460 --> 00:28:51,060
<i>The trap is set,
let's wait for someone to walk into it.</i>

238
00:28:53,860 --> 00:28:59,460
<i>I'll try to speed things up
poking her colleagues.</i>

239
00:29:05,140 --> 00:29:11,540
We can't give up now.
- Haukur! It's so nice to see you!

240
00:29:13,740 --> 00:29:18,980
You know her?
- Yes, you are... Stella?

241
00:29:19,660 --> 00:29:24,900
Haukur helped me with an exam a couple of year ago.
- Is that right?

242
00:29:24,980 --> 00:29:29,900
What was that about?
- Some law stuff...

243
00:29:29,900 --> 00:29:33,100
Haukur is really good at that.

244
00:29:33,100 --> 00:29:37,820
Prime Minister, how are you?
- You don't have to answer.

245
00:29:37,820 --> 00:29:42,340
What are you doing here?
- My job. And you?

246
00:29:42,340 --> 00:29:48,500
Why don't you show your friend out, Haukur?

247
00:29:56,860 --> 00:30:01,420
There's a lot of tension.
- The situation is not easy.

248
00:30:01,420 --> 00:30:06,700
You got a minute?
I actually came to see you.

249
00:30:08,380 --> 00:30:10,540
Get in here.

250
00:30:16,220 --> 00:30:20,420
It's terrible... and you defend that man?

251
00:30:21,500 --> 00:30:25,780
Has he confessed?
- He says he didn't do it.

252
00:30:25,890 --> 00:30:31,440
He was on top of her covered in blood.
- Innocent until proved guilty, isn't it?

253
00:30:33,580 --> 00:30:36,660
But I'm here to talk about the victim.

254
00:30:36,660 --> 00:30:40,700
Halla? What's to say, it's all...

255
00:30:41,180 --> 00:30:46,460
I still can't believe it.
- But you were both PM's assistants.

256
00:30:46,460 --> 00:30:52,580
Weren't you in contact all the time?
- Not really... we only talked work.

257
00:30:52,580 --> 00:30:56,420
We had different duties.
- Was she good at her job?

258
00:31:04,900 --> 00:31:07,620
Off the record?

259
00:31:08,380 --> 00:31:13,220
The PM was thinking of replacing her.
- Why?

260
00:31:13,900 --> 00:31:17,820
She liked... private parties.

261
00:31:18,900 --> 00:31:21,620
I don't wanna...

262
00:31:22,180 --> 00:31:27,460
I don't wanna speak ill
of a dead person but...

263
00:31:27,460 --> 00:31:31,980
...she was always jacked up
when she got out of the bathroom.

264
00:31:31,980 --> 00:31:37,820
I'm not pointing fingers but...
I don't think it was from milk and coffee.

265
00:31:37,990 --> 00:31:46,500
Say... Edda Kristjáns, your boss' sister,
tried to take the case away from me.

266
00:31:46,660 --> 00:31:51,100
Edda?
I know nothing abut that.

267
00:31:52,940 --> 00:31:59,500
Now you're the only assistant left,
can you get me an appointment with him?

268
00:32:00,820 --> 00:32:05,680
I don't know... he's very busy.
I'll try.

269
00:32:05,680 --> 00:32:10,380
Where was he the night of the murder?
- The PM?

270
00:32:10,480 --> 00:32:14,260
Home, by himself.

271
00:32:15,060 --> 00:32:21,540
<i>Hand on his mouth... he's lying.</i>
- I gotta go now.

272
00:32:23,340 --> 00:32:25,660
Good luck.

273
00:32:29,180 --> 00:32:32,420
<i>All roads seem to lead to Sverrir.</i>

274
00:32:32,420 --> 00:32:36,740
<i>His crew is being very tight.</i>

275
00:32:38,380 --> 00:32:41,020
<i>Hello, bait!</i>

276
00:32:43,940 --> 00:32:46,580
Tolli, are you free?

277
00:32:52,540 --> 00:32:55,900
<i>Cab driver Tolli helps me
until I have my own car.</i>

278
00:32:55,900 --> 00:33:02,500
<i>He's an old client,
I helped him with an insurance settlement.</i>

279
00:33:02,820 --> 00:33:08,100
<i>My bait is in the middle of nowhere.
I hope this isn't a hole in the water.</i>

280
00:33:11,700 --> 00:33:16,400
I can't go any further.
- Thanks, put it on the tab.

281
00:33:16,400 --> 00:33:18,620
No problem, Stella.

282
00:33:37,740 --> 00:33:43,060
<i>Nice place... these boy
can really cheer you up.</i>

283
00:33:46,620 --> 00:33:50,060
Johnny. Straight.
- What?

284
00:33:50,940 --> 00:33:54,140
Johnny. Black Label.

285
00:33:59,740 --> 00:34:04,340
<i>Well... some things never stop amazing me.</i>

286
00:34:04,340 --> 00:34:09,380
<i>Wait a second... Porn Valdi!</i>

287
00:34:09,820 --> 00:34:16,620
<i>He got his nickname when he was importing
strippers for his only-men erotic club.</i>

288
00:34:16,620 --> 00:34:22,610
<i>He's got his hands into all kinds of stuff
but he never got arrested.</i>

289
00:34:25,420 --> 00:34:36,460
<i>He tried to improve his image by doing "confidential
jobs" for his best buddy, PM Sverrir Kristiánsson.</i>

290
00:34:38,020 --> 00:34:42,740
<i>I wouldn't be shocked to learn
he's involved in the murder.</i>

291
00:34:57,620 --> 00:35:01,020
<i>Am I speaking with the
PM's friends and family?</i>

292
00:35:01,020 --> 00:35:05,500
I'm at the bar.
I just wanted to say hi.

293
00:35:33,780 --> 00:35:38,140
What do you want?
- Thanks for finding my bag.

294
00:35:38,140 --> 00:35:41,020
I don't know what you mean.
- Stop fucking around.

295
00:35:41,020 --> 00:35:46,900
You or one of yours broke into Halla's
apartment. Are you covering for your brother?

296
00:35:46,900 --> 00:35:51,300
Stop following me or you'll regret it.

297
00:35:51,300 --> 00:35:55,000
Threatening someone is a felony.

298
00:35:55,000 --> 00:35:58,940
I know the law.
- Why were you with Valdi?

299
00:35:58,940 --> 00:36:03,180
He and your brother are buddies, aren't they?

300
00:36:04,780 --> 00:36:07,940
You're a smart woman...

301
00:36:07,940 --> 00:36:14,620
...but it's time you realize you got
yourself into something way bigger than you.

302
00:36:15,060 --> 00:36:20,860
Don't think you can use this case
for your own profit.

303
00:36:20,860 --> 00:36:25,500
It's out of your reach.
- That went for Halla too?

304
00:36:31,580 --> 00:36:37,780
<i>What's the meaning of Edda and Valdi's
meeting in respect to this big case?</i>

305
00:37:20,140 --> 00:37:23,060
Hello.
- Good evening.

306
00:37:27,300 --> 00:37:33,020
We got a call about some suspicious activities.
- Yes, someone broke in.

307
00:37:41,940 --> 00:37:47,940
You live here?
- Yes. Who called you? Gunna?

308
00:37:47,940 --> 00:37:50,100
Gunna who?
- From upstairs.

309
00:37:50,100 --> 00:37:54,100
Yes, it was Gunna.

310
00:37:55,740 --> 00:37:59,860
We'll take you to the station for the report.

311
00:37:59,860 --> 00:38:04,700
Can't we do it here?
- It's better over there.

312
00:38:04,700 --> 00:38:06,860
Ok.

313
00:38:07,220 --> 00:38:11,820
Is Raggi on duty tonight?
- Yes.

314
00:38:12,140 --> 00:38:17,420
Let's go get this over with.
- Ok.

315
00:38:32,100 --> 00:38:35,980
Aren't we going the wrong direction?

316
00:38:44,000 --> 00:38:47,740
We forgot about the cellphone!

317
00:38:52,100 --> 00:38:55,140
Give it to me!

318
00:39:08,300 --> 00:39:11,580
Stay calm now!

319
00:39:31,980 --> 00:39:36,460
Have you ever touched a woman before?

320
00:40:24,780 --> 00:40:28,220
We stop for a bite.

321
00:40:35,100 --> 00:40:40,060
So... where's the blue bag?
- What bag?

322
00:40:45,340 --> 00:40:49,580
Don't try me, bitch... where is it?

323
00:40:50,300 --> 00:40:53,980
Ok, ok, I'll tell you!

324
00:40:56,420 --> 00:41:06,300
Drive to Borgarvegur... take the first on the
right at the roundabout and get out at Spöngin.

325
00:41:07,620 --> 00:41:12,180
Then... keep driving up your mom's ass.

326
00:41:12,860 --> 00:41:19,060
You're very funny.
- I can go on all night.

327
00:41:25,020 --> 00:41:27,540
You lost something?

328
00:41:29,500 --> 00:41:32,620
You're not going anywhere.

329
00:41:45,180 --> 00:41:50,220
What's on your face...?
- Fuck you! Go get her!

330
00:41:55,660 --> 00:41:59,060
Did you see which way she went?

331
00:41:59,980 --> 00:42:03,420
Let's just get out of here.

332
00:42:18,980 --> 00:42:22,900
<b><i>ENGLISH SUBTITLES BY BARISHNIKOV@KATCR.CO</i></b>

