﻿1
00:00:00,023 --> 00:00:02,131
<i>Previously on The Last Man on Earth...</i>

2
00:00:02,156 --> 00:00:03,984
Let's have Todd put my baby in you.

3
00:00:04,461 --> 00:00:06,229
No friggin' way.

4
00:00:06,263 --> 00:00:09,087
Mike Shelby Miller,
will you be our sperm?

5
00:00:09,347 --> 00:00:10,500
I-I'd be honored.

6
00:00:10,979 --> 00:00:12,336
Friggin' ridiculous.

7
00:00:12,369 --> 00:00:14,053
(laughs) I'm pregnant!

8
00:00:14,078 --> 00:00:15,345
(squeals, laughs)

9
00:00:15,379 --> 00:00:16,413
Oh!

10
00:00:18,449 --> 00:00:19,918
(buzzing quietly)

11
00:00:24,216 --> 00:00:25,942
I still can't believe I'm not sterile.

12
00:00:25,967 --> 00:00:27,176
(both chuckle)

13
00:00:27,374 --> 00:00:29,777
You didn't masturbate
too much, after all.

14
00:00:29,897 --> 00:00:31,299
Yeah, it was just the right amount.

15
00:00:31,502 --> 00:00:32,245
Thank you.

16
00:00:32,270 --> 00:00:35,240
I am so proud of your sperm.

17
00:00:35,490 --> 00:00:36,758
I am, too.

18
00:00:37,312 --> 00:00:38,796
Guys, guys.

19
00:00:39,024 --> 00:00:41,032
Gail, Carol is pregnant.

20
00:00:41,040 --> 00:00:43,190
Yeah? Okay, good.
Whatever. Good for you.

21
00:00:43,215 --> 00:00:45,017
You're not gonna believe the crazy thing

22
00:00:45,042 --> 00:00:46,256
I just saw out there.

23
00:00:46,371 --> 00:00:48,106
It was a flying record player!

24
00:00:48,670 --> 00:00:51,268
It just came out of the
sky, came right at me!

25
00:00:51,293 --> 00:00:52,638
It was like... (yells)

26
00:00:52,748 --> 00:00:54,782
Like when you go to, um,

27
00:00:54,807 --> 00:00:56,785
you know, Waffle House. It was like...

28
00:00:57,168 --> 00:00:58,970
it was flying skillets.

29
00:00:59,043 --> 00:01:00,544
It was, uh, no. It was like...

30
00:01:00,765 --> 00:01:03,352
two hovering griddles.

31
00:01:03,452 --> 00:01:06,063
Gail, honey, how much have
you had to drink today?

32
00:01:06,088 --> 00:01:07,311
A lady never tells.

33
00:01:07,341 --> 00:01:08,732
Gail, I think everybody's just saying

34
00:01:08,757 --> 00:01:10,472
that sometimes you drink a little bit.

35
00:01:10,534 --> 00:01:11,534
A lot a bit.

36
00:01:11,670 --> 00:01:14,215
Well, maybe you're just a little
confused about what you saw.

37
00:01:14,240 --> 00:01:15,620
I know exactly what I saw out there!

38
00:01:15,645 --> 00:01:19,301
There was a whole mess of-of
floating hair dryers,

39
00:01:19,326 --> 00:01:21,327
and they were staring right at me!

40
00:01:23,552 --> 00:01:25,770
(slurring): Do y'all really
think I "mrink" too much?

41
00:01:26,282 --> 00:01:27,365
Well, I think you might have answered

42
00:01:27,390 --> 00:01:28,559
your own question there.

43
00:01:34,975 --> 00:01:36,669
Oh, farts.

44
00:01:49,285 --> 00:01:51,287
TANDY: Mmm.

45
00:01:51,312 --> 00:01:52,520
(laughs)

46
00:01:52,545 --> 00:01:55,182
Mmm, mmm, mmm, mmm, mmm, mmm.

47
00:01:56,325 --> 00:01:57,588
Mmm.

48
00:01:58,393 --> 00:02:00,926
TODD: What the hell's going on in here?

49
00:02:04,102 --> 00:02:05,277
Nothing.

50
00:02:06,812 --> 00:02:07,968
Really?

51
00:02:09,823 --> 00:02:13,494
'Cause it doesn't smell like nothing.

52
00:02:15,299 --> 00:02:16,632
Mike farted.

53
00:02:16,754 --> 00:02:17,593
Yeah.

54
00:02:17,618 --> 00:02:18,954
Yeah, I-I farted.

55
00:02:19,153 --> 00:02:21,223
(laughs) Oh.

56
00:02:24,224 --> 00:02:25,926
That's funny.

57
00:02:26,009 --> 00:02:31,169
'Cause your farts smell
like freshly cooked bacon.

58
00:02:33,060 --> 00:02:34,450
Thank you.

59
00:02:35,763 --> 00:02:37,108
TANDY: Todd, we're adults here,

60
00:02:37,133 --> 00:02:39,255
and I'm gonna be honest
with you about this.

61
00:02:39,563 --> 00:02:41,291
Mike and I just finished off the bacon.

62
00:02:42,893 --> 00:02:45,084
Uh, but I-I did fart.

63
00:02:47,579 --> 00:02:48,832
You guys finished the bacon.

64
00:02:48,865 --> 00:02:50,467
MIKE: Mm-hmm.

65
00:02:50,499 --> 00:02:51,961
That's cool.

66
00:02:52,021 --> 00:02:54,091
Thank you, Todd.

67
00:02:56,563 --> 00:02:58,219
You want, uh, any cracklins, braheem?

68
00:02:58,244 --> 00:03:02,816
(shouting): No, I don't want
any friggin' cracklins, braheem!

69
00:03:04,787 --> 00:03:08,951
You know, I was wondering
if it ever crossed your mind

70
00:03:08,985 --> 00:03:11,568
to consult with me first.

71
00:03:11,593 --> 00:03:14,095
You know, I'm guessing it didn't, Tandy.

72
00:03:14,267 --> 00:03:16,460
Did it? (chuckles softly)

73
00:03:16,867 --> 00:03:17,868
Did it?!

74
00:03:19,809 --> 00:03:23,272
I found this bacon.

75
00:03:24,928 --> 00:03:26,402
- We ran this by the group...
- Yeah.

76
00:03:26,507 --> 00:03:28,004
And everyone agreed

77
00:03:28,296 --> 00:03:30,066
that Mike should get
to finish off the bacon,

78
00:03:30,091 --> 00:03:31,693
'cause he hasn't had any yet.

79
00:03:31,718 --> 00:03:33,119
Everyone?

80
00:03:33,390 --> 00:03:34,983
That's funny, 'cause
last time I checked,

81
00:03:35,008 --> 00:03:37,945
I was part and parcel of "everyone."

82
00:03:38,052 --> 00:03:40,049
We looked for you everywhere,

83
00:03:40,082 --> 00:03:41,677
and we couldn't find you, bud.

84
00:03:41,702 --> 00:03:43,785
Well, you didn't look hard enough, bud.

85
00:03:52,206 --> 00:03:53,875
So where are you from in Australia?

86
00:03:53,962 --> 00:03:55,723
- All over. Yeah.
- Oh, yeah? Yeah?

87
00:03:55,748 --> 00:03:56,596
I've actually, um,

88
00:03:56,621 --> 00:03:58,171
- I've been to Adelaide. Yeah.
- Yeah. Yeah.

89
00:03:58,196 --> 00:04:00,557
I actually spent a bit
of time in Adelaide.

90
00:04:00,582 --> 00:04:03,021
Oh, well, then, yeah, you know.
Wh-What were you doing there?

91
00:04:03,857 --> 00:04:05,175
Okay, um...

92
00:04:05,414 --> 00:04:06,342
(chuckles)

93
00:04:06,367 --> 00:04:11,213
I should say that I<i> did</i>
a bit of time in Adelaide.

94
00:04:12,518 --> 00:04:13,804
- I was in jail.
- Oh.

95
00:04:14,915 --> 00:04:15,919
Wow.

96
00:04:15,952 --> 00:04:19,851
I somehow got arrested,
uh, for leaving a bank

97
00:04:19,876 --> 00:04:21,478
with some money that wasn't mine.

98
00:04:21,503 --> 00:04:22,569
- Uh-huh. -
And then a man

99
00:04:22,594 --> 00:04:25,130
in a uniform found a gun on me

100
00:04:25,222 --> 00:04:27,291
that turned out to be mine.

101
00:04:27,316 --> 00:04:29,518
So you're talking about armed robbery.

102
00:04:29,543 --> 00:04:31,875
Oh, more like an armed misunderstanding.

103
00:04:31,900 --> 00:04:33,402
And how did that go over in court?

104
00:04:33,436 --> 00:04:36,512
Well, as I said, I did a bit
of time in Adelaide. (laughs)

105
00:04:36,537 --> 00:04:38,136
Mm-hmm, mm-hmm,
yeah. (laughs)

106
00:04:38,161 --> 00:04:39,205
- Yeah.
- So, well,

107
00:04:39,230 --> 00:04:40,893
then how'd you end up, you
know, here in the States?

108
00:04:41,010 --> 00:04:42,721
Oh, I got a job at the State Department.

109
00:04:42,885 --> 00:04:44,541
They hired you with a criminal record?

110
00:04:44,566 --> 00:04:45,721
They didn't hire me.

111
00:04:45,746 --> 00:04:48,086
(American accent): They
hired Amanda Williams

112
00:04:48,111 --> 00:04:49,822
from Cleveland, Ohio.

113
00:04:49,847 --> 00:04:51,552
Majored in political science

114
00:04:51,577 --> 00:04:53,788
at<i> the</i> Ohio State University.

115
00:04:53,789 --> 00:04:55,190
Go Pi Phi!

116
00:04:55,258 --> 00:04:56,064
Go! Whoo!

117
00:04:56,089 --> 00:04:57,572
That's amaz... you know, you're like

118
00:04:57,574 --> 00:04:59,997
- the most interesting person alive.
- Nah.

119
00:05:00,022 --> 00:05:01,490
Yeah, does everybody
else know about this?

120
00:05:01,514 --> 00:05:02,363
Hell no.

121
00:05:02,364 --> 00:05:03,733
What are you talk... why not?

122
00:05:03,939 --> 00:05:05,416
They've never asked me.

123
00:05:05,541 --> 00:05:07,269
It's like they just latched
onto the Australia thing

124
00:05:07,294 --> 00:05:08,630
and stopped right there.

125
00:05:08,655 --> 00:05:11,082
(Australian accent): Cheers,
Erica! Dingoes ate my baby!

126
00:05:11,107 --> 00:05:12,108
Didgeridoo!

127
00:05:12,409 --> 00:05:13,038
(normal voice): What's up, Mike?

128
00:05:13,063 --> 00:05:14,443
Hey, Phil. G'day.

129
00:05:14,476 --> 00:05:15,611
Yep.

130
00:05:19,675 --> 00:05:20,996
Hey, bud.

131
00:05:21,699 --> 00:05:23,191
Thinking about going out

132
00:05:23,216 --> 00:05:25,277
and burning down the Santa Monica Pier.

133
00:05:25,472 --> 00:05:26,472
You want to come with?

134
00:05:26,777 --> 00:05:29,043
Yeah, what, was your bacon brother busy?

135
00:05:29,192 --> 00:05:30,594
(sighs)

136
00:05:30,777 --> 00:05:32,195
Come on, T.

137
00:05:32,495 --> 00:05:34,248
You're my original bacon brother.

138
00:05:34,273 --> 00:05:35,941
I'm talking OBB.

139
00:05:35,966 --> 00:05:38,803
And you want to hazard a guess
as to who's down with OBB?

140
00:05:39,369 --> 00:05:40,372
Huh?

141
00:05:41,737 --> 00:05:43,138
Every last Tandy.

142
00:05:43,171 --> 00:05:43,933
Oh, come on.

143
00:05:43,958 --> 00:05:45,422
Tandy, ever since Mike showed up,

144
00:05:45,447 --> 00:05:46,861
you don't have time for me anymore.

145
00:05:46,886 --> 00:05:48,161
Oh, don't you dare.

146
00:05:48,223 --> 00:05:49,958
Uh, every time I try
to hang out with you,

147
00:05:50,022 --> 00:05:51,955
you're always either
with Gail or Melissa.

148
00:05:51,980 --> 00:05:53,867
- That is not true!
- Oh, yes, it is.

149
00:05:53,892 --> 00:05:55,655
You're just like one of
those guys who as soon

150
00:05:55,680 --> 00:05:58,120
as they get two girlfriends,
they just disappear.

151
00:05:58,145 --> 00:05:58,931
No, you know what?

152
00:05:58,956 --> 00:06:00,505
I'm your backup plan.

153
00:06:00,645 --> 00:06:01,769
Your number two.

154
00:06:01,794 --> 00:06:03,651
And it's totally appropriate, Tandy,

155
00:06:03,676 --> 00:06:05,921
because that's exactly
how you make me feel.

156
00:06:06,475 --> 00:06:08,798
Like a big old number two.

157
00:06:08,960 --> 00:06:10,900
And you know where you
dropped that stinky?

158
00:06:11,561 --> 00:06:12,837
Right here.

159
00:06:14,243 --> 00:06:16,005
On my heart.

160
00:06:21,351 --> 00:06:24,421
Yes, yes. Here we are. Okay.

161
00:06:25,227 --> 00:06:27,664
You still waiting on
your date there, ma'am?

162
00:06:27,730 --> 00:06:29,640
Oh, I lied about that
to keep the creeps away.

163
00:06:29,718 --> 00:06:30,698
Really?

164
00:06:30,736 --> 00:06:31,962
Would you mind if I joined you, then?

165
00:06:31,987 --> 00:06:33,254
I don't know. Are you a creep?

166
00:06:33,760 --> 00:06:34,770
Yeah, I think so.

167
00:06:34,802 --> 00:06:37,078
(laughs): Well, in that case, sit down.

168
00:06:37,103 --> 00:06:39,559
All right, let that be there.

169
00:06:39,609 --> 00:06:40,563
Yeah.

170
00:06:40,588 --> 00:06:42,569
Although, I feel like I should tell you,

171
00:06:42,594 --> 00:06:44,263
I am carrying another man's baby.

172
00:06:44,547 --> 00:06:45,949
You are? Wow.

173
00:06:46,014 --> 00:06:47,415
Well, that's complicated.

174
00:06:47,448 --> 00:06:48,153
On the plus side,

175
00:06:48,178 --> 00:06:50,747
I am the only single
woman left on the planet.

176
00:06:50,771 --> 00:06:52,840
Ah. Okay. See, I can work with that.

177
00:06:52,865 --> 00:06:53,615
That's nice, yeah.

178
00:06:53,640 --> 00:06:55,080
Well, I guess, if we're
being honest with each other,

179
00:06:55,105 --> 00:06:57,094
I should let you know
that I'm, uh, married.

180
00:06:57,119 --> 00:06:58,953
- Chef Miyagi!
- Mm-hmm.

181
00:06:58,978 --> 00:07:00,763
Yeah, but it's okay. I'm
getting a divorce soon.

182
00:07:00,788 --> 00:07:02,783
Yeah, my brother's actually
sleeping with my wife.

183
00:07:02,817 --> 00:07:04,252
(laughs)

184
00:07:11,717 --> 00:07:13,987
(groans) Water.

185
00:07:14,227 --> 00:07:15,829
It burns my throat.

186
00:07:16,063 --> 00:07:17,631
I just don't like it.

187
00:07:17,665 --> 00:07:19,038
You'll get used to it.

188
00:07:19,069 --> 00:07:20,982
Maybe I'll try putting an olive in it.

189
00:07:21,007 --> 00:07:22,803
Transition into it, you know?

190
00:07:22,836 --> 00:07:24,647
(groans) Hello.

191
00:07:24,672 --> 00:07:27,116
Oh, Geez of Nazareth!

192
00:07:27,580 --> 00:07:28,863
Man, you got to stop

193
00:07:28,888 --> 00:07:31,391
coming up on me like that, Carol.

194
00:07:31,472 --> 00:07:34,081
I'm gonna start making people
wear bells around their necks.

195
00:07:34,932 --> 00:07:37,485
Uh, Gail's trying to stop
drinking, so she's a little...

196
00:07:37,564 --> 00:07:39,921
well, you saw her.

197
00:07:40,361 --> 00:07:42,090
Well, good for you, Gail.

198
00:07:42,122 --> 00:07:44,825
And as a show of solidarity,
I'm gonna join you.

199
00:07:44,939 --> 00:07:46,460
I can't drink, anyway.

200
00:07:46,626 --> 00:07:48,333
Baby on board. Beep, beep!

201
00:07:48,358 --> 00:07:49,459
Out of my way!

202
00:07:49,530 --> 00:07:51,366
(laughs) Yeah.

203
00:07:51,398 --> 00:07:52,958
Not drinking for two now.

204
00:07:52,983 --> 00:07:54,602
Well, good luck with that.

205
00:07:54,634 --> 00:07:56,370
Looks like I'm drinking for four now.

206
00:08:06,436 --> 00:08:08,090
Hey, this is actually pretty easy.

207
00:08:08,115 --> 00:08:10,684
Yeah, that motion seems to
come to you pretty naturally.

208
00:08:10,718 --> 00:08:11,985
(laughs)

209
00:08:12,019 --> 00:08:13,487
(coughing)

210
00:08:14,190 --> 00:08:14,948
Doing okay?

211
00:08:14,973 --> 00:08:16,001
Yeah, yeah. No, I'm fine.

212
00:08:16,026 --> 00:08:17,434
Just a little tuckered out, is all.

213
00:08:17,459 --> 00:08:18,420
You're a little tuckered out?

214
00:08:18,445 --> 00:08:20,365
That's funny, I thought
your name was Mike. Boom.

215
00:08:20,585 --> 00:08:21,327
Solid.

216
00:08:21,352 --> 00:08:22,588
(chuckles): I know.

217
00:08:22,913 --> 00:08:23,921
Hey, bud.

218
00:08:24,890 --> 00:08:25,797
I just want to thank you

219
00:08:25,822 --> 00:08:27,577
for having sex with Carol and stuff.

220
00:08:27,602 --> 00:08:29,136
It-it meant a lot to me.

221
00:08:29,294 --> 00:08:31,247
To be honest with you, I'm
really excited to be an uncle.

222
00:08:31,272 --> 00:08:33,242
Well, you're gonna be a godfather, too.

223
00:08:33,493 --> 00:08:35,973
Well, I guess that's better than
being a godfather three. Boom.

224
00:08:36,083 --> 00:08:36,810
Solid.

225
00:08:37,036 --> 00:08:38,309
I know. (chuckles)

226
00:08:38,481 --> 00:08:39,714
So what do you say?

227
00:08:39,897 --> 00:08:42,015
You want to be our godfather?

228
00:08:42,218 --> 00:08:44,151
(imitating Vito Corleone):
It's an offer I can't refuse.

229
00:08:44,185 --> 00:08:45,998
I don't underst... why
are you talking like that?

230
00:08:46,023 --> 00:08:47,254
TODD: Well, well, well.

231
00:08:48,169 --> 00:08:50,018
Oh, hey, bud. How hangs it?

232
00:08:50,043 --> 00:08:51,739
I was just coming by to get some milk,

233
00:08:51,764 --> 00:08:54,195
but you guys probably already
dried her out, didn't you?

234
00:08:54,531 --> 00:08:56,163
Oh, come on, Toddler.

235
00:08:56,196 --> 00:08:57,664
I'm not a toddler.

236
00:08:57,843 --> 00:08:58,865
I'm a man.

237
00:08:58,898 --> 00:09:00,578
- Hey, Mike is my brother,
- (coughing)

238
00:09:00,603 --> 00:09:02,268
and I'm going to spend time with him,

239
00:09:02,293 --> 00:09:04,253
and I will not apologize for that.

240
00:09:04,278 --> 00:09:05,404
I'm sorry.

241
00:09:05,429 --> 00:09:07,574
Oh, yeah, make me seem
like the crazy one, huh?

242
00:09:07,608 --> 00:09:09,236
- (coughing)
- "Todd's upset at Tandy

243
00:09:09,261 --> 00:09:10,829
for hanging out with his brother."

244
00:09:10,854 --> 00:09:12,212
Huh? Is that how you're gonna spin this?

245
00:09:12,245 --> 00:09:13,540
- I'm not spinning it.
- (coughing continues)

246
00:09:13,565 --> 00:09:15,990
If anything, I'm keeping
it firmly in place!

247
00:09:16,015 --> 00:09:18,577
Okay, you know what? It's
time to face facts, Tandy.

248
00:09:18,602 --> 00:09:20,647
Here's the thing, ever
since he showed up,

249
00:09:20,672 --> 00:09:22,408
you don't have any time...

250
00:09:23,975 --> 00:09:25,344
Good God.

251
00:09:36,138 --> 00:09:38,899
It's just a cold, okay? Or allergies.

252
00:09:38,924 --> 00:09:40,959
But Todd said he coughed up blood!

253
00:09:40,992 --> 00:09:42,998
You know what? Uh, Todd's
just spreading rumors.

254
00:09:43,023 --> 00:09:44,000
Yeah, that's right.

255
00:09:44,025 --> 00:09:46,422
You know where he
works? At the rumor mill.

256
00:09:46,679 --> 00:09:48,130
Yeah, and, uh, you know what his name

257
00:09:48,155 --> 00:09:49,824
would be if Bruce Willis was his dad?

258
00:09:51,135 --> 00:09:52,746
- Rumer.
- It's not a rumor.

259
00:09:52,771 --> 00:09:55,015
Yes, it is. I mean,
this is clearly revenge

260
00:09:55,040 --> 00:09:56,973
'cause you're so jealous of me and Mike.

261
00:09:56,998 --> 00:09:59,334
And that, my friend,
is what's not a rumor.

262
00:09:59,359 --> 00:10:01,071
No, that's a Tallulah and Scout.

263
00:10:01,096 --> 00:10:02,187
Bruce Willis's other kids.

264
00:10:02,212 --> 00:10:04,368
Tandy, whatever is going on between us,

265
00:10:04,393 --> 00:10:05,484
this is bigger.

266
00:10:05,509 --> 00:10:07,473
Now, I saw it. It was blood.

267
00:10:07,498 --> 00:10:08,951
You need to tell the truth.

268
00:10:09,607 --> 00:10:10,231
Okay, okay.

269
00:10:10,256 --> 00:10:12,291
There might have been, uh, just a teeny,

270
00:10:12,316 --> 00:10:13,580
tiny little bit of blood.

271
00:10:13,605 --> 00:10:15,347
- (groans)
- But I've coughed up blood,

272
00:10:15,372 --> 00:10:16,419
like, a million times,

273
00:10:16,444 --> 00:10:18,671
and that doesn't mean that
I have the virus, right?

274
00:10:18,809 --> 00:10:19,950
He's fine.

275
00:10:19,975 --> 00:10:21,286
But does he have the rash?

276
00:10:21,311 --> 00:10:22,597
Not on his genitals.

277
00:10:22,622 --> 00:10:24,209
Sorry, Tandy. Just trying to help.

278
00:10:24,234 --> 00:10:25,402
(Mike coughing)

279
00:10:25,636 --> 00:10:27,310
- Oh.
- Where are you going?

280
00:10:27,335 --> 00:10:28,470
Away from him.

281
00:10:28,707 --> 00:10:30,676
Why are you all panicking?

282
00:10:30,709 --> 00:10:32,377
We're immune, remember?

283
00:10:32,410 --> 00:10:33,846
We're all safe.

284
00:10:34,506 --> 00:10:36,011
Not all of us.

285
00:10:36,949 --> 00:10:38,117
Oh, my God.

286
00:10:38,433 --> 00:10:39,918
What about our babies?

287
00:10:40,020 --> 00:10:42,175
TODD: There is no
guarantee that those babies

288
00:10:42,200 --> 00:10:43,585
will be immune to the virus.

289
00:10:43,610 --> 00:10:46,399
Yeah, Tandy. Do you want to
give the virus to the babies,

290
00:10:46,424 --> 00:10:47,857
you dumb son of a bitch?

291
00:10:47,882 --> 00:10:49,360
Oh, Gail, have a drink!

292
00:10:49,393 --> 00:10:50,862
(throwing her voice):
I agree with Erica.

293
00:10:50,896 --> 00:10:52,363
Who said that? Todd?

294
00:10:53,005 --> 00:10:54,915
Thank you for seconding
Erica's statement.

295
00:10:54,940 --> 00:10:56,804
I agree with Todd and Erica.

296
00:10:56,829 --> 00:10:59,966
Look, I am telling you, Mike is fine.

297
00:11:01,685 --> 00:11:02,791
I'm out of here.

298
00:11:04,201 --> 00:11:05,505
On your tail.

299
00:11:05,970 --> 00:11:07,462
- Wha...
- Wait for me.

300
00:11:07,487 --> 00:11:09,956
Takes me a little longer
to get up these days.

301
00:11:10,044 --> 00:11:12,548
Already on the second
trimester of my first week.

302
00:11:12,766 --> 00:11:13,683
You're going, too?

303
00:11:13,708 --> 00:11:15,261
Sorry, Tandy, but I don't think

304
00:11:15,286 --> 00:11:17,589
we can afford to take a chance here.

305
00:11:21,751 --> 00:11:23,380
So we're just gonna leave him, huh?

306
00:11:23,405 --> 00:11:25,642
We're just gonna let
him wake up all alone

307
00:11:25,667 --> 00:11:27,933
and try to piece together
what the hell happened?

308
00:11:28,112 --> 00:11:30,401
We can't be stupid about this.

309
00:11:30,435 --> 00:11:31,658
Well, maybe there's a way to be

310
00:11:31,683 --> 00:11:34,219
smart and stupid at the same time.

311
00:11:34,726 --> 00:11:38,310
(coughing)

312
00:11:58,619 --> 00:11:59,832
Sup, dawg?

313
00:12:00,057 --> 00:12:01,057
Don't be alarmed.

314
00:12:02,604 --> 00:12:04,184
Why on earth would I be alarmed?

315
00:12:04,209 --> 00:12:05,604
(chuckles) That's the spirit.

316
00:12:05,636 --> 00:12:07,140
Hey, why don't you come
on into the living room

317
00:12:07,165 --> 00:12:08,668
and have a seat, huh?

318
00:12:16,326 --> 00:12:18,495
ERICA: Oh, Jenga!

319
00:12:18,559 --> 00:12:19,619
(Tandy clears throat)

320
00:12:21,312 --> 00:12:22,890
Look who I found, huh?

321
00:12:23,609 --> 00:12:25,632
What do you say we go
join the group, huh?

322
00:12:26,500 --> 00:12:27,659
Right this way.

323
00:12:28,723 --> 00:12:30,417
Don't be shy.

324
00:12:30,442 --> 00:12:32,246
If you guys could make a little room.

325
00:12:32,485 --> 00:12:35,113
Nice of Melissa to clear
you a little spot there.

326
00:12:37,464 --> 00:12:38,569
Hey, Mike.

327
00:12:39,581 --> 00:12:41,011
Hi.

328
00:12:41,482 --> 00:12:43,007
So how was your little nappy-poo?

329
00:12:45,983 --> 00:12:47,451
Do I have the virus?

330
00:12:47,555 --> 00:12:49,056
Well, I mean, you know...

331
00:12:49,580 --> 00:12:50,983
Maybe.

332
00:12:51,623 --> 00:12:52,217
Okay, yeah.

333
00:12:52,250 --> 00:12:53,314
Look, l-look.

334
00:12:53,339 --> 00:12:55,141
These guys think that
there's, uh, you know,

335
00:12:55,166 --> 00:12:57,889
just a-a small possibility
that you might just have,

336
00:12:57,914 --> 00:13:00,085
like, a-a teeny-tiny
touch of the virus.

337
00:13:00,110 --> 00:13:02,070
- Just-just a smidgen, really.
- Okay. Okay, okay.

338
00:13:03,419 --> 00:13:04,763
I mean, we're not worried.

339
00:13:04,804 --> 00:13:06,105
I mean, not one bit.

340
00:13:07,396 --> 00:13:09,277
Yeah, you guys reek of confidence.

341
00:13:11,420 --> 00:13:13,289
So I assume that thing's for me, huh?

342
00:13:15,076 --> 00:13:18,313
Hey, look, we're doing this
'cause we care about you.

343
00:13:19,083 --> 00:13:21,412
We just have to be careful, you know?

344
00:13:21,656 --> 00:13:23,623
So why don't you just pop in there,

345
00:13:23,648 --> 00:13:25,225
ride out this cold,

346
00:13:25,589 --> 00:13:28,825
and one day, we'll all
laugh at this like hyenas.

347
00:13:29,054 --> 00:13:30,690
(chuckles)

348
00:13:32,106 --> 00:13:33,495
Where do I go to the bathroom?

349
00:13:33,520 --> 00:13:35,627
Oh, don't worry. We got
that all taken care of.

350
00:13:35,794 --> 00:13:37,317
(quietly): Todd, go get me a bucket.

351
00:13:38,036 --> 00:13:40,364
This is gonna be great, bud.

352
00:13:40,397 --> 00:13:42,342
Yeah, we're just gonna
have a nice normal night.

353
00:13:42,553 --> 00:13:43,553
And who knows?

354
00:13:43,707 --> 00:13:46,210
Maybe you'll even have a
little fun along the way.

355
00:13:47,016 --> 00:13:48,416
- TODD: <i>Miley Cyrus!</i>
- MELISSA:<i> Yes!</i>

356
00:13:48,534 --> 00:13:51,195
Uh, okay, this guy's
dead from the virus.

357
00:13:51,220 --> 00:13:52,783
- Lead singer of U2.
- Uh, Bono!

358
00:13:52,808 --> 00:13:54,400
Yes! Okay.

359
00:13:54,425 --> 00:13:56,822
Um, this guy's regular dead.

360
00:13:56,847 --> 00:13:58,646
Uh, "Frankly, Scarlett,
I don't give a damn."

361
00:13:58,671 --> 00:13:59,708
- Uh, Clark Gable!
- Yes.

362
00:13:59,733 --> 00:14:01,706
- Five seconds!
- Okay, okay, okay, um...

363
00:14:01,731 --> 00:14:03,174
dead New England Patriots quarterback,

364
00:14:03,199 --> 00:14:05,167
married to the dead
model, both from the virus.

365
00:14:05,192 --> 00:14:06,176
- Tom Brady!
- Yes!

366
00:14:06,201 --> 00:14:07,970
- TANDY: Time!
- Yes!

367
00:14:08,205 --> 00:14:11,596
Guys, do we have to say that the
person's dead every single time?

368
00:14:11,909 --> 00:14:13,492
You know, it just seems
like a major buzz kill.

369
00:14:13,517 --> 00:14:14,877
Well, an even bigger buzz kill

370
00:14:14,902 --> 00:14:17,410
is losing the game
due to lack of clarity.

371
00:14:17,796 --> 00:14:18,636
You want to go, Mike?

372
00:14:18,670 --> 00:14:19,753
Uh, no, no, I'm fine.

373
00:14:19,778 --> 00:14:21,648
Aw, come on, Chef Miyagi.

374
00:14:21,673 --> 00:14:23,575
Come on, come on. Mike, Mike...

375
00:14:23,608 --> 00:14:25,083
ALL (chanting): Mike, Mike, Mike...

376
00:14:25,108 --> 00:14:27,111
O... Okay, okay, okay, all right.

377
00:14:27,136 --> 00:14:28,454
- Ready, go.
- Okay.

378
00:14:28,479 --> 00:14:30,081
Um, I'm not sure how
to describe this person.

379
00:14:30,114 --> 00:14:31,916
- Enrique Iglesias!
- No.

380
00:14:32,096 --> 00:14:32,854
Julio Iglesias!

381
00:14:32,879 --> 00:14:34,554
Let him get a clue out.

382
00:14:34,579 --> 00:14:35,854
I got a method!

383
00:14:35,886 --> 00:14:37,356
Uh, just pass. Pass on this one. Pass.

384
00:14:37,388 --> 00:14:38,757
(coughs)

385
00:14:38,790 --> 00:14:40,925
Okay, uh, this person
was on<i> Seinfeld.</i>

386
00:14:41,019 --> 00:14:41,949
Seinfeld!

387
00:14:41,974 --> 00:14:43,823
- No, close.
- Glenn Close!

388
00:14:43,916 --> 00:14:46,056
No, I-I don't know if
you're gonna get this one.

389
00:14:46,081 --> 00:14:47,207
It just says, um,

390
00:14:47,729 --> 00:14:49,442
"The dead guy who played Newman."

391
00:14:49,682 --> 00:14:50,947
I don't know, um...

392
00:14:50,972 --> 00:14:53,088
Hey, can we maybe do
something else, maybe? Just...

393
00:14:53,306 --> 00:14:54,673
What's wrong, Mike?

394
00:14:55,619 --> 00:14:57,674
It's just a little
weird, you know, just...

395
00:14:58,604 --> 00:14:59,604
The suits.

396
00:14:59,775 --> 00:15:00,900
It's the friggin' suits. I knew it.

397
00:15:01,054 --> 00:15:02,481
Well, you know what? This...

398
00:15:02,738 --> 00:15:03,755
Tandy, no!

399
00:15:03,780 --> 00:15:06,380
No. Look, it's fine, okay?

400
00:15:06,568 --> 00:15:07,568
He has a cold.

401
00:15:07,982 --> 00:15:09,186
That's it.

402
00:15:09,404 --> 00:15:10,404
Now, look at this.

403
00:15:10,636 --> 00:15:14,640
I'm taking my friggin' suit off,
and the world is still turning.

404
00:15:15,180 --> 00:15:16,328
All right?

405
00:15:16,837 --> 00:15:18,163
So what do you say?

406
00:15:18,967 --> 00:15:20,548
Does anybody else want to join me?

407
00:15:26,897 --> 00:15:29,024
So this is actually a pretty typical

408
00:15:29,049 --> 00:15:30,201
second date for me, you know.

409
00:15:30,226 --> 00:15:31,553
- (chuckles)
- Yeah.

410
00:15:31,578 --> 00:15:33,626
You know, first date, always hibachi,

411
00:15:33,651 --> 00:15:36,369
and then the second date,
I like to invite a lady

412
00:15:36,394 --> 00:15:38,752
back to my quarantine
bubble, and, you know...

413
00:15:38,823 --> 00:15:40,400
Mm. (chuckles)

414
00:15:40,666 --> 00:15:42,859
Yeah, you're a real
smooth operator, Mike.

415
00:15:42,884 --> 00:15:43,929
(both laugh)

416
00:15:43,954 --> 00:15:46,290
(coughs)

417
00:15:49,532 --> 00:15:50,595
(sniffles)

418
00:15:50,807 --> 00:15:51,862
(clears throat)

419
00:15:51,896 --> 00:15:55,817
Sorry my sickness is,
uh, you know, scaring you.

420
00:15:56,402 --> 00:15:57,669
I'm not worried.

421
00:15:58,004 --> 00:16:00,572
Yeah, I think I'm coming
around to Tandy's theory.

422
00:16:00,895 --> 00:16:02,120
It's just a cold.

423
00:16:06,993 --> 00:16:08,546
Would it be okay if I, uh...

424
00:16:09,904 --> 00:16:11,121
It would.

425
00:16:17,597 --> 00:16:20,592
Hang on, let's... let's do this right.

426
00:16:25,430 --> 00:16:26,899
(chuckles)

427
00:16:31,951 --> 00:16:33,038
TANDY: Ooh!

428
00:16:33,070 --> 00:16:34,605
(all oohing)

429
00:16:34,638 --> 00:16:36,523
(whooping)

430
00:16:36,548 --> 00:16:37,684
Oh, my God, you guys, hurry!

431
00:16:37,709 --> 00:16:38,833
Come quick!

432
00:16:49,554 --> 00:16:51,455
Oh, farts.

433
00:16:52,946 --> 00:16:54,987
Oh, you poor little thing.

434
00:16:55,708 --> 00:16:58,178
Your mama's gone, but you still got us.

435
00:16:58,823 --> 00:17:00,230
Has Mike been around this cow?

436
00:17:00,401 --> 00:17:01,748
He was milking it today.

437
00:17:01,917 --> 00:17:03,416
He has to go, now.

438
00:17:03,441 --> 00:17:06,126
There's no way the virus
could take down a cow

439
00:17:06,151 --> 00:17:07,392
in, like, six hours.

440
00:17:07,417 --> 00:17:09,012
Then how else do you explain it?

441
00:17:09,037 --> 00:17:10,200
Uh, you know, uh, uh...

442
00:17:10,225 --> 00:17:11,309
It could have been old age.

443
00:17:11,334 --> 00:17:12,735
Uh, the cow could have had, like,

444
00:17:12,760 --> 00:17:14,623
some congenital hoof disorder.

445
00:17:14,648 --> 00:17:16,630
Uh, hell, could have
been this little guy!

446
00:17:16,655 --> 00:17:19,367
I mean, look at him
and those demon eyes.

447
00:17:19,682 --> 00:17:21,034
No, Tandy.

448
00:17:21,541 --> 00:17:23,311
That cow survived the
virus just like us,

449
00:17:23,336 --> 00:17:24,538
and now look at her.

450
00:17:24,672 --> 00:17:25,933
I'm with Melissa.

451
00:17:25,958 --> 00:17:27,293
Three strikes, you're out.

452
00:17:27,318 --> 00:17:28,318
Let's do this.

453
00:17:28,474 --> 00:17:30,521
But you... oh, wait!

454
00:17:30,546 --> 00:17:32,302
You forgot your friggin' pitchforks

455
00:17:32,327 --> 00:17:34,007
and your torches!

456
00:17:34,032 --> 00:17:36,204
Hey, whoa! Whoa, whoa,
whoa, whoa! What's going on?!

457
00:17:36,229 --> 00:17:38,104
I'm sorry, bud. We're
moving you, all right?

458
00:17:38,129 --> 00:17:39,720
- What? What? Why? What happened?!
- We got him.

459
00:17:39,752 --> 00:17:41,313
- Get out of here!
- Come on, guys...

460
00:17:41,446 --> 00:17:43,590
Get away from him! What are you doing?!

461
00:17:43,985 --> 00:17:46,913
If you move him, you're
gonna have to move me, too.

462
00:17:46,938 --> 00:17:48,598
Great. Todd, move Tandy.

463
00:17:48,623 --> 00:17:49,498
Todd!

464
00:17:49,631 --> 00:17:51,264
I friggin' love you, man.

465
00:17:51,373 --> 00:17:55,403
But if you take one more step,
I will friggin' rip you apart.

466
00:17:56,373 --> 00:17:58,603
Phil, if they want me
to go, I'll just go.

467
00:17:58,628 --> 00:18:01,409
No! Mike, you're staying right here.

468
00:18:02,776 --> 00:18:04,385
This is my brother.

469
00:18:05,436 --> 00:18:07,922
Okay? My own flesh and blood.

470
00:18:08,655 --> 00:18:10,118
Until two weeks ago,

471
00:18:10,487 --> 00:18:12,786
I thought I had lost him forever.

472
00:18:13,901 --> 00:18:15,123
What are the chances?

473
00:18:15,156 --> 00:18:18,759
The entire world dies...
there's seven of us...

474
00:18:18,901 --> 00:18:20,721
and my brother shows up.

475
00:18:22,331 --> 00:18:24,131
That is not a coincidence.

476
00:18:26,703 --> 00:18:29,169
I said good-bye to him once.

477
00:18:29,437 --> 00:18:31,509
I am not gonna do it again.

478
00:18:33,235 --> 00:18:36,743
He has a friggin' cold.

479
00:18:36,777 --> 00:18:38,179
(coughing)

480
00:18:40,015 --> 00:18:42,495
Come on, we got to find
a house for the night.

481
00:18:54,096 --> 00:18:56,129
You mind if I stay with Mike tonight?

482
00:18:56,833 --> 00:18:58,744
- All right?
- Okay, it's fine.

483
00:18:59,019 --> 00:19:00,530
Just be careful.

484
00:19:01,106 --> 00:19:03,142
You're the best. You're the best.

485
00:19:06,821 --> 00:19:07,977
CAROL: Okay, Mike.

486
00:19:08,376 --> 00:19:09,108
Good night.

487
00:19:09,133 --> 00:19:10,207
Night.

488
00:19:13,903 --> 00:19:15,641
Phil, come on.

489
00:19:15,666 --> 00:19:17,050
Maybe I should just go
away for a while, all right?

490
00:19:17,083 --> 00:19:19,614
It's a cold, Mike.

491
00:19:21,114 --> 00:19:22,190
It's a cold.

492
00:19:23,083 --> 00:19:24,083
Yeah.

493
00:19:24,544 --> 00:19:26,201
All right, you want to get some sleep?

494
00:19:27,138 --> 00:19:28,138
Sure.

495
00:19:33,754 --> 00:19:35,570
What do you think, 69 or 11?

496
00:19:36,363 --> 00:19:38,705
Why don't we start with
11 and go from there.

497
00:19:39,441 --> 00:19:40,741
Sure.

498
00:19:41,103 --> 00:19:42,267
(sniffles)

499
00:19:43,619 --> 00:19:44,621
(sighs)

500
00:19:51,250 --> 00:19:53,555
You want to do one for old time's sake?

501
00:19:56,259 --> 00:19:57,259
(coughs)

502
00:19:59,189 --> 00:20:02,546
♪ I don't know you ♪

503
00:20:02,571 --> 00:20:05,843
BOTH: ♪ But I want you ♪

504
00:20:05,868 --> 00:20:09,502
♪ All the more for that ♪

505
00:20:09,535 --> 00:20:11,836
(coughs)

506
00:20:11,861 --> 00:20:15,365
♪ Words fall through me ♪

507
00:20:15,390 --> 00:20:18,749
♪ Always fool me ♪

508
00:20:18,774 --> 00:20:23,196
♪ And I can't react... ♪

509
00:20:24,156 --> 00:20:25,157
(coughs)

510
00:20:46,881 --> 00:20:48,516
"So long, Phil.

511
00:20:48,750 --> 00:20:50,980
Didn't want you to have
to say good-bye again."

512
00:20:55,049 --> 00:21:00,049
Synced and corrected by Octavia
- www.addic7ed.com -

