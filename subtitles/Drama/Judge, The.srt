1
00:00:06,757 --> 00:00:09,393
Those are the decisions you drafted,

2
00:00:09,393 --> 00:00:10,460
Chief,

3
00:00:10,460 --> 00:00:13,497
I wrote them according to what we agreed upon,

4
00:00:13,497 --> 00:00:15,999
Why is there a big question mark here?

5
00:00:15,999 --> 00:00:17,401
I'm slightly caught aback,

6
00:00:17,401 --> 00:00:18,869
Why do you think I put that there?

7
00:00:19,503 --> 00:00:21,405
I'm sure you had a reason, but,,,

8
00:00:21,405 --> 00:00:23,840
- Think hard about it, - Still, this is,,,

9
00:00:24,641 --> 00:00:26,610
- Chief, will you really,,, - Really,,,

10
00:00:27,911 --> 00:00:29,179
Really what?

11
00:00:29,513 --> 00:00:32,516
I'll think hard about it,

12
00:00:33,150 --> 00:00:34,518
I'll think hard about it,

13
00:00:34,684 --> 00:00:36,586
Think hard about it,

14
00:00:36,987 --> 00:00:39,790
Don't just bury yourself in Jang Soon Bok's retrial request,

15
00:01:01,678 --> 00:01:04,214
Oh, my goodness, What is that,,,

16
00:01:04,614 --> 00:01:06,583
A big red question mark?

17
00:01:06,883 --> 00:01:08,585
Who did this? How rude,

18
00:01:08,585 --> 00:01:10,220
Chief Oh reviewed this,

19
00:01:11,555 --> 00:01:13,023
The chief reviewed it,

20
00:01:13,356 --> 00:01:16,526
Does he usually put this huge question mark,,,

21
00:01:16,893 --> 00:01:18,595
What? Rude? He's the chief,

22
00:01:18,762 --> 00:01:20,831
I do not understand, Please explain to us,

23
00:01:22,265 --> 00:01:24,367
I'll let you know later,

24
00:01:24,634 --> 00:01:26,870
I, the main judge on the trial, drafted the decision,,,

25
00:01:26,870 --> 00:01:28,572
and submitted it to the chief, and,,,

26
00:01:29,339 --> 00:01:32,075
When we say, "submit," we refer to when an associate judge,,,

27
00:01:32,075 --> 00:01:33,677
drafts the decision of the court,,,

28
00:01:33,677 --> 00:01:37,013
and gives to the head judge of the panel for review,

29
00:01:37,547 --> 00:01:39,783
When the chief puts a question mark, it means,,,

30
00:01:39,916 --> 00:01:42,486
he can't understand or there is a problem,

31
00:01:43,053 --> 00:01:45,055
and that you should think it over again,

32
00:01:45,055 --> 00:01:46,656
There's so much meaning in it,

33
00:01:47,023 --> 00:01:48,458
There's no meaning,

34
00:01:48,725 --> 00:01:51,228
Isn't he saying scrap it and start over?

35
00:01:52,662 --> 00:01:55,332
Ms, Lee, You have a talent,

36
00:01:55,332 --> 00:01:58,168
How do you say something that would drive a person mad so casually?

37
00:01:59,369 --> 00:02:00,437
Chief,

38
00:02:07,277 --> 00:02:08,345
Chief!

39
00:02:09,479 --> 00:02:11,948
I have a million question marks about this question mark!

40
00:02:13,216 --> 00:02:15,852
If you're taking revenge for refusing to give up,,,

41
00:02:15,852 --> 00:02:17,621
Jang Soon Bok's retrial request,

42
00:02:18,388 --> 00:02:20,123
I'll give you a chance,

43
00:02:21,391 --> 00:02:24,461
Get rid of this huge question mark!

44
00:02:29,499 --> 00:02:31,268
Do you have something to say?

45
00:02:31,368 --> 00:02:32,369
Chief,

46
00:02:32,969 --> 00:02:35,338
I intended to think hard about it, but,,,

47
00:02:36,606 --> 00:02:38,742
Our team dinner is tomorrow, correct?

48
00:02:38,742 --> 00:02:40,610
If we didn't choose the menu item yet,

49
00:02:40,610 --> 00:02:41,878
I'm craving bossam,

50
00:02:41,878 --> 00:02:44,047
We'll all crave bossam then,

51
00:02:44,648 --> 00:02:45,649
Yes, bossam,

52
00:02:47,918 --> 00:02:50,554
Judge Lee, I thought you can't eat pork,

53
00:02:50,754 --> 00:02:51,855
You get hives,

54
00:02:51,855 --> 00:02:53,957
Hives won't kill me,

55
00:02:53,957 --> 00:02:57,394
If Chief Oh would tell me what this huge question mark means,,,

56
00:03:05,602 --> 00:03:09,439
I thought once you become a judge, decisions get written easily,

57
00:03:10,106 --> 00:03:11,975
How could they get written easily?

58
00:03:12,275 --> 00:03:15,378
A person's life hangs on every decision,

59
00:03:15,612 --> 00:03:18,782
Of course, I'm sure how we feel when we write decisions,,,

60
00:03:19,115 --> 00:03:21,851
is how doctors feel before surgeries,

61
00:03:22,152 --> 00:03:25,622
When a junior judge receives a chief judge's edits,

62
00:03:26,189 --> 00:03:27,891
you feel either joy or sorrow,

63
00:03:28,225 --> 00:03:31,361
Every associate judge's wish is edits for minor typos,

64
00:03:31,428 --> 00:03:32,796
A missing period,

65
00:03:32,796 --> 00:03:35,699
That's the best possible result you can get,

66
00:03:36,766 --> 00:03:38,535
Usually, they leave detailed comments,,,

67
00:03:38,635 --> 00:03:41,238
or a question mark at certain parts,

68
00:03:46,343 --> 00:03:47,577
(Refer to "Birth of Execution",)

69
00:03:47,577 --> 00:03:49,713
They often say to refer to a certain book,

70
00:03:49,713 --> 00:03:51,381
but if it's in red,,,

71
00:03:52,115 --> 00:03:53,216
Help me!

72
00:03:55,252 --> 00:03:57,621
Well, you shout, "Help me,"

73
00:03:58,822 --> 00:04:02,459
I think Judge Lee expected a single period too,

74
00:04:02,459 --> 00:04:04,728
Nothing but a large question mark on the first page,,,

75
00:04:05,328 --> 00:04:07,330
It means he had it in for you,,,

76
00:04:07,697 --> 00:04:09,432
because of Jang Soon Bok's retrial request,

77
00:04:11,268 --> 00:04:14,004
You think that too, right? He has it in for me,

78
00:04:15,071 --> 00:04:17,907
What should I do now? What would you do?

79
00:04:26,850 --> 00:04:28,785
I thought it over,

80
00:04:29,219 --> 00:04:32,122
and you know Jang Soon Bok's retrial request?

81
00:04:32,756 --> 00:04:35,058
- We should back off, - Back off?

82
00:04:35,425 --> 00:04:38,495
This isn't the time for us to work so hard to help Judge Lee,

83
00:04:38,695 --> 00:04:39,963
What if it gets dismissed?

84
00:04:39,963 --> 00:04:42,265
Chief Oh will come after us as well,

85
00:04:44,000 --> 00:04:46,336
I never thought we were helping Judge Lee,

86
00:04:46,936 --> 00:04:49,739
but setting right a wrong if the decision was incorrect,

87
00:04:50,106 --> 00:04:52,442
That's our job as the committee for reviewing incorrect verdicts,

88
00:04:52,442 --> 00:04:54,778
I thought it was our job as well,

89
00:04:54,778 --> 00:04:57,847
but who gives us our evaluations?

90
00:04:57,847 --> 00:04:59,049
Chief Oh,

91
00:04:59,049 --> 00:05:00,784
What if we're the only ones not to get As?

92
00:05:00,784 --> 00:05:04,254
Yes, but still, how could we be so low,,,

93
00:05:04,254 --> 00:05:06,089
You stay out of it,

94
00:05:07,057 --> 00:05:08,525
Sit straight,

95
00:05:09,759 --> 00:05:13,496
I wouldn't say this if I were as young as you,

96
00:05:13,663 --> 00:05:15,598
I left the Blue House at this age,,,

97
00:05:15,598 --> 00:05:17,167
with everyone saying I was crazy,

98
00:05:17,334 --> 00:05:18,535
If I want to become a part of this committee,

99
00:05:18,535 --> 00:05:20,370
I can't get on the chief's bad side,

100
00:05:21,538 --> 00:05:22,939
You too,

101
00:05:22,939 --> 00:05:26,176
If you're going to raise Ha Neul as a single mom,,,

102
00:05:26,176 --> 00:05:27,911
I received your submission, so please stop talking,

103
00:05:28,645 --> 00:05:31,047
I'm sure you don't feel good saying these things anyway,

104
00:05:31,314 --> 00:05:36,019
In exchange, let's dig deep into Choi Kyung Ho's case,

105
00:05:44,994 --> 00:05:47,364
How could you do this if you know that?

106
00:05:47,364 --> 00:05:49,232
- You're driving me mad! - Let go of me!

107
00:05:49,899 --> 00:05:52,535
- Judge Lee, stop it! - Let go of my arms!

108
00:05:52,535 --> 00:05:55,071
- Get back down here! Stop it! - Judge Lee!

109
00:05:56,306 --> 00:05:58,308
All right, Starting now,

110
00:05:58,675 --> 00:06:02,112
you'll be writing comments on the video you just watched,

111
00:06:02,779 --> 00:06:03,780
Yes?

112
00:06:06,816 --> 00:06:09,419
Anyone? Then I'll go first,

113
00:06:10,920 --> 00:06:14,591
"Don't you ever tell anyone,,,"

114
00:06:14,791 --> 00:06:17,060
"that I'm your role model,"

115
00:06:26,002 --> 00:06:28,538
You, Trying to exit through the back door,

116
00:06:32,542 --> 00:06:35,412
Actually, I'm not in this class,

117
00:06:36,179 --> 00:06:38,481
If you're auditing my class, you can at least comment on the video,

118
00:06:40,483 --> 00:06:41,518
Well,,,

119
00:06:42,352 --> 00:06:44,687
I'm not in any position to leave any comments,

120
00:06:44,687 --> 00:06:46,856
Why don't you just tell me? I'll type it for you,

121
00:06:48,892 --> 00:06:49,893
If you insist,,,

122
00:06:52,562 --> 00:06:54,063
"Why did you do that?"

123
00:06:54,531 --> 00:06:59,269
"You should hate the sin, not the sinner," Put a crying emoji,

124
00:07:00,703 --> 00:07:03,406
Is it possible to hate the sin, and not the sinner?

125
00:07:03,907 --> 00:07:06,176
Who's responsible for the sin? It's the sinner,

126
00:07:06,176 --> 00:07:08,878
Isn't it only natural to hate the sinner?

127
00:07:13,383 --> 00:07:15,151
What I mean is that we must give,,,

128
00:07:15,185 --> 00:07:18,254
sinners a second chance to reform their lives despite our hatred,

129
00:07:18,254 --> 00:07:19,722
I'm sure this judge in the video,,,

130
00:07:19,722 --> 00:07:23,259
must have despised this heinous defendant,,,

131
00:07:23,293 --> 00:07:26,129
who didn't even repent his unpardonable act of crime,

132
00:07:27,564 --> 00:07:28,731
I can understand her,

133
00:07:28,731 --> 00:07:30,400
Judges are humans too, I can understand her rampage,

134
00:07:30,400 --> 00:07:32,702
That's what I think too, Judges are humans too,

135
00:07:33,670 --> 00:07:35,839
However, the moment you become a judge,

136
00:07:36,506 --> 00:07:38,208
you must get rid of that thought,

137
00:07:39,108 --> 00:07:40,577
When you're on the bench,

138
00:07:40,810 --> 00:07:44,547
you mustn't give yourself the excuse that you're a human too,

139
00:07:44,781 --> 00:07:48,518
Judges who are generous on their own mistakes are the most dangerous,

140
00:07:50,086 --> 00:07:53,256
Don't you agree, Judge Lee Jung Joo?

141
00:08:04,400 --> 00:08:05,869
That couldn't have been more embarrassing,

142
00:08:05,902 --> 00:08:08,037
Professor Yoo, I know that you're trying to educate your students,

143
00:08:08,037 --> 00:08:10,807
but is it fair for you to put me on the chopping block and condemn me?

144
00:08:11,007 --> 00:08:12,809
It's funny how you came at that very moment,

145
00:08:15,111 --> 00:08:16,446
Is it because of Choi Kyung Ho?

146
00:08:20,383 --> 00:08:21,751
Han Joon told me,

147
00:08:21,985 --> 00:08:24,053
Choi Kyung Ho had a trial,

148
00:08:24,621 --> 00:08:26,189
and you were on the panel,

149
00:08:28,191 --> 00:08:29,692
How much did you hear?

150
00:08:29,692 --> 00:08:31,160
How much should I know?

151
00:08:32,061 --> 00:08:33,296
I want you to tell me,

152
00:08:36,466 --> 00:08:39,702
Someone else murdered the girl, and he had a deal,

153
00:08:40,003 --> 00:08:41,004
And a warning,

154
00:08:41,004 --> 00:08:42,372
Don't lose sleep over what he said,

155
00:08:43,172 --> 00:08:44,807
I only came to talk about,,,

156
00:08:44,807 --> 00:08:46,576
Jang Soon Bok's petition for a retrial,

157
00:08:46,910 --> 00:08:48,244
I just had a few questions,

158
00:08:48,978 --> 00:08:50,079
Ask away,

159
00:08:53,683 --> 00:08:56,753
Jang Soon Bok murdered her husband, Seo Gi Ho,

160
00:08:58,154 --> 00:09:00,623
I heard he managed your cottage,

161
00:09:01,090 --> 00:09:02,091
That's true,

162
00:09:02,292 --> 00:09:05,995
Kim Ga Young's dad, Kim Hee Chul, used to manage it for us,

163
00:09:06,496 --> 00:09:09,132
but his daughter's death devastated him, and he left Korea,

164
00:09:09,632 --> 00:09:12,702
Mr, Seo Gi Ho was killed as soon as we hired him,

165
00:09:13,269 --> 00:09:16,306
Kim Ga Young's dad managed your cottage?

166
00:09:16,306 --> 00:09:17,307
Yes,

167
00:09:17,707 --> 00:09:20,877
I've never seen him since I've never been to the cottage,

168
00:09:21,110 --> 00:09:23,379
I saw him for the first time at the trial,

169
00:09:23,880 --> 00:09:25,982
- I'll do it, - No, let me,

170
00:09:29,152 --> 00:09:30,153
Are you all right?

171
00:09:30,787 --> 00:09:33,823
Yes, I'm fine, This knife is sharp,

172
00:09:35,925 --> 00:09:40,363
Hold on, It's from the same brand of Jang Soon Bok's murder weapon,

173
00:09:45,001 --> 00:09:46,002
Look at this,

174
00:09:46,102 --> 00:09:48,438
I think we need to inform Judge Lee,,,

175
00:09:48,438 --> 00:09:51,207
as this is crucial information regarding the murder weapon,

176
00:09:51,708 --> 00:09:53,476
I told you to lay off,

177
00:09:54,444 --> 00:09:57,347
This information is going to stay between the two of us for now,

178
00:09:59,349 --> 00:10:00,416
This knife is the murder weapon,

179
00:10:01,284 --> 00:10:04,687
This isn't a knife you can get your hands on easily in Korea,

180
00:10:04,787 --> 00:10:05,788
Do you know the brand?

181
00:10:05,888 --> 00:10:08,424
The Swedish royal family used this particular brand,

182
00:10:08,524 --> 00:10:10,426
They manufacture the knives in limited quantities,

183
00:10:10,426 --> 00:10:12,662
Right, When I worked at the Blue House,

184
00:10:12,662 --> 00:10:13,896
I saw the First Lady use this brand,

185
00:10:15,264 --> 00:10:16,466
Jang Soon Bok,,,

186
00:10:17,100 --> 00:10:18,768
killed her husband with this?

187
00:10:21,104 --> 00:10:22,138
That's not possible,

188
00:10:23,406 --> 00:10:25,508
When my husband was the chief of staff at the Blue House,

189
00:10:25,908 --> 00:10:27,877
it was a gift from the First Lady,

190
00:10:28,578 --> 00:10:31,614
I heard that there's a limited number of them even in Sweden,

191
00:10:31,614 --> 00:10:32,649
Are you sure?

192
00:10:32,649 --> 00:10:35,151
It'd be hard to buy this knife in Korea,

193
00:10:35,885 --> 00:10:39,689
How did she murder her husband with this expensive knife?

194
00:10:40,923 --> 00:10:42,859
That would be your reasonable doubt,

195
00:10:43,626 --> 00:10:46,229
You're right, I can't help myself, but doubt,

196
00:10:46,362 --> 00:10:48,164
If there's still room for reasonable doubts,

197
00:10:50,099 --> 00:10:51,701
shouldn't you start over?

198
00:10:53,569 --> 00:10:54,604
You should,,,

199
00:10:55,171 --> 00:10:57,273
listen to the defendant's claim even if it's late,

200
00:10:57,707 --> 00:10:58,775
With that in mind,

201
00:10:59,375 --> 00:11:01,644
you shouldn't dismiss what Choi Kyung Ho said,

202
00:11:03,813 --> 00:11:05,181
If what he said is true,

203
00:11:05,181 --> 00:11:07,250
it implies your ruling was wrong,

204
00:11:07,617 --> 00:11:08,851
Would you be okay with that?

205
00:11:12,155 --> 00:11:13,856
Would you be okay if you were in my shoes?

206
00:11:14,557 --> 00:11:18,061
Given my disposition, I might bite my tongue and vomit blood,

207
00:11:18,661 --> 00:11:20,063
I knew it,

208
00:11:24,634 --> 00:11:26,903
Even if I do such things,

209
00:11:26,903 --> 00:11:28,004
my guilt wouldn't be,,,

210
00:11:29,005 --> 00:11:31,274
more important than the pain of the wrongfully accused,

211
00:11:33,309 --> 00:11:37,413
What I feel isn't even close to the suffering the accused went through,

212
00:11:37,880 --> 00:11:39,182
The possibility of misjudgment,,,

213
00:11:40,083 --> 00:11:41,517
comes with the job,

214
00:11:43,986 --> 00:11:47,256
If misjudgment scares you enough to avoid the substantive truth,

215
00:11:48,558 --> 00:11:49,926
you must take off your robe,

216
00:12:08,411 --> 00:12:11,380
For unavoidable reasons, today was the earliest she could join us,

217
00:12:12,081 --> 00:12:13,616
This is Ms, Jin Se Ra,

218
00:12:14,684 --> 00:12:15,852
I look forward to working for you,

219
00:12:16,652 --> 00:12:19,088
All right, For your information,

220
00:12:19,422 --> 00:12:22,492
I like people who do a good job rather than just look forward,

221
00:12:22,492 --> 00:12:23,693
I was just being polite,

222
00:12:24,594 --> 00:12:26,496
In that case, I'll do my job well,

223
00:12:27,463 --> 00:12:29,799
You were a famous idol singer after all,

224
00:12:29,799 --> 00:12:31,000
You should give us autographs,,,

225
00:12:31,000 --> 00:12:32,735
My gosh, of course, she should,

226
00:12:33,503 --> 00:12:35,371
- I'd like to get mine first, - Hold on!

227
00:12:37,673 --> 00:12:39,575
She doesn't give out autographs,

228
00:12:40,009 --> 00:12:42,411
She didn't even take a photo or give out autographs,,,

229
00:12:42,678 --> 00:12:44,447
even to her fans,

230
00:12:44,447 --> 00:12:47,984
Please try to understand, As one of the oldest members in Que Sera Sera,

231
00:12:48,384 --> 00:12:49,685
I'd like to ask for your understanding,

232
00:12:56,292 --> 00:12:58,561
That's a bit disappointing,

233
00:13:04,300 --> 00:13:06,335
The ratio of the meat and the fat should be 8 to 2,

234
00:13:06,536 --> 00:13:09,205
No, no, Without the fat, it will taste dry, It won't be delicious,

235
00:13:09,205 --> 00:13:11,908
Our chief has high blood pressure, It must be moderately seasoned,

236
00:13:11,908 --> 00:13:14,243
It should be on the borderline of being salty and bland,

237
00:13:16,078 --> 00:13:18,781
Oh, right! You need to serve us either stir-fried anchovies,,,

238
00:13:18,781 --> 00:13:20,416
or stir-fried fishcake as one of the side dishes,

239
00:13:21,517 --> 00:13:23,085
Stir-fried anchovies with shishito peppers?

240
00:13:23,519 --> 00:13:25,388
He's a fan of that dish,

241
00:13:28,991 --> 00:13:30,092
I'll call you back later,

242
00:13:30,092 --> 00:13:32,328
What on earth is wrong with this ruling? What is it?

243
00:13:32,795 --> 00:13:34,797
I don't get it no matter how many times I read it,

244
00:13:35,565 --> 00:13:36,899
Am I the problem?

245
00:13:36,899 --> 00:13:38,701
That question mark is better than,,,

246
00:13:38,701 --> 00:13:41,370
Chief Oh asking for the file so he could write the ruling himself,

247
00:13:41,904 --> 00:13:43,105
When I was the associate judge on his left,

248
00:13:43,105 --> 00:13:45,541
Chief Oh didn't even give me a chance to fix,

249
00:13:45,541 --> 00:13:47,143
He corrected the ruling himself,

250
00:13:47,310 --> 00:13:48,511
Whenever I think back,

251
00:13:48,678 --> 00:13:51,380
I'm so humiliated that I want to go into hiding,

252
00:13:53,983 --> 00:13:57,720
I won't ever write a question mark when I become the chief judge,

253
00:13:57,887 --> 00:13:59,422
Never,

254
00:14:02,058 --> 00:14:04,560
I can't figure it out, Please look over it,

255
00:14:04,560 --> 00:14:05,795
and tell me what I'm missing,

256
00:14:05,995 --> 00:14:08,431
I'll try, I don't know if I'd be of any help,

257
00:14:23,613 --> 00:14:24,680
What is it?

258
00:14:25,481 --> 00:14:27,316
What is it? What's going on?

259
00:14:28,351 --> 00:14:29,352
What is it?

260
00:14:29,719 --> 00:14:31,354
Oh, no, This is not happening,

261
00:14:31,420 --> 00:14:32,455
What?

262
00:14:34,590 --> 00:14:35,625
"Ral",,,

263
00:14:39,829 --> 00:14:42,265
- "Oh Ji Ral"? - "Oh Ji Ral"?

264
00:14:44,533 --> 00:14:45,534
(Judge Oh Ji Ral)

265
00:14:45,534 --> 00:14:47,570
"Oh Ji Ral"?

266
00:14:49,705 --> 00:14:50,706
As in "a nut job"?

267
00:14:53,209 --> 00:14:54,577
- Oh Ji,,, - Oh Ji Ral?

268
00:14:54,577 --> 00:14:55,611
A nut job?

269
00:14:57,346 --> 00:14:59,982
- I shouldn't be laughing, - Right,

270
00:14:59,982 --> 00:15:02,084
No, don't, Don't laugh,

271
00:15:23,372 --> 00:15:24,440
What?

272
00:15:24,707 --> 00:15:27,276
Chief Oh, you are truly an oasis,,,

273
00:15:27,276 --> 00:15:29,612
to the 73rd Criminal Judicial Panel,

274
00:15:29,612 --> 00:15:30,680
(Oasis: the most-preferred chief judge)

275
00:15:30,680 --> 00:15:33,749
I respect you immensely for taking someone like me who gets easily,,,

276
00:15:34,250 --> 00:15:35,451
excited and chatty,

277
00:15:36,919 --> 00:15:39,522
From now on, I won't frown on your suggestions to correct my rulings,

278
00:15:39,789 --> 00:15:41,791
and I will do my best to meet the deadlines of my rulings,

279
00:15:46,696 --> 00:15:48,030
I'm terribly sorry,

280
00:15:53,936 --> 00:15:55,004
(Judge Oh Ji Rak)

281
00:15:57,740 --> 00:15:58,774
(Judge Oh Ji Rak)

282
00:16:00,910 --> 00:16:01,944
Okay,

283
00:16:06,482 --> 00:16:08,217
A true question mark,

284
00:16:08,985 --> 00:16:11,153
This question mark will be my lamp,

285
00:16:11,153 --> 00:16:12,588
I set it,,,

286
00:16:12,588 --> 00:16:15,791
as my wallpaper so that I'll never forget it,

287
00:16:17,093 --> 00:16:18,627
Every time I see this question mark,

288
00:16:18,627 --> 00:16:20,463
I will remember to think at least one more time,,,

289
00:16:20,663 --> 00:16:22,932
and become your left hand,

290
00:16:23,366 --> 00:16:25,801
I will try again and again,

291
00:16:30,139 --> 00:16:31,140
Sure,

292
00:16:44,487 --> 00:16:46,322
How long do you plan to keep your mouth shut?

293
00:16:47,156 --> 00:16:49,525
Where did you get that knife?

294
00:16:54,497 --> 00:16:55,564
You won't talk?

295
00:16:57,466 --> 00:16:58,868
I don't remember,

296
00:16:59,935 --> 00:17:01,737
That's such an obvious response,

297
00:17:02,571 --> 00:17:05,307
That's gibberish you give at a government hearing,

298
00:17:05,541 --> 00:17:08,244
Forgetting is the greatest blessing from above,

299
00:17:09,145 --> 00:17:10,946
I am enjoying that blessing,

300
00:17:11,580 --> 00:17:14,183
That's right, I have something to tell you before I forget,

301
00:17:14,383 --> 00:17:15,384
What?

302
00:17:15,384 --> 00:17:17,186
That moron, Choi Kyung Ho,

303
00:17:17,553 --> 00:17:20,623
said to relay a message to our Knucklehead Prosecutor,

304
00:17:24,160 --> 00:17:26,228
This borders on being a warning,

305
00:17:26,662 --> 00:17:28,998
He said not to touch Judge Lee Jung Joo,

306
00:17:30,299 --> 00:17:33,869
He said you'll regret it if you don't leave her alone,

307
00:17:37,106 --> 00:17:38,307
I relayed the message,

308
00:17:40,109 --> 00:17:42,011
That warning sounds familiar,

309
00:17:43,712 --> 00:17:46,449
You must be close to Choi Kyung Ho, so relay my message too,

310
00:17:46,782 --> 00:17:48,784
You'll play your hand after receiving a warning,

311
00:17:49,685 --> 00:17:52,421
That's interesting, Go ahead, Play your hand,

312
00:18:18,013 --> 00:18:20,382
- She hasn't made any progress? - No,

313
00:18:21,383 --> 00:18:23,819
You need to wake up and stand trial,

314
00:18:25,087 --> 00:18:27,556
Please let me in,

315
00:18:27,857 --> 00:18:30,526
I want to stay with my mom,

316
00:18:30,526 --> 00:18:34,196
You can't go inside, Visiting hours are over,

317
00:18:34,196 --> 00:18:37,166
My mom hasn't been able to eat,

318
00:18:37,166 --> 00:18:38,434
She's hungry,

319
00:18:39,768 --> 00:18:41,737
I need to bring her this,

320
00:18:41,737 --> 00:18:43,973
She can't eat stuff like that!

321
00:18:45,241 --> 00:18:46,809
Yong Soo,,,

322
00:18:48,611 --> 00:18:49,912
brought this,

323
00:18:51,914 --> 00:18:53,115
and this,

324
00:18:54,083 --> 00:18:55,284
and this,

325
00:18:56,051 --> 00:18:58,988
and this for my mom,

326
00:18:58,988 --> 00:19:03,125
If you keep doing this, you can't stay outside her room either,

327
00:19:04,059 --> 00:19:07,496
I have to give this to my mom,

328
00:19:13,302 --> 00:19:14,436
What is this?

329
00:19:19,942 --> 00:19:21,110
Why do you have this?

330
00:19:22,912 --> 00:19:23,913
Tell me,

331
00:19:26,048 --> 00:19:27,116
Why,,,

332
00:19:28,517 --> 00:19:30,085
Why do you have this?

333
00:19:49,872 --> 00:19:51,307
Please,

334
00:20:11,560 --> 00:20:12,895
Where's the sneaker?

335
00:20:13,796 --> 00:20:14,797
Tell me,

336
00:20:14,897 --> 00:20:17,032
I,,, I don't know,

337
00:20:17,533 --> 00:20:19,668
I,,, I don't have it,

338
00:20:41,590 --> 00:20:42,758
Gosh,

339
00:20:43,392 --> 00:20:45,261
The path to the team dinner,,,

340
00:20:46,028 --> 00:20:48,097
was never so frightening before,

341
00:20:48,831 --> 00:20:52,434
Chief Oh, I'm sorry, I made a huge mistake,

342
00:20:56,338 --> 00:20:57,339
A mistake?

343
00:20:57,773 --> 00:21:00,909
The names are automatically inserted these days,,,

344
00:21:00,909 --> 00:21:02,278
in the template,

345
00:21:02,278 --> 00:21:04,346
That's not what you call a mistake,

346
00:21:04,713 --> 00:21:07,116
That was intentional, Intentional!

347
00:21:07,516 --> 00:21:08,817
The thing is,,,

348
00:21:10,419 --> 00:21:13,255
I deleted your name by mistake while I was editing the document,

349
00:21:13,389 --> 00:21:15,924
so I typed it in myself and made a typo,

350
00:21:17,426 --> 00:21:20,129
That's right, I did that before too,

351
00:21:20,129 --> 00:21:22,998
I realized it was possible to make a mistake like that,

352
00:21:24,867 --> 00:21:26,101
Say it was a mistake,

353
00:21:26,869 --> 00:21:29,438
Why did it have to be "Oh Ji Ral" of all things?

354
00:21:29,438 --> 00:21:31,307
How much resentment were you harboring,,,

355
00:21:31,307 --> 00:21:34,576
that it was "Oh Ji Ral"? That's not even a name!

356
00:21:34,610 --> 00:21:37,513
It's because you think I'm full of gibberish!

357
00:21:37,513 --> 00:21:39,982
That's how you ended up typing "Oh Ji Ral"!

358
00:21:39,982 --> 00:21:41,550
That's exactly what you were thinking,

359
00:21:41,550 --> 00:21:44,386
I'm convinced you were thinking like so! You were!

360
00:21:44,386 --> 00:21:47,189
You thought I was a nut job, so that's why you wrote that,

361
00:21:47,189 --> 00:21:48,590
How else would you have typed that in?

362
00:21:49,224 --> 00:21:51,727
Oh Ji Ral, Not even "Original" or something,

363
00:21:53,262 --> 00:21:54,997
We should give you a big welcoming party, but,,,

364
00:21:55,431 --> 00:21:59,735
No, it's fine, I prefer a cozy welcoming party with you,

365
00:22:02,805 --> 00:22:04,006
- Here, - Thank you,

366
00:22:08,610 --> 00:22:10,579
Do you not drink at all?

367
00:22:11,013 --> 00:22:12,548
This is alcohol in the courtroom,

368
00:22:13,816 --> 00:22:17,986
To render decisions that are as refreshing as soda?

369
00:22:22,524 --> 00:22:23,759
That's beyond my abilities,

370
00:22:24,793 --> 00:22:26,862
Even if it's a frustrating decision, I still do my best,

371
00:22:26,862 --> 00:22:29,365
Even if it's a decision that is frustrating for the parties,

372
00:22:29,498 --> 00:22:31,300
as long as it's one they can accept,

373
00:22:32,935 --> 00:22:35,604
That's not bad, At least it wouldn't be a wrong decision,,,

374
00:22:36,872 --> 00:22:38,140
like in Kyung Ho's trial,

375
00:22:39,575 --> 00:22:43,011
I didn't know he'd say that in the courtroom,

376
00:22:45,147 --> 00:22:47,249
I should've told you sooner,

377
00:22:47,750 --> 00:22:50,819
When I asked Kyung Ho again later on,

378
00:22:51,253 --> 00:22:53,422
he took it back and said there was no "real" murderer,

379
00:22:54,757 --> 00:22:56,592
I don't know what kind of a deal he made,

380
00:22:59,862 --> 00:23:00,996
- Five, please, - Hello,

381
00:23:00,996 --> 00:23:02,264
- Where? - Hello,

382
00:23:02,264 --> 00:23:03,332
Judge Sah!

383
00:23:04,199 --> 00:23:05,601
Sit, sit,

384
00:23:09,705 --> 00:23:11,507
(My mom, Jang Soon Bok, is innocent, She is not a murderer,)

385
00:23:31,693 --> 00:23:34,096
My mom, Jang Soon Bok, is not a murderer,

386
00:23:35,731 --> 00:23:37,866
My mom is innocent,

387
00:23:46,108 --> 00:23:48,277
Here's to the Pyeongchang Winter Olympics!

388
00:23:48,277 --> 00:23:50,746
- Cheers! - Cheers!

389
00:23:50,746 --> 00:23:52,047
Bottoms up,

390
00:23:59,755 --> 00:24:01,690
Why are you so sloppy?

391
00:24:01,857 --> 00:24:04,059
You are so bad at anything you do,

392
00:24:04,059 --> 00:24:05,627
When you first start,,,

393
00:24:05,627 --> 00:24:08,497
working at the court, it isn't easy,

394
00:24:08,697 --> 00:24:09,865
Jang Soon Bok,,,

395
00:24:10,299 --> 00:24:13,001
They called to say they don't think she'll wake up,

396
00:24:13,001 --> 00:24:14,369
Do you want to go to the hospital?

397
00:24:17,439 --> 00:24:18,740
I'm going to go first,

398
00:24:19,208 --> 00:24:22,044
Hey, Don't you know that a judge,,,

399
00:24:22,044 --> 00:24:25,481
can't meet a defendant personally outside of court?

400
00:24:25,481 --> 00:24:26,849
I'll follow protocol to go to the hospital,

401
00:24:26,849 --> 00:24:28,383
I need to go finish writing a decision,

402
00:24:29,151 --> 00:24:31,787
I see, A decision,

403
00:24:32,221 --> 00:24:35,657
You need to write that well, You have to write it well,

404
00:24:36,024 --> 00:24:38,494
Typographical errors that even clerks don't make,,,

405
00:24:38,494 --> 00:24:41,296
- are mistakes that only,,, - Me too,

406
00:24:42,130 --> 00:24:45,234
I'll go write my judgments well, Judgment,

407
00:25:12,861 --> 00:25:15,030
Come here, Walk next to me,

408
00:25:18,066 --> 00:25:19,968
I'm used to walking this way with my panel,

409
00:25:19,968 --> 00:25:21,436
so I prefer it this way,

410
00:25:31,013 --> 00:25:34,149
I'm an individual judge, so I prefer this,

411
00:25:36,385 --> 00:25:39,154
They say it's light and day being an individual judge,

412
00:25:39,154 --> 00:25:41,156
When will I ever become one?

413
00:25:42,291 --> 00:25:46,328
Even the air in your office is better than in ours,

414
00:25:50,365 --> 00:25:52,067
Are you on bad terms with Chief Oh,,,

415
00:25:53,468 --> 00:25:55,070
because of Jang Soon Bok's petition for retrial?

416
00:25:55,571 --> 00:25:56,572
It seems that way,

417
00:25:56,572 --> 00:25:59,541
She's certainly getting in my way of success,

418
00:26:00,809 --> 00:26:02,244
Gosh, it's cold,

419
00:26:06,715 --> 00:26:07,749
Who is that?

420
00:26:17,993 --> 00:26:20,128
Excuse me, Hey,

421
00:26:20,762 --> 00:26:21,763
Is he okay?

422
00:26:22,564 --> 00:26:23,632
He's sleeping,

423
00:26:23,665 --> 00:26:24,933
It's too cold to sleep here,

424
00:26:36,511 --> 00:26:37,546
Can you help him up?

425
00:26:45,153 --> 00:26:46,154
Gosh,

426
00:26:50,726 --> 00:26:52,427
Don't do anything others might get the wrong idea about,

427
00:26:53,895 --> 00:26:55,530
Let's ask the security guards to look after him and leave,

428
00:27:02,304 --> 00:27:03,305
Hold on,

429
00:27:10,178 --> 00:27:11,913
Excuse me, Hey,

430
00:27:12,080 --> 00:27:13,115
What's going on?

431
00:27:13,115 --> 00:27:15,550
Hey, listen, Wake up for a second,

432
00:27:15,951 --> 00:27:17,252
Hey, hey,

433
00:27:20,022 --> 00:27:21,456
By any chance,,,

434
00:27:26,361 --> 00:27:27,696
Where did you get that?

435
00:27:29,264 --> 00:27:30,499
What is it about?

436
00:27:32,968 --> 00:27:34,970
You're not in trouble, Just answer my question, please,

437
00:27:35,837 --> 00:27:38,373
Is the owner of that sneaker,,,

438
00:27:38,674 --> 00:27:40,876
Kim Ga Young?

439
00:27:42,377 --> 00:27:46,448
Do you know Ga Young?

440
00:27:46,815 --> 00:27:48,884
Yes, Do you know her?

441
00:27:50,952 --> 00:27:54,589
Ga Young and I are close,

442
00:27:56,458 --> 00:27:57,459
Really?

443
00:27:58,360 --> 00:27:59,761
How did you get that?

444
00:28:00,662 --> 00:28:03,398
It's all right, Just take your time, and answer the question,

445
00:28:03,465 --> 00:28:04,833
He's scared,

446
00:28:05,367 --> 00:28:08,437
It's all right, You don't have to tell us right now,

447
00:28:08,670 --> 00:28:13,442
Kyung Ho told me that I could keep it,

448
00:28:13,642 --> 00:28:16,511
Kyung Ho? Are you referring to Mr, Choi Kyung Ho?

449
00:28:22,050 --> 00:28:23,585
Do you know Mr, Choi Kyung Ho?

450
00:28:24,052 --> 00:28:27,489
Kyung Ho and I are close,

451
00:28:29,458 --> 00:28:31,259
Kyung Ho,,,

452
00:28:32,060 --> 00:28:35,363
went to save Ga Young,

453
00:28:36,665 --> 00:28:38,033
He went to save her?

454
00:28:40,736 --> 00:28:42,270
He went,

455
00:28:42,537 --> 00:28:47,342
but Ga Young wasn't there, I only found her sneaker,

456
00:28:47,476 --> 00:28:49,644
Then Choi Kyung Ho didn't,,,

457
00:28:49,644 --> 00:28:50,979
Kill her?

458
00:28:51,546 --> 00:28:56,084
Kyung Ho isn't a bad guy,

459
00:28:57,452 --> 00:28:58,620
Kyung Ho,,,

460
00:28:58,920 --> 00:29:04,025
told me that he had to rescue Ga Young,

461
00:29:04,426 --> 00:29:07,362
Kyung Ho said,,,

462
00:29:07,729 --> 00:29:10,432
that he was going to,,,

463
00:29:10,432 --> 00:29:13,368
catch the bad guy,

464
00:29:25,781 --> 00:29:30,018
1, 3, 7, 1,

465
00:29:39,261 --> 00:29:40,929
(Han Joon)

466
00:29:43,465 --> 00:29:45,534
(Han Joon)

467
00:29:45,534 --> 00:29:50,272
1, 3, 7, 1,

468
00:29:52,674 --> 00:29:57,412
1, 3, 7, 1,

469
00:30:03,051 --> 00:30:08,123
It's the license plate number of the guy who killed Ga Young,

470
00:30:12,894 --> 00:30:14,062
I don't recognize your warning,

471
00:30:16,765 --> 00:30:17,833
Let's break our deal,

472
00:30:24,639 --> 00:30:26,274
(Han Joon)

473
00:30:37,819 --> 00:30:39,154
(Nothing to Lose)

474
00:30:39,154 --> 00:30:42,490
If Kyung Ho is innocent, what should I do?

475
00:30:42,557 --> 00:30:45,560
Be careful, If you keep this up, you might lose your life,

476
00:30:45,560 --> 00:30:47,462
- Kill him, - Kill him? The crazy prosecutor?

477
00:30:47,462 --> 00:30:49,264
You see, crazy people don't die that easily,

478
00:30:49,264 --> 00:30:50,365
Do Han Joon,

479
00:30:50,465 --> 00:30:51,499
There's no way,

480
00:30:51,566 --> 00:30:52,901
Why do you,,,

