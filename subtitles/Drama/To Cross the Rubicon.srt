1
00:00:04,007 --> 00:00:07,176
Thirteen... fourteen...

2
00:00:08,678 --> 00:00:09,912
Comin' to getcha!

3
00:00:09,946 --> 00:00:10,913
Fifteen...

4
00:00:12,549 --> 00:00:14,783
sixteen...

5
00:00:14,818 --> 00:00:18,787
seventeen, eighteen,

6
00:00:18,822 --> 00:00:21,790
nineteen, twenty.

7
00:00:21,825 --> 00:00:24,927
Ready or not, here I come!

8
00:00:55,058 --> 00:00:57,026
What do you
make of the news, uh,

9
00:00:57,060 --> 00:00:59,628
55 cents a share they're gonna
report for the first quarter.

10
00:02:41,000 --> 00:02:46,000
Sync by fant0m
www.addic7ed.com

11
00:03:00,683 --> 00:03:02,084
American Policy Institute.

12
00:03:02,118 --> 00:03:03,585
How may I help you?

13
00:03:03,620 --> 00:03:05,687
One moment.

14
00:03:05,722 --> 00:03:07,789
Well, five down.

15
00:03:07,824 --> 00:03:10,092
<i>What do lucky lepidoptera</i>

16
00:03:10,126 --> 00:03:11,793
<i>larvae eat?</i>

17
00:03:11,828 --> 00:03:15,164
<i>Oh... Lepidoptera larvae eat
clover, among other plants.</i>

18
00:03:15,198 --> 00:03:16,431
If they're lucky...

19
00:03:16,466 --> 00:03:17,733
Four-leaf clover.

20
00:03:17,767 --> 00:03:19,501
Four-leaf clover.
Uh-huh. Well...

21
00:03:19,536 --> 00:03:20,769
No, uh-uh.

22
00:03:20,803 --> 00:03:21,770
I need 19 letters.

23
00:03:21,804 --> 00:03:22,771
Try the Gaelic.

24
00:03:22,805 --> 00:03:24,573
Yeah. Which is?

25
00:03:24,607 --> 00:03:26,975
<i>I don't know. The only Gaelic
I know is Pog mo thoin.</i>

26
00:03:27,010 --> 00:03:27,976
What does that mean?

27
00:03:28,011 --> 00:03:29,011
Uh, try Latin.

28
00:03:29,045 --> 00:03:31,246
<i>Uh, marsilea
quadrifolia.</i>

29
00:03:31,281 --> 00:03:33,248
<i>Marsi... yes.</i>

30
00:03:33,283 --> 00:03:34,950
Mm-hmm. That'll work. There you go.

31
00:03:36,219 --> 00:03:37,486
Why do I drive myself crazy?

32
00:03:37,520 --> 00:03:39,621
Keep it.

33
00:04:00,843 --> 00:04:02,578
Hi, David.

34
00:04:04,480 --> 00:04:06,949
Uh, watch that number.

35
00:04:15,825 --> 00:04:18,160
So, how are we feeling?

36
00:04:18,194 --> 00:04:20,562
I'm-I'm fine.
Fine. You?

37
00:04:20,997 --> 00:04:23,798
What day is today, Will?

38
00:04:23,833 --> 00:04:25,533
Wednesday?

39
00:04:25,568 --> 00:04:26,768
Yeah...

40
00:04:26,802 --> 00:04:29,237
Yeah.

41
00:04:29,271 --> 00:04:31,640
April 8.

42
00:04:31,674 --> 00:04:34,075
My birthday.

43
00:04:34,110 --> 00:04:36,077
Get cracking.

44
00:04:36,112 --> 00:04:38,613
I'm taking you to lunch
in four hours.

45
00:04:40,583 --> 00:04:43,551
Maggie, I really...

46
00:05:21,657 --> 00:05:23,258
You forgot to
bring the donuts.

47
00:05:23,292 --> 00:05:25,660
Shit.

48
00:05:25,695 --> 00:05:27,128
Sorry.

49
00:05:27,163 --> 00:05:29,664
Getting the donuts is
your most important job.

50
00:05:31,167 --> 00:05:33,134
Good morning,
boys and girls.

51
00:05:33,169 --> 00:05:35,003
Okay.

52
00:05:35,037 --> 00:05:37,338
Last night's intakes.

53
00:05:37,373 --> 00:05:39,441
We got satellite photos

54
00:05:39,475 --> 00:05:41,776
of Grant's favorite
imaginary missile silos.

55
00:05:41,811 --> 00:05:43,344
Well, now...

56
00:05:43,379 --> 00:05:44,913
Better safe
than sorry.

57
00:05:44,947 --> 00:05:46,481
The Iranians are not
currently our best friends.

58
00:05:46,515 --> 00:05:48,783
Miles,

59
00:05:48,818 --> 00:05:51,986
why don't you

60
00:05:52,021 --> 00:05:54,089
take a look at our
brothers in Pakistan?

61
00:05:54,123 --> 00:05:56,124
They've taken a sudden
interest in telemarketing.

62
00:05:56,158 --> 00:05:57,258
Telemarketing?!

63
00:05:57,293 --> 00:05:58,326
Yeah.
Run it through Hal.

64
00:05:58,360 --> 00:05:59,861
Will...

65
00:05:59,895 --> 00:06:02,864
want to take another run
at Yuri the missile salesman?

66
00:06:02,898 --> 00:06:04,599
Not without more financials.

67
00:06:04,633 --> 00:06:06,201
He's running everything
through the Caymans...

68
00:06:06,235 --> 00:06:08,036
Well, seems CIA borrowed
his briefcase yesterday,

69
00:06:08,070 --> 00:06:10,305
and it's just full
of bank statements.

70
00:06:12,541 --> 00:06:15,510
And last,
but not least, Tanya,

71
00:06:15,544 --> 00:06:17,011
as our newest member,

72
00:06:17,046 --> 00:06:19,814
you, of course, get
the most difficult

73
00:06:19,849 --> 00:06:22,917
and tedious
of the day's intake.

74
00:06:22,952 --> 00:06:25,186
Thank you.

75
00:06:25,221 --> 00:06:27,055
Read it and weep.

76
00:06:27,089 --> 00:06:29,157
...weapons, organizational
structure...

77
00:06:29,191 --> 00:06:32,327
This will take me...

78
00:06:32,361 --> 00:06:34,829
the rest of my life.

79
00:06:34,864 --> 00:06:38,566
Ugh! I...
I need your help.

80
00:06:44,306 --> 00:06:46,274
Will's help.

81
00:06:46,308 --> 00:06:47,709
I'm just pitching in.

82
00:06:47,743 --> 00:06:49,110
Yeah. All right.

83
00:06:50,513 --> 00:06:52,247
Lanarca,
Seville, Ajaccio

84
00:06:52,281 --> 00:06:54,449
all have fountains
in their main public square,

85
00:06:54,483 --> 00:06:56,084
but not Dubrovnik.

86
00:06:56,118 --> 00:06:58,153
Uh, Dubrovnik and Lanarca
have university hospitals,

87
00:06:58,187 --> 00:06:59,821
but not Seville
and Ajaccio.

88
00:06:59,855 --> 00:07:02,290
I mean, what-what, what
the hell's the pattern?

89
00:07:02,324 --> 00:07:03,758
This is the Malaysian cipher?

90
00:07:03,793 --> 00:07:06,194
Yeah.

91
00:07:06,228 --> 00:07:08,830
Maybe some kind
of syllae algorithm?

92
00:07:08,864 --> 00:07:10,465
Yeah, yeah,
I tried that.

93
00:07:10,499 --> 00:07:12,534
Even Hal
drew a blank.

94
00:07:14,904 --> 00:07:18,173
They were all part
of the Roman Empire.

95
00:07:18,207 --> 00:07:21,543
They all have Palearctic
Mediterranean rain forest

96
00:07:21,577 --> 00:07:23,378
and are part
of the chapparal biome;

97
00:07:23,412 --> 00:07:25,480
they're all
in countries that spend

98
00:07:25,514 --> 00:07:28,016
less than five percent
of their GDP on the military,

99
00:07:28,050 --> 00:07:30,919
but maybe we are asking
the wrong question.

100
00:07:30,953 --> 00:07:33,488
You're looking at the past
and the present.

101
00:07:35,157 --> 00:07:36,691
What about the future?

102
00:07:36,725 --> 00:07:39,227
Itinerary possibly?

103
00:08:09,959 --> 00:08:11,659
Let's go.

104
00:08:11,694 --> 00:08:13,528
Up and out. Lunch.

105
00:08:18,601 --> 00:08:20,501
Can I take a rain check?

106
00:08:20,536 --> 00:08:21,936
I knew it.

107
00:08:21,971 --> 00:08:23,471
I-I... I'm...
I have so much

108
00:08:23,505 --> 00:08:26,074
work to do, I just...

109
00:08:27,443 --> 00:08:29,944
Rain check then.

110
00:08:31,180 --> 00:08:33,114
I'm not giving up.

111
00:10:15,884 --> 00:10:17,919
What's with the broom?

112
00:10:19,088 --> 00:10:20,955
Um...

113
00:10:20,990 --> 00:10:23,591
Mitchell was sweeping up the
other night as I was leaving.

114
00:10:23,625 --> 00:10:26,060
He brushed against my foot
w"by accident.

115
00:10:26,095 --> 00:10:27,562
Let me guess.

116
00:10:27,596 --> 00:10:29,163
Ten years bad luck?

117
00:10:29,198 --> 00:10:30,865
Worse. Much worse.

118
00:10:30,899 --> 00:10:32,433
Brings imprisonment or death.

119
00:10:32,468 --> 00:10:35,103
The only way around it is
to spit on the broom,

120
00:10:35,137 --> 00:10:38,306
so I bought it from him
and did what I had to do.

121
00:10:38,340 --> 00:10:40,074
It's ridiculous, I know.

122
00:10:40,109 --> 00:10:42,477
David, I, um...

123
00:10:42,511 --> 00:10:45,279
I think I found a pattern
in the big ticket papers.

124
00:10:45,314 --> 00:10:47,515
There might be others...
I'm not sure...

125
00:10:47,549 --> 00:10:49,083
but it's not just
the repetition.

126
00:10:49,118 --> 00:10:50,551
Three down.

127
00:10:50,586 --> 00:10:52,487
Two chambers
of the legislative branch.

128
00:10:52,521 --> 00:10:54,055
Bicameral. Simple enough.

129
00:10:54,089 --> 00:10:56,224
Two across... Fillmore,
where the Warlocks,

130
00:10:56,258 --> 00:10:57,558
later known
as the Grateful Dead,

131
00:10:57,593 --> 00:10:58,693
played in San Francisco.

132
00:10:58,727 --> 00:11:00,661
But also,
Millard Fillmore...

133
00:11:00,696 --> 00:11:03,064
lard-ass, know-nothing
13th President.

134
00:11:03,098 --> 00:11:05,400
The executive. Four down...

135
00:11:05,434 --> 00:11:06,834
would-be alma mater

136
00:11:06,869 --> 00:11:08,302
of felonious record-holding
wide receiver.

137
00:11:08,337 --> 00:11:11,139
Answer: Marshall,
as in Randy Moss,

138
00:11:11,173 --> 00:11:12,507
Marshall University
Thundering Herd.

139
00:11:12,541 --> 00:11:15,543
But also, Thurgood
Marshall, judicial.

140
00:11:15,577 --> 00:11:16,978
Five down.

141
00:11:17,012 --> 00:11:19,414
<i>What do lucky
lepidoptera larvae eat?</i>

142
00:11:19,448 --> 00:11:21,149
<i>Answer: marsilea
quadrifolia.</i>

143
00:11:21,183 --> 00:11:22,850
Which is? Four-leaf clover.

144
00:11:22,885 --> 00:11:25,753
Our three branches
of government are here...

145
00:11:25,788 --> 00:11:28,723
legislative,
executive, judicial.

146
00:11:28,757 --> 00:11:31,859
What or who does
that fourth leaf represent?

147
00:11:31,894 --> 00:11:34,595
And what's
the message?

148
00:11:34,630 --> 00:11:37,065
I've seen this before.

149
00:11:37,099 --> 00:11:39,700
Crossword editors,
quirky sense of humor.

150
00:11:39,735 --> 00:11:42,603
Probably some kind of inside
joke. No, no, no, no, no.

151
00:11:42,638 --> 00:11:44,172
I've done my share
of crosswords, and I've never

152
00:11:44,206 --> 00:11:45,540
seen anything
quite like this. I mean...

153
00:11:45,574 --> 00:11:48,209
I can see why this
caught your eye.

154
00:11:49,244 --> 00:11:51,679
Thanks, Will.

155
00:12:13,102 --> 00:12:14,569
American Policy Institute.

156
00:12:14,603 --> 00:12:16,370
How may I help you?

157
00:12:22,244 --> 00:12:26,481
80 years of work
to build a business,

158
00:12:26,515 --> 00:12:29,484
five hours of trading
to put it on life support.

159
00:12:29,518 --> 00:12:31,786
All from a few

160
00:12:31,820 --> 00:12:35,756
artfully chosen words.

161
00:12:35,791 --> 00:12:38,559
It's a thing of beauty.

162
00:12:39,661 --> 00:12:42,096
Something
in the crossword today.

163
00:12:42,131 --> 00:12:44,398
I've never seen
anything like it.

164
00:12:46,135 --> 00:12:49,103
Crossword?

165
00:12:49,138 --> 00:12:51,005
Which paper?

166
00:12:51,039 --> 00:12:53,307
All the indicators.

167
00:12:53,342 --> 00:12:56,277
Someone either
didn't expect

168
00:12:56,311 --> 00:12:59,647
the pattern to be caught,
or wasn't afraid if it was.

169
00:13:00,716 --> 00:13:03,317
Anybody else see this?

170
00:13:03,352 --> 00:13:05,820
Internally? No.

171
00:13:16,965 --> 00:13:19,500
Good catch.

172
00:13:36,051 --> 00:13:38,819
Why cereal for lunch?

173
00:13:40,556 --> 00:13:42,623
Cornflakes,
specifically.

174
00:13:42,658 --> 00:13:45,660
Every day.

175
00:13:45,694 --> 00:13:48,296
Spangler's a man of habits.

176
00:13:48,330 --> 00:13:49,897
Have you ever
talked to him?

177
00:13:49,932 --> 00:13:52,033
I bumped into
him once, and, uh...

178
00:13:53,602 --> 00:13:56,304
...said I'm sorry.

179
00:13:57,372 --> 00:13:59,207
This is

180
00:13:59,241 --> 00:14:00,608
inedible.

181
00:14:04,112 --> 00:14:05,613
So, tell me something.

182
00:14:05,647 --> 00:14:09,183
Why is Will Travers
so mopey?

183
00:14:09,218 --> 00:14:11,586
He's not mopey.

184
00:14:11,620 --> 00:14:14,055
He's, well, just introspective.

185
00:14:14,089 --> 00:14:15,456
He walks around every day

186
00:14:15,490 --> 00:14:18,492
looking like his
favorite cat just died.

187
00:14:22,864 --> 00:14:25,333
Try wife and child.

188
00:14:25,367 --> 00:14:26,834
Try 9/11.

189
00:14:33,408 --> 00:14:36,510
Morning of, he was supposed
to meet his wife and daughter

190
00:14:36,545 --> 00:14:38,646
at the top
of the World Trade Center.

191
00:14:38,680 --> 00:14:41,682
A surprise
for his daughter's birthday.

192
00:14:41,717 --> 00:14:43,684
He was late.

193
00:14:43,719 --> 00:14:45,953
He was always late.

194
00:14:45,988 --> 00:14:47,722
He was coming out of the subway

195
00:14:47,756 --> 00:14:49,690
when the first plane hit
the tower.

196
00:14:52,728 --> 00:14:56,197
And he's never been late
for anything since.

197
00:15:14,316 --> 00:15:16,917
I'll have the same.

198
00:15:16,952 --> 00:15:18,486
Uh, for both of them.

199
00:15:18,520 --> 00:15:20,087
Happy birthday.

200
00:15:20,122 --> 00:15:22,390
Thank you, David.

201
00:15:26,995 --> 00:15:28,596
So,

202
00:15:28,630 --> 00:15:31,098
uh, I want you to know
I'm almost done

203
00:15:31,133 --> 00:15:33,234
with a first draft
on the Nigeria thing.

204
00:15:33,268 --> 00:15:35,636
Good. How is it?

205
00:15:35,671 --> 00:15:37,405
It's what you asked for.

206
00:15:37,439 --> 00:15:39,073
We're dealing with the
hypotheticals of instability,

207
00:15:39,107 --> 00:15:40,975
but I think
the more interesting story is

208
00:15:41,009 --> 00:15:43,244
in the infant democracy.

209
00:15:43,278 --> 00:15:44,745
What's the big
picture here?

210
00:15:44,780 --> 00:15:46,881
You'll know soon enough.

211
00:15:51,353 --> 00:15:53,587
And how's Miles doing
with North Korea?

212
00:15:55,857 --> 00:16:00,161
You know what
the biggest joke is here?

213
00:16:00,195 --> 00:16:02,663
It's my business,
our business,

214
00:16:02,698 --> 00:16:04,699
to tell people
what to think.

215
00:16:04,733 --> 00:16:07,268
And the truth is,

216
00:16:07,302 --> 00:16:10,638
I have no idea
what to think anymore.

217
00:16:10,672 --> 00:16:12,173
About what?

218
00:16:12,207 --> 00:16:14,008
This.

219
00:16:14,042 --> 00:16:16,677
What we do.

220
00:16:21,249 --> 00:16:22,650
Happy birthday.

221
00:16:29,291 --> 00:16:32,326
Road food!

222
00:16:32,361 --> 00:16:34,628
Well!

223
00:16:35,764 --> 00:16:38,065
You reading
minds these days?

224
00:16:38,100 --> 00:16:40,334
Look, I don't blame you
for taking stock

225
00:16:40,369 --> 00:16:41,769
and weighing your options.

226
00:16:41,803 --> 00:16:43,838
With a workload
like yours...

227
00:16:43,872 --> 00:16:45,940
No, it's not that.

228
00:16:45,974 --> 00:16:48,442
I don't work too much.

229
00:16:48,477 --> 00:16:50,077
It's just that
when I leave,

230
00:16:50,112 --> 00:16:51,979
I can't talk to
anybody about it.

231
00:16:52,013 --> 00:16:54,181
That's the hdest thing
in this job,

232
00:16:54,216 --> 00:16:56,917
you know, keeping secrets.

233
00:16:59,187 --> 00:17:02,923
What... What are your
birthday plans tonight?

234
00:17:02,958 --> 00:17:04,525
Count the minutes
till it's over.

235
00:17:04,559 --> 00:17:05,893
No. No, no, no.

236
00:17:05,927 --> 00:17:07,294
Why don't you...

237
00:17:07,329 --> 00:17:09,697
Why don't you come
and eat dinner with us?

238
00:17:09,731 --> 00:17:12,199
No.
Huh? Why not?

239
00:17:14,803 --> 00:17:17,838
Days like these I...
I used to spend

240
00:17:17,873 --> 00:17:21,375
with Natalie
and Shauny.

241
00:17:21,410 --> 00:17:23,911
I miss 'em too much.

242
00:17:25,013 --> 00:17:27,314
I do, too.

243
00:17:27,349 --> 00:17:29,917
But they're gone, Will.

244
00:17:29,951 --> 00:17:33,654
They're gone,
and that's just something

245
00:17:33,688 --> 00:17:37,158
both of us have to accept.

246
00:17:46,868 --> 00:17:49,336
This goes with the book.

247
00:17:49,371 --> 00:17:53,307
Don't open it
till you get home.

248
00:18:39,921 --> 00:18:41,589
David.

249
00:18:41,623 --> 00:18:43,357
David, your Norton? Uh, it's...

250
00:18:43,391 --> 00:18:45,392
Are you crazy?
I can't keep this.

251
00:18:45,427 --> 00:18:47,828
You're smiling.

252
00:18:47,863 --> 00:18:50,364
I can hear you smiling, Will.

253
00:18:50,398 --> 00:18:52,466
You're keeping it.

254
00:18:52,501 --> 00:18:55,135
The bike, the note.
Why-Why now?

255
00:18:57,372 --> 00:19:00,040
I'll tell you in the morning.

256
00:19:00,075 --> 00:19:03,077
I'm catching the 5:51
out of Putnam Station.

257
00:19:03,111 --> 00:19:06,046
Meet me at the Annex Bar.

258
00:19:07,249 --> 00:19:09,583
I'll see you there.

259
00:19:09,618 --> 00:19:11,352
Oh, wait. Wait. David...

260
00:19:48,957 --> 00:19:52,860
Which was part
of a firecracker ceremony

261
00:19:52,894 --> 00:19:56,397
that's intended
to scare off bad spirits.

262
00:19:56,431 --> 00:19:58,699
They were very loud
and I enjoyed it very much.

263
00:19:58,733 --> 00:20:02,403
Basically, I learned,
at a very young age, it's part

264
00:20:02,437 --> 00:20:05,406
of your culture and heritage,
so it's very important.

265
00:20:05,440 --> 00:20:08,142
Thomas K. Rhumor died
in his sleep

266
00:20:08,176 --> 00:20:09,643
last night at the age of 64.

267
00:20:09,678 --> 00:20:11,412
I'm writing.
I'm fine.

268
00:20:11,446 --> 00:20:14,315
Rhumor was a self-made
billionaire whose philanthropy

269
00:20:14,349 --> 00:20:15,749
focused here at home

270
00:20:15,784 --> 00:20:19,219
on the needs
of the underprivileged.

271
00:20:19,254 --> 00:20:22,389
And we go back now
to the breaking news story...

272
00:20:22,424 --> 00:20:23,924
we just told you about.
Wow. you see this?

273
00:20:23,959 --> 00:20:25,326
We're getting
the first pictures...

274
00:20:25,360 --> 00:20:26,794
Damn shame.

275
00:20:26,828 --> 00:20:28,462
...of a train crash
that's taken place

276
00:20:28,496 --> 00:20:31,165
just outside of Putnam Station
on the Fairchester line.

277
00:20:31,199 --> 00:20:34,401
Emergency crews are
on the scene.

278
00:20:34,436 --> 00:20:37,671
Early reports indicate
multiple fatalities.

279
00:20:56,443 --> 00:21:01,712
But what I admired most
about my friend was his loyalty.

280
00:21:01,747 --> 00:21:07,084
We all slept a little
better knowing that someone

281
00:21:07,119 --> 00:21:11,222
with his integrity
was in our lives.

282
00:21:11,256 --> 00:21:14,825
He was a devoted
husband to Joan,

283
00:21:14,860 --> 00:21:19,864
father-in-law to Will,

284
00:21:19,898 --> 00:21:24,835
father to Anna, Kevin
and Natalie,

285
00:21:24,870 --> 00:21:27,338
God rest her soul.

286
00:21:27,372 --> 00:21:29,507
David was an affirming

287
00:21:29,541 --> 00:21:32,410
flame, and we
should all feel

288
00:21:32,444 --> 00:21:35,746
grateful for having known him.

289
00:21:48,327 --> 00:21:50,428
Will...

290
00:21:50,462 --> 00:21:53,397
I know. I know.

291
00:21:55,801 --> 00:21:58,369
It's okay. It's okay.

292
00:22:02,741 --> 00:22:05,242
I am so sorry, Will.

293
00:22:07,279 --> 00:22:09,747
So am I.

294
00:22:13,885 --> 00:22:16,053
You spoke
eloquently.

295
00:22:16,088 --> 00:22:18,589
Thank you,
Ms. Young.

296
00:22:20,625 --> 00:22:22,526
I'll catch up.

297
00:22:27,466 --> 00:22:30,201
We have something pressing
to discuss, Will.

298
00:22:34,639 --> 00:22:36,240
David's position

299
00:22:36,274 --> 00:22:39,143
was far too integral

300
00:22:39,177 --> 00:22:41,879
to leave open for
even a week or a month

301
00:22:41,913 --> 00:22:44,448
good decorum
would demand.

302
00:22:46,785 --> 00:22:51,522
So I'd like to put off this
conversation, but I can't.

303
00:22:51,556 --> 00:22:55,226
People I answer to,
upstairs and elsewhere,

304
00:22:55,260 --> 00:22:58,729
are, uh... eager
for resolution.

305
00:23:01,299 --> 00:23:05,603
I'd like you to step
into his position.

306
00:23:08,140 --> 00:23:10,941
Well, I'd say I'm better off
staying where I am,

307
00:23:10,976 --> 00:23:14,044
but I don't know
if that's true anymore.

308
00:23:14,079 --> 00:23:16,881
It might be best
for me to leave.

309
00:23:22,487 --> 00:23:24,722
I expected that.

310
00:23:24,756 --> 00:23:30,060
Well, take a couple of
days to think it over.

311
00:23:30,095 --> 00:23:32,496
If your mind
doesn't change,

312
00:23:32,531 --> 00:23:35,065
I'll accept that answer

313
00:23:35,100 --> 00:23:37,568
and your resignation.

314
00:24:54,246 --> 00:24:57,114
Knight to king's bishop three.

315
00:25:00,685 --> 00:25:03,787
Knight to king's bishop three.

316
00:25:05,290 --> 00:25:07,424
Who is this?

317
00:25:07,459 --> 00:25:10,160
Let me talk to David.

318
00:25:11,630 --> 00:25:12,897
Where's David?

319
00:25:12,931 --> 00:25:14,098
Uh, no, uh...

320
00:25:15,367 --> 00:25:17,101
There was an accident.

321
00:28:02,814 --> 00:28:06,216
Are you Ed Bancroft?

322
00:28:06,251 --> 00:28:09,720
My-My name is Will Travers.

323
00:28:09,754 --> 00:28:13,424
I answered the phone
when you called David's office.

324
00:28:13,458 --> 00:28:16,493
May I come inside
and speak to you for a moment?

325
00:28:42,120 --> 00:28:45,122
David's been killed.

326
00:28:45,156 --> 00:28:48,559
He died in a train accident
on his way to work.

327
00:29:02,807 --> 00:29:05,275
Can I get you some tea?

328
00:29:06,611 --> 00:29:08,645
No, thank you.

329
00:29:08,680 --> 00:29:10,080
I'm addicted to Earl Grey.

330
00:29:10,115 --> 00:29:13,317
Sit.

331
00:29:18,089 --> 00:29:20,591
David said
you were very bright.

332
00:29:20,625 --> 00:29:23,026
One of the best
in the building.

333
00:29:23,061 --> 00:29:26,330
That true?

334
00:29:28,800 --> 00:29:31,268
I keep up.

335
00:29:33,805 --> 00:29:36,640
I'll never be a
legend like you.

336
00:29:36,674 --> 00:29:39,309
What does the legend
say about me?

337
00:29:39,344 --> 00:29:42,613
You were a genius
at cracking codes,

338
00:29:42,647 --> 00:29:46,283
until the codes

339
00:29:46,317 --> 00:29:49,119
cracked you like an egg.

340
00:29:49,154 --> 00:29:51,722
Like an egg.

341
00:29:51,756 --> 00:29:55,025
Not my metaphor.

342
00:29:55,059 --> 00:29:58,629
David was the only one
who stayed in touch.

343
00:30:01,866 --> 00:30:04,301
We played
our endless chess games.

344
00:30:04,335 --> 00:30:07,838
Talked.

345
00:30:07,872 --> 00:30:10,374
I really can't believe
he's dead.

346
00:30:14,245 --> 00:30:17,147
He was very fond of you.

347
00:30:17,182 --> 00:30:19,516
You never broke his heart

348
00:30:19,551 --> 00:30:21,952
like that idiot son of his,

349
00:30:21,986 --> 00:30:24,488
up in Vermont.

350
00:30:24,522 --> 00:30:27,658
Evan's troubled.

351
00:30:27,692 --> 00:30:30,594
He's a nut.

352
00:30:31,796 --> 00:30:35,499
Ed... I need
to show you something.

353
00:30:39,904 --> 00:30:42,105
Haveou...

354
00:30:42,140 --> 00:30:44,374
you ever seeany?

355
00:30:52,050 --> 00:30:56,653
Four crossword puzzles

356
00:30:56,688 --> 00:30:59,957
shared across
several newspapers.

357
00:31:02,527 --> 00:31:04,862
No.

358
00:31:04,896 --> 00:31:07,164
This is new to me.

359
00:31:07,198 --> 00:31:08,832
Sorry.

360
00:31:14,172 --> 00:31:17,374
They showed up
the day before David died.

361
00:31:20,879 --> 00:31:23,413
Coincidence?

362
00:31:27,018 --> 00:31:29,253
It was nice
to meet you, Will.

363
00:31:29,287 --> 00:31:31,688
I'm feeling very tired.

364
00:31:31,723 --> 00:31:34,191
You can let yourself out.

365
00:31:35,260 --> 00:31:38,729
Right.

366
00:31:44,769 --> 00:31:47,537
Good night.

367
00:34:32,236 --> 00:34:35,105
Sorry to wake you.

368
00:34:35,139 --> 00:34:37,641
I didn't know where else to go.

369
00:34:45,450 --> 00:34:48,518
I know how this is going to
sound, but all his rituals,

370
00:34:48,553 --> 00:34:50,220
his rituals, how
superstitious he was...

371
00:34:50,254 --> 00:34:51,955
Slow down.

372
00:34:51,989 --> 00:34:53,590
His biggest phobia by far was 13.

373
00:34:53,624 --> 00:34:55,025
I mean, he respected
and feared it,

374
00:34:55,059 --> 00:34:56,359
he constantly
watched out for it.

375
00:34:56,394 --> 00:34:58,095
He would
never get on a boat,

376
00:34:58,129 --> 00:34:59,563
an elevator, train,
a boat, a taxi...

377
00:34:59,597 --> 00:35:01,298
Will, can you
lower your voice?

378
00:35:01,332 --> 00:35:02,599
...or train that started

379
00:35:02,633 --> 00:35:04,534
or ended with 13,
was divisible by 13,

380
00:35:04,569 --> 00:35:07,170
aggregated to 13...
Will... Will, your voice.

381
00:35:07,205 --> 00:35:09,005
They haven't... they haven't
retrieved his car yet.

382
00:35:09,040 --> 00:35:11,208
His car is still there
at the train depot

383
00:35:11,242 --> 00:35:12,542
where he left it.

384
00:35:12,577 --> 00:35:14,511
Where he supposedly left it.
Supposedly?

385
00:35:14,545 --> 00:35:16,279
It was parked
in spot 13.

386
00:35:16,314 --> 00:35:18,548
Well, maybe he didn't
see the number.

387
00:35:18,583 --> 00:35:20,050
He saw it.

388
00:35:20,084 --> 00:35:22,252
Well, maybe he just
needed to get to work,

389
00:35:22,286 --> 00:35:23,753
he saw the train coming up,

390
00:35:23,788 --> 00:35:26,189
and he took the only spot
that was there. No, no, no.

391
00:35:26,224 --> 00:35:28,625
There's no way David Hadas would
have parked in that spot.

392
00:35:28,659 --> 00:35:30,594
So if he didn't
park in the spot...

393
00:35:30,628 --> 00:35:32,963
Then he didn't get on the train.

394
00:35:32,997 --> 00:35:35,565
But his remains
were identified.

395
00:35:35,600 --> 00:35:37,767
I know that.

396
00:35:37,802 --> 00:35:40,270
So, how were they found there
in the wreckage?

397
00:35:40,304 --> 00:35:42,105
I don't know.

398
00:35:42,140 --> 00:35:43,907
I don't know.
I don't know. I don't know.

399
00:35:43,941 --> 00:35:46,743
What happened was horrible,
but it happened.

400
00:35:46,777 --> 00:35:49,112
He's gone.

401
00:35:49,147 --> 00:35:51,014
He boarded the train,
and it crashed.

402
00:35:51,048 --> 00:35:53,150
He died with 36 strangers.

403
00:35:53,184 --> 00:35:54,718
Maybe there is no why.

404
00:35:54,752 --> 00:35:56,253
There's always a why.
Will.

405
00:35:56,287 --> 00:35:58,722
You just don't
understand it.

406
00:36:03,194 --> 00:36:06,329
When's the last time
you had a good night's sleep?

407
00:36:06,364 --> 00:36:09,499
You need to rest.

408
00:36:09,534 --> 00:36:12,068
This is hard.

409
00:36:14,205 --> 00:36:16,506
Yeah.

410
00:36:18,543 --> 00:36:20,777
Mommy?

411
00:36:22,847 --> 00:36:25,348
I'm-I'm sorry.

412
00:36:26,684 --> 00:36:29,219
I'm-I'm so sorry.
I, uh...

413
00:37:52,051 --> 00:37:55,419
Did you get any rest?

414
00:37:57,722 --> 00:37:59,323
I'm sorry about last night.

415
00:37:59,357 --> 00:38:01,292
Is your daughter,
is she...?

416
00:38:01,326 --> 00:38:03,394
Uh, she-she's fine.

417
00:38:05,063 --> 00:38:07,131
What's that?

418
00:38:13,772 --> 00:38:15,773
My resignation.

419
00:38:15,807 --> 00:38:18,242
Don't worry,

420
00:38:18,276 --> 00:38:20,744
I won't be here, but your life
isn't going to change much.

421
00:38:20,779 --> 00:38:24,448
You don't call working
for Grant Test a change?

422
00:38:24,482 --> 00:38:27,985
You know they're gonna put him in
David's job the minute you turn it down.

423
00:38:28,019 --> 00:38:31,155
And where are you going?

424
00:38:31,189 --> 00:38:33,924
Away.

425
00:38:33,959 --> 00:38:36,493
I'm going away.

426
00:38:54,512 --> 00:38:55,813
You can't resign.

427
00:38:55,847 --> 00:38:57,615
Why not?

428
00:38:57,649 --> 00:39:00,384
Because I would have
to work for Grant.

429
00:39:00,418 --> 00:39:01,719
Glory has its price.

430
00:39:01,753 --> 00:39:03,454
I'm serious.

431
00:39:03,488 --> 00:39:05,856
You can't quit. You do
not have that option.

432
00:39:05,890 --> 00:39:07,825
Sure I do.
No, you don't.

433
00:39:16,735 --> 00:39:20,104
It was my birthday
last week.

434
00:39:20,138 --> 00:39:22,573
I had no idea.

435
00:39:22,607 --> 00:39:25,209
Maggie reminded me.

436
00:39:25,243 --> 00:39:27,478
Yeah, I forget stuff
all the time. So what?

437
00:39:27,512 --> 00:39:29,013
Your own birthday?
We all get

438
00:39:29,047 --> 00:39:31,181
a little preoccupied.
It goes without saying.

439
00:39:31,216 --> 00:39:33,684
It is the nature
of the job.

440
00:39:33,718 --> 00:39:36,020
Miles, this place is a tomb.

441
00:39:39,758 --> 00:39:41,759
I'm really sorry
about David.

442
00:39:41,793 --> 00:39:44,895
I... know what
he meant to you.

443
00:39:44,929 --> 00:39:46,997
I know he brought
you here.

444
00:39:47,032 --> 00:39:49,233
And I... I know he
took care of you.

445
00:39:49,267 --> 00:39:53,704
But I really don't think
that he would want you to quit.

446
00:39:56,808 --> 00:39:58,709
I'm not so sure about that.

447
00:39:58,743 --> 00:40:02,780
I think that he would want
you to pick up the torch.

448
00:40:04,282 --> 00:40:07,418
Go away.

449
00:40:08,787 --> 00:40:14,458
Well, if nothing else,
stay for David.

450
00:41:09,214 --> 00:41:10,848
What changed your mind?

451
00:41:14,085 --> 00:41:16,820
You know what? I don't care.

452
00:41:16,855 --> 00:41:19,590
Let's introduce
you upstairs.

453
00:42:08,606 --> 00:42:11,475
The last time was
just plain embarrassing.

454
00:42:11,509 --> 00:42:14,845
Well, you can't bring that kind
of stuff home with you.

455
00:42:16,281 --> 00:42:18,382
Yeah, leave it
at work, please.

456
00:42:20,685 --> 00:42:22,653
Oh, what, me?

457
00:42:25,757 --> 00:42:27,858
All right, look,
I'm hanging up now.

458
00:42:27,892 --> 00:42:30,394
Yes, I am.

459
00:42:30,428 --> 00:42:32,596
Mr. Spangler got called
down to Washington.

460
00:42:32,630 --> 00:42:35,032
He wants to meet you
tomorrow morning.

461
00:42:35,066 --> 00:42:37,701
9:00 sharp.

462
00:42:39,170 --> 00:42:41,171
Old one, please.

463
00:42:49,414 --> 00:42:52,216
This is your new
security agreement.

464
00:42:52,250 --> 00:42:55,118
You'll be cleared to
a higher level: G5.

465
00:42:55,153 --> 00:42:57,354
Sign here, here...

466
00:42:58,756 --> 00:43:00,123
...and here.

467
00:44:02,420 --> 00:44:06,323
Anybody double-check
that Tom is actually dead?

468
00:44:07,558 --> 00:44:09,559
He blew his
brains out.

469
00:44:09,594 --> 00:44:12,195
I saw his body,
for God's sake.

470
00:44:13,631 --> 00:44:15,032
Good.

471
00:44:17,235 --> 00:44:19,670
We're back on track.

472
00:44:23,000 --> 00:44:28,000
Sync by fant0m
www.addic7ed.com

