1
00:05:20,787 --> 00:05:23,119
Anyway, it was worthless.

2
00:05:23,189 --> 00:05:24,781
Couldn't even speak.

3
00:07:37,757 --> 00:07:40,817
There... there ain't much.

4
00:08:15,194 --> 00:08:17,162
Get up.

5
00:09:32,238 --> 00:09:34,263
It's crazy to go on.

6
00:09:36,042 --> 00:09:38,067
We'll never get there.

7
00:09:56,195 --> 00:09:58,561
We'll never get there this way.

8
00:09:58,631 --> 00:10:00,292
You're so stubborn.

9
00:10:00,366 --> 00:10:02,391
Try going back.

10
00:10:04,270 --> 00:10:05,931
There's no end to it.

11
00:10:06,005 --> 00:10:07,700
We should have arrived
at midday.

12
00:10:08,774 --> 00:10:11,709
The sand's hard on the feet.

13
00:11:08,901 --> 00:11:11,699
Keep walking,
you lazy critter!

14
00:11:28,921 --> 00:11:30,855
Come on! It's there!

15
00:15:56,488 --> 00:15:58,456
It's going to rain.

16
00:15:58,524 --> 00:16:02,824
God willing
and the Holy Virgin too!

17
00:18:33,579 --> 00:18:35,137
It's a sturdy house.

18
00:18:40,819 --> 00:18:42,116
Good pasture.

19
00:18:43,055 --> 00:18:45,023
Good for fattening up.

20
00:18:53,232 --> 00:18:54,927
Good pasture.

21
00:19:10,482 --> 00:19:12,541
What happened to Tom�s?

22
00:19:20,092 --> 00:19:22,652
He went out on his own,
like us.

23
00:19:22,728 --> 00:19:25,196
Think he took everything
with him?

24
00:19:26,098 --> 00:19:28,123
I bet he left
the grinding wheel.

25
00:19:28,200 --> 00:19:30,964
Think he took the leather bed?

26
00:19:31,036 --> 00:19:33,027
It was so lovely.

27
00:19:34,139 --> 00:19:37,802
So soft and cozy
in the corner of the room.

28
00:19:37,876 --> 00:19:41,209
Tom�s of the sugar-mill
had the best.

29
00:19:41,280 --> 00:19:44,909
Tom�s was a worthy man.

30
00:19:44,983 --> 00:19:47,816
Nice smell of tanned leather.

31
00:19:48,820 --> 00:19:51,653
All it needed
was a bit of lace

32
00:19:51,723 --> 00:19:55,250
and it would look
like a chapel.

33
00:19:55,327 --> 00:19:57,295
A man of letters.

34
00:19:57,362 --> 00:19:59,762
He always said
the right things

35
00:19:59,831 --> 00:20:01,093
even when it was
going to rain.

36
00:20:01,166 --> 00:20:03,600
He knew how to do things.

37
00:20:03,669 --> 00:20:06,035
He could always mend everything

38
00:20:06,104 --> 00:20:07,799
even other people's lives.

39
00:20:07,873 --> 00:20:11,468
Even that! But he could never
forgive the drought.

40
00:20:11,543 --> 00:20:15,001
The farm has died
without Tom�s and the mill.

41
00:20:15,080 --> 00:20:19,915
On day we'll have a leather bed
like Tom�s's.

42
00:21:03,495 --> 00:21:06,521
Get your tools and go back.

43
00:21:06,598 --> 00:21:08,225
I don't want anybody here.

44
00:21:52,678 --> 00:21:56,739
I told you to leave!
And now!

45
00:21:56,815 --> 00:21:59,045
I'm a good cowhand.

46
00:22:00,285 --> 00:22:01,650
I'm a good cowhand.

47
00:22:02,487 --> 00:22:03,954
I can do anything.

48
00:22:04,022 --> 00:22:05,319
Maybe...

49
00:22:07,893 --> 00:22:10,020
Are these your men?
That's all?

50
00:22:11,263 --> 00:22:14,323
And the dog.
I'll do any job.

51
00:22:14,399 --> 00:22:16,196
How much do you charge?

52
00:22:20,939 --> 00:22:24,568
My usual.
One calf of each four born.

53
00:22:24,643 --> 00:22:28,443
A quarter's fine.
You can stay.

54
00:23:02,614 --> 00:23:04,275
Let's go, girl!

55
00:24:43,482 --> 00:24:47,748
Spotty... you'll grow,
my lovely!

56
00:30:15,814 --> 00:30:17,441
100,000 r�is per head.

57
00:30:21,252 --> 00:30:22,685
Isn't that fair?

58
00:31:56,981 --> 00:31:59,609
The boss says
it's 100,000 per head.

59
00:32:44,362 --> 00:32:47,024
The money will do to settle
accounts with the boss.

60
00:32:47,966 --> 00:32:51,766
We'll buy the leather for
the bed with what's left over,

61
00:32:51,836 --> 00:32:54,100
just like Tom�s's.

62
00:32:54,172 --> 00:32:56,299
We could spend less.

63
00:32:58,109 --> 00:33:01,101
Yes. We'll spend less.

64
00:33:03,014 --> 00:33:05,175
We'll sleep in a leather bed.

65
00:33:06,017 --> 00:33:07,848
We'll be real dandies!

66
00:34:42,613 --> 00:34:44,410
Do you want to settle today?

67
00:34:46,217 --> 00:34:47,912
How much do I owe you?

68
00:34:48,619 --> 00:34:51,918
Well... I mean...

69
00:34:53,091 --> 00:34:56,390
Get the book.
It's on top of the desk.

70
00:36:05,663 --> 00:36:08,188
Sorry, but that's not
quite enough.

71
00:36:17,708 --> 00:36:18,766
No, it's right.

72
00:36:20,011 --> 00:36:23,913
What my wife said is more.
There must be some mistake.

73
00:36:25,583 --> 00:36:27,483
It's the interest.

74
00:36:27,552 --> 00:36:30,020
Haven't I been lending you money
all year?

75
00:36:30,087 --> 00:36:31,486
There's no mistake.

76
00:36:31,556 --> 00:36:34,081
My wife knows her business.

77
00:36:34,158 --> 00:36:38,390
She knows accounts.
That's not enough.

78
00:36:38,462 --> 00:36:41,454
Here's your pay.
There's no more.

79
00:36:41,532 --> 00:36:44,194
This isn't right!
I'm no slave!

80
00:36:45,236 --> 00:36:48,000
There are no slaves here.
Take your money.

81
00:36:48,873 --> 00:36:51,103
Otherwise,
look for work elsewhere.

82
00:36:52,243 --> 00:36:54,234
I don't employ
impertinent upstarts!

83
00:36:56,180 --> 00:36:58,148
No need for arguments.

84
00:36:58,216 --> 00:37:04,212
Sorry, I didn't mean it.
It was my wife's fault, Boss.

85
00:37:04,288 --> 00:37:07,519
I can't read.
She told me, it was that.

86
00:37:07,592 --> 00:37:09,150
And I believed her.

87
00:37:09,994 --> 00:37:12,724
All right, Fabiano.
Get to work.

88
00:37:13,431 --> 00:37:17,299
But I won't fall for it again.
Sorry, sir.

89
00:37:25,776 --> 00:37:29,507
I tell you Manezinho's wife
betrayed him.

90
00:37:31,882 --> 00:37:34,248
I tried her,
but she wouldn't.

91
00:37:40,558 --> 00:37:42,253
Do you have the tax form?

92
00:37:44,695 --> 00:37:47,391
You have to pay taxes,
you know.

93
00:37:48,399 --> 00:37:50,026
Never heard of taxes.

94
00:37:50,101 --> 00:37:51,398
Is this pig yours?

95
00:37:52,503 --> 00:37:56,837
Is it for sale?
Then you must pay taxes.

96
00:38:01,078 --> 00:38:03,308
But that's not a pig.

97
00:38:05,283 --> 00:38:06,614
It's a bit of a pig.

98
00:38:06,684 --> 00:38:09,448
If you sell, you pay tax,
you trickster!

99
00:38:09,520 --> 00:38:10,544
You offend me!

100
00:38:10,621 --> 00:38:15,217
Sorry. I thought I could just
sell off my belongings.

101
00:38:15,960 --> 00:38:17,894
I didn't know about no taxes.

102
00:38:17,962 --> 00:38:19,122
Well, now you know.

103
00:38:19,196 --> 00:38:22,859
I didn't know the Town Hall
owned part of my pig.

104
00:38:22,933 --> 00:38:24,924
But as you said, it's over.

105
00:38:27,772 --> 00:38:31,970
I'll give the meat to my family.
Can I eat the meat?

106
00:38:32,610 --> 00:38:33,668
Can I, or not?

107
00:38:33,744 --> 00:38:35,712
Stop gabbing and go home!

108
00:39:13,951 --> 00:39:15,976
Come on!
Don't dirty your clothes.

109
00:45:25,155 --> 00:45:26,144
Give me one.

110
00:45:30,494 --> 00:45:32,121
How much?

111
00:45:32,196 --> 00:45:33,857
400 r�is.

112
00:45:40,704 --> 00:45:42,763
Why do you
water it all down?

113
00:46:01,759 --> 00:46:02,726
See you later.

114
00:46:03,794 --> 00:46:05,455
The party'll be good?

115
00:46:15,672 --> 00:46:18,869
Shall we play a hand inside?

116
00:46:20,277 --> 00:46:23,212
Yes or no.

117
00:46:24,114 --> 00:46:25,103
Let's see.

118
00:46:28,919 --> 00:46:31,615
Go away... There's people here.
Sit down.

119
00:47:52,536 --> 00:47:53,594
Take another.

120
00:48:13,557 --> 00:48:14,922
Give another.

121
00:48:15,926 --> 00:48:17,484
Give another.

122
00:48:26,303 --> 00:48:29,329
What's the card?
Turn it over.

123
00:48:50,894 --> 00:48:52,919
- Whose turn?
- Wait a minute.

124
00:49:01,738 --> 00:49:02,705
Hold on, mister.

125
00:49:41,078 --> 00:49:43,706
You have no right
to harass the law-abiding.

126
00:49:43,780 --> 00:49:45,771
Back off,
you low-down critter!

127
00:49:45,849 --> 00:49:48,340
That's no way
to treat your partners,

128
00:49:48,418 --> 00:49:50,318
to turn away and leave.

129
00:49:50,387 --> 00:49:54,517
Is it my fault you threw in
all your belongings?

130
00:50:24,254 --> 00:50:27,348
That's bad manners.

131
00:50:27,424 --> 00:50:29,358
Please take your foot away.

132
00:50:29,426 --> 00:50:30,984
Son of a bitch!

133
00:50:48,078 --> 00:50:49,340
Mummy, where's Baleia?

134
00:50:49,413 --> 00:50:51,142
I don't know.

135
00:51:02,793 --> 00:51:04,192
Has Baleia come back?

136
00:51:08,865 --> 00:51:09,832
Neither has Father.

137
00:51:09,900 --> 00:51:14,200
He resisted arrest and insulted
me in front of everybody.

138
00:51:14,271 --> 00:51:17,968
He must learn not to poke fun
at the authorities.

139
00:51:19,976 --> 00:51:21,375
Hand over the civilian.

140
00:52:38,388 --> 00:52:39,548
Shut up!

141
00:52:55,872 --> 00:53:00,502
Bastard!
Scum of the earth!

142
00:53:12,722 --> 00:53:14,553
Where's Baleia, Mum?

143
00:53:17,994 --> 00:53:19,291
She's disappeared.

144
00:53:22,332 --> 00:53:23,822
Disappeared?

145
00:58:28,705 --> 00:58:31,868
- Anyone home?
- Coming!

146
00:58:33,710 --> 00:58:35,234
What can I do for you?

147
00:58:35,311 --> 00:58:37,973
Tell the vicar
I need to talk to him.

148
00:58:58,401 --> 00:59:00,494
- Your blessing, Father.
- God bless you.

149
00:59:00,570 --> 00:59:04,006
Tell the doctor
I've come for my godson.

150
01:00:33,329 --> 01:00:35,229
What are you doing here,
Fabiano?

151
01:00:35,298 --> 01:00:36,890
Nothing,
he's getting better.

152
01:00:36,966 --> 01:00:39,298
Send him away.

153
01:02:01,851 --> 01:02:04,445
Get on.
I can walk.

154
01:03:20,530 --> 01:03:23,226
The captain pays well.
Going farther?

155
01:04:12,148 --> 01:04:14,173
St. Ilifonso, as I sew...

156
01:04:14,250 --> 01:04:15,911
make flesh and bones regrow.

157
01:04:15,985 --> 01:04:18,613
Hear my plea three times o'er!

158
01:05:04,734 --> 01:05:10,400
To hell with you!

159
01:05:30,993 --> 01:05:32,984
May God bless you.

160
01:05:44,674 --> 01:05:46,471
Mum, what's hell?

161
01:05:50,880 --> 01:05:52,745
It's a horrible place.

162
01:05:54,183 --> 01:05:55,844
How horrible?

163
01:06:13,836 --> 01:06:15,463
Put your foot here.

164
01:06:33,222 --> 01:06:34,348
Out!

165
01:06:45,267 --> 01:06:47,167
Father, what's hell?

166
01:07:03,619 --> 01:07:05,177
What?

167
01:07:05,254 --> 01:07:06,619
Hell.

168
01:07:10,693 --> 01:07:12,888
It's where the damned go.

169
01:07:12,962 --> 01:07:15,328
Fires and red-hot pokers.

170
01:07:17,967 --> 01:07:20,162
Have you been there?

171
01:08:09,051 --> 01:08:10,450
Baleia...

172
01:08:38,214 --> 01:08:39,738
Hell...

173
01:08:44,620 --> 01:08:46,986
Hot pokers...

174
01:08:51,694 --> 01:08:53,457
Hell...

175
01:09:00,503 --> 01:09:02,164
Horrible place...

176
01:09:07,143 --> 01:09:08,974
Hell...

177
01:09:14,683 --> 01:09:16,310
Horrible place...

178
01:09:20,523 --> 01:09:22,889
Horrible place...

179
01:09:27,329 --> 01:09:29,957
The damned...

180
01:09:36,172 --> 01:09:39,005
With hot pokers.

181
01:09:42,511 --> 01:09:45,378
With hot pokers.

182
01:09:47,716 --> 01:09:49,240
Hell...

183
01:10:21,750 --> 01:10:23,945
Miserable wretched life!

184
01:10:24,019 --> 01:10:26,419
Working like a dog!
And what for?

185
01:10:30,159 --> 01:10:31,854
Not even enough to eat.

186
01:10:41,170 --> 01:10:46,233
Imagine! You earn a pittance
and lose it all gambling!

187
01:10:48,377 --> 01:10:50,277
All I needed.

188
01:10:54,516 --> 01:10:56,677
It's all drying up.

189
01:11:36,992 --> 01:11:40,257
Devils... Hot pokers...

190
01:11:41,664 --> 01:11:44,098
I want to die
and get it over with.

191
01:13:54,561 --> 01:13:56,620
We'll never have a real bed.

192
01:14:02,235 --> 01:14:03,930
We were saving up.

193
01:14:06,539 --> 01:14:09,565
We'd bought the leather
and the wood.

194
01:14:13,780 --> 01:14:16,214
All you do is talk.

195
01:14:16,282 --> 01:14:20,844
I want to see something...
I want to see!

196
01:14:28,995 --> 01:14:30,895
There was money.

197
01:14:30,964 --> 01:14:32,591
Who works here?

198
01:14:34,567 --> 01:14:36,228
It is what it is.

199
01:14:41,374 --> 01:14:44,241
It all went to gambling
and drink.

200
01:14:45,545 --> 01:14:47,172
Cost less
than patent shoes.

201
01:14:47,247 --> 01:14:50,910
Why expensive shoes?
To walk like a parrot?

202
01:15:29,122 --> 01:15:30,680
A bad sign.

203
01:15:30,757 --> 01:15:32,918
The desert will catch fire.

204
01:15:32,992 --> 01:15:36,758
The sun sucks up the water
and that wretch takes the rest.

205
01:15:36,830 --> 01:15:38,730
They want to kill the cattle.

206
01:15:41,134 --> 01:15:43,125
They want to kill the cattle.

207
01:16:32,919 --> 01:16:35,854
You drink the water...

208
01:16:38,591 --> 01:16:41,321
And the cattle go thirsty
and die.

209
01:16:43,496 --> 01:16:45,623
You kill the cattle.

210
01:18:20,994 --> 01:18:25,658
It will catch fire.
No use waiting.

211
01:20:17,377 --> 01:20:18,935
Rustle up the stragglers.

212
01:20:27,954 --> 01:20:32,982
I'll get the rest tomorrow
and settle up.

213
01:20:34,494 --> 01:20:36,792
I want it all here
early tomorrow.

214
01:20:58,818 --> 01:21:01,981
Go and find pasture.

215
01:21:24,243 --> 01:21:26,609
We need to move tomorrow.

216
01:21:26,879 --> 01:21:30,542
We can't.
We have to get the cattle.

217
01:21:30,616 --> 01:21:34,347
We must move early
before the boss arrives.

218
01:21:34,420 --> 01:21:36,684
Go get the orange cow's calf.

219
01:21:38,191 --> 01:21:40,056
It could be a long trip.

220
01:21:46,933 --> 01:21:48,264
Baleia...

221
01:21:52,171 --> 01:21:53,866
Let's find the calf.

222
01:24:42,708 --> 01:24:44,608
How do we get to the road?

223
01:24:51,317 --> 01:24:52,284
Which way?

224
01:25:01,627 --> 01:25:03,822
The end of the trail,
to your right.

225
01:25:50,042 --> 01:25:53,569
Come on, boy.
We got to go.

226
01:26:40,926 --> 01:26:42,120
Baleia...

227
01:26:49,702 --> 01:26:50,964
Let's go, Baleia.

228
01:27:11,824 --> 01:27:13,291
Go play with Baleia.

229
01:28:01,874 --> 01:28:05,037
Stay still there.
Come here.

230
01:28:15,921 --> 01:28:17,616
He's going to kill Baleia!

231
01:28:33,005 --> 01:28:35,030
He's going to kill Baleia!

232
01:28:35,107 --> 01:28:37,837
Filthy varmint! Cry baby!

233
01:28:40,679 --> 01:28:43,705
She's sick.

234
01:28:43,782 --> 01:28:46,410
No use to anybody!

235
01:29:04,837 --> 01:29:05,997
Baleia...

236
01:33:57,763 --> 01:33:59,754
So where are we going?

237
01:34:19,318 --> 01:34:21,479
Will we live
like we did before?

238
01:34:22,054 --> 01:34:24,079
Maybe. Who knows?

239
01:34:26,625 --> 01:34:29,389
Maybe yes, maybe no.

240
01:34:30,028 --> 01:34:33,020
Maybe we'll find
somewhere even better.

241
01:34:33,966 --> 01:34:36,457
Why couldn't we be real people
one day?

242
01:34:37,336 --> 01:34:39,702
People who sleep
in leather beds.

243
01:34:42,608 --> 01:34:45,304
Why do we always
have to be wretched?

244
01:34:45,377 --> 01:34:47,937
Running in the wild
like animals.

245
01:34:48,013 --> 01:34:53,542
We could run like animals
like we've always done.

246
01:34:58,957 --> 01:35:01,221
We have to walk
a long way.

247
01:35:01,293 --> 01:35:03,761
But my sandals
are brand-new.

248
01:35:03,829 --> 01:35:06,992
We can walk,
can't we?

249
01:35:07,900 --> 01:35:09,060
Yes, we can.

250
01:35:09,134 --> 01:35:11,967
You're good for walking.
You're strong.

251
01:35:12,037 --> 01:35:17,532
You're well.
You can walk a long way.

252
01:35:18,310 --> 01:35:22,144
No matter.
I'll just lose weight.

253
01:35:22,214 --> 01:35:24,273
Down to the bone!

254
01:35:54,413 --> 01:35:56,040
What are you thinking about?

255
01:35:57,683 --> 01:35:59,082
Children don't think.

256
01:35:59,851 --> 01:36:03,343
Them? A bit more
and they'll put on weight.

257
01:36:03,789 --> 01:36:06,087
They'll be herding
the cattle.

258
01:36:06,158 --> 01:36:08,683
Hard to imagine!

259
01:36:09,394 --> 01:36:12,090
May Our Lady free them
from that misfortune.

260
01:36:13,765 --> 01:36:15,027
Cow-herding?

261
01:36:15,734 --> 01:36:19,226
There must be a place for us
in God's world.

262
01:36:20,172 --> 01:36:22,936
Even just a poor plot of land.

263
01:36:23,008 --> 01:36:25,340
As long as it gives
enough to eat all year.

264
01:36:25,844 --> 01:36:28,745
The Virgin will help us
change our lives.

265
01:36:29,715 --> 01:36:33,708
Nice land and plenty
of corn and beans.

266
01:36:33,785 --> 01:36:36,982
More than enough
to bring up children on.

267
01:36:37,656 --> 01:36:39,715
We'll have a new life.

268
01:36:39,791 --> 01:36:44,319
Without having to run after
cattle through scrub.

269
01:36:44,930 --> 01:36:48,923
And then,
living in a big city.

270
01:36:49,001 --> 01:36:51,765
So many things to see.

271
01:36:51,837 --> 01:36:53,828
These eyes
have only seen misery.

272
01:36:53,905 --> 01:36:58,069
The children will go to school
and learn.

273
01:36:58,143 --> 01:37:02,512
Until they can read a book
and do arithmetic,

274
01:37:02,581 --> 01:37:04,208
just like Master Tom�s.

275
01:37:04,282 --> 01:37:08,275
Big dreams.
Master Tom�s knew a lot.

276
01:37:08,353 --> 01:37:11,447
But when he started out
he never got there.

277
01:37:12,024 --> 01:37:14,891
Did reading help him?

278
01:37:15,560 --> 01:37:19,394
It didn't even get him
two leagues down the road.

279
01:37:19,464 --> 01:37:21,762
Who goes running
in the scrub?

280
01:37:21,833 --> 01:37:24,267
Hiding like an animal?

281
01:37:24,336 --> 01:37:26,964
One day,
we'll become real people.

282
01:37:27,039 --> 01:37:29,735
We can't go on
living like animals,

283
01:37:29,808 --> 01:37:31,332
hiding in the desert.

284
01:37:31,977 --> 01:37:33,069
Can we?

285
01:37:40,952 --> 01:37:42,544
No, we can't.

286
01:38:56,428 --> 01:39:00,228
AND THE DESERT WOULD GO ON
FORCING STRONG

287
01:39:00,298 --> 01:39:04,359
BUT SIMPLE FOLK LIKE
FABIANO, VIT�RIA,

288
01:39:04,436 --> 01:39:08,600
AND THE TWO CHILDREN
TO THE CITY.

