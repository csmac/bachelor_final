1
00:00:34,765 --> 00:00:37,800
Vincent Malloy is seven years old

2
00:00:37,801 --> 00:00:40,801
He�s always polite and does what he�s told

3
00:00:40,802 --> 00:00:44,830
For a boy his age, he�s considerate and nice

4
00:00:44,830 --> 00:00:50,554
But he wants to be just like Vincent Price

5
00:01:02,015 --> 00:01:05,500
He doesn�t mind living
with his sister, dog and cats

6
00:01:05,500 --> 00:01:09,950
Though he�d rather share a home
with spiders and bats

7
00:01:18,118 --> 00:01:23,118
There he could reflect on the horrors
he�s invented

8
00:01:25,181 --> 00:01:31,303
And wander dark hallways,
alone and tormented

9
00:01:38,196 --> 00:01:43,035
Vincent is nice
when his aunt comes to see him

10
00:01:43,035 --> 00:01:47,999
But imagines dipping her in wax
for his wax museum

11
00:01:58,086 --> 00:02:02,586
He likes to experiment
on his dog Abercrombie

12
00:02:02,586 --> 00:02:05,870
In the hopes of creating a horrible zombie

13
00:02:05,870 --> 00:02:08,415
So he and his horrible zombie dog

14
00:02:09,615 --> 00:02:16,144
Could go searching for victims
in the London fog

15
00:02:17,607 --> 00:02:20,951
His thoughts, though,
aren�t only of ghoulish crimes

16
00:02:20,952 --> 00:02:24,010
He likes to paint and read
to pass some of the times

17
00:02:24,013 --> 00:02:27,175
While other kids read books like
Go, Jane, Go!

18
00:02:27,176 --> 00:02:32,850
Vincent�s favourite author is Edgar Allen Poe

19
00:02:32,850 --> 00:02:35,591
One night, while reading a gruesome tale

20
00:02:35,591 --> 00:02:38,413
He read a passage that made him turn pale

21
00:02:38,413 --> 00:02:40,709
Such horrible news he could not survive

22
00:02:40,709 --> 00:02:45,004
For his beautiful wife had been buried alive!

23
00:02:48,250 --> 00:02:51,303
He dug out her grave
to make sure she was dead

24
00:02:51,303 --> 00:02:57,725
Unaware that her grave was his mother�s
flower bed

25
00:02:58,872 --> 00:03:01,364
His mother sent Vincent off to his room

26
00:03:01,364 --> 00:03:04,229
He knew he�d been banished
to the tower of doom

27
00:03:04,229 --> 00:03:08,000
Where he was sentenced to spend
the rest of his life

28
00:03:08,000 --> 00:03:11,930
Alone with the portrait of his beautiful wife

29
00:03:13,509 --> 00:03:16,984
While alone and insane encased in his tomb

30
00:03:16,984 --> 00:03:19,929
Vincent�s mother burst suddenly
into the room

31
00:03:19,929 --> 00:03:23,292
She said: �If you want to,
you can go out and play

32
00:03:23,292 --> 00:03:26,450
It�s sunny outside, and a beautiful day�

33
00:03:31,472 --> 00:03:35,260
Vincent tried to talk,
but he just couldn�t speak

34
00:03:35,260 --> 00:03:38,515
The years of isolation
had made him quite weak

35
00:03:40,600 --> 00:03:43,667
So he took out some paper
and scrawled with a pen:

36
00:03:43,667 --> 00:03:48,700
�I am possessed by this house,
and can never leave it again�

37
00:03:48,900 --> 00:03:53,500
His mother said: �You�re not possessed,
and you�re not almost dead

38
00:03:53,500 --> 00:03:56,162
These games that you play
are all in your head

39
00:03:56,162 --> 00:03:59,562
You�re not Vincent Price,
you�re Vincent Malloy

40
00:03:59,562 --> 00:04:03,325
You�re not tormented or insane,
you�re just a young boy

41
00:04:03,325 --> 00:04:05,878
You�re seven years old and you are my son

42
00:04:05,878 --> 00:04:09,700
I want you to get outside and
have some real fun.

43
00:04:09,700 --> 00:04:12,633
�Her anger now spent,
she walked out through the hall

44
00:04:12,633 --> 00:04:16,159
And while Vincent backed slowly
against the wall

45
00:04:16,159 --> 00:04:19,433
The room started to swell,
to shiver and creak

46
00:04:19,433 --> 00:04:22,709
His horrid insanity had reached its peak

47
00:04:23,731 --> 00:04:25,838
He saw Abercrombie, his zombie slave

48
00:04:25,838 --> 00:04:28,838
And heard his wife call
from beyond the grave

49
00:04:28,838 --> 00:04:32,300
She spoke from her coffin
and made ghoulish demands

50
00:04:32,300 --> 00:04:35,996
While, through cracking walls,
reached skeleton hands

51
00:04:39,343 --> 00:04:42,959
Every horror in his life
that had crept through his dreams

52
00:04:42,959 --> 00:04:47,421
Swept his mad laughter to terrified screams!

53
00:04:48,262 --> 00:04:50,861
To escape the madness,
he reached for the door

54
00:04:50,861 --> 00:04:56,595
But fell limp and lifeless down on the floor

55
00:04:56,595 --> 00:04:59,750
His voice was soft and very slow

56
00:04:59,750 --> 00:05:04,470
As he quoted The Raven
from Edgar Allen Poe:

57
00:05:04,500 --> 00:05:07,600
�and my soul from out that shadow

58
00:05:07,600 --> 00:05:11,300
that lies floating on the floor

59
00:05:11,500 --> 00:05:14,600
shall be lifted?

60
00:05:14,600 --> 00:05:17,600
Nevermore��

