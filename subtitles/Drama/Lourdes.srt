1
00:03:02,000 --> 00:03:03,200
Attention, please.

2
00:03:05,100 --> 00:03:06,900
Change of plan.

3
00:03:07,800 --> 00:03:11,400
We've put off our visit
to the grotto until tomorrow.

4
00:03:12,600 --> 00:03:14,000
I'm sorry.

5
00:03:15,000 --> 00:03:17,200
Our journey was rather tiring,

6
00:03:17,600 --> 00:03:20,200
we'd all been
looking forward to this,

7
00:03:20,900 --> 00:03:22,500
but it's too late now.

8
00:03:22,700 --> 00:03:24,200
So tomorrow,

9
00:03:24,400 --> 00:03:28,100
our schedule
will be even busier.

10
00:03:29,600 --> 00:03:33,100
This is why I ask you all
to make an effort,

11
00:03:33,300 --> 00:03:35,800
so that we can get everything done,

12
00:03:36,200 --> 00:03:38,900
in a cheerful manner
and, more importantly,

13
00:03:39,100 --> 00:03:41,700
so that we can open up
to one another.

14
00:03:43,700 --> 00:03:46,900
Each one of us should make the most
of this opportunity.

15
00:03:47,200 --> 00:03:49,700
We, the helpers,

16
00:03:50,500 --> 00:03:53,000
but of course you, the pilgrims.

17
00:03:53,400 --> 00:03:55,700
You came here to help

18
00:03:55,900 --> 00:03:58,100
foremost those of us who are sick,

19
00:03:58,500 --> 00:04:00,400
those who are in need.

20
00:04:01,500 --> 00:04:03,600
They shall feel better,

21
00:04:03,800 --> 00:04:06,400
even if only for a few days.

22
00:04:07,100 --> 00:04:09,800
They shall forget
their loneliness

23
00:04:10,100 --> 00:04:12,700
and find some happiness
and relief here.

24
00:04:15,200 --> 00:04:17,400
At the end of our stay,

25
00:04:17,700 --> 00:04:21,100
we'll award a prize
for the best pilgrim,

26
00:04:21,800 --> 00:04:24,400
as a reward for their good deeds.

27
00:04:25,900 --> 00:04:29,800
Information
about Lourdes and the apparitions

28
00:04:30,200 --> 00:04:32,500
can be found in this booklet.

29
00:05:09,800 --> 00:05:11,300
Are you enjoying it?

30
00:05:11,600 --> 00:05:12,500
Yes.

31
00:05:18,000 --> 00:05:20,800
It's a bit touristy,

32
00:05:21,400 --> 00:05:23,500
but that's how every pilgrimage is.

33
00:05:24,100 --> 00:05:26,000
Have you been on many?

34
00:05:26,300 --> 00:05:28,300
Yes. It's the only way I get out.

35
00:05:29,500 --> 00:05:32,000
Travelling isn't easy
in a wheelchair.

36
00:05:55,300 --> 00:05:56,800
You take the legs.

37
00:06:01,500 --> 00:06:03,700
One, two, three...

38
00:06:22,900 --> 00:06:24,200
Look here.

39
00:06:34,600 --> 00:06:36,500
The wheelchair, please.

40
00:06:47,300 --> 00:06:48,300
All right?

41
00:06:48,500 --> 00:06:49,900
A bit higher.

42
00:06:51,800 --> 00:06:53,300
That's fine, thanks.

43
00:07:05,700 --> 00:07:07,900
Hail Mary, full of grace.

44
00:07:08,200 --> 00:07:09,800
The Lord is with thee.

45
00:07:10,000 --> 00:07:12,100
Blessed art thou among women,

46
00:07:12,300 --> 00:07:15,100
and blessed is the fruit
of thy womb, Jesus.

47
00:07:15,400 --> 00:07:17,700
Holy Mary, Mother of God,

48
00:07:18,000 --> 00:07:19,900
pray for us sinners,

49
00:07:20,200 --> 00:07:23,200
now and at the hour of our death.
Amen.

50
00:07:35,300 --> 00:07:36,300
Goodnight.

51
00:08:09,200 --> 00:08:10,500
Do you like her?

52
00:08:14,600 --> 00:08:16,200
Beautiful, isn't she?

53
00:08:22,300 --> 00:08:24,500
The Virgin Mary is watching us.

54
00:09:19,800 --> 00:09:21,800
Don't forget your raincoats.

55
00:10:39,900 --> 00:10:42,200
Holy Mary, Mother of God,

56
00:10:42,400 --> 00:10:44,600
pray for us sinners,

57
00:10:44,800 --> 00:10:48,200
now and at the hour of our death.
Amen.

58
00:10:49,100 --> 00:10:52,900
Hail Mary, full of grace.
The Lord is with thee.

59
00:10:53,300 --> 00:10:55,600
Blessed art thou among women,

60
00:10:55,800 --> 00:10:58,500
and he whom John baptised
in the Jordan,

61
00:10:58,700 --> 00:11:00,700
your child Jesus, is blessed.

62
00:12:11,300 --> 00:12:12,100
Thank you.

63
00:12:37,600 --> 00:12:40,500
Well, are you pleased
to be going in?

64
00:12:41,400 --> 00:12:42,600
I guess so.

65
00:12:46,000 --> 00:12:49,600
They say that the last healing
occurred in the baths.

66
00:12:49,900 --> 00:12:50,900
What happened exactly?

67
00:12:51,200 --> 00:12:55,300
There was a man who'd suffered
from multiple sclerosis for years.

68
00:12:55,700 --> 00:12:58,900
Apparently, it was in the baths
that he suddenly recovered

69
00:12:59,200 --> 00:13:01,100
the use of his arms and legs.

70
00:13:02,700 --> 00:13:04,600
That's how he was cured.

71
00:13:05,300 --> 00:13:06,100
Yes.

72
00:13:06,300 --> 00:13:08,500
I'd heard about that too.

73
00:13:19,900 --> 00:13:23,700
Sorry to butt in, but it didn't
happen exactly as you said.

74
00:13:23,900 --> 00:13:28,000
Firstly, it happened
at the blessing, not in the baths.

75
00:13:28,400 --> 00:13:31,400
And secondly,
his paralysis came back later.

76
00:13:31,700 --> 00:13:33,000
The poor thing!

77
00:13:33,400 --> 00:13:37,600
That's why it wasn't
officially recognised as a miracle.

78
00:13:39,100 --> 00:13:41,400
He did walk again, though?

79
00:13:41,600 --> 00:13:44,600
Yes, but it didn't last.
So it doesn't count.

80
00:13:45,300 --> 00:13:47,200
They're very strict about that.

81
00:13:47,800 --> 00:13:48,800
Next!

82
00:13:49,100 --> 00:13:51,000
What if the healing does last?

83
00:14:44,500 --> 00:14:47,600
It'll be all right.
It'll do you good.

84
00:14:48,400 --> 00:14:49,700
You'll see.

85
00:15:34,400 --> 00:15:36,000
You're next.

86
00:15:44,800 --> 00:15:47,900
Our Lady of Lourdes,
pray for us.

87
00:15:48,400 --> 00:15:49,400
Saint Bernadette,

88
00:15:50,100 --> 00:15:51,500
pray for us.

89
00:15:51,800 --> 00:15:55,200
O Mary, conceived without sin,
pray for us.

90
00:16:30,500 --> 00:16:31,900
Lord, I have come here

91
00:16:32,200 --> 00:16:35,700
for a little comfort
and human warmth.

92
00:16:36,000 --> 00:16:37,700
Usually, I am alone.

93
00:16:39,000 --> 00:16:42,900
I pray that I receive
some care and attention here.

94
00:16:43,600 --> 00:16:46,800
Lord, hear our voices
raised in prayer.

95
00:16:48,700 --> 00:16:52,900
Lord, my fiancée left me
after my motorbike accident.

96
00:16:54,100 --> 00:16:56,300
Let me find a new fiancée

97
00:16:56,600 --> 00:16:59,100
who's better able
to handle my disability.

98
00:16:59,700 --> 00:17:02,700
Lord, hear our voices
raised in prayer.

99
00:17:07,300 --> 00:17:08,400
Lord,

100
00:17:09,300 --> 00:17:11,800
I sometimes feel that
my life is passing me by.

101
00:17:12,900 --> 00:17:15,400
I feel useless.

102
00:17:17,300 --> 00:17:19,400
Please help me.

103
00:17:20,500 --> 00:17:23,200
Lord, hear our voices
raised in prayer.

104
00:17:23,500 --> 00:17:25,100
Hear our voices.

105
00:17:28,300 --> 00:17:33,100
Lord, stay with us

106
00:17:34,400 --> 00:17:40,300
For it is toward evening

107
00:18:03,300 --> 00:18:06,200
We're going for a drink later.
Want to come?

108
00:18:07,500 --> 00:18:08,500
Maybe.

109
00:18:09,800 --> 00:18:12,100
I bet Kuno will come too!

110
00:18:13,200 --> 00:18:14,300
We'll see.

111
00:18:16,500 --> 00:18:18,400
Well, are you enjoying yourselves?

112
00:18:18,900 --> 00:18:19,700
Yes.

113
00:18:21,600 --> 00:18:23,400
We met on the trip to Rome...

114
00:18:24,600 --> 00:18:25,100
didn't we?

115
00:18:27,600 --> 00:18:29,000
Yes, of course.

116
00:18:32,300 --> 00:18:34,300
Well, enjoy yourself.

117
00:18:34,800 --> 00:18:36,100
Thanks.

118
00:18:36,200 --> 00:18:37,500
In fact,

119
00:18:38,400 --> 00:18:40,600
I rather prefer the cultural trips.

120
00:18:43,500 --> 00:18:44,900
Everywhere is different.

121
00:18:45,900 --> 00:18:47,100
Yes.

122
00:18:54,800 --> 00:18:56,700
Do you want some Lourdes water?

123
00:18:57,200 --> 00:18:58,500
No, thanks.

124
00:19:08,200 --> 00:19:09,600
Excuse me, father.

125
00:19:10,700 --> 00:19:13,400
- May I ask you something?
- Of course.

126
00:19:14,000 --> 00:19:17,500
I've heard that one can be healed
even physically here.

127
00:19:18,200 --> 00:19:19,500
It does happen...

128
00:19:21,300 --> 00:19:23,100
Miraculous healing.

129
00:19:23,300 --> 00:19:24,600
Yes.

130
00:19:25,100 --> 00:19:27,500
I want to ask you...
What do I have to do?

131
00:19:31,400 --> 00:19:34,700
I believe, of course,
that God can perform miracles.

132
00:19:36,500 --> 00:19:40,100
But only if we open our hearts
fully to His grace.

133
00:19:41,300 --> 00:19:42,900
We must say, "Yes, " to Him.

134
00:19:43,800 --> 00:19:46,000
And what exactly must we do?

135
00:19:49,100 --> 00:19:51,500
Firstly our souls must be healed.

136
00:19:53,000 --> 00:19:55,400
Only then can the body be healed.

137
00:19:57,200 --> 00:19:59,200
I felt a bit lost at first.

138
00:20:00,500 --> 00:20:02,500
But now I enjoy the volunteer work.

139
00:20:08,800 --> 00:20:10,900
And it's for a change.

140
00:20:11,500 --> 00:20:14,100
Usually I go skiing
during the holidays.

141
00:20:15,300 --> 00:20:18,900
But sometimes I miss
the meaning behind it.

142
00:20:20,000 --> 00:20:22,600
Contrary to charity work.

143
00:20:24,000 --> 00:20:26,500
It's important to me
to have a goal,

144
00:20:26,800 --> 00:20:28,300
or meaning in my life.

145
00:21:10,600 --> 00:21:11,800
Are you all right?

146
00:21:16,200 --> 00:21:17,700
What's happened?

147
00:21:25,400 --> 00:21:28,200
- What's the matter with her?
- I don't know.

148
00:21:36,400 --> 00:21:37,800
It's upsetting.

149
00:21:38,500 --> 00:21:41,000
The tide buries us all in the end.

150
00:21:45,600 --> 00:21:47,900
You gave us quite a scare.

151
00:21:49,400 --> 00:21:50,900
I'll handle this.

152
00:22:08,300 --> 00:22:09,700
Sorry.

153
00:22:10,100 --> 00:22:11,700
Don't worry about it.

154
00:23:12,200 --> 00:23:14,400
That's fine. Thanks.

155
00:23:18,200 --> 00:23:19,900
And the prayer?

156
00:23:20,200 --> 00:23:21,600
Sorry.

157
00:23:28,200 --> 00:23:32,200
Hail Mary, full of grace.
The Lord is with thee.

158
00:23:32,800 --> 00:23:34,600
Blessed art thou among women,

159
00:23:34,900 --> 00:23:37,500
and blessed is the fruit
of thy womb, Jesus.

160
00:23:37,800 --> 00:23:39,800
Holy Mary, Mother of God,

161
00:23:40,100 --> 00:23:42,100
pray for us sinners,

162
00:23:42,400 --> 00:23:45,600
now and at the hour of our death.
Amen.

163
00:24:28,300 --> 00:24:29,800
Oh, sorry.

164
00:24:55,900 --> 00:24:57,700
Not behind the ear.

165
00:24:59,100 --> 00:25:00,200
There.

166
00:25:03,900 --> 00:25:06,200
- Do you like it?
- And the earrings?

167
00:25:06,500 --> 00:25:07,500
Yes.

168
00:25:24,600 --> 00:25:26,500
Did you have fun last night?

169
00:25:33,100 --> 00:25:34,400
Good morning.

170
00:25:39,000 --> 00:25:40,700
Careful, it's hot.

171
00:25:41,500 --> 00:25:44,500
- Good morning. Are you well?
- Yes, thanks.

172
00:25:49,600 --> 00:25:52,200
Is everything
all right with you too?

173
00:25:52,500 --> 00:25:53,900
Do you like it here?

174
00:25:55,400 --> 00:25:57,100
I had a strange dream.

175
00:25:59,200 --> 00:26:01,400
I dreamt that I was paralysed

176
00:26:02,200 --> 00:26:04,400
and that the Virgin Mary
appeared to me.

177
00:26:05,200 --> 00:26:06,900
She said something,

178
00:26:07,400 --> 00:26:10,700
but I didn't understand very well
what she said.

179
00:26:11,700 --> 00:26:15,100
So I got up and moved closer.

180
00:26:17,000 --> 00:26:20,600
And it was then that I realised
I was no longer paralysed.

181
00:26:21,500 --> 00:26:23,500
It's not easy, I know.

182
00:26:23,800 --> 00:26:27,800
But we must learn
to accept our fate with humility.

183
00:26:28,800 --> 00:26:31,100
We pray for the healing of the soul,

184
00:26:31,400 --> 00:26:33,100
not that of the body.

185
00:26:34,300 --> 00:26:37,800
And the suffering you bear
can have a deep meaning.

186
00:26:38,400 --> 00:26:40,100
Paul said,

187
00:26:40,400 --> 00:26:43,500
"I rejoice in my suffering
for your sake,

188
00:26:44,200 --> 00:26:48,900
"and in my flesh I complete what
is lacking in Christ's afflictions

189
00:26:49,700 --> 00:26:52,500
"for the sake of His body,
that is, the Church. "

190
00:26:59,100 --> 00:27:03,000
If I told you
what I dreamt about last night!

191
00:27:23,100 --> 00:27:24,400
Do you think he's married?

192
00:27:24,700 --> 00:27:26,600
Who cares?

193
00:27:27,000 --> 00:27:29,500
During the blessing
there have been healings, too!

194
00:27:29,800 --> 00:27:31,000
You're going too far now!

195
00:27:31,200 --> 00:27:34,700
Not at all! When the priest passes
with the sanctum

196
00:27:35,000 --> 00:27:37,200
and he stops to bless them,

197
00:27:37,400 --> 00:27:40,300
sometimes people are healed.

198
00:27:40,700 --> 00:27:43,200
Don't believe all you're told.

199
00:27:43,400 --> 00:27:44,500
We'll see.

200
00:33:16,100 --> 00:33:18,400
Here she is, the eager one!

201
00:33:18,600 --> 00:33:21,400
But it won't do you any good.

202
00:33:22,200 --> 00:33:24,400
Do you really think
God would heal someone

203
00:33:24,700 --> 00:33:27,300
just because they pushed
in front of the others?

204
00:33:27,600 --> 00:33:31,400
In future, please don't move away
from the rest of the group.

205
00:33:34,600 --> 00:33:36,800
- Was it fun last night?
- She doesn't remember!

206
00:33:37,200 --> 00:33:38,600
Stop it!

207
00:33:38,800 --> 00:33:42,200
- The girls have got to you.
- Of course they have!

208
00:33:43,800 --> 00:33:45,300
- Shall we go?
- Yes.

209
00:33:47,400 --> 00:33:49,300
- Goodbye.
- Your hat.

210
00:33:54,500 --> 00:33:58,300
I suggest you concentrate more
on your duties.

211
00:33:58,600 --> 00:34:01,000
We're not here to have fun.

212
00:34:05,100 --> 00:34:06,800
Where have you been?

213
00:34:08,600 --> 00:34:11,300
Are you glad to be here?
It was lovely, wasn't it?

214
00:34:12,000 --> 00:34:14,800
The blessing was lovely,
don't you think?

215
00:34:15,700 --> 00:34:17,700
Yes, it was fine. Thanks.

216
00:34:23,000 --> 00:34:24,700
Are you feeling all right?

217
00:34:25,000 --> 00:34:25,900
Yes.

218
00:34:27,400 --> 00:34:29,400
You're all pale.

219
00:34:30,100 --> 00:34:31,700
I'm always pale.

220
00:35:41,800 --> 00:35:43,600
Wait! I'll give you a hand.

221
00:35:45,900 --> 00:35:48,600
- Thank you.
- That's what I'm here for.

222
00:36:01,700 --> 00:36:03,700
- It's really nice of you.
- No.

223
00:36:08,400 --> 00:36:10,500
I have to atone for my sins.

224
00:36:18,700 --> 00:36:22,400
Let us pray to the Lord
not to despair in the face of death.

225
00:36:22,900 --> 00:36:26,400
May we see the grave
for what it truly is:

226
00:36:27,600 --> 00:36:29,600
an in-between place,

227
00:36:30,100 --> 00:36:32,500
where we leave our mortal bodies.

228
00:36:33,800 --> 00:36:37,300
We shall now move on
to the Station of the Resurrection,

229
00:36:38,100 --> 00:36:40,500
for this is the truth
of Christianity:

230
00:36:41,300 --> 00:36:42,800
eternal life.

231
00:37:42,100 --> 00:37:44,600
Everyone was praying devotedly,

232
00:37:44,900 --> 00:37:47,800
repeating the words
spoken by the priest.

233
00:37:48,400 --> 00:37:51,000
I was exhausted
and feIt unweIl,

234
00:37:51,200 --> 00:37:53,000
so I couldn't pray any more.

235
00:37:53,300 --> 00:37:57,100
I concentrated instead
on a single thought:

236
00:37:57,900 --> 00:38:00,500
"Lord, may Thy will be done.

237
00:38:00,700 --> 00:38:02,700
"Virgin Mary, pray for us. "

238
00:38:04,200 --> 00:38:05,500
Suddenly,

239
00:38:05,700 --> 00:38:09,400
I felt a bolt flash through me
from my head to my feet.

240
00:38:09,700 --> 00:38:11,400
Like an electric shock.

241
00:38:11,800 --> 00:38:13,600
I thought it was the end.

242
00:38:15,100 --> 00:38:19,400
But no, I was kneeling
in front of my wheelchair,

243
00:38:19,600 --> 00:38:21,800
upright, with my hands together.

244
00:38:23,300 --> 00:38:26,700
I don't know what happened

245
00:38:27,300 --> 00:38:29,500
but I knew that I was cured.

246
00:38:29,800 --> 00:38:31,600
The pain was all gone.

247
00:38:32,500 --> 00:38:35,700
And my limbs, which had been
paralysed and flaccid,

248
00:38:36,500 --> 00:38:38,800
were full of renewed strength.

249
00:39:18,100 --> 00:39:20,600
- Did you notice anything?
- What?

250
00:39:21,000 --> 00:39:24,300
The man who'd had the miracle,
he was sitting down.

251
00:39:24,800 --> 00:39:26,000
Yes, so?

252
00:39:26,400 --> 00:39:28,600
We didn't see him get up once.

253
00:39:29,300 --> 00:39:31,400
Doesn't that make you think?

254
00:40:44,400 --> 00:40:46,500
Don't you think we might get lost?

255
00:40:52,400 --> 00:40:53,600
The Holy Ghost, Jesus

256
00:40:53,900 --> 00:40:56,100
and the Virgin Mary are on a cloud,

257
00:40:56,400 --> 00:40:58,400
discussing their holiday plans.

258
00:40:58,600 --> 00:41:01,300
The Holy Ghost says,
"Let's go to Bethlehem. "

259
00:41:01,600 --> 00:41:05,200
Jesus says, "Bethlehem?
No, we've been there lots of times. "

260
00:41:05,500 --> 00:41:09,200
The Holy Ghost thinks and says,
"Then how about Jerusalem?"

261
00:41:10,700 --> 00:41:14,700
Jesus says, "Jerusalem?
No, we've been there lots of times. "

262
00:41:14,900 --> 00:41:17,500
The Holy Ghost thinks and says,
"I've got it!

263
00:41:18,100 --> 00:41:19,600
"Let's go to Lourdes. "

264
00:41:20,000 --> 00:41:22,500
The Virgin Mary says, "Yeah, great!

265
00:41:22,700 --> 00:41:24,600
"I've never been there before!"

266
00:43:09,100 --> 00:43:11,900
This morning you have free time.

267
00:43:12,100 --> 00:43:16,000
You can choose between confession
and going to the baths.

268
00:43:16,300 --> 00:43:18,300
Benefit from this opportunity.

269
00:43:18,500 --> 00:43:20,100
We'll meet at two o'clock

270
00:43:20,400 --> 00:43:23,000
by the Basilica of the Rosary,
for the photograph.

271
00:43:23,200 --> 00:43:24,100
Be on time.

272
00:43:36,400 --> 00:43:38,900
- The napkin.
- Oh, yes. Sorry.

273
00:43:40,800 --> 00:43:43,900
I dreamt about you last night.

274
00:43:44,400 --> 00:43:46,400
The Virgin Mary appeared to you

275
00:43:46,500 --> 00:43:48,300
and said something to you.

276
00:43:49,200 --> 00:43:50,300
What did she say?

277
00:43:50,500 --> 00:43:53,300
"Rise, take up your bed and walk. "

278
00:44:20,000 --> 00:44:22,100
So, is Lourdes better than Rome?

279
00:44:24,100 --> 00:44:26,500
Let's just say
that Rome is somehow...

280
00:44:26,800 --> 00:44:28,300
more cultural.

281
00:44:29,300 --> 00:44:32,100
You're right. I prefer Rome too.

282
00:44:35,200 --> 00:44:37,500
Do you prefer confession
or baths?

283
00:44:37,800 --> 00:44:39,700
We could do both.

284
00:44:44,300 --> 00:44:45,800
Our time is limited.

285
00:44:47,100 --> 00:44:49,400
Do you mind
if I don't come with you?

286
00:44:58,300 --> 00:45:00,300
You can't just leave her!

287
00:45:01,000 --> 00:45:02,500
I'll be back!

288
00:45:26,300 --> 00:45:28,100
I am often angry.

289
00:45:32,600 --> 00:45:35,500
Why did I fall ill
and not someone else?

290
00:45:37,700 --> 00:45:39,100
Why me?

291
00:45:42,500 --> 00:45:44,300
Sometimes I envy the others,

292
00:45:45,400 --> 00:45:49,000
those who can walk
and do everything normally,

293
00:45:49,900 --> 00:45:51,500
without thinking.

294
00:45:55,500 --> 00:45:58,000
There are some worse off than me,

295
00:45:58,300 --> 00:46:00,700
but I don't feel any pity for them.

296
00:46:03,100 --> 00:46:05,000
I'd like to be healthy too

297
00:46:05,600 --> 00:46:07,300
and have a normal life.

298
00:46:14,600 --> 00:46:16,000
What is that?

299
00:46:18,800 --> 00:46:20,300
A "normal life"?

300
00:46:24,100 --> 00:46:26,200
Your life is unique.

301
00:46:27,200 --> 00:46:29,200
Each life is unique.

302
00:46:31,000 --> 00:46:33,300
God created this diversity.

303
00:46:34,400 --> 00:46:37,900
Each life is different,
none is better than another.

304
00:46:39,600 --> 00:46:43,400
Or do you think that someone
who can use his legs

305
00:46:44,500 --> 00:46:46,300
is necessarily happier?

306
00:46:50,800 --> 00:46:52,400
Let us pray together.

307
00:46:54,400 --> 00:46:56,500
Lord, we pray to You.

308
00:46:56,800 --> 00:46:58,700
Heal this young woman.

309
00:47:00,500 --> 00:47:01,900
Heal her soul.

310
00:47:03,400 --> 00:47:05,100
And, if You wish,

311
00:47:06,100 --> 00:47:08,000
heal her body too.

312
00:47:22,700 --> 00:47:24,100
Excuse me.

313
00:47:25,200 --> 00:47:26,200
Sorry.

314
00:47:27,800 --> 00:47:30,200
- Lucky!
- That was close.

315
00:47:30,500 --> 00:47:33,500
I hope that this time
I'll get rid of my eczema.

316
00:48:15,000 --> 00:48:17,800
- Our Lady of Lourdes.
- Pray for us.

317
00:48:18,400 --> 00:48:20,900
- Saint Bernadette.
- Pray for us.

318
00:48:21,400 --> 00:48:23,900
O Mary of the immaculate
conception.

319
00:48:24,200 --> 00:48:26,700
Pray for us, poor sinners.

320
00:49:47,600 --> 00:49:49,100
Anna?

321
00:50:03,600 --> 00:50:04,800
Anna.

322
00:50:09,600 --> 00:50:11,200
A miracle?

323
00:50:11,900 --> 00:50:12,800
Anna!

324
00:50:13,200 --> 00:50:15,200
- What's going on?
- No idea.

325
00:50:18,200 --> 00:50:19,800
It's a miracle.

326
00:50:51,000 --> 00:50:52,300
Mum.

327
00:51:13,400 --> 00:51:15,000
Excuse me.

328
00:51:15,900 --> 00:51:17,000
Sorry.

329
00:51:20,300 --> 00:51:22,400
Hold it, ladies and gentlemen.

330
00:51:22,600 --> 00:51:24,600
On three, give me a smile, please.

331
00:51:25,300 --> 00:51:27,500
One, two, three...

332
00:51:30,100 --> 00:51:31,900
Thank you. Have a nice day.

333
00:51:35,600 --> 00:51:36,900
Let's go.

334
00:52:12,200 --> 00:52:14,400
... heal also your servant
from the infirmity

335
00:52:14,700 --> 00:52:17,600
of body and spirit
that afflicts him. Amen.

336
00:52:28,300 --> 00:52:30,200
Holy Father,
doctor of souls and bodies,

337
00:52:30,500 --> 00:52:33,000
you who sent
your only begotten son Jesus Christ

338
00:52:33,400 --> 00:52:36,500
to cure every disease
and to free us from death,

339
00:52:36,800 --> 00:52:39,900
heal also your servant from
the infirmity of his body and soul

340
00:52:40,200 --> 00:52:42,000
that afflicts him. Amen.

341
00:54:36,700 --> 00:54:38,600
Holy Father,
doctor of souls and bodies,

342
00:54:38,900 --> 00:54:41,900
you who sent your son Jesus Christ
to cure every disease

343
00:54:42,100 --> 00:54:43,600
and to free us from death,

344
00:54:43,900 --> 00:54:47,000
heal also your servant from
the infirmity of body and spirit

345
00:54:47,200 --> 00:54:49,500
that afflicts him. Amen.

346
00:55:13,300 --> 00:55:14,800
For our last day,

347
00:55:15,000 --> 00:55:18,200
there will be
an excursion into the mountains.

348
00:55:19,000 --> 00:55:21,000
We shall go up the Pic du Jer.

349
00:55:21,600 --> 00:55:22,500
I'm sorry,

350
00:55:22,700 --> 00:55:25,900
but only the most able-bodied of you
will be able to come.

351
00:55:26,200 --> 00:55:28,500
Those in wheelchairs must stay here.

352
00:55:29,100 --> 00:55:32,700
Our closing party, on the other hand,
is open to everyone.

353
00:55:38,700 --> 00:55:40,500
Are you allowed?

354
00:55:40,800 --> 00:55:42,100
Yes.

355
00:55:43,200 --> 00:55:44,800
It's whipped cream.

356
00:56:00,400 --> 00:56:01,800
- More?
- Yes.

357
00:56:09,500 --> 00:56:12,600
- You look so healthy today.
- Thank you.

358
00:56:14,300 --> 00:56:15,800
Enjoy.

359
00:56:17,400 --> 00:56:19,300
I'd like a little more.

360
00:56:34,600 --> 00:56:36,900
Sorry to bother you...

361
00:56:37,500 --> 00:56:40,900
Could you help me
prepare things for the party?

362
00:56:41,500 --> 00:56:43,600
Of course.
What would you like to do?

363
00:56:43,900 --> 00:56:46,800
I'd like to put a paper chain
over there,

364
00:56:47,300 --> 00:56:50,500
and maybe over there too.

365
00:56:50,900 --> 00:56:53,200
And a few balloons.

366
00:56:53,600 --> 00:56:55,200
Fine. When?

367
00:56:55,600 --> 00:56:57,200
After the meal.

368
00:56:57,600 --> 00:57:00,300
I'll finish my pudding
and be right with you.

369
00:57:00,600 --> 00:57:01,500
Thank you.

370
00:57:03,400 --> 00:57:04,600
Father,

371
00:57:05,200 --> 00:57:07,300
I have something to ask you.

372
00:57:07,800 --> 00:57:10,500
Is God good or is He all-powerful?

373
00:57:13,400 --> 00:57:17,600
If He's all-powerful and good,
He could heal everyone, couldn't He?

374
00:57:19,900 --> 00:57:21,100
He does.

375
00:57:23,700 --> 00:57:27,600
But for some it's more discreet.
It's on the inside, you see?

376
00:57:29,200 --> 00:57:31,500
Take a person in despair,
for example,

377
00:57:31,700 --> 00:57:35,500
who suddenly, through God's grace,
finds a meaning to life.

378
00:57:36,400 --> 00:57:38,600
That too is a miracle.

379
00:57:38,800 --> 00:57:40,400
If you like.

380
00:57:45,100 --> 00:57:47,000
- A bit lower.
- Sorry.

381
00:57:48,800 --> 00:57:50,100
Thank you.

382
00:58:14,500 --> 00:58:15,900
Cécile...

383
00:58:19,300 --> 00:58:21,000
Can I help you?

384
01:02:37,900 --> 01:02:39,400
It's hot!

385
01:02:50,700 --> 01:02:53,400
Could you, just a little... ?

386
01:03:21,400 --> 01:03:23,200
Congratulations.

387
01:03:26,600 --> 01:03:28,100
From me too.

388
01:03:32,200 --> 01:03:34,500
Have you been to the medical office?

389
01:03:35,700 --> 01:03:36,700
No.

390
01:03:36,800 --> 01:03:40,200
You ought to, or it won't
be officially acknowledged.

391
01:03:41,300 --> 01:03:45,100
How sad that Miss Cécile
can't see this.

392
01:03:45,500 --> 01:03:47,700
We must give thanks
to the Virgin Mary.

393
01:03:48,000 --> 01:03:50,100
And pray that it lasts.

394
01:05:21,200 --> 01:05:23,300
She's the one it happened to.

395
01:05:24,100 --> 01:05:25,300
It's a miracle!

396
01:05:29,100 --> 01:05:31,900
We're here
for a very important reason.

397
01:05:32,200 --> 01:05:35,400
We have reason to believe
that this young woman,

398
01:05:35,700 --> 01:05:39,300
contrary to all expectations,
has been cured. But we are not sure.

399
01:05:40,000 --> 01:05:43,000
What should we do in such a case?

400
01:05:45,700 --> 01:05:47,700
You're not the only ones today.

401
01:06:13,100 --> 01:06:15,000
Next, please.

402
01:06:42,700 --> 01:06:44,700
This patient's ailment

403
01:06:45,000 --> 01:06:47,700
goes through phases
of intensification and remission.

404
01:06:48,100 --> 01:06:49,700
Multiple sclerosis
has its ups and downs,

405
01:06:49,900 --> 01:06:52,800
and this would seem
to be a temporary improvement

406
01:06:53,200 --> 01:06:54,800
of her overall state,

407
01:06:55,500 --> 01:06:56,900
which will not last.

408
01:07:09,100 --> 01:07:11,000
Will you stand, please?

409
01:07:15,600 --> 01:07:17,700
Carry on, carry on...

410
01:07:32,200 --> 01:07:33,400
Come back.

411
01:07:57,600 --> 01:07:58,800
Very good.

412
01:08:04,900 --> 01:08:06,500
Be careful.

413
01:08:07,200 --> 01:08:09,100
You should get some rest.

414
01:08:09,400 --> 01:08:12,200
Your body needs to readapt itself.

415
01:08:12,900 --> 01:08:15,800
Above all else,
get plenty of fresh air.

416
01:08:18,300 --> 01:08:20,000
Now what do we do?

417
01:08:20,700 --> 01:08:24,500
This case will have to go
before a medical committee.

418
01:08:26,600 --> 01:08:30,200
Such an improvement
is most uncommon.

419
01:08:32,200 --> 01:08:34,200
It's extraordinary.

420
01:08:38,300 --> 01:08:41,100
Goodbye and good luck.

421
01:08:42,300 --> 01:08:43,800
Thank you.

422
01:11:44,200 --> 01:11:47,100
In fact, it happened gradually.

423
01:11:48,600 --> 01:11:51,800
I could feel something in my hands
after the baths.

424
01:11:52,400 --> 01:11:54,300
Then, in the grotto,

425
01:11:54,500 --> 01:11:56,400
I could lift my hand
to touch the rocks,

426
01:11:56,600 --> 01:11:59,000
whereas I couldn't even lift
a finger before.

427
01:11:59,300 --> 01:12:02,100
After the baths?
That's interesting.

428
01:12:06,600 --> 01:12:08,300
Continue.

429
01:12:09,600 --> 01:12:13,700
During the night I heard
what sounded like a voice,

430
01:12:14,100 --> 01:12:16,100
telling me to get up.

431
01:12:16,600 --> 01:12:20,000
So I got up and went to the toilet,
as natural as can be.

432
01:12:20,700 --> 01:12:23,200
As if I suddenly remembered
what to do.

433
01:12:23,800 --> 01:12:26,500
Like looking for your glasses
when you're wearing them!

434
01:12:26,900 --> 01:12:29,200
But what about inside yourself?

435
01:12:29,500 --> 01:12:32,200
Do you feel a sort of illumination?

436
01:12:34,400 --> 01:12:35,600
Not really.

437
01:12:39,600 --> 01:12:41,900
Do you think that makes
a difference?

438
01:12:43,700 --> 01:12:45,200
It depends.

439
01:12:46,900 --> 01:12:47,900
On what?

440
01:12:48,300 --> 01:12:50,500
Let's say that it depends on the way

441
01:12:51,400 --> 01:12:53,500
you internalise this healing.

442
01:12:54,400 --> 01:12:57,800
One may wonder whether
it will reinforce your faith

443
01:12:58,300 --> 01:13:01,200
and make your attitude an example

444
01:13:01,600 --> 01:13:03,400
for all Christians.

445
01:13:12,700 --> 01:13:16,400
In view of the circumstances,
some of you have requested

446
01:13:17,500 --> 01:13:19,300
that we retake the group photo,

447
01:13:19,600 --> 01:13:24,100
as a way of, let's say,
capturing this great event.

448
01:13:26,100 --> 01:13:29,200
So I would like to see you all
tomorrow after breakfast

449
01:13:29,400 --> 01:13:31,600
by the Basilica of the Rosary.

450
01:13:33,300 --> 01:13:35,400
For the helpers...

451
01:13:35,700 --> 01:13:38,700
We are now going to go
to the little chapel.

452
01:13:39,400 --> 01:13:41,800
We've decided to hold a novena

453
01:13:42,000 --> 01:13:44,100
on behalf of our Cécile,

454
01:13:44,600 --> 01:13:47,800
who is still, unfortunately,
between life and death.

455
01:14:44,500 --> 01:14:46,700
One, two, three...

456
01:14:49,900 --> 01:14:51,700
Hold it, I'll take another.

457
01:14:52,400 --> 01:14:53,800
On three again.

458
01:14:54,400 --> 01:14:56,600
One, two, three...

459
01:14:59,100 --> 01:15:00,700
Thank you. Have a nice day.

460
01:15:21,700 --> 01:15:23,800
How are you? Still feeling well?

461
01:15:24,400 --> 01:15:25,600
Yes, fine.

462
01:15:26,100 --> 01:15:27,000
So much the better.

463
01:15:27,200 --> 01:15:28,300
Yes.

464
01:15:53,500 --> 01:15:55,300
You're not on the list.

465
01:15:56,300 --> 01:15:58,600
She's coming instead of Cécile.

466
01:16:02,100 --> 01:16:03,500
Cécile...

467
01:16:05,100 --> 01:16:06,500
Carré...

468
01:16:10,800 --> 01:16:13,600
I want to start a career
as soon as possible.

469
01:16:14,400 --> 01:16:17,300
I have the feeling
great things are in store for me.

470
01:16:17,600 --> 01:16:19,100
You know...

471
01:16:19,900 --> 01:16:23,600
I really feel
that I have a future now.

472
01:16:24,600 --> 01:16:25,800
Of course.

473
01:16:26,100 --> 01:16:28,300
I wish you success.

474
01:16:29,200 --> 01:16:29,800
Thank you.

475
01:16:30,000 --> 01:16:33,700
Or you could have a family.
That's possible now, isn't it?

476
01:16:34,600 --> 01:16:35,600
Yes.

477
01:16:36,800 --> 01:16:38,100
That's true.

478
01:17:15,800 --> 01:17:18,700
- Do you need a hand?
- No, thanks.

479
01:17:20,400 --> 01:17:22,200
I'm pleased for her.

480
01:17:22,900 --> 01:17:24,600
She's made such an effort.

481
01:17:25,200 --> 01:17:28,500
I wonder why her, and not...
I don't know, let's say...

482
01:17:28,700 --> 01:17:30,100
Mr Hruby?

483
01:17:30,800 --> 01:17:32,000
Why Mr Hruby?

484
01:17:32,300 --> 01:17:35,000
Or the girl in the wheelchair.

485
01:17:35,300 --> 01:17:37,400
Her mother comes here every year.

486
01:17:38,600 --> 01:17:40,800
That's not what counts, obviously.

487
01:17:43,600 --> 01:17:46,700
Yes, doing too much doesn't help
either. That's for sure.

488
01:17:48,600 --> 01:17:50,900
Just look
at what happened to Cécile.

489
01:17:51,800 --> 01:17:53,800
She was at it non-stop.

490
01:17:54,900 --> 01:17:58,100
Yes, but Mrs Carré
pulled her weight too.

491
01:18:01,300 --> 01:18:03,900
She doesn't seem very pious,
our miracle girl.

492
01:18:19,300 --> 01:18:21,500
Sorry to bother you, father.

493
01:18:22,000 --> 01:18:23,600
But I'd like to ask you

494
01:18:23,800 --> 01:18:25,700
something
we're all wondering about.

495
01:18:25,900 --> 01:18:28,700
Why has she been cured
and not...

496
01:18:28,900 --> 01:18:30,600
Mr Hruby, let's say?

497
01:18:35,000 --> 01:18:36,600
God is free.

498
01:18:39,300 --> 01:18:41,300
His ways are often mysterious to us,

499
01:18:41,600 --> 01:18:44,300
when we expect everything
to be explained.

500
01:18:46,300 --> 01:18:49,000
Why is this person cured
and not another?

501
01:18:50,800 --> 01:18:52,800
That's the way life is.

502
01:18:53,900 --> 01:18:56,400
One person can play the piano,
another can't.

503
01:18:56,600 --> 01:18:59,300
One has a gift for languages,
another doesn't.

504
01:18:59,700 --> 01:19:01,700
One is rich, another is not.

505
01:19:07,500 --> 01:19:09,000
Thank you.

506
01:19:24,500 --> 01:19:26,900
Are you all alone?

507
01:19:30,000 --> 01:19:31,600
I'm thinking.

508
01:19:35,700 --> 01:19:37,100
All of this is...

509
01:19:38,400 --> 01:19:39,700
very...

510
01:19:43,100 --> 01:19:44,600
It bothers me.

511
01:20:10,700 --> 01:20:12,900
Skiing must be great here in winter.

512
01:20:13,900 --> 01:20:16,200
I'm looking forward to skiing again.

513
01:20:48,700 --> 01:20:51,400
She'll have to come
to terms with it too.

514
01:21:44,300 --> 01:21:46,100
You're very special.

515
01:21:47,800 --> 01:21:49,000
Really?

516
01:21:49,900 --> 01:21:51,200
Really.

517
01:21:52,700 --> 01:21:54,800
Getting up like that and walking.

518
01:21:59,800 --> 01:22:01,400
How did you do it?

519
01:22:14,500 --> 01:22:16,100
I'm so...

520
01:22:25,100 --> 01:22:26,600
I'd so like to...

521
01:22:30,800 --> 01:22:32,800
I'm scared of hurting you.

522
01:23:05,800 --> 01:23:07,400
One, two, one, two.

523
01:23:11,000 --> 01:23:12,100
One, two.

524
01:23:12,700 --> 01:23:14,500
May the Lord be with you.

525
01:23:14,700 --> 01:23:16,400
And with your spirit.

526
01:23:23,500 --> 01:23:25,800
Something miraculous has happened.

527
01:23:28,700 --> 01:23:31,000
God has sent us a powerful sign,

528
01:23:32,600 --> 01:23:34,500
a sign of His grace

529
01:23:35,500 --> 01:23:37,500
and His love.

530
01:23:40,900 --> 01:23:43,400
We can see it with our own eyes.

531
01:24:00,900 --> 01:24:03,300
Heaven has reached down to earth.

532
01:24:06,100 --> 01:24:08,300
And what does this tell us?

533
01:24:10,200 --> 01:24:12,200
That God has not forsaken us,

534
01:24:13,900 --> 01:24:16,100
that He is always watching over us,

535
01:24:16,400 --> 01:24:18,100
that He loves us.

536
01:24:19,400 --> 01:24:22,500
Through this sign
He has manifested His presence.

537
01:24:23,900 --> 01:24:25,300
He is telling us,

538
01:24:26,200 --> 01:24:28,000
"You are not alone. "

539
01:25:11,000 --> 01:25:13,100
Thank you very much.

540
01:25:16,000 --> 01:25:19,300
I am very grateful.
This is an honour for me.

541
01:25:21,400 --> 01:25:24,000
I am also grateful for the grace,

542
01:25:25,300 --> 01:25:27,400
that I was the one to be cured.

543
01:25:28,100 --> 01:25:30,400
It's given me something
to think about.

544
01:25:30,700 --> 01:25:33,300
Why me and not... ?

545
01:25:34,500 --> 01:25:37,100
But I think there's meaning to it.
Well...

546
01:25:38,100 --> 01:25:40,700
For me, anyway.
What I mean is,

547
01:25:41,200 --> 01:25:43,400
I hope to be the right person.

548
01:25:55,500 --> 01:25:57,400
I am happy to award you

549
01:25:57,600 --> 01:26:00,000
the prize
for the year's best pilgrim.

550
01:26:25,400 --> 01:26:27,900
It's been good, though, hasn't it?

551
01:26:28,700 --> 01:26:31,400
Yes, but tomorrow
I'll be alone again.

552
01:26:32,100 --> 01:26:34,200
We are not alone.

553
01:26:35,400 --> 01:26:36,900
We are.

554
01:27:42,800 --> 01:27:45,000
- Do you want to dance?
- Yes.

555
01:29:22,300 --> 01:29:24,500
I'm so happy.

556
01:30:11,200 --> 01:30:12,300
Are you all right?

557
01:30:13,500 --> 01:30:14,400
Yes.

558
01:30:16,900 --> 01:30:19,700
- Do you want your stick?
- No, thanks.

559
01:30:33,500 --> 01:30:35,200
Come and sit down.

560
01:30:35,500 --> 01:30:37,100
No, I'm fine.

561
01:31:44,700 --> 01:31:46,300
I don't need it.

562
01:32:02,600 --> 01:32:04,600
- I'll be right back.
- OK.

563
01:32:19,200 --> 01:32:20,700
A pity.

564
01:32:21,300 --> 01:32:23,500
I almost believed it.

565
01:32:23,900 --> 01:32:25,400
What do you mean?

566
01:32:25,900 --> 01:32:27,800
She tripped, that's all.

567
01:32:31,800 --> 01:32:34,100
Just imagine if it doesn't last.

568
01:32:35,000 --> 01:32:36,900
That would be so cruel.

569
01:32:37,700 --> 01:32:39,600
How can God do that?

570
01:32:41,800 --> 01:32:45,800
If it doesn't last,
then it wasn't a real miracle.

571
01:32:47,500 --> 01:32:49,700
So He is not in charge.

572
01:32:53,500 --> 01:32:55,200
Who is, then?

573
01:33:00,500 --> 01:33:02,500
Do you think there'll be a dessert?

