﻿1
00:00:02,102 --> 00:00:04,236
- It's headed
right for the pin.

2
00:00:04,238 --> 00:00:06,238
- Oh, man.

3
00:00:06,240 --> 00:00:08,107
- Why can't I hit a ball
like that?

4
00:00:08,109 --> 00:00:09,308
- I could answer that,

5
00:00:09,310 --> 00:00:12,378
Or you could just look
in the mirror.

6
00:00:13,480 --> 00:00:15,781
- All right,
what did I miss?

7
00:00:15,783 --> 00:00:19,685
- The swedish guy
is starting to choke.

8
00:00:19,687 --> 00:00:21,153
- Swedish guy?

9
00:00:21,155 --> 00:00:26,158
Who taught him how to swing,
heidi?

10
00:00:26,160 --> 00:00:29,061
- Actually,
heidi was swiss.

11
00:00:34,100 --> 00:00:36,735
- Get out of my chair.

12
00:00:36,737 --> 00:00:39,304
- Hey, robert, can you
move over a little bit, please?

13
00:00:39,306 --> 00:00:40,773
- No. Take the middle.
- I don't want the middle.

14
00:00:40,775 --> 00:00:42,174
Gianni, you move over
to the middle.

15
00:00:42,176 --> 00:00:43,509
I'll go on the end.
- Just sit down.

16
00:00:43,511 --> 00:00:45,744
- All right, you're gonna
feel a little man thigh.

17
00:00:45,746 --> 00:00:48,147
How does that feel?
You like that?

18
00:00:48,149 --> 00:00:49,748
- Come on!
Cool it already.

19
00:00:49,750 --> 00:00:51,483
- People, shut up,
all of you guys.

20
00:00:51,485 --> 00:00:53,118
- What?
What's wrong?

21
00:00:53,120 --> 00:00:55,554
- Nothing. It's...

22
00:00:55,556 --> 00:01:00,192
Debra's not feeling well
bio...Hormonally.

23
00:01:01,594 --> 00:01:03,695
- Oh...Got it.

24
00:01:03,697 --> 00:01:08,367
The enemy within.

25
00:01:15,308 --> 00:01:17,709
- So...Do me a favor.

26
00:01:17,711 --> 00:01:19,478
Just don't do--
don't give her a reason--

27
00:01:19,480 --> 00:01:20,813
Be cool. Be cool. Be cool.

28
00:01:23,783 --> 00:01:24,850
- Hey, ray.

29
00:01:24,852 --> 00:01:26,385
These were on top
of the hamper.

30
00:01:26,387 --> 00:01:27,820
Are they dirty,
or are they clean?

31
00:01:27,822 --> 00:01:29,321
- Uh, yeah,
those are dirty.

32
00:01:29,323 --> 00:01:30,856
- Oh, they're dir--
okay, well, then, ray,

33
00:01:30,858 --> 00:01:34,593
Let me enlighten you
as to how a hamper works, okay?

34
00:01:34,595 --> 00:01:37,629
There's a lid on it,
and that lid lifts up.

35
00:01:37,631 --> 00:01:40,199
Now, you take your clothes--
not your clean clothes,

36
00:01:40,201 --> 00:01:43,268
Not the clothes you're
wearing right now,

37
00:01:43,270 --> 00:01:45,204
But your dirty clothes--

38
00:01:45,206 --> 00:01:47,239
You lift that lid,
you insert the clothes,

39
00:01:47,241 --> 00:01:49,408
And you shut that lid.

40
00:01:52,812 --> 00:01:55,447
- Yeah. Got it. Got it.

41
00:01:55,449 --> 00:01:56,615
- This is very nice, ray.

42
00:01:56,617 --> 00:01:58,150
Yeah. This is very nice.

43
00:01:58,152 --> 00:01:59,485
Pretzel crumbs
all over the place.

44
00:01:59,487 --> 00:02:00,552
- Yeah, I'm gonna clean
those up.

45
00:02:07,827 --> 00:02:09,862
- Okay, see what
I'm talking about?

46
00:02:09,864 --> 00:02:13,132
Frickin' mood swings.

47
00:02:13,134 --> 00:02:14,700
They find me chopped up
in the freezer,

48
00:02:14,702 --> 00:02:17,169
Don't believe
the suicide note.

49
00:02:23,376 --> 00:02:24,643
- That's a beautiful shot.

50
00:02:24,645 --> 00:02:27,312
- Boy,
it's a rough time of the month.

51
00:02:27,314 --> 00:02:29,281
You know a lot of criminals
turn themselves in

52
00:02:29,283 --> 00:02:33,152
When their wives are like this?

53
00:02:33,154 --> 00:02:34,786
- Look,
just do me a favor.

54
00:02:34,788 --> 00:02:36,155
Don't be, like, animals,
all right?

55
00:02:36,157 --> 00:02:38,824
I don't need--
- iceberg dead ahead.

56
00:02:43,329 --> 00:02:44,429
- Coaster.

57
00:02:44,431 --> 00:02:45,797
Robert, coaster.

58
00:02:45,799 --> 00:02:48,800
- What?

59
00:02:55,575 --> 00:02:56,909
- Need help?
Need any help?

60
00:02:56,911 --> 00:02:58,777
- No. I'm going to the bank,
all right?

61
00:02:58,779 --> 00:03:00,812
Listen,
when the buzzer goes off,

62
00:03:00,814 --> 00:03:02,514
Could you maybe take the clothes
out of the dryer?

63
00:03:02,516 --> 00:03:04,883
- Yeah. No problem.
How long you gonna be?

64
00:03:04,885 --> 00:03:07,252
- Why?

65
00:03:07,254 --> 00:03:08,320
- Oh, nothing.

66
00:03:08,322 --> 00:03:10,689
No. Take your time.

67
00:03:10,691 --> 00:03:13,292
Or hurry back if you want to.
It's your choice.

68
00:03:25,371 --> 00:03:28,574
ah...

69
00:03:28,576 --> 00:03:32,578
I thought
she'd never leave.

70
00:03:32,580 --> 00:03:37,216
- Is debra aware of how
she's coming off to others?

71
00:03:37,218 --> 00:03:40,786
- No, she--she doesn't
hear herself.

72
00:03:40,788 --> 00:03:44,356
And if I bring it up to her,
she gets all nuts and denies it.

73
00:03:44,358 --> 00:03:46,792
That's the sickness.

74
00:03:46,794 --> 00:03:50,295
- You should get her on tape
and make her listen to it.

75
00:03:50,297 --> 00:03:51,396
- You want to wear a wire?

76
00:03:51,398 --> 00:03:55,234
I'll shave you down
and hook you up.

77
00:03:57,303 --> 00:03:59,471
- Why doesn't she just
take something for this?

78
00:03:59,473 --> 00:04:00,839
- What's she gonna take?

79
00:04:00,841 --> 00:04:05,844
- Raymond, have you ever been
in a drugstore, huh?

80
00:04:05,846 --> 00:04:10,315
There's a whole wing
dedicated to this problem.

81
00:04:10,317 --> 00:04:11,617
- Yeah, all right,
what am I gonna do?

82
00:04:11,619 --> 00:04:13,318
What am I gonna do,
buy her something?

83
00:04:13,320 --> 00:04:14,853
Besides, I wouldn't
even know what to get.

84
00:04:14,855 --> 00:04:18,423
- Get 'em all.

85
00:04:18,425 --> 00:04:19,825
- Yeah, all right.

86
00:04:19,827 --> 00:04:21,493
Just don't worry about me,
all right?

87
00:04:21,495 --> 00:04:22,961
I'll just ride it out.

88
00:04:22,963 --> 00:04:27,266
- You dumb bastard.

89
00:04:27,268 --> 00:04:28,567
- What?

90
00:04:28,569 --> 00:04:30,035
- Ride it out?

91
00:04:30,037 --> 00:04:31,803
You put a stop
to this now.

92
00:04:31,805 --> 00:04:33,405
- All right, dad.
All right.

93
00:04:33,407 --> 00:04:35,607
- Let me ask you,
she's in a bad mood what,

94
00:04:35,609 --> 00:04:37,976
Two, three days tops?

95
00:04:37,978 --> 00:04:40,379
Not so tough now, right?

96
00:04:40,381 --> 00:04:45,450
She's gonna get a mood
that lasts five days...

97
00:04:45,452 --> 00:04:47,452
Then it's a week.

98
00:04:47,454 --> 00:04:49,054
And before you know it,

99
00:04:49,056 --> 00:04:51,356
What used to be
a bad mood now takes over

100
00:04:51,358 --> 00:04:55,527
And becomes her only mood.

101
00:04:55,529 --> 00:04:56,895
And then...

102
00:04:56,897 --> 00:05:00,766
Well, then you become
like me...

103
00:05:00,768 --> 00:05:02,501
Where not a day goes by

104
00:05:02,503 --> 00:05:04,870
That I don't wish
there was a comet

105
00:05:04,872 --> 00:05:09,875
Screaming towards earth
to bring me sweet relief.

106
00:05:15,749 --> 00:05:17,382
- What's so funny?

107
00:05:17,384 --> 00:05:19,017
- Dumb bastard.
He's a dumb bastard.

108
00:05:24,357 --> 00:05:27,359
- Hey, banjopants.

109
00:05:32,765 --> 00:05:35,667
Hope you're in the mood
for some delicious italian,

110
00:05:35,669 --> 00:05:37,969
And I don't mean just me.

111
00:05:39,539 --> 00:05:41,707
Big bag of nemo's takeout.

112
00:05:41,709 --> 00:05:43,375
- Oh...

113
00:05:43,377 --> 00:05:44,976
- What? I--I went by,

114
00:05:44,978 --> 00:05:46,611
And I stopped,
and I picked up some dinner.

115
00:05:46,613 --> 00:05:49,014
I also picked up
some after-dinner mints...

116
00:05:49,016 --> 00:05:52,017
Which for your sake,
I've already dipped into.

117
00:05:52,019 --> 00:05:53,685
- But I have already
started dinner, ray.

118
00:05:53,687 --> 00:05:55,854
Why didn't you call me
ahead of time?

119
00:05:55,856 --> 00:05:57,756
- Who knows?
But look, surprise.

120
00:05:57,758 --> 00:05:59,624
Garlic bread.

121
00:05:59,626 --> 00:06:02,527
- Well, I've made a salad,
and I've cooked the rice,

122
00:06:02,529 --> 00:06:05,364
But...
Forget it.

123
00:06:05,366 --> 00:06:08,800
We'll eat your food.

124
00:06:16,008 --> 00:06:17,809
- You all right?

125
00:06:17,811 --> 00:06:19,044
- Yeah...Fine.

126
00:06:19,046 --> 00:06:21,146
I'm sorry.
Thanks for the dinner.

127
00:06:21,148 --> 00:06:23,048
- Don't worry about it.

128
00:06:23,050 --> 00:06:25,617
Look, I know a lot
of stuff's going on...

129
00:06:25,619 --> 00:06:28,620
Feminine-ally...

130
00:06:32,024 --> 00:06:34,926
You know.

131
00:06:34,928 --> 00:06:36,762
I know it's--
it's not your fault.

132
00:06:36,764 --> 00:06:39,398
I understand that.
It's, uh...

133
00:06:39,400 --> 00:06:43,435
You know, it's like
a jekyll-and-hyde thing...

134
00:06:43,437 --> 00:06:47,439
Only more...Hyde...

135
00:06:47,441 --> 00:06:48,774
If he's the bad one.

136
00:06:48,776 --> 00:06:49,775
It was hyde, right?

137
00:06:49,777 --> 00:06:50,876
Whatever. Whatever.

138
00:06:50,878 --> 00:06:53,979
That's yelling at my friends
for making a mess

139
00:06:53,981 --> 00:06:56,515
Or getting all crazy
about clothes on the hamper.

140
00:06:56,517 --> 00:06:57,916
I know that
that's not you.

141
00:06:57,918 --> 00:07:00,685
That's hyde...

142
00:07:00,687 --> 00:07:04,156
If he was
the bad one.

143
00:07:04,158 --> 00:07:05,891
But you know what?

144
00:07:05,893 --> 00:07:08,193
While--while I was out today,
I got you something.

145
00:07:08,195 --> 00:07:09,628
I--I really...

146
00:07:09,630 --> 00:07:10,996
I want to help you
feel better, you know,

147
00:07:10,998 --> 00:07:14,166
And...And--and this should
take care of all your...

148
00:07:14,168 --> 00:07:16,635
Symptoms.

149
00:07:21,941 --> 00:07:26,511
- Except for bitchy,
right, ray?

150
00:07:28,649 --> 00:07:33,752
- I mean, there's nothing
in here for bitchy.

151
00:07:40,193 --> 00:07:43,195
- Probably need a prescription
for bitchy.

152
00:07:49,735 --> 00:07:51,536
- Boy, this is
just like you, ray.

153
00:07:51,538 --> 00:07:54,539
Just when I think that you can't
be any more insensitive,

154
00:07:54,541 --> 00:07:56,675
You rise to the occasion.

155
00:07:56,677 --> 00:07:59,077
- Hi, are the kids home yet?
I've got cookies.

156
00:07:59,079 --> 00:08:01,713
- Oh, mama, mom--mom,
please, just go home.

157
00:08:01,715 --> 00:08:02,848
It's not a good time--

158
00:08:02,850 --> 00:08:04,182
- It's okay.
She can stay if she wants.

159
00:08:04,184 --> 00:08:05,951
I'm not afraid
if she hears this.

160
00:08:05,953 --> 00:08:08,854
- Jekyll would be.

161
00:08:08,856 --> 00:08:10,722
- Tell you what,
I cannot...

162
00:08:10,724 --> 00:08:12,858
Take this anymore, okay?

163
00:08:12,860 --> 00:08:14,926
You make a huge mess,
you don't help me at all,

164
00:08:14,928 --> 00:08:16,261
You invite
your friends over

165
00:08:16,263 --> 00:08:18,096
To drive me crazy
all day,

166
00:08:18,098 --> 00:08:20,765
And all you can think of is,
"oh, she must have pms.

167
00:08:20,767 --> 00:08:22,167
Let's just load her up
with drugs."

168
00:08:22,169 --> 00:08:25,837
- No. It's got
st. John's warts in it.

169
00:08:25,839 --> 00:08:27,239
- I can't believe you.

170
00:08:27,241 --> 00:08:30,509
If there's ever
anything wrong, it's pms, huh?

171
00:08:30,511 --> 00:08:32,244
Sometimes--I just--
I don't know what to do.

172
00:08:32,246 --> 00:08:34,713
Sometimes I just want
to smack you.

173
00:08:40,853 --> 00:08:43,555
- Debra's right, raymond.

174
00:08:44,992 --> 00:08:48,727
- You smacked me.
- I know I did. I'm sorry.

175
00:08:48,729 --> 00:08:50,629
- Well, what--uh--
what you do that for?

176
00:08:50,631 --> 00:08:52,731
- It's just that it sounds
like you're becoming

177
00:08:52,733 --> 00:08:55,734
Just like your father.

178
00:08:55,736 --> 00:08:57,802
- What are you doing?
- I don't know.

179
00:08:57,804 --> 00:09:00,171
I don't know.
It's just that...

180
00:09:00,173 --> 00:09:02,908
He was so awful during my...

181
00:09:02,910 --> 00:09:05,243
Ladies' days.

182
00:09:05,245 --> 00:09:07,812
Anytime I happened
to make a comment

183
00:09:07,814 --> 00:09:09,648
About his usual
disgusting behavior,

184
00:09:09,650 --> 00:09:10,982
He would just
blame it on that.

185
00:09:10,984 --> 00:09:14,219
- That's what ray does.
- That's what they all do.

186
00:09:14,221 --> 00:09:15,287
- What are you talking about?

187
00:09:15,289 --> 00:09:16,588
Look, I'm just
trying to help.

188
00:09:16,590 --> 00:09:17,756
- You should be quiet.
- Yes.

189
00:09:17,758 --> 00:09:18,924
Yes. Oh, no.

190
00:09:18,926 --> 00:09:19,925
He is trying to help.

191
00:09:19,927 --> 00:09:24,095
Look what he bought me.
Magic pills, huh?

192
00:09:24,097 --> 00:09:25,597
Maybe I should try some.

193
00:09:25,599 --> 00:09:28,066
mmm. Oh, look.

194
00:09:28,068 --> 00:09:29,901
Ray's clothes
just flew off the bed

195
00:09:29,903 --> 00:09:30,936
And into the hamper.

196
00:09:30,938 --> 00:09:32,337
These really work.

197
00:09:32,339 --> 00:09:35,607
- Oh. Let me have one.

198
00:09:35,609 --> 00:09:37,943
Ohh...Frank's toenails

199
00:09:37,945 --> 00:09:41,079
Just crawled right
into the garbage can.

200
00:09:47,153 --> 00:09:52,157
- What are you doing, mom?
It's me. Raymie.

201
00:09:52,159 --> 00:09:54,225
- Raymond, you know
that I always let you two

202
00:09:54,227 --> 00:09:56,361
Fight your own battles,
but I can't let you go on

203
00:09:56,363 --> 00:09:59,030
With this cycle of
your father's imbecility.

204
00:09:59,032 --> 00:10:00,031
- I'm out of here.

205
00:10:00,033 --> 00:10:01,132
- Oh, come on.
Where you going?

206
00:10:01,134 --> 00:10:06,037
- I'm leaving too.
I'm gonna talk to your father.

207
00:10:06,039 --> 00:10:07,906
- Oh, please, come on.
Debra, debra, please.

208
00:10:07,908 --> 00:10:09,207
- Raymond, don't you
say one more word,

209
00:10:09,209 --> 00:10:10,675
Or I'm gonna send
your mother right back in here

210
00:10:10,677 --> 00:10:14,312
To smack the crap
out of you.

211
00:10:23,055 --> 00:10:26,658
- The three-game series is even
at one apiece.

212
00:10:26,660 --> 00:10:27,726
- Hi.

213
00:10:27,728 --> 00:10:29,928
- Oh, you're home.

214
00:10:29,930 --> 00:10:31,029
Did you have a good time?

215
00:10:31,031 --> 00:10:32,063
- Yeah.

216
00:10:32,065 --> 00:10:33,264
- What'd you do?

217
00:10:33,266 --> 00:10:35,734
- I told you.
I went shopping with amy.

218
00:10:35,736 --> 00:10:39,137
- Shopping? For 5 hours?

219
00:10:39,139 --> 00:10:42,774
Where are your shopping bags?

220
00:10:42,776 --> 00:10:43,975
- I didn't buy anything.

221
00:10:43,977 --> 00:10:47,412
Then we went back to amy's and,
you know, just sat around,

222
00:10:47,414 --> 00:10:49,114
Talking, relaxing.

223
00:10:49,116 --> 00:10:51,316
- Oh, sitting around,
talking, relaxing, yeah.

224
00:10:51,318 --> 00:10:52,917
You want to hear my evening?

225
00:10:52,919 --> 00:10:55,887
Peanut butter and jelly.

226
00:10:55,889 --> 00:10:57,822
Then I gave 'em a bath,
'cause they were covered

227
00:10:57,824 --> 00:11:00,091
In peanut butter and jelly.

228
00:11:00,093 --> 00:11:02,694
Then I'm mean, because I won't
let them sleep in a tree house,

229
00:11:02,696 --> 00:11:04,729
And then they went to bed
yelling and screaming.

230
00:11:04,731 --> 00:11:08,233
So that was my evening.

231
00:11:08,235 --> 00:11:10,301
Thank you very much.

232
00:11:10,303 --> 00:11:12,203
- Well, that's my evening
every night, so--

233
00:11:12,205 --> 00:11:13,371
- Oh, don't even.

234
00:11:17,410 --> 00:11:20,445
You just wanted me
to suffer tonight.

235
00:11:20,447 --> 00:11:23,014
- What?
- Yes. It's not fair.

236
00:11:23,016 --> 00:11:24,683
You get to walk out
and do whatever you want,

237
00:11:24,685 --> 00:11:26,217
And--and--
let's just say it--

238
00:11:26,219 --> 00:11:28,186
Use your "ladies' days"
as an excuse

239
00:11:28,188 --> 00:11:31,990
To treat me rotten
every month.

240
00:11:35,428 --> 00:11:38,163
- My needing to get out of here
once in a while

241
00:11:38,165 --> 00:11:43,168
Has nothing to do
with my "ladies' days."

242
00:11:45,137 --> 00:11:48,173
- Bull-loney.

243
00:11:53,279 --> 00:11:54,379
- What?

244
00:11:54,381 --> 00:11:55,847
<font color="#ffff00">-</font> Baloney.

245
00:11:55,849 --> 00:11:57,849
You want to see?
'cause I don't care, okay?

246
00:11:57,851 --> 00:12:00,719
I tried to be nice,
and that doesn't work.

247
00:12:00,721 --> 00:12:02,187
Here. Look at this.
You see this date?

248
00:12:02,189 --> 00:12:05,123
That's today.
Now, this is last month.

249
00:12:05,125 --> 00:12:07,258
What a coincidence
that on the same day last month,

250
00:12:07,260 --> 00:12:10,128
I found my golf clubs
upside down in the garbage can.

251
00:12:10,130 --> 00:12:12,363
I'm telling you,
I'm sick of this, all right?

252
00:12:12,365 --> 00:12:14,199
Every month the--
the same wacko screaming at me

253
00:12:14,201 --> 00:12:16,835
For no reason,
having meltdowns and crying,

254
00:12:16,837 --> 00:12:18,069
Crying over nothing.

255
00:12:18,071 --> 00:12:22,807
And god forbid I should mention
what might be the problem.

256
00:12:22,809 --> 00:12:23,942
You know what I think?

257
00:12:23,944 --> 00:12:25,310
I think that you
enjoy your ladies' days,

258
00:12:25,312 --> 00:12:27,112
Because you think I deserve
to be treated like this.

259
00:12:27,114 --> 00:12:28,279
"oh, look what day it is.

260
00:12:28,281 --> 00:12:29,280
Oh, come here, ray."

261
00:12:29,282 --> 00:12:32,917
Pow. Pow. Pow. Pow.

262
00:12:32,919 --> 00:12:35,920
"oh, oh, I've been
saving that up."

263
00:12:35,922 --> 00:12:37,922
And then after
a couple days of that,

264
00:12:37,924 --> 00:12:39,124
You're like, "oh, sorry, ray.

265
00:12:39,126 --> 00:12:43,094
I was just a little bit,
heh, you know."

266
00:12:43,096 --> 00:12:44,963
And by the way, the next time
I used those clubs,

267
00:12:44,965 --> 00:12:46,865
I shot a 110, so don't think
I don't know

268
00:12:46,867 --> 00:12:49,901
You didn't put a curse on them.

269
00:12:49,903 --> 00:12:52,303
- You are out of your mind.

270
00:12:52,305 --> 00:12:57,041
- No, you are...
Once a month.

271
00:12:57,043 --> 00:13:00,512
How come you don't treat me
like this on all the other days?

272
00:13:00,514 --> 00:13:01,880
- I treat you just fine, ray.

273
00:13:01,882 --> 00:13:03,214
You're just mad,
'cause you had to spend

274
00:13:03,216 --> 00:13:04,816
One lousy night
with the kids.

275
00:13:04,818 --> 00:13:06,351
- Oh, so you don't get crazy
this time of month?

276
00:13:06,353 --> 00:13:08,019
- No.
- You remain perfectly calm,

277
00:13:08,021 --> 00:13:10,388
Don't overreact,
don't treat me meanly,

278
00:13:10,390 --> 00:13:12,891
Or yell at me
like a screaming, raving maniac.

279
00:13:12,893 --> 00:13:14,392
- No, I do not.

280
00:13:16,462 --> 00:13:19,864
4:38 today...

281
00:13:22,268 --> 00:13:24,269
Right when
the guys went home.

282
00:13:24,271 --> 00:13:27,806
- I've told you 8 million times
when you empty the dryer,

283
00:13:27,808 --> 00:13:29,908
You clean the lint screen.

284
00:13:29,910 --> 00:13:31,042
Look at all this lint.

285
00:13:31,044 --> 00:13:32,210
What, do you like lint, ray?

286
00:13:32,212 --> 00:13:33,545
Maybe I'll get you some
for your birthday.

287
00:13:33,547 --> 00:13:36,881
Happy birthday, ray.
Here's your lint.

288
00:13:46,091 --> 00:13:49,093
- I just thought you'd,
you know,

289
00:13:49,095 --> 00:13:50,528
Want to hear
what you sound like

290
00:13:50,530 --> 00:13:54,299
When you're like this.

291
00:13:54,301 --> 00:13:56,935
Okay?

292
00:13:58,604 --> 00:14:01,506
- You are a gigantic ass.

293
00:14:05,244 --> 00:14:08,246
10:32, "gigantic ass."

294
00:14:15,254 --> 00:14:20,158
Listen, I'm sorry.

295
00:14:20,160 --> 00:14:24,896
I know what it's like
to hear yourself on tape.

296
00:14:24,898 --> 00:14:29,367
I remember when I heard
my first radio interview.

297
00:14:29,369 --> 00:14:32,971
First of all, I sounded
like snuffleupagus.

298
00:14:37,443 --> 00:14:39,944
I said, "do I really sound
like that?" and andy said--

299
00:14:39,946 --> 00:14:41,412
- Oh, would you stop it.

300
00:14:43,249 --> 00:14:45,350
I'm not crying
because of what I sound like.

301
00:14:45,352 --> 00:14:48,887
I'm crying because I'm married
to an insensitive durfwad

302
00:14:48,889 --> 00:14:51,356
Who instead of trying to make
life better for his wife,

303
00:14:51,358 --> 00:14:54,626
Tape-records her so he has proof
that she's a terrible person.

304
00:14:54,628 --> 00:14:59,163
- What's a durfwad?

305
00:14:59,165 --> 00:15:00,665
- What, was that tape
for the guys, huh?

306
00:15:00,667 --> 00:15:02,133
You gonna play it
for the guys?

307
00:15:02,135 --> 00:15:04,135
"hey, come on, fellas,
step right up

308
00:15:04,137 --> 00:15:06,537
And listen to the witch
who killed my good time."

309
00:15:06,539 --> 00:15:08,006
- No. No.
It's just for us.

310
00:15:08,008 --> 00:15:10,241
- Yeah. Yeah.

311
00:15:10,243 --> 00:15:11,509
Where's my tape recorder, ray?

312
00:15:11,511 --> 00:15:15,079
When I was in labor
with the twins for 36 hours,

313
00:15:15,081 --> 00:15:17,548
And you were asking the nurse
if the tv gets espn?

314
00:15:20,152 --> 00:15:22,520
- Well, I mean, 36 hours,
that's not a short time.

315
00:15:22,522 --> 00:15:24,188
- Yeah. Guess what.

316
00:15:24,190 --> 00:15:25,990
It's even longer
when you're trying to push

317
00:15:25,992 --> 00:15:27,091
Two human beings
out of your body

318
00:15:27,093 --> 00:15:30,995
"does this hospital
have fudgesicles?"

319
00:15:30,997 --> 00:15:33,031
Yeah, where's
my tape recorder, huh?

320
00:15:33,033 --> 00:15:34,365
Or when you ask me
why I get so upset

321
00:15:34,367 --> 00:15:36,334
Because I find your
underpants in the kitchen, huh?

322
00:15:36,336 --> 00:15:40,538
Or when you started snoring
at my grandmother's funeral?

323
00:15:40,540 --> 00:15:43,074
Or when you taped
a football game

324
00:15:43,076 --> 00:15:47,478
Over our wedding video?

325
00:15:47,480 --> 00:15:49,681
- Do you--do you really need
a tape recorder?

326
00:15:49,683 --> 00:15:51,015
You seem to remember everything.

327
00:15:51,017 --> 00:15:53,551
- Yeah.

328
00:15:53,553 --> 00:15:55,586
That's right, ray.
I remember everything.

329
00:15:55,588 --> 00:15:57,255
And I'm not gonna forget,
either.

330
00:15:57,257 --> 00:15:58,656
- Honey, I think you make
some excellent points here,

331
00:15:58,658 --> 00:16:01,259
But I can't help
wondering that, you know,

332
00:16:01,261 --> 00:16:03,428
Maybe part of the reason
you're so upset right now

333
00:16:03,430 --> 00:16:05,096
Might possibly
be pms-related.

334
00:16:05,098 --> 00:16:07,498
- Would somebody get me
a tape recorder,

335
00:16:07,500 --> 00:16:10,668
- Look, I don't know what to do.
I just don't know what to do.

336
00:16:10,670 --> 00:16:12,603
- Yeah. Yeah.
No kidding.

337
00:16:12,605 --> 00:16:15,139
Listen, if I had pms--
and I'm not saying that I have--

338
00:16:15,141 --> 00:16:16,507
Is that how you help me?

339
00:16:16,509 --> 00:16:19,077
By taping me,
by telling me I have pms?

340
00:16:19,079 --> 00:16:22,580
That doesn't help me, ray.
That doesn't help me.

341
00:16:22,582 --> 00:16:24,215
- Well, what do
you want me to do?

342
00:16:24,217 --> 00:16:26,017
I don't know what to do.

343
00:16:26,019 --> 00:16:28,086
Show me what to do.
Just draw it out for me.

344
00:16:28,088 --> 00:16:30,188
- Have you ever thought
about giving me a hug?

345
00:16:30,190 --> 00:16:33,124
- A hug.

346
00:16:33,126 --> 00:16:36,260
Did you ever think
of hugging me, you jerk?

347
00:16:36,262 --> 00:16:37,495
- Well, it's pretty hard
to hug someone

348
00:16:37,497 --> 00:16:39,464
Who's trying to kill you.

349
00:16:39,466 --> 00:16:40,531
- Yeah, well, it never
occurred to you.

350
00:16:40,533 --> 00:16:42,100
You've never even
tried it before.

351
00:16:42,102 --> 00:16:44,369
- Well, look.
This is not huggable.

352
00:16:44,371 --> 00:16:46,404
This--this is not debra.

353
00:16:46,406 --> 00:16:48,373
This is the woman
who shows up once a month

354
00:16:48,375 --> 00:16:52,143
To rip into me like a monkey
on a cupcake.

355
00:16:52,145 --> 00:16:53,344
All right, that's it.

356
00:16:53,346 --> 00:16:54,479
That's the reason
I taped you:

357
00:16:54,481 --> 00:16:58,416
And maybe admit
that there might be a problem

358
00:16:58,418 --> 00:17:02,587
That isn't just me
being a dorkwang or whatever.

359
00:17:06,292 --> 00:17:08,526
- Hello?

360
00:17:08,528 --> 00:17:10,561
Hi, amy.

361
00:17:10,563 --> 00:17:13,131
Yeah. Yeah.

362
00:17:13,133 --> 00:17:16,534
You went back
for that dress?

363
00:17:16,536 --> 00:17:18,536
How much was it?

364
00:17:18,538 --> 00:17:22,106
Oh. You are so bad.

365
00:17:22,108 --> 00:17:24,275
Yeah. Right. Right. Right.

366
00:17:24,277 --> 00:17:26,811
Oh, god. I know.
Me too. Me too.

367
00:17:26,813 --> 00:17:29,680
Oh, my god.

368
00:17:29,682 --> 00:17:33,217
Okay, well, listen.
I'll call you tomorrow, okay?

369
00:17:33,219 --> 00:17:38,189
Okay. All right. Bye-bye.

370
00:17:44,730 --> 00:17:48,766
- Feeling better?

371
00:17:48,768 --> 00:17:50,635
- She's so funny.

372
00:17:50,637 --> 00:17:52,570
- Yeah, she is.

373
00:17:52,572 --> 00:17:56,340
Could we put her number
on speed dial?

374
00:17:59,111 --> 00:18:00,211
- Listen, ray.

375
00:18:00,213 --> 00:18:04,782
Oh. I'm sorry.

376
00:18:04,784 --> 00:18:09,253
I guess I am a little,
you know.

377
00:18:09,255 --> 00:18:12,790
- Well, I didn't want
to say anything.

378
00:18:19,565 --> 00:18:22,467
- Listen, I...

379
00:18:22,469 --> 00:18:25,336
I--I don't mean to--
to lay into you like that.

380
00:18:25,338 --> 00:18:27,605
- I know. I know. I know.

381
00:18:27,607 --> 00:18:32,610
- Come on. Sit.
- No. It's okay.

382
00:18:32,612 --> 00:18:34,145
- Ray.

383
00:18:38,283 --> 00:18:39,584
Honey, listen.

384
00:18:39,586 --> 00:18:44,856
I need a little understanding,
just a little caring.

385
00:18:44,858 --> 00:18:46,257
I need to know that,
you know,

386
00:18:46,259 --> 00:18:48,126
If I'm going through
a rough time,

387
00:18:48,128 --> 00:18:50,361
That--that
you're there for me,

388
00:18:50,363 --> 00:18:52,430
And that you want to,
you know,

389
00:18:52,432 --> 00:18:55,299
Just take care of me.

390
00:18:55,301 --> 00:18:58,903
- I do want to.

391
00:18:58,905 --> 00:19:01,205
You're my girl.

392
00:19:03,909 --> 00:19:05,676
- I love you, ray.

393
00:19:13,185 --> 00:19:14,218
- Oh, god, you're a mess.

394
00:19:14,220 --> 00:19:17,221
- I know.

395
00:19:21,193 --> 00:19:23,628
- Listen, you want to just try
those pills that I got you?

396
00:19:23,630 --> 00:19:26,631
'cause I really just want you
to feel better.

397
00:19:26,633 --> 00:19:29,500
- Yeah, I know, but I just
want you to know that,

398
00:19:29,502 --> 00:19:31,636
You know, no pill
is gonna solve everything.

399
00:19:31,638 --> 00:19:34,672
- I mean, do you have any idea
what goes on in here?

400
00:19:34,674 --> 00:19:36,240
- Yeah. Let me just get 'em.
- I know, but--

401
00:19:36,242 --> 00:19:37,742
You know, yes,
there's all these emotions--

402
00:19:37,744 --> 00:19:40,178
Believe me. Listen.
I'm very into all of that.

403
00:19:40,180 --> 00:19:41,245
Let me get the pills.

404
00:20:18,717 --> 00:20:21,219
- Here we go.
Hmm?

405
00:20:21,221 --> 00:20:22,553
Here we go.
It's okay. It's okay.

406
00:20:22,555 --> 00:20:23,988
Look, it's, "for bloating--

407
00:20:23,990 --> 00:20:25,990
For bloating, cramps,"
and, oh, right there,

408
00:20:25,992 --> 00:20:27,825
"irritability
associated with pms."

409
00:20:27,827 --> 00:20:30,728
We have a winner.

410
00:20:30,730 --> 00:20:32,863
Goes great
with ginger ale.

411
00:20:32,865 --> 00:20:34,532
Okay, here we go.

412
00:20:37,236 --> 00:20:40,972
Here we go.

413
00:20:40,974 --> 00:20:42,473
What? Hey. Come on.

414
00:20:42,475 --> 00:20:43,908
- Why don't you just
lock me in the attic

415
00:20:43,910 --> 00:20:45,576
For a few days?

416
00:20:56,722 --> 00:20:58,356
- What?

417
00:20:58,358 --> 00:21:02,326
You, you're the one
who said, "hug me."

418
00:21:02,328 --> 00:21:05,763
The hug was your idea.

419
00:21:10,670 --> 00:21:12,737
Come on, comet.

420
00:21:24,616 --> 00:21:27,551
- So you think I'm just rude
and insensitive,

421
00:21:27,553 --> 00:21:28,686
That I'm so tough
to live with,

422
00:21:28,688 --> 00:21:32,290
And you're the queen of sheba?

423
00:21:32,292 --> 00:21:34,358
Did you ever
hear yourself?

424
00:21:34,360 --> 00:21:35,893
Well, in case you haven't,

425
00:21:35,895 --> 00:21:39,297
Here's a little trick
that my boy raymond taught me.

426
00:21:39,299 --> 00:21:43,634
I said, "where's my eggs?"

427
00:21:43,636 --> 00:21:45,036
Wait a minute.
Wait a minute.

428
00:21:45,038 --> 00:21:46,937
Here, here. Wait a minute.
Here, here it is.

429
00:21:46,939 --> 00:21:50,308
What?. It's just my feet.
They're not dirty.

430
00:21:50,310 --> 00:21:51,776
Aw, crap.

431
00:21:51,778 --> 00:21:55,646
You scared you're gonna hear
something you don't like?

432
00:21:55,648 --> 00:21:58,482
If I scratch it,
it's because it itches.

