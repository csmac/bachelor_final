1
00:06:57,280 --> 00:06:58,633
Read!

2
00:07:02,360 --> 00:07:07,150
October 22nd, 2009, Thursday.

3
00:07:07,960 --> 00:07:13,990
Year 1388 after the Rajah.

4
00:07:14,480 --> 00:07:19,918
170th day after the beginning of spring.

5
00:07:20,160 --> 00:07:25,393
295th day of the year.

6
00:07:26,520 --> 00:07:30,752
Daytime gets three minutes shorter.

7
00:07:31,800 --> 00:07:35,634
Defense of...

8
00:07:35,880 --> 00:07:38,917
Kansan 1601.

9
00:07:40,160 --> 00:07:44,950
"Facilitate things to people...

10
00:07:45,160 --> 00:07:48,197
do not make it harder for them...

11
00:07:48,360 --> 00:07:52,478
give them good tidings instead
of breeding hate."

12
00:07:54,080 --> 00:07:57,038
Hadith.

13
00:07:59,600 --> 00:08:00,999
Yusuf...

14
00:08:10,080 --> 00:08:12,196
Take care of these,
your only pair.

15
00:08:12,360 --> 00:08:13,759
OK.

16
00:08:26,120 --> 00:08:29,271
Dad, I dreamed last night.

17
00:08:31,640 --> 00:08:37,317
I was sitting under a tree.
The stars...

18
00:08:39,600 --> 00:08:43,513
Dreams shouldn't be spread around,
whisper.

19
00:09:00,520 --> 00:09:03,273
Don't tell your dreams to anybody.

20
00:11:07,120 --> 00:11:09,873
Children, now close your notebooks.

21
00:11:12,440 --> 00:11:14,237
Who wants to read?

22
00:11:21,200 --> 00:11:23,191
Lpek, please read.

23
00:11:28,320 --> 00:11:33,394
The Lion and the Mouse.

24
00:11:33,920 --> 00:11:39,870
On top of a sleeping lion...

25
00:11:40,200 --> 00:11:44,796
a little mouse...

26
00:11:45,000 --> 00:11:49,551
was running around.

27
00:11:49,800 --> 00:11:54,430
Its tiny steps...

28
00:11:54,640 --> 00:12:01,034
woke the lion up.

29
00:12:01,320 --> 00:12:05,154
He tossed the mouse...

30
00:12:06,360 --> 00:12:09,238
to the ground.

31
00:12:09,400 --> 00:12:13,188
And he grabbed it...

32
00:12:13,360 --> 00:12:16,318
with his paw.

33
00:12:16,520 --> 00:12:18,636
Fine, lpek. Well done.

34
00:12:18,880 --> 00:12:21,348
Let's clap for lpek.

35
00:12:27,280 --> 00:12:29,111
Come here lpek.

36
00:12:34,680 --> 00:12:37,558
This is for you.

37
00:12:43,080 --> 00:12:44,308
OK.

38
00:13:43,600 --> 00:13:46,239
Be seated.
- Thank you!

39
00:13:56,720 --> 00:14:00,156
Yes, Yusuf?
- May I read, teacher?

40
00:14:00,320 --> 00:14:01,958
Let's try.

41
00:14:16,560 --> 00:14:21,509
The Lion and the Mouse.
On top of a sleeping...

42
00:14:21,720 --> 00:14:26,396
Not that one.
Turn to "The Eagle and the Turtle."

43
00:14:26,600 --> 00:14:28,192
Read that one.

44
00:14:30,320 --> 00:14:33,312
Turn the page.
You'll see it.

45
00:14:33,880 --> 00:14:35,313
Turn the page.

46
00:14:44,840 --> 00:14:46,432
Go on, Yusuf.

47
00:15:13,280 --> 00:15:15,999
Go on, Yusuf. Read.

48
00:15:18,840 --> 00:15:25,234
The e-e-eagle...

49
00:15:32,480 --> 00:15:37,076
a-a a-n-n-nd th-e-e...

50
00:15:38,080 --> 00:15:44,519
T-t-tu-tu-rt-t-t-tle...

51
00:15:54,440 --> 00:15:59,275
Quiet! Again, Yusuf.
Relax and speak louder.

52
00:16:00,840 --> 00:16:05,550
The e-e-eagle...

53
00:16:06,280 --> 00:16:08,475
a-a-an

54
00:16:15,960 --> 00:16:17,757
Quiet!

55
00:16:23,080 --> 00:16:25,116
A-a-an and

56
00:16:27,360 --> 00:16:29,999
t-t-t-tu-tur...

57
00:16:35,960 --> 00:16:39,191
r-r-t-tle.

58
00:16:40,680 --> 00:16:43,831
Fine, Yusuf.
Let's try again later.

59
00:18:42,080 --> 00:18:44,992
I don't know what to do with him.

60
00:19:00,880 --> 00:19:02,791
Drink your milk, Yusuf.

61
00:19:26,120 --> 00:19:27,792
Cut it.

62
00:19:52,040 --> 00:19:54,395
What did you do today?

63
00:20:09,640 --> 00:20:14,509
If you like, you can tell me
by whispering into my ear.

64
00:20:33,000 --> 00:20:36,197
You know what? The tailless bear,

65
00:20:36,400 --> 00:20:39,278
that bear had cubs,
the one that went after Idris' beehives.

66
00:20:39,480 --> 00:20:41,869
Really?
- Really.

67
00:20:44,720 --> 00:20:48,599
Have you seen the cubs?

68
00:20:48,800 --> 00:20:50,791
Yes I have.

69
00:20:51,400 --> 00:20:53,675
When can I see them?

70
00:20:53,880 --> 00:20:57,919
On Saturday, when we go
to change the hives.

71
00:20:58,120 --> 00:20:59,553
OK.

72
00:21:51,960 --> 00:21:53,279
Yusuf...

73
00:21:55,240 --> 00:21:57,196
what's this flower called?

74
00:21:57,720 --> 00:21:59,676
Violet.

75
00:22:01,160 --> 00:22:03,879
Both...

76
00:22:04,080 --> 00:22:07,516
its honey and pollen
are of pinkish colour.

77
00:22:07,920 --> 00:22:09,512
What about its taste?
- What?

78
00:22:09,680 --> 00:22:12,319
Lts taste?
- Nice and sweet.

79
00:22:12,560 --> 00:22:14,755
Yes, sweet.

80
00:22:32,720 --> 00:22:34,995
What's this flower called?

81
00:22:35,480 --> 00:22:39,155
Erikotu.
Its pollen can make honey.

82
00:23:14,600 --> 00:23:16,192
Yusuf...

83
00:23:16,640 --> 00:23:18,835
What about that flower?

84
00:23:19,040 --> 00:23:22,794
But it doesn't have petals.

85
00:23:23,000 --> 00:23:26,879
Not now, because it's off-season.

86
00:23:27,080 --> 00:23:30,277
You tasted its honey last year
and it made you dizzy.

87
00:23:30,480 --> 00:23:32,675
The crazy honey!

88
00:28:54,320 --> 00:28:58,029
Havva come up to the blackboard
and read out page seventy five.

89
00:28:58,280 --> 00:29:00,919
Everybody open up your books
and follow.

90
00:31:59,920 --> 00:32:02,150
What are you doing, Yusuf?

91
00:32:34,640 --> 00:32:37,518
Put it near the stove
and it will dry.

92
00:32:37,680 --> 00:32:39,910
Go change your clothes.

93
00:32:41,440 --> 00:32:42,919
Yusuf!

94
00:32:44,000 --> 00:32:45,433
Yusuf?

95
00:35:53,240 --> 00:35:56,994
I need rope from Hamdi's father.
Stay and do your homework.

96
00:36:51,560 --> 00:36:54,279
Good afternoon, Huseyin.
- Hello, Yakup.

97
00:36:55,960 --> 00:36:58,599
Is it done?
- The rope is done.

98
00:37:00,400 --> 00:37:02,914
Come see me.

99
00:37:04,120 --> 00:37:05,951
Here, for you.

100
00:37:10,560 --> 00:37:12,994
Is that for me?
- Yes.

101
00:37:23,800 --> 00:37:26,360
Do you like it?
- I like it.

102
00:37:28,360 --> 00:37:30,794
Thank you, Uncle Yakup.

103
00:37:38,160 --> 00:37:40,116
Have a nice day, Huseyin.

104
00:37:40,320 --> 00:37:41,594
Thank you.

105
00:37:41,880 --> 00:37:45,668
Dad, look at what Uncle Yakup gave me.
- Thats very nice, son.

106
00:37:51,400 --> 00:37:52,879
Yusuf!

107
00:37:57,560 --> 00:37:58,879
Yusuf!

108
00:38:01,240 --> 00:38:03,231
Yusuf, come here!

109
00:38:10,200 --> 00:38:11,838
What's wrong, son?

110
00:38:14,640 --> 00:38:16,312
Tell me, son!

111
00:38:17,920 --> 00:38:21,071
Fa-fa-father...

112
00:38:21,280 --> 00:38:23,271
Are you scared?

113
00:38:25,480 --> 00:38:27,835
Come and help me!

114
00:39:22,280 --> 00:39:25,431
The tea is ready.
Bring it to your father.

115
00:40:29,280 --> 00:40:33,512
Me, me, me!

116
00:41:03,440 --> 00:41:07,319
Open your notebooks.
I'll check your homework.

117
00:41:32,040 --> 00:41:35,316
And your homework, Hamdi?
Didn't you do it?

118
00:41:36,520 --> 00:41:39,796
How many times must I tell you?

119
00:41:40,320 --> 00:41:42,311
Do your homework!

120
00:41:43,200 --> 00:41:46,237
Tell your mother to come see me.

121
00:41:48,040 --> 00:41:50,679
Be good like Yusuf.

122
00:42:59,520 --> 00:43:03,638
Will you place those bees
in the hives?

123
00:43:03,880 --> 00:43:05,029
Yes.

124
00:43:07,840 --> 00:43:10,229
When will you be back?

125
00:43:10,440 --> 00:43:12,237
In two days.

126
00:43:15,680 --> 00:43:18,433
Is Camgoz going with you?

127
00:43:18,640 --> 00:43:19,595
Yes.

128
00:43:52,320 --> 00:43:54,550
May I go with you?

129
00:43:56,240 --> 00:43:59,277
Who will take care of your mother?

130
00:44:44,720 --> 00:44:47,757
There was a sailboat there.

131
00:44:48,680 --> 00:44:51,399
It sailed out to sea.

132
00:45:27,360 --> 00:45:29,112
Yakup?

133
00:45:30,400 --> 00:45:33,756
Maybe we should take Yusuf
to lmam Niyazi.

134
00:45:37,800 --> 00:45:41,156
Gokhan said that there is honey
around the Karaucurum.

135
00:45:42,120 --> 00:45:45,237
I will place the new hives there.

136
00:45:46,560 --> 00:45:47,993
Why?

137
00:45:48,560 --> 00:45:51,154
There is no honey here.

138
00:45:51,320 --> 00:45:54,949
The beehives across the valley
are also empty.

139
00:45:55,840 --> 00:45:57,478
And the bees?

140
00:45:59,560 --> 00:46:02,233
God knows where they are.

141
00:46:04,080 --> 00:46:07,470
I must check the hives
around the junction.

142
00:46:59,640 --> 00:47:06,591
I seek refuge
in the Lord of mankind.

143
00:47:06,840 --> 00:47:08,910
The King of mankind,

144
00:47:09,120 --> 00:47:12,556
the God of mankind.

145
00:47:12,800 --> 00:47:18,193
Refuge from the evil of the whisperer

146
00:47:18,360 --> 00:47:22,592
who whispers in the hearts
of mankind.

147
00:47:22,840 --> 00:47:28,039
Be they of Djinn and mankind.

148
00:50:42,640 --> 00:50:43,959
Good Morning.

149
00:50:44,600 --> 00:50:45,874
Good Morning.

150
00:51:48,360 --> 00:51:50,316
Onder Saybas.
- Here.

151
00:51:50,640 --> 00:51:52,756
Gulcan Kesici.

152
00:51:52,960 --> 00:51:54,916
Yusuf Ozbek.

153
00:51:55,120 --> 00:51:56,235
Yusuf Ozbek?

154
00:51:57,360 --> 00:51:59,271
Hamdi Kaygin.

155
00:52:00,640 --> 00:52:03,359
Hamdi is absent?
- He didn't come today.

156
00:52:03,560 --> 00:52:06,518
Is he sick?
- I don't know.

157
00:52:07,080 --> 00:52:09,230
Sevval Agun.
- Here.

158
00:52:09,720 --> 00:52:11,517
Kemal Baylar.

159
00:52:12,280 --> 00:52:14,475
Nazlican Kopuz.

160
00:52:14,960 --> 00:52:17,190
Nermin Alinoglu.

161
00:52:27,160 --> 00:52:31,039
Pricked by wheat,
on fine grass I'll tread:

162
00:52:31,680 --> 00:52:34,877
Dreamer as I am,
my feet will feel its freshness.

163
00:52:35,080 --> 00:52:38,231
I'll let the wind wet my bare head.

164
00:52:38,480 --> 00:52:41,233
I'll not talk,
my mind will be a void:

165
00:52:41,920 --> 00:52:44,878
But endless love will surge
within my soul,

166
00:52:45,080 --> 00:52:50,279
And I'll go far, really far,
like a vagabond,

167
00:52:50,720 --> 00:52:53,393
In the country,
happy as if I was with a girl.

168
00:52:53,600 --> 00:52:56,910
On blue summer nights,
I'll go along pathways.

169
00:52:57,120 --> 00:53:00,271
Pricked by wheat,
on fine grass I'll tread:

170
00:53:00,480 --> 00:53:05,110
Dreamer as I am,
my feet will feel its freshness.

171
00:53:05,280 --> 00:53:07,589
I'll let the wind wet my bare head.

172
00:56:06,680 --> 00:56:08,193
Hamdi is inside.

173
00:58:34,280 --> 00:58:36,589
Brother Zekeriya,
did you find honey?

174
00:58:36,840 --> 00:58:38,956
A poor harvest this year.

175
00:58:39,160 --> 00:58:41,993
Have you been to the Karaucurum?
- No.

176
00:58:42,280 --> 00:58:44,794
I was wondering if you saw Yakup.

177
00:58:45,000 --> 00:58:49,437
Faik met him the day before yesterday.
- Brother Faik saw him?

178
00:58:50,400 --> 00:58:52,311
Faik, come here!

179
00:58:52,560 --> 00:58:55,518
Hello!
- Hello Brother Faik, did you see Yakup?

180
00:58:55,680 --> 00:58:59,639
Yes, he was trying to place his hives.

181
00:58:59,880 --> 00:59:02,519
He'll be back soon.
- Around the Karaucurum?

182
00:59:02,680 --> 00:59:05,990
Yes, he's looking for honey.
- Thank you, Brother.

183
00:59:20,120 --> 00:59:22,076
Are you hungry, Yusuf?

184
00:59:33,960 --> 00:59:35,598
On...

185
00:59:42,560 --> 00:59:47,190
blue summer nights...

186
00:59:47,880 --> 00:59:51,190
I'll go...

187
00:59:52,600 --> 00:59:57,116
along pathways...

188
01:00:47,640 --> 01:00:53,670
27th of October, 2009.

189
01:01:02,440 --> 01:01:07,992
The tenth month.
31st day. 44th Week.

190
01:01:08,280 --> 01:01:13,877
Daytime gets two minutes shorter.
Fish storm.

191
01:01:15,200 --> 01:01:16,553
Yusuf!

192
01:01:17,280 --> 01:01:19,589
Where are the eggs, son?

193
01:01:19,840 --> 01:01:22,593
I'm baking cookies
for your father's return.

194
01:02:07,680 --> 01:02:09,875
Bring me the eggs.

195
01:02:43,520 --> 01:02:45,431
Drink your milk, Yusuf.

196
01:03:36,040 --> 01:03:38,554
You didn't drink it.

197
01:03:42,920 --> 01:03:45,036
What's wrong, son?

198
01:03:45,240 --> 01:03:47,435
It's a pity to waste the good milk.

199
01:06:35,200 --> 01:06:36,792
Come here.

200
01:06:41,520 --> 01:06:43,397
Open it.

201
01:07:07,200 --> 01:07:11,193
I had a very nice dream last night.

202
01:07:16,720 --> 01:07:19,154
Your dad, you and me...

203
01:07:19,360 --> 01:07:22,591
we were going to place
the hives together.

204
01:07:23,280 --> 01:07:25,032
Down at the stream.

205
01:07:28,280 --> 01:07:32,956
That spot where
we used to place the hives.

206
01:07:37,040 --> 01:07:39,600
Then I saw a flower.

207
01:07:41,840 --> 01:07:44,593
I bent down to take it.

208
01:07:47,480 --> 01:07:49,948
I put it in my bag...

209
01:07:51,000 --> 01:07:53,798
to plant it at home.

210
01:07:56,600 --> 01:08:00,991
When we returned home,
those flowers were everywhere.

211
01:08:02,040 --> 01:08:05,396
In the kitchen, in the bedroom...

212
01:09:20,880 --> 01:09:22,950
Open your books.

213
01:09:23,480 --> 01:09:25,436
You too, Yusuf.

214
01:09:29,400 --> 01:09:33,518
Nutrition and its benefits.

215
01:09:33,680 --> 01:09:39,710
All living organisms must be nourished...

216
01:09:39,960 --> 01:09:43,839
to live.

217
01:09:44,160 --> 01:09:47,152
Because nutrition...

218
01:09:47,400 --> 01:09:49,868
provides the elements...

219
01:09:50,080 --> 01:09:54,835
that organisms need to continue living.

220
01:09:55,320 --> 01:10:00,599
Walking, running...

221
01:10:00,840 --> 01:10:06,995
doing work of any kind,
all requires energy.

222
01:10:07,520 --> 01:10:11,479
While our organs work,

223
01:10:11,640 --> 01:10:15,553
the energy is...

224
01:10:35,240 --> 01:10:36,719
Yusuf!

225
01:10:40,920 --> 01:10:43,354
Yusuf, I can't see.

226
01:10:49,640 --> 01:10:51,915
Stop it, son.

227
01:11:13,760 --> 01:11:15,671
What's wrong with you?

228
01:11:21,720 --> 01:11:23,870
Shall we do it that way?

229
01:11:45,160 --> 01:11:47,993
Do you remember Gurol?

230
01:11:48,360 --> 01:11:51,511
He was accepted into
the Istanbul Police Academy.

231
01:11:56,720 --> 01:12:00,190
Your Uncle Bekir will visit us
this year for Bayram.

232
01:12:37,400 --> 01:12:40,312
Would you also like to be a policeman?

233
01:12:44,920 --> 01:12:47,195
What do you want to be, Yusuf?

234
01:12:58,760 --> 01:13:00,478
Don't worry, dear.

235
01:13:00,640 --> 01:13:03,677
He can't be in trouble.
He'll be back.

236
01:13:03,920 --> 01:13:08,391
If you watch the boy,
I can go down to the gendarmerie.

237
01:13:19,560 --> 01:13:20,913
Have a seat.

238
01:13:22,400 --> 01:13:26,109
You have grown so much.

239
01:13:27,960 --> 01:13:31,111
I knitted this sweater.
Try it.

240
01:13:39,720 --> 01:13:42,314
It looks so nice, Yusuf.

241
01:13:44,720 --> 01:13:47,314
Come with me to the highland?

242
01:13:47,520 --> 01:13:49,158
Want to come?

243
01:14:40,280 --> 01:14:45,479
I was asleep in Mecca when the ceiling
opened and Gabriel descended into my room.

244
01:14:45,720 --> 01:14:48,792
He pulled out my heart
and washed it with holy water.

245
01:14:49,000 --> 01:14:52,675
Then he brought a bowl filled
with wisdom and faith.

246
01:14:52,880 --> 01:14:56,156
He filled my heart with those
and returned it in place.

247
01:14:56,640 --> 01:15:00,235
He brought a fantastic white beast
called "Burak".

248
01:15:00,440 --> 01:15:03,273
Smaller than a mule
but larger than a donkey.

249
01:15:03,480 --> 01:15:06,438
The strides of this animal

250
01:15:06,680 --> 01:15:08,796
are as long as the eye can see.

251
01:15:09,000 --> 01:15:10,592
I was mounted on this animal.

252
01:15:10,800 --> 01:15:15,396
Gabriel held my hand and took me
to the first level of heaven.

253
01:15:15,640 --> 01:15:19,189
When we arrived, Gabriel said
to the guard: "Open the gate!"

254
01:15:19,400 --> 01:15:22,836
The guard asked: "Who is it?"

255
01:15:23,040 --> 01:15:24,393
Gabriel answered: "Gabriel!"

256
01:15:24,640 --> 01:15:27,359
The guard asked: "Is anybody with you?"

257
01:15:27,600 --> 01:15:31,354
"Yes, Mohammed is with me!"
Gabriel said.

258
01:15:31,600 --> 01:15:36,037
The guard: "Was he dispatched?
Is it time for him to ascend to heaven?"

259
01:15:36,200 --> 01:15:38,475
"Yes," said Gabriel.

260
01:15:38,720 --> 01:15:40,472
And the guard replied:

261
01:15:40,720 --> 01:15:44,793
"Then may he be welcome.
What a nice arrival this is!"

262
01:15:45,160 --> 01:15:48,038
He opened the gate and we ascended
into the first level of heaven.

263
01:15:48,200 --> 01:15:53,035
I saw a man. At his sides,
there were many people.

264
01:15:53,240 --> 01:15:55,071
When he looked to his right,
he laughed.

265
01:15:55,280 --> 01:15:57,748
When he looked to his left,
he cried.

266
01:15:57,960 --> 01:16:02,795
He said to me,
"Welcome, mighty prophet."

267
01:16:03,000 --> 01:16:05,389
I asked Gabriel: "Who is this?"

268
01:16:05,640 --> 01:16:09,030
"Greet your ancestor Adam."

269
01:16:09,200 --> 01:16:12,431
So I greeted him, as he did me.

270
01:16:12,680 --> 01:16:14,750
Gabriel told me that Adam...

271
01:16:14,960 --> 01:16:17,110
was surrounded by the souls
of his descendants.

272
01:16:17,320 --> 01:16:19,595
Then Gabriel took me to
the second level of heaven.

273
01:16:19,880 --> 01:16:22,348
He requested for the gate to be opened.

274
01:16:22,600 --> 01:16:28,311
In this second level, I saw
the prophets Jesus and John.

275
01:16:28,520 --> 01:16:33,878
Gabriel said: "Greet Jesus and John."

276
01:16:35,680 --> 01:16:39,468
In the third level, I met Joseph.
In the fourth, the prophet Enoch.

277
01:16:39,720 --> 01:16:42,632
In the fifth level, I met prophet Aaron.

278
01:16:42,920 --> 01:16:46,879
I greeted them and they said to me:

279
01:16:47,080 --> 01:16:51,073
"Welcome pious brother,
welcome mighty Prophet."

280
01:16:52,360 --> 01:16:54,078
They brought me a bowl of wine,

281
01:16:54,280 --> 01:16:56,953
a bowl of milk,
and a bowl of honey.

282
01:16:57,120 --> 01:16:59,031
I took the bowl of milk.

283
01:20:49,160 --> 01:20:51,037
How are you, Yusuf?

284
01:20:53,440 --> 01:20:55,715
Send my regards to your dad.

285
01:21:25,000 --> 01:21:27,434
Come on, Yusuf, put this on.

286
01:21:32,320 --> 01:21:35,630
I'll let you know, mother.

287
01:21:58,160 --> 01:22:01,277
Auntie Ayse, watch him a moment.

288
01:22:09,480 --> 01:22:14,793
Brother Kemal, have you seen Yakup?
- No, but maybe he's over there.

289
01:22:15,000 --> 01:22:16,831
Up there.

290
01:28:33,360 --> 01:28:35,828
Yusuf, go on to school.

291
01:29:15,800 --> 01:29:17,870
Read, Yusuf.

292
01:29:44,440 --> 01:29:47,637
T-t-the L-l-l-l-io-o-o-n...

293
01:29:55,400 --> 01:29:58,756
a-a-nd t-t-the M-m-mouse...

294
01:30:43,680 --> 01:30:48,037
Fine, Yusuf. Well done.
Clap for your friend.

295
01:30:48,600 --> 01:30:50,795
Come here, Yusuf.

296
01:31:39,880 --> 01:31:42,758
He had an accident in the Karaucurum.

297
01:31:42,960 --> 01:31:44,757
We're going to take his body soon.

298
01:31:46,200 --> 01:31:47,918
May the Lord give you strength.

299
01:31:48,120 --> 01:31:50,031
There is nothing we can do.

300
01:31:57,640 --> 01:31:59,631
May the Lord give you strength.

