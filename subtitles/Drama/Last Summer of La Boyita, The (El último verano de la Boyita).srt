1
00:00:01,000 --> 00:00:04,074


2
00:03:52,256 --> 00:03:55,359
My wife's going to pay
at the association tomorrow.

3
00:03:55,359 --> 00:03:57,384
These 50 are for you.

4
00:03:59,064 --> 00:04:00,899
You sing just like him.

5
00:04:00,899 --> 00:04:04,198
If you need anything ask the waiters.

6
00:04:18,053 --> 00:04:20,920
Bocha, get something to eat.

7
00:05:50,959 --> 00:05:53,086
I have an important show
next Friday.

8
00:05:55,363 --> 00:05:57,661
We've been rehearsing a lot.

9
00:05:59,268 --> 00:06:01,429
Do you want to come?

10
00:06:09,646 --> 00:06:12,615
I'm playing "Unchained
Melody", alone on the piano.

11
00:06:14,219 --> 00:06:17,245
I'll talk to your mom
if you like.

12
00:06:25,365 --> 00:06:26,423
We're here.

13
00:06:36,177 --> 00:06:38,645
Lisa! Lisa,
come here.

14
00:06:40,148 --> 00:06:42,946
Remember to keep
a level head.

15
00:06:44,419 --> 00:06:45,647
Bye beautiful.

16
00:07:11,250 --> 00:07:12,217
Hey, Gutierrez!

17
00:07:13,385 --> 00:07:14,181
Gutierrez!

18
00:07:14,753 --> 00:07:15,754
Hey, Gutierrez!

19
00:07:15,754 --> 00:07:18,023
You're listening
to concerts all day long.

20
00:07:18,024 --> 00:07:20,857
2 hours at a burner, man.
Get on with it. Come on!

21
00:08:12,286 --> 00:08:14,254
I'm yours, baby!

22
00:08:44,157 --> 00:08:45,522
Dollars, right?

23
00:08:48,996 --> 00:08:50,691
How much does Business cost?

24
00:08:52,866 --> 00:08:53,628
Hi there!

25
00:08:54,701 --> 00:08:56,862
Economy then.
What time does it depart?

26
00:08:59,407 --> 00:09:00,772
August 14th.

27
00:09:02,411 --> 00:09:03,503
Perfect.

28
00:09:12,088 --> 00:09:13,020
Elvis.

29
00:09:13,957 --> 00:09:15,389
- Great to see you.
- Freddie.

30
00:09:16,826 --> 00:09:18,487
Come in and have a drink.

31
00:09:21,865 --> 00:09:23,696
- Britney! Hi.
- Hi.

32
00:09:50,798 --> 00:09:53,358
- Which one's ham and cheese?
- No idea.

33
00:10:19,698 --> 00:10:24,499
The son of a bitch is making it
by the barrel. Look at this house.

34
00:10:26,640 --> 00:10:28,471
They don't call me anymore.

35
00:10:29,410 --> 00:10:31,901
You're lucky,
you're back again.

36
00:10:36,118 --> 00:10:37,915
I invented rock and roll.

37
00:10:39,655 --> 00:10:41,623
I was never gone.

38
00:10:50,567 --> 00:10:52,364
You want me to blow you?

39
00:11:07,486 --> 00:11:09,283
Why are you like that?

40
00:11:11,291 --> 00:11:12,656
Like what?

41
00:11:14,027 --> 00:11:15,722
Like the guy on the TV.

42
00:11:19,534 --> 00:11:21,968
'Cos God gave me his voice.

43
00:11:23,237 --> 00:11:25,067
I just had to accept it.

44
00:11:41,524 --> 00:11:43,685
Listen to what he says.

45
00:11:47,197 --> 00:11:48,994
Amazing, isn't it?

46
00:11:50,435 --> 00:11:52,562
I don't know English.

47
00:11:55,040 --> 00:11:58,009
He says women've scratched
and bitten him.

48
00:11:58,745 --> 00:12:01,270
But he knows they don't
want to hurt him.

49
00:12:03,717 --> 00:12:06,117
They just want a piece of me.

50
00:12:20,035 --> 00:12:21,002
Guys.

51
00:12:47,332 --> 00:12:50,768
No I don't think so.
OK. Bye.

52
00:12:52,506 --> 00:12:53,841
Yes, can I help you?

53
00:12:53,841 --> 00:12:55,809
- I'm here to get paid.
- Name?

54
00:12:58,080 --> 00:12:59,547
What's your name?

55
00:12:59,781 --> 00:13:02,272
- I'm under Carlos Gutierrez.
- Gutierrez...

56
00:13:03,719 --> 00:13:08,281
Julio... Maria...
Carlos, here you are. 700 pesos.

57
00:13:09,826 --> 00:13:11,794
They owe me shows for
the last 4 months.

58
00:13:11,795 --> 00:13:13,092
That's what I have.

59
00:13:13,697 --> 00:13:14,959
700...

60
00:13:17,234 --> 00:13:18,963
Your lunch ticket.

61
00:13:34,954 --> 00:13:35,821
Who is it?

62
00:13:35,822 --> 00:13:36,914
<i>Elvis.</i>

63
00:13:39,926 --> 00:13:41,359
- Hi
- Hi

64
00:13:41,995 --> 00:13:43,019
<i>Mom!</i>

65
00:13:43,865 --> 00:13:44,854
I'm coming.

66
00:13:45,766 --> 00:13:47,165
I brought you this.

67
00:13:48,536 --> 00:13:50,003
Thank you.

68
00:13:51,572 --> 00:13:52,840
Are you coming in?

69
00:13:52,840 --> 00:13:53,807
Okay

70
00:13:57,179 --> 00:13:58,407
What's that?

71
00:13:58,948 --> 00:14:00,283
It' s for Lisa.

72
00:14:00,283 --> 00:14:01,272
<i>Lisa!</i>

73
00:14:04,755 --> 00:14:07,223
- Do you want to eat?
- Okay.

74
00:14:10,661 --> 00:14:12,128
You're getting fat.

75
00:14:12,664 --> 00:14:15,132
I'm trying to put on weight.

76
00:14:16,935 --> 00:14:18,903
- What's that?
- How about "Hello"?

77
00:14:19,304 --> 00:14:20,271
Hello

78
00:14:21,374 --> 00:14:22,363
It's for you.

79
00:14:24,043 --> 00:14:25,510
It's black.

80
00:14:26,746 --> 00:14:28,714
Didn't they have
another colour?

81
00:14:29,383 --> 00:14:30,714
See what it does.

82
00:14:31,586 --> 00:14:33,554
- It doesn't do anything.
- Wait.

83
00:14:35,089 --> 00:14:37,057
It doesn't do anything.

84
00:14:40,062 --> 00:14:41,393
<i>ELVIS.</i>

85
00:14:43,133 --> 00:14:44,600
I taught it.

86
00:14:46,636 --> 00:14:49,264
Take it off the table,
I'm going to set the plates.

87
00:15:01,720 --> 00:15:03,551
Lisa got chosen
for the school choir.

88
00:15:05,225 --> 00:15:07,716
Yeah, Lorena too.

89
00:15:08,328 --> 00:15:10,796
We're going to sing
at the school concert.

90
00:15:13,500 --> 00:15:14,728
I don't know...

91
00:15:15,869 --> 00:15:18,337
...the world of music
is very tough.

92
00:15:18,872 --> 00:15:21,272
It's the school choir, Carlos,
she's not going on tour.

93
00:15:22,377 --> 00:15:25,244
I'm just saying that
it's a tough business.

94
00:15:29,218 --> 00:15:30,981
Do you want to go
or not, love?

95
00:15:31,622 --> 00:15:34,557
Yes, I want to be with the girls.

96
00:15:35,792 --> 00:15:37,760
Well, we 'll talk later.

97
00:15:40,798 --> 00:15:41,764
Lisa...

98
00:15:45,102 --> 00:15:48,071
If music is what you want,
I will help you, OK?

99
00:15:52,443 --> 00:15:53,535
<i>ELVIS.</i>

100
00:15:59,619 --> 00:16:00,854
<i>Why are you looking
at me like that?</i>

101
00:16:00,854 --> 00:16:03,390
<i>Many young artists
can't take the pressure.</i>

102
00:16:03,390 --> 00:16:04,357
What pressure?

103
00:16:05,026 --> 00:16:07,494
- You'll understand me one day.
- Lower your voice!

104
00:16:09,196 --> 00:16:11,994
You take her to school once,
and you think you're a hero.

105
00:16:12,634 --> 00:16:14,465
And then you bring
that fucking bird.

106
00:16:15,437 --> 00:16:17,268
Who' s going to buy
the food for the birdy?

107
00:16:17,606 --> 00:16:19,267
It's a mate for Lisa.

108
00:16:19,841 --> 00:16:21,468
Don't get jealous.

109
00:16:21,777 --> 00:16:24,007
Damn the night I met you.

110
00:16:26,216 --> 00:16:28,047
Can't you act like a father,
for once?

111
00:16:30,387 --> 00:16:32,756
Priscilla, you'll always
love me.

112
00:16:32,757 --> 00:16:34,588
Get off Carlos!
What are you doing?

113
00:16:35,126 --> 00:16:37,822
- Don't call me Carlos?
- What's your name, then?

114
00:16:38,931 --> 00:16:40,899
Sometimes I think
you want to drive her nuts.

115
00:16:42,702 --> 00:16:44,966
Get out. Just get out.

116
00:16:45,838 --> 00:16:47,772
- What is it?
- Get out of here.

117
00:16:48,475 --> 00:16:49,203
Go!

118
00:16:54,181 --> 00:16:55,113
<i>Go!</i>

119
00:17:25,017 --> 00:17:27,485
Carlos, can I borrow
some deodorant?

120
00:17:33,893 --> 00:17:34,860
Thank you.

121
00:18:47,845 --> 00:18:50,814
To Sarah, please

122
00:19:06,999 --> 00:19:08,557
<i>Come on Sarah.</i>

123
00:20:23,587 --> 00:20:25,111
Do you hear me, ma?

124
00:20:29,927 --> 00:20:31,622
I'm about to do something big,

125
00:20:33,799 --> 00:20:34,925
what I always dreamed of.

126
00:20:44,544 --> 00:20:47,240
I want you to know that
it's thanks to you and dad.

127
00:20:53,521 --> 00:20:55,648
You will be proud of me.

128
00:21:01,596 --> 00:21:03,359
You're going to see me
make it.

129
00:21:45,781 --> 00:21:47,942
Lydia

130
00:21:49,318 --> 00:21:50,586
Here.

131
00:21:50,586 --> 00:21:54,249
This is for a few months.

132
00:21:54,924 --> 00:21:57,916
I don' t want her
needing anything. Bye.

133
00:22:10,341 --> 00:22:11,342
What is it, Gutierrez?

134
00:22:11,342 --> 00:22:13,912
Here. Keep these.

135
00:22:13,913 --> 00:22:16,006
- They'll be worth millions.
- What?

136
00:22:17,717 --> 00:22:19,719
Where are you going?

137
00:22:19,719 --> 00:22:21,414
He spoke in English.
Did you hear that?

138
00:23:52,792 --> 00:23:54,657
Can you give me the eyeliner?

139
00:23:55,262 --> 00:23:57,093
This one is OK, it works.

140
00:23:57,531 --> 00:24:01,627
- Spray? You have some?
- No, I have mousse.

141
00:24:02,270 --> 00:24:03,430
It works for you?

142
00:24:14,750 --> 00:24:16,581
<i>It's really cold in here.</i>

143
00:24:17,086 --> 00:24:18,518
<i>Yeah, I'm shivering.</i>

144
00:24:23,793 --> 00:24:25,261
<i>How many people came?</i>

145
00:24:25,262 --> 00:24:28,698
<i>Not much.
But they usually come late.</i>

146
00:26:42,085 --> 00:26:44,713
Have you ever felt that
you've done everything,

147
00:26:45,789 --> 00:26:48,349
that you've reached
your goals?

148
00:26:50,694 --> 00:26:51,820
No.

149
00:26:55,767 --> 00:26:58,327
Still, I got a promotion
at the supermarket.

150
00:27:05,812 --> 00:27:08,280
I don't think you're good for her,
Carlos.

151
00:27:10,584 --> 00:27:13,087
Lisa needs somebody who talks
to her, who understands her.

152
00:27:13,087 --> 00:27:15,289
- Not your silly songs.
- What do you mean silly songs?

153
00:27:15,289 --> 00:27:16,756
Wait. Listen to me.

154
00:27:17,726 --> 00:27:22,629
A  friend at work got me a lawyer
and I'm asking for custody.

155
00:27:25,267 --> 00:27:27,132
It'll all be easier.

156
00:27:31,742 --> 00:27:34,609
You can visit her  for Christmas
and birthdays and stuff.

157
00:27:37,415 --> 00:27:39,280
I think it's best for Lisa.

158
00:27:43,155 --> 00:27:45,248
We can't take this anymore.

159
00:27:47,326 --> 00:27:49,089
I'm fed up.

160
00:28:50,364 --> 00:28:56,166
Good. Watch the chorus,
the song has to be perfect.

161
00:28:58,674 --> 00:29:01,810
- This show is very important.

162
00:29:01,811 --> 00:29:04,514
<i>- Hello. Just a moment.</i>

163
00:29:04,513 --> 00:29:06,242
It has to be like the last one.

164
00:29:06,783 --> 00:29:08,080
I want to sing Unchained Melody.

165
00:29:08,418 --> 00:29:10,287
- Elvis.
- Not now, I'm rehearsing.

166
00:29:10,288 --> 00:29:12,188
It's for you.
They say it's important.

167
00:29:14,725 --> 00:29:15,692
Hello.

168
00:29:20,599 --> 00:29:21,429
What?

169
00:29:32,579 --> 00:29:34,342
I'm looking for my wife
and daughter.

170
00:29:40,155 --> 00:29:42,350
Excuse me, excuse me!

171
00:29:43,458 --> 00:29:46,427
My wife and daughter had an accident.
They told me to come here.

172
00:29:48,331 --> 00:29:50,166
Name and surname?

173
00:29:50,167 --> 00:29:53,830
Alejandra Olemberg,
Lisa Marie Gutierrez.

174
00:30:01,613 --> 00:30:03,444
No, I can't.

175
00:30:03,849 --> 00:30:05,544
I said no!

176
00:30:05,818 --> 00:30:07,253
I can't play.

177
00:30:07,253 --> 00:30:09,187
Who's with Alejandra Olemberg?

178
00:30:13,459 --> 00:30:14,827
Your wife had an accident.

179
00:30:14,828 --> 00:30:17,564
She suffered cranial trauma
and she's unconscious.

180
00:30:17,565 --> 00:30:20,534
We're running a brain scan to
see if there is any damage.

181
00:30:21,202 --> 00:30:25,332
- Are you sure it's my wife?
- Alejandra Olemberg.

182
00:30:26,708 --> 00:30:27,902
And my daughter?

183
00:30:28,343 --> 00:30:29,611
The girl is fine.

184
00:30:29,611 --> 00:30:32,414
Doctors are assessing her condition
in the emergency ward.

185
00:30:32,414 --> 00:30:34,882
- Where's that?
- Down the hall.

186
00:31:27,310 --> 00:31:28,777
She is ready.

187
00:31:52,438 --> 00:31:53,666
Lisa...

188
00:31:56,610 --> 00:31:57,838
...shall we go?

189
00:32:03,651 --> 00:32:05,050
Where?

190
00:32:05,587 --> 00:32:06,747
You're coming with me.

191
00:32:30,616 --> 00:32:32,117
Isn't mom coming?

192
00:32:32,117 --> 00:32:34,551
No. She's staying
in the hospital for a while.

193
00:32:34,888 --> 00:32:36,856
Let's go get the blackbird?

194
00:32:37,056 --> 00:32:39,524
We gave it to a neighbor.

195
00:33:04,687 --> 00:33:07,656
- What are they?
- Banana and peanut butter.

196
00:33:23,509 --> 00:33:28,105
- I don't like it. It tastes funny.
- Lisa Marie loved them.

197
00:33:36,924 --> 00:33:39,392
Is there anything else on?

198
00:33:45,001 --> 00:33:47,697
I have another concert
if you like.

199
00:33:58,048 --> 00:33:59,072
It hurts.

200
00:34:00,150 --> 00:34:03,608
The doctor said to give me
the medicine every 4 hours.

201
00:34:05,524 --> 00:34:06,786
It's been 7 already.

202
00:34:11,298 --> 00:34:14,233
- Where are they?
- In my backpack.

203
00:34:20,942 --> 00:34:22,910
It's in your room.

204
00:34:27,816 --> 00:34:29,283
Where?

205
00:34:32,087 --> 00:34:35,352
Look around. But
don't mess up my stuff.

206
00:35:22,511 --> 00:35:23,535
Dad.

207
00:35:26,316 --> 00:35:27,340
Dad.

208
00:35:34,191 --> 00:35:35,419
I can't sleep.

209
00:35:40,732 --> 00:35:42,222
What is it?

210
00:36:04,493 --> 00:36:05,960
Is mom going to die?

211
00:36:08,531 --> 00:36:09,998
What?

212
00:36:10,566 --> 00:36:11,965
Is mom going to die?

213
00:36:15,806 --> 00:36:16,830
I don't know.

214
00:36:22,113 --> 00:36:23,137
I'm scared.

215
00:36:39,399 --> 00:36:40,423
Me too.

216
00:36:43,271 --> 00:36:47,230
It' s been 48 hours. She's stable,
but still not responding.

217
00:38:12,038 --> 00:38:15,530
There's no toilet,
but I peed anyway.

218
00:38:23,384 --> 00:38:24,681
What did the doctor say?

219
00:38:25,554 --> 00:38:26,987
They don't know yet.

220
00:38:42,140 --> 00:38:43,732
Can you play pool?

221
00:38:44,642 --> 00:38:46,837
- Yes. Can you?
- No.

222
00:38:49,548 --> 00:38:51,072
D'you want me to teach you?

223
00:38:53,220 --> 00:38:54,084
Sure.

224
00:38:54,920 --> 00:38:57,650
Lean on this, and you guide it
with these 2 fingers.

225
00:38:58,525 --> 00:39:01,619
You have to hit the ball
into these holes.

226
00:39:05,633 --> 00:39:09,069
The colored or striped one.
Now move, I'm starting.

227
00:39:13,609 --> 00:39:17,238
Hit it with all your
strength. There.

228
00:39:18,582 --> 00:39:21,379
Now you do it.

229
00:39:24,387 --> 00:39:27,356
You've got to aim
at the white one,

230
00:39:28,959 --> 00:39:33,931
and make the cue stick
go straight for the hole.

231
00:39:33,932 --> 00:39:35,627
No, no.

232
00:39:36,001 --> 00:39:37,969
- How?
- Watch me do it.

233
00:39:44,409 --> 00:39:45,603
<i>Great.</i>

234
00:39:48,180 --> 00:39:49,841
<i>755.</i>

235
00:39:50,984 --> 00:39:53,145
7-5-5.

236
00:39:53,820 --> 00:39:56,152
<i>Anyone has that number?</i>

237
00:39:57,691 --> 00:40:01,650
<i>You must participate to win.</i>

238
00:40:03,965 --> 00:40:07,264
<i>959.</i>

239
00:40:07,637 --> 00:40:10,231
Stay here.
I'm going to play.

240
00:41:01,366 --> 00:41:03,634
I can't hear a thing.

241
00:41:03,634 --> 00:41:05,403
- This damn sound, this damn club.

242
00:41:05,403 --> 00:41:06,427
<i>- Come on Sandro!</i>

243
00:41:07,238 --> 00:41:09,798
What's your problem?
You come up here and sing.

244
00:41:13,679 --> 00:41:15,414
Lady, what are you laughing at?

245
00:41:15,415 --> 00:41:17,144
Fuck you lady and your
fucking hairdo.

246
00:41:17,350 --> 00:41:18,585
And fuck this damn club!

247
00:41:18,585 --> 00:41:19,643
Shit.

248
00:41:26,293 --> 00:41:28,062
Open the door!

249
00:41:28,062 --> 00:41:30,621
What's the matter? Are you nuts?
Don't do this.

250
00:41:38,507 --> 00:41:40,372
<i>Come on, chubs.</i>

251
00:41:42,010 --> 00:41:45,377
<i>Listen. They're all waiting
out there. Come on.</i>

252
00:41:46,617 --> 00:41:48,847
<i>Come on, asshole.
They're waiting for you.</i>

253
00:41:49,052 --> 00:41:50,610
<i>What's wrong with you?
Who do you think you are?</i>

254
00:41:52,456 --> 00:41:54,447
<i>Come on,
open the door.</i>

255
00:42:00,131 --> 00:42:03,464
Do you hear me?
Come out!

256
00:42:06,705 --> 00:42:10,835
Take it easy guys.
The show must go on.

257
00:42:32,368 --> 00:42:34,734
Fucking hell!

258
00:42:51,157 --> 00:42:52,784
Where were you?

259
00:42:55,328 --> 00:42:57,262
I had something to do.

260
00:42:59,766 --> 00:43:03,463
- I thought you'd gone.
- No.

261
00:43:04,238 --> 00:43:05,865
Why did you get mad?

262
00:43:06,207 --> 00:43:08,107
I didn't get mad.

263
00:43:33,572 --> 00:43:36,097
Yes honey bun, all yours.

264
00:43:36,508 --> 00:43:38,975
Yes, all yours.

265
00:43:39,545 --> 00:43:41,280
Sure, for you of course,

266
00:43:41,280 --> 00:43:44,443
I've got to go. Bye.

267
00:43:45,218 --> 00:43:48,153
I need to get paid.
It's 7 shows.

268
00:43:48,355 --> 00:43:50,380
We don't make payments here.

269
00:43:50,591 --> 00:43:52,115
You got to stand in line,
like everyone else.

270
00:43:52,493 --> 00:43:54,290
You have to pay me now.

271
00:43:55,096 --> 00:43:57,499
We haven't been paying
in the last 20 days...

272
00:43:57,499 --> 00:43:59,701
...cos we're fixing
the building.

273
00:43:59,701 --> 00:44:02,329
I have my daughter, and
Priscilla in hospital.

274
00:44:03,038 --> 00:44:04,072
You have to pay me.

275
00:44:04,072 --> 00:44:06,700
I'm sorry. You'll have to wait
like everyone else.

276
00:44:09,345 --> 00:44:13,182
Call before coming out here.
Talk to the girls first.

277
00:44:13,183 --> 00:44:14,480
You have the number, don't you?

278
00:44:16,821 --> 00:44:20,154
- Do you know who I am?
- No. Who are you?

279
00:44:20,525 --> 00:44:23,892
- I'm Elvis.
- Elvis, from where?

280
00:44:24,930 --> 00:44:26,557
From Memphis, asshole.

281
00:44:27,099 --> 00:44:28,667
I'm going to have this place
burned down.

282
00:44:28,668 --> 00:44:31,865
Get out. You know how many
Elvises like you I have?

283
00:44:33,506 --> 00:44:35,736
- Which do you want?
- This one.

284
00:44:49,692 --> 00:44:51,887
- You want an olive?
- Yeah.

285
00:44:56,165 --> 00:44:58,735
Stay here for a while,
I have something to do.

286
00:44:58,736 --> 00:45:00,471
<i>- And when will you be back?</i>

287
00:45:00,471 --> 00:45:01,802
- Before the show.

288
00:45:20,294 --> 00:45:22,762
Please. I need my job back.

289
00:48:33,280 --> 00:48:34,882
What happened?
You left me a message.

290
00:48:34,882 --> 00:48:37,112
Your wife came to and
she's stable.

291
00:48:37,484 --> 00:48:39,586
We're keeping her here
for another 24 hours,

292
00:48:39,586 --> 00:48:42,189
and if she's okay,
we're moving her to a ward.

293
00:48:42,190 --> 00:48:43,452
- Will she be all right?
- Yes.

294
00:48:49,099 --> 00:48:50,361
Can I see her?

295
00:48:51,001 --> 00:48:53,868
In a bit. She has to rest
for a while longer.

296
00:49:27,976 --> 00:49:29,944
It hurts.

297
00:49:34,283 --> 00:49:38,014
Don't worry Priscilla.
They said you're going to be fine.

298
00:49:42,593 --> 00:49:46,263
Do you know how long
you have to stay?

299
00:49:46,264 --> 00:49:47,731
No. I don't.

300
00:49:52,972 --> 00:49:56,373
- How's Lisa?
- She's fine.

301
00:49:57,209 --> 00:50:02,841
I took her home a few days.
Now she's with my mom.

302
00:50:04,619 --> 00:50:06,313
With your mom?

303
00:50:17,598 --> 00:50:21,261
I was passing a corner.

304
00:50:24,506 --> 00:50:27,669
And I just can't remember
what happened.

305
00:50:38,689 --> 00:50:42,216
I didn't put her safety
belt on.

306
00:50:48,434 --> 00:50:50,800
I didn't put it on.

307
00:51:04,552 --> 00:51:07,077
I'm sorry.

308
00:51:09,258 --> 00:51:10,725
I'm sorry.

309
00:51:18,169 --> 00:51:24,369
Tell Lisa I'm sorry.
That I miss her.

310
00:51:26,577 --> 00:51:29,375
And I love her very much.

311
00:51:33,018 --> 00:51:35,187
- And when will she be back?
- In a few days.

312
00:51:35,188 --> 00:51:38,453
But now she's better.
She says she misses you.

313
00:51:39,726 --> 00:51:41,717
Don't you want to come, Lydia?

314
00:51:43,698 --> 00:51:45,563
I'm wearing the blue suit.

315
00:52:53,510 --> 00:52:57,514
What are you doing?

316
00:52:57,514 --> 00:52:59,539
Nothing.

317
00:53:02,653 --> 00:53:06,350
You can sleep a little more
if you like. It's early.

318
00:53:09,929 --> 00:53:12,056
I have a present for you.

319
00:53:14,401 --> 00:53:16,028
What is it?

320
00:53:22,944 --> 00:53:24,070
And this?

321
00:53:26,113 --> 00:53:27,671
Where did you get it?

322
00:53:28,250 --> 00:53:29,808
A friend at school
gave it to me.

323
00:53:30,219 --> 00:53:31,777
His dad had it.

324
00:53:37,694 --> 00:53:39,457
Thanks, beautiful.

325
00:53:41,164 --> 00:53:42,426
We leave it here.

326
00:53:45,737 --> 00:53:48,001
They made me quite
thin, don't you think?

327
00:53:52,144 --> 00:53:53,839
How's the choir doing?

328
00:53:54,746 --> 00:53:57,442
I don't go anymore.
I got bored.

329
00:53:57,851 --> 00:53:59,614
Don't worry Lisa,

330
00:54:02,122 --> 00:54:04,317
you'll find something that you like.

331
00:54:05,159 --> 00:54:07,228
You were bom with a gift,
like me.

332
00:54:07,228 --> 00:54:08,563
You just have to find it.

333
00:54:08,563 --> 00:54:12,795
- Does mom have a gift too?
- No, not everyone has it.

334
00:54:13,535 --> 00:54:15,935
I don't think she can stay here.

335
00:54:16,238 --> 00:54:20,766
- Please, I couldn' t stay yesterday.
- Okay, just for a bit.

336
00:54:22,512 --> 00:54:25,742
- Can you fetch her later?
- Right.

337
00:54:26,783 --> 00:54:28,752
What happened with the car?

338
00:54:28,752 --> 00:54:31,812
I almost had to pay for them
to keep it.

339
00:56:16,876 --> 00:56:20,209
That's enough.
I have to sleep.

340
00:57:36,533 --> 00:57:39,696
You should have benefits
for artists.

341
00:57:40,871 --> 00:57:42,998
Some VIP room.

342
00:57:43,608 --> 00:57:46,076
I was out there waiting
for an hour.

343
00:57:59,292 --> 00:58:01,726
Can I go to the show tonight?

344
00:58:01,829 --> 00:58:05,560
Now your mother's out,
you have to stay with her.

345
00:58:11,707 --> 00:58:13,800
Why did you sell the TV?

346
00:58:14,377 --> 00:58:17,073
'Cos I'm going on tour
in a few days.

347
00:58:18,948 --> 00:58:21,075
It's very important for me.

348
00:58:24,855 --> 00:58:29,758
- But, when will you be back?
- I don't know.

349
00:58:33,164 --> 00:58:35,359
But you'll be fine, baby.

350
00:58:52,419 --> 00:58:54,751
I'll miss you.

351
00:58:54,888 --> 00:58:56,617
Me too.

352
00:59:04,733 --> 00:59:06,335
When you really believe
in something

353
00:59:06,335 --> 00:59:08,200
you have to pursue it
'til the end.

354
00:59:09,305 --> 00:59:11,205
Those who don't
are unhappy.

355
00:59:17,680 --> 00:59:19,875
I want you to be proud
of your father.

356
00:59:27,259 --> 00:59:32,219
My boss told me they're going
to pay me the months I couldn't work.

357
00:59:32,432 --> 00:59:34,195
Great.

358
00:59:36,169 --> 00:59:39,764
Ma, can we make
some banana sandwiches?

359
00:59:40,240 --> 00:59:41,229
<i>Gross!</i>

360
00:59:56,092 --> 00:59:57,059
Bye, precious

361
00:59:59,597 --> 01:00:00,564
Bye.

362
01:00:01,464 --> 01:00:02,931
Let's go honey.

363
01:01:41,178 --> 01:01:43,476
I want to hire a limo.

364
01:01:43,982 --> 01:01:45,449
White.

365
01:01:46,785 --> 01:01:48,252
For tomorrow.

366
01:01:52,258 --> 01:01:53,993
Elvis...

367
01:01:53,993 --> 01:01:55,324
Presley.

368
01:01:56,000 --> 01:01:59,060

