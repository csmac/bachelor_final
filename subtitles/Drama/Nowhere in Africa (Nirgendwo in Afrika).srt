{1}{1}23.976
{1}{80}Good idea. You are his friend.
{216}{291}Sohrau, October 2nd 1940.
{295}{381}My dear son, It�s nice to hear,
{382}{482}that you had luck with your 2nd farm.
{483}{583}Emigrating has become|impossible. Pretty soon
{584}{696}Hitler will close the borders.|There�s nothing left we could sell.
{697}{797}People say that the Jews|will be banned in ghettos.
{798}{938}In this case Greschek will|send you our new address.
{939}{1045}Walter, for the first time|in my life I am worried!
{1066}{1188}Worried... especially about Liesel|but as well about my own life
{1237}{1348}In Germany we feel|like on a lonely island.
{1349}{1499}Nobody dares talking. It�s horrible.
{1500}{1626}My son. I send you this letter|and the rose seeds you wanted.
{1627}{1727}May these seeds grow|in the African earth
{1728}{1840}to bring Jettl Regina|and you a lot of joy.
{1866}{1936}I always think of you! Your father.
{2016}{2064}They have made a big journey.
{2071}{2141}Yes, a big journey.
{2276}{2377}I want to enrol Regina at|school as soon as possible.
{3143}{3193}Owuor?
{3231}{3281}Bwana!
{3287}{3351}Did you learn how to cook?
{3366}{3437}Owuor!
{3482}{3508}You found us!
{3509}{3577}Memsaab doesn't have|a cook in Ol Joro Orok?
{3578}{3616}No, we don't have a cook.
{3617}{3683}How do you know all|that? How did you find us?
{3721}{3763}Rummler has a good nose.
{3944}{4014}A bit of flour.
{4086}{4138}Thanks!
{4158}{4214}It's a young male and a young female.
{4216}{4279}They walked into the woods.
{4283}{4322}They are lost.
{4323}{4377}It's dangerous during the night.
{4385}{4415}That's an angel.
{4427}{4452}He lives in heaven.
{4454}{4478}And what's that?
{4479}{4506}Those are wings.
{4507}{4554}He needs those to fly in heaven.
{4555}{4600}Have you ever seen an angel?
{4607}{4661}No. You can't see angels.
{4662}{4709}You can't see your|dead relatives either.
{4710}{4756}Yes. Angels protect us too,
{4757}{4802}if we are in danger.
{4814}{4853}I'm an angel...
{4854}{4922}and scare off the bad ghosts!
{5373}{5419}Come here!
{5508}{5558}No!
{5683}{5723}Hmmm, kids.
{5900}{5941}Come over here!
{5961}{6004}Go away! Go away!
{6443}{6507}When you come back I|wont be a child anymore.
{6517}{6588}Bullshit. i'll be back|soon. Every vacation.
{6589}{6691}Next harvest I'll be back already.
{6987}{7030}Swear that you come back.
{7106}{7150}I swear.
{7768}{7843}I think that's him.
{8191}{8267}Study hard my dear!
{8283}{8416}Mum, I�ll be back on vacation!
{8417}{8459}Memmsaab dogo!
{8464}{8539}When I come back, I will|tell you everything, Owuor.
{9377}{9422}Come, let's go home.
{10375}{10487}We are Jewish. He said|that we should get out!
{11300}{11400}Quickly I realized what|it means to be an outsider.
{11606}{11706}I was happy to have Inge.
{11707}{11817}She didn�t like the|British games either.
{12204}{12304}I wonder how simple words|lose their meaning here.
{12305}{12407}For example tax return Or rrrailway�
{12426}{12526}Sometimes I wonder why I still|get dressed in the morning
{12527}{12631}I could just use a bag.|People wouldn�t care.
{12632}{12682}I do care!
{12703}{12742}Do you still think I am attractive?
{12743}{12789}Yes ofcourse I do.
{12963}{13063}Did you meet someone in the hotel?
{13064}{13113}Do you think people should|stay together their whole lives?
{13114}{13289}Probably not. Maybe we are just|unstable because of the situation...
{13359}{13419}Why? Do you want to leave?
{13467}{13591}Sometimes I think my|life could be happier.
{13812}{13913}I want to make it with you!
{13914}{13991}You are my wife. I love you!
{14053}{14130}I miss them all so much!
{14135}{14210}My mother, K�the...
{14229}{14304}It hurts so much!
{19161}{19211}Owuor!
{20315}{20375}And? Isnt't that beautiful Jogona?
{20376}{20435}You can't speak with the night.
{20436}{20462}You are right.
{20463}{20521}You can't speak with the night.
{20586}{20694}But the woman who wrote this poem|imagined that the night has ears.
{20696}{20784}It's beautiful to make music with words.
{20785}{20832}Your father is a smart man.
{20833}{20889}He speaks with thunder|and lightning too.
{20892}{20994}He speaks with ancestors.|That's something different.
{21104}{21205}You have to undress yourslef.|You clothes are getting dirty.
{21206}{21257}No I won't undress myself.
{21258}{21309}I'm not a little child anymore.
{21310}{21390}You are stupid if you|dirten your school clothes.
{21395}{21445}I'm not a child anymore.
{21447}{21495}You must not see my breast.
{21496}{21585}Your breasts arenn't different|to other women's breasts.
{21593}{21675}Of course. A Mzungus breast is|different than black women's breasts.
{21676}{21708}You must not show them.
{21709}{21818}Mzungus' schools teach strange stuff.
{21819}{21926}Are you even allowed to climb trees?
{22028}{22064}Yes.
{22067}{22183}But only if their|clothes dont get dirty.
{22373}{22418}Wait Jogona!
{23255}{23305}Dad!
{23309}{23380}Ma!
{23561}{23641}My dears. We are very excited.
{23654}{23757}Tomorrow we have to go to|work in Poland Don�t forget us.
{23758}{23867}Mother and K�the.
{23868}{23968}They are not allowed to|write more. 20 words only...
{23995}{24049}They used 19.
{24549}{24592}These aren't good days,
{24594}{24669}when the little Memsaab|has to go to school.
{24684}{24798}Perhaps they want to try|to escape through Poland.
{24871}{24971}Maybe they found a way.
{24989}{25103}Say something, Walter,|finally! Please talk to me!
{25173}{25321}Your mother wanted you to know it.|She wouldn�t have written if not.
{25322}{25398}Poland means death
{25399}{25499}Shall I tell you something? Sometimes|I envy you because of this letter!
{25500}{25600}Now you can be sure! I have|to ask myself every day,
{25601}{25688}if my father is fine. Where he is.
{25701}{25776}Where my sister is.
{26055}{26155}Every day the German army looses|more And more territory in the east.
{26156}{26256}A lot of people died.
{26257}{26320}Since the Russian winter|offensive began the
{26321}{26382}amount of killed, wounded|and German prisoners
{26383}{26481}increased over 400.000.
{26871}{26919}What's this?
{26920}{26984}This woman doesn't need your|help. She wants to die today.
{26985}{27052}And her family leaves her out here?
{27053}{27115}The coyotes will pick up|her corps during the night.
{27253}{27329}Kimani, I can't allow that.
{27367}{27414}I'm the Memsaab on this farm.
{27415}{27461}I want you guys to|carry her into the house.
{27474}{27550}If she dies in the house|it isn't clean anymore.
{27551}{27664}The family would have to make many|sacrifies to get it clean again.
{27665}{27727}That's very expensive.
{27728}{27787}But still. I order it!
{27788}{27880}As soon as she's in the house those|people will carry her out again!
{27912}{27954}This woman wants to die out here.
{27956}{27997}That's how tradition demands it.
{27998}{28036}It's ok Memsaab.
{28047}{28086}My mother won't die alone.
{28087}{28164}We're all with her. She isn't afraid.
{28769}{28869}From now on you can count your|days. They call it Operation J..
{28870}{28970}The reorganization of the African forces
{28971}{29071}They really take Germans?
{29072}{29197}They have a good Chance to get in.
{29198}{29301}Just don�t tell them that you|are bored and need something else.
{29302}{29426}They want to hear that you can�t|wait to fight for the right side.
{29526}{29661}And Jettel?
{29662}{29787}I can�t believe that I get|another chance in this war.
{29788}{29841}What�s up with you?
{29842}{29964}It�s not my war. I don�t|give a shit about Germany!
{30417}{30509}I won�t go to Nairobi with you.
{30510}{30610}I stay here with Owuor.|That�s my final decision.
{30611}{30766}For you alone it�s too dangerous.
{30879}{30936}You wanted to go to|Germany and Rongai! But you
{30937}{30992}never wanted to be there|where we actually were.
{30993}{31093}And now you don�t want to leave.
{31094}{31193}But you�d meet a lot of peolple.
{31194}{31294}I don�t want this anymore!
{31295}{31395}If something happens to you...
{31396}{31496}They are sending men to Burma!
{31598}{31732}I can�t hang out here|anymore. I�m feeling useless.
{31744}{31868}I understand that.
{31873}{31950}I stay here with Owuor.
{32426}{32476}Thank you!
{33889}{33936}Sometimes I don't see you a day...
{33938}{33983}and a night. Where are you then?
{33984}{34046}I'm visiting my wife and my children.
{34047}{34099}At the lake. Near Kisumu.
{34100}{34150}They dont see you often.
{34151}{34249}They understand that I|can't leave Memsaab alone.
{34479}{34536}But your wife is always alone.
{34541}{34586}That's something different.
{34589}{34655}White women are helpless,|black women aren't
{34703}{34742}How many kids do you have?
{34746}{34810}I have 6 kids and 3 wives.
{34824}{34862}3 wives?
{34863}{34942}That's a lot of work if you visit them!
{34947}{35043}I give them money.|- 12 Schillings is not much.
{35044}{35145}It's enough. They have their Schambas,
{35147}{35226}With corn and beens.|And fishes from the lake.
{35227}{35259}Do they respect you?
{35274}{35298}They respect me.
{35299}{35369}I'm working in the house of a Bwana.
{35517}{35581}I respect you too Owour.
{35582}{35676}Without you we would|be lost in this country.
{35713}{35754}Could you help me out?
{35827}{35892}I'm a cook not a barber.
{35926}{35972}Please! I can't do it alone.
{36939}{37044}How are your corn fields? Are they fine?
{37045}{37120}Yes they are
{37164}{37310}Do you want to do an excursion?
{37311}{37373}I�ll wait outside then
{37410}{37485}Nice that you�re here!
{37906}{37982}This country is beautiful!
{37983}{38083}The allies landed with|more than 300.000 men
{38084}{38191}in Normandy.
{38192}{38284}Bad for Hitler, so I�d say: good.
{38295}{38391}You don�t care what�s|going on in Germany?
{38394}{38460}And you?
{38461}{38597}What?
{38603}{38765}What do I do?
{39079}{39122}Are you already there?
{39124}{39166}I was worried about you.
{39243}{39343}Regina. I showed your|mother Lake Bagoria.
{39344}{39432}We were pretty close to your school.
{39570}{39683}What do you accuse me of?|That I wasn�t here once?
{39684}{39784}Why didn�t you go with dad?
{39785}{39884}You are the one who doesn't|want to leave the farm.
{39885}{39964}Bullshit. You don�t love him anymore.
{40021}{40137}S��kind made an excursion|with me. That�s it.
{40141}{40255}The other day in the hotel... In NORFOLK.|Do you think I didn�t realize that?
{40256}{40350}You started an affair with that soldier.
{40358}{40458}He arranged this farm We were homeless!
{40459}{40581}And what can S��kind do for|you if you sleep with him?
{40885}{40935}Regina!
{40955}{41005}Regina!
{41188}{41226}Did you see my child?
{41227}{41302}The Mzungu-Kid?
{41305}{41345}Over there in the house.
{41407}{41457}Regina?
{41682}{41732}Regina?
{41839}{41916}I�ll be right there mum.
{42406}{42507}I often slept at Jogona�s place.
{42508}{42608}You didn�t realize it because|I was always back at dawn.
{42609}{42743}My wild girl. You can�t just disappear!
{42777}{42880}It�s beautiful! I can understand|why you wanted to have it
{42881}{42981}Because of this my dad almost|sent me Back to Germany.
{42982}{43107}You didn�t wear it a single time?
{43209}{43334}We bought it at Wertheimm|in Breslau. For 44 Mark.
{43335}{43458}And then we ate a piece|of cake at Cafe Monheim.
{43459}{43558}I can�t even remember Germany.
{43559}{43673}Only if I eat nuts.|Then I think of grandpa.
{43754}{43853}Mamma... why are the Jews so hated?
{43854}{43953}You and dad, you are not|really Jewish. You eat meat.
{43954}{44098}And you don�t pray right?
{44139}{44274}In school they told us that|the Jews killed God�s son.
{44275}{44387}For me and dad Jewish|played an important role.
{44388}{44488}We thought we were as|much German as possible.
{44489}{44598}The German culture, the|language. That was our home.
{44599}{44699}Maybe we Jews are really different?
{44700}{44794}Aunt Ruth and uncle|Salommon are different.
{44800}{44924}They obey the Jewish rules
{44925}{45039}Tolerance doesn�t mean|that we are all equal.
{45043}{45205}If I learned something here, it's|how important differences are.
{45217}{45292}Differences are good, Regina.
{45293}{45419}Smart people wont bother you|Just because you are different.
{45420}{45526}Mum? Tonight we will celebrate Pokot
{45530}{45630}They slaughtered a lamb|under the holy tree.
{45631}{45714}There will be beer and|they will be singing.
{45741}{45843}You have to see that.|It�s pretty different!
{50632}{50716}The war in Germany is over.
{50917}{51029}A couple of days ago I received a|letter from a German teacher in Tarnopol.
{51030}{51080}From whom?
{51081}{51191}I don�t know this guy. He|was a teacher before the war.
{51234}{51315}He knew my father and Liesel.
{51484}{51612}Father gave him my adress.|One week before he died.
{51841}{51948}Father and Liesel hid|in a school�s cellar.
{51949}{52095}2 SS-men beat father to death|in the middle of the street.
{52143}{52243}One month later Liesel came to|Belsec. With the 3rd transport.
{52244}{52339}No one returned from that one.
{52404}{52513}She married during|the escape. A Tscheque
{52514}{52571}Erwin Schweiger.
{52576}{52651}He was a truck driver.
{54580}{54691}Wiesbaden, November 4th|1946. Dear Mr. Redlich!
{54692}{54796}We are glad to let you know
{54797}{54922}that we can offer you a job.
{54923}{55023}You will be used as judge in|Frankfurt� �later on maybe as lawyer.
{55024}{55125}You will help building up the|new judicial machinery in Germany.
{55252}{55377}We wish you and your family|good luck for the future...
{56901}{56951}Dad!
{57010}{57134}How did you manage this?
{57135}{57241}You want me to show you my|place? I am here and here is Inge.
{57242}{57342}There is the supervision,|a girl from 9th grade.
{57343}{57438}Priscilla. A British bitch.
{57518}{57593}What�s going on, anything with mum?
{57594}{57682}Nah, don�t worry Everything�s alright.
{57712}{57811}I need your help!
{57812}{57888}I want to go back to Germany!
{57889}{57971}I know. You want to and mum doesn�t.
{58002}{58118}I thought about it very carefully.|I want and I have to go back!
{58119}{58217}Please don�t make it so hard.
{58229}{58329}I could never forgive|myself if I make you unhappy.
{58330}{58430}Why do we have to go back? Other|people don�t have to go either!
{58431}{58531}Inge�s dad is going to be British|next week. You could do that as well.
{58532}{58612}You are in the army. You could!
{58621}{58721}Maybe he gets a British passport.
{58722}{58822}Even though he is far away|from being British. Tell me:
{58823}{58936}Can you imagine that your|Headmaster will invite him?
{58937}{59037}No. Sure he wont.
{59038}{59138}A British passport isn�t enough for me!|I don�t want to be a man who has a name,
{59139}{59239}which doesn�t belong to him. Here|in Kenya I�ll always be an outsider.
{59240}{59359}Do you know what this means|to me? I know that very well.
{59360}{59435}My brave girl.
{59442}{59552}Promise me that you wont be|disappointed if we go back to Germany.
{59553}{59641}Promise me that you�ll trust me.
{59749}{59841}If we have to go back...
{59893}{59989}can Owuor join us?
{60069}{60144}Not this time.
{60840}{60901}Hello my friend.
{61016}{61045}Very good.
{61050}{61080}Where is Memsaab?
{61081}{61145}Out on the field with Bwana S��kind.
{61184}{61238}Was Bwana S��kind often here?
{61947}{62047}And then a corn cob|drops out of his trousers.
{62048}{62155}Corn grew pretty good this year. The|rain came exactly at the right time!
{62156}{62256}Gibson can be satisfied.
{62257}{62331}And I give a fuck about this corn!
{62332}{62407}Asante, Owuor.
{62664}{62794}I requested to get sent back home.
{62948}{63109}The army is obliged to discharge|its men when the war is over.
{63110}{63185}For us this would be Germany.
{63186}{63264}They pay for our tickets.
{63281}{63381}So only you decided this?|Maybe Regina and I want
{63382}{63544}something totally different!
{63572}{63694}Why do you do this? Why don�t you|talk to me about things like these?
{63871}{63971}How can you think about|going back to Germany...
{63972}{64092}Stay away! You are not|going to destroy my family!
{64112}{64230}Do you know what your problem|is? You don�t know who you are!
{64233}{64338}We are Jews, Walter! Even if|it doesn�t mean anything to you.
{64730}{64780}Jettel.
{64835}{64914}I say this only once.
{64917}{65026}You know, if you want|to stay with me you can.
{65027}{65101}That would make me very happy.
{65517}{65591}These are going to be the|best "K�nigsberger Klopse"
{65593}{65666}you've ever eaten.
{65668}{65716}But you don't have Kapern
{65718}{65764}and you still don't|know, what Kapern are!
{65768}{65821}I don't know what Kapern are,
{65824}{65891}but I do know, that|Kapern aren't needed!
{66074}{66137}Happy new year, Rummler!
{66595}{66696}This have been the best|"K�nigsberger Klopse" I've ever eaten.
{66697}{66734}Thanks a lot!
{66820}{66890}I think she prefers your Ugali.
{67138}{67216}Happy new year, Jettel.
{67304}{67464}We need more guards for the fields. A lot|of corn is being stolen during the night.
{67471}{67566}Maybe you can talk to the guys.
{67567}{67613}Walter?
{67614}{67772}I need to make my decision. The|government is waiting for my response.
{67791}{67866}I won�t go back to Germany.
{67867}{67945}You want to give up our family?
{67946}{68025}-I don't understand why|still believe in that country.
{68066}{68166}I am a lawyer! I love my job!|And I have the feeling that
{68167}{68269}I could be useful in this �new Germany
{68270}{68373}Damn idealist! You think there|are no more Nazis around there?
{68374}{68473}They are the murderers of our parents!
{68474}{68591}That�s not just bad language. That|stands for the faith in mankind
{68592}{68754}Maybe that�s naive but everything|else leads to death and destruction.
{68812}{68948}This country saved our lives.|But it isn�t our country.
{69537}{69578}Can't you sleep?
{69579}{69647}No.
{69771}{69863}In Germany there's a lot of|snow in the winter, do you see?
{69865}{69918}A lot of snow is very cold?
{69953}{69988}Yes.
{70082}{70154}I hate snow.
{70417}{70527}You do all the work for Owuor, girl!
{70528}{70647}Mum needs your help.|Tomorrow harvest will begin!
{70648}{70749}The last couple of years she|made it pretty good without me.
{70750}{70858}Sometimes I prefer to be|in school than being here.
{70911}{70986}How come dad?
{71125}{71200}I don�t know.
{73784}{73835}Grasshoppers!
{74436}{74476}The grasshoppers are coming!
{74669}{74734}If they land on the fields|they'll eat everything!
{77777}{77866}They�re flying away,|they�re flying away mum!
{79092}{79194}Do you want some? It�s|good for your voice!
{79439}{79516}Thanks for coming back.
{81831}{81967}You have to be careful. I am pregnant.
{81972}{82084}And I am the father?
{82131}{82227}Jettel, honey, I don�t want to lose you.
{82878}{82982}Can�t we sit like this all the night?
{84367}{84456}Women and men in the village|never kiss each other.
{84457}{84557}They don�t even know what a kiss is.
{84558}{84616}What a pitty!
{84800}{84930}Everything I love is lying in this bed.
{85295}{85397}The songs of the women went|deeper into my heart than usual.
{85398}{85518}In the morning dad told me that|mum Is going to get another baby.
{85519}{85619}I felt Owuor right behind me and I knew
{85620}{85696}that this news was good as well as bad.
{86162}{86238}The letter�. I�m going to destroy it.
{86242}{86342}Which letter?
{86343}{86432}I can�t go without you.
{86577}{86678}My parents have made|their trip to Frankfurt.
{86679}{86805}The first night my dad was pretty drunk.
{86806}{86882}And my mother was upset.
{86958}{87054}I am so afraid of these|people, aren�t you?
{87061}{87111}Yes I am.
{87208}{87282}Do you love me?
{87296}{87371}If you let me.
{87561}{87636}Then decide for us.
{90341}{90391}Owuor?
{90465}{90518}What are you doing here?
{90521}{90548}I am waiting for the sun.
{90549}{90595}And why?
{90598}{90648}Do you want to sell|the dog at the market?
{90671}{90726}I didn't want you to see me.
{90727}{90797}Rummler and I will do a long safari.
{90800}{90872}Where this safari|leads to, no one knows.
{91019}{91125}A man has to do a safari|if his time has come.
{91387}{91436}Say goodbye to little Memsaab!
{91437}{91484}Shall I tell my little daughter:
{91485}{91533}"Owour is gone and didn't|want to see you one more time?"
{91534}{91595}Shall I tell her|"Rummler is gone forever?"
{91596}{91649}This dog is part of my child.
{91652}{91764}You were there, when Rummler|and Regina became friends.
{91766}{91827}Little Memsaab will understand.
{91829}{91886}She always understands everything.
{91887}{91942}She has eyes and a heart like we do.
{91943}{92026}Please Owour! Say goodbye!
{92037}{92141}Dad. Owuor has to go. Or do|you want his heart to dry?
{92142}{92239}He doesn�t want to die.
{92240}{92316}If so he would have died long time ago!
{92434}{92476}Here is my black coat.
{92480}{92518}You forgot it.
{92519}{92555}I didn't forget anything.
{92556}{92593}That coat doesn't belong to me anymore.
{92600}{92634}But I gave it to you as a gift
{92635}{92693}as you saved my life!
{92694}{92724}You said:
{92725}{92782}"I don't need this coat anymore...
{92783}{92839}it's from a past life that I've lost. "
{92840}{92901}Now you've found life again.
{92902}{92959}The life with the coat.
{93005}{93046}Please Owour, take it with you!
{93048}{93088}Without this coat you'll forget me.
{93127}{93174}I'll never forget you Bwana
{93175}{93242}I learned so many words from you.
{93333}{93454}Owour, you have to lift me up one last|time. Like in Rongai on my first day.
{93558}{93593}You must not go.
{93594}{93632}You don't even want to do that safari.
{93633}{93691}Take care of Bwana.|He's still like a kid!
{93693}{93786}You are smart. You have|to show him the way!
{94755}{94807}Good luck!
{94810}{94885}You too! Thank you.
{95204}{95289}I hope you�ll be happy.
{95335}{95410}Take care.
{96547}{96647}Why are we stopping now? I have no idea.
{96782}{96841}Bananas, bananas, �
{97060}{97145}I can't buy anything,|I'm poor like a monkey.
{97184}{97228}For the monkey!
{97229}{97266}Thank you.
{97896}{98032}My brother was born on July 6th 1947.|Thanks to God Ngai the birth went well,
{98033}{98133}he was totally ok.|He was tall and strong
{98134}{98248}My parents called him|Max. Like grandfather!
{98297}{98413}>> Subtitles translated by team QiX <<
