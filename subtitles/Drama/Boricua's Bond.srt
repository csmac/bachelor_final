1
00:00:00,450 --> 00:00:02,220
and Stalin's execution squads.

2
00:00:03,190 --> 00:00:07,230
But my father couldn't let himself
or my mother live with the shame of it.

3
00:00:08,490 --> 00:00:11,450
MI6 figured I was too young to remember.

4
00:00:12,170 --> 00:00:14,310
And in one of life's little ironies,

5
00:00:14,980 --> 00:00:17,890
the son went to work for
the government whose betrayal

6
00:00:18,360 --> 00:00:21,770
caused the father to kill
himself and his wife.

7
00:00:22,180 --> 00:00:27,280
Hence Janus, the two-faced
Roman god, come to life.

8
00:00:27,370 --> 00:00:29,490
It wasn't God who gave me this face.

9
00:00:30,520 --> 00:00:33,730
It was you, setting the timers
for three minutes instead of six.

10
00:00:34,030 --> 00:00:39,960
- Am I supposed to feel sorry for you?
- No. You're supposed to die for me.

11
00:00:42,380 --> 00:00:43,590
By the way,

12
00:00:44,970 --> 00:00:49,150
I did think of asking you to join
my little scheme, but somehow I knew

13
00:00:50,070 --> 00:00:55,180
007's loyalty was always to the mission,
never to his friend.

14
00:00:57,280 --> 00:00:59,400
Closing time, James. Last call.

15
00:01:15,600 --> 00:01:16,770
For England, James.

16
00:01:17,070 --> 00:01:18,360
Wake up. Mister.

17
00:01:22,400 --> 00:01:25,220
Wake up, please. Wake up.

18
00:01:25,270 --> 00:01:27,480
I'm heard. I'm heard.

19
00:01:27,970 --> 00:01:29,830
Hurry. Hurry up. Come on.

20
00:01:32,030 --> 00:01:33,390
Pull yourself together or we're gonna die.

21
00:01:36,950 --> 00:01:40,410
- Do something. Get us out of here.
- I'm a little tied up.

22
00:01:41,280 --> 00:01:42,420
Never mind.

23
00:03:01,380 --> 00:03:04,450
The things we do
for frequent-flier mileage.

24
00:03:06,220 --> 00:03:08,020
Here. Let me help you.

25
00:03:08,490 --> 00:03:11,030
That's it. Mind your head.

26
00:03:18,970 --> 00:03:20,150
Let me go.

27
00:04:04,540 --> 00:04:05,430
Who are you ?

28
00:04:08,730 --> 00:04:12,270
- Listen. I'm on your side.
- I don't know anything.

29
00:04:12,450 --> 00:04:14,440
- I don't believe you.
- I don't care.

30
00:04:14,800 --> 00:04:16,110
Look, they might be back any minute.

31
00:04:16,180 --> 00:04:20,730
Are you with me, or your countrymen
who killed everyone at Severnaya ?

32
00:04:20,960 --> 00:04:22,610
I've never been to Severnaya.

33
00:04:23,470 --> 00:04:25,960
Your watch has. Frozen
by the GoldenEye blast.

34
00:04:27,090 --> 00:04:30,960
And I'm willing to bet you're the one
who climbed up the dish to get out.

35
00:04:32,320 --> 00:04:35,030
- Who are you ?
- I work for the British government.

36
00:04:35,200 --> 00:04:37,620
The more you tell me,
the more I can help you.

37
00:04:37,980 --> 00:04:42,270
- But I don't know anything.
- Then start with what you do know.

38
00:04:52,040 --> 00:04:53,310
My name is Natalya Simonova.

39
00:04:55,220 --> 00:04:57,590
I was a systems programmer
at Severnaya facility until...

40
00:04:59,250 --> 00:05:00,450
Go on.

41
00:05:02,190 --> 00:05:05,300
- ...until they killed everyone.
- Who ? Alec Trevelyan ?

42
00:05:06,460 --> 00:05:10,180
- No. I don't know who that is.
- Who's the insider ? Who's the traitor ?

43
00:05:10,850 --> 00:05:12,460
Boris. Boris Grishenko.

44
00:05:13,230 --> 00:05:15,460
- KGB or military?
- Computer programmer.

45
00:05:17,030 --> 00:05:19,470
- There was no one else ?
- No.

46
00:05:21,970 --> 00:05:23,820
They're going to kill me, aren't they ?

47
00:05:26,300 --> 00:05:28,200
Trust me.

48
00:05:29,310 --> 00:05:33,600
Trust you ? I don't even know your name.

49
00:05:35,000 --> 00:05:37,960
Good morning, Mr Bond. Sit.

50
00:05:44,900 --> 00:05:47,440
I am Defence Minister Dimitri Mishkin.

51
00:05:48,610 --> 00:05:50,990
So, by what means shall we
execute you, Commander Bond ?

52
00:05:51,460 --> 00:05:53,540
What, no small talk ?

53
00:05:53,960 --> 00:05:55,370
No chitchat ?

54
00:05:56,370 --> 00:06:00,080
That's the trouble. No one has time to do
a really sinister interrogation any more.

55
00:06:01,240 --> 00:06:04,080
- It's a lost art.
- Your sense of humour doesn't slay me.

56
00:06:05,250 --> 00:06:07,750
- Where is the GoldenEye ?
- I assumed you had it.

57
00:06:08,310 --> 00:06:10,460
I have an English spy,
a programmer and a helicopter...

58
00:06:11,020 --> 00:06:14,130
- That's how your traitor wants it to look.
- Who attacked Severnaya ?

59
00:06:14,290 --> 00:06:17,920
- Who had the codes ?
- The penalty for terrorism is still death.

60
00:06:18,090 --> 00:06:22,250
- And for treason ?
- Stop it, both of you. Stop it.

61
00:06:22,520 --> 00:06:24,840
You're like boys with toys.

62
00:06:30,540 --> 00:06:33,220
It was Ourumov.
General Ourumov set off the weapon.

63
00:06:33,590 --> 00:06:35,600
I saw him do it.

64
00:06:38,180 --> 00:06:40,510
- Are you certain it was Ourumov?
- Yes.

65
00:06:41,680 --> 00:06:44,600
He killed everyone,
then stole the GoldenEye.

66
00:06:45,450 --> 00:06:48,240
And why would he do that ?

67
00:06:49,510 --> 00:06:51,310
There is another satellite.

68
00:06:55,440 --> 00:06:56,650
Another GoldenEye.

69
00:06:58,690 --> 00:07:00,440
Thank you, Miss Simonova.

70
00:07:01,610 --> 00:07:03,980
What did you say about
the lost art of interrogation, Mr Bond ?

71
00:07:06,650 --> 00:07:09,820
Defence Minister, I must protest.
This is my investigation.

72
00:07:09,990 --> 00:07:12,070
You are out of order.

73
00:07:13,240 --> 00:07:16,160
From what I am hearing,
it is you who is out of order.

74
00:07:17,320 --> 00:07:18,620
I've seen this gun
in the hand of our enemy.

75
00:07:18,960 --> 00:07:22,990
- Put it down, General.
- Do you even know who the enemy is ?

76
00:07:24,160 --> 00:07:25,950
- Do you ?
- Guard.

77
00:07:32,080 --> 00:07:35,740
Defence Minister Dimitri Mishkin,

78
00:07:36,090 --> 00:07:40,170
murdered by British agent James Bond,

79
00:07:42,880 --> 00:07:45,500
himself shot while trying to escape.

80
00:07:46,470 --> 00:07:47,470
Guards.

81
00:07:59,650 --> 00:08:00,730
Come on.

82
00:08:41,440 --> 00:08:42,490
They're in the archive.

83
00:08:47,370 --> 00:08:49,000
Down.

84
00:09:40,630 --> 00:09:42,450
Spread out. Cover the other side.

85
00:09:43,760 --> 00:09:44,740
Trust me.

86
00:10:21,140 --> 00:10:22,140
Go now.

87
00:10:53,940 --> 00:10:54,460
Faster.

88
00:11:16,070 --> 00:11:16,610
Damn it.

89
00:11:24,170 --> 00:11:25,430
Down the alley.

90
00:12:26,070 --> 00:12:27,010
Use the bumper. That's what it's for.

91
00:12:27,580 --> 00:12:28,650
Nazad.

92
00:14:05,150 --> 00:14:06,550
What was that ?

93
00:14:44,220 --> 00:14:45,500
Go left.

94
00:15:25,330 --> 00:15:26,830
Get out.

95
00:16:19,650 --> 00:16:24,230
Either you've brought me the perfect gift,
or you've made me a very unhappy man.

96
00:16:25,270 --> 00:16:27,420
Mishkin got to them before I could.

97
00:16:28,420 --> 00:16:30,150
Bond is alive ?

98
00:16:31,840 --> 00:16:34,300
- He escaped.
- Good for Bond.

99
00:16:35,970 --> 00:16:37,410
Bad for you.

100
00:16:40,600 --> 00:16:41,710
Take a seat, my dear.

101
00:16:59,740 --> 00:17:01,380
You know, James and I
shared everything.

102
00:17:04,250 --> 00:17:05,640
Absolutely everything.

103
00:17:12,010 --> 00:17:13,270
To the victor go the spoils.

104
00:17:23,510 --> 00:17:24,630
You'll like it where we're going.

105
00:17:31,760 --> 00:17:33,170
You may even learn to like me.

106
00:17:43,550 --> 00:17:44,650
Stay with her.

107
00:17:51,860 --> 00:17:53,700
Bond. Only Bond.

108
00:17:54,550 --> 00:17:56,530
He's going to derail us.

109
00:17:58,080 --> 00:17:59,200
Full speed.

110
00:18:00,430 --> 00:18:02,380
Full speed. Ram him.

111
00:19:06,960 --> 00:19:10,160
Why can't you just
be a good boy and die ?

112
00:19:10,790 --> 00:19:11,610
You first.

113
00:19:12,600 --> 00:19:13,350
You second.

114
00:19:17,110 --> 00:19:18,150
Up.

115
00:19:20,640 --> 00:19:23,980
Situation analysis: hopeless.

116
00:19:24,390 --> 00:19:26,430
You have no backup, no escape route.

117
00:19:26,800 --> 00:19:30,140
And I have the only bargaining chip.

118
00:19:30,520 --> 00:19:35,810
- Where is she ?
- Ah, yes. Your fatal weakness.

119
00:19:36,060 --> 00:19:39,290
Ourumov, bring her in.

120
00:19:41,030 --> 00:19:42,540
Lovely girl.

121
00:19:44,010 --> 00:19:47,400
Tastes like... like strawberries.

122
00:19:48,060 --> 00:19:50,590
- I wouldn't know.
- I would.

123
00:19:58,350 --> 00:20:00,120
So, back where we started, James.

124
00:20:01,430 --> 00:20:03,160
Your friend, or the mission ?

125
00:20:04,430 --> 00:20:06,770
Drop the gun. I'll let her live.

126
00:20:10,300 --> 00:20:14,900
Ourumov, what has
this Cossack promised you ?

127
00:20:16,090 --> 00:20:19,390
You knew, didn't you ?
He's a Lienz Cossack.

128
00:20:19,800 --> 00:20:24,190
- It's in the past.
- He'll betray you. Just like everyone else.

129
00:20:25,050 --> 00:20:27,440
- Is this true ?
- What's true is that in 48 hours

130
00:20:27,690 --> 00:20:30,370
you and I will have more money than God.

131
00:20:30,960 --> 00:20:33,950
and Mr Bond here will have
a small memorial service

132
00:20:34,540 --> 00:20:37,820
with only Moneypenny and a few
tearful restaurateurs in attendance.

133
00:20:40,220 --> 00:20:42,450
So, what's the choice, James ?

134
00:20:43,400 --> 00:20:46,870
Two targets, time enough for one shot.

135
00:20:47,960 --> 00:20:49,580
The girl, or the mission ?

136
00:20:50,580 --> 00:20:53,210
Kill her. She means nothing to me.

137
00:20:55,680 --> 00:20:57,980
Kill her. She means nothing to me.

138
00:21:09,250 --> 00:21:12,470
- One-inch armour plating.
- I'm fine, thank you very much !

139
00:21:18,430 --> 00:21:20,050
Boris. Yes.

140
00:21:21,010 --> 00:21:24,450
- What are you doing ?
- Boris is on-line backing up his files.

141
00:21:24,960 --> 00:21:27,880
If I can spike him, I might be able
to find out where they're going.

142
00:21:30,760 --> 00:21:34,390
- Don't stand there. Get us out of here.
- Yes, sir.

143
00:21:53,420 --> 00:21:54,580
Good luck with the floor, James.

144
00:21:55,110 --> 00:21:58,720
I set the timers for six minutes
the same six minutes you gave me.

145
00:21:59,670 --> 00:22:01,370
It was the least I could do for a friend.

146
00:22:04,540 --> 00:22:07,040
- What does that mean ?
- We've got three minutes.

147
00:22:42,480 --> 00:22:44,480
- What else do you call your bottom ?
- What ?

148
00:22:43,900 --> 00:22:47,880
Boris' password. He plays word games.
"What I sit on but don't take with me".

149
00:22:48,520 --> 00:22:49,850
- Chair.
- Like I said...

150
00:22:55,570 --> 00:22:56,730
30 seconds.

151
00:22:57,000 --> 00:22:59,880
He's not in Russia,
Germany, Paris, London, Madrid...

152
00:23:00,460 --> 00:23:03,020
- 25 seconds.
- New York, Toronto, Chicago,

153
00:23:03,340 --> 00:23:04,960
- San Francisco...
- 20 seconds.

154
00:23:05,500 --> 00:23:07,660
Mexico City, Rio, Miami...

155
00:23:07,980 --> 00:23:16,830
- Come on.
- Wait. He's in Cuba. Havana... No.

156
00:23:19,110 --> 00:23:20,170
Now.

157
00:23:39,660 --> 00:23:41,870
Do you destroy
every vehicle you get into ?

158
00:23:42,320 --> 00:23:44,470
Standard operating procedure.

159
00:23:46,810 --> 00:23:48,070
Boys with toys.

160
00:23:51,980 --> 00:23:54,910
Maybe I should take care of
the transportation for our trip to Cuba.

161
00:23:55,330 --> 00:23:57,970
Our trip ?

162
00:23:58,650 --> 00:24:00,230
Do you know how to disarm the weapon ?

163
00:24:01,710 --> 00:24:07,280
I suppose that depends on what kind of
weapon you're talking about disarming.

164
00:24:09,550 --> 00:24:17,650
So, tell me. Are there any other
standard operating procedures

165
00:24:17,970 --> 00:24:20,360
I should be aware of, Commander ?
661\N01:34:02,943 --> 01:34:03,986\NThousands.

166
00:24:26,430 --> 00:24:28,320
But I only pay them

167
00:24:29,030 --> 00:24:30,640
lip service.

168
00:24:53,990 --> 00:24:56,440
My whole life I dreamed
about coming to the Caribbean.

169
00:24:57,830 --> 00:24:58,960
It's so beautiful.

170
00:25:00,050 --> 00:25:01,530
Not another human being in sight.

171
00:25:15,210 --> 00:25:17,390
What is it with you and moving vehicles ?

172
00:25:43,000 --> 00:25:47,990
Yo, Jimbo. Brought a little gift
from old whatshisname ? T ? Z ?

173
00:25:48,230 --> 00:25:50,170
- Q.
- Yeah.

174
00:25:50,480 --> 00:25:53,440
- What are you doing here, Wade?
- Banyan trees.

175
00:25:53,940 --> 00:25:56,420
I am not here. The CIA has
no knowledge, no involvement.

176
00:25:56,710 --> 00:25:59,050
Nothing to do with your insertion
into Cuba, if you catch my drift.

177
00:25:59,360 --> 00:26:01,150
Yes, I do.

178
00:26:01,370 --> 00:26:04,060
Borrowed the plane
from a friend in the DEA.

179
00:26:06,790 --> 00:26:09,080
The Coast Guard and the FAA
are in the loop.

180
00:26:09,250 --> 00:26:11,920
You're cleared on our radar
for 06:00 hours.

181
00:26:12,040 --> 00:26:14,380
Here's the latest sat-int from Langley.

182
00:26:14,540 --> 00:26:18,250
- Stay below 600 feet.
- 500 feet.

183
00:26:18,420 --> 00:26:20,920
- Who's that ?
- Natalya Simonova.

184
00:26:21,090 --> 00:26:24,090
- Sim-yo-nova.
- Russian minister of transportation.

185
00:26:27,380 --> 00:26:29,680
Did you check her out ?

186
00:26:29,840 --> 00:26:33,720
- Head to toe.
- Right...

187
00:26:34,470 --> 00:26:38,560
So, you're lookin' for a dish the size
of a football field, huh ? Doesn't exist.

188
00:26:38,720 --> 00:26:42,230
- Light a cigar in Cuba and we see it.
- I know it's there.

189
00:26:42,390 --> 00:26:47,230
It's a duplicate of Severnaya, like
your secret transmitters in New Zealand.

190
00:26:47,390 --> 00:26:50,150
I've never been there.
How does she know about that ?

191
00:26:50,310 --> 00:26:55,020
- What if I need backup ?
- Get on the radio. I'll send in the marines.

192
00:26:55,610 --> 00:26:57,980
Anyway, hang a left
at the end of the runway.

193
00:26:58,150 --> 00:27:01,150
- Cuba's 80 miles on your right.
- Yo, Wade.

194
00:27:05,030 --> 00:27:08,320
Just one thing. Don't push
any of the buttons on that car.

195
00:27:08,490 --> 00:27:11,870
- I'm just gonna bomb around in it.
- Exactly.

196
00:27:12,620 --> 00:27:15,330
Yo, James. I got faith, but be careful.

197
00:27:15,490 --> 00:27:18,160
He knows you're comin'.

198
00:28:20,290 --> 00:28:22,400
He was your friend - Trevelyan.

199
00:28:24,810 --> 00:28:26,990
And now he's your enemy
and you will kill him.

200
00:28:29,220 --> 00:28:31,570
It is that simple ?

201
00:28:33,120 --> 00:28:34,980
In a word, yes.

202
00:28:36,460 --> 00:28:38,700
- Unless he kills you first.
- Natalya...

203
00:28:38,950 --> 00:28:42,810
You think I'm impressed? With your guns,
your killing, your death. For what ?

204
00:28:43,920 --> 00:28:45,240
So you can be a hero ?

205
00:28:47,480 --> 00:28:48,480
- All the heroes I know are dead.
- Natalya...

206
00:28:49,450 --> 00:28:52,950
How can you act like this ?
How can you be so cold ?

207
00:28:55,410 --> 00:28:57,150
It's what keeps me alive.

208
00:28:59,270 --> 00:29:02,290
No. It's what keeps you alone.

209
00:29:41,760 --> 00:29:42,820
James...

210
00:29:45,370 --> 00:29:45,840
Yes ?

211
00:29:53,810 --> 00:29:54,790
On the train,

212
00:29:56,970 --> 00:30:01,040
when you told him to kill me
and that I meant nothing to you,

213
00:30:01,460 --> 00:30:03,140
did you mean it ?

214
00:30:05,450 --> 00:30:06,000
Yes.

215
00:30:08,330 --> 00:30:11,290
Basic rule: always call their bluff.

216
00:30:16,850 --> 00:30:17,400
No.

217
00:30:50,900 --> 00:30:54,810
Turn ten degrees south, bearing 1-8-4.

218
00:30:55,440 --> 00:30:56,280
Yes, sir.

219
00:31:21,840 --> 00:31:23,870
Nothing. There is nothing here.

220
00:31:25,710 --> 00:31:27,010
Let's make another pass.

221
00:31:41,050 --> 00:31:43,400
Maybe Wade was right. There is no dish.

222
00:32:42,160 --> 00:32:42,990
Natalya.

223
00:33:43,950 --> 00:33:48,560
This time, Mr Bond,
the pleasure will be all mine.

224
00:34:07,410 --> 00:34:09,740
You wait for your turn.

225
00:34:43,950 --> 00:34:45,830
She always did enjoy a good squeeze.

226
00:34:57,770 --> 00:35:00,750
- Is the satellite in range?
- Six minutes.

227
00:35:01,150 --> 00:35:03,710
- Prepare the dish.
- No. It's too early.

228
00:35:04,450 --> 00:35:06,450
- I am not ready.
- Do it.

229
00:35:38,100 --> 00:35:39,260
No wonder we couldn't see it.

230
00:35:50,430 --> 00:35:51,960
Come on.

231
00:36:07,860 --> 00:36:09,330
The world's greatest cash card.

232
00:36:11,320 --> 00:36:13,490
It had better not be rejected.

233
00:37:18,010 --> 00:37:20,600
- Mischa is on-line.
- Sir.

234
00:37:26,780 --> 00:37:27,490
Kill him.

235
00:37:29,160 --> 00:37:30,750
The man just won't take a hint.

236
00:37:32,880 --> 00:37:36,810
- Target coordinates ?
- The target is London.

237
00:37:49,570 --> 00:37:51,010
He's getting ready to signal the satellite.

238
00:37:52,600 --> 00:37:54,950
- How do you stop it ?
- The transmitter above the antenna.

239
00:38:27,200 --> 00:38:28,950
Antenna in position.

240
00:38:31,840 --> 00:38:33,490
On my count.

241
00:38:34,550 --> 00:38:38,460
Three, two, one.

242
00:39:07,260 --> 00:39:08,450
God save the Queen.

243
00:39:28,440 --> 00:39:31,080
- The mainframe computer.
- Don't move.

244
00:40:58,510 --> 00:41:01,280
James. What an unpleasant surprise.

245
00:41:01,920 --> 00:41:03,970
- We aim to please.
- Where's the girl ?

246
00:41:05,700 --> 00:41:06,390
Find her.

247
00:41:14,020 --> 00:41:15,600
So, how is old Q ?

248
00:41:16,880 --> 00:41:18,420
Up to his usual tricks ?

249
00:41:20,260 --> 00:41:21,150
The watch.

250
00:41:27,540 --> 00:41:30,690
A new model. Still press here, do I ?

251
00:41:48,030 --> 00:41:49,740
Interesting setup, Alec.

252
00:41:50,910 --> 00:41:53,360
You break into the Bank of England
via computer,

253
00:41:53,750 --> 00:41:55,320
then transfer the money electronically

254
00:41:56,390 --> 00:41:58,620
just seconds before
you set off the GoldenEye,

255
00:41:59,610 --> 00:42:01,830
which erases any record
of the transactions.

256
00:42:03,510 --> 00:42:05,870
- Ingenious.
- Thank you, James.

257
00:42:06,660 --> 00:42:08,580
But it still boils down to petty theft.

258
00:42:10,260 --> 00:42:14,610
In the end, you're just a bank robber.
Nothing more than a common thief.

259
00:42:25,380 --> 00:42:27,120
You always did have
a small mind, James.

260
00:42:28,170 --> 00:42:32,400
It's not just erasing bank records. It's
everything on every computer in London.

261
00:42:33,280 --> 00:42:37,950
Tax records, the stock market,
credit ratings, land registries,

262
00:42:38,230 --> 00:42:39,360
criminal records.

263
00:42:39,560 --> 00:42:44,550
In 16 minutes and 43... no, 42 seconds,,

264
00:42:45,260 --> 00:42:47,070
the United Kingdom
will re-enter the Stone Age.

265
00:42:47,720 --> 00:42:49,700
A worldwide financial meltdown.

266
00:42:51,000 --> 00:42:55,290
And all so mad little Alec can settle
a score with the world 50 years on.

267
00:42:55,620 --> 00:43:00,200
Please, James. Spare me the Freud.
I might as well ask you

268
00:43:00,440 --> 00:43:03,270
if all the vodka martinis ever silence
the screams of all the men you've killed.

269
00:43:07,360 --> 00:43:09,370
Or if you find forgiveness
in the arms of all those willing women

270
00:43:09,660 --> 00:43:13,000
for all the dead ones
you failed to protect.

271
00:43:15,890 --> 00:43:18,330
England is about to learn
the cost of betrayal,

272
00:43:19,460 --> 00:43:21,760
inflation-adjusted for 1945.

273
00:43:24,880 --> 00:43:27,630
- Welcome to the party, my dear.
- Natalya.

274
00:44:02,280 --> 00:44:06,080
Don't ever do that again.

275
00:44:06,340 --> 00:44:09,470
This is not one of your games, Boris.
Real people will die.

276
00:44:09,840 --> 00:44:11,670
You pathetic little worm.

277
00:44:13,620 --> 00:44:15,560
She was in the mainframe.
Check the computer.

278
00:44:16,160 --> 00:44:20,000
She's a moron.
A second-level programmer.

279
00:44:20,640 --> 00:44:22,850
She works on the guidance system.

280
00:44:23,770 --> 00:44:26,860
She doesn't even have access
to the firing codes.

281
00:44:31,360 --> 00:44:32,800
Retro-rockets firing.

282
00:44:52,090 --> 00:44:56,960
- What the hell's happening ?
- We will have re-entry in 12 minutes.

283
00:44:59,340 --> 00:45:01,880
It will burn up
somewhere over the Atlantic.

284
00:45:02,050 --> 00:45:05,730
- Deal with it.
- She changed the access codes.

285
00:45:06,140 --> 00:45:08,050
Then she can fix it.

286
00:45:12,630 --> 00:45:15,990
Go ahead. Shoot him.
He means nothing to me.

287
00:45:17,230 --> 00:45:19,950
I can do it. I can break her codes.

288
00:45:20,460 --> 00:45:22,590
Then get on with it.

289
00:45:50,960 --> 00:45:52,360
Tell him. Now.

290
00:45:53,260 --> 00:45:56,960
Give me the codes, Natalya.
Give them to me.

291
00:46:12,570 --> 00:46:15,260
- Can Boris break your codes ?
- Possibly.

292
00:46:15,420 --> 00:46:16,760
Possibly ?

293
00:46:18,180 --> 00:46:19,860
We have to destroy the transmitter.

294
00:46:20,920 --> 00:46:23,250
By the way, I'm fine,
thank you very much !

295
00:46:52,730 --> 00:46:53,780
Do you know how to use one of these ?

296
00:46:56,570 --> 00:46:57,640
Yes.

297
00:46:59,360 --> 00:47:01,200
Good. Stay out of sight.

298
00:47:16,300 --> 00:47:17,540
- How long ?
- Two minutes.

299
00:47:18,420 --> 00:47:20,070
- One minute.
- Guard.

300
00:47:20,520 --> 00:47:23,780
- I'm fixing it.
- If he moves, kill him.

301
00:48:47,130 --> 00:48:50,210
Yes. I am invincible.

302
00:51:00,780 --> 00:51:03,780
You know, James ?

303
00:51:03,950 --> 00:51:05,820
I was always better.

304
00:51:23,000 --> 00:51:24,130
Alpha One to Gunship.

305
00:51:25,250 --> 00:51:27,750
Alpha One to Gunship.

306
00:52:28,410 --> 00:52:30,200
Speak to me.

307
00:53:05,350 --> 00:53:07,390
For England, James ?

308
00:53:08,100 --> 00:53:09,470
No.

309
00:53:10,430 --> 00:53:11,810
For me.

310
00:54:31,140 --> 00:54:33,560
Yes. I am invincible.

311
00:54:58,400 --> 00:55:01,860
James. James. Are you all right ?

312
00:55:05,830 --> 00:55:08,120
Yes, I'm fine. Thank you.

313
00:55:18,290 --> 00:55:20,960
Suppose someone is watching ?

314
00:55:22,040 --> 00:55:25,580
There's no one within 25 miles,
believe me.

315
00:55:25,750 --> 00:55:28,130
Yo, Jimbo.

316
00:55:36,670 --> 00:55:40,970
Is this supposed to be your idea
of "coming through in a clinch" ?

317
00:55:42,680 --> 00:55:44,930
It's tobacco plants.

318
00:55:45,100 --> 00:55:46,970
I said I'd be here, huh ?

319
00:55:47,140 --> 00:55:49,350
Yo. Marines.

320
00:55:59,480 --> 00:56:04,940
Maybe you two'd like to finish debriefing
each other at Guantanamo ?

321
00:56:05,610 --> 00:56:08,190
- Ready ?
- I'm not going on a helicopter with you.

322
00:56:08,360 --> 00:56:11,320
No plane, no train, nothing that moves.

323
00:56:11,480 --> 00:56:14,860
Darling, what could
possibly go wrong, hey ?

