﻿1
00:00:03,378 --> 00:00:05,462
Oh, my God, I love this chicken.

2
00:00:05,672 --> 00:00:10,426
Oh, you know what they say,
"The best things in life are free."

3
00:00:10,635 --> 00:00:13,095
You're right. I eat your food a lot.
How about this?

4
00:00:13,304 --> 00:00:15,264
You can raid my fridge
any time you want.

5
00:00:15,473 --> 00:00:16,807
Oh, that's very kind of you.

6
00:00:17,017 --> 00:00:20,060
Next time I have a hankering
to wash down a D-cell battery...

7
00:00:20,270 --> 00:00:23,147
...with a jar of old pickle juice,
I'll come a-knocking.

8
00:00:24,482 --> 00:00:27,151
Hey, Raj, you didn't send your RSVP in.

9
00:00:27,360 --> 00:00:29,960
I'm supposed to ask you if you're
bringing someone to the wedding.

10
00:00:30,030 --> 00:00:31,113
I'll let you know.

11
00:00:31,322 --> 00:00:33,991
Could you make it soon?
There's a battle over seating.

12
00:00:34,200 --> 00:00:38,203
In one corner, Bernadette's mom.
In the other three, mine.

13
00:00:39,039 --> 00:00:41,999
Yeah, I hate wedding receptions.
I wish the bride and groom...

14
00:00:42,208 --> 00:00:44,001
...would take a cue from Bilbo Baggins.

15
00:00:44,210 --> 00:00:47,129
Slip on the ring, disappear,
and everyone goes home.

16
00:00:47,672 --> 00:00:49,965
Oh, you liked Professor Geyser's wedding.

17
00:00:50,550 --> 00:00:52,468
They had a make-your-own-sundae bar.

18
00:00:52,677 --> 00:00:54,845
Oh, that was a night to remember.

19
00:00:55,055 --> 00:00:58,974
Do you know, on one trip,
I just had a bowl of nuts.

20
00:00:59,559 --> 00:01:02,061
Anyway, you gotta let me know
if you have a plus-one.

21
00:01:02,270 --> 00:01:04,563
If not, my mom's trying
to sneak in the doctor...

22
00:01:04,773 --> 00:01:06,648
...who sucked the fat out of her neck.

23
00:01:10,695 --> 00:01:11,737
All right, uh, fine.

24
00:01:11,946 --> 00:01:13,906
I'm coming and I'm bringing somebody.

25
00:01:14,115 --> 00:01:15,449
Koothrappali plus one.

26
00:01:15,658 --> 00:01:18,494
- Who are you bringing?
- Who are you bringing?

27
00:01:18,703 --> 00:01:20,746
He's bringing me. Who are you bringing?

28
00:01:20,955 --> 00:01:24,416
Wow, what a bunch of Nosey O'Donnells.

29
00:01:25,126 --> 00:01:26,752
Come on, who is it?

30
00:01:26,961 --> 00:01:30,798
I'm not telling. I'm from Asia.
I'm mysterious. Deal with it.

31
00:01:32,133 --> 00:01:34,593
Howard, are you having
a make-your-own-sundae bar?

32
00:01:34,803 --> 00:01:36,512
Uh, I don't think so.

33
00:01:36,721 --> 00:01:39,556
Well, you should. Fifty percent
of marriages end in divorce.

34
00:01:39,766 --> 00:01:43,268
But 100 percent of make-your-own-sundae
bars end in happiness.

35
00:01:44,320 --> 00:01:47,920
<i>Our whole universe
was in a hot, dense state.</i>

36
00:01:47,950 --> 00:01:51,268
<i>Then nearly 14 billion years
ago expansion started... Wait!</i>

37
00:01:51,270 --> 00:01:52,818
<i>The Earth began to cool.</i>

38
00:01:52,820 --> 00:01:55,438
<i>The autotrophs began to drool,
Neanderthals developed tools.</i>

39
00:01:55,440 --> 00:01:58,158
<i>We built the Wall.
We built the pyramids.</i>

40
00:01:58,160 --> 00:02:00,798
<i>Math, Science, History,
unraveling the mystery.</i>

41
00:02:00,800 --> 00:02:02,698
<i>That all started
with a big bang.</i>

42
00:02:02,700 --> 00:02:04,650
<i>Bang!</i>

43
00:02:14,756 --> 00:02:18,759
- Hello, Mommy, Daddy. How are you?
- Pretty good, can't complain.

44
00:02:18,969 --> 00:02:22,096
Oh, I'm sure you can.
Just give it a minute.

45
00:02:22,889 --> 00:02:24,306
Listen, uh...

46
00:02:24,516 --> 00:02:26,317
There's something I want
to talk to you about.

47
00:02:26,518 --> 00:02:30,479
I wasn't ready until now
but I think it's time.

48
00:02:30,689 --> 00:02:31,897
It's finally happening.

49
00:02:32,107 --> 00:02:34,400
You're coming out of the closet,
aren't you?

50
00:02:35,819 --> 00:02:38,487
We love you
and we accept your alternate lifestyle.

51
00:02:38,697 --> 00:02:41,782
Just keep it to yourself.

52
00:02:42,325 --> 00:02:44,368
No, I'm not gay.

53
00:02:45,453 --> 00:02:47,621
If anything, I'm metrosexual.

54
00:02:47,831 --> 00:02:49,331
What's that?

55
00:02:49,541 --> 00:02:52,835
It means I like women
as well as their skin-care products.

56
00:02:53,336 --> 00:02:54,920
Well, if you're not coming out...

57
00:02:55,130 --> 00:02:57,423
...why did you call us
during the cricket semi-finals?

58
00:02:57,632 --> 00:03:01,510
I'm tired of trying to meet someone...

59
00:03:01,720 --> 00:03:04,513
...and I think I'd like you
to help me find a...

60
00:03:04,723 --> 00:03:06,056
A wife.

61
00:03:06,600 --> 00:03:09,894
And just to clarify, a female wife?

62
00:03:10,103 --> 00:03:12,313
Yes!

63
00:03:12,522 --> 00:03:15,107
Matchmaking. Very smart move, son.

64
00:03:15,317 --> 00:03:18,110
Much better than marrying for love.

65
00:03:22,908 --> 00:03:25,534
We married for love.

66
00:03:25,744 --> 00:03:28,287
And it's been wonderful.

67
00:03:31,374 --> 00:03:34,543
Quantum physics makes me so happy.

68
00:03:35,170 --> 00:03:36,420
Yeah? I'm glad.

69
00:03:36,630 --> 00:03:38,839
It's like looking at the universe naked.

70
00:03:47,933 --> 00:03:50,059
- Hi. Guys got a minute?
- Sure, what's up?

71
00:03:50,268 --> 00:03:53,187
I was thinking about Sheldon's little joke
the other night...

72
00:03:53,396 --> 00:03:56,649
- ...about me eating all your food.
- Oh, that was no joke.

73
00:03:57,943 --> 00:04:01,904
But I understand your confusion
as I am our group's resident cutup.

74
00:04:02,948 --> 00:04:05,574
I'm sorry, you are our resident cutup?

75
00:04:06,618 --> 00:04:07,910
Yes.

76
00:04:09,371 --> 00:04:10,955
- Prove it.
- Knock-knock.

77
00:04:11,164 --> 00:04:12,725
- Who's there?
- Interrupting physicist.

78
00:04:12,832 --> 00:04:15,292
- Interrupting phys...
- Muon. Ha, ha.

79
00:04:18,213 --> 00:04:21,173
Anyway, I got a little residual check
from my commercial.

80
00:04:21,383 --> 00:04:25,344
And I thought, "How about I get the guys
a little thank-you to pay them back?"

81
00:04:25,553 --> 00:04:28,055
So, Sheldon... Ta-da.

82
00:04:29,516 --> 00:04:33,602
A vintage, mint-in-box 1975
Mego Star Trek Transporter...

83
00:04:33,812 --> 00:04:37,022
...with real transporter action. Hot darn!

84
00:04:38,191 --> 00:04:41,151
- Where did you get that?
- From Stuart at the comic-book store.

85
00:04:41,361 --> 00:04:43,654
You went to the comic-book store
by yourself?

86
00:04:43,863 --> 00:04:45,489
Yeah. It was fun.

87
00:04:45,699 --> 00:04:49,076
I walked in and two different guys
got asthma attacks.

88
00:04:50,829 --> 00:04:52,288
Felt pretty good.

89
00:04:52,497 --> 00:04:54,790
Well, this calls for an expression
of gratitude.

90
00:04:55,000 --> 00:04:57,626
Ooh, am I about to get
a rare Sheldon Cooper hug?

91
00:04:57,836 --> 00:04:59,962
No, not this time.
Then they wouldn't be special.

92
00:05:00,171 --> 00:05:02,214
Click. Thanks, Penny.

93
00:05:03,133 --> 00:05:04,925
You're welcome.

94
00:05:05,135 --> 00:05:07,886
Don't worry, I didn't forget about you.

95
00:05:08,096 --> 00:05:11,849
Leonard, I got you a label-maker.

96
00:05:18,565 --> 00:05:20,482
Wow.

97
00:05:21,234 --> 00:05:22,401
No, it's great.

98
00:05:22,610 --> 00:05:25,446
- Also, uh, mint-in-box.
- Mm.

99
00:05:25,655 --> 00:05:30,075
- And I got you a Transporter too!
- Awesome!

100
00:05:33,121 --> 00:05:36,707
Look, it was actually designed
for my vintage Mr. Spock action figure.

101
00:05:36,916 --> 00:05:38,917
Oh, that's great.
Open it and put him in there.

102
00:05:39,127 --> 00:05:40,878
- Open it?
- No, good Lord, no.

103
00:05:42,047 --> 00:05:43,172
Why? They're just toys.

104
00:05:43,381 --> 00:05:45,341
- Collectibles.
- They're mint-in-box.

105
00:05:45,550 --> 00:05:47,134
Can't we open one up and take a look?

106
00:05:47,344 --> 00:05:49,178
- Aah!
- No, don't!

107
00:05:49,387 --> 00:05:52,473
Once you open the box, it loses its value.

108
00:05:52,682 --> 00:05:56,727
Yeah, yeah. My mom gave me
the same lecture about my virginity.

109
00:05:58,772 --> 00:06:02,232
Gotta tell you, it was a lot more fun
taking it out and playing with it.

110
00:06:05,111 --> 00:06:07,529
You sure you want us here
when you meet this woman?

111
00:06:07,739 --> 00:06:11,867
Oh, yes. In my culture, it's expected to
have a chaperone to oversee a first date.

112
00:06:12,077 --> 00:06:15,204
I want to make a good impression.
No offense, but with guys you here...

113
00:06:15,413 --> 00:06:17,581
...I look like I'm 6'2".

114
00:06:19,584 --> 00:06:22,378
The nuns always chaperoned
the dances at my high school.

115
00:06:22,587 --> 00:06:25,172
They used to make us leave room
between us for the Holy Spirit.

116
00:06:25,382 --> 00:06:26,423
Hmm.

117
00:06:26,633 --> 00:06:30,969
Hindus do the same thing.
Except they leave room for a cow.

118
00:06:31,763 --> 00:06:33,889
Listen, I love your charming
racist humor.

119
00:06:34,099 --> 00:06:36,934
But any chance you could not mock
my religion while she's here?

120
00:06:37,143 --> 00:06:41,230
- You made fun of me for eating lox.
- It's different. Your people don't worship lox.

121
00:06:41,439 --> 00:06:43,982
Clearly you've never been to brunch
with my cousins.

122
00:06:44,984 --> 00:06:47,277
So arranged marriages,
the parents just decide...

123
00:06:47,487 --> 00:06:48,821
...and then you get married?

124
00:06:49,030 --> 00:06:52,741
Oh, no, no. I get a say in it. I'm sure
whoever shows up will be better company...

125
00:06:52,951 --> 00:06:56,703
...than the threesome I've been having
with Aunt Jemima and Mrs. Butterworth.

126
00:06:58,998 --> 00:07:00,457
Excuse me, are you Rajesh?

127
00:07:00,667 --> 00:07:02,960
Oh, yes. You must be Lakshmi.

128
00:07:03,169 --> 00:07:05,170
- Uh, nice to meet you.
- Nice to meet you too.

129
00:07:05,380 --> 00:07:08,465
Oh, uh, these are my friends.
Uh, this is Bernadette.

130
00:07:08,675 --> 00:07:10,008
- Hello.
- Uh, and this is Howard.

131
00:07:10,218 --> 00:07:11,552
- Nice to meet you.
- You too.

132
00:07:11,761 --> 00:07:13,720
Please, have a seat.

133
00:07:14,264 --> 00:07:16,140
I'm thinking double wedding.

134
00:07:21,062 --> 00:07:23,272
Dr. Cooper.

135
00:07:25,692 --> 00:07:27,734
Dr. Cooper?

136
00:07:29,028 --> 00:07:30,279
Is someone there?

137
00:07:30,488 --> 00:07:32,364
Down here. On your desk.

138
00:07:37,328 --> 00:07:40,789
- Spock?
- I need to speak with you.

139
00:07:42,000 --> 00:07:44,293
Fascinating.

140
00:07:44,502 --> 00:07:47,796
The only logical explanation
is that this is a dream.

141
00:07:48,006 --> 00:07:50,549
It is not the only logical explanation.

142
00:07:50,758 --> 00:07:52,926
For example,
you could be hallucinating...

143
00:07:53,136 --> 00:07:57,598
...after being hit on the head by,
say, a coconut.

144
00:07:59,392 --> 00:08:01,393
Was I hit on the head by a coconut?

145
00:08:01,603 --> 00:08:05,022
I am not going to dignify that
with a response.

146
00:08:06,024 --> 00:08:07,357
Now to the matter at hand.

147
00:08:07,567 --> 00:08:10,319
You need to play with the Transporter toy.

148
00:08:10,528 --> 00:08:12,988
- But it's mint-in-box.
- Yes.

149
00:08:13,198 --> 00:08:15,491
And to open it would destroy its value.

150
00:08:15,700 --> 00:08:18,035
But remember, like me...

151
00:08:18,411 --> 00:08:20,037
...you also have a human half.

152
00:08:20,872 --> 00:08:23,957
Well, I'm not going to dignify that
with a response.

153
00:08:24,167 --> 00:08:26,335
Consider this.

154
00:08:26,794 --> 00:08:29,838
- What is the purpose of a toy?
- To be played with.

155
00:08:30,048 --> 00:08:32,674
Therefore, to not play with it would be...?

156
00:08:32,884 --> 00:08:36,845
Illogical. Oh, damn it, Spock,
you're right.

157
00:08:38,056 --> 00:08:39,264
I'll do it.

158
00:08:39,474 --> 00:08:41,475
Sheldon, wait.

159
00:08:42,810 --> 00:08:44,186
You have to wake up first.

160
00:08:44,812 --> 00:08:46,271
Oh, of course.

161
00:08:46,481 --> 00:08:49,441
Set phasers to "Dumb," right? Ha, ha.

162
00:08:50,735 --> 00:08:52,110
Goodie, goodie, goodie.

163
00:08:54,572 --> 00:08:59,076
This is wrong, this is wrong.
I'm so excited, but this is wrong.

164
00:09:01,663 --> 00:09:03,455
I'm gonna do it.

165
00:09:03,665 --> 00:09:05,457
I'm doing it.

166
00:09:05,667 --> 00:09:07,334
I did it.

167
00:09:10,713 --> 00:09:11,797
Oh.

168
00:09:12,006 --> 00:09:14,341
That's what I always thought
1975 smelled like.

169
00:09:18,471 --> 00:09:20,931
One to beam down, Mr. Scott.

170
00:09:21,140 --> 00:09:23,183
Aye, aye, Mr. Spock.

171
00:09:23,393 --> 00:09:25,477
Energize.

172
00:09:25,979 --> 00:09:27,479
Aah.

173
00:09:28,773 --> 00:09:30,816
Energize.

174
00:09:42,287 --> 00:09:44,079
Don't be broken.

175
00:09:44,289 --> 00:09:47,040
Oh. Please, don't be broken.

176
00:09:48,126 --> 00:09:50,252
What did you make me do?

177
00:09:51,671 --> 00:09:55,257
Okay, okay. Think.

178
00:10:01,222 --> 00:10:03,473
It's only logical.

179
00:10:11,187 --> 00:10:13,230
My goodness, that was delicious.

180
00:10:13,439 --> 00:10:17,734
Well, I hope you saved room
for chocolate lava cake.

181
00:10:17,944 --> 00:10:19,152
Impressive.

182
00:10:19,362 --> 00:10:21,446
What goes into making something
like that?

183
00:10:21,656 --> 00:10:24,449
Well, you start out by trying
to make chocolate soufflé.

184
00:10:24,659 --> 00:10:26,159
And when it falls, you panic...

185
00:10:26,369 --> 00:10:28,870
...quickly change the name,
and voilà, lava cake.

186
00:10:30,248 --> 00:10:33,333
I bet our parents are dying to know
how things are going here.

187
00:10:33,543 --> 00:10:36,002
Well, let's see.

188
00:10:37,088 --> 00:10:38,505
Yep, three missed calls.

189
00:10:38,714 --> 00:10:41,633
Four missed calls, two text messages,
and a failed video chat.

190
00:10:41,843 --> 00:10:43,969
- I win.
- Ha, ha.

191
00:10:44,178 --> 00:10:46,721
So, what are we going to tell them?

192
00:10:47,557 --> 00:10:50,475
- I'd like to tell them things are going well.
- Me too.

193
00:10:50,685 --> 00:10:52,310
But before we get their hopes up...

194
00:10:52,520 --> 00:10:55,355
...we should probably make sure
we're on the same page.

195
00:10:55,565 --> 00:10:56,857
Okay.

196
00:10:57,066 --> 00:10:59,359
What page are you on?

197
00:10:59,569 --> 00:11:03,155
I'm on the one where I'm under
a lot of pressure from my parents...

198
00:11:03,364 --> 00:11:06,074
...to get married and settle down
and have a family.

199
00:11:06,284 --> 00:11:09,619
And I'm going to do it
so they don't find out I'm gay.

200
00:11:12,373 --> 00:11:13,832
Say again?

201
00:11:14,041 --> 00:11:15,208
I'm gay.

202
00:11:15,710 --> 00:11:17,169
Like...

203
00:11:17,378 --> 00:11:20,088
Dude-on-dude, but with women?

204
00:11:20,882 --> 00:11:23,341
I know a fake marriage
isn't an honest way to live...

205
00:11:23,551 --> 00:11:27,012
...but you, of all people, should know
how difficult it is to come out, ya?

206
00:11:27,221 --> 00:11:29,848
Why me "of all people"?

207
00:11:30,057 --> 00:11:32,684
Well, there's a rumor
back in New Delhi that you're...

208
00:11:32,894 --> 00:11:34,019
How shall we say?

209
00:11:34,228 --> 00:11:36,396
...comfortable in a sari.

210
00:11:36,981 --> 00:11:39,524
- I'm not gay.
- Really?

211
00:11:39,734 --> 00:11:42,485
The chocolate lava cake,
the little soaps in the bathroom.

212
00:11:42,695 --> 00:11:45,530
And I'm sorry, but you're wearing
more perfume than I am.

213
00:11:46,240 --> 00:11:48,992
That's Unbreakable by Khloe and Lamar.

214
00:11:50,119 --> 00:11:52,579
And for your information, it's unisex.

215
00:11:53,998 --> 00:11:56,541
Fill in the blank,
"I love the nightlife..."

216
00:11:56,751 --> 00:11:58,585
- I like to boogie.
- Got you.

217
00:11:58,794 --> 00:12:02,172
With women. I like to boogie with women.

218
00:12:03,132 --> 00:12:06,760
That's disappointing. You were exactly
the kind of phony-baloney husband...

219
00:12:06,969 --> 00:12:08,428
...I was looking for.

220
00:12:09,263 --> 00:12:10,472
Thank you.

221
00:12:10,681 --> 00:12:13,767
And once again, my baloney likes girls.

222
00:12:15,686 --> 00:12:18,647
Wait, wait. You don't wanna put
a bite of that in your mouth...

223
00:12:18,856 --> 00:12:21,983
...without trying
my homemade Chantilly cream.

224
00:12:22,568 --> 00:12:24,611
Yeah, okay, that time I heard it.

225
00:12:37,833 --> 00:12:40,335
Oh, dear.

226
00:12:40,836 --> 00:12:44,631
Two suns and no sunscreen.

227
00:12:48,594 --> 00:12:51,137
Hello again, Sheldon.

228
00:12:52,014 --> 00:12:54,766
What is it now, Tiny Spock?

229
00:12:54,976 --> 00:12:56,893
I am very disappointed in you.

230
00:12:57,103 --> 00:12:59,562
You broke your toy
and switched it with Leonard's.

231
00:12:59,772 --> 00:13:01,898
You should be ashamed of yourself.

232
00:13:02,108 --> 00:13:04,109
You're the one who told me
to play with it.

233
00:13:04,318 --> 00:13:08,446
If I told you to jump off the bridge
of the Enterprise, would you do it?

234
00:13:08,656 --> 00:13:09,698
No.

235
00:13:09,907 --> 00:13:13,952
If I got on the bridge of the Enterprise,
I would never ever leave.

236
00:13:14,161 --> 00:13:17,831
Trust me, it gets old after a while.

237
00:13:19,792 --> 00:13:21,459
You must right your wrong, Sheldon.

238
00:13:21,669 --> 00:13:24,170
Why? I got away with it.

239
00:13:24,380 --> 00:13:27,299
Leonard has his toy,
and he's never going to open it.

240
00:13:27,508 --> 00:13:31,011
So he won't know it's broken.
And I have a toy that isn't broken.

241
00:13:31,220 --> 00:13:32,470
Everybody's happy.

242
00:13:32,680 --> 00:13:34,264
Well, I am unhappy.

243
00:13:34,473 --> 00:13:37,225
I thought where you come from
they don't have emotions.

244
00:13:37,977 --> 00:13:41,521
I come from a factory in Taiwan.

245
00:13:42,857 --> 00:13:44,190
Now, do the right thing.

246
00:13:44,692 --> 00:13:46,026
You know what you are?

247
00:13:46,235 --> 00:13:49,154
Well, you're a green-blooded buzzkill.

248
00:13:49,363 --> 00:13:51,448
Perhaps it's time you beam on
out of here.

249
00:13:51,657 --> 00:13:54,534
Fine. I will just use the Transporter.

250
00:13:54,744 --> 00:13:58,246
Oh, right, you broke it.

251
00:13:59,749 --> 00:14:04,210
Very well. Cooper to Enterprise,
one to beam up.

252
00:14:04,420 --> 00:14:05,545
Energize.

253
00:14:16,223 --> 00:14:19,684
Aah! Tiny Spock! Help!

254
00:14:25,441 --> 00:14:27,567
- Ready for lunch?
- Yeah, one sec. Ahh.

255
00:14:28,778 --> 00:14:32,906
Oh, good. Ma would've killed me
if she found out I broke my retainer.

256
00:14:34,950 --> 00:14:38,370
Hey, uh, can I run something by you?
It's about Lakshmi.

257
00:14:38,579 --> 00:14:40,705
Yeah, sure. How are things going?

258
00:14:40,915 --> 00:14:42,082
We hit a couple of bumps.

259
00:14:42,291 --> 00:14:46,044
She lives over in Manhattan Beach,
so it takes like an hour to get there.

260
00:14:46,295 --> 00:14:47,962
And she's a lesbian.

261
00:14:49,715 --> 00:14:51,049
What do you mean,
she's a lesbian?

262
00:14:51,258 --> 00:14:54,552
Well, you know whenever you and I
would try to hit on women in bars...

263
00:14:54,762 --> 00:14:57,764
...they'd blow us off, then we'd tell
each other they probably were gay?

264
00:14:57,973 --> 00:14:59,307
It's like that.

265
00:14:59,517 --> 00:15:01,810
Except this time, it's true.

266
00:15:02,978 --> 00:15:06,272
- Why did she even go out with you?
- She was looking for a husband...

267
00:15:06,482 --> 00:15:08,274
...so she can appear to be straight.

268
00:15:08,484 --> 00:15:11,861
And, you know,
it sounded crazy to me at first...

269
00:15:12,238 --> 00:15:14,823
...but I'm actually
thinking about doing it.

270
00:15:15,366 --> 00:15:20,745
Okay. Well, so the reason that might
sound crazy is because it's crazy!

271
00:15:21,288 --> 00:15:23,248
Look, Howard, you're in a relationship.

272
00:15:23,457 --> 00:15:25,166
You know you have to make
compromises.

273
00:15:25,376 --> 00:15:28,628
Yes, but my compromises
are about which bedspread to buy...

274
00:15:28,838 --> 00:15:32,257
...or whose turn it is to do the laundry,
Bernadette's or my mom's.

275
00:15:36,303 --> 00:15:39,347
It's a great deal.
We both get our parents off our backs.

276
00:15:39,557 --> 00:15:41,975
I don't have to come home to
an empty apartment every night.

277
00:15:42,184 --> 00:15:47,355
Plus, once I'm married, I can finally eat
carbs again and let myself go.

278
00:15:48,023 --> 00:15:52,068
Why don't you tell your parents
you wanna try to find someone else?

279
00:15:52,278 --> 00:15:55,071
You know, one who hasn't slept
with more women than you.

280
00:15:56,198 --> 00:15:58,199
Because this one wants to marry me.

281
00:15:58,409 --> 00:16:00,702
I might never find another one who does.

282
00:16:00,911 --> 00:16:04,873
So, you're seriously thinking about marrying
someone you're never gonna have sex with?

283
00:16:05,666 --> 00:16:07,083
I can't believe your attitude.

284
00:16:07,293 --> 00:16:10,545
I thought you were in favor
of gay people getting married.

285
00:16:11,380 --> 00:16:13,715
Yes, to other gay people.

286
00:16:14,717 --> 00:16:17,677
Do you hear how homophobic you sound?

287
00:16:58,469 --> 00:17:01,471
Oh, I don't want a broken toy.

288
00:17:04,141 --> 00:17:05,934
Nothing.

289
00:17:07,728 --> 00:17:10,522
- What?
- Nothing. I said nothing.

290
00:17:10,731 --> 00:17:13,733
- That was weird.
- Really? I don't even notice anymore.

291
00:17:14,902 --> 00:17:18,363
Ahh, I cannot believe you guys
aren't gonna play with these.

292
00:17:18,572 --> 00:17:20,490
We told you, you don't. It's mint-in-box.

293
00:17:20,699 --> 00:17:22,617
I don't know. I just think it's a waste.

294
00:17:28,207 --> 00:17:30,750
Just relax, I'm just looking at the box.

295
00:17:30,960 --> 00:17:35,630
You should look with your eyes and not
your muscular Nebraska man-hands.

296
00:17:36,924 --> 00:17:38,758
What is your problem?

297
00:17:38,968 --> 00:17:41,886
My problem is that I don't want you
to break Leonard's toy...

298
00:17:42,096 --> 00:17:46,224
...which you probably did by shaking it.
She shook it, we all saw her.

299
00:17:46,767 --> 00:17:50,228
Leonard, I bought you this
because I wanted you to have fun with it.

300
00:17:50,437 --> 00:17:52,689
I didn't want it to sit in this box.

301
00:17:52,898 --> 00:17:54,607
You know what? You're right.

302
00:17:55,276 --> 00:17:58,111
I mean, it's from you.
I'm never gonna sell it.

303
00:17:58,320 --> 00:17:59,362
- I'm opening it.
- Yes.

304
00:18:11,792 --> 00:18:13,459
- It's broken.
- What?

305
00:18:13,669 --> 00:18:15,461
Oh, nice job, man-hands.

306
00:18:17,506 --> 00:18:18,798
I didn't break it.

307
00:18:19,008 --> 00:18:21,134
I guess Stuart sold it to me like this.

308
00:18:21,927 --> 00:18:23,761
Yes. Yes, he did.

309
00:18:23,971 --> 00:18:27,181
That is a perfectly satisfying
and plausible explana...

310
00:18:27,391 --> 00:18:30,226
You know, let's all be mad at Stuart.

311
00:18:30,436 --> 00:18:33,271
I paid a lot for this.
Let's take it over there and show him.

312
00:18:33,480 --> 00:18:34,939
Absolutely.

313
00:18:37,610 --> 00:18:38,651
Wait.

314
00:18:42,489 --> 00:18:44,616
It was me.

315
00:18:44,825 --> 00:18:46,826
I opened your toy...

316
00:18:47,036 --> 00:18:50,330
...discovered it was broken,
and didn't tell you.

317
00:18:51,707 --> 00:18:55,793
- Why would you open mine?
- I didn't. That was a lie.

318
00:18:56,003 --> 00:18:58,630
I opened my own toy...

319
00:18:58,839 --> 00:19:01,924
...and it was already broken,
so I switched them.

320
00:19:02,134 --> 00:19:06,220
- Well, you should talk to Stuart.
- I can't, because that was a lie.

321
00:19:07,931 --> 00:19:12,226
Yours was broken in an earthquake
and that's a lie.

322
00:19:14,647 --> 00:19:16,189
What is the truth?

323
00:19:17,483 --> 00:19:20,526
My Mr. Spock doll came to me
in a dream and forced me to open it.

324
00:19:20,736 --> 00:19:24,781
And when the toy broke,
I switched it for yours.

325
00:19:24,990 --> 00:19:28,159
Later, he encouraged me to do
the right thing and I defied him.

326
00:19:28,369 --> 00:19:31,245
And then I was attacked by a Gorn.

327
00:19:35,042 --> 00:19:37,377
- Okay, that I believe.
- Mm.

328
00:19:37,586 --> 00:19:39,671
Leonard, Penny,
I want you both to know...

329
00:19:39,880 --> 00:19:44,175
...that I regret my actions
towards the two of you. That's a lie.

330
00:19:45,386 --> 00:19:46,761
So is that one mine?

331
00:19:47,721 --> 00:19:49,847
- Yes.
- Well, hand it over so I can open it.

332
00:19:50,474 --> 00:19:51,891
Okay.

333
00:19:52,101 --> 00:19:54,769
And, Leonard,
even though I don't have one anymore...

334
00:19:54,978 --> 00:19:57,563
- ...I hope you have fun playing with it.
- That's a lie?

335
00:19:57,773 --> 00:19:59,232
- A big, fat whopper.
- Yeah.

336
00:20:01,068 --> 00:20:03,194
I hope it breaks.

337
00:20:12,951 --> 00:20:15,619
This is a treat. What brings you guys by?

338
00:20:16,162 --> 00:20:18,914
Raj, Howie told me what's going on
with you and Lakshmi.

339
00:20:19,499 --> 00:20:22,710
- You told her?
- I told everybody.

340
00:20:24,504 --> 00:20:27,464
We believe there's someone out there
who'll love you for you.

341
00:20:27,674 --> 00:20:30,092
We kind of agreed to disagree
on that one.

342
00:20:30,301 --> 00:20:33,512
But we both think
you shouldn't marry this woman.

343
00:20:33,722 --> 00:20:36,390
So while I'm waiting
for this mysterious perfect match...

344
00:20:36,599 --> 00:20:39,852
...who may or may not exist,
I'm supposed to just be alone?

345
00:20:40,061 --> 00:20:41,520
Not necessarily.

346
00:20:41,730 --> 00:20:46,066
I think we found someone for you
to cuddle with.

347
00:20:47,527 --> 00:20:49,111
Oh, my goodness.

348
00:20:49,320 --> 00:20:53,115
Aren't you the cutest little Yorkie ever?

349
00:20:55,160 --> 00:20:56,910
- You got him for me?
- Her.

350
00:20:57,120 --> 00:20:59,705
We thought you two would hit it off.

351
00:20:59,914 --> 00:21:03,417
I think we already have.
Thank you guys so much.

352
00:21:03,626 --> 00:21:06,795
Let's go see if you fit in my man-purse.

353
00:21:09,174 --> 00:21:12,426
Metrosexual, my ass.