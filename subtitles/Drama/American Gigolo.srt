1
00:00:09,900 --> 00:00:11,970
I think I'm in a frame.

2
00:00:12,060 --> 00:00:15,052
- Who's putting you in?
- I don't know.

3
00:00:15,140 --> 00:00:17,529
All I see is a frame.

4
00:00:17,620 --> 00:00:20,418
It's that Rheiman murder
out in Palm Springs.

5
00:00:20,500 --> 00:00:22,570
You tricked with them?

6
00:00:22,660 --> 00:00:25,458
Once. Just once. Favour to Leon.

7
00:00:25,540 --> 00:00:28,930
- Two-timing bastard.
- I'm nice. I can't help it.

8
00:00:29,020 --> 00:00:30,738
You check around for me?

9
00:00:30,820 --> 00:00:33,539
Why don't you ask
your new friend Leon?

10
00:00:33,620 --> 00:00:36,009
I may just do that.

11
00:00:36,100 --> 00:00:39,615
You trick for other people,
cheat me out of money,

12
00:00:39,700 --> 00:00:43,739
and then when you need a favour,
you come back to me.

13
00:00:44,860 --> 00:00:48,569
If I help you...
you'll have to help me.

14
00:00:49,620 --> 00:00:50,609
How?

15
00:00:50,700 --> 00:00:55,410
You'll have to come back to me,
work strictly for me.

16
00:00:55,500 --> 00:00:58,572
Wait a minute.
It's got nothing to do with that.

17
00:00:58,660 --> 00:01:01,811
Like shit it doesn't.
I made you, Julie.

18
00:01:01,900 --> 00:01:05,415
I taught you everything you know,
how to dress, how to behave,

19
00:01:05,500 --> 00:01:07,775
how to treat women, how to make love.

20
00:01:07,860 --> 00:01:11,978
Then you say, "I'm too good for you,"
and cut me out.

21
00:01:12,060 --> 00:01:14,449
It ain't right, Julie.

22
00:01:15,660 --> 00:01:18,811
Things are different.
I'm more than I used to be.

23
00:01:20,460 --> 00:01:24,612
I'm getting older, Anne.
I got to keep moving forward.

24
00:01:24,700 --> 00:01:27,658
Please, Julie, save me the speeches.

25
00:01:29,380 --> 00:01:33,293
- How's your Swedish coming?
- I'll be ready.

26
00:01:33,380 --> 00:01:35,450
She's flying in
from Stockholm on Thursday.

27
00:01:35,540 --> 00:01:40,933
She apparently has certain
sexual idiosyncrasies.

28
00:01:41,020 --> 00:01:44,330
Are you going to come back with me?

29
00:01:47,420 --> 00:01:51,413
- You did it, didn't you?
- What?

30
00:01:51,500 --> 00:01:54,458
The Rheiman killing.

31
00:01:59,980 --> 00:02:04,576
Don't worry, Julie.
It doesn't matter.

32
00:02:12,500 --> 00:02:15,219
Thank you.
It's been a wonderful evening.

33
00:02:20,220 --> 00:02:23,769
- Can I see you again tomorrow?
- How about this one?

34
00:02:23,860 --> 00:02:27,489
There was a sexy
girl from Borgan Place...

35
00:02:30,340 --> 00:02:32,296
Do you live alone?

36
00:02:34,580 --> 00:02:36,969
It's such a sweet face.

37
00:02:45,340 --> 00:02:48,218
- No, not yet.
- Why not?

38
00:02:48,300 --> 00:02:51,372
- Let's just kiss some more, OK?
- What's wrong?

39
00:02:53,380 --> 00:02:56,338
Nothing's wrong. I just don't...
want to make love.

40
00:03:02,660 --> 00:03:05,333
I love to be with you.

41
00:03:05,420 --> 00:03:09,049
I love it when you kiss me
and when you touch me,

42
00:03:09,140 --> 00:03:12,098
but when you make love,
you go to work.

43
00:03:15,460 --> 00:03:17,815
If it's your period, I don't mind.

44
00:03:17,900 --> 00:03:20,892
Don't give me that.
You know what I'm talking about.

45
00:03:20,980 --> 00:03:23,210
I can't give you any pleasure,

46
00:03:23,300 --> 00:03:26,212
and you can't fool me any more.

47
00:03:26,300 --> 00:03:28,291
You don't know what you're saying.

48
00:03:28,380 --> 00:03:31,338
Yes, I do...and so do you.

49
00:03:41,660 --> 00:03:44,970
Yeah, I know.

50
00:03:46,980 --> 00:03:50,893
You're talking about me giving up
what I am, what I do.

51
00:03:51,780 --> 00:03:53,736
Is that so shocking?

52
00:04:00,700 --> 00:04:03,373
I care about you. I really do.

53
00:04:09,220 --> 00:04:11,654
That's still not enough.

54
00:04:15,980 --> 00:04:18,016
It's all I'm good at.

55
00:04:19,100 --> 00:04:21,056
That's not true.

56
00:04:22,380 --> 00:04:25,690
It doesn't matter.
I can't quit anyway.

57
00:04:25,780 --> 00:04:29,216
If I quit, I'm out on a limb.

58
00:04:29,300 --> 00:04:32,975
Well, then we'd be even.
I'm already out on one.

59
00:04:37,740 --> 00:04:40,652
People I know
take care of each other.

60
00:04:43,740 --> 00:04:46,129
I need their protection.

61
00:04:49,940 --> 00:04:54,456
I'm being framed by somebody.
I don't know who.

62
00:04:56,220 --> 00:05:00,259
There was a murder, Palm Springs,
a couple weeks ago.

63
00:05:00,340 --> 00:05:02,331
Police think I did it.

64
00:05:04,100 --> 00:05:07,251
It was a woman. A woman I met once.

65
00:05:08,980 --> 00:05:11,938
I wasn't there,
but my alibi completely denies it.

66
00:05:12,020 --> 00:05:15,979
- Why would anyone do that to you?
- I can't figure that out.

67
00:05:16,060 --> 00:05:19,416
- It doesn't make any sense.
- When was the murder?

68
00:05:19,500 --> 00:05:21,570
A week ago Tuesday.

69
00:05:24,180 --> 00:05:28,731
- Weren't we together that night?
- Baby, I wish we were.

70
00:05:42,980 --> 00:05:45,335
All right,
boys, stand full-face, please.

71
00:05:45,420 --> 00:05:46,933
This is bullshit. These cops...

72
00:05:47,020 --> 00:05:49,580
Number three, take off your glasses.

73
00:05:53,300 --> 00:05:55,416
Number two, straighten up.

74
00:05:56,500 --> 00:05:58,058
Turn to your right.

75
00:06:00,500 --> 00:06:02,775
No, Number three, to your right.

76
00:06:02,860 --> 00:06:04,054
Jerk.

77
00:06:04,140 --> 00:06:06,096
How much you getting paid?

78
00:06:06,180 --> 00:06:09,411
What are you talking about?
I'm just a con.

79
00:06:09,500 --> 00:06:13,049
So am I. I'm getting 15.

80
00:06:14,180 --> 00:06:16,853
- You are?
- Yeah. 15 bucks.

81
00:06:16,940 --> 00:06:18,771
Now left, please.

82
00:06:21,260 --> 00:06:22,579
Left.

83
00:06:25,060 --> 00:06:26,413
Thank you.

84
00:06:29,620 --> 00:06:33,249
- The wino's getting 20.
- You're kidding.

85
00:06:33,340 --> 00:06:36,298
You ain't getting paid,
you're getting screwed.

86
00:06:36,380 --> 00:06:38,735
Shit! What the fuck's going on?

87
00:06:38,820 --> 00:06:41,618
I want to speak to my lawyer!

88
00:06:41,700 --> 00:06:44,294
You'll see your lawyer.
Get in line.

89
00:06:44,380 --> 00:06:46,496
Make sure I speak to him.

90
00:06:46,580 --> 00:06:49,890
Number one, turn to your right.

91
00:06:49,980 --> 00:06:54,337
I know my rights. This isn't Russia.
We have a constitution here.

92
00:06:54,420 --> 00:06:59,050
- I know my rights.
- Thank you. You can leave now.

93
00:06:59,140 --> 00:07:02,018
How about this fuckin' jerk
here, three.

94
00:07:02,100 --> 00:07:05,934
You getting paid, too?
What the fuck is going on?

95
00:07:08,900 --> 00:07:11,892
Well, what do you think?

96
00:07:11,980 --> 00:07:15,290
- Not bad.
- I'm gonna steal your thunder.

97
00:07:15,380 --> 00:07:17,689
He's teaching me how to dress.

98
00:07:17,780 --> 00:07:20,533
This is Lieutenant Curtis
from Palm Springs.

99
00:07:20,620 --> 00:07:22,417
He's...

100
00:07:22,500 --> 00:07:26,778
- He's in charge of this case.
- Wait a minute. I thought you were?

101
00:07:26,860 --> 00:07:30,455
No, I'm in charge
of pimps, prostitutes and hustlers.

102
00:07:30,540 --> 00:07:34,215
Pretty low on the pole.
Good joke, huh?

103
00:07:38,620 --> 00:07:40,338
You've been identified, Julian.

104
00:07:41,820 --> 00:07:43,776
- Julian.
- By whom?

105
00:07:43,860 --> 00:07:46,374
Who could possibly identify me?

106
00:07:46,460 --> 00:07:49,418
You parked 50 feet
from the Rheiman house

107
00:07:49,500 --> 00:07:51,456
around 10:00 on the 22nd,

108
00:07:51,540 --> 00:07:54,896
proceeded up the street
and entered the house.

109
00:07:55,980 --> 00:07:59,450
- Someone saw you.
- I request formal charges...

110
00:07:59,540 --> 00:08:03,499
I don't understand.
Who would say this? I was not there.

111
00:08:03,580 --> 00:08:06,777
Then she identified
subject number one.

112
00:08:07,860 --> 00:08:11,648
Then she wasn't sure
which one of you it was.

113
00:08:15,220 --> 00:08:19,213
- You got the license number?
- She didn't see it.

114
00:08:19,300 --> 00:08:21,052
- Shit!
- Right.

115
00:08:21,140 --> 00:08:22,573
Here.

116
00:08:23,660 --> 00:08:27,130
We looked around your house.
We had a warrant.

117
00:08:27,220 --> 00:08:29,688
- We know.
- You recognise these?

118
00:08:29,780 --> 00:08:32,897
- They were under your mattress.
- So what?

119
00:08:32,980 --> 00:08:37,292
They had Mrs Rheiman's fingerprints
all over them. That's so what.

120
00:08:37,380 --> 00:08:40,338
- Did you find the jewels?
- What jewels?

121
00:08:40,420 --> 00:08:43,730
I read the paper, too.
Jewels stolen from Rheiman's house.

122
00:08:43,820 --> 00:08:46,732
Whoever planted that money
probably planted the jewels.

123
00:08:46,820 --> 00:08:48,378
There were no jewels.

124
00:08:48,460 --> 00:08:51,850
Didn't you ever wonder
this may be a frame?

125
00:08:51,940 --> 00:08:55,649
- It occurred to us.
- It occurred to you.

126
00:08:55,740 --> 00:08:59,130
- Did you check Rheiman?
- He has an alibi.

127
00:08:59,220 --> 00:09:02,849
- Big deal.
- You're grasping at straws.

128
00:09:02,940 --> 00:09:05,613
What about Leon James, the black guy?

129
00:09:05,700 --> 00:09:10,057
- He said you were high-class.
- What do you think?

130
00:09:17,580 --> 00:09:19,775
I think you're guilty as sin.

131
00:09:19,860 --> 00:09:22,613
I think you went to the Rheimans',

132
00:09:22,700 --> 00:09:25,533
did a trick,
played some rough games.

133
00:09:25,620 --> 00:09:28,453
You got drugged or stupid or both.

134
00:09:28,540 --> 00:09:32,613
You beat her, killed her,
stole the money and the jewels.

135
00:09:34,020 --> 00:09:37,774
Then there's no use talking further
until you press charges.

136
00:09:37,860 --> 00:09:41,773
Whether we think Julian's guilty...
we're not going to arrest him.

137
00:09:41,860 --> 00:09:43,532
If you're being framed,

138
00:09:43,620 --> 00:09:47,010
you're gonna be more use to us
floating around.

139
00:09:50,500 --> 00:09:52,058
Can we go now?

140
00:10:03,860 --> 00:10:06,977
Just one more thing, Julian...

141
00:10:08,620 --> 00:10:12,010
don't tell cons
they get paid for line- ups.

142
00:10:12,100 --> 00:10:15,058
Some cops don't have
a sense of humour.

143
00:11:12,620 --> 00:11:15,339
Hi. I'm here
to see Mrs Williams.

144
00:11:15,420 --> 00:11:19,413
- I'm Julian Kay.
- I'll take care of this, Wilma.

145
00:11:19,500 --> 00:11:24,972
- Julian, why are you here?
- Can I talk to you?

146
00:11:25,060 --> 00:11:27,016
What about?

147
00:11:29,260 --> 00:11:31,376
Were the police here?

148
00:11:31,460 --> 00:11:33,576
There was a detective here.

149
00:11:33,660 --> 00:11:37,335
I was very surprised
you used my name.

150
00:11:37,420 --> 00:11:40,810
I'm very, very sorry
about that, Lisa,

151
00:11:40,900 --> 00:11:43,653
but this is very important to me.

152
00:11:43,740 --> 00:11:45,731
I told him the truth.

153
00:11:45,820 --> 00:11:50,496
Lisa, you didn't,
I was with you until after midnight.

154
00:11:50,580 --> 00:11:52,411
That's not the truth.

155
00:11:52,500 --> 00:11:54,855
Julian, I can't...

156
00:11:54,940 --> 00:11:56,737
you weren't here.

157
00:11:56,820 --> 00:11:58,731
What is it, dear?

158
00:11:58,820 --> 00:12:00,139
This is Julian Kay,

159
00:12:00,220 --> 00:12:03,530
the one who is helping
with the decorating.

160
00:12:03,620 --> 00:12:08,410
The boy who said he was with me
the night of that murder.

161
00:12:08,500 --> 00:12:13,016
Mr Kay, it's bad enough
you made a fool of my wife

162
00:12:13,100 --> 00:12:15,056
But then spreading lies...

163
00:12:15,140 --> 00:12:18,052
- Leave us alone.
- I wasn't lying.

164
00:12:18,140 --> 00:12:22,895
Whatever you did, you won't
to get out of it by dragging...

165
00:12:22,980 --> 00:12:25,335
I just came here for the truth.

166
00:12:25,420 --> 00:12:29,811
- I know you're lying.
- How do you know I'm lying?

167
00:12:29,900 --> 00:12:34,690
Because I was here with my wife
the entire evening.

168
00:12:34,780 --> 00:12:36,532
Thank you, Mr Kay.

169
00:13:39,820 --> 00:13:42,015
You're not very good at this.

170
00:13:42,100 --> 00:13:45,297
- What?
- Following people. You're lousy.

171
00:13:45,380 --> 00:13:48,736
- What are you talking about?
- Who's paying you?

172
00:13:48,820 --> 00:13:51,857
- You're not a cop.
- I'm calling the police.

173
00:13:51,940 --> 00:13:54,693
You want to call? Go ahead, call.

174
00:13:54,780 --> 00:13:56,816
What have we got here?

175
00:13:56,900 --> 00:13:59,255
Floyd...Floyd Wicker. Nice name.

176
00:13:59,340 --> 00:14:01,649
Born 7-23-52.

177
00:14:01,740 --> 00:14:03,571
Hawthorne Avenue, LA.

178
00:14:03,660 --> 00:14:05,651
Driver's license, American Express,

179
00:14:05,740 --> 00:14:08,459
Mastercharge,
picture of girlfriend...

180
00:14:08,540 --> 00:14:12,055
So-so,
eyes too close together, Floyd.

181
00:14:12,140 --> 00:14:14,017
What else have we got?

182
00:14:14,100 --> 00:14:16,170
California State
Congressional Library pass,

183
00:14:16,260 --> 00:14:19,775
California State identification,
California...

184
00:14:21,220 --> 00:14:23,609
State Senate pass...

185
00:14:23,700 --> 00:14:25,611
Senator Charles Stratton.

186
00:14:25,700 --> 00:14:29,170
- Who the fuck are you?
- You can read.

187
00:14:33,260 --> 00:14:34,409
Here.

188
00:14:34,500 --> 00:14:36,331
You work for Senator Stratton?

189
00:14:38,660 --> 00:14:40,013
Yes.

190
00:14:42,380 --> 00:14:46,373
- Why are you following me?
- I was told to.

191
00:14:46,460 --> 00:14:49,020
- By the senator?
- Who else?

192
00:14:49,100 --> 00:14:52,809
Why didn't he ask me
about my private life himself?

193
00:14:54,500 --> 00:14:57,856
Huh? You don't know. You don't know.

194
00:14:57,940 --> 00:15:01,410
Well, Floyd, I'm gonna
make this easy for you

195
00:15:01,500 --> 00:15:03,252
so you don't forget.

196
00:15:03,340 --> 00:15:05,376
This is my telephone number.

197
00:15:05,460 --> 00:15:10,011
Now, you tell...the senator
if he wants to talk to me...

198
00:15:10,100 --> 00:15:13,456
he can just give me a call,
all right?

199
00:15:13,540 --> 00:15:16,498
Now, you fuck off
and leave me alone!

200
00:15:46,460 --> 00:15:48,815
I received your message, Mr Kay.

201
00:15:50,620 --> 00:15:53,930
- Thought I'd see you.
- Good evening, Senator.

202
00:15:54,020 --> 00:15:56,614
- A vodka tonic.
- Yes, sir.

203
00:15:58,020 --> 00:15:59,658
This is heartwarming.

204
00:15:59,740 --> 00:16:03,415
Nice to see you haven't lost touch
with your constituency.

205
00:16:03,500 --> 00:16:07,175
I'm not talking as a senator,
but as a husband.

206
00:16:07,260 --> 00:16:09,854
That's too bad. I'm not a wife.

207
00:16:11,700 --> 00:16:13,338
Very funny.

208
00:16:14,420 --> 00:16:16,058
What do you want?

209
00:16:16,140 --> 00:16:19,655
- Want?
- I know who you are.

210
00:16:19,740 --> 00:16:22,300
I can't stop my wife being a fool,

211
00:16:22,380 --> 00:16:25,770
but I can stop her
from being blackmailed.

212
00:16:29,980 --> 00:16:34,292
- I don't know what you mean.
- You don't?

213
00:16:34,380 --> 00:16:38,771
The police think you're involved
in a murder in Palm Springs.

214
00:16:38,860 --> 00:16:43,058
Three days later,
you seduced my wife at a fund-raiser

215
00:16:43,140 --> 00:16:45,734
to blackmail me into helping you.

216
00:16:48,060 --> 00:16:51,416
- You're crazy, mister.
- Am I?

217
00:17:03,980 --> 00:17:07,768
Last night, my wife asked me
about the Rheiman murder.

218
00:17:07,860 --> 00:17:11,170
Didn't make any sense
till I heard about you.

219
00:17:12,300 --> 00:17:14,530
You talked to Michelle about this?

220
00:17:17,420 --> 00:17:19,376
You're not gonna blackmail me.

221
00:17:19,460 --> 00:17:21,610
You don't know anything about this.

222
00:17:21,700 --> 00:17:24,772
But I do know a whore when I see one.

223
00:17:24,860 --> 00:17:26,418
How much?

224
00:17:29,140 --> 00:17:31,495
Not to see your wife again?

225
00:17:35,620 --> 00:17:37,372
5,000.

226
00:17:40,860 --> 00:17:43,454
You'll have the cash in the morning.

227
00:17:47,820 --> 00:17:51,176
I'm seeing Michelle
because I want to see her...

228
00:17:51,260 --> 00:17:53,854
and because she wants to see me.

229
00:17:55,340 --> 00:17:57,979
I don't want a dime from you.

230
00:18:10,780 --> 00:18:12,657
That may be even simpler.

231
00:18:12,740 --> 00:18:16,699
You live off the good graces
of a few people,

232
00:18:16,780 --> 00:18:20,819
like Mrs Andros,
who was at the door a minute ago...

233
00:18:20,900 --> 00:18:23,460
and of places such as this.

234
00:18:23,540 --> 00:18:25,496
You're just a hanger-on.

235
00:18:25,580 --> 00:18:29,698
Unless you want to find
a new crowd to hang onto,

236
00:18:29,780 --> 00:18:31,736
don't see my wife again.

237
00:19:32,620 --> 00:19:35,134
- How you doing?
- Not bad. How about you?

238
00:19:35,220 --> 00:19:37,688
I'm looking for Leon James.
Seen him around?

239
00:19:37,780 --> 00:19:40,419
Yeah. He's probably at Probe.

240
00:19:40,500 --> 00:19:42,695
OK. Thanks. Take care.

241
00:20:12,900 --> 00:20:14,253
Hey, Julie.

242
00:21:05,420 --> 00:21:07,490
Hey, Jason.

243
00:21:07,580 --> 00:21:10,219
I haven't seen you lately.
What brings you here?

244
00:21:10,300 --> 00:21:12,211
Have you seen Leon around?

245
00:21:12,300 --> 00:21:15,576
Oh, yeah.
He was showing off his new boy.

246
00:21:15,660 --> 00:21:18,220
I think he's over there someplace.

247
00:21:18,300 --> 00:21:20,052
OK, thanks.

248
00:21:38,300 --> 00:21:41,212
Hey, Julie!
I thought that was you.

249
00:21:41,300 --> 00:21:44,372
I've been looking for you.
I want to talk.

250
00:21:44,460 --> 00:21:47,054
- Let's go out back.
- OK.

251
00:21:48,300 --> 00:21:50,814
What you doing here?

252
00:21:50,900 --> 00:21:53,414
You...coming from a funeral?

253
00:21:53,500 --> 00:21:56,378
I hear you're
showing off a new boy.

254
00:21:56,460 --> 00:22:00,772
I'm gonna make sure none
of these fruits snatch him from me.

255
00:22:05,020 --> 00:22:07,614
You heard anything
about the Rheiman killing?

256
00:22:07,700 --> 00:22:09,656
That's all I've heard about.

257
00:22:09,740 --> 00:22:12,300
Cops on my ass like white on rice.

258
00:22:12,380 --> 00:22:17,408
- They're giving me the third degree.
- Everybody else, too.

259
00:22:17,500 --> 00:22:19,730
I may need your help.

260
00:22:19,820 --> 00:22:22,015
I don't have an alibi.

261
00:22:23,100 --> 00:22:25,216
You're the one that sent me.

262
00:22:26,860 --> 00:22:28,612
You need one?

263
00:22:29,820 --> 00:22:31,299
Yeah.

264
00:22:31,380 --> 00:22:33,610
I'll see what I can do.

265
00:22:41,140 --> 00:22:43,370
Who do you think killed her?

266
00:22:44,540 --> 00:22:46,451
If I was the police,

267
00:22:46,540 --> 00:22:51,216
I'd be more interested in Rheiman.
He's a freak, you know.

268
00:22:51,300 --> 00:22:55,532
He's a freak...
but he's got an alibi.

269
00:22:55,620 --> 00:22:57,531
An alibi?

270
00:22:58,700 --> 00:23:02,136
Julie, I got something working
tonight, a little thing.

271
00:23:02,220 --> 00:23:05,576
I'll cut you in on it
if you're interested.

272
00:23:05,660 --> 00:23:07,537
Straight?

273
00:23:07,620 --> 00:23:10,293
This time of night? Are you serious?

274
00:23:11,380 --> 00:23:13,974
Leon, I'm sick of doing your shit.

275
00:23:14,060 --> 00:23:17,177
No more fag stuff.
No more kink. Nothing.

276
00:23:17,260 --> 00:23:21,014
- Finito! Got it?
- I heard about your problems.

277
00:23:21,100 --> 00:23:23,056
What problems?

278
00:23:23,140 --> 00:23:26,530
Your clientele, Julie,
your rich pussy.

279
00:23:26,620 --> 00:23:29,896
They won't touch you.
They say it's too hot.

280
00:23:29,980 --> 00:23:34,019
- They're looking for new boys.
- Fuck 'em. Fuck 'em!

281
00:23:34,100 --> 00:23:36,375
Hey, Julie, listen...

282
00:23:36,460 --> 00:23:40,214
you ask me to help you
out of a tight spot,

283
00:23:40,300 --> 00:23:42,177
I'm glad to do it.

284
00:23:42,260 --> 00:23:46,173
But I ask you to do my trick,
you say no.

285
00:23:46,260 --> 00:23:50,173
Now, how you expect me
to help you, huh?

286
00:23:52,060 --> 00:23:54,494
You give me the alibi,
then we'll talk about tricks.

287
00:24:15,900 --> 00:24:19,336
- I have to talk to you.
- Hey, baby.

288
00:24:19,420 --> 00:24:21,615
God, I'm glad to see you.

289
00:24:23,540 --> 00:24:26,816
Are you OK? You look so worn out.

290
00:24:26,900 --> 00:24:28,731
I didn't sleep much.

291
00:24:28,820 --> 00:24:31,971
- Want to go for a walk?
- Yeah.

292
00:24:32,060 --> 00:24:33,618
I'll get my shoes.

293
00:24:37,900 --> 00:24:41,973
What's wrong?
Are you gonna tell me about it?

294
00:24:46,420 --> 00:24:47,614
Hey.

295
00:24:49,020 --> 00:24:51,818
Hey...you.

296
00:24:55,820 --> 00:24:57,697
Come on, tell me.

297
00:24:58,900 --> 00:25:02,210
When I said I didn't want
to make love to you,

298
00:25:02,300 --> 00:25:05,497
I didn't mean it. I want to fuck you.

299
00:25:05,580 --> 00:25:07,889
I always want to fuck you.

300
00:25:17,700 --> 00:25:21,056
- Is that what you came to tell me?
- No.

301
00:25:21,140 --> 00:25:24,450
Charles asked me to leave
for two months,

302
00:25:24,540 --> 00:25:28,852
till the primaries are over.
I'm going to Rome.

303
00:25:33,260 --> 00:25:34,818
Two months...

304
00:25:37,940 --> 00:25:41,137
I'll be back. Wait for me.

305
00:25:45,020 --> 00:25:48,933
I don't know if I can.
The police searched my apartment.

306
00:25:49,020 --> 00:25:51,739
- Maybe Charles...
- That's not what I'm saying.

307
00:26:00,180 --> 00:26:02,375
I don't know what I'm saying.

308
00:26:11,420 --> 00:26:15,254
All my life...I've been
looking for something.

309
00:26:15,340 --> 00:26:17,137
I don't even know what it is.

310
00:26:17,220 --> 00:26:20,053
- Let me...
- Maybe you're what I'm looking for.

311
00:26:25,260 --> 00:26:28,935
I gave Charles my word.
I've got to go.

312
00:26:30,620 --> 00:26:32,815
It'll be all right.

313
00:26:35,100 --> 00:26:36,977
Yeah.

314
00:26:37,060 --> 00:26:39,290
I have to leave.

315
00:31:02,020 --> 00:31:03,499
Shit!

316
00:32:29,220 --> 00:32:32,417
I don't know.
I haven't seen him since yesterday.

317
00:32:32,500 --> 00:32:35,572
- Tell me.
- I don't know. Honest.

318
00:32:35,660 --> 00:32:38,220
The police have been
here all morning.

319
00:32:38,300 --> 00:32:42,179
- Another cop's in his room.
- Does he call in?

320
00:32:42,260 --> 00:32:44,057
I'm not supposed to tell.

321
00:32:44,140 --> 00:32:48,213
OK. Next time he calls in
I've gotta talk to him.

322
00:32:48,300 --> 00:32:50,655
Ask if I can meet him.

323
00:33:02,860 --> 00:33:06,250
Do you know a blonde kid
that hangs around with Leon James?

324
00:33:06,340 --> 00:33:08,410
Nah. Couldn't tell you.

325
00:33:24,580 --> 00:33:28,653
Anne, I'm sorry.
I've got to talk to you.

326
00:33:29,740 --> 00:33:32,459
I can't now.
I'm looking for somebody.

327
00:33:32,540 --> 00:33:34,974
Perino's? Look, I don't have time.

328
00:33:36,660 --> 00:33:39,732
Yeah, all right, Perino's. Yes, 8:30.

329
00:33:53,260 --> 00:33:55,330
Jill, this is Julian.

330
00:33:56,820 --> 00:33:59,857
Yeah, I know.
You got some messages for me?

331
00:34:03,780 --> 00:34:05,338
Michelle?

332
00:34:09,580 --> 00:34:11,935
Tell her to meet me at Perino's.

333
00:34:16,300 --> 00:34:18,291
Son of a bitch, it's Rheiman.

334
00:34:50,220 --> 00:34:53,178
Mr Julian.
You can't come in like this.

335
00:34:53,260 --> 00:34:55,091
I was working. I forgot.

336
00:34:55,180 --> 00:34:58,138
- I'll get you a jacket.
- No, it's OK.

337
00:35:03,860 --> 00:35:06,420
Julie, you look like shit.
What's wrong?

338
00:35:06,500 --> 00:35:09,776
- Everything. Straight bourbon.
- What are you doing?

339
00:35:09,860 --> 00:35:13,739
The way you look
you couldn't get a maid to fuck you.

340
00:35:13,820 --> 00:35:18,655
Anne, the cops are after me. They're
framing me for the Rheiman murder.

341
00:35:18,740 --> 00:35:21,493
- Where have you been?
- Looking for somebody.

342
00:35:21,580 --> 00:35:23,332
Last night?

343
00:35:23,420 --> 00:35:27,618
Mrs Farfter came in last night
on a flight from Stockholm.

344
00:35:27,700 --> 00:35:31,739
There was no one there to meet her.
She was furious.

345
00:35:31,820 --> 00:35:34,698
I've got hell to pay. What happened?

346
00:35:34,780 --> 00:35:36,498
Did you hear me?

347
00:35:36,580 --> 00:35:40,095
I said the cops are after me...
for murder.

348
00:35:40,180 --> 00:35:42,136
So you stand me up.

349
00:35:42,220 --> 00:35:44,131
Jesus Christ!

350
00:35:46,300 --> 00:35:47,858
What do you want?

351
00:35:53,060 --> 00:35:54,812
Are you gonna help me?

352
00:35:55,900 --> 00:36:00,371
I've checked around, Julie.
You are in trouble.

353
00:36:00,460 --> 00:36:02,655
That's why I need your help.

354
00:36:02,740 --> 00:36:05,300
You never did anything for me.

355
00:36:05,380 --> 00:36:07,132
Never did anything for...

356
00:36:07,220 --> 00:36:11,691
- I'm your number-one boy.
- And you fight me every turn.

357
00:36:11,780 --> 00:36:16,217
You ask favours, then blow a gig
I worked six months to arrange.

358
00:36:16,300 --> 00:36:19,372
- I explained that already.
- I'm through, Julie.

359
00:36:19,460 --> 00:36:21,655
You'll have to fend for yourself.

360
00:36:21,740 --> 00:36:25,096
I don't care
what happens to you any more.

361
00:36:37,180 --> 00:36:39,136
Excuse me. I'll be back.

362
00:36:52,460 --> 00:36:56,738
Julian, thank God you got my message.
I've been looking for you.

363
00:36:56,820 --> 00:36:59,015
It's all right, baby. It's OK.

364
00:36:59,100 --> 00:37:02,775
No, it's not all right.
Charles talked to the DA.

365
00:37:02,860 --> 00:37:06,136
They found the weapon.
They're going to arrest you.

366
00:37:06,220 --> 00:37:08,450
That can't be. It can't be.

367
00:37:08,540 --> 00:37:10,849
What are you going to do?

368
00:37:10,940 --> 00:37:12,817
I don't know.

369
00:37:17,980 --> 00:37:20,289
What can I do to help?

370
00:37:20,380 --> 00:37:23,338
- Nothing.
- There's gotta be something.

371
00:37:23,420 --> 00:37:26,173
Don't you understand?
I'm in big trouble.

372
00:37:26,260 --> 00:37:28,251
- Just stay away.
- No.

373
00:37:28,340 --> 00:37:32,049
There's gonna be a scandal.
You know what a scandal is?

374
00:37:32,140 --> 00:37:35,257
You know what that's like?
Stay away from me.

375
00:37:35,340 --> 00:37:38,935
I'll ruin your life, your husband's.
It's all over.

376
00:37:39,020 --> 00:37:40,658
Not if you're innocent.

377
00:37:41,940 --> 00:37:46,775
How do you know I'm innocent?
Look at me, right in the face

378
00:37:46,860 --> 00:37:50,535
Can you tell me without hesitation
you know I'm innocent?

379
00:37:50,620 --> 00:37:52,178
Yes.

380
00:37:58,180 --> 00:37:59,738
Listen, forget me.

381
00:37:59,820 --> 00:38:01,412
Is someone in there?

382
00:38:01,500 --> 00:38:03,058
I never loved you.

383
00:40:13,740 --> 00:40:16,937
- Julie?
- Let me in. It's important.

384
00:40:21,180 --> 00:40:25,219
You look terrible.
You want to clean up, shave?

385
00:40:25,300 --> 00:40:29,816
I'm sorry to bug you this late.
I've been looking for you.

386
00:40:32,100 --> 00:40:33,658
I'm listening.

387
00:40:35,300 --> 00:40:39,418
You got my alibi ready?
I'm gonna need it today.

388
00:40:39,500 --> 00:40:42,139
It's not ready yet, Julie. Tomorrow.

389
00:40:42,220 --> 00:40:44,415
I'm still piecing it together.

390
00:40:45,580 --> 00:40:49,175
It's not easy.
I'm trying to get it right.

391
00:40:49,260 --> 00:40:52,491
- Why are you framing me?
- What?

392
00:40:52,580 --> 00:40:57,096
Judy Rheiman's murder...
you were behind it. You, Rheiman,

393
00:40:57,180 --> 00:41:00,456
your cute little blonde
boyfriend in the bedroom there,

394
00:41:00,540 --> 00:41:03,338
who planted the jewels in my car.

395
00:41:03,420 --> 00:41:05,888
I don't know
what you're talking about.

396
00:41:05,980 --> 00:41:08,733
What the fuck is that, then?

397
00:41:08,820 --> 00:41:12,256
He's the one that
actually killed her, isn't he?

398
00:41:14,420 --> 00:41:16,650
Leon, what happened?

399
00:41:16,740 --> 00:41:20,574
You sent him out there
with Rheiman and his wife,

400
00:41:20,660 --> 00:41:23,333
he got rough and he killed her.

401
00:41:23,420 --> 00:41:25,297
Shut your fucking face!

402
00:41:25,380 --> 00:41:28,611
I never saw that shit
before in my life.

403
00:41:28,700 --> 00:41:30,930
I want to know one thing.

404
00:41:31,020 --> 00:41:34,456
What will it cost
to get you off my back?

405
00:41:34,540 --> 00:41:37,100
You ain't talking money,
you're talking murder.

406
00:41:37,180 --> 00:41:40,092
- How much?
- I feel sorry for you.

407
00:41:40,180 --> 00:41:41,408
How much?

408
00:41:43,020 --> 00:41:46,376
It don't make no difference
how much, Julie.

409
00:41:46,460 --> 00:41:49,338
The other side will always pay more.

410
00:42:06,820 --> 00:42:09,015
Got it on real tight, huh?

411
00:42:13,540 --> 00:42:16,691
You're real proud
of yourself, aren't you?

412
00:42:16,780 --> 00:42:20,739
The police found the murder weapon
a block from your apartment.

413
00:42:21,820 --> 00:42:23,811
That bitch in Palm Springs

414
00:42:23,900 --> 00:42:28,815
is gonna change her mind
any time now and identify you.

415
00:42:29,900 --> 00:42:33,336
Police want you real bad, Julie.

416
00:42:33,420 --> 00:42:35,376
Now they got you.

417
00:42:51,700 --> 00:42:55,978
Look, I'm worth maybe...
20, 30 grand. It's yours.

418
00:43:00,740 --> 00:43:03,413
I'll break your new boys for you.

419
00:43:06,540 --> 00:43:08,895
I'll work just for you.

420
00:43:08,980 --> 00:43:10,732
Just for you, Leon,

421
00:43:10,820 --> 00:43:13,334
40/60 split.

422
00:43:13,420 --> 00:43:15,490
It's fair, 40/60.

423
00:43:16,580 --> 00:43:18,093
40...

424
00:43:19,580 --> 00:43:21,536
30/70. I'll...

425
00:43:23,300 --> 00:43:25,894
I'll do fag tricks. I'll do kink.

426
00:43:25,980 --> 00:43:28,699
I'll do anything you want me to!

427
00:43:30,580 --> 00:43:33,617
Leave me alone and get out, Julie.

428
00:43:39,780 --> 00:43:41,532
Why me?

429
00:43:43,500 --> 00:43:45,491
Why did you pick me?

430
00:43:46,620 --> 00:43:50,693
Because you were framable, Julie.

431
00:43:52,620 --> 00:43:55,532
You stepped on too many toes.

432
00:43:55,620 --> 00:43:58,180
Nobody cared about you.

433
00:43:59,260 --> 00:44:02,252
I never liked you much myself.

434
00:44:04,140 --> 00:44:05,971
Now get out.

435
00:44:11,820 --> 00:44:14,573
Who's down there?

436
00:44:16,100 --> 00:44:18,614
Your new boyfriend? You motherfucker!

437
00:44:22,300 --> 00:44:23,938
Oh, my God!

438
00:44:24,020 --> 00:44:28,536
Don't let me fall!
Please, Julie, help me!

439
00:44:28,620 --> 00:44:31,293
Don't fall. I need you.

440
00:44:31,380 --> 00:44:33,177
Give me your hand.

441
00:44:33,260 --> 00:44:35,615
- Reach up.
- I'm trying.

442
00:44:36,700 --> 00:44:38,577
Help me. Help me, please!

443
00:44:38,660 --> 00:44:42,130
God! Don't fall!

444
00:45:53,620 --> 00:45:56,214
Julian!
Would you answer a few questions?

445
00:45:57,300 --> 00:46:00,736
- Who was involved?
- How old are you?

446
00:46:00,820 --> 00:46:04,335
How long had you
been seeing Mrs Rheiman?

447
00:46:04,420 --> 00:46:09,050
- Any Hollywood notables?
- How much money have you made?

448
00:46:09,140 --> 00:46:11,938
Julian, give me an exclusive.

449
00:46:12,020 --> 00:46:15,695
How much do you get for a trick?

450
00:46:15,780 --> 00:46:18,578
Give me a break, Julian.

451
00:46:18,660 --> 00:46:21,891
Do you feel
you're being framed, Julian?

452
00:46:21,980 --> 00:46:25,017
Were you ever afraid of the husbands?

453
00:46:49,140 --> 00:46:51,893
I wish I had arrested you earlier.

454
00:46:51,980 --> 00:46:54,210
It would've been easier
for everybody.

455
00:46:54,300 --> 00:46:56,689
You'd better end
this pretrial publicity.

456
00:46:56,780 --> 00:46:59,738
We're moving him
to Palm Springs soon.

457
00:47:02,460 --> 00:47:04,735
You have anything to say, Julian?

458
00:47:15,540 --> 00:47:17,292
Julian?

459
00:47:21,380 --> 00:47:22,733
No.

460
00:47:55,740 --> 00:47:57,492
Hello, Julian.

461
00:47:58,780 --> 00:48:00,771
Going away on your trip?

462
00:48:00,860 --> 00:48:03,249
No. I decided not to.

463
00:48:05,580 --> 00:48:08,970
- Does your husband know you're here?
- No.

464
00:48:10,860 --> 00:48:12,452
Why did you come?

465
00:48:15,020 --> 00:48:18,296
I didn't think
I'd ever see you again.

466
00:48:22,580 --> 00:48:24,411
Don't come back, Michelle.

467
00:48:32,820 --> 00:48:35,095
Are you satisfied with your lawyer?

468
00:48:36,540 --> 00:48:38,212
It doesn't matter.

469
00:48:39,460 --> 00:48:41,098
Don't go.

470
00:49:03,740 --> 00:49:07,699
Michelle Stratton is paying
for my services, Julian.

471
00:49:07,780 --> 00:49:11,853
I think you know this is
a matter of some secrecy.

472
00:49:15,620 --> 00:49:17,497
Tell her not to bother.

473
00:49:17,580 --> 00:49:20,174
Julian, you'll have to help us more.

474
00:49:23,380 --> 00:49:25,575
You tell Michelle to forget me.

475
00:49:28,220 --> 00:49:31,018
I'm more worried
about your defence, Julian,

476
00:49:31,100 --> 00:49:33,773
than about her private life.

477
00:49:33,860 --> 00:49:37,933
We'll must go into the details
the night of the murder.

478
00:49:38,020 --> 00:49:40,978
The maid saw you trying to save Leon,

479
00:49:41,060 --> 00:49:44,370
so the police are only pressing
the Rheiman case.

480
00:49:44,460 --> 00:49:47,577
The papers and TV
are trying you already.

481
00:49:47,660 --> 00:49:50,732
The police aren't
investigating any more.

482
00:49:50,820 --> 00:49:54,335
We need some help, some facts,

483
00:49:54,420 --> 00:49:57,253
to get the investigation going again.

484
00:49:57,340 --> 00:50:00,252
Then we can find out
about Leon, Rheiman...

485
00:50:00,340 --> 00:50:01,853
maybe the blonde boy.

486
00:50:03,700 --> 00:50:05,452
How about it, Julian?

487
00:50:20,140 --> 00:50:21,493
Lieutenant Curtis?

488
00:50:21,580 --> 00:50:24,048
Yes, Mrs Stratton. This is an honour.

489
00:50:24,140 --> 00:50:25,778
May I sit?

490
00:50:26,980 --> 00:50:30,256
I was told you wanted to see me.

491
00:50:30,340 --> 00:50:34,777
I want to talk to you about
the Julian Kay/Rheiman murder case.

492
00:50:34,860 --> 00:50:36,418
Yes.

493
00:50:36,500 --> 00:50:38,855
I'm paying
for Mr Kay's defence.

494
00:50:39,940 --> 00:50:41,578
I know.

495
00:50:41,660 --> 00:50:46,176
I'm paying for Mr Kay's defence
because I know he's not guilty.

496
00:50:46,260 --> 00:50:48,820
I don't want
an obstruction of justice.

497
00:50:48,900 --> 00:50:51,414
How do you know he's not guilty?

498
00:50:51,500 --> 00:50:54,651
Because he wasn't
in Palm Springs that night.

499
00:50:54,740 --> 00:50:58,050
- No?
- No. He was with me.

500
00:51:13,460 --> 00:51:16,497
I told them, Julian.

501
00:51:16,580 --> 00:51:18,935
I heard.

502
00:51:20,620 --> 00:51:23,373
You didn't have to do that, Michelle.

503
00:51:23,460 --> 00:51:25,655
You could have forgotten me.

504
00:51:25,740 --> 00:51:27,696
I'd rather die.

505
00:51:32,140 --> 00:51:34,700
What will you do now?

506
00:51:34,780 --> 00:51:38,614
I don't know. I can't go home.

507
00:51:38,700 --> 00:51:41,897
The newsmen
are waiting outside for me.

508
00:51:41,980 --> 00:51:43,936
There's dozens of them.

509
00:51:48,180 --> 00:51:50,057
Why did you do it?

510
00:51:52,420 --> 00:51:54,650
I had no choice.

511
00:51:55,820 --> 00:51:57,776
I love you.

512
00:52:05,380 --> 00:52:07,655
My God, Michelle...

513
00:52:09,660 --> 00:52:12,970
it's taken me so long
to come to you.

514
00:54:52,540 --> 00:54:55,498
English

