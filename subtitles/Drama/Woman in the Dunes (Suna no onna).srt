1
00:01:10,209 --> 00:01:14,349
WOMAN OF THE DUNES

2
00:05:14,050 --> 00:05:16,439
What a terrible place to live

3
00:05:41,610 --> 00:05:43,220
Are you inspecting?

4
00:05:45,089 --> 00:05:46,279
lnspecting?

5
00:05:48,769 --> 00:05:52,069
ls there an inspection?

6
00:05:53,050 --> 00:05:55,959
Well, if you're not inspecting...

7
00:05:59,209 --> 00:06:01,240
l'm collecting insects

8
00:06:03,209 --> 00:06:08,339
My speciality is these sand insects

9
00:06:10,850 --> 00:06:13,339
Then you're not from the government

10
00:06:14,050 --> 00:06:18,189
The government? No, l'm
a schoolteacher

11
00:06:20,370 --> 00:06:24,300
A teacher... l see

12
00:07:41,689 --> 00:07:45,060
One needs so many corroborating
certificates

13
00:07:46,610 --> 00:07:49,449
ldentity card, usage permit,
contract...

14
00:07:50,050 --> 00:07:53,100
...union card, letters of
recommendation...

15
00:07:54,449 --> 00:07:59,610
...income and custody certificates,
even a pedigree certificate

16
00:08:08,610 --> 00:08:11,629
Aren't there anymore certificates?

17
00:08:13,850 --> 00:08:16,480
Have l overlooked any?

18
00:08:20,050 --> 00:08:22,149
No more to certify?

19
00:08:23,050 --> 00:08:26,910
Who knows the last certificate?

20
00:08:28,050 --> 00:08:31,379
Certificates are limitless, it seems

21
00:08:33,049 --> 00:08:35,960
There's no end to them

22
00:08:38,690 --> 00:08:43,399
You criticised me, you said
I argued too much

23
00:08:44,690 --> 00:08:47,009
But these are facts, not theories

24
00:08:54,690 --> 00:08:56,480
What now, Professor?

25
00:08:57,450 --> 00:09:01,279
Well, l'll come back again tomorrow

26
00:09:03,049 --> 00:09:05,299
But the last bus has gone

27
00:09:08,049 --> 00:09:11,490
Well, can't l stay somewhere?

28
00:09:13,370 --> 00:09:14,980
ln this village?

29
00:09:18,049 --> 00:09:20,679
Maybe along the main road somewhere

30
00:09:22,409 --> 00:09:25,669
-You're walking?
-l'm in no hurry

31
00:09:27,450 --> 00:09:32,049
You needn't go so far; l'll be glad
to help if l can

32
00:09:33,610 --> 00:09:37,049
This is a poor village, but
if you're not fussy...

33
00:09:38,009 --> 00:09:42,120
Really? That would give me more time
tomorrow

34
00:09:42,929 --> 00:09:47,250
And l'd very much like to stay in
a house in such a village

35
00:10:28,289 --> 00:10:31,269
Careful, there are deep holes

36
00:10:46,370 --> 00:10:50,620
Aunt, here's a guest for you,
hurry up

37
00:10:53,009 --> 00:10:55,360
l'm just coming

38
00:10:57,289 --> 00:11:00,730
-Are you sure?
-lt's no trouble at all

39
00:11:07,690 --> 00:11:12,220
-A rope ladder?
-lt must seem strange to you

40
00:11:15,049 --> 00:11:17,370
lt's really quite an adventure

41
00:11:17,850 --> 00:11:21,360
Mind how you go, don't look up

42
00:11:22,450 --> 00:11:24,870
You'll get sand in your eyes

43
00:12:15,049 --> 00:12:17,120
This way, please

44
00:12:35,049 --> 00:12:36,629
Welcome, guest

45
00:12:37,009 --> 00:12:38,690
Pleased to meet you

46
00:13:01,889 --> 00:13:03,259
Dinner will be ready soon

47
00:13:05,450 --> 00:13:08,149
Could l have a bath first?

48
00:13:08,690 --> 00:13:10,509
Yes, but...

49
00:13:11,049 --> 00:13:12,700
Never mind, then

50
00:13:13,129 --> 00:13:15,340
The day after tomorrow

51
00:13:17,929 --> 00:13:21,370
That's hopeless, l've only got
three days leave

52
00:13:22,009 --> 00:13:23,480
l'm sorry

53
00:14:10,129 --> 00:14:13,919
-Only one lamp?
-l'll soon be finished here

54
00:14:15,610 --> 00:14:17,639
You miss electricity

55
00:14:35,529 --> 00:14:40,519
Shellfish broth and bream -
local food is best

56
00:14:44,850 --> 00:14:47,799
-Why the umbrella?
-To keep the sand off

57
00:14:53,289 --> 00:14:58,100
-ls the roof damaged?
-No, it's the same with a new roof

58
00:14:59,690 --> 00:15:00,950
Why, then?

59
00:15:01,769 --> 00:15:05,000
lt's more pesky than a gribble

60
00:15:07,049 --> 00:15:08,279
Gribble?

61
00:15:09,690 --> 00:15:12,850
lt makes holes in wood

62
00:15:14,129 --> 00:15:16,659
A termite

63
00:15:17,289 --> 00:15:20,269
No, it's this big and scaly

64
00:15:22,769 --> 00:15:25,259
That's pri'conus insularis

65
00:15:32,450 --> 00:15:35,750
lsn't it red, with long antennae?

66
00:15:36,529 --> 00:15:40,250
No, bronze, like a grain of rice

67
00:15:42,009 --> 00:15:44,820
chalcophora japonica, perhaps

68
00:15:47,370 --> 00:15:52,360
lf it gets a hold, it will rot away
a huge beam

69
00:15:54,769 --> 00:15:58,210
-chalcophora japonica?
-No, the sand

70
00:16:01,929 --> 00:16:03,399
How can sand...

71
00:16:03,929 --> 00:16:06,139
Well, it draws water

72
00:16:07,929 --> 00:16:12,389
lmpossible. Sand is a naturally dry
substance

73
00:16:13,370 --> 00:16:15,539
Still, it rots things

74
00:16:17,690 --> 00:16:20,850
Don't talk rubbish.
Use your common sense

75
00:16:26,129 --> 00:16:29,919
A desert is a desert because it's dry

76
00:16:32,850 --> 00:16:36,080
Have you ever heard of a damp desert?

77
00:16:36,769 --> 00:16:38,450
Some tea, please

78
00:17:02,769 --> 00:17:05,190
Still, it does rot things

79
00:17:11,769 --> 00:17:16,369
lf you leave sand on clogs,
they rot within a month

80
00:17:24,369 --> 00:17:26,190
What nonsense

81
00:17:27,609 --> 00:17:30,380
Wood rots, and so does sand

82
00:17:31,369 --> 00:17:36,920
The roof of a buried house became
soil rich enough to grow cucumbers

83
00:17:40,049 --> 00:17:42,299
Well, never mind

84
00:17:57,529 --> 00:17:59,740
-Enough?
-Yes, thank you

85
00:18:03,049 --> 00:18:04,799
That was good

86
00:18:15,650 --> 00:18:17,579
lt is serious

87
00:18:19,849 --> 00:18:25,289
When the wind blows, one or two feet
of sand drift in a night

88
00:18:59,769 --> 00:19:02,539
Even the bedding is damp

89
00:19:04,289 --> 00:19:05,410
No others?

90
00:19:08,210 --> 00:19:12,460
-No family?
-No, l'm alone

91
00:19:20,289 --> 00:19:24,680
There was a storm. The sand
swallowed my husband and daughter

92
00:19:26,130 --> 00:19:27,500
Swallowed?

93
00:19:27,849 --> 00:19:33,289
lt roared down like a waterfall.
So they went out

94
00:19:35,609 --> 00:19:38,630
To rescue the chicken house and...

95
00:19:40,369 --> 00:19:41,910
They were buried?

96
00:19:44,049 --> 00:19:45,769
Such a wind

97
00:19:50,450 --> 00:19:54,700
That's terrible... really terrible

98
00:20:05,289 --> 00:20:06,829
lt's the sand

99
00:20:23,009 --> 00:20:24,450
Doing research?

100
00:20:27,849 --> 00:20:31,750
Have you seen this insect here?

101
00:20:34,130 --> 00:20:36,380
lt's called the blister-beetle

102
00:20:42,609 --> 00:20:47,809
l'm collecting the genus on this trip;
it has many species

103
00:20:49,009 --> 00:20:52,519
My name will be in the encyclopaedia
if l find a new one

104
00:20:54,289 --> 00:20:58,079
My only talent is to hold
insects' tails

105
00:21:03,690 --> 00:21:06,920
The tools for the helper

106
00:21:11,609 --> 00:21:16,529
-So there is someone else
-You do say nice things

107
00:21:17,369 --> 00:21:20,279
Didn't he talk about a helper?

108
00:21:20,849 --> 00:21:22,779
He meant you

109
00:21:23,210 --> 00:21:27,069
-Me? Why me?
-lt's all right, take no notice

110
00:21:28,009 --> 00:21:29,619
Some misunderstanding?

111
00:21:30,930 --> 00:21:35,250
-Do you need the lamp?
-Well, l'd rather have it

112
00:21:37,450 --> 00:21:40,569
,
Never mind, l'm used to the work
anyway

113
00:21:57,210 --> 00:21:59,170
This is filthy

114
00:24:06,930 --> 00:24:08,579
No

115
00:24:35,690 --> 00:24:39,269
-Can l help?
-lt's not right on the first day

116
00:24:39,930 --> 00:24:41,960
What do you mean, ''first day''?

117
00:24:43,049 --> 00:24:45,079
l'm only staying tonight

118
00:24:49,210 --> 00:24:50,960
You always clear sand at night?

119
00:24:51,369 --> 00:24:55,299
lt's easier when the sand is damp

120
00:25:01,690 --> 00:25:03,369
The basket

121
00:26:21,049 --> 00:26:23,150
What a job they have

122
00:26:24,210 --> 00:26:28,140
ln our village, the Native Place
spirit is strong

123
00:26:28,930 --> 00:26:30,680
What spirit?

124
00:26:32,210 --> 00:26:34,460
Love of One's Native Place

125
00:26:36,450 --> 00:26:39,150
l see. That's fine

126
00:26:45,809 --> 00:26:48,480
-When do you stop?
-At dawn

127
00:26:51,369 --> 00:26:54,349
The sand doesn't wait for us

128
00:27:24,769 --> 00:27:27,119
What a ruin

129
00:31:44,009 --> 00:31:46,680
That's funny, the ladder's gone

130
00:33:12,210 --> 00:33:16,710
Madam... please get up

131
00:33:30,849 --> 00:33:35,490
Excuse me, but where's the ladder?
It's disappeared

132
00:33:43,369 --> 00:33:48,250
l must leave now; l've only
three days leave

133
00:33:55,849 --> 00:33:58,480
lt was a rope ladder

134
00:34:00,009 --> 00:34:02,259
lt can't be put up from here

135
00:34:08,050 --> 00:34:10,369
You were in league with them

136
00:34:41,050 --> 00:34:42,349
l'm sorry

137
00:34:43,530 --> 00:34:46,019
What do you mean, ''sorry''?

138
00:34:51,449 --> 00:34:53,130
What do you mean?

139
00:34:54,289 --> 00:34:55,659
l'm sorry

140
00:34:56,449 --> 00:35:01,119
That means nothing. l have to make
the most of my time

141
00:35:03,530 --> 00:35:06,269
Don't you understand yet?

142
00:35:06,929 --> 00:35:09,489
What is there to understand?

143
00:35:13,369 --> 00:35:18,920
But, really, life here is too hard
for a woman alone

144
00:35:20,130 --> 00:35:21,780
What's that to do with me?

145
00:35:25,530 --> 00:35:30,449
The winds are coming;
there'll be sandstorms

146
00:35:33,010 --> 00:35:35,150
l'm your prisoner?

147
00:35:35,690 --> 00:35:36,849
l'm sorry

148
00:35:37,289 --> 00:35:41,150
Listen, l'm not a tramp, you know

149
00:35:41,849 --> 00:35:45,010
l've got a respectable job,
a registered address

150
00:35:46,530 --> 00:35:51,199
When the police find me, you'll be
charged with illegal confinement

151
00:36:07,050 --> 00:36:11,400
The sand will bury the house,
if we don't clear it

152
00:36:12,929 --> 00:36:16,760
Then let it. Why involve me?

153
00:36:22,449 --> 00:36:25,429
The next house will be in danger
if this is buried

154
00:36:26,130 --> 00:36:29,710
So it's buried. You, too

155
00:36:32,929 --> 00:36:38,510
Why do you slave to cling to such
a place? Are you insane?

156
00:36:49,250 --> 00:36:53,150
Why be faithful to the villagers?

157
00:36:54,210 --> 00:36:56,949
l've no sympathy for such
old, ''noble'' legends

158
00:36:57,610 --> 00:37:02,070
lf the village is afraid of
sand erosion, plant trees

159
00:37:02,849 --> 00:37:06,820
They proved it's much cheaper this way

160
00:37:10,530 --> 00:37:15,730
Call someone who's in authority here.
l can tell him a few things

161
00:37:19,090 --> 00:37:24,219
Come on. How do you contact
people outside?

162
00:37:25,849 --> 00:37:28,550
Bang on a tin can?

163
00:37:33,610 --> 00:37:35,010
Say something

164
00:38:22,289 --> 00:38:24,989
At least l'm not going against nature...

165
00:38:25,530 --> 00:38:28,579
...l'm not trying to make
a gentle slope steep

166
00:41:23,050 --> 00:41:25,010
Guest, are you awake?

167
00:41:49,289 --> 00:41:51,320
How do you feel?

168
00:41:51,849 --> 00:41:54,059
Not too bad

169
00:41:55,050 --> 00:41:56,630
l'll wipe your back

170
00:42:26,449 --> 00:42:29,960
That hurts, l tell you

171
00:42:38,449 --> 00:42:39,320
Shall l rub it?

172
00:42:39,929 --> 00:42:42,670
lt's no joke. My spine may be broken

173
00:42:43,210 --> 00:42:46,019
lf you're so kind, why not call
a doctor?

174
00:42:54,849 --> 00:42:56,460
ls my underwear dry?

175
00:42:56,849 --> 00:43:00,469
lt's better not to wear underwear
when you're asleep

176
00:43:01,929 --> 00:43:03,860
You'll get sand rash

177
00:43:06,530 --> 00:43:08,739
Sand draws water

178
00:43:30,289 --> 00:43:32,679
Don't make a sound

179
00:44:07,690 --> 00:44:11,869
You'll have to put up with it.
Now l've trapped you

180
00:44:13,010 --> 00:44:14,800
lt's your own fault

181
00:44:17,570 --> 00:44:20,099
A human being is not a dog

182
00:44:21,289 --> 00:44:24,199
lt's not the same as chaining up
a dog

183
00:45:15,130 --> 00:45:17,300
What are you doing?

184
00:45:18,010 --> 00:45:20,150
Still in bed, you two?

185
00:45:24,849 --> 00:45:27,199
l won't let go until you pull this up

186
00:45:27,769 --> 00:45:31,489
She's roped up indoors. Pull this up
if you want to save her

187
00:46:38,210 --> 00:46:41,860
No. They haven't won yet

188
00:46:43,369 --> 00:46:48,110
The battle has just begun. It's them
who are going to be in trouble

189
00:47:33,369 --> 00:47:35,690
Why should I be nervous?

190
00:47:36,769 --> 00:47:39,469
lt's me who's pinning them down,
after all

191
00:47:40,929 --> 00:47:43,489
This is real/y a good e Kperi'ence

192
00:47:44,010 --> 00:47:47,559
When I get home I might write
a story about all this

193
00:48:56,849 --> 00:48:58,989
Only cheap sake

194
00:49:01,210 --> 00:49:05,739
So they want me to drink to my
success; how considerate

195
00:49:14,210 --> 00:49:18,739
There's been a coup d'état.
It seems no one can stand still

196
00:49:23,369 --> 00:49:24,739
A cigarette?

197
00:49:25,369 --> 00:49:28,880
No, they make your throat dry

198
00:49:30,050 --> 00:49:32,579
-Water?
-Not yet

199
00:49:34,289 --> 00:49:35,590
Just ask

200
00:49:37,289 --> 00:49:40,480
l'm not doing this out of
personal spite

201
00:49:42,530 --> 00:49:43,860
Are you in pain?

202
00:49:46,530 --> 00:49:51,130
You must be careful. That ration
comes only once a week

203
00:49:53,690 --> 00:49:54,849
Ration?

204
00:49:55,210 --> 00:50:00,829
Only where there are men.
The village council pays for it

205
00:50:11,329 --> 00:50:16,699
Have other men been trapped like this?
l'm only curious

206
00:50:19,289 --> 00:50:22,130
We're very short of hands, you see

207
00:50:23,449 --> 00:50:24,849
What men?

208
00:50:26,050 --> 00:50:31,070
Well, about last autumn there was
the picture postcard man

209
00:50:33,289 --> 00:50:37,119
They say he was a salesman for
people who make them

210
00:50:38,369 --> 00:50:39,670
Any others?

211
00:50:40,449 --> 00:50:43,289
Since the beginning of the year...

212
00:50:44,929 --> 00:50:48,550
...a student on some sort of survey

213
00:50:50,849 --> 00:50:54,179
l hear he's with a neighbour

214
00:50:57,369 --> 00:50:59,969
l suppose he hasn't a ladder, either

215
00:51:03,530 --> 00:51:06,829
Our young people won't stay here

216
00:51:12,610 --> 00:51:15,519
They get more money in the cities

217
00:51:17,050 --> 00:51:21,329
After all, they've got cinemas
and restaurants there

218
00:51:22,690 --> 00:51:24,230
This is ridiculous

219
00:51:28,849 --> 00:51:30,599
What's the matter?

220
00:51:32,010 --> 00:51:35,909
Sorry, but will you please scratch
my right ear?

221
00:51:48,210 --> 00:51:49,440
Like this?

222
00:52:29,289 --> 00:52:32,730
l'm sorry, but will you give me
some water

223
00:52:37,289 --> 00:52:40,590
Some water. l beg you

224
00:52:58,210 --> 00:53:00,880
Not much left; when's the ration?

225
00:53:04,010 --> 00:53:06,500
That's enough

226
00:53:18,289 --> 00:53:20,460
lt's all your own fault

227
00:53:29,050 --> 00:53:30,559
The cords

228
00:53:31,769 --> 00:53:33,099
Want them off?

229
00:53:36,130 --> 00:53:42,210
l don't pity you, but l just can't
stand your misery

230
00:53:43,849 --> 00:53:49,719
But there's one condition. you won't
shovel the sand, do you promise?

231
00:53:51,530 --> 00:53:55,500
l promise. l'll promise anything

232
00:54:14,610 --> 00:54:19,599
lf l suffer, you'll suffer,
just remember that

233
00:54:29,050 --> 00:54:30,630
Where to?

234
00:56:53,610 --> 00:56:55,429
What can we do?

235
00:56:57,050 --> 00:57:02,280
We haven't moved the sand;
we've left it for two nights

236
00:57:03,289 --> 00:57:05,710
What can we do about water?

237
00:57:07,010 --> 00:57:11,789
Why don't you help a little?
l was kind enough to untie you

238
00:57:13,610 --> 00:57:18,389
Well, then, we must work

239
00:57:19,369 --> 00:57:20,670
Stop joking

240
00:57:21,130 --> 00:57:24,179
Who's got the right to force us
into a deal like that?

241
00:57:28,929 --> 00:57:31,880
There's such a thing as the
right man for the right job

242
00:57:35,849 --> 00:57:39,500
l'm a teacher and something of
a scholar

243
00:57:41,010 --> 00:57:44,380
Surely they can think of a
better way to put me to use

244
00:57:48,010 --> 00:57:50,500
For example, we could...

245
00:57:51,849 --> 00:57:55,010
...think of some way to exploit
the sand's appeal

246
00:57:57,010 --> 00:58:00,559
After all, that's what brought me
all the way here

247
00:58:03,010 --> 00:58:04,690
l'm serious

248
00:58:06,369 --> 00:58:11,039
That's probably why that postcard
salesman came, too

249
00:58:13,210 --> 00:58:17,070
You advertise it and develop it...

250
00:58:19,530 --> 00:58:22,230
...as a new tourist area

251
00:58:24,050 --> 00:58:29,599
But for a tourist area you need
a hot spring

252
00:58:31,769 --> 00:58:36,019
Well, that was just an idea, of course

253
00:58:40,210 --> 00:58:45,579
The sand might be good for
some kind of crop

254
00:58:46,610 --> 00:58:50,469
The Young People's Association
grows a few things

255
00:58:51,369 --> 00:58:54,280
Things like peanuts and tulips

256
00:58:55,530 --> 00:58:57,559
They said the tulip bulbs...

257
00:58:59,969 --> 00:59:04,179
...were so big they showed them at
an agricultural fair in Tokyo

258
00:59:05,210 --> 00:59:06,369
That's enough!

259
00:59:10,530 --> 00:59:14,219
A monkey could learn such work

260
00:59:15,690 --> 00:59:18,320
A monkey? Could it?

261
00:59:48,690 --> 00:59:50,369
lt's bad for you

262
01:00:05,530 --> 01:00:07,349
My breath's on fire

263
01:00:28,969 --> 01:00:30,199
lt's useless

264
01:00:33,050 --> 01:00:34,420
This sand

265
01:00:38,289 --> 01:00:41,800
lt could swallow up a city,
a country even

266
01:00:47,769 --> 01:00:49,309
Did you know that?

267
01:00:49,690 --> 01:00:54,679
A Roman town called Sabrata...
and a town in The Rubiyat...

268
01:00:56,849 --> 01:01:01,840
...were buried by tiny particles
one-eighth of a millimetre across

269
01:01:05,849 --> 01:01:09,539
You can't fight it. It's hopeless

270
01:01:18,369 --> 01:01:19,980
Don't!

271
01:01:25,050 --> 01:01:27,079
l'll make a ladder

272
01:03:11,769 --> 01:03:14,260
Shall l brush off the sand?

273
01:03:17,130 --> 01:03:22,960
But the women in Tokyo,
aren't they prettier?

274
01:03:25,050 --> 01:03:26,280
Ridiculous!

275
01:03:30,210 --> 01:03:31,369
l'll do it

276
01:08:52,909 --> 01:08:54,659
Goddam it!

277
01:08:58,149 --> 01:08:59,899
My blood will rot

278
01:10:22,069 --> 01:10:24,560
What a dirty trick

279
01:10:33,909 --> 01:10:39,460
What must l do? Please do something!
My blood will rot!

280
01:10:41,189 --> 01:10:45,189
Well, if we begin to work...

281
01:10:47,270 --> 01:10:52,930
All right, l'm beaten.
l'll let them win!

282
01:12:42,270 --> 01:12:47,079
Guest, there's water.
They're sending water

283
01:13:52,470 --> 01:13:56,050
Guest, it's easier if you hold
the shovel lower down

284
01:13:56,710 --> 01:13:59,520
Use your left hand; keep a balance

285
01:14:02,750 --> 01:14:04,329
l'm tired

286
01:14:14,989 --> 01:14:18,430
Rest, you're not used to this yet

287
01:14:19,149 --> 01:14:24,560
All my muscles are aching.
l'm exhausted

288
01:14:25,550 --> 01:14:28,180
You can't help it, with new work

289
01:14:28,750 --> 01:14:32,399
Will l get used to it?

290
01:14:33,470 --> 01:14:35,079
Oh, yes

291
01:14:37,229 --> 01:14:42,710
l don't understand. Don't you
find this pointless?

292
01:14:44,310 --> 01:14:48,210
Are you living to clear sand,
or clearing sand to live?

293
01:14:49,710 --> 01:14:53,399
Of course, this place isn't as
interesting as Tokyo...

294
01:14:54,069 --> 01:14:56,979
l'm not talking about Tokyo

295
01:15:02,630 --> 01:15:06,529
How can you stand being shut in
like this?

296
01:15:08,069 --> 01:15:10,489
But it's my home

297
01:15:14,710 --> 01:15:16,920
Then assert your rights

298
01:15:22,470 --> 01:15:25,840
l'm just going to wait for help

299
01:15:27,310 --> 01:15:28,850
Help?

300
01:15:30,390 --> 01:15:35,100
Of course. Someone will go to my room
before long

301
01:15:36,470 --> 01:15:40,930
They'll find a book with
the pages left open

302
01:15:41,710 --> 01:15:44,380
Money in my suit pockets

303
01:15:49,229 --> 01:15:51,720
They'll become suspicious

304
01:15:52,310 --> 01:15:54,939
But l suppose this is your life

305
01:15:56,550 --> 01:16:00,239
But, you see, the bones
are buried here, too

306
01:16:02,989 --> 01:16:06,500
My husband and daughter

307
01:16:07,909 --> 01:16:09,590
We'll find them

308
01:16:10,229 --> 01:16:13,810
Then you can leave. l'll help you

309
01:16:14,829 --> 01:16:18,619
You know how to make them
drop the ladder down

310
01:16:19,829 --> 01:16:23,090
What is there special for me
to do outside?

311
01:16:23,750 --> 01:16:25,470
You can walk

312
01:16:25,909 --> 01:16:27,350
Walk?

313
01:16:27,750 --> 01:16:31,399
That's right. It's wonderful
to walk where you like

314
01:16:33,710 --> 01:16:37,079
lsn't it tiring to walk
without doing anything?

315
01:16:38,390 --> 01:16:42,289
Shut up! Even a chained dog
goes mad

316
01:16:43,750 --> 01:16:45,399
Are you human?

317
01:16:55,750 --> 01:16:59,960
lf there were no sand
no one would bother about me

318
01:17:02,989 --> 01:17:06,359
That's true. For you, too

319
01:18:10,989 --> 01:18:14,289
-Look, here's one
-Stop it!

320
01:18:15,869 --> 01:18:18,750
This is what l came out here
Iooking for

321
01:18:20,390 --> 01:18:22,529
A kind of tiger-beetle

322
01:18:32,229 --> 01:18:34,510
-Got any scissors?
-Scissors?

323
01:18:35,630 --> 01:18:38,050
l just want to cut my whiskers

324
01:18:40,149 --> 01:18:42,430
l have some somewhere

325
01:18:58,670 --> 01:19:00,319
They're not very sharp

326
01:19:00,710 --> 01:19:04,960
-They're pretty rusty
-The sand does it

327
01:19:52,069 --> 01:19:55,579
They don't work.
They won't close properly

328
01:20:00,750 --> 01:20:03,909
We'll ask for a razor when
they bring the ration

329
01:20:06,229 --> 01:20:09,319
See? The sand in my beard
makes it all scratchy

330
01:20:12,390 --> 01:20:15,899
That sand just makes a mess
of everything

331
01:20:31,630 --> 01:20:35,840
-Go ahead!
-Thank you

332
01:20:37,909 --> 01:20:41,979
-What's your man doing?
-He's got stomach-ache

333
01:20:42,710 --> 01:20:44,920
Have you been at it too much?

334
01:22:14,069 --> 01:22:18,810
-A bath?
-No, l'm very tired today

335
01:22:22,630 --> 01:22:26,529
-l'd like one
-Not when you're ill

336
01:22:28,229 --> 01:22:34,029
l'm very sweaty. l just can't
get to sleep at all

337
01:22:36,310 --> 01:22:38,199
l'll boil water, then

338
01:22:38,630 --> 01:22:41,649
-l'll help
-No, l'll do it

339
01:23:16,069 --> 01:23:19,300
-What are you thinking about?
-Nothing much

340
01:23:22,989 --> 01:23:25,619
Let's take a photograph of you

341
01:23:26,149 --> 01:23:27,829
l've never...

342
01:23:31,149 --> 01:23:32,689
Don't fuss

343
01:23:47,909 --> 01:23:50,539
-You want to go home
-Why?

344
01:23:53,310 --> 01:23:55,869
l think you'd like a radio

345
01:23:59,829 --> 01:24:03,409
-You could hear about the world
-Nonsense!

346
01:24:05,149 --> 01:24:08,729
But you could hear about everything,
about Tokyo

347
01:24:09,989 --> 01:24:13,640
l only collected insects because of
useless things

348
01:24:15,750 --> 01:24:21,090
One's name in an encyclopaedia, that's
worth more than everyday stupidities

349
01:24:23,470 --> 01:24:28,460
But a radio would take your mind
off things

350
01:24:33,470 --> 01:24:37,579
l know, let's drink the sake

351
01:24:38,550 --> 01:24:40,689
But you're ill

352
01:24:41,149 --> 01:24:43,920
-To help sleep
-l'm sleepy enough without it

353
01:24:47,550 --> 01:24:49,479
Oh, come on

354
01:24:56,710 --> 01:24:58,920
Really, l can't

355
01:25:05,710 --> 01:25:08,060
Come on, it's all right

356
01:25:09,390 --> 01:25:12,090
-lt's boiling...
-First a drink

357
01:25:12,909 --> 01:25:14,659
You're a terrible man

358
01:25:18,470 --> 01:25:20,149
Right down

359
01:26:54,149 --> 01:26:56,109
That's better

360
01:26:59,630 --> 01:27:03,529
You are cruel, when l'm so tired

361
01:37:52,909 --> 01:37:58,109
Help! Someone help me!

362
01:38:16,909 --> 01:38:18,869
Grab this

363
01:38:24,750 --> 01:38:27,869
l'm sorry. Please pull me out

364
01:38:28,550 --> 01:38:31,989
No, it's not like pulling up a carrot

365
01:38:33,909 --> 01:38:37,420
We'll save you. Just hold that board

366
01:38:40,550 --> 01:38:42,760
lt's a good thing we came along

367
01:38:43,710 --> 01:38:48,699
This is ''Salty Bean Paste''.
Even dogs won't come near it

368
01:38:50,069 --> 01:38:53,260
l wonder how many have fallen in

369
01:38:56,189 --> 01:38:58,189
Sometimes strangers hike here

370
01:38:58,630 --> 01:39:02,319
We might dig up lots of
valuable things

371
01:39:03,750 --> 01:39:06,590
Two or three cameras, at least

372
01:40:15,310 --> 01:40:18,680
-Be quiet
-l'm sorry

373
01:40:27,710 --> 01:40:29,529
Shall l make some tea?

374
01:40:30,989 --> 01:40:32,289
What's the time?

375
01:40:32,909 --> 01:40:36,699
l don't know... about 8.30...

376
01:40:43,390 --> 01:40:44,789
l failed

377
01:40:45,390 --> 01:40:46,649
Yes

378
01:40:51,550 --> 01:40:54,220
Complete and utter failure

379
01:40:57,550 --> 01:40:59,090
Have you...

380
01:41:01,310 --> 01:41:02,920
...a wife?

381
01:41:05,550 --> 01:41:06,949
ln Tokyo

382
01:41:08,390 --> 01:41:10,069
That's none of your business

383
01:41:16,310 --> 01:41:18,520
But it's too bad

384
01:41:21,069 --> 01:41:25,949
lf l'd succeeded, l was going to buy
a radio and send it to you

385
01:41:27,710 --> 01:41:30,590
-A radio?
-You wanted one...

386
01:41:34,390 --> 01:41:38,039
No. You don't have to do that

387
01:41:39,630 --> 01:41:42,720
l can do piecework and buy one here

388
01:41:45,390 --> 01:41:47,279
l failed

389
01:41:49,390 --> 01:41:51,600
Shall l dry you?

390
01:41:55,149 --> 01:41:57,569
l should have known the geography

391
01:42:08,829 --> 01:42:11,079
But they'll be looking for me

392
01:42:12,710 --> 01:42:16,819
There will be my friends,
the co-operative...

393
01:42:19,390 --> 01:42:22,409
...the Board of Education
and the PTA

394
01:42:25,029 --> 01:42:26,750
They won't leave me to my fate

395
01:42:29,149 --> 01:42:31,399
l'll wash you

396
01:42:51,989 --> 01:42:52,899
What's that?

397
01:42:57,909 --> 01:43:01,840
Hope. A crow trap

398
01:43:08,310 --> 01:43:10,520
l put bait here...

399
01:43:11,470 --> 01:43:15,369
...and then the crow gets sucked down
with the sand

400
01:43:16,390 --> 01:43:19,090
But crows are crafty

401
01:43:20,069 --> 01:43:22,100
Not when they're hungry

402
01:43:28,390 --> 01:43:31,340
But why is it hope?

403
01:43:32,750 --> 01:43:34,189
Hope because...

404
01:43:35,750 --> 01:43:39,819
l'll tie a message to the crow's leg,
asking for help

405
01:43:42,829 --> 01:43:47,039
lt hasn't come yet. It's almost
three months now

406
01:43:49,550 --> 01:43:52,640
-What hasn't?
-Help

407
01:44:02,310 --> 01:44:05,289
Maybe they thought you ran away

408
01:44:06,550 --> 01:44:07,949
Ran away?

409
01:44:10,829 --> 01:44:13,569
That you hated your job

410
01:44:15,229 --> 01:44:16,560
lmpossible

411
01:44:19,710 --> 01:44:25,510
l left my bank book. l left
a book open on my desk

412
01:44:26,750 --> 01:44:30,329
My whole room is shouting for help

413
01:44:31,789 --> 01:44:33,260
What's there?

414
01:44:34,470 --> 01:44:37,310
-Where?
-ln Tokyo

415
01:44:38,630 --> 01:44:40,770
Why not go and see?

416
01:44:42,470 --> 01:44:43,939
lt's too much

417
01:44:54,829 --> 01:44:56,649
Here's the ration

418
01:45:01,550 --> 01:45:04,319
Wait a moment

419
01:45:09,909 --> 01:45:16,199
Will you let me out a little,
just half an hour a day?

420
01:45:20,470 --> 01:45:23,350
l want to see the sea.
l won't run away

421
01:45:24,310 --> 01:45:26,270
Twenty minutes... ten minutes...

422
01:45:26,750 --> 01:45:31,250
l've been good. It's three months
now... l'll go mad

423
01:45:33,550 --> 01:45:36,710
l'll discuss it with the village
council

424
01:45:37,550 --> 01:45:40,359
l don't mind if there's a guard

425
01:47:31,310 --> 01:47:32,850
You're diligent

426
01:47:33,550 --> 01:47:36,359
l'll soon earn 1,000 yen

427
01:48:07,470 --> 01:48:08,659
That's enough

428
01:48:17,470 --> 01:48:20,560
l don't want to die in a ditch

429
01:48:51,710 --> 01:48:54,869
What's a radio worth in this place?

430
01:48:56,630 --> 01:49:00,529
They're not your friends.
It's everyone for himself

431
01:49:05,710 --> 01:49:08,060
You're being exploited

432
01:49:09,229 --> 01:49:12,810
You wag your tail, but just you wait

433
01:49:14,069 --> 01:49:17,119
We'll be deserted, left to ourselves

434
01:49:17,710 --> 01:49:20,380
No, because of the sand

435
01:49:20,909 --> 01:49:26,000
Sand? What use is it?
Nothing but trouble

436
01:49:27,229 --> 01:49:30,529
We sell it. To builders

437
01:49:31,470 --> 01:49:34,489
lt's against the law to use
such salt sand

438
01:49:35,390 --> 01:49:38,340
lt's a secret. We sell it half price

439
01:49:38,909 --> 01:49:42,420
But the buildings would soon collapse

440
01:49:45,069 --> 01:49:47,029
That's not my business

441
01:49:49,989 --> 01:49:53,989
Well, that's not your business.
Yes, that's true

442
01:49:55,750 --> 01:50:01,229
But somebody must profit by it.
Why help them?

443
01:50:02,550 --> 01:50:04,479
lt's a co-operative

444
01:50:05,710 --> 01:50:09,079
People like me are treated very well

445
01:50:22,829 --> 01:50:24,720
l'll sieve them

446
01:50:28,470 --> 01:50:34,199
lt's like building a house on water,
ignoring the existence of boats

447
01:50:35,710 --> 01:50:39,819
You can't get the idea of a house
out of your head

448
01:50:41,550 --> 01:50:45,060
Haven't you been wanting to go home,
too?

449
01:51:09,470 --> 01:51:11,149
Even if it's false...

450
01:51:12,750 --> 01:51:15,140
...as long as there's hope...

451
01:51:16,229 --> 01:51:19,600
...something different may happen
tomorrow

452
01:51:23,069 --> 01:51:24,609
l'm always afraid

453
01:51:27,750 --> 01:51:30,380
ln the morning, when l go to sleep...

454
01:51:30,989 --> 01:51:35,520
...l'm afraid of being alone again
when l wake up

455
01:51:37,710 --> 01:51:41,079
l'm afraid, really afraid...

456
01:52:07,310 --> 01:52:09,729
Here, put them in this

457
01:52:42,630 --> 01:52:45,510
What now? The basket already?

458
01:52:46,630 --> 01:52:49,510
l hear you want to see the sea

459
01:52:51,630 --> 01:52:55,069
Will you let me?

460
01:52:57,630 --> 01:53:00,539
lt's not out of the question

461
01:53:01,229 --> 01:53:06,779
l beg you. Just half an hour a day,
l won't escape

462
01:53:08,909 --> 01:53:13,789
Then let us see you doing ''it''.
The two of you

463
01:53:15,470 --> 01:53:19,399
Outdoors, where we can all watch you

464
01:53:21,149 --> 01:53:22,310
''lt''?

465
01:53:27,149 --> 01:53:29,989
What men and women do together. ''it''

466
01:54:01,310 --> 01:54:06,789
We all discussed your request.
We all agreed what you must do

467
01:54:47,149 --> 01:54:49,039
What now?

468
01:54:50,229 --> 01:54:52,510
lt's ridiculous

469
01:54:53,710 --> 01:54:56,340
But it's a chance, at least

470
01:54:57,909 --> 01:55:00,329
lgnore them

471
01:55:03,229 --> 01:55:07,340
We could, that's one way, but
we needn't take it seriously

472
01:55:08,310 --> 01:55:10,800
What? Are you a sex maniac?

473
01:57:14,750 --> 01:57:17,170
You live like a sow, anyway

474
01:58:28,109 --> 01:58:31,020
Pretending will do

475
02:01:06,189 --> 02:01:07,659
But how?

476
02:01:13,430 --> 02:01:14,760
Funny...

477
02:01:24,189 --> 02:01:28,159
There hasn't been a drop of rain
for three weeks

478
02:02:02,189 --> 02:02:04,220
Capillary action

479
02:02:05,869 --> 02:02:07,310
That's it

480
02:02:20,630 --> 02:02:23,819
Maybe l can develop a water supply

481
02:04:11,189 --> 02:04:15,720
Surface evaporation probably
draws up water

482
02:04:17,510 --> 02:04:19,859
The sand is a pump

483
02:04:21,510 --> 02:04:24,390
l'm living on a suction pump

484
02:04:27,869 --> 02:04:31,449
Perhaps I can make a really
ecient reservoir

485
02:05:28,029 --> 02:05:30,869
lt's awful. l'm choking

486
02:05:31,590 --> 02:05:33,659
lt's nearly December

487
02:06:29,869 --> 02:06:31,479
Still trying?

488
02:06:33,029 --> 02:06:36,609
l'll catch a crow easily
when food is scarce

489
02:06:38,350 --> 02:06:40,979
Really, men are...

490
02:06:43,949 --> 02:06:47,319
l want to be the trapper for once

491
02:06:53,510 --> 02:06:57,760
We can hear the weather forecast
when we get the radio

492
02:07:22,430 --> 02:07:23,760
l'm afraid

493
02:07:45,189 --> 02:07:47,289
l'll go and bury this

494
02:08:30,430 --> 02:08:32,180
Until tomorrow

495
02:08:50,750 --> 02:08:53,170
ls anyone there?

496
02:09:09,430 --> 02:09:12,310
l don't want to

497
02:09:19,909 --> 02:09:21,170
-What's wrong?
-A pain

498
02:09:42,510 --> 02:09:43,909
A child?

499
02:09:50,430 --> 02:09:53,520
-Since when?
-Since October

500
02:09:58,189 --> 02:10:00,680
lt hurts? Do you feel sick?

501
02:10:05,350 --> 02:10:09,319
That's it, it's probably a miscarriage

502
02:10:10,189 --> 02:10:11,489
You know?

503
02:10:12,350 --> 02:10:15,720
He used to shoe horses for a vet

504
02:10:16,710 --> 02:10:18,390
Get a doctor

505
02:10:20,270 --> 02:10:22,159
He'll be too late

506
02:10:22,949 --> 02:10:24,699
Doctors today...

507
02:10:25,109 --> 02:10:27,739
Go and get the van

508
02:11:05,029 --> 02:11:08,680
l'm afraid. Has the wind dropped?

509
02:11:10,949 --> 02:11:13,439
Almost died down

510
02:11:22,789 --> 02:11:25,630
-What now?
-Wrap her up

511
02:11:45,789 --> 02:11:47,090
A radio

512
02:11:53,869 --> 02:11:55,380
A radio

513
02:11:58,189 --> 02:11:59,729
Only now...

514
02:12:00,710 --> 02:12:02,390
Want to hear it?

515
02:12:13,710 --> 02:12:15,220
Want it?

516
02:12:15,590 --> 02:12:17,659
Tuck it in the bedding

517
02:12:25,189 --> 02:12:26,869
Have you got another board?

518
02:12:28,630 --> 02:12:31,090
Take one from the bench outside

519
02:12:45,510 --> 02:12:47,510
The one underneath

520
02:13:25,350 --> 02:13:27,170
What did you say?

521
02:13:38,229 --> 02:13:41,560
Nothing... Iater

522
02:14:00,350 --> 02:14:01,960
l don't want to...

523
02:19:27,110 --> 02:19:30,200
l needn't run off yet

524
02:19:32,510 --> 02:19:37,110
My return ticket's open. I can
use it whenever i like

525
02:19:42,430 --> 02:19:47,489
Anyway, there's my reservoir
experiment, it's very exciting

526
02:19:50,110 --> 02:19:53,479
The villagers must be the first
to know of it

527
02:19:55,790 --> 02:19:58,420
l'll tell them today or tomorrow

528
02:19:59,350 --> 02:20:01,700
As for escaping...

529
02:20:04,790 --> 02:20:08,299
...l'll think about that afterwards

530
02:20:16,950 --> 02:20:19,090
Judgement : Court of Family Affairs

531
02:20:19,629 --> 02:20:24,020
Concerning the disappearance of
Junpei Niki, born 7th March, 1926

532
02:20:24,790 --> 02:20:31,500
The Court acknowledges his absence
for seven years, since August, 1956

533
02:20:32,629 --> 02:20:38,180
The verdict of the Court
is as follows.

534
02:20:39,190 --> 02:20:42,559
Junpei Niki is adjudged to be
a missing person

535
02:20:43,190 --> 02:20:47,649
Certined 5th October, 1963.
Judge of Domestic Relations

