﻿1
00:00:02,108 --> 00:00:05,028
<i>Blastoff!</i>

2
00:00:05,308 --> 00:00:05,789
He's smart.

3
00:00:06,028 --> 00:00:08,429
<i>Something has entered</i>
<i>the earth's atmosphere.</i>

4
00:00:08,669 --> 00:00:09,429
He's courageous.

5
00:00:09,669 --> 00:00:12,069
<i>We've gotta show these aliens</i>
<i>what we're made of.</i>

6
00:00:12,309 --> 00:00:13,230
He's inventive.

7
00:00:13,470 --> 00:00:15,830
<i>Bubble travel is the way of the future!</i>

8
00:00:16,070 --> 00:00:19,750
And he's got a really big head.

9
00:00:20,150 --> 00:00:21,510
<i>I can fix that.</i>

10
00:00:21,750 --> 00:00:24,110
He's Jimmy neutron,
star of his very own adventure,

11
00:00:24,350 --> 00:00:26,351
from Paramount pictures
and nickelodeon movies.

12
00:00:26,591 --> 00:00:27,751
<i>Gotta blast!</i>

13
00:00:27,991 --> 00:00:29,471
Jimmy neutron is a boy genius.

14
00:00:29,711 --> 00:00:32,031
<i>We're ready for intergalactic travel!</i>

15
00:00:32,272 --> 00:00:34,352
He's one-third Einstein,

16
00:00:34,592 --> 00:00:37,192
one-third Bart Simpson,

17
00:00:37,432 --> 00:00:41,112
and then one-third Jim carrey.

18
00:00:42,433 --> 00:00:43,353
<i>You rock.</i>

19
00:00:43,593 --> 00:00:46,834
I can't really tell you
the whole story of "Jimmy neutron".

20
00:00:47,074 --> 00:00:52,074
Basically, aliens come to earth
and abduct all the parents.

21
00:00:52,875 --> 00:00:57,355
You must erase all impressions
of any kind of alien that you've seen.

22
00:00:57,595 --> 00:00:59,755
<i>You let us go, you big ball of phlegm!</i>

23
00:00:59,995 --> 00:01:02,355
Even "star trek" never
produced aliens like this.

24
00:01:02,596 --> 00:01:05,996
<i>That's no way to talk to the king.</i>
<i>He is the royal phlegm!</i>

25
00:01:06,236 --> 00:01:08,916
Parents are abducted
by the evil yokians,

26
00:01:09,156 --> 00:01:12,836
and Jimmy and all the kids need to
work together to get their parents back.

27
00:01:13,076 --> 00:01:16,197
<i>That gives us about two days</i>
<i>to build our interstellar warships.</i>

28
00:01:16,437 --> 00:01:18,437
<i>We'll also need to bring snacks.</i>

29
00:01:18,677 --> 00:01:21,798
And now, blast behind
the scenes on "Jimmy neutron".

30
00:01:22,038 --> 00:01:22,918
There you go again!

31
00:01:23,158 --> 00:01:23,878
Meet the stars.

32
00:01:24,118 --> 00:01:26,318
Throw these minuscule vermin
into the dungeon!

33
00:01:26,558 --> 00:01:27,518
Explore the gadgets.

34
00:01:27,758 --> 00:01:29,158
<i>This is my hypnobeam.</i>

35
00:01:29,398 --> 00:01:32,118
Experience the cutting-edge animation.

36
00:01:32,359 --> 00:01:33,479
<i>Here we go! Here we go!</i>

37
00:01:33,719 --> 00:01:37,079
It's all part of bringing
Jimmy's world to the big screen.

38
00:01:37,320 --> 00:01:41,480
<i>Fasten your seat belt.</i>
<i>It's gonna be a bumpy ride!</i>

39
00:01:49,441 --> 00:01:50,641
<i>Gotta blast!</i>

40
00:01:50,881 --> 00:01:54,842
My goal was to try to hit as many
kid-fantasy buttons as possible.

41
00:01:55,082 --> 00:01:56,442
<i>DNA match confirmed.</i>

42
00:01:56,681 --> 00:01:58,962
In the movie,
Jimmy's got a lot of neat stuff.

43
00:01:59,202 --> 00:02:03,323
He's got an underground lab,
this huge complex under the house,

44
00:02:03,562 --> 00:02:07,123
trap doors everywhere - cos trap doors
are cool, secret passages are cool.

45
00:02:07,364 --> 00:02:11,604
And in his lab he concocts all these
amazing experiments and inventions.

46
00:02:11,844 --> 00:02:14,284
<i>The latest burping-soda formula.</i>

47
00:02:14,524 --> 00:02:18,124
<i>A guaranteed one burp per sip.</i>

48
00:02:18,605 --> 00:02:23,925
He's constantly launching rockets
off the roof, setting the drapes on fire...

49
00:02:24,165 --> 00:02:25,965
He builds all this stuff and takes off,

50
00:02:26,205 --> 00:02:29,686
and when they think he's asleep
in his room he's out rocketing around.

51
00:02:29,926 --> 00:02:32,886
Basically, a little hellion.

52
00:02:33,126 --> 00:02:37,286
His coolest invention is Goddard,
his robotic dog that he built.

53
00:02:37,527 --> 00:02:40,007
And Goddard is like Jimmy's batbelt:

54
00:02:40,247 --> 00:02:43,327
He can turn into anything at any time.

55
00:02:43,568 --> 00:02:45,328
<i>Thanks, boy. Oh, good dog!</i>

56
00:02:45,568 --> 00:02:47,288
Jimmy's constantly tooling with him

57
00:02:47,528 --> 00:02:50,408
and trying to come up with new things
that Goddard can do.

58
00:02:50,648 --> 00:02:54,329
He can transform into a radio
and a disco ball

59
00:02:54,568 --> 00:02:57,769
and a fire extinguisher at any moment.

60
00:02:58,009 --> 00:03:00,209
It's pretty amazing.

61
00:03:00,450 --> 00:03:04,130
Jimmy neutron is a bit of
an outcast - he's so bright

62
00:03:04,370 --> 00:03:07,530
that a lot of the other kids don't
understand where he's coming from.

63
00:03:07,770 --> 00:03:10,411
<i>Behold. The shrink ray!</i>

64
00:03:10,651 --> 00:03:12,091
<i>Help me. Help me.</i>

65
00:03:12,331 --> 00:03:15,051
<i>I'm so tiny - just like Jimmy's brain!</i>

66
00:03:15,291 --> 00:03:19,772
<i>Some of the greatest inventors started</i>
<i>as complete, hopeless failures too.</i>

67
00:03:20,012 --> 00:03:21,972
Carl is Jimmy's best friend,

68
00:03:22,212 --> 00:03:27,733
and if he has an experiment to try out,
Carl is his Guinea pig.

69
00:03:27,973 --> 00:03:31,574
<i>I don't know, Jimmy.</i>
<i>You know, I get carsick.</i>

70
00:03:31,813 --> 00:03:34,294
<i>Well, luckily, this is a rocket.</i>

71
00:03:34,533 --> 00:03:37,454
Carl wheezer is named Carl wheezer
for one obvious reason,

72
00:03:37,694 --> 00:03:40,134
and that is, in fact,
because Carl wheezes.

73
00:03:40,375 --> 00:03:43,974
He's not nearly as handsome and
young-hero-like as Jimmy neutron is,

74
00:03:44,215 --> 00:03:46,295
but it's Jimmy's show, so he's the hero.

75
00:03:46,535 --> 00:03:49,775
<i>We need to ask ourselves</i>
<i>"what would ultra lord do?"</i>

76
00:03:50,015 --> 00:03:54,176
Sheen's just quirky and a little different,
and he's really into ultra lord.

77
00:03:54,416 --> 00:03:59,377
Sheen loves ultra lord
to the point of it being a problem.

78
00:03:59,616 --> 00:04:00,696
<i>In episode 224,</i>

79
00:04:00,937 --> 00:04:04,897
<i>he fried the zeebot's brain with his</i>
<i>heat-seeking infra-thought. It was cool!</i>

80
00:04:05,137 --> 00:04:08,178
He's very nervous, but once
he gets that ultra lord thing going,

81
00:04:08,418 --> 00:04:11,938
he has no fear, no nothing - he gets
an ultra lord mask, it's all over.

82
00:04:12,178 --> 00:04:13,419
<i>Neutron!</i>

83
00:04:13,658 --> 00:04:16,338
Cindy vortex is
the know-it-all from school.

84
00:04:16,579 --> 00:04:18,019
<i>Nice invention, nerdtron!</i>

85
00:04:18,259 --> 00:04:20,459
Cindy and Jimmy
actually like each other a lot,

86
00:04:20,700 --> 00:04:25,820
but they're at a time and they're at
an age where they're very competitive.

87
00:04:26,059 --> 00:04:28,500
They like sparring
with each other mentally.

88
00:04:28,740 --> 00:04:30,821
It's kind of a love-hate friendship.

89
00:04:31,060 --> 00:04:32,981
<i>I didn't think we liked girls yet, Jim.</i>

90
00:04:33,220 --> 00:04:35,261
<i>Oh, we don't! We don't, no.</i>

91
00:04:35,501 --> 00:04:37,022
<i>Is this one of those macho things?</i>

92
00:04:37,261 --> 00:04:39,541
Cindy is all-out crazed,

93
00:04:39,781 --> 00:04:42,582
and Libby is the best friend of Cindy.

94
00:04:42,822 --> 00:04:46,582
Libby tries to tone her down, and is
pretty much the conscience of her.

95
00:04:46,822 --> 00:04:49,903
<i>What's wrong with you, Cindy?</i>

96
00:04:50,023 --> 00:04:54,783
The cool thing about the movie is
all of the characters are entirely unique.

97
00:04:55,023 --> 00:04:56,224
<i>Retroland!</i>

98
00:04:56,464 --> 00:04:59,504
It's really like a fun family.

99
00:05:00,744 --> 00:05:02,384
<i>I can hang out with ultra lord!</i>

100
00:05:02,624 --> 00:05:04,744
<i>And there's a petting zoo.</i>

101
00:05:04,985 --> 00:05:06,185
<i>Well, look at this.</i>

102
00:05:06,424 --> 00:05:07,945
<i>Let's go!</i>

103
00:05:08,185 --> 00:05:12,745
The look of Jimmy neutron
is done with 3-d computer graphics,

104
00:05:12,985 --> 00:05:16,346
so it's not like a 2-d show,
it's not hand-painted.

105
00:05:16,586 --> 00:05:20,266
All the characters
are constructed on the computer.

106
00:05:20,507 --> 00:05:23,187
It's like having a live-action set
in the computer.

107
00:05:23,426 --> 00:05:26,187
The first thing they'll wanna see
is the animatic.

108
00:05:26,427 --> 00:05:29,747
When we're designing the characters,
we'll design them on paper,

109
00:05:29,987 --> 00:05:34,588
and sculpt maquettes
out of some of the main characters,

110
00:05:34,828 --> 00:05:37,469
and then the modellers can take those

111
00:05:37,708 --> 00:05:40,749
and build the same thing
in the computer.

112
00:05:40,989 --> 00:05:44,989
We can make modifications right there
in the computer, and it works great.

113
00:05:45,229 --> 00:05:48,830
<i>Engaging pulse rockets now!</i>

114
00:05:49,069 --> 00:05:53,310
We love the "squash and stretch" and
action of the looney tunes heritage,

115
00:05:53,551 --> 00:05:57,951
and to take looney tunes into cgi
and make a cartoon-animated world

116
00:05:58,191 --> 00:06:01,431
that takes advantage
of what computer graphics can do,

117
00:06:01,672 --> 00:06:04,272
you've got something
very different and very special.

118
00:06:04,512 --> 00:06:07,112
It's original, it seems to me,
the art for this.

119
00:06:07,352 --> 00:06:09,712
It's extraordinary, animation today.

120
00:06:09,952 --> 00:06:14,993
Whereas once upon a time we were
used to a classic style of animation,

121
00:06:15,233 --> 00:06:19,314
now you never know
what you're going to see.

122
00:06:26,514 --> 00:06:30,635
<i>How many times have we told you</i>
<i>not to launch yourself off the roof?</i>

123
00:06:30,875 --> 00:06:32,635
<i>Probably nine.</i>

124
00:06:32,875 --> 00:06:36,356
<i>Exactly nine. They say repetition</i>
<i>is good for a developing brain.</i>

125
00:06:36,596 --> 00:06:40,756
Jimmy's parents are fantastic because
they accept the fact that he is a genius.

126
00:06:40,996 --> 00:06:45,237
<i>I received a message from outer space</i>
<i>but it was garbled in the ionosphere...</i>

127
00:06:45,476 --> 00:06:47,677
<i>Message from space? Wow!</i>

128
00:06:47,917 --> 00:06:51,357
They're great characters in themselves,
cos his mom can fix the car,

129
00:06:51,598 --> 00:06:55,158
and she's obviously
the engineering part of the family.

130
00:06:55,398 --> 00:06:58,758
The father has sort of
a wide-eyed innocence himself.

131
00:06:58,999 --> 00:07:01,398
He loves the toys in the cereal box.

132
00:07:01,638 --> 00:07:03,159
<i>This is a good one!</i>

133
00:07:03,399 --> 00:07:06,199
And he totally loves Jimmy's gadgets.

134
00:07:06,439 --> 00:07:09,599
Anytime he sees him he's, like,
"boy! Wish I had that toy!"

135
00:07:09,840 --> 00:07:12,800
<i>Is that a brainteaser puzzle?</i>
<i>I'm so good at these!</i>

136
00:07:13,040 --> 00:07:16,720
<i>I was President of the brainteaser club</i>
<i>in high school. Watch this.</i>

137
00:07:16,961 --> 00:07:20,560
Jimmy's got a great relation
with his parents, and respects them,

138
00:07:20,801 --> 00:07:23,921
and tries to do what they say,
but sometimes it's just hard.

139
00:07:24,161 --> 00:07:25,401
<i>You'll spoil your dinner.</i>

140
00:07:25,641 --> 00:07:29,162
<i>We have told you time and time again</i>
<i>about playing with rockets.</i>

141
00:07:29,402 --> 00:07:31,923
<i>Your father will have a few words</i>
<i>to say when he gets home.</i>

142
00:07:32,162 --> 00:07:33,523
<i>Oh, and by the way.</i>

143
00:07:33,762 --> 00:07:36,923
<i>Mom says you're grounded. Sorry!</i>

144
00:07:40,603 --> 00:07:45,684
The kids are down on their parents.
They feel they're being treated unfairly.

145
00:07:45,924 --> 00:07:49,485
They wish they'd go away. "Wouldn't
it be great if our parents disappeared?"

146
00:07:49,725 --> 00:07:52,365
<i>We could do whatever we wanted,</i>
<i>whenever we wanted.</i>

147
00:07:52,605 --> 00:07:55,045
<i>We could have fun all the time!</i>

148
00:07:55,285 --> 00:07:57,885
<i>Jimmy?</i>

149
00:07:58,806 --> 00:08:00,686
Basically, aliens come to earth

150
00:08:00,926 --> 00:08:03,726
and abduct all the parents,
leaving the kids in charge.

151
00:08:03,966 --> 00:08:06,727
<i>No parents!</i>

152
00:08:06,966 --> 00:08:10,807
So it's just kids running amok.

153
00:08:15,248 --> 00:08:18,248
It's like every kid's fantasy.
The whole town is taken over,

154
00:08:18,488 --> 00:08:23,168
and the kids do everything that they
can imagine doing without the parents.

155
00:08:23,408 --> 00:08:26,408
After a couple of days,
the kids start to miss the parents

156
00:08:26,649 --> 00:08:28,209
and realise "oh, my God..."

157
00:08:28,450 --> 00:08:32,049
<i>The earth's been visited by aliens!</i>

158
00:08:32,290 --> 00:08:36,210
The parents have been abducted
by aliens that are eggs: The yokians.

159
00:08:36,450 --> 00:08:41,891
So it's almost like having
your breakfast kidnap you.

160
00:08:42,131 --> 00:08:46,171
Don't look so surprised.
We are an advanced alien race.

161
00:08:46,412 --> 00:08:49,331
What did you expect?

162
00:08:49,572 --> 00:08:52,732
These characters are
eggs outside their shell,

163
00:08:52,972 --> 00:08:56,732
and if they're not contained in
some way, they run all over the floor.

164
00:08:56,973 --> 00:09:00,653
<i>They must be an advanced species,</i>
<i>millions of years ahead of us.</i>

165
00:09:00,893 --> 00:09:04,973
<i>When I sneeze, it looks like</i>
<i>an advanced species too.</i>

166
00:09:05,214 --> 00:09:07,534
They fly around in giant chicken ships,

167
00:09:07,774 --> 00:09:09,694
and they worship this chicken God,

168
00:09:09,934 --> 00:09:12,615
and they're bent on stealing
the parents of retroville

169
00:09:12,855 --> 00:09:15,254
to sacrifice them to their God poultra.

170
00:09:15,495 --> 00:09:17,575
<i>Jimmy didn't mean to ruin our lives</i>

171
00:09:17,815 --> 00:09:20,936
<i>and get our parents eaten</i>
<i>by a giant space monster!</i>

172
00:09:21,175 --> 00:09:24,456
<i>Nobody eats my parents</i>
<i>unless I say so!</i>

173
00:09:24,696 --> 00:09:28,097
<i>Come on!</i>

174
00:09:29,816 --> 00:09:32,457
<i>Roll tape.</i>

175
00:09:33,777 --> 00:09:38,258
Hello, and welcome to our special
edition of "poultra: God of wrath".

176
00:09:38,498 --> 00:09:42,058
If you're watching this, chances
are your friends and/or relatives

177
00:09:42,298 --> 00:09:45,178
are about to be sacrificed
to the mighty poultra -

178
00:09:45,418 --> 00:09:47,739
which is a great honour indeed.

179
00:09:47,979 --> 00:09:51,419
And very painful.

180
00:09:51,539 --> 00:09:54,219
Mary and I play yokian newscasters,

181
00:09:54,460 --> 00:09:56,659
not unlike ourselves here on earth.

182
00:09:56,900 --> 00:09:59,540
There's a lot of prestige
to being a yokian anchor.

183
00:09:59,780 --> 00:10:03,741
They're from outer space,
reporting on the human sacrifices

184
00:10:03,980 --> 00:10:05,621
that are about to take place.

185
00:10:05,860 --> 00:10:08,021
<i>I, king goobot the fifth,</i>

186
00:10:08,261 --> 00:10:11,142
<i>give you sacrifice!</i>

187
00:10:11,381 --> 00:10:14,022
I play king goobot,

188
00:10:14,262 --> 00:10:17,102
and the king is an angry sort, and...

189
00:10:17,342 --> 00:10:19,862
No, no, Martin. I'm playing king goobot.

190
00:10:20,103 --> 00:10:22,983
You are? Then I would
have done it differently.

191
00:10:23,223 --> 00:10:24,703
Do it again. Take two.

192
00:10:24,943 --> 00:10:27,344
- I play king goobot.
- Yes.

193
00:10:27,583 --> 00:10:30,704
King goobot is
a very powerful alien leader.

194
00:10:30,944 --> 00:10:33,064
<i>When did it become acceptable</i>

195
00:10:33,304 --> 00:10:36,985
<i>to approach my royal throne</i>
<i>unannounced?</i>

196
00:10:37,225 --> 00:10:39,265
And this is my prime minister.

197
00:10:39,505 --> 00:10:41,106
<i>Oh, please, oh, please...</i>

198
00:10:41,345 --> 00:10:43,426
<i>I would love for that to stop.</i>

199
00:10:43,666 --> 00:10:44,466
<i>... Oh, please.</i>

200
00:10:44,705 --> 00:10:46,306
His name is ooblar.

201
00:10:46,546 --> 00:10:48,226
Ooblar - me.

202
00:10:48,466 --> 00:10:50,506
He's kind of the servant to the king,

203
00:10:50,746 --> 00:10:53,027
and the king is, shall we say, moody,

204
00:10:53,267 --> 00:10:57,947
and can freak out and kill people
arbitrarily - so he's frightened.

205
00:10:58,187 --> 00:11:00,867
Space him!

206
00:11:01,508 --> 00:11:04,228
So Jimmy has gotta
get his parents back,

207
00:11:04,468 --> 00:11:07,068
or they're gonna get eaten
by a gigantic chicken.

208
00:11:07,308 --> 00:11:09,148
And you just don't want that.

209
00:11:09,388 --> 00:11:11,869
<i>Ok, so me, you and a dog</i>

210
00:11:12,108 --> 00:11:15,029
<i>are gonna battle an alien civilisation.</i>

211
00:11:15,269 --> 00:11:17,710
<i>But he's a good dog, aren't you, boy?</i>

212
00:11:17,950 --> 00:11:20,030
The idea of building a rocket
in the back yard

213
00:11:20,270 --> 00:11:23,751
where you can jump in with your
buddies and your dog and some tang,

214
00:11:23,990 --> 00:11:27,831
like real astronauts, and go into outer
space, will appeal to the audience.

215
00:11:28,071 --> 00:11:32,031
<i>Sensors lead to the orion star system</i>
<i>three million light years away,</i>

216
00:11:32,272 --> 00:11:35,632
<i>so we'll need to leave by Friday.</i>
<i>Any questions?</i>

217
00:11:35,871 --> 00:11:38,632
Jimmy organises this
great armada to go into space

218
00:11:38,872 --> 00:11:43,713
by retrofitting all of the rides
at the amusement park in town.

219
00:11:43,952 --> 00:11:46,073
So he takes the Ferris wheel

220
00:11:46,313 --> 00:11:49,473
and the roller coasters
and everything in the amusement park,

221
00:11:49,713 --> 00:11:52,554
turns them into these spaceships
powered by neutronium.

222
00:11:52,794 --> 00:11:58,195
<i>You heard the man! Stop sucking</i>
<i>your thumbs and let's light this candle!</i>

223
00:12:10,116 --> 00:12:13,436
My theory is that the more fun
the audio session is,

224
00:12:13,677 --> 00:12:16,237
and the more the characters laugh,
the better the show.

225
00:12:16,476 --> 00:12:18,477
- You stay here, I'll be back.
- All right.

226
00:12:18,717 --> 00:12:20,477
Stay! I go. You stay.

227
00:12:20,717 --> 00:12:22,117
- I'm staying.
- Ok.

228
00:12:22,357 --> 00:12:29,238
It's really trying to find people that also
have the ability to work off each other.

229
00:12:31,438 --> 00:12:32,759
That was great. I felt...

230
00:12:32,999 --> 00:12:36,799
No, seriously. I felt like you were
really being abducted by aliens.

231
00:12:37,039 --> 00:12:40,400
Voicing an animated character
is such a liberating experience.

232
00:12:40,639 --> 00:12:44,160
There are no boundaries, because
the animators can do anything.

233
00:12:44,400 --> 00:12:47,800
You have that opportunity
to be that broad.

234
00:12:48,040 --> 00:12:53,161
And childish, in a way -
it's like being back in playschool.

235
00:12:53,401 --> 00:12:56,482
I am awarding permission
to commence with the commencing!

236
00:12:56,722 --> 00:12:59,562
The commencing shall commence!
All hail the commencee,

237
00:12:59,801 --> 00:13:02,722
for he alone doth do
the commencing, which I think is fun.

238
00:13:02,962 --> 00:13:03,603
Ooblar.

239
00:13:03,842 --> 00:13:06,843
It is a very creative process,
doing animation.

240
00:13:07,082 --> 00:13:09,723
You can come up with
a new voice, a new character,

241
00:13:09,963 --> 00:13:11,763
so creatively it's most interesting.

242
00:13:12,003 --> 00:13:17,564
- Poultra will be quite pleased.
- That's great. Thanks. Really good.

243
00:13:23,925 --> 00:13:26,605
We're working on
"Jimmy neutron" right now,

244
00:13:26,845 --> 00:13:30,486
and we enhance the sound
of the film, we bring it to life.

245
00:13:30,726 --> 00:13:35,366
We create sound effects by the use
of various props and sound gadgets

246
00:13:35,606 --> 00:13:37,166
that create a world of sound.

247
00:13:37,406 --> 00:13:39,567
<i>Jimmy neutron</i>
is a wonderful project for us,

248
00:13:39,807 --> 00:13:42,887
because we get to
entirely create the soundtrack.

249
00:13:43,127 --> 00:13:47,127
In animation, you really have free rein,

250
00:13:47,567 --> 00:13:50,928
and you can be as creative
and as crazy as you want.

251
00:13:51,168 --> 00:13:53,648
All the sounds that
you hear are made up,

252
00:13:53,888 --> 00:13:56,849
and I particularly like the egg creatures,

253
00:13:57,089 --> 00:14:00,010
because they have a gooey kind
of sound, but they're in armour,

254
00:14:00,249 --> 00:14:04,130
and they're really fun
to create sound effects.

255
00:14:07,250 --> 00:14:11,450
Jimmy neutron is a little boy genius
who creates wonderful inventions,

256
00:14:11,690 --> 00:14:14,811
and one of his inventions is his dog.

257
00:14:15,051 --> 00:14:20,452
Normally when we do dogs, we'll use
gloves with little nail clips attached.

258
00:14:21,291 --> 00:14:23,532
But Goddard,
he is the coolest robot ever,

259
00:14:23,772 --> 00:14:26,692
so we had to come up with
some great sounds for him.

260
00:14:26,932 --> 00:14:32,173
And he looks like he has little tin cans
for feet, so that's what we use.

261
00:14:34,253 --> 00:14:38,734
The most fun I have is going to work,
and we just get together, and we play.

262
00:14:38,973 --> 00:14:42,734
We create these sound effects,
and we play.

263
00:14:44,894 --> 00:14:45,815
That sounded great.

264
00:14:46,055 --> 00:14:49,175
Now I'd like to go into the ending.

265
00:14:49,895 --> 00:14:52,416
If it's fun to work on,
it's gonna be fun to watch.

266
00:14:52,656 --> 00:14:54,456
That's how we treat the whole process,

267
00:14:54,696 --> 00:14:59,216
in terms of voices, animation,
everybody that's working on it.

268
00:14:59,377 --> 00:15:01,656
There are as many jokes
for adults as for kids.

269
00:15:01,896 --> 00:15:04,216
<i>But, dad, all my friends</i>
<i>are gonna be there.</i>

270
00:15:04,457 --> 00:15:08,178
<i>I know, son. But if all your friends were</i>
<i>named cliff, would you jump off them?</i>

271
00:15:08,418 --> 00:15:11,298
It's clever, and funny, and slapsticky,

272
00:15:11,538 --> 00:15:16,338
all rolled into one delicious
bonbon of entertainment.

273
00:15:19,299 --> 00:15:21,259
It's wackier than I expected.

274
00:15:21,499 --> 00:15:25,860
And you will find that wackiness
reflected in our performances.

275
00:15:26,100 --> 00:15:27,860
I can't wait.

276
00:15:28,099 --> 00:15:30,940
<i>Gotta blast!</i>