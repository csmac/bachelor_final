﻿1
00:04:55,420 --> 00:04:57,001
Hey.

2
00:06:21,798 --> 00:06:24,710
You haven't changed. Please, take a seat.

3
00:06:26,094 --> 00:06:27,630
How is your sister?

4
00:06:28,471 --> 00:06:29,881
I have three sisters.

5
00:06:29,973 --> 00:06:32,635
The one with the big tits.
The one I always wanted to fuck.

6
00:06:34,269 --> 00:06:35,475
I hear good things, Latif.

7
00:06:36,271 --> 00:06:38,432
It says here you can shoot straight.

8
00:06:38,523 --> 00:06:41,560
Officer material, promoted already.
Lieutenant.

9
00:06:42,027 --> 00:06:44,234
Please, help yourself.

10
00:06:51,870 --> 00:06:52,905
Juice?

11
00:07:00,045 --> 00:07:02,661
I won't keep you guessing, Latif.

12
00:07:02,756 --> 00:07:04,371
I want you to come and work for me.

13
00:07:05,050 --> 00:07:07,507
- Work for you?
- Here, in Baghdad.

14
00:07:07,636 --> 00:07:09,342
Like old friends.

15
00:07:09,429 --> 00:07:12,341
- We were friends, weren't we, at school?
- Classmates.

16
00:07:14,351 --> 00:07:16,433
Didn't you once paint a picture
of my father?

17
00:07:16,519 --> 00:07:18,134
- You still have it?
- I gave it to you.

18
00:07:19,064 --> 00:07:22,352
I have an awful feeling
I told him I did it myself.

19
00:07:23,818 --> 00:07:26,275
Do you know who this is?

20
00:07:26,363 --> 00:07:28,228
- Of course.
- No, no, no.

21
00:07:28,323 --> 00:07:30,109
It is not who you think it is.

22
00:07:30,200 --> 00:07:32,065
His name is Faoaz al-Emari.

23
00:07:32,827 --> 00:07:35,990
Not Saddam. I see him all of the time.

24
00:07:36,081 --> 00:07:38,823
In the newspapers, in the magazines,
on television.

25
00:07:38,917 --> 00:07:41,704
Even I cannot tell who is who.

26
00:07:41,795 --> 00:07:44,036
It is not so unusual, Latif.

27
00:07:44,381 --> 00:07:47,748
Stalin had a double. He had dozens.
So did the Shah.

28
00:07:48,218 --> 00:07:49,207
The country is at war.

29
00:07:49,302 --> 00:07:51,384
My father cannot be expected
to be everywhere at once.

30
00:07:51,471 --> 00:07:55,214
Half the time there is no good reason,
but the people, they expect it.

31
00:07:55,934 --> 00:07:58,550
Add to that there is always the chance
some fucked-up Shi'a scum

32
00:07:58,645 --> 00:08:00,601
will take a shot at him.

33
00:08:01,731 --> 00:08:03,847
Look at yourself, Latif.

34
00:08:03,942 --> 00:08:06,274
Look. Look, look.

35
00:08:06,361 --> 00:08:08,397
Look at me.

36
00:08:08,488 --> 00:08:09,944
We could be twins, no?

37
00:08:10,073 --> 00:08:11,984
You are taller.

38
00:08:12,075 --> 00:08:14,066
How much? This much?

39
00:08:15,370 --> 00:08:18,032
Didn't they used to say
you looked like Uday Saddam Hussein?

40
00:08:18,123 --> 00:08:20,159
Didn't they used to say that at school?

41
00:08:21,501 --> 00:08:23,241
I want you, Latif.

42
00:08:24,754 --> 00:08:27,086
I want you to be my fidai.

43
00:08:28,133 --> 00:08:30,249
I want you to be my brother.

44
00:08:32,262 --> 00:08:34,423
Don't say anything.

45
00:08:34,514 --> 00:08:36,596
Take 10 minutes.

46
00:08:36,683 --> 00:08:38,674
Think it over.

47
00:08:40,270 --> 00:08:42,261
What happens if I say no?

48
00:08:46,026 --> 00:08:48,017
T en minutes.

49
00:09:03,877 --> 00:09:05,333
He is shitting bricks.

50
00:09:05,420 --> 00:09:08,958
His grandfather was a Kurd. It's a pity.

51
00:09:09,049 --> 00:09:11,756
But the family's been in Baghdad
a long time.

52
00:09:11,843 --> 00:09:13,379
His father's done well.

53
00:09:14,262 --> 00:09:17,095
I believe he sells domestic appliances.

54
00:09:17,182 --> 00:09:19,924
He's never said a word against the party.

55
00:09:21,269 --> 00:09:25,558
Latif is the best we could find.
And we looked.

56
00:09:25,648 --> 00:09:27,639
Well, I have to have him.

57
00:09:31,738 --> 00:09:34,320
You will have whatever you want.

58
00:09:34,407 --> 00:09:37,740
Everything I own will belong to you.

59
00:09:38,495 --> 00:09:39,985
Uday,

60
00:09:41,164 --> 00:09:42,950
I am my father's son.

61
00:09:44,751 --> 00:09:47,663
I would give my last drop of blood for Iraq.

62
00:09:48,922 --> 00:09:52,710
But when this war is over,
I'm going to work in his business.

63
00:09:53,927 --> 00:09:56,259
I am the eldest son.

64
00:09:57,514 --> 00:10:00,881
I have brothers and sisters.
I have given them my word.

65
00:10:02,060 --> 00:10:03,971
- Sleep on it.
- Uday...

66
00:10:05,563 --> 00:10:07,474
You are asking me to

67
00:10:07,941 --> 00:10:09,602
extinguish myself.

68
00:11:14,591 --> 00:11:16,206
Thought it over?

69
00:11:44,621 --> 00:11:47,078
Please be clear about this, Latif.

70
00:11:48,166 --> 00:11:52,000
Uday has chosen you. You belong to him.

71
00:11:52,754 --> 00:11:55,962
You have about five minutes
to think about this

72
00:11:56,966 --> 00:12:00,550
before a car pulls up
outside your house in Al-Adhamiya

73
00:12:01,095 --> 00:12:02,960
and your family, every one of them,

74
00:12:03,681 --> 00:12:07,390
your father, your mother,
your sisters and brothers,

75
00:12:07,936 --> 00:12:09,972
is thrown into Abu Ghraib.

76
00:12:11,814 --> 00:12:14,305
God willing, they will die quickly.

77
00:12:15,610 --> 00:12:18,898
I've said too much.
You have about two minutes left.

78
00:12:29,874 --> 00:12:32,911
You'll have a cook and four maids.

79
00:12:33,044 --> 00:12:35,831
These pretty girls will get you
anything you feel like.

80
00:12:35,964 --> 00:12:39,582
Strawberries in winter, figs in July.
Just ask.

81
00:12:40,176 --> 00:12:42,883
But lay a finger on one of them
without prior permission

82
00:12:42,971 --> 00:12:45,508
and you'll never walk upright again.

83
00:12:47,767 --> 00:12:50,304
And these are Uday's rooms.

84
00:12:50,395 --> 00:12:53,853
He wants you to use them,
now that you are to be his brother.

85
00:12:54,649 --> 00:12:58,983
Leather sofas, Italian marble,
all designed by Gianfranco Giannelli.

86
00:13:00,280 --> 00:13:02,316
Handmade shoes, suits,

87
00:13:02,407 --> 00:13:05,490
Brioni, Versace, Armani, of course.

88
00:13:05,576 --> 00:13:10,286
Silk ties, watches. Breitling, Cartier, Rolex.

89
00:13:10,373 --> 00:13:12,079
Silk pajamas.

90
00:13:13,793 --> 00:13:15,533
These are very nice.

91
00:13:15,628 --> 00:13:17,243
Poor child never knows what to wear.

92
00:13:17,338 --> 00:13:19,124
He changes his mind every five minutes.

93
00:13:19,215 --> 00:13:20,921
Depends on what car he's driving.

94
00:13:21,009 --> 00:13:24,752
Honestly, habibi, that boy, he wears me out.

95
00:13:24,846 --> 00:13:27,462
You should probably clean yourself up.

96
00:13:52,874 --> 00:13:54,114
No calls.

97
00:13:57,920 --> 00:14:00,662
Incoming, yes. Outgoing, no.

98
00:14:11,934 --> 00:14:13,720
My name is Munem Hammed al-Tikriti.

99
00:14:15,021 --> 00:14:17,933
We're going to be seeing
a lot of each other.

100
00:14:20,276 --> 00:14:22,688
I write to my mother once a week.

101
00:14:23,821 --> 00:14:26,107
She's going to think that I got killed in Iran.

102
00:14:26,240 --> 00:14:27,821
Inshallah.

103
00:14:27,909 --> 00:14:29,774
Inshallah?

104
00:14:30,244 --> 00:14:31,609
My family are going to think that I'm dead.

105
00:14:32,330 --> 00:14:34,867
Let them think you died a martyr.

106
00:14:34,957 --> 00:14:37,664
I know it's difficult,
but it's what Uday wants.

107
00:14:37,752 --> 00:14:39,458
He will accept nothing less.

108
00:14:39,545 --> 00:14:43,379
She will be knocking on the door
of every hospital in Baghdad.

109
00:14:44,967 --> 00:14:47,424
When you say your prayers tonight,

110
00:14:47,512 --> 00:14:50,049
when you turn out the light
and close your eyes,

111
00:14:50,139 --> 00:14:52,721
this is what
you should be saying to yourself,

112
00:14:52,809 --> 00:14:56,222
"Latif Yahia is dead. He died in Iran.

113
00:14:57,563 --> 00:15:00,145
"May God have mercy on him.

114
00:15:00,274 --> 00:15:03,937
"Now I am Uday Saddam Hussein."

115
00:15:06,572 --> 00:15:08,312
Guten Tag.

116
00:15:08,408 --> 00:15:10,649
Take off your clothes, please.

117
00:15:24,298 --> 00:15:25,663
Cough.

118
00:15:27,718 --> 00:15:29,208
One more time.

119
00:15:30,888 --> 00:15:32,924
And one more time, please.

120
00:15:38,229 --> 00:15:40,185
Uday is three centimeters taller.

121
00:15:40,690 --> 00:15:42,100
We can use built-up shoes.

122
00:15:54,162 --> 00:15:55,572
Look at that. He is perfect.

123
00:15:55,705 --> 00:15:58,697
- Uday, this is boring.
- Shut up.

124
00:15:59,750 --> 00:16:02,036
How can you sit here all night
looking at yourself?

125
00:16:02,170 --> 00:16:04,912
- He looks great.
- He doesn't even look like you.

126
00:16:05,006 --> 00:16:06,166
What?

127
00:16:06,257 --> 00:16:07,997
His cock's too big.

128
00:16:13,347 --> 00:16:15,963
What do you mean, his cock is too big?

129
00:16:22,607 --> 00:16:25,144
Uday's life story. Learn it.

130
00:16:38,789 --> 00:16:40,325
Come on, come on, tell me.

131
00:16:40,416 --> 00:16:43,328
Shape of face, hair, nose, physique,
all in the high 90s.

132
00:16:43,419 --> 00:16:45,751
- Good.
- Your eyes are five millimeters bigger.

133
00:16:45,838 --> 00:16:48,454
- They can use make-up.
- And the teeth? What about the teeth?

134
00:17:27,713 --> 00:17:29,453
Two peas in a pod.

135
00:17:31,008 --> 00:17:33,841
- I was just trying the teeth.
- Smile. Go on, smile.

136
00:17:33,928 --> 00:17:36,169
No, no, no. Like this.

137
00:17:39,058 --> 00:17:42,221
And do the hair. Do the hair.

138
00:17:42,311 --> 00:17:45,098
You like the Rolex.

139
00:17:45,189 --> 00:17:46,429
Keep it.

140
00:17:54,156 --> 00:17:55,236
You look good in blue.

141
00:17:56,409 --> 00:17:58,616
Yes, it definitely suits you.

142
00:18:03,541 --> 00:18:05,532
Latif, hurry up.

143
00:18:06,502 --> 00:18:08,709
- There's too much ice.
- Can I say blue is your color?

144
00:18:08,796 --> 00:18:09,911
Shut up.

145
00:18:40,911 --> 00:18:43,118
I like the way you fight.

146
00:18:43,205 --> 00:18:44,194
You fight like me.

147
00:18:45,333 --> 00:18:46,994
He fights like me.

148
00:18:51,631 --> 00:18:53,872
I had a word with Dr. Linz.

149
00:18:53,966 --> 00:18:56,127
He says your cock's too big.

150
00:18:56,218 --> 00:18:58,834
We need to make a surgical reduction.

151
00:18:59,847 --> 00:19:02,554
- A what?
- Chop a bit off.

152
00:19:03,100 --> 00:19:04,761
- What?
- Not much.

153
00:19:05,269 --> 00:19:06,509
Just a teeny-weeny.

154
00:19:06,979 --> 00:19:08,515
Not much?

155
00:19:08,606 --> 00:19:12,064
My cock is well-known in Baghdad.
The women, they talk.

156
00:19:14,070 --> 00:19:15,810
I'm joking with you, you fuck.

157
00:19:27,917 --> 00:19:29,703
This is Said Kammuneh.

158
00:19:29,794 --> 00:19:32,752
He's a member
of the Iraqi Olympic Committee.

159
00:19:33,881 --> 00:19:36,088
He has something to show you.

160
00:19:49,105 --> 00:19:53,690
This is from the personal collection
of Uday Saddam Hussein.

161
00:20:11,168 --> 00:20:13,659
That guy, he was a police officer.

162
00:20:13,754 --> 00:20:16,166
We were on the same football team.

163
00:20:17,425 --> 00:20:19,791
Great left foot. Good in the air.

164
00:20:22,555 --> 00:20:24,967
I shot this myself. Wait a minute.

165
00:20:30,938 --> 00:20:32,269
Good work, huh?

166
00:20:32,356 --> 00:20:35,689
We got a lot of this shit
from the East Germans and the Russians.

167
00:20:36,694 --> 00:20:39,356
See that guy? That's Rakti, the wrestler.

168
00:20:39,447 --> 00:20:41,654
He won a bronze medal in Moscow.

169
00:20:41,824 --> 00:20:44,486
The President calls him
the sharp sword of the Republic.

170
00:20:45,286 --> 00:20:48,528
You know that gas station
on Haifa Street by the lights?

171
00:20:48,622 --> 00:20:50,738
The President gave him that.

172
00:20:51,625 --> 00:20:54,116
The fat fuck's making 20 grand a month.

173
00:20:55,004 --> 00:20:56,665
That's good money.

174
00:21:00,593 --> 00:21:03,756
Look, I'll leave this with you.

175
00:21:20,529 --> 00:21:22,645
- Seen enough?
- Enough?

176
00:21:23,657 --> 00:21:25,818
Have I seen enough?

177
00:21:25,910 --> 00:21:29,073
I remember the day Saddam Hussein
became president of this country.

178
00:21:29,580 --> 00:21:32,788
I was still in short trousers.
We ran through the streets, shouting,

179
00:21:32,875 --> 00:21:36,367
"Saddam! Saddam Hussein!
Saddam Hussein!" Firing our guns.

180
00:21:36,796 --> 00:21:39,458
Even my father. Even my father.

181
00:21:39,548 --> 00:21:40,958
This stuff, Munem...

182
00:21:41,050 --> 00:21:43,382
These people...
I hope Said said something about this.

183
00:21:43,469 --> 00:21:45,676
They're criminals, deserters.

184
00:21:46,388 --> 00:21:49,095
Pornographers, counterfeiters,
murderers, of course.

185
00:21:49,183 --> 00:21:51,219
Putschists. Enemies of the Republic.

186
00:21:51,685 --> 00:21:54,973
Saddam Hussein built this country.
Never forget that.

187
00:21:55,064 --> 00:21:58,022
The schools, the roads, hospitals,
the mosques.

188
00:21:58,400 --> 00:22:00,766
He gave the Iraqi people
everything they want.

189
00:22:00,861 --> 00:22:03,523
He demands only one thing in return.

190
00:22:03,614 --> 00:22:06,526
Never raise a hand against his family.

191
00:22:07,284 --> 00:22:09,696
Do so and you will suffer.

192
00:22:09,787 --> 00:22:12,449
The people you love will suffer.

193
00:22:12,873 --> 00:22:16,616
Do well here, work hard
and you'll be one of the family.

194
00:22:21,423 --> 00:22:25,166
One day all this will be over. Inshallah.

195
00:22:25,803 --> 00:22:27,168
Inshallah?

196
00:22:28,597 --> 00:22:30,383
Only God knows when.

197
00:23:18,689 --> 00:23:20,771
Come, come, come, come, come.

198
00:23:38,500 --> 00:23:41,867
Munem disapproves. Look at him.

199
00:23:42,463 --> 00:23:45,421
He's angry with me.
He thinks I should leave you at home.

200
00:23:46,842 --> 00:23:49,959
- Who's this?
- Latif? He's my brother.

201
00:23:51,513 --> 00:23:55,847
Nobody must know this,
but Saddam, he has three sons.

202
00:23:55,935 --> 00:23:57,050
You tell anybody this,

203
00:23:57,144 --> 00:24:00,887
I will have to cut your tongue out
with a razor and feed it to the cat.

204
00:24:03,859 --> 00:24:06,441
Everything I own, I give to him.

205
00:24:06,695 --> 00:24:07,901
You don't own me.

206
00:24:08,530 --> 00:24:10,942
Sarrab, best fuck in Beirut.

207
00:24:18,332 --> 00:24:21,870
Loosen your tie. Let me do it for you.

208
00:24:23,504 --> 00:24:25,836
There. Isn't that better?

209
00:24:30,302 --> 00:24:31,883
Be careful what you say.

210
00:24:32,638 --> 00:24:34,378
I can read your mind.

211
00:24:36,100 --> 00:24:38,091
Are you really the President's son?

212
00:24:39,186 --> 00:24:41,347
You know what they say on TV.

213
00:24:42,439 --> 00:24:44,054
We are all the President's sons.

214
00:24:45,567 --> 00:24:46,773
Hey!

215
00:24:48,278 --> 00:24:49,358
Hey!

216
00:25:48,338 --> 00:25:50,374
Look at him.

217
00:25:50,466 --> 00:25:53,378
He can't keep his hands off that drag queen.

218
00:25:58,265 --> 00:26:00,426
Poor Uday.

219
00:26:00,517 --> 00:26:02,849
Sometimes I think he wants
to fuck himself to death.

220
00:26:10,903 --> 00:26:12,894
Will you excuse me a moment?

221
00:26:20,370 --> 00:26:21,485
Keys.

222
00:26:22,790 --> 00:26:24,405
Give me the keys.

223
00:26:27,669 --> 00:26:29,284
Give me the fucking keys!

224
00:26:30,089 --> 00:26:33,001
Baby, I so love your tits!

225
00:26:33,967 --> 00:26:36,504
I hope so, lover. You paid for them.

226
00:26:59,159 --> 00:27:01,650
What have you done with my brother?

227
00:28:02,431 --> 00:28:03,967
You see, Munem?

228
00:28:05,767 --> 00:28:09,510
Discipline. We must have discipline.

229
00:28:10,355 --> 00:28:12,687
If we do not have discipline,
we have no control.

230
00:28:12,774 --> 00:28:14,856
If we have no control, we have chaos.

231
00:28:14,943 --> 00:28:17,434
If we have chaos, we have disorder.

232
00:28:25,871 --> 00:28:29,409
You do understand, Latif? It is quite simple.

233
00:28:30,918 --> 00:28:32,909
We must have discipline.

234
00:28:34,630 --> 00:28:35,870
Got it?

235
00:28:37,966 --> 00:28:39,172
Got it?

236
00:28:41,303 --> 00:28:42,383
Good.

237
00:28:53,440 --> 00:28:56,273
Latif. Five minutes.

238
00:29:19,007 --> 00:29:20,167
Latif.

239
00:29:52,207 --> 00:29:54,368
- No, no, no!
- We must have discipline!

240
00:29:56,795 --> 00:29:59,537
Discipline! We must have discipline!

241
00:30:10,517 --> 00:30:11,723
How was that, Munem?

242
00:30:13,228 --> 00:30:14,559
Excellent.

243
00:30:16,857 --> 00:30:18,017
Sorry.

244
00:30:30,203 --> 00:30:31,238
Good.

245
00:30:58,190 --> 00:30:59,771
Empty your pockets.

246
00:31:49,282 --> 00:31:51,147
My grandfather,

247
00:31:51,493 --> 00:31:55,156
he talks to me the same way he did
when I was 10 years old.

248
00:32:25,360 --> 00:32:26,816
God is great.

249
00:32:28,321 --> 00:32:30,061
He gave me two sons.

250
00:32:31,366 --> 00:32:32,856
Now I have three.

251
00:32:50,177 --> 00:32:53,886
Make sure you give me no reason
to be angry with you.

252
00:32:59,186 --> 00:33:00,471
And?

253
00:33:08,403 --> 00:33:11,691
Uday, you owe me 50 dinar.

254
00:33:12,324 --> 00:33:13,530
50 dinar?

255
00:33:14,117 --> 00:33:15,152
You see?

256
00:33:15,243 --> 00:33:19,987
Trying to get money from your brother
is like trying to get blood from a stone.

257
00:33:22,834 --> 00:33:23,914
What?

258
00:33:26,546 --> 00:33:27,581
It's not him.

259
00:33:28,798 --> 00:33:30,038
What?

260
00:34:26,773 --> 00:34:29,480
It's your turn, brother.

261
00:34:29,568 --> 00:34:32,184
Fuck them. Fucking Kuwaitis.

262
00:34:33,989 --> 00:34:36,071
I hate them all.

263
00:34:36,157 --> 00:34:38,022
Worse than horseflies.

264
00:34:44,499 --> 00:34:47,332
My brothers, welcome to Baghdad.

265
00:35:39,846 --> 00:35:40,881
Fuck!

266
00:35:41,556 --> 00:35:44,263
Fuck! Fuck, fuck, fuck!

267
00:35:44,351 --> 00:35:45,557
Fucking Kuwaitis!

268
00:35:45,644 --> 00:35:47,475
I fucking hate Jews, I hate horseflies

269
00:35:47,562 --> 00:35:48,972
and I hate fucking Persians,

270
00:35:49,064 --> 00:35:51,555
but I hate fucking Kuwaitis more!

271
00:35:51,650 --> 00:35:53,060
They insult us.

272
00:35:53,151 --> 00:35:56,643
They are filth. They maul our women.

273
00:35:56,738 --> 00:35:59,571
The Kuwaitis have been
stealing $280 million a year

274
00:36:00,533 --> 00:36:02,239
from the oilfields at Rumaila.

275
00:36:02,369 --> 00:36:05,827
$280 million a year!

276
00:36:07,165 --> 00:36:08,951
You know we traced the weapon.

277
00:36:09,042 --> 00:36:12,250
I just know the fucking Kuwaitis
are behind this!

278
00:36:12,337 --> 00:36:13,668
I hope my father wipes them out.

279
00:36:14,589 --> 00:36:17,126
No, I'm telling you this.
I don't care who knows it!

280
00:36:17,217 --> 00:36:20,050
The age of the sheikhs is over. Over!

281
00:36:21,596 --> 00:36:23,928
- What?
- It's me, habibi.

282
00:36:25,934 --> 00:36:27,595
What are you doing right now?

283
00:36:27,686 --> 00:36:32,055
I'm getting my nails done. Nice and sharp.

284
00:36:32,148 --> 00:36:34,013
The better to scratch you with.

285
00:36:35,110 --> 00:36:38,602
Did I ever tell you
how much I love your ass?

286
00:36:38,697 --> 00:36:42,610
It's the best ass in Baghdad.
I'll see you soon.

287
00:36:43,284 --> 00:36:44,694
Hey, you, you!

288
00:36:45,453 --> 00:36:47,114
You'll meet many women.

289
00:36:47,997 --> 00:36:51,205
Whores, wealthy women from Al Mansour

290
00:36:51,292 --> 00:36:54,409
who come looking for a favor
and offer you favors in return.

291
00:36:54,504 --> 00:36:57,337
Schoolgirls he snatches off the street.

292
00:36:58,258 --> 00:37:01,421
- You'll be given your fair share.
- Ready, guys?

293
00:37:01,511 --> 00:37:05,800
Never even speak to a woman
Uday has chosen for himself.

294
00:37:09,728 --> 00:37:11,264
He is insane.

295
00:37:12,981 --> 00:37:15,768
You know it, Munem.

296
00:37:15,859 --> 00:37:17,065
He's insane.

297
00:37:18,653 --> 00:37:21,144
You turn a blind eye. You have no choice.

298
00:37:21,656 --> 00:37:23,317
I understand that.

299
00:37:24,284 --> 00:37:26,616
You are a good man in a bad job.

300
00:37:27,704 --> 00:37:28,819
Look at him.

301
00:37:30,665 --> 00:37:31,700
He's psychotic.

302
00:37:31,791 --> 00:37:34,703
Come on, kiss. Kiss more. More. Kiss, kiss.

303
00:37:36,171 --> 00:37:38,082
Who wants to sleep with me tonight? You?

304
00:38:07,368 --> 00:38:09,654
Through the window
of a British worker's flat,

305
00:38:09,746 --> 00:38:11,532
the Battle of Kuwait unfolds.

306
00:38:13,625 --> 00:38:17,209
Saddam Hussein, very simply,
you cannot bully your neighbor.

307
00:38:17,295 --> 00:38:19,832
You cannot wipe him out,
a member of the Arab League,

308
00:38:19,923 --> 00:38:21,288
a member of the United Nations.

309
00:38:22,550 --> 00:38:27,294
Saddam H ussein!
The hero of the Battle of Qadisiyyah.

310
00:38:27,388 --> 00:38:29,879
The knight of the Arab nations.

311
00:38:29,974 --> 00:38:33,137
Direct descendant of the prophet.

312
00:38:33,228 --> 00:38:35,184
Saddam the noble fighter.

313
00:38:35,980 --> 00:38:39,643
Scion of the family of Imam al-Hussein.

314
00:38:39,734 --> 00:38:44,569
Ladies and gentlemen,
Uday Saddam Hussein.

315
00:39:00,755 --> 00:39:04,668
The age of the sheikhs is over!

316
00:39:07,262 --> 00:39:10,004
With the help of Almighty God,

317
00:39:10,098 --> 00:39:12,589
Kuwait has come home!

318
00:40:13,578 --> 00:40:17,196
Hey, you. The teeth. You with the teeth.

319
00:40:17,457 --> 00:40:20,039
Come on. I'll give you a ride.

320
00:40:20,126 --> 00:40:23,994
Hey! Hey, hey, hey! You!
No, no, no, no. You, you, you.

321
00:40:28,635 --> 00:40:29,715
Hey, you.

322
00:40:30,845 --> 00:40:32,961
Don't be scared. Come for a ride.

323
00:40:34,849 --> 00:40:37,215
Come on, get in the car.
I'll give you a lift home.

324
00:40:37,310 --> 00:40:39,016
I'll take you back.

325
00:40:41,689 --> 00:40:45,307
Hey, don't be scared.
Why do you run? Why do you run?

326
00:40:45,401 --> 00:40:47,608
Come on. Why are you running?
I'll take you home.

327
00:40:49,489 --> 00:40:50,524
Come on.

328
00:40:51,157 --> 00:40:53,489
Hey. How about you?

329
00:40:55,536 --> 00:40:56,867
I love what you've done with your hair.

330
00:41:01,876 --> 00:41:04,037
Hey, hey, hey!

331
00:41:04,629 --> 00:41:07,621
It's okay, it's okay.
Don't be scared. Don't be scared.

332
00:41:07,715 --> 00:41:10,127
It's all right.

333
00:41:10,218 --> 00:41:14,052
You're beautiful. Don't be scared.
I'm not going to hurt you.

334
00:41:15,139 --> 00:41:16,879
Brother, she's beautiful. You want her?

335
00:41:18,142 --> 00:41:20,007
- Take.
- No!

336
00:41:20,103 --> 00:41:23,641
What? What?
Don't fucking look at me like that.

337
00:41:23,731 --> 00:41:25,312
- Like what?
- Like that.

338
00:41:25,400 --> 00:41:27,766
I know that fucking look.
I know what you're thinking.

339
00:41:27,860 --> 00:41:30,146
Think what the fuck you like.
I like cunt. I want cunt.

340
00:41:30,238 --> 00:41:32,024
I see a cunt, I want to fuck it. I don't care.

341
00:41:32,115 --> 00:41:34,356
I love my country,
I love my mother, my father.

342
00:41:34,450 --> 00:41:37,066
But I love cunt more than I love God!

343
00:42:35,678 --> 00:42:38,135
Does her mama know that she's out?

344
00:42:49,317 --> 00:42:51,308
You know this man?

345
00:42:52,862 --> 00:42:54,648
This is Kamel Hannah.

346
00:42:55,364 --> 00:42:58,026
He's my father's greatest friend.

347
00:42:58,117 --> 00:42:59,823
He tastes his food.

348
00:43:00,995 --> 00:43:03,532
Did she get a note from her teacher?

349
00:43:10,338 --> 00:43:11,373
See this watch?

350
00:43:11,756 --> 00:43:12,916
You see this watch?

351
00:43:13,007 --> 00:43:14,588
How much you think a watch like this costs?

352
00:43:14,675 --> 00:43:17,758
How much? How much you think
a watch like this costs? $10,000.

353
00:43:17,845 --> 00:43:21,633
It's a $10,000 watch.
My father bought him that watch.

354
00:43:21,724 --> 00:43:26,058
Why? Why did my father buy Kamel Hannah
a watch like this?

355
00:43:26,145 --> 00:43:27,760
Shall I tell you why? Shall I tell you why?

356
00:43:27,855 --> 00:43:30,312
Shall I tell you why my father
loves Kamel Hannah so much?

357
00:43:30,399 --> 00:43:33,516
- Because he gets his whores for him.
- Rubbish.

358
00:43:35,196 --> 00:43:38,313
Our mother... Our mother
sits around the house and weeps

359
00:43:38,407 --> 00:43:41,524
because of the whores
this man brings my father.

360
00:43:43,204 --> 00:43:45,695
She wanders, she walks around the palace,

361
00:43:45,832 --> 00:43:47,322
walks around the palace,
staring in the mirrors,

362
00:43:47,416 --> 00:43:49,702
staring in the mirrors,
looking at herself like a fucking zombie

363
00:43:49,794 --> 00:43:53,207
because of the whores
this man brings my father!

364
00:43:56,676 --> 00:43:58,212
- Shall I kill him, brother?
- Uday.

365
00:44:04,142 --> 00:44:06,053
What do you think, brother?

366
00:44:06,144 --> 00:44:08,055
What do you think? Shall I kill him?

367
00:44:08,479 --> 00:44:10,140
Kill your father's greatest friend?

368
00:44:10,231 --> 00:44:13,894
He is a twittering little pimp
that has driven my mother mad.

369
00:44:15,111 --> 00:44:17,898
- Maybe I'll let you kill him for me.
- Uday.

370
00:44:18,406 --> 00:44:20,067
No.

371
00:44:21,033 --> 00:44:24,150
I don't know him. It's meaningless.

372
00:44:24,245 --> 00:44:26,702
If I am going to kill somebody,
I want to know why I'm going to do it.

373
00:44:31,502 --> 00:44:32,582
Uday.

374
00:44:35,214 --> 00:44:36,294
Uday.

375
00:44:38,176 --> 00:44:40,918
- You fucker!
- Easy, easy.

376
00:44:44,056 --> 00:44:45,921
You should be ashamed of your...

377
00:44:48,144 --> 00:44:49,600
Motherfucker.

378
00:44:51,981 --> 00:44:54,222
Come into my bedroom. Come on.

379
00:44:55,234 --> 00:44:57,395
Would you like a drink? A nice drink?

380
00:44:58,279 --> 00:45:00,190
I like to drink whiskey.

381
00:45:01,616 --> 00:45:03,652
Take it, here, here.

382
00:45:12,960 --> 00:45:15,702
Would you like some?
Would you like some?

383
00:45:20,301 --> 00:45:23,213
That's it. In just one. In one. In one.

384
00:45:29,560 --> 00:45:33,144
You're very beautiful.
You're a very beautiful girl.

385
00:45:34,273 --> 00:45:36,730
Beg me... Beg me to fuck you.

386
00:45:36,817 --> 00:45:39,729
Beg me. Go on, beg me to fuck you.

387
00:45:41,239 --> 00:45:42,820
He humiliate me.

388
00:45:49,372 --> 00:45:53,832
Why not? Just say it.
Beg me. Beg me to fuck you.

389
00:45:53,918 --> 00:45:56,079
- I can't.
- Come on.

390
00:45:56,170 --> 00:45:57,910
- I...
- Yes?

391
00:45:58,005 --> 00:46:01,122
- ...beg...
- ...beg...

392
00:46:01,217 --> 00:46:03,173
...you... I can't.

393
00:46:03,261 --> 00:46:05,126
- Go on.
- No.

394
00:46:10,893 --> 00:46:13,680
- Beg me. Beg me.
- No.

395
00:46:16,565 --> 00:46:19,352
Beg me! Beg me to fuck you.

396
00:46:20,194 --> 00:46:22,150
Beg me to fuck you.

397
00:46:22,947 --> 00:46:25,484
Beg me to fuck you.

398
00:46:26,742 --> 00:46:30,030
Beg me to fuck you! Beg me!

399
00:46:30,413 --> 00:46:32,995
- Beg! Beg! Beg!
- I beg...

400
00:46:35,668 --> 00:46:38,375
Yassem! Yassem!

401
00:46:39,547 --> 00:46:42,380
- Who's doing that?
- That pig, Kamel Hannah. He won't stop.

402
00:46:44,051 --> 00:46:45,882
He says he doesn't take orders
from faggots.

403
00:47:04,113 --> 00:47:05,193
Stop!

404
00:47:06,073 --> 00:47:08,405
- Fuck you.
- I order you to stop!

405
00:47:08,492 --> 00:47:12,155
I don't take orders from you!
I take orders from Saddam Hussein.

406
00:47:35,311 --> 00:47:36,346
Uday, Uday...

407
00:47:38,898 --> 00:47:40,104
No! No!

408
00:47:47,490 --> 00:47:49,446
Boss, come on. Let's go.

409
00:47:56,665 --> 00:47:57,996
Just get her out of here, okay?

410
00:49:28,382 --> 00:49:30,589
He took pills, sleeping pills.

411
00:50:00,498 --> 00:50:02,079
Give me the knife.

412
00:50:12,176 --> 00:50:14,258
You'll kill him.

413
00:50:14,386 --> 00:50:16,342
The loss of blood and...

414
00:50:47,836 --> 00:50:50,293
I should have gelded him at birth.

415
00:52:02,494 --> 00:52:06,612
At 7:00 tonight the armed forces
of the United States began an operation

416
00:52:06,707 --> 00:52:08,322
at the direction of the President

417
00:52:08,709 --> 00:52:12,293
to force Saddam Hussein
to withdraw his troops from Kuwait

418
00:52:12,379 --> 00:52:15,291
and to end his occupation of their country.

419
00:52:27,561 --> 00:52:29,768
That's the latest Tomahawk cruise missile

420
00:52:29,855 --> 00:52:31,811
to fly just over our position here.

421
00:52:32,107 --> 00:52:35,224
Anti-aircraft gunners all around
have been trying to shoot them down.

422
00:52:37,738 --> 00:52:40,480
I have said to the people of Iraq

423
00:52:40,574 --> 00:52:42,906
that our quarrel was not with them,

424
00:52:43,911 --> 00:52:46,243
but instead with their leadership

425
00:52:46,330 --> 00:52:48,742
and above all with Saddam Hussein.

426
00:53:06,433 --> 00:53:09,391
Kuwait has been stealing our oil.

427
00:53:09,478 --> 00:53:11,594
Millions of dollars a year.

428
00:53:12,439 --> 00:53:14,475
Millions of dollars. Millions of dollars.

429
00:53:14,608 --> 00:53:19,022
Kuwait has been stealing our oil.
Millions of dollars a year.

430
00:53:19,113 --> 00:53:23,026
From the oilfields.
From our oilfields at Rumaila.

431
00:53:23,826 --> 00:53:25,032
Kuwait.

432
00:53:26,829 --> 00:53:28,410
Kuwait, Kuwait.

433
00:53:28,497 --> 00:53:31,113
Kuwait has stolen our Iraqi oil!

434
00:53:31,208 --> 00:53:34,291
Millions of dollars a year
from our oilfields at Rumaila.

435
00:53:34,378 --> 00:53:36,835
Our country is losing
more than a billion dollars a year!

436
00:53:39,133 --> 00:53:40,498
Take the glasses off.

437
00:53:40,592 --> 00:53:42,128
What are you doing here?

438
00:53:42,219 --> 00:53:45,086
- How did you get in?
- Do you want me to go?

439
00:53:47,516 --> 00:53:51,179
Why are you wearing Ray-Bans
at 1 :00 in the morning?

440
00:53:51,270 --> 00:53:52,726
Take them off.

441
00:53:53,647 --> 00:53:56,639
- I want to be sure it's you.
- Are you sure?

442
00:53:57,693 --> 00:53:58,899
I can always tell.

443
00:53:58,986 --> 00:54:01,022
You cannot stay here.

444
00:54:01,989 --> 00:54:03,729
Have you seen what's going on outside?

445
00:54:06,034 --> 00:54:09,777
Hear what Saddam said on television?
"The mother of all battles."

446
00:54:11,039 --> 00:54:14,623
Iraqi people are not stupid.
Nobody believes that crap.

447
00:54:16,545 --> 00:54:20,663
Iraq is going to burn to the ground.
We won't see that on TV.

448
00:54:23,427 --> 00:54:25,839
And Uday? Is he reporting for duty?

449
00:54:26,472 --> 00:54:30,090
No. Uday has gone to bed
with a splitting headache

450
00:54:30,184 --> 00:54:34,348
because Yassem Al-Helou forgot
to press his tweed hunting breeches.

451
00:54:35,355 --> 00:54:38,313
He's a child. He'll tire of you.

452
00:54:39,109 --> 00:54:41,020
He tires of everything.

453
00:54:41,737 --> 00:54:44,194
One day soon, he'll tire of me

454
00:54:44,281 --> 00:54:47,819
and I'll end up
at the bottom of the lake with the fish.