﻿1
00:00:05,213 --> 00:00:07,616
I love being.

2
00:00:07,617 --> 00:00:09,568
[Crowd cheering]

3
00:00:09,569 --> 00:00:13,180
But my friend Olivia
has bigger dreams.

4
00:00:13,181 --> 00:00:16,141
Olivia majored in journalism
back when people

5
00:00:16,142 --> 00:00:19,102
gave a crap
about newspapers.

6
00:00:19,103 --> 00:00:22,398
Today's headline... tiny
Korean chick rejected again,

7
00:00:22,399 --> 00:00:25,051
friend offers
sincere support.

8
00:00:25,052 --> 00:00:27,069
I should just give up.

9
00:00:27,070 --> 00:00:28,278
You really should.

10
00:00:28,279 --> 00:00:29,863
Hey, Olivia, I can
cheer you up.

11
00:00:29,864 --> 00:00:32,375
Remember that baby
bird I rescued?

12
00:00:32,376 --> 00:00:33,776
I named him Lancelot.

13
00:00:33,777 --> 00:00:35,511
Look what
I taught him to do.

14
00:00:35,512 --> 00:00:37,346
[Whistling]

15
00:00:41,749 --> 00:00:44,370
Dee Dee, if some old lady tries
to give you an apple,

16
00:00:44,371 --> 00:00:45,587
you turn that bitch down.

17
00:00:45,588 --> 00:00:48,641
It took me 3 weeks to
nurse him back to health.

18
00:00:48,642 --> 00:00:50,242
Now he thinks
I'm his mommy.

19
00:00:50,243 --> 00:00:52,244
Can you say, "mommy"?

20
00:00:52,245 --> 00:00:56,598
Say, "mommy, mommy,
mommy, mama."

21
00:00:56,599 --> 00:00:57,683
Say, "mama."

22
00:00:57,684 --> 00:00:59,852
[Screeches]

23
00:01:02,395 --> 00:01:05,190
Well, now he can't.

24
00:01:12,029 --> 00:01:14,616
<font color="#00ffff">Sync & corrections by Rafael UPD</font>
<font color="#00ffff">www.Addic7ed.Com/</font>

25
00:01:18,627 --> 00:01:19,694
hey, did you guys hear
about the Hooters

26
00:01:19,695 --> 00:01:21,999
that's opening
down route 10?

27
00:01:22,000 --> 00:01:23,388
Hooters?
Mm-hmm.

28
00:01:23,389 --> 00:01:24,475
Please.

29
00:01:24,476 --> 00:01:27,368
I'm gonna open
a bar called wieners.

30
00:01:27,369 --> 00:01:30,037
And all the waiters would
have to wear a speedo

31
00:01:30,038 --> 00:01:33,925
with just the tip of their wiener
sticking out of the top.

32
00:01:34,969 --> 00:01:37,389
Nobody wants
to see penis cleavage.

33
00:01:37,390 --> 00:01:40,642
Especially
at my eye level.

34
00:01:40,643 --> 00:01:42,352
Look, guys,
the bottom line is

35
00:01:42,353 --> 00:01:43,721
we have to
compete with Hooters.

36
00:01:43,722 --> 00:01:45,106
So Jerry and I were
talking and I came up

37
00:01:45,107 --> 00:01:50,328
with a little idea for the uniforms
that he thought was brilliant.

38
00:01:50,735 --> 00:01:53,447
You're going to look
great in that, Rick.

39
00:01:53,448 --> 00:01:57,568
Look, I know you think that these
objectify women and they do,

40
00:01:57,569 --> 00:02:01,496
but that's our business
plan, so just go for it.

41
00:02:02,941 --> 00:02:03,941
What makes this worse

42
00:02:03,942 --> 00:02:05,994
is that my cousins in
Korea probably made this.

43
00:02:05,995 --> 00:02:07,419
Yeah.

44
00:02:07,420 --> 00:02:08,946
I am buying this for you

45
00:02:08,947 --> 00:02:10,965
because it took me
so long to get them.

46
00:02:10,966 --> 00:02:12,016
Oh, thank you.

47
00:02:12,017 --> 00:02:13,718
Hey, I couldn't help but
overhear your conversation

48
00:02:13,719 --> 00:02:17,522
about your new uniforms and I can
understand why you don't like them.

49
00:02:17,523 --> 00:02:21,092
But from a marketing point
of view, they make sense.

50
00:02:21,390 --> 00:02:23,594
I'm not doing
a porno movie.

51
00:02:23,595 --> 00:02:24,695
Oh, no, no, no.

52
00:02:24,696 --> 00:02:27,981
I'm a partner
in a marketing firm.

53
00:02:27,982 --> 00:02:29,716
Tim Cornick.

54
00:02:29,717 --> 00:02:30,901
Cornick and Chung.

55
00:02:30,902 --> 00:02:32,869
Oh, wow.

56
00:02:32,870 --> 00:02:34,237
Are you guys hiring?

57
00:02:34,238 --> 00:02:35,388
Are you looking?

58
00:02:35,389 --> 00:02:37,866
No, but my roommate is and
she majored in journalism.

59
00:02:37,867 --> 00:02:39,409
Ooh, that's rough.

60
00:02:39,784 --> 00:02:41,511
No, but she's really smart.

61
00:02:41,512 --> 00:02:43,246
And she's Asian,
so Chung will have

62
00:02:43,247 --> 00:02:45,282
someone to play
pai gaw poker with.

63
00:02:45,831 --> 00:02:47,918
I think we are looking
for a copywriter.

64
00:02:47,919 --> 00:02:49,653
I'd be happy
to get her an interview

65
00:02:49,654 --> 00:02:51,888
if you'd let me
take you out to dinner.

66
00:02:52,338 --> 00:02:55,926
Wow. That is
an unethical proposal.

67
00:02:55,927 --> 00:02:58,795
So, naturally, I'm in.

68
00:03:06,686 --> 00:03:08,088
Dee Dee, what are you doing?

69
00:03:08,437 --> 00:03:10,173
Making myself
look bigger.

70
00:03:10,690 --> 00:03:12,901
Why don't you
just eat something?

71
00:03:12,902 --> 00:03:13,877
I was reading online

72
00:03:13,878 --> 00:03:15,061
that it seems
like a-face

73
00:03:15,062 --> 00:03:16,580
may have
a dominance problem.

74
00:03:16,581 --> 00:03:18,265
Dee Dee, he's a cat.

75
00:03:18,266 --> 00:03:19,900
Cat's kill birds.

76
00:03:19,901 --> 00:03:21,218
A-face is going to have
to find some way

77
00:03:21,219 --> 00:03:23,078
to live in harmony
with Lancelot.

78
00:03:23,079 --> 00:03:25,188
That thing lived?

79
00:03:25,189 --> 00:03:27,824
Yes, but he's going to
have major trust issues.

80
00:03:27,825 --> 00:03:31,561
And I don't think he's going
to be able to have kids.

81
00:03:31,562 --> 00:03:35,732
Chelsea, thank you so much
for getting me this interview.

82
00:03:36,340 --> 00:03:38,919
I am happy to sleep
your way to the top.

83
00:03:39,754 --> 00:03:41,571
I just hope you
find a job you love.

84
00:03:41,572 --> 00:03:42,956
I love working
at the flower shop.

85
00:03:42,957 --> 00:03:45,241
I love the smell.
I love my smock.

86
00:03:45,242 --> 00:03:47,936
I don't even mind that I'm
cold and wet all the time.

87
00:03:48,245 --> 00:03:51,414
I just pretend I'm working
in a dog's nose.

88
00:03:51,415 --> 00:03:53,817
Dee Dee, if you ever
decide to drop acid,

89
00:03:53,818 --> 00:03:56,085
please make sure I'm there.

90
00:03:56,652 --> 00:03:57,587
You know what?

91
00:03:57,588 --> 00:03:59,940
I haven't had a prospect
this promising in 2 years.

92
00:03:59,941 --> 00:04:00,891
I'm so nervous.

93
00:04:00,892 --> 00:04:02,626
On "the apprentice"
the interview is always

94
00:04:02,627 --> 00:04:05,095
the most important part.
We should practice.

95
00:04:05,096 --> 00:04:07,664
Here, Chelsea, you
and I will be hr people.

96
00:04:07,665 --> 00:04:10,000
I'll be Mrs. Wiggenbottom.

97
00:04:10,001 --> 00:04:13,003
And I'll be Mrs.
take-it-in-the-bottom.

98
00:04:14,962 --> 00:04:15,939
Oh, you. Ok.

99
00:04:15,940 --> 00:04:18,074
Now, miss pack, out of all
the qualified applicants,

100
00:04:18,075 --> 00:04:21,478
what makes you think we should
hire you as assistant copywriter?

101
00:04:21,479 --> 00:04:23,079
Mrs. Wiggenbottom,
may I interrupt?

102
00:04:23,080 --> 00:04:24,164
Oh, yes, of course you may.

103
00:04:24,165 --> 00:04:27,617
Miss pack, are any of your
friends dating Tim Cornick?

104
00:04:28,642 --> 00:04:30,554
Yes.
You've got the job.

105
00:04:31,103 --> 00:04:32,389
That's not how it works.

106
00:04:32,390 --> 00:04:36,259
I mean, is that you got your
job, Mrs. take-it-in-the bottom?

107
00:04:37,109 --> 00:04:38,728
Yep.

108
00:04:39,570 --> 00:04:41,498
And my name.

109
00:04:43,100 --> 00:04:45,569
Chelsea: That night I
went out with Tim Cornick.

110
00:04:45,570 --> 00:04:47,170
Because I was doing
this to help Olivia,

111
00:04:47,171 --> 00:04:48,688
I really turned
on the charm.

112
00:04:48,689 --> 00:04:53,543
I just have to warn you, I am
a world class armpit farter.

113
00:04:54,362 --> 00:04:55,612
I was the youngest of 5 kids,

114
00:04:55,613 --> 00:04:57,697
so I was always
looking for attention.

115
00:04:57,698 --> 00:04:58,949
I was a ham, too.

116
00:04:58,950 --> 00:05:01,651
Do you want me to burp
the alphabet for you?

117
00:05:02,718 --> 00:05:05,555
Waiter, we need
a sparkling water, asap.

118
00:05:05,556 --> 00:05:08,508
Oh, hey, Olivia
had her interview today.

119
00:05:08,509 --> 00:05:09,684
Do you know how she did?

120
00:05:09,685 --> 00:05:11,094
She sailed through
the first round.

121
00:05:11,095 --> 00:05:12,103
Flying colors.

122
00:05:12,104 --> 00:05:13,426
First round?

123
00:05:13,427 --> 00:05:14,664
Yeah, these jobs
are so competitive.

124
00:05:14,665 --> 00:05:16,633
There's a whole
series of interviews.

125
00:05:16,634 --> 00:05:18,568
You know, she has to go
through the process.

126
00:05:18,569 --> 00:05:20,987
But, don't worry,
I've got her back.

127
00:05:20,988 --> 00:05:22,864
Ok.

128
00:05:24,949 --> 00:05:27,077
Ahem.

129
00:05:27,078 --> 00:05:28,828
Is that...

130
00:05:28,829 --> 00:05:30,288
What?

131
00:05:30,289 --> 00:05:31,831
Listen.

132
00:05:31,832 --> 00:05:33,541
What is it?

133
00:05:34,652 --> 00:05:36,903
It's butterfly kisses.

134
00:05:36,904 --> 00:05:38,054
Oh.

135
00:05:38,055 --> 00:05:42,409
It's about a father who
puts his daughter to bed

136
00:05:42,410 --> 00:05:45,328
and gives her
butterfly kisses...

137
00:05:45,329 --> 00:05:46,463
With his eyelashes.

138
00:05:48,625 --> 00:05:52,469
And the last verse is
the girl's wedding day

139
00:05:52,470 --> 00:05:57,073
and the mom is putting
baby's breath in her hair.

140
00:05:57,074 --> 00:06:02,662
And her dad is giving
her butterfly kisses.

141
00:06:02,663 --> 00:06:08,001
You know, and he's just like this
big... he's like a big clunky guy.

142
00:06:08,002 --> 00:06:12,055
And he loves his
daughter so much.

143
00:06:12,538 --> 00:06:14,791
You know, it's...

144
00:06:14,792 --> 00:06:15,959
Do you have a daughter?

145
00:06:15,960 --> 00:06:20,088
Oh, no. I just... I love
the idea of it.

146
00:06:21,797 --> 00:06:23,733
[No audio]

147
00:06:43,652 --> 00:06:46,573
I don't understand
what's going on here.

148
00:06:46,574 --> 00:06:48,057
Shh!

149
00:06:48,490 --> 00:06:50,443
Are you kidding me?

150
00:06:51,327 --> 00:06:52,787
Listen to the words.

151
00:06:56,367 --> 00:06:58,535
Oh, God, my phone
is vibrating.

152
00:06:58,536 --> 00:07:01,004
Who knows why
it isn't lighting up?

153
00:07:01,956 --> 00:07:03,173
You know, it's work.

154
00:07:03,174 --> 00:07:05,759
I have to take this. Hello?

155
00:07:05,760 --> 00:07:08,803
Oh, my God!

156
00:07:10,247 --> 00:07:12,265
Well, I'm on
a really fun date,

157
00:07:12,266 --> 00:07:15,418
but I guess
I'll come right in.

158
00:07:15,419 --> 00:07:16,269
What is it?

159
00:07:16,270 --> 00:07:18,221
One of my fellow
cocktail waitresses,

160
00:07:18,222 --> 00:07:21,191
missy, got into
a car accident.

161
00:07:21,565 --> 00:07:22,992
Oh. Is she ok?

162
00:07:23,359 --> 00:07:25,236
No, she died!

163
00:07:27,613 --> 00:07:29,115
I have to go cover for her.

164
00:07:29,116 --> 00:07:32,494
I know that if I died,
she would cover for me.

165
00:07:33,454 --> 00:07:34,320
I mean, she would have.

166
00:07:34,321 --> 00:07:35,989
Obviously, you know,
she can't now but...

167
00:07:35,990 --> 00:07:37,373
Thank you
so much for dinner.

168
00:07:37,374 --> 00:07:38,274
I gotta go.

169
00:07:38,275 --> 00:07:39,159
That's awful.

170
00:07:39,160 --> 00:07:41,327
Let me pay and I'll
drive you over there.

171
00:07:41,669 --> 00:07:42,662
In a car?

172
00:07:42,663 --> 00:07:45,615
Too soon.

173
00:07:49,468 --> 00:07:50,720
Hey, what are
you doing here?

174
00:07:50,721 --> 00:07:52,505
I thought you were on
your date with Tim Cornick.

175
00:07:52,506 --> 00:07:55,341
Girl, you know I would
do anything for you.

176
00:07:55,342 --> 00:07:57,185
I will give you
a bikini wax

177
00:07:57,595 --> 00:07:59,312
when you're
low on funds,

178
00:07:59,764 --> 00:08:01,397
but I will
not date a cry baby.

179
00:08:01,398 --> 00:08:04,150
Did you do
something scary in bed?

180
00:08:05,336 --> 00:08:07,003
No, we were at dinner.

181
00:08:07,004 --> 00:08:08,037
He heard a country song.

182
00:08:08,038 --> 00:08:10,106
Oh, well, then he was
probably just trying

183
00:08:10,107 --> 00:08:12,358
to seem sensitive
to get you into bed.

184
00:08:12,359 --> 00:08:14,344
He didn't know
he could just ask.

185
00:08:14,345 --> 00:08:15,410
Ha ha.

186
00:08:15,411 --> 00:08:17,914
No, he's like a really
sensitive guy.

187
00:08:17,915 --> 00:08:20,817
I think he was
menstruating.

188
00:08:20,818 --> 00:08:22,986
Chels, listen, most
girls would love a guy

189
00:08:22,987 --> 00:08:25,588
who's emotionally
available enough to cry.

190
00:08:25,589 --> 00:08:26,706
You're more like a dude.

191
00:08:26,707 --> 00:08:27,874
Yeah, if that means
I don't lose it

192
00:08:27,875 --> 00:08:30,994
in front of people, then, yeah,
I guess I'm like a dude.

193
00:08:30,995 --> 00:08:33,763
And your deep voice
scares my mother.

194
00:08:34,374 --> 00:08:35,398
Thanks.

195
00:08:35,399 --> 00:08:37,267
Well, it doesn't
matter anyways.

196
00:08:37,268 --> 00:08:39,636
Come on, you've got to keep
dating Tim Cornick.

197
00:08:39,637 --> 00:08:40,436
I need this job.

198
00:08:40,437 --> 00:08:41,888
I need to pay off
my student loans.

199
00:08:41,889 --> 00:08:44,899
I need to not be wearing
footballs on my boobs.

200
00:08:46,400 --> 00:08:48,144
Baseballs are still available.

201
00:08:48,145 --> 00:08:50,363
Hey, Rick, have you
checked your email lately?

202
00:08:50,364 --> 00:08:51,281
No. Why?
What's up?

203
00:08:51,282 --> 00:08:53,783
Jerry wants us to wear
new uniforms, too.

204
00:08:53,784 --> 00:08:55,251
What?
What are they?

205
00:08:55,252 --> 00:08:56,386
I don't know.

206
00:08:56,387 --> 00:08:57,854
You know, I really hope we
don't have to wear suspenders.

207
00:08:57,855 --> 00:09:02,667
It's like wearing a sign that
says, "pick me up, drunk man."

208
00:09:03,761 --> 00:09:05,562
I wonder what
the new uniforms are.

209
00:09:05,563 --> 00:09:09,098
I wonder why Jerry would leave
his BlackBerry on the bar.

210
00:09:09,099 --> 00:09:10,166
You sent the email?

211
00:09:10,167 --> 00:09:13,036
Yeah. Wait till you
see these uniforms.

212
00:09:13,037 --> 00:09:17,140
Unlike your education, some
things are worth the money.

213
00:09:18,008 --> 00:09:19,392
Oh, no, it's Tim Cornick.

214
00:09:19,393 --> 00:09:20,727
He can't see me
like this.

215
00:09:20,728 --> 00:09:24,022
Ok. Go. But for God's sakes,
keep the music upbeat.

216
00:09:25,149 --> 00:09:26,249
Hey, don't ask.

217
00:09:26,250 --> 00:09:30,019
Chelsea, I hope I didn't do
anything to turn you off.

218
00:09:30,020 --> 00:09:31,854
I'm kind of an emotional guy.

219
00:09:31,855 --> 00:09:35,158
You're not like most marketing
executives, are you, Tim?

220
00:09:35,159 --> 00:09:36,960
Well, I'm the creative one.

221
00:09:36,961 --> 00:09:38,661
My partner handles
the business stuff.

222
00:09:38,662 --> 00:09:41,289
You know? I do the heart.
He does the head.

223
00:09:41,290 --> 00:09:42,298
Oh.

224
00:09:42,299 --> 00:09:44,667
What's his number?

225
00:09:44,668 --> 00:09:49,539
Hey, by the way, I am so
sorry about your friend.

226
00:09:49,540 --> 00:09:53,142
But everyone seems to be doing ok.

227
00:09:53,592 --> 00:09:54,878
Oh, missy.

228
00:09:54,879 --> 00:09:56,646
She didn't actually die.

229
00:09:56,647 --> 00:09:58,982
She just lost a foot.

230
00:09:58,983 --> 00:09:59,883
Oh, really?

231
00:09:59,884 --> 00:10:01,476
That's still sad and
everyone seems upbeat.

232
00:10:01,477 --> 00:10:04,687
No, well, people saw her making
fun of other people with one foot,

233
00:10:04,688 --> 00:10:07,148
so everyone thought
she kind of deserved it.

234
00:10:08,158 --> 00:10:10,410
Wow, karma.

235
00:10:10,411 --> 00:10:11,227
Yeah.

236
00:10:11,228 --> 00:10:12,428
You know that song
"karma chameleon"?

237
00:10:12,429 --> 00:10:14,781
By heart, don't need a reminder.

238
00:10:15,833 --> 00:10:17,250
Ok.

239
00:10:17,251 --> 00:10:20,453
But when do I get to
take you out again?

240
00:10:21,954 --> 00:10:22,956
Please?

241
00:10:22,957 --> 00:10:24,407
Tomorrow night.

242
00:10:24,832 --> 00:10:25,675
Great.

243
00:10:25,676 --> 00:10:29,420
How about one butterfly
kiss for the road?

244
00:10:31,348 --> 00:10:33,116
Ok.

245
00:10:43,025 --> 00:10:45,191
Chelsea: While Olivia's
interview process was grueling.

246
00:10:45,192 --> 00:10:46,968
She was trying her
best to stand out.

247
00:10:46,969 --> 00:10:48,106
[Grunting]

248
00:10:49,231 --> 00:10:51,189
- Aah!
- Ha ha ha!

249
00:10:51,650 --> 00:10:54,153
And I tried my hardest
not to put out.

250
00:10:56,947 --> 00:10:58,646
Since I was forced to be with him,

251
00:10:58,647 --> 00:11:00,565
I actually started
to feel for the guy.

252
00:11:00,993 --> 00:11:03,201
His raw emotion made me feel
protective toward him,

253
00:11:03,620 --> 00:11:05,937
as Dee Dee was toward the
now half-eaten Lancelot.

254
00:11:05,938 --> 00:11:11,409
Or maybe it was just the 6 shots
of vodka before the main course.

255
00:11:14,329 --> 00:11:16,514
Hit me again.

256
00:11:18,350 --> 00:11:22,870
Chelsea, I feel like we're
becoming best friends,

257
00:11:22,871 --> 00:11:24,517
but you're not that into me.

258
00:11:24,518 --> 00:11:27,525
Oh, believe me, Tim,
we are not best friends.

259
00:11:27,978 --> 00:11:31,829
But we're not lovers either
and I don't blame you.

260
00:11:31,830 --> 00:11:35,199
Nobody wants to make love with me.

261
00:11:35,200 --> 00:11:36,571
Mmm.

262
00:11:36,968 --> 00:11:39,036
I have sleep apnea.

263
00:11:39,037 --> 00:11:41,826
You know?
I'm allergic to everything.

264
00:11:41,827 --> 00:11:45,376
The last woman I was with,
right in the middle of our intimacy

265
00:11:45,377 --> 00:11:47,123
she said, "this is a mistake."

266
00:11:48,874 --> 00:11:51,712
"You can finish,
but this is a mistake."

267
00:11:51,713 --> 00:11:53,130
No.

268
00:11:55,917 --> 00:11:59,010
So I finished, you know.

269
00:11:59,011 --> 00:12:00,922
I haven't been with anybody

270
00:12:00,923 --> 00:12:04,849
in 18 months and I am so lonely.

271
00:12:04,850 --> 00:12:08,296
Tim, you are such a great guy.

272
00:12:08,852 --> 00:12:12,523
You know, any woman would
be lucky to be with you.

273
00:12:12,524 --> 00:12:14,285
You don't want to be with me.

274
00:12:14,286 --> 00:12:16,361
Of course I do.

275
00:12:16,362 --> 00:12:18,206
You do?
Yeah.

276
00:12:18,207 --> 00:12:21,542
Oh, Chelsea, you have no
idea what that means to me.

277
00:12:21,824 --> 00:12:24,612
Yeah, but... but...
But not tonight.

278
00:12:25,452 --> 00:12:28,382
Because I have
had so much to drink,

279
00:12:28,383 --> 00:12:33,954
I'm afraid that I won't
be able to get it open.

280
00:12:39,311 --> 00:12:44,181
I love that you
are sharing with me.

281
00:12:44,888 --> 00:12:47,934
Hey, can I get another shot?

282
00:12:50,606 --> 00:12:52,073
I don't know what to do, Dee Dee.

283
00:12:52,074 --> 00:12:55,876
If I were you, I'd get a job
where you wear a shirt.

284
00:12:56,645 --> 00:12:58,646
No, I'm talking about Tim Cornick.

285
00:12:59,027 --> 00:13:00,780
We're both having guy problems.

286
00:13:00,781 --> 00:13:01,988
What?

287
00:13:01,989 --> 00:13:04,818
Mr. Kitty.
He's a real handful.

288
00:13:04,819 --> 00:13:06,787
But we love them, right?

289
00:13:07,786 --> 00:13:09,622
Chelsea, I've got good news!

290
00:13:09,623 --> 00:13:12,093
- You got the job?
- Yes!

291
00:13:12,094 --> 00:13:14,762
I still have to pick up
shifts at the bar,

292
00:13:14,763 --> 00:13:15,880
but I get to work sitting down

293
00:13:16,378 --> 00:13:18,297
and you get to break up
with Tim Cornick.

294
00:13:18,298 --> 00:13:19,934
No, I can't.

295
00:13:19,935 --> 00:13:20,935
He was so sad last night

296
00:13:20,936 --> 00:13:23,052
that I got drunk and
promised to sleep with him.

297
00:13:23,053 --> 00:13:25,206
- What?
- Yeah.

298
00:13:25,207 --> 00:13:26,207
And if I turn him down now,

299
00:13:26,208 --> 00:13:27,441
it'll totally break his heart.

300
00:13:27,442 --> 00:13:30,311
But if you make love with him,
he's going to want to marry you.

301
00:13:30,642 --> 00:13:32,546
Well, if I do have sex with him,

302
00:13:32,547 --> 00:13:36,016
at least we can use
his tears as lubricant.

303
00:13:43,906 --> 00:13:46,200
This is unacceptable.

304
00:13:46,201 --> 00:13:47,361
So it objectifies you.

305
00:13:47,362 --> 00:13:49,764
This is our new business plan.

306
00:13:53,957 --> 00:13:56,937
This is why I quit footlocker.

307
00:13:59,421 --> 00:14:02,510
Wow. Todd is really packing.

308
00:14:02,511 --> 00:14:03,311
Yeah.

309
00:14:03,312 --> 00:14:04,595
Yeah, the gods of proportion

310
00:14:04,596 --> 00:14:06,814
have such mixed feelings about him.

311
00:14:07,304 --> 00:14:08,882
All right, you know
what? That's it.

312
00:14:08,883 --> 00:14:11,185
I'm calling Jerry and I
am telling him we're not

313
00:14:11,186 --> 00:14:12,977
going to wear these
uniforms anymore.

314
00:14:12,978 --> 00:14:13,988
Yeah, neither are we.

315
00:14:13,989 --> 00:14:15,222
Yeah, I mean, what's
he going to do?

316
00:14:15,223 --> 00:14:16,090
Is he going to fire all of us?

317
00:14:16,091 --> 00:14:17,491
No. This is demeaning, it's wrong,

318
00:14:17,492 --> 00:14:19,160
and we're not going
to do it anymore.

319
00:14:19,161 --> 00:14:22,236
You go, girl.
Thanks.

320
00:14:22,531 --> 00:14:24,432
Jerry!

321
00:14:24,433 --> 00:14:25,615
It's Rick.

322
00:14:25,616 --> 00:14:26,917
Yeah, well, I'm just standing here

323
00:14:26,918 --> 00:14:31,205
in the tightest little outfit
and I thought I'd give you a call.

324
00:14:32,704 --> 00:14:33,841
Yeah, never mind.

325
00:14:33,842 --> 00:14:36,010
You did this?

326
00:14:37,346 --> 00:14:40,614
Why should I be having all the fun
in the totally degrading uniform?

327
00:14:41,046 --> 00:14:43,017
At least yours is sexy, all right?

328
00:14:43,018 --> 00:14:44,919
There's no girl in her
right mind that will

329
00:14:44,920 --> 00:14:48,522
want to sleep with me
looking like this.

330
00:14:49,458 --> 00:14:50,725
Because you look disgusting.

331
00:14:50,726 --> 00:14:55,196
And people don't want to sleep
with people who look disgusting.

332
00:14:55,197 --> 00:14:58,165
I never used the word
disgusting, ok?

333
00:14:58,166 --> 00:15:02,236
And, hey, for the record,
it's a little cold in here.

334
00:15:02,237 --> 00:15:04,570
[Clears throat]

335
00:15:04,571 --> 00:15:07,141
Not over here.

336
00:15:10,078 --> 00:15:12,630
Chelsea: That night to protect
Tim Cornick's tender heart,

337
00:15:12,631 --> 00:15:15,249
we worked overtime
to make me so repulsive

338
00:15:15,250 --> 00:15:17,518
that he would rather
go home and have sex with

339
00:15:17,519 --> 00:15:19,085
himself than have sex with me.

340
00:15:19,354 --> 00:15:22,713
Ooh, your grandmother
had some ugly stuff.

341
00:15:22,714 --> 00:15:26,560
Ooh, what's this?

342
00:15:26,883 --> 00:15:30,147
That is an actual douche bag.

343
00:15:32,200 --> 00:15:33,100
Neat.

344
00:15:33,101 --> 00:15:35,236
Hey, Rick's birthday is coming up.

345
00:15:35,237 --> 00:15:37,822
We can use this to ice his cake.

346
00:15:38,979 --> 00:15:40,356
Have you guys seen a-face?

347
00:15:40,357 --> 00:15:41,649
No, why?

348
00:15:41,650 --> 00:15:43,244
Lancelot passed away.

349
00:15:43,525 --> 00:15:45,696
Oh, Dee, I'm sorry.

350
00:15:45,697 --> 00:15:46,881
It's for the best.

351
00:15:46,882 --> 00:15:47,848
The vet said he could save him,

352
00:15:47,849 --> 00:15:51,519
but in the end he'd be
more machine than bird.

353
00:15:52,421 --> 00:15:54,912
Do you think I should
let a-face eat him?

354
00:15:54,913 --> 00:15:55,923
What?

355
00:15:55,924 --> 00:15:56,757
Well, he's already dead

356
00:15:56,758 --> 00:15:58,225
and a-face wanted him really bad.

357
00:15:58,749 --> 00:16:00,628
I think that's a great idea.

358
00:16:00,629 --> 00:16:01,495
Circle of life, Dee Dee.

359
00:16:01,496 --> 00:16:05,466
Super. I'll get him
out of the freezer.

360
00:16:05,467 --> 00:16:07,268
Whoa, check it out,
there's a wig in here.

361
00:16:07,269 --> 00:16:09,637
He's already seen
your real hair, Chelsea.

362
00:16:10,177 --> 00:16:13,040
Oh, girl, this is not for my head.

363
00:16:19,714 --> 00:16:23,183
Welcome to where I have sex.

364
00:16:24,858 --> 00:16:26,819
You're going to like it.

365
00:16:26,820 --> 00:16:30,324
A lot of guys have.

366
00:16:31,490 --> 00:16:33,060
Are you crying?

367
00:16:33,575 --> 00:16:35,620
Oh, no. My eyes are just itchy.

368
00:16:35,621 --> 00:16:37,622
Tim...

369
00:16:37,623 --> 00:16:41,034
Before we make love...

370
00:16:41,035 --> 00:16:45,206
There's a little shameful secret
that I need to share with you.

371
00:16:45,504 --> 00:16:47,441
If you want to spank me,

372
00:16:47,442 --> 00:16:50,110
I will pull my
pants down right now.

373
00:16:50,509 --> 00:16:54,548
No. Just keep your pants on.

374
00:16:55,430 --> 00:17:00,621
I have spider veins...
Hundreds of them.

375
00:17:01,144 --> 00:17:02,606
Look.

376
00:17:03,275 --> 00:17:07,611
I'm so happy you have a flaw.

377
00:17:08,819 --> 00:17:11,280
You're actually so intimidating.

378
00:17:11,281 --> 00:17:12,531
Let me caress them.

379
00:17:12,532 --> 00:17:15,002
Don't do that.

380
00:17:15,804 --> 00:17:17,371
I guess I should
take my clothes off.

381
00:17:17,372 --> 00:17:19,740
Oh, wait, wait, wait, don't rush.

382
00:17:20,288 --> 00:17:22,309
I want to enjoy every minute.

383
00:17:22,310 --> 00:17:25,753
Let's just snuggle, you
know, and see what pops up.

384
00:17:25,754 --> 00:17:26,797
Oh.

385
00:17:26,798 --> 00:17:29,133
[Moaning]

386
00:17:29,134 --> 00:17:31,552
No, it's just so hot, I just...

387
00:17:32,134 --> 00:17:33,678
Let me take my clothes off first.

388
00:17:33,679 --> 00:17:35,723
Are you ready?

389
00:17:35,724 --> 00:17:37,473
I'm ready.

390
00:17:49,609 --> 00:17:50,804
Ohh.

391
00:17:51,111 --> 00:17:55,042
You are exactly what I pictured.

392
00:17:55,657 --> 00:17:57,928
This is who you pictured?

393
00:17:58,201 --> 00:18:02,415
You are the most earthy, natural,

394
00:18:02,416 --> 00:18:05,435
beautiful woman that
I have ever seen.

395
00:18:05,436 --> 00:18:08,831
And that is coming from
a huge Janice Joplin fan.

396
00:18:08,832 --> 00:18:09,922
So...

397
00:18:10,424 --> 00:18:13,260
Chelsea: No matter what I did,
Tim accepted me for who I was.

398
00:18:13,261 --> 00:18:15,428
So I came up with
a lie that would remove me

399
00:18:15,429 --> 00:18:18,305
from the situation
with grace and dignity.

400
00:18:18,306 --> 00:18:21,267
Tim, I have herpes.

401
00:18:21,636 --> 00:18:23,060
So do I!

402
00:18:23,061 --> 00:18:25,372
Oh, I'm so relieved.

403
00:18:25,373 --> 00:18:26,506
I didn't know how to tell you.

404
00:18:26,507 --> 00:18:28,858
But now I don't even have to
worry if I get an outbreak!

405
00:18:29,160 --> 00:18:30,244
Ha ha ha!

406
00:18:30,245 --> 00:18:31,378
Chelsea: Holy crap!

407
00:18:31,379 --> 00:18:33,297
After years of dodging
the herpes bullet,

408
00:18:33,298 --> 00:18:35,165
I was not going to
go down like this.

409
00:18:35,697 --> 00:18:38,200
But then help came from
an unexpected place.

410
00:18:38,469 --> 00:18:40,369
[Meowing]

411
00:18:40,370 --> 00:18:43,247
- You have a cat?
- Yeah.

412
00:18:43,248 --> 00:18:46,333
No wonder my eyes
have been watering!

413
00:18:46,334 --> 00:18:48,645
Ah-choo! Ohh.

414
00:18:48,646 --> 00:18:50,463
I'm deathly allergic.

415
00:18:50,464 --> 00:18:51,732
Really?
Yeah.

416
00:18:52,130 --> 00:18:54,884
Are you really attached
to that cat?

417
00:18:54,885 --> 00:18:57,337
So attached.

418
00:18:57,677 --> 00:19:00,374
It was my grandmother's cat.

419
00:19:00,375 --> 00:19:01,308
And when she died,

420
00:19:01,309 --> 00:19:02,242
on her deathbed she said,

421
00:19:02,243 --> 00:19:04,810
"Chelsea, I am putting
my soul into this cat.

422
00:19:04,811 --> 00:19:05,946
"And then when he dies,

423
00:19:05,947 --> 00:19:07,938
"you have to put his
soul into another cat.

424
00:19:07,939 --> 00:19:12,735
In short you must always
have a cat."

425
00:19:12,736 --> 00:19:14,862
What are we going to do?

426
00:19:14,863 --> 00:19:16,256
I don't know.

427
00:19:16,529 --> 00:19:18,792
We could never get married.

428
00:19:18,793 --> 00:19:20,367
We could never live together.

429
00:19:20,368 --> 00:19:26,099
Ohh. This is so tragic.

430
00:19:26,734 --> 00:19:29,603
I guess this is good-bye.

431
00:19:31,806 --> 00:19:36,143
Can I have one
butterfly kiss for the road?

432
00:19:43,618 --> 00:19:46,153
Good-bye, Chelsea.

433
00:19:46,688 --> 00:19:48,288
Good-bye, Tim Cornick.

434
00:19:48,289 --> 00:19:50,457
Don't call.
Don't come to the bar.

435
00:19:50,458 --> 00:19:54,044
Just let me heal
for the rest of my life.

436
00:19:54,484 --> 00:19:57,664
Maybe one day
medical science will...

437
00:20:08,381 --> 00:20:09,497
Hey, Chels. ♪

438
00:20:09,498 --> 00:20:11,166
Hey, man.

439
00:20:11,167 --> 00:20:14,335
So are you glad to be back
in your old uniform?

440
00:20:14,336 --> 00:20:15,840
Yeah, thanks for talking to Jerry.

441
00:20:15,841 --> 00:20:17,055
Hey, you're welcome.

442
00:20:17,056 --> 00:20:18,506
You know, if you would
have listened to me

443
00:20:18,507 --> 00:20:19,891
in the first place,
I wouldn't have had

444
00:20:19,892 --> 00:20:21,387
to humiliate you to make my point.

445
00:20:21,388 --> 00:20:24,348
Yeah, that was a good one.

446
00:20:24,697 --> 00:20:26,684
Ha ha. Thank you.

447
00:20:26,685 --> 00:20:28,433
Hey, you know what else is good?

448
00:20:28,434 --> 00:20:31,355
This lock I bought
for the stereo cabinet.

449
00:20:31,356 --> 00:20:33,271
Why would I care about that?

450
00:20:33,272 --> 00:20:34,773
Well...

451
00:20:34,774 --> 00:20:36,735
[Country music playing]

452
00:20:42,032 --> 00:20:45,166
Man, I hope that's
not stuck on repeat.

453
00:20:45,484 --> 00:20:48,038
Hey, have a good shift, Chels.

454
00:20:48,039 --> 00:20:50,688
This won't be good
for business.

455
00:20:51,190 --> 00:20:54,879
They just don't write
them like this anymore.

456
00:20:57,797 --> 00:21:00,634
<font color="#00ffff">Sync & corrections by Rafael UPD</font>
<font color="#00ffff">www.Addic7ed.Com/</font>

