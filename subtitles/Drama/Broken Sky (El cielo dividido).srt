1
00:00:44,575 -> 00:00:50,865
<i> There will come a time when there
know what to call what unites us. </ i>

2
00:00:50,866 -> 00:00:56,686
Your name will <i> fading little by little
our memory. </ i>

3
00:00:56,687 -> 00:00:59,687
<i> And soon disappear completely. </ i>

4
00:00:59,688 -> 00:01:01,180
<i> Margaritte Duras
"Hiroshima mon amour." </ I>

5
00:01:15,878 -> 00:01:16,926
<i> I remember you </ i>

6
00:01:22,300 -> 00:01:23,167
<i> morning ... </ i>

7
00:01:25,150 -> 00:01:26,084
<i> ... you. </ i>

8
00:01:28,356 -> 00:01:29,325
<i> the afternoon ... </ i>

9
00:01:31,489 -> 00:01:32,368
<i> and night ... </ i>

10
00:01:38,161 -> 00:01:39,069
<i> you ... always. </ i>

11
00:08:21,969 -> 00:08:27,249
Aristophanes said that true love
only found among similar.

12
00:08:21,969 -> 00:08:31,422
It is used to explain the mythology.
In the beginning were men

13
00:08:31,423 -> 00:08:36,317
and women
and something called "androgens".

14
00:08:36,318 -> 00:08:40,443
These androgens were two men in one,
or two women or a man ...

15
00:08:40,442 -> 00:08:47,597
... And a woman. At one point found themselves
very strong and decided to defy the gods.

16
00:08:47,598 -> 00:08:53,606
But did not have
aust�cia to Zeus, who ordered Apollo to ...

17
00:08:53,607 -> 00:09:00,089
undo these "wounds". Apollo decides to share them
the chest and belly and just leaves a hole

18
00:09:00,090 -> 00:09:04,492
which is the navel, as a souvenir
this recklessness.

19
00:09:04,493 -> 00:09:10,749
Therefore, Aristophanes says that man
will always seek this

20
00:09:10,750 -> 00:09:15,207
other half of which was partitioned
at first, and when ...

21
00:09:15,208 -> 00:09:21,585
This is ... the other half is then
true love and true happiness.

22
00:10:29,300 -> 00:10:30,010
<i> Jonas </ i>

23
00:15:15,700 -> 00:15:16,556
Come on!

24
00:24:30,300 -> 00:24:31,447
Can you imagine?

25
00:24:38,181 -> 00:24:39,327
I do not know ...

26
00:24:44,300 -> 00:24:45,175
I'm afraid

27
00:28:15,670 -> 00:28:18,284
<i> They have the opportunity to live a
unfailing love </ i>

28
00:28:19,746 -> 00:28:21,935
<i> and will perhaps have to die
one day for him. </ i>

29
00:30:39,090 -> 00:30:40,852
- Hi
- A beer

30
00:33:31,850 -> 00:33:33,494
<i> Jonas can not stop crying ... </ i>

31
00:33:34,487 -> 00:33:38,166
It <i> much time with her eyes
spot where he disappeared ... </ i>

32
00:33:39,750 -> 00:33:40,466
<i> cry ... </ i>

33
00:33:40,480 -> 00:33:42,872
<i> Want to see it again
So cry ... </ i>

34
00:33:43,510 -> 00:33:46,624
<i> not know, but is right
that he was the one who has always hoped. </ i>

35
00:33:47,514 -> 00:33:51,111
<i> saw by chance,
and now wants to see him again, whatever the cost. </ i>

36
00:33:52,137 -> 00:33:54,700
<i> Gerardo, do not understand.
He does not understand. </ I>

37
00:33:55,235 -> 00:33:56,986
<i> remains oblivious
to this story. </ i>

38
00:33:58,970 -> 00:34:05,566
<i> "Heaven divided" </ i>

39
00:34:06,392 -> 00:34:09,538
Subtitles by:
ZHEN Kirel

40
00:42:03,697 -> 00:42:08,269
<i> Their bodies are in the same place
Enclosed within itself out. </ I>

41
00:42:08,270 -> 00:42:10,866
<i> Sleep always comes first to Jonas. </ i>

42
00:42:11,673 -> 00:42:14,971
<i> Sleep well at night because it leads
to forget the life he leads with Gerardo ... </ i>

43
00:42:14,972 -> 00:42:16,982
<i> ... and who wants to leave. </ i>

44
01:03:55,280 -> 01:03:58,432
<i> after that was waking
Gerardo died when Jonas </ i>

45
01:03:58,435 -> 01:04:01,489
<i> lost his eyes, his voice ... </ i>

46
01:03:59,790 -> 01:04:06,587
<i> closed his eyes against his mouth,
soon his lips against her ... </ i>

47
01:04:06,912 -> 01:04:11,092
<i> your hands too ...
but, above all, his eyes closed. </ i>

48
01:11:09,200 -> 01:11:12,196
<i> Other people say they are
hiding to penetrate up </ i>

49
01:11:12,197 -> 01:11:14,750
<i> come together and they
without knowing it or love it. </ i>

50
01:11:16,017 -> 01:11:16,948
<i> ... almost without seeing it. </ i>

51
01:16:19,125 -> 01:16:25,191
These tears <i> hurt the body of Gerardo
as heavenly sword, </ i>

52
01:16:25,226 -> 01:16:28,171
<i> cross your heart while
no trace, nothing. </ i>

53
01:16:29,325 -> 01:16:30,525
<i> only one certainty, </ i>

54
01:16:31,032 -> 01:16:32,905
A scar that burns <i>
as an open wound: </ i>

55
01:16:34,214 -> 01:16:36,405
<i> had ceased to exist
in the life of Jonah. </ i>

56
01:16:37,909 -> 01:16:39,252
<i> no longer exist ever. </ i>

57
01:20:24,748 -> 01:20:26,465
Gerardo <i> turned to Sergio </ i>

58
01:20:27,939 -> 01:20:31,323
<i> She looked at him with an attention so intense
and deeper than the saw. </ i>

59
01:20:32,499 -> 01:20:35,261
<i> Soon the idea of ??its existence
came upon his spirit. </ i>

60
01:20:36,345 -> 01:20:37,618
She looked at him <i>
how to love him. </ i>

61
01:20:39,300 -> 01:20:40,298
<i> had started
it happen. </ i>

62
01:37:28,021 -> 01:37:28,790
Come

63
01:37:29,553 -> 01:37:31,062
No, really, I can not.

64
01:37:31,063 -> 01:37:32,184
Come!

65
01:40:48,200 -> 01:40:50,526
Have you loved long ago.

66
01:47:14,343 -> 01:47:17,975
Until then <i> Entera Jonas
the existence of this other feeling </ i>

67
01:47:18,931 -> 01:47:21,373
<i> It was a long time
until he came to his conscience </ i>

68
01:47:22,315 -> 01:47:24,395
Suddenly everything changes <i>
around them. </ i>

69
01:47:25,060 -> 01:47:26,231
Jonas wonders <i>
 why? </ i>

70
01:47:27,414 -> 01:47:30,026
<i> can not imagine another love
what Gerardo lived through it. </ i>

71
01:47:30,780 -> 01:47:33,260
<i> can not support it or understand it </ i>

72
02:04:11,290 -> 02:04:15,264
<i> now only remains for them the possibility
to live with the memory of a kiss, </ i>

73
02:04:16,052 -> 02:04:17,000
<i> a word, </ i>

74
02:04:17,800 -> 02:04:18,939
<i> a hug </ i>

75
02:04:19,983 -> 02:04:22,685
<i> a unique look
a love for all. </ i>

76
02:07:39,770 -> 02:07:41,221
Changes that face

77
02:07:42,599 -> 02:07:44,599
You will see that
nothing happened.

78
02:07:47,900 -> 02:07:49,051
That I do not know.

79
02:08:27,025 -> 02:08:28,414
You think so?

80
02:08:29,530 -> 02:08:30,320
Of course.

81
02:08:38,140 -> 02:08:40,089
Stop this bullshit!

82
02:09:16,900 -> 02:09:18,062
Sergio!

83
02:10:34,440 -> 02:10:36,480
Sergio <i> embraced
against his chest. </ i>

84
02:10:37,245 -> 02:10:39,176
<i> told him I loved him
more than anything in the world. </ i>

85
02:10:41,000 -> 02:10:42,717
Gerardo turned <i>
him </ i>

86
02:10:43,440 -> 02:10:45,121
<i> saw him first. </ i>

87
02:10:46,240 -> 02:10:47,250
<i> said he believed it. </ i>

88
02:10:47,251 -> 02:10:48,251
I also <i> </ i>

89
02:11:55,950 -> 02:11:57,755
Long after Jonah <i>
called him. </ i>

90
02:11:58,549 -> 02:11:59,564
<i> "I am",
I told him. </ i>

91
02:12:04,100 -> 02:12:05,170
<i> Gerardo recognized his voice. </ i>

92
02:12:06,900 -> 02:12:07,900
<i> Jonas said: </ i>

93
02:12:08,300 -> 02:12:10,114
<i> "I just wanted to hear your voice" </ i>

94
02:12:10,115 -> 02:12:11,637
<i> I am. </ i>

95
02:12:13,100 -> 02:12:14,708
They were silent <i> </ i>

96
02:12:17,000 -> 02:12:20,751
<i> Jonas said that every day, in some
time without wanting </ i>

97
02:12:20,752 -> 02:12:22,950
<i> your image returned
to his memory. </ i>

98
02:12:25,000 -> 02:12:26,025
<i> His voice trembled. </ i>

99
02:12:28,453 -> 02:12:30,451
<i> recalled that Gerardo
voice and felt sorry. </ i>

100
02:12:33,100 -> 02:12:34,824
After <i> no longer knew
what to say. </ i>

101
02:12:37,400 -> 02:12:39,093
<i> And then Jonas told him </ i>

102
02:12:42,000 -> 02:12:43,251
<i> he was sorry. </ i>

103
02:12:43,900 -> 02:12:44,530
<i> still loved him. </ i>

104
02:12:45,100 -> 02:12:47,008
<i> could never
stop loving him. </ i>

105
02:12:47,850 -> 02:12:48,850
<i> that love to death </ i>

106
02:12:54,900 -> 02:12:57,485
<i> Jonas then heard crying
Gerardo on the phone. </ i>

107
02:12:59,800 -> 02:13:00,856
<i> wept. </ i>

108
02:13:04,750 -> 02:13:06,007
<i> Gerardo was in bed </ i>

109
02:13:07,300 -> 02:13:08,330
<i> continued to cry </ i>

110
02:13:11,600 -> 02:13:14,203
<i> Recalling him as the
oblivion of love itself. </ i>

111
02:13:15,400 -> 02:13:17,204
<i> ... as the terror of oblivion. </ i>

112
02:14:19,070 -> 02:14:22,070
<i> Here is the result of all this
time that I meant </ i>

113
02:14:22,071 -> 02:14:24,200
<i> I loved you,
it screams. </ i>

114
02:14:24,250 -> 02:14:25,200
<i> That's all. </ i>

115
02:14:26,250 -> 02:14:27,075
<i> December 2005. </ i>

116
02:14:37,664 -> 02:14:40,664
Subtitles by:
ZHEN Kirel