﻿1
00:00:10,000 --> 00:00:20,000
FEMALE AGENTS (2008) CD 2/1

2
00:00:51,660 --> 00:00:56,540
APRIL 1944
BOURG-EN-BRESSE STATION, FRANCE

3
00:02:16,740 --> 00:02:18,780
Search the warehouses!

4
00:02:18,900 --> 00:02:20,740
Spotlight!

5
00:03:13,860 --> 00:03:14,900
Bring him here!

6
00:03:19,620 --> 00:03:21,740
I'll deal with him.

7
00:03:43,380 --> 00:03:46,380
A sniper! That window up there!

8
00:05:25,860 --> 00:05:31,860
FEMALE AGENTS

9
00:05:35,420 --> 00:05:40,500
In 1940, Winston Churchill
created a new kind of secret service,

10
00:05:40,620 --> 00:05:44,540
the Special Operations Executive,
better known as the SOE.

11
00:05:44,660 --> 00:05:49,940
One section was responsible
for overseeing operations in France.

12
00:05:50,060 --> 00:05:53,260
In 1941, a new head was appointed,
Colonel Maurice Buckmaster.

13
00:05:53,380 --> 00:05:55,740
In 1944, the SOE's "French Section"

14
00:05:55,860 --> 00:05:58,140
was dedicated, at a heavy human cost,

15
00:05:58,260 --> 00:06:01,100
to ensuring the success of D-Day.

16
00:06:06,700 --> 00:06:09,940
SATURDAY 27th MAY, 1944
TEMPSFORD AIRFIELD, ENGLAND

17
00:06:45,540 --> 00:06:47,340
I heard about your husband.

18
00:06:48,580 --> 00:06:49,740
I'm sorry.

19
00:06:51,980 --> 00:06:55,500
Claude was shot 40 days ago.
I intend to fight on.

20
00:06:55,620 --> 00:06:57,700
With or without your blessing.

21
00:07:00,900 --> 00:07:03,060
How did you find me, Louise?

22
00:07:03,180 --> 00:07:07,860
A liaison officer told me he'd trained
with a Desfontaines in London.

23
00:07:07,980 --> 00:07:09,500
I thought nothing of it.

24
00:07:09,620 --> 00:07:11,460
I was sure you'd given up.

25
00:07:12,980 --> 00:07:14,700
That I was like Father...

26
00:07:15,780 --> 00:07:18,500
saying, "The war's over, let's just capitulate. "

27
00:07:18,620 --> 00:07:21,820
And blindly follow Pétain? Yes.

28
00:07:21,940 --> 00:07:23,580
I was sure of it.

29
00:07:23,700 --> 00:07:25,220
That you'd bailed.

30
00:07:26,220 --> 00:07:29,340
Actually, by going to London,

31
00:07:29,460 --> 00:07:31,820
you found the best way to fight on

32
00:07:31,940 --> 00:07:34,660
and get away from Claude and I.

33
00:07:34,780 --> 00:07:37,140
I never got what you saw in him.

34
00:07:37,260 --> 00:07:39,940
Not to mention he was a communist.

35
00:07:53,740 --> 00:07:57,100
SOE HQ, LONDON
COLONEL BUCKMASTER'S SECTION

36
00:07:59,540 --> 00:08:03,060
How long did you work as a nurse
in Bourg-en-Bresse?

37
00:08:03,180 --> 00:08:05,100
I never really stopped.

38
00:08:05,220 --> 00:08:07,660
Would you care to go back to it?

39
00:08:08,980 --> 00:08:11,140
I'm not here to heal casualties.

40
00:08:11,260 --> 00:08:15,140
You'd be evacuating one.
From a German hospital in Normandy.

41
00:08:15,260 --> 00:08:16,940
A German casualty?

42
00:08:17,060 --> 00:08:19,820
A Brit disguised as one, actually.

43
00:08:20,580 --> 00:08:22,900
What is this madness?

44
00:08:23,020 --> 00:08:27,220
Probably what the poor fellow said
when he saw what had happened.

45
00:08:27,340 --> 00:08:29,420
The "poor fellow" is a geologist.

46
00:08:29,540 --> 00:08:30,980
On a mission for us,

47
00:08:31,100 --> 00:08:33,620
he was spotted by a German in Normandy.

48
00:08:33,740 --> 00:08:37,100
Our man shot him and stole his uniform.

49
00:08:37,220 --> 00:08:40,140
- That's pretty cheeky.
- Stupid, you mean.

50
00:08:40,260 --> 00:08:43,500
He was in Lisieux when
the Royal Air Force bombed it.

51
00:08:44,500 --> 00:08:46,900
So he became a German casualty.

52
00:08:47,020 --> 00:08:50,420
And was sent to a Wehrmacht hospital
in Pont-I'Evêque.

53
00:08:51,980 --> 00:08:53,740
Why was he in Normandy?

54
00:08:53,860 --> 00:08:56,620
That's just what the Nazis
must never find out.

55
00:08:56,740 --> 00:09:00,140
You think I can get him out all on my own,

56
00:09:00,260 --> 00:09:01,660
dressed as a nurse?

57
00:09:01,780 --> 00:09:03,380
Not all on your own.

58
00:09:03,500 --> 00:09:05,060
There'll be five of you.

59
00:09:05,180 --> 00:09:08,220
Five women under Pierre's command.

60
00:09:08,340 --> 00:09:11,260
<i>Your brother devised the whole mission.</i>

61
00:09:11,380 --> 00:09:13,580
<i>We already have one agent in place.</i>

62
00:09:13,700 --> 00:09:16,900
<i>An Italian from Milan's Jewish aristocracy.</i>

63
00:09:17,020 --> 00:09:18,100
How are you?

64
00:09:18,220 --> 00:09:21,380
<i>Her codename is Maria.
She'll liaise with you.</i>

65
00:09:21,500 --> 00:09:24,460
<i>She's already made contact
with the geologist.</i>

66
00:09:24,860 --> 00:09:28,460
To complete the unit,
here are some potential recruits.

67
00:09:29,020 --> 00:09:31,540
Some have already done
a commando course.

68
00:09:33,100 --> 00:09:35,300
How long do we have to train?

69
00:09:35,420 --> 00:09:39,100
A one-day refresher course,
then you fly straight out.

70
00:09:39,220 --> 00:09:41,380
That's insane!

71
00:09:41,500 --> 00:09:43,820
Have you heard of Colonel Heindrich?

72
00:09:44,620 --> 00:09:45,500
No.

73
00:09:45,620 --> 00:09:48,140
He was with the Abwehr,
counter-espionage,

74
00:09:48,260 --> 00:09:51,940
and he just joined the SS's intelligence unit.

75
00:09:52,060 --> 00:09:54,260
He must be on the geologist's tail.

76
00:10:15,180 --> 00:10:18,460
These were taken by one of our spies
on the English coast.

77
00:10:24,860 --> 00:10:28,460
What are those strange objects
in the harbour, Colonel?

78
00:10:28,580 --> 00:10:31,380
They look like floating grain silos.

79
00:10:31,500 --> 00:10:34,260
Grain silos protected by ack-ack guns?

80
00:10:34,380 --> 00:10:40,140
I believe these blocks play a role
in landing the enemy's invasion forces.

81
00:10:40,260 --> 00:10:41,700
Colonel Heindrich,

82
00:10:41,820 --> 00:10:46,660
why have we concentrated the majority
of our units around Calais?

83
00:10:46,780 --> 00:10:52,380
The Allied fleet can unload troops
and equipment faster in an existing port.

84
00:10:52,500 --> 00:10:54,260
And don't forget,

85
00:10:54,380 --> 00:10:58,220
it's the shortest route
between the English and French coasts.

86
00:10:58,340 --> 00:11:01,060
I'm sorry to insist, Field Marshal,

87
00:11:01,180 --> 00:11:03,660
but what if it's all a diversion?

88
00:11:04,540 --> 00:11:07,220
What if the landings
took place somewhere else?

89
00:11:07,340 --> 00:11:10,060
Such as, Colonel?

90
00:11:11,820 --> 00:11:14,460
Across from Southampton, in Normandy,

91
00:11:15,180 --> 00:11:16,820
on one of the beaches.

92
00:11:17,860 --> 00:11:20,740
Where we spotted an English geologist.

93
00:11:22,540 --> 00:11:27,540
Compass, readings, samples...
In his panic, he left all this behind.

94
00:11:27,660 --> 00:11:30,900
I'm sure he hasn't made it back
to England yet.

95
00:11:32,540 --> 00:11:35,580
The thing about you SS people

96
00:11:35,700 --> 00:11:40,740
that most amuses me
is that you never have any doubts.

97
00:11:42,380 --> 00:11:45,420
Colonel, do you seriously believe

98
00:11:45,540 --> 00:11:51,380
I will move all my Panzer divisions
because of a phantom geologist?

99
00:12:00,140 --> 00:12:02,100
Colonel, if I may say so,

100
00:12:02,220 --> 00:12:04,420
you see the world with different eyes.

101
00:12:04,540 --> 00:12:05,940
The Field Marshal's Prussian.

102
00:12:06,060 --> 00:12:08,300
As far as I know, we're not fighting Prussia.

103
00:12:08,420 --> 00:12:10,740
Talk to someone
who has access to the Führer.

104
00:12:10,860 --> 00:12:12,300
Who? Rommel?

105
00:12:13,260 --> 00:12:15,180
He'd listen to you.

106
00:12:15,300 --> 00:12:17,580
He could get you a hearing in Berlin.

107
00:12:24,740 --> 00:12:27,540
First, let's find the English geologist.

108
00:12:27,660 --> 00:12:29,740
Without him, I've no hope.

109
00:12:31,660 --> 00:12:34,580
SUNDAY 28th MAY, 1944
HOLLOWAY PRISON, LONDON

110
00:12:34,700 --> 00:12:37,700
A stay of execution
by order of the Lord Chancellor.

111
00:12:37,820 --> 00:12:40,500
It's not a mistake?
I killed a man, remember.

112
00:12:40,620 --> 00:12:43,500
Your pimp? I don't call that a man.

113
00:12:45,260 --> 00:12:47,540
Nobody forced me into anything.

114
00:12:47,660 --> 00:12:50,300
Stop being silly.
If not for us, you'd have been hanged.

115
00:12:51,420 --> 00:12:54,060
That's your only alternative
to our proposition.

116
00:12:55,180 --> 00:12:57,620
I knew there'd be a catch. What is this?

117
00:12:57,740 --> 00:13:00,900
We need you for a mission in France.

118
00:13:01,020 --> 00:13:03,020
Who do I have to fuck? Pétain?

119
00:13:03,140 --> 00:13:07,540
We are reliably informed
that you used to perform nude in Soho.

120
00:13:08,860 --> 00:13:12,180
You need a girl who'll get her leg over,
so here I am?

121
00:13:12,300 --> 00:13:14,140
You must be desperate.

122
00:13:14,260 --> 00:13:16,660
We also need a girl who can kill.

123
00:13:16,780 --> 00:13:19,060
What do I get out of all this?

124
00:13:21,060 --> 00:13:22,860
What are you thinking?

125
00:13:24,060 --> 00:13:25,620
That you can refuse?

126
00:13:29,740 --> 00:13:32,940
I'm not the type who works for nothing.
Thanks anyway.

127
00:13:37,060 --> 00:13:39,660
You'll die like a whore
who never had a chance.

128
00:13:39,780 --> 00:13:41,100
Is that what you want?

129
00:13:44,580 --> 00:13:47,140
If the mission's a success,
you'll be pardoned.

130
00:13:49,660 --> 00:13:52,260
I'm sure she's the right choice.

131
00:13:53,660 --> 00:13:56,900
We can't trust her, she's a nutcase.

132
00:13:57,020 --> 00:14:01,060
A rope round her neck and she said no.
That takes a hell of a nerve.

133
00:14:01,180 --> 00:14:04,620
When the next one finally arrives,
let me do the talking.

134
00:14:07,660 --> 00:14:09,060
What are you doing?

135
00:14:10,700 --> 00:14:13,020
I'd hate to cramp your style.

136
00:14:21,100 --> 00:14:23,380
I'm Gaëlle Lemenech.
We have an appointment?

137
00:14:23,500 --> 00:14:25,780
I'm Louise. Please, sit down.

138
00:14:25,900 --> 00:14:28,740
Sorry I'm late.
The General often keeps us back.

139
00:14:29,740 --> 00:14:31,300
Hello, Hoe.

140
00:14:31,420 --> 00:14:32,860
Hi, Pick.

141
00:14:32,980 --> 00:14:35,500
- Still owe me a drink.
- Tomorrow?

142
00:14:36,820 --> 00:14:38,380
"Hoe"?

143
00:14:38,500 --> 00:14:41,140
Yes, it's a tradition in our unit.

144
00:14:41,260 --> 00:14:43,700
They give us the names of tools.

145
00:14:43,820 --> 00:14:47,260
They wanted to call me Saw but I refused.

146
00:14:47,380 --> 00:14:50,260
"Eye-Saw!"
You see my point, don't you?

147
00:14:53,420 --> 00:14:56,100
Besides explosives,
what do you do for De Gaulle?

148
00:14:56,220 --> 00:14:57,700
Do you ever go on missions?

149
00:14:57,820 --> 00:15:00,460
Never. It's my only regret.

150
00:15:02,780 --> 00:15:03,980
Thanks.

151
00:15:06,540 --> 00:15:09,900
Making bombs without blowing them up
is frustrating.

152
00:15:10,020 --> 00:15:12,940
You can vent your frustrations
with me in France.

153
00:15:16,140 --> 00:15:18,420
Sorry, but I can't.

154
00:15:18,540 --> 00:15:21,260
I work for De Gaulle, not Churchill.

155
00:15:29,380 --> 00:15:32,940
If you don't mind me saying,
you're in with that officer.

156
00:15:33,060 --> 00:15:34,100
Sorry?

157
00:15:34,220 --> 00:15:36,620
The lieutenant.
He can't take his eyes off you.

158
00:15:36,740 --> 00:15:40,060
If I were you, I wouldn't think twice.

159
00:15:40,180 --> 00:15:42,540
Who knows? Tomorrow, we may be dead.

160
00:15:54,380 --> 00:15:56,820
You realise what you're asking?

161
00:15:56,940 --> 00:16:01,460
Explosives expert and trained commando.
If you can do better...

162
00:16:03,180 --> 00:16:04,700
It's humiliating.

163
00:16:05,580 --> 00:16:07,980
You weren't always so picky.

164
00:16:14,540 --> 00:16:17,060
- Well?
- She'll do it.

165
00:16:17,180 --> 00:16:18,980
Was it hard?

166
00:16:19,100 --> 00:16:23,580
The scouts, the cancelled wedding,
the fiancé sent to work in Germany...

167
00:16:23,700 --> 00:16:26,380
Listening to that all night was tough, yes.

168
00:16:27,380 --> 00:16:30,100
Is it true the Gestapo tortured you last year?

169
00:16:32,540 --> 00:16:34,340
Yes.

170
00:16:39,140 --> 00:16:41,260
Why go back?

171
00:16:41,380 --> 00:16:43,300
What are you trying to prove?

172
00:16:45,100 --> 00:16:47,340
I'm not trying to prove anything.

173
00:16:56,780 --> 00:17:00,020
MONDAY 29th MAY, 1944
FANY HQ, LONDON

174
00:17:08,500 --> 00:17:10,020
You're sure it's for me?

175
00:17:15,820 --> 00:17:18,180
Who told you I love Paris-Brests?

176
00:17:18,300 --> 00:17:21,220
We talked to your parents in Orléans.

177
00:17:21,340 --> 00:17:23,700
Apparently, they bake wonderful cakes.

178
00:17:23,820 --> 00:17:28,020
Their only daughter runs off to Paris.
That must really hurt.

179
00:17:28,140 --> 00:17:32,740
Especially when she flees to London
under an assumed identity.

180
00:17:34,380 --> 00:17:36,460
Mademoiselle Suzy Desprès.

181
00:17:37,420 --> 00:17:38,700
Very pretty, Liliane.

182
00:17:41,460 --> 00:17:43,860
I wonder if that doesn't suit you more.

183
00:17:46,100 --> 00:17:49,300
We know you were
the mistress of a Nazi officer

184
00:17:49,420 --> 00:17:52,380
when you danced
at the Folies-Bergères in Paris.

185
00:17:52,500 --> 00:17:55,660
And here we have the banns for a wedding...

186
00:17:56,900 --> 00:17:59,540
due to take place on 14th October, '41,

187
00:18:00,300 --> 00:18:02,340
at the church of Saint-Germain-des-Prés.

188
00:18:03,980 --> 00:18:06,700
Curiously, the ceremony never took place.

189
00:18:08,780 --> 00:18:09,820
Why?

190
00:18:15,660 --> 00:18:18,900
Stand up. Remove that uniform.

191
00:18:19,020 --> 00:18:21,260
You're unworthy of it.

192
00:18:23,780 --> 00:18:26,020
Leave this one to me.

193
00:18:30,540 --> 00:18:33,060
Why not come for me before, if you knew?

194
00:18:33,180 --> 00:18:34,740
Because we need you now.

195
00:18:34,860 --> 00:18:36,500
For my secretarial skills?

196
00:18:36,620 --> 00:18:39,020
No, dancing back home. For a good cause.

197
00:18:39,140 --> 00:18:40,900
I'll never go back to France.

198
00:18:41,020 --> 00:18:43,220
It's that or a court martial. Choose!

199
00:18:43,340 --> 00:18:46,620
I put my past behind me.
I broke off the engagement.

200
00:18:46,740 --> 00:18:49,540
If you want to put it behind you,
I have the solution.

201
00:18:59,260 --> 00:19:02,140
TUESDAY 30th MAY, 1944
TEMPSFORD AIRFIELD

202
00:19:17,020 --> 00:19:18,020
For unforeseen expenses.

203
00:19:22,140 --> 00:19:23,820
How much is in there?

204
00:19:30,940 --> 00:19:35,660
And these are your travel permits
and ration books.

205
00:19:36,300 --> 00:19:39,740
You must each learn
your new identity by heart.

206
00:19:39,860 --> 00:19:41,420
It's very important.

207
00:19:41,540 --> 00:19:44,100
Raymonde? Do I look like a Raymonde?

208
00:19:44,220 --> 00:19:46,340
Funny name for a music-hall artiste.

209
00:19:46,460 --> 00:19:49,780
If the Gestapo arrest you,
you hold out for 48 hours.

210
00:19:49,900 --> 00:19:53,700
- 48 hours?
- Long enough for the others to reach safety.

211
00:19:54,740 --> 00:19:56,500
What if they torture us?

212
00:19:56,620 --> 00:19:58,580
Detach yourselves from reality.

213
00:19:59,500 --> 00:20:03,660
Always remember, talking too soon
can cost people their lives.

214
00:20:05,140 --> 00:20:07,900
As a last resort, you have this.

215
00:20:10,020 --> 00:20:11,620
- Cyanide?
- Yes.

216
00:20:12,580 --> 00:20:17,100
If ever you need it, bite on it.
The poison acts in five minutes.

217
00:20:18,020 --> 00:20:19,620
And farewell, Raymonde!

218
00:20:25,700 --> 00:20:27,820
- Have you ever been blessed?
- Never.

219
00:20:27,940 --> 00:20:30,380
If I get on my knees, it's not to pray.

220
00:20:32,820 --> 00:20:36,260
Your brother's a sweetie,
but I'm not stupid.

221
00:20:36,380 --> 00:20:38,540
Did you ask him to chat me up?

222
00:20:39,500 --> 00:20:40,660
Careful what you say.

223
00:20:45,060 --> 00:20:46,140
Actually, I'm glad.

224
00:20:46,260 --> 00:20:49,100
I was bored and underpaid with De Gaulle.

225
00:21:12,740 --> 00:21:15,420
- You lost this.
- It doesn't matter.

226
00:21:15,540 --> 00:21:18,220
I'd never use it.
It's against my religion.

227
00:21:18,340 --> 00:21:20,940
God doesn't care
what goes on down here.

228
00:21:21,060 --> 00:21:22,860
You say that because of your husband?

229
00:21:26,020 --> 00:21:29,060
My brother was shot on his 20th birthday.

230
00:21:29,180 --> 00:21:31,420
But I never lost my faith.

231
00:21:35,780 --> 00:21:40,380
<i>Field Marshal Pétain, head of state,
will address you now.</i>

232
00:21:40,500 --> 00:21:43,260
<i>French people,
don't make our situation worse</i>

233
00:21:43,380 --> 00:21:47,860
<i>by resorting to acts
that give rise to tragic reprisals.</i>

234
00:21:47,980 --> 00:21:53,580
<i>It will be innocents in the French
population who suffer the consequences.</i>

235
00:21:53,700 --> 00:21:57,780
<i>Don't listen to those
who try to exploit our distress.</i>

236
00:21:57,900 --> 00:22:00,780
<i>They will lead the country to disaster.</i>

237
00:22:00,900 --> 00:22:06,980
<i>France will save herself by observing
the highest standards of discipline.</i>

238
00:22:07,100 --> 00:22:11,060
<i>Obey, therefore,
the orders of the government.</i>

239
00:22:16,620 --> 00:22:20,780
Colonel? She's waiting at the bar.

240
00:22:21,700 --> 00:22:24,340
It wasn't easy, but I've found you a little gem.

241
00:22:24,460 --> 00:22:27,700
My dear Eddy, if it was easy,
I'd find her myself.

242
00:22:27,820 --> 00:22:31,180
Colonel, I have something to ask you...

243
00:22:31,300 --> 00:22:33,660
Not your export business again?

244
00:22:33,780 --> 00:22:36,300
If you help me, you'll triple your income.

245
00:22:36,420 --> 00:22:38,380
Triple my income?

246
00:22:38,500 --> 00:22:40,460
You think that interests me?

247
00:23:23,660 --> 00:23:26,700
NIGHT OF 31 st MAY, 1944
NORMANDY COAST

248
00:23:59,340 --> 00:24:01,980
Stop praying!
You'll bring us bad luck!

249
00:24:02,100 --> 00:24:05,100
Leave me alone.
I do what I want.

250
00:24:26,380 --> 00:24:28,420
Nurse!

251
00:24:28,540 --> 00:24:30,100
Come quickly!

252
00:24:35,260 --> 00:24:37,180
Everybody, prepare to jump!

253
00:24:39,500 --> 00:24:41,860
Suzy, clip yourself on. Hurry!

254
00:24:41,980 --> 00:24:43,740
Check your gear.

255
00:24:46,580 --> 00:24:48,140
Number four, OK!

256
00:24:51,580 --> 00:24:53,260
Number three, OK!

257
00:24:54,100 --> 00:24:55,860
Number two, OK!

258
00:24:56,620 --> 00:24:58,220
Number one, OK!

259
00:25:25,260 --> 00:25:28,140
THURSDAY 1st JUNE, 1944
HOTEL REGINA, PARIS

260
00:25:33,100 --> 00:25:36,980
Colonel, I got a call from
the military hospital in Pont-I'Evêque.

261
00:25:37,940 --> 00:25:40,660
I'm waiting for confirmation,

262
00:25:40,780 --> 00:25:42,700
but we may have our geologist.

263
00:25:47,060 --> 00:25:49,540
ROAD TO PONT-L'EVÊQUE, NORMANDY

264
00:25:57,820 --> 00:26:00,340
Your permits, please.

265
00:26:03,820 --> 00:26:05,620
Who's she? I don't know her.

266
00:26:05,740 --> 00:26:08,340
She's new. Replacing Lucienne.

267
00:26:31,500 --> 00:26:34,340
Your papers, please!

268
00:26:35,180 --> 00:26:38,620
- We're here for the show.
- Go right ahead.

269
00:26:39,940 --> 00:26:43,180
Find the Englishman
and I'll catch up with you.

270
00:26:44,180 --> 00:26:46,060
What are you going to do?

271
00:26:47,580 --> 00:26:49,620
I need a word with my brother.

272
00:26:50,860 --> 00:26:52,780
Nobody gets out until I say so.

273
00:26:56,220 --> 00:26:58,700
There's a problem.
Suzy's ex just arrived.

274
00:27:00,180 --> 00:27:02,860
- Are you sure?
- Positive. The one In the photo.

275
00:27:03,860 --> 00:27:06,860
We can't let him take the geologist
or see Suzy.

276
00:27:07,780 --> 00:27:10,980
The SS-man she was to marry,
was it Heindrich?

277
00:27:13,660 --> 00:27:15,700
What are you keeping from me?

278
00:27:15,820 --> 00:27:18,700
Get back to Maria.
That's an order.

279
00:27:21,780 --> 00:27:23,980
Come on, everybody out.

280
00:27:25,780 --> 00:27:28,100
Careful, girls, you're being watched.

281
00:28:03,300 --> 00:28:06,580
- They transferred him upstairs.
- Shit!

282
00:28:06,700 --> 00:28:08,740
Come on!

283
00:28:49,740 --> 00:28:51,500
I'll get a stretcher.

284
00:28:51,620 --> 00:28:53,300
I'll wait here.

285
00:29:15,980 --> 00:29:17,780
In 30 seconds, you're on.

286
00:29:19,420 --> 00:29:22,180
A word of advice.
Don't look, you may get a shock!

287
00:29:34,380 --> 00:29:37,180
- The patient needs treatment.
- Nobody enters.

288
00:29:37,300 --> 00:29:38,980
- Colonel's orders.
- Get off!

289
00:29:39,660 --> 00:29:42,140
- I won't allow this, Colonel!
- What are you talking about?

290
00:29:42,260 --> 00:29:45,220
The man you're beating.
It's a hospital, not an abattoir.

291
00:29:51,180 --> 00:29:54,220
You are forbidden to enter that room.

292
00:29:54,340 --> 00:29:55,660
Understood?

293
00:31:03,300 --> 00:31:05,580
What are you doing there? Get out!

294
00:31:08,180 --> 00:31:09,980
Finished?

295
00:31:38,860 --> 00:31:39,820
I won't be long.

296
00:31:40,980 --> 00:31:44,300
Excuse me, can I make a call to Paris?

297
00:32:33,220 --> 00:32:36,180
Encore! Encore!

298
00:32:49,340 --> 00:32:51,100
Louise! The show's ended.

299
00:32:51,220 --> 00:32:52,340
Let's go!

300
00:33:01,860 --> 00:33:03,780
Hurry up!

301
00:33:28,140 --> 00:33:29,900
Open the windows!

302
00:33:39,180 --> 00:33:40,820
Jeanne!

303
00:33:44,660 --> 00:33:46,580
Come on!

304
00:33:48,300 --> 00:33:51,660
Cover them, with me! Come here!

305
00:33:52,500 --> 00:33:55,300
Open up! Back a bit more!

306
00:34:18,860 --> 00:34:20,460
20 seconds!

307
00:34:27,460 --> 00:34:28,420
Ten seconds!

308
00:34:30,420 --> 00:34:32,300
- Hurry!
- Let's go!

309
00:34:51,660 --> 00:34:54,060
Tell the Wehrmacht
to sweep the whole area!

310
00:34:54,180 --> 00:34:57,700
I can't, Colonel.
The radio room's been blown up.

311
00:35:04,460 --> 00:35:08,220
This is Colonel Karl Heindrich.
Put me through to Wehrmacht HQ in...

312
00:35:08,340 --> 00:35:10,340
- Colonel...
- Wait!

313
00:35:10,460 --> 00:35:14,540
.. Wehrmacht HQ in Pont-I'Evêque.
I need reinforcements! What?

314
00:35:14,660 --> 00:35:17,340
One of the soldiers took photos of the show.

315
00:35:17,460 --> 00:35:18,420
What?

316
00:35:26,380 --> 00:35:28,580
Well done, ladies.

317
00:35:28,700 --> 00:35:30,140
You did a good job.

318
00:35:34,220 --> 00:35:36,460
When the war's over,

319
00:35:36,580 --> 00:35:39,020
I'll light a candle in church for you.

320
00:35:39,940 --> 00:35:41,420
You may think it's stupid,

321
00:35:43,500 --> 00:35:45,540
but promise me you'll do the same.

322
00:35:52,700 --> 00:35:54,380
You too, Louise.

323
00:35:55,220 --> 00:35:57,260
I'll never go back into a church.

324
00:35:57,900 --> 00:36:01,540
And candles are for the dead,
not the living.

325
00:36:04,300 --> 00:36:06,220
Maria, have you got a pencil?

326
00:36:38,140 --> 00:36:41,940
<i>Colonel, an English aircraft
was spotted over the coast.</i>

327
00:36:42,060 --> 00:36:43,620
Message received.

328
00:36:47,420 --> 00:36:50,540
Is there a spot nearby
where a plane could land?

329
00:37:01,340 --> 00:37:03,180
- Bernard, René, get the fires lit.
- OK.

330
00:37:03,700 --> 00:37:04,980
Stay in the ambulance.

331
00:37:05,100 --> 00:37:07,020
Louise, we need to talk.

332
00:37:10,660 --> 00:37:13,180
- We're heading for Paris.
- What's Phoenix mean?

333
00:37:15,020 --> 00:37:17,420
- I can't tell you.
- Or about Heindrich either?

334
00:37:17,540 --> 00:37:20,860
If he'd seen Suzy,
we'd all be dead by now.

335
00:37:20,980 --> 00:37:24,140
Why did you lie to me?
Don't you trust me any more?

336
00:37:25,620 --> 00:37:27,740
- You haven't changed.
- Wait...

337
00:37:29,260 --> 00:37:33,420
- We can still eliminate Heindrich in Paris.
- Paris?

338
00:37:33,540 --> 00:37:37,300
It's crawling with collaborators.
It's too dangerous. With Suzy?

339
00:37:37,420 --> 00:37:40,500
She's our best chance.
It's why we chose her.

340
00:37:41,100 --> 00:37:43,020
You want to use her as bait?

341
00:37:43,140 --> 00:37:44,900
That's your Plan B?

342
00:37:45,020 --> 00:37:47,620
She won't have the nerve.
She'd rather kill herself.

343
00:37:48,260 --> 00:37:50,100
The girls did their jobs.

344
00:37:50,220 --> 00:37:53,260
They were chosen for a single mission.
I gave my word.

345
00:37:54,180 --> 00:37:57,460
Let them go.
We'll find a way without them.

346
00:37:59,340 --> 00:38:02,500
Maria, René and I drive to Paris tonight.

347
00:38:02,620 --> 00:38:05,780
Bernard knows a safe house.
You'll join us tomorrow.

348
00:38:05,900 --> 00:38:09,140
We'll meet at the Institute for the Blind
at Duroc.

349
00:38:09,260 --> 00:38:11,340
The director is with us.

350
00:38:11,460 --> 00:38:13,820
They'll be risking their lives, you realise?

351
00:38:14,700 --> 00:38:17,580
Every day, agents die
accomplishing their missions.

352
00:38:19,020 --> 00:38:21,300
You'd never have done this to men.

353
00:38:43,500 --> 00:38:45,700
Girls, here comes our taxi to Buckingham!

354
00:39:05,460 --> 00:39:08,820
The mission's not over.
We must be in Paris tomorrow.

355
00:39:08,940 --> 00:39:11,460
Paris? Is he insane?

356
00:39:11,580 --> 00:39:13,940
Nobody said anything about Paris.

357
00:39:14,060 --> 00:39:16,820
We had a deal. I respected my side of it.

358
00:39:16,940 --> 00:39:18,620
You gave me your word.

359
00:39:20,460 --> 00:39:21,620
I'm going back.

360
00:39:22,380 --> 00:39:25,220
We're all going to Paris. No exceptions.

361
00:39:25,340 --> 00:39:26,420
Go on, fire!

362
00:39:26,540 --> 00:39:29,100
Jeanne, not now. We have to obey orders.

363
00:39:29,220 --> 00:39:31,380
You're a bunch of shits!

364
00:39:31,500 --> 00:39:33,500
Hurry back to the safe house.

365
00:39:56,300 --> 00:39:58,940
Stop! Get your hands in the air! Don't move!

366
00:40:15,860 --> 00:40:17,980
Cease firing!

367
00:40:19,020 --> 00:40:21,420
No, don't. I'll go.

368
00:40:22,940 --> 00:40:25,260
I want him alive. Aim for his legs!

369
00:40:38,540 --> 00:40:41,260
Grab him, quickly!

370
00:40:41,660 --> 00:40:43,020
Get his pill!

371
00:41:04,420 --> 00:41:05,980
Move it out!

372
00:41:09,340 --> 00:41:10,780
Don't.

373
00:41:27,300 --> 00:41:29,220
Is the mission confirmed?

374
00:41:30,700 --> 00:41:32,820
You spend the night here.

375
00:41:32,940 --> 00:41:34,740
Bernard will take me to Lisieux.

376
00:41:34,860 --> 00:41:36,660
I'm going ahead to Paris.

377
00:41:36,780 --> 00:41:40,420
Sleep in the cellar tonight,
just to be on the safe side.

378
00:41:42,700 --> 00:41:44,620
I'm really sorry about your brother.

379
00:42:00,020 --> 00:42:04,060
A freezing cellar and rotten apples
when we have millions in cash.

380
00:42:04,940 --> 00:42:07,340
What better time to spend our cash?

381
00:42:07,460 --> 00:42:09,820
How can you think of money now?

382
00:42:09,940 --> 00:42:11,860
It's the way I am.

383
00:42:11,980 --> 00:42:14,980
I'm a whore, not a choirboy. Never forget it.

384
00:42:15,100 --> 00:42:17,100
You're used to humiliation. I'm not!

385
00:42:17,220 --> 00:42:19,620
- I refuse to crawl to the Germans!
- Let go of me!

386
00:42:19,740 --> 00:42:22,820
If we're ordered to Paris,
we don't argue, OK?

387
00:42:22,940 --> 00:42:25,980
Stuff your orders!
I'm not a spy or a resistance hero.

388
00:42:26,100 --> 00:42:27,460
Find some professionals.

389
00:42:27,580 --> 00:42:29,820
Pierre's been arrested. René's dead.

390
00:42:29,940 --> 00:42:31,940
It's our duty to take their place.

391
00:42:32,060 --> 00:42:33,380
Our duty?

392
00:42:33,500 --> 00:42:37,060
As soon as the war's over,
we'll be whores and slaves again.

393
00:42:37,180 --> 00:42:39,220
I'm not dying for the French flag.

394
00:42:39,340 --> 00:42:43,060
In fact, I'll take my share
and be gone tomorrow.

395
00:42:43,180 --> 00:42:45,420
Anybody touches the money...

396
00:42:57,260 --> 00:42:58,700
Stop!

397
00:44:16,020 --> 00:44:17,620
Hurry, it's light already.

398
00:44:26,100 --> 00:44:30,740
We did our best, Colonel,
but the negative was partly damaged.

399
00:44:30,860 --> 00:44:32,860
Only this one is of any use.

400
00:44:32,980 --> 00:44:36,260
- Have you had it enlarged?
- Naturally.

401
00:44:50,380 --> 00:44:53,380
FRIDAY, 2nd JUNE, 1944
GESTAPO HQ, AVENUE FOCH, PARIS

402
00:45:20,460 --> 00:45:24,460
Shaky handwriting. A wounded man, maybe.
I found it in your pocket.

403
00:45:25,260 --> 00:45:27,180
The geologist wrote it, didn't he?

404
00:45:30,180 --> 00:45:33,660
I have no idea.
I don't know who you're talking about.

405
00:45:36,300 --> 00:45:39,020
And I've no idea who you are, Mr SOE.

406
00:45:40,140 --> 00:45:43,380
But given your nerve,
you can't be just anybody.

407
00:45:43,500 --> 00:45:48,300
And if you're not just anybody,
you must know what Phoenix refers to.

408
00:45:54,140 --> 00:45:55,620
What are these blocks for?

409
00:45:56,900 --> 00:45:58,860
Is it to do with the landings?

410
00:45:58,980 --> 00:46:00,940
I've no idea.

411
00:46:05,180 --> 00:46:06,460
Continue.

412
00:46:29,100 --> 00:46:32,020
- Who do you think you are?
- Don't play hard to get.

413
00:46:32,140 --> 00:46:34,580
Call me a slut, why don't you?

414
00:46:35,460 --> 00:46:39,180
Leave my friend alone
or I'll call the Germans.

415
00:46:45,140 --> 00:46:47,020
Coffee, ladies?

416
00:46:47,140 --> 00:46:48,620
No, thank you.

417
00:46:50,580 --> 00:46:52,780
Miss, is something wrong?

418
00:46:55,220 --> 00:46:57,260
A doctor! Find a doctor!

419
00:47:04,620 --> 00:47:07,220
Louise fainted in the restaurant car.

420
00:47:07,340 --> 00:47:08,900
Why didn't you help her?

421
00:47:09,020 --> 00:47:12,300
You're joking! It's packed with Germans.

422
00:47:12,420 --> 00:47:13,940
Come on, we're going back.

423
00:47:22,380 --> 00:47:26,500
- That's not yours.
- It's a friend's. We're looking for her.

424
00:47:26,620 --> 00:47:29,140
She's in that compartment. She fainted.

425
00:47:29,260 --> 00:47:31,220
A doctor's examining her.

426
00:47:43,460 --> 00:47:45,100
Your blood pressure's low.

427
00:47:45,220 --> 00:47:47,220
You need to eat something.

428
00:47:48,220 --> 00:47:51,900
- Are you a doctor?
- I wouldn't have examined you otherwise.

429
00:47:56,300 --> 00:47:58,700
When was your last period?

430
00:48:01,740 --> 00:48:03,700
Some time before the war.

431
00:48:03,820 --> 00:48:06,540
It must be all these people.
I couldn't breathe.

432
00:48:07,460 --> 00:48:08,820
I feel much better now.

433
00:48:08,940 --> 00:48:10,660
Congratulations to the mother...

434
00:48:12,380 --> 00:48:13,780
and father.

435
00:48:16,540 --> 00:48:18,340
You didn't know?

436
00:48:19,980 --> 00:48:22,740
- What about Louise?
- She's with a German doctor.

437
00:48:22,860 --> 00:48:25,460
Shit! Where's Jeanne?

438
00:48:25,580 --> 00:48:27,540
She was right behind me.

439
00:48:27,660 --> 00:48:29,100
Did she have the suitcase?

440
00:48:30,500 --> 00:48:32,340
Yes.

441
00:48:32,460 --> 00:48:34,340
Stay here. Don't move!

442
00:48:40,300 --> 00:48:43,180
Louise is with a Kraut.
We're fucked. Come with me.

443
00:48:43,300 --> 00:48:47,340
- Running won't win over your mum.
- Or save my dad from Verdun.

444
00:48:47,460 --> 00:48:49,180
Don't get preachy with me.

445
00:48:51,980 --> 00:48:53,380
What's that?

446
00:48:55,660 --> 00:48:57,300
"Thou shalt not kill"?

447
00:48:57,420 --> 00:48:59,460
Who's getting preachy now?

448
00:49:02,380 --> 00:49:05,580
All aboard! We'll soon be on our way.

449
00:49:29,700 --> 00:49:32,380
Louise, it's Suzy. Open the door.

450
00:49:32,500 --> 00:49:34,580
Are you OK? Do you need help?

451
00:49:34,700 --> 00:49:36,660
I'm fine. Tired, that's all.

452
00:49:54,340 --> 00:49:55,940
Colonel...

453
00:49:56,060 --> 00:49:57,780
Look at this one!

454
00:49:57,900 --> 00:49:59,340
A definite resemblance.

455
00:50:01,060 --> 00:50:07,180
Eddy, I'll have to transmit your file
to your Gestapo friends on Rue Lauriston.

456
00:50:07,300 --> 00:50:11,740
Her lookalike's
under a stone somewhere, but it takes time.

457
00:50:11,860 --> 00:50:15,900
Time is running out.
For you and for me.

458
00:50:26,900 --> 00:50:28,820
Look where you're going!

459
00:50:29,860 --> 00:50:31,780
Sorry, I was miles away.

460
00:50:31,900 --> 00:50:34,580
- Are you OK? Are you hurt?
- It's nothing.

461
00:50:34,700 --> 00:50:37,140
Perhaps I could buy you a drink.

462
00:50:40,820 --> 00:50:42,820
SAINT-LAZARE STATION, PARIS

463
00:51:03,260 --> 00:51:05,900
Be at the Institute for the Blind in one hour.

464
00:51:06,020 --> 00:51:07,540
Make sure no one follows you.

465
00:51:07,660 --> 00:51:12,060
Or else, tomorrow noon at Concorde,
on the Pont de Neuilly platform.

466
00:51:12,180 --> 00:51:14,420
Now, let's split up.

467
00:51:19,220 --> 00:51:21,700
- May I?
- Thank you, doctor.

468
00:51:21,820 --> 00:51:25,020
- This is my friend, Suzy.
- It's a pleasure to meet you.

469
00:51:25,140 --> 00:51:26,700
Follow me.

470
00:51:30,220 --> 00:51:31,620
The ladies are with me.

471
00:51:39,420 --> 00:51:41,100
Goodbye, miss.

472
00:51:52,980 --> 00:51:57,860
Officer, all the way from Lisieux,
that fellow with a moustache

473
00:51:57,980 --> 00:51:59,820
couldn't keep his hands to himself.

474
00:51:59,940 --> 00:52:02,220
Fine, we'll have a word with him.

475
00:52:25,900 --> 00:52:27,660
Go through, miss.

476
00:52:32,860 --> 00:52:34,940
Please come with me.

477
00:52:42,180 --> 00:52:45,140
- And our prisoner?
- Still nothing.

478
00:52:45,260 --> 00:52:47,580
We've got to make him talk.

479
00:52:47,700 --> 00:52:49,780
I need something to persuade Rommel.

480
00:52:49,900 --> 00:52:53,500
Colonel, these photos
are from the SS archives.

481
00:52:55,140 --> 00:52:57,500
They're close to the woman you described.

482
00:53:12,500 --> 00:53:14,660
Louise Desfontaines.

483
00:53:16,700 --> 00:53:19,220
That's her. I'm sure of it.

484
00:53:35,060 --> 00:53:37,460
I'm late. The postman overslept.

485
00:53:42,540 --> 00:53:44,460
Hurry, they're waiting for you.

486
00:53:50,500 --> 00:53:52,460
Your rooms will soon be ready.

487
00:54:00,620 --> 00:54:02,700
We were worried. Any problems?

488
00:54:02,820 --> 00:54:04,660
I got lost a few streets away.

489
00:54:04,780 --> 00:54:08,540
- Gaëlle isn't here?
- Not yet. I left her at the checkpoint.

490
00:54:09,740 --> 00:54:12,460
- Why are you all dressed up?
- I wish I knew.

491
00:54:12,580 --> 00:54:14,180
But apparently it's top secret.

492
00:54:15,660 --> 00:54:18,140
- I'm taking her out.
- Promise me, no Germans.

493
00:54:19,420 --> 00:54:23,100
- I promise.
- Can we talk?

494
00:54:23,220 --> 00:54:25,140
Not now, we're in a hurry.

495
00:54:27,180 --> 00:54:30,700
All right. In that case, Suzy stays here.

496
00:54:35,860 --> 00:54:37,340
Come on, then.

497
00:54:41,180 --> 00:54:43,140
This Eddy guy, can we rely on him?

498
00:54:45,580 --> 00:54:48,940
I made sure he'd arrange
for Suzy to meet her colonel.

499
00:54:53,020 --> 00:54:54,380
Help me.

500
00:54:58,780 --> 00:55:01,140
Where do you plan their reunion?

501
00:55:01,260 --> 00:55:04,100
Where they fell in love.
Room 813, at the Regina.

502
00:55:07,100 --> 00:55:09,780
- Who'll pull the trigger?
- Suzy.

503
00:55:09,900 --> 00:55:11,260
The gun will be in the room.

504
00:55:11,380 --> 00:55:14,700
Suzy kill Heindrich?
Forget it, she wouldn't hurt a fly.

505
00:55:16,580 --> 00:55:18,500
You'll have to convince her, then.

506
00:55:31,300 --> 00:55:34,100
For the last time, where is this woman?

507
00:55:34,820 --> 00:55:36,380
I don't know.

508
00:55:36,500 --> 00:55:38,420
I've never seen her before.

509
00:55:38,540 --> 00:55:40,820
I find that hard to believe.

510
00:55:43,580 --> 00:55:47,380
Our men picked this one up
at Saint-Lazare an hour ago.

511
00:55:47,500 --> 00:55:49,740
Off the Lisieux train.

512
00:55:53,420 --> 00:55:55,340
Introductions are unnecessary.

513
00:56:25,340 --> 00:56:27,860
You may be indifferent to your own pain,

514
00:56:27,980 --> 00:56:30,340
but perhaps not to that of your friend.

515
00:56:37,580 --> 00:56:38,620
Stop! Enough.


