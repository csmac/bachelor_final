1
00:00:08,900 --> 00:00:10,856
Four

2
00:04:20,140 --> 00:04:21,858
(sending message - Mom)

3
00:04:24,020 --> 00:04:25,976
(sending message - Sis)

4
00:04:33,220 --> 00:04:35,176
(sending message - Ingrid)

5
00:04:35,620 --> 00:04:37,576
(sending message - Lone)

6
00:05:08,340 --> 00:05:10,296
There is your best line.

7
00:05:21,300 --> 00:05:23,256
(New messages from the Falk.
Read them now?)

8
00:05:23,860 --> 00:05:30,891
(From: the Falk.
I can't take it anymore. Feel like blowing my head off.
Does that make me a coward?)

9
00:06:46,980 --> 00:06:48,936
Ok. That's fine.

10
00:06:50,900 --> 00:06:52,856
Bye.

11
00:07:57,700 --> 00:07:59,656
(Calling Mom)

12
00:09:01,420 --> 00:09:02,900
(New message - Sis)

13
00:09:02,900 --> 00:09:05,539
(From: Sis
I want to go home.)

14
00:10:50,980 --> 00:10:52,936
I want to go home.

15
00:11:42,020 --> 00:11:43,976
Come.

16
00:12:27,620 --> 00:12:29,576
Bathroom?

17
00:13:20,740 --> 00:13:21,968
(Turn off sound.)

18
00:13:31,780 --> 00:13:33,736
She probably has company.

19
00:13:34,180 --> 00:13:36,136
Doubt she will let me in tonight.

20
00:13:37,020 --> 00:13:38,976
What will you do?

21
00:13:39,700 --> 00:13:41,656
I'll be alright.

22
00:13:56,980 --> 00:13:58,936
Don't have time to sit here now.

23
00:14:06,180 --> 00:14:08,136
Don't you need them yourself?

24
00:14:22,100 --> 00:14:24,056
You don't have to leave.

25
00:15:07,300 --> 00:15:09,256
I've told you not to come here.

26
00:15:10,100 --> 00:15:12,056
I miss you.

27
00:15:13,540 --> 00:15:15,496
Can I come in?

28
00:15:19,220 --> 00:15:20,414
Are you seeing someone?

29
00:15:20,580 --> 00:15:22,020
No, I am not.

30
00:15:22,020 --> 00:15:23,976
Please leave.

31
00:15:25,700 --> 00:15:27,656
Mom and dad can't find you here.

32
00:15:45,380 --> 00:15:47,336
Can I borrow some money?

33
00:15:49,060 --> 00:15:51,016
It's important.

34
00:15:51,460 --> 00:15:53,416
Please.

35
00:17:01,060 --> 00:17:03,016
Hello.

36
00:17:04,340 --> 00:17:06,296
Do you have a light?

37
00:17:43,140 --> 00:17:45,096
Are you ok?

38
00:18:58,420 --> 00:19:00,376
Hi! Silver!

39
00:19:08,420 --> 00:19:10,376
Look!

40
00:19:12,020 --> 00:19:13,976
My God, she has puppies!

41
00:19:17,220 --> 00:19:19,176
-Did you know?
-Yes, they are six weeks old now.

42
00:19:20,580 --> 00:19:22,536
That one is Sis. And that is the Falk.

43
00:19:22,660 --> 00:19:24,616
I don't know if they are girls or boys, but....

44
00:19:31,940 --> 00:19:33,896
Hey! This is private property!

45
00:19:35,380 --> 00:19:37,336
We're sorry, we'll leave now.

46
00:19:39,940 --> 00:19:41,700
Bye, Silver.

47
00:19:41,700 --> 00:19:44,737
-Do you know the dog?
-Yes, she used to be ours.

48
00:19:45,540 --> 00:19:47,496
Our father gave her away
just before he died.

49
00:19:48,020 --> 00:19:49,419
He died?

50
00:19:49,860 --> 00:19:51,816
I thought he was
only moving.

51
00:19:53,060 --> 00:19:55,016
He was full of cancer.

52
00:19:55,460 --> 00:19:57,416
Did he say where he was moving?

53
00:19:58,020 --> 00:19:59,976
I didn't know he even had a family.

54
00:20:06,660 --> 00:20:09,697
Just stay here and visit the dog if you like.

55
00:20:12,820 --> 00:20:14,776
Why did you say he died?

56
00:20:15,780 --> 00:20:17,060
What did you want to say?

57
00:20:17,060 --> 00:20:20,257
The truth. That he left, because he
couldn't stand mom anymore.

58
00:21:28,660 --> 00:21:32,573
(From Ingrid
You can sleep here. I miss you.
Home alone until Wednesday.)

59
00:23:28,340 --> 00:23:32,174
-Little one.
-Sweetheart.

60
00:23:39,300 --> 00:23:41,256
How are you?

61
00:24:17,540 --> 00:24:19,496
Dance for me.

62
00:26:12,740 --> 00:26:14,696
Remember to lock when you leave.

63
00:29:11,140 --> 00:29:13,779
(I love you, mommy.)

64
00:32:11,140 --> 00:32:13,096
(New Messages from Mom.
Read now?)

65
00:32:14,980 --> 00:32:20,213
(From Mom.
Love you, too, Falk)

66
00:35:06,980 --> 00:35:08,936
Flatmates

67
00:35:32,340 --> 00:35:34,296
I'm going to find me a sexy girl tonight.

68
00:35:37,780 --> 00:35:39,736
So get off then.

69
00:35:40,420 --> 00:35:42,376
Are you horny or what?

70
00:35:42,500 --> 00:35:43,780
What are you talking about?

71
00:35:43,780 --> 00:35:45,736
You're rubbing yourself against me.

72
00:35:46,940 --> 00:35:48,896
You're the one who wanted to lie here.

73
00:35:49,220 --> 00:35:51,097
Yeah, but you're the one rubbing.

74
00:35:51,260 --> 00:35:55,094
Okay Hampus, I'm damn horny.
Especially on your tight little ass.

75
00:35:56,020 --> 00:35:57,533
You are.

76
00:35:57,860 --> 00:35:59,578
You really are. Let me touch it.

77
00:35:59,700 --> 00:36:01,656
Okay, be gentle.

78
00:36:01,780 --> 00:36:03,736
Fucking hell, that's disgusting!

79
00:36:04,420 --> 00:36:06,376
I'll have to screw you a little then.

80
00:36:12,900 --> 00:36:14,413
Fuck you.

81
00:36:14,660 --> 00:36:16,616
Yes, I fucked you.

82
00:36:31,380 --> 00:36:33,940
Hey Bjorn, have a look...do I look good?

83
00:36:34,180 --> 00:36:35,499
No.

84
00:36:35,700 --> 00:36:37,372
What do you mean "no"?

85
00:36:37,540 --> 00:36:39,496
You can't look good.

86
00:36:41,700 --> 00:36:43,975
I'm going out. Want to join?

87
00:36:44,900 --> 00:36:46,856
No I'll stay here.

88
00:36:47,860 --> 00:36:51,091
Come on! You can fix this any time.

89
00:36:51,540 --> 00:36:53,337
No, I want to finish up today.

90
00:36:53,460 --> 00:36:55,416
We can go to a gay club

91
00:36:57,220 --> 00:36:59,176
... and you could get laid. It's been a while.

92
00:37:02,180 --> 00:37:04,136
That's probably why you are so
grumpy all the time.

93
00:37:04,500 --> 00:37:06,456
Yeah, that's probably why Hampus.

94
00:37:11,620 --> 00:37:13,576
So stay home then, and keep on sulking.

95
00:37:19,940 --> 00:37:21,540
Give me a ring if you change your mind.

96
00:37:21,540 --> 00:37:22,814
Bye.

97
00:37:57,860 --> 00:38:00,897
Hi this is Hampus. Say something if it's important
....if not don't. Bye.

98
00:39:41,380 --> 00:39:42,449
Hi.

99
00:40:00,900 --> 00:40:02,856
Bye bye.

100
00:40:25,780 --> 00:40:27,736
So what do you think?

101
00:40:27,940 --> 00:40:29,896
She's cute.

102
00:40:30,180 --> 00:40:32,136
Yeah...

103
00:40:34,820 --> 00:40:36,856
In love?

104
00:40:38,340 --> 00:40:40,296
Don't know.... I don't know.

105
00:40:47,940 --> 00:40:50,374
You're so cute. Little baby bear.

106
00:40:54,100 --> 00:40:56,409
Give me a hug so I can feel.

107
00:41:02,500 --> 00:41:04,456
Nice and clean.

108
00:41:08,420 --> 00:41:09,380
Now?

109
00:41:09,380 --> 00:41:10,574
No.

110
00:41:11,940 --> 00:41:14,977
Now! No it turned fuzzy again.

111
00:41:15,220 --> 00:41:17,176
There you've got it.

112
00:41:27,620 --> 00:41:30,339
How many channels do we have? Look...

113
00:42:29,460 --> 00:42:31,416
Come in.

114
00:42:34,260 --> 00:42:35,215
Hi Bjorn.

115
00:42:35,460 --> 00:42:36,449
Hi.

116
00:42:38,980 --> 00:42:40,936
Uhm... do you have my...
where is my mobile charger Hampus?

117
00:42:44,340 --> 00:42:46,012
It's in here somewhere...

118
00:42:46,180 --> 00:42:47,738
Where?

119
00:42:47,940 --> 00:42:49,896
I don't know.

120
00:42:51,460 --> 00:42:53,416
Can you help me look for it?

121
00:42:54,340 --> 00:42:56,296
Take mine then. It's in the socket.

122
00:43:09,620 --> 00:43:11,850
You'll get it back when you've found mine.

123
00:43:14,420 --> 00:43:16,376
Yes, that sounds good.

124
00:43:16,540 --> 00:43:18,849
Bye bye.

125
00:43:20,260 --> 00:43:22,216
Stop it!

126
00:43:44,100 --> 00:43:46,056
Yes?

127
00:43:46,500 --> 00:43:48,456
Hi.

128
00:43:48,660 --> 00:43:50,616
I'm leaving now, so I thought I'd say goodbye.

129
00:43:52,900 --> 00:43:56,415
Okay, bye bye. Going home?

130
00:43:57,220 --> 00:43:59,176
Yes, well no... I'm off to work actually.

131
00:44:01,060 --> 00:44:03,016
Shall we go? I'll follow you to the tram.

132
00:44:03,380 --> 00:44:04,938
You don't have to, I can go...

133
00:44:05,140 --> 00:44:07,096
Of course I am, come on.

134
00:44:09,700 --> 00:44:11,053
Okay, well...bye, bye.

135
00:44:11,300 --> 00:44:12,335
Bye.

136
00:45:00,100 --> 00:45:02,056
Hi this is Hampus. Say something if it's important

137
00:45:03,060 --> 00:45:05,016
... if not don't. Bye.

138
00:45:27,940 --> 00:45:29,896
What are you up to?

139
00:45:30,500 --> 00:45:32,456
I'm looking for the ISS.

140
00:45:32,820 --> 00:45:34,776
What "the ISS"? What's that?

141
00:45:36,100 --> 00:45:38,056
The International Space Station.

142
00:45:40,100 --> 00:45:42,056
Really. Show me then, Where is it?

143
00:45:44,740 --> 00:45:46,696
You can't see it now

144
00:45:48,100 --> 00:45:50,056
So what have you been up to all night?

145
00:45:50,180 --> 00:45:52,136
I've been painting.

146
00:45:52,900 --> 00:45:54,856
Paint...? What?

147
00:46:03,460 --> 00:46:05,416
Have you been painting all night?

148
00:46:08,580 --> 00:46:09,300
Yeah.

149
00:46:09,300 --> 00:46:11,256
Have some water Hampus.

150
00:46:12,260 --> 00:46:14,216
"Have some water Hampus".

151
00:46:16,900 --> 00:46:18,856
My little sweetheart.

152
00:46:24,740 --> 00:46:26,696
I'll get you some water.

153
00:46:37,140 --> 00:46:41,213
Wow, that's great. What a job!

154
00:46:53,700 --> 00:46:55,656
I've bought you a space shuttle

155
00:46:58,500 --> 00:47:00,456
It stands at the parking lot.

156
00:47:00,740 --> 00:47:01,570
Good. Come on.

157
00:47:01,780 --> 00:47:03,140
I really have!

158
00:47:03,140 --> 00:47:03,860
I really have!

159
00:47:03,860 --> 00:47:04,900
But I bought a space shuttle!

160
00:47:04,900 --> 00:47:05,696
Come.

161
00:47:08,340 --> 00:47:09,056
No, no, no!

162
00:47:09,220 --> 00:47:11,780
No stop it, I want to sleep... I want to sleep here.

163
00:47:12,980 --> 00:47:14,811
There's not enough space for both of us.

164
00:47:15,620 --> 00:47:17,060
Come on, be a little generous for once.

165
00:47:17,060 --> 00:47:18,698
Come on, be a little generous for once.

166
00:47:55,860 --> 00:47:58,180
Hampus...

167
00:47:58,180 --> 00:47:59,249
Hampus...

168
00:48:03,460 --> 00:48:05,416
...shall I sleep in your bed instead?

169
00:53:08,260 --> 00:53:10,216
Have you cleaned up in here?

170
00:53:11,300 --> 00:53:12,740
No.

171
00:53:12,740 --> 00:53:13,252
No.

172
00:53:14,500 --> 00:53:16,456
It looks as if you've cleaned up.

173
00:53:39,620 --> 00:53:40,020
This is not working Bjorn.

174
00:53:40,020 --> 00:53:41,578
This is not working Bjorn.

175
00:56:21,460 --> 00:56:24,420
To the moon

176
00:56:24,420 --> 00:56:34,614
To the moon

177
00:57:04,420 --> 00:57:05,460
Ouch! Cunt!

178
00:57:05,460 --> 00:57:06,370
Ouch! Cunt!

179
00:57:26,820 --> 00:57:28,776
Cloth.

180
00:57:56,420 --> 00:58:00,260
Elvis? How long are we staying here?

181
00:58:00,260 --> 00:58:00,498
Elvis? How long are we staying here?

182
00:58:00,580 --> 00:58:02,536
I don't know.

183
00:59:25,380 --> 00:59:27,336
Bingo.

184
01:00:08,260 --> 01:00:10,216
How much does a hotdog cost?

185
01:00:14,660 --> 01:00:17,220
-What?
-How much does a hotdog cost?

186
01:00:17,220 --> 01:00:20,292
-It depends.
-On what?

187
01:00:21,060 --> 01:00:25,417
-What type.
-A regular sausage in bread.

188
01:00:28,100 --> 01:00:30,056
-With onion?
-Yes.

189
01:00:33,540 --> 01:00:37,294
-Hot or cold?
-Cold.

190
01:00:39,300 --> 01:00:41,256
20 kroner.

191
01:00:48,900 --> 01:00:50,856
I am starving!

192
01:01:20,740 --> 01:01:22,696
No!

193
01:01:23,140 --> 01:01:24,732
What are you doing?

194
01:01:24,940 --> 01:01:27,090
Get back here right now!

195
01:01:46,820 --> 01:01:52,770
If you could wish for one thing
what would you wish for?

196
01:01:54,500 --> 01:01:56,456
What would you wish for?

197
01:02:00,900 --> 01:02:03,972
What? If you could choose one thing,
what would you wish for?

198
01:02:04,100 --> 01:02:06,056
I don't bloody know.

199
01:02:09,700 --> 01:02:11,656
Choose one thing! Answer me!

200
01:02:15,780 --> 01:02:17,736
Choose one thing! Answer me!

201
01:02:19,620 --> 01:02:22,851
If you could choose! What?

202
01:02:32,100 --> 01:02:35,649
-If you could choose one thing?
-For you to shut up a little while!

203
01:02:43,300 --> 01:02:45,256
I'd choose a hotdog stand.

204
01:02:46,500 --> 01:02:48,456
I'd eat as many hotdogs as I felt like eating.

205
01:05:45,860 --> 01:05:47,816
Where are you going?

206
01:05:49,860 --> 01:05:51,816
-To the bathroom.
-I am going with you.

207
01:05:53,380 --> 01:05:55,336
I am only going to the bathroom.
Do you need to go?

208
01:05:58,500 --> 01:06:00,456
Do you have to go?

209
01:06:04,100 --> 01:06:06,614
-Aren't you going to pee?
-Don't have to, after all.

210
01:06:14,180 --> 01:06:17,217
-What's that?
-A razor.

211
01:06:25,380 --> 01:06:27,336
I am going to shave. Ok?

212
01:06:29,860 --> 01:06:31,816
I'll wait.

213
01:07:05,780 --> 01:07:08,772
When I get my hotdog stand
I won't let anyone buy anything.

214
01:07:09,220 --> 01:07:10,539
I'm keeping them all to myself.

215
01:07:31,940 --> 01:07:33,259
Watch out.

216
01:08:21,700 --> 01:08:23,656
Look at what I found!

217
01:08:32,260 --> 01:08:34,216
Elvis?

218
01:08:44,100 --> 01:08:46,056
Did mom have black hair?

219
01:08:48,180 --> 01:08:50,136
No.

220
01:08:58,980 --> 01:09:00,936
Yellow?

221
01:10:07,460 --> 01:10:09,416
Let's go.

222
01:10:14,340 --> 01:10:15,693
Where?

223
01:10:15,900 --> 01:10:17,891
To the moon.

224
01:10:18,620 --> 01:10:20,576
-Is it far?
-What?

225
01:10:21,060 --> 01:10:23,016
To the moon.

226
01:10:23,620 --> 01:10:25,576
As far as it is from the moon to here.

227
01:10:25,780 --> 01:10:27,816
That's all?

228
01:10:30,020 --> 01:10:31,976
I can't wait to go.

229
01:16:31,460 --> 01:16:33,416
I can't wait until summer.

230
01:16:36,420 --> 01:16:38,376
Maybe we'll still be here?

231
01:16:40,740 --> 01:16:42,696
Maybe.

232
01:16:55,300 --> 01:16:57,256
Where do you think they are going?

233
01:16:59,860 --> 01:17:01,816
Home.

234
01:19:32,340 --> 01:19:34,296
SPARK

235
01:21:32,900 --> 01:21:34,540
Can you believe it?

236
01:21:34,540 --> 01:21:36,019
They actually made a mistake with the cake!

237
01:21:37,780 --> 01:21:39,736
It's a good thing that I stopped by
and checked up on them.

238
01:21:40,860 --> 01:21:45,138
It would have been a disaster if Anders didn't get
his favourite cake for his birthday.

239
01:21:46,300 --> 01:21:48,256
By the way, we need to know if they are coming.

240
01:21:48,740 --> 01:21:50,696
Who?

241
01:21:54,980 --> 01:21:56,936
-Nice to meet you.
-Nice to see you again.

242
01:21:59,220 --> 01:22:01,140
This is Marie. You remember her?

243
01:22:01,140 --> 01:22:02,653
The artist.

244
01:22:03,940 --> 01:22:05,896
Oh.Yes.That's right.

245
01:22:08,900 --> 01:22:12,973
-Could I have a word?
-Yes, of course. I'll walk you out.

246
01:22:13,260 --> 01:22:15,216
Just a minute, Mother.

247
01:22:16,580 --> 01:22:18,536
I got the job in Copenhagen!

248
01:22:18,660 --> 01:22:20,616
Congratulations! How wonderful for you!

249
01:22:20,900 --> 01:22:22,856
For the both of us! It's fantastic!

250
01:22:22,980 --> 01:22:24,936
I am so excited.

251
01:22:25,540 --> 01:22:27,417
Everything is fine. Right?

252
01:22:27,620 --> 01:22:29,576
Yes.... yes.... But let's not talk about it now.

253
01:22:30,980 --> 01:22:32,936
I'll call you later, ok?

254
01:22:34,220 --> 01:22:34,811
Ok...

255
01:22:36,180 --> 01:22:38,136
I'll talk to you tonight?

256
01:22:38,300 --> 01:22:39,699
Yes.

257
01:22:44,100 --> 01:22:45,580
Happy Birthday, Anders.

258
01:22:45,580 --> 01:22:47,260
Congratulations on your promotion.

259
01:22:47,260 --> 01:22:50,700
So you've heard?
Looks like I will be following in your footsteps.

260
01:22:50,700 --> 01:22:52,213
Cheers!

261
01:23:01,780 --> 01:23:03,780
What? Who can that be now?

262
01:23:03,780 --> 01:23:05,736
I'll go see.

263
01:23:11,220 --> 01:23:14,530
-What are you doing here?
-You're not answering your phone.

264
01:23:14,660 --> 01:23:16,616
-Who is it?
-It's Marie.

265
01:23:17,300 --> 01:23:19,256
-It's Marie.
-Marie! Come in!

266
01:23:20,340 --> 01:23:22,296
We've just started eating....

267
01:23:22,660 --> 01:23:25,220
Are you having a party?

268
01:23:25,220 --> 01:23:27,370
-No, I don't want to...
-The more the merrier! Come on!

269
01:23:35,140 --> 01:23:37,096
Is there room for one more?

270
01:23:37,860 --> 01:23:39,771
Oh? Another guest?

271
01:23:40,100 --> 01:23:41,738
I guess we can manage.

272
01:24:30,660 --> 01:24:32,616
For he's a jolly good fellow...

273
01:24:38,820 --> 01:24:40,580
Welcome. Cheers.

274
01:24:40,580 --> 01:24:42,536
Cheers!

275
01:24:47,460 --> 01:24:49,018
And who are you?

276
01:24:49,220 --> 01:24:53,259
I'm Marie.... Just a friend of Caroline's.

277
01:25:26,820 --> 01:25:28,820
Caroline. Caroline?

278
01:25:28,820 --> 01:25:30,856
It's time to bring out the cake.

279
01:25:33,220 --> 01:25:35,654
-Everything is ready in the kitchen.
-Yes, of course...

280
01:25:52,100 --> 01:25:54,056
I can't understand how you can stand this charade.

281
01:25:57,380 --> 01:25:59,336
It will be a relief to go to Copenhagen.

282
01:26:00,580 --> 01:26:02,536
Stop it.

283
01:26:07,140 --> 01:26:09,734
-Everything is so easy for you.
-What do you mean?

284
01:26:12,020 --> 01:26:15,490
We can't just leave, just like that.

285
01:26:16,100 --> 01:26:18,056
Of course we can.

286
01:26:18,660 --> 01:26:20,616
We have talked about so many times.

287
01:26:21,380 --> 01:26:23,336
This is what we want.

288
01:26:35,460 --> 01:26:37,416
Let's not talk about it now.

289
01:26:37,700 --> 01:26:40,009
I have already accepted the job in Copenhagen.

290
01:26:47,300 --> 01:26:48,653
Kiss me.

291
01:26:55,620 --> 01:26:57,576
Come over to my place afterwards.

292
01:26:58,500 --> 01:27:01,651
-Are you coming? Everyone is waiting.
-We're coming now.

293
01:27:03,940 --> 01:27:07,620
I'm leaving.
I can't stand being here any longer.

294
01:27:07,620 --> 01:27:09,220
I'll talk to you later. Right?

295
01:27:09,220 --> 01:27:11,176
I'll do my best.

296
01:27:19,940 --> 01:27:23,728
-What a cake!
-Happy birthday.

297
01:27:27,460 --> 01:27:30,657
Dear guests, friends and family.

298
01:27:32,740 --> 01:27:34,696
And not least, my dear Caroline.

299
01:27:35,780 --> 01:27:38,214
This day is not just about me.

300
01:27:41,500 --> 01:27:43,331
It is also about us.

301
01:27:46,660 --> 01:27:48,616
Caroline...

302
01:27:49,860 --> 01:27:51,816
Will you marry me?

303
01:27:59,140 --> 01:28:00,619
Yes.

304
01:28:10,820 --> 01:28:12,776
Congratulations.

305
01:28:16,740 --> 01:28:19,618
-Welcome into the family.
-Thank you.

306
01:30:54,820 --> 01:30:56,776
I am so excited!

307
01:31:00,580 --> 01:31:02,536
Will you marry me?

308
01:31:04,740 --> 01:31:07,538
-It's time for us to have a serious talk.
-I don't want to talk about it.

309
01:31:13,060 --> 01:31:15,016
You are so beautiful.

310
01:31:23,780 --> 01:31:25,736
I have already accepted the job in Copenhagen.

311
01:31:30,020 --> 01:31:31,976
This is what we want.

312
01:31:35,780 --> 01:31:38,089
Caroline, will you marry me?

313
01:31:39,940 --> 01:31:41,896
Caroline, do it for my sake.

314
01:31:44,740 --> 01:31:46,696
I can't see how you can stand this charade.

315
01:32:47,780 --> 01:32:49,736
/text by CMP

316
01:33:58,740 --> 01:34:04,610
DVD production
by Cicknave Media Productions

