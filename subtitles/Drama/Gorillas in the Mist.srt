﻿1
00:00:00,088 --> 00:00:01,585
Previously on "The Mist..."

2
00:00:01,651 --> 00:00:03,435
- No one could love you.
- Alex loves me.

3
00:00:03,482 --> 00:00:05,383
Until she finds someone that can fuck her.

4
00:00:05,422 --> 00:00:06,915
[gunshot]

5
00:00:06,982 --> 00:00:08,259
I did.

6
00:00:08,314 --> 00:00:10,911
KEVIN: What did you do? What did you do?

7
00:00:10,974 --> 00:00:12,983
Jay was gonna take her away from me.

8
00:00:14,623 --> 00:00:15,958
Everyone knows.

9
00:00:16,044 --> 00:00:19,552
- Knows what?
- That you were a slut.

10
00:00:19,615 --> 00:00:21,469
- Who's the father?
- I thought it didn't matter.

11
00:00:21,532 --> 00:00:22,929
We can't tell her it's not me.

12
00:00:22,999 --> 00:00:24,728
She adores you. It would break her heart.

13
00:00:24,790 --> 00:00:26,052
I didn't touch her that night.

14
00:00:26,107 --> 00:00:27,217
Just like your father.

15
00:00:27,271 --> 00:00:29,230
Always blaming others
for your own failures.

16
00:00:29,301 --> 00:00:30,693
He thinks I raped her, too.

17
00:00:30,756 --> 00:00:32,957
If this thing stops, will you go with me?

18
00:00:33,035 --> 00:00:34,042
I'll go with you.

19
00:00:36,812 --> 00:00:39,504
Do you know how long
we've been looking for you?

20
00:00:39,559 --> 00:00:40,315
You know me?

21
00:00:40,382 --> 00:00:41,883
I'm just happy you're safe, sir.

22
00:00:43,719 --> 00:00:45,554
- I was with Kevin.
- Where is he?

23
00:00:45,620 --> 00:00:47,050
He's dead.

24
00:00:47,113 --> 00:00:49,025
You've been ready to
send people out for food

25
00:00:49,060 --> 00:00:50,065
when we have plenty.

26
00:00:50,112 --> 00:00:51,102
What do you think they're gonna say

27
00:00:51,137 --> 00:00:53,160
when they know you have your own supply?

28
00:00:53,195 --> 00:00:54,694
- Shelley's dead.
- What happened?

29
00:00:54,740 --> 00:00:56,817
- It was Alex.
- They're gonna come after her.

30
00:00:56,864 --> 00:00:57,798
Lock them up.

31
00:00:59,813 --> 00:01:01,282
Alex.

32
00:01:04,473 --> 00:01:06,575
[music]

33
00:01:06,609 --> 00:01:11,570
Subtitles by explosiveskull

34
00:01:23,336 --> 00:01:24,537
Did you do this?

35
00:01:24,585 --> 00:01:25,876
I had to keep him away from you.

36
00:01:25,923 --> 00:01:27,421
You lied to me.

37
00:01:27,487 --> 00:01:28,701
You've lost your mind.

38
00:01:28,736 --> 00:01:29,826
- He's a psycho.
- EVE: Alex.

39
00:01:29,861 --> 00:01:31,218
Leave me alone!

40
00:01:31,291 --> 00:01:32,879
I have to protect you.

41
00:01:37,425 --> 00:01:39,794
Now more than ever.

42
00:01:46,173 --> 00:01:47,608
What happened?

43
00:01:49,376 --> 00:01:50,844
My dad's dead.

44
00:02:00,187 --> 00:02:01,555
It's okay.

45
00:02:01,622 --> 00:02:04,293
- No!
- Adrian, no!

46
00:02:04,340 --> 00:02:05,573
He saved my life.

47
00:02:05,608 --> 00:02:06,691
- He raped you.
- He didn't.

48
00:02:06,746 --> 00:02:08,352
- I believe him.
- Oh!

49
00:02:08,407 --> 00:02:09,638
He did.

50
00:02:09,705 --> 00:02:11,707
I have proof.

51
00:02:11,773 --> 00:02:13,575
What proof?

52
00:02:15,544 --> 00:02:18,914
We were at the hospital,

53
00:02:18,980 --> 00:02:21,247
your dad and me.

54
00:02:21,249 --> 00:02:23,585
We talked to the doctor.

55
00:02:23,652 --> 00:02:26,388
They got the results for the DNA test

56
00:02:26,455 --> 00:02:28,090
before the mist even came.

57
00:02:28,156 --> 00:02:30,436
- It was him!
- Oh!

58
00:02:33,762 --> 00:02:35,364
What?

59
00:03:06,962 --> 00:03:08,797
Did you find him?

60
00:03:12,100 --> 00:03:13,568
No.

61
00:03:13,635 --> 00:03:16,872
Did you see the other people?

62
00:03:16,938 --> 00:03:18,807
Let's go to them.

63
00:03:18,874 --> 00:03:21,810
I don't think they'll understand.

64
00:03:21,877 --> 00:03:24,479
They haven't seen what we've seen.

65
00:03:24,546 --> 00:03:27,094
Then they will have seen something else.

66
00:03:27,141 --> 00:03:29,317
They haven't seen you survive the mist.

67
00:03:31,019 --> 00:03:33,922
You're ashamed of what you're going to do.

68
00:03:35,653 --> 00:03:37,259
You should be proud.

69
00:03:41,029 --> 00:03:43,598
They don't know Jay,

70
00:03:43,665 --> 00:03:45,267
not like I do.

71
00:03:57,479 --> 00:03:59,147
Wait here.

72
00:04:01,817 --> 00:04:03,618
I'll find him.

73
00:04:07,756 --> 00:04:12,694
[foreboding music playing]

74
00:04:19,034 --> 00:04:20,469
This is it.

75
00:04:20,535 --> 00:04:22,971
This is the last of the rations.

76
00:04:23,038 --> 00:04:25,941
- Keep your voice down.
- Why?

77
00:04:26,007 --> 00:04:27,598
You don't think they'll find out?

78
00:04:28,443 --> 00:04:31,780
After this, starvation.

79
00:04:31,847 --> 00:04:35,717
Or we start sending
people out on food runs.

80
00:04:35,784 --> 00:04:37,918
Chief?

81
00:04:37,996 --> 00:04:39,744
Chief Heisel?

82
00:04:44,259 --> 00:04:47,032
- KYLE: How did you get here?
- SUSAN: Is help coming?

83
00:04:47,103 --> 00:04:49,831
Is it over? Are we safe?

84
00:04:49,898 --> 00:04:52,134
Are we?

85
00:04:59,274 --> 00:05:01,207
I'm sorry.

86
00:05:01,209 --> 00:05:02,544
It's just me.

87
00:05:02,611 --> 00:05:05,013
I was hoping my son would be here.

88
00:05:13,288 --> 00:05:16,358
Maybe we go to my office.

89
00:05:18,627 --> 00:05:20,696
It's been hard.

90
00:05:20,762 --> 00:05:23,229
Things haven't exactly been good here.

91
00:05:23,231 --> 00:05:25,005
Things haven't been good anywhere.

92
00:05:25,008 --> 00:05:25,769
You know me.

93
00:05:25,824 --> 00:05:29,237
You know I only try to do
what's best for everybody.

94
00:05:29,271 --> 00:05:31,273
I just want...

95
00:05:31,339 --> 00:05:34,710
I just want people to get along.

96
00:05:34,776 --> 00:05:36,378
You know that, right?

97
00:05:36,445 --> 00:05:39,247
Where is he?

98
00:05:42,417 --> 00:05:45,721
Eve Copeland locked him
up in a storage room.

99
00:05:45,787 --> 00:05:50,759
Then we locked Eve Copeland
and her daughter up with him.

100
00:05:50,826 --> 00:05:53,628
What? Why?

101
00:05:53,695 --> 00:05:57,866
As I said, it's been hard.

102
00:05:57,933 --> 00:06:00,802
The Copeland girl killed a woman.

103
00:06:00,869 --> 00:06:03,413
People want justice.

104
00:06:03,448 --> 00:06:04,706
I just want to help them.

105
00:06:04,773 --> 00:06:06,475
Justice?

106
00:06:06,541 --> 00:06:09,211
No, it... just...

107
00:06:09,277 --> 00:06:11,053
We just want to ask the Copelands

108
00:06:11,088 --> 00:06:14,750
to find another place to be
until this all blows over.

109
00:06:14,816 --> 00:06:18,220
You taking the law into
your own hands, huh?

110
00:06:22,224 --> 00:06:24,826
This is a private mall.

111
00:06:24,893 --> 00:06:28,397
It's not illegal to throw people out.

112
00:06:32,501 --> 00:06:34,503
You've got something there.

113
00:06:38,240 --> 00:06:41,143
When this whole thing is over,

114
00:06:41,243 --> 00:06:44,874
we'll all have done things
we won't be proud of.

115
00:06:44,968 --> 00:06:49,918
But for now, we just gotta get to the when.

116
00:06:52,988 --> 00:06:55,255
Give me my son

117
00:06:55,257 --> 00:06:58,827
and I won't interfere
with how you run your mall.

118
00:07:14,443 --> 00:07:17,647
I don't know what the
fuck we're waiting for.

119
00:07:17,686 --> 00:07:20,949
Can someone tell me what
the fuck we're waiting for?

120
00:07:50,379 --> 00:07:51,913
Aah!

121
00:07:51,980 --> 00:07:53,515
MIA: Kevin!

122
00:07:53,582 --> 00:07:57,552
Kevin, I thought you were...

123
00:07:57,619 --> 00:07:59,554
What the hell?

124
00:08:01,656 --> 00:08:04,273
Adrian said you got shot in the head.

125
00:08:04,312 --> 00:08:05,051
Where is he?

126
00:08:05,086 --> 00:08:07,705
He's, uh, he's looking for your family.

127
00:08:07,752 --> 00:08:09,248
- Shit.
- Wait, wait, wait.

128
00:08:09,283 --> 00:08:11,102
What's going on? What the fuck happened?

129
00:08:12,634 --> 00:08:14,202
Adrian raped my daughter.

130
00:08:14,236 --> 00:08:15,947
What?

131
00:08:15,982 --> 00:08:16,672
When I figured it out,

132
00:08:16,707 --> 00:08:18,845
he knocked me out and left me there.

133
00:08:18,908 --> 00:08:20,633
I've been looking everywhere.

134
00:08:20,668 --> 00:08:21,616
There's a big group down by the fountain,

135
00:08:21,651 --> 00:08:23,602
but no Eve and Alex.

136
00:08:23,657 --> 00:08:25,947
Yeah, I saw that group, too.
They look pretty unstable.

137
00:08:26,014 --> 00:08:28,517
There's nobody down there. I checked.

138
00:08:28,583 --> 00:08:29,551
Okay.

139
00:08:29,618 --> 00:08:30,719
Where's Bryan?

140
00:08:30,786 --> 00:08:31,990
His name is Jonah.

141
00:08:32,045 --> 00:08:34,160
It's... it's a long story.

142
00:08:34,222 --> 00:08:36,716
He left while I was asleep.

143
00:08:36,751 --> 00:08:38,507
He's probably looking for Adrian.

144
00:08:38,585 --> 00:08:40,629
We've gotta find them, both of them.

145
00:08:40,696 --> 00:08:43,916
Look, I'll take one
floor, you take the next.

146
00:08:43,984 --> 00:08:46,568
- Okay.
- We'll meet back in the middle.

147
00:08:46,635 --> 00:08:49,438
I'm glad you're here.

148
00:08:49,504 --> 00:08:51,039
Be careful.

149
00:08:53,508 --> 00:08:57,412
If we know each other,
then tell me who I am.

150
00:08:57,479 --> 00:08:59,182
I'm not the person you want to talk to.

151
00:08:59,237 --> 00:09:00,882
- We need to go.
- Go where?

152
00:09:00,949 --> 00:09:03,385
Arrowhead.

153
00:09:03,452 --> 00:09:05,185
No.

154
00:09:05,187 --> 00:09:07,759
We have to. I'm sorry. I'm
not gonna hurt you, sir.

155
00:09:07,813 --> 00:09:09,847
You just knocked me out.

156
00:09:09,901 --> 00:09:13,195
Dr. Bruin told us to be careful.

157
00:09:13,228 --> 00:09:15,330
She the woman who tortured me?

158
00:09:15,397 --> 00:09:18,300
She's the reason you're alive, sir.

159
00:09:18,367 --> 00:09:20,502
Why do you keep calling me sir?

160
00:09:24,539 --> 00:09:28,477
If I'm your superior, then release me.

161
00:09:31,013 --> 00:09:33,782
I order you to release me.

162
00:09:33,849 --> 00:09:36,251
- Sir.
- I order you, soldier.

163
00:09:55,037 --> 00:09:57,539
You're free, sir.

164
00:10:07,416 --> 00:10:09,451
Connor.

165
00:10:09,518 --> 00:10:12,354
JAY: Dad.

166
00:10:12,421 --> 00:10:13,488
Dad.

167
00:10:17,859 --> 00:10:19,394
Can you get up?

168
00:10:19,461 --> 00:10:21,530
Let's go.

169
00:10:21,596 --> 00:10:23,999
Be careful. Easy.

170
00:10:24,066 --> 00:10:26,676
- All right.
- You have to help us.

171
00:10:26,711 --> 00:10:27,903
They're gonna kill us.

172
00:10:27,969 --> 00:10:30,472
Come on.

173
00:10:30,539 --> 00:10:32,908
EVE: Connor?

174
00:10:32,974 --> 00:10:34,910
Connor?

175
00:10:41,983 --> 00:10:48,357
The boy can go, but you
two need to come with us.

176
00:10:48,423 --> 00:10:52,803
[suspenseful music]

177
00:10:54,153 --> 00:10:55,955
Where are we going?

178
00:10:56,795 --> 00:10:58,229
I found him.

179
00:10:59,633 --> 00:11:02,403
Mrs. Raven?

180
00:11:02,469 --> 00:11:05,673
He's so soft.

181
00:11:10,978 --> 00:11:13,948
Dad, we have to go back for Eve and Alex.

182
00:11:13,983 --> 00:11:16,570
- They're gonna kill them.
- He's so beautiful.

183
00:11:16,605 --> 00:11:18,786
Nature can be so cruel.

184
00:11:20,921 --> 00:11:23,624
Dad?

185
00:11:23,691 --> 00:11:26,193
We can't leave them behind.

186
00:11:26,260 --> 00:11:28,395
We have to.

187
00:11:28,462 --> 00:11:30,364
You'll understand.

188
00:11:30,431 --> 00:11:32,533
Understand what?

189
00:11:32,599 --> 00:11:34,902
Just trust me.

190
00:11:34,969 --> 00:11:36,737
Come, Benedict.

191
00:11:41,842 --> 00:11:43,978
Where are we going?

192
00:11:46,113 --> 00:11:47,581
It's okay, son.

193
00:11:47,648 --> 00:11:51,085
Everything is going to be okay now.

194
00:11:51,151 --> 00:11:52,619
Come.

195
00:12:05,032 --> 00:12:07,001
Do you know what's happening out there?

196
00:12:07,067 --> 00:12:09,136
It's the Black Spring.

197
00:12:09,203 --> 00:12:11,672
Black Spring? What's that?

198
00:12:11,739 --> 00:12:14,241
I'll show you.

199
00:12:14,308 --> 00:12:15,609
Show me what?

200
00:12:15,676 --> 00:12:17,287
How to survive it.

201
00:12:17,342 --> 00:12:19,728
We should go back, get Eve and Alex.

202
00:12:19,775 --> 00:12:22,883
Dad, we should help them.

203
00:12:22,916 --> 00:12:24,623
We can't.

204
00:12:24,701 --> 00:12:26,086
Why?

205
00:12:26,153 --> 00:12:27,988
CONNOR: We just can't.

206
00:12:28,055 --> 00:12:30,324
You're gonna have to trust me.

207
00:12:30,391 --> 00:12:33,594
Do you trust me?

208
00:12:35,663 --> 00:12:38,299
Yeah, of course.

209
00:12:38,365 --> 00:12:40,367
I love you.

210
00:12:40,434 --> 00:12:42,102
You know that?

211
00:12:44,605 --> 00:12:45,973
Yeah.

212
00:12:46,040 --> 00:12:48,275
And I am so proud of you.

213
00:12:48,342 --> 00:12:51,979
You are the best thing
that ever happened to me.

214
00:12:52,046 --> 00:12:54,114
Dad, what's going on?

215
00:12:54,181 --> 00:12:56,050
You need to come here now.

216
00:12:56,116 --> 00:12:58,319
You trust me, right?

217
00:13:03,125 --> 00:13:04,826
What are we doing?

218
00:13:08,462 --> 00:13:11,865
I, um, I can't explain it.

219
00:13:11,932 --> 00:13:14,234
It'll sound weird.

220
00:13:14,301 --> 00:13:17,071
You just have to trust me.

221
00:13:17,137 --> 00:13:19,173
You're very brave.

222
00:13:19,239 --> 00:13:21,208
Dad?

223
00:13:25,913 --> 00:13:28,148
I'm sorry.

224
00:13:28,215 --> 00:13:30,250
I am so sorry.

225
00:13:30,317 --> 00:13:32,553
I made you.

226
00:13:32,619 --> 00:13:35,316
What? What are you talking about?

227
00:13:35,379 --> 00:13:37,925
It's not your fault.

228
00:13:37,992 --> 00:13:40,094
It's not your fault. It's not your fault.

229
00:13:40,160 --> 00:13:41,762
What's she doing?

230
00:13:41,895 --> 00:13:44,164
No. Dad, no.

231
00:13:44,231 --> 00:13:48,669
No! No, Dad.

232
00:13:48,736 --> 00:13:50,537
Dad!

233
00:13:50,604 --> 00:13:52,206
Dad, let me in!

234
00:13:52,272 --> 00:13:57,244
Dad, what are you doing?

235
00:13:57,311 --> 00:13:59,377
I'm your son! What are you doing?

236
00:13:59,412 --> 00:14:00,614
Let me in!

237
00:14:01,649 --> 00:14:04,451
Dad!

238
00:14:35,349 --> 00:14:37,618
It's still there.

239
00:14:37,685 --> 00:14:39,453
It will disappear.

240
00:14:39,520 --> 00:14:42,423
He may still be alive.

241
00:14:42,489 --> 00:14:44,625
It may not have taken him yet.

242
00:14:44,692 --> 00:14:46,894
Come.

243
00:15:02,409 --> 00:15:05,846
[crying]

244
00:15:16,323 --> 00:15:19,960
Losing someone you love
can be so beautiful.

245
00:15:20,027 --> 00:15:21,862
You'll see.

246
00:15:24,932 --> 00:15:27,935
[crying]

247
00:15:31,872 --> 00:15:34,208
I'm proud of you.

248
00:15:34,274 --> 00:15:36,977
I'm so proud of you.

249
00:15:40,080 --> 00:15:42,883
It'll be over soon.

250
00:15:42,916 --> 00:15:46,520
It will all be over.

251
00:15:54,962 --> 00:15:56,951
- KIMMI: Why are you doing this?
- KYLE: We have to.

252
00:15:56,986 --> 00:15:58,073
We're just making sure

253
00:15:58,136 --> 00:16:00,170
this is a safe space for everyone to be

254
00:16:00,205 --> 00:16:02,883
- while we wait this out.
- You're insane.

255
00:16:02,953 --> 00:16:04,663
We haven't hurt anyone.

256
00:16:04,718 --> 00:16:06,173
We kept to ourselves.

257
00:16:06,240 --> 00:16:08,891
- Tell that to Shelley.
- I didn't do anything.

258
00:16:08,954 --> 00:16:10,796
- I saw you.
- That's a lie.

259
00:16:10,859 --> 00:16:12,113
And how do we trust you?

260
00:16:12,175 --> 00:16:14,848
Shelley saw you making out with
the boy you accused of rape.

261
00:16:14,882 --> 00:16:16,818
- Maybe she didn't...
- What?

262
00:16:16,853 --> 00:16:18,062
Maybe she didn't what?

263
00:16:18,097 --> 00:16:19,353
Didn't mean it?

264
00:16:19,420 --> 00:16:23,622
Is that why you lied about
surviving in the mist?

265
00:16:23,692 --> 00:16:26,060
You know what? Maybe Shelley was right.

266
00:16:26,126 --> 00:16:29,632
Maybe you are a part of it.
That's why you killed her.

267
00:16:29,687 --> 00:16:30,722
We don't have to stand for this.

268
00:16:30,757 --> 00:16:32,561
- We're not your prisoners.
- Stop.

269
00:16:32,596 --> 00:16:35,269
I have fought too many assholes like you

270
00:16:35,336 --> 00:16:38,041
to give up that power freely. Come on.

271
00:16:38,104 --> 00:16:40,074
- Aah!
- Oh! Oh!

272
00:16:43,877 --> 00:16:46,947
We have the right to defend ourselves.

273
00:16:49,183 --> 00:16:51,552
Anybody else disagree?

274
00:16:53,287 --> 00:16:56,390
Yeah, I thought so.

275
00:17:17,645 --> 00:17:21,448
[music]

276
00:17:34,862 --> 00:17:37,515
How could you do it?

277
00:17:37,577 --> 00:17:40,334
How could you do that to her?

278
00:17:40,401 --> 00:17:42,827
Uh! No!

279
00:17:42,862 --> 00:17:43,871
No!

280
00:17:48,075 --> 00:17:49,312
[grunting]

281
00:17:49,351 --> 00:17:52,005
Get off me please, please,
please, please, please,

282
00:17:52,052 --> 00:17:53,653
please, please, please stop. Please.

283
00:17:53,692 --> 00:17:54,454
No.

284
00:17:55,716 --> 00:17:59,320
I know where they are.

285
00:17:59,386 --> 00:18:02,256
I know where they're keeping Eve and Alex.

286
00:18:06,593 --> 00:18:08,462
We need to go.

287
00:18:08,529 --> 00:18:10,527
I can't.

288
00:18:10,597 --> 00:18:12,032
Sir, we have to.

289
00:18:12,099 --> 00:18:15,035
- I can't leave my friends.
- What friends?

290
00:18:15,102 --> 00:18:18,172
The people I met who helped me get here.

291
00:18:18,238 --> 00:18:19,448
None of that matters now.

292
00:18:19,503 --> 00:18:20,874
It matters to me.

293
00:18:20,941 --> 00:18:23,490
These are people you've
known for five days.

294
00:18:23,563 --> 00:18:25,391
I've known you for eight years.

295
00:18:28,349 --> 00:18:30,184
You have no idea, do you?

296
00:18:30,250 --> 00:18:32,686
You have no idea who you are.

297
00:18:35,089 --> 00:18:37,746
Don't you want to know who you are?

298
00:18:37,824 --> 00:18:39,293
- Tell me.
- Come with me.

299
00:18:39,360 --> 00:18:40,894
- I need to bring them.
- You can't.

300
00:18:40,961 --> 00:18:42,563
They can never know. You understand?

301
00:18:42,630 --> 00:18:45,165
If you care for them, they can never know.

302
00:18:45,232 --> 00:18:47,534
Jonah?

303
00:18:47,601 --> 00:18:49,570
Where have you been?

304
00:18:49,637 --> 00:18:53,040
I, um...

305
00:18:53,107 --> 00:18:54,875
Who's this?

306
00:18:54,942 --> 00:18:57,177
I'm a friend of Jonah's.

307
00:19:05,653 --> 00:19:08,689
Kevin's, uh...

308
00:19:08,822 --> 00:19:10,524
Kevin's here.

309
00:19:10,591 --> 00:19:14,428
He's alive and he needs our help.

310
00:19:14,495 --> 00:19:16,336
Wow, um...

311
00:19:16,406 --> 00:19:18,065
WES: Jonah.

312
00:19:18,132 --> 00:19:20,644
JONAH: This guy can help
me find out who I am.

313
00:19:22,136 --> 00:19:23,737
I need to go.

314
00:19:23,837 --> 00:19:25,324
Wait. Go where?

315
00:19:25,402 --> 00:19:27,839
I'm sorry.

316
00:19:27,841 --> 00:19:31,597
What do you mean? Jonah, go where?

317
00:19:31,632 --> 00:19:32,880
I have to go with him.

318
00:19:32,946 --> 00:19:35,616
What the fuck are you talking about?

319
00:19:35,683 --> 00:19:36,848
You have to go.

320
00:19:36,850 --> 00:19:38,523
Please.

321
00:19:39,853 --> 00:19:42,643
But we... Let's just help him.

322
00:19:42,678 --> 00:19:44,258
Then we can all go together.

323
00:19:44,325 --> 00:19:46,193
I'm afraid that's not possible.

324
00:19:46,260 --> 00:19:50,364
- JONAH: I'll find you.
- Jonah, please.

325
00:19:50,431 --> 00:19:52,533
We have to go, sir.

326
00:19:52,599 --> 00:19:55,803
Please.

327
00:20:01,275 --> 00:20:04,078
Don't leave me.

328
00:20:08,315 --> 00:20:10,918
I'm sorry.

329
00:20:20,561 --> 00:20:23,130
She didn't do anything.

330
00:20:23,197 --> 00:20:24,996
Meaning you have?

331
00:20:25,031 --> 00:20:26,066
No.

332
00:20:26,133 --> 00:20:27,635
You killed her.

333
00:20:27,701 --> 00:20:29,057
Part of your rules as well?

334
00:20:29,092 --> 00:20:30,838
She was going to break the rules,

335
00:20:30,904 --> 00:20:32,382
and the rules are clear.

336
00:20:32,453 --> 00:20:34,174
Whoever endangers the group

337
00:20:34,241 --> 00:20:36,441
has to leave the mall.

338
00:20:36,476 --> 00:20:38,412
That means Alex must go.

339
00:20:38,479 --> 00:20:42,142
- MAN: She has to.
- I'm not letting you throw her out.

340
00:20:42,235 --> 00:20:44,104
Well, then you're leaving with her.

341
00:20:51,592 --> 00:20:54,400
I'm sorry, but those are the rules.

342
00:20:54,447 --> 00:20:56,031
No, please, Gus.

343
00:21:07,041 --> 00:21:08,976
Con-Connor.

344
00:21:09,043 --> 00:21:10,642
You have to help us. You have to stop this.

345
00:21:10,677 --> 00:21:12,313
GUS: No, Connor's staying out of this.

346
00:21:12,379 --> 00:21:13,806
I can throw out anybody I want.

347
00:21:13,841 --> 00:21:15,949
- Please, you have to.
- He doesn't have to do anything.

348
00:21:16,016 --> 00:21:17,117
Yes, he does.

349
00:21:17,184 --> 00:21:18,519
Connor?

350
00:21:26,060 --> 00:21:28,062
I'm so sorry.

351
00:21:28,128 --> 00:21:30,331
Please forgive me.

352
00:21:30,397 --> 00:21:32,066
For what?

353
00:21:37,237 --> 00:21:40,574
I was already pregnant when I met Kevin.

354
00:21:40,641 --> 00:21:44,350
He didn't want to know who it was.

355
00:21:44,385 --> 00:21:47,247
He thought it was better that way.

356
00:21:47,314 --> 00:21:49,631
I'm so sorry.

357
00:21:49,709 --> 00:21:51,185
What?

358
00:21:53,754 --> 00:21:57,591
EVE: There's a reason
you couldn't be with Jay.

359
00:22:02,396 --> 00:22:04,565
You understand?

360
00:22:18,445 --> 00:22:21,076
After everything else they've lied about,

361
00:22:21,154 --> 00:22:23,287
you expect us to trust this?

362
00:22:24,326 --> 00:22:25,992
MAN: Connor?

363
00:22:27,721 --> 00:22:29,056
She's lying.

364
00:22:30,668 --> 00:22:32,620
Mrs. Carmody was right.

365
00:22:32,706 --> 00:22:34,542
These people will do anything.

366
00:22:34,596 --> 00:22:35,829
She's sick.

367
00:22:35,896 --> 00:22:37,531
You're a sick person.

368
00:22:37,598 --> 00:22:38,999
You would have said something before.

369
00:22:39,066 --> 00:22:40,868
You would have said something.

370
00:22:40,934 --> 00:22:42,769
Mom?

371
00:22:42,836 --> 00:22:45,372
You love Dad so much,

372
00:22:45,439 --> 00:22:47,439
we couldn't take it away from you.

373
00:22:47,502 --> 00:22:49,261
There's no place in
here for people like you.

374
00:22:49,331 --> 00:22:50,608
She always was a whore.

375
00:22:50,679 --> 00:22:51,785
The girl fucked her brother.

376
00:22:51,847 --> 00:22:53,909
- I didn't.
- Now he didn't do it?

377
00:22:53,979 --> 00:22:56,069
- Which is it?
- You disgust me.

378
00:22:56,132 --> 00:22:57,994
You're freaks, both of you.

379
00:22:58,049 --> 00:23:00,440
You've all lost your fucking mind.

380
00:23:02,256 --> 00:23:04,625
It's time.

381
00:23:06,193 --> 00:23:09,463
No.

382
00:23:09,530 --> 00:23:13,261
EVE: Connor, please.
Please, she's your daughter.

383
00:23:14,468 --> 00:23:16,056
Where are they?

384
00:23:16,119 --> 00:23:18,805
Let me go and I'll tell you.

385
00:23:18,872 --> 00:23:22,075
I don't even know if I'm
going to let you live.

386
00:23:22,142 --> 00:23:24,111
You wouldn't.

387
00:23:25,779 --> 00:23:30,317
Remember what I told
Nash in the psych ward?

388
00:23:30,417 --> 00:23:32,786
I want to see you suffer.

389
00:23:32,853 --> 00:23:35,622
Uh!

390
00:23:35,689 --> 00:23:37,624
And I want you to learn...

391
00:23:37,691 --> 00:23:39,726
Uh!

392
00:23:39,793 --> 00:23:42,345
That there will be no salvation.

393
00:23:42,400 --> 00:23:44,131
Uh!

394
00:23:45,966 --> 00:23:49,078
But if you tell me where my family is,

395
00:23:49,148 --> 00:23:51,505
then maybe, just maybe,

396
00:23:51,572 --> 00:23:53,900
I will stop before you die.

397
00:23:56,810 --> 00:23:59,029
I love her.

398
00:23:59,099 --> 00:24:01,248
Uh!

399
00:24:04,918 --> 00:24:06,787
Please stop.

400
00:24:06,854 --> 00:24:10,090
Last chance.

401
00:24:10,157 --> 00:24:12,526
Where's my family?

402
00:24:12,593 --> 00:24:13,861
They're gonna kill her.

403
00:24:13,927 --> 00:24:16,980
They're gonna throw her out of the mall.

404
00:24:17,035 --> 00:24:19,183
Eve, too.

405
00:24:19,253 --> 00:24:21,065
- You need to hurry.
- Kevin?

406
00:24:21,535 --> 00:24:23,403
Uh!

407
00:24:23,437 --> 00:24:27,046
Kevin? We need to go.

408
00:24:27,089 --> 00:24:27,840
KEVIN: Where's Jonah?

409
00:24:27,875 --> 00:24:29,038
- MIA: He left.
- KEVIN: What?

410
00:24:29,101 --> 00:24:31,547
With another soldier. He's gone.

411
00:24:31,582 --> 00:24:33,480
Let's just go.

412
00:24:33,515 --> 00:24:35,048
Not quite yet.

413
00:24:35,115 --> 00:24:37,184
[moans]

414
00:24:37,251 --> 00:24:39,152
Kevin, here!

415
00:24:39,219 --> 00:24:40,721
Come on.

416
00:24:40,787 --> 00:24:43,296
Adrian doesn't matter anymore.

417
00:24:43,358 --> 00:24:45,058
Come on. Let's go.

418
00:24:45,125 --> 00:24:47,427
Let's go!

419
00:24:47,461 --> 00:24:49,897
- Get off me.
- Come on.

420
00:24:49,963 --> 00:24:51,465
No!

421
00:24:51,532 --> 00:24:54,968
Please let us walk out together.

422
00:24:55,035 --> 00:24:57,871
Okay.

423
00:24:57,938 --> 00:24:59,973
Then get to walking then.

424
00:25:04,368 --> 00:25:06,304
KEVIN: Stop!

425
00:25:07,495 --> 00:25:08,630
Eve!

426
00:25:10,600 --> 00:25:11,685
Kevin?

427
00:25:11,752 --> 00:25:14,054
[crying]

428
00:25:17,958 --> 00:25:19,259
Dad?

429
00:25:23,997 --> 00:25:25,399
Are you okay?

430
00:25:31,438 --> 00:25:33,707
They want to kick us out.

431
00:25:38,412 --> 00:25:40,013
Aah!

432
00:25:58,932 --> 00:26:01,535
Get out. Now!

433
00:26:01,602 --> 00:26:03,637
- You're leaving, too.
- Don't do this!

434
00:26:03,672 --> 00:26:06,206
I am not gonna let this
place fall into chaos,

435
00:26:06,273 --> 00:26:07,982
and chaos is what you bring.

436
00:26:08,055 --> 00:26:10,544
Please. This is insane.

437
00:26:10,611 --> 00:26:11,990
You attacked us.

438
00:26:15,983 --> 00:26:19,453
Please.

439
00:26:19,520 --> 00:26:21,788
This is our one chance at safety.

440
00:26:21,855 --> 00:26:24,424
I know you.

441
00:26:24,491 --> 00:26:26,091
I know you.

442
00:26:26,146 --> 00:26:28,889
Please. Please!

443
00:26:31,164 --> 00:26:33,383
Just let my family stay.

444
00:26:33,430 --> 00:26:34,693
Family?

445
00:26:34,763 --> 00:26:36,770
What...

446
00:26:36,837 --> 00:26:39,664
Your wife screwed the chief.

447
00:26:39,719 --> 00:26:41,642
Your daughter isn't really yours.

448
00:26:41,708 --> 00:26:44,011
Not much of a family, huh?

449
00:26:46,547 --> 00:26:48,782
MAN: Bunch of bastards!

450
00:26:58,859 --> 00:27:01,295
Fuck you.

451
00:27:01,373 --> 00:27:02,441
WOMAN: Get out.

452
00:27:02,563 --> 00:27:05,699
Fuck all of you.

453
00:27:05,766 --> 00:27:07,201
Get the door.

454
00:27:08,869 --> 00:27:09,870
Move.

455
00:27:13,874 --> 00:27:17,177
[music]

456
00:27:24,282 --> 00:27:26,184
WOMAN: Hurry up!

457
00:27:31,692 --> 00:27:33,894
Run.

458
00:27:37,311 --> 00:27:38,230
Follow me.

459
00:27:38,265 --> 00:27:39,667
GUS: Get the door! Get the door!

460
00:27:44,470 --> 00:27:46,325
- KEVIN: Stay with me.
- EVE: Where?

461
00:27:46,360 --> 00:27:48,338
We have a car. It's at
the back of the mall.

462
00:27:48,373 --> 00:27:49,811
- I can't see.
- Just run.

463
00:27:49,876 --> 00:27:51,444
KEVIN: Follow the wall down to the corner.

464
00:28:04,581 --> 00:28:07,318
[groaning]

465
00:28:11,288 --> 00:28:14,224
[gasping]

466
00:28:18,929 --> 00:28:21,532
[gasping]

467
00:28:26,270 --> 00:28:28,024
KEVIN: Alex?

468
00:28:28,071 --> 00:28:29,506
Alex?

469
00:28:31,575 --> 00:28:33,577
Alex?

470
00:28:33,644 --> 00:28:35,913
Go find the truck and bring it here.

471
00:28:35,980 --> 00:28:39,183
[gasping]

472
00:28:42,920 --> 00:28:44,288
She can't be gone.

473
00:28:44,355 --> 00:28:46,365
She can't just disappear. Alex? Alex?

474
00:28:46,400 --> 00:28:47,191
Alex?

475
00:28:47,258 --> 00:28:50,628
[gasping]

476
00:28:59,770 --> 00:29:01,282
Alex?

477
00:29:01,355 --> 00:29:02,601
Alex?

478
00:29:09,313 --> 00:29:10,614
Hey.

479
00:29:13,550 --> 00:29:14,985
Alex?

480
00:29:15,052 --> 00:29:18,204
Let's go. Let's go.

481
00:29:18,239 --> 00:29:20,425
[Jay gasping]

482
00:29:20,460 --> 00:29:21,945
- Jay!
- Alex!

483
00:29:22,008 --> 00:29:24,261
No!

484
00:29:24,328 --> 00:29:26,130
[gasping]

485
00:29:26,196 --> 00:29:27,099
- Get in the car.
- Alex!

486
00:29:27,145 --> 00:29:28,471
Let go! Jay!

487
00:29:28,510 --> 00:29:30,567
- Get in the car.
- Let go!

488
00:29:30,634 --> 00:29:31,769
- Alex, get in the car.
- Jay!

489
00:29:31,835 --> 00:29:35,306
[groaning]

490
00:29:35,372 --> 00:29:37,198
- Get in the car.
- Get in the car.

491
00:29:37,233 --> 00:29:38,198
No!

492
00:29:38,261 --> 00:29:40,077
- Eve, get in the car.
- No!

493
00:29:48,227 --> 00:29:50,230
How far away is Arrowhead?

494
00:29:50,265 --> 00:29:51,730
It's two, three hours.

495
00:29:53,199 --> 00:29:55,835
You're doing the right thing, sir.

496
00:29:55,901 --> 00:29:59,238
Do you know what it is out there?

497
00:29:59,371 --> 00:30:01,740
Where it came from?

498
00:30:01,857 --> 00:30:04,927
Do I know?

499
00:30:05,019 --> 00:30:07,721
We should leave.

500
00:30:07,788 --> 00:30:10,124
The doctor will have answers for you.

501
00:30:18,899 --> 00:30:22,236
[suspenseful music]

502
00:30:28,509 --> 00:30:30,778
[crying]

503
00:30:35,416 --> 00:30:36,850
Jesus.

504
00:30:42,756 --> 00:30:44,792
He didn't do it.

505
00:30:48,729 --> 00:30:51,865
I want you to know that.

506
00:30:54,902 --> 00:30:56,770
Who was it then?

507
00:30:56,837 --> 00:31:00,441
Who told you I was dead?

508
00:31:07,915 --> 00:31:11,819
Let's go. I don't want to be here.

509
00:31:11,886 --> 00:31:13,988
I don't want to be near these people.

510
00:31:18,459 --> 00:31:20,561
[engine]

511
00:31:23,230 --> 00:31:27,657
[suspenseful music]

512
00:31:37,301 --> 00:31:39,236
It'll be gone soon.

513
00:31:39,303 --> 00:31:43,707
Once they are dead, it'll be gone.

514
00:31:45,075 --> 00:31:46,910
Why would it be gone?

515
00:31:46,977 --> 00:31:50,439
Because I...

516
00:31:50,517 --> 00:31:52,732
I am Nature's messenger.

517
00:31:52,767 --> 00:31:55,536
She chose me,

518
00:31:55,603 --> 00:31:58,806
first through the moth and then the spider.

519
00:31:58,873 --> 00:32:01,034
The Black Spring is here.

520
00:32:01,112 --> 00:32:03,482
She needed someone, someone who...

521
00:32:05,104 --> 00:32:06,390
She showed me...

522
00:32:06,425 --> 00:32:07,771
- MAN: What did she say?
- [chatter]

523
00:32:07,834 --> 00:32:10,076
NATHALIE: ...showed me what had to be done.

524
00:32:10,143 --> 00:32:12,768
A Forest Ranger I...

525
00:32:12,838 --> 00:32:16,048
A Forest Ranger I knew, he...

526
00:32:17,617 --> 00:32:19,051
He found a bear,

527
00:32:19,118 --> 00:32:22,889
a mother bear who had given birth.

528
00:32:22,955 --> 00:32:25,925
She'd given birth to three cubs.

529
00:32:28,127 --> 00:32:32,120
The rangers kept an eye on the cubs

530
00:32:32,206 --> 00:32:35,268
to make sure they wouldn't
be injured by other predators.

531
00:32:53,286 --> 00:32:56,856
[suspenseful music]

532
00:33:07,099 --> 00:33:09,902
[truck stops]

533
00:33:15,182 --> 00:33:16,149
What?

534
00:33:19,078 --> 00:33:20,413
Dad?

535
00:33:20,480 --> 00:33:22,381
What are you doing?

536
00:33:24,717 --> 00:33:27,253
Kevin, no.

537
00:33:30,786 --> 00:33:33,285
- WOMAN: What is he doing?
- Put your seatbelts on.

538
00:33:38,731 --> 00:33:39,899
What are you doing?

539
00:33:39,953 --> 00:33:41,667
Put your seatbelts on now.

540
00:33:57,083 --> 00:33:58,751
No, no, no, no, no, no!

541
00:33:58,818 --> 00:34:00,052
[shouting]

542
00:34:02,388 --> 00:34:03,322
[screaming]

543
00:34:11,063 --> 00:34:12,534
[engine revving]

544
00:34:12,569 --> 00:34:14,577
- It's stuck. It's stuck.
- Shit.

545
00:34:14,612 --> 00:34:17,703
[distant screaming]

546
00:34:30,904 --> 00:34:32,185
Stop!

547
00:34:32,251 --> 00:34:34,520
- Alex. Alex!
- Please.

548
00:34:34,587 --> 00:34:36,489
- KEVIN: Alex!
- ALEX: Come with us.

549
00:34:36,556 --> 00:34:38,524
Please.

550
00:34:42,109 --> 00:34:44,953
Come on.

551
00:34:45,027 --> 00:34:46,562
Come on, get in the car. Come on.

552
00:34:54,874 --> 00:34:56,642
[echoing screams]

553
00:34:56,709 --> 00:35:01,047
[music]

554
00:35:01,105 --> 00:35:04,075
[echoing screams continue]

555
00:35:07,013 --> 00:35:11,221
♪ Just a perfect day ♪

556
00:35:11,290 --> 00:35:15,522
♪ Drink sangria in the park ♪

557
00:35:15,653 --> 00:35:18,317
♪ And then later ♪

558
00:35:18,395 --> 00:35:22,460
- ♪ when it gets dark... ♪
- All of you died?

559
00:35:22,527 --> 00:35:24,729
It's all right.

560
00:35:27,211 --> 00:35:31,465
- ♪ Just a perfect day ♪
- We all die.

561
00:35:31,500 --> 00:35:35,387
- I'm dying.
- ♪ Feed animals in the zoo ♪

562
00:35:35,773 --> 00:35:38,448
♪ Then later ♪

563
00:35:38,503 --> 00:35:42,947
♪ A movie, too and then home... ♪

564
00:35:45,213 --> 00:35:50,456
♪ Oh, it's such a perfect day ♪

565
00:35:50,491 --> 00:35:55,482
♪ I'm glad I spent it with you ♪

566
00:35:56,549 --> 00:35:59,248
♪ Oh, such a perfect day ♪

567
00:35:59,283 --> 00:36:04,169
- ♪ You just keep me hanging on ♪
- MAN: Aah!

568
00:36:04,231 --> 00:36:10,404
♪ You just keep me hanging on ♪

569
00:36:10,450 --> 00:36:12,656
Is he alive?

570
00:36:12,695 --> 00:36:14,078
BENEDICT: I'm sorry, sweetie.

571
00:36:14,145 --> 00:36:17,785
I'm so sorry.

572
00:36:17,824 --> 00:36:18,886
Oh...

573
00:36:18,925 --> 00:36:23,821
♪ Problems all left alone ♪

574
00:36:23,888 --> 00:36:26,478
♪ Weekenders on our own ♪

575
00:36:26,548 --> 00:36:29,160
She took him.

576
00:36:29,227 --> 00:36:31,529
[hissing]

577
00:36:34,442 --> 00:36:37,468
♪ Just a perfect day ♪

578
00:36:37,535 --> 00:36:42,208
♪ You made me forget myself ♪

579
00:36:43,408 --> 00:36:47,241
♪ I thought I was someone else ♪

580
00:36:47,303 --> 00:36:51,115
♪ Someone good ♪

581
00:36:51,182 --> 00:36:57,375
- ♪ Oh, it's such a perfect day ♪
- [pounding on door]

582
00:36:57,430 --> 00:37:02,826
- [distant screaming]
- ♪ I'm glad I spent it with you ♪

583
00:37:03,027 --> 00:37:06,235
♪ Oh, such a perfect day ♪

584
00:37:06,282 --> 00:37:11,202
- [screaming]
- ♪ You just keep me hanging on ♪

585
00:37:11,269 --> 00:37:17,002
♪ You just keep me hanging on ♪

586
00:37:20,745 --> 00:37:24,582
[music]

587
00:37:32,149 --> 00:37:35,426
- Jay saved my life.
- ♪ You're going to reap... ♪

588
00:37:35,493 --> 00:37:38,329
- Just now, before we left.
- ♪ ...just what you sow ♪

589
00:37:42,964 --> 00:37:47,371
- He was innocent.
- ♪ ...just what you sow ♪

590
00:37:52,286 --> 00:37:54,728
♪ You're going to reap ♪

591
00:37:54,763 --> 00:37:57,348
♪ just what you sow ♪

592
00:37:57,415 --> 00:38:01,452
[sobs]

593
00:38:01,519 --> 00:38:04,321
♪ You're going to reap ♪

594
00:38:04,399 --> 00:38:07,971
♪ just what you sow ♪

595
00:38:09,310 --> 00:38:12,747
[music]

596
00:38:52,587 --> 00:38:56,357
Bangor is right, Inland is
left. We could do either.

597
00:38:56,424 --> 00:38:58,259
Let's get out of this town.

598
00:38:58,326 --> 00:39:01,229
It can't be all over.

599
00:39:01,295 --> 00:39:02,697
It can't be everywhere.

600
00:39:02,763 --> 00:39:03,996
How do you know?

601
00:39:03,998 --> 00:39:06,767
I don't.

602
00:39:14,207 --> 00:39:15,241
Dad.

603
00:39:16,277 --> 00:39:18,430
What's that?

604
00:39:18,497 --> 00:39:20,446
Something's coming. It's getting closer.

605
00:39:20,481 --> 00:39:23,751
[train approaching]

606
00:39:23,818 --> 00:39:25,586
It's a train.

607
00:39:29,957 --> 00:39:32,358
- EVE: Where are you going?
- KEVIN: To the station.

608
00:39:32,405 --> 00:39:34,128
I can get there before it does.

609
00:39:34,195 --> 00:39:35,596
EVE: What if doesn't stop?

610
00:39:35,663 --> 00:39:38,566
KEVIN: We have to try.

611
00:39:38,633 --> 00:39:40,501
Dad, hurry.

612
00:39:44,071 --> 00:39:46,641
That's it.

613
00:39:57,852 --> 00:40:00,518
You sure this is a good idea?

614
00:40:00,573 --> 00:40:01,720
They can save us.

615
00:40:01,790 --> 00:40:03,228
Maybe it can get us out of here.

616
00:40:03,283 --> 00:40:04,700
With your wife.

617
00:40:04,747 --> 00:40:06,027
Turn off the light.

618
00:40:07,895 --> 00:40:11,165
[train screeches to a stop]

619
00:40:32,186 --> 00:40:35,342
What do we do?

620
00:40:35,443 --> 00:40:36,491
What are you crazy? We run.

621
00:40:36,554 --> 00:40:39,160
- They can rescue us.
- Wait!

622
00:40:39,227 --> 00:40:41,629
[doors opening]

623
00:40:43,197 --> 00:40:47,062
- Uh! Uh!
- Aah!

624
00:40:47,116 --> 00:40:49,704
- MEN: Get off!
- MAN: No!

625
00:40:51,272 --> 00:40:53,207
- [screaming]
- MEN: Get off! Get off!

626
00:40:54,442 --> 00:40:56,477
What are they doing?

627
00:40:59,080 --> 00:41:02,984
[screaming]

628
00:41:10,491 --> 00:41:12,660
They're feeding it.

629
00:41:12,695 --> 00:41:15,919
Subtitles by explosiveskull

