1
00:04:32,160 --> 00:04:33,149
Daeng.

2
00:04:33,200 --> 00:04:35,111
I'm going to buy some cigarettes,

3
00:04:35,160 --> 00:04:36,229
do you want anything?

4
00:04:37,240 --> 00:04:38,229
- No, thanks.

5
00:05:55,240 --> 00:06:02,032
"Noy" tel. 081-6828965

6
00:06:42,080 --> 00:06:44,036
They seem pretty drunk eh?

7
00:07:22,240 --> 00:07:24,071
Hello there.

8
00:07:26,000 --> 00:07:28,036
Could I borrow a lighter?

9
00:07:35,240 --> 00:07:38,038
Could I get a cigarette too?

10
00:07:46,200 --> 00:07:48,191
How does it work?

11
00:08:31,240 --> 00:08:33,151
What kind of music do you listen to?

12
00:08:35,200 --> 00:08:38,158
- I listen to all kinds...

13
00:08:38,200 --> 00:08:40,077
- But, not very often.

14
00:08:44,120 --> 00:08:46,111
Your hands look like a musicians.

15
00:09:01,040 --> 00:09:03,031
Do you like "Job"?

16
00:09:03,200 --> 00:09:05,031
- Who?

17
00:09:07,160 --> 00:09:09,116
Do you like "Job Bunjob?'

18
00:09:09,240 --> 00:09:10,229
- I never heard of him.

19
00:10:46,080 --> 00:10:47,115
- How old are you?

20
00:10:53,040 --> 00:10:55,031
I'll be 19 tomorrow...

21
00:10:55,080 --> 00:10:56,195
Why do you ask?

22
00:10:56,240 --> 00:10:58,993
I listen to old music, is that it?

23
00:11:00,040 --> 00:11:03,077
- No, but it doesn't fit your age.

24
00:11:05,160 --> 00:11:08,118
I don't listen to "pop"
it's retarded.

25
00:11:08,160 --> 00:11:10,071
- what do you mean, 'retarded'?

26
00:11:18,200 --> 00:11:20,156
coffee please.

27
00:11:20,160 --> 00:11:22,071
- make it two.

28
00:11:25,120 --> 00:11:28,112
- What are you doing sitting here
at 5:30 in the morning?

29
00:11:28,200 --> 00:11:31,192
Waiting for my Mom,
we are meeting here.

30
00:11:32,080 --> 00:11:33,229
- Your from Bangkok, huh?

31
00:11:34,000 --> 00:11:35,035
No.

32
00:11:35,080 --> 00:11:36,195
I live in Phuket.

33
00:11:36,240 --> 00:11:39,118
But Mom will be here at 10:30.

34
00:11:39,240 --> 00:11:41,231
- So your Mom lives in Bangkok?

35
00:11:42,120 --> 00:11:43,155
Nope.

36
00:11:43,200 --> 00:11:45,031
Stockholm.

37
00:11:45,040 --> 00:11:47,190
- Ah Germany...

38
00:11:48,160 --> 00:11:50,230
Sweden!

39
00:11:54,120 --> 00:11:56,190
What about you? You live in Bangkok?

40
00:11:56,200 --> 00:11:59,112
- No, I'm from Phuket,

41
00:11:59,160 --> 00:12:02,118
but I went to America more
than 10 years ago.

42
00:12:04,120 --> 00:12:06,076
And your just getting back now or what?

43
00:12:06,160 --> 00:12:08,071
- I came back for a funeral.

44
00:12:09,240 --> 00:12:12,118
Why do you have to stay in a hotel?

45
00:12:12,200 --> 00:12:14,031
- I don't have a house in Bangkok.

46
00:14:32,120 --> 00:14:35,032
Hey...hey.

47
00:14:39,000 --> 00:14:41,116
Why don't you go freshen up in my room,

48
00:14:42,080 --> 00:14:44,036
there's time to take a nap.

49
00:14:44,080 --> 00:14:46,150
Your mom isn't going to be here until 10.

50
00:14:49,200 --> 00:14:51,077
coming?

51
00:15:02,000 --> 00:15:03,194
Check please.

52
00:15:11,080 --> 00:15:13,196
- Why don't you bring your boyfriend, too?

53
00:15:16,040 --> 00:15:18,156
What makes you think this guy
is my boyfriend?

54
00:17:22,040 --> 00:17:23,075
Hello.

55
00:17:26,040 --> 00:17:27,189
What's your name?

56
00:17:28,120 --> 00:17:29,109
- My name is Wit.

57
00:17:29,160 --> 00:17:31,071
My name is Ploy.

58
00:17:31,240 --> 00:17:32,992
- Nice to meet you.

59
00:17:33,160 --> 00:17:34,991
Hey.

60
00:17:50,160 --> 00:17:53,118
Are you Wit's girlfriend?

61
00:17:53,160 --> 00:17:54,149
- Wife.

62
00:17:55,240 --> 00:17:56,229
Hello.

63
00:17:57,040 --> 00:17:57,995
- Hi.

64
00:17:58,040 --> 00:17:59,155
Go on in.

65
00:18:12,200 --> 00:18:16,079
If I had know we were having a party,
I would have put something else on.

66
00:18:16,120 --> 00:18:17,997
- We met down at the bar.

67
00:18:18,080 --> 00:18:19,069
She is waiting for her mom

68
00:18:19,240 --> 00:18:22,038
and they are meeting in the lobby at 10.

69
00:18:22,080 --> 00:18:23,069
- 10:30.

70
00:18:23,080 --> 00:18:24,229
oh yeah 10:30.

71
00:18:25,040 --> 00:18:26,075
Her Mom is from Germany.

72
00:18:26,080 --> 00:18:27,069
- Sweden, Wit.

73
00:18:27,120 --> 00:18:28,235
I meant Sweden.

74
00:18:29,000 --> 00:18:30,228
- From Stockholm.

75
00:18:31,200 --> 00:18:34,033
Ploy is from Phuket, like us.

76
00:18:34,200 --> 00:18:37,078
Oh, really?

77
00:18:42,120 --> 00:18:44,076
What happened to your eye?

78
00:18:50,040 --> 00:18:52,076
Mind if I go to the bathroom?

79
00:18:52,120 --> 00:18:55,192
- No...
Make yourself comfortable.

80
00:19:15,160 --> 00:19:17,037
What is this Wit?

81
00:19:18,000 --> 00:19:19,991
You decide to have a party
without telling me?

82
00:19:20,240 --> 00:19:22,071
It's 6 o'clock in the morning!

83
00:19:22,160 --> 00:19:24,071
- What party?

84
00:19:24,080 --> 00:19:25,195
She's just a little girl.

85
00:19:25,240 --> 00:19:27,151
She was waiting for her mom all alone.

86
00:19:27,200 --> 00:19:29,156
for like, 4 hours already.

87
00:19:29,240 --> 00:19:31,196
Well I'm going to sleep understand?
Sleep!

88
00:19:31,240 --> 00:19:33,117
- Go to sleep then!

89
00:19:33,240 --> 00:19:36,152
How can I sleep with some
stranger in the room.

90
00:19:36,240 --> 00:19:38,196
- Well don't worry about it.

91
00:19:40,080 --> 00:19:42,196
We've been on a plane for 20 hours Wit!!

92
00:19:44,040 --> 00:19:46,156
And I'm tired, understand? Tired!

93
00:19:46,200 --> 00:19:49,988
Can't I just get some sleep for 4-5
hours, without some guest here?

94
00:19:50,040 --> 00:19:51,996
- You don't have to entertain.

95
00:20:02,080 --> 00:20:03,229
Feel better?

96
00:20:15,080 --> 00:20:19,198
When she is done in the bathroom,
get her out of this room.

97
00:20:20,040 --> 00:20:21,996
- Where do you want her to go?

98
00:20:22,040 --> 00:20:24,031
That's not our problem, Wit.

99
00:20:24,200 --> 00:20:27,078
- You don't care about
other peoples problems?

100
00:20:27,160 --> 00:20:29,037
What's wrong with her?

101
00:20:29,080 --> 00:20:31,036
- She's waiting for her Mom.

102
00:20:31,080 --> 00:20:33,036
That's not your problem.

103
00:20:33,120 --> 00:20:36,032
That's between her and her mom.

104
00:20:36,080 --> 00:20:39,152
If it's a problem, how is she
going to solve it here?

105
00:20:39,200 --> 00:20:42,078
- OK, it's not our problem.

106
00:20:42,120 --> 00:20:47,069
I just invited her up to freshen up
and get a little sleep.

107
00:20:47,120 --> 00:20:49,998
Sleep here?

108
00:20:51,120 --> 00:20:55,159
- I told her she could rest here
before her mom comes.

109
00:20:56,080 --> 00:20:59,117
So what, this is an orphanage now right?

110
00:20:59,160 --> 00:21:01,196
- No its not, Daeng.

111
00:21:04,160 --> 00:21:07,038
This room is only big enough
for the two of us.

112
00:21:08,080 --> 00:21:12,073
What are you going to do,
why didn't you ask me first?

113
00:27:32,120 --> 00:27:33,109
Wit?

114
00:27:34,120 --> 00:27:35,075
Wit?

115
00:27:40,080 --> 00:27:42,196
Get that girl out of here, now.

116
00:27:44,120 --> 00:27:45,155
- Where is she now?

117
00:27:45,200 --> 00:27:46,997
In the shower.

118
00:27:47,160 --> 00:27:48,991
- You want me to just burst in?

119
00:27:49,040 --> 00:27:49,995
Yes.

120
00:27:56,080 --> 00:27:57,115
If you don't tell her..

121
00:27:58,000 --> 00:27:59,035
then I'll tell her myself.

122
00:28:00,040 --> 00:28:01,109
- You wouldn't do that.

123
00:28:02,240 --> 00:28:03,195
How do you know?

124
00:28:04,120 --> 00:28:05,155
- You won't.

125
00:28:07,120 --> 00:28:09,076
Is that a challenge?

126
00:28:11,040 --> 00:28:14,077
- No, but I know you won't do it.

127
00:28:14,120 --> 00:28:16,156
- You're going to worry yourself
to death.

128
00:29:03,040 --> 00:29:04,189
Did I wake you?

129
00:29:05,120 --> 00:29:06,109
- No.

130
00:29:15,240 --> 00:29:18,073
You don't have to pick that up,
I'll do it.

131
00:29:19,120 --> 00:29:20,155
Shouldn't you finish in there?

132
00:29:20,200 --> 00:29:22,031
- I'm all finished.

133
00:29:33,200 --> 00:29:35,111
Thank you.

134
00:32:22,040 --> 00:32:24,031
I know your not asleep.

135
00:38:05,200 --> 00:38:08,033
Someone in the room is a thief!

136
00:38:08,080 --> 00:38:09,035
- What's missing?

137
00:38:22,080 --> 00:38:24,071
You like her don't you?

138
00:38:29,120 --> 00:38:30,997
You like her don't you?

139
00:38:33,160 --> 00:38:34,115
Wit?

140
00:38:34,160 --> 00:38:35,149
- What?

141
00:38:36,080 --> 00:38:37,115
Why aren't you answering?

142
00:38:37,160 --> 00:38:39,196
- You know why I'm not answering.

143
00:38:42,120 --> 00:38:47,069
- I think you should take the medicine
the doctor gave you, and go to sleep.

144
00:38:49,160 --> 00:38:53,233
Yes, I'll go to sleep and then
you will go to that girl.

145
00:38:54,000 --> 00:38:56,070
- That girl could be my daughter!

146
00:38:56,160 --> 00:38:58,071
Thats even better, right?

147
00:42:43,200 --> 00:42:46,237
I thought this would be more comfortable.

148
00:42:56,120 --> 00:42:58,156
- Thank you, sis.

149
00:45:31,160 --> 00:45:33,230
Is this Wit's room?

150
00:45:36,040 --> 00:45:37,996
- And you are?

151
00:45:38,040 --> 00:45:39,075
I'm Noy.

152
00:45:41,080 --> 00:45:42,069
- Oh.

153
00:45:43,040 --> 00:45:44,109
Do you know me?

154
00:45:46,240 --> 00:45:50,995
- I know you, I'm not sure
if Wit's awake or not.

155
00:47:52,040 --> 00:47:53,189
Where did you go?

156
00:47:54,080 --> 00:47:56,036
- I gave her a pillow.

157
00:48:01,000 --> 00:48:02,115
I need to talk to you.

158
00:48:02,160 --> 00:48:03,991
- About?

159
00:48:04,120 --> 00:48:05,235
About us.

160
00:48:07,080 --> 00:48:09,116
- OK, what.

161
00:48:15,040 --> 00:48:19,192
How can we talk, if your reading that book.

162
00:48:23,080 --> 00:48:24,149
- Alright, What is it?

163
00:48:39,120 --> 00:48:40,235
Do you love me?

164
00:48:41,080 --> 00:48:42,195
- Why are you asking?

165
00:48:43,080 --> 00:48:44,229
Do you love me?

166
00:48:44,240 --> 00:48:45,992
- Yes.

167
00:48:47,240 --> 00:48:50,152
How come I don't feel like you love me?

168
00:48:52,040 --> 00:48:55,237
- What do you want me to do about it?

169
00:48:56,240 --> 00:48:59,038
I don't want you to do anything!

170
00:48:59,240 --> 00:49:05,031
I want you to love me,
I want to know, you love me.

171
00:49:09,120 --> 00:49:12,032
- If I didn't love you, why did I
marry you?

172
00:49:13,080 --> 00:49:15,071
- If I didn't love you, why would
I stay for 8 years?

173
00:49:15,120 --> 00:49:17,031
7 years.

174
00:49:25,080 --> 00:49:26,991
We don't have time with each other.

175
00:49:27,040 --> 00:49:29,156
- Were together, all day, everyday.

176
00:49:29,200 --> 00:49:31,077
That's not what I meant.

177
00:49:31,120 --> 00:49:32,109
- Well what do you mean then?

178
00:49:33,080 --> 00:49:36,197
Like the two of us, going out to eat.

179
00:49:37,000 --> 00:49:41,994
- What? We have a restaurant why
would we go eat at someone else's?

180
00:49:42,040 --> 00:49:44,235
To hang out, go to a movie, anything!

181
00:49:46,000 --> 00:49:47,115
- I work everyday...

182
00:49:47,200 --> 00:49:49,191
when the restaurant closes
I just want to rest.

183
00:49:49,240 --> 00:49:54,189
What rest, you go to your friends house
and watch football all night!

184
00:49:56,040 --> 00:49:58,156
- So you want me to stop seeing my friends?

185
00:49:58,200 --> 00:50:01,237
I don't want you to stop anything.

186
00:50:02,040 --> 00:50:06,079
I just need to know that you want me too.

187
00:50:10,080 --> 00:50:13,993
- We are human, if we want, we want,
if we don't, then we don't.

188
00:50:14,040 --> 00:50:16,110
Are you saying you don't need me?

189
00:50:16,120 --> 00:50:18,031
- That's not what I said.

190
00:50:29,080 --> 00:50:33,232
Do you remember when we were first together?

191
00:50:34,200 --> 00:50:38,193
You just told me, for no reason

192
00:50:39,080 --> 00:50:41,036
that you loved me, everyday.

193
00:50:42,080 --> 00:50:44,036
- It's been 8 years, Daeng.

194
00:50:44,080 --> 00:50:45,069
Seven years.

195
00:50:47,080 --> 00:50:51,995
- So you want me to have said, "I love you, I love
you, I love you", everyday all this time?

196
00:50:52,080 --> 00:50:55,993
Why not? Don't you love me?

197
00:50:57,040 --> 00:50:58,996
- So like a parrot, Myna Bird or something?

198
00:51:00,080 --> 00:51:03,993
So what, I make you feel like
a Bird in a cage?

199
00:51:05,040 --> 00:51:08,112
- Ok, I love you, I love you,
I love you.

200
00:51:08,160 --> 00:51:14,076
I don't want you to just say it,
I want you to mean it!

201
00:51:43,240 --> 00:51:45,071
- are you having your period or something?

202
00:51:47,000 --> 00:51:49,195
Why do you ask that every time we talk?

203
00:51:50,040 --> 00:51:51,189
- Your so shaky all the time.

204
00:51:51,240 --> 00:51:55,074
What do you care, if I have my period or not?

205
00:51:55,160 --> 00:51:57,151
We haven't slept with each other for a year.

206
00:51:57,160 --> 00:52:00,038
- That's my fault then?

207
00:52:01,080 --> 00:52:02,069
I don't know.

208
00:52:03,160 --> 00:52:06,038
- I haven't been with my wife in 2 years!

209
00:52:06,080 --> 00:52:09,038
It's not like I've been with anyone else.

210
00:52:10,080 --> 00:52:12,071
- And you think I have?

211
00:52:20,080 --> 00:52:21,149
Yes I do.

212
00:52:21,200 --> 00:52:22,155
- Who?

213
00:52:26,160 --> 00:52:27,070
A woman named Noy.

214
00:52:27,120 --> 00:52:28,075
- Who?!

215
00:52:29,200 --> 00:52:30,155
Noy.

216
00:52:32,040 --> 00:52:34,156
I want to know if you have anything
going on with a woman named Noy?

217
00:52:34,200 --> 00:52:36,156
- What are you a psychic,
or palm reader now?

218
00:52:36,200 --> 00:52:38,998
Don't try to change the subject.

219
00:53:06,080 --> 00:53:08,116
- Did you go through my stuff?

220
00:53:10,040 --> 00:53:14,033
No, I was looking for a key
to open the luggage.

221
00:53:14,040 --> 00:53:16,076
It was in the pocket of your shirt.

222
00:53:16,120 --> 00:53:18,111
- Did you check my cellphone too?

223
00:53:24,080 --> 00:53:27,038
- Should I be more careful of my things,
since we live together?

224
00:53:30,200 --> 00:53:32,031
Tell me who she is.

225
00:53:32,120 --> 00:53:33,189
- Don't change the subject.

226
00:53:35,000 --> 00:53:36,069
Who is she?

227
00:53:36,200 --> 00:53:38,191
- If you don't tell me why you
were looking through my stuff,

228
00:53:38,240 --> 00:53:40,117
then I wont tell you anything.

229
00:53:40,120 --> 00:53:41,109
That's a totally different subject!

230
00:53:41,160 --> 00:53:44,038
- That's the same thing, its has to do
with trust!

231
00:53:44,120 --> 00:53:46,236
If were going to live like this maybe
we shouldn't live together.

232
00:56:25,160 --> 00:56:27,037
Daeng.

233
00:56:27,080 --> 00:56:28,149
Where are you going?

234
00:57:06,200 --> 00:57:08,111
I forgot where I put my necklace.

235
00:57:08,240 --> 00:57:10,151
- It's not in the bathroom?

236
00:57:11,160 --> 00:57:12,991
No it's not.

237
00:57:17,160 --> 00:57:19,116
Where did your girlfriend go?

238
00:57:20,080 --> 00:57:22,036
- Probably walking around downstairs.

239
00:57:24,040 --> 00:57:25,155
You guys were arguing, huh?

240
00:57:28,240 --> 00:57:30,037
Because of me right?

241
00:57:30,160 --> 00:57:32,037
- No.

242
00:57:33,160 --> 00:57:36,038
- We were arguing because we didn't
have anything else to do.

243
00:57:38,160 --> 00:57:42,199
- We have been together for a long time. Sometimes
the argument is to prove we are still close.

244
00:57:43,000 --> 00:57:46,151
- Arguing to confirm closeness.

245
00:57:47,200 --> 00:57:49,191
- The worse the argument..

246
00:57:49,240 --> 00:57:52,073
- But it also brings us closer.

247
00:57:54,040 --> 00:57:56,156
Why don't you hug? It's easier.

248
00:57:58,000 --> 00:58:02,152
Hugging, kissing, having sex.

249
00:58:03,160 --> 00:58:05,151
- We've been together 8 years.

250
00:58:05,160 --> 00:58:07,037
What? It has an expiration date?

251
00:58:07,120 --> 00:58:08,155
- Yes.

252
00:58:08,160 --> 00:58:10,151
Like canned food or something!

253
00:58:12,240 --> 00:58:13,229
- That's not the same.

254
00:58:15,040 --> 00:58:18,077
- A can of food tells you
when it will expire.

255
00:58:18,160 --> 00:58:21,072
- This doesn't tell you when it goes bad.

256
01:00:37,080 --> 01:00:39,036
Your Wife is a real beauty.

257
01:00:39,080 --> 01:00:40,195
- She used to be in movies.

258
01:00:40,240 --> 01:00:41,229
Really?

259
01:00:42,040 --> 01:00:43,029
- A long time ago.

260
01:00:49,240 --> 01:00:52,118
She seemed familiar.

261
01:00:56,040 --> 01:00:57,075
How did you meet?

262
01:00:57,240 --> 01:01:00,038
- I was working at a restaurant in America.

263
01:01:00,160 --> 01:01:02,196
- She came in with her husband.

264
01:01:02,240 --> 01:01:03,992
- That's how we met.

265
01:01:10,160 --> 01:01:12,151
Do you two have any children?

266
01:01:13,040 --> 01:01:14,155
- No.

267
01:01:15,080 --> 01:01:19,039
- Daeng has a son with her ex-husband,

268
01:01:19,120 --> 01:01:21,236
and I have a daughter with my ex-girlfriend.

269
01:01:28,040 --> 01:01:30,235
Did they break up because of you?

270
01:01:32,040 --> 01:01:33,109
- That was part of it.

271
01:01:35,080 --> 01:01:39,039
- But I think they broke up,
because their relationship expired.

272
01:01:43,160 --> 01:01:45,116
What about you and your ex?

273
01:01:45,240 --> 01:01:47,196
Did your relationship expire too?

274
01:01:51,240 --> 01:01:52,992
- No, she died.

275
01:02:11,000 --> 01:02:12,991
Oh, I'm sorry.

276
01:04:33,120 --> 01:04:34,189
Are you reading this?

277
01:04:34,240 --> 01:04:36,117
- Go ahead.

278
01:04:39,160 --> 01:04:43,153
- Excuse me, your Jiranan right?

279
01:04:44,080 --> 01:04:45,069
I'm Tiranan, yes.

280
01:04:45,120 --> 01:04:47,156
- Oh Tiranan.

281
01:04:54,200 --> 01:04:57,192
- That paper is 3 days old.

282
01:05:30,080 --> 01:05:32,071
- Someone wants to talk to you.

283
01:05:36,120 --> 01:05:37,075
Who is it?

284
01:05:38,120 --> 01:05:40,111
- It's one of your fans.

285
01:05:52,040 --> 01:05:52,995
Hello?

286
01:05:55,240 --> 01:05:57,071
Yes.

287
01:05:59,080 --> 01:06:00,035
Really?

288
01:06:02,160 --> 01:06:04,037
Thank you.

289
01:06:06,200 --> 01:06:08,191
But I don't act anymore.

290
01:06:10,040 --> 01:06:12,076
I've been living in America.

291
01:06:13,120 --> 01:06:15,031
It's been about 10 years.

292
01:06:17,000 --> 01:06:19,036
I came back for my grandmothers funeral.

293
01:06:20,240 --> 01:06:22,071
I came back by myself.

294
01:06:23,200 --> 01:06:24,155
What?

295
01:06:26,040 --> 01:06:27,189
Lover?

296
01:06:29,120 --> 01:06:32,032
I don't have one, I'm single.

297
01:06:47,160 --> 01:06:48,115
Thank you.

298
01:06:48,120 --> 01:06:49,997
- No problem.

299
01:06:50,040 --> 01:06:52,190
- Please excuse me.

300
01:07:04,080 --> 01:07:06,150
Your a lonely person aren't you.

301
01:07:08,120 --> 01:07:10,156
- Isn't everyone lonely?

302
01:07:11,080 --> 01:07:13,036
- A lot of times, people don't know
they are lonely.

303
01:07:14,240 --> 01:07:16,117
- Since they have things to keep them busy.

304
01:07:19,200 --> 01:07:24,115
That's true, is your wife lonely too?

305
01:07:25,040 --> 01:07:26,189
- Lonely and drunk.

306
01:07:28,040 --> 01:07:29,029
What?

307
01:07:29,080 --> 01:07:30,149
- Nothing.

308
01:07:39,080 --> 01:07:41,116
I had a really gross dream.

309
01:07:42,120 --> 01:07:43,109
- What?

310
01:07:44,120 --> 01:07:47,078
Do you remember the maid
we passed in the hallway?

311
01:07:49,080 --> 01:07:51,036
I dreamt that she was doing
something with the bartender.

312
01:07:51,080 --> 01:07:52,035
- What was it?

313
01:07:53,120 --> 01:07:55,031
They were having sex.

314
01:07:55,120 --> 01:07:56,075
- Did it turn you on?

315
01:07:58,240 --> 01:08:01,038
Jerk!

316
01:08:04,080 --> 01:08:05,195
Go ahead.

317
01:08:15,080 --> 01:08:16,195
Doy?

318
01:08:18,040 --> 01:08:19,234
Doy!

319
01:08:21,160 --> 01:08:22,070
Go ahead.

320
01:08:22,120 --> 01:08:23,189
DOY!

321
01:08:25,080 --> 01:08:27,116
It seems my girlfriend went to go shopping.

322
01:08:51,240 --> 01:08:53,117
It's dark huh?

323
01:08:58,120 --> 01:08:59,109
Come this way.

324
01:09:22,040 --> 01:09:23,029
- You live here?

325
01:09:23,080 --> 01:09:28,074
Yes my condo is also a showroom,
and warehouse.

326
01:09:28,080 --> 01:09:31,117
It's like "three in one".

327
01:09:44,200 --> 01:09:46,156
Are you OK?

328
01:09:48,120 --> 01:09:49,155
- I'm not sure.

329
01:09:51,160 --> 01:09:53,116
- Is this your collection?

330
01:09:53,200 --> 01:09:58,069
I sell, rent it, and collect it too.

331
01:09:58,240 --> 01:10:00,151
"Three in one".

332
01:10:09,160 --> 01:10:11,037
Have a seat.

333
01:10:29,160 --> 01:10:31,151
Want some Vodka?

334
01:10:45,200 --> 01:10:47,156
- Can I use your phone?

335
01:10:47,200 --> 01:10:49,077
- I need to call a friend.

336
01:10:49,120 --> 01:10:50,155
Go ahead.

337
01:10:50,240 --> 01:10:52,151
It's on the table on the corner.

338
01:11:03,160 --> 01:11:05,116
It's next to the hand.

339
01:11:48,200 --> 01:11:50,156
Room 603 please.

340
01:11:50,200 --> 01:11:54,113
His name is Wit Waitiyakun.

341
01:11:56,240 --> 01:11:58,117
What?

342
01:12:00,080 --> 01:12:01,195
He's out?

343
01:12:03,200 --> 01:12:05,111
How long ago?

344
01:12:08,080 --> 01:12:09,195
Your sure?

345
01:12:11,120 --> 01:12:16,990
He left with a girl?

346
01:14:58,040 --> 01:14:59,075
Get a hold of your friend?

347
01:15:01,160 --> 01:15:02,149
- No

348
01:15:09,080 --> 01:15:10,069
Drink up.

349
01:15:54,200 --> 01:15:56,191
- Can I have one on the rocks?

350
01:18:38,240 --> 01:18:40,037
Hello.

351
01:21:10,080 --> 01:21:11,149
Daeng?

352
01:21:13,200 --> 01:21:14,155
Daeng?

353
01:21:20,040 --> 01:21:21,155
Cold water?

354
01:21:49,160 --> 01:21:51,230
- Move over a little.

355
01:21:52,200 --> 01:21:54,998
Warm enough?

356
01:21:58,040 --> 01:22:00,110
- Just move over a little.

357
01:22:00,240 --> 01:22:03,152
- Your wife wouldn't like it.

358
01:22:04,040 --> 01:22:06,031
I don't have a wife.

359
01:22:08,080 --> 01:22:10,071
- Who did I talk to on the phone earlier then?

360
01:22:10,200 --> 01:22:12,998
That was my sister.

361
01:22:15,040 --> 01:22:16,996
- You guys are all the same aren't they.

362
01:22:17,040 --> 01:22:18,996
It really was my sister.

363
01:22:29,120 --> 01:22:29,996
- Don't.

364
01:22:31,240 --> 01:22:32,195
- Don't!

365
01:24:07,200 --> 01:24:09,156
Think you can just kick me for no reason?

366
01:24:10,040 --> 01:24:13,077
Kicking my balls.. that's unacceptable.

367
01:24:43,080 --> 01:24:44,195
You don't have to worry.

368
01:24:45,000 --> 01:24:46,991
I'll use a condom.

369
01:25:03,080 --> 01:25:04,035
Daeng.

370
01:25:10,160 --> 01:25:11,115
Daeng?

371
01:27:13,080 --> 01:27:14,149
Daeng.

372
01:27:15,120 --> 01:27:16,235
Don't leave me.

373
01:29:54,120 --> 01:29:55,075
Daeng?

374
01:29:57,160 --> 01:29:58,115
Daeng?

375
01:30:20,160 --> 01:30:23,232
Daeng?

376
01:30:24,120 --> 01:30:25,155
I'm sorry.

377
01:30:54,120 --> 01:30:55,109
Daeng?

378
01:30:56,160 --> 01:30:58,071
I'm not going to do it again!

379
01:31:45,040 --> 01:31:46,075
Daeng?

380
01:31:47,200 --> 01:31:48,189
I'm sorry.

381
01:34:09,080 --> 01:34:15,189
Love...

382
01:34:15,240 --> 01:34:23,158
you float in the wind....

383
01:34:23,200 --> 01:34:35,158
confident... passionate...
affectionate...

384
01:34:35,160 --> 01:34:41,030
Iove...is it really from the heart...

385
01:34:41,080 --> 01:34:49,078
isn't it funny... we always protect it...

386
01:34:49,120 --> 01:34:54,069
even though it can't be seen...

387
01:34:54,120 --> 01:35:02,073
it provokes and chases our heart...

388
01:35:21,040 --> 01:35:26,990
my heart can sympathize....
somewhat...

389
01:35:27,040 --> 01:35:35,197
hope you don't get...Hurt by love...
and hurt again..

390
01:35:35,200 --> 01:35:40,115
annoying pain of love...
tears flow...

391
01:35:40,160 --> 01:35:48,192
If there are both...
I might have to die

392
01:37:09,080 --> 01:37:09,990
Shall we go?

393
01:37:12,240 --> 01:37:13,150
- Let's go.

394
01:37:54,200 --> 01:37:56,077
Daeng?

395
01:37:58,160 --> 01:38:00,037
I love you.

396
01:38:26,080 --> 01:38:27,991
- The girl from this morning...

397
01:38:29,080 --> 01:38:31,071
- She was kinda cute.

398
01:38:34,040 --> 01:38:48,193
English translation by: BuccKeao.
Subbed by: Jan de Uitvreter.

