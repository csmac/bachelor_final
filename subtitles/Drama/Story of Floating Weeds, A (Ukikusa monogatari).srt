1
00:00:02,358 --> 00:00:03,586
Previously on Weeds:

2
00:00:03,759 --> 00:00:05,920
For my cover business,
what about a bakery?

3
00:00:06,095 --> 00:00:07,858
These will satisfy your munchies...

4
00:00:08,030 --> 00:00:10,396
...and give you a long-lasting,
all-body high.

5
00:00:10,566 --> 00:00:13,091
-I'm ready to take orders.
-Fuck it. I'll take it all.

6
00:00:13,269 --> 00:00:16,796
Last week's buy,
this week's buy, my ring, my Rover.

7
00:00:16,973 --> 00:00:19,237
You still short for all the shit
you gonna wanna take this week.

8
00:00:19,408 --> 00:00:21,808
-I'll be back.
-I don't like dealing with things.

9
00:00:21,978 --> 00:00:24,378
I'd much prefer to pretend
they don't exist.

10
00:00:24,547 --> 00:00:27,243
Megan. Hey, Megan. Megan.

11
00:00:27,416 --> 00:00:29,976
-Can you hear me?
-That deaf girl down on Dewey Street.

12
00:02:12,488 --> 00:02:15,321
Fire! Fire! Mom, what are you doing?
There's a fire!

13
00:02:15,491 --> 00:02:17,823
If there's a fire, I'll run, okay?

14
00:02:17,993 --> 00:02:22,396
Otherwise, I'm gonna bash in
the smoke alarm with a broom handle.

15
00:02:27,036 --> 00:02:28,731
-Andy?
-Sorry, Pants.

16
00:02:28,904 --> 00:02:31,634
-I was trying to surprise you guys.
-Uncle Andy!

17
00:02:31,807 --> 00:02:34,105
Hey, guys.

18
00:02:34,410 --> 00:02:36,901
Yes. I'm digging the love.

19
00:02:37,079 --> 00:02:39,547
-Oh, I missed you.
-So how was Alaska?

20
00:02:39,715 --> 00:02:44,345
Alaska was so beautiful.
In the summer, you could party all night.

21
00:02:44,520 --> 00:02:47,956
Then it got dark, and this psycho girl
tried to bring me up on charges--

22
00:02:48,124 --> 00:02:50,820
-How did you get in here?
-I swear I'll pay for the window.

23
00:02:50,993 --> 00:02:52,460
I didn't wanna wake you.

24
00:02:52,628 --> 00:02:56,394
I'm joking, I'm joking, I'm joking.

25
00:02:56,632 --> 00:03:00,159
Hey, you guys wanna help me make
breakfast? Silas, squeeze the oranges.

26
00:03:00,336 --> 00:03:02,099
Shane, I need you making the coffee.

27
00:03:02,271 --> 00:03:05,138
Sorry about the grease fire.
That bacon really spits up, huh?

28
00:03:05,307 --> 00:03:08,470
Shane, go get a broom
so I can bash your Uncle Andy with it.

29
00:03:08,644 --> 00:03:10,737
Come on, Nancy Pantsy.

30
00:03:10,913 --> 00:03:13,438
-Making eggs Florentine.
-Don't call me "Pants."

31
00:03:14,617 --> 00:03:16,141
Oh, shit. You?

32
00:03:16,318 --> 00:03:19,549
Hey, Lupita. I know you missed me.
How about a hug?

33
00:03:20,322 --> 00:03:24,224
-I'm not cleaning up his mess.
-Uncle Andy, did you bring us anything?

34
00:03:24,393 --> 00:03:26,156
Of course. Look who you're talking to.

35
00:03:26,328 --> 00:03:29,229
Get my duffel in the guestroom.
The black one, not the green.

36
00:03:29,398 --> 00:03:31,628
-Stay out of the green.
-Guestroom. Guestroom.

37
00:03:32,668 --> 00:03:34,260
I guess I could sleep with Lupita.

38
00:03:34,437 --> 00:03:37,497
I do love the fiery Latinas.

39
00:03:38,407 --> 00:03:40,967
Know this, Lupita.
Until you learn to love me...

40
00:03:41,844 --> 00:03:43,744
...I got enough love for both of us.

41
00:03:43,913 --> 00:03:47,144
How did you and Judah
emerge from the same woman?

42
00:03:47,316 --> 00:03:51,150
I know. What family gets blessed
with not one, but two studs?

43
00:03:51,320 --> 00:03:54,517
Okay, everybody gather. First...

44
00:03:54,690 --> 00:03:57,420
...young Silas, that's for you.

45
00:03:57,593 --> 00:04:00,892
-Awesome. So does it work?
-Yeah, for another three weeks.

46
00:04:01,063 --> 00:04:04,624
Then you're gonna scratch off the
serial number and get a new service plan.

47
00:04:04,800 --> 00:04:06,199
Okay, for Mama....

48
00:04:09,638 --> 00:04:11,367
What's that?

49
00:04:11,841 --> 00:04:14,708
-For your neck. You're always talking--
-Andy, this is--

50
00:04:14,877 --> 00:04:16,674
Take it.

51
00:04:18,314 --> 00:04:21,249
For my neck. Of course, thank you.

52
00:04:21,417 --> 00:04:22,748
Lupita.

53
00:04:22,918 --> 00:04:25,546
I'll put it in your bedroom,
next to the little one.

54
00:04:25,721 --> 00:04:27,211
What about me?

55
00:04:27,389 --> 00:04:29,323
Oh, I don't....

56
00:04:30,993 --> 00:04:32,585
-Best for last.
-No way!

57
00:04:32,761 --> 00:04:35,321
-No. No way.
-Come on, I'll teach him how to use them.

58
00:04:35,498 --> 00:04:37,261
Okay, Shane. Hand them over.
Andy!

59
00:04:37,433 --> 00:04:40,334
I'm gonna hold onto those
till we're in a better space.

60
00:04:40,503 --> 00:04:43,199
Who's hungry?
Everybody go sit down.

61
00:04:50,045 --> 00:04:51,842
Andy?

62
00:04:52,481 --> 00:04:55,746
-How long you planning on staying?
-Don't worry, I'll help out.

63
00:04:55,918 --> 00:04:57,613
How long?

64
00:04:57,786 --> 00:05:00,186
Just till I figure some things out.

65
00:05:00,356 --> 00:05:02,756
-Like?
-I don't know.

66
00:05:02,925 --> 00:05:05,792
-My life.
-Sure.

67
00:05:05,961 --> 00:05:07,189
That.

68
00:05:09,498 --> 00:05:13,366
Good to see you, Pants.
It's good to be with family.

69
00:05:13,536 --> 00:05:15,436
Come on, let's eat.

70
00:05:16,539 --> 00:05:18,632
Hey, buddy, what you doing?

71
00:05:18,807 --> 00:05:20,707
Just lM'ing my girlfriend.

72
00:05:20,876 --> 00:05:23,106
You don't need a credit card
for that, do you?

73
00:05:23,279 --> 00:05:25,770
No. Real girlfriend.

74
00:05:25,948 --> 00:05:27,779
Megan. She's deaf.

75
00:05:28,384 --> 00:05:31,080
Deaf Megan. She cute?

76
00:05:31,253 --> 00:05:34,086
Yeah. She's totally cute.

77
00:05:36,091 --> 00:05:37,922
Oh, yeah.

78
00:05:38,093 --> 00:05:40,288
She doesn't look deaf.
She can't hear anything?

79
00:05:40,462 --> 00:05:42,123
That's what "deaf" means.

80
00:05:42,298 --> 00:05:45,563
Man, how did you get
so smart at, what, 16?

81
00:05:45,734 --> 00:05:49,534
It took me years to learn slightly
defective chicks are the way to go.

82
00:05:49,705 --> 00:05:52,105
I once went out with this girl
with a baby arm.

83
00:05:52,274 --> 00:05:54,139
Insane in the sack.

84
00:05:54,743 --> 00:05:58,873
Plus, when she grabbed my dick
with her little hand, it looked gigantic.

85
00:05:59,415 --> 00:06:03,283
-So how do you talk to her?
-We just mostly hang out and stuff.

86
00:06:03,452 --> 00:06:06,046
-You should learn Braille.
-You mean sign language?

87
00:06:06,221 --> 00:06:08,416
-Whatever.
-What's this?

88
00:06:08,591 --> 00:06:12,049
-That's mine.
-"Chris died for your sins."

89
00:06:12,227 --> 00:06:13,888
-ls that a joke?
-Yeah, on me.

90
00:06:14,063 --> 00:06:16,497
That's what happens
when you outsource to Malaysia.

91
00:06:16,665 --> 00:06:19,293
Eight-year-olds in the sweatshop
can't spell for shit.

92
00:06:19,468 --> 00:06:22,665
See, I was trying to jump on board
the whole red-state Jesus thing.

93
00:06:22,838 --> 00:06:26,171
The fashion of the Christ.
I end up with 3000 of these.

94
00:06:26,342 --> 00:06:29,743
Totally useless. If there's one thing
I learned about the Christ crowd...

95
00:06:30,746 --> 00:06:34,876
...absolutely no sense of humor.
Should've gone after the Jew market.

96
00:06:35,050 --> 00:06:36,347
At least we can take a joke.

97
00:06:37,252 --> 00:06:39,152
Come on, Shane.

98
00:06:43,892 --> 00:06:45,416
Look who's here.

99
00:06:45,594 --> 00:06:49,428
Baby, it's "the early bird gets the worm,"
not the weed.

100
00:06:49,598 --> 00:06:51,566
You need to read up on your Bartlett's.

101
00:06:51,734 --> 00:06:55,966
My brother-in-law took the kids to school.
I thought I'd get a jump on the day.

102
00:06:56,138 --> 00:06:58,663
-You want some oatmeal?
-No.

103
00:06:58,841 --> 00:07:02,208
-Andy made eggs Florentine.
-Andy?

104
00:07:02,378 --> 00:07:04,869
When did that fancy-cooking trouble
get back in town?

105
00:07:05,047 --> 00:07:08,881
This morning, I woke up
and there he was.

106
00:07:09,051 --> 00:07:12,509
Sort of like crotch crabs,
only not as treatable, huh?

107
00:07:12,688 --> 00:07:15,282
-I forgot you knew him.
-Been hoping to forget him.

108
00:07:16,058 --> 00:07:17,491
How did Andy and Conrad meet?

109
00:07:17,660 --> 00:07:20,060
I never got a straight answer
out of either of them.

110
00:07:20,229 --> 00:07:23,323
They worked at Circuit City together.
Got caught stealing together.

111
00:07:23,499 --> 00:07:26,195
Got fired together. They go way back.

112
00:07:26,368 --> 00:07:29,963
-Never should've let that boy work retail.
-You gonna need to come back later.

113
00:07:30,139 --> 00:07:33,905
-We ain't got in everything you ordered.
-I got 1 7 food orders to fill.

114
00:07:34,076 --> 00:07:36,203
Listen to Betty Cracker.

115
00:07:36,378 --> 00:07:39,870
You got bunches of that crap last time.
You out already?

116
00:07:40,049 --> 00:07:42,381
I've got some.
Not enough for what I need to make.

117
00:07:42,551 --> 00:07:44,451
My crowd loves to eat their smoke.

118
00:07:44,620 --> 00:07:47,817
Well, I could hook you up
with a little shwag if you really crazy.

119
00:07:47,990 --> 00:07:49,582
Oh, shit.

120
00:07:49,758 --> 00:07:51,988
I've still got the gourmet fuckup
in my kitchen.

121
00:07:52,161 --> 00:07:56,791
I'm never gonna get anything done.
Oh, fuck. I'm fucked.

122
00:07:56,965 --> 00:08:00,492
You've got a nasty mouth
when you stressed.

123
00:08:00,669 --> 00:08:02,364
-You got a pen?
-Yeah.

124
00:08:02,938 --> 00:08:06,999
I'm gonna give you a number, and you say
I said it was okay for you to call.

125
00:08:07,309 --> 00:08:08,571
Who am I calling?

126
00:08:10,479 --> 00:08:12,379
The Candyman.

127
00:08:13,449 --> 00:08:15,041
Wear sporty shoes.

128
00:08:32,935 --> 00:08:36,132
"Megan89. Are you home?"

129
00:08:38,340 --> 00:08:39,967
Yeah.

130
00:08:40,142 --> 00:08:42,337
"Me too. Sore throat."

131
00:08:42,511 --> 00:08:44,411
Bummer.

132
00:08:44,580 --> 00:08:46,275
What are you wearing?

133
00:08:48,350 --> 00:08:50,477
Take it off. "Okay."

134
00:08:55,124 --> 00:08:58,651
I tried to spread the word.
I eat here every day, invited people.

135
00:08:58,827 --> 00:09:01,853
"Oh, it's too ethnic."
"Oh, it's too spicy."

136
00:09:02,030 --> 00:09:04,055
"Oh, those people
don't wash their hands."

137
00:09:04,233 --> 00:09:06,497
What the fuck is wrong
with these morons who go...

138
00:09:06,668 --> 00:09:10,502
...to wait an hour in line at some
fucking crappy Olive Garden...

139
00:09:10,672 --> 00:09:14,073
...and let a treasure like this
go out of business? I can't take it.

140
00:09:14,243 --> 00:09:16,438
Doug, why are we here?

141
00:09:16,912 --> 00:09:18,777
Did you taste the saag aloo?

142
00:09:18,947 --> 00:09:22,314
It's to die for, and then be reincarnated,
and then die for again.

143
00:09:22,484 --> 00:09:24,975
-Why aren't you eating?
-I told you I ate already.

144
00:09:25,154 --> 00:09:26,883
-Where?
-The Olive Garden.

145
00:09:27,055 --> 00:09:29,114
I wouldn't take a dump
in the Olive Garden.

146
00:09:29,291 --> 00:09:31,122
I like the hot artichoke-spinach dip.

147
00:09:31,693 --> 00:09:33,923
-I can't even look at you.
-Okay, enough, enough.

148
00:09:34,096 --> 00:09:37,156
I'm sure the saag aloo is delicious.

149
00:09:38,066 --> 00:09:39,499
Wow, that's incredible.

150
00:09:39,668 --> 00:09:42,102
Tell me this doesn't kick
your artichoke dip's ass.

151
00:09:43,505 --> 00:09:48,306
Tell me more. Are your nipples hard?

152
00:09:48,877 --> 00:09:52,677
Come on. "Like shooting stars.

153
00:09:53,849 --> 00:09:56,682
Now you." No, more you.

154
00:09:56,852 --> 00:09:58,786
Are you touching yourself?

155
00:10:01,790 --> 00:10:04,987
"I'm parting the lips
of my blushing vagina."

156
00:10:05,427 --> 00:10:07,054
Yes. Good.

157
00:10:13,202 --> 00:10:17,070
Keep typing.

158
00:10:17,840 --> 00:10:20,638
What do you think about this place
as the bakery?

159
00:10:21,043 --> 00:10:24,535
I mean, location's pretty good,
kitchen's already set up.

160
00:10:24,713 --> 00:10:29,013
If you make something mediocre enough,
you may even make a go at it legitimately.

161
00:10:29,184 --> 00:10:32,915
-Let me think about it.
-There we go, Mr. Wilson.

162
00:10:33,322 --> 00:10:35,017
Twenty orders for you.

163
00:10:35,190 --> 00:10:38,887
Remember to let them cool
before you put them in the freezer.

164
00:10:39,061 --> 00:10:40,790
Mr. Advani, where will you go?

165
00:10:40,963 --> 00:10:44,729
Oh, we just bought
an Olive Garden franchise close by.

166
00:10:44,900 --> 00:10:47,391
-You must come visit.
-Fuck Agrestic.

167
00:10:47,569 --> 00:10:49,264
Yes, sir. Okay.

168
00:10:50,439 --> 00:10:52,236
Pay when you're ready.

169
00:10:52,407 --> 00:10:55,535
-When are my treats coming in?
-I'm having a little trouble...

170
00:10:55,711 --> 00:10:58,544
...getting my kitchen to myself lately,
but I'll let you know.

171
00:11:16,465 --> 00:11:18,456
Hey, Lupita,
I was just thinking about you.

172
00:11:20,369 --> 00:11:21,597
It's for you.

173
00:11:25,107 --> 00:11:26,802
Wipe your hands.

174
00:11:35,517 --> 00:11:37,280
Yeah, this is Andy.

175
00:11:37,452 --> 00:11:40,353
Hey, Shane.

176
00:11:40,522 --> 00:11:42,149
Really?

177
00:11:42,324 --> 00:11:44,349
Awesome.

178
00:11:44,526 --> 00:11:47,324
Yeah. I'll bring the shirts.

179
00:11:47,496 --> 00:11:49,293
Let's say 15 minutes.

180
00:11:49,464 --> 00:11:53,264
Okay. Bye-- Hey, make it 20. Okay.

181
00:11:53,435 --> 00:11:58,168
Hey, get your original
"Chris died" T-shirts right here. Yeah.

182
00:11:58,340 --> 00:12:00,638
Only 10 dollars.
Have your money ready.

183
00:12:00,809 --> 00:12:03,573
There you go. One size fits all.
It's gonna shrink a little.

184
00:12:03,745 --> 00:12:05,337
Oh, yeah.

185
00:12:05,514 --> 00:12:08,779
-Okay, that's for you.
-Uncle Andy?

186
00:12:08,951 --> 00:12:10,680
What, what, what? Hang on.

187
00:12:10,852 --> 00:12:13,218
Now, remember, kids,
Chris loves you, so--

188
00:12:13,388 --> 00:12:15,356
-What? What?
-It's the principal.

189
00:12:17,092 --> 00:12:18,821
Oh, shit, man. We're just heating up.

190
00:12:20,028 --> 00:12:22,326
-Excuse me.
-Look, kids, Chris is risen.

191
00:12:22,965 --> 00:12:25,991
What--? What--? Wait.

192
00:12:31,873 --> 00:12:33,534
Oh, no.

193
00:12:33,942 --> 00:12:37,639
Have you all seen these?
They are all over the school.

194
00:12:41,783 --> 00:12:46,015
And as a Christian, I must say
I am deeply, deeply offended. It's profane.

195
00:12:46,188 --> 00:12:48,520
Has anyone talked
to the principal about this?

196
00:12:48,690 --> 00:12:50,351
-Who's Chris?
-Where's Celia?

197
00:12:50,525 --> 00:12:53,289
-I bet she'd have something to say.
-It's 1 0 minutes after.

198
00:12:53,462 --> 00:12:56,124
-Maybe we should start without her.
-Does anyone know how?

199
00:12:56,298 --> 00:13:00,029
Well, I think we need to. The first order
of business should be these shirts.

200
00:13:00,202 --> 00:13:02,466
-They're funny.
-Excuse me?

201
00:13:03,271 --> 00:13:06,104
I think you're making
way too big a deal about it.

202
00:13:06,274 --> 00:13:07,502
It's a joke.

203
00:13:07,676 --> 00:13:10,907
Oh, we do not joke about
our Lord Jesus Christ.

204
00:13:13,548 --> 00:13:16,711
It's a stupid T-shirt.

205
00:13:17,085 --> 00:13:20,851
-We should ignore it.
-Only one man died for my sins, Nancy.

206
00:13:21,023 --> 00:13:23,821
-And his name wasn't Chris.
-Well, it sort of was, wasn't it?

207
00:13:23,992 --> 00:13:25,220
Like a nickname?

208
00:13:25,394 --> 00:13:29,091
If we make a big deal about it,
in a week it'll become a full-blown trend.

209
00:13:29,264 --> 00:13:31,858
-They're kids, it'll become cool.
-Principle Dodge.

210
00:13:32,034 --> 00:13:34,730
-Have you seen these blasphemies?
-Yes, Maggie, I have.

211
00:13:34,903 --> 00:13:37,531
-Are you doing something about them?
-I'm about to.

212
00:13:37,873 --> 00:13:41,832
Mrs. Botwin, would you mind coming into
my office to discuss your son's business?

213
00:13:42,277 --> 00:13:44,302
-Shane was selling them?
-Yes.

214
00:13:44,479 --> 00:13:47,448
With some guy in a van who sped off.

215
00:13:47,616 --> 00:13:49,083
I'll never talk.

216
00:13:50,185 --> 00:13:52,016
Hey.

217
00:13:52,187 --> 00:13:53,950
Hey, what do you like better?

218
00:13:54,122 --> 00:13:57,353
"Jesus say relax," or,
"I'm too sexy for my Lord"?

219
00:13:57,526 --> 00:13:59,858
How about,
"Asshole ditched my 1 0-year-old"?

220
00:14:01,096 --> 00:14:03,189
I admit it, I panicked.
The guy had a whistle.

221
00:14:04,433 --> 00:14:05,798
They wanted to suspend him.

222
00:14:06,068 --> 00:14:10,402
For what? If Shane wants to believe in
Chris, and they try to suspend him for it...

223
00:14:10,572 --> 00:14:13,939
...whoa, that's freedom of religion.
That's, like, the First Commandment.

224
00:14:14,109 --> 00:14:17,078
-We could nail them on that.
-I don't think Chris is protected...

225
00:14:17,245 --> 00:14:19,338
...under freedom of religion, Andy.

226
00:14:19,514 --> 00:14:22,847
In fact, the angry Christian moms
I heard from in the PTA...

227
00:14:23,018 --> 00:14:24,747
...were pretty offended by it.

228
00:14:25,287 --> 00:14:27,278
Well, that's so intolerant.

229
00:14:27,456 --> 00:14:29,822
I mean, what would Jesus do?

230
00:14:30,692 --> 00:14:35,254
Religious bullshit aside,
you don't wanna know how far...

231
00:14:35,430 --> 00:14:39,264
...I had to crawl up the principal's ass
just to keep you two out of trouble.

232
00:14:39,434 --> 00:14:44,531
Not to mention the unbelievable
amount of shit I'm gonna get...

233
00:14:44,706 --> 00:14:48,164
...from those hypo-Christian
bitch moms tomorrow.

234
00:14:48,343 --> 00:14:52,336
Hey, I was just trying to pitch in.

235
00:14:52,747 --> 00:14:56,513
Earn a little money. I saw an opportunity
to help the family and I took it.

236
00:14:56,685 --> 00:14:59,882
Really? It was for the family, huh?

237
00:15:00,188 --> 00:15:01,917
Where's the money?

238
00:15:02,824 --> 00:15:05,190
Don't worry,
I'll use it towards family expenses.

239
00:15:06,061 --> 00:15:07,653
-Hello?
-I gotta cover my overhead.

240
00:15:07,829 --> 00:15:09,990
Yeah, I did call.
Thanks for getting back to me.

241
00:15:10,165 --> 00:15:12,531
Can you hold on a second, please?

242
00:15:12,701 --> 00:15:14,794
Go away. I have to take this call.

243
00:15:14,970 --> 00:15:17,370
-Who is it?
-None of your business. Get out of here.

244
00:15:17,539 --> 00:15:19,769
Why, you got a boyfriend?
Isn't it a little soon?

245
00:15:19,941 --> 00:15:22,774
Maybe I should take my vibrator back.
Fine, angry!

246
00:15:22,944 --> 00:15:25,242
God! Keep the vibrator.

247
00:15:27,315 --> 00:15:31,445
Thanks for getting back to me,
Candyman.

248
00:15:43,131 --> 00:15:45,565
-You're the Candyman?
-Yes.

249
00:15:45,734 --> 00:15:48,168
-You getting any exercise?
-Excuse me?

250
00:15:48,336 --> 00:15:52,295
If you're not committed to personal fitness,
I can't in good conscience sell to you.

251
00:15:52,474 --> 00:15:54,874
I wouldn't say Heylia's
in the best shape.

252
00:15:55,043 --> 00:15:56,738
Heylia's a lazy fat-fat.

253
00:15:56,912 --> 00:16:00,780
I'm hoping to put her into a diabetic coma,
so I have no problem selling to her.

254
00:16:00,949 --> 00:16:02,780
-Why?
-Scare her.

255
00:16:02,951 --> 00:16:05,920
Some people never learn
until their life is on the line.

256
00:16:10,659 --> 00:16:12,957
-Know what you are?
-What am l?

257
00:16:13,128 --> 00:16:16,359
Skinny fat.
What are we gonna do about that?

258
00:16:16,531 --> 00:16:20,797
-We're gonna start exercising right away.
-Don't humor me. I'm very serious.

259
00:16:20,969 --> 00:16:23,904
I used to weight 31 4 pounds.

260
00:16:25,607 --> 00:16:29,373
-Congratulations.
-The key? Exercise.

261
00:16:29,544 --> 00:16:32,035
You know, I'm not buying
for personal use.

262
00:16:32,214 --> 00:16:35,274
Are your customers
just a bunch of Fatty McFat-Fats?

263
00:16:35,750 --> 00:16:37,877
Well, they're smokers.

264
00:16:38,053 --> 00:16:41,819
But it stands to reason
that if they eat rather than smoke...

265
00:16:41,990 --> 00:16:46,393
...they should be able to breathe easier,
should they decide to exercise.

266
00:16:46,561 --> 00:16:50,088
That's a reasonable assumption.
I hope you'll take it upon yourself...

267
00:16:50,265 --> 00:16:52,256
-...to encourage them to do so.
-Of course.

268
00:16:52,434 --> 00:16:54,902
-And I'll take everything you've got.
-Really?

269
00:16:55,070 --> 00:16:57,197
-Yeah.
-And?

270
00:16:57,372 --> 00:17:00,068
And I will make a greater effort
to exercise.

271
00:17:00,242 --> 00:17:01,709
No, no, no.

272
00:17:02,611 --> 00:17:06,206
-And I will exercise.
-Yes, you will.

273
00:17:09,384 --> 00:17:13,286
Great. Dumb and dumber, reunited.

274
00:17:13,622 --> 00:17:16,489
Man, it's good to see you.
You smell really good.

275
00:17:16,658 --> 00:17:19,126
You better get the fuck off me.
What's wrong with you?

276
00:17:19,294 --> 00:17:22,559
Can't a straight man admire the stink
of another handsome straight man?

277
00:17:22,731 --> 00:17:24,028
Not unless you in prison.

278
00:17:24,199 --> 00:17:26,224
-ls that where you been?
-Close. Alaska.

279
00:17:26,401 --> 00:17:28,926
-What have you been up to?
-Same old same old, dude.

280
00:17:29,104 --> 00:17:31,664
Just dealing, growing.
Doing a little Pilates.

281
00:17:31,840 --> 00:17:34,138
-No.
-I love that shit, man.

282
00:17:34,309 --> 00:17:37,506
Stretch you out, dude. It give you power.
Make you feel good as hell.

283
00:17:37,679 --> 00:17:39,340
-Hot instructor.
-Ass like a peach.

284
00:17:39,514 --> 00:17:42,074
-You just wanna bite that shit.
-Nice, nice.

285
00:17:42,250 --> 00:17:45,617
So, Conrad, man,
can you hook me up with a little?

286
00:17:56,865 --> 00:17:58,628
That's a lot of weed, huh?

287
00:17:59,834 --> 00:18:01,563
You got the money?

288
00:18:01,736 --> 00:18:03,260
For that much, I don't....

289
00:18:03,438 --> 00:18:07,101
Great. Lady Saltine sends her little
errand boy with no scratch.

290
00:18:07,275 --> 00:18:09,175
Nancy?

291
00:18:09,511 --> 00:18:12,708
Yeah. She only paid for the half order
she picked up this morning.

292
00:18:12,881 --> 00:18:15,475
And that's the other half.

293
00:18:15,650 --> 00:18:17,618
You got my money, boy genius?

294
00:18:27,095 --> 00:18:30,690
So Nancy's getting pretty big, huh?

295
00:18:31,933 --> 00:18:33,264
Fucking kingpin.

296
00:18:40,342 --> 00:18:41,969
What's so funny?

297
00:18:42,143 --> 00:18:44,907
What? Oh, Conrad does Pilates.

298
00:18:47,248 --> 00:18:50,081
-Okay. Where was l?
-I'm surrounded.

299
00:18:50,251 --> 00:18:53,846
You're surrounded by the enemy.
The enemy edges in closer.

300
00:18:54,089 --> 00:18:57,252
Closer. Then, when they're just
inches away...

301
00:18:57,425 --> 00:19:00,394
...you invoke the Dong Hai defense.

302
00:19:00,562 --> 00:19:02,052
What's that?

303
00:19:02,764 --> 00:19:06,063
You protect your dong and you
swing like your life depended on it.

304
00:19:06,234 --> 00:19:07,861
Cool!

305
00:19:09,771 --> 00:19:12,103
I need to talk to you, you fucking perv.

306
00:19:13,908 --> 00:19:16,433
All right. Go practice that.

307
00:19:17,312 --> 00:19:19,872
What? Did that loony chick
from Alaska call?

308
00:19:20,048 --> 00:19:22,778
-I'm talking about what you did to Megan.
-Who's Megan?

309
00:19:22,951 --> 00:19:26,717
Megan. My girlfriend? You told her
to take her clothes off when she lM'd.

310
00:19:26,888 --> 00:19:29,550
Oh, that Megan. Yeah.
I did that for you, bro.

311
00:19:29,724 --> 00:19:31,419
-You should be thanking me.
-What?

312
00:19:31,593 --> 00:19:34,960
-That girl really digs you.
-No shit. She's my fucking girlfriend.

313
00:19:35,130 --> 00:19:37,121
But you haven't gotten
into her panties yet?

314
00:19:37,298 --> 00:19:40,495
-That's none of your business.
-You know why?

315
00:19:40,668 --> 00:19:43,967
You're afraid to make the big moves
because she's handicapped and shit.

316
00:19:44,139 --> 00:19:47,108
That's what you need to learn.
Treat them like everybody else.

317
00:19:47,275 --> 00:19:50,540
And then they pop right open
like a can of Pringles.

318
00:19:51,613 --> 00:19:54,946
-So....
-So she's ready, man.

319
00:19:55,116 --> 00:19:56,777
You gotta go for it.

320
00:19:56,951 --> 00:19:58,646
-You gonna see her tonight?
-Yeah.

321
00:19:58,820 --> 00:20:01,755
-Well?
-Silas, check me out.

322
00:20:03,992 --> 00:20:05,721
You better be right.

323
00:20:06,761 --> 00:20:09,525
Trust me, man. It's good to go.

324
00:20:16,771 --> 00:20:18,671
Hey, Pants.

325
00:20:19,374 --> 00:20:22,605
Please tell me I didn't hear
that you had cybersex...

326
00:20:22,777 --> 00:20:26,144
-...with a 15-year-old deaf girl.
-Absolutely not.

327
00:20:26,314 --> 00:20:28,214
-I want you out tonight.
-Really?

328
00:20:28,383 --> 00:20:30,977
-Because I was just cooking dinner.
-I want you out now.

329
00:20:31,152 --> 00:20:34,485
Don't you wanna hear my menu?
Because I got some really great recipes...

330
00:20:34,656 --> 00:20:38,592
...over at my friend Conrad's joint
earlier today.

331
00:20:38,760 --> 00:20:41,923
Pot roast, corned beef hash.

332
00:20:42,096 --> 00:20:45,122
Tonight, though, I'm gonna go ltalian
with a little baked ziti...

333
00:20:45,300 --> 00:20:47,359
...and a big plate
of spaghetti marijuana.

334
00:20:47,535 --> 00:20:49,264
-I mean--
-You sneaky piece of shit.

335
00:20:52,941 --> 00:20:57,605
Come, now, Nancy Pants.
Isn't that the pot calling the kettle black?

336
00:20:57,779 --> 00:21:01,044
-What did you do?
-Well, I went to my old friend Conrad's...

337
00:21:01,216 --> 00:21:04,049
...to catch up on old times--
You remember Conrad, don't you?

338
00:21:04,219 --> 00:21:06,881
He's a pot dealer.
He's the one you're supposed to call...

339
00:21:07,055 --> 00:21:08,920
...if you ever needed a little puff.

340
00:21:09,090 --> 00:21:11,581
--and much to my surprise...

341
00:21:11,759 --> 00:21:16,059
...I was handed a giant bag of weed...

342
00:21:16,231 --> 00:21:18,722
...and told it was for you.

343
00:21:18,900 --> 00:21:22,233
-Where's my stuff?
-So I thought:

344
00:21:22,403 --> 00:21:27,966
"Either Nancy's got a big problem,
or Nancy's got a lot of friends."

345
00:21:28,142 --> 00:21:29,370
You've been--?

346
00:21:29,544 --> 00:21:32,377
-You've been making friends, Nancy?
-Where's my stuff?

347
00:21:33,448 --> 00:21:34,676
Don't worry. It's safe.

348
00:21:46,561 --> 00:21:48,256
"Don't worry."

349
00:21:48,429 --> 00:21:50,090
Everything you touch turns to shit.

350
00:21:53,635 --> 00:21:56,399
I'm family. We'll work it out.

351
00:21:56,704 --> 00:21:59,332
I really want you to leave.

352
00:22:00,475 --> 00:22:03,842
Look, the way I see it is...

353
00:22:04,012 --> 00:22:06,310
...you're in way over your head here,
you know?

354
00:22:06,481 --> 00:22:09,382
You got a house, you got bills,
you're a mommy.

355
00:22:09,551 --> 00:22:11,781
Dealing is a full-time job.

356
00:22:12,320 --> 00:22:14,447
You need some help, Nancy Pants.

357
00:22:14,622 --> 00:22:18,558
Don't call me "Pants."

358
00:22:18,726 --> 00:22:22,719
-Judah called me "Pants," not you.
-Hey, I miss him too, Nancy.

359
00:22:22,897 --> 00:22:26,355
Whatever you think about me,
Judah was my brother.

360
00:22:26,534 --> 00:22:28,900
And I loved him.

361
00:22:29,404 --> 00:22:30,769
And I have your back.

362
00:22:32,240 --> 00:22:36,643
Now, I'm gonna go inside and I'm gonna
finish cooking dinner for the family.

363
00:22:38,012 --> 00:22:40,446
I'm making stoned crabs.
I had to get that one out.

364
00:22:40,615 --> 00:22:42,446
I was working on them
all the way home.

365
00:22:45,486 --> 00:22:47,283
Hey.

366
00:22:47,822 --> 00:22:49,153
You look hot in that leotard.

367
00:23:19,854 --> 00:23:22,049
Come on.

368
00:26:02,016 --> 00:26:04,075
I have cancer.

