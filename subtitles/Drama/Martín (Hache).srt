1
00:00:41,560 --> 00:00:43,915
One hour later

2
00:02:53,800 --> 00:02:56,473
We have orders to kill you
if you scream.

3
00:04:19,080 --> 00:04:22,550
Don't scream, Mme la Duchesse,

4
00:04:22,600 --> 00:04:24,477
I have a headache.

5
00:04:27,560 --> 00:04:29,437
ln fact, I will untie you.

6
00:04:51,760 --> 00:04:53,512
What good would your cries do?

7
00:04:54,520 --> 00:04:56,112
No one can hear you.

8
00:04:56,160 --> 00:04:59,755
May I ask what you intend
to do with me?

9
00:05:03,120 --> 00:05:05,156
Nothing at all, Madame.

10
00:05:17,800 --> 00:05:20,075
You are here for a short time.

11
00:05:22,760 --> 00:05:26,150
I will explain who you are,

12
00:05:26,200 --> 00:05:27,872
and who I am.

13
00:05:29,320 --> 00:05:33,871
When you wriggle on your sofa,
in your boudoir,

14
00:05:33,920 --> 00:05:37,071
I have no words for my thoughts.

15
00:05:38,560 --> 00:05:40,312
Here, my mind is unfettered.

16
00:05:43,560 --> 00:05:44,834
Fear nothing.

17
00:05:47,200 --> 00:05:49,236
Perhaps you think of rape,

18
00:05:49,840 --> 00:05:51,671
I wouldn't think of it.

19
00:05:57,360 --> 00:05:58,679
Madame,

20
00:06:00,880 --> 00:06:04,270
one minute, one will suffice for me

21
00:06:04,320 --> 00:06:06,880
to affect the rest of your life.

22
00:06:08,280 --> 00:06:12,478
You inspire a spirit of justice in me.

23
00:06:16,000 --> 00:06:18,560
Atone for your fault.

24
00:06:19,680 --> 00:06:24,037
God may forgive you, I wish it.

25
00:06:28,040 --> 00:06:30,600
Spare the tears, Madame.

26
00:06:33,480 --> 00:06:36,756
Nothing in you can move me now.

27
00:06:38,040 --> 00:06:39,871
All is said.

28
00:06:44,520 --> 00:06:47,318
You have a right to speak harshly,

29
00:06:59,600 --> 00:07:01,795
and I deserve this punishment.

30
00:07:06,320 --> 00:07:07,878
Armand,

31
00:07:09,320 --> 00:07:12,995
I thought as I resisted love,

32
00:07:13,040 --> 00:07:16,237
I was obeying
woman's nature of modesty.

33
00:07:18,960 --> 00:07:22,509
You take my weaknesses

34
00:07:22,560 --> 00:07:24,278
and make crimes of them.

35
00:07:25,560 --> 00:07:29,712
My severity spoke more of love
than of complacency.

36
00:07:32,840 --> 00:07:35,229
And besides,

37
00:07:35,280 --> 00:07:37,635
what is your grievance?

38
00:07:38,560 --> 00:07:42,235
My heart was not enough,

39
00:07:42,280 --> 00:07:44,999
you brutally demanded my person.

40
00:07:46,200 --> 00:07:47,349
Brutally!

41
00:07:50,640 --> 00:07:52,995
You came to me

42
00:07:53,040 --> 00:07:55,918
as with those bad women,

43
00:07:55,960 --> 00:07:57,837
with no respect,

44
00:08:00,040 --> 00:08:03,919
none of the attentions of love.

45
00:08:05,280 --> 00:08:07,919
Had I not the right to reflect?

46
00:08:11,160 --> 00:08:13,196
lf I did wrong,

47
00:08:17,160 --> 00:08:19,549
may I not be forgiven?

48
00:08:19,600 --> 00:08:21,397
May I not mend?

49
00:08:26,080 --> 00:08:27,559
Armand,

50
00:08:27,600 --> 00:08:32,594
these women you take me for
give themselves, but they fight.

51
00:08:32,640 --> 00:08:35,712
Well, I fought,

52
00:08:35,760 --> 00:08:37,398
but here I am.

53
00:08:42,880 --> 00:08:44,472
I love you!

54
00:08:45,840 --> 00:08:47,671
I am yours!

55
00:08:49,960 --> 00:08:51,313
Madame,

56
00:08:51,360 --> 00:08:54,591
Antoinette cannot save
the Duchesse de Langeais.

57
00:08:54,640 --> 00:08:57,473
I believe neither one nor the other.

58
00:09:03,120 --> 00:09:06,317
Armand, who are these men?

59
00:09:06,360 --> 00:09:08,351
What will you do to me?

60
00:09:11,960 --> 00:09:17,318
These men are as discreet as I am
on what will be done here.

61
00:09:18,320 --> 00:09:20,231
Look at them as my instruments.

62
00:09:21,120 --> 00:09:22,792
One of them is a surgeon.

63
00:09:24,240 --> 00:09:25,753
Surgeon?

64
00:09:29,440 --> 00:09:30,555
Speak,

65
00:09:32,120 --> 00:09:34,270
tell me if it's my life you want,

66
00:09:34,320 --> 00:09:35,912
I will give it to you.

67
00:09:37,880 --> 00:09:39,757
You don't understand?

68
00:09:43,400 --> 00:09:45,470
Did I not speak of justice?

69
00:10:02,160 --> 00:10:03,991
Look at this star.

70
00:10:05,000 --> 00:10:07,434
We will stamp it on the forehead,

71
00:10:07,480 --> 00:10:09,357
here,

72
00:10:09,400 --> 00:10:11,755
between the eyes...

73
00:10:11,800 --> 00:10:14,189
You will not hide it with diamonds.

74
00:10:14,240 --> 00:10:16,435
Your forehead will have the mark

75
00:10:16,480 --> 00:10:19,040
applied on the shoulders of convicts.

76
00:10:22,920 --> 00:10:25,070
My Armand,

77
00:10:25,120 --> 00:10:28,476
brand your creature quickly

78
00:10:28,520 --> 00:10:31,478
as your poor little trifle.

79
00:10:32,760 --> 00:10:35,194
I see but mercy

80
00:10:35,240 --> 00:10:37,117
and forgiveness,

81
00:10:40,000 --> 00:10:42,639
but eternal joy in your vengeance.

82
00:10:44,480 --> 00:10:47,677
By marking a woman

83
00:10:47,720 --> 00:10:49,870
as yours,

84
00:10:51,560 --> 00:10:55,189
with your red brand,

85
00:10:55,240 --> 00:10:56,673
well,

86
00:10:58,400 --> 00:11:00,630
you can never abandon me,

87
00:11:02,320 --> 00:11:04,356
you will be mine forever.

88
00:11:07,640 --> 00:11:09,756
Come in,

89
00:11:09,800 --> 00:11:11,552
come gentlemen,

90
00:11:13,440 --> 00:11:16,512
and mark the Duchesse de Langeais.

91
00:11:16,560 --> 00:11:19,393
She is forever more
to M. de Montriveau.

92
00:11:19,440 --> 00:11:21,715
Come quickly,

93
00:11:21,760 --> 00:11:24,228
my forehead is hotter than your iron.

94
00:11:36,760 --> 00:11:38,751
I spare you, Madame.

95
00:11:40,720 --> 00:11:43,234
lt's as if it never happened.

96
00:11:48,800 --> 00:11:50,677
But here

97
00:11:50,720 --> 00:11:52,233
let us bid goodbye.

98
00:11:52,280 --> 00:11:53,713
Goodbye?

99
00:11:54,720 --> 00:11:55,948
Goodbye.

100
00:11:57,680 --> 00:11:59,636
I have lost faith.

101
00:12:01,320 --> 00:12:05,279
You would torment me,
you would always be the Duchess, and...

102
00:12:06,400 --> 00:12:09,358
nothing.

103
00:12:10,040 --> 00:12:11,075
Goodbye.

104
00:12:15,000 --> 00:12:17,275
What do you wish?

105
00:12:17,320 --> 00:12:20,198
To go home or return
to Mme de S�rizy's?

106
00:12:21,680 --> 00:12:23,193
Where do you want to be?

107
00:12:24,160 --> 00:12:25,593
Take me to the ball.

108
00:12:27,360 --> 00:12:31,035
Throw to hellish society
one who suffers in it,

109
00:12:31,080 --> 00:12:33,674
and will continue to suffer in it

110
00:12:33,720 --> 00:12:35,790
if there is no happiness for her.

111
00:12:39,840 --> 00:12:41,956
lf I leave,

112
00:12:42,000 --> 00:12:44,833
allow me to take something.

113
00:12:44,880 --> 00:12:47,314
Anything! This.

114
00:12:49,280 --> 00:12:51,589
When you wish to leave, tell me.

115
00:12:52,760 --> 00:12:55,228
- I wish to stay.
- Out of the question.

116
00:13:03,040 --> 00:13:06,874
Here. This was poorly wet!

117
00:13:06,920 --> 00:13:10,071
- You smoke?
- What wouldn't I do to please you?

118
00:13:11,080 --> 00:13:12,638
Go away, Madame.

119
00:13:17,400 --> 00:13:18,628
I obey.

120
00:13:21,520 --> 00:13:26,196
We must cover you
so you may not see the way.

121
00:13:36,720 --> 00:13:38,551
I am ready, Armand.

122
00:13:41,520 --> 00:13:42,873
Can you see?

123
00:13:42,920 --> 00:13:44,273
No.

124
00:13:48,280 --> 00:13:49,713
I can hear you.

125
00:13:49,760 --> 00:13:51,751
You can see!

126
00:13:51,800 --> 00:13:53,677
A bit.

127
00:13:53,720 --> 00:13:55,836
You deceive me still?

128
00:14:05,880 --> 00:14:08,075
Lead me, Monsieur,

129
00:14:08,120 --> 00:14:09,712
I shall not open my eyes.

130
00:14:29,280 --> 00:14:32,078
My friend, I love you

131
00:14:32,120 --> 00:14:34,156
as your bourgeois love you.

132
00:14:35,600 --> 00:14:38,592
This society has not corrupted me.

133
00:14:40,480 --> 00:14:43,870
I am young
and have just become younger.

134
00:14:45,040 --> 00:14:47,190
I am a child,

135
00:14:47,240 --> 00:14:49,310
your child,

136
00:14:49,360 --> 00:14:51,316
you have just created me.

137
00:14:53,360 --> 00:14:56,079
Don't banish me from my Eden.

138
00:14:56,120 --> 00:14:57,439
Move ahead.

139
00:15:26,680 --> 00:15:28,750
My dear Antoinette,

140
00:15:28,800 --> 00:15:30,756
we were looking for you everywhere.

141
00:15:30,800 --> 00:15:33,109
I came here to breathe.

142
00:15:34,760 --> 00:15:38,309
The reception rooms are unbearably hot.

143
00:15:39,360 --> 00:15:40,554
Goodbye, my dear.

144
00:16:23,280 --> 00:16:24,793
I love him...

145
00:16:24,840 --> 00:16:28,628
A week long, she looks for him
at balls and receptions...

146
00:17:25,680 --> 00:17:28,035
in vain.

147
00:17:35,560 --> 00:17:39,269
What is this impatience, my dear?

148
00:17:42,800 --> 00:17:44,631
Another cup of tea?

149
00:17:46,240 --> 00:17:49,038
For the tea, I say yes.

150
00:18:00,960 --> 00:18:03,269
You have fallen out
with M. de Montriveau?

151
00:18:05,120 --> 00:18:07,111
He doesn't come here anymore?

152
00:18:08,160 --> 00:18:10,230
He is nowhere to be seen,

153
00:18:11,640 --> 00:18:14,154
he is surely busy with a woman...

154
00:18:17,720 --> 00:18:19,278
So you miss him.

155
00:18:23,840 --> 00:18:27,435
I heard monstrous things about him.

156
00:18:29,080 --> 00:18:32,231
Hurt him, he never comes back,
forgives nothing.

157
00:18:32,960 --> 00:18:34,871
Love him, he keeps you in chains.

158
00:18:38,560 --> 00:18:42,314
Those who praise him
say he's a great soul.

159
00:18:42,360 --> 00:18:45,909
Society does not care for great souls.

160
00:18:47,640 --> 00:18:51,155
Men of that stamp are best at home.

161
00:18:51,200 --> 00:18:52,633
May they remain there.

162
00:18:57,320 --> 00:18:59,675
I am cross not to see him.

163
00:19:03,080 --> 00:19:05,594
I had a sincere amity for him...

164
00:19:09,320 --> 00:19:11,072
I love great souls.

165
00:19:19,360 --> 00:19:20,429
Go, Lisette.

166
00:19:21,040 --> 00:19:22,553
I don't want to be disturbed.

167
00:20:15,120 --> 00:20:18,908
The next day, at dawn,
her letter is carried by Julien.

168
00:20:21,120 --> 00:20:22,792
Did you see him?

169
00:20:22,840 --> 00:20:24,034
Yes, Madame.

170
00:20:27,600 --> 00:20:29,113
He is in Paris then!

171
00:20:30,640 --> 00:20:32,915
How is he? What did he say?

172
00:20:35,040 --> 00:20:36,917
Tell me everything, Julien.

173
00:20:36,960 --> 00:20:40,270
M. le Marquis said he would come
to Mme la Duchesse.

174
00:20:40,320 --> 00:20:41,833
He is coming!

175
00:20:44,280 --> 00:20:45,713
I need Lisette.

176
00:22:26,800 --> 00:22:28,233
The fire is dying.

177
00:22:31,760 --> 00:22:33,716
I want big flames.

178
00:22:52,640 --> 00:22:54,198
Hand me my shawl.

179
00:23:07,880 --> 00:23:08,995
Madame,

180
00:23:09,680 --> 00:23:11,398
it is ten o'clock...

181
00:23:19,320 --> 00:23:20,719
I will go.

182
00:23:22,277 --> 00:23:24,632
But, the following morning...

183
00:24:40,037 --> 00:24:41,026
Yes?

184
00:24:46,317 --> 00:24:51,186
M. le Marquis bids I tell
Mme la Duchesse it was well.

185
00:24:51,237 --> 00:24:52,272
Well?

186
00:25:05,877 --> 00:25:07,310
For twenty-two days,

187
00:25:07,357 --> 00:25:10,315
Mme de Langeais
wrote to M. de Montriveau,

188
00:25:10,357 --> 00:25:11,585
with no reply.

189
00:25:32,317 --> 00:25:35,832
And don't forget that Spinoza

190
00:25:35,877 --> 00:25:37,549
in his third volume...

191
00:25:38,317 --> 00:25:39,796
Oh,

192
00:25:39,837 --> 00:25:41,236
what is it?

193
00:25:41,277 --> 00:25:43,711
Mme de Langeais' carriage.

194
00:25:43,757 --> 00:25:44,985
lt is empty?

195
00:25:45,677 --> 00:25:46,996
lt appears so.

196
00:25:47,037 --> 00:25:49,949
ln front of General de Montriveau's!

197
00:25:49,997 --> 00:25:52,033
- Damn!
- Damn.

198
00:25:52,637 --> 00:25:53,752
What of it?

199
00:26:01,797 --> 00:26:05,107
Mme de Langeais
commits the noblest imprudence!

200
00:26:05,157 --> 00:26:07,227
Mme de Langeais is a heroic woman.

201
00:26:07,277 --> 00:26:10,235
Now, she can love but Montriveau.

202
00:26:10,277 --> 00:26:13,952
What will society become, Monsieur,
if you honour vice,

203
00:26:13,997 --> 00:26:15,953
and forget virtue!

204
00:26:15,997 --> 00:26:19,069
Before all of Paris,
to renounce for one's lover,

205
00:26:19,117 --> 00:26:20,709
to society,

206
00:26:20,757 --> 00:26:23,032
to fortune, and one's rank...

207
00:26:23,077 --> 00:26:24,874
A feminine coup d'�tat!

208
00:26:37,197 --> 00:26:38,676
Dear aunt,

209
00:26:38,717 --> 00:26:42,187
do you really think
my daughter is at M. de Montriveau's?

210
00:26:49,077 --> 00:26:51,033
What do you think, Vidame?

211
00:26:52,397 --> 00:26:55,070
lf the Duchess were na.i.ve, l'd say...

212
00:26:55,117 --> 00:26:58,507
A woman who loves becomes na.i.ve,
my poor Vidame.

213
00:26:59,797 --> 00:27:01,992
You are getting old then?

214
00:27:02,037 --> 00:27:03,993
But what is to be done?

215
00:27:04,037 --> 00:27:06,790
There are a thousand ways to fix it.

216
00:27:06,837 --> 00:27:09,431
lf M. de Montriveau is gallant, he will.

217
00:27:09,477 --> 00:27:13,436
You don't know Montriveau,
my dear aunt.

218
00:27:13,477 --> 00:27:15,752
He's a lord of the day,

219
00:27:17,877 --> 00:27:19,913
a pupil of Bonaparte,

220
00:27:21,477 --> 00:27:24,116
with a major command in the Guard,

221
00:27:24,157 --> 00:27:25,988
and not the slightest ambition.

222
00:27:27,237 --> 00:27:30,866
At the first ill word,
he might tell the King:

223
00:27:30,917 --> 00:27:34,387
"Here is my resignation,
leave me alone."

224
00:27:38,357 --> 00:27:39,790
My child,

225
00:27:40,757 --> 00:27:42,315
what is happening?

226
00:27:43,797 --> 00:27:46,436
What is happening, dear father?

227
00:27:46,477 --> 00:27:49,787
Paris thinks you are
at M. de Montriveau's.

228
00:27:53,477 --> 00:27:56,753
My dear Antoinette, you didn't go out?

229
00:27:56,797 --> 00:27:59,994
No, I did not.

230
00:28:02,677 --> 00:28:05,908
But I want all of Paris to think
l'm at Mr. de Montriveau's.

231
00:28:07,677 --> 00:28:10,350
Don't you know the result?

232
00:28:11,277 --> 00:28:14,155
You embarrass your husband,
your rank in society.

233
00:28:14,197 --> 00:28:18,236
We're no longer under the Valois,
we can still fix everything.

234
00:28:18,277 --> 00:28:21,269
My aunt, I don't want to fix anything.

235
00:28:24,317 --> 00:28:28,549
I want all of Paris to say I was
this morning at M. de Montriveau's.

236
00:28:29,437 --> 00:28:32,986
Antoinette, a woman must never
give reason to her husband.

237
00:28:36,877 --> 00:28:39,675
I thought like you, dear uncle,

238
00:28:39,717 --> 00:28:42,106
as long as I didn't love.

239
00:28:43,357 --> 00:28:45,427
I saw but interests,

240
00:28:46,437 --> 00:28:48,905
where I now see but sentiment.

241
00:28:48,957 --> 00:28:51,755
But, my dear,

242
00:28:51,797 --> 00:28:56,587
life is simply a complication
of interests and sentiment.

243
00:28:57,837 --> 00:29:01,671
To be happy, in your position,

244
00:29:01,717 --> 00:29:06,552
one must reconcile
sentiment and interest.

245
00:29:06,597 --> 00:29:10,954
I shall be eighty and don't recall,
under any regime,

246
00:29:10,997 --> 00:29:16,993
encountering a love worth
the price you are willing to pay.

247
00:29:26,677 --> 00:29:29,828
My child, you speak of sentiment.

248
00:29:31,277 --> 00:29:36,829
A Navarreins cannot do certain things
without failing his house.

249
00:29:36,877 --> 00:29:39,994
You would not be dishonoured alone.

250
00:29:41,477 --> 00:29:43,752
Dishonour.

251
00:29:43,797 --> 00:29:48,234
Such a fuss over an empty carriage.

252
00:29:49,317 --> 00:29:53,708
Please be so kind to retire.
Leave me alone with Antoinette.

253
00:29:55,117 --> 00:29:56,311
Go on!

254
00:30:13,517 --> 00:30:15,394
My little gem,

255
00:30:15,437 --> 00:30:18,156
no man is worth the sacrifices

256
00:30:18,197 --> 00:30:21,826
we are crazy enough to pay their love.

257
00:30:21,877 --> 00:30:23,549
- My dear aunt...
- Listen to me.

258
00:30:23,597 --> 00:30:26,794
I know by experience
you will do as you please.

259
00:30:26,837 --> 00:30:28,987
I would have done the same at your age.

260
00:30:29,037 --> 00:30:34,589
But, my darling, don't give away the right
to make Dukes of Langeais.

261
00:30:34,637 --> 00:30:40,109
Stay in a position
to be your husband's wife.

262
00:30:40,157 --> 00:30:43,467
An imprudence means a stipend,

263
00:30:43,517 --> 00:30:45,428
a wandering life...

264
00:30:45,477 --> 00:30:48,355
At the lover's mercy.

265
00:30:48,397 --> 00:30:53,517
A thousand times better to go
to Montriveau's at night, in a cab, disguised,

266
00:30:53,557 --> 00:30:58,347
than sending your carriage in broad daylight.
You're a little fool, my dear child!

267
00:30:58,397 --> 00:31:00,786
Your carriage flattered his vanity,

268
00:31:00,837 --> 00:31:02,953
your person
would have taken his heart.

269
00:31:04,557 --> 00:31:06,115
My aunt, don't slander him.

270
00:31:06,157 --> 00:31:09,547
Don't vex anyone,
neither him, nor us.

271
00:31:09,597 --> 00:31:11,428
I'll take care of pleasing all.

272
00:31:11,477 --> 00:31:14,787
But promise not to do anything
without consulting with me.

273
00:31:14,837 --> 00:31:15,952
I promise.

274
00:31:15,997 --> 00:31:19,433
- To tell me everything...
- Everything that can be told.

275
00:31:19,477 --> 00:31:24,756
Dearest, precisely what cannot be told,
I wish to know.

276
00:31:28,117 --> 00:31:31,154
Go on, lead me to my carriage.

277
00:31:35,637 --> 00:31:37,389
Dear aunt,

278
00:31:37,437 --> 00:31:39,712
I may then go to him in disguise?

279
00:31:39,757 --> 00:31:42,874
lndeed, it can always be denied.

280
00:31:43,757 --> 00:31:47,716
Two days later, Mme de Langeais
wrote M. de Montriveau another letter,

281
00:31:47,757 --> 00:31:48,951
yet with no reply.

282
00:31:48,997 --> 00:31:51,386
But, this time, she took measures...

283
00:32:17,677 --> 00:32:20,271
When is M. de Montriveau to come?

284
00:32:20,317 --> 00:32:23,195
He will not return tonight, Madame.

285
00:32:27,997 --> 00:32:30,795
I don't recognize anything.

286
00:32:30,837 --> 00:32:32,589
Does he have two homes?

287
00:32:36,157 --> 00:32:38,273
You will not answer.

288
00:32:42,797 --> 00:32:44,435
I understand.

289
00:32:44,477 --> 00:32:48,390
I only paid the right to enter.
Very well.

290
00:34:39,557 --> 00:34:41,513
A carriage,

291
00:34:41,557 --> 00:34:43,070
quick!

292
00:34:45,197 --> 00:34:48,109
After twenty-four hours of reflection,

293
00:34:48,157 --> 00:34:50,910
she sent for the old Vidame de Pamiers.

294
00:35:15,797 --> 00:35:17,469
M. the Vidame de Pamiers.

295
00:35:25,317 --> 00:35:28,389
Dear child, you are very pale,

296
00:35:28,437 --> 00:35:30,746
did you not sleep last night?

297
00:35:30,797 --> 00:35:34,631
Don't smile, I beg of you,

298
00:35:34,677 --> 00:35:37,510
at a woman who is so wretched.

299
00:35:43,317 --> 00:35:45,990
You may be the last relative,

300
00:35:46,037 --> 00:35:48,835
the last friend whose hand I shake.

301
00:35:50,277 --> 00:35:51,505
Grant me, dear Vidame,

302
00:35:51,557 --> 00:35:55,550
a service I could not ask my father,

303
00:35:55,597 --> 00:35:57,713
or my uncle Grandlieu...

304
00:35:57,757 --> 00:36:00,066
or any woman.

305
00:36:10,317 --> 00:36:12,877
I beg you to obey me

306
00:36:12,917 --> 00:36:15,909
and forget you obeyed,

307
00:36:15,957 --> 00:36:18,391
whatever the result...

308
00:36:19,437 --> 00:36:20,836
You must go,

309
00:36:23,037 --> 00:36:26,347
with this letter,
to M. de Montriveau,

310
00:36:26,397 --> 00:36:27,750
and see him,

311
00:36:29,077 --> 00:36:31,307
show it to him,

312
00:36:31,357 --> 00:36:34,952
ask him to read it.

313
00:36:36,877 --> 00:36:39,869
You may, to bring him to it,

314
00:36:39,917 --> 00:36:43,148
say it is a matter of my life or death.

315
00:36:43,197 --> 00:36:45,711
- lf he deigns...
- lf he deigns?

316
00:36:46,997 --> 00:36:49,147
lf he deigns read it,

317
00:36:49,197 --> 00:36:52,906
say one more thing:

318
00:36:54,557 --> 00:36:58,027
he must, for any answer, see me.

319
00:36:59,637 --> 00:37:01,992
You will find him at five o'clock.

320
00:37:03,197 --> 00:37:06,075
- Are you sure?
- I know it.

321
00:37:09,117 --> 00:37:12,314
lf three hours later, at eight,

322
00:37:12,357 --> 00:37:14,587
he has not come out,

323
00:37:15,557 --> 00:37:17,036
all is said.

324
00:37:18,957 --> 00:37:22,154
The Duchesse de Langeais
will have left this world.

325
00:37:23,757 --> 00:37:24,826
My dear child...

326
00:37:24,877 --> 00:37:28,426
I don't wish to hear
any kind of comment

327
00:37:28,477 --> 00:37:30,308
or advice...

328
00:38:11,877 --> 00:38:15,153
"lf you love me, cease this cruel game,

329
00:38:15,197 --> 00:38:16,596
"you would kill me.

330
00:38:18,077 --> 00:38:20,227
"I want to be irresistibly loved,

331
00:38:20,277 --> 00:38:23,906
"or left ruthlessly.

332
00:38:24,637 --> 00:38:28,755
"lf you refuse to read this letter,
it will be burned.

333
00:38:28,797 --> 00:38:31,630
"lf having read it,
you are not three hours later,

334
00:38:31,677 --> 00:38:35,272
"forever, my only spouse,

335
00:38:35,317 --> 00:38:37,877
"my end will honour my love...

336
00:38:40,517 --> 00:38:44,829
"lf, having read it,
you are not three hours later..."

337
00:38:51,437 --> 00:38:53,314
These gentlemen have just arrived.

338
00:38:53,357 --> 00:38:54,472
Have them wait.

339
00:39:05,157 --> 00:39:07,113
Good evening Marsay.

340
00:39:07,157 --> 00:39:08,954
Hello de Trailles.

341
00:39:33,557 --> 00:39:35,513
Mission accomplished.

342
00:39:35,557 --> 00:39:37,593
What did he say?

343
00:39:37,637 --> 00:39:39,832
Nothing. He kept silent.

344
00:39:43,877 --> 00:39:45,913
Stay and dine with me.

345
00:39:45,957 --> 00:39:47,754
Let us talk, laugh.

346
00:39:47,797 --> 00:39:50,265
Let us be like two old philosophers.

347
00:39:54,397 --> 00:39:56,115
You are a gallant man

348
00:39:56,997 --> 00:39:59,670
and the adventures of your youth have,

349
00:39:59,717 --> 00:40:03,551
l'd like to believe,
made you indulgent with women.

350
00:40:03,597 --> 00:40:04,996
Not the least.

351
00:40:05,037 --> 00:40:06,265
Really?

352
00:40:06,317 --> 00:40:08,433
Anything pleases them.

353
00:40:12,717 --> 00:40:15,151
I think he is sublime!

354
00:40:15,197 --> 00:40:18,473
Sublime, not at all!

355
00:40:18,517 --> 00:40:19,996
Don't ever say sublime!

356
00:40:20,037 --> 00:40:25,031
Divine, adorable, marvellous...
old style!

357
00:40:25,077 --> 00:40:29,195
The proper word is "stunning"!

358
00:40:29,237 --> 00:40:31,956
For instance, Fragoletta de Latouche,

359
00:40:31,997 --> 00:40:35,433
the preface is stunning!

360
00:40:36,517 --> 00:40:38,075
At the opera, yesterday,

361
00:40:38,117 --> 00:40:41,712
the Tinti was stunning!

362
00:41:33,837 --> 00:41:35,236
Dear Vidame,

363
00:41:36,797 --> 00:41:40,312
be so gracious and ask if he is in.

364
00:42:20,477 --> 00:42:21,910
He is indeed.

365
00:42:27,157 --> 00:42:30,547
Now you must leave me.

366
00:42:31,477 --> 00:42:33,991
I no longer need protection.

367
00:42:42,117 --> 00:42:43,869
But the passer-by?

368
00:42:44,917 --> 00:42:46,987
None can show me disrespect.

369
00:43:44,317 --> 00:43:46,148
And also,

370
00:43:46,197 --> 00:43:48,267
there is poetry.

371
00:43:48,317 --> 00:43:51,389
For the Taglioni,
Mme de Maufrigneuse says,

372
00:43:51,437 --> 00:43:53,667
"There is poetry in her dance."

373
00:43:54,357 --> 00:43:55,756
Even better:

374
00:43:57,037 --> 00:43:58,948
there is drama!

375
00:43:58,997 --> 00:44:03,195
Bonaparte, there is drama!

376
00:44:05,277 --> 00:44:07,268
What do you say, Montriveau?

377
00:44:10,197 --> 00:44:11,949
Yes?

378
00:44:11,997 --> 00:44:14,750
Latouche, there is drama!

379
00:44:14,797 --> 00:44:17,357
Fragoletta, what drama!

380
00:44:17,397 --> 00:44:19,388
There is drama in this book!

381
00:44:21,757 --> 00:44:23,031
Or else,

382
00:44:23,957 --> 00:44:27,108
you're by a clear expanse of water,

383
00:44:27,157 --> 00:44:29,751
on a calm evening

384
00:44:29,797 --> 00:44:32,357
with a proper young maid...

385
00:44:32,397 --> 00:44:34,786
Proper, but...

386
00:44:34,837 --> 00:44:36,907
stunning!

387
00:44:38,997 --> 00:44:41,306
Stunning but...

388
00:44:41,357 --> 00:44:42,631
proper.

389
00:44:43,477 --> 00:44:45,195
You say,

390
00:44:45,237 --> 00:44:46,795
"There is drama here!"

391
00:45:28,277 --> 00:45:30,154
My God...

392
00:45:40,477 --> 00:45:42,069
For instance,

393
00:45:44,317 --> 00:45:47,753
I went to see my uncle at Pierrefonds.

394
00:45:47,797 --> 00:45:49,867
That man is rich,

395
00:45:50,917 --> 00:45:52,714
he has horses,

396
00:45:52,757 --> 00:45:59,708
well, he doesn't know what a tiger is,
a groom, a britschka,

397
00:45:59,757 --> 00:46:02,715
and still travels in a cabriolet.

398
00:46:03,757 --> 00:46:05,270
Poor fellow.

399
00:46:14,237 --> 00:46:15,716
Gentlemen,

400
00:46:15,757 --> 00:46:17,156
farewell.

401
00:46:18,277 --> 00:46:20,188
Some business obliges me to leave.

402
00:46:53,557 --> 00:46:55,070
Hell's Boulevard,

403
00:46:55,117 --> 00:46:58,507
for the last time, she looked
at the noisy, smoky Paris,

404
00:46:58,557 --> 00:47:01,196
bathed in the red of the lights.

405
00:47:52,637 --> 00:47:55,595
And then... what?

406
00:47:57,037 --> 00:48:00,154
She was at my door at eight.

407
00:48:00,197 --> 00:48:02,347
At a quarter past eight
she disappeared.

408
00:48:05,837 --> 00:48:07,350
I lost her...

409
00:48:16,037 --> 00:48:19,666
lf my life were mine,
l'd have blown my brains.

410
00:48:22,917 --> 00:48:24,111
Calm down.

411
00:48:26,157 --> 00:48:28,990
Duchesses don't fly off like wagtails.

412
00:48:30,237 --> 00:48:31,636
They need a carriage,

413
00:48:33,237 --> 00:48:35,114
these angels have no wings.

414
00:48:38,757 --> 00:48:41,669
On the road, or hiding in Paris,
we will find her.

415
00:48:47,917 --> 00:48:49,236
So we see you tomorrow.

416
00:48:52,237 --> 00:48:53,909
Sleep, if you can.

417
00:49:14,717 --> 00:49:18,107
Neither Montriveau, nor his friends
found trace of the Duchess.

418
00:49:17,757 --> 00:49:20,032
She had obviously chosen the cloister.

419
00:49:32,717 --> 00:49:36,187
A few months after the last interview,

420
00:49:36,237 --> 00:49:41,311
a merchant ship left Marseille
and headed for Spain.

421
00:52:30,077 --> 00:52:31,669
lt's agreed,

422
00:52:31,717 --> 00:52:35,073
neither ruse, nor force

423
00:52:35,117 --> 00:52:37,392
will succeed in the city.

424
00:52:38,517 --> 00:52:39,791
Fergus.

425
00:52:41,677 --> 00:52:43,827
Ruse, no. But force!

426
00:52:44,957 --> 00:52:47,471
We destroy the city, and the convent.

427
00:52:50,437 --> 00:52:54,350
Like pirates, we burn all,
and leave the rest

428
00:52:54,397 --> 00:52:55,989
for the crows.

429
00:52:56,037 --> 00:52:57,516
What do you say, Montriveau?

430
00:53:00,197 --> 00:53:03,030
I could care less for the lives,

431
00:53:04,357 --> 00:53:06,313
but I prefer being discreet.

432
00:53:09,277 --> 00:53:11,711
Unless there is a third way.

433
00:53:11,757 --> 00:53:13,634
There are but two.

434
00:53:14,477 --> 00:53:16,832
Either arms which frighten Europe...

435
00:53:16,877 --> 00:53:21,871
or a ghostly kidnapping, mysterious,

436
00:53:21,917 --> 00:53:26,513
convincing the nuns
they were visited by the devil.

437
00:53:26,557 --> 00:53:28,229
More difficult,

438
00:53:29,157 --> 00:53:32,035
but more elegant.
I also prefer.

439
00:53:33,557 --> 00:53:35,309
Who votes for the brutal solution?

440
00:53:37,517 --> 00:53:38,916
Then it will be the other.

441
00:53:39,997 --> 00:53:42,795
We attack
from where it seems impractical.

442
00:53:42,837 --> 00:53:45,954
And all this, without a sound.

443
00:53:45,997 --> 00:53:48,352
A few hours later

444
00:55:37,757 --> 00:55:39,031
What is this singing?

445
00:55:41,437 --> 00:55:42,506
I shall see.

446
00:55:43,597 --> 00:55:44,666
Move ahead.

447
00:56:13,037 --> 00:56:15,187
All the nuns are in the church.

448
00:58:37,437 --> 00:58:40,793
She was a woman, now she is nothing.

449
00:58:43,517 --> 00:58:47,226
Let's tie a ball to each foot,
and throw her to sea.

450
00:58:48,597 --> 00:58:51,714
Don't think of it but as a book,

451
00:58:51,757 --> 00:58:54,146
read in your childhood.

452
00:58:55,677 --> 00:58:57,633
Yes.

453
00:58:57,677 --> 00:58:59,588
lt is but a poem.

