0
00:00:01,000 --> 00:00:08,600
{\a4\3c&HE05310&\fs15\i1} 
<font color="FFFF00"> Re Adaptation By: Ali Nabawy</font>
<font color="FFFF00"> ��� ������ ������� : ��� �����</font>
<font color="FFFF00"> ����� ���� ������� �����������</font>
http://wwwsevenart.blogspot.com/
Alynabawy@Hotmail.Com 
analazyz@yahoo.com

1
00:00:11,525 --> 00:00:13,899
SIMON OF THE DESERT

2
00:01:37,201 --> 00:01:40,882
Since 6 years, 6 weeks, 
6 days, Simon...

3
00:01:41,230 --> 00:01:46,441
... you have been on this column
for the edification of all.

4
00:01:46,742 --> 00:01:49,075
Go now on this other one 

5
00:01:49,417 --> 00:01:52,887
...offered by the rich Praxed?...

6
00:01:53,159 --> 00:01:54,680
...according to your wish.

7
00:01:54,950 --> 00:01:57,349
From here, you'll continue...

8
00:01:57,632 --> 00:02:01,081
to revive your brothers' fervour...

9
00:02:01,353 --> 00:02:06,302
by following our father's way,
Simeon Stylit.

10
00:02:06,722 --> 00:02:10,883
It's the tribute paid by a family...

11
00:02:11,179 --> 00:02:13,562
to which you have brought back happiness...

12
00:02:13,865 --> 00:02:16,411
in curing me from the accursed evil.

13
00:03:09,241 --> 00:03:10,616
Wait !

14
00:03:16,957 --> 00:03:18,290
Here is your mother, Simon.

15
00:03:18,644 --> 00:03:21,171
She wants to live by your side
until she dies.

16
00:03:21,532 --> 00:03:23,240
Let her kiss you for the last time.

17
00:03:23,587 --> 00:03:26,943
You should rather have come 
back to your place, Woman...

18
00:03:28,300 --> 00:03:32,800
Obey. Receive her in joy.

19
00:03:36,312 --> 00:03:39,001
Stay, if it is your wish.

20
00:03:39,326 --> 00:03:42,401
By this kiss, I bid you farewell.

21
00:03:43,308 --> 00:03:47,453
Nothing must interfere
between God and his servant.

22
00:03:47,789 --> 00:03:48,485
Farewell.

23
00:03:48,830 --> 00:03:51,881
In His Presence,
we will meet again. 

24
00:04:02,985 --> 00:04:04,568
Your benediction, Father.

25
00:04:04,930 --> 00:04:08,379
I'll give you more.
From my hand, you will receive...

26
00:04:08,709 --> 00:04:10,774
.. the priestly ordination.

27
00:04:20,577 --> 00:04:22,649
Prepare yourself, my son.

28
00:04:25,163 --> 00:04:25,903
No!

29
00:04:26,290 --> 00:04:29,086
I don't deserve this!

30
00:04:34,524 --> 00:04:38,538
I'm a sinner
unworthy of this grace.

31
00:04:39,300 --> 00:04:42,730
Follow your ordeal...
Go in peace...

32
00:04:43,067 --> 00:04:43,978
...for the moment.

33
00:05:19,601 --> 00:05:21,288
Brother Simon.

34
00:05:27,221 --> 00:05:29,380
Pray, Brothers, pray !

35
00:05:29,685 --> 00:05:32,524
Our father who art in heaven...

36
00:05:34,401 --> 00:05:37,194
Hallowed be thy name...

37
00:05:39,380 --> 00:05:41,713
thy kingdom come...

38
00:05:44,024 --> 00:05:46,307
thy will be done...

39
00:05:48,110 --> 00:05:50,577
On earth as it is in heaven.

40
00:05:53,901 --> 00:05:55,331
Blessed one...

41
00:05:55,711 --> 00:05:57,639
Those stumps were hands...

42
00:05:58,035 --> 00:06:01,848
Our poverty is great,
our daughters are starving,

43
00:06:03,529 --> 00:06:06,305
How did this occur?

44
00:06:06,795 --> 00:06:09,002
They cut them!

45
00:06:09,367 --> 00:06:10,899
Why ?

46
00:06:11,244 --> 00:06:13,201
Because he has stolen.

47
00:06:13,565 --> 00:06:14,942
Yes, it's true...

48
00:06:15,250 --> 00:06:16,932
... but I redeemed.

49
00:06:17,386 --> 00:06:20,130
Father, relieve my misery.

50
00:06:20,516 --> 00:06:23,254
Help us!
Have mercy of those children.

51
00:06:28,786 --> 00:06:31,216
I can only pray.

52
00:06:31,576 --> 00:06:34,303
Pray with me, ... silently.

53
00:06:41,490 --> 00:06:45,144
God will maybe let us see a miracle
from Simon.

54
00:06:45,497 --> 00:06:47,050
Thus it is.

55
00:06:58,076 --> 00:07:00,036
Now...

56
00:07:00,613 --> 00:07:04,649
Thank God.
Go back to your occupations.

57
00:07:11,355 --> 00:07:15,069
Let's go back home, 
we have to dig up the vegetable garden.

58
00:07:15,509 --> 00:07:18,754
We have to buy a new spade,
ours is worn.

59
00:07:23,343 --> 00:07:26,742
Show your hands, father!
Are they the same?

60
00:07:27,213 --> 00:07:30,321
Shut up, stupid! Let me!

61
00:07:36,978 --> 00:07:37,978
Did you see?

62
00:07:38,410 --> 00:07:38,994
What?

63
00:07:39,401 --> 00:07:40,045
The trick of the hands?

64
00:07:40,386 --> 00:07:41,086
Yes.

65
00:07:41,488 --> 00:07:42,416
Do you have some bread?

66
00:07:42,781 --> 00:07:43,343
Yes.

67
00:07:43,682 --> 00:07:45,220
Give me a piece.

68
00:07:58,262 --> 00:08:01,744
With all those stories...
we are late.

69
00:08:02,059 --> 00:08:03,572
Are you leaving, Brother?

70
00:08:03,891 --> 00:08:09,081
We should keep Simon company,
and pray.

71
00:08:32,355 --> 00:08:35,172
Where does this one-eyed 
woman come from?

72
00:08:35,727 --> 00:08:38,282
One-eyed? You are mistaken.

73
00:08:38,865 --> 00:08:41,442
I tell you she is one-eyed.

74
00:08:41,815 --> 00:08:44,380
She has her two eyes, indeed...

75
00:08:44,705 --> 00:08:45,715
How do you know it?

76
00:08:46,049 --> 00:08:47,456
Because I stared at her.

77
00:08:50,234 --> 00:08:53,410
You forgot the commandments:

78
00:08:53,787 --> 00:08:56,482
"Don't stare at any woman"...

79
00:08:56,817 --> 00:09:00,923
"and don't let yourself be seduced
by the look of a female."

80
00:09:01,198 --> 00:09:02,419
And, before all...

81
00:09:03,203 --> 00:09:08,037
"do not burn at the fire of a 
vain contemplation."

82
00:09:11,918 --> 00:09:13,254
Brother.

83
00:09:13,780 --> 00:09:18,485
...I prefer that you don't come 
and see me anymore...

84
00:09:18,770 --> 00:09:22,596
... as far as your look 
won't have the desired modesty.

85
00:09:58,046 --> 00:10:00,005
Hello, Brother Mathias.

86
00:10:00,410 --> 00:10:04,682
Why didn't you bring some
milk to the monastery?

87
00:10:05,018 --> 00:10:08,534
A witch gave her the evil eye.

88
00:10:08,883 --> 00:10:12,158
At dawn, I will bring you
some fresh milk. 

89
00:10:13,013 --> 00:10:15,552
You want to see the blessed Simon?

90
00:10:16,102 --> 00:10:18,182
Yesterday I brought him 
dairy products...

91
00:10:18,541 --> 00:10:20,982
and some bread fresh of three days.

92
00:10:21,971 --> 00:10:26,154
Damned be my soul,
he didn't even look at me.

93
00:10:26,519 --> 00:10:30,865
Don't blaspheme,
he was probably praying.

94
00:10:31,863 --> 00:10:33,509
What has it got, this one?

95
00:10:33,924 --> 00:10:35,779
A bad leg.

96
00:10:36,087 --> 00:10:37,697
Do you like her, rascal?

97
00:10:38,027 --> 00:10:40,823
She is the new one: Domitila.

98
00:10:42,389 --> 00:10:44,630
She is well done!

99
00:10:44,929 --> 00:10:46,866
you swipe her, she practically swoons.

100
00:10:47,163 --> 00:10:48,260
Devil she is!

101
00:10:49,528 --> 00:10:51,292
May God protect you, Brother.

102
00:10:51,788 --> 00:10:52,542
Amen.

103
00:10:55,118 --> 00:10:57,096
You love your beasts too much.

104
00:10:57,418 --> 00:10:58,854
The Devil prowls...

105
00:10:59,186 --> 00:11:00,312
...in the desert.

106
00:11:00,897 --> 00:11:02,739
I hear him during the night

107
00:11:29,291 --> 00:11:31,235
I forgot the end.

108
00:11:33,789 --> 00:11:36,530
Simon! Father Simon!

109
00:11:37,544 --> 00:11:38,951
It's me!

110
00:11:40,173 --> 00:11:43,794
I bring you food!
Blessed one, look!

111
00:11:44,844 --> 00:11:47,048
May the Peace of the Lord
be with you...

112
00:11:47,386 --> 00:11:49,053
...and with you, Brother.

113
00:11:49,402 --> 00:11:52,346
With the lettuce,  I brought some 
oil and some bread.

114
00:11:52,658 --> 00:11:55,269
My body doesn't need so much.

115
00:11:55,591 --> 00:11:59,879
Few lettuce leaves are enough.

116
00:12:01,014 --> 00:12:03,339
I eat fast too.

117
00:12:03,687 --> 00:12:06,312
But, the less I eat...

118
00:12:06,606 --> 00:12:08,219
...the fatter I get.

119
00:12:09,346 --> 00:12:12,251
The priest wants you to
eat more.

120
00:12:12,581 --> 00:12:14,761
I'm grateful to him...

121
00:12:15,061 --> 00:12:17,234
... but, may he forgive me...

122
00:12:17,975 --> 00:12:21,976
...I know that those fats are 
necessary for me.

123
00:12:22,277 --> 00:12:23,778
Forgive me, you too.

124
00:12:24,144 --> 00:12:28,068
At the monastery, they don't give us
wine anymore...

125
00:12:28,745 --> 00:12:31,318
we drink water with cumin 
and pepper.

126
00:12:31,631 --> 00:12:33,625
The Priest says that it fortifies.

127
00:12:34,279 --> 00:12:37,716
You have eaten nearly nothing
for five days.

128
00:12:39,717 --> 00:12:41,765
You have hardly touched water.

129
00:12:42,931 --> 00:12:44,945
It's foul.

130
00:13:00,018 --> 00:13:02,099
Do you need anything?

131
00:13:02,442 --> 00:13:04,113
No.

132
00:13:05,799 --> 00:13:08,905
Go in peace, 
let me fight my war.

133
00:13:09,245 --> 00:13:10,316
Your war?

134
00:13:10,681 --> 00:13:13,682
Innocent is the one who 
still ignores it ! 

135
00:13:16,192 --> 00:13:17,475
Son ...

136
00:13:18,245 --> 00:13:20,214
You are very clean.

137
00:13:22,749 --> 00:13:24,071
Thank you, Father.

138
00:13:24,546 --> 00:13:28,611
cleanness of the body 
and the clothes ... 

139
00:13:28,947 --> 00:13:31,609
... fits to men of the world...

140
00:13:31,972 --> 00:13:35,922
It's a sin for the religious.

141
00:13:36,197 --> 00:13:39,759
As you say it, 
I will remember.

142
00:13:58,945 --> 00:14:01,171
He is a presomptuous
ignorant!

143
00:14:04,303 --> 00:14:08,072
Man is the most despicable 
of your creatures, Lord!

144
00:14:08,426 --> 00:14:10,870
His only presence takes
me away from Thou.

145
00:14:15,148 --> 00:14:16,746
No walk today?

146
00:14:17,110 --> 00:14:19,626
Sometimes, I'm hungry and thirsty.

147
00:14:20,241 --> 00:14:22,471
I forgot my body...

148
00:14:22,822 --> 00:14:25,414
this miserable recalled it to me.

149
00:14:28,067 --> 00:14:31,791
I won't eat anything 
before the sunset.

150
00:14:32,168 --> 00:14:35,414
I am so far to be worthy of Thou!

151
00:14:40,062 --> 00:14:42,692
What a temptation!...
Be able to go down...

152
00:14:43,043 --> 00:14:45,697
... and feel the soil 
under my feet...

153
00:14:46,019 --> 00:14:47,100
...to run !

154
00:14:48,745 --> 00:14:51,846
Be careful, don't fall!

155
00:14:58,493 --> 00:15:01,167
Do you think about me, 
sometimes, my son?


156
00:15:01,533 --> 00:15:04,934
Not often, mother, 
I have no time.

157
00:15:05,235 --> 00:15:07,620
Why are you so proud, son?

158
00:15:07,917 --> 00:15:10,315
Proud of my freedom!...

159
00:15:10,589 --> 00:15:12,817
or of my slavedom, mother!

160
00:15:13,203 --> 00:15:15,982
Here I am,... Simon!

161
00:15:18,565 --> 00:15:20,520
You're going to see!

162
00:15:21,903 --> 00:15:25,927
Give up your turpitude

163
00:15:26,437 --> 00:15:30,384
Know that what is lost...

164
00:15:30,751 --> 00:15:33,927
...is lost.

165
00:15:34,423 --> 00:15:37,902
And for you especially...

166
00:15:38,206 --> 00:15:41,456
...big-bearded man!

167
00:15:41,833 --> 00:15:45,610
King of this land of rabbits

168
00:15:52,711 --> 00:15:55,585
You measure your merit...

169
00:15:55,960 --> 00:15:58,879
... at your beard...

170
00:15:59,291 --> 00:16:05,216
.. and at your teeth cleaned with
the urine of Syriac...

171
00:16:07,427 --> 00:16:09,404
What are you doing here? 

172
00:16:09,801 --> 00:16:11,000
Play!

173
00:16:11,466 --> 00:16:12,800
Where do you come from?

174
00:16:13,213 --> 00:16:14,305
From there.

175
00:16:14,500 --> 00:16:15,807
And.. where do you go?

176
00:16:16,136 --> 00:16:16,846
There!

177
00:16:18,206 --> 00:16:19,724
Who are you? 

178
00:16:20,008 --> 00:16:22,349
An innocent child, Simon.

179
00:16:22,629 --> 00:16:25,501
See my so innocent legs.

180
00:16:26,999 --> 00:16:30,350
Under my reign, Simon... 

181
00:16:30,683 --> 00:16:35,544
.. those who are here
shouldn't be there...

182
00:16:35,840 --> 00:16:39,347
those who should be  
are not there.

183
00:16:42,268 --> 00:16:45,427
You owe you merit to your beard...

184
00:16:45,765 --> 00:16:51,461
... and to your teeth washed with
Syriac urine.

185
00:16:52,991 --> 00:16:54,043
Simon,

186
00:16:54,370 --> 00:16:56,721
See as my tongue is long!

187
00:17:08,309 --> 00:17:10,230
I don't scare you, Satan!

188
00:17:10,528 --> 00:17:13,907
Christ! Christ! Christ! 

189
00:17:20,797 --> 00:17:23,999
I will come back, beard,
I will come back.

190
00:17:40,481 --> 00:17:41,856
Heavenly Father,

191
00:17:43,221 --> 00:17:46,934
And Jesus, may he be blessed...

192
00:17:48,321 --> 00:17:52,462
I come, at the sunset, to praise

193
00:17:54,924 --> 00:17:59,242
...the Father, the Son,
and the Holy Spirit.

194
00:18:00,838 --> 00:18:04,314
What is more fair that Thou art...

195
00:18:04,707 --> 00:18:07,501
...glorified by the holy voices...

196
00:18:07,862 --> 00:18:13,363
...of the Son of God...

197
00:19:12,643 --> 00:19:14,632
Because he says...

198
00:19:15,001 --> 00:19:18,926
anyone leaves father, mother, brothers...

199
00:19:19,269 --> 00:19:23,506
...wives, children 
for my cause,...

200
00:19:24,321 --> 00:19:26,449
...will receive hundred times over...

201
00:19:26,826 --> 00:19:29,057
...he will inherit eternal life.

202
00:19:29,981 --> 00:19:32,787
I was slave of the world.

203
00:19:33,169 --> 00:19:36,247
I'm here to be Thy Slave.

204
00:19:36,653 --> 00:19:38,060
It's written:

205
00:19:38,424 --> 00:19:42,476
God helps anyone practises good.

206
00:19:43,420 --> 00:19:46,592
Let's do not give up asceticism...

207
00:19:46,954 --> 00:19:49,395
...let's stretch it like an arrow,...

208
00:19:49,878 --> 00:19:53,119
...and, forgetting what we'll leave behind us...

209
00:19:53,441 --> 00:19:57,945
... let's pursue our flight 
to reach the Eternal...

210
00:19:58,258 --> 00:19:59,945
...The heavenly way.

211
00:20:00,308 --> 00:20:04,558
Give your bread and your clothes
to the poors...

212
00:20:05,039 --> 00:20:07,987
They will open the gates
of Heaven for you.

213
00:20:09,564 --> 00:20:12,163
Short is the time of our life.

214
00:20:12,686 --> 00:20:14,349
Look... Brothers!

215
00:20:19,761 --> 00:20:23,719
Look... The Saint has a well-
furnished bag.

216
00:20:24,040 --> 00:20:25,368
Some cheese....

217
00:20:26,630 --> 00:20:27,864
... some bread....

218
00:20:31,026 --> 00:20:32,195
...and some wine...

219
00:20:32,569 --> 00:20:35,302
He says he is the 
champion of the penitence...

220
00:20:35,602 --> 00:20:39,263
He is on this column to be above men.

221
00:20:39,525 --> 00:20:40,987
Is he a "Samaritan"?

222
00:20:42,564 --> 00:20:45,152
He is an arrogant led by Satan.

223
00:20:45,412 --> 00:20:47,155
Here are the pieces of evidence.

224
00:20:48,214 --> 00:20:51,397
"Let's stretch our asceticism
like an arrow..."

225
00:20:52,561 --> 00:20:55,651
I trust you, Simon...

226
00:20:55,951 --> 00:20:57,778
I know that those foods...

227
00:20:58,050 --> 00:21:00,298
... are traps of the Devil. 

228
00:21:00,794 --> 00:21:04,972
You don't have to convince me... 

229
00:21:05,260 --> 00:21:08,109
...but tell your disciples, 
if you know it...

230
00:21:08,386 --> 00:21:12,200
... why those foods were found here.

231
00:21:12,965 --> 00:21:16,834
Better worth the calomnys
for the believer's soul... 

232
00:21:17,134 --> 00:21:20,842
...than the praises which 
 flatter the pride.

233
00:21:22,372 --> 00:21:27,400
The sweet one, the humble one
calls me a slanderer, Brothers!

234
00:21:31,269 --> 00:21:34,682
I swear I didn't put this food down here!

235
00:21:36,305 --> 00:21:41,138
Blessed one, speak 
and we'll believe you!

236
00:21:42,739 --> 00:21:43,621
Speak!

237
00:21:44,007 --> 00:21:47,175
Lord, I'm your slave!
Thy will be done...

238
00:21:48,098 --> 00:21:51,768
He refuses to speak.
He is guilty.

239
00:21:52,105 --> 00:21:53,943
Noone can doubt any more.

240
00:21:54,719 --> 00:21:57,637
Let's pray , Brothers,  let's pray for him.

241
00:21:57,949 --> 00:22:00,497
A servant of Gord accuses 
another one.

242
00:22:00,817 --> 00:22:03,022
We came to look for edification,

243
00:22:03,326 --> 00:22:05,750
we find the scandal.

244
00:22:06,567 --> 00:22:12,683
Let's pray the Holy Spirit to 
reveal us who's really guilty.

245
00:22:13,382 --> 00:22:15,375
Let's kneel down to pray.

246
00:22:53,444 --> 00:22:58,803
In the name of the Father, of the son,
and the Holy Spirit...

247
00:23:00,009 --> 00:23:01,465
What has he got?

248
00:23:01,884 --> 00:23:04,053
Sign yourself, Brother Tifon.

249
00:23:08,201 --> 00:23:09,703
Castrated pig!

250
00:23:10,041 --> 00:23:12,498
I did put this grub in your bag...

251
00:23:12,805 --> 00:23:15,663
I'll torment you again...!

252
00:23:15,962 --> 00:23:19,685
... Son of a B... until it comes
out of your mouth...

253
00:23:19,990 --> 00:23:22,463
... maledictions against 
the blessed Sacrament...

254
00:23:22,787 --> 00:23:24,826
...and his putative mother!

255
00:23:28,104 --> 00:23:30,769
In the belly of this daughter of dog...

256
00:23:34,018 --> 00:23:35,763
Down with the Holy trinity!

257
00:23:36,256 --> 00:23:37,692
Long live the Holy trinity!

258
00:23:37,997 --> 00:23:38,726
Viva!

259
00:23:38,961 --> 00:23:40,161
Down with Christ's resurrection!

260
00:23:40,598 --> 00:23:41,528
Viva!!

261
00:23:42,506 --> 00:23:43,896
Viva apocastasy!

262
00:23:44,179 --> 00:23:44,883
Death to it!

263
00:23:45,890 --> 00:23:48,363
What is "apocastasy"?

264
00:23:50,634 --> 00:23:52,320
Death to Jesus Christ!

265
00:23:52,634 --> 00:23:55,240
Death to him! ... Sorry, Viva!

266
00:23:58,526 --> 00:24:01,000
I implore you, 
in the name of Christ...

267
00:24:01,318 --> 00:24:03,968
Leave the body of this man out!

268
00:24:04,320 --> 00:24:08,061
Flee, Satan, in face of 
the sign of the cross !

269
00:24:13,740 --> 00:24:16,020
Catch him, take him away...

270
00:24:16,386 --> 00:24:20,291
I'll finish to exorcize him 
my own way.

271
00:24:20,760 --> 00:24:23,221
May the peace be with you, Simon

272
00:24:37,445 --> 00:24:39,246
Z?on!

273
00:24:39,759 --> 00:24:41,480
Z?on... Listen to me! 

274
00:24:41,858 --> 00:24:45,010
Move away from 
this beardless boy.

275
00:24:50,413 --> 00:24:51,378
Me? 

276
00:24:52,094 --> 00:24:57,182
May he not live in the monastery
for fear of the evil one.

277
00:24:57,605 --> 00:25:01,531
May he come back when his beard
will be grown.

278
00:25:03,880 --> 00:25:08,481
Brother, thank you for your advises
and your carefulness.

279
00:25:09,175 --> 00:25:13,401
Don't come back into the monastery.

280
00:25:13,732 --> 00:25:18,140
When you'll have a beard,
you can come back.

281
00:25:33,269 --> 00:25:35,578
Beware of those bearded men. 

282
00:25:35,916 --> 00:25:38,536
The devil prowls in the desert.

283
00:25:38,922 --> 00:25:41,162
I hear him at night.

284
00:27:23,112 --> 00:27:26,396
Lord, my thoughts wander away from Thou.

285
00:27:36,046 --> 00:27:39,901
Here is the most humble of Thy Servants

286
00:27:41,800 --> 00:27:44,680
You are my preferred son. 

287
00:27:45,058 --> 00:27:47,000
You asceticism is sublime.

288
00:27:47,354 --> 00:27:50,825
I love you,
you speak by my mouth.

289
00:27:51,602 --> 00:27:54,837
Since 8 years, 8 months, 8 days.

290
00:27:55,175 --> 00:27:58,003
your soul has burnt 
on this column...

291
00:27:58,385 --> 00:28:00,885
...like the flame of a candle.

292
00:28:01,171 --> 00:28:03,271
Blessed are those who suffer.

293
00:28:04,248 --> 00:28:06,925
I want to die in Thou, Lord. 

294
00:28:07,691 --> 00:28:10,084
Receive my soul.

295
00:28:10,893 --> 00:28:12,935
Why are you crying, Lord?

296
00:28:13,362 --> 00:28:15,901
I cry on you, beloved son...

297
00:28:16,240 --> 00:28:17,968
Your penitences...

298
00:28:18,294 --> 00:28:20,434
...and your sacrifices afflict me.

299
00:28:21,452 --> 00:28:24,580
Stop it!
They displease to my heart.

300
00:28:24,909 --> 00:28:26,480
Change!

301
00:28:27,277 --> 00:28:28,898
To change, Lord?

302
00:28:29,241 --> 00:28:30,643
Come down.

303
00:28:31,124 --> 00:28:35,051
Go in the world, 
revel in pleasure...

304
00:28:37,482 --> 00:28:41,693
...up to nausea. Like this, your
reveled flesh will release your mind.

305
00:28:42,055 --> 00:28:45,980
And you'll be by my side.

306
00:28:51,288 --> 00:28:54,651
Satan! I don't fear you!

307
00:28:55,000 --> 00:28:57,721
The Lord foils your plots.

308
00:28:58,252 --> 00:29:02,057
Do you only search perdition 
for the mankind?

309
00:29:02,411 --> 00:29:03,496
You ...

310
00:29:03,875 --> 00:29:07,484
... who, before the fall, enjoyed
the Divine Presence. 

311
00:29:08,636 --> 00:29:10,480
If I repent...

312
00:29:10,800 --> 00:29:13,563
Will God give me back my
original glory?

313
00:29:14,105 --> 00:29:16,226
By no means, Satan.

314
00:29:16,601 --> 00:29:18,496
Repent, if you can...,

315
00:29:18,832 --> 00:29:22,211
...but you'll stay like this 
for eternity.

316
00:29:23,494 --> 00:29:27,121
He comes out with some good ones,
this idiot.

317
00:29:27,451 --> 00:29:30,487
May your father's pant repent!

318
00:29:30,826 --> 00:29:34,160
I am fine as I am,
big pig !

319
00:29:39,010 --> 00:29:41,779
...On the host !...

320
00:29:42,171 --> 00:29:44,592
By the belly of this daughter of B...!

321
00:29:46,881 --> 00:29:49,804
In the name of the Christ...

322
00:29:50,343 --> 00:29:53,904
...let the just pray in peace!

323
00:29:54,221 --> 00:29:57,300
I'll come back, O beard man, 
I'll come back!

324
00:30:11,942 --> 00:30:14,950
If I don't release myself 
from the Evil one, today...

325
00:30:15,278 --> 00:30:17,215
... it will be tomorrow.

326
00:30:17,545 --> 00:30:20,301
...or in 5 years, or in 10 years...

327
00:30:20,634 --> 00:30:23,180
Continence, prayer...

328
00:30:23,480 --> 00:30:26,588
charity, humility
will be my arms.

329
00:30:33,606 --> 00:30:36,445
Blinded as I am...

330
00:30:36,757 --> 00:30:39,853
... I took the wolf for a lamb.

331
00:30:40,871 --> 00:30:43,625
I deserve eternal penitence.

332
00:30:43,906 --> 00:30:47,401
Until you call me to you...

333
00:30:47,739 --> 00:30:50,265
... I'll stand on one foot. 

334
00:31:53,688 --> 00:31:57,089
Thank you, Lord, to provide 
for my subsistence.

335
00:32:07,113 --> 00:32:08,967
So that you fertilize the earth,

336
00:32:09,299 --> 00:32:12,279
save us from the hail.

337
00:32:28,621 --> 00:32:31,596
I bless you, innocent creature.

338
00:32:31,880 --> 00:32:34,134
Sing the Lord's glory. 

339
00:32:39,076 --> 00:32:41,720
Who can I bless now?

340
00:32:42,075 --> 00:32:46,263
It's a holy exercise, 
it keeps me busy...

341
00:32:46,622 --> 00:32:49,055
... and it offends noone. 

342
00:32:49,525 --> 00:32:51,361
What am I saying?

343
00:32:51,634 --> 00:32:55,559
Sometimes, I don't know any longer what I'm saying.

344
00:32:56,230 --> 00:32:58,152
What I am saying?

345
00:32:58,417 --> 00:32:59,542
What do I say?

346
00:32:59,902 --> 00:33:00,873
Father.

347
00:33:02,154 --> 00:33:05,077
Bless Pelagie, she is yearning 
for children.

348
00:33:05,366 --> 00:33:08,500
We will see if this sweety
gives birth properly.

349
00:33:08,784 --> 00:33:11,058
I bless you too,
beloved Brother...

350
00:33:11,326 --> 00:33:13,830
... you are poor of 
wealth and spirit.

351
00:33:14,192 --> 00:33:17,270
Don't bless me like a goat!

352
00:33:17,565 --> 00:33:22,752
I estimate you, I love you.
Tomorrow I 'll bring you...

353
00:33:23,112 --> 00:33:24,482
...very fresh milk.

354
00:33:24,787 --> 00:33:29,026
Thank you... but you know 
it's useless.

355
00:33:29,563 --> 00:33:33,093
For me, is a bit nuts...

356
00:33:34,486 --> 00:33:37,844
by filling himself up with air!

357
00:33:38,294 --> 00:33:42,723
Believe me, I drink and eat according 
to my needs.

358
00:33:43,007 --> 00:33:46,791
I'm not a pure spirit, but a man...

359
00:33:47,069 --> 00:33:49,839
...to whom hangs heavy  
his carnal envelope.

360
00:33:50,119 --> 00:33:53,240
About the other needs...

361
00:33:53,529 --> 00:33:55,720
My excrements are like goat's ones...

362
00:33:56,020 --> 00:33:57,567
... because of my thinness.

363
00:33:57,925 --> 00:34:01,762
Of all your sermon, I only 
understood "thinness". 

364
00:34:50,715 --> 00:34:52,217
What are you looking for?

365
00:34:52,554 --> 00:34:55,119
Your forgiveness and your benediction.

366
00:34:56,664 --> 00:34:59,824
Who are you?
What have I to forgive you?

367
00:35:00,092 --> 00:35:02,985
I'm the one who had 
looked a woman.

368
00:35:03,693 --> 00:35:06,483
Go in peace if God forgives you.

369
00:35:06,771 --> 00:35:10,730
I have some bad news 
for God's servants.

370
00:35:11,012 --> 00:35:13,856
The hordes of the Antichrist
advance on Rome.

371
00:35:14,164 --> 00:35:15,960
They'll be here very soon. 

372
00:35:16,266 --> 00:35:20,003
Blessed be the wounds which
bring us the glory of God!

373
00:35:20,340 --> 00:35:23,514
Men will always fight 
in fratricidal battles. 

374
00:35:23,804 --> 00:35:26,255
Because of this malediction
"yours", "mine".

375
00:35:26,538 --> 00:35:27,643
What are you speaking about?

376
00:35:27,976 --> 00:35:30,610
Of man who kill to defend what 
he thinks to be "his".

377
00:35:31,528 --> 00:35:34,730
What is "yours"?

378
00:35:35,048 --> 00:35:36,257
What is "mine"?

379
00:35:36,601 --> 00:35:39,280
You'll understand.

380
00:35:41,498 --> 00:35:43,780
This bag is yours, isn't it?

381
00:35:44,123 --> 00:35:47,557
Bu if I say the opposite,
we will quarrel.

382
00:35:47,871 --> 00:35:49,416
Do you want to try?

383
00:35:49,858 --> 00:35:52,425
Simon, this bag is MINE!

384
00:35:52,719 --> 00:35:54,166
Say that it's yours.

385
00:35:54,434 --> 00:35:55,103
It's mine...

386
00:35:55,371 --> 00:35:56,972
I tell you it's mine!

387
00:35:57,257 --> 00:35:59,521
Well.. so, take it.

388
00:36:02,925 --> 00:36:07,114
Your selflessness is good 
for your soul.

389
00:36:07,601 --> 00:36:10,177
But... as your penitence... 

390
00:36:10,974 --> 00:36:12,813
... can it be useful to man?

391
00:36:13,833 --> 00:36:18,074
We don't speak the same language.

392
00:36:18,940 --> 00:36:21,494
Go in peace, Brother.

393
00:37:16,492 --> 00:37:18,364
Protect me, Lord.

394
00:37:31,793 --> 00:37:35,677
I promised you I would come back. 
It is the last time.

395
00:37:36,199 --> 00:37:38,849
Vade retro, Satana!

396
00:37:39,228 --> 00:37:42,347
No more "Vade" than "Retro".

397
00:37:42,824 --> 00:37:47,400
If the Lord allows
that you tempt me,

398
00:37:47,708 --> 00:37:49,313
I'll endure it. 

399
00:37:49,737 --> 00:37:51,025
But you 'll succeed in nothing.

400
00:37:51,362 --> 00:37:55,336
The stink of your breathe
comes up to me. 

401
00:37:56,128 --> 00:37:58,041
You don't smell good, either.

402
00:37:58,315 --> 00:38:02,115
Prepair yourself, we'll do a long travel!

403
00:38:02,436 --> 00:38:03,943
Very long. 

404
00:38:06,213 --> 00:38:08,481
In the name of the Christ...

405
00:38:08,842 --> 00:38:10,382
Stop with those gestures,

406
00:38:10,721 --> 00:38:13,532
this time, it won't help in anything.

407
00:38:14,012 --> 00:38:17,769
Simon of the desert!
It will surprise you...

408
00:38:18,039 --> 00:38:20,677
...but between you and me,
there are only few differences.

409
00:38:20,971 --> 00:38:22,601
As you, I believe...

410
00:38:22,903 --> 00:38:24,403
...in Almighty God.

411
00:38:24,702 --> 00:38:26,929
Because I enjoyed his Presence,

412
00:38:27,293 --> 00:38:31,269
About his unique son, 
there is a lot to say.

413
00:38:36,545 --> 00:38:38,751
Prepair yourself to leave.

414
00:38:39,150 --> 00:38:42,541
Do you know where I bring you?
To the Sabbath.

415
00:38:42,893 --> 00:38:45,713
There, you 'll see shooting out the flash of the tongues

416
00:38:46,027 --> 00:38:48,800
...and opening the wet flower of the flesh.

417
00:38:49,315 --> 00:38:50,271
Enough!

418
00:38:51,273 --> 00:38:53,159
Let's go! Do you hear me?

419
00:38:53,481 --> 00:38:55,489
One is coming to take us.

420
00:41:58,807 --> 00:42:00,931
What are you thinking about?

421
00:42:01,206 --> 00:42:02,123
About nothing.

422
00:42:02,434 --> 00:42:04,469
What is the name of this dance?

423
00:42:04,806 --> 00:42:06,188
"Radioactive flesh".

424
00:42:06,523 --> 00:42:09,047
It's the last dance. 
It's the final.

425
00:42:16,572 --> 00:42:18,800
Enjoy yourself!
I have to leave.

426
00:42:19,106 --> 00:42:21,592
You should better stay here.

427
00:42:21,891 --> 00:42:22,885
Why?

428
00:42:23,320 --> 00:42:24,764
Where could you go?

429
00:42:25,072 --> 00:42:26,679
You have to stay here.

430
00:42:28,230 --> 00:42:31,063
You have to stay until the end.

431
00:42:32,230 --> 00:42:40,063
{\a4\3c&HE05310&\fs15\i1} 
<font color="FFFF00"> Re Adaptation By: Ali Nabawy</font>
<font color="FFFF00"> ��� ������ ������� : ��� �����</font>
<font color="FFFF00"> ����� ���� ������� �����������</font>
http://wwwsevenart.blogspot.com/
Alynabawy@Hotmail.Com 
analazyz@yahoo.com