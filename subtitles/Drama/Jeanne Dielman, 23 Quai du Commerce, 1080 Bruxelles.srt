1
00:01:00,197 --> 00:01:06,761
Written and Directed by

2
00:02:14,371 --> 00:02:16,362
<i>Hello.</i>

3
00:04:07,651 --> 00:04:10,119
<i>See you next week, then.</i>

4
00:15:48,284 --> 00:15:50,377
Don't read while you eat.

5
00:19:36,479 --> 00:19:38,709
Is it good?

6
00:19:57,233 --> 00:20:01,294
I added less water than last week.
Maybe that's why it's better.

7
00:21:26,822 --> 00:21:29,985
You hardly eat.
You don't look well, you know.

8
00:22:23,446 --> 00:22:25,937
Want something else?
Some fruit?

9
00:23:58,941 --> 00:24:01,967
We got a letter
from Aunt Fernande in Canada.

10
00:24:42,484 --> 00:24:44,315
I'll read it to you.

11
00:25:00,603 --> 00:25:02,764
<i>"Dear Jeanne and Sylvain,</i>

12
00:25:02,938 --> 00:25:06,374
I'm sorry I've taken
so long to write back.

13
00:25:06,542 --> 00:25:09,204
I often think of you
and Belgium.

14
00:25:09,378 --> 00:25:13,542
But I've been so busy
with the kids starting back to school,

15
00:25:13,716 --> 00:25:16,913
and suddenly
I realized winter's here.

16
00:25:17,086 --> 00:25:20,487
It's been snowing a lot,
and I hardly get out.

17
00:25:20,656 --> 00:25:25,093
The roads were so bad that the kids
couldn't go to school last Friday,

18
00:25:25,261 --> 00:25:27,889
and I'm at home
most of the time,

19
00:25:28,063 --> 00:25:31,260
because without a car,
you get bogged down.

20
00:25:31,433 --> 00:25:34,425
<i>Jack says he'll teach me
to drive next year.</i>

21
00:25:34,603 --> 00:25:37,629
The distances
are simply too huge here.

22
00:25:37,806 --> 00:25:42,072
He says I'm in Canada now
and I should act like the locals.

23
00:25:42,244 --> 00:25:45,702
The women here all drive,
but I'm afraid I'll never learn.

24
00:25:45,881 --> 00:25:49,373
But now all the cars
are completely covered by snow.

25
00:25:49,551 --> 00:25:51,712
The children are fine,
big and strong.

26
00:25:51,887 --> 00:25:53,821
Real Canadians, just like Jack.

27
00:25:53,989 --> 00:25:57,925
I fear Jane may get too tall,
but anyway, she's fine.

28
00:25:58,093 --> 00:26:00,323
I hope you'll come
see them this summer.

29
00:26:00,496 --> 00:26:04,398
We have a large guest room,
and Sylvain can sleep with his cousin.

30
00:26:04,566 --> 00:26:06,796
Maybe we can
introduce you to someone.

31
00:26:06,969 --> 00:26:11,804
Jack says you should remarry, that a
good-Iooking woman shouldn't be alone.

32
00:26:11,974 --> 00:26:14,568
George has been dead
six years now.

33
00:26:14,743 --> 00:26:18,645
We know you're very brave
and you say you'd rather stay single

34
00:26:18,814 --> 00:26:21,112
so as not to complain.

35
00:26:21,283 --> 00:26:24,013
But sometimes when I think of you,
tears come to my eyes.

36
00:26:24,186 --> 00:26:27,952
Well, Jack will be home soon
and the table's not set yet,

37
00:26:28,123 --> 00:26:30,683
so I'll say good-bye now,
with a hug for you both.

38
00:26:30,859 --> 00:26:33,225
Your loving sister and aunt,
Fernande.

39
00:26:33,395 --> 00:26:38,731
PS: I sent a birthday present
for Jeanne last month by boat."

40
00:27:00,422 --> 00:27:03,152
I wonder
what that present could be.

41
00:27:05,394 --> 00:27:07,487
Would you like
to go to Canada?

42
00:27:20,509 --> 00:27:23,273
<i>"The Enemy,"
by Charles Baudelaire.</i>

43
00:27:24,446 --> 00:27:28,314
<i>"My youth was but
a gloomy storm</i>

44
00:27:28,617 --> 00:27:32,144
With occasional flashes
of dazzling sun

45
00:27:32,788 --> 00:27:36,053
Thunder and rain
have created such havoc

46
00:27:37,226 --> 00:27:40,855
That little scarlet fruit
remains in my garden

47
00:27:42,064 --> 00:27:45,465
Now I've reached
the autumn of my thoughts

48
00:27:45,634 --> 00:27:48,194
And I have to use..."

49
00:27:48,370 --> 00:27:49,860
The spade.

50
00:27:50,038 --> 00:27:53,974
<i>"And I have to use
the spade and rake</i>

51
00:27:54,143 --> 00:27:57,772
To reclaim a bit
of the flooded land

52
00:27:59,281 --> 00:28:02,876
Where the water digs holes
large as tombs."

53
00:28:03,051 --> 00:28:04,678
Once more.

54
00:28:04,953 --> 00:28:07,854
<i>"The Enemy,"
by Charles Baudelaire.</i>

55
00:28:09,324 --> 00:28:12,418
<i>"My youth was but
a gloomy storm</i>

56
00:28:12,594 --> 00:28:16,086
With occasional flashes
of dazzling suns

57
00:28:18,400 --> 00:28:21,892
Thunder and rain
have created such havoc

58
00:28:22,070 --> 00:28:25,164
That little scarlet fruit
remains in my garden

59
00:28:25,340 --> 00:28:27,934
Now I've reached
the autumn of my thoughts

60
00:28:28,110 --> 00:28:31,170
And I have to use
the spade and rake

61
00:28:31,346 --> 00:28:34,315
To reclaim a bit
of the flooded land

62
00:28:34,483 --> 00:28:37,043
Where the water digs holes
large as tombs."

63
00:28:40,189 --> 00:28:42,589
You're picking up the accent
more and more.

64
00:28:42,758 --> 00:28:46,194
I know. I can even pronounce
the Rs like Yan,

65
00:28:46,361 --> 00:28:49,159
and no one laughs
at me at school anymore.

66
00:28:49,331 --> 00:28:50,923
Perhaps,

67
00:28:51,099 --> 00:28:54,364
but nobody forced you to go
to the Flemish school.

68
00:28:55,003 --> 00:28:57,130
Yes, I know... your friend.

69
00:29:15,324 --> 00:29:18,953
I wonder if you can still
say your Rs like me.

70
00:30:13,515 --> 00:30:19,647
<i>And now, the Bagatelle for Piano No. 27,
Opus 126 in A Minor, by Beethoven,</i>

71
00:30:19,821 --> 00:30:23,780
<i>"F�r Elise,"
played by Suzanne Duch�ne.</i>

72
00:31:42,104 --> 00:31:44,197
Come have a look.

73
00:31:55,484 --> 00:31:57,884
You want it long, don't you?

74
00:32:18,073 --> 00:32:20,268
I'm almost out of yarn.

75
00:33:35,984 --> 00:33:38,475
- This is Tuesday, isn't it?
- Yes.

76
00:39:44,919 --> 00:39:47,888
You're always reading,
just like your father.

77
00:39:48,056 --> 00:39:50,524
I know.
You already told me.

78
00:39:51,025 --> 00:39:53,858
How did you meet my father?

79
00:39:54,262 --> 00:39:56,355
Why do you ask that now?

80
00:39:56,531 --> 00:39:58,590
I just read the word "miracle,"

81
00:39:58,766 --> 00:40:02,827
and Aunt Fernande always said
it was a miracle she met Jack.

82
00:40:03,004 --> 00:40:06,701
Yes, he came in '44
to liberate us.

83
00:40:06,875 --> 00:40:09,571
They tossed chewing gum
and chocolates to us,

84
00:40:09,744 --> 00:40:12,178
and we threw flowers to them.

85
00:40:12,347 --> 00:40:15,578
I met your father
after the Americans had left.

86
00:40:15,750 --> 00:40:18,810
I was living with my aunts,
because my parents were dead.

87
00:40:18,987 --> 00:40:23,151
One Saturday, I went to the
Bois de la Cambre with a girlfriend.

88
00:40:23,324 --> 00:40:25,622
I don't remember the weather.

89
00:40:25,793 --> 00:40:30,787
She knew him. You know who I mean.
I've shown you her picture.

90
00:40:31,432 --> 00:40:33,024
So we began seeing each other.

91
00:40:33,201 --> 00:40:36,864
I was working as a billing clerk
for horrible pay.

92
00:40:37,038 --> 00:40:40,565
Life with my aunts was dull.
I didn't feel like getting married,

93
00:40:40,742 --> 00:40:45,577
<i>but it seemed to be
"the thing to do," as they say.</i>

94
00:40:45,747 --> 00:40:48,807
<i>My aunts kept saying,
"He's nice. He's got money.</i>

95
00:40:48,983 --> 00:40:51,178
He'll make you happy."

96
00:40:51,352 --> 00:40:53,547
But I still couldn't decide.

97
00:40:53,721 --> 00:40:58,954
But I really wanted
a life of my own, and a child.

98
00:40:59,627 --> 00:41:04,223
Then his business suddenly
hit the rocks, so I married him.

99
00:41:05,033 --> 00:41:07,866
Things like that happened
after the war.

100
00:41:08,036 --> 00:41:10,129
My aunts
had changed their minds.

101
00:41:10,305 --> 00:41:13,138
They said a pretty girl like me
could do better

102
00:41:13,308 --> 00:41:15,833
and find a man
who'd give me a good life.

103
00:41:16,010 --> 00:41:20,709
They said he was ugly and so on,
but I didn't listen.

104
00:41:22,050 --> 00:41:25,508
If he was ugly, did you want
to make love with him?

105
00:41:26,621 --> 00:41:30,523
Ugly or not,
it wasn't all that important.

106
00:41:30,692 --> 00:41:34,150
Besides, "making love,"
as you call it, is merely a detail.

107
00:41:34,329 --> 00:41:37,321
And I had you.
And he wasn't as ugly as all that.

108
00:41:37,498 --> 00:41:39,932
Would you want to remarry?

109
00:41:40,535 --> 00:41:43,095
No. Get used
to someone else?

110
00:41:43,838 --> 00:41:45,999
I mean someone you love.

111
00:41:47,242 --> 00:41:49,210
Oh, you know...

112
00:41:49,944 --> 00:41:52,845
Well, if I were a woman,

113
00:41:53,014 --> 00:41:56,973
I could never make love with someone
I wasn't deeply in love with.

114
00:41:57,151 --> 00:42:00,416
How could you know?
You're not a woman.

115
00:42:00,588 --> 00:42:02,351
Lights out?

116
00:42:02,523 --> 00:42:04,184
All right.

117
00:42:04,492 --> 00:42:06,050
Sleep well.

118
00:43:03,551 --> 00:43:06,543
End of the first day

119
00:55:02,236 --> 00:55:04,261
Did you wash your hands?

120
00:56:06,133 --> 00:56:08,795
Couldn't I have
a little more today?

121
01:09:14,320 --> 01:09:16,550
Thank you, ma'am.

122
01:09:42,248 --> 01:09:44,648
How are you, ma'am?

123
01:09:44,817 --> 01:09:48,753
Fine, thanks. Could you
fix these by tomorrow?

124
01:09:48,922 --> 01:09:50,947
My son's wearing
his other pair,

125
01:09:51,124 --> 01:09:53,718
but his feet get soaked
when it rains.

126
01:09:53,893 --> 01:09:56,225
They're last year's shoes.

127
01:09:56,896 --> 01:09:59,023
I'll have a look.

128
01:10:03,169 --> 01:10:05,467
And the heels too
while I'm at it.

129
01:10:05,638 --> 01:10:09,472
All right. How's tomorrow at 4:00?
- Fine.

130
01:10:09,642 --> 01:10:13,169
- How's your boy doing?
- Just fine.

131
01:10:13,346 --> 01:10:16,611
You're happy with him?
He listens to you?

132
01:10:16,783 --> 01:10:17,841
Good.

133
01:10:18,017 --> 01:10:19,985
What would I do without him?

134
01:10:20,153 --> 01:10:22,314
They'll be ready tomorrow at 4:00.

135
01:11:15,508 --> 01:11:17,840
Hello.
It's been a long time.

136
01:11:18,678 --> 01:11:20,646
Stop by for coffee
this afternoon.

137
01:11:20,813 --> 01:11:24,214
<i>I can't today.
Maybe next week.</i>

138
01:11:24,384 --> 01:11:26,682
All right. See you then.

139
01:18:21,267 --> 01:18:23,201
Hello.

140
01:23:29,274 --> 01:23:30,241
Here he is.

141
01:23:30,409 --> 01:23:32,604
<i>You weren't eating,
were you?</i>

142
01:23:32,878 --> 01:23:34,846
<i>What are you making for dinner?</i>

143
01:23:35,013 --> 01:23:38,380
Wednesdays it's breaded veal
with peas and carrots.

144
01:23:39,017 --> 01:23:41,247
<i>I had no idea
what to make today.</i>

145
01:23:41,420 --> 01:23:45,754
<i>Myself, I'd be fine
with just bread and butter.</i>

146
01:23:46,625 --> 01:23:49,822
<i>I don't have much appetite
ever since I started smoking.</i>

147
01:23:49,995 --> 01:23:54,489
<i>But the children need meat,
so I went to the butcher's.</i>

148
01:23:54,666 --> 01:23:57,100
<i>There was a long line.</i>

149
01:23:57,636 --> 01:24:02,835
<i>I always think I should go
earlier or later to avoid the line,</i>

150
01:24:03,008 --> 01:24:05,374
<i>but this time, I thought,</i>

151
01:24:05,544 --> 01:24:08,877
<i>"I'll listen to what other women order.
I might get some ideas. "</i>

152
01:24:09,047 --> 01:24:11,277
<i>But I got
even more discouraged.</i>

153
01:24:12,084 --> 01:24:14,712
<i>One ordered ground meat.</i>

154
01:24:14,887 --> 01:24:17,947
<i>I thought,
"Probably for meat loaf,</i>

155
01:24:18,123 --> 01:24:21,286
<i>but I just served that yesterday
with apple sauce.</i>

156
01:24:21,460 --> 01:24:26,989
<i>Then I thought, "No, she'll make
hash. "But hash again...</i>

157
01:24:27,166 --> 01:24:32,229
<i>When my turn came,
I still hadn't decided.</i>

158
01:24:32,704 --> 01:24:36,902
<i>So I ordered the same thing
as the woman in front of me</i>

159
01:24:37,075 --> 01:24:40,738
<i>and wound up paying 300 francs
for two pounds of veal.</i>

160
01:24:41,580 --> 01:24:46,074
<i>It's a good two days' worth,
and none of us really like veal.</i>

161
01:24:46,251 --> 01:24:48,446
<i>So the kids turn up
their noses once again.</i>

162
01:24:48,620 --> 01:24:51,088
<i>People say veal
has no vitamins.</i>

163
01:24:51,256 --> 01:24:55,056
<i>And you can't eat fish these days -
it can kill you.</i>

164
01:24:56,161 --> 01:24:59,528
<i>If only they didn't
come home for lunch every day,</i>

165
01:24:59,698 --> 01:25:01,757
<i>but my husband says
the food at school isn't good,</i>

166
01:25:01,934 --> 01:25:04,630
<i>and they're small
for their age as it is.</i>

167
01:25:04,903 --> 01:25:08,600
<i>I tell you,
if it were up to just me...</i>

168
01:25:10,309 --> 01:25:13,437
<i>But he'll be gone
all next week.</i>

169
01:25:13,612 --> 01:25:15,671
<i>I'll miss him, you know?</i>

170
01:25:15,848 --> 01:25:18,078
<i>I'll stay at my mother's,</i>

171
01:25:18,250 --> 01:25:22,050
<i>so they'll have to get used
to eating with the other kids.</i>

172
01:25:22,221 --> 01:25:23,779
<i>It's better that way.</i>

173
01:25:23,956 --> 01:25:26,390
<i>My husband can't object.</i>

174
01:25:26,558 --> 01:25:28,788
<i>Does your son
eat lunch at school?</i>

175
01:25:28,961 --> 01:25:31,225
Yes, he always has.
He's not fussy,

176
01:25:31,396 --> 01:25:33,523
and neither was my husband.

177
01:25:33,699 --> 01:25:36,532
<i>Oh, mine isn't either.
It's all those kids.</i>

178
01:25:36,702 --> 01:25:39,671
<i>But what can you do,
you know?</i>

179
01:25:41,340 --> 01:25:43,501
<i>At least there are ways
these days.</i>

180
01:25:43,675 --> 01:25:45,666
<i>Well, I've gotta go.</i>

181
01:25:45,844 --> 01:25:47,675
<i>Bye.</i>

182
01:29:24,296 --> 01:29:25,627
Excuse me.

183
01:29:26,965 --> 01:29:30,765
I'd like a ball of yarn
this color.

184
01:29:32,637 --> 01:29:35,367
I think
I saw some over there.

185
01:29:38,110 --> 01:29:40,340
- This color?
- Yes.

186
01:29:51,423 --> 01:29:53,050
Yes, that's it.

187
01:33:31,276 --> 01:33:32,903
Good-bye, ma'am.

188
01:35:38,169 --> 01:35:39,693
<i>Hello.</i>

189
01:37:08,326 --> 01:37:10,157
<i>See you next Thursday.</i>

190
01:42:33,384 --> 01:42:35,784
A bag of potatoes, please.

191
01:42:43,527 --> 01:42:44,994
Here you are.

192
01:42:45,496 --> 01:42:47,020
Thank you.

193
01:48:34,411 --> 01:48:36,038
Mother?

194
01:48:43,153 --> 01:48:44,882
Your hair's a mess.

195
01:48:45,055 --> 01:48:47,615
I let the potatoes
cook too long.

196
01:49:53,190 --> 01:49:55,090
Don't read while you eat.

197
01:51:37,428 --> 01:51:39,453
You'll have to wait a bit.

198
01:51:40,364 --> 01:51:43,993
The meat and vegetables
were on a very low flame.

199
01:52:28,412 --> 01:52:30,778
Just another minute or two.

200
01:52:44,094 --> 01:52:47,621
I could have made mashed potatoes,
but we're having that tomorrow.

201
01:53:30,841 --> 01:53:34,436
You must be hungry.
You had swimming this morning.

202
01:53:35,879 --> 01:53:38,109
I didn't go.
Neither did Yan.

203
01:53:38,282 --> 01:53:40,045
Why?

204
01:53:40,217 --> 01:53:42,583
Yan skipped it
just to keep me company.

205
01:53:42,753 --> 01:53:46,314
I said I had a headache,
and we stayed in the nurse's office.

206
01:53:46,490 --> 01:53:48,822
We had to pass
our certification today.

207
01:53:49,026 --> 01:53:51,654
I don't like you doing that.

208
01:54:09,513 --> 01:54:12,209
I should write back
to Aunt Fernande.

209
01:54:12,382 --> 01:54:15,078
The potatoes
should be done now.

210
01:57:24,341 --> 01:57:27,139
Aren't we going to listen
to the radio today?

211
01:57:27,377 --> 01:57:28,810
Yes.

212
01:58:42,819 --> 01:58:45,982
Is something wrong?
Maybe it's the radio.

213
01:58:46,256 --> 01:58:48,224
Yes, it could be that singer.

214
01:58:48,391 --> 01:58:52,623
No, I just don't know
how to reply to their invitation.

215
02:01:07,697 --> 02:01:10,791
Can we not go tonight?
We ate so late.

216
02:01:10,967 --> 02:01:12,867
We're going.

217
02:03:30,106 --> 02:03:34,406
Come home early tomorrow if you like,
and bring Yan along for a snack.

218
02:03:35,378 --> 02:03:37,938
I think Yan's in love
with the nurse.

219
02:03:38,114 --> 02:03:41,641
He didn't catch the streetcar.
Said he had things to do.

220
02:03:41,818 --> 02:03:46,152
But when I passed by later,
he hadn't left.

221
02:03:47,323 --> 02:03:50,224
He bought a book
that explains lots of things

222
02:03:50,393 --> 02:03:52,725
about climaxes and orgasms.

223
02:03:52,896 --> 02:03:55,956
He says we should be interested
in women at our age,

224
02:03:56,132 --> 02:03:58,430
but he doesn't want
some young girl.

225
02:03:58,601 --> 02:04:01,035
He says a man's penis
is like a sword.

226
02:04:01,204 --> 02:04:03,866
The deeper you thrust it in,
the better.

227
02:04:04,040 --> 02:04:06,235
<i>But I thought,
"A sword hurts."</i>

228
02:04:06,409 --> 02:04:09,606
<i>He said,
"True, but it's like fire."</i>

229
02:04:09,779 --> 02:04:12,077
But then where's the pleasure?

230
02:04:12,248 --> 02:04:14,478
There's no point
talking about these things.

231
02:04:14,651 --> 02:04:18,087
He's the one who told me
everything when I was 10.

232
02:04:18,254 --> 02:04:21,382
I said, "What?
Dad does that to Mom?"

233
02:04:21,558 --> 02:04:26,018
I hated Dad for months after that,
and I wanted to die.

234
02:04:26,196 --> 02:04:29,393
When he died, I thought
it was punishment from God.

235
02:04:29,566 --> 02:04:32,057
Now I don't even believe
in God anymore.

236
02:04:32,235 --> 02:04:34,863
Yan also said it wasn't just
to make babies.

237
02:04:35,038 --> 02:04:39,805
So I started having nightmares
so you'd stay with me at night

238
02:04:39,976 --> 02:04:43,537
and Dad wouldn't have a chance
to thrust inside you.

239
02:04:43,713 --> 02:04:45,704
You shouldn't have worried.

240
02:04:45,882 --> 02:04:48,350
It's late.
I'm turning out the light.

241
02:04:48,818 --> 02:04:50,080
All right.

242
02:04:51,387 --> 02:04:52,581
Mom?

243
02:04:52,755 --> 02:04:55,747
It's late, Sylvain.
Sleep well.

244
02:05:49,279 --> 02:05:52,248
End of the second day

245
02:13:12,988 --> 02:13:14,819
Did you wash your hands?

246
02:13:22,865 --> 02:13:24,662
Your button.

247
02:14:57,660 --> 02:14:59,321
Sylvain!

248
02:22:27,509 --> 02:22:29,739
- All sold out?
- Is it empty?

249
02:23:48,690 --> 02:23:50,681
A small loaf, ma'am?

250
02:23:51,193 --> 02:23:53,525
Do you have a bag?

251
02:24:05,774 --> 02:24:07,139
You have a bag?

252
02:24:07,309 --> 02:24:09,436
Give me a new one.

253
02:24:16,651 --> 02:24:18,949
That's 10 francs 50.

254
02:24:33,602 --> 02:24:36,901
- Thanks.
- Good-bye.

255
02:52:47,661 --> 02:52:52,394
What a beautiful little boy!

256
02:54:44,811 --> 02:54:48,372
What's this all about?

257
02:57:21,568 --> 02:57:23,968
<i>Thanks. Good-bye.</i>

258
02:58:56,963 --> 02:58:58,555
Oh, miss?

259
02:59:01,534 --> 02:59:03,195
Yes, ma'am.

260
02:59:03,369 --> 02:59:07,362
Would you happen to have
a button like this?

261
02:59:07,540 --> 02:59:10,373
I've looked,
but I couldn't find any.

262
02:59:12,011 --> 02:59:16,471
- Maybe these?
- No, they're a bit too small.

263
02:59:19,853 --> 02:59:21,514
Good-bye.

264
02:59:40,940 --> 02:59:43,670
I know it's hard
to match it perfectly,

265
02:59:43,843 --> 02:59:47,006
because my sister Fernande
sent it from Canada.

266
02:59:47,180 --> 02:59:50,479
She sent it years ago,
but Sylvain's only wearing it now

267
02:59:50,650 --> 02:59:53,813
because it was too large.

268
02:59:54,220 --> 02:59:58,919
When Sylvain was six, my sister
came back to Belgium for three months.

269
02:59:59,092 --> 03:00:02,550
He slept in our room...
my husband was still alive.

270
03:00:04,097 --> 03:00:09,125
And Fernande slept on the couch
with Jonathan... "Jon" for short.

271
03:00:09,702 --> 03:00:13,365
Jon was five, but he was taller
and stouter than Sylvain.

272
03:00:13,540 --> 03:00:16,668
So if I write her for one,

273
03:00:16,843 --> 03:00:19,403
it's quite possible
that this style no longer exists.

274
03:00:19,579 --> 03:00:23,481
Since people always say Europe
is five years behind America,

275
03:00:23,650 --> 03:00:25,550
I thought I might find one.

276
03:00:25,718 --> 03:00:27,982
It's practically impossible
to find the same thing.

277
03:00:28,154 --> 03:00:30,145
Better just to change
all the buttons.

278
03:00:30,323 --> 03:00:33,019
It makes a garment look new.

279
03:00:33,192 --> 03:00:35,820
Like going to the hairdresser's.

280
03:00:58,484 --> 03:01:01,419
I've never seen
buttons like that.

281
03:01:02,589 --> 03:01:05,956
Try in the square.
You might find some there.

282
03:02:08,054 --> 03:02:09,544
Isn't Gis�le here today?

283
03:02:09,722 --> 03:02:11,747
She gets off work at 4:00.

284
03:02:11,924 --> 03:02:14,324
I see.
Coffee, please.

285
03:02:59,906 --> 03:03:01,533
Here you are.

