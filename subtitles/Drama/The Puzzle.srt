﻿[Script Info]
Title: HorribleSubs
ScriptType: v4.00+
WrapStyle: 0
PlayResX: 848
PlayResY: 480
ScaledBorderAndShadow: yes

[V4+ Styles]
Format: Name, Fontname, Fontsize, PrimaryColour, SecondaryColour, OutlineColour, BackColour, Bold, Italic, Underline, StrikeOut, ScaleX, ScaleY, Spacing, Angle, BorderStyle, Outline, Shadow, Alignment, MarginL, MarginR, MarginV, Encoding
Style: B1,Commerce Black Condensed SSi,64,&H00FFFFFF,&H0000FFFF,&H00000000,&H7F404040,1,0,0,0,100,100,0,0,1,2,1,2,93,93,40,0
Style: OS,Commerce Black Condensed SSi,36,&H00FFFFFF,&H0000FFFF,&H00000000,&H7F404040,1,0,0,0,100,100,0,0,1,2,1,8,1,1,20,0
Style: Default,Commerce Black Condensed SSi,40,&H00FFFFFF,&H000000FF,&H00000000,&H96000000,0,0,0,0,100,100,0,0,1,2,1.2,2,30,30,30,1
Style: Italics,Commerce Black Condensed SSi,40,&H00FFFFFF,&H0000FFFF,&H00000000,&H7F404040,1,1,0,0,100,100,0,0,1,2,1,2,27,27,29,0
Style: Copy of Italics,Commerce Black Condensed SSi,40,&H00FFFFFF,&H0000FFFF,&H00000000,&H7F404040,1,1,0,0,100,100,0,0,1,2,1,8,27,27,29,0
Style: Copy of OS,Commerce Black Condensed SSi,30,&H00FFFFFF,&H0000FFFF,&H00000000,&H7F404040,1,0,0,0,100,100,0,0,1,2,1,8,27,1,1,0
Style: Copy of Default,Commerce Black Condensed SSi,40,&H00FFFFFF,&H0000FFFF,&H00000000,&H7F404040,1,0,0,0,100,100,0,0,1,2,1,8,27,27,29,0

[Events]
Format: Layer, Start, End, Style, Name, MarginL, MarginR, MarginV, Effect, Text

Dialogue: 0,0:00:06.27,0:00:10.29,Default,,0000,0000,0000,,This is... the house I \Nlived in a long time ago.
Dialogue: 0,0:00:20.32,0:00:23.62,Default,,0000,0000,0000,,What is this about, Orpheus?!
Dialogue: 0,0:00:24.46,0:00:28.62,Default,,0000,0000,0000,,This puzzle has transcended time and space.
Dialogue: 0,0:00:29.00,0:00:30.59,Default,,0000,0000,0000,,It is the Time Maze!
Dialogue: 0,0:00:31.10,0:00:34.83,Default,,0000,0000,0000,,Change your past and seize...
Dialogue: 0,0:00:35.87,0:00:38.30,Default,,0000,0000,0000,,the future where you become a Phi-Brain!
Dialogue: 0,0:02:10.06,0:02:13.98,Default,,0000,0000,0000,,Even If Something’s Missing
Dialogue: 0,0:02:18.27,0:02:23.03,Default,,0000,0000,0000,,If I realized back then that the POG\Nwas keeping an eye on me...
Dialogue: 0,0:02:29.71,0:02:31.37,Default,,0000,0000,0000,,I solved it!
Dialogue: 0,0:02:35.90,0:02:38.00,Default,,0000,0000,0000,,Kaito, it’s dinner time.
Dialogue: 0,0:02:38.34,0:02:41.31,Default,,0000,0000,0000,,Hey, Mom! What’s that?
Dialogue: 0,0:02:41.67,0:02:42.39,Default,,0000,0000,0000,,Huh?
Dialogue: 0,0:02:43.61,0:02:46.30,Default,,0000,0000,0000,,Orpheus is showing me another \None of his fake pasts...
Dialogue: 0,0:02:46.76,0:02:49.39,Default,,0000,0000,0000,,No, that’s not right! This is...
Dialogue: 0,0:02:51.64,0:02:56.44,Default,,0000,0000,0000,,After Mama and Daddy died,\NI lived on my own, just like that.
Dialogue: 0,0:02:57.16,0:03:00.40,Default,,0000,0000,0000,,If only I had met Jin sooner...
Dialogue: 0,0:03:01.07,0:03:02.16,Default,,0000,0000,0000,,Excuse me...
Dialogue: 0,0:03:03.32,0:03:05.70,Default,,0000,0000,0000,,I seem to have gotten lost.
Dialogue: 0,0:03:06.03,0:03:07.70,Default,,0000,0000,0000,,Jin! But why?
Dialogue: 0,0:03:08.59,0:03:12.46,Default,,0000,0000,0000,,Did the past change to \Nmatch what I was thinking?!
Dialogue: 0,0:03:12.77,0:03:15.38,Default,,0000,0000,0000,,I’m sorry, Kaito.
Dialogue: 0,0:03:16.21,0:03:16.96,Default,,0000,0000,0000,,Kaito!
Dialogue: 0,0:03:16.96,0:03:18.05,Default,,0000,0000,0000,,I get it.
Dialogue: 0,0:03:16.96,0:03:18.75,Copy of Default,,0000,0000,0000,,\NWhat’s wrong, Mom?
Dialogue: 0,0:03:18.71,0:03:20.87,Default,,0000,0000,0000,,He wants us to get through this maze
Dialogue: 0,0:03:20.87,0:03:23.28,Default,,0000,0000,0000,,without letting it consume \Nour hearts, doesn’t he?
Dialogue: 0,0:03:24.68,0:03:28.93,Default,,0000,0000,0000,,The puzzle contains... \Nwhatever pasts they desire?!
Dialogue: 0,0:03:29.84,0:03:33.98,Default,,0000,0000,0000,,Exactly! That is what makes \Nit the Puzzle of God!
Dialogue: 0,0:03:35.09,0:03:38.73,Default,,0000,0000,0000,,How am I supposed to go \Nabout solving this puzzle?
Dialogue: 0,0:03:39.43,0:03:41.24,Default,,0000,0000,0000,,But I have to solve it!
Dialogue: 0,0:03:42.74,0:03:44.26,Default,,0000,0000,0000,,I must save Jin!
Dialogue: 0,0:03:47.57,0:03:48.85,Default,,0000,0000,0000,,They were fakes...
Dialogue: 0,0:03:49.37,0:03:51.04,Default,,0000,0000,0000,,They weren’t my mom and dad...
Dialogue: 0,0:03:51.74,0:03:55.75,Default,,0000,0000,0000,,Mom and Dad didn’t lose their lives, but...
Dialogue: 0,0:03:56.67,0:03:57.74,Default,,0000,0000,0000,,Are you sure?
Dialogue: 0,0:04:00.66,0:04:02.34,Default,,0000,0000,0000,,Didn’t you love them?
Dialogue: 0,0:04:02.68,0:04:04.80,Default,,0000,0000,0000,,Are you sure you want to \Nlet them go over this?
Dialogue: 0,0:04:05.05,0:04:05.80,Default,,0000,0000,0000,,Raetsel?!
Dialogue: 0,0:04:06.30,0:04:07.84,Default,,0000,0000,0000,,Kaito Daimon!
Dialogue: 0,0:04:08.55,0:04:12.68,Default,,0000,0000,0000,,Kaito Daimon’s past is \Noverlapping with mine.
Dialogue: 0,0:04:15.85,0:04:17.11,Default,,0000,0000,0000,,It’s a promise!
Dialogue: 0,0:04:17.52,0:04:19.65,Default,,0000,0000,0000,,I’m holding you to this promise!
Dialogue: 0,0:04:26.81,0:04:29.95,Default,,0000,0000,0000,,I can’t believe we’re going on a\Njourney with Kaito Daimon!
Dialogue: 0,0:04:33.79,0:04:37.28,Default,,0000,0000,0000,,Did Raetsel wish for this past?
Dialogue: 0,0:04:38.02,0:04:39.54,Default,,0000,0000,0000,,Or did I...?
Dialogue: 0,0:04:48.79,0:04:50.16,Default,,0000,0000,0000,,I solved it, Jin!
Dialogue: 0,0:04:50.88,0:04:52.72,Default,,0000,0000,0000,,So did I! Lookie!
Dialogue: 0,0:04:53.43,0:04:55.98,Default,,0000,0000,0000,,Oh! You two are good!
Dialogue: 0,0:04:56.56,0:04:57.92,Default,,0000,0000,0000,,This feels nostalgic.
Dialogue: 0,0:04:58.35,0:05:00.98,Default,,0000,0000,0000,,I don’t care to change the past anymore,
Dialogue: 0,0:05:01.44,0:05:04.65,Default,,0000,0000,0000,,but I wish we coulda hung out \Nwith Jin like that forever...
Dialogue: 0,0:05:07.15,0:05:10.08,Default,,0000,0000,0000,,Jin, you look like you truly \Nare enjoying yourself...
Dialogue: 0,0:05:16.04,0:05:16.73,Default,,0000,0000,0000,,Jin!
Dialogue: 0,0:05:16.73,0:05:17.58,Default,,0000,0000,0000,,Jin!
Dialogue: 0,0:05:19.11,0:05:25.13,Default,,0000,0000,0000,,That’s right. Throughout our journey,\NJin was always suffering in agony...
Dialogue: 0,0:05:27.64,0:05:28.78,Default,,0000,0000,0000,,I didn’t know...
Dialogue: 0,0:05:29.21,0:05:34.14,Default,,0000,0000,0000,,I’ve never seen Jin suffer like that over\Nsolving a puzzle, not even once.
Dialogue: 0,0:05:34.23,0:05:34.89,Default,,0000,0000,0000,,Here.
Dialogue: 0,0:05:39.08,0:05:41.65,Default,,0000,0000,0000,,No fair! You gotta give me something!
Dialogue: 0,0:05:43.40,0:05:44.90,Default,,0000,0000,0000,,I’ll give you something next time, Kaito.
Dialogue: 0,0:05:45.20,0:05:48.27,Copy of Default,,0000,0000,0000,,Why not now?! I want something!
Dialogue: 0,0:05:45.67,0:05:50.56,Default,,0000,0000,0000,,If we continued to travel like this,\NJin would never have left me...
Dialogue: 0,0:05:51.61,0:05:53.22,Default,,0000,0000,0000,,Jin! Kaito!
Dialogue: 0,0:05:54.20,0:05:54.87,Default,,0000,0000,0000,,Huh?
Dialogue: 0,0:05:54.87,0:05:55.79,Default,,0000,0000,0000,,What is it?
Dialogue: 0,0:05:58.50,0:06:02.42,Default,,0000,0000,0000,,Will the three of us be together \Nforever, just like this?
Dialogue: 0,0:06:02.76,0:06:05.59,Default,,0000,0000,0000,,Huh? Of course we will!
Dialogue: 0,0:06:05.59,0:06:09.26,Default,,0000,0000,0000,,Yeah! We’ll be together forever, Raetsel!
Dialogue: 0,0:06:10.21,0:06:12.52,Default,,0000,0000,0000,,Right, all three of us...
Dialogue: 0,0:06:23.73,0:06:26.07,Default,,0000,0000,0000,,Raetsel, this is where we part ways.
Dialogue: 0,0:06:27.15,0:06:28.82,Default,,0000,0000,0000,,Why are you leaving, Jin?!
Dialogue: 0,0:06:29.24,0:06:31.57,Default,,0000,0000,0000,,Is he going to solve the Puzzle of God...?!
Dialogue: 0,0:06:32.06,0:06:34.58,Default,,0000,0000,0000,,Is he leaving to battle Count Pythagoras?!
Dialogue: 0,0:06:35.21,0:06:37.66,Default,,0000,0000,0000,,Kaito, you’re coming with me.
Dialogue: 0,0:06:38.66,0:06:39.67,Default,,0000,0000,0000,,No!
Dialogue: 0,0:06:40.32,0:06:42.68,Default,,0000,0000,0000,,J-just me?
Dialogue: 0,0:06:43.21,0:06:47.02,Default,,0000,0000,0000,,I want you to see this \Nthrough to the end, Kaito.
Dialogue: 0,0:06:47.74,0:06:48.51,Default,,0000,0000,0000,,Take me with--
Dialogue: 0,0:06:48.51,0:06:49.32,Default,,0000,0000,0000,,Raetsel!
Dialogue: 0,0:06:50.30,0:06:56.21,Default,,0000,0000,0000,,You’re missing something, \Nso I can’t risk taking you.
Dialogue: 0,0:06:57.18,0:06:59.60,Default,,0000,0000,0000,,Jin, why would you say such a thing?!
Dialogue: 0,0:07:00.39,0:07:04.15,Default,,0000,0000,0000,,I don’t want this past! Why am I seeing it?
Dialogue: 0,0:07:04.54,0:07:06.23,Default,,0000,0000,0000,,Let’s go, Kaito.
Dialogue: 0,0:07:06.23,0:07:07.32,Default,,0000,0000,0000,,But...
Dialogue: 0,0:07:07.67,0:07:09.19,Default,,0000,0000,0000,,Wait, Jin.
Dialogue: 0,0:07:11.49,0:07:14.16,Default,,0000,0000,0000,,Why, Jin?
Dialogue: 0,0:07:14.64,0:07:18.10,Default,,0000,0000,0000,,Raetsel, has the past consumed your heart?!
Dialogue: 0,0:07:18.99,0:07:21.25,Default,,0000,0000,0000,,You’re leaving me behind again...
Dialogue: 0,0:07:24.04,0:07:26.75,Default,,0000,0000,0000,,How dare you abandon me...!
Dialogue: 0,0:07:27.26,0:07:29.26,Default,,0000,0000,0000,,Pull yourself together, Raetsel!
Dialogue: 0,0:07:29.75,0:07:31.17,Default,,0000,0000,0000,,I won’t accept this...
Dialogue: 0,0:07:31.60,0:07:33.51,Default,,0000,0000,0000,,I won’t accept this!
Dialogue: 0,0:07:33.84,0:07:36.18,Default,,0000,0000,0000,,Raetsel!
Dialogue: 0,0:07:40.81,0:07:41.77,Default,,0000,0000,0000,,What happened?!
Dialogue: 0,0:07:42.27,0:07:44.00,Default,,0000,0000,0000,,I’m all the way back here again!
Dialogue: 0,0:07:44.81,0:07:46.02,Default,,0000,0000,0000,,What’s going on?
Dialogue: 0,0:07:48.44,0:07:49.53,Default,,0000,0000,0000,,Are you sure?
Dialogue: 0,0:07:53.07,0:07:54.07,Default,,0000,0000,0000,,Raetsel!
Dialogue: 0,0:07:54.43,0:07:57.41,Default,,0000,0000,0000,,I get it! She plans on redoing her past!
Dialogue: 0,0:07:57.70,0:07:58.69,Default,,0000,0000,0000,,Starting here!
Dialogue: 0,0:08:07.75,0:08:09.92,Default,,0000,0000,0000,,Raetsel, this is where we part ways.
Dialogue: 0,0:08:11.55,0:08:13.93,Default,,0000,0000,0000,,You’re missing something.
Dialogue: 0,0:08:16.34,0:08:17.43,Default,,0000,0000,0000,,You’re missing something.
Dialogue: 0,0:08:18.30,0:08:19.18,Default,,0000,0000,0000,,You’re missing something!
Dialogue: 0,0:08:20.01,0:08:20.93,Default,,0000,0000,0000,,You’re missing something!
Dialogue: 0,0:08:21.76,0:08:22.69,Default,,0000,0000,0000,,You’re missing something!
Dialogue: 0,0:08:22.89,0:08:25.15,Default,,0000,0000,0000,,No!
Dialogue: 0,0:08:29.98,0:08:32.02,Default,,0000,0000,0000,,You need to stop, Raetsel!
Dialogue: 0,0:08:34.49,0:08:37.83,Default,,0000,0000,0000,,Jin, what the heck is Raetsel missing?!
Dialogue: 0,0:08:37.83,0:08:38.82,Default,,0000,0000,0000,,Here.
Dialogue: 0,0:08:42.58,0:08:43.56,Default,,0000,0000,0000,,What’s this?
Dialogue: 0,0:08:43.83,0:08:45.21,Default,,0000,0000,0000,,My good luck charm.
Dialogue: 0,0:08:45.63,0:08:48.14,Default,,0000,0000,0000,,I’d like you to keep it from now on.
Dialogue: 0,0:08:48.14,0:08:50.34,Default,,0000,0000,0000,,When I’m challenging a Puzzle of Fools...
Dialogue: 0,0:08:50.84,0:08:53.59,Default,,0000,0000,0000,,Not good! At this rate, \Nit’s going to consume me!
Dialogue: 0,0:08:57.27,0:09:01.01,Default,,0000,0000,0000,,Raetsel, you’re missing something.
Dialogue: 0,0:09:02.10,0:09:02.98,Default,,0000,0000,0000,,Raetsel!
Dialogue: 0,0:09:03.18,0:09:05.68,Default,,0000,0000,0000,,No! No, no, no!
Dialogue: 0,0:09:05.68,0:09:08.81,Default,,0000,0000,0000,,I will NEVER accept that!
Dialogue: 0,0:09:14.27,0:09:15.19,Default,,0000,0000,0000,,Not again...
Dialogue: 0,0:09:15.19,0:09:16.24,Default,,0000,0000,0000,,We’re back here again!
Dialogue: 0,0:09:18.53,0:09:19.42,Default,,0000,0000,0000,,Are you sure?
Dialogue: 0,0:09:20.41,0:09:21.91,Default,,0000,0000,0000,,Didn’t you love them?
Dialogue: 0,0:09:22.63,0:09:24.29,Default,,0000,0000,0000,,Are you sure you want to \Nlet them go over this?
Dialogue: 0,0:09:25.33,0:09:27.00,Default,,0000,0000,0000,,Raetsel vanished?!
Dialogue: 0,0:09:46.35,0:09:47.76,Default,,0000,0000,0000,,Good morning...
Dialogue: 0,0:09:48.40,0:09:49.39,Default,,0000,0000,0000,,Good morning!
Dialogue: 0,0:09:49.98,0:09:51.22,Default,,0000,0000,0000,,Good morning.
Dialogue: 0,0:09:54.94,0:09:57.83,Default,,0000,0000,0000,,What is this all about?!
Dialogue: 0,0:09:58.69,0:10:01.87,Default,,0000,0000,0000,,Her desire to reject that bitter memory
Dialogue: 0,0:10:02.32,0:10:05.45,Default,,0000,0000,0000,,has caused Raetsel to deviate \Nfrom the flow of time.
Dialogue: 0,0:10:06.08,0:10:08.75,Default,,0000,0000,0000,,She has deviated from the flow of time?
Dialogue: 0,0:10:09.60,0:10:13.46,Default,,0000,0000,0000,,Raetsel is no longer in \None of the past worlds.
Dialogue: 0,0:10:14.54,0:10:17.71,Default,,0000,0000,0000,,She is in a fabricated world that she\Ncreated to protect her own mind.
Dialogue: 0,0:10:18.12,0:10:19.80,Default,,0000,0000,0000,,Her heart has been completely consumed.
Dialogue: 0,0:10:20.51,0:10:23.16,Default,,0000,0000,0000,,No one can contact her there.
Dialogue: 0,0:10:23.16,0:10:27.60,Default,,0000,0000,0000,,\NLikewise, it is impossible for her to ever come back.
Dialogue: 0,0:10:28.25,0:10:31.27,Default,,0000,0000,0000,,Then that would make this \Npuzzle’s winner...
Dialogue: 0,0:10:32.52,0:10:35.44,Default,,0000,0000,0000,,Kaito Daimon.
Dialogue: 0,0:10:41.28,0:10:42.44,Default,,0000,0000,0000,,Wanna tag along?
Dialogue: 0,0:10:43.19,0:10:44.28,Default,,0000,0000,0000,,You could come with me.
Dialogue: 0,0:10:44.91,0:10:47.41,Default,,0000,0000,0000,,You have a gift with puzzles.
Dialogue: 0,0:10:48.08,0:10:49.74,Default,,0000,0000,0000,,What’s goin’ on?
Dialogue: 0,0:10:49.74,0:10:53.17,Default,,0000,0000,0000,,Doesn’t this Jin know about Raetsel?
Dialogue: 0,0:10:53.61,0:10:56.36,Default,,0000,0000,0000,,Have you been traveling by \Nyourself this whole time?
Dialogue: 0,0:10:56.36,0:10:57.54,Default,,0000,0000,0000,,Yeah, that’s right.
Dialogue: 0,0:10:58.78,0:11:02.22,Default,,0000,0000,0000,,That’s why I’d like a talented \Npartner such as yourself.
Dialogue: 0,0:11:02.59,0:11:06.81,Default,,0000,0000,0000,,Does this mean Raetsel chose a\Npast where she never met Jin?!
Dialogue: 0,0:11:07.33,0:11:09.56,Default,,0000,0000,0000,,Come on! Let’s go together.
Dialogue: 0,0:11:10.35,0:11:11.52,Default,,0000,0000,0000,,Kaito!
Dialogue: 0,0:11:12.73,0:11:14.82,Default,,0000,0000,0000,,I had a kinda weird dream.
Dialogue: 0,0:11:15.19,0:11:17.10,Default,,0000,0000,0000,,A dream? What was it about?
Dialogue: 0,0:11:18.85,0:11:20.13,Default,,0000,0000,0000,,I don’t remember.
Dialogue: 0,0:11:20.48,0:11:22.61,Default,,0000,0000,0000,,More importantly, did you fill that out?
Dialogue: 0,0:11:23.28,0:11:24.28,Default,,0000,0000,0000,,Fill what out?
Dialogue: 0,0:11:24.28,0:11:26.07,Default,,0000,0000,0000,,The questionnaire about your future plans.
Dialogue: 0,0:11:26.07,0:11:27.87,Default,,0000,0000,0000,,Didn’t you say it was due today?
Dialogue: 0,0:11:29.70,0:11:31.96,Default,,0000,0000,0000,,What? You still haven’t filled it out?
Dialogue: 0,0:11:32.83,0:11:37.46,Default,,0000,0000,0000,,It’s nice that you’re carefully\Nconsidering your future, but...
Dialogue: 0,0:11:37.46,0:11:38.71,Default,,0000,0000,0000,,I’m sorry.
Dialogue: 0,0:11:38.92,0:11:40.72,Default,,0000,0000,0000,,Our student-teacher meeting is next week,
Dialogue: 0,0:11:41.09,0:11:43.80,Default,,0000,0000,0000,,so make sure to submit it \Nbefore then. Sound good?
Dialogue: 0,0:11:43.80,0:11:44.72,Default,,0000,0000,0000,,Yes.
Dialogue: 0,0:11:48.47,0:11:51.72,Default,,0000,0000,0000,,I wrote that I wanted to go on to college.
Dialogue: 0,0:11:52.35,0:11:54.23,Default,,0000,0000,0000,,I put that I wanna be a nurse!
Dialogue: 0,0:11:54.23,0:11:55.52,Default,,0000,0000,0000,,How about you, Miss Erena?
Dialogue: 0,0:11:55.52,0:11:57.98,Default,,0000,0000,0000,,I think I really do want to become a model...
Dialogue: 0,0:11:58.36,0:12:02.37,Default,,0000,0000,0000,,Actually, I’ve already sent \Nmy resume to an audition!
Dialogue: 0,0:12:02.69,0:12:04.09,Default,,0000,0000,0000,,Awesome!
Dialogue: 0,0:12:04.09,0:12:07.88,Default,,0000,0000,0000,,Miss Raetsel, is there \Nanything you’d like to do?
Dialogue: 0,0:12:10.66,0:12:13.00,Default,,0000,0000,0000,,Raetsel, you’ve always been like this.
Dialogue: 0,0:12:13.55,0:12:19.45,Default,,0000,0000,0000,,You get top grades, excel in sports,\Nand could have any guy you wanted!
Dialogue: 0,0:12:19.45,0:12:21.87,Default,,0000,0000,0000,,But you always seem so bored.
Dialogue: 0,0:12:21.87,0:12:24.51,Default,,0000,0000,0000,,That’s not... actually true.
Dialogue: 0,0:12:26.26,0:12:27.51,Default,,0000,0000,0000,,Manager!
Dialogue: 0,0:12:28.62,0:12:33.27,Default,,0000,0000,0000,,Hey, Erena! Quit chitchatting \Nand come hit the track.
Dialogue: 0,0:12:33.47,0:12:35.73,Default,,0000,0000,0000,,Yeah, yeah! I know the drill.
Dialogue: 0,0:12:35.98,0:12:38.51,Default,,0000,0000,0000,,The track team has practice during lunch?
Dialogue: 0,0:12:38.51,0:12:40.49,Default,,0000,0000,0000,,Only when a tournament is coming up!
Dialogue: 0,0:12:40.49,0:12:44.78,Default,,0000,0000,0000,,I’m not about to let victory escape\Nus as long as I’m the captain!
Dialogue: 0,0:12:45.29,0:12:47.28,Default,,0000,0000,0000,,Later! I’ll see you around.
Dialogue: 0,0:12:47.28,0:12:48.03,Default,,0000,0000,0000,,Uh-huh.
Dialogue: 0,0:12:48.41,0:12:50.85,Default,,0000,0000,0000,,I collected the data on all of our opponents.
Dialogue: 0,0:12:50.85,0:12:52.45,Default,,0000,0000,0000,,What?! You’re done already?
Dialogue: 0,0:12:52.45,0:12:54.04,Default,,0000,0000,0000,,Way to go, Cube!
Dialogue: 0,0:12:54.54,0:12:56.55,Default,,0000,0000,0000,,Mr. Gammon and Miss Erena!
Dialogue: 0,0:12:56.55,0:13:00.24,Default,,0000,0000,0000,,Isn’t the romance between a \Ncaptain and his manager nice?
Dialogue: 0,0:13:00.24,0:13:02.80,Default,,0000,0000,0000,,Someday Mr. Jikukawa and \NI will be a couple...
Dialogue: 0,0:13:03.29,0:13:06.24,Default,,0000,0000,0000,,But I don’t think you \Nshould date your teacher...
Dialogue: 0,0:13:06.24,0:13:09.49,Default,,0000,0000,0000,,I won’t confess how I feel \Nuntil after graduation!
Dialogue: 0,0:13:19.56,0:13:22.82,Default,,0000,0000,0000,,I wonder what I like...
Dialogue: 0,0:13:23.40,0:13:25.32,Default,,0000,0000,0000,,What do I want to do?
Dialogue: 0,0:13:33.46,0:13:37.16,Default,,0000,0000,0000,,Kaito Daimon hasn’t moved from\Nthat location for a while now.
Dialogue: 0,0:13:40.79,0:13:41.59,Default,,0000,0000,0000,,He moved!
Dialogue: 0,0:14:16.08,0:14:17.24,Default,,0000,0000,0000,,Excuse me!
Dialogue: 0,0:14:17.24,0:14:20.17,Default,,0000,0000,0000,,Here. Didn’t you solve \Nit just the other day?
Dialogue: 0,0:14:20.66,0:14:23.41,Default,,0000,0000,0000,,It’d be sad if you threw it away.
Dialogue: 0,0:14:23.83,0:14:24.96,Default,,0000,0000,0000,,How do you figure?
Dialogue: 0,0:14:25.14,0:14:28.96,Default,,0000,0000,0000,,Well, don’t you like puzzles?\NIsn’t that why you solve them?
Dialogue: 0,0:14:29.47,0:14:31.44,Default,,0000,0000,0000,,It’d be sad if you threw it \Naway even though you like it...
Dialogue: 0,0:14:32.49,0:14:33.64,Default,,0000,0000,0000,,It’s revenge.
Dialogue: 0,0:14:34.87,0:14:37.18,Default,,0000,0000,0000,,I will solve all of the puzzles in the world.
Dialogue: 0,0:14:38.35,0:14:40.14,Default,,0000,0000,0000,,That is my revenge.
Dialogue: 0,0:14:40.89,0:14:42.60,Default,,0000,0000,0000,,Your revenge...?
Dialogue: 0,0:14:57.41,0:14:59.29,Default,,0000,0000,0000,,Is that another puzzle?
Dialogue: 0,0:15:00.91,0:15:03.46,Default,,0000,0000,0000,,Looks fun! Let me try my hand at it.
Dialogue: 0,0:15:03.46,0:15:04.00,Default,,0000,0000,0000,,H-hey!
Dialogue: 0,0:15:05.25,0:15:07.76,Default,,0000,0000,0000,,I’m Raetsel. And you are?
Dialogue: 0,0:15:11.51,0:15:12.52,Default,,0000,0000,0000,,It’s nice to meet you!
Dialogue: 0,0:15:30.03,0:15:33.88,Default,,0000,0000,0000,,Amazing! It’s almost like a secret fort.
Dialogue: 0,0:15:37.99,0:15:39.79,Default,,0000,0000,0000,,What made you start following me around?
Dialogue: 0,0:15:41.29,0:15:44.06,Default,,0000,0000,0000,,The way you were playing \Nwith puzzles... I guess.
Dialogue: 0,0:15:44.06,0:15:45.04,Default,,0000,0000,0000,,Puzzles?
Dialogue: 0,0:15:45.04,0:15:47.30,Default,,0000,0000,0000,,I thought they seemed fun.
Dialogue: 0,0:15:47.30,0:15:50.79,Default,,0000,0000,0000,,\NNothing else has ever struck me that way before.
Dialogue: 0,0:15:52.92,0:15:54.34,Default,,0000,0000,0000,,I wonder why...
Dialogue: 0,0:16:24.33,0:16:27.09,Copy of Default,,0000,0000,0000,,How to Make Puzzles
Dialogue: 0,0:16:25.83,0:16:26.67,Default,,0000,0000,0000,,Kaito...
Dialogue: 0,0:16:27.75,0:16:29.84,Default,,0000,0000,0000,,Kaito... Daimon...
Dialogue: 0,0:17:27.32,0:17:28.89,Default,,0000,0000,0000,,All of them are missing something...
Dialogue: 0,0:17:28.89,0:17:31.31,Default,,0000,0000,0000,,Nuh-huh. They aren’t missing anything!
Dialogue: 0,0:17:32.11,0:17:33.48,Default,,0000,0000,0000,,Uh, but...
Dialogue: 0,0:17:33.82,0:17:36.69,Default,,0000,0000,0000,,Aren’t these paintings incomplete?
Dialogue: 0,0:17:36.69,0:17:39.28,Default,,0000,0000,0000,,Nuh-huh! They’re all done.
Dialogue: 0,0:17:39.45,0:17:41.49,Default,,0000,0000,0000,,Everything is missing something!
Dialogue: 0,0:17:41.99,0:17:43.72,Default,,0000,0000,0000,,Same for people!
Dialogue: 0,0:17:45.87,0:17:49.79,Default,,0000,0000,0000,,Everyone is missing something.\NWe’re lacking something or other.
Dialogue: 0,0:17:50.48,0:17:51.80,Default,,0000,0000,0000,,And that’s just fine!
Dialogue: 0,0:17:52.00,0:17:56.80,Default,,0000,0000,0000,,So there is no need to try \Nand fill those holes in.
Dialogue: 0,0:17:57.34,0:17:59.97,Default,,0000,0000,0000,,If you do, they’ll just get bigger.
Dialogue: 0,0:17:59.97,0:18:02.31,Default,,0000,0000,0000,,If you try to draw closer, \Nthey’ll just go farther away.
Dialogue: 0,0:18:02.66,0:18:03.97,Default,,0000,0000,0000,,Isn’t it strange?
Dialogue: 0,0:18:14.73,0:18:16.94,Default,,0000,0000,0000,,Everyone lacks something...
Dialogue: 0,0:19:04.28,0:19:05.41,Default,,0000,0000,0000,,Kaito!
Dialogue: 0,0:19:08.08,0:19:10.08,Default,,0000,0000,0000,,Why did you take off without telling me?
Dialogue: 0,0:19:10.96,0:19:13.26,Default,,0000,0000,0000,,Was I getting in your way?
Dialogue: 0,0:19:13.26,0:19:14.09,Default,,0000,0000,0000,,No.
Dialogue: 0,0:19:14.42,0:19:15.54,Default,,0000,0000,0000,,Then why...?
Dialogue: 0,0:19:15.67,0:19:19.59,Default,,0000,0000,0000,,My mom and dad weren’t my real parents.
Dialogue: 0,0:19:21.67,0:19:23.35,Default,,0000,0000,0000,,They belonged to a certain organization
Dialogue: 0,0:19:23.78,0:19:27.72,Default,,0000,0000,0000,,and were teaching me how to solve\Npuzzles to achieve some goal.
Dialogue: 0,0:19:28.22,0:19:30.53,Default,,0000,0000,0000,,Is that why you’re getting \Nback at the puzzles?
Dialogue: 0,0:19:31.39,0:19:32.89,Default,,0000,0000,0000,,Even now, I’m scared.
Dialogue: 0,0:19:33.60,0:19:36.19,Default,,0000,0000,0000,,Scared someone is going to betray me again.
Dialogue: 0,0:19:36.98,0:19:39.86,Default,,0000,0000,0000,,Is that why you’ve been living by yourself?
Dialogue: 0,0:19:40.49,0:19:43.43,Default,,0000,0000,0000,,Is that why you tried to leave me?
Dialogue: 0,0:19:45.95,0:19:47.87,Default,,0000,0000,0000,,I was starting to enjoy the puzzles.
Dialogue: 0,0:19:49.45,0:19:51.27,Default,,0000,0000,0000,,I used to loathe them,
Dialogue: 0,0:19:51.75,0:19:56.29,Default,,0000,0000,0000,,but when I solve puzzles with \Nyou, they’re kinda fun...
Dialogue: 0,0:19:56.79,0:19:59.21,Default,,0000,0000,0000,,I started to lose sight of myself...
Dialogue: 0,0:20:00.25,0:20:02.89,Default,,0000,0000,0000,,I can understand that. \NI was having fun myself.
Dialogue: 0,0:20:03.84,0:20:07.08,Default,,0000,0000,0000,,The truth is that you like puzzles, Kaito!
Dialogue: 0,0:20:10.90,0:20:14.52,Default,,0000,0000,0000,,You know, I thought I didn’t \Nwant to do anything.
Dialogue: 0,0:20:14.81,0:20:18.06,Default,,0000,0000,0000,,Or that I didn’t like anything at all.
Dialogue: 0,0:20:18.06,0:20:23.32,Default,,0000,0000,0000,,I was afraid that maybe \NI was missing something.
Dialogue: 0,0:20:23.82,0:20:25.49,Default,,0000,0000,0000,,But I had fun...
Dialogue: 0,0:20:27.36,0:20:29.70,Default,,0000,0000,0000,,It’s okay if I’m missing \Nsomething, isn’t it?
Dialogue: 0,0:20:30.49,0:20:33.21,Default,,0000,0000,0000,,Even if I’m lacking, \NI still had a good time.
Dialogue: 0,0:20:33.83,0:20:36.59,Default,,0000,0000,0000,,I genuinely enjoyed myself.
Dialogue: 0,0:20:50.77,0:20:53.32,Default,,0000,0000,0000,,Where am I? Where have I been...?
Dialogue: 0,0:20:55.27,0:20:59.30,Default,,0000,0000,0000,,You got completely absorbed \Ninto that fake world.
Dialogue: 0,0:21:00.73,0:21:03.61,Default,,0000,0000,0000,,Did you come to my rescue?
Dialogue: 0,0:21:04.44,0:21:05.50,Default,,0000,0000,0000,,How?!
Dialogue: 0,0:21:05.99,0:21:07.99,Default,,0000,0000,0000,,I knew the normal method wouldn’t work.
Dialogue: 0,0:21:08.39,0:21:12.26,Default,,0000,0000,0000,,So I thought super hard that \NI wanted to take a path
Dialogue: 0,0:21:12.72,0:21:14.12,Default,,0000,0000,0000,,that I’d never follow in the real world.
Dialogue: 0,0:21:15.06,0:21:17.12,Default,,0000,0000,0000,,And that method worked!
Dialogue: 0,0:21:17.41,0:21:19.63,Default,,0000,0000,0000,,It’s a path you’ve never taken before?
Dialogue: 0,0:21:20.54,0:21:22.46,Default,,0000,0000,0000,,I didn’t go with Jin.
Dialogue: 0,0:21:24.46,0:21:26.97,Default,,0000,0000,0000,,Did you actually chose me over Jin?!
Dialogue: 0,0:21:28.39,0:21:30.74,Default,,0000,0000,0000,,After going through a life \Nwhere I didn’t choose Jin,
Dialogue: 0,0:21:30.74,0:21:32.85,Default,,0000,0000,0000,,I understood how you felt for the first time.
Dialogue: 0,0:21:33.78,0:21:36.15,Default,,0000,0000,0000,,Spending your life hating \Nthe thing you loved...
Dialogue: 0,0:21:36.68,0:21:40.02,Default,,0000,0000,0000,,Getting abandoned by the \Nperson you believed in...
Dialogue: 0,0:21:40.65,0:21:42.38,Default,,0000,0000,0000,,It’s really painful.
Dialogue: 0,0:21:42.71,0:21:45.65,Default,,0000,0000,0000,,Let’s ask Jin what he meant when he\Nsaid you’re missing something!
Dialogue: 0,0:21:46.22,0:21:48.78,Default,,0000,0000,0000,,And while we’re at it,\Nlet’s give him a piece of our minds!
Dialogue: 0,0:21:50.70,0:21:55.41,Default,,0000,0000,0000,,The real Kaito Daimon appeared inside\Nof Raetsel’s fabricated world?!
Dialogue: 0,0:21:55.70,0:21:58.82,Default,,0000,0000,0000,,I thought you said no one could reach her!
Dialogue: 0,0:21:59.33,0:22:01.89,Default,,0000,0000,0000,,After placing himself in the same situation,
Dialogue: 0,0:22:01.89,0:22:04.42,Default,,0000,0000,0000,,\Nhe drew close to Raetsel’s psyche.
Dialogue: 0,0:22:05.21,0:22:07.03,Default,,0000,0000,0000,,Sure enough, you are fascinating...
Dialogue: 0,0:22:08.22,0:22:09.64,Default,,0000,0000,0000,,Kaito Daimon!
Dialogue: 0,0:22:17.77,0:22:18.94,Default,,0000,0000,0000,,Wh-what’s going on?!
Dialogue: 0,0:22:24.90,0:22:25.99,Default,,0000,0000,0000,,Raetsel!
Dialogue: 0,0:22:26.48,0:22:28.74,Default,,0000,0000,0000,,Raetsel!
Dialogue: 0,0:24:03.29,0:24:05.38,Default,,0000,0000,0000,,Jin, thank you.
Dialogue: 0,0:24:05.98,0:24:11.75,Default,,0000,0000,0000,,Everything you taught me through puzzles,\NJin, is still alive inside of me.
Dialogue: 0,0:24:12.30,0:24:14.51,Default,,0000,0000,0000,,So please rest at ease.
Dialogue: 0,0:24:15.10,0:24:18.42,Default,,0000,0000,0000,,No matter what happens, I swear \NI will protect the future!
Dialogue: 0,0:24:19.34,0:24:23.77,Default,,0000,0000,0000,,Next up is the last episode,\N“It’s Insanely Fun to be Alive.”
Dialogue: 0,0:24:19.91,0:24:27.97,Copy of Default,,0000,0000,0000,,It’s Insanely Fun to be Alive
Dialogue: 0,0:24:24.56,0:24:27.21,Default,,0000,0000,0000,,Puzzle time... Let’s begin.
Dialogue: 0,0:24:27.98,0:24:30.99,Copy of Default,,0000,0000,0000,,Eh?! Wait a sec.\NI’m supposed to be the heroine!!
Dialogue: 0,0:24:27.98,0:24:30.99,Default,,0000,0000,0000,,Fin Art: Akeru Otsuki
Dialogue: 0,0:24:30.99,0:24:32.99,Default,,0000,0000,0000,,