1
00:00:00,411 --> 00:00:02,133
"Dashan Temple"

2
00:00:05,695 --> 00:00:07,007
Seven Swords

3
00:00:07,042 --> 00:00:10,746
Episode 38

4
00:01:13,995 --> 00:01:14,998
Chu Zhaonan!

5
00:01:18,044 --> 00:01:21,605
I'm here to take revenge.

6
00:01:23,984 --> 00:01:26,851
You cannot beat me...
Go back!

7
00:01:56,335 --> 00:02:00,734
"Liu Jingyi's Memorial Tablet"

8
00:02:17,137 --> 00:02:18,365
Dad...

9
00:02:20,307 --> 00:02:21,774
I am unfilial.

10
00:02:24,778 --> 00:02:27,169
My martial arts are not good enough...

11
00:02:28,348 --> 00:02:31,448
to take revenge for you.

12
00:02:35,021 --> 00:02:36,249
Dad...

13
00:02:37,215 --> 00:02:38,603
From Heaven...

14
00:02:42,505 --> 00:02:44,192
give me the power...

15
00:02:45,298 --> 00:02:46,994
to kill Chu Zhaonan.

16
00:02:54,808 --> 00:02:55,701
Dad!

17
00:02:57,409 --> 00:02:59,084
Do you hear me?

18
00:02:59,779 --> 00:03:01,144
You have to help me...

19
00:03:02,248 --> 00:03:04,409
to kill Chu Zhaonan.

20
00:03:23,970 --> 00:03:24,672
Yufang.

21
00:03:36,653 --> 00:03:37,119
Go home!

22
00:03:45,948 --> 00:03:46,653
Yufang.

23
00:03:53,767 --> 00:03:55,598
I cannot let you take revenge.

24
00:04:00,040 --> 00:04:01,405
Yufang.

25
00:04:02,269 --> 00:04:02,769
Yufang

26
00:04:06,613 --> 00:04:08,046
You...
Yufang.

27
00:04:09,249 --> 00:04:11,920
You...
You were really going to kill Yufang?

28
00:04:15,522 --> 00:04:16,955
First Brother...
- Speak!

29
00:04:18,391 --> 00:04:19,161
Chu Zhaonan...

30
00:04:20,960 --> 00:04:21,824
Let me ask you...

31
00:04:22,808 --> 00:04:24,829
Why did you kill our jailed brothers?

32
00:04:26,599 --> 00:04:27,560
You're interrogating me?

33
00:04:29,002 --> 00:04:30,264
If I hadn't killed them,

34
00:04:30,603 --> 00:04:31,868
I could not have entered the hall,

35
00:04:32,072 --> 00:04:34,097
to try to kill Duo Geduo.

36
00:04:34,407 --> 00:04:35,499
For this cause...

37
00:04:36,393 --> 00:04:37,463
The old chief is dead.

38
00:04:38,411 --> 00:04:39,537
Jiang Feian is dead.

39
00:04:40,680 --> 00:04:42,511
How many more people will you kill?

40
00:04:42,982 --> 00:04:44,506
However many it takes.

41
00:04:44,751 --> 00:04:47,151
Until I kill Duo Geduo...

42
00:04:47,353 --> 00:04:48,381
I will not stop.

43
00:04:49,289 --> 00:04:50,567
Please explain yourself!

44
00:04:55,829 --> 00:04:57,053
Chief.

45
00:04:57,997 --> 00:04:59,742
Other people may not understand.

46
00:05:00,233 --> 00:05:02,030
But how could you not understand?

47
00:05:03,369 --> 00:05:05,735
My Dragon Sword was made to kill.

48
00:05:11,342 --> 00:05:12,355
But not all of them were killed,

49
00:05:13,013 --> 00:05:14,652
some were knocked unconscious.

50
00:05:15,381 --> 00:05:19,340
If that was not the case,
they wouldn't have lived...

51
00:05:19,552 --> 00:05:20,781
to condemn me.

52
00:05:21,693 --> 00:05:24,053
And during the attack
did Duo Geduo make you kill our people?

53
00:05:25,525 --> 00:05:26,958
What about Jiang Feian?

54
00:05:27,327 --> 00:05:29,090
How did he die?

55
00:05:34,067 --> 00:05:36,233
Jiang Feian?

56
00:05:47,513 --> 00:05:49,735
Explain. Explain.

57
00:05:50,336 --> 00:05:52,946
Why must I explain everything?

58
00:05:54,454 --> 00:05:55,978
Once Duo Geduo is dead,

59
00:05:56,189 --> 00:05:57,816
everything will have been worth it.

60
00:06:03,359 --> 00:06:04,231
Tonight...

61
00:06:04,771 --> 00:06:07,213
I will kill Duo Geduo in Qiantang Jiang.

62
00:06:07,634 --> 00:06:08,896
If you are interested,

63
00:06:09,135 --> 00:06:10,232
you can come and watch.

64
00:06:10,409 --> 00:06:11,864
Chu Zhaonan...

65
00:06:13,073 --> 00:06:16,408
Why did we leave Mount Heaven?
Do you still remember?

66
00:06:17,911 --> 00:06:19,344
Why do we want to kill Duo Geduo?

67
00:06:19,546 --> 00:06:20,774
Do you still not understand?

68
00:06:23,016 --> 00:06:25,644
Master Shadow-glow once said to me...

69
00:06:26,019 --> 00:06:27,611
Don't think about the past,

70
00:06:27,820 --> 00:06:29,720
just concentrate on the present.

71
00:06:29,956 --> 00:06:32,388
I'm saying this to you now.

72
00:06:32,592 --> 00:06:35,527
Don't go to extremes.

73
00:06:35,728 --> 00:06:37,218
Don't be too persistent.

74
00:06:40,867 --> 00:06:42,334
This again.

75
00:06:42,569 --> 00:06:44,703
Don't think about the past or the future.

76
00:06:45,238 --> 00:06:47,938
I don't have any regrets in my life.

77
00:06:49,943 --> 00:06:50,701
Go back now.

78
00:06:51,258 --> 00:06:54,344
We can talk about this later.

79
00:06:58,284 --> 00:06:59,808
Let's go!

80
00:08:15,361 --> 00:08:16,064
Master...

81
00:08:17,147 --> 00:08:18,038
Can a sword...

82
00:08:19,463 --> 00:08:21,053
change a person's nature?

83
00:08:21,734 --> 00:08:27,470
It's not the flag moving.
It's not the wind blowing.

84
00:08:28,241 --> 00:08:30,909
It's your heart that is wavering.

85
00:08:31,444 --> 00:08:32,977
Beware of evil inside.

86
00:08:33,179 --> 00:08:36,333
Don't be too persistent.

87
00:08:41,988 --> 00:08:45,924
This sword...
was given to me by my master.

88
00:08:48,528 --> 00:08:52,769
He could not let go of his burden.

89
00:08:54,067 --> 00:08:55,698
Since he has given this to me,

90
00:08:56,235 --> 00:08:58,303
he has passed on his burden too.

91
00:08:58,838 --> 00:09:00,931
If you put down your sword...

92
00:09:01,291 --> 00:09:05,008
You may also put down your burden.

93
00:09:09,949 --> 00:09:10,631
Listen...

94
00:09:13,298 --> 00:09:14,496
You can hear the sword...

95
00:09:16,740 --> 00:09:17,945
sing its lament.

96
00:09:23,963 --> 00:09:27,464
Form does not differ from void,
nor void from form.

97
00:09:27,834 --> 00:09:31,270
Form is identical to void,
and void identical to form.

98
00:09:31,741 --> 00:09:35,735
So are inception, conception,
thought and consciousness.

99
00:09:37,777 --> 00:09:39,244
That's right.

100
00:09:41,114 --> 00:09:45,847
Form and void are one,
life and death are normal.

101
00:09:46,819 --> 00:09:50,849
Amitabha.
Merciful Buddha.

102
00:09:51,557 --> 00:09:54,588
You have seen it clearly.

103
00:10:24,591 --> 00:10:26,349
You all saw that!

104
00:10:28,194 --> 00:10:30,822
Chu Zhaonan
really wanted to kill Yufang.

105
00:10:59,481 --> 00:11:01,972
"Dashan Temple"

106
00:11:25,785 --> 00:11:26,546
Mulang...

107
00:11:28,139 --> 00:11:29,276
Someone is following us.

108
00:11:30,123 --> 00:11:31,090
Go and take care of him.

109
00:11:31,491 --> 00:11:33,686
I will go with the others
to the warehouse.

110
00:11:36,031 --> 00:11:38,449
Be careful...
We are close to the warehouse.

111
00:11:38,558 --> 00:11:40,630
Don't let anybody know it's whereabouts.

112
00:12:21,040 --> 00:12:22,266
It's you.

113
00:12:22,578 --> 00:12:25,675
You said that if I could catch up with you,

114
00:12:25,845 --> 00:12:28,871
then you would take me
to the Red Spear Society.

115
00:12:37,236 --> 00:12:39,366
I'm worried that tonight will be a trap.

116
00:12:39,997 --> 00:12:42,326
We cannot trust Chu Zhaonan anymore.

117
00:12:43,842 --> 00:12:45,888
We lost many people last night.

118
00:12:46,532 --> 00:12:47,624
Everybody...

119
00:12:47,833 --> 00:12:50,727
has been waiting
for the seven swords to finish this.

120
00:12:51,898 --> 00:12:53,303
I don't care if it is a trap.

121
00:12:53,805 --> 00:12:57,032
We are going to fight
with Duo Geduo face to face.

122
00:12:57,276 --> 00:13:00,343
We cannot and should not
escape this battle.

123
00:13:03,015 --> 00:13:04,776
Let's do it!
Enough of this talk.

124
00:13:05,022 --> 00:13:05,545
Xin Longzi.
- Third Brother.

125
00:13:05,751 --> 00:13:07,116
You have forgotten your wine.

126
00:13:07,962 --> 00:13:08,855
Step aside!

127
00:13:09,789 --> 00:13:10,906
I want to see the chief.

128
00:13:16,228 --> 00:13:16,999
Chief...

129
00:13:17,380 --> 00:13:19,259
How long is this meeting going to last?

130
00:13:19,517 --> 00:13:20,693
Don't we have things to do?

131
00:13:22,735 --> 00:13:23,503
Brothers...

132
00:13:24,273 --> 00:13:25,304
You needn't worry.

133
00:13:26,339 --> 00:13:28,466
The seven swords
will finish this tonight.

134
00:13:40,462 --> 00:13:41,883
Did you kill the person following us?

135
00:13:43,697 --> 00:13:44,176
I did.

136
00:13:45,057 --> 00:13:46,057
Come on.
Follow me.

137
00:13:50,236 --> 00:13:53,437
The Dragon attacks well.
It will go here.

138
00:13:54,066 --> 00:13:56,466
The Transience is good at defence.
It should go here.

139
00:13:57,703 --> 00:13:59,926
Zhibang. Yuanyin.
You go at the sides.

140
00:14:00,306 --> 00:14:03,236
Mulang can move
to wherever he is needed most.

141
00:14:03,876 --> 00:14:06,344
Xin Longzi and I will be at the centre.

142
00:14:06,846 --> 00:14:11,777
With this formation,
we can attack and defend very well.

143
00:14:12,469 --> 00:14:14,909
I guess Duo Geduo will attack us...

144
00:14:15,621 --> 00:14:17,386
with many soldiers.

145
00:14:17,623 --> 00:14:21,241
As long as we stay united,

146
00:14:21,795 --> 00:14:23,196
we can beat anybody.

147
00:14:25,564 --> 00:14:26,326
In the past, Chu Zhaonan...

148
00:14:26,532 --> 00:14:28,425
has always stayed in the middle.

149
00:14:28,822 --> 00:14:29,936
Third Brother was always on the side.

150
00:14:30,910 --> 00:14:32,042
If we change the formation...

151
00:14:32,851 --> 00:14:33,884
Will he accept it?

152
00:14:35,675 --> 00:14:37,006
I have thought about it.

153
00:14:37,743 --> 00:14:40,812
Duo Geduo has always wanted
to control the Dragon Sword.

154
00:14:41,521 --> 00:14:43,715
Let it stay on the side.

155
00:14:44,024 --> 00:14:46,209
The soldiers will automatically
move toward it.

156
00:14:46,481 --> 00:14:49,492
Then our whole formation
will become more flexible.

157
00:14:49,789 --> 00:14:50,578
What do you think?

158
00:14:51,258 --> 00:14:52,683
This formation is sound.

159
00:14:53,384 --> 00:14:55,979
But what if Chu Zhaonan
really has surrendered to Duo Geduo?

160
00:14:58,197 --> 00:15:01,054
Then Duo Geduo
will make the best of tonight.

161
00:15:01,701 --> 00:15:05,663
Unless the seven of us are united
we cannot win.

162
00:15:06,272 --> 00:15:08,146
This is the only choice we have.

163
00:15:11,711 --> 00:15:16,351
And, Yang Yunchong still hasn't returned.
We must find him before tonight.

164
00:15:18,751 --> 00:15:19,454
Where is Yang Yunchong?

165
00:15:21,620 --> 00:15:22,644
If he wants his child back,

166
00:15:22,855 --> 00:15:24,446
tell him to meet me at Liuhe Tower.

167
00:15:25,491 --> 00:15:26,389
I know.

168
00:15:26,592 --> 00:15:27,854
He must have gone to Liuhe Tower.

169
00:15:28,060 --> 00:15:30,239
All right.
Let's prepare ourselves.

170
00:15:31,030 --> 00:15:31,952
Then we'll go to Liuhe Tower.

171
00:15:32,632 --> 00:15:34,975
Once we find Yang Yunchong
we'll go to Qiantang Jiang.

172
00:15:36,112 --> 00:15:38,724
Xin Longzi...
Make sure you have enough wine.

173
00:16:00,760 --> 00:16:02,022
Yufang.

174
00:16:02,661 --> 00:16:04,060
Senior Fu said...

175
00:16:04,463 --> 00:16:05,725
that your father was hit in the knees...

176
00:16:05,965 --> 00:16:07,626
by a hidden weapon.

177
00:16:08,467 --> 00:16:10,566
That's why he fell
onto First Brother's sword.

178
00:16:21,247 --> 00:16:21,971
Yufang...

179
00:16:24,817 --> 00:16:25,609
Yufang!

180
00:16:26,452 --> 00:16:28,147
Calm down... Calm down!

181
00:16:30,024 --> 00:16:30,757
Yufang...

182
00:16:32,144 --> 00:16:33,073
Calm down!

183
00:16:33,559 --> 00:16:34,317
Yufang.

184
00:16:38,336 --> 00:16:40,062
Don't make excuses for him.

185
00:16:40,966 --> 00:16:42,579
I saw it with my own eyes.

186
00:16:43,669 --> 00:16:44,658
Yufang.

187
00:16:52,278 --> 00:16:53,611
Take me to Qiantang Jiang.

188
00:16:56,949 --> 00:16:58,422
I must take revenge for my dad.

189
00:16:59,218 --> 00:16:59,915
No way...

190
00:17:00,352 --> 00:17:03,087
In this mission,
everyone has their own responsibilities.

191
00:17:03,489 --> 00:17:05,584
If you go,
who will take care of the children?

192
00:17:10,062 --> 00:17:10,994
Believe me.

193
00:17:12,431 --> 00:17:13,796
We will find out the truth.

194
00:17:14,166 --> 00:17:15,518
If First Brother is responsible,

195
00:17:16,936 --> 00:17:17,896
then I'll definitely...

196
00:17:18,510 --> 00:17:20,166
take revenge for you.

197
00:17:23,142 --> 00:17:24,609
Mulang.

198
00:17:26,827 --> 00:17:28,366
My dad is gone.

199
00:17:30,583 --> 00:17:32,876
He isn't with me anymore.

200
00:17:36,655 --> 00:17:38,954
I have nobody now.

201
00:17:45,793 --> 00:17:48,806
I have nobody left.

202
00:17:49,835 --> 00:17:52,130
No. That's not so.

203
00:17:52,538 --> 00:17:54,028
You have us.

204
00:17:56,408 --> 00:17:57,841
Dad.

205
00:17:59,478 --> 00:18:00,775
Dad.

206
00:18:06,085 --> 00:18:07,245
Zhibang.

207
00:18:27,306 --> 00:18:28,008
Yufang...

208
00:18:28,930 --> 00:18:30,407
I'm giving the belt to you now.

209
00:18:32,544 --> 00:18:33,704
Tonight,

210
00:18:39,184 --> 00:18:41,175
If I don't make it back,

211
00:18:43,722 --> 00:18:47,624
you will have to lead
the Red Spear Society.

212
00:18:59,171 --> 00:19:00,297
Zhibang.

213
00:19:29,501 --> 00:19:31,865
everyone has their own responsibilities.

214
00:19:33,338 --> 00:19:38,073
Your responsibility is to lead the society.

215
00:19:40,245 --> 00:19:41,542
Don't worry...

216
00:19:41,947 --> 00:19:44,515
I will take good care of the children.

217
00:19:45,017 --> 00:19:46,746
I'll wait for your return.

218
00:20:02,267 --> 00:20:03,495
Go now.

219
00:20:04,670 --> 00:20:05,703
Get some rest.

220
00:20:36,144 --> 00:20:37,097
There there.

221
00:20:39,173 --> 00:20:40,593
Be a good girl.

222
00:20:44,351 --> 00:20:45,649
Don't cry Baozhu.

223
00:20:46,217 --> 00:20:47,643
You'll wake your father.

224
00:20:54,974 --> 00:20:57,221
I wish you were my daughter.

225
00:22:01,770 --> 00:22:03,588
Don't cry. Don't cry.

226
00:22:05,090 --> 00:22:06,023
Don't cry Baozhu.

227
00:22:38,190 --> 00:22:39,018
Don't cry.

228
00:22:40,792 --> 00:22:41,805
Be good. Don't cry.

229
00:22:44,429 --> 00:22:45,521
Be good. Don't cry.

230
00:22:45,731 --> 00:22:47,198
Don't cry. Don't cry. Don't cry.

231
00:22:47,399 --> 00:22:48,832
Stop crying!

232
00:22:53,238 --> 00:22:54,398
I'm sorry.

233
00:22:55,827 --> 00:22:56,717
She's hungry.

234
00:22:57,984 --> 00:22:59,009
She needs feeding now.

235
00:23:20,133 --> 00:23:21,924
I have never thought of hurting her.

236
00:23:22,830 --> 00:23:24,190
But you just don't believe me.

237
00:23:31,828 --> 00:23:32,436
Fei Hongjin.

238
00:23:32,586 --> 00:23:33,651
Don't say anymore.

239
00:23:34,617 --> 00:23:35,569
I just want you to know...

240
00:23:36,444 --> 00:23:37,470
I won't force you.

241
00:24:02,674 --> 00:24:03,547
Take good care of her.

242
00:24:04,076 --> 00:24:04,900
Don't let anything happen to her.

243
00:24:05,777 --> 00:24:06,764
Her life is mine.

244
00:24:07,613 --> 00:24:08,710
If anything happens to her,

245
00:24:09,081 --> 00:24:10,009
I will hold you responsible.

246
00:24:13,478 --> 00:24:14,619
They are waiting for you.

247
00:24:23,975 --> 00:24:26,153
Yang Yunchong. Let's go!

248
00:24:35,874 --> 00:24:36,898
The Transience Sword is not here...

249
00:24:37,797 --> 00:24:38,747
You can use this for now.

250
00:24:45,854 --> 00:24:46,717
Thank you.

251
00:25:01,205 --> 00:25:01,890
Fei Hongjin...

252
00:25:02,993 --> 00:25:04,155
I'm in your debt.

253
00:25:05,737 --> 00:25:06,965
If I cannot pay you back in this life,

254
00:25:08,774 --> 00:25:10,241
I will pay you back in the next.

255
00:25:11,510 --> 00:25:12,631
I don't believe in reincarnation.

256
00:25:12,899 --> 00:25:14,611
You'll have to pay me back in this life.

257
00:25:15,547 --> 00:25:18,641
If you can't, then don't go.

258
00:25:31,663 --> 00:25:33,130
Take care of Baozhu.

259
00:26:14,822 --> 00:26:27,960
At midnight tonight, His Highness Duo Geduo
will await the seven swords at Qiantang Jiang.

260
00:27:18,370 --> 00:27:19,398
What do you think?

261
00:27:22,307 --> 00:27:23,674
Do you like tangerines?

262
00:27:26,178 --> 00:27:28,041
I always dream about this tree.

263
00:27:28,446 --> 00:27:29,936
But I cannot draw it very well.

264
00:27:47,933 --> 00:27:49,366
You draw better than me.

265
00:27:50,902 --> 00:27:52,164
You draw very well.

266
00:27:52,397 --> 00:27:54,798
But because you only used black,
the picture lacked something.

267
00:27:54,869 --> 00:27:56,870
Doesn't it look better now?

268
00:28:07,798 --> 00:28:08,847
What's wrong?

269
00:28:09,721 --> 00:28:10,826
I don't like it.

270
00:28:16,294 --> 00:28:18,417
Come on. Try again.

271
00:28:45,003 --> 00:28:45,606
Minghui.

272
00:29:11,691 --> 00:29:14,277
Come on...
That's it.

273
00:29:27,733 --> 00:29:28,399
Well done

274
00:29:31,351 --> 00:29:32,696
Do you want to rest?

275
00:29:41,513 --> 00:29:42,245
Your Highness.

276
00:29:46,117 --> 00:29:47,176
Father.

277
00:29:47,886 --> 00:29:49,844
Your Highness...
Please don't.

278
00:30:01,299 --> 00:30:02,892
The soldiers are ready.

279
00:30:03,234 --> 00:30:05,774
They await your command.

280
00:30:06,738 --> 00:30:08,171
What has been happening?

281
00:30:08,373 --> 00:30:10,824
Mr. Despicable has found the chief.
He is in the warehouse.

282
00:30:11,810 --> 00:30:13,377
I have sent a team of soldiers...

283
00:30:13,578 --> 00:30:14,978
to surround the warehouse.

284
00:30:16,707 --> 00:30:18,779
We will eradicate the Red Spear Society.

285
00:30:19,157 --> 00:30:20,250
Kill them all!

286
00:30:20,752 --> 00:30:21,503
Yes.

287
00:30:24,648 --> 00:30:25,776
Minghui is inside.

288
00:30:27,037 --> 00:30:28,292
Would you like to speak to her?

289
00:30:38,770 --> 00:30:39,964
Minghui.

290
00:30:47,348 --> 00:30:48,244
Minghui.

291
00:31:14,873 --> 00:31:15,691
No more killings...

292
00:31:16,530 --> 00:31:17,503
No more killings!

293
00:31:17,675 --> 00:31:18,383
Minghui!

294
00:31:20,044 --> 00:31:21,636
No more killings!

295
00:31:22,762 --> 00:31:23,279
Minghui.

296
00:31:23,328 --> 00:31:25,215
He brought a sword.
He is here to kill you.

297
00:31:25,247 --> 00:31:26,940
Go quickly. Go!
- Minghui.

298
00:31:29,653 --> 00:31:30,317
Minghui...

299
00:31:30,931 --> 00:31:31,586
Minghui!

300
00:31:41,150 --> 00:31:42,225
Yang Yunchong...

301
00:31:42,986 --> 00:31:44,331
You will die tonight!

302
00:31:45,860 --> 00:31:46,693
Minghui.

303
00:31:53,511 --> 00:31:54,604
You promised me...

304
00:31:55,013 --> 00:31:56,571
that you would stop the killings.

305
00:31:57,977 --> 00:31:59,313
Will you keep you promise?

306
00:32:02,108 --> 00:32:03,214
I will...

307
00:32:06,391 --> 00:32:09,588
after tonight, they will stop.

308
00:32:27,593 --> 00:32:28,674
What will you do?

309
00:32:29,033 --> 00:32:31,405
Kill Duo Geduo, or rescue Yang Yunchong?

310
00:32:31,583 --> 00:32:32,462
Kill Duo Geduo.

311
00:32:35,140 --> 00:32:36,073
Rescue Yang Yunchong!

312
00:32:43,312 --> 00:32:44,208
Leave me alone!

313
00:32:44,345 --> 00:32:45,928
I will decide my own destiny.

314
00:33:17,314 --> 00:33:18,293
You have come.

315
00:33:18,797 --> 00:33:22,324
Brothers,
our goal since leaving Mount Heaven,

316
00:33:22,534 --> 00:33:25,162
will succeed or fail tonight.

317
00:33:25,952 --> 00:33:29,000
Perhaps we should not
have left Mount Heaven at all.

318
00:33:29,774 --> 00:33:30,693
Second Brother...

319
00:33:31,504 --> 00:33:32,523
It's been a year already.

320
00:33:33,077 --> 00:33:36,436
Do you still remember the reason
we left Mount Heaven?

321
00:33:37,515 --> 00:33:38,365
To save people.

322
00:33:43,021 --> 00:33:43,887
Save people.

323
00:33:44,556 --> 00:33:46,649
That was just a pretence.

324
00:33:48,026 --> 00:33:49,152
Think about it.

325
00:33:49,727 --> 00:33:51,290
Since we left Mount Heaven,

326
00:33:51,496 --> 00:33:52,895
whom have we saved?

327
00:33:53,498 --> 00:33:54,726
No one.

328
00:33:55,934 --> 00:33:58,232
What we have done, is kill many people.

329
00:33:58,468 --> 00:34:00,545
We have killed and killed again.

330
00:34:01,348 --> 00:34:03,566
The Dragon Sword is drenched with blood.

331
00:34:05,343 --> 00:34:07,834
Your swords are blood stained too.

332
00:34:12,750 --> 00:34:15,712
Only Senior Fu's sword remains untainted.

333
00:34:16,521 --> 00:34:18,250
Tonight, we face a thousand soldiers,

334
00:34:18,857 --> 00:34:22,293
Will your sword...
spill blood tonight?

335
00:34:22,485 --> 00:34:25,055
Of course it will.
If Duo Geduo doesn't die,

336
00:34:25,607 --> 00:34:27,530
there will never be peace in this world.

337
00:34:27,873 --> 00:34:31,429
The dead will never be able to rest.

338
00:34:31,669 --> 00:34:33,899
As for my own sins,

339
00:34:34,138 --> 00:34:36,470
I may or may not find atonement,

340
00:34:36,674 --> 00:34:38,073
that depends on Heaven's will.

341
00:34:38,343 --> 00:34:38,992
Good!

342
00:34:39,670 --> 00:34:41,333
Swords are made to kill people.

343
00:34:41,846 --> 00:34:45,646
As swordsmen,
we shouldn't think too much.

344
00:34:46,217 --> 00:34:49,015
There is only one truth we need to know.

345
00:34:49,220 --> 00:34:52,048
A righteous sword,
always prevails.

346
00:34:52,407 --> 00:34:53,524
You're wrong.

347
00:34:54,381 --> 00:34:56,677
A sword is not good or evil.

348
00:34:57,528 --> 00:35:00,121
It depends on who wields it.

349
00:35:00,798 --> 00:35:03,232
I know you have suffered.

350
00:35:03,434 --> 00:35:06,603
The time will come
for you to address that suffering.

351
00:35:06,823 --> 00:35:08,317
But for tonight,

352
00:35:08,573 --> 00:35:12,123
the seven swords
must be united in order to win.

353
00:35:13,444 --> 00:35:14,365
Chu Zhaonan...

354
00:35:15,382 --> 00:35:17,171
If you are still a swordsman of Mount Heaven,

355
00:35:17,348 --> 00:35:18,748
then we'll go together.

356
00:35:19,390 --> 00:35:21,469
Let's defeat the soldiers tonight.

357
00:35:22,005 --> 00:35:23,159
And kill Duo Geduo.

358
00:35:37,035 --> 00:35:38,024
Teacher...

359
00:35:40,171 --> 00:35:41,386
Do you need to take a pee?

360
00:35:42,840 --> 00:35:44,705
All right. No need to wake up Teacher.

361
00:35:46,477 --> 00:35:47,671
Go on. Don't be scared.

362
00:36:29,247 --> 00:36:31,044
The soldiers are here. Hurry!

363
00:36:31,565 --> 00:36:32,745
Attack!

364
00:36:37,056 --> 00:36:37,931
Quick, hurry.

365
00:36:59,996 --> 00:37:01,845
Get the children out now!

366
00:37:53,805 --> 00:37:55,739
Come inside quickly. Hurry!

367
00:38:28,058 --> 00:38:29,637
Go on. Hurry!

368
00:38:54,866 --> 00:38:55,724
First Brother.

369
00:38:56,377 --> 00:38:58,966
Let's forget what has happened
this past year.

370
00:38:59,588 --> 00:39:01,015
Duo Geduo is right over there now.

371
00:39:01,540 --> 00:39:03,865
Come on. Let's take his head.

372
00:39:04,850 --> 00:39:05,536
All right...

373
00:39:06,067 --> 00:39:07,013
I'm here today...

374
00:39:07,583 --> 00:39:10,107
to cut Duo Geduo's head from his body.

375
00:39:10,616 --> 00:39:11,263
Good.

376
00:39:12,382 --> 00:39:13,224
Let's go!

