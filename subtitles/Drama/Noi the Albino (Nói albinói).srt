1
00:01:32,492 --> 00:01:37,020
Monday Tuesday Wednesday
Thursday Everyday

2
00:02:04,858 --> 00:02:07,759
<i>My name is Kenji.</i>

3
00:02:07,894 --> 00:02:12,024
<i>This could be me
three hours from now.</i>

4
00:02:27,313 --> 00:02:30,646
<i>Why do I want to kill myself?</i>

5
00:02:31,751 --> 00:02:33,514
<i>I don't know.</i>

6
00:02:40,627 --> 00:02:46,293
<i>I wouldn't kill myself for the same
reasons as other suicidal people.</i>

7
00:02:48,034 --> 00:02:50,002
<i>Money problems.</i>

8
00:02:50,136 --> 00:02:51,467
<i>Broken heart.</i>

9
00:02:51,604 --> 00:02:53,538
<i>Hopelessness.</i>

10
00:02:54,841 --> 00:02:56,672
<i>No, not me.</i>

11
00:02:58,545 --> 00:03:02,447
<i>Many books say death is relaxing.</i>

12
00:03:03,383 --> 00:03:05,351
<i>Did you know that?</i>

13
00:03:08,688 --> 00:03:11,953
<i>No need to follow the latest trends.</i>

14
00:03:12,091 --> 00:03:14,787
<i>No need to keep pace
with the rest of the world.</i>

15
00:03:14,928 --> 00:03:16,896
<i>No more e-mail.</i>

16
00:03:17,030 --> 00:03:19,157
<i>No more telephone.</i>

17
00:03:20,567 --> 00:03:22,330
<i>It will be like taking a nap.</i>

18
00:03:22,468 --> 00:03:25,528
<i>Before waking up refreshed
and ready to begin your next life.</i>

19
00:03:26,172 --> 00:03:28,163
<i>That's what they say.</i>

20
00:03:29,809 --> 00:03:33,370
This is bliss

21
00:04:59,465 --> 00:05:01,228
Surprise!

22
00:05:10,777 --> 00:05:11,869
Present.

23
00:05:12,011 --> 00:05:14,946
But don't open it. It's mine.

24
00:05:18,551 --> 00:05:21,577
Nice place. Big.

25
00:05:21,721 --> 00:05:23,746
Not bad.

26
00:05:24,157 --> 00:05:28,560
Sorry I didn't warn you I was coming.
I had a bit of an emergency.

27
00:05:29,962 --> 00:05:31,395
Got any ice?

28
00:05:32,598 --> 00:05:35,863
You don't have a TV?

29
00:05:36,002 --> 00:05:38,698
How can you live without a TV?

30
00:05:43,876 --> 00:05:46,868
You can't just read.
You'll go crazy.

31
00:05:47,013 --> 00:05:49,880
I can buy you a TV,
and DVD too.

32
00:05:50,016 --> 00:05:52,746
It's fucking hot!

33
00:05:54,020 --> 00:05:56,648
Is Bangkok traffic always this bad?

34
00:05:56,789 --> 00:05:59,349
Took me almost two hours
to get here.

35
00:05:59,492 --> 00:06:03,360
Can't believe you don't have a TV.

36
00:06:08,234 --> 00:06:10,099
Suicide again?

37
00:06:12,405 --> 00:06:14,498
Hanging yourself this time?

38
00:06:39,966 --> 00:06:42,594
It's too hot!

39
00:06:42,802 --> 00:06:44,861
I need a shower.

40
00:06:47,874 --> 00:06:50,399
This heat can kill you.

41
00:06:51,244 --> 00:06:54,111
Why bother with suicide?

42
00:08:27,039 --> 00:08:31,840
Black Lizard, by Yukio Mishima.

43
00:09:45,384 --> 00:09:48,080
"Out jogging.
Don't answer the phone."

44
00:09:48,454 --> 00:09:50,820
This is bliss

45
00:11:33,492 --> 00:11:34,459
Kenji.

46
00:11:34,593 --> 00:11:36,322
Tea is getting cold.

47
00:12:42,294 --> 00:12:44,922
Got to find a way
to go back to Japan.

48
00:12:45,131 --> 00:12:47,656
If not Japan, maybe Brazil.

49
00:12:48,134 --> 00:12:51,695
Japan is better.
Fuck overseas.

50
00:12:51,837 --> 00:12:54,567
But here you have a brother.

51
00:12:54,840 --> 00:12:58,367
Yeah, the crazy one.

52
00:12:58,611 --> 00:13:02,047
He reads too much.

53
00:13:02,648 --> 00:13:07,984
You can't go back to Japan.
The boss will kill you.

54
00:13:08,921 --> 00:13:11,947
But I've been with him a long time.

55
00:13:12,391 --> 00:13:15,189
He's just in a bad mood.

56
00:13:15,461 --> 00:13:17,656
A bad mood?

57
00:13:19,932 --> 00:13:24,369
You fucked his daughter!

58
00:13:27,807 --> 00:13:30,275
If you fucked my daughter,

59
00:13:30,409 --> 00:13:34,709
I'd cut your dick off
and stuff it in your mouth!

60
00:13:38,250 --> 00:13:39,979
Really?

61
00:13:40,352 --> 00:13:43,082
You've seen too many
Yakuza movies.

62
00:14:04,610 --> 00:14:08,341
Kenji, ready to go home?

63
00:14:08,480 --> 00:14:10,345
Yes.

64
00:14:18,457 --> 00:14:20,254
Kenji...

65
00:14:21,293 --> 00:14:26,629
This is the form for the public.

66
00:14:26,765 --> 00:14:29,825
This one is for staff.

67
00:14:29,969 --> 00:14:31,800
Sorry.

68
00:14:43,182 --> 00:14:45,116
Kenji.

69
00:14:46,118 --> 00:14:49,815
I'm making sushi at home tonight.
Are you free?

70
00:14:49,989 --> 00:14:52,753
I'm allergic to fish.

71
00:14:52,892 --> 00:14:56,225
Sorry.

72
00:14:58,764 --> 00:15:00,527
Nid?

73
00:15:05,471 --> 00:15:07,166
Have you seen Nid?

74
00:15:07,306 --> 00:15:09,297
She's somewhere here.

75
00:15:09,909 --> 00:15:11,240
Nid!

76
00:15:13,646 --> 00:15:15,273
Nid!

77
00:15:19,151 --> 00:15:20,584
What?

78
00:15:20,719 --> 00:15:22,619
Let's go home.

79
00:15:22,755 --> 00:15:24,518
I can't.

80
00:15:26,725 --> 00:15:28,590
Let's go home.

81
00:15:30,162 --> 00:15:32,630
My client's still here.

82
00:15:36,435 --> 00:15:38,198
I have to talk to you.

83
00:15:38,337 --> 00:15:40,237
About?

84
00:15:51,150 --> 00:15:56,315
The Last Lizard.

85
00:16:01,160 --> 00:16:03,151
Are you Japanese?

86
00:16:04,863 --> 00:16:07,923
Are you from Japan?

87
00:16:10,235 --> 00:16:12,533
Japan...

88
00:16:13,739 --> 00:16:16,572
Japan...

89
00:16:17,676 --> 00:16:22,238
Japanese... speak?

90
00:16:24,450 --> 00:16:26,111
Stop fucking with the radio.

91
00:16:30,823 --> 00:16:32,484
Nid, stop.

92
00:16:36,295 --> 00:16:37,557
Are you crazy?

93
00:16:37,696 --> 00:16:39,561
Take your headphones off.

94
00:16:42,034 --> 00:16:43,729
What the fuck?

95
00:16:44,670 --> 00:16:46,535
Did you fuck Jon?

96
00:16:47,306 --> 00:16:49,171
Who told you that?

97
00:16:49,508 --> 00:16:51,271
Did you fuck him?

98
00:16:55,114 --> 00:16:57,275
Jon told me everything today.

99
00:16:57,416 --> 00:17:00,908
What?
What did that dickhead tell you?

100
00:17:01,053 --> 00:17:02,645
He's a dickhead. He's shit!

101
00:17:02,788 --> 00:17:05,450
So why did you have to go fuck
a shit like that?

102
00:17:10,062 --> 00:17:13,395
You couldn't leave him alone
because he's my shit, right?

103
00:17:19,171 --> 00:17:25,804
Jon's such a dickhead. Don't know
why you care so much about him.

104
00:17:31,750 --> 00:17:34,048
I know...

105
00:17:35,788 --> 00:17:39,622
...Japanese.

106
00:17:41,960 --> 00:17:44,121
I...

107
00:17:45,898 --> 00:17:51,803
Japanese...

108
00:17:52,037 --> 00:17:55,473
Getting off here?

109
00:18:07,853 --> 00:18:11,050
Listen, Jon started it, not me.

110
00:18:11,190 --> 00:18:14,853
That's not the point.
The point is you're my sister.

111
00:18:18,063 --> 00:18:24,400
He's probably got his dick in some
other bitch's pussy right now.

112
00:18:26,772 --> 00:18:28,137
Get out.

113
00:18:31,643 --> 00:18:33,110
Out!

114
00:18:37,516 --> 00:18:44,445
The lizard wakes up and finds
he's the last lizard alive.

115
00:18:45,357 --> 00:18:49,088
His family and friends are all gone.

116
00:18:54,533 --> 00:18:56,364
Those he didn't like,

117
00:18:56,502 --> 00:19:00,734
those who picked on him in school,
are also gone.

118
00:19:01,874 --> 00:19:04,604
The lizard is all alone.

119
00:19:04,743 --> 00:19:10,204
He misses his family and friends.

120
00:19:13,152 --> 00:19:16,212
Even his enemies.

121
00:19:17,489 --> 00:19:21,357
It's better being with your enemies
than being alone.

122
00:19:22,728 --> 00:19:25,060
That's what he thought.

123
00:19:32,070 --> 00:19:36,666
Staring at the sunset, he thinks:

124
00:19:36,808 --> 00:19:44,476
"What is the point in living
if I don't have anyone to talk to?"

125
00:19:53,659 --> 00:19:56,787
But even that thought
doesn't mean anything

126
00:19:57,362 --> 00:20:00,388
when you're the last lizard.

127
00:20:48,747 --> 00:20:49,506
Nid!

128
00:21:16,708 --> 00:21:19,871
Your car is
in the parking lot out front.

129
00:22:49,835 --> 00:22:52,429
You know my crazy brother.

130
00:23:04,950 --> 00:23:07,248
Make yourself at home.

131
00:23:09,020 --> 00:23:13,548
We have a lot
to catch up on, Buddy.

132
00:23:13,692 --> 00:23:15,922
Ice, ice, ice!

133
00:23:25,437 --> 00:23:27,837
Is this a house or a library?

134
00:23:30,375 --> 00:23:34,038
Damn, no ashtray.

135
00:23:36,047 --> 00:23:38,311
Cool place.

136
00:23:39,384 --> 00:23:42,649
You moved here for girls?

137
00:23:42,788 --> 00:23:44,881
Or boys?

138
00:23:46,358 --> 00:23:48,849
Thanks.
I'll wait until it's cold.

139
00:27:46,598 --> 00:27:50,261
Kenji, aren't you going home?

140
00:28:30,909 --> 00:28:32,900
Kenji, are you coming?

141
00:28:33,044 --> 00:28:35,103
You go first.

142
00:34:22,427 --> 00:34:24,827
You can speak Thai?

143
00:34:27,432 --> 00:34:29,195
Want some pizza?

144
00:34:42,280 --> 00:34:45,340
I know.
I saw the name card in your bag.

145
00:35:43,207 --> 00:35:44,936
Thank you.

146
00:37:33,017 --> 00:37:34,575
What are you staring at?

147
00:38:14,592 --> 00:38:17,561
Are you washing dishes?
Don't.

148
00:38:20,865 --> 00:38:23,732
Do you want to go home?
I can drop you off.

149
00:38:52,697 --> 00:38:55,894
Don't burn my house down.

150
00:38:56,033 --> 00:38:57,660
Nothing.

151
00:40:13,778 --> 00:40:16,338
I am home.

152
00:42:51,235 --> 00:42:53,169
Who the fuck are you?

153
00:42:58,542 --> 00:43:02,273
What are you doing there?

154
00:43:02,413 --> 00:43:06,315
Get Noi on the phone!
Noi! Noi!

155
00:46:38,061 --> 00:46:39,926
What are you doing?

156
00:46:47,538 --> 00:46:49,506
How dare you come in here?

157
00:46:51,241 --> 00:46:53,004
Get out.

158
00:46:57,981 --> 00:47:00,211
Get the fuck out!

159
00:47:50,734 --> 00:47:54,226
Why did you leave me, Nid?

160
00:48:05,015 --> 00:48:09,714
I'm scared.
I don't want to be alone.

161
00:50:54,651 --> 00:50:59,054
Hey, did you see a Japanese guy?

162
00:51:20,777 --> 00:51:22,540
What are you doing?

163
00:51:27,651 --> 00:51:29,881
I bought you some sushi.

164
00:51:31,822 --> 00:51:33,517
Fucking expensive.

165
00:51:37,060 --> 00:51:39,961
Okay, I know you're sorry.

166
00:52:04,354 --> 00:52:05,981
Thank you.

167
00:52:11,561 --> 00:52:13,586
Good luck.

168
00:52:26,309 --> 00:52:28,243
I don't want...

169
00:52:31,882 --> 00:52:33,577
Thank you.

170
00:52:33,783 --> 00:52:36,217
I thought you didn't want any more.

171
00:52:48,832 --> 00:52:50,561
Too expensive.

172
00:52:56,973 --> 00:52:58,668
I'm full.

173
00:52:59,943 --> 00:53:02,036
Then stop eating.

174
00:53:02,412 --> 00:53:04,004
Never mind.

175
00:53:08,785 --> 00:53:10,616
No problem.

176
00:53:15,625 --> 00:53:21,063
One, two, three, four, five...five.

177
00:53:27,204 --> 00:53:29,434
You are very pretty.

178
00:53:37,047 --> 00:53:38,708
Thank you.

179
00:53:48,225 --> 00:53:52,719
I'm studying Japanese.

180
00:53:53,830 --> 00:53:55,491
Really?

181
00:53:55,632 --> 00:53:58,726
I'm going to Japan.

182
00:54:00,537 --> 00:54:02,061
Where in Japan?

183
00:54:02,205 --> 00:54:04,105
Osaka.

184
00:54:09,746 --> 00:54:11,304
When?

185
00:54:11,548 --> 00:54:14,949
Soon. Monday.

186
00:54:15,085 --> 00:54:16,916
This Monday?

187
00:54:58,161 --> 00:54:59,788
Really?

188
00:55:32,395 --> 00:55:34,363
Nothing.

189
00:55:34,531 --> 00:55:39,400
Just a friend. Friend, understand?

190
00:55:40,537 --> 00:55:43,131
No, don't come. I'm going out.

191
00:55:44,874 --> 00:55:48,310
Why do I have to tell you
everything? This is my life!

192
00:55:51,014 --> 00:55:54,142
If you want to talk shit,
don't call anymore!

193
00:56:12,135 --> 00:56:13,796
You want to stay, stay.

194
00:56:18,942 --> 00:56:20,603
Go to hell!

195
00:56:49,739 --> 00:56:52,333
1,375 baht.

196
00:57:01,818 --> 00:57:05,982
Why am I cleaning the house
when I'm leaving in a few days?

197
00:57:31,481 --> 00:57:32,971
I knew it.

198
00:57:33,116 --> 00:57:37,075
The bitch blew me off this morning.

199
00:57:53,903 --> 00:57:57,839
Now, go back down and blow.

200
00:58:14,090 --> 00:58:15,318
Watch your teeth!

201
00:59:58,027 --> 00:59:59,790
Did you fart?

202
01:00:04,601 --> 01:00:06,569
You fart?

203
01:00:06,703 --> 01:00:07,567
You fart?

204
01:00:07,704 --> 01:00:09,228
Not me!

205
01:00:11,341 --> 01:00:13,036
That hurts.

206
01:00:20,450 --> 01:00:22,008
Let's go home.

207
01:02:42,258 --> 01:02:43,555
Hey, Jap boy!

208
01:02:52,835 --> 01:02:54,462
Hey, Jap boy!

209
01:02:54,604 --> 01:02:56,629
Are you upstairs?

210
01:03:00,710 --> 01:03:02,337
Jap boy!

211
01:04:07,543 --> 01:04:09,477
Nid, I'm sorry.

212
01:04:32,835 --> 01:04:35,770
Thank you for cleaning the house.

213
01:04:41,711 --> 01:04:43,372
You're welcome.

214
01:04:43,512 --> 01:04:45,673
Tomorrow we'll do the laundry.

215
01:06:08,564 --> 01:06:11,124
That jerk!

216
01:08:12,988 --> 01:08:14,819
Shut up, old man!

217
01:09:05,741 --> 01:09:07,606
Hey, come sit here.

218
01:10:43,005 --> 01:10:44,768
what?

219
01:13:34,879 --> 01:13:35,675
Hey!

220
01:13:36,747 --> 01:13:39,113
You need a woman.

221
01:14:19,357 --> 01:14:20,654
What do you want?

222
01:14:20,791 --> 01:14:25,160
You had Japanese
for dinner last night?

223
01:14:27,365 --> 01:14:29,629
Are you fucking him right now?

224
01:14:29,767 --> 01:14:31,257
Fuck your mother!

225
01:14:31,402 --> 01:14:34,599
Come on, I miss you.

226
01:14:35,673 --> 01:14:37,334
I don't miss you.

227
01:14:38,843 --> 01:14:41,107
But what if I miss you?

228
01:14:41,612 --> 01:14:43,239
Go fuck yourself, then.

229
01:14:43,381 --> 01:14:46,214
Come on, don't be rude, darling.

230
01:14:49,086 --> 01:14:50,951
Go to hell, asshole.

231
01:14:56,660 --> 01:14:59,220
Let's go together then.

232
01:15:01,832 --> 01:15:03,891
I told you!

233
01:15:06,203 --> 01:15:08,034
Don't be rude!

234
01:15:19,417 --> 01:15:22,784
This will teach you some manners.

235
01:15:32,463 --> 01:15:34,693
So, this is your new fuck?

236
01:15:37,435 --> 01:15:39,869
You'll be sorry!
Fucking Jap!

237
01:16:59,283 --> 01:17:01,683
Bon App�tit.

238
01:18:35,112 --> 01:18:37,103
It doesn't look like you.

239
01:24:14,818 --> 01:24:16,615
Good-bye, Nid.

240
01:26:14,905 --> 01:26:16,497
You need it.

241
01:27:24,941 --> 01:27:26,465
When?

242
01:27:28,879 --> 01:27:29,971
One day.

243
01:27:51,735 --> 01:27:55,637
Kansai lnternational Airport, Osaka

244
01:28:02,879 --> 01:28:04,847
Just one bag, sir?

245
01:28:04,981 --> 01:28:05,845
Yeah.

246
01:28:06,616 --> 01:28:09,346
We are only going there
to kill someone.

247
01:28:09,486 --> 01:28:11,386
Then coming right back.

248
01:28:14,658 --> 01:28:19,960
Hope you're not hijacking the plane.

249
01:28:20,664 --> 01:28:24,657
Don't worry. We're not Arabs.

250
01:28:27,070 --> 01:28:29,664
Here are your boarding passes.

251
01:28:29,806 --> 01:28:33,503
Your gate number is 69.

252
01:28:33,643 --> 01:28:36,271
Have a pleasant flight, sir.

253
01:28:36,413 --> 01:28:40,577
Mr. Tajima, you're very funny.

254
01:28:41,318 --> 01:28:44,344
Hey, young lady,

255
01:28:45,121 --> 01:28:47,612
you have some seaweed
stuck in your teeth.

256
01:30:46,509 --> 01:30:48,340
Are you Kenji?

257
01:32:26,309 --> 01:32:28,300
Good day, sir.

258
01:32:29,045 --> 01:32:30,808
Good day.

259
01:33:23,266 --> 01:33:24,494
What the fuck?

260
01:34:07,243 --> 01:34:08,642
Are you Kenji?

261
01:34:51,054 --> 01:34:53,784
Get me a beer. I'm thirsty.

262
01:35:03,533 --> 01:35:05,057
Excuse me.

263
01:35:51,280 --> 01:35:52,941
Freeze!

264
01:35:57,320 --> 01:35:59,049
Kenji?

265
01:36:47,136 --> 01:36:50,594
This is bliss

266
01:39:15,051 --> 01:39:18,214
Noi, someone's here to see you.

267
01:40:10,515 --> 01:40:14,015
Separate subtitles extracted from
idx+sub by Officina verborum

