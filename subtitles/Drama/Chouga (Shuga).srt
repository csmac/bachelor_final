1
00:00:22,791 --> 00:00:27,808
Shuga
Based on "Anna Karenina" by Leo Tolstoy

2
00:00:35,599 --> 00:00:41,191
Written and directed by Darezhan Omirbaev

3
00:00:44,586 --> 00:00:47,396
Director of photography
Boris Troshev

4
00:00:50,094 --> 00:00:53,974
Sound
Olivier Dandret

5
00:00:56,112 --> 00:00:59,784
Production Design
Nuriya Dusembaeva Kaspakova

6
00:01:03,023 --> 00:01:07,329
Distribution
Ainur Turgambaeva

7
00:01:08,867 --> 00:01:11,691
Aidos Sagatov

8
00:01:12,811 --> 00:01:15,493
Ainur Sapargali

9
00:01:16,605 --> 00:01:19,359
Zhasulan Asauov

10
00:03:38,254 --> 00:03:40,710
What am I going to do with these days,
moving away from me as a stream? 

11
00:03:41,746 --> 00:03:44,413
What am I going to do with these nights,
these sleepless nights? 

12
00:03:45,438 --> 00:03:48,085
My heart overflows
with repentance. 

13
00:03:49,347 --> 00:03:51,797
But what am I going to do with my love,
my passionate love? 

14
00:03:52,782 --> 00:03:55,584
What am I going to do with myself,
if I am not close to you? 

15
00:03:56,616 --> 00:03:59,801
What will I do with my destiny,
so different from that of Majnun? 

16
00:04:00,714 --> 00:04:03,135
Oh, I don't care so much 
for this wasted life. 

17
00:04:04,000 --> 00:04:06,977
But what am I going to do with my love,
my passionate love? 

18
00:04:07,952 --> 00:04:10,669
What am I going to do with these days,
moving away from me as a stream? 

19
00:04:11,646 --> 00:04:14,262
What am I going to do with these nights,
these sleepless nights? 

20
00:04:15,679 --> 00:04:18,148
My heart overflows
with repentance. 

21
00:04:19,235 --> 00:04:22,068
But what am I going to do with my love,
my passionate love? 

22
00:06:31,043 --> 00:06:32,979
- Hello, Altynay!
- Hello!

23
00:06:33,268 --> 00:06:34,881
- Happy Birthday!
- Thank you!

24
00:06:35,061 --> 00:06:36,853
You haven't forgotten!

25
00:06:37,288 --> 00:06:38,177
Have you finished your classes?

26
00:06:38,212 --> 00:06:43,507
Because if you're finished...

27
00:06:48,228 --> 00:06:50,301
- Happy birthday, Altynay!
- Thank you!

28
00:06:50,536 --> 00:06:52,129
I wish you the best!

29
00:06:53,157 --> 00:06:55,976
Thanks, Ablay! Thanks for coming!

30
00:06:56,232 --> 00:06:59,708
Ablay, I present you Tolegen.
He came to congratulate me too.

31
00:07:00,343 --> 00:07:02,273
- Nice to meet you. Ablay.
- Tolegen.

32
00:07:03,241 --> 00:07:06,996
Tolegen works in films.
He's an apprentice operator.

33
00:07:07,319 --> 00:07:09,817
Great! When I was a kid, I was very interested in photography.

34
00:07:11,985 --> 00:07:13,834
But now I have no time...

35
00:07:16,724 --> 00:07:19,479
How about we go?
They're waiting.

36
00:07:20,027 --> 00:07:22,309
Okay, let me put on my coat.

37
00:07:25,669 --> 00:07:28,938
You come with us, Tolegen? It's close.

38
00:07:29,573 --> 00:07:32,561
No thanks. I can't.

39
00:07:34,081 --> 00:07:42,173
Pity. We could have talked about good movies ...
I'm so sick all this Hollywood.

40
00:07:47,834 --> 00:07:53,656
- You come with us, Tolegen?
- No, thanks, I can't.

41
00:07:54,011 --> 00:07:56,250
As you like. Let's go.

42
00:07:58,985 --> 00:08:01,655
Nice to meet you.

43
00:10:20,360 --> 00:10:24,034
Shuga, I called you,
but you were not home.

44
00:10:24,832 --> 00:10:30,601
You must come quickly to Almaty.
I'll explain everything when you get there.

45
00:10:32,037 --> 00:10:35,460
No, take the night train.

46
00:10:36,357 --> 00:10:38,744
You'll be late if you take something else.

47
00:11:08,587 --> 00:11:12,131
Thank you. I knew you'd understand.

48
00:12:00,184 --> 00:12:01,823
Hello, Ablay.

49
00:12:03,731 --> 00:12:04,944
Hello, Bakhyt.

50
00:12:05,575 --> 00:12:09,071
- What are you doing here?
- My mother is arriving from Astana. And you?

51
00:12:09,385 --> 00:12:12,476
My sister too.
Do you know my sister Shuga?

52
00:12:13,747 --> 00:12:17,573
Her husband is mister Izbassarov.
He's a member of Parliament.

53
00:12:19,604 --> 00:12:23,639
I'm not really interested in politics.
But I don't know your sister.

54
00:12:24,449 --> 00:12:27,385
You'll meet her today. She's a special woman.

55
00:12:29,141 --> 00:12:33,279
How was the party yesterday? 
Altynay came back home very happy.

56
00:12:33,861 --> 00:12:36,319
Yes, we went to the Soho. It was fun.

57
00:12:36,629 --> 00:12:39,361
I know the place. It's very nice.

58
00:12:42,287 --> 00:12:45,123
And did you go with Tolegen?

59
00:12:45,855 --> 00:12:51,012
We met him at the university, but he didn't join us, he's a bit strange...

60
00:12:51,605 --> 00:12:52,646
Yes, he really is.

61
00:12:53,781 --> 00:12:58,988
He's in love with Altynay, 
but honestly, she prefers you.

62
00:12:59,023 --> 00:13:00,461
Thank you, Bakhyt.

63
00:13:08,639 --> 00:13:10,491
Astana-Almaty

64
00:14:12,503 --> 00:14:15,735
Hello mum, welcome!

65
00:14:16,763 --> 00:14:20,244
Hello son! Thank God
you found me!

66
00:14:20,576 --> 00:14:24,591
- How was the trip?
- Very good, thanks.

67
00:14:50,552 --> 00:14:53,316
Your brother is not here?

68
00:14:54,805 --> 00:14:56,355
He's waiting outside.

69
00:14:57,666 --> 00:15:00,102
This young man is my son.

70
00:15:00,345 --> 00:15:02,798
I told you he was getting restless.

71
00:15:03,433 --> 00:15:04,294
Her name is Shuga.

72
00:15:04,729 --> 00:15:05,816
- Shuga.
- Ablay.

73
00:15:06,451 --> 00:15:10,954
You probably know her brother,
Bakhyt Aishanov. He works at Alem Bank.

74
00:15:11,355 --> 00:15:12,755
Yes, I know Bakhyt.

75
00:15:14,569 --> 00:15:17,699
Thanks to your mother, I know all about you.

76
00:15:18,285 --> 00:15:20,245
Which means you must be sick of me.

77
00:15:21,242 --> 00:15:28,425
He's growing, nothing happens,
it is impossible to ever be separated.

78
00:15:28,921 --> 00:15:30,731
Shuga has a 7 year-old son in Astana.

79
00:15:31,314 --> 00:15:34,236
It's the first time that
she leaves him alone, can you believe that? 

80
00:15:35,721 --> 00:15:38,559
- Well, I have to go. Bye!
- Bye!

81
00:15:40,478 --> 00:15:42,520
- Bye!
- Bye!

82
00:15:46,734 --> 00:15:49,702
A very interesting woman.

83
00:15:50,615 --> 00:15:52,824
There are not many like her nowadays.

84
00:15:53,809 --> 00:15:58,516
It's a shame that she's married.
Otherwise she'd be a good match for you.

85
00:16:20,737 --> 00:16:22,835
Have you known Ablay for a long time?

86
00:16:23,699 --> 00:16:27,197
I have, he is the youngest 
son of mister Baitassov, a millionaire.

87
00:16:28,546 --> 00:16:30,068
They are very rich.

88
00:16:30,891 --> 00:16:34,526
They own banks, hotels,
oil fields...

89
00:16:34,893 --> 00:16:38,923
But Ablay is humble and
nice... a good person.

90
00:16:41,691 --> 00:16:43,167
And what does he do?

91
00:16:44,085 --> 00:16:46,636
He has a business in Astana. 
That's all I know.

92
00:16:49,609 --> 00:16:51,947
Hopefully he will marry our Altynay.

93
00:16:57,666 --> 00:16:59,610
Really?

94
00:17:17,196 --> 00:17:20,473
But let's talk about you.
What happened to you?

95
00:17:20,508 --> 00:17:23,686
Another fight with Togzhan?

96
00:17:24,721 --> 00:17:26,877
Yes, you're my only hope.

97
00:17:29,512 --> 00:17:32,534
Everything will be fine.

98
00:17:53,417 --> 00:17:55,273
Aunt Shuga!

99
00:19:27,529 --> 00:19:32,476
It's over... I can't go on living with him.

100
00:19:32,836 --> 00:19:36,140
And I cannot leave him. The kids ...

101
00:19:37,488 --> 00:19:40,477
His mere presence makes me sick.

102
00:19:50,309 --> 00:19:51,321
I don't say that to comfort you,

103
00:19:52,056 --> 00:19:53,413
nor to excuse him.

104
00:19:55,537 --> 00:19:58,422
But there's one thing I'm sure of.

105
00:19:58,457 --> 00:19:59,872
Bakhyt is one of those men

106
00:20:00,507 --> 00:20:03,881
for whom family and
kids are sacred.

107
00:20:06,216 --> 00:20:10,054
As for the rest ... It's something else.

108
00:20:11,689 --> 00:20:15,424
These are seperate worlds.

109
00:20:16,551 --> 00:20:21,603
I don't understand how this can be, 
but that's how it is.

110
00:20:29,719 --> 00:20:31,841
But he sleeps with her!

111
00:20:46,356 --> 00:20:48,497
And you? you'd forgive him?

112
00:20:49,899 --> 00:20:51,045
I don't know...

113
00:20:56,145 --> 00:20:58,903
Actually, if I know it, I'd forgive him.

114
00:21:00,999 --> 00:21:05,039
It wouldn't be the same, 
but I would forgive.

115
00:21:06,701 --> 00:21:08,337
And I'd forgive completely,

116
00:21:09,373 --> 00:21:11,553
as if nothing had happened.

117
00:21:13,682 --> 00:21:17,500
I would... Otherwise it wouldn't be true forgiveness.

118
00:21:36,121 --> 00:21:37,086
What are we doing sitting here?

119
00:21:37,121 --> 00:21:40,281
I'm going to make some tea.

120
00:21:53,540 --> 00:21:59,046
"Is Sex in the Marriage Possible ?" 

121
00:22:40,504 --> 00:22:44,146
Has Amir grown?

122
00:22:44,181 --> 00:22:47,172
Yes, he's had a growth spurt this year.

123
00:22:47,207 --> 00:22:48,824
You wouldn't recognise him!

124
00:22:48,859 --> 00:22:55,414
Let me show you
some recent photos.

125
00:23:09,709 --> 00:23:11,751
- Won't you come in?
- No, thank you, next time.

126
00:23:12,217 --> 00:23:20,557
Wait, I'll get you the CD.

127
00:23:52,082 --> 00:23:53,752
This is in summer, in Great Britain.

128
00:24:08,121 --> 00:24:09,512
- Take it.
- Thank you.

129
00:24:10,013 --> 00:24:11,513
- See you tomorrow.
- Okay, Bye.

130
00:29:02,550 --> 00:29:06,017
Tell dad and mum that I'm moving to the student dorm.

131
00:30:25,639 --> 00:30:28,566
Levin: Stiva, Why?
don't you want to tell me? 

132
00:30:28,866 --> 00:30:31,291
Levin: How is Kitty Scherbatskaya? 

133
00:30:32,126 --> 00:30:34,810
When will she get married? 
Or is she already?

134
00:31:19,084 --> 00:31:21,263
Shu station. 20-minute stop.

135
00:32:21,547 --> 00:32:23,083
A bottle of sparkling water, please.

136
00:32:37,433 --> 00:32:39,102
Ablay?

137
00:32:44,667 --> 00:32:48,012
I didn't know you were travelling too.

138
00:32:49,413 --> 00:32:53,528
Are you going to Astana by car? 
By night? Why?

139
00:32:55,617 --> 00:32:57,852
Why ?...

140
00:32:58,779 --> 00:33:04,745
I'm travelling to be where you are. 
I can't live without you. 

141
00:35:09,802 --> 00:35:13,998
Mister Bakhytkhoja would like to see you, but he had a very important meeting. 

142
00:35:14,102 --> 00:35:16,092
That's why he sent me.

143
00:35:56,208 --> 00:35:57,018
Mum !

144
00:39:28,802 --> 00:39:30,812
Hi.

145
00:39:33,539 --> 00:39:37,426
- Welcome! What's up in Almaty?
- Did Max give you the money?

146
00:39:37,911 --> 00:39:40,046
Yes. He had 200,000.

147
00:39:41,568 --> 00:39:42,663
If he had not paid...

148
00:39:43,136 --> 00:39:44,942
We would have blown up his fucking head.

149
00:39:47,758 --> 00:39:51,579
Would you like some tea?

150
00:39:52,457 --> 00:39:56,953
What have you been doing to my flat, pig? 
You could have cleaned up a bit.

151
00:39:58,866 --> 00:40:00,001
Who's there?

152
00:40:01,102 --> 00:40:04,003
I met her in a bar.
I don't remember her name.

153
00:40:04,655 --> 00:40:07,225
You like her... ?

154
00:40:07,560 --> 00:40:10,195
Stupid...

155
00:40:37,608 --> 00:40:45,109
Everything will be fine, madam.
We all go through the same at this age.

156
00:40:46,270 --> 00:40:50,306
But the truth is that not everybody tries to commit suicide.

157
00:41:04,699 --> 00:41:13,410
The best remedy for Altynay
is time. Time heals everything.

158
00:41:42,666 --> 00:41:46,083
Yesterday they called me from Almaty.

159
00:41:46,474 --> 00:41:51,892
- Altynay is very sick.
- Really? How sad...

160
00:41:52,927 --> 00:41:57,040
- It doesn't seem to worry you that much. 
- Should it? Is it serious?

161
00:42:04,985 --> 00:42:07,189
You behaved badly with her, Ablay.

162
00:42:08,090 --> 00:42:12,290
I know. But you should understand why I did it. 

163
00:43:08,619 --> 00:43:11,671
Ablay, this is very serious for me.

164
00:43:11,706 --> 00:43:17,669
What you told me affects me
much more than you imagine.

165
00:49:58,510 --> 00:50:06,028
It's over. From now on, 
I only have you. Remember that.

166
00:50:11,089 --> 00:50:19,185
I can't even remember my own life...

167
00:50:40,238 --> 00:50:44,538
Astrology gives an answer
to the enigma of love.

168
00:50:44,539 --> 00:50:47,439
It is a conjunction of characters.

169
00:50:47,957 --> 00:50:49,683
And what influences people's character?

170
00:50:50,184 --> 00:50:52,384
First, their birth date

171
00:50:52,401 --> 00:50:53,801
Their date of birth, you understand?

172
00:50:54,236 --> 00:50:56,762
Don't forget it.

173
00:50:58,975 --> 00:51:02,661
And which Zodiac sign matches mine the best?

174
00:51:02,838 --> 00:51:06,554
Sagittarius, of course! ? 
What is your girlfriend's sign?

175
00:51:11,499 --> 00:51:15,463
Just that, Sagittarius.
But she's not my girlfriend.

176
00:51:16,499 --> 00:51:21,172
Tolegen! You're a man, right?
Why don't you try to see her again?

177
00:51:21,607 --> 00:51:25,645
A Kazakh proverb that says: "You cannot
avoid what is meant for you"

178
00:51:26,330 --> 00:51:29,147
Women are human beings just like us.

179
00:51:30,732 --> 00:51:32,410
They also make mistakes.

180
00:51:34,362 --> 00:51:37,282
The main thing, Tolegen, is to not give up on a dream.

181
00:51:43,071 --> 00:51:47,190
For example, a girl and a boy
don't know each other in this life.

182
00:51:47,721 --> 00:51:51,197
But in a previous life they were
like the two wheels of a bicycle,

183
00:51:51,598 --> 00:51:53,198
or mother and son,

184
00:51:53,463 --> 00:51:56,170
or the two halves of an apple,

185
00:51:56,805 --> 00:52:03,459
and if they meet in this life,
they recognize each other and instantly fall in love.

186
00:52:03,994 --> 00:52:11,402
It's called reincarnation. You got it?
I have to go, bye.

187
00:52:44,404 --> 00:52:45,658
Altynay...

188
00:54:19,743 --> 00:54:26,056
Altusha, my dear!
Congratulations for your degree!

189
00:54:28,328 --> 00:54:31,983
What could you deserve on such
an important day?

190
00:54:33,609 --> 00:54:38,815
"You know? Now that
I have a profession,

191
00:54:39,616 --> 00:54:45,817
I wish that you find your soul mate.

192
00:54:46,315 --> 00:54:52,730
Not a crackpot
like me, someone serious.

193
00:54:53,472 --> 00:54:58,994
Someone who loves you and always takes care of you.
Let's cheer to that.

194
00:54:59,495 --> 00:55:01,496
Thank you.

195
00:55:11,409 --> 00:55:15,511
- Congratulations!
- Thank you. Thanks for coming.

196
00:55:16,582 --> 00:55:20,618
- I saw you on the bus.
- Really? When?

197
00:55:22,923 --> 00:55:27,047
A month ago. You were back from skating in Medeu.

198
00:55:27,282 --> 00:55:30,009
Ah, yes. I went skating
with some friends.

199
00:55:31,444 --> 00:55:32,992
More tea, Tolegen?

200
00:58:13,266 --> 00:58:15,375
I heard that Altynay married
a guy who works in films.

201
00:58:15,790 --> 00:58:19,350
Thank God. I'm sure 
she'll have many children.

202
00:58:20,579 --> 00:58:22,170
We too will have many children, right?

203
00:58:22,671 --> 00:58:26,072
Well of course. And they'll be conceived in Paris.

204
00:58:27,206 --> 00:58:30,382
I love you.

205
00:59:26,827 --> 00:59:31,223
By the way, do you know Shuga,
Bakhyt's sister?

206
00:59:31,758 --> 00:59:39,529
Because she's abandoned her family, her son,
and fled abroad with her lover.

207
00:59:39,964 --> 00:59:45,403
Oh my God, before, those things 
only happened in films...

208
00:59:46,495 --> 00:59:51,719
Indeed, but true love makes people do such things.

209
00:59:52,986 --> 00:59:58,258
Love is love, but
leave your family, your son?

210
00:59:58,559 --> 01:00:00,259
She's a wife, a mother!

211
01:01:30,171 --> 01:01:31,154
Right?

212
01:01:35,763 --> 01:01:38,659
Sure, sweetie,
I'll be there in 20 minutes.

213
01:02:38,435 --> 01:02:42,184
You can't imagine how much I suffer waiting for you.

214
01:02:44,664 --> 01:02:49,518
When you're with me, I confide in you.

215
01:02:50,953 --> 01:02:52,093
But when you're somewhere else,

216
01:02:52,528 --> 01:02:55,492
I lead a life completely
unknown to me...

217
01:06:47,763 --> 01:06:49,869
Mum!

218
01:08:22,733 --> 01:08:24,807
Mum  ? Leave me alone!

219
01:08:25,342 --> 01:08:29,739
I am an adult, I don't need you to tell me
how to behave in my private life.

220
01:08:30,174 --> 01:08:33,242
And please tell dad to calm down.

221
01:08:37,244 --> 01:08:38,044
Okay.

222
01:08:44,317 --> 01:08:46,737
Okay ... let's talk.

223
01:09:12,705 --> 01:09:14,721
Shuga! ? What is it this time?

224
01:09:24,481 --> 01:09:32,204
Come on, I'll show you a funny thing
that some friends made for me.

225
01:10:03,514 --> 01:10:05,899
Can you believe it? They have found
the morons

226
01:10:06,200 --> 01:10:09,900
who attacked me on New Year's Eve.
You remember? Look.

227
01:13:44,150 --> 01:13:45,092
I'm waiting.

228
01:14:03,006 --> 01:14:04,378
I pass!

229
01:14:10,103 --> 01:14:12,142
One.

230
01:14:39,359 --> 01:14:45,841
- Ablay?
- Yes?

231
01:14:45,876 --> 01:14:54,527
What if we have to pay for everything in life ? 
Even for love ? How can we ?

232
01:14:54,562 --> 01:14:57,071
I don't know.

233
01:15:44,604 --> 01:15:46,143
Shuga?

234
01:16:54,115 --> 01:16:58,144
They say that animals never
kill themselves. Only people do.

235
01:16:59,279 --> 01:17:08,506
Indeed. People are intelligent enough 
to avoid more suffering.

236
01:17:42,265 --> 01:17:44,823
We're here. Last stop.

237
01:20:58,261 --> 01:21:03,652
Really? Good.

238
01:21:05,013 --> 01:21:06,777
Listen to this, Tolegen.

239
01:21:06,812 --> 01:21:11,821
We Kazakhs have a strong influence
from shamanism in our culture.

240
01:21:12,178 --> 01:21:16,259
For example, superstition: never raise your hand 
looking at the moon, things like that.

241
01:21:16,294 --> 01:21:19,504
Some things comfort us 
some don't.

242
01:21:19,739 --> 01:21:21,275
I believe in dreams.

243
01:21:21,850 --> 01:21:25,379
When you dream
everything lacks consistency.

244
01:21:25,714 --> 01:21:30,846
But if you tell your dream to someone
it's like it remains fixed.

245
01:21:30,881 --> 01:21:35,577
Whenever I have a dream,
I tell my mother ...

246
01:22:17,675 --> 01:22:22,889
- Shyntas!
- Yes, it's me.

247
01:22:27,551 --> 01:22:30,482
- Is Mrs Altynay Shyntas,
from room 112, your wife?

248
01:22:30,887 --> 01:22:32,487
Yes

249
01:22:32,994 --> 01:22:35,316
Congratulations! You're a father!

250
01:22:36,251 --> 01:22:39,685
Congratulations!

251
01:22:49,547 --> 01:22:52,606
What's the date today? November 20th?

252
01:22:52,641 --> 01:22:55,505
So, you child is a scorpion

253
01:22:56,040 --> 01:22:57,550
It's the best.

254
01:22:57,785 --> 01:22:59,719
The sign of Dostoevski.

255
01:23:01,354 --> 01:23:09,586
Tolegen... has some relative 
or a friend or yours died recently?

256
01:23:09,621 --> 01:23:12,363
No, Why?

257
01:23:12,698 --> 01:23:20,713
According to astrologists, when a scorpion is born, 
someone else within the same family dies.

258
01:23:22,048 --> 01:23:28,469
Or vice versa: when a scorpion dies,
a birth occurs.

259
01:23:30,504 --> 01:23:37,227
Come on now. Don't say foolish things.

260
01:23:49,930 --> 01:23:51,349
Ok?

261
01:24:02,297 --> 01:24:04,621
Okay.

262
01:24:18,529 --> 01:24:21,971
You knew Shuga... Bakhyt's sister?

263
01:24:23,006 --> 01:24:25,655
No. What happened ?

264
01:24:26,790 --> 01:24:29,313
She died yesterday.

265
01:24:30,548 --> 01:24:33,903
She jumped under a train in Astana.

266
01:24:36,138 --> 01:24:40,682
I saw her once... a beautiful woman.

267
01:24:42,617 --> 01:24:44,798
I told you! And you didn't believe me.

268
01:24:47,268 --> 01:24:48,500
Come on, let's drink!

269
01:24:48,835 --> 01:24:52,527
I toast to your child's health, may he become a true man.

270
01:24:53,062 --> 01:24:55,540
May he have an easy life.

271
01:25:23,799 --> 01:25:26,131
Tolegen, the era of
Pisces finished in 2003.

272
01:25:26,432 --> 01:25:28,332
A new era is has begun.

273
01:25:29,466 --> 01:25:35,336
Your child was born in this new era...
