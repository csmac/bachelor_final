{3858}{3916}Mrs. Peterson !
{3918}{3974}Ramon, hey !|How's it goin' ?
{3976}{4032}How you doin' ?
{4035}{4083}There it is,|right there on the desk.
{4085}{4164}- Where's she headed ?|- She's snowbound, Ramon.
{4258}{4327}- See you went with the pink.|- Yeah, yeah.
{4329}{4380}It's kind of|a pink day today.
{4383}{4451}I'm gonna have another pickup|for you on Thursday.
{4453}{4505}- All right. We'll see you then.|- All right. Thanks, Ramon.
{4508}{4568}Bless my soul|What's wrong with me
{4571}{4647}I'm itchin' like a man|on a fuzzy tree
{4649}{4719}My friends say I'm actin'|wild as a bug
{4721}{4767}I'm in love
{5350}{5413}- Peterson ! Peterson !
{5446}{5496}- Peterson.
{5558}{5622}- Mr. Cowboy, da ?|- Da.
{5730}{5803}Mmm, it's pretty.
{5805}{5879}Mmm.|It's from my wife.
{7474}{7521}Time...
{7524}{7579}- rules over us without mercy,
{7581}{7671}not caring if we're healthy or ill,|hungry or drunk,
{7673}{7760}Russian, American,|beings from Mars.
{7763}{7832}It's like a fire. It could either|destroy us or keep us warm.
{7834}{7898}That's why every FedEx office|has a clock.
{7900}{7968}Because we live or we die|by the clock.
{7970}{8016}We never turn our back|on it.
{8018}{8067}And we never, ever|allow ourselves...
{8069}{8148}the sin of losing track|of time !
{8150}{8199}Locally, it's 1:56.
{8202}{8294}That means we've got|three hours and four minutes...
{8296}{8342}before the end-of-the-day's|package sort.
{8344}{8448}- That's how long we have.|That's how much time we have...
{8450}{8520}before this pulsating, accursed,|relentless taskmaster...
{8522}{8584}tries to put us|out of business.
{8587}{8660}Hey, Nicolai. Hey.|Nicolai, good to see you.
{8663}{8707}How are you, kid ?|Look what you did.
{8709}{8759}You just delivered|your very first FedEx package.
{8762}{8847}That deserves something special,|like a Snickers bar...
{8850}{8918}and a C.D. player.
{8920}{8966}And something to listen to-|a C.D. There.
{8968}{9035}Elvis Presley.|Fifty million fans can't be wrong.
{9037}{9083}You all recognize this,|don't you ?
{9085}{9150}I took the liberty|of sending this to myself.
{9152}{9271}I FedEx'd it|before I left Memphis.
{9273}{9324}You're probably wondering|what could be in here.
{9327}{9382}What could it possibly be ?|ls it architectural plans ?
{9384}{9430}Maybe technical drawings ?
{9432}{9498}Is this the new wallpaper|for the- for the bathroom ?
{9548}{9614}It is... a clock,
{9616}{9662}which I started|at absolute zero...
{9664}{9783}and is now at 87 hours,|22 minutes and 17 seconds.
{9785}{9902}From Memphis, America|to Nicolai in Russia, 87 hours.
{9904}{9981}Eighty-seven hours|is a shameful outrage.
{9984}{10056}This is just an egg timer !
{10059}{10124}What if it had been something else ?|Like your paycheck ?
{10127}{10201}- Or fresh boysenberries ?|Or adoption papers ?
{10204}{10252}Eighty-seven hours|is an eternity.
{10255}{10301}The cosmos was created|in less time !
{10304}{10370}Wars have been fought and|nations toppled in 87 hours !
{10372}{10418}Fortunes made and squandered.
{10520}{10565}What ?
{10568}{10613}What are you saying|about me ?
{10616}{10661}I tell them,|what do they expect ?
{10664}{10752}This man, when his truck broke down,|he stole a boy's bicycle|to do his delivery.
{10755}{10819}I borrowed it !|I borrowed a kid's bike.
{10821}{10897}And I got my packages delivered,|and that is what you people|are gonna have to start doing.
{10900}{10983}You have to start doing|whatever it takes, because|in three hours and two minutes...
{10986}{11060}every one of these packages|has to be on the big truck...
{11063}{11108}and on its way|to the airport.
{11111}{11171}- Fifteen minutes !
{11173}{11252}- Crunch time ! Crunch time !|- Crunch time !
{11255}{11332}Let's go ! Every package|on the airport truck.
{11334}{11383}Go !|Crunch, crunch, crunch !
{11386}{11432}- We have a big problem.|- What ?
{11434}{11499}- The truck in Red Square is stuck.|- What do you mean ?
{11501}{11563}- It's stuck.|- Stuck how ? ln snow ? ln ice ?
{11566}{11649}It's stuck ! The most important truck-|the Kremlin truck ! Many packages.
{11890}{11946}All right, all right, all right.|Let's put the table right here.
{11948}{11994}We'll just do the sort.
{11996}{12071}Ah, yeah, a clamp.|That would make us stuck.
{12074}{12135}Let's go. Unload-|Get him out of there.
{12138}{12199}Right here.|Two lines, two lines.
{12201}{12270}One to the airport truck,|one to the Moscow truck.
{12272}{12311}Got it ?
{12314}{12399}C.D.G., F.E., M.E,|Memphis on the airport truck.|Everything else right there.
{12402}{12466}Nicolai ! Tick-tock, tick-tock.|Four minutes.
{12522}{12584}Hi, this is Kelly.|If you're calling for Chuck, press one.
{12587}{12638}Otherwise leave a message|after the tone. Thanks.
{12640}{12688}- Hello, Kelly. Are you there ?
{12691}{12750}Pick up, pick up, pick up,|pick up. You're not there.
{12752}{12824}You're not gonna believe this. I'm doin'|the sorts in the middle of Red Square...
{12826}{12872}in the shadow|of Lenin's tomb.
{12874}{12920}I miss you
{12922}{12971}I miss you|I really wanna kiss you
{12973}{13041}I'm outta here in two minutes and|I'm pickin' up the sweep through Paris,
{13044}{13105}so I should be back in Memphis|about 1 8 hours or so.
{13108}{13158}That's the good news.|The bad news is...
{13160}{13206}I gotta go to|the dentist this week.
{13208}{13255}I got something in there|that's hurtin'.
{13257}{13324}I love you and I'm gonna see you soon,|and you know what that means.
{13326}{13389}Bye-bye. Nicolai ! Tick-tock,|tick-tock, tick-tock, tick-tock !
{13812}{13906}- I absolutely, positively|have to get to Memphis tonight.
{13908}{13957}Can't help you.|Try U.P.S.
{13960}{14023}With this extra head wind, are we gonna|be able to push it and make the sort ?
{14025}{14082}- We'll do our best.|- "Our best," huh ?
{14084}{14186}Gwen, is there something wrong|with our doctors that Jack|keeps getting certified to fly ?
{14188}{14244}- Aren't you concerned ?|- I'm terrified.
{14247}{14293}But a girl's gotta do|what a girl's gotta do.
{14296}{14364}That's the spirit.|Relentless is our goal. Relentless !
{14367}{14442}What do you expect from the guy|who stole a crippled kid's bicycle...
{14444}{14500}- when his truck broke down ?|- I borrowed it.
{14502}{14553}But I love that|the kid's now crippled.
{14556}{14615}So you missed the last truck|by two minutes ?
{14617}{14686}Two minutes.|Actually, it was less than that.
{14688}{14758}The plane wasn't that heavy.|You could've added some fuel,
{14760}{14806}picked up the speed|and made up the time.
{14808}{14883}It's about the trucks.|Today's truck was two minutes late.
{14886}{14958}Tomorrow's will be four minutes late,|and then six minutes|and eight minutes late.
{14960}{15055}- Next thing you know,|we're the U.S. Mail.|- Yeah, well-
{15058}{15118}All I'm sayin' is, if you'd|gotten all those trucks on the plane,
{15120}{15186}those Russkies would be|walking on water right now.
{15188}{15247}Hey, don't-|Don't gimme that look.
{15249}{15308}- It's just grape juice. Right, Chuck ?|- Yeah.
{15311}{15384}It's like a 1992|full-bodied grape juice.
{15387}{15439}I see no evil, hear no evil,|speak no evil.
{15442}{15522}We'd offer you some but, you know,|somebody's got to fly the plane.
{15524}{15607}Yeah, well,|I "just say no," right ?
{15609}{15676}Listen, Stan,|I've been meaning to ask you.
{15678}{15727}How's Mary ?
{15729}{15808}Oh. Uh, well,
{15810}{15884}- we really don't know anything yet.|- Mm-hmm.
{15887}{16003}Uh, she went to|the doctor yesterday, and, uh,
{16005}{16096}it hasn't meta-|metastasized...
{16099}{16169}as far as they|can tell right now.
{16172}{16241}It's just kind of|wait and see.
{16244}{16295}I'm so sorry.
{16298}{16375}Tell her I'm gonna come by|and see her on my next layover.
{16377}{16456}I want you to know, Stan,|we are all thinking about her and you.
{16459}{16523}- And just blessings.|- Thank you, Gwen.
{16525}{16587}Hi, this is Kelly.|If you're calling for Chuck, press one.
{16590}{16660}Otherwise, leave a message|after the tone. Thanks.
{16786}{16851}Stan.
{16853}{16912}Listen.|Uh, I heard about this-
{16955}{17031}I heard about this doctor|down in Emory, in Atlanta.
{17033}{17079}Uh, he's supposed to be|the absolute best.
{17081}{17131}He worked on|Frank Toretta's wife.
{17133}{17179}Frank Toretta is|a systems analyst.
{17181}{17240}He played center field for us.|You know him.
{17242}{17306}In the softball tournament|last Labor Day.
{17308}{17352}Anyway, beside the point.
{17355}{17463}Uh, what I was thinking is,|is that I could get his number-
{17466}{17528}the doctor-|and, uh,
{17531}{17589}you know,|put you all together and-
{17626}{17684}You know, you could|get this thing fixed.
{17687}{17732}You could beat it.
{17764}{17811}Thanks, Chuck.
{17813}{17872}So, I'll get his number.
{18409}{18503}- Is Kelly Frears around ?|- She's copying her dissertation.
{19259}{19315}- You're home.|- Home indeed.
{19317}{19363}I love that you're home.
{19795}{19918}Just days before Santa departs|on his annual gift-giving venture,
{19920}{19990}he's been declared|physically fit to fly.
{19992}{20084}Santa got checked out by doctors at|the V.A. Hospital in Augusta, Georgia.
{20086}{20154}After they declared|the jolly old elfin tiptop shape,
{20156}{20240}Santa Claus took off into the wild|blue yonder for an early start.
{20243}{20302}In Sarajevo,|he spent the day with children,
{20304}{20372}handing out presents and|even helping them decorate...
{20375}{20420}the special Christmas tree.
{20520}{20577}Santa and a sea of elves|took over the floor...
{20580}{20625}at the Hong Kong|Futures Exchange.
{20628}{20684}Market traders|bought Santa hats, donating-
{21225}{21272}The turkey's a little dry,|isn't it ?
{21275}{21334}- No.|- The turkey is perfect.
{21427}{21504}- How many did you do last night ?|- Last night ? 2.9 million.
{21507}{21591}Now you've got to be in the market|for more of these candied yams.
{21594}{21628}Here you go.
{21630}{21677}2.9 has got to be|the record.
{21680}{21725}Look at those marshmallows.
{21728}{21775}When I was there,|we did two million.
{21777}{21837}- We thought that was a big deal.|- The glory days.
{21840}{21900}- What'd they do the first night ?|- The very first night ?
{21903}{21948}- Twelve.|- Twelve thousand?
{21951}{22014}- Hell, no. Twelve.|- That's right.
{22016}{22082}And they did the sort|right there on a card table.
{22084}{22149}And Fred Smith had|that card table bronzed.
{22152}{22216}I've been hearing that|for five years.
{22219}{22318}- It's in his office today.|- They go from that to|the new hub up in Anchorage.
{22320}{22366}It's state of the art-|a perfect marriage...
{22368}{22427}between technology|and systems management.
{22429}{22495}Speaking of marriage, Chuck,|when are you gonna make|an honest woman out of Kelly ?
{22553}{22621}- How long ?|- Fourteen minutes into the meal.
{22624}{22701}- You won. I owe you $5.|- So I win.
{22704}{22751}- Way before the pie.|- I told him on the way up.
{22753}{22828}Right about the time|the pie came out, the marriage-
{22831}{22902}Kelly is still recovering from her|failed relationship with that parolee.
{22904}{22956}He was a lawyer.
{22959}{23012}I knew it had something to do|with raising your right hand...
{23015}{23060}and swearing|to tell the truth.
{23063}{23136}Remember when he fell on the sidewalk|and broke his shoulder|at the wedding reception ?
{23139}{23204}He tripped on a curb|holding a door open for me.
{23207}{23286}- Aunt Kelly was married before ?|- It's not even worth remembering.
{23288}{23357}So since her failed marriage|to the parolee-
{23360}{23479}- Oh, my God.|- Kelly can't see being with a man|who wears a pager to bed.
{23481}{23528}What're you gonna|attach it to, Hoss ?
{23531}{23599}When you wear it to bed, I hope|you got one of those things...
{23601}{23645}that go vibratin'.
{23648}{23705}Watch it.|I'm not touching that.
{23708}{23788}Oh, did you|hit an olive pit ?
{23791}{23892}- I thought I took all of them out.|- No, no, it's not that.
{24524}{24578}- Okay, I'll cancel Saturday.|- No, don't.
{24580}{24626}If I'm not here, I'm not.|But if I am, well,
{24628}{24674}- then I am.|- It's cancelled.
{24676}{24722}But you gotta be here|New Year's Eve.
{24724}{24782}Malaysia can't be that bad.|I'll be here New Year's Eve.
{24784}{24843}- When are you defending|your dissertation ?|-January 1 2.
{24845}{24951}I'm gonna have to|switch over the South America thing.
{24953}{25009}If I do that|on the third or fourth,
{25012}{25057}I'm gonna have to head|back out on the 13th.
{25060}{25112}So long as you're here|New Year's Eve.
{25115}{25185}I will be here New Year's Eve.|I promise.
{25265}{25369}- What ?|- What about our Christmas ?|I got a gift for you.
{25435}{25495}We have to do it|in the car.
{25497}{25619}You'll be doing all right
{25622}{25695}I'd just like a minute over here,|all right ? Thanks.
{25749}{25852}But I'll have a blue
{25855}{25910}-Blue, blue, blue Christmas|- Two minutes, okay ?
{25912}{25973}Two more minutes.|Thanks. Hey !
{25976}{26034}- Merry Christmas.|- I thought you were gonna stiff me.
{26036}{26103}- No.|- What is the ribbon ?|ls it a thousand-pound...
{26105}{26185}- test fishing line or something ?|- Here's your fancy thing.
{26188}{26252}Thanks.
{26322}{26374}I love the wrapping.
{26413}{26460}- And I love the box.|- Good.
{26526}{26623}Oh. Oh, my.
{26651}{26696}Oh, that is terrific.
{26699}{26757}My granddaddy used it|on the Southern Pacific.
{26823}{26918}Hey, I took this. This is|my favorite picture of you.
{26957}{27027}You know what I'm gonna do ?|I'm always gonna keep this...
{27029}{27093}on Memphis time.
{27163}{27208}Kelly time.
{27211}{27259}Hmm.
{27261}{27308}You haven't said anything about|your presents. Is there a problem ?
{27311}{27360}I'm sorry, no !|I love 'em.
{27363}{27415}Look at my journal.|Ah !
{27417}{27507}- What about the pager ?|- Real leather.|The pages have gold on 'em.
{27509}{27558}- Did you like the pager or not ?|- Yes.
{27560}{27650}I love it. Look at him in|his little blue Russian house.
{27652}{27721}- It's from Russia.|- My God.
{27724}{27806}- It's not one of those loud ones ?|- No. You can program it|so it vibrates...
{27808}{27870}and lights up|and see it in the dark.
{27872}{27969}It seems like|a real nice pager.
{27972}{28059}- Sorry about the hand towels.|You're hard to shop for.|- No, no. I love 'em.
{28061}{28158}- Every time I wash my hands,|I will think of you.|- All right, I gotta go.
{28160}{28254}- Merry Christmas.|- Merry Christmas.
{28256}{28302}- I love you.|- I love you too.
{28484}{28550}Chuck ! Keys !
{28788}{28863}- Thank you.|- That reminds me. I almost forgot.
{28865}{28947}I have one more present|for you.
{28949}{29072}Only this isn't an "open in the car"|kind of present...
{29075}{29120}Iike, say, hand towels.
{29123}{29180}Which were a joke,|by the way.
{29283}{29343}I'm terrified.
{29345}{29419}Just take it and hold onto it,|and you can open it...
{29421}{29484}on New Year's Eve.
{29487}{29532}And I love you.
{29624}{29690}I'll be right back.
{30744}{30804}Hey, Al, where are we ?
{30807}{30853}Somewhere over|the Pacific Ocean.
{30856}{30916}Ha, ha, ha !|You pilots, you're funny.
{30919}{30973}Tahiti Control,|FedEx 88.
{30976}{31043}Position Jenna|at 1 526-
{31045}{31116}Is all this turbulence from Santa|and those eight tiny reindeer ?
{31119}{31179}Tamara at 1620.
{31182}{31236}Erick is next.
{31239}{31301}- Fuel: niner-five-decimal-five.
{31304}{31357}Blaine, tell them we're|deviating south for weather.
{31360}{31416}Make another position plot|on your deviation left.
{31418}{31476}Tahiti Control, FedEx 88.
{31479}{31542}Position Jenna at 1526.
{31544}{31601}- Flight level 350.|- I got us plotted.
{31604}{31673}We're 200 miles south|of our original course.
{31676}{31740}Continue plotting,|and check contingency procedures.
{31743}{31789}Tahiti Control, FedEx 88.
{31792}{31849}Broadcasting in the blind.|How do you read ?
{31852}{31909}- l've never|been out of comm this long.
{31912}{31984}Did you try the higher|H.F. frequency ?
{31987}{32045}Tahiti Control, FedEx 88.
{32048}{32117}Better buckle up, Chuck.|It's gonna get bumpy.
{32120}{32187}Tahiti Control,|FedEx 88.
{32189}{32239}Do you read?
{32444}{32492}Tahiti Control,|FedEx 88.
{32494}{32564}Position Jenna at 1526.
{32567}{32615}Flightlevel350.
{32617}{32663}Expecting Tamara at 1-
{33651}{33723}Fire ! Explosion!
{33726}{33772}Let's get a hose!
{33822}{33914}Hold on to it !|Come on !
{33968}{34018}Seat belt !
{34169}{34237}We may have to ditch !
{34467}{34515}Okay ?
{34534}{34573}Fire in the hold!
{34576}{34623}We're goin' down !
{34625}{34681}Mayday ! Mayday !
{34935}{34990}Bringing it down and out!
{34992}{35064}Ten thousand feet !|Masks off !
{35067}{35121}Masks off !
{35176}{35249}Chuck ! Life vest!
{36016}{36084}Chuck !|Chuck, stay there !
{36087}{36155}Where's your life vest ?
{36724}{36769}Ditching switch on !
{37108}{37172}I've got visual !
{37272}{37329}Brace for impact!
{50092}{50144}Hello !
{52576}{52634}Hello !
{52636}{52682}Anybody !
{52961}{53009}Anybody !
{53118}{53183}Help !
{53186}{53232}Help !
{56276}{56321}What is that ?
{56400}{56456}Hey !
{56736}{56781}Hey!
{56784}{56844}Anybody ?
{62877}{62927}Hello ?
{68529}{68607}Get ! Go, go.|Go ! Go !
{69996}{70067}"Albert R. Miller."
{70069}{70145}Not Alan. Albert.
{72077}{72131}So, that's it.
{76351}{76399}Hey ! A ship !
{76401}{76455}Hey ! Hey !
{76457}{76503}Hey ! Hey !
{76505}{76562}A ship ! Over here !
{76564}{76642}Hey ! Hey !
{76644}{76711}Over here ! Over here !
{76844}{76890}Over here !
{76892}{76978}Over here !|Over here !
{76980}{77042}Help me ! Help !|Right here ! Right here !
{77044}{77090}Help ! Look here !|Look, look, look!
{77092}{77211}S... O... S !|Please !
{77213}{77282}Help ! Come on!
{77284}{77324}Help !
{90309}{90355}" Happy birthday."
{90404}{90503}"The most beautiful thing in the world|is, of course, the world itself.
{90505}{90603}Johnny, have the happiest|birthday ever. Score. Your Grandpa."
{93415}{93460}Ew.
{95116}{95168}Come on.
{95171}{95231}Come on. Come on.
{95233}{95330}Come on. Come on.
{101824}{101897}You wouldn't have a match...|by any chance, would you ?
{102002}{102048}Oh, oh, oh !
{102414}{102466}The air got to it.
{102469}{102514}The air got to it !
{103538}{103619}Come on, come on.|Come on.
{103742}{103790}I did it.|I did it.
{103891}{103939}Fire !
{103942}{103992}There you go!
{103994}{104042}Light it up !|Come on !
{104045}{104104}The time to hesitate|is through
{104107}{104162}Ouch !
{104165}{104218}Ouch !
{104221}{104312}No time to wallow|in the mire
{104314}{104390}Try now, we can only lose
{104393}{104461}'Cause girl|we couldn't get much higher
{104464}{104559}Come on, baby|light my fire
{104561}{104633}Here you go !|Here you go !
{104635}{104689}It's a signal fire !
{104719}{104797}And it spells out|S.O.S !
{104799}{104843}Whoa !
{104873}{104927}It's a meteor shower !
{104930}{104975}Fireflies !|Go ! Run !
{104978}{105046}You're free ! You're free !|Ouch ! Ouch !
{105049}{105094}Yeah !
{105097}{105151}Yes !
{105154}{105212}Look what I have created !
{105214}{105265}I have made fire !
{105268}{105348}I... have made fire !
{105636}{105681}Mmm !
{105849}{105894}You gotta love crab.
{105948}{105993}In the nick of time too.
{106043}{106113}I couldn't take much more|of those coconuts.
{106116}{106165}Coconut milk's|a natural laxative.
{106216}{106285}Things that Gilligan|never told us.
{106287}{106333}Oh.
{106621}{106674}Pretty well-made fire,|huh, Wilson ?
{106867}{106947}So... Wilson.
{106950}{107002}We were en route...
{107005}{107102}from Memphis|for 11 and a half-hours.
{107105}{107200}About 475 miles an hour.
{107202}{107297}So they think that|we are right here.
{107299}{107345}But...
{107347}{107420}we went out|of radio contact...
{107423}{107527}and flew around that storm|for about an hour.
{107530}{107597}So that's a distance|of what, 400 miles ?
{107640}{107685}Four hundred miles squared.
{107688}{107781}That's 1 60,000...
{107784}{107872}times pi- 3.1 4.
{107941}{108049}Five hundred and two thousand,|four-
{108052}{108145}That's a search area|of 500,000 square miles.
{108207}{108257}That's twice the size|of Texas.
{108318}{108363}They may never find us.
{108966}{109032}This tooth is just killing me.
{109034}{109093}It started out just hurting|when I bit down,
{109096}{109182}but now it just hurts|all the time.
{109185}{109249}All the time.
{109251}{109315}It's-lt's a good thing there's|not much to eat around here,
{109317}{109368}because I don't think|I could chew it.
{109408}{109497}Just keep sucking on all|that coconut and all that crab.
{109557}{109603}And just think,
{109605}{109664}I used to avoid|going to the dentist...
{109666}{109741}Iike the plague.
{109744}{109830}I put it off every|single chance I got. But now,
{109833}{109926}oh, what I wouldn't give...
{109929}{110003}to have a-a dentist|right here in this cave.
{110137}{110185}In fact,|I wish you were a dentist.
{110325}{110370}Yeah.
{110373}{110418}Dr. Wilson.
{110668}{110713}You wanna hear|something funny ?
{110753}{110825}Back home in Memphis,
{110828}{110921}my dentist's name is|Dr. James Spaulding.
{111407}{111466}She's much prettier|in real life.
{117658}{117703}Shut up !
{119263}{119309}" Bakersfield" ?
{119414}{119477}Bakersfield !
{120842}{120888}This could work.
{120925}{120970}This could work.
{121788}{121872}Twenty-two-|Forty-four lashings.
{121921}{121967}Forty-four lashings.|So-
{122027}{122073}We have to make rope again.
{122171}{122224}Wilson, we're gonna have to make|a hell of a lot of rope.
{122378}{122442}Eight lashings|of these structural.
{122445}{122514}That's 24 apiece.
{122517}{122598}That'll be 100- 160.
{122601}{122652}Here we are today.
{122655}{122765}That gives us another month and a half|until we're into March and April,
{122768}{122846}which is our best chances for|the high tides and the offshore breezes.
{122886}{123013}We need-|We need 424 feet...
{123016}{123100}of good rope, plus another 50 feet,|say, for miscellaneous.
{123103}{123208}Round that off to|475 feet of good rope.
{123210}{123309}Now, if we average|1 5 feet a day-
{123312}{123414}Plus, we have to build it,
{123417}{123505}we have to stock it,|we have to launch it.
{123565}{123619}That's gonna be tight.
{123621}{123667}That is not much time.
{123701}{123747}But we-
{123749}{123812}We live and we die by time,|don't we ?
{123865}{123958}Now, let's not commit the sin|of turning our back on time.
{124084}{124139}I know.
{124141}{124212}I know.
{124678}{124722}This is it.
{124783}{124855}That's all that's left.
{124857}{124931}I checked over the whole island,|and that is all that's left.
{124933}{124979}So we're gonna be short.
{125059}{125105}Short.
{125273}{125322}We'll just have to make some more|out of the videotape.
{125367}{125418}Yes.|No, we have time.
{125421}{125467}We do. We have time.|Look !
{125469}{125521}The wind's still blowing in|from the west.
{125681}{125740}I know !
{125742}{125816}Yeah, I know- I know where|there's 30 feet of extra rope !
{125819}{125898}But- But I'm not|going back up there.
{127632}{127704}There, there.|There, you see ? Huh ?
{127707}{127768}There.|Are you happy ?
{127936}{128023}Do you have to keep bringing that up ?|Can't you just forget it ?
{128025}{128081}Huh ? You were right.
{128124}{128169}You were right.|It was a good thing that we did a test,
{128172}{128254}'cause it wasn't gonna be|just a quick little snap.
{128257}{128330}I would have landed|on the rocks.
{128333}{128445}Broken my leg or my back or my neck.|Bled to death.
{128448}{128513}But it was the only option I had|at the time though, okay ?
{128515}{128576}It was what, a year ago ?
{128578}{128624}So let's just forget it.
{128734}{128780}And what is your point ?
{128825}{128885}Well...
{128888}{128933}we might just make it.
{128936}{128982}Did that thought|ever cross your brain ?
{129024}{129116}Well, regardless, I would rather take|my chance out there on the ocean...
{129118}{129189}than to stay here and die|on this shithole island,
{129192}{129318}spending the rest of my life|talking to a goddamn volleyball !
{129444}{129489}Shut you up.
{129649}{129739}Wilson ! Wilson !
{129898}{129951}Wilson!
{129980}{130034}Wilson !
{130037}{130082}Wilson !
{130139}{130194}Wilson.
{130243}{130289}Wilson !
{130321}{130428}Oh, God ! Wilson !
{130430}{130476}Wilson !
{130610}{130656}Oh ! Oh !
{130658}{130735}Never again.|Never again, never again.
{130737}{130812}You're okay.|You okay ? Yeah.
{131266}{131318}Yeah, I know you.
{131321}{131378}I know you.|I know you.
{131497}{131575}So, we okay ?
{131609}{131659}Okay ?
{131712}{131760}Okay.
{134094}{134140}You still awake ?
{134205}{134251}Me too.
{134435}{134481}You scared ?
{134558}{134604}Me too.
{136467}{136540}Okay.|Here we go, Wilson.
{136542}{136618}You don't have to worry|about anything.
{136621}{136672}I'll do all the paddling.
{136675}{136721}You just hang on.
{137819}{137900}Not yet !|Hold on !
{138159}{138234}Not yet !|Not yet !
{138266}{138314}Not yet !
{138403}{138476}Stand by, Wilson !
{138478}{138545}Hold on !|Hold on, Wilson !
{138961}{139036}I think we did it !
{139074}{139139}I think we did it !
{139141}{139187}Wilson !
{139242}{139288}I think we did it !
{144373}{144456}Oh ! They're gone !
{144581}{144627}I don't know why !
{146623}{146669}Where's Wilson ?
{146716}{146785}Where's Wilson ?
{146787}{146833}Wilson, where are you ?
{146873}{146918}Wilson !
{147002}{147048}Wilson !
{147050}{147104}Wilson!
{147757}{147803}Wilson !
{147805}{147863}I'm comin'!
{147907}{147953}Wilson !
{148443}{148492}Wilson !
{148495}{148553}Wilson-
{148665}{148728}Wilson !
{148867}{148930}Wilson ! Wilson !
{149218}{149274}Wilson !
{149359}{149405}Wilson!
{149407}{149462}I'm sorry !
{149504}{149552}I'm sorry, Wilson!
{149554}{149615}Wilson, I'm sorry !
{149645}{149701}I'm sorry!
{149704}{149755}Wilson!
{149757}{149807}I can't!
{149809}{149855}Wilson !
{149889}{149944}Wilson !
{150847}{150909}I'm- I'm sorry.
{150981}{151027}I'm sorry.
{151072}{151142}I'm sorry.
{154042}{154088}Kelly.
{154219}{154265}Kelly.
{154300}{154345}Kelly.
{154483}{154556}Hello ?|Oh, how are you ?
{154604}{154649}Okay.
{155640}{155709}Forty-five minutes.
{155711}{155776}One Dr. Pepper.
{155778}{155830}Two cups of ice.
{155833}{155878}I like ice.
{155947}{155989}Well, here's the drill.
{155992}{156062}Um, plane pulls in,|we get off,
{156065}{156130}and there's a little ceremony|right there in the hangar.
{156163}{156219}Fred Smith|will say a few words.
{156221}{156304}All you have to do is smile|and say "thank you."
{156340}{156388}Then we'll take you over|to see Kelly.
{156452}{156497}She's actually gonna|be there, huh ?
{156532}{156586}Well, that's what|we have arranged.
{156589}{156630}I mean, if you're sure|you wanna do that.
{156633}{156682}Oh, yeah, yeah.|Yes.
{156784}{156835}I don't know what|I'm gonna say to her.
{156873}{156924}What in the world|am I gonna say to her ?
{157021}{157099}Chuck, Kelly had|to let you go.
{157101}{157153}You know?
{157156}{157221}She thought you were dead.
{157224}{157272}And we buried you.
{157316}{157411}We had a funeral and a coffin|and a gravestone.
{157413}{157460}The whole thing.
{157490}{157536}You had a coffin ?
{157602}{157648}What was in it ?
{157679}{157745}Well, everybody put|something in.
{157747}{157861}You know, just a cell phone|or a beeper, some pictures.
{157863}{157912}I put in some Elvis CDs.
{157984}{158029}So you had my funeral...
{158089}{158151}and then you had|Mary's funeral.
{158268}{158354}Stan, I'm so sorry|I wasn't around when Mary died.
{158413}{158504}I should've been there for you,|and I wasn't.
{158585}{158656}I'm so sorry.
{158658}{158717}Four years ago,
{158720}{158816}the FedEx family lost|five of our sons.
{158818}{158892}That was a terrible|and tragic day.
{158918}{158993}But today,|one of those sons,
{158996}{159088}Chuck Noland,|has been returned to us.
{159090}{159141}Chuck, welcome home.
{159368}{159469}Just moments ago, Fred Smith|welcomed home Chuck Noland.
{159471}{159552}- How about it, Michelle ?|- It's such an incredible|and amazing story...
{159554}{159642}- to comeback from the dead.|- Well, it's so amazing that|Smith himself welcomed him back.
{159645}{159731}And Fred acknowledged that,|"While time waits for no man,
{159733}{159803}we take time to pause|to honor one of our own. "
{159805}{159859}Now, that's an expensive pause.
{160052}{160100}I'm-l'm sorry.|I must be in the wrong place.
{160103}{160169}No, you're in|the right place.
{160172}{160275}You probably don't remember me.|I actually did root canal on you|about five years ago.
{160277}{160328}Jim Spaulding referred you.
{160376}{160421}Oh, yeah, yeah.
{160540}{160585}I'm Kelly's husband.
{160625}{160685}Jerry Lovett.
{160688}{160760}Kelly wanted-
{160763}{160809}Kelly wanted to be here-
{160872}{160965}Look, this is very hard|for everyone.
{160967}{161028}I can't even imagine|how hard it is for you.
{161145}{161190}Kelly, uh-
{161254}{161300}She's had it rough.
{161302}{161366}First when she thought|she lost you,
{161369}{161414}and now dealing|with all of this.
{161417}{161497}It's-lt's confusing.|It's very emotional for her.
{161499}{161545}She's-
{161586}{161669}She's... sort of lost.
{161729}{161781}Maybe you could just|give her a little more time.
{161919}{161972}Anyway, uh,
{161974}{162020}I'm sorry that-
{163397}{163442}Okay, people, let's go.
{163445}{163499}Party's over. Let's go home.|Let's go home.
{163501}{163562}You're definitely|gonna have to go down to the DMV.
{163565}{163625}Okay, Maynard, let's go.|Chuck's had a big day.
{163628}{163675}- I have to brief this man|on his meetings tomorrow...|- I know.
{163677}{163723}with the accountants|and the attorneys, all right ?
{163725}{163785}I will see you|bright and early in the morning.
{163787}{163889}- Hey, Chuck, welcome home. We love you.|- Becca, I need to speak to you.
{163892}{163978}You know, Chuck, we gotta catch up|on some of that fishin', now.
{163981}{164026}Take care.
{164089}{164148}- Good night.|- Good night.
{164223}{164281}You got everything|you need ? Okay.
{164284}{164329}Well, if you need anything,|just sign for it,
{164332}{164385}and I'll see you|in the morning.
{164388}{164454}Get some sleep, okay ?|We got another big day tomorrow.
{164457}{164515}It takes a lot of paperwork|to bring back a man.
{164555}{164633}- Bring you back to life, man.|- Bye, Chuck.
{164636}{164688}- Take care, now.|- Tomorrow.
{164691}{164744}Tomorrow we're gonna|bring you back to life.
{167258}{167304}How long are you|gonna be, man ?
{167368}{167430}Well...
{167433}{167507}- how long will that buy me ?|- About ten minutes.
{168088}{168136}I'm awake.
{168138}{168187}I saw your taxi drive up.
{168249}{168294}Get in here|out of the rain.
{168465}{168517}I saw you|down at the hub today,
{168520}{168574}so I know you were down there.
{169015}{169061}Let me get you a towel.
{169348}{169393}They're sleeping.
{169484}{169536}If you come in,|I'll make you some coffee.
{169861}{169906}It's a nice house.
{169909}{169954}Yeah, we got|a nice mortgage too.
{170141}{170205}What's your daughter's name ?
{170208}{170268}Katie.
{170271}{170317}Katie.
{170397}{170471}- She's a beautiful little girl.|- She's a handful.
{170814}{170860}Just let me get|one thing straight here.
{171099}{171177}We have a pro football team now,|but they're in Nashville ?
{171310}{171432}Um, yeah. Oh, my God. Okay.|They used to be in Houston.
{171435}{171534}First they were the Oilers,|and now they're the Titans.
{171537}{171600}The Houston Oilers|are the Tennessee Titans ?
{171602}{171648}Yeah.
{171650}{171696}But that's not all.
{171698}{171769}They went to the Super Bowl|last year.
{171772}{171833}- And I missed that.|- You would've died. It was so exciting.
{171836}{171920}They almost won by one yard.|One lousy yard right at the end.
{171978}{172057}I got whole milk,|2% and nonfat.
{172059}{172113}I don't have any half-and-half,|and that's what you like.
{172144}{172189}That'll do fine.
{172504}{172556}What happened to you|becoming a professor ?
{172605}{172696}You're not|Dr. Kelly Frears-Lovett ?
{172773}{172831}When you, um-
{172833}{172901}When your plane went down,
{172904}{172956}everything just sort of|got put on hold.
{173026}{173085}I think about|taking it up again, though.
{173377}{173423}I came out here|to give you this.
{173465}{173510}Oh, my God.
{173561}{173606}I'm sorry it doesn't work.
{173645}{173711}And l, uh,|I kept the picture.
{173742}{173789}It was all faded anyway.
{173824}{173890}I want you to have it.|I gave it to you.
{173893}{173965}That's a family heirloom,|and it should stay in your family.
{174208}{174273}That's everything I have|from when you went down to now.
{174327}{174387}They said they never figured out|what caused the crash.
{174389}{174475}Probably some mislabeled|hazardous material caught fire.
{174580}{174635}So here's where|that ship found you.
{174667}{174743}You drifted about 500 miles.
{174745}{174798}This is where|your island was,
{174801}{174880}about 600 miles|south of the Cook lslands.
{174920}{174972}And these are|the search grids.
{174974}{175043}Ships went back and forth|for weeks looking for you.
{175261}{175310}I never should've|gotten on that plane.
{175367}{175426}I never should've|gotten out of the car.
{175519}{175565}I wanna show you something.
{175616}{175661}Come here.
{175893}{175939}This is-|This is our car.
{175997}{176042}You kept our car.
{176096}{176173}All right, now,|this is weird.
{176377}{176428}It's a good car.
{176431}{176486}Had a lot of memories|in this car.
{176489}{176551}Two very nice memories.
{176553}{176602}Oh, that trip|down to the Gulf, yeah.
{176665}{176710}So, can I drive it ?
{176713}{176758}It's your car.
{176944}{177019}That's good, 'cause my-|my taxi's gone.
{177166}{177222}Um-
{177225}{177270}Let me get some stuff|out of the back.
{177810}{177868}You think you're gonna|have more kids ?
{177870}{177916}I don't know.
{177918}{177986}It's kind of confusing|right now.
{177989}{178053}Well, you should.
{178056}{178102}I mean, really,|you should.
{178161}{178207}I would.
{178259}{178305}So, what now ?
{178378}{178424}I don't know.
{178485}{178530}I really don't know.
{178836}{178881}You said|you'd be right back.
{179007}{179053}I'm so sorry.
{179100}{179145}Me too.
{180798}{180844}Chuck !
{180937}{181002}Chuck !
{181005}{181050}Chuck !
{181465}{181511}I always knew you were alive.
{181513}{181576}I knew it.
{181579}{181630}But everybody said|I had to stop saying that-
{181633}{181680}that I had to let you go.
{181714}{181784}I love you.
{181786}{181830}You're the love of my life.
{181893}{181938}I love you too, Kelly.
{181977}{182023}More than you'll ever know.
{183486}{183532}Chuck ?
{183697}{183765}You have to go home.
{184923}{184972}We both had done|the math, and-
{185030}{185087}Kelly added it all up.
{185128}{185173}She knew she had|to let me go.
{185176}{185280}I added it up,|knew that I'd-
{185282}{185328}I'd lost her.
{185373}{185426}'Cause I was never gonna|get off that island.
{185499}{185561}I was gonna die there,
{185564}{185609}totally alone.
{185763}{185832}I mean, I was gonna get sick or|I was gonna get injured or something.
{185932}{185977}The only choice I had,
{186042}{186097}the only thing|I could control...
{186147}{186230}was when and how...
{186233}{186278}and where|that was gonna happen.
{186281}{186388}So... I made a rope.
{186461}{186530}And I went up to the summit|to hang myself.
{186617}{186667}But I had to test it,|you know ?
{186669}{186739}Of course.|You know me.
{186786}{186832}And the weight of the log...
{186834}{186913}snapped the limb|of the tree.
{186915}{186961}So l-l-
{186964}{187026}I couldn't even kill myself|the way I wanted to.
{187029}{187098}I had power over nothing.
{187302}{187387}And that's when this feeling|came over me like a warm blanket.
{187485}{187533}I knew...
{187536}{187590}somehow...
{187593}{187648}that I had to stay alive.
{187687}{187733}Somehow.
{187759}{187813}I had to keep breathing,
{187876}{187924}even though there was|no reason to hope.
{187977}{188064}And all my logic said that|I would never see this place again.
{188181}{188249}So that's what I did.
{188251}{188312}I stayed alive.
{188314}{188360}I kept breathing.
{188414}{188468}And then one day|that logic was proven all wrong,
{188470}{188516}because the tide...
{188567}{188627}came in,|gave me a sail.
{188740}{188785}And now, here I am.
{188866}{188937}I'm back...
{188939}{188999}in Memphis, talking to you.
{189050}{189110}I have ice in my glass.
{189271}{189328}And I've lost her|all over again.
{189516}{189573}I'm so sad that|I don't have Kelly.
{189697}{189778}But I'm so grateful that|she was with me on that island.
{189951}{190001}And I know|what I have to do now.
{190072}{190120}I gotta keep breathing.
{190185}{190244}Because tomorrow,|the sun will rise.
{190274}{190333}Who knows what the tide|could bring ?
{190669}{190754}I gave a letter to the postman
{190757}{190802}He put it in his sack
{190846}{190921}Bright and early|next morning
{190924}{190987}He brought my letter back
{190989}{191035}She wrote upon it
{191037}{191083}Return to sender
{191114}{191164}Address unknown
{191217}{191262}No such number
{191908}{191953}Hello !
{192060}{192105}Hello !
{192292}{192343}FedEx !
{193964}{194037}- You look lost.|- I do ?
{194040}{194093}Where are you headed ?
{194183}{194229}Well, I was just about|to figure that out.
{194269}{194324}Well, that's 83 South.
{194326}{194423}And this road here will|hook you up with l-40 East.
{194425}{194479}If you turn right,
{194481}{194569}that'll take you to|Amarillo, Flagstaff,
{194572}{194617}California.
{194646}{194692}And if you head back|that direction,
{194694}{194766}you'll find a whole lotta nothin'|all the way to Canada.
{194816}{194861}I got it.
{194864}{194916}All right, then.
{194977}{195023}Good luck, cowboy.
{195025}{195068}- Thank you.