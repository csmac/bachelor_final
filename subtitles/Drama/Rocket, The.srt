{1370}{1461}Shit, man.|What a lemon!
{1463}{1527}I don't know, man.|One minute it's running like a top...
{1529}{1606}and the next minute it's broken down|on the side of the road.
{1608}{1695}I can't fix a car like this.|I don't have the tools to do it, man!
{1697}{1783}Even if I did, I can't promise you|I'd know how to fix a car like that!
{1784}{1841}You see the house over here?
{1843}{1901}Let's walk over to it.|We'll use the phone.
{1903}{1941}We need to go home, okay?
{1943}{2007}Whoa, no.|Let's go to plan B.
{2009}{2058}We'll hitchhike to the next town--|Look, we hitchhike--
{2060}{2130}Look, it's over, okay?|We're broke.
{2132}{2174}We're gonna go home.
{2176}{2222}First of all, what is broke to you?
{2224}{2309}You're so spoiled. Having only|a few hundred dollars is not broke.
{2311}{2363}We're okay.|This is a setback. We hitchhike--
{2389}{2436}What is this?
{2489}{2530}All right. $16.
{2532}{2586}So, where's the rest of the money?
{2588}{2639}You gave it to Inez.
{2679}{2732}No, I never gave Inez anything.
{2777}{2829}In the envelope.
{3493}{3613}Pointless act! You don't give|a $500 tip to the housekeeper!
{3615}{3687}That's inappropriate!|That's inexcusable!
{3688}{3762}That, I don't forgive!|What were you thinking?
{3810}{3859}- Don't call her the housekeeper.|- That's what she is.
{3860}{3920}Don't threaten me. That's what she is.|She is a housekeeper.
{3922}{3977}- People are housekeepers.|- You better watch it.
{3979}{4037}You don't know what you're talking about|right now.
{4039}{4106}Her name's Inez.
{4108}{4170}And my name's Dignan, man.|So what?
{4227}{4280}She didn't love you, man.
{4340}{4375}Now where are you going?
{4707}{4764}Come on. Let's go.
{4765}{4818}Why did you do it, man?
{4860}{4937}I don't really think you know|what I was going through back there--
{4939}{5004}Please don't lay that on me now, man...
{5006}{5069}because I'm not interested|in hearing any of that.
{5118}{5200}Did it ever occur to you that I might|enjoy a great stay...
{5201}{5265}at some mental hospital|out in the middle of nowhere...
{5267}{5363}going running at night, getting a tan|with a bunch of beautiful girls?
{5365}{5417}Did you ever think about that?
{5419}{5526}What do you think Dignan was doing|that whole time you were out there?
{5528}{5635}I told you Dignan got fired--|out on his ass.
{5636}{5687}But you never thought about that,|did you?
{5748}{5831}In the end it's easier to think about|yourself than to think about Dignan.
{5874}{5935}Dignan, come on.
{5983}{6080}I thought I was supposed to be the one|that was a little crazy, you know?
{6166}{6218}Dignan, come on.
{7158}{7159}"Dear Grace, I enjoyed your letter.
{7159}{7223}"Dear Grace, I enjoyed your letter.
{7224}{7287}I agree Camp Douglas isn't all|it's cracked up to be.
{7288}{7327}It never was.
{7328}{7391}But if you're feeling lonely|or homesick even...
{7392}{7487}I recommend that, one, you call me.|I'm staying at Bob Mapplethorpe's...
{7489}{7551}and, two,|that you keep extremely busy.
{7552}{7628}- It's working for me.|- Come here, Hector.
{7630}{7687}I've got a whole, new program.
{7688}{7775}Bob and I get up at 5:30 every morning|to run our paper route.
{7776}{7819}I've got three jobs.
{7821}{7926}The money isn't much, and Bob|isn't suited for this kind of work.
{7928}{7968}But we keep each other company...
{7970}{8063}and we both feel much better about|ourselves now that we're working hard.
{8064}{8161}- Glad you enjoyed it. You all come back.|- Thanks, Mr. Fine.
{8163}{8257}Jack, I've been working.|I made $75 in tips last night.
{8259}{8339}Bob's brother, Future Man, has|accumulated some extensive legal debts.
{8341}{8400}But we're gradually helping him|to pay those off.
{8401}{8448}Can I at least have three bucks for gas?
{8450}{8555}I'm also coaching a Little League|soccer team called the Hurricanes.
{8556}{8618}They're mostly beginners,|but they've got a lot of spirit...
{8620}{8674}and they don't let defeat get them down.
{8676}{8724}They remind me of Dignan in that way.
{8726}{8833}Say what you will about him,|he's no cynic and he's no quitter.
{8835}{8893}I'm usually so exhausted now|at the end of the day...
{8895}{8955}so that I don't have time|to think about blown opportunities...
{8956}{9003}or wasted time.
{9004}{9069}I do have one bit of advice|for you though, Grace.
{9071}{9147}Take the time in school|to learn a foreign language.
{9149}{9247}I myself never did,|and I feel I paid a price for it.
{9288}{9331}Well, so long, Grace.
{9332}{9407}I miss you very much.|Don't forget to write me back.
{9408}{9443}Anthony."
{9444}{9498}Okay, there he goes.
{9600}{9645}It's Dignan. Wait up!
{9776}{9816}Where are you going?
{9818}{9881}I'm just walking Hector.
{9907}{9959}How long you been back?
{9960}{10003}A while.
{10004}{10088}So how's it goin'?
{10090}{10151}It's goin' pretty good.
{10307}{10359}I wish that some of the stuff|that was said...
{10361}{10454}hadn't been said out there by me...
{10456}{10496}and--
{10498}{10554}I'm sorry...
{10556}{10590}about--
{10657}{10713}Do you wanna shake hands?
{10932}{10976}What's this you got on, man?
{10978}{11037}It's a jumpsuit.
{11039}{11093}You like it?|I ordered a dozen of them.
{11095}{11152}Anthony, I'd like to introduce you|to Applejack.
{11154}{11217}- Applejack, this is Anthony.|- Anthony Adams.
{11376}{11418}Mr. Henry! It's Dignan!
{11440}{11484}Got some visitors.
{11548}{11599}- What time did he say he'd be here?|- Right now.
{11692}{11747}Did you guys feel that?|I just felt something--
{11748}{11794}Hey! Jesus Christ!
{11820}{11886}How's the weather down there?
{11923}{11985}- Come on in.|- Well, it's locked.
{11987}{12045}- No, no. Come on, son.|- Okay.
{12047}{12105}I just tried it,|but I'll try it again.
{12196}{12245}Yes, it is!
{12291}{12332}He poured more water on me.
{12405}{12459}- Dignan, good to see you.|- Hi, Mr. Henry.
{12461}{12504}Applejack, good to--
{12506}{12552}And who are you?
{12604}{12663}- That's Anthony--|- This is no good, bringing him here.
{12665}{12734}What the shit, man?|What are you thinking?
{12736}{12776}Are you out of your fuckin' mind?
{12827}{12857}How are you?
{12896}{12965}This is my business manager, Rowboat.
{12967}{13034}Get you a little wet, did I, son?|Come on in, boys.
{13036}{13082}There you go.
{13155}{13209}That's it.|Give it a little topspin.
{13211}{13254}A little more!
{13286}{13321}Good. He listens.
{13352}{13426}- This kid in?|- I don't know. Are you in?
{13530}{13531}- You don't understand.|- Let me tell you about this job.
{13531}{13574}- You don't understand.|- Let me tell you about this job.
{13576}{13646}I got this program|and I'm trying to stick to it, okay?
{13648}{13694}Just hear me out.
{13738}{13829}It's called Hinckley Cold Storage.|Here are a few of the key ingredients:
{13831}{13915}dynamite, pole vaulting,|laughing gas, choppers--
{13917}{14006}Can you see how incredible|this is going to be? Come on!
{14008}{14054}I can't do it.
{14106}{14182}I know, but if he doesn't have|the enthusiasm, then who needs him?
{14184}{14252}Oh, you said a mouthful.
{14253}{14309}If you want, I could speak to him.
{14311}{14364}Would you, Mr. Henry?|That'd be great.
{14366}{14437}- Sugar cone?|- You bet.
{14439}{14489}- Do you want anything, Anthony?|- No, thanks.
{14544}{14616}Listen.
{14617}{14693}You know...
{14695}{14751}you're breaking his heart,|you know that, don't you?
{14791}{14880}What do you mean?
{14882}{14948}Kid that worked|on one of my garden crews--
{14950}{15031}I mean, nice fella,|but he didn't perform.
{15032}{15092}So I had to fire him.
{15093}{15138}Then six months later...
{15164}{15241}I get a call from a gas station...
{15243}{15291}about 200 miles|outside of Victoria there.
{15293}{15360}I said, "What the hell|are you doing out there?"
{15362}{15400}But there's something in his voice.
{15402}{15458}You know what I'm saying?
{15460}{15538}I felt for him.|I mean, poor guy.
{15540}{15603}Here he is, he thought he had a team.
{15604}{15671}Turns out to be a man alone.
{15697}{15763}And that's tough, man.|Real tough.
{15764}{15808}Here you go.
{16244}{16316}- Had any time to think?|- Yeah, I've done a lot of thinking.
{16694}{16752}We're still holding|your position open for you.
{16754}{16807}Yeah, I wish you wouldn't do that.
{16808}{16884}- Come on. Go for a ride.|- It's too small for the both of us.
{16886}{16939}No it isn't. Jump on.|I'll give you a pump.
{16941}{17023}- Where'd you get this?|- I got it from a fr--
{17196}{17233}What are you wearing?
{17235}{17279}It's a jumpsuit.
{17280}{17358}- Clay, look at this guy.|- He looks like a rodeo clown.
{17360}{17416}He looks like a little banana.|Where are you from anyway, man?
{17418}{17470}I'm from around here.
{17472}{17507}- He used to mow our lawn.|- No shit?
{17509}{17606}He was great-- clipping the hedges,|sweeping up, mowing the lawn.
{17649}{17715}What was the name|of your little lawn mowing company?
{17716}{17755}The Lawn Wranglers.
{17812}{17851}Let's go.
{17905}{17955}Keep up the mowing, kemo sabe.
{18069}{18143}It was landscaping, not just mowing.
{18144}{18183}Man, don't listen to that guy.
{18184}{18233}I don't know.|Sometimes I--
{18235}{18285}I'm not always as confident as I look.
{18287}{18343}- Did you see what he had on?|- Yeah.
{18345}{18395}It was pretty cool.
{18432}{18473}All right.
{18499}{18545}Wait a second.
{18611}{18655}What?
{18812}{18914}Goddamn it. I'm in.
{18943}{19007}- What?|- I'm in.
{19008}{19056}I knew you'd be back.
{19058}{19104}Three conditions.
{19106}{19176}One: you mastermind the plan.
{19233}{19299}Two: Bob's on the team.
{19300}{19350}Let's hear the third one.
{19474}{19525}You gotta get me one of these jumpsuits.
{19527}{19561}- You like these?|- Yeah!
{19563}{19622}Done. Deal. All right.
{19624}{19699}- You're doing the right thing.|- Yeah, I know I am.
{19701}{19764}All right. I'll see you later on.
{19839}{19919}- Need any help?|- No, I think I got it.
{20100}{20142}Ha-ha! Got the eye.
{20260}{20318}Got the eye again.
{20423}{20485}Well, move back a little bit!
{20487}{20543}Hi, guys.|Welcome back.
{20544}{20626}- He's back, Mr. Henry.|- Where's Bob? The rich kid?
{20660}{20721}- Hey, what do you say?|- How's it goin'?
{20723}{20772}Dignan! What do you say?|Come on in.
{20774}{20827}- Oh, nothing much.|- What are you guys drinking?
{20829}{20903}- Water for me, please.|- I'll have a Tom Collins, please.
{20905}{20942}Tom Collins. Okay.
{20944}{21018}I want you to know it was really too bad|about what happened out there.
{21020}{21075}- I'm glad to see you.|- Yeah, it was unfortunate.
{21076}{21129}- Let's not even talk about it.|- It was stupid.
{21131}{21176}It was real stupid.|It was extremely stupid.
{21178}{21259}I want you to know|I don't expect an apology.
{21260}{21325}Can you believe this guy?|"I don't expect an apology."
{21327}{21413}- Let's not get into this.|- Of course you don't wanna get into it.
{21415}{21467}You would have let my brother|rot in fucking jail!
{21468}{21543}- You said 48 hours!|- I never agreed to that.
{21544}{21611}- That's a lie!|- Bullshit!
{21613}{21675}- Backyard! Right now! Let's go!|- Just wait here.
{21676}{21799}- Come on, Bob.|- Backyard? This is my house, asshole!
{21800}{21840}- Get out here!|- Wait a second!
{21842}{21918}I know you're feeling angry|and I think the thing to do is--
{21920}{21967}- We're all here. Let's talk.|- Wait a second!
{21968}{22043}Bob is a big boy.|Now, we are in the real world.
{22044}{22140}We don't settle problems with hugs. We|settle them with bare-knuckle tactics.
{22142}{22219}Let's talk about that.
{22220}{22272}- Fine!|- Bob, stay there!
{22274}{22356}- In my own house?|- Stop it!
{22384}{22448}Just stop!
{22450}{22532}- You got me with a left hook.|- Okay, no more fighting.
{22534}{22576}Bob, come here.
{22578}{22633}I'm gonna hug him.
{22680}{22743}What are we doing out here?|What happened?
{22744}{22778}Man!
{22780}{22859}- You went crazy.|- I know. I just-- I don't know.
{22860}{22911}- Come here.|- I was looking forward to seeing you.
{22913}{23006}Bob, we'd like to have you on the team,|if you'll have us.
{23008}{23092}Roberto, let me get something straight.
{23094}{23166}- You don't play any tennis.|- No.
{23168}{23264}- And you don't play any golf.|- No.
{23266}{23326}So why do you belong to a country club?
{23364}{23407}You got me.
{23408}{23451}You're a piece of work.
{23453}{23522}- The food is very good.|- Well, yeah. You just sign.
{23524}{23585}My work is the finger work.
{23587}{23633}Usually my work is--
{23659}{23733}Little sound and good touch.
{23735}{23784}- How you doin', Bob?|- Hey, Jackson.
{23786}{23847}When will your folks|be back in town?
{23849}{23913}That's hard to say.|Last I heard they were in Singapore.
{23915}{24007}Your brother was up here the other day.|He said you ran away from home.
{24008}{24050}- What?|- You ran away from home.
{24052}{24095}Oh, shit.
{24120}{24227}- I'll see you.|- Hi, guys. Bob, fancy seeing you here.
{24229}{24274}The rest of the gang?
{24276}{24318}- Hi, John Mapplethorpe.|- Kumar.
{24320}{24358}Nice to meet you.
{24396}{24458}Applejack, nice to meet you.
{24460}{24538}- John, great pleasure to meet you.|- And your name is?
{24540}{24606}Jackson said you told him|I ran away from home.
{24608}{24658}I might have mentioned it.
{24660}{24719}I'm 26 years old.|I didn't run away from home.
{24720}{24794}Oh, that's right.|You're on that top secret mission.
{24796}{24865}I'm sorry. I forgot I wasn't|supposed to say anything.
{24905}{24976}I'd appreciate it if you didn't|run around telling lies about me.
{24978}{25032}I'm sorry. I apologize.
{25034}{25118}I know you have a reputation to uphold.
{25120}{25183}It won't happen again.
{25185}{25249}- Jonathan.|- Yeah?
{25317}{25361}You know, Jonathan...
{25384}{25460}the world needs dreamers.
{25462}{25510}- Excuse me?|- No, I don't think so.
{25543}{25625}You know, one day I believe|that you're gonna wake up...
{25627}{25681}and realize you no longer|have a brother...
{25683}{25752}and you no longer have any friends,|and on that day...
{25754}{25845}I'm gonna be standing front and center|just laughing my fuckin' head off.
{26072}{26106}God!
{26142}{26190}I'm just messin' around with my brother.
{26192}{26232}- Are we okay here?|- Okay.
{26463}{26515}I hope this doesn't offend you, Bob...
{26540}{26596}but your brother's a cocksucker.
{26598}{26634}- I know.|- Did I offend you?
{26636}{26692}- That didn't offend me.|- Good.
{26974}{27052}Well, welcome|I've got a beautiful house
{27078}{27188}Hispanic male, nonsmoker,|enters white van, southwest door.
{27190}{27239}- Mark that down.|- I did.
{27265}{27352}There you go.|Drive away, just like you always do.
{27447}{27523}We got it, man. We know it backwards,|and we know it forewords...
{27524}{27587}because we've done the legwork,|and we've done the research.
{27588}{27640}And now it pays off tomorrow.
{27642}{27707}- Just gotta follow through.|- What are you working on?
{27709}{27756}Let me show you this.
{27788}{27845}Wow, that's one of your little drawings.
{27847}{27927}There he goes-- pole vaults over the|thing. There he goes, and there he is.
{27928}{28009}I love the drawings. I love it.|You're creative.
{28011}{28075}Tell me something.
{28100}{28147}What do you think of Inez?
{28148}{28206}- As a person?|- Yeah, as a girl.
{28246}{28304}I liked her.
{28306}{28374}I thought she was a good person.|She was friendly.
{28376}{28462}She was attractive. I didn't get|to know her as well as you did.
{28464}{28521}That's the only reason|I even bother to hesitate.
{28523}{28614}That kid Rocky struck me|as kind of a weirdo.
{28616}{28667}What? Rocky?
{28668}{28715}- I mean, he said he loved you.|- What?
{28716}{28757}Yeah, he said he loved you.
{28759}{28806}- Was he--|- I mean, that just seemed strange.
{28808}{28851}Was he translating?
{28893}{28975}Was he translating for me|when he said he loved you?
{28976}{29023}He said he loved you.
{29052}{29098}He wasn't translating.|That's how I understood him.
{29100}{29187}He was talking in English.|I was like, "What? Okay. I--"
{29189}{29236}Whoa! Anthony, where are you going?
{29237}{29322}He's just a mixed-up kid!|Anthony, come on!
{29324}{29393}You're blowing our cover.|Maybe I didn't understand him.
{29395}{29451}I'm calling for a motel.|I don't know the area code.
{29453}{29497}I just know the name of the motel.
{29499}{29590}- Listen, can I borrow him a minute?|- All right. Sure.
{29592}{29638}I was thinking.
{29640}{29717}It might give you a feeling of security|if I went in there with you tomorrow.
{29719}{29775}You know, just for backup.
{29776}{29842}- Huh, Chicky?|- Oh, that's right.
{29844}{29900}Well, I mean, it couldn't hurt,|could it?
{29902}{29967}No, it couldn't hurt, but--
{30006}{30095}You gotta weigh the pros and cons.|You're the one who hipped me to that.
{30097}{30155}Shoot, well, what are the cons?
{30156}{30223}Cons-- if you go in there tomorrow...
{30224}{30296}then it's just another score|by Abe Henry.
{30297}{30339}That's true.
{30380}{30437}Can I speak to Inez, please?
{30439}{30533}She's one of the housekeepers.|She worked there for a while.
{30535}{30583}Please don't do this to me, ma'am.
{30585}{30633}Look, I'll-- Let me see what I can do.
{30635}{30694}Well, it's your decision.
{30732}{30779}If it's okay with you...
{30805}{30859}it would mean a lot to me...
{30860}{30925}if you'd let me try this one alone.
{30927}{30983}Damn. I'll tell you something, kid.
{30984}{31050}You've got the guts of a damn lion.
{31072}{31108}Housekeeping.
{31110}{31158}Carmen, how's it goin'?
{31160}{31221}Yeah, I'm looking for Inez.|Have you seen her today? Is she in?
{31247}{31293}She's not here.
{31295}{31349}Hey, man. How's it goin'?
{31656}{31679}Hello.
{31680}{31765}I'm looking for Inez.|She's one of the housekeepers.
{31791}{31861}- She is?|- Yeah, she is. Hold on.
{32010}{32068}- It's for you. Telephone.|- Thank you.
{32232}{32315}Yes!|She recognizes my voice!
{32354}{32416}No, I'm at a party.
{32418}{32471}God, you sound terrific.|How are you?
{32504}{32565}That's great!
{32567}{32624}No, I'm great,|especially now that I got you.
{32626}{32705}I've been looking all over for you.|I talked to every person at the hotel.
{32707}{32754}When am I going to see you?
{32788}{32846}Gosh, your English is really improving.
{32848}{32922}- It sounds-- You're fluent now, Inez!|- Thank you.
{32944}{33007}So the word on the street--
{33008}{33060}Or should I say...
{33062}{33144}rumor has if that, well--
{33184}{33225}I heard you love me.
{33295}{33347}Yes, I do.
{33348}{33429}Yes!
{33431}{33501}God, Inez!
{34052}{34089}Bird Dog to Scarecrow.
{34120}{34195}- Come in.|- Go ahead, Bird Dog.
{34196}{34269}I got a visual read on you.|You're all clear.
{34271}{34334}Outstanding, Bird Dog.|Outstanding. Stand by.
{34336}{34384}Let's go!
{34386}{34429}Kumar, are you ready?
{34431}{34471}- Yes.|- Let's get lucky!
{34564}{34626}Everybody move! Let's go!|Keep up the intensity!
{34653}{34702}Come on!
{34704}{34743}Scarecrow?
{34744}{34827}- Come in, Jackknife.|- Is everything all right up there, man?
{34828}{34906}Affirmative. We're approaching|the elevator right now on my left.
{34908}{34959}- Stand by. Come in, Bird Dog.|- Copy.
{34961}{35044}- Move you your second position. Fast!|- I'm moving.
{35120}{35207}You guys take the elevator.|I'm gonna secure the stairway.
{35208}{35263}- Kumar, are you okay?|- Yeah.
{35265}{35343}I'm heading up the stairway. Everything|looks good. I just hit my knee!
{35344}{35411}Testing! Kumar!
{35413}{35491}What? I'm okay.
{35492}{35539}Stop! Get it down! Go!
{35604}{35659}Let's go. Go.|Where are you goin'? Come here.
{35661}{35723}Rendezvous at the checkpoint|in six minutes.
{35724}{35779}Checkpoint!
{35781}{35849}- Who's that man?|- What do you mean?
{35851}{35897}That's Applejack! Come on!
{36124}{36179}- Where we are going?|- Right down the corridor here.
{36180}{36231}- Are you nervous?|- Yes, I am nervous.
{36328}{36371}There it is. Oh, boy.
{36372}{36413}That's perfect.
{36540}{36570}No problem.
{36572}{36627}Hey, Jackknife, come in.
{36628}{36694}- Yeah.|- You know somethin'?
{36696}{36730}What?
{36836}{36883}I really don't wanna do this robbery.
{36884}{36962}- You know?|- Man, neither do I.
{36964}{37059}- Do you want me to come up there?|- Keep this line clear at all times.
{37061}{37105}Come on. Any activity, Bird Dog?
{37107}{37151}Negatory. We're all clear.
{37152}{37205}Good. It's supposed to be.
{37207}{37270}I got a pretty spectacular view|from up here.
{37272}{37394}I can see the whole city from the east|side all the way to the west side.
{37396}{37455}Anthony, the elevator's moving.
{37457}{37509}- Who's on the elevator?|- I don't know.
{37511}{37567}Well, check it!|It's moving right now!
{37852}{37919}Bob, what are you doing?
{37920}{37974}- My walkie-talkie busted.|- Give it to me!
{37976}{38041}- Did you drop it? Come on!|- No, I didn't drop it!
{38043}{38089}You're supposed to be|on the loading dock!
{38091}{38162}You go down there.|I'm by myself down there!
{38203}{38260}- Move it!|- What's going on?
{38262}{38312}It was Bob.|He dropped his walkie-talkie.
{38314}{38383}Who's watching the door?|Get back in position, assholes!
{38494}{38554}Applejack, talk to me.|What's going on?
{38556}{38601}Somebody's coming out of there.
{38603}{38646}- Look!|- Jesus Christ!
{38648}{38690}Hold it right there, guys!
{38692}{38721}- Freeze!|- Come this way.
{38723}{38771}Come here! Up against this wall!|Come here!
{38773}{38821}Up against this wall! Move!
{38823}{38880}- Move!|- Right here!
{38882}{38931}- Just look at the ceiling.|- Up against the wall!
{38932}{38975}- What are you doing here?|- We work here!
{38976}{39033}- You're always at lunch now!|- Not always.
{39035}{39118}Yes, always!|Okay, let me think.
{39120}{39183}Time!
{39184}{39222}Hold on! Jesus!
{39224}{39311}- What the hell are you doing here?|- My walkie-talkie broke, man.
{39312}{39351}- Two minutes.|- Good.
{39352}{39423}- Put on your masks!|- They've already seen our faces, Dignan!
{39424}{39459}Put on your masks!
{39460}{39545}Kumar, we've got problems.|What are you doing sitting down?
{39547}{39632}- I tried. I could not get it.|- You can't get it?
{39634}{39689}- No, I could not do it.|- Come on, man!
{39691}{39746}Bob, will you please put the mask on.
{39748}{39781}- Or turn away.|- I can't get it on!
{39783}{39820}Dignan, what's goin' on?
{39822}{39907}- Where's Kumar?|- Forget it, then. Just forget it!
{39908}{39980}You can't do it, because you|never knew how in the first place!
{39982}{40023}You are totally lost out here!
{40056}{40106}- What are you guys doin' here?|- Hold it!
{40108}{40191}- Who are these guys?|- Jesus!
{40232}{40293}- Jesus, Bob! What the hell--?|- Put it down, big guy!
{40295}{40359}- What is your problem?|- I didn't do anything!
{40384}{40467}- Are you okay?|- Did you shoot him?
{40468}{40525}He must be having a heart attack|or something!
{40527}{40565}Drop your smoke.|Let's go!
{40567}{40600}Come on!
{40684}{40746}Check his pulse!
{40776}{40834}- Check his pulse!|- Get to the elevator!
{40836}{40891}- I can't see.|- A little more!
{40892}{40930}I can't believe this!
{40932}{41019}- I didn't shoot him.|- He's got a bum ticker, man!
{41020}{41089}Hang in there, man.|Hang in there.
{41091}{41154}Prop him up. Come on.|Get him up there.
{41156}{41217}- Get up. Come on, man.|- He don't look good.
{41252}{41295}- What is this?|- Stay calm. Stay where you are.
{41297}{41403}- Who tripped the alarm, man?|- It's the fire alarm!
{41404}{41490}- Who tripped the fire alarm?|- It's because of all this fuckin' smoke!
{41492}{41546}Where's Kumar?
{41643}{41750}- We're going down. Let's go!|- Man, I blew it.
{41752}{41817}- I blew it, man.|- What were you doing in the freezer?
{41819}{41866}I don't know.|I lose my touch, man.
{41868}{41930}Did you ever have a touch to lose, man?
{41932}{41981}What were you doing in there?
{41983}{42032}Let's go!
{42124}{42183}- Wait for Kumar.|- Come on, Kumar!
{42184}{42231}Hurry up, Kumar!
{42312}{42359}- Open your door.|- I can't. It's locked.
{42360}{42397}- Open that door!|- Move!
{42488}{42536}- The elevator broke.|- What?
{42562}{42617}- Where's Applejack?|- He's stuck on the second floor!
{42619}{42678}- Between floors up there.|- Did he give you the keys?
{42680}{42735}- Applejack drove.|- What happened?
{42736}{42829}- I don't know.|- Run!
{42831}{42867}Go!
{42869}{42932}- What the hell happened to the plan?|- I don't know. Come on.
{42934}{42975}- I'll meet you at Bob's. Go!|- What?
{42976}{43036}- I gotta get Applejack.|- No, you're not.
{43038}{43084}- Yeah, I am.|- No!
{43132}{43188}- I gotta fly solo on this, baby.|- Okay, Dignan--
{43190}{43271}- I don't have time to argue now!|- I don't have time either!
{43272}{43339}Now, you get back to Bob's!|Just go!
{43394}{43442}Who is in charge here?
{43444}{43526}You are, you dumb son of a bitch!|Now, please leave!
{43528}{43581}Give me this one.
{43583}{43668}You gotta give me this one.
{43670}{43754}Dignan, you know what's gonna happen|if you go back there.
{43780}{43829}No, I don't.
{43831}{43890}They'll never catch me, man...
{43892}{43933}'cause I'm fuckin' innocent.
{44999}{45063}Get in here.|Concentrate on your breathing.
{45064}{45126}You gotta get oxygen to your heart.
{45241}{45307}I haven't got the keys.|Unlock the door!
{45579}{45633}Freeze! Don't move!
{45635}{45692}- Put your hands on the car!|- Put your hands up! Now!
{46061}{46114}Drop the gun!
{46724}{46771}- Freeze!|- Drop it!
{46773}{46820}Drop the gun!
{46883}{46935}Drop the gun!
{48692}{48738}Bob, there he is.
{48914}{48986}One month down, twenty-three to go.
{48988}{49032}Anyway, I said to the D.A.:
{49034}{49111}"That cop who hit me|must have given me C.R.S. disease."
{49112}{49175}- What's that?|- That's just what the D.A. asked.
{49176}{49231}C.R.S. is a disease|where you can't remember shit.
{49233}{49276}- Like amnesia.|- Right.
{49278}{49349}C.R.S. "Can't remember shit." C.R.S.
{49376}{49430}- Tell Mr. Henry I said that.|- Okay.
{49468}{49536}So is Mr. Henry gonna come by|and see me or what?
{49564}{49611}I don't think so.
{49612}{49678}He actually robbed Bob's house.
{49680}{49732}- You're kidding?|- No, I'm not.
{49734}{49787}I can't believe that.
{49789}{49836}I almost robbed that place myself.
{49900}{49956}You think Applejack knew?
{49958}{50072}We haven't heard from Applejack|since he got out of the hospital.
{50074}{50124}His case got dismissed.
{50126}{50172}- What? Why?|- We're not sure.
{50174}{50224}We think Mr. Henry had something|to do with it.
{50226}{50317}Well, they take the health problems|into account.
{50445}{50543}- So what all did he get?|- Pretty much everything.
{50545}{50640}- I bet he got that grand piano.|- He got everything.
{50642}{50710}I bet that grand piano's|worth ten grand.
{50818}{50877}I'm sorry, Bob.
{50904}{50959}Yeah, but you know,|in a strange way...
{50961}{51034}it's brought me and Future Man|closer together.
{51036}{51141}I mean, we went out|to look for a new piano the other day...
{51143}{51219}and he looks at me and says:|"Bob, just because you're a fuck-up...
{51220}{51263}doesn't mean you're not my brother."
{51386}{51467}That kind of touched me.
{51468}{51521}He doesn't normally open up like that.
{51583}{51649}- Hey, I talked to Inez last night.|- Really?
{51651}{51714}Gonna be bringing you a care package|when she comes up here.
{51716}{51795}- No shit?|- Yeah.
{51796}{51895}She liked me.|Oh, yeah. Hang on to that one.
{51896}{51957}You want anything special?
{52084}{52146}Well--
{52148}{52211}- I got something for you guys.|- Oh, yeah?
{52212}{52271}Belt buckles that I made.
{52272}{52315}There's one for you and one for you.
{52316}{52404}I made a couple more|that I was gonna give...
{52406}{52452}to, you know, Applejack,|Kumar or Mr. Henry.
{52454}{52521}But-- I don't know.|Maybe give it to Inez.
{52523}{52571}I don't care. Give it to those guys.
{52573}{52632}I don't have any hard feelings.
{52634}{52717}Here's a little piece.
{52719}{52765}That's for the tip.
{52877}{52930}- Thanks.|- We did it though, didn't we?
{53034}{53096}Yeah, we did it, all right.
{53238}{53300}Well, thank you for comin'.
{53370}{53408}It's good seein' you.
{53500}{53578}- Did you bring that grappling hook?|- Grappling hook?
{53580}{53629}I think I might have found|a way out of here.
{53631}{53689}- You're kidding!|- No, I'm not.
{53691}{53760}- How?|- Wait for my instructions.
{53785}{53850}When we go through the next gate, you'll|have 30 seconds to take out the guard.
{53852}{53877}- What?|- 30 seconds!
{53879}{53934}Have the car running|at the northwest checkpoint.
{53936}{54028}- Bob and I will scale the barricade.|- Hold on!
{54030}{54137}And Bob, remember, shield me from|the bullets! They won't shoot civilians!
{54139}{54182}- Are you ready?|- Hold on!
{54184}{54256}Let's go! Now!
{54410}{54491}Isn't it funny how you used to be|in the nuthouse and now I'm in jail?
