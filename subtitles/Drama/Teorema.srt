1
00:00:14,480 --> 00:00:18,951
Your boss has given you this factory.
What do you think?

2
00:00:19,280 --> 00:00:22,556
So he's the main character
in this story?

3
00:00:22,720 --> 00:00:26,508
Hasn't this blocked all chance of a
workers' revolution in the future?

4
00:00:26,800 --> 00:00:31,954
Is his act unique, or a general
modern world tendency?

5
00:00:32,120 --> 00:00:34,031
A general tendency, I think

6
00:00:34,280 --> 00:00:38,398
Can such acts, as symbols of
a new source of power...

7
00:00:38,560 --> 00:00:44,396
...herald the transformation of
all humanity into the bourgeoisie?

8
00:00:45,040 --> 00:00:49,272
The bourgeoisie will never turn
all humanity into bourgeoisie

9
00:00:49,800 --> 00:00:52,553
So the not very original answer is:

10
00:00:52,720 --> 00:00:55,951
No matter what he does, even giving
away a factory...

11
00:00:56,200 --> 00:00:58,714
...a bourgeois is always wrong

12
00:00:58,880 --> 00:01:01,633
- Is that it?
- I don't want to reply

13
00:01:01,880 --> 00:01:04,075
This bourgeoisie is making
revolutionary changes...

14
00:01:04,360 --> 00:01:06,032
...in our situation

15
00:01:06,200 --> 00:01:09,715
It's identifying all humanity
with the bourgeois class

16
00:01:09,960 --> 00:01:12,474
It no longer faces a class struggle...

17
00:01:12,720 --> 00:01:15,951
...using the army, the nation
or the church

18
00:01:16,120 --> 00:01:18,873
Then it would fail; it would lose
its natural allies

19
00:01:19,040 --> 00:01:23,352
You must answer the new questions
raised by the bourgeoisie

20
00:01:24,480 --> 00:01:26,198
Of course

21
00:01:26,520 --> 00:01:28,192
What are the answers to these
questions?

22
00:01:29,040 --> 00:01:31,395
What are the answers to these
questions?

23
00:03:47,340 --> 00:03:51,333
<i>God led the people about
through the way of the wilderness</i>

24
00:07:50,100 --> 00:07:51,852
ARRIVING TOMORROW

25
00:08:02,420 --> 00:08:04,251
Pietro, how about some music?

26
00:12:43,220 --> 00:12:47,293
Well, do you feel better?
It was nothing.

27
00:14:51,640 --> 00:14:54,393
Sorry. I'm afraid it's all rather
makeshift

28
00:14:54,640 --> 00:14:56,392
It's fine

29
00:15:01,040 --> 00:15:03,031
It's been a real invasion

30
00:15:08,600 --> 00:15:11,398
I used to sleep in this room
when I was little

31
00:22:28,800 --> 00:22:29,994
I'm here

32
00:22:54,000 --> 00:22:55,718
Sorry

33
00:24:37,800 --> 00:24:40,234
Your friends are calling you

34
00:24:47,480 --> 00:24:49,072
You're a serious player

35
00:25:04,480 --> 00:25:06,994
These are my friends

36
00:28:46,720 --> 00:28:48,392
What's the time?

37
00:28:49,000 --> 00:28:50,228
I don't know

38
00:28:50,560 --> 00:28:54,997
- What does it matter?
- It's six o'clock

39
00:30:01,000 --> 00:30:03,992
It's about a sick man like me

40
00:30:08,000 --> 00:30:12,790
<i>Even in this disagreeable situation
Ivan Ilich found comfort</i>

41
00:30:13,000 --> 00:30:18,233
<i>Gerasim came to wash him, Gerasim
was a clean, fresh peasant boy</i>

42
00:32:30,000 --> 00:32:31,558
What are you reading?

43
00:32:32,000 --> 00:32:34,230
<i>His life was his and his alone</i>

44
00:32:34,480 --> 00:32:39,315
<i>It would take longer to duplicate
his goodness than to create a star</i>

45
00:32:40,120 --> 00:32:43,635
<i>The godhead which had come
without my hoping for it...</i>

46
00:32:43,800 --> 00:32:46,712
<i>... has not returned, and will
never return again</i>

47
00:35:52,800 --> 00:35:57,555
I like speeding. Now I'll show you
what the car can do

48
00:35:57,720 --> 00:36:00,632
I believe you, but I'm not young
any more

49
00:36:16,240 --> 00:36:18,117
You're not Gerasim...

50
00:36:18,400 --> 00:36:20,868
...it's difficult to face you

51
00:36:24,160 --> 00:36:28,551
There are two reasons
I must talk to you

52
00:36:28,880 --> 00:36:33,396
Firstly, there's my moral sense

53
00:36:34,560 --> 00:36:39,236
Then there's a sort of confusion
within me...

54
00:36:40,800 --> 00:36:46,079
...which maybe I can only clarify
by talking about it

55
00:38:48,800 --> 00:38:53,157
<i>You have seduced me, Lord,
and I have let myself be seduced</i>

56
00:38:53,640 --> 00:38:57,076
<i>You have violated me
and you have prevailed</i>

57
00:38:58,240 --> 00:39:03,553
<i>I have become an object of daily
derision, everyone mocks me</i>

58
00:39:05,480 --> 00:39:09,996
<i>For I heard the defaming of many,
terror on every side</i>

59
00:39:10,400 --> 00:39:13,392
<i>"Denounce him, and we will
denounce him"</i>

60
00:39:14,800 --> 00:39:17,997
<i>All my friends awaited my downfall,
saying:</i>

61
00:39:18,560 --> 00:39:21,791
<i>"Perhaps he will let himself be
seduced...</i>

62
00:39:22,000 --> 00:39:24,958
<i>"...and we shall take our revenge
on him"</i>

63
00:39:54,360 --> 00:39:57,750
What did you say? So you do talk!

64
00:40:33,000 --> 00:40:36,151
I must go. Tomorrow

65
00:40:44,800 --> 00:40:46,791
I don't know myself any more

66
00:40:47,080 --> 00:40:50,789
Because what made me the same
as others has been destroyed

67
00:40:51,000 --> 00:40:55,630
I was like all the others,
maybe with many faults

68
00:40:55,800 --> 00:41:00,476
You've made me different by taking
me from the natural order of things

69
00:41:00,640 --> 00:41:04,315
While you were near,
I didn't realize it

70
00:41:04,720 --> 00:41:07,712
Now I understand, you're leaving...

71
00:41:07,880 --> 00:41:12,158
...and knowing I'm losing you
makes me aware of my difference

72
00:41:12,800 --> 00:41:15,394
What will become of me from now on?

73
00:41:15,560 --> 00:41:20,236
The future will be like living with
someone nothing to do with myself

74
00:41:20,640 --> 00:41:25,395
Maybe I must probe the depths of
this difference you have revealed...

75
00:41:25,560 --> 00:41:29,155
...which is the innermost anguish
of my being

76
00:41:29,320 --> 00:41:35,714
But whether I desire it or not, won't
this set me against everyone?

77
00:41:40,000 --> 00:41:43,151
I realize now that I've never had...

78
00:41:43,800 --> 00:41:47,395
...any real interest in anything

79
00:41:49,320 --> 00:41:52,312
I don't mean some special interest

80
00:41:52,560 --> 00:41:56,553
Not even ordinary interests,
like my husband's in his work...

81
00:41:56,720 --> 00:42:00,633
...or my son's in his studies,
or Odetta's domestic cult

82
00:42:02,480 --> 00:42:04,471
I had no interests

83
00:42:05,280 --> 00:42:09,398
I don't know how I could lead
such an empty life, but I did...

84
00:42:09,960 --> 00:42:16,229
If there was anything...
some instinctive love of life...

85
00:42:16,480 --> 00:42:21,156
...it dried up like a garden
where nobody goes

86
00:42:22,560 --> 00:42:27,634
That emptiness was filled
with false, mean values...

87
00:42:27,880 --> 00:42:31,555
...with a hideous load of false ideas

88
00:42:33,640 --> 00:42:39,158
Now I realize it, you've filled
my life with a total interest

89
00:42:40,800 --> 00:42:45,715
So, in parting, you're not destroying
anything I had before...

90
00:42:45,880 --> 00:42:50,237
...except perhaps my chaste bourgeois
reputation. What do I care?

91
00:42:51,720 --> 00:42:54,917
But what you yourself gave me...

92
00:42:55,160 --> 00:42:59,153
...love in my empty life...

93
00:42:59,800 --> 00:43:03,156
...by leaving me you're
destroying it completely

94
00:43:04,720 --> 00:43:07,359
By knowing me, you've made me
a normal girl

95
00:43:08,000 --> 00:43:11,151
You've made me find the right
solution for my life

96
00:43:11,320 --> 00:43:15,791
Before, I didn't know men,
but I was afraid of them

97
00:43:16,000 --> 00:43:18,639
I loved only my father

98
00:43:19,640 --> 00:43:23,838
But now, in leaving me, you're
making me worse than before

99
00:43:25,320 --> 00:43:28,551
Did you want that? Now the pain
of losing you...

100
00:43:29,000 --> 00:43:34,552
...will cause a relapse more
dangerous than the sickness I had...

101
00:43:34,720 --> 00:43:38,395
...before this brief cure
your presence brought

102
00:43:38,640 --> 00:43:42,076
Before, I didn't understand
this illness, but now I do

103
00:43:42,800 --> 00:43:46,873
Through the love you gave me
I've become aware of my illness

104
00:43:48,040 --> 00:43:52,158
Now how can I replace you?
Is there anyone who can?

105
00:43:52,320 --> 00:43:57,155
I don't think I can go on living

106
00:44:33,000 --> 00:44:36,151
You must have come here to destroy

107
00:44:37,480 --> 00:44:42,156
The destruction you've caused in me
couldn't be more complete

108
00:44:43,800 --> 00:44:47,918
You've simply destroyed the image
I always had of myself

109
00:44:48,400 --> 00:44:51,392
Now I can't see anything at all...

110
00:44:52,160 --> 00:44:55,232
...that can rebuild my identity

111
00:44:56,080 --> 00:44:58,071
What do you suggest?

112
00:44:59,320 --> 00:45:02,995
A scandal like death in the eyes of
society...

113
00:45:03,160 --> 00:45:05,913
...a complete loss of myself

114
00:45:06,720 --> 00:45:10,554
How can this be for a man
used to the idea of order...

115
00:45:10,720 --> 00:45:14,793
...of the future and, above all,
of ownership?

116
00:45:29,640 --> 00:45:32,791
- I'll carry it
- No, I'll carry it

117
00:45:33,160 --> 00:45:35,151
Then let's carry it together

118
00:52:53,080 --> 00:52:56,789
- Have you a tape measure?
- At once, miss

119
00:56:29,240 --> 00:56:31,629
Time to eat, miss

120
00:57:28,640 --> 00:57:30,790
I can't help it. It's beyond my
expertise. Sorry

121
00:58:32,240 --> 00:58:34,231
Open your fist, miss, please

122
01:02:33,000 --> 01:02:36,629
How pathetic! I'm a real shit!

123
01:02:59,240 --> 01:03:01,151
I'm not eating

124
01:03:03,240 --> 01:03:06,073
You must eat. It's good

125
01:03:07,080 --> 01:03:09,310
I don't want to

126
01:03:10,760 --> 01:03:12,955
Why don't you want to eat? It's good

127
01:03:16,200 --> 01:03:18,668
At least tell us what you want to eat

128
01:04:55,000 --> 01:04:59,152
We must try to invent new techniques,
unrecognizable...

129
01:05:01,160 --> 01:05:04,630
...which are unlike any
previous method...

130
01:05:05,480 --> 01:05:08,631
...to avoid childishness, ridicule...

131
01:05:10,240 --> 01:05:15,633
...make our world unlike any other...

132
01:05:18,160 --> 01:05:21,630
...where previous standards
don't apply...

133
01:05:22,400 --> 01:05:25,153
...which must be new,
like the technique

134
01:05:27,240 --> 01:05:30,232
Nobody must realize that
the artist is worthless...

135
01:05:30,400 --> 01:05:33,710
...that he's an abnormal,
inferior being...

136
01:05:33,880 --> 01:05:37,793
...who squirms and twists like a worm
to survive

137
01:05:40,720 --> 01:05:43,996
Nobody must ever catch him out
as naive

138
01:05:46,160 --> 01:05:48,879
Everything must be presented
as perfect...

139
01:05:50,880 --> 01:05:56,637
...based on unknown,
unquestionable rules...

140
01:05:56,880 --> 01:05:59,394
...like an madman, that's it

141
01:06:00,320 --> 01:06:04,632
Pane after pane, because I can't
correct anything...

142
01:06:05,640 --> 01:06:07,995
...and nobody must notice

143
01:06:09,480 --> 01:06:12,313
A sign painted on a pane...

144
01:06:13,000 --> 01:06:15,992
...corrects, without soiling it...

145
01:06:16,640 --> 01:06:19,871
...a sign painted earlier
on another pane

146
01:06:21,880 --> 01:06:25,236
But everyone must believe...

147
01:06:25,400 --> 01:06:30,474
...that it isn't the trick of an
untalented artist, impotent artist

148
01:06:31,880 --> 01:06:35,998
Not at all. It must look like
a sure decision...

149
01:06:36,160 --> 01:06:39,994
...fearless, lofty and
almost arrogant

150
01:06:43,240 --> 01:06:48,872
Nobody must know that a sign
succeeds by chance... is fragile

151
01:06:51,800 --> 01:06:56,078
That as soon as a sign appears
well made, by a miracle...

152
01:06:58,000 --> 01:07:02,994
...it must be protected,
looked after, as in a shrine

153
01:07:04,800 --> 01:07:07,792
But nobody must realize...

154
01:07:09,080 --> 01:07:12,993
...that the artist is a poor,
trembling idiot, second-rate...

155
01:07:13,400 --> 01:07:19,316
...living by chance and risk,
in disgrace like a child...

156
01:07:19,800 --> 01:07:23,315
...his life reduced to absurd
melancholy...

157
01:07:23,480 --> 01:07:29,794
...degraded by the feeling of
something lost for ever

158
01:08:22,160 --> 01:08:24,799
DOWN WITH ALL STATES

159
01:08:25,080 --> 01:08:28,072
DOWN WITH ALL CHURCHES

160
01:08:28,320 --> 01:08:30,914
UP WITH PAINTERS OF...

161
01:08:44,480 --> 01:08:46,789
Blue reminds me of him

162
01:08:46,960 --> 01:08:51,272
But obviously blue isn't enough,
it's only a part

163
01:08:51,560 --> 01:08:56,714
Who can permit me such mutilation,
what ideology can justify it?

164
01:08:57,000 --> 01:09:00,356
Weren't my first miserable attempts
at portraits better?

165
01:09:25,200 --> 01:09:30,832
Look at the state she's in,
eating nettles

166
01:09:31,300 --> 01:09:33,655
Lord, what a scandal

167
01:13:52,000 --> 01:13:53,433
I don't smoke

168
01:20:57,020 --> 01:20:59,090
Which is the road back to Milan?

169
01:21:01,420 --> 01:21:04,014
Turn left, then straight on

170
01:24:16,280 --> 01:24:18,430
What would happen to me if...

171
01:24:20,100 --> 01:24:24,013
...I stripped myself of everything,
gave my factory to the workers?

172
01:29:12,500 --> 01:29:14,092
Let's go

173
01:31:17,600 --> 01:31:21,479
Don't be afraid, I didn't come to die,
but to weep

174
01:31:22,160 --> 01:31:23,912
Not tears of sorrow

175
01:31:24,160 --> 01:31:28,870
No, they will be a source...
but not a source of suffering

176
01:31:33,480 --> 01:31:36,199
Go away now

