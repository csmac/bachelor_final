﻿00:00:00.000 --> 00:00:07.000
<i>Watch and Download App at official site FastDrama.Co to support us</i>

00:00:12.743 --> 00:00:14.614
(Hate to Love You)

00:00:15.554 --> 00:00:16.923
You're here.

00:00:18.083 --> 00:00:19.153
Yes.

00:00:20.254 --> 00:00:22.524
I'm here to meet a friend.

00:00:23.423 --> 00:00:24.824
A friend?

00:00:26.224 --> 00:00:29.033
Like you said,

00:00:29.603 --> 00:00:32.234
let's become good companions.

00:00:33.473 --> 00:00:35.173
We'll be good friends.

00:00:35.533 --> 00:00:36.804
I'm sure of it.

00:00:38.204 --> 00:00:39.713
That's why I'm saying this,

00:00:40.113 --> 00:00:43.243
but there's someplace you should go with me.

00:00:43.984 --> 00:00:45.213
Where is that?

00:00:45.783 --> 00:00:47.314
I'd like to show you...

00:00:47.314 --> 00:00:50.223
what kind of person I am.

00:00:51.253 --> 00:00:52.993
If your feelings remain the same,

00:00:53.353 --> 00:00:55.893
let's really become friends.

00:00:56.423 --> 00:00:58.223
Taxi, taxi.

00:00:59.634 --> 00:01:02.094
Haeng Ja? Haeng Ja...

00:01:06.734 --> 00:01:07.833
Here you go.

00:01:17.014 --> 00:01:18.684
She never takes taxis.

00:01:20.184 --> 00:01:21.583
And who is that man?

00:01:26.893 --> 00:01:29.594
Bo Geum, Bo Geum.

00:01:29.594 --> 00:01:32.464
What is so urgent?

00:01:32.663 --> 00:01:35.464
I just saw Haeng Ja take a taxi somewhere.

00:01:35.863 --> 00:01:38.234
It was her, right? It wasn't someone else, was it?

00:01:38.464 --> 00:01:41.934
You're right, it was her. She just left.

00:01:42.074 --> 00:01:43.943
Who is the man that was with her?

00:01:44.204 --> 00:01:46.204
Oh, that man.

00:01:46.544 --> 00:01:47.613
Who is it?

00:01:47.744 --> 00:01:52.143
She recently made a new friend.

00:01:52.814 --> 00:01:54.613
That's all you have to know.

00:01:56.083 --> 00:01:57.154
A friend?

00:01:58.654 --> 00:02:00.223
A boyfriend?

00:02:02.523 --> 00:02:04.624
That word makes it feel so real.

00:02:05.023 --> 00:02:06.264
A boyfriend.

00:02:10.733 --> 00:02:11.834
Wait.

00:02:12.904 --> 00:02:14.033
What are we doing here?

00:02:16.203 --> 00:02:18.603
This building belongs to me.

00:02:20.374 --> 00:02:21.513
Even this?

00:02:22.943 --> 00:02:24.013
Oh, this.

00:02:24.614 --> 00:02:25.814
I see.

00:02:25.883 --> 00:02:28.753
Why are you showing this to me?

00:02:29.253 --> 00:02:33.453
I grew up at the market all my life.

00:02:33.654 --> 00:02:35.753
I'm tougher, wilder and rougher...

00:02:36.054 --> 00:02:39.864
than you probably think I am.

00:02:39.864 --> 00:02:42.494
Do you think I wanted to be friends without knowing that?

00:02:43.904 --> 00:02:48.934
If you approached me because you need money,

00:02:49.503 --> 00:02:51.543
you can tell me now.

00:02:53.344 --> 00:02:57.283
If I needed money, I would've continued being your tutor.

00:02:59.283 --> 00:03:02.114
I thought you enjoyed...

00:03:02.783 --> 00:03:05.154
spending time with me.

00:03:06.624 --> 00:03:08.193
I must've been wrong.

00:03:09.293 --> 00:03:11.494
No, you aren't.

00:03:11.693 --> 00:03:14.334
As we get older,

00:03:14.834 --> 00:03:16.533
it's hard to believe others.

00:03:17.133 --> 00:03:20.473
I'm sure it's worse for you, since you've met all kinds of people.

00:03:24.774 --> 00:03:27.614
I understand, but am disappointed.

00:03:29.883 --> 00:03:31.184
I'm sorry.

00:03:31.184 --> 00:03:34.314
Living alone, things like this...

00:03:35.084 --> 00:03:36.753
has caused me problems.

00:03:37.483 --> 00:03:41.353
I wasn't suspicious of you.

00:03:45.624 --> 00:03:48.334
We promised to be good companions.

00:03:50.964 --> 00:03:52.834
Yes, let's give it a try.

00:03:53.374 --> 00:03:56.774
Let's enjoy the comfort we have as we get older.

00:04:07.383 --> 00:04:09.214
She practically has me in the palm of her hand.

00:04:09.214 --> 00:04:10.823
I knew she wouldn't be easy,

00:04:11.654 --> 00:04:13.094
but I'll have to stay alert.

00:04:14.253 --> 00:04:18.763
I wonder how much money she actually has.

00:04:18.763 --> 00:04:20.333
More buildings are showing up every day,

00:04:21.133 --> 00:04:22.393
although I'm not complaining.

00:04:23.934 --> 00:04:25.434
Here you go.

00:04:47.323 --> 00:04:49.023
Over here.

00:04:53.193 --> 00:04:54.494
You're late.

00:04:54.893 --> 00:04:57.664
I'm sorry. Something came up on my way here.

00:04:58.734 --> 00:05:01.503
I'll treat you to anything you want, since I was late.

00:05:02.073 --> 00:05:03.174
Really?

00:05:19.523 --> 00:05:20.753
You're embarrassing me.

00:05:21.153 --> 00:05:23.924
Stop staring at me, and eat. The food is good here.

00:05:27.593 --> 00:05:30.804
I'll tell you about the nourishing cream after we eat.

00:05:34.974 --> 00:05:36.674
Choong Seo.

00:05:36.674 --> 00:05:40.474
Sitting here with you reminds me of old times.

00:05:41.843 --> 00:05:42.943
Old times?

00:05:43.713 --> 00:05:47.854
As time goes by, I keep thinking of when we first met.

00:05:48.354 --> 00:05:49.513
I had forgotten about it.

00:05:50.784 --> 00:05:54.153
Really? I remember every detail.

00:05:55.354 --> 00:05:57.224
What do you remember the most?

00:05:59.494 --> 00:06:02.263
Actually, it's a secret.

00:06:03.133 --> 00:06:04.463
What is it?

00:06:05.333 --> 00:06:06.534
I was...

00:06:07.604 --> 00:06:11.843
interested in you when you were 20 years old.

00:06:14.614 --> 00:06:15.713
Really?

00:06:17.083 --> 00:06:20.814
A rich girl came to the theater every day,

00:06:21.184 --> 00:06:24.023
saying she'd help, but only caused more trouble.

00:06:24.583 --> 00:06:26.994
I got in so much trouble with my mom.

00:06:27.224 --> 00:06:29.763
I barely lifted a finger at home.

00:06:33.864 --> 00:06:37.463
I was waiting one day with a fluttering heart,

00:06:38.263 --> 00:06:39.903
but she didn't show up.

00:06:40.474 --> 00:06:45.203
I never saw you again after that day.

00:06:47.273 --> 00:06:49.374
When was that?

00:06:50.484 --> 00:06:52.143
I don't remember.

00:06:53.213 --> 00:06:57.054
I heard rumors that you got married to a rich guy.

00:07:01.494 --> 00:07:05.494
I think that was...

00:07:06.534 --> 00:07:08.833
my first love,

00:07:10.604 --> 00:07:13.573
that caused me pain before passing.

00:07:17.674 --> 00:07:18.974
First love?

00:07:28.354 --> 00:07:29.784
You're late.

00:07:30.583 --> 00:07:32.153
Yes, I'm sorry.

00:07:32.684 --> 00:07:35.054
Please don't say anything today.

00:07:35.424 --> 00:07:38.864
What is it? Did the person you visited at the hospital die?

00:07:39.263 --> 00:07:40.364
The hospital?

00:07:41.364 --> 00:07:43.133
Oh, right. The hospital...

00:07:43.304 --> 00:07:44.903
Were you really visiting someone?

00:07:45.763 --> 00:07:47.734
You look like someone who was on a date.

00:07:47.874 --> 00:07:51.343
I bumped into a man I used to know.

00:07:51.343 --> 00:07:52.773
By chance?

00:07:53.674 --> 00:07:57.713
You're saying you met the man you kept talking about by chance?

00:08:00.854 --> 00:08:03.184
Why would you lie when you'll get caught right away?

00:08:03.784 --> 00:08:06.253
I shouldn't tell you anything.

00:08:08.323 --> 00:08:10.963
Did things end as soon as you met him? What's with the face?

00:08:11.593 --> 00:08:12.794
No.

00:08:13.734 --> 00:08:16.763
He told me I was his first love.

00:08:18.903 --> 00:08:20.003
Really?

00:08:20.474 --> 00:08:23.104
Why are you sighing when that's a good thing?

00:08:23.703 --> 00:08:25.403
I feel weird.

00:08:25.874 --> 00:08:29.414
I realized there was a time in my life like that, and I miss it,

00:08:30.513 --> 00:08:32.843
but I don't want to go back.

00:08:33.184 --> 00:08:36.213
Why not? Did something happen between you two?

00:08:36.453 --> 00:08:38.054
No, it's not because of that.

00:08:39.083 --> 00:08:41.853
It's about the difficult 30 years that followed.

00:08:42.754 --> 00:08:44.294
I can't go through that again.

00:08:46.664 --> 00:08:47.764
Gosh.

00:08:49.634 --> 00:08:51.733
Who was your first love?

00:08:52.973 --> 00:08:54.174
First love?

00:08:59.274 --> 00:09:02.174
Wait, is it In Woo's dad?

00:09:02.414 --> 00:09:03.544
Seriously?

00:09:03.684 --> 00:09:06.083
You were so innocent.

00:09:06.514 --> 00:09:08.814
I had fallen hard.

00:09:09.254 --> 00:09:12.254
He must've been a good person back then.

00:09:12.483 --> 00:09:16.223
Of course, he was the pride of the town.

00:09:16.223 --> 00:09:18.764
I eloped with him in the middle of the night, so I wouldn't...

00:09:19.024 --> 00:09:21.164
Wait a second. What am I saying?

00:09:21.634 --> 00:09:25.404
Your life would be nothing compared to my ups and downs.

00:09:25.463 --> 00:09:26.934
You ran away with him?

00:09:27.573 --> 00:09:29.904
You were always a drama queen.

00:09:30.044 --> 00:09:31.404
I wasn't normal.

00:09:32.044 --> 00:09:35.073
Do you think you could run away like that again?

00:09:35.573 --> 00:09:38.314
Why not, if I meet the right person?

00:09:39.483 --> 00:09:40.483
I feel the same.

00:09:45.884 --> 00:09:47.394
- Please enjoy. - Thank you.

00:09:52.294 --> 00:09:54.034
You said you skipped lunch too.

00:09:54.294 --> 00:09:55.894
Would this be enough for you?

00:09:56.404 --> 00:09:57.863
I have to live like this for a while.

00:09:59.363 --> 00:10:01.873
Don't look at me like that. It's not as bad as it seems.

00:10:03.004 --> 00:10:04.103
You said you have something to tell me.

00:10:05.774 --> 00:10:09.644
Have you ever thought about your mother's remarriage?

00:10:10.514 --> 00:10:11.914
Is my mother getting married again?

00:10:12.343 --> 00:10:13.483
Who is the man this time?

00:10:13.853 --> 00:10:14.953
Well...

00:10:16.323 --> 00:10:17.424
Bu Sik.

00:10:18.453 --> 00:10:19.654
Sorry. I overreacted.

00:10:20.654 --> 00:10:22.054
There's no point of hiding anything from you.

00:10:23.164 --> 00:10:25.363
You saw how my mother caused trouble because of men as you grew up.

00:10:27.064 --> 00:10:30.363
I asked you about that because of my dad.

00:10:31.804 --> 00:10:35.634
It looks like he has found someone to marry.

00:10:38.203 --> 00:10:39.404
Does that make you worry?

00:10:40.973 --> 00:10:44.113
My father never listens to other people.

00:10:44.983 --> 00:10:46.713
He'll get remarried if he has made up his mind.

00:10:47.554 --> 00:10:48.713
Then what's the problem?

00:10:50.554 --> 00:10:52.554
I don't want him to get remarried.

00:10:54.593 --> 00:10:57.963
However, I don't want him to continue living like a nomad either.

00:10:59.463 --> 00:11:01.863
Do you want him to get back together with your mother?

00:11:03.963 --> 00:11:05.363
That's impossible.

00:11:05.804 --> 00:11:08.034
In Jung would say she will leave home immediately,

00:11:08.934 --> 00:11:10.473
and I would feel uncomfortable too.

00:11:12.703 --> 00:11:14.573
I don't know what this feeling is.

00:11:15.943 --> 00:11:17.943
I think both of us are selfish children.

00:11:20.054 --> 00:11:22.654
Our parents were the ones who were selfish first.

00:11:23.384 --> 00:11:24.453
That's true, but...

00:11:28.853 --> 00:11:30.723
- Why? - It's just that...

00:11:32.424 --> 00:11:34.233
I didn't expect to hear you saying such a thing.

00:11:36.634 --> 00:11:37.764
You're right.

00:11:38.664 --> 00:11:40.434
Our parents were selfish first.

00:11:45.274 --> 00:11:46.373
Let's eat.

00:11:51.184 --> 00:11:52.784
- Oh, my. Is it stuck there? - Hang on.

00:11:53.353 --> 00:11:56.623
I thought Mi Ae's son was dating Haeng Ja's daughter.

00:11:58.323 --> 00:12:01.394
But he's with Jung Sook's eldest daughter this time.

00:12:02.353 --> 00:12:03.693
Did Mi Ae go somewhere?

00:12:05.164 --> 00:12:06.564
Mi Ae has been busy lately.

00:12:06.764 --> 00:12:09.333
Gosh. I bet she's not as busy as her son.

00:12:09.634 --> 00:12:10.733
Bu Sik?

00:12:11.634 --> 00:12:13.103
I'm sure he's busy as well.

00:12:13.434 --> 00:12:15.603
He must be busy dating Haeng Ja's daughter.

00:12:16.644 --> 00:12:19.473
Wasn't Mi Ae's son dating Haeng Ja's daughter?

00:12:19.674 --> 00:12:21.544
I don't care. Why do you ask?

00:12:21.544 --> 00:12:24.613
I saw Mi Ae's son eating ramyeon with your eldest daughter.

00:12:25.314 --> 00:12:26.914
- With In Woo? - Yes.

00:12:26.914 --> 00:12:28.384
They looked quite intimate.

00:12:28.384 --> 00:12:31.024
They've known each other for over 10 years.

00:12:31.024 --> 00:12:32.424
There's nothing weird about it.

00:12:32.424 --> 00:12:36.664
My goodness. A man with a girlfriend would eat with his girlfriend.

00:12:36.764 --> 00:12:38.323
Why would he eat with his friend?

00:12:40.993 --> 00:12:43.363
You should be relieved that I'm the one who saw them.

00:12:43.463 --> 00:12:45.264
Our neighbors like gossiping one another.

00:12:45.363 --> 00:12:47.904
If a rumor starts, it would be bad for your daughter.

00:12:52.774 --> 00:12:55.014
Bu Sik. Bu Sik.

00:12:55.914 --> 00:12:58.014
Yes? I ate dinner.

00:12:58.713 --> 00:13:00.953
Did you eat ramyeon with In Woo at a cart bar?

00:13:00.953 --> 00:13:04.424
- How did you know that? - You two lived here for 10 years.

00:13:04.524 --> 00:13:07.693
Everyone knows you guys. Are you going to keep meeting her like that?

00:13:07.693 --> 00:13:10.424
She said she had something to discuss. It wasn't a date.

00:13:10.424 --> 00:13:13.634
I see. So In Woo changed her strategy.

00:13:13.634 --> 00:13:14.934
I said it wasn't like that.

00:13:15.134 --> 00:13:16.363
Don't be so naive.

00:13:16.363 --> 00:13:17.564
There's a rumor about you already.

00:13:17.564 --> 00:13:19.774
They say that my son is dating two women at the same time.

00:13:19.774 --> 00:13:20.904
That's ridiculous.

00:13:20.973 --> 00:13:22.573
That's what people always do.

00:13:22.573 --> 00:13:24.174
People will always spread rumors...

00:13:24.174 --> 00:13:25.943
about someone's wrongdoings and failures.

00:13:27.274 --> 00:13:30.443
I've been worried since you said marriage can be a good opportunity.

00:13:30.514 --> 00:13:33.554
Those who say such things always settle with someone they love.

00:13:33.684 --> 00:13:34.853
You should be careful too.

00:13:34.853 --> 00:13:36.424
In Woo isn't a person like that.

00:13:36.684 --> 00:13:40.554
They say that you can never see through a person's mind.

00:13:40.823 --> 00:13:42.123
Isn't In Woo a person?

00:13:43.223 --> 00:13:44.323
I'll be careful.

00:13:44.564 --> 00:13:46.993
By the way, how close have you gotten to Eun Jo?

00:13:47.294 --> 00:13:48.493
We're getting along well.

00:13:48.664 --> 00:13:51.004
I think you're considering marrying her as well.

00:13:51.164 --> 00:13:54.233
You have to be aggressive to win a woman's heart.

00:13:54.404 --> 00:13:56.373
Speaking of which, hurry up, and get married.

00:13:56.573 --> 00:13:58.144
If you drag it on, you may ruin the whole thing.

00:14:01.274 --> 00:14:02.373
Marrying her?

00:14:06.654 --> 00:14:09.184
- I'm home. - Come in for a second.

00:14:10.723 --> 00:14:12.254
Why? I'm tired.

00:14:13.424 --> 00:14:16.863
Oh, right. I ate dinner.

00:14:17.064 --> 00:14:18.193
With whom?

00:14:19.634 --> 00:14:20.794
Would you even know the person?

00:14:21.833 --> 00:14:23.904
Tell me whatever you have to say. I'll go to sleep now.

00:14:24.203 --> 00:14:25.564
You seem to be in a good mood.

00:14:26.064 --> 00:14:27.373
Why would I be in a bad mood?

00:14:27.703 --> 00:14:30.443
You cried your heart out because you were dumped just a few days ago.

00:14:30.603 --> 00:14:32.343
Who said that I got dumped?

00:14:32.743 --> 00:14:33.914
Pull yourself together.

00:14:34.544 --> 00:14:37.343
Everyone knows that Bu Sik is interested in someone else.

00:14:37.544 --> 00:14:40.254
He's only being fooled by a sly wench for now.

00:14:40.254 --> 00:14:43.684
When he comes to his senses, he might come back to me.

00:14:43.924 --> 00:14:45.223
My goodness. What did you say?

00:14:45.583 --> 00:14:47.693
If you're finished, I'll leave. I'm so tired.

00:14:51.323 --> 00:14:52.764
Why is she so stubborn?

00:14:55.093 --> 00:14:57.233
What if Bu Sik ends up with someone else?

00:14:57.634 --> 00:14:59.264
What would she do then?

00:15:03.943 --> 00:15:05.404
Hey. Did you just come home?

00:15:06.713 --> 00:15:09.644
- Yes. - Come, and sit down here.

00:15:09.743 --> 00:15:10.983
Myung Jo.

00:15:12.443 --> 00:15:14.314
Why? Do you have something to tell us?

00:15:15.754 --> 00:15:16.853
Why?

00:15:16.853 --> 00:15:21.554
Well, I have something to say to both of you.

00:15:28.363 --> 00:15:31.103
Well... The thing is...

00:15:33.034 --> 00:15:37.044
Gosh. I don't know where to start.

00:15:38.044 --> 00:15:40.414
Why is it so difficult to say?

00:15:41.743 --> 00:15:44.314
Not too long ago,

00:15:44.743 --> 00:15:49.884
I met a lecturer at the culture center.

00:15:50.784 --> 00:15:53.593
Since he has a vast knowledge of things,

00:15:53.654 --> 00:15:57.463
I started taking a class with him.

00:16:01.333 --> 00:16:06.164
As you know, I haven't had much education.

00:16:06.434 --> 00:16:10.073
You two will find a match for you soon,

00:16:10.174 --> 00:16:12.103
and I'll have in-laws.

00:16:12.343 --> 00:16:13.814
Just get to the point already.

00:16:14.743 --> 00:16:16.314
So what's your point?

00:16:17.044 --> 00:16:18.314
Are you going to enroll in a school?

00:16:20.514 --> 00:16:21.613
Go on.

00:16:23.054 --> 00:16:25.123
I can't go back to school at my age.

00:16:25.884 --> 00:16:26.993
So...

00:16:29.794 --> 00:16:33.764
What I'm saying is that I decided to...

00:16:34.463 --> 00:16:37.564
become good friends with the lecturer.

00:16:38.404 --> 00:16:39.434
Friends?

00:16:44.343 --> 00:16:47.573
I'll introduce him to you two when it's the right time.

00:16:48.144 --> 00:16:51.384
We're friends who can talk to each other while growing old together.

00:16:51.644 --> 00:16:54.453
That kind of friend. Just friends. Do you understand?

00:17:01.294 --> 00:17:05.623
So that means that she has a boyfriend, right?

00:17:07.393 --> 00:17:08.964
Did you see him?

00:17:09.603 --> 00:17:11.403
She said she'll introduce him to us soon.

00:17:12.663 --> 00:17:14.474
A man can be better judge of other men.

00:17:15.504 --> 00:17:17.403
He's too old to be called a man.

00:17:17.804 --> 00:17:19.544
You don't like this situation, do you?

00:17:20.444 --> 00:17:21.673
Are you happy about this?

00:17:22.113 --> 00:17:23.343
I told you before...

00:17:23.444 --> 00:17:26.353
that I wouldn't mind her getting married if he's a good person.

00:17:26.913 --> 00:17:29.984
She said they're just friends. Why would you talk about marriage?

00:17:30.353 --> 00:17:31.754
You never know what could happen.

00:17:31.754 --> 00:17:32.923
Don't get ahead of yourself.

00:17:32.923 --> 00:17:34.823
- Eun Jo. - I want to be alone.

00:17:35.653 --> 00:17:36.794
I understand.

00:17:47.804 --> 00:17:48.933
She could get married?

00:17:50.004 --> 00:17:51.173
Haeng Ja will do that?

00:18:13.593 --> 00:18:16.704
You seem more tenacious than I thought.

00:18:16.964 --> 00:18:18.064
What else should I do?

00:18:19.433 --> 00:18:20.534
Come with me.

00:18:22.433 --> 00:18:24.744
Gosh. I didn't mean that. More work?

00:18:32.683 --> 00:18:33.853
You did a good job today.

00:18:38.284 --> 00:18:40.823
I wish I could drink with you, but I'm busy.

00:18:41.353 --> 00:18:42.593
Drink this before you go home.

00:18:42.593 --> 00:18:43.863
It will help with your tiredness.

00:18:45.123 --> 00:18:46.294
Actually...

00:18:47.663 --> 00:18:49.363
See you tomorrow, Suk.

00:18:51.163 --> 00:18:52.304
What was that?

00:18:59.774 --> 00:19:00.873
Are you drinking alone?

00:19:01.444 --> 00:19:02.544
That's good.

00:19:03.583 --> 00:19:05.544
I've been feeling down. Let's drink together.

00:19:05.814 --> 00:19:08.583
Excuse me. Give me a glass a pair of chopsticks, please.

00:19:08.583 --> 00:19:10.224
No, no. This is not what it looks like.

00:19:10.224 --> 00:19:11.323
It's okay.

00:19:12.153 --> 00:19:13.224
- Thank you. - You're welcome.

00:19:13.224 --> 00:19:14.353
You don't mind if I join you, right?

00:19:23.264 --> 00:19:24.603
- Let's drink. - Okay.

00:19:31.843 --> 00:19:33.413
One shot should be okay, right?

00:19:42.083 --> 00:19:43.683
I hope I made the right decision.

00:19:45.123 --> 00:19:48.224
I can quit any time if it doesn't seem right.

00:19:48.823 --> 00:19:52.623
I've been fine all these years.

00:19:52.623 --> 00:19:54.363
Eun Jo, Haeng Ja.

00:19:55.464 --> 00:19:56.863
Why is he calling us?

00:20:01.274 --> 00:20:03.873
What is going on?

00:20:04.004 --> 00:20:05.073
Wait.

00:20:05.073 --> 00:20:07.873
Isn't this the young man that works at the beauty center?

00:20:08.873 --> 00:20:10.044
Hong Suk?

00:20:10.143 --> 00:20:12.343
What happened?

00:20:12.954 --> 00:20:15.583
- Haeng Ja, my room. - All right, come this way.

00:20:19.593 --> 00:20:23.264
Gosh, what in the world happened?

00:20:23.363 --> 00:20:24.663
Did he drink?

00:20:25.363 --> 00:20:26.363
What?

00:20:26.363 --> 00:20:28.764
I thought you were sleeping. When did you go out, drink...

00:20:28.764 --> 00:20:30.304
and bring another person home?

00:20:30.304 --> 00:20:32.234
I was having trouble sleeping,

00:20:32.234 --> 00:20:35.034
so I went to a tent bar for a drink and ran into him.

00:20:35.034 --> 00:20:36.073
My goodness.

00:20:36.073 --> 00:20:39.343
How much did he drink?

00:20:39.343 --> 00:20:41.843
He passed out after taking a sip.

00:20:41.843 --> 00:20:43.014
That doesn't make sense.

00:20:43.014 --> 00:20:45.544
I can't believe that even if you say it, Myung Jo.

00:20:45.784 --> 00:20:48.284
- I'm sure he's right. - What?

00:20:48.353 --> 00:20:50.583
But you shouldn't have brought him here.

00:20:50.583 --> 00:20:53.724
Eun Jo, what kind of person is he?

00:20:54.024 --> 00:20:56.294
He only has three numbers on his phone.

00:20:56.593 --> 00:20:59.323
Aunt, Mr. Lee, and yours.

00:21:00.093 --> 00:21:02.163
No one was answering,

00:21:02.494 --> 00:21:03.734
so I had no choice but to bring him.

00:21:06.163 --> 00:21:07.274
It's cold.

00:21:08.903 --> 00:21:12.274
You did the right thing, since it's cold outside.

00:21:12.974 --> 00:21:16.843
However, I can't believe he can't drink any more than a sip.

00:21:19.014 --> 00:21:20.954
Do you know anything about him?

00:21:22.454 --> 00:21:24.423
He shouldn't be drinking.

00:21:28.153 --> 00:21:30.393
Maybe I should've answered the phone properly.

00:21:30.593 --> 00:21:33.393
He said it's Hong Suk, the employee at the beauty center.

00:21:33.464 --> 00:21:36.034
What if you answered and his identity got revealed?

00:21:36.093 --> 00:21:37.504
How are you going to deal with that?

00:21:37.734 --> 00:21:41.204
Still, you shouldn't have hung up without knowing the situation.

00:21:41.204 --> 00:21:44.103
It's okay, I got a feeling.

00:21:44.103 --> 00:21:45.244
Nothing will happen.

00:21:45.244 --> 00:21:47.974
And remember what happened at the club. It's nothing new.

00:21:48.573 --> 00:21:50.044
How could you be so confident?

00:21:50.244 --> 00:21:51.944
Do you think he'd leave Suk Pyo in the streets...

00:21:51.944 --> 00:21:53.913
after calling us?

00:21:53.913 --> 00:21:55.254
He'd at least call the police.

00:21:55.954 --> 00:21:58.883
It's nothing to worry about, so go to bed.

00:21:59.224 --> 00:22:01.294
This is why I should track his location.

00:22:01.454 --> 00:22:03.064
I don't believe this.

00:22:05.393 --> 00:22:06.994
These two nuisances.

00:22:08.163 --> 00:22:11.403
Suk Pyo has been acting weird nowadays.

00:22:13.403 --> 00:22:16.474
Come on out and have breakfast.

00:22:27.284 --> 00:22:28.883
Why are you standing there?

00:22:28.883 --> 00:22:30.383
Have something to help with your hangover,

00:22:30.683 --> 00:22:33.224
although I'm not sure if you have a hangover.

00:22:35.454 --> 00:22:36.564
I'm sorry.

00:22:36.724 --> 00:22:39.163
Why do you always apologize when you're here?

00:22:39.163 --> 00:22:42.294
Don't be a stranger. You can make yourself at home.

00:22:42.294 --> 00:22:45.903
I heard you did a part-time job with Eun Jo.

00:22:46.103 --> 00:22:50.244
You can think of this as a co-worker's home.

00:22:50.843 --> 00:22:52.373
Why aren't you sitting down?

00:22:53.214 --> 00:22:54.373
Have a seat.

00:23:01.883 --> 00:23:04.883
I like having more people at home.

00:23:05.554 --> 00:23:08.194
Stop over-reacting. It's just one more person.

00:23:08.254 --> 00:23:12.294
I don't remember the last time I had breakfast with you two.

00:23:12.464 --> 00:23:14.393
You're always too busy,

00:23:14.393 --> 00:23:16.363
and I barely see you.

00:23:16.964 --> 00:23:19.633
You must've been lonely.

00:23:20.633 --> 00:23:21.974
Is that why...

00:23:24.304 --> 00:23:28.314
Don't you have to call your family?

00:23:28.314 --> 00:23:30.244
They must be worried.

00:23:30.613 --> 00:23:32.883
No one will answer, even if I called right now.

00:23:32.984 --> 00:23:35.254
Don't worry. He's all grown up.

00:23:36.323 --> 00:23:37.583
Eat up.

00:23:37.823 --> 00:23:40.323
Haeng Ja is known for her food at the market.

00:23:40.994 --> 00:23:42.554
Thank you.

00:23:42.923 --> 00:23:45.593
You can come over whenever you want a home-cooked meal.

00:23:45.593 --> 00:23:48.393
People need to eat well to survive.

00:23:48.393 --> 00:23:51.433
A young man like you shouldn't rely on instant food like ramyeon.

00:23:51.704 --> 00:23:52.804
Okay.

00:23:55.633 --> 00:23:57.204
- Haeng Ja. - Yes?

00:23:57.704 --> 00:24:00.913
You haven't been collecting money. What business are you starting?

00:24:03.244 --> 00:24:05.143
Finish up, and go to school. Aren't you late?

00:24:05.143 --> 00:24:06.214
No, I'm fine.

00:24:07.653 --> 00:24:11.524
Are you learning the business you've been telling us about?

00:24:11.524 --> 00:24:14.524
- I'll support... - Haeng Ja.

00:24:15.954 --> 00:24:19.294
Who cares? I'm sure he knows who I am now.

00:24:19.294 --> 00:24:21.133
- Right? - Of course.

00:24:22.494 --> 00:24:24.704
Eat up.

00:24:24.833 --> 00:24:27.704
You need to be on a full stomach to be less sad.

00:24:27.704 --> 00:24:32.044
It's like I have another son, and it feels good.

00:24:39.853 --> 00:24:41.054
All right. Goodbye.

00:24:46.454 --> 00:24:48.724
Why? Is someone else in the hospital?

00:24:49.393 --> 00:24:50.964
You know what's going on.

00:24:51.393 --> 00:24:53.734
You said you'd give me a break for now.

00:24:54.133 --> 00:24:55.534
I did? When did I say that?

00:24:55.593 --> 00:24:59.064
I'm going to try to make it work this time.

00:24:59.673 --> 00:25:01.833
It sounds like you're determined to get married.

00:25:02.234 --> 00:25:03.474
Why not?

00:25:04.843 --> 00:25:06.774
You want to get dragged through mud again?

00:25:07.774 --> 00:25:09.544
If you get married again,

00:25:09.714 --> 00:25:11.383
- it'll be your fourth, right? - Come on.

00:25:11.383 --> 00:25:12.643
Don't say that.

00:25:12.784 --> 00:25:14.913
I married Bu Sik's dad for money...

00:25:15.214 --> 00:25:17.054
and the second one...

00:25:17.123 --> 00:25:19.893
I met during my darkest time and thought it'd be better.

00:25:19.893 --> 00:25:22.524
The third one was a total swindler.

00:25:23.994 --> 00:25:25.764
Don't you feel bad for Bu Sik?

00:25:25.764 --> 00:25:28.734
That's why I'm going for a better person this time.

00:25:29.133 --> 00:25:32.504
I've known him for a long time, so there's nothing to lie about.

00:25:32.504 --> 00:25:34.133
You haven't seen him for a while.

00:25:34.133 --> 00:25:35.474
That's a dangerous thought.

00:25:35.704 --> 00:25:39.004
You said you're meeting a man too.

00:25:39.103 --> 00:25:41.373
It's just for fun.

00:25:41.474 --> 00:25:43.613
I have no intention of getting married.

00:25:43.883 --> 00:25:46.143
Yesterday, you said you could run away if you met the right man.

00:25:46.143 --> 00:25:47.514
Why are you changing your words?

00:25:47.754 --> 00:25:48.984
Did you take that seriously?

00:25:49.514 --> 00:25:51.683
Are you being fickle?

00:25:52.224 --> 00:25:53.823
Never mind about me.

00:25:54.954 --> 00:25:57.323
I hope it works out if that's how you feel.

00:25:57.964 --> 00:26:00.764
The second time is hard, but it's the same after that.

00:26:01.833 --> 00:26:03.704
Don't be so confident.

00:26:03.804 --> 00:26:06.804
Who knows? Maybe you'll get married before I do.

00:26:07.704 --> 00:26:08.833
I'm leaving.

00:26:11.373 --> 00:26:12.744
What is she talking about?

00:26:12.843 --> 00:26:14.643
Me, get married to him?

00:26:16.514 --> 00:26:18.744
No, that won't work out.

00:26:19.444 --> 00:26:23.353
Wait, if she's even thinking of marrying him...

00:26:24.224 --> 00:26:28.494
I'll have to see who this man is.

00:26:29.764 --> 00:26:32.224
(Dia Pawnshop)

00:26:32.694 --> 00:26:33.964
You're going to sell the building?

00:26:34.294 --> 00:26:38.363
Yes, there is an old three-story building behind a school.

00:26:39.163 --> 00:26:41.034
It's only caused me trouble since the day I bought it.

00:26:41.034 --> 00:26:43.403
I'm not getting much rent out of it,

00:26:43.603 --> 00:26:45.073
so I'm willing to sell it at a low price.

00:26:45.573 --> 00:26:46.673
I see.

00:26:47.873 --> 00:26:52.284
I'm considering asking you to take the job.

00:26:52.643 --> 00:26:55.554
Would you give it a try?

00:26:55.784 --> 00:26:58.784
It's full or units that are mortgaged.

00:26:58.784 --> 00:27:00.353
It's very complicated.

00:27:01.494 --> 00:27:02.593
I'll do it.

00:27:03.623 --> 00:27:05.323
All right, thank you.

00:27:05.323 --> 00:27:07.234
I think you'll do a good job.

00:27:07.464 --> 00:27:08.534
Thank you.

00:27:08.994 --> 00:27:10.833
All right, that's settled.

00:27:12.933 --> 00:27:15.204
What are you going to do about Eun Jo?

00:27:15.774 --> 00:27:16.774
Excuse me?

00:27:16.774 --> 00:27:20.343
Everyone in the neighborhood knows you're seeing each other.

00:27:20.944 --> 00:27:22.044
Well...

00:27:23.683 --> 00:27:25.284
I'd like to marry her.

00:27:26.413 --> 00:27:27.554
Ms. Kim.

00:27:29.883 --> 00:27:32.653
Mother, I should've asked for your blessing first and I'm sorry.

00:27:33.823 --> 00:27:34.954
Mother?

00:27:35.323 --> 00:27:36.593
Could I call you that?

00:27:37.823 --> 00:27:39.593
You already did.

00:27:40.163 --> 00:27:43.103
Seeing that Eun Jo is dating him without a complaint,

00:27:44.234 --> 00:27:47.034
she must be interested.

00:28:00.754 --> 00:28:02.014
Goodbye.

00:28:02.484 --> 00:28:03.823
- You can go in. - Okay.

00:28:06.353 --> 00:28:08.754
Hi, what brings you here?

00:28:08.954 --> 00:28:10.224
Hi, Ms. Kim.

00:28:10.494 --> 00:28:13.933
This is a VIP ticket for the beauty center.

00:28:13.994 --> 00:28:15.564
Mr. Hong sent it over.

00:28:15.794 --> 00:28:17.734
VI... What?

00:28:17.863 --> 00:28:22.234
This is a magic ticket you can use on anything at the center,

00:28:22.234 --> 00:28:25.143
including hair, makeup, massages and clothes.

00:28:25.244 --> 00:28:27.544
- Right? - That's absolutely right.

00:28:27.774 --> 00:28:30.984
Why would he send this to me?

00:28:30.984 --> 00:28:32.883
I'm sure it isn't free.

00:28:33.183 --> 00:28:35.554
I heard you're starting a new business.

00:28:36.083 --> 00:28:38.653
He told me you'd understand if I said this.

00:28:39.593 --> 00:28:41.593
You're young, but smart.

00:28:42.524 --> 00:28:45.794
Please visit the center whenever you can...

00:28:45.794 --> 00:28:47.863
and come to me when you need to discuss your business.

00:28:49.333 --> 00:28:50.863
- Have a nice day. - Bye.

00:28:53.673 --> 00:28:57.974
Eun Jo is so lucky.

00:28:58.643 --> 00:29:01.673
- Why? - You'll probably give this to her.

00:29:01.673 --> 00:29:04.284
Why would I give it to her? I'm going to use it myself.

00:29:04.683 --> 00:29:06.044
You are?

00:29:06.143 --> 00:29:08.383
I'm a woman too, you know.

00:29:08.514 --> 00:29:12.024
I'm going to get facials from now on...

00:29:12.153 --> 00:29:14.524
and take care of myself.

00:29:21.663 --> 00:29:24.034
She was a woman.

00:29:25.633 --> 00:29:27.373
You're going to do what?

00:29:27.633 --> 00:29:28.974
A facial.

00:29:29.744 --> 00:29:33.173
I heard men get all kinds of treatment nowadays.

00:29:33.673 --> 00:29:36.383
My face is getting dry from the cold weather.

00:29:36.544 --> 00:29:38.383
I think I need to do something about it.

00:29:39.883 --> 00:29:42.923
Then, pay for the facials.

00:29:43.254 --> 00:29:45.353
I know, but the thing is...

00:29:45.353 --> 00:29:46.423
Is money the problem?

00:29:47.724 --> 00:29:50.123
Don't you get a staff discount?

00:29:50.923 --> 00:29:53.833
Wait, no. You're a certified facial therapist.

00:29:54.133 --> 00:29:55.833
Can't you do it for me for free?

00:29:58.663 --> 00:30:00.133
Who brought what?

00:30:00.234 --> 00:30:04.103
Ms. Kim Haeng Ja brought a VIP ticket for the beauty center.

00:30:04.603 --> 00:30:05.774
Is that so?

00:30:05.774 --> 00:30:08.113
Then you serve her yourself. Pay extra attention to her,

00:30:08.113 --> 00:30:11.014
and provide her with the best service.

00:30:11.143 --> 00:30:13.413
Yes, I will. Beauty, beauty.

00:30:15.554 --> 00:30:17.954
Could she be interested in the beauty business?

00:30:19.454 --> 00:30:20.593
Hang on.

00:30:21.823 --> 00:30:24.964
We have to get on her good side before other companies do.

00:30:25.964 --> 00:30:27.234
What should I do?

00:30:29.034 --> 00:30:31.433
If we could drag her into our business...

00:30:32.804 --> 00:30:34.004
That would be incredible.

00:30:40.643 --> 00:30:41.744
Please lie down here.

00:30:42.373 --> 00:30:44.744
- Lie down here? - Yes.

00:30:44.984 --> 00:30:46.413
Please take off your robe first.

00:30:47.383 --> 00:30:50.353
I'm not wearing anything under this.

00:30:51.724 --> 00:30:53.653
It must be your first time getting a facial.

00:30:54.224 --> 00:30:56.593
No one can do well at the first try.

00:31:01.234 --> 00:31:03.633
Do men get facials here as well?

00:31:06.804 --> 00:31:07.903
Of course.

00:31:08.534 --> 00:31:10.544
That's embarrassing. Why would a man do that?

00:31:14.173 --> 00:31:16.044
Do something about him.

00:31:16.044 --> 00:31:19.413
I'm uncomfortable because it's as if I'm with a man in the same room.

00:31:24.353 --> 00:31:28.353
Gosh. I'm trying all sorts of things thanks to Mr. Jung.

00:31:30.123 --> 00:31:32.363
Wake up. I said, "Wake up."

00:31:32.363 --> 00:31:33.534
Move to the next room.

00:31:33.534 --> 00:31:35.464
She's treating her customer badly.

00:31:36.433 --> 00:31:40.333
They said they provide the best service, but that wasn't true.

00:31:53.054 --> 00:31:55.153
Why would she wake me up while I was having a good time?

00:31:55.984 --> 00:31:57.554
What room should I go to?

00:31:59.994 --> 00:32:01.554
- Please wait for a moment. - Okay.

00:32:11.004 --> 00:32:12.103
My goodness.

00:32:12.433 --> 00:32:14.633
What is Ms. Kim doing here?

00:32:17.044 --> 00:32:18.403
Seriously.

00:32:36.994 --> 00:32:39.423
Gosh. I got startled.

00:32:39.663 --> 00:32:41.833
Mr... Mr. Jung.

00:32:43.964 --> 00:32:45.504
Ms. Kim, you're here as well.

00:32:53.173 --> 00:32:56.514
(Vocational Training Institute)

00:33:02.653 --> 00:33:05.353
A penny for your thoughts, Mr. Jung.

00:33:07.353 --> 00:33:08.524
I'm sorry.

00:33:09.964 --> 00:33:11.593
All the customers left earlier.

00:33:12.863 --> 00:33:15.194
- I see. - Check this out.

00:33:15.393 --> 00:33:18.363
- Our mailbox was full of letters. - Let me see.

00:33:18.363 --> 00:33:22.133
- We received a lot this time. - Exactly.

00:33:24.274 --> 00:33:26.613
My gosh. What's this smell, madam?

00:33:27.814 --> 00:33:31.683
- Oh, no. I left stew on the stove. - Not again.

00:33:32.343 --> 00:33:33.984
She might burn the whole cafe down.

00:33:37.353 --> 00:33:38.454
Look at this.

00:33:38.954 --> 00:33:41.194
Do people still send handwritten letters like this?

00:33:42.694 --> 00:33:44.663
"Dear Ms. Kim Haeng Ja, our sponsor."

00:33:45.964 --> 00:33:47.064
"Our sponsor"?

00:33:49.093 --> 00:33:50.163
What should I do?

00:33:50.504 --> 00:33:53.073
- Should I pour water in here? - What's this smell?

00:33:53.073 --> 00:33:55.204
I have to take care of this first, Ms. Kim.

00:33:55.274 --> 00:33:56.343
Hello, Ms. Kim.

00:34:00.044 --> 00:34:01.474
Your skin is glowing.

00:34:02.643 --> 00:34:04.784
You look great yourself as well.

00:34:05.413 --> 00:34:07.714
I wanted to impress you.

00:34:08.654 --> 00:34:13.024
Oh, right. Is the foot you hurt earlier okay?

00:34:13.453 --> 00:34:15.123
That was nothing.

00:34:15.123 --> 00:34:17.763
- Shall I show you? - No. That's okay.

00:34:17.763 --> 00:34:19.324
You might break your bone.

00:34:19.864 --> 00:34:23.203
I mean, you could hurt your foot.

00:34:23.603 --> 00:34:25.134
Please speak as you always do.

00:34:25.303 --> 00:34:28.533
Your primitive expressions sound charming to me.

00:34:29.533 --> 00:34:33.174
You're teasing me, right, Mr. Jung?

00:34:34.274 --> 00:34:35.373
You're right.

00:34:37.314 --> 00:34:39.513
Please don't do that again.

00:34:40.453 --> 00:34:41.953
I was surprised.

00:34:42.984 --> 00:34:44.754
Your skin looks stunning.

00:34:45.353 --> 00:34:47.123
- Do I look okay? - You look great.