﻿1
00:00:08,400 --> 00:00:13,472
Nail Shop Paris

2
00:00:15,190 --> 00:00:17,421
Bunny.

3
00:00:17,421 --> 00:00:19,539
How's Alex?

4
00:00:20,393 --> 00:00:27,004
He's sleeping. The doctor said he should
stay in the hospital for a couple of days.

5
00:00:27,924 --> 00:00:31,264
He let the disease grow.

6
00:00:31,264 --> 00:00:34,028
By the way, isn't Kay with you?

7
00:00:34,028 --> 00:00:37,508
I didn't see him.
Didn't you leave with him?

8
00:00:52,851 --> 00:00:54,703
Kay.

9
00:00:57,181 --> 00:01:02,175
The aura around you
is so depressingly gray.

10
00:01:02,175 --> 00:01:05,578
It means there's something you're worrying about.

11
00:01:05,578 --> 00:01:07,874
Am I right?

12
00:01:07,874 --> 00:01:09,546
What?

13
00:01:14,064 --> 00:01:17,243
Pick one.
Come on.

14
00:01:28,724 --> 00:01:30,965
Fortune's wheel.

15
00:01:30,965 --> 00:01:37,377
It means life altering, luck
or a fateful meeting.

16
00:01:39,251 --> 00:01:43,195
Kay, did you get a girlfriend?

17
00:01:43,195 --> 00:01:44,973
Shut up.

18
00:02:04,240 --> 00:02:08,464
A fateful meeting?

19
00:02:12,464 --> 00:02:14,335
I hate her so much.

20
00:02:14,335 --> 00:02:18,046
She looks bad on the screen
because her skin is so bad.

21
00:02:18,046 --> 00:02:20,494
Why does she take it out on me?

22
00:02:20,494 --> 00:02:23,513
She's a chain smoker
and has a bad temper.

23
00:02:23,513 --> 00:02:28,152
And she's foul-mouthed.
She swears a lot.

24
00:02:29,165 --> 00:02:33,102
You shouldn't hear any swear words.
Your pretty ears will get dirty.

25
00:02:35,066 --> 00:02:37,721
My eyes are about to get dirty.

26
00:02:46,797 --> 00:02:49,528
What the heck is this?

27
00:02:49,528 --> 00:02:52,228
You think I can even buy
gum with this money?

28
00:02:52,228 --> 00:02:56,864
Don't play with me like this.
Prepare more money. Understand?

29
00:03:02,929 --> 00:03:05,252
I'm sorry.
I'm sorry.

30
00:03:05,252 --> 00:03:10,101
I'm sorry. Excuse me, can I get a bill?

31
00:03:10,101 --> 00:03:13,131
How can she smile like
that in this situation?

32
00:03:20,564 --> 00:03:24,648
I see. I should go see Alex later.

33
00:03:24,648 --> 00:03:29,932
- Jin, take care of Alex's customers too.
- Okay. Don't worry.

34
00:03:32,724 --> 00:03:36,076
- Why isn't Kay here yet?
- He must be late.

35
00:03:36,076 --> 00:03:38,340
Bunny, take this.

36
00:03:42,081 --> 00:03:43,471
Nail class?

37
00:03:43,471 --> 00:03:50,092
Alex asked me for that. You should go there
and take the class 3 times a week. Good luck.

38
00:03:50,092 --> 00:03:52,041
- Okay.
- Get to work, guys.

39
00:03:55,941 --> 00:03:59,670
Why?
Are you impressed by Alex?

40
00:03:59,670 --> 00:04:01,712
No.

41
00:04:04,680 --> 00:04:06,657
You got a new haircut, Kay.

42
00:04:06,657 --> 00:04:08,093
Yeah.

43
00:04:08,093 --> 00:04:10,875
You look good.

44
00:04:15,237 --> 00:04:16,810
What's wrong with him?

45
00:04:16,810 --> 00:04:20,901
Just ask me why Pororo wears stupid glasses.

46
00:04:20,901 --> 00:04:22,378
Why does he wear that?

47
00:04:22,378 --> 00:04:24,778
Because he wants to.

48
00:04:51,110 --> 00:04:53,495
Excuse me, can I sit here?

49
00:04:53,495 --> 00:04:55,200
Sure.

50
00:05:02,838 --> 00:05:06,615
- What's wrong?
- No. Nothing.

51
00:05:10,153 --> 00:05:13,266
Nice to meet you.
I'm Jeong Mi Ae.

52
00:05:13,266 --> 00:05:15,527
I'm Hong...

53
00:05:15,527 --> 00:05:18,836
Just call me Bunny.

54
00:05:30,946 --> 00:05:36,894
Now, you're going to practice with
someone who's sitting next you.

55
00:05:36,894 --> 00:05:40,475
I'll do it first.

56
00:05:53,187 --> 00:05:55,399
What's wrong?

57
00:05:56,769 --> 00:05:58,855
Nothing.

58
00:06:13,855 --> 00:06:15,539
Boss.

59
00:06:15,539 --> 00:06:17,740
You brought a lot of stuff.

60
00:06:19,886 --> 00:06:21,607
Comic books?

61
00:06:21,607 --> 00:06:24,651
I thought he would be bored.

62
00:06:24,651 --> 00:06:27,910
Bunny is better than me.

63
00:06:27,910 --> 00:06:29,982
I'll leave you two alone.
I have to make a call.

64
00:06:34,204 --> 00:06:35,764
Have a seat.

65
00:06:39,783 --> 00:06:41,972
How's the class?
Do you like it?

66
00:06:41,972 --> 00:06:44,277
Yeah.

67
00:06:45,990 --> 00:06:51,896
Alex, do you remember Tae Hee?

68
00:06:51,896 --> 00:06:59,665
You said you noticed Tae Hee got the blues
by seeing red lines on her nails or something.

69
00:06:59,665 --> 00:07:02,552
Anyway, is that right?

70
00:07:02,552 --> 00:07:05,252
Yeah. Why do you ask?

71
00:07:06,562 --> 00:07:11,285
There's this girl that has
red lines on her nails.

72
00:07:11,285 --> 00:07:17,740
But an interesting thing is that she doesn't
seem like a person with depression.

73
00:07:17,740 --> 00:07:21,622
She always smiles like a robot.

74
00:07:21,622 --> 00:07:23,784
Maybe she did become a robot.

75
00:07:23,784 --> 00:07:24,996
What?

76
00:07:24,996 --> 00:07:26,651
There are some people like her.

77
00:07:26,651 --> 00:07:30,014
They have to smile everyday for
their job and it becomes a habit.

78
00:07:30,014 --> 00:07:34,774
So even if they want to cry and be angry,
they just smile like a machine.

79
00:07:39,465 --> 00:07:41,307
Bunny.

80
00:07:41,307 --> 00:07:43,679
You're sharp.

81
00:07:44,793 --> 00:07:49,440
Take care of her.
She might need some help.

82
00:07:50,593 --> 00:07:53,641
Okay.
Don't worry.

83
00:08:03,920 --> 00:08:06,101
You study so hard.

84
00:08:08,574 --> 00:08:10,791
Are you not going to write
your novel anymore?

85
00:08:16,822 --> 00:08:23,467
It's good to become a nail artist but think
about the people who are waiting for your novel.

86
00:08:24,427 --> 00:08:27,001
Are you not meeting Jin tonight?

87
00:08:27,001 --> 00:08:30,649
One of my co-workers told me
not to meet him too often.

88
00:08:30,649 --> 00:08:31,749
Why not?

89
00:08:31,749 --> 00:08:34,518
She said if I meet him too much,
he will get tired of me.

90
00:08:34,518 --> 00:08:37,370
That doesn't make sense at all.

91
00:08:37,370 --> 00:08:39,695
You're right.

92
00:08:44,562 --> 00:08:47,199
Jin, it's me.

93
00:08:47,199 --> 00:08:50,189
Why is she so happy?

94
00:09:24,299 --> 00:09:29,427
- What are you doing?
- Stay still. I'm an expert, you know.

95
00:09:30,833 --> 00:09:37,960
If a person has clean nails, she is
beloved by many people around her.

96
00:09:37,960 --> 00:09:40,424
You're one of them.

97
00:09:46,358 --> 00:09:47,904
Nice.

98
00:09:47,904 --> 00:09:50,006
Give me a second.

99
00:10:01,298 --> 00:10:02,646
What's that?

100
00:10:14,206 --> 00:10:16,187
It's couple's nails.

101
00:10:18,039 --> 00:10:22,850
I'll buy couple's rings for us.
Just wait.

102
00:10:30,344 --> 00:10:32,482
How are you feeling?

103
00:10:32,482 --> 00:10:35,618
Good.
Are you going somewhere?

104
00:10:35,618 --> 00:10:37,693
Yeah.
It won't take long.

105
00:10:47,417 --> 00:10:49,898
Do you love that man?

106
00:10:52,182 --> 00:10:57,539
He can make you become a human,
but he might be a hunter.

107
00:10:57,539 --> 00:11:01,814
Since you know how to assimilate with humans

108
00:11:01,815 --> 00:11:06,089
you don't have to risk your life like this.

109
00:11:06,816 --> 00:11:10,695
And that might put other
Gumihos into danger.

110
00:11:29,901 --> 00:11:33,442
<i>I'm sorry.
Good bye.

111
00:11:34,783 --> 00:11:37,140
Let's go.

112
00:11:58,155 --> 00:12:02,414
You're such a bad brat.
You made me stay up all night.

113
00:12:19,074 --> 00:12:22,022
Hey, don't be sloppy!

114
00:12:25,517 --> 00:12:27,899
What's wrong, Kay?
There are customers here.

115
00:12:45,823 --> 00:12:49,507
Don't worry, Bunny.
Kay must be in a bad mood today.

116
00:12:52,242 --> 00:12:53,303
Okay.

117
00:12:53,303 --> 00:12:56,597
Smile.
Smile!

118
00:12:56,597 --> 00:12:58,753
Stop.

119
00:12:58,753 --> 00:13:02,106
- You're like a goldfish.
- Dude!

120
00:13:02,106 --> 00:13:06,226
What's wrong with me?

121
00:13:26,358 --> 00:13:28,155
Bye.

122
00:13:50,540 --> 00:13:54,745
That man extorted you of your money,
didn't he?

123
00:13:56,661 --> 00:13:58,142
You saw it?

124
00:13:58,142 --> 00:14:00,958
Are you smiling?

125
00:14:00,958 --> 00:14:05,159
Actually, I saw him at the
restaurant the other day.

126
00:14:10,914 --> 00:14:13,230
You shouldn't be smiling like this now.

127
00:14:15,240 --> 00:14:16,550
I'm sorry.

128
00:14:16,550 --> 00:14:19,596
I've been working at a mall and
dealing with customers for a long time.

129
00:14:19,597 --> 00:14:22,642
Smiling became my habit.

130
00:14:24,936 --> 00:14:28,584
Look. Do you see these
red lines on your nail?

131
00:14:28,584 --> 00:14:32,261
This means that you're depressed.

132
00:14:32,261 --> 00:14:39,532
You might not feel it but your mind is
suffering because of that damn man.

133
00:14:44,163 --> 00:14:47,612
You need to stay away from
that bastard right now.

134
00:14:47,612 --> 00:14:51,722
If you can't do it, I'll help you.

135
00:15:07,866 --> 00:15:10,449
Who is this little brat?

136
00:15:11,706 --> 00:15:14,868
Don't contact Mi Ae every again.

137
00:15:14,868 --> 00:15:17,255
Don't even come near her.

138
00:15:19,028 --> 00:15:21,304
What is this load of bollocks?

139
00:15:21,304 --> 00:15:24,674
You think she likes you because
she doesn't get mad at you?

140
00:15:24,674 --> 00:15:26,334
Hey.

141
00:15:26,334 --> 00:15:31,654
You don't know who you're messing with.

142
00:15:31,654 --> 00:15:35,266
I'm done talking.
Bye.

143
00:15:37,208 --> 00:15:39,957
Excuse me.
You can't do this here.

144
00:15:45,132 --> 00:15:48,860
If you bother her one more
time, I'm not letting it go.

145
00:15:48,860 --> 00:15:51,057
Don't forget.

146
00:15:51,057 --> 00:15:53,190
Let's go.

147
00:16:05,728 --> 00:16:08,692
You're so brave. Weren't you scared?

148
00:16:08,692 --> 00:16:13,520
You should've called me.

149
00:16:13,520 --> 00:16:16,715
Why?
So you could help me?

150
00:16:16,715 --> 00:16:20,090
No. So I can tell Kay to help you.

151
00:16:23,021 --> 00:16:26,784
Anyway, people like him should get punished.

152
00:16:26,784 --> 00:16:29,788
Why does he exploit people like her?

153
00:16:29,788 --> 00:16:33,215
I told you she's changed.

154
00:16:35,269 --> 00:16:39,137
By the way, did you read a fan
fiction that our fans wrote?

155
00:16:39,137 --> 00:16:40,146
A fan fiction?

156
00:16:40,146 --> 00:16:44,548
It's so funny. You and I are dating
and Kay has a crush on you.

157
00:16:44,548 --> 00:16:46,810
So it becomes a love triangle.

158
00:16:46,810 --> 00:16:49,460
It's kind of erotic too.

159
00:16:51,218 --> 00:16:55,210
Kay bullies me all the time.
That's not possible.

160
00:16:55,210 --> 00:17:00,368
You know, if somebody likes you, they want
to bully you so they can get your attention.

161
00:17:00,368 --> 00:17:02,332
It's like that.

162
00:17:03,668 --> 00:17:05,569
Not possible.

163
00:17:26,961 --> 00:17:29,076
You read this fan fiction, too?

164
00:17:29,076 --> 00:17:31,099
Isn't it so funny?

165
00:17:31,099 --> 00:17:33,501
Hey, Jin.

166
00:17:33,501 --> 00:17:39,030
The way I treated Beanie...
was that weird?

167
00:17:43,010 --> 00:17:44,424
Of course, it was.

168
00:17:44,424 --> 00:17:48,377
What's the matter with you?
You acted like you have feelings for him.

169
00:17:48,379 --> 00:17:50,143
Where is he now?

170
00:17:50,143 --> 00:17:52,782
In the hospital.

171
00:18:01,249 --> 00:18:03,534
What's wrong?

172
00:18:03,534 --> 00:18:06,027
Does he really have feelings
for Bunny?

173
00:18:13,210 --> 00:18:15,301
- Bunny.
- There you are.

174
00:18:15,301 --> 00:18:16,580
Are you going somewhere?

175
00:18:16,580 --> 00:18:19,501
He's about to get examined.
Have a seat while you wait.

176
00:18:19,501 --> 00:18:21,042
Okay.

177
00:21:05,761 --> 00:21:07,706
Are you not done yet?

178
00:21:12,952 --> 00:21:15,090
Bunny, is everything okay?

179
00:21:15,090 --> 00:21:17,217
What?
Yeah.

180
00:21:17,217 --> 00:21:20,100
Why are you zoning out?

181
00:21:20,100 --> 00:21:26,050
Jin, what's the relationship
between Boss and Alex?

182
00:21:27,142 --> 00:21:28,418
Relationship?

183
00:21:28,418 --> 00:21:30,235
Employer and employee.

184
00:21:30,235 --> 00:21:34,211
No, no. How did they meet?

185
00:21:34,211 --> 00:21:39,765
I heard that when Alex was
wandering after he quit his job...

186
00:21:39,765 --> 00:21:43,422
Boss was the one who led him
to become a nail artist.

187
00:21:44,058 --> 00:21:46,020
So she was like his savior.

188
00:21:46,020 --> 00:21:50,236
Come to think of it, everyone
here had hard times.

189
00:21:50,236 --> 00:21:53,523
The same goes for me and Kay.

190
00:21:53,523 --> 00:21:57,922
By the way, I'm going to Club with Ji Soo.
Do you want to come with us?

191
00:21:57,922 --> 00:22:00,356
I'm not interested in that.

192
00:22:00,356 --> 00:22:03,696
You're only interested in Alex,
aren't you?

193
00:22:03,696 --> 00:22:05,876
You're a stalker.

194
00:22:05,876 --> 00:22:07,500
Dude!

195
00:22:19,590 --> 00:22:21,226
Didn't you go home yet?

196
00:22:22,297 --> 00:22:26,139
No.
I left something here.

197
00:22:30,833 --> 00:22:32,989
- Bye, Kay.
- Bye.

198
00:22:39,494 --> 00:22:42,551
Why do I have to lie because
of Beanie?

199
00:22:43,385 --> 00:22:45,412
Excuse me.

200
00:22:47,142 --> 00:22:48,719
Is Bunny here?

201
00:23:03,163 --> 00:23:06,592
I'm not going to worry about him.

202
00:23:25,562 --> 00:23:27,920
<i>Beanie.

203
00:23:52,677 --> 00:23:54,206
Let go.

204
00:23:54,206 --> 00:23:55,627
Where are we going?

205
00:23:55,627 --> 00:24:01,569
Why? You turned on me in a fury
the other day. Do it again.

206
00:24:06,794 --> 00:24:10,684
You think I can't do that?

207
00:24:13,318 --> 00:24:16,600
Just wait there.

208
00:24:25,858 --> 00:24:28,369
Guys, look at this brat.

209
00:24:32,629 --> 00:24:34,353
Get him!

210
00:25:11,904 --> 00:25:13,991
My phone.

211
00:25:20,634 --> 00:25:23,222
There are you are, you little rat.

212
00:25:40,788 --> 00:25:42,081
What are you doing here?

213
00:25:52,107 --> 00:25:53,901
How are you feeling?

214
00:25:55,276 --> 00:25:57,570
I'm okay.

215
00:25:59,484 --> 00:26:02,748
What's wrong?
Are you worried about something?

216
00:26:02,748 --> 00:26:07,038
You're not the type of guy who'd come
visit me just because you're worried.

217
00:26:13,996 --> 00:26:15,477
- Kay.
- Yes?

218
00:26:20,136 --> 00:26:22,430
Check my hand.

219
00:26:22,430 --> 00:26:25,067
You said you know everything
if you check a hand.

220
00:26:25,067 --> 00:26:29,668
Check if I'm crazy or mentally ill.

221
00:26:31,179 --> 00:26:33,332
What the heck are you saying?

222
00:26:39,253 --> 00:26:41,002
Jin.

223
00:26:41,002 --> 00:26:42,532
What's up?

224
00:26:42,532 --> 00:26:46,132
Kay, did you see Bunny?

225
00:26:46,132 --> 00:26:47,799
Why are you asking me that?

226
00:26:47,989 --> 00:26:50,411
You were the last
person at the salon.

227
00:26:50,411 --> 00:26:56,323
Ji Soo wanted Bunny to join us so I called
him but he's not answering the phone.

228
00:26:56,323 --> 00:26:59,605
Ask him if Bunny said where
he's going.

229
00:27:01,051 --> 00:27:03,859
Kay, did he say where
he was going?

230
00:27:03,859 --> 00:27:08,368
No. He was with this man...

231
00:27:08,368 --> 00:27:10,480
Is Bunny here?

232
00:27:10,480 --> 00:27:13,049
I have to go.

233
00:27:15,120 --> 00:27:16,820
Kay.

234
00:27:28,578 --> 00:27:30,512
You know a man with a
scar on his face, don't you?

235
00:27:30,512 --> 00:27:33,101
Where's that bastard?

236
00:27:35,605 --> 00:27:38,214
You little brat.

237
00:27:38,214 --> 00:27:41,473
What do you care about the
business between me and Mi Ae?

238
00:27:41,473 --> 00:27:45,711
Hey, do you really have
a crush on her?

239
00:27:48,104 --> 00:27:51,365
- He looks like a girl.
- Why would two girls be together?

240
00:27:51,365 --> 00:27:56,089
Yeah, he looks so pretty.
Why don't we strip him and check?

241
00:27:57,153 --> 00:28:00,348
Let me go.
Stop!

242
00:28:00,348 --> 00:28:04,311
You shouldn't have been rude to me.

243
00:28:08,313 --> 00:28:10,768
What the hell?

244
00:28:11,569 --> 00:28:15,206
- Kay.
- If you want to keep walking with two legs...

245
00:28:15,206 --> 00:28:19,832
Bring my shoe back and
get out of here.

246
00:28:20,788 --> 00:28:22,778
Look at that son of a bitch.

247
00:28:23,040 --> 00:28:25,353
Go kill him.

248
00:28:34,409 --> 00:28:36,000
Watch out.

249
00:28:46,458 --> 00:28:48,014
Kay.

250
00:28:54,700 --> 00:28:59,150
Bro, your arm is bleeding.

251
00:29:04,205 --> 00:29:07,387
Don't you have a brain?

252
00:29:07,387 --> 00:29:09,432
You don't know how to think?

253
00:29:09,432 --> 00:29:12,513
Why did you follow him?
You don't even know how to fight.

254
00:29:13,793 --> 00:29:15,493
Did you want to kill yourself
or something?

255
00:29:15,493 --> 00:29:21,984
I didn't follow him. He forced me.
What was I supposed to do?

256
00:29:21,984 --> 00:29:23,249
You are...

257
00:29:26,714 --> 00:29:29,006
You should've taken your
phone with you.

258
00:29:30,047 --> 00:29:34,855
I didn't have time for that.

259
00:29:42,793 --> 00:29:46,175
Bunny.
Are you okay?

260
00:29:46,175 --> 00:29:49,455
- Did you get hurt?
- Yeo... Ki Joo.

261
00:29:49,455 --> 00:29:51,895
You must be so shocked.

262
00:29:55,892 --> 00:30:01,242
If this happens one more time,
I'm not going to worry about it.

263
00:30:01,242 --> 00:30:04,988
- Understand?
- What's wrong with you? He's shocked.

264
00:30:31,141 --> 00:30:33,731
Damn bastard.

265
00:30:33,731 --> 00:30:36,829
You made me so worried.

266
00:30:49,591 --> 00:30:51,006
Are you Jeong Mi Ae?

267
00:30:56,986 --> 00:31:00,775
I see.
Bunny must have been so scared.

268
00:31:02,816 --> 00:31:05,928
- Is that all?
- Pardon?

269
00:31:05,928 --> 00:31:08,936
I thought you would worry
about him more than that.

270
00:31:11,258 --> 00:31:14,457
I feel so sorry.
He was in trouble because of me.

271
00:31:14,457 --> 00:31:19,900
You can report it to the police or whatever.
It's not my business.

272
00:31:21,982 --> 00:31:24,078
I have to leave now.

273
00:31:25,197 --> 00:31:27,901
Are you okay?

274
00:31:27,901 --> 00:31:31,059
- Yes.
- Is she a psycho by any chance?

275
00:31:31,059 --> 00:31:33,519
I mean a psychopath.

276
00:31:34,733 --> 00:31:36,976
She doesn't seem like one.

277
00:31:36,976 --> 00:31:42,468
Kay, you think you can
find Hyung Shik again?

278
00:31:44,661 --> 00:31:46,986
Why? Are you really
reporting him to the police?

279
00:31:46,986 --> 00:31:50,359
No, I'm not.

280
00:31:50,359 --> 00:31:54,092
Bunny, you said they've known
each other for a long time, right?

281
00:31:55,274 --> 00:31:58,955
Yes. She told me she has
known him since high school.

282
00:31:59,929 --> 00:32:04,392
He might know what her problem is.

283
00:32:04,392 --> 00:32:05,656
What do you mean?

284
00:32:05,656 --> 00:32:10,257
She doesn't feel emotions and she
suffers from severe migraines.

285
00:32:11,485 --> 00:32:15,912
I think she has alexithymia.

286
00:32:17,874 --> 00:32:19,824
Alexi...what?

287
00:32:19,824 --> 00:32:21,573
Alexithymia.

288
00:32:21,573 --> 00:32:25,154
It's like you can't feel
nor express your emotions.

289
00:32:25,154 --> 00:32:28,582
Of course, it's impossible to
notice how other people feel.

290
00:32:28,582 --> 00:32:32,277
Then what's going to happen to her?

291
00:32:32,277 --> 00:32:36,667
It might lead her to premature dementia
in the worst case.

292
00:32:36,667 --> 00:32:39,639
Isn't that a serious disease?

293
00:32:39,639 --> 00:32:42,981
It might be because of a
congenital handicap.

294
00:32:42,981 --> 00:32:46,410
But I think it's because
of some kind of trauma.

295
00:32:46,410 --> 00:32:52,087
It's like her old wounds from the
past blocked her emotions.

296
00:32:53,111 --> 00:32:57,154
So to cure her disease, we need
to dig up her old wounds.

297
00:32:57,154 --> 00:33:01,357
Wouldn't Hyung Shik know
about something?

298
00:33:10,405 --> 00:33:12,478
Park Hyung Shik.

299
00:33:12,478 --> 00:33:15,143
Can I talk to you for a second?

300
00:33:27,743 --> 00:33:31,358
What?
Trauma?

301
00:33:31,358 --> 00:33:34,706
I'm too stupid to know what
that word means.

302
00:33:34,706 --> 00:33:38,139
But I know that bitch isn't
a normal girl.

303
00:33:38,139 --> 00:33:41,195
Do you know why she has
to pay money to me?

304
00:33:44,568 --> 00:33:47,747
<i>Her classmates didn't talk to her since
she always looked depressed.

305
00:33:47,747 --> 00:33:50,340
<i>She ended up getting bullied.

306
00:33:50,341 --> 00:33:55,225
<i>I figured out that she was
raped by her stepfather.

307
00:33:55,225 --> 00:33:59,296
She was going to kill herself.

308
00:33:59,296 --> 00:34:02,561
She was 16 years old at the time.

309
00:34:02,561 --> 00:34:05,387
Since that incident,
she has been like that.

310
00:34:06,981 --> 00:34:13,111
She doesn't get mad nor sad,
like a robot.

311
00:34:14,007 --> 00:34:17,299
Well... after she got a job at the
mall, she smiles all the time.

312
00:34:19,052 --> 00:34:25,020
But do you know why her smile
drives me nuts?

313
00:34:25,020 --> 00:34:33,788
But you've been watching her because you
have feelings for her. Am I wrong?

314
00:34:35,492 --> 00:34:40,978
I followed her to get money from
her because she owed me her life.

315
00:34:45,202 --> 00:34:46,724
Let's go.

316
00:34:46,724 --> 00:34:49,609
Thank you, anyway.

317
00:34:51,206 --> 00:34:52,691
Hey.

318
00:34:56,716 --> 00:34:58,843
I know I'm a hopeless man.

319
00:35:01,034 --> 00:35:03,798
But please help that girl.

320
00:35:03,798 --> 00:35:06,936
If you let her go, she might
try to kill herself again.

321
00:35:09,077 --> 00:35:13,384
I never thought she had that kind of past.

322
00:35:14,920 --> 00:35:16,408
What do we do now?

323
00:35:16,408 --> 00:35:19,403
The best thing is get her to see a doctor.

324
00:35:20,066 --> 00:35:24,113
But she needs to be able to
admit her problem first.

325
00:35:24,113 --> 00:35:26,920
It won't be easy.

326
00:35:26,920 --> 00:35:33,069
You have to use a little mysticism for this kind of problem.

327
00:35:33,069 --> 00:35:40,248
I mean I assume that people like
her are possessed by a ghost.

328
00:35:40,248 --> 00:35:45,945
So she was probably possessed
by a furious ghost.

329
00:35:45,945 --> 00:35:49,473
- You're talking nonsense again.
- No, I'm not.

330
00:35:49,473 --> 00:35:57,386
The best cure for this is to use a
person with pure and honest energy.

331
00:35:57,386 --> 00:36:03,579
The best treatment for her is a person
who has clear and fresh energy.

332
00:36:03,579 --> 00:36:06,742
Whatever. Can you make us pork cutlet please?

333
00:36:06,742 --> 00:36:09,697
Dude, it's real.

334
00:36:11,642 --> 00:36:13,726
Hey, make one pork cutlet.

335
00:36:13,726 --> 00:36:16,380
He might be right.

336
00:36:16,380 --> 00:36:18,895
Don't say that.

337
00:36:19,832 --> 00:36:21,042
I'm not sure about everything he said,

338
00:36:21,042 --> 00:36:27,310
but a person's heart can be
healed by another person.

339
00:36:29,394 --> 00:36:35,105
Bunny, you started this.
So you need to help her until the end.

340
00:36:35,105 --> 00:36:39,442
- Can you do that?
- Yes.

341
00:36:40,841 --> 00:36:44,500
Good job today.
I'll see you tomorrow.

342
00:36:44,500 --> 00:36:47,482
- Bye.
- Bye.

343
00:36:51,168 --> 00:36:52,806
Let's go.

344
00:36:56,090 --> 00:36:57,851
You go ahead, Jin.

345
00:37:01,420 --> 00:37:03,823
I'm going to go see Ji Soo.

346
00:37:06,532 --> 00:37:08,229
Kay.

347
00:37:10,166 --> 00:37:12,382
You walk so fast.

348
00:37:13,273 --> 00:37:14,914
Your legs are short.

349
00:37:20,231 --> 00:37:23,906
Thank you for what you did on that day.

350
00:37:23,906 --> 00:37:27,767
I didn't get to say thank you so...

351
00:37:30,853 --> 00:37:32,870
Is your arm okay now?

352
00:37:33,827 --> 00:37:36,666
Why do you care?

353
00:37:38,063 --> 00:37:41,635
How can I not care?
Your arm got hurt because of me.

354
00:37:44,253 --> 00:37:47,843
- That's it?
- What?

355
00:37:50,306 --> 00:37:53,047
Never mind.

356
00:37:56,425 --> 00:37:59,715
You think I can help her?

357
00:38:02,246 --> 00:38:04,329
I'm talking about Mi Ae.

358
00:38:04,329 --> 00:38:08,228
I told the boss I can help her.

359
00:38:08,228 --> 00:38:13,743
But I honestly don't know.
Maybe I should never have gotten involved.

360
00:38:15,565 --> 00:38:17,966
There's no such thing as a single
right answer in the real world.

361
00:38:20,051 --> 00:38:25,894
Pick anything and make that your answer.
Then that becomes the answer.

362
00:38:25,894 --> 00:38:30,309
You're so cool, Kay.

363
00:38:30,309 --> 00:38:33,608
I understand why you have fans.

364
00:38:36,527 --> 00:38:38,504
Are you crazy?

365
00:38:43,246 --> 00:38:46,642
Kay, where are you going?

366
00:38:51,371 --> 00:38:56,191
<i>Diamond Nail

367
00:39:13,773 --> 00:39:15,342
Hello.

368
00:39:15,342 --> 00:39:17,331
Bunny.

369
00:39:17,331 --> 00:39:19,688
Are you okay now?

370
00:39:19,688 --> 00:39:23,209
Yeah.

371
00:39:23,209 --> 00:39:26,789
But did you really worry about me?

372
00:39:29,586 --> 00:39:30,880
Well...

373
00:39:30,880 --> 00:39:34,952
Do you want to go out on a date with me?

374
00:39:34,952 --> 00:39:37,516
You're free on this weekend?

375
00:40:05,759 --> 00:40:07,729
What?

376
00:40:07,729 --> 00:40:12,869
You're mad, aren't you?
Don't you get mad?

377
00:40:42,418 --> 00:40:45,897
I'm sorry.
I acted a little crazy, didn't I?

378
00:40:49,846 --> 00:40:56,227
I wanted to see the real you,
not that smile today.

379
00:40:57,480 --> 00:40:59,024
But it's not working.

380
00:41:01,653 --> 00:41:08,529
Alex said you're sick now.

381
00:41:08,529 --> 00:41:11,014
Because of the deep wound in your heart,

382
00:41:11,014 --> 00:41:13,962
this thing that connects your
brain and heart is broken.

383
00:41:13,962 --> 00:41:19,754
So you don't feel mad, happy, and sad.
You don't try to feel those emotions.

384
00:41:23,021 --> 00:41:29,182
But I thought if I tried,
you would open your mind.

385
00:41:29,182 --> 00:41:32,937
Maybe I was wrong.

386
00:41:35,858 --> 00:41:41,965
Bunny, if you're done talking, can I go home?

387
00:41:43,257 --> 00:41:45,478
I'm tired.

388
00:41:46,945 --> 00:41:49,640
Sure.

389
00:42:18,518 --> 00:42:20,163
Hey Bunny,
How did it go?

390
00:42:20,163 --> 00:42:21,888
It didn't work at all.

391
00:42:21,888 --> 00:42:25,054
Maybe I can't help her.

392
00:42:25,054 --> 00:42:28,476
Don't be disappointed.
It's hard to cure that disease.

393
00:42:28,476 --> 00:42:32,189
Is there really no way to fix her?

394
00:42:40,105 --> 00:42:42,367
Come in.

395
00:42:44,706 --> 00:42:48,349
Bunny, we could've practiced in class.

396
00:42:48,349 --> 00:42:51,467
We should practice like it's for real.

397
00:42:51,467 --> 00:42:53,481
Have a seat.

398
00:42:58,210 --> 00:43:00,177
What is he trying to do?

399
00:43:00,177 --> 00:43:04,849
He's trying to figure it out.
She's his first customer.

400
00:43:15,030 --> 00:43:17,980
We're supposed to practice glue-on nails, right?

401
00:43:23,101 --> 00:43:25,716
I'll begin.

402
00:43:33,961 --> 00:43:35,566
Do you like this place?

403
00:43:37,296 --> 00:43:38,522
I like it.

404
00:43:38,522 --> 00:43:42,546
Do you know why this place is called Paris?

405
00:43:45,851 --> 00:43:50,465
When I first started working
here, my boss told me why.

406
00:43:50,465 --> 00:43:53,446
Just like the myth of Paris,

407
00:43:53,446 --> 00:44:00,674
our goal is to comfort and
cure customers' wounds.

408
00:44:03,693 --> 00:44:12,337
To help our customers, my co-workers
sometimes have to fight and get hurt.

409
00:44:13,530 --> 00:44:18,541
They even get cursed at.

410
00:44:19,989 --> 00:44:25,356
Seeing all that, I realized what
my boss was talking about.

411
00:44:28,365 --> 00:44:33,768
So this time, I'm going to give it a shot.

412
00:44:35,820 --> 00:44:37,896
You're my customer.

413
00:44:49,231 --> 00:44:52,431
I don't have a mother.

414
00:44:53,349 --> 00:44:57,319
On top of that, I was from the countryside.

415
00:44:57,319 --> 00:45:05,702
So people made fun of me.
They even bullied me.

416
00:45:08,563 --> 00:45:13,917
So as time went by and I got
older, I never left my house.

417
00:45:15,753 --> 00:45:18,251
I got a job that I could do alone.

418
00:45:19,423 --> 00:45:22,868
And I had only one friend, my roommate.

419
00:45:22,868 --> 00:45:28,238
I thought I was happy at that time.

420
00:45:28,238 --> 00:45:32,199
But I wasn't.

421
00:45:33,519 --> 00:45:35,987
Because of other people,

422
00:45:35,987 --> 00:45:40,411
Sometimes, I feel happy, sad...

423
00:45:40,411 --> 00:45:45,271
mad, angry...

424
00:45:45,271 --> 00:45:48,307
annoyed, disappointed...

425
00:45:48,307 --> 00:45:51,687
and scared.

426
00:45:51,687 --> 00:45:56,047
Without having those feelings,
I wasn't really living a life.

427
00:45:58,614 --> 00:46:08,247
So I decided not to let
Mi Ae's life to be my old life.

428
00:46:10,536 --> 00:46:12,264
Bunny.

429
00:46:12,938 --> 00:46:19,467
I heard from Hyung Shik what had
happened to you in the past.

430
00:46:19,467 --> 00:46:23,668
I understand why you closed your mind.

431
00:46:23,668 --> 00:46:32,060
But I really want to teach you how
it is to feel those emotions.

432
00:46:34,519 --> 00:46:38,856
Just like this, I'm never going to let you go.

433
00:46:48,132 --> 00:46:52,294
So?
Do you like your the diamond glue-on nails?

434
00:46:58,403 --> 00:47:00,595
Mi Ae...

435
00:47:07,175 --> 00:47:09,235
What's wrong with me?

436
00:47:09,235 --> 00:47:12,671
Your body does feel.

437
00:47:15,713 --> 00:47:22,637
Even if your brain doesn't feel
anything, your body and heart can feel.

438
00:47:24,324 --> 00:47:29,735
They feel hurt and sad.

439
00:47:33,815 --> 00:47:35,942
Help me, Bunny.

440
00:47:35,942 --> 00:47:40,383
What do I do now?

441
00:48:07,030 --> 00:48:08,822
So what's she going to do?

442
00:48:08,822 --> 00:48:12,047
I went to see a doctor with her and
got her admitted for treatment.

443
00:48:12,047 --> 00:48:15,855
The doctor said it's not
an easy disease to cure.

444
00:48:15,855 --> 00:48:19,871
But since Mi Ae has the will to get better,
she's going to be fine.

445
00:48:19,871 --> 00:48:24,303
Wow!  You did a great job
with your first customer.

446
00:48:24,303 --> 00:48:27,866
I guess so.

447
00:48:27,866 --> 00:48:32,464
Anyways, congratulations for becoming a
member of our Paris family officially.

448
00:48:40,387 --> 00:48:42,217
Hello.

449
00:48:42,217 --> 00:48:46,039
Sure.
I'll go now.

450
00:48:46,039 --> 00:48:48,034
Bye.

451
00:48:50,516 --> 00:48:52,702
I'm sorry.
I have to leave now.

452
00:48:52,702 --> 00:48:55,592
Who was it?
Your girlfriend?

453
00:48:56,885 --> 00:48:58,924
Have fun, guys.

454
00:49:10,724 --> 00:49:14,456
- I'm leaving too.
- Kay.

455
00:49:14,456 --> 00:49:16,298
Kay!

456
00:49:18,003 --> 00:49:20,892
What's wrong with them?

457
00:49:20,892 --> 00:49:24,516
Bunny, how about we go to a club?

458
00:49:24,516 --> 00:49:26,351
No.

459
00:49:26,351 --> 00:49:30,791
Bunny, I couldn't even enjoy the club
last time because of you.

460
00:49:54,536 --> 00:49:58,088
I knew it would be like this.

461
00:50:10,171 --> 00:50:12,027
Where did she go?

462
00:50:12,907 --> 00:50:14,373
Call her.

463
00:50:17,983 --> 00:50:20,679
Yeo Joo, where are you?

464
00:50:20,679 --> 00:50:25,016
- I'm going home.
- Why didn't you tell us?

465
00:50:25,016 --> 00:50:28,444
You guy were having fun without me anyway.

466
00:50:28,444 --> 00:50:31,096
Just have fun, guys.

467
00:50:31,096 --> 00:50:33,201
Bye.

468
00:50:35,570 --> 00:50:38,327
What's wrong with her?

469
00:50:43,442 --> 00:50:44,464
Yeo Joo...

470
00:50:49,251 --> 00:50:50,746
Jin!

471
00:50:52,474 --> 00:50:55,576
- I didn't look! I swear!
- What?

472
00:50:56,911 --> 00:50:59,585
Where am I anyway?

473
00:50:59,585 --> 00:51:01,891
Should I get a taxi?

474
00:51:08,652 --> 00:51:10,752
That car...

475
00:51:56,516 --> 00:51:58,689
- What are you doing?
- What the heck?

476
00:52:00,280 --> 00:52:01,933
Are you okay, Jin?

477
00:52:19,017 --> 00:52:21,409
You're such a mess.

478
00:52:21,409 --> 00:52:24,842
Why are you being so nice to me?

479
00:52:27,367 --> 00:52:30,709
What's wrong Yeo Joo?
Are you crying?

480
00:52:30,709 --> 00:52:32,529
Ji Soo, I'm so sad.

481
00:52:34,109 --> 00:52:37,079
I'm so sad, Ji soo!