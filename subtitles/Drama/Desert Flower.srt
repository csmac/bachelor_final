1
00:00:59,760 --> 00:01:04,914
DESERT FLOWER

2
00:02:12,000 --> 00:02:13,149
Waris!

3
00:02:14,800 --> 00:02:16,119
Old Man!

4
00:02:26,640 --> 00:02:28,358
Mom says, we have to move on.

5
00:02:29,120 --> 00:02:32,351
Her name is Fattuma. - You can't
call a sheep like Allah's daughter.

6
00:02:32,800 --> 00:02:34,358
Says who?

7
00:04:26,160 --> 00:04:29,357
Waris,
I don't want anything to separate us.

8
00:04:30,200 --> 00:04:32,839
I'll never leave you alone!
Promise!

9
00:05:13,480 --> 00:05:16,392
LONDON, YEARS LATER

10
00:07:47,880 --> 00:07:51,589
I have nowhere else to go!
I can't go back to Somalia!

11
00:37:14,760 --> 00:37:17,354
Aren't you ashamed?
Showing this white man your body.

12
00:37:17,800 --> 00:37:19,438
Our tradition is of no concern to them.

13
00:37:30,960 --> 00:37:33,997
If you change what you are,
then you are betraying your parents,

14
00:37:34,440 --> 00:37:36,112
your people and your heritage.

15
00:37:36,560 --> 00:37:40,712
Does your mother know what you are
about to do? Shame on you!

16
00:38:40,280 --> 00:38:43,113
Mommy, I am bleeding down there.

17
00:38:45,160 --> 00:38:48,311
Good, it means

18
00:38:48,760 --> 00:38:51,115
you are a woman now!

19
00:38:51,880 --> 00:38:53,552
It's terribly painful.

20
00:38:54,480 --> 00:38:56,596
All pain will pass.

21
00:39:14,760 --> 00:39:18,639
Waris, this is Galool Mohammed.

22
00:39:29,640 --> 00:39:33,076
Come closer my dear, come closer.

23
00:39:33,520 --> 00:39:35,397
Do you like my watch?

24
00:39:38,440 --> 00:39:40,317
You will marry him

25
00:39:40,760 --> 00:39:43,832
to be his fourth wife.

26
00:39:44,280 --> 00:39:47,636
I arranged the wedding for tomorrow.

27
00:40:20,920 --> 00:40:22,797
Mom, I will not marry him!

28
00:40:23,240 --> 00:40:27,392
Galool Mohammed paid a lot for you.
You can't say no.

29
00:41:22,560 --> 00:41:25,438
Where are you going?
, To grandmother.

30
00:41:26,120 --> 00:41:30,079
Mogadishu! It's far away!
I come with you!

31
00:41:33,520 --> 00:41:36,273
No! They need you here, little brother!

32
00:41:36,720 --> 00:41:40,554
Don't worry!
In the city I earn lots of money

33
00:41:41,000 --> 00:41:43,514
and then I come back to get you!

34
00:44:43,680 --> 00:44:44,669
Mommy.

35
00:45:50,400 --> 00:45:51,628
Mogadishu?

36
00:45:52,080 --> 00:45:53,593
Yes. Hurry up.

37
00:46:22,880 --> 00:46:24,393
Shut up!

38
00:46:27,640 --> 00:46:28,595
Mommy!

39
00:46:31,080 --> 00:46:32,399
I said shut up!

40
00:47:05,800 --> 00:47:07,597
Where is the mosque?

41
00:47:08,800 --> 00:47:09,994
Over there.

42
00:47:56,960 --> 00:48:00,794
Mother, see what comes of
living with goats and sheep!

43
00:48:02,240 --> 00:48:05,277
We can't afford to keep her.
She has to go back home!

44
00:48:05,840 --> 00:48:10,118
A girl who dares the long journey
through the desert has a good reason.

45
00:48:10,560 --> 00:48:14,269
She blamed our family!
- Enough! Go back to work!

46
00:48:19,720 --> 00:48:21,631
A miracle you survived!

47
00:48:22,080 --> 00:48:23,877
It was Allah's will.

48
00:48:25,720 --> 00:48:29,759
You have the same lips as your mother.
I always called her Afyareh.

49
00:48:30,200 --> 00:48:32,794
Grandma, why have we never seen you?

50
00:48:33,240 --> 00:48:37,313
Because your mother ran away
through the desert to marry your father.

51
00:48:39,480 --> 00:48:44,508
A man we did not approve
because he was nothing but a nomad.

52
00:48:45,840 --> 00:48:49,628
I wonder if it made her happy.
All that pain must be worth something.

