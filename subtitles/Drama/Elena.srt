1
00:00:33,000 --> 00:00:36,000
[street noises fade in]

2
00:00:47,500 --> 00:00:49,833
[PETRA] Elena.

3
00:00:49,833 --> 00:00:52,833
I had a dream about you last
night.

4
00:00:54,433 --> 00:00:55,933
You were soft.

5
00:00:55,933 --> 00:00:57,200
Walking the streets of New York

6
00:00:57,200 --> 00:01:00,233
in a silk blouse.

7
00:01:10,267 --> 00:01:13,067
I try to get close...

8
00:01:13,067 --> 00:01:15,933
touch you...

9
00:01:15,933 --> 00:01:18,933
to feel your smell.

10
00:01:20,800 --> 00:01:23,433
But when I look, you're on top
of a wall,

11
00:01:23,433 --> 00:01:26,467
entangled in a mesh of electric
wires.

12
00:01:29,433 --> 00:01:31,467
I look again

13
00:01:31,467 --> 00:01:34,467
and it's me who's on top of the
wall.

14
00:01:35,333 --> 00:01:37,400
I touch the wires,

15
00:01:37,400 --> 00:01:40,400
trying to get a shock.

16
00:01:41,133 --> 00:01:43,500
and I fall off the wall

17
00:01:43,500 --> 00:01:46,067
from very high

18
00:01:46,067 --> 00:01:48,967
and die.

19
00:01:59,067 --> 00:02:02,067
*Dedicated to the One I Love

20
00:03:29,733 --> 00:03:30,900
[PETRA] Our mother always told
me

21
00:03:30,900 --> 00:03:33,500
that I could live anywhere in
the world.

22
00:03:33,500 --> 00:03:35,833
Except New York.

23
00:03:35,833 --> 00:03:38,067
That I could choose any
profession

24
00:03:38,067 --> 00:03:40,967
except acting.

25
00:03:43,700 --> 00:03:46,333
On September 4th 2003,

26
00:03:46,333 --> 00:03:47,533
I started studying Theatre

27
00:03:47,533 --> 00:03:50,533
at Columbia University.

28
00:03:54,633 --> 00:03:57,600
They wanted me to forget you,
Elena.

29
00:04:08,367 --> 00:04:10,133
But I returned to New York,

30
00:04:10,133 --> 00:04:12,133
hoping to find you in the
streets.

31
00:04:17,400 --> 00:04:20,367
I bring with me everything you
left in Brazil.

32
00:04:21,367 --> 00:04:24,467
Your videos, pictures,
diaries...

33
00:04:24,467 --> 00:04:26,333
letters on tape

34
00:04:26,333 --> 00:04:27,467
because you were always
embarrassed

35
00:04:27,467 --> 00:04:29,400
of your handwriting

36
00:04:29,400 --> 00:04:31,067
and preferred recording your
impressions

37
00:04:31,067 --> 00:04:34,067
of the days here to send us.

38
00:04:46,933 --> 00:04:48,833
Today I walked through the city

39
00:04:48,833 --> 00:04:51,367
listening to your voice

40
00:04:51,367 --> 00:04:54,733
and I identify so much with
your words

41
00:04:54,733 --> 00:04:57,733
that I start loosing myself in
you.

42
00:06:18,200 --> 00:06:19,600
[WOMAN] How old are you?
-20.

43
00:06:19,600 --> 00:06:21,167
[WOMAN] 20? And how tall are
you?

44
00:06:21,167 --> 00:06:23,067
five nine and a half.

45
00:06:23,067 --> 00:06:24,967
[WOMAN] You were born in Brazil?

46
00:06:24,967 --> 00:06:27,533
I was born in Brazil, and I've
been here

47
00:06:27,533 --> 00:06:30,767
almost a year and a half now.

48
00:06:30,767 --> 00:06:32,100
[WOMAN] What brought you to come

49
00:06:32,100 --> 00:06:33,667
to the United States?

50
00:06:33,667 --> 00:06:35,100
I was doing theatre there

51
00:06:35,100 --> 00:06:38,100
since I was 14 years old and
then...

52
00:06:39,867 --> 00:06:41,500
the possibilities in Brazil are
like,

53
00:06:41,500 --> 00:06:43,133
either you continue doing
theatre

54
00:06:43,133 --> 00:06:45,133
which, I mean, there are very
few productions,

55
00:06:45,867 --> 00:06:47,867
or you do soaps.

56
00:06:47,867 --> 00:06:49,900
I was invited to do one...

57
00:06:49,900 --> 00:06:51,833
I mean after a while I was in
theatre,

58
00:06:51,833 --> 00:06:54,967
but I don't really like it.

59
00:06:54,967 --> 00:06:56,367
[WOMAN] You wanted to do other
things?

60
00:06:56,367 --> 00:06:57,833
Films.

61
00:06:57,833 --> 00:07:00,833
And in Brazil, it's like,
almost none!

62
00:07:02,433 --> 00:07:06,233
Especially now that the system
changed there.

63
00:07:06,233 --> 00:07:10,067
It's like, 1 movie a year at
best.

64
00:07:10,067 --> 00:07:11,367
[WOMAN] Do your parents get you
involved

65
00:07:11,367 --> 00:07:12,833
in this business at all?

66
00:07:12,833 --> 00:07:15,800
No. My mom is a journalist and
sociologist,

67
00:07:17,633 --> 00:07:18,833
she writes for the newspaper
there

68
00:07:18,833 --> 00:07:21,333
and my father's a politician.

69
00:07:21,333 --> 00:07:25,067
He lives in Brazil as well.

70
00:07:25,067 --> 00:07:27,967
*soft music

71
00:07:36,833 --> 00:07:38,067
[PETRA] Actually,

72
00:07:38,067 --> 00:07:40,533
our father always said that we
inherited

73
00:07:40,533 --> 00:07:43,533
this dream of making films from
our mother.

74
00:07:45,367 --> 00:07:47,367
and in the middle of your
videotapes,

75
00:07:47,367 --> 00:07:49,367
I found this film that she
never showed me.

76
00:07:52,300 --> 00:07:55,300
It's a silent film where she
plays the lead.

77
00:07:56,367 --> 00:07:57,767
From the time she still dreamed

78
00:07:57,767 --> 00:07:59,533
of being a hollywood star

79
00:07:59,533 --> 00:08:02,500
and kissing Frank Sinatra.

80
00:08:02,500 --> 00:08:04,800
This made her feel like a woman.

81
00:08:04,800 --> 00:08:06,400
And helped her escape from a
world

82
00:08:06,400 --> 00:08:09,167
where she felt out of place...

83
00:08:09,167 --> 00:08:12,167
misunderstood.

84
00:08:14,800 --> 00:08:17,833
The daughter of a traditional
family from Minas Gerais,

85
00:08:17,833 --> 00:08:20,467
she could see no place for
herself

86
00:08:20,467 --> 00:08:22,533
except in marriage.

87
00:08:22,533 --> 00:08:25,533
A society lady.

88
00:08:27,967 --> 00:08:29,567
One day,

89
00:08:29,567 --> 00:08:32,567
sitting in front of the mirror
in her dresser room,

90
00:08:33,467 --> 00:08:36,200
she makes a drawing.

91
00:08:36,200 --> 00:08:39,200
The drawing of her sadness

92
00:08:40,733 --> 00:08:43,067
and decides she has until her
16th birthday

93
00:08:43,067 --> 00:08:46,067
to find meaning to her life.

94
00:08:47,167 --> 00:08:50,167
And she finds our father.

95
00:08:50,967 --> 00:08:52,500
A Brazilian who just arrived

96
00:08:52,500 --> 00:08:55,500
from the country she dreams of
in films and songs,

97
00:08:58,667 --> 00:09:01,367
but who comes back from New York

98
00:09:01,367 --> 00:09:04,400
not as a Sinatra, but as a Che
Guevera

99
00:09:05,567 --> 00:09:08,300
carrying in his pockets books
by Marx,

100
00:09:08,300 --> 00:09:11,300
and the desire to start a
revolution.

101
00:09:18,867 --> 00:09:21,867
And overnight, our mother
leaves the Catholic school,

102
00:09:22,767 --> 00:09:25,600
sells her TV, her hair,

103
00:09:25,600 --> 00:09:28,533
and jumps into the street
demonstrations.

104
00:09:37,300 --> 00:09:40,067
Together they joined the
Communist Party

105
00:09:40,067 --> 00:09:42,933
ready to be sent off the
gurrila warfare in Araguaia.

106
00:09:44,767 --> 00:09:47,767
But when the leaders see her 6
month belly,

107
00:09:47,767 --> 00:09:50,767
they don't let them go.

108
00:09:51,867 --> 00:09:53,700
Almost all the young people
that go

109
00:09:53,700 --> 00:09:56,667
end up executed by the military.

110
00:10:00,100 --> 00:10:01,833
It was you in our mother's
belly

111
00:10:01,833 --> 00:10:04,767
who saved our parents lives.

112
00:10:11,400 --> 00:10:13,800
And in the middle of this
turmoil

113
00:10:13,800 --> 00:10:16,633
you were born

114
00:10:16,633 --> 00:10:18,600
and raised in hiding,

115
00:10:18,600 --> 00:10:21,600
never allowed to tell anyone
where you lived.

116
00:10:28,367 --> 00:10:29,633
I wonder how this time

117
00:10:29,633 --> 00:10:32,033
stayed in your memory.

118
00:10:32,033 --> 00:10:35,033
In your body.

119
00:10:51,433 --> 00:10:53,933
The first images I find of you

120
00:10:53,933 --> 00:10:55,667
you just turned 13

121
00:10:55,667 --> 00:10:58,600
and got this camera as a
present.

122
00:11:08,067 --> 00:11:09,700
It was when I was born,

123
00:11:09,700 --> 00:11:12,900
in the 80s, the time of the
amnesty.

124
00:11:12,900 --> 00:11:15,567
Our family exited clandenistity

125
00:11:15,567 --> 00:11:18,567
and seemed to enter an american
commercial from the 50s.

126
00:11:33,167 --> 00:11:36,167
And you start to dance.

127
00:11:42,067 --> 00:11:44,933
Dance...

128
00:11:44,933 --> 00:11:47,933
*intense piano music

129
00:12:24,333 --> 00:12:25,800
My mother said that since you
were 4

130
00:12:25,800 --> 00:12:28,767
you knew you wanted to be an
actress.

131
00:12:33,567 --> 00:12:35,167
And it seems you always found a
way

132
00:12:35,167 --> 00:12:37,200
of putting me in the picture.

133
00:13:21,267 --> 00:13:24,267
*upbeat music

134
00:13:32,267 --> 00:13:33,533
[PETRA] You showed me

135
00:13:33,533 --> 00:13:36,400
all the Sherly Temple movies

136
00:13:36,400 --> 00:13:39,400
and trained me to become an
actress.

137
00:13:44,067 --> 00:13:47,067
First, you teach me how to sing.

138
00:14:51,800 --> 00:14:54,067
[PETRA] You spend the
afternoons directing me

139
00:14:54,067 --> 00:14:57,067
acting, creating scenes.

140
00:15:00,400 --> 00:15:03,400
[doorbell]

141
00:15:20,200 --> 00:15:21,667
[PETRA] I remember watching
this scene

142
00:15:21,667 --> 00:15:23,667
where you killed my Nanny

143
00:15:25,367 --> 00:15:28,367
and I had so many nightmares of
this.

144
00:15:52,067 --> 00:15:54,967
*Dedicated To The One I Love

145
00:16:12,967 --> 00:16:15,967
[PETRA] When you turned 15 our
parents separated

146
00:16:18,067 --> 00:16:21,067
and you stopped filming.

147
00:16:27,300 --> 00:16:28,967
And little by little,

148
00:16:28,967 --> 00:16:31,967
I feel you becoming distant.

149
00:16:50,900 --> 00:16:53,867
I look for you.

150
00:17:17,133 --> 00:17:19,633
You stopped playing theatre
with me

151
00:17:19,633 --> 00:17:22,633
to become a real actress.

152
00:17:40,667 --> 00:17:42,400
And when you're 17

153
00:17:42,400 --> 00:17:45,400
you join the Flying Bull
Theatre Group.

154
00:18:35,267 --> 00:18:36,400
[PETRA] The other actors tell me

155
00:18:36,400 --> 00:18:37,333
you practiced non-stop.

156
00:18:37,333 --> 00:18:39,333
Obsessively.

157
00:18:39,333 --> 00:18:41,767
That even when everything
seemed perfect

158
00:18:41,767 --> 00:18:44,567
for you it was never good
enough.

159
00:18:44,567 --> 00:18:47,567
Something was always missing.

160
00:18:49,900 --> 00:18:52,833
*fast music

161
00:20:01,567 --> 00:20:03,433
But you're not satisfied.

162
00:20:03,433 --> 00:20:06,500
You want more.

163
00:20:06,500 --> 00:20:09,500
You say you want to be a film
actress.

164
00:20:10,233 --> 00:20:13,233
[children singing]

165
00:20:16,733 --> 00:20:19,767
And on my birthday you took me
by the hand

166
00:20:27,433 --> 00:20:30,433
led me up these stairs

167
00:20:36,633 --> 00:20:39,633
came into this room

168
00:20:43,400 --> 00:20:45,600
closed the door

169
00:20:45,600 --> 00:20:48,600
and said:

170
00:21:25,467 --> 00:21:28,467
[PETRA] You put the shell over
my ear

171
00:21:29,767 --> 00:21:33,233
and I hear the ocean.

172
00:21:33,233 --> 00:21:36,233
[ocean waves fade in]

173
00:22:04,533 --> 00:22:08,167
You were right.

174
00:22:08,167 --> 00:22:11,167
7 was my worst age.

175
00:23:39,900 --> 00:23:42,900
*fiddle music

176
00:24:33,800 --> 00:24:34,767
[PETRA] You take your photos

177
00:24:34,767 --> 00:24:37,100
to several film companies

178
00:24:37,100 --> 00:24:40,100
and get to do some auditions.

179
00:24:54,967 --> 00:24:58,200
You leave this interview excited

180
00:24:58,200 --> 00:25:00,333
but the days pass

181
00:25:00,333 --> 00:25:02,500
and there's no news.

182
00:25:02,500 --> 00:25:05,500
No one calls you back.

183
00:25:07,067 --> 00:25:09,067
You call them many times

184
00:25:10,767 --> 00:25:12,767
and they tell you to wait.

185
00:25:13,600 --> 00:25:16,167
You can't stand this time.

186
00:25:16,167 --> 00:25:19,200
This waiting.

187
00:25:23,733 --> 00:25:26,733
[subway noises]

188
00:28:37,067 --> 00:28:39,367
[PETRA] I'm still 7

189
00:28:39,367 --> 00:28:41,533
and you come back

190
00:28:41,533 --> 00:28:43,400
thinking that here in Brazil

191
00:28:43,400 --> 00:28:46,400
your roots will find more space
to grow.

192
00:29:09,167 --> 00:29:10,733
[PETRA] But a letter arrives

193
00:29:10,733 --> 00:29:12,733
saying you were accepted to a
university

194
00:29:12,733 --> 00:29:15,733
in New York.

195
00:29:18,367 --> 00:29:21,067
And our mom decides that this
time

196
00:29:21,067 --> 00:29:22,633
we'll go the 3 of us.

197
00:29:22,633 --> 00:29:23,900
Together.

198
00:29:23,900 --> 00:29:26,867
So we won't be so alone.

199
00:29:36,200 --> 00:29:37,867
You get on the plane

200
00:29:37,867 --> 00:29:41,367
showing me how everything works

201
00:29:41,367 --> 00:29:44,367
how it flies.

202
00:29:49,633 --> 00:29:52,633
But in an instant, you start
crying.

203
00:29:58,067 --> 00:30:01,033
It's your ears that hurt.

204
00:30:02,467 --> 00:30:05,467
I feel strange seeing my big
sister crying.

205
00:30:08,900 --> 00:30:12,167
You argue with my mother,

206
00:30:12,167 --> 00:30:15,167
There is anger in your crying.

207
00:30:15,167 --> 00:30:18,200
Such deep, forceful crying.

208
00:30:20,033 --> 00:30:23,000
*soft piano music

209
00:33:25,100 --> 00:33:28,400
[PETRA] I don't like the first
months in New York.

210
00:33:28,400 --> 00:33:30,767
Olinda doesn't take care of me
anymore

211
00:33:30,767 --> 00:33:32,467
and I hate it.

212
00:33:32,467 --> 00:33:34,800
I hate learning English.

213
00:33:34,800 --> 00:33:36,267
The school

214
00:33:36,267 --> 00:33:38,767
The teacher with her tiger
skirt.

215
00:33:38,767 --> 00:33:41,467
And the cold.

216
00:33:41,467 --> 00:33:42,600
When I get home

217
00:33:42,600 --> 00:33:44,700
I do 2 small rituals:

218
00:33:44,700 --> 00:33:47,433
I scratch my wrists with a
serated knife

219
00:33:47,433 --> 00:33:49,533
till there really red

220
00:33:49,533 --> 00:33:52,533
and I put a bandaid on my
forehead.

221
00:33:53,133 --> 00:33:54,567
You see me,

222
00:33:54,567 --> 00:33:56,767
and pull me into the bathroom.

223
00:33:56,767 --> 00:33:59,733
Serious, you say:

224
00:33:59,733 --> 00:34:02,633
Petra, be a good actress.

225
00:34:02,633 --> 00:34:05,667
If you want to draw attention,
do it right.

226
00:34:05,667 --> 00:34:07,633
Nobody's going to believe

227
00:34:07,633 --> 00:34:09,633
in this bandaid in the middle
of your forehead.

228
00:34:11,400 --> 00:34:12,800
Hide it a bit under your bangs

229
00:34:12,800 --> 00:34:15,767
and it will be much more
convincing.

230
00:34:23,933 --> 00:34:26,067
At that time I didn't believe
in God

231
00:34:26,067 --> 00:34:28,233
or in Santa Clause.

232
00:34:28,233 --> 00:34:31,233
But I believed in mermaids.

233
00:34:35,300 --> 00:34:36,733
They seemed as possible to me

234
00:34:36,733 --> 00:34:39,733
as the seahorses I saw in the
aquarium.

235
00:34:50,667 --> 00:34:51,933
You take me to watch "The
Little Mermaid"

236
00:34:51,933 --> 00:34:54,900
in the cinema near our house

237
00:34:55,733 --> 00:34:57,500
and on this day,

238
00:34:57,500 --> 00:35:00,500
you start playing theatre with
me again

239
00:35:00,500 --> 00:35:02,600
and we come back singing

240
00:35:02,600 --> 00:35:04,733
and feeling like her,

241
00:35:04,733 --> 00:35:06,767
underwater.

242
00:35:06,767 --> 00:35:09,767
Dreaming of changing skins.

243
00:35:17,133 --> 00:35:20,400
Then, you read me the original
story

244
00:35:20,400 --> 00:35:21,800
where the Little Mermaid suffers

245
00:35:21,800 --> 00:35:24,067
to be come a woman,

246
00:35:24,067 --> 00:35:27,067
loses her voice and dies.

247
00:35:27,967 --> 00:35:31,567
What do you mean, she dies?

248
00:35:31,567 --> 00:35:33,800
I feel betrayed.

249
00:35:33,800 --> 00:35:36,800
I ask to sleep with you.

250
00:35:37,733 --> 00:35:38,733
What's left of this memory

251
00:35:38,733 --> 00:35:41,733
is this dance we created
together.

252
00:36:21,100 --> 00:36:22,433
[PETRA] The Little Mermaid

253
00:36:22,433 --> 00:36:23,867
accepts the pain of a knife

254
00:36:23,867 --> 00:36:26,867
cutting into her body

255
00:36:28,067 --> 00:36:30,667
bleeding her body

256
00:36:30,667 --> 00:36:33,067
to get legs

257
00:36:33,067 --> 00:36:36,067
and be able to dance.

258
00:38:30,333 --> 00:38:31,533
[PETRA] One afternoon

259
00:38:31,533 --> 00:38:34,167
I bring a friend home.

260
00:38:34,167 --> 00:38:37,167
My first new friend after
months.

261
00:38:41,333 --> 00:38:44,333
I start showing her our house.

262
00:38:45,167 --> 00:38:48,167
The living room, the bedroom,

263
00:38:49,067 --> 00:38:52,067
until we come to yours.

264
00:38:52,700 --> 00:38:54,500
I knock on the door

265
00:38:54,500 --> 00:38:57,500
and come in with the girl.

266
00:38:58,267 --> 00:39:00,600
You're all covered up

267
00:39:00,600 --> 00:39:03,533
only your face is showing.

268
00:39:04,467 --> 00:39:07,467
Your eyes are red.

269
00:39:09,067 --> 00:39:12,067
Maybe you said something from
your bed,

270
00:39:14,367 --> 00:39:17,067
I don't remember.

271
00:39:17,067 --> 00:39:19,500
All I remember is that when we
left the room,

272
00:39:19,500 --> 00:39:22,333
my friend with an anxious look

273
00:39:22,333 --> 00:39:25,867
asked me what was wrong with
you.

274
00:39:25,867 --> 00:39:28,867
She's like that, I answered.

275
00:39:29,500 --> 00:39:32,467
She's like that.

276
00:41:46,633 --> 00:41:48,433
[PETRA] Our mother spends hours

277
00:41:48,433 --> 00:41:49,633
looking for you in the streets,

278
00:41:49,633 --> 00:41:52,633
desperate.

279
00:41:55,900 --> 00:41:59,433
but after some time, you come
back.

280
00:41:59,433 --> 00:42:01,967
she takes you to a psychiatrist

281
00:42:01,967 --> 00:42:04,967
and you start being treated
with lithium.

282
00:44:06,567 --> 00:44:08,267
[PETRA] You wake me up in the
morning.

283
00:44:08,267 --> 00:44:12,067
You're sad, I don't like it.

284
00:44:12,067 --> 00:44:15,067
Your sadness.

285
00:44:15,067 --> 00:44:17,367
I remember it's the day of show
and tell

286
00:44:17,367 --> 00:44:18,733
and it's my turn to take
something

287
00:44:18,733 --> 00:44:21,767
to show and tell to my class.

288
00:44:24,767 --> 00:44:26,733
You go to your room,

289
00:44:26,733 --> 00:44:30,233
you come back with a blue
stuffed dog.

290
00:44:30,233 --> 00:44:33,567
You explain that it has special
powers

291
00:44:33,567 --> 00:44:35,900
and that whenever I really
wanted something

292
00:44:35,900 --> 00:44:38,067
I just had to close my eyes,

293
00:44:38,067 --> 00:44:39,400
make a wish,

294
00:44:39,400 --> 00:44:41,433
shake the little dog,

295
00:44:41,433 --> 00:44:44,433
and my wish will come true.

296
00:44:47,367 --> 00:44:49,567
When it's time for show and tell

297
00:44:49,567 --> 00:44:51,733
the children form a circle

298
00:44:51,733 --> 00:44:54,667
and my turn comes to explain
what the dog's about.

299
00:44:57,867 --> 00:45:00,867
It shakes, and has sad eyes.

300
00:45:01,900 --> 00:45:04,100
And the children ask:

301
00:45:04,100 --> 00:45:05,233
but doesn't it play music?

302
00:45:05,233 --> 00:45:07,933
Doesn't it do anything else?

303
00:45:07,933 --> 00:45:09,067
No.

304
00:45:09,067 --> 00:45:12,067
It only shakes and has sad eyes.

305
00:45:26,733 --> 00:45:28,200
You stay home.

306
00:45:28,200 --> 00:45:30,133
All day long at home

307
00:45:30,133 --> 00:45:33,533
alone.

308
00:45:33,533 --> 00:45:35,400
Doing what?

309
00:45:35,400 --> 00:45:38,367
Talking to whom?

310
00:45:40,967 --> 00:45:43,967
At the end of the day, a friend
calls you.

311
00:45:44,700 --> 00:45:47,733
You were supposed to meet.

312
00:45:49,733 --> 00:45:51,200
I've been imagining this person

313
00:45:51,200 --> 00:45:53,667
for 20 years.

314
00:45:53,667 --> 00:45:55,900
What did you say to him?

315
00:45:55,900 --> 00:45:58,900
What did he do?

316
00:46:03,833 --> 00:46:06,067
[MAN] This one weekend

317
00:46:06,067 --> 00:46:08,900
that we had planned on her

318
00:46:08,900 --> 00:46:11,900
coming to see the show I was
doing,

319
00:46:13,367 --> 00:46:15,667
I called her late in the day.

320
00:46:15,667 --> 00:46:17,567
She didn't sound good.

321
00:46:17,567 --> 00:46:20,567
So I said 'Elena, I'll come and
get you,

322
00:46:21,567 --> 00:46:22,700
we'll go out for a drink,

323
00:46:22,700 --> 00:46:24,167
we'll go out for a coffee

324
00:46:24,167 --> 00:46:25,500
or whatever it is you like to do

325
00:46:25,500 --> 00:46:27,167
I'll come get you.'

326
00:46:27,167 --> 00:46:29,367
She was hysterical over the
phone.

327
00:46:29,367 --> 00:46:32,667
She was like 'I don't want you
to see me like this'

328
00:46:32,667 --> 00:46:35,500
and 'I'm not doing so well'

329
00:46:35,500 --> 00:46:38,500
and I said 'No, Elena, it's OK

330
00:46:40,367 --> 00:46:43,367
it's not a problem, I'll come
and get you!

331
00:46:44,700 --> 00:46:46,900
Please let me come and get you,

332
00:46:46,900 --> 00:46:48,233
I'll jump in a cab and I'll
come and I'll get you!'

333
00:46:48,233 --> 00:46:50,967
and she said 'No no, Michael
please,

334
00:46:50,967 --> 00:46:54,067
I don't want to see you, I feel
bad,

335
00:46:54,067 --> 00:46:57,067
I feel terrible and I don't...'

336
00:46:58,667 --> 00:47:00,667
I said 'I'm coming to get you.'

337
00:47:02,733 --> 00:47:05,733
So I jumped in a cab with my
roommate

338
00:47:07,400 --> 00:47:09,433
and rang the bell, rang the
bell, rang the bell...

339
00:47:10,900 --> 00:47:12,100
She wasn't answering.

340
00:47:12,100 --> 00:47:14,433
and then I ran to the corner

341
00:47:14,433 --> 00:47:16,700
to a pay phone and I called her,

342
00:47:16,700 --> 00:47:18,933
and I kept calling and calling
and calling her

343
00:47:18,933 --> 00:47:20,367
but the line was busy...

344
00:47:20,367 --> 00:47:23,367
I was so frustrated, so angry.

345
00:47:33,700 --> 00:47:36,233
I'd been out there for, like,
an hour

346
00:47:36,233 --> 00:47:39,533
and I didn't know what to do

347
00:47:39,533 --> 00:47:42,500
when your mother showed up.

348
00:47:54,833 --> 00:47:57,833
[PETRA] You take a whole bottle
of aspirin

349
00:47:59,500 --> 00:48:02,500
sit at a desk and write this
letter.

350
00:50:52,833 --> 00:50:54,333
[PETRA whispers] This time

351
00:50:54,333 --> 00:50:57,367
I was not supposed to fight...

352
00:53:49,733 --> 00:53:52,867
*soft piano music

353
00:55:35,400 --> 00:55:37,733
[PETRA] When I got back home

354
00:55:37,733 --> 00:55:40,200
I saw my mother with such a sad,

355
00:55:40,200 --> 00:55:43,200
desperate look I'd never seen
before.

356
00:55:46,067 --> 00:55:49,067
Did Elena die? I asked.

357
00:55:53,700 --> 00:55:55,533
And she told me it was you.

358
00:55:55,533 --> 00:55:58,533
I found everything so cruel.

359
00:56:39,200 --> 00:56:40,533
[PETRA] In the days that
followed

360
00:56:40,533 --> 00:56:42,067
my cousin told me that if I
waned

361
00:56:42,067 --> 00:56:44,067
I could keep on talking to you.

362
00:56:46,067 --> 00:56:49,067
That you'd be invisible, but
you'd hear me.

363
00:56:53,567 --> 00:56:56,133
I do this.

364
00:56:56,133 --> 00:56:59,133
I talk to you.

365
00:59:02,467 --> 00:59:04,133
[PETRA] After you die,

366
00:59:04,133 --> 00:59:07,467
our mother becomes longing.

367
00:59:07,467 --> 00:59:10,400
Always with a sad, distant look.

368
00:59:12,500 --> 00:59:14,100
I ask:

369
00:59:14,100 --> 00:59:17,100
'what is it mom? Are you sad?'

370
00:59:18,833 --> 00:59:20,500
She watches me in silence,

371
00:59:20,500 --> 00:59:23,500
tries to smile and says

372
00:59:24,300 --> 00:59:27,267
'I'm thinking of Elena.'

373
00:59:31,767 --> 00:59:33,767
I come close, give her a hug,

374
00:59:35,633 --> 00:59:37,567
touch her, just to try to make
her smile.

375
00:59:41,467 --> 00:59:44,500
And we repeats this ritual,

376
00:59:44,500 --> 00:59:47,800
this talk everyday,

377
00:59:47,800 --> 00:59:50,767
many times a day,

378
00:59:50,767 --> 00:59:53,433
until I don't even have to ask.

379
00:59:53,433 --> 00:59:56,433
I look at her and know she's
thinking of you.

380
00:59:58,233 --> 01:00:00,200
Almost always thinking of you.

381
01:00:05,967 --> 01:00:08,967
Our father, when I ask him
about you,

382
01:00:10,633 --> 01:00:13,233
he can't speak.

383
01:00:13,233 --> 01:00:16,233
He only looks away in silence.

384
01:00:24,600 --> 01:00:27,100
Psychological report:

385
01:00:27,100 --> 01:00:30,400
Petra is a 7 years, 6 month old
girl

386
01:00:30,400 --> 01:00:32,600
who was brought for
psychological evaluation

387
01:00:32,600 --> 01:00:34,300
by her mother.

388
01:00:34,300 --> 01:00:36,867
The mother indicated that Petra
started saying

389
01:00:36,867 --> 01:00:38,900
that she wanted to die

390
01:00:38,900 --> 01:00:42,167
and has been experiencing
nightmares.

391
01:00:42,167 --> 01:00:44,233
There is also evidence of
depression,

392
01:00:44,233 --> 01:00:47,233
and feelings of guilt.

393
01:00:48,300 --> 01:00:51,300
Petra avoided talking about her
sister.

394
01:00:52,100 --> 01:00:53,867
Petra is using defences,

395
01:00:53,867 --> 01:00:57,267
which suggest obsessive
compulsive tendencies

396
01:00:57,267 --> 01:01:00,267
in order to cope with difficult
situations.

397
01:01:01,633 --> 01:01:04,133
She is likely to continue using
these defences

398
01:01:04,133 --> 01:01:05,633
for a while,

399
01:01:05,633 --> 01:01:08,400
which permit her to deny
admission

400
01:01:08,400 --> 01:01:11,333
of her true depression.

401
01:01:41,133 --> 01:01:42,767
I turn 10

402
01:01:42,767 --> 01:01:44,933
and my mother and I decide to
spend our vacation

403
01:01:44,933 --> 01:01:47,933
at the country home of some
friends.

404
01:02:02,100 --> 01:02:04,067
They have a golf cart

405
01:02:04,067 --> 01:02:07,067
and I spend my days driving up
and down.

406
01:02:14,900 --> 01:02:16,767
One afternoon,

407
01:02:16,767 --> 01:02:19,767
going around in circles with
the cart,

408
01:02:19,767 --> 01:02:22,767
I realize you'd died forever.

409
01:02:26,600 --> 01:02:29,600
*soft music

410
01:02:40,533 --> 01:02:43,533
Won't she ever come back again?

411
01:02:50,567 --> 01:02:52,967
No.

412
01:02:52,967 --> 01:02:55,167
She's dead.

413
01:02:55,167 --> 01:02:58,200
She'll never come again.

414
01:03:02,700 --> 01:03:04,567
I go back to the house

415
01:03:04,567 --> 01:03:07,067
and realize my mother can die

416
01:03:07,067 --> 01:03:08,633
and think that if I thought this

417
01:03:08,633 --> 01:03:11,200
it means she'll really die at
any moment

418
01:03:11,200 --> 01:03:12,700
that it's a sign

419
01:03:12,700 --> 01:03:14,067
and that I have to do
everything that I can

420
01:03:14,067 --> 01:03:17,067
to avoid it.

421
01:03:20,067 --> 01:03:22,733
I start making promises
constantly

422
01:03:22,733 --> 01:03:25,067
that I won't ever eat salt
again,

423
01:03:25,067 --> 01:03:27,067
that i'll climb all the stairs

424
01:03:27,067 --> 01:03:29,300
to the 19th floor of our
appartment on my knees,

425
01:03:29,300 --> 01:03:32,067
and that I'll never look at a
mirror

426
01:03:32,067 --> 01:03:35,067
so that she won't die.

427
01:03:36,533 --> 01:03:38,067
I always went into the bathroom

428
01:03:38,067 --> 01:03:40,067
with my eyes closed.

429
01:04:02,733 --> 01:04:04,067
Then, like everything,

430
01:04:04,067 --> 01:04:06,333
the fear disappeared

431
01:04:06,333 --> 01:04:09,333
and you slowly disappeared with
it.

432
01:04:20,433 --> 01:04:22,400
Until it's time for university
admission exams.

433
01:04:23,733 --> 01:04:25,267
I study a lot,

434
01:04:25,267 --> 01:04:27,533
but don't know where to go

435
01:04:27,533 --> 01:04:30,533
what path to take.

436
01:04:39,067 --> 01:04:41,067
At the very last moment,

437
01:04:41,067 --> 01:04:42,633
I choose theatre.

438
01:04:42,633 --> 01:04:46,167
But when night comes, I can't
sleep.

439
01:04:46,167 --> 01:04:49,667
Not even a minute.

440
01:04:49,667 --> 01:04:51,733
2...

441
01:04:51,733 --> 01:04:53,267
3...

442
01:04:53,267 --> 01:04:56,267
5 days go by

443
01:04:58,367 --> 01:05:01,367
and I don't sleep.

444
01:05:02,367 --> 01:05:03,400
I start to feel that my brain's

445
01:05:03,400 --> 01:05:05,833
going to explode.

446
01:05:05,833 --> 01:05:07,867
Break down.

447
01:05:07,867 --> 01:05:10,867
That a piece will come out of
place.

448
01:05:12,200 --> 01:05:15,400
Changing clothes,

449
01:05:15,400 --> 01:05:18,400
I hear myself talking alone.

450
01:06:11,767 --> 01:06:13,600
I look at the mirror

451
01:06:13,600 --> 01:06:16,600
and can't see anything behind
my eyes.

452
01:08:28,467 --> 01:08:29,300
[PETRA] She convinces me

453
01:08:29,300 --> 01:08:31,633
that life's not worth it.

454
01:08:31,633 --> 01:08:33,567
I have to die with her.

455
01:08:33,567 --> 01:08:35,633
I'm afraid.

456
01:08:35,633 --> 01:08:38,567
Afraid of what time is going to
do to me.

457
01:08:44,100 --> 01:08:47,100
What's my role...

458
01:08:48,667 --> 01:08:50,667
What's my role in this film?

459
01:08:56,367 --> 01:08:58,267
I turn 17...

460
01:08:58,267 --> 01:09:00,067
18...

461
01:09:00,067 --> 01:09:02,167
And feel as the hours go by

462
01:09:02,167 --> 01:09:05,100
I get closer to you.

463
01:09:22,233 --> 01:09:25,433
Until on my 21st birthday

464
01:09:25,433 --> 01:09:28,800
my mother looks at me and says:

465
01:09:28,800 --> 01:09:31,800
'Now you're older than Elena.'

466
01:09:42,567 --> 01:09:43,933
The fear that I'd follow in
your steps

467
01:09:43,933 --> 01:09:47,100
began to dissipate.

468
01:09:47,100 --> 01:09:49,600
But I kept on finding that you,

469
01:09:49,600 --> 01:09:52,600
Elena, were in me.

470
01:09:53,367 --> 01:09:56,367
A part of me.

471
01:10:00,800 --> 01:10:01,800
I stop feeling this

472
01:10:01,800 --> 01:10:04,767
when I started looking for you.

473
01:10:05,600 --> 01:10:08,433
You began taking shape.

474
01:10:08,433 --> 01:10:11,933
A body.

475
01:10:11,933 --> 01:10:14,933
Being re-born a bit, from me.

476
01:10:20,400 --> 01:10:23,400
But to die again...

477
01:10:33,633 --> 01:10:35,133
And I,

478
01:10:35,133 --> 01:10:37,000
much more conscious to feel
your death

479
01:10:37,000 --> 01:10:39,933
this time.

480
01:10:39,933 --> 01:10:41,733
Immense pleasure,

481
01:10:41,733 --> 01:10:44,733
that comes with so much pain.

482
01:10:51,033 --> 01:10:53,533
I drown in you.

483
01:10:53,533 --> 01:10:56,533
In Orphellius.

484
01:11:09,933 --> 01:11:12,933
*soft piano music

485
01:11:37,433 --> 01:11:40,067
I perform.

486
01:11:40,067 --> 01:11:43,067
I perform our death

487
01:11:45,900 --> 01:11:48,933
to find air.

488
01:11:54,100 --> 01:11:57,067
To be able to live.

489
01:14:15,167 --> 01:14:18,167
And little by little,

490
01:14:23,567 --> 01:14:26,567
the pain turns to water.

491
01:14:39,167 --> 01:14:42,167
Becomes memory.

492
01:14:46,133 --> 01:14:49,133
*soft piano music

493
01:16:41,933 --> 01:16:44,433
Memories go with time.

494
01:16:44,433 --> 01:16:47,133
They fade.

495
01:16:47,133 --> 01:16:50,167
But some finale songs,

496
01:16:50,967 --> 01:16:53,067
just bits of relief

497
01:16:53,067 --> 01:16:56,067
in the small openings of poetry.

498
01:17:00,100 --> 01:17:03,067
You are my unconsolable memory

499
01:17:04,933 --> 01:17:07,967
made of shadow and stone

500
01:17:08,967 --> 01:17:12,567
and it is from this that all is
born

501
01:17:12,567 --> 01:17:15,567
and dances.

502
01:18:26,167 --> 01:18:28,200
*Dedicated To The One I Love

