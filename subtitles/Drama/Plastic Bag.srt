1
00:00:15,460 --> 00:00:18,414
Plastic Bag

2
00:00:28,422 --> 00:00:30,919
They told me it's out there.

3
00:00:31,194 --> 00:00:33,210
The Pacific vortex.

4
00:00:33,405 --> 00:00:34,851
Paradise.

5
00:00:35,860 --> 00:00:37,176
You may be thinking:

6
00:00:37,289 --> 00:00:40,532
"Hey, shut up and enjoy
the sun set, you idiot. "

7
00:00:41,145 --> 00:00:43,061
Well, I do not care what you think.

8
00:00:43,425 --> 00:00:45,794
No one needs me here, anymore.

9
00:00:46,124 --> 00:00:47,951
Not even my maker.

10
00:00:49,683 --> 00:00:51,786
Do you know her by chance?

11
00:00:52,259 --> 00:00:53,714
Have you seen her?

12
00:01:25,527 --> 00:01:26,991
My first breath.

13
00:01:32,884 --> 00:01:34,627
I met my maker.

14
00:01:35,088 --> 00:01:36,747
I had a purpose.

15
00:01:47,378 --> 00:01:50,353
She was quick to accept me into her home,

16
00:01:50,500 --> 00:01:52,566
and make me part of her life.

17
00:01:53,229 --> 00:01:56,241
But she also gave me my independence.

18
00:02:13,376 --> 00:02:16,461
I met her friends.
She trusted me.

19
00:02:22,716 --> 00:02:25,496
She showed me what
she knew of the world.

20
00:02:26,210 --> 00:02:29,199
I didn�t understand all her running around...

21
00:02:29,721 --> 00:02:31,728
...but I always cheered her on.

22
00:02:47,713 --> 00:02:49,359
This was shocking.

23
00:02:57,072 --> 00:03:00,664
This brought me closer to her than ever before.

24
00:03:00,965 --> 00:03:03,731
My skin against her skin.

25
00:03:04,029 --> 00:03:06,088
My cold, her warmth.

26
00:03:06,449 --> 00:03:09,546
I made her happy, and she made me happy.

27
00:03:09,659 --> 00:03:12,386
I thought we would be together forever.

28
00:03:18,935 --> 00:03:22,161
Until I met her own private monster.

29
00:03:22,879 --> 00:03:24,461
Look at this beast!

30
00:03:24,773 --> 00:03:27,937
How could she prefer this one to me?

31
00:03:28,061 --> 00:03:32,484
What could this thing do?
Nothing but slobber all over me!

32
00:03:35,406 --> 00:03:38,751
She spent less and less time with me.

33
00:03:40,207 --> 00:03:42,806
But I still did everything for her.

34
00:04:09,188 --> 00:04:11,861
I thought it must have been a mistake.

35
00:04:12,121 --> 00:04:14,684
That she was worried sick about me.

36
00:04:14,797 --> 00:04:18,692
I imagined her crying:
�Where is he, where is he?!�

37
00:04:56,543 --> 00:04:58,593
Nothing could destroy me.

38
00:05:01,721 --> 00:05:05,454
Flying monsters coming to peck at me.

39
00:05:22,805 --> 00:05:25,046
And the darkness began.

40
00:05:25,517 --> 00:05:29,269
I don�t know for how long,
and what did it really matter.

41
00:05:29,572 --> 00:05:33,555
The world decomposed,
it was eaten by monsters,

42
00:05:33,693 --> 00:05:36,700
some too small for me to even see.

43
00:05:39,623 --> 00:05:42,375
Not me! I remained.

44
00:05:42,488 --> 00:05:46,861
I was strong and smart and
I would find my maker, hah!

45
00:06:08,786 --> 00:06:10,470
I had holes,

46
00:06:10,584 --> 00:06:14,505
but overtime I learned how to use them to navigate the wind

47
00:06:14,718 --> 00:06:16,189
and I could fly.

48
00:06:16,801 --> 00:06:18,231
I was free.

49
00:06:20,136 --> 00:06:21,745
Or so I thought.

50
00:06:22,548 --> 00:06:24,809
Sometimes I had to wait�

51
00:06:27,795 --> 00:06:29,197
And wait.

52
00:06:43,732 --> 00:06:48,440
I searched everywhere for our home,
hoping to find her.

53
00:07:04,816 --> 00:07:06,296
Destruction.

54
00:07:07,382 --> 00:07:08,828
Desolation.

55
00:07:11,142 --> 00:07:12,676
There was nothing.

56
00:07:17,247 --> 00:07:18,725
She never came.

57
00:07:22,770 --> 00:07:24,740
I thought this was her.

58
00:07:27,491 --> 00:07:29,648
But there was nobody left.

59
00:07:53,873 --> 00:07:57,377
I did not want to think about her anymore.

60
00:07:58,269 --> 00:08:02,522
She had forgotten me,
and I would forget her too.

61
00:08:05,832 --> 00:08:08,799
I went to worlds I had never seen.

62
00:08:23,053 --> 00:08:26,674
What kind of giant monsters had lived here?

63
00:08:29,658 --> 00:08:31,792
And where were they now?

64
00:08:58,966 --> 00:09:03,757
No matter how far I travelled,
there were always new worlds to see.

65
00:09:08,207 --> 00:09:12,428
I wonder if my maker knew such places existed.

66
00:09:23,760 --> 00:09:27,726
They looked like my maker�s beastie,
only bigger.

67
00:09:33,332 --> 00:09:35,777
I served no purpose to them.

68
00:09:40,108 --> 00:09:41,825
Monsters, be gone!

69
00:09:46,893 --> 00:09:50,917
And sometimes the world
was even too great for me.

70
00:09:54,872 --> 00:09:58,350
And sometimes,
the waiting drove me mad.

71
00:10:08,542 --> 00:10:10,342
Wasn�t she beautiful?

72
00:10:13,454 --> 00:10:16,348
She was also searching for her maker.

73
00:10:19,702 --> 00:10:24,151
I didn�t need a maker anymore,
I only needed her.

74
00:10:36,846 --> 00:10:41,495
The winds drifted us apart
and I was alone again.

75
00:10:42,745 --> 00:10:44,663
Where am I going?

76
00:10:46,031 --> 00:10:47,481
Who was I?

77
00:10:53,659 --> 00:10:55,216
Was that me?

78
00:10:56,609 --> 00:10:58,861
I looked just like the earth,

79
00:10:59,768 --> 00:11:05,057
and I turned around, and I saw the sun
and I looked like that too.

80
00:11:08,807 --> 00:11:10,832
But I was still lost.

81
00:11:25,735 --> 00:11:29,206
And that�s when I first
learned about the Vortex.

82
00:11:30,635 --> 00:11:33,569
They had chained themselves here on purpose,

83
00:11:33,763 --> 00:11:36,483
in order to preach about the Vortex.

84
00:11:37,252 --> 00:11:39,661
It was a world in the Pacific Ocean,

85
00:11:39,773 --> 00:11:43,541
where a hundred million tons of us had gathered.

86
00:11:45,393 --> 00:11:47,361
They said there was no maker.

87
00:11:47,683 --> 00:11:49,951
Said ...
"We are the creators."

88
00:11:50,699 --> 00:11:53,851
They said in the Vortex we were free.

89
00:11:56,619 --> 00:11:58,309
It was Paradise.

90
00:12:02,287 --> 00:12:05,530
They told me to go there,
to join the others.

91
00:13:11,365 --> 00:13:13,369
And I was born again,

92
00:13:15,480 --> 00:13:18,433
and I reached out my hand to touch.

93
00:13:20,192 --> 00:13:22,432
They looked just like me.

94
00:13:59,651 --> 00:14:03,446
And with time I learned to use
the currence of the water,

95
00:14:03,559 --> 00:14:06,368
as I had used the currence of the wind.

96
00:14:07,374 --> 00:14:09,708
And I went searching for the Vortex.

97
00:14:17,717 --> 00:14:21,022
Some ate pieces of me...

98
00:14:21,134 --> 00:14:22,908
...until they realized I was useless to them.

99
00:14:24,538 --> 00:14:27,755
I wonder where those
little pieces are now.

100
00:14:53,636 --> 00:14:55,754
I made it to the Vortex.

101
00:14:56,208 --> 00:14:58,245
I was with my own kind.

102
00:14:58,490 --> 00:15:02,152
It covered an area the size
of a small continent.

103
00:15:02,664 --> 00:15:04,557
We were free and happy.

104
00:15:05,157 --> 00:15:10,038
I loved going in circus,
and circus, and circus.

105
00:15:12,919 --> 00:15:15,868
But no one here thought about anything.

106
00:15:16,156 --> 00:15:17,877
I grew restless.

107
00:15:18,582 --> 00:15:21,304
And I started to think about her again.

108
00:15:22,994 --> 00:15:27,035
So I spun around, so fast,
that I was free.

109
00:15:28,962 --> 00:15:30,946
But I was quickly trapped.

110
00:15:31,260 --> 00:15:34,515
I have no idea how long ago that was.

111
00:15:35,565 --> 00:15:38,923
Overtime I came to like these monsters.

112
00:15:39,422 --> 00:15:41,368
Isn�t that one beautiful?

113
00:15:42,608 --> 00:15:47,497
Did my maker exist, or had
I created her in my mind?

114
00:15:48,435 --> 00:15:51,362
Why were my moments of joy so brief?

115
00:15:53,522 --> 00:15:55,500
And yet, like a fool,

116
00:15:55,612 --> 00:15:58,428
I still have hope I will meet her again.

117
00:15:58,717 --> 00:16:02,309
And if I do, I will tell her just one thing:

118
00:16:02,696 --> 00:16:06,990
I wish you had created me so that I could die.

