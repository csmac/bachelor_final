1
00:00:42,850 --> 00:00:46,780
DIARY OF A LOST GIRL

2
00:00:47,450 --> 00:00:50,539
after the novel
by Margarete B�hme

3
00:00:51,090 --> 00:00:52,840
Script: Rudolf Leonhardt

4
00:00:53,210 --> 00:00:55,740
Direction: G. W. Pabst

5
00:02:22,250 --> 00:02:26,750
So you've had your way
with this housekeeper too!

6
00:02:57,449 --> 00:03:01,560
Why is Elisabeth leaving.
- today of all days?

7
00:04:05,250 --> 00:04:08,409
PHARMACY

8
00:04:11,650 --> 00:04:15,439
Oh why does Elisabeth
have to go away?

9
00:04:35,170 --> 00:04:39,589
l'll tell you about it this
evening - little Thymian!

10
00:04:44,689 --> 00:04:46,899
- big Thymian!

11
00:05:52,370 --> 00:05:55,110
My confirmation gift!

12
00:07:02,449 --> 00:07:05,290
But... l'm no countess!

13
00:07:07,730 --> 00:07:11,910
That doesn't matter -
l'M Count Osdorff!

14
00:07:30,569 --> 00:07:32,360
This evening at half-past 10
in the pharmacy

15
00:08:40,409 --> 00:08:44,549
This is Meta,
my new housekeeper!

16
00:11:38,129 --> 00:11:40,940
Good night, Herr Pharmacist!

17
00:13:48,850 --> 00:13:51,169
l'm all alone!

18
00:14:03,570 --> 00:14:06,340
l want to be here for you!

19
00:14:13,250 --> 00:14:17,389
l'll tell you what happened
with Elisabeth!

20
00:14:24,769 --> 00:14:27,580
- l'll tell you everything!

21
00:16:24,529 --> 00:16:28,710
And she doesn't want to
say who the father is.

22
00:17:33,009 --> 00:17:35,430
You have to marry her.

23
00:17:40,130 --> 00:17:45,960
Yes, if the pharmacy weren't
so heavily mortgaged - !

24
00:17:52,890 --> 00:17:57,069
Of course, Thymian
must be asked, too.

25
00:18:16,730 --> 00:18:20,589
l can't marry him:
l don't even love him - !

26
00:18:35,730 --> 00:18:39,380
Don't lay a finger on the
innocent child, - you - !

27
00:18:55,769 --> 00:18:59,250
The family -

28
00:19:02,210 --> 00:19:04,069
- has decided -

29
00:20:21,130 --> 00:20:23,940
Ww. Bolke: Midwife

30
00:21:03,329 --> 00:21:05,430
RULES OF ATTENDANCE:...
Forbidden...

31
00:26:40,369 --> 00:26:41,769
Dear Father,

32
00:26:58,970 --> 00:27:02,380
Dear Osdorff,
l am so unhappy.

33
00:27:36,930 --> 00:27:41,000
Your nephew still doesn't
want to obey, Herr Count.

34
00:29:22,490 --> 00:29:26,180
l've tried everything with you,
every school and every trade:

35
00:29:26,809 --> 00:29:29,690
you've failed at all of them!

36
00:29:33,410 --> 00:29:37,549
Now l've had enough! See
how far you get on your own.

37
00:29:52,849 --> 00:29:58,289
In  the reformatory one lived
according to the house-rules.

38
00:32:34,490 --> 00:32:38,210
And so the housekeeper
became the lady of the house.

39
00:33:12,329 --> 00:33:14,859
Thymian's Room

40
00:33:30,970 --> 00:33:33,849
NURSERY

41
00:34:28,969 --> 00:34:33,150
Count Osdorff resolved
to help Thymian.

42
00:34:51,210 --> 00:34:53,840
Dear Osdorff:
l am so unhappy.

43
00:34:54,329 --> 00:34:59,840
What did l actually do that l
am treated so strictly and cruelly?

44
00:35:08,489 --> 00:35:11,369
...because Meta has father
wrapped around her finger.

45
00:35:11,889 --> 00:35:15,369
l beg you to go to him. Perhaps then
l'll be able to go home again.

46
00:35:15,969 --> 00:35:18,639
Yours,
Thymian

47
00:35:28,929 --> 00:35:33,110
-...l'm afraid to write to Father:
Meta is so mean... -

48
00:35:43,530 --> 00:35:47,710
l heard that your uncle
has thrown you out -

49
00:35:53,849 --> 00:35:56,969
- go now to Thymian.
You belong together:

50
00:35:57,530 --> 00:36:01,010
the outcast and
the lost-girl.

51
00:36:51,769 --> 00:36:55,880
Go to her -
Tell her something nice - !

52
00:37:59,449 --> 00:38:02,960
Here the pharmacy won't be
auctioned off to you just yet.

53
00:38:03,570 --> 00:38:05,780
now that l possess
the mortgages.

54
00:38:15,449 --> 00:38:18,539
Even the strictest discipline
cannot repress

55
00:38:19,090 --> 00:38:21,760
the girls' desire
to look pretty.

56
00:39:53,210 --> 00:39:56,900
PUNISH ERIKA

57
00:40:21,969 --> 00:40:23,619
Count Nikolaus Osdorff

58
00:40:50,610 --> 00:40:55,559
The Count claims to come
on behalf of your father.

59
00:41:39,849 --> 00:41:42,829
No-one at home wants to know
anything more about you:

60
00:41:43,369 --> 00:41:46,110
Meta has married
your father.

61
00:42:26,050 --> 00:42:31,320
Steal the key,
and l'll help you further.

62
00:42:32,170 --> 00:42:35,329
l'll wait for you tonight
in front of your house!

63
00:47:48,050 --> 00:47:51,840
l know where we can stay.

64
00:47:55,409 --> 00:47:58,739
No: l want to go to my child.

65
00:48:03,690 --> 00:48:07,519
l'll write down for you
where l can be found.

66
00:48:09,289 --> 00:48:13,119
Bar ''FOR TWO ANGELS''
Dessauer Str. 17

67
00:49:17,650 --> 00:49:20,949
I'd like to see my child,

68
00:49:25,809 --> 00:49:29,139
Little Erika Henning -

69
00:49:34,010 --> 00:49:37,309
She happens to be dead -

70
00:49:45,289 --> 00:49:48,619
He just took her away.

71
00:50:53,409 --> 00:50:57,239
Where you headed, Iuv?

72
00:51:10,449 --> 00:51:12,519
- to Erika!

73
00:51:32,090 --> 00:51:36,659
Erika and Count Osdorff are
received with open arms

74
00:51:37,409 --> 00:51:41,380
by the proprietress
of the establishment.

75
01:02:31,409 --> 01:02:34,150
And what's more,
he leaves his regards!

76
01:03:00,489 --> 01:03:03,650
Oh, be reasonable,
little one:

77
01:03:04,210 --> 01:03:07,860
you don't even have
a shirt on your back!

78
01:03:55,570 --> 01:03:59,889
If your friend helps you,
you'll be outta this whole mess!

79
01:04:03,130 --> 01:04:07,480
Me?! l'm penniless, and my uncle
won't give me anything more!

80
01:04:16,769 --> 01:04:19,019
l want to go to work!

81
01:04:24,210 --> 01:04:27,469
What are you able to do,
my poor little thing?

82
01:05:01,130 --> 01:05:07,489
Be so kind as to place
this advert in the paper.

83
01:05:41,130 --> 01:05:46,150
Dance Lessons. Thymian Henning
Dessauer Str. 17

84
01:06:07,809 --> 01:06:12,449
Dance Lessons by the beautiful
Thymian Henning

85
01:11:40,130 --> 01:11:43,989
Little child,
how did you end up here...?

86
01:11:57,930 --> 01:12:01,760
...l tell you, you'll
bitterly regret this!

87
01:12:18,449 --> 01:12:21,539
That's Dr. Vitalis -
he always wants to save us -

88
01:12:22,090 --> 01:12:24,760
- but in the end he joins us.

89
01:13:27,970 --> 01:13:31,869
You look like you're
at a funeral, dear friend:

90
01:13:32,529 --> 01:13:35,619
try and be a li'l happy!

91
01:13:39,810 --> 01:13:43,109
Today's your birthday?

92
01:14:00,890 --> 01:14:05,029
l know how l'll help:
we'll do a lottery for you.

93
01:14:59,369 --> 01:15:03,199
Once a year the pharmacist
takes his wife out on the town.

94
01:15:15,010 --> 01:15:18,729
So here you have the
seamy side of the big city.

95
01:15:50,449 --> 01:15:54,279
And what can you
win with the lot?

96
01:18:47,090 --> 01:18:50,949
Yes, Thymian, now you
are a lost-girl -

97
01:19:03,050 --> 01:19:05,579
- just as we're all lost!

98
01:19:06,689 --> 01:19:10,239
Three years have passed
since the painful meeting

99
01:19:10,850 --> 01:19:13,380
between father and daughter.

100
01:19:13,850 --> 01:19:15,850
Thymian still leads
the same life.

101
01:19:17,850 --> 01:19:24,380
After a brief, trying illness,
my beloved husband,

102
01:19:25,409 --> 01:19:31,979
the pharmacist
Karl Friedrich Henning

103
01:19:33,010 --> 01:19:37,189
passed away yesterday,
on the 17th of June, 1929.

104
01:19:50,729 --> 01:19:54,239
INHERITANCE NOTICES

105
01:19:54,850 --> 01:19:59,130
Thymian Henning, as legal
heir to Herr Henning's estate,

106
01:19:59,850 --> 01:20:03,260
is requested to attend
the reading of the will,

107
01:20:03,850 --> 01:20:07,539
which will take place
in the house of the deceased.

108
01:20:40,050 --> 01:20:42,819
Money will enter the house -
over a short course -

109
01:20:43,329 --> 01:20:46,279
by way of a dark lady!

110
01:21:14,329 --> 01:21:16,399
Why, you're rich!

111
01:21:20,810 --> 01:21:24,949
Yes, l could start
another life -

112
01:21:32,489 --> 01:21:36,630
For another life -
you need a new identity!

113
01:21:57,970 --> 01:22:00,180
He should marry her!

114
01:22:00,609 --> 01:22:03,279
Countess Thymian Osdorff:
there's an identity!

115
01:23:08,170 --> 01:23:12,100
You insist on exercising
your right

116
01:23:12,770 --> 01:23:16,350
to take over the pharmacy....

117
01:23:16,970 --> 01:23:22,800
...and drive your friend's poor
widow out of her home?

118
01:25:16,170 --> 01:25:20,100
Countess, Herr Meinert has
redeemed the mortgage

119
01:25:20,770 --> 01:25:24,350
of 45.000 marks that you
inherited from your mother...

120
01:25:24,970 --> 01:25:27,569
and thus becomes the sole
owner of the pharmacy.

121
01:25:28,050 --> 01:25:30,329
Here is the money.

122
01:29:49,170 --> 01:29:51,199
Filthy slut!

123
01:30:58,050 --> 01:31:01,880
The money!
Where's all the money? -

124
01:31:04,289 --> 01:31:11,000
l gave it to my little sister,
so she doesn't end up like me.

125
01:33:35,170 --> 01:33:38,960
And me? Am l to
blame in the end?

126
01:33:58,170 --> 01:34:01,220
I, his uncle, bear
most of the blame.

127
01:34:01,770 --> 01:34:05,319
l should never have
repudiated that helpless lad!

128
01:34:23,649 --> 01:34:29,550
May l attempt to repay you
with the debt l owe him?

129
01:34:50,250 --> 01:34:55,590
We're now in Swinem�nde.
Uncle Osdorff is so kind to me.

130
01:34:56,449 --> 01:35:01,720
My former life often seems
like an ugly, terrible dream.

131
01:37:23,729 --> 01:37:26,079
Cousin Osdorff - !

132
01:37:51,409 --> 01:37:55,590
My niece -
Countess Osdorff:

133
01:38:19,010 --> 01:38:24,140
Little Thymian, you don't
recognize me any more?

134
01:38:53,010 --> 01:39:00,000
Dear Countess, l hope you'll
let me take care of you.

135
01:39:12,689 --> 01:39:16,199
I'd like -
for us to leave!

136
01:39:24,770 --> 01:39:27,819
We beg you, dear Countess,
to work with us

137
01:39:28,369 --> 01:39:31,920
in our society for saving
wayward young girls.

138
01:39:34,289 --> 01:39:40,020
Countess Thymian Osdorff
is invited to the board meeting

139
01:39:40,930 --> 01:39:46,159
and to examine our institution
on 19 August 1929.

140
01:40:51,930 --> 01:40:55,720
Our youngest member -
Countess Osdorff.

141
01:42:08,010 --> 01:42:12,010
Today we have an
especially difficult case.

142
01:42:18,130 --> 01:42:23,079
A young girl has once again
been entrusted to us....

143
01:42:23,890 --> 01:42:26,800
...who has struck down our
every educational measure

144
01:42:27,329 --> 01:42:29,289
by repeatedly
running away.

145
01:43:29,130 --> 01:43:32,500
How we've worried
about her -

146
01:43:50,090 --> 01:43:55,529
Fabulous!! My dear,
try the soup they make here!

147
01:44:26,569 --> 01:44:31,590
Yet she constantly turns away
from the blessings of our home -

148
01:45:17,250 --> 01:45:21,430
l know this house
and its "blessings" -

149
01:45:39,289 --> 01:45:42,340
Your ignorance
won't help her -

150
01:45:42,890 --> 01:45:45,630
l want to try, for l was
once what she is.

151
01:46:45,130 --> 01:46:50,119
A little more love and no-one
would be lost in this world!

