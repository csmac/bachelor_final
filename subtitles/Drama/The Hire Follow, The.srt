1
00:01:52,020 --> 00:01:55,251
<i>Pierrette was twenty-two that day</i>

2
00:01:56,540 --> 00:02:01,819
<i>That's no age to die, people say,
as though there were a right age</i>

3
00:02:03,020 --> 00:02:06,649
<i>Who could have done it?
No one, as usual</i>

4
00:02:08,460 --> 00:02:11,816
<i>She probably fought back</i>

5
00:02:12,020 --> 00:02:17,970
<i>A mistake; panicked, the man
killed her without meaning to...</i>

6
00:02:19,060 --> 00:02:24,418
<i>... losing his head over a few francs
in her bag</i>

7
00:02:25,700 --> 00:02:28,976
<i>They'll find the killer some day...</i>

8
00:02:30,700 --> 00:02:34,249
<i>... but no one will hold her
in his arms again</i>

9
00:03:00,780 --> 00:03:04,978
...nine, ten, eleven, twelve...

10
00:03:31,380 --> 00:03:35,009
See? You're all right now

11
00:03:58,980 --> 00:04:00,891
Before I question you...

12
00:04:01,220 --> 00:04:05,736
...tell me why people don't like you

13
00:04:07,020 --> 00:04:11,969
They don't, it's true; but then,
I don't like them

14
00:04:12,300 --> 00:04:14,734
That�s no reason

15
00:04:17,140 --> 00:04:20,291
What did you do?

16
00:04:20,620 --> 00:04:25,978
Nothing. I'm not very sociable,
and they don't like that

17
00:04:27,220 --> 00:04:31,338
Conversations die away when I go by

18
00:04:31,620 --> 00:04:34,088
I prefer silence, anyway

19
00:04:34,380 --> 00:04:35,813
You're weird

20
00:04:36,140 --> 00:04:40,577
See? You're just like the rest
of them

21
00:04:42,860 --> 00:04:45,977
Pierrette Bourgeois was murdered
nearby

22
00:04:48,060 --> 00:04:52,611
A taxi-driver saw a man
run towards this block...

23
00:04:52,940 --> 00:04:58,651
...not very tall, in a dark overcoat.
Curious, eh?

24
00:05:00,380 --> 00:05:06,649
She was a very lively girl, everyone
says so... Pierrette was always lively

25
00:05:09,540 --> 00:05:12,418
She was so pretty when alive

26
00:05:13,620 --> 00:05:16,817
I had to tell her fianc�

27
00:05:17,300 --> 00:05:20,258
Life is horrible

28
00:05:21,620 --> 00:05:24,180
I was just joking

29
00:05:24,460 --> 00:05:27,258
Like you, she lived alone

30
00:05:27,860 --> 00:05:31,330
Like you, she was unsociable...

31
00:05:31,620 --> 00:05:33,975
...and never talked to anyone

32
00:05:36,020 --> 00:05:38,250
She was weird

33
00:05:44,460 --> 00:05:46,496
Has it got to you?

34
00:08:09,780 --> 00:08:14,490
You're a real man when you kiss me...
I love it

35
00:08:15,020 --> 00:08:18,569
Will you give me a child?

36
00:08:19,380 --> 00:08:21,416
Several, if you like

37
00:08:30,700 --> 00:08:34,739
Let's walk a bit; I've been
shut up all day

38
00:08:36,020 --> 00:08:37,738
Listen, Alice...

39
00:08:40,540 --> 00:08:43,452
Why didn't you fix things?

40
00:08:44,060 --> 00:08:48,975
It's not my fault... I did speak
to my father about us

41
00:08:50,020 --> 00:08:52,011
He'd like to meet you

42
00:08:52,460 --> 00:08:55,736
- How about that job for me?
- Don't rush things

43
00:09:17,780 --> 00:09:19,418
Be patient

44
00:09:22,540 --> 00:09:24,974
You're important to me, you know

45
00:09:30,620 --> 00:09:32,975
Don't treat me like this, Emile

46
00:09:51,780 --> 00:09:55,489
Where�ve you been? Five minutes,
you said

47
00:12:24,460 --> 00:12:26,018
Like a picture of me?

48
00:14:15,540 --> 00:14:18,054
I've something to tell you, Alice

49
00:14:18,380 --> 00:14:25,013
It came to me just now as I woke up...
I think it's serious

50
00:14:30,460 --> 00:14:33,020
You'd better know the truth

51
00:14:38,140 --> 00:14:44,739
Will you marry me? Yes, I want you
to be my wife

52
00:14:45,700 --> 00:14:48,009
Don't knock yourself out

53
00:14:48,540 --> 00:14:51,976
- What do you mean?
- You don't have to sweet talk me

54
00:14:52,860 --> 00:14:56,978
- Don't you believe me?
- No, but I love you just the same

55
00:15:00,060 --> 00:15:02,972
- You're weird
- Yes, I know

56
00:15:51,540 --> 00:15:53,258
We'll try again

57
00:15:54,900 --> 00:15:56,174
Begin, please

58
00:16:26,620 --> 00:16:28,258
Was it him, or not?

59
00:16:28,540 --> 00:16:30,929
It could be, but I'm not sure, sorry

60
00:16:31,300 --> 00:16:33,416
I can go home, then?

61
00:17:47,020 --> 00:17:49,853
Backwards!

62
00:18:06,540 --> 00:18:11,330
Please blindfold me, Mademoiselle

63
00:18:48,700 --> 00:18:51,168
Nine nights, is that right?

64
00:18:51,380 --> 00:18:55,259
Don't worry, Paul; I always check

65
00:19:13,300 --> 00:19:17,009
Some places I'm not hated

66
00:19:20,380 --> 00:19:22,655
You have a record

67
00:19:23,700 --> 00:19:26,897
Six months for indecent assault

68
00:19:29,060 --> 00:19:31,893
That�s not going to help your case

69
00:19:32,300 --> 00:19:35,736
Hire isn't your real name?

70
00:19:36,300 --> 00:19:39,656
No, it's Hirovitch

71
00:19:40,140 --> 00:19:42,779
My grandfather changed it

72
00:19:43,300 --> 00:19:48,693
Tell me, how long is it since
you came inside a woman?

73
00:23:11,140 --> 00:23:15,497
How old would she have been...
seventy, eighty?

74
00:23:18,700 --> 00:23:21,009
She�d been around for ever

75
00:23:22,140 --> 00:23:27,419
People loved her, this little old lady
who spent all her time...

76
00:23:28,940 --> 00:23:31,249
...feeding the pigeons

77
00:23:32,460 --> 00:23:33,734
Shut up!

78
00:23:37,380 --> 00:23:43,569
Going from park to park, distributing
birdseed by the handful

79
00:23:45,860 --> 00:23:50,650
All the park-keepers knew her
and were touched

80
00:23:52,140 --> 00:23:57,737
A photographer wanted to market a
postcard of her surrounded by pigeons

81
00:23:59,380 --> 00:24:00,813
She agreed

82
00:24:02,300 --> 00:24:04,177
She became famous

83
00:24:06,060 --> 00:24:10,258
One day it emerged... maybe she
left a note...

84
00:24:12,020 --> 00:24:15,569
...that the birdseed was poisoned

85
00:24:17,380 --> 00:24:21,737
For years, she�d been killing pigeons

86
00:24:22,940 --> 00:24:27,730
Her neighbours didn't believe it,
she seemed so nice

87
00:24:27,940 --> 00:24:30,818
Come on, you'll catch cold

88
00:24:31,700 --> 00:24:33,418
Like someone else?

89
00:24:33,700 --> 00:24:38,410
- Should I get Rosa?
- Not Rosa, not Jasmine, not anybody

90
00:24:38,700 --> 00:24:42,170
I'm sick of screwing you sluts!

91
00:27:56,060 --> 00:27:58,335
I don't need help, leave me alone

92
00:30:45,060 --> 00:30:49,019
You needn't be afraid; I'm no hoodlum

93
00:30:49,780 --> 00:30:51,577
I'm Monsieur Hire

94
00:30:54,060 --> 00:30:56,290
I thought it was empty

95
00:30:56,620 --> 00:30:59,088
I never use the lights

96
00:31:05,540 --> 00:31:07,576
You can see everything

97
00:31:12,380 --> 00:31:17,818
If you hadn�t moved that time,
I'd never have known

98
00:31:19,620 --> 00:31:21,019
You took advantage

99
00:31:22,140 --> 00:31:26,816
- All I did was look
- You frightened me that night

100
00:31:27,140 --> 00:31:31,179
I thought you wanted to hurt me

101
00:31:32,060 --> 00:31:34,290
Why would someone like you...

102
00:31:34,620 --> 00:31:37,009
...spy on people that way?

103
00:31:39,140 --> 00:31:40,493
I might have reported you

104
00:31:42,420 --> 00:31:44,695
I might, you know

105
00:31:45,020 --> 00:31:47,011
You might, but didn't

106
00:31:55,860 --> 00:31:58,010
Why are you afraid of me?

107
00:32:01,860 --> 00:32:04,932
You can't imagine how I've suffered

108
00:32:07,940 --> 00:32:12,092
I lied; I didn't mind you watching me

109
00:32:12,700 --> 00:32:14,053
It's nice

110
00:32:14,380 --> 00:32:18,896
With someone else, it wouldn't be

111
00:32:19,860 --> 00:32:22,249
But you're sweet

112
00:32:23,620 --> 00:32:27,249
How can you say that?
You don't know me

113
00:32:49,860 --> 00:32:53,011
Sit by me and let's talk

114
00:32:56,460 --> 00:32:58,178
Don't you want to?

115
00:33:03,140 --> 00:33:07,338
- Have you been watching me for long?
- Yes, every night

116
00:33:10,020 --> 00:33:12,250
When I go to bed, what do you do?

117
00:33:13,060 --> 00:33:15,654
Nothing. I wait

118
00:33:15,860 --> 00:33:20,490
- What for?
- I don't know. I sleep very little

119
00:33:22,620 --> 00:33:24,292
So you watch all the time?

120
00:33:24,620 --> 00:33:26,576
You know all about me?

121
00:33:29,300 --> 00:33:33,339
Not all, certainly, but some things

122
00:33:36,860 --> 00:33:41,251
What�s your favourite moment?
When I undress, or...

123
00:33:42,060 --> 00:33:45,257
- Go now, don't stay here
- I'm sorry, I thought...

124
00:33:46,140 --> 00:33:47,334
Get the hell out!

125
00:35:14,700 --> 00:35:19,820
That�s more like it...
but not so sharp

126
00:35:21,620 --> 00:35:25,169
It's a quieter, more rarefied scent

127
00:35:55,980 --> 00:35:58,574
Waiter, the same again, please

128
00:36:38,620 --> 00:36:42,329
- Drinking makes you look lovely
- lf I look lovely, then kiss me

129
00:36:46,860 --> 00:36:49,658
Do you still want to marry me?

130
00:36:51,140 --> 00:36:54,974
- How about tomorrow morning?
- What�s all your hurry?

131
00:36:56,700 --> 00:37:00,409
I'm serious; marry me right away

132
00:37:08,060 --> 00:37:10,494
I knew you didn't mean it before

133
00:37:12,700 --> 00:37:14,019
Say something

134
00:37:16,380 --> 00:37:17,574
The cops called

135
00:37:19,060 --> 00:37:22,416
We swore not to talk about that

136
00:37:22,700 --> 00:37:24,338
They know nothing

137
00:37:27,220 --> 00:37:29,017
No one saw me

138
00:37:33,860 --> 00:37:38,490
We won't mention it again;
I had to tell you

139
00:37:39,380 --> 00:37:42,417
Don't worry, we'll be together

140
00:37:44,620 --> 00:37:46,497
You're so beautiful

141
00:37:54,220 --> 00:37:57,496
I love you enough for both of us

142
00:38:34,380 --> 00:38:36,336
I brought back your scarf

143
00:38:38,620 --> 00:38:42,659
- Why not pick it up? It's yours
- Why mine?

144
00:38:43,540 --> 00:38:45,496
It seems very you

145
00:38:48,940 --> 00:38:52,012
Yours would be cleaner, of course

146
00:38:53,540 --> 00:38:57,169
Those stains are Pierrette's blood

147
00:38:57,700 --> 00:39:01,010
Your neighbours said it was yours

148
00:39:02,060 --> 00:39:04,016
I do have one like it

149
00:39:05,060 --> 00:39:08,814
In that case, show it to me

150
00:39:10,060 --> 00:39:11,493
Certainly

151
00:39:28,700 --> 00:39:30,418
New, isn't it?

152
00:39:31,460 --> 00:39:35,169
I rarely wear it; it's so dreary

153
00:39:35,460 --> 00:39:37,416
New, in effect

154
00:39:38,380 --> 00:39:42,339
Anything else? Good night, then

155
00:39:44,540 --> 00:39:45,768
A note's under the mat

156
00:40:02,220 --> 00:40:03,653
She�s gorgeous...

157
00:40:03,940 --> 00:40:06,898
...and no fool, either

158
00:40:16,220 --> 00:40:19,018
I'm so sorry...

159
00:40:20,540 --> 00:40:24,249
...about what happened the other day

160
00:40:25,300 --> 00:40:29,816
I know neither of us means the other
any harm

161
00:40:31,220 --> 00:40:34,815
Will you lunch with me on Sunday?

162
00:40:35,060 --> 00:40:39,338
Just the two of us together. Alice

163
00:41:11,380 --> 00:41:13,655
Don't you ever tidy up?

164
00:41:18,300 --> 00:41:22,259
- What are you doing here?
- We had a date this afternoon

165
00:41:22,620 --> 00:41:27,489
Sorry, I had a late night;
what time is it?

166
00:41:27,700 --> 00:41:32,012
Midday, but I've to meet my aunt
instead

167
00:41:33,060 --> 00:41:37,099
I thought I'd be nice and warn you

168
00:41:39,380 --> 00:41:45,819
I miss not seeing you, so as I've
half-an-hour to spare, here I am

169
00:42:57,220 --> 00:42:58,812
You smell nice

170
00:42:59,620 --> 00:43:01,690
Sorry, I'm a bit late

171
00:43:02,540 --> 00:43:07,409
- Have you been waiting long?
- No. I'm so glad you came

172
00:43:09,380 --> 00:43:11,177
You're not cross with me?

173
00:43:13,780 --> 00:43:18,900
My nerve almost failed me
when I left the note

174
00:43:20,060 --> 00:43:24,019
I find that hard to believe.
Shall we eat?

175
00:43:25,940 --> 00:43:29,012
I love stations. Especially this one...

176
00:43:30,780 --> 00:43:32,736
...watching, imagining...

177
00:43:34,460 --> 00:43:38,419
That man who�s early, filling in time...

178
00:43:39,060 --> 00:43:41,893
...the old lady looking for her train...

179
00:43:42,220 --> 00:43:45,895
...the young woman meeting her lover...

180
00:43:47,060 --> 00:43:49,733
...or maybe walking out on him...

181
00:43:51,060 --> 00:43:53,813
Young women can be so unpredictable

182
00:43:55,780 --> 00:43:58,010
I love watching people

183
00:43:58,620 --> 00:44:02,249
- I'm sorry, that was tactless
- It's all right

184
00:44:03,260 --> 00:44:07,731
I often come here alone. This is nicer

185
00:44:16,780 --> 00:44:20,170
In Lausanne, you know it's spring...

186
00:44:20,380 --> 00:44:25,170
...when people set about repainting
their shutters

187
00:44:26,220 --> 00:44:30,054
They like life to be beautiful

188
00:44:31,060 --> 00:44:36,180
- Do you know Lausanne?
- I've never been abroad

189
00:44:37,380 --> 00:44:40,497
I arrived a few months ago,
a country girl

190
00:44:41,300 --> 00:44:44,895
An old aunt met me at this station

191
00:44:45,940 --> 00:44:50,172
She put me on the right suburban train

192
00:44:51,540 --> 00:44:55,499
The woman I work for at the dairy
collected me

193
00:44:55,700 --> 00:44:59,739
That�s all I've seen of Paris

194
00:45:00,020 --> 00:45:03,649
Emile doesn't take me anywhere.
He's my fianc�

195
00:45:04,060 --> 00:45:06,255
- I know
- You know him?

196
00:45:07,220 --> 00:45:09,575
I know of him

197
00:45:09,860 --> 00:45:14,012
I've seen him... he's often at
your place

198
00:45:19,780 --> 00:45:23,568
You're right, stations are wonderful...

199
00:45:26,020 --> 00:45:29,410
I know pretty much everything
about you

200
00:45:33,780 --> 00:45:39,730
I have a house in Lausanne, not large
but nice, and I own it

201
00:45:39,940 --> 00:45:42,898
I don't use it, not at present

202
00:45:51,540 --> 00:45:53,849
Some day, I'll move there...

203
00:45:54,140 --> 00:45:56,608
...and won't come back

204
00:45:58,540 --> 00:46:02,499
- Why are you laughing?
- I almost didn't come today

205
00:46:03,460 --> 00:46:06,054
I thought we'd have nothing to say...

206
00:46:06,540 --> 00:46:09,008
...but there�s so much

207
00:46:19,460 --> 00:46:22,020
I want to show you something

208
00:46:28,020 --> 00:46:33,731
Just look through it; you'll see my
apartment. I've been there ten years

209
00:46:34,020 --> 00:46:37,410
I'd rather you kept this

210
00:46:45,220 --> 00:46:48,735
- Does Emile know about me?
- No, and he�s not going to

211
00:46:53,020 --> 00:46:57,172
- How can you love that boy?
- I don't know... he�s my fianc�

212
00:46:58,380 --> 00:47:04,171
I don't want to talk about it;
let's just enjoy being together

213
00:47:05,860 --> 00:47:07,737
It's too late for that

214
00:47:10,940 --> 00:47:13,408
Why go on play-acting?

215
00:47:15,540 --> 00:47:20,011
We both know you're not here
for my company

216
00:47:23,780 --> 00:47:25,691
You're intelligent, Alice

217
00:47:27,300 --> 00:47:29,655
Don't say you don't understand

218
00:47:33,300 --> 00:47:36,019
Or do you just not want to?

219
00:47:36,780 --> 00:47:41,729
I'm cold. Won't you take me
in your arms...

220
00:47:44,020 --> 00:47:45,976
...love me a little?

221
00:47:47,380 --> 00:47:50,019
- Could you love two men?
- Why not?

222
00:47:52,060 --> 00:47:54,016
You'd go to any lengths, then?

223
00:48:00,700 --> 00:48:04,170
Kiss me, please say you will

224
00:48:14,300 --> 00:48:19,010
You're so sweet to me...
I can't believe how sweet

225
00:48:21,460 --> 00:48:24,020
All for the charming Emile's sake?

226
00:48:25,540 --> 00:48:29,010
Was I watching that night...

227
00:48:30,620 --> 00:48:32,736
...when he woke you...

228
00:48:33,940 --> 00:48:37,489
...having just killed Pierrette...

229
00:48:38,780 --> 00:48:42,011
...and you hid his bloody raincoat?

230
00:48:43,260 --> 00:48:46,730
Yes, I was watching as usual...

231
00:48:47,780 --> 00:48:51,739
...and I saw everything

232
00:48:57,460 --> 00:49:00,020
Will you still be nice to me...

233
00:49:04,220 --> 00:49:10,011
...still want to take me lovingly
in your arms...

234
00:49:12,780 --> 00:49:17,570
...still let me watch you
as you undress?

235
00:49:19,220 --> 00:49:21,176
Why don't you tell the police?

236
00:49:25,700 --> 00:49:28,168
Because I can't

237
00:49:31,300 --> 00:49:37,739
Some evenings, alone at home,
I find myself unable to stop crying

238
00:49:40,380 --> 00:49:42,336
I've never told anyone this

239
00:49:43,940 --> 00:49:48,331
At those times, I come here

240
00:49:51,060 --> 00:49:53,733
Scents and smells all mingle

241
00:49:55,300 --> 00:50:00,738
I never ask for a particular girl,
perhaps to avoid becoming involved

242
00:50:02,780 --> 00:50:08,571
I sit here, close my eyes and wait

243
00:50:10,780 --> 00:50:16,571
When the girl comes in, I can tell
who it is by her scent

244
00:50:18,940 --> 00:50:20,896
My eyes remain shut

245
00:50:22,460 --> 00:50:27,011
She is close, and gently starts
to undress me

246
00:50:29,220 --> 00:50:33,736
She is naked, with just a towel
around her waist

247
00:50:36,140 --> 00:50:41,419
Sometimes, her breasts lightly brush
my back...

248
00:50:42,220 --> 00:50:44,893
...a furtive sensation I adore...

249
00:50:46,620 --> 00:50:48,656
...the skin so silken

250
00:50:49,460 --> 00:50:52,896
I never know where her lips
will caress me

251
00:50:54,460 --> 00:50:56,894
Then, as my desire mounts...

252
00:50:58,380 --> 00:51:01,895
...she lies beside me,
unknotting the towel...

253
00:51:03,300 --> 00:51:07,737
...and offers herself to me,
legs slightly parted

254
00:51:11,700 --> 00:51:13,736
But that�s over

255
00:51:15,460 --> 00:51:19,897
These women find no favour with me now

256
00:51:21,940 --> 00:51:24,010
Why tell me this?

257
00:51:25,620 --> 00:51:30,455
I happened to see you,
and it's all changed

258
00:51:32,060 --> 00:51:36,417
Soon, I couldn't tear my eyes
from your window

259
00:51:40,780 --> 00:51:43,738
I no longer frequent those women...

260
00:51:46,460 --> 00:51:48,849
...because I'm in love with you

261
00:51:55,540 --> 00:51:57,576
I love you, Alice

262
00:52:04,540 --> 00:52:07,418
That�s why I'll say nothing
to the police

263
00:52:09,620 --> 00:52:12,896
I'd have denounced Emile, but for you

264
00:52:13,140 --> 00:52:16,018
But it was an accident...

265
00:52:18,780 --> 00:52:24,730
Legally, you're an accessory,
and I couldn't bear to lose you

266
00:52:27,300 --> 00:52:29,734
I love you too much

267
00:52:30,060 --> 00:52:34,736
You mustn't love me...
you can't, it's impossible

268
00:52:38,220 --> 00:52:43,738
Don't worry, everything�s going to be
all right, you'll see

269
00:52:50,220 --> 00:52:53,815
- It's late, I'll see you home
- No, I'll go alone

270
00:52:55,940 --> 00:52:57,737
As you wish, Alice

271
00:59:33,940 --> 00:59:38,013
Let me go, there are cops everywhere!

272
00:59:41,460 --> 00:59:44,020
I'll hide with Fran�ois

273
00:59:44,220 --> 00:59:48,338
- What about us?
- Forget it

274
00:59:48,620 --> 00:59:51,930
- You can't leave me
- Can't you see the mess I'm in?

275
00:59:52,220 --> 00:59:53,653
I hate you

276
01:00:07,380 --> 01:00:11,578
Come on, they�re after you!

277
01:00:31,140 --> 01:00:33,813
- You promised me
- You're well out of this

278
01:00:34,060 --> 01:00:38,178
- I'd have followed you anywhere
- You mustn't, Alice

279
01:00:38,380 --> 01:00:40,018
What are you waiting for?

280
01:00:41,540 --> 01:00:43,735
Remember, I love you

281
01:01:17,220 --> 01:01:22,340
I see only one solution.
Will you go away with me?

282
01:01:24,540 --> 01:01:26,576
He isn't worthy of you

283
01:01:27,940 --> 01:01:32,252
I can make you forget him

284
01:01:33,860 --> 01:01:38,888
I'll be patient if you love him
more at first

285
01:01:40,780 --> 01:01:47,015
You will learn to love me in time;
I won't rush you

286
01:01:49,460 --> 01:01:52,816
You can trust me to protect you

287
01:02:01,940 --> 01:02:05,819
No one will ever love you as I do

288
01:02:08,060 --> 01:02:13,657
I'll devote my life to
making you smile again

289
01:02:15,380 --> 01:02:17,177
I love you so much

290
01:02:18,700 --> 01:02:22,136
I'm a man of my word, Alice

291
01:02:22,940 --> 01:02:25,170
I'll never desert you

292
01:02:30,860 --> 01:02:32,896
Here's your ticket

293
01:02:36,620 --> 01:02:39,009
The train leaves at 7.12

294
01:02:42,780 --> 01:02:44,736
I'll wait at the station

295
01:03:13,940 --> 01:03:16,898
- Where did they go?
- Pardon?

296
01:03:17,220 --> 01:03:22,578
Stop putting on your act;
I'm sick of it

297
01:03:24,780 --> 01:03:28,011
Why go on refusing to talk?

298
01:03:31,060 --> 01:03:34,894
Don't you realize it'll be
pinned on you?

299
01:03:39,460 --> 01:03:45,649
Oh hell, why should I care what
happens to a poor fool like you?

300
01:07:34,060 --> 01:07:38,895
I was sure it wasn't you,
but I was wrong

301
01:07:39,940 --> 01:07:42,818
But for her...

302
01:07:43,100 --> 01:07:45,330
...I'd never have known

303
01:07:49,820 --> 01:07:52,937
Cleaning your flat, she found
Pierrette's bag

304
01:07:54,940 --> 01:07:57,010
Hidden in a cupboard

305
01:07:57,860 --> 01:08:01,409
Not very clever... was it,
Mademoiselle?

306
01:08:04,340 --> 01:08:07,013
It's just shock, Monsieur Hire

307
01:08:08,620 --> 01:08:11,657
She didn't see you as a killer

308
01:08:14,380 --> 01:08:18,009
Nor did I, but that�s always the way

309
01:08:19,460 --> 01:08:25,330
The question I must ask is: Do you
admit to having hidden this bag?

310
01:08:30,460 --> 01:08:31,893
Well, Monsieur Hire...

311
01:08:32,220 --> 01:08:34,575
...yes or no?

312
01:08:48,300 --> 01:08:53,499
You'll think me a fool, Alice,
but I don't feel any anger...

313
01:08:56,220 --> 01:08:58,973
...just a deathly sadness

314
01:09:02,380 --> 01:09:04,018
But never mind

315
01:09:09,060 --> 01:09:11,255
You gave me my greatest joy

316
01:09:18,860 --> 01:09:21,090
This isn't funny any more

317
01:09:21,380 --> 01:09:23,018
Bring your case

318
01:12:41,540 --> 01:12:43,337
Dear Inspector

319
01:12:44,540 --> 01:12:46,258
When you read this...

320
01:12:46,540 --> 01:12:49,498
... Alice and I will be far away

321
01:12:50,540 --> 01:12:57,013
You'll find in the locker the raincoat
Emile wore when he killed Pierrette...

322
01:12:57,780 --> 01:13:03,810
... picked up and hidden by me when
he threw it away that night

323
01:13:05,380 --> 01:13:10,408
Technically an accessory,
Alice had nothing to do with it

324
01:13:13,220 --> 01:13:17,816
To protect her innocence,
I'm taking her away

325
01:13:19,300 --> 01:13:22,019
Please don't try to find us

326
01:13:22,860 --> 01:13:27,251
We are happy together...

327
01:13:28,060 --> 01:13:32,736
...and nothing else matters

328
01:13:33,780 --> 01:13:35,259
Monsieur Hire

