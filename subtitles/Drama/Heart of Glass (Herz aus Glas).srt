﻿1
00:00:17,001 --> 00:00:24,584
HEART OF GLASS

2
00:03:47,917 --> 00:03:50,751
I look into the distance,

3
00:03:50,876 --> 00:03:53,334
to the end of the world.

4
00:03:55,001 --> 00:03:57,918
Before the day is over,
the end will come.

5
00:04:00,667 --> 00:04:03,750
First, time will tumble
and then the earth.

6
00:04:05,292 --> 00:04:07,459
The clouds will begin to race.

7
00:04:07,584 --> 00:04:10,709
The earth boils over.
This is the sign.

8
00:04:11,709 --> 00:04:14,167
This is the beginning of the end.

9
00:04:16,084 --> 00:04:18,167
The world's edge begins to crumble,

10
00:04:18,292 --> 00:04:20,625
everything starts to collapse,

11
00:04:21,542 --> 00:04:24,834
tumbles, falls, crumbles and collapses.

12
00:04:26,709 --> 00:04:29,251
I look into the cataract.

13
00:04:29,376 --> 00:04:31,793
I feel an undertow.

14
00:04:31,917 --> 00:04:35,042
It draws me,
it sucks me down.

15
00:04:36,042 --> 00:04:38,334
I begin to fall.

16
00:04:39,417 --> 00:04:43,334
I fall... I am faint from falling.

17
00:05:28,209 --> 00:05:32,792
Now I look at one spot
in the tumbling waters.

18
00:05:33,834 --> 00:05:37,584
I seek one spot
on which my eyes can rest.

19
00:05:39,334 --> 00:05:42,709
I become light, lighter and lighter.

20
00:05:44,834 --> 00:05:47,917
Everything becomes light,
I fly upwards.

21
00:05:53,417 --> 00:05:58,334
Out of the falling and the flying,
a new land rises.

22
00:06:01,084 --> 00:06:05,626
Like the submerged Atlantis,
the earth rises out of the waters.

23
00:06:06,709 --> 00:06:09,001
I see a new earth.

24
00:08:18,292 --> 00:08:21,000
Come here, don't be afraid.

25
00:08:25,001 --> 00:08:27,126
Come here, I said.

26
00:08:38,709 --> 00:08:43,292
The Giant has...
eyes like mill-stones.

27
00:08:51,126 --> 00:08:52,709
Fingers...

28
00:08:55,542 --> 00:08:57,500
...like tree branches.

29
00:09:06,542 --> 00:09:10,375
His nose is a boulder.

30
00:09:16,042 --> 00:09:17,917
The village lives in fear.

31
00:09:18,042 --> 00:09:21,875
Rup says he's seen a Giant.

32
00:09:22,001 --> 00:09:24,918
The time of the Giants
is coming back.

33
00:09:25,792 --> 00:09:29,584
The Giant breaks the trees
and beats our cattle

34
00:09:29,709 --> 00:09:33,584
and tears out our guts
if he sees us.

35
00:09:34,376 --> 00:09:37,001
He's sucking out our brains.

36
00:09:43,084 --> 00:09:47,459
Tell Rup there are no Giants.

37
00:09:49,792 --> 00:09:52,875
Next time he should note
the position of the sun.

38
00:09:54,292 --> 00:09:57,917
The sun was low,
the Giant was just the shadow of a dwarf.

39
00:10:10,292 --> 00:10:14,542
If nothing changes,
you think that's a blessing.

40
00:10:19,251 --> 00:10:21,668
But I see a fire.

41
00:10:24,542 --> 00:10:27,000
And I see the glass factory.

42
00:10:29,917 --> 00:10:32,250
And I'll tell you something else.

43
00:10:33,542 --> 00:10:36,125
Look up at the two bridges.

44
00:10:36,251 --> 00:10:39,584
A liar is about to cross one bridge,
a thief the other.

45
00:11:20,792 --> 00:11:23,334
Tomorrow you'll be finished, Ascherl

46
00:11:45,709 --> 00:11:49,376
I'll sleep off my hangover
on your corpse.

47
00:11:53,751 --> 00:11:57,126
Innkeeper,
bring Wudy another beer.

48
00:12:11,542 --> 00:12:14,084
I'll sleep on your corpse.

49
00:12:21,834 --> 00:12:25,834
Hias said that
I'll sleep on your corpse.

50
00:12:28,042 --> 00:12:30,750
Hias sees into the future.

51
00:12:36,834 --> 00:12:41,417
Provided we sleep in the hay.

52
00:12:42,876 --> 00:12:48,751
And I must fall down
onto the threshing-place first.

53
00:12:50,459 --> 00:12:54,459
Then you must fall on me.

54
00:12:55,334 --> 00:13:00,209
If you don't fall soft,
you'll be dead, too.

55
00:13:25,667 --> 00:13:28,125
Mühlbeck has died

56
00:13:28,251 --> 00:13:32,459
and no one knows
the secret of the Ruby glass.

57
00:13:41,209 --> 00:13:43,792
He knew how to write.

58
00:13:50,834 --> 00:13:53,709
He could easily have written it down.

59
00:13:59,417 --> 00:14:01,625
How the Ruby glass is made.

60
00:14:02,751 --> 00:14:05,834
Have you ever written a word?

61
00:14:10,792 --> 00:14:15,750
But Mühlbeck could have talked,
Mühlbeck could.

62
00:15:55,167 --> 00:15:57,292
My God!

63
00:15:58,959 --> 00:16:01,459
That was the second glass.

64
00:16:08,126 --> 00:16:11,251
This splendour is now relinquished
from the world.

65
00:16:20,667 --> 00:16:24,375
Now what will protect me now
from the Evil of the universe?

66
00:17:47,584 --> 00:17:50,167
You wouldn't dare.

67
00:17:51,751 --> 00:17:53,751
I would.

68
00:19:09,376 --> 00:19:12,084
Father, have you seen Adalbert?

69
00:20:07,792 --> 00:20:09,959
Naked in bed again...

70
00:20:10,084 --> 00:20:12,001
Come on, get dressed!

71
00:20:58,042 --> 00:21:01,375
Glass has a fragile soul.

72
00:21:02,417 --> 00:21:04,584
It is unstained.

73
00:21:05,501 --> 00:21:08,459
A crack is the Sin.

74
00:21:08,584 --> 00:21:11,834
After the Sin, there is no sound.

75
00:21:40,751 --> 00:21:46,418
Will the future see the necessary fall
of the factories...

76
00:21:48,751 --> 00:21:52,501
...just as we see the ruined fortress
as a sign of inevitable change?

77
00:22:07,042 --> 00:22:10,834
People say that Hias has seen to it,

78
00:22:10,959 --> 00:22:16,292
that nettles are springing out of
the glass factories.

79
00:22:16,417 --> 00:22:22,792
The lilac bushes...will pine
for the company of men, they say.

80
00:22:26,376 --> 00:22:28,918
The Ruby must save us.

81
00:22:30,501 --> 00:22:32,668
Tear Mühlbeck's house down

82
00:22:32,792 --> 00:22:35,459
and look for the secret
in every crevice.

83
00:22:38,209 --> 00:22:44,084
Dig three feet into the earth
on which his house stood,

84
00:22:46,001 --> 00:22:49,251
for Mühlbeck
could have buried his secret.

85
00:22:51,709 --> 00:22:57,084
Bring me the green davenport
that he gave to his mother, Anamirl.

86
00:23:05,501 --> 00:23:09,043
The untidiness of the stars
makes my head ache.

87
00:23:36,917 --> 00:23:40,667
The master does not want
his breakfast now.

88
00:23:40,792 --> 00:23:42,959
Ludmilla may wear her hair down today.

89
00:23:43,876 --> 00:23:45,834
Yes, she may wear her hair down.

90
00:23:46,459 --> 00:23:48,917
A favour for the servant.

91
00:24:58,834 --> 00:25:00,917
Father,

92
00:25:01,042 --> 00:25:04,834
glass-blower Gigl thinks
he has the secret of Ruby glass.

93
00:25:06,626 --> 00:25:09,376
This is the day to stand up.

94
00:25:11,792 --> 00:25:15,417
No, I'd sooner stay in my chair.

95
00:25:16,292 --> 00:25:20,875
I feel as though my backbone
had crumbled.

96
00:25:21,001 --> 00:25:23,251
Your backbone is not soft.

97
00:25:24,584 --> 00:25:26,709
Your backbone is not soft.

98
00:25:27,626 --> 00:25:31,251
You won't collapse
like a heap of stones.

99
00:25:32,251 --> 00:25:35,084
For twelve years
you have been sitting in this chair.

100
00:25:35,209 --> 00:25:37,001
For twelve years!

101
00:25:44,917 --> 00:25:48,709
For twelve years
I've shown you your shoes.

102
00:25:48,834 --> 00:25:50,959
For twelve years!

103
00:26:01,417 --> 00:26:03,042
Fine!

104
00:26:03,876 --> 00:26:05,793
I'll have you carried, as usual.

105
00:26:26,751 --> 00:26:30,168
Are you sure?
Is this really Ruby glass?

106
00:26:30,292 --> 00:26:31,709
Yes.

107
00:26:40,834 --> 00:26:43,126
Is this really Ruby glass?

108
00:27:12,084 --> 00:27:14,376
That's supposed to be Ruby?

109
00:27:15,959 --> 00:27:18,209
Another one.

110
00:29:06,792 --> 00:29:09,125
Should I leave this place?

111
00:29:10,417 --> 00:29:13,125
Madness is rampant in the village.

112
00:29:14,584 --> 00:29:17,251
The factory owner
wants a new furnace,

113
00:29:18,334 --> 00:29:20,834
but there are no builders.

114
00:29:31,667 --> 00:29:35,000
Suddenly, I see a fire
flowing in the brook...

115
00:29:36,209 --> 00:29:38,584
...and the wind pushing the fire on.

116
00:29:39,959 --> 00:29:43,334
I see the trees burning like matchsticks.

117
00:29:45,584 --> 00:29:48,542
I see many people running up a hill.

118
00:29:49,709 --> 00:29:53,042
Breathless, they stop at the top

119
00:29:53,167 --> 00:29:55,250
and, paralysed, they turn to stone.

120
00:29:56,376 --> 00:29:58,418
One beside the other.

121
00:29:59,251 --> 00:30:01,584
A whole forest of stone.

122
00:30:05,251 --> 00:30:07,918
Then it becomes dark and still.

123
00:30:09,626 --> 00:30:12,751
Down below, everything has perished.

124
00:30:14,126 --> 00:30:16,668
These is no man, no house.

125
00:30:17,751 --> 00:30:19,876
Only some debris.

126
00:30:26,084 --> 00:30:32,209
Yes, and then I see someone
running on the road below

127
00:30:32,334 --> 00:30:35,917
with a burning branch in his hand,
crying,

128
00:30:36,042 --> 00:30:40,625
"Am I really the last one,
the only one left?"

129
00:34:47,917 --> 00:34:50,000
Send for the herdsman Hias,

130
00:34:50,126 --> 00:34:53,793
that he may see the mystery
of the Ruby glass.

131
00:35:06,542 --> 00:35:10,459
Even if we must tear Mühlbeck
from his grave

132
00:35:10,584 --> 00:35:14,126
so that Hias can read his brain.

133
00:35:18,542 --> 00:35:21,375
How strange,
a whole town made of glass...

134
00:35:22,501 --> 00:35:24,751
...with people living in it.

135
00:35:24,876 --> 00:35:28,084
How can people live
in glass houses?

136
00:35:30,501 --> 00:35:32,418
Here's the church.

137
00:35:32,542 --> 00:35:36,667
Animals live in the church,
all kinds of animals.

138
00:35:36,792 --> 00:35:40,834
Hares, chickens, deer,

139
00:35:40,959 --> 00:35:43,834
birds, cows,

140
00:35:45,292 --> 00:35:48,042
but there are no people in the church.

141
00:35:49,292 --> 00:35:52,042
The streets are deserted.

142
00:35:52,167 --> 00:35:54,417
Everything is covered in snow.

143
00:36:21,167 --> 00:36:24,375
Let it be.
There is more to break today.

144
00:36:27,376 --> 00:36:29,751
Leave the mansion.

145
00:36:30,542 --> 00:36:34,792
The master could slip
and end up sitting on your face.

146
00:36:53,542 --> 00:36:55,750
Why the snivelling?

147
00:37:02,376 --> 00:37:08,293
Better that the servant prays for us
to find the secret of the Ruby.

148
00:37:13,292 --> 00:37:15,417
So much will happen.

149
00:37:15,542 --> 00:37:17,625
Hias is outside.

150
00:37:18,292 --> 00:37:20,459
Already?

151
00:37:25,751 --> 00:37:27,626
He knew?

152
00:37:27,751 --> 00:37:30,043
He didn't need a messenger?

153
00:37:31,042 --> 00:37:35,209
Master, please send a hunter
to slay the bear.

154
00:37:45,126 --> 00:37:48,376
The bulls are frightened

155
00:37:48,501 --> 00:37:54,209
and Sam and I can't promise
that he won't kill a bull

156
00:37:55,542 --> 00:37:57,792
or cause a stampede.

157
00:37:59,792 --> 00:38:03,750
On the day of the bear,
a bull will run as far as Mainz.

158
00:38:05,542 --> 00:38:07,500
Mühlbeck has died.

159
00:38:12,084 --> 00:38:14,792
He has taken his secret with him.

160
00:38:27,417 --> 00:38:30,292
You are to see
the secret of the Ruby.

161
00:38:31,084 --> 00:38:33,251
Mühlbeck has abandoned us.

162
00:38:37,626 --> 00:38:39,918
I don't know the ingredient.

163
00:38:41,084 --> 00:38:43,959
You know it for ten florins.

164
00:38:46,751 --> 00:38:49,459
Then you know it for a thousand.

165
00:38:51,376 --> 00:38:54,918
Do you want our people
to eat oatmeal bread again,

166
00:38:55,042 --> 00:38:57,375
that gives them headaches?

167
00:38:59,542 --> 00:39:01,709
Then tell me the secret

168
00:39:02,792 --> 00:39:07,375
so we can make Ruby glass again.
You can become foreman.

169
00:39:08,751 --> 00:39:11,626
And I'll carry a millstone to Trier.

170
00:39:17,001 --> 00:39:19,334
I've only come because of the hunter.

171
00:39:21,459 --> 00:39:23,542
I want the Ruby again.

172
00:39:25,542 --> 00:39:28,709
I want the red glass, understand?

173
00:39:30,417 --> 00:39:33,084
I need a glass to contain my blood

174
00:39:33,209 --> 00:39:35,542
or it will trickle away.

175
00:39:41,042 --> 00:39:43,750
The sun is hurting me.

176
00:39:45,959 --> 00:39:48,167
You will never see the sun again.

177
00:39:48,834 --> 00:39:51,667
Rats will bite your ear lobes.

178
00:40:20,959 --> 00:40:23,834
Wudy is dead.

179
00:40:23,959 --> 00:40:25,459
No!

180
00:40:25,584 --> 00:40:28,209
Ascherl's the one
who's been nabbed.

181
00:40:28,334 --> 00:40:35,667
Hias predicted
that the one underneath falls first.

182
00:40:35,792 --> 00:40:37,375
And look,

183
00:40:37,501 --> 00:40:40,793
the other falls on top of him,
he falls softly.

184
00:40:40,917 --> 00:40:43,209
Wudy survived it.

185
00:40:45,584 --> 00:40:47,417
Let's separate them.

186
00:41:03,001 --> 00:41:07,709
The one whose arm falls first
is the dead man.

187
00:41:13,209 --> 00:41:15,334
I'll be damned!

188
00:41:15,459 --> 00:41:18,501
Don't curse in the face of the dead.

189
00:41:45,876 --> 00:41:48,334
Now we know.

190
00:42:05,084 --> 00:42:09,167
What Hias foresees, happens.

191
00:43:17,376 --> 00:43:19,668
It's Toni.

192
00:43:27,834 --> 00:43:33,084
You buried our foreman, Mühlbeck,
the week before last.

193
00:43:34,417 --> 00:43:36,875
Now they don't know what to do.

194
00:43:39,376 --> 00:43:41,876
Hias predicted it.

195
00:43:43,709 --> 00:43:46,251
You know about the Ruby glass?

196
00:43:50,251 --> 00:43:55,626
The Ruby is the master's malady.

197
00:44:11,376 --> 00:44:13,918
The davenport is here.

198
00:44:14,042 --> 00:44:16,292
Bring it in.

199
00:44:29,084 --> 00:44:31,959
I'm delighted with this letter.

200
00:44:32,084 --> 00:44:35,001
Adalbert, give me the letter-opener.

201
00:44:53,626 --> 00:44:56,584
We'll read this message.

202
00:44:56,709 --> 00:44:59,126
Can he decipher that?

203
00:45:11,251 --> 00:45:17,584
If a letter reaches someone
with the words scattered around...

204
00:45:21,417 --> 00:45:24,084
...it should make you think.

205
00:45:46,084 --> 00:45:48,959
Are you going up to the woods again?

206
00:45:57,376 --> 00:45:59,876
I don't need the flour now.

207
00:46:01,876 --> 00:46:05,501
I see that I only get away
when the snow lies.

208
00:46:10,042 --> 00:46:14,375
I'll put it back, then.

209
00:46:41,792 --> 00:46:44,167
Your Mühlbecks dead.

210
00:47:05,417 --> 00:47:07,834
They've taken away your davenport.

211
00:47:23,792 --> 00:47:26,209
Yes, he always sat there.

212
00:47:36,459 --> 00:47:39,876
When night falls,
the people will die.

213
00:47:52,792 --> 00:47:55,000
By day the rain pours...

214
00:47:57,709 --> 00:48:00,126
...yet the land is drying up.

215
00:48:31,001 --> 00:48:36,209
You have been invited because
you describe the Ruby so beautifully.

216
00:48:37,834 --> 00:48:39,876
I can't hear enough of it.

217
00:48:44,917 --> 00:48:47,292
The land of the Ruby.

218
00:48:48,501 --> 00:48:50,751
My land.

219
00:48:54,459 --> 00:48:56,792
And all the people...

220
00:48:58,709 --> 00:49:01,126
...dance in the red glow...

221
00:49:02,917 --> 00:49:04,917
...and live in it.

222
00:49:07,792 --> 00:49:10,042
Their blood, their life.

223
00:49:11,626 --> 00:49:14,001
Everything is in the glass.

224
00:49:15,292 --> 00:49:18,792
In the red, in the colour.

225
00:49:20,501 --> 00:49:25,168
This land is the only one.

226
00:49:27,792 --> 00:49:30,500
Everything is in this land

227
00:49:30,667 --> 00:49:32,917
and everything is Ruby.

228
00:50:08,917 --> 00:50:11,459
Your prayer has worked a miracle.

229
00:50:13,209 --> 00:50:16,667
For an hour, I have known
something I never knew.

230
00:50:18,626 --> 00:50:21,751
I can sell my secret
to all glassworks.

231
00:50:25,459 --> 00:50:27,417
Break all you can.

232
00:50:31,126 --> 00:50:34,501
I shall have ten racks of Ruby glass
taken to the Arber

233
00:50:34,667 --> 00:50:38,500
.and thrown into the lake
so it is dyed red.

234
00:50:40,417 --> 00:50:43,167
Adalbert, did you understand?

235
00:50:46,167 --> 00:50:49,625
The lake is dyed red.

236
00:50:50,459 --> 00:50:53,126
The glass for the lake
must go this very day.

237
00:50:54,126 --> 00:50:57,293
Take everything from the store.

238
00:51:05,334 --> 00:51:07,709
And another thing.

239
00:51:09,084 --> 00:51:13,501
Restuff the davenport and mend it

240
00:51:15,376 --> 00:51:19,501
and take it to Anamirl
with ten florins as compensation

241
00:51:22,584 --> 00:51:27,501
Tell her I no longer wish
her dead Mühlbeck to go to the devil.

242
00:51:29,251 --> 00:51:32,709
May he be surrounded
by a host of angels.

243
00:52:08,459 --> 00:52:10,667
I have it.

244
00:52:23,209 --> 00:52:25,834
It is here.

245
00:52:29,084 --> 00:52:31,084
Here.

246
00:52:35,501 --> 00:52:37,834
And inside here.

247
00:52:55,584 --> 00:52:57,292
We all...

248
00:53:05,709 --> 00:53:09,584
I've already sent for
the Pléssberg furnace builders.

249
00:53:25,501 --> 00:53:28,834
The mistress will be surprised
when she returns.

250
00:53:30,751 --> 00:53:33,001
She'll see nothing left standing.

251
00:53:33,834 --> 00:53:35,501
Lunatic.

252
00:53:36,167 --> 00:53:40,584
When she dismounts,
she'll fall into the mud.

253
00:53:40,709 --> 00:53:43,167
And you'll be on a big boat, puking.

254
00:53:43,292 --> 00:53:44,792
Anything else?

255
00:53:44,917 --> 00:53:47,834
Will there be any free beer today?

256
00:53:57,167 --> 00:54:00,125
The master's out of order.

257
00:54:00,251 --> 00:54:04,001
And in the glassworks
there's been Beelzebub.

258
00:54:04,126 --> 00:54:07,626
He sent out ten men
with racks of glass,

259
00:54:08,667 --> 00:54:12,084
but they weren't so stupid
as to throw valuable glass in the lake.

260
00:54:13,459 --> 00:54:16,251
They'll smuggle it
over the border and sell it.

261
00:54:19,209 --> 00:54:22,459
Ludmilla, go before he wants
something from you.

262
00:54:23,709 --> 00:54:26,667
Ludmilla, make yourself beautiful.

263
00:54:26,792 --> 00:54:29,167
The master wants your company.

264
00:54:30,292 --> 00:54:32,917
I must provide some music.

265
00:54:34,167 --> 00:54:38,875
In the meadow there'll be someone
who can play the hurdy-gurdy.

266
00:54:39,001 --> 00:54:41,501
But I'd have to sing with it.

267
00:54:43,292 --> 00:54:45,584
Harp-Tom is in the inn.

268
00:54:45,709 --> 00:54:49,501
Tell him to come,
it will be to his advantage.

269
00:55:30,417 --> 00:55:34,667
Now you've a soft seat again.

270
00:56:47,376 --> 00:56:51,501
The night is coming on slowly today.

271
00:56:54,334 --> 00:56:57,042
It creeps into the village

272
00:56:57,167 --> 00:57:00,834
and in the stables
people softly cuddle with the animals.

273
00:57:04,167 --> 00:57:07,292
In the factory they're working again,
against fear,

274
00:57:08,167 --> 00:57:10,667
but they know their work is in vain.

275
00:57:12,376 --> 00:57:15,668
I told them, the factory
will burn down in the night.

276
00:57:17,751 --> 00:57:21,709
But, like sleep-walkers,
people walk towards their doom.

277
01:06:02,792 --> 01:06:04,584
And then?

278
01:06:06,876 --> 01:06:09,251
Then the little one starts a war

279
01:06:10,709 --> 01:06:13,667
and the big one across the ocean
extinguishes it.

280
01:06:16,001 --> 01:06:19,334
Then you won't get a loaf
for two hundred florins.

281
01:06:23,084 --> 01:06:25,417
Then a strict master comes,

282
01:06:26,792 --> 01:06:31,500
who takes people's shirts
and their skin with them.

283
01:06:35,334 --> 01:06:38,376
After the War
you think there'll be Peace,

284
01:06:39,709 --> 01:06:41,459
but there won't be.

285
01:06:50,209 --> 01:06:52,167
Drink up.

286
01:06:58,126 --> 01:07:00,501
I miss Ascherl

287
01:07:01,792 --> 01:07:03,917
You shouldn't have smothered him.

288
01:07:07,876 --> 01:07:09,626
I miss Ascherl

289
01:07:12,209 --> 01:07:14,001
He should have been here today.

290
01:07:14,626 --> 01:07:19,793
Can't you go out to him?
He can't come inside.

291
01:07:29,167 --> 01:07:31,500
Bring me some Ascherl

292
01:07:46,417 --> 01:07:50,167
Even if I believe all you say,
I don't believe that.

293
01:07:51,626 --> 01:07:54,959
Believe it or not, that's your affair.

294
01:07:55,084 --> 01:07:58,126
I tell what I see.
I don't know if it will happen.

295
01:08:01,251 --> 01:08:02,751
And then?

296
01:08:09,501 --> 01:08:14,084
The peasants will dress
like townspeople

297
01:08:14,209 --> 01:08:16,876
and the townspeople will be like apes.

298
01:08:19,626 --> 01:08:22,459
Women will wear trousers and boots.

299
01:08:25,417 --> 01:08:29,459
The peasants will stand in their dung heaps
with polished boots.

300
01:08:32,334 --> 01:08:35,626
The peasants will eat cake
and discuss politics.

301
01:08:59,001 --> 01:09:00,959
Who'll play for us?

302
01:09:01,667 --> 01:09:04,917
I'll play for these two.

303
01:09:56,167 --> 01:09:58,500
Keep playing!

304
01:10:31,751 --> 01:10:33,668
They all fight.

305
01:10:34,501 --> 01:10:36,793
There is War in every house.

306
01:10:38,001 --> 01:10:40,376
No man will like another man.

307
01:10:41,917 --> 01:10:44,667
The elegant and fine people
will be murdered.

308
01:10:45,917 --> 01:10:48,500
He who has smooth hands
will be slain.

309
01:10:50,167 --> 01:10:54,167
The peasants will encircle their houses
with a high fence

310
01:10:54,292 --> 01:10:57,709
and shoot at the townspeople
from their windows.

311
01:10:57,834 --> 01:11:00,376
The townspeople will beg,
"Let me plough the ground",

312
01:11:00,501 --> 01:11:02,459
but they will be slain.

313
01:11:03,584 --> 01:11:06,001
No man will like another man.

314
01:11:07,501 --> 01:11:10,459
If two people are on one bench

315
01:11:10,584 --> 01:11:13,292
and one asks the other
to move over

316
01:11:13,417 --> 01:11:17,209
and the other doesn't,
it'll be his death.

317
01:11:18,292 --> 01:11:21,292
That will be the time
of the Clearing of the Benches.

318
01:11:32,667 --> 01:11:34,959
She is cooling off,

319
01:11:35,084 --> 01:11:37,251
then she won't crack any more.

320
01:12:06,292 --> 01:12:09,167
That's the pure ingredient.

321
01:12:21,501 --> 01:12:24,209
What are factories still good for?

322
01:13:40,126 --> 01:13:45,126
Paulin must dance

323
01:13:45,251 --> 01:13:48,876
naked on the table!

324
01:14:07,584 --> 01:14:11,042
You won't be able to tell
between summer and winter.

325
01:14:12,001 --> 01:14:14,584
Everyone will have a different head

326
01:14:14,709 --> 01:14:18,376
and the forest will get sparse,
like the beggar's gown.

327
01:14:20,001 --> 01:14:22,501
The Small will be tall again.

328
01:14:24,376 --> 01:14:27,959
When the Redcoats come
with their red coats,

329
01:14:28,084 --> 01:14:31,334
then you must run fast

330
01:14:31,459 --> 01:14:34,501
and take a loaf of bread with you.

331
01:14:36,292 --> 01:14:40,459
Whoever has three loaves
and drops one on the way

332
01:14:40,584 --> 01:14:43,084
must not stoop.

333
01:14:43,709 --> 01:14:47,709
Even if you lose the second one,
you must leave it behind.

334
01:14:47,834 --> 01:14:51,376
You can subsist with one loaf

335
01:14:51,501 --> 01:14:53,751
because it doesn't last long.

336
01:14:56,251 --> 01:15:00,751
The few who survive
must have an iron head.

337
01:15:02,667 --> 01:15:07,209
People become ill,
but no one can help them.

338
01:15:08,501 --> 01:15:11,459
Those who survive,
will stick together

339
01:15:11,584 --> 01:15:13,917
and call each other Brother and Sister.

340
01:15:15,334 --> 01:15:19,084
Take everything off,
so we see something!

341
01:15:49,292 --> 01:15:51,334
The factory is on fire!

342
01:15:51,459 --> 01:15:54,334
The factory is on fire!

343
01:17:05,667 --> 01:17:08,709
If it's burning,
I'd like to be there.

344
01:17:09,501 --> 01:17:11,626
Where are my shoes?

345
01:17:11,751 --> 01:17:14,084
Where are my shoes?

346
01:17:25,917 --> 01:17:29,292
For twelve years
there's been no fire.

347
01:17:29,417 --> 01:17:31,084
Twelve years!

348
01:17:31,209 --> 01:17:34,751
And my shoes are gone.

349
01:17:55,334 --> 01:17:59,501
People settle down as though
never wanting to leave this world.

350
01:18:01,251 --> 01:18:05,334
But overnight
the Clearing of the World begins.

351
01:18:12,667 --> 01:18:16,167
From the orient a huge bird appears

352
01:18:16,292 --> 01:18:18,334
and shits into the sea.

353
01:18:19,501 --> 01:18:22,334
The sea rises as high as a house
and boils.

354
01:18:22,459 --> 01:18:26,001
The earth trembles
and a big island half drowns.

355
01:18:27,042 --> 01:18:30,500
The big city with the iron tower
is in flames,

356
01:18:31,584 --> 01:18:37,417
but the fire was started by its own men
and the city is levelled.

357
01:18:39,626 --> 01:18:44,418
In Italy the clergy is killed
and the churches collapse.

358
01:18:46,251 --> 01:18:48,793
The Pope sits in a cell.

359
01:18:51,251 --> 01:18:54,459
During his flight
he consecrates a goat as bishop.

360
01:18:57,167 --> 01:18:59,417
People starve.

361
01:19:00,709 --> 01:19:04,126
The three days of darkness
draw nearer.

362
01:19:09,959 --> 01:19:12,834
Where the black box drops,

363
01:19:12,959 --> 01:19:16,251
a green and yellow dust arises.

364
01:19:18,792 --> 01:19:21,042
The weather will change.

365
01:19:22,792 --> 01:19:25,417
Vineyards will be grown here

366
01:19:26,959 --> 01:19:29,709
and there is fruit I don't know.

367
01:19:44,251 --> 01:19:46,168
Ludmilla!

368
01:19:46,292 --> 01:19:49,584
Ludmilla lies dead
in the master's office

369
01:19:51,001 --> 01:19:55,376
and Toni plays her
one song after another on his harp.

370
01:20:52,834 --> 01:20:56,417
Hias has wished this calamity on us.

371
01:20:57,126 --> 01:20:58,584
I merely predicted it.

372
01:20:58,709 --> 01:21:02,626
He has Devil's eyes,
he has the Evil Eye. Lock him up!

373
01:21:45,167 --> 01:21:47,084
I do not see any more.

374
01:21:50,667 --> 01:21:52,584
It is so dark.

375
01:21:53,667 --> 01:21:55,959
I must see something again.

376
01:22:05,334 --> 01:22:07,792
I must go to the woods.

377
01:22:07,917 --> 01:22:09,917
I have to go to the woods.

378
01:22:24,667 --> 01:22:26,917
I want to see the woods again.

379
01:22:30,667 --> 01:22:33,042
Don't you want to see any people?

380
01:22:34,917 --> 01:22:36,834
I like you.

381
01:22:38,126 --> 01:22:40,501
You have a heart of glass.

382
01:25:05,959 --> 01:25:08,376
And now I'm going to roast the bear.

383
01:25:29,876 --> 01:25:34,209
In the night someone looks across
the forest and sees not a single light.

384
01:25:36,126 --> 01:25:40,251
And when he sees
a juniper bush in the twilight,

385
01:25:40,376 --> 01:25:43,501
he runs to see
if it isn't a human being.

386
01:25:44,626 --> 01:25:46,918
There are so few left.

387
01:25:48,792 --> 01:25:52,084
In the forest, roosters crow,
but the people have perished.

388
01:26:01,084 --> 01:26:03,792
Something else occurs to me.

389
01:26:03,917 --> 01:26:05,417
I see it again.

390
01:26:07,417 --> 01:26:10,792
A coachman stops
and gets down from his coach.

391
01:26:12,209 --> 01:26:15,209
He knocks down on the ground
with his whip and says,

392
01:26:16,417 --> 01:26:19,625
"There was once
the big town of Straubing."

393
01:26:25,834 --> 01:26:28,501
Now I see the rocky island again.

394
01:26:33,917 --> 01:26:36,417
I see it quite clearly.

395
01:26:43,584 --> 01:26:47,501
A rocky island far out in the sea

396
01:26:47,667 --> 01:26:50,000
and a second, smaller island.

397
01:26:51,292 --> 01:26:54,542
They lie on the far edge
of the inhabited world.

398
01:26:57,376 --> 01:27:02,376
On one island, for centuries,
some forgotten men have lived.

399
01:27:03,626 --> 01:27:07,918
And because they live on the far edge
of the inhabited world,

400
01:27:08,042 --> 01:27:12,709
word has not reached them
that the earth is round.

401
01:27:12,834 --> 01:27:18,292
They have retained the belief
that the earth is flat

402
01:27:18,417 --> 01:27:22,792
and that the ocean far beyond
ends in a yawning abyss.

403
01:27:28,084 --> 01:27:31,542
I see a man on top of the rock.

404
01:27:33,042 --> 01:27:37,084
For years he stood alone,
looking out over the sea,

405
01:27:37,209 --> 01:27:41,001
day after day,
always in the same place.

406
01:27:42,751 --> 01:27:45,293
He is the first one to doubt.

407
01:28:28,501 --> 01:28:32,876
Then, years later,
three other men join him.

408
01:28:34,209 --> 01:28:38,751
For many years they gaze
across the sea from the rock.

409
01:28:42,459 --> 01:28:48,917
Then, one day,
they decide to risk the ultimate.

410
01:28:49,042 --> 01:28:53,834
They want to reach the edge of the world,
to see if there is really an abyss.

411
01:28:57,251 --> 01:29:00,251
Musicians accompany their departure.

412
01:30:16,792 --> 01:30:19,834
Then the men set out,

413
01:30:19,959 --> 01:30:22,751
pathetic and senseless.

414
01:30:22,876 --> 01:30:25,209
In a boat that is far too small.

415
01:32:20,542 --> 01:32:28,500
It may have seemed like a sign of hope

416
01:32:28,667 --> 01:32:39,667
that the birds followed them
out into the vastness of the sea.

