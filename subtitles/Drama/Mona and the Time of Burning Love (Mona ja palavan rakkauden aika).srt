﻿1
01:18:02,928 --> 01:18:08,934
I was in a village... called San Remo...

2
01:18:11,020 --> 01:18:14,940
...the Krauts pounded us hard...

3
01:18:15,107 --> 01:18:16,650
...me & Stan...

4
01:18:16,901 --> 01:18:21,614
...you remember Stan? We were the
only two left from our platoon.

5
01:18:21,906 --> 01:18:29,246
We heard a distant cry coming
from an abandoned shed.

6
01:18:30,039 --> 01:18:31,624
When night fell...

7
01:18:32,291 --> 01:18:33,626
A trap it was?

8
01:18:36,962 --> 01:18:38,464
Remember...

9
01:18:39,256 --> 01:18:41,800
...invert your nouns and
your verbs.

