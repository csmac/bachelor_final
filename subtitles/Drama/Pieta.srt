﻿1
00:00:18,561 --> 00:00:20,646
<i>KIM Ki-duk Film presents
in association with NEW</i>

2
00:00:21,522 --> 00:00:23,482
<i>a KIM Ki-duk Film production.</i>

3
00:00:24,483 --> 00:00:26,277
<i>CHO Min-soo LEE Jung-jin</i>

4
00:00:27,319 --> 00:00:29,155
<i>executive producers
KIM Ki-duk, KIM Woo-taek</i>

5
00:00:30,239 --> 00:00:32,116
<i>producer KIM Soon-mo</i>

6
00:00:33,159 --> 00:00:35,161
<i>written and directed by
KIM Ki-duk.</i>

7
00:00:36,078 --> 00:00:38,372
<i>The 18th film by KIM Ki-duk.</i>

8
00:01:16,243 --> 00:01:16,743
<i>PIETA.</i>

9
00:02:16,428 --> 00:02:18,889
<i>HALLELUJAH FOREVER.</i>

10
00:04:20,928 --> 00:04:23,597
Oh Shit.

11
00:04:27,476 --> 00:04:29,561
Hello? Won-bong?

12
00:04:30,229 --> 00:04:31,729
It's Hoon-chul. Shit.

13
00:04:31,730 --> 00:04:33,857
Lend me some money.

14
00:04:34,692 --> 00:04:35,859
Right now, man!

15
00:04:37,278 --> 00:04:39,571
Or I'm gonna be a cripple
today!

16
00:04:41,240 --> 00:04:43,659
Sell your house and help me!

17
00:04:44,576 --> 00:04:47,454
Hello? Won-bong! Hey!

18
00:04:47,830 --> 00:04:49,665
He'll be here soon!

19
00:04:49,790 --> 00:04:52,792
Shit, he can't kill me
for just $3,000.

20
00:04:52,793 --> 00:04:54,753
It's now $30,000!

21
00:04:54,837 --> 00:04:58,257
How can the interest be
10 times in 3 months!

22
00:04:58,674 --> 00:05:01,759
He said he'll injure you and
take the insurance claim.

23
00:05:01,760 --> 00:05:06,056
That's why they lent it to us
when we got nothing.

24
00:05:06,640 --> 00:05:08,851
Why'd you sign the insurance!

25
00:05:08,976 --> 00:05:10,644
I had no other choice!

26
00:05:11,145 --> 00:05:13,980
I wish I could sell
even you right now!

27
00:05:13,981 --> 00:05:15,857
Hey! Hey!

28
00:05:15,858 --> 00:05:18,235
Do something! You used it, too!

29
00:05:18,569 --> 00:05:20,279
But only I become a cripple?

30
00:05:20,779 --> 00:05:23,782
You can't. Then, I'll have to
support you.

31
00:05:27,911 --> 00:05:29,163
Stop it!

32
00:05:41,800 --> 00:05:42,425
Get up.

33
00:05:42,426 --> 00:05:45,095
- What are you doing?
- Hold still.

34
00:05:45,220 --> 00:05:47,389
At a time like this?
Aren't you scared?

35
00:05:47,473 --> 00:05:49,141
It's cuz I'm terrified!

36
00:06:12,664 --> 00:06:14,374
Shit.

37
00:06:14,375 --> 00:06:15,459
Who the hell?

38
00:06:30,474 --> 00:06:32,017
Son of a bitch.

39
00:06:34,061 --> 00:06:35,854
Ah... yes...

40
00:06:51,036 --> 00:06:53,789
- What are you doing!
- Stay outside.

41
00:06:53,872 --> 00:06:55,541
What are you trying to pull?

42
00:06:55,874 --> 00:06:57,584
Stay out if you don't
wanna be a cripple.

43
00:07:04,049 --> 00:07:05,008
Myung-ja!

44
00:07:06,093 --> 00:07:09,680
No, Myung-ja! Don't do this!

45
00:07:11,265 --> 00:07:15,561
<i>Myung-ja! Please no! Oh shit.</i>

46
00:07:16,603 --> 00:07:19,731
Do whatever you want
and give us another week.

47
00:07:20,566 --> 00:07:22,860
If he becomes a cripple,
we're dead.

48
00:08:06,153 --> 00:08:09,448
You son of a bitch!

49
00:08:15,913 --> 00:08:17,122
Please don't!

50
00:08:17,956 --> 00:08:20,083
Please don't!

51
00:08:25,172 --> 00:08:27,174
Please don't!

52
00:08:59,289 --> 00:09:00,916
Damn piece of shit!

53
00:09:02,000 --> 00:09:03,669
You'll pay for this!

54
00:09:09,007 --> 00:09:12,678
Irresponsibly borrowing
and not paying up.

55
00:09:13,762 --> 00:09:15,806
Folks like you are shit!

56
00:09:55,387 --> 00:09:56,972
Shit!

57
00:09:59,391 --> 00:10:01,059
The hell!

58
00:11:43,328 --> 00:11:44,663
The hell are you doing?

59
00:12:03,306 --> 00:12:04,683
Who the hell are you?

60
00:12:16,528 --> 00:12:19,197
Crazy bitch! Get out!

61
00:12:29,416 --> 00:12:31,168
What the fuck!

62
00:12:53,148 --> 00:12:54,357
Got a grudge on me?

63
00:13:00,280 --> 00:13:01,072
Stab me.

64
00:13:01,656 --> 00:13:02,574
Go ahead!

65
00:14:32,789 --> 00:14:33,665
Kang-do!

66
00:14:39,838 --> 00:14:40,922
Lee Kang-do.

67
00:14:48,138 --> 00:14:50,348
I'm sorry I abandoned you.

68
00:14:54,060 --> 00:14:55,979
The hell did you just say?

69
00:14:58,189 --> 00:15:00,442
Forgive me for coming so late.

70
00:15:00,984 --> 00:15:02,903
The fuck are you saying!

71
00:15:10,368 --> 00:15:11,870
Forgive me.

72
00:15:12,704 --> 00:15:15,373
The fuck is this! Crazy bitch!

73
00:15:16,666 --> 00:15:17,709
Kang-do...

74
00:15:19,336 --> 00:15:21,630
Fuck! Don't say my name!

75
00:15:28,845 --> 00:15:29,971
Kang-do.

76
00:15:30,513 --> 00:15:33,224
Bitch! Don't say my name!
I got no one!

77
00:15:39,814 --> 00:15:43,109
Please forgive me.

78
00:15:46,321 --> 00:15:47,781
Crazy bitch!

79
00:16:20,772 --> 00:16:24,608
Just leave it.
I'll do it later.

80
00:16:24,609 --> 00:16:26,945
It's okay. I'll do it.

81
00:16:27,320 --> 00:16:29,489
Go home and rest.

82
00:16:30,949 --> 00:16:32,158
Mom? Look at me.

83
00:16:34,035 --> 00:16:34,911
Turn your head.

84
00:16:41,292 --> 00:16:42,293
It's pretty.

85
00:16:46,965 --> 00:16:47,799
It's warm.

86
00:16:58,810 --> 00:16:59,728
Here.

87
00:17:13,158 --> 00:17:15,493
That's all I got.

88
00:17:16,786 --> 00:17:19,456
How can the interest
be 10 times more?

89
00:17:31,051 --> 00:17:32,552
Don't look, Mom!

90
00:17:35,180 --> 00:17:37,015
Go in! Don't look!

91
00:17:50,320 --> 00:17:52,822
No, Tae-seung...

92
00:18:26,064 --> 00:18:27,065
Weight?

93
00:18:28,483 --> 00:18:30,110
65kil0s.

94
00:18:30,610 --> 00:18:31,236
Grab it.

95
00:18:32,779 --> 00:18:34,114
I said, grab it!

96
00:18:40,453 --> 00:18:41,746
If I become a cripple...

97
00:18:42,330 --> 00:18:45,667
there's no one to take care of
my poor mom.

98
00:18:46,960 --> 00:18:48,837
Don't you have a mom?

99
00:18:55,093 --> 00:18:56,261
It's not high enough.

100
00:18:57,095 --> 00:19:00,515
I can't be a cripple
and burden Mom!

101
00:19:04,978 --> 00:19:08,898
If you die, the insurance gets
messed up. Just jump.

102
00:19:09,315 --> 00:19:10,692
Evil bastard.

103
00:19:11,568 --> 00:19:12,735
Who's that woman?

104
00:19:14,404 --> 00:19:16,655
Please! Give me more time!

105
00:19:16,656 --> 00:19:19,033
Just one more week!

106
00:19:20,368 --> 00:19:21,035
Please!

107
00:19:41,472 --> 00:19:43,600
Get lost before
I throw you over!

108
00:19:45,602 --> 00:19:48,938
It's all my fault.
Cuz I abandoned you...

109
00:19:53,276 --> 00:19:55,820
Cut the shit! You bitch!

110
00:19:56,988 --> 00:19:58,281
Or I'll kill you!

111
00:19:59,824 --> 00:20:03,328
I don't mind
dying by your hands.

112
00:20:05,997 --> 00:20:08,708
Crazy bitch.

113
00:20:14,964 --> 00:20:17,383
My leg!

114
00:20:18,051 --> 00:20:20,136
Shit!

115
00:20:36,486 --> 00:20:39,154
Oh, my leg!

116
00:20:39,155 --> 00:20:41,824
One joint completely gone.

117
00:20:43,785 --> 00:20:45,870
The claim will be just
what you owe us.

118
00:20:48,706 --> 00:20:49,874
Son of a bitch!

119
00:20:50,708 --> 00:20:53,044
You'll burn to death!

120
00:20:53,127 --> 00:20:58,216
You'll scream in agony
and burn in hell forever!

121
00:21:02,262 --> 00:21:04,222
How dare you say that
to my son!

122
00:21:22,448 --> 00:21:25,910
Stop following me! Go away!

123
00:21:27,412 --> 00:21:28,913
What will you eat today?

124
00:21:29,414 --> 00:21:32,292
Pork? Eel? Duck?

125
00:21:32,667 --> 00:21:33,876
I'll buy.

126
00:21:34,627 --> 00:21:35,962
I'll do it myself!

127
00:22:26,596 --> 00:22:28,723
<i>JANG /VII-SUN 010-4760-4524.</i>

128
00:22:31,601 --> 00:22:34,145
<i>JANG /VII-SUN 010-4760-4524.</i>

129
00:23:05,885 --> 00:23:09,806
<i>DEAR MOM.</i>

130
00:23:18,022 --> 00:23:20,024
Hi, Jong-bok.

131
00:23:20,900 --> 00:23:22,193
Long time no see.

132
00:23:22,985 --> 00:23:25,113
Why'd you call?

133
00:23:26,614 --> 00:23:29,158
Me? I'm not busy.

134
00:23:29,784 --> 00:23:34,539
It'll be torn down here soon.
So, there's no business.

135
00:23:36,374 --> 00:23:38,000
All I got left are debts.

136
00:23:38,376 --> 00:23:41,587
Drinks? Tomorrow?

137
00:23:42,422 --> 00:23:45,550
Sure. Sounds good. Where?

138
00:23:47,301 --> 00:23:51,639
Okay, then. See you tomorrow.

139
00:23:52,306 --> 00:23:52,932
Bye.

140
00:24:49,947 --> 00:24:50,907
Mister.

141
00:24:52,200 --> 00:24:53,117
Mister!

142
00:25:11,302 --> 00:25:13,137
<i>JUGYO-DONG 151 YOON MI-HYUN.</i>

143
00:25:32,323 --> 00:25:33,491
Bastard.

144
00:25:34,825 --> 00:25:36,160
Think that's the end?

145
00:25:37,787 --> 00:25:38,746
Dying?

146
00:25:41,666 --> 00:25:43,292
Irresponsible fool.

147
00:26:21,706 --> 00:26:23,374
Isn't that Kang-chul?

148
00:26:24,750 --> 00:26:26,544
It's Kang-chul, right?

149
00:26:31,966 --> 00:26:33,384
Are you his friend?

150
00:26:34,260 --> 00:26:38,138
Got any bank accounts,
rings, or necklaces?

151
00:26:38,139 --> 00:26:39,557
No.

152
00:27:16,510 --> 00:27:19,180
<i>JA NG /VII-SUN.</i>

153
00:27:33,444 --> 00:27:35,863
You're really my mom
who abandoned me?

154
00:27:37,448 --> 00:27:38,324
Really?

155
00:27:40,660 --> 00:27:42,328
Want me to call you mom?

156
00:27:44,288 --> 00:27:45,289
Answer me.

157
00:27:49,627 --> 00:27:50,795
You bitch!

158
00:27:52,963 --> 00:27:57,968
Bullshit! I lived 30 years
without a mom!

159
00:28:00,221 --> 00:28:01,931
If I ever see you again...

160
00:28:03,099 --> 00:28:05,267
I'll rip you to shreds!

161
00:28:06,560 --> 00:28:08,479
Shaking me up...

162
00:28:09,647 --> 00:28:10,356
What?

163
00:28:11,732 --> 00:28:12,525
What!

164
00:28:13,901 --> 00:28:15,236
Hello?

165
00:28:16,404 --> 00:28:17,238
Shit!

166
00:28:35,840 --> 00:28:40,803
You're the one who left me
as I was born?

167
00:28:44,140 --> 00:28:50,604
<i>When mommy goes to the island</i>

168
00:28:51,689 --> 00:28:56,777
<i>to pick oysters</i>

169
00:28:58,362 --> 00:29:04,285
<i>the baby is left alone</i>

170
00:29:05,286 --> 00:29:10,916
<i>to Watch the house.</i>

171
00:29:12,710 --> 00:29:19,175
<i>Ebb and flow of tidal waves</i>

172
00:29:22,887 --> 00:29:28,851
<i>sounds like a lullaby.</i>

173
00:29:34,607 --> 00:29:39,403
<i>With his elbow as a pillow</i>

174
00:29:40,946 --> 00:29:46,911
<i>the baby falls asleep.</i>

175
00:30:28,160 --> 00:30:30,412
Prove to me you're my mom.

176
00:30:34,124 --> 00:30:35,334
I'm sorry.

177
00:30:36,919 --> 00:30:39,630
Give me proof!

178
00:30:45,344 --> 00:30:48,305
I have a big birth mark.
Where is it?

179
00:30:49,181 --> 00:30:51,975
If you're my mom, you'd know!

180
00:30:51,976 --> 00:30:54,103
I was too young.

181
00:30:54,645 --> 00:30:57,481
I got scared and ran
as soon as I had you.

182
00:30:59,400 --> 00:31:00,651
You lie!

183
00:31:01,235 --> 00:31:02,403
Who are you?

184
00:31:03,028 --> 00:31:05,030
Why show up all of a sudden?

185
00:31:06,782 --> 00:31:07,866
Kang-do.

186
00:31:07,867 --> 00:31:10,661
What do you want from me?

187
00:31:51,827 --> 00:31:53,746
If you're my mom, eat this.

188
00:32:57,476 --> 00:32:59,770
MY Door Kang-do.

189
00:33:43,147 --> 00:33:45,107
I came out of here?

190
00:33:48,068 --> 00:33:50,154
Here for sure?

191
00:33:52,865 --> 00:33:53,824
Really?

192
00:33:57,661 --> 00:33:59,371
Then, can I go back in?

193
00:34:02,458 --> 00:34:04,334
I'm going back in!

194
00:34:04,877 --> 00:34:05,961
Kang-do.

195
00:34:17,056 --> 00:34:19,141
If you're not my mom,
I'll stop.

196
00:34:19,558 --> 00:34:22,227
You're not my mom, are you!

197
00:34:31,695 --> 00:34:35,074
Hold still! I'm going in!

198
00:39:16,313 --> 00:39:18,482
Wash up. I'm making breakfast.

199
00:39:40,796 --> 00:39:41,755
Try it.

200
00:41:12,596 --> 00:41:13,680
Hi, Honey.

201
00:41:14,973 --> 00:41:16,057
How's the baby?

202
00:41:18,101 --> 00:41:22,022
Then, I'll be a daddy
next month?

203
00:41:27,569 --> 00:41:29,779
Got the ultrasound?
Who does he look like?

204
00:41:31,948 --> 00:41:35,869
No, he can't resemble me
or he'll end up like me.

205
00:41:36,077 --> 00:41:37,120
Look closely.

206
00:41:48,048 --> 00:41:50,842
Okay, then. I love you, too.

207
00:41:52,594 --> 00:41:53,762
I have a customer.

208
00:41:54,262 --> 00:41:56,598
I'll call you later.

209
00:42:02,479 --> 00:42:05,023
Can I borrow more money?

210
00:42:07,484 --> 00:42:09,611
$30,000 more.

211
00:42:10,403 --> 00:42:11,905
I'll gladly be a cripple.

212
00:42:13,782 --> 00:42:14,950
Why'd you borrow it?

213
00:42:17,118 --> 00:42:18,411
Cuz we're having a baby.

214
00:42:20,121 --> 00:42:25,167
He'll be born next month.
I wanna give him everything.

215
00:42:25,168 --> 00:42:26,211
I feel bad.

216
00:42:27,504 --> 00:42:30,507
If he's born cuz of me,
I should take responsibility.

217
00:42:38,848 --> 00:42:41,017
I'd do anything for the baby.

218
00:42:41,935 --> 00:42:44,813
I made money off this.

219
00:42:46,523 --> 00:42:48,024
But, now I'll make
the sacrifice.

220
00:42:51,736 --> 00:42:53,029
I'm ready.

221
00:42:55,365 --> 00:42:56,199
I envy him.

222
00:43:01,121 --> 00:43:01,913
For what?

223
00:43:04,541 --> 00:43:06,042
How you think of him.

224
00:43:09,588 --> 00:43:11,756
Don't all parents
feel the same?

225
00:43:12,882 --> 00:43:14,467
Your parents would've done
the same.

226
00:43:19,264 --> 00:43:23,893
How much insurance money
can I get for one hand?

227
00:43:26,229 --> 00:43:29,231
$30,000. That'll cover
your debt to us.

228
00:43:29,232 --> 00:43:33,570
Then, $60,000 for both?

229
00:43:35,238 --> 00:43:37,072
Then, cut this one, too.

230
00:43:37,073 --> 00:43:39,326
Get $60,000 and give me
$30,000.

231
00:43:47,417 --> 00:43:50,920
I wanna play it one last time.

232
00:44:02,599 --> 00:44:08,021
If I continued with music,
I would've ended up worse.

233
00:44:39,636 --> 00:44:42,180
I don't need it. Take it.

234
00:45:42,365 --> 00:45:43,366
Aren't you doing it?

235
00:45:44,701 --> 00:45:45,869
What are you doing?

236
00:45:48,663 --> 00:45:52,083
I gotta be a cripple.

237
00:45:53,251 --> 00:45:56,462
I have to provide for my son.
So, hurry up!

238
00:45:56,463 --> 00:45:57,881
Make me a cripple!

239
00:46:05,346 --> 00:46:07,599
<i>INSURANCE CLAIM.</i>

240
00:46:17,609 --> 00:46:19,569
Play it for the baby.

241
00:47:49,075 --> 00:47:52,871
What'd you do before
you met me?

242
00:47:54,622 --> 00:47:55,874
Got any family?

243
00:47:57,375 --> 00:47:58,793
No husband or kids?

244
00:48:05,508 --> 00:48:07,343
Do I have any siblings?

245
00:48:16,436 --> 00:48:17,770
Why're you crying?

246
00:48:19,689 --> 00:48:20,440
I'm not.

247
00:48:28,615 --> 00:48:30,158
Today's the last job.

248
00:48:32,702 --> 00:48:35,872
Anything you need or wanna do?

249
00:48:38,124 --> 00:48:39,500
Anyone you want to kill?

250
00:49:30,927 --> 00:49:32,053
What do you want?

251
00:49:35,598 --> 00:49:36,432
This?

252
00:49:41,270 --> 00:49:43,106
I lasted 50 years here.

253
00:49:44,607 --> 00:49:46,192
But ended up with nothing.

254
00:49:48,319 --> 00:49:50,947
The owners get new buildings
coming in.

255
00:49:52,740 --> 00:49:54,617
What about tenants like us?

256
00:49:56,828 --> 00:49:57,954
What is money?

257
00:50:01,124 --> 00:50:04,877
I had no intention of paying
back from the start.

258
00:50:05,420 --> 00:50:07,964
I got it to spend like crazy,
then die.

259
00:50:10,633 --> 00:50:16,014
Ever looked down on
Cheonggyecheon from the sky?

260
00:50:30,194 --> 00:50:32,280
This place is going to be gone.

261
00:50:34,741 --> 00:50:38,870
I came when I was 16,
50 years ago.

262
00:51:03,811 --> 00:51:05,354
Soon, this place will be...

263
00:51:06,481 --> 00:51:07,857
filled with high-rises, too.

264
00:51:26,000 --> 00:51:28,044
Death complicates the claim!

265
00:51:29,378 --> 00:51:30,296
Death?

266
00:51:33,049 --> 00:51:34,425
What is death?

267
00:51:49,524 --> 00:51:51,984
Don't feel guilty cuz of me.

268
00:52:26,936 --> 00:52:29,313
<i>HAPPY PR1 VA TE LOANS.</i>

269
00:52:46,289 --> 00:52:50,835
<i>LEE SANG-GU $30, 000.</i>

270
00:53:21,449 --> 00:53:23,409
It made me feel sorry to you.

271
00:53:37,840 --> 00:53:40,176
What did you do
to the man today?

272
00:53:41,552 --> 00:53:43,804
Jumped off a building
by himself.

273
00:53:44,472 --> 00:53:45,765
He must be dead.

274
00:53:51,812 --> 00:53:53,022
What is money?

275
00:53:55,274 --> 00:53:55,983
Money?

276
00:54:00,279 --> 00:54:02,990
The beginning and the end
of all things.

277
00:54:05,493 --> 00:54:11,415
Love, honor, violence, fury...

278
00:54:11,874 --> 00:54:14,669
hatred, jealousy...

279
00:54:16,587 --> 00:54:17,797
revenge“.

280
00:54:19,757 --> 00:54:21,008
Death.

281
00:54:21,634 --> 00:54:22,677
Revenge?

282
00:54:26,389 --> 00:54:28,891
Yes, revenge.

283
00:54:36,065 --> 00:54:38,943
If anyone comes here,
don't open the door.

284
00:54:39,068 --> 00:54:40,194
Call me right away!

285
00:54:40,987 --> 00:54:41,904
I will.

286
00:54:44,532 --> 00:54:46,033
Do you wanna go out?

287
00:55:06,429 --> 00:55:07,471
Tasty?

288
00:55:21,694 --> 00:55:22,778
Try this on.

289
00:55:27,241 --> 00:55:28,284
So cute.

290
00:55:41,047 --> 00:55:42,256
Me! Give it to me!

291
00:55:46,802 --> 00:55:49,513
Say, one more please.
Ask for one more.

292
00:55:55,895 --> 00:55:57,354
Sir, please.

293
00:55:57,355 --> 00:56:00,316
Sir, please help me.

294
00:56:00,608 --> 00:56:03,611
What is he, a retard?

295
00:56:09,158 --> 00:56:10,076
Look.

296
00:56:12,411 --> 00:56:13,496
What's so funny?

297
00:56:15,956 --> 00:56:19,710
It's funny.
Grown adults acting like kids.

298
00:56:22,338 --> 00:56:25,841
A mom having fun with
her son she met in 30 years.

299
00:56:29,345 --> 00:56:30,679
- That's funny?
- Ma'am!

300
00:56:32,056 --> 00:56:35,184
Crazy bitch!

301
00:56:36,560 --> 00:56:37,353
What?

302
00:56:38,896 --> 00:56:40,564
Let's go!

303
00:56:43,442 --> 00:56:44,902
I'm sorry. Apologize!

304
00:56:45,986 --> 00:56:46,946
I'm sorry.

305
00:57:13,556 --> 00:57:15,891
Get back!

306
00:57:19,395 --> 00:57:20,729
Devilish bastard!

307
00:57:21,313 --> 00:57:23,399
I never once forgot you!

308
00:57:24,483 --> 00:57:25,401
See?

309
00:57:25,860 --> 00:57:30,739
I can't do a thing as a cripple!
I'm a bum!

310
00:57:31,157 --> 00:57:32,908
Now, it's your turn to die!

311
00:57:33,075 --> 00:57:34,243
Get down!

312
00:57:35,077 --> 00:57:36,203
Kneel!

313
00:57:37,037 --> 00:57:38,622
Mom didn't do anything!

314
00:57:40,082 --> 00:57:41,917
Mom, my ass!

315
00:57:42,460 --> 00:57:46,338
You insulted me in front of
my mom for just $3,000!

316
00:57:47,631 --> 00:57:48,716
Watch closely.

317
00:57:52,636 --> 00:57:54,513
I told you you'd burn to death!

318
00:57:54,763 --> 00:57:57,600
Pour it on! Now!

319
00:57:58,184 --> 00:58:00,810
Forgive him. It's my fault.

320
00:58:00,811 --> 00:58:03,521
I abandoned him.
He grew up without love.

321
00:58:03,522 --> 00:58:05,107
Bullshit!

322
00:58:05,524 --> 00:58:07,610
The bastard was born evil.

323
00:58:07,860 --> 00:58:11,739
He's the devil who tests
people with money!

324
00:58:12,615 --> 00:58:14,825
Pour it on! Now!

325
00:58:17,578 --> 00:58:18,454
Son of a bitch.

326
00:58:22,166 --> 00:58:23,250
Kang-do! No!

327
00:58:24,084 --> 00:58:25,002
- No!
- Don't move!

328
00:58:29,340 --> 00:58:30,674
I'll light it for ya!

329
00:58:48,943 --> 00:58:49,777
Kang-do!

330
00:58:50,778 --> 00:58:51,654
St0l3-

331
00:58:52,696 --> 00:58:53,531
St0l3-

332
00:58:54,990 --> 00:58:55,950
St0l3-

333
00:59:23,936 --> 00:59:25,521
He probably didn't die.

334
00:59:32,236 --> 00:59:34,446
Or report it to the cops.

335
01:02:43,886 --> 01:02:44,803
I'm scared.

336
01:02:49,933 --> 01:02:51,727
That you'll suddenly vanish.

337
01:02:55,355 --> 01:02:58,442
I can't survive being
alone again.

338
01:03:02,362 --> 01:03:03,739
Today's your birthday.

339
01:03:07,576 --> 01:03:09,995
Go buy a cake. Let's celebrate.

340
01:03:12,456 --> 01:03:15,626
So, this was my birthday
present?

341
01:03:16,960 --> 01:03:20,130
It looks a little small for me.

342
01:03:20,923 --> 01:03:21,715
Why?

343
01:03:23,342 --> 01:03:25,219
It's not done yet?

344
01:06:51,800 --> 01:06:53,218
Boss, it's Lee Kang-do.

345
01:06:57,264 --> 01:06:58,807
Why you!

346
01:06:59,558 --> 01:07:00,892
You son of a bitch!

347
01:07:05,230 --> 01:07:06,398
I'm sorry, boss!

348
01:07:14,448 --> 01:07:16,074
I'm sorry, boss!

349
01:07:16,199 --> 01:07:18,035
You ungrateful bastard.

350
01:07:19,119 --> 01:07:19,828
Get lost!

351
01:07:20,537 --> 01:07:21,997
Someone else is doing it.

352
01:07:23,206 --> 01:07:24,082
Boss!

353
01:07:24,708 --> 01:07:26,835
Please give my mom back!

354
01:07:27,502 --> 01:07:28,837
You have a mother?

355
01:07:32,507 --> 01:07:36,178
That's why you quit?
Scared of revenge?

356
01:07:39,347 --> 01:07:40,390
It's not me.

357
01:07:41,016 --> 01:07:43,351
It's probably one of
the cripples you made.

358
01:07:50,567 --> 01:07:53,862
I told you to collect money,
not make 'em cripples!

359
01:07:54,362 --> 01:07:56,782
You butcher!

360
01:08:23,308 --> 01:08:26,686
Where were you?
Know how much I worried!

361
01:08:27,020 --> 01:08:28,814
Why didn't you take your phone!

362
01:08:36,905 --> 01:08:38,031
I'm sorry.

363
01:09:30,167 --> 01:09:34,796
<i>Happy birthday to you.</i>

364
01:09:35,630 --> 01:09:40,260
<i>Happy birthday to you</i>

365
01:09:53,940 --> 01:09:55,233
I have a favor.

366
01:09:57,277 --> 01:09:58,862
Plant me a tree.

367
01:10:08,997 --> 01:10:13,043
Where's the sweater?
I wanna wear it today.

368
01:10:38,777 --> 01:10:41,738
Can a pine tree live
by the water?

369
01:10:42,781 --> 01:10:43,698
It'll live.

370
01:10:46,034 --> 01:10:46,952
Thanks.

371
01:10:52,540 --> 01:10:56,544
When I die, bury me here.

372
01:10:56,795 --> 01:10:57,545
What?

373
01:10:58,546 --> 01:11:00,757
Don't say stuff like that!

374
01:11:03,468 --> 01:11:05,011
Everyone dies.

375
01:11:07,180 --> 01:11:11,309
But still...
It's not for a long time.

376
01:11:57,772 --> 01:11:59,065
Go to your bed!

377
01:12:01,318 --> 01:12:02,235
Get out!

378
01:12:18,626 --> 01:12:20,462
Did I do something wrong?

379
01:12:30,555 --> 01:12:34,768
Go water the tree.
It'll wither to death.

380
01:12:35,560 --> 01:12:36,561
Okay.

381
01:12:47,322 --> 01:12:48,198
Hello?

382
01:12:50,784 --> 01:12:53,119
Kang-do! Someone's here!

383
01:12:53,411 --> 01:12:54,662
- Mom?
<i>- Why're you doing this!</i>

384
01:12:55,497 --> 01:12:56,247
Mom!

385
01:12:58,124 --> 01:12:59,751
Stop it!

386
01:12:59,834 --> 01:13:03,797
No! Please! No!

387
01:13:06,257 --> 01:13:09,009
Help me! Help me!

388
01:13:09,010 --> 01:13:11,178
Who the fuck are you!

389
01:13:11,179 --> 01:13:13,305
Why are you doing this!

390
01:13:13,306 --> 01:13:13,848
Mom?

391
01:13:15,350 --> 01:13:21,856
It's my fault!
Not my son's! Please!

392
01:13:26,694 --> 01:13:27,529
Mom!

393
01:13:52,345 --> 01:13:53,930
Mom!

394
01:14:35,054 --> 01:14:36,681
Yes, Han's Machinery.

395
01:14:37,348 --> 01:14:39,350
Yes, of course we can do it.

396
01:14:40,310 --> 01:14:44,480
20¢? That's impossible.

397
01:14:44,481 --> 01:14:47,108
The price of copper went up.

398
01:14:47,442 --> 01:14:51,571
Then, I'll do it for 30¢.
That's it.

399
01:14:52,197 --> 01:14:54,699
Hello? Hello?

400
01:14:55,658 --> 01:14:57,952
Son of a bitch!

401
01:14:58,620 --> 01:15:00,788
Take the skin off my back
why don't ya!

402
01:15:00,914 --> 01:15:02,707
Damn bastard!

403
01:15:04,459 --> 01:15:07,420
Damn sons of bitches!

404
01:15:09,797 --> 01:15:12,591
Greedy bastards!

405
01:15:12,592 --> 01:15:13,968
Sons of bitches!

406
01:15:14,302 --> 01:15:16,930
Damn bastards!

407
01:15:40,328 --> 01:15:42,914
I'll kill you! Bastard!

408
01:15:43,665 --> 01:15:46,459
I'll rip you to shreds!
You bastard!

409
01:15:50,838 --> 01:15:52,048
Looking for Jong-do?

410
01:15:52,298 --> 01:15:54,133
He quit and went to Yang-pyung.

411
01:16:02,809 --> 01:16:03,893
Where's your son?

412
01:16:04,769 --> 01:16:06,604
Where's Lee Jong-do!

413
01:16:18,283 --> 01:16:19,325
Jong-do.

414
01:16:20,702 --> 01:16:22,120
Your friend's here.

415
01:16:24,205 --> 01:16:29,877
How nice there's a friend
who still remembers you.

416
01:16:30,378 --> 01:16:32,422
You must be so happy.

417
01:16:32,797 --> 01:16:36,509
Don't you ever die
cuz of money.

418
01:16:37,135 --> 01:16:39,053
Damn money!

419
01:16:46,811 --> 01:16:51,899
How can you leave me behind
over losing an arm?

420
01:16:52,025 --> 01:16:54,736
After everything I did
to raise you.

421
01:16:58,072 --> 01:16:59,824
Damn bastard!

422
01:17:00,408 --> 01:17:02,702
That bastard...

423
01:17:19,636 --> 01:17:22,096
Where's your dad! Tell me!

424
01:17:37,862 --> 01:17:38,946
What'd you do yesterday?

425
01:17:40,615 --> 01:17:41,824
Thought of you, fool.

426
01:17:42,492 --> 01:17:43,409
50?

427
01:17:44,786 --> 01:17:49,415
Never once forgot you
doing this to me for $10,000.

428
01:17:50,416 --> 01:17:52,168
So, what did you do!

429
01:17:52,794 --> 01:17:54,253
Getting revenge!

430
01:17:56,297 --> 01:17:57,464
Tell the truth!

431
01:17:57,465 --> 01:18:00,509
Stabbing you! Burning you!

432
01:18:00,510 --> 01:18:03,012
Sinking you!
Ripping you to shreds!

433
01:18:04,222 --> 01:18:05,640
That's not enough!

434
01:18:06,140 --> 01:18:09,644
If you had parents,
I'd take 'em as you watch and...

435
01:18:12,313 --> 01:18:14,732
But it's no use.

436
01:18:16,025 --> 01:18:17,193
A cripple like me.

437
01:18:17,276 --> 01:18:19,320
All I can do is hope
you rot in hell.

438
01:18:19,654 --> 01:18:20,488
Just you wait.

439
01:18:21,114 --> 01:18:25,201
I remind my kid what you did
every single day!

440
01:18:46,305 --> 01:18:52,854
<i>When mommy goes to the island</i>

441
01:18:53,396 --> 01:18:59,193
<i>to pick oysters</i>

442
01:19:00,528 --> 01:19:06,701
<i>the baby is left alone</i>

443
01:19:08,411 --> 01:19:14,292
<i>to Watch the house.</i>

444
01:19:15,168 --> 01:19:21,591
<i>Ebb and flow of tidal waves</i>

445
01:19:22,300 --> 01:19:28,222
<i>sounds like a lullaby.</i>

446
01:19:30,725 --> 01:19:37,064
<i>With his elbow as a pillow</i>

447
01:19:38,691 --> 01:19:45,114
<i>the baby falls asleep.</i>

448
01:20:26,948 --> 01:20:28,115
Thank you.

449
01:20:33,329 --> 01:20:35,039
I led a foolish life.

450
01:20:35,706 --> 01:20:38,125
So, I cannot see
as much as others.

451
01:20:40,086 --> 01:20:41,629
That is enough.

452
01:21:07,238 --> 01:21:11,409
It's $3 for a bag.
What would you like?

453
01:21:46,068 --> 01:21:47,862
Rice puffs again?

454
01:22:04,545 --> 01:22:06,172
You bitch!

455
01:22:06,339 --> 01:22:09,091
What bastard did you
go and fuck!

456
01:22:09,634 --> 01:22:11,677
Shut up
if you don't wanna starve.

457
01:22:12,053 --> 01:22:14,180
We're living like this
cuz you borrowed that money.

458
01:22:15,932 --> 01:22:19,185
What? I didn't do it
just for me.

459
01:22:20,978 --> 01:22:25,274
Bitch! It's all your fault!

460
01:22:26,567 --> 01:22:28,027
Get lost, you bitch!

461
01:22:32,156 --> 01:22:35,451
If you doubt me so much,
you go and make money!

462
01:22:36,744 --> 01:22:38,329
You bitch!

463
01:22:45,920 --> 01:22:46,754
Shit!

464
01:23:07,900 --> 01:23:09,026
Where's my mom?

465
01:23:09,151 --> 01:23:12,321
The hell! You had a mom?

466
01:23:14,115 --> 01:23:15,324
Then, why the act!

467
01:23:16,450 --> 01:23:18,244
Cuz I'm scared.

468
01:23:21,497 --> 01:23:25,167
I can't even move! See!

469
01:23:30,297 --> 01:23:31,257
Wait!

470
01:23:34,260 --> 01:23:35,261
Beer money?

471
01:23:45,271 --> 01:23:46,188
Don't need it!

472
01:23:46,439 --> 01:23:47,523
The devil's money!

473
01:23:53,821 --> 01:23:58,993
If there was no law,
I'd kill you a hundred times!

474
01:24:03,998 --> 01:24:05,332
You piece of shit!

475
01:24:06,459 --> 01:24:08,753
Wish I can grind you
with my car!

476
01:24:33,486 --> 01:24:35,529
1 got $40! Shit.

477
01:24:36,322 --> 01:24:37,073
Retard!

478
01:26:26,307 --> 01:26:30,477
<i>$5,000 BECAME $50,000.
I'LL BECOME A CRIPPLE TODAY.</i>

479
01:26:31,729 --> 01:26:35,649
<i>WHAT IS MONEY? LIFE? DEATH?
I'M SORRY MOM.</i>

480
01:27:53,852 --> 01:27:55,062
Why'd you do it?

481
01:27:56,563 --> 01:27:58,607
Why were you so cruel?

482
01:28:04,113 --> 01:28:06,282
I cannot forgive you.

483
01:28:09,201 --> 01:28:12,037
You're a devil who tests
people with money.

484
01:28:16,250 --> 01:28:18,752
Now, you feel the heartache
as them.

485
01:28:20,337 --> 01:28:23,966
Watch your family die
before your very eyes!

486
01:28:30,931 --> 01:28:32,975
No use crying now.

487
01:28:34,768 --> 01:28:36,395
Pathetic fool.

488
01:28:37,938 --> 01:28:39,565
Devilish bastard!

489
01:29:13,682 --> 01:29:14,391
What is it?

490
01:29:33,243 --> 01:29:34,495
The hell!

491
01:29:35,954 --> 01:29:36,997
The fuck!

492
01:29:38,415 --> 01:29:39,166
Mom?

493
01:29:39,917 --> 01:29:40,667
Mom!

494
01:29:40,834 --> 01:29:42,086
You bitch!

495
01:29:42,795 --> 01:29:43,629
Mom!

496
01:29:49,051 --> 01:29:49,927
Mom!

497
01:29:54,014 --> 01:29:55,140
Get lost, bitch!

498
01:30:05,526 --> 01:30:06,360
Mom!

499
01:31:39,036 --> 01:31:40,370
Sang-go?

500
01:31:42,289 --> 01:31:44,082
You waited long, right?

501
01:31:46,793 --> 01:31:49,421
The fool's soul will die now.

502
01:31:51,590 --> 01:31:53,634
When I die before his eyes...

503
01:31:55,844 --> 01:31:59,306
he'll be in dire pain
over losing his family.

504
01:32:00,557 --> 01:32:02,643
He'll go crazy.

505
01:32:07,981 --> 01:32:09,149
But...

506
01:32:14,863 --> 01:32:17,824
Why am I so sad?

507
01:32:24,373 --> 01:32:26,500
Sang-go! I'm sorry.

508
01:32:30,837 --> 01:32:33,298
I didn't intend to feel this.

509
01:32:39,429 --> 01:32:41,223
But I feel sorry for him.

510
01:32:45,185 --> 01:32:47,187
Poor Kang-do!

511
01:33:05,080 --> 01:33:07,499
Mom! Where are you?

512
01:33:08,083 --> 01:33:08,917
Mom!

513
01:33:10,294 --> 01:33:11,169
Mom!

514
01:33:11,503 --> 01:33:12,504
Kang-do!

515
01:33:13,880 --> 01:33:16,133
Don't come up!

516
01:33:16,592 --> 01:33:17,842
Please don't kill me!

517
01:33:17,843 --> 01:33:20,512
No! You son of a bitch!

518
01:33:21,138 --> 01:33:24,308
Let Mom go! What do you want!

519
01:33:24,891 --> 01:33:26,226
Mom didn't do anything!

520
01:33:27,060 --> 01:33:28,228
Kill me instead!

521
01:33:28,353 --> 01:33:30,897
Kang-do! I'm scared!

522
01:33:31,982 --> 01:33:35,235
Forgive me! Please!
Don't kill me!

523
01:33:35,861 --> 01:33:37,279
Kang-do! I'm scared!

524
01:33:37,571 --> 01:33:39,865
No! Mom! No!

525
01:33:42,034 --> 01:33:43,243
I was wrong!

526
01:33:43,785 --> 01:33:46,246
I'm sorry! I'm sorry!

527
01:33:46,872 --> 01:33:48,373
I'll die instead!

528
01:33:48,832 --> 01:33:51,209
Let Mom go! Please!

529
01:33:52,044 --> 01:33:55,756
I'll die! Mom is innocent!

530
01:33:56,006 --> 01:33:58,090
Kill me! I'm sorry!

531
01:33:58,091 --> 01:33:59,593
Kill me, please!

532
01:33:59,968 --> 01:34:02,095
Let Mom go! Please!

533
01:34:02,763 --> 01:34:07,142
I'm so sorry!
Please don't kill Mom!

534
01:34:08,685 --> 01:34:11,980
Please forgive me.
She didn't do anything.

535
01:34:12,397 --> 01:34:13,774
It's my fault.

536
01:34:14,441 --> 01:34:18,820
Forgive me.
And please let my...

537
01:34:19,655 --> 01:34:22,616
Please let my mom go. Kill me.

538
01:34:22,949 --> 01:34:24,117
Please let her go.

539
01:34:27,913 --> 01:34:30,707
Please don't harm her.
I'm sorry.

540
01:34:31,124 --> 01:34:33,543
I am to blame.

541
01:34:34,378 --> 01:34:36,213
Please don't kill her.

542
01:34:48,308 --> 01:34:49,309
Sang-go?

543
01:34:51,395 --> 01:34:53,355
You're not alone any more.

544
01:35:23,343 --> 01:35:24,511
Mom!

