﻿1
00:00:19,800 --> 00:00:24,000
-Landlady. <br>-Yes? <br>- I'm thinking of making a deal. We should discuss it upstairs.

2
00:00:24,000 --> 00:00:26,800
Make some tea and bring some flower cookies for our guest.

3
00:00:26,800 --> 00:00:28,200
Please.

4
00:00:33,400 --> 00:00:34,500
After you.

5
00:00:47,400 --> 00:00:58,900
<i>Timing and Subtitles by the Gone With the Shirt Team @ Viki<br>Please do not reuse or reupload our subtitles.</i>

6
00:01:13,800 --> 00:01:17,700
<i>When a Snail Falls in Love</i><br>-Episode 20-

7
00:01:19,140 --> 00:01:20,370
Jin Dun,

8
00:01:21,400 --> 00:01:24,400
you felt really weird, right?

9
00:01:26,400 --> 00:01:30,300
The smuggling and drug dealing you organized.

10
00:01:32,190 --> 00:01:34,630
How did they get discovered by us?

11
00:01:41,750 --> 00:01:46,500
Do you remember the Ye Lanqing kidnapping case from 13 years ago?

12
00:01:47,200 --> 00:01:51,400
You colluded with Hu Zhishan and killed Ye Lanqing with dynamite.

13
00:01:51,400 --> 00:01:55,500
A policeman from the rescue operation went missing.

14
00:02:09,150 --> 00:02:11,020
That policeman...

15
00:02:13,580 --> 00:02:17,630
He knew it was a trap but he still came alone.

16
00:02:23,120 --> 00:02:25,120
Don't Chinese police like you

17
00:02:26,340 --> 00:02:29,070
value your own lives?

18
00:02:36,800 --> 00:02:38,800
We do value our lives.

19
00:02:40,720 --> 00:02:44,010
But we value our responsibility more.

20
00:02:47,360 --> 00:02:53,010
Some things are more valuable than being saved and living like a dog.

21
00:02:54,970 --> 00:02:56,840
Can you understand?

22
00:03:14,200 --> 00:03:15,500
Big Hu!

23
00:03:41,600 --> 00:03:45,800
<i>Someone like Zhao Xiaolu, only someone with a very close relationship can earn her trust like that.</i>

24
00:03:45,800 --> 00:03:49,200
<i>Therefore, I think the biggest possible answer is a couple relationship.</i>

25
00:03:49,200 --> 00:03:51,200
<i>Plus this person is very arrogant.</i>

26
00:04:07,400 --> 00:04:09,400
-Hello? <br>- Director Zhan!

27
00:04:09,400 --> 00:04:13,000
This is Xu Xu. I have some very urgent information I need to report to you.

28
00:04:13,800 --> 00:04:18,200
We can't trust General Po, he might be the Golden Python.

29
00:04:18,200 --> 00:04:21,200
<i>The safety of the suspects and witnesses on this train can't be guaranteed.</i>

30
00:04:21,200 --> 00:04:23,800
I don't have enough time to explain right now.

31
00:04:23,800 --> 00:04:25,400
Okay, I understand.

32
00:04:25,400 --> 00:04:29,800
Don't make a move yet. Try to stabilize him. The border police will be there very soon.

33
00:04:29,800 --> 00:04:32,600
- I can't hear you, Director Zhan. <br>- Persist for another half-hour and then the border police will arrive.

34
00:04:33,700 --> 00:04:34,800
Hello?

35
00:04:36,000 --> 00:04:37,200
Hello?

36
00:04:51,940 --> 00:04:55,560
How long ago did Po's train depart?

37
00:04:56,960 --> 00:04:59,230
Where are you?

38
00:06:09,600 --> 00:06:12,100
Miss, the general wants to meet you.

39
00:06:18,800 --> 00:06:21,200
I need to talk to my colleagues.

40
00:06:21,200 --> 00:06:24,400
I am sorry, the general wants to meet you now.

41
00:06:24,400 --> 00:06:25,600
Please.

42
00:06:32,900 --> 00:06:36,200
Stop, general's order. This is a secure area.

43
00:06:36,200 --> 00:06:38,000
You guys are not allowed to enter.

44
00:06:38,000 --> 00:06:41,600
- Are you blocking us? <br>- Yes. Please.

45
00:06:43,900 --> 00:06:45,500
Where are the two, my colleagues?

46
00:06:45,500 --> 00:06:47,200
What is going on?

47
00:06:47,200 --> 00:06:49,800
We are just taking her back to her room.

48
00:07:12,200 --> 00:07:13,800
Xu Xu?

49
00:07:13,800 --> 00:07:16,300
Secure area. It's the general's order.

50
00:07:20,400 --> 00:07:25,500
Not long after Ye Zixi entered Ye Corporation,

51
00:07:25,500 --> 00:07:29,800
I found out she was flirting with Zhang Shiyong.

52
00:07:29,800 --> 00:07:34,400
I thought I could do something since they were together.

53
00:07:34,400 --> 00:07:37,300
The fact that Ye Zixi wanted the Ye Corporation's stocks

54
00:07:37,300 --> 00:07:41,300
was obvious like it was written on her face. But Zhang Shiyong was different.

55
00:07:41,300 --> 00:07:46,200
I think his ambition and greed hid his intelligence.

56
00:07:46,200 --> 00:07:50,400
He was constantly trying to dig up more information about that 1.9 billion account.

57
00:07:50,400 --> 00:07:52,800
He also sent people to spy on me.

58
00:07:54,440 --> 00:07:56,550
I needed to prepare a backup plan, right?

59
00:07:58,200 --> 00:08:03,210
<i>So I sent someone to gather information and found this man called Yang Yu.</i>

60
00:08:03,210 --> 00:08:04,600
<i>He was looking to create trouble for Zhang Shiyong.</i>

61
00:08:04,600 --> 00:08:09,300
<i>So you ordered Jin Maitian's people to contact Yang Yu and create trouble for Zhang Shiyong.</i>

62
00:08:10,300 --> 00:08:13,800
You were disappointed when Yang Yu didn't get him, right?

63
00:08:13,800 --> 00:08:17,200
That's right. I wasn't surprised by the outcome though.

64
00:08:17,200 --> 00:08:20,400
That time, I just wanted to teach Zhang Shiyong a lesson.

65
00:08:20,400 --> 00:08:25,500
But Zhang Shiyong used the cops to threaten me.

66
00:08:56,800 --> 00:08:58,500
<i>You can come in now.</i>

67
00:09:13,800 --> 00:09:15,300
Sit.

68
00:09:31,100 --> 00:09:34,900
Inspecter Xu, please sit.

69
00:09:52,000 --> 00:09:55,900
General Po, do you need anything from me?

70
00:09:58,200 --> 00:10:02,600
I heard that you are good at psychological analysis.

71
00:10:02,600 --> 00:10:05,500
You can analyze the identity of the other party.

72
00:10:07,010 --> 00:10:10,340
Who told you that I'm good at psychological analysis?

73
00:10:12,000 --> 00:10:16,400
Was it Tisa... or our commander?

74
00:10:16,400 --> 00:10:18,700
How were they talking about me behind my back?

75
00:10:19,500 --> 00:10:21,200
You don't need to know that.

76
00:10:21,200 --> 00:10:26,400
All I'm interested in is your analysis of me.

77
00:10:26,400 --> 00:10:32,500
I'm sorry. My expertise is used for solving cases, not for fun.

78
00:10:34,620 --> 00:10:38,030
Chinese people always love to brag.

79
00:10:39,710 --> 00:10:42,310
Looks like you are also no different.

80
00:10:43,780 --> 00:10:45,980
You don't need to goad me.

81
00:10:46,920 --> 00:10:52,900
It's not hard to analyse you.

82
00:10:57,200 --> 00:10:58,500
Fine.

83
00:11:00,800 --> 00:11:03,000
If your analysis is right,

84
00:11:03,800 --> 00:11:08,400
I'll give you this handgun as a gift.

85
00:11:08,400 --> 00:11:12,400
From now on, we are friends.

86
00:11:15,100 --> 00:11:17,600
Guns are regulated in China.

87
00:11:18,500 --> 00:11:20,700
I have no use for it even if you give me one.

88
00:11:24,800 --> 00:11:26,500
How about this?

89
00:11:26,500 --> 00:11:32,000
If my analysis is right, give this one to me. Is that okay?

90
00:11:33,800 --> 00:11:35,000
This?

91
00:11:39,890 --> 00:11:41,840
This is not valuable.

92
00:11:43,520 --> 00:11:47,540
In China, we believe in affinity.

93
00:11:47,540 --> 00:11:52,380
It means that just by looking at an object, you feel that you two are fated.

94
00:11:52,400 --> 00:11:56,400
For you, this item might just be an ordinary decoration.

95
00:11:56,400 --> 00:12:01,900
But I feel that it has a lot of vigor in its style. This must have been made by you personally.

96
00:12:19,700 --> 00:12:23,600
Ye Zixi. You used Ye Lanyuan's trust.

97
00:12:23,600 --> 00:12:28,400
After stealing the international financial report, you deliberately leaked it, framed Ye Zixi and then

98
00:12:28,400 --> 00:12:30,300
personally killed her.

99
00:12:32,300 --> 00:12:34,800
She is too intelligent.

100
00:12:34,800 --> 00:12:37,600
She was in my way. I must get my hands

101
00:12:37,600 --> 00:12:40,700
on that 1.9 million.

102
00:12:42,050 --> 00:12:44,240
There really was no other choice.

103
00:12:54,570 --> 00:12:55,830
Ji Bai,

104
00:12:58,300 --> 00:13:04,900
do you want to know what your childhood friend, your little sister Ye Zixi

105
00:13:04,900 --> 00:13:07,600
said before she died?

106
00:13:13,770 --> 00:13:16,500
<i>Silly girl, let me ask you a question.</i>

107
00:13:18,000 --> 00:13:21,400
<i>Who do you want to contact the most at this time?</i>

108
00:13:21,400 --> 00:13:25,400
<i>Yang Yu? Zhang Shiyong?</i>

109
00:13:26,420 --> 00:13:28,860
<i>Or Ji Bai?</i>

110
00:13:41,600 --> 00:13:45,000
She just stared at me and I returned her gaze.

111
00:13:45,000 --> 00:13:49,300
She didn't want to die. She wanted to find her Third Brother. She was very afraid.

112
00:14:00,800 --> 00:14:06,600
A stream of blood... from her chest that was as white and creamy as white cheese...

113
00:14:06,600 --> 00:14:13,700
Gudu... Gudu... it flowed out like that...<br><i>(T/N: 'gudu' = transliteration of blood spurting out)</i>

114
00:14:19,300 --> 00:14:23,000
Firstly, you're a firm and resolute person.

115
00:14:23,000 --> 00:14:29,000
You base your actions on your own principles, and not what others determine as right or wrong.

116
00:14:29,000 --> 00:14:33,000
That's why your troops are both fearful and respectful of you.

117
00:14:33,000 --> 00:14:37,900
In their eyes, you're both Heaven and Earth (their god).

118
00:14:49,000 --> 00:14:54,000
Secondly, you lust after power, and determined to boot.

119
00:14:54,000 --> 00:14:58,800
That's why at just over the age of 40, you're able to have such standing in the military.

120
00:14:58,800 --> 00:15:03,300
Thirdly, you have a slight tendency to be violent and sadistic.

121
00:15:03,300 --> 00:15:07,600
You can see this from the way you shot and killed the criminal the other day.

122
00:15:13,700 --> 00:15:17,000
But not all kinds of sadistic action can bring you pleasure,

123
00:15:17,000 --> 00:15:22,000
even, at some point, you'll resist the desire to be violent.

124
00:15:22,000 --> 00:15:24,500
You're now the general of an army.

125
00:15:24,500 --> 00:15:28,000
If you completely indulged yourself, you'd definitely have all sorts of channels

126
00:15:28,000 --> 00:15:31,700
and more violent methods to satisfy your sadistic desires.

127
00:15:32,700 --> 00:15:35,600
But in Myanmar, I didn't hear of any such criticisms.

128
00:15:35,600 --> 00:15:39,800
That's why I think that even though you're unable to get over your addiction for sadism,

129
00:15:39,800 --> 00:15:42,400
but you've always held yourself back.

130
00:15:44,180 --> 00:15:48,080
General Po, regarding this, I'd like to show my admiration for you.

131
00:15:51,790 --> 00:15:55,250
Fourthly, you don't have women around you.

132
00:15:55,990 --> 00:16:00,470
That's why I reckon that you have a steady partner.

133
00:16:14,400 --> 00:16:20,000
Fifthly, judging from your office decoration style,

134
00:16:20,000 --> 00:16:23,500
you're someone who likes extravagance and flamboyance.

135
00:16:24,600 --> 00:16:27,800
I guess that you must feel bitter towards the Commander.

136
00:16:30,400 --> 00:16:32,300
You're competing with him.

137
00:16:37,200 --> 00:16:39,100
<i>You need to be careful of this cop.</i>

138
00:16:40,600 --> 00:16:45,100
<i>She's very formidable. It's like she can see through everyone's disguise.</i>

139
00:16:59,120 --> 00:17:03,590
General Po, is my analysis accurate?

140
00:17:25,000 --> 00:17:27,600
You really are an interesting girl.

141
00:17:27,600 --> 00:17:29,700
This makes me like you a lot,

142
00:17:31,000 --> 00:17:32,990
and want to talk to you longer,

143
00:17:35,140 --> 00:17:40,470
but I have something to do now. I can't let you leave now.

144
00:17:49,400 --> 00:17:51,400
Take her away.

145
00:18:11,400 --> 00:18:14,700
How was it? You saw that Xu Xu?

146
00:18:23,770 --> 00:18:27,470
Let the headquarters know, my train is spoiled,

147
00:18:28,400 --> 00:18:31,600
and needs to do an emergency change of route and go to another station.

148
00:18:31,600 --> 00:18:33,200
<i>Received! </i>

149
00:18:35,120 --> 00:18:36,860
What's happened?

150
00:18:47,600 --> 00:18:49,470
The plan to send you to Laos,

151
00:18:51,100 --> 00:18:53,900
I'm afraid we won't be able to wait until the return journey to do that.

152
00:18:53,900 --> 00:18:56,000
We're going to do it now?

153
00:19:02,800 --> 00:19:04,400
<i>Stay here.</i>

154
00:19:18,000 --> 00:19:19,500
Ji Bai,

155
00:19:21,700 --> 00:19:24,500
right now, you think that you've won and you're feeling very pleased with yourself, aren't you?

156
00:19:26,000 --> 00:19:28,800
Let me tell you. The schemes that I've come up with,

157
00:19:28,800 --> 00:19:31,400
the people who I've killed, the money I've earned,

158
00:19:31,400 --> 00:19:35,000
also the heroin that I've sold,

159
00:19:35,000 --> 00:19:39,600
do you even know how much I've sold? You guys don't know.

160
00:19:39,600 --> 00:19:43,800
One day, you'll send me to the execution grounds.

161
00:19:45,500 --> 00:19:46,600
Pow!

162
00:19:48,400 --> 00:19:51,600
And everything will disappear into dust.

163
00:19:51,600 --> 00:19:57,200
All these will turn into mysteries. I'm willing to bet

164
00:19:57,200 --> 00:20:00,800
that you don't know about the other things I've done.

165
00:20:02,600 --> 00:20:04,500
You're not as smart as me.

166
00:20:06,000 --> 00:20:08,400
You're not smart either.

167
00:20:08,400 --> 00:20:12,000
You unscrupulously did so many things,

168
00:20:12,000 --> 00:20:15,200
only because in your heart, there wasn't any happiness or warmth worth protecting

169
00:20:15,200 --> 00:20:19,100
or cherishing in your heart.

170
00:20:26,400 --> 00:20:32,000
I've been in the police for ten years. If I were to list all the degenerates who I've caught, like you,

171
00:20:32,000 --> 00:20:35,300
it'd be much longer than your list of people you've killed or misdeeds you've done.

172
00:20:36,400 --> 00:20:39,300
You've now become a name on my list.

173
00:20:39,300 --> 00:20:44,400
Therefore, your list ends as of today,

174
00:20:44,400 --> 00:20:48,500
whereas my list will continue on.

175
00:20:53,000 --> 00:21:03,000
<i>Timing and Subtitles by the Gone With the Shirt Team @ Viki<br>Please do not reuse or reupload our subtitles.</i>

176
00:21:32,400 --> 00:21:36,200
<i>[Xu Xu has been taken away by Po.]</i>

177
00:22:00,400 --> 00:22:02,100
<i>Stop there! Don't run! </i>

178
00:22:11,000 --> 00:22:12,300
Boss,

179
00:22:13,200 --> 00:22:14,700
are you okay?

180
00:22:17,900 --> 00:22:21,200
It is General Po's army.Go find Da Hu and Hou Zi.

181
00:22:21,200 --> 00:22:24,400
We need to ensure the suspects' safety. Xu Xu has been taken away.

182
00:22:24,400 --> 00:22:26,300
I need to go find her.

183
00:23:10,170 --> 00:23:12,780
Stop. What are you doing here?

184
00:23:12,800 --> 00:23:14,900
What? Pardon me?

185
00:23:17,260 --> 00:23:18,920
Can you speak English?

186
00:23:21,400 --> 00:23:24,600
I am looking for my colleague, Xu Xu.

187
00:23:24,600 --> 00:23:28,000
The train is running, the army is in control. This is General Po's order.

188
00:23:28,000 --> 00:23:29,800
General Po?

189
00:23:29,800 --> 00:23:32,600
Yeah, General Po with me.

190
00:23:32,600 --> 00:23:34,700
We are friend, good friend.

191
00:23:35,800 --> 00:23:37,000
Stop, don't move.

192
00:23:37,000 --> 00:23:39,600
- You see, you know? <br>- I am going to shoot. I said stop.

193
00:23:39,600 --> 00:23:41,100
Teacher!

194
00:23:59,000 --> 00:24:00,900
Teacher, I am here!

195
00:24:01,900 --> 00:24:03,300
Back off!

196
00:24:06,200 --> 00:24:07,800
Don't move! Stop! Nobody moves

197
00:24:07,800 --> 00:24:10,500
without General Po's order. Nobody move.

198
00:24:21,600 --> 00:24:23,000
Teacher.

199
00:24:23,000 --> 00:24:25,600
Po is the Myanmar official who has been backing Zhao Xiaolu all this time.

200
00:24:25,600 --> 00:24:28,800
I already contacted Director Zhan and asked for support from the border police.

201
00:24:30,400 --> 00:24:32,100
What happened to you?

202
00:24:33,200 --> 00:24:34,700
I feel dizzy.

203
00:24:46,200 --> 00:24:48,800
Sit down. Sit down.

204
00:24:49,800 --> 00:24:51,600
Back off. Back off.

205
00:25:03,600 --> 00:25:05,000
Let's go.

206
00:25:19,600 --> 00:25:21,400
Hurry up.

207
00:25:23,000 --> 00:25:25,000
Hurry up.

208
00:25:26,800 --> 00:25:28,600
They are over there.

209
00:26:21,600 --> 00:26:24,800
Our leader has contacted your commander Juewen.

210
00:26:24,800 --> 00:26:26,600
Our leader has contacted your commander Juewen.

211
00:26:26,600 --> 00:26:30,800
Po betrayed the army. He is not a good officer.

212
00:26:30,800 --> 00:26:35,800
Commander Juewen is on his way here and he will work with the Chinese army to take this bad guy down.

213
00:26:35,800 --> 00:26:38,000
Commander Juewen is on his way coming here and he will work with the Chinese army to take this bad guy down.

214
00:26:38,000 --> 00:26:41,000
Everyone else, put your weapon down and cooperate with the investigation.

215
00:26:41,000 --> 00:26:42,400
You will not be found guilty if you don't know anything.

216
00:26:42,400 --> 00:26:45,600
You will not be found guilty if you don't know anything.

217
00:26:45,600 --> 00:26:48,400
<i>Help the Chinese police, Po betrayed us.</i>

218
00:26:48,400 --> 00:26:50,200
Yes, sir.

219
00:26:50,200 --> 00:26:52,200
Put your weapon down.

220
00:26:52,200 --> 00:26:55,600
Down. Put your weapon down.

221
00:26:55,600 --> 00:26:59,100
Put it down.

222
00:27:22,800 --> 00:27:25,900
-What is going on? <br>-Let's go check it out.

223
00:27:35,600 --> 00:27:40,500
The train changed direction. If it continues travelling east, we'll arrive at Laos' border.

224
00:28:09,800 --> 00:28:11,800
Po wants to escape.

225
00:28:13,200 --> 00:28:17,500
No, the one who needs to escape is Zhao Xiaolu.

226
00:28:18,400 --> 00:28:21,300
When will we meet again?

227
00:28:23,200 --> 00:28:25,300
When all the Chinese are gone,

228
00:28:26,600 --> 00:28:28,400
I will escort you back.

229
00:28:29,400 --> 00:28:31,300
She is on the train, too.

230
00:28:32,800 --> 00:28:36,300
Bring that policewoman to me. Don't let her contact anyone.

231
00:28:44,200 --> 00:28:45,700
Go in, go in.

232
00:28:50,500 --> 00:28:53,000
Track them down, track them down.

233
00:28:53,000 --> 00:28:55,000
They are somewhere here.

234
00:29:16,400 --> 00:29:18,400
No, no way.

235
00:29:24,000 --> 00:29:27,200
-We don't have time. Hurry up.

236
00:29:27,200 --> 00:29:29,400
- No, no way.<br>- Hurry up.

237
00:29:40,200 --> 00:29:42,000
Give me your hand.

238
00:29:42,000 --> 00:29:43,900
Give me your hand.

239
00:29:52,500 --> 00:29:57,000
Find an open area. Figure out a way to contact the headquarters and tell them the location of this train.

240
00:29:57,000 --> 00:29:59,000
Can you finish this mission?

241
00:29:59,800 --> 00:30:01,200
Teacher.

242
00:30:02,000 --> 00:30:04,400
Contact the border army as soon as possible.

243
00:30:04,400 --> 00:30:08,500
We have a chance if you go.

244
00:30:08,500 --> 00:30:11,100
If we stay, the only result is we will both die.

245
00:30:12,800 --> 00:30:16,800
I don't want to go. I won't go.

246
00:30:18,400 --> 00:30:21,400
I don't want to leave.

247
00:30:23,200 --> 00:30:25,200
I am not going.

248
00:31:10,800 --> 00:31:12,500
Ji Bai!

249
00:31:15,860 --> 00:31:27,070
<i>Timing and Subtitles by the Gone With the Shirt Team @ Viki<br>Please do not reuse or reupload our subtitles.</i>

250
00:31:35,600 --> 00:31:40,400
♫ <i> How I wish to say that we are not in the wrong.</i> ♫

251
00:31:40,400 --> 00:31:44,600
♫ <i> Just that the world is a circle.</i> ♫

252
00:31:44,600 --> 00:31:51,000
♫ <i> So round. After going one round, then we realise.</i> ♫

253
00:31:51,000 --> 00:31:56,600
♫ <i> With time, I have finally understood.</i> ♫

254
00:31:56,600 --> 00:32:02,200
♫ <i> Between us, day by day. </i> ♫

255
00:32:02,200 --> 00:32:05,800
♫ <i>Those stories that happened. </i>♫

256
00:32:05,800 --> 00:32:13,600
♫ <i> weren’t discovered in time and everything happened with fear of regrets.</i> ♫

257
00:32:13,600 --> 00:32:17,800
♫ <i> Your indistinct face seems to have vanished in the distance</i> ♫

258
00:32:17,800 --> 00:32:22,600
♫ <i> yet still tugs at my heart. </i> ♫

259
00:32:22,600 --> 00:32:30,500
♫ <i> The breeze past my ears reminds me this is not an illusion</i> ♫

260
00:32:30,500 --> 00:32:38,400
♫ <i> Seeing everyday as a remembrance, then you will know how intense this love is</i> ♫

261
00:32:38,400 --> 00:32:46,400
♫ <i> Before the rain ceased, I suddenly wish to see your face once more.</i> ♫

262
00:32:46,400 --> 00:32:51,000
♫ <i> Your sweet smile </i> ♫

263
00:32:51,000 --> 00:32:59,200
♫ <i> has foolishly stopped at happiness’ destination.</i> ♫

264
00:32:59,200 --> 00:33:07,200
♫ <i> The breeze past my ears reminds me this is not an illusion</i> ♫

265
00:33:07,200 --> 00:33:15,000
♫ <i> Seeing everyday as a remembrance, then you will know how intense this love is</i> ♫

266
00:33:15,000 --> 00:33:23,200
♫ <i> Before the rain ceased, I suddenly wish to see your face once more.</i> ♫

267
00:33:23,200 --> 00:33:27,400
♫ <i> Your sweet smile </i> ♫

268
00:33:27,400 --> 00:33:35,800
♫ <i> has foolishly stopped at happiness’ destination.</i> ♫

