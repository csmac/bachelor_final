1
00:02:10,083 --> 00:02:11,999
Evelyn!

2
00:04:11,333 --> 00:04:13,290
You're late.

3
00:04:39,833 --> 00:04:41,707
Did I say you could sit?

4
00:04:45,833 --> 00:04:48,457
You can start by cleaning the study.

5
00:05:10,500 --> 00:05:12,540
And don't take all day this time.

6
00:07:37,125 --> 00:07:39,540
While you're there,
you can take the bin out.

7
00:07:57,291 --> 00:07:59,207
Hello, Lorna.

8
00:08:24,750 --> 00:08:26,790
Where are you going?

9
00:08:27,916 --> 00:08:30,040
I thought I'd finished for the day.

10
00:08:30,166 --> 00:08:32,457
You finish when I say.

11
00:08:32,583 --> 00:08:36,457
- But there is nothing left to do.
- Oh, there's plenty left to do.

12
00:08:36,583 --> 00:08:38,790
You can start by rubbing my feet.

13
00:09:31,625 --> 00:09:33,457
May I go to the toilet?

14
00:09:34,083 --> 00:09:35,332
No.

15
00:09:36,541 --> 00:09:38,582
But I...

16
00:09:38,708 --> 00:09:41,749
I think it's better
that you stay by my feet

17
00:09:41,875 --> 00:09:44,707
and continue doing
what I asked you to do.

18
00:12:02,000 --> 00:12:03,332
Come in.

19
00:12:13,541 --> 00:12:15,665
I finished sweeping the porch.

20
00:12:15,791 --> 00:12:18,582
- Can I go now?
- Did you wash my things?

21
00:12:19,875 --> 00:12:22,665
- What things?
- You keep forgetting.

22
00:12:23,333 --> 00:12:25,665
I left a whole pile for you.

23
00:12:25,791 --> 00:12:29,790
OK, but can you show me
how to use the machine?

24
00:12:29,916 --> 00:12:32,374
You won't be using the machine.

25
00:13:48,500 --> 00:13:51,290
- It's all done.
- You rinsed the soap off?

26
00:13:53,625 --> 00:13:56,249
Let's see if you've done
your job properly.

27
00:14:06,500 --> 00:14:08,790
What's this doing here?

28
00:14:13,791 --> 00:14:15,665
This was on the pile.

29
00:14:15,791 --> 00:14:17,832
This was on the pile
for you to wash.

30
00:14:17,958 --> 00:14:19,874
Sorry, I..

31
00:14:20,000 --> 00:14:23,499
- I didn't see it.
- How could you not see it?

32
00:14:23,625 --> 00:14:26,540
I.. I can wash it now,
it's not a problem.

33
00:14:26,666 --> 00:14:29,374
No, you can't wash it now
and it is a problem,

34
00:14:29,500 --> 00:14:31,499
because I have other plans
for you now.

35
00:14:31,625 --> 00:14:34,249
It's just a few minutes,
that's all it takes.

36
00:14:34,375 --> 00:14:36,290
I've waited enough already.

37
00:14:36,416 --> 00:14:39,582
You haven't washed anything properly
and you forgot this.

38
00:14:40,250 --> 00:14:42,665
- Sorry.
- You will be.

39
00:14:43,625 --> 00:14:46,624
- What are you going to do?
- A little punishment.

40
00:14:46,750 --> 00:14:50,332
- But I... I can do it now.
- It's too late.

41
00:15:03,000 --> 00:15:04,624
Lie down.

42
00:15:10,625 --> 00:15:12,665
Open your mouth.

43
00:16:09,375 --> 00:16:11,832
Thank you, so much.

44
00:16:12,375 --> 00:16:13,790
Not too cold?

45
00:16:13,916 --> 00:16:15,832
The colder the better.

46
00:18:40,083 --> 00:18:42,874
By highlighting
the geographical proximity

47
00:18:43,000 --> 00:18:45,749
of Gryllotalpa gryllotalpa
and Gryllotalpa vineae,

48
00:18:45,875 --> 00:18:49,457
these recordings should demonstrate
how fundamental sound is

49
00:18:49,583 --> 00:18:51,415
as a classifying factor.

50
00:18:51,541 --> 00:18:56,374
Species identification will partly
rely upon the duration of one syllable

51
00:18:56,500 --> 00:18:58,499
usually in milliseconds.

52
00:18:58,625 --> 00:19:01,707
The carrier wave expressed
in kilohertz

53
00:19:01,833 --> 00:19:04,374
is another factor in identification.

54
00:19:04,500 --> 00:19:08,915
Since these species are so visually
indistinguishable from each other,

55
00:19:09,041 --> 00:19:13,207
the sound they produce
should differentiate the two.

56
00:19:13,333 --> 00:19:16,624
Dr Schuller, if we could hear
the Gryllotalpa gryllotalpa please.

57
00:19:40,125 --> 00:19:43,665
And now if we hear
Gryllotalpa vineae, please?

58
00:20:55,666 --> 00:20:57,499
Did I say you could stop?

59
00:21:13,166 --> 00:21:15,374
'Cynthia.

60
00:21:15,500 --> 00:21:16,665
'Cynthia.

61
00:21:17,958 --> 00:21:21,457
'This is all I ever dreamed about,

62
00:21:21,583 --> 00:21:23,457
'to be owned by you.

63
00:21:24,375 --> 00:21:26,332
'To be used by you.

64
00:21:27,208 --> 00:21:29,957
'I can't tell you how happy I am.

65
00:21:30,916 --> 00:21:33,999
'I never thought I could find
someone like you.

66
00:21:35,541 --> 00:21:38,207
'I never thought it would be possible.

67
00:21:39,750 --> 00:21:42,540
'I won't let you down, Cynthia.

68
00:21:43,166 --> 00:21:44,707
'Never.

69
00:21:44,833 --> 00:21:46,707
'I won't let you down.

70
00:21:47,750 --> 00:21:49,874
'As long as I'm yours,

71
00:21:50,000 --> 00:21:51,999
'I remain alive.

72
00:21:53,541 --> 00:21:55,665
'I love you, Cynthia.

73
00:21:56,708 --> 00:21:58,207
'I love you. '

74
00:25:09,958 --> 00:25:11,415
You're late.

75
00:25:36,125 --> 00:25:38,582
Did I say you could sit?

76
00:25:41,833 --> 00:25:43,957
You can start by cleaning the study.

77
00:25:56,083 --> 00:25:57,957
And don't take all day this time.

78
00:27:07,875 --> 00:27:09,832
So you've finished already?

79
00:27:10,833 --> 00:27:14,457
Good for you. I have something else
for you to do now.

80
00:27:21,708 --> 00:27:23,415
Come here.

81
00:28:08,375 --> 00:28:10,582
What are you doing?

82
00:28:10,708 --> 00:28:12,707
I told you to clean my boots.

83
00:28:14,291 --> 00:28:16,582
Do that again and you'll be punished.

84
00:29:34,708 --> 00:29:36,832
You don't listen, do you?

85
00:29:37,625 --> 00:29:39,415
You just don't listen!

86
00:29:39,541 --> 00:29:43,749
Now you'll see what happens
when you don't listen.

87
00:29:43,875 --> 00:29:46,290
When you're being bad and lazy.

88
00:30:25,416 --> 00:30:28,124
...what distinguishes
Thaumetopoea pityocampa

89
00:30:28,250 --> 00:30:30,249
from other Tussock moths

90
00:30:30,375 --> 00:30:32,415
is the communal nature of their larvae.

91
00:30:33,041 --> 00:30:37,249
The sun gives them enough energy
for their nocturnal foraging.

92
00:30:37,375 --> 00:30:38,457
You like it?

93
00:30:39,333 --> 00:30:40,957
- That's really nice.
- Yes?

94
00:30:41,083 --> 00:30:42,165
Yes.

95
00:31:41,708 --> 00:31:42,957
Tell me something.

96
00:31:52,958 --> 00:31:55,249
There's so many things to tell you.

97
00:31:57,791 --> 00:31:59,457
Far too many things.

98
00:32:05,916 --> 00:32:08,624
But I can start with
how much I love you...

99
00:32:11,125 --> 00:32:14,374
and how happy I am
that you're here with me.

100
00:32:16,083 --> 00:32:18,874
- How happy I am...
- Talk about the other things.

101
00:32:33,333 --> 00:32:35,290
I'm not happy.

102
00:32:37,541 --> 00:32:39,374
I'm not happy with you at all.

103
00:32:39,500 --> 00:32:41,415
- Really?
- Really.

104
00:32:41,541 --> 00:32:43,249
What have I done?

105
00:32:46,041 --> 00:32:48,207
It's what you haven't done.

106
00:32:51,625 --> 00:32:54,082
You haven't been a good maid.

107
00:32:54,208 --> 00:32:57,582
And I have to do
everything I want with you.

108
00:32:58,500 --> 00:32:59,957
You're mine now.

109
00:33:02,458 --> 00:33:04,124
Keep talking.

110
00:33:04,250 --> 00:33:06,040
Say something else.

111
00:33:10,083 --> 00:33:12,749
You haven't washed
my panties recently.

112
00:33:14,833 --> 00:33:16,915
Nor have you polished my boots.

113
00:33:20,000 --> 00:33:22,832
If you want to be a good maid,

114
00:33:22,958 --> 00:33:25,624
you have to do these things a lot more.

115
00:33:27,125 --> 00:33:29,207
Otherwise, you'll be punished.

116
00:33:32,250 --> 00:33:34,290
Keep talking.

117
00:33:36,583 --> 00:33:38,582
I don't know what to say.

118
00:33:39,166 --> 00:33:41,165
Just go back to the beginning, then.

119
00:33:48,250 --> 00:33:49,915
I'm not happy.

120
00:33:50,041 --> 00:33:51,874
I'm not happy with you at all.

121
00:33:58,375 --> 00:33:59,624
Improvise!

122
00:34:03,541 --> 00:34:05,582
When you work for me, it's for life.

123
00:34:08,083 --> 00:34:10,457
You have to do...

124
00:34:10,583 --> 00:34:13,124
whatever I want, whenever I want.

125
00:34:14,750 --> 00:34:17,415
Because if you don't,

126
00:34:17,541 --> 00:34:20,249
I just... might tie you up

127
00:34:20,375 --> 00:34:22,957
and use you as my chair
for the afternoon.

128
00:34:24,958 --> 00:34:28,332
I can read about cave crickets...

129
00:34:30,458 --> 00:34:33,790
whilst you're helpless underneath me

130
00:34:33,916 --> 00:34:37,999
and I put my weight
upon your face and...

131
00:34:39,000 --> 00:34:42,957
you just have to lie there and wait

132
00:34:43,083 --> 00:34:46,540
until I'm finished reading
my book and...

133
00:35:27,541 --> 00:35:31,749
Try to have more conviction
in your voice next time.

134
00:35:34,166 --> 00:35:35,999
More conviction.

135
00:35:37,208 --> 00:35:38,374
OK.

136
00:37:18,958 --> 00:37:24,040
...although what distinguishes the skippers
from hair streaks, coppers and blues

137
00:37:24,166 --> 00:37:27,165
is the way the clubs and antennae
gradually thicken,

138
00:37:27,291 --> 00:37:28,957
often ending in hooked points.

139
00:37:29,083 --> 00:37:30,082
Also, their larvae...

140
00:37:30,208 --> 00:37:32,915
- This is elementary level.
- Shh.

141
00:37:33,041 --> 00:37:35,749
...pupating in silk shelters
at the base of legumes or herbs.

142
00:37:35,875 --> 00:37:39,290
However, in order to survive,
the larvae need plentiful light,

143
00:37:39,416 --> 00:37:42,290
which often occurs inadvertently
when livestock are grazing.

144
00:37:42,416 --> 00:37:44,749
The most explicit example for this

145
00:37:44,875 --> 00:37:47,290
is the larvae
of the Silver-spotted Skipper,

146
00:37:47,416 --> 00:37:49,624
which feed on sheep's fescue grass,

147
00:37:49,750 --> 00:37:53,915
which in turn is never allowed
to grow too plentifully by grazing cows.

148
00:37:54,041 --> 00:37:54,832
Thank you.

149
00:37:54,958 --> 00:37:58,749
Thank you, Dr Viridana.
Would anyone like to ask a question?

150
00:38:00,833 --> 00:38:04,707
I often have trouble differentiating
between the Small and Large Skipper.

151
00:38:04,833 --> 00:38:06,790
Is there any way to tell them apart?

152
00:38:06,916 --> 00:38:09,207
Easy.
One is large, one is small.

153
00:38:09,333 --> 00:38:12,665
A lot of people have that trouble, since
they are both the same size and colour.

154
00:38:12,791 --> 00:38:17,457
But the Small Skipper often holds its
wings at a 45-degree angle when resting.

155
00:38:17,583 --> 00:38:19,999
Its orange colouring is more uniform, too,

156
00:38:20,125 --> 00:38:23,957
while the Large Skipper has mottled
orange patterns on its underside.

157
00:38:24,958 --> 00:38:26,165
Any other questions?

158
00:38:28,916 --> 00:38:31,624
Could you talk about the venation
of the Grizzled Skipper?

159
00:38:33,291 --> 00:38:35,540
How do you mean?

160
00:38:35,666 --> 00:38:39,165
In terms of longitudinal and cross veins

161
00:38:39,291 --> 00:38:44,415
and how... how they vary in relation
to other species within the family.

162
00:38:44,541 --> 00:38:47,582
Well, the Grizzled Skipper is
so visually distinctive amongst the family

163
00:38:47,708 --> 00:38:50,457
that there's no need to go
into such detail on the wings.

164
00:38:50,583 --> 00:38:53,332
It's the only skipper in the region
to have black markings

165
00:38:53,458 --> 00:38:56,457
- and is by far the easiest to identify.
- I know.

166
00:38:57,208 --> 00:39:02,165
I was just wondering how one goes
about classifying other Pyrgus species

167
00:39:02,291 --> 00:39:04,082
in other regions.

168
00:39:04,208 --> 00:39:08,249
I'm afraid it's something I can't answer.
This talk only concerns this region.

169
00:39:09,333 --> 00:39:12,082
Any other questions? No?

170
00:39:12,208 --> 00:39:14,040
OK, well, once again,

171
00:39:14,166 --> 00:39:16,207
thank you very much, Dr Viridana.

172
00:39:16,333 --> 00:39:18,124
For the next talk, we'll have...

173
00:39:18,250 --> 00:39:20,332
- What's that look?
- Nothing.

174
00:39:20,458 --> 00:39:21,915
...parasitic Hymenoptera.

175
00:40:00,291 --> 00:40:02,207
I feel so stupid, Cynthia.

176
00:40:02,333 --> 00:40:07,040
- Stupid about what?
- That Grizzled Skipper nonsense.

177
00:40:07,166 --> 00:40:11,040
- It's OK. Don't worry about it.
- You don't think I'm stupid, do you?

178
00:40:11,625 --> 00:40:14,249
Of course not. It's OK.

179
00:40:22,458 --> 00:40:24,040
What?

180
00:40:24,166 --> 00:40:26,040
Can you let me go now?

181
00:40:29,000 --> 00:40:30,165
No.

182
00:44:16,625 --> 00:44:19,374
So, you would like your lover
to sleep on top of you?

183
00:44:19,500 --> 00:44:20,790
Ah, yes.

184
00:44:21,458 --> 00:44:24,332
I make two types of bed
for what you want.

185
00:44:24,458 --> 00:44:28,249
One where the compartment underneath
slides out and then back in,

186
00:44:28,375 --> 00:44:30,749
or one where the bed mattress lifts up

187
00:44:30,875 --> 00:44:33,624
and then can be pulled down
and locked once you're inside.

188
00:44:33,750 --> 00:44:35,707
Once you're inside, it's the same.

189
00:44:35,833 --> 00:44:39,165
It's just the sensation of being put in
and set free that is different.

190
00:44:39,291 --> 00:44:41,124
Which one would you recommend?

191
00:44:41,250 --> 00:44:45,415
The bed lifting up is often more favoured
than the drawer-style compartment.

192
00:44:45,541 --> 00:44:47,624
Sometimes the drawers can get stuck.

193
00:44:47,750 --> 00:44:50,790
Thankfully, that usually happens
when they're out, rather than in,

194
00:44:50,916 --> 00:44:54,165
but it's a critical moment when they're
about to be locked up for the night

195
00:44:54,291 --> 00:44:56,749
and then realise that they can't be.

196
00:44:56,875 --> 00:45:00,249
Well, the bed lifting up
seems fine by me.

197
00:45:00,375 --> 00:45:03,207
It's just a simple spring
that lifts the bed up.

198
00:45:03,333 --> 00:45:07,707
Customers find the slamming effect of the
bed closing down on them very dramatic.

199
00:45:07,833 --> 00:45:09,915
That bed is very popular.

200
00:45:10,625 --> 00:45:14,290
I think I made one for someone
in the neighbourhood a few months ago.

201
00:45:14,416 --> 00:45:16,124
- Oh, really?
- Yeah.

202
00:45:16,250 --> 00:45:20,249
- Who was that?
- Ah, I can't remember her name.

203
00:45:20,375 --> 00:45:22,707
Let me think...

204
00:45:22,833 --> 00:45:26,207
It's the house with the wisteria porch
and yellow colonnade,

205
00:45:26,333 --> 00:45:28,749
a few minutes down the lane.

206
00:45:28,875 --> 00:45:31,124
- Yellow colonnade?
- That's got to be Ambrosia.

207
00:45:31,250 --> 00:45:33,124
Is that her house?

208
00:45:33,250 --> 00:45:35,457
She might have painted
the colonnade another colour,

209
00:45:35,583 --> 00:45:37,749
but that's got to be Ambrosia.

210
00:45:37,875 --> 00:45:40,874
I'm surprised you could make a bed
big enough for her to be locked in.

211
00:45:41,000 --> 00:45:43,415
OK, then it's the same customer
I'm thinking of.

212
00:45:44,250 --> 00:45:46,749
But yes, that design
is a lot more popular

213
00:45:46,875 --> 00:45:48,957
and because of the spring,
it's easy to lift up.

214
00:45:49,083 --> 00:45:52,040
With both designs,
there's a lock at each end of the bed,

215
00:45:52,166 --> 00:45:53,999
but even without those,

216
00:45:54,125 --> 00:45:55,999
the weight of one's lover sleeping on top

217
00:45:56,125 --> 00:45:58,165
means that it's almost
impossible to escape.

218
00:45:59,125 --> 00:46:01,249
- And...
- I can also add metal hooks to the inside

219
00:46:01,375 --> 00:46:04,249
if you like having your hands and feet
tied to something.

220
00:46:05,083 --> 00:46:06,999
That sounds perfect.

221
00:46:07,125 --> 00:46:10,332
And how long does it take
once we've placed an order?

222
00:46:10,458 --> 00:46:13,624
Oh, usually around eight weeks.

223
00:46:16,000 --> 00:46:17,540
Eight weeks!

224
00:46:17,666 --> 00:46:24,415
I'm afraid there's a lot of demand
and it takes time to make each bed.

225
00:46:24,541 --> 00:46:25,999
Eight weeks?

226
00:46:26,125 --> 00:46:28,915
Is there any way
you can offer a faster service?

227
00:46:29,041 --> 00:46:31,582
It's just that Evelyn's birthday
is coming up

228
00:46:31,708 --> 00:46:33,749
and I was planning this
as a present.

229
00:46:34,500 --> 00:46:38,707
- Oh, it depends when your birthday is.
- Two weeks.

230
00:46:38,833 --> 00:46:41,082
It will be my birthday in two weeks.

231
00:46:42,166 --> 00:46:43,582
Two weeks?

232
00:46:45,791 --> 00:46:48,249
It's impossible. I'm sorry.

233
00:46:48,375 --> 00:46:51,790
We can offer you extra,
if it helps.

234
00:46:51,916 --> 00:46:54,707
We have a mount
of extremely rare Satyrids.

235
00:46:54,833 --> 00:46:57,374
You wouldn't find those
even in the museums.

236
00:46:57,500 --> 00:46:59,832
They're worth far more
than Nymphalids or Burnets.

237
00:46:59,958 --> 00:47:02,874
Evelyn's right.
It's a highly collectable mount.

238
00:47:03,000 --> 00:47:06,874
I'm sorry, but I have too much pressure
from other customers.

239
00:47:07,000 --> 00:47:11,374
And I cannot do it any sooner
than eight weeks. I'm so sorry.

240
00:47:12,500 --> 00:47:15,165
Well, we'd have to discuss it,
anyway, before we order.

241
00:47:18,541 --> 00:47:22,040
It's OK.
There will be another birthday.

242
00:47:22,166 --> 00:47:25,499
If you like, we could look for something
that is not so much in demand.

243
00:47:26,291 --> 00:47:28,040
The bed would have been perfect.

244
00:47:31,375 --> 00:47:34,540
Would a human toilet
be a suitable compromise?

245
00:47:36,833 --> 00:47:37,915
Really?

246
00:47:38,041 --> 00:47:40,624
- Well...
- I really have to go now.

247
00:47:40,750 --> 00:47:43,790
But wait.
Don't you want to hear about this?

248
00:47:43,916 --> 00:47:46,415
Oh, I have to go.
I told you I had to leave.

249
00:47:47,375 --> 00:47:50,082
I'm sorry for being so abrupt.
It was a pleasure meeting you.

250
00:47:50,208 --> 00:47:51,707
It was a pleasure for me.

251
00:47:52,541 --> 00:47:54,790
I'll be back in an hour,
will I meet you here?

252
00:47:54,916 --> 00:47:56,124
I'll be here.

253
00:47:56,250 --> 00:47:58,624
Don't make any orders
without consulting with me.

254
00:47:58,750 --> 00:48:01,332
I won't, don't worry.
I'll let you know.

255
00:48:01,458 --> 00:48:02,957
- Bye-bye.
- Bye.

256
00:48:06,041 --> 00:48:08,790
So... there are two types available.

257
00:48:08,916 --> 00:48:11,624
The basic model allows you
to experience...

258
00:52:38,208 --> 00:52:40,249
What is that look?

259
00:53:12,250 --> 00:53:13,624
There.

260
00:53:13,750 --> 00:53:16,249
Just a little longer, please.
I'm in such agony.

261
00:53:16,375 --> 00:53:18,457
Rubbing it's not going to help.

262
00:53:18,583 --> 00:53:21,082
You need an ice pack
and some painkillers.

263
00:53:25,500 --> 00:53:28,040
Do you want me to call a doctor?

264
00:53:28,166 --> 00:53:30,082
No, it's OK.

265
00:53:35,875 --> 00:53:39,457
My back does this every few years.
It'll get better.

266
00:53:41,916 --> 00:53:42,915
Here.

267
00:53:44,291 --> 00:53:45,582
Thank you.

268
00:53:59,583 --> 00:54:00,624
Thanks.

269
00:54:15,958 --> 00:54:17,999
- Tell me?
- OK.

270
00:54:19,541 --> 00:54:21,540
You don't sound enthusiastic.

271
00:54:21,666 --> 00:54:23,499
It says to be cold.

272
00:54:23,625 --> 00:54:25,749
Yes, but not now.

273
00:54:25,875 --> 00:54:29,499
And one thing I forgot to write -
don't talk about it.

274
00:54:29,625 --> 00:54:32,499
- Just surprise me.
- I know, I know.

275
00:54:32,625 --> 00:54:35,832
Well, you were a bit slow
to surprise me last time.

276
00:54:35,958 --> 00:54:38,499
Well, it's not a surprise
if you're expecting it, is it?

277
00:54:39,625 --> 00:54:43,499
I know, but... what I mean by surprise
is within 24 hours.

278
00:54:45,250 --> 00:54:48,374
But not in the first hour,
because that wouldn't be a surprise.

279
00:54:48,500 --> 00:54:49,540
OK, OK.

280
00:54:49,666 --> 00:54:51,707
And not in the last hour, either.

281
00:54:52,708 --> 00:54:54,707
I'm just all frustrated by then.

282
00:54:57,875 --> 00:54:59,957
So within 22 hours, then?

283
00:55:04,333 --> 00:55:05,332
Yes.

284
00:55:05,458 --> 00:55:07,040
Anything else?

285
00:55:08,375 --> 00:55:09,415
No.

286
00:56:30,500 --> 00:56:32,374
Come in.

287
00:56:42,916 --> 00:56:44,665
It's all done.

288
00:56:45,833 --> 00:56:47,915
You rinsed the soap off?

289
00:56:51,166 --> 00:56:53,624
Let's see if you've done
your job properly.

290
00:57:11,375 --> 00:57:13,165
What's this doing here?

291
00:57:23,208 --> 00:57:26,499
How long do I have to stay
in here for?

292
00:57:26,625 --> 00:57:28,582
For as long as I want.

293
00:58:57,041 --> 00:58:59,165
Are you sure you're going to be OK?

294
00:59:07,000 --> 00:59:08,665
Can you breathe in there?

295
00:59:08,791 --> 00:59:10,874
Yes!

296
00:59:36,000 --> 00:59:38,207
Pinastri.

297
00:59:41,666 --> 00:59:43,040
Pinastri.

298
00:59:50,625 --> 00:59:52,499
Pinastri!

299
01:00:17,916 --> 01:00:19,790
- What happened?
- Nothing.

300
01:00:19,916 --> 01:00:22,832
- Just a mosquito bite.
- Aw.

301
01:00:27,375 --> 01:00:30,707
I told you,
it's more fun to sleep with me.

302
01:00:31,333 --> 01:00:34,915
Maybe you can put me back in there,
but just don't tie me.

303
01:00:35,041 --> 01:00:37,124
Come to bed.

304
01:02:09,958 --> 01:02:13,332
'This is all I ever dreamed about.

305
01:02:16,458 --> 01:02:19,749
'This is all I ever dreamed about. '

306
01:03:05,833 --> 01:03:07,874
Pinastri.

307
01:03:13,625 --> 01:03:15,540
Lie down.

308
01:03:18,875 --> 01:03:20,957
Open your mouth.

309
01:03:39,125 --> 01:03:42,415
Try turning the tap on.
Shh!

310
01:04:01,958 --> 01:04:06,415
- Why did you write about mole crickets?
- Why not?

311
01:04:08,083 --> 01:04:10,624
Such ugly things.

312
01:04:10,750 --> 01:04:12,957
No wonder they hide
under the ground.

313
01:04:13,083 --> 01:04:16,040
Ugly maybe,
but beautifully eloquent.

314
01:04:20,250 --> 01:04:21,540
Aargh!

315
01:04:21,666 --> 01:04:26,540
- Are you OK?
- OK. Just about.

316
01:04:31,166 --> 01:04:34,665
It would just be nice
if you volunteered to rub my back.

317
01:04:35,625 --> 01:04:37,290
Sorry.

318
01:04:37,416 --> 01:04:39,374
I didn't know you needed anything.

319
01:04:39,500 --> 01:04:41,665
Well, you can see
that I'm in pain.

320
01:04:41,791 --> 01:04:46,415
Well, it's not exactly inspiring
to see you dressed like that.

321
01:04:48,708 --> 01:04:51,165
Good thing you're not a doctor, then.

322
01:04:51,291 --> 01:04:54,415
That's really some look
you have there

323
01:04:55,333 --> 01:04:57,540
Even the tramps are less shabby.

324
01:04:59,916 --> 01:05:03,540
I'm starting to wonder why I bothered
buying you a whole wardrobe.

325
01:05:03,666 --> 01:05:07,749
I need an instruction manual to get
into half of the things you buy me.

326
01:05:07,875 --> 01:05:11,249
I would just like to feel comfortable,
thank you very much.

327
01:05:21,625 --> 01:05:23,332
This is giving me a headache.

328
01:05:30,416 --> 01:05:32,832
You're not going to turn it off?

329
01:05:34,041 --> 01:05:36,415
Did you ask me to?

330
01:06:29,000 --> 01:06:31,415
How long
do I have to stay in here for?

331
01:06:32,125 --> 01:06:34,207
For as long as I want.

332
01:07:17,208 --> 01:07:19,207
Morning, Lorna.

333
01:07:37,625 --> 01:07:39,874
Do you want to go with me
to the library?

334
01:07:43,000 --> 01:07:44,915
What time is it?

335
01:07:46,083 --> 01:07:47,915
It's around two.

336
01:07:52,708 --> 01:07:55,707
I thought we could also
have an ice cream after.

337
01:07:55,833 --> 01:07:58,332
Another time.

338
01:07:59,125 --> 01:08:02,290
The Institute will close for winter
any day soon.

339
01:08:02,416 --> 01:08:03,832
I know.

340
01:08:06,958 --> 01:08:09,082
So you're not coming, then?

341
01:08:10,458 --> 01:08:14,249
I need to sleep if you're going
to put me in the trunk tonight.

342
01:08:15,583 --> 01:08:17,415
That's news to me.

343
01:08:19,500 --> 01:08:22,915
It would be nice if you did it
without having to be asked.

344
01:09:58,166 --> 01:10:01,957
There used to be a time when my body
would just repair itself overnight.

345
01:10:02,083 --> 01:10:04,874
It's all downhill from now.

346
01:10:05,000 --> 01:10:09,624
Don't be such a pessimist!
You have many, many years left in you.

347
01:10:09,750 --> 01:10:12,915
Oh, that reminds me.
On the subject of getting old.

348
01:10:14,125 --> 01:10:18,207
- I spoke to Lorna the other day.
- I don't know why you bother.

349
01:10:18,333 --> 01:10:21,415
What's that miserable sow
have to say, anyway?

350
01:10:21,541 --> 01:10:25,290
She wanted to alert us
to some root problems with the birch.

351
01:10:25,416 --> 01:10:28,082
She claims they might be
unsettling her foundations.

352
01:10:28,208 --> 01:10:31,124
Let her claim away.
What a nonsense.

353
01:10:31,250 --> 01:10:34,999
That tree rests too far from the house
to have any effect.

354
01:10:35,125 --> 01:10:36,874
Ignore her.

355
01:10:37,000 --> 01:10:39,915
She's nothing
but a bloated bag of trapped wind.

356
01:10:41,500 --> 01:10:46,165
She also claims she spotted you polishing
Dr Schuller's boots in her backyard.

357
01:10:47,625 --> 01:10:50,415
Tell me that's just another sly rumour.

358
01:10:50,541 --> 01:10:53,499
Don't be ridiculous.
You know what Lorna's like.

359
01:10:53,625 --> 01:10:57,749
Are you really going to get taken in
by such idle gossip?

360
01:10:57,875 --> 01:11:00,374
No, but this was rather precise.

361
01:11:00,500 --> 01:11:03,665
It would hurt me more
if you denied such an accusation.

362
01:11:03,791 --> 01:11:07,749
So, I'm condemned
for denying a false accusation?

363
01:11:07,875 --> 01:11:10,707
- That's a good move
- Just tell me the truth.

364
01:11:10,833 --> 01:11:12,915
I'm telling you the truth.

365
01:11:13,041 --> 01:11:16,457
That Lorna makes it her business
to spread dirty lies.

366
01:11:17,750 --> 01:11:20,332
You polished her boots, didn't you?

367
01:11:22,666 --> 01:11:23,790
Tell me.

368
01:11:25,750 --> 01:11:27,374
Don't lie to me, Evelyn.

369
01:11:30,000 --> 01:11:31,999
Tell me what you did.

370
01:11:32,791 --> 01:11:34,624
I'm sorry.

371
01:11:34,750 --> 01:11:36,582
I'm really sorry.

372
01:11:40,375 --> 01:11:42,999
So what else did you two get up to?

373
01:11:43,125 --> 01:11:45,249
- Nothing.
- Really?

374
01:11:46,958 --> 01:11:49,207
Nothing. I swear! Nothing!

375
01:11:49,333 --> 01:11:51,415
I just polished her boots.

376
01:11:53,083 --> 01:11:55,082
I find that hard to believe.

377
01:11:55,833 --> 01:11:57,957
Did she punish you?

378
01:11:59,041 --> 01:12:01,624
- Did she punish you?
- No!

379
01:12:01,750 --> 01:12:05,290
So you just polished her boots
out of pure good will?

380
01:12:05,416 --> 01:12:06,665
OK, she...

381
01:12:06,791 --> 01:12:09,124
She told me off a bit.

382
01:12:09,250 --> 01:12:11,540
That's all. Nothing more.

383
01:12:13,000 --> 01:12:15,415
How did she tell you off?

384
01:12:15,541 --> 01:12:18,457
- Let's not...
- How did she tell you off?

385
01:12:18,583 --> 01:12:21,874
I don't know. Just some harsh words.

386
01:12:22,000 --> 01:12:24,707
Some threats.
That's all. Then I went home.

387
01:12:24,833 --> 01:12:28,374
- I bet you did.
- I didn't do anything.

388
01:12:28,500 --> 01:12:32,457
We didn't kiss, we didn't touch.
Nothing, I swear.

389
01:12:33,250 --> 01:12:35,665
Believe me, I didn't do anything.

390
01:12:36,666 --> 01:12:39,499
That's your idea of not doing anything?

391
01:12:39,625 --> 01:12:42,957
For what it's worth,
you might as well have gone all the way.

392
01:12:43,083 --> 01:12:45,207
The damage has been done.

393
01:12:47,041 --> 01:12:50,249
- What damage?
- You betrayed me!

394
01:12:52,708 --> 01:12:54,624
Do you call that betrayal?

395
01:12:54,750 --> 01:12:57,915
Why would you lie about something
if you didn't regard it as betrayal?

396
01:12:58,041 --> 01:13:02,832
Because you'd automatically think
something else happened when it didn't.

397
01:13:02,958 --> 01:13:05,957
- Where are you going?
- Somewhere.

398
01:13:06,083 --> 01:13:07,582
Anywhere.

399
01:13:09,041 --> 01:13:11,915
- Anywhere away from you.
- Please, Cynthia.

400
01:13:12,041 --> 01:13:16,499
Please. I'm sorry.
That was just a bad day.

401
01:13:16,625 --> 01:13:18,999
I was feeling weak. I was frustrated.

402
01:13:19,125 --> 01:13:21,624
I'm sorry.
I wasn't getting what I needed here.

403
01:13:21,750 --> 01:13:26,207
So, had I ordered a human toilet,
none of this would've happened then?

404
01:13:28,791 --> 01:13:31,749
I'm sorry. I'm weak and I'm sorry.

405
01:13:33,500 --> 01:13:35,749
Cynthia! Please!

406
01:13:35,875 --> 01:13:37,082
Please, Cynthia!

407
01:13:37,541 --> 01:13:38,999
I love you. Please.

408
01:13:39,708 --> 01:13:45,290
The tunnelling larvae of Xestobium
rufovillosum are a severe pest

409
01:13:45,416 --> 01:13:47,749
in both buildings
and in the timber trade.

410
01:13:47,875 --> 01:13:50,415
Their trail of devastation
isn't as patterned

411
01:13:50,541 --> 01:13:52,957
as species from
the Scolytidae family.

412
01:13:53,083 --> 01:13:55,749
Their galleries and tunnels
deep within the wood

413
01:13:55,875 --> 01:13:58,165
are distinctive to each species.

414
01:13:59,250 --> 01:14:02,999
Xestobium rufovillosum favours oak,
whether living or felled.

415
01:14:03,750 --> 01:14:06,874
'Although seldom seen,
its presence is often felt

416
01:14:07,000 --> 01:14:09,374
'by the sinister
tapping sound it makes

417
01:14:09,500 --> 01:14:11,499
'to attract a mate during the spring. '

418
01:14:17,833 --> 01:14:19,790
Wait! Wait!

419
01:14:37,333 --> 01:14:39,415
Now, my love.

420
01:14:39,541 --> 01:14:41,957
A small birthday treat for you.

421
01:14:43,500 --> 01:14:46,999
AII I can say is
I hope you like it as much as I do.

422
01:14:54,375 --> 01:14:56,082
Is this a joke?

423
01:14:56,208 --> 01:14:59,915
No, no.
The ingredients for your birthday cake.

424
01:15:00,041 --> 01:15:01,999
Which you are going to make.

425
01:15:02,916 --> 01:15:04,915
Here's the recipe.

426
01:15:05,833 --> 01:15:07,874
And while you're doing that,

427
01:15:08,000 --> 01:15:10,957
I'll go change into something nicer.

428
01:15:11,916 --> 01:15:14,624
- I don't know if I can...
- Oh, you have to.

429
01:15:15,708 --> 01:15:17,457
Read the recipe

430
01:15:17,583 --> 01:15:19,540
and make the cake.

431
01:15:20,500 --> 01:15:22,790
I want you ready by nine.

432
01:15:52,750 --> 01:15:54,665
Now, put it here.

433
01:16:04,916 --> 01:16:07,124
Where's my fork?

434
01:16:28,791 --> 01:16:30,999
- What about...?
- Don't come so close.

435
01:16:31,125 --> 01:16:33,582
Your breath is like a hyena.

436
01:16:35,958 --> 01:16:37,957
Now, lie down.

437
01:16:39,375 --> 01:16:40,540
Here.

438
01:17:03,125 --> 01:17:05,415
- This is not funny.
- Did I say you could speak?

439
01:17:05,541 --> 01:17:06,749
Pinastri.

440
01:17:07,791 --> 01:17:09,499
Pinastri.

441
01:17:10,666 --> 01:17:12,040
Pinastri, Pinastri.

442
01:17:14,250 --> 01:17:17,665
Oh, if we could all just say Pinastri
to end our torments.

443
01:20:29,458 --> 01:20:31,665
'What's this doing here?

444
01:20:31,791 --> 01:20:33,665
'This was on the pile.

445
01:20:33,791 --> 01:20:36,540
'This was on the pile for you to wash. '

446
01:20:36,666 --> 01:20:39,249
'I'm sorry. I didn't see it. '

447
01:20:39,375 --> 01:20:43,207
'How could you not see it?
I left it there on the pile.

448
01:20:43,333 --> 01:20:46,124
'Answer me.
How could you not see it?'

449
01:21:01,750 --> 01:21:04,332
'But I can wash it now.
It's not a problem. '

450
01:21:04,458 --> 01:21:06,915
'No, you can't wash it now
and it is a problem,

451
01:21:07,041 --> 01:21:09,665
'because I have other plans
for you now. '

452
01:21:09,791 --> 01:21:12,415
- 'Sorry. '
- 'You will be. '

453
01:22:30,250 --> 01:22:32,457
Pinastri.

454
01:22:39,958 --> 01:22:48,999
Pinastri.

455
01:23:59,750 --> 01:24:01,082
'Pinastri. '

456
01:25:32,083 --> 01:25:34,082
Pinastri.

457
01:28:48,416 --> 01:28:50,207
Cynthia.

458
01:28:53,208 --> 01:28:55,540
Can you sit on me?

459
01:28:57,500 --> 01:28:58,874
Please?

460
01:29:47,583 --> 01:29:50,040
'The anatomy of Gryllotalpidae

461
01:29:50,166 --> 01:29:53,415
'is highly modified
for a subterranean existence.

462
01:29:54,625 --> 01:29:58,207
'The squat and broad
velvety haired body

463
01:29:58,333 --> 01:30:00,915
'with flattened claws
and short antennae

464
01:30:01,041 --> 01:30:03,499
'is designed to burrow into the soil,

465
01:30:03,625 --> 01:30:06,874
'occasionally to depths of one metre. '

466
01:30:07,541 --> 01:30:12,040
The spade-like forelimbs
are enlarged and very powerful,

467
01:30:12,166 --> 01:30:14,415
ideally suited for digging.

468
01:30:15,458 --> 01:30:17,332
'With the advent of winter,

469
01:30:17,458 --> 01:30:21,707
'all Gryllotalpidae retreat
into their burrows. '

470
01:30:21,833 --> 01:30:24,249
There they will remain
in hibernation,

471
01:30:24,375 --> 01:30:27,040
still and dormant,

472
01:30:27,166 --> 01:30:29,999
until the spring sun
warms the soil enough

473
01:30:30,125 --> 01:30:32,415
to stir them from their slumber.

474
01:34:01,708 --> 01:34:03,665
It's all done.

475
01:34:05,875 --> 01:34:07,999
You rinsed the soap off?

476
01:34:11,250 --> 01:34:13,749
Let's see if you've done
your job properly.

477
01:34:38,791 --> 01:34:40,832
What's th...?

478
01:34:49,416 --> 01:34:51,707
What's this doing here?

479
01:34:56,625 --> 01:34:58,582
I left it on the pile.

480
01:34:59,791 --> 01:35:02,499
I left it on the pile
for you to wash.

481
01:35:02,625 --> 01:35:04,165
Sorry... I...

482
01:35:05,416 --> 01:35:07,540
I didn't see it.

483
01:35:10,416 --> 01:35:12,415
How could you not see it?

484
01:35:14,958 --> 01:35:17,540
I left it there

485
01:35:17,666 --> 01:35:19,040
on the pile.

486
01:35:19,166 --> 01:35:21,207
But I can wash it now.

487
01:35:22,250 --> 01:35:24,040
It's not a problem.

488
01:35:25,250 --> 01:35:28,040
No, you can't wash it now.

489
01:35:29,583 --> 01:35:31,582
And it is a problem.

490
01:35:36,500 --> 01:35:38,540
I can do this. OK

491
01:35:48,000 --> 01:35:49,999
I'm sorry.

492
01:35:51,125 --> 01:35:55,749
Please don't be mad at me.

493
01:35:57,083 --> 01:35:58,665
It's OK.

494
01:36:02,791 --> 01:36:07,374
I can change, if this is what
it does to you. I can change.

495
01:36:08,875 --> 01:36:11,124
All this is just a luxury.

496
01:36:13,000 --> 01:36:14,832
The important thing is you.

497
01:36:14,958 --> 01:36:19,249
No, but maybe you'll end up
resenting me. We can't win.

498
01:36:19,375 --> 01:36:21,415
How can I resent you?

499
01:36:21,541 --> 01:36:26,124
I love you.

500
01:36:27,125 --> 01:36:30,165
I know have a different way
of showing it.

501
01:36:30,875 --> 01:36:32,832
But I love you.

502
01:36:36,000 --> 01:36:39,249
You don't believe me,
do you?

503
01:36:52,416 --> 01:36:54,999
What do you want me to do
to prove it?

504
01:36:57,875 --> 01:37:02,915
Just tell me.
Just tell me what to do and I'll do it.

505
01:37:03,041 --> 01:37:06,165
Oh, please, believe that I love you.

506
01:38:58,541 --> 01:39:02,207
- 'Are you OK?'
- 'Of course. '

507
01:39:02,333 --> 01:39:05,249
- 'Are you sure?'
- 'I'm sure.

508
01:39:05,375 --> 01:39:07,415
'I'm sure, don't worry.

509
01:39:08,500 --> 01:39:10,499
'Everything's fine.

510
01:39:11,625 --> 01:39:13,790
'Everything's more than fine. '

511
01:39:15,166 --> 01:39:17,707
- 'Really?'
- 'Really.

512
01:39:18,416 --> 01:39:20,124
'Everything is fine. '

