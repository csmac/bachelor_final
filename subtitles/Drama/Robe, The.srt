﻿1
00:00:12,099 --> 00:00:20,099
<font face="Monotype Corsiva" color=#D900D9"> ♪ BurtonW ♪ </ font>

2
00:01:45,500 --> 00:01:47,858
The Indians, who
went to Flangis,

3
00:01:47,939 --> 00:01:51,336
thought there was
a city of gold ...

4
00:01:51,377 --> 00:01:53,734
QUEBEC, NORTH AMERICA, 1634.

5
00:01:53,775 --> 00:01:56,333
In the South there is a city of gold.

6
00:01:56,373 --> 00:01:57,532
My brother saw it.

7
00:02:01,370 --> 00:02:02,928
Tramblé.

8
00:02:02,969 --> 00:02:04,488
Look at these, Tramblé.

9
00:02:07,006 --> 00:02:09,204
"Where did you trap them,
Sainte Marie"?

10
00:02:09,244 --> 00:02:10,723
In the forest.

11
00:02:10,763 --> 00:02:12,402
It's my secret, Tramblé.

12
00:02:12,442 --> 00:02:15,040
And how do you pay the
Indian trappers?

13
00:02:15,081 --> 00:02:17,799
With knives, Father.
With cooking pots.

14
00:02:17,838 --> 00:02:20,557
With Brandy and Whiskey, I am told.
With Brandy.

15
00:02:27,592 --> 00:02:30,230
And what if there is no mission?

16
00:02:30,270 --> 00:02:32,149
What if your father Laforgue arrives ...

17
00:02:32,189 --> 00:02:34,388
to find yet another
two Jesuit martyrs?

18
00:02:34,427 --> 00:02:36,786
We'll cross that bridge
when we come to it.

19
00:02:36,866 --> 00:02:38,105
It's not we.

20
00:02:38,145 --> 00:02:40,264
It's Father Laforgue
who must cross it.

21
00:02:41,422 --> 00:02:43,621
Has he journeyed far from this settlement?

22
00:02:43,661 --> 00:02:44,980
"Even in good weather?"

23
00:02:47,219 --> 00:02:50,336
He has been studying the Huron and
Algonquin languages.

24
00:02:50,377 --> 00:02:53,015
He is very dedicated and devout.

25
00:02:53,054 --> 00:02:54,214
Of course.

26
00:02:55,374 --> 00:02:58,052
Even I have been on these
journeys, Monsieur Champlain ...

27
00:02:58,771 --> 00:03:02,408
from the lakes of Hurons
to the rivers of Maine.

28
00:03:02,449 --> 00:03:05,007
- And now ...
- We are too old.

29
00:03:05,047 --> 00:03:08,884
- Perhaps, Younger men are more suitable...
- Perhaps. Perhaps not.

30
00:03:15,080 --> 00:03:17,958
1500 miles by canoe ...

31
00:03:17,998 --> 00:03:19,957
in that country,

32
00:03:19,997 --> 00:03:21,756
at the beginning of the winter ...

33
00:03:23,595 --> 00:03:25,873
Death is almost certain,
Father Bourque.

34
00:03:25,913 --> 00:03:29,151
Death is not always
a great evil, Monsieur Champlain.

35
00:03:33,068 --> 00:03:36,586
God should have made me a Jesuit,
to have answers for everything.

36
00:03:49,098 --> 00:03:51,895
He must paddle with them
12 hours a day,

37
00:03:51,936 --> 00:03:53,854
or they will not respect him.

38
00:03:53,894 --> 00:03:56,652
He must smile
and not show anger.

39
00:03:56,692 --> 00:04:00,090
He must carry a pack animals loads
on his back as they do.

40
00:04:00,130 --> 00:04:04,168
The journey, like our
lives is in God's hands.

41
00:04:11,362 --> 00:04:14,200
Look at them. The English
and the Dutch are colonists.

42
00:04:14,240 --> 00:04:15,439
We have priests.

43
00:04:16,479 --> 00:04:18,518
Who would want to bring a woman here.

44
00:04:18,558 --> 00:04:21,516
And have kids running half
wild in the woods?

45
00:04:21,595 --> 00:04:23,475
We are Starving in winter..

46
00:04:26,712 --> 00:04:28,511
- Good afternoon, father.
- Good afternoon.

47
00:04:28,551 --> 00:04:29,750
Good afternoon, Father.

48
00:04:32,908 --> 00:04:35,706
- Do you think they're going upriver?
- Who cares?

49
00:04:35,746 --> 00:04:37,065
As long as I am not going.

50
00:04:37,105 --> 00:04:39,423
Leave the forest
to the savages.

51
00:04:39,463 --> 00:04:43,141
Remember that priest
who came back like that?

52
00:04:43,181 --> 00:04:45,459
It was the Iroquois.
They caught him.

53
00:04:45,499 --> 00:04:47,578
That was just fingers.

54
00:04:47,618 --> 00:04:49,976
One might lose something, that is more useful.

55
00:06:46,898 --> 00:06:49,856
Why do you want to accompany
Father Laforgue?

56
00:06:52,135 --> 00:06:55,013
For the greater glory of God.

57
00:06:55,092 --> 00:06:56,092
Indeed?

58
00:06:57,051 --> 00:06:59,690
I am told you're bored
in this settlement.

59
00:07:00,769 --> 00:07:02,367
Sometimes. A little.

60
00:07:03,846 --> 00:07:07,004
To come all this way
and to just build huts ...

61
00:07:07,044 --> 00:07:08,363
I understand.

62
00:07:09,722 --> 00:07:11,721
Can you read and write?

63
00:07:12,561 --> 00:07:15,599
Yes, Father. I was taught by my uncle,
who was a priest in Tour.

64
00:07:21,075 --> 00:07:22,994
He speaks it more fluently than I do.

65
00:07:25,712 --> 00:07:28,110
If we send you and if ...

66
00:07:28,150 --> 00:07:29,550
when you return ...

67
00:07:30,868 --> 00:07:33,787
you could go to France, to
study for the priesthood,

68
00:07:33,827 --> 00:07:35,186
Would that please you, Daniel?

69
00:07:37,344 --> 00:07:38,703
Yes, Father.

70
00:10:10,521 --> 00:10:12,360
These are our Fathers.

71
00:10:13,839 --> 00:10:16,397
They are soldiers of Heaven.

72
00:10:16,437 --> 00:10:18,956
They left their friends
and their country ...

73
00:10:18,995 --> 00:10:21,434
to show you
the way to Paradise.

74
00:10:31,387 --> 00:10:33,106
Chomin ...

75
00:10:33,146 --> 00:10:36,064
I entrust to you Father Laforgue.

76
00:10:38,862 --> 00:10:41,661
Who is journeying into the land
of Herots.

77
00:10:43,259 --> 00:10:45,298
Love and honor him.

78
00:10:45,338 --> 00:10:46,737
Guard him well.

79
00:11:39,622 --> 00:11:42,899
Look at him, dressed like
a savage Chieftan.

80
00:11:42,940 --> 00:11:44,618
We are not colonizing the Indians.

81
00:11:44,658 --> 00:11:47,337
- They are colonizing us.
- Not me, they are not.

82
00:11:47,376 --> 00:11:50,414
I am not becoming one
of those wild woods men.

83
00:11:50,454 --> 00:11:53,172
In one more year, I am going back to France.

84
00:11:53,213 --> 00:11:54,572
How are you?

85
00:11:54,612 --> 00:11:56,091
Are any of us?

86
00:11:56,170 --> 00:11:59,008
If winter doesn't kill us,
the Indians might.

87
00:11:59,048 --> 00:12:01,527
If they don't,
it could be the English.

88
00:12:01,567 --> 00:12:02,926
So keep your faith.

89
00:12:02,966 --> 00:12:05,524
I made that point
with you, God in mind.

90
00:15:43,777 --> 00:15:45,816
Good morning.

91
00:15:45,856 --> 00:15:47,455
You are?

92
00:15:47,495 --> 00:15:50,733
- Paul Laforgue.
- You have to serve my mass?

93
00:15:51,813 --> 00:15:53,851
I am the priest of New France.

94
00:15:55,130 --> 00:15:57,689
The savages did this to me.

95
00:15:57,728 --> 00:16:00,206
"Indians"? But why?

96
00:16:00,247 --> 00:16:01,766
They are uncivilized,

97
00:16:01,806 --> 00:16:03,804
just as the English
or the Germans were ...

98
00:16:03,844 --> 00:16:05,363
before we took our faith to them.

99
00:16:06,562 --> 00:16:08,761
In a few weeks ...

100
00:16:08,801 --> 00:16:10,520
I will be returning.

101
00:16:10,560 --> 00:16:12,079
Returning to New France?

102
00:16:12,159 --> 00:16:15,436
The savages live
in total darkness.

103
00:16:15,477 --> 00:16:17,355
We must convert them.

104
00:16:17,395 --> 00:16:20,673
What more glorious task
than that, Paul.

105
00:16:20,713 --> 00:16:22,991
What more glorious task.

106
00:18:23,151 --> 00:18:25,589
Black Robe, what are you doing?

107
00:18:26,868 --> 00:18:28,187
I'm making words.

108
00:18:29,187 --> 00:18:30,346
"Words"?

109
00:18:31,425 --> 00:18:32,944
You do not speak.

110
00:18:34,024 --> 00:18:35,302
I'll show you.

111
00:18:37,901 --> 00:18:40,339
- Tell me something.
- Tell What?

112
00:18:40,379 --> 00:18:42,658
Something I do not know.

113
00:18:46,135 --> 00:18:49,293
My woman's mother...

114
00:18:49,333 --> 00:18:51,572
died in snow last winter.

115
00:19:22,231 --> 00:19:25,269
"Last winter, Chomin's wife mother...

116
00:19:25,309 --> 00:19:27,028
died in the snow"."

117
00:19:44,696 --> 00:19:47,614
I have still other greater things
that I can teach you.

118
00:20:59,286 --> 00:21:00,885
Father ...

119
00:21:00,925 --> 00:21:03,163
I did not mean any disrespect ...

120
00:21:04,122 --> 00:21:06,162
but do you have any doubts?

121
00:21:07,440 --> 00:21:08,720
What about?

122
00:21:10,638 --> 00:21:12,517
Them ... these people.

123
00:21:13,996 --> 00:21:15,914
If we change them.

124
00:21:15,955 --> 00:21:19,392
If we do not change them,
How could they enter Heaven?

125
00:21:19,472 --> 00:21:22,350
Intelligence is not lacking
among these people.

126
00:21:23,950 --> 00:21:26,228
He speaks only
Algonquin, father.

127
00:21:50,931 --> 00:21:52,650
What is the meaning of that word?

128
00:21:54,369 --> 00:21:57,647
You love that tobacco,
more than you love us.

129
00:21:57,687 --> 00:22:00,085
What can I do? I know that.

130
00:22:00,125 --> 00:22:01,804
He will ask and ask until they have ...

131
00:22:01,844 --> 00:22:04,003
everything, we brought with us.

132
00:22:04,042 --> 00:22:07,240
Father, they do not understand.

133
00:22:07,240 --> 00:22:09,559
They share everything
without question.

134
00:22:09,599 --> 00:22:11,437
Then they should question.

135
00:22:11,478 --> 00:22:13,836
They plan nothing,
but think only of the moment:

136
00:22:13,875 --> 00:22:15,275
hunting foods...

137
00:22:21,191 --> 00:22:23,069
Go and get the tobacco, Daniel.

138
00:24:17,873 --> 00:24:21,990
They say it's soothing
once you become used to it.

139
00:24:35,141 --> 00:24:37,340
That may take some time.

140
00:24:59,605 --> 00:25:01,683
They don't seem to be very happy father.

141
00:25:02,722 --> 00:25:05,760
They should be.
I have told them the truth.

142
00:26:06,879 --> 00:26:10,317
So charming,
Mademoiselle Lafontaine.

143
00:26:10,357 --> 00:26:12,196
And so attractive.

144
00:26:13,955 --> 00:26:15,473
So modest.

145
00:26:17,353 --> 00:26:19,111
She is from a very good family.

146
00:33:59,402 --> 00:34:01,921
What do you think they
are talking about, father?

147
00:34:01,960 --> 00:34:03,400
I don't know.

148
00:34:03,439 --> 00:34:04,559
Of us?

149
00:36:46,850 --> 00:36:48,929
Mother, what are you doing?

150
00:36:50,487 --> 00:36:51,967
Praying to Saint Joan.

151
00:36:53,486 --> 00:36:57,403
Perhaps God has chosen you
as he chose her.

152
00:36:57,443 --> 00:37:00,681
You must not compare me
to a saint, Mother.

153
00:37:00,721 --> 00:37:04,439
"In New France, you will remember me
in your prayers?

154
00:37:04,478 --> 00:37:05,837
I always do..

155
00:37:07,396 --> 00:37:09,475
God will hear your prayer.

156
00:37:09,515 --> 00:37:11,953
He has chosen you
to die for Him.

157
00:37:14,152 --> 00:37:16,670
I will never see you again.

158
00:38:52,366 --> 00:38:53,845
I'm afraid, Lord.

159
00:38:55,564 --> 00:38:57,682
I don't welcome death ...

160
00:38:57,722 --> 00:38:59,641
as a holy person should.

161
00:40:48,008 --> 00:40:49,127
Daniel?

162
00:40:56,962 --> 00:40:59,401
I too have committed
the sin of the flesh.

163
00:40:59,441 --> 00:41:01,199
Do you, father?

164
00:41:01,199 --> 00:41:03,438
"In France, long ago"?

165
00:41:03,438 --> 00:41:04,877
No, here.

166
00:41:06,036 --> 00:41:07,955
Sin of intent.

167
00:41:07,995 --> 00:41:09,474
Here.

168
00:41:09,514 --> 00:41:11,073
I've lusted after her.

169
00:41:12,552 --> 00:41:14,310
Kneel with me ...

170
00:41:14,310 --> 00:41:16,310
and say an act of contrition.

171
00:41:16,349 --> 00:41:20,626
Father, Life is not so simple
for the rest of us.

172
00:41:20,666 --> 00:41:22,265
I am not a Jesuit.

173
00:41:22,305 --> 00:41:24,344
You said you wanted to
serve God.

174
00:41:24,384 --> 00:41:26,143
Yes, Father, but I ...

175
00:41:31,459 --> 00:41:33,937
I'm afraid of this country.

176
00:41:33,978 --> 00:41:36,376
The Devil rules here.

177
00:41:36,416 --> 00:41:39,414
Controls the hearts
and minds ...

178
00:41:39,454 --> 00:41:41,172
of these poor people.

179
00:41:41,213 --> 00:41:46,209
But they are true Christians,
they live for each other.

180
00:41:46,249 --> 00:41:48,847
They forgive things,
we would not forgive.

181
00:41:48,847 --> 00:41:52,045
The Devil makes them resist
the truth of our teachings.

182
00:41:52,045 --> 00:41:54,124
Why should they believe them?

183
00:41:54,163 --> 00:41:56,362
They have an after world of
their own.

184
00:41:56,362 --> 00:41:58,441
They have no concept of one.

185
00:41:58,481 --> 00:42:01,159
Annuka has told me ...

186
00:42:01,199 --> 00:42:05,116
they believe that, in the forest at night,
the dead can see.

187
00:42:05,156 --> 00:42:09,074
Souls of men hunt
the souls of animals.

188
00:42:09,113 --> 00:42:12,991
"Is that what she told you?"
It's childish, Daniel.

189
00:42:13,031 --> 00:42:15,669
Is it harder to believe
in a paradise ...

190
00:42:15,709 --> 00:42:17,708
where we all sit on the clouds with a good God?

191
00:43:56,322 --> 00:43:57,601
Black Robe.

192
00:45:21,345 --> 00:45:23,743
Oh, God of mercy...

193
00:45:23,743 --> 00:45:26,741
Please, Bless
this innocent child.

194
00:46:21,905 --> 00:46:24,063
Where are my paddlers?
You promised.

195
00:46:32,297 --> 00:46:36,614
You go with your own mission
to the Hurons, alone, Black Robe.

196
00:46:36,654 --> 00:46:39,892
Ask your Jesus, to help you,
Black Robe.

197
00:52:24,301 --> 00:52:25,420
Lord ...

198
00:52:26,660 --> 00:52:30,177
If it be Thy wish that
I suffer greater tribulations

199
00:52:30,217 --> 00:52:32,735
in the days ahead ...

200
00:52:32,775 --> 00:52:34,734
I welcome it.

201
00:52:35,933 --> 00:52:39,291
Thou has given me this cross
for thy honor ...

202
00:52:39,331 --> 00:52:42,488
for the salvation
of these poor barbarians.

203
00:52:43,928 --> 00:52:45,327
I thank Thee.

204
00:59:00,475 --> 00:59:02,394
Father, Forgive me.

205
00:59:02,434 --> 00:59:03,593
What for?

206
00:59:05,112 --> 00:59:06,871
For the things I said to you.

207
00:59:08,310 --> 00:59:10,069
For leaving you.

208
00:59:10,069 --> 00:59:13,387
God is with us.
He is the one who forgives us.

209
01:02:59,195 --> 01:03:00,154
Sing.

210
01:03:01,114 --> 01:03:02,193
Sing!

211
01:04:55,038 --> 01:04:56,996
The arrowhead is still there.

212
01:04:57,036 --> 01:04:58,914
If we try
to remove it, you will die.

213
01:05:01,792 --> 01:05:04,151
You want to be one of us?

214
01:05:04,191 --> 01:05:06,189
What do you think now?

215
01:05:06,230 --> 01:05:09,627
The Iroquois are not men.
They are animals.

216
01:05:09,668 --> 01:05:13,345
They're the same as us
or Hurons.

217
01:05:13,385 --> 01:05:17,142
If they showed pity, others
will say that they are weak.

218
01:05:19,981 --> 01:05:21,460
Tomorrow ...

219
01:05:21,500 --> 01:05:22,979
do not cry out.

220
01:05:24,458 --> 01:05:26,616
If we do cry out, "Will they stop?"

221
01:05:26,656 --> 01:05:28,934
No, They would not stop.

222
01:05:29,894 --> 01:05:32,812
But if you cry out when you die,

223
01:05:32,852 --> 01:05:34,810
they will have your spirit.

224
01:05:34,850 --> 01:05:37,329
When I die, Chomin,
I will go to Paradise.

225
01:05:37,369 --> 01:05:39,927
Let me baptize you
so you will go there also.

226
01:05:39,967 --> 01:05:42,326
Why would I go to your paradise?

227
01:05:42,365 --> 01:05:44,204
Are my people there?

228
01:05:44,204 --> 01:05:48,521
"My wife? My son? There's
only Black Robes there".

229
01:11:08,027 --> 01:11:09,985
We go downstream to my people.

230
01:11:10,025 --> 01:11:12,104
No, We go upstream
to the Huron mission.

231
01:11:14,382 --> 01:11:16,901
You're so weak, The Iroquois
could easily catch you.

232
01:11:16,941 --> 01:11:19,659
And they would not expect you
to travel away from your people.

233
01:11:22,057 --> 01:11:24,376
You're not so stupid, Black Robe.

234
01:12:42,523 --> 01:12:45,401
Lord, I beg you ...

235
01:12:45,442 --> 01:12:47,880
show your mercy
to these savage people ...

236
01:12:47,920 --> 01:12:50,837
who will never look upon Your face
in Paradise.

237
01:15:13,142 --> 01:15:14,621
Tell me, Black Robe ...

238
01:15:15,581 --> 01:15:17,420
What does your dreams see now?

239
01:15:18,459 --> 01:15:20,298
I'm too weary for dreams.

240
01:15:21,417 --> 01:15:23,495
But you must.

241
01:15:23,535 --> 01:15:27,054
If you do not ... how do you see
the way that lies ahead?

242
01:15:28,452 --> 01:15:30,411
I trust in God.

243
01:15:30,451 --> 01:15:33,009
He will guide me ...
all the way to Paradise.

244
01:15:34,169 --> 01:15:36,806
You have not seen this paradise.

245
01:15:37,885 --> 01:15:39,925
No man should welcome death.

246
01:15:40,923 --> 01:15:43,203
This world is a cruel place ...

247
01:15:44,282 --> 01:15:46,840
but it has the sunlight.

248
01:15:49,239 --> 01:15:51,476
I'm sorry that I should leave now.

249
01:16:59,031 --> 01:17:01,949
Chomin, do you hear me?
My God loves you.

250
01:17:10,024 --> 01:17:13,302
If you accept His love,
he will admit you to Paradise.

251
01:20:36,365 --> 01:20:37,965
"I'll go with you, Father".

252
01:20:40,083 --> 01:20:41,841
You must stay with her.

253
01:20:41,882 --> 01:20:43,881
She has lost everything
because of us.

254
01:20:44,839 --> 01:20:46,279
She needs you more than I do.

255
01:20:50,317 --> 01:20:52,834
We will do what she ask.

256
01:20:52,875 --> 01:20:56,032
What can we say to people who thinks
that dreams are the real world...

257
01:20:56,073 --> 01:20:59,030
and this one is an illusion?

258
01:21:00,389 --> 01:21:01,908
Perhaps they're right.

259
01:21:03,748 --> 01:21:05,786
Good bye, Father Laforgue.

260
01:21:05,825 --> 01:21:07,585
No Farewells,

261
01:21:07,624 --> 01:21:09,064
not in this land

262
01:21:09,103 --> 01:21:11,861
and no greetings, no names.

263
01:21:11,902 --> 01:21:13,780
The forests speak.

264
01:21:13,820 --> 01:21:16,219
The dead talk at night.

265
01:21:16,259 --> 01:21:17,897
God bless you both.

266
01:23:37,884 --> 01:23:38,963
Father?

267
01:23:52,834 --> 01:23:54,193
Who are you?

268
01:23:55,152 --> 01:23:56,951
I'm Father Laforgue.

269
01:23:56,991 --> 01:23:58,230
How many are with you?

270
01:23:59,189 --> 01:24:00,388
I am alone.

271
01:24:05,545 --> 01:24:09,342
Some months ago
that fever struck this village.

272
01:24:09,383 --> 01:24:11,900
The Indians thought that
we brought it to punish ...

273
01:24:11,941 --> 01:24:13,699
those who would not accept our faith.

274
01:24:14,659 --> 01:24:16,697
Many died.

275
01:24:16,737 --> 01:24:20,735
One man who lost his child
killed father Duval.

276
01:24:21,654 --> 01:24:23,173
What are they all doing now?

277
01:24:24,412 --> 01:24:27,211
Who knows
what the savages will do?

278
01:24:27,250 --> 01:24:29,489
Who knows what they think?

279
01:24:29,529 --> 01:24:34,845
Even after 20 years,
I still do not understand them.

280
01:24:34,886 --> 01:24:37,364
They would probably torture
and kill us both.

281
01:24:39,762 --> 01:24:41,640
But what about those converts?

282
01:24:41,681 --> 01:24:43,200
We must have some influence.

283
01:24:43,240 --> 01:24:47,277
Converts?
Perhaps, There are no converts.

284
01:24:47,317 --> 01:24:51,434
Our only hope
is that ... some believe ...

285
01:24:51,434 --> 01:24:54,112
that baptism will
cure their fever.

286
01:24:55,272 --> 01:24:57,710
If they ask for baptism,

287
01:24:57,750 --> 01:25:00,269
we must have a great
public ceremony at once.

288
01:25:01,827 --> 01:25:02,906
Father Jerome ...

289
01:25:03,986 --> 01:25:05,624
Shouldn't we ...?

290
01:25:05,665 --> 01:25:07,743
I mean ...

291
01:25:07,784 --> 01:25:10,342
Should they not understand our faith ...

292
01:25:10,382 --> 01:25:11,741
before accepting it?

293
01:25:11,781 --> 01:25:13,500
Understand?

294
01:25:13,540 --> 01:25:15,818
But they are in danger of death...

295
01:25:15,858 --> 01:25:19,176
and we are offering them
a place in Paradise.

296
01:27:35,085 --> 01:27:38,362
Father death may be near.

297
01:27:38,362 --> 01:27:40,241
Will you hear my confession?

298
01:27:41,880 --> 01:27:43,839
If you will hear mine.

299
01:27:51,954 --> 01:27:54,311
Bless me, Father,
for I have sinned.

300
01:28:28,728 --> 01:28:30,367
Lord ...

301
01:28:30,407 --> 01:28:33,405
Why is Father Jerome
is with you in Heaven ...

302
01:28:33,445 --> 01:28:36,484
while Chomin lies
forever in utter darkness?

303
01:28:38,362 --> 01:28:39,802
Help me.

304
01:29:10,141 --> 01:29:12,019
Demon!
Why are you here?

305
01:29:13,019 --> 01:29:14,897
I am not a Demon.

306
01:29:14,937 --> 01:29:17,336
I take the place of Father Jérome.

307
01:29:17,376 --> 01:29:20,373
- Is he dead?
- Yes.

308
01:29:20,414 --> 01:29:22,093
How long will you stay?

309
01:29:23,452 --> 01:29:24,571
All of my life.

310
01:29:25,650 --> 01:29:28,169
If we take the
water sorcery ...

311
01:29:28,208 --> 01:29:29,567
We will not be sick?

312
01:29:30,767 --> 01:29:33,285
Baptism does not cure you.

313
01:29:33,325 --> 01:29:35,844
The other Black Robe said so.

314
01:29:35,883 --> 01:29:39,681
He meant only that
we must ask the help of Jesus.

315
01:29:39,721 --> 01:29:42,079
Perhaps, He will answer your prayers.

316
01:30:07,742 --> 01:30:09,621
Many want to kill you,
Black Robe.

317
01:30:11,580 --> 01:30:12,699
I know.

318
01:30:21,932 --> 01:30:23,812
A Demon cannot feel pain.

319
01:30:25,131 --> 01:30:26,370
Are you a Man?

320
01:30:28,648 --> 01:30:30,407
Yes.

321
01:30:30,407 --> 01:30:32,365
You must help us, Black Robe.

322
01:30:33,325 --> 01:30:35,164
Do you love us?

323
01:30:59,867 --> 01:31:03,065
- Yes
- Then, Baptize us.

324
01:31:09,981 --> 01:31:11,460
Spare them.

325
01:31:11,500 --> 01:31:13,298
Spare them, Oh Lord.

