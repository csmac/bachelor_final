﻿1
00:05:13,628 --> 00:05:15,254
François! There he is.

2
00:05:15,422 --> 00:05:16,839
Mister Roullot.

3
00:05:18,967 --> 00:05:20,468
Have a seat.

4
00:05:23,221 --> 00:05:25,139
Tell me. How are you doing?

5
00:05:25,557 --> 00:05:27,141
Come on, sit! Sit!

6
00:05:30,687 --> 00:05:32,104
No thank you. I’m OK.

7
00:05:32,272 --> 00:05:33,439
Thank you very much.

8
00:05:35,025 --> 00:05:35,691
I'm fine!

9
00:05:36,068 --> 00:05:38,235
So, what have you got for me?

10
00:05:40,155 --> 00:05:44,116
Just a few items regarding protocol
upon your return to France.

11
00:05:44,284 --> 00:05:49,246
We've decided that there
should be someone assigned 24/7 to...

12
00:05:49,623 --> 00:05:53,000
not supervise but... someone
in the background on continuous rotation.

13
00:05:53,627 --> 00:05:57,254
We've installed metal detectors
at your offices. In your homes...

14
00:05:57,422 --> 00:05:59,590
Both offices and all three homes.
Same goes for Simone.

15
00:06:00,425 --> 00:06:04,136
And from now on... this might be
a little more delicate but...

16
00:06:04,304 --> 00:06:08,516
Everyone will have to carry
an accreditation issued by my office.

17
00:06:09,267 --> 00:06:11,394
It’s a bit much, isn't it?

18
00:06:12,062 --> 00:06:13,187
Don't you think?

19
00:06:14,481 --> 00:06:18,734
Given the circumstances,
we think it's appropriate.

20
00:06:19,236 --> 00:06:21,862
You should know there are rumors.

21
00:06:25,951 --> 00:06:29,662
Rumors that have already spread
quite a bit

22
00:06:29,830 --> 00:06:31,497
within the United States Secret Service.

23
00:06:34,584 --> 00:06:36,502
I shouldn't be the one to tell you.

24
00:07:00,569 --> 00:07:03,404
Wait, look at this. There you are!

25
00:07:05,449 --> 00:07:09,118
Let me introduce you to Anna.
This is François.

26
00:07:14,833 --> 00:07:18,169
No, you're too kind. I'm OK.

27
00:07:19,212 --> 00:07:22,006
I didn't come for that.
I’m on duty, thanks.

28
00:07:39,441 --> 00:07:43,736
My dear sir,
thank your for your attention.

29
00:07:44,279 --> 00:07:46,405
And on that note,
I’m going to have to go.

30
00:07:52,996 --> 00:07:54,413
Just sit for a second.

31
00:07:54,581 --> 00:07:56,874
Unfortunately, we have to go.

32
00:07:57,292 --> 00:07:58,667
Thank you for your attention.

33
00:07:58,835 --> 00:08:00,753
Alright, no big deal.

34
00:08:03,632 --> 00:08:06,050
Careful out there. Long live France!

35
00:11:19,160 --> 00:11:21,912
- Do you need help with your bags?
- No thank you.

36
00:11:24,499 --> 00:11:26,166
- How have you been?
- Very well.

37
00:11:26,710 --> 00:11:27,626
You're in great shape.

38
00:11:27,794 --> 00:11:28,585
Well, thank you.

39
00:11:28,837 --> 00:11:29,962
Are you still dancing?

40
00:11:30,130 --> 00:11:31,505
Yes, of course.

41
00:11:32,048 --> 00:11:33,090
Did you have a nice trip?

42
00:11:33,258 --> 00:11:34,258
Very pleasant.

43
00:11:34,426 --> 00:11:35,551
Everything went well?

44
00:11:35,719 --> 00:11:36,510
Very well.

45
00:11:37,137 --> 00:11:38,762
Where are you going on vacation?

46
00:11:39,305 --> 00:11:41,932
I’m off to Greece soon, apparently.

47
00:11:43,017 --> 00:11:44,059
Have you ever been?

48
00:11:44,227 --> 00:11:45,853
Watch out for those Greeks!

49
00:12:06,458 --> 00:12:07,833
We're almost there.

50
00:12:08,835 --> 00:12:11,086
So you're taking singing classes?

51
00:12:11,337 --> 00:12:14,923
- You know everything.
- Yes.

52
00:12:14,966 --> 00:12:16,467
This is your room.

53
00:33:50,052 --> 00:33:51,594
What are you having, sweetie?

54
00:33:51,887 --> 00:33:53,554
Something light, maybe a salad.

55
00:33:54,389 --> 00:33:55,890
You know what, I’ll have...

56
00:33:57,100 --> 00:33:58,100
I’ll have...

57
00:34:45,524 --> 00:34:47,358
When you work in a bank

58
00:34:47,692 --> 00:34:50,528
people tend to listen
to what you have to say.

59
00:34:59,788 --> 00:35:02,081
How's the fucking? Any good?

60
00:35:04,334 --> 00:35:05,209
What?

61
00:35:18,890 --> 00:35:20,474
I’m not allowed to know.

62
00:35:21,101 --> 00:35:24,562
Come on, it's nothing.
Fucking is natural. Are you at least-

63
00:35:24,729 --> 00:35:26,230
Enough!

64
00:35:27,023 --> 00:35:29,775
I, for one,
fucked all night and it was great.

65
00:35:29,943 --> 00:35:32,987
It’s so important
to feel good with a man.

66
00:35:33,321 --> 00:35:35,406
I know I’m not supposed
to say things like-

67
00:35:35,574 --> 00:35:38,450
OK, Dad, enough. We got it.

68
00:42:18,268 --> 00:42:21,353
I left my blackberry at the hotel.

69
00:42:21,604 --> 00:42:23,355
Security's going to run it over to me.

70
00:42:23,523 --> 00:42:25,399
Would you mind...

71
00:42:25,567 --> 00:42:27,693
I’ll warn the gate manager.

72
00:42:27,986 --> 00:42:30,654
I’m heading back to Paris.
I don't want to forget it.

73
00:42:30,822 --> 00:42:33,073
Worst case,
we can have it sent insured mail.

74
00:42:33,241 --> 00:42:34,491
No, I don't want it mailed.

75
00:42:34,659 --> 00:42:35,826
They're going to run it over to me.

76
00:42:35,994 --> 00:42:38,161
Then let's go see the gate manager.

77
00:42:48,715 --> 00:42:51,300
Let's go talk to the head of security.

78
00:42:58,808 --> 00:42:59,683
Sorry.

79
00:42:59,851 --> 00:43:00,726
Not a problem, sir.

80
00:43:00,893 --> 00:43:02,436
I'm sorry for the trouble.

81
00:43:02,979 --> 00:43:04,187
Everything taken care of?
Are we good?

82
00:43:04,355 --> 00:43:05,564
Yes, you may board.

83
00:43:05,732 --> 00:43:08,108
Thank you very much. Thank you, sir.

84
00:43:22,749 --> 00:43:23,874
Have a pleasant trip, Mr. Devereaux.

85
00:43:24,042 --> 00:43:25,000
Thank you very much.

86
00:46:06,913 --> 00:46:08,371
It’s in a different passport.

87
00:46:09,040 --> 00:46:10,499
In my other passport.

88
00:51:20,392 --> 00:51:26,189
If I could only find the words to express
our gratitude to Simone.

89
00:51:27,233 --> 00:51:28,733
For her devotion.

90
00:51:29,777 --> 00:51:33,279
For her love for the State of Israel.

91
00:51:33,739 --> 00:51:36,366
You are too kind. I'm only-

92
00:51:36,534 --> 00:51:40,745
Not to mention the charities
she has founded single-handedly,

93
00:51:41,831 --> 00:51:44,165
worldwide...

94
00:51:46,252 --> 00:51:49,462
And the justice she has restored

95
00:51:49,630 --> 00:51:51,965
to those without a voice.

96
01:16:17,388 --> 01:16:18,555
Here we are.

97
01:16:21,058 --> 01:16:22,267
This is the house.

98
01:16:27,273 --> 01:16:30,275
It’s a little dark, but I’m sure it'll do.

99
01:16:32,320 --> 01:16:35,780
This is what $60,000 gets you.

100
01:16:41,746 --> 01:16:43,038
It’s big.

101
01:20:05,950 --> 01:20:07,826
I need something to drink.

102
01:25:54,715 --> 01:25:55,631
What's wrong?

103
01:25:57,050 --> 01:25:58,342
Mr. Devereaux?

104
01:25:58,719 --> 01:26:00,720
Yeah. Can you introduce me?

105
01:26:00,888 --> 01:26:02,096
Absolutely.

106
01:26:08,854 --> 01:26:10,438
Hi, Mr. Devereaux.

107
01:26:12,107 --> 01:26:12,982
How are you, Michel?

108
01:26:13,150 --> 01:26:14,525
Just fine. How are you?

109
01:26:14,985 --> 01:26:17,195
Allow me to introduce my little girl.

110
01:26:18,113 --> 01:26:18,821
What a pleasure...

111
01:26:18,989 --> 01:26:19,864
Marie.

112
01:26:21,074 --> 01:26:22,533
What is it that Marie does?

113
01:26:22,701 --> 01:26:23,784
I'm in law school.

114
01:26:24,328 --> 01:26:26,454
- What year?
- Third.

115
01:26:28,332 --> 01:26:29,624
That's fantastic.

116
01:26:29,791 --> 01:26:31,959
Are you enjoying the exhibition?

117
01:26:32,127 --> 01:26:33,419
Yeah, it's not bad.

118
01:26:34,046 --> 01:26:37,006
I like that it's about Africa.
It’s something I'm interested in.

119
01:26:37,174 --> 01:26:38,633
And what do you want to do?

120
01:26:38,800 --> 01:26:40,176
I want to be a lawyer.

121
01:26:44,181 --> 01:26:47,934
Are you proud of your...
These are young artists from Sarcelles?

122
01:26:48,227 --> 01:26:50,686
Yeah, you gotta...

123
01:26:50,854 --> 01:26:51,729
Sorry...

124
01:26:53,941 --> 01:26:55,441
One needs to...

125
01:26:55,609 --> 01:26:57,360
Are you also into photography?

126
01:26:57,527 --> 01:26:58,611
I would love to...

127
01:26:58,779 --> 01:27:02,823
But law school keeps me pretty busy.

128
01:27:04,451 --> 01:27:05,534
How old are you?

129
01:27:05,702 --> 01:27:07,411
Sorry, I'm a little intimidated...

130
01:27:09,122 --> 01:27:10,373
You're very beautiful.

131
01:27:10,540 --> 01:27:11,415
Thank you.

132
01:27:15,128 --> 01:27:17,046
Maybe we can meet up later?

133
01:27:32,729 --> 01:27:34,272
What do you think of him?

134
01:27:36,608 --> 01:27:39,068
Yeah. He's interesting.

135
01:27:45,993 --> 01:27:50,997
What was it you wanted to do?
Where did you want to work?

136
01:27:51,748 --> 01:27:54,375
I want to work
for the lnternational Criminal Court.

137
01:27:55,252 --> 01:27:59,672
You'll be confronting real murderers.

138
01:28:00,048 --> 01:28:01,173
Yes.

139
01:28:02,009 --> 01:28:02,967
I’ll prosecute them.

140
01:28:03,135 --> 01:28:04,302
What do you like about it?

141
01:28:05,721 --> 01:28:06,762
Justice?

142
01:28:06,930 --> 01:28:08,222
Yes, justice.

143
01:28:11,393 --> 01:28:13,436
So who are you seeing,
aside from your wife?

144
01:28:14,354 --> 01:28:17,064
Hardly anyone.
My work keeps me busy.

145
01:28:17,816 --> 01:28:18,858
I don't believe you.

146
01:28:19,026 --> 01:28:22,361
Yes! People credit me
with more than my share of affairs.

147
01:28:23,864 --> 01:28:25,656
But I have nothing against that.

148
01:28:25,824 --> 01:28:29,368
Womanizers are a turn-on, aren't they?

149
01:28:30,329 --> 01:28:31,537
And intelligent men.

150
01:28:32,831 --> 01:28:36,584
What turns you on more?
Womanizers or intelligent men?

151
01:28:36,752 --> 01:28:38,919
Brilliant men like you.

152
01:29:14,373 --> 01:29:17,083
Do I look like the girls
you usually sleep with?

153
01:29:18,251 --> 01:29:19,126
No.

154
01:29:20,337 --> 01:29:24,799
They don't have your youth,
your beauty, your intelligence.

155
01:29:25,967 --> 01:29:28,219
You know, I used to really like...

156
01:29:29,888 --> 01:29:32,598
How can I put it?
I liked sluts. Whores.

157
01:29:34,643 --> 01:29:36,602
I can't shake the reputation.

158
01:29:37,104 --> 01:29:40,898
But with you...
You know what I mean.

159
01:29:41,191 --> 01:29:43,484
I was at boarding school, with the nuns.

160
01:29:44,486 --> 01:29:46,487
Now I’m even more excited.

161
01:29:48,115 --> 01:29:50,032
Come here, cutie pie.

162
01:29:55,163 --> 01:29:57,998
Oh that's good.
Did the nuns teach you that?

163
01:30:00,460 --> 01:30:02,878
They're brilliant. They really are.

164
01:30:03,046 --> 01:30:05,339
They were great.
We studied this all day long.

165
01:30:05,882 --> 01:30:10,261
There were lots of erotic books
in the 17th and 18th centuries

166
01:30:10,429 --> 01:30:11,554
Written by nuns.

167
01:30:11,763 --> 01:30:12,596
Written by nuns?

168
01:30:12,848 --> 01:30:13,639
Of course!

169
01:30:13,807 --> 01:30:15,307
They were crazy about sex.

170
01:30:15,475 --> 01:30:19,728
Most nuns joined the convent

171
01:30:19,896 --> 01:30:23,566
Because their families sent them off.

172
01:30:25,652 --> 01:30:28,154
Because they had committed adultery.

173
01:30:31,992 --> 01:30:34,201
They're going to send you
to the convent too.

174
01:30:37,330 --> 01:30:39,331
You're so beautiful, my darling.

175
01:35:03,513 --> 01:35:04,680
A very good year!

176
01:35:17,110 --> 01:35:18,736
Thank you, Mr. Devereaux.

177
01:35:18,903 --> 01:35:21,822
A promise is a promise.

178
01:35:21,990 --> 01:35:23,782
And I promised you this interview.

179
01:35:24,951 --> 01:35:28,203
As long as things don't get too personal.

180
01:35:28,371 --> 01:35:29,747
Is this your office?

181
01:35:29,914 --> 01:35:32,458
Yes, but we'll go over here.

182
01:35:32,959 --> 01:35:34,418
You don't want to do it in there?

183
01:35:34,586 --> 01:35:36,462
No, a lot of people come through here.

184
01:35:37,422 --> 01:35:39,173
OK, as you wish.

185
01:35:39,340 --> 01:35:43,010
It’s very kind of you
to grant me this interview.

186
01:35:43,303 --> 01:35:45,345
Here we are, take a seat over there.

187
01:35:45,513 --> 01:35:47,473
This is where I rest when I’m working,

188
01:35:47,640 --> 01:35:50,100
but I also use it for interviews.

189
01:35:53,438 --> 01:35:55,189
What would you like to know?

190
01:35:59,903 --> 01:36:02,529
By the way, I read your book.
It’s very good.

191
01:36:02,697 --> 01:36:04,573
- Is that true?
- Quite the wordsmith.

192
01:36:04,741 --> 01:36:07,743
You read it? That's so kind of you!

193
01:36:08,119 --> 01:36:12,039
That's really... nice.
Really, I'm pleased.

194
01:36:12,457 --> 01:36:17,795
It was so nice of you to agree to this.
I don't want to take up too much of your time.

195
01:36:18,505 --> 01:36:25,469
Can you tell me how you lost
the Jean-Claude Méry tape?

196
01:36:26,179 --> 01:36:27,346
That's why I’m here.

197
01:36:27,889 --> 01:36:29,014
I didn't watch it.

198
01:36:29,474 --> 01:36:31,975
- You never saw it?
- No, I didn't watch the tape.

199
01:36:32,143 --> 01:36:37,773
I don't understand.
You told me you watched it.

200
01:36:37,941 --> 01:36:41,109
- No, I didn't see it.
- But if you didn't watch it then-

201
01:36:43,988 --> 01:36:45,572
You are quite beautiful.

202
01:36:46,115 --> 01:36:48,700
You're beautiful. You know-

203
01:36:48,868 --> 01:36:51,370
Look, if you never watched the-

204
01:36:51,538 --> 01:36:53,330
Hold on.
I know your mother well.

205
01:36:53,498 --> 01:36:54,122
Thank you for your-

206
01:36:54,290 --> 01:36:57,084
You know, I know your mother well.

207
01:36:58,378 --> 01:36:59,586
Very well, actually.

208
01:36:59,754 --> 01:37:02,214
She told me a lot about you
when you were a child.

209
01:37:02,382 --> 01:37:03,674
You are very gifted.

210
01:37:04,259 --> 01:37:06,134
You had a wonderful education.

211
01:37:07,303 --> 01:37:09,680
Why are you talking about my mother?

212
01:37:09,848 --> 01:37:11,890
Because I know her very well. Here...

213
01:37:12,058 --> 01:37:13,392
That's enough.

214
01:39:12,679 --> 01:39:13,971
Sorry.

215
01:39:15,890 --> 01:39:16,974
It will be OK.

216
01:39:18,142 --> 01:39:20,185
It’s not easy but it will be OK.

217
01:39:22,897 --> 01:39:25,607
What about Josh?
How's he dealing with all of this?

218
01:39:25,775 --> 01:39:28,193
Josh is OK,
but his parents are pretty upset.

219
01:39:29,570 --> 01:39:32,239
Have his parents
ever really been happy?

220
01:39:32,699 --> 01:39:34,282
I have no idea.

221
01:39:35,201 --> 01:39:38,161
But I do know they were happy
their son was dating the daughter

222
01:39:38,329 --> 01:39:40,664
of the head of a major
international institution.

223
01:39:41,332 --> 01:39:45,002
Maybe they should just be happy
that he was marrying you.

224
01:39:46,879 --> 01:39:50,507
I would marry you even if your father
was a scumbag.

225
01:39:53,011 --> 01:39:55,303
That's very nice,
but you know things don't work like that.

226
01:39:58,057 --> 01:39:59,141
And Simone?

227
01:40:00,560 --> 01:40:01,268
Very angry.

228
01:40:03,646 --> 01:40:05,647
She never had much of a sense of humor.

229
01:40:12,196 --> 01:40:15,032
I shouldn't find any of this funny.

230
01:40:15,950 --> 01:40:17,075
You don't find it funny?

231
01:40:17,243 --> 01:40:18,910
She should accept you
the way Josh accepts me.

232
01:40:19,078 --> 01:40:20,412
She knew what she was getting into.

233
01:40:26,711 --> 01:40:27,919
What are you going to do?

234
01:40:33,509 --> 01:40:36,344
You mean when they ask:
"Blindfolded or not?"

235
01:40:36,512 --> 01:40:37,846
Dad, stop it!

236
01:40:43,352 --> 01:40:45,687
I wish I could have helped you to stop.

237
01:40:46,981 --> 01:40:48,231
I didn't want to.

238
01:40:49,609 --> 01:40:50,692
Correction.

239
01:40:51,527 --> 01:40:53,570
I don't want to.

240
01:40:58,284 --> 01:41:00,077
They can all go fuck themselves!

241
01:41:50,628 --> 01:41:52,671
I think it's a little bit my fault.

242
01:44:23,447 --> 01:44:24,990
It’s my fault.

243
01:44:26,492 --> 01:44:27,951
Just like everybody else...

244
01:44:33,082 --> 01:44:36,668
Since childhood, I’ve been brainwashed.

245
01:44:37,044 --> 01:44:40,338
By my parents, my teachers...

246
01:44:41,674 --> 01:44:45,218
My teachers, my superiors at work...

247
01:44:51,892 --> 01:44:56,313
I’m lucky, I'm not a Christian.

248
01:44:58,190 --> 01:44:59,858
But I'd like to say this:

249
01:45:00,651 --> 01:45:06,364
When I die,
I will kiss God's ass forever.

250
01:45:12,788 --> 01:45:14,414
I found my God...

251
01:45:16,417 --> 01:45:17,292
You.

252
01:45:43,402 --> 01:45:44,861
My first God?

253
01:45:47,948 --> 01:45:51,659
I didn't find it in a church,
but in a classroom.

254
01:45:53,579 --> 01:45:55,080
It was idealism.

255
01:45:55,664 --> 01:45:57,332
What a magnificent God!

256
01:45:58,542 --> 01:46:00,210
To believe everything would be OK.

257
01:46:03,172 --> 01:46:06,216
I was in the temple that is university.

258
01:46:06,717 --> 01:46:08,218
First as a student,

259
01:46:08,677 --> 01:46:10,220
then as a professor.

260
01:46:10,930 --> 01:46:15,100
And I allowed myself
to be wrapped in that hallowed light.

261
01:46:15,267 --> 01:46:16,351
Injustice?

262
01:46:17,728 --> 01:46:20,188
We had righted all the wrongs.

263
01:46:20,898 --> 01:46:22,232
World hunger?

264
01:46:23,192 --> 01:46:25,068
Everyone would eat until they were full.

265
01:46:25,486 --> 01:46:26,611
Poverty?

266
01:46:26,987 --> 01:46:31,699
A distant memory whose existence
would be difficult even to imagine.

267
01:46:34,620 --> 01:46:37,539
Wealth would be spread around.

268
01:46:37,998 --> 01:46:39,958
To each according to his needs.

269
01:46:41,168 --> 01:46:42,210
That's right.

270
01:46:46,257 --> 01:46:48,258
It was only when I arrived
at the World Bank

271
01:46:48,426 --> 01:46:50,552
that the enormity
of the world's pathos,

272
01:46:50,719 --> 01:46:53,972
the infinite suffering
inherent in human nature,

273
01:46:54,140 --> 01:46:57,642
revealed itself in all
its horrible manifestations.

274
01:47:00,604 --> 01:47:01,813
Slowly.

275
01:47:01,981 --> 01:47:03,440
One day at a time.

276
01:47:04,108 --> 01:47:04,858
No.

277
01:47:05,443 --> 01:47:07,318
One minute at a time.

278
01:47:08,070 --> 01:47:14,117
I understood the futility of struggling
against this insurmountable tsunami

279
01:47:16,954 --> 01:47:20,498
of troubles that we face.

280
01:47:21,959 --> 01:47:24,085
Things will not change.

281
01:47:24,795 --> 01:47:26,546
The hungry will die.

282
01:47:27,923 --> 01:47:28,882
The sick?

283
01:47:30,050 --> 01:47:31,551
They too will die.

284
01:47:32,344 --> 01:47:33,761
Poverty,

285
01:47:34,221 --> 01:47:36,222
It's good business.

286
01:47:37,349 --> 01:47:40,852
Wise men are comforted
by their limitations.

287
01:47:41,520 --> 01:47:44,105
I’m overwhelmed by this revelation.

288
01:47:44,356 --> 01:47:45,106
No.

289
01:47:46,066 --> 01:47:48,776
I can't return to that blissful youth.

290
01:47:48,944 --> 01:47:50,987
No redemption for me.

291
01:49:00,182 --> 01:49:01,724
What did the doctor say?

292
01:49:04,979 --> 01:49:07,188
He said it was all my mother's fault.

293
01:49:10,568 --> 01:49:11,401
Are you serious?

294
01:49:12,528 --> 01:49:14,571
He said he was having lunch
with his mother,

295
01:49:14,738 --> 01:49:17,365
and instead of asking her
to pass the butter, he told her

296
01:49:18,784 --> 01:49:21,953
"You fucking bitch, you ruined my life. "

297
01:50:10,044 --> 01:50:14,505
Another pedantic, narrow-minded
and shortsighted sophist,

298
01:50:14,673 --> 01:50:19,552
whose only goal is to convince me
to join the rest of herd.

299
01:50:21,096 --> 01:50:22,847
I won't fall in line.

300
01:50:23,849 --> 01:50:25,141
It pisses me off

301
01:50:25,309 --> 01:50:27,810
that one more time
I couldn't say no to you.

302
01:50:30,648 --> 01:50:32,482
Do you know what you've done,

303
01:50:33,150 --> 01:50:37,362
over these years,
little by little, bit by bit?

304
01:50:39,698 --> 01:50:43,368
You've succeeded
in making me hate myself.

305
01:50:44,370 --> 01:50:45,536
You see?

306
01:50:46,538 --> 01:50:47,664
You've succeeded.

307
01:52:10,497 --> 01:52:12,665
Damn it! Will you stop?

308
01:54:57,205 --> 01:54:58,789
Oh! I had forgotten your plans.

309
01:54:59,291 --> 01:55:01,042
Your plans of becoming president.

310
01:55:01,209 --> 01:55:04,754
Your plans for me to become
President of the Republic.

311
01:55:04,922 --> 01:55:07,131
That, I had forgotten about.

312
01:55:07,382 --> 01:55:10,176
It pisses you off
that I didn't end up in prison.

313
01:55:10,344 --> 01:55:12,136
No, it doesn't piss me off!

314
01:55:13,096 --> 01:55:17,808
To spend a little time
thinking about others.

315
01:55:17,976 --> 01:55:20,269
About yourself and what you've done.

316
01:55:21,146 --> 01:55:23,731
I think somehow
it would be good for you.

317
01:55:23,899 --> 01:55:26,067
A bit like military service,
which doesn't exist anymore.

318
01:55:26,234 --> 01:55:28,361
Sometimes it's good for a man.

319
01:55:28,528 --> 01:55:33,074
A little... a little...
Discipline, damn it!

320
01:55:33,241 --> 01:55:34,951
- I have discipline!
- You don't have discipline.

321
01:55:35,118 --> 01:55:37,411
It doesn't matter, you know my flaws.

322
01:55:37,579 --> 01:55:39,246
That's enough!

323
01:55:39,414 --> 01:55:40,373
I know.

324
01:55:40,540 --> 01:55:42,625
That's enough... I'm fed up.

325
01:55:43,710 --> 01:55:45,211
You're such a child!

326
01:55:45,379 --> 01:55:46,879
You're like a little child.

327
01:55:47,714 --> 01:55:49,632
I'm such a martyr!
Oh my God! Oh my God!

328
01:55:49,800 --> 01:55:51,384
Me, me, me! Shit!

329
01:55:51,635 --> 01:55:53,594
I’ve been your wife for twenty years.

330
01:55:54,972 --> 01:55:56,263
But you're very happy to be my wife.

331
01:55:56,431 --> 01:55:59,767
I was very happy to be your wife.
I wanted it for my life.

332
01:56:05,524 --> 01:56:09,110
Your plans!
Your plans for me to become president.

333
01:56:09,611 --> 01:56:11,404
You, the First Lady.

334
01:56:12,072 --> 01:56:13,239
Your plans!

335
01:56:27,212 --> 01:56:28,546
I’m not capable.

336
01:56:36,722 --> 01:56:39,348
I’m not capable of being president.

337
01:56:39,516 --> 01:56:40,224
You know that.

338
01:56:40,392 --> 01:56:41,767
I don't want to.

339
01:56:46,481 --> 01:56:53,904
I don't owe anybody anything.

340
01:57:11,798 --> 01:57:14,592
What are you going to do
when we get back to Paris?

341
01:57:17,012 --> 01:57:19,013
I’m the monster. I’m the monster.

342
01:57:35,906 --> 01:57:40,576
That's wonderful.

343
01:58:19,032 --> 01:58:22,034
That is a beautiful phrase. Truly.

344
02:05:13,488 --> 02:05:15,447
Subtitling: L.V.T. - Paris

