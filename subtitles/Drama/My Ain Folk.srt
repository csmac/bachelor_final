1
00:02:32,104 --> 00:02:34,062
Finish with it!

2
00:03:22,562 --> 00:03:24,562
Do you realise you've made me late for work?

3
00:03:26,271 --> 00:03:28,104
- Well?
- Well?

4
00:03:29,021 --> 00:03:31,062
I don't want any of your ruddy cheek!

5
00:03:31,146 --> 00:03:33,979
I want to see my gran!

6
00:03:51,104 --> 00:03:53,521
I'm not going!

7
00:03:53,604 --> 00:03:54,979
Run for it, Jamie.

8
00:03:55,062 --> 00:03:57,104
I'm not going! I'm not going!

9
00:03:59,937 --> 00:04:02,562
I'm not going! I'm not going! I'm not going!

10
00:04:02,646 --> 00:04:04,687
I'm not going!

11
00:04:06,104 --> 00:04:06,896
Da!

12
00:04:09,354 --> 00:04:11,146
Da!

13
00:04:29,396 --> 00:04:33,062
They're gonna take us away!
They're gonna take us away!

14
00:05:33,687 --> 00:05:37,104
He was brought up
by his granny on his mother's side.

15
00:05:37,187 --> 00:05:39,229
Would you know where his mother is?

16
00:05:40,020 --> 00:05:41,604
She's in an asylum.

17
00:05:41,687 --> 00:05:46,437
By law, the father will have to accept
full responsibility for the child's welfare.

18
00:05:46,520 --> 00:05:48,562
Well, he's always welcome here.

19
00:05:48,645 --> 00:05:50,770
The boy is obviously in need of care.

20
00:05:50,854 --> 00:05:52,895
Don't you try to tell me my business.

21
00:05:53,645 --> 00:05:55,895
My son's character's spotless.

22
00:05:56,979 --> 00:05:59,437
There's not a black mark on his conscience.

23
00:07:09,645 --> 00:07:12,520
Your mother's ruined my son's life.

24
00:07:15,562 --> 00:07:17,604
He had the makings of a great man.

25
00:07:21,729 --> 00:07:24,562
He could have had the whole world at his feet.

26
00:07:27,604 --> 00:07:29,645
All gone.

27
00:07:31,645 --> 00:07:33,812
She wanted to destroy him!

28
00:07:40,979 --> 00:07:43,229
What made you think you could come here?

29
00:08:22,229 --> 00:08:24,270
Tommy!

30
00:08:31,104 --> 00:08:34,604
Tommy!

31
00:08:46,187 --> 00:08:48,229
Come on and wash yourself.

32
00:09:04,853 --> 00:09:06,895
Come on, give that to me.

33
00:09:07,853 --> 00:09:09,895
What a state!

34
00:09:09,978 --> 00:09:12,020
It's freezing.

35
00:09:12,103 --> 00:09:14,145
It's cold, Jamie. It's cold.

36
00:09:15,687 --> 00:09:17,728
There we are.

37
00:09:27,437 --> 00:09:29,478
Look up. Look up.

38
00:09:53,645 --> 00:09:55,687
Are you all right?

39
00:09:59,312 --> 00:10:00,312
Hm?

40
00:10:10,687 --> 00:10:12,270
How old are you now, son?

41
00:10:20,728 --> 00:10:23,187
Would you like to come home and live with me?

42
00:11:36,770 --> 00:11:39,603
You whore! You whore! You bloody whore!

43
00:11:39,687 --> 00:11:42,478
I'll swing for you! Bitch! Bitch! Bitch!

44
00:11:42,562 --> 00:11:43,978
Bitch of hell!

45
00:12:07,020 --> 00:12:10,645
~ Summer suns are glowing

46
00:12:10,937 --> 00:12:14,020
~ Over land and sea

47
00:12:14,978 --> 00:12:19,186
~ Happy light is flowing

48
00:12:19,270 --> 00:12:22,520
~ Bountiful and free

49
00:12:23,853 --> 00:12:28,020
~ Everything rejoices

50
00:12:28,103 --> 00:12:31,270
~ In the mellow rays

51
00:12:32,353 --> 00:12:36,353
~ All earth's thousand voices

52
00:12:36,436 --> 00:12:39,853
~ Swell the psalm of praise

53
00:12:41,353 --> 00:12:45,228
~ We will never doubt Thee

54
00:12:45,978 --> 00:12:49,020
~ Though Thou veil Thy light

55
00:12:50,228 --> 00:12:54,478
~ Life is dark without Thee

56
00:12:54,561 --> 00:12:58,186
~ Death with Thee is bright

57
00:12:58,978 --> 00:13:03,353
~ Light of light, shine o'er us

58
00:13:03,436 --> 00:13:06,936
~ On our pilgrim way

59
00:13:07,520 --> 00:13:11,853
~ Go Thou still before us

60
00:13:11,936 --> 00:13:16,145
~ To the endless day

61
00:13:27,686 --> 00:13:29,728
Come away from there.

62
00:13:29,811 --> 00:13:31,853
You're not allowed in there.

63
00:13:50,145 --> 00:13:52,186
Open the door!

64
00:17:59,311 --> 00:18:01,353
This'll never be a blaze.

65
00:18:25,019 --> 00:18:26,436
Ach!

66
00:18:28,019 --> 00:18:30,061
Come on, pet.

67
00:18:32,311 --> 00:18:33,936
Oh...

68
00:18:34,019 --> 00:18:36,061
you must have peed your bed.

69
00:18:36,936 --> 00:18:39,936
Ach, never mind, it was my fault, dear.

70
00:18:40,019 --> 00:18:43,519
Your mother should have taken you out
much earlier than she did.

71
00:18:43,603 --> 00:18:45,394
I'll sit and wash that.

72
00:18:45,478 --> 00:18:48,644
And I'll get you a clean blanket, so I will.

73
00:18:48,728 --> 00:18:50,769
My bottle's safe.

74
00:18:50,853 --> 00:18:52,894
Come on, pet.

75
00:18:52,978 --> 00:18:55,019
It's not your fault. Dinnae worry.

76
00:18:56,978 --> 00:18:59,228
Ach, she's just my wee dearie, so she is.

77
00:18:59,311 --> 00:19:02,103
You're my wee darling, are you?

78
00:19:02,186 --> 00:19:04,228
Ach, you're just a wee bit...

79
00:19:04,311 --> 00:19:06,561
Ach, you're wanting to go down, are you?

80
00:19:06,644 --> 00:19:08,686
Wait a minute. Wait a minute till I get this done.

81
00:19:08,769 --> 00:19:10,811
There you are. Away you go.

82
00:19:53,436 --> 00:19:55,477
Who's the best boy...

83
00:19:56,561 --> 00:19:58,602
.. in all the wide world?

84
00:20:11,686 --> 00:20:13,852
Who is it, my darling?

85
00:20:17,227 --> 00:20:19,269
Have a drink.

86
00:20:28,894 --> 00:20:30,936
Ah.

87
00:20:35,477 --> 00:20:37,519
Ah!

88
00:20:41,811 --> 00:20:46,019
My young prince!

89
00:20:56,727 --> 00:21:00,019
Please, Jesus,
make my granny drunk every night.

90
00:21:07,394 --> 00:21:09,727
I got word from the asylum.

91
00:21:09,811 --> 00:21:11,852
Your mother's dead.

92
00:21:18,977 --> 00:21:23,436
Whatever misfortune she had in her life,
she didnae deserve that.

93
00:21:42,977 --> 00:21:47,227
There is more beauty than horror
in our countryside.

94
00:21:48,186 --> 00:21:50,227
More hope than despair.

95
00:21:50,311 --> 00:21:52,352
If you get stung by a bee,

96
00:21:53,102 --> 00:21:55,144
you can cure it with...

97
00:21:55,227 --> 00:21:56,936
a docken leaf,

98
00:21:57,019 --> 00:21:59,269
because it has healing properties.

99
00:22:00,227 --> 00:22:06,852
There is a legend that says it is safe
to take home anything from nature,

100
00:22:06,936 --> 00:22:08,519
except for one thing -

101
00:22:08,602 --> 00:22:11,311
this is called dead man's flourish.

102
00:22:12,019 --> 00:22:14,061
If you take home this flower,

103
00:22:14,144 --> 00:22:16,186
it means somebody will die.

104
00:26:26,560 --> 00:26:29,685
Your electric treatment
must have done you good.

105
00:26:29,768 --> 00:26:34,060
I would have came and seen you quicker,
only you know what like she is.

106
00:26:41,810 --> 00:26:43,852
He's wondering who I am.

107
00:26:43,935 --> 00:26:45,977
Is he all right?

108
00:26:48,185 --> 00:26:49,810
Hello, darling.

109
00:26:49,893 --> 00:26:51,727
How are you getting on, darling?

110
00:26:51,810 --> 00:26:54,352
Any chance of coming up
and seeing you some time?

111
00:26:57,227 --> 00:26:59,268
Oh, you havnae changed a bit.

112
00:27:01,185 --> 00:27:03,227
Well, I'm away.

113
00:27:13,310 --> 00:27:15,352
- Is that you when you were young?
- Mm-hm.

114
00:27:15,435 --> 00:27:17,477
Let me see your muscles now.

115
00:27:24,393 --> 00:27:26,435
I've got bigger muscles than you.

116
00:27:27,977 --> 00:27:29,602
Hm?

117
00:28:37,102 --> 00:28:38,518
Thanks.

118
00:28:38,602 --> 00:28:40,643
Your mother was a very nice girl.

119
00:28:42,518 --> 00:28:44,560
I was very fond of her.

120
00:28:45,977 --> 00:28:48,018
Always remember that.

121
00:29:11,185 --> 00:29:13,977
Grandad,
Granny isn't very good to you, is she?

122
00:29:14,060 --> 00:29:16,102
No, son.

123
00:29:17,685 --> 00:29:19,643
Why are you laughing?

124
00:29:36,393 --> 00:29:40,018
Dear Jamie, just a few lines
to let you know how I am keeping.

125
00:29:40,102 --> 00:29:41,768
Well, I am keeping well.

126
00:29:41,852 --> 00:29:44,393
My da came to see me,
then he never came back.

127
00:29:45,143 --> 00:29:47,976
If he comes again,
I am going to ask him to bring you.

128
00:29:52,851 --> 00:29:55,643
A man came to talk to me
about a career in the Army.

129
00:29:56,393 --> 00:29:58,435
But I don't care about anything.

130
00:30:06,560 --> 00:30:10,268
The home is not bad,
so long as you learn to look out for yourself.

131
00:30:10,351 --> 00:30:12,310
From what I can see,

132
00:30:12,393 --> 00:30:14,435
it's every man for himself.

133
00:30:14,518 --> 00:30:18,310
They would take your head
if it wasn't fixed onto your shoulders.

134
00:30:19,560 --> 00:30:22,976
I am wearing long trousers now.
That is all I have to say.

135
00:30:23,060 --> 00:30:25,101
Love, Tommy.

136
00:30:35,768 --> 00:30:37,810
How are you getting on, son?

137
00:30:39,518 --> 00:30:42,101
Are you getting along all right, son, are you?

138
00:30:53,018 --> 00:30:55,810
Well, son,
you're growing up to be a fine, big lad.

139
00:30:57,768 --> 00:31:00,851
I had a word with the wife
as regards giving you a home.

140
00:31:02,268 --> 00:31:04,851
It's not that we don't want you, son, but er...

141
00:31:04,935 --> 00:31:06,976
I think you'll understand.

142
00:31:08,643 --> 00:31:10,976
Because you're growing up to be a man now.

143
00:31:25,351 --> 00:31:27,893
Well, son, I've been a bit worried about you.

144
00:31:30,393 --> 00:31:33,976
But I don't suppose it will be very long
before you start work.

145
00:31:35,435 --> 00:31:37,476
Who knows?

146
00:31:37,560 --> 00:31:39,601
You might get a girl.

147
00:31:39,685 --> 00:31:41,851
Everything might go all right for you.

148
00:31:45,643 --> 00:31:48,101
As long as you mind and look after yourself.

149
00:31:48,185 --> 00:31:50,560
You'll do that, son, won't you?

150
00:31:50,643 --> 00:31:52,685
Want a fag?

151
00:31:52,768 --> 00:31:54,810
Oh, thanks, son.

152
00:32:15,226 --> 00:32:17,268
You see, son, it's the kids.

153
00:32:18,185 --> 00:32:22,518
They're set in their ways, and it'd
probably be difficult for you to fit in.

154
00:32:23,268 --> 00:32:26,476
But don't let it worry you.
I'll be back to see you again.

155
00:32:45,851 --> 00:32:48,018
Can I come up here very often to see him?

156
00:32:48,101 --> 00:32:50,435
- Certain dates, is it?
- Any day of the week.

157
00:32:50,518 --> 00:32:52,435
Is it? That's not so bad, then.

158
00:32:52,518 --> 00:32:54,560
'Cos I do miss him really, you know.

159
00:34:48,809 --> 00:34:50,851
Help yourself to an apple, son.

160
00:35:05,309 --> 00:35:07,143
I don't like apples.

161
00:35:16,351 --> 00:35:18,393
Whisht.

162
00:35:29,226 --> 00:35:31,684
There's a mouse listening.

163
00:35:35,601 --> 00:35:39,518
Do you know what pearls look like?

164
00:35:47,351 --> 00:35:50,976
Do you know where your mother kept them?

165
00:36:02,393 --> 00:36:08,518
Well, go and bring them to Granny,
and she'll look after them.

166
00:37:09,226 --> 00:37:11,351
Come on now, son. Where's the pearls?

167
00:37:11,434 --> 00:37:13,476
I don't know.

168
00:37:14,976 --> 00:37:16,392
You're lying.

169
00:37:16,476 --> 00:37:18,476
I couldnae find them.

170
00:37:18,559 --> 00:37:21,851
You know what happens to laddies
who tell lies, don't you?

171
00:37:21,934 --> 00:37:23,351
They go to hell.

172
00:37:23,434 --> 00:37:26,101
- Where's the bloody pearls?
- I don't know where they are!

173
00:37:26,184 --> 00:37:27,767
I'll kill you, you wee bastard!

174
00:37:27,851 --> 00:37:30,851
JAMIE: I don't know where they are!
- Where are they?

175
00:37:30,934 --> 00:37:33,476
You're a bloody liar!
Tell me where they are!

176
00:37:33,559 --> 00:37:35,601
Tell me where they are!

177
00:37:35,684 --> 00:37:38,017
Where are they?

178
00:37:38,101 --> 00:37:40,684
You're a bloody liar.
You know where they are!

179
00:37:40,767 --> 00:37:42,809
I'll kill you now!

180
00:37:45,017 --> 00:37:47,142
- I'll bloody kill you!
- I don't know!

181
00:37:56,726 --> 00:37:58,767
There, now. There, now.

182
00:37:59,892 --> 00:38:01,934
Your granny loves you.

183
00:38:02,642 --> 00:38:04,684
Your granny loves you.

184
00:38:05,392 --> 00:38:07,434
Your granny loves you.

185
00:38:09,309 --> 00:38:11,351
Your granny loves you.

186
00:38:12,059 --> 00:38:14,101
Your granny loves you.

187
00:38:14,809 --> 00:38:17,059
Your granny loves you.

188
00:38:18,184 --> 00:38:20,226
Your granny loves you.

189
00:38:21,517 --> 00:38:23,351
Your granny loves you.

190
00:38:32,267 --> 00:38:34,434
Breathe a word, son, and I'll kill you.

191
00:39:16,601 --> 00:39:19,267
The world.
Now, who lives in the world?

192
00:39:19,351 --> 00:39:21,101
- We do.
- We do.

193
00:39:21,184 --> 00:39:23,642
Are we a big part
of the world or a small part?

194
00:39:23,726 --> 00:39:26,517
Small part.
- A very small part, indeed.

195
00:39:26,601 --> 00:39:29,976
But if you take us compared with this,

196
00:39:30,726 --> 00:39:34,226
which is a picture of
the whole of the solar system in the sky,

197
00:39:34,309 --> 00:39:37,434
we are so tiny that we wouldn't be seen.

198
00:39:37,517 --> 00:39:40,726
Now, this circle here represents the sun.

199
00:39:40,809 --> 00:39:42,851
Now, can you touch the sun?

200
00:39:42,934 --> 00:39:44,976
- No, miss.
- Why?

201
00:39:45,059 --> 00:39:46,476
It's too hot.

202
00:39:46,559 --> 00:39:48,601
Hands up when you want to answer.

203
00:39:48,684 --> 00:39:50,809
Put away those rubber bands, please.

204
00:39:50,892 --> 00:39:52,934
It's a sphere. Say it.

205
00:39:53,017 --> 00:39:54,642
A sphere.

206
00:39:54,726 --> 00:39:56,642
Now, you can't touch the sun.
It's too hot.

207
00:39:56,726 --> 00:39:59,517
And you couldn't get near
it because it is too...?

208
00:39:59,601 --> 00:40:01,642
- What?
- Hot.

209
00:40:01,726 --> 00:40:03,767
Apart from being too hot.

210
00:40:03,851 --> 00:40:06,601
- Too far away.
- Too far away. Very good.

211
00:40:06,684 --> 00:40:11,351
Now, if you wanted to travel to the sun,
it would take you years and years and years

212
00:40:11,434 --> 00:40:13,267
and lifetimes of people.

213
00:40:13,351 --> 00:40:18,559
If we take this as being the sun,
around it go a lot of things which are called...?

214
00:40:19,434 --> 00:40:20,434
What?

215
00:40:20,517 --> 00:40:22,559
- Planets.
- Planets.

216
00:40:22,642 --> 00:40:24,975
Now, we live on a planet. This is a planet.

217
00:40:25,059 --> 00:40:27,600
This is the Earth, the planet that we live on.

218
00:40:27,684 --> 00:40:30,017
Now, we're not the nearest one to the sun.

219
00:40:30,100 --> 00:40:32,142
The nearest one is Mercury.

220
00:40:32,225 --> 00:40:34,267
Mercury's a very hot planet.

221
00:40:34,350 --> 00:40:38,059
It's so hot that you could fry an egg
on the ground with no bother.

222
00:40:57,725 --> 00:40:59,767
Off to see your old flame, are you?

223
00:41:02,142 --> 00:41:04,184
Off to get her sympathy?

224
00:41:04,975 --> 00:41:07,017
You can't wait to get on top of her.

225
00:41:08,975 --> 00:41:09,975
Hm?

226
00:41:14,100 --> 00:41:16,142
It's a long, long time.

227
00:41:16,225 --> 00:41:17,892
It's imagination.

228
00:41:19,017 --> 00:41:22,600
He never thought I knew about his whore,
all these long years.

229
00:41:27,434 --> 00:41:29,475
But I never let him touch me.

230
00:41:29,559 --> 00:41:31,600
No.

231
00:41:31,684 --> 00:41:33,725
Not once.

232
00:41:34,475 --> 00:41:36,517
Not once.

233
00:41:37,309 --> 00:41:38,934
No.

234
00:41:41,142 --> 00:41:44,809
He never cared for me!

235
00:42:30,142 --> 00:42:32,184
You're wearing my shirt.

236
00:43:16,309 --> 00:43:17,642
Grandad!

237
00:43:28,350 --> 00:43:30,392
Grandad!

238
00:43:56,392 --> 00:43:58,767
Would you not be better off in a home?

239
00:44:03,558 --> 00:44:07,183
Grandad hasn't got the strength
to fight for you any more.

240
00:44:15,642 --> 00:44:17,683
Forget the whore, son.

241
00:44:17,767 --> 00:44:19,392
Forget her.

242
00:44:19,475 --> 00:44:21,517
She disnae exist.

243
00:44:25,600 --> 00:44:27,642
You're a king.

244
00:44:27,725 --> 00:44:29,767
The whole world is yours.

245
00:44:31,892 --> 00:44:34,808
Go and find a woman that's worthy of you.

246
00:44:35,975 --> 00:44:38,017
But you must never give up.

247
00:44:38,892 --> 00:44:40,308
Never.

248
00:44:44,142 --> 00:44:46,517
I believe in you, my darling.

249
00:44:48,683 --> 00:44:50,725
I believe in you.

250
00:44:54,933 --> 00:44:56,975
There, there.

251
00:45:02,892 --> 00:45:05,225
Now, now, my darling.

252
00:47:39,725 --> 00:47:41,766
And don't come back!

253
00:47:46,141 --> 00:47:48,891
Oh, for God's sake, what's going on?

254
00:47:48,975 --> 00:47:51,016
Threw me out.

255
00:47:55,516 --> 00:47:57,683
And you mind your own bloody business!

256
00:47:57,766 --> 00:48:00,725
You never cared for my son,
and you broke his heart.

257
00:48:00,808 --> 00:48:03,183
You sympathised with him, with him, that!

258
00:48:03,266 --> 00:48:06,891
You were a whore right from the beginning.
You were nae wife to him.

259
00:48:06,975 --> 00:48:09,225
I warned him. I told him it would come to this.

260
00:48:09,308 --> 00:48:12,766
But he wouldn't have believed it
because you wanted your clutches on him.

261
00:48:12,850 --> 00:48:16,225
Away you go, take him.
There's his silks, go on, wash them!

262
00:48:16,308 --> 00:48:18,350
Your bloody son... A bloody whore?

263
00:48:18,433 --> 00:48:20,475
I'm a whore? You're a...

264
00:48:20,558 --> 00:48:22,600
You get bloody in.

265
00:48:23,516 --> 00:48:26,391
- Stop pushing!
- Away, woman, and shut your door!

266
00:48:26,475 --> 00:48:28,516
You tell me to stop...

267
00:48:28,600 --> 00:48:30,641
- Ma!
- Let go of me!

268
00:48:30,725 --> 00:48:32,933
You get in! Get bloody in!

269
00:48:33,016 --> 00:48:36,100
You'd break my son's heart and then...

270
00:48:36,183 --> 00:48:38,225
Your son's heart...

271
00:48:38,308 --> 00:48:40,475
You broke his fucking heart, not me!

272
00:48:43,850 --> 00:48:45,891
- You bloody old bag!
- You whore!

273
00:48:45,975 --> 00:48:49,975
- You're just a whore!
- Don't call me a whore, you bloody old bag!

274
00:48:50,058 --> 00:48:52,100
Break, break!

