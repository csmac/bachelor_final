1
00:03:25,238 --> 00:03:30,244
The truth is it's been four years
since we heard from her...

2
00:03:33,346 --> 00:03:36,225
She sold the house to my godmother,
may she rest in peace.

3
00:03:36,416 --> 00:03:39,693
And then we came to live here
with my husband and our children.

4
00:03:39,886 --> 00:03:41,456
We came to live here.

5
00:03:44,357 --> 00:03:45,927
The only thing I know is...

6
00:03:46,960 --> 00:03:51,705
that she was with a man from Chaco,

7
00:03:51,898 --> 00:03:56,904
she got together with him
and then she disappeared from town...

8
00:03:59,372 --> 00:04:02,376
She came back later to sell the house
and...

9
00:04:03,376 --> 00:04:04,912
they left together:

10
00:04:05,311 --> 00:04:10,659
But she didn't leave a phone number,
address, nothing...

11
00:04:15,655 --> 00:04:17,293
You came to visit her?

12
00:04:17,490 --> 00:04:19,367
Yeah.

13
00:04:19,892 --> 00:04:24,898
That's too bad, darling,
she didn't leave a number... nothing.

14
00:14:47,052 --> 00:14:48,861
- Good morning.
- Good morning, how are you?

15
00:14:49,054 --> 00:14:54,504
I'm looking for a job,
for a short time... for the summer...

16
00:14:54,894 --> 00:14:58,899
cleaning the pool, mowing the lawn...

17
00:14:59,265 --> 00:15:02,109
The thing is that this is not my house,
I'm watching it for my aunt and uncle

18
00:15:02,301 --> 00:15:05,111
and I think they take care of those things.

19
00:15:05,304 --> 00:15:08,945
For me, only a few pesos a day would be fine,
it would be only for the summer

20
00:15:09,141 --> 00:15:10,882
because...

21
00:15:11,076 --> 00:15:16,219
I have a job promised in Buenos Aires...
but... painting...

22
00:15:16,415 --> 00:15:20,372
Come back tomorrow, let me discuss it
with my aunt and uncle,

23
00:15:20,372 --> 00:15:22,530
maybe there is something to do, okay?

24
00:15:22,620 --> 00:15:24,967
- Yes, great. Thanks.
- No, don't mention it.

25
00:15:25,357 --> 00:15:27,564
You are Eugenio, right?

26
00:15:27,960 --> 00:15:29,234
Yeah.

27
00:15:30,262 --> 00:15:36,611
Because... I used to swim in that pool
when I was little...

28
00:15:37,002 --> 00:15:41,041
I'm 'The Russian', Esther's son...

29
00:15:41,240 --> 00:15:45,052
The one who had a convenience store
next to the empty field, next to an

30
00:15:45,244 --> 00:15:47,121
abandoned bus...

31
00:15:48,180 --> 00:15:54,495
Martin? How are you?
Your face looked familiar...

32
00:15:55,054 --> 00:15:56,397
- I'll come back tomorrow.
- Do you want to come in?

33
00:15:56,588 --> 00:15:59,933
No, no... It's fine.
Eh, do I come back tomorrow?

34
00:16:00,125 --> 00:16:01,263
Okay, come back tomorrow

35
00:16:01,460 --> 00:16:04,304
- Okay, good. Thank you so much.
- You're welcome.

36
00:16:04,496 --> 00:16:05,566
See you tomorrow.

37
00:17:21,106 --> 00:17:24,212
Martin, come here,
I bought something to eat.

38
00:17:50,702 --> 00:17:52,409
Where are you staying?

39
00:17:57,209 --> 00:17:58,586
At my aunt's place.

40
00:17:58,777 --> 00:18:01,223
She lives next to the hospital.

41
00:18:02,815 --> 00:18:04,795
What about your mother?

42
00:18:05,384 --> 00:18:10,299
No... my mom passed away
when I was thirteen.

43
00:18:12,424 --> 00:18:14,370
That was when...

44
00:18:14,827 --> 00:18:18,400
I went to live
with my grandmother in Uruguay.

45
00:18:19,598 --> 00:18:21,202
To Montevideo?

46
00:18:21,867 --> 00:18:25,178
To Cardozo. Have you been there?

47
00:18:25,370 --> 00:18:29,182
I went to Montevideo, not Cardozo.
What is it? A town?

48
00:18:29,475 --> 00:18:34,447
Yes. A town
just in the middle of Uruguay.

49
00:18:35,614 --> 00:18:37,491
And you never came back?

50
00:18:37,683 --> 00:18:42,428
No. Well, yes. I went back to Buenos Aires
a couple of times with my grandmother:

51
00:18:42,621 --> 00:18:48,537
And here, only once when I was eighteen,
also with my grandmother:

52
00:18:49,895 --> 00:18:51,738
So, do you have cousins?

53
00:18:52,364 --> 00:18:54,366
Eh, no...

54
00:18:54,566 --> 00:18:58,245
Yes, but they all live in Buenos Aires
and...

55
00:19:03,375 --> 00:19:07,187
And where you will be staying
in Buenos Aires?

56
00:19:09,548 --> 00:19:13,587
No, no... I don't know yet.

57
00:20:32,264 --> 00:20:37,873
Excuse me, can I take a bath
in the shower in the storage room?

58
00:20:38,270 --> 00:20:41,581
No, there's no hot water there,
but you can bathe in the bathroom in the house.

59
00:20:41,773 --> 00:20:44,481
- No, that's okay, no problem.
- Come on, I'll give you a towel.

60
00:20:44,676 --> 00:20:46,349
- No, it's okay.
- Come on.

61
00:23:08,887 --> 00:23:12,862
Martin, I have some clothes here,
I don't know if you want to look at them.

62
00:23:19,464 --> 00:23:23,810
They belonged to my dad,
maybe something will fit you.

63
00:23:24,035 --> 00:23:27,608
Just saying, so you don't have to work
in your own clothes... I don't know...

64
00:23:28,707 --> 00:23:30,414
Try them on...

65
00:23:30,509 --> 00:23:31,512
Thanks.

66
00:23:31,610 --> 00:23:35,114
You're welcome. Keep anything you want.

67
00:23:35,514 --> 00:23:37,824
- Eugenio...
- Yeah?

68
00:23:38,483 --> 00:23:39,655
Thank you.

69
00:23:40,552 --> 00:23:42,463
Yes, no problem.

70
00:24:33,171 --> 00:24:35,447
Okay, I'll be going now.

71
00:24:35,640 --> 00:24:37,586
You look like another person.

72
00:24:37,776 --> 00:24:40,916
Hey... come and sit down for a second.

73
00:24:41,112 --> 00:24:43,456
Ok...

74
00:24:47,652 --> 00:24:50,098
Eat some cookies,
you have been working the whole day.

75
00:24:50,489 --> 00:24:52,469
Thanks.

76
00:24:53,024 --> 00:24:58,073
I spoke with my uncle, and I convinced him
to have some necessary repairs donr in the house.

77
00:24:58,263 --> 00:24:59,970
So there will be work for the whole summer.

78
00:25:00,165 --> 00:25:03,042
You are staying until the last week
of February, right? - Yeah.

79
00:25:03,134 --> 00:25:05,114
And when exactly do you start in March?

80
00:25:05,604 --> 00:25:07,049
The first week.

81
00:25:07,239 --> 00:25:08,513
Where?

82
00:25:08,707 --> 00:25:14,851
In... maintenance at a... school...
...a religious school.

83
00:25:16,181 --> 00:25:20,892
Listen, whenever you want to take a bath,
fell free. Don't even ask me, okay?

84
00:25:21,086 --> 00:25:23,930
I'm going by the hospital now,
do you want a ride?

85
00:25:24,122 --> 00:25:29,094
No, I'm going to... the public pool,
right next to it is my cousin's house.

86
00:25:29,194 --> 00:25:30,134
I'll take you.

87
00:25:30,228 --> 00:25:33,175
No, it's only seven blocks
and it is the other direction.

88
00:25:33,565 --> 00:25:35,636
Come on, I'll give you a ride.

89
00:25:57,022 --> 00:25:59,832
It's so hot.

90
00:26:00,292 --> 00:26:03,671
Even more now that the sun is so intense.

91
00:26:06,565 --> 00:26:07,635
What?

92
00:26:07,933 --> 00:26:10,072
You'll wet the seat.

93
00:26:10,268 --> 00:26:12,578
No, no problem.

94
00:26:31,289 --> 00:26:33,326
- Thank you.
- No problem. See you.

95
00:26:33,725 --> 00:26:35,796
- Bye. See you tomorrow.
- See you tomorrow.

96
00:27:22,974 --> 00:27:24,920
Martin.

97
00:27:25,110 --> 00:27:28,724
Don't you want to get in the pool?
You've been out in the sun the whole day...

98
00:27:28,913 --> 00:27:30,153
No, that's okay, thanks.

99
00:27:30,348 --> 00:27:31,827
Come on, get in.

100
00:27:32,017 --> 00:27:34,827
Just for a bit,
I'll get you a bathing suit.

101
00:27:35,020 --> 00:27:38,365
Come on, please, don't make me beg.
Okay?

102
00:27:38,757 --> 00:27:40,826
- No...
- Leave this here... Come with me,

103
00:27:40,826 --> 00:27:43,672
I'll lend you a bathing suit
and you'll swim a bit.

104
00:27:43,862 --> 00:27:45,705
<i>Okay... okay...</i>

105
00:27:48,900 --> 00:27:51,745
- We are the same size, don't you think?
- Yes.

106
00:27:51,836 --> 00:27:54,248
Let's see.

107
00:27:58,677 --> 00:28:01,419
Try this one,
I think it should fit fine...

108
00:28:01,613 --> 00:28:02,682
Thanks.

109
00:28:02,781 --> 00:28:06,024
- Try it on here. - Here?
- Yes, it's cool.

110
00:28:06,685 --> 00:28:07,690
Excuse me...

111
00:28:41,753 --> 00:28:43,699
How does it fit?

112
00:28:43,888 --> 00:28:46,061
Good...

113
00:28:46,257 --> 00:28:47,736
Pretty good...

114
00:28:47,926 --> 00:28:50,406
Do you have this one in green?
I'll take it.

115
00:28:50,795 --> 00:28:52,797
Come on, let's go... Leave it...

116
00:28:52,997 --> 00:28:55,705
We'll tidy things up later:

117
00:30:41,105 --> 00:30:43,085
two pineapples...

118
00:30:43,274 --> 00:30:44,309
What?

119
00:30:45,577 --> 00:30:49,024
Two pineapples...

120
00:35:23,321 --> 00:35:26,268
Here it is... look.

121
00:35:30,895 --> 00:35:36,311
See? Here.

122
00:35:38,302 --> 00:35:40,805
Cool.

123
00:35:43,641 --> 00:35:48,351
Hey, why did you come
from your aunt's house?

124
00:35:48,546 --> 00:35:51,857
Because... my grandmother
passed away and...

125
00:35:52,250 --> 00:35:53,422
The one who raised you?

126
00:35:53,618 --> 00:35:55,291
Right...

127
00:35:55,920 --> 00:36:03,532
I lived with her ...
and when she died, my uncle...

128
00:36:03,728 --> 00:36:07,266
said to me that I had to leave the room
I was staying in ...

129
00:36:07,465 --> 00:36:10,844
because there were going
to fix the house to sell it.

130
00:36:11,235 --> 00:36:14,216
To do the legal inheritance and then
sell it.

131
00:36:14,405 --> 00:36:17,443
And he left you on the street
from one day to the next?

132
00:36:17,909 --> 00:36:22,483
He let me stay for three months.
I stayed until December:

133
00:36:25,449 --> 00:36:29,727
And I imagine you inherited part of the house?

134
00:36:29,921 --> 00:36:32,629
The thing is that the house really
belonged to a man that was...

135
00:36:32,823 --> 00:36:38,874
...with my grandmother for twenty years
but he's not my grandpa, I never met my grandpa.

136
00:36:39,964 --> 00:36:45,471
And well, my uncle, well, I call him 'uncle'
but he's really not anything...

137
00:36:45,670 --> 00:36:50,949
- Right...
- Let my grandma stay there but...

138
00:36:51,342 --> 00:36:56,690
...now that she passed away...
I have nothing to do with the house anymore.

139
00:36:58,516 --> 00:37:00,962
And you came here
before going to Buenos Aires?

140
00:37:01,352 --> 00:37:02,695
Yeah.

141
00:37:28,512 --> 00:37:31,322
What happened? What happened, Martin?

142
00:37:31,515 --> 00:37:33,654
It's nothing... I got caught...

143
00:37:35,219 --> 00:37:36,361
Are you all right?

144
00:37:36,454 --> 00:37:37,660
Yes, yes, I got caught...

145
00:37:38,823 --> 00:37:40,325
I got caught in the wire.

146
00:37:40,524 --> 00:37:44,099
- Oh, but you cut yourself, dude.
- No, it's nothing. - Let's see...

147
00:37:44,099 --> 00:37:46,472
I cut myself here.

148
00:37:47,265 --> 00:37:50,337
- No... let me put something on that...
- No, it's nothing.

149
00:37:50,534 --> 00:37:52,639
- It could get infected.
- No, no, it's fine.

150
00:37:52,837 --> 00:37:56,011
No, look how bad it is, no,
come on, come on...

151
00:37:57,775 --> 00:38:02,451
It's pretty bad, come on. Right now.

152
00:38:23,768 --> 00:38:26,044
- You should get a tetanus shot.
- No, no, it's fine.

153
00:38:26,437 --> 00:38:32,012
Yes, yes, now I'll take you to the hospital
and then I'll give you a ride to your aunt's.

154
00:38:50,661 --> 00:38:57,442
I'm not...
I'm not staying at my aunt's.

155
00:39:04,442 --> 00:39:06,820
And where are you staying?

156
00:40:12,042 --> 00:40:14,082
And this is all you have?

157
00:40:14,082 --> 00:40:19,823
I have two, three boxes with stuff
in a cousin's garage in Luj�n.

158
00:40:21,018 --> 00:40:23,726
- Couldn't you have called him?
- No...

159
00:40:23,921 --> 00:40:29,835
He has three children. He barely has
room for them. I didn't want to bother him...

160
00:40:32,997 --> 00:40:36,945
It's okay... it's okay.

161
00:42:14,798 --> 00:42:19,645
- You really have a job in March, right?
- Yeah, they gave me their word, but...

162
00:42:19,837 --> 00:42:22,010
Yeah

163
00:42:22,206 --> 00:42:25,517
Why would they say so if not?

164
00:42:39,290 --> 00:42:41,032
Tomorrow I'll take you to the hospital,

165
00:42:41,032 --> 00:42:45,573
so you can check if you need a tetanus shot
or something, Mm?

166
00:43:48,926 --> 00:43:50,872
- Martin.
- Yeah.

167
00:43:51,095 --> 00:43:56,272
I brought you some books, in case
you want to read in your free time.

168
00:43:56,667 --> 00:43:58,374
Great, thanks.

169
00:44:41,779 --> 00:44:44,350
I remember, I remember...

170
00:44:45,716 --> 00:44:48,996
Tomorrow I'm going to Buenos Aires in the morning,
and I'll be back the day after:

171
00:44:49,186 --> 00:44:52,998
You have some leftovers in the fridge;
meat and potatoes from the other day...

172
00:44:53,190 --> 00:44:55,727
Otherwise, you can cook something...
whatever you want.

173
00:44:55,926 --> 00:44:57,064
Okay.

174
00:44:58,328 --> 00:45:01,832
You will be taking care of the house,
mmh?

175
00:45:06,236 --> 00:45:09,046
I'll get another coke...

176
00:49:12,716 --> 00:49:15,162
Excuse me, Martin.

177
00:49:16,553 --> 00:49:19,227
Eh, I'm going to wash some clothes,
I'll take this.

178
00:49:19,423 --> 00:49:20,629
Sure.

179
00:49:22,559 --> 00:49:25,403
Would you come to my room when you finish?

180
00:49:25,595 --> 00:49:27,074
Yeah.

181
00:50:06,536 --> 00:50:09,107
- Eugenio?
- Here, come in.

182
00:50:17,147 --> 00:50:19,457
You asked me to come?

183
00:50:22,619 --> 00:50:23,663
Yeah.

184
00:50:26,356 --> 00:50:31,101
Eh, I brought you some clothes,
check if some of it fits you...

185
00:50:31,294 --> 00:50:35,003
...from this pile. I have so much clothing.

186
00:50:35,198 --> 00:50:37,644
And I never use it... try it on.

187
00:50:40,137 --> 00:50:45,416
And try this on too. Check if it fits
because I can still exchange it.

188
00:50:45,609 --> 00:50:47,589
Okay.

189
00:50:56,586 --> 00:50:58,497
This one is empty.

190
00:50:58,688 --> 00:51:01,794
A friend of mine has a store
and he gave them to me for free...

191
00:51:02,192 --> 00:51:03,603
I'll tell him when I see him.

192
00:51:04,594 --> 00:51:07,074
- Should I try it on?
- Yes, try it on here!

193
00:51:07,264 --> 00:51:10,143
That... There is no problem,
don't be a sissy...

194
00:52:42,792 --> 00:52:45,238
And what are you writing?

195
00:52:48,165 --> 00:52:49,337
A book...

196
00:52:49,633 --> 00:52:57,450
Generally I write for newspapers,
articles... But now I'm writing a book.

197
00:52:58,942 --> 00:53:01,286
Like a story?

198
00:53:02,212 --> 00:53:04,818
Yeah... a novel.

199
00:53:12,322 --> 00:53:14,268
And what's the story about?

200
00:53:19,429 --> 00:53:21,204
It's...

201
00:53:21,565 --> 00:53:25,911
A father who lives with his children
on a kind of large piece of land...

202
00:53:26,303 --> 00:53:27,839
and he's the owner:

203
00:53:28,905 --> 00:53:30,316
And one day...

204
00:53:30,840 --> 00:53:35,414
The youngest daughter, a six-year-old,
starts to ask questions.

205
00:53:36,880 --> 00:53:39,723
The family is like a type of period family
where no one talks at the table...

206
00:53:39,916 --> 00:53:42,226
...unless the father gives permission,
right?

207
00:53:42,886 --> 00:53:45,924
And the girl starts to ask questions
that really surprise her father...

208
00:53:46,323 --> 00:53:47,927
One day, for instance, she asks...

209
00:53:48,625 --> 00:53:51,834
why does he own all that land?

210
00:53:53,230 --> 00:53:55,836
The father answers quite naturally
that he inherited it.

211
00:53:56,233 --> 00:53:59,305
But the girl starts to get interested
in how could it be...

212
00:53:59,970 --> 00:54:03,782
that people decide that one day
to the next something belongs to them.

213
00:54:04,808 --> 00:54:05,878
It's a...

214
00:54:07,310 --> 00:54:11,884
a game between the father's power
and this kind of perspective...

215
00:54:12,282 --> 00:54:15,589
between innocent and communist,
so to speak, of the girl.

216
00:54:15,589 --> 00:54:17,829
And the father doesn't know where it comes from.

217
00:54:18,421 --> 00:54:21,334
It's a sort of tale
about the fair and the unfair...

218
00:54:22,325 --> 00:54:26,296
but from the innocence point of view,
of trying to... understand.

219
00:54:27,464 --> 00:54:31,310
These questions lead the father
to see the girl as a germ...

220
00:54:31,501 --> 00:54:33,606
of something that could devour him.

221
00:54:34,671 --> 00:54:37,015
The novel is called 'The Germ'.

222
00:54:39,976 --> 00:54:41,478
That's it...

223
00:54:44,981 --> 00:54:46,824
- It's good.
- Mhm.

224
00:56:26,649 --> 00:56:27,957
Are you thirsty?

225
00:56:29,586 --> 00:56:30,587
Thanks.

226
00:57:04,854 --> 00:57:07,630
We stoned a cat to death here...

227
00:57:09,159 --> 00:57:11,435
That's right.

228
00:57:12,529 --> 00:57:16,978
I didn't remember that I had been with you...
I remembered just now.

229
00:57:17,667 --> 00:57:21,080
The truth is I remembered
that I had done it with some kid, next to a river,

230
00:57:21,471 --> 00:57:23,007
under a bridge...

231
00:57:30,947 --> 00:57:36,187
Every time I see a dead cat I remember
that cat lying under the bridge all beat up.

232
00:57:41,791 --> 00:57:45,466
Do you remember when we used
to go hunting with my dad?

233
00:57:46,196 --> 00:57:48,107
I totally remember:

234
00:57:49,032 --> 00:57:51,205
Your dad told my mom
that we were going fishing...

235
00:57:51,601 --> 00:57:54,548
- Yeah... -
and she wouldn't worry.

236
00:57:54,838 --> 00:57:57,580
She didn't even imagine that two hours later
we would be walking with a gun in our hands.

237
00:57:57,774 --> 00:58:00,733
Well, it wasn't really a gun,
it was an air gun.

238
00:58:00,733 --> 00:58:03,143
- Really?
- Yeah, it's still here.

239
00:58:03,143 --> 00:58:05,405
If you see it you would laugh.
It's this small.

240
00:58:06,249 --> 00:58:09,230
Of course, you were eight years old
and you remember it like a shotgun...

241
00:58:09,619 --> 00:58:10,791
but it really wasn't.

242
00:58:12,055 --> 00:58:14,467
No, my dad wasn't that crazy...

243
00:58:14,724 --> 00:58:17,204
Still, you can shoot your eye out
with a pellet.

244
00:58:56,599 --> 00:58:57,771
Should we go for a swim?

245
00:58:58,134 --> 00:59:00,910
- We go there next to the bridge?
- I don't have a swim suit.

246
00:59:01,104 --> 00:59:05,814
- Swim naked, I won't look.
- No, it's not about that.

247
00:59:06,009 --> 00:59:07,920
C'mon, let's go.

248
01:00:20,083 --> 01:00:22,188
Want to go eat?

249
01:00:24,954 --> 01:00:27,059
I'll be right out.

250
01:01:16,939 --> 01:01:18,008
What?

251
01:01:57,480 --> 01:01:59,187
- Can I?
- Wait.

252
01:02:14,363 --> 01:02:18,311
It is very old... it is old and the...
the sight is crooked.

253
01:02:18,868 --> 01:02:20,905
That's why... Try.

254
01:02:26,309 --> 01:02:28,346
- Be careful.
- Yeah.

255
01:02:52,835 --> 01:02:57,346
The sight was crooked right?
Did you aim to the right or to the left of the can?

256
01:02:57,540 --> 01:02:59,349
No, I aimed at the can.

257
01:03:03,212 --> 01:03:04,254
Let's see.

258
01:03:08,317 --> 01:03:09,359
There you go.

259
01:03:10,253 --> 01:03:12,096
I'll load it for you because...

260
01:03:30,406 --> 01:03:33,046
Shall we make a bet to see
who can hit the can more?

261
01:03:33,142 --> 01:03:34,150
- Okay.
- Yeah?

262
01:03:34,243 --> 01:03:35,779
What do we bet?

263
01:03:35,978 --> 01:03:38,015
No, nothing, just to know who is better:

264
01:03:39,048 --> 01:03:40,049
Okay...

265
01:04:13,382 --> 01:04:16,920
Mhm... Mhm..

266
01:04:17,119 --> 01:04:20,464
Yeah, yeah... Uhuh...

267
01:04:23,125 --> 01:04:26,470
Yeah... yeah, yeah...

268
01:04:29,232 --> 01:04:31,234
Eh...

269
01:04:58,427 --> 01:05:00,338
- What?
- Nothing, nothing.

270
01:05:00,529 --> 01:05:01,807
- What!?
- Nothing.

271
01:05:01,898 --> 01:05:02,903
Come on, what?

272
01:05:04,400 --> 01:05:06,437
How much older are you than me?

273
01:05:07,970 --> 01:05:09,578
I don't know, two or three years.

274
01:05:10,907 --> 01:05:13,046
I remember, when we were kids...

275
01:05:13,676 --> 01:05:17,453
That your dad once... he gave my mom
a bunch of your clothes.

276
01:05:17,947 --> 01:05:21,918
And I spent a whole winter
wearing a red sweater...

277
01:05:22,118 --> 01:05:25,463
that had 'Eugenio' written in black here.

278
01:05:25,655 --> 01:05:27,396
No! I forgot about that sweater!

279
01:05:28,291 --> 01:05:31,363
My grandmother knitted it. She knitted
one for my brother and sister too...

280
01:05:31,560 --> 01:05:36,166
Mine was the only one that had the full name.
The others said 'Santi' and 'Flor'.

281
01:05:36,365 --> 01:05:37,409
Right...

282
01:05:40,503 --> 01:05:43,109
I don't remember much about your siblings...

283
01:05:43,572 --> 01:05:46,412
It makes sense.
I'm a couple of years older than you,

284
01:05:46,412 --> 01:05:49,282
and my siblings are even older.
Flor is five years older and Santi seven.

285
01:05:49,478 --> 01:05:50,650
Right.

286
01:05:52,381 --> 01:05:54,554
Amazing, I didn't even remember...

287
01:05:56,419 --> 01:06:00,424
How awesome was my dad,
he was always willing to help people out...

288
01:06:01,424 --> 01:06:03,301
I miss him, actually.

289
01:06:04,694 --> 01:06:06,469
Did he pass away recently?

290
01:06:08,397 --> 01:06:10,570
He passed away two years ago.

291
01:06:11,600 --> 01:06:15,807
That's when I inherited this house.
And my uncles bought it from us to help me out.

292
01:06:17,206 --> 01:06:19,516
Because my brother and sister wanted the money,

293
01:06:19,709 --> 01:06:21,586
and I didn't want to lose the house.

294
01:06:23,045 --> 01:06:25,286
And my uncle never told me so
but...

295
01:06:26,382 --> 01:06:27,554
I know it was like that.

296
01:06:27,750 --> 01:06:31,527
When he heard that we had to sell the house
he decided to buy it himself.

297
01:06:58,748 --> 01:07:02,423
- Easy, easy. Put your arms back.
- Here?

298
01:07:47,730 --> 01:07:50,540
The loser has to give something to the winner:

299
01:07:51,367 --> 01:07:53,110
You'll have to give me your crucifix.

300
01:07:53,402 --> 01:07:57,282
No... I can't. It is a gift
from a girl I really care for:

301
01:07:57,473 --> 01:07:59,282
- A girlfriend?
- No.

302
01:08:00,843 --> 01:08:03,346
- Okay, and what will you give me?
- Anything you want...

303
01:08:03,546 --> 01:08:05,116
Okay, your eye glasses.

304
01:08:05,781 --> 01:08:08,227
Son of a bitch! Are you ready?

305
01:08:08,417 --> 01:08:11,762
- Sure... What's the finish line?
- At the end.

306
01:08:11,954 --> 01:08:13,255
- On the count of three?
- Okay.

307
01:08:13,355 --> 01:08:14,459
One?

308
01:08:15,491 --> 01:08:16,526
Two...

309
01:08:34,343 --> 01:08:35,720
No, it didn't count.

310
01:08:37,379 --> 01:08:40,155
It didn't count because
I felt my heart speed up a lot.

311
01:08:40,483 --> 01:08:43,657
I stopped because I thought
it was going to stop...

312
01:08:43,853 --> 01:08:46,800
like tachycardia or arrhythmia.
Feel.

313
01:08:47,857 --> 01:08:53,307
Notice how it beats off time...
Pumpum, pum, pum.

314
01:08:53,496 --> 01:08:56,705
Like, off time... Do you feel it?

315
01:09:05,674 --> 01:09:08,154
It feels on time...

316
01:09:09,645 --> 01:09:12,251
Might be, maybe it was just me...

317
01:09:20,189 --> 01:09:23,363
Ugh, my god...

318
01:11:58,013 --> 01:11:59,321
Hey Martin.

319
01:12:02,685 --> 01:12:04,528
I found another one...

320
01:12:17,733 --> 01:12:22,568
Martin... Hey... Hey...

321
01:12:27,776 --> 01:12:29,449
Martin...

322
01:12:48,497 --> 01:12:50,340
Martin...

323
01:12:54,703 --> 01:12:56,944
Hey, Martin...

324
01:13:06,048 --> 01:13:08,688
Martin, I found another one...

325
01:13:25,467 --> 01:13:27,879
Martin, I found another one...

326
01:13:42,551 --> 01:13:47,091
I'll stay here fora a little bit...

327
01:13:50,459 --> 01:13:51,961
A little bit here...

328
01:18:59,501 --> 01:19:01,378
- Excuse me, Eugenio.
- Yeah.

329
01:19:01,770 --> 01:19:04,979
- Do I take the trash out now or..
- I can't right now, Martin.

330
01:20:39,968 --> 01:20:42,380
Why didn't you come with the boys?

331
01:20:42,838 --> 01:20:45,580
Nicolas had a birthday party,
and Andre said he wanted to come...

332
01:20:46,141 --> 01:20:48,817
- 'Uncle Eugenio this and Uncle Eugenio that'...
- So why didn't you bring him then?

333
01:20:49,010 --> 01:20:51,414
No, I wanted to get away from Buenos Aires
by myself, the business is going non stop.

334
01:20:51,613 --> 01:20:53,957
My head is about to explode.

335
01:21:02,190 --> 01:21:03,396
You like him.

336
01:21:04,059 --> 01:21:05,067
What?

337
01:21:06,995 --> 01:21:10,499
You gave him a job because you like him.
Do you think I'm stupid?

338
01:21:11,233 --> 01:21:14,546
What are you going to do?
Are you going to fuck him and then tell him:

339
01:21:14,936 --> 01:21:18,213
'Hey, sorry, I gave you a job
because you turned me on.'

340
01:21:18,406 --> 01:21:21,580
I couldn't not help him.
You would have done the same thing.

341
01:21:21,977 --> 01:21:24,082
Do you want to be his boyfriend?

342
01:21:25,180 --> 01:21:28,423
Don't push it... don't push it. Enough.

343
01:21:30,285 --> 01:21:32,356
- Did something happen already?
- No!

344
01:21:33,455 --> 01:21:34,533
Does he know?

345
01:21:35,190 --> 01:21:36,897
What should I have told him?

346
01:21:37,092 --> 01:21:40,096
'I'll give you a job but hey, I'm gay,

347
01:21:40,295 --> 01:21:43,071
if you want to work or not'

348
01:21:49,404 --> 01:21:52,248
And if you fuck him and the summer ends,
what are you going to do then?

349
01:21:53,508 --> 01:21:55,954
You'll take him to Buenos Aires,
to a Levi's store...

350
01:21:56,344 --> 01:22:00,486
You buy him some clothes...
You take him to Palermo and support him?

351
01:22:01,549 --> 01:22:03,551
Or you'll send him to work
on a construction site...

352
01:22:04,085 --> 01:22:06,622
And you'll wait for him at six o'clock
with a hot tea.

353
01:22:07,022 --> 01:22:08,032
Stop it. Enough!

354
01:22:09,190 --> 01:22:10,225
Enough...

355
01:22:17,699 --> 01:22:19,610
I'll get some ice.

356
01:22:20,001 --> 01:22:23,642
And we'll drink something.
I have to leave at seven.

357
01:22:24,072 --> 01:22:26,018
Okay.

358
01:23:11,086 --> 01:23:12,292
Over here...

359
01:23:17,459 --> 01:23:19,132
- Done?
- Yeah.

360
01:23:20,662 --> 01:23:23,040
Should I... continue?

361
01:23:26,267 --> 01:23:27,371
Oh, sorry.

362
01:23:27,569 --> 01:23:29,549
Hey, be careful with that stuff...

363
01:24:30,331 --> 01:24:31,409
What?

364
01:24:36,337 --> 01:24:39,216
- We need to buy chlorine.
- Yes, I'll give you some money later:

365
01:25:21,116 --> 01:25:22,288
- Martin. - Yeah.

366
01:25:23,451 --> 01:25:25,453
The poison you bought the other day...

367
01:25:25,854 --> 01:25:29,862
The one you used on the room next to the grill.
Is there any left or did you use it all?

368
01:25:30,258 --> 01:25:33,296
No, there is a lot left,
cause you have to mix it with water...

369
01:25:33,495 --> 01:25:35,873
- I used only a third of it.
- Oh.

370
01:25:37,232 --> 01:25:40,236
Do you mind if I wash my clothes here?

371
01:25:40,435 --> 01:25:42,472
- No, no, no.
- Sure?

372
01:25:42,670 --> 01:25:45,412
- Yeah, yeah, yeah... sure.
- So that's not a problem?

373
01:25:46,141 --> 01:25:47,814
- I mean, it's no big deal if you mind.
- No.

374
01:25:48,209 --> 01:25:52,224
It's... the poison is next to the little room,
where the tiles are.

375
01:25:53,681 --> 01:25:55,456
I'll look for it there...

376
01:26:39,928 --> 01:26:43,205
See?... So...

377
01:26:45,900 --> 01:26:47,607
- Do you see how they fall?
- Yeah.

378
01:26:47,802 --> 01:26:49,577
You have to do this until they are all gone...

379
01:26:49,771 --> 01:26:52,377
- Because they block they light...
- Right.

380
01:26:52,574 --> 01:26:56,522
You have to do it from side to side
and then sweep them up...

381
01:26:57,445 --> 01:26:59,322
It's full of them.
It's always full of leaves...

382
01:26:59,514 --> 01:27:01,885
the thing is that they never fall,
so then they start to...

383
01:27:02,717 --> 01:27:03,788
Stop, stop-

384
01:27:05,386 --> 01:27:07,525
- Wait, wait, wait.
- What's the problem?

385
01:27:07,722 --> 01:27:09,497
- Nothing, I have a...
- Are you okay?

386
01:27:09,691 --> 01:27:11,602
Yes, yes, but I have a...

387
01:27:12,760 --> 01:27:16,606
- I can't open it.
- Wait, wait... let me see. Stay still.

388
01:27:17,498 --> 01:27:21,469
- Open it slowly... slowly. - Slowly.

389
01:27:23,338 --> 01:27:26,342
- Blow - No, no, no... there.

390
01:27:27,442 --> 01:27:30,753
- There it is, see?
- Oh look, it's so small.

391
01:27:30,979 --> 01:27:36,395
- You're full of leaves.
- Yes, it's awful, how many there are.

392
01:27:37,885 --> 01:27:39,865
A lot.

393
01:27:44,592 --> 01:27:46,572
You also have some...

394
01:27:47,028 --> 01:27:48,598
No, no, no, no...

395
01:27:50,365 --> 01:27:51,366
No, no.

396
01:27:55,903 --> 01:27:58,247
What? Is is because of the drawings?

397
01:33:16,958 --> 01:33:19,131
Pineapple...

