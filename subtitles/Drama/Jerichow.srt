1
00:02:05,880 --> 00:02:07,791
Sorry about your mother.

2
00:02:10,160 --> 00:02:11,673
Damn it!

3
00:02:14,320 --> 00:02:18,518
You know my father?
He was a stupid pig.

4
00:02:18,720 --> 00:02:20,836
But he was right about one thing.

5
00:02:21,000 --> 00:02:23,116
If a friend asks to borrow a thousand,
give him a hundred.

6
00:02:23,320 --> 00:02:25,959
Otherwise he'll cross the street
when he sees you.

7
00:02:26,120 --> 00:02:29,351
You'll feel guilty
that you want your money back.

8
00:02:30,120 --> 00:02:32,998
That's how I feel now,
lousy and guilty.

9
00:02:33,720 --> 00:02:37,076
Why didn't you say,
"my mother died, I have to leave"?

10
00:02:37,280 --> 00:02:40,556
You just disappeared,
that's not right.

11
00:02:42,680 --> 00:02:45,240
I planned to return tomorrow.
- Don't give me that crap!

12
00:02:46,680 --> 00:02:48,910
I'll pay you back.
- How?

13
00:02:49,120 --> 00:02:53,159
They took everything yesterday:
Ice machine, espresso machine, the stereo.

14
00:02:54,160 --> 00:02:57,277
Eight months rent due,
that I 'm stuck with.

15
00:02:57,720 --> 00:02:59,836
How are you going to pay up?

16
00:03:03,640 --> 00:03:05,517
How much did she leave you?

17
00:03:06,560 --> 00:03:08,551
Don't give me that look, Thomas.

18
00:03:09,880 --> 00:03:12,838
Jewelry, savings account...
Meisner porcelain, something!

19
00:03:14,240 --> 00:03:17,596
Should we search the place?
Thomas, want us to do that?

20
00:03:36,800 --> 00:03:38,950
I'm going to renovate the house.

21
00:03:39,560 --> 00:03:42,028
Why?
- I want to live here.

22
00:03:42,880 --> 00:03:44,108
Here?

23
00:03:45,800 --> 00:03:49,110
I'll fix the house,
look for work, pay you back.

24
00:03:49,320 --> 00:03:53,233
How much per month, 75?
Maybe 100 for Christmas?

25
00:04:11,560 --> 00:04:13,391
What was wrong with her?

26
00:04:15,080 --> 00:04:17,753
Something with her stomach.
- Cancer?

27
00:04:38,520 --> 00:04:40,397
Where was your room?

28
00:04:46,960 --> 00:04:48,951
Lutz, let us talk alone.

29
00:04:52,640 --> 00:04:54,631
It was hard to find you.

30
00:04:57,320 --> 00:04:59,595
I thought you were up to something.

31
00:05:08,200 --> 00:05:09,633
Is this her?

32
00:05:13,000 --> 00:05:15,275
A lemonade tree!
- What?

33
00:05:16,160 --> 00:05:17,957
Pippi Longstocking.

34
00:05:18,640 --> 00:05:20,995
You probably didn't know her
in East Germany.

35
00:05:22,640 --> 00:05:24,471
The lemonade-tree
didn't have a house.

36
00:05:24,680 --> 00:05:27,877
It had lemonade inside,
as much as you wanted.

37
00:05:33,000 --> 00:05:35,594
Let's climb up there.
- The wood has rotted.

38
00:05:35,800 --> 00:05:38,837
Come on, Thomas.

39
00:05:39,320 --> 00:05:43,598
Leon, the wood is totally rotten.
- Come on.

40
00:05:49,440 --> 00:05:53,274
You'll come crashing down.
- You didn't used to be so scared.

41
00:06:27,000 --> 00:06:29,389
I needed it to renovate the house.

42
00:06:31,760 --> 00:06:33,318
You lied.

43
00:06:35,960 --> 00:06:37,109
Thomas...

44
00:08:54,040 --> 00:08:56,759
You don't need to come here
in work clothes.

45
00:08:56,960 --> 00:08:59,394
I'm renovating.
- At your own house?

46
00:08:59,720 --> 00:09:01,915
Otherwise it'd be illegal employment.

47
00:09:04,000 --> 00:09:06,878
Harvest helper,
starts June 25th.

48
00:09:07,040 --> 00:09:08,109
Harvesting what?

49
00:09:08,520 --> 00:09:10,795
Cucumbers, on a machine.

50
00:09:11,240 --> 00:09:12,832
And until then?

51
00:09:14,200 --> 00:09:16,634
I have absolutely no money left.

52
00:09:17,280 --> 00:09:19,157
We have to check that.

53
00:09:19,920 --> 00:09:20,989
When?

54
00:09:22,560 --> 00:09:24,232
We'll send someone on Monday.

55
00:09:24,440 --> 00:09:27,034
I can prove my bank account
is at zero right now.

56
00:09:27,240 --> 00:09:30,869
You own a house,
we have to estimate its value.

57
00:09:34,600 --> 00:09:36,591
You were in Afghanistan.

58
00:09:36,800 --> 00:09:40,588
Didn't you receive
a discharge bonus?

59
00:09:42,800 --> 00:09:45,314
I got a dishonorable discharge.

60
00:09:45,560 --> 00:09:47,039
Why?

61
00:09:50,800 --> 00:09:53,519
I have to fill out your profile.

62
00:09:54,600 --> 00:09:56,511
For a cucumber harvester?

63
00:09:57,000 --> 00:09:58,274
Yes.

64
00:10:03,160 --> 00:10:05,549
54.31

65
00:10:09,120 --> 00:10:12,237
You give me that now?
I have to re-do it.

66
00:10:12,440 --> 00:10:15,398
You have to pay for alcohol
and cigarettes yourself.

67
00:10:15,640 --> 00:10:17,676
Take them out, please.

68
00:10:18,040 --> 00:10:20,110
I didn't know.

69
00:11:18,120 --> 00:11:20,475
Should I call an ambulance?

70
00:11:20,760 --> 00:11:22,557
Absolutely not.

71
00:11:29,080 --> 00:11:31,640
Can you get this thing out of here?

72
00:11:50,600 --> 00:11:51,919
Thanks.

73
00:12:15,200 --> 00:12:16,997
Mr. Ökzan?

74
00:12:20,400 --> 00:12:23,358
I'm calling the station and you'll
have to give us a breath test.

75
00:12:23,560 --> 00:12:25,232
I wasn't driving.

76
00:12:25,440 --> 00:12:26,668
He was driving!

77
00:12:29,000 --> 00:12:30,638
Is that true?

78
00:12:35,600 --> 00:12:37,636
Whose bags are these?
- Mine.

79
00:12:38,640 --> 00:12:41,279
Why are they standing here?
- They fell out.

80
00:12:42,400 --> 00:12:44,152
They landed quite nicely.

81
00:12:46,000 --> 00:12:47,479
You were lucky this time.

82
00:13:04,960 --> 00:13:07,952
Behind the forest
is a side road,

83
00:13:08,400 --> 00:13:09,799
where that asshole is waiting.

84
00:13:10,000 --> 00:13:13,959
If I drive by, they'll stop me
and take away my license.

85
00:13:14,400 --> 00:13:16,675
Can you drive for me?

86
00:13:23,160 --> 00:13:24,195
There.

87
00:13:49,960 --> 00:13:52,315
Overthere, on the right.

88
00:14:44,800 --> 00:14:46,552
What happened?

89
00:14:48,280 --> 00:14:51,192
Where were you?
I couldn't reach you.

90
00:14:51,680 --> 00:14:54,194
My battery died,
I didn't have the recharger.

91
00:15:41,320 --> 00:15:43,151
Excuse me.

92
00:15:51,200 --> 00:15:52,792
Excuse me.

93
00:15:53,240 --> 00:15:56,277
I'll leave the keys in the car.

94
00:15:56,520 --> 00:15:57,953
Should I call a taxi?

95
00:15:58,160 --> 00:16:00,071
No thanks.

96
00:16:08,280 --> 00:16:09,838
Wait a minute.

97
00:16:21,680 --> 00:16:23,671
Foryou.
- That's not necessary.

98
00:16:23,880 --> 00:16:25,996
Come on, it's okay.

99
00:19:08,040 --> 00:19:09,632
Excuse me.

100
00:19:14,200 --> 00:19:15,952
Do you have a minute?

101
00:19:22,760 --> 00:19:24,955
The damn cops caught me,
on Wednesday...

102
00:19:25,400 --> 00:19:30,349
...with 1.9% blood alcohol.
My license is gone for a year.

103
00:19:34,040 --> 00:19:34,950
So now?

104
00:19:35,160 --> 00:19:39,915
I own 45 snack bars around here.
I make a daily tour, take orders...

105
00:19:40,120 --> 00:19:44,079
...deliver, pick up the receipts,
check up on the business.

106
00:19:45,120 --> 00:19:47,588
Now I need someone to drive me.

107
00:19:51,000 --> 00:19:54,197
Six days a week, with overtime.

108
00:19:55,240 --> 00:19:57,674
You'll earn 2 to 2.5 thousand.

109
00:20:04,240 --> 00:20:05,593
Okay.

110
00:20:07,000 --> 00:20:08,513
Tomorrow morning. 8 A. M.

111
00:20:09,160 --> 00:20:10,718
8 A. M.

112
00:20:11,440 --> 00:20:12,998
You know where.

113
00:20:15,920 --> 00:20:18,036
Here's an advance.

114
00:20:21,960 --> 00:20:23,188
Laura?

115
00:21:07,760 --> 00:21:08,875
Good morning.

116
00:21:09,120 --> 00:21:10,394
Good morning.

117
00:21:11,640 --> 00:21:12,709
Laura!

118
00:21:15,760 --> 00:21:18,797
Good that you watched her.
She's doing your job.

119
00:21:19,000 --> 00:21:21,673
Putting together orders,
loading the truck.

120
00:21:21,880 --> 00:21:25,509
Come an hour earliertomorrow,
to get the details.

121
00:21:26,600 --> 00:21:28,750
This is my wife, Laura.

122
00:21:29,760 --> 00:21:33,275
He should come earliertomorrow.
- That's what we just said.

123
00:21:33,520 --> 00:21:36,592
Fine, I have to finish up.

124
00:21:39,560 --> 00:21:40,959
Did you bring your papers?

125
00:21:41,160 --> 00:21:43,037
We'll do that tonight.

126
00:21:43,520 --> 00:21:48,150
You deliverthe orders,
and they sign here, okay?

127
00:21:48,640 --> 00:21:52,713
Laura gets the new orders,
she does the accounting.

128
00:21:52,880 --> 00:21:55,917
She gives you the delivery forms
and you put the orders together.

129
00:21:56,080 --> 00:21:57,115
Got it.

130
00:21:57,320 --> 00:22:02,713
You'll learn the fine points in due time.
You have to load the truck properly.

131
00:22:04,000 --> 00:22:08,391
You can't screw around
loading up, that costs time.

132
00:22:09,840 --> 00:22:14,470
Should we drive first to the furthest
delivery point orthe nearest?

133
00:22:14,720 --> 00:22:16,551
Is this a quiz?

134
00:22:16,840 --> 00:22:18,398
I'm curious.

135
00:22:20,280 --> 00:22:23,511
They sent me six candidates
from the job center.

136
00:22:23,720 --> 00:22:25,836
Not one of them got it right.

137
00:22:27,760 --> 00:22:30,274
Are you thinking
or don't want to play the game?

138
00:22:30,520 --> 00:22:33,273
The nearest one.
- Correct.

139
00:22:33,800 --> 00:22:36,473
Why?
- You'll save gas.

140
00:22:39,280 --> 00:22:40,793
How?

141
00:22:41,360 --> 00:22:45,876
Forthe longest leg the truck is empty,
it's lighter and uses less gas.

142
00:22:46,040 --> 00:22:47,155
Perfectly correct.

143
00:22:47,440 --> 00:22:49,237
Perfectly correct.

144
00:23:50,600 --> 00:23:52,397
Sorry...

145
00:23:53,720 --> 00:23:54,869
For what?

146
00:23:55,040 --> 00:23:57,076
The stupid quiz.

147
00:24:00,000 --> 00:24:02,639
I was just acting bossy.
I'd be angry too.

148
00:24:10,200 --> 00:24:13,829
Six times 70...

149
00:24:17,000 --> 00:24:20,629
597.54 that adds up to...

150
00:24:26,000 --> 00:24:28,070
In summer?

151
00:25:01,640 --> 00:25:04,518
Gina Mango-Nectar.

152
00:25:15,000 --> 00:25:18,037
Send me Murath then!
As soon as possible.

153
00:25:19,040 --> 00:25:22,077
Of course he needs papers,
are you stupid?

154
00:25:24,440 --> 00:25:27,398
Send him to Mustafah,
he knows everything.

155
00:25:29,160 --> 00:25:31,390
You did well.
- What did I do?

156
00:25:31,640 --> 00:25:34,598
Everyone here cheats on me.

157
00:25:38,280 --> 00:25:41,397
Did you ever do that before?
- What?

158
00:25:41,760 --> 00:25:43,955
That martial-arts move.

159
00:25:44,720 --> 00:25:46,836
Were you in the army?

160
00:25:55,520 --> 00:25:57,636
Are you busy on the weekend?

161
00:25:57,840 --> 00:25:58,875
Why?

162
00:25:59,040 --> 00:26:02,316
You passed the test.
We should celebrate.

163
00:26:03,040 --> 00:26:04,553
Okay.

164
00:26:08,680 --> 00:26:11,194
Laura is a pretty woman, huh?

165
00:26:12,560 --> 00:26:14,949
Why shouldn't you look?

166
00:26:17,000 --> 00:26:19,719
I was watching you this morning.

167
00:26:26,400 --> 00:26:28,516
Turn around.
- That's the road to Stendal.

168
00:26:28,720 --> 00:26:30,233
Turn around!

169
00:26:42,000 --> 00:26:42,955
Hello?

170
00:26:43,120 --> 00:26:44,553
Hello, where are you?

171
00:26:44,720 --> 00:26:47,029
I'm picking up an extra delivery.

172
00:26:48,040 --> 00:26:49,553
Which delivery?

173
00:26:49,760 --> 00:26:51,478
From the beverage company.

174
00:26:51,680 --> 00:26:53,113
Oh yes.

175
00:26:54,920 --> 00:26:57,559
I fired Halid, he was cheating.

176
00:26:57,920 --> 00:27:00,992
Mustafa gave me the tip,
he'll take overthe place.

177
00:27:01,200 --> 00:27:04,510
Will you take care of
the termination papers tonight?

178
00:27:06,120 --> 00:27:08,793
See you later, I love you.

179
00:27:55,200 --> 00:27:56,633
Come on.

180
00:28:00,400 --> 00:28:02,038
Come on, Thomas.

181
00:28:08,880 --> 00:28:10,996
Laura, come on.

182
00:28:11,160 --> 00:28:13,230
You dance like a Greek.

183
00:28:14,960 --> 00:28:17,713
How'd you know,
how a fucking Greek dances?

184
00:28:17,920 --> 00:28:20,150
From Zorba-the-Greek.

185
00:28:21,840 --> 00:28:23,478
Assholes.

186
00:28:43,520 --> 00:28:46,318
Show me how the Germans dance.

187
00:28:46,560 --> 00:28:47,470
Come.

188
00:28:47,640 --> 00:28:50,279
Come on, Laura.

189
00:28:59,120 --> 00:29:01,759
Come on German Thomas, dance.

190
00:29:04,400 --> 00:29:07,119
Show me how the Germans dance.

191
00:29:32,920 --> 00:29:34,797
Put your hand here.

192
00:29:36,200 --> 00:29:39,670
Relaxed, not so stiff...

193
00:29:39,880 --> 00:29:41,393
...and listen.

194
00:29:50,200 --> 00:29:51,838
You can do it.

195
00:29:56,600 --> 00:29:58,477
Keep dancing.

196
00:30:02,160 --> 00:30:03,957
I'll be right back.

197
00:31:02,960 --> 00:31:04,951
Damn, a nice friend you are.

198
00:31:05,160 --> 00:31:08,038
Why don't you help him,
he's completely plastered.

199
00:31:57,920 --> 00:31:59,751
Give me your hand.

200
00:33:27,280 --> 00:33:29,077
I'll take care of him.

201
00:33:34,240 --> 00:33:36,470
The key is over here.

202
00:33:47,080 --> 00:33:49,275
You can take the car.

203
00:33:51,880 --> 00:33:53,233
Thanks.

204
00:33:54,000 --> 00:33:56,639
See you tomorrow.
- Yes, tomorrow.

205
00:35:40,040 --> 00:35:41,109
Did I oversleep?

206
00:35:41,280 --> 00:35:43,157
Do you have coffee?

207
00:35:50,080 --> 00:35:51,957
One minute...
- I'll make it.

208
00:36:08,880 --> 00:36:11,348
I made one foryou too.
- Thanks.

209
00:36:14,440 --> 00:36:16,476
Did you hurt yourself?

210
00:36:19,000 --> 00:36:21,514
I sanded down the doors yesterday.

211
00:36:22,080 --> 00:36:24,594
Last night?
- I couldn't sleep.

212
00:36:27,800 --> 00:36:30,109
That's a damn long stretch.

213
00:36:30,280 --> 00:36:34,273
Why didn't you take the car?
- Want me to lose my license too?

214
00:36:38,440 --> 00:36:41,193
Thomas, you can help me out.

215
00:36:41,520 --> 00:36:43,397
I have to leave on Thursday
for a few days.

216
00:36:43,640 --> 00:36:46,279
I'll show you the ropes and
you take over while I'm gone.

217
00:36:46,760 --> 00:36:49,069
Of course you'll earn more.

218
00:36:50,240 --> 00:36:53,277
I brought property in Turkey
for me and Laura.

219
00:36:53,800 --> 00:36:57,509
In Arslankoy,
in the Taurus mountains.

220
00:36:58,520 --> 00:37:01,796
I have to go there,
talk to the architect, etc.

221
00:37:02,040 --> 00:37:03,871
A vacation house?

222
00:37:04,560 --> 00:37:08,712
No, it's going to be forever.

223
00:37:10,640 --> 00:37:12,756
Do you have a cigarette?

224
00:37:20,920 --> 00:37:22,035
Thanks.

225
00:37:30,160 --> 00:37:31,673
Pretty funny.

226
00:37:37,400 --> 00:37:39,356
What will you do there?

227
00:37:40,000 --> 00:37:41,558
I was born there.

228
00:37:41,760 --> 00:37:43,716
I was two when we left.

229
00:37:47,720 --> 00:37:51,110
Six months ago
I buried my fatherthere.

230
00:37:52,360 --> 00:37:55,079
He wanted to be buried there.

231
00:38:00,000 --> 00:38:01,877
First time that I returned
to Turkey.

232
00:38:02,040 --> 00:38:04,952
Fucking military draft,
I couldn't be buggered with that.

233
00:38:08,560 --> 00:38:11,279
Have you ever seen
orange trees blossoming?

234
00:38:12,160 --> 00:38:14,037
It's unbelievable.

235
00:38:14,960 --> 00:38:16,552
Nice.

236
00:38:17,120 --> 00:38:19,588
What street is this here?

237
00:38:22,000 --> 00:38:24,150
Friedrich-Engels Damm.

238
00:38:26,400 --> 00:38:27,992
Ökzan here.

239
00:38:28,280 --> 00:38:31,078
I need a taxi at
Friedrich-Engels Damm.

240
00:38:31,600 --> 00:38:33,397
Which number?

241
00:38:33,640 --> 00:38:34,914
One.

242
00:38:56,320 --> 00:38:59,471
There's no more wine.
The miniatures are finished too.

243
00:38:59,680 --> 00:39:02,990
We just got a delivery last week.
- I'll check again.

244
00:39:27,080 --> 00:39:28,877
Find any?

245
00:39:29,640 --> 00:39:33,315
Nope. We have to order more.

246
00:39:36,400 --> 00:39:38,914
The receipts. Here.

247
00:39:39,760 --> 00:39:42,228
The change. You count it,
enterthe sum here.

248
00:39:42,640 --> 00:39:44,631
Give it all to Laura in the evening.

249
00:39:44,840 --> 00:39:47,832
She goes to our suppliers
and makes purchases.

250
00:39:49,320 --> 00:39:50,878
Check the kitchens.

251
00:39:51,000 --> 00:39:54,629
The health department pigs
have their eyes on me.

252
00:39:57,520 --> 00:40:01,354
Here are the numbers from last week.
Look at this guy, Chien Tháng.

253
00:40:01,640 --> 00:40:05,872
Each week a little less,
bit by bit.

254
00:40:06,280 --> 00:40:08,714
He's giving it a try.
- Trying what?

255
00:40:08,920 --> 00:40:12,549
Look, 14th week: 1,286 Euro.

256
00:40:12,760 --> 00:40:15,718
15th week: 1,167 and
the weather was good.

257
00:40:15,920 --> 00:40:20,118
Drinks alone make you a thousand.
- Got it.

258
00:40:20,760 --> 00:40:22,716
Are you hungry?

259
00:40:23,400 --> 00:40:26,597
We'll drive there and you order
something to eat...

260
00:40:26,800 --> 00:40:29,189
...something expensive.

261
00:40:29,440 --> 00:40:32,716
When you pay, see if he
enters it in the register...

262
00:40:32,920 --> 00:40:36,629
...or just gives you change
with no receipt.

263
00:40:42,640 --> 00:40:45,473
Take away, or eat here?
- Take out, please.

264
00:41:12,720 --> 00:41:15,314
Something to drink?
- Two colas.

265
00:41:15,520 --> 00:41:17,112
Two colas.

266
00:41:21,400 --> 00:41:23,994
Makes 13.50.

267
00:41:26,080 --> 00:41:27,479
Thanks.

268
00:41:29,320 --> 00:41:30,548
Don't do that.

269
00:41:34,760 --> 00:41:37,911
Sorry, don't say anything.

270
00:41:46,040 --> 00:41:49,589
Here's the change, 6.50.
Please say nothing.

271
00:41:51,200 --> 00:41:52,952
Did he beg?

272
00:41:53,760 --> 00:41:56,672
"Please say nothing,
please say nothing."

273
00:41:59,760 --> 00:42:01,830
You have a good heart.

274
00:42:06,120 --> 00:42:09,032
I have one too, that's the problem.

275
00:42:16,280 --> 00:42:19,158
Next time, you stir-fry that guy.

276
00:42:34,960 --> 00:42:36,632
Thomas.

277
00:42:38,720 --> 00:42:40,950
Keep the carfrom now on.

278
00:42:41,680 --> 00:42:42,999
Thanks.

279
00:42:43,800 --> 00:42:45,028
Ciao.

280
00:42:46,240 --> 00:42:47,912
See you tomorrow.

281
00:42:52,440 --> 00:42:54,476
Wait, I'll carry it.

282
00:42:54,680 --> 00:42:56,398
Everything okay?

283
00:44:07,680 --> 00:44:10,353
What was that?
- Some kind of animal maybe.

284
00:44:12,760 --> 00:44:14,591
Where are you going?

285
00:44:19,960 --> 00:44:22,633
Probably that damn raccoon.

286
00:44:25,560 --> 00:44:27,949
It was back there.
- Where?

287
00:44:42,720 --> 00:44:45,075
He's gone, fucking beast.

288
00:44:56,720 --> 00:45:00,269
What are you doing?
- Nothing, I thought I heard something.

289
00:45:02,160 --> 00:45:04,116
Careful overthere.

290
00:45:20,640 --> 00:45:23,154
Can you drive me
to the airport tomorrow?

291
00:45:24,360 --> 00:45:25,918
Of course.

292
00:46:07,200 --> 00:46:08,713
I'm okay.

293
00:46:15,760 --> 00:46:16,954
Shit...

294
00:46:40,000 --> 00:46:41,718
Are you okay?

295
00:46:42,160 --> 00:46:43,513
Yes.

296
00:46:44,000 --> 00:46:45,592
Let's go.

297
00:46:52,680 --> 00:46:54,238
Turn it off.

298
00:47:04,520 --> 00:47:07,159
The discount store in Tangermünde
is going to close.

299
00:47:08,040 --> 00:47:11,919
We have to find a new location
forthe snack-barthere.

300
00:47:12,720 --> 00:47:15,154
What do you think about this spot?

301
00:47:19,720 --> 00:47:23,952
There's no discount store here either.
- Still it's a hot-spot.

302
00:47:24,160 --> 00:47:26,879
Where would you put a snack-bar?

303
00:47:27,280 --> 00:47:30,033
Are you serious?
- Sure.

304
00:47:37,440 --> 00:47:38,555
Overthere.

305
00:47:38,760 --> 00:47:41,115
At the bus-stop?

306
00:47:41,920 --> 00:47:43,035
Nope.

307
00:47:44,080 --> 00:47:46,355
The most traffic is overthere.

308
00:47:46,760 --> 00:47:49,399
What time is it now?
- Just before eight.

309
00:47:49,640 --> 00:47:52,200
So people are going to work.

310
00:47:52,400 --> 00:47:54,868
Who stops to eat or drink then?

311
00:47:55,040 --> 00:47:57,713
We have to be on the side
where they return from work.

312
00:47:57,920 --> 00:48:01,151
On the left side.
- I'm convinced.

313
00:48:01,360 --> 00:48:03,749
In front or behind the traffic light?

314
00:48:04,320 --> 00:48:06,834
Behind it.
- Why?

315
00:48:07,160 --> 00:48:08,832
Instinct.

316
00:48:10,960 --> 00:48:12,473
There's Laura.

317
00:48:15,080 --> 00:48:17,116
That's her, right?

318
00:48:17,400 --> 00:48:19,391
I can't tell...

319
00:48:23,680 --> 00:48:25,716
Shit!

320
00:48:26,440 --> 00:48:28,670
Follow her!
- What?

321
00:48:28,880 --> 00:48:30,950
Do it, drive!

322
00:48:33,720 --> 00:48:35,358
Go!

323
00:48:47,360 --> 00:48:49,794
Don't lose her.

324
00:48:57,160 --> 00:48:58,991
Slow down, slow.

325
00:48:59,560 --> 00:49:01,994
She shouldn't see us.

326
00:49:05,680 --> 00:49:08,274
What the hell are we doing?

327
00:49:14,000 --> 00:49:15,672
Keep driving.

328
00:49:17,080 --> 00:49:19,674
Take the second right.

329
00:50:20,720 --> 00:50:22,153
Let's go.

330
00:50:25,840 --> 00:50:27,592
You complete idiot!

331
00:50:45,600 --> 00:50:47,238
Shit...

332
00:51:50,440 --> 00:51:53,989
Listen...
- Enjoyed yourself, feel better now?

333
00:51:56,320 --> 00:51:58,914
How long has it been going on?

334
00:52:01,520 --> 00:52:03,715
Been fucking him a long time?

335
00:52:12,280 --> 00:52:13,633
Why?

336
00:52:16,560 --> 00:52:17,629
Why?

337
00:52:17,800 --> 00:52:19,756
Fuck yourself!

338
00:53:00,120 --> 00:53:03,510
I cheated on you
but not like you think.

339
00:53:05,880 --> 00:53:08,314
I made a deal with him.

340
00:53:09,520 --> 00:53:13,593
He charges more
and we split the difference.

341
00:53:15,360 --> 00:53:17,476
Why did you do it?

342
00:53:21,440 --> 00:53:22,998
Why?

343
00:53:28,800 --> 00:53:31,792
First he wanted me,
but got absolutely nothing.

344
00:53:32,000 --> 00:53:34,468
I was just afterthe money.

345
00:53:35,440 --> 00:53:38,318
You think that's not so bad?
- It isn't!

346
00:53:43,360 --> 00:53:45,828
How much have you put aside?

347
00:53:46,000 --> 00:53:46,989
3,450.

348
00:53:47,200 --> 00:53:51,671
How much?
- 3,450. He got the same.

349
00:53:55,920 --> 00:53:58,229
How long has it been going on?

350
00:53:59,680 --> 00:54:01,511
Since December.

351
00:54:05,080 --> 00:54:07,355
Where did you put the money?

352
00:54:10,440 --> 00:54:11,953
Overthere.

353
00:54:22,720 --> 00:54:23,948
Come.

354
00:55:42,280 --> 00:55:44,077
We made up.

355
00:55:50,680 --> 00:55:52,830
Can you handle everything?

356
00:55:57,520 --> 00:55:58,839
Yes.

357
00:56:27,960 --> 00:56:29,632
I can take that myself.

358
00:56:29,840 --> 00:56:31,159
Thanks.

359
00:56:32,680 --> 00:56:34,989
You get going with the deliveries.

360
00:57:47,440 --> 00:57:49,874
I made us something to eat.

361
00:57:51,320 --> 00:57:53,470
Where are you going?

362
00:57:55,200 --> 00:57:57,077
What's wrong?

363
00:58:02,640 --> 00:58:06,076
Turn it off.
Where are you going?

364
00:58:06,840 --> 00:58:08,796
How often did you fuck him?

365
00:58:09,000 --> 00:58:10,479
With Ali?

366
00:58:12,960 --> 00:58:16,350
You don't seriously mean
that beverage dealer?

367
00:58:19,720 --> 00:58:20,914
You're jealous.

368
00:58:21,080 --> 00:58:24,516
Don't give me that shit.

369
00:59:09,600 --> 00:59:11,955
Do you have some dry clothes?

370
00:59:24,000 --> 00:59:25,228
Here.

371
00:59:35,960 --> 00:59:38,520
Why didn't you drive?

372
00:59:39,560 --> 00:59:43,758
If the damn neighbors notice,
Ali will hear about it in no time.

373
00:59:48,840 --> 00:59:50,353
Laura...

374
00:59:52,800 --> 00:59:54,677
I'm an idiot.

375
01:00:01,040 --> 01:00:03,156
What's wrong?
- No!

376
01:00:04,080 --> 01:00:06,355
What?
- Nothing!

377
01:00:48,320 --> 01:00:51,357
Lucy, come here, little Lucy.

378
01:00:51,600 --> 01:00:54,239
Come to mama.

379
01:01:28,000 --> 01:01:30,594
I made you one too.

380
01:01:54,560 --> 01:01:56,118
Morning.

381
01:01:57,560 --> 01:01:59,869
I have to tell you something.

382
01:02:10,120 --> 01:02:13,032
I did two years of prison.

383
01:02:13,240 --> 01:02:16,198
I have 142,000 Euro in debts.

384
01:02:16,400 --> 01:02:18,789
I'm 34 years old.

385
01:02:24,000 --> 01:02:27,959
When I got out of prison
I started working.

386
01:02:28,280 --> 01:02:30,794
In a hot-food stall.

387
01:02:31,200 --> 01:02:33,395
Then in a bar.

388
01:02:36,240 --> 01:02:39,277
That wasn't good.

389
01:02:42,680 --> 01:02:45,353
I met Ali in the bar.

390
01:02:45,680 --> 01:02:48,717
He got me out of there.

391
01:02:49,000 --> 01:02:50,877
Married me.

392
01:02:53,120 --> 01:02:54,792
He had lots of money.

393
01:02:55,000 --> 01:02:58,470
He was the first guy I met
who was respectable.

394
01:03:24,000 --> 01:03:26,195
You call that respectable?

395
01:03:29,680 --> 01:03:31,671
Can you get the car?

396
01:03:33,200 --> 01:03:34,599
Why?

397
01:03:35,040 --> 01:03:37,076
Because of the neighbors.

398
01:03:40,200 --> 01:03:42,191
We have to start work.

399
01:03:44,760 --> 01:03:46,990
You want to start work?

400
01:03:48,640 --> 01:03:50,551
Please, Thomas.

401
01:04:18,760 --> 01:04:21,069
We're not going to work.

402
01:04:23,840 --> 01:04:25,637
We're picking up yourthings.

403
01:04:30,000 --> 01:04:31,956
Say something!

404
01:04:33,440 --> 01:04:34,714
It's impossible.

405
01:04:34,920 --> 01:04:35,989
What?

406
01:04:36,600 --> 01:04:37,999
He beats you.

407
01:04:38,240 --> 01:04:40,071
He beats you!

408
01:04:41,000 --> 01:04:43,195
You didn't listen to me.

409
01:04:49,200 --> 01:04:51,839
Who do you owe money to?
- What?

410
01:04:53,560 --> 01:04:54,834
To him?

411
01:04:56,040 --> 01:04:57,598
Your debts.

412
01:04:58,040 --> 01:05:01,191
No, I owe it to the bank.
Ali took overthe debts.

413
01:05:01,400 --> 01:05:03,675
Then everything is fine.
- Nothing is fine.

414
01:05:03,880 --> 01:05:07,919
He's rich and stuck with your debts.
- He's rich and he's not dumb.

415
01:05:09,840 --> 01:05:12,593
I don't get it, can you explain?

416
01:05:14,160 --> 01:05:16,230
We made a deal.

417
01:05:16,720 --> 01:05:19,075
There's a pre-nuptial agreement.

418
01:05:19,800 --> 01:05:22,553
If I leave him I get nothing...

419
01:05:22,760 --> 01:05:25,228
...except my debts back.

420
01:05:37,160 --> 01:05:38,593
You see?

421
01:05:40,200 --> 01:05:42,270
Now you've clammed up.

422
01:05:43,000 --> 01:05:46,515
Always the same, the savior
shuts up and gives up.

423
01:05:46,720 --> 01:05:48,836
Stop talking crap, Laura!

424
01:05:49,000 --> 01:05:50,877
If it's free you heroes
are all gung-ho...

425
01:05:51,040 --> 01:05:52,712
Stop it!

426
01:05:59,720 --> 01:06:01,039
I love you, Laura.

427
01:06:06,760 --> 01:06:10,833
Just look at yourself.
You live like a bum.

428
01:06:39,720 --> 01:06:40,948
Laura!

429
01:06:41,400 --> 01:06:43,709
I think you should quit work.

430
01:09:10,200 --> 01:09:11,679
Thomas?

431
01:09:14,840 --> 01:09:16,114
Thomas!

432
01:09:21,040 --> 01:09:22,519
Thomas?

433
01:09:39,640 --> 01:09:41,198
Come!

434
01:09:58,280 --> 01:09:59,599
Laura.

435
01:10:08,880 --> 01:10:12,839
You can't love,
if you don't have money.

436
01:10:13,000 --> 01:10:16,959
That's something I know.

437
01:10:31,200 --> 01:10:34,112
He should nevertouch me again.

438
01:10:45,720 --> 01:10:48,518
Why didn't you let him fall?

439
01:10:57,600 --> 01:11:00,068
Why didn't you let him fall?

440
01:11:33,680 --> 01:11:36,672
He got right behind the wheel.

441
01:11:37,560 --> 01:11:41,951
I put the picnic basket in back,
he had already started the car.

442
01:11:43,040 --> 01:11:48,637
I always hated his impatience,
and I get a face full of exhaust.

443
01:11:50,440 --> 01:11:54,672
Suddenly the car lurched forwards
and overthe cliff.

444
01:11:55,280 --> 01:11:58,795
I saw the explosion, a burst of flame.

445
01:12:00,000 --> 01:12:01,831
I ran to the road.

446
01:12:02,000 --> 01:12:04,878
I tried to flag down a car.

447
01:12:06,200 --> 01:12:09,829
I stood at the road,
screaming and waving...

448
01:12:10,000 --> 01:12:12,719
...until finally someone stopped.

449
01:12:23,560 --> 01:12:24,675
Come.

450
01:12:33,680 --> 01:12:35,352
I'll hide here.

451
01:12:39,840 --> 01:12:42,752
You go with him to the sea
and wait until he's drunk.

452
01:12:42,960 --> 01:12:44,871
You both come back and
I'll knock him out...

453
01:12:45,040 --> 01:12:49,272
...put him behind the wheel,
shift into "D" and close the door.

454
01:12:55,880 --> 01:12:58,713
There's a train station 10 km away.

455
01:13:47,320 --> 01:13:49,390
You've landed already.

456
01:13:55,280 --> 01:13:56,395
I'll take that.
- It's okay.

457
01:13:56,640 --> 01:13:59,916
You can get in...
- I'll put it in the back.

458
01:14:02,920 --> 01:14:05,753
It was supposed to be a surprise.

459
01:14:07,720 --> 01:14:10,678
I want to go to the sea with you.

460
01:14:13,000 --> 01:14:14,194
Sorry.

461
01:14:41,400 --> 01:14:43,470
How's your brother?

462
01:14:47,400 --> 01:14:48,958
Just fine.

463
01:14:52,560 --> 01:14:54,073
And the children?

464
01:14:56,840 --> 01:14:58,239
Fine.

465
01:15:08,600 --> 01:15:11,353
The harvest must have started.

466
01:15:20,200 --> 01:15:21,474
Ali?

467
01:16:41,080 --> 01:16:43,071
I'm so scared of dying.

468
01:16:47,960 --> 01:16:50,235
I wasn't in Turkey.

469
01:16:54,160 --> 01:16:58,073
I was in Leipzig, in a clinic.

470
01:17:00,000 --> 01:17:03,470
My heartbeat is irregular.

471
01:17:04,720 --> 01:17:06,836
It's been like that for six months.

472
01:17:09,240 --> 01:17:13,358
The heart specialists tried,
it was my big chance.

473
01:17:14,560 --> 01:17:16,790
But it didn't work.

474
01:17:24,600 --> 01:17:27,478
I've got two months to live, Laura.

475
01:17:27,920 --> 01:17:29,558
Maybe three.

476
01:17:39,680 --> 01:17:43,798
They leave you lying there after
the diagnosis, imagine that?

477
01:17:45,280 --> 01:17:47,510
You just lie there.

478
01:17:52,600 --> 01:17:55,672
I live in a country
that doesn't want me.

479
01:17:57,000 --> 01:17:59,195
With a woman...

480
01:17:59,400 --> 01:18:01,595
...that I bought.

481
01:18:07,640 --> 01:18:11,269
In the clinic
I only thought of you.

482
01:18:20,560 --> 01:18:22,949
Can you stay with me?

483
01:19:05,200 --> 01:19:07,873
I forgot to lock the car.

484
01:19:34,000 --> 01:19:36,468
What's wrong?
- Impossible, we can't do it.

485
01:19:36,720 --> 01:19:37,914
Go home.
- Laura?

486
01:19:38,080 --> 01:19:39,513
I'll explain later.

487
01:19:53,600 --> 01:19:57,513
You have to send my brother
2,000 a month.

488
01:19:58,080 --> 01:20:00,958
Otherwise he can't keep up his farm.

489
01:20:03,760 --> 01:20:06,194
I'll put the company in your name.

490
01:20:06,360 --> 01:20:09,352
We'll do that next week,
at the public notary.

491
01:20:10,960 --> 01:20:14,077
We'll take care of your debts too,
of course.

492
01:20:20,120 --> 01:20:22,395
Thomas will help you.

493
01:20:23,160 --> 01:20:24,957
He's okay.

494
01:20:43,320 --> 01:20:46,073
I'll be right back.
- Where are you going?

495
01:20:46,760 --> 01:20:49,069
I'll get us something to drink.

496
01:22:27,600 --> 01:22:28,919
Ali?

497
01:22:31,000 --> 01:22:32,752
What's wrong?

498
01:22:39,360 --> 01:22:41,351
What's wrong?

499
01:22:45,840 --> 01:22:47,751
Do you want to drive home?

500
01:23:15,000 --> 01:23:16,592
Come out.

501
01:23:22,920 --> 01:23:24,148
Thomas!

502
01:23:27,840 --> 01:23:29,478
Thomas!

503
01:23:55,000 --> 01:23:56,877
Get out of here.

504
01:24:00,760 --> 01:24:02,318
Get lost.

505
01:24:23,880 --> 01:24:25,393
Pigs!

506
01:24:27,680 --> 01:24:29,511
You pigs!

507
01:25:39,200 --> 01:25:40,349
Ali...

