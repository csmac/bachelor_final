﻿1
00:00:07,300 --> 00:00:11,240
<i>Kurusu Rena... Someday, she has a VISION, </i>

2
00:00:11,240 --> 00:00:15,180
<i>an ability to see the reflection of the murderer.</i>

3
00:00:15,180 --> 00:00:17,180
Christina!

4
00:00:17,180 --> 00:00:21,630
<i>They keep calling Kurusu Rena with "Christina" when they kill people. </i>

5
00:00:21,630 --> 00:00:23,300
I can't kill three persons!

6
00:00:23,300 --> 00:00:27,400
<i>Asano Kazuma, a detective on his off-duty period, and Rena has draw near into the mystery of the VISION.</i>

7
00:00:30,910 --> 00:00:32,910
What is this?

8
00:01:03,300 --> 00:01:05,870
Chapter 5. Body contact

9
00:02:11,880 --> 00:02:14,880
Christina...

10
00:02:25,670 --> 00:02:30,700
Thanks everyone!

11
00:02:30,700 --> 00:02:33,650
Well… This is the best! 

12
00:02:33,650 --> 00:02:36,640
- Ah Mr. Niitsu, thanks for your hard work! 
- Thanks for your hard work.

13
00:02:36,640 --> 00:02:39,370
That was great, right Yukino? 

14
00:02:39,370 --> 00:02:42,210
I had a lot of fun.

15
00:02:42,210 --> 00:02:45,910
I'm glad that I can get a chance to perform on stage and I think it's not that bad.

16
00:02:45,910 --> 00:02:48,980
- Hey Rena, don't you think so?
 - How if you also become a member of group idol? 

17
00:02:48,980 --> 00:02:52,770
Well…  It will look nice with the three of you.

18
00:02:52,770 --> 00:02:56,040
Should we make a new group with the three of you? 

19
00:02:56,040 --> 00:02:58,410
- Then, I'll be the leader!
 - Eh!?

20
00:02:58,410 --> 00:03:02,110
I don't want to do it next time.
 If you still want to do it, then you two do it. 

21
00:03:09,800 --> 00:03:12,670
Oh! So it was finished. Thanks for your hard work. 

22
00:03:12,670 --> 00:03:15,310
- Thanks for your hard work. 
 - You look good with the costume.

23
00:03:15,310 --> 00:03:18,510
- What?
- You never change the displeasure face expressions of yours.

24
00:03:18,510 --> 00:03:21,710
You are always easy to get pissed off.

25
00:03:21,710 --> 00:03:24,870
You never care the important things, do you?

26
00:03:24,870 --> 00:03:28,500
What are you doing now is a good thing. 

27
00:03:28,500 --> 00:03:32,670
Even you object it now, it was happened, wasn't it?

28
00:03:32,670 --> 00:03:36,510
Of course, I know that it's not my decision if you are going to do it or not. 

29
00:03:36,510 --> 00:03:41,520
Rena… I'm your ally, right?

30
00:03:41,520 --> 00:03:46,520
That's why… Do this job, ok? This is a good job. 

31
00:03:46,520 --> 00:03:50,040
That talks again. 

32
00:03:50,040 --> 00:03:53,280
Rena, please believe in me. 

33
00:03:53,280 --> 00:03:55,880
I have resolution for the job.

34
00:04:12,010 --> 00:04:14,000
What's with you?

35
00:05:28,910 --> 00:05:33,010
Note: freeter in Japanese: someone in their 20's who is moving from job to job, mainly part time jobs which pay small wages.
The murder scene is in here. The victim is 27 years old, 

36
00:05:33,010 --> 00:05:35,950
a freeter who always chase after the group idol.
Note: freeter in Japanese: someone in their 20's who is moving from job to job, mainly part time jobs which pay small wages.

37
00:05:35,950 --> 00:05:38,770
Both his room and his computer full with the idol pictures.

38
00:05:38,770 --> 00:05:43,740
We still analyze the computer now. 

39
00:05:43,740 --> 00:05:46,340
He attached this thing on the computer screen.

40
00:05:46,340 --> 00:05:51,910
It was written on here. 
This is Kazuma's job. 

41
00:05:51,910 --> 00:05:53,900
Then, what about this mark?

42
00:05:53,900 --> 00:05:57,200
The smartphone was dropped one kilometer from the murder scene. 

43
00:05:57,200 --> 00:05:59,340
Look at the display. 
- This is Rena.

44
00:05:59,340 --> 00:06:03,070
What is she doing? 
 - Last night, she was appeared on live performance as pinch hitter.

45
00:06:03,070 --> 00:06:06,210
Ahhh~ 
 But the pictures on that time, 

46
00:06:06,210 --> 00:06:08,310
didn't take by the smartphone's owner.

47
00:06:08,310 --> 00:06:10,320
It seems like he also picked up the pictures from Internet. 

48
00:06:10,320 --> 00:06:15,640
This is the bloodstains left on the murder scene, 
and this is where there was a tire track suddenly brakes.

49
00:06:15,640 --> 00:06:17,640
In other words?
- In other words?

50
00:06:17,640 --> 00:06:20,840
Why am I always the one who give all the explanation? 

51
00:06:20,840 --> 00:06:25,280
In other words, he killed the victim in here. 
 The murderer was in a hurry escaped with a car, 

52
00:06:25,280 --> 00:06:28,570
he was driving too fast, and he hit someone in here.

53
00:06:28,570 --> 00:06:32,640
Maybe he was too scared to get caught so he escaped and take the corpse in his car. 

54
00:06:32,640 --> 00:06:34,740
At that time, his smartphone was dropped.

55
00:06:34,740 --> 00:06:38,140
Ah I see.
Just "I see"?

56
00:06:38,140 --> 00:06:41,410
You should give your opinion, a detective on his off-duty period, Asano Kazuma!

57
00:06:41,410 --> 00:06:44,070
Is the smartphone's owner discovered?
 - He still can't be found.

58
00:06:44,070 --> 00:06:50,170
How about the data inside his computer?
 - It is also full with group idol's pictures.

59
00:06:50,170 --> 00:06:55,310
However, there are no pictures of him and also no communication traces.

60
00:06:55,310 --> 00:06:57,410
We still investigate it now.

61
00:06:57,410 --> 00:06:59,980
It means we still can't find any clues about him.

62
00:06:59,980 --> 00:07:02,970
That's right.

63
00:07:02,970 --> 00:07:08,340
Listen! The murderer wanted to dedicate himself to Christina, 

64
00:07:08,340 --> 00:07:10,780
and he killed two persons already.

65
00:07:10,780 --> 00:07:16,350
One more person… I'm sure he has one more target already, but who is his next target?

66
00:07:16,350 --> 00:07:22,000
Why did the murderers take the corpse in his car purposely?

67
00:07:22,000 --> 00:07:26,440
Err… Revealing…
- Why?

68
00:07:26,440 --> 00:07:29,640
Hey, why is it? - Err… 
 Why? Why? - Hey!

69
00:07:29,640 --> 00:07:33,520
Why are you questioning and why am I only answering?

70
00:07:33,520 --> 00:07:35,900
How can you do that? If I don't know about those things, 

71
00:07:35,900 --> 00:07:38,870
then you should think about it too, Idiot!

72
00:07:38,870 --> 00:07:44,540
I understand now, you don't know anything. 

73
00:07:44,540 --> 00:07:46,910
I think I got criticize. 

74
00:07:46,910 --> 00:07:48,910
Are you awfully on rebellious period today?

75
00:07:48,910 --> 00:07:51,570
Hee… Since I'm done with the toilet, I'm going to Rena's place.

76
00:07:51,570 --> 00:07:54,600
If I get something, I will report.
- Kurusu Rena is going to shooting location in countryside.  

77
00:07:54,600 --> 00:07:56,300
Shooting location in countryside?

78
00:08:05,510 --> 00:08:07,500
Let me hold your bag.
- Eh? Why?

79
00:08:07,500 --> 00:08:09,500
Just let me hold it.
- What's for!?

80
00:08:09,500 --> 00:08:12,140
While working, you must put your effort in it. 
 I only want you to concentrate in your work.

81
00:08:12,140 --> 00:08:14,440
That's good, right? 
- What? 

82
00:08:16,440 --> 00:08:23,410
Hey! As I said before, what is the most necessary thing we do in this place?

83
00:08:23,410 --> 00:08:25,410
What is it?

84
00:08:28,420 --> 00:08:30,470
Yes, it is for job.

85
00:08:30,470 --> 00:08:36,330
Finally, you got it. 
I'm relieved.

86
00:08:36,330 --> 00:08:39,580
Ah! Today is just a minor work but the photographer is really famous.

87
00:08:39,580 --> 00:08:43,270
He is young but he is known as the best first class photographer. 

88
00:08:43,270 --> 00:08:45,800
Heeee…
- Don't just "Heeee…"

89
00:08:45,800 --> 00:08:47,810
Ah! Good morning!
 Good morning!

90
00:08:47,810 --> 00:08:52,580
Good morning!
- I am looking forward to work with you. 
 - I ask your favor.

91
00:08:52,580 --> 00:08:58,570
So, if it's success, this will be a gate to major work. 
 I hope you understand.

92
00:08:58,570 --> 00:09:01,470
Note: CM is Japanese TV commercial.
It will looks like we are going to shoot CM on Tokyo, 

93
00:09:01,470 --> 00:09:03,740
need going to overseas suddenly, eating delicious food.

94
00:09:03,740 --> 00:09:05,740
In spare time of photographing we are playing, 

95
00:09:05,740 --> 00:09:08,280
and when we are back, we will receive a lot of fee. 

96
00:09:08,280 --> 00:09:12,310
Mr. Kiyosue's dream is so small. 
- How come is it a small dream? 

97
00:09:12,310 --> 00:09:15,600
Good morning!
- Good morning.
 - I'm looking forward to work with you.

98
00:09:20,000 --> 00:09:22,840
Hey! Is that person a photographer?

99
00:09:22,840 --> 00:09:25,940
Eh where?
- There.

100
00:09:25,940 --> 00:09:28,910
- Let's go
 - Ok.

101
00:09:36,250 --> 00:09:38,240
Hello~

102
00:09:38,240 --> 00:09:40,240
<i>Eh? Mr. Kiyosue?</i>

103
00:09:40,240 --> 00:09:43,140
Rena is in preparation to do an important work.

104
00:09:43,140 --> 00:09:45,780
Now, I'm heading there.

105
00:09:45,780 --> 00:09:47,770
Can you tell that to Rena?

106
00:09:47,770 --> 00:09:53,540
I heard there was a case. 
 Because of that, I was conveyed to Mr. Sakisaka,

107
00:09:53,540 --> 00:09:58,880
that I want him to stop coming here and there to investigate Rena without any reason.

108
00:09:58,880 --> 00:10:01,980
<i>He doesn't have a right to do that thing, right?</i>
- Yes, I also think that. 

109
00:10:01,980 --> 00:10:07,340
<i>In every cases happened until now, there is no evidence that Rena and Christina are to be tied together.</i>

110
00:10:07,340 --> 00:10:09,640
This case has not yet solved.

111
00:10:09,640 --> 00:10:11,640
You don't understand what will be happen if we leave such guy alone, right?

112
00:10:11,640 --> 00:10:13,640
Even Mr. Kiyosue was attacked once.

113
00:10:13,640 --> 00:10:17,450
Anyway, cancel it for today. 
 She is doing an important work right now.

114
00:10:17,450 --> 00:10:19,450
I will always be with her.

115
00:10:19,450 --> 00:10:22,400
<i>So don't worry.</i>
- I'm not like that police,

116
00:10:22,400 --> 00:10:25,700
I'm worried about her personally, so I'm going over there.

117
00:10:25,700 --> 00:10:27,710
<i>If it's the reason, you don't have a right to stop me, right?</i>

118
00:10:27,710 --> 00:10:31,110
I have. I really have a right to stop you.
 Bye~

119
00:10:34,410 --> 00:10:36,510
What's with that guy? 

120
00:10:40,540 --> 00:10:44,210
Eh!? Aaaaa!

121
00:10:44,210 --> 00:10:48,280
I've such a bad luck this time.

122
00:10:48,280 --> 00:10:52,150
There is no doubt that freeter's murder is his acquaintance.

123
00:10:52,150 --> 00:10:54,570
We will search those guys who have caused any trouble with the victim.

124
00:10:54,570 --> 00:10:56,470
We should emerge the case by look for the information.

125
00:10:59,370 --> 00:11:01,670
Sakisaka Inspector.
 - What?

126
00:11:01,670 --> 00:11:05,380
How should we report about "Christina" message?

127
00:11:05,380 --> 00:11:07,710
We have no choice besides grasp of the theory. 

128
00:11:07,710 --> 00:11:11,700
The murderer didn't write Kurusu Rena name.
 Isn't it, Mr. Sakisaka?

129
00:11:11,700 --> 00:11:14,170
Note: The X-Files is an American science fiction drama television series. 
Two FBI agents investigate the strange and unexplained while hidden forces work to impede their efforts.
In Japan, there is no such thing like the X-file.

130
00:11:14,170 --> 00:11:18,010
What is that?
- It's ok. No need to talk about it.
Note: The X-Files is an American science fiction drama television series. 
Two FBI agents investigate the strange and unexplained while hidden forces work to impede their efforts.

131
00:11:18,010 --> 00:11:24,050
Akiyama, please go to get some information from her share house mates, Maria and Takami Yukino.

132
00:11:24,050 --> 00:11:31,940
Then if you can, go to Kurusu Rena's room and look her belongings for a while.

133
00:11:31,940 --> 00:11:33,940
Huh!?

134
00:11:33,940 --> 00:11:35,740
By doing that it is counted as an illegal investigation, right? 

135
00:11:37,740 --> 00:11:41,950
It's not, if you just look it for a while. 
- It's locked though.

136
00:11:41,950 --> 00:11:43,750
But it's still possible, right? 

137
00:11:49,000 --> 00:11:51,000
Be careful on your way.
- Yes.

138
00:12:05,140 --> 00:12:08,040
The mail has come. Is it from Christina?

139
00:12:23,140 --> 00:12:25,540
Sorry for waiting.
- Yes.

140
00:12:25,540 --> 00:12:27,540
How is this? 

141
00:12:27,540 --> 00:12:31,280
I'm sorry… Since the main staff will arrive late because of traffic jam, 

142
00:12:31,280 --> 00:12:33,280
can you wait a little longer?

143
00:12:33,280 --> 00:12:36,370
Then please wait here. 

144
00:12:36,370 --> 00:12:38,370
I'm sorry.
- No problem.

145
00:12:49,050 --> 00:12:51,050
Is there someone? 

146
00:12:51,050 --> 00:12:53,370
Are you seen something again? 

147
00:13:04,150 --> 00:13:06,750
Oh! 
- Hey!

148
00:13:09,000 --> 00:13:11,070
What's with this?
- Hey, what are you doing?

149
00:13:11,070 --> 00:13:13,070
Please let go of me.

150
00:13:13,070 --> 00:13:16,210
You are coming here again so you can steal some news! 

151
00:13:16,210 --> 00:13:18,210
I'm sorry. I accidentally near here.

152
00:13:18,210 --> 00:13:20,410
Liar! Hey! What's this camera for?

153
00:13:20,410 --> 00:13:24,350
You come here often to peeping photos and steal it in our photographing schedule.

154
00:13:24,350 --> 00:13:27,300
If you are doing it again next time, I will report it. Remember my word!

155
00:13:27,300 --> 00:13:29,640
Please forgive me.
I won't do it next time. 

156
00:13:29,640 --> 00:13:33,140
Sorry. I'm really sorry.
 I will be careful from now.

157
00:13:40,250 --> 00:13:43,840
<i>You're so careless.</i>

158
00:13:43,840 --> 00:13:45,840
You didn't notice me. 

159
00:13:48,570 --> 00:13:50,580
Are you one of the photographer's staff?

160
00:13:50,580 --> 00:13:54,910
Yes... It seems the main staff hasn't arrived yet.

161
00:13:54,910 --> 00:13:57,450
I'm sorry but can you wait for a little longer? 

162
00:13:57,450 --> 00:13:59,530
I see... I'll wait.

163
00:14:05,510 --> 00:14:08,210
Can I take your picture?

164
00:14:08,210 --> 00:14:10,210
Now?

165
00:14:10,210 --> 00:14:14,010
But if you don't want, let it be.

166
00:14:18,140 --> 00:14:20,300
<i>The photographer is really famous.</i>

167
00:14:20,300 --> 00:14:22,300
<i>He is young but he is known as the best first class photographer. </i>

168
00:14:25,480 --> 00:14:28,780
It's ok with me.

169
00:14:28,780 --> 00:14:30,830
I'm glad. 

170
00:14:30,830 --> 00:14:33,730
Then, let's do it outside.
- Ok.

171
00:15:05,110 --> 00:15:08,510
Are you alright?
 - Yes.

172
00:15:18,840 --> 00:15:22,140
Thank you.
- No problem.

173
00:15:27,340 --> 00:15:31,570
This is the pictures of acquaintance's colleagues with the murder victim.

174
00:15:31,570 --> 00:15:33,830
They might come to live performance several times. 

175
00:15:33,830 --> 00:15:39,030
But I don't know who he is arguing with. 
Is it enough? 

176
00:15:39,030 --> 00:15:46,010
Yes.
- Excuse me.

177
00:15:46,010 --> 00:15:49,830
What is your true purpose? 
- Hmm?

178
00:15:49,830 --> 00:15:53,340
Your purpose to come here.
 Even you are shown these pictures to Yukino, 

179
00:15:53,340 --> 00:15:56,540
it's not that simple to find the murderer.

180
00:15:58,410 --> 00:16:03,680
I ever thought that many Models are stupid, but actually they are not.

181
00:16:03,680 --> 00:16:05,880
Err… anyway…

182
00:16:10,500 --> 00:16:14,010
Can't you enter Ms. Rena's room? 

183
00:16:14,010 --> 00:16:17,310
Do you think I will cooperate with you to do such thing?

184
00:16:19,350 --> 00:16:22,500
We also have an appropriate reason. It's a house searches.

185
00:16:22,500 --> 00:16:27,100
That thing is troublesome.
If you keep silent, you can't remain disagree too.

186
00:16:29,170 --> 00:16:32,670
And I may not be able to proof her innocence.

187
00:16:42,440 --> 00:16:44,440
You're really smart. You made decision really fast. 

188
00:16:44,440 --> 00:16:46,640
The police are really worst.

189
00:17:03,810 --> 00:17:05,810
Ok…

190
00:17:19,270 --> 00:17:23,610
Do they still not come yet? 
- Yes… They still not call you.

191
00:17:23,610 --> 00:17:28,600
So what are you doing in your free time? 
- I do my job. 

192
00:17:28,600 --> 00:17:32,300
It's a little too late. I will ask them.

193
00:17:42,850 --> 00:17:45,380
Why is the call not answered?

194
00:17:47,300 --> 00:17:51,670
I feel like I was being sentimentize.

195
00:17:59,350 --> 00:18:01,680
Sakisaka Inspector.
- Yes?

196
00:18:01,680 --> 00:18:04,830
A little while before the case, seems like there was a big quarrel between those guys who were chased after their idol.

197
00:18:04,830 --> 00:18:06,670
We can't get in touch with those three persons.

198
00:18:06,670 --> 00:18:08,670
Their pictures are here.

199
00:18:08,670 --> 00:18:11,570
Is there a murderer in this picture? 
- Yes, there is a high possibility he is in this picture.

200
00:18:14,680 --> 00:18:16,850
So, which one? 

201
00:18:16,850 --> 00:18:20,670
Let's see…
- "Let's see"?

202
00:18:20,670 --> 00:18:23,740
You are really type of person who have your own opinion without saying it,

203
00:18:23,740 --> 00:18:26,610
you're not obstacle and pleased with your superior, while keep work.

204
00:18:26,610 --> 00:18:28,010
That thing… 

205
00:18:28,010 --> 00:18:32,680
It's Japanese people type, right? 
 Let's go!

206
00:18:43,170 --> 00:18:50,010
Ah! When you come in? I didn't notice at all.

207
00:18:50,010 --> 00:18:55,340
I come in as usual.
- Please don't frighten me.

208
00:18:55,340 --> 00:19:01,040
That's right, I'm sorry.
- No need to apologize.

209
00:19:06,350 --> 00:19:10,330
Have you ever had a feeling of loneliness before?

210
00:19:10,330 --> 00:19:14,730
Eh?
- Even you're not alone but you feel lonely.

211
00:19:19,340 --> 00:19:21,340
I'm sorry for asking strange question.

212
00:19:24,680 --> 00:19:26,680
I felt it before.

213
00:19:28,670 --> 00:19:35,380
I felt loneliness as well, but as usual thing come and pass through, I get used to it.

214
00:19:42,830 --> 00:19:45,340
Geez… Finally, I arrived.

215
00:19:59,330 --> 00:20:02,200
Yes?
- <i>I think I know the murderer.</i>

216
00:20:02,200 --> 00:20:05,170
He left Kurusu Rena's picture in computer. 

217
00:20:05,170 --> 00:20:09,640
That's the last night live performance's photo, 
and it is the same picture in the smartphone which was dropped. 

218
00:20:09,640 --> 00:20:12,010
He's a guy who does this for Christina's sake. 

219
00:20:12,010 --> 00:20:15,300
Did you contact Rena?

220
00:20:15,300 --> 00:20:19,340
No, I didn't… Her manager is Mr. Kiyosue, right? He told me not to contact her.

221
00:20:19,340 --> 00:20:22,340
So you didn't contact her.

222
00:20:22,340 --> 00:20:25,410
Ah! Can you send the murderer's picture?

223
00:20:25,410 --> 00:20:27,410
<i>Yes, I can.</i>

224
00:20:38,510 --> 00:20:40,680
Hey Rena!
- What?

225
00:20:40,680 --> 00:20:45,330
This will be a victory or defeat.
If you can manage it well, you will success after this. 

226
00:20:45,330 --> 00:20:47,500
I'm counting on you, Rena.

227
00:20:47,500 --> 00:20:49,540
Since my future also depend on you.

228
00:20:49,540 --> 00:20:53,840
Are you worried?
- No, no… I'm not worried.

229
00:20:53,840 --> 00:20:56,640
I ask your favor, Ms. Rena.
- Yes.

230
00:21:07,510 --> 00:21:10,170
I ask your favor.
- I ask your favor.

231
00:21:10,170 --> 00:21:14,850
I'm sorry for the lateness. I'm the photographer, Kondo.

232
00:21:14,850 --> 00:21:17,180
Eh?

233
00:21:17,180 --> 00:21:19,180
Did something happen? 

234
00:21:21,170 --> 00:21:24,340
Are you the photographer?

235
00:21:24,340 --> 00:21:26,370
Are there two photographers? 

236
00:21:26,370 --> 00:21:28,380
Huh?

237
00:21:28,380 --> 00:21:30,380
You are the photographer, right? 

238
00:21:30,380 --> 00:21:32,880
No, I'm just an assistant. 

239
00:21:35,000 --> 00:21:38,240
- Christina.
 - Eh?

240
00:21:38,240 --> 00:21:43,840
I can't fulfill your wish anymore.

241
00:21:43,840 --> 00:21:48,840
But... I want to meet you.

242
00:21:51,830 --> 00:21:53,830
No! Stop it!

243
00:21:56,840 --> 00:21:59,670
Rena!
- <i>Are you alright?</i>

244
00:21:59,670 --> 00:22:02,070
Hey! What's happened?

245
00:22:05,010 --> 00:22:08,510
There is someone, right? Is it this man? 

246
00:22:11,500 --> 00:22:15,170
How come?
- So, he is.

247
00:22:15,170 --> 00:22:19,840
That guy is the murderer. 
 - The murderer?

248
00:22:19,840 --> 00:22:27,000
Is he killed someone again?
- Yes.

249
00:22:27,000 --> 00:22:30,500
But it's strange. There is no such thing…

250
00:22:30,500 --> 00:22:34,100
Because this person...

251
00:22:38,010 --> 00:22:40,980
touches me before. 
 - Eh?

252
00:22:40,980 --> 00:22:43,080
This person also holds me.

253
00:22:46,670 --> 00:22:50,510
I ask you to calm down, Rena. You can't continue shooting if you don't calm down! 

254
00:22:50,510 --> 00:22:58,530
Christina.
Please come here.

255
00:23:03,570 --> 00:23:06,510
Where are you going, Rena? You're in the middle of shooting.

256
00:23:06,510 --> 00:23:09,010
He called me. 
That person called me. 

257
00:23:12,510 --> 00:23:16,170
Rena! What's the matter?

258
00:23:16,170 --> 00:23:20,670
He is different from the previous criminal.
- He is trying to tell me something.

259
00:23:20,670 --> 00:23:23,570
Hey! What's happened?

260
00:23:23,570 --> 00:23:26,180
Someone hurry bring her back!

261
00:23:53,140 --> 00:23:57,950
Goodbye Christina.

262
00:24:13,480 --> 00:24:15,480
Rena...

263
00:24:26,140 --> 00:24:30,480
There is something inside…

264
00:24:37,970 --> 00:24:39,970
Who is the owner of this car?

265
00:24:46,210 --> 00:24:48,980
Stop it!
Hey! stop it!

266
00:24:48,980 --> 00:24:51,150
Shut up!
- Stop it!

267
00:24:51,150 --> 00:24:53,150
It's not like what did you think.

268
00:24:55,800 --> 00:24:57,800
Hey!

269
00:25:00,820 --> 00:25:03,040
Ah...

270
00:25:13,280 --> 00:25:18,040
No!

271
00:25:34,810 --> 00:25:38,650
I don't know why it's happened!

272
00:25:38,650 --> 00:25:42,630
I don't know… I really don't know!

273
00:25:42,630 --> 00:25:44,630
Hey wait!

274
00:25:47,000 --> 00:25:49,400
He is attempted hit and run accident, and abandoned the corpse.

275
00:26:01,330 --> 00:26:04,870
Kondo who killed Tanaka using his car, are going to filming location at the hill...

276
00:26:04,870 --> 00:26:07,840
and trying to hide Tanaka's corpse. 

277
00:26:07,840 --> 00:26:09,840
<i>Christina...</i>

278
00:26:09,840 --> 00:26:13,210
He discovered Christina, and killed a man.

279
00:26:13,210 --> 00:26:16,370
Unexpectedly, he got hit by a photographer who has scheduled to shooting Kurusu Rena,

280
00:26:16,370 --> 00:26:18,370
then he died.

281
00:26:20,370 --> 00:26:22,370
That coincidence is amazing.

282
00:26:22,370 --> 00:26:25,540
In this world, that coincidence also happened once in a while.

283
00:26:25,540 --> 00:26:28,550
I have encountered something like that too.
 The murderer who had been missing for five years,

284
00:26:28,550 --> 00:26:33,870
was sleeping next to me in the sauna.

285
00:26:33,870 --> 00:26:38,200
So, did you find something from Rena's room?

286
00:26:38,200 --> 00:26:42,710
Please look at this.

287
00:26:42,710 --> 00:26:45,880
This is Kurusu Rena's picture when she studied abroad in Melbourne.

288
00:26:45,880 --> 00:26:49,370
I have checked it but the pictures are mixed. 

289
00:26:52,200 --> 00:26:56,210
Mixed?
- Yes.

290
00:26:56,210 --> 00:26:58,210
Heee~

291
00:27:13,870 --> 00:27:17,310
There is a person inside the trunk of car.
- Yes.

292
00:27:17,310 --> 00:27:20,510
I'm sure before we discovered his body,

293
00:27:24,470 --> 00:27:26,470
he still alive.

294
00:27:30,710 --> 00:27:34,380
They are starting to hire a new photographer. 

295
00:27:34,380 --> 00:27:37,050
I see.

296
00:27:37,050 --> 00:27:39,870
Let's go back.

297
00:27:39,870 --> 00:27:44,040
Ah! If you don't have a problem, I will send her back. 

298
00:27:44,040 --> 00:27:47,210
Eh?

299
00:27:47,210 --> 00:27:49,540
Let him send me back.

300
00:27:49,540 --> 00:27:53,840
I see… Ok, then.

301
00:27:56,200 --> 00:28:01,700
Hey Rena... I didn't tell you about the case,

302
00:28:01,700 --> 00:28:07,080
because I only want you to do your job well.

303
00:28:07,080 --> 00:28:10,710
I know that.

304
00:28:10,710 --> 00:28:15,500
I see. Then be careful.

305
00:28:15,500 --> 00:28:17,500
Ok.

306
00:28:24,380 --> 00:28:26,380
Should we go now?

307
00:28:29,700 --> 00:28:31,700
Hey.

308
00:28:34,700 --> 00:28:42,710
If something happened, you should come, ok? 

309
00:28:42,710 --> 00:28:48,030
Come to my place, properly. 

310
00:28:48,030 --> 00:28:52,040
Promise me.

311
00:28:52,040 --> 00:28:57,140
By any chance, are you confessing to me?

312
00:28:59,550 --> 00:29:01,880
You are stupid.

313
00:29:01,880 --> 00:29:05,220
Thank you for praising me.

314
00:29:05,220 --> 00:29:07,220
Let's go!

315
00:29:09,210 --> 00:29:14,210
　

