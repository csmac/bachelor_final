1
00:00:35,076 --> 00:00:39,604
Did you see him wave? There he is!

2
00:00:49,390 --> 00:00:51,415
Will you help me up?

3
00:00:51,559 --> 00:00:56,553
lnstead of getting sicker,
he just stayed the same

4
00:00:56,698 --> 00:01:00,532
He didn't get sicker
He didn't get better

5
00:01:02,770 --> 00:01:09,334
They were kind-hearted and thought
he was going out on his own steam

6
00:01:17,285 --> 00:01:22,188
The doc must've come over,
or someone gave him something

7
00:01:22,323 --> 00:01:26,419
Probably some kind of medicine
or something

8
00:01:26,561 --> 00:01:30,759
l could have just took it
and put it in a ditch

9
00:01:32,600 --> 00:01:37,867
Like they do to a horse
They shoot him right away

10
00:02:00,228 --> 00:02:02,219
Buster, fetch!

11
00:02:02,363 --> 00:02:06,026
Whoa Easy

12
00:02:08,136 --> 00:02:10,366
Whoa, pup

13
00:02:11,072 --> 00:02:13,802
Sally

14
00:02:15,443 --> 00:02:19,174
Whoa, pup Sally Sally

15
00:02:23,651 --> 00:02:25,881
Whoa, pup

16
00:02:27,955 --> 00:02:30,924
Whoa, whoa

17
00:02:52,380 --> 00:02:55,474
You seem jumpy today

18
00:02:57,952 --> 00:02:59,977
l'm sick of these stinking birds

19
00:03:00,121 --> 00:03:04,581
- What's the matter?
- You don't wanna know about it

20
00:03:55,843 --> 00:04:00,780
Just as things were about to blow,
this flying circus came in

21
00:04:02,283 --> 00:04:07,084
After six months on this patch,
l needed a breath of fresh air

22
00:04:17,932 --> 00:04:21,197
They were screaming and yelling
and bopping each other

23
00:04:21,335 --> 00:04:26,398
The big one pushed the little one
and said, ''l started You start ''

24
00:04:26,541 --> 00:04:29,601
The little one started in

25
00:04:29,744 --> 00:04:35,341
lf they couldn't think of a good one
to come back with, they'd stop

26
00:04:46,127 --> 00:04:51,463
The little one said, ''l didn't do this ''
The big one said, '' Yes, you did ''

27
00:04:51,599 --> 00:04:53,692
They couldn't sort it out

28
00:04:54,702 --> 00:04:57,500
l wanna give them a hand for this

29
00:05:14,121 --> 00:05:17,579
The devil just sitting there laughing

30
00:05:17,725 --> 00:05:20,694
He's glad when people does bad

31
00:05:20,828 --> 00:05:23,092
Then he sends them
to the snake house

32
00:05:23,231 --> 00:05:25,256
He sits there laughing

33
00:05:25,399 --> 00:05:30,427
while you're tied up and snakes
are eating your eyes out

34
00:05:30,571 --> 00:05:35,372
They go down your throat
and eat all your systems up

35
00:05:39,447 --> 00:05:43,679
You're confusing me, you know
You're driving me crazy

36
00:05:43,818 --> 00:05:47,117
l don't know if l'm coming or going

37
00:05:47,255 --> 00:05:49,280
You never did

38
00:06:09,110 --> 00:06:11,578
l think the devil was on the farm

39
00:06:29,096 --> 00:06:32,964
- What's going on?
- What's the matter?

40
00:06:35,269 --> 00:06:38,067
You know what l mean

41
00:06:40,541 --> 00:06:44,602
Why do you let him
touch you like that?

42
00:06:51,118 --> 00:06:53,382
What are you talking about?

43
00:06:53,521 --> 00:06:56,922
How do brothers and sisters act
where you come from?

44
00:06:57,058 --> 00:06:59,925
Did you ever have a sister?

45
00:07:00,695 --> 00:07:03,596
Well, then, who are you to judge?

46
00:07:10,871 --> 00:07:14,102
Did you say anything to him?

47
00:07:15,643 --> 00:07:18,669
l've never seen him
act like this before

48
00:07:18,813 --> 00:07:21,873
- He must think we're awful
- What do you care?

49
00:07:31,525 --> 00:07:33,720
You're in love with him

50
00:07:43,237 --> 00:07:46,138
- Are you leaving?
- Yeah

51
00:07:46,273 --> 00:07:48,036
What for?

52
00:07:48,175 --> 00:07:51,611
l've got business to take care of
They're giving me a lift

53
00:07:53,247 --> 00:07:56,774
- Just like that, huh?
- Guess so

54
00:08:19,740 --> 00:08:22,641
He'd seen how it all was

55
00:08:25,513 --> 00:08:28,482
She loves the farmer

56
00:09:12,660 --> 00:09:17,962
He taught me keys on the pianos
and notes

57
00:09:20,067 --> 00:09:23,798
He taught me
about the parts of our globe

58
00:12:29,957 --> 00:12:33,188
Hey! l missed you so much

59
00:12:33,327 --> 00:12:36,125
- Did you miss me?
- Yeah, a lot

60
00:12:36,263 --> 00:12:40,324
Hey, you look just the same
We've been reading about you

61
00:12:40,467 --> 00:12:44,028
- What do you mean?
- h, about Chicago

62
00:12:44,171 --> 00:12:47,572
Did you come all the way
by motorcycle?

63
00:13:50,137 --> 00:13:52,105
l'm sorry

64
00:14:00,714 --> 00:14:03,547
You didn't do nothing to me

65
00:14:04,818 --> 00:14:07,946
l didn't know what l had with you

66
00:14:11,825 --> 00:14:14,157
l think about it

67
00:14:15,295 --> 00:14:18,731
The things l said to you

68
00:14:18,866 --> 00:14:22,199
How l pushed you into this

69
00:14:30,978 --> 00:14:34,141
l got nobody to blame but myself

70
00:14:39,019 --> 00:14:43,388
Well, l got to get going
Before it gets too late

71
00:17:44,404 --> 00:17:46,463
Smoke them out! Swat them out!

72
00:17:50,978 --> 00:17:53,173
That way! Let's go Come on!

73
00:20:19,192 --> 00:20:23,128
What's this? What are you doing?

74
00:20:23,263 --> 00:20:27,359
- What do you care?
- What are you doing?

75
00:20:28,869 --> 00:20:31,269
Are you crazy?!

76
00:20:48,755 --> 00:20:50,518
Let it burn!

77
00:20:58,432 --> 00:21:01,128
Get tractors, blankets!

78
00:21:43,143 --> 00:21:47,136
What are we gonna do?

79
00:21:51,885 --> 00:21:54,615
We gotta get out of here

80
00:21:55,689 --> 00:21:58,123
He knows

81
00:24:12,726 --> 00:24:15,923
What did you want from me?

82
00:24:33,546 --> 00:24:35,639
Wait

83
00:24:39,452 --> 00:24:40,942
You're a liar!

84
00:26:11,277 --> 00:26:13,643
Shall l pull it out?

85
00:27:06,699 --> 00:27:12,069
l'll tell you the whole story later
We can't do anything about it now

86
00:27:26,820 --> 00:27:32,918
You know, you're skinning me alive,
but l'll give you the old van for the boat

87
00:27:43,870 --> 00:27:49,706
Nobody's perfect There was never
a perfect person around

88
00:27:50,443 --> 00:27:55,540
You just got half devil
and half angel in you

89
00:28:11,064 --> 00:28:14,795
She promised herself
she'd lead a good life from now on

90
00:28:14,934 --> 00:28:16,993
She blamed it all on herself

91
00:28:24,177 --> 00:28:27,340
lt's good l like it

92
00:28:27,480 --> 00:28:29,573
She didn't care
if she was happy or not

93
00:28:29,716 --> 00:28:33,880
She just wanted to make up
for what she did wrong

94
00:28:34,921 --> 00:28:37,321
l have a three-pound cop

95
00:28:37,457 --> 00:28:42,053
The sun looks ghostly when there's
a mist on the river and it's quiet

96
00:28:42,195 --> 00:28:44,629
l never known it before

97
00:28:48,668 --> 00:28:54,163
You could see people on the shore,
but not what they were doing

98
00:28:54,307 --> 00:28:59,540
They were probably
calling for help or something,

99
00:28:59,679 --> 00:29:03,672
or they were trying to bury
somebody or something

100
00:29:06,920 --> 00:29:10,321
We seen trees
with leaves shaking

101
00:29:10,456 --> 00:29:15,792
lt looks like shadows of guys
coming at you and stuff

102
00:29:17,196 --> 00:29:22,361
We heard owls squawking away,
''ooming'' away

103
00:29:22,502 --> 00:29:26,461
We didn't know where we were
going or what we were gonna do

104
00:29:26,606 --> 00:29:31,839
l've never been on a boat before
That was the first time

105
00:29:33,346 --> 00:29:37,715
Have you seen her? Where?

106
00:29:37,850 --> 00:29:44,016
Some sights l saw were so spooky
they gave me goose-pimples

107
00:29:44,157 --> 00:29:50,255
l felt like cold hands touching
the back of my neck,

108
00:29:50,396 --> 00:29:55,493
and it could be the dead
coming for me or something

109
00:29:55,635 --> 00:29:59,935
l remember this guy
His name was Black Jack He died

110
00:30:00,073 --> 00:30:03,008
He only had one leg, and he died

111
00:30:03,142 --> 00:30:08,580
And l think that was Black Jack,
making those noises

112
00:31:19,619 --> 00:31:21,416
He's there!

113
00:31:45,945 --> 00:31:48,709
Check out those trees!

114
00:31:56,656 --> 00:31:58,283
Keep down!

115
00:32:01,894 --> 00:32:04,886
- Just stay here
- Wait a minute

116
00:32:18,478 --> 00:32:23,472
- Go around back that way
- Don't let him get back to the river

117
00:33:14,901 --> 00:33:18,029
l'm gonna go this way
You go over there

118
00:33:30,149 --> 00:33:32,413
Hey, Linder!

119
00:35:15,288 --> 00:35:18,086
You want that, too, don't you?

120
00:35:19,091 --> 00:35:21,685
You'll make lots of friends

121
00:35:44,917 --> 00:35:47,317
You'll be all right

122
00:35:49,021 --> 00:35:52,184
Turn around and go inside

123
00:36:20,820 --> 00:36:23,983
ranges, oranges

124
00:37:33,926 --> 00:37:35,917
Come on

125
00:37:37,363 --> 00:37:42,164
Don't worry about that stuff l got
a new boyfriend He's in the army

126
00:37:42,301 --> 00:37:47,432
Maybe the other got killed Anyway,
he said l'm pretty Maybe l am

127
00:37:47,573 --> 00:37:50,440
Look at this

128
00:37:50,576 --> 00:37:53,739
Hey, what did you do
at that school anyway?

129
00:38:13,632 --> 00:38:16,430
lnto the alley Hurry up

130
00:38:19,472 --> 00:38:23,033
l don't wait two hours for nobody

131
00:38:23,943 --> 00:38:26,969
Maybe he don't have a watch

132
00:38:27,112 --> 00:38:30,878
Yeah, if l believe that,
he'll tell me another

133
00:38:31,884 --> 00:38:34,614
His name was Edward

134
00:38:35,921 --> 00:38:39,948
He was nice
l liked him l don't know

135
00:38:40,092 --> 00:38:44,586
He said he was gonna buy me a fur
l always wanted a fur

136
00:38:44,730 --> 00:38:48,063
- Where are you going?
- For a walk

137
00:38:49,201 --> 00:38:51,999
l don't know where, but

138
00:38:53,138 --> 00:38:57,939
Go beat the heck out of some tree
Take it out on them

139
00:38:58,077 --> 00:39:00,136
You coming with me, or what?

140
00:39:00,279 --> 00:39:04,875
This girl didn't know where she was
going or what she was gonna do

141
00:39:05,017 --> 00:39:08,009
She didn't have no money on her

142
00:39:09,288 --> 00:39:12,553
Maybe she'd meet up
with a character

143
00:39:14,026 --> 00:39:19,692
l was hoping things would work out
for her She was a good friend

