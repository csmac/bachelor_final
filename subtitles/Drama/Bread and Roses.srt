1
00:00:41,888 --> 00:00:44,288
Let's go, let's go,
let's go.

2
00:00:44,358 --> 00:00:46,849
Get behind me, get behind me.

3
00:00:49,363 --> 00:00:52,730
Behind me...
don't nobody stay behind.

4
00:01:50,223 --> 00:01:54,387
Shut up, motherfucker.
Cover yourself with the blanket.

5
00:01:54,461 --> 00:01:56,088
Be quiet.

6
00:03:25,185 --> 00:03:27,585
Move over there.
Are you the relatives?

7
00:03:27,654 --> 00:03:30,487
Did you bring the money?

8
00:03:54,347 --> 00:03:57,009
Over there.
The money.

9
00:03:57,083 --> 00:04:00,018
- Yes, and the boys?
- Stay there.

10
00:04:02,088 --> 00:04:05,956
Who paid you?
What's up? What's going on?

11
00:04:07,827 --> 00:04:09,556
- Who do I let off?
- The children.

12
00:04:09,629 --> 00:04:12,621
Get off, fucking kids.
Hurry up, I don't want to see you.

13
00:04:20,607 --> 00:04:24,043
Move, bitch.
Lose weight, bitch.

14
00:04:27,547 --> 00:04:29,071
What's up?

15
00:04:29,649 --> 00:04:31,583
Come on.
Let's go, come on.

16
00:04:34,354 --> 00:04:36,948
Maya Montenegro?

17
00:04:37,757 --> 00:04:39,054
Here.

18
00:04:39,125 --> 00:04:40,990
Get back in, bitch!

19
00:04:43,597 --> 00:04:46,794
- What's this?
- I got mugged.

20
00:04:46,866 --> 00:04:48,697
I don't give a shit
if you got mugged.

21
00:04:48,768 --> 00:04:52,101
I'll pay you for her.
Here's my wedding band.

22
00:04:52,172 --> 00:04:53,400
I'm coming, Maya.

23
00:04:53,473 --> 00:04:56,874
- My watch.
- I'll pay too.

24
00:04:56,943 --> 00:04:58,774
Let her out.

25
00:05:02,248 --> 00:05:05,308
- Don't do it.
- Don't move.

26
00:05:05,385 --> 00:05:07,615
- I beg you.
- Fuck you.

27
00:05:07,687 --> 00:05:10,315
Don't take her away.

28
00:05:15,729 --> 00:05:17,356
Rosa, help me!

29
00:05:17,430 --> 00:05:19,159
Where are you taking me,
motherfucker?

30
00:05:19,232 --> 00:05:22,895
Rosa, help me, Rosa.
Help me, Rosa!

31
00:05:29,743 --> 00:05:31,904
Move away from the window.

32
00:05:33,947 --> 00:05:36,541
Why are you crying now, huh?

33
00:05:40,153 --> 00:05:42,212
Where are you taking me?

34
00:05:42,288 --> 00:05:44,119
Shut up.
Shut the fuck up.

35
00:05:50,864 --> 00:05:54,027
- You cry a lot, huh?
- Don't fuck with me.

36
00:05:54,100 --> 00:05:56,830
- Shut the fuck up.
- Take out a coin.

37
00:05:56,903 --> 00:05:59,303
Let's have a toss to see
what happens to her.

38
00:05:59,372 --> 00:06:01,966
Somebody will get laid
tonight, man.

39
00:06:02,042 --> 00:06:03,771
Do you want it,
or not?

40
00:06:07,947 --> 00:06:09,710
Toss it, man.
Let's see it.

41
00:06:10,784 --> 00:06:12,342
Fuck it, man.

42
00:06:20,527 --> 00:06:23,121
Let me carry your bag,
you're tired.

43
00:06:23,196 --> 00:06:26,791
Don't be like that with me.
I'm not like that prick.

44
00:06:39,879 --> 00:06:41,744
What's wrong?
Don't worry.

45
00:06:41,815 --> 00:06:43,908
I'm not like that prick.

46
00:06:47,787 --> 00:06:51,655
You're not as ugly as
I first thought.

47
00:06:51,725 --> 00:06:54,717
But you stink, motherfucker.

48
00:06:57,063 --> 00:06:59,691
You don't smell bad
yourself, honey.

49
00:07:04,504 --> 00:07:06,495
Do you like them?

50
00:07:09,743 --> 00:07:12,007
Where did you get them?

51
00:07:12,078 --> 00:07:17,175
In Tijuana, they're the coolest
in all Tijuana.

52
00:07:17,250 --> 00:07:20,845
Later.
It's late, sweetheart.

53
00:07:44,544 --> 00:07:47,342
- Do you like to sing?
- No, honey.

54
00:07:49,449 --> 00:07:51,713
I know very well
that I'm outside...

55
00:07:51,785 --> 00:07:52,683
Hit it.

56
00:07:52,752 --> 00:07:57,314
But the day that I die,
I know you'll have to cry.

57
00:07:57,390 --> 00:07:59,551
You do sing nice, sweetie.

58
00:07:59,626 --> 00:08:02,686
Cry and cry...

59
00:08:02,762 --> 00:08:05,094
Fly and fly...

60
00:08:05,165 --> 00:08:07,099
It's "cry," man.

61
00:08:07,167 --> 00:08:11,160
Oh, that's right.
I'll follow you, then.

62
00:08:12,438 --> 00:08:15,168
Then a mule-seller
told me...

63
00:08:15,241 --> 00:08:17,971
No, I got it wrong
already.

64
00:08:18,044 --> 00:08:21,810
That you don't have
to get there first,

65
00:08:21,881 --> 00:08:24,679
But you have to know
how to do it...

66
00:08:24,751 --> 00:08:26,184
That's it,
go on, go on.

67
00:08:26,252 --> 00:08:30,018
With or without money,

68
00:08:30,089 --> 00:08:33,581
I always do what I want,

69
00:08:33,660 --> 00:08:36,458
And my word is the law.

70
00:08:37,831 --> 00:08:39,230
Oh, mama!

71
00:08:39,299 --> 00:08:40,891
Turn around,
let me lather you up.

72
00:08:40,967 --> 00:08:42,764
I'm liking you already.

73
00:08:42,836 --> 00:08:44,497
Put some water on.

74
00:08:49,409 --> 00:08:51,138
You lather me up good.

75
00:08:51,211 --> 00:08:54,476
And that's how
you're gonna stay.

76
00:08:54,547 --> 00:08:59,314
With or without money,

77
00:08:59,385 --> 00:09:02,252
I always do what I want.

78
00:09:02,322 --> 00:09:05,450
- You sing well.
- Let's see.

79
00:09:05,525 --> 00:09:08,323
- Excuse me?
- Hold on a sec.

80
00:09:12,365 --> 00:09:13,957
Don't be long.

81
00:09:14,033 --> 00:09:16,399
Didn't you like
my voice?

82
00:09:16,469 --> 00:09:20,200
You do sing,
I had heard you before.

83
00:09:20,273 --> 00:09:22,537
I know you like me,
honey.

84
00:09:22,609 --> 00:09:25,134
I told you I'd heard you
in the car.

85
00:09:25,211 --> 00:09:29,170
I know, you've been looking
at my hair, it drives them crazy.

86
00:09:29,249 --> 00:09:32,912
How do you
take care of it, man?

87
00:09:32,986 --> 00:09:35,580
But you must know
how to get there.

88
00:09:42,262 --> 00:09:45,754
What the fuck?
You bitch.

89
00:09:45,832 --> 00:09:48,494
Bitch.
Open it, bitch.

90
00:09:48,568 --> 00:09:50,433
Open it right now!

91
00:09:50,503 --> 00:09:52,334
Fucking bitch!

92
00:09:52,405 --> 00:09:55,602
Fucking bitch.
Where are you?

93
00:09:56,676 --> 00:09:58,940
Fucking bitch!

94
00:10:02,749 --> 00:10:05,240
Come back, you cunt!

95
00:10:05,318 --> 00:10:08,219
My boots!
Son of a bitch!

96
00:10:29,876 --> 00:10:31,901
Luis, how are you?

97
00:10:43,189 --> 00:10:44,713
Mom was so worried.

98
00:10:44,791 --> 00:10:46,850
You're
taller than me.

99
00:10:46,926 --> 00:10:50,453
What happened?
I was so scared.

100
00:10:50,530 --> 00:10:53,556
Thank God you're here.

101
00:10:59,772 --> 00:11:01,364
Granny.

102
00:11:08,247 --> 00:11:11,216
Uncle Quique.
Let me see Uncle Quique.

103
00:11:17,323 --> 00:11:20,258
Check out Valeria
in her dress.

104
00:11:20,326 --> 00:11:22,794
At her party.
Beautiful.

105
00:11:44,584 --> 00:11:47,610
What happened
with the money?

106
00:11:47,687 --> 00:11:51,145
I've been penniless
since Bert got sick.

107
00:11:51,224 --> 00:11:53,920
Why didn't you tell me, Rosa?
Those guys are crazy.

108
00:11:53,993 --> 00:11:56,291
Who's crazy?
I told you.

109
00:11:56,362 --> 00:11:59,126
I told you, but you bugged
and bugged me.

110
00:11:59,198 --> 00:12:00,859
You've never listened to me.

111
00:12:48,181 --> 00:12:50,274
- Please.
- Okay.

112
00:13:12,205 --> 00:13:15,140
Hey, baby.
Excuse me again.

113
00:13:15,208 --> 00:13:16,675
Where are you from?

114
00:13:16,742 --> 00:13:18,232
- Me?
- Yeah.

115
00:13:18,311 --> 00:13:21,246
- From Mexico.
- Whereabouts in Mexico?

116
00:13:21,314 --> 00:13:24,306
- Mexico City.
- They say Mexico City girls

117
00:13:24,383 --> 00:13:26,078
are all sluts.

118
00:13:26,152 --> 00:13:28,882
I don't know.
Where are you from?

119
00:13:28,955 --> 00:13:31,287
Guatemala.

120
00:13:31,357 --> 00:13:33,450
You know
what I've been told?

121
00:13:33,526 --> 00:13:35,460
That Guatemalans
are all fairies.

122
00:13:35,528 --> 00:13:36,654
That's not possible.

123
00:13:40,700 --> 00:13:43,635
Careful with the bottle.

124
00:13:59,619 --> 00:14:03,646
I hope you don't get me into trouble
with the guy at the door.

125
00:14:03,723 --> 00:14:05,054
Why?

126
00:14:05,124 --> 00:14:07,285
I told you you couldn't come.
You can't come in.

127
00:14:07,360 --> 00:14:08,349
What do I do?

128
00:14:08,427 --> 00:14:10,725
I don't know.
Just stay out.

129
00:14:10,796 --> 00:14:12,559
- I'll wait here.
- Shut up.

130
00:14:12,632 --> 00:14:15,192
I'll be here
all night, Rosa.

131
00:15:00,346 --> 00:15:02,837
Hi.
Are you okay?

132
00:15:02,915 --> 00:15:04,746
- Excuse me?
- Are you lost?

133
00:15:04,817 --> 00:15:05,909
No, no, I'm okay.

134
00:15:05,985 --> 00:15:09,352
Do you know anybody
who could help me get a job?

135
00:15:09,422 --> 00:15:11,822
Not really,
but I could ask around.

136
00:15:27,707 --> 00:15:30,369
You sure you'll be okay here?

137
00:15:30,443 --> 00:15:31,876
- Yeah.
- Yeah?

138
00:15:36,549 --> 00:15:38,949
Fucking son of a bitch
bastard.

139
00:15:40,219 --> 00:15:41,743
Motherfucker.
Let's go.

140
00:16:15,554 --> 00:16:17,454
What did he say?

141
00:19:27,980 --> 00:19:32,212
Watch, Maya, first use
the screwdriver, like this.

142
00:19:33,986 --> 00:19:36,250
That's easier.

143
00:19:36,322 --> 00:19:38,984
- Yeah, that's the idea.
- Not with the finger.

144
00:19:39,058 --> 00:19:43,085
But first you have to clean up
these little bits.

145
00:19:46,699 --> 00:19:48,360
It's stuck.

146
00:19:48,434 --> 00:19:50,493
But be careful,
because sometimes

147
00:19:50,569 --> 00:19:53,333
the draft blows
the dirt in your face.

148
00:20:05,251 --> 00:20:07,947
Did I tell you my theory
about these uniforms?

149
00:20:08,020 --> 00:20:09,385
No.

150
00:20:09,455 --> 00:20:11,423
They make us invisible.

151
00:20:11,490 --> 00:20:14,186
Come on.
Let's move on to the other one.

152
00:20:14,260 --> 00:20:15,352
Why?

153
00:20:15,427 --> 00:20:18,191
We have to wait for it
to come back.

154
00:20:22,635 --> 00:20:25,934
- What are you doing?
- Hold it a minute.

155
00:20:27,006 --> 00:20:29,941
Good heavens, woman.
What are you doing?

156
00:20:48,093 --> 00:20:50,960
You're nuts, woman.
It's your first day.

157
00:21:35,474 --> 00:21:37,908
Excuse me.
Don't be scared.

158
00:22:24,523 --> 00:22:26,821
I'm not a thief.
I promise.

159
00:22:41,707 --> 00:22:44,801
- Thanks a lot, miss.
- You're welcome.

160
00:24:04,757 --> 00:24:06,884
So what's your name?

161
00:24:47,266 --> 00:24:49,200
Mom, Auntie is here.

162
00:24:49,268 --> 00:24:50,735
Already?

163
00:24:50,803 --> 00:24:53,169
Where are you?
Why did it take you so long?

164
00:24:53,238 --> 00:24:54,933
Because the lines
were endless.

165
00:24:55,007 --> 00:24:58,272
I got you this one

166
00:24:58,343 --> 00:25:00,072
because they didn't have
the other kind.

167
00:25:12,491 --> 00:25:14,356
- Mom!
- What?

168
00:25:14,426 --> 00:25:16,257
There's somebody
at the door.

169
00:25:17,329 --> 00:25:18,796
Who is it?

170
00:29:12,064 --> 00:29:13,793
Mom, calm down!

171
00:30:12,958 --> 00:30:14,892
Hurry up.
I've got a lot to do.

172
00:30:14,960 --> 00:30:16,450
Damn it!

173
00:30:19,197 --> 00:30:22,394
To hell with that.
Everybody talking at the same time.

174
00:30:25,770 --> 00:30:27,567
You just have to listen.

175
00:30:29,541 --> 00:30:31,441
He's not talking nonsense.

176
00:30:31,510 --> 00:30:32,772
Shut up!

177
00:32:15,513 --> 00:32:18,209
All these books.

178
00:32:18,283 --> 00:32:22,583
He's spent three years like this,
coming in early.

179
00:32:22,654 --> 00:32:24,485
Why so many books?

180
00:32:24,556 --> 00:32:27,024
Try helping him,
you could learn something.

181
00:32:27,092 --> 00:32:29,788
You know what's
the best part?

182
00:32:29,861 --> 00:32:33,888
He copies every single thing
from the book.

183
00:32:33,965 --> 00:32:35,899
What's your problem?

184
00:32:35,967 --> 00:32:37,901
Why the fuck
does he do that?

185
00:32:37,969 --> 00:32:40,164
And why the fuck
do you bug him so much?

186
00:32:40,238 --> 00:32:43,401
He's supposed to be the smart one,
the intelligent one.

187
00:32:43,475 --> 00:32:46,035
You know what
I don't understand?

188
00:32:46,111 --> 00:32:50,775
Why the fuck don't you just
photocopy those things?

189
00:34:47,499 --> 00:34:50,798
She reminds me
so much of my mother.

190
00:39:43,661 --> 00:39:45,652
Where are we going?

191
00:39:45,730 --> 00:39:47,630
Don't be so impatient.

192
00:39:47,699 --> 00:39:49,997
Go on, tell me.

193
00:39:50,068 --> 00:39:52,536
Wait and see.

194
00:40:24,602 --> 00:40:26,399
Look.

195
00:40:30,675 --> 00:40:31,937
Read it.

196
00:40:36,647 --> 00:40:38,672
Did you pay $1,600?

197
00:40:38,750 --> 00:40:41,480
Yes, I've been saving
for this for years.

198
00:40:41,552 --> 00:40:44,316
And you know what?
I'm going to college.

199
00:40:45,389 --> 00:40:47,380
- This one?
- Yes.

200
00:40:47,458 --> 00:40:49,790
- Like that?
- I'm paying 200/0.

201
00:40:49,861 --> 00:40:51,692
They're gonna pay 800/0.

202
00:40:51,763 --> 00:40:53,253
Who?

203
00:40:53,331 --> 00:40:56,266
A company.
It's about $16,000.

204
00:40:56,334 --> 00:40:59,360
16,000 fucking dollars.
Imagine that.

205
00:40:59,437 --> 00:41:01,871
I've been waiting for this
since I got to this country.

206
00:41:01,939 --> 00:41:03,930
How are you gonna pay
for that?

207
00:41:04,008 --> 00:41:06,909
Well, see,
first I took an exam.

208
00:41:07,945 --> 00:41:10,937
I didn't do so well
in the oral exam,

209
00:41:11,015 --> 00:41:13,381
but I came in first
in the math test.

210
00:41:14,519 --> 00:41:17,454
But even then they didn't give me
a scholarship, and you know why?

211
00:41:17,522 --> 00:41:20,116
Simply because
I'm not an American citizen.

212
00:41:20,191 --> 00:41:22,125
But then...

213
00:41:22,193 --> 00:41:24,593
I found a private foundation

214
00:41:24,662 --> 00:41:27,460
belonging to two Mexican brothers,
with a lot of money.

215
00:41:27,532 --> 00:41:30,023
They had about 800
applications,

216
00:41:30,101 --> 00:41:32,797
and guess who they chose?

217
00:41:37,041 --> 00:41:38,975
And then?

218
00:41:39,043 --> 00:41:41,671
But now what I have to do
is show proof...

219
00:41:41,746 --> 00:41:44,943
that I paid 200/0
of the cost

220
00:41:45,016 --> 00:41:49,180
by the first day of the semester,
and I only have $1,600 to go.

221
00:41:49,253 --> 00:41:51,312
- And that's it.
- Really?

222
00:41:51,389 --> 00:41:53,482
- That's it.
- Congratulations.

223
00:41:54,358 --> 00:41:58,158
No, I can't believe it.

224
00:43:00,424 --> 00:43:03,825
"I've just moved into
my new office, Mama,

225
00:43:03,895 --> 00:43:07,092
but things at home
are not going so well.

226
00:43:09,100 --> 00:43:12,934
You can't find a good maid
available nowadays,

227
00:43:13,004 --> 00:43:15,097
and forget about
a good cook.

228
00:43:15,172 --> 00:43:17,834
Besides, I need somebody
to walk the dog,

229
00:43:17,909 --> 00:43:20,673
so I am sending
my chauffeur, Hector...

230
00:43:21,746 --> 00:43:24,840
to go straight into the city
to pick you up.

231
00:43:24,916 --> 00:43:27,908
He has strict orders
to lock you up in the trunk

232
00:43:27,985 --> 00:43:30,453
if you start acting up."

233
00:45:37,415 --> 00:45:40,350
Can I talk to you?
Have a seat.

234
00:45:44,488 --> 00:45:46,718
- How are you?
- Fine.

235
00:45:49,393 --> 00:45:50,951
It's hard.

236
00:45:51,028 --> 00:45:52,393
Three and a half floors.

237
00:45:52,463 --> 00:45:54,556
It takes a lot of energy.

238
00:45:56,200 --> 00:45:59,101
How would you like
a vacation?

239
00:45:59,170 --> 00:46:01,638
- Would you like that?
- Sure, I'd like that.

240
00:46:04,208 --> 00:46:06,369
Everything paid for.

241
00:46:06,444 --> 00:46:09,038
- $12.50 an hour.
- Oh, that's nice.

242
00:46:09,113 --> 00:46:11,206
Sounds good?

243
00:46:13,284 --> 00:46:14,842
That, too?

244
00:46:20,424 --> 00:46:22,949
And we're gonna be very busy.

245
00:46:24,028 --> 00:46:27,657
I'm gonna need
people we can trust a lot.

246
00:46:29,967 --> 00:46:32,663
How would you like
to be a supervisor?

247
00:46:32,736 --> 00:46:34,636
Me?
That would be cool.

248
00:46:34,705 --> 00:46:37,868
I'd try to do it
as best as I can.

249
00:46:37,942 --> 00:46:39,876
Yeah, because
you've been here a long time.

250
00:46:39,944 --> 00:46:42,777
- How many years?
- 17 years here.

251
00:46:42,847 --> 00:46:45,543
And do you think
you could be a supervisor?

252
00:46:45,616 --> 00:46:47,743
Yes, I could do it.

253
00:46:51,655 --> 00:46:54,852
That's all I wanted to know.
Thank you.

254
00:46:59,897 --> 00:47:01,990
Sit down again.

255
00:47:12,910 --> 00:47:15,174
Who was it, Berta?

256
00:47:18,782 --> 00:47:21,046
Do you know?

257
00:54:47,230 --> 00:54:49,255
- Good evening.
- Good evening.

258
00:54:49,333 --> 00:54:51,767
It's a pleasure
to be with you tonight.

259
00:55:13,056 --> 00:55:14,250
We can do it!

260
00:55:14,324 --> 00:55:16,121
We can do it!

261
00:56:57,694 --> 00:57:00,060
- What do we want?
- Justice!

262
00:57:00,130 --> 00:57:02,428
- When do we want it?
- Now!

263
00:57:08,004 --> 00:57:09,869
Janitors united...

264
00:57:09,940 --> 00:57:12,101
Will never be divided!

265
01:01:12,983 --> 01:01:15,178
.' I started to learn English.'

266
01:01:15,251 --> 01:01:18,186
.' Because I had no choice.'

267
01:01:20,256 --> 01:01:25,751
.' To fight the gringo boss
I had to have a voice... '

268
01:01:42,112 --> 01:01:44,410
.' I used to work at a place.'

269
01:01:44,481 --> 01:01:46,915
.' Where they wanted
to boss me around, .'

270
01:01:48,084 --> 01:01:50,575
.' Just because
of the goddamn English.'

271
01:01:50,653 --> 01:01:53,144
.' I couldn't speak.'

272
01:02:03,767 --> 01:02:08,397
.' The gringo boss got angry
and yelled at me in English... '

273
01:02:12,475 --> 01:02:14,375
.' "You wetbacks
don't understand.'

274
01:02:14,444 --> 01:02:16,674
.' What you are
supposed to do.".'

275
01:02:38,968 --> 01:02:42,096
Here's one from the people,
for the people!

276
01:02:42,172 --> 01:02:43,867
It goes like this.

277
01:03:05,829 --> 01:03:08,263
.' I'm here to sing to you.'

278
01:03:08,331 --> 01:03:10,492
.' About a sad situation.'

279
01:03:10,567 --> 01:03:13,035
.' With the laws being made.'

280
01:03:13,103 --> 01:03:15,435
.' By the White House
and Immigration.'

281
01:03:15,505 --> 01:03:18,065
.' All of my Latin people.'

282
01:03:18,141 --> 01:03:20,268
.' Suffer from discrimination.'

283
01:03:20,343 --> 01:03:22,709
.' They take away our rights.'

284
01:03:22,779 --> 01:03:25,111
.' And sic the INS on us.'

285
01:03:29,853 --> 01:03:32,344
.' But I won't leave this place.'

286
01:03:32,422 --> 01:03:34,947
.' I won't leave,
I'll never leave... '

287
01:03:56,012 --> 01:03:58,503
.' So I say it here singing.'

288
01:03:58,581 --> 01:04:00,913
.' This joyful melody.'

289
01:04:00,984 --> 01:04:03,077
.' It's better to die fighting.'

290
01:04:03,153 --> 01:04:05,383
.' Than to die on your knees.'

291
01:04:05,455 --> 01:04:07,719
.' We have to follow the example.'

292
01:04:07,790 --> 01:04:10,020
.' Of the legendary Cesar Chavez.'

293
01:04:10,093 --> 01:04:12,527
.' And all together we say now, .'

294
01:04:12,595 --> 01:04:14,756
.' We can do it, we can do it.'

295
01:04:14,831 --> 01:04:16,093
Scream!

296
01:04:16,166 --> 01:04:17,633
.' We can do it! .'

297
01:04:17,700 --> 01:04:20,225
.' We can do it! .'

298
01:04:47,430 --> 01:04:51,833
We'll go on, of course.

299
01:04:51,901 --> 01:04:54,392
To the day-laborers
of the Norte!

300
01:08:45,835 --> 01:08:47,735
We shall overcome!

301
01:09:57,740 --> 01:10:00,038
Can you imagine
their faces when...?

302
01:10:32,408 --> 01:10:34,876
We either win it all
or we lose it all,

303
01:10:34,944 --> 01:10:37,276
and I think it looks
quite complicated.

304
01:10:40,583 --> 01:10:44,576
You know, I've something
to tell you about college.

305
01:10:44,654 --> 01:10:46,519
I have everything ready.

306
01:10:46,589 --> 01:10:51,492
I'll be able to pay the $1,600
by the deadline they gave me.

307
01:10:52,795 --> 01:10:55,958
And I'll still have $125 left.

308
01:10:56,032 --> 01:10:58,592
But if I don't pay
on time...

309
01:10:58,668 --> 01:11:01,330
the scholarship moves on
to the next person on the list.

310
01:11:02,338 --> 01:11:05,432
But what are you
trying to tell me?

311
01:11:05,508 --> 01:11:09,842
I'm not gonna do this.

312
01:11:09,912 --> 01:11:11,846
What?

313
01:11:17,720 --> 01:11:20,314
The truth is, Maya, that...

314
01:11:20,389 --> 01:11:23,790
I don't want tojeopardize
my scholarship.

315
01:11:23,859 --> 01:11:26,259
I don't want to lose it.

316
01:11:26,329 --> 01:11:28,695
You know that
I've been killing myself

317
01:11:28,764 --> 01:11:31,232
the last five years
to get it,

318
01:11:31,300 --> 01:11:34,326
and to lose itjust like that?
I don't think so.

319
01:11:35,404 --> 01:11:37,668
Just like that?

320
01:11:37,740 --> 01:11:41,437
The way I see it,
our families can't afford...

321
01:11:41,510 --> 01:11:44,968
to lose the money
we send them, either.

322
01:11:45,047 --> 01:11:48,346
But, it's different, Maya.

323
01:11:49,418 --> 01:11:52,910
I don't know about you,
but I think that...

324
01:11:52,989 --> 01:11:55,651
if we don't do it in this way,

325
01:11:55,725 --> 01:11:59,786
we'll be left with what we had.
Nothing's gonna happen.

326
01:12:01,430 --> 01:12:04,160
But I'm not doing it
just for me, Maya.

327
01:12:04,233 --> 01:12:06,098
I'm doing it for you, too.

328
01:12:08,604 --> 01:12:10,799
Don't say that.

329
01:12:10,873 --> 01:12:13,740
Why not, Maya?

330
01:12:13,809 --> 01:12:16,505
You know I love you.

331
01:12:17,780 --> 01:12:20,408
And I want us
to be together.

332
01:12:21,684 --> 01:12:24,346
But I don't know
what I want.

333
01:12:29,659 --> 01:12:31,559
What about Sam?

334
01:12:33,162 --> 01:12:34,720
I like him.

335
01:12:38,234 --> 01:12:40,361
He's cool,
he's funny.

336
01:12:41,804 --> 01:12:43,533
I'm learning from him.

337
01:12:47,576 --> 01:12:49,874
He believes in something.

338
01:12:49,945 --> 01:12:52,413
And he's also white.

339
01:12:53,949 --> 01:12:56,247
Don't give me that shit.

340
01:13:00,856 --> 01:13:03,791
I don't know
what you are thinking.

341
01:13:03,859 --> 01:13:06,054
That's why you're doing it,
right?

342
01:13:09,365 --> 01:13:14,029
What was it that you said
when they fired Teresa?

343
01:13:15,838 --> 01:13:18,306
"She looks like my mother."

344
01:13:20,743 --> 01:13:23,303
That's why I'm doing it.

345
01:13:24,714 --> 01:13:27,046
I'm doing it
because my sister

346
01:13:27,116 --> 01:13:31,348
has been working 16 hours a day
since she got here.

347
01:13:31,420 --> 01:13:35,413
Because her husband
can't pay for the hospital bills.

348
01:13:35,491 --> 01:13:39,791
He doesn't have medical insurance,
like 40 million people in this country,

349
01:13:39,862 --> 01:13:43,195
in this fucking country!
The richest country in the world!

350
01:13:43,265 --> 01:13:46,462
I'm doing it because
I have to give Perez

351
01:13:46,535 --> 01:13:50,528
two months of my salary,
and I have to beg him for a job.

352
01:13:51,640 --> 01:13:55,633
I'm doing it because we feed
those bastards,

353
01:13:55,711 --> 01:13:59,078
we wipe their asses,
we do everything for them.

354
01:13:59,148 --> 01:14:01,412
We raise their children,

355
01:14:01,484 --> 01:14:04,112
and they still look
right through us.

356
01:14:09,024 --> 01:14:13,961
And maybe one day
I will go to school too, you see?

357
01:14:14,029 --> 01:14:17,294
But what good will it be
if I forget about everything?

358
01:18:55,344 --> 01:18:59,007
We can do it!
We can do it!

359
01:25:01,877 --> 01:25:04,004
You bitch!

360
01:25:06,815 --> 01:25:09,010
Why?

361
01:25:11,086 --> 01:25:14,214
Ella.
Ella, your friend, Ella.

362
01:25:14,289 --> 01:25:16,780
Dolores, Ben,

363
01:25:16,858 --> 01:25:19,952
Ruben will lose
his place in college,

364
01:25:20,028 --> 01:25:22,428
and he wasn't there.

365
01:25:22,497 --> 01:25:24,590
Why don't you
shut the fuck up?

366
01:25:24,666 --> 01:25:27,328
I won't shut up.
Dolores is pregnant.

367
01:25:29,371 --> 01:25:31,839
Anyway, they were gonna
lose theirjobs.

368
01:25:31,907 --> 01:25:34,501
It was just
a matter of time.

369
01:25:39,247 --> 01:25:41,841
So it was you?

370
01:25:41,917 --> 01:25:44,647
Of course it was me.
And I'd do it again.

371
01:25:44,719 --> 01:25:47,051
I won't stand around
with my arms crossed

372
01:25:47,122 --> 01:25:49,852
while my husband is dying waiting
to be seen on a waiting list.

373
01:25:51,626 --> 01:25:53,560
Right now, stupid.

374
01:25:53,628 --> 01:25:55,596
It's not a fucking
fairy tale, huh?

375
01:25:57,999 --> 01:26:00,524
Why?

376
01:26:00,602 --> 01:26:03,662
- Why what?
- Why did you sell us out?

377
01:26:03,738 --> 01:26:06,673
- Why did you sell us out?
- You sold yourselves out.

378
01:26:06,741 --> 01:26:09,642
You did it yourselves.
You alone.

379
01:26:09,711 --> 01:26:11,440
You did it on your own.

380
01:26:11,513 --> 01:26:14,209
We're always saying,
"We're gonna win."

381
01:26:14,282 --> 01:26:16,614
But when will we realize it?

382
01:26:16,685 --> 01:26:20,382
When will we realize that they're
much stronger than us?

383
01:26:20,455 --> 01:26:22,821
It's always been like that.

384
01:26:22,891 --> 01:26:26,850
How many times
have we discussed it? God!

385
01:26:26,928 --> 01:26:29,089
Pick up those clothes.

386
01:26:31,533 --> 01:26:34,798
You're a fucking traitor,
Rosa.

387
01:26:34,870 --> 01:26:37,464
You're a fucking traitor,
sis.

388
01:26:37,539 --> 01:26:41,498
Really?
You think I'm a traitor?

389
01:26:41,576 --> 01:26:43,544
You do, don't you?

390
01:26:43,612 --> 01:26:45,546
You think that?

391
01:26:46,781 --> 01:26:49,306
Even when I was
supporting everybody?

392
01:26:49,384 --> 01:26:54,344
Sending money to you and Mama?
You think so?

393
01:26:54,422 --> 01:26:57,448
When I was feeding
everybody?

394
01:26:57,526 --> 01:26:59,517
Did you guys ever wonder...

395
01:26:59,594 --> 01:27:02,222
how did Rosa manage
to send the money?

396
01:27:02,297 --> 01:27:04,424
How old was I
when I got to Tijuana?

397
01:27:04,499 --> 01:27:07,900
I was a little girl.
A little girl, right?

398
01:27:07,969 --> 01:27:10,460
And you didn't even wonder
about how I did it.

399
01:27:10,539 --> 01:27:13,667
What for? Rosa kept your mouths
pretty full.

400
01:27:13,742 --> 01:27:16,142
You know how I did it?

401
01:27:16,211 --> 01:27:18,771
Turning tricks.

402
01:27:22,884 --> 01:27:25,910
I was a hooker.
What do you think about that?

403
01:27:25,987 --> 01:27:30,822
- I didn't know.
- I was turning tricks, honey.

404
01:27:30,892 --> 01:27:33,861
So that you guys
wouldn't starve.

405
01:27:40,902 --> 01:27:43,166
"Suck their dicks, Rosa."

406
01:27:43,238 --> 01:27:44,865
I never knew that.

407
01:27:45,941 --> 01:27:48,535
"Come on, because your family
is starving."

408
01:27:50,278 --> 01:27:52,041
"Come on, come on!"

409
01:27:53,348 --> 01:27:57,409
Sounds awful, huh?
Disgusting?

410
01:27:57,485 --> 01:28:00,249
What do you think?

411
01:28:00,322 --> 01:28:03,314
Nobody asked me, huh?
My dad leaves,

412
01:28:03,391 --> 01:28:06,724
and who gets screwed?
Who gets screwed?

413
01:28:06,795 --> 01:28:09,389
Rosa. Let Rosa
start fucking everybody.

414
01:28:09,464 --> 01:28:11,557
I didn't know, Rosa.

415
01:28:11,633 --> 01:28:13,760
Let her suck
everybody's fucking dicks...

416
01:28:13,835 --> 01:28:17,532
blacks, whites,
sleazeballs, slimebags.

417
01:28:17,606 --> 01:28:20,268
Let her fuck
everybody, right?

418
01:28:20,342 --> 01:28:23,038
- But we didn't know.
- My husband gets sick,

419
01:28:23,111 --> 01:28:25,773
and I have to fuck.
Whose turn is it? Rosa's.

420
01:28:25,847 --> 01:28:28,782
Let Rosa pick up
all the pieces, right?

421
01:28:28,850 --> 01:28:31,910
She has to be doing
everything all the time,

422
01:28:31,987 --> 01:28:35,684
as if Rosa were an idiot,
fucking everybody, right?

423
01:28:35,757 --> 01:28:39,625
So you had something
to eat!

424
01:28:39,694 --> 01:28:42,993
I hate the whole fucking world!
I hate it!

425
01:28:43,064 --> 01:28:46,966
I've put up with it
all my fucking life!

426
01:28:47,035 --> 01:28:51,096
I've been keeping it here,
in my own gut.

427
01:28:55,810 --> 01:28:58,142
Come on, come here!

428
01:28:58,213 --> 01:29:00,340
- Rosa, I didn't know.
- I'm talking to you.

429
01:29:00,415 --> 01:29:02,542
Do you know
how I got you yourjob?

430
01:29:02,617 --> 01:29:04,517
Didn't you want
a job here?

431
01:29:04,586 --> 01:29:08,488
"Come on, sis, get me a job,
because I can do everything."

432
01:29:08,556 --> 01:29:10,421
You know what I did?

433
01:29:10,492 --> 01:29:12,858
Yes, honey,
I had to fuck Perez.

434
01:29:12,927 --> 01:29:14,918
- Don'tjoke about that.
- I fucked him.

435
01:29:14,996 --> 01:29:18,056
I fucked him for you!
I fucked him for you!

436
01:29:18,133 --> 01:29:21,569
Because I'm tired.
Tired!

437
01:29:21,636 --> 01:29:24,503
All my life.
My husband gets sick, I go fuck.

438
01:29:24,572 --> 01:29:26,335
- My dad leaves, I go fuck.
- I didn't know!

439
01:29:26,408 --> 01:29:29,605
You want a job?
I go fuck. All the time!

440
01:29:29,678 --> 01:29:31,612
Goddamn it,
I didn't know!

441
01:29:31,680 --> 01:29:33,614
No? Everybody looked
the other way.

442
01:29:33,682 --> 01:29:35,843
- I didn't know, Rosa.
- Now do you feel bad?

443
01:29:35,917 --> 01:29:37,544
Do you feel bad?

444
01:29:52,600 --> 01:29:56,434
My own daughter,
my baby asks me where her dad is.

445
01:30:01,876 --> 01:30:04,936
I don't know
who the fuck is her father.

446
01:30:05,013 --> 01:30:07,880
"You were born
in a brothel, honey."

447
01:30:09,284 --> 01:30:11,582
I am a fucking traitor.

448
01:30:11,653 --> 01:30:14,053
A traitor
who is lying to herself,

449
01:30:14,122 --> 01:30:16,522
to my own family,
to my baby girl.

450
01:30:20,328 --> 01:30:23,388
Rosa, what did we do
to you?

451
01:30:23,465 --> 01:30:26,263
What did you
do to me?

452
01:30:30,739 --> 01:30:33,401
I didn't know.

453
01:33:00,321 --> 01:33:03,256
What's that?

454
01:33:03,324 --> 01:33:05,224
Check it out.

455
01:33:17,772 --> 01:33:20,206
Is this a joke or what?

456
01:33:21,276 --> 01:33:23,710
You start on September 12th.

457
01:33:32,987 --> 01:33:36,582
How did you manage
to get all that money?

458
01:33:36,658 --> 01:33:38,853
Don't ask.

459
01:33:40,295 --> 01:33:41,455
Listen...

460
01:33:41,529 --> 01:33:45,397
if you don't become the most
important attorney in this country,

461
01:33:45,466 --> 01:33:47,730
I will never forgive you.

462
01:34:00,315 --> 01:34:02,681
I don't know
what to tell you.

463
01:34:02,750 --> 01:34:05,241
Go to school.

464
01:34:05,320 --> 01:34:06,685
Thanks.

465
01:35:48,322 --> 01:35:50,313
Up with the union!

466
01:35:50,391 --> 01:35:52,723
Down with exploitation!

467
01:39:44,825 --> 01:39:48,761
Because we are well organized

468
01:39:48,829 --> 01:39:52,697
we shall not be moved.

469
01:40:29,437 --> 01:40:32,031
Not even by the police.

470
01:40:32,106 --> 01:40:33,596
We shall not be moved.

471
01:40:33,674 --> 01:40:35,801
Not even by the police.

472
01:40:35,876 --> 01:40:37,810
We shall not be moved...

473
01:42:39,767 --> 01:42:41,894
Which one can you sing best?

474
01:44:36,951 --> 01:44:38,475
Does she have papers?

475
01:45:16,824 --> 01:45:18,018
Understand?

476
01:45:57,598 --> 01:45:59,793
Leave her alone.

477
01:46:03,837 --> 01:46:05,395
Come here!

478
01:46:14,948 --> 01:46:17,348
I don't see her.

479
01:46:17,418 --> 01:46:19,613
Maya's not there.

480
01:46:19,687 --> 01:46:22,554
- There she is!
- Here she comes.

481
01:46:22,623 --> 01:46:25,251
We're with you, Maya.

482
01:46:27,261 --> 01:46:29,195
Don't worry, honey.

483
01:47:38,899 --> 01:47:40,833
Take care.

484
01:47:40,901 --> 01:47:43,165
Take care of yourself.

485
01:47:47,441 --> 01:47:49,375
I love you.

