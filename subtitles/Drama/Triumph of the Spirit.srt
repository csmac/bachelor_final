1
00:02:53,053 --> 00:02:54,816
They're watching me.

2
00:02:56,089 --> 00:02:58,489
<i>I don't know how long I was there.</i>

3
00:03:03,563 --> 00:03:06,623
<i>I stayed alive by thinking</i>
<i>of the ones I loved.</i>

4
00:03:08,869 --> 00:03:10,200
<i>Allegra.</i>

5
00:03:11,505 --> 00:03:15,236
- You're impossible.
- You're delicious. You taste like apricots.

6
00:03:20,347 --> 00:03:21,575
<i>Poppa.</i>

7
00:03:29,389 --> 00:03:32,483
<i>My brother, Avram,</i>
<i>and my best friend, Jacko.</i>

8
00:03:45,272 --> 00:03:46,830
Salamo.

9
00:03:47,841 --> 00:03:51,868
<i>Jacko and I boxed together</i>
<i>ever since we were kids on the docks.</i>

10
00:03:52,012 --> 00:03:53,104
You got it.

11
00:03:54,481 --> 00:03:56,244
You drop your guard.

12
00:03:56,483 --> 00:03:59,043
He thinks you're tired,
so that brings him in.

13
00:03:59,119 --> 00:04:02,520
Slip what's coming, then over with
a straight right. That's the punch.

14
00:04:03,757 --> 00:04:06,920
<i>What I cared about most</i>
<i>was fighting and winning.</i>

15
00:04:12,999 --> 00:04:14,227
Come on!

16
00:04:19,739 --> 00:04:21,263
Champion!

17
00:04:31,284 --> 00:04:32,342
Yes!

18
00:04:32,719 --> 00:04:34,414
One, two...

19
00:04:34,988 --> 00:04:36,751
Back to your corner!

20
00:04:36,823 --> 00:04:38,518
Three, four...

21
00:04:39,259 --> 00:04:40,851
...five, six...

22
00:04:41,127 --> 00:04:44,722
...seven, eight, nine, ten.

23
00:05:01,448 --> 00:05:02,608
Salamo!

24
00:05:03,516 --> 00:05:06,610
Ladies and gentlemen...

25
00:05:07,854 --> 00:05:09,321
...the winner...

26
00:05:10,023 --> 00:05:14,517
...and the new middleweight champion
of the Balkans...

27
00:05:14,828 --> 00:05:19,060
...Salonika's own Salamo Arouch!

28
00:05:46,159 --> 00:05:48,787
<i>Then the Nazis occupied Greece.</i>

29
00:06:11,418 --> 00:06:14,319
<i>We couldn't eat in cafés like everyone else.</i>

30
00:06:14,954 --> 00:06:17,388
<i>We couldn't ride in cars and buses.</i>

31
00:06:18,425 --> 00:06:21,758
<i>We couldn't even walk on the streets</i>
<i>of our own city.</i>

32
00:06:22,896 --> 00:06:27,560
<i>Our homes and jobs were taken away,</i>
<i>and we were forced to live in ghettos.</i>

33
00:06:27,634 --> 00:06:30,330
<i>Anyone found outside the walls was shot.</i>

34
00:06:40,213 --> 00:06:43,512
It's tomorrow morning!
We're leaving, 6:00 sharp.

35
00:06:44,284 --> 00:06:47,720
The train station, 6:00!
One suitcase apiece is all you're allowed!

36
00:06:47,787 --> 00:06:50,688
Tomorrow morning at 6:00,
the train station!

37
00:06:51,858 --> 00:06:53,223
What is it?

38
00:06:53,693 --> 00:06:55,456
Train station, 6:00 in the morning!

39
00:07:05,839 --> 00:07:10,037
<i>Every day, we were taken by the SS</i>
<i>to do forced labor.</i>

40
00:07:12,178 --> 00:07:14,009
We're being shipped out. Everyone goes.

41
00:07:14,080 --> 00:07:16,674
- When?
- Tomorrow, 6:00 in the morning.

42
00:07:17,150 --> 00:07:20,210
- You see, the rumors were right.
- Where are they sending us?

43
00:07:20,320 --> 00:07:22,914
- Who knows.
- Won't be the Riviera, you can bet on that.

44
00:07:23,056 --> 00:07:25,490
Baby, come to Grandpa.

45
00:07:29,062 --> 00:07:32,896
Don't cry. As long as we're together,
it's not the end of the world.

46
00:07:32,966 --> 00:07:34,228
Come on, we have to pack.

47
00:07:34,300 --> 00:07:36,700
Poppa, I want to marry Sarah
tonight before we go.

48
00:07:36,770 --> 00:07:37,759
There's no time.

49
00:07:37,837 --> 00:07:39,464
- We love each other.
- No!

50
00:07:39,539 --> 00:07:42,997
- Maybe they'll let us stay together.
- Come, Salamo. We have to pack.

51
00:07:43,076 --> 00:07:47,979
- For once in your life, will you listen to me?
- Why shouldn't they get married?

52
00:07:49,115 --> 00:07:51,106
- Where are you going?
- I'll be back.

53
00:07:51,184 --> 00:07:52,208
We don't have much time!

54
00:08:13,373 --> 00:08:17,366
<i>I believe in a high and eternal justice</i>
<i>that is helping us.</i>

55
00:08:25,285 --> 00:08:29,619
<i>Even when I was an unknown little soldier,</i>
<i>I believed it couldn't be possible...</i>

56
00:08:29,756 --> 00:08:32,850
<i>... that my people were bound to go down.</i>

57
00:08:49,809 --> 00:08:53,040
<i>And because I believed in the help</i>
<i>of a higher creature...</i>

58
00:08:53,112 --> 00:08:57,776
<i>... that's why I started my struggle</i>
<i>as a small and quite unknown quantity...</i>

59
00:08:57,851 --> 00:08:59,910
<i>... in this big German Reich.</i>

60
00:09:05,792 --> 00:09:09,751
<i>Allegra and her family were hiding</i>
<i>in another part of the city.</i>

61
00:09:11,231 --> 00:09:15,691
<i>The safest place for us to meet</i>
<i>was in the darkness of a movie theater.</i>

62
00:09:17,237 --> 00:09:20,206
<i>It is impossible my people will go down.</i>

63
00:09:28,147 --> 00:09:31,742
<i>And are now more entitled than before</i>
<i>to live in peace.</i>

64
00:09:33,186 --> 00:09:35,120
I missed you so much.

65
00:09:37,290 --> 00:09:42,057
A thousand times, I wished my family
had stayed in the ghetto instead of hiding.

66
00:09:49,469 --> 00:09:51,801
You still taste like apricots.

67
00:09:52,038 --> 00:09:54,905
I can't bear not knowing where you'll be.

68
00:10:11,891 --> 00:10:14,587
Jacko left. He has joined the partisans.

69
00:10:16,596 --> 00:10:18,928
He didn't show up for relocation...

70
00:10:19,666 --> 00:10:21,998
...and the SS shot his family.

71
00:10:25,872 --> 00:10:30,002
<i>Hitler has now finished his speech,</i>
<i>and all together the company is singing.</i>

72
00:10:30,109 --> 00:10:31,508
I better go.

73
00:10:39,385 --> 00:10:40,852
I have to go.

74
00:11:19,559 --> 00:11:21,891
May I see your documents, please?

75
00:11:45,785 --> 00:11:49,585
<i>Blessed art Thou, O Lord our God,</i>
<i>King of the Universe...</i>

76
00:11:49,655 --> 00:11:52,590
<i>... who has sanctified us</i>
<i>by thy commandments.</i>

77
00:11:53,059 --> 00:11:56,927
Blessed art Thou, O Lord,
who sanctifieth thy people Israel...

78
00:11:56,996 --> 00:11:59,487
...by the sacred covenant of the wedding.

79
00:11:59,766 --> 00:12:01,563
Do you have the rings?

80
00:12:22,822 --> 00:12:25,222
Blessed art Thou, O Lord our God...

81
00:12:26,059 --> 00:12:29,995
...King of the Universe,
who hast created joy and gladness...

82
00:12:30,730 --> 00:12:32,527
...mirth and exultation...

83
00:12:33,199 --> 00:12:34,496
...pleasure and delight.

84
00:12:39,672 --> 00:12:42,266
<i>They promised us a new life in Poland.</i>

85
00:12:43,009 --> 00:12:47,946
<i>Momma brought her best silver,</i>
<i>Poppa took the family photo album.</i>

86
00:12:49,715 --> 00:12:52,206
<i>Move to your assigned car.</i>

87
00:12:52,652 --> 00:12:54,381
<i>Remain calm!</i>

88
00:12:54,787 --> 00:12:59,019
<i>Do as you are told,</i>
<i>and everything will be all right!</i>

89
00:13:00,026 --> 00:13:02,586
<i>Please move quickly!</i>

90
00:13:04,730 --> 00:13:06,755
Where are the children?

91
00:13:08,401 --> 00:13:09,868
I'm over here!

92
00:13:17,443 --> 00:13:18,774
Quickly!

93
00:13:19,345 --> 00:13:20,539
Quickly!

94
00:13:36,796 --> 00:13:39,287
<i>For six days and nights we traveled.</i>

95
00:13:39,999 --> 00:13:42,229
<i>People suffered, and many died.</i>

96
00:13:43,703 --> 00:13:47,104
<i>We didn't know it then,</i>
<i>but we were just one family...</i>

97
00:13:47,173 --> 00:13:49,505
<i>... among more than 12 million people...</i>

98
00:13:50,009 --> 00:13:53,775
<i>... who were sent to 1,000</i>
<i>concentration camps across Europe.</i>

99
00:13:55,448 --> 00:13:58,645
<i>Our destination was the death camp...</i>

100
00:13:59,418 --> 00:14:01,352
<i>... Auschwitz-Birkenau.</i>

101
00:15:08,854 --> 00:15:09,946
Get off!

102
00:15:10,022 --> 00:15:12,149
Put your luggage here inside.

103
00:15:12,625 --> 00:15:13,990
Shut up!

104
00:15:14,060 --> 00:15:15,425
Don't talk!

105
00:15:15,761 --> 00:15:16,955
Get off!

106
00:15:19,899 --> 00:15:21,230
Salamo!

107
00:15:41,053 --> 00:15:42,077
Poppa.

108
00:15:42,154 --> 00:15:44,122
Put your luggage inside.

109
00:15:44,290 --> 00:15:45,552
Salamo!

110
00:15:58,437 --> 00:15:59,597
Julie!

111
00:16:41,647 --> 00:16:43,080
Poppa!

112
00:16:44,183 --> 00:16:45,343
Poppa!

113
00:16:46,052 --> 00:16:47,076
No!

114
00:16:47,553 --> 00:16:49,680
- No! Poppa!
- Beppo!

115
00:16:50,022 --> 00:16:52,456
- Poppa!
- Beppo!

116
00:16:52,658 --> 00:16:55,650
Beppo, what is it?

117
00:16:55,895 --> 00:16:58,295
My father's sick. They took him away!

118
00:16:59,799 --> 00:17:01,266
Where's your sister?

119
00:17:01,334 --> 00:17:02,494
Elena!

120
00:18:36,295 --> 00:18:38,263
Where are you taking her?

121
00:18:43,402 --> 00:18:44,460
No.

122
00:18:44,537 --> 00:18:46,300
- Sarah!
- Avram.

123
00:19:38,190 --> 00:19:39,623
How old are you?

124
00:19:39,925 --> 00:19:42,223
- 12.
- No, you are 16.

125
00:19:51,203 --> 00:19:52,932
Momma, where are you?

126
00:19:57,643 --> 00:20:00,703
Right. Left. Left.

127
00:20:05,651 --> 00:20:06,879
- Your age?
- 16.

128
00:20:07,319 --> 00:20:08,752
16.

129
00:20:09,555 --> 00:20:10,715
Right.

130
00:20:11,824 --> 00:20:13,189
Left.

131
00:20:15,995 --> 00:20:18,759
- What kind of work do you do?
- Stevedore.

132
00:20:20,466 --> 00:20:21,660
Left.

133
00:20:25,104 --> 00:20:26,196
Left.

134
00:20:28,641 --> 00:20:29,733
Left.

135
00:20:32,111 --> 00:20:34,011
Do you know this woman?

136
00:20:34,780 --> 00:20:36,372
She's my mother.

137
00:20:43,923 --> 00:20:45,481
Give your child to her.

138
00:20:45,558 --> 00:20:48,288
You will work,
and your child and her grandmother...

139
00:20:48,360 --> 00:20:52,057
...will be sent to a health camp
where they will be comfortable.

140
00:20:52,131 --> 00:20:54,099
Can I stay with my child?

141
00:20:57,603 --> 00:20:59,366
- Yes.
- Thank you.

142
00:21:34,406 --> 00:21:36,431
Where are they taking them?

143
00:21:42,982 --> 00:21:44,973
Things will be better now.

144
00:21:45,751 --> 00:21:49,050
First, you will be going
into the bathhouse...

145
00:21:49,121 --> 00:21:51,817
...where you will shower
and be disinfected.

146
00:21:51,890 --> 00:21:54,381
Then, you will go into the camp...

147
00:21:54,460 --> 00:21:57,020
...where your families will join you.

148
00:22:54,019 --> 00:22:58,319
Remember your numbers
so you can pick up your clothing later.

149
00:22:58,724 --> 00:23:02,091
Please tie your shoes together
and place them on the floor...

150
00:23:02,161 --> 00:23:03,594
...under your number.

151
00:23:04,296 --> 00:23:07,094
Let's take off your shoes.

152
00:23:08,400 --> 00:23:11,597
Please hurry. We have a lot to do.

153
00:23:11,770 --> 00:23:14,432
Soon you will enjoy a hot shower.

154
00:24:27,212 --> 00:24:31,808
Place all your personal possessions
on the blanket.

155
00:24:32,684 --> 00:24:34,015
Watches...

156
00:24:34,820 --> 00:24:37,721
...rings, money...

157
00:24:38,423 --> 00:24:39,685
...keys...

158
00:24:40,192 --> 00:24:41,716
...everything.

159
00:24:43,328 --> 00:24:46,820
Anyone found with anything
on his person...

160
00:24:47,933 --> 00:24:49,400
...will be shot.

161
00:24:58,510 --> 00:25:00,808
Now, take off all your clothing.

162
00:25:05,017 --> 00:25:06,279
Please...

163
00:25:07,619 --> 00:25:09,484
...take off your clothes.

164
00:26:18,457 --> 00:26:19,924
No talking!

165
00:26:24,263 --> 00:26:25,855
I said no talking!

166
00:27:30,262 --> 00:27:31,854
- What is he saying?
- Quiet!

167
00:27:39,171 --> 00:27:41,696
Line up! Row of five, over there! Move!

168
00:27:42,341 --> 00:27:43,433
Move!

169
00:27:49,348 --> 00:27:50,576
Row one!

170
00:28:11,570 --> 00:28:12,935
Right hand!

171
00:28:23,415 --> 00:28:25,007
I said right hand!

172
00:28:38,196 --> 00:28:40,687
Listen. I'm only going to say this once.

173
00:28:41,633 --> 00:28:44,363
For those that can hear me, tell the rest.

174
00:28:45,937 --> 00:28:48,735
First come the SS, our lord and masters.

175
00:28:49,307 --> 00:28:51,775
Then comes our block <i>Altester,</i> Kyr.

176
00:28:52,778 --> 00:28:56,043
Then comes the assistants, Otto and me.

177
00:28:57,716 --> 00:28:59,411
Then come the rats.

178
00:29:00,452 --> 00:29:02,181
Then come the lice.

179
00:29:02,921 --> 00:29:04,582
And then come you.

180
00:29:05,757 --> 00:29:07,850
You have but one purpose...

181
00:29:08,360 --> 00:29:10,453
...to work and obey orders.

182
00:29:11,963 --> 00:29:14,193
- Sick man here!
- Shut up!

183
00:29:14,266 --> 00:29:17,167
- This man needs help!
- I said shut up!

184
00:29:47,966 --> 00:29:49,331
Kyr.

185
00:29:50,635 --> 00:29:51,966
Come here.

186
00:30:03,081 --> 00:30:06,482
Into the barracks.
You'll be assigned places. Move!

187
00:30:52,164 --> 00:30:54,758
You'll be assigned the bottom bunks!

188
00:30:54,833 --> 00:30:56,323
Remember them!

189
00:30:57,636 --> 00:30:58,830
Let's go!

190
00:31:19,925 --> 00:31:21,722
Remember these places!

191
00:31:24,763 --> 00:31:28,392
You have five minutes till roll call.

192
00:31:30,402 --> 00:31:31,767
Hey, Greek.

193
00:31:32,704 --> 00:31:35,901
You can put him in my place down here.

194
00:31:49,120 --> 00:31:50,712
Watch your head.

195
00:31:56,361 --> 00:31:58,158
Just breathe deeply.

196
00:32:08,506 --> 00:32:10,736
Your side's a little swollen.

197
00:32:11,409 --> 00:32:13,434
You may have a cracked rib.

198
00:32:18,083 --> 00:32:19,550
My name is Janush.

199
00:32:19,784 --> 00:32:22,753
I'm Polish,
but I speak a little of your tongue.

200
00:32:24,556 --> 00:32:25,921
<i>Kapo</i> Kyr...

201
00:32:26,558 --> 00:32:28,788
...uses people like punching bags.

202
00:32:29,361 --> 00:32:31,556
He fight to entertain the SS.

203
00:32:34,933 --> 00:32:37,401
He's Maj. Rauscher's prized one.

204
00:32:42,841 --> 00:32:45,708
Roll call. We must go. All.

205
00:32:48,046 --> 00:32:50,071
You exit quickly, Greeks.

206
00:35:32,911 --> 00:35:34,242
I can't...

207
00:35:35,914 --> 00:35:37,313
...take this.

208
00:35:51,663 --> 00:35:52,960
Elena...

209
00:35:53,031 --> 00:35:55,829
...remember that summer
we spent on Crete?

210
00:35:57,602 --> 00:35:59,365
You had a boyfriend.

211
00:36:00,238 --> 00:36:01,830
What was his name?

212
00:36:02,307 --> 00:36:04,275
I know what you're up to.

213
00:36:05,910 --> 00:36:07,673
I don't want to play.

214
00:36:16,287 --> 00:36:17,879
I can't do this.

215
00:36:18,356 --> 00:36:19,823
Yes, you can.

216
00:36:20,959 --> 00:36:22,824
Damn it! Get up!

217
00:37:07,872 --> 00:37:09,533
Momma and Julie.

218
00:37:11,409 --> 00:37:13,604
Please God, they're all right.

219
00:37:15,113 --> 00:37:16,978
They're strong, Poppa.

220
00:37:17,882 --> 00:37:19,679
They'll be all right.

221
00:40:03,081 --> 00:40:04,571
Come here.

222
00:40:05,116 --> 00:40:06,174
You.

223
00:41:30,902 --> 00:41:32,699
Why were they hanged?

224
00:41:34,372 --> 00:41:35,896
The resistance.

225
00:41:37,108 --> 00:41:38,871
They were my friends.

226
00:42:14,645 --> 00:42:16,306
You do what I say.

227
00:42:17,081 --> 00:42:18,810
Remember and learn.

228
00:42:55,086 --> 00:42:56,280
Janush.

229
00:43:01,492 --> 00:43:03,255
You know everything.

230
00:43:05,129 --> 00:43:07,256
Where do they take the women?

231
00:43:08,299 --> 00:43:10,062
Do you ever see them?

232
00:43:11,903 --> 00:43:15,100
Sometimes on work details.

233
00:43:18,309 --> 00:43:19,936
And the children?

234
00:43:21,946 --> 00:43:23,174
Tell us.

235
00:43:23,781 --> 00:43:25,840
What about the children?

236
00:43:29,453 --> 00:43:30,784
They're...

237
00:43:32,456 --> 00:43:33,480
...gone.

238
00:43:33,557 --> 00:43:34,649
Gone?

239
00:43:35,526 --> 00:43:36,891
Gone where?

240
00:43:37,728 --> 00:43:38,888
Where?

241
00:44:43,594 --> 00:44:44,993
Go to sleep.

242
00:44:46,163 --> 00:44:47,824
You need your rest.

243
00:45:25,903 --> 00:45:27,598
Here comes the Gypsy.

244
00:46:00,805 --> 00:46:02,136
Get him out.

245
00:46:04,108 --> 00:46:05,507
Get him out!

246
00:47:11,108 --> 00:47:12,837
What are you doing?

247
00:47:18,482 --> 00:47:20,916
- I'm starving.
- I'm hungry, too.

248
00:47:23,587 --> 00:47:25,316
Just a little more.

249
00:47:25,823 --> 00:47:27,313
You had yours.

250
00:47:30,928 --> 00:47:34,193
- I know how you feel.
- How can you know how I feel?

251
00:47:36,300 --> 00:47:37,767
I'm pregnant.

252
00:48:38,195 --> 00:48:40,186
He wants you on the truck.

253
00:49:00,451 --> 00:49:04,217
1939, the Balkan Championship
was quite a long time ago.

254
00:49:04,355 --> 00:49:07,222
The <i>Sturmbannführer</i> wants to know
where you've fought since.

255
00:49:07,391 --> 00:49:09,586
A few bouts in the Greek Army.

256
00:49:10,561 --> 00:49:12,324
Nothing since the war.

257
00:49:16,200 --> 00:49:19,328
But I'm still strong,
and I've never lost a fight.

258
00:49:35,386 --> 00:49:38,617
May I ask the <i>Sturmbannführer</i> a question?

259
00:49:44,428 --> 00:49:45,690
Go ahead.

260
00:49:47,798 --> 00:49:50,164
The 1936 Olympics...

261
00:49:50,601 --> 00:49:52,694
...you were there, weren't you?

262
00:49:53,570 --> 00:49:55,970
I was with the Greek Olympic team.

263
00:49:56,640 --> 00:49:58,232
I saw you fight.

264
00:50:00,811 --> 00:50:02,176
You, sir...

265
00:50:03,080 --> 00:50:04,445
<i>... sehr gut.</i>

266
00:50:14,825 --> 00:50:17,521
You told us you never lost a fight.

267
00:50:18,162 --> 00:50:20,562
So where is your gold medal?

268
00:50:27,871 --> 00:50:30,271
The <i>Sturmbannführer</i>
says you must understand...

269
00:50:30,341 --> 00:50:32,775
...the officers bet on these fights.

270
00:50:32,843 --> 00:50:37,712
There is much money at stake,
and the <i>Sturmbannführer</i> wants to win.

271
00:50:38,549 --> 00:50:41,643
Please tell the <i>Sturmbannführer</i>
that I will win for him.

272
00:50:42,252 --> 00:50:44,243
He can bet his life on it.

273
00:52:48,912 --> 00:52:52,678
I've been to Salonika, before the war.
It's beautiful.

274
00:52:53,450 --> 00:52:54,917
How many rounds?

275
00:52:55,252 --> 00:52:56,446
Rounds?

276
00:52:56,787 --> 00:52:58,118
No rounds.

277
00:52:58,722 --> 00:53:01,623
You fight till one goes down
and can't get up.

278
01:00:16,759 --> 01:00:19,159
Pass it over so I can have my share.

279
01:00:19,229 --> 01:00:21,288
I fought for it. It's mine.

280
01:00:21,631 --> 01:00:24,566
Connection in this place
could help you stay alive.

281
01:00:31,641 --> 01:00:34,109
Your father's strong, but what is he...

282
01:00:34,277 --> 01:00:36,074
...55, 56?

283
01:00:38,815 --> 01:00:41,875
Just a matter of time
before he starts to fail.

284
01:01:13,650 --> 01:01:14,810
Poppa...

285
01:01:15,785 --> 01:01:17,753
...I got a prize for wining.

286
01:01:17,820 --> 01:01:20,152
- Are you all right?
- Yes.

287
01:01:29,866 --> 01:01:31,094
Avram.

288
01:01:51,220 --> 01:01:52,448
Give us a piece.

289
01:01:53,423 --> 01:01:54,617
Please.

290
01:01:55,725 --> 01:01:56,987
Please.

291
01:01:58,461 --> 01:01:59,689
Come on!

292
01:02:00,396 --> 01:02:02,125
You know us.

293
01:02:02,465 --> 01:02:03,762
Please.

294
01:02:04,367 --> 01:02:05,561
Salamo.

295
01:02:33,496 --> 01:02:34,827
Thank you.

296
01:02:50,847 --> 01:02:52,337
One for Danny.

297
01:02:53,549 --> 01:02:56,882
You're doing good, Salamo.

298
01:03:03,192 --> 01:03:05,057
Congratulations, Greek.

299
01:03:05,995 --> 01:03:09,362
I see you won,
and I hope you continue to win...

300
01:03:10,266 --> 01:03:14,396
...because the moment you lose,
you are not longer useful.

301
01:03:15,271 --> 01:03:16,533
And then...

302
01:03:17,674 --> 01:03:19,733
...it's out the chimney.

303
01:03:58,648 --> 01:04:00,081
Listen...

304
01:04:00,550 --> 01:04:04,008
...your foot no good. I know. I see.

305
01:04:04,220 --> 01:04:06,279
I could help you.

306
01:04:06,355 --> 01:04:08,016
- One piece of bread.
- No.

307
01:04:19,402 --> 01:04:21,836
Last night I dreamed it was a girl...

308
01:04:22,972 --> 01:04:26,533
...and when I woke up this morning,
I thought I felt her.

309
01:04:26,609 --> 01:04:28,975
Elena, you can't have a baby here.

310
01:04:29,512 --> 01:04:32,913
What about "Rachel"?
I always loved "Rachel."

311
01:04:33,182 --> 01:04:36,117
The SS doctors do experiments
on pregnant prisoners.

312
01:04:36,185 --> 01:04:40,019
When they're finished, the baby is dead.
The mother, too.

313
01:04:40,389 --> 01:04:41,947
At the end of the dream...

314
01:04:42,024 --> 01:04:46,051
...you and I were in the theater in Salonika,
watching her dance.

315
01:04:47,764 --> 01:04:50,562
In Lublin I was a nurse.

316
01:04:51,367 --> 01:04:53,699
For twice pieces of bread...

317
01:04:54,403 --> 01:04:55,563
...I fix.

318
01:04:56,439 --> 01:04:57,667
No baby.

319
01:04:57,907 --> 01:04:59,875
I don't want to hear it.

320
01:05:00,343 --> 01:05:01,708
One ration?

321
01:05:02,078 --> 01:05:03,306
Shut up.

322
01:05:04,480 --> 01:05:05,947
One and a half.

323
01:05:06,582 --> 01:05:08,311
I do it good.

324
01:05:19,729 --> 01:05:20,889
Greeks!

325
01:05:21,330 --> 01:05:24,993
The following numbers step out
for a special work detail.

326
01:05:36,145 --> 01:05:37,635
That's me.

327
01:05:39,115 --> 01:05:42,016
If they think they're going
to send me up a smokestack...

328
01:05:42,084 --> 01:05:44,052
...they've got a fight on their hands.

329
01:05:44,120 --> 01:05:46,816
Don't worry.
They don't kill the strong ones.

330
01:05:48,457 --> 01:05:51,426
Don't make trouble. Do as you're told.

331
01:06:25,294 --> 01:06:27,194
Your duties begin here.

332
01:06:28,364 --> 01:06:31,993
Everything of value is shipped back
to the Third Reich.

333
01:06:50,253 --> 01:06:51,948
As soon as it's safe...

334
01:06:52,321 --> 01:06:55,813
...you will take out the bodies,
and then you will wash the room.

335
01:06:56,058 --> 01:06:57,423
That's all.

336
01:06:57,760 --> 01:06:59,091
Follow me.

337
01:07:08,204 --> 01:07:11,731
You will get plenty to eat, even vodka.

338
01:07:12,642 --> 01:07:13,802
Now...

339
01:07:14,510 --> 01:07:17,240
...you go to the front of the furnaces.

340
01:07:17,380 --> 01:07:20,178
There you will see what you will be doing.

341
01:07:48,544 --> 01:07:50,034
I won't do it.

342
01:08:13,869 --> 01:08:15,632
What was he saying?

343
01:08:16,806 --> 01:08:19,707
The Greeks refused
to work in crematorium.

344
01:08:20,609 --> 01:08:21,701
What?

345
01:08:22,979 --> 01:08:25,243
- The Germans killed them.
- What?

346
01:08:27,516 --> 01:08:29,211
- Yeah.
- My God.

347
01:08:51,240 --> 01:08:53,037
They refused to work.

348
01:08:55,444 --> 01:08:56,468
So?

349
01:08:57,079 --> 01:08:58,478
All the men.

350
01:08:59,715 --> 01:09:01,012
Avram...

351
01:09:01,851 --> 01:09:04,046
...Shimon, Nikko.

352
01:10:43,652 --> 01:10:45,085
Why did he do that?

353
01:10:45,154 --> 01:10:48,282
Why?

354
01:12:17,246 --> 01:12:19,441
Six seconds left.

355
01:13:00,456 --> 01:13:02,117
These are my people.

356
01:13:05,294 --> 01:13:07,990
I wanna give this to somebody special.

357
01:13:14,770 --> 01:13:16,704
Okay. Only one moment.

358
01:14:09,725 --> 01:14:10,953
Come in.

359
01:14:20,002 --> 01:14:21,629
So you stole it?

360
01:14:23,939 --> 01:14:25,201
Sit down.

361
01:15:05,047 --> 01:15:06,639
To the Russians.

362
01:17:47,276 --> 01:17:49,801
- My clogs are gone.
- Look under the blanket.

363
01:17:49,878 --> 01:17:52,403
I'll die without them. They're gone.

364
01:17:54,583 --> 01:17:55,845
Her clogs.

365
01:17:56,585 --> 01:17:58,519
Have you seen her clogs?

366
01:17:58,587 --> 01:18:01,522
Her clogs. Who took her clogs?

367
01:18:02,190 --> 01:18:03,521
Her clogs?

368
01:18:04,927 --> 01:18:06,087
Bread.

369
01:18:06,895 --> 01:18:09,386
Bread. Tell me who took her clogs!

370
01:18:11,767 --> 01:18:12,927
Show me your clogs.

371
01:18:15,571 --> 01:18:17,004
Show them to me!

372
01:18:18,574 --> 01:18:20,041
They're mine!

373
01:18:25,781 --> 01:18:27,248
They're mine!

374
01:18:49,237 --> 01:18:50,499
Get out!

375
01:18:51,106 --> 01:18:53,267
Get out!

376
01:19:09,858 --> 01:19:11,416
I can't look at her.

377
01:19:17,799 --> 01:19:19,733
It was her or you.

378
01:19:29,811 --> 01:19:32,507
You have a new work assignment:
The steel mill.

379
01:19:32,581 --> 01:19:34,105
My father, too?

380
01:19:35,651 --> 01:19:36,913
He stays.

381
01:19:37,252 --> 01:19:39,220
It's a better assignment.

382
01:19:39,888 --> 01:19:42,413
Better food, and you work inside.

383
01:19:43,725 --> 01:19:45,590
It comes from Rauscher.

384
01:20:25,534 --> 01:20:26,694
Jacko.

385
01:21:13,682 --> 01:21:14,910
Poppa?

386
01:21:15,751 --> 01:21:18,914
Come. You should lie down.

387
01:21:19,354 --> 01:21:20,753
Come on.

388
01:21:56,558 --> 01:21:59,083
It was a month before we left.

389
01:22:00,695 --> 01:22:02,458
I was at the market.

390
01:22:04,699 --> 01:22:06,826
This woman was staring at me.

391
01:22:10,372 --> 01:22:12,966
I thought she was gonna turn me in.

392
01:22:17,612 --> 01:22:19,375
I was so frightened.

393
01:22:23,418 --> 01:22:25,443
He must have been watching...

394
01:22:26,421 --> 01:22:29,788
...because just as she started
to come towards me...

395
01:22:32,127 --> 01:22:33,594
...he came up...

396
01:22:34,596 --> 01:22:36,359
...and kissed me...

397
01:22:36,865 --> 01:22:40,164
...and said we were late
for lunch with his parents.

398
01:22:44,172 --> 01:22:45,833
And he took my arm...

399
01:22:46,508 --> 01:22:48,976
...and walked me out to the street.

400
01:22:58,153 --> 01:23:00,587
Then he took me to his apartment...

401
01:23:05,060 --> 01:23:06,857
...and he kissed me.

402
01:23:13,101 --> 01:23:14,432
I'm sorry.

403
01:23:18,974 --> 01:23:22,034
How could I have wanted you
to get rid of her?

404
01:23:42,264 --> 01:23:43,754
I'm so hungry.

405
01:23:50,205 --> 01:23:51,797
Here, take this.

406
01:23:52,207 --> 01:23:55,472
- No. You need it as much as I do.
- You need it more. Take it.

407
01:23:55,543 --> 01:23:57,773
Go on. I'll be out in a minute.

408
01:27:37,665 --> 01:27:41,032
I'm going to the gas. Tell my mother.

409
01:27:41,102 --> 01:27:43,400
Don't worry. We'll be all right.

410
01:27:54,682 --> 01:27:56,547
Put this in your mouth.

411
01:28:01,222 --> 01:28:03,156
Think about your wife.

412
01:28:18,072 --> 01:28:19,437
I am alive.

413
01:29:10,491 --> 01:29:11,890
Did you see?

414
01:29:12,927 --> 01:29:14,895
I think you're all right.

415
01:29:32,780 --> 01:29:34,771
My father's been selected.

416
01:29:42,290 --> 01:29:43,518
Come in.

417
01:29:47,495 --> 01:29:49,258
Can you do anything?

418
01:29:52,734 --> 01:29:55,168
Someone would have to take his place
on the list.

419
01:29:56,971 --> 01:29:58,563
Can you do that?

420
01:30:00,508 --> 01:30:01,998
It's too late.

421
01:34:23,671 --> 01:34:25,764
Never baby. She lie.

422
01:35:00,908 --> 01:35:03,706
It wasn't true about having a lover.

423
01:35:08,249 --> 01:35:10,046
I never had one.

424
01:36:11,145 --> 01:36:12,942
Salamo!

425
01:36:23,557 --> 01:36:26,355
We're finally moving against the bastards.

426
01:36:30,698 --> 01:36:32,427
I don't understand.

427
01:36:34,735 --> 01:36:37,636
The underground. We have explosives.

428
01:36:39,407 --> 01:36:41,739
We're going to hit a crematorium.

429
01:36:44,211 --> 01:36:46,509
Why are you telling me all this?

430
01:36:48,282 --> 01:36:50,614
'Cause when we move against them...

431
01:36:51,719 --> 01:36:53,448
...I need your help.

432
01:37:02,997 --> 01:37:04,487
Come.

433
01:40:13,621 --> 01:40:15,020
If I live...

434
01:40:17,524 --> 01:40:19,185
...l'll find you.

435
01:40:20,194 --> 01:40:21,684
I'll be there.

436
01:41:39,273 --> 01:41:40,467
Get up.

437
01:41:47,281 --> 01:41:49,078
Rauscher wants to see you.

438
01:41:49,783 --> 01:41:51,080
Now?

439
01:41:51,885 --> 01:41:53,113
Now.

440
01:41:59,693 --> 01:42:01,251
There's rumors...

441
01:42:02,229 --> 01:42:04,823
...the Russians are outside of Warsaw.

442
01:42:06,400 --> 01:42:08,391
Auschwitz is breaking up.

443
01:42:09,636 --> 01:42:12,833
Prisoners able to work
are being moved to Germany.

444
01:42:15,209 --> 01:42:19,202
The rest are up the chimney,
including the Gypsies. Hurry up.

445
01:43:17,637 --> 01:43:19,662
They know it's over.

446
01:43:40,960 --> 01:43:44,418
He told Rauscher
that they're both responsible.

447
01:43:46,132 --> 01:43:48,566
He can't go home and face his wife.

448
01:44:19,432 --> 01:44:20,524
Come here, Salamo.

449
01:44:49,395 --> 01:44:50,555
Jacko.

450
01:45:26,232 --> 01:45:28,029
He wants you to fight.

451
01:45:42,181 --> 01:45:43,944
He can barely stand.

452
01:45:44,784 --> 01:45:46,445
This isn't boxing.

453
01:46:45,044 --> 01:46:46,807
Everything is ready.

454
01:46:52,718 --> 01:46:55,516
The following men report for special duty.

455
01:46:55,688 --> 01:46:57,883
You'll get plenty to eat...

456
01:46:59,125 --> 01:47:00,490
...even vodka.

457
01:47:01,394 --> 01:47:04,295
Now, go to the front of the furnaces.

458
01:47:04,363 --> 01:47:07,230
There you will see what you'll be doing.

459
01:47:48,574 --> 01:47:49,836
Let's go!

460
01:49:47,693 --> 01:49:50,025
Jew!

461
01:49:55,568 --> 01:49:59,163
How did you carry the explosives
into the crematorium?

462
01:51:45,377 --> 01:51:46,935
<i>Salamo!</i>

463
01:52:53,913 --> 01:52:55,244
It's over.

464
01:53:44,830 --> 01:53:46,821
<i>All those I love are gone.</i>

465
01:53:47,600 --> 01:53:52,537
<i>Their faces and what happened here</i>
<i>are forever burned into my mind.</i>

466
01:53:57,176 --> 01:53:59,610
<i>How can we do this to our brothers?</i>

467
01:54:48,627 --> 01:54:51,255
<i>All I have left is my love for Allegra.</i>

468
01:54:52,998 --> 01:54:54,659
<i>I will find her...</i>

469
01:54:56,035 --> 01:54:57,832
<i>... and begin again.</i>

