1
00:00:35,828 --> 00:00:38,579
CERTIFIED COPY

2
00:01:48,817 --> 00:01:51,277
Good morning, ladies and gentlemen.

3
00:01:51,403 --> 00:01:54,947
Just a few words
while you're waiting.

4
00:01:55,032 --> 00:01:58,409
As you know,
James Miller will be joining us.

5
00:01:58,535 --> 00:02:00,536
He's a little late.

6
00:02:00,662 --> 00:02:04,749
He can't blame the traffic,
his room is upstairs.

7
00:02:04,875 --> 00:02:08,377
I hope he won't be long.

8
00:02:08,462 --> 00:02:12,882
We're here for the presentation
of his latest book

9
00:02:13,008 --> 00:02:14,717
Certified Copy

10
00:02:14,843 --> 00:02:19,472
that was awarded
best foreign essay of the year.

11
00:02:20,390 --> 00:02:23,935
Well, that's about it...

12
00:02:24,019 --> 00:02:26,020
Here's James.

13
00:02:37,032 --> 00:02:40,576
Please, take your seats.

14
00:02:40,661 --> 00:02:44,831
Autographing will be
after the conference.

15
00:02:57,052 --> 00:03:00,304
I'd intended
to briefly introduce James to you

16
00:03:00,430 --> 00:03:02,807
but who better than himself.

17
00:03:02,933 --> 00:03:04,976
Over to you.

18
00:03:05,769 --> 00:03:07,228
Good morning.

19
00:04:37,736 --> 00:04:40,363
I'd like to sincerely thank

20
00:04:40,489 --> 00:04:43,783
my friend and colleague Marco Lenzi

21
00:04:43,909 --> 00:04:46,827
whose translation

22
00:04:46,954 --> 00:04:50,164
is simply perfect.

23
00:04:50,290 --> 00:04:55,836
He's really conveyed
the spirit of my book

24
00:04:57,005 --> 00:05:00,633
and made this Italian edition possible.

25
00:05:00,759 --> 00:05:02,927
As well as our meeting today.

26
00:05:03,762 --> 00:05:08,015
Well done and thanks again.

27
00:09:39,996 --> 00:09:41,997
Will I take your bag?

28
00:09:42,123 --> 00:09:43,540
No, it's okay.

29
00:09:43,667 --> 00:09:45,167
It'll be too heavy.

30
00:09:52,175 --> 00:09:53,676
Come on.

31
00:11:03,121 --> 00:11:04,913
You ordered a cheeseburger?

32
00:11:05,040 --> 00:11:06,040
Yes.

33
00:11:06,791 --> 00:11:07,916
Fries?

34
00:11:08,001 --> 00:11:09,043
Yes.

35
00:11:09,169 --> 00:11:10,753
Double?

36
00:11:10,920 --> 00:11:12,463
You won't eat them, but yes.

37
00:11:12,589 --> 00:11:13,630
Coke?

38
00:11:13,798 --> 00:11:15,549
Can you look at me?

39
00:11:17,802 --> 00:11:19,136
Coke?

40
00:11:19,220 --> 00:11:20,346
Yes.

41
00:11:24,392 --> 00:11:25,851
How many books did you buy?

42
00:11:27,062 --> 00:11:28,145
Six.

43
00:11:29,230 --> 00:11:31,398
Lucky you didn't like it.

44
00:11:31,483 --> 00:11:32,941
I never said that.

45
00:11:33,026 --> 00:11:35,402
I heard you telling Pierre.

46
00:11:36,571 --> 00:11:41,825
I didn't say I didn't like it,
just that some parts annoyed me.

47
00:11:42,786 --> 00:11:46,205
But he explained them to you.

48
00:11:46,289 --> 00:11:48,791
Yes, but I wasn't convinced.

49
00:11:51,795 --> 00:11:53,796
How come you bought so many?

50
00:11:54,672 --> 00:11:58,300
To get you talking.
And for presents.

51
00:11:58,426 --> 00:11:59,802
Who for?

52
00:11:59,969 --> 00:12:01,428
For Marie, amongst others.

53
00:12:02,430 --> 00:12:04,932
But...
she already has it.

54
00:12:06,184 --> 00:12:09,770
Well, I want to give her
a signed copy.

55
00:12:10,522 --> 00:12:12,147
Good idea, no?

56
00:12:12,273 --> 00:12:14,691
So, you want to see him again.

57
00:12:18,363 --> 00:12:20,531
You want to see that guy again.

58
00:12:20,615 --> 00:12:22,324
Well, maybe.

59
00:12:22,450 --> 00:12:25,369
And the others, who are they for?

60
00:12:25,453 --> 00:12:27,037
One's for Alain.

61
00:12:27,205 --> 00:12:29,164
But you can't stand him!

62
00:12:29,290 --> 00:12:33,961
Right. I want to give a book I dislike
to a man I dislike.

63
00:12:34,712 --> 00:12:35,754
Can't I?

64
00:12:35,922 --> 00:12:37,923
Sure, you can.

65
00:12:39,592 --> 00:12:42,761
So, what were you saying
to that other guy?

66
00:12:47,016 --> 00:12:48,434
What other guy?

67
00:12:48,601 --> 00:12:50,227
The writer's friend.

68
00:12:51,187 --> 00:12:53,355
You have to know everything?

69
00:12:54,107 --> 00:12:58,318
You said we were buddies
who kept no secrets.

70
00:12:58,445 --> 00:13:01,530
But you were just being nosy
about Elisa, right?

71
00:13:01,656 --> 00:13:04,450
But who cares? I know it all anyway.

72
00:13:04,576 --> 00:13:06,743
Good for you.

73
00:13:07,745 --> 00:13:12,583
I know you like this James
and want to fall in love with him.

74
00:13:12,709 --> 00:13:16,336
And you gave your number
to his friend, so he'll call you.

75
00:13:16,463 --> 00:13:18,130
Not at all.

76
00:13:18,214 --> 00:13:22,634
I did give my number,
but not for what you think.

77
00:13:24,220 --> 00:13:26,680
I just want to find out more
about his book.

78
00:13:26,806 --> 00:13:29,266
It is my job after all.

79
00:13:29,392 --> 00:13:33,687
And you kept... I couldn't listen.
We had to leave.

80
00:13:33,813 --> 00:13:37,357
You weren't listening.
You kept whispering...

81
00:13:37,484 --> 00:13:41,069
Neither were you.
You kept playing with that thing.

82
00:13:42,780 --> 00:13:45,824
At least, I... I was playing

83
00:13:45,950 --> 00:13:48,952
but I also listened.
You were just starry-eyed.

84
00:13:49,078 --> 00:13:51,788
Mind your own business, okay?

85
00:13:53,541 --> 00:13:56,919
No need to get so mad.

86
00:13:57,045 --> 00:14:01,590
We're just here, having a chat...

87
00:14:01,758 --> 00:14:03,550
Yes, we're having a chat.

88
00:14:03,718 --> 00:14:06,595
I don't see why you get so annoyed.

89
00:14:06,721 --> 00:14:09,973
I'm not annoyed. You annoy me!

90
00:14:11,684 --> 00:14:14,019
And get a haircut!

91
00:14:16,189 --> 00:14:17,940
Just one thing, okay?

92
00:14:18,066 --> 00:14:20,192
One thing and then I stop.

93
00:14:22,987 --> 00:14:24,947
One question:

94
00:14:27,116 --> 00:14:29,326
Why didn't you want him

95
00:14:29,410 --> 00:14:30,452
to sign with my surname?

96
00:14:30,578 --> 00:14:33,580
You're really going too far!

97
00:14:33,748 --> 00:14:35,332
My name is Julien...

98
00:14:35,416 --> 00:14:37,376
You're getting on my nerves. Enough!

99
00:14:37,460 --> 00:14:39,461
But I do have a surname!

100
00:19:50,439 --> 00:19:51,898
Are you leaving?

101
00:19:53,025 --> 00:19:54,734
Be back for lunch?

102
00:19:54,902 --> 00:19:56,611
I don't think so.

103
00:19:56,737 --> 00:20:01,741
Remind Julien
he's got a private lesson at 2.

104
00:20:01,909 --> 00:20:03,410
Sure.

105
00:32:53,847 --> 00:32:55,264
Yes?

106
00:32:59,936 --> 00:33:01,520
No. It's impossible.

107
00:33:03,523 --> 00:33:05,691
Speak up. I can't hear you.

108
00:33:07,027 --> 00:33:09,570
No. You talk to him. I'm busy.

109
00:33:11,948 --> 00:33:14,450
I can't talk to him now.

110
00:33:16,870 --> 00:33:18,829
I don't know. Talk to him.

111
00:33:18,997 --> 00:33:20,915
I have no idea.

112
00:33:22,626 --> 00:33:24,210
I'm not far away.

113
00:33:27,422 --> 00:33:30,257
I don't know yet.

114
00:33:31,259 --> 00:33:32,259
Okay?

115
00:33:32,427 --> 00:33:34,595
Yes. I'll let you know.

116
00:33:35,597 --> 00:33:38,599
I can't right now. You see with him.

117
00:33:39,976 --> 00:33:41,644
Speak to you later.

118
00:37:17,110 --> 00:37:19,862
This is the famous Muse Polimnia,

119
00:37:19,946 --> 00:37:22,448
the portrait of a woman,

120
00:37:22,574 --> 00:37:26,911
whose dramatic story is told
on the cartel

121
00:37:26,995 --> 00:37:28,787
next to the painting.

122
00:37:28,955 --> 00:37:34,209
For years, this painting
was believed to be Roman Art.

123
00:37:34,336 --> 00:37:37,922
It wasn't until the 20th Century,
about 50 years ago,

124
00:37:38,006 --> 00:37:42,718
that it was revealed to be the work
of a skilled forger from Naples.

125
00:37:42,802 --> 00:37:45,971
However, the museum
decided to conserve

126
00:37:46,139 --> 00:37:48,682
this fabulous portrait
as an original.

127
00:37:48,808 --> 00:37:54,188
It is actually as beautiful
as the original.

128
00:37:54,356 --> 00:37:55,648
When was it made?

129
00:37:55,774 --> 00:37:58,692
In the 18th Century.

130
00:37:58,818 --> 00:38:02,738
And it was considered as an original
for 200 years.

131
00:38:02,864 --> 00:38:05,282
After the Second World War,

132
00:38:05,367 --> 00:38:10,955
in-depth research revealed
even the name of the forger

133
00:38:11,081 --> 00:38:13,082
who made this amazing work.

134
00:38:13,166 --> 00:38:17,544
The museum then decided
to keep it with great care.

135
00:38:17,671 --> 00:38:20,005
In a sense, it's our Mona Lisa.

136
00:38:20,131 --> 00:38:24,176
The original is in Herculaneum,
near Naples.

137
00:38:24,302 --> 00:38:28,180
The story of the discovery itself
is interesting.

138
00:38:28,306 --> 00:38:29,765
It was part of a Roman fresco.

139
00:38:29,891 --> 00:38:35,187
And the excavation director
happened to be from Tuscany.

140
00:38:35,397 --> 00:38:40,150
He commissioned the forger
to make this perfect copy,

141
00:38:40,318 --> 00:38:44,989
so as to claim
that it was found in his region

142
00:38:45,156 --> 00:38:48,575
and use its prestige for Tuscany.

143
00:41:37,245 --> 00:41:38,787
A coffee and a cappuccino.

144
00:41:38,913 --> 00:41:40,414
Coming up.

145
00:44:35,340 --> 00:44:37,257
It's ready.

146
00:48:26,779 --> 00:48:28,613
His coffee's going cold.

147
00:48:31,867 --> 00:48:33,535
That's how he is.

148
00:48:34,328 --> 00:48:36,871
He's a good husband though.

149
00:48:37,957 --> 00:48:39,165
Sorry?

150
00:48:39,250 --> 00:48:41,334
He's a good husband.

151
00:48:44,922 --> 00:48:46,464
How do you know?

152
00:48:46,591 --> 00:48:48,008
I can tell.

153
00:48:49,010 --> 00:48:50,719
Where do you come from?

154
00:48:50,803 --> 00:48:53,430
I'm from France.

155
00:48:53,598 --> 00:48:55,807
Where did you learn Italian?

156
00:48:57,602 --> 00:49:01,146
I've lived in Italy for five years.

157
00:49:01,314 --> 00:49:03,106
Where?

158
00:49:03,274 --> 00:49:04,524
First in Florence,

159
00:49:04,650 --> 00:49:06,526
now in Arezzo.

160
00:49:07,528 --> 00:49:10,655
How come you speak English together?

161
00:49:13,909 --> 00:49:15,744
He's English.

162
00:49:15,911 --> 00:49:18,663
And he doesn't speak your language?

163
00:49:18,748 --> 00:49:20,123
Nor Italian?

164
00:49:21,375 --> 00:49:23,668
He only speaks his own language.

165
00:49:24,503 --> 00:49:27,005
But you can speak his. Good for you.

166
00:49:28,758 --> 00:49:32,844
He's not into languages.
He's not into anything.

167
00:49:32,970 --> 00:49:35,221
Except himself and his job.

168
00:49:35,389 --> 00:49:37,098
That's good.

169
00:49:37,224 --> 00:49:40,518
- A man must love his job.
- What about us women?

170
00:49:40,603 --> 00:49:44,397
It keeps them busy.
And we live our lives.

171
00:49:44,523 --> 00:49:48,234
I didn't get married to live alone.

172
00:49:48,361 --> 00:49:51,780
I'd like to live my life
with my husband.

173
00:49:53,532 --> 00:49:56,534
Is a good husband
too much to ask for?

174
00:49:56,702 --> 00:49:58,787
Our lives can't be all that bad

175
00:49:58,954 --> 00:50:01,456
if all we can complain about

176
00:50:01,582 --> 00:50:03,416
is our husbands working too hard.

177
00:50:03,542 --> 00:50:05,126
You see,

178
00:50:05,252 --> 00:50:07,629
when there's not another woman,

179
00:50:07,797 --> 00:50:10,382
we see their job as our rival.

180
00:50:11,425 --> 00:50:15,345
We also work, but with moderation.

181
00:50:15,429 --> 00:50:18,431
Moderation is our choice,

182
00:50:18,557 --> 00:50:21,226
whereas they can't help it.

183
00:50:22,228 --> 00:50:26,940
For them, not working
is like not breathing: impossible!

184
00:50:28,567 --> 00:50:31,069
I never asked my husband to stop.

185
00:50:31,237 --> 00:50:34,280
Of course not. How could you?

186
00:50:34,407 --> 00:50:36,282
The world would simply stop.

187
00:50:36,409 --> 00:50:38,660
But we put the brakes on.

188
00:50:38,786 --> 00:50:43,665
My sister keeps encouraging
her lazy husband to work.

189
00:50:43,791 --> 00:50:46,668
There are exceptions.

190
00:50:46,836 --> 00:50:50,463
Don't you think there should be
a happy balance?

191
00:50:50,589 --> 00:50:54,050
Ideally, yes.
But that doesn't exist.

192
00:50:54,176 --> 00:50:55,802
Bring us some wine.

193
00:50:55,928 --> 00:50:59,514
Coming. It'd be stupid of us

194
00:50:59,640 --> 00:51:04,310
to ruin our lives for an ideal.

195
00:51:36,218 --> 00:51:39,429
But mum's the word.

196
00:51:39,597 --> 00:51:42,390
They don't need to know.

197
00:51:48,981 --> 00:51:53,610
But how can I put up with a husband
who's never there?

198
00:51:54,779 --> 00:51:58,948
They're never totally absent.

199
00:52:00,075 --> 00:52:03,536
He makes you a married woman.

200
00:52:03,621 --> 00:52:05,705
That's what matters.

201
00:52:06,874 --> 00:52:10,001
At my age, you understand that.

202
00:52:11,253 --> 00:52:13,379
How long have you been married?

203
00:52:14,924 --> 00:52:16,174
Fifteen years.

204
00:52:16,258 --> 00:52:18,218
Do you have children?

205
00:52:18,385 --> 00:52:19,594
Yes, a son.

206
00:52:19,720 --> 00:52:22,639
It was his birthday last week.

207
00:52:22,765 --> 00:52:26,142
He didn't even bother to call him.

208
00:52:26,227 --> 00:52:27,268
I don't believe it.

209
00:52:27,436 --> 00:52:28,561
It's true.

210
00:52:28,687 --> 00:52:31,231
All he cares about
is himself and his job.