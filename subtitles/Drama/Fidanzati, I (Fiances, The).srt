1
00:06:08,801 --> 00:06:09,768
Well?

2
00:07:47,900 --> 00:07:49,060
Carletto.

3
00:07:56,075 --> 00:07:58,236
A glass of water, please.

4
00:08:42,889 --> 00:08:44,584
- Want to dance?
- No, I don't feel like it.

5
00:08:44,857 --> 00:08:46,518
- Why not?
- I don't feel like it.

6
00:08:46,592 --> 00:08:48,719
- What's the matter?
- Nothing.

7
00:08:48,861 --> 00:08:50,522
You're in a bad mood tonight.

8
00:09:02,275 --> 00:09:06,302
Look, if I don't take it,
there are ten others who will.

9
00:09:08,314 --> 00:09:10,282
What do you think it will change?

10
00:09:17,857 --> 00:09:19,791
You get such crazy ideas.

11
00:09:31,304 --> 00:09:33,795
About a year and a half in Sicily.

12
00:09:34,874 --> 00:09:38,640
We have to build a new department,
and we need specialized workers.

13
00:09:38,911 --> 00:09:41,106
Sorry, sir, but at the moment
he's not specialized.

14
00:09:41,380 --> 00:09:43,314
We'll promote him.

15
00:09:43,416 --> 00:09:50,151
For once a poor guy like me
gets the chance to get ahead...

16
00:09:52,425 --> 00:09:54,086
and all she can do is whine.

17
00:10:39,338 --> 00:10:40,999
Come on, let's dance.

18
00:11:11,337 --> 00:11:13,032
Are you married?

19
00:11:13,439 --> 00:11:16,602
Good. Fewer complications.
You know how it is with transfers.

20
00:11:32,324 --> 00:11:33,951
How old is he?

21
00:11:34,260 --> 00:11:35,818
Seventy.

22
00:11:37,430 --> 00:11:39,227
They're treated well.

23
00:11:40,433 --> 00:11:42,731
They can't complain.

24
00:11:44,403 --> 00:11:47,736
The only problem is loneliness.

25
00:11:49,542 --> 00:11:51,442
They feel abandoned...

26
00:11:52,812 --> 00:11:55,007
and if they don't get used to it,

27
00:11:55,081 --> 00:11:58,073
after a while, they just waste away.

28
00:12:04,190 --> 00:12:05,919
Is he a relative?

29
00:12:06,325 --> 00:12:07,553
No, he's...

30
00:12:07,626 --> 00:12:09,150
Your dad?

31
00:12:09,428 --> 00:12:10,417
Yes.

32
00:12:11,931 --> 00:12:15,230
Well, if you have no other choice,

33
00:12:15,367 --> 00:12:16,994
I guess you'll have to accept it.

34
00:12:52,505 --> 00:12:54,063
What is it?

35
00:14:12,151 --> 00:14:14,119
- Are you Tommasini, the engineer?
- Yes, I am.

36
00:14:14,186 --> 00:14:16,916
Hello, I'm Mr. Lo Giudice.
It's a pleasure to meet you.

37
00:14:17,122 --> 00:14:19,283
Mr. Malpiani sends his regrets
that he couldn't come.

38
00:14:19,391 --> 00:14:20,358
Thank you, miss.

39
00:14:20,459 --> 00:14:21,926
- Is the car here?
- Yes.

40
00:14:22,027 --> 00:14:23,995
Did you have a pleasant trip?
- Yes, thank you.

41
00:14:47,319 --> 00:14:49,219
- Excuse me.
- Of course.

42
00:15:18,150 --> 00:15:21,142
- Have you been with us long?
- Three or four months.

43
00:15:22,154 --> 00:15:24,384
- Where did you get your degree?
- In Rome.

44
00:15:24,757 --> 00:15:26,384
With whom?

45
00:15:27,393 --> 00:15:29,623
Professor Tesorieri.

46
00:15:29,695 --> 00:15:30,753
Ah, yes.

47
00:15:32,298 --> 00:15:34,289
What department are you in?

48
00:15:34,400 --> 00:15:36,129
- Sulphuric.
- Really?

49
00:15:37,469 --> 00:15:39,096
How do you like it?

50
00:15:39,672 --> 00:15:41,970
Well, actually,

51
00:15:42,074 --> 00:15:44,736
I've only been there four months,
but I like it.

52
00:15:44,944 --> 00:15:48,004
I don't think I can make
an accurate judgment yet.

53
00:15:48,213 --> 00:15:51,740
I've only been there since graduation.
It hasn't been long.

54
00:15:55,387 --> 00:15:59,221
What a knucklehead!
Did you see how he came out?

55
00:15:59,725 --> 00:16:02,660
He's in the middle of rush-hour traffic
and he acts like he's in the country.

56
00:16:02,761 --> 00:16:07,755
He rushes out like a bullet.
Some people are so rude.

57
00:16:09,034 --> 00:16:14,165
That animal belongs in the wild,
not in the middle of traffic.

58
00:16:16,241 --> 00:16:20,701
Just this morning
a cyclist cut me off.

59
00:16:24,416 --> 00:16:27,977
I almost ran into a wall.

60
00:16:29,421 --> 00:16:32,390
You gotta have nerves of steel.

61
00:16:32,591 --> 00:16:38,587
These days you got cars
coming at you from every which way.

62
00:16:39,164 --> 00:16:41,530
It's unbelievable.

63
00:16:44,003 --> 00:16:45,527
Look there.

64
00:16:46,171 --> 00:16:47,934
See those lights over there?

65
00:16:49,008 --> 00:16:50,839
Those are our plants.

66
00:17:05,224 --> 00:17:06,987
Maybe later we can call Moranzi.

67
00:17:07,259 --> 00:17:10,228
I don't think we'll find him.
He does his own thing.

68
00:17:10,496 --> 00:17:12,054
- Have you had dinner?
- Yes.

69
00:17:12,164 --> 00:17:14,689
- Then thank you, and good night.
- Good night.

70
00:17:14,967 --> 00:17:18,232
- Are you staying here too?
- No, I live in town with my family.

71
00:17:25,277 --> 00:17:28,838
- Good evening. How are you?
- Fine, thanks. Welcome back.

72
00:17:28,947 --> 00:17:31,677
- Any calls?
- No calls.

73
00:17:31,750 --> 00:17:34,617
I need a room for Mr. Cabrini.
He'll be staying for three or four days.

74
00:17:34,720 --> 00:17:36,119
- Very well.
- Thank you.

75
00:17:38,390 --> 00:17:41,188
Tommasini, how are you?

76
00:17:42,694 --> 00:17:44,753
- Get settled, and I'll see you later.
- All right.

77
00:17:44,863 --> 00:17:46,023
And now...

78
00:17:46,131 --> 00:17:50,192
Mr. Cabrini can only stay
four days at the most.

79
00:17:50,302 --> 00:17:52,327
I have a reservation for that room.

80
00:17:52,438 --> 00:17:55,532
Yes, just a few days,
until we find him a place to stay.

81
00:17:56,008 --> 00:17:58,340
Will I have my usual room?
- Of course.

82
00:19:08,447 --> 00:19:10,312
The spaghetti is ready.

83
00:19:10,516 --> 00:19:13,007
- And the chicken.
- All right.

84
00:19:44,550 --> 00:19:46,245
What would you like?
The kitchen's closing.

85
00:19:47,519 --> 00:19:50,818
- What do you have?
- Do you know what time it is?

86
00:19:50,889 --> 00:19:52,948
We're supposed to close at 9:30.

87
00:19:53,225 --> 00:19:55,386
- I didn't know.
- Spaghetti or soup?

88
00:19:55,494 --> 00:19:57,462
- It doesn't matter.
- Do you have coupons?

89
00:19:57,663 --> 00:20:00,996
- No, I just got here.
- You'll have to pay me later, then.

90
00:20:01,066 --> 00:20:02,658
Very well. Thank you.

91
00:20:09,641 --> 00:20:11,666
Coming in this late?

92
00:20:11,743 --> 00:20:13,734
You're working too much, sir.

93
00:20:20,052 --> 00:20:25,183
It may surprise you, but waiting
tables is exhausting work.

94
00:20:27,092 --> 00:20:30,061
Can you imagine serving
a hundred meals, just the two of us?

95
00:20:30,128 --> 00:20:32,062
Not to mention lunch.

96
00:20:32,965 --> 00:20:34,330
Ninety, a hundred.

97
00:20:35,300 --> 00:20:37,700
I've done 150 on occasion.

98
00:20:37,903 --> 00:20:42,966
Then, when I get home,
I have to start all over again.

99
00:20:43,275 --> 00:20:45,436
My wife's not there,
so I have to manage on my own.

100
00:20:45,510 --> 00:20:49,674
Our little boy is in the hospital,
so she's staying with my mother.

101
00:20:56,355 --> 00:21:02,385
Two months and they still
don't know what's wrong with him.

102
00:21:02,928 --> 00:21:06,989
The local doctor always said
it was an ear infection.

103
00:21:07,532 --> 00:21:10,763
Then we took him to a specialist,
and he said it wasn't.

104
00:21:11,536 --> 00:21:13,367
Is he in pain?

105
00:21:13,639 --> 00:21:16,005
How can we be sure?
He's only a few months old.

106
00:21:16,275 --> 00:21:19,335
He cries and spits up
everything he eats.

107
00:21:19,845 --> 00:21:21,005
I'm sorry.

108
00:21:21,079 --> 00:21:23,604
- Spaghetti is up.
- Excuse me.

109
00:21:24,082 --> 00:21:25,606
Of course.

110
00:22:08,593 --> 00:22:10,527
Good evening.
Please don't get up.

111
00:22:10,796 --> 00:22:12,957
Mr. Tommasini
asked me to tell you

112
00:22:13,031 --> 00:22:16,023
that in the morning you're to report
to the plant and ask for Bertinotti.

113
00:22:16,301 --> 00:22:18,166
All right. Thank you.

114
00:22:53,972 --> 00:22:55,906
Number 17, please.

115
00:22:57,709 --> 00:22:59,472
Thanks. Good night.

116
00:23:00,579 --> 00:23:03,070
- Wake-up call at 7:45.
- Okay.

117
00:23:03,448 --> 00:23:05,416
- I'm calling Milan.
- All right.

118
00:23:05,517 --> 00:23:06,882
Number 30.

119
00:23:08,019 --> 00:23:09,953
39. Thanks.

120
00:23:10,322 --> 00:23:11,914
Number 24.

121
00:23:13,658 --> 00:23:16,684
Excuse me, I have to be at the plant
tomorrow morning by 8:00.

122
00:23:16,762 --> 00:23:19,788
There's a bus at 7:30.
It'll get you there by 8:00.

123
00:23:21,600 --> 00:23:23,568
I'm going out.
Good night.

124
00:24:20,091 --> 00:24:23,219
If I don't take it,
there are ten others who will.

125
00:24:24,362 --> 00:24:27,388
They're going to promote me
to specialized worker.

126
00:24:29,334 --> 00:24:31,529
What are you afraid of?

127
00:24:32,270 --> 00:24:34,898
You think I'm just going to disappear?

128
00:27:24,609 --> 00:27:25,974
Good night.

129
00:27:50,568 --> 00:27:52,195
Who is it?

130
00:27:58,977 --> 00:28:00,945
- Hello?
- Your wake-up call.

131
00:28:01,046 --> 00:28:02,673
Oh, thank you. Sorry.

132
00:30:25,890 --> 00:30:28,450
Excuse me. I'm supposed
to report to Mr. Bertinotti.

133
00:30:28,660 --> 00:30:30,059
Second floor.

134
00:30:31,996 --> 00:30:35,056
- Mr. Bertinotti, please.
- He'll be right here. Have a seat.

135
00:30:35,600 --> 00:30:37,363
Have a seat.

136
00:30:37,735 --> 00:30:42,263
Paging Mr. Padellaro.

137
00:30:47,579 --> 00:30:50,776
Salvatore Clementelli is
needed urgently at PS3.

138
00:31:02,193 --> 00:31:03,820
Please come in.

139
00:31:04,629 --> 00:31:07,393
Mr. Bertinotti
is busy at the moment.

140
00:31:07,665 --> 00:31:11,601
Meanwhile, here are your ID cards.
Excuse me just a moment.

141
00:31:14,873 --> 00:31:17,034
Come with me.
Can we come in?

142
00:31:17,141 --> 00:31:18,540
Excuse me.

143
00:31:18,643 --> 00:31:21,578
I told you,
only your daughter can come in.

144
00:31:21,779 --> 00:31:24,714
If all the employees brought
their families to work...

145
00:33:16,661 --> 00:33:19,425
They won't wear
their protective eye gear.

146
00:33:20,264 --> 00:33:23,324
They've been working like this
for too many years.

147
00:33:23,801 --> 00:33:27,396
They have a different work ethic,
and it goes back generations.

148
00:33:27,538 --> 00:33:30,132
Just think: At first, when it rained,
they wouldn't come to work.

149
00:33:30,341 --> 00:33:33,504
- You're kidding.
- Everything stops here when it rains.

150
00:33:33,778 --> 00:33:36,770
They don't have an industrial mentality,
and there's nothing you can do.

151
00:33:37,048 --> 00:33:40,449
We need to start all over.
With the new generation, who knows?

152
00:35:59,056 --> 00:36:00,216
Hello.

153
00:36:20,478 --> 00:36:26,007
Last night, on Via Enrico Toti,

154
00:36:26,284 --> 00:36:30,084
or on Via Garibaldi,
or at the Splendido Theater,

155
00:36:30,521 --> 00:36:34,958
a wallet containing 5000 lire

156
00:36:35,826 --> 00:36:39,592
and other important papers
was lost.

157
00:36:40,565 --> 00:36:45,867
Whoever finds it is welcome
to keep the 5000 lire

158
00:36:46,304 --> 00:36:51,606
but is requested to return
the papers to their owner.

159
00:38:21,999 --> 00:38:26,493
Children, how many times
do I have to tell you?

160
00:38:26,570 --> 00:38:31,735
When you come into church,
you mustn't talk or run

161
00:38:31,809 --> 00:38:35,438
or laugh or spit!

162
00:38:35,713 --> 00:38:37,840
You must pray.

163
00:38:44,488 --> 00:38:46,319
Keep still, children.

164
00:38:54,999 --> 00:38:59,368
Go back to your seats.

165
00:40:19,750 --> 00:40:21,047
What can I do for you?

166
00:40:21,152 --> 00:40:23,245
- I'm looking for a room.
- Come in.

167
00:40:35,699 --> 00:40:37,326
Come in, please.

168
00:40:37,802 --> 00:40:40,771
When do you need it?
- As soon as possible.

169
00:40:41,038 --> 00:40:45,304
I'm staying at the plant's hostel,
but I don't know how long I can stay.

170
00:40:47,745 --> 00:40:51,203
Everything's full right now.

171
00:40:51,282 --> 00:40:53,273
There's that bed in the corner.

172
00:40:53,918 --> 00:40:56,682
It might be available in a few days.

173
00:40:56,887 --> 00:40:59,583
Come and I'll show you another room.

174
00:41:03,961 --> 00:41:08,421
This is available right now,
but it's a single.

175
00:41:08,699 --> 00:41:11,998
It goes for between
10,000 and 12,000 a month.

176
00:41:12,937 --> 00:41:17,340
This one even has a nice terrace
with a view of the street.

177
00:41:17,441 --> 00:41:21,172
You can get some fresh air.
- How much is it?

178
00:41:21,812 --> 00:41:25,213
When the plant opened it was different,
but now it's hardly worth it.

179
00:41:25,483 --> 00:41:29,249
They've wised up
and hiked up all the prices.

180
00:41:29,520 --> 00:41:33,820
Things cost the same as in Milan now,
and some things even more.

181
00:41:34,859 --> 00:41:39,023
Before I got transferred, I'd have a little
left at the end of the month. Not now.

182
00:41:39,129 --> 00:41:41,393
All things considered,
it's not really such a good deal

183
00:41:41,499 --> 00:41:43,399
to come work down here anymore.

184
00:41:43,501 --> 00:41:45,696
Maybe if your family
comes with you.

185
00:41:46,570 --> 00:41:49,801
A lot of them bring their wives,
but it's a heck of a life.

186
00:41:49,874 --> 00:41:51,205
They can't handle it.

187
00:41:51,275 --> 00:41:53,334
The people down here aren't bad.

188
00:41:53,611 --> 00:41:56,307
They've always been hungry,
so it's only natural

189
00:41:56,413 --> 00:41:59,610
that when they see a little
cash going around, they want some.

190
00:41:59,717 --> 00:42:02,686
You know there are school
teachers working in construction?

191
00:42:03,320 --> 00:42:06,983
With the first money they set aside,
they start building a house.

192
00:42:07,057 --> 00:42:10,322
They don't care if they have
to abandon it halfway through.

193
00:42:10,594 --> 00:42:13,825
They're happy just having four bricks
one on top of the other.

194
00:42:13,898 --> 00:42:15,866
They eat bread and lemons

195
00:42:15,966 --> 00:42:19,094
and save their money
to pay off debts.

196
00:42:19,203 --> 00:42:21,569
They plant oranges
and tangerines,

197
00:42:21,639 --> 00:42:24,733
but mostly lemons and tangerines,
because they're fast growers.

198
00:42:25,209 --> 00:42:28,508
In a few years, as soon as
the orchard starts making some money,

199
00:42:28,779 --> 00:42:31,441
they leave the plant and go
to live on their plot of land.

200
00:42:31,549 --> 00:42:36,486
It's not that they're lazy.
They do their work.

201
00:42:36,954 --> 00:42:39,388
They're just a little crazy.

202
00:42:39,857 --> 00:42:42,223
The sun's real hot,

203
00:42:42,660 --> 00:42:45,527
and they never have a chance
to blow off steam.

204
00:42:46,830 --> 00:42:48,559
Want a cigarette?
- No, thanks.

205
00:42:48,666 --> 00:42:50,930
- Take one.
- No, I just had one.

206
00:42:51,535 --> 00:42:53,730
I was one of the first
to come down here.

207
00:42:55,039 --> 00:42:57,701
You should have seen it.
It was full of Algerians back then.

208
00:42:58,576 --> 00:43:01,170
They'd bring their lunch
wrapped up in a rag.

209
00:43:01,812 --> 00:43:05,976
Now they all carry briefcases
and look like executives.

210
00:43:07,484 --> 00:43:09,315
They've moved up in the world.

211
00:43:10,387 --> 00:43:10,554
It's amazing.
Wait till you see them.

212
00:43:10,554 --> 00:43:14,581
It's amazing.
Wait till you see them.

213
00:43:15,125 --> 00:43:18,686
It only lasts a week,
but they party enough to last all year.

214
00:43:19,496 --> 00:43:21,691
You should see the getups
the women put on.

215
00:43:21,966 --> 00:43:25,094
They don't even talk,
so no one will recognize them.

216
00:43:25,202 --> 00:43:29,138
What are you honking about?
Just get off your mule?

217
00:43:29,406 --> 00:43:32,807
You won't catch me
driving around here.

218
00:43:33,510 --> 00:43:36,604
Who lets them drive? They'd be
better off driving bumper cars.

219
00:44:13,450 --> 00:44:16,817
Hey, idiot! Watch your step.
I'm not wearing clogs, you know.

220
00:44:19,723 --> 00:44:22,783
Keep together
or we'll lose each other.

221
00:44:23,394 --> 00:44:25,385
If we get separated,
meet up later at the car.

222
00:44:25,496 --> 00:44:27,589
Coming through.

223
00:44:27,798 --> 00:44:30,232
Stay back, knucklehead.

224
00:44:31,802 --> 00:44:33,997
Hey, guys, you gotta watch out.

225
00:45:38,302 --> 00:45:40,327
Look what I found.

226
00:45:40,771 --> 00:45:42,295
She looks like a beached whale.

227
00:45:42,406 --> 00:45:45,341
Leave some for me.

228
00:45:45,676 --> 00:45:47,439
I hope she isn't a man.

229
00:45:47,711 --> 00:45:50,475
With this getup, who's to say?

230
00:46:16,373 --> 00:46:18,273
He's been drinking again.

231
00:46:30,654 --> 00:46:32,349
May I come in?

232
00:46:34,858 --> 00:46:37,850
These are the papers, ma'am.
If he really can't take it -

233
00:46:37,928 --> 00:46:40,021
- All right.
- I've signed them.

234
00:46:40,130 --> 00:46:43,531
It's no use.
He'll just have to stop.

235
00:46:43,801 --> 00:46:48,363
If an old man can't even have
a glass of wine, what's left to live for?

236
00:46:54,144 --> 00:46:56,339
- Where are you from?
- Catania. How about you?

237
00:46:56,446 --> 00:46:57,845
I'm from up north.

238
00:46:57,915 --> 00:47:01,578
Poor thing, so far from home.

239
00:47:01,852 --> 00:47:04,218
I'll give you a kiss,
just to console you.

240
00:47:06,924 --> 00:47:10,553
- No, that's not allowed.
- It won't be any fun then.

241
00:47:10,627 --> 00:47:12,424
You'll see. It'll be wonderful.

242
00:49:46,283 --> 00:49:49,116
I'm not paying.

243
00:49:50,420 --> 00:49:51,887
Have fun paying up!

244
00:49:52,155 --> 00:49:54,020
Oh, go to bed!

245
00:49:56,760 --> 00:49:58,694
You're a real piece of work.

246
00:49:58,895 --> 00:50:00,863
Not you, sir. I meant the horse.

247
00:51:35,725 --> 00:51:37,352
Who's there?

248
00:52:56,706 --> 00:52:58,173
Quick, run!

249
00:53:03,046 --> 00:53:04,308
Hey!

250
00:53:04,681 --> 00:53:08,014
I saw you!
Where did you go?

251
00:53:09,019 --> 00:53:11,681
Show your face, if you've got guts.
Come on.

252
00:53:31,675 --> 00:53:34,405
Are you done
making a racket out here?

253
00:53:34,678 --> 00:53:38,114
Aren't you tired of fooling around?
- They're the ones playing around.

254
00:53:38,448 --> 00:53:40,678
Sure, it's their fault.
Go to bed. It's late.

255
00:54:15,619 --> 00:54:17,177
Just a minute.

256
00:54:17,254 --> 00:54:19,119
Sorry.

257
00:54:21,858 --> 00:54:24,190
Thank you.
Very kind of you.

258
00:54:26,563 --> 00:54:27,723
Blessed mother.

259
00:55:39,202 --> 00:55:41,397
Here are the keys.

260
00:55:41,671 --> 00:55:44,970
One for the apartment
and one for the front door.

261
00:55:45,241 --> 00:55:47,766
Good. By the way,
I couldn't open the bathroom door.

262
00:55:47,877 --> 00:55:51,938
It's a little tricky.
Come and I'll show you how it works.

263
00:55:52,449 --> 00:55:54,883
When you open it,
you have to hit it hard.

264
00:55:55,819 --> 00:55:58,515
Then give it a push, see?

265
00:55:59,122 --> 00:56:02,455
Please have a seat.
Someone will be right with you.

266
00:58:59,969 --> 00:59:02,199
Just a minute, sir.

267
00:59:05,775 --> 00:59:09,233
This letter came for you
two days ago.

268
00:59:09,512 --> 00:59:11,571
I always put the mail here.

269
00:59:11,848 --> 00:59:14,544
You should always check.
Sometimes I forget.

270
00:59:14,717 --> 00:59:17,242
You've been shopping, I see.

271
00:59:17,353 --> 00:59:19,651
Mom, the door!

272
00:59:25,461 --> 00:59:27,156
Excuse me.

273
01:02:41,824 --> 01:02:43,724
Who told you that?

274
01:02:49,999 --> 01:02:51,864
It was Marco, wasn't it?

275
01:02:55,104 --> 01:02:56,366
That dirty, rotten -

276
01:02:56,472 --> 01:02:59,100
What are you so angry about?

277
01:03:00,376 --> 01:03:02,674
I'm the one who should be angry.

278
01:03:02,945 --> 01:03:05,004
You mean you're not angry?

279
01:03:10,486 --> 01:03:12,351
I'm insulted and disappointed.

280
01:03:14,190 --> 01:03:16,488
I'm going to ask you a question,

281
01:03:17,493 --> 01:03:19,825
and I want a straight answer.

282
01:03:30,706 --> 01:03:32,606
Evening.

283
01:03:44,520 --> 01:03:46,181
Do you still want to marry me?

284
01:03:49,292 --> 01:03:50,759
Now?

285
01:03:50,927 --> 01:03:52,690
How would we manage it?

286
01:03:54,897 --> 01:03:57,092
I didn't ask you when.

287
01:03:59,168 --> 01:04:03,002
I just want to know
if you still want to marry me.

288
01:04:17,753 --> 01:04:19,220
There's no water.

289
01:04:19,388 --> 01:04:20,582
Why not?

290
01:04:20,690 --> 01:04:23,420
They turn it off on Sundays.

291
01:04:27,196 --> 01:04:29,721
- Would you like a little of mine?
- No, thank you.

292
01:04:29,899 --> 01:04:33,266
- It's nice and cold.
- Thanks all the same. Don't bother.

293
01:04:33,336 --> 01:04:36,237
It's no bother. Come on.

294
01:04:36,339 --> 01:04:40,070
Come on.
I'll pour it for you.

295
01:04:46,015 --> 01:04:48,506
Good thing I kept a little aside.

296
01:04:48,618 --> 01:04:50,108
That was smart, huh?

297
01:04:51,454 --> 01:04:53,115
It's so hot.

298
01:04:56,926 --> 01:04:58,655
Shall I pour a little on your back?

299
01:05:00,997 --> 01:05:03,397
Nice and cold, eh?

300
01:05:03,532 --> 01:05:05,193
You like that?

301
01:05:05,268 --> 01:05:06,701
Thank you. That's enough.

302
01:05:50,646 --> 01:05:54,548
In your letter
you wrote only of my father...

303
01:05:56,052 --> 01:05:57,679
and said nothing about yourself.

304
01:06:00,089 --> 01:06:01,818
How are you?

305
01:06:02,758 --> 01:06:05,318
Are you keeping your spirits up?

306
01:06:12,835 --> 01:06:14,496
Why haven't you written?

307
01:06:16,806 --> 01:06:21,300
I sent my postcard two weeks ago,
and still no answer from you.

308
01:06:23,212 --> 01:06:27,171
My address is the same as before,
at the pensione.

309
01:06:28,217 --> 01:06:30,185
The last address I sent you.

310
01:06:31,620 --> 01:06:35,317
My regards to everyone,
and a kiss for you.

311
01:06:42,598 --> 01:06:44,259
Dear Giovanni,

312
01:06:44,900 --> 01:06:47,926
I received your postcard
and your letter.

313
01:06:49,572 --> 01:06:54,532
I hesitated to write back.
I wasn't sure you wanted me to.

314
01:06:56,278 --> 01:06:59,611
When I received your letter,
I got scared.

315
01:07:00,282 --> 01:07:03,479
I wanted to open it right away,
but I couldn't work up the courage.

316
01:07:05,287 --> 01:07:08,654
I felt excited and happy
running up the stairs.

317
01:07:10,359 --> 01:07:15,160
But then suddenly
that happiness frightened me.

318
01:07:17,066 --> 01:07:19,967
I was afraid. I don't know why.

319
01:07:21,103 --> 01:07:23,071
A thousand thoughts
raced through my mind,

320
01:07:23,339 --> 01:07:26,399
most of them disturbing.

321
01:07:28,077 --> 01:07:32,013
I even thought
this might be your last letter.

322
01:07:33,215 --> 01:07:35,615
I admit I was afraid.

323
01:07:36,419 --> 01:07:39,684
I had lost faith, and hope as well.

324
01:07:41,724 --> 01:07:44,693
Now I'm sorry I thought ill of you.

325
01:07:50,032 --> 01:07:52,091
I went to see your father.

326
01:07:52,368 --> 01:07:56,031
He's fine,
and he sends you his love.

327
01:07:57,106 --> 01:08:00,337
In the evenings he sits in front
of Mrs. Seminari's open door.

328
01:08:00,609 --> 01:08:02,600
They keep it open because of the heat,

329
01:08:03,245 --> 01:08:05,577
and this way he can watch TV.

330
01:08:06,115 --> 01:08:08,743
At least it keeps him
from going to the bar.

331
01:08:09,485 --> 01:08:14,354
In other words, the reacting gas,
that is, the ethylene,

332
01:08:14,657 --> 01:08:17,558
mixed with air and an inhibiting agent,

333
01:08:17,827 --> 01:08:19,954
passes through
an exchange chamber,

334
01:08:20,696 --> 01:08:24,928
where the mixture is heated
and then sent to a reactor.

335
01:08:29,138 --> 01:08:30,730
My dear Liliana,

336
01:08:30,873 --> 01:08:33,068
the heat here has become unbearable:

337
01:08:33,342 --> 01:08:36,436
up to 140� in the sun
and 113� in the shade.

338
01:08:37,246 --> 01:08:40,010
Only the classroom
at the plant is bearable,

339
01:08:40,282 --> 01:08:42,341
because it's air-conditioned.

340
01:08:43,586 --> 01:08:45,679
I bet it's hot up there too.

341
01:08:47,356 --> 01:08:49,517
Do you still go dancing at night?

342
01:08:50,426 --> 01:08:52,360
I've stopped going.

343
01:08:52,828 --> 01:08:54,728
There are no dance halls here.

344
01:08:56,398 --> 01:08:58,332
But that's not the only reason.

345
01:08:58,434 --> 01:09:02,268
I was used to dancing with you.
I'm not comfortable with other girls.

346
01:09:02,738 --> 01:09:04,706
On Sundays I don't know what to do.

347
01:09:05,741 --> 01:09:07,641
I just wander around aimlessly.

348
01:09:07,743 --> 01:09:10,473
Besides, without my motorcycle,
I can't really go anywhere.

349
01:09:11,981 --> 01:09:15,781
Write me when you get the chance,
because it's nice to get home at night

350
01:09:15,851 --> 01:09:19,218
and have the lady at the pensione
tell me I have mail.

351
01:09:20,089 --> 01:09:22,580
Regards to my father
and everyone else.

352
01:09:22,691 --> 01:09:24,215
And a kiss for you.

353
01:09:25,628 --> 01:09:27,357
Dear Giovanni,

354
01:09:28,297 --> 01:09:31,460
it's been so many days
since you left.

355
01:09:32,234 --> 01:09:34,031
Over two months.

356
01:09:35,070 --> 01:09:38,039
I don't go dancing anymore.

357
01:09:38,974 --> 01:09:41,340
I don't want to go to the Speranza

358
01:09:42,044 --> 01:09:43,909
and see all our friends.

359
01:09:44,013 --> 01:09:46,311
I don't want them asking about you...

360
01:09:47,616 --> 01:09:50,813
with those insinuating smiles -
you know how people are.

361
01:09:51,654 --> 01:09:53,986
I'd be embarrassed.

362
01:09:56,625 --> 01:09:59,492
We have so many memories
from that place.

363
01:09:59,728 --> 01:10:01,719
You could say that's where we met,

364
01:10:01,997 --> 01:10:04,557
when I used to accompany
my older sister there.

365
01:10:05,601 --> 01:10:08,069
I was 1 5 or 1 6, remember?

366
01:10:09,905 --> 01:10:12,601
You were just back
from the military.

367
01:10:15,945 --> 01:10:17,537
I've never told you this,

368
01:10:17,646 --> 01:10:19,910
but the first time
you asked me to dance,

369
01:10:20,182 --> 01:10:23,379
I said no because I didn't know how.

370
01:10:24,153 --> 01:10:27,281
I had my girlfriends
teach me at home,

371
01:10:27,656 --> 01:10:30,921
and every night after that,
I hoped you would ask me again.

372
01:11:10,799 --> 01:11:12,460
How many memories since then,

373
01:11:13,135 --> 01:11:15,228
good and bad.

374
01:11:16,839 --> 01:11:21,401
Ever since you left,
I cherish even the bad ones.

375
01:11:23,245 --> 01:11:27,579
At times all those memories
made me want to cry.

376
01:11:29,018 --> 01:11:32,818
But I didn't want to cry.
I wanted to be strong.

377
01:11:33,589 --> 01:11:35,523
So I would try to feel resignation,

378
01:11:36,392 --> 01:11:39,020
just in case
you decided not to come back.

379
01:11:39,795 --> 01:11:44,061
I tried to forget you,
to erase you from my thoughts.

380
01:11:46,902 --> 01:11:49,735
But now, thank goodness,
everything has changed.

381
01:11:50,506 --> 01:11:53,907
Those sad thoughts seem so far away.

382
01:12:29,645 --> 01:12:31,909
What beautiful letters
you write, dear Liliana.

383
01:12:32,548 --> 01:12:35,244
You're so good at expressing yourself.

384
01:12:35,551 --> 01:12:37,485
I'm not as good,

385
01:12:37,753 --> 01:12:40,187
and I often can't say
everything I feel.

386
01:12:40,656 --> 01:12:43,489
But I'm sure you understand me
just the same,

387
01:12:43,959 --> 01:12:48,487
because the feelings you express
are the same ones I feel.

388
01:12:48,664 --> 01:12:50,689
You speak for us both.

389
01:12:54,670 --> 01:12:55,898
You know, Giovanni...

390
01:12:57,373 --> 01:13:00,467
perhaps your trip
was good for us both.

391
01:13:01,677 --> 01:13:07,582
I think the distance helped us
understand a lot of things.

392
01:13:09,017 --> 01:13:11,713
We've been sweethearts for so long.

393
01:13:12,755 --> 01:13:14,814
So many years.

394
01:13:16,592 --> 01:13:18,924
More than sweethearts -
you know what I mean -

395
01:13:20,596 --> 01:13:22,564
but we've never really spoken...

396
01:13:24,133 --> 01:13:27,330
the way two lovers should.

397
01:13:30,005 --> 01:13:31,768
We each kept
our thoughts to ourselves

398
01:13:33,008 --> 01:13:35,340
and were content
just being together.

399
01:13:37,279 --> 01:13:41,773
But perhaps our being together
was becoming a mere habit.

400
01:13:43,118 --> 01:13:46,485
Perhaps we didn't realize
we were each still alone.

401
01:13:49,925 --> 01:13:52,587
We're much closer now.

402
01:13:53,262 --> 01:13:57,562
I realized it when I thought back
on all our time together.

403
01:13:58,901 --> 01:14:01,597
I feel as if we're starting
all over again.

404
01:14:02,471 --> 01:14:04,132
It's like reliving the same feelings,

405
01:14:04,406 --> 01:14:07,239
but as if we're somehow different,

406
01:14:08,610 --> 01:14:10,077
somehow better.

407
01:14:59,495 --> 01:15:00,826
Hi.

408
01:15:01,263 --> 01:15:03,823
It's me, Giovanni.

409
01:15:05,534 --> 01:15:08,230
No, nothing. No reason.

410
01:15:09,204 --> 01:15:11,331
I felt like calling you.

411
01:15:11,907 --> 01:15:13,670
How are you?

412
01:15:14,009 --> 01:15:15,738
No, nothing.

413
01:15:17,813 --> 01:15:19,940
I just thought I'd call you.

414
01:15:20,215 --> 01:15:23,707
It's Sunday and it's half the rate.

415
01:15:25,854 --> 01:15:27,549
How are you?

416
01:15:31,059 --> 01:15:35,496
It's cooled off a bit,
but a storm is gathering.

417
01:15:37,799 --> 01:15:39,790
What are you doing?

418
01:15:48,110 --> 01:15:50,476
I have to go to work now.

419
01:15:50,846 --> 01:15:52,711
My bus leaves in 15 minutes.

420
01:15:55,484 --> 01:15:58,544
I don't feel like working today.

421
01:15:58,887 --> 01:16:01,117
I'd rather stay home.

422
01:16:01,823 --> 01:16:04,417
They could do without me
for one day.

423
01:16:04,526 --> 01:16:06,960
- Three minutes.
- Thank you.

424
01:16:07,729 --> 01:16:09,856
Maybe I'll call again next Sunday.

425
01:16:09,932 --> 01:16:13,527
But write me anyway, okay? Bye.

426
01:17:03,352 --> 01:17:06,583
THE END

