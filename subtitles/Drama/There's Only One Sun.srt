1
00:00:05,320 --> 00:00:06,480
There's Only One Sun

2
00:00:14,520 --> 00:00:16,800
Light...

3
00:00:16,960 --> 00:00:19,200
Dark...

4
00:00:19,360 --> 00:00:20,800
Light...

5
00:00:23,200 --> 00:00:24,800
Dark...

6
00:00:24,960 --> 00:00:28,000
When you say it
I can almost feel it.

7
00:00:30,320 --> 00:00:32,360
Read it for me.

8
00:00:32,480 --> 00:00:35,720
It just rained.

9
00:00:35,880 --> 00:00:38,440
Green lights
are shining in the water.

10
00:00:38,600 --> 00:00:42,200
Green? I remember red.

11
00:00:43,480 --> 00:00:45,200
Did they change them?

12
00:00:45,400 --> 00:00:47,720
They did.

13
00:00:50,000 --> 00:00:52,600
When did you go blind?

14
00:00:52,680 --> 00:00:55,880
A year ago.

15
00:00:55,960 --> 00:00:58,000
How did it happen?

16
00:01:04,240 --> 00:01:07,600
I saw something I shouldn't have.

17
00:01:09,800 --> 00:01:13,280
He probably had a name once,
but no one knows it now.

18
00:01:13,360 --> 00:01:14,960
His codename is "Light. "

19
00:01:15,320 --> 00:01:17,440
From today on, you are blind.

20
00:01:17,600 --> 00:01:22,720
He's paranoid about people
recognizing his face.

21
00:01:22,840 --> 00:01:27,600
This Lightcatcher
will lead you to him.

22
00:01:30,680 --> 00:01:32,920
Keep the signal on at all times.

23
00:01:33,000 --> 00:01:35,120
We wouldn't want to lose you.

24
00:01:41,920 --> 00:01:45,720
That day, they showed me
the only image of him in existence.

25
00:02:02,400 --> 00:02:05,640
I'm Agent 006
of the Central Authorities,

26
00:02:05,720 --> 00:02:07,440
Human Sanitation Department.

27
00:02:07,560 --> 00:02:09,000
We remove pepole.

28
00:02:18,280 --> 00:02:22,120
With the Lightcatcher, I could
feel his presence everywhere,

29
00:02:22,320 --> 00:02:25,440
but no signal was
strong enough to reach him.

30
00:02:44,960 --> 00:02:47,800
A woman in your condition
shouldn't be alone.

31
00:02:47,880 --> 00:02:49,600
It's dangerous.

32
00:02:52,680 --> 00:02:54,640
I don't mind.

33
00:02:56,000 --> 00:03:00,000
The danger or the loneliness?

34
00:03:00,200 --> 00:03:02,400
I don't care.

35
00:03:07,200 --> 00:03:10,040
Neither do I.

36
00:04:06,000 --> 00:04:09,400
Will you do it?

37
00:04:09,560 --> 00:04:11,600
Anything.

38
00:04:11,720 --> 00:04:14,000
Anything to keep us together.

39
00:04:26,800 --> 00:04:31,280
The plan was to cross the border.
We needed permits.

40
00:04:33,760 --> 00:04:35,560
I managed to get two.

41
00:04:39,680 --> 00:04:41,720
But I wasn't sure.

42
00:04:49,400 --> 00:04:53,480
At the border, I told him I had one.

43
00:04:53,560 --> 00:04:55,720
Only one of us could make it.

44
00:05:01,200 --> 00:05:03,880
That's how he wanted it to end.

45
00:05:09,880 --> 00:05:12,160
Maybe it would have been
better that way.

46
00:05:14,320 --> 00:05:16,960
But that night,

47
00:05:17,040 --> 00:05:18,680
I shot first.

48
00:05:56,400 --> 00:05:59,720
One year ago I saw samething
I shouldn't have.

49
00:06:01,760 --> 00:06:03,920
I saw him as a young man.

50
00:06:04,000 --> 00:06:05,520
He was beautiful.

51
00:06:19,760 --> 00:06:21,200
It's hard to look at things directly.

52
00:06:24,000 --> 00:06:27,400
They're too bright and too dark.

53
00:06:30,920 --> 00:06:33,520
Sometimes we need
to see things through a screen.

54
00:06:36,880 --> 00:06:39,200
On one side of the screen,
memories fade.

55
00:06:44,480 --> 00:06:47,400
On the other, they glow forever.

56
00:06:49,800 --> 00:06:54,440
Afterwards, they tried to remove Light
from my memory.

57
00:06:54,520 --> 00:06:56,800
But I can still feel his warmth.

58
00:07:26,960 --> 00:07:32,520
There's only one sun,
but it travels the world every day.

59
00:07:32,680 --> 00:07:37,800
This sun is all mine
and I won't ever give it away!

60
00:07:38,200 --> 00:07:40,440
Marina Tsvetaeva

