1
00:01:34,700 --> 00:01:39,149
Turin HORSE

2
00:01:44,035 --> 00:01:48,482
In Turin on January 3rd 1889,

3
00:01:48,792 --> 00:01:53,955
Friedrich Nietzsche steps out of the
Door of number six Via Carlo Alberto,

4
00:01:54,469 --> 00:01:57,949
Perhaps to take a stroll, perhaps to
go by the post office...

5
00:01:57,987 --> 00:01:59,511
...to collect his mail.

6
00:02:00,386 --> 00:02:03,388
Not far from him, or indeed
very far removed from him,

7
00:02:03,464 --> 00:02:06,783
a cabman is having trouble
with his stubborn horse.

8
00:02:07,701 --> 00:02:10,862
Despite all his urging,
the horse refuses to move,

9
00:02:11,339 --> 00:02:15,178
whereupon the cabman
- Giuseppe? Carlo? Ettore? -

10
00:02:15,936 --> 00:02:19,904
loses his patience
and takes his whip to it.

11
00:02:21,214 --> 00:02:23,293
Nietzsche comes up to the throng

12
00:02:23,532 --> 00:02:27,091
and that puts an end
to the brutal scene of the cabman,

13
00:02:27,410 --> 00:02:29,615
who by this time is foaming with rage.

14
00:02:29,929 --> 00:02:31,975
The solidly built and full-mustached Nietzsche
suddenly jumps up to the cab

15
00:02:32,527 --> 00:02:34,688
and throws his arms around
the horse neck...

16
00:02:35,045 --> 00:02:37,415
...sobbing.

17
00:02:39,203 --> 00:02:41,090
His neighbor takes him home,

18
00:02:41,762 --> 00:02:44,386
where he lies still and silent,
for two days on a divan

19
00:02:45,420 --> 00:02:46,902
until he mutters...

20
00:02:47,278 --> 00:02:49,245
the obligatory last words:

21
00:02:49,997 --> 00:02:52,236
"Mutter, ich bin dummm."
("Mother, i'm fool.")

22
00:02:53,355 --> 00:02:57,880
and lives for another ten years,
gentle and demented,

23
00:02:58,432 --> 00:02:59,875
in the care of his mother and sisters.

24
00:03:01,750 --> 00:03:04,433
Of the horse... we know nothing.

25
00:07:25,998 --> 00:07:31,258
THE FIRST DAY

26
00:20:38,660 --> 00:20:39,865
It's ready.

27
00:26:31,016 --> 00:26:33,539
Go to bed.

28
00:28:13,157 --> 00:28:14,919
Hey, you!

29
00:28:17,194 --> 00:28:18,195
What is it?

30
00:28:19,113 --> 00:28:21,239
Can't you hear them either?

31
00:28:23,250 --> 00:28:24,718
What?

32
00:28:26,429 --> 00:28:28,509
The woodworm, they're not doing it.

33
00:28:29,548 --> 00:28:31,435
I've heard them for 58 years.

34
00:28:32,146 --> 00:28:33,715
But I don't hear them mow.

35
00:28:41,700 --> 00:28:43,862
They really have stopped.

36
00:28:51,535 --> 00:28:52,998
What's it all about, papa?

37
00:28:55,652 --> 00:28:57,415
I do not know.

38
00:28:59,210 --> 00:29:00,336
Let's sleep.

39
00:29:11,523 --> 00:29:15,206
She lies back and pulls
the blanket over herself.

40
00:29:16,040 --> 00:29:17,689
Ohlsdorfer turns on his side

41
00:29:18,199 --> 00:29:20,041
and fixes his eyes stopped on the window.

42
00:29:21,037 --> 00:29:24,721
The girl stares atthe ceiling,
her father at the window.

43
00:29:26,194 --> 00:29:28,957
At times a ties can be heard
crashing down from the roof

44
00:29:29,832 --> 00:29:32,310
and shattering noisily.

45
00:29:32,751 --> 00:29:36,356
The gale roars relentlessly around the house.

46
00:30:11,788 --> 00:30:16,469
THE SECOND DAY

47
00:40:42,124 --> 00:40:43,090
Come here!

48
00:43:27,388 --> 00:43:28,812
Can't you see she won't move?

49
00:43:47,137 --> 00:43:48,978
Stop it!

50
00:45:58,700 --> 00:46:00,303
Come here!

51
00:54:45,796 --> 00:54:47,080
It's ready.

52
00:59:57,097 --> 00:59:59,866
I've run out of palinka. Would you give me
a bottle? (Hung. fruk. vodka)

53
01:00:03,533 --> 01:00:04,738
Give him some...

54
01:00:08,109 --> 01:00:10,653
- Why didn't you go into town?
- The wind's blown it away.

55
01:00:12,647 --> 01:00:14,455
How come?

56
01:00:14,846 --> 01:00:16,175
It's gone to ruin.

57
01:00:18,924 --> 01:00:20,765
Why would it go to ruin?

58
01:00:24,080 --> 01:00:26,450
Because everything's in ruins,

59
01:00:26,679 --> 01:00:28,159
Everything's been degraded,

60
01:00:29,277 --> 01:00:31,564
but I could say that they've ruined...

61
01:00:32,076 --> 01:00:34,076
and degraded everything.

62
01:00:34,895 --> 01:00:37,121
Because this is not some
kind of cataclysm,

63
01:00:37,593 --> 01:00:40,037
coming about with
so-called innocent human aid.

64
01:00:40,351 --> 01:00:41,995
On the contrary...

65
01:00:43,070 --> 01:00:45,833
It's about man's own judgement,

66
01:00:46,348 --> 01:00:49,269
his own judgement over his own self,

67
01:00:49,786 --> 01:00:52,205
which of course God has a hand in,

68
01:00:52,984 --> 01:00:55,110
or dare I say: takes part in

69
01:00:56,082 --> 01:00:57,830
And whatever he takes part in

70
01:00:58,761 --> 01:01:02,908
is the most ghastly creation
that you can imagine.

71
01:01:05,177 --> 01:01:06,945
Because, you see, the world has been debased.

72
01:01:07,795 --> 01:01:09,305
So it doesn't matter what I say,

73
01:01:10,234 --> 01:01:12,255
because everything has been debased
that they've acquired.

74
01:01:13,253 --> 01:01:16,858
and since they've acquired everything
in a sneaky, underhand fight,

75
01:01:17,940 --> 01:01:20,329
they've debased everything.

76
01:01:21,128 --> 01:01:23,015
Because whatever they touch...

77
01:01:23,166 --> 01:01:26,054
-and they touch everything-
they've debased.

78
01:01:28,963 --> 01:01:32,010
This is the way it was until the final victory.
Until the trimphant end.

79
01:01:32,701 --> 01:01:34,686
Acquire, debase,

80
01:01:35,279 --> 01:01:37,160
debase, acquire.

81
01:01:38,078 --> 01:01:40,078
Or I can put it differently if you like:

82
01:01:40,877 --> 01:01:43,276
To touch, debase and thereby acquire,

83
01:01:43,914 --> 01:01:46,756
or touched, acquire and thereby debase.

84
01:01:46,913 --> 01:01:48,880
It's been going on like this for centuries..

85
01:01:49,551 --> 01:01:50,915
On, on and on.

86
01:01:52,110 --> 01:01:55,114
This and only this, sometimes on the sly,
sometimes rudely,

87
01:01:56,047 --> 01:01:57,992
sometimes gently, sometimes brutally,

88
01:01:58,546 --> 01:02:00,945
but it has been going on and on.

89
01:02:01,905 --> 01:02:03,712
Yet only in one way,

90
01:02:04,542 --> 01:02:06,066
like a rat attack from ambush.

91
01:02:07,441 --> 01:02:09,984
Becouse for this perfect victory,

92
01:02:10,299 --> 01:02:11,947
it was also essential that the other side...

93
01:02:12,777 --> 01:02:16,063
think is, everything that's excellent,
great in some way and noble,

94
01:02:16,296 --> 01:02:20,900
should not engage in any kind of fight.

95
01:02:21,872 --> 01:02:24,052
There shouldn't be any kind of struggle,

96
01:02:24,292 --> 01:02:26,577
just the sudden disappearance of one side,

97
01:02:27,089 --> 01:02:31,728
meaning the disappearance of the excellent,
the great, the noble.

98
01:02:32,326 --> 01:02:36,726
So that by now these winning winners
who attack from ambush rule earth,

99
01:02:38,683 --> 01:02:40,365
and there isn't a single tiny nook

100
01:02:41,001 --> 01:02:43,525
where one can hide something from them,

101
01:02:44,279 --> 01:02:46,087
because everything they can lay
their hands on is theirs.

102
01:02:47,717 --> 01:02:49,400
Even things we think they can't reach
- but they do reach -

103
01:02:50,316 --> 01:02:51,198
are also theirs.

104
01:02:54,034 --> 01:02:57,280
Because the sky is already theirs
and all our dreams.

105
01:02:58,112 --> 01:02:59,556
Theirs is the moment, nature,

106
01:03:00,271 --> 01:03:01,998
infinite silence.

107
01:03:03,309 --> 01:03:05,117
Even immortality is theirs, you understand?

108
01:03:05,787 --> 01:03:08,026
Everything, everything is lost forever!

109
01:03:08,786 --> 01:03:10,070
And those many noble,

110
01:03:10,864 --> 01:03:14,232
great and excellent just stood there,
if I can put it that way.

111
01:03:14,942 --> 01:03:16,022
They stopped at this point,

112
01:03:16,761 --> 01:03:18,869
and had to understand, and had to accept,

113
01:03:19,260 --> 01:03:22,421
that there is neither god nor gods.

114
01:03:23,017 --> 01:03:25,825
And the excellent, the great a nd the noble

115
01:03:26,535 --> 01:03:29,855
had to understand and accept
this right from the beginning.

116
01:03:30,573 --> 01:03:33,141
But of course, they were quite incapable
of understanding it.

117
01:03:33,172 --> 01:03:37,572
They believed it and accepted it
but they didn't understand it.

118
01:03:41,166 --> 01:03:43,771
They just stood there, bewildered,
but not resigned,

119
01:03:44,765 --> 01:03:47,242
until something - that spark from the brain -

120
01:03:48,123 --> 01:03:49,726
finally enlightened them.

121
01:03:50,881 --> 01:03:55,680
And all at once they realized,
that there is neither god nor gods.

122
01:03:56,518 --> 01:03:59,406
All at once they saw that
there is neither good nor bad.

123
01:03:59,836 --> 01:04:02,042
Then they saw and understood

124
01:04:02,354 --> 01:04:06,038
that if this was so,
then they themselves do not exist either!

125
01:04:06,752 --> 01:04:09,400
You see, I reckon this may have been

126
01:04:09,530 --> 01:04:11,319
the moment when we can say that

127
01:04:11,409 --> 01:04:14,857
they were extinguished, they burnt out.

128
01:04:15,867 --> 01:04:17,754
Extinguished and burnt out

129
01:04:18,345 --> 01:04:19,869
like the fire left to smoulder in the meadow.

130
01:04:21,463 --> 01:04:23,112
One was a constant loser,

131
01:04:24,142 --> 01:04:25,831
the another was the constant winner.

132
01:04:26,781 --> 01:04:28,060
Defeat, victory,

133
01:04:28,820 --> 01:04:30,258
defeat, victory,

134
01:04:31,737 --> 01:04:35,216
and one day
- here in the neighbourhood -

135
01:04:36,535 --> 01:04:38,184
I had to realize,

136
01:04:39,733 --> 01:04:42,461
and I did realize, that I was mistaken,

137
01:04:43,091 --> 01:04:45,490
I was truly mistaken when I thought

138
01:04:46,010 --> 01:04:47,658
that there has never been

139
01:04:47,669 --> 01:04:51,496
and could never be
any kind of change here on earth.

140
01:04:51,926 --> 01:04:54,292
Because, believe me, I know now

141
01:04:55,084 --> 01:04:57,767
that this change has indeed taken place.

142
01:05:00,721 --> 01:05:02,722
Come off it! That's rubbish!

143
01:07:01,891 --> 01:07:07,091
THE THIRD DAY

144
01:17:47,218 --> 01:17:48,945
Coat!

145
01:20:49,972 --> 01:20:51,695
She's not eating.

146
01:20:54,530 --> 01:20:56,074
She will.

147
01:21:00,266 --> 01:21:01,576
Do eat!

148
01:21:03,964 --> 01:21:05,529
You have to eat!

149
01:25:41,623 --> 01:25:42,789
What's that?

150
01:25:43,623 --> 01:25:45,088
What's happening?

151
01:25:45,921 --> 01:25:47,306
A cart's approaching.

152
01:25:49,679 --> 01:25:50,845
Who are they?

153
01:25:55,276 --> 01:25:57,415
Gypsies, I think.

154
01:25:59,084 --> 01:26:00,802
What the fuck do they want here?

155
01:26:06,689 --> 01:26:09,108
I don't know, but they're coming this way!

156
01:26:09,687 --> 01:26:12,173
The stinking rotten bastards!

157
01:26:21,500 --> 01:26:23,064
What shall we do?

158
01:26:24,179 --> 01:26:25,780
Go and chase them away!

159
01:26:28,037 --> 01:26:29,326
What are you waiting for?

160
01:26:29,936 --> 01:26:31,624
Get moving!

161
01:27:10,043 --> 01:27:14,329
...There's water here...

162
01:27:29,832 --> 01:27:32,231
...Come and help...!

163
01:27:32,789 --> 01:27:34,790
...Come and help papa...!

164
01:27:37,187 --> 01:27:38,869
...Come on, hey...!
... Come and drink...!

165
01:27:40,065 --> 01:27:41,906
...Hold the horse...

166
01:27:42,224 --> 01:27:44,668
...Look...

167
01:27:47,580 --> 01:27:49,865
...Here comes the girl...
Here's the girl...

168
01:27:51,019 --> 01:27:52,746
...Her eyes are like the devil's...

169
01:27:52,778 --> 01:27:55,302
Get away from here!
Go away!

170
01:27:56,096 --> 01:27:57,095
What are you doing here?

171
01:27:57,815 --> 01:27:58,781
Gert away from here!

172
01:27:59,934 --> 01:28:01,298
...Come with us...!

173
01:28:02,133 --> 01:28:03,572
I won't!

174
01:28:03,671 --> 01:28:04,430
I'm not going anywhere!

175
01:28:05,170 --> 01:28:06,534
...Come with us to America...!

176
01:28:07,450 --> 01:28:09,337
Are you deaf?
Let me go!

177
01:28:09,888 --> 01:28:11,252
I'm not going with you!

178
01:28:11,807 --> 01:28:12,966
God forbid!

179
01:28:13,886 --> 01:28:15,684
...You'll like it there...!

180
01:28:15,895 --> 01:28:16,631
I don't care!

181
01:28:16,744 --> 01:28:17,543
Let go!

182
01:28:19,782 --> 01:28:23,767
Fuck you sons of bitches!
Get the hell out of here!

183
01:28:24,640 --> 01:28:27,289
I'll rip your guts out,
for fuck's sake!

184
01:28:28,078 --> 01:28:30,158
Dirty rotten gypsies!

185
01:28:31,956 --> 01:28:33,797
...This is for the water...

186
01:28:35,713 --> 01:28:38,190
He'll kill me! Papa!

187
01:28:38,512 --> 01:28:41,161
Papa! Faster!

188
01:28:45,187 --> 01:28:47,996
...Just come over here, you worm...!

189
01:28:49,465 --> 01:28:51,432
...We'll be back...!

190
01:28:52,663 --> 01:28:54,345
The water is ours!

191
01:28:55,102 --> 01:28:57,102
The earth is ours!

192
01:29:03,377 --> 01:29:05,185
You're weak!
You're weak!

193
01:29:05,216 --> 01:29:07,821
Drop dead! Drop dead!
Drop dead!

194
01:30:57,681 --> 01:30:59,170
One.

195
01:31:00,679 --> 01:31:03,488
Since ho-ly pla-ces

196
01:31:04,797 --> 01:31:11,244
only al-low the prac-tice

197
01:31:12,833 --> 01:31:19,871
of things that serve

198
01:31:21,267 --> 01:31:27,145
the ven-er-a-tion of the Lord,

199
01:31:29,103 --> 01:31:35,106
and every-thing is for-bid-den

200
01:31:35,579 --> 01:31:39,468
that is not fit-ted for

201
01:31:41,455 --> 01:31:47,334
the ho-li-ness of the place,

202
01:31:47,972 --> 01:31:50,974
and since

203
01:31:51,570 --> 01:31:57,687
ho-ly pla-ces are vi-o-lated

204
01:32:01,004 --> 01:32:08,884
by the great in-jus-tice of ac-tions

205
01:32:09,799 --> 01:32:18,361
that have ta-ken place within them

206
01:32:19,434 --> 01:32:27,313
that scan-dal-ize the con-gre-gation,

207
01:32:27,709 --> 01:32:30,711
for this rea-son

208
01:32:32,107 --> 01:32:41,112
no ser-vice can be held there

209
01:32:41,821 --> 01:32:44,026
Un-til,

210
01:32:45,218 --> 01:32:51,779
though a ce-re-mony of pen-i-tence,

211
01:32:52,175 --> 01:32:58,860
the in-jus-ti-ces

212
01:32:59,650 --> 01:33:04,175
have been put right.

213
01:33:05,007 --> 01:33:09,293
The ce-le-brant

214
01:33:10,045 --> 01:33:16,765
tells the con-gre-gation:

215
01:33:18,400 --> 01:33:25,325
The Lord is with you!

216
01:33:26,555 --> 01:33:33,638
Mor-ning will turn to night.

217
01:33:35,750 --> 01:33:39,957
Night will end...

218
01:33:40,287 --> 01:33:44,654
The storm continues to rage outside.

219
01:33:45,764 --> 01:33:50,410
The wind still sweeps relentlessly
across the land from the same direction.

220
01:33:50,561 --> 01:33:54,980
But now there is nothing
in its path to obstruct it.

221
01:33:55,259 --> 01:33:58,318
Only a great cloud of dust
whipped up by the wind...

222
01:33:58,676 --> 01:34:00,061
...rushes recklessly forward.

223
01:34:01,115 --> 01:34:03,195
Bone-dry dust and the ravaging nothing...

224
01:34:03,573 --> 01:34:07,631
...that the wind rolls before it
as it rages unbridled

225
01:34:08,790 --> 01:34:12,029
over the barren land.

226
01:34:29,179 --> 01:34:34,482
THE FOURTH DAY

227
01:36:47,219 --> 01:36:48,921
- Come with me!
- What's up?

228
01:36:49,637 --> 01:36:51,858
- Come, quick!
- What's wrong?

229
01:36:52,616 --> 01:36:54,179
The well!

230
01:37:38,609 --> 01:37:40,717
Phew!
Fuck it!

231
01:37:57,238 --> 01:37:59,344
Cover it!

232
01:38:46,350 --> 01:38:48,670
And the palinka?

233
01:40:29,291 --> 01:40:30,939
Why don't you eat?

234
01:40:51,598 --> 01:40:53,565
You're not going anywhere...

235
01:43:44,898 --> 01:43:46,422
Drink!

236
01:43:47,296 --> 01:43:49,535
At least drink a little water!

237
01:43:57,690 --> 01:44:00,578
For my sake!

238
01:46:23,966 --> 01:46:26,968
Pack up clothes, dishes, needle
and thread, things like that!

239
01:46:27,443 --> 01:46:28,489
Whot for?

240
01:46:29,303 --> 01:46:31,449
We're not staying here. Pack!

241
01:47:51,236 --> 01:47:53,521
Blankets, palinka.

242
01:48:15,022 --> 01:48:16,704
Palinka!

243
01:48:30,732 --> 01:48:32,495
Potatoes too.

244
01:48:53,159 --> 01:48:54,887
Get the handcart!

245
01:51:44,260 --> 01:51:46,102
Come on!

246
02:03:18,799 --> 02:03:24,101
THE FIFTH DAY

247
02:15:31,436 --> 02:15:33,436
What's this darkness, papa?

248
02:15:40,371 --> 02:15:42,040
Light the lamps!

249
02:15:44,308 --> 02:15:45,990
Fuck it!

250
02:20:09,116 --> 02:20:11,196
Why didn't you fill it up?

251
02:20:12,153 --> 02:20:14,433
It's full.

252
02:20:40,337 --> 02:20:42,383
Bring some embers!

253
02:21:20,035 --> 02:21:21,523
What is all this?

254
02:21:22,733 --> 02:21:23,838
I don't know.

255
02:21:24,891 --> 02:21:26,699
Let's go to bed.

256
02:21:35,106 --> 02:21:37,072
Even the embers went out.

257
02:21:50,977 --> 02:21:52,977
Tomorrow we'll try again.

258
02:22:13,283 --> 02:22:17,172
We can hear them
groping their way to the bads.

259
02:22:18,480 --> 02:22:20,402
We can hear them lying down,

260
02:22:20,879 --> 02:22:23,323
and pulling the blanket over them.

261
02:22:24,517 --> 02:22:27,199
We can hear them breathing.

262
02:22:27,755 --> 02:22:30,359
Only their breathing.

263
02:22:32,073 --> 02:22:35,280
Dead silence outside,
the storm is over.

264
02:22:36,430 --> 02:22:40,114
Dead silence falls on the house too.

265
02:22:43,467 --> 02:22:48,754
THE SIXTH DAY

266
02:24:14,294 --> 02:24:15,942
Eat.

267
02:24:38,800 --> 02:24:42,279
We have to eat.

