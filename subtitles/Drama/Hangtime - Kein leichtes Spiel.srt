1
00:00:22,356 --> 00:00:26,027
<i>I WOULDN'T LIKE TO BE A MAN</i>

2
00:00:26,152 --> 00:00:29,280
A COMEDY IN THREE ACTS
by Ernst Lubitsch

3
00:00:29,405 --> 00:00:31,824
Cast:

4
00:00:40,499 --> 00:00:43,711
Direction:

5
00:00:43,836 --> 00:00:48,132
Decoration and Set Design:

6
00:00:48,257 --> 00:00:50,092
FIRST ACT

7
00:01:43,062 --> 00:01:46,983
Is that proper for a young girl?

8
00:02:05,626 --> 00:02:08,963
I was playin' a bit o' poker.

9
00:02:12,800 --> 00:02:16,679
When I was your age - -!

10
00:02:20,558 --> 00:02:24,645
Come on, that was a long time ago.

11
00:02:43,581 --> 00:02:48,210
I really don't understand
how a lady can smoke.

12
00:03:22,036 --> 00:03:24,830
Now what's all this?

13
00:03:26,624 --> 00:03:30,753
I'm slugging back my troubles.

14
00:03:49,855 --> 00:03:55,444
Well, it looks like you've got
a few nasty troubles of your own!

15
00:04:17,883 --> 00:04:22,429
Would you allow us to serenade you?

16
00:04:49,206 --> 00:04:52,626
We'd like something sweet too.

17
00:05:25,159 --> 00:05:27,953
Where are you off to, then?!

18
00:05:31,290 --> 00:05:35,211
Oh, if Petrus only knew about this...!

19
00:06:05,115 --> 00:06:09,453
<i>Dearest Councillor of Commerce,</i>

20
00:06:09,578 --> 00:06:15,793
<i>The institution you have planned
has now been founded.</i>

21
00:06:15,918 --> 00:06:20,547
<i>It is essential that you come.</i>

22
00:07:30,075 --> 00:07:34,955
Behave yourself, so you make
your governess very happy.

23
00:07:40,419 --> 00:07:44,465
See, you're supposed to be happy
with me.

24
00:07:51,430 --> 00:07:56,602
And you want to be a refined young girl?

25
00:08:00,731 --> 00:08:03,525
I don't want that at all!

26
00:08:27,341 --> 00:08:31,720
I think the poor child's
going to be pretty miserable.

27
00:08:51,323 --> 00:08:54,368
What are you up to?

28
00:08:56,203 --> 00:08:57,830
Dilly-dallying!

29
00:09:03,794 --> 00:09:07,172
What would your uncle say about this?

30
00:09:10,384 --> 00:09:14,012
Oh, I'm feeling pretty awful!

31
00:10:17,451 --> 00:10:20,579
Your new guardian.

32
00:10:38,222 --> 00:10:42,643
You're the guardian
who's in for a rude awakening?

33
00:10:54,321 --> 00:10:58,367
Stand up when you're addressing me!!

34
00:11:05,332 --> 00:11:08,168
Now curtsy!

35
00:11:12,089 --> 00:11:13,799
Lower!

36
00:11:37,197 --> 00:11:40,909
Gracious Miss, your car is here.

37
00:12:02,306 --> 00:12:08,562
Young girls your age
belong in bed at this time!

38
00:12:31,001 --> 00:12:34,129
I'll break you down yet!

39
00:12:53,565 --> 00:12:55,317
Down to here!

40
00:13:01,406 --> 00:13:05,118
What a petty man!

41
00:14:11,143 --> 00:14:15,480
Why didn't I come into this world
as a boy.

42
00:14:35,542 --> 00:14:39,838
END OF THE FIRST ACT

43
00:14:39,963 --> 00:14:42,674
SECOND ACT

44
00:14:58,148 --> 00:15:01,693
I'd like an evening-suit.

45
00:15:15,916 --> 00:15:17,626
Measurements -

46
00:15:27,761 --> 00:15:32,557
By which gentleman do you wish
to be served?

47
00:15:40,774 --> 00:15:44,152
Perhaps the gentlemen will compromise.

48
00:15:52,035 --> 00:15:54,996
Me - the left arm!

49
00:15:56,414 --> 00:15:59,000
Me - the right arm!

50
00:16:04,798 --> 00:16:08,677
And what'll be leftover here
for me then?

51
00:16:25,986 --> 00:16:28,947
Ossi emancipates herself

52
00:16:59,811 --> 00:17:03,690
Good heavens does a man
not have it easy.

53
00:17:25,629 --> 00:17:29,424
And this here's not as simple as it looks.

54
00:18:33,238 --> 00:18:35,824
The world's mine at any price!

55
00:19:11,109 --> 00:19:13,820
A charming lad!

56
00:20:31,564 --> 00:20:35,860
Such a cheeky boy -
won't even stand up!!

57
00:20:50,708 --> 00:20:55,588
Well no need to make such a fuss!
After all, you're a man!

58
00:21:00,510 --> 00:21:02,137
That's what you say!

59
00:21:10,103 --> 00:21:12,939
In the Mause-Palast

60
00:22:26,846 --> 00:22:30,141
Rough folk - these men!

61
00:23:08,846 --> 00:23:11,432
Take a look at this one!

62
00:23:58,354 --> 00:24:00,106
Well, junior?

63
00:25:05,338 --> 00:25:09,217
Ruthless people, these women!

64
00:25:53,553 --> 00:25:55,221
Stupid fellow!

65
00:26:14,407 --> 00:26:19,120
I'll seduce her away from him
in no time.

66
00:26:59,243 --> 00:27:00,661
Don't clobber me!

67
00:27:02,705 --> 00:27:06,667
I swear I won't do it again.

68
00:27:21,224 --> 00:27:24,143
That's women!

69
00:27:27,480 --> 00:27:31,150
And you're here squabbling with me?

70
00:27:41,160 --> 00:27:45,331
END OF THE SECOND ACT

71
00:27:45,456 --> 00:27:49,293
"I Wouldn't Like to Be a Man!"
THIRD ACT

72
00:27:49,418 --> 00:27:52,213
After the Ball's End

73
00:28:43,681 --> 00:28:47,727
You keep puffing - inhale already!

74
00:29:14,253 --> 00:29:17,590
You're making such a wretched face!

75
00:29:20,885 --> 00:29:24,055
I feel wretched, too!

76
00:30:42,007 --> 00:30:43,676
Maybe not, then.

77
00:31:00,860 --> 00:31:04,238
Well - how you doin' now?

78
00:31:06,657 --> 00:31:10,870
It's better not to ask!

79
00:31:19,295 --> 00:31:22,006
To brotherhood!

80
00:31:37,146 --> 00:31:40,024
What's your name, anyway?

81
00:31:47,156 --> 00:31:50,618
It's better not to ask!

82
00:32:17,394 --> 00:32:22,233
Today I'm noticin' for the first time
that the earth's spinning!

83
00:33:09,655 --> 00:33:12,700
So where are we off to?

84
00:33:14,743 --> 00:33:17,830
Go straight ahead.

85
00:34:14,470 --> 00:34:18,515
Y'aren't quite on the wagon yerself.

86
00:34:40,871 --> 00:34:43,624
Ossi's his name?

87
00:34:47,252 --> 00:34:51,715
Oh, he's pissed all right!

88
00:37:36,880 --> 00:37:39,633
He's in the bathtub?

89
00:38:26,972 --> 00:38:31,101
I want to see my governess.

90
00:38:38,609 --> 00:38:42,654
Come now, wipe away your tears.

91
00:38:45,324 --> 00:38:48,535
I haven't got a handkerchief!

92
00:40:10,534 --> 00:40:13,704
Well well, what brings you here?

93
00:40:17,833 --> 00:40:21,211
I'm here to visit my cousin.

94
00:40:28,010 --> 00:40:32,472
Don't tell the little one
anything of our adventure.

95
00:40:46,069 --> 00:40:47,738
Promise!

96
00:40:51,283 --> 00:40:54,161
The lass is pretty cute, too!

97
00:40:59,541 --> 00:41:02,252
I beg to differ.

98
00:41:30,655 --> 00:41:34,534
Well, already out of the bathtub?

99
00:41:36,119 --> 00:41:38,955
Out of which bathtub?

100
00:41:40,665 --> 00:41:43,377
Well, out of the bathtub.

101
00:41:47,172 --> 00:41:51,718
Naturally,
one can't stay in there forever!

102
00:42:14,533 --> 00:42:18,870
Being a man, -
that's exhausting stuff!

103
00:42:24,626 --> 00:42:29,047
Ossi has to get up.
I'll wake her right away.

104
00:42:42,853 --> 00:42:46,731
No one wakes the way I wake!

105
00:42:50,026 --> 00:42:53,029
I'm an old alarm clock!

106
00:43:12,174 --> 00:43:15,802
That's right, the one and only!

107
00:43:19,097 --> 00:43:22,976
And you let yourself be kissed by me?

108
00:43:24,895 --> 00:43:28,148
Well - didn't you like how it tasted?

109
00:43:37,449 --> 00:43:40,785
I'll break you down yet!

110
00:43:47,626 --> 00:43:50,003
Down to here!

111
00:44:42,597 --> 00:44:45,225
I wouldn't like to be a man!

