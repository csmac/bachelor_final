1
00:25:47,431 --> 00:25:48,932
Welcome to Egypt. Here.

2
00:27:07,511 --> 00:27:09,221
<i>Who is it?</i>

3
00:27:14,601 --> 00:27:16,020
<i>Who is there?</i>

4
00:27:18,939 --> 00:27:20,065
<i>Mister Akef?</i>

5
00:27:26,864 --> 00:27:27,781
Enter.

6
00:27:30,409 --> 00:27:31,785
One minute.

7
00:27:39,668 --> 00:27:42,338
- How are you?
- Fine. How are you?

8
00:28:18,540 --> 00:28:19,750
Who was it?

9
00:28:26,882 --> 00:28:28,884
Your guest is in
the dining room.

10
00:28:29,218 --> 00:28:30,344
My guest?

11
00:29:17,349 --> 00:29:19,643
<i>Monica on Friends
danced at a restaurant.</i>

12
00:29:19,977 --> 00:29:21,186
With the inflatable breasts.

13
00:29:58,891 --> 00:29:59,767
I love you.

14
00:30:35,678 --> 00:30:36,804
Anyone there?

15
00:30:39,431 --> 00:30:40,891
What do you think?

16
00:30:46,772 --> 00:30:47,940
We are full.

17
00:30:51,652 --> 00:30:54,029
- How long?
- Say... 3 days.

18
00:31:10,212 --> 00:31:11,046
Of course.

19
00:31:22,308 --> 00:31:23,350
Thanks.

20
00:32:55,776 --> 00:32:56,527
Go away!

21
00:32:57,653 --> 00:32:58,904
Please, come in.

22
00:37:27,339 --> 00:37:30,259
Hello,
my small apple of love.

23
00:37:40,060 --> 00:37:42,187
No, I just want a smile!

24
00:39:48,022 --> 00:39:50,357
Listen to the rhythm.

25
00:39:50,899 --> 00:39:52,109
First slowly,

26
00:39:52,443 --> 00:39:53,485
then you move faster.

27
00:40:03,203 --> 00:40:04,955
Stop the music.

28
00:40:05,289 --> 00:40:06,415
What is going on up there?

29
00:40:10,169 --> 00:40:11,170
Continue the music

30
00:48:16,488 --> 00:48:17,656
The American, do you remember?

31
00:48:19,700 --> 00:48:20,993
Welcome to Egypt.

32
00:48:49,438 --> 00:48:50,606
And that one?

33
00:48:51,190 --> 00:48:52,399
This one also.

34
00:48:53,067 --> 00:48:54,443
Try it.

35
00:49:12,962 --> 00:49:13,712
What do you think?

36
00:49:13,963 --> 00:49:14,797
Too cute!

37
00:49:45,995 --> 00:49:46,704
<i>Whore!</i>

38
00:49:50,124 --> 00:49:50,874
Don't worry.

39
00:49:51,208 --> 00:49:52,918
We will wait until they leave.

40
00:50:03,804 --> 00:50:05,055
Bitch!

41
00:50:07,182 --> 00:50:08,309
Dirty whore!

42
00:50:09,226 --> 00:50:10,269
BITCH!

43
00:50:11,729 --> 00:50:12,980
I love you!

44
00:50:24,658 --> 00:50:25,451
You stay here!

45
00:50:25,784 --> 00:50:26,577
No!

46
00:56:27,980 --> 00:56:31,442
How can you hire this Barbie?

47
00:56:32,943 --> 00:56:33,736
You make fun?

48
00:56:34,153 --> 00:56:35,362
Look at her,

49
00:56:35,863 --> 00:56:37,197
She is white gold!

50
00:57:23,035 --> 00:57:24,703
Understand me?

51
00:58:15,587 --> 00:58:17,589
<i>Ismahan</i>

52
01:02:15,703 --> 01:02:16,787
My friend!

53
01:02:18,998 --> 01:02:19,999
Give me a hug.

54
01:02:21,458 --> 01:02:23,252
Look at this

55
01:02:39,059 --> 01:02:40,352
How much, you little whore?

56
01:02:42,688 --> 01:02:44,231
Let her go!

57
01:08:39,420 --> 01:08:41,005
For you, only 15 pounds.

58
01:08:43,382 --> 01:08:44,425
Very good price.

59
01:08:45,467 --> 01:08:47,344
Hello my small white flower.

60
01:08:49,388 --> 01:08:50,431
Get on!

61
01:12:42,204 --> 01:12:45,124
- Who is it?
- A high class snob!

62
01:13:48,812 --> 01:13:49,939
Bring some more humus.

63
01:16:52,371 --> 01:16:54,623
<i>Ismahan</i>

64
01:18:36,016 --> 01:18:37,101
Do it for me

65
01:19:03,877 --> 01:19:07,423
She is not a tourist.
She is dancing at the wedding of El Houari.

66
01:19:12,303 --> 01:19:13,554
I know you.

67
01:19:14,221 --> 01:19:16,348
If she is with you,
Mrs. Ismahan,

68
01:19:16,682 --> 01:19:18,100
You will pay wholesale.

69
01:19:19,977 --> 01:19:22,313
And this one is a gift from me.

70
01:22:40,761 --> 01:22:43,764
Promise me that our wedding
will be this nice!

71
01:22:52,022 --> 01:22:53,941
Greetings, Zack.  How are you?

72
01:22:54,275 --> 01:22:54,984
Fine thanks.

73
01:22:55,317 --> 01:22:56,735
It is a beautiful reception.

74
01:22:57,569 --> 01:22:59,905
It will soon be your turn.

75
01:23:00,739 --> 01:23:01,490
In a month.

76
01:23:01,824 --> 01:23:03,367
Mine also, I hope.

77
01:23:04,576 --> 01:23:06,245
Rather well for a foreigner.

78
01:24:04,470 --> 01:24:06,305
I know her! It's Lola.

79
01:24:07,014 --> 01:24:08,557
Lola, the American.

80
01:24:09,058 --> 01:24:09,934
I'm going to say hello.

81
01:24:17,316 --> 01:24:18,442
One second.

82
01:30:47,039 --> 01:30:49,333
People are not the same
as these letters

83
01:30:49,625 --> 01:30:53,587
You cannot put them aside
whenever you wish.

84
01:31:38,841 --> 01:31:41,719
Give her time, Mrs Aida

85
01:31:58,152 --> 01:31:59,445
I'm telling you the truth

86
01:31:59,778 --> 01:32:01,322
You are joking! A mailman?

87
01:37:39,535 --> 01:37:40,869
Hello my small white flower.

88
01:40:28,871 --> 01:40:30,205
''If you have the will,

89
01:40:30,539 --> 01:40:33,334
your force will be
in the measure of your desire.''

90
01:41:38,857 --> 01:41:39,733
Thank you.

