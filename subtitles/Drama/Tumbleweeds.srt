1
00:00:06,900 --> 00:00:14,700
I love you so much I can't let you go

2
00:00:14,900 --> 00:00:21,200
And sometimes I believe you love me.

3
00:00:22,000 --> 00:00:29,900
But somewhere between your heart and mine

4
00:00:30,100 --> 00:00:36,100
There's a door without any key

5
00:00:37,900 --> 00:00:45,000
Somewhere between your heart and mine

6
00:00:45,200 --> 00:00:52,400
There's a window that I can't see through

7
00:00:52,900 --> 00:01:00,400
There's a wall so high it reaches the sky

8
00:01:00,600 --> 00:01:07,600
Somewhere between me and you

9
00:01:08,800 --> 00:01:16,000
Somewhere between your heart and mine

10
00:01:16,200 --> 00:01:23,300
There's love I can't understand

11
00:01:23,600 --> 00:01:31,600
It's there for a while that fades like a smile

12
00:01:31,800 --> 00:01:37,900
And I am left in the middle again

13
00:01:39,200 --> 00:01:46,400
Somewhere between your heart and mine

14
00:01:46,600 --> 00:01:54,000
There's a window that I can't see through

15
00:01:54,200 --> 00:02:02,000
There's a wall so high it reaches the sky

16
00:02:02,200 --> 00:02:09,000
Somewhere between me and you

17
00:02:10,000 --> 00:02:17,400
Somewhere between your heart and mine

18
00:02:17,600 --> 00:02:25,100
There's a window that I can't see through

19
00:02:25,400 --> 00:02:33,200
There's a wall so high it reaches the sky

20
00:02:33,400 --> 00:02:39,900
Somewhere between me and you

21
00:02:40,800 --> 00:02:48,200
Somewhere between me and you....

