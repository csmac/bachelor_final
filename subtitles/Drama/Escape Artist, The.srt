1
00:02:10,147 --> 00:02:12,675
"I won't talk!
I won't say a word!!!"

2
00:02:25,388 --> 00:02:26,789
"Speak!"

3
00:02:29,442 --> 00:02:30,744
"Speak!"

4
00:05:18,068 --> 00:05:20,195
" Long live free Georgia!!!"

5
00:06:57,417 --> 00:06:59,419
" I will get you for this."

6
00:12:39,551 --> 00:12:42,004
"Listen up! Three girls
who can dance!"

7
00:13:28,684 --> 00:13:31,186
"The name's Miller.
Peppy Miller!"

8
00:14:35,667 --> 00:14:38,795
"Clifton, go to the jewelry store. Get something
nice for my wife. To make it up to her."

9
00:15:40,816 --> 00:15:45,195
"With all your nonsense, they don't
mention our movie until page 5!"

10
00:17:47,568 --> 00:17:50,195
"Where are you going, Miss?
We have a picture to shoot."

11
00:24:23,422 --> 00:24:27,676
"If you want to be an actress, you
need to have something the others don't."

12
00:28:42,239 --> 00:28:44,367
"I have something to show you."

13
00:28:52,966 --> 00:28:54,943
"May I have my chair?!"

14
00:28:56,570 --> 00:28:58,947
"Hey, you're not Napoleon.
You're just an extra!"

15
00:30:16,867 --> 00:30:19,319
"Don't laugh, George!
That's the future."

16
00:30:21,722 --> 00:30:24,575
"If that's the future,
you can have it!"

17
00:35:03,937 --> 00:35:07,716
"You and I belong to another era,
George. The world is talking now."

18
00:35:13,322 --> 00:35:16,700
"People want new faces,
talking faces.'

19
00:35:25,417 --> 00:35:27,368
"I wish it wasn't like
this, but the public wants

20
00:35:27,369 --> 00:35:29,346
fresh meat and the
public is never wrong."

21
00:35:41,583 --> 00:35:44,937
"I'm the one people come to see.
They never needed to hear me!"

22
00:35:52,845 --> 00:35:57,699
"Make your talking movies. I'll make a great movie.
And it's not like I need you for that!"

23
00:36:15,050 --> 00:36:16,927
"Fresh meat..."

24
00:37:02,931 --> 00:37:06,310
"What a coincidence, I was just thinking
about you. I signed with Kinograph!"

25
00:37:14,193 --> 00:37:17,070
"Maybe now we'll do
another picture together!"

26
00:38:30,435 --> 00:38:31,687
"Toys!"

27
00:42:43,313 --> 00:42:45,190
"We have to talk, George."

28
00:42:58,662 --> 00:43:00,789
"Why do you refuse to talk?"

29
00:43:30,444 --> 00:43:32,321
"I'm unhappy, George."

30
00:43:34,723 --> 00:43:37,451
"So are millions of us."

31
00:44:50,274 --> 00:44:52,776
"Your movie doesn't come out
until tomorrow and you're,

32
00:44:52,943 --> 00:44:55,821
already Hollywood's new sweetheart!
How do you explain that?"

33
00:45:04,313 --> 00:45:07,165
"I don't know. Maybe because I talk
and the audience can hear me."

34
00:45:15,424 --> 00:45:19,678
"People are tired of old actors mugging
at the camera to be understood."

35
00:45:36,069 --> 00:45:39,573
"Out with the old, in with the new.
Make way for the young!"

36
00:45:42,576 --> 00:45:44,286
"That's the life!"

37
00:46:07,809 --> 00:46:09,436
"I've made way for you."

38
00:47:07,911 --> 00:47:10,289
"It looks like we've gone bust."

39
00:47:20,048 --> 00:47:23,302
"Unless the movie is a big hit..."

40
00:52:02,439 --> 00:52:04,917
"Hello. I wanted to
talk to you. I..."

41
00:52:07,444 --> 00:52:09,796
"I just saw Tears of Love."

42
00:52:12,799 --> 00:52:15,052
"And now want a refund?"

43
00:52:22,935 --> 00:52:25,062
"Not too much mugging?"

44
00:52:33,820 --> 00:52:38,700
"No. I'm sorry about last night.
I didn't mean what I said..."

45
00:52:44,990 --> 00:52:48,043
"But you were right.
Make way for the young..."

46
00:53:02,307 --> 00:53:06,812
"It's an honor to meet you.
My father is a big fan."

47
00:53:15,496 --> 00:53:18,323
"Ok. We're going now.
I'll call you soon."

48
00:53:32,546 --> 00:53:35,549
"Take me home.
I want to be alone."

49
00:57:40,544 --> 00:57:43,922
"How long has it been
since I last paid you, Clifton?"

50
00:57:46,800 --> 00:57:48,552
"One year, Sir."

51
00:58:17,789 --> 00:58:19,917
"You're fired."

52
00:58:33,931 --> 00:58:36,433
"I'm not kidding, Clifton.
You're fired."

53
00:58:52,324 --> 00:58:55,285
"Keep the car. And find
yourself another job."

54
00:59:00,415 --> 00:59:02,918
"I don't want another job."

55
01:01:58,677 --> 01:02:01,180
"Congratulations! It's all sold,
you've got nothing left!"

56
01:07:04,773 --> 01:07:07,426
"Excuse me,
may I trouble you one moment?"

57
01:07:24,044 --> 01:07:26,547
"If only he could talk!"

58
01:08:21,435 --> 01:08:23,937
"Look what's become of you..."

59
01:08:27,065 --> 01:08:30,569
"You've been stupid!
You've been proud!"

60
01:08:50,297 --> 01:08:52,049
"Get back here, you loser!"

61
01:13:01,548 --> 01:13:03,550
"Oh my God! That's
George Valentin!"

62
01:13:09,556 --> 01:13:10,807
"I'll get my car!"

63
01:13:17,940 --> 01:13:20,567
"I'll say one thing, he
owes his life to that dog!"

64
01:14:00,315 --> 01:14:03,068
"We're ready. Go get
Miss Miller."

65
01:15:28,195 --> 01:15:30,697
"He's out of danger.
He needs rest now."

66
01:15:43,544 --> 01:15:47,422
"He was clutching this. It was
hard to get it away from him."

67
01:16:25,419 --> 01:16:28,547
"You think he could
rest at my house?"

68
01:18:39,553 --> 01:18:43,557
"I have to be on the set at nine o'clock.
We're shooting a very important scene."

69
01:19:24,431 --> 01:19:28,185
"Just think of it! *Sparkle of Love*,
starring George Valentin and Peppy Miller!"

70
01:19:47,287 --> 01:19:50,290
"George is a silent movie
actor. He's a nobody now."

71
01:20:00,551 --> 01:20:02,427
"What are you doing?"

72
01:20:07,432 --> 01:20:09,685
"I won't work anymore.
It's either him or me."

73
01:20:20,696 --> 01:20:24,074
"What I mean is, it's him AND
me! Or it's neither of us!"

74
01:20:33,792 --> 01:20:36,545
"Hey, I'm blackmailing
you! Get it?"

75
01:21:05,449 --> 01:21:07,701
"All right, send him
the damn screenplay."

76
01:21:47,074 --> 01:21:49,701
"I work for Miss Miller now."

77
01:21:55,666 --> 01:21:57,668
"Once again, she's
looking out for you."

78
01:22:31,451 --> 01:22:35,764
"Beware of your pride, if I may say so, Sir.
Miss Miller is a good person, believe me."

79
01:32:59,037 --> 01:33:04,042
"I feel so bad. I only wanted to
help you, take care of you..."

80
01:33:39,661 --> 01:33:42,664
"If only you would let
me help you, George Valentin."

81
01:33:48,045 --> 01:33:51,548
"It's impossible, Peppy. I'm washed up.
No one wants to see me speak."

82
01:33:59,206 --> 01:34:02,309
"There is one thing we
could try. Trust me."

83
01:36:15,410 --> 01:36:16,510
Cut!

84
01:36:17,211 --> 01:36:18,511
Perfect!

85
01:36:20,012 --> 01:36:21,002
Beautiful!

86
01:36:21,013 --> 01:36:22,913
Could you give me just one more?

87
01:36:24,414 --> 01:36:25,814
With pleasure!

88
01:36:28,215 --> 01:36:32,215
- Back to one!  - Okay, back to one, Guys!
- Everybody! Quiet! Back to one. Let's get ready to do this.

89
01:36:44,714 --> 01:36:45,914
Come on in, Guys!

90
01:36:51,715 --> 01:36:54,215
...everybody, quiet, please!

91
01:36:54,515 --> 01:36:55,515
Ready! On your mark!

92
01:36:57,716 --> 01:36:58,716
Quiet! Hold your talk!

93
01:37:07,416 --> 01:37:09,916
Okay! Let's do it, Guys!
Here we go! Picture's up!

94
01:37:12,017 --> 01:37:14,317
Here we go! And roll sound!

95
01:37:15,318 --> 01:37:16,218
Roll camera!

96
01:37:16,619 --> 01:37:17,819
Silence, please!

97
01:37:18,420 --> 01:37:20,820
And ACTION!

