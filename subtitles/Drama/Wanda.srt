1
00:02:29,112 --> 00:02:30,830
Come on, honey, have some coffee.

2
00:02:49,472 --> 00:02:51,349
Come on, you better get up.

3
00:02:57,152 --> 00:02:59,302
He's mad cause I'm here.

4
00:06:31,072 --> 00:06:35,623
Steve! Tell the boss I gotta go to court,
I'll be back in a couple of hours.

5
00:06:36,112 --> 00:06:38,182
Tell him I gotta go to court!

6
00:06:38,392 --> 00:06:41,064
I'll be back in a couple of hours.
See ya!

7
00:07:09,992 --> 00:07:11,107
Tony!

8
00:07:15,152 --> 00:07:17,063
Hello, Wanda. I'm coming!

9
00:07:32,432 --> 00:07:33,467
Picking coal again?

10
00:07:33,672 --> 00:07:36,106
Yes, picking coal again, Wanda.

11
00:07:37,672 --> 00:07:39,264
There ain't much left.

12
00:07:41,112 --> 00:07:42,989
Could you loan me a little bit of money?

13
00:07:43,232 --> 00:07:45,507
Well I don't have too much money.

14
00:07:46,672 --> 00:07:50,187
- Well that's okay.
- But if I had more, I would give you more.

15
00:07:50,632 --> 00:07:52,350
I just need a little bit.

16
00:07:59,032 --> 00:07:59,828
Here it is, Wanda.

17
00:08:00,032 --> 00:08:02,944
I hope that you'll get some more money

18
00:08:03,152 --> 00:08:05,029
out of somebody else.

19
00:08:05,232 --> 00:08:07,541
That's the best that I can do.

20
00:08:12,952 --> 00:08:14,863
Gonna pick some coal today?

21
00:08:16,472 --> 00:08:18,940
Yes, I pick a little coal.

22
00:08:19,152 --> 00:08:23,987
I ain't gonna do much now, all I'm going
to do is just pick this one more pail.

23
00:08:24,712 --> 00:08:27,749
And then I'm going to
take it easy for the day.

24
00:08:27,952 --> 00:08:32,946
This afternoon I will go fishing
for a few hours.

25
00:08:34,472 --> 00:08:39,990
And that will take up my time and I will
enjoy myself for a while.

26
00:09:30,992 --> 00:09:32,789
Wanda Goronski.

27
00:09:45,872 --> 00:09:47,146
Do you know where she is?

28
00:09:48,392 --> 00:09:50,952
I dunno, she wouldn't even care enough
to come to court.

29
00:09:52,352 --> 00:09:54,308
Well go look in the hall.

30
00:10:00,592 --> 00:10:02,628
Are those your children, over there?

31
00:10:03,152 --> 00:10:04,141
Those are my kids.

32
00:10:04,352 --> 00:10:06,183
And who is that young lady?

33
00:10:06,632 --> 00:10:10,386
That's Miss Godek.
She's been helping to take care of the kids.

34
00:10:10,632 --> 00:10:13,510
We kinda wanna get married,
cause the kids need a mother.

35
00:10:21,432 --> 00:10:22,945
She isn't in the hallway.

36
00:10:23,512 --> 00:10:25,230
Christ, that's just like her.

37
00:10:52,232 --> 00:10:53,421
She don't care about anything.

38
00:10:53,432 --> 00:10:55,741
She's a lousy wife, she's always
bumming around, drinking.

39
00:10:55,952 --> 00:10:57,783
Never took care of us,
never took care of the kids.

40
00:10:57,992 --> 00:11:00,745
I used to get up for work,
make my own breakfast,

41
00:11:00,952 --> 00:11:02,385
change the kids.

42
00:11:02,592 --> 00:11:04,742
Come home from work, she's
lying around on the couch.

43
00:11:04,952 --> 00:11:07,512
Kids are dirty, there's
diapers on the floor.

44
00:11:08,152 --> 00:11:11,827
Sometimes the kids is outside,
running around, nobody watching them.

45
00:11:12,632 --> 00:11:15,590
Wanda Goronski? Step up here, please.

46
00:11:19,672 --> 00:11:20,866
No smoking.

47
00:11:41,952 --> 00:11:44,227
Mrs. Goronski, your husband told me

48
00:11:44,472 --> 00:11:46,463
you deserted him and the children.

49
00:11:46,672 --> 00:11:48,071
What do you have to say?

50
00:11:50,672 --> 00:11:51,707
Nothing.

51
00:11:52,032 --> 00:11:53,670
Did you desert them?

52
00:11:56,472 --> 00:11:59,544
Listen, judge, if he wants a divorce,
just give it to him.

53
00:12:00,232 --> 00:12:03,781
Now Mrs. Goronski, you have
two young children here.

54
00:12:10,432 --> 00:12:12,502
They'd be o... better off with him.

55
00:12:13,192 --> 00:12:14,830
- What?
- They'd be better off with him.

56
00:12:15,032 --> 00:12:17,023
You have no objections to this divorce?

57
00:12:17,232 --> 00:12:19,188
No, I don't have any objection to it.

58
00:13:40,432 --> 00:13:42,627
Milt, there's someone here to see you.

59
00:13:46,352 --> 00:13:49,424
Hey, if you think I can make this
lot next week, you're crazy.

60
00:13:50,192 --> 00:13:52,945
What do you think, we can make
instant dresses around here?

61
00:13:55,352 --> 00:13:57,501
What can I do for you, lover?

62
00:13:59,112 --> 00:14:02,548
Um, I wanted to see about
getting my pay for last week.

63
00:14:02,752 --> 00:14:07,382
Remember, I worked two days last week and
I was supposed to get $12 a day for it.

64
00:14:07,592 --> 00:14:10,470
As I remember, you were paid for two days.

65
00:14:10,992 --> 00:14:12,869
Well I didn't get $24.

66
00:14:13,072 --> 00:14:16,144
Well, you were paid gross $24,

67
00:14:16,352 --> 00:14:21,221
less the taxes, and your pay came out
to $9.87.

68
00:14:21,992 --> 00:14:24,222
You mean you take out that much?
Out of...

69
00:14:24,432 --> 00:14:26,343
Yes, they do.

70
00:14:26,552 --> 00:14:28,508
They take out of mine, and yours,
and everybody else's.

71
00:14:28,712 --> 00:14:29,861
And that's all I get?

72
00:14:30,072 --> 00:14:32,028
Sorry, that's the score.

73
00:14:45,672 --> 00:14:50,700
Um, you said that you might be taking on
some more... Some more people this week.

74
00:14:51,072 --> 00:14:54,008
Do you think you might be able to use me?

75
00:14:54,072 --> 00:14:57,269
Well, we are taking on more people.

76
00:14:57,472 --> 00:15:00,589
But you're just too slow in our operations.

77
00:15:00,792 --> 00:15:01,907
And we can't use you.

78
00:15:02,392 --> 00:15:04,110
Well I could learn... I could...

79
00:15:04,312 --> 00:15:07,941
I'm sorry, my dear.
That's just the best I can do for you.

80
00:15:08,152 --> 00:15:09,870
You have to take it as it stands.

81
00:15:10,512 --> 00:15:14,630
You're just too slow for sewing operations
and that's it.

82
00:15:18,272 --> 00:15:19,671
Any questions?

83
00:15:22,552 --> 00:15:26,905
No... Okay, thank you.

84
00:15:35,792 --> 00:15:37,386
You want something, blondie?

85
00:15:40,952 --> 00:15:45,465
Yeah, I'll have a... uh... a Rolling Rock.

86
00:15:47,712 --> 00:15:49,065
Where's your car parked?

87
00:15:49,272 --> 00:15:50,751
That green Olds out there.

88
00:15:50,952 --> 00:15:53,420
- That green station wagon?
- That's the one.

89
00:16:00,432 --> 00:16:01,945
I'll take care of that.

90
00:17:15,352 --> 00:17:16,546
Where are you going?

91
00:17:32,992 --> 00:17:34,744
Well just a minute. [... ]

92
00:17:41,992 --> 00:17:44,142
Just a minute, I'll just be a minute.
Wait a minute.

93
00:18:06,912 --> 00:18:07,628
Wait just a minute!

94
00:23:18,192 --> 00:23:20,183
Hey, did you see a purse here?

95
00:23:39,552 --> 00:23:40,780
My wallet...

96
00:24:44,752 --> 00:24:47,949
Hey! I thought that door was locked!
We're closed!

97
00:24:48,192 --> 00:24:50,068
- I just wanna...
- Look, we're closed.

98
00:24:50,312 --> 00:24:52,906
I just wanna... I'll be
back in just one minute...

99
00:26:05,032 --> 00:26:06,943
What the hell are you doing, taking a bath?

100
00:26:19,232 --> 00:26:21,903
Hey! Come out!

101
00:26:38,912 --> 00:26:41,984
Hey, you got a towel or something?
There's no paper back there.

102
00:27:09,992 --> 00:27:12,187
Well could I have uh...
something to drink...

103
00:27:12,392 --> 00:27:15,304
Beer... Something like that...
Anything...

104
00:27:37,352 --> 00:27:38,910
You know what happened to me?

105
00:27:39,712 --> 00:27:41,350
Somebody stole all my money.

106
00:27:49,752 --> 00:27:52,186
Sitting in the movie...
And he stole all my money...

107
00:28:26,912 --> 00:28:29,551
Hey, have you got a comb or something
I could borrow?

108
00:28:29,912 --> 00:28:31,550
Took my comb too.

109
00:28:36,192 --> 00:28:39,262
Can I borrow a comb or something?

110
00:29:15,192 --> 00:29:16,068
Let's go.

111
00:29:16,272 --> 00:29:17,751
I'm good. Thank you.

112
00:29:49,512 --> 00:29:51,423
Wipe your mouth, will ya?

113
00:29:55,792 --> 00:29:57,510
Want some more spaghetti?

114
00:29:57,752 --> 00:30:01,027
No, uh... No, I had enough, thank you.

115
00:30:05,872 --> 00:30:08,784
Did you want that piece of bread?

116
00:30:09,792 --> 00:30:10,861
Can I have it?

117
00:30:12,592 --> 00:30:16,631
That's the best part I like.
Don't you like that? That part?

118
00:30:17,392 --> 00:30:21,461
Soak it up... Huh?

119
00:30:22,272 --> 00:30:23,421
You don't like it?

120
00:30:24,072 --> 00:30:25,141
I do.

121
00:30:40,472 --> 00:30:42,030
What's the matter? You got a headache?

122
00:31:19,192 --> 00:31:24,945
- Mr. Dennis, don't you want to
know what my name is? - No.

123
00:31:36,672 --> 00:31:37,991
Are you married?

124
00:31:41,712 --> 00:31:43,464
You're wearing a wedding ring...

125
00:31:44,832 --> 00:31:46,629
I don't like nosey people.

126
00:31:56,272 --> 00:31:57,999
I was just trying to be...

127
00:31:58,312 --> 00:32:00,268
Hey! Look, don't touch my head!

128
00:32:03,072 --> 00:32:05,427
I was just trying to be friendly...

129
00:32:08,192 --> 00:32:11,069
I don't like friendly people.

130
00:32:26,392 --> 00:32:29,111
Get up and get dressed and go on,
get me something to eat.

131
00:32:32,192 --> 00:32:36,999
It's night time... Middle...
Middle of the night...

132
00:32:37,152 --> 00:32:39,382
Won't be anything open now,
would there?

133
00:32:40,192 --> 00:32:43,423
Go outside and turn left, two doors down.
It's open all night.

134
00:32:49,552 --> 00:32:50,746
Well come on!

135
00:32:51,032 --> 00:32:52,829
Make it snappy, I'm hungry!

136
00:33:16,992 --> 00:33:18,710
Can't find my wallet...

137
00:33:23,912 --> 00:33:26,346
Oh, well. Nothing in it anyhow.

138
00:33:27,792 --> 00:33:28,827
Now... Um...

139
00:33:30,152 --> 00:33:32,221
What do you want to get?

140
00:33:34,032 --> 00:33:36,341
I want you go get three hamburgers.
Nothing on them.

141
00:33:37,152 --> 00:33:41,555
No garbage. No onions, no butter on the bun.

142
00:33:41,952 --> 00:33:43,510
I want the bun toasted.

143
00:33:45,592 --> 00:33:47,071
Get me a newspaper.

144
00:33:49,432 --> 00:33:50,660
Hey, are you listening?

145
00:33:50,952 --> 00:33:55,021
Yeah. Is that all?

146
00:33:56,832 --> 00:34:00,141
Where can I get this stuff?

147
00:34:00,512 --> 00:34:04,790
You go out of the hotel and turn left. It's
two doors down. The place is open all night.

148
00:34:04,992 --> 00:34:07,108
I go out of the hotel...

149
00:34:07,672 --> 00:34:09,071
and turn left, right?

150
00:34:15,352 --> 00:34:16,580
Three hamburgers.

151
00:34:18,192 --> 00:34:19,999
And a newspaper.

152
00:34:28,712 --> 00:34:30,145
What am I gonna get it with?

153
00:34:34,752 --> 00:34:37,220
Ooh, wait a minute, I'm
don't need this much.

154
00:34:38,272 --> 00:34:39,944
I'm thinking you might need it.

155
00:34:40,272 --> 00:34:41,421
That much?

156
00:34:45,832 --> 00:34:47,424
Three hamburgers and uh...

157
00:34:47,992 --> 00:34:49,186
Newspaper!

158
00:37:19,192 --> 00:37:20,999
Mr. Dennis!

159
00:37:25,632 --> 00:37:27,999
Mr. Dennis, are you in there?

160
00:37:34,352 --> 00:37:35,944
Hey! Hey, stupid!

161
00:37:38,352 --> 00:37:41,185
I forgot which number it was...

162
00:37:41,432 --> 00:37:44,742
and the elevator man told me it was...

163
00:37:56,312 --> 00:37:58,382
Hey, put that on the table!

164
00:38:03,792 --> 00:38:07,421
Did you know that place you told me
was open all night, it wasn't open.

165
00:38:07,632 --> 00:38:12,706
I met a man that told me where there was
another one, and I had to go there.

166
00:38:14,872 --> 00:38:17,828
What's the matter, you crazy or something?

167
00:38:18,112 --> 00:38:20,023
You talk to every bum on the street?

168
00:38:20,232 --> 00:38:21,631
I didn't talk to anybody.

169
00:38:21,832 --> 00:38:23,948
You just told me two you were gabbing with.

170
00:38:31,912 --> 00:38:33,311
There's your change...

171
00:38:36,832 --> 00:38:38,788
What'd you do that for? That hurt.

172
00:38:53,032 --> 00:38:56,229
I thought I told you no onions. No garbage!

173
00:38:57,752 --> 00:38:58,999
Take it off.

174
00:39:01,592 --> 00:39:03,991
Where's the waste basket...

175
00:39:08,352 --> 00:39:09,785
Hey, there's my wallet.

176
00:39:09,992 --> 00:39:11,471
What's it doing in here?

177
00:39:30,072 --> 00:39:31,300
How's that?

178
00:39:38,672 --> 00:39:40,583
I took all the onions off!

179
00:39:44,672 --> 00:39:46,902
I don't know why you don't like onions.
I do...

180
00:42:30,752 --> 00:42:32,026
What are you doing?

181
00:42:32,952 --> 00:42:33,941
Get in!

182
00:42:55,432 --> 00:42:57,582
Why don't you just use these?

183
00:42:59,672 --> 00:43:03,347
Look, you wanna walk - get out.
Go ahead!

184
00:43:05,952 --> 00:43:07,590
Hurry up! Make up your mind!

185
00:43:29,792 --> 00:43:30,861
Go ahead!

186
00:43:31,512 --> 00:43:32,422
Read!

187
00:43:35,392 --> 00:43:37,462
"Mr. Malucchi... "

188
00:43:38,192 --> 00:43:41,787
"could only describe... "

189
00:43:41,992 --> 00:43:44,460
"the man as being the kind... "

190
00:43:44,792 --> 00:43:46,703
"you wouldn't notice... "

191
00:43:47,912 --> 00:43:52,622
"He was just a customer... "

192
00:43:52,832 --> 00:43:55,107
"sitting at the bar... "

193
00:43:56,192 --> 00:43:57,784
"he waited"

194
00:43:58,032 --> 00:43:59,943
"til everybody left. "

195
00:44:00,432 --> 00:44:03,868
"Although the robber came in alone,"

196
00:44:04,232 --> 00:44:05,950
"he left... "

197
00:44:06,832 --> 00:44:08,151
"according... "

198
00:44:09,352 --> 00:44:11,707
"to Mr. Malucchi... "

199
00:44:11,992 --> 00:44:17,350
"with a female whom he wasn't
able to see... "

200
00:44:17,592 --> 00:44:22,746
"because he was lying behind the bar. "

201
00:44:28,792 --> 00:44:29,907
Go ahead!

202
00:44:30,272 --> 00:44:31,227
That's all.

203
00:44:40,272 --> 00:44:42,740
Hey, what are you trying to get me into?

204
00:45:00,792 --> 00:45:01,747
Get out!

205
00:45:19,352 --> 00:45:21,024
I didn't do anything.

206
00:45:54,632 --> 00:45:56,065
Read it again.

207
00:45:59,032 --> 00:46:03,024
"Police are on the lookout for the couple. "

208
00:46:05,952 --> 00:46:07,590
That's all it says.

209
00:46:11,792 --> 00:46:14,431
Says police are on the
lookout for the couple.

210
00:46:15,472 --> 00:46:17,224
Go back to your comics.

211
00:47:12,432 --> 00:47:15,185
You don't have to do anything... just drive.

212
00:47:20,232 --> 00:47:22,382
I'm waiting for my kid to come home.

213
00:47:23,272 --> 00:47:26,150
The little bit that I got,
I don't wanna lose.

214
00:47:27,272 --> 00:47:30,548
If I go with you, maybe...

215
00:47:31,112 --> 00:47:36,027
one out of a hundred, we get away.
I'm not taking that chance now.

216
00:47:37,512 --> 00:47:39,786
- Sure?
- That's it.

217
00:47:39,672 --> 00:47:41,708
I'm waiting for my kid...

218
00:47:42,672 --> 00:47:44,025
and...

219
00:47:46,632 --> 00:47:50,386
I wanna straighten him out. Try my hardest.

220
00:47:50,712 --> 00:47:53,385
I haven't got long to go.

221
00:47:53,752 --> 00:47:59,668
So the little bit that I got...
on the job that I have...

222
00:48:00,312 --> 00:48:02,462
[... ] I'm not taking any chances.

223
00:48:03,952 --> 00:48:05,510
I've had enough.

224
00:48:05,912 --> 00:48:09,951
Joe, won't be any chances.
All you do is drive.

225
00:48:11,432 --> 00:48:13,229
That's a big chance.

226
00:48:14,192 --> 00:48:15,784
I can't do it...

227
00:48:17,072 --> 00:48:18,585
I cannot do it...

228
00:48:30,312 --> 00:48:32,985
I got my mind made up... Norman.

229
00:48:35,472 --> 00:48:37,791
I won't do it.

230
00:49:15,752 --> 00:49:17,946
Do you drive?

231
00:49:19,232 --> 00:49:21,382
Yeah, I guess so, kind of.

232
00:49:31,792 --> 00:49:33,191
Yeah, get over here.

233
00:50:12,992 --> 00:50:15,186
Are we still on 81?

234
00:50:16,232 --> 00:50:17,984
- Yeah.
- Just keep going straight.

235
00:50:38,832 --> 00:50:41,983
Have you still got...
have you still got a headache?

236
00:50:53,472 --> 00:50:55,986
Hey maybe I better pull over...

237
00:50:59,392 --> 00:51:03,542
You want me to pull over or something?

238
00:51:06,232 --> 00:51:08,426
Should I pull over or something?

239
00:51:08,672 --> 00:51:11,824
Just keep quiet, will ya?
That's all, just go straight ahead.

240
00:51:59,712 --> 00:52:03,940
Let's go. Come on, let's go!

241
00:54:05,512 --> 00:54:07,343
I feel so hungry, aren't you?

242
00:54:23,432 --> 00:54:25,900
Hey, go away. Go away there. Go away.

243
00:54:42,312 --> 00:54:43,791
The sun's going down.

244
00:55:11,232 --> 00:55:13,267
Hey, let me see...

245
00:55:16,552 --> 00:55:19,589
Why don't you do something about your hair?

246
00:55:19,792 --> 00:55:22,101
- Looks terrible.
- What can I do?

247
00:55:24,952 --> 00:55:26,624
I lost all my [... ].

248
00:55:31,312 --> 00:55:32,461
Why don't you...

249
00:55:33,352 --> 00:55:34,865
Why don't you cover it up?

250
00:55:35,312 --> 00:55:36,999
Cover it up?

251
00:55:39,032 --> 00:55:41,431
What would I cover it up with?

252
00:55:43,072 --> 00:55:44,824
Why don't you get a hat and put it...

253
00:55:45,072 --> 00:55:46,187
A hat?

254
00:55:47,472 --> 00:55:48,587
Yeah...

255
00:55:49,352 --> 00:55:50,580
A nice hat...

256
00:55:54,992 --> 00:55:56,555
Well...

257
00:55:58,272 --> 00:55:59,785
cause I... uh...

258
00:56:00,952 --> 00:56:03,227
don't have anything to get a hat with.

259
00:56:12,112 --> 00:56:13,545
I don't have anything...

260
00:56:14,552 --> 00:56:16,668
Never did have anything,
never will have anything.

261
00:56:18,352 --> 00:56:19,546
You're stupid.

262
00:56:20,432 --> 00:56:21,660
I'm stupid...

263
00:56:23,592 --> 00:56:25,981
You don't want anything,
you won't have anything.

264
00:56:26,192 --> 00:56:30,026
You don't have anything, you're nothing.
Might as well be dead.

265
00:56:30,632 --> 00:56:33,544
You're not even a citizen
of the United States.

266
00:56:41,472 --> 00:56:43,905
I guess I'm dead then.

267
00:56:48,072 --> 00:56:51,555
What do you mean? Is that what you wanna be?

268
00:56:51,832 --> 00:56:52,708
Dead.

269
00:57:41,232 --> 00:57:42,551
Come back here!

270
00:57:42,992 --> 00:57:44,061
Come back!

271
00:57:56,712 --> 00:57:57,999
Hey!

272
00:57:58,352 --> 00:57:59,999
Come here!

273
00:58:50,712 --> 00:58:52,987
Why don't you get a hat.

274
00:59:15,272 --> 00:59:19,070
Mr. Dennis, wake up.

275
00:59:21,312 --> 00:59:23,985
Mr. Dennis, come on, it's getting cold.

276
00:59:33,912 --> 00:59:37,187
Mr. Dennis...
Come on, Mr. Dennis, I'm getting cold.

277
01:01:10,952 --> 01:01:13,068
I thought I told you to get a dress.

278
01:01:13,392 --> 01:01:14,950
I did, I got one in here.

279
01:01:15,592 --> 01:01:18,581
Get in the back and put it on.

280
01:01:26,272 --> 01:01:27,625
No slacks!

281
01:01:29,592 --> 01:01:31,662
When you're with me, no slacks!

282
01:01:39,192 --> 01:01:41,023
No hair curlers.

283
01:01:43,512 --> 01:01:45,104
Makes you look cheap.

284
01:01:59,512 --> 01:02:01,104
Wanna look cheap?

285
01:02:20,912 --> 01:02:23,027
Hey. Get up there.

286
01:03:32,992 --> 01:03:34,550
Hey, they fit.

287
01:04:02,112 --> 01:04:03,545
Where's your husband?

288
01:04:07,392 --> 01:04:09,745
What husband?

289
01:04:18,272 --> 01:04:19,999
<i>Your husband.</i>

290
01:04:28,272 --> 01:04:31,662
I guess he got hisself a
real good wife by now.

291
01:04:38,112 --> 01:04:40,228
Got a real good wife.

292
01:04:48,512 --> 01:04:50,070
What about the kids?

293
01:04:51,712 --> 01:04:52,781
Kids?

294
01:04:58,392 --> 01:05:01,031
Yeah. I saw their picture in your wallet.

295
01:05:02,592 --> 01:05:04,025
They're with him.

296
01:05:11,032 --> 01:05:12,590
Better off with him.

297
01:05:18,952 --> 01:05:20,590
I'm just no good.

298
01:05:23,912 --> 01:05:25,391
Just no good!

299
01:05:57,272 --> 01:05:58,625
Mr. Dennis...

300
01:06:01,632 --> 01:06:03,463
Where are we going?
Huh?

301
01:06:07,552 --> 01:06:08,951
No questions!

302
01:06:12,232 --> 01:06:14,143
When you're with me, no questions.

303
01:06:19,712 --> 01:06:20,906
Move over here.

304
01:09:27,272 --> 01:09:28,261
Hey, pop!

305
01:09:35,152 --> 01:09:39,827
- Oh, pop, it's good to see you again.
- It's been a long time.

306
01:10:48,672 --> 01:10:50,469
If you'll follow me, please.

307
01:10:54,752 --> 01:10:57,949
Now as we go down
we will see nidges in the walls.

308
01:10:58,152 --> 01:11:01,781
These nidges in the walls are the places
where they buried their dead.

309
01:11:01,992 --> 01:11:04,870
If you see one that's flat on top

310
01:11:05,072 --> 01:11:07,427
that shows a good christian
that died and was buried in the catacombs.

311
01:11:07,632 --> 01:11:10,783
But if you see one that has a
[... ] over it, sort of a hat...

312
01:11:10,992 --> 01:11:13,347
then that's a place where
a martyr was buried.

313
01:11:13,552 --> 01:11:18,785
And of course we all know that a martyr is a
person who was killed for his or her faith.

314
01:11:19,432 --> 01:11:23,425
Here on the side we have Saint Thecla
who was a disciple of Saint Paul.

315
01:11:23,632 --> 01:11:28,626
- Are you looking for a job?
- Yeah... Yeah, pop.

316
01:11:29,192 --> 01:11:32,264
Did you find one...
Find a job?

317
01:11:32,552 --> 01:11:33,951
Not yet, pop.

318
01:11:35,192 --> 01:11:38,184
Good boy... Gotta be a good boy.

319
01:11:39,712 --> 01:11:43,500
So you find a job,
and you'll be alright.

320
01:11:45,312 --> 01:11:47,143
You... working every day.

321
01:11:49,632 --> 01:11:52,783
Lotsa... lotsa jobs in the store.

322
01:12:01,192 --> 01:12:05,184
Here, pop, you take that.

323
01:12:05,632 --> 01:12:07,270
Come on, pop, you take it.

324
01:12:07,472 --> 01:12:09,349
No, no, I don't want your money.

325
01:12:09,552 --> 01:12:12,146
Now look... I saw that... you need it.
Come on now...

326
01:12:12,352 --> 01:12:14,502
I don't want your money.

327
01:12:15,792 --> 01:12:17,544
I got my own money.

328
01:12:19,472 --> 01:12:23,670
Here, take it back. I don't want your money.

329
01:12:27,592 --> 01:12:30,902
You was a bad boy, I don't want your money.

330
01:12:32,632 --> 01:12:33,747
Hey, pop.

331
01:12:35,032 --> 01:12:36,988
I'll be back in about a week.

332
01:12:37,632 --> 01:12:40,100
And then I'll have a good job.

333
01:12:41,432 --> 01:12:43,229
I'll like it then.

334
01:13:18,592 --> 01:13:19,627
Come here.

335
01:13:34,192 --> 01:13:36,228
Here. I wrote it all down, step by step.

336
01:13:38,192 --> 01:13:39,181
Read it.

337
01:13:42,512 --> 01:13:44,025
Memorize it, huh?

338
01:13:44,992 --> 01:13:45,868
One:

339
01:13:46,872 --> 01:13:48,021
Get to the house.

340
01:13:48,232 --> 01:13:50,826
Two: gain entrance.

341
01:13:51,152 --> 01:13:53,382
Mr. Dennis, I can't do this.

342
01:13:53,592 --> 01:13:54,911
Ah, come on!

343
01:13:57,912 --> 01:13:59,301
No, Mr. Dennis. I really mean it.

344
01:14:02,872 --> 01:14:04,464
I can't do it.

345
01:14:07,632 --> 01:14:09,145
What do you mean, you can't do it?

346
01:14:09,792 --> 01:14:13,148
Just what I've been trying to tell you,
I can't do it.

347
01:14:17,552 --> 01:14:18,621
You can do it!

348
01:14:19,472 --> 01:14:21,269
I can't!

349
01:14:22,632 --> 01:14:25,465
Now here. You take this and memorize it.

350
01:14:26,112 --> 01:14:27,591
Come on.
There, no go ahead.

351
01:14:31,552 --> 01:14:32,746
I can't!

352
01:15:15,512 --> 01:15:18,231
I can't do it... I can't do it...

353
01:15:18,472 --> 01:15:20,747
I can't do it, Mr. Dennis...

354
01:15:21,032 --> 01:15:23,387
I can't... I can't...

355
01:15:27,592 --> 01:15:29,866
I can't do it.

356
01:15:32,352 --> 01:15:33,580
Now you listen to me.

357
01:15:42,672 --> 01:15:45,505
Wanda... Maybe you never
did anything before...

358
01:15:48,752 --> 01:15:50,265
Maybe you never did...

359
01:15:53,312 --> 01:15:55,223
But you're gonna do this.

360
01:16:09,232 --> 01:16:11,826
Number one: gain entrance...

361
01:16:12,832 --> 01:16:14,185
Wait a minute!

362
01:16:15,112 --> 01:16:17,023
Number one: get to the house.

363
01:16:17,232 --> 01:16:19,382
Oh yeah. Number one: get to the house.

364
01:16:19,592 --> 01:16:22,106
- Number two: get entrance. Right?
- Right.

365
01:16:24,352 --> 01:16:26,741
Hey, what if they won't let us in?

366
01:16:27,592 --> 01:16:31,062
Look, would you stop raising problems.
Just get the plan, come on.

367
01:16:31,832 --> 01:16:33,060
Number three?

368
01:16:33,592 --> 01:16:34,945
Number three...

369
01:16:46,472 --> 01:16:47,905
Hey, are you alright?

370
01:17:19,072 --> 01:17:21,540
What's the matter?
Are you sick or something?

371
01:17:22,312 --> 01:17:23,711
No, I'm alright.

372
01:17:23,912 --> 01:17:26,187
So I guess it's time to go, isn't it?

373
01:17:27,872 --> 01:17:29,703
- Yeah.
- I guess we better go then.

374
01:17:38,432 --> 01:17:41,981
Oh for Christ's sake!
What the hell is wrong with you?!

375
01:17:42,592 --> 01:17:45,504
I'm alright, Mr. Dennis, I really am...
I'm alright now.

376
01:18:11,672 --> 01:18:13,185
The telephone is over here.

377
01:18:13,712 --> 01:18:14,827
Mr. Anderson?

378
01:18:16,752 --> 01:18:18,231
What is this...?

379
01:18:18,432 --> 01:18:19,501
- Get over there.
- What's going on?

380
01:18:29,672 --> 01:18:32,345
Get him loose! Get him loose!

381
01:18:33,712 --> 01:18:36,306
You turn him loose! Stop it!

382
01:18:36,512 --> 01:18:37,388
Stop it!

383
01:18:40,832 --> 01:18:42,948
Get over there on that couch.
Get over there!

384
01:18:48,112 --> 01:18:49,864
You! Get over there!

385
01:18:52,232 --> 01:18:53,824
You girls - sit down!

386
01:19:10,712 --> 01:19:11,428
Turn your head around.

387
01:19:20,512 --> 01:19:21,547
See this? Huh?

388
01:19:24,392 --> 01:19:26,223
That's a real live bomb.

389
01:19:29,632 --> 01:19:32,908
Set to go off in exactly
one hour and 15 minutes.

390
01:19:33,352 --> 01:19:34,387
Anderson...

391
01:19:37,072 --> 01:19:38,982
You cooperate with us, and...

392
01:19:40,072 --> 01:19:42,142
we'll get back in time to disarm it.

393
01:19:49,352 --> 01:19:50,705
Hold it in your lap.

394
01:20:00,232 --> 01:20:01,585
Don't move.

395
01:20:06,112 --> 01:20:07,750
I set the trigger.

396
01:20:11,512 --> 01:20:13,503
Set for the proper time.

397
01:20:22,272 --> 01:20:23,307
You hear that?

398
01:20:29,592 --> 01:20:30,820
Be careful.

399
01:20:39,032 --> 01:20:41,181
Anderson,

400
01:20:45,472 --> 01:20:48,225
you're taking me to work with you.

401
01:20:48,432 --> 01:20:49,751
Come on, put that coat on.

402
01:20:57,032 --> 01:20:57,987
Hurry up.

403
01:21:01,672 --> 01:21:03,310
Hey, wait! Wait! Wait!

404
01:21:07,112 --> 01:21:08,431
You've got the key...

405
01:21:15,752 --> 01:21:17,105
You did good.

406
01:21:19,632 --> 01:21:21,190
You're really something.

407
01:21:23,272 --> 01:21:24,387
Go ahead.

408
01:24:33,112 --> 01:24:34,261
Let's go!

409
01:24:35,112 --> 01:24:36,306
Come on, come on!

410
01:24:43,352 --> 01:24:44,626
Straight ahead.

411
01:25:44,352 --> 01:25:46,024
You're not gonna get away with it.

412
01:25:49,472 --> 01:25:50,746
Let's go.

413
01:25:59,072 --> 01:26:02,030
- You're gonna get caught.
- Mr. Anderson, shut up!

414
01:26:05,552 --> 01:26:07,144
When we get to the bank...

415
01:26:08,232 --> 01:26:10,541
you'll do exactly what I tell you.

416
01:26:19,112 --> 01:26:20,181
Come on. Watch it.

417
01:26:33,792 --> 01:26:36,784
Good morning, Jim, how are you...
Would you step back with me, please.

418
01:26:37,032 --> 01:26:39,751
And you folks, come down here.
I'd like to speak with you for a minute.

419
01:26:52,472 --> 01:26:54,463
It appears we have a hold-up on our hands.

420
01:27:00,112 --> 01:27:02,704
Oh, I can't find it...

421
01:27:03,992 --> 01:27:05,311
I don't, uh...

422
01:27:40,792 --> 01:27:43,784
You bring those cars in
as soon as possible, you understand?

423
01:27:46,032 --> 01:27:48,671
Could you tell me how to get to the bank?

424
01:28:52,232 --> 01:28:57,147
Cars 1, 2 and 6, and 67.
Wyoming Avenue, Third National Bank.

425
01:28:59,792 --> 01:29:00,499
Fill it up.

426
01:29:05,352 --> 01:29:07,468
You! Get out there, stand over there.

427
01:29:08,272 --> 01:29:09,227
Closer.

428
01:29:20,992 --> 01:29:22,664
Come on, put it down there.

429
01:29:24,592 --> 01:29:25,820
Move over there.

430
01:29:49,992 --> 01:29:53,268
Hold it! Drop your gun!

431
01:29:55,952 --> 01:29:58,625
Drop it! Drop that gun!

432
01:30:11,072 --> 01:30:12,471
- Come, stay there.
- Okay, it's all over.

433
01:30:52,072 --> 01:30:55,109
<i>...we repeat our first bulletin
[... ] news pictures.</i>

434
01:30:56,112 --> 01:30:57,750
<i>Norman Dennis,
the bank bandit,</i>

435
01:30:57,952 --> 01:31:01,740
<i>finally died just moments ago
at State General Hospital.</i>

436
01:31:02,272 --> 01:31:07,392
<i>You are seeing Dennis as
he was shot down in the</i>

437
01:31:07,592 --> 01:31:09,025
<i>Third National Bank this morning, by police.</i>

438
01:31:12,512 --> 01:31:14,548
Ah, you don't ever say nothing.

439
01:31:15,992 --> 01:31:17,425
I'm talking and talking and talking

440
01:31:17,632 --> 01:31:19,588
and you're just sitting there.

441
01:31:34,872 --> 01:31:37,181
I wanna get us some fresh beers.

442
01:31:40,432 --> 01:31:42,627
Bartender, can we have a
couple of fresh beers here?

443
01:31:42,832 --> 01:31:46,108
<i>...at this point,
the bomb was inoperable.</i>

444
01:31:46,472 --> 01:31:50,306
<i>We then turned around to examine</i>

445
01:31:50,512 --> 01:31:52,025
<i>what appeared to be dynamite.</i>

446
01:31:52,552 --> 01:31:57,751
<i>And on dismantling the dynamite, it appears</i>

447
01:31:57,952 --> 01:32:00,341
<i>that this was strictly a
dummy bomb,</i>

448
01:32:02,552 --> 01:32:03,722
<i>used to scare these people.</i>

449
01:32:04,312 --> 01:32:07,463
<i>This bomb, to the ordinary person...</i>

450
01:37:42,912 --> 01:37:45,221
Honey, you waiting for somebody?

