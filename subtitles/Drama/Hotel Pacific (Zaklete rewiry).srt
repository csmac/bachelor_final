1
00:03:52,941 --> 00:03:54,817
Oh, sorry.

2
00:03:54,985 --> 00:03:57,737
Vera?
Bob Leckie.

3
00:03:57,905 --> 00:03:59,822
I'm your neighbor
from across the street.

4
00:03:59,990 --> 00:04:01,741
I know who you are, Bob.

5
00:04:03,327 --> 00:04:05,995
I thought I was going shopping,
but then I passed Saint Mary's

6
00:04:06,163 --> 00:04:08,706
and thought I'd pray instead.

7
00:04:08,874 --> 00:04:11,500
I joined the Marines.

8
00:04:11,668 --> 00:04:14,170
Thought I'd do my bit.

9
00:04:14,338 --> 00:04:16,422
Fifth in line.

10
00:04:16,590 --> 00:04:19,425
Well, if I don't see you
before you go...

11
00:04:19,593 --> 00:04:22,219
take care of yourself.

12
00:04:25,849 --> 00:04:28,976
Um, wait, Vera,
let me get that for you.

13
00:04:29,144 --> 00:04:31,354
Oh, thank you.

14
00:04:31,521 --> 00:04:34,899
Maybe I'll write you.

15
00:04:35,067 --> 00:04:37,443
All right.

16
00:04:44,826 --> 00:04:48,037
Basilone!
Over here.

17
00:04:48,205 --> 00:04:49,830
Excuse me, folks.

18
00:04:49,998 --> 00:04:52,375
- I miss anything?
- Nah.

19
00:04:52,542 --> 00:04:55,002
The movie hasn't
started yet.

20
00:04:55,170 --> 00:04:57,797
Don't tell me
I missed this thing.

21
00:04:57,965 --> 00:05:00,633
- Chesty's come and gone.
- Yeah, just missed him.

22
00:05:00,801 --> 00:05:02,218
Come on, did I
fuck this up or what?

23
00:05:02,386 --> 00:05:04,136
- Oh, yeah.
- Man: Attention on deck!

24
00:05:09,768 --> 00:05:11,727
Carry on.

25
00:05:13,355 --> 00:05:14,438
Fuck you.

26
00:05:22,030 --> 00:05:25,574
The uniform that you wear

27
00:05:25,742 --> 00:05:28,953
and the globe-and-anchor
emblem that you have earned

28
00:05:29,121 --> 00:05:32,915
will make the difference
between the freedom of the world

29
00:05:33,083 --> 00:05:35,584
and its enslavement.

30
00:05:37,170 --> 00:05:39,672
December 7th was
quite a day

31
00:05:39,840 --> 00:05:41,924
in Pearl Harbor, Hawaii.

32
00:05:42,092 --> 00:05:44,343
On the same day,
December 8th

33
00:05:44,511 --> 00:05:46,762
on the other side of
the International Dateline,

34
00:05:46,930 --> 00:05:49,724
in places called Guam, Wake,

35
00:05:49,891 --> 00:05:52,309
the Malay Peninsula,

36
00:05:52,477 --> 00:05:55,688
Hong Kong and
the Philippine Islands

37
00:05:55,856 --> 00:05:59,483
were also attacked
by the army, air force

38
00:05:59,651 --> 00:06:03,571
and navy of
the empire of Japan.

39
00:06:03,739 --> 00:06:06,991
The Japanese
are in the process

40
00:06:07,159 --> 00:06:09,076
of taking half
of the world.

41
00:06:09,244 --> 00:06:10,911
And they mean
to keep it

42
00:06:11,079 --> 00:06:13,998
with death from
the air, land and sea.

43
00:06:14,166 --> 00:06:17,168
But here is what the Japs
are not expecting:

44
00:06:17,335 --> 00:06:20,463
the United States
Marine Corps.

45
00:06:20,630 --> 00:06:24,592
Now never mind Europe-

46
00:06:24,760 --> 00:06:27,136
the Nazis, Mussolini.

47
00:06:27,304 --> 00:06:30,931
Hitler is not gonna
be ourjob,

48
00:06:31,099 --> 00:06:33,851
not until they can't
whip him without us.

49
00:06:36,021 --> 00:06:38,022
The Pacific will be
our theater of war.

50
00:06:38,190 --> 00:06:41,025
The Marines will do battle
with the Japs

51
00:06:41,193 --> 00:06:44,195
on tiny specks of turf
that we have never heard of.

52
00:06:44,362 --> 00:06:48,783
You noncommissioned officers,

53
00:06:48,950 --> 00:06:52,203
you are the sinew
and the muscle of the corps.

54
00:06:52,370 --> 00:06:56,957
The orders come from the brass
and you get it done.

55
00:06:57,125 --> 00:06:59,960
And whenever
this war is over,

56
00:07:00,128 --> 00:07:02,630
when we have swept upon
the main islands of Japan

57
00:07:02,798 --> 00:07:06,634
and destroyed every scrap
of that empire,

58
00:07:06,802 --> 00:07:09,303
the strategy will have been
that of others.

59
00:07:09,471 --> 00:07:13,808
The victory will
have been won by you-

60
00:07:13,975 --> 00:07:16,977
you, the NCOs,

61
00:07:17,145 --> 00:07:20,439
with the chevrons on your
sleeves and the instincts in your guts

62
00:07:20,607 --> 00:07:22,983
and the blood on your boots.

63
00:07:27,114 --> 00:07:31,075
Those of you who are lucky enough
to get home for Christmas,

64
00:07:31,243 --> 00:07:33,536
hold your loved ones dearly

65
00:07:33,703 --> 00:07:36,622
and join them in prayers
for peace on earth

66
00:07:36,790 --> 00:07:38,999
and goodwill toward all men.

67
00:07:39,167 --> 00:07:41,877
And then report back here

68
00:07:42,045 --> 00:07:45,631
ready to sail across
God's vast ocean

69
00:07:45,799 --> 00:07:47,883
where we will meet
our enemy

70
00:07:48,051 --> 00:07:50,636
and kill them all.

71
00:07:50,804 --> 00:07:54,056
Merry Christmas.

72
00:07:54,224 --> 00:07:56,642
Happy 1942.

73
00:07:58,061 --> 00:08:00,312
Man:
Squad, atten-hut!

74
00:08:00,480 --> 00:08:02,648
Carry on.

75
00:08:20,876 --> 00:08:22,001
I got it, Ma.

76
00:08:22,169 --> 00:08:24,712
Man:
Angelo? Angelo?

77
00:08:24,880 --> 00:08:26,505
- Hey!
- Where you fellas been?

78
00:08:26,673 --> 00:08:29,550
We had to walk from the station
and we took the wrong street.

79
00:08:29,718 --> 00:08:31,594
Yeah, Manny had the wrong street.
You believe that?

80
00:08:31,761 --> 00:08:34,680
Get in here.
Come on, come on.

81
00:08:34,848 --> 00:08:36,515
Salute!

82
00:08:36,683 --> 00:08:38,726
Come here.
Hey, everybody!

83
00:08:38,894 --> 00:08:41,687
- This is JP and Manny!
- All: Hey!

84
00:08:41,855 --> 00:08:43,314
Hey, everybody.
How you doin'?

85
00:08:43,481 --> 00:08:46,442
Nice to see you.
Good to see you.

86
00:08:46,610 --> 00:08:48,527
Thank you, sir.

87
00:08:48,695 --> 00:08:49,904
JP.

88
00:08:56,203 --> 00:08:58,871
Your wife is
so beautiful!

89
00:08:59,039 --> 00:09:03,250
I wish my sons
could marry someone this beautiful.

90
00:09:03,418 --> 00:09:04,460
- Eh!
- Ma!

91
00:09:04,628 --> 00:09:05,961
Manny:
You sound like my mama.

92
00:09:06,129 --> 00:09:07,963
She could not understand how
I could get through high school

93
00:09:08,131 --> 00:09:11,133
- without that wedding date set.
- I could.

94
00:09:11,301 --> 00:09:12,635
All right.

95
00:09:12,802 --> 00:09:15,346
Everybody listen up,
please.

96
00:09:15,513 --> 00:09:17,348
JP Morgan,

97
00:09:17,515 --> 00:09:19,350
Manny Rodriguez,
welcome to our home.

98
00:09:19,517 --> 00:09:21,185
We're happy you
could join us tonight.

99
00:09:21,353 --> 00:09:25,272
Soon two of my brothers are
leaving to serve their country.

100
00:09:25,440 --> 00:09:27,483
And George,
we're losing sleep wondering

101
00:09:27,651 --> 00:09:31,070
what Marine Corps boot camp
is gonna do to you.

102
00:09:31,238 --> 00:09:34,657
My brother John, you've been there
rough and ready for years.

103
00:09:34,824 --> 00:09:37,201
You were in the Philippines
and came back to join the Marines

104
00:09:37,369 --> 00:09:39,912
because you wanted to be
the best and first in.

105
00:09:40,080 --> 00:09:42,957
When all this is over,

106
00:09:43,124 --> 00:09:45,167
let's say
a year from tonight,

107
00:09:45,335 --> 00:09:47,169
we'll sit down at
this table again

108
00:09:47,337 --> 00:09:49,088
for a welcome-home feast.

109
00:09:49,256 --> 00:09:51,423
To all of you,

110
00:09:51,591 --> 00:09:55,344
just get the job done

111
00:09:55,512 --> 00:09:57,304
and come home to us.

112
00:10:03,395 --> 00:10:05,562
- Salute.
- All: Salute.

113
00:10:06,898 --> 00:10:08,482
Oh!

114
00:10:08,650 --> 00:10:10,150
Those cannoli
for the guests!

115
00:10:10,318 --> 00:10:12,778
- He won't catch him.
- Yeah, you know it.

116
00:10:27,085 --> 00:10:29,962
Depot announcer:
Bus to Rochester leaves in 20 minutes.

117
00:10:30,130 --> 00:10:32,464
Please have
your tickets ready.

118
00:10:35,927 --> 00:10:37,553
Thanks for the ride, Dad.

119
00:10:37,721 --> 00:10:40,014
Must be a branch or something
stuck under the wheel.

120
00:10:40,181 --> 00:10:42,308
It's pulling to one side.

121
00:10:45,312 --> 00:10:46,812
Ech!

122
00:10:49,441 --> 00:10:50,899
Thanks for the ride.

123
00:10:51,067 --> 00:10:53,319
Oh yeah, sure.

124
00:10:53,486 --> 00:10:56,030
You hardly packed a thing.

125
00:10:56,197 --> 00:10:59,283
Final call for
the 6:45 to Newark.

126
00:10:59,451 --> 00:11:02,745
Marine's outfit includes everything
from bayonets to socks.

127
00:11:02,912 --> 00:11:05,331
I just wish I could have
brought my typewriter.

128
00:11:05,498 --> 00:11:07,291
Can't see why
you need that.

129
00:11:07,459 --> 00:11:12,296
Oh, I thought I might fight
by day and write by night, Dad.

130
00:11:13,298 --> 00:11:15,007
Can't see a thing.

131
00:11:15,175 --> 00:11:17,885
Oh, I hope I don't
need a new axle.

132
00:11:18,053 --> 00:11:19,762
No way I'll get one.

133
00:11:19,929 --> 00:11:21,722
Yeah.

134
00:11:21,890 --> 00:11:25,517
There's a war on.

135
00:11:25,685 --> 00:11:28,854
Everybody's got to
make sacrifices.

136
00:11:29,022 --> 00:11:31,732
Your mother said I was
a fool not to buy a Ford.

137
00:11:31,900 --> 00:11:33,692
Hope she's not right.

138
00:11:33,860 --> 00:11:38,030
Bus 45 and 73, direct service
to New York City,

139
00:11:38,198 --> 00:11:40,616
loading at depots 5 and 7.

140
00:11:44,704 --> 00:11:46,413
Well, that's it, Dad.

141
00:11:46,581 --> 00:11:49,291
I gotta go.

142
00:12:05,517 --> 00:12:07,017
Goodbye, son.

143
00:12:55,942 --> 00:12:58,152
Son-

144
00:13:00,238 --> 00:13:03,740
Eugene, I- I'm sorry.

145
00:13:28,683 --> 00:13:30,559
Murmur's still there.

146
00:13:38,735 --> 00:13:41,069
The boy's disappointed,
Mary Frank.

147
00:13:41,237 --> 00:13:43,864
I'm his mother.

148
00:14:18,441 --> 00:14:21,235
Churchill over radio:
... at Pearl Harbor

149
00:14:21,402 --> 00:14:23,987
in the Pacific Islands,
in the Philippines,

150
00:14:24,155 --> 00:14:26,698
in Malaya and
the Dutch East Indies,

151
00:14:26,866 --> 00:14:30,744
they must now know
the stakes are mortal.

152
00:14:30,912 --> 00:14:33,580
When we look at the resources
of the United States

153
00:14:33,748 --> 00:14:36,625
and the British Empire
compared to those of Japan,

154
00:14:36,793 --> 00:14:38,335
when we...

155
00:14:41,631 --> 00:14:45,133
...we have so long
valiantly withstood invasion.

156
00:14:55,520 --> 00:14:57,187
Gene, supper's ready.

157
00:15:03,194 --> 00:15:04,820
Deacon, you stay!

158
00:15:11,661 --> 00:15:15,664
You're 18, Eugene.
You don't need your father's permission.

159
00:15:15,832 --> 00:15:17,457
I can't go
against him, Sid.

160
00:15:17,625 --> 00:15:19,960
When do you leave?

161
00:15:20,128 --> 00:15:23,505
I'm on the 6:00 AM
train to Atlanta.

162
00:15:23,673 --> 00:15:25,173
Uh, here.

163
00:15:26,676 --> 00:15:28,510
Got you something.

164
00:15:28,678 --> 00:15:30,512
"Barrack-Room Ballads"
by Rudyard Kipling.

165
00:15:30,680 --> 00:15:32,180
Thank you, Gene,
but I didn't-

166
00:15:32,348 --> 00:15:34,141
It's just if you need something
to read on the train or...

167
00:15:34,309 --> 00:15:35,976
when you ship out.

168
00:15:38,271 --> 00:15:40,772
I wish we
were going together.

169
00:15:40,940 --> 00:15:43,108
Yeah, well, you take care
of yourself, greaser.

170
00:15:43,276 --> 00:15:45,902
Oh. You don't have to
worry about me.

171
00:16:16,517 --> 00:16:19,144
Get it while it's hot.

172
00:16:20,855 --> 00:16:22,481
Get your plates out.
Get your plates out.

173
00:16:22,649 --> 00:16:24,358
Today's the day, Marines!

174
00:16:24,525 --> 00:16:26,902
This is it.

175
00:16:27,070 --> 00:16:28,570
Today's the day!

176
00:16:28,738 --> 00:16:30,530
Get moving.
Don't wait for a seat.

177
00:16:30,698 --> 00:16:32,366
A lot of Marines to feed!
Move move move!

178
00:16:32,533 --> 00:16:36,036
Release it up! The quicker we eat,
the quicker we get topside,

179
00:16:36,204 --> 00:16:37,829
the quicker we get ashore.
Come on, Marines.

180
00:16:37,997 --> 00:16:39,915
Let's go go go!

181
00:16:40,083 --> 00:16:41,708
Oranges coming through.
Hey, come on, guys!

182
00:16:41,876 --> 00:16:43,752
I worked hard
for these oranges!

183
00:16:43,920 --> 00:16:47,714
- Dessert!
- Oh, hey!

184
00:16:47,882 --> 00:16:50,175
Make sure you save
one of those for me.

185
00:16:52,011 --> 00:16:55,806
I just want off this goddamn rust bucket!
I can't hear myself think.

186
00:16:55,973 --> 00:16:58,141
At least they give you a good
meaI before your send-off.

187
00:16:58,309 --> 00:16:59,935
Yeah, like
the electric chair.

188
00:17:00,103 --> 00:17:01,436
- But you, Runner?
- Yes?

189
00:17:01,604 --> 00:17:04,064
You have won a trip
to a tropical paradise.

190
00:17:04,232 --> 00:17:07,442
Oh, "Guadal Kenel"...
"Guada... "

191
00:17:07,610 --> 00:17:08,985
I still can't pronounce it.

192
00:17:09,153 --> 00:17:10,987
Yeah, well, whatever it's called,
you guys are lucky I'm here

193
00:17:11,155 --> 00:17:14,533
'cause I'm planning to take out
an entire Jap regiment all by myself!

194
00:17:15,952 --> 00:17:17,327
How you plan on doing that,
Sergeant York?

195
00:17:17,495 --> 00:17:20,038
Line 'em up, mow 'em down.
Real turkey shoot!

196
00:17:20,206 --> 00:17:22,207
I got a feeling it's gonna be a little
more complicated than that.

197
00:17:22,375 --> 00:17:23,667
No, it's gonna be
a turkey shoot.

198
00:17:23,835 --> 00:17:24,960
Can someone remind me
why we're here again?

199
00:17:25,128 --> 00:17:27,212
We're here to keep
the Japs out of Australia.

200
00:17:27,380 --> 00:17:31,258
- No, we're here to keep the Japs-
- Something about shipping lanes.

201
00:17:33,594 --> 00:17:36,722
No, guys, guys, guys, guys.
Professor Leckie, enlighten us.

202
00:17:36,889 --> 00:17:39,391
Wanna know why we're here?

203
00:17:39,559 --> 00:17:41,268
Mm-hmm.

204
00:17:44,105 --> 00:17:47,482
"Without a sign, his sword
the brave man draws

205
00:17:47,650 --> 00:17:51,111
and asks no omen
but his country's cause. "

206
00:17:55,241 --> 00:17:57,659
Man: Drink up!
Last chance for fresh water!

207
00:17:57,827 --> 00:18:00,495
- Give me back my smokes.
- Leckie, you gotta stop this.

208
00:18:00,663 --> 00:18:02,831
Let's go, boys.
Everybody on deck.

209
00:18:02,999 --> 00:18:04,124
Let's move!

210
00:18:04,292 --> 00:18:06,251
Topside right now!

211
00:18:06,419 --> 00:18:08,003
Wrap it up now.
Get your gear.

212
00:18:08,171 --> 00:18:12,466
Everybody topside for
a pre-landing briefing.

213
00:18:12,633 --> 00:18:15,802
Forget all the horseshit
you've heard about the Japs.

214
00:18:15,970 --> 00:18:18,972
They had their turn.
Now it's our turn.

215
00:18:19,140 --> 00:18:21,892
The treacherous bastards
may have started this war,

216
00:18:22,059 --> 00:18:24,644
but I promise you
we will finish it!

217
00:18:24,812 --> 00:18:26,313
All:
Yeah!

218
00:18:26,481 --> 00:18:29,316
Slant-eyed monkeys
want to use that shit island

219
00:18:29,484 --> 00:18:31,318
and its airfield
to kill us!

220
00:18:31,486 --> 00:18:33,445
We'll yank
the surviving Japs

221
00:18:33,613 --> 00:18:36,615
out of their shit-filled holes
by their yellow balls.

222
00:18:36,783 --> 00:18:39,576
They'll go round-eyed
when the first American plane

223
00:18:39,744 --> 00:18:41,161
lands on their airfield!

224
00:18:41,329 --> 00:18:43,246
Heads down in
the boats!

225
00:18:43,414 --> 00:18:48,168
Hit the beach. Keep moving till you
rendezvous point some primary objectives.

226
00:18:48,336 --> 00:18:51,046
When you see the Japs,
kill 'em all!

227
00:18:51,214 --> 00:18:52,547
All:
Yeah!

228
00:18:52,715 --> 00:18:54,633
- Go!
- Let's go get 'em!

229
00:18:58,429 --> 00:19:02,182
Move it, move it! Hands on the vertical,
feet on the horizontaI.

230
00:19:02,350 --> 00:19:04,476
Make it tight!
Come on!

231
00:19:10,525 --> 00:19:13,151
Hand on the vertical
down there!

232
00:19:40,179 --> 00:19:42,931
I could really use
a stiff one right now.

233
00:19:50,648 --> 00:19:54,067
Go get 'em, navy!

234
00:20:43,618 --> 00:20:46,077
Hoof it up to the beach!
Spread out!

235
00:20:46,245 --> 00:20:48,413
Man:
All right, let's go! Now!

236
00:20:48,581 --> 00:20:50,707
Go go go!

237
00:20:51,876 --> 00:20:54,502
Hit shore, hit shore!

238
00:21:07,308 --> 00:21:10,518
Man:
What took you so long?

239
00:21:10,686 --> 00:21:13,271
Welcome to Guadalcanal.

240
00:21:22,865 --> 00:21:25,325
Man:
Move it up the beach, Marines.

241
00:21:39,674 --> 00:21:44,469
Ah, now I've got you,
you son of a bitch.

242
00:21:48,474 --> 00:21:51,643
Hey, now what
do I do?

243
00:21:51,811 --> 00:21:53,895
I think the islanders use
a machete or a rock.

244
00:21:54,063 --> 00:21:56,982
Try using your head, Chuckler.
It's big and rocklike.

245
00:22:01,112 --> 00:22:02,988
Hey, Johnny Reb.

246
00:22:07,284 --> 00:22:10,078
Man: For what it's worth,
CorporaI Dobson says

247
00:22:10,246 --> 00:22:13,373
the Japs might have
poisoned the coconuts.

248
00:22:13,541 --> 00:22:15,542
They poisoned
a billion coconuts?

249
00:22:15,710 --> 00:22:18,378
Oh, shit!
My hand!

250
00:22:18,546 --> 00:22:20,171
- Excuse me.
- Son of a bitch!

251
00:22:20,339 --> 00:22:23,508
What happened?
Hey, Doc!

252
00:22:26,345 --> 00:22:30,974
Intelligence has it the Japs
moved back into the jungle.

253
00:22:31,142 --> 00:22:33,727
Clean the sand out of your weapons.
We move in three minutes.

254
00:22:35,604 --> 00:22:38,023
Let's go find some Japs.

255
00:25:34,200 --> 00:25:36,576
Oh!

256
00:25:36,744 --> 00:25:39,078
Gibson, you okay?

257
00:25:39,246 --> 00:25:40,747
Fuck.
I'm fine.

258
00:25:40,915 --> 00:25:42,415
You all right?

259
00:25:42,583 --> 00:25:44,000
Thanks.

260
00:26:55,489 --> 00:26:57,490
Goddamn bastards.

261
00:27:08,168 --> 00:27:10,753
Keep it moving.

262
00:28:06,810 --> 00:28:08,811
Man:
Take cover!

263
00:28:10,147 --> 00:28:12,982
- Go go!
- Man: Cease fire!

264
00:28:13,150 --> 00:28:16,903
Cease fire!
Cease fire!

265
00:28:38,425 --> 00:28:40,885
Man:
Son of a bitch.

266
00:28:44,598 --> 00:28:48,017
He went to take a piss.
Somebody opened up.

267
00:28:51,271 --> 00:28:52,939
Did he say the password?

268
00:28:53,107 --> 00:28:55,358
I don't know.

269
00:28:55,526 --> 00:28:57,777
Let's start walking.

270
00:28:57,945 --> 00:29:00,029
We're moving to
the top of the ridge.

271
00:29:03,450 --> 00:29:06,369
We're moving!

272
00:29:22,428 --> 00:29:25,638
Jameson:
Corrigan, Stone, get over here.

273
00:29:27,224 --> 00:29:29,058
We've been chasing geese, boys.

274
00:29:29,226 --> 00:29:31,769
That's the 5th Marines
on the airfield.

275
00:29:34,064 --> 00:29:36,149
Guess we took the airfield.

276
00:29:36,316 --> 00:29:38,818
Well, let's go home.

277
00:29:38,986 --> 00:29:43,906
LPs at 300yds,
no fires, two-hour watches.

278
00:29:45,075 --> 00:29:46,909
Let's assume the Japs are
watching right now,

279
00:29:47,077 --> 00:29:48,327
preparing to come at us.

280
00:29:48,495 --> 00:29:50,413
Assume?

281
00:29:50,581 --> 00:29:52,457
They are.

282
00:29:55,586 --> 00:29:58,379
Let the little yellow
bastards come.

283
00:30:02,551 --> 00:30:05,761
- Jesus Christ.
- Unbelievable.

284
00:30:09,266 --> 00:30:13,352
Lord, can you imagine being
on one of those ships?

285
00:30:13,520 --> 00:30:16,314
Looks like
the 4th of July.

286
00:30:16,482 --> 00:30:19,150
Guys?
30 cal ammo.

287
00:30:19,318 --> 00:30:20,943
Thank you,
thank you.

288
00:30:21,111 --> 00:30:24,822
- Hope it was a fucking Jap ship.
- It was- it's gotta be.

289
00:30:28,202 --> 00:30:30,203
That's AdmiraI Turner
blowing the whole Jap fleet

290
00:30:30,370 --> 00:30:32,371
to the bottom of
the channeI.

291
00:30:34,791 --> 00:30:37,376
I love your optimism.

292
00:30:39,296 --> 00:30:43,174
Oh, we're killin' 'em.

293
00:30:52,392 --> 00:30:56,020
Son of a bitch.
Where's the navy?

294
00:30:56,188 --> 00:30:57,730
Gone.

295
00:30:57,898 --> 00:31:02,443
We lost four cruisers.
Turner took everyone left

296
00:31:02,611 --> 00:31:04,153
and headed for open sea.

297
00:31:04,321 --> 00:31:06,030
We're it for now.

298
00:31:06,198 --> 00:31:09,367
The Elliot's with Turner?

299
00:31:09,535 --> 00:31:11,369
Zero crashed
into her midship.

300
00:31:11,537 --> 00:31:13,454
They couldn't control the fire
so they scuttled her.

301
00:31:13,622 --> 00:31:14,872
Bon voyage.

302
00:31:15,040 --> 00:31:17,333
Yeah, well, she went down
with half the battalion's ammo,

303
00:31:17,501 --> 00:31:19,043
most of our grub
and medical supplies

304
00:31:19,211 --> 00:31:21,837
and all our ass-wipe.

305
00:31:22,005 --> 00:31:24,465
Let's dig in, Marines!

306
00:31:24,633 --> 00:31:26,884
Start diggin' in.

307
00:31:34,893 --> 00:31:37,562
- Here.
- Hey, Phillips.

308
00:31:37,729 --> 00:31:38,813
Want some?

309
00:31:38,981 --> 00:31:41,315
It's Jap wine
left behind.

310
00:31:41,483 --> 00:31:44,318
It's made out of donkey piss
but it's not bad.

311
00:31:48,490 --> 00:31:52,243
The Jap navy is right over
the horizon, Phillips.

312
00:31:52,411 --> 00:31:54,453
If not now, then when?

313
00:31:54,621 --> 00:31:57,707
If you can't fight 'em drunk,
don't fight 'em at all.

314
00:32:05,882 --> 00:32:08,217
There you go.

315
00:32:08,385 --> 00:32:10,386
Man: Smoking lamps out!
Pass it down!

316
00:32:10,554 --> 00:32:12,513
- Smoking lamps out.
- Smoking lamps out!

317
00:32:12,681 --> 00:32:14,015
Smoking lamps out.

318
00:32:14,182 --> 00:32:16,475
Smoking lamps out!

319
00:32:41,126 --> 00:32:43,127
- What what?
- Stay low! Stay low!

320
00:32:43,295 --> 00:32:46,172
They're just trying to
spot our position so hold your fire.

321
00:32:46,340 --> 00:32:49,592
Hold your fire. They're just trying to
spot our position.

322
00:32:49,760 --> 00:32:51,385
Hold your fire.

323
00:32:51,553 --> 00:32:53,387
Keep your head down!

324
00:32:53,555 --> 00:32:56,223
They're just trying to
spot our position.

325
00:32:56,391 --> 00:32:58,142
Hold your fire.

326
00:33:25,253 --> 00:33:27,338
Corrigan:
Jesus Christ.

327
00:33:27,506 --> 00:33:29,423
There are thousands
of them.

328
00:33:29,591 --> 00:33:32,593
Man:
The whole damn Japanese Empire.

329
00:33:32,761 --> 00:33:33,928
You four over here.

330
00:33:34,096 --> 00:33:37,598
Coconut plantation
used a cove about 10 miles up.

331
00:33:37,766 --> 00:33:39,767
They're probably
headed there.

332
00:33:39,935 --> 00:33:43,396
Don't get too comfortable
in your holes.

333
00:34:09,923 --> 00:34:13,342
Set in along here.
Watch the opposite bank.

334
00:34:15,178 --> 00:34:17,388
Let's go-
put the hole right here.

335
00:34:24,104 --> 00:34:27,273
Man:
That's good. Keep it coming.

336
00:34:27,441 --> 00:34:29,775
Keep going.

337
00:34:31,695 --> 00:34:33,779
Corrigan:
"A" company made contact

338
00:34:33,947 --> 00:34:35,948
three miles east.

339
00:34:36,116 --> 00:34:39,410
Destroy any letters you have
with any dates...

340
00:34:39,578 --> 00:34:41,662
or addresses.

341
00:34:53,467 --> 00:34:56,177
Man: We got movement!
We got movement!

342
00:35:16,990 --> 00:35:18,866
Hey, look look look!

343
00:35:36,301 --> 00:35:38,552
Get back to your positions!

344
00:35:42,599 --> 00:35:44,809
- Man: Hanging!
- Fire!

345
00:36:00,158 --> 00:36:03,202
Fire!

346
00:36:03,370 --> 00:36:06,664
They're flanking us!
Stand back!

347
00:36:09,209 --> 00:36:11,544
Oh, we gotta move!
We gotta move!

348
00:36:11,711 --> 00:36:13,879
We gotta move over
to the left flank!

349
00:36:15,715 --> 00:36:17,883
Stand back!

350
00:36:23,765 --> 00:36:25,140
Come on!

351
00:36:25,308 --> 00:36:27,309
Come on,
just a few more yards!

352
00:36:52,002 --> 00:36:54,420
60, follow me!

353
00:36:55,422 --> 00:36:58,340
Move into
the forward position!

354
00:36:58,508 --> 00:37:02,052
Skipper!
Skipper, are you okay?

355
00:37:06,099 --> 00:37:07,349
God damn it.

356
00:37:07,517 --> 00:37:10,561
He's lost it. Come on!
Keep moving, keep moving!

357
00:37:10,729 --> 00:37:12,479
Get up here!

358
00:37:17,319 --> 00:37:20,571
- Oh, get up!
- Move!

359
00:37:41,718 --> 00:37:43,719
Left, hold up!
Get in there!

360
00:37:43,887 --> 00:37:45,179
There!

361
00:37:49,392 --> 00:37:52,353
We gotta move! We gotta move!

362
00:37:57,192 --> 00:38:00,527
- We gotta keep moving!
- Yeah! Yeah!

363
00:38:03,865 --> 00:38:06,075
Okay, let's go!
Let's go!

364
00:38:11,539 --> 00:38:13,207
Block them right, God damn it!
Block them right.

365
00:38:18,922 --> 00:38:20,965
Get those bastards runnin'!

366
00:38:21,132 --> 00:38:23,300
All right, ready?
Go go go!

367
00:38:27,430 --> 00:38:28,847
Clear!

368
00:38:36,272 --> 00:38:37,898
Ahhh!

369
00:39:14,811 --> 00:39:18,063
Man: Ammo resupplies
at the collection point.

370
00:39:18,231 --> 00:39:20,607
Smitty, resupply
down the line.

371
00:39:27,323 --> 00:39:29,908
Hey, ammo.

372
00:39:34,289 --> 00:39:35,914
Look at 'em all.

373
00:39:37,083 --> 00:39:39,209
I mean, we- we-
we chewed 'em up.

374
00:39:39,377 --> 00:39:41,420
They just kept
on coming.

375
00:39:43,673 --> 00:39:45,716
A real turkey shoot.

376
00:39:47,385 --> 00:39:50,304
Lucky.

377
00:39:57,604 --> 00:39:59,938
Corpsman,
we got a live one here.

378
00:40:05,945 --> 00:40:08,906
Stick a pin in him, Doc.

379
00:40:15,705 --> 00:40:19,208
- Shit!
- Fucking yellow monkeys!

380
00:40:21,920 --> 00:40:23,545
Shit.

381
00:40:35,892 --> 00:40:38,811
Man: Let's have some fun with
that son of a bitch!

382
00:40:41,481 --> 00:40:43,440
- Ahh!
- Get that fucker!

383
00:40:43,608 --> 00:40:46,026
- Whoo!
- Ahhh!

384
00:40:46,194 --> 00:40:50,489
Ahhh! Ahhh!

385
00:41:14,597 --> 00:41:17,307
Where's your Tojo now?

386
00:41:20,353 --> 00:41:23,021
Run, fucker!

387
00:41:31,406 --> 00:41:33,907
Marine:
Hey, what'd you do that for?

388
00:41:43,042 --> 00:41:45,836
Man:
Gather any papers for S-2.

389
00:41:46,004 --> 00:41:48,213
Jesus, the smell.

390
00:41:48,381 --> 00:41:50,257
They got Rivers,

391
00:41:50,425 --> 00:41:52,176
Morley, Abbott,

392
00:41:52,343 --> 00:41:54,928
Marton, McDougal.

393
00:41:55,096 --> 00:41:58,098
Stanley's blinded.

394
00:41:58,266 --> 00:42:01,226
Captain Jameson's
been relieved.

395
00:42:03,229 --> 00:42:05,647
I better get started on
the letters to their families.

396
00:42:05,815 --> 00:42:08,400
- Want a smoke?
- Thanks.

397
00:42:09,527 --> 00:42:12,196
- Oh, and Jeurgins?
- Sir?

398
00:42:12,363 --> 00:42:14,156
That's a good job
moving around last night.

399
00:42:14,324 --> 00:42:16,992
- Sir.
- Got you promoted to corporal.

400
00:42:20,997 --> 00:42:23,040
Well, we've lost Lew.

401
00:42:24,292 --> 00:42:27,252
"Yes, ma'am,
I am a corporal. "

402
00:42:35,136 --> 00:42:39,139
Oh, shit, these must be
the bastards who took Guam.

403
00:42:40,266 --> 00:42:42,142
I'll be damned.

404
00:43:42,453 --> 00:43:44,121
Robert's voice:
"Dear Vera,

405
00:43:44,289 --> 00:43:47,791
it seems a lifetime since
we met outside Saint Mary's.

406
00:43:47,959 --> 00:43:50,168
This great undertaking
for God and country

407
00:43:50,336 --> 00:43:52,296
has landed us in
a tropical paradise

408
00:43:52,463 --> 00:43:54,464
somewhere in what
Jack London refers to

409
00:43:54,632 --> 00:43:56,633
as 'those
terrible Solomons. '

410
00:43:56,801 --> 00:43:58,635
It is a Garden of Eden.

411
00:43:58,803 --> 00:44:02,139
The jungle holds both beauty
and terror in its depths,

412
00:44:02,307 --> 00:44:04,766
most terrible of which
is man.

413
00:44:04,934 --> 00:44:08,103
We have met the enemy and have learned
nothing more about him.

414
00:44:08,271 --> 00:44:12,274
I have, however, learned
some things about myself.

415
00:44:12,442 --> 00:44:14,359
There are things men
can do to one another

416
00:44:14,527 --> 00:44:17,446
that are sobering
to the souI.

417
00:44:17,613 --> 00:44:19,781
It is one thing to reconcile
these things with God,

418
00:44:19,949 --> 00:44:23,243
but another to square it
with yourself. "

419
00:44:29,042 --> 00:44:30,834
How long you been
sitting on this one?

420
00:44:31,002 --> 00:44:32,002
A week.

421
00:44:32,170 --> 00:44:34,671
Wanted to get it
really ripe for you.

422
00:44:34,839 --> 00:44:37,257
How kind.

423
00:44:39,510 --> 00:44:41,595
Next time don't let it
wait so long.

424
00:44:41,763 --> 00:44:43,305
Man:
This is what they call Guadalcanal.

425
00:44:43,473 --> 00:44:45,349
Runner:
Finally, our reinforcements.

426
00:44:47,018 --> 00:44:49,186
- 'Morning, boys.
- Shit, it's Chesty Puller.

427
00:44:49,354 --> 00:44:52,439
Look at the 7th, all
dressed up for Sunday school.

428
00:44:52,607 --> 00:44:54,066
Hey, you guys forget
to set the alarm?

429
00:44:54,233 --> 00:44:56,818
- Yeah, where you been?
- Up with the sun in Samoa

430
00:44:56,986 --> 00:44:59,821
- screwing your girlfriend.
Oh!

431
00:44:59,989 --> 00:45:01,907
Hoosier: I have a girlfriend?
Lucky me.

432
00:45:02,075 --> 00:45:03,450
Filthy, just like you are.

433
00:45:03,618 --> 00:45:05,660
Hey, Colonel Puller,
where you headin'?

434
00:45:05,828 --> 00:45:07,996
Tokyo.
Care to join us?

435
00:45:08,164 --> 00:45:10,916
I don't know. It's a ways, sir.
Write us when you get there.

436
00:45:11,084 --> 00:45:13,210
- I will!
- You guys step aside.

437
00:45:13,378 --> 00:45:16,505
- The real Marines are here now.
- And have been here for some time.

438
00:45:16,672 --> 00:45:17,714
All:
Yeah!

439
00:45:17,882 --> 00:45:21,718
- 7th Marines!
- Welcome to the war, fellas.

440
00:45:24,889 --> 00:45:26,139
You seen those guys?

441
00:45:26,307 --> 00:45:28,100
They look like they been
through the wringer.

442
00:45:28,267 --> 00:45:31,311
That's one way of
putting it.

443
00:45:36,067 --> 00:45:38,151
"Best wishes, Father.

444
00:45:38,319 --> 00:45:40,195
Your mother would
like to know

445
00:45:40,363 --> 00:45:43,073
if you want us to send you
your dress-blue uniform. "

446
00:45:44,826 --> 00:45:46,993
Must think we get a lot
of fancy-dress balls around here.

447
00:45:47,161 --> 00:45:48,995
Hey, if we have one,
can I be your date?

448
00:45:49,163 --> 00:45:51,081
You are ugly.
I want Hoosier.

449
00:45:51,249 --> 00:45:53,041
Take a number.

450
00:45:53,209 --> 00:45:54,918
Hey, Johnny Reb,
it's your turn.

451
00:45:55,086 --> 00:45:58,964
This one here is from
a buddy of mine back in Mobile.

452
00:46:01,050 --> 00:46:04,719
"Dear Sid, I hope this gets
to you before your birthday.

453
00:46:04,887 --> 00:46:06,972
You wouldn't recognize
home now.

454
00:46:07,140 --> 00:46:10,058
Thousands of workers are
pouring into the shipyards.

455
00:46:10,226 --> 00:46:12,394
The Gulf is blacked out now
because of the U-boats.

456
00:46:12,562 --> 00:46:14,312
Your sister and I went-"

457
00:46:14,480 --> 00:46:16,189
- Sister?
- All: Aww!

458
00:46:16,357 --> 00:46:20,569
"Your sister and I went down to see
the wreckage of a ship they sunk.

459
00:46:20,736 --> 00:46:22,571
She's worried about you,
of course, but I told her

460
00:46:22,738 --> 00:46:26,324
a greaser like yourself is
too crafty for the Japs.

461
00:46:26,492 --> 00:46:30,162
Truth is, you're
the lucky one, Sid.

462
00:46:30,329 --> 00:46:32,247
You'll never have that
nagging thought

463
00:46:32,415 --> 00:46:34,583
that you let your family,
your friends,

464
00:46:34,750 --> 00:46:35,876
and your country down.

465
00:46:36,043 --> 00:46:38,545
Because that's what
I'm afraid of. "

466
00:46:38,713 --> 00:46:42,132
Gear up!
Gun patrol, on your feet.

467
00:46:42,300 --> 00:46:44,050
- Let's go.
- Men: All right, that's it.

468
00:46:44,218 --> 00:46:46,470
Hey, finish it.

469
00:46:48,139 --> 00:46:50,557
"So I'll leave you knowing
that like the poem says,

470
00:46:50,725 --> 00:46:52,392
'You're a better man
than I am, Gunga Din. '

471
00:46:52,560 --> 00:46:56,897
Your humble and obedient
servant, Eugene Sledge. "

472
00:47:04,947 --> 00:47:06,948
So your friend wants
to be a Marine?

473
00:47:07,116 --> 00:47:11,036
Yeah, but his father won't let him.
He's got a heart murmur.

474
00:47:11,204 --> 00:47:14,247
Ah, a murmuring heart.

475
00:47:14,415 --> 00:47:16,333
Hey, when's your
birthday, Sid?

476
00:47:16,501 --> 00:47:18,335
It was a couple
weeks ago.

477
00:47:18,503 --> 00:47:19,920
How old are you?

478
00:47:20,087 --> 00:47:21,421
18.

479
00:47:21,589 --> 00:47:23,381
Happy birthday.

