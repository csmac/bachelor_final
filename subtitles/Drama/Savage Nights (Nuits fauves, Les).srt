1
00:01:38,862 --> 00:01:39,331
Thank you.

2
00:02:25,581 --> 00:02:30,242
FOUR NIGHTS OF A DREAMER

3
00:03:48,252 --> 00:03:50,178
FIRST NIGHT

4
00:05:15,793 --> 00:05:17,150
Leave me alone!

5
00:05:32,548 --> 00:05:33,298
Don't touch me.

6
00:05:33,298 --> 00:05:34,731
Don't stay there.

7
00:05:35,645 --> 00:05:36,884
Quick!

8
00:06:03,783 --> 00:06:04,691
What's wrong?

9
00:06:18,180 --> 00:06:20,257
Give me your hand.
I'll take you home.

10
00:06:39,573 --> 00:06:40,695
I'll be there, same place.

11
00:06:40,695 --> 00:06:43,293
under the statue,
tomorrow, same time.

12
00:06:43,833 --> 00:06:46,271
I'll have to.
I could not help it.

13
00:07:03,096 --> 00:07:05,330
SECOND NIGHT

14
00:08:13,429 --> 00:08:15,018
Do you know why I came?

15
00:08:15,867 --> 00:08:16,869
No...

16
00:08:17,357 --> 00:08:19,554
We have to be
very clever...

17
00:08:20,402 --> 00:08:22,824
I've never done anything
as clever.

18
00:08:34,905 --> 00:08:38,042
I don't know you. Sit down
and tell me your story.

19
00:08:38,775 --> 00:08:41,685
My story?
I have no story.

20
00:08:42,132 --> 00:08:44,246
I see no one,
I speak to no one.

21
00:08:45,749 --> 00:08:48,920
STORY OF JACQUES

22
00:08:50,757 --> 00:08:52,478
I live at 6 rue
Antoine-Dubois.

23
00:08:54,270 --> 00:08:56,513
A loft on the
third floor.

24
00:12:06,026 --> 00:12:08,046
How many times
I was in love!

25
00:12:08,322 --> 00:12:09,944
How is that? With whom?

26
00:12:10,139 --> 00:12:13,341
With no one, an ideal,
the woman in my dream.

27
00:12:13,731 --> 00:12:14,571
That's stupid.

28
00:12:15,826 --> 00:12:18,503
Yes. And God sends
an angel to show me...

29
00:12:19,485 --> 00:12:22,269
To reconcile me
with myself.

30
00:13:34,920 --> 00:13:37,308
In the park we walk
together, hand in hand,

31
00:13:37,308 --> 00:13:40,794
hoping, losing hope...

32
00:13:41,514 --> 00:13:43,438
The castle is strange.

33
00:13:45,751 --> 00:13:48,276
She's there half the year
with her old husband,

34
00:13:48,302 --> 00:13:51,439
Silent, boring,
who scares us a little.

35
00:13:51,842 --> 00:13:54,930
But our love is pure
and innocent...

36
00:14:18,818 --> 00:14:20,993
Is pure and innocent...

37
00:14:27,001 --> 00:14:28,537
Pure and innocent...

38
00:14:45,212 --> 00:14:47,724
I find her again in Venice.

39
00:14:48,187 --> 00:14:51,180
Isn't it her, in this palace
of lights and music?

40
00:14:51,981 --> 00:14:54,065
It is. She is dancing...

41
00:15:07,888 --> 00:15:08,937
She is dancing...

42
00:15:17,004 --> 00:15:20,027
She sees me. We leap onto
the balcony...

43
00:15:28,805 --> 00:15:31,666
She tears her mask off
and whispers in my ear:

44
00:15:31,939 --> 00:15:33,279
"Jacques, I am free!" ...

45
00:15:34,076 --> 00:15:36,120
Ended are the sorrows
of the ancestral castle,

46
00:15:36,301 --> 00:15:38,499
of the old garden,
of the old husband...

47
00:15:55,824 --> 00:15:57,692
In the park we walk
together, hand in hand,

48
00:15:57,692 --> 00:15:59,761
hoping, losing hope...

49
00:16:11,469 --> 00:16:13,149
The castle is strange.

50
00:16:18,843 --> 00:16:21,160
She's there half the year
with her old husband,

51
00:16:21,160 --> 00:16:23,648
Silent, boring,
who scares us a little.

52
00:16:25,415 --> 00:16:27,901
But our love
is pure and innocent...

53
00:16:30,871 --> 00:16:32,171
I find her again in Venice.

54
00:17:57,700 --> 00:17:58,961
Do you recognize me?

55
00:18:00,442 --> 00:18:01,265
Yes...

56
00:18:03,365 --> 00:18:04,505
The Art School.

57
00:18:06,319 --> 00:18:07,669
Of course!

58
00:18:09,086 --> 00:18:10,619
Craftsmanship is dead!

59
00:18:16,439 --> 00:18:18,496
Chardin's <i>brioche</i>,

60
00:18:18,681 --> 00:18:20,103
Manet's <i>pivoines</i>,

61
00:18:20,263 --> 00:18:22,817
Van Gogh's <i>chair</i> and
<i>godillots</i>, it's over!

62
00:18:41,992 --> 00:18:44,056
I am for a mature art,

63
00:18:44,244 --> 00:18:46,322
open to its time,

64
00:18:46,583 --> 00:18:49,201
which does not burst in
contact with Nature,

65
00:18:49,481 --> 00:18:52,229
but simply is a meeting of
the painter and a concept.

66
00:18:52,715 --> 00:18:54,008
Do you understand?

67
00:19:16,424 --> 00:19:17,969
No thank you, really.

68
00:19:22,994 --> 00:19:26,347
What's crucial is not the
object, not the painter,

69
00:19:26,619 --> 00:19:27,898
but the gesture which lifts

70
00:19:26,619 --> 00:19:29,201
the presence from
the object,

71
00:19:29,206 --> 00:19:31,686
which is suspended in a
space which delimits it,

72
00:19:31,802 --> 00:19:33,947
in fact, supports it.
Do you understand?

73
00:19:36,443 --> 00:19:38,400
Not the object there,
not the painter there,

74
00:19:38,468 --> 00:19:40,673
but the object and the
painter which are not there.

75
00:19:40,745 --> 00:19:42,363
Their visible
disappearance

76
00:19:42,366 --> 00:19:43,703
makes the canvas,

77
00:19:43,733 --> 00:19:45,143
so sensually structured

78
00:19:45,144 --> 00:19:46,437
as to form
a whole light source

79
00:19:46,613 --> 00:19:48,317
which is specifically solid.
Do you understand?

80
00:19:52,777 --> 00:19:54,088
Look at these spots!

81
00:20:11,015 --> 00:20:12,005
Did you make them?

82
00:20:14,865 --> 00:20:16,089
Yes. And the smaller
they are,

83
00:20:16,181 --> 00:20:18,519
the larger the world
they define by suggestion.

84
00:20:18,896 --> 00:20:20,761
One does not see
the spots,

85
00:20:21,025 --> 00:20:23,487
one sees everything that
is not there. Perforce.

86
00:20:23,919 --> 00:20:26,090
Thus, we're obliged,
we painters,

87
00:20:26,385 --> 00:20:27,710
to re-think fundamentally

88
00:20:27,801 --> 00:20:28,996
the problematics of art,

89
00:20:29,263 --> 00:20:33,598
effecting, of necessity,
the process of its accretion.

90
00:20:37,842 --> 00:20:39,257
You'll excuse me.

91
00:20:50,542 --> 00:20:51,868
See you soon.

92
00:20:55,401 --> 00:20:56,385
For sure?

93
00:20:57,541 --> 00:20:58,571
For sure.

94
00:21:38,756 --> 00:21:40,783
In the park we walk
together, hand in hand,

95
00:21:40,881 --> 00:21:42,562
hoping, losing hope...

96
00:21:45,167 --> 00:21:46,954
The castle is strange.

97
00:21:49,464 --> 00:21:51,779
She's there half the year
with her old husband,

98
00:21:51,780 --> 00:21:54,192
Silent, boring,
who scares us a little.

99
00:21:55,196 --> 00:21:57,727
But our love is pure
and innocent...

100
00:21:59,203 --> 00:22:01,089
I find her again in Venice.

101
00:22:02,461 --> 00:22:05,345
Isn't it her, in this palace
of lights and music?

102
00:22:05,955 --> 00:22:08,573
It is. She is dancing...

103
00:22:10,468 --> 00:22:11,832
Is that your life?

104
00:22:12,133 --> 00:22:13,366
Yes.

105
00:22:13,542 --> 00:22:15,557
But yesterday, you
came straight to me.

106
00:22:16,472 --> 00:22:18,001
I was happy yesterday.

107
00:22:18,184 --> 00:22:20,166
I had been in the country.

108
00:22:20,450 --> 00:22:22,512
You are not happy
every day?

109
00:22:22,596 --> 00:22:24,105
Some days are horrible,

110
00:22:24,106 --> 00:22:26,466
when it seems I cannot
go on living.

111
00:22:27,249 --> 00:22:29,866
I see people in
a turmoil and a rage,

112
00:22:30,141 --> 00:22:32,344
while my dream
becomes flat: a bore,

113
00:22:33,164 --> 00:22:34,568
and realize that all that

114
00:22:34,661 --> 00:22:36,055
was so dear to me
never existed.

115
00:22:36,775 --> 00:22:37,977
There is nothing to regret.

116
00:22:38,246 --> 00:22:40,068
What I have lost
is a perfect zero!

117
00:22:40,812 --> 00:22:43,933
Stop it. I won't pity you;
give me a piece of advice.

118
00:22:44,726 --> 00:22:46,129
STORY OF MARTHE

119
00:22:46,130 --> 00:22:48,096
STORY OF MARTHE
I live with my mother.

120
00:22:50,322 --> 00:22:53,711
We barely exist on
my father's alimony.

121
00:23:07,008 --> 00:23:10,790
Every year we rent out
a room next to mine.

122
00:23:28,255 --> 00:23:30,269
How is he? Nice?
Pleasant?

123
00:24:18,545 --> 00:24:19,754
What is that?

124
00:24:40,436 --> 00:24:42,134
What is that?

125
00:24:52,572 --> 00:24:55,151
Books lent to us
by the boarder.

126
00:24:57,238 --> 00:24:58,890
Confess that you want me
to marry.

127
00:24:59,157 --> 00:25:00,399
Me, why?

128
00:25:00,559 --> 00:25:02,187
Because you always
rent to a man.

129
00:25:02,772 --> 00:25:04,744
Be careful. With or without
a pretext of marriage,

130
00:25:04,745 --> 00:25:06,663
men know how to
manoeuvre girls.

131
00:25:06,786 --> 00:25:08,205
When they've
had enought...

132
00:25:08,472 --> 00:25:11,533
I know, mother, I know...
then drop them.

133
00:25:43,037 --> 00:25:44,504
Are you the girl?

134
00:25:45,364 --> 00:25:47,133
Let me go down.
What are you doing?

135
00:25:47,207 --> 00:25:49,476
Do you wan to go
to the movies tonight?

136
00:25:50,195 --> 00:25:52,692
You mustn't tell. We'll
go where you want...

137
00:25:56,322 --> 00:25:57,078
Yes?

138
00:26:08,822 --> 00:26:10,582
It's not nice.

139
00:26:19,238 --> 00:26:20,519
It's tough!

140
00:26:34,189 --> 00:26:37,392
THE BONDS OF LOVE

141
00:26:44,878 --> 00:26:46,395
Who got you these seats?

142
00:26:46,535 --> 00:26:47,496
Guess.

143
00:26:47,505 --> 00:26:49,411
Him? ... Is he going too?

144
00:26:49,591 --> 00:26:50,328
No.

145
00:26:50,615 --> 00:26:52,030
Let's not go!
It's a nuisance.

146
00:26:52,600 --> 00:26:54,622
He deprived himself
for us.

147
00:26:55,454 --> 00:26:57,041
A film premiere is
a bore.

148
00:26:57,310 --> 00:26:58,532
Not always.

149
00:28:21,997 --> 00:28:23,882
We have fallen into a trap.

150
00:30:12,981 --> 00:30:14,882
Let's go. Come on!

151
00:30:16,267 --> 00:30:17,882
We can't!

152
00:30:30,763 --> 00:30:31,887
A trap?

153
00:30:32,177 --> 00:30:34,054
A trick! He wanted to
punish me...

154
00:30:34,601 --> 00:30:35,430
Who did?

155
00:30:36,171 --> 00:30:38,304
The boarder who got us
the seats.

156
00:30:39,331 --> 00:30:41,133
Punish for what?

157
00:30:41,376 --> 00:30:43,877
Not going to the movies
with him.

158
00:30:44,244 --> 00:30:45,224
Do you think so?

159
00:30:45,244 --> 00:30:47,503
He confessed it to
me later.

160
00:31:17,315 --> 00:31:19,129
I thought we'd see him
more often.

161
00:31:27,527 --> 00:31:29,684
But, mother, I never
saw him.

162
00:36:38,251 --> 00:36:39,920
What, he's moving, and
you told me nothing?

163
00:36:40,108 --> 00:36:41,260
I thought you knew.

164
00:36:41,345 --> 00:36:42,194
Me?

165
00:36:42,645 --> 00:36:43,895
Yes, I forgot.

166
00:37:29,158 --> 00:37:30,450
Take me with you!

167
00:37:33,811 --> 00:37:34,613
But...

168
00:37:34,614 --> 00:37:36,308
Take me with you!
You must take me with you.

169
00:37:52,212 --> 00:37:53,052
Are you going?

170
00:37:53,053 --> 00:37:54,072
Yes.

171
00:37:54,284 --> 00:37:55,091
Far away?

172
00:37:55,191 --> 00:37:56,396
To Yale, in America.

173
00:37:56,472 --> 00:37:57,964
I have a fellowship.
I'm going.

174
00:37:58,064 --> 00:38:00,526
I am bored here.
Let me follow you.

175
00:38:03,967 --> 00:38:05,314
It's impossible!

176
00:38:32,699 --> 00:38:33,787
Marthe!

177
00:39:27,875 --> 00:39:29,601
Marthe, my dear.

178
00:39:48,915 --> 00:39:51,081
How would we live?

179
00:39:53,202 --> 00:39:54,638
I don't know.

180
00:39:54,931 --> 00:39:57,620
But I swear to you,
when I return,

181
00:39:58,080 --> 00:40:00,462
if I am fit to be married...

182
00:40:12,512 --> 00:40:14,245
...and if you still love me...

183
00:40:17,023 --> 00:40:17,859
Hey!

184
00:40:41,382 --> 00:40:43,620
In a year from now...
here...

185
00:40:43,803 --> 00:40:46,238
day for day... same time.

186
00:41:34,191 --> 00:41:35,806
It has been a year...

187
00:41:35,890 --> 00:41:37,725
He came back three days
ago, but not to me.

188
00:41:38,588 --> 00:41:39,945
How do you know?

189
00:41:41,557 --> 00:41:43,475
I know. It's the third day.

190
00:42:05,546 --> 00:42:06,874
If I went and talked
to him?

191
00:42:11,356 --> 00:42:12,631
Is it possible?

192
00:42:13,181 --> 00:42:15,336
Not really. But write him
a letter.

193
00:42:19,589 --> 00:42:21,800
I can't, and I don't
want to force him.

194
00:42:22,828 --> 00:42:23,992
You have the right to.

195
00:42:24,260 --> 00:42:25,665
He tied himself with
a promise

196
00:42:25,666 --> 00:42:26,617
and left you
entirely free,

197
00:42:25,890 --> 00:42:28,086
even to refuse.

198
00:42:30,516 --> 00:42:32,913
How would you write
the letter?

199
00:42:33,280 --> 00:42:35,069
Tell him... I don't know...

200
00:42:35,329 --> 00:42:37,789
that for a year you lived
with hope...

201
00:42:37,955 --> 00:42:39,502
that you were happy...

202
00:42:39,641 --> 00:42:41,444
but that now the doubt
is unbearable.

203
00:42:42,441 --> 00:42:43,493
Tell him:

204
00:42:43,945 --> 00:42:46,556
"I don't accuse you, if
you have forgotten me."

205
00:42:47,223 --> 00:42:48,443
Why the formal "vous"?

206
00:42:50,019 --> 00:42:52,337
"I don't accuse you, if
you have forgotten me."

207
00:42:52,795 --> 00:42:54,057
"But I know are incapable

208
00:42:54,139 --> 00:42:55,322
of causing such pain

209
00:42:55,363 --> 00:42:58,362
"to the one who loved you
so, and who loves you."

210
00:43:01,300 --> 00:43:02,761
It's absolutely right.

211
00:43:04,088 --> 00:43:05,741
Thank you, thank you.

212
00:43:33,329 --> 00:43:35,268
It had been agreed that,
as soon as he'd arrive,

213
00:43:35,269 --> 00:43:37,011
he'd let
friends of mine know.

214
00:43:37,078 --> 00:43:39,381
I can't go there. Be kind
deliver my letter to them,

215
00:43:39,382 --> 00:43:41,104
they'll forward it.

216
00:43:41,285 --> 00:43:42,360
If there is an answer,

217
00:43:42,361 --> 00:43:44,662
you bring it to me
tomorrow night.

218
00:43:46,129 --> 00:43:48,175
But the letter,
you must write it...

219
00:43:50,291 --> 00:43:52,232
It can't be done until
after tomorrow.

220
00:43:52,532 --> 00:43:54,349
The letter... is there.

221
00:43:56,742 --> 00:43:58,442
With the adress
for delivery.

222
00:44:08,020 --> 00:44:10,057
Until tomorrow!
If it rains...

223
00:44:10,832 --> 00:44:12,054
But it won't rain.

224
00:46:15,309 --> 00:46:18,537
Marthe! Marthe! Marthe!

225
00:48:51,927 --> 00:48:53,997
THIRD NIGHT

226
00:49:04,127 --> 00:49:06,790
It's done! I delivered
your letter!

227
00:49:09,172 --> 00:49:10,777
Do you know why I'm
so glad?

228
00:49:11,362 --> 00:49:12,994
Why I love you so
much today?

229
00:49:14,752 --> 00:49:17,636
I love you because you're
not in love with me.

230
00:49:21,681 --> 00:49:23,432
You're so sweet!

231
00:49:24,591 --> 00:49:25,660
When I am married,

232
00:49:25,911 --> 00:49:27,194
we'll be more than
brother and sister.

233
00:49:27,669 --> 00:49:30,012
I'll love you almost
as much as him.

234
00:49:30,095 --> 00:49:32,848
You say that because you
are nervous and afraid,

235
00:49:32,849 --> 00:49:34,709
you believe he will
not come.

236
00:49:34,800 --> 00:49:36,567
If I believed that,
I would cry.

237
00:50:06,309 --> 00:50:08,167
You are afraid!

238
00:50:15,680 --> 00:50:17,635
We'll welcome
him together.

239
00:50:17,728 --> 00:50:19,824
I want him to see how
much we love each other.

240
00:50:21,143 --> 00:50:22,642
How much we love
each other!

241
00:50:26,710 --> 00:50:29,219
You know what happened
to me today?

242
00:50:30,323 --> 00:50:32,349
First I did what you had
asked me.

243
00:50:33,498 --> 00:50:36,014
At home, I worked:
it was going well.

244
00:50:36,656 --> 00:50:38,446
Then I threw myself on
the bed.

245
00:50:39,936 --> 00:50:42,308
Suddenly, time stopped.
No more time!

246
00:50:42,809 --> 00:50:45,383
And only one feeling,
one desire...

247
00:50:46,186 --> 00:50:47,506
And then
a musical phrase,

248
00:50:47,550 --> 00:50:49,513
heard long ago
and forgotten.

249
00:50:49,591 --> 00:50:51,316
Soft, marvellous...

250
00:50:52,605 --> 00:50:54,679
I felt it had been
waiting all my life

251
00:50:55,534 --> 00:50:56,775
and that only now...

252
00:50:57,651 --> 00:50:58,898
How can that be?

253
00:54:37,246 --> 00:54:38,716
Eleven!

254
00:54:39,446 --> 00:54:40,533
Yes.

255
00:54:45,372 --> 00:54:46,847
Don't cry.

256
00:54:46,848 --> 00:54:48,915
If he couldn't come and
he answers in writing,

257
00:54:49,117 --> 00:54:50,858
his letter won't arrive
until tomorrow.

258
00:54:50,953 --> 00:54:53,150
Tomorrow I'll go to your
friends and let you know.

259
00:54:55,673 --> 00:54:56,943
You don't mind?

260
00:54:57,002 --> 00:54:59,324
There are still a
thousand possibilities.

261
00:55:02,438 --> 00:55:04,552
It's true. I hadn't thought
of that.

262
00:55:04,732 --> 00:55:06,432
What a little girl you are!

263
00:55:20,773 --> 00:55:22,574
It's because of you
that I cry.

264
00:55:23,940 --> 00:55:25,456
Why isn't he you?

265
00:55:25,643 --> 00:55:27,232
Not made like you?

266
00:55:27,330 --> 00:55:29,318
You're more clever and
more kind than he,

267
00:55:29,320 --> 00:55:31,025
but it is him I love.

268
00:55:31,054 --> 00:55:33,877
Of course; I don't yet know
him completely.

269
00:55:34,772 --> 00:55:36,106
How he looked at me

270
00:55:36,244 --> 00:55:37,734
when I came to his room
with my bag!

271
00:55:38,198 --> 00:55:39,738
You love him more
than anything.

272
00:55:39,842 --> 00:55:41,126
More than yourself.

273
00:55:41,596 --> 00:55:42,919
Maybe.

274
00:55:43,121 --> 00:55:45,725
But why does a man always
have something to hide?

275
00:55:46,457 --> 00:55:48,039
Why not just say what is
in one's heart,

276
00:55:48,222 --> 00:55:49,291
as if afraid of
wasting feelings

277
00:55:49,292 --> 00:55:50,775
by revealing them
too quickly.

278
00:55:51,158 --> 00:55:52,622
There might be lots
of reasons.

279
00:56:00,770 --> 00:56:02,728
You're not like the others

280
00:56:03,241 --> 00:56:05,754
I feel that you give up
something for me.

281
00:56:06,019 --> 00:56:07,062
How can I tell you?

282
00:56:07,020 --> 00:56:09,329
It's silly. I don't know.

283
00:56:09,404 --> 00:56:11,841
But I'd so much want
you to be happy!

284
00:56:12,869 --> 00:56:15,821
What you've told me about
your dreamer is false.

285
00:56:16,261 --> 00:56:18,863
You're very different from
what you pretend to be.

286
00:56:19,879 --> 00:56:22,131
If one day you fall in love,
she will be happy.

287
00:56:22,232 --> 00:56:23,860
I know it!

288
00:56:27,596 --> 00:56:29,395
He will not come today.

289
00:56:29,594 --> 00:56:31,268
It is too late already.

290
00:56:34,913 --> 00:56:36,619
He will come tomorrow.

291
00:56:57,546 --> 00:56:59,805
Now we are bound
together for ever.

292
00:57:21,742 --> 00:57:24,459
Marthe! Marthe!

293
00:58:06,151 --> 00:58:07,713
Tomorrow maybe.

294
00:58:10,274 --> 00:58:11,442
Maybe.

295
01:00:58,130 --> 01:01:00,308
FOURTH NIGHT

296
01:01:25,286 --> 01:01:26,516
Still nothing?

297
01:01:26,758 --> 01:01:28,484
He hasn't come
to see you?

298
01:01:32,587 --> 01:01:35,128
He can do what he wants,
I don't give a damn!

299
01:03:04,714 --> 01:03:06,519
Don't stay there. Come!

300
01:03:08,538 --> 01:03:12,035
Don't tell me he'll come.
He will not.

301
01:03:12,150 --> 01:03:14,047
And why? Why?

302
01:03:14,163 --> 01:03:16,606
Was there anything
in my letter?

303
01:03:18,143 --> 01:03:21,275
And to think I was
the one who went to him!

304
01:03:23,540 --> 01:03:26,127
No, it's not possible.
We are mistaken.

305
01:03:26,354 --> 01:03:28,295
He doesn't know yet. He
hasn't received the letter.

306
01:03:29,285 --> 01:03:31,389
Or maybe someone spoke
badly of me.

307
01:03:33,927 --> 01:03:35,588
Anyway, not
another letter.

308
01:03:35,589 --> 01:03:38,579
Enough. I don't love him
anymore. I'll forget him.

309
01:03:40,188 --> 01:03:41,626
You wouldn't have
behaved like that

310
01:03:41,627 --> 01:03:43,728
with the one who had
come to you.

311
01:03:44,234 --> 01:03:46,566
You'd think her unable
to defend herself

312
01:03:46,567 --> 01:03:48,327
against her love
for you.

313
01:03:49,011 --> 01:03:51,867
She is innocent.
She didn't do anything!

314
01:04:05,600 --> 01:04:06,810
What's the matter?

315
01:04:15,575 --> 01:04:17,347
I love you,
that's the matter.

316
01:07:07,445 --> 01:07:10,514
I knew it, but I thought
it wasn't serious.

317
01:07:10,515 --> 01:07:12,505
I can't help it.
It's not my fault.

318
01:07:12,506 --> 01:07:14,388
Something tells me
I'm right.

319
01:07:14,389 --> 01:07:16,540
Because I could never
hurt you.

320
01:07:16,541 --> 01:07:18,451
I'll always be
your friend.

321
01:07:24,044 --> 01:07:25,140
Sit down!

322
01:07:27,622 --> 01:07:29,280
No. I'll go.

323
01:07:29,499 --> 01:07:31,408
You were the first to talk
of love.

324
01:07:31,516 --> 01:07:32,931
You're the guilty one.

325
01:07:35,294 --> 01:07:36,534
Stay!

326
01:07:36,800 --> 01:07:38,020
To say what?

327
01:07:38,270 --> 01:07:39,434
Speak!

328
01:07:39,730 --> 01:07:41,288
...I believed you didn't still
love him?

329
01:07:42,260 --> 01:07:44,765
Even if you'd gone
on loving this man,

330
01:07:45,050 --> 01:07:47,587
I'd have loved
you secretly.

331
01:07:49,809 --> 01:07:51,851
What have you done
to me?

332
01:07:55,005 --> 01:07:56,473
Let's go!

333
01:08:28,583 --> 01:08:31,242
Listen, since he
abandoned me, forgot me,

334
01:08:31,315 --> 01:08:34,112
although I still love him.
I won't cheat you...

335
01:08:36,109 --> 01:08:37,406
Listen to me!

336
01:08:46,143 --> 01:08:48,661
How could I have been
so blind and stupid?

337
01:08:50,918 --> 01:08:52,566
Let's part.
I only torment you.

338
01:08:53,238 --> 01:08:54,462
I want to leave.

339
01:08:54,645 --> 01:08:56,772
One second! You can wait
one second!

340
01:08:57,455 --> 01:08:58,767
Wait for what?

341
01:08:59,044 --> 01:09:01,557
I love him. But it'll pass.
It must pass.

342
01:09:01,812 --> 01:09:02,978
Tonight will end it...

343
01:09:08,345 --> 01:09:10,604
I hate him, he doesn't give
a damn about me.

344
01:09:11,083 --> 01:09:13,205
You love me.
He never did.

345
01:09:13,206 --> 01:09:15,139
I love you as
you love me.

346
01:09:15,154 --> 01:09:17,030
I had told you that before.
You remember?

347
01:09:17,492 --> 01:09:20,614
If I love you, it's because
you are superior to him.

348
01:09:34,585 --> 01:09:35,848
Let's go in!

349
01:09:46,857 --> 01:09:47,910
I am not frivolous.

350
01:09:50,428 --> 01:09:52,311
For a whole year
I loved him

351
01:09:52,342 --> 01:09:54,366
and I never
was unfaithful.

352
01:10:01,906 --> 01:10:03,760
He didn't respect it...

353
01:10:04,662 --> 01:10:06,333
I don't love him

354
01:10:06,423 --> 01:10:08,913
because I can only
love what I admire...

355
01:10:11,314 --> 01:10:13,676
It's better than
if I'd been wrong.

356
01:10:13,965 --> 01:10:16,122
and learnt later
who he really is.

357
01:10:16,402 --> 01:10:19,280
Maybe my love was
only an illusion,

358
01:10:19,386 --> 01:10:20,964
I was under my
mother's thumb,

359
01:10:20,965 --> 01:10:23,726
and I wanted to get away
from home.

360
01:10:26,054 --> 01:10:27,434
What I want to say

361
01:10:27,829 --> 01:10:29,648
is that, although
I love him...

362
01:10:29,950 --> 01:10:31,507
--no, although I
have loved him...

363
01:10:32,114 --> 01:10:34,937
If you still can push him
out of my heart...

364
01:10:35,241 --> 01:10:36,445
if you still love me...

365
01:10:37,600 --> 01:10:39,151
as much as you do, today,

366
01:10:39,454 --> 01:10:40,059
then...

367
01:10:41,404 --> 01:10:42,486
then...

368
01:11:20,441 --> 01:11:22,009
Are you happy?

369
01:11:24,262 --> 01:11:26,116
Let's go to
the <i>Drugstore</i>.

370
01:13:40,424 --> 01:13:43,006
We must go now.
It's time.

371
01:13:44,681 --> 01:13:47,476
Don't leave your loft.
But come. Be our boarder.

372
01:13:47,775 --> 01:13:49,916
The room is free.

373
01:14:10,011 --> 01:14:11,447
Look at the moon!

374
01:14:12,377 --> 01:14:13,741
Look!

375
01:14:18,745 --> 01:14:19,900
Is it you, Marthe?

376
01:14:21,516 --> 01:14:22,301
Who is it?

377
01:14:23,388 --> 01:14:24,688
It's him.

378
01:15:42,521 --> 01:15:45,033
She sees me from afar.
She flies towards me.

379
01:15:47,720 --> 01:15:50,180
The night is
luminous, marvellous,

380
01:15:50,870 --> 01:15:52,846
the only such night
in a lifetime.

381
01:15:54,293 --> 01:15:56,314
An enthusiastic cry!
All is forgotten:

382
01:15:56,315 --> 01:15:58,331
separation, pain, tears...

383
01:16:00,495 --> 01:16:03,310
Jacques, forgive me for
being wrong.

384
01:16:03,490 --> 01:16:04,898
"I was blind.

385
01:16:04,899 --> 01:16:06,900
"I hurt you
as I hurt myself.

386
01:16:07,108 --> 01:16:09,141
I have suffered for
you a thousand deaths...

387
01:16:09,835 --> 01:16:11,734
"but it's you I love."

388
01:16:17,584 --> 01:16:19,310
I am astounded, elated.

389
01:16:21,310 --> 01:16:23,148
What strength
makes your eyes

390
01:16:23,149 --> 01:16:25,061
shine with such a flame,

391
01:16:25,275 --> 01:16:27,970
lights your face
with such a smile?

392
01:16:30,289 --> 01:16:32,550
Thank you for your love.

393
01:16:33,555 --> 01:16:36,037
And be blessed for the
happiness you bring me.

394
01:16:57,737 --> 01:17:00,363
She sees me from afar.
She flies towards me.

395
01:17:02,899 --> 01:17:04,190
The night is luminous,

396
01:17:04,290 --> 01:17:05,869
marvellous,

397
01:17:06,164 --> 01:17:08,326
the only such night
in a lifetime.

398
01:17:09,791 --> 01:17:11,564
An enthusiastic cry!
All is forgotten:

399
01:17:11,864 --> 01:17:14,019
separation, pain, tears...

400
01:17:15,684 --> 01:17:18,287
Jacques, forgive me for
being wrong.

401
01:17:18,487 --> 01:17:19,897
"I was blind.

402
01:17:19,898 --> 01:17:21,984
"I hurt you
as I hurt myself.

403
01:17:22,326 --> 01:17:24,402
I have suffered for
you a thousand deaths...

404
01:17:25,365 --> 01:17:26,966
"but it's you I love."

405
01:17:32,737 --> 01:17:34,977
I am astounded, elated.

406
01:17:36,678 --> 01:17:38,521
What strength
makes your eyes

407
01:17:38,525 --> 01:17:40,367
shine with such a flame,

408
01:17:40,560 --> 01:17:43,149
lights your face
with such a smile?

409
01:17:45,494 --> 01:17:47,387
Thank you for your love.

410
01:17:48,954 --> 01:17:51,759
And be blessed for the
happiness you bring me.

411
01:18:28,559 --> 01:18:30,559
English subtitles
.:: Norgen (norgen@centrum.cz) ::.

