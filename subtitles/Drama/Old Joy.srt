﻿1
00:01:12,923 --> 00:01:15,649
Leave a message,
we'll call you back.

2
00:01:17,261 --> 00:01:22,948
Hey, it's Kurt! I am calling you.

3
00:01:23,225 --> 00:01:26,270
I'm in town. Just
hoping you're around.

4
00:01:26,296 --> 00:01:29,314
I don't know if you're
there or what...

5
00:01:29,315 --> 00:01:33,959
Pick it up, man. You
will not regret it.

6
00:01:34,278 --> 00:01:36,628
Hey, Kurt...

7
00:01:37,865 --> 00:01:41,175
So you're here?
Where're you stayin'?

8
00:01:42,329 --> 00:01:45,929
Yeah, thanks. The rumours are true.

9
00:01:46,457 --> 00:01:48,932
Nah, we didn't wanna know.

10
00:01:49,543 --> 00:01:52,019
Oh yeah? What've
you got? Lay it on me.

11
00:01:53,381 --> 00:01:55,731
Sure... I could be into that.

12
00:01:56,425 --> 00:01:59,485
I could use some time in
the woods, absolutely.

13
00:02:01,472 --> 00:02:06,158
Yeah... I think that could be cool.
Let me just run it by Tania.

14
00:02:08,771 --> 00:02:11,580
I'll call you in ten... Right on.

15
00:02:13,067 --> 00:02:15,068
What?

16
00:02:15,069 --> 00:02:18,170
Don't ask my permission...

17
00:02:18,197 --> 00:02:20,547
I said I'd run it by you.

18
00:02:24,912 --> 00:02:28,373
He wants to check out some hot springs
near Estacada, camp out tonight.

19
00:02:28,374 --> 00:02:32,434
Get an early start tomorrow,
be home by... noon I'd say.

20
00:02:33,713 --> 00:02:38,106
Look, if it's gonna be a problem I'll just
call him back and tell him I can't go.

21
00:02:42,888 --> 00:02:44,723
Hey... you should come too.
You're totally welcome.

22
00:02:44,724 --> 00:02:46,865
You know that, it'd be fun.

23
00:02:47,560 --> 00:02:50,911
I'm not exactly up for camping.

24
00:02:52,732 --> 00:02:55,415
It's probably gonna rain anyway.

25
00:02:55,484 --> 00:02:57,235
Could rain all day.

26
00:02:57,236 --> 00:03:00,697
Look, you're just waiting
for me to tell you to go...

27
00:03:00,698 --> 00:03:02,824
We know you're going so I don't know
why we have to go through this thing

28
00:03:02,825 --> 00:03:04,868
of me letting you off the hook.

29
00:03:04,869 --> 00:03:08,470
I'm not gonna enjoy myself if
you're... miserable about it.

30
00:03:38,235 --> 00:03:40,919
Go in, Lucy, go in!

31
00:04:00,966 --> 00:04:02,801
that'd be Eleven Nine.
Hugh, how are you?

32
00:04:02,802 --> 00:04:05,220
- Very good. How are you today?
- Good, thanks!

33
00:04:05,221 --> 00:04:10,642
I'm callin' in... I never could
understand why Lyndon Johnson

34
00:04:10,643 --> 00:04:14,411
became the champion of the
Civil Rights Movement.

35
00:04:14,688 --> 00:04:17,690
are you talking about? I mean the
Voting Rights Act was a great thing.

36
00:04:17,691 --> 00:04:19,609
- Oh yes...
- It was the...

37
00:04:19,610 --> 00:04:22,612
It was the beginning, literally the
beginning of American democracy.

38
00:04:22,613 --> 00:04:25,073
- I grew up in a state...
- It also became the foundation of the...

39
00:04:25,074 --> 00:04:28,034
- Southern Strategy.
- He said we've delivered the South to the

40
00:04:28,035 --> 00:04:31,621
Republican Party for a long
time when he signed the Act.

41
00:04:31,622 --> 00:04:33,581
- I mean is that what you're talking about?
- Yeah.

42
00:04:33,582 --> 00:04:34,749
You said it with regret...

43
00:04:34,750 --> 00:04:36,459
These were all wedge issues.

44
00:04:36,460 --> 00:04:42,340
When Reagan talked about...
He's for State's rights.

45
00:04:42,341 --> 00:04:44,884
All it was, it was just a veiled
way of saying: "He's a racist."

46
00:04:44,885 --> 00:04:47,929
When Nixon talked about "Law and Order",
that's what he was talking about!

47
00:04:47,930 --> 00:04:49,681
He was talking about the...

48
00:04:49,682 --> 00:04:54,435
Think about this. You had... Lee Atwater
kickin' around when all these guys were

49
00:04:54,436 --> 00:04:57,355
devising the Southern
Strategy. Including...

50
00:04:57,356 --> 00:05:01,860
Including Karl Rove, he's taken the
Southern Strategy to the next step,

51
00:05:01,861 --> 00:05:03,820
and that's to divide
us by way of religion.

52
00:05:03,821 --> 00:05:05,864
So, I don't think you're insane.

53
00:05:05,865 --> 00:05:08,017
I mean, I was joking when
I said you're insane...

54
00:05:08,018 --> 00:05:09,617
Not really.

55
00:05:11,161 --> 00:05:14,622
I'll tell you... I'll tell you this.
I agree with the fact that...

56
00:05:14,623 --> 00:05:16,374
- May I interject here?
- Yeah.

57
00:05:16,375 --> 00:05:21,963
I didn't call up and assert that Lyndon
Johnson did this, I asked a question.

58
00:05:21,964 --> 00:05:25,133
But Bobby reacted as
if I was asserting it.

59
00:05:25,134 --> 00:05:27,093
And then he starts calling names.

60
00:05:27,094 --> 00:05:30,320
- Let me go back...
- It was a bad question.

61
00:05:30,639 --> 00:05:34,866
Listen Bobby, there's no such
thing as a bad question...

62
00:06:02,713 --> 00:06:04,813
Kurt!

63
00:06:09,470 --> 00:06:11,611
Hello?

64
00:06:19,647 --> 00:06:22,039
Hello!

65
00:07:06,443 --> 00:07:08,794
Hey man!

66
00:07:08,862 --> 00:07:11,213
Hey, Kurt!

67
00:07:25,796 --> 00:07:31,509
I brought a cooler.
Take it. And a wagon. And a TV.

68
00:07:31,510 --> 00:07:35,278
- You're looking good.
- Really? I wish!

69
00:07:37,516 --> 00:07:39,684
He hasn't sold the house yet?

70
00:07:39,685 --> 00:07:43,286
No, not yet. But my
days are numbered here.

71
00:07:43,731 --> 00:07:45,481
I'm really glad you could come.

72
00:07:45,482 --> 00:07:47,483
I didn't know if you could
make it on such short notice,

73
00:07:47,484 --> 00:07:49,960
everyone's so busy now.

74
00:07:50,571 --> 00:07:55,324
You know I had a dream about you
the other night? Where you're...

75
00:07:55,325 --> 00:07:58,953
in, like, a hospital or something.
It was really weird.

76
00:07:58,954 --> 00:08:01,911
But you were the best
thing about it by far.

77
00:08:01,912 --> 00:08:03,124
That's cool...

78
00:08:03,125 --> 00:08:06,961
- How's Ashland?
- Ashland? Amazing!

79
00:08:06,962 --> 00:08:09,229
Transformative.

80
00:08:09,298 --> 00:08:12,524
I'm at a whole new
place now really.

81
00:08:17,973 --> 00:08:19,807
Wow, you've really
got it all goin' on.

82
00:08:19,808 --> 00:08:21,142
Hell yeah!

83
00:08:21,143 --> 00:08:24,270
- Where's your shit?
- It's in the car. I've got the dog too.

84
00:08:24,271 --> 00:08:27,940
Really? Why don't we take
your ride. What do you think?

85
00:08:27,941 --> 00:08:30,193
Cause this thing is just
a slug in the mountains.

86
00:08:30,194 --> 00:08:32,737
Erm... sure! I can drive.

87
00:08:32,738 --> 00:08:35,172
Alright! Let's load it up!

88
00:08:45,584 --> 00:08:49,712
The Democrats are sitting on their damn
hands. What do you make of that?

89
00:08:49,713 --> 00:08:54,801
First of all, the Democrats in Congress
have made a tragic mistake, and that is...

90
00:08:54,802 --> 00:08:57,053
they seem to think that they're
part of the Government...

91
00:08:57,054 --> 00:09:02,616
- Yeah. Yeah, that's a good point.
- Rather than going to the people.

92
00:09:02,768 --> 00:09:06,521
Opportunity is not just knocking at
the door, it's pounding at the door.

93
00:09:06,522 --> 00:09:10,608
There's the Democratic donkey, you know,
sittin' there sipping tea sayin':

94
00:09:10,609 --> 00:09:14,711
"I don't know! I don't know if I should
get up and answer the door or not."

95
00:09:14,905 --> 00:09:17,021
It is there. You know
we saw the same thing

96
00:09:17,022 --> 00:09:22,802
in last year's presidential election
when our party's nominee did not...

97
00:09:23,247 --> 00:09:26,499
- It'll just take two seconds.
- ...the corporate crooks.

98
00:09:26,500 --> 00:09:29,877
- Hope that's all?
- ...the fight is not

99
00:09:29,878 --> 00:09:32,380
about them. It's not
a fight for them.

100
00:09:32,381 --> 00:09:38,568
- It's a fight to hold on to the very solid...
- Yo, you got ten bucks?

101
00:09:38,971 --> 00:09:42,557
by the Exxon, by the Coors, by the
Murdochs, by Richard Mellon Scaife.

102
00:09:42,558 --> 00:09:48,020
It's a fight, we can't win by removing
ourselves, standing on the sidelines...

103
00:09:48,021 --> 00:09:49,605
Some people say we
need a third party,

104
00:09:49,606 --> 00:09:51,232
I wish we had a second one.

105
00:09:51,233 --> 00:09:54,735
And I'm gonna fight for that
to be the Democratic Party.

106
00:09:54,736 --> 00:09:56,737
But I certainly
understand people...

107
00:09:56,738 --> 00:09:58,739
- Hey, what's up?
- What's goin on?

108
00:09:58,740 --> 00:10:01,675
- 30 dollars?
- 30 dollars...

109
00:10:03,495 --> 00:10:06,956
- ...and other folks, because...
- Take it easy.

110
00:10:06,957 --> 00:10:09,375
And if we do not stand
up for working folks...

111
00:10:09,376 --> 00:10:13,921
The one good thing about the... he
is trying to flip the party back

112
00:10:13,922 --> 00:10:16,648
to the grassroots level.
In last month...

113
00:10:29,938 --> 00:10:32,914
This thing go back any further?

114
00:10:39,865 --> 00:10:41,699
How's your dad?

115
00:10:41,700 --> 00:10:45,510
He's alright, you know.
All things considered.

116
00:10:46,205 --> 00:10:49,498
Last I heard he was
living in... a new place.

117
00:10:49,499 --> 00:10:51,417
Yeah, right.

118
00:10:51,418 --> 00:10:53,878
For some reason he
decided at age 70,

119
00:10:53,879 --> 00:10:57,689
that he didn't want to be married
anymore. So he moved out.

120
00:10:58,425 --> 00:11:00,509
Yeah, that's what I heard.

121
00:11:00,510 --> 00:11:02,819
Then he ended up with these...

122
00:11:02,971 --> 00:11:06,114
fuckin' blood clots on his brain.

123
00:11:06,183 --> 00:11:08,283
Oh, fuck.

124
00:11:08,477 --> 00:11:11,995
And they just showed
up, out of nowhere.

125
00:11:12,147 --> 00:11:15,274
He was really weak for a
while, nobody knew what to do.

126
00:11:15,275 --> 00:11:19,544
Then they... dissolved.
Just went away.

127
00:11:20,656 --> 00:11:22,714
That's heavy.

128
00:11:22,824 --> 00:11:25,576
Really put my mom
through the wringer.

129
00:11:25,577 --> 00:11:29,637
Yeah, I bet. I bet.

130
00:11:31,291 --> 00:11:36,019
It's kind of like when an old
Eskimo goes off to die alone.

131
00:11:39,216 --> 00:11:41,524
Who knows?

132
00:11:50,352 --> 00:11:52,702
You still have this.

133
00:11:53,230 --> 00:11:55,815
- Didn't this use to be Noah's?
- Yeah.

134
00:11:55,816 --> 00:12:00,168
Found it behind Krugers, there's
crumbs in there where you can add it.

135
00:12:01,488 --> 00:12:04,756
And then, it was mine for a while.

136
00:12:06,451 --> 00:12:09,386
Man... Mark, you really
hold on to shit.

137
00:12:10,914 --> 00:12:13,306
Not that I should talk.

138
00:12:14,876 --> 00:12:17,712
I've still got crates of
records in the garage,

139
00:12:17,713 --> 00:12:21,523
stuff that I haven't even
listened to in... ten years.

140
00:12:23,552 --> 00:12:27,772
I'm gonna have to take the whole load down
to Syd's I think. See what I can get for it.

141
00:12:27,773 --> 00:12:30,599
- Syd's just gone man.
- No way!

142
00:12:30,600 --> 00:12:33,853
The rent got to be too heavy,
now it's a... smoothie place.

143
00:12:33,854 --> 00:12:37,023
- Rejuicenation.
- No!

144
00:12:37,024 --> 00:12:39,499
Syd sells on eBay, now.

145
00:12:39,860 --> 00:12:45,046
Oh that makes sense.
I should be doing that.

146
00:12:45,157 --> 00:12:49,118
Tania went by on the last day, said
the only records left in the bins

147
00:12:49,119 --> 00:12:51,261
were our friends'.

148
00:12:54,791 --> 00:12:56,933
No more Syd's.

149
00:12:59,796 --> 00:13:01,813
End of an era.

150
00:15:39,164 --> 00:15:41,264
Come on!

151
00:16:22,332 --> 00:16:24,875
That was amazing! Were
you catching that?

152
00:16:24,876 --> 00:16:29,103
- That young soldier got a raw deal...
- And everybody that was there...

153
00:16:40,850 --> 00:16:44,118
- Do you know where we're goin'?
- Oh yeah!

154
00:16:49,734 --> 00:16:52,543
Cruisy... Downtown!

155
00:16:52,946 --> 00:16:55,114
Now you take this.

156
00:16:55,115 --> 00:16:56,865
No, you take this one.

157
00:16:56,866 --> 00:16:59,092
You take this one.

158
00:17:01,538 --> 00:17:04,013
These aren't even funny.

159
00:17:08,503 --> 00:17:11,020
Back to you, Lucy.

160
00:17:50,754 --> 00:17:54,355
Sunshine!

161
00:18:13,777 --> 00:18:16,294
This is a great spot, man!

162
00:18:16,988 --> 00:18:19,005
You're gonna love it!

163
00:18:20,408 --> 00:18:23,243
I was here...
two summers ago.

164
00:18:23,244 --> 00:18:25,496
Totally private. No one around.

165
00:18:25,497 --> 00:18:27,764
And most of all...

166
00:18:27,957 --> 00:18:32,226
It has this otherworldly
peacefulness about it.

167
00:18:34,339 --> 00:18:36,840
You can really... think.

168
00:18:36,841 --> 00:18:39,108
Sounds awesome.

169
00:18:39,594 --> 00:18:43,571
'Cause you can't... you can't
get real quiet anymore.

170
00:18:44,599 --> 00:18:49,827
I visited this hot spring
in Arizona last summer.

171
00:18:50,647 --> 00:18:54,066
Where no one is allowed to
talk at all. Total silence.

172
00:18:54,067 --> 00:18:56,334
It's fuckin' amazing.

173
00:18:56,986 --> 00:19:00,963
They want me to come back
and work as a chef sometime.

174
00:19:02,784 --> 00:19:05,911
I've got a whole menu
worked out for 'em.

175
00:19:05,912 --> 00:19:09,347
Right, I think I read about
that place in a magazine.

176
00:19:17,132 --> 00:19:19,133
You remember Yogi?

177
00:19:19,134 --> 00:19:22,527
Sure, he stiffed us on the
rent when he left town.

178
00:19:23,471 --> 00:19:26,390
Really? I forgot about that.

179
00:19:26,391 --> 00:19:29,685
Well, I ran into him
in Big Sur last month.

180
00:19:29,686 --> 00:19:32,604
We had the most incredible
night together.

181
00:19:32,605 --> 00:19:37,250
We were out under the stars,
dancing to these drums.

182
00:19:37,485 --> 00:19:40,904
Everybody was jumping
over these huge bonfires.

183
00:19:40,905 --> 00:19:43,824
I'd never seen anything like it.

184
00:19:43,825 --> 00:19:49,178
Everyone there... was just
so joyful that night.

185
00:19:51,750 --> 00:19:56,602
Beautiful women...
singing and dancing.

186
00:19:57,756 --> 00:20:02,608
I don't know, just a really,
really amazing group of people.

187
00:20:05,138 --> 00:20:10,601
- No shit.
- Yeah. You should've been there.

188
00:20:10,602 --> 00:20:13,744
I think Yogi got laid.

189
00:20:50,266 --> 00:20:52,366
Slow down...

190
00:20:53,645 --> 00:20:56,438
Slow down, you wanna go left.

191
00:20:56,439 --> 00:20:59,707
- Really?
- Yeah.

192
00:21:00,109 --> 00:21:02,418
Trust me Mark.

193
00:21:02,987 --> 00:21:05,129
Okay!

194
00:21:05,365 --> 00:21:07,381
I'm in your hands.

195
00:21:33,309 --> 00:21:35,618
I remember that.

196
00:22:42,003 --> 00:22:47,148
I think we're somewhere...
in this area.

197
00:22:58,102 --> 00:23:00,494
I need a little space.

198
00:23:53,908 --> 00:23:56,842
- It's the mother of your child.
- Thank you.

199
00:24:00,498 --> 00:24:02,624
I have no idea...

200
00:24:02,625 --> 00:24:05,267
No it's fine...

201
00:24:30,778 --> 00:24:34,672
Can you hear me? Can you hear me?

202
00:24:38,536 --> 00:24:40,678
What was that?

203
00:24:42,040 --> 00:24:45,250
No... I mean, we are
looking for it.

204
00:24:45,251 --> 00:24:48,435
I can't hear you.
We're gonna have to go.

205
00:24:48,796 --> 00:24:51,730
I'll call you when
I have reception.

206
00:24:54,719 --> 00:24:58,779
I love you.

207
00:25:04,103 --> 00:25:07,204
The sign up there
is literally blank.

208
00:25:11,360 --> 00:25:17,089
- We've entered a whole other zone.
- Far out.

209
00:25:21,079 --> 00:25:25,222
- Up or down?
- Up!

210
00:25:45,895 --> 00:25:47,786
Getting dark...

211
00:25:49,941 --> 00:25:52,041
What do you think, Kurt?

212
00:25:53,402 --> 00:25:57,046
I spotted a turn-off where
we can camp for the night.

213
00:25:58,407 --> 00:26:01,884
Just a few miles back
down that road we passed.

214
00:26:02,537 --> 00:26:06,639
We can camp there... And find
the river in the morning.

215
00:26:09,127 --> 00:26:11,268
You know how to get there?

216
00:26:12,505 --> 00:26:15,940
Yeah, I know how to get there,
but you gotta turn around.

217
00:26:49,959 --> 00:26:52,961
Well. That's close to
here I guess, right?

218
00:26:52,962 --> 00:26:55,562
Still get a good start
in the morning.

219
00:26:56,007 --> 00:26:58,357
Looks fine to me.

220
00:27:04,223 --> 00:27:06,365
Lucy, stay here.

221
00:27:40,801 --> 00:27:43,485
It's good to get out of the city.

222
00:27:44,889 --> 00:27:47,823
Forget all this is
out here sometimes.

223
00:27:48,100 --> 00:27:50,284
No shit.

224
00:27:53,898 --> 00:27:58,235
It's not like there's any big difference
between the forest and the city though.

225
00:27:58,236 --> 00:28:00,419
Know what I mean?

226
00:28:01,072 --> 00:28:07,217
It's all one huge thing, now. There's trees
in the city, and garbage in the forest.

227
00:28:07,662 --> 00:28:10,304
What's the big
difference, you know?

228
00:28:10,873 --> 00:28:13,057
I see what you mean.

229
00:28:19,548 --> 00:28:22,300
So you lookin' forward
to fatherhood?

230
00:28:22,301 --> 00:28:24,443
Yeah.

231
00:28:25,012 --> 00:28:27,112
Sure, I mean...

232
00:28:27,390 --> 00:28:32,076
We're both stretched so thin with work,
it's almost impossible to imagine, but...

233
00:28:32,311 --> 00:28:34,912
It'll have to work itself out.

234
00:28:35,147 --> 00:28:39,166
We'll just... find another rhythm.

235
00:28:39,527 --> 00:28:42,252
Do it is whatever people do.

236
00:28:42,446 --> 00:28:46,465
So... fuckin' brave, man.

237
00:28:47,368 --> 00:28:53,013
I've never gotten myself into anything
that I couldn't get myself out of.

238
00:28:53,207 --> 00:28:56,809
It's just... Having a kid
is so fuckin' for real.

239
00:29:02,174 --> 00:29:04,691
Watch out Ham's cans!

240
00:29:07,054 --> 00:29:10,030
- Did you... are you pumpin' these for me?
- I pumped it.

241
00:29:24,155 --> 00:29:27,157
It's easier to aim with one
hand than with two hands.

242
00:29:27,158 --> 00:29:28,366
Really?

243
00:29:28,367 --> 00:29:31,635
Why do you think on TV
they do it with two hands?

244
00:29:32,663 --> 00:29:35,514
Everything is about looking cool.

245
00:29:35,916 --> 00:29:37,761
But I look pretty cool
with one hand, right?

246
00:29:37,787 --> 00:29:39,318
Yeah...

247
00:29:41,005 --> 00:29:45,342
Two hands is the responsible shooting.
One hand is renegade shooting.

248
00:29:45,343 --> 00:29:47,985
Yeah, renegade...

249
00:29:48,095 --> 00:29:50,404
I'll try leftie.

250
00:29:50,765 --> 00:29:54,309
Switch, switch.
Switch Hitch shooting.

251
00:29:54,310 --> 00:29:58,704
I think one more shot...
Let me take one more shot.

252
00:30:01,400 --> 00:30:05,002
You know I was gonna
sleep out, but...

253
00:30:05,613 --> 00:30:09,115
Is it... cool to sleep in?

254
00:30:09,116 --> 00:30:11,910
- Sleep in the tent with you?
- Fine.

255
00:30:11,911 --> 00:30:13,536
- So you're gonna sleep in the tent?
- It's a two men tent.

256
00:30:13,537 --> 00:30:15,580
Two men and a doggie.

257
00:30:15,581 --> 00:30:18,807
- That's cool.
- Two men and a doggie tent.

258
00:30:18,834 --> 00:30:20,543
That's pretty cool.

259
00:30:20,544 --> 00:30:23,922
Sleeping with the dogs.
Pretty cool. She's a girl.

260
00:30:23,923 --> 00:30:26,064
Keeps you warm.

261
00:30:31,597 --> 00:30:35,225
- I took some night classes.
- Anything good?

262
00:30:35,226 --> 00:30:39,036
It was alright. Some
physics classes...

263
00:30:41,982 --> 00:30:47,461
But here's the thing about that,
It's that I knew more than they did.

264
00:30:49,865 --> 00:30:54,369
All this quark and superstring
shit, I know all about that.

265
00:30:54,370 --> 00:30:56,329
It makes sense.

266
00:30:56,330 --> 00:30:58,514
And don't get me wrong.

267
00:30:59,875 --> 00:31:04,436
I understand it. But that's
not the final answer.

268
00:31:04,880 --> 00:31:09,107
- Wow, you think you understand it?
- Basically...

269
00:31:10,970 --> 00:31:13,111
It's like this see...

270
00:31:14,682 --> 00:31:16,813
Sometimes things look like
they don't have any order,

271
00:31:16,839 --> 00:31:19,785
and then, from a different level,

272
00:31:19,812 --> 00:31:22,704
you realise that it
does have order.

273
00:31:22,898 --> 00:31:24,983
It's like climbing a mountain.

274
00:31:24,984 --> 00:31:30,196
Look around, you see trees and rocks
and bushes, pressing around you.

275
00:31:30,197 --> 00:31:34,117
And then you get above the tree line, you
see everything you just went through

276
00:31:34,118 --> 00:31:40,138
and it all like comes together. You know,
you see that it has a shape, after all.

277
00:31:42,334 --> 00:31:47,437
Sometimes it takes a long time to get high
enough to see it, but it's there, it's...

278
00:31:49,258 --> 00:31:52,264
It's all about space
and time and how it...

279
00:31:52,290 --> 00:31:55,464
How they change the
rules sometimes.

280
00:31:56,265 --> 00:31:58,907
Makes perfect sense to me.

281
00:32:00,102 --> 00:32:05,273
It's like... two mirrors.
Moving through space.

282
00:32:05,274 --> 00:32:10,627
There's a single atom
moving between them.

283
00:32:11,614 --> 00:32:15,507
Fuck. I forget...

284
00:32:17,411 --> 00:32:18,620
Whatever...

285
00:32:18,621 --> 00:32:23,625
But I... I... The thing is: I get it.
I get it on a fundamental level.

286
00:32:23,626 --> 00:32:28,546
The thing is... Is that
I have my own theory.

287
00:32:28,547 --> 00:32:30,772
Ah yeah?

288
00:32:30,799 --> 00:32:34,276
It's that the universe
is falling, man.

289
00:32:34,386 --> 00:32:36,054
That's what explains it all.

290
00:32:36,055 --> 00:32:42,227
The entire universe is in the shape of a
falling tear. Dropping down through space.

291
00:32:42,228 --> 00:32:45,631
I'm telling you man. I don't
know how it happened,

292
00:32:45,657 --> 00:32:48,233
but that's just the
way that it works.

293
00:32:48,234 --> 00:32:55,213
This tear, it's been dropping down
for ever, just doesn't stop...

294
00:32:59,787 --> 00:33:06,209
So did you tell them that? Your theory
about the tear-shaped universe?

295
00:33:06,210 --> 00:33:08,769
Did I tell them?

296
00:33:12,049 --> 00:33:14,524
Who the hell am I?

297
00:33:19,014 --> 00:33:22,684
They don't care about my theory,
doesn't mean shit to them...

298
00:33:22,685 --> 00:33:25,494
I don't have any numbers for it.

299
00:33:26,647 --> 00:33:28,789
God...

300
00:33:52,381 --> 00:33:55,065
I miss you, Mark.

301
00:33:55,676 --> 00:33:58,902
I miss you really, really bad.

302
00:33:59,305 --> 00:34:01,180
I want us to be real friends again.

303
00:34:01,181 --> 00:34:06,743
There's something... between us, and I
don't like it. I want it to go away.

304
00:34:10,357 --> 00:34:14,126
Man, what are you talking
about? We're fine.

305
00:34:14,737 --> 00:34:17,113
Are you serious?
Do you really think that?

306
00:34:17,114 --> 00:34:19,297
Of course!

307
00:34:19,408 --> 00:34:23,510
Course I do, we're fine.
We're totally fine.

308
00:34:23,954 --> 00:34:26,096
I don't know...

309
00:34:29,710 --> 00:34:31,935
My God!

310
00:34:38,552 --> 00:34:40,986
God, I'm sorry.

311
00:34:43,349 --> 00:34:45,907
I'm just being crazy.

312
00:34:50,439 --> 00:34:52,581
I'm sorry.

313
00:34:59,573 --> 00:35:02,966
I'm just being crazy.
I know...

314
00:35:03,285 --> 00:35:06,178
Don't pay any attention
to me, okay?

315
00:35:06,622 --> 00:35:09,014
We're fine...

316
00:35:11,251 --> 00:35:13,769
Everything's totally fine.

317
00:35:15,547 --> 00:35:18,064
Feel a lot better now.

318
00:39:01,231 --> 00:39:04,791
- Your dog's in distress.
- Yeah...

319
00:39:04,902 --> 00:39:11,214
Tania calls it "separation anxiety".
It hates to be left out.

320
00:39:13,118 --> 00:39:15,343
Hi!

321
00:39:17,915 --> 00:39:19,707
Thanks.

322
00:39:19,708 --> 00:39:22,443
How far are we from
Bagby Hot Springs?

323
00:39:22,469 --> 00:39:26,854
Oh, you're real close. I think.
I've never been there, but...

324
00:39:26,880 --> 00:39:31,151
it's around here somewhere.
Maybe 10 miles... away.

325
00:39:31,219 --> 00:39:33,079
We might have a little
map printout in the back.

326
00:39:33,105 --> 00:39:34,564
- I'll check for ya.
- Great.

327
00:39:34,565 --> 00:39:38,399
And I'll bring you some coffee
while you're looking at the menus?

328
00:39:43,315 --> 00:39:49,553
This is better anyway. Now we won't be
rushed, we can take our sweet time...

329
00:39:56,620 --> 00:39:58,371
- Need more time?
- Nope.

330
00:39:58,372 --> 00:40:00,513
Alright.

331
00:40:01,792 --> 00:40:04,309
Hey, hang on a sec.

332
00:40:04,503 --> 00:40:14,569
Could I get... two scrambled
eggs, dry toast and sausage.

333
00:40:21,311 --> 00:40:26,148
Before my call comes. Could I get
the same as my friend please?

334
00:40:26,149 --> 00:40:32,822
Except, bacon instead of
sausage, and no dry toast.

335
00:40:32,823 --> 00:40:34,824
Wet toast please.

336
00:40:34,825 --> 00:40:36,966
Very good.

337
00:40:37,119 --> 00:40:40,136
Not too wet, just make it damp.

338
00:40:48,714 --> 00:40:52,565
I know... I know, I'm sorry.

339
00:40:52,926 --> 00:40:55,553
Well, he thought he knew
where it was, but...

340
00:40:55,554 --> 00:40:58,029
Remember who we're dealing with.

341
00:40:58,765 --> 00:41:03,159
I don't know. Apparently
we're... closing in on it.

342
00:41:04,062 --> 00:41:06,329
Did you do anything last night?

343
00:41:07,733 --> 00:41:09,999
She's fine.

344
00:41:18,243 --> 00:41:20,510
We're super close.

345
00:41:23,373 --> 00:41:25,849
I never doubted you, man.

346
00:41:28,253 --> 00:41:30,687
This is where we're goin'.

347
00:41:31,673 --> 00:41:34,983
- This is where we are?
- Awesome.

348
00:42:09,169 --> 00:42:12,046
Yo, yo, yo yo yo...

349
00:42:12,047 --> 00:42:15,940
There! On the tree.

350
00:42:17,552 --> 00:42:19,220
I don't know how we
could've missed that?

351
00:42:19,221 --> 00:42:22,681
I'm tellin' you man. This
is a very special place.

352
00:42:22,682 --> 00:42:27,118
If you can't see the little arrows
at night, you can't get in.

353
00:43:53,190 --> 00:43:55,274
- Grew pickles?
- Yes.

354
00:43:55,275 --> 00:43:58,068
So they grew pickles?
It's only like a half-sour.

355
00:43:58,069 --> 00:43:59,937
It was... I guess
it's called half-sour

356
00:43:59,963 --> 00:44:03,573
where they're sort of salty and
don't have a lot of flavour.

357
00:44:16,922 --> 00:44:20,064
- How are you feeling?
- Good.

358
00:44:20,300 --> 00:44:24,152
I feel good. How about you?

359
00:44:24,304 --> 00:44:26,654
Feel good too.

360
00:44:36,399 --> 00:44:38,875
I see sticks!

361
00:44:50,956 --> 00:44:53,264
Yeah, over here!

362
00:44:53,333 --> 00:44:55,725
My God, Lucy, look!

363
00:44:56,962 --> 00:44:59,270
Go catch it!

364
00:45:57,772 --> 00:46:03,402
It's only one day a week but,
really pulls me out of myself.

365
00:46:03,403 --> 00:46:05,628
The kids are cool.

366
00:46:05,864 --> 00:46:07,448
They're really into it.

367
00:46:07,449 --> 00:46:11,243
And it's fun thinking about
carpentry and woodworking...

368
00:46:11,244 --> 00:46:14,720
in a way I haven't for a long time.

369
00:46:16,249 --> 00:46:19,809
Makes you realise how
long ago sixteen was.

370
00:46:21,296 --> 00:46:25,258
So now this... it was
basically a dumping ground...

371
00:46:25,284 --> 00:46:30,386
is this really happening
community garden.

372
00:46:33,183 --> 00:46:35,908
I'm so proud of you, Mark.

373
00:46:37,145 --> 00:46:39,287
I'm serious.

374
00:46:39,522 --> 00:46:41,873
You've really done something.

375
00:46:43,109 --> 00:46:46,396
You've really given something
back to the community.

376
00:46:46,422 --> 00:46:48,388
I don't know...

377
00:46:48,415 --> 00:46:51,891
No, really. I'm serious.

378
00:46:54,704 --> 00:46:57,305
It's really something.

379
00:46:58,875 --> 00:47:03,936
It's not that big a deal... It's not
that... much time out of my life.

380
00:47:04,297 --> 00:47:07,690
It's nothing you couldn't do if you
felt like it too.

381
00:47:09,010 --> 00:47:13,488
Not that you don't give to the community,
I mean...

382
00:47:14,933 --> 00:47:17,950
Just a... different community.

383
00:48:14,534 --> 00:48:15,659
This is it, Mark.

384
00:48:15,660 --> 00:48:19,929
- Cool, this looks fantastic!
- I hope you like it.

385
00:48:41,394 --> 00:48:43,661
Awesome.

386
00:48:48,109 --> 00:48:53,629
Alright. Let's start these flowing.

387
00:48:56,075 --> 00:48:58,676
Plug it up, and then...

388
00:49:03,374 --> 00:49:07,351
Let's grab it.

389
00:49:11,591 --> 00:49:13,774
Come on.

390
00:49:20,975 --> 00:49:23,326
Fill up here.

391
00:49:27,524 --> 00:49:33,085
And then... just make the tubs
cool enough to get our bodies in.

392
00:49:38,535 --> 00:49:41,385
Just dump it in.

393
00:49:42,997 --> 00:49:45,556
- Whole thing?
- Yeah!

394
00:49:48,294 --> 00:49:50,811
Probably wanna refill.

395
00:49:58,596 --> 00:50:03,407
Look at that water.

396
00:55:27,800 --> 00:55:29,885
Man...

397
00:55:29,886 --> 00:55:31,986
Yesterday...

398
00:55:33,264 --> 00:55:37,449
Wanted to get a notebook.
For my journal.

399
00:55:38,144 --> 00:55:45,499
So I borrowed Pete's bike, and
first when I took off riding,

400
00:55:45,985 --> 00:55:51,197
and where I was on the sidewalk, there
was this old man on the sidewalk too.

401
00:55:51,198 --> 00:55:56,536
And... you know he was walkin', walkin'
walkin', so I thought: "Oh, I'm clear."

402
00:55:56,537 --> 00:56:00,290
And I took off to the left, around him,
and just then he stepped to the left,

403
00:56:00,291 --> 00:56:02,000
and I didn't ring the
bell or anything.

404
00:56:02,001 --> 00:56:08,965
When I got past him, he yelled at me. He
was like: "Hey! Thanks for the warning!"

405
00:56:08,966 --> 00:56:14,319
And I just felt so... bad.
All of a sudden I felt so guilty.

406
00:56:15,848 --> 00:56:19,825
But it was fine, you know. Never
gonna see this guy again.

407
00:56:19,927 --> 00:56:23,060
But I realise I forgot something
at the house. So I rode back.

408
00:56:23,086 --> 00:56:26,872
Had to ride back by him. Just looked
at his face, like, I felt like:

409
00:56:26,898 --> 00:56:29,953
"Oh, he's so angry at me.
Totally hates me."

410
00:56:29,954 --> 00:56:32,847
Whatever. Can forget him again.

411
00:56:33,366 --> 00:56:38,260
So, then I wanna go, you know...
Get something to drink.

412
00:56:38,704 --> 00:56:42,374
And I go into a little
store, stop the bike.

413
00:56:42,375 --> 00:56:45,293
When I come back out, I get
on the bike, I'm riding,

414
00:56:45,294 --> 00:56:50,856
I'm riding down the same street, cause
it's a hill. And I pass the guy again.

415
00:56:51,968 --> 00:56:56,361
And again, I'm just like, feeling
more and more tense, like...

416
00:56:57,056 --> 00:57:01,267
Then I've got a stretch, you know,
before I get to the notebook store.

417
00:57:01,268 --> 00:57:07,315
So I'm like: "Okay! I can finally move
on with my life, not have this guy!"

418
00:57:07,316 --> 00:57:10,610
Go down to the notebook store, look
for my notebook. It's like 20 blocks.

419
00:57:10,611 --> 00:57:14,463
Look for my notebook. Come
up to the cash register.

420
00:57:14,515 --> 00:57:19,715
I'm buyin' the notebook, and the
automatic doors open, and I look up

421
00:57:19,841 --> 00:57:24,289
and the guy is like, walking right
into the store, right, in front of me.

422
00:57:24,583 --> 00:57:27,050
And I'm just... can't believe it!

423
00:57:27,076 --> 00:57:32,132
And then the cashier woman, I hear
her talk to me all of a sudden,

424
00:57:32,133 --> 00:57:35,826
She's like: "Superstar."

425
00:57:37,730 --> 00:57:39,467
What?

426
00:57:39,493 --> 00:57:41,267
And I look at her.

427
00:57:41,293 --> 00:57:44,188
And she's this Indian woman, maybe
like 50 year-old Indian woman,

428
00:57:44,214 --> 00:57:47,802
with glasses and a dot on
her forehead. And I was like:

429
00:57:47,828 --> 00:57:51,884
"What?" She said: "Jesus
Christ Superstar!"

430
00:57:52,403 --> 00:57:54,904
"You said 'Jesus Christ!"'

431
00:57:54,905 --> 00:57:58,074
I was like: "No! I didn't
say 'Jesus Christ."'

432
00:57:58,075 --> 00:58:02,912
You know? I looked at the bagging guy.
Asked like: "Did I say 'Jesus Christ'?"

433
00:58:02,913 --> 00:58:05,597
He's like, just, shrugs.

434
00:58:06,000 --> 00:58:08,851
And she's like:
"It's okay! It's okay!"

435
00:58:09,712 --> 00:58:13,882
felt like, I feel like I would've known
if I'd said "Jesus Christ", you know?

436
00:58:13,883 --> 00:58:17,761
I would've heard my voice in my
memory, like: "Jesus Christ."

437
00:58:17,762 --> 00:58:20,696
"Jesus Christ", Kurt
said, "Jesus Christ."

438
00:58:20,765 --> 00:58:25,242
So, I was still shaking my head, I was
like, I feel like I'm going crazy.

439
00:58:26,062 --> 00:58:29,689
And I'm still sorta tense and everything.
"I feel like I'm going crazy!"

440
00:58:29,690 --> 00:58:33,709
She's like: "It's okay! You're
not crazy. It's okay."

441
00:58:34,236 --> 00:58:36,821
She was really nice.

442
00:58:36,822 --> 00:58:39,798
And right when she said that, I...

443
00:58:39,992 --> 00:58:44,322
This dream that I'd had just,
just yesterday morning,

444
00:58:44,348 --> 00:58:48,765
just flashed on me,
because, the same lady,

445
00:58:48,791 --> 00:58:54,838
the cashier lady was in the dream,
doing the same thing, basically.

446
00:58:55,508 --> 00:58:59,052
I mean, it wasn't in a store...

447
00:58:59,053 --> 00:59:04,599
But I was really upset about something.
Totally depressed about something.

448
00:59:04,600 --> 00:59:09,042
And she just... put her
arms around me and said...

449
00:59:09,068 --> 00:59:11,848
"It's okay! You're okay."

450
00:59:13,984 --> 00:59:18,629
"Sorrow is nothing
but worn-out joy."

451
00:59:44,265 --> 00:59:47,491
I see all kinds of
shit out there, man.

452
00:59:49,395 --> 00:59:54,373
Most people don't see
it, they don't want to.

453
00:59:58,362 --> 01:00:00,629
Hey, what's goin' on?

454
01:00:01,490 --> 01:00:04,383
Just relax, man.

455
01:00:08,289 --> 01:00:12,391
- Just... settle in.
- Alright.

456
01:03:30,115 --> 01:03:32,591
Lucy, come!

457
01:07:34,735 --> 01:07:38,294
- All righty!
- Yep.

458
01:07:41,575 --> 01:07:44,884
That was... that was awesome, Kurt.

459
01:07:46,330 --> 01:07:48,847
I'll call you soon, man.

460
01:07:59,217 --> 01:08:01,651
Hold on a sec.

461
01:08:27,371 --> 01:08:29,497
See ya.

462
01:08:29,498 --> 01:08:31,431
See ya, Kurt.

463
01:08:37,464 --> 01:08:42,426
take care of the new people
who come along every year

464
01:08:42,427 --> 01:08:45,763
to join the labour force
or to re-join them.

465
01:08:45,764 --> 01:08:50,017
And it doesn't take care of the
sharply higher cost of necessities.

466
01:08:50,018 --> 01:08:54,605
It's one thing to say inflation appears
under control of the aggregate.

467
01:08:54,606 --> 01:08:57,155
But when you notice
that housing costs,

468
01:08:57,181 --> 01:09:00,653
healthcare costs and
energy costs are exploding

469
01:09:00,654 --> 01:09:04,418
you're talking about things that
make up the overwhelming share

470
01:09:04,444 --> 01:09:06,909
of the budget of an
ordinary family.

471
01:09:06,910 --> 01:09:10,776
And so the combination of
uncertainty about the future

472
01:09:10,802 --> 01:09:13,416
and the pressures on the present

473
01:09:13,417 --> 01:09:17,670
create this mood. Now the White
House is well aware of this.

474
01:09:17,671 --> 01:09:20,044
You hear the word
"recovery" all the time,

475
01:09:20,070 --> 01:09:23,758
because they don't dare use
the word "prosperity".

476
01:09:23,844 --> 01:09:27,930
You also see things like these...
photo opportunities this week

477
01:09:27,931 --> 01:09:33,576
from the... uh... so-called...
'working vacation' in Texas.

478
01:09:54,666 --> 01:09:57,573
Think you can help me
out with a little change?

479
01:09:57,599 --> 01:10:00,086
Ahhh... I'm sorry, man.

480
01:10:20,066 --> 01:10:21,609
- Hey man.
- Right on, thanks.

481
01:10:21,610 --> 01:10:24,210
- Have a good night.
- You too.