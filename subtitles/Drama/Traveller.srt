1
00:01:05,031 --> 00:01:06,726
Prof. Yoshiyama...

2
00:01:07,367 --> 00:01:10,097
Here's the research data you asked for.

3
00:01:10,437 --> 00:01:11,529
Thank you.

4
00:01:16,509 --> 00:01:20,969
Don't forget the conference at 14:00.

5
00:01:22,082 --> 00:01:24,516
Immersing yourself in
your own research is fine...

6
00:01:24,751 --> 00:01:27,515
but don't be late this time.

7
00:01:55,582 --> 00:01:59,484
TIME TRAVELLER
- The Girl Who Leapt Through Time -

8
00:02:11,431 --> 00:02:14,059
Here's one from 1972.

9
00:02:14,501 --> 00:02:19,097
Why do you collect coins
from so long ago?

10
00:02:19,372 --> 00:02:21,203
I'm not sure myself.

11
00:02:21,508 --> 00:02:22,702
"1972"

12
00:02:32,318 --> 00:02:33,342
Original novel by Yasutaka Tsutsui
"The Girl Who Leapt Through Time"

13
00:02:33,753 --> 00:02:35,084
"Shotoku Univ. Entrance Exam

14
00:02:35,588 --> 00:02:38,113
Results for 2010"

15
00:02:44,531 --> 00:02:46,123
No way!

16
00:02:48,701 --> 00:02:49,725
Here it is.

17
00:02:51,604 --> 00:02:53,572
Excuse me. I'm sorry.

18
00:02:55,775 --> 00:02:57,538
"Department of Pharmacy"

19
00:03:02,382 --> 00:03:05,112
Directed by Masaaki Taniguchi

20
00:03:28,541 --> 00:03:31,237
Miracle happens!

21
00:03:31,778 --> 00:03:34,246
Congratulations!

22
00:03:48,494 --> 00:03:51,861
Congratulations on passing the exam.

23
00:03:52,398 --> 00:03:54,628
Thanks.

24
00:04:01,274 --> 00:04:03,708
We always celebrate here.

25
00:04:03,943 --> 00:04:05,342
Yeah.

26
00:04:06,713 --> 00:04:12,379
This boat feels smaller each time.

27
00:04:13,386 --> 00:04:17,288
I wonder when we'll come next.

28
00:04:23,863 --> 00:04:25,728
Mom...

29
00:04:27,267 --> 00:04:31,328
What kind of person was Dad?

30
00:04:31,871 --> 00:04:34,271
Why ask now?

31
00:04:36,442 --> 00:04:38,433
I was just wondering.

32
00:04:38,678 --> 00:04:44,844
I barely remember him
so I'm curious.

33
00:04:45,752 --> 00:04:47,515
He's different.

34
00:04:47,720 --> 00:04:52,282
He's back in Japan
shooting movies again.

35
00:04:52,592 --> 00:04:55,186
He's in the mountains right now.

36
00:04:57,730 --> 00:04:59,493
You keep in touch?

37
00:04:59,766 --> 00:05:02,291
He emails me sometimes.

38
00:05:03,636 --> 00:05:05,866
Why didn't you tell me?

39
00:05:06,306 --> 00:05:08,240
You never asked.

40
00:05:08,441 --> 00:05:10,602
That's why?

41
00:05:11,377 --> 00:05:13,277
For you, ducky.

42
00:05:18,451 --> 00:05:20,544
"Misonou High: Archery Club"

43
00:05:20,953 --> 00:05:24,320
"Today is the seniors'
last day of practice"

44
00:05:41,974 --> 00:05:43,236
I knew it.

45
00:05:43,509 --> 00:05:46,444
Your form was cool though.

46
00:05:49,782 --> 00:05:51,340
Shut up.

47
00:05:53,052 --> 00:05:57,318
Just take them home
a little by little.

48
00:05:57,623 --> 00:06:01,059
I'm glad you're here.

49
00:06:01,694 --> 00:06:02,956
Thanks from me too.

50
00:06:03,496 --> 00:06:06,465
Well, it's nice being
around young people.

51
00:06:06,933 --> 00:06:08,491
You know that song?

52
00:06:08,701 --> 00:06:10,601
Of course. Takuro Yoshida is my idol.

53
00:06:10,937 --> 00:06:13,064
He's a strange kid.

54
00:06:13,473 --> 00:06:15,304
- What's this song?
- "It was Spring"

55
00:06:15,541 --> 00:06:17,406
Correct.

56
00:06:17,777 --> 00:06:24,910
Our voices disappeared...
It was spring...

57
00:06:26,786 --> 00:06:30,313
My throat hurts.
Too much singing.

58
00:06:30,923 --> 00:06:33,517
You're such a big help.

59
00:06:33,760 --> 00:06:38,663
Mom always says it's great
to have you around.

60
00:06:39,399 --> 00:06:40,559
She says that?

61
00:06:41,734 --> 00:06:43,895
She says that.

62
00:06:45,605 --> 00:06:47,664
Is she still busy?

63
00:06:47,974 --> 00:06:52,741
She's a workaholic.
I hope she doesn't get sick.

64
00:06:53,613 --> 00:06:55,706
Thank you for helping.

65
00:06:56,015 --> 00:06:58,882
See ya.

66
00:07:17,403 --> 00:07:18,597
Goro!

67
00:07:19,439 --> 00:07:21,066
Sorry to bother you.

68
00:07:21,507 --> 00:07:23,338
You have something for me?

69
00:07:24,410 --> 00:07:26,071
A photo of you.

70
00:07:26,579 --> 00:07:27,910
Of me?

71
00:07:28,080 --> 00:07:33,609
Remember my neighbor
Granny Fukamachi? It's from her.

72
00:07:34,554 --> 00:07:37,523
She found it in her attic.

73
00:07:42,395 --> 00:07:45,694
She doesn't know why it was there...

74
00:07:46,065 --> 00:07:48,533
but wants you to have it.

75
00:07:50,736 --> 00:07:56,641
Was this guy in our class though?

76
00:07:57,109 --> 00:08:00,977
She grows them
in her greenhouse.

77
00:08:01,447 --> 00:08:04,746
She knows you like lavender.

78
00:08:16,696 --> 00:08:21,895
Saturday. Science lab.

79
00:08:44,156 --> 00:08:46,488
Yoshiyama...

80
00:08:48,661 --> 00:08:50,891
I have to go.

81
00:08:51,564 --> 00:08:54,089
Sorry to keep you.

82
00:09:12,118 --> 00:09:13,517
Sorry.

83
00:09:22,528 --> 00:09:23,995
Yoshiyama.

84
00:09:25,498 --> 00:09:26,965
Fukamachi...

85
00:09:51,324 --> 00:09:53,224
Hello.

86
00:10:02,868 --> 00:10:05,530
My name's Yoshiyama.

87
00:10:05,871 --> 00:10:06,929
The daughter?

88
00:10:07,106 --> 00:10:08,596
Yes.

89
00:10:09,008 --> 00:10:12,944
She has no serious external injuries...

90
00:10:13,245 --> 00:10:16,214
and they'll heal completely
in two weeks.

91
00:10:16,682 --> 00:10:21,984
But she hit her head hard
and is still unconscious.

92
00:10:22,254 --> 00:10:24,882
We don't know when
she'll come around.

93
00:11:15,174 --> 00:11:18,769
Talk to her as much as you can.

94
00:11:20,179 --> 00:11:23,307
I'll be back later.

95
00:11:48,941 --> 00:11:50,636
Mom...

96
00:11:54,213 --> 00:11:56,408
Mom...

97
00:11:59,685 --> 00:12:02,620
Wake up!

98
00:12:19,772 --> 00:12:21,706
You haven't slept.

99
00:12:23,342 --> 00:12:25,435
You should lie down.

100
00:12:28,180 --> 00:12:30,239
No.

101
00:12:32,418 --> 00:12:38,186
You have to stay strong for
your mom when she wakes up.

102
00:12:42,361 --> 00:12:49,824
Today, let's travel back
in time to Mar. 3, 1974.

103
00:12:50,770 --> 00:12:54,706
The Noshiro night bus accident.

104
00:12:55,141 --> 00:12:58,702
The bus, full of passengers,

105
00:12:59,011 --> 00:13:03,004
departed Shinjuku
on the night of Mar. 2.

106
00:13:03,282 --> 00:13:07,412
It traveled northbound
on Route 4 until...

107
00:13:07,820 --> 00:13:12,723
36 years ago

108
00:13:14,960 --> 00:13:20,193
I was supposed to be on this bus.

109
00:13:22,268 --> 00:13:27,433
It fell off a cliff killing
38 people onboard...

110
00:13:28,207 --> 00:13:31,301
I was going on a ski trip.

111
00:13:34,280 --> 00:13:41,448
But I forgot my bus ticket at home.

112
00:13:48,828 --> 00:13:50,386
"A Night Bus Accident
Kills All Passengers"

113
00:13:51,030 --> 00:13:52,520
A friend of mine...

114
00:13:53,232 --> 00:13:57,168
died in this accident.

115
00:13:58,003 --> 00:13:59,493
But...

116
00:14:01,373 --> 00:14:04,342
I'm alive.

117
00:14:04,977 --> 00:14:07,775
It was fate.

118
00:14:12,585 --> 00:14:16,544
Fate is on your mom's side.

119
00:14:18,324 --> 00:14:21,225
So she'll wake up.

120
00:14:26,532 --> 00:14:29,000
I'm sure of it.

121
00:14:36,375 --> 00:14:38,309
Thanks.

122
00:15:25,491 --> 00:15:28,221
"Birth of Akari Hasegawa"

123
00:15:58,557 --> 00:16:00,991
"Masamichi Hasegawa"

124
00:16:07,366 --> 00:16:10,164
"I'll be filming in the
mountains for 4 months. "

125
00:16:27,453 --> 00:16:28,545
"This is Akari. Mom had an accident...

126
00:16:29,054 --> 00:16:31,181
and is in the hospital. "

127
00:16:31,490 --> 00:16:36,189
"If... "

128
00:16:59,585 --> 00:17:01,450
Mom...

129
00:17:06,992 --> 00:17:09,119
It's...

130
00:17:13,098 --> 00:17:15,362
my birthday today.

131
00:17:19,571 --> 00:17:21,732
I'm 18.

132
00:17:26,478 --> 00:17:28,503
Sort of...

133
00:17:31,216 --> 00:17:34,185
sounds grown-up, huh?

134
00:18:07,286 --> 00:18:08,617
Mom?

135
00:18:12,724 --> 00:18:14,282
Mom!

136
00:18:15,427 --> 00:18:16,621
Akari...

137
00:18:16,829 --> 00:18:19,423
It's me.

138
00:18:21,266 --> 00:18:22,028
I have to go.

139
00:18:22,267 --> 00:18:25,395
No! Lie still.

140
00:18:26,772 --> 00:18:28,103
What?

141
00:18:28,307 --> 00:18:31,174
To see him.

142
00:18:32,845 --> 00:18:35,177
Kazuo Fukamachi.

143
00:18:37,182 --> 00:18:40,481
Kazuo Fukamachi?

144
00:18:42,254 --> 00:18:44,552
The photo and key...

145
00:18:45,224 --> 00:18:47,215
in my bag.

146
00:18:47,392 --> 00:18:49,257
Bag?

147
00:18:50,162 --> 00:18:51,629
The photo...

148
00:19:10,182 --> 00:19:11,672
This?

149
00:19:26,131 --> 00:19:28,326
I have to go.

150
00:19:28,567 --> 00:19:30,364
No, Mom!

151
00:19:30,569 --> 00:19:35,404
I'll go, okay?

152
00:19:35,607 --> 00:19:37,336
I'll go.

153
00:19:37,576 --> 00:19:40,170
Where is he?

154
00:19:40,412 --> 00:19:47,284
Saturday, April 1972.

155
00:19:48,120 --> 00:19:52,887
The junior high science lab.

156
00:19:56,195 --> 00:20:00,598
1972?

157
00:20:00,866 --> 00:20:04,131
The key to my desk...

158
00:20:04,469 --> 00:20:08,530
There's a liquid...

159
00:20:08,874 --> 00:20:14,107
It'll take you to the past.

160
00:20:14,413 --> 00:20:25,483
Drink it and wish hard.

161
00:20:27,526 --> 00:20:30,859
Tell him that...

162
00:20:34,333 --> 00:20:36,858
Okay.

163
00:20:37,736 --> 00:20:40,603
I'll go tell him.

164
00:20:40,906 --> 00:20:43,739
So you have to...

165
00:20:43,976 --> 00:20:47,605
Mom... Wake up!

166
00:21:45,771 --> 00:21:50,003
"Showa Era 47"

167
00:21:51,910 --> 00:21:55,471
That's 1972.

168
00:22:34,953 --> 00:22:40,323
Saturday, April 1972.
Science lab.

169
00:23:12,023 --> 00:23:14,514
What am I doing?

170
00:23:20,732 --> 00:23:23,462
This is crazy.

171
00:23:30,442 --> 00:23:33,900
Was it...

172
00:23:34,513 --> 00:23:40,713
February 1974?.

173
00:23:53,832 --> 00:23:59,031
Saturday, February 1974.
Science lab.

174
00:24:25,764 --> 00:24:27,959
What the heck?

175
00:26:01,626 --> 00:26:04,993
She came falling.

176
00:26:05,230 --> 00:26:06,458
Out of thin air.

177
00:27:12,230 --> 00:27:13,595
I'm sorry.

178
00:27:13,798 --> 00:27:20,135
Who are you?.

179
00:27:23,642 --> 00:27:26,941
I'm Ryota and
this is my apartment.

180
00:27:27,212 --> 00:27:28,679
Remember yesterday?

181
00:27:30,749 --> 00:27:32,910
Yesterday...

182
00:27:34,319 --> 00:27:36,913
Kazuo Fukamachi?!

183
00:27:37,088 --> 00:27:38,953
You're...

184
00:27:39,724 --> 00:27:41,089
Not.

185
00:27:41,593 --> 00:27:45,051
Is this the Setagaya
Jr. High science lab?

186
00:27:45,363 --> 00:27:46,887
Shotoku University.

187
00:27:47,599 --> 00:27:50,329
Oh, no!

188
00:27:53,872 --> 00:27:58,138
I couldn't just leave you there.

189
00:27:58,376 --> 00:28:00,674
So I brought you here.

190
00:28:03,281 --> 00:28:05,841
What's today's date?

191
00:28:06,117 --> 00:28:10,281
Sunday, Feb. 17, 1974.

192
00:28:31,076 --> 00:28:32,907
1974...

193
00:28:33,144 --> 00:28:34,702
February 1974.

194
00:28:35,046 --> 00:28:39,415
April 1972.

195
00:28:42,887 --> 00:28:46,015
I'm 2 years too late.

196
00:28:52,964 --> 00:28:55,125
Your bag!

197
00:29:01,039 --> 00:29:02,438
Where's Setagaya Jr. High?

198
00:29:02,874 --> 00:29:05,399
Uh, the station's that way.

199
00:29:19,457 --> 00:29:22,722
I'm completely lost.
Come with me.

200
00:29:23,461 --> 00:29:25,656
"Setagaya Junior High School"

201
00:29:28,066 --> 00:29:30,933
Excuse me... I'm looking for someone.

202
00:29:31,102 --> 00:29:33,969
This guy.

203
00:29:35,373 --> 00:29:37,967
That's Kazuko Yoshiyama
next to him, isn't it?

204
00:29:38,343 --> 00:29:39,935
That's right.

205
00:29:40,245 --> 00:29:43,408
But I'm looking for the guy.

206
00:29:43,948 --> 00:29:46,075
I've never seen him.

207
00:29:46,317 --> 00:29:51,220
Stop loafing around and go home.

208
00:30:03,768 --> 00:30:05,030
Fresh off the grill.

209
00:30:05,203 --> 00:30:06,465
Thank you.

210
00:30:07,071 --> 00:30:11,804
It really is February 1974.

211
00:30:13,878 --> 00:30:15,846
What's your name?

212
00:30:16,147 --> 00:30:17,739
Akari Yoshiyama.

213
00:30:18,783 --> 00:30:20,307
Your home?

214
00:30:24,222 --> 00:30:29,751
Go home when you finish eating.
Your parents must be worried.

215
00:30:30,128 --> 00:30:32,756
You ran away, right?

216
00:30:38,770 --> 00:30:40,761
I...

217
00:30:43,141 --> 00:30:51,014
came from 2010.

218
00:30:54,185 --> 00:30:58,554
I came from the future
to meet someone.

219
00:30:58,990 --> 00:31:00,355
You hit your head hard.

220
00:31:00,825 --> 00:31:05,228
I'm serious. I appeared
out of nowhere, didn't I?

221
00:31:05,430 --> 00:31:08,524
Yeah, but...

222
00:31:09,033 --> 00:31:10,762
Okay, proof.

223
00:31:20,044 --> 00:31:22,069
My wallet.

224
00:31:25,617 --> 00:31:28,882
A 500-yen coin and...

225
00:31:32,257 --> 00:31:36,819
What's this era?.

226
00:31:39,564 --> 00:31:43,227
This is technology of 2010.

227
00:31:43,568 --> 00:31:45,832
A small radio?

228
00:31:46,237 --> 00:31:47,932
A mobile.

229
00:31:48,172 --> 00:31:50,140
Mobile?

230
00:31:50,875 --> 00:31:54,140
It's mainly a phone.

231
00:31:54,879 --> 00:31:58,474
No signals, of course.

232
00:31:59,050 --> 00:32:01,348
It's also...

233
00:32:02,253 --> 00:32:06,246
a camera. Cool, huh?

234
00:32:10,395 --> 00:32:15,856
A multiple communication device.

235
00:32:16,134 --> 00:32:19,467
I use this in my SF movie.

236
00:32:19,871 --> 00:32:26,106
You can transmit and record
messages from anywhere.

237
00:32:26,377 --> 00:32:30,177
My invention, right in my hands...

238
00:32:32,283 --> 00:32:35,116
It hit the spot.

239
00:32:35,553 --> 00:32:40,217
So you believe me?
You're an SF geek, right?

240
00:32:40,391 --> 00:32:42,382
Geek?.

241
00:32:42,593 --> 00:32:43,560
You.

242
00:32:44,028 --> 00:32:46,189
Ryota, let me stay with you.

243
00:32:46,431 --> 00:32:48,592
"Ryota"?... No way.

244
00:32:49,033 --> 00:32:53,333
I'll clean, wash, and cook.

245
00:32:53,638 --> 00:32:57,404
I won't get in the way, so please.

246
00:33:01,379 --> 00:33:02,937
Wait.

247
00:33:03,147 --> 00:33:05,172
I have to do this.

248
00:33:05,483 --> 00:33:08,543
Give a message to Kazuo...

249
00:33:09,053 --> 00:33:13,285
and go back to my mom.

250
00:33:15,560 --> 00:33:17,994
If I find him...

251
00:33:18,396 --> 00:33:21,092
and give him the message.

252
00:33:22,600 --> 00:33:28,038
she might wake up again.

253
00:33:28,306 --> 00:33:32,037
So, please.

254
00:33:33,277 --> 00:33:35,472
Help me find him.

255
00:33:57,435 --> 00:33:59,300
I'm sorry.

256
00:34:53,658 --> 00:34:55,285
Come in.

257
00:35:14,779 --> 00:35:17,213
What are you doing?

258
00:35:19,517 --> 00:35:21,417
See.

259
00:35:22,186 --> 00:35:23,084
What's up?

260
00:35:23,321 --> 00:35:26,552
You tell us.

261
00:35:27,258 --> 00:35:28,816
Hello. - Hello.

262
00:35:29,393 --> 00:35:31,156
She's cute.

263
00:35:31,529 --> 00:35:33,690
Your type, huh?

264
00:35:34,132 --> 00:35:35,429
No...

265
00:35:36,267 --> 00:35:39,293
I'm his cousin Akari.

266
00:35:39,604 --> 00:35:44,564
Yeah... my cousin.
She's here visiting colleges.

267
00:35:44,876 --> 00:35:48,175
Great! Do you have a boyfriend?

268
00:35:48,679 --> 00:35:50,704
He's Kadoi. I'm Natsuko.

269
00:35:51,215 --> 00:35:53,240
We're in Ryota's movie.

270
00:35:53,618 --> 00:35:56,519
We're just across the hall.

271
00:36:01,292 --> 00:36:03,658
Come if you want.

272
00:36:10,635 --> 00:36:12,728
Natsuko...

273
00:36:17,375 --> 00:36:18,808
I think...

274
00:36:25,783 --> 00:36:26,875
Here.

275
00:36:28,386 --> 00:36:30,217
Natsuko Ichise.

276
00:36:32,323 --> 00:36:37,556
"The actress most likely
to hold a grudge"

277
00:36:37,929 --> 00:36:40,159
That's her.

278
00:36:47,638 --> 00:36:51,199
He should be in high school.

279
00:36:52,210 --> 00:36:56,772
Which one though...?

280
00:36:57,915 --> 00:37:03,945
Is there a famous director
called Ryota Mizorogi?

281
00:37:06,524 --> 00:37:10,324
I don't know about movies.

282
00:37:17,501 --> 00:37:18,729
His address?

283
00:37:19,503 --> 00:37:21,232
I don't know.

284
00:37:24,342 --> 00:37:28,335
Using a red towel as a scarf.

285
00:37:28,546 --> 00:37:29,410
What's that?

286
00:37:29,814 --> 00:37:31,941
A hit song, "Kanda River".

287
00:37:32,416 --> 00:37:35,351
Let's walk from the bathhouse.

288
00:37:35,453 --> 00:37:36,715
I want a bath.

289
00:37:36,921 --> 00:37:38,582
There isn't one.

290
00:37:38,923 --> 00:37:40,754
No bath?

291
00:37:41,225 --> 00:37:43,625
Then let's go to the bathhouse.

292
00:37:43,794 --> 00:37:47,321
No bathing for another 2 days.

293
00:37:48,432 --> 00:37:49,763
You don't bathe everyday?

294
00:37:50,001 --> 00:37:52,492
That's a waste.
Plus, it's winter.

295
00:37:52,770 --> 00:37:54,465
The season matters?

296
00:37:54,805 --> 00:37:57,330
Use the communal sink.

297
00:37:58,576 --> 00:38:00,601
In the hallway?

298
00:38:05,416 --> 00:38:06,974
Forget it.

299
00:38:26,337 --> 00:38:30,797
Sleep under the kotatsu.
It's heated.

300
00:38:59,470 --> 00:39:01,301
Sorry.

301
00:39:29,867 --> 00:39:33,132
"Setagaya Junior High School"

302
00:39:42,713 --> 00:39:44,806
We're clear.

303
00:39:46,016 --> 00:39:49,952
Class 3C...

304
00:39:50,921 --> 00:39:53,685
Here it is.

305
00:39:54,024 --> 00:39:57,516
Kazuko Yoshiyama...

306
00:39:57,695 --> 00:39:58,923
Here.

307
00:39:59,163 --> 00:40:00,528
Your mom?

308
00:40:01,365 --> 00:40:02,889
Yeah.

309
00:40:05,035 --> 00:40:09,938
Where's Kazuo Fukamachi?

310
00:40:13,677 --> 00:40:15,440
He's not here.

311
00:40:15,646 --> 00:40:17,079
Let me see.

312
00:40:30,961 --> 00:40:33,020
Nope.

313
00:40:34,565 --> 00:40:35,964
But look.

314
00:40:36,400 --> 00:40:41,702
They're both wearing 3C class badges.

315
00:40:41,972 --> 00:40:44,634
Weird...

316
00:40:48,612 --> 00:40:51,706
He's not a ghost, is he?

317
00:40:57,988 --> 00:41:00,388
I have an idea.

318
00:41:02,460 --> 00:41:05,156
Let's go ask my mom.

319
00:41:09,233 --> 00:41:10,530
Here's her address.

320
00:41:11,101 --> 00:41:12,966
Hello.

321
00:41:13,504 --> 00:41:15,131
Hello.

322
00:41:19,076 --> 00:41:21,544
Is anyone there?

323
00:41:37,061 --> 00:41:38,756
Uncle Goro?

324
00:41:40,698 --> 00:41:42,165
Uncle?

325
00:41:43,501 --> 00:41:46,129
So you're Kazuko's cousin.

326
00:41:46,704 --> 00:41:48,467
Yeah...

327
00:41:49,807 --> 00:41:51,104
You're the big brother.

328
00:41:51,275 --> 00:41:52,606
Yes...

329
00:41:53,944 --> 00:41:56,174
You don't look alike.

330
00:41:57,648 --> 00:42:00,640
You surprised me when
you called me "uncle. "

331
00:42:01,018 --> 00:42:02,679
Sorry.

332
00:42:03,187 --> 00:42:09,023
Kazuko always talks about you
so I feel like I know you.

333
00:42:09,293 --> 00:42:11,727
She talks about me?!

334
00:42:13,864 --> 00:42:19,063
It seems like nobody's
home right now.

335
00:42:19,537 --> 00:42:21,198
They moved to Yokohama.

336
00:42:21,539 --> 00:42:23,939
When?.

337
00:42:24,174 --> 00:42:28,110
When she entered high school.
You didn't know?

338
00:42:29,146 --> 00:42:32,582
We haven't been in touch.

339
00:42:32,783 --> 00:42:34,683
Oh yeah...

340
00:42:37,288 --> 00:42:43,523
Do you know him?.

341
00:42:44,295 --> 00:42:47,128
Never seen him but
he's wearing our uniform.

342
00:42:47,598 --> 00:42:49,293
Oh...

343
00:42:50,568 --> 00:42:54,060
What's Kazuko's school and address?

344
00:42:54,738 --> 00:42:58,105
I have them at home.

345
00:42:58,309 --> 00:43:01,608
I'll be off work tonight
so call me.

346
00:43:01,879 --> 00:43:07,647
Asakura Liquor Shop.
Dial "D-R-I-N-K-U-P."

347
00:43:08,118 --> 00:43:11,019
See ya.

348
00:43:13,591 --> 00:43:15,957
"Drink up"?

349
00:43:19,263 --> 00:43:21,697
We have some time.

350
00:43:22,600 --> 00:43:26,559
Can I take you somewhere?

351
00:43:44,355 --> 00:43:46,823
"Moving Picture Club"

352
00:43:58,002 --> 00:43:59,833
What is it?

353
00:44:00,104 --> 00:44:03,767
A flying car of the future
in year 2011.

354
00:44:04,274 --> 00:44:06,003
Start.

355
00:44:12,616 --> 00:44:15,710
The Planet of Light.

356
00:44:16,220 --> 00:44:20,179
The earth began spinning off its axis,
losing its field of gravity.

357
00:44:20,424 --> 00:44:23,257
Fundamental changes occurred
in the earth's crust.

358
00:44:23,427 --> 00:44:29,161
The artist, Hiro, painted a cherry tree
with 100 buds on the wall.

359
00:44:29,333 --> 00:44:33,292
When these buds blossom,
help will arrive.

360
00:44:33,671 --> 00:44:38,233
They waited, believing it was so.

361
00:44:39,209 --> 00:44:40,972
And...

362
00:44:43,213 --> 00:44:47,274
"Bathhouse"

363
00:44:52,656 --> 00:44:53,918
I go this way.

364
00:44:54,191 --> 00:44:56,091
I go this way.

365
00:44:57,227 --> 00:44:59,320
I'll transform myself
into a girl of the 70s.

366
00:44:59,830 --> 00:45:01,798
Feels wonderful...

367
00:45:07,104 --> 00:45:10,972
Paradise...

368
00:45:48,712 --> 00:45:50,942
Right there.

369
00:45:51,115 --> 00:45:54,744
Feels great.

370
00:45:57,888 --> 00:45:59,480
Hey.

371
00:46:00,491 --> 00:46:04,222
It felt so good.

372
00:46:04,461 --> 00:46:05,723
You took too long.

373
00:46:06,029 --> 00:46:08,827
I'm all warmed up.

374
00:46:09,133 --> 00:46:10,361
I'm cold.

375
00:46:10,768 --> 00:46:12,360
How do I look?

376
00:46:12,803 --> 00:46:16,170
Beautimus, don't you think?

377
00:46:16,440 --> 00:46:20,069
Beautimus?
What language is that?

378
00:46:20,377 --> 00:46:22,277
Beautimus.

379
00:46:23,280 --> 00:46:25,271
It's totally 70s!

380
00:46:29,253 --> 00:46:32,245
Just like that song.

381
00:46:34,558 --> 00:46:37,721
What?

382
00:46:40,364 --> 00:46:44,164
You were staring at me.

383
00:46:44,368 --> 00:46:46,336
No, I wasn't.

384
00:46:46,770 --> 00:46:48,829
Let's go. It's cold.

385
00:47:12,796 --> 00:47:15,230
It's too hard to pick her out.

386
00:47:15,432 --> 00:47:21,200
But she's here for sure.
Let's find her.

387
00:47:36,286 --> 00:47:37,981
Your mom?

388
00:47:47,397 --> 00:47:49,456
Excuse me...

389
00:47:49,833 --> 00:47:53,064
I'm looking for someone.

390
00:47:53,337 --> 00:47:57,398
A guy named Kazuo Fukamachi.

391
00:47:57,841 --> 00:47:59,900
Kazuo Fukamachi?

392
00:48:00,110 --> 00:48:05,377
Your classmate in junior high.

393
00:48:09,353 --> 00:48:11,821
That's me?

394
00:48:13,257 --> 00:48:17,318
You know him, right?

395
00:48:17,628 --> 00:48:21,462
No. I'm sorry.

396
00:48:23,000 --> 00:48:26,458
I don't remember taking this photo either.

397
00:48:28,372 --> 00:48:32,502
But... you're in it with him.

398
00:48:33,043 --> 00:48:35,136
Same class badge...

399
00:48:50,260 --> 00:48:52,319
I'm sorry.

400
00:48:56,433 --> 00:48:59,163
I'll come back tomorrow.

401
00:49:00,037 --> 00:49:04,974
Maybe you'll remember something.

402
00:49:05,542 --> 00:49:07,066
Goodbye.

403
00:49:16,687 --> 00:49:21,147
Your mom doesn't even
know him. We're stumped.

404
00:49:22,192 --> 00:49:25,059
He really might be a ghost.

405
00:49:26,096 --> 00:49:28,155
Absolutely not.

406
00:49:28,365 --> 00:49:32,495
She made the liquid to
come see him herself.

407
00:49:34,204 --> 00:49:35,899
You're right.

408
00:49:38,408 --> 00:49:44,369
Can we go visit someone?

409
00:50:03,600 --> 00:50:08,628
Gotetsu... It's Ryota.

410
00:50:19,316 --> 00:50:21,113
Oh, you were here.

411
00:50:21,284 --> 00:50:23,309
Hi.

412
00:50:24,621 --> 00:50:27,021
Does everybody do this?

413
00:50:27,257 --> 00:50:29,487
Your girlfriend?

414
00:50:29,726 --> 00:50:34,095
No. My cousin Akari.

415
00:50:34,431 --> 00:50:35,295
Hello.

416
00:50:35,499 --> 00:50:39,196
Akari... "Light"... Nice name.

417
00:50:39,436 --> 00:50:43,429
Light is essential to good shots.

418
00:50:45,075 --> 00:50:47,703
Please get dressed.

419
00:50:48,111 --> 00:50:50,773
This is Gotetsu, the cameraman.

420
00:50:52,549 --> 00:50:54,414
Gotetsu?

421
00:50:54,584 --> 00:50:57,747
His nickname for staying up

422
00:50:58,221 --> 00:51:01,554
five straight nights gambling.

423
00:51:01,758 --> 00:51:03,521
Hey there.

424
00:51:08,265 --> 00:51:09,698
Well drawn.

425
00:51:09,800 --> 00:51:11,461
Good, huh?

426
00:51:25,282 --> 00:51:26,544
You okay?

427
00:51:26,817 --> 00:51:28,250
Don't you get it?

428
00:51:28,552 --> 00:51:31,749
My dick won't get hard
with a drawing like this.

429
00:51:32,289 --> 00:51:34,120
Make it hard.

430
00:51:34,357 --> 00:51:35,415
It'll get hard.

431
00:51:35,725 --> 00:51:40,458
Gimme a hard-on.

432
00:51:59,549 --> 00:52:02,848
Shit! My camera!

433
00:52:04,588 --> 00:52:08,149
Get a rag.

434
00:52:11,261 --> 00:52:11,556
Wipe it.

435
00:52:11,761 --> 00:52:13,194
Ryota...

436
00:52:46,163 --> 00:52:49,189
Do you remember anything?

437
00:52:55,438 --> 00:52:57,599
I'm sorry I can't help you.

438
00:53:00,277 --> 00:53:06,477
Is there something between
me and him in the photo?

439
00:53:10,287 --> 00:53:14,121
Rather a lot...

440
00:53:16,459 --> 00:53:20,759
So, why a school in Yokohama?

441
00:53:21,231 --> 00:53:23,756
I want to go into pharmacy studies,

442
00:53:24,167 --> 00:53:30,367
and this school has
a strong science course.

