﻿2
00:03:32,707 --> 00:03:34,299
Ari, dear.

3
00:03:34,667 --> 00:03:38,580
Will you please get out of bed.
We need to get going.

4
00:03:45,947 --> 00:03:49,656
We can't depart like this.

5
00:03:52,267 --> 00:03:53,939
Ari...

6
00:03:54,747 --> 00:03:57,625
I don't want to move.

7
00:03:57,707 --> 00:04:02,462
I know, but I'm going to places
that aren't for children.

8
00:04:02,507 --> 00:04:05,817
I wouldn't be able to
take care of you.

9
00:04:07,427 --> 00:04:10,225
You won't just be with your dad.

10
00:04:10,267 --> 00:04:14,180
Your granny is there too
and she's dying to see you.

11
00:04:15,347 --> 00:04:19,056
Please get up, dear.

12
00:04:29,587 --> 00:04:33,102
There's no room for the bike in the car.
-I'm not going without it.

13
00:04:33,187 --> 00:04:37,817
There's no room for it.
Put it in the truck.

14
00:05:27,347 --> 00:05:29,258
Come here, darling.

15
00:05:32,427 --> 00:05:35,385
I'm going to miss you so much,
you know that.

16
00:05:36,467 --> 00:05:41,939
I'll call you later.
It will be fine, I promise.

17
00:06:24,067 --> 00:06:30,336
<b>SPARROWS</b>

18
00:07:53,667 --> 00:07:56,625
Hi buddy.
Hi.

19
00:07:58,827 --> 00:08:02,422
Is that all you've got?
-Yes.

20
00:08:21,267 --> 00:08:23,497
How was the flight?

21
00:08:23,707 --> 00:08:26,460
It was fine.

22
00:08:27,867 --> 00:08:30,222
No turbulence?

23
00:08:30,707 --> 00:08:33,938
No, just a bit.

24
00:08:46,867 --> 00:08:51,816
The tunnel had opened before you moved ?
-No.

25
00:08:52,867 --> 00:08:54,778
Are you sure?

26
00:08:54,907 --> 00:08:58,616
Yeah, I'm certain.

27
00:08:59,427 --> 00:09:03,705
Makes the journey
a lot shorter.

28
00:09:46,387 --> 00:09:51,063
Now we live here.
-I know, Mom told me.

29
00:09:53,667 --> 00:09:57,137
Yes... Hello.

30
00:09:57,867 --> 00:10:00,461
I'm on my way.

31
00:10:14,547 --> 00:10:16,583
We eat at Mom's at 7.

32
00:10:16,787 --> 00:10:19,824
But rush over to her
because she's waiting for you.

33
00:10:19,947 --> 00:10:22,541
I have to get back to work.

34
00:10:22,667 --> 00:10:25,261
You have the second room upstairs.

35
00:10:25,547 --> 00:10:28,823
Everything alright?
-OK.

36
00:11:42,987 --> 00:11:50,667
Oh, my darling boy.
My dearest, darling boy.

37
00:11:51,027 --> 00:11:55,259
It's so good to see you.
-Hi Grandma.

38
00:11:55,947 --> 00:11:59,906
It's been ages since
I've seen you, dear.

39
00:12:00,107 --> 00:12:04,146
The last time was
the Christmas concert on TV.

40
00:12:04,627 --> 00:12:08,506
You sang so beautifully.
-Yeah, thanks.

41
00:12:10,427 --> 00:12:12,179
How was Spain?

42
00:12:12,307 --> 00:12:17,256
Fine. We old ladies
drank quite a lot, though.

43
00:12:23,627 --> 00:12:29,418
Would you like some coffee?
-No thanks, I don't drink coffee.

44
00:12:29,747 --> 00:12:33,581
No, he doesn't drink coffee.

45
00:12:35,027 --> 00:12:39,543
Your mom called earlier.
Did she reach you?

46
00:12:40,147 --> 00:12:41,739
Yes.

47
00:12:42,107 --> 00:12:44,257
That was nice.

48
00:12:44,307 --> 00:12:47,379
I spoke to her husband
the other day.

49
00:12:47,507 --> 00:12:50,704
What a charming man.

50
00:12:52,027 --> 00:12:54,541
And they've moved to Angola.

51
00:12:54,707 --> 00:12:56,459
No, Uganda.

52
00:12:56,507 --> 00:13:02,025
And then they go on to Kenya, Ethiopia
and a few other places.

53
00:13:02,587 --> 00:13:08,935
I don't care what your father says.
I admire your mothers energy.

54
00:13:15,947 --> 00:13:19,337
Mmm... Good.

55
00:13:21,987 --> 00:13:26,105
I walked by
our old house earlier on.

56
00:13:26,227 --> 00:13:29,299
You did?
-Yeah.

57
00:13:30,747 --> 00:13:34,057
Has the house been for sale long?

58
00:13:34,747 --> 00:13:42,142
Yes, since the bank
took it. And the boat.

59
00:13:42,427 --> 00:13:46,136
Please have some more to eat, boys.

60
00:13:48,147 --> 00:13:52,902
Would you pass me the jam?
-Sure, there you go.

61
00:13:54,867 --> 00:13:59,065
Hey, I had a word with Diddi
about you earlier.

62
00:14:00,227 --> 00:14:03,936
So, he's expecting you
for work tomorrow morning.

63
00:14:04,307 --> 00:14:07,902
Work?
-Yes, work.

64
00:14:08,787 --> 00:14:12,575
Did you plan to stay in bed
until school starts?

65
00:14:13,307 --> 00:14:14,342
No.

66
00:14:14,467 --> 00:14:18,745
Could you pass me back the jam?
-Sorry.

67
00:14:36,787 --> 00:14:41,417
It doesn't stay this bright
at night in Reykjavík?

68
00:14:41,747 --> 00:14:43,339
No.

69
00:14:48,107 --> 00:14:52,180
We are a lot farther north here.

70
00:15:04,987 --> 00:15:08,423
Greetings.
-Hi.

71
00:15:08,907 --> 00:15:12,946
Hi. Dagur.
-Ari.

72
00:15:13,867 --> 00:15:17,462
Welcome to the West.
-Thanks.

73
00:15:20,587 --> 00:15:25,422
I'm going upstairs.
-Alright mate.

74
00:15:30,507 --> 00:15:34,705
Look, the rules have changed.

75
00:15:35,027 --> 00:15:38,064
The boy is living with
me now. Get it?

76
00:15:38,147 --> 00:15:40,536
Yes, yes.

77
00:15:50,547 --> 00:15:55,063
You can't just waltz
in and out of here anymore.

78
00:15:55,187 --> 00:15:56,381
No, no, no.

79
00:16:31,507 --> 00:16:36,262
You have a cigarette?
-Yes, of course.

80
00:16:38,947 --> 00:16:42,576
This is my new assistant.

81
00:16:42,907 --> 00:16:47,537
He is as strong
as a 6-year old girl.

82
00:16:47,827 --> 00:16:50,057
See you later.

83
00:17:39,027 --> 00:17:41,825
Hi.
-Hello.

84
00:17:50,027 --> 00:17:52,143
Hi.

85
00:17:53,627 --> 00:17:55,697
Hi.

86
00:17:56,427 --> 00:17:59,747
Wow! I can't believe it.
Great to see you.

87
00:17:59,747 --> 00:18:01,942
You too.

88
00:18:02,147 --> 00:18:05,344
Been here long?
-No, just got here yesterday.

89
00:18:05,387 --> 00:18:07,537
Stopping long?

90
00:18:07,627 --> 00:18:10,664
Well, I've actually moved back.
-OK.

91
00:18:10,747 --> 00:18:13,500
I'm working at the
fish factory this summer.

92
00:18:13,587 --> 00:18:15,976
Then I'll be going to
school here in the autumn.

93
00:18:16,067 --> 00:18:18,376
Hi.
-Hi.

94
00:18:23,307 --> 00:18:28,301
This is Einar, my boyfriend.
This is Ari, a childhood friend.

95
00:18:28,347 --> 00:18:30,781
Hello.
-Hello.

96
00:18:30,987 --> 00:18:33,547
Shouldn't we get going?
-Yes.

97
00:18:33,627 --> 00:18:35,982
See you.
-Yes.

98
00:18:36,387 --> 00:18:38,343
Bye.

99
00:18:44,467 --> 00:18:46,264
Hi.
-Hi.

100
00:18:46,307 --> 00:18:51,700
Hey, was that little Lára out there?

101
00:18:51,827 --> 00:18:54,500
What? Yes.

102
00:18:55,027 --> 00:18:59,782
Really? She's matured quite a bit!
-Dad, please!

103
00:19:00,227 --> 00:19:04,300
Don't be so touchy.

104
00:19:19,107 --> 00:19:24,056
How long has this foreigner that your
mother married been in Iceland?

105
00:19:24,267 --> 00:19:28,863
About eight years.
-Eight years?

106
00:19:30,587 --> 00:19:35,661
I tried calling your mother
and he answered the phone.

107
00:19:35,987 --> 00:19:39,343
Doesn't he speak any Icelandic?

108
00:19:39,467 --> 00:19:43,460
Yes, he does.
He just has an accent.

109
00:19:43,587 --> 00:19:47,660
We always speak in Icelandic.

110
00:19:48,067 --> 00:19:53,619
Well, I couldn't understand
a single word he said.

111
00:20:02,747 --> 00:20:07,901
Been long since you went hunting?
-Yes, it's been a while.

112
00:20:09,267 --> 00:20:11,986
Doesn't the Dane
take you out shooting?

113
00:20:12,067 --> 00:20:15,218
No, he's a vegetarian.

114
00:20:16,267 --> 00:20:19,577
A vegetarian, eh?
-Yes.

115
00:20:22,867 --> 00:20:27,497
How about we go hunting soon.

116
00:20:27,587 --> 00:20:33,423
Sure.
-Great, it's a deal then.

117
00:20:38,387 --> 00:20:43,745
Greetings. How's things?
-Just ding dong.

118
00:20:51,747 --> 00:20:57,185
Did you hear about Siggi?
-What has he done now?

119
00:20:57,787 --> 00:21:01,336
He screwed it all up again.
-Not again?

120
00:21:01,507 --> 00:21:04,260
The boat was burning oil
when he went out this morning.

121
00:21:04,307 --> 00:21:07,105
But he went anyway.

122
00:21:07,907 --> 00:21:11,263
The man is mentally challenged.

123
00:21:12,307 --> 00:21:16,016
You missed!
-Fuck off.

124
00:21:22,867 --> 00:21:26,177
Going somewhere?
-Yes, I'm just going out for a bit.

125
00:21:26,267 --> 00:21:29,179
Join the game?
-No thanks.

126
00:21:29,347 --> 00:21:33,260
Come on, join us. Ari?

127
00:21:33,347 --> 00:21:35,986
Hello Ari.
-Hi.

128
00:21:37,747 --> 00:21:40,784
Welcome home.
-Thanks.

129
00:21:42,707 --> 00:21:44,902
Good to see you.
-Yes. Bye.

130
00:21:44,987 --> 00:21:47,581
Hi boys.
-Hi.

131
00:22:08,427 --> 00:22:11,021
Oh, fuck!

132
00:22:11,427 --> 00:22:12,985
What are you doing, man?

133
00:22:13,027 --> 00:22:16,303
Stop it! Stop!

134
00:22:17,987 --> 00:22:20,740
Fuck! Stop!

135
00:22:24,067 --> 00:22:27,855
You two, stop this bullshit!

136
00:22:27,907 --> 00:22:28,817
Yes.

137
00:22:28,907 --> 00:22:31,944
Right now I said!
-Yes. Sorry.

138
00:22:34,467 --> 00:22:36,264
Fucking moron!

139
00:22:36,387 --> 00:22:40,619
Sorry, man.
I went a bit far, maybe.

140
00:22:40,907 --> 00:22:44,946
Retard!
-You alright?

141
00:22:45,947 --> 00:22:50,145
Are we cool then?
Dude?

142
00:22:50,547 --> 00:22:52,777
-Yes, dude.

143
00:22:53,067 --> 00:22:58,221
I'm a stinking mess.
I'm off to the pool, wanna come?

144
00:22:58,307 --> 00:23:01,140
Sure.
I'm going to finish here.

145
00:23:01,267 --> 00:23:04,020
OK. About half an hour?

146
00:23:04,107 --> 00:23:06,575
See you later.
-Yes.

147
00:24:08,667 --> 00:24:10,737
Here.

148
00:24:15,107 --> 00:24:17,257
You can pat him first.

149
00:24:19,547 --> 00:24:21,902
Wow!

150
00:24:22,267 --> 00:24:26,101
Cool, eh?
-Yes, very cool.

151
00:24:32,427 --> 00:24:34,702
Hello dude.

152
00:24:37,627 --> 00:24:40,983
He's so calm.
-Yes, he likes you.

153
00:24:41,067 --> 00:24:42,546
Yes.

154
00:24:45,667 --> 00:24:48,500
Really cute.
-Yeah.

155
00:25:06,547 --> 00:25:09,857
Are you going somewhere?
-Yes.

156
00:25:10,067 --> 00:25:13,025
Aren't you going to watch
the show with us?

157
00:25:13,427 --> 00:25:15,099
-No, I'm going out for a while.

158
00:25:15,187 --> 00:25:17,747
Don't come home too late.
-No, no.

159
00:25:17,787 --> 00:25:20,665
Thanks for dinner, Grandma.
-Yes.

160
00:25:29,827 --> 00:25:33,820
How's it going between
the two of you, dear?

161
00:25:33,947 --> 00:25:36,336
I don't know.

162
00:25:37,667 --> 00:25:42,104
I don't think he gets me
anymore than I get him.

163
00:25:45,347 --> 00:25:49,579
You might have waited until
I've finished eating?

164
00:25:50,747 --> 00:25:55,298
Want me to put it out?
-No, no.

165
00:26:23,907 --> 00:26:27,502
What's going on?
-Midsummer Night's party in the valley.

166
00:26:27,947 --> 00:26:31,781
Have you got a ride?
-Yes, with Robbi.

167
00:26:31,867 --> 00:26:34,461
Do you think there is room for me?

168
00:26:34,547 --> 00:26:38,256
I don't think so...
The car is full.

169
00:26:39,147 --> 00:26:41,741
Yes, I have to go.
Try one of the others.

170
00:26:41,867 --> 00:26:43,585
OK.

171
00:26:43,947 --> 00:26:46,381
Hurry up.

172
00:26:56,067 --> 00:27:00,379
No chance I can ride with you?
-There's only room for two.

173
00:27:00,787 --> 00:27:02,584
Please.

174
00:27:02,707 --> 00:27:06,336
You can jump on the back
if you want.

175
00:27:07,307 --> 00:27:10,743
Would that be okay?
-Yeah, hurry up then. We're leaving.

176
00:28:56,267 --> 00:28:58,098
Hey, fuck!
-Sorry.

177
00:28:58,227 --> 00:29:02,015
What were you thinking?
-I'm sorry, I didn't know.

178
00:29:02,067 --> 00:29:04,900
Hey! What's going on?

179
00:29:05,067 --> 00:29:08,616
Spying on my girlfriend?
What kind of pervert are you?

180
00:29:09,747 --> 00:29:14,104
Fucking creep!
Watching her pee!

181
00:29:16,067 --> 00:29:19,264
Stay away from her!

182
00:29:19,427 --> 00:29:22,260
Or I'll fucking kill you!

183
00:29:22,427 --> 00:29:24,702
Leave him alone!
He's just a friend.

184
00:29:25,027 --> 00:29:26,699
I will kill him!

185
00:29:27,267 --> 00:29:30,179
Are you ok?
-Forget him!

186
00:29:30,227 --> 00:29:33,902
You are drunk.
-You're fucking dead, creep!

187
00:29:35,387 --> 00:29:39,426
He's just a friend!
-I'll kill you, dude.

188
00:29:56,147 --> 00:29:58,741
How could he know she was peeing?

189
00:29:58,867 --> 00:30:02,746
It was obvious! She handed me
her beer and walked away.

190
00:30:02,907 --> 00:30:05,262
He's a fucking pervert.

191
00:30:05,467 --> 00:30:11,383
Even if he was,
can't you just leave him alone?

192
00:30:11,507 --> 00:30:16,820
If he leaves her alone.
-He just arrived, give him a break.

193
00:30:16,987 --> 00:30:22,459
He's a fucking pervert!
-Okay, can't we all just be friends?

194
00:30:22,627 --> 00:30:24,982
Are we good?
-Yeah, we're good.

195
00:30:25,147 --> 00:30:30,460
Just tell him to leave her alone.
-You leave him alone, he leaves Lára alone.

196
00:30:32,187 --> 00:30:34,747
He better obeys.

197
00:30:37,707 --> 00:30:39,857
Hi.

198
00:30:43,947 --> 00:30:45,346
What were you thinking.

199
00:30:45,467 --> 00:30:51,542
I wasn't thinking, what was he thinking?
-Do you know how bad this looks for you?

200
00:30:51,707 --> 00:30:56,497
I don't give a shit.
-Maybe you don't, but I do.

201
00:30:57,147 --> 00:31:01,743
At least I've talked to Einar.
He's going to leave you alone.

202
00:31:01,947 --> 00:31:06,896
But you have to stay away from Lára.
OK?

203
00:31:07,067 --> 00:31:13,939
I'm going to kill him.
-No, Ari. Listen, leave Lára alone!

204
00:31:14,227 --> 00:31:15,740
OK?

205
00:31:15,827 --> 00:31:18,057
Yes.
-Then this is over.

206
00:31:18,147 --> 00:31:21,583
Great.
-Great.

207
00:31:22,747 --> 00:31:25,341
Very good.

208
00:31:46,467 --> 00:31:49,459
Ari. Ari. Come on.

209
00:31:49,627 --> 00:31:56,226
What have you done?
Come on, let's get you home.

210
00:32:35,187 --> 00:32:37,337
I'll take the boy
to his grandmother's.

211
00:32:37,427 --> 00:32:43,377
Get the fuck out of here, you slut!
Fuck off! Out!

212
00:33:21,747 --> 00:33:25,979
When she finally got him home,
your condition was not better.

213
00:33:26,067 --> 00:33:30,822
Partying all night, and Dagur
screwing Vera on the sofa!

214
00:33:31,027 --> 00:33:34,064
Do you believe that hysterical bitch?

215
00:33:34,147 --> 00:33:39,267
You should be ashamed of yourself!.
What kind of role model are you?

216
00:33:39,627 --> 00:33:41,106
And you're still drunk.
-Stop it, Mom.

217
00:33:41,307 --> 00:33:47,143
Isn't it time you stop this self-pity
that has plagued you since the divorce?

218
00:33:47,587 --> 00:33:52,263
Wake up to the fact that you've
barely seen your son for years?

219
00:33:52,347 --> 00:33:57,102
You should set an example for him.
You're getting a second chance.

220
00:33:57,267 --> 00:33:59,861
Why can't you understand that?

221
00:34:00,867 --> 00:34:02,858
Man up!

222
00:34:03,547 --> 00:34:09,383
Do yourself and everyone else a favour
and go back to those AA meetings.

223
00:34:33,467 --> 00:34:36,937
FOR SALE.

224
00:36:25,587 --> 00:36:28,863
Hi mom, it's me.

225
00:36:31,067 --> 00:36:35,379
No, I don't know the time difference.

226
00:36:36,307 --> 00:36:42,382
No. This place sucks
and I don't want to be here.

227
00:36:43,467 --> 00:36:48,018
Dad? Yes, Dad's fine.
I don't know anybody.

228
00:36:48,187 --> 00:36:51,020
I just want to go home.

229
00:36:55,907 --> 00:37:00,025
No, I am not going to school here.

230
00:37:02,307 --> 00:37:05,583
Why can't I just be with you?

231
00:37:07,107 --> 00:37:11,339
Oh mom, please just come home.

232
00:37:13,587 --> 00:37:15,623
I don't care.

233
00:37:15,747 --> 00:37:21,697
No. It won't get better.
It will not get any fucking better.

234
00:37:21,827 --> 00:37:23,579
No! No!

235
00:37:23,987 --> 00:37:28,697
Fuck you! Do you hear me?
Fuck you!

236
00:38:07,707 --> 00:38:12,064
What's this?
A knitting club?

237
00:38:17,267 --> 00:38:20,304
I'm putting this in the sink.

238
00:38:20,547 --> 00:38:23,584
Mom, is there any coffee?
-No, but I can make some.

239
00:38:23,707 --> 00:38:25,698
No, I'll get some at work.

240
00:38:25,747 --> 00:38:29,581
Shouldn't we have dinner together?
-Yes.

241
00:38:29,747 --> 00:38:33,137
Bloody noise!

242
00:38:37,467 --> 00:38:41,540
You know he's very proud of you,
don't you?

243
00:38:44,547 --> 00:38:48,745
Did he tell you that we watched your
Christmas concert together?

244
00:38:48,907 --> 00:38:50,943
No.

245
00:38:51,747 --> 00:38:55,706
Don't take all his macho nonsense
too seriously.

246
00:38:55,787 --> 00:38:58,540
It's his handicap.

247
00:39:03,347 --> 00:39:06,498
What have you been up to?

248
00:39:06,827 --> 00:39:10,740
Haven't you been in touch with Lára
since you came?

249
00:39:11,347 --> 00:39:13,702
No, not really.

250
00:39:13,827 --> 00:39:19,538
Really? You were such good friends,
always together.

251
00:39:20,227 --> 00:39:24,300
Yeah. I don't know.
It's a bit complicated.

252
00:39:24,507 --> 00:39:29,456
She's older now.
Got herself a boyfriend and all that.

253
00:39:29,627 --> 00:39:37,056
What difference does that make?
- I don't know. Maybe none.

254
00:40:00,067 --> 00:40:02,376
There's your boy.

255
00:40:03,747 --> 00:40:05,897
Hi.
-Hi.

256
00:40:07,947 --> 00:40:13,897
I was wondering,
do you want to go hunting?

257
00:40:14,867 --> 00:40:16,823
Hunting?

258
00:40:17,107 --> 00:40:20,144
I like the sound of that.

259
00:40:20,267 --> 00:40:24,624
Yes. Let's do it.

260
00:40:25,107 --> 00:40:30,500
I'll borrow a boat, but we need the gun.
It's in the car, the back seat.

261
00:40:30,587 --> 00:40:35,502
Bring the bullets from the grey bag.
-OK.

262
00:41:09,907 --> 00:41:14,503
Hold it tight against your shoulder.
Otherwise it could knock you out.

263
00:41:14,627 --> 00:41:15,980
See?

264
00:41:18,787 --> 00:41:20,823
Understand?
-Yes.

265
00:41:20,947 --> 00:41:22,903
Try it.

266
00:41:24,467 --> 00:41:27,425
What did I say?
Tight against your shoulder.

267
00:41:27,547 --> 00:41:29,503
Or it will hit you.

268
00:41:30,547 --> 00:41:33,186
Hey, look! A seal.

269
00:41:36,267 --> 00:41:40,545
Make eye contact with him
and aim directly at his head.

270
00:41:40,707 --> 00:41:43,426
Otherwise he'll sink.

271
00:41:44,627 --> 00:41:48,302
Now. Now! He's within range.
He's looking at you.

272
00:41:49,947 --> 00:41:51,175
Now!

273
00:41:51,267 --> 00:41:54,065
What's wrong with you?
He'll get away.

274
00:41:54,267 --> 00:41:57,179
Tight against the shoulder.
Aim.

275
00:45:16,547 --> 00:45:18,105
Thank you.

276
00:45:18,867 --> 00:45:21,142
Cheers.
-Cheers.

277
00:46:01,307 --> 00:46:02,865
Dad.

278
00:46:02,907 --> 00:46:05,785
What's the matter?

279
00:46:07,667 --> 00:46:12,457
Come and sit next to me.
-What is it?

280
00:46:14,467 --> 00:46:18,096
I don't want you to go inside.
Come and sit with me.

281
00:46:19,747 --> 00:46:22,341
What's going on?

282
00:46:33,747 --> 00:46:35,658
What happened?

283
00:46:39,187 --> 00:46:42,497
She was just lying there
when I arrived.

284
00:46:44,107 --> 00:46:48,020
There's nothing we can do my boy.

285
00:48:02,627 --> 00:48:04,504
Ari.

286
00:48:12,787 --> 00:48:15,301
Are you alright?

287
00:48:20,667 --> 00:48:24,819
Sorry to hear
about your grandmother.

288
00:53:12,467 --> 00:53:15,106
Hi buddy.
I've been looking for you.

289
00:53:15,227 --> 00:53:16,785
Hi.

290
00:53:17,027 --> 00:53:20,144
I'm preparing the funeral.
Are you coming with me?

291
00:53:20,267 --> 00:53:22,542
Yes.

292
00:53:45,547 --> 00:53:48,823
I thought you were going to the pub.

293
00:53:52,467 --> 00:53:57,495
Yes, I thought so too.

294
00:54:08,307 --> 00:54:11,856
I have to get my act together.

295
00:54:15,427 --> 00:54:18,897
I just wanted to tell you that.

296
00:54:28,747 --> 00:54:33,582
This looks great on you.
A perfect flt.

297
00:54:33,787 --> 00:54:37,666
I think I'll take this one.
-Yes

298
00:54:38,067 --> 00:54:40,786
And take the other one for the boy.
-Yes.

299
00:54:40,867 --> 00:54:44,223
And I need some shoes.
-What size?

300
00:54:44,267 --> 00:54:46,622
45.
-45.

301
00:54:55,747 --> 00:55:00,696
Don't I look like an idiot?
-No, you look fine.

302
00:55:01,627 --> 00:55:06,985
Did you tie it yourself?.
-No, the man gave it to me like that.

303
00:55:07,947 --> 00:55:10,939
Your mother used to do it for me.

304
00:55:19,387 --> 00:55:21,855
You look great.

305
00:55:34,907 --> 00:55:37,899
How is it?
-Good, a bit warm.

306
00:55:49,947 --> 00:55:53,940
This is life.

307
00:55:56,227 --> 00:56:00,266
I felt bad.
I was alone,

308
00:56:00,467 --> 00:56:05,905
but then I started dancing,
and found love.

309
00:56:18,347 --> 00:56:20,941
That's not dancing.

310
00:56:23,027 --> 00:56:25,860
She'll make it to the next round.

311
00:56:25,947 --> 00:56:28,142
Do you think so?

312
00:56:28,747 --> 00:56:31,386
With a big ass like this?

313
00:56:31,547 --> 00:56:35,426
Dad, it's a dance competition
not a beauty contest.

314
00:56:35,867 --> 00:56:39,064
I don't care. It's TV.

315
00:56:40,507 --> 00:56:44,056
Her ass fills up the whole screen.

316
01:00:32,387 --> 01:00:36,300
Earth to earth.

317
01:00:41,667 --> 01:00:45,296
Ashes to ashes.

318
01:00:50,867 --> 01:00:55,224
Dust to dust.

319
01:01:00,347 --> 01:01:05,216
We are leaving, dear.
My condolences for your loss.

320
01:01:05,987 --> 01:01:10,378
Don't be afraid to talk to me
if you need anything.

321
01:01:10,667 --> 01:01:14,103
OK.
-Greet your father.

322
01:01:14,547 --> 01:01:17,857
My condolences.

323
01:01:20,707 --> 01:01:23,096
Bye.

324
01:02:33,027 --> 01:02:36,656
So that's where you're hiding.

325
01:03:03,347 --> 01:03:09,138
It's all very sad.
My condolences, Ari.

326
01:03:14,267 --> 01:03:19,944
But life goes on, dear.
You have to remember that.

327
01:03:23,227 --> 01:03:33,102
Before you know it you'll have kids and
be married to an ugly, old woman like me

328
01:03:46,467 --> 01:03:49,186
You're not ugly.

329
01:03:51,307 --> 01:03:54,822
Thank you, dear.

330
01:06:30,787 --> 01:06:33,745
Do you want to come inside me?

331
01:06:41,627 --> 01:06:44,744
Isn't it good?

332
01:09:41,667 --> 01:09:46,422
Don't you miss the city sometimes?

333
01:09:47,387 --> 01:09:50,299
Yes, sometimes.

334
01:09:52,307 --> 01:09:56,619
I went south with my mom last year.
We saw your concert.

335
01:09:56,667 --> 01:09:59,943
You did?
-It was awesome.

336
01:10:00,027 --> 01:10:01,824
Thanks.

337
01:10:10,427 --> 01:10:13,783
I scored this from my uncle.

338
01:10:24,827 --> 01:10:26,943
Thanks.

339
01:10:39,147 --> 01:10:40,660
Wasted.
-No.

340
01:10:40,707 --> 01:10:43,585
Completely stoned.
-I'm not.

341
01:10:43,747 --> 01:10:46,705
Then you will get
the fucking munchies.

342
01:10:46,907 --> 01:10:49,944
Last time we smoked,
Bassi had five hot dogs.

343
01:10:50,027 --> 01:10:53,099
Five hot dogs?
That's disgusting.

344
01:10:53,187 --> 01:10:55,655
It was awesome.
Believe me.

345
01:10:55,707 --> 01:10:59,461
With buns and everything?
-Yes, it was awesome.

346
01:11:06,547 --> 01:11:10,904
See that spider's web?
-Yuck.

347
01:11:15,427 --> 01:11:17,577
Lára!

348
01:11:20,547 --> 01:11:22,538
What's going on?

349
01:11:24,787 --> 01:11:27,381
Lára!
Where are you going?

350
01:11:27,747 --> 01:11:29,465
Lára!

351
01:11:29,547 --> 01:11:31,856
Lára, come here.

352
01:11:32,067 --> 01:11:34,137
Lára!

353
01:11:37,587 --> 01:11:40,818
What is going on?
-What the hell was that?

354
01:11:41,067 --> 01:11:42,705
Lára!

355
01:11:42,747 --> 01:11:45,580
Einar, leave her alone.

356
01:11:48,147 --> 01:11:51,981
What's he gotten himself into?
-I don't know.

357
01:11:53,387 --> 01:11:57,585
That is so bloody typical for guys.
-What do you mean?

358
01:11:57,667 --> 01:12:02,218
Sex, sex, sex.
All guys think about is sex.

359
01:12:02,427 --> 01:12:04,736
Of course.

360
01:12:04,787 --> 01:12:07,540
Lára split up with Einar because
she didn't love him.

361
01:12:07,627 --> 01:12:12,496
He's upset they never had sex.

362
01:12:12,627 --> 01:12:15,221
What are you saying?
Are you saying...

363
01:12:16,707 --> 01:12:21,098
Einar has never fucked Lára?
-No, she never loved him.

364
01:12:21,267 --> 01:12:24,623
But she could have fucked him.

365
01:12:24,747 --> 01:12:27,739
Shut up, you retard.

366
01:12:29,427 --> 01:12:33,579
I was just teasing her.
Jesus Christ!

367
01:12:35,787 --> 01:12:37,823
What was that?

368
01:14:05,547 --> 01:14:09,859
Ari. Ari!

369
01:14:13,267 --> 01:14:14,666
Ari!

370
01:14:17,147 --> 01:14:18,546
What?

371
01:14:18,587 --> 01:14:21,147
Where were you last night?
-What do you mean?

372
01:14:21,307 --> 01:14:24,697
You heard me.
Where were you last night?

373
01:14:24,867 --> 01:14:27,665
I was right here.
-You're lying.

374
01:14:27,827 --> 01:14:32,298
Really? Or were you so drunk
that you can't remember?

375
01:14:32,387 --> 01:14:35,823
You're nothing
but a drunk and a loser.

376
01:14:35,987 --> 01:14:41,744
You lost Mom, the boat, the house,
everything. You're a fucking loser!

377
01:14:42,347 --> 01:14:45,657
You hardly saw me,
or spoke to me for years.

378
01:14:45,747 --> 01:14:47,817
Six years.

379
01:14:49,267 --> 01:14:51,986
You never remembered
my birthday.

380
01:14:52,107 --> 01:14:55,543
I'm ashamed of being your son.
Do you hear me?

381
01:14:56,187 --> 01:14:59,463
You're a fucking loser.

382
01:15:48,787 --> 01:15:51,904
I was thinking of frying
some fish tonight.

383
01:15:52,187 --> 01:15:54,303
I'm not hungry.

384
01:15:55,267 --> 01:15:58,976
This should cover the beer.
-You don't have to do that.

385
01:15:59,107 --> 01:16:02,497
Aren't you going to eat?
-No, I'm not hungry.

386
01:16:03,187 --> 01:16:04,745
OK.

387
01:16:16,947 --> 01:16:20,542
"Dear Ari. Best wishes
for all your birthdays. Dad."

388
01:16:34,227 --> 01:16:37,503
Yes, that is fucking nice.
That really is funny.

389
01:16:40,107 --> 01:16:43,099
Yes, it was good.
-Yeah.

390
01:16:43,827 --> 01:16:46,261
My dad would never allow me to.

391
01:16:46,947 --> 01:16:50,098
He goes mad when I drink.

392
01:16:50,387 --> 01:16:51,740
Oh, fuck.

393
01:17:04,627 --> 01:17:07,300
Hi
-Hi.

394
01:17:16,547 --> 01:17:19,539
What an idiot.
-That was so lame.

395
01:17:34,747 --> 01:17:37,215
You're such a pussy.
-What do you mean?

396
01:17:37,307 --> 01:17:40,777
Do something about this.
-Shut up.

397
01:17:41,027 --> 01:17:45,498
Dude. Why don't you screw her?
You know she digs you, go for it.

398
01:17:45,827 --> 01:17:47,863
Shut up.

399
01:17:47,987 --> 01:17:51,946
Seriously.
Have you never had sex?

400
01:17:53,427 --> 01:17:58,103
No.
-What, really?

401
01:17:58,587 --> 01:18:01,545
No
-OK.

402
01:18:01,907 --> 01:18:06,219
I'll hook you up.
Trust me.

403
01:18:06,347 --> 01:18:09,020
What do you mean?
-Trust me.

404
01:18:09,147 --> 01:18:12,503
-Bassi. Hey Bassi. Don't.

405
01:18:39,147 --> 01:18:41,263
Do you have it?

406
01:18:41,467 --> 01:18:46,382
Maybe I should tell your mom...
-Dude, stop it.

407
01:18:46,627 --> 01:18:50,620
How many are you?
-We're four.

408
01:18:54,427 --> 01:18:58,102
Half now and half later.
Break it in two.

409
01:18:58,267 --> 01:19:02,385
Okay.
-Just half now, you got that?

410
01:19:03,067 --> 01:19:06,264
Look what I scored.
- What is that?

411
01:19:06,347 --> 01:19:12,456
It's Ketamine.
Everybody take one.

412
01:27:46,467 --> 01:27:50,426
Was I good?

413
01:28:18,467 --> 01:28:23,382
I'm glad it was with you.

414
01:29:53,867 --> 01:29:58,065
Would you like to meet me tonight?

415
01:30:00,907 --> 01:30:02,977
Yes.

416
01:32:17,886 --> 01:30:45,743
The End @

