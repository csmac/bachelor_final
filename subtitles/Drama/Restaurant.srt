﻿1
00:00:00,000 --> 00:00:04,000
Subtitles by DramaFever

2
00:01:06,240 --> 00:01:11,440
<i>[Restaurant]</i>

3
00:01:13,820 --> 00:01:19,620
<i>Episode 19</i>
[ Stone Pot Jangjorim Bibimbap]

4
00:01:27,110 --> 00:01:30,109
What is the dish that
takes the longest to make here?

5
00:01:30,110 --> 00:01:32,909
<i>It was the first time
I was asked a question like that.</i>

6
00:01:32,910 --> 00:01:36,409
<i>I've been asked which dish could be made
the quickest countless times before.</i>

7
00:01:36,410 --> 00:01:37,809
I'm not sure.

8
00:01:37,810 --> 00:01:39,909
Is it too stupid of a question?

9
00:01:39,910 --> 00:01:45,609
As long as I have the ingredients,
I make whatever you order.

10
00:01:45,610 --> 00:01:48,909
Then, would you be able to make
stone pot jangjorim bibimbap?

11
00:01:48,910 --> 00:01:55,009
Jangjorim takes
quite a long time to make.

12
00:01:55,010 --> 00:01:56,909
Is that the kind of thing you want?

13
00:01:56,910 --> 00:01:57,910
Yes.

14
00:01:57,911 --> 00:01:59,809
It's because I have
a lot of things to think about.

15
00:01:59,810 --> 00:02:01,009
I'm sorry.

16
00:02:01,010 --> 00:02:02,810
You can take your time.

17
00:02:03,610 --> 00:02:04,610
Okay.

18
00:02:08,710 --> 00:02:11,209
<i>While the blood was
being drained from the meat</i>

19
00:02:11,210 --> 00:02:13,510
<i>I decided to boil the quail eggs first.</i>

20
00:02:42,510 --> 00:02:45,509
<i>You just have to boil down the meat
that's ripped up by hand</i>

21
00:02:45,510 --> 00:02:49,810
<i>in a broth made with soy sauce
and starch syrup one more time.</i>

22
00:02:50,510 --> 00:02:53,309
<i>Since it's possible to use it
as a side dish</i>

23
00:02:53,310 --> 00:02:56,410
<i>I tend to make a lot of it at once
when I do make it.</i>

24
00:03:08,910 --> 00:03:10,310
<i>But...</i>

25
00:03:11,110 --> 00:03:13,309
<i>I guess it took too long.</i>

26
00:03:13,310 --> 00:03:17,109
How can she sleep when
such a good smell is filling the air?

27
00:03:17,110 --> 00:03:19,310
She really has a strong mind.

28
00:03:20,510 --> 00:03:22,510
Couldn't she be a girl
who ran away from home?

29
00:03:23,310 --> 00:03:26,210
She looks pretty normal for
someone like that.

30
00:03:28,610 --> 00:03:31,509
I'm a singer. You don't know me?

31
00:03:31,510 --> 00:03:33,409
I'm sorry.

32
00:03:33,410 --> 00:03:36,110
Which part did you start listening from?

33
00:03:53,010 --> 00:03:56,709
<i>Behind the back that's
leaving me right now</i>

34
00:03:56,710 --> 00:04:00,009
<i>I have no choice but to smile.</i>

35
00:04:00,010 --> 00:04:06,909
<i>It's because the fact that
we've come this far is enough for me.</i>

36
00:04:06,910 --> 00:04:13,709
<i>I guess I was too greedy,
when I knew this would happen.</i>

37
00:04:13,710 --> 00:04:19,810
<i>It's late, but I want to say I'm sorry.</i>

38
00:04:26,210 --> 00:04:28,209
It's obvious that you wouldn't know it

39
00:04:28,210 --> 00:04:30,210
because it's a song that
I'm making right now.

40
00:04:38,810 --> 00:04:41,209
Here, you've waited for a long time.

41
00:04:41,210 --> 00:04:42,610
It's hot.

42
00:04:43,810 --> 00:04:46,910
- Stone pot jangjorim bibimbap.
- It looks tasty.

43
00:04:58,410 --> 00:05:00,809
I love this sound.

44
00:05:00,810 --> 00:05:05,310
Strangely, I become calm
when I hear rice cooking.

45
00:05:07,510 --> 00:05:09,310
The sound on a cutting board...

46
00:05:10,110 --> 00:05:13,710
the sound of steam coming from
a high pressure rice cooker, and...

47
00:05:14,210 --> 00:05:16,409
the sound of soup boiling.

48
00:05:16,410 --> 00:05:20,609
The feeling of waiting while
my mom cooked for me when I was young.

49
00:05:20,610 --> 00:05:22,609
When I hear those sounds

50
00:05:22,610 --> 00:05:25,310
I feel like musical motifs
come to mind easily...

51
00:05:25,910 --> 00:05:28,210
and I also feel like
my troubles are being solved too.

52
00:05:29,410 --> 00:05:31,110
I fall asleep easily too.

53
00:05:34,710 --> 00:05:37,609
This is frozen jangjorim soy sauce.

54
00:05:37,610 --> 00:05:41,110
It'll taste pretty good
if you mix it with warm rice and eat it.

55
00:06:09,810 --> 00:06:11,110
It tastes good.

56
00:06:18,530 --> 00:06:21,889
- I'm finished.
- A slim person sure does eat well.

57
00:06:21,890 --> 00:06:23,509
I can't eat because I don't have
anything to eat.

58
00:06:23,510 --> 00:06:26,409
Also, singing actually
burns a lot of calories.

59
00:06:26,410 --> 00:06:27,709
Is that so?

60
00:06:27,710 --> 00:06:30,709
Then, I should go on a singing diet.

61
00:06:30,710 --> 00:06:33,410
I used to be pretty good at singing.

62
00:06:35,910 --> 00:06:43,910
<i>On the street, upon which rains falls,
I think of the sight of you.</i>

63
00:06:49,010 --> 00:06:50,809
Yoo Mi, you're amazing.

64
00:06:50,810 --> 00:06:52,809
- You could sing a duet together.
- What?

65
00:06:52,810 --> 00:06:54,509
Well, we could be
'The Skinny One and the Fat One.'

66
00:06:54,510 --> 00:06:56,010
That would become a hot topic.

67
00:06:59,510 --> 00:07:02,610
I've only done this much
because I fell asleep.

68
00:07:03,910 --> 00:07:06,510
I keep getting stuck here.

69
00:07:09,810 --> 00:07:13,669
- Can I ask you for a favor?
- Yes.

70
00:07:13,670 --> 00:07:17,309
I've wanted to ask you this
for a little while.

71
00:07:17,310 --> 00:07:19,510
Can you lend me this please?

72
00:07:27,510 --> 00:07:30,310
<i>She was a young lady who
was peculiar in many ways.</i>

73
00:07:51,310 --> 00:07:56,309
<i>I wonder where I'm going.</i>

74
00:07:56,310 --> 00:08:01,009
<i>I wonder where I'm approximately at.</i>

75
00:08:01,010 --> 00:08:09,010
<i>There's nothing new about the morning
that I face when I open my eyes anymore.</i>

76
00:08:11,010 --> 00:08:15,809
<i>I wonder if my heart,
which is never satisfied</i>

77
00:08:15,810 --> 00:08:20,809
<i>will become all better
once time passes.</i>

78
00:08:20,810 --> 00:08:23,409
<i>The overflowing number of people.</i>

79
00:08:23,410 --> 00:08:27,710
<i>Standing alone among them,
is a twenty-two...</i>

80
00:08:35,710 --> 00:08:38,410
I'm sorry. I'm sorry.

81
00:08:39,610 --> 00:08:40,910
What's wrong with this?

82
00:08:46,210 --> 00:08:48,910
<i>[Nak Won Instrument Shopping District
Underground Market]</i>

83
00:09:17,610 --> 00:09:19,010
Excuse me.

84
00:09:20,610 --> 00:09:22,010
Yes.

85
00:09:25,810 --> 00:09:28,610
- Can you fix this too?
- This...

86
00:09:33,510 --> 00:09:35,109
Would you be able to fix this?

87
00:09:35,110 --> 00:09:36,610
Wait a minute.

88
00:09:54,310 --> 00:09:55,909
As expected, it can't, can it?

89
00:09:55,910 --> 00:09:59,010
But, where did you get
this Indian accordion?

90
00:10:12,510 --> 00:10:15,109
Of food, clothing. and shelter,
I would've lost food and shelter

91
00:10:15,110 --> 00:10:17,709
if it wasn't for you, Jin Sung.

92
00:10:17,710 --> 00:10:18,809
Excuse me?

93
00:10:18,810 --> 00:10:23,410
I eat and sometimes sleep here too.

94
00:10:23,910 --> 00:10:25,909
What does that...

95
00:10:25,910 --> 00:10:28,109
I don't do that often.

96
00:10:28,110 --> 00:10:32,510
If the subway stops running for the day,
I take a short nap.

97
00:10:33,210 --> 00:10:35,710
I can't afford to take a taxi, so...

98
00:11:00,910 --> 00:11:05,309
Here are the lemon noodles
that you ordered.

99
00:11:05,310 --> 00:11:09,109
But, you've always ordered dishes
that take a long time to make.

100
00:11:09,110 --> 00:11:10,309
What's the occasion today?

101
00:11:10,310 --> 00:11:12,510
Because I don't have time
to lose right now.

102
00:11:13,210 --> 00:11:15,510
I've found a treasure.

103
00:11:16,110 --> 00:11:17,310
A treasure.

104
00:11:32,710 --> 00:11:38,109
<i>There's a love that never changes.</i>

105
00:11:38,110 --> 00:11:39,809
<i>We hope together.</i>

106
00:11:39,810 --> 00:11:43,409
<i>I want to be together with you.</i>

107
00:11:43,410 --> 00:11:45,810
<i>When it's tough, let's...</i>

108
00:11:49,110 --> 00:11:53,010
<i>When it's tough, let's...</i>

109
00:11:56,010 --> 00:11:58,710
I keep getting stuck here.

110
00:12:05,010 --> 00:12:07,310
- What did you do just now?
- Excuse me?

111
00:12:08,510 --> 00:12:13,710
I just thought that it would be nice
if you make the latter part like this.

112
00:12:14,310 --> 00:12:16,309
I'm sorry. This will be done soon.

113
00:12:16,310 --> 00:12:17,910
Please wait just a bit longer.

114
00:12:21,910 --> 00:12:23,709
Can you do it for me one more time?

115
00:12:23,710 --> 00:12:25,910
- Excuse me?
- That latter part...

116
00:12:28,010 --> 00:12:30,010
So...
hold on a second.

117
00:12:36,010 --> 00:12:38,210
Well, like this.

118
00:12:46,110 --> 00:12:48,909
<i>I want to be together with you.</i>

119
00:12:48,910 --> 00:12:52,909
<i>We'll be together
when it gets tough.</i>

120
00:12:52,910 --> 00:12:56,709
<i>You and I will be together.</i>

121
00:12:56,710 --> 00:13:00,509
<i>I want to dream together with you.</i>

122
00:13:00,510 --> 00:13:04,209
<i>We'll be together when it gets tough.</i>

123
00:13:04,210 --> 00:13:08,010
<i>You and I together.</i>

124
00:13:08,910 --> 00:13:10,710
I enjoyed the meal.

125
00:13:11,910 --> 00:13:14,009
On top of having
an exceptional talent for music

126
00:13:14,010 --> 00:13:16,409
he pursues a type of music
that's similar to mine.

127
00:13:16,410 --> 00:13:21,010
But, how did a person like this
end up as an instrument repair...

128
00:13:21,810 --> 00:13:24,909
- I'm sorry.
- No, it's okay.

129
00:13:24,910 --> 00:13:28,709
Well, instrument repair can be
considered part of the field of music.

130
00:13:28,710 --> 00:13:32,009
The truth is, I was playing in a band.

131
00:13:32,010 --> 00:13:34,109
But it was hard to make
a living off of it.

132
00:13:34,110 --> 00:13:35,609
So all of our members scattered.

133
00:13:35,610 --> 00:13:36,909
No wonder.

134
00:13:36,910 --> 00:13:39,089
It was my dream to be in a band.

135
00:13:39,090 --> 00:13:42,809
Of course,
I'm just an obscure rookie singer

136
00:13:42,810 --> 00:13:46,210
so I'm just doing it by myself
since it's hard to manage members.

137
00:13:48,210 --> 00:13:50,210
How about we form a band?

138
00:14:00,310 --> 00:14:01,709
What's this?

139
00:14:01,710 --> 00:14:03,309
- Is he your lover?
- No.

140
00:14:03,310 --> 00:14:05,510
- Your boyfriend?
- No.

141
00:14:06,210 --> 00:14:08,209
- He's the leader of our band.
- No...

142
00:14:08,210 --> 00:14:09,609
Band?

143
00:14:09,610 --> 00:14:11,809
Since we're talking about it,
let's sign the contract.

144
00:14:11,810 --> 00:14:15,209
The down payment will be lemon noodles.

145
00:14:15,210 --> 00:14:17,109
If you don't want to...

146
00:14:17,110 --> 00:14:18,209
If I don't want to?

147
00:14:18,210 --> 00:14:20,209
- Then, cough it up.
- Excuse me?

148
00:14:20,210 --> 00:14:22,609
I'm in a completely
serious mode right now.

149
00:14:22,610 --> 00:14:24,609
I'm really being serious.

150
00:14:24,610 --> 00:14:26,209
Please.

151
00:14:26,210 --> 00:14:27,409
Let's do it together.

152
00:14:27,410 --> 00:14:32,409
The thing is, I'm not sure what
the other members will think about it

153
00:14:32,410 --> 00:14:34,549
and all of them have become
hardworking people now.

154
00:14:34,550 --> 00:14:37,309
Of course, there's no need for them
to give up their occupations.

155
00:14:37,310 --> 00:14:40,909
We'll be busy on weekends
or whenever we have the time.

156
00:14:40,910 --> 00:14:43,809
While continuing the work we normally do,
we can play the music we want

157
00:14:43,810 --> 00:14:46,279
and earn some pocket money too.

158
00:14:46,280 --> 00:14:48,209
Does that earn you
at least some pocket money?

159
00:14:48,210 --> 00:14:50,909
Do you want me to tell you
a secret of this industry?

160
00:14:50,910 --> 00:14:54,709
On hazy nights, there are people
who leave 50,000 won bills

161
00:14:54,710 --> 00:14:57,310
after mistaking them for 5,000 won bills.

162
00:14:58,610 --> 00:15:02,609
<i>A duo with a pretty good combination
of members was born in my restaurant.</i>

163
00:15:02,610 --> 00:15:04,609
But, what should we name our band?

164
00:15:04,610 --> 00:15:06,009
I'm not sure.

165
00:15:06,010 --> 00:15:08,309
For something like this,
simple things are good.

166
00:15:08,310 --> 00:15:10,809
Your name is Jang Da Young.
Jang Da Young.

167
00:15:10,810 --> 00:15:12,209
What is your name?

168
00:15:12,210 --> 00:15:14,209
It's Jo Jin Sung.

169
00:15:14,210 --> 00:15:15,409
Jo Jin Sung.

170
00:15:15,410 --> 00:15:19,310
Jang Da Young, Jo Jin Sung.

171
00:15:20,810 --> 00:15:23,509
Jang Jo...

172
00:15:23,510 --> 00:15:26,509
Jang Jo... Jang Jo?

173
00:15:26,510 --> 00:15:28,509
Jang Jo...

174
00:15:28,510 --> 00:15:31,709
Jang Jo Rim.

175
00:15:31,710 --> 00:15:33,709
The Jang Jo Rim Band.

176
00:15:33,710 --> 00:15:36,909
But, I don't really like the idea of
appealing with a funny name

177
00:15:36,910 --> 00:15:39,009
because it seems a bit shallow.

178
00:15:39,010 --> 00:15:41,809
I mean, from the phrase, 'Jang Jo, Dan Jo'
[meaning 'major and minor'].

179
00:15:41,810 --> 00:15:44,189
You can take the 'Jang Jo' part

180
00:15:44,190 --> 00:15:46,009
and combine it with
'Rim, ' which means 'Forest.'

181
00:15:46,010 --> 00:15:49,510
If you do that,
it becomes 'A Forest of Music.'

182
00:15:50,710 --> 00:15:52,909
A Forest of Music, you say...

183
00:15:52,910 --> 00:15:54,109
Yeah.

184
00:15:54,110 --> 00:15:55,809
How about making this your mark?

185
00:15:55,810 --> 00:15:58,609
Look, Jang Da Young, Jo Jin Sung.

186
00:15:58,610 --> 00:16:00,109
Jang Jo Rim.

187
00:16:00,110 --> 00:16:01,910
I absolutely love it.

188
00:16:18,410 --> 00:16:23,709
<i>There's a love that never changes.</i>

189
00:16:23,710 --> 00:16:29,009
<i>When we hope together,
I want to be with you.</i>

190
00:16:29,010 --> 00:16:32,609
<i>When it gets tough,
let's be together.</i>

191
00:16:32,610 --> 00:16:37,410
<i>You and I together.</i>

192
00:16:38,010 --> 00:16:39,509
- What do you think?
- It's good.

193
00:16:39,510 --> 00:16:42,910
Now, if we add this to it...

194
00:16:53,510 --> 00:16:54,610
What do you think?

195
00:16:55,110 --> 00:16:57,010
- It's absolutely amazing.
- Right?

196
00:16:58,210 --> 00:17:02,509
By the way, it's really nice
working in an instrument store.

197
00:17:02,510 --> 00:17:05,310
We can use all of
the instruments that we need.

198
00:17:08,020 --> 00:17:09,510
It's complete.

199
00:17:10,410 --> 00:17:11,910
Yeah.

200
00:17:24,610 --> 00:17:26,509
They're my friends who
were in a band with me.

201
00:17:26,510 --> 00:17:28,810
They came right after work today, so...

202
00:17:29,410 --> 00:17:33,409
Now that we've gathered like this,
don't we seem like the Village People?

203
00:17:33,410 --> 00:17:36,310
<i>Y-M-C-A.</i>

204
00:17:39,710 --> 00:17:41,810
So, you don't like
these kinds of things.

205
00:18:14,510 --> 00:18:16,009
Let me sleep!

206
00:18:16,010 --> 00:18:18,209
I can't live because
you're so loud.

207
00:18:18,210 --> 00:18:19,609
Are you the only ones who have a life?

208
00:18:19,610 --> 00:18:22,310
Seriously. Jeez, so annoying.

209
00:18:33,110 --> 00:18:35,309
Two iced Americanos please.

210
00:18:35,310 --> 00:18:37,709
You're the one who plays
the Indian accordion, right?

211
00:18:37,710 --> 00:18:40,609
- Yes.
- I saw you at your performance last time.

212
00:18:40,610 --> 00:18:42,009
I'm your fan.

213
00:18:42,010 --> 00:18:44,710
- Please give me your autograph.
- Okay.

214
00:18:49,810 --> 00:18:52,909
By any chance, aren't you
the leader of The Jang Jo Rim Band?

215
00:18:52,910 --> 00:18:54,509
Excuse me? Oh, yes.

216
00:18:54,510 --> 00:18:57,409
I'm a big fan of yours.
Can you take one photo with me?

217
00:18:57,410 --> 00:18:59,610
One, two, three.

218
00:19:00,510 --> 00:19:06,210
<i>I wonder if my soul's
suffering will stop.</i>

219
00:19:06,810 --> 00:19:14,810
<i>I'm looking for my love with whom
my soul in my dream can rest.</i>

220
00:19:18,710 --> 00:19:24,010
<i>If my body rests...</i>

221
00:19:24,910 --> 00:19:30,710
<i>until morning comes...</i>

222
00:19:31,410 --> 00:19:36,910
<i>will I be able to meet you,
my beloved...</i>

223
00:19:37,610 --> 00:19:44,710
<i>in my dream.</i>

224
00:19:47,210 --> 00:19:50,310
- Thank you.
- Thank you.

225
00:19:52,410 --> 00:19:53,710
Excuse me.

226
00:19:54,810 --> 00:19:56,409
Would you like an autograph or a photo?

227
00:19:56,410 --> 00:19:58,109
Both are possible.

228
00:19:58,110 --> 00:19:59,610
It's not that.

229
00:20:00,510 --> 00:20:02,510
Call and come when you have the time.

230
00:20:05,860 --> 00:20:07,910
<i>[Director Seo Man Chang
YPM Entertainment]</i>

231
00:20:11,910 --> 00:20:14,110
YPM.

232
00:20:18,310 --> 00:20:20,509
Seriously, who would've known
we'd get a business card

233
00:20:20,510 --> 00:20:22,109
from such a big entertainment company?

234
00:20:22,110 --> 00:20:24,209
Doesn't that company
specialize in idols?

235
00:20:24,210 --> 00:20:27,209
Yeah, I don't think that
it suits The Jang Jo Rim Band very well.

236
00:20:27,210 --> 00:20:29,309
So, our first hypothetical situation...

237
00:20:29,310 --> 00:20:32,610
change our direction of music
and become idols.

238
00:20:33,110 --> 00:20:35,209
I am a little worried.

239
00:20:35,210 --> 00:20:38,409
But, I have the looks and body.

240
00:20:38,410 --> 00:20:40,409
- Aren't you the age too?
- Yoo Mi.

241
00:20:40,410 --> 00:20:42,909
I must be old. The words that
come to my mind just spill out.

242
00:20:42,910 --> 00:20:47,409
But, isn't someone happier when
they do the music that they want?

243
00:20:47,410 --> 00:20:50,609
The difficult choice between
appeal to popularity and appeal to art.

244
00:20:50,610 --> 00:20:54,110
It's pretty banal,
but it's an eternal problem.

245
00:20:54,610 --> 00:20:56,209
What are you going to do?

246
00:20:56,210 --> 00:20:57,409
We should do it.

247
00:20:57,410 --> 00:21:00,810
We can earn money
and do what we want later.

248
00:21:07,010 --> 00:21:10,239
The second hypothetical situation is
the principle of survival of the fittest.

249
00:21:10,240 --> 00:21:12,209
In other words,
out of the two of us

250
00:21:12,210 --> 00:21:13,809
only the one with
star potential will remain

251
00:21:13,810 --> 00:21:16,809
and the other person will fall behind.

252
00:21:16,810 --> 00:21:18,509
If that person is you, then do it.

253
00:21:18,510 --> 00:21:20,609
After all, I have another job, too.

254
00:21:20,610 --> 00:21:24,109
Hey, instrument repair is actually a job
that requires professional skill.

255
00:21:24,110 --> 00:21:25,609
I can do it when I grow old, too.

256
00:21:25,610 --> 00:21:28,309
Then, the penalty for breaching
the contract is three times greater

257
00:21:28,310 --> 00:21:30,309
so will three bowls of lemon noodles do?

258
00:21:30,310 --> 00:21:31,709
You can do that.

259
00:21:31,710 --> 00:21:35,309
Jeez, there you go again.
You really don't know me.

260
00:21:35,310 --> 00:21:36,409
Never.

261
00:21:36,410 --> 00:21:39,009
Once a partner, always a partner.

262
00:21:39,010 --> 00:21:41,510
We'll go all the way,
with the loyalty of young people.

263
00:21:42,610 --> 00:21:43,809
Okay.

264
00:21:43,810 --> 00:21:46,409
What are you going to do
if it's the other way around?

265
00:21:46,410 --> 00:21:48,809
Jin Sung is praiseworthy too.

266
00:21:48,810 --> 00:21:51,910
What are you going to do
if they tell only Jin Sung to come?

267
00:21:52,410 --> 00:21:53,710
What are you going to do?

268
00:21:55,510 --> 00:21:59,309
Well, would three bowls of lemon noodles
and three glasses of beer do?

269
00:21:59,310 --> 00:22:00,609
Jin Sung.

270
00:22:00,610 --> 00:22:02,909
Never. We'll go all the way.

271
00:22:02,910 --> 00:22:04,410
We'll go.

272
00:22:09,110 --> 00:22:11,109
My beer glass is empty.

273
00:22:11,110 --> 00:22:16,410
That's why it's 'beer' in English. [Beer
is pronounced like 'empty' in Korean.]

274
00:22:19,410 --> 00:22:21,809
Jin Sung, is that a joke?

275
00:22:21,810 --> 00:22:25,909
Anyway, I don't think we'll be able to
appear on talk shows because of Jin Sung.

276
00:22:25,910 --> 00:22:27,499
Hey, aren't you getting
ahead of yourself?

277
00:22:27,500 --> 00:22:29,309
If I want to get
a little more ahead of myself.

278
00:22:29,310 --> 00:22:32,309
I'll recommend this restaurant
as a star's favorite restaurant.

279
00:22:32,310 --> 00:22:37,409
The seats we're sitting on might become
attractions for Hallyu tourists.

280
00:22:37,410 --> 00:22:38,709
This is a big problem.

281
00:22:38,710 --> 00:22:41,009
Then, if I earn a lot of money,
what will I spend it on?

282
00:22:41,010 --> 00:22:44,209
Hey, I contributed to
the birth of your band too.

283
00:22:44,210 --> 00:22:46,109
At the year-end awards ceremony
you have to say

284
00:22:46,110 --> 00:22:51,910
'I thank Yoo Mi, who came up
with our band's name, for this award.'

285
00:22:52,810 --> 00:22:54,409
<i>I thought it would be okay</i>

286
00:22:54,410 --> 00:22:57,609
<i>if even any one of the
hypothetical situations was correct.</i>

287
00:22:57,610 --> 00:22:59,910
<i>But, unfortunately,
that wasn't the case.</i>

288
00:23:06,810 --> 00:23:11,709
So, you're saying that
you only need our song, right?

289
00:23:11,710 --> 00:23:14,309
Well, roughly, yes.

290
00:23:14,310 --> 00:23:17,009
Still, it's a song that we made.
Wouldn't it be better for us to sing...

291
00:23:17,010 --> 00:23:19,200
We'll pay one hundred million won for it.

292
00:23:23,510 --> 00:23:26,409
Well, then,
though we're sad to give it away...

293
00:23:26,410 --> 00:23:30,209
But, it is on the condition that
you hand the copyright over to us as well.

294
00:23:30,210 --> 00:23:31,809
The clause of keeping it a secret
is also a given.

295
00:23:31,810 --> 00:23:33,309
What does the clause of
keeping it a secret imply?

296
00:23:33,310 --> 00:23:35,509
It would mean that
if this contract is revealed to the world

297
00:23:35,510 --> 00:23:38,310
then, you'll have to pay a great penalty
for breach of contract.

298
00:23:39,710 --> 00:23:42,330
We are willing to pay you more,
if the amount is too small.

299
00:23:54,110 --> 00:23:56,810
<i>[YPM Entertainment]</i>

300
00:24:06,310 --> 00:24:07,610
You're here.

301
00:24:09,810 --> 00:24:13,909
Please give us the dish that
takes the longest to make in the world.

302
00:24:13,910 --> 00:24:16,810
It's because we have
a lot of things to think about.

303
00:24:26,110 --> 00:24:27,910
<i>Should we just hand it over?</i>

304
00:24:28,510 --> 00:24:30,310
We can just write another song.

305
00:24:30,810 --> 00:24:32,109
A song is a story.

306
00:24:32,110 --> 00:24:34,109
Do you like the idea of
other people telling our story?

307
00:24:34,110 --> 00:24:35,609
I don't like it.

308
00:24:35,610 --> 00:24:39,709
But other people relay their stories
through good singers too.

309
00:24:39,710 --> 00:24:43,609
Well, those people do it by
honorably putting their names forward.

310
00:24:43,610 --> 00:24:46,309
But, we're only getting
money from it, which is just a shell.

311
00:24:46,310 --> 00:24:48,810
The shell is important, too.

312
00:24:52,610 --> 00:24:56,709
I mean, I don't like dying from heat
in a rooftop house during the summer.

313
00:24:56,710 --> 00:24:58,209
I also hate carrying heavy instruments

314
00:24:58,210 --> 00:25:01,310
and getting called a nuisance girl
on the bus or subway.

315
00:25:04,610 --> 00:25:05,810
Okay.

316
00:25:06,410 --> 00:25:08,110
You already have an answer.

317
00:25:08,810 --> 00:25:10,209
Hand it over then.

318
00:25:10,210 --> 00:25:11,509
I'll drop out.

319
00:25:11,510 --> 00:25:13,609
Aren't your words a bit harsh?

320
00:25:13,610 --> 00:25:16,609
I won't ask you to give me
a certain percentage of the money

321
00:25:16,610 --> 00:25:19,910
so do whatever you want.
I'm going to leave.

322
00:25:34,610 --> 00:25:38,509
<i>Set me free. Let me be.</i>

323
00:25:38,510 --> 00:25:42,209
<i>I can't breathe.</i>

324
00:25:42,210 --> 00:25:46,109
<i>Set me free. Let me be.</i>

325
00:25:46,110 --> 00:25:49,709
<i>I've become sick and tired of it.</i>

326
00:25:49,710 --> 00:25:53,509
<i>Set me free. Let me be.</i>

327
00:25:53,510 --> 00:25:57,309
<i>I've become too exhausted.</i>

328
00:25:57,310 --> 00:26:01,009
<i>Set me free. Let me be.</i>

329
00:26:01,010 --> 00:26:04,809
<i>I can't take it anymore.</i>

330
00:26:04,810 --> 00:26:11,550
<i>Set me free.</i>

331
00:26:12,010 --> 00:26:19,910
<i>Set me free.</i>

332
00:26:24,410 --> 00:26:27,609
I have a few important announcements
to make in the middle of the performance.

333
00:26:27,610 --> 00:26:31,010
License plate number 7897,
please move your car.

334
00:26:31,710 --> 00:26:37,909
Also, if there's a Jo Jin Sung
in the audience

335
00:26:37,910 --> 00:26:41,209
please come to the Late Night Restaurant
later no matter what.

336
00:26:41,210 --> 00:26:42,610
No matter what.

337
00:26:45,010 --> 00:26:47,009
Here. Talk while having a slice.

338
00:26:47,010 --> 00:26:48,810
Thank you.

339
00:26:52,510 --> 00:26:55,210
It's a duet version of
'Until Morning Comes.'

340
00:26:59,410 --> 00:27:00,509
See?

341
00:27:00,510 --> 00:27:02,209
This song is ours.

342
00:27:02,210 --> 00:27:04,410
I'm not going to sing it by myself
no matter what.

343
00:27:09,010 --> 00:27:10,010
<i>In the end...</i>

344
00:27:10,011 --> 00:27:12,209
<i>[The Jang Jo Rim Band's
Lottery Ticket Concert]</i>

345
00:27:12,210 --> 00:27:14,309
<i>the conclusion that they reached was...</i>

346
00:27:14,310 --> 00:27:16,560
<i>[We welcome cash.
We welcome lottery tickets even more.]</i>

347
00:27:21,810 --> 00:27:25,109
<i>Out of the large number of people</i>

348
00:27:25,110 --> 00:27:33,009
<i>how did we come to meet?</i>

349
00:27:33,010 --> 00:27:41,010
<i>It's quite fascinating that
we have the same goal like this.</i>

350
00:27:46,310 --> 00:27:50,709
<i>We're different.</i>

351
00:27:50,710 --> 00:27:57,509
<i>We can achieve anything
if we're together.</i>

352
00:27:57,510 --> 00:28:03,009
<i>There's a love that never changes.</i>

353
00:28:03,010 --> 00:28:08,509
<i>When we hope together.
I want to be with you.</i>

354
00:28:08,510 --> 00:28:12,109
<i>When it gets tough,
let's be together.</i>

355
00:28:12,110 --> 00:28:15,609
<i>You and I will be together.</i>

356
00:28:15,610 --> 00:28:17,609
Can you actually win
a hundred million won by doing this?

357
00:28:17,610 --> 00:28:18,909
Didn't you see the news?

358
00:28:18,910 --> 00:28:21,309
Last week's first place prize
was carried over

359
00:28:21,310 --> 00:28:23,309
so the first place prize
this week is 1.6 billion won.

360
00:28:23,310 --> 00:28:24,910
1.6 billion won?

361
00:28:26,110 --> 00:28:27,509
Let's focus on this.

362
00:28:27,510 --> 00:28:30,710
It may not seem like it,
but this is a 1.6 billion won concert.

363
00:28:33,010 --> 00:28:36,310
We love you, Jang Jo Rim!

364
00:28:37,810 --> 00:28:40,810
Jang Jo Rim forever!

365
00:28:41,510 --> 00:28:44,910
We love you, Jang Jo Rim!

366
00:28:46,410 --> 00:28:49,310
Jang Jo Rim forever!

367
00:28:50,110 --> 00:28:53,810
Jang! Jo! Rim!

368
00:29:00,610 --> 00:29:03,510
<i>You could say that
it was quite wise.</i>

369
00:29:12,010 --> 00:29:13,809
Encore! Encore!

370
00:29:13,810 --> 00:29:16,609
- Jang Jo Rim!
- Encore!

371
00:29:16,610 --> 00:29:18,139
Jang Jo Rim! Jang Jo Rim!

372
00:29:18,140 --> 00:29:26,140
Subtitles by DramaFever

