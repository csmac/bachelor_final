1
00:01:07,325 --> 00:01:11,617
The day before yesterday
at a pet market in Kostroma...

2
00:01:11,784 --> 00:01:13,326
...I saw buntings.

3
00:01:15,784 --> 00:01:18,034
I don't think I'd ever
seen them before.

4
00:01:20,285 --> 00:01:24,577
They are strange birds, plain.

5
00:01:25,869 --> 00:01:30,035
But something from the past,
from my childhood or my dreams...

6
00:01:30,453 --> 00:01:32,495
...flashed and beckoned me...

7
00:01:37,454 --> 00:01:41,204
...when the old man selling them
said in a flat voice,

8
00:01:42,454 --> 00:01:43,621
"Buntings".

9
00:01:49,455 --> 00:01:53,330
Buntings.
300 rubles for the pair.

10
00:02:05,207 --> 00:02:06,624
I live in Neya.

11
00:02:07,332 --> 00:02:11,624
It's one of those towns that
no one remembers today.

12
00:02:12,333 --> 00:02:15,166
It stands on the Neya river...

13
00:02:16,626 --> 00:02:19,876
...lost somewhere between
the woods of Vologda and Vyatka.

14
00:02:21,792 --> 00:02:28,542
The Neya, Unzha, Poksha, Vokhtoma,
the Viga, Mera, Vaya, Sogozha...

15
00:02:29,752 --> 00:02:32,085
Beautiful names left
behind by the Merja...

16
00:02:33,044 --> 00:02:38,378
...a Finnish tribe that dissolved
into the Slavs some 400 years ago.

17
00:02:38,753 --> 00:02:43,044
The orphaned villages, a few rites,
rivers with forgotten names.

18
00:02:44,087 --> 00:02:45,337
That's all that's left from them,

19
00:02:47,670 --> 00:02:50,212
although many people here still
think of themselves as Merjans.

20
00:02:50,838 --> 00:02:54,838
These northern outskirts
always hold on to it longer.

21
00:02:57,381 --> 00:03:00,631
Our people are a bit strange,
their faces are inexpressive.

22
00:03:00,797 --> 00:03:01,922
There are no passions boiling,

23
00:03:02,964 --> 00:03:05,506
although sudden affections
and divorces are not uncommon.

24
00:03:07,340 --> 00:03:08,632
There is promiscuity,

25
00:03:09,090 --> 00:03:13,382
but for a Merjan it's ancient -
like an ethnic rite or a custom.

26
00:03:15,508 --> 00:03:16,508
Ask me why?

27
00:03:18,049 --> 00:03:19,924
No one remembers anymore.

28
00:04:10,515 --> 00:04:12,973
I don't remember when
or why it started,

29
00:04:14,765 --> 00:04:16,806
but I wanted to know,
to understand who we are.

30
00:04:18,807 --> 00:04:20,974
Why are we like this
and not like something else?

31
00:04:23,807 --> 00:04:25,349
My father was a local poet.

32
00:04:26,142 --> 00:04:28,267
He wrote under the pseudonym
Vesa Sergeyev.

33
00:04:30,225 --> 00:04:36,058
Maybe that's why I began collecting
snatches of songs, names, words.

34
00:04:37,684 --> 00:04:40,809
Some I needed to look for and
some were always right next to me.

35
00:04:42,810 --> 00:04:47,852
I just hadn't written before, so
at first it was coming out badly.

36
00:04:49,644 --> 00:04:52,435
But the biggest thing was that
I didn't know what to write about...

37
00:04:52,645 --> 00:04:56,270
...although my father would often
tell me that if your soul hurts...

38
00:04:56,936 --> 00:04:59,853
...write about the things
you see around you.

39
00:05:01,521 --> 00:05:05,687
My name is Aist. It's an uncommon
name. Obviously Merjan.

40
00:05:07,438 --> 00:05:10,438
I'm just over 40.
I have no family.

41
00:05:11,272 --> 00:05:12,855
I work at the Neya paper mill.

42
00:06:25,697 --> 00:06:26,697
Thank you.

43
00:06:32,657 --> 00:06:33,407
Excuse me for a second.

44
00:06:42,241 --> 00:06:43,241
Miron is calling for you.

45
00:06:50,950 --> 00:06:52,117
The director's calling.

46
00:07:20,454 --> 00:07:21,454
It's working.

47
00:07:25,705 --> 00:07:28,122
Uh-huh. Very well. Turn it off.

48
00:07:45,291 --> 00:07:45,874
Hello.

49
00:07:46,041 --> 00:07:46,707
Hi.

50
00:07:46,916 --> 00:07:47,749
May I?

51
00:07:49,417 --> 00:07:51,000
Hello, Aist Vsevolodovich.

52
00:07:51,167 --> 00:07:52,167
Hello, Miron Alekseyevich.

53
00:08:46,882 --> 00:08:47,590
It's from shadberry.

54
00:08:58,175 --> 00:08:59,008
It's good.

55
00:09:10,927 --> 00:09:15,468
My wife Tanya died.
Last night.

56
00:09:27,304 --> 00:09:31,054
I'm going to the rope factory
in Gorbatov to sign a contract.

57
00:09:31,887 --> 00:09:33,345
Call me back tomorrow evening.

58
00:09:39,305 --> 00:09:40,305
Do you want another drink?

59
00:09:41,472 --> 00:09:42,472
No, thank you.

60
00:09:48,139 --> 00:09:49,139
I'm not taking her to the morgue.

61
00:09:51,223 --> 00:09:52,765
I don't want to show her
to anybody.

62
00:09:55,015 --> 00:09:58,057
I'd prefer to do everything
just with you.

63
00:10:15,185 --> 00:10:16,351
I don't want to be alone.

64
00:10:22,352 --> 00:10:23,894
Let's go right now.

65
00:10:25,894 --> 00:10:26,936
Please.

66
00:10:29,644 --> 00:10:31,102
Well, alright.

67
00:10:35,770 --> 00:10:38,020
And then I remembered
about my buntings.

68
00:10:38,771 --> 00:10:43,188
I might not be home for three
days. Who's going to feed them?

69
00:10:44,521 --> 00:10:48,021
Anyway I had some vague feeling
I should take them with us.

70
00:10:48,897 --> 00:10:50,480
Miron Alekseyevich didn't mind.

71
00:16:42,983 --> 00:16:45,441
We adorned her like a bride.

72
00:16:46,691 --> 00:16:49,149
People here always dress
the deceased women that way.

73
00:16:49,858 --> 00:16:53,274
It's the same way the girlfriends
decorate the happy bride.

74
00:16:55,109 --> 00:16:59,400
In the morning, before the
wedding, they wash her,

75
00:17:00,443 --> 00:17:04,693
wipe her dry, and prepare
multicolored threads.

76
00:17:06,277 --> 00:17:12,486
The bride lies or sits down while
her girlfriends crowd around her.

77
00:17:13,402 --> 00:17:15,861
They tickle her, make jokes
and lots of noise.

78
00:17:17,195 --> 00:17:19,445
Then they tie the threads
into her pubic hair.

79
00:17:21,403 --> 00:17:24,737
That's how tomorrow's wife
will go to her husband.

80
00:17:26,613 --> 00:17:29,613
And at night he will take
the threads from her hair,

81
00:17:30,489 --> 00:17:35,489
tie them into a knot
and hang them on an alder tree.

82
00:20:11,174 --> 00:20:12,966
We were leaving our beloved Neya.

83
00:20:14,467 --> 00:20:16,342
Long ago I read that a nation
is alive as long as...

84
00:20:16,967 --> 00:20:21,259
...it remembers its language
and keeps its traditions.

85
00:20:22,926 --> 00:20:27,676
This rite is the last thing
that connects a Merjan with life.

86
00:20:29,511 --> 00:20:33,677
What will be left
if it will be forgotten?

87
00:20:35,094 --> 00:20:40,969
We were leaving our beloved Neya.
We didn't know then it was forever.

88
00:21:27,518 --> 00:21:33,976
I married her when she was 19.
I was already close to 40.

89
00:21:35,685 --> 00:21:36,685
She lived in Vokhma.

90
00:21:38,102 --> 00:21:39,227
She was very shy.

91
00:21:40,810 --> 00:21:42,935
She was embarrassed that she
didn't know how to use make-up...

92
00:21:43,228 --> 00:21:44,645
...or how to wear
interesting clothes.

93
00:21:48,186 --> 00:21:50,686
But Tanya was so close to me.

94
00:21:52,562 --> 00:21:54,229
She totally obeyed me.

95
00:21:55,937 --> 00:22:00,646
I would tell her, "Take off your
dress. Open up this way,

96
00:22:00,813 --> 00:22:05,105
try it like this, stand right
here, move your hips."

97
00:22:11,856 --> 00:22:14,148
All three of Tanya's holes
were working.

98
00:22:15,232 --> 00:22:16,690
And it was me who unsealed them.

99
00:22:19,274 --> 00:22:22,940
But everything always happened
only by my initiative.

100
00:22:28,650 --> 00:22:30,650
We call this kind of talk "smoke".

101
00:22:31,776 --> 00:22:35,526
It's a custom to tell about
the one you love...

102
00:22:36,109 --> 00:22:38,067
...as long as the body
is still on the earth.

103
00:22:38,693 --> 00:22:41,943
You say things you'd
never tell a stranger...

104
00:22:42,360 --> 00:22:43,943
...while your beloved
was still alive.

105
00:22:44,693 --> 00:22:46,402
But over the dead you're allowed...

106
00:22:46,944 --> 00:22:49,861
...because it makes your face
brighter...

107
00:22:50,819 --> 00:22:52,986
...and turns your grief
into tenderness.

108
00:27:15,561 --> 00:27:18,186
- Good day. Where are you from?
- From Neya.

109
00:27:19,061 --> 00:27:21,644
- Where are you going?
- To Mescherskaya Porosl.

110
00:27:22,603 --> 00:27:24,603
- And what are you carrying?
- A veretenitsa.

111
00:27:36,646 --> 00:27:38,188
- And what kind of birds are they?
- Buntings.

112
00:27:38,480 --> 00:27:40,189
We call our beloved
women veretenitsa.

113
00:27:40,897 --> 00:27:42,439
Of course the lieutenant
knows that.

114
00:27:42,939 --> 00:27:45,480
Besides, it's not hard
to see what we're carrying.

115
00:27:46,773 --> 00:27:49,315
Many people here still
remember that they are Merjan.

116
00:28:48,197 --> 00:28:51,613
So those are buntings you have?
I've never seen them before.

117
00:28:54,364 --> 00:28:55,614
But I always loved that word.

118
00:28:59,615 --> 00:29:02,199
Buntinkina is Tanya's maiden name.

119
00:29:04,199 --> 00:29:08,074
When we were young I always
called her Buntinkina or Bunting.

120
00:29:14,200 --> 00:29:16,200
Tanya loved birds very much,

121
00:29:18,326 --> 00:29:20,826
but couldn't stand
seeing them in cages.

122
00:29:23,327 --> 00:29:26,952
I was thinking about getting her
a heron so it could stroll freely.

123
00:29:40,829 --> 00:29:48,120
Another 20. 90 big ax handles.
200 small birch ax handles.

124
00:29:48,246 --> 00:29:52,746
20 beech ax handles.
160 shovel handles.

125
00:29:53,330 --> 00:29:55,955
None of the beech.
We'll take the rest.

126
00:30:00,331 --> 00:30:01,247
Help us out, please.

127
00:30:48,253 --> 00:30:49,253
Aist Vsevolodovich,

128
00:30:50,920 --> 00:30:54,378
do you mind if I keep "smoking"?

129
00:30:57,504 --> 00:30:59,796
Do you remember when
we celebrated my 50th?

130
00:31:01,797 --> 00:31:05,130
I drank some wine
and I wanted Tanya so badly.

131
00:31:07,422 --> 00:31:10,130
I looked at her
and she understood me,

132
00:31:11,506 --> 00:31:14,673
but her eyes told me that it
was better not to right then.

133
00:31:16,382 --> 00:31:21,966
I got so upset that
my stomach started to hurt.

134
00:31:25,675 --> 00:31:28,508
Miron continued to tell me
how much he loved his Tanya,

135
00:31:29,258 --> 00:31:33,925
but he really didn't have to. The
whole town knew about his passion...

136
00:31:34,676 --> 00:31:39,134
...how they hid in the local hotel,
how he washed her with vodka.

137
00:31:40,802 --> 00:31:43,510
There were rumors that
Tanya didn't love him,

138
00:31:44,510 --> 00:31:46,385
but Miron said nothing about it.

139
00:31:48,636 --> 00:31:51,803
Dear Miron Alekseyevich!
This song is for you.

140
00:31:52,095 --> 00:31:54,636
It is "The Smell of Summer"
with lyrics by Vesa Sergeyev.

141
00:32:11,305 --> 00:32:24,263
I went to the pharmacy
Bought some soapwort I'd found,

142
00:32:24,640 --> 00:32:37,807
And some dried swamp viburnum,
That I bought by the pound.

143
00:32:38,559 --> 00:32:50,309
I found some cudweed,
And also some thyme.

144
00:32:51,518 --> 00:33:04,435
A heap of smooth corn silk,
And knotgrass so fine.

145
00:33:07,979 --> 00:33:14,395
Toadflax with cowberry,
And young poplar leaf,

146
00:33:14,605 --> 00:33:21,021
Some mint, some tansy,
and sage I believe.

147
00:33:21,439 --> 00:33:27,856
Dandelion root
And juniper berries,

148
00:33:28,023 --> 00:33:34,440
More than one hundred packs
I bought in my hurry.

149
00:33:34,566 --> 00:33:41,149
I brought it all home
And boiled it well.

150
00:33:41,274 --> 00:33:45,858
I wanted so badly
That fine summer smell.

151
00:36:34,338 --> 00:36:36,796
Tanya worked at
the same paper mill.

152
00:36:38,755 --> 00:36:40,171
We liked each other.

153
00:36:41,756 --> 00:36:46,214
I photographed her once
and something flashed between us.

154
00:36:46,714 --> 00:36:49,922
Something sparked
and hopelessly sped away.

155
00:38:47,812 --> 00:38:48,604
Hello!

156
00:39:28,276 --> 00:39:30,942
- Show me the hedgehog.
- The one with the band?

157
00:39:31,193 --> 00:39:32,360
Yes. The blue one.

158
00:39:33,235 --> 00:39:33,943
It blinks.

159
00:39:38,569 --> 00:39:39,986
Here you go. Thirty rubles.

160
00:39:43,361 --> 00:39:44,361
Press its belly.

161
00:39:48,654 --> 00:39:50,529
Is it broken? Let me exchange it.

162
00:39:50,570 --> 00:39:52,279
No. It's exactly what I need.

163
00:39:53,780 --> 00:39:54,946
I won't have to break it myself.

164
00:40:04,114 --> 00:40:05,781
We entered Meschyorskaya Porosl.

165
00:40:06,989 --> 00:40:08,906
It's the Merjan name
for the town of Gorbatov.

166
00:40:10,698 --> 00:40:12,198
Its emblem has
a blooming apple tree.

167
00:40:13,115 --> 00:40:14,573
It's a nice town on the Oka River.

168
00:40:28,659 --> 00:40:29,950
And why here?

169
00:40:30,825 --> 00:40:32,409
The honeymoon.

170
00:40:33,950 --> 00:40:36,409
We didn't want to go too far.
Besides it was expensive.

171
00:40:39,410 --> 00:40:41,993
Tanya fell in love with Oka
when she was still in school.

172
00:40:42,661 --> 00:40:48,411
We rented a house here after our
wedding. Just for a week.

173
00:40:50,119 --> 00:40:54,994
The honeymoon in
Meschyorskaya Porosl...

174
00:52:05,286 --> 00:52:06,620
We committed Tanya to the water.

175
00:52:07,703 --> 00:52:10,620
People here always do that,
it's a rule.

176
00:52:12,871 --> 00:52:16,912
Our cemeteries are half-empty,
mostly newcomers lay there.

177
00:52:17,912 --> 00:52:21,829
But water is the dream
of every Merjan.

178
00:52:23,163 --> 00:52:27,705
Drowning means to suffocate from
joy, tenderness and yearning.

179
00:52:29,706 --> 00:52:32,581
If we find someone drowned
we don't burn them.

180
00:52:33,164 --> 00:52:36,456
We tie on a weight and give
them back to the water.

181
00:52:37,874 --> 00:52:41,040
The water replaces their body
with a new flexible one.

182
00:52:42,165 --> 00:52:45,290
Death from water
is immortality for a Merjan.

183
00:53:15,337 --> 00:53:22,879
Oh, Neya River.
We know your fish by name.

184
00:53:23,463 --> 00:53:27,005
Anya and Lyosha,
Pasha and Kira.

185
00:53:27,713 --> 00:53:30,588
Oh, Neya River.
Oh, Neya River.

186
00:53:31,589 --> 00:53:35,506
Sleeping under the ice is
Tatyana the pearl oyster.

187
00:53:35,589 --> 00:53:38,506
Seryozha the perch
isn't sleeping...

188
00:53:44,757 --> 00:53:48,007
My father dreamed of drowning
and living next to the shore.

189
00:53:48,216 --> 00:53:50,758
He was a queer fish,
that self-taught Merjan poet.

190
00:53:52,174 --> 00:53:55,758
People laughed at him. Sometimes
they paid attention to his words.

191
00:53:55,884 --> 00:53:57,759
Sometimes they beat him up.

192
00:53:58,925 --> 00:54:02,009
His naive poems were printed
in "The Neya Lights".

193
00:54:04,301 --> 00:54:10,801
The paper sold well. It was cheap
and useful for domestic chores.

194
00:54:12,344 --> 00:54:14,761
But my father believed that
people needed his poems.

195
00:54:16,469 --> 00:54:23,302
Once we made a hole in the ice and
sank his most treasured possession.

196
00:54:24,137 --> 00:54:27,887
He was a queer fish,
that self-taught Merjan poet.

197
00:54:56,807 --> 00:55:02,766
A Mexican toy
That my Cuban friend found

198
00:55:03,017 --> 00:55:08,100
Looks like a boot
And with water makes sound.

199
00:55:08,893 --> 00:55:13,184
At the place of the heel
Is a round open space,

200
00:55:14,018 --> 00:55:19,226
At the toe of the boot
Is a sad young girl's face.

201
00:55:19,935 --> 00:55:24,810
My friend wrote a letter
And gave me advice:

202
00:55:25,311 --> 00:55:31,645
Add water, then rock her
And stare at her eyes

203
00:55:32,479 --> 00:55:38,479
I poured in some water
And gave it a rock.

204
00:55:38,646 --> 00:55:43,479
The girl started moaning
My chair squeaked in shock.

205
00:55:44,522 --> 00:55:47,188
And from out those huge eyes,
From invisible holes...

206
00:55:47,605 --> 00:55:52,022
My mother died during childbirth
when I was in seventh grade.

207
00:55:53,106 --> 00:55:57,939
After that my father changed
a lot. He stopped behaving oddly.

208
00:56:00,899 --> 00:56:03,107
He didn't "smoke" at the funeral.

209
00:56:04,315 --> 00:56:08,607
Afterwards he would often
call the river's name.

210
00:56:09,483 --> 00:56:11,816
He would swim in cold weather
with pain in his heart.

211
00:56:12,233 --> 00:56:14,900
He would walk drunk on
the ice that was still thin.

212
00:56:19,359 --> 00:56:23,067
We committed my mom and my
still-born sister Nina to the water.

213
00:56:25,110 --> 00:56:30,193
My father dreamed of drowning, but
the Merjan don't drown themselves.

214
00:56:30,403 --> 00:56:34,486
It's impolite to rush to heaven,
passing by the others.

215
00:56:35,361 --> 00:56:40,069
The river chooses people herself.
The water is the highest judge.

216
00:57:15,533 --> 00:57:19,324
Look at the hair on my little feet.
I will do the same with you.

217
00:57:20,367 --> 00:57:24,242
The two of us are sandpipers
Wearing slippers of blue.

218
00:57:24,950 --> 00:57:28,659
We'll put packs on our backs
Stuffed full of timothy hay

219
00:57:29,326 --> 00:57:32,410
Aist, bring the can.
Aist!

220
00:57:34,661 --> 00:57:39,702
And perhaps now we'll die
Dancing this Turkish Halay.

221
00:58:44,085 --> 00:58:46,544
I wasn't there when
my father died.

222
00:58:47,378 --> 00:58:51,295
He had a bad death.
He drank contaminated alcohol.

223
00:58:53,920 --> 00:58:56,836
But I knew that
he died from sorrow.

224
00:59:08,297 --> 00:59:14,088
On the way back to Neya we got
lost. We ended up in Molochai.

225
00:59:15,714 --> 00:59:18,839
This town has a very sad
and tender meaning for us.

226
00:59:21,257 --> 00:59:22,840
It's like Paris for the Europeans.

227
00:59:25,257 --> 00:59:27,382
It's a shame it
doesn't exist anymore.

228
00:59:29,425 --> 00:59:34,841
It dissolved into the outskirts
of another big, modern...

229
00:59:35,967 --> 00:59:37,551
...and living town.

230
01:02:56,450 --> 01:02:58,616
- Hi!
- Hi!

231
01:02:59,451 --> 01:03:00,576
Do you want us?

232
01:03:02,159 --> 01:03:03,701
We want you a lot.

233
01:03:04,701 --> 01:03:06,451
It's so good that you exist.

234
01:03:14,869 --> 01:03:19,036
The girls' names were Julia
and Rima. They were okay.

235
01:03:20,036 --> 01:03:23,453
They laughed when we introduced
ourselves as Miron and Aist.

236
01:05:01,257 --> 01:05:03,216
Your wife died?

237
01:05:05,924 --> 01:05:07,966
Yes. Recently.

238
01:05:15,343 --> 01:05:16,093
Rim!

239
01:05:39,346 --> 01:05:40,512
Rim, wait!

240
01:05:43,096 --> 01:05:45,679
We were very thankful
to Julia and Rima.

241
01:05:47,638 --> 01:05:50,972
Because a live woman's body is also
a river that carries grief away.

242
01:05:52,388 --> 01:05:56,805
It's only a shame that
you can't drown in it.

243
01:06:22,851 --> 01:06:24,059
Turn it on.

244
01:06:26,809 --> 01:06:28,851
- Here?
- The little triangle.

245
01:06:32,768 --> 01:06:37,185
I knew that Tanya liked you.
She was sad.

246
01:06:38,394 --> 01:06:41,811
She would sit quietly looking
somewhere into the distance.

247
01:06:44,145 --> 01:06:47,520
I wasn't angry at her.
I loved her a lot.

248
01:06:50,104 --> 01:06:53,229
We couldn't have children.
It was very hard for her.

249
01:06:53,646 --> 01:06:57,521
I don't remember what
or how I answered him.

250
01:06:57,813 --> 01:07:03,230
My thoughts and memories swept
over me and carried me away.

251
01:07:05,147 --> 01:07:10,522
Only later I asked if he believed
he'd meet his Tatyana again...

252
01:07:11,815 --> 01:07:15,565
Ésomething ripped in him,
something broke down.

253
01:07:16,941 --> 01:07:20,983
His expression changed
and he stopped the car.

254
01:08:15,115 --> 01:08:21,948
I felt sorry for him, for my
odd father, for my mum, for Tanya.

255
01:08:24,532 --> 01:08:26,074
Our names will be forgotten too,

256
01:08:26,282 --> 01:08:29,407
just like the Merjans have
forgotten their sacred words.

257
01:08:31,075 --> 01:08:34,825
A Merjan doesn't have gods,
only love for one another.

258
01:08:36,283 --> 01:08:38,742
All that Miron had left now
was his love for Tanya.

259
01:08:41,409 --> 01:08:45,368
And he had left to believe was
that he'll reunite with her...

260
01:08:45,577 --> 01:08:49,244
...when it's his time to become ash
and be committed to the water.

261
01:08:50,285 --> 01:08:54,535
The belief in this half-forgotten
rite was perhaps as naive...

262
01:08:55,411 --> 01:08:58,161
...as my desire to restore
our lost culture.

263
01:08:59,120 --> 01:09:01,703
If something is doomed to
disappear, then so be it.

264
01:09:02,412 --> 01:09:03,871
Then so be it.

265
01:09:04,704 --> 01:09:05,579
Then so be it.

266
01:09:34,458 --> 01:09:36,458
I should have let her go...

267
01:09:37,166 --> 01:09:41,125
I should have let her go,
Aist Vsevolodovich.

268
01:10:24,881 --> 01:10:27,006
We were going back home.

269
01:10:27,422 --> 01:10:33,714
It was empty and cold, although
this November happened to be warm.

270
01:10:34,548 --> 01:10:39,298
Miron was silent, it didn't make
any sense to "smoke" any more.

271
01:10:41,174 --> 01:10:42,591
We had to make a detour.

272
01:10:44,133 --> 01:10:48,341
We found ourselves back around
the same place we left Tanya.

273
01:10:49,592 --> 01:10:54,175
We had returned as if an invisible
force wouldn't let us leave here.

274
01:10:56,551 --> 01:10:58,843
Miron looked like he didn't notice.

275
01:10:59,718 --> 01:11:04,593
He cheered up strangely,
and I too felt a load off my mind.

276
01:11:05,927 --> 01:11:08,469
I felt sad and pure.

277
01:11:08,886 --> 01:11:13,636
But the sadness didn't press on
me. It enveloped me like a mother.

278
01:12:01,601 --> 01:12:04,351
Your birds are
probably very smart.

279
01:12:05,184 --> 01:12:06,768
Let's ask them for something.

280
01:12:10,602 --> 01:12:11,894
Immortality?

281
01:12:17,895 --> 01:12:19,145
Yes, I suppose.

282
01:12:22,603 --> 01:12:23,853
Do you hear us?

283
01:12:29,021 --> 01:12:30,187
They hear.

284
01:12:40,773 --> 01:12:43,314
When we drove up on
the Kineshemsky Bridge,

285
01:12:44,148 --> 01:12:48,773
Miron whispered,
"My Tanyusha is gone."

286
01:13:34,070 --> 01:13:38,320
The buntings grew quiet.

287
01:13:40,030 --> 01:13:41,321
Actually too quiet.

288
01:14:16,325 --> 01:14:21,659
We fell from the bridge into the
Volga, the great Merjan river.

289
01:14:23,326 --> 01:14:28,660
The buntings helped us, darting
to kiss the driver's eyes.

290
01:14:30,244 --> 01:14:33,411
Miron Alekseyevich immediately
went looking for Tatyana.

291
01:14:34,495 --> 01:14:37,662
As for me, I found my father's
silted typewriter...

292
01:14:38,787 --> 01:14:41,787
and typed this book
on sides of dead fish.

293
01:14:44,538 --> 01:14:48,829
And the water will carry
Merja's secrets away.

294
01:14:51,497 --> 01:14:52,955
Which ones and to where?

295
01:14:54,205 --> 01:14:57,372
Everyone will find out for
himself when the time comes.

296
01:15:00,206 --> 01:15:02,331
Only love has no end.

297
01:15:04,540 --> 01:15:07,206
Only love has no end.

298
01:15:17,708 --> 01:15:22,958
DEDICATED TO THE MEMORY MY PARENTS

