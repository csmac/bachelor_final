1
00:00:18,960 --> 00:00:24,751
I MET A HAPPY GIPSY, TOO

2
00:02:19,040 --> 00:02:23,033
Hop in, Tisa,
you'll get soaked.

3
00:02:27,240 --> 00:02:32,109
Are you running away again?
-No, I was at my aunt's.

4
00:02:33,280 --> 00:02:36,238
Mirta sent me.
-Like hell he did!

5
00:02:38,920 --> 00:02:42,151
Mirta says you want to be
a caf� singer. Is that true?

6
00:02:42,480 --> 00:02:45,517
I'm not ashamed of that. I don't
want to tend geese all my life.

7
00:02:45,640 --> 00:02:48,518
Has Mirta plucked
the geese yet?

8
00:02:49,320 --> 00:02:53,871
Not yet, they're not ready.

9
00:02:54,800 --> 00:02:59,555
Has he got a buyer?
-I don't know and I don't care.

10
00:02:59,840 --> 00:03:02,638
What do you care about?
Boyfriends?

11
00:03:03,040 --> 00:03:06,032
That's none of your business.
-You are my business.

12
00:03:06,160 --> 00:03:09,072
Look at yourself!
You belong in a feather bed.

13
00:03:09,520 --> 00:03:12,353
Shut up, you dirty Gypsy!

14
00:03:14,920 --> 00:03:17,070
You're a dirty Gypsy yourself!

15
00:03:19,040 --> 00:03:24,478
Don't worry, there'll be plenty
left over for your bride-groom.

16
00:03:24,840 --> 00:03:29,834
My taxi is not a bordello.
-Shut up and drive!

17
00:03:33,040 --> 00:03:35,873
The driver's right,
leave me alone!

18
00:03:42,960 --> 00:03:45,428
I know him.
He's just teasing.

19
00:03:55,160 --> 00:03:59,119
See, I got you home safely.
I didn't bother you.

20
00:03:59,440 --> 00:04:04,116
Stop roaming around. It's
more dangerous than you think.

21
00:04:04,560 --> 00:04:07,632
Tell Mirta to stay
out of my territory.

22
00:04:07,840 --> 00:04:11,150
I'll stay out of his. There's
plenty to go around Vojvodina.

23
00:04:11,280 --> 00:04:13,555
Can you give me
50 dinars, please?

24
00:04:16,920 --> 00:04:20,833
What's it for?
-Thanks. A new scarf.

25
00:04:32,640 --> 00:04:36,315
These geese are
well-taken care of.

26
00:04:42,520 --> 00:04:45,910
Their feathers are
the best quality.

27
00:04:46,080 --> 00:04:50,153
A goose is a goose, mother,
and I know what I can pay.

28
00:04:51,800 --> 00:04:53,836
That's too much!

29
00:04:55,000 --> 00:04:58,834
At least pay as much as �erd.
-How can l?

30
00:05:02,720 --> 00:05:05,029
I have to make a profit.

31
00:05:06,680 --> 00:05:10,912
I can't sell them for less
than what �erd offered.

32
00:05:12,400 --> 00:05:15,915
We have to stay
with our price.

33
00:05:17,640 --> 00:05:22,634
If you need money,
we could give you a loan.

34
00:05:22,880 --> 00:05:27,715
Mother, you know very well that
geese always grow new feathers.

35
00:05:28,120 --> 00:05:30,918
The sooner you pluck them,
the faster they'll grow.

36
00:05:31,120 --> 00:05:34,999
Do you mean it, about the loan?
Do you have it on you?

37
00:05:35,240 --> 00:05:38,550
Don't you worry, mother,
just pluck those feathers.

38
00:05:42,440 --> 00:05:46,797
Leave it, we'll pour our own.

39
00:05:52,360 --> 00:05:58,629
20.000, and that's generous.
-You make five times that amount.

40
00:05:59,280 --> 00:06:04,752
Bora, what's the idea? -Did you
see those under-nourished geese?

41
00:06:05,560 --> 00:06:10,031
Only a sucker like me would
buy them. -Give me 30.000.

42
00:06:10,240 --> 00:06:14,756
They're quality church geese.
They're on special food.

43
00:06:18,160 --> 00:06:22,711
Milance, tell me if it's right
that Bora gives me only 30.000.

44
00:06:22,840 --> 00:06:25,673
That's none of his business.
He's police.

45
00:06:25,840 --> 00:06:30,118
That's why I want his opinion.
Milance, come here a minute.

46
00:06:33,200 --> 00:06:35,714
Sit down, Bora is buying.

47
00:06:36,000 --> 00:06:39,595
You can only drink with a Gypsy,
or throw him in jail.

48
00:06:39,760 --> 00:06:43,070
We're no more crooked
than anyone else.

49
00:06:43,280 --> 00:06:46,670
We're only trading,
doing business.

50
00:06:55,520 --> 00:06:59,957
Take a seat.
-She'll skin you alive!

51
00:07:01,840 --> 00:07:04,912
Not me, I can handle her.

52
00:07:11,160 --> 00:07:13,230
Music!

53
00:08:42,360 --> 00:08:49,118
...1, 2, 3, 4...
-Shut up! The baby's sick!

54
00:08:52,680 --> 00:08:57,117
You drunken bastard!
I should scratch your eyes out!

55
00:09:03,880 --> 00:09:08,829
Let him be. He's so drunk,
he won't know the difference.

56
00:09:10,040 --> 00:09:14,033
Not while he's here. Tomorrow,
after he's gone. -Come on!

57
00:09:36,080 --> 00:09:39,789
Don't cry.
I'll help you bury her.

58
00:09:45,520 --> 00:09:48,557
�erd, what are we
going to do?

59
00:09:49,360 --> 00:09:54,275
The child hasn't even been
baptised. How can I bury her?

60
00:10:06,240 --> 00:10:08,310
Don't worry.

61
00:10:11,280 --> 00:10:14,795
The Lord giveth
and taketh away.

62
00:10:15,120 --> 00:10:19,079
We'll bury her this afternoon.
-I don't want that!

63
00:10:19,880 --> 00:10:24,158
She's my first-born and she
has to be baptised! -All right!

64
00:10:27,600 --> 00:10:31,559
I'll manage somehow,
no matter how much it costs.

65
00:10:43,800 --> 00:10:45,836
Hello, Gypsy.

66
00:10:47,040 --> 00:10:51,272
Have any more
antiques arrived?

67
00:10:51,760 --> 00:10:56,754
How about the antique desk?
Did you keep it for us?

68
00:10:58,160 --> 00:11:01,994
Hey you crook, when are you
going to join the working class?

69
00:11:02,200 --> 00:11:05,829
A crooked gypsy or a crooked
worker. What's the difference?

70
00:11:18,080 --> 00:11:22,596
Mother, I brought you
a child to baptise.

71
00:11:23,200 --> 00:11:25,794
Please, it's urgent!

72
00:11:32,160 --> 00:11:36,233
I can't baptise a dead child
or bury it.

73
00:11:40,720 --> 00:11:46,989
I bought all your feathers
and lent you the money.

74
00:11:48,240 --> 00:11:53,109
Do it as a favour.
We may need each other.

75
00:12:19,960 --> 00:12:22,838
Hey, old man,
open up in there!

76
00:12:28,960 --> 00:12:33,795
Open up!
-Why are you shouting?

77
00:12:34,560 --> 00:12:37,518
Shandor, we have
money to spend.

78
00:12:37,800 --> 00:12:40,758
You'll earn more,
than picking up carpets.

79
00:12:52,800 --> 00:12:55,439
Get the cards out!
-Who's going to deal?

80
00:13:00,720 --> 00:13:04,838
Sit down, old man, and take your
percentage. -No more than usual.

81
00:13:08,080 --> 00:13:10,640
All aboard, I'm driving!

82
00:13:12,960 --> 00:13:17,238
Count what's on the table.
That's my bet.

83
00:13:26,960 --> 00:13:28,678
Damn it!

84
00:13:29,720 --> 00:13:35,352
It looks like I'm out of gas!
Toot, toot!

85
00:13:36,640 --> 00:13:41,191
I'll bet the minimum.

86
00:13:44,120 --> 00:13:48,033
Give me another 2,
for the lot. -Not enough.

87
00:13:48,240 --> 00:13:50,629
Is that all you've got?

88
00:13:53,320 --> 00:13:55,038
How's this?

89
00:14:00,840 --> 00:14:03,593
It's Swiss.
-All right.

90
00:14:13,040 --> 00:14:16,112
Of all the damn luck!
-That's rough, Bora!

91
00:14:16,400 --> 00:14:20,951
I had 12, and you had 10.
-Look out for the curve!

92
00:14:27,120 --> 00:14:32,274
You've got no more money.
-Lend me 5.000. -No.

93
00:14:33,160 --> 00:14:37,312
Take my shirt and shoes.
They're yours if I don't pay.

94
00:14:39,080 --> 00:14:40,991
And the jacket.

95
00:14:43,720 --> 00:14:45,472
Here's 5.000.

96
00:14:56,440 --> 00:14:58,317
One card.

97
00:15:11,600 --> 00:15:14,558
The end of the line!

98
00:15:20,840 --> 00:15:23,912
You've lost everything,
you good-for-nothing!

99
00:15:27,920 --> 00:15:32,550
You've ruined me and my children
and sold everything. What for?

100
00:15:33,440 --> 00:15:38,150
You good-for-nothing drunkard!
You lousy Gypsy bastard!

101
00:15:38,520 --> 00:15:42,559
You kept on losing, until you
lost the shirt of your back!

102
00:15:46,880 --> 00:15:49,474
How are we going to eat?!
I spit on you, you pig!

103
00:16:10,400 --> 00:16:15,269
That's enough, Bora! She should
have scratched your eyes out!

104
00:16:15,840 --> 00:16:22,871
You're a no good gambler.
You should have sold your head.

105
00:16:25,120 --> 00:16:30,114
Go and buy more feathers.
They have feathers in Padina.

106
00:16:30,560 --> 00:16:34,872
With what?
-You'll find a way.

107
00:16:35,280 --> 00:16:39,398
You'll find the money,
somehow.

108
00:16:40,480 --> 00:16:44,871
You pig! You've taken everything
from the house! -Enough!

109
00:16:45,320 --> 00:16:50,440
Give me back my T.V. set,
you lousy bastard! -Shut up!

110
00:16:52,240 --> 00:16:56,597
Shut up, or I'll kill you!
I've had enough!

111
00:16:56,920 --> 00:17:02,552
You've ruined me!
You lousy drunkard, scum!

112
00:18:04,600 --> 00:18:10,596
Brothers and sisters, today's
sermon will be from St Matthew.

113
00:18:12,400 --> 00:18:18,589
I will speak Serbian, so that our
Serbian brothers can understand.

114
00:18:19,960 --> 00:18:25,353
The man came, paid me well,
then took them away.

115
00:18:25,760 --> 00:18:30,151
Was he a Gypsy?
-I don't know.

116
00:18:30,520 --> 00:18:34,513
He was dark, like you.

117
00:18:36,520 --> 00:18:41,435
Where was he from?
-Stop asking me questions!

118
00:18:42,400 --> 00:18:47,713
Try to remember!
I've always paid you well.

119
00:18:48,600 --> 00:18:52,593
Tell me who he is.
-Who, Mirta? -The sorter?

120
00:18:53,480 --> 00:18:57,268
Why didn't you say
he was here!

121
00:18:57,840 --> 00:19:02,994
You only asked me where
he came from, not his name.

122
00:19:05,400 --> 00:19:09,791
You're in my territory, Mirta!
They're not your feathers!

123
00:19:10,520 --> 00:19:15,958
Who says? -We decided to
split the villages between us!

124
00:19:16,240 --> 00:19:24,557
Look at the list! -I can't read.
I could wipe myself with it.

125
00:19:29,360 --> 00:19:32,557
You can't read,
but you can listen!

126
00:19:38,600 --> 00:19:42,275
Bacinci, Debeljaca, Gospodinci...

127
00:19:42,760 --> 00:19:47,436
Count them together with me!
Ten for you and ten for me!

128
00:19:47,920 --> 00:19:52,357
Bacinci, Debeljaca, Gospodinci...

129
00:20:01,040 --> 00:20:08,230
Now, your ten.
Dobanovci, Bogojevci, Sonta...

130
00:20:19,520 --> 00:20:23,479
See, an equal split!

131
00:20:24,120 --> 00:20:28,113
You should have known
that Padina is mine!

132
00:20:32,040 --> 00:20:34,998
You're right. I was wrong.

133
00:20:35,440 --> 00:20:39,558
I can't return the feathers,
I sold them.

134
00:20:41,440 --> 00:20:44,238
I'll stay in Apatin.

135
00:20:44,840 --> 00:20:49,436
I'll advance you the money.
Buy the feathers, instead of me.

136
00:20:49,720 --> 00:20:52,837
We'll split the profit.
-Are you trying to con me?

137
00:20:52,960 --> 00:20:55,758
Your father conned your mother!

138
00:20:55,920 --> 00:20:59,071
You take my territory,
and split the work.

139
00:20:59,320 --> 00:21:02,676
You get the feathers,
I'll sort and sell them.

140
00:21:05,840 --> 00:21:09,276
Bring two pitchers of wine,
for Bora and me!

141
00:21:22,520 --> 00:21:24,476
Enough, already!

142
00:21:27,520 --> 00:21:30,034
He pinched me again!

143
00:21:37,480 --> 00:21:42,110
Hey, Tisa, why are
you looking so sad?

144
00:21:43,080 --> 00:21:48,871
What are you doing up so late?
-Sing us a song! -I can't.

145
00:21:50,000 --> 00:21:54,471
I thought you wanted to be
a singer. Well, sing then!

146
00:22:03,600 --> 00:22:08,913
"As I travelled on
my long journey,

147
00:22:11,120 --> 00:22:16,035
I met a happy Gypsy, too"

148
00:22:19,160 --> 00:22:21,913
That's enough! Go home!

149
00:22:23,120 --> 00:22:26,396
You've plenty to do
do for your wedding!

150
00:22:32,760 --> 00:22:35,832
Can't you give me
a moment of peace?

151
00:22:36,280 --> 00:22:40,512
I'm waiting for you to get rich.
-It won't be long now.

152
00:22:42,120 --> 00:22:49,196
Forget about her, Bora.
Here's 5.000. Now sing for us!

153
00:24:02,480 --> 00:24:06,268
I'll never play again!
I'm sticking to music!

154
00:24:10,720 --> 00:24:15,840
..."As I travelled on my
long journey"...

155
00:24:17,320 --> 00:24:22,633
..."I met a happy Gypsy, too"...

156
00:24:37,880 --> 00:24:41,634
..."Hey, Gypsies!"...

157
00:24:44,720 --> 00:24:47,837
..."Hey, people!"...

158
00:25:48,840 --> 00:25:52,913
Get up, you drunken bastard!
Can you hear me?

159
00:25:54,200 --> 00:25:59,433
You Gypsy bastard!
How much did you drink?

160
00:26:03,280 --> 00:26:05,589
Bora, help me!

161
00:26:07,280 --> 00:26:11,034
My sister would kill me, if l
left him here. He's so drunk!

162
00:26:11,360 --> 00:26:14,557
Let's get him into the cart.

163
00:26:20,040 --> 00:26:23,919
Into the garbage cart,
you Gypsy bastard!

164
00:26:43,560 --> 00:26:47,872
You can't wipe your own nose
and you want to get married!

165
00:29:24,160 --> 00:29:27,948
Ruza! Take this!

166
00:29:30,160 --> 00:29:33,835
I'm going away on business.

167
00:30:28,880 --> 00:30:32,589
Who owns this flock of geese?
-The one up front.

168
00:30:33,240 --> 00:30:37,518
Behind the coffin?
-Further to the front.

169
00:30:38,840 --> 00:30:44,756
There's no one in front of the
coffin. -What about in it?

170
00:30:45,960 --> 00:30:49,919
God rest our departed master's
soul. -Bloody master!

171
00:30:53,000 --> 00:30:55,355
Where are the heirs?

172
00:30:56,560 --> 00:31:00,678
In front, leading the mourners.

173
00:31:28,720 --> 00:31:30,836
To each his due.

174
00:31:36,720 --> 00:31:39,473
Here's to the souls of the dead.

175
00:31:39,760 --> 00:31:43,355
And for the living, how about
selling me your feathers.

176
00:31:43,480 --> 00:31:48,600
You don't have money for
my flock. -I do. How much?

177
00:31:50,560 --> 00:31:53,518
What should the price be?
-100.000 and no less.

178
00:31:53,760 --> 00:31:56,991
What'll we do,
if we don't sell?

179
00:31:57,400 --> 00:32:01,439
Sell them another day.

180
00:32:02,800 --> 00:32:06,839
100.000 and they'll
be plucked by tomorrow.

181
00:32:07,280 --> 00:32:10,989
Too much! -You won't
find such down!

182
00:32:11,600 --> 00:32:14,194
1 kilo of feathers, a piece.

183
00:32:16,000 --> 00:32:20,949
Every Gypsy, when trading
his horse, sings his praise.

184
00:32:21,440 --> 00:32:26,150
Anything less, no deal.
-Forget it then.

185
00:32:27,040 --> 00:32:29,634
I don't carry that much
money on me.

186
00:32:34,720 --> 00:32:38,030
Wait a minute!
Wait, my friend!

187
00:32:38,320 --> 00:32:41,437
Dear Lord, stop running
like an express train!

188
00:32:41,840 --> 00:32:46,755
You'll never be able to do
business with those peasants.

189
00:32:47,320 --> 00:32:51,359
Come, I'll show you something.

190
00:32:53,360 --> 00:32:57,638
Feathers, lots of feathers!

191
00:33:09,160 --> 00:33:12,630
You must be married,
a young fellow like you.

192
00:33:13,520 --> 00:33:19,117
I bet you were married
Gypsy style and not in a church.

193
00:33:25,160 --> 00:33:31,030
People aren't virtuous, nowadays.
So, a priest has nothing to do.

194
00:33:35,280 --> 00:33:39,512
When was the last time
you thought of your soul?

195
00:33:39,840 --> 00:33:43,230
Bring your wife and
I'll marry you properly.

196
00:33:46,640 --> 00:33:50,474
Praise be God!
You want some?

197
00:33:51,240 --> 00:33:53,959
You Gypsies drink like fish.

198
00:33:55,880 --> 00:34:00,874
To you, Bora. You must
have your marriage blessed.

199
00:34:01,240 --> 00:34:08,715
Listen to Pavle, a poor priest.
-Are you a priest, or not?

200
00:34:09,200 --> 00:34:13,512
Of course! -I don't give a damn
about priests! -Don't swear!

201
00:34:14,120 --> 00:34:17,271
We came here on business.

202
00:34:19,480 --> 00:34:22,995
All right, you dirty Gypsy.

203
00:34:26,640 --> 00:34:30,235
The feather bed
of Brother Hrizosoma.

204
00:34:34,160 --> 00:34:38,438
This one belongs to
Brother Pajsije.

205
00:34:39,200 --> 00:34:43,432
Now you'll see what a
surprise I have for you.

206
00:34:50,200 --> 00:34:54,478
This is all that's left
of Brother Jovana.

207
00:34:55,760 --> 00:35:00,038
Here are the beds
of my brothers,

208
00:35:00,400 --> 00:35:04,518
but you haven't
asked about them.

209
00:35:05,640 --> 00:35:11,397
Some have passed away, others
have abandoned their faith.

210
00:35:12,440 --> 00:35:19,516
There's no life in the church.
We're as poor as the Gypsies.

211
00:35:20,240 --> 00:35:24,950
Even you Gypsies are forced to
work, because of the antichrists!

212
00:35:25,120 --> 00:35:29,033
Their souls will burn in hell,
like shish-kebab. Believe me!

213
00:35:34,320 --> 00:35:40,714
We can't make a living anymore,
Bora, everyone's corrupted.

214
00:35:41,360 --> 00:35:46,275
There's no place for angels
on this Earth. Look at me.

215
00:35:46,800 --> 00:35:51,749
As soon as I sell everything
to the Gypsies, I'll be leaving.

216
00:35:53,320 --> 00:35:55,880
Where will you go?
-Germany.

217
00:36:30,720 --> 00:36:32,438
What's going on?

218
00:36:32,720 --> 00:36:40,798
Tisa's husband isn't a man,
he's a child. -So?

219
00:36:41,560 --> 00:36:45,155
Tisa wants to ditch him.

220
00:36:45,680 --> 00:36:50,549
His mother wants to wait and
prove he's a man to everyone.

221
00:36:51,400 --> 00:36:56,030
You can do it!
-Go help him.

222
00:36:56,600 --> 00:37:02,516
He doesn't need help, he's a man.
-Some man, just like his father!

223
00:37:02,880 --> 00:37:04,791
Shut up, you bitch!

224
00:37:07,320 --> 00:37:12,952
Get out of here! You belong
under your mother's skirt!

225
00:37:16,600 --> 00:37:18,955
Show her that you can do it!

226
00:37:19,280 --> 00:37:21,475
Take him back!
I don't want him!

227
00:37:21,600 --> 00:37:27,197
He's a man, you're just a slut!
-I'm not! Your son's not a man!

228
00:37:27,920 --> 00:37:31,515
You cast a spell on him!
-Go back to your mother!

229
00:37:40,840 --> 00:37:44,310
You've never known
what a real man is like!

230
00:37:48,560 --> 00:37:52,553
She's a witch!
Don't let them kill each other!

231
00:37:57,560 --> 00:38:01,155
She's killing my daughter!
-Give me a knife!

232
00:38:04,160 --> 00:38:07,038
I'll kill her!

233
00:38:18,560 --> 00:38:21,757
Look at the dog!

234
00:38:35,480 --> 00:38:38,677
Where will you go now?

235
00:38:41,760 --> 00:38:45,673
Do you want to come
to my place?

236
00:38:47,120 --> 00:38:48,553
I don't know.

237
00:38:51,520 --> 00:38:58,437
I don't want what's not mine.
We're partners, as agreed.

238
00:38:59,720 --> 00:39:04,635
I advance you the money,
you bring me the feathers.

239
00:39:04,960 --> 00:39:09,715
I don't disagree. We've always
been good friends, Mirta.

240
00:39:10,200 --> 00:39:11,997
How much do I owe?

241
00:39:12,680 --> 00:39:19,279
First we finish this wine,
then we talk.

242
00:39:20,600 --> 00:39:23,433
That makes...
-Never mind.

243
00:39:25,840 --> 00:39:31,949
You have someone in your house
that I want. Tisa is free again.

244
00:39:34,480 --> 00:39:38,792
I'll give you Padina
and 2 of my villages.

245
00:39:39,960 --> 00:39:44,795
Why do you need Tisa?
She only tends the geese.

246
00:39:45,040 --> 00:39:48,112
Find someone else.

247
00:39:48,800 --> 00:39:52,156
I have nothing more to say
about our business.

248
00:39:52,600 --> 00:39:55,956
And Tisa, keep your hands off!
-Mirta, I'm serious.

249
00:39:57,640 --> 00:40:01,758
I want to marry her,
I even have a priest.

250
00:40:02,040 --> 00:40:05,953
I got rid of my old bag.

251
00:40:06,680 --> 00:40:13,518
I'm no worse than that kid.
-Bora, you're not very smart.

252
00:40:15,200 --> 00:40:18,909
She's my stepdaughter.

253
00:40:19,240 --> 00:40:23,631
I want her married to a young
kid, not a drunkard like you.

254
00:40:25,200 --> 00:40:30,069
That way, she stays
at home, for me.

255
00:40:35,280 --> 00:40:37,874
Take back your territories.

256
00:40:38,240 --> 00:40:44,395
Don't come to my house again.
Each man for himself.

257
00:43:26,200 --> 00:43:30,478
She's got a knife, Mirta!
Get out!

258
00:43:43,880 --> 00:43:49,159
What do you want? Do you
want to come in? Come on.

259
00:43:57,000 --> 00:44:02,757
Did you run away? -I'll promise
anything, if you let me stay.

260
00:44:04,240 --> 00:44:08,518
I don't know where to go,
or what to do.

261
00:44:09,200 --> 00:44:14,797
What am I going to do with you?
I can't take care of myself.

262
00:44:22,000 --> 00:44:25,549
Here's 500 dinars.
Go to Belgrade.

263
00:44:26,080 --> 00:44:29,311
My son, �urika, is there.

264
00:44:37,560 --> 00:44:41,553
He's living with my sister
at Zarkovo.

265
00:44:42,720 --> 00:44:45,359
It's not hard to find.

266
00:44:52,000 --> 00:44:58,394
Tell them his mother sent you.
They might let you stay a week.

267
00:45:01,000 --> 00:45:05,118
Then, you're on your own,
out on the streets. You'll see.

268
00:45:39,120 --> 00:45:43,113
Mirta asked to take
good care of her.

269
00:46:43,760 --> 00:46:49,915
How should I know where they
took her? I'm out 500 dinars.

270
00:47:21,280 --> 00:47:24,477
If she took my advice,
she's in Belgrade, in a big city.

271
00:47:24,800 --> 00:47:27,155
Not rotting away, Gypsy style.

272
00:47:32,120 --> 00:47:34,759
What's she going to do
in Belgrade?

273
00:47:36,960 --> 00:47:38,757
Walk the streets?

274
00:47:57,680 --> 00:48:00,478
She can either clean
the streets or walk them.

275
00:48:01,440 --> 00:48:04,477
The bastard, Shandor,
won't tell me anything.

276
00:48:05,680 --> 00:48:08,592
Money will help.

277
00:48:21,360 --> 00:48:23,715
Be quiet!
You'll wake him up, Bora.

278
00:48:26,840 --> 00:48:29,149
You must be joking!
I thought it was buried.

279
00:48:29,240 --> 00:48:30,958
Shut up, good for nothing!

280
00:48:31,680 --> 00:48:34,672
You haven't given birth
to another?

281
00:48:34,840 --> 00:48:37,274
Say, between the three of us,
we produce them fast.

282
00:48:37,440 --> 00:48:39,749
Don't talk so loud!
He's my sisters, not mine.

283
00:48:39,840 --> 00:48:42,798
She brought him to us when
her husband abandoned her.

284
00:48:42,920 --> 00:48:45,115
What else could she do?
-All right, I get it.

285
00:48:45,280 --> 00:48:49,114
But she better get another man.
There's plenty of man.

286
00:48:50,000 --> 00:48:52,594
Say, where's �erd?
I want to see him.

287
00:48:52,760 --> 00:48:55,399
You know where he is.
I suppose he's getting drunk.

288
00:49:05,120 --> 00:49:07,793
I need money, �erd.
Hundred thousand.

289
00:49:08,400 --> 00:49:10,994
Are you out of your mind?
Hundred thousand?

290
00:49:11,160 --> 00:49:12,593
You could turn me
upside-down

291
00:49:12,720 --> 00:49:14,790
right here before
you'll find that much.

292
00:49:14,960 --> 00:49:16,712
The money is gone.
I bought merchandise.

293
00:49:16,880 --> 00:49:20,236
Beautiful old furniture, few
armchairs, a set of doors.

294
00:49:20,320 --> 00:49:23,392
I spent the money. I must go
straight to Belgrade to find a buyer.

295
00:49:23,520 --> 00:49:24,794
And where is that leave me?

296
00:49:24,960 --> 00:49:27,474
Hold on to my ears and go
dancing for small change?

297
00:49:27,600 --> 00:49:29,556
That's not my business!
There was once a man who

298
00:49:29,640 --> 00:49:31,995
made enough money to buy
a rope and hanged himself!

299
00:49:32,080 --> 00:49:34,435
You can stay at my place
if you have no place to go,

300
00:49:34,560 --> 00:49:36,312
but leave my wife alone!
We'll go tomorrow

301
00:49:36,440 --> 00:49:38,908
to see the mother superior.
She owes us money.

302
00:49:39,080 --> 00:49:41,275
She might pay us back.
We can ask her anyhow.

303
00:49:46,320 --> 00:49:49,437
Rajko!
How much do we owe?

304
00:49:51,360 --> 00:49:54,955
�erd sent me. I need
money. A hundred thousand.

305
00:49:55,040 --> 00:49:57,600
We lend it to you.
I need it back.

306
00:49:57,680 --> 00:50:00,592
You should know better than
to ask me for money.

307
00:50:00,760 --> 00:50:04,594
It's not June yet. Anyway,
I can't pay you. It's not the time.

308
00:50:04,840 --> 00:50:07,957
Pretty geese! Pretty geese!
Seed! Seed!

309
00:50:12,360 --> 00:50:15,113
We're very poor, my son.
Poor sisters who pray for others

310
00:50:15,280 --> 00:50:18,078
and ask nothing more than
to be good neighbours.

311
00:50:18,400 --> 00:50:20,994
Pretty geese!
Seed, seed, seed!

312
00:50:23,280 --> 00:50:25,555
And besides, my son,
�erd said:

313
00:50:25,720 --> 00:50:28,439
you can pay back when
we come for feathers.

314
00:50:28,960 --> 00:50:31,793
It won't be long. We'll pay you
as soon as feather is ready.

315
00:50:31,920 --> 00:50:32,989
That's a great help!

316
00:50:33,160 --> 00:50:37,472
Give back my TV set!
Give me back! The TV is mine!

317
00:50:37,800 --> 00:50:41,679
You dirty bustard! You drinker!
Taking everything we have!

318
00:50:41,800 --> 00:50:44,598
Give it back to me!
-Why don't you get lost!

319
00:50:44,760 --> 00:50:47,672
You heard what I said!
-I dare you! You've ruined me!

320
00:50:47,800 --> 00:50:50,155
If you don't shut up,
I'll kill you!

321
00:50:50,320 --> 00:50:53,312
I want my TV set back!
It's mine! You rotten gambler!

322
00:50:53,440 --> 00:50:56,193
I'll kill you for this!
Give back my TV set!

323
00:51:00,160 --> 00:51:02,958
Come on out, old man!

324
00:51:18,760 --> 00:51:20,876
Where is Tisa?

325
00:51:21,200 --> 00:51:24,272
I don't know.
I swear I don't!

326
00:51:24,720 --> 00:51:27,234
I've got money. Tell me.

327
00:51:28,720 --> 00:51:32,076
You're scared of Mirta.
Coward!

328
00:51:33,720 --> 00:51:39,033
I've got nothing to be afraid of,
I just don't know.

329
00:51:40,040 --> 00:51:43,032
You obviously don't
need the money.

330
00:51:48,560 --> 00:51:51,711
Bora, listen! Wait!

331
00:51:58,400 --> 00:52:04,157
How much money?
-First, tell me where Tisa is.

332
00:52:06,440 --> 00:52:08,715
You Gypsy bastard!

333
00:52:09,080 --> 00:52:12,993
Give me 3.000 and I'll tell you
where she is.

334
00:52:14,760 --> 00:52:17,877
It's a deal. Let's go, then.

335
00:52:31,080 --> 00:52:34,959
Where are you taking her?
Leave her alone!

336
00:52:35,240 --> 00:52:39,233
May your eyes drop out
of your head!

337
00:52:55,360 --> 00:52:58,750
Lord, I bring Thee Your humble
servants, Bora and...

338
00:52:59,040 --> 00:53:02,476
What's your name? -Tisa.
-That's the name of a river.

339
00:53:02,640 --> 00:53:05,791
You're lucky they didn't call
you Danube. -Get on with it!

340
00:53:06,000 --> 00:53:08,514
In the name of the Father....

341
00:53:13,800 --> 00:53:16,837
Any fish in the river?
-Shut up!

342
00:53:18,640 --> 00:53:22,758
Bless this union of man
and woman...

343
00:53:42,480 --> 00:53:46,712
Where did you find this
sweet thing? -Just keep singing!

344
00:53:50,880 --> 00:53:55,590
...etcetera, etcetera.
Grant them peace and grace...

345
00:54:08,400 --> 00:54:11,597
In the name of the Father,
Son and Holy Spirit. Amen.

346
00:54:12,040 --> 00:54:15,999
Bora, you're finally married.
You lucky girl!

347
00:54:19,120 --> 00:54:24,831
You should've done this earlier.
Now, give me those crowns.

348
00:54:27,240 --> 00:54:31,358
I may use them again.
One never knows.

349
00:54:33,240 --> 00:54:37,552
That's that. Now we can
have a drink to celebrate.

350
00:54:51,240 --> 00:54:56,189
Have you decided where
you'll sleep? -In the village.

351
00:54:57,120 --> 00:55:01,033
But it's the middle of the night!
-We'll manage.

352
00:55:02,080 --> 00:55:08,315
We're used to it.
-I won't permit that. Come.

353
00:55:10,280 --> 00:55:15,479
You will spend your first
night in the house of God.

354
00:55:15,880 --> 00:55:21,989
This is your first night and
God has blessed your marriage.

355
00:55:22,720 --> 00:55:29,114
You should sleep in
Brother Hrizosova's bed.

356
00:55:31,120 --> 00:55:33,839
It's big and far away,

357
00:55:34,160 --> 00:55:38,756
so that I won't have
sinful thoughts.

358
00:55:41,040 --> 00:55:47,991
I'll bring you an eiderdown,
with angel feathers. My own.

359
00:55:50,600 --> 00:55:55,515
I didn't sell everything
to you Gypsies.

360
00:55:57,160 --> 00:55:59,515
Light as the soul.

361
00:56:27,760 --> 00:56:30,911
Wait for me here.
I won't be long.

362
00:56:40,360 --> 00:56:44,239
Remember me,
I tried to buy your feathers.

363
00:56:44,640 --> 00:56:47,996
Be seated,
the soup's getting cold.

364
00:56:55,200 --> 00:56:57,714
I came to make a deal.

365
00:57:01,440 --> 00:57:04,238
It's better that I take them,
and not the plague.

366
00:57:05,120 --> 00:57:08,590
Here, read.

367
00:57:13,360 --> 00:57:22,029
It says: "The geese will die of
the plague. It is spreading."

368
00:57:22,440 --> 00:57:25,113
We told you the price.

369
00:57:25,600 --> 00:57:28,751
If you have the money,
take the geese.

370
00:57:29,120 --> 00:57:31,714
You won't find such
beautiful geese.

371
00:57:32,640 --> 00:57:34,949
How much?

372
00:57:39,120 --> 00:57:45,719
Like I said, 100.000. -They all
have feathers like an angel.

373
00:57:46,280 --> 00:57:48,999
Until they get the plague.

374
00:57:50,000 --> 00:57:57,588
80.000. At least you'll
get something. -What now?

375
00:57:58,040 --> 00:58:03,194
Nothing under 90.
-lf he won't? -He'll accept.

376
00:58:05,840 --> 00:58:12,678
97. -Stop being a Gypsy!
84, and it's a deal.

377
00:58:14,440 --> 00:58:20,595
That's the price.
-All I have is 90.

378
00:58:26,080 --> 00:58:28,674
All right, it's a deal.

379
00:58:29,000 --> 00:58:31,434
Get the flock together
and we'll pluck them.

380
00:58:56,480 --> 00:58:59,040
There you are, my son.
-From now on,

381
00:58:59,280 --> 00:59:02,716
Tisa will be staying with us.
Ruza will also be staying.

382
00:59:02,960 --> 00:59:05,269
She doesn't bother me.

383
00:59:36,960 --> 00:59:42,956
I just made a deal with your son.
Sell the feathers to me, not Bora.

384
00:59:43,240 --> 00:59:46,835
I can pay you in advance.
I've brought you money.

385
00:59:52,560 --> 00:59:54,949
I can't sing.

386
01:00:23,480 --> 01:00:27,553
What have I done this time?
What the hell!

387
01:00:28,720 --> 01:00:36,400
Bora, what have you done?
-I've no idea. Article 132 and 76.

388
01:00:38,480 --> 01:00:43,634
That's disturbing the peace.
-And a traffic offence.

389
01:00:44,320 --> 01:00:49,075
That costs 5.000. Disturbing the
peace and a traffic violation.

390
01:00:49,360 --> 01:00:52,750
They'll put poor Sava
in jail for 20 days.

391
01:00:53,200 --> 01:00:59,196
They'll only fine him 15.000.
-20 days, for sure.

392
01:01:01,400 --> 01:01:07,316
Minimum penalty for
assault and battery.

393
01:01:09,240 --> 01:01:12,915
Put up or shut up!

394
01:01:13,360 --> 01:01:16,670
He won't get a jail sentence.
-5.000. -I accept.

395
01:01:16,880 --> 01:01:21,032
I'll bet you 5.000 he gets
the jail sentence. -All right.

396
01:01:26,480 --> 01:01:32,715
Bora, you're not interested?
I've got a job you might like.

397
01:01:33,360 --> 01:01:38,753
You can make a bundle selling
horses, for a cut, at the fair.

398
01:01:39,560 --> 01:01:42,791
When are you going?
-ln a day or two.

399
01:01:43,680 --> 01:01:45,591
Bora Pavlovic!

400
01:01:48,960 --> 01:01:51,554
Are you interested?
-Sure.

401
01:01:55,520 --> 01:02:02,676
December 7, you threw feathers
from a truck. Intentionally?

402
01:02:04,120 --> 01:02:07,032
Not really, I was drunk.

403
01:02:07,880 --> 01:02:11,555
Why do I have to keep
fighting with you Gypsies?

404
01:02:11,880 --> 01:02:17,796
5.000 dinars or 10 days in jail.
Which will it be?

405
01:02:20,920 --> 01:02:26,074
I'll pay. -All right,
you can pay the fine.

406
01:02:27,320 --> 01:02:31,199
Bora, we know each other.
-We do, Judge.

407
01:02:31,520 --> 01:02:35,718
How could you,
a feather buyer, throw them?

408
01:02:36,040 --> 01:02:39,430
What can I say?
I was drunk.

409
01:02:41,200 --> 01:02:44,158
I'm a Gypsy.

410
01:02:46,320 --> 01:02:51,474
I threw them and they floated,
as if they had wings.

411
01:03:03,000 --> 01:03:06,356
Tisa, when I picked
you up by the road,

412
01:03:06,680 --> 01:03:09,558
were you coming
from Belgrade? -No.

413
01:03:12,680 --> 01:03:17,708
Have you ever seen the city?
-No, I haven't.

414
01:03:19,880 --> 01:03:23,953
I've been there.
Belgrade is a great city!

415
01:03:50,480 --> 01:03:54,268
Shut up, you old bat!
-You good-for-nothing drunkard!

416
01:03:55,680 --> 01:03:59,116
Don't start again!
Get out and don't come back!

417
01:03:59,360 --> 01:04:02,875
Find yourself another man!
You hag, parasite!

418
01:04:31,400 --> 01:04:36,394
Have you really been to Belgrade?
-I was born and raised there.

419
01:04:38,200 --> 01:04:46,676
It's great! Go, while Bora's
away. I'll give you the money.

420
01:04:47,600 --> 01:04:50,637
Go to my family.
They're street cleaners.

421
01:04:50,920 --> 01:04:55,357
I have where to go.
Lence's son, he'll let me stay.

422
01:04:56,360 --> 01:05:02,993
Want to be a singer? I'll give
you all I have, for the train.

423
01:05:14,520 --> 01:05:17,159
What are you staring at?
Come here!

424
01:05:19,680 --> 01:05:23,195
Lence loves me very much
and sends me everything I want.

425
01:05:23,680 --> 01:05:26,717
But I like Sombor more
and I'd go back right now.

426
01:05:26,880 --> 01:05:29,633
How long have you been
in Belgrade? -Two years.

427
01:05:29,840 --> 01:05:33,435
When I learn to use my
wooden legs, I'll go home.

428
01:06:21,760 --> 01:06:25,992
Lence said that I could stay here
for a while. -What does she know.

429
01:06:28,960 --> 01:06:32,953
Singing in the streets,
you can just get by.

430
01:06:33,920 --> 01:06:38,914
You should have stayed with her,
if you want to be a singer.

431
01:06:39,840 --> 01:06:44,311
Or you could be a street cleaner.
It's bad, but it could be worse.

432
01:08:14,440 --> 01:08:16,635
Hey, look!

433
01:08:18,000 --> 01:08:23,597
Just what we need, to keep
us awake. -Pick her up.

434
01:08:51,360 --> 01:08:53,476
Here I am, peek-a-boo!

435
01:09:02,760 --> 01:09:06,514
He said that you're
a pretty girl.

436
01:09:10,960 --> 01:09:18,150
Give him a kiss, and he'll sing
you a song. He's so bashful.

437
01:09:21,880 --> 01:09:26,715
No kiss?
Where are you from?

438
01:09:27,760 --> 01:09:30,797
I'm a Gypsy from Sombor.

439
01:09:31,280 --> 01:09:34,477
Gypsies are all right!

440
01:10:11,040 --> 01:10:13,395
Leave me alone!

441
01:10:31,120 --> 01:10:34,954
Bitch! You lousy Gypsy!

442
01:10:54,320 --> 01:10:59,075
Have you gone crazy?
Leave the girl alone!

443
01:11:01,000 --> 01:11:04,310
What girl, she's an animal!

444
01:11:04,760 --> 01:11:07,991
I know these Gypsies!

445
01:11:16,960 --> 01:11:20,316
He's crazy!
What's wrong with him?

446
01:11:22,120 --> 01:11:25,032
Nothing but mud!

447
01:11:26,480 --> 01:11:32,271
What monotonous, flat country!
-Yeh, what a place.

448
01:12:22,400 --> 01:12:26,518
What are you doing here,
little girl?

449
01:12:54,760 --> 01:12:58,753
Tell me where she is.
Tell me, or else!

450
01:13:00,040 --> 01:13:03,635
Give me back my T.V. set,
and I'll tell you.

451
01:13:04,200 --> 01:13:12,039
Where did she go?
-First, return my T.V.

452
01:13:32,000 --> 01:13:35,879
Hello, are you �urika?
-That's me.

453
01:13:38,920 --> 01:13:41,673
Tisa sends this,
so you can go to the movies.

454
01:13:41,960 --> 01:13:45,430
Did she go back to you?
-Where else would she be?

455
01:13:45,640 --> 01:13:48,791
She said she was
going to Lenca's.

456
01:13:49,120 --> 01:13:52,874
She probably did. Good bye.

457
01:14:16,040 --> 01:14:18,474
Where is she?
-What do you mean?

458
01:14:21,400 --> 01:14:28,750
Better talk, if you want to live!
-Go to hell! You won't find her!

459
01:14:33,920 --> 01:14:37,469
Talk now, or you never will!

460
01:14:39,200 --> 01:14:44,593
She's with them other bastards.
You won't get her. -Where is she?

461
01:14:50,680 --> 01:14:53,558
Go to Mirta!

462
01:15:00,320 --> 01:15:02,356
I hope he kills you!

463
01:18:10,400 --> 01:18:14,473
You admit that there were four
stolen horses among them?

464
01:18:17,440 --> 01:18:21,991
Where did you sell them?
-At the fair, in Dobanovci.

465
01:18:23,040 --> 01:18:28,956
That's where Mirta
attacked you with a knife,

466
01:18:29,240 --> 01:18:34,951
then you killed him in the shed.
-I swear, I'm innocent!

467
01:18:35,400 --> 01:18:42,476
Who else would have done it?
-I swear to God, I'm innocent!

468
01:18:43,240 --> 01:18:48,234
We have witnesses. Better confess,
then you'll get a lighter sentence.

469
01:18:48,800 --> 01:18:53,954
I'm innocent, I swear on my
children! I'll do anything!

470
01:19:01,800 --> 01:19:06,954
Take him to solitary,
to refresh his memory.

471
01:19:11,600 --> 01:19:16,958
Bora, the feather dealer, killed
Mirta. The whole town knows that.

472
01:19:18,040 --> 01:19:21,032
Where's Bora, the feather dealer?
-I don't know.

473
01:19:25,160 --> 01:19:28,994
Come here. Have you seen
Bora Petrovic? -No.

474
01:19:29,960 --> 01:19:32,474
I haven't seen him.
I don't know.

475
01:19:35,200 --> 01:19:39,113
He's disappeared.
Bora hasn't been around.

476
01:19:45,000 --> 01:19:47,673
Damn police, leave us alone!

477
01:19:51,320 --> 01:19:53,550
I haven't seen him.

478
01:19:55,960 --> 01:19:58,554
He's been gone
for a long time.

479
01:19:59,000 --> 01:20:00,513
Step outside.

480
01:20:03,000 --> 01:20:07,039
Have you seen Bora Petrovic?
-Never heard of him.

481
01:20:14,480 --> 01:20:17,631
Haven't seen him
for a long time.

482
01:20:20,360 --> 01:20:24,035
He used to buy feathers.

483
01:20:27,720 --> 01:20:31,508
How should I know.
I don't want to see him.

484
01:20:32,680 --> 01:20:35,592
Bora has just disappeared.

