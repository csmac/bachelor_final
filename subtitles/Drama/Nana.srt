1
00:00:06,130 --> 00:00:09,622
Hachi! Don't just stand there!

2
00:00:09,834 --> 00:00:11,301
This is your fight!

3
00:00:15,206 --> 00:00:18,664
He's your boyfriend. Win him back!

4
00:00:27,151 --> 00:00:28,641
I don't want to.

5
00:00:35,927 --> 00:00:38,259
I never want to see him again.

6
00:02:42,119 --> 00:02:44,314
I want to do it in a bigger place.

7
00:02:47,992 --> 00:02:51,052
Yeah, the bathtub's too small.

8
00:02:52,630 --> 00:02:54,427
No, I'm talking about our gigs.

9
00:02:54,565 --> 00:02:55,554
Oh.

10
00:02:56,333 --> 00:02:59,860
We attract huge crowds.
We should book a bigger venue.

11
00:03:00,838 --> 00:03:01,930
Do it then.

12
00:03:02,973 --> 00:03:05,032
Don't you care?

13
00:03:10,314 --> 00:03:15,513
What was Yasu talking about earlier?
It sounded serious.

14
00:03:24,862 --> 00:03:25,829
Ah, my pills.

15
00:03:31,402 --> 00:03:33,563
You're such a junkie.

16
00:03:34,271 --> 00:03:38,173
No, the pill, you idiot!
You don't use protection.

17
00:03:38,743 --> 00:03:40,608
What if I get pregnant?

18
00:03:42,947 --> 00:03:45,177
Then we'd raise kids.

19
00:03:45,416 --> 00:03:49,352
I worked all through high school,
I'm loaded.

20
00:03:49,620 --> 00:03:55,752
I could support a kid or two.

21
00:03:57,261 --> 00:04:02,130
Maybe you could, but not me.
I want to play gigs...

22
00:04:28,759 --> 00:04:29,817
NANA...

23
00:04:33,264 --> 00:04:37,826
Live your life the way you want.

24
00:04:39,470 --> 00:04:40,630
What?

25
00:04:42,540 --> 00:04:44,474
I'm going to Tokyo.

26
00:05:09,667 --> 00:05:14,604
I agree, Shoji's to blame.
But so are you, in part.

27
00:05:14,805 --> 00:05:18,036
Junko, whose side are you on?
He cheated on me.

28
00:05:18,275 --> 00:05:19,936
He's been unfaithful all this time!

29
00:05:20,177 --> 00:05:21,838
Not all this time...

30
00:05:22,046 --> 00:05:26,881
I did everything he wanted.
Found a job, was independent...

31
00:05:27,084 --> 00:05:31,043
That's got nothing to do with it.

32
00:05:33,023 --> 00:05:37,221
You know, all you ever really
think about is yourself.

33
00:05:37,561 --> 00:05:40,553
Did you ever think how
Shoji really felt?

34
00:05:42,900 --> 00:05:46,131
It takes two people
to make a relationship work.

35
00:05:46,704 --> 00:05:50,299
You've got to think about your partner!

36
00:06:28,445 --> 00:06:31,380
We're the Black Stones.

37
00:06:32,650 --> 00:06:38,782
The wide-open window,
The deep sky dances above

38
00:06:39,056 --> 00:06:43,220
Ah, look up

39
00:06:48,032 --> 00:06:54,562
What's the meaning
of each passing day?

40
00:06:54,805 --> 00:07:00,835
Ah, you scream
Run away from home, go

41
00:07:01,111 --> 00:07:07,516
Put on your worn rocking shoes
Hop over the puddle

42
00:07:07,718 --> 00:07:12,485
Flashback
You're clever

43
00:07:12,690 --> 00:07:15,158
Ah, remember

44
00:07:15,693 --> 00:07:21,563
Over that rainbow

45
00:07:22,333 --> 00:07:27,134
I wanna return to that morning

46
00:07:28,505 --> 00:07:34,171
Gathering our dreams

47
00:07:35,212 --> 00:07:39,911
When we walked together,
glamorous days

48
00:07:55,165 --> 00:08:03,368
Is there any value to an empty love?

49
00:08:03,440 --> 00:08:08,241
Ah, you scream
Let it all out, go

50
00:08:08,445 --> 00:08:14,782
Drink up, rock'n roll
A breathless battle

51
00:08:15,019 --> 00:08:19,547
Flashback
Your flavor

52
00:08:19,757 --> 00:08:22,351
Ah, remember

53
00:08:22,860 --> 00:08:28,696
Let's go collect those stars

54
00:08:29,199 --> 00:08:34,330
I wanna wear them like a necklace

55
00:08:35,706 --> 00:08:41,576
We strung the dreams together

56
00:08:42,413 --> 00:08:51,788
As we danced, glamorous days

57
00:08:52,056 --> 00:09:01,192
I can't sleep

58
00:09:21,618 --> 00:09:23,586
Does Nana Komatsu live here?

59
00:09:23,787 --> 00:09:24,913
Yes.

60
00:09:26,156 --> 00:09:27,953
Sign here, please.

61
00:09:37,901 --> 00:09:41,393
Hachi. It's a registered
letter from your folks.

62
00:09:42,306 --> 00:09:43,967
I'll sign for it.

63
00:09:45,342 --> 00:09:47,242
Please.

64
00:09:48,846 --> 00:09:49,835
Thank you.

65
00:10:16,940 --> 00:10:22,845
Wow! I won!

66
00:10:23,647 --> 00:10:27,139
I won tickets to a Trapnest concert.

67
00:10:27,584 --> 00:10:29,313
2 front row tickets!

68
00:10:29,520 --> 00:10:35,789
I'll be able to see Takumi
right in front of me.

69
00:10:35,959 --> 00:10:39,827
You'd never get tickets like this
in Tokyo. You're coming, right?

70
00:10:41,198 --> 00:10:45,692
Right? NANA.
Right?

71
00:11:01,218 --> 00:11:03,413
Hey, hey, hey, hey!

72
00:11:04,054 --> 00:11:05,544
Everybody, listen up!

73
00:11:06,557 --> 00:11:09,617
Trapnest are playing a gig
back home next week.

74
00:11:09,860 --> 00:11:11,794
Who wants to come?

75
00:11:24,007 --> 00:11:27,135
I asked NANA,
but she wasn't interested.

76
00:11:27,344 --> 00:11:31,041
It's a long way to travel,
but they're front row seats.

77
00:11:31,281 --> 00:11:32,873
What a waste not to go.

78
00:11:35,085 --> 00:11:37,451
No, NANA wouldn't want to go.

79
00:11:38,956 --> 00:11:41,288
She hasn't told you anything?

80
00:11:45,562 --> 00:11:47,052
Ren and NANA?

81
00:11:50,500 --> 00:11:51,432
Yeah.

82
00:11:52,502 --> 00:11:55,232
Ren from Trapnest and NANA?

83
00:11:55,706 --> 00:11:58,766
Ren wasn't always in Trapnest.

84
00:11:59,343 --> 00:12:04,007
He played in our band back home
until Trapnest recruited him.

85
00:12:04,848 --> 00:12:07,043
I see...

86
00:12:10,654 --> 00:12:12,519
Why aren't you surprised?

87
00:12:13,924 --> 00:12:18,452
You told me everything the other night,
when you were drunk.

88
00:12:19,263 --> 00:12:21,390
Did I? Really?

89
00:12:21,565 --> 00:12:22,395
Yeah.

90
00:12:24,134 --> 00:12:25,158
Oops!

91
00:12:27,371 --> 00:12:32,172
But why didn't NANA follow Ren
to Tokyo?

92
00:12:32,376 --> 00:12:35,777
They still could have stayed together.

93
00:12:36,213 --> 00:12:39,944
NANA didn't want to center
her life around Ren's career.

94
00:12:40,117 --> 00:12:43,450
She wanted to make it on her own.

95
00:12:43,620 --> 00:12:46,748
So she made a tough decision.

96
00:12:47,958 --> 00:12:50,654
How come you're telling the story?

97
00:12:50,827 --> 00:12:52,852
Because I heard it from you.

98
00:12:55,399 --> 00:12:56,457
Oh, really?

99
00:12:59,836 --> 00:13:04,330
Is that really what NANA wants?

100
00:13:10,414 --> 00:13:13,110
She's an independent woman.

101
00:13:14,251 --> 00:13:16,742
With Ren's career on a roll,

102
00:13:16,987 --> 00:13:20,787
she knew she couldn't play housewife...

103
00:13:22,459 --> 00:13:26,691
So she worked on her singing,
and built up her confidence.

104
00:13:26,897 --> 00:13:31,527
And vowed she'd come to Tokyo
someday when the time was right.

105
00:13:32,569 --> 00:13:35,129
She chose pride over love.

106
00:13:36,873 --> 00:13:38,465
Simply put, yeah.

107
00:13:40,610 --> 00:13:45,377
But who knows how she really feels?

108
00:13:45,582 --> 00:13:52,112
She keeps her feelings
about Ren to herself.

109
00:14:09,306 --> 00:14:12,070
Be better than Ren.

110
00:14:40,604 --> 00:14:42,037
What are you doing?

111
00:14:42,839 --> 00:14:44,739
Need any help?

112
00:14:46,209 --> 00:14:49,667
No... NANA...

113
00:14:53,950 --> 00:14:57,215
I still want you
to come with me to see Trapnest.

114
00:15:02,492 --> 00:15:04,960
You don't have to go to the concert.

115
00:15:05,162 --> 00:15:08,029
But I'd like you to see my hometown.

116
00:15:08,231 --> 00:15:10,529
Not that there's much to see.

117
00:15:10,700 --> 00:15:14,295
I could introduce you to my folks.

118
00:15:16,773 --> 00:15:19,469
I'll go to the concert.

119
00:15:20,811 --> 00:15:21,800
Really?

120
00:15:22,412 --> 00:15:23,538
Sure.

121
00:15:24,080 --> 00:15:25,479
Why the change of mind?

122
00:15:27,517 --> 00:15:28,575
No reason.

123
00:15:29,085 --> 00:15:30,416
I'd like to check them out.

124
00:15:31,221 --> 00:15:32,381
Trapnest?

125
00:15:34,291 --> 00:15:35,986
Your parents.

126
00:16:14,598 --> 00:16:17,897
What a nice house! You must be rich.

127
00:16:18,034 --> 00:16:21,026
Hardly. Property is cheap around here.

128
00:16:26,476 --> 00:16:29,343
Welcome!
Make yourself at home.

129
00:16:29,746 --> 00:16:31,179
Hello!

130
00:16:31,581 --> 00:16:32,878
Pleased to meet you.

131
00:16:34,251 --> 00:16:36,276
Welcome back.

132
00:16:37,654 --> 00:16:40,088
I thought you were bringing a man home.

133
00:16:40,223 --> 00:16:42,623
Don't say that, Nami!

134
00:16:42,993 --> 00:16:47,953
You left in such a hurry,
we thought you'd eloped.

135
00:16:48,198 --> 00:16:49,256
Yeah.

136
00:16:49,466 --> 00:16:54,529
Don't be silly. I live with NANA.
No boyfriend.

137
00:16:54,771 --> 00:16:55,533
What?

138
00:16:55,705 --> 00:16:56,694
Right?

139
00:16:56,907 --> 00:16:58,841
That's too bad.

140
00:16:59,009 --> 00:17:02,706
I was looking forward to meeting
someone... who could put up with you,

141
00:17:03,046 --> 00:17:07,176
to take you off my hands.

142
00:17:08,485 --> 00:17:10,578
Sorry. We're a noisy bunch.

143
00:17:11,187 --> 00:17:12,245
Not at all.

144
00:17:12,455 --> 00:17:14,889
Please come in.

145
00:17:16,927 --> 00:17:21,660
Your mother's awesome. I love her.

146
00:17:21,865 --> 00:17:25,266
Just your average,
middle-aged housewife.

147
00:17:25,468 --> 00:17:28,369
My father should be home soon.
He's average too.

148
00:17:28,605 --> 00:17:30,334
Don't get your hopes up.

149
00:17:31,908 --> 00:17:36,106
What's wrong with being average?
I don't have parents.

150
00:17:36,346 --> 00:17:41,045
I used to dream of living in a house

151
00:17:43,019 --> 00:17:44,577
No parents?
With a large family.

152
00:17:47,691 --> 00:17:53,960
Yeah, my mom left home
when I was 4.

153
00:17:54,197 --> 00:17:58,395
I never had a father,
so I lived with my grandma.

154
00:17:58,868 --> 00:18:02,497
She was my mom's mother.
But she died when I was 15.

155
00:18:06,276 --> 00:18:09,211
Don't look so depressed.

156
00:18:12,182 --> 00:18:13,444
I'm so happy.

157
00:18:14,284 --> 00:18:15,376
What?

158
00:18:16,086 --> 00:18:18,850
You never told me this before.

159
00:18:19,089 --> 00:18:25,187
You never talk about yourself.
It made me feel lonely.

160
00:18:26,963 --> 00:18:28,760
I see...

161
00:18:29,232 --> 00:18:30,927
I never thought about it.

162
00:18:31,868 --> 00:18:34,860
You only had to ask.

163
00:18:35,171 --> 00:18:37,332
No need to feel lonely.

164
00:18:39,776 --> 00:18:41,676
Something funny?

165
00:18:42,012 --> 00:18:44,913
You're so cool!

166
00:18:45,081 --> 00:18:50,144
You're more like a boyfriend than
a girlfriend. I think I'm falling in love.

167
00:18:50,387 --> 00:18:52,184
You're hopeless.

168
00:18:53,590 --> 00:18:57,356
Will you really tell me anything if I ask?

169
00:18:58,228 --> 00:18:59,195
Yeah.

170
00:18:59,329 --> 00:19:01,160
You'll answer me straight?

171
00:19:01,297 --> 00:19:02,559
What is it?

172
00:19:04,634 --> 00:19:06,829
Do you still love Ren?

173
00:19:20,316 --> 00:19:21,715
You knew?

174
00:19:23,520 --> 00:19:28,685
Sorry. I pretended not to. But...

175
00:19:32,362 --> 00:19:36,423
You brought me here knowing that?

176
00:19:40,103 --> 00:19:41,229
Thanks.

177
00:19:59,789 --> 00:20:02,883
Trapnest concert this way!

178
00:20:09,799 --> 00:20:11,926
I'm going to have a smoke.

179
00:20:12,569 --> 00:20:13,433
OK.

180
00:20:19,142 --> 00:20:23,306
NANA didn't give me a straight answer.

181
00:20:23,747 --> 00:20:28,150
But she didn't need to.

182
00:21:48,231 --> 00:21:52,031
He's 2 years older than me.
I'll introduce you to him later.

183
00:21:52,268 --> 00:21:54,293
You'll love him.

184
00:21:54,504 --> 00:21:58,304
Who cares? I came to watch the gigs.

185
00:21:59,309 --> 00:22:03,939
Ren was abandoned in the warehouse
district soon after he was born.

186
00:22:04,113 --> 00:22:07,708
Made the news. He still lives there.

187
00:22:07,917 --> 00:22:10,579
He says that's where he belongs.

188
00:22:11,221 --> 00:22:15,783
Wants to be a professional musician.
A good background story can help.

189
00:22:15,959 --> 00:22:20,225
That's pathetic.
Cashing in on one's misfortune.

190
00:22:20,597 --> 00:22:21,723
I don't buy it.

191
00:22:21,865 --> 00:22:24,891
Uh oh, we'll be late!

192
00:23:05,541 --> 00:23:13,346
The orange sky
that shines down upon the city

193
00:23:13,516 --> 00:23:25,986
But I won't look back, I've let go of
yesterday and started on my journey

194
00:23:26,162 --> 00:23:30,622
My heart is driven by passion,
not delusion

195
00:23:30,833 --> 00:23:37,204
It's strange, but I'm not frightened
Searching for myself

196
00:23:37,407 --> 00:23:43,971
Overcoming unhappiness
- so far away

197
00:23:44,213 --> 00:23:51,119
My dreams are packed up in a suitcase

198
00:23:51,321 --> 00:23:56,224
Always chasing that
distant shining light

199
00:24:25,621 --> 00:24:29,489
I ran through the snowstorm

200
00:24:29,892 --> 00:24:33,123
The red dress hidden beneath my coat.

201
00:24:40,103 --> 00:24:45,097
I don't know how to describe the
feeling that was born that night.

202
00:24:45,775 --> 00:24:48,903
My life had been dark for so long that

203
00:24:49,112 --> 00:24:52,809
Ren's light was dazzling.

204
00:24:58,988 --> 00:25:03,891
Ren and I first made love the
Christmas one year after we met.

205
00:25:12,869 --> 00:25:14,461
"For NANA"

206
00:25:14,704 --> 00:25:17,901
A homemade cake?
My fans love me.

207
00:25:20,076 --> 00:25:24,240
It could be poisoned.
It's actually from one of my fans.

208
00:25:24,814 --> 00:25:28,682
Poison for the girl who's seeing Ren.

209
00:25:28,985 --> 00:25:32,443
Taste it. I don't want to die for
something I haven't done.

210
00:25:33,256 --> 00:25:34,814
Neither do I.

211
00:25:35,358 --> 00:25:40,796
Fans should know band members never
touch the girl vocalist.

212
00:26:00,750 --> 00:26:04,413
Now I don't care if they poison me.

213
00:26:21,337 --> 00:26:24,204
I would have been happy to die.

214
00:26:25,541 --> 00:26:28,101
Because I wanted him.

215
00:26:28,478 --> 00:26:32,278
I wanted him so much.

216
00:26:44,427 --> 00:26:45,689
Lotus flower?

217
00:26:50,433 --> 00:26:52,333
Called a "Ren."

218
00:27:04,347 --> 00:27:08,408
"Ren Honjo
NANA Ozaki"

219
00:27:13,422 --> 00:27:17,415
Ren taught me the joy of singing.

220
00:27:17,960 --> 00:27:19,928
He taught me how to play the guitar.

221
00:27:20,563 --> 00:27:22,861
He gave me hope.

222
00:27:23,966 --> 00:27:28,994
I was pulled towards him like
the tide to the moon.

223
00:27:39,081 --> 00:27:43,643
But Ren wasn't the reason I sang.

224
00:27:44,754 --> 00:27:49,987
I always sang for myself. And I still do.

225
00:28:23,960 --> 00:28:29,455
I watched Ren's every move,
trying to get him to look this way...

226
00:28:29,699 --> 00:28:34,329
hoping that he'd notice
NANA standing there.

227
00:28:36,472 --> 00:28:38,372
Could I reach him?

228
00:28:43,713 --> 00:28:47,672
If you haven't changed your mind

229
00:28:47,883 --> 00:28:53,515
Stay by my side, tonight

230
00:28:53,689 --> 00:28:59,628
Don't want to pretend to be strong

231
00:28:59,729 --> 00:29:08,433
I should be less childish
Every time I think about you baby

232
00:29:08,704 --> 00:29:19,478
I can say it now - I miss you

233
00:29:19,682 --> 00:29:25,518
It is so hard to say I'm sorry

234
00:29:32,895 --> 00:29:34,624
No luggage?

235
00:29:35,531 --> 00:29:39,592
Just a guitar and cigarettes
are all I need.

236
00:29:40,569 --> 00:29:44,767
He's bullshitting. He's already
sent his stuff on ahead.

237
00:29:45,975 --> 00:29:48,876
Guess I won't be your porter anymore.

238
00:29:52,114 --> 00:29:54,048
Train's here.

239
00:31:29,512 --> 00:31:32,174
I lived with Ren for 15 months.

240
00:31:32,348 --> 00:31:35,408
Winter was coming to an end.

241
00:31:35,584 --> 00:31:38,644
And so were we.

242
00:32:03,646 --> 00:32:07,639
Sometimes, I sensed Ren
glancing our way.

243
00:32:07,850 --> 00:32:12,878
But his eyes always
seemed elsewhere... far away.

244
00:32:17,793 --> 00:32:23,493
It is more for you
and not anyone else.

245
00:32:23,699 --> 00:32:26,668
That I sing this song

246
00:32:26,869 --> 00:32:37,643
A never ending story
continuously shining.

247
00:32:37,847 --> 00:32:54,185
Always to tell you eternally

248
00:33:39,708 --> 00:33:41,972
Ren, you missed a few notes
out there.

249
00:33:42,177 --> 00:33:44,702
You covered them up well.

250
00:33:45,848 --> 00:33:47,179
But I noticed.

251
00:33:49,084 --> 00:33:50,415
I gotta pee.

252
00:33:57,826 --> 00:34:00,420
Wow, even Ren makes mistakes...

253
00:34:01,664 --> 00:34:05,623
"Even Homer sometimes nods."
It's a saying.

254
00:34:08,470 --> 00:34:23,579
Encore! Encore!

255
00:34:27,957 --> 00:34:30,619
I wonder if Ren noticed you?

256
00:34:31,160 --> 00:34:34,254
Not under those bright lights.

257
00:34:34,463 --> 00:34:35,623
Really?

258
00:34:35,831 --> 00:34:40,666
Don't know. Never played
on a stage like this.

259
00:34:41,170 --> 00:34:42,535
You never know...

260
00:34:44,039 --> 00:34:48,703
Must feel really good playing
an arena like this.

261
00:34:56,518 --> 00:34:58,952
Yes, Yamazaki Law Office.

262
00:34:59,722 --> 00:35:02,213
Hey, Yasu. NANA's here!

263
00:35:03,325 --> 00:35:06,260
If you knew, why didn't you tell me?

264
00:35:06,862 --> 00:35:08,693
It was a surprise.

265
00:35:10,666 --> 00:35:15,160
It threw me off completely!
I recovered somehow, though.

266
00:35:20,275 --> 00:35:23,005
I'll give you her friend's number.

267
00:35:23,579 --> 00:35:24,739
Call her if you'd like.

268
00:35:25,914 --> 00:35:28,974
I wouldn't know what to say.

269
00:35:33,155 --> 00:35:38,218
If you're no longer interested...
I'll take her.

270
00:35:50,172 --> 00:35:55,439
Trapnest played 2 more songs,
but Ren never looked this way.

271
00:35:59,048 --> 00:36:00,106
Come in.

272
00:36:01,116 --> 00:36:02,344
We're back.

273
00:36:04,086 --> 00:36:05,610
Welcome home.

274
00:36:05,788 --> 00:36:09,121
You must be starving. Dinner's ready.

275
00:36:13,262 --> 00:36:14,627
Hello?

276
00:36:16,432 --> 00:36:20,266
Hold on. I'll pass you over.

277
00:36:20,969 --> 00:36:21,867
Yasu.

278
00:36:24,740 --> 00:36:26,640
What is it?

279
00:36:27,342 --> 00:36:30,641
Ren might call you.

280
00:36:31,480 --> 00:36:32,845
What's that mean?

281
00:36:36,518 --> 00:36:38,713
Stay out of my business!

282
00:36:39,321 --> 00:36:42,188
I've got nothing to say to him!

283
00:36:42,658 --> 00:36:44,819
Say that to his face.

284
00:36:46,929 --> 00:36:49,864
You've still got the key, right?

285
00:36:52,067 --> 00:36:53,659
Give it back.

286
00:36:55,070 --> 00:36:58,631
If you're through with him,
then set him free.

287
00:37:10,419 --> 00:37:14,685
Alright. Where's he staying?

288
00:37:20,162 --> 00:37:21,595
NANA...

289
00:37:23,899 --> 00:37:27,699
It's about time I did this.
I'm returning the key.

290
00:37:28,337 --> 00:37:29,269
Key?

291
00:37:31,039 --> 00:37:35,169
The key to the lock on his necklace.

292
00:37:35,477 --> 00:37:38,708
But if you return the key,
that would mean...

293
00:37:43,886 --> 00:37:46,855
Get me a cab.

294
00:38:19,221 --> 00:38:20,153
Excuse me.

295
00:38:21,957 --> 00:38:23,754
Are you on the guest list?

296
00:38:24,126 --> 00:38:24,990
Yes.

297
00:38:25,160 --> 00:38:27,958
Can I see some ID?

298
00:38:43,612 --> 00:38:46,638
She's with me. Let her through.

299
00:39:03,131 --> 00:39:05,656
I came to return... the key...

300
00:39:09,905 --> 00:39:11,702
Is that all?

301
00:39:13,308 --> 00:39:17,472
And to say that it's over.

302
00:39:21,283 --> 00:39:24,684
I don't like clinging to the past.

303
00:39:25,954 --> 00:39:26,784
Good luck!

304
00:39:26,922 --> 00:39:27,946
Wait!

305
00:39:30,125 --> 00:39:31,888
Don't get the wrong idea.

306
00:39:33,729 --> 00:39:36,493
I've got nothing to say to you.

307
00:39:40,168 --> 00:39:44,662
Oh, there is one thing.

308
00:39:47,209 --> 00:39:50,303
We'll be bigger than Trapnest someday.

309
00:39:54,216 --> 00:39:58,846
NANA. You haven't changed a bit.

310
00:40:00,155 --> 00:40:03,420
You're looking well.

311
00:40:51,807 --> 00:40:53,035
Ren...

312
00:40:56,545 --> 00:40:58,445
Let go!

313
00:40:58,647 --> 00:41:00,410
I don't want you back!

314
00:41:01,850 --> 00:41:03,545
Let go!

315
00:41:14,863 --> 00:41:16,353
I missed you.

316
00:41:20,435 --> 00:41:22,995
I wish I'd never come.

317
00:41:36,651 --> 00:41:41,145
I wonder how NANA felt
that night going to see Ren.

318
00:41:42,391 --> 00:41:45,451
I really wonder...

319
00:41:56,405 --> 00:41:57,064
"Shoji"

320
00:41:57,272 --> 00:41:59,035
"Delete"

321
00:41:59,274 --> 00:41:59,933
"Delete entry"

322
00:42:00,175 --> 00:42:01,164
"Delete Shoji?"

323
00:42:05,981 --> 00:42:08,142
"Yes"

324
00:42:10,552 --> 00:42:17,082
If her feelings for Ren

325
00:42:17,259 --> 00:42:19,853
she would have gone to him ages ago.

326
00:43:14,916 --> 00:43:17,976
Those roses are expensive.

327
00:43:18,320 --> 00:43:21,721
Don't be cheap, Mr. "Rock Star".

328
00:43:24,059 --> 00:43:28,496
Yeah. They'd only wither anyway.

329
00:43:30,398 --> 00:43:33,458
But you won't. Never.

330
00:43:38,340 --> 00:43:41,776
Feels like old times, huh?

331
00:43:41,977 --> 00:43:45,743
Sitting in the bathtub, chatting...

332
00:43:48,250 --> 00:43:50,878
I still rent that room.

333
00:43:51,052 --> 00:43:54,613
With no one living there? That's a waste.

334
00:43:56,091 --> 00:43:59,458
That's where I belong.

335
00:44:03,198 --> 00:44:07,498
One day I'll buy it and retire there,

336
00:44:07,802 --> 00:44:09,770
with you.

337
00:44:10,238 --> 00:44:12,706
Says who?

338
00:44:15,443 --> 00:44:19,777
It was an empty warehouse
when I was a kid.

339
00:44:19,948 --> 00:44:23,406
I used to escape from the orphanage
and hide there.

340
00:44:23,618 --> 00:44:25,518
I'd play guitar all night.

341
00:44:29,291 --> 00:44:32,419
I wasn't born talented.

342
00:44:32,627 --> 00:44:34,527
It's all from practice.

343
00:44:36,531 --> 00:44:39,364
You never told me that before.

344
00:44:41,403 --> 00:44:44,304
Never told anyone.

345
00:44:44,506 --> 00:44:47,373
I want people to think I'm gifted.

346
00:45:09,598 --> 00:45:10,895
Hey, Ren?

347
00:45:14,603 --> 00:45:16,036
It's no good.

348
00:45:19,474 --> 00:45:22,341
We can't live together like before.

349
00:45:24,112 --> 00:45:26,376
Can't get over my pride.

350
00:45:30,151 --> 00:45:32,915
But maybe we could meet sometimes.

351
00:45:33,154 --> 00:45:37,784
Make love... talk about our lives.

352
00:45:42,030 --> 00:45:46,194
And when we're older...

353
00:45:46,401 --> 00:45:49,802
when pride and fame mean nothing...

354
00:45:50,538 --> 00:45:52,733
when I'm tired of singing...

355
00:45:56,011 --> 00:45:59,378
I'll return to that warehouse with you.

356
00:46:55,570 --> 00:47:00,439
Back then, I didn't want to
fall in love again.

357
00:47:02,043 --> 00:47:06,070
But I knew that no matter
how hurt I was,

358
00:47:06,281 --> 00:47:08,647
I would keep on dreaming.

359
00:47:09,918 --> 00:47:12,853
And someday I would find love again.

360
00:47:13,021 --> 00:47:18,687
That's how I felt as I prayed for
NANA's happiness.

361
00:47:30,472 --> 00:47:33,441
How did it go with Ren?

362
00:47:39,047 --> 00:47:42,414
You promised you'd tell me anything.

363
00:47:45,120 --> 00:47:48,283
I owe you one this time.

364
00:47:50,825 --> 00:47:53,521
Anything I can get for you in return?

365
00:47:53,695 --> 00:47:55,162
Takumi's autograph?

366
00:47:55,363 --> 00:47:57,831
That's easy. I'll ask Ren.

367
00:47:58,032 --> 00:47:59,192
Ren?

368
00:47:59,667 --> 00:48:01,498
It's only an autograph.

369
00:48:01,703 --> 00:48:04,866
No. Did you get back with him?

370
00:48:06,474 --> 00:48:08,305
Something like that...

371
00:48:08,843 --> 00:48:10,140
That's great, NANA.

372
00:48:10,345 --> 00:48:15,078
No it isn't. I don't know what happened.

373
00:48:15,650 --> 00:48:17,515
It wasn't supposed to be this way.

374
00:48:17,719 --> 00:48:18,879
Why not?

375
00:48:21,489 --> 00:48:24,083
I wanted to end it.

376
00:48:24,626 --> 00:48:27,026
Make a clean break.

377
00:48:29,164 --> 00:48:34,534
How could I be so weak? I'm pathetic.

378
00:48:39,941 --> 00:48:44,344
NANA blushed and she seemed
smaller than usual.

379
00:48:44,512 --> 00:48:49,176
I'd never seen her so tame... so sweet.

380
00:48:53,755 --> 00:48:55,347
Ms. Komatsu!

381
00:48:55,557 --> 00:48:58,151
Can't you do something as
simple as this?

382
00:48:58,393 --> 00:49:01,226
You had enough time.

383
00:49:03,031 --> 00:49:08,128
When I was your age I took
my job more seriously.

384
00:49:08,369 --> 00:49:12,203
This isn't a game, you know.

385
00:49:12,407 --> 00:49:16,434
No matter how I try, I don't fit in here.

386
00:49:16,644 --> 00:49:20,307
I need to find a better job.

387
00:49:20,582 --> 00:49:22,516
Set your cell phone to silent mode!

388
00:49:25,119 --> 00:49:31,080
Is there nowhere in the world
that I belong?

389
00:49:35,430 --> 00:49:37,557
I envy NANA.

390
00:49:46,474 --> 00:49:47,771
Li-zhi!

391
00:49:47,942 --> 00:49:50,308
What? That was quick!

392
00:49:59,387 --> 00:50:00,615
What?

393
00:50:01,823 --> 00:50:02,949
Ron!

394
00:50:03,591 --> 00:50:05,149
No way!

395
00:50:05,393 --> 00:50:08,920
Li-zhi! Double East.

396
00:50:12,233 --> 00:50:16,829
You're too good at this. Go easy on us.

397
00:50:16,971 --> 00:50:18,370
I am.

398
00:50:25,847 --> 00:50:27,872
Hang on...

399
00:50:30,785 --> 00:50:32,184
Hachi?

400
00:50:35,290 --> 00:50:36,917
Could be.

401
00:50:38,726 --> 00:50:40,489
You're on!

402
00:51:03,151 --> 00:51:04,379
Welcome home...

403
00:51:05,687 --> 00:51:07,052
Nana.

404
00:51:23,671 --> 00:51:25,730
I made her cry.

405
00:51:31,312 --> 00:51:34,440
The reason I started to cry...

406
00:51:34,649 --> 00:51:36,640
was because I knew this was
NANA's way of saying thank you.

407
00:51:36,918 --> 00:51:40,854
Her gift to me.

408
00:51:48,463 --> 00:51:49,725
Here.

409
00:52:22,597 --> 00:52:26,556
I felt an overwhelming sense of love.

410
00:52:26,768 --> 00:52:29,134
I'd never felt happier.

411
00:52:45,987 --> 00:52:48,854
I know I can't go through life

412
00:52:49,023 --> 00:52:52,254
expecting the world to come to me.

413
00:52:52,393 --> 00:52:56,557
But NANA let me dream
a wonderful dream.

414
00:53:01,335 --> 00:53:05,294
It was like falling in love
for the very first time.

415
00:53:09,043 --> 00:53:17,849
The wide-open window
The deep sky dances above

416
00:53:18,086 --> 00:53:20,850
Ah, look up"

417
00:53:24,726 --> 00:53:31,393
What's the meaning
of each passing day?

418
00:53:31,799 --> 00:53:37,396
Ah, you scream
Run away from home, go

419
00:53:37,572 --> 00:53:40,735
Put on your worn rocking shoes

420
00:53:40,908 --> 00:53:44,844
Hop over the puddle

421
00:53:45,012 --> 00:53:49,142
Flashback
You're clever

422
00:53:49,317 --> 00:53:51,945
Ah, remember

423
00:53:52,386 --> 00:53:58,291
Over that rainbow

424
00:53:59,060 --> 00:54:03,929
I wanna return to that morning

425
00:54:05,233 --> 00:54:11,433
Gathering our dreams

426
00:54:11,873 --> 00:54:17,641
When we walked together
glamorous days

