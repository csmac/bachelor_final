1
00:00:13,762 --> 00:00:19,632
"Yu Yu Hakusho"

2
00:00:19,701 --> 00:00:27,301
In a crowded city,
as I bump shoulders, I'm all alone.

3
00:00:27,375 --> 00:00:33,644
On an endless prairie,
as the wind whistles by, I'm all alone.

4
00:00:33,715 --> 00:00:41,315
Which one is it that makes
me want to cry more, I wonder.

5
00:00:41,389 --> 00:00:49,660
Marking it with twin circles,
I feel a bit more grown-up!

6
00:00:49,731 --> 00:00:53,929
It must be, that when I run into
these terribly difficult walls,

7
00:00:54,002 --> 00:00:56,994
and unexpectedly, for whatever reason,

8
00:00:57,072 --> 00:01:04,911
The courage and power to break
through rises up from within,

9
00:01:04,980 --> 00:01:11,943
All because of how, when I run
into these terribly difficult people,

10
00:01:12,020 --> 00:01:19,290
they show me kindness, I bet!

11
00:01:19,361 --> 00:01:23,764
Thank you very much!

12
00:02:07,889 --> 00:02:11,222
C- call an ambulance! An ambulance!

13
00:02:12,227 --> 00:02:14,388
It wasn't my fault...!

14
00:02:26,241 --> 00:02:33,113
Urameshi Yusuke, age 14. He was supposed
to have been the hero of this story, but...

15
00:02:33,181 --> 00:02:36,378
He seems to have suddenly died!

16
00:02:41,900 --> 00:02:44,630
"Surprised To Be Dead"

17
00:02:48,342 --> 00:02:51,903
What? That's me lying down there, isn't it?

18
00:03:02,356 --> 00:03:05,018
--All right, move aside, move aside!
--Please, make way!

19
00:03:08,029 --> 00:03:11,624
The child is okay! He just has a scratch!

20
00:03:11,699 --> 00:03:13,963
This one's not gonna make it.

21
00:03:16,370 --> 00:03:18,634
--Okay, let's put him on the stretcher.
--Right!

22
00:03:18,706 --> 00:03:21,300
Hey! Wait a minute, come on!

23
00:03:21,375 --> 00:03:25,311
At any rate, let's hurry and take him
and the little boy to the ambulance!

24
00:03:25,379 --> 00:03:27,904
Hey, I said wait, Pops!

25
00:03:27,982 --> 00:03:31,110
I'm right here! That's not me! Hey!

26
00:03:32,320 --> 00:03:34,049
I'm talking to you!

27
00:03:44,999 --> 00:03:52,269
What the hell is going on here?
I gotta calm down and remember! Um...

28
00:03:52,340 --> 00:03:56,936
As I remember, today I went to
school for the first time in ten days...

29
00:03:57,011 --> 00:04:01,812
Urameshi Yusuke, come see Takenaka in the
guidance office immediately! Urameshi!

30
00:04:01,882 --> 00:04:02,974
Urameshi, you got that!?

31
00:04:09,023 --> 00:04:10,581
Oh, It's you, Keiko.

32
00:04:13,027 --> 00:04:14,961
Here you are, all right!

33
00:04:18,366 --> 00:04:22,302
Stop this! Stop skipping your classes up
here! Ah, you're even in your street shoes!

34
00:04:22,370 --> 00:04:25,305
Always with the annoying lectures!

35
00:04:25,373 --> 00:04:26,670
What do you mean, annoying!?

36
00:04:26,741 --> 00:04:30,905
You're not going to make it to the
next grade if you skip too much!

37
00:04:30,978 --> 00:04:33,412
Takenaka-sensei has been
paging you for a bit.

38
00:04:33,481 --> 00:04:36,245
Unbelievable! Once you do come,
you're called out right away!

39
00:04:36,317 --> 00:04:38,251
You've done something
wrong again, haven't you?

40
00:04:38,319 --> 00:04:41,152
Anyhow, if you don't go,
as a class representative,

41
00:04:41,222 --> 00:04:43,053
I'll be yelled at as well, you know.

42
00:04:43,124 --> 00:04:44,921
Do you understand!?

43
00:04:46,694 --> 00:04:48,594
White, huh?

44
00:04:54,335 --> 00:04:57,771
You idiot! Grow up! Die! You pervert!

45
00:05:01,342 --> 00:05:05,608
How low! He hasn't grown up a
bit since way back when!

46
00:05:05,680 --> 00:05:09,275
--Indeed!
--Keiko!

47
00:05:09,350 --> 00:05:10,612
Are you done?

48
00:05:10,685 --> 00:05:13,279
What are you sneaking around about?

49
00:05:13,354 --> 00:05:16,619
You see, we're afraid of
Urameshi-kun, is the thing.

50
00:05:18,359 --> 00:05:21,624
You sure aren't bothered by talking to him.

51
00:05:21,696 --> 00:05:24,290
If you're together with Urameshi-kun, you're
going to get a bad reputation, too, Keiko!

52
00:05:24,365 --> 00:05:25,957
Is that it?

53
00:05:26,033 --> 00:05:30,231
Sure, he's a vulgar barbarian,
but he's no real harm.

54
00:05:30,304 --> 00:05:32,898
But the stories about
Urameshi-kun are awful!

55
00:05:32,973 --> 00:05:36,067
They say he's sought after by
bad guys throughout the city!

56
00:05:36,143 --> 00:05:39,579
And that anywhere he sets off to,
two thousand men will move with him!

57
00:05:39,647 --> 00:05:43,583
Hold on, it's nothing at all like that!

58
00:05:43,651 --> 00:05:47,849
He couldn't get two people to
move, let alone two thousand!

59
00:05:47,922 --> 00:05:49,583
He only has a few friends, after all.

60
00:05:49,657 --> 00:05:52,217
--Even so...
--I'm still afraid of him!

61
00:05:55,329 --> 00:06:01,268
Urameshi! Urameshi! Report to
the guidance office immediately!

62
00:06:01,335 --> 00:06:03,599
That Takenaka jerk's so persistent.

63
00:06:03,671 --> 00:06:07,937
Seriously? He left you
a wallet? What a bonus!

64
00:06:08,008 --> 00:06:11,034
The guy tried to start
something with me downtown,

65
00:06:11,112 --> 00:06:13,945
so I told him that I was Urameshi's cousin!

66
00:06:14,014 --> 00:06:18,610
You are so bad! If this gets
out, you're gonna get killed!

67
00:06:18,686 --> 00:06:22,986
No problem! "Even fools and blunt scissors
are useful to the skilled," after all.

68
00:06:25,359 --> 00:06:26,189
What is it?

69
00:06:29,964 --> 00:06:32,228
U- Urameshi-kun!

70
00:06:32,299 --> 00:06:33,493
S- sorry!

71
00:06:36,637 --> 00:06:40,232
I don't want that crap!

72
00:06:40,307 --> 00:06:43,572
Hey, what are you doing there!

73
00:06:43,644 --> 00:06:45,578
Iwamoto-sensei!

74
00:06:45,646 --> 00:06:50,242
You've got nothing to worry about
now! What has he done to you?

75
00:06:50,317 --> 00:06:51,841
Nothing, nothing at all...

76
00:06:55,322 --> 00:07:01,261
Oh, so you're mugging people at
school in broad daylight? You scum!

77
00:07:01,328 --> 00:07:02,590
What?

78
00:07:02,663 --> 00:07:07,600
There's no need for the likes of you
to come to school! Get out, now!

79
00:07:07,668 --> 00:07:10,262
Shut your hole! That's up to me to decide.

80
00:07:21,682 --> 00:07:24,617
Sheesh. Play me for the fool, will he?

81
00:07:29,290 --> 00:07:32,225
Ow, man! Who did that?

82
00:07:32,293 --> 00:07:35,888
--Takenaka!
--Add "-sensei" to that, "-sensei!"

83
00:07:35,963 --> 00:07:38,557
Didn't you hear? I've been
paging you for a while!

84
00:07:38,632 --> 00:07:42,898
Stop bugging me! I'm leaving
'cause I was told to leave.

85
00:07:42,970 --> 00:07:45,564
No doubt it's because you've
done something wrong again!

86
00:07:45,639 --> 00:07:48,574
I haven't done nothing!

87
00:07:48,642 --> 00:07:51,907
Well, come to the guidance office
anyway. I'll offer you some tea!

88
00:07:51,979 --> 00:07:56,245
Today, I'm not letting you go home till
night. Are you listening to me? Mm!?

89
00:07:59,987 --> 00:08:01,579
It's just a toy.

90
00:08:01,655 --> 00:08:04,249
Stay out of my business, would you?

91
00:08:06,327 --> 00:08:10,263
Wait, Yusuke!

92
00:08:10,331 --> 00:08:12,856
Sheesh! When I finally do
feel like going to school,

93
00:08:12,933 --> 00:08:15,595
everybody gives me some smug lecture...

94
00:08:15,669 --> 00:08:19,469
They can't be serious! I'm sick of it!

95
00:08:23,010 --> 00:08:25,274
What, are you just waking up?

96
00:08:25,346 --> 00:08:27,541
Make some coffee.

97
00:08:27,615 --> 00:08:31,210
Jeez, what's the use?

98
00:08:31,285 --> 00:08:36,222
--Aren't you supposed to be at school?
--It made me angry, so I took off.

99
00:08:36,290 --> 00:08:41,227
If you're not going to go, just up and
quit. School's not free, you know.

100
00:08:41,295 --> 00:08:46,232
You're gonna give me a lecture
too, Ma? I've had enough, okay?

101
00:08:46,300 --> 00:08:48,063
If you don't like it, you can leave home.

102
00:08:48,135 --> 00:08:50,160
Not that you've got what
it takes to make it alone.

103
00:08:55,643 --> 00:08:59,636
This is not my day, dammit!
I'm really getting pissed off!

104
00:09:10,324 --> 00:09:12,918
Urameshi...

105
00:09:12,993 --> 00:09:16,929
You're such a pest,
Kuwabara! Every single day!

106
00:09:16,997 --> 00:09:21,593
Shut up and fight me! I am Number
One at Sarayashiki Middle School!

107
00:09:21,669 --> 00:09:26,936
I'll have you licking the soles
of my shoes today, for sure!

108
00:09:27,007 --> 00:09:31,239
I'm really in a bad mood today,
see? I'm gonna beat you senseless!

109
00:09:40,621 --> 00:09:42,555
So strong!

110
00:09:42,623 --> 00:09:44,056
He's a demon!

111
00:09:44,959 --> 00:09:47,325
Ah, that was refreshing.

112
00:09:51,966 --> 00:09:56,062
Kuwabara-san, you should stop picking
fights with Uramashi, already.

113
00:09:56,136 --> 00:09:59,230
You have 0 wins,
156 losses now, Kuwabara-san.

114
00:09:59,306 --> 00:10:00,568
Shut the hell up!

115
00:10:00,641 --> 00:10:03,371
I'll do this until I've won!

116
00:10:04,645 --> 00:10:05,634
Kuwabara-san!

117
00:10:08,983 --> 00:10:12,646
Right, after that I met that child...

118
00:10:18,993 --> 00:10:20,517
Thanks!

119
00:10:22,329 --> 00:10:27,528
Look, you brat! It's dangerous here!
Lots of cars are going by, you know!

120
00:10:42,282 --> 00:10:44,773
What a fool! What a fool!

121
00:10:47,955 --> 00:10:52,892
Hm, my performance still
has what it takes, I guess.

122
00:10:52,960 --> 00:10:56,521
Listen, don't run out into the
road, 'cause it's dangerous, okay?

123
00:10:56,597 --> 00:10:58,963
Go play somewhere safe.

124
00:11:10,978 --> 00:11:15,677
He's still at it! There's just no
helping a stupid kid like that!

125
00:11:24,324 --> 00:11:27,225
Hey! That's dangerous!
Don't run out into the street!

126
00:11:29,997 --> 00:11:30,827
Hey!

127
00:11:34,668 --> 00:11:35,635
Watch out!

128
00:11:48,348 --> 00:11:52,284
That's right! I was hit by a car!

129
00:11:52,352 --> 00:11:54,286
Then I've died?

130
00:11:54,354 --> 00:11:58,290
Hold on-- then what am I doing here?

131
00:11:58,358 --> 00:12:00,952
Don't tell me I'm what you'd call a ghost!

132
00:12:01,028 --> 00:12:03,588
Bingo, bingo! Bin-go!

133
00:12:09,036 --> 00:12:11,527
I didn't expect you to be so
quick to understand.

134
00:12:24,618 --> 00:12:26,552
Who the hell are you?

135
00:12:26,620 --> 00:12:32,490
In a sudden accident such as this, there's so
many people who don't believe it, you know!

136
00:12:32,559 --> 00:12:34,151
I asked you who you were!

137
00:12:34,228 --> 00:12:39,165
I'm your guide across the
River Sanzu, Botan-chan!

138
00:12:39,233 --> 00:12:43,829
In the west, I guess I'd be what you'd
call the Grim Reaper. Pleased to meet you!

139
00:12:43,904 --> 00:12:46,839
There's no "pleased to meet
you" about it, okay, sweetheart?

140
00:12:46,907 --> 00:12:51,503
You should try not to give dirty looks
to servants from the Other World!

141
00:12:51,578 --> 00:12:54,741
Despite how I look,
I'm actually pretty shocked.

142
00:12:54,815 --> 00:12:57,181
Where do you get off
saying "Bingo!" you idiot?

143
00:12:57,251 --> 00:13:01,187
--Oh?
--Can't you act a little more serious!?

144
00:13:01,255 --> 00:13:05,783
To think that rather than being surprised
at seeing me, you heap abuse on me!

145
00:13:05,859 --> 00:13:08,521
I see, just as it's written in your grade book.

146
00:13:08,595 --> 00:13:10,722
Urameshi Yusuke, Age 14.

147
00:13:10,798 --> 00:13:13,528
Personality: Crude, violent,
short-tempered and reckless.

148
00:13:13,600 --> 00:13:19,197
On top of which, you're prone to shoplifting
and not too bright. Oh, you even did this?

149
00:13:19,273 --> 00:13:21,207
Isn't it a good thing that you died?

150
00:13:21,275 --> 00:13:24,870
It's none of your business!

151
00:13:24,945 --> 00:13:27,470
Damn...

152
00:13:27,548 --> 00:13:31,678
So, incidentally, what happened to
the kid I pushed out of the way?

153
00:13:31,752 --> 00:13:33,481
Is he injured or what?

154
00:13:33,554 --> 00:13:35,613
Shall we go check on him?

155
00:13:41,562 --> 00:13:43,553
His forehead and hand got skinned a bit,

156
00:13:43,630 --> 00:13:47,157
but there is nothing wrong
with his bones or brainwaves.

157
00:13:47,234 --> 00:13:48,792
Masaru...!

158
00:13:51,238 --> 00:13:54,173
So, he's okay, is he?

159
00:13:54,241 --> 00:13:56,801
Then, I'm cool. I have no
regrets in this world,

160
00:13:56,877 --> 00:13:59,710
so you can take me to
Hell or wherever you want.

161
00:14:01,248 --> 00:14:03,512
What's so funny!?

162
00:14:03,584 --> 00:14:07,850
You've got it wrong. I'm not
here to take you someplace.

163
00:14:07,921 --> 00:14:09,821
I came to ask you if you're
willing to undergo a trial

164
00:14:09,890 --> 00:14:11,858
in order to be returned to life.

165
00:14:11,925 --> 00:14:15,190
A trial to return to life?

166
00:14:15,262 --> 00:14:18,527
What sort of thing would that be?

167
00:14:18,599 --> 00:14:22,228
The truth is, see, your
death was unexpected,

168
00:14:22,302 --> 00:14:25,533
as far as the Reikai, or Spirit Realm goes.

169
00:14:25,606 --> 00:14:29,235
Who could have possibly known that you
would lay down your life to save a child?

170
00:14:29,309 --> 00:14:31,470
Not even Buddha himself thought so.

171
00:14:31,545 --> 00:14:35,481
There is still no place for you to
go, either in Paradise or in Hell!

172
00:14:35,549 --> 00:14:41,044
No place for me, you say!? Why not!?
I died in place of that kid, didn't I!?

173
00:14:42,222 --> 00:14:45,817
I wasn't going to say anything, because
of the shock it would be to you, but...

174
00:14:45,893 --> 00:14:48,191
Actually, that little boy was
supposed to have been hit by the car,

175
00:14:48,262 --> 00:14:53,825
but miraculously survive without a scratch!

176
00:14:53,901 --> 00:14:56,165
Without a scratch!?

177
00:14:56,236 --> 00:14:59,831
That's right. So, as hard as this is to say,

178
00:14:59,907 --> 00:15:03,399
Your death was in vain!

179
00:15:06,246 --> 00:15:10,842
Shocked, huh? Well, that's
probably to be expected.

180
00:15:10,918 --> 00:15:12,112
Hey now, calm down.

181
00:15:12,186 --> 00:15:15,519
As an alternative, I've told you
there's another chance, haven't I?

182
00:15:15,589 --> 00:15:21,186
You could undergo the trial to
return you to life, you know!

183
00:15:21,261 --> 00:15:23,957
It seems your case is
an unexpected happening

184
00:15:24,031 --> 00:15:26,192
that occurs once in a hundred years.

185
00:15:26,266 --> 00:15:29,463
Yeesh! This is ridiculous!
What a load of crap!

186
00:15:32,539 --> 00:15:35,007
If you stay like this,
you cannot rest in peace!

187
00:15:35,075 --> 00:15:39,478
It'd be better if you underwent
the trial, even if you think it's no use!

188
00:15:39,546 --> 00:15:43,482
How about it? It's not a bad offer, right?

189
00:15:43,550 --> 00:15:46,815
No, I'm fine!

190
00:15:46,887 --> 00:15:52,154
I'm fine as a ghost. Nothing good would
come of me returning to life, anyway.

191
00:15:52,226 --> 00:15:53,488
Oh?

192
00:15:53,560 --> 00:15:56,757
Everyone must be
relieved that I'm dead now.

193
00:15:56,830 --> 00:15:59,492
If I return to life, they would just spite me.

194
00:15:59,566 --> 00:16:04,162
My ma would have an easier life
if she didn't have me around.

195
00:16:04,238 --> 00:16:07,503
What a sad thing to say,
considering you're only 14 years old.

196
00:16:07,574 --> 00:16:12,170
So, there's no need for me to bother
with some trial, or return to life!

197
00:16:12,246 --> 00:16:15,181
Well, you don't have to rush like
this to come to your decision.

198
00:16:15,249 --> 00:16:22,178
Go watch your wake and think it over
carefully. I can get your answer then.

199
00:16:22,256 --> 00:16:25,191
I told you, I'm fine!

200
00:16:39,206 --> 00:16:42,801
Ah, it's the guys from school.

201
00:16:42,876 --> 00:16:47,813
Those bastards, here someone's died, and
they're there with their cheerful faces!

202
00:16:47,881 --> 00:16:49,815
Keiko!

203
00:16:49,883 --> 00:16:52,351
Yusuke...! Yusuke...!

204
00:16:56,223 --> 00:16:58,487
There's no reason for her to cry like that.

205
00:16:58,558 --> 00:17:01,493
Yusuke!

206
00:17:01,561 --> 00:17:03,153
T- this is no good, we should go back.

207
00:17:03,230 --> 00:17:09,829
Shut up! Take your hands
off me! Let go, I say!

208
00:17:09,903 --> 00:17:12,497
Ah, Kuwabara!

209
00:17:12,572 --> 00:17:16,235
That was a dirty trick, up and
dying like this, undefeated!

210
00:17:18,578 --> 00:17:20,512
Kuwabara-san, this is a wake here!

211
00:17:20,580 --> 00:17:25,847
Hey, Urameshi, listen up! I was gonna be
the one to kill you! You hear me!? Hey!

212
00:17:25,919 --> 00:17:27,784
There's no way he can hear you!

213
00:17:27,854 --> 00:17:32,791
Come back and fight me, you bastard!
Come back to life, damn you!

214
00:17:32,859 --> 00:17:35,589
I won't allow this! Hey!
Come back, Yusuke!

215
00:17:35,662 --> 00:17:40,463
Who am I supposed to
fight with from now on?

216
00:17:40,534 --> 00:17:41,466
Kuwabara-san...

217
00:17:41,535 --> 00:17:43,799
You dumbass!

218
00:17:43,870 --> 00:17:49,536
Dammit! Come and take my punch!

219
00:17:51,545 --> 00:17:53,877
Kuwabara-san, let's go.

220
00:17:58,218 --> 00:17:59,651
Sorry for the fuss.

221
00:18:03,223 --> 00:18:05,157
Kuwabara...

222
00:18:05,225 --> 00:18:09,821
--Who are those guys?
--Some of the folks Urameshi hung out with.

223
00:18:09,896 --> 00:18:13,832
--They are so disruptive!
--Indeed, they have no common decency!

224
00:18:13,900 --> 00:18:17,563
Well, he did something
good right at the end there.

225
00:18:17,637 --> 00:18:19,502
Thanks to him dying, the stock
of our school has now gone up.

226
00:18:19,573 --> 00:18:24,272
This one time, he might
have been kicking a child

227
00:18:24,344 --> 00:18:26,505
about when a car happened to
come along, Iwamoto-sensei.

228
00:18:26,580 --> 00:18:29,447
That's quite possible, Akashi-sensei.

229
00:18:29,516 --> 00:18:33,111
Bastards, they're saying whatever they like!

230
00:18:33,186 --> 00:18:34,380
Hey, you!

231
00:18:36,189 --> 00:18:38,453
Who is it!?

232
00:18:38,525 --> 00:18:40,993
Between those guys' behavior
and your words just now,

233
00:18:41,061 --> 00:18:44,121
which do you think is more indecent?

234
00:18:44,197 --> 00:18:46,757
Takenaka...

235
00:19:01,548 --> 00:19:07,487
I was so surprised, Yusuke, when I
heard that you had saved a little boy!

236
00:19:07,554 --> 00:19:09,351
I mean, YOU...

237
00:19:11,224 --> 00:19:17,493
However, for whatever reason, I can't
bring myself to praise you at all.

238
00:19:17,564 --> 00:19:24,493
Yusuke, now that you've died,
you won't amount to anything!

239
00:19:24,571 --> 00:19:26,664
Yusuke...

240
00:19:35,182 --> 00:19:37,810
Okay, this way.

241
00:19:47,527 --> 00:19:51,463
Okay, you should say something
to your big brother now.

242
00:19:51,531 --> 00:19:54,864
Big brother, thank you very much!

243
00:20:11,885 --> 00:20:13,147
Say, Mama,

244
00:20:13,220 --> 00:20:16,485
what was big brother doing inside that box?

245
00:20:16,556 --> 00:20:18,490
Was he sleeping?

246
00:20:18,558 --> 00:20:21,493
That's right.

247
00:20:21,561 --> 00:20:25,156
Mama, when big brother
wakes up, let's come again!

248
00:20:25,232 --> 00:20:28,099
I really want to say thank you
to big brother when he's awake!

249
00:20:28,168 --> 00:20:32,901
Big brother there was really funny!

250
00:20:37,177 --> 00:20:39,509
S- sure...

251
00:20:46,853 --> 00:20:49,447
How about it? Have you made up your mind?

252
00:20:49,523 --> 00:20:51,457
Listen, Botan...

253
00:20:51,525 --> 00:20:52,457
What?

254
00:20:52,526 --> 00:20:57,463
This trial thing to be returned to life...
what sort of trial is it, exactly?

255
00:20:57,531 --> 00:21:01,126
I don't know, either.

256
00:21:01,201 --> 00:21:03,795
You're undergoing it, then?

257
00:21:03,870 --> 00:21:05,804
Mm hm.

258
00:21:05,872 --> 00:21:07,567
All right, then, follow me.

259
00:21:14,214 --> 00:21:17,149
Hey, where are we going?

260
00:21:17,217 --> 00:21:18,479
To the Other World!

261
00:21:18,552 --> 00:21:19,814
The Other World?

262
00:21:19,886 --> 00:21:22,616
Right. We're going someplace
where there's a gentleman

263
00:21:22,689 --> 00:21:26,147
that can explain all about the
trial to return you to life!

264
00:21:26,226 --> 00:21:28,490
Now be quiet and come along!

265
00:21:28,562 --> 00:21:32,157
No sooner had Yusuke taken
the stage than he died!

266
00:21:32,232 --> 00:21:35,497
However, it has come about that he will
undergo a trial in order to return to life.

267
00:21:35,569 --> 00:21:39,096
What sort of trial could it possibly be?

268
00:21:46,129 --> 00:21:49,064
I don't know what this trial to
return to life is all about,

269
00:21:49,132 --> 00:21:52,397
but if I can overcome it, it
means I can come back to life!

270
00:21:52,468 --> 00:21:57,064
Hold on, if my body gets all burned up at the
funeral, nothing can come of it, can it!?

271
00:21:57,140 --> 00:22:01,736
Ma can't be relied on... The only thing
I can do is ask Keiko to look after it!

272
00:22:01,811 --> 00:22:05,076
But how am I supposed to tell her?

273
00:22:05,148 --> 00:22:09,414
Next time, "Koenma of the Spirit
Realm: A Trial Toward Resurrection"

274
00:22:09,485 --> 00:22:11,282
The Other World's not watching for nothing!

275
00:22:23,205 --> 00:22:34,810
The second hand is sharp
again tonight, as it chops up time,

276
00:22:34,883 --> 00:22:37,818
Chi chi chi...

277
00:22:37,886 --> 00:22:49,491
I'll never be able to finish
this homework, so I toss it away!

278
00:22:49,564 --> 00:22:53,830
Son of a gun!

279
00:22:53,902 --> 00:23:01,502
It's the dreams that tumble away, oh,

280
00:23:01,576 --> 00:23:08,175
That are the ones I want to go after,

281
00:23:08,250 --> 00:23:15,782
And if I stop, they will get away, oh,

282
00:23:15,857 --> 00:23:22,786
So I can't ease up on them!

283
00:23:22,864 --> 00:23:31,329
It's the dreams that tumble away, oh...!

