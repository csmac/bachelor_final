﻿1
00:00:01,000 --> 00:02:39,222
http://subscene.com/u/659433
Improved by: @Ivandrofly

2
00:02:39,224 --> 00:02:41,358
<font color="#808080">(FEMALE VOICE PRONOUNCING LETTERS)</font>
T... D...

3
00:02:43,194 --> 00:02:46,331
S... Z... Th...

4
00:02:46,332 --> 00:02:49,499
B... T... V...

5
00:02:56,174 --> 00:02:59,911
H... T... D... K... G...

6
00:02:59,912 --> 00:03:04,047
S... Z... P... B...

7
00:03:08,052 --> 00:03:11,222
Ba-Ba... T... T...

8
00:03:11,223 --> 00:03:14,058
K... Kuh... Ch...

9
00:03:14,059 --> 00:03:16,527
Th... V... Th...

10
00:03:16,528 --> 00:03:20,265
Zzz... Sss... Bzz... Ch...

11
00:03:20,266 --> 00:03:23,334
B-B-Buh... V-V-Vuh...

12
00:03:23,335 --> 00:03:25,736
G-G-Guh... D-D-Duh...

13
00:03:26,604 --> 00:03:29,340
B-B-Buh... B-B-Buh...

14
00:03:29,341 --> 00:03:33,211
B-B-Beh... B-B-Beh... Bah...

15
00:03:33,212 --> 00:03:35,480
N-N-Nuh... N-N-Nuh...

16
00:03:35,481 --> 00:03:37,414
N-N-Nuh... No.

17
00:03:39,183 --> 00:03:42,419
N-N-Nuh... N-N-Nuh...
N-N-Nuh... No.

18
00:03:44,122 --> 00:03:45,655
N-N-Nuh...

19
00:03:48,660 --> 00:03:52,029
F... Feel... Field...

20
00:03:52,997 --> 00:03:56,467
Fill... Filled... Filts...

21
00:03:56,468 --> 00:04:00,271
Foil... Failed... Fell...

22
00:04:00,272 --> 00:04:04,475
Felds... Pill... Pills...

23
00:04:04,476 --> 00:04:06,777
Pall... Nall...

24
00:04:11,716 --> 00:04:14,584
Foal... Foals...

25
00:04:15,687 --> 00:04:19,257
Fold... Fold...

26
00:04:19,258 --> 00:04:20,625
Pool...

27
00:04:20,626 --> 00:04:22,627
Pool...

28
00:04:22,628 --> 00:04:24,227
Sell... Se...

29
00:09:36,707 --> 00:09:40,677
<font color="#808080">(DOORS CLOSING)</font>

30
00:09:44,915 --> 00:09:47,550
<font color="#808080">(MOTORCYCLE ENGINE STARTS)</font>

31
00:09:56,494 --> 00:09:58,461
<font color="#808080">(VAN ENGINE STARTS)</font>

32
00:11:09,900 --> 00:11:12,635
<font color="#808080">(WOMEN CHATTERING)</font>

33
00:11:14,572 --> 00:11:17,540
<font color="#808080">(CHATTERING CONTINUES)</font>

34
00:12:34,118 --> 00:12:35,952
<font color="#808080">(CHATTERING, SHOUTING)</font>

35
00:12:51,469 --> 00:12:53,870
<font color="#808080">(CHATTERING, SHOUTING CONTINUES)</font>

36
00:13:45,523 --> 00:13:47,190
<font color="#808080">(HORN HONKS)</font>

37
00:13:53,731 --> 00:13:56,132
I'm looking for the M8.

38
00:13:57,601 --> 00:13:59,870
Eh...
<font color="#808080">(CHUCKLES)</font>

39
00:13:59,871 --> 00:14:03,673
Aye, you've gotta go straight
along that way and...

40
00:14:04,608 --> 00:14:06,677
This is gonna be pure
hard to explain.

41
00:14:06,678 --> 00:14:09,146
- Do you know where the Asda is?
- No.

42
00:14:09,147 --> 00:14:12,316
Yeah, I don't even know what
way you're pointin'. What?

43
00:14:12,317 --> 00:14:14,423
See, if you just drive
straight along up there...

44
00:14:14,424 --> 00:14:17,621
- Yes?
- And see the traffic lights up there?

45
00:14:17,622 --> 00:14:20,991
Take a right. Then you'll go
past Ibrox Stadium, right?

46
00:14:20,992 --> 00:14:22,860
And go straight
through a roundabout.

47
00:14:22,861 --> 00:14:27,598
Then take a left. So
it's right, then just...

48
00:14:27,599 --> 00:14:30,801
And it's there. The motorway is there.
It's back of Asda. You'll see it.

49
00:14:30,802 --> 00:14:32,937
- Back of Asda, see?
- Okay.

50
00:14:32,938 --> 00:14:35,238
- Am I keeping you from something?
- <font color="#808080">(CHUCKLES)</font> No.

51
00:14:36,307 --> 00:14:38,242
- Where are you going?
- Along here.

52
00:14:38,243 --> 00:14:41,679
- For what? For work?
- No, I need to go meet somebody.

53
00:14:41,680 --> 00:14:43,280
- Oh, you're going to meet someone.
- Aye.

54
00:14:43,281 --> 00:14:44,681
- All right. Thank you.
- Sure.

55
00:15:06,103 --> 00:15:10,273
<font color="#808080">(SIREN WAILING)</font>

56
00:15:15,779 --> 00:15:17,915
<font color="#804040">WOMAN</font>: I'm looking for a post office.

57
00:15:17,916 --> 00:15:19,950
- <font color="#804040">MAN</font>: Post office?
- Yes.

58
00:15:19,951 --> 00:15:21,318
Where are you from?

59
00:15:21,319 --> 00:15:22,753
- Where I am from?
- Yeah.

60
00:15:22,754 --> 00:15:25,289
- Albania.
- Oh, are you?

61
00:15:25,290 --> 00:15:26,857
- You have family here?
- Yes.

62
00:15:26,858 --> 00:15:28,325
Oh, you do.

63
00:15:28,326 --> 00:15:31,328
That's the reason I'm saying
you have to go another way...

64
00:15:31,329 --> 00:15:33,631
for another...

65
00:15:33,632 --> 00:15:35,732
It's after the shopping ce...

66
00:15:58,389 --> 00:16:00,824
- Hi.
- Lost?

67
00:16:00,825 --> 00:16:02,826
- Yeah, lost.
- What are you looking for?

68
00:16:02,827 --> 00:16:05,229
- I'm looking for the M8.
- Up to the roundabout...

69
00:16:05,230 --> 00:16:07,197
- Are you walking?
- Yep.

70
00:16:07,198 --> 00:16:09,266
- Where are you walking to?
- Home.

71
00:16:09,267 --> 00:16:12,269
- Oh, you're going home. To your family?
- No, no, just myself.

72
00:16:12,270 --> 00:16:14,405
- Just yourself?
- Aye. It's great.

73
00:16:14,406 --> 00:16:18,008
- Yeah? How?
- I can do whatever I like.

74
00:16:18,009 --> 00:16:20,444
- So where are you coming from?
- Govan. Right there.

75
00:16:20,445 --> 00:16:22,179
- Sorry?
- Govan.

76
00:16:22,180 --> 00:16:25,315
- Govan? Do you work there?
- No, no.

77
00:16:25,316 --> 00:16:27,818
Don't work there.
I work for myself.

78
00:16:27,819 --> 00:16:31,287
- Do you want a lift?
- Eh... Aye, why not?

79
00:16:38,195 --> 00:16:40,831
You'll have to tell
me how to get there.

80
00:16:40,832 --> 00:16:43,366
- Just follow this road straight up.
- Okay.

81
00:16:44,301 --> 00:16:46,103
Sure.
<font color="#808080">(ENGINE STARTS)</font>

82
00:16:46,104 --> 00:16:48,372
- So what do you do?
- Electrician.

83
00:16:48,373 --> 00:16:49,806
Oh, you're an electrician.

84
00:16:51,375 --> 00:16:53,811
<font color="#808080">(GROANS)</font>
It's cold, right?

85
00:16:53,812 --> 00:16:56,412
- Ah, it's a wee bit nippy, aye.
- Yeah.

86
00:16:57,981 --> 00:16:59,783
So what is it youse all doing?

87
00:16:59,784 --> 00:17:01,885
I'm... I'm just, uh, driving...

88
00:17:01,886 --> 00:17:06,290
just, uh... some furniture
for my family and...

89
00:17:06,291 --> 00:17:08,892
- Oh, right?
- Yeah.

90
00:17:08,893 --> 00:17:10,394
So you live alone?

91
00:17:10,395 --> 00:17:12,162
- Yes.
- And you love it?

92
00:17:12,163 --> 00:17:15,399
- Aye. It's great.
- What do you love about living alone?

93
00:17:15,400 --> 00:17:18,302
- Nobody to nag you.
- Do I go straight through?

94
00:17:18,303 --> 00:17:21,337
Yeah, just straight through.
Straight through the whole way.

95
00:17:43,494 --> 00:17:45,895
<font color="#808080">(CHATTERING)</font>

96
00:17:54,772 --> 00:17:58,041
It should be just cart around that
street and stay in the right hand lane.

97
00:17:58,042 --> 00:18:00,310
- What's that?
- That's a tattoo of my name.

98
00:18:00,311 --> 00:18:01,979
- Of your name?
- Yeah.

99
00:18:01,980 --> 00:18:03,213
- What's your name?
- Andy.

100
00:18:03,214 --> 00:18:04,848
- Andy. Hi.
- Pleased to meet you.

101
00:18:04,849 --> 00:18:07,484
- Nice to meet you too.
- Thank you very much.

102
00:18:07,485 --> 00:18:09,820
<font color="#808080">(WOMAN SHOUTS)</font>
Andy!

103
00:18:09,821 --> 00:18:11,021
- Want me?
- <font color="#804040">MAN</font>: You coming?

104
00:18:11,022 --> 00:18:13,489
<font color="#808080">(ANDY CHATTERING, INDISTINCT, FADES)</font>

105
00:18:16,360 --> 00:18:19,463
The spot back here, you get... you put
your hand out. Anybody can stop.

106
00:18:19,464 --> 00:18:22,266
There's lonely old guys.
Know what I mean?

107
00:18:22,267 --> 00:18:25,869
It was... Hey, might as well check
her out, driving away in a big van.

108
00:18:25,870 --> 00:18:29,005
- Sure you can drive this, huh? Huh?
- <font color="#808080">(LAUGHS)</font>

109
00:18:32,442 --> 00:18:35,512
- No girlfriend? Really?
- Nah, I don't have a girlfriend at all.

110
00:18:35,513 --> 00:18:38,047
You're very charming.

111
00:18:42,119 --> 00:18:43,586
That's better.

112
00:18:45,055 --> 00:18:47,189
Yeah? Sorted?

113
00:18:48,225 --> 00:18:50,327
- You have a handsome face.
- Aye?

114
00:18:50,328 --> 00:18:53,130
- Yeah.
- Thanks a lot. Cheers.

115
00:18:53,131 --> 00:18:55,566
- Do you think I'm pretty?
- I think you're gorgeous.

116
00:18:55,567 --> 00:18:57,633
- Do you?
- Aye, definitely.

117
00:18:58,135 --> 00:19:00,002
Good.

118
00:19:03,040 --> 00:19:04,575
And a friendly smile as well.

119
00:19:04,576 --> 00:19:06,877
- Do I?
- Aye, right deep.

120
00:19:06,878 --> 00:19:09,012
- What about your smile?
- Aye.

121
00:19:09,013 --> 00:19:10,514
You've a nice smile yourself.

122
00:19:10,515 --> 00:19:12,916
- Cheers.
- Good.

123
00:19:12,917 --> 00:19:14,450
Good.

124
00:21:36,360 --> 00:21:40,629
<font color="#808080">(CHATTERING, WHOOPING)</font>
<font color="#808080">(SPEAKERS, REGGAE)</font>

125
00:21:42,399 --> 00:21:45,101
♪ <font color="#808080">(FADES)</font> ♪

126
00:21:54,411 --> 00:21:56,345
No, no, no, lads! No!

127
00:22:08,091 --> 00:22:12,261
<font color="#808080">(CHATTERING CONTINUES)</font>

128
00:22:52,402 --> 00:22:55,171
BABY: Mommy! Whoo!

129
00:23:54,297 --> 00:23:56,166
I thought...

130
00:23:56,167 --> 00:23:57,801
you had your eye on my towel.

131
00:23:57,802 --> 00:24:00,203
No. I wanted to speak to you.

132
00:24:00,204 --> 00:24:02,804
Do you know if there's any good
spots to surf around here?

133
00:24:03,440 --> 00:24:07,310
I'm not a surfer. Just living
in a little tent over there.

134
00:24:07,311 --> 00:24:10,213
- Just hanging around.
- Yeah?

135
00:24:10,214 --> 00:24:13,349
- Reading, swimming.
- You're not from here? Where are you from?

136
00:24:14,885 --> 00:24:17,987
- I'm from Czech Republic.
- Why are you in Scotland?

137
00:24:20,357 --> 00:24:22,692
I just... wanted to
get away from it all.

138
00:24:22,693 --> 00:24:24,928
Yeah? Why here?

139
00:24:24,929 --> 00:24:26,930
Because it's...
<font color="#808080">(LAUGHS)</font>

140
00:24:26,931 --> 00:24:28,397
It's nowhere.
<font color="#808080">(SHOUTING, LAUGHING)</font>

141
00:24:38,175 --> 00:24:40,776
<font color="#808080">(SHOUTING)</font>

142
00:25:04,701 --> 00:25:06,802
<font color="#808080">(SHOUTING)</font>

143
00:25:20,951 --> 00:25:22,685
<font color="#808080">(SCREAMS)</font>

144
00:26:19,676 --> 00:26:22,478
<font color="#808080">(SHOUTING)</font>

145
00:27:40,623 --> 00:27:43,592
<font color="#808080">(BABY SCREAMING, CRYING)</font>

146
00:27:45,061 --> 00:27:48,230
<font color="#808080">(CRYING CONTINUES)</font>

147
00:28:35,745 --> 00:28:39,214
<font color="#808080">(MOTORCYCLE ENGINE IDLING)</font>

148
00:28:44,120 --> 00:28:46,588
<font color="#808080">(ENGINE CONTINUES IDLING)</font>

149
00:29:00,003 --> 00:29:02,638
<font color="#808080">(BABY CONTINUES SCREAMING, CRYING)</font>

150
00:29:35,939 --> 00:29:38,974
<font color="#808080">(BABY SCREAMING, COUGHING, FAINT)</font>

151
00:29:48,485 --> 00:29:50,052
<font color="#808080">(SCREAMING, COUGHING CONTINUE)</font>

152
00:29:54,824 --> 00:29:57,060
<font color="#808080">(CAR ENGINE RACING)</font>
<font color="#808080">(SHOUTING, INDISTINCT)</font>

153
00:29:57,061 --> 00:30:01,197
<font color="#808080">(SPEAKING, MUFFLED)</font>

154
00:30:01,198 --> 00:30:03,865
<font color="#808080">(OTHERS SHOUTING, MUFFLED)</font>

155
00:31:14,804 --> 00:31:17,372
<font color="#808080">(AIRPLANE PASSING)</font>

156
00:31:21,911 --> 00:31:25,747
<font color="#808080">(WOMEN CHATTERING, LAUGHING)</font>

157
00:31:32,322 --> 00:31:35,691
<font color="#808080">(CHATTERING CONTINUES)</font>

158
00:31:56,212 --> 00:31:59,781
♪ <font color="#808080">(SPEAKERS, TECHNO)</font> ♪

159
00:32:33,383 --> 00:32:36,351
♪ <font color="#808080">(CONTINUES, MUFFLED)</font> ♪

160
00:32:59,942 --> 00:33:02,979
Whoa! Whoa, whoa. Excuse me.

161
00:33:02,980 --> 00:33:05,314
Can I talk to you before
you run away, darling?

162
00:33:05,315 --> 00:33:07,884
I wanted to speak to you earlier,
right, but you disappeared.

163
00:33:07,885 --> 00:33:10,787
I thought you went to
the ladies' or somethin'.

164
00:33:10,788 --> 00:33:14,257
Anyway, right, fuck
all that shite. Right?

165
00:33:14,258 --> 00:33:17,060
I really want to buy you a drink,
man. You look absolutely stunning.

166
00:33:17,061 --> 00:33:18,861
And I...

167
00:33:18,862 --> 00:33:22,065
You know what? I'm all
alone. You're all alone.

168
00:33:22,066 --> 00:33:24,267
What do you say? What's one drink?

169
00:33:24,268 --> 00:33:26,035
What's one drink?

170
00:33:26,036 --> 00:33:28,805
- I saw you on the road.
- What, you saw <i>me</i> on the road?

171
00:33:28,806 --> 00:33:31,441
- Earlier, yeah.
- Are you having a laugh with me?

172
00:33:31,442 --> 00:33:33,943
You saw me? You wanna come
get a drink and a dance?

173
00:33:33,944 --> 00:33:36,746
- Are you alone here?
- Yeah, I'm all alone. Right?

174
00:33:36,747 --> 00:33:39,981
♪ <font color="#808080">(CONTINUES, LOUD)</font> ♪

175
00:38:56,098 --> 00:38:58,266
<font color="#808080">(TAP ON WINDOW)</font>

176
00:39:03,506 --> 00:39:07,575
The man from the car,
he buy the rose for you.

177
00:40:03,299 --> 00:40:07,236
<font color="#804040">WOMAN ON RADIO</font>: A man's body has been found
washed up on a beach... near Arbroath.

178
00:40:07,237 --> 00:40:09,805
His wife and child are
reported missing.

179
00:40:09,806 --> 00:40:12,241
The police say the body
has been identified...

180
00:40:12,242 --> 00:40:15,277
as 36-year-old Kenneth
McClelland from Edinburgh.

181
00:40:15,278 --> 00:40:17,480
The body was found by a tourist...

182
00:40:17,481 --> 00:40:20,583
and is believed to have been
in the water for some time.

183
00:40:20,584 --> 00:40:25,387
Mr. McClelland was married and worked as a
chemistry lecturer at Edinburgh University.

184
00:40:25,388 --> 00:40:29,158
The alarm was raised when he failed
to turn up for work this morning.

185
00:40:29,159 --> 00:40:32,661
His car was found at the
nearby Deer Park Golf Club.

186
00:40:32,662 --> 00:40:36,232
His wife, 32-year-old Alison
McClelland, and 18-month-old son...

187
00:40:36,233 --> 00:40:38,434
are believed to have been with him.

188
00:40:38,435 --> 00:40:41,570
A police and coast guard search
operation involving a helicopter...

189
00:40:41,571 --> 00:40:43,739
has been halted due to fog.

190
00:40:43,740 --> 00:40:45,574
The next bulletin is on the hour.

191
00:40:45,575 --> 00:40:48,544
♪ <font color="#808080">(NEWS THEME)</font> ♪

192
00:40:48,545 --> 00:40:50,346
<font color="#804040">MAN</font>: Well, plenty to talk
about, as ever, with Kay.

193
00:40:50,347 --> 00:40:53,182
You can call now...
10-500-92-95-00.

194
00:40:53,183 --> 00:40:55,184
You can text as well... 80295.

195
00:40:55,185 --> 00:40:57,353
- It's 8:17.
- <font color="#804040">WOMAN 2</font>: Good morning, Gary.

196
00:40:57,354 --> 00:41:00,222
Well, as we've heard, for a
whole host of reasons...

197
00:41:00,223 --> 00:41:02,558
2014 is a very important
year for Scotland.

198
00:41:02,559 --> 00:41:04,860
Of course, the referendum. We're
expecting that date today.

199
00:41:04,861 --> 00:41:07,930
But the Commonwealth
Games and Ryder Cup...

200
00:41:07,931 --> 00:41:10,299
- You seem very excited about that.
- I am.

201
00:41:10,300 --> 00:41:13,701
He's jumping out of his seat at the
prospect of the year he's gonna have...

202
00:41:49,939 --> 00:41:53,508
<font color="#808080">(MAN ON RADIO, INDISTINCT)</font>
<i>Fuckin' hell, you're gorgeous.</i>

203
00:41:55,611 --> 00:41:57,845
- You think so?
- It's your eyes.

204
00:41:58,948 --> 00:42:01,449
Somethin' about your eyes.

205
00:42:02,384 --> 00:42:04,887
Your eyes, your lips.

206
00:42:04,888 --> 00:42:07,055
- That black hair.
- Yeah?

207
00:42:10,259 --> 00:42:11,993
You just look...

208
00:42:13,262 --> 00:42:14,830
amazing.

209
00:42:14,831 --> 00:42:17,398
<font color="#808080">(VOICES ARGUING ON RADIO)</font>

210
00:45:25,487 --> 00:45:27,122
<font color="#804040">MAN</font>: Hey, you okay?

211
00:45:27,123 --> 00:45:30,459
<font color="#808080">(MAN SPEAKS, MUFFLED)</font>

212
00:45:30,460 --> 00:45:33,227
<font color="#808080">(MEN SPEAKING, MUFFLED)</font>

213
00:45:35,497 --> 00:45:38,566
<font color="#804040">MAN</font>: Want a hand up? Want a hand?

214
00:45:44,973 --> 00:45:48,175
<font color="#808080">(INDISTINCT)</font>

215
00:46:32,754 --> 00:46:35,590
<font color="#808080">(CHATTERING)</font>

216
00:46:54,910 --> 00:46:59,080
<font color="#808080">(CHATTERING)</font>

217
00:49:01,370 --> 00:49:04,839
<font color="#808080">(NO AUDIBLE DIALOGUE)</font>
<font color="#808080">(TRAIN PASSING)</font>

218
00:49:19,221 --> 00:49:22,323
<font color="#808080">(MEN SHOUTING)</font>

219
00:49:23,225 --> 00:49:25,894
Get out, eh? Get out!

220
00:49:25,895 --> 00:49:28,229
<font color="#808080">(SHOUTING CONTINUES)</font>

221
00:50:36,098 --> 00:50:37,832
<font color="#808080">(ENGINE IDLING)</font>

222
00:51:07,295 --> 00:51:08,863
Excuse me.

223
00:51:10,132 --> 00:51:11,767
Excuse me.

224
00:51:11,768 --> 00:51:14,770
Uh, I'm a bit lost, really.

225
00:51:14,771 --> 00:51:16,937
Am I headed towards Taynuilt?

226
00:51:17,939 --> 00:51:19,040
Yeah.

227
00:51:19,041 --> 00:51:21,977
I am? Is it far?

228
00:51:21,978 --> 00:51:24,146
It's about 10 minutes away.

229
00:51:24,147 --> 00:51:26,413
Ten minutes? Are you...
Are you headed there?

230
00:51:27,315 --> 00:51:31,051
- I'm going to the supermarket.
- Is it on the way?

231
00:51:34,256 --> 00:51:36,057
Yeah.

232
00:51:36,058 --> 00:51:38,225
I could drop you off if you'd like.

233
00:51:43,999 --> 00:51:46,000
Yeah?

234
00:52:14,930 --> 00:52:17,464
Here. Let me turn this up for you.

235
00:52:26,074 --> 00:52:28,042
That's better.

236
00:52:43,158 --> 00:52:44,992
You're very quiet.

237
00:52:50,932 --> 00:52:53,267
So why do you shop at night then?

238
00:52:57,372 --> 00:52:59,206
People wind me up.

239
00:53:01,443 --> 00:53:02,609
How?

240
00:53:04,913 --> 00:53:06,714
They're ignorant.

241
00:53:10,318 --> 00:53:12,686
What about your friends?

242
00:53:15,957 --> 00:53:18,092
So you don't have any friends?

243
00:53:23,265 --> 00:53:24,465
No.

244
00:53:25,467 --> 00:53:27,201
How about a girlfriend?

245
00:53:33,408 --> 00:53:35,142
How old are you?

246
00:53:36,611 --> 00:53:38,278
I'm 26.

247
00:53:40,148 --> 00:53:43,050
When was the last time
you had a girlfriend?

248
00:53:45,420 --> 00:53:47,589
Never had one.

249
00:53:47,590 --> 00:53:49,423
So don't you get lonely then?

250
00:53:56,698 --> 00:53:58,532
You've very nice hands.

251
00:54:03,672 --> 00:54:05,572
You've beautiful hands.

252
00:54:27,529 --> 00:54:29,998
Do you want to look at me?

253
00:54:29,999 --> 00:54:32,366
- This isn't Tesco's, is it?
- No.

254
00:54:38,540 --> 00:54:41,108
I noticed you looking at me before.

255
00:54:46,614 --> 00:54:48,148
And?

256
00:54:49,617 --> 00:54:51,618
I liked it.

257
00:54:56,224 --> 00:54:58,460
You're uncomfortable.

258
00:54:58,461 --> 00:55:01,261
No. Just want to go to Tesco's.

259
00:55:07,402 --> 00:55:10,137
So you never think about it then?

260
00:55:12,607 --> 00:55:14,341
Think about what?

261
00:55:15,543 --> 00:55:17,778
Being with a girl.

262
00:55:24,052 --> 00:55:26,619
When was the last time
you touched someone?

263
00:55:38,700 --> 00:55:40,601
Let me see your hands.

264
00:55:47,509 --> 00:55:49,176
Cold.

265
00:55:52,380 --> 00:55:54,148
I have to get back soon.

266
00:56:05,293 --> 00:56:07,261
How was that?

267
00:56:08,163 --> 00:56:09,463
Cold.

268
00:56:12,667 --> 00:56:15,236
Do you want to do it again?

269
00:56:15,237 --> 00:56:17,337
Yeah.

270
00:56:25,146 --> 00:56:27,382
Do you want to touch my neck?

271
00:56:27,383 --> 00:56:29,216
Here.

272
00:56:37,459 --> 00:56:39,760
How was that?

273
00:56:41,463 --> 00:56:43,297
Yeah.

274
00:56:44,699 --> 00:56:46,800
Your hands are very soft.

275
00:56:54,509 --> 00:56:57,344
I have a place about
30 minutes away.

276
00:56:58,480 --> 00:57:00,881
Will you come with me there?

277
00:57:14,662 --> 00:57:17,831
<font color="#808080">(ENGINE ACCELERATES)</font>

278
00:57:54,569 --> 00:57:57,871
<font color="#808080">(CLOTHES RUSTLING)</font>

279
00:57:59,807 --> 00:58:01,576
<font color="#804040">MAN</font>: Cold.

280
00:58:01,577 --> 00:58:03,877
<font color="#804040">WOMAN</font>: I won't let that stop us.

281
00:58:23,665 --> 00:58:25,332
Come to me.

282
00:58:40,615 --> 00:58:42,450
Dreaming?

283
00:58:42,451 --> 00:58:43,851
Yes.

284
00:58:43,852 --> 00:58:45,587
Dreaming?

285
00:58:45,588 --> 00:58:47,421
Yes, we are.

286
00:58:54,762 --> 00:58:56,630
<font color="#808080">(RAGGED BREATHING)</font>

287
00:59:55,957 --> 00:59:59,126
<font color="#808080">(FOOTSTEPS)</font>

288
01:01:17,872 --> 01:01:22,042
<font color="#808080">(INSECT BUZZING)</font>

289
01:01:33,020 --> 01:01:37,557
<font color="#808080">(BUZZING CONTINUES)</font>

290
01:01:44,632 --> 01:01:46,800
<font color="#808080">(LOCK CLICKING)</font>

291
01:02:02,917 --> 01:02:04,584
<font color="#808080">(CAR DOOR OPENS)</font>

292
01:02:49,030 --> 01:02:54,034
<font color="#808080">(MOTORCYCLE, DISTANT)</font>

293
01:04:06,541 --> 01:04:10,043
<font color="#808080">(CAR DOOR SHUTS)</font>
<font color="#808080">(ENGINE STARTS)</font>

294
01:05:07,134 --> 01:05:08,968
<font color="#808080">(ENGINE STOPS)</font>

295
01:06:22,376 --> 01:06:26,012
♪ <font color="#808080">(MAN SINGING, DISTANT)</font> ♪

296
01:06:27,415 --> 01:06:32,018
♪ <font color="#808080">(CONTINUES)</font> ♪

297
01:06:33,988 --> 01:06:37,257
♪ <font color="#808080">(CONTINUES, CLOSER)</font> ♪

298
01:06:40,094 --> 01:06:44,964
<font color="#808080">(FOOTSTEPS)</font>

299
01:06:46,267 --> 01:06:48,868
<font color="#808080">(BIRD SQUAWKS)</font>
<font color="#808080">(WINGS FLUTTERING)</font>

300
01:07:37,151 --> 01:07:41,020
<font color="#808080">(CHATTERING)</font>

301
01:07:47,495 --> 01:07:50,129
<font color="#808080">(CHATTERING CONTINUES)</font>

302
01:09:03,370 --> 01:09:05,471
<font color="#808080">(CHATTERING STOPS)</font>

303
01:09:09,243 --> 01:09:12,712
<font color="#808080">(CHATTERING RESUMES)</font>

304
01:09:46,647 --> 01:09:49,615
The bus will be along in a minute.

305
01:10:45,472 --> 01:10:47,854
Do you not think you should be
wearing a jacket, little lass?

306
01:10:47,855 --> 01:10:50,010
The weather's terrible up here.

307
01:10:50,312 --> 01:10:54,280
You'll catch your death of cold if you've
not got a jacket or a hat or something on.

308
01:10:57,518 --> 01:11:00,186
You've not got the right
clothes on at all.

309
01:11:07,594 --> 01:11:09,495
You all right there?

310
01:11:11,665 --> 01:11:13,399
Eh?

311
01:11:14,368 --> 01:11:16,202
You okay?

312
01:11:21,408 --> 01:11:22,977
Can I do something for you?

313
01:11:22,978 --> 01:11:25,679
Chase yourself, dafty.

314
01:11:25,680 --> 01:11:28,781
Leave the lassie alone. She doesn't
want anything to do with you.

315
01:11:37,391 --> 01:11:39,492
Do you need any help?

316
01:11:49,470 --> 01:11:50,837
Yes.

317
01:11:58,312 --> 01:12:00,179
Right.

318
01:13:10,651 --> 01:13:13,219
<font color="#808080">(MAN SHOUTS, INDISTINCT)</font>

319
01:13:25,632 --> 01:13:28,401
<font color="#808080">(CHATTERING)</font>

320
01:13:53,360 --> 01:13:55,328
Let me just take your coat.

321
01:14:18,652 --> 01:14:20,286
<font color="#808080">(AUDIENCE LAUGHING ON TV)</font>

322
01:14:20,721 --> 01:14:22,189
<i><font color="#808080">(TAPS JAR)</font></i>

323
01:14:22,190 --> 01:14:23,956
<i>Spoon, Jar. Jar, spoon.</i>

324
01:14:26,426 --> 01:14:28,829
<i>Spoon, Jar. Jar... Wonder whether
that's working, don't you?</i>

325
01:14:28,830 --> 01:14:30,397
<font color="#808080">(LAUGHS)</font>
Spoon, Jar. Jar, spoon.

326
01:14:30,398 --> 01:14:32,266
Spoon, Jar. Jar, spoon.

327
01:14:32,267 --> 01:14:34,300
Spoon, Jar. Spoon, Jar. Jar, spoon.

328
01:14:34,902 --> 01:14:36,670
<font color="#808080">(SPOON RATTLES IN JAR)</font>

329
01:14:36,671 --> 01:14:40,407
♪ <font color="#808080">(VOCALIZING, MARCH)</font> ♪
♪ <font color="#808080">(SPOON RATTLES IN JAR)</font> ♪

330
01:14:40,408 --> 01:14:42,008
Stop!

331
01:14:43,610 --> 01:14:45,479
♪ <font color="#808080">(VOCALIZING, MARCH)</font> ♪
♪ <font color="#808080">(SPOON RATTLES IN JAR)</font> ♪

332
01:14:45,480 --> 01:14:47,748
Stop!

333
01:14:47,749 --> 01:14:48,749
<i>♪ <font color="#808080">(VOCALIZING, MARCH)</font> ♪</i>

334
01:14:48,750 --> 01:14:50,350
<i>Pull it. Pull it.</i>

335
01:14:50,351 --> 01:14:53,387
<i><font color="#808080">(LAUGHTER)</font>
<font color="#808080">(APPLAUSE)</font></i>

336
01:14:53,388 --> 01:14:55,289
<font color="#808080">(RADIO STATIC, FREQUENCIES CHANGING)</font>

337
01:14:55,290 --> 01:14:58,057
<i>♪ Now I've stood on your shadow ♪</i>

338
01:14:59,726 --> 01:15:01,894
<i>♪ And I've watched it grow ♪</i>

339
01:15:03,430 --> 01:15:06,065
<i>♪ And it's shaken and ♪
♪ it's driven me ♪</i>

340
01:15:07,267 --> 01:15:09,436
<i>♪ And let me know ♪
♪ Let me know ♪</i>

341
01:15:09,437 --> 01:15:13,339
<i>♪ Let me know, let me know about ♪
♪ all the old forty-fives ♪</i>

342
01:15:14,441 --> 01:15:16,742
<i>♪ And the paperback rooms ♪</i>

343
01:15:17,978 --> 01:15:20,846
<i>♪ And it's scattered ♪
♪ all the photographs ♪</i>

344
01:15:21,915 --> 01:15:23,883
<i>♪ Of summers and suns ♪</i>

345
01:15:25,252 --> 01:15:27,753
<i>♪ And you're a real gone kid ♪</i>

346
01:15:29,556 --> 01:15:33,260
<i>♪ And maybe now, baby ♪
♪ Maybe now, baby ♪</i>

347
01:15:33,261 --> 01:15:36,597
<i>♪ Maybe now, baby ♪
♪ Maybe now, baby ♪</i>

348
01:15:36,598 --> 01:15:37,965
<i>♪ And maybe now, baby ♪</i>

349
01:15:37,966 --> 01:15:40,434
<i>♪ Maybe now, baby ♪</i>

350
01:15:40,435 --> 01:15:42,368
<i>♪ I'll do what I should have di ♪</i>

351
01:15:43,537 --> 01:15:45,839
<i>♪ 'Cause you're a real ♪</i>

352
01:15:45,840 --> 01:15:47,073
<i>♪ Gone... ♪</i>

353
01:16:17,004 --> 01:16:18,904
Night.

354
01:18:14,588 --> 01:18:17,556
<font color="#808080">(ENGINES IDLING)</font>

355
01:18:28,835 --> 01:18:31,070
<font color="#808080">(ENGINE STARTS)</font>

356
01:19:03,570 --> 01:19:07,573
<font color="#808080">(HORSES TROTTING)</font>

357
01:20:35,161 --> 01:20:36,929
You all right?

358
01:20:42,302 --> 01:20:44,937
If you're frightened, let's go.

359
01:20:51,111 --> 01:20:52,811
Come on.

360
01:20:53,980 --> 01:20:56,081
It's all right.

361
01:20:57,217 --> 01:20:59,051
That's it.

362
01:21:23,209 --> 01:21:26,745
<i><font color="#808080">(WIND WHISTLING)</font></i>

363
01:21:36,790 --> 01:21:38,724
It's okay. It's okay.

364
01:21:50,971 --> 01:21:53,005
It's okay.

365
01:21:54,174 --> 01:21:56,108
It's okay.

366
01:22:01,214 --> 01:22:03,215
All right.

367
01:22:06,386 --> 01:22:08,288
Okay.

368
01:22:08,289 --> 01:22:10,223
You did it.

369
01:22:10,224 --> 01:22:12,057
You did it.

370
01:22:12,959 --> 01:22:15,728
<font color="#808080">(MOTORCYCLE REVVING)</font>

371
01:26:25,078 --> 01:26:26,378
You all right?

372
01:28:18,324 --> 01:28:22,194
<font color="#808080">(BRANCH SNAPS)</font>

373
01:28:33,272 --> 01:28:36,442
You just off for a ramble
in the woods then? Eh?

374
01:28:36,443 --> 01:28:38,823
- Huh?
- You just off for a ramble in the woods?

375
01:28:38,824 --> 01:28:42,715
Yeah, yeah? Oh, it's, uh... Watch how
you step here. It's a bit, uh...

376
01:28:42,716 --> 01:28:46,152
It's a bit slippery round here,
but... this time of year.

377
01:28:46,153 --> 01:28:50,323
It's all right in summer. Well, it's still
wet in summer, but not as bad as it is now.

378
01:28:50,324 --> 01:28:52,625
But there's plenty of
trails around here.

379
01:28:52,626 --> 01:28:54,560
Have you been here before?

380
01:28:54,561 --> 01:28:56,295
No? First time? Yeah?

381
01:28:56,296 --> 01:28:58,664
Oh, you won't get lost.
They're all clearly marked.

382
01:28:58,665 --> 01:29:02,135
But, uh, there's about 2,000
acres of forest here,

383
01:29:02,136 --> 01:29:06,072
so you shouldn't... you should have,
uh, plenty of places to go in there.

384
01:29:06,073 --> 01:29:07,807
- You on your own?
- Yeah.

385
01:29:07,808 --> 01:29:12,578
Yeah, on your own? Oh, well, it's a
nice place if you want some solitude,

386
01:29:12,579 --> 01:29:16,416
you know, to "gather your
thoughts" and all that, yeah?

387
01:29:16,417 --> 01:29:19,217
- So, uh, enjoy yourself anyway, okay?
- Thank you.

388
01:31:13,599 --> 01:31:17,569
<font color="#808080">(RAINDROPS PATTERING)</font>

389
01:35:19,412 --> 01:35:21,045
<font color="#808080">(BRANCH SNAPS)</font>

390
01:36:06,559 --> 01:36:11,563
<font color="#808080">(HORN HONKING)</font>

391
01:36:29,615 --> 01:36:30,582
<font color="#808080">(GRUNTS)</font>

392
01:36:32,551 --> 01:36:34,185
<font color="#808080">(GASPS)</font>

393
01:36:38,991 --> 01:36:40,091
<font color="#808080">(GASPS)</font>

394
01:37:06,018 --> 01:37:07,852
<font color="#808080">(UNZIPPING JACKET)</font>

395
01:37:11,223 --> 01:37:12,890
<font color="#808080">(WHIMPERING)</font>

396
01:37:31,043 --> 01:37:34,312
<font color="#808080">(RIPPING)</font>

397
01:37:34,412 --> 01:37:39,412
http://subscene.com/u/659433
Improved by: @Ivandrofly

