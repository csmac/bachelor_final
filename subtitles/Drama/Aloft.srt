1
00:09:55,440 --> 00:09:57,727
Well, I should get there soon.

2
00:09:57,840 --> 00:09:59,524
Luckily I had the GPS.

3
00:09:59,680 --> 00:10:00,681
How is it?

4
00:10:01,000 --> 00:10:04,641
Crazy. There's snow everywhere.
It's like driving on the clouds.

5
00:10:04,920 --> 00:10:08,083
I am glad. You sound well.

6
00:10:08,200 --> 00:10:13,206
Please help me find
the coordinates for the next act.

7
00:10:13,520 --> 00:10:17,047
If not... Her son
is my last chance to find her.

8
00:10:17,320 --> 00:10:18,380
I know, I am trying.

9
00:10:18,480 --> 00:10:21,820
I wish you had changed your mind...

10
00:10:21,920 --> 00:10:23,684
I can't help it.

11
00:10:24,440 --> 00:10:27,808
Anyway, everything's fine.

12
00:10:28,400 --> 00:10:30,562
Don't worry, I feel good.

13
00:10:34,200 --> 00:10:36,043
I know, baby.

14
00:10:36,600 --> 00:10:37,726
Give me that.

15
01:07:16,140 --> 01:07:17,824
It won't be necessary.

16
01:07:19,780 --> 01:07:21,305
Single.

17
01:07:21,700 --> 01:07:25,671
Diagnosed in 2011.

18
01:07:26,620 --> 01:07:31,023
You stopped treatment 5 months ago.

19
01:07:32,020 --> 01:07:33,465
No kids.

20
01:08:12,540 --> 01:08:13,666
I am sorry.

21
01:08:18,020 --> 01:08:20,546
I don't know what to say.

22
01:08:21,220 --> 01:08:24,429
I don't know why he reacted like that.

23
01:08:27,340 --> 01:08:29,991
I hope... I don't know.

24
01:08:30,540 --> 01:08:32,747
I don't know what to say.

25
01:08:33,820 --> 01:08:36,221
I don't know why...

26
01:08:36,460 --> 01:08:37,746
I am so sorry.

27
01:08:39,380 --> 01:08:42,031
I hope this doesn't change anything.

28
01:08:42,540 --> 01:08:43,541
Please.

29
01:08:45,020 --> 01:08:46,385
You want Nana?

30
01:08:51,460 --> 01:08:53,967
Don't worry my little girl.

31
01:08:56,340 --> 01:08:59,310
Don't worry. Be strong.

32
01:09:03,220 --> 01:09:05,461
I have some documents for you.

33
01:09:06,420 --> 01:09:09,230
- Sign the papers.
- Thank you.

34
01:09:09,580 --> 01:09:11,867
Confidentiality agreement.

35
01:09:15,860 --> 01:09:17,066
Thank you.
