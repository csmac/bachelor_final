1
00:00:08,250 --> 00:00:09,410
Don't let him up.

2
00:00:27,336 --> 00:00:32,638
Final score 7-5,
in favor of Cal Chetley.

3
00:00:33,509 --> 00:00:38,446
Team score
Riverdale 12, Claremore 9.

4
00:00:42,785 --> 00:00:46,448
That was Cal Chetley's first win ever
in a Riverdale uniform.

5
00:00:46,622 --> 00:00:50,251
Let's hear it for our 135-pounder.

6
00:01:27,730 --> 00:01:29,095
Keep going.

7
00:02:17,112 --> 00:02:18,136
No.

8
00:02:20,215 --> 00:02:22,240
Hold him.

9
00:02:23,919 --> 00:02:27,377
- Damn it.
- You got speed? Let's see it.

10
00:02:27,556 --> 00:02:30,047
- Oh yeah, I got speed.
- Show-off!

11
00:02:50,612 --> 00:02:52,170
Thanks.

12
00:02:52,347 --> 00:02:54,338
Hey.

13
00:03:02,124 --> 00:03:04,718
We're gonna pick it up a bit
for regionals.

14
00:03:04,893 --> 00:03:07,361
- More time?
- No.

15
00:03:07,529 --> 00:03:09,554
A better use of the time.

16
00:03:09,832 --> 00:03:10,856
How?

17
00:03:11,033 --> 00:03:13,194
Well, you got that week off
for spring break.

18
00:03:13,368 --> 00:03:15,529
Eleven days till they start.

19
00:03:15,704 --> 00:03:17,865
That's enough time to get serious.

20
00:03:18,674 --> 00:03:21,734
- Serious?
- Yeah.

21
00:03:21,910 --> 00:03:24,037
Serious enough
to show me the reverse cradle?

22
00:03:24,213 --> 00:03:25,703
No.

23
00:03:33,655 --> 00:03:36,419
You know, a good friend
wouldn't eat those in front of me.

24
00:03:37,826 --> 00:03:39,817
Don't watch.

25
00:03:40,429 --> 00:03:43,398
So we going to the dance together?

26
00:03:43,932 --> 00:03:45,900
- Might be working.
- What?

27
00:03:46,068 --> 00:03:48,298
Yes, I'm taking you to the dance.

28
00:03:51,106 --> 00:03:54,906
- What are you wearing?
- I haven't really thought about it yet.

29
00:03:56,411 --> 00:04:00,211
- And dare I ask what you're wearing?
- It's a surprise.

30
00:04:00,382 --> 00:04:03,010
- Well, is it legal?
- I'm making it.

31
00:04:04,319 --> 00:04:05,911
Well, that's encouraging.

32
00:04:08,957 --> 00:04:11,755
Luli, you're not still showing the boobs,
are you?

33
00:04:14,363 --> 00:04:16,388
You're looking good on that mat, Calvin.

34
00:04:16,565 --> 00:04:19,830
You actually look like you know
what you're doing.

35
00:04:21,003 --> 00:04:22,595
Yep.

36
00:04:25,741 --> 00:04:29,438
- Can I talk to you for a minute?
- Depends on what you wanna talk about.

37
00:04:30,078 --> 00:04:35,914
I just want you to know I'm proud of you
for getting out there.

38
00:04:36,585 --> 00:04:38,177
- But?
- But nothing.

39
00:04:38,353 --> 00:04:41,880
I wish it were golf, but it's not, so...

40
00:04:44,593 --> 00:04:47,562
Just know I'm gonna be there for you.

41
00:04:48,964 --> 00:04:51,728
- Okay.
- No matter what, I'm gonna be there.

42
00:04:51,900 --> 00:04:56,860
You gotta know that.
Just don't let it push you too far, please.

43
00:04:57,039 --> 00:04:59,303
Mom, I'm having fun.

44
00:05:00,108 --> 00:05:03,043
One day when we're older
and sitting in rocking chairs...

45
00:05:03,211 --> 00:05:07,147
...I want you to describe to me,
in a way I can understand...

46
00:05:07,316 --> 00:05:09,876
...how wrestling is fun.

47
00:05:11,286 --> 00:05:12,719
Okay.

48
00:05:13,956 --> 00:05:15,423
Hey.

49
00:05:16,024 --> 00:05:18,322
Practice ends about 4:30
every day, right?

50
00:05:19,628 --> 00:05:22,654
So, what exactly do you do
for the next few hours?

51
00:05:26,335 --> 00:05:28,769
I just, you know, jog.

52
00:05:28,937 --> 00:05:30,598
Work on the weights, go to lab.

53
00:05:31,440 --> 00:05:33,169
Okay.

54
00:05:33,342 --> 00:05:35,640
Well, it must be working. You look good.

55
00:05:43,285 --> 00:05:45,150
- Hi.
- Hey.

56
00:05:46,054 --> 00:05:47,783
Shit howdy.
Who are you?

57
00:05:49,825 --> 00:05:53,124
His name's Theo Henderson.
He's a bigtime tiny wrestler.

58
00:05:53,695 --> 00:05:55,925
We call him the Gnat.

59
00:05:57,933 --> 00:06:01,164
- He's the smallest guy I could find.
- Well, it's nice to meet you.

60
00:06:01,336 --> 00:06:03,566
- What's up, man?
- How much do you weigh?

61
00:06:03,739 --> 00:06:07,573
- About 145.
- Each leg?

62
00:06:10,078 --> 00:06:13,013
- Come on, come on, spin. Get it.
- Good job.

63
00:06:13,181 --> 00:06:14,773
Switch.

64
00:06:27,295 --> 00:06:30,731
Come on, Cal. Let's go.
Let's go. Beat him.

65
00:07:12,441 --> 00:07:15,672
This is kind of like
that <i>Extreme Makeover</i> show.

66
00:07:15,844 --> 00:07:17,573
Minus the boob job.

67
00:07:21,917 --> 00:07:25,182
No one's ever done anything like this
for me before.

68
00:07:27,589 --> 00:07:30,285
You're gonna be the belle of the ball,
Miss Luli.

69
00:07:30,459 --> 00:07:33,758
- What are you wearing?
- Something I made myself.

70
00:07:35,497 --> 00:07:37,863
- You do that?
- Oh, yeah.

71
00:07:38,867 --> 00:07:43,463
I make sure I accentuate some areas
more than others.

72
00:07:45,974 --> 00:07:49,137
How about some girl-to-girl advice?

73
00:07:50,679 --> 00:07:52,237
Well, that would be a first.

74
00:07:52,414 --> 00:07:54,814
Scooch over.

75
00:07:58,220 --> 00:08:01,986
Don't sell the whole package
at one time.

76
00:08:02,157 --> 00:08:03,784
Give them small hints.

77
00:08:04,426 --> 00:08:07,418
Keep them guessing
as long as you can.

78
00:08:08,730 --> 00:08:11,255
And then keep them guessing
a little more.

79
00:08:12,667 --> 00:08:18,128
Let them know you have other options.
Be confident. Don't chase, be chased.

80
00:08:19,207 --> 00:08:21,232
I wish.

81
00:08:21,943 --> 00:08:24,969
Well, this helps.

82
00:08:25,147 --> 00:08:27,411
- Miss Chetley?
- Yes?

83
00:08:27,582 --> 00:08:29,982
How old were you
when you first kissed a guy?

84
00:08:30,318 --> 00:08:32,513
- Thirty-two.
- What the hell?

85
00:08:32,687 --> 00:08:36,487
- I was 16.
- I guess.

86
00:08:40,629 --> 00:08:46,192
- Was it Cal's dad?
- No, but it got his attention.

87
00:08:49,004 --> 00:08:50,266
He was second.

88
00:08:51,306 --> 00:08:54,139
- Were you chasing?
- No.

89
00:08:55,811 --> 00:08:57,278
Sort of directing.

90
00:08:58,814 --> 00:09:01,180
That's what I'm talking about.

91
00:09:05,487 --> 00:09:08,752
- Good kid.
- Yeah.

92
00:09:08,990 --> 00:09:11,185
He seems to know
where he's going, man.

93
00:09:12,294 --> 00:09:13,625
Where you been, Mike?

94
00:09:15,063 --> 00:09:19,363
Look, I don't hear from you for I don't
know how long, and then out of the blue?

95
00:09:20,035 --> 00:09:23,129
- I don't know.
- You broke one of your golden rules.

96
00:09:23,305 --> 00:09:28,800
You stopped moving. You're up now.
You look good. Stay up.

97
00:09:28,977 --> 00:09:30,308
Keep moving.

98
00:09:32,247 --> 00:09:33,578
There's your reason.

99
00:09:38,153 --> 00:09:40,417
I don't know, man.
Sometimes it ain't that simple.

100
00:09:42,591 --> 00:09:43,649
Cal.

101
00:09:44,960 --> 00:09:46,484
Come on.

102
00:09:47,963 --> 00:09:49,521
Come on.

103
00:09:52,367 --> 00:09:53,959
Get down.

104
00:09:56,271 --> 00:09:57,761
All right.

105
00:09:57,939 --> 00:10:00,066
The reason the reverse cradle
is such a risk...

106
00:10:00,242 --> 00:10:03,370
...is you gotta trick your opponent
into thinking you're done, okay?

107
00:10:03,545 --> 00:10:05,604
- Okay.
- All right? Now, listen.

108
00:10:06,181 --> 00:10:09,673
If I know you're stalling, I'm going
for a pin. You're gonna ball up.

109
00:10:09,851 --> 00:10:13,082
So when you ball up, I gotta press you
to flatten you out, right?

110
00:10:13,255 --> 00:10:15,815
I'll press you and my head will come
over your shoulder.

111
00:10:15,991 --> 00:10:17,959
You see it come over the second time...

112
00:10:18,126 --> 00:10:20,253
...you got a moment.
You gotta grab a headlock.

113
00:10:21,029 --> 00:10:23,725
Grab it, right there.
Okay, good.

114
00:10:23,899 --> 00:10:27,062
You're gonna roll to your shoulders,
hook a leg and squeeze.

115
00:10:27,235 --> 00:10:28,862
Roll, hook and squeeze. Go.

116
00:10:30,272 --> 00:10:32,240
Lock it. Good.

117
00:10:33,308 --> 00:10:35,799
Reverse cradle. No escape.

118
00:10:37,612 --> 00:10:40,172
- Don't mess with me.
- What?

119
00:10:41,950 --> 00:10:44,578
- You don't mess with me.
- Oh, my God.

120
00:10:47,856 --> 00:10:50,256
Shit howdy. You wanna go?

121
00:11:19,454 --> 00:11:21,581
Check it out, man.
Now the party's started.

122
00:11:21,756 --> 00:11:23,018
Yeah. For sure.

123
00:11:57,192 --> 00:11:59,490
That was nice, Cal.

124
00:12:00,328 --> 00:12:04,094
- You hungry?
- Yeah.

125
00:12:04,766 --> 00:12:06,233
Thank you, thank you, thank you.

126
00:12:06,401 --> 00:12:08,767
We are gonna pick it up a little
for you guys.

127
00:12:21,750 --> 00:12:23,047
Hey, Stringfellow.

128
00:12:24,586 --> 00:12:27,487
Will 5 bucks
get me a little private showing?

129
00:12:29,824 --> 00:12:33,487
Come on, babe. I'm like the only kid here
who hasn't been down Boob Boulevard.

130
00:12:34,362 --> 00:12:36,125
Sounds like a personal problem.

131
00:12:36,297 --> 00:12:39,232
- Barrow, walk away.
- Come on, Luli. I'm a big spender.

132
00:12:39,401 --> 00:12:40,766
- Hey.
- You pervert.

133
00:12:40,935 --> 00:12:42,664
- Hey.
- What the hell are you thinking?

134
00:12:42,837 --> 00:12:45,772
Get off me. What, Chetley?
What are you gonna do?

135
00:12:56,317 --> 00:12:57,909
You all right?

136
00:13:00,155 --> 00:13:02,282
Come on. Let's go.

137
00:13:02,457 --> 00:13:04,857
- You okay? Come on.
- Back up.

138
00:13:05,026 --> 00:13:07,153
Get off me, man.

139
00:13:28,950 --> 00:13:32,113
Why does stuff like this
always happen to me?

140
00:13:32,287 --> 00:13:36,053
Because you're Luli.
You bring it on yourself.

141
00:13:36,224 --> 00:13:38,556
- I hate myself.
- No, you don't.

142
00:13:39,461 --> 00:13:41,326
Just stop trying so hard.

143
00:13:41,496 --> 00:13:44,397
You're like a butterfly in a wind storm.

144
00:13:44,566 --> 00:13:46,466
Meaning what?

145
00:13:47,802 --> 00:13:49,326
Meaning...

146
00:13:50,305 --> 00:13:57,268
I don't know what it means.
It means you're just special, I guess.

147
00:13:58,246 --> 00:14:00,271
No, it doesn't.

148
00:14:00,448 --> 00:14:03,849
- They just don't like me.
- They don't know you.

149
00:14:04,552 --> 00:14:08,648
But you go about it the wrong way.
If they knew you, if they really knew you...

150
00:14:09,557 --> 00:14:11,024
...they'd like you.

151
00:14:14,963 --> 00:14:16,328
I do.

152
00:14:18,600 --> 00:14:20,363
Hey.

153
00:14:22,704 --> 00:14:24,137
You really look nice tonight.

154
00:14:25,340 --> 00:14:27,274
Thank you.

155
00:14:27,775 --> 00:14:30,801
Hey. Come here.

156
00:15:26,267 --> 00:15:28,531
- Hey, Mike. It's been a while.
- Hey.

157
00:15:28,703 --> 00:15:31,035
So, what are you drinking?

158
00:15:33,041 --> 00:15:35,942
- Whatever's on tap is fine.
- You got it.

159
00:16:02,003 --> 00:16:05,461
Excuse me. Here you go.

160
00:16:49,751 --> 00:16:51,218
I don't want to do this, man.

161
00:16:51,386 --> 00:16:54,480
I don't think you got
a whole lot of choice in the matter.

162
00:18:24,912 --> 00:18:26,607
Hello?

163
00:18:28,149 --> 00:18:29,776
What?

164
00:18:31,619 --> 00:18:33,849
What?

165
00:18:35,256 --> 00:18:37,087
Where?

166
00:18:37,392 --> 00:18:41,590
I... I can't understand you.

167
00:18:46,033 --> 00:18:47,591
Cal?

168
00:18:50,938 --> 00:18:52,667
Was that Mike?

169
00:18:54,742 --> 00:18:58,735
- Why is he calling you?
- What did he say?

170
00:18:58,913 --> 00:19:00,574
I don't know.

171
00:19:00,748 --> 00:19:02,773
He's drunk.

172
00:19:02,950 --> 00:19:06,681
- No, he's not drunk.
- He's in jail.

173
00:19:06,854 --> 00:19:10,051
- That's bullshit, Mom. He's not drunk.
- How would you know?

174
00:19:10,224 --> 00:19:11,452
Cal, how would you know?

175
00:19:11,626 --> 00:19:13,890
Because I see him every day.
That's how I know.

176
00:19:14,061 --> 00:19:16,325
We've been working on the mat
for the last month.

177
00:19:16,497 --> 00:19:18,965
Mom, he hasn't had one drink,
so something happened.

178
00:19:19,133 --> 00:19:21,727
- A month? And that's where you've been?
- Just a month.

179
00:19:21,903 --> 00:19:24,394
- How do you think I've won anything?
- Coach Tennant.

180
00:19:24,572 --> 00:19:28,633
- He's working with two dozen wrestlers.
- How and where did you find Mike?

181
00:19:28,810 --> 00:19:31,472
In his house. It's not that hard
if you're really looking.

182
00:19:31,646 --> 00:19:33,170
That's not what I'm asking you.

183
00:19:33,347 --> 00:19:35,144
This is me you're talking to.

184
00:19:35,316 --> 00:19:37,216
You've never kept anything
from me before.

185
00:19:37,385 --> 00:19:41,287
You've spent more time with Mike
in the past month than I have in 10 years.

186
00:19:41,456 --> 00:19:44,857
And you don't even know him,
and you don't think I should know?

187
00:19:45,026 --> 00:19:46,425
- I didn't want it to go wrong.
- What?

188
00:19:46,594 --> 00:19:47,686
- Mike.
- Why?

189
00:19:48,930 --> 00:19:53,424
Because he's my brother.
I have the right to know my own brother.

190
00:20:20,294 --> 00:20:23,457
What do the two of you talk about?

191
00:20:25,299 --> 00:20:26,994
Wrestling.

192
00:20:28,102 --> 00:20:30,002
That's it?

193
00:20:31,806 --> 00:20:34,070
Work and school.

194
00:20:34,408 --> 00:20:38,435
I guess it's not like we talk about the
wives or kids or our favorite colors.

195
00:20:38,980 --> 00:20:40,743
I don't even know him that well, do I?

196
00:20:40,915 --> 00:20:43,713
That's not fair, Cal.

197
00:20:45,720 --> 00:20:48,553
I was afraid of you and Mike together.

198
00:20:48,723 --> 00:20:54,423
- Why?
- Mike is unpredictable.

199
00:20:54,595 --> 00:20:57,428
I became afraid of him.

200
00:20:58,299 --> 00:21:00,494
I didn't know how to help him.

201
00:21:03,271 --> 00:21:07,230
But you're the best friend I've ever had.

202
00:21:16,183 --> 00:21:19,016
What exactly happened in the car?

203
00:21:23,591 --> 00:21:26,958
Your dad and Mike...

204
00:21:29,163 --> 00:21:34,328
...were headed to Eufaula
to scout one of Mike's opponents.

205
00:21:34,502 --> 00:21:38,233
That's what they did.
These guys were prepared, Cal.

206
00:21:39,507 --> 00:21:44,137
But there was...
There was heavy snow, ice.

207
00:21:47,515 --> 00:21:49,813
Mike was thrown from the car.

208
00:21:52,653 --> 00:21:54,848
Mac wasn't.

209
00:21:56,390 --> 00:21:59,848
I begged them not to go.

210
00:22:00,027 --> 00:22:04,327
Dad knew Mike
could take this other kid, but...

211
00:22:05,066 --> 00:22:06,090
But...

212
00:22:06,267 --> 00:22:10,397
And he didn't think they needed to go,
but Mike pushed it.

213
00:22:10,571 --> 00:22:14,701
I remember him throwing
your dad's words back in his face.

214
00:22:14,875 --> 00:22:16,775
"Take no opponent for granted."

215
00:22:19,680 --> 00:22:22,808
So Mac did what he always did.

216
00:22:23,718 --> 00:22:26,152
He kept his word.

217
00:22:29,557 --> 00:22:33,459
- Did Mike quit wrestling after that?
- No. No.

218
00:22:33,628 --> 00:22:36,153
No, he got hurt a year later...

219
00:22:38,099 --> 00:22:43,127
...but he really gave up
the night your dad died.

220
00:22:44,338 --> 00:22:47,205
He blamed himself for Mac's death.

221
00:22:47,375 --> 00:22:48,933
I think maybe...

222
00:22:51,479 --> 00:22:52,707
What?

223
00:22:56,217 --> 00:22:57,980
I was...

224
00:22:59,954 --> 00:23:03,014
Cal, I was so lost.

225
00:23:04,025 --> 00:23:10,624
I was so devastated,
and I didn't know it at the time...

226
00:23:11,165 --> 00:23:16,626
...but I blamed him too, and Mike felt it.

227
00:23:17,938 --> 00:23:22,534
I was 18 when Mike was born.
Your dad and I were just kids.

228
00:23:22,710 --> 00:23:25,338
He was my first,
and your first is always your baby.

229
00:23:26,480 --> 00:23:28,107
You were an adult
when you came out.

230
00:23:28,282 --> 00:23:29,579
I had to ask for your ID.

231
00:23:33,654 --> 00:23:35,849
Then I lost both of them.

232
00:23:36,023 --> 00:23:42,690
Ma, you said that Mike blamed himself...

233
00:23:43,531 --> 00:23:46,261
...for Dad's being killed.

234
00:23:46,434 --> 00:23:48,766
Did anybody ever tell him
it wasn't his fault?

235
00:23:53,207 --> 00:23:57,405
I think everybody in here has a shot
at the medal round this weekend.

236
00:23:57,578 --> 00:24:00,706
The next couple of days, we're gonna
show them what we're made of.

237
00:24:01,515 --> 00:24:04,746
So let's get it in our heads right now.
Okay?

238
00:24:04,919 --> 00:24:09,379
I'll see you guys in the morning.
Bus leaves at 7:30 sharp. Be here.

239
00:24:10,424 --> 00:24:13,188
Let's get our hands in here.
Tornadoes on three.

240
00:24:13,694 --> 00:24:14,752
One, two, three.

241
00:24:14,929 --> 00:24:16,920
Tornadoes.

242
00:24:39,353 --> 00:24:42,083
Hey. Good luck. Good to see you.

243
00:25:27,201 --> 00:25:29,192
Something told me I should be here.

244
00:25:31,005 --> 00:25:32,597
Why now?

245
00:25:35,543 --> 00:25:37,306
Cal.

246
00:25:39,380 --> 00:25:41,575
He believes in you...

247
00:25:41,749 --> 00:25:46,209
...and he was right.

248
00:25:47,888 --> 00:25:51,517
A friend in the DA's office...

249
00:25:52,593 --> 00:25:55,756
...a guy I dated for about 20 minutes...

250
00:25:55,930 --> 00:25:58,194
...ran down the waitress
where you were arrested.

251
00:25:58,365 --> 00:26:04,964
She'll testify you didn't take a drink
and you didn't start the fight.

252
00:26:08,209 --> 00:26:10,700
I talked with your parole officer.

253
00:26:13,314 --> 00:26:15,339
Cal needs you, Mike.

254
00:26:16,784 --> 00:26:18,012
It scares you, doesn't it?

255
00:26:19,954 --> 00:26:21,922
Yes.

256
00:26:25,993 --> 00:26:28,086
This scares me...

257
00:26:34,602 --> 00:26:38,504
...but Cal really wants you
to be there for him.

258
00:26:39,640 --> 00:26:41,699
And I want you to be there.

259
00:26:43,143 --> 00:26:44,667
For Cal.

260
00:26:45,913 --> 00:26:47,972
You know, Mike...

261
00:26:50,484 --> 00:26:52,452
...I've tracked you over three states...

262
00:26:52,620 --> 00:26:53,882
...and not a day goes by...

263
00:26:54,054 --> 00:26:57,751
...that I don't think maybe
this is the day you'll show up.

264
00:26:58,659 --> 00:27:02,618
And I'm listening to Cal tell me
that you two wrestle, that you talk.

265
00:27:02,796 --> 00:27:05,697
And I'm thinking, " I want that."
I want...

266
00:27:05,866 --> 00:27:10,462
I want my son back,
but I still don't know how.

267
00:27:12,640 --> 00:27:15,700
Let's get you out of here. Come on.

268
00:27:57,751 --> 00:28:00,879
Yeah! Yeah! Yeah, kid!

269
00:28:13,133 --> 00:28:15,465
- You did fine.
- Hey, Cal. Calvin?

270
00:28:15,636 --> 00:28:21,973
<i>With that win, Riverdale's Cal Chetley</i>
<i>moves into the 135-pound semi-finals.</i>

271
00:28:23,911 --> 00:28:25,469
What the hell?

272
00:28:41,295 --> 00:28:43,024
You don't belong here.

273
00:28:43,697 --> 00:28:47,030
- Maybe I do.
- No, you don't.

274
00:28:55,209 --> 00:28:57,643
For all the goodness
your dad brought to your life...

275
00:28:57,811 --> 00:29:03,841
...I don't believe he ever taught you
how to accept loss.

276
00:29:07,154 --> 00:29:12,421
It was always
about showing up prepared.

277
00:29:13,394 --> 00:29:15,862
I need you to show up.

278
00:29:16,030 --> 00:29:18,498
Maybe I'm not right for Cal.

279
00:29:22,069 --> 00:29:26,506
It's like you said, just don't think
I know how to lose.

280
00:29:27,775 --> 00:29:29,333
You do now.

281
00:29:31,678 --> 00:29:34,146
This may sound simple...

282
00:29:36,550 --> 00:29:39,348
...but that boy waiting for you...

283
00:29:39,887 --> 00:29:42,685
...hasn't experienced
the pain we've been through.

284
00:29:45,793 --> 00:29:50,890
I can't breathe sometimes.

285
00:29:55,736 --> 00:29:58,398
Because in my heart...

286
00:29:59,907 --> 00:30:01,966
...I know I blamed you at a time...

287
00:30:04,545 --> 00:30:07,571
...when I should have been
protecting you.

288
00:30:09,516 --> 00:30:11,381
And I never made it right.

289
00:30:16,090 --> 00:30:18,888
I am so sorry.

290
00:30:22,296 --> 00:30:24,764
I'm your mother...

291
00:30:25,599 --> 00:30:29,160
...but I was choking
on my grief and my anger.

292
00:30:34,408 --> 00:30:40,904
But, baby, we have to somehow
put Mac to rest.

293
00:30:43,383 --> 00:30:44,941
Yeah.

294
00:30:45,119 --> 00:30:48,885
I think that's what Cal is trying to do.

295
00:30:49,957 --> 00:30:51,584
He's...

296
00:30:53,527 --> 00:30:56,291
He's trying to help us.

297
00:31:10,777 --> 00:31:13,337
Dad, he made me feel invincible.

298
00:31:13,514 --> 00:31:17,382
I just... I couldn't pull him from the car.

299
00:31:23,824 --> 00:31:29,729
Cal. Cal is trying to pull you out.

300
00:31:31,398 --> 00:31:32,865
Let him.

301
00:31:56,990 --> 00:31:58,014
Was I right?

302
00:31:58,458 --> 00:32:00,255
Seems that you were.

303
00:32:00,961 --> 00:32:03,429
Is he out now?

304
00:32:04,898 --> 00:32:06,365
Yeah.

305
00:32:06,533 --> 00:32:08,364
Beyond that...

306
00:32:11,605 --> 00:32:13,436
...I'm not sure.

307
00:32:13,607 --> 00:32:18,010
Hopefully he's working some things out.

308
00:32:18,178 --> 00:32:19,202
How did you do?

309
00:32:20,647 --> 00:32:23,013
I'm still in it.

310
00:32:23,183 --> 00:32:25,583
Good for you.

311
00:32:26,286 --> 00:32:27,947
Now, go wrestle.

312
00:32:29,790 --> 00:32:32,190
When you show up, things happen.

313
00:32:34,261 --> 00:32:36,422
I know that's what Dad would say.

314
00:32:38,165 --> 00:32:40,099
- You coming?
- I'll be there.

315
00:32:40,867 --> 00:32:42,300
All right.

316
00:32:51,278 --> 00:32:57,376
<i>Welcome to Tulsa in day two of the</i>
<i>Oklahoma District Three championship.</i>

317
00:32:57,551 --> 00:33:01,715
<i>All coaches report</i>
<i>to the director's table by 10.</i>

318
00:33:01,888 --> 00:33:06,757
<i>Jesuits Lugats,</i>
<i>report to the scores table at Mat 2.</i>

319
00:33:18,672 --> 00:33:20,071
Know anything about Leonard?

320
00:33:20,240 --> 00:33:25,439
Well, from what I hear,
he puts his pants on one leg at a time.

321
00:33:25,612 --> 00:33:29,070
<i>All wrestlers begin clearing the mats</i>
<i>at this time.</i>

322
00:33:30,183 --> 00:33:33,084
<i>One-hundred and 35 pound</i>
<i>semi-final match.</i>

323
00:33:33,253 --> 00:33:36,188
<i>Hayes vs. Rayber.</i>

324
00:34:04,551 --> 00:34:07,850
Red. What are you doing here?

325
00:34:08,021 --> 00:34:10,285
Rumor has it
you won some matches yesterday.

326
00:34:11,124 --> 00:34:12,955
Yeah.

327
00:34:13,126 --> 00:34:14,286
Where you going?

328
00:34:14,461 --> 00:34:16,725
Oh, I'm done.

329
00:34:17,564 --> 00:34:21,967
I didn't want this.
I was looking for something else.

330
00:34:22,636 --> 00:34:23,660
You sure about that?

331
00:34:25,739 --> 00:34:30,506
Took a lot of courage to come this far.
You made something happen.

332
00:34:30,677 --> 00:34:33,305
Now I hear you're two matches away
from a district title.

333
00:34:33,480 --> 00:34:35,311
I'd say that's time pretty well spent.

334
00:34:36,783 --> 00:34:39,377
How come you always seem to know
what to say?

335
00:34:39,553 --> 00:34:42,647
Sometimes these things take time
to work out.

336
00:34:42,823 --> 00:34:46,725
But no one's asking you to win, Cal.

337
00:34:46,893 --> 00:34:48,861
Just don't quit.

338
00:34:50,964 --> 00:34:56,903
Hey. I'm gonna grab a cup of coffee,
head inside there and get a seat.

339
00:34:57,070 --> 00:34:59,698
Looking forward to your match.

340
00:35:08,115 --> 00:35:10,083
<i>Winner Mark Hayes, by decision.</i>

341
00:35:21,361 --> 00:35:25,559
<i>Second semi-finals, 135-pound class.</i>
<i>John Leonard, North Shore...</i>

342
00:35:25,732 --> 00:35:28,895
<i>...vs. Cal Chetley of Riverdale.</i>

343
00:35:35,041 --> 00:35:38,374
<i>Leonard vs. Chetley, center mat.</i>

344
00:35:48,789 --> 00:35:50,518
- Hey, where you been?
- Just wandering.

345
00:35:50,690 --> 00:35:51,884
- Come on, you ready?
- Yeah.

346
00:35:52,058 --> 00:35:56,119
All right, listen. Two minutes at a time.
Listen for me. Get out there.

347
00:36:11,144 --> 00:36:14,773
<i>Chetley in the red anklet,</i>
<i>Leonard in the green.</i>

348
00:36:16,716 --> 00:36:18,980
Give them cauliflower ear!

349
00:36:21,922 --> 00:36:26,359
Come on, Cal. You can do it.

350
00:36:30,864 --> 00:36:32,764
Come on, shoot.

351
00:36:50,450 --> 00:36:53,544
You're okay. You're all right.
You're all right. You're all right.

352
00:36:58,959 --> 00:37:00,824
Go, go, go!

353
00:37:28,455 --> 00:37:34,223
<i>After one period,</i>
<i>Leonard leads Chetley 5 to 2.</i>

354
00:37:34,928 --> 00:37:37,988
You're all right. You're good.
You're good. You're good.

355
00:37:54,281 --> 00:37:55,873
Come on!

356
00:38:09,396 --> 00:38:10,954
All right.

357
00:38:17,771 --> 00:38:19,568
Get him!

358
00:38:21,741 --> 00:38:23,971
There you go.

359
00:38:26,046 --> 00:38:28,640
Yeah. Yeah.
Come on, get in.

360
00:38:28,815 --> 00:38:30,180
Come on. Go! Go!

361
00:38:30,383 --> 00:38:32,647
- Go. Crack him. Crack him.
- Damn it, come on.

362
00:38:37,657 --> 00:38:39,784
Come on. Yep.

363
00:38:42,162 --> 00:38:45,393
Come on, Cal!

364
00:38:45,565 --> 00:38:47,089
Take him down.

365
00:38:52,572 --> 00:38:54,005
Hey.

366
00:39:02,282 --> 00:39:03,476
You're all right, man.

367
00:39:15,128 --> 00:39:19,462
<i>Winner Chetley, Riverdale High School</i>
<i>by injury default.</i>

368
00:39:22,202 --> 00:39:24,466
That's my Cal!

369
00:39:34,781 --> 00:39:36,009
Oh, God.

370
00:39:36,182 --> 00:39:39,276
- Who taught you the guillotine?
- I've been doing a little homework.

371
00:39:39,452 --> 00:39:40,817
That was textbook wrestling.

372
00:39:40,987 --> 00:39:43,512
You showed a lot of character,
no two ways about it.

373
00:39:43,690 --> 00:39:44,952
I'll come back and get you.

374
00:39:45,125 --> 00:39:46,956
- Good to see you, Mike.
- You too, coach.

375
00:39:47,127 --> 00:39:50,153
- Been a long time.
- Smell of the mat never leaves you.

376
00:39:50,330 --> 00:39:54,027
You're welcome to take the coach's seat
next to me when he's up if you want.

377
00:39:54,200 --> 00:39:57,363
- I'd like that.
- Good. I recognize the Chetley spirit.

378
00:39:58,438 --> 00:40:01,168
In about an hour I'll come back
and get the both of you.

379
00:40:01,341 --> 00:40:02,831
Okay.

380
00:40:03,410 --> 00:40:05,810
- Everything all right?
- Yeah.

381
00:40:05,979 --> 00:40:08,140
- Yeah.
- Yeah.

382
00:40:09,416 --> 00:40:12,385
- I had a little help.
- Mom?

383
00:40:16,356 --> 00:40:20,315
Okay, remember, Cal.
You're not me. You're not Dad.

384
00:40:20,493 --> 00:40:22,017
You gotta wrestle your own way.

385
00:40:22,195 --> 00:40:24,663
Use your opponent's momentum
against him.

386
00:40:25,365 --> 00:40:28,425
Use them long legs, damn it.
It's your best weapon.

387
00:40:28,601 --> 00:40:30,364
- You got your music.
- Right here.

388
00:40:30,537 --> 00:40:32,596
You got 10 minutes. What do you got?

389
00:40:32,772 --> 00:40:36,208
- Opera.
- What?

390
00:40:36,376 --> 00:40:40,176
It's like Italian opera.

391
00:40:42,716 --> 00:40:46,777
Who are you?
Never mind, we're doing it your way.

392
00:40:47,187 --> 00:40:50,281
- Crank it up, you take it in.
- All right.

393
00:40:52,992 --> 00:40:54,687
Opera.

394
00:41:27,560 --> 00:41:29,721
This is a good day.

395
00:41:32,432 --> 00:41:35,196
You know our boy listens to opera?

396
00:41:35,802 --> 00:41:39,533
I want you to know you're everything
Cal described, only better looking.

397
00:41:39,706 --> 00:41:42,266
- I'm Luli.
- Hi, Luli.

398
00:41:42,442 --> 00:41:45,411
Could you give this to Cal
before this last match?

399
00:41:45,578 --> 00:41:46,977
It's real important.

400
00:41:47,147 --> 00:41:49,980
I sure will.
What is this, a Volkswagen?

401
00:42:07,467 --> 00:42:08,695
Hey, Shaquille O'Neal.

402
00:42:09,269 --> 00:42:10,293
That was horrible.

403
00:42:11,538 --> 00:42:12,562
What's this?

404
00:42:12,739 --> 00:42:15,469
This is a heavy present
from somebody named Luli.

405
00:42:20,513 --> 00:42:25,007
"Here's your edge. I love you. Lu."

406
00:42:27,821 --> 00:42:31,348
- Oh, my God. Oh, my God.
- A pump.

407
00:42:31,524 --> 00:42:34,823
- Now, that's romantic.
- It is if you know Luli.

408
00:42:35,762 --> 00:42:37,992
What the hell's this?

409
00:42:40,700 --> 00:42:43,931
That's Luli too.

410
00:42:47,307 --> 00:42:50,743
Hey, give us one more, kid.
We're all out there.

411
00:42:51,611 --> 00:42:52,873
I will.

412
00:42:53,046 --> 00:42:54,308
Cal.

413
00:42:54,480 --> 00:42:56,345
You're already a medalist.

414
00:42:56,950 --> 00:42:58,315
Let's go out as number one.

415
00:42:59,285 --> 00:43:00,718
Okay.

416
00:44:06,953 --> 00:44:08,818
What the hell?

417
00:44:09,689 --> 00:44:10,815
Damn, coach.

418
00:44:10,990 --> 00:44:13,083
I hope the fire marshal
doesn't shut us down.

419
00:44:30,143 --> 00:44:31,974
You know that guy?

420
00:44:32,912 --> 00:44:34,004
That's Dad's coach.

421
00:44:35,415 --> 00:44:39,317
- Red's Dad's coach?
- Red. That's Harry.

422
00:44:40,186 --> 00:44:42,347
- Harry?
- Harry Newman.

423
00:44:44,490 --> 00:44:46,151
<i>"Peace, my fearless friend.</i>

424
00:44:46,926 --> 00:44:49,861
He touched so many
with his courage and heart.

425
00:44:50,463 --> 00:44:53,330
You took it far, we'll take it from here.
Harry Newman."

426
00:44:56,536 --> 00:44:57,867
That guy's Harry Newman?

427
00:44:59,672 --> 00:45:01,537
You know him?

428
00:45:01,874 --> 00:45:04,342
Yeah. Through fishing, mostly.

429
00:45:08,815 --> 00:45:11,807
<i>- And now, our 135-pound finalist...</i>
- Dad's here.

430
00:45:11,985 --> 00:45:18,288
<i>...representing St. Paul's with a dual-meet</i>
<i>record of 23 and 0, Mark Hayes.</i>

431
00:45:18,458 --> 00:45:21,222
<i>And representing</i>
<i>Riverdale High School...</i>

432
00:45:21,394 --> 00:45:25,421
<i>...with a dual meet record of 5 and 6,</i>
<i>Calvin Chetley.</i>

433
00:45:25,598 --> 00:45:29,125
<i>Chetley in the red,</i>
<i>Hayes in the green.</i>

434
00:45:31,170 --> 00:45:34,230
<i>And please welcome back</i>
<i>to the tournament past All-American...</i>

435
00:45:34,407 --> 00:45:36,671
<i>...NCAA finalist...</i>

436
00:45:36,843 --> 00:45:42,372
<i>...and three-time</i>
<i>state champion, Mike Chetley.</i>

437
00:45:53,726 --> 00:45:55,455
Here we go.

438
00:46:03,302 --> 00:46:05,668
Get around him.
Gotta get around him. Come on, now.

439
00:46:07,940 --> 00:46:09,532
Get him, Cal.

440
00:46:15,715 --> 00:46:16,841
Let's go.

441
00:46:45,578 --> 00:46:47,478
He's setting it up.

442
00:47:03,129 --> 00:47:04,221
Come on. Come on!

443
00:47:11,137 --> 00:47:14,334
- He's out! He's out!
- Come on. Cinch it, Cal. Cinch it.

444
00:47:15,108 --> 00:47:17,303
- Get him!
- Come on!

445
00:47:17,477 --> 00:47:18,501
Oh, baby, buckle up.

446
00:47:23,683 --> 00:47:26,675
Squeeze it, Cal. Squeeze it.

447
00:47:30,423 --> 00:47:33,290
Cal, squeeze it! Squeeze it!

448
00:47:33,459 --> 00:47:34,926
Come on, come on!

449
00:47:35,361 --> 00:47:38,228
Come on! Get him.

450
00:47:38,397 --> 00:47:42,424
- Get him, Cal, get him!
- Come on, cinch it, Cal, cinch it.

451
00:47:42,602 --> 00:47:44,160
Come on.

452
00:47:45,505 --> 00:47:47,598
- Get him, Cal!
- Come on!

453
00:47:48,341 --> 00:47:50,502
Cal, come on! Pin him!

454
00:48:39,525 --> 00:48:45,327
<i>Mark Hayes, winner by decision.</i>
<i>Final score: Hayes 11, Chetley 9.</i>

455
00:50:24,430 --> 00:50:28,867
<i>Like I said,</i>
<i>not all legends are about victory.</i>

456
00:50:29,035 --> 00:50:31,162
<i>Some are about struggle...</i>

457
00:50:31,337 --> 00:50:35,000
<i>...finding out who you are</i>
<i>and your reason for being.</i>

458
00:50:35,641 --> 00:50:40,578
<i>Big Mac Chetley died before</i>
<i>he had a chance to finish the job.</i>

459
00:50:40,746 --> 00:50:42,373
<i>That was his greatest fear:</i>

460
00:50:42,548 --> 00:50:46,382
<i>that Sharon and the boys</i>
<i>would struggle on without him.</i>

461
00:50:53,292 --> 00:50:55,522
- You got heart, kid.
- Thanks.

462
00:50:55,695 --> 00:50:58,528
- You thought I was done, didn't you?
- Yeah, I did.

463
00:50:58,698 --> 00:51:01,895
<i>He once asked me</i>
<i>if anything were to happen to him...</i>

464
00:51:02,068 --> 00:51:05,060
<i>...that I might help keep an eye</i>
<i>on the boys.</i>

465
00:51:05,237 --> 00:51:07,899
<i>Mac, your boys are fine.</i>

466
00:51:08,074 --> 00:51:10,406
<i>So is your girl.</i>

