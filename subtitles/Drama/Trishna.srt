1
00:02:37,907 --> 00:02:39,867
Can we go off road?

2
00:03:36,007 --> 00:03:37,174
The car is stuck

3
00:03:37,384 --> 00:03:38,384
lt's stuck?

4
00:07:22,275 --> 00:07:23,525
Are you working next week?

5
00:07:23,651 --> 00:07:24,610
Yes

6
00:07:24,736 --> 00:07:26,320
l'm tired. My feet are killing me

7
00:07:27,030 --> 00:07:28,655
But those boys were gorgeous

8
00:07:34,079 --> 00:07:36,372
Next week l want to get a new sari

9
00:07:40,585 --> 00:07:42,753
No, we only live up the road

10
00:07:47,676 --> 00:07:49,218
Ask him to sit at the front

11
00:07:51,346 --> 00:07:52,596
Don't worry. lt'll be OK

12
00:07:53,431 --> 00:07:54,807
He doesn't understand Hindi

13
00:07:55,558 --> 00:07:57,142
His mother is English

14
00:07:57,435 --> 00:07:58,394
Let's go

15
00:07:59,562 --> 00:08:01,105
Has he never learnt Hindi?

16
00:08:03,066 --> 00:08:05,359
Now you're in lndia you'll learn

17
00:08:18,540 --> 00:08:20,374
l know some Marwari

18
00:08:20,750 --> 00:08:21,834
You know Marwari?

19
00:08:24,379 --> 00:08:26,005
You look very beautiful

20
00:08:31,219 --> 00:08:33,345
l want to say - you are beautiful

21
00:08:43,440 --> 00:08:45,441
Aren't you going to invite me for some tea?

22
00:08:45,692 --> 00:08:47,234
No my father is home

23
00:09:44,084 --> 00:09:45,250
Trishna get me some water!

24
00:09:49,381 --> 00:09:50,255
Who were those boys?

25
00:09:52,050 --> 00:09:53,050
ln the taxi

26
00:09:54,135 --> 00:09:55,552
Just tourists from the hotel

27
00:10:13,238 --> 00:10:14,279
Wake up

28
00:10:15,490 --> 00:10:16,657
Wake up. You're going to be late

29
00:10:17,450 --> 00:10:18,283
Come on

30
00:10:23,081 --> 00:10:24,873
Trishna. Wake up

31
00:11:02,287 --> 00:11:03,287
Hello

32
00:11:03,997 --> 00:11:05,164
How's it going?

33
00:11:07,459 --> 00:11:08,459
Sort that out

34
00:11:14,758 --> 00:11:16,133
Move it along

35
00:11:33,818 --> 00:11:34,985
Put it there

36
00:11:42,702 --> 00:11:44,119
Put it down here

37
00:11:44,245 --> 00:11:45,162
They are taking their time

38
00:11:45,288 --> 00:11:47,206
Get a move on stop looking at me

39
00:11:48,124 --> 00:11:49,291
Not like that

40
00:12:44,806 --> 00:12:45,806
Dad

41
00:12:51,229 --> 00:12:52,271
Dad!

42
00:13:07,162 --> 00:13:08,746
Do you get a lot of pain?

43
00:13:10,415 --> 00:13:12,499
Relax your hand

44
00:13:12,959 --> 00:13:14,376
Lift it up slightly, that's right

45
00:14:17,732 --> 00:14:18,857
Dad?

46
00:14:23,071 --> 00:14:24,405
Are you OK?

47
00:14:28,159 --> 00:14:30,411
Forgive me

48
00:14:30,537 --> 00:14:33,205
Don't say that. Get some rest

49
00:14:58,606 --> 00:14:59,857
Stop the car

50
00:15:24,591 --> 00:15:26,133
How far to the airport?

51
00:15:26,718 --> 00:15:27,760
Quite far

52
00:17:29,341 --> 00:17:31,633
ls this Trishna's father's house?

53
00:17:33,762 --> 00:17:34,887
Pratiksha!

54
00:17:35,096 --> 00:17:36,096
What?

55
00:17:37,932 --> 00:17:38,932
What is it?

56
00:17:39,726 --> 00:17:41,143
ls this Trishna's father's house?

57
00:17:41,269 --> 00:17:42,061
Yes

58
00:17:42,145 --> 00:17:43,187
lt's for Trishna

59
00:17:44,481 --> 00:17:45,689
Mum!

60
00:17:45,815 --> 00:17:47,107
Trishna! Trishna!

61
00:17:47,525 --> 00:17:48,734
What?

62
00:17:48,860 --> 00:17:50,235
There's a letter for you!

63
00:17:50,945 --> 00:17:52,363
Where from?

64
00:18:01,539 --> 00:18:02,664
Yes Dad?

65
00:18:02,791 --> 00:18:03,916
Here. Read this to me

66
00:18:14,844 --> 00:18:15,886
What's it say?

67
00:18:15,929 --> 00:18:17,304
lt is saying

68
00:18:19,224 --> 00:18:21,600
They're offering me a place
at the hotel Samode Bagh in Jaipur

69
00:18:21,726 --> 00:18:23,435
Trishna where's Jaipur?

70
00:18:23,603 --> 00:18:25,479
Jaipur is very far

71
00:18:27,357 --> 00:18:29,983
Every week l will get 2500 rupees

72
00:18:30,110 --> 00:18:31,527
2500 rupees?

73
00:18:32,320 --> 00:18:34,113
And 500 rupees for the journey

74
00:18:35,740 --> 00:18:38,325
l didn't think the boy would come back

75
00:18:38,785 --> 00:18:39,785
Can you go by yourself?

76
00:18:40,578 --> 00:18:41,412
Live all alone?

77
00:18:42,080 --> 00:18:43,789
lt's 2500 rupees a week

78
00:19:00,306 --> 00:19:02,599
Go to school every day! OK?

79
00:20:16,341 --> 00:20:17,257
Hello

80
00:20:17,425 --> 00:20:18,634
Hotel Samode Bagh

81
00:20:19,094 --> 00:20:20,427
My name is Trishna

82
00:20:21,012 --> 00:20:21,887
Where are you?

83
00:20:22,472 --> 00:20:24,890
l'm at the bus station in Jaipur

84
00:20:25,266 --> 00:20:27,017
Stay there and someone will pick you up

85
00:20:27,519 --> 00:20:28,686
OK. Thank you

86
00:22:55,250 --> 00:22:57,292
At your command, Sir

87
00:23:01,631 --> 00:23:02,881
You are most welcome

88
00:23:05,510 --> 00:23:06,385
At your command, Sir

89
00:23:32,746 --> 00:23:33,579
Come

90
00:24:03,651 --> 00:24:07,696
Yes Mum. lt's the same. l'm fine

91
00:24:16,164 --> 00:24:18,457
The world has come alive

92
00:24:18,750 --> 00:24:20,501
And dances with joy

93
00:24:20,835 --> 00:24:24,380
Like my heart

94
00:24:40,188 --> 00:24:41,939
With your help

95
00:24:42,816 --> 00:24:44,566
With you by my side

96
00:24:45,193 --> 00:24:48,487
You make me happy

97
00:24:49,739 --> 00:24:51,657
When you look at me

98
00:24:52,242 --> 00:24:53,992
When you touch me

99
00:24:54,536 --> 00:24:56,954
You make me happy

100
00:25:00,500 --> 00:25:02,793
Hi Devshri. How are you?

101
00:25:04,671 --> 00:25:06,296
Do you miss me?

102
00:25:08,675 --> 00:25:09,633
Come

103
00:26:06,608 --> 00:26:08,942
When l was at college

104
00:26:09,778 --> 00:26:11,195
l had so much fun

105
00:26:12,238 --> 00:26:14,239
We would finish lessons

106
00:26:14,908 --> 00:26:17,117
go to the canteen,
drink some tea, eat some food

107
00:26:17,577 --> 00:26:18,702
then go and sit in the park

108
00:26:19,287 --> 00:26:21,872
We used to make fun of all the new girls

109
00:26:51,528 --> 00:26:53,612
Don't be scared, it won't hurt

110
00:27:04,374 --> 00:27:05,374
Knock on the door

111
00:27:05,500 --> 00:27:07,334
Say, 'Room service'

112
00:27:07,502 --> 00:27:08,752
Then show the menu

113
00:27:09,170 --> 00:27:12,423
This is our dining tent
where we serve breakfast

114
00:27:12,549 --> 00:27:13,841
l'll show you the kitchen

115
00:27:13,967 --> 00:27:17,136
A la carte and the buffet
come from the same kitchen

116
00:27:18,555 --> 00:27:20,431
This is for the buffet

117
00:27:20,807 --> 00:27:21,640
OK

118
00:27:37,574 --> 00:27:39,533
My plaster has come off

119
00:27:40,410 --> 00:27:41,368
Yeah. Really

120
00:28:19,532 --> 00:28:20,407
Who is this for?

121
00:28:20,533 --> 00:28:21,533
lt's for you

122
00:28:22,077 --> 00:28:22,910
Chanchal

123
00:28:23,036 --> 00:28:24,661
Put the box over there

124
00:28:26,873 --> 00:28:28,040
Yes. lt's good there

125
00:28:50,647 --> 00:28:52,690
Turn the volume down

126
00:28:54,401 --> 00:28:56,443
l wish he loved me

127
00:28:57,862 --> 00:28:59,363
ln your dreams

128
00:28:59,614 --> 00:29:02,199
l'd have my own hotel

129
00:29:03,284 --> 00:29:05,369
l could do whatever l want

130
00:29:06,579 --> 00:29:09,081
We would go to Switzerland on holiday

131
00:29:10,500 --> 00:29:11,667
How many children will you have?

132
00:29:11,793 --> 00:29:12,835
l haven't decided yet

133
00:30:52,894 --> 00:30:54,520
Trishna what are you doing? Hurry up

134
00:31:06,157 --> 00:31:08,367
OK we'll be going now

135
00:31:20,714 --> 00:31:21,714
Hello

136
00:35:21,663 --> 00:35:23,122
You take out the inside

137
00:35:23,581 --> 00:35:24,581
Hold it like this

138
00:35:26,793 --> 00:35:27,626
Where are you from?

139
00:35:28,169 --> 00:35:29,962
l'm from Ossian. Have you heard of it?

140
00:35:30,088 --> 00:35:31,088
No

141
00:35:31,631 --> 00:35:33,841
l don't think anything
can happen staying here

142
00:35:34,092 --> 00:35:35,300
Why are you saying that?

143
00:35:35,552 --> 00:35:37,094
lt's not so good here

144
00:35:37,220 --> 00:35:38,804
l have big dreams

145
00:35:38,930 --> 00:35:41,974
Be the manager of a big hotel

146
00:35:42,809 --> 00:35:44,351
l am thinking about moving country

147
00:35:44,519 --> 00:35:45,811
Solid planning

148
00:36:05,707 --> 00:36:06,957
Sir is in the bedroom

149
00:37:27,330 --> 00:37:29,248
This pair will match

150
00:37:31,626 --> 00:37:32,626
How do they look?

151
00:37:38,133 --> 00:37:39,633
lt's me Trishna

152
00:37:40,719 --> 00:37:41,552
l'm not sure

153
00:37:42,053 --> 00:37:43,721
l think we are just passing the market

154
00:37:46,808 --> 00:37:48,142
This is my friend Chanchal

155
00:37:48,852 --> 00:37:51,270
Chanchal these are all my friends
from College

156
00:38:19,466 --> 00:38:22,926
Hey guys! l was thinking
it's nearly over here

157
00:38:23,053 --> 00:38:26,055
should we go out somewhere?

158
00:38:28,350 --> 00:38:30,017
Some parties are long, some are short

159
00:38:31,853 --> 00:38:32,811
Casbah?

160
00:38:33,146 --> 00:38:34,521
How about it?

161
00:38:39,778 --> 00:38:42,488
Come on. We've already
told the boss we'll be back by 9

162
00:38:42,614 --> 00:38:43,822
Relax

163
00:38:44,574 --> 00:38:45,532
OK l'm going on my own

164
00:38:45,825 --> 00:38:46,784
- You'll be OK right?
- Yes

165
00:38:46,951 --> 00:38:48,535
OK take care

166
00:38:53,625 --> 00:38:55,793
She worries too much

167
00:39:14,187 --> 00:39:15,187
Stop!

168
00:39:26,366 --> 00:39:27,825
Who are you calling?

169
00:39:29,035 --> 00:39:30,661
You can use my phone

170
00:39:32,539 --> 00:39:33,706
Leave me alone

171
00:39:35,000 --> 00:39:36,500
Don't touch me or l'll scream

172
00:39:40,463 --> 00:39:41,922
What's your problem?

173
00:39:45,301 --> 00:39:46,135
Look at you

174
00:39:46,261 --> 00:39:47,761
You haven't got any balls

175
00:41:50,260 --> 00:41:57,224
Where were you?

176
00:41:58,143 --> 00:41:59,601
Go to sleep

177
00:42:00,770 --> 00:42:02,479
Jay phoned me after you left

178
00:42:06,067 --> 00:42:07,818
He was looking for you

179
00:42:12,240 --> 00:42:13,240
l rang you

180
00:42:13,366 --> 00:42:15,367
Why didn't you answer?

181
00:42:23,168 --> 00:42:24,877
Did he find you?

182
00:42:27,922 --> 00:42:28,922
What happened?

183
00:42:29,049 --> 00:42:31,008
l'm fine. Go to sleep

184
00:43:22,644 --> 00:43:25,979
Devshri

185
00:43:28,316 --> 00:43:29,817
How are you?

186
00:43:29,943 --> 00:43:31,026
You OK?

187
00:43:31,569 --> 00:43:32,736
How are you Lakshman?

188
00:43:32,862 --> 00:43:33,821
Did you forget me?

189
00:43:38,993 --> 00:43:40,160
You have time off work?

190
00:43:41,663 --> 00:43:42,496
Who is it?

191
00:43:42,622 --> 00:43:43,872
ls it Trishna?

192
00:43:49,587 --> 00:43:50,838
How are you?

193
00:43:51,965 --> 00:43:52,798
Fine

194
00:44:03,143 --> 00:44:04,768
Don't make it too strong

195
00:44:06,396 --> 00:44:07,688
How is school going?

196
00:44:07,814 --> 00:44:09,857
No l've left school now

197
00:44:09,983 --> 00:44:10,816
Why?

198
00:44:11,317 --> 00:44:13,235
We don't have money for the fees

199
00:44:13,903 --> 00:44:15,821
What happened to the money l sent you?

200
00:44:16,281 --> 00:44:18,449
We owe money for the Jeep

201
00:44:22,954 --> 00:44:24,371
How come you're here?

202
00:44:26,833 --> 00:44:28,250
l've left the hotel job

203
00:44:30,545 --> 00:44:31,420
What happened?

204
00:44:32,172 --> 00:44:33,589
l just had to leave

205
00:44:34,132 --> 00:44:35,299
Why?

206
00:44:37,093 --> 00:44:38,510
Dad l just had to

207
00:44:42,349 --> 00:44:43,766
How are we going to get by?

208
00:44:45,185 --> 00:44:47,811
l can work on Uncle's farm

209
00:44:48,980 --> 00:44:50,064
Uncle?

210
00:44:51,316 --> 00:44:53,359
He can't pay 2500 rupees a week

211
00:44:55,737 --> 00:44:56,737
l know

212
00:45:38,279 --> 00:45:39,113
What's happened?

213
00:45:39,197 --> 00:45:40,030
You OK?

214
00:45:40,532 --> 00:45:41,532
Bring some water

215
00:46:21,740 --> 00:46:22,990
Drink this

216
00:47:04,199 --> 00:47:05,741
She's not had her period

217
00:47:05,909 --> 00:47:07,326
Not had her period?

218
00:47:08,203 --> 00:47:09,578
How many months has it been?

219
00:47:09,746 --> 00:47:11,121
Three

220
00:47:11,247 --> 00:47:12,498
Three months?

221
00:47:13,625 --> 00:47:15,334
Any other problems?

222
00:47:15,585 --> 00:47:17,169
l'm sick in the mornings

223
00:48:09,347 --> 00:48:10,222
Here

224
00:48:25,655 --> 00:48:28,240
You have to go to your Uncle's
and get work in the factory

225
00:48:32,120 --> 00:48:34,079
l don't want to leave home again

226
00:48:36,291 --> 00:48:37,624
YourAunt Gomti is ill

227
00:48:38,543 --> 00:48:40,294
Dad says you have to go and help out

228
00:48:44,382 --> 00:48:46,133
l don't want to go away again

229
00:48:53,016 --> 00:48:55,768
Dad says that you have to

230
00:49:02,525 --> 00:49:03,525
OK

231
00:49:04,277 --> 00:49:05,903
lf that's what you want

232
00:49:44,734 --> 00:49:46,068
Mum

233
00:49:46,277 --> 00:49:47,736
l'm going now

234
00:49:52,575 --> 00:49:53,701
Look after yourself

235
00:49:54,953 --> 00:49:55,786
l'm going Grandma

236
00:49:57,622 --> 00:49:59,081
Bless you

237
00:50:03,378 --> 00:50:04,378
l'm going Dad

238
00:50:24,649 --> 00:50:25,691
What is it?

239
00:50:25,817 --> 00:50:27,609
l'm coming with you

240
00:50:28,278 --> 00:50:29,570
Don't be silly

241
00:50:30,447 --> 00:50:31,989
You can't come with me

242
00:50:33,158 --> 00:50:33,991
Let me go

243
00:51:29,130 --> 00:51:30,839
Where is this address?

244
00:51:32,467 --> 00:51:35,094
lt is just over there

245
00:51:50,068 --> 00:51:51,610
Uncle

246
00:51:51,861 --> 00:51:52,736
Bless you

247
00:51:53,279 --> 00:51:54,697
- How's everyone?
- OK

248
00:51:54,823 --> 00:51:55,656
How is your Dad?

249
00:51:55,782 --> 00:51:56,824
Good

250
00:51:58,118 --> 00:51:59,868
Gomti, Trishna is here

251
00:52:01,913 --> 00:52:03,247
Be happy child

252
00:52:03,873 --> 00:52:04,790
How are you?

253
00:52:05,083 --> 00:52:05,916
l'm not well

254
00:52:06,668 --> 00:52:08,085
lt's been 4 or 5 months

255
00:52:08,211 --> 00:52:09,878
l'm in so much pain

256
00:52:10,463 --> 00:52:13,549
My whole body hurts,
l can't even stand up

257
00:53:43,556 --> 00:53:45,099
YourAunt needs food, you're late

258
00:53:45,266 --> 00:53:47,142
Yes almost done

259
00:54:02,492 --> 00:54:04,451
Trishna, someone's here for you

260
00:54:04,577 --> 00:54:05,411
What?

261
00:54:05,453 --> 00:54:07,204
There's someone to see you

262
00:54:48,913 --> 00:54:52,875
Sir, may l take five minutes?

263
00:54:53,501 --> 00:54:54,626
OK

264
01:00:29,087 --> 01:00:30,921
Let's start with the pizzicato

265
01:00:33,383 --> 01:00:35,259
Can you play the track a little lower?

266
01:00:35,385 --> 01:00:36,468
Lower?

267
01:00:37,137 --> 01:00:38,846
Lower the track in the headphones

268
01:00:59,993 --> 01:01:04,413
l am devoted to you my love

269
01:01:04,539 --> 01:01:08,709
Your beauty makes me love you

270
01:01:08,835 --> 01:01:14,131
l'm devoted to you my love

271
01:01:21,014 --> 01:01:22,181
You mean like this

272
01:01:26,770 --> 01:01:31,231
With you by my side, my love

273
01:01:36,488 --> 01:01:43,702
You turn the night into day

274
01:01:45,205 --> 01:01:50,250
You've captured my heart

275
01:01:50,877 --> 01:01:54,630
You've cast a spell on me

276
01:01:54,756 --> 01:02:00,594
You make me blossom
like a jasmine flower

277
01:02:04,432 --> 01:02:08,644
l want to be with you forever

278
01:02:09,479 --> 01:02:13,565
The world is beautiful when l am with you

279
01:02:21,574 --> 01:02:24,118
l am devoted to you

280
01:02:26,371 --> 01:02:29,832
l want to be with you forever

281
01:03:13,835 --> 01:03:16,920
Your love sets me on fire

282
01:03:17,630 --> 01:03:21,675
When we're together l have no rest

283
01:03:23,261 --> 01:03:25,971
You light up my desire

284
01:05:31,890 --> 01:05:34,516
Hi. Avit

285
01:05:35,310 --> 01:05:36,518
Trishna

286
01:05:37,270 --> 01:05:38,687
You dance really well

287
01:05:39,481 --> 01:05:41,023
l'm a dance co-ordinator

288
01:05:41,149 --> 01:05:44,526
l help organise the classes
and the dancers for shoots

289
01:05:44,652 --> 01:05:46,820
That sort of thing

290
01:05:49,366 --> 01:05:50,783
Not for me thanks

291
01:05:55,288 --> 01:05:56,705
How long have you been here?

292
01:05:57,040 --> 01:05:58,207
ln Bombay?

293
01:06:00,877 --> 01:06:01,752
ls she a friend of yours?

294
01:06:01,795 --> 01:06:03,128
Yes she is

295
01:06:04,005 --> 01:06:04,963
You didn't even introduce me

296
01:06:05,131 --> 01:06:06,590
You didn't give us a chance to

297
01:06:06,716 --> 01:06:08,550
l was just giving her some advice

298
01:12:43,405 --> 01:12:44,405
How much are the bananas?

299
01:12:44,614 --> 01:12:45,447
Thirty rupees

300
01:12:45,490 --> 01:12:46,323
Thirty?

301
01:12:49,327 --> 01:12:54,039
Do you think you will have to pay
a high price for your mistakes?

302
01:13:44,215 --> 01:13:45,382
How are you?

303
01:13:46,009 --> 01:13:47,301
lt's been ages

304
01:13:47,427 --> 01:13:49,136
You haven't called or anything

305
01:13:49,304 --> 01:13:50,721
Sorry

306
01:13:51,306 --> 01:13:52,222
So tell me

307
01:13:52,349 --> 01:13:53,724
What's happened?

308
01:13:53,850 --> 01:13:54,892
Nothing, l've just been at home

309
01:13:55,018 --> 01:13:56,101
Have you heard from Jay?

310
01:13:57,020 --> 01:13:58,312
He hasn't called?

311
01:13:58,438 --> 01:14:00,397
No news at all?

312
01:14:01,816 --> 01:14:04,360
From the ten husbands in this competition

313
01:14:04,652 --> 01:14:07,363
The one who is strongest will survive

314
01:14:14,412 --> 01:14:15,412
Hello?

315
01:14:16,456 --> 01:14:17,414
l'm sorry

316
01:14:18,124 --> 01:14:19,958
l was assuming this flat would be vacant

317
01:14:20,919 --> 01:14:22,503
The lease is up

318
01:14:22,629 --> 01:14:24,838
So you have to vacate the flat

319
01:14:26,591 --> 01:14:27,675
When?

320
01:14:28,677 --> 01:14:29,843
This weekend?

321
01:14:31,846 --> 01:14:33,639
ls it OK if l have a quick look around?

322
01:14:34,391 --> 01:14:35,557
Yes go ahead

323
01:15:07,757 --> 01:15:09,341
Good morning, are you hungry?

324
01:15:09,467 --> 01:15:10,300
Yes

325
01:15:10,385 --> 01:15:12,261
l'll get you some breakfast

326
01:15:12,679 --> 01:15:13,679
- Aggie!
- Yes?

327
01:15:13,805 --> 01:15:14,805
Come on

328
01:15:31,656 --> 01:15:33,240
Can l take your number?

329
01:15:52,052 --> 01:15:53,594
So this is what a shoot looks like

330
01:15:54,387 --> 01:15:56,013
Wow, look at all those lights!

331
01:15:56,139 --> 01:15:59,391
Yes, it's very different to rehearsals

332
01:16:09,986 --> 01:16:12,529
This is playback from 'A' Camera

333
01:16:15,700 --> 01:16:16,950
They're good dancers

334
01:16:17,660 --> 01:16:18,494
You think so?

335
01:16:42,310 --> 01:16:44,103
Thousands have been destroyed!

336
01:16:44,312 --> 01:16:45,729
So many hunted!

337
01:16:46,106 --> 01:16:49,024
All the boys are keen to impress

338
01:16:49,567 --> 01:16:51,777
An endless queue, right and left

339
01:16:51,986 --> 01:16:53,737
They beg for love

340
01:16:53,863 --> 01:16:56,990
Oh there will be tension!

341
01:17:07,961 --> 01:17:10,921
lt's good to come to shoots like that

342
01:17:12,257 --> 01:17:15,134
You can make good contacts

343
01:17:15,260 --> 01:17:17,386
People who can help your career

344
01:17:19,848 --> 01:17:23,726
One thing you need to do
is get a Cine Dancer Card

345
01:17:25,061 --> 01:17:26,687
lt's very important

346
01:17:26,813 --> 01:17:28,605
l can help you get one

347
01:17:29,149 --> 01:17:31,567
- All dancers have them?
- Yes all dancers

348
01:17:31,693 --> 01:17:33,027
lt costs money

349
01:17:33,611 --> 01:17:34,987
30,000 rupees to start with

350
01:17:35,822 --> 01:17:40,993
Then after that once you start getting work
you can pay off the rest in installments

351
01:17:41,119 --> 01:17:43,871
l don't have that kind of money

352
01:17:44,497 --> 01:17:46,874
Take the 30,000 from me

353
01:17:47,751 --> 01:17:50,294
Once you start working
you can pay me off

354
01:17:50,420 --> 01:17:52,338
You'll take it?

355
01:17:53,089 --> 01:17:54,340
Promise?

356
01:22:27,739 --> 01:22:29,865
Just put it down

357
01:27:32,794 --> 01:27:34,420
Shall we go inside?

358
01:28:47,827 --> 01:28:48,911
Trishna?

359
01:28:49,037 --> 01:28:50,079
Yes?

360
01:28:52,332 --> 01:28:54,166
Do you know Jay Singh from before?

361
01:28:55,502 --> 01:28:56,585
Yes

362
01:28:57,379 --> 01:28:58,671
Where do you know him from?

363
01:29:03,301 --> 01:29:05,052
From another hotel

364
01:29:06,888 --> 01:29:09,056
Before this you were in Mumbai right?

365
01:29:13,436 --> 01:29:15,229
Which hotel in Mumbai?

366
01:29:16,940 --> 01:29:19,274
l was staying at a friend's house

367
01:29:21,986 --> 01:29:23,612
l see

368
01:38:16,688 --> 01:38:19,565
This is the truth

369
01:38:19,899 --> 01:38:24,361
The truth love has taught me

370
01:38:24,738 --> 01:38:29,158
My love, you showed me
how the world really is

371
01:38:58,438 --> 01:39:01,940
You showed me a place
where the storm never ends

372
01:39:03,234 --> 01:39:07,321
You showed me a world
where the night never ends

373
01:39:07,572 --> 01:39:12,076
You took me to the realm of love

374
01:39:12,327 --> 01:39:16,455
With you by my side,
l left this world behind

375
01:39:20,085 --> 01:39:23,462
Every moment l was filled with fear

376
01:39:24,881 --> 01:39:27,883
Every step l took made me more afraid

377
01:39:29,052 --> 01:39:33,013
My love, you turned my day into night

378
01:39:33,848 --> 01:39:39,228
My love, why did you do this to me?

379
01:40:17,600 --> 01:40:21,729
You showed me a place
where the storm never ends

380
01:40:22,355 --> 01:40:26,191
You showed me a world
where the night never ends

381
01:40:26,693 --> 01:40:30,404
You took me to the realm of love

382
01:40:31,448 --> 01:40:36,160
With you by my side,
l left this world behind

383
01:40:39,330 --> 01:40:42,708
Every moment l was filled with fear

384
01:40:44,127 --> 01:40:47,713
Every step l took made me more afraid

385
01:40:48,173 --> 01:40:52,760
My love, you turned my day into night

386
01:40:52,969 --> 01:40:57,556
My love, why did you do this to me?

387
01:41:00,602 --> 01:41:02,811
Wow, you've got so big!

388
01:41:05,315 --> 01:41:06,482
And a new school uniform

389
01:41:06,608 --> 01:41:10,235
Lakshman, how are you?
Good?

390
01:41:11,571 --> 01:41:14,656
Look Trishna,
there's a new fridge and a new TV

391
01:41:23,708 --> 01:41:25,000
How are you Mum?

392
01:41:29,005 --> 01:41:30,673
Pratiksha?

393
01:41:31,883 --> 01:41:33,092
Hey sis it's you

394
01:41:35,387 --> 01:41:36,595
God you've got tall!

395
01:41:38,098 --> 01:41:40,099
Step down, let me see you properly

396
01:41:40,809 --> 01:41:41,892
New school uniform?

397
01:41:42,060 --> 01:41:43,352
l'm back at school

398
01:41:43,561 --> 01:41:45,312
Good. Really good

399
01:42:01,746 --> 01:42:02,871
Dad

400
01:42:05,125 --> 01:42:06,834
How are you?

401
01:42:07,377 --> 01:42:08,877
Where did you go?

402
01:42:09,504 --> 01:42:11,296
Mumbai

403
01:42:13,925 --> 01:42:15,134
Pratiksha

404
01:42:15,260 --> 01:42:16,885
Get me some water

405
01:42:26,104 --> 01:42:27,062
When did she get here?

406
01:42:27,188 --> 01:42:28,230
Just now

407
01:42:30,900 --> 01:42:32,234
This is for you

408
01:42:33,611 --> 01:42:35,029
You can wear this to a wedding

409
01:42:35,155 --> 01:42:36,572
Maybe even your wedding

410
01:42:36,740 --> 01:42:37,573
Nice

411
01:42:38,783 --> 01:42:41,118
This is for you. Try it on

412
01:42:43,038 --> 01:42:46,206
Oh wow! You look like Shahrukh Khan
and Rani Mukherjee!

413
01:42:49,169 --> 01:42:50,836
Come on let's eat

414
01:42:57,177 --> 01:43:00,095
Lakshman stop fighting
and eat your food!

415
01:43:02,098 --> 01:43:03,807
So where's your husband?

416
01:43:05,560 --> 01:43:07,019
Who?

417
01:43:11,900 --> 01:43:14,902
Everybody knows that l'm living off you

418
01:43:18,656 --> 01:43:20,532
You should be happy

419
01:43:21,701 --> 01:43:23,327
You have a newjeep

420
01:43:27,707 --> 01:43:29,625
Anyone want more?

421
01:43:42,097 --> 01:43:54,024
lt's my fantasy - come and see

422
01:44:13,920 --> 01:44:16,630
Still doing homework?
Haven't you finished yet?

423
01:44:16,756 --> 01:44:20,050
Have you brushed your teeth? Dirty girl

424
01:44:22,345 --> 01:44:24,221
Then pack your schoolbags

425
01:44:38,194 --> 01:44:40,237
Do all your homework

426
01:44:41,364 --> 01:44:43,741
No TV tonight

427
01:44:53,668 --> 01:44:55,502
Study well Lakshman

428
01:44:56,671 --> 01:44:58,797
You too Devshri

429
01:45:00,383 --> 01:45:01,800
Off you go

430
01:45:15,482 --> 01:45:16,440
Attention

431
01:45:17,108 --> 01:45:18,108
Stand at ease

432
01:45:19,861 --> 01:45:21,403
Get ready for the prayer

433
01:45:23,323 --> 01:45:24,656
Our Father

434
01:45:25,825 --> 01:45:26,784
ln heaven

435
01:45:28,661 --> 01:45:29,787
Give us today

436
01:45:30,789 --> 01:45:32,122
Our daily bread

437
01:45:33,166 --> 01:45:34,583
And forgive us

438
01:45:35,418 --> 01:45:36,502
Our sins

439
01:45:37,962 --> 01:45:38,962
As we

440
01:45:40,465 --> 01:45:41,632
Forgive those

441
01:45:42,884 --> 01:45:44,426
Who sin

442
01:45:45,095 --> 01:45:46,762
Against us

443
01:45:47,430 --> 01:45:48,681
Do not bring us

444
01:45:49,766 --> 01:45:50,766
To the test

445
01:45:51,893 --> 01:45:52,893
But deliver us

446
01:45:54,145 --> 01:45:55,229
From evil

447
01:46:01,444 --> 01:46:02,611
Get ready for the pledge

448
01:46:04,614 --> 01:46:05,989
lndia is my country

449
01:46:07,742 --> 01:46:08,742
All lndians

450
01:46:10,203 --> 01:46:11,203
Are my

451
01:46:12,622 --> 01:46:14,206
Brothers and sisters

452
01:46:15,834 --> 01:46:17,584
l shall give my parents

453
01:46:19,504 --> 01:46:20,504
Teachers

454
01:46:20,964 --> 01:46:22,423
And all elders

455
01:46:23,883 --> 01:46:24,800
Respect

456
01:46:25,719 --> 01:46:26,760
And treat everyone

457
01:46:28,221 --> 01:46:29,263
With courtesy

458
01:46:30,765 --> 01:46:31,974
To my country

459
01:46:33,351 --> 01:46:34,601
And my people

460
01:46:35,770 --> 01:46:37,479
l pledge my devotion

461
01:46:39,566 --> 01:46:40,607
ln their well-being

462
01:46:41,860 --> 01:46:43,235
And prosperity

463
01:46:44,738 --> 01:46:45,654
Alone lies

464
01:46:47,032 --> 01:46:48,115
My happiness

