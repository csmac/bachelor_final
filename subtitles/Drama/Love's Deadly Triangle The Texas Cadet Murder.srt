1
00:02:08,914 --> 00:02:13,026
'North Sea, Texas'

2
00:03:05,400 --> 00:03:07,436
It is...

3
00:03:09,600 --> 00:03:14,469
...good.

4
00:03:15,880 --> 00:03:17,880
Agreed.

5
00:03:21,920 --> 00:03:28,268
A, b, c, d, e, f, g,

6
00:03:28,480 --> 00:03:35,397
h, i, j, k, l, m, n, o...

7
00:03:36,560 --> 00:03:38,994
Open eyes.

8
00:03:50,200 --> 00:03:54,900
Put it on the chair, Marie.
And help me undress.

9
00:04:31,320 --> 00:04:33,595
Lily of the valley.

10
00:04:42,240 --> 00:04:45,240
Thanks, Marie, you can go now.

11
00:05:56,040 --> 00:05:59,040
What are you doing in my room?

12
00:05:59,440 --> 00:06:01,440
Hey, Pim...

13
00:06:02,880 --> 00:06:06,480
Come down, you fool.
Mom's not angry.

14
00:06:42,520 --> 00:06:46,620
Look what I found, children.
A little boy.

15
00:06:56,760 --> 00:06:58,760
Hush, it's over now.

16
00:06:59,120 --> 00:07:01,953
Get Pim a glass of water.

17
00:07:04,970 --> 00:07:06,970
It's ok.

18
00:07:13,720 --> 00:07:15,720
Here you go, Mister.

19
00:07:16,440 --> 00:07:18,440
You smell good.

20
00:07:19,400 --> 00:07:24,713
You smell really nice.
- It's Yvette�s perfume.

21
00:07:45,160 --> 00:07:47,879
It's time to go home, now.

22
00:07:48,080 --> 00:07:50,060
But the puzzle's not finished yet.

23
00:07:50,160 --> 00:07:55,180
I know your mother doesn't worry,
but I don't want her...

24
00:07:55,280 --> 00:07:57,280
Just a bit longer.

25
00:07:57,920 --> 00:08:00,036
You better go now.

26
00:08:04,360 --> 00:08:06,360
Bye, Pim.

27
00:08:09,760 --> 00:08:11,760
Here.

28
00:08:30,800 --> 00:08:32,800
Hi, Mirza.

29
00:09:42,160 --> 00:09:44,660
Here. That's pretty good.

30
00:11:00,600 --> 00:11:06,675
'My left hand is the maid'

31
00:12:40,640 --> 00:12:42,260
You'll see when I turn eighteen.

32
00:12:42,360 --> 00:12:47,060
It'll be a shiny motorbike
instead of a bicycle.

33
00:12:47,960 --> 00:12:49,960
Eighteen.

34
00:12:57,280 --> 00:13:01,910
Look. A Suzuki 380 GT.

35
00:13:03,000 --> 00:13:06,436
You can ride on it.
- I?

36
00:13:17,680 --> 00:13:19,680
It's hot.

37
00:13:25,000 --> 00:13:28,754
Pim, just imagine. 380cc.

38
00:13:38,240 --> 00:13:39,860
When you turn fifteen,

39
00:13:39,960 --> 00:13:43,860
you can have my bike,
for your birthday.

40
00:13:44,240 --> 00:13:46,240
Thanks, Gino.

41
00:13:46,640 --> 00:13:48,640
Wow.

42
00:14:52,960 --> 00:14:56,191
Pimmy. You bastard.

43
00:14:58,560 --> 00:15:00,420
The door.

44
00:15:00,520 --> 00:15:02,520
The bell's ringing.

45
00:15:14,520 --> 00:15:18,229
For you. From me.
A boy needs a knife.

46
00:15:18,880 --> 00:15:20,880
�tienne.

47
00:15:21,000 --> 00:15:23,140
Wasn't yesterday your birthday?

48
00:15:23,240 --> 00:15:25,540
It's tomorrow. Fifteen.

49
00:15:27,720 --> 00:15:30,720
Is that you, �tienne?
- Yvette.

50
00:15:31,200 --> 00:15:35,398
Here. It's yours.
A boy needs a knife.

51
00:15:36,400 --> 00:15:38,400
What do you say?

52
00:15:38,520 --> 00:15:40,620
'Thank you, �tienne.'

53
00:15:47,520 --> 00:15:49,520
Good dog.

54
00:16:53,840 --> 00:16:56,340
Have some sardines later.

55
00:16:56,360 --> 00:16:58,590
I will.
- Yes, thanks.

56
00:17:09,960 --> 00:17:12,860
Behave yourself. Lock the doors.

57
00:17:12,960 --> 00:17:14,820
You're a big boy.

58
00:17:14,920 --> 00:17:17,220
We'll be back tomorrow.

59
00:17:35,160 --> 00:17:37,160
So do you like it?

60
00:17:38,520 --> 00:17:40,060
Your knife?

61
00:17:40,160 --> 00:17:42,360
Thanks a lot, �tienne.

62
00:19:05,200 --> 00:19:07,020
It's me.

63
00:19:07,120 --> 00:19:09,120
Hello, 'me'.

64
00:19:11,520 --> 00:19:14,320
You want some lemonade, Pim?

65
00:19:15,200 --> 00:19:16,620
The latest news.

66
00:19:16,720 --> 00:19:21,020
The latest news of the
day before yesterday.

67
00:19:21,680 --> 00:19:24,620
Your boss is taking
my mother to Lille,

68
00:19:24,720 --> 00:19:28,780
to the Accordion Festival.
They're staying over night.

69
00:19:28,880 --> 00:19:31,460
When you tempt the devil...

70
00:19:31,560 --> 00:19:35,460
My mother doesn't like that guy.
She's only interested in his car.

71
00:19:35,560 --> 00:19:37,740
Silence is golden, Pim.

72
00:19:37,840 --> 00:19:40,340
That's why we're so rich.

73
00:19:48,960 --> 00:19:51,360
Mom, can Pim sleep over?

74
00:19:52,240 --> 00:19:54,240
Can he?

75
00:19:54,720 --> 00:20:00,260
Of course. It's not good to be on your 
own all night in an empty house.

76
00:20:00,360 --> 00:20:02,620
You can sleep in my bed.

77
00:20:02,720 --> 00:20:09,120
It's summer. We'll sleep out in
the dunes. In a tent, Pim and me.

78
00:20:37,640 --> 00:20:39,840
A gift from my mother.

79
00:20:44,520 --> 00:20:46,520
From my boss.

80
00:20:48,280 --> 00:20:50,280
Not bad.

81
00:20:53,560 --> 00:20:56,060
Now we both have a knife.

82
00:21:05,400 --> 00:21:08,000
It's my birthday tomorrow.

83
00:21:10,080 --> 00:21:12,080
That's why.

84
00:22:28,800 --> 00:22:34,900
No one needs to know about this,
Pimmy. It's just between us.

85
00:24:26,160 --> 00:24:28,560
Hip Hip Hurray.
- Cheers.

86
00:24:28,760 --> 00:24:31,060
Hip Hip Hurray.
- Cheer.

87
00:24:33,680 --> 00:24:35,680
For your birthday.

88
00:24:39,960 --> 00:24:44,556
Pencils. Thanks, Sabrina.
I don't have a 4B.

89
00:24:47,440 --> 00:24:49,440
Coffee?

90
00:24:58,760 --> 00:25:00,760
Pim?

91
00:25:00,880 --> 00:25:02,380
Yes.

92
00:25:02,480 --> 00:25:04,480
Dreamer.

93
00:25:07,960 --> 00:25:11,360
Did you sleep alright in the tent?

94
00:25:13,360 --> 00:25:16,060
We should do it more often.

95
00:25:28,680 --> 00:25:30,900
Mom made pancakes, can you believe it?

96
00:25:31,000 --> 00:25:33,300
Of course, like always.

97
00:25:37,800 --> 00:25:40,900
Can Pim come to Aunt Ginette's?

98
00:25:41,640 --> 00:25:43,640
No, that's impossible.

99
00:26:30,480 --> 00:26:34,660
Right. They�re a different
species, those French.

100
00:26:34,760 --> 00:26:36,960
I'm going to lie down.

101
00:26:41,520 --> 00:26:43,954
No kiss from you, then?

102
00:26:46,400 --> 00:26:49,800
You're making pancakes? I could...

103
00:27:05,760 --> 00:27:09,639
When I learn the steps, I'll show you.

104
00:27:11,480 --> 00:27:13,480
Pim?
- Yes?

105
00:27:14,600 --> 00:27:16,600
Dreamer.

106
00:27:17,640 --> 00:27:20,940
I use a glue stick. It's cleaner.

107
00:27:28,200 --> 00:27:30,200
The bike.

108
00:28:28,560 --> 00:28:33,260
Beauty and the Beast.
- Watch what you say, man.

109
00:28:36,000 --> 00:28:38,200
What are you thinking?

110
00:28:39,480 --> 00:28:41,755
You forgot to bring the tent.

111
00:28:59,120 --> 00:29:01,120
Well...
- Wait.

112
00:29:05,840 --> 00:29:08,479
Here. Happy birthday.

113
00:29:09,280 --> 00:29:11,475
Thanks.

114
00:30:30,640 --> 00:30:33,359
It's me.
- Hi, 'me'.

115
00:30:34,480 --> 00:30:37,260
You want some lemonade, Pim?
- We gotta go, Pim and me.

116
00:30:37,360 --> 00:30:40,360
Not on the Motorway.
- Ok, Mom.

117
00:30:53,520 --> 00:30:55,556
Let�s go.
- Already?

118
00:30:56,440 --> 00:30:59,000
I gotta go.
- Where to?

119
00:31:00,160 --> 00:31:02,160
La Panne.

120
00:31:02,600 --> 00:31:04,875
Can I come?
- No.

121
00:31:06,600 --> 00:31:09,512
Why not?
- You just can�t.

122
00:31:10,560 --> 00:31:12,180
Not together?

123
00:31:12,280 --> 00:31:15,100
We'll go together next time.
- Promise?

124
00:31:15,200 --> 00:31:17,200
I promise. Come on.

125
00:31:30,280 --> 00:31:32,780
Not a word to anyone, ok?

126
00:31:51,640 --> 00:31:56,634
Back already?
- Yeah. Gino's bike broke.

127
00:31:57,880 --> 00:32:01,180
�tienne, couldn't you...
- Of course, what's the problem?

128
00:32:01,280 --> 00:32:04,750
Oh, well, it's the carburettor.

129
00:32:05,560 --> 00:32:11,060
The carburettor? We'll just
change it. I'll take a look.

130
00:32:11,320 --> 00:32:14,060
Is Gino at home?
- Yes,... well,... no.

131
00:32:14,160 --> 00:32:16,260
It's not necessary. Don't go.

132
00:32:16,360 --> 00:32:20,353
It's no problem...
- It's not necessary!

133
00:32:20,840 --> 00:32:23,180
Gino can do it himself.
- Pim.

134
00:32:23,280 --> 00:32:25,700
What have you got against �tienne?

135
00:32:25,800 --> 00:32:28,500
A little politeness please.

136
00:32:30,640 --> 00:32:37,140
Well, I'll pick you up on Saturday
for the Accordion Final. Yvette.

137
00:33:33,440 --> 00:33:36,140
We're going to Texas after.

138
00:33:37,640 --> 00:33:40,791
We'll be back around eleven.

139
00:33:43,360 --> 00:33:45,360
Ta-ta.

140
00:34:02,960 --> 00:34:04,960
Ta-ta.

141
00:34:59,520 --> 00:35:05,470
A, b, c, d...

142
00:35:22,000 --> 00:35:25,390
...e, f, g...

143
00:35:27,040 --> 00:35:29,040
Gino.

144
00:36:55,440 --> 00:36:57,440
It's me.

145
00:36:58,600 --> 00:37:00,600
Hello, my boy.

146
00:37:00,720 --> 00:37:03,520
I�m just mending my dress...

147
00:37:05,360 --> 00:37:07,794
Gino went to Dunkerque.

148
00:37:10,080 --> 00:37:14,340
Why doesn�t he find a
girlfriend who lives closer?

149
00:37:14,440 --> 00:37:18,840
The missus wanted to
go dancing. In La Panne.

150
00:37:19,800 --> 00:37:22,000
Her name is Fran�oise.

151
00:37:22,520 --> 00:37:24,860
You want some lemonade?
It's orange.

152
00:37:24,960 --> 00:37:26,960
Gino and dancing.

153
00:37:29,040 --> 00:37:31,040
Children grow up.

154
00:37:31,040 --> 00:37:35,240
Shell I open the white?
The orange is flat.

155
00:37:37,120 --> 00:37:41,120
He might bring her back later.
Fran�oise.

156
00:37:41,400 --> 00:37:45,313
Fran�oise. She wears white sandals.

157
00:37:48,280 --> 00:37:50,060
On a bike?

158
00:37:50,160 --> 00:37:53,516
Don't tease the boy, Sabrina.

159
00:37:53,720 --> 00:37:57,780
Such a bike is expensive, 
even second hand .

160
00:37:57,880 --> 00:38:02,271
And the salary of an
apprentice is small.

161
00:38:02,480 --> 00:38:07,980
All Mom does is work.
But it's always been Gino's dream.

162
00:38:09,440 --> 00:38:11,580
And you can save money
on so many things.

163
00:38:11,680 --> 00:38:14,513
Sugar, for instance.
- Mom.

164
00:38:16,480 --> 00:38:18,835
Just between us, Pim...

165
00:38:19,600 --> 00:38:21,600
The sugar,

166
00:38:22,360 --> 00:38:25,420
I collect them off the trays in the cafe.

167
00:38:25,520 --> 00:38:27,500
The doctor doesn�t allow me sugar, but...

168
00:38:27,600 --> 00:38:29,600
Mom.

169
00:38:29,640 --> 00:38:35,180
they throw them away, those packets,
even when they haven't been opened.

170
00:38:35,280 --> 00:38:38,080
Do you want a sandwich, Pim?

171
00:38:38,160 --> 00:38:42,260
And yesterday, for
the first time, Gino...

172
00:38:44,680 --> 00:38:47,140
...worked at the Burger Bar.

173
00:38:47,240 --> 00:38:50,300
Friday nights and all day Sunday.

174
00:38:50,400 --> 00:38:52,060
It pays rather well.

175
00:38:52,160 --> 00:38:54,460
And �tienne says he's
doing well at the garage.

176
00:38:54,560 --> 00:38:56,300
He learns quickly, our Gino.

177
00:38:56,400 --> 00:39:02,500
And if there's a problem with
the bike, he can fix it himself.

178
00:39:04,160 --> 00:39:07,160
He has golden hands, that boy.

179
00:39:10,240 --> 00:39:12,540
Gino will be back late.

180
00:39:13,400 --> 00:39:19,500
What do you expect? He's eighteen.
And that girl loves to dance.

181
00:39:19,600 --> 00:39:21,180
Well, the paperboy has to go.

182
00:39:21,280 --> 00:39:24,460
I'm going to bed.
I've been tired lately.

183
00:39:24,560 --> 00:39:26,700
You don't want anything to drink?

184
00:39:26,800 --> 00:39:30,200
You're going to Caf� Texas?
- Yeah.

185
00:40:14,880 --> 00:40:16,820
When the cat's away...

186
00:40:16,920 --> 00:40:19,620
Your mother's not here yet.

187
00:40:21,760 --> 00:40:26,260
Did you give her the papers?
Is Marcella still as crazy?

188
00:40:26,360 --> 00:40:29,420
Where's your mother playing tonight? 
Is it far?

189
00:40:29,520 --> 00:40:30,860
La Panne.

190
00:40:30,960 --> 00:40:34,460
From now on he can bring
me the old papers.

191
00:40:34,560 --> 00:40:36,420
He's a good boy.

192
00:40:36,520 --> 00:40:38,520
Something to drink?

193
00:40:38,600 --> 00:40:41,200
Well? Still no girlfriend?

194
00:40:41,800 --> 00:40:42,940
Grenadine.

195
00:40:43,040 --> 00:40:47,940
Oh, her name is Grenadine?
Certainly not from around here.

196
00:40:48,040 --> 00:40:52,020
No no no, he doesn't
know yet. Right, my boy?

197
00:40:52,120 --> 00:40:54,620
So, you don't look at girls?

198
00:40:54,720 --> 00:40:59,920
Tell us. Which one of us do you like?
Go on, tell us.

199
00:41:12,000 --> 00:41:18,348
Go on, read the magazine.
Or do some drawing, little boy.

200
00:41:46,600 --> 00:41:49,900
Ta-da.
- Better late than never...

201
00:41:50,600 --> 00:41:53,239
Everything ok, my boy?

202
00:41:56,920 --> 00:41:59,020
He can draw you later.
- Sure.

203
00:41:59,120 --> 00:42:01,380
Come on, Pim. Who haven't you drawn?

204
00:42:01,480 --> 00:42:03,940
Me, he hasn't drawn me, yet.

205
00:42:04,040 --> 00:42:07,640
It's too hard. You're too beautiful.

206
00:42:08,680 --> 00:42:11,460
Was it crowded in La Panne?
- Oh la la.

207
00:42:11,560 --> 00:42:13,560
Hey. Stop it.

208
00:42:29,360 --> 00:42:33,239
Here. A beer for a big boy.

209
00:42:34,200 --> 00:42:38,398
Your mother agrees.
It... She...

210
00:42:40,360 --> 00:42:42,360
Forget it.

211
00:42:45,600 --> 00:42:47,600
Gino.

212
00:44:24,080 --> 00:44:26,080
I'm gonna tell Mom.

213
00:44:38,040 --> 00:44:40,040
Keep your mouth shut, ok?

214
00:46:44,080 --> 00:46:49,438
This doesn�t work.
It takes two. Come on.

215
00:46:49,640 --> 00:46:51,640
Don't be shy.

216
00:46:52,800 --> 00:46:54,800
It's... wait.

217
00:47:24,680 --> 00:47:26,680
What's the matter?

218
00:47:27,960 --> 00:47:29,960
I'm not a dancer.

219
00:47:33,040 --> 00:47:35,440
Do you think I'm pretty?

220
00:47:35,840 --> 00:47:37,840
Yes.

221
00:47:38,560 --> 00:47:40,560
Then hold me.

222
00:47:41,360 --> 00:47:43,430
Sabrina, I...

223
00:47:48,880 --> 00:47:50,880
Strange boy.

224
00:47:54,280 --> 00:47:56,280
Thank you, Pim.

225
00:47:59,760 --> 00:48:03,820
Tell me, what's her name,
that little French girl?

226
00:48:03,920 --> 00:48:05,920
Gino's girlfriend.

227
00:48:07,960 --> 00:48:10,030
I don't know.

228
00:48:10,240 --> 00:48:13,500
Maybe he'll bring her to the fair.

229
00:48:13,600 --> 00:48:16,500
�tienne says that she's cute.

230
00:48:19,600 --> 00:48:24,500
Tell me. Are you going
to spend all night drawing?

231
00:48:24,960 --> 00:48:28,420
Normal boys your age go
out with their friends.

232
00:48:28,520 --> 00:48:31,990
Or with a girl.
Oh la la.

233
00:48:33,560 --> 00:48:37,360
Normal women your age
no longer go out.

234
00:48:54,120 --> 00:48:56,120
'Room for rent'

235
00:49:04,040 --> 00:49:06,040
Sabrina.

236
00:49:11,840 --> 00:49:18,020
Yes, water is included in the
price. Electricity and heating, too.

237
00:49:18,120 --> 00:49:20,120
The bell.

238
00:49:20,320 --> 00:49:21,940
The doorbell rang.

239
00:49:22,040 --> 00:49:24,820
I didn't...
- I'm sure it rang.

240
00:49:24,920 --> 00:49:26,920
Wait here.

241
00:50:16,760 --> 00:50:18,760
Nobody there.

242
00:50:18,800 --> 00:50:21,900
Here, have one.
- I don't smoke.

243
00:50:23,400 --> 00:50:25,400
Very wise.

244
00:50:26,440 --> 00:50:29,340
Have you heard from the gypsy
who rented the room before?

245
00:50:29,440 --> 00:50:32,876
Zoltan? No.

246
00:50:33,080 --> 00:50:37,100
He disappeared one day.
Flew off, like a bird.

247
00:50:37,200 --> 00:50:39,200
With the Funfair.

248
00:50:41,600 --> 00:50:44,831
That's happiness, my child.

249
00:50:45,040 --> 00:50:48,340
North in summer, south in winter.

250
00:50:49,280 --> 00:50:55,958
Venice, Paris, Le Pir�e, Lima, Capri...

251
00:50:56,560 --> 00:50:58,900
Foreign countries and foreigners.

252
00:50:59,000 --> 00:51:02,600
Do whatever you want, no worries,...

253
00:51:05,680 --> 00:51:07,680
...no children.

254
00:51:10,880 --> 00:51:12,880
The room is fine.

255
00:51:21,160 --> 00:51:26,154
Where's Pim?
- At a party. That's all he said.

256
00:51:27,400 --> 00:51:29,900
Is he quiet at home, too?

257
00:51:30,400 --> 00:51:33,631
You think he's quiet?
- Yes.

258
00:51:34,480 --> 00:51:38,140
I don't think so.
I often hear his voice.

259
00:51:38,240 --> 00:51:41,440
That boy lives in another world.

260
00:51:43,080 --> 00:51:45,780
I try to read his thoughts.

261
00:51:48,480 --> 00:51:51,552
Draw, draw, that he can do.

262
00:51:51,760 --> 00:51:55,360
But I almost never see his drawings.

263
00:51:57,560 --> 00:52:01,260
Sometimes I worry, Sabrina, you know?

264
00:52:01,600 --> 00:52:03,600
I only have one son.

265
00:52:05,240 --> 00:52:07,240
Pim is a dreamer.

266
00:52:11,040 --> 00:52:13,340
Tell me... What's her name?

267
00:52:13,440 --> 00:52:17,640
Fran�oise. But it's
not certain that she...

268
00:52:18,000 --> 00:52:20,000
Well...

269
00:52:20,760 --> 00:52:23,340
Tell her you liked the room.

270
00:52:23,440 --> 00:52:28,340
Why didn't she come to
see it herself? Is she shy?

271
00:52:31,800 --> 00:52:34,140
What kind of work is
she looking for here?

272
00:52:34,240 --> 00:52:38,060
There's no more work here
than in Northern France.

273
00:52:38,160 --> 00:52:43,314
Oh yes, no visitors in the
room. Tell your brother.

274
00:52:44,040 --> 00:52:46,508
Does she speak Dutch?

275
00:52:48,920 --> 00:52:51,957
You don't even know that?

276
00:53:41,120 --> 00:53:44,320
Plenty of rest, the doctor says.

277
00:53:44,800 --> 00:53:46,800
Get Pim a drink.

278
00:54:01,400 --> 00:54:05,820
Fortunately I still have my
daughter, now that Gino has left.

279
00:54:05,920 --> 00:54:09,220
The house has become so very big.

280
00:54:09,520 --> 00:54:11,220
He moved in with her.

281
00:54:11,320 --> 00:54:14,700
They both work at a
restaurant in Dunkerque.

282
00:54:14,800 --> 00:54:17,780
Washing dishes? Mopping the floor?
I don't know.

283
00:54:17,880 --> 00:54:24,480
He didn't even come to the hospital.
- Gino doesn't like hospitals.

284
00:54:33,840 --> 00:54:36,638
Well, I'm off.

285
00:55:34,640 --> 00:55:36,640
Pim.

286
00:55:37,040 --> 00:55:41,840
That �tienne. What was he
thinking, bloody idiot?

287
00:55:45,120 --> 00:55:49,220
At the hotel in Lille,
we fooled around a bit.

288
00:55:49,320 --> 00:55:51,820
It was just a little fun.

289
00:55:52,320 --> 00:55:55,820
But I said it mustn't happen again.

290
00:55:57,480 --> 00:56:00,660
When we came back from Dunkerque,

291
00:56:00,760 --> 00:56:04,260
he parked his car in a picnic area.

292
00:56:04,800 --> 00:56:08,200
He couldn't keep his hands off me.

293
00:56:09,920 --> 00:56:11,956
I bit him.

294
00:56:18,120 --> 00:56:21,820
Come inside, Mom.
You'll catch a cold.

295
00:56:23,440 --> 00:56:25,440
Yes.

296
00:56:43,840 --> 00:56:45,840
Mirza. Mirza.

297
00:57:04,240 --> 00:57:06,240
Zoltan.

298
00:57:07,960 --> 00:57:09,420
You're here.

299
00:57:09,520 --> 00:57:11,520
Hello, Yvette.

300
00:57:12,760 --> 00:57:14,760
I'm back.

301
00:57:15,600 --> 00:57:18,300
Pim will show you the room.

302
00:57:31,360 --> 00:57:35,760
You can hang your clothes
here, on the hooks.

303
00:57:46,360 --> 00:57:48,360
Grown up now.

304
00:57:48,640 --> 00:57:51,340
That's normal, I'm sixteen.

305
00:58:03,520 --> 00:58:06,100
Did you give Zoltan a clean towel?

306
00:58:06,200 --> 00:58:08,589
A towel, yes.

307
00:58:10,040 --> 00:58:13,340
The extra money will come in handy.

308
00:58:13,440 --> 00:58:16,140
He's rented the room before.

309
00:58:16,240 --> 00:58:20,074
He was only a boy then.
He's a man now.

310
00:58:21,680 --> 00:58:25,620
Finally we can take
down the damn posters.

311
00:58:25,720 --> 00:58:30,020
I can't believe Fran�oise
wanted to rent it.

312
00:58:49,120 --> 00:58:51,620
You still drink red wine?

313
00:59:05,520 --> 00:59:07,909
Zoltan.
- Pim.

314
00:59:09,200 --> 00:59:11,200
Pimmy.

315
00:59:12,280 --> 00:59:14,280
I remember.

316
00:59:15,320 --> 00:59:17,320
Pimmy.

317
00:59:43,560 --> 00:59:48,160
Shall I play a tune for
you gentlemen? A waltz.

318
01:00:44,600 --> 01:00:46,600
Mirza.

319
01:00:58,200 --> 01:01:00,460
Where were you all night?

320
01:01:00,560 --> 01:01:04,394
Well, my friend. You little rascal.

321
01:01:06,040 --> 01:01:10,740
Make yourself a sandwich.
There's some jam left.

322
01:02:07,240 --> 01:02:09,240
I'll clean up.

323
01:02:14,360 --> 01:02:17,060
It's Friday. I'm going out.

324
01:02:27,680 --> 01:02:29,680
Hush.

325
01:02:30,520 --> 01:02:32,520
He'll be back.

326
01:03:19,200 --> 01:03:23,000
Hi, Pim. Is your mother working today?

327
01:03:25,200 --> 01:03:26,220
She is working.

328
01:03:26,320 --> 01:03:28,860
What do you want to
drink? Some grenadine?

329
01:03:28,960 --> 01:03:30,940
He's probably a Romanian.

330
01:03:31,040 --> 01:03:33,220
No, he's a Bulgarian
in search of happiness.

331
01:03:33,320 --> 01:03:35,320
Grenadine.

332
01:03:35,600 --> 01:03:37,780
Why doesn't he sleep in the caravan,

333
01:03:37,880 --> 01:03:39,540
at the bumper cars?

334
01:03:39,640 --> 01:03:41,980
Oh, just ignore him, Pim?

335
01:03:42,080 --> 01:03:44,799
Here. It's on the house.

336
01:03:45,680 --> 01:03:49,300
And when the fair ends,
is he going to stay?

337
01:03:49,400 --> 01:03:52,420
Better a big car than a big guy, Julien.

338
01:03:52,520 --> 01:03:56,798
Life's short too short.
Just shut up, now.

339
01:04:14,960 --> 01:04:17,190
Tonight, spaghetti.

340
01:04:17,400 --> 01:04:20,100
Then you know more than me.

341
01:05:01,240 --> 01:05:03,834
Pimmy? A towel, please.

342
01:05:04,320 --> 01:05:06,320
Ok.

343
01:05:19,280 --> 01:05:21,700
There's vanilla ice cream for dessert.

344
01:05:21,800 --> 01:05:25,270
Dessert?
- Yes, dessert.

345
01:05:41,400 --> 01:05:45,000
The Cologne Fair, it's worth seeing.

346
01:05:45,160 --> 01:05:47,660
Pimmy, you wouldn't believe your eyes.

347
01:05:47,760 --> 01:05:51,460
It's very large, there's always work.

348
01:05:52,280 --> 01:05:56,780
We'll play this tomorrow.
You are my audience.

349
01:06:22,200 --> 01:06:24,200
Yeah?

350
01:06:26,400 --> 01:06:28,400
Gino.

351
01:06:30,600 --> 01:06:32,600
Yeah.

352
01:06:33,480 --> 01:06:35,480
Ok, what is it...

353
01:06:46,080 --> 01:06:48,080
Sabrina.

354
01:06:48,840 --> 01:06:51,540
Sabrina has a crush on you.

355
01:06:52,280 --> 01:06:55,955
I can see it. She's crazy about you.

356
01:06:57,680 --> 01:07:00,280
But you already know that.

357
01:07:06,880 --> 01:07:10,220
You're going to be seventeen, Pimmy.

358
01:07:10,320 --> 01:07:12,320
Seventeen.

359
01:07:15,040 --> 01:07:17,156
You gotta...

360
01:07:18,160 --> 01:07:20,160
We both gotta...

361
01:07:25,280 --> 01:07:28,880
Sometimes you have to make a choice.

362
01:07:30,280 --> 01:07:34,280
I was going to tell you about Fran�oise.

363
01:07:41,720 --> 01:07:43,720
Pimmy, wait.

364
01:07:47,480 --> 01:07:49,880
We can still be friends.

365
01:07:53,080 --> 01:07:56,277
We were children. Kids.

366
01:07:57,480 --> 01:07:59,480
It was just play.

367
01:08:02,240 --> 01:08:06,940
Imagine, you and Sabrina.
It would be fantastic.

368
01:08:07,160 --> 01:08:09,980
I'm leaving. There is a fair in Cologne,

369
01:08:10,080 --> 01:08:12,940
Zoltan will take me there with him.

370
01:08:13,040 --> 01:08:15,040
The gypsy?

371
01:08:17,080 --> 01:08:22,020
You're beautiful when you're jealous.
- Who's jealous?

372
01:08:22,120 --> 01:08:25,420
You think I was in love with you?

373
01:08:31,880 --> 01:08:33,880
Pim.

374
01:08:56,920 --> 01:08:58,920
Be quiet.

375
01:09:57,240 --> 01:09:59,440
Bring me a drink, Pim.

376
01:10:04,600 --> 01:10:06,750
No no no, wine.

377
01:10:07,840 --> 01:10:09,840
Wine.

378
01:10:16,320 --> 01:10:18,520
Zoltan, you look good.

379
01:10:19,120 --> 01:10:21,120
Yes, I'm going out.

380
01:10:22,200 --> 01:10:24,316
Some wine, Zoltan?

381
01:10:25,320 --> 01:10:27,320
Thank you, Pimmy.

382
01:10:35,400 --> 01:10:37,400
Thank you.

383
01:10:51,040 --> 01:10:54,300
Pimmy, see you later.
- See you later.

384
01:10:54,400 --> 01:10:56,400
Bye.

385
01:10:57,760 --> 01:10:59,760
What a fool.

386
01:13:22,440 --> 01:13:24,780
The boy is completely lost.

387
01:13:24,880 --> 01:13:26,620
He slept the whole
afternoon on the sofa.

388
01:13:26,720 --> 01:13:28,060
He just woke up.

389
01:13:28,160 --> 01:13:32,060
You don't need to tell me, I live here.

390
01:13:32,600 --> 01:13:34,600
Here.

391
01:13:39,400 --> 01:13:43,100
You can sleep in Gino's room tonight.

392
01:13:45,400 --> 01:13:50,315
And tomorrow. And the
day after tomorrow.

393
01:13:56,720 --> 01:14:00,120
Your soup's getting cold, Sabrina.

394
01:14:02,600 --> 01:14:05,660
Pim really can't come to live with us.

395
01:14:05,760 --> 01:14:07,820
He's always been part of this family.

396
01:14:07,920 --> 01:14:10,354
Yeah, I'd noticed.

397
01:14:11,040 --> 01:14:16,140
That didn't bother you before.
- That's in the past.

398
01:14:17,440 --> 01:14:19,940
Little Pim found his way.

399
01:14:20,480 --> 01:14:24,700
When the gypsy was living with
them, we never saw him.

400
01:14:24,800 --> 01:14:28,180
You knew that Pim and Gino...
- He's staying here.

401
01:14:28,280 --> 01:14:31,500
That boy has never had a true home.

402
01:14:31,600 --> 01:14:35,479
Some mother. Gone with the wind.

403
01:14:37,960 --> 01:14:39,780
Yvette.

404
01:14:39,880 --> 01:14:43,380
Even your father wasn't
immune to that man-eater.

405
01:14:43,480 --> 01:14:45,480
That rhymes.

406
01:14:45,920 --> 01:14:51,720
Yvette Mimosa. With her seductive
manner and her accordion.

407
01:14:53,000 --> 01:14:55,620
That blonde slut thought
she could have them all.

408
01:14:55,720 --> 01:14:59,180
When one left, he was
immediately replaced.

409
01:14:59,280 --> 01:15:04,980
She was unfaithful, Miss
Provocative. That hasn't changed.

410
01:15:05,200 --> 01:15:07,600
Oh, my handsome Stefano.

411
01:15:08,640 --> 01:15:13,220
You don't have a picture, Mom?
All women have a picture of...

412
01:15:13,320 --> 01:15:15,834
Stop asking me.

413
01:15:19,040 --> 01:15:21,640
I should check my kidneys.

414
01:15:22,520 --> 01:15:24,780
I have the right to know
what my father looked like.

415
01:15:24,880 --> 01:15:29,431
Sabrina, I... I can...

416
01:15:31,000 --> 01:15:34,500
Is he still alive, at
least? I just want to know.

417
01:15:34,600 --> 01:15:41,100
No, I don't know. And don't speak
to me in this tone of voice, ok?

418
01:17:01,960 --> 01:17:04,760
I used to be better at this.

419
01:17:22,680 --> 01:17:24,680
Shall I make tea?

420
01:17:31,400 --> 01:17:32,980
Where are you going?

421
01:17:33,080 --> 01:17:35,719
Nowhere. Out for some air.

422
01:17:36,360 --> 01:17:38,700
Put on your coat.
- It's summer, Mom.

423
01:17:38,800 --> 01:17:44,260
But it feels like autumn. And stop
at the butcher's on the way back.

424
01:17:44,360 --> 01:17:51,060
Who said I was coming back?
- Half a pound of sliced bacon, Sabrina.

425
01:19:02,960 --> 01:19:04,960
So quick.

426
01:19:57,640 --> 01:19:59,640
My bag.

427
01:20:02,240 --> 01:20:04,276
Where are you going?

428
01:20:23,280 --> 01:20:27,280
What are you looking for?
Can I help you?

429
01:20:38,280 --> 01:20:40,280
Your father.

430
01:20:44,760 --> 01:20:46,760
Look.

431
01:21:05,320 --> 01:21:07,320
Mom?

432
01:21:16,200 --> 01:21:18,200
Mom.

433
01:23:36,760 --> 01:23:41,311
A, b, c, d, e,

434
01:23:41,520 --> 01:23:48,232
f, g, h, i, j, k, l, m,

435
01:23:48,440 --> 01:23:52,797
n, o, p, q, r,

436
01:23:53,000 --> 01:23:59,951
s, t, u, v, w, x, y, z.

437
01:24:02,200 --> 01:24:04,200
The sea.

438
01:24:46,040 --> 01:24:48,040
Where's Fran�oise?

439
01:24:49,720 --> 01:24:51,820
She didn't feel well.

440
01:24:53,360 --> 01:24:55,360
What a bitch.

441
01:25:43,240 --> 01:25:45,240
Want some water?

442
01:25:45,240 --> 01:25:47,240
Sure.

443
01:26:19,640 --> 01:26:22,950
'House to let'

444
01:26:29,320 --> 01:26:32,220
Ms. Desmet said there are still
things belonging of your mother,

445
01:26:32,320 --> 01:26:35,140
and he asked what to do with them.

446
01:26:35,240 --> 01:26:37,240
Ditch them.

447
01:26:42,960 --> 01:26:45,460
Make yourself a sandwich.

448
01:26:46,000 --> 01:26:49,900
Perhaps I can bring back some Takeaway.

449
01:27:00,600 --> 01:27:02,600
Dreadful weather.

450
01:27:03,800 --> 01:27:05,950
Can I come in?

451
01:27:07,200 --> 01:27:09,200
Hush, Mirza.

452
01:27:24,200 --> 01:27:28,000
Horrible rain.
- You can say that loud.

453
01:27:30,720 --> 01:27:33,300
Your sister's not here.
On Monday, Tuesday and...

454
01:27:33,400 --> 01:27:35,400
I came to see you.

455
01:27:51,880 --> 01:27:53,880
Here.

456
01:27:56,040 --> 01:27:59,840
Do you want something to drink?
- Beer.

457
01:28:15,120 --> 01:28:17,920
I'm living here temporarily.

458
01:28:18,760 --> 01:28:20,760
So you know.

459
01:28:22,840 --> 01:28:24,840
It's temporary.

460
01:28:37,680 --> 01:28:40,180
What do you want from me?

461
01:28:55,600 --> 01:28:57,800
I was waiting for you.

462
01:29:07,240 --> 01:29:10,915
Here, a gift is forever.

463
01:29:15,800 --> 01:29:17,900
Keep it in a knot,...

464
01:29:19,400 --> 01:29:22,600
...and you'll never forget me.

465
01:29:45,920 --> 01:29:47,920
Stay.

