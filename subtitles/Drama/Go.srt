1
00:01:16,600 --> 00:01:19,600
You know what I like about Christmas?
The surprises.

2
00:01:20,000 --> 00:01:22,800
You get this box and you're
sure of what's inside.

3
00:01:23,200 --> 00:01:24,500
You shake it, weigh it.

4
00:01:24,700 --> 00:01:27,600
You're convinced you have it pegged.
No doubt in your mind.

5
00:01:28,100 --> 00:01:30,300
But then you open it
and it's different.

6
00:01:30,700 --> 00:01:32,500
You know. Wow! Bang! Surprise!

7
00:01:32,900 --> 00:01:35,700
I mean, it's kind of like
you and me here, you know?

8
00:01:36,800 --> 00:01:40,100
I'm not saying it's anything it's not.

9
00:01:41,100 --> 00:01:44,100
Come on, this time yesterday,
who would've thunk it?

10
00:01:58,300 --> 00:02:00,200
Paper or plastic?

11
00:02:03,200 --> 00:02:05,000
Paper or plastic?

12
00:02:07,600 --> 00:02:10,100
- Paper or plastic?
- Both.

13
00:02:13,600 --> 00:02:15,900
You didn't double my coupons?

14
00:02:17,100 --> 00:02:20,900
They're at the bottom in red,
where it says "double coupons."

15
00:02:27,500 --> 00:02:31,300
You can't put bleach in
the same bag as food. It's poison.

16
00:02:42,100 --> 00:02:45,300
Don't think you're something
you're not. I had your job.

17
00:02:48,500 --> 00:02:50,300
Look how far it got you.

18
00:02:56,000 --> 00:02:59,900
Nothing brings back warm holiday
memories like the songs you love.

19
00:03:00,300 --> 00:03:01,600
- Don't.
- Why not?

20
00:03:01,900 --> 00:03:04,600
Because she's been on for 14 hours.

21
00:03:06,800 --> 00:03:08,300
- Ronna?
- No.

22
00:03:08,800 --> 00:03:10,700
- I didn't ask yet.
- The answer's no.

23
00:03:11,100 --> 00:03:12,800
Christmas is a time for giving.

24
00:03:13,200 --> 00:03:14,300
Mistletoe?

25
00:03:16,700 --> 00:03:19,700
Pardon me if I'm not in
a holly-jolly mood right now.

26
00:03:20,200 --> 00:03:21,600
I'm getting evicted.

27
00:03:22,400 --> 00:03:23,700
Tomorrow.

28
00:03:24,300 --> 00:03:26,600
They wouldn't evict you at Christmas.

29
00:03:27,500 --> 00:03:29,500
You'd be ho-ho-homeless.

30
00:03:30,800 --> 00:03:32,200
- How much you owe?
- $380.

31
00:03:32,600 --> 00:03:34,400
- That's nothing.
- It's more than I got.

32
00:03:34,700 --> 00:03:35,600
Want my shift?

33
00:03:37,900 --> 00:03:40,400
My best mates are going
to Las Vegas this weekend.

34
00:03:40,800 --> 00:03:43,300
I've never been.
I'm told it's incredible.

35
00:03:43,600 --> 00:03:47,300
If you take my shift,
I can go with them. Everybody wins.

36
00:03:50,700 --> 00:03:52,200
Cash up front?

37
00:03:53,900 --> 00:03:55,100
Deal.

38
00:03:57,600 --> 00:04:00,100
I'll throw in an extra $20
for a blowjob.

39
00:04:05,900 --> 00:04:07,500
Any available cashier.

40
00:04:07,800 --> 00:04:10,000
I need a cashier up at the front.

41
00:04:13,600 --> 00:04:15,500
Look, I went the last time.

42
00:04:15,800 --> 00:04:18,200
Okay. Dead Celebrities.
Loser goes up.

43
00:04:18,600 --> 00:04:19,400
Omar Sharif.

44
00:04:22,400 --> 00:04:23,500
Steve McQueen.

45
00:04:24,500 --> 00:04:25,500
Michael Landon.

46
00:04:26,300 --> 00:04:27,300
Lee Marvin.

47
00:04:29,200 --> 00:04:30,000
Shit.

48
00:04:35,000 --> 00:04:36,300
Malcolm X.

49
00:04:41,600 --> 00:04:42,900
You can't say Malcolm X.

50
00:04:43,300 --> 00:04:44,800
He's famous. He's dead.

51
00:04:45,200 --> 00:04:47,200
That's a rule.
Nothing starts with an "X."

52
00:04:47,600 --> 00:04:49,000
That's not my problem.

53
00:04:49,300 --> 00:04:50,500
You can challenge.

54
00:04:50,900 --> 00:04:52,300
- I challenge.
- Bullshit!

55
00:04:52,600 --> 00:04:55,000
Give me one dead celebrity
that begins with an "X."

56
00:04:56,800 --> 00:04:57,800
Fuck!

57
00:04:58,700 --> 00:05:01,500
There is one.
I know I thought of one before.

58
00:05:03,300 --> 00:05:06,200
Break was over four minutes ago.
Who's up front?

59
00:05:16,000 --> 00:05:17,300
I am.

60
00:05:23,900 --> 00:05:25,200
X erxes.

61
00:05:25,400 --> 00:05:26,300
X erxes.

62
00:05:26,600 --> 00:05:29,500
Some dead pharaoh guy,
and his name starts with an "X."

63
00:05:29,800 --> 00:05:32,700
That wasn't it.
Never heard of fucking X erxes.

64
00:05:33,800 --> 00:05:35,900
Pharaoh could've saved your ass.

65
00:05:36,300 --> 00:05:37,800
There's an opera about him.

66
00:05:38,400 --> 00:05:39,600
X erxes.

67
00:05:39,900 --> 00:05:41,800
I took Music Appreciation twice.

68
00:05:42,400 --> 00:05:46,200
What do you mean, he's snowed in?
What's he doing in Chicago anyway?

69
00:05:46,700 --> 00:05:49,200
Look, just call me
when you get to a landline.

70
00:05:49,600 --> 00:05:52,000
He can't get out of the airport.
We're fucked.

71
00:05:52,400 --> 00:05:53,800
Is the British guy still here?

72
00:05:54,400 --> 00:05:55,800
He went to Vegas for the weekend.

73
00:05:57,400 --> 00:05:58,700
Vegas.

74
00:06:03,600 --> 00:06:04,500
Hi...

75
00:06:05,200 --> 00:06:06,500
...Ronna. Right?

76
00:06:08,400 --> 00:06:11,100
Where can I get something
to go with this juice?

77
00:06:11,500 --> 00:06:13,000
Doughnuts, aisle four.

78
00:06:13,400 --> 00:06:15,600
I was hoping for something
more euphoric.

79
00:06:16,000 --> 00:06:17,800
The British guy hooks us up.

80
00:06:20,300 --> 00:06:21,100
How much?

81
00:06:21,300 --> 00:06:23,200
- Twenty at $20.
- You're overpaying.

82
00:06:23,500 --> 00:06:27,400
We're desperate. We're going to
a party at a warehouse. What is it?

83
00:06:28,600 --> 00:06:29,800
"Mary X-mas Superfest."

84
00:06:30,400 --> 00:06:32,600
- You going?
- We had a pre-party planned.

85
00:06:33,000 --> 00:06:36,100
But it's all or nothing
because there's 20 of us.

86
00:06:36,400 --> 00:06:40,000
My friend's stuck in Chicago.
He was supposed to come through for us.

87
00:06:40,500 --> 00:06:42,500
We're dead.
If you could do anything...

88
00:06:43,800 --> 00:06:45,200
...to help us out...

89
00:06:45,700 --> 00:06:46,500
Please.

90
00:06:56,800 --> 00:06:58,300
I'll see.
Give me your number.

91
00:06:59,200 --> 00:07:01,500
Kick ass! Thank you.

92
00:07:03,200 --> 00:07:04,800
So you wanna buy all this OJ?

93
00:07:05,200 --> 00:07:06,500
Absolutely.

94
00:07:13,500 --> 00:07:14,900
You know Simon's in Vegas?

95
00:07:15,100 --> 00:07:17,300
I don't need Simon. I'm going to Todd.

96
00:07:17,800 --> 00:07:19,000
Todd Gaines?

97
00:07:19,500 --> 00:07:20,700
Who's Todd Gaines?

98
00:07:21,000 --> 00:07:22,600
That's Simon's dealer.

99
00:07:23,000 --> 00:07:25,200
You can't go around Simon like that.

100
00:07:25,800 --> 00:07:29,200
Listen, if Simon were here,
which he's not, he'd charge $15...

101
00:07:29,500 --> 00:07:32,600
...when he gets it for $10.
Times 20 hits, that's $100.

102
00:07:33,100 --> 00:07:36,900
It's a leap. You're moving up
the drug food chain without permission.

103
00:07:37,400 --> 00:07:38,800
You shouldn't do this.

104
00:07:39,100 --> 00:07:43,000
Why can't you chill the fuck out, okay?
It's just once.

105
00:07:43,400 --> 00:07:46,900
When Simon gets back, we can still
overpay if it makes you happy.

106
00:07:47,300 --> 00:07:48,700
But this is my deal.

107
00:07:48,900 --> 00:07:50,900
Just sit back and watch.

108
00:08:16,700 --> 00:08:18,600
- Speak.
- Todd, it's Ronna Martin.

109
00:08:19,000 --> 00:08:20,700
You know me through Simon.

110
00:08:22,700 --> 00:08:24,100
Can I come up?

111
00:08:46,800 --> 00:08:49,400
I thought you bought
quarters off of Simon.

112
00:08:49,800 --> 00:08:52,000
At least, what Simon
pretends are quarters.

113
00:08:52,300 --> 00:08:54,000
I keep him honest.

114
00:08:55,600 --> 00:08:58,200
At that level, you should pinch.
It's the economics.

115
00:08:58,600 --> 00:09:00,400
You leaving?

116
00:09:16,100 --> 00:09:17,500
Be good.

117
00:09:20,500 --> 00:09:22,500
I take it this is not a social call.

118
00:09:25,700 --> 00:09:27,200
I need a favour.

119
00:09:27,600 --> 00:09:30,100
I didn't know we were
such good friends.

120
00:09:30,500 --> 00:09:33,400
If we were, you'd know
I give head before I give favours.

121
00:09:33,700 --> 00:09:35,900
I ain't giving my best friends head...

122
00:09:36,300 --> 00:09:39,200
...so your chance of getting a favour
is pretty slim.

123
00:09:39,800 --> 00:09:41,500
Tell me what you wanna buy.

124
00:09:41,800 --> 00:09:43,500
Twenty hits of Ecstasy.

125
00:10:34,500 --> 00:10:37,500
You come here out of the blue
asking for 20 hits...

126
00:10:38,000 --> 00:10:42,100
...when 20 is the magic number where
"intent to sell" becomes "trafficking"?

127
00:10:42,400 --> 00:10:46,200
- I'd never fuck you like that.
- How would you fuck me?

128
00:11:06,400 --> 00:11:07,500
This is real.

129
00:11:07,900 --> 00:11:11,000
Pharmaceutical grade,
not that crunchy, herbal rave shit.

130
00:11:11,400 --> 00:11:14,800
Don't let anybody double dose.
They'll fry in the emergency room.

131
00:11:15,300 --> 00:11:17,900
- Understood.
- Only one hit per headbanger.

132
00:11:18,300 --> 00:11:19,700
Understood.

133
00:11:21,700 --> 00:11:23,300
Twenty at $15 is $300.

134
00:11:24,600 --> 00:11:26,000
Fifteen?

135
00:11:27,300 --> 00:11:29,700
I was thinking more like 10.

136
00:11:30,300 --> 00:11:32,200
I know you charge Simon 10.

137
00:11:32,500 --> 00:11:34,700
Inflation is a bitch.

138
00:11:36,900 --> 00:11:39,600
There are 20 of us.
I need all of this.

139
00:11:39,900 --> 00:11:42,700
But I only have $200.
That's all I have.

140
00:11:44,800 --> 00:11:46,200
No, no. Hear me out.

141
00:11:46,500 --> 00:11:49,600
Hear me out, okay?
This $200 is like a down payment.

142
00:11:50,000 --> 00:11:52,100
All right?
You give me the stuff...

143
00:11:52,500 --> 00:11:55,400
...I go to them, get the extra $100,
come back and pay you.

144
00:11:55,900 --> 00:12:00,000
That would be doing you a favour.
And you know how I feel about favours.

145
00:12:00,400 --> 00:12:02,100
I could leave something with you.

146
00:12:03,200 --> 00:12:04,500
Collateral.

147
00:12:05,100 --> 00:12:07,500
I already got a fucking Swatch.

148
00:12:08,000 --> 00:12:10,700
I need something
I know you'll come back for.

149
00:12:12,100 --> 00:12:14,100
- I'm not going.
- It's 45 minutes.

150
00:12:14,400 --> 00:12:16,100
Hour, tops. Just sit there.

151
00:12:16,400 --> 00:12:17,500
He's a drug dealer.

152
00:12:17,800 --> 00:12:20,200
Jesus, Claire.
Don't get 8-1-8 on me here.

153
00:12:20,500 --> 00:12:22,100
How much have I done for you?

154
00:12:22,500 --> 00:12:24,900
No. You're making me an accessory.

155
00:12:25,400 --> 00:12:28,700
Claire, my bracelet you're wearing,
that's an accessory.

156
00:12:29,100 --> 00:12:31,600
You're just some chick
sitting in an apartment.

157
00:12:32,100 --> 00:12:33,400
I'm not.

158
00:12:35,500 --> 00:12:37,300
Okay. No bullshit, okay?

159
00:12:38,500 --> 00:12:42,000
I don't get this money, I get evicted.
My ass is on the street.

160
00:12:46,000 --> 00:12:49,100
Claire, I don't have anyone else
to go to, okay?

161
00:12:49,500 --> 00:12:52,300
I'm coming to you
and I am asking for help.

162
00:12:52,700 --> 00:12:54,000
Please.

163
00:13:00,200 --> 00:13:02,600
Fine. 45 minutes.
That's 8:00.

164
00:13:02,900 --> 00:13:05,100
We'll be back by 8. I promise.

165
00:13:13,900 --> 00:13:15,600
It's 237.

166
00:13:16,500 --> 00:13:18,900
Jesus! Next time, ask for directions.

167
00:13:32,100 --> 00:13:33,100
You're a pro.

168
00:13:33,500 --> 00:13:35,300
I'm a top-seeded amateur.

169
00:13:39,100 --> 00:13:41,500
And I'm a very happy man.

170
00:13:51,000 --> 00:13:54,500
Come on in.
Damn glad to meet you, Rhonda.

171
00:13:54,800 --> 00:13:55,600
Ronna.

172
00:13:56,000 --> 00:13:59,000
When I heard Philly got snowed in,
I told these guys...

173
00:13:59,300 --> 00:14:02,200
...it was getting loose there.
I'm glad we found you.

174
00:14:03,700 --> 00:14:05,200
Work, work, work.

175
00:14:06,000 --> 00:14:08,000
Friday must be a busy night.

176
00:14:09,200 --> 00:14:11,000
That was just a friend.

177
00:14:12,200 --> 00:14:13,700
What can I get you to drink?

178
00:14:15,000 --> 00:14:17,000
Some of that orange juice
would be great.

179
00:14:17,500 --> 00:14:18,500
Great.

180
00:14:27,600 --> 00:14:28,800
All right.

181
00:14:32,000 --> 00:14:33,500
I understand...

182
00:14:34,700 --> 00:14:36,700
...this party's gonna be huge.

183
00:14:37,100 --> 00:14:37,900
Massive.

184
00:14:38,300 --> 00:14:39,600
Traci Lords is a promoter.

185
00:14:39,900 --> 00:14:41,000
She is.

186
00:14:42,700 --> 00:14:44,300
That's what I heard.

187
00:14:47,300 --> 00:14:50,200
Ron, hon, we are fresh out of OJ.
Cerveza?

188
00:14:54,400 --> 00:14:57,900
Hey, we bought a bunch
of orange juice tonight.

189
00:14:58,400 --> 00:15:01,000
I just left it in the car.
But I can go get it.

190
00:15:04,200 --> 00:15:07,400
Zack tells me that you've got 20 at $20.
Is that right?

191
00:15:10,900 --> 00:15:12,000
You got a bathroom?

192
00:15:12,600 --> 00:15:14,400
It's down the hall on the right.

193
00:15:14,700 --> 00:15:15,700
Let me show you.

194
00:15:18,100 --> 00:15:20,200
Maybe we could do this first?

195
00:15:30,500 --> 00:15:31,400
Fuck!

196
00:15:31,700 --> 00:15:35,700
We said Chicago. You said Philly.
I mean, maybe she didn't notice.

197
00:15:36,200 --> 00:15:38,200
Everybody just keep it calm.
We're good.

198
00:15:46,200 --> 00:15:47,200
You're fine.

199
00:16:01,500 --> 00:16:03,300
Everything all right?

200
00:16:04,000 --> 00:16:04,900
Fine.

201
00:16:06,800 --> 00:16:07,800
Shit!

202
00:16:17,000 --> 00:16:18,000
Shit!

203
00:16:22,700 --> 00:16:24,100
It's all yours.

204
00:16:31,000 --> 00:16:33,600
Ronna, hon, do we got
a deal here or not?

205
00:16:34,900 --> 00:16:38,900
No, actually, we don't. I came
to tell you I couldn't get anything.

206
00:16:39,400 --> 00:16:42,100
A resourceful girl like you?
I don't believe that.

207
00:16:42,700 --> 00:16:43,900
It's true.

208
00:16:46,200 --> 00:16:48,600
I just wanna make a deal here.
Can we?

209
00:16:48,900 --> 00:16:50,900
Who the fuck are you? Monty Hall?

210
00:16:54,800 --> 00:16:56,500
Did you know I'm only 17?

211
00:16:57,900 --> 00:17:01,100
I probably shouldn't be drinking beer.
Seeing as I'm so...

212
00:17:01,700 --> 00:17:03,400
...underage and all?

213
00:17:11,000 --> 00:17:13,300
That means, get away from the door.

214
00:17:20,100 --> 00:17:22,600
We go back to Todd's,
say they didn't show up...

215
00:17:22,900 --> 00:17:24,700
...then exchange the pills for Claire.

216
00:17:25,000 --> 00:17:26,100
We can't.

217
00:17:26,400 --> 00:17:28,700
- Why not?
- Because they're gone! I flushed them!

218
00:17:29,400 --> 00:17:30,900
Think of something.

219
00:17:31,400 --> 00:17:33,400
I need 100 bucks or 20 hits of X.

220
00:17:37,300 --> 00:17:38,400
What?

221
00:17:43,700 --> 00:17:45,500
You took one, didn't you?

222
00:17:48,900 --> 00:17:50,100
Fuck you, Mannie!

223
00:17:50,400 --> 00:17:53,100
How could you do this to me?
I need you now.

224
00:17:53,900 --> 00:17:55,400
Drive!

225
00:17:55,800 --> 00:17:57,000
Where?

226
00:17:57,300 --> 00:17:58,900
Just drive.

227
00:17:59,700 --> 00:18:00,800
I have an idea.

228
00:20:16,300 --> 00:20:18,200
Were they round or oval?

229
00:20:19,000 --> 00:20:20,100
White.

230
00:20:21,400 --> 00:20:22,400
Round.

231
00:20:22,900 --> 00:20:24,200
Like aspirin.

232
00:20:25,200 --> 00:20:26,800
Like baby aspirin.

233
00:20:30,600 --> 00:20:32,300
A or B?

234
00:20:33,900 --> 00:20:34,700
B.

235
00:20:35,100 --> 00:20:35,900
You're sure?

236
00:20:36,600 --> 00:20:38,800
Well, I wasn't really looking.

237
00:20:41,000 --> 00:20:43,100
Stop fucking paging me, Claire!

238
00:20:56,500 --> 00:20:57,500
Speak.

239
00:20:59,200 --> 00:21:00,700
I'm just licking my dick.

240
00:21:01,000 --> 00:21:02,600
What's up with you?

241
00:21:03,500 --> 00:21:06,700
It's called the Crazy Horse.
You have to look it up.

242
00:21:07,600 --> 00:21:10,400
I don't know what I'm up to.
Where's this party at?

243
00:21:13,500 --> 00:21:15,800
The party?
Where's it at?

244
00:21:25,900 --> 00:21:28,000
It's called "Mary X-mas."

245
00:21:28,300 --> 00:21:30,000
"Mary," like a chick.

246
00:21:31,400 --> 00:21:33,600
Like her name is Mary.

247
00:21:33,900 --> 00:21:36,800
Not like you marry her,
you fucking moron.

248
00:21:37,200 --> 00:21:40,400
I don't know. It's some warehouse shit.
Will this be cool?

249
00:21:41,300 --> 00:21:42,300
Yeah, I guess.

250
00:21:42,500 --> 00:21:47,000
My friend Claire here says
it's gonna be a kick-ass fucking time.

251
00:21:48,500 --> 00:21:50,000
What, you know her?

252
00:21:51,200 --> 00:21:53,100
It's your buddy Simon.
He's in Vegas.

253
00:21:54,100 --> 00:21:55,300
Yeah, I know.

254
00:21:56,200 --> 00:21:57,800
She knows.

255
00:21:59,200 --> 00:22:00,900
Hell, I don't know.

256
00:22:03,600 --> 00:22:04,500
Maybe.

257
00:22:07,500 --> 00:22:10,200
Well, save a load for me, big boy.

258
00:22:10,500 --> 00:22:11,900
Whatever.

259
00:22:24,100 --> 00:22:26,400
What do you want for Christmas, Claire?

260
00:22:28,000 --> 00:22:29,400
I don't know.

261
00:22:30,500 --> 00:22:31,900
Do you wanna get laid?

262
00:22:33,400 --> 00:22:34,400
No.

263
00:22:35,900 --> 00:22:37,800
No, you don't wanna get laid?

264
00:22:38,200 --> 00:22:41,400
Or no, you do, but you don't wanna
get laid with me?

265
00:22:41,800 --> 00:22:43,100
Look, they'll be here.

266
00:22:47,400 --> 00:22:48,900
They'll be here.

267
00:22:54,700 --> 00:22:56,000
Are you a virgin?

268
00:22:57,400 --> 00:22:58,600
What?

269
00:22:58,900 --> 00:23:00,700
Come on, Claire.

270
00:23:01,800 --> 00:23:03,400
Answer the question.

271
00:23:03,700 --> 00:23:06,600
Answer the question, Claire!

272
00:23:08,300 --> 00:23:10,500
Breakfast Club. I get it.
Very funny.

273
00:23:16,000 --> 00:23:17,600
Don't say anything.

274
00:23:18,500 --> 00:23:20,500
Try not to look so stoned.

275
00:23:23,400 --> 00:23:26,200
This girl's brother already
came through with it...

276
00:23:26,800 --> 00:23:28,200
I understand that.

277
00:23:32,600 --> 00:23:35,900
Let me just, you know,
fill out a return slip here...

278
00:23:36,300 --> 00:23:38,600
...and the manager will give
you a refund.

279
00:23:38,900 --> 00:23:41,400
I'm trying to explain to you
what happened.

280
00:23:41,700 --> 00:23:45,600
They already got the stuff somewhere
else. It was a miscommunication.

281
00:24:24,800 --> 00:24:28,000
I can hear your thoughts.

282
00:24:30,900 --> 00:24:32,800
Xiang Kai-Shek.

283
00:24:33,400 --> 00:24:35,700
Famous Chinese ruler.
Starts with "X."

284
00:24:37,100 --> 00:24:40,900
No, "C"
Chiang Kai-Shek.

285
00:24:41,900 --> 00:24:43,900
You're going to die.

286
00:24:51,100 --> 00:24:53,200
I'm keeping $50. Call it interest.

287
00:24:53,900 --> 00:24:56,100
I'm really sorry about all this.

288
00:25:03,800 --> 00:25:05,800
I just gave you a favour.

289
00:25:07,400 --> 00:25:09,600
Here I thought you just gave me head.

290
00:25:47,000 --> 00:25:47,900
Crown me.

291
00:25:54,700 --> 00:25:57,300
At the risk of sounding like,
you know...

292
00:25:57,600 --> 00:25:58,700
...me...

293
00:25:59,100 --> 00:26:01,100
...what'll you do about being evicted?

294
00:26:01,400 --> 00:26:02,800
Aren't you still short?

295
00:26:12,500 --> 00:26:13,500
What?

296
00:26:42,000 --> 00:26:43,600
I'm Kelly and this is Donna.

297
00:26:44,000 --> 00:26:46,200
We're wondering
if you wanna hang out.

298
00:26:48,700 --> 00:26:50,800
I think I feel something.

299
00:26:51,600 --> 00:26:53,600
It's really smooth, isn't it?

300
00:26:54,300 --> 00:26:57,800
Donna's brother here is a pharmacist,
so he got it for us.

301
00:27:01,200 --> 00:27:03,700
His name's Chip. My brother.

302
00:27:06,400 --> 00:27:08,900
Is it like a wave?

303
00:27:09,900 --> 00:27:11,500
Or is it like a zoom?

304
00:27:11,900 --> 00:27:13,300
It's like...

305
00:27:14,000 --> 00:27:15,900
...floating.

306
00:27:16,100 --> 00:27:17,100
Like...

307
00:27:17,400 --> 00:27:21,700
..." Hey, man, how's the ground
down there?" and shit.

308
00:27:22,100 --> 00:27:22,900
I got it.

309
00:27:24,600 --> 00:27:26,400
Holy shit!

310
00:27:26,700 --> 00:27:28,300
Fuck, yeah!

311
00:27:29,400 --> 00:27:30,800
Shit!

312
00:27:33,100 --> 00:27:34,100
It's really that cool?

313
00:27:34,500 --> 00:27:38,800
Man, you gotta try this shit.
I'll buy it for you.

314
00:27:40,000 --> 00:27:41,800
You know what makes it even better?

315
00:27:42,300 --> 00:27:45,000
If you take, like,
a lot of pot with it.

316
00:27:45,400 --> 00:27:47,700
I mean, like, a lot of pot.

317
00:27:48,300 --> 00:27:50,700
I can't believe
you're selling allergy medicine.

318
00:27:51,100 --> 00:27:54,400
We're out of that.
We're down to chewable aspirin.

319
00:27:55,800 --> 00:28:00,000
Some people are saying
that you got some really good stuff.

320
00:28:01,300 --> 00:28:02,800
Show me your tits.

321
00:28:19,200 --> 00:28:20,500
So how much have you made?

322
00:28:21,100 --> 00:28:22,800
Four hundred.

323
00:29:53,900 --> 00:29:54,800
Where'd you get it?

324
00:29:55,100 --> 00:29:57,500
This girl inside.
She and her friend had it.

325
00:29:57,800 --> 00:29:59,400
Ecstasy. The real shit?

326
00:29:59,800 --> 00:30:01,100
Pharmaceutical grade.

327
00:30:01,300 --> 00:30:03,400
None of the crunchy, herbal rave shit.

328
00:30:03,700 --> 00:30:06,100
Best 20 bucks you could spend.

329
00:31:08,800 --> 00:31:09,900
What's wrong?

330
00:31:10,500 --> 00:31:12,100
I can't feel my hands.

331
00:31:13,400 --> 00:31:14,800
Jesus, you're burning up.

332
00:31:15,800 --> 00:31:17,700
I can't carry you, okay?

333
00:31:21,400 --> 00:31:22,600
I'm leaving you here.

334
00:31:23,500 --> 00:31:25,100
I'm gonna go get the car.

335
00:31:25,500 --> 00:31:28,200
All right? Just hide here, okay?

336
00:31:28,600 --> 00:31:29,700
Just like a little mouse.

337
00:31:30,100 --> 00:31:34,700
I'm gonna get the car, then come
back for you. I'll see you soon, okay?

338
00:31:58,200 --> 00:31:59,700
How are sales?

339
00:32:03,900 --> 00:32:04,700
I can explain.

340
00:32:04,900 --> 00:32:06,400
I'm not gonna ask you to.

341
00:32:07,700 --> 00:32:10,000
It's not a highly ethical industry.

342
00:32:10,300 --> 00:32:13,300
But goddamn, you fucked me over
for 20 lousy hits.

343
00:32:16,700 --> 00:32:20,700
It's not what it looks like.
It sort of is, but it's complicated.

344
00:32:21,200 --> 00:32:22,500
Not really.

345
00:32:23,700 --> 00:32:25,900
I know I fucked up, okay?
Please...

346
00:32:26,200 --> 00:32:27,500
...I can make it up to you.

347
00:32:27,700 --> 00:32:30,300
I'm the last person
you should ask a favour from.

348
00:32:30,700 --> 00:32:32,200
I have the money.

349
00:32:33,100 --> 00:32:34,500
I have more than I owe you.

350
00:32:34,700 --> 00:32:36,400
So now you're an entrepreneur.

351
00:34:04,700 --> 00:34:06,700
Jalisco? Where's Jalisco?

352
00:34:07,300 --> 00:34:08,200
In Mexico.

353
00:34:11,300 --> 00:34:14,000
Simon, no one is going
to take your shift.

354
00:34:18,100 --> 00:34:20,900
Nothing brings back
warm holiday memories like...

355
00:34:21,500 --> 00:34:22,300
Don't.

356
00:34:22,500 --> 00:34:23,300
Why not?

357
00:34:24,000 --> 00:34:26,000
Because she's been on for 14 hours.

358
00:34:28,100 --> 00:34:29,700
- Ronna.
- No.

359
00:34:30,000 --> 00:34:31,900
- I didn't ask yet.
- The answer's no.

360
00:34:32,200 --> 00:34:33,900
Christmas is a time for giving.

361
00:34:34,300 --> 00:34:35,500
Mistletoe?

362
00:34:39,200 --> 00:34:40,900
- How much you owe?
- $380.

363
00:34:41,300 --> 00:34:43,200
- That's nothing.
- It's more than I got.

364
00:34:43,500 --> 00:34:44,400
Want my shift?

365
00:34:46,500 --> 00:34:49,500
My best mates are going
to Las Vegas this weekend.

366
00:34:50,000 --> 00:34:52,300
I've never been.
I'm told it's incredible.

367
00:34:52,600 --> 00:34:56,500
If you take my shift,
I can go with them. Everybody wins.

368
00:34:58,200 --> 00:34:59,700
Cash up front?

369
00:35:02,300 --> 00:35:03,200
Deal.

370
00:35:05,600 --> 00:35:07,500
I'll give you an extra $20
for a blowjob.

371
00:35:18,500 --> 00:35:20,800
Fucking let me out of here!

372
00:35:32,300 --> 00:35:33,300
What?

373
00:35:34,000 --> 00:35:35,700
Shut your big butt.

374
00:35:44,100 --> 00:35:45,800
Motherfucking jack-in-the-box!

375
00:35:46,200 --> 00:35:48,300
Dude, you passed out
before we left L.A.

376
00:35:51,100 --> 00:35:52,400
He still looks great.

377
00:35:52,900 --> 00:35:53,800
Watch out.

378
00:35:56,400 --> 00:35:57,200
Damn!

379
00:35:57,600 --> 00:35:59,200
Man, watch the kicks!

380
00:36:00,700 --> 00:36:02,500
That is nasty.

381
00:36:05,800 --> 00:36:06,800
Peep this.

382
00:36:07,100 --> 00:36:11,000
This chick's bobbing up and down
on my dick like Marilyn Chambers...

383
00:36:11,500 --> 00:36:12,800
She found your dick?

384
00:36:13,700 --> 00:36:17,200
Then she moves around the outside.
She's painting the tree.

385
00:36:17,700 --> 00:36:19,300
It hits her in the eye.

386
00:36:19,600 --> 00:36:23,300
And her contact? It's, like, stuck
on the end of my dick.

387
00:36:26,600 --> 00:36:29,300
Yo, her contact is stuck
on the end of my dick!

388
00:36:29,800 --> 00:36:31,000
Was it hard or soft?

389
00:36:31,200 --> 00:36:32,000
My dick?

390
00:36:32,300 --> 00:36:35,200
- The contact lens.
- Remember if it was coloured?

391
00:36:35,500 --> 00:36:37,300
That she had two blue eyes...

392
00:36:37,600 --> 00:36:39,600
...and now one blue and one brown?

393
00:36:40,000 --> 00:36:40,800
What does it matter?

394
00:36:41,900 --> 00:36:44,400
It matters because it happened to me.

395
00:36:44,800 --> 00:36:47,800
That was my story.
I told that story a year ago, man.

396
00:36:48,800 --> 00:36:51,700
The difference is I knew
those small details.

397
00:36:52,100 --> 00:36:54,200
That and my story was true.

398
00:36:56,600 --> 00:36:57,400
Whatever.

399
00:36:57,600 --> 00:36:58,500
"Whatever"?

400
00:36:59,000 --> 00:36:59,800
Whatever.

401
00:37:00,000 --> 00:37:01,400
What do you mean?

402
00:37:01,700 --> 00:37:05,100
Pull your stinky dinky out of my ass!
I was making conversation.

403
00:37:05,400 --> 00:37:06,300
Fuck!

404
00:37:06,600 --> 00:37:08,100
Give a nigger a break!

405
00:37:08,500 --> 00:37:10,700
"Nigger"? What nigger, this nigger?

406
00:37:11,000 --> 00:37:13,700
I told you my mother's mother's
mother was black.

407
00:37:14,000 --> 00:37:15,800
This ain't Roots.

408
00:37:16,100 --> 00:37:17,900
Show me this Nubian's picture.

409
00:37:18,200 --> 00:37:20,200
I don't carry her picture.

410
00:37:20,500 --> 00:37:22,400
If you were less black,
you'd be clear.

411
00:37:22,600 --> 00:37:23,900
She was black as night!

412
00:37:24,300 --> 00:37:25,700
Okay, stop! Truce!

413
00:37:26,000 --> 00:37:27,100
Shut up!

414
00:37:31,800 --> 00:37:33,000
Man, I see black.

415
00:37:33,300 --> 00:37:36,000
I know I am.
Colour's a state of mind, Marcus.

416
00:37:36,700 --> 00:37:39,700
You know, you're right.
Thank you, Rhythm Nation.

417
00:37:40,100 --> 00:37:41,700
Fuck you, Vanilla Ice.

418
00:37:44,800 --> 00:37:46,900
So what does Valentina do?

419
00:37:47,300 --> 00:37:49,300
- Valentina's a nutritionist.
- Nice.

420
00:37:49,800 --> 00:37:52,500
She also teaches a class
at this college.

421
00:37:52,900 --> 00:37:54,600
Class? What class?

422
00:37:55,100 --> 00:37:58,400
It's called
"Tantric Sexuality for Couples."

423
00:37:58,800 --> 00:38:00,600
She teaches people how to fuck?

424
00:38:01,600 --> 00:38:02,700
Yo, I taught myself.

425
00:38:03,700 --> 00:38:05,400
Yo, are you done yet, girl?

426
00:38:06,000 --> 00:38:07,800
That's a shrimp ass.

427
00:38:09,200 --> 00:38:11,100
That shrimp's full of iodine.

428
00:38:11,400 --> 00:38:15,100
This shit's expensive. You're spending
$5 for lettuce and seeds.

429
00:38:16,000 --> 00:38:17,100
Damn!

430
00:38:21,100 --> 00:38:23,300
The thing is, most people...

431
00:38:23,800 --> 00:38:25,300
...don't know how to make love.

432
00:38:25,600 --> 00:38:28,200
They stick it in, move it around
till they get off.

433
00:38:28,700 --> 00:38:31,500
But what Tantra teaches you
is how to deepen...

434
00:38:31,800 --> 00:38:33,700
...prolong the sexual experience.

435
00:38:34,100 --> 00:38:35,600
Bring it to a higher level.

436
00:38:36,000 --> 00:38:39,900
If one man in 10 was having the sex
I'm having, there'd be no war.

437
00:38:40,800 --> 00:38:43,000
What's the longest you two ever did it?

438
00:38:43,300 --> 00:38:45,100
- Fourteen hours.
- Holy shit!

439
00:38:45,400 --> 00:38:46,900
How many times you shoot?

440
00:38:47,200 --> 00:38:48,600
Not once, man.

441
00:38:49,900 --> 00:38:52,200
You didn't go once?
Not even at the end?

442
00:38:52,600 --> 00:38:54,600
You redirect the orgasm inside.

443
00:38:55,400 --> 00:38:58,300
How long do your orgasms last?
Two, three seconds?

444
00:38:59,900 --> 00:39:03,400
I've had orgasms that have lasted
up to an hour and a half.

445
00:39:03,900 --> 00:39:05,200
That's bullshit!

446
00:39:05,500 --> 00:39:07,200
Honest to God and I mean Allah.

447
00:39:07,600 --> 00:39:08,400
Word.

448
00:39:08,600 --> 00:39:09,500
- Word.
- Wait.

449
00:39:09,900 --> 00:39:11,700
- No love.
- When'd you last get off?

450
00:39:12,000 --> 00:39:14,800
I haven't ejaculated in six months.

451
00:39:18,500 --> 00:39:20,200
Anyone can do it.

452
00:39:20,500 --> 00:39:22,000
It just takes discipline.

453
00:39:22,300 --> 00:39:25,500
Bullshit. You're some
Obi-Wan Kenobi motherfucker.

454
00:39:26,000 --> 00:39:28,900
You call me old school.
I'm for coming and going.

455
00:39:31,300 --> 00:39:32,600
Hour and a half.

456
00:39:36,600 --> 00:39:39,400
What the hell you doing with a gold...
Todd Gaines?

457
00:39:39,800 --> 00:39:41,500
The drug dealer.

458
00:39:42,000 --> 00:39:46,200
He gets a discount here.
He let me borrow it. He's a good guy.

459
00:39:46,600 --> 00:39:50,000
He's the good drug dealer. Right.
Sometimes I get confused.

460
00:39:50,400 --> 00:39:53,600
Relax, we're gonna pay cash.
That's just to get the room.

461
00:39:55,300 --> 00:39:56,200
Come on, Singh.

462
00:39:56,500 --> 00:39:59,100
I told you not to eat
those shrimp, didn't I?

463
00:39:59,500 --> 00:40:01,500
Todd, it's Simon.
What's up?

464
00:40:03,100 --> 00:40:04,900
I'm in Vegas. We just got here.

465
00:40:05,300 --> 00:40:07,900
What was the name of
that place you said we...

466
00:40:08,300 --> 00:40:09,800
Crazy Horse.

467
00:40:10,100 --> 00:40:12,100
Right. So what you up to tonight?

468
00:40:12,400 --> 00:40:13,700
Singh, come on now!

469
00:40:14,000 --> 00:40:15,300
Hold on, man.

470
00:40:23,400 --> 00:40:24,700
Damn, Singh!

471
00:40:26,000 --> 00:40:29,000
See, I told you not to eat
those shrimp, didn't I?

472
00:40:29,400 --> 00:40:30,600
You're going to a wedding?

473
00:40:30,900 --> 00:40:32,900
So what is it, like a rave?

474
00:40:34,000 --> 00:40:35,700
How's your stomach feel, Singh?

475
00:40:37,400 --> 00:40:39,000
No gambling for you.

476
00:40:39,400 --> 00:40:40,400
I know Claire.

477
00:40:41,100 --> 00:40:42,500
So you gonna fuck her?

478
00:40:47,600 --> 00:40:50,000
All I know is I plan
to get thoroughly laid.

479
00:40:50,300 --> 00:40:51,100
Who are you?

480
00:40:51,400 --> 00:40:52,200
This is our room.

481
00:40:52,500 --> 00:40:53,800
What are you doing?

482
00:40:54,100 --> 00:40:55,500
Raping small children.

483
00:41:04,100 --> 00:41:06,500
Did I mention how much
I like your jacket?

484
00:41:14,000 --> 00:41:16,600
Bitch, look at your shirt.
This ain't Hawaii.

485
00:41:17,600 --> 00:41:18,500
Lend me some money.

486
00:41:18,700 --> 00:41:20,600
- Man, where's your money?
- I lost it.

487
00:41:21,300 --> 00:41:22,700
We've been here five minutes.

488
00:41:23,100 --> 00:41:26,200
I played this game at a $100 table
and didn't understand it.

489
00:41:26,600 --> 00:41:28,600
Now I do.
I figured out how to beat it.

490
00:41:28,900 --> 00:41:30,700
Cool. Give me your wallet.

491
00:41:31,400 --> 00:41:32,200
Here you go.

492
00:41:32,900 --> 00:41:36,100
- What're you doing?
- Come back in an hour for it.

493
00:41:36,900 --> 00:41:40,600
In an hour, I'll give you your money.
Now get out. You're bad luck.

494
00:41:42,200 --> 00:41:44,300
Scram. Damn it!

495
00:42:05,000 --> 00:42:06,200
Darling?

496
00:42:26,200 --> 00:42:27,300
Hello.

497
00:42:28,200 --> 00:42:29,500
Cheers!

498
00:43:03,100 --> 00:43:04,500
Oh, my God!

499
00:43:10,400 --> 00:43:13,600
So if you're from over there...

500
00:43:14,000 --> 00:43:16,800
...then where did you meet
these friends of yours...

501
00:43:17,800 --> 00:43:19,400
...who I don't see?

502
00:43:21,500 --> 00:43:24,800
The others already knew each other.
I met Marcus in traffic school.

503
00:43:25,100 --> 00:43:26,900
In traffic school?

504
00:43:28,000 --> 00:43:29,800
Listen, I'm a good driver, I am.

505
00:43:30,100 --> 00:43:32,300
I learned everything
from American television.

506
00:43:32,600 --> 00:43:33,400
Great.

507
00:43:33,700 --> 00:43:35,800
Hunter, Magnum P. I...

508
00:43:36,100 --> 00:43:37,900
Knight Rider is an excellent program.

509
00:43:39,900 --> 00:43:41,900
Are you having a go at me here?

510
00:43:43,900 --> 00:43:45,400
Our stop.

511
00:43:52,400 --> 00:43:55,400
So do you want to be
getting high with us?

512
00:44:08,800 --> 00:44:10,900
Otherwise, I can't hold it in.

513
00:44:26,100 --> 00:44:26,900
Oh, my God!

514
00:44:27,200 --> 00:44:28,200
Oh, my God!

515
00:44:30,700 --> 00:44:32,400
Oh, my God! Oh, my God!

516
00:44:33,600 --> 00:44:35,000
- You're fine.
- I'm so bad!

517
00:44:35,300 --> 00:44:38,200
- I don't know what I'm doing!
- Tell her she's fine!

518
00:44:39,100 --> 00:44:40,800
You're beautiful.

519
00:45:12,500 --> 00:45:14,200
Did you go?

520
00:45:18,500 --> 00:45:19,900
Why not?

521
00:45:22,400 --> 00:45:24,000
Tantra, baby.

522
00:46:15,300 --> 00:46:16,200
Sorry.

523
00:46:32,400 --> 00:46:34,000
Could you hand me a towel?

524
00:46:42,600 --> 00:46:44,000
Baby, what's up?

525
00:46:52,800 --> 00:46:55,500
I'm not a bathroom attendant!

526
00:46:56,500 --> 00:46:58,100
Redneck motherf...!

527
00:47:02,200 --> 00:47:03,100
Man, we're leaving.

528
00:47:03,400 --> 00:47:04,900
- Hell, yeah.
- Let's go.

529
00:47:14,400 --> 00:47:17,000
Keep it close and there's
an extra $10 for you.

530
00:47:21,800 --> 00:47:24,000
Get in the car. Get in.

531
00:47:49,200 --> 00:47:50,100
Shit! Look at this.

532
00:47:50,400 --> 00:47:51,900
Oh, my Lord!

533
00:47:53,900 --> 00:47:54,800
Don't point it at me.

534
00:47:55,100 --> 00:47:58,000
- How do I know if it's loaded?
- First, stop pointing it!

535
00:48:00,700 --> 00:48:02,900
You know, I've never held
a real gun before.

536
00:48:03,300 --> 00:48:04,800
It's heavier than I thought.

537
00:48:05,100 --> 00:48:06,400
Could you put it down, please?

538
00:48:06,800 --> 00:48:08,800
I wonder if it's loaded.

539
00:48:09,200 --> 00:48:10,600
How do you get this...

540
00:48:16,600 --> 00:48:18,300
I can't do it. It's stuck.

541
00:48:22,900 --> 00:48:24,800
Give me that gun! Take the wheel.

542
00:48:25,000 --> 00:48:27,300
Goddamn you!

