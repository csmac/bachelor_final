1
00:02:12,347 --> 00:02:15,299
It's over.

2
00:02:18,808 --> 00:02:21,511
I will always speak to you...

3
00:02:22,058 --> 00:02:25,140
...and I don't mind if
you don't say anything.

4
00:02:29,352 --> 00:02:34,224
Just beacuse you went away, it doesn't
mean you're not here anymore.

5
00:02:36,813 --> 00:02:40,558
Perhaps all I ever needed
was this gift.

6
00:02:44,189 --> 00:02:47,722
The one you gave to me at the end.

7
00:04:25,386 --> 00:04:28,587
WOMB

8
00:05:40,992 --> 00:05:44,323
- Shall I read to you?
- No, thanks.

9
00:05:47,911 --> 00:05:51,241
Good night, Grandpa.

10
00:06:19,294 --> 00:06:23,503
Hi! I'm Tommy.
And you?

11
00:06:27,129 --> 00:06:28,954
- Rebecca.

12
00:06:30,516 --> 00:06:32,092
Have you moved here?

13
00:06:32,923 --> 00:06:36,337
No, my grandpa lives here,
I'm staying with him.

14
00:07:51,405 --> 00:07:54,606
Rebecca, your grandfather's on the phone.

15
00:07:56,740 --> 00:08:00,604
She's fallen asleep.
Shall I wake her up?

16
00:08:03,449 --> 00:08:06,356
No. Not at all.
That's fine.

17
00:08:06,813 --> 00:08:08,524
She's welcome to stay tonight.

18
00:08:14,120 --> 00:08:15,873
Good night!

19
00:09:19,722 --> 00:09:22,591
Tell me where we're going.

20
00:11:29,635 --> 00:11:32,587
Up till now, he's only seen
blades of grass before.

21
00:11:33,470 --> 00:11:36,671
It must seem like a
different planet to him.

22
00:11:38,221 --> 00:11:41,635
Tomorrow we'll take him on the beach
and show him the sea.

23
00:11:43,431 --> 00:11:46,927
I won't be here tomorrow.
I'm going away.

24
00:11:50,349 --> 00:11:53,301
Are you going home?

25
00:11:55,226 --> 00:11:58,178
- No.
- Where then?

26
00:11:58,852 --> 00:12:02,053
- To Tokyo.
- Japan?

27
00:12:04,395 --> 00:12:05,857
- What for?
-- My mother was there job.

28
00:12:07,446 --> 00:12:09,704
Mom's got a job there.

29
00:12:09,938 --> 00:12:14,728
- We'll be living on the 72nd floor.
- 72nd?

30
00:12:16,774 --> 00:12:19,856
That's high.

31
00:12:21,317 --> 00:12:24,268
I have to go.

32
00:13:02,412 --> 00:13:05,944
- Hi?
- Rebecca?

33
00:13:05,630 --> 00:13:06,865
Where are you?

34
00:13:06,866 --> 00:13:09,829
On the beach, I came out to look to the storm.

35
00:13:10,957 --> 00:13:15,911
-- When are you going tomorrow?
- I'm leaving at six, on the ferry.

36
00:13:17,125 --> 00:13:20,989
I'll see you off, I've got a good idea.

37
00:13:23,126 --> 00:13:26,209
- Rebecca?
- Yes, I'm here.

38
00:13:28,587 --> 00:13:33,376
Tommy, hello?
Tommy?

39
00:14:12,141 --> 00:14:15,555
It's time, Rebecca.

40
00:16:03,382 --> 00:16:06,915
- Excuse me.
- What can I do for you?

41
00:16:08,508 --> 00:16:12,717
- I've come to see Tommy.
- Oh, he hasn't lived here for a long time.

42
00:16:16,803 --> 00:16:19,755
You look very familiar.
Have I met you somewhere before?

43
00:16:20,846 --> 00:16:23,596
I'm Rebecca.

44
00:16:28,055 --> 00:16:31,837
Rebecca ... The little girl,
that went to Japan?

45
00:16:34,849 --> 00:16:39,425
For a while, Tommy nagged us
about going to Tokyo.

46
00:16:41,560 --> 00:16:44,725
But then he forgot about it, luckily.

47
00:16:48,478 --> 00:16:51,098
Where does he live now?

48
00:16:51,729 --> 00:16:55,511
Do you know the little harbor
down by the embankment?

49
00:16:56,689 --> 00:16:59,523
You'll find him there.

50
00:17:13,402 --> 00:17:16,022
Hello?

51
00:17:37,160 --> 00:17:41,025
Who are you?
What are you doing?

52
00:17:43,619 --> 00:17:47,365
- I am looking for Thomas.
- I'm Thomas.

53
00:17:52,039 --> 00:17:54,872
Who are you?

54
00:18:03,834 --> 00:18:06,786
Hi, Tom.

55
00:18:18,214 --> 00:18:20,832
Hi.

56
00:18:21,839 --> 00:18:24,672
Who is she?

57
00:18:27,966 --> 00:18:30,799
How was Japan?

58
00:18:31,551 --> 00:18:34,964
- Fine.
- 72 th floor.

59
00:18:42,512 --> 00:18:48,628
So, let us like normal people.
I Rous, this volume. And as you name?

60
00:18:56,975 --> 00:19:01,265
- Are you going back?
- No, I finished my degree.

61
00:19:01,809 --> 00:19:04,298
- What did you study?
- Maths.

62
00:19:05,102 --> 00:19:07,591
And now?
What do you do now?

63
00:19:08,019 --> 00:19:11,221
I design software for sonar devices.

64
00:19:14,688 --> 00:19:18,552
Sonar devices that detects tiny cracks
in underground storage tanks.

65
00:19:23,774 --> 00:19:29,606
Like the kind used in petrol stations,
you know they sometimes crack.

66
00:19:31,735 --> 00:19:34,817
Cool.

67
00:19:38,736 --> 00:19:41,688
So Bingo let you come in just like that?

68
00:19:42,821 --> 00:19:45,679
I've never seen him do that,
he hates strangers usually.

69
00:19:45,680 --> 00:19:47,607
Once he bit a girl's thigh,
though it were monthly.

70
00:19:50,741 --> 00:19:54,865
- Maybe he liked your scent.
- Maybe.

71
00:19:59,826 --> 00:20:03,359
Maybe you two just start
sniffing each other?

72
00:20:17,331 --> 00:20:20,532
How many years is it?

73
00:20:23,458 --> 00:20:27,785
- Twelve.
- Twelve.

74
00:20:33,211 --> 00:20:36,494
Maybe I should go.

75
00:21:36,104 --> 00:21:38,973
Does Rose know that you're here?

76
00:21:41,523 --> 00:21:44,475
I don't know.

77
00:21:49,691 --> 00:21:51,829
She is your girlfriend, isn't she?

78
00:21:53,524 --> 00:21:55,056
Girlfriend?

79
00:22:03,363 --> 00:22:07,227
I met her yesterday for
the first time in my life.

80
00:22:45,249 --> 00:22:48,533
How long is is since your
grandfather passed away?

81
00:22:51,251 --> 00:22:54,203
Long time ago.

82
00:22:55,836 --> 00:22:59,037
It's odd.

83
00:23:00,546 --> 00:23:05,833
Seemed bigger in my mind.

84
00:23:11,424 --> 00:23:16,083
Do you remember that night, when
you said you'd see me off because you had an idea...

85
00:23:16,718 --> 00:23:20,250
...and you didn't turn up?

86
00:23:21,968 --> 00:23:25,051
What was your idea, do you remember?

87
00:23:28,095 --> 00:23:31,048
I thought you'd never ask.

88
00:23:53,937 --> 00:23:57,683
I wanted to give him to you that morning.

89
00:24:01,063 --> 00:24:05,852
Tokyo would have been
a great adventure for him.

90
00:24:15,443 --> 00:24:20,101
I fed him, for about a year actually,
after you left.

91
00:24:24,195 --> 00:24:27,396
He managed to eat and shit
at the same time.

92
00:24:29,072 --> 00:24:32,273
Which is impressive.

93
00:24:33,365 --> 00:24:36,114
He looked too good.

94
00:24:36,824 --> 00:24:40,107
And one day, he didn't
appear anymore.

95
00:24:43,493 --> 00:24:46,694
He was dead.

96
00:24:50,036 --> 00:24:52,739
Why didn't you come?

97
00:24:54,454 --> 00:24:57,157
I overslept.

98
00:25:00,206 --> 00:25:02,956
But now I'm awake.

99
00:25:40,092 --> 00:25:42,926
You're cold.

100
00:26:18,687 --> 00:26:20,425
Weird.

101
00:26:22,358 --> 00:26:24,188
I know.

102
00:26:26,787 --> 00:26:28,350
Yeah.

103
00:27:19,372 --> 00:27:23,118
- What are you doing this weekend?
- Same as you.

104
00:27:24,457 --> 00:27:27,159
I can't do it.

105
00:27:27,582 --> 00:27:30,332
- I have to go away.
- I'll come with you.

106
00:27:31,292 --> 00:27:34,705
I have to go alone.
It'll only be two days.

107
00:27:35,626 --> 00:27:38,246
Where were yo go, I go.

108
00:27:42,295 --> 00:27:45,377
This is somewhere you can't go.

109
00:27:50,422 --> 00:27:53,754
Then you shouldn't go either.

110
00:28:00,551 --> 00:28:01,935
They simply drained the marsh...

111
00:28:01,936 --> 00:28:06,063
...and caught it thousands of tons of
reinforced concrete over there.

112
00:28:06,093 --> 00:28:08,193
They built in six months, which is a record.

113
00:28:08,194 --> 00:28:12,838
On paper, it's a wellness center.
They've called it Sparkling Park.

114
00:28:13,554 --> 00:28:16,968
Profit will mainly come from
cyber-prostitution, you know...

115
00:28:17,139 --> 00:28:21,797
...cyber-bitches, the plastic surgery,
cloning of domestic animals.

116
00:28:23,348 --> 00:28:25,219
They opened last week.

117
00:28:25,467 --> 00:28:28,441
This will be their first weekend,
or rather our weekend...

118
00:28:28,442 --> 00:28:30,547
...because our group will
be there too.

119
00:28:30,830 --> 00:28:31,615
Group?

120
00:28:31,834 --> 00:28:36,185
Pollungroup. A militant environmental
activist group.

121
00:28:36,978 --> 00:28:41,719
It will be a planned action,
with 23 participants.

122
00:28:42,312 --> 00:28:45,544
We go inside, we cause
mass hysteria Rebecca...

123
00:28:45,545 --> 00:28:49,469
...I'm telling you, mass hysteria,
thus diverting the security guards.

124
00:28:49,470 --> 00:28:53,286
Now, meanwhile outside, the other guys
were blocked the enterance and...

125
00:28:53,287 --> 00:28:55,773
...exit to the carpark with a quick
setting cement pool.

126
00:28:55,775 --> 00:28:59,614
You know quick-cement?
No? Fine, don't worry about it.

127
00:28:59,776 --> 00:29:04,849
- Anyway, then the media will turn up...
- How do you do that, mass hysteria?

128
00:29:24,057 --> 00:29:25,749
Ready?

129
00:29:45,581 --> 00:29:50,039
You see those rucksacks? We're going to release
the cockroaches from those in the shops.

130
00:29:50,750 --> 00:29:54,957
Around the pools and the restaurants, just
depending on the where the largest crowd are, really.

131
00:29:57,876 --> 00:30:00,828
"Sparkling Park"

132
00:30:07,421 --> 00:30:10,622
I've got six ruckssacks.

133
00:30:25,510 --> 00:30:29,256
- Where are you going? - Away.
- What's all of this you're carrying?

134
00:30:30,261 --> 00:30:32,750
Backpacks.

135
00:30:32,967 --> 00:30:35,842
- Do you want to come in for a coffee?
- No, no, not now.

136
00:30:36,318 --> 00:30:38,073
Well, do I even get a kiss?

137
00:30:55,980 --> 00:31:00,009
- What?
- I love your plan. Did you think it up all by yourself?

138
00:31:00,811 --> 00:31:04,344
A-ha, yeah, of course.

139
00:31:06,355 --> 00:31:07,680
What?

140
00:31:07,937 --> 00:31:12,388
I am but a simple cockroach breeder Rebecca.

141
00:31:13,607 --> 00:31:16,605
I hope that makes me good enough for you.

142
00:31:17,983 --> 00:31:21,813
I've always dreamt of meeting
a cockroach breeder.

143
00:31:46,617 --> 00:31:49,319
I need to pee.

144
00:31:51,159 --> 00:31:54,443
- How badly?
- Badly.

145
00:34:00,739 --> 00:34:06,140
"I will wait for you,
for as long as it takes. "

146
00:37:34,718 --> 00:37:37,384
- I can not eat Rebecca, I'm sorry.
-- At least sit down.

147
00:37:37,552 --> 00:37:39,900
At least sit down.

148
00:37:44,055 --> 00:37:46,887
I can't sit, either.

149
00:37:50,973 --> 00:37:54,138
Would you tell us please
why you invited us here?

150
00:37:56,600 --> 00:37:59,220
Of course.

151
00:38:26,858 --> 00:38:29,561
He could be here with us again.

152
00:38:41,988 --> 00:38:44,821
- Again? Who, Thomas?

153
00:39:03,489 --> 00:39:06,074
Department of Genetic Replication
Consent to Exhumation and Taking of Tissue Samples

154
00:39:06,829 --> 00:39:09,662
Who would give birth to him?

155
00:39:23,542 --> 00:39:25,912
Ralph.

156
00:39:33,545 --> 00:39:35,143
We are atheists...

157
00:39:36,862 --> 00:39:40,168
...and we brought up Thomas
as an atheist.

158
00:39:40,672 --> 00:39:46,433
But that does not mean we could rummage
in our deceased's grave and clone them.

159
00:39:47,174 --> 00:39:50,256
We are not farm animals.

160
00:39:52,259 --> 00:39:55,341
We accept what life gives us...

161
00:39:57,010 --> 00:40:00,156
...and also what it takes away.

162
00:40:00,157 --> 00:40:01,617
But...

163
00:40:02,747 --> 00:40:04,390
But what?

164
00:40:10,889 --> 00:40:14,173
But life has also given us
this opportunity.

165
00:40:16,515 --> 00:40:19,717
This gift.

166
00:40:24,685 --> 00:40:27,767
Don't you see?

167
00:40:38,189 --> 00:40:43,346
If I were you Rebecca,
I would repaint this room white.

168
00:40:48,706 --> 00:40:51,061
Ralph?

169
00:41:33,205 --> 00:41:36,038
Thank you.

170
00:41:42,666 --> 00:41:45,416
Would you like us to move away?

171
00:41:47,209 --> 00:41:50,741
No, stay.

172
00:41:53,002 --> 00:41:57,661
He loved the sea,
he'll love it again.

173
00:41:59,921 --> 00:42:02,702
As for us, there is no way we can stay here.

174
00:42:03,363 --> 00:42:04,945
We're moving next week.

175
00:42:06,048 --> 00:42:08,537
Okay.

176
00:42:09,882 --> 00:42:12,585
Thank you, Ralph.

177
00:42:15,551 --> 00:42:16,893
Rebecca...

178
00:42:19,149 --> 00:42:21,024
...have a good think about it.

179
00:42:21,553 --> 00:42:24,836
You can still change your mind.

180
00:43:06,899 --> 00:43:09,518
Thank you.

181
00:46:02,867 --> 00:46:05,238
- Hi.
- Hello.

182
00:46:06,368 --> 00:46:09,652
- How old is your baby?
- Three months.

183
00:46:11,579 --> 00:46:14,530
Do you want some pie?
Baked it myself.

184
00:46:15,037 --> 00:46:17,906
Thanks, but not now.
I have to go to home.

185
00:46:18,330 --> 00:46:21,744
- Okay.
- Bye. - Bye.

186
00:46:29,917 --> 00:46:32,999
It's alright, baby.

187
00:46:45,922 --> 00:46:48,672
Eat, sweetheart.

188
00:46:52,340 --> 00:46:55,090
It's just us two.

189
00:47:26,642 --> 00:47:29,261
Tommy?

190
00:47:30,101 --> 00:47:32,575
Come out for a bit, I want to tell you something.

191
00:47:32,576 --> 00:47:34,988
- What?
- Come out darling.

192
00:47:36,728 --> 00:47:41,517
I know. I know I'll be going to
primary school and I go on Monday.

193
00:47:43,230 --> 00:47:47,355
Yes, on Monday, you'll go to
primary school. But now come out.

194
00:47:48,773 --> 00:47:51,855
I can see you okay from in here.

195
00:48:00,228 --> 00:48:03,435
I have to speak to you
about your daddy.

196
00:48:07,820 --> 00:48:11,685
Your daddy died in a car
accident before you were born.

197
00:48:15,865 --> 00:48:18,484
- Really?
- Yes.

198
00:48:19,366 --> 00:48:23,360
-- I thought he went back to his home planet.
- His home planet?

199
00:48:24,825 --> 00:48:28,571
- Yes.
- Your father was from this planet.

200
00:48:31,494 --> 00:48:33,050
That's a shame.

201
00:48:33,051 --> 00:48:34,905
Listen Thomas.

202
00:48:36,829 --> 00:48:41,287
Your father was real,
he was a student in biology.

203
00:48:42,914 --> 00:48:45,664
And he was hit by a car.

204
00:48:50,875 --> 00:48:53,827
Do you understand?

205
00:49:34,479 --> 00:49:39,241
A man of words and not of deeds
Is like a garden full of weeds.

206
00:49:39,242 --> 00:49:44,141
And when the weeds begin to grow,
It's like a garden full of snow.

207
00:49:44,142 --> 00:49:49,138
And when the snow begins to fall,
It's like a bird upon the wall.

208
00:49:49,139 --> 00:49:54,189
And when the bird away does fly,
It's like an eagle in the sky.

209
00:49:54,191 --> 00:49:59,242
And when the sky begins to roar,
It's like a lion at the door.

210
00:49:59,243 --> 00:50:04,243
And when the door begins to crack,
It's like a stick across your back.

211
00:50:04,244 --> 00:50:09,479
And when your back begins to smart,
It's like a penknife in your heart.

212
00:50:09,480 --> 00:50:16,854
And when your heart begins to bleed,
you're dead, and dead, and dead indeed...

213
00:50:46,034 --> 00:50:49,235
I keep dying.

214
00:50:51,119 --> 00:50:54,070
Try again.

215
00:51:27,087 --> 00:51:29,921
Mom?

216
00:51:36,548 --> 00:51:39,831
Eric's staying over tonight.

217
00:51:42,300 --> 00:51:44,920
Hi.

218
00:51:47,177 --> 00:51:50,377
Can I get you anything?

219
00:52:06,266 --> 00:52:09,347
Hi, Mom.
Look at that little rabbit.

220
00:52:11,516 --> 00:52:14,800
It's lovely.
What's your name?

221
00:52:15,810 --> 00:52:18,513
I Dima, and that breast.

222
00:52:19,353 --> 00:52:23,217
-- Snow Lump or just Lump?
-- Just Lump.

223
00:52:40,734 --> 00:52:44,599
Well, we're off.
Bye, Tommy, see you.

224
00:52:52,112 --> 00:52:55,146
Wouldn't you like to come and
play at our house Dima?

225
00:52:57,614 --> 00:53:00,447
No, thank you.

226
00:53:07,366 --> 00:53:10,745
- I can smell it. What about you?
- I could.

227
00:53:10,746 --> 00:53:11,789
What did you smell?

228
00:53:11,618 --> 00:53:14,451
- The copy smell.
- What?

229
00:53:15,119 --> 00:53:17,822
Copy smell. Copies have a
weird smell.

230
00:53:18,620 --> 00:53:22,906
- Where did you hear that?
- Oh mom, everyone knows that.

231
00:53:22,830 --> 00:53:27,404
They smell like window cleaner.
It comes from their skin.

232
00:53:42,544 --> 00:53:45,246
So?

233
00:53:54,339 --> 00:53:57,871
Eric says that you invited
little Dima into your home.

234
00:53:59,340 --> 00:54:01,710
Yes.
Why?

235
00:54:04,758 --> 00:54:10,178
Look Rebecca, we don't have any
problem with human repliaction...

236
00:54:10,179 --> 00:54:13,155
...we think that some clones are decent people

237
00:54:13,156 --> 00:54:15,341
they have their own lives and rights...

238
00:54:15,637 --> 00:54:19,846
The thing is, it's too complicated
for our kids.

239
00:54:22,013 --> 00:54:26,223
Now we hear about these things in the news
all the time, but quite frankly...

240
00:54:26,223 --> 00:54:30,384
we do not want our children coming up
against these things first hand.

241
00:54:30,933 --> 00:54:33,967
Neither do you, I suppose.

242
00:54:43,686 --> 00:54:46,471
Dima is the victim of
artificial incest.

243
00:54:47,271 --> 00:54:51,017
Her mother gave birth to her own mother.

244
00:54:51,689 --> 00:54:54,771
Did you know that?

245
00:55:00,731 --> 00:55:05,732
So, if you'd known, you obviously wouldn't
have invited her into your home, would you?

246
00:55:16,238 --> 00:55:19,319
No, of course not.

247
00:55:27,366 --> 00:55:30,235
I'm gonna go now.

248
00:55:59,792 --> 00:56:02,874
Come on Eric.

249
00:56:10,642 --> 00:56:13,131
Come and give me a hand Tommy.

250
00:56:14,018 --> 00:56:16,970
It's your party after all.

251
00:56:33,232 --> 00:56:35,982
Hi.

252
00:57:20,162 --> 00:57:24,572
Molly, it's Rebecca. I just wanted to
make sure you got the invitation?

253
00:57:28,206 --> 00:57:31,075
But he...

254
00:57:44,837 --> 00:57:47,788
I see.

255
00:58:06,816 --> 00:58:08,892
Happy birthday, Tommy.

256
00:58:08,893 --> 00:58:09,935
- But the others aren't...
- There'll just be the two of us, my love.

257
00:58:15,053 --> 00:58:18,006
Two?
But why?

258
00:58:20,823 --> 00:58:25,958
- The others aren't coming.
- Why aren't they coming?

259
00:58:31,517 --> 00:58:34,266
Because of the copy?

260
00:58:34,809 --> 00:58:38,887
But we didn't invite the copy,
we didn't invite her on purpose.

261
00:58:43,319 --> 00:58:47,973
Why aren't they coming mom?
Please?

262
00:58:53,732 --> 00:58:57,145
- Mom!
- Because the mothers...

263
00:59:05,096 --> 00:59:07,930
Their mothers were not allowed.

264
00:59:08,899 --> 00:59:10,695
Why not?

265
00:59:10,987 --> 00:59:14,851
- Because they are stupid.
- Stupid?

266
01:00:09,420 --> 01:00:12,419
Where we going, Mom?

267
01:00:44,031 --> 01:00:47,340
This is exactly how I imagined it.

268
01:00:49,266 --> 01:00:52,348
- Do you like it?
- Yes.

269
01:00:54,392 --> 01:00:57,805
Cool, it's like the end of the world.

270
01:01:30,028 --> 01:01:32,860
Mom?

271
01:01:34,778 --> 01:01:38,062
Come quickly.

272
01:02:05,746 --> 01:02:08,698
I thought they were extinct long ago.

273
01:02:09,830 --> 01:02:12,782
Can I take one home?

274
01:02:14,915 --> 01:02:18,661
Home?
That's where we are, Tommy.

275
01:03:17,227 --> 01:03:21,215
Tommy?
I'm back.

276
01:03:26,644 --> 01:03:29,928
You can look.

277
01:03:43,397 --> 01:03:47,899
- Cool!
- What are you gonna call him?

278
01:03:51,319 --> 01:03:53,053
I don't know.

279
01:03:54,968 --> 01:03:56,851
Bingo?

280
01:03:58,529 --> 01:04:01,943
Bingo?
No.

281
01:04:08,581 --> 01:04:10,246
Booboo.

282
01:04:12,204 --> 01:04:14,851
- Really?
- Yeah.

283
01:04:17,827 --> 01:04:21,110
Thanks, Mom.

284
01:04:49,252 --> 01:04:52,335
Hi, I'm Mark.

285
01:04:54,921 --> 01:04:57,754
We are neighbors.

286
01:05:04,340 --> 01:05:08,003
- Can I see in your binoculars?
- Sure.

287
01:05:08,758 --> 01:05:11,710
Come on.

288
01:06:18,361 --> 01:06:21,444
And you there when Father died?

289
01:06:24,489 --> 01:06:27,322
Yes.

290
01:06:31,283 --> 01:06:35,028
- How did it happen?
- What made you think of it now?

291
01:06:35,825 --> 01:06:38,528
I do not know.

292
01:06:39,284 --> 01:06:42,367
You know, it was an accident.

293
01:06:46,578 --> 01:06:49,412
What sort of accident?

294
01:06:52,217 --> 01:06:56,576
We were on the road, we stopped,
he got out of the car...

295
01:06:57,248 --> 01:07:01,325
- ...and another car hit him.
- What sort of car?

296
01:07:04,834 --> 01:07:07,952
Van, Tommy, coming very fast.

297
01:07:08,502 --> 01:07:11,998
- What sort of van?
- I do not know.

298
01:07:14,711 --> 01:07:18,125
Why did you stop?

299
01:07:27,966 --> 01:07:31,830
- Why did you stop?
- I had to pee.

300
01:08:31,359 --> 01:08:34,891
Now I can do whatever I want with you.

301
01:08:37,861 --> 01:08:41,026
Go ahead.

302
01:08:49,114 --> 01:08:51,485
Tommy?

303
01:08:56,616 --> 01:08:59,450
Tommy?

304
01:10:57,985 --> 01:11:00,604
Good night, Mom.

305
01:11:15,156 --> 01:11:18,440
Good night, Tommy!

306
01:11:34,329 --> 01:11:37,530
Everything will be alright my love.

307
01:12:31,054 --> 01:12:33,922
Good morning!

308
01:12:36,224 --> 01:12:40,847
Mom...
Good morning!

309
01:12:41,682 --> 01:12:47,018
Weird. I was in university...

310
01:12:48,600 --> 01:12:51,932
...I was a rusty tin...

311
01:12:51,933 --> 01:12:56,602
...or not even a tin,
but some kind of container.

312
01:13:00,586 --> 01:13:04,761
I moved to creak, as the old job.

313
01:13:08,856 --> 01:13:14,096
When I lent down, this rusty sort of
liquid flow through my mouth.

314
01:13:16,442 --> 01:13:19,192
It was cool.

315
01:13:23,487 --> 01:13:26,687
Good morning!

316
01:13:30,738 --> 01:13:34,603
Mom, this is Monica.
Monica, mom.

317
01:13:35,573 --> 01:13:38,655
- Hi.
- Hi.

318
01:13:42,284 --> 01:13:45,317
What did you dream about?

319
01:13:47,368 --> 01:13:50,320
Nothing.

320
01:13:54,620 --> 01:13:57,821
Enjoy.

321
01:14:10,601 --> 01:14:13,744
Did we wake you when
we came in last night?

322
01:14:15,418 --> 01:14:18,500
- I wasn't asleep.
- But it was dawn.

323
01:14:19,294 --> 01:14:22,625
Did you ever sleep, Mom?

324
01:14:24,295 --> 01:14:27,247
Sometimes.

325
01:14:33,173 --> 01:14:36,587
Monica went out for couple of things,
she'll be back soon.

326
01:14:38,008 --> 01:14:41,753
She'll be living here for a while, I mean
of course only if you don't mind.

327
01:14:45,825 --> 01:14:46,797
Mom?

328
01:14:49,300 --> 01:14:51,631
She is a nice girl.

329
01:14:53,562 --> 01:14:56,969
Pleased to have her here.

330
01:15:11,184 --> 01:15:15,014
...after thought?
I love thinking about this.

331
01:15:15,169 --> 01:15:20,018
Imagine great snow storms over the oceans,
in the mountains, deep in the clouds...

332
01:15:20,019 --> 01:15:21,955
for millions of years on this planet,

333
01:15:21,956 --> 01:15:24,299
...billions of tiny snow crystals have
been whirling about...

334
01:15:24,300 --> 01:15:28,506
...and no two of the are same.
Just like us in fact.

335
01:15:28,741 --> 01:15:31,815
- As people?
- Yeah, as two.

336
01:15:31,791 --> 01:15:34,996
I was thinking of all living things,
not just our faces and our fingerprints.

337
01:15:34,997 --> 01:15:38,983
villus in our lungs, our irises,
the structure of our brains...

338
01:15:38,983 --> 01:15:40,836
...and nails, you know,
everything about us.

339
01:15:40,988 --> 01:15:44,782
I mean, think about, how long the birds here?
They came not long after dinasours.

340
01:15:44,783 --> 01:15:48,379
Basicly, way before us. Can you imagine,
how many billions of feathers they produce...

341
01:15:48,737 --> 01:15:52,068
From ostrich to terns, to hummingbirds, from penguins

342
01:15:52,488 --> 01:15:54,785
Everywhere in the world.
It must be an orbital thing.

343
01:15:54,786 --> 01:15:57,856
A billion times of billion times of billion, you know...

344
01:15:58,421 --> 01:16:02,361
There are no two alike.
This is a duck's feather.

345
01:16:02,991 --> 01:16:06,890
It's almost incomprehensible, billions
of amazingly complex patterns...

346
01:16:06,891 --> 01:16:09,776
...permanently whirling around us
and never repeat themselves.

347
01:16:13,768 --> 01:16:15,415
What's the matter?

348
01:16:15,416 --> 01:16:18,985
I don't know Tommy...
This is so...

349
01:16:21,254 --> 01:16:23,713
...so immature.

350
01:16:23,714 --> 01:16:27,808
- Really?
- Yes, really.

351
01:16:29,166 --> 01:16:30,961
So?

352
01:16:32,741 --> 01:16:36,720
- So what?
- Do you like kids?

353
01:17:17,379 --> 01:17:18,671
Come in.

354
01:17:19,948 --> 01:17:21,326
Hi Mom.

355
01:17:21,889 --> 01:17:24,592
Smells great, doesn't it?

356
01:17:24,940 --> 01:17:28,390
Monica baked it, I was the kitchen boy.

357
01:17:28,826 --> 01:17:32,217
- He is a very good kitchen boy.
- Thank you very much.

358
01:17:35,560 --> 01:17:38,393
Thanks.

359
01:17:42,270 --> 01:17:46,135
- I'm not hungry now, I'll have it later.
- Okay.

360
01:18:50,957 --> 01:18:53,576
Mom.

361
01:19:02,669 --> 01:19:05,751
Good night!

362
01:19:14,216 --> 01:19:15,824
I don't think she's been up for the day.

363
01:19:16,432 --> 01:19:20,373
- I think you should talk to her.
- And what shall I say it?

364
01:21:14,916 --> 01:21:17,452
Mom?

365
01:21:18,500 --> 01:21:21,582
It's you.

366
01:21:50,593 --> 01:21:53,342
Wait.

367
01:22:03,013 --> 01:22:05,965
- Hang on.
- Why?

368
01:22:15,016 --> 01:22:17,885
I have to take a shower.

369
01:22:20,102 --> 01:22:23,385
Let's take one together.

370
01:22:49,902 --> 01:22:53,103
Tommy, don't.

371
01:22:54,528 --> 01:22:57,610
No, no!
No.

372
01:23:04,865 --> 01:23:07,437
Stop it.

373
01:24:17,487 --> 01:24:21,507
- Wait.
- What? What's wrong?

374
01:24:22,763 --> 01:24:25,844
I think she woke up.

375
01:25:34,534 --> 01:25:37,616
Aren't you going to say anything?

376
01:26:18,296 --> 01:26:21,129
Hello.

377
01:26:28,716 --> 01:26:31,917
Can I help you?

378
01:26:52,765 --> 01:26:56,178
Go inside now Tommy.

379
01:27:03,310 --> 01:27:06,511
Who are you?

380
01:27:48,740 --> 01:27:51,145
Who is that, Mom?

381
01:28:04,036 --> 01:28:06,656
Mom.

382
01:29:11,972 --> 01:29:15,137
Who was that woman, Mom?

383
01:29:28,602 --> 01:29:31,684
Tommy?

384
01:29:36,229 --> 01:29:38,932
Tommy?

385
01:31:28,804 --> 01:31:32,005
I know her.

386
01:32:09,483 --> 01:32:12,435
Tommy?

387
01:32:20,743 --> 01:32:24,480
Let me in, please.

388
01:32:25,571 --> 01:32:28,984
You've been in there for hours.

389
01:32:30,948 --> 01:32:34,444
Please, open the door.

390
01:32:41,992 --> 01:32:44,944
I'm leaving.

391
01:33:51,512 --> 01:33:53,883
Mom?

392
01:34:01,057 --> 01:34:03,890
Mom?

393
01:34:34,192 --> 01:34:37,356
I have to tell you something.

394
01:34:42,702 --> 01:34:47,810
I was nine,
I was on the beach...

395
01:34:50,154 --> 01:34:53,237
It was so cold.

396
01:34:55,573 --> 01:34:58,607
- Then I met this boy?
- What, what, what boy?

397
01:34:58,943 --> 01:35:03,028
What beach? Who cares?
Who is this woman, Mother?

398
01:38:35,012 --> 01:38:38,426
No! No!
No!

399
01:39:08,938 --> 01:39:11,558
I'm scared.

400
01:39:15,526 --> 01:39:20,188
I do not know who you are.

401
01:39:23,308 --> 01:39:27,350
Or who I am either.

402
01:39:33,446 --> 01:39:36,777
Who are you, Mom?

403
01:39:39,364 --> 01:39:42,198
Who are you?

404
01:39:46,658 --> 01:39:49,740
Why did you do it?

405
01:39:51,284 --> 01:39:54,616
Why did you do this thing?

406
01:39:56,828 --> 01:40:01,970
- Look at yourself, Tommy, you're here.
- So? So? So?

407
01:40:02,228 --> 01:40:06,660
What am I supposed to do now?
What am I supposed to do with this fucking life of mine?

408
01:40:06,662 --> 01:40:09,340
Don't you see I have no idea who I am!

409
01:40:09,578 --> 01:40:12,703
Can't you understand?

410
01:40:14,291 --> 01:40:18,618
- What more could you want?
- More than what?

411
01:40:20,960 --> 01:40:24,705
You're here, Tommy.

412
01:40:26,169 --> 01:40:29,252
You're alive

413
01:40:30,004 --> 01:40:33,086
Take your hands off me.

414
01:40:34,172 --> 01:40:38,298
Take your hands off me.

415
01:40:51,010 --> 01:40:53,962
Who are you?

416
01:41:01,888 --> 01:41:05,006
Who are you?

417
01:41:07,098 --> 01:41:09,848
Who are you?

418
01:41:11,558 --> 01:41:14,510
Who are you?

419
01:44:50,139 --> 01:44:52,013
Thank you...

420
01:44:54,047 --> 01:44:56,731
...Rebecca.

