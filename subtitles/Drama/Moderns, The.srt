﻿1
00:00:00,000 --> 00:00:02,730
Would you people excuse Hart and I
a few minutes?

2
00:00:02,803 --> 00:00:05,328
We have an urgent matter to discuss.

3
00:00:07,975 --> 00:00:09,374
Very modern.

4
00:00:11,979 --> 00:00:14,539
No, Armand can stay.

5
00:00:16,550 --> 00:00:18,541
Armand speaks no English.

6
00:00:20,487 --> 00:00:23,718
<i>Indeed, le petit coquin</i> rarely speaks at all.

7
00:00:30,731 --> 00:00:33,291
The other night with you was wonderful.

8
00:00:34,635 --> 00:00:37,866
But there's a complication.

9
00:00:41,508 --> 00:00:44,944
My husband died unexpectedly
over the weekend.

10
00:00:49,083 --> 00:00:51,984
As you can imagine,
it was a terrible shock.

11
00:00:52,986 --> 00:00:56,080
- We were very close.
- Right.

12
00:00:56,824 --> 00:00:58,451
A perfect couple.

13
00:01:00,928 --> 00:01:02,259
I'm sorry.

14
00:01:05,165 --> 00:01:07,963
You won't have to copy
those paintings now.

15
00:01:10,938 --> 00:01:12,098
I see.

16
00:01:15,809 --> 00:01:17,777
I knew you'd understand.

17
00:01:18,846 --> 00:01:21,041
There is one hitch, Nathalie.

18
00:01:22,382 --> 00:01:26,785
You and I had an arrangement.
I expect you to fulfill it.

19
00:01:28,856 --> 00:01:30,619
But I intend to.

20
00:01:33,393 --> 00:01:35,088
I intend to.

21
00:01:36,396 --> 00:01:40,389
What would you like most?
A suit, or a watch?

22
00:01:41,902 --> 00:01:44,200
A weekend together by the sea?

23
00:01:45,405 --> 00:01:47,771
Because that was
part of the arrangement, too.

24
00:01:47,975 --> 00:01:49,237
Remember?

25
00:01:51,678 --> 00:01:54,806
Enjoying opportunities
as they present themselves...

26
00:01:55,082 --> 00:01:57,607
...and not losing
what's most important to us.

27
00:01:57,684 --> 00:02:01,586
Exactly.
Which is why you're going to pay, Nathalie.

28
00:02:01,755 --> 00:02:03,985
In full. In cash.

29
00:02:05,092 --> 00:02:07,788
Or you'll never see your pictures again.

30
00:02:09,363 --> 00:02:11,490
Don't you threaten me, Hart.

31
00:02:12,299 --> 00:02:15,359
I will never be threatened again by anyone!

32
00:02:17,504 --> 00:02:22,203
You're behaving like a cheap whore from
the <i>rue Saint-Denis,</i> and not like an artist.

33
00:02:22,609 --> 00:02:24,338
I'm ashamed of you.

34
00:02:26,613 --> 00:02:27,773
<i>Brute.</i>

35
00:02:31,652 --> 00:02:32,812
Brute.

36
00:03:00,814 --> 00:03:05,547
So who is this Libby Valentin?
I mean, is she French or what?

37
00:03:05,886 --> 00:03:07,376
She's "or what."

38
00:03:07,754 --> 00:03:10,120
Is she really a nun?

39
00:03:10,190 --> 00:03:11,714
Defrocked.

40
00:03:12,259 --> 00:03:16,662
You know, I think it was Gustave Flaubert
who said there are only four reasons...

41
00:03:16,730 --> 00:03:19,130
...for getting involved with women.

42
00:03:19,733 --> 00:03:21,325
- Sex!
- One.

43
00:03:21,401 --> 00:03:22,868
- Money!
- Two.

44
00:03:23,136 --> 00:03:25,036
- Publicity!
- Three.

45
00:03:25,105 --> 00:03:27,733
Goddamn it, what was the fourth one?

46
00:03:27,875 --> 00:03:30,867
- Drunkenness.
- Drunkenness.

47
00:03:33,547 --> 00:03:38,075
Buddy, Rachel's not for you.
She's not gonna make you happy.

48
00:03:38,418 --> 00:03:39,510
Yeah.

49
00:03:40,387 --> 00:03:42,150
I mean, in Hollywood...

50
00:03:42,222 --> 00:03:45,419
...the women are gonna be tall.
- There's a reason.

51
00:03:45,492 --> 00:03:47,517
- They're blonde.
- There's another reason.

52
00:03:47,594 --> 00:03:50,563
Promiscuity is just a religion with them.

53
00:03:52,332 --> 00:03:55,233
Oiseau, you know, you are absolutely right.

54
00:03:56,370 --> 00:04:00,067
- No permanent attachments.
- That's right, buddy.

55
00:04:00,140 --> 00:04:01,835
Do you realize...

56
00:04:02,442 --> 00:04:04,034
...that drink...

57
00:04:04,278 --> 00:04:07,543
...and permanent attachments
have been the undoing of virtually...

58
00:04:07,614 --> 00:04:08,945
...all of our friends?

59
00:04:09,016 --> 00:04:11,746
- That's right.
- Hit it.

60
00:04:12,019 --> 00:04:16,479
"She's got a Sunday school face

61
00:04:23,330 --> 00:04:25,059
"She's got..."

62
00:04:25,132 --> 00:04:26,690
She's got a what?

63
00:04:26,767 --> 00:04:28,291
"She's got a...

64
00:04:34,775 --> 00:04:38,142
"She's got a Sunday school face"

65
00:04:43,817 --> 00:04:46,877
- Wonderful song.
- I know that car.

66
00:04:48,388 --> 00:04:49,912
Get some sleep.

67
00:04:55,062 --> 00:04:56,723
Monsieur Hart.

68
00:05:14,715 --> 00:05:15,909
Shit!

69
00:05:47,080 --> 00:05:48,547
Nathalie.

70
00:06:06,733 --> 00:06:07,893
Libby!

71
00:06:12,606 --> 00:06:15,939
I disclaim all responsibility.

72
00:06:16,843 --> 00:06:19,539
Come here.
I want to show you something.

73
00:06:22,582 --> 00:06:26,450
- Lf I didn't know they were copies...
- What makes you think they're copies?

74
00:06:26,520 --> 00:06:28,920
Because I've seen the originals.

75
00:06:30,157 --> 00:06:32,819
Nathalie asked me to ship them
to New York.

76
00:06:32,926 --> 00:06:35,554
I suppose you put my fee in your pocket.

77
00:06:36,229 --> 00:06:37,628
<i>Jamais.</i>

78
00:06:38,465 --> 00:06:41,832
<i>Sur mon honneur,</i> as a woman of God.

79
00:06:44,971 --> 00:06:46,871
These are the originals.

80
00:06:47,641 --> 00:06:50,132
Nathalie took my copies by mistake.

81
00:06:53,680 --> 00:06:55,841
She did?

82
00:07:01,988 --> 00:07:04,980
A little bit like stabbing yourself
in the back, isn't it?

83
00:07:09,529 --> 00:07:13,090
So what are we supposed to do
with these?

84
00:07:13,800 --> 00:07:15,700
I'll cut you in for 30 percent.

85
00:07:25,712 --> 00:07:28,203
It's a crackerjack scheme, dear boy.

86
00:07:28,381 --> 00:07:31,646
But now leave everything to me.

87
00:07:31,718 --> 00:07:33,345
When it comes to <i>artifice...</i>

88
00:07:33,420 --> 00:07:36,583
<i>...moi,</i> Valentin, am a master.

89
00:07:38,258 --> 00:07:42,786
Stone will pay through his nose
to get these in his <i>salon.</i>

90
00:07:42,863 --> 00:07:46,264
Should have heard him on the telephone.
Beside himself.

91
00:07:46,766 --> 00:07:50,725
I'll get the money he owes me,
you'll get the money I owe you...

92
00:07:50,804 --> 00:07:54,740
...and when Nathalie de Ville sees
these new beauties...

93
00:07:54,808 --> 00:07:59,245
...at Stone's exhibition,
the Pope himself would blush with shame.

94
00:07:59,446 --> 00:08:03,246
Perhaps this one, then,
which again is in keeping...

95
00:08:03,316 --> 00:08:06,376
...with the whole anticipation
concept we discussed.

96
00:08:06,453 --> 00:08:11,015
I particularly like this one with, as you see,
the focus on the lady...

97
00:08:11,091 --> 00:08:14,219
No, I don't like any of these.

98
00:08:16,096 --> 00:08:20,055
I need an ad that, when you look at it,
would jump at you.

99
00:08:20,734 --> 00:08:23,498
Something simple but dramatic.

100
00:08:23,703 --> 00:08:25,102
Jump. Yes.

101
00:08:36,583 --> 00:08:37,641
<i>Oui?</i>

102
00:08:41,354 --> 00:08:43,549
- <i>Oui.</i>
- <i>Et madame?</i>

103
00:08:45,659 --> 00:08:47,286
<i>Entrez, entrez.</i>

104
00:08:50,430 --> 00:08:51,692
Valentin.

105
00:09:00,607 --> 00:09:03,940
- Libby, I changed my mind.
- About what?

106
00:09:04,010 --> 00:09:07,571
- I don't want to show anything of mine.
- Okay.

107
00:09:07,714 --> 00:09:09,682
I'll just pick it up when we go.

108
00:09:11,151 --> 00:09:12,948
I don't want you...

109
00:09:13,019 --> 00:09:15,146
...to participate in this.

110
00:09:19,025 --> 00:09:20,322
<i>Merci.</i>

111
00:09:23,263 --> 00:09:26,460
- May I call you on Friday?
- Yes.

112
00:09:27,434 --> 00:09:29,459
- Thank you again.
- Thank you.

113
00:09:29,803 --> 00:09:31,134
Excuse me.

114
00:09:33,206 --> 00:09:35,071
What is he doing here?

115
00:09:35,308 --> 00:09:37,936
It was Hart who put me in touch...

116
00:09:38,011 --> 00:09:40,912
...with the owner <i>de ces ouvres.</i>

117
00:09:41,214 --> 00:09:43,114
He wanted to come along.

118
00:09:44,217 --> 00:09:47,653
Did you bring me
some samples of your work?

119
00:09:48,989 --> 00:09:53,949
I've decided I prefer not to be collected.
Makes me feel like a manufacturer.

120
00:09:55,228 --> 00:09:57,662
You're in the wrong business, then.

121
00:09:58,999 --> 00:10:01,126
- Got anything to drink?
- No.

122
00:10:01,234 --> 00:10:03,930
My wife is an alcoholic.

123
00:10:07,941 --> 00:10:10,876
- How is your wife?
- What do you mean by that?

124
00:10:12,145 --> 00:10:13,407
Not much.

125
00:10:13,780 --> 00:10:17,079
- Do you want to see them?
- Yes! Let's see it!

126
00:10:55,855 --> 00:10:58,050
Look at the skin tone.

127
00:10:58,458 --> 00:11:00,221
The shape.

128
00:11:07,634 --> 00:11:09,499
So sensual.

129
00:11:12,238 --> 00:11:16,299
Did you know that Modigliani's
models were tarts?

130
00:11:18,278 --> 00:11:20,473
All women are tarts.

131
00:11:22,415 --> 00:11:24,280
That's why I love them.

132
00:11:25,185 --> 00:11:26,709
I think...

133
00:11:28,054 --> 00:11:30,249
...you could easily say...

134
00:11:30,590 --> 00:11:33,991
...that these three works capture...

135
00:11:34,060 --> 00:11:35,584
<i>...capture...</i>

136
00:11:36,129 --> 00:11:39,997
...the true <i>spirit, I'essence de modernit.</i>

137
00:11:40,900 --> 00:11:45,337
- I'm prepared to give you a bargain price.
- Yes.

138
00:11:45,739 --> 00:11:49,175
I'm informed that
you specialize in bargains.

139
00:11:49,576 --> 00:11:53,342
I have a certain talent in that area, yes.

140
00:11:53,413 --> 00:11:58,282
But am I to assume
that money is an object?

141
00:11:58,351 --> 00:11:59,875
Don't be daft.

142
00:11:59,953 --> 00:12:02,945
Money is always an object.

143
00:12:04,691 --> 00:12:08,457
For myself, I'm only interested in price.

144
00:12:09,596 --> 00:12:11,223
See that Picabia?

145
00:12:12,499 --> 00:12:16,401
I could burn it tomorrow,
and it wouldn't mean a thing to me.

146
00:12:17,837 --> 00:12:21,273
But I did pay 7,000 francs for it...

147
00:12:21,541 --> 00:12:25,671
...so I am not as impulsive
as I ordinarily might be.

148
00:12:26,446 --> 00:12:29,142
In fact, if I'd paid less...

149
00:12:29,582 --> 00:12:31,607
...it would be less beautiful.

150
00:12:33,586 --> 00:12:35,383
Stone, I've changed my mind.

151
00:12:35,822 --> 00:12:38,950
I think it would be an honor
for you to own some of my work.

152
00:12:43,163 --> 00:12:46,655
I'll just run over to the gallery
and pick something up.

153
00:13:13,092 --> 00:13:14,821
"It rained today.

154
00:13:16,463 --> 00:13:19,057
"Even the rain seems like a lie here.

155
00:13:20,867 --> 00:13:24,928
"I detest the verisimilitude of Parisian life."

156
00:13:50,730 --> 00:13:53,324
- Bertie's downstairs.
- Rachel.

157
00:13:54,234 --> 00:13:56,964
I decided to go to Hollywood with Oiseau.

158
00:13:57,704 --> 00:14:01,140
- Yes?
- Yes. You're coming with me.

159
00:14:01,674 --> 00:14:02,766
Am I?

160
00:14:03,576 --> 00:14:04,804
You are.

161
00:14:15,054 --> 00:14:16,214
Not in the tub.

162
00:14:16,289 --> 00:14:18,655
- Why not?
- Trust me.

163
00:14:31,671 --> 00:14:33,070
I don't know.

164
00:14:33,139 --> 00:14:36,040
I think I need
more of a commitment from you.

165
00:14:37,110 --> 00:14:38,702
Yeah? How long?

166
00:14:53,560 --> 00:14:55,585
Are these genuine?

167
00:14:56,829 --> 00:14:58,922
<i>Est-ce que le ciel est bleu?</i>

168
00:14:59,966 --> 00:15:02,196
Ordinarily, I wouldn't care.

169
00:15:02,835 --> 00:15:05,702
If a picture sells for a big price...

170
00:15:05,772 --> 00:15:09,037
...and it's praised to the teeth
by the experts...

171
00:15:10,543 --> 00:15:12,408
...is it really worthless, then...

172
00:15:12,478 --> 00:15:15,606
...when it's discovered
the signature is forged?

173
00:15:15,748 --> 00:15:19,206
Depends on whether
you're the buyer or the seller.

174
00:15:20,587 --> 00:15:24,250
All the critics and art snobs will be
at my salon.

175
00:15:24,524 --> 00:15:27,118
- I want to make an impression.
- Yes.

176
00:15:27,193 --> 00:15:29,320
- Not a reputation.
- No.

177
00:15:29,729 --> 00:15:31,492
I already have that.

178
00:15:38,871 --> 00:15:40,031
Now.

179
00:15:41,608 --> 00:15:43,405
The interesting part.

180
00:15:45,645 --> 00:15:46,907
How much?

181
00:15:49,449 --> 00:15:50,848
<i>Une chanson...</i>

182
00:15:51,451 --> 00:15:52,816
<i>... 5,000...</i>

183
00:15:53,686 --> 00:15:55,176
<i>... amricains.</i>

184
00:15:56,923 --> 00:15:58,550
I'll take them.

185
00:15:58,858 --> 00:16:01,383
- You will?
- But not at that price.

186
00:16:03,296 --> 00:16:07,027
I've already given you
the dealer's discount.

187
00:16:10,303 --> 00:16:12,430
I'll give you $3,000.

188
00:16:12,905 --> 00:16:15,373
You insult me, Mr. Stone.

189
00:16:15,675 --> 00:16:17,074
Not you.

190
00:16:18,244 --> 00:16:20,872
- Take it or leave it.
- I'll take it.

191
00:16:21,447 --> 00:16:23,472
I thought you might.

192
00:16:26,152 --> 00:16:28,177
- Good.
- Good.

193
00:16:34,661 --> 00:16:36,561
Perfect timing.

194
00:16:43,369 --> 00:16:45,030
What have we here?

195
00:16:49,409 --> 00:16:50,899
Distinguished.

196
00:16:54,781 --> 00:16:58,273
- I'm sorry, Bertie. I didn't know...
- Rachel, come here.

197
00:16:59,585 --> 00:17:02,679
- <i>Allô,</i> Rachel.
- Hello, Libby.

198
00:17:04,857 --> 00:17:06,984
What do you think of Hart's painting?

199
00:17:08,661 --> 00:17:11,061
Yeah. I'd like to know.

200
00:17:13,032 --> 00:17:14,499
- Hello.
- Afternoon.

201
00:17:15,635 --> 00:17:17,034
It's nice.

202
00:17:17,704 --> 00:17:19,103
Should I buy it?

203
00:17:20,973 --> 00:17:22,838
If you like it, Bertie.

204
00:17:26,412 --> 00:17:29,973
Yes, I think I have just the right spot for it.

205
00:17:30,717 --> 00:17:33,584
- I'll give you $100.
- Done.

206
00:17:54,140 --> 00:17:55,164
<i>I did it.</i>

207
00:17:55,341 --> 00:17:57,138
Did it, dear boy.

208
00:17:57,477 --> 00:18:01,174
We started at $2,500,
and I suckered him up to $3,000.

209
00:18:02,682 --> 00:18:05,776
- Let's go to the bank.
- No, let's go to the bar.

210
00:18:14,594 --> 00:18:16,789
<i>We have to talk about something.</i>

211
00:18:17,897 --> 00:18:19,922
<i>I'm not interested.</i>

212
00:18:20,299 --> 00:18:23,234
What are you interested in,
besides yourself?

213
00:18:23,870 --> 00:18:25,132
Not much.

214
00:18:39,118 --> 00:18:40,710
But I'm looking.

215
00:18:59,872 --> 00:19:02,932
Writing about yourself can be very lonely.

216
00:19:03,009 --> 00:19:05,807
- What are you writing?
- It's my obituary.

217
00:19:06,212 --> 00:19:10,546
It's too important a job to leave
to those hacks in the obituary department.

218
00:19:10,616 --> 00:19:12,777
Why don't you just quit
the goddamn paper?

219
00:19:12,852 --> 00:19:16,549
This'll interest you. I've decided on poison.

220
00:19:16,856 --> 00:19:21,725
Drowning was my second choice.
I mean, it's better than asphyxiation.

221
00:19:22,895 --> 00:19:25,295
I saw a man once who'd been hung.

222
00:19:25,765 --> 00:19:28,359
He looked a bit disappointed.

223
00:19:28,434 --> 00:19:31,267
You can't kill yourself, Oiseau.
We're going to Hollywood.

224
00:19:31,337 --> 00:19:34,704
I'm gonna tell you why Carlos beat you
in the ring a couple days ago.

225
00:19:34,774 --> 00:19:37,402
- Who?
- Carlos. Stone.

226
00:19:37,476 --> 00:19:40,138
I'm working on something.
It doesn't matter.

227
00:19:40,213 --> 00:19:43,011
- Right, let's have it.
- Scruples.

228
00:19:43,082 --> 00:19:47,610
- Scruples?
- You can't win Andrea with scruples.

229
00:19:47,687 --> 00:19:49,154
- Who?
- Andrea.

230
00:19:49,222 --> 00:19:51,213
Rachel. Forget it.

231
00:19:51,490 --> 00:19:55,950
I'm sitting there, watching him
waltz around you like he owns you.

232
00:19:56,028 --> 00:19:58,656
He doesn't own me. He does not own me.

233
00:19:58,731 --> 00:20:01,097
He thinks because he buys something,
he owns it.

234
00:20:01,167 --> 00:20:03,260
It's the same to him.
A piece of ass, property.

235
00:20:03,336 --> 00:20:04,564
It's about ownership.

236
00:20:04,637 --> 00:20:07,765
He doesn't own me, he doesn't own Rachel,
and he never will.

237
00:20:07,840 --> 00:20:10,741
Guys like that are bad for all of us, Clark.

238
00:20:10,810 --> 00:20:12,141
- Nick.
- Nick.

239
00:20:12,211 --> 00:20:13,303
Who?

240
00:20:13,779 --> 00:20:17,078
You know,
writing about yourself can be very lonely.

241
00:20:18,351 --> 00:20:21,377
- What are you writing?
- It's my obituary.

242
00:20:21,888 --> 00:20:26,382
It's too important a job to leave
to those hacks in the obituary department.

243
00:20:26,459 --> 00:20:29,257
Why don't you quit that goddamn paper
like I did?

244
00:20:29,328 --> 00:20:32,388
This'll interest you. I've decided on pills.

245
00:20:32,465 --> 00:20:33,557
He doesn't own me.

246
00:20:33,633 --> 00:20:35,032
- Who?
- Stone.

247
00:20:35,167 --> 00:20:37,533
- Carlos.
- Who?

248
00:21:06,799 --> 00:21:09,029
Sex is all about power.

249
00:21:09,468 --> 00:21:11,561
<i>Champagne, monsieur?</i>

250
00:21:12,738 --> 00:21:14,968
You can't help but make a mess.

251
00:21:17,843 --> 00:21:22,371
- What do you mean?
- Take Nathalie de Ville, for instance.

252
00:21:24,483 --> 00:21:27,850
She's as cool as
somebody else's cucumber.

253
00:21:27,954 --> 00:21:29,148
Armand.

254
00:21:41,834 --> 00:21:42,994
<i>Merci.</i>

255
00:21:52,812 --> 00:21:54,279
<i>Les critiques.</i>

256
00:22:02,054 --> 00:22:04,545
You look stunning tonight, my dear.

257
00:22:05,791 --> 00:22:07,691
I look stunning every night, Bertie.

258
00:22:07,760 --> 00:22:10,228
Have you had a chance
to look over my collection?

259
00:22:10,296 --> 00:22:12,321
Come, I'll give you a tour.

260
00:22:12,665 --> 00:22:14,360
My latest acquisition...

261
00:22:14,433 --> 00:22:17,596
...should impress even someone
as formidable as you.

262
00:22:19,071 --> 00:22:22,131
Bertie. You really did it.

263
00:22:24,744 --> 00:22:29,113
Outside of a museum, I've never seen
such a fascinating collection of pictures.

264
00:22:29,181 --> 00:22:30,443
A museum?

265
00:22:33,919 --> 00:22:36,285
- Oh, l...
- It doesn't matter.

266
00:22:37,590 --> 00:22:39,251
What do you think?

267
00:22:42,795 --> 00:22:45,730
Get a hold of yourself. It's only a painting.

268
00:22:46,899 --> 00:22:49,993
These are forgeries, Bertie.
They simply have to be.

269
00:22:50,069 --> 00:22:54,506
Nonsense. I bought them from the estate
of the late <i>Comte de Polignac.</i>

270
00:22:54,573 --> 00:22:56,700
Then you bought forgeries.

271
00:22:57,676 --> 00:23:00,406
I own the originals
of these three paintings.

272
00:23:03,516 --> 00:23:06,952
In fact, I shipped them to America
only the other day.

273
00:23:09,889 --> 00:23:13,689
And Bertie,
my papers have been authenticated.

274
00:23:15,461 --> 00:23:17,053
You've been had.

275
00:23:39,685 --> 00:23:43,143
That painting is no more of a fake
than you are.

276
00:23:50,930 --> 00:23:53,956
Look at the nipples in the Matisse.

277
00:23:56,335 --> 00:24:00,066
Henri would have never painted nipples
in that manner.

278
00:24:01,273 --> 00:24:02,900
They look like cones...

279
00:24:03,542 --> 00:24:04,804
...or warts.

280
00:24:09,215 --> 00:24:13,311
Perhaps I should have framed
the money I spent on them instead.

281
00:24:16,422 --> 00:24:19,823
- Bertie, I'm afraid you've been had.
- Shut up!

282
00:24:26,966 --> 00:24:29,526
Monsieur Stone, I think we've had enough.

283
00:24:30,302 --> 00:24:32,896
No! I've had enough.

284
00:24:34,874 --> 00:24:39,777
I don't give a tuppenny damn for
your silly opinions on the value of art.

285
00:24:40,246 --> 00:24:43,545
There is no value,
except what I choose to put on it.

286
00:24:44,617 --> 00:24:47,051
This is art...

287
00:24:47,119 --> 00:24:49,713
...because I paid hard cash for it.

288
00:24:51,090 --> 00:24:52,751
Don't you understand?

289
00:24:52,825 --> 00:24:56,124
Your precious painters
mean nothing to me.

290
00:24:57,329 --> 00:25:00,628
I could have Nathalie's mutt
shit on a canvas.

291
00:25:01,000 --> 00:25:03,901
And if I paid $5,000 for it...

292
00:25:04,069 --> 00:25:07,300
...you critics would call it a masterpiece.

293
00:25:07,740 --> 00:25:12,575
I'm afraid not, monsieur Stone.
I'm afraid you have missed the entire point.

294
00:25:12,778 --> 00:25:14,905
- Have I?
- Yes.

295
00:25:15,147 --> 00:25:17,513
Fakes is what we have called them...

296
00:25:17,583 --> 00:25:21,075
...and fakes is what they are,
fit only for the fire.

297
00:25:23,389 --> 00:25:24,651
The fire?

298
00:25:40,940 --> 00:25:44,000
I see what you mean about the nipple.

299
00:26:05,097 --> 00:26:06,428
<i>Au revoir.</i>

300
00:26:27,753 --> 00:26:30,813
- Laloux?
- <i>Oui, monsieur.</i>

301
00:26:30,889 --> 00:26:33,858
- Take that picture down.
- <i>Bien, monsieur.</i>

302
00:26:34,893 --> 00:26:38,761
- Look at this nipple.
- This one is not so bad.

303
00:26:38,831 --> 00:26:39,957
It's worse.

304
00:26:41,033 --> 00:26:43,695
- The shape, the form...
- The shape is...

305
00:26:43,769 --> 00:26:46,135
- It's all wrong.
- But the color...

306
00:27:11,063 --> 00:27:12,462
The masterpiece.

307
00:27:49,735 --> 00:27:53,034
I should throw all of you in as well.

308
00:27:59,878 --> 00:28:04,611
- Looking for the toilet, Hem?
- Art is never the whole story.

309
00:28:06,985 --> 00:28:08,475
It's upstairs.

310
00:28:39,017 --> 00:28:41,417
There's more to you than meets the eye.

311
00:28:41,720 --> 00:28:46,248
Piss off, or I'll barbecue your dog.

312
00:29:05,677 --> 00:29:10,239
What happened, Bertie?
Didn't anybody like your paintings?

313
00:29:14,953 --> 00:29:16,420
You're drunk.

314
00:29:18,891 --> 00:29:21,951
Did you really think
you could buy your way in?

315
00:29:28,767 --> 00:29:31,099
I feel so sorry for you, Bertie.

316
00:29:45,050 --> 00:29:46,711
Clean up the mess.

317
00:29:49,121 --> 00:29:50,452
All of it.

318
00:29:58,230 --> 00:30:00,061
I'm going out.

319
00:30:20,786 --> 00:30:23,220
- <i>Bonjour.</i>
- <i>Merci.</i>

320
00:30:25,624 --> 00:30:26,613
What?

321
00:30:27,926 --> 00:30:28,915
No!

322
00:30:31,563 --> 00:30:33,861
The son of a bitch actually did it.

323
00:30:38,737 --> 00:30:41,137
You know,
he kept telling me he was gonna do it.

324
00:30:42,841 --> 00:30:44,809
I thought he was just...

325
00:30:47,513 --> 00:30:49,071
I didn't believe him.

326
00:30:49,615 --> 00:30:51,378
I believed him.

327
00:30:52,918 --> 00:30:56,251
It makes me feel like a coward, you know?

328
00:30:59,024 --> 00:31:01,754
But we're not the cowards, are we, Hart?

329
00:31:03,395 --> 00:31:05,989
The dead are the real cowards, right?

330
00:31:10,435 --> 00:31:11,902
I don't know.

331
00:31:17,809 --> 00:31:20,369
The dead are not the cowards.

332
00:32:12,230 --> 00:32:13,424
Rachel?

333
00:32:15,867 --> 00:32:17,630
Rachel, is that you?

334
00:32:21,974 --> 00:32:23,202
It's me.

335
00:32:27,746 --> 00:32:31,204
- You son of a bitch!
- It was the only thing I could do.

336
00:32:31,617 --> 00:32:34,051
I'm dead, and I'll soon be gone.

337
00:32:34,553 --> 00:32:37,147
And now I can be anybody I want to be.

338
00:32:39,958 --> 00:32:42,153
Jesus! I need a drink.

339
00:32:43,729 --> 00:32:46,197
Did you see my obituaries?
They're all raves.

340
00:32:46,264 --> 00:32:49,165
And I'm gonna be buried tomorrow
afternoon at Pere-Lachaise.

341
00:32:49,234 --> 00:32:51,634
Racine has made me
the most charming coffin.

342
00:32:51,703 --> 00:32:54,763
- I think we ought to look in.
- Naturally.

343
00:32:54,840 --> 00:32:58,276
We've got plenty of time.
The train doesn't leave till 4:00.

344
00:32:58,343 --> 00:33:00,072
<i>Aprs vous, madame.</i>

345
00:33:00,746 --> 00:33:04,307
Could I lean on you?
These heels are just killing me.

346
00:33:05,050 --> 00:33:06,415
Yeah, sure.

347
00:33:08,220 --> 00:33:11,121
We'll have a beautiful boat ride
to New York.

348
00:33:11,189 --> 00:33:13,623
And then Hollywood!

349
00:33:13,692 --> 00:33:17,924
- I'm just dying to get there.
- I ought to kill you myself.

350
00:33:30,609 --> 00:33:31,803
No.

351
00:33:32,277 --> 00:33:35,371
Really, we can't go in there.
I'll be recognized.

352
00:33:35,447 --> 00:33:37,847
Why don't we go
and have a <i>tasse de verveine?</i>

353
00:33:37,916 --> 00:33:42,376
- No, I need a drink.
- All right. I'll come by at 1:00 tomorrow.

354
00:33:42,454 --> 00:33:46,550
- I'll pick you up in a taxi.
- All right. Don't talk to strangers.

355
00:34:06,244 --> 00:34:08,974
Since we're living in Paris,
does that make us Parisian?

356
00:34:13,752 --> 00:34:16,152
Pal. How you doing?

357
00:34:20,058 --> 00:34:23,027
I think I miss the little bastard.

358
00:34:25,630 --> 00:34:27,757
Yeah, I know what you mean.

359
00:34:28,900 --> 00:34:31,095
You know what time it is?

360
00:34:32,571 --> 00:34:36,871
Time? Too late. That's the time.

361
00:34:38,043 --> 00:34:39,510
He loved you.

362
00:34:40,479 --> 00:34:43,846
I didn't even like him, but he loved you.

363
00:34:44,750 --> 00:34:47,981
"I love you, I swear I do

364
00:34:48,086 --> 00:34:51,351
"Even if I can't find the right words

365
00:34:51,690 --> 00:34:54,523
"Believe me, it is true

366
00:34:54,593 --> 00:34:56,288
"I love you

367
00:34:58,930 --> 00:35:01,296
"Believe me, it is true

368
00:35:01,366 --> 00:35:03,300
"I love you

369
00:35:06,171 --> 00:35:08,298
"Believe me, it is true

370
00:35:08,373 --> 00:35:10,341
"I love you"

371
00:37:37,522 --> 00:37:40,252
Rachel. Where the hell have you...

372
00:37:42,527 --> 00:37:44,256
Don't worry, Nicky.

373
00:37:44,396 --> 00:37:48,127
At least, when they find us,
our skeletons will be embracing.

374
00:37:58,710 --> 00:38:01,770
I forgot to tell you. Oiseau is still alive.

375
00:38:02,080 --> 00:38:03,809
I didn't know he was dead.

376
00:38:03,882 --> 00:38:07,841
He's gonna meet us at the boat train
tomorrow at 4:00, right after his funeral.

377
00:38:08,687 --> 00:38:11,383
- I thought you said he was alive.
- Ain't it great?

378
00:38:11,456 --> 00:38:12,480
Hart.

379
00:38:12,557 --> 00:38:15,651
- Libby, what are you doing here?
- I shadowed you here.

380
00:38:15,727 --> 00:38:19,527
Stone's men came into the gallery,
slashed all the paintings.

381
00:38:19,597 --> 00:38:23,158
Not just your paintings,
but valuable ones as well.

382
00:38:23,802 --> 00:38:24,894
What?

383
00:38:25,503 --> 00:38:27,494
I never expected violence.

384
00:38:28,173 --> 00:38:29,606
I'm leaving town.

385
00:38:31,977 --> 00:38:33,376
Taxi!

386
00:38:37,148 --> 00:38:38,877
He's gonna kill me.

387
00:38:40,285 --> 00:38:41,752
Can't they take a joke?

388
00:38:41,820 --> 00:38:45,586
This place, completely uncivilized.

389
00:38:52,564 --> 00:38:54,589
<i>Cannes. Rapidement.</i>

390
00:39:03,908 --> 00:39:06,706
- Told you he was crazy.
- I don't want to talk about him.

391
00:39:06,778 --> 00:39:08,973
- Let's go upstairs.
- We can't stay here.

392
00:39:09,047 --> 00:39:11,208
- Why not?
- It's the first place he'll look.

393
00:39:11,282 --> 00:39:13,750
- I don't give a damn.
- Come on, we'll get a hotel.

394
00:39:48,253 --> 00:39:50,278
Get in the car, Rachel.

395
00:39:53,925 --> 00:39:55,790
She's staying with me.

396
00:39:58,596 --> 00:40:00,223
You had your fun.

397
00:40:00,799 --> 00:40:02,460
Now let's go home.

398
00:40:03,668 --> 00:40:07,365
- It's not that simple, Bertie.
- Come on.

399
00:40:10,075 --> 00:40:11,906
I need you, Rachel.

400
00:40:15,146 --> 00:40:17,376
You don't need anybody, Bertie.

401
00:40:18,349 --> 00:40:20,874
I'm going back to America with Nicky.

402
00:40:21,352 --> 00:40:24,719
- Don't be stupid.
- I'm not stupid.

403
00:40:33,731 --> 00:40:35,221
She's my wife.

404
00:40:36,968 --> 00:40:40,426
Nicky and I were married a long time ago,
before you and me.

405
00:40:40,505 --> 00:40:42,405
We were never divorced.

406
00:40:45,343 --> 00:40:46,833
Sorry, Bertie.

407
00:40:51,583 --> 00:40:52,982
I knew that!

408
00:40:56,387 --> 00:40:58,480
I know everything about you.

409
00:41:02,127 --> 00:41:04,459
You don't know anything about me.

410
00:41:04,529 --> 00:41:08,056
What are you laughing at? Get in the car!

411
00:41:08,299 --> 00:41:10,233
What are you gonna do? Kill me?

412
00:41:12,470 --> 00:41:13,960
Then kill her.

413
00:41:14,539 --> 00:41:16,700
- Go ahead, kill me.
- You slut.

414
00:41:16,774 --> 00:41:19,140
- Go ahead. I want you to.
- Maybe I should.

415
00:41:19,210 --> 00:41:21,337
- You want me to kill you?
- Come on, do it.

416
00:41:21,412 --> 00:41:23,175
- You want me to kill you?
- Do it!

417
00:41:25,450 --> 00:41:27,179
You get in the car!

418
00:41:29,020 --> 00:41:30,385
You coward.

419
00:41:31,389 --> 00:41:34,688
I don't want you, Bertie.
I don't want your jewelry.

420
00:41:34,792 --> 00:41:37,454
I don't want your chocolates.
I don't want your money.

421
00:41:37,529 --> 00:41:39,690
I don't want your rubbers. I don't want...

422
00:41:39,764 --> 00:41:43,598
- Fucking bitch! Get in the car!
- I'm sick of it! I hate you!

423
00:41:44,736 --> 00:41:48,103
I can make up my own mind,
what's left of it.

424
00:41:48,173 --> 00:41:51,336
Between the two of you,
I don't have one thought left in my head.

425
00:41:53,244 --> 00:41:55,178
Could you stop it, please?

426
00:41:55,580 --> 00:41:58,674
This has nothing to do with you. Stop it!

427
00:42:00,084 --> 00:42:02,314
I'll decide what happens to me.

428
00:42:20,738 --> 00:42:24,071
What's the matter?
Can't you live without me?

429
00:42:25,410 --> 00:42:26,638
Cowards.

430
00:42:55,206 --> 00:42:58,266
Rachel. Wait a minute. Wait!

431
00:42:59,277 --> 00:43:01,142
Where are you gonna go?

432
00:43:01,980 --> 00:43:03,447
I don't know.

433
00:43:26,437 --> 00:43:27,927
He can't swim.

434
00:43:31,743 --> 00:43:33,176
He can't swim?

435
00:43:36,781 --> 00:43:38,510
That figures.

436
00:44:37,709 --> 00:44:38,903
Rachel!

437
00:46:13,271 --> 00:46:14,829
<i>Non, non, non...</i>

438
00:46:21,579 --> 00:46:22,807
Rachel!

439
00:46:39,230 --> 00:46:40,993
<i>Vive Dada!</i>

440
00:46:52,210 --> 00:46:54,974
Jesus, I didn't know it would be so sad.

441
00:46:58,249 --> 00:46:59,978
<i>Vive</i> Oiseau!

442
00:47:00,351 --> 00:47:01,375
Look at them.

443
00:47:01,452 --> 00:47:05,855
If it weren't for me, people would've
thought "surreal" was a breakfast food.

444
00:47:09,260 --> 00:47:11,387
The dead are the most brave.

445
00:47:12,830 --> 00:47:15,128
They take their love with them.

446
00:47:17,568 --> 00:47:21,402
Can you imagine the courage
to love someone...

447
00:47:22,473 --> 00:47:24,236
...who loves you...

448
00:47:25,977 --> 00:47:28,707
...when there's nothing
you can do about it?

449
00:47:31,048 --> 00:47:32,310
Let's go.

450
00:47:46,497 --> 00:47:49,432
Jesus,
I just didn't know it would be so sad.

451
00:47:54,472 --> 00:47:58,238
We did appreciate him,
but did we tell him so...

452
00:47:58,309 --> 00:48:01,506
...so he was secure in his mind,
knowing that we did?

453
00:48:01,579 --> 00:48:04,139
We did not, and he was not.

454
00:48:05,082 --> 00:48:09,712
And so,
with his sweet smile and gentle concerns...

455
00:48:09,787 --> 00:48:12,085
...Oiseau is not still with us.

456
00:48:14,759 --> 00:48:16,124
He is not.

457
00:48:18,062 --> 00:48:20,223
Oiseau!

458
00:49:22,426 --> 00:49:25,554
Driver, Gare <i>Saint-Lazare, s'il vous plaît.</i>

459
00:49:29,133 --> 00:49:31,033
Oiseau!

460
00:50:05,403 --> 00:50:08,600
Jesus, I just ran into Maurice Ravel
in the men's room.

461
00:50:08,673 --> 00:50:10,436
He didn't recognize me.

462
00:50:12,443 --> 00:50:16,106
You know, Paris has been taken over
by people who are just imitators...

463
00:50:16,180 --> 00:50:18,410
...of people who were
imitators themselves.

464
00:50:18,482 --> 00:50:21,110
It's become a parody.
It's finished. It's over.

465
00:50:21,218 --> 00:50:24,984
Believe me, Hollywood is gonna be
like a breath of fresh air.

466
00:50:28,526 --> 00:50:31,359
Hart, let's face it.

467
00:50:31,462 --> 00:50:34,056
Rachel, she's not coming.

468
00:50:34,331 --> 00:50:36,993
- Yeah, I know.
- So let's go.

469
00:50:37,601 --> 00:50:39,000
Paris is...

470
00:50:41,105 --> 00:50:43,164
...a traveling picnic.

471
00:50:43,340 --> 00:50:45,740
Paris is a portable banquet.

472
00:50:45,943 --> 00:50:47,843
You should work on that.

473
00:50:59,250 --> 00:51:04,017
This whole experience gives me
a great idea for a movie I could write.

474
00:51:04,356 --> 00:51:07,086
And it would be perfect for von Stroheim.

475
00:51:07,325 --> 00:51:09,725
You can get a very good meal on this train.

476
00:51:09,794 --> 00:51:12,228
And they actually have
an excellent wine list.

477
00:51:15,634 --> 00:51:19,331
This is gonna be the best thing
that ever happened to you.

478
00:51:20,572 --> 00:51:22,437
Yeah, the hell with it.

479
00:51:23,408 --> 00:51:25,273
<i>Au revoir, Paris.</i>

480
00:51:26,511 --> 00:51:29,571
<i>"Au revoir, Paris"</i>

481
00:51:40,058 --> 00:51:41,252
Touché.

482
00:51:41,459 --> 00:51:44,121
<i>"Au revoir, Paris"</i>

483
00:52:31,309 --> 00:52:34,437
- We're just gonna be a few minutes.
- Yes, sir, I'll be here.

484
00:52:34,512 --> 00:52:36,639
The train to Hollywood leaves in an hour.

485
00:52:36,715 --> 00:52:40,014
We'll make the train.
I just want to see what they have in here.

486
00:52:40,118 --> 00:52:43,417
- It's gonna rain.
- You're so morose, it's monotonous.

487
00:52:43,488 --> 00:52:48,187
Babe Ruth hit three home runs today,
and we're going to Hollywood...

488
00:52:48,259 --> 00:52:51,319
...where the sky's always blue,
the sun always shines...

489
00:52:51,396 --> 00:52:52,988
...and we're gonna be rich!

490
00:52:53,064 --> 00:52:57,467
This, of course, is Modigliani.
A short, but fruitful life.

491
00:52:57,769 --> 00:53:01,227
His work is evidenced
by the plasticity he employed...

492
00:53:01,306 --> 00:53:03,797
...in his nudes and portraiture as well.

493
00:53:04,075 --> 00:53:07,408
Always the oval face,
the elongated trunk, arms.

494
00:53:08,713 --> 00:53:11,113
Yes, Henri Matisse.

495
00:53:11,516 --> 00:53:12,847
<i>Odalisque.</i>

496
00:53:13,017 --> 00:53:17,010
Study, if you will, the face.
It's the most realized of all of his works.

497
00:53:17,088 --> 00:53:19,386
In my mind, the best.

498
00:53:20,391 --> 00:53:23,918
No. Look at this. Can you imagine this?

499
00:53:25,497 --> 00:53:28,193
Boy, Nathalie certainly gets around.

500
00:53:28,967 --> 00:53:32,494
Aren't you Irving Fegelman?
I'm Ada Fuoco.

501
00:53:32,804 --> 00:53:35,432
We grew up together on Flatbush Avenue.

502
00:53:35,940 --> 00:53:37,965
<i>Je ne connais pas I'anglais.</i>

503
00:53:38,243 --> 00:53:40,438
But you look just like Irving.

504
00:53:42,013 --> 00:53:44,709
Jesse, he looks exactly like Irving.

505
00:53:49,921 --> 00:53:54,688
Let's proceed to Paul Cézanne,
considered to be the father of modern art.

506
00:53:54,759 --> 00:53:58,422
This is a work of rare emotional delicacy.

507
00:54:00,031 --> 00:54:03,728
This revelation cannot be taught...

508
00:54:04,502 --> 00:54:06,697
...nor can it be duplicated.

509
00:54:08,540 --> 00:54:12,340
Only the greatest artists can achieve
what has happened here...

510
00:54:12,410 --> 00:54:16,107
...and only then,
in that rare moment in time.

511
00:54:17,048 --> 00:54:19,380
So let's silently observe this.

512
00:54:19,951 --> 00:54:24,388
Come on, I'm bored with these pictures.
I want to go where the pictures move.

513
00:54:24,489 --> 00:54:26,389
I like your stuff better anyway.

514
00:54:26,457 --> 00:54:29,949
Just a minute.
This might help me appreciate my work.

515
00:54:35,934 --> 00:54:39,267
Gentlemen. Did it touch you?

516
00:54:40,305 --> 00:54:42,296
Did you sense the mystery?

517
00:54:42,473 --> 00:54:45,465
Do you know now
why this is a masterpiece?

518
00:54:46,477 --> 00:54:50,413
Why we praise it?
Why we genuflect before it?

519
00:54:51,583 --> 00:54:55,417
Why it will live through the ages
for all humanity?

520
00:54:56,521 --> 00:54:59,046
My God, I hope you understand that.

521
00:55:05,864 --> 00:55:10,028
Why is it, you are never
where you're supposed to be?

522
00:55:12,337 --> 00:55:13,599
I am now.

523
00:55:40,131 --> 00:55:41,894
Looks like rain.

524
00:55:42,934 --> 00:55:44,333
No, not now.

525
00:55:46,137 --> 00:55:47,661
Maybe tomorrow?

526
00:55:55,580 --> 00:55:58,708
I was thinking, Nicky,
maybe we should move to Hollywood.

527
00:55:58,783 --> 00:56:01,752
- Ever thought about it?
- You been talking to Oiseau?

528
00:56:04,255 --> 00:56:07,418
John, if you were lucky enough
to have lived in Paris...

529
00:56:07,492 --> 00:56:10,928
...lucky enough to have been young,
it didn't matter who you were...

530
00:56:10,995 --> 00:56:14,328
...because it was always worth it,
and it was good.