1
00:00:01,617 --> 00:00:03,312
Can we see them?

2
00:00:06,822 --> 00:00:09,086
Overseas Geneva Bank Ltd.

3
00:00:54,704 --> 00:00:56,604
It's time to work.

4
00:03:36,531 --> 00:03:37,862
I'll be right back.

5
00:03:42,971 --> 00:03:45,872
- I lose.
- Stand there.

6
00:03:46,007 --> 00:03:47,099
Yes, sir.

7
00:03:48,142 --> 00:03:49,608
Honor to Kasparov.

8
00:03:55,116 --> 00:03:56,845
Lower your eyes.

9
00:03:56,985 --> 00:03:58,680
Why are you laughing?

10
00:03:58,820 --> 00:04:00,218
What did you say?

11
00:04:00,354 --> 00:04:02,345
I said you look like Danton.

12
00:04:03,725 --> 00:04:05,283
I told you so.

13
00:04:08,396 --> 00:04:10,330
History professor?

14
00:04:10,465 --> 00:04:11,897
No, philosophy.

15
00:04:12,032 --> 00:04:15,730
Same thing.
Have you read Michelet?

16
00:04:15,870 --> 00:04:16,894
Yes, sir.

17
00:04:17,038 --> 00:04:18,562
Do you remember his description

18
00:04:18,706 --> 00:04:21,868
of the Committee of Public Safety?

19
00:04:27,482 --> 00:04:29,177
I don't remember that.

20
00:04:29,317 --> 00:04:32,444
They were third-rate writers
and failed actors.

21
00:04:32,586 --> 00:04:35,521
No one had slept for 3 weeks.

22
00:04:35,656 --> 00:04:37,090
Everyone was crying.

23
00:04:49,404 --> 00:04:52,771
Danton wasn't part of the Committee.

24
00:04:52,907 --> 00:04:55,170
Articulate!

25
00:04:55,309 --> 00:04:59,974
Danton wasn't in it.
He was Minister of Justice.

26
00:05:00,114 --> 00:05:01,274
Turn around.

27
00:05:02,317 --> 00:05:03,908
Bend over.

28
00:05:05,052 --> 00:05:06,383
Lift up your shirts.

29
00:05:26,606 --> 00:05:28,130
You too, Miss.

30
00:05:47,761 --> 00:05:51,322
Oh, what we take up the ass!

31
00:06:15,122 --> 00:06:16,521
Your grandfather?

32
00:06:20,627 --> 00:06:22,094
He's your brother?

33
00:06:27,334 --> 00:06:28,960
They're your sisters?

34
00:06:53,894 --> 00:06:55,293
How about them?

35
00:07:00,666 --> 00:07:04,103
What will it be?

36
00:07:04,237 --> 00:07:06,467
Philosophy will be our girlfriend.

37
00:07:07,407 --> 00:07:08,601
Forever.

38
00:07:11,510 --> 00:07:14,138
Day and night.

39
00:07:22,388 --> 00:07:24,356
Even if she loses her name.

40
00:07:25,592 --> 00:07:27,287
Even in her absence.

41
00:07:32,231 --> 00:07:34,165
A clandestine friend.

42
00:07:35,468 --> 00:07:39,371
We respect what prevents us
from getting close to her...

43
00:07:39,506 --> 00:07:41,872
As we sense
that we're not awake...

44
00:08:48,674 --> 00:08:53,441
And that which is wakeful in us,
even in our sleep,

45
00:08:53,579 --> 00:08:55,878
is due to her difficult friendship.

46
00:09:47,733 --> 00:09:49,428
Farewell, Perdican.

47
00:10:38,884 --> 00:10:41,250
I said to wear a skirt!

48
00:10:41,387 --> 00:10:43,014
Yes, Dad.

49
00:10:53,998 --> 00:10:57,832
Cinema replaces our gaze

50
00:10:57,969 --> 00:11:00,495
with a world
in harmony with our desires.

51
00:11:05,743 --> 00:11:08,371
Water! Only water!

52
00:11:12,384 --> 00:11:14,045
No, it's no good.

53
00:11:17,755 --> 00:11:22,625
Since I'm in the movies,
all I do is wait!

54
00:11:23,595 --> 00:11:26,620
I'm half an f-stop off.

55
00:11:26,764 --> 00:11:29,358
He's the new runner, sir.

56
00:11:29,500 --> 00:11:32,367
I'm asking what this is!

57
00:11:32,503 --> 00:11:34,335
The location, sir.

58
00:11:42,680 --> 00:11:44,308
What are you eyeing, asshole?

59
00:11:44,450 --> 00:11:46,509
Nothing, ma'am.

60
00:11:46,652 --> 00:11:47,845
A pack of Marlboros.

61
00:11:47,985 --> 00:11:49,384
Yes, ma'am.

62
00:11:49,520 --> 00:11:50,782
Stay here!

63
00:12:00,932 --> 00:12:02,900
I'm studying cinema in high school.

64
00:12:07,673 --> 00:12:09,537
Give him a script.

65
00:12:12,377 --> 00:12:13,867
Go have a seat.

66
00:12:17,582 --> 00:12:19,573
The poster.

67
00:12:25,757 --> 00:12:27,156
It'll cost a fortune.

68
00:12:27,291 --> 00:12:30,556
There's money in the sea.

69
00:12:30,695 --> 00:12:33,323
The Hakim brothers taught me that.

70
00:12:40,571 --> 00:12:45,065
What we need is a casino nearby
and a school for Solange.

71
00:12:45,209 --> 00:12:46,437
No problem.

72
00:12:46,577 --> 00:12:47,908
Give me the figures.

73
00:12:48,045 --> 00:12:51,445
- What is this?
- The location.

74
00:12:54,685 --> 00:12:57,176
It's not natural.
Get something else.

75
00:12:57,321 --> 00:13:00,222
Seriously... what's the problem?

76
00:13:00,358 --> 00:13:04,124
Everything. There's not enough water.
It's all wrong.

77
00:13:04,262 --> 00:13:07,993
I was told to keep expenses down.

78
00:13:08,132 --> 00:13:10,657
Let's go.
I'll call the production office.

79
00:13:16,073 --> 00:13:17,973
Don't forget the paychecks.

80
00:13:26,250 --> 00:13:28,150
Who will the actress be?

81
00:13:28,286 --> 00:13:29,776
We'll tell you later.

82
00:13:31,289 --> 00:13:32,812
Here are the pictures.

83
00:13:42,333 --> 00:13:45,165
They're all cows.
What do you think?

84
00:13:47,237 --> 00:13:49,934
Why do cows always look
so grumpy?

85
00:13:50,074 --> 00:13:53,202
lf your breasts were yanked on
four times a day

86
00:13:53,344 --> 00:13:57,780
and you only got fucked once a year,
you'd look grumpy too.

87
00:13:59,984 --> 00:14:01,849
Have you read it?

88
00:14:01,986 --> 00:14:03,351
Yes. In my opinion...

89
00:14:03,488 --> 00:14:04,954
We don't want it!

90
00:14:06,556 --> 00:14:10,926
I don't care about the story!
The title should be "Fatal Bolero"

91
00:14:11,062 --> 00:14:13,622
and the girl should die
in a twister!

92
00:14:18,168 --> 00:14:19,567
Yes, sir.

93
00:14:23,040 --> 00:14:27,999
This must be why I've always felt
a profound sadness in the cinema.

94
00:14:29,246 --> 00:14:32,842
Both a possibility of expression

95
00:14:32,984 --> 00:14:36,249
and the trace of something
essential, renounced.

96
00:14:40,423 --> 00:14:42,891
Butt-fuck me, bastard.

97
00:14:43,026 --> 00:14:46,190
Shove your cock
up the bitch's ass.

98
00:14:46,330 --> 00:14:48,729
Slide your dick up her butt.

99
00:14:48,865 --> 00:14:50,992
Stick your prick...

100
00:14:53,970 --> 00:14:55,871
Place your bets.

101
00:14:59,409 --> 00:15:02,105
-So what's the news?
-Some good, some bad.

102
00:15:02,245 --> 00:15:03,940
I'll be right back.

103
00:15:05,115 --> 00:15:06,447
I found cheap actors.

104
00:15:08,718 --> 00:15:11,448
He's here. He wants money.

105
00:15:11,588 --> 00:15:14,989
Who are these people asking for money?
What have they done?

106
00:15:15,125 --> 00:15:17,924
None of them
has ever sent me a nice letter.

107
00:15:19,195 --> 00:15:22,562
I'll sign two more, then it's over!

108
00:15:26,436 --> 00:15:31,100
We still need... some cash, sir!

109
00:15:31,241 --> 00:15:32,674
Show me that.

110
00:15:32,809 --> 00:15:34,800
Place your bets.

111
00:15:39,182 --> 00:15:40,581
Go sit down.

112
00:15:42,185 --> 00:15:43,880
Did I spell it right?

113
00:15:44,020 --> 00:15:45,044
No!

114
00:15:52,195 --> 00:15:53,924
Place your bets.

115
00:15:54,931 --> 00:15:58,423
Number 36, Baron...

116
00:15:58,568 --> 00:16:01,263
You think so?

117
00:16:01,403 --> 00:16:02,894
Like the Popular Front?

118
00:16:03,039 --> 00:16:05,234
Exactly. Paid vacations.

119
00:16:05,375 --> 00:16:06,865
Long live vacations!

120
00:16:07,010 --> 00:16:09,205
Do your homework!

121
00:16:09,346 --> 00:16:10,370
Number 36.

122
00:16:17,687 --> 00:16:20,019
36, red, even, passe.

123
00:16:24,927 --> 00:16:28,694
Godammit, piss up my crack,
piss up my bung hole!

124
00:16:28,832 --> 00:16:30,231
Give me a raise, Baron.

125
00:16:36,573 --> 00:16:38,131
Butt-fuck me to death !

126
00:16:38,274 --> 00:16:40,105
We found cheap actors.

127
00:16:47,717 --> 00:16:48,945
Go away!

128
00:16:49,085 --> 00:16:51,053
I need cash, sir!

129
00:17:08,904 --> 00:17:10,964
See if they're still breathing.

130
00:17:11,107 --> 00:17:12,199
You're crazy.

131
00:17:17,246 --> 00:17:19,680
How horrible!

132
00:17:19,815 --> 00:17:22,683
Not at all. Didn't Cocteau say:
"The cinema..."

133
00:17:22,819 --> 00:17:24,684
That's right.

134
00:17:30,592 --> 00:17:32,253
There's a woman in here.

135
00:17:43,005 --> 00:17:45,941
A man in here.

136
00:17:46,076 --> 00:17:47,736
We'll meet at the hotel.

137
00:18:37,860 --> 00:18:40,590
Knowledge of the possibility

138
00:18:40,729 --> 00:18:43,596
of representation consoles us

139
00:18:43,732 --> 00:18:47,327
for being enslaved to life.

140
00:18:55,844 --> 00:18:58,870
Knowledge of life

141
00:18:59,014 --> 00:19:01,948
consoles us
for the fact that representation

142
00:19:02,083 --> 00:19:04,018
is but shadow.

143
00:19:10,492 --> 00:19:13,221
No film stock.

144
00:19:13,361 --> 00:19:14,761
- Since when?
- 3 weeks.

145
00:19:14,897 --> 00:19:16,626
I'm half an f-stop off.

146
00:19:16,765 --> 00:19:18,232
I'll take care of it.

147
00:19:22,337 --> 00:19:23,997
We're not tourists.

148
00:19:24,138 --> 00:19:25,367
Why "Fatal"?

149
00:19:43,991 --> 00:19:45,925
There's no use. I can't.

150
00:19:46,794 --> 00:19:48,660
Let's start over, Miss.

151
00:19:50,799 --> 00:19:52,357
It's too hard.

152
00:20:04,079 --> 00:20:06,410
It's because you're acting.

153
00:20:06,547 --> 00:20:08,447
Acting weakens the text.

154
00:20:14,356 --> 00:20:18,416
Acting removes its presence.
It sucks the blood out of it.

155
00:20:38,212 --> 00:20:41,875
You've received money.
It's time to work.

156
00:20:43,317 --> 00:20:45,479
When will you shoot the battle?

157
00:20:45,621 --> 00:20:47,645
There is no battle.

158
00:20:47,788 --> 00:20:48,982
The script!

159
00:20:59,200 --> 00:21:00,633
How about page 121?

160
00:21:20,388 --> 00:21:23,050
Ah yes... you're right.

161
00:21:24,725 --> 00:21:26,920
There... It's done.

162
00:21:30,898 --> 00:21:33,230
Lesson 32.
John Ford. Henry Fonda.

163
00:21:33,367 --> 00:21:36,495
"She Wore a Yellow Ribbon".

164
00:21:50,885 --> 00:21:52,079
I have an idea.

165
00:21:52,219 --> 00:21:53,709
He has an idea!

166
00:22:08,035 --> 00:22:10,094
Don't you care that I love you?

167
00:22:22,416 --> 00:22:23,905
See you tomorrow.

168
00:22:31,692 --> 00:22:34,159
- Can I come in?
- What for?

169
00:22:34,294 --> 00:22:36,092
So I can teach you the rest.

170
00:22:56,850 --> 00:23:00,218
- You're harsh.
- That's how I feel.

171
00:23:00,354 --> 00:23:01,787
Because you've never suffered.

172
00:23:01,922 --> 00:23:06,415
I've made mistakes...
but I've loved.

173
00:23:08,161 --> 00:23:11,256
I don't understand the "Fatal".
Can I ask him?

174
00:23:11,399 --> 00:23:12,957
He won't answer you.

175
00:23:14,402 --> 00:23:16,266
In a novel,

176
00:23:16,403 --> 00:23:18,166
a house or a person

177
00:23:18,304 --> 00:23:20,363
gets all its signification

178
00:23:20,507 --> 00:23:22,703
its very existence,
from the writer.

179
00:23:24,145 --> 00:23:27,443
Here, a house or a person

180
00:23:27,580 --> 00:23:31,311
gets a small amount
of its meaning from me.

181
00:23:31,451 --> 00:23:35,718
It's true signification
is much larger. It's gigantic.

182
00:23:35,856 --> 00:23:38,790
It is to exist here and now,

183
00:23:38,925 --> 00:23:40,825
like you and me,

184
00:23:40,960 --> 00:23:45,364
like no imagined character can exist.

185
00:23:45,499 --> 00:23:49,059
Its immense weight,
its mystery and its dignity

186
00:23:49,202 --> 00:23:50,999
arise from this fact.

187
00:23:52,372 --> 00:23:54,603
The fact that I exist,

188
00:23:54,742 --> 00:23:57,540
me too,

189
00:23:57,678 --> 00:24:00,703
not like in a work of fiction,

190
00:24:00,847 --> 00:24:06,047
but as a human being.

191
00:24:17,765 --> 00:24:18,732
Yes, Dad.

192
00:24:35,448 --> 00:24:37,575
I knew it'd end like this.

193
00:24:37,717 --> 00:24:41,050
Not knowing
where to put the camera.

194
00:24:41,187 --> 00:24:42,211
What do we do?

195
00:24:42,355 --> 00:24:45,813
Put the camera
in front of the actors.

196
00:24:55,034 --> 00:24:56,228
Rolling!

197
00:24:59,539 --> 00:25:01,700
"Bolero Fatal", 52.

198
00:25:08,014 --> 00:25:10,574
Yes, there's more.

199
00:25:12,618 --> 00:25:15,645
Now that I'm unemployed,

200
00:25:17,390 --> 00:25:21,952
during these slow, empty hours

201
00:25:22,095 --> 00:25:27,055
there rises
from the depths of my soul

202
00:25:27,200 --> 00:25:31,603
and... into my mind,

203
00:25:31,738 --> 00:25:36,573
a sadness.

204
00:25:49,656 --> 00:25:51,453
2.8 minus 1/4.

205
00:25:51,591 --> 00:25:53,286
Yes, Dad!

206
00:25:57,729 --> 00:26:02,030
We'll make it easier.
Just say the "Yes" at the beginning.

207
00:26:02,168 --> 00:26:04,136
Yes... sir.

208
00:26:04,270 --> 00:26:06,499
Perfect. That will do.

209
00:26:06,638 --> 00:26:09,574
We weren't rolling.
We're reloading.

210
00:26:09,709 --> 00:26:11,267
We have time.

211
00:26:23,957 --> 00:26:25,185
What's the problem now?

212
00:26:31,063 --> 00:26:33,464
I prefer to die.

213
00:26:51,149 --> 00:26:53,083
Whenever you're ready, Miss.

214
00:27:33,792 --> 00:27:36,090
I'm going to lunch.

215
00:27:36,228 --> 00:27:38,196
I'm staying.

216
00:28:41,827 --> 00:28:43,226
How horrible.

217
00:29:01,279 --> 00:29:05,545
It's what I like in cinema.

218
00:29:05,684 --> 00:29:09,484
A saturation of glorious signs,

219
00:29:09,621 --> 00:29:12,749
bathing in the light
of their absent explanation.

220
00:29:18,095 --> 00:29:19,824
I'm cold!

221
00:29:19,964 --> 00:29:21,125
Fight, Miss.

222
00:29:21,266 --> 00:29:22,426
I am fighting.

223
00:29:24,169 --> 00:29:25,363
Let's go.

224
00:29:26,971 --> 00:29:28,370
Okay.

225
00:29:29,573 --> 00:29:31,302
You're better already.

226
00:29:31,442 --> 00:29:32,603
Yes.

227
00:29:32,744 --> 00:29:35,008
Okay, cut!

228
00:29:35,147 --> 00:29:37,138
The sound's okay!

229
00:29:37,282 --> 00:29:38,714
The camera too!

230
00:29:57,936 --> 00:29:59,994
My master once said:

231
00:30:00,137 --> 00:30:02,970
"I conceive of nothing as infinite.

232
00:30:03,107 --> 00:30:07,477
"How can I conceive
of anything as infinite?"

233
00:30:07,612 --> 00:30:11,206
"Listen," I said.
"Imagine a space,

234
00:30:11,348 --> 00:30:14,875
"and that beyond this space
is more space,

235
00:30:15,019 --> 00:30:19,719
"and farther on,
there's more and more.

236
00:30:19,858 --> 00:30:21,347
"It's never-ending."

237
00:30:22,560 --> 00:30:26,189
"Why," asked my master?

238
00:30:26,330 --> 00:30:29,858
I was stupefied.
"lf it ends," I shouted,

239
00:30:30,001 --> 00:30:32,764
"what's beyond it?"

240
00:30:32,903 --> 00:30:36,669
"lf it ends, beyond it is nothing,"

241
00:30:36,807 --> 00:30:38,468
he answered.

242
00:30:40,946 --> 00:30:44,745
My master was the only philosopher
who was truly sincere.

243
00:31:03,033 --> 00:31:04,330
What's going on?

244
00:31:04,468 --> 00:31:05,958
We're ruined, honey.

245
00:31:07,271 --> 00:31:09,831
They ran off with the money.

246
00:31:09,974 --> 00:31:10,941
Where to?

247
00:31:18,749 --> 00:31:22,014
Now I'll have to work.

248
00:31:22,152 --> 00:31:24,211
No, we have the negative.

249
00:31:24,355 --> 00:31:27,119
What's that for?

250
00:31:27,258 --> 00:31:30,227
We'll make prints
and open the movie fast.

251
00:31:33,130 --> 00:31:36,497
It's in black and white!

252
00:31:36,634 --> 00:31:38,261
No, it's in color.

253
00:31:38,402 --> 00:31:40,700
Dumb bitch,
it's black and white!

254
00:31:40,838 --> 00:31:43,272
lf it's poetry, I'll walk out.

255
00:31:44,708 --> 00:31:46,972
We pay enough taxes as it is.

256
00:31:47,111 --> 00:31:51,013
Look, we'll be able to buy
your Omega.

257
00:31:51,148 --> 00:31:53,616
Not an Omega.
I'm not in the Olympics.

258
00:31:53,751 --> 00:31:57,413
Whatever you want!

259
00:31:58,155 --> 00:32:00,316
Judging by that...

260
00:32:02,159 --> 00:32:03,820
I'll end up with a Swatch.

261
00:32:05,362 --> 00:32:06,885
Is your husband here?

262
00:32:07,030 --> 00:32:09,465
It's Mr Felix!

263
00:32:09,600 --> 00:32:11,864
Quiet. Keep working.

264
00:32:12,002 --> 00:32:13,026
Does it start soon?

265
00:32:13,170 --> 00:32:16,969
lf it doesn't, I'm leaving.

266
00:32:17,106 --> 00:32:19,404
It's in black and white.

267
00:32:22,046 --> 00:32:24,014
I hope it's not poetry.

268
00:32:25,683 --> 00:32:26,911
It looks like a winner!

269
00:32:27,051 --> 00:32:28,415
Why "Fatal"?

270
00:32:32,690 --> 00:32:36,285
It's the name of a war.
We lost many friends.

271
00:32:36,427 --> 00:32:38,155
Your fucking movie
is full of corpses?

272
00:32:38,294 --> 00:32:41,593
- Not exactly.
- The cinema is all about waiting!

273
00:32:41,731 --> 00:32:43,165
Shut up!

274
00:32:43,300 --> 00:32:45,825
Sorry, kid. There are no tits!

275
00:32:45,970 --> 00:32:48,097
Miss, you'll see.

276
00:32:56,981 --> 00:32:59,141
What's the movie about?

277
00:32:59,282 --> 00:33:02,774
The photography is gorgeous,
you'll see, Miss.

278
00:33:02,919 --> 00:33:04,352
Are there any boobs?

279
00:33:06,790 --> 00:33:10,225
- No, no boobs.
- Any knockers?

280
00:33:12,528 --> 00:33:15,588
Your grandmother used that word.

281
00:33:15,732 --> 00:33:16,961
No knockers!

282
00:33:17,101 --> 00:33:18,125
Get lost, dickwad!

283
00:33:18,268 --> 00:33:24,035
Get that asshole out of here!

284
00:33:24,173 --> 00:33:25,800
Beat it, little prick!

285
00:33:25,942 --> 00:33:29,071
Let's go see "Terminator 4".

286
00:33:29,213 --> 00:33:32,841
Come back, friends! Come back!

287
00:33:32,982 --> 00:33:34,813
We'll change the movie!

288
00:33:47,263 --> 00:33:49,094
Where's the mistake?

289
00:33:49,232 --> 00:33:53,532
Mother and I told you,
you have to turn the page.

290
00:33:58,441 --> 00:34:00,102
Justice prevails.

291
00:34:03,379 --> 00:34:04,744
I knew it.

292
00:35:10,747 --> 00:35:15,582
Try it as a farewell to the tutti
and an invitation to the solo.

293
00:35:35,471 --> 00:35:37,939
It's another world.
Built up from nothing,

294
00:35:38,073 --> 00:35:41,873
then come back to nothing.

295
00:36:24,586 --> 00:36:26,076
Let's begin.

296
00:36:27,657 --> 00:36:29,852
Yes, Governor.

297
00:36:31,092 --> 00:36:33,117
I prefer Wagner.

298
00:36:33,261 --> 00:36:36,788
Not Wagner!
You'd hear him at the bottom of the sea!

299
00:36:36,932 --> 00:36:41,393
Whereas Mozart...
It's gentle and light.

300
00:36:41,537 --> 00:36:42,833
That's what people think.

301
00:37:07,996 --> 00:37:09,156
Let's begin.

302
00:37:11,299 --> 00:37:13,563
Yes, Governor.

303
00:37:23,411 --> 00:37:25,811
Too many notes in Mozart.

304
00:37:29,317 --> 00:37:31,114
That's what people think.

