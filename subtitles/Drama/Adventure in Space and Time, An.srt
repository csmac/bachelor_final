1
00:00:00,800 --> 00:00:03,300
Adventure time Season 01 Business time
by Rumi(http://rumistar.blogspot.kr/)

2
00:00:04,300 --> 00:00:06,600
Subtitles for English learners

3
00:00:16,137 --> 00:00:19,464
I found another bike, and more computers.
What do you got?

4
00:00:19,464 --> 00:00:21,614
I keep finding baby shoes!

5
00:00:22,680 --> 00:00:25,701
WHAT THE HECK, MAN?!
And they're all lefties!

6
00:00:25,701 --> 00:00:27,600
Sorry I'm not finding any loot.

7
00:00:27,600 --> 00:00:32,800
Keep it together, chubby,
because I think we found enough scrap
to finish building our gauntlet dock

8
00:00:32,800 --> 00:00:35,100
a dock that is also a gauntlet.

9
00:00:35,100 --> 00:00:40,800
Aww. Building this gauntlet dock is hard, man.
Hard work sucks

10
00:00:40,800 --> 00:00:42,147
Don't be lazy, Jake.

11
00:00:43,900 --> 00:00:45,700
Incoming iceberg!

12
00:00:45,700 --> 00:00:47,300
Whoa! That one's huge!

13
00:00:47,300 --> 00:00:50,900
I got dibs!
Ooo. I hope it's not baby shoes.

14
00:00:50,900 --> 00:00:53,300
[Gasps] It's a guy.

15
00:00:53,720 --> 00:00:56,000
Creepy. It's a buncha guys.

16
00:00:56,000 --> 00:00:58,289
You know what they remind me of?
Well-dressed pickles.

17
00:00:58,289 --> 00:01:03,170
Stand back, Jake!
I'm gonna melt 'em outta there!
Full charge.

18
00:01:04,237 --> 00:01:08,880
Whoa, don't roast 'em, man.
You gotta flambe.

19
00:01:11,500 --> 00:01:14,780
Ah. Perfecto.
Bon appetit.

20
00:01:14,780 --> 00:01:18,000
Hey! Can you hear me? Hello?

21
00:01:19,000 --> 00:01:20,000
I don't think they made it.

22
00:01:20,000 --> 00:01:22,000
Hey.
This one's got some kind of pack.

23
00:01:22,000 --> 00:01:23,820
[Finn screams]

24
00:01:23,820 --> 00:01:30,323
I remember... business.

25
00:01:30,723 --> 00:01:32,343
Let go! Let go! Let go!

26
00:01:33,750 --> 00:01:36,003
We... are businessmen.

27
00:01:36,003 --> 00:01:39,400
Oooh... Well, I'm Finn and he's Jake.

28
00:01:39,400 --> 00:01:41,243
What kind of business do you do?

29
00:01:41,243 --> 00:01:45,300
Been frozen... so long,
I...

30
00:01:47,094 --> 00:01:49,074
can't remember.

31
00:01:49,074 --> 00:01:55,303
Can't remember!
Oh. Looking for help, your business?
We love work for you.

32
00:01:55,303 --> 00:01:59,811
Nah. Adventurers don't need any help.
- Yes! Help us fix that dock.

33
00:01:59,811 --> 00:02:00,811
Dock-ka?

34
00:02:00,811 --> 00:02:02,489
Yeah! Right over there.

35
00:02:02,489 --> 00:02:07,383
Yes. This dock-ka...
could be more... efficient.

36
00:02:07,383 --> 00:02:08,500
No! Wait!

37
00:02:09,521 --> 00:02:13,891
Jake, this dock is our fun pie.
We should be the ones to bake it.

38
00:02:13,891 --> 00:02:17,801
But they're begging for it, Finn.
Just look at 'em.

39
00:02:17,801 --> 00:02:20,700
[Murmuring] Build dock. Build dock.

40
00:02:20,700 --> 00:02:26,691
Finn. These poor souls are lost without jobs.
We can't ignore their plight.

41
00:02:26,691 --> 00:02:27,441
Uhh...

42
00:02:27,441 --> 00:02:29,150
Go on guys, fix it up!

43
00:02:29,150 --> 00:02:30,700
Woo!

44
00:02:33,920 --> 00:02:36,888
Well... they do seem really happy to do it.

45
00:02:36,888 --> 00:02:40,000
Of course, man.
They said they "love work for you."

46
00:02:40,000 --> 00:02:44,305
Uhh... We finish.

47
00:02:44,305 --> 00:02:45,395
Whoa!

48
00:02:45,395 --> 00:02:46,725
Told you so.

49
00:02:46,725 --> 00:02:48,615
Come on!
Let's try it out!

50
00:03:06,605 --> 00:03:09,034
Yeah! Woo!

51
00:03:09,034 --> 00:03:11,410
[Panting]

52
00:03:11,410 --> 00:03:17,500
See? If we hire them full time,
they'll be back in business
and we'll be able to focus on fun stuff.

53
00:03:17,500 --> 00:03:19,000
Everybody wins!

54
00:03:19,000 --> 00:03:20,200
Maybe you're right.

55
00:03:20,200 --> 00:03:23,100
You hear that, guys?
Maybe I'm right!

56
00:03:23,100 --> 00:03:28,197
Really? Job?
With... ad-ven-tur-ers?

57
00:03:28,197 --> 00:03:30,047
Yeah, mans!

58
00:03:43,227 --> 00:03:44,657
Feels weird doing nothing.

59
00:03:44,657 --> 00:03:46,600
Relax, man. This is your day off.

60
00:03:46,600 --> 00:03:49,102
Help! Somebody help!

61
00:03:49,102 --> 00:03:52,252
Trouble, dude!
Get your axe! I'll get my... wha?

62
00:03:52,252 --> 00:03:55,292
Adventure pack... ready for you.

63
00:03:55,292 --> 00:03:56,308
Hey, thanks man.

64
00:03:58,152 --> 00:04:01,792
I have to say, Jake.
My sword is totally shiny and stinkin' sharp!

65
00:04:01,792 --> 00:04:02,500
Uh huh.

66
00:04:02,500 --> 00:04:09,002
Even my shoes feel different.
Not only are they clean,
I feel radder, faster, more... adequate.

67
00:04:09,002 --> 00:04:10,200
Cool!

68
00:04:13,000 --> 00:04:14,059
Help!

69
00:04:14,059 --> 00:04:18,110
Don't cry, Hot Dog Princess!
Jake and I will fend off these battle cubes!

70
00:04:26,000 --> 00:04:28,170
Man. I'm getting tired.

71
00:04:28,170 --> 00:04:33,500
[Groan] Me too.
These cubes are... frickin' resilient.

72
00:04:33,500 --> 00:04:38,000
Hey, business dudes!
Hold off these cubes so we can catch our breath?

73
00:04:38,000 --> 00:04:40,860
Jake! They don't know how to fight.

74
00:04:45,800 --> 00:04:49,200
Water. Orange slices. Help rehydrate.

75
00:04:52,239 --> 00:04:54,079
These guys are great, right?

76
00:04:54,079 --> 00:04:57,000
I gotta admit... they are helping.

77
00:04:57,000 --> 00:05:00,789
Oh! I think I figured out how to defeat the battle cubes!

78
00:05:00,789 --> 00:05:04,249
That's 'cause you had time to rest your body
and refresh your brain.

79
00:05:04,249 --> 00:05:06,389
Ok! We'll take it from here, guys!

80
00:05:06,389 --> 00:05:08,199
Yeah! Good work.

81
00:05:09,889 --> 00:05:11,579
Grab all the cubes together!

82
00:05:12,749 --> 00:05:13,700
Got 'em!

83
00:05:15,589 --> 00:05:16,699
It worked!

84
00:05:17,919 --> 00:05:20,249
Wake up, Hot Dog Princess.
You're free.

85
00:05:20,249 --> 00:05:26,439
Oh! Thank you, Finn and Jake!
Especially you, Finn.

86
00:05:27,700 --> 00:05:30,169
Ugh. She smells like old hot dog water.

87
00:05:36,000 --> 00:05:37,879
I take one for team.

88
00:05:37,879 --> 00:05:41,699
This is awesome!
You get a promotion, fella.

89
00:05:42,889 --> 00:05:45,000
Take one for team, too.

90
00:05:48,509 --> 00:05:50,000
Oh... Lost again!

91
00:05:50,000 --> 00:05:51,029
My turn.

92
00:05:53,329 --> 00:05:54,069
What's that?

93
00:05:54,069 --> 00:05:59,659
Hero vision monitor.
Use satellite to tell us world problems.

94
00:05:59,659 --> 00:06:01,800
Oh, wow. Anything going on?

95
00:06:01,800 --> 00:06:05,209
Just small things.
You save stress for big thing.

96
00:06:05,209 --> 00:06:06,829
Freakin' awesome, man.

97
00:06:06,829 --> 00:06:07,889
What are they doing?

98
00:06:08,449 --> 00:06:09,499
Takin' care of business.

99
00:06:14,389 --> 00:06:15,229
Jake, hit me!

100
00:06:23,000 --> 00:06:24,694
Mission complete.

101
00:06:24,694 --> 00:06:25,384
Yeah!

102
00:06:25,384 --> 00:06:26,234
We beat Adventure Master!

103
00:06:26,234 --> 00:06:27,734
Holy moly!

104
00:06:27,734 --> 00:06:29,784
We're adventure masters!

105
00:06:29,784 --> 00:06:30,724
Woo! Yes!

106
00:06:33,574 --> 00:06:36,434
I never knew being fat and lazy was so rewarding.

107
00:06:36,434 --> 00:06:40,000
Yeah.
You're gut's so huge and moldable.

108
00:06:40,000 --> 00:06:40,794
Hey!

109
00:06:40,794 --> 00:06:41,894
Hold on a sec!

110
00:06:41,894 --> 00:06:43,314
[Laughs] Man, that tickles.

111
00:06:43,314 --> 00:06:48,700
I'm the Ice King, and I'll never find a bride
because I'm such a tool.

112
00:06:48,700 --> 00:06:51,464
[Laughs] Alright, let me try.

113
00:06:53,720 --> 00:06:57,824
I'm Princess Bubblegum and I'm a dork,
because I like science!

114
00:06:57,824 --> 00:07:02,000
I've also got a really annoying voice
that Finn thinks is attractive!

115
00:07:02,000 --> 00:07:04,304
[Laughs] That's a- Hey, what'd you say?

116
00:07:04,304 --> 00:07:07,066
Ahh! Oh, my gosh!
Leave me alone.

117
00:07:07,066 --> 00:07:12,450
All I said was "you're ugly," which is totally true.
Somebody help me! Oh no!

118
00:07:12,450 --> 00:07:16,000
Man, I am not in the mood
for saving Lumpy Space Princess

119
00:07:16,000 --> 00:07:17,723
Hey, business dudes!

120
00:07:18,003 --> 00:07:19,043
What do you guys think?

121
00:07:19,043 --> 00:07:22,493
Hmm... Just one monster. We do.

122
00:07:22,493 --> 00:07:26,200
Right. We'll just save our strength
for the big adventures, then.

123
00:07:26,800 --> 00:07:28,420
Ahh. Nice call, dude.

124
00:07:28,420 --> 00:07:32,730
Imagine how awesome the adventure's going to be
when it's time for us to go out there.

125
00:07:32,730 --> 00:07:36,130
I'm too tired to imagine stuff,
but I bet you're right.

126
00:07:36,830 --> 00:07:39,450
To being great adventurers!

127
00:07:49,000 --> 00:07:50,130
Oh, gosh.

128
00:07:50,130 --> 00:07:51,120
Help us!

129
00:07:51,120 --> 00:07:53,000
Huh? Jake! Did you hear that?

130
00:07:53,000 --> 00:07:55,640
Yeah. Let the businessmen handle it.

131
00:07:55,640 --> 00:07:57,430
Help! Please!

132
00:07:57,430 --> 00:07:58,230
I heard it again.

133
00:07:58,230 --> 00:08:00,000
Businessmen, dude.

134
00:08:00,000 --> 00:08:01,920
Heeeeeelp!

135
00:08:06,600 --> 00:08:09,000
Woo-hoo! Woo!

136
00:08:09,000 --> 00:08:13,000
Jake! We messed up!
The businessmen have gone bat-crazy, dude!

137
00:08:13,000 --> 00:08:15,750
Man, just let the businessmen handle it.

138
00:08:15,750 --> 00:08:18,230
THE BUSINESSMEN ARE THE PROBLEM,
you lazy plug-hole!

139
00:08:18,230 --> 00:08:18,850
Huh?

140
00:08:18,850 --> 00:08:20,200
Guys!

141
00:08:21,060 --> 00:08:22,210
Guys, stop it!

142
00:08:24,230 --> 00:08:25,650
Stop it, guys!

143
00:08:25,650 --> 00:08:27,000
Stop what, boss?

144
00:08:27,000 --> 00:08:28,600
You're jacking up those Fuzzy Friends!

145
00:08:28,600 --> 00:08:34,310
But... We're being heroes. Like you, boss.
We're protecting them.

146
00:08:34,310 --> 00:08:37,593
Collecting them in our care-sack so they cannot be hurt.

147
00:08:38,833 --> 00:08:41,473
It's the most efficient way to save people.

148
00:08:41,473 --> 00:08:43,350
But you're making them unhappy!

149
00:08:43,350 --> 00:08:49,183
Irrelevant! These people are in our care-sack.
Their happiness is not priority

150
00:08:49,183 --> 00:08:54,123
I am your boss! And you guys are all fired!

151
00:08:54,123 --> 00:08:56,000
Fi-red?

152
00:09:04,663 --> 00:09:10,733
Nooo! Alright, guys.
Now I've gotta take you down... Finn style.

153
00:09:15,000 --> 00:09:17,423
I'm kicking your care-sack, dudes!

154
00:09:17,423 --> 00:09:20,173
Finn? Whoa... Crud.

155
00:09:21,733 --> 00:09:23,173
I'm comin' buddy!

156
00:09:26,573 --> 00:09:29,500
Ahh. I'm so fat, dude;
I don't know what to do.

157
00:09:29,500 --> 00:09:32,985
[Gasp] That's it! Jake! Demoralize them!

158
00:09:33,863 --> 00:09:35,105
What? Why?

159
00:09:35,105 --> 00:09:37,525
Do it, man! I have a legit plan!

160
00:09:37,525 --> 00:09:42,105
Ok, Alright.
Hey! You guys are horrible at business!

161
00:09:44,745 --> 00:09:45,945
Huh? They're sucking me up, dude!

162
00:09:45,945 --> 00:09:51,705
Now, eat that ice cream some more to become fatter,
while also using your Stretchy Powers to grow huge!

163
00:09:51,705 --> 00:09:53,605
Ok!

164
00:09:59,435 --> 00:10:00,225
This sucks.

165
00:10:00,225 --> 00:10:03,765
Yeah, dude! Keep growin'!
You're breaking apart their robot!

166
00:10:06,665 --> 00:10:08,235
Oh no!

167
00:10:10,000 --> 00:10:13,395
I'm going to kill you, not-boss!

168
00:10:13,395 --> 00:10:17,235
Wait, man! Wait!
I wanna re-hire you guys!

169
00:10:17,235 --> 00:10:20,000
Re-hire? Really?

170
00:10:20,000 --> 00:10:21,145
Yeah, mans.

171
00:10:21,145 --> 00:10:24,000
Woo hoo!

172
00:10:25,900 --> 00:10:30,404
Yay! Hip-hip hooray! Pancakes!

173
00:10:31,754 --> 00:10:34,434
So, wait... What'd you hire them to do?

174
00:10:34,434 --> 00:10:37,714
I hired them to stuff themselves in that iceberg
and get outta here.

175
00:10:37,714 --> 00:10:41,524
[Sigh] I'm gonna miss 'em.
And I'm gonna miss this gut.

176
00:10:42,704 --> 00:10:45,114
[Laughs] I'm gonna miss my gut too.

177
00:10:46,424 --> 00:10:48,504
Aww! Aww...