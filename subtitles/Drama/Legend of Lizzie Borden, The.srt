﻿1
00:00:01,750 --> 00:00:03,215
So I just start talking?

2
00:00:03,326 --> 00:00:04,688
Pretty much.

3
00:00:06,699 --> 00:00:08,599
Is there a specific
topic or...

4
00:00:08,599 --> 00:00:11,699
You're the one that wanted to
hijack my video today.

5
00:00:12,899 --> 00:00:13,999
Fair point.

6
00:00:15,699 --> 00:00:16,999
Hello everyone.

7
00:00:18,799 --> 00:00:19,699
As you might surmise from

8
00:00:19,699 --> 00:00:21,699
Lizzie's last videos,
she and I have

9
00:00:21,699 --> 00:00:24,810
begun... a relationship.

10
00:00:25,599 --> 00:00:27,799
And it's been six days now...
- It's been a week.

11
00:00:29,799 --> 00:00:31,999
No, it's been six days.
I have it in my calendar.

12
00:00:32,465 --> 00:00:34,665
It's been a week!

13
00:00:35,799 --> 00:00:37,699
We kissed on my birthday,

14
00:00:37,699 --> 00:00:38,699
one week ago.

15
00:00:38,699 --> 00:00:41,599
Kissing you marks the inception
of our relationship.

16
00:00:41,699 --> 00:00:43,175
Yes, but that was
in the evening.

17
00:00:43,799 --> 00:00:44,799
It wasn't a full day.

18
00:00:45,175 --> 00:00:46,799
Partial days count!

19
00:00:46,799 --> 00:00:48,799
We're rounding up,
so it's been a week.

20
00:00:50,509 --> 00:00:51,509
Fine.

21
00:00:51,599 --> 00:00:53,799
It has been a week,

22
00:00:53,799 --> 00:00:54,799
now.

23
00:00:55,599 --> 00:00:57,799
And it has been
without a doubt...

24
00:00:58,092 --> 00:00:59,799
the best week of my life.

25
00:01:02,217 --> 00:01:04,509
My name is William Darcy and...

26
00:01:07,599 --> 00:01:08,999
Lizzie Bennet is amazing.

27
00:01:15,599 --> 00:01:17,599
You know I'm just
teasing you, right?

28
00:01:17,599 --> 00:01:18,599
About the week 
versus six days.

29
00:01:18,799 --> 00:01:19,799
I know.

30
00:01:20,599 --> 00:01:21,799
I've been learning.

31
00:01:21,799 --> 00:01:22,999
Slowly.

32
00:01:23,499 --> 00:01:24,799
Eat enough dinners
in the Bennet household

33
00:01:24,799 --> 00:01:26,799
and you learn plenty about
being teased.

34
00:01:26,799 --> 00:01:29,175
That just means
my dad likes you!

35
00:01:29,559 --> 00:01:31,099
He did invite you
to see his trains.

36
00:01:31,599 --> 00:01:34,599
Still, had I known about the
casual nature of social interaction

37
00:01:34,599 --> 00:01:36,299
in the Bennet family,

38
00:01:36,399 --> 00:01:39,042
I might have better understood
what I was doing wrong.

39
00:01:39,299 --> 00:01:41,799
You didn't do
anything wrong.

40
00:01:42,799 --> 00:01:45,799
I believe you typify
our first meeting as...

41
00:01:45,799 --> 00:01:48,199
the most awkward dance ever.

42
00:01:49,799 --> 00:01:51,099
Oh, yeah!

43
00:01:51,799 --> 00:01:54,050
That was really bad!

44
00:01:54,799 --> 00:01:56,526
Not my finest hour.

45
00:01:57,034 --> 00:01:58,799
But you didn't wanna
to like me then.

46
00:01:58,799 --> 00:02:01,799
You wanted to take Bing
and get the hell out of dodge.

47
00:02:02,799 --> 00:02:04,799
My mind changed soon enough.

48
00:02:05,599 --> 00:02:06,799
When?

49
00:02:07,959 --> 00:02:09,301
When?

50
00:02:10,134 --> 00:02:12,667
When did you change
your mind about me?

51
00:02:14,883 --> 00:02:16,799
I haven't a clue!

52
00:02:17,967 --> 00:02:19,799
I honestly don't remember when.

53
00:02:19,883 --> 00:02:22,799
I don't think there was 
one specific moment.

54
00:02:24,799 --> 00:02:27,390
I was in the middle of it 
before I knew that I had begun.

55
00:02:29,799 --> 00:02:30,799
What about you?

56
00:02:31,267 --> 00:02:33,718
When did you discover
your feelings had changed?

57
00:02:34,599 --> 00:02:36,799
Oh... I think...

58
00:02:37,599 --> 00:02:41,632
The moment I saw the offices
at Pemberley Digital.

59
00:02:41,799 --> 00:02:45,196
You should know, 
those napping pods?

60
00:02:45,199 --> 00:02:47,325
Women swoon!

61
00:02:50,299 --> 00:02:52,209
No, like you...

62
00:02:52,599 --> 00:02:54,009
it happened...

63
00:02:54,599 --> 00:02:55,799
gradually.

64
00:02:56,299 --> 00:02:57,899
Until...

65
00:02:58,059 --> 00:02:59,799
one day...

66
00:03:00,599 --> 00:03:02,799
things were just...

67
00:03:05,799 --> 00:03:07,799
We should probably
change that topic.

68
00:03:07,799 --> 00:03:09,799
Good plan!

69
00:03:11,799 --> 00:03:12,799
Well...

70
00:03:13,799 --> 00:03:15,799
I've been thinking
about the future.

71
00:03:15,799 --> 00:03:17,634
There's a mood changer.

72
00:03:18,599 --> 00:03:19,899
As wonderful

73
00:03:20,799 --> 00:03:22,724
as this week has been,

74
00:03:22,724 --> 00:03:24,565
I can only telecommute
for so long before

75
00:03:24,565 --> 00:03:26,565
my investors start
to think I'm...

76
00:03:26,565 --> 00:03:29,765
neglecting my responsibilities
for my own personal indulgences.

77
00:03:29,765 --> 00:03:31,765
Which technically, you are.

78
00:03:32,399 --> 00:03:33,799
Point being...

79
00:03:34,799 --> 00:03:36,634
I need to get back soon.

80
00:03:37,399 --> 00:03:38,799
And I know you need
to finish up

81
00:03:38,799 --> 00:03:40,799
your degree, which,
according to my calendar,

82
00:03:41,099 --> 00:03:43,890
will be in about 
the next six weeks time.

83
00:03:43,925 --> 00:03:47,799
Which terrifies me
in no way whatsoever.

84
00:03:49,499 --> 00:03:50,799
Well, I was thinking
it might terrify you less

85
00:03:50,799 --> 00:03:52,342
if you had a job lined up.

86
00:03:53,799 --> 00:03:56,399
Say at... Pemberley Digital.

87
00:03:57,799 --> 00:03:59,799
Oh. Wow!

88
00:04:00,499 --> 00:04:02,599
I would set up an
entire department around you,

89
00:04:02,599 --> 00:04:04,699
you could have access
to state-of-the-art equipment.

90
00:04:04,899 --> 00:04:07,378
Not to mention you'd be
in San Francisco with me.

91
00:04:09,151 --> 00:04:10,676
What do you say?

92
00:04:13,799 --> 00:04:14,799
No.

93
00:04:18,799 --> 00:04:21,799
Is this one of those times when
you're teasing me? I cannot tell.

94
00:04:22,799 --> 00:04:23,799
I don't wanna work

95
00:04:23,799 --> 00:04:26,799
at Pemberley Digital,
as amazing as it is.

96
00:04:27,799 --> 00:04:29,799
I want to be with you.

97
00:04:30,134 --> 00:04:33,299
But I don't want to be 
the girl who dates the boss.

98
00:04:34,599 --> 00:04:35,999
Does that make sense?

99
00:04:40,925 --> 00:04:41,925
Yes.

100
00:04:45,599 --> 00:04:47,799
Well, what do you think
you will do when you graduate?

101
00:04:48,799 --> 00:04:49,799
Yeah...

102
00:04:49,799 --> 00:04:51,599
funny thing.

103
00:04:51,599 --> 00:04:52,975
When you have a video diary

104
00:04:52,975 --> 00:04:54,800
that gets millions of views,

105
00:04:54,842 --> 00:04:58,351
it attracts the attention
of several people

106
00:04:58,392 --> 00:05:00,009
who run digital media companies.

107
00:05:00,799 --> 00:05:03,799
So you wanna work
for my competitors?

108
00:05:03,799 --> 00:05:06,799
Actually, 
I was thinking of

109
00:05:06,799 --> 00:05:08,799
becoming one of
your competitors.

110
00:05:11,392 --> 00:05:13,291
Since I wrote
about the diaries

111
00:05:13,291 --> 00:05:15,772
as a start-up for
my final independent study,

112
00:05:15,799 --> 00:05:17,699
I have the business plan
in place.

113
00:05:17,699 --> 00:05:20,284
And since I mentioned
that project on the Internet,

114
00:05:20,799 --> 00:05:23,799
I have been getting messages
from potential investors.

115
00:05:25,299 --> 00:05:26,999
I might know
some investors too.

116
00:05:31,799 --> 00:05:34,676
I was also thinking that...

117
00:05:35,799 --> 00:05:38,799
If I'm going to be starting
my own company, that...

118
00:05:40,217 --> 00:05:43,999
San Francisco might be
a great place to do it.

119
00:05:44,800 --> 00:05:46,599
Optimal, even.

120
00:05:49,599 --> 00:05:51,925
I think I'm warming up
to this vision of the future.

121
00:05:52,799 --> 00:05:54,799
Be aware that my vision 
of the future includes

122
00:05:54,799 --> 00:05:57,342
lots of trips home
and visits from Lydia.

123
00:05:57,599 --> 00:06:00,225
I'm really enjoying getting
to know my little sister better.

124
00:06:00,799 --> 00:06:01,999
Of course.

125
00:06:03,599 --> 00:06:06,188
I should apologize for anything
I ever said about her.

126
00:06:06,599 --> 00:06:08,799
You don't think
she's energetic?

127
00:06:09,799 --> 00:06:11,054
No, she is. But...

128
00:06:11,054 --> 00:06:12,937
then again, so is Gigi.

129
00:06:13,399 --> 00:06:15,699
Just took a while for me
to see the similarities.

130
00:06:16,376 --> 00:06:18,799
To see anything
other than my first impression.

131
00:06:21,899 --> 00:06:22,942
So...

132
00:06:23,926 --> 00:06:25,799
sound like a good future?

133
00:06:27,799 --> 00:06:29,747
I think it will do nicely.

134
00:06:30,799 --> 00:06:33,799
But right now, we need to worry
about the present.

135
00:06:35,499 --> 00:06:38,301
I made a reservation for 
our one week anniversary.

136
00:06:38,799 --> 00:06:40,699
I thought you said
it was six days.

137
00:06:42,399 --> 00:06:43,999
My aunt Catherine
is in town.

138
00:06:44,499 --> 00:06:46,899
I thought it might be nice
to get dinner with her.

139
00:06:47,499 --> 00:06:49,799
I think she and your mother
would get along well.

140
00:06:52,599 --> 00:06:53,799
Very funny!

141
00:06:53,842 --> 00:06:56,634
But I can tell
when you're teasing.

142
00:07:00,259 --> 00:07:03,799
You're... You're teasing. Right?

143
00:07:05,799 --> 00:07:07,799
- Right?
- We have to go!