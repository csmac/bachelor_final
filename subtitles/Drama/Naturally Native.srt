1
00:00:04,050 --> 00:00:07,076
How you choose to express yourself

2
00:00:07,076 --> 00:00:11,009
It's all your own and I can tell

3
00:00:11,009 --> 00:00:14,016
It comes naturally

4
00:00:14,016 --> 00:00:18,080
It comes naturally

5
00:00:18,080 --> 00:00:22,010
You follow what you feel inside

6
00:00:22,010 --> 00:00:25,056
It's intuitive, you don't have to try

7
00:00:25,056 --> 00:00:29,016
It comes naturally

8
00:00:29,016 --> 00:00:32,083
It comes naturally

9
00:00:32,083 --> 00:00:40,020
And it takes my breath away

10
00:00:40,020 --> 00:00:45,060
You are the thunder and I am the lightning

11
00:00:45,060 --> 00:00:47,083
And I love the way you

12
00:00:47,083 --> 00:00:52,079
Know who you are and to me it's exciting

13
00:00:52,079 --> 00:00:55,060
When you know it's meant to be

14
00:00:55,060 --> 00:00:59,036
Everything comes naturally, it comes naturally

15
00:00:59,036 --> 00:01:02,063
When you're with me, baby

16
00:01:02,063 --> 00:01:07,096
Everything comes naturally, it comes naturally

17
00:01:07,096 --> 00:01:11,036
Bay-bay-baby

18
00:01:11,036 --> 00:01:14,076
You have a way of moving me

19
00:01:14,076 --> 00:01:18,006
A force of nature, your energy

20
00:01:18,006 --> 00:01:21,033
It comes naturally

21
00:01:21,033 --> 00:01:25,036
It comes naturally, yeah

22
00:01:25,036 --> 00:01:32,020
And it takes my breath away

23
00:01:32,020 --> 00:01:37,006
What you do, so naturally

24
00:01:37,006 --> 00:01:41,070
You are the thunder and I am the lightning

25
00:01:41,070 --> 00:01:43,083
And I love the way you

26
00:01:43,083 --> 00:01:48,083
Know who you are and to me it's exciting

27
00:01:48,083 --> 00:01:51,076
When you know it's meant to be

28
00:01:51,076 --> 00:01:55,083
Everything comes naturally, it comes naturally

29
00:01:55,083 --> 00:01:58,076
When you're with me, baby

30
00:01:58,076 --> 00:02:04,009
Everything comes naturally, it comes naturally

31
00:02:04,009 --> 00:02:06,066
Bay-bay-baby

32
00:02:06,066 --> 00:02:09,076
When we collide sparks fly

33
00:02:09,076 --> 00:02:12,053
When you look in my eyes

34
00:02:12,053 --> 00:02:20,009
It takes my breath away

35
00:02:20,009 --> 00:02:24,093
You are the thunder and I am the lightning

36
00:02:24,093 --> 00:02:26,040
And I love the way you

37
00:02:26,040 --> 00:02:31,096
Know who you are and to me it's exciting

38
00:02:31,096 --> 00:02:34,090
When you know it's meant to be

39
00:02:34,090 --> 00:02:39,013
Everything comes naturally, it comes naturally

40
00:02:39,013 --> 00:02:41,096
When you're with me, baby

41
00:02:41,096 --> 00:02:47,033
Everything comes naturally, it comes naturally

42
00:02:47,033 --> 00:02:49,026
Bay-bay-baby

43
00:02:49,026 --> 00:02:51,059
Naturally

44
00:02:51,059 --> 00:02:52,083
Naturally

45
00:02:52,083 --> 00:02:54,033
Naturally

46
00:02:54,033 --> 00:02:57,069
Everything, baby, comes naturally

47
00:02:57,069 --> 00:02:59,036
Naturally

48
00:02:59,036 --> 00:03:01,076
Naturally

49
00:03:01,080 --> 00:03:03,006
bay bay baby
