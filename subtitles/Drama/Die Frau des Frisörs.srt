1
00:00:00,002 --> 00:00:00,003
- The Cave of the Yellow Dog (2005)
- IMDB #0432325
- English audio dubbed version.
- fps = 25.00 or 30.00
- Length = 1:30:23
- First line = 1:57,684 Last line = 1:27:08,289
- Created by DonToribio (Subscence.Com moniker)
- Posted FIRST to Subscene November 23, 2012
- Credit to Subscener MovieManiac for timings and
- A base to get me off to a good start!

2
00:01:57,684 --> 00:02:00,619
Dad, what are you doing
with his tail?

3
00:02:02,388 --> 00:02:03,956
I'm putting it under his head...

4
00:02:04,057 --> 00:02:06,924
so he'll be reborn as a person
with a ponytail,

5
00:02:07,026 --> 00:02:09,226
and not as a dog with a tail.

6
00:02:09,261 --> 00:02:10,762
Oh, really?

7
00:02:15,001 --> 00:02:19,529
Everyone eventually dies,
but no one is ever really dead.

8
00:02:33,052 --> 00:02:34,246
Let's go.

9
00:03:03,349 --> 00:03:09,549
THE CAVE OF THE YELLOW DOG

10
00:03:31,641 --> 00:03:33,403
<i>Now the ladle's broken, too.</i>

11
00:03:47,927 --> 00:03:50,494
<i>Chuluna, come here.
Look at this.</i>

12
00:03:52,832 --> 00:03:54,060
<i>Hurry up!</i>

13
00:05:13,246 --> 00:05:17,512
There you go, you're home.

14
00:05:18,985 --> 00:05:20,311
Hello, Mom!

15
00:05:21,154 --> 00:05:23,333
Look at you my daughter.

16
00:05:23,734 --> 00:05:25,513
You got so big.

17
00:05:26,292 --> 00:05:27,691
Hey, Dad!

18
00:05:27,794 --> 00:05:29,728
My big girl!
Did you have a good trip?

19
00:05:29,829 --> 00:05:31,956
- Yes.
- How did you get here?

20
00:05:32,065 --> 00:05:33,936
- Dad! I had a driver.
- Ah!

21
00:05:35,234 --> 00:05:37,668
- Did you miss us?
- Yes! A lot!

22
00:05:37,770 --> 00:05:39,704
- Did you learn a lot?
- Yes.

23
00:05:43,176 --> 00:05:45,337
Your uniform looks
so good on you.

24
00:05:46,079 --> 00:05:47,307
Yes, it really does.

25
00:05:47,980 --> 00:05:50,505
Yes, but the collar is really tight.

26
00:05:50,616 --> 00:05:52,348
It's supposed to be like that.

27
00:05:52,852 --> 00:05:54,446
Hold on, I've got your deel.

28
00:05:54,754 --> 00:05:56,792
Hey, Dad?

29
00:05:58,791 --> 00:06:01,087
- Yes, Little One?
- What were you doing outside?

30
00:06:01,194 --> 00:06:02,927
Oh, I was watching the sheep.

31
00:06:04,831 --> 00:06:06,980
That big wolf come back?

32
00:06:08,868 --> 00:06:10,292
It did.

33
00:06:12,538 --> 00:06:14,563
Did you learn a lot of new songs?

34
00:06:14,674 --> 00:06:15,800
Yes.

35
00:06:15,908 --> 00:06:18,138
Teach 'em to your sister, all right?

36
00:06:18,244 --> 00:06:20,211
Here, let me help you with this.

37
00:06:28,488 --> 00:06:32,751
Hey, Dad! Do you want to have a look
at the books I used at school?

38
00:06:32,786 --> 00:06:34,442
Yes! I do!

39
00:06:40,566 --> 00:06:42,625
You've really gotten big.

40
00:06:42,935 --> 00:06:44,760
Show me the books.

41
00:06:48,574 --> 00:06:51,652
- Let's see.
- Okay

42
00:06:53,287 --> 00:06:55,266
There ya go. Look.

43
00:06:57,116 --> 00:06:58,947
Is this your math book?

44
00:06:59,351 --> 00:07:00,780
Your writing is so good.

45
00:07:00,887 --> 00:07:04,085
Look, I got a star on this page here.

46
00:07:04,120 --> 00:07:05,751
You did? Let me see.

47
00:07:08,394 --> 00:07:09,585
That's great.

48
00:07:10,830 --> 00:07:12,671
And another star!

49
00:07:55,062 --> 00:07:57,171
Mom, can I have some dung?

50
00:07:57,276 --> 00:07:58,800
Of course, take some.

51
00:08:03,183 --> 00:08:06,513
Sister, come here, let's play with this!

52
00:08:08,788 --> 00:08:13,691
We'll can build the houses on top of each other,
like I saw in the city when I was going to school.

53
00:08:13,793 --> 00:08:14,760
Okay.

54
00:08:16,162 --> 00:08:22,567
If we were there in the city,
we would live way up at the top.

55
00:08:22,668 --> 00:08:27,302
Because it's much too dark down by the street.

56
00:08:28,841 --> 00:08:34,339
Like this. Pile them on. Okay.

57
00:08:35,097 --> 00:08:36,854
One on top!

58
00:09:11,250 --> 00:09:12,165
<i>Nansal!</i>

59
00:09:12,166 --> 00:09:13,081
Yes!

60
00:09:13,185 --> 00:09:15,653
<i>Can you help me, I need you to
collect some dung to cure the meat?</i>

61
00:09:15,755 --> 00:09:19,386
But Mom, I've never done that!
I don't know what I'm looking for.

62
00:09:19,592 --> 00:09:21,257
<i>Just give it a try, okay?</i>

63
00:09:22,328 --> 00:09:25,275
Come on, Little One,
get up!

64
00:09:44,317 --> 00:09:46,452
- Nansal!
- Yes.

65
00:09:46,477 --> 00:09:49,235
Don't get too far away, please.
- Okay.

66
00:10:07,940 --> 00:10:11,425
- Those two vultures are still circling.
- Yeah.

67
00:10:11,460 --> 00:10:13,036
- Hello, my friends!
- Hey, how are you?

68
00:10:13,078 --> 00:10:14,678
Hi, hello to you.

69
00:10:16,749 --> 00:10:19,011
- How's the grazing?
- Very good.

70
00:10:19,118 --> 00:10:20,380
Been a good summer for you?

71
00:10:20,486 --> 00:10:23,007
- Yes, and you?
- It's been good.

72
00:10:23,042 --> 00:10:25,251
- So, how's the hunting going for you?
- Okay.

73
00:10:25,287 --> 00:10:27,090
- Not bad.
- Yeah.

74
00:10:27,326 --> 00:10:30,131
- Where have you been?
- We were at Baruun Go!

75
00:10:30,432 --> 00:10:31,465
Thanks.

76
00:10:32,164 --> 00:10:34,559
We had to go there to hunt wolves again.

77
00:10:34,560 --> 00:10:37,080
There were a lot of them
there this time, yeah.

78
00:10:37,115 --> 00:10:40,863
So, tell me my friend, is it peaceful by you?

79
00:10:40,898 --> 00:10:43,305
I'm afraid it's not very good.

80
00:10:44,410 --> 00:10:46,546
There's a lot of wolves right now.

81
00:10:46,779 --> 00:10:48,974
More and more every year, it seems to me.

82
00:10:50,816 --> 00:10:52,465
I agree.

83
00:12:35,388 --> 00:12:37,494
Oops! Almost fell down.

84
00:13:06,986 --> 00:13:09,348
Hey! Who's in there?

85
00:13:16,228 --> 00:13:19,595
Seems like everyone is selling-off their herds.

86
00:13:19,698 --> 00:13:21,598
- Yes, I know.
- And moving on.

87
00:13:24,236 --> 00:13:27,164
You know, I really hate to admit it,
but I think that times are changing.

88
00:13:27,273 --> 00:13:31,240
- It's true, I have to agree with you, you're right.
- Only stray dogs are here.

89
00:13:31,277 --> 00:13:35,670
The worst part is those dogs are mixing-in
with the wolves, becoming part of the pack.

90
00:13:35,881 --> 00:13:38,748
I know, just last night they
got two more of my sheep.

91
00:13:38,851 --> 00:13:40,487
Ah, that's bad.

92
00:13:40,822 --> 00:13:42,545
- It's been happening everywhere lately.
- You're right.

93
00:13:42,655 --> 00:13:46,439
Something needs to be done about it
soon or it will really get out of hand.

94
00:13:46,559 --> 00:13:47,953
- Right.
- I agree with you.

95
00:13:48,060 --> 00:13:51,305
When I was much younger, we all
used to fight the wolves together.

96
00:13:51,365 --> 00:13:55,134
But now, unfortunately, fewer
and fewer people live around here,

97
00:13:55,169 --> 00:13:58,059
so it's becoming a much
bigger problem to deal with.

98
00:13:58,060 --> 00:13:59,060
- Difficult.
- That's true.

99
00:13:59,070 --> 00:14:00,630
I have to go check on my sheep.

100
00:14:01,107 --> 00:14:03,537
- Right.
- You get to town much?

101
00:14:03,642 --> 00:14:08,350
Oh, no, no, not really, but tomorrow
I'm heading into town to sell some skins.

102
00:14:08,885 --> 00:14:12,281
Say, I ordered some ammunition
with Badam.

103
00:14:12,316 --> 00:14:14,099
Could you pick it up for me
if you get the chance?

104
00:14:14,120 --> 00:14:14,567
Sure.

105
00:14:14,633 --> 00:14:16,544
- I'd appreciate that.
- No problem at all.

106
00:14:18,591 --> 00:14:20,254
I'm gonna take you home.

107
00:14:26,565 --> 00:14:28,392
Wait! Wait! Wait! Here ya go!

108
00:14:31,793 --> 00:14:34,578
You're so soft, you feel really good.

109
00:14:35,680 --> 00:14:37,295
Wait! Wait! Wait up!

110
00:14:39,845 --> 00:14:43,008
Here, Boy! Come on! Let's go!

111
00:14:54,593 --> 00:15:00,194
Hey! Come back here now! Back!
Get back! Get Back! Get back! Come on!

112
00:15:03,268 --> 00:15:05,782
There ya go!
Gotcha!

113
00:15:07,640 --> 00:15:09,684
Where's your mother, huh?

114
00:15:12,111 --> 00:15:16,648
Do you know where?
Where is she, huh?

115
00:15:36,802 --> 00:15:39,737
Nansal, what have you got there?
Where did you get that dog?

116
00:15:41,140 --> 00:15:45,076
I found him out there.
I want to play with him.

117
00:15:45,177 --> 00:15:49,637
Listen, you were supposed to collecting dung,
young lady, but instead it seems you were playing.

118
00:15:49,649 --> 00:15:52,080
What if that dog's owner wants him back now?

119
00:15:52,651 --> 00:15:54,812
I won't give him back!

120
00:15:54,820 --> 00:15:58,219
Is that right? Well, he is a little cutie.

121
00:15:59,024 --> 00:16:02,101
I wonder if he got lost and
wandered away or something.

122
00:16:10,336 --> 00:16:12,270
Come on, Spotty.

123
00:16:13,305 --> 00:16:17,613
Well, maybe someone left him behind
when it was time for them to move on.

124
00:16:18,174 --> 00:16:19,800
He looks a little hungry.

125
00:16:20,613 --> 00:16:25,450
Give me your belt.
I'm gonna give it to  him.

126
00:16:31,815 --> 00:16:32,621
Here.

127
00:16:39,932 --> 00:16:43,928
I gonna name him Spot,
because he has spots, see?

128
00:17:04,656 --> 00:17:06,055
Hey, Nansal!

129
00:17:07,559 --> 00:17:10,187
- Get rid of this dog, please.
- Uh-uh!

130
00:17:10,996 --> 00:17:14,876
What do you mean, "No"?
Take him back, like I asked you to.

131
00:17:15,401 --> 00:17:16,959
No.

132
00:17:17,069 --> 00:17:19,299
He was probably living with wolves.

133
00:17:19,405 --> 00:17:21,930
There were no wolves
where I found him.

134
00:17:22,875 --> 00:17:25,318
- Oh, really?
- Uh-huh.

135
00:17:25,353 --> 00:17:27,141
Only wolves live in caves.

136
00:17:28,614 --> 00:17:29,979
But I found him.

137
00:17:32,751 --> 00:17:34,776
Now he belongs to me.

138
00:17:42,394 --> 00:17:44,339
Why weren't you stricter with her?

139
00:17:45,264 --> 00:17:47,828
If you'd said no then it'd be easier now.

140
00:17:49,768 --> 00:17:51,032
It could be destiny,

141
00:17:52,140 --> 00:17:54,511
and maybe that's why
he came to us right now.

142
00:18:23,602 --> 00:18:24,762
Get away from that!

143
00:18:29,241 --> 00:18:30,799
Get away! Go!

144
00:20:44,576 --> 00:20:48,444
When you get back tomorrow,
we'll start getting ready for the move, all right?

145
00:20:48,547 --> 00:20:49,514
All right.

146
00:20:53,652 --> 00:20:56,177
Do you need anything from town?
Flour or rice?

147
00:20:57,089 --> 00:21:00,688
I think that can wait until we've moved on,
but I do need a new ladle.

148
00:21:00,792 --> 00:21:01,781
All right.

149
00:21:14,172 --> 00:21:15,802
Make sure the dog goes away.

150
00:21:16,975 --> 00:21:19,739
- Bye! Take care of yourselves!
-  Be careful!

151
00:23:54,292 --> 00:23:55,553
That really hurt.

152
00:24:07,045 --> 00:24:09,241
Nansal, help with the cheese?

153
00:24:16,121 --> 00:24:18,214
I'll lift it up then you pull it out.

154
00:24:20,024 --> 00:24:21,213
Come on.

155
00:24:23,195 --> 00:24:25,295
A little more.

156
00:24:25,697 --> 00:24:27,255
Good job. Okay.

157
00:24:28,567 --> 00:24:30,598
Bring me the wooden board, please.

158
00:24:42,380 --> 00:24:43,904
Be careful, don't fall.

159
00:24:48,357 --> 00:24:50,146
Just put it here. Yeah.

160
00:24:52,058 --> 00:24:54,591
Now, gently pull it up.

161
00:24:55,527 --> 00:24:58,515
Now, stand it up like this.
Yes, just like that.

162
00:25:06,671 --> 00:25:09,539
- Do you cut it in half first?
- Not yet.

163
00:25:15,146 --> 00:25:18,632
- This turned out pretty well.
- That's good.

164
00:25:24,823 --> 00:25:30,673
Know what, I hid Spot in the herd so Dad
wouldn't find him before he went away.

165
00:25:38,570 --> 00:25:40,943
I'm afraid that's just not good enough.

166
00:25:40,944 --> 00:25:43,016
You're going to have to take
him back before Father gets home.

167
00:25:43,051 --> 00:25:48,074
How come you don't want me to keep him?
Why are you being so mean?

168
00:25:49,581 --> 00:25:53,108
He's so cute. I want him.

169
00:25:54,519 --> 00:25:56,749
It's not about wanting him to stay here.

170
00:25:59,224 --> 00:26:03,125
- That should be good enough for now. Let go.
- Okay

171
00:26:06,765 --> 00:26:09,359
Stretch your hand, just like this.

172
00:26:10,769 --> 00:26:14,102
Now try to bite the palm of
your hand. Right there.

173
00:26:18,076 --> 00:26:19,307
I can't do it.

174
00:26:19,379 --> 00:26:22,706
You can't? Are you really
sure you can't. Try it again.

175
00:26:26,618 --> 00:26:29,009
And now? You still can't?

176
00:26:29,154 --> 00:26:30,746
It doesn't work.

177
00:26:30,855 --> 00:26:34,484
Although it seems like it's close,
it's still too far away to bite.

178
00:26:34,993 --> 00:26:37,655
The truth is, you can't have everything.

179
00:26:44,101 --> 00:26:45,696
I can't do it!

180
00:27:32,283 --> 00:27:34,071
- Nansal!
- Yes.

181
00:27:35,135 --> 00:27:37,186
Remember not to leave any of
the young animals behind.

182
00:27:39,290 --> 00:27:42,309
Always keep that mountain in your sight.

183
00:27:42,310 --> 00:27:45,329
And please try not to
get lost this time, okay?

184
00:27:45,330 --> 00:27:46,630
- Yeah.
- You hear me?

185
00:27:47,700 --> 00:27:52,099
- Huh?
- Dad already taught all this stuff to me, Mom.

186
00:27:52,134 --> 00:27:54,628
The tip of the mountain
is my landmark.

187
00:27:54,739 --> 00:27:57,294
Oh! Well, you are a smart girl, aren't you?

188
00:28:03,222 --> 00:28:05,364
Off you go, ride carefully.

189
00:28:14,359 --> 00:28:19,895
Spot, come on! Spot! Here! Here now,
over here! Here! Come on!

190
00:29:16,154 --> 00:29:17,662
- Mom?
- Huh?

191
00:29:17,829 --> 00:29:21,103
Why didn't you wake me up before?

192
00:29:21,138 --> 00:29:23,534
Well, why did you want
to get up so early?

193
00:29:23,828 --> 00:29:26,489
- Because I...
- Uh-huh?

194
00:29:27,599 --> 00:29:32,181
I...I wanted to go to town with Dad.

195
00:29:32,216 --> 00:29:34,449
All right. Why did you want to do that?

196
00:29:35,006 --> 00:29:36,667
So I could look at all the houses.

197
00:29:36,775 --> 00:29:40,053
Oh! Is that so?
And why do you want to do that?

198
00:29:42,778 --> 00:29:47,045
People can go pee in their houses in the city.

199
00:29:47,152 --> 00:29:51,816
Is that right? So, people in the city
can pee inside their houses?

200
00:29:52,824 --> 00:29:54,451
Nansal said so.

201
00:29:54,559 --> 00:29:55,884
Oh, I see.

202
00:30:14,212 --> 00:30:17,503
Can you bite my palm?
Go ahead, give it a try.

203
00:30:21,085 --> 00:30:22,295
Spot, try it.

204
00:30:23,087 --> 00:30:25,548
Go on! Just give it a try, okay?

205
00:30:30,428 --> 00:30:32,355
Go on! You have to do it just once.

206
00:30:41,339 --> 00:30:44,206
You can't do it either, Spot, can you now, Boy?

207
00:30:50,615 --> 00:30:52,913
Spot! Spot!

208
00:30:53,952 --> 00:31:00,343
Now Spot, do you want to go back to your cave
or do you want to stay here with us? Huh?

209
00:31:01,459 --> 00:31:02,638
Huh! Do you?

210
00:31:03,227 --> 00:31:06,530
Does that taste good?
Huh, boy?

211
00:31:11,715 --> 00:31:13,106
- Uh, Mom...
- Ah-huh.

212
00:31:14,610 --> 00:31:20,503
Can I wear the deel
that you're sewing now?

213
00:31:22,113 --> 00:31:25,742
It's going to be too big for you.
It's for your sister.

214
00:31:26,784 --> 00:31:30,207
- She's got to go back to school soon.
- Oh, yeah.

215
00:31:34,259 --> 00:31:37,251
Will she be gone a long time?

216
00:31:37,362 --> 00:31:39,685
Yes, she'll be gone a very long time.

217
00:31:40,286 --> 00:31:42,067
Again.

218
00:31:44,302 --> 00:31:46,202
Then I'll miss her.

219
00:31:46,304 --> 00:31:48,101
So will I.

220
00:31:54,379 --> 00:31:57,524
Your brother's crying, go see what it is, please.

221
00:31:58,025 --> 00:31:59,957
Be quiet, will you!

222
00:33:11,856 --> 00:33:13,646
Where did he get to?

223
00:33:36,047 --> 00:33:37,339
Here, Spot!

224
00:33:46,057 --> 00:33:47,649
Where are you now?

225
00:34:02,540 --> 00:34:05,135
<i>Spot! Spot!</i>

226
00:34:14,285 --> 00:34:15,809
Stay here.

227
00:34:54,659 --> 00:34:58,368
Spot! Spot!

228
00:35:01,532 --> 00:35:02,624
Hey, Spot!

229
00:35:11,642 --> 00:35:12,836
Hey, Spot!

230
00:35:13,945 --> 00:35:15,037
Hey!

231
00:35:23,955 --> 00:35:25,513
Just a minute, Little One.

232
00:35:32,830 --> 00:35:34,442
Here, let me feed you.

233
00:35:36,100 --> 00:35:37,408
Come on.

234
00:35:38,312 --> 00:35:39,979
No, let me do it.

235
00:35:41,283 --> 00:35:42,938
Come on, eat your food.

236
00:35:46,787 --> 00:35:51,923
Eat up, that's good. You're eating.
Good girl. Good job.

237
00:35:53,751 --> 00:35:58,206
The herd came back on its own.
I have to find your sister.

238
00:35:59,701 --> 00:36:00,304
'Kay.

239
00:36:00,339 --> 00:36:02,553
- Can you watch your brother?
- 'Kay.

240
00:36:13,337 --> 00:36:14,834
Do you want more?

241
00:36:23,083 --> 00:36:24,019
There.

242
00:36:25,126 --> 00:36:26,871
Are you finished?

243
00:37:49,133 --> 00:37:50,225
Spot!

244
00:38:04,248 --> 00:38:07,212
Yeah! There you are! Hi, Spot!

245
00:38:08,219 --> 00:38:09,982
What are you doing in here?

246
00:38:13,591 --> 00:38:17,269
I was so afraid that you left me
and went back to your cave.

247
00:38:17,362 --> 00:38:22,648
Hey! You think you can just sleep anywhere? Huh?

248
00:38:26,837 --> 00:38:29,827
Hey, Spot! We have to go home.

249
00:38:30,841 --> 00:38:32,741
Wake up, Lazy!

250
00:39:17,922 --> 00:39:22,256
Stop it! Stop banging on it.
We aren't allowed to play with this one.

251
00:39:23,227 --> 00:39:24,761
You don't play with God.

252
00:39:24,862 --> 00:39:26,591
I'm gonna tell Mom!

253
00:39:30,292 --> 00:39:33,213
I told you not to.

254
00:41:28,252 --> 00:41:32,689
It'll dry nicely here.

255
00:42:12,564 --> 00:42:15,559
Go on. Take a big sip.

256
00:42:38,174 --> 00:42:39,304
Spot.

257
00:42:52,570 --> 00:42:56,768
You're lucky he didn't end up
in the Cave of the Yellow Dog.

258
00:43:00,176 --> 00:43:02,643
What's Cave of the Yellow Dog mean?

259
00:43:03,047 --> 00:43:04,607
I'll tell you the story.

260
00:43:05,084 --> 00:43:12,234
Many, many years ago, a clan of great
wealth lived nearby on these very lands.

261
00:43:12,623 --> 00:43:15,592
And they had a truly beautiful daughter.

262
00:43:16,193 --> 00:43:19,651
One day,
the daughter became very ill.

263
00:43:20,363 --> 00:43:22,964
There was no medicine that could cure her.

264
00:43:25,069 --> 00:43:29,362
So, the father decided
to seek the advice of a very wise man.

265
00:43:29,773 --> 00:43:35,856
The wise, old man spoke. He said, "Your yellow
dog is angry. You must get rid of it."

266
00:43:36,691 --> 00:43:42,532
Confused, the father replied, "But why?
He always protects our herd and my family very well."

267
00:43:43,406 --> 00:43:48,961
And the wise man answered, "That's what I have to
say and what you wanted to know."

268
00:43:48,996 --> 00:43:52,043
The father didn't want to kill the yellow dog...

269
00:43:52,087 --> 00:43:54,522
but he had to do something.

270
00:43:55,699 --> 00:44:00,102
So he hid it in a cave where it was
impossible for the dog to get away.

271
00:44:00,204 --> 00:44:04,944
He carried in food daily.
But one day, the dog wasn't there.

272
00:44:04,945 --> 00:44:09,145
The cave had nothing in it. No one knows
where the yellow dog went.

273
00:44:11,980 --> 00:44:14,103
Eventually the daughter was healthy.

274
00:44:14,338 --> 00:44:15,685
And the reason was...

275
00:44:16,375 --> 00:44:19,639
that the daughter had fallen
in love with a young man.

276
00:44:19,792 --> 00:44:22,164
So, now, with the yellow dog gone...

277
00:44:22,529 --> 00:44:25,881
the couple could meet
without being disturbed.

278
00:44:26,082 --> 00:44:32,317
So, Young One, the point of this story is
that quite often what seems to be going on...

279
00:44:32,318 --> 00:44:35,373
may not really be everything that is happening.

280
00:44:41,011 --> 00:44:43,202
I think the rain is over now.

281
00:44:49,920 --> 00:44:52,549
<i>My daughter took over for her father
today, and look what happened.</i>

282
00:44:52,690 --> 00:44:55,722
- Oh, it's fine.
- Thanks for watching her.

283
00:44:55,826 --> 00:44:58,192
Goodbye. Travel safely.

284
00:45:18,048 --> 00:45:19,276
Spot! Let's go.

285
00:45:19,983 --> 00:45:21,322
Take care of yourself.

286
00:45:21,357 --> 00:45:23,149
Have a good trip home.

287
00:46:14,304 --> 00:46:16,204
My two Little Ones.

288
00:46:19,843 --> 00:46:21,802
Come on, let's put them to bed.

289
00:46:24,114 --> 00:46:26,514
You should get ready for bed, too.

290
00:46:33,172 --> 00:46:34,782
Quietly, please.

291
00:46:49,273 --> 00:46:51,677
Oh, what is it this time?

292
00:47:23,807 --> 00:47:25,125
- Hey, mom?
- Yes.

293
00:47:25,145 --> 00:47:27,482
Do you remember your previous lives?

294
00:47:27,517 --> 00:47:28,977
No. No, not really.

295
00:47:29,012 --> 00:47:30,004
Why not?

296
00:47:31,981 --> 00:47:36,780
Only little children can do that.
That's why they tell colorful stories.

297
00:47:39,015 --> 00:47:41,341
Some say they're talking
about previous lives.

298
00:47:41,425 --> 00:47:43,584
Can I talk about those things?

299
00:47:43,694 --> 00:47:45,924
Of course.
You talked a lot.

300
00:47:46,029 --> 00:47:48,159
What kind of stories did I tell?

301
00:47:48,265 --> 00:47:51,937
Keep it down. Shh! Time to sleep. Lie down now.

302
00:47:54,465 --> 00:47:55,484
Good girl.

303
00:47:58,313 --> 00:47:59,201
There.

304
00:48:30,741 --> 00:48:32,538
Grandma...

305
00:48:32,643 --> 00:48:35,134
The yellow dog story, what happened?

306
00:48:35,245 --> 00:48:39,838
The young couple got married,
and then they had a child.

307
00:48:39,883 --> 00:48:44,081
Perhaps the dog was reborn
with a ponytail.

308
00:48:45,222 --> 00:48:46,553
Really?

309
00:48:48,091 --> 00:48:51,288
Will I come back as a person
in my next life?

310
00:48:52,195 --> 00:48:55,095
Come here.
Let me show you something.

311
00:49:19,791 --> 00:49:25,122
Let me know when you balance a grain
of rice on the tip of this needle.

312
00:49:38,141 --> 00:49:40,370
But I don't think that's possible!

313
00:49:42,412 --> 00:49:47,648
See, my child? That's how hard
it is to be born again as a person.

314
00:49:47,883 --> 00:49:52,539
That's why
a human life is so valuable.

315
00:50:16,246 --> 00:50:19,977
There! I see a boy on a big camel.

316
00:50:21,385 --> 00:50:26,045
Another one. And look, there's
a boy riding on a big horse.

317
00:50:26,256 --> 00:50:29,712
- Where is that?
- Hey! Over there!

318
00:50:30,193 --> 00:50:34,385
- Look! Look there!
- I see...I see one...

319
00:50:34,420 --> 00:50:38,004
I see lots of animals all around.

320
00:50:38,039 --> 00:50:40,976
Oh! I found an elephant!

321
00:50:42,372 --> 00:50:43,896
I see him.

322
00:50:44,584 --> 00:50:46,706
What else? What else do you see?

323
00:50:46,741 --> 00:50:48,808
Look! I see a giraffe.

324
00:50:49,046 --> 00:50:50,946
Oh, really! Where?

325
00:50:51,048 --> 00:50:54,017
Look  at that one, it's round.

326
00:50:54,051 --> 00:50:56,062
That one is all squared out.

327
00:50:56,097 --> 00:50:57,242
<i>Which one?</i>

328
00:50:57,288 --> 00:50:59,683
<i>This one here, see?</i>

329
00:51:09,566 --> 00:51:10,658
Don't get too close.

330
00:51:11,946 --> 00:51:14,790
- Hey! - Hey!
- Hey! - Hey!

331
00:51:46,870 --> 00:51:48,316
Get out of the smoke.

332
00:51:49,940 --> 00:51:50,955
Here we go.

333
00:51:52,971 --> 00:51:56,002
It'll be okay! I tell you what,
Little One, I'll sing to you,

334
00:51:56,003 --> 00:51:59,034
and you can dance, all
right? Won't that be fun?

335
00:52:28,778 --> 00:52:30,608
Mom, I gotta tell you this!

336
00:52:30,814 --> 00:52:32,977
How can I help you, Little One?

337
00:52:33,617 --> 00:52:35,981
Mamma, Nansalmaa's lying.

338
00:52:36,016 --> 00:52:37,468
She's lying?

339
00:52:40,924 --> 00:52:42,084
What did she say?

340
00:52:42,659 --> 00:52:46,634
We were looking at clouds and
she said she saw a giraffe.

341
00:52:49,266 --> 00:52:51,914
Just let her be,
and don't laugh at her, please.

342
00:52:56,339 --> 00:52:57,884
I guess I could.

343
00:53:00,844 --> 00:53:04,940
I think she's telling another life,
just like you said she would.

344
00:53:05,649 --> 00:53:06,911
Is it a really good story?

345
00:53:09,786 --> 00:53:12,721
Then maybe it's true!

346
00:53:19,796 --> 00:53:21,058
Can you imagine that?

347
00:53:24,601 --> 00:53:26,717
There you go, Precious.

348
00:53:28,292 --> 00:53:29,418
Okay.

349
00:53:31,875 --> 00:53:33,001
Now we dance.

350
00:54:18,021 --> 00:54:19,045
Spot!

351
00:54:20,657 --> 00:54:21,749
Spot?

352
00:54:23,260 --> 00:54:24,557
Hey, Spot!

353
00:54:33,870 --> 00:54:35,330
Hey, Spot! Come on!

354
00:54:45,849 --> 00:54:48,288
Come here! Hey, come here!

355
00:55:14,678 --> 00:55:17,607
<i>- I'm home.
- I'm glad you're back.</i>

356
00:55:18,348 --> 00:55:19,576
Spot.

357
00:55:19,916 --> 00:55:22,817
<i>Did you get everything done?
Are you tired?</i>

358
00:55:22,919 --> 00:55:24,511
The trip wasn't too bad.

359
00:55:25,588 --> 00:55:28,216
I sold the sheepskins.
That'll help.

360
00:55:34,597 --> 00:55:37,914
- Were you able to see Uncle Dorj?
- Ah...yes.

361
00:55:39,821 --> 00:55:41,149
I'm glad you could.

362
00:55:42,233 --> 00:55:44,610
- Did you two discuss Nansal?
- I did.

363
00:55:46,445 --> 00:55:48,599
She can live there while she's at school.

364
00:55:49,946 --> 00:55:51,943
We need to make sure it's all right with her.

365
00:55:52,916 --> 00:55:55,976
Maybe I could get a job at
one of the stores in town.

366
00:55:56,686 --> 00:56:01,088
Yes, well, I'm really not sure that would
be enough money for us to live on.

367
00:56:05,895 --> 00:56:08,423
- Where are the kids now?
- They're outside playing.

368
00:56:24,781 --> 00:56:29,225
Just a minute. Here ya go, just as you asked.
- Oh, thank you, it's nice.

369
00:56:30,687 --> 00:56:34,431
Are you sure it's heat resistant enough
to cook? I really like the color though.

370
00:56:34,466 --> 00:56:35,729
- Hopefully it'll do the job.
- It's light.

371
00:56:35,811 --> 00:56:37,625
Here, I got this for us, too.

372
00:56:38,862 --> 00:56:42,754
What is this? Oh, a flashlight.
That's what I thought.

373
00:56:42,799 --> 00:56:45,523
I thought we could use it.
I figured it'd come in handy.

374
00:56:45,524 --> 00:56:46,833
Yes, that's fine.

375
00:56:53,076 --> 00:56:55,135
Did you find out about the election results?

376
00:56:55,712 --> 00:56:58,372
I heard they're all gonna have
to be repeated sometime soon.

377
00:56:58,407 --> 00:57:00,485
Really? I wondered what
could have happened.

378
00:57:03,564 --> 00:57:08,681
<i>I never found out. How's everything
here, okay? Yes, you didn't miss anything.</i>

379
00:57:09,059 --> 00:57:10,590
How ya doin', Spot.

380
00:57:11,294 --> 00:57:13,987
Sit still, try and help me, don't make any noise.

381
00:57:15,064 --> 00:57:19,360
Hey, hey! Come here, You!
I missed you!

382
00:57:20,470 --> 00:57:23,199
Did you miss your father, just a little?

383
00:57:24,707 --> 00:57:28,124
Come here. And did you look
after your brother and sister?

384
00:57:28,159 --> 00:57:29,087
- Yeah!
- Yes!

385
00:57:29,122 --> 00:57:31,606
- Um, look!
- Here, brought you something from town.

386
00:57:34,218 --> 00:57:36,950
And one for you. There ya go.

387
00:57:37,588 --> 00:57:40,158
And something for you. Here.

388
00:57:40,623 --> 00:57:43,390
Ooh, I almost forgot. I got you something else.

389
00:57:43,694 --> 00:57:46,788
Now, make sure you share this with each other.

390
00:57:51,000 --> 00:57:52,923
Let me show you how to use it.

391
00:57:53,790 --> 00:57:54,871
Just like this.

392
00:58:01,072 --> 00:58:02,159
Wonderful!

393
00:58:14,360 --> 00:58:16,503
That's great. Now don't forget to share it.

394
00:58:18,404 --> 00:58:19,461
Look, look, look!

395
00:58:28,071 --> 00:58:29,603
It's cute! I want to see that!

396
00:58:29,939 --> 00:58:32,130
- Wait a...
- Remember, don't break it!

397
00:58:33,109 --> 00:58:36,647
Don't reach out for it!
He'll come by himself!

398
00:58:36,680 --> 00:58:39,018
<i>- Treat it gently!
- Oh, he likes that.</i>

399
00:58:41,985 --> 00:58:44,283
<i>That's great!</i>

400
00:58:46,855 --> 00:58:48,111
Get out of here!

401
00:58:48,558 --> 00:58:52,119
Nansal! Listen!
Didn't I tell you to get rid of this dog?

402
00:59:11,614 --> 00:59:14,920
<i>- Did you pack the ammunition?
- Yes, I did. Thank you.</i>

403
00:59:15,355 --> 00:59:17,182
This is the dog
I told you about.

404
00:59:17,520 --> 00:59:18,885
He looks healthy.

405
00:59:20,690 --> 00:59:22,721
He'll definitely make a good hunting dog.

406
00:59:22,725 --> 00:59:24,863
- He's got the right color.
- He does, doesn't he?

407
00:59:26,629 --> 00:59:28,063
Is your dog getting too old?

408
00:59:28,164 --> 00:59:32,534
Yes, he is. He won't be able
to help for much longer.

409
00:59:32,936 --> 00:59:34,042
Where did you get him?

410
00:59:34,240 --> 00:59:35,565
My daughter found him.

411
00:59:35,566 --> 00:59:37,129
Is he a hunting breed?

412
00:59:37,130 --> 00:59:38,817
- Could be.
- Is that so?

413
00:59:38,852 --> 00:59:41,622
When I found him he was sleeping in a cave.

414
00:59:41,778 --> 00:59:42,938
In a cave?

415
00:59:43,946 --> 00:59:46,801
Didn't I already tell you,
we're not keeping the dog.

416
00:59:51,602 --> 00:59:54,284
<i>We're moving tomorrow and
he's not coming with us.</i>

417
00:59:55,245 --> 01:00:00,507
<i>You better keep an eye on your sheep,
living in a cave, that's where wolves are.</i>

418
01:00:45,475 --> 01:00:46,635
Here's your deel.

419
01:00:47,710 --> 01:00:49,903
Wasn't the last wolf attack
bad enough?

420
01:00:53,416 --> 01:00:55,715
What does that have to do
with us keeping the dog?

421
01:00:56,986 --> 01:00:58,328
I'm worried.

422
01:00:59,025 --> 01:01:02,074
If he had been living with wolves,

423
01:01:02,375 --> 01:01:05,424
they could follow his
trail and decimate our herd.

424
01:01:10,700 --> 01:01:12,730
We have no idea where he comes from.

425
01:01:18,007 --> 01:01:19,491
<i>Mom! Come quick!</i>

426
01:01:19,492 --> 01:01:20,976
What is it?

427
01:01:21,077 --> 01:01:24,405
<i>Mom! Mom! Come here!</i>

428
01:01:25,581 --> 01:01:28,398
<i>Stand back! Get some water! Quickly!</i>

429
01:01:28,718 --> 01:01:31,881
<i>Be careful! Careful! Don't get burned!</i>

430
01:01:35,091 --> 01:01:37,854
What bad luck.
Bad luck.

431
01:01:38,995 --> 01:01:40,509
Ah! That's such a shame.

432
01:01:46,703 --> 01:01:47,931
<i>Can I see it?</i>

433
01:01:49,405 --> 01:01:51,572
Oh, your father's present is ruined.

434
01:02:28,811 --> 01:02:32,166
<i>Mom, can I play with the flashlight?</i>

435
01:02:32,381 --> 01:02:35,434
<i>- All right, but be careful with it.
- 'Kay.</i>

436
01:02:40,189 --> 01:02:41,781
<i>Nansal!</i>

437
01:02:45,194 --> 01:02:51,586
Nansal, look at me!
Nansal, look now!

438
01:02:55,605 --> 01:02:56,697
<i>Get up.</i>

439
01:02:59,509 --> 01:03:01,136
<i>Come on, let's play!</i>

440
01:06:25,448 --> 01:06:28,252
Hey, Nansal! We need your help over here.

441
01:06:56,979 --> 01:06:59,181
You're doing such a good job!

442
01:07:29,080 --> 01:07:31,283
Look at our Little One!

443
01:07:31,449 --> 01:07:34,982
Ooh, careful you don't
fall! Come on now.

444
01:07:35,017 --> 01:07:35,748
<i>Get him down.</i>

445
01:07:35,749 --> 01:07:38,143
Here we go! Here! Here!

446
01:07:38,754 --> 01:07:40,416
Let go! Ooops!

447
01:09:04,940 --> 01:09:07,205
- Here, let me do it!
- No, it's all right.

448
01:09:25,928 --> 01:09:28,740
Nansal! Come and clean this up!

449
01:10:54,783 --> 01:10:57,445
What would we do without
our Great Little Shepherd?

450
01:10:57,691 --> 01:10:59,925
Come on, Little One! Come on! Come on!

451
01:11:05,527 --> 01:11:07,294
Nansal, come watch your brother!

452
01:11:21,946 --> 01:11:23,209
Got him?

453
01:11:24,280 --> 01:11:25,451
Now watch!

454
01:12:11,972 --> 01:12:15,701
- Thank you for the summer here.
- Yes, thank you.

455
01:14:59,695 --> 01:15:01,890
Nansal, get back up there on the cart!

456
01:15:01,897 --> 01:15:05,479
I was just picking up
Batbayar's belt, Mom!

457
01:15:11,840 --> 01:15:13,478
Chuluna, stop the cart!

458
01:15:17,645 --> 01:15:19,912
Batbayar gone, he's not in the basket!

459
01:15:19,947 --> 01:15:22,390
Nansal, where's your brother?
Have you seen him?

460
01:15:23,118 --> 01:15:25,726
- I can't find him.
- He was in here!

461
01:15:25,788 --> 01:15:28,510
I don't see him.
Where's my son?

462
01:15:34,129 --> 01:15:36,229
Stay with the kids,
I'll ride back.

463
01:15:37,866 --> 01:15:39,561
<i>Mom, what's wrong?</i>

464
01:19:02,938 --> 01:19:05,026
My son! Hey!

465
01:19:15,383 --> 01:19:16,690
Let's look at you.

466
01:19:53,687 --> 01:19:55,198
Where's Spot?

467
01:19:59,327 --> 01:20:00,981
Here! Here!

468
01:21:14,002 --> 01:21:17,156
- Come here, You!
- He'll be okay, again.

469
01:21:17,606 --> 01:21:18,937
Okay, sit in here.

470
01:21:21,610 --> 01:21:24,812
Oh, look how comfortable it is.
You'll like it in there.

471
01:21:26,782 --> 01:21:29,173
- Keep an eye on him!
- Hold onto him tight, now!

472
01:21:29,918 --> 01:21:32,203
You have to hold him
if he tries to climb out.

473
01:21:34,189 --> 01:21:35,884
Oh, this isn't going to work.

474
01:21:37,072 --> 01:21:38,957
I better tie you down.

475
01:21:39,451 --> 01:21:40,633
Get in there.

476
01:21:45,967 --> 01:21:48,074
Help me, please!
Hold on to him!

477
01:21:50,506 --> 01:21:54,232
Ready? I'll take your horse with me, okay?

478
01:21:55,677 --> 01:21:57,085
Nansal, hold on tight there!

479
01:21:57,086 --> 01:22:01,004
- Hooooooo!
- Hup hup hup  hup  hup,  hup  hup  hup hup!

480
01:23:10,919 --> 01:23:13,930
<i>We remind you to vote in the upcoming elections.</i>

481
01:23:13,931 --> 01:23:17,112
<i>There will be town hall meetings on the 21st.</i>

482
01:23:17,733 --> 01:23:20,717
<i>Please stop by to meet the Democratic candidates.</i>

483
01:23:20,979 --> 01:23:22,624
<i>And remember to vote!</i>

484
01:23:23,277 --> 01:23:25,514
<i>We hope you'll make
the right decision.</i>

485
01:23:26,090 --> 01:23:29,081
<i>We remind you to vote in the upcoming elections.</i>

486
01:23:29,467 --> 01:23:32,358
<i>There will be town hall meetings on the 21st.</i>

487
01:23:32,874 --> 01:23:35,936
<i>Please stop by to meet the Democratic candidates.</i>

488
01:23:36,198 --> 01:23:37,835
<i>And remember to vote!</i>

489
01:23:38,757 --> 01:23:40,701
<i>We hope you'll make
the right decision.</i>

490
01:23:41,417 --> 01:23:44,298
<i>We remind you to vote in the upcoming elections.</i>

491
01:23:44,917 --> 01:23:47,572
<i>There will be town hall meetings on the 21st.</i>

492
01:23:48,045 --> 01:23:51,139
<i>Please stop by to meet the Democratic candidates.</i>

493
01:25:41,636 --> 01:25:49,543
SPECIAL THANKS
TO THE BATCHULUUN FAMILY

494
01:26:45,946 --> 01:26:54,676
NANSAL  AND  ZOCHOR  (WHICH MEANS COLORFUL)

495
01:26:58,947 --> 01:27:01,677
You're always asleep
when I want to play.

496
01:27:04,219 --> 01:27:05,709
Maybe...

497
01:27:08,289 --> 01:27:15,195
you were a real lazybones
in a previous life.

