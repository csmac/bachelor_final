1
00:00:00,000 --> 00:00:07,000
<i>official site FastDrama</i>

2
00:00:10,009 --> 00:00:14,619
(Lovers in Bloom)

3
00:00:15,849 --> 00:00:17,489
(Episode 99)

4
00:00:18,620 --> 00:00:21,660
- Grandma! - Yes? My gosh, you're back,

5
00:00:21,730 --> 00:00:23,230
Grandma, look at this,

6
00:00:23,390 --> 00:00:25,129
Mr, Tall gave it to me as a gift,

7
00:00:25,460 --> 00:00:27,500
- Do you want one too? - What?

8
00:00:28,170 --> 00:00:29,300
What is this?

9
00:00:30,000 --> 00:00:33,100
Gosh, he's so mischievous,

10
00:00:33,469 --> 00:00:35,969
Woo Ri, you should go brush your teeth,

11
00:00:36,039 --> 00:00:37,439
Okay, Mom,

12
00:00:39,380 --> 00:00:41,079
- Goong Hwa, you're back, - Yes,

13
00:00:42,609 --> 00:00:45,350
- You two looked so lovely earlier, - Sorry?

14
00:00:45,350 --> 00:00:48,549
Goodness, you were piggybacking her in front of all the neighbors,

15
00:00:49,950 --> 00:00:50,960
You saw that, Mom?

16
00:00:50,960 --> 00:00:53,619
Of course, Didn't you do that so everyone could see?

17
00:00:53,619 --> 00:00:55,460
Mom, that's enough,

18
00:00:55,689 --> 00:00:58,700
All right, I should go to bed,

19
00:00:59,399 --> 00:01:00,799
Goodnight, Mom,

20
00:01:01,170 --> 00:01:02,329
Goodnight to you too,

21
00:01:02,469 --> 00:01:04,240
- Have a good sleep, - All right,

22
00:01:06,069 --> 00:01:08,469
Bo Ra, you should get some rest too,

23
00:01:08,810 --> 00:01:12,409
And you don't have to greet me when I get home, I'm younger, you know,

24
00:01:12,480 --> 00:01:14,180
You're her sister-in-law,

25
00:01:14,980 --> 00:01:18,280
- She's very concerned about you, - Gosh, whatever,

26
00:01:18,620 --> 00:01:20,920
Don't be, You two should go to bed,

27
00:01:21,950 --> 00:01:23,189
Goodnight, Goong Hwa,

28
00:01:23,319 --> 00:01:24,459
Goodnight, Goong Hwa,

29
00:01:24,489 --> 00:01:26,459
- Goodnight, - Have a good sleep,

30
00:01:54,920 --> 00:01:57,290
What's wrong? Is Bo Ra feeling sick?

31
00:01:57,390 --> 00:02:01,390
She seems to have a fever, so I wanted to freeze some wet towels,

32
00:02:03,430 --> 00:02:05,099
Shouldn't she go see a doctor?

33
00:02:05,430 --> 00:02:06,500
We'll wait and see first,

34
00:02:06,959 --> 00:02:08,299
I think this will make the fever go down,

35
00:02:09,830 --> 00:02:11,639
I guess I woke up all of you,

36
00:02:12,240 --> 00:02:13,569
Go in, and get more sleep, Mom,

37
00:02:14,169 --> 00:02:15,270
All right,

38
00:02:17,939 --> 00:02:20,949
He keeps going back and forth,

39
00:02:21,680 --> 00:02:23,650
It looks like he'll stay up all night,

40
00:02:30,659 --> 00:02:31,889
Bo Ra, this won't do,

41
00:02:32,689 --> 00:02:33,889
Let's go see a doctor,

42
00:02:35,330 --> 00:02:38,759
I said I'm fine, It's just a fever,

43
00:02:39,430 --> 00:02:41,599
We have some fever reducer pills, Do you want some?

44
00:02:42,069 --> 00:02:43,699
I don't like taking medicine,

45
00:02:44,599 --> 00:02:48,139
You should sleep, Su Hyuk, I can't sleep if you stay up,

46
00:02:48,439 --> 00:02:50,169
I shouldn't have piggybacked you like that,

47
00:02:50,639 --> 00:02:52,009
We should've just come back home,

48
00:02:53,039 --> 00:02:54,479
I said I liked it,

49
00:02:54,879 --> 00:02:57,009
I started having this fever before that anyway,

50
00:03:02,490 --> 00:03:04,360
You didn't need to bring this,

51
00:03:05,319 --> 00:03:07,830
I heard drinking warm milk can be helpful when you can't fall asleep,

52
00:03:07,960 --> 00:03:09,930
It's probably you who can't fall asleep, not me,

53
00:03:11,199 --> 00:03:14,500
But still, I went all the way to the substation,

54
00:03:14,699 --> 00:03:17,169
How could he leave like that without eating my lunch box?

55
00:03:17,240 --> 00:03:18,699
Gosh, I knew it,

56
00:03:19,099 --> 00:03:22,770
He looked so cold-hearted when I first saw him,

57
00:03:23,270 --> 00:03:26,610
Why don't you help me get over him?

58
00:03:27,479 --> 00:03:29,110
Do you have a lingering affection for him by any chance?

59
00:03:29,110 --> 00:03:32,250
Why would I? That's nonsense, Why would I be missing him?

60
00:03:33,349 --> 00:03:36,889
- He's handsome, - Gosh, How can you smile like that?

61
00:03:36,889 --> 00:03:38,460
Then should I cry instead?

62
00:03:39,689 --> 00:03:42,530
I'm just saying that we shouldn't talk about him anymore,

63
00:03:42,830 --> 00:03:45,400
You saw that we're over at the substation,

64
00:03:45,400 --> 00:03:48,030
You saw how he acted with your own eyes,

65
00:03:48,099 --> 00:03:51,740
Then you should at least stop going on patrol as partners,

66
00:03:51,740 --> 00:03:53,969
I'm worried because you work with him all day long,

67
00:03:54,710 --> 00:03:57,979
I'm sure that makes it hard for you to get over him,

68
00:03:58,009 --> 00:04:01,250
Seriously, you're worried for nothing,

69
00:04:03,509 --> 00:04:05,780
He'll be transferred to the headquarters soon,

70
00:04:08,189 --> 00:04:09,319
Is that right?

71
00:04:10,560 --> 00:04:11,719
I think that's a good thing,

72
00:04:11,789 --> 00:04:14,330
So don't worry about me anymore,

73
00:04:14,590 --> 00:04:19,199
I'll never be foolish enough to shed tears because of a man,

74
00:04:19,629 --> 00:04:23,769
I swear on my lovely daughter, Woo Ri,

75
00:04:24,100 --> 00:04:25,199
I swear,

76
00:04:26,740 --> 00:04:29,339
It's over, Completely,

77
00:04:38,579 --> 00:04:39,750
You're home,

78
00:04:39,920 --> 00:04:42,449
- Why are you out here? - Nothing much,

79
00:04:42,449 --> 00:04:46,259
Why didn't you come home early, eat dinner with us, and get rest?

80
00:04:46,259 --> 00:04:47,730
You know how my work is,

81
00:04:47,790 --> 00:04:50,129
Ju Young seems to like you so much,

82
00:04:50,430 --> 00:04:51,829
I like him too,

83
00:04:53,000 --> 00:04:58,069
My goodness, You look so weary,

84
00:04:59,740 --> 00:05:03,069
I should tell Mother and get you some herbal medicine,

85
00:05:03,470 --> 00:05:04,610
That's not necessary,

86
00:05:04,879 --> 00:05:06,779
Get some for Dad, I don't need that,

87
00:05:06,879 --> 00:05:10,149
But you do, Do you even eat your meals regularly?

88
00:05:10,779 --> 00:05:12,750
Yes, I do, Please don't worry, Mom,

89
00:05:14,689 --> 00:05:15,689
We're home,

90
00:05:15,689 --> 00:05:16,920
- You're home, - Hi,

91
00:05:20,629 --> 00:05:21,660
Did you hear that?

92
00:05:22,629 --> 00:05:23,689
He called her "Mom",

93
00:05:25,100 --> 00:05:26,430
I didn't mishear that, did I?

94
00:05:30,170 --> 00:05:31,269
"Mom"?

95
00:05:31,370 --> 00:05:32,899
It's a good thing,

96
00:05:33,470 --> 00:05:35,610
Tae Jin grew up without a mother's love,

97
00:05:35,670 --> 00:05:39,639
You should be relieved if he feels the similar affection from Mother,

98
00:05:39,939 --> 00:05:41,750
What if my mom had seen that?

99
00:05:42,279 --> 00:05:44,819
Tae Jin,,, Unbelievable,

100
00:05:45,180 --> 00:05:47,649
Hey, Look how Mother treats Tae Jin,

101
00:05:47,790 --> 00:05:49,290
She wakes up early and makes breakfast for him,

102
00:05:49,290 --> 00:05:51,290
She sits beside him and has a nice chat with him,

103
00:05:51,389 --> 00:05:54,560
She waits for him to come home without going to bed,

104
00:05:55,129 --> 00:05:57,660
If I were Tae Jin, I would've become affectionate for her too,

105
00:05:59,800 --> 00:06:01,699
When I saw him like that,

106
00:06:02,230 --> 00:06:04,470
I felt sorry for him and felt like I was going to cry,

107
00:06:04,470 --> 00:06:08,709
Even so, he calls his own mom "that woman", How could he?

108
00:06:10,009 --> 00:06:11,839
It's because he's deeply scarred,

109
00:06:12,779 --> 00:06:15,050
I'm sure he'll come around some day too,

110
00:06:15,149 --> 00:06:17,879
When will that be? Mom is going to leave soon,

111
00:06:17,879 --> 00:06:19,350
But she'll come back,

112
00:06:19,350 --> 00:06:22,220
How would you know if she'll come back or not?

113
00:06:22,949 --> 00:06:25,259
They tore down our restaurant,

114
00:06:25,259 --> 00:06:29,459
How can we be sure if they will just let her leave?

115
00:06:30,029 --> 00:06:33,329
What if something bad happens to her?

116
00:06:34,800 --> 00:06:38,899
If you're that worried, you should at least call her often,

117
00:06:41,439 --> 00:06:42,740
(Wanted List)

118
00:06:50,250 --> 00:06:51,420
What's wrong with you?

119
00:06:54,689 --> 00:06:55,990
Is it because you worry about me?

120
00:06:56,819 --> 00:06:58,060
Why else would you act like this?

121
00:06:58,920 --> 00:07:00,529
Do you still have feelings for me by any chance?

122
00:07:00,730 --> 00:07:02,589
You said you no longer care about me,

123
00:07:03,129 --> 00:07:05,199
Mr, Cha, you're acting odd,

124
00:07:05,730 --> 00:07:07,430
Officer Moo is a police officer,

125
00:07:07,629 --> 00:07:09,930
We're trying to catch a bad guy,

126
00:07:11,000 --> 00:07:12,899
You said gender doesn't matter when it comes to police work,

127
00:07:14,110 --> 00:07:17,310
Do you like Officer Moo by any chance?

128
00:07:30,420 --> 00:07:32,089
(Studio Office)

129
00:07:33,060 --> 00:07:34,589
Double check the concept,

130
00:07:34,629 --> 00:07:36,029
and come up with some designs and catchphrases,

131
00:07:37,259 --> 00:07:39,329
Let's eat some carbs first,,,

132
00:07:39,500 --> 00:07:41,269
so my brain can work properly,

133
00:07:41,500 --> 00:07:43,069
Is it already time for lunch?

134
00:07:44,139 --> 00:07:46,139
You don't even check your meal time,

135
00:07:47,970 --> 00:07:51,209
Are you getting spoiled by your mother and wife?

136
00:07:51,480 --> 00:07:52,740
Let's go out,

137
00:07:56,110 --> 00:07:59,019
Oh, my, Why are you two coming in together?

138
00:07:59,019 --> 00:08:00,920
We met out front,

139
00:08:00,920 --> 00:08:02,550
Mom, why did you come here?

140
00:08:02,750 --> 00:08:04,920
I know you couldn't sleep well last night,

141
00:08:04,920 --> 00:08:08,160
You didn't even sleep last night, but you left without eating,

142
00:08:08,160 --> 00:08:09,459
Why wouldn't I be worried?

143
00:08:10,230 --> 00:08:12,529
I told Bo Ra to stay home, but she left too,

144
00:08:12,529 --> 00:08:14,329
You heard that she has a full schedule today,

145
00:08:15,569 --> 00:08:16,899
Did you even eat lunch?

146
00:08:16,899 --> 00:08:19,100
We were about to go out to have some hangover soup,

147
00:08:20,139 --> 00:08:21,610
But didn't you bring us lunch?

148
00:08:22,040 --> 00:08:26,079
Well, yes, But it'd be better for you to eat hot soup,

149
00:08:27,040 --> 00:08:29,709
Eat this later, and go have lunch,

150
00:08:30,779 --> 00:08:32,420
We'll stay here until you're back,

151
00:08:32,420 --> 00:08:34,289
She's right, You must be hungry,

152
00:08:34,289 --> 00:08:36,720
Why didn't you eat yet?

153
00:08:37,320 --> 00:08:38,720
Then we'll go and have lunch,

154
00:08:38,789 --> 00:08:39,960
- See you later, - See you,

155
00:08:39,960 --> 00:08:41,059
- Let's go, - See you,

156
00:08:47,059 --> 00:08:49,429
Is everything okay with your family?

157
00:08:50,070 --> 00:08:52,840
What problem would happen to us?

158
00:08:54,340 --> 00:08:56,710
Your family members must be at ease now,,,

159
00:08:56,809 --> 00:08:58,440
since the trouble has gone,

160
00:08:58,710 --> 00:09:01,909
Well, yes, They're all doing fine,

161
00:09:03,210 --> 00:09:04,779
Is Officer Moo doing all right?

162
00:09:05,149 --> 00:09:08,120
I hope she'll get back together with Tae Jin soon,

163
00:09:09,220 --> 00:09:11,289
- I don't know about that, - To tell you the truth,

164
00:09:11,759 --> 00:09:16,029
my husband also wants Tae Jin to get back with her,,,

165
00:09:16,090 --> 00:09:18,000
Please, forget it,

166
00:09:18,629 --> 00:09:19,759
Pardon?

167
00:09:19,860 --> 00:09:23,100
Please, forget it, They're over already,

168
00:09:23,870 --> 00:09:26,340
Goong Hwa has gotten over him,

169
00:09:26,340 --> 00:09:30,639
No way, It's not that easy to forget someone you love,

170
00:09:30,639 --> 00:09:34,850
She's very confident and smart just like me,

171
00:09:34,850 --> 00:09:37,419
She wouldn't cling on to someone she broke up with,

172
00:09:37,419 --> 00:09:39,220
She's not that kind of person,

173
00:09:39,580 --> 00:09:41,889
She's always clear about how she feels,

174
00:09:41,889 --> 00:09:43,250
Is that so?

175
00:09:43,820 --> 00:09:49,029
Tae Jin is so tender-hearted and considerate,,,

176
00:09:49,230 --> 00:09:51,659
that he can't bear to watch other people heartbroken,

177
00:09:52,600 --> 00:09:55,129
He seems very cold-hearted to me,

178
00:09:55,230 --> 00:09:59,240
Tae Jin isn't cold-hearted, I'm not saying this because he's my son,

179
00:10:01,570 --> 00:10:06,840
I mean, he's my son now, isn't he?

180
00:10:07,350 --> 00:10:10,080
I'm not saying this because he's my son,

181
00:10:10,149 --> 00:10:12,419
but he's such a nice person,

182
00:10:12,419 --> 00:10:14,820
It's rare to find a man like him these days,

183
00:10:15,149 --> 00:10:17,090
I hope Officer Moo knows that,

184
00:10:17,289 --> 00:10:20,259
Whatever, Hope for their reunion as much as you want,

185
00:10:20,259 --> 00:10:21,860
We've decided to give up on him,

186
00:10:22,090 --> 00:10:25,230
I don't even want to know about him anymore,

187
00:10:33,799 --> 00:10:35,009
Is that true?

188
00:10:35,110 --> 00:10:36,240
Yes,

189
00:10:36,340 --> 00:10:40,340
Ju Young said that she's a very talented cook,

190
00:10:40,679 --> 00:10:43,049
I know that you don't like the idea,

191
00:10:43,049 --> 00:10:47,250
but kids don't give up until they give it a try,

192
00:10:47,350 --> 00:10:49,820
So let Hae Chan learn to cook for now,

193
00:10:50,450 --> 00:10:53,620
- But it must cost a fortune,,, - It's free,

194
00:10:54,590 --> 00:10:56,289
Ju Young already told her about Hae Chan,

195
00:10:56,690 --> 00:10:58,429
Still, I can't let him take the lessons for free,

196
00:10:58,429 --> 00:11:01,500
Gosh, Ju Young introduced him to her,

197
00:11:01,700 --> 00:11:04,240
He said she's like his sister-in-law,

198
00:11:04,240 --> 00:11:05,370
Sister-in-law?

199
00:11:05,440 --> 00:11:10,370
In fact, she's Officer Moo's sister-in-law,

200
00:11:12,139 --> 00:11:15,110
Come to think of it, Officer Moo is so foolish,

201
00:11:15,110 --> 00:11:17,480
There's no man like Tae Jin,

202
00:11:17,679 --> 00:11:20,350
She can't find anyone like him in the entire country,

203
00:11:20,350 --> 00:11:22,350
She should've been more patient and waited longer,

204
00:11:25,019 --> 00:11:27,059
Well, I think I should go now,

205
00:11:28,789 --> 00:11:31,500
Oh, my, What brings you here?

206
00:11:31,730 --> 00:11:33,870
I came here to talk to Hee Jin,

207
00:11:33,870 --> 00:11:37,799
Is that so? Then have a nice talk, I'll go home,

208
00:11:37,970 --> 00:11:39,039
Ms, Oh,

209
00:11:39,899 --> 00:11:41,909
Yes? What is it?

210
00:11:41,940 --> 00:11:44,539
I'm sorry about what happened before,

211
00:11:45,139 --> 00:11:48,210
Don't say that, I should be the one who should apologize,

212
00:11:48,980 --> 00:11:49,980
See you later,

213
00:11:57,490 --> 00:12:01,929
Hee Jin, how did it go with your landlord?

214
00:12:02,190 --> 00:12:04,929
Oh, that? Our landlord hasn't said anything yet,

215
00:12:06,059 --> 00:12:08,129
I guess your landlord is a nice person,

216
00:12:09,129 --> 00:12:13,240
I thought something bad might happen to your restaurant,

217
00:12:13,470 --> 00:12:17,940
Dad, I didn't mean to tell you this,

218
00:12:19,179 --> 00:12:21,580
Tae Jin didn't tell you anything, did he?

219
00:12:22,580 --> 00:12:23,679
About what?

220
00:12:23,679 --> 00:12:27,990
Some thugs came and tore down the restaurant,

221
00:12:28,990 --> 00:12:30,049
What did you say?

222
00:12:30,049 --> 00:12:35,029
It turns out that it was the son of the man my mom is living with,,,

223
00:12:35,230 --> 00:12:37,159
who sent the thugs to do that,

224
00:12:39,299 --> 00:12:42,799
Mom visited me not long ago,

225
00:12:42,799 --> 00:12:45,070
She said she'll leave that family and go abroad,

226
00:12:45,740 --> 00:12:49,039
They tore down this place because she met us,

227
00:12:49,169 --> 00:12:51,340
I'm not sure if they will let her go without causing trouble,

228
00:12:52,639 --> 00:12:57,480
Dad, what if something bad happens to Mom?

229
00:13:08,659 --> 00:13:11,629
Please let us know if there's any side dishes you crave for,

230
00:13:12,000 --> 00:13:15,230
Seonny will prepare everything for you,

231
00:13:16,929 --> 00:13:18,570
My gosh, Seonny,

232
00:13:19,970 --> 00:13:21,309
Okay, good,

233
00:13:21,840 --> 00:13:23,639
What should I post though?

234
00:13:24,139 --> 00:13:27,350
Is there an easy-to-make dish that looks romantic?

235
00:13:27,909 --> 00:13:29,049
Meatballs?

236
00:13:29,450 --> 00:13:30,980
Is it too common?

237
00:13:31,350 --> 00:13:33,649
Chili pepper fritters? No, no,

238
00:13:33,720 --> 00:13:35,919
That's too labor-intensive,

239
00:13:36,289 --> 00:13:39,720
That's right, I can't make Seonny work too hard,

240
00:13:41,960 --> 00:13:45,759
Is there anything I can do to express my feelings for her?

241
00:13:46,629 --> 00:13:49,269
The cutie-pie of the team is here, everyone,

242
00:13:53,500 --> 00:13:55,440
That guy, Mr, Park,

243
00:13:55,610 --> 00:13:59,309
always barges in whenever I'm trying to take my chances with Seonny,

244
00:14:01,210 --> 00:14:04,179
Hey, did you guys have another feast for lunch?

245
00:14:04,220 --> 00:14:07,720
I can easily take care of your leftovers if you have any,

246
00:14:08,019 --> 00:14:10,350
People don't change, right?

247
00:14:10,350 --> 00:14:14,059
Gosh, people die when they change suddenly,

248
00:14:20,600 --> 00:14:21,600
Hello?

249
00:14:23,100 --> 00:14:24,100
Yes,

250
00:14:24,840 --> 00:14:26,940
Yes, this is Officer Moo Goong Hwa,

251
00:14:29,840 --> 00:14:33,779
Pardon me? You have information about a case of murder?

252
00:14:39,580 --> 00:14:41,590
(Restricted Caller ID, Record)

253
00:14:44,490 --> 00:14:47,330
Then would you be able to meet us here?

254
00:14:47,629 --> 00:14:50,029
No, let's meet somewhere else,

255
00:14:53,759 --> 00:14:54,870
Somewhere else?

256
00:14:58,039 --> 00:14:59,940
Sure, Yes, I know that place,

257
00:15:00,440 --> 00:15:02,470
Yes, I'll see you there tomorrow, then,

258
00:15:11,679 --> 00:15:15,419
A murder case,,, It must be the golf lounge case, right?

259
00:15:15,620 --> 00:15:16,919
Isn't it obvious?

260
00:15:20,220 --> 00:15:22,629
No, let's meet somewhere else,

261
00:15:24,429 --> 00:15:25,460
Somewhere else?

262
00:15:25,460 --> 00:15:27,429
If you walk along the main street from the substation,

263
00:15:27,500 --> 00:15:30,269
you'll see an intersection, Do you know the cafe there?

264
00:15:31,970 --> 00:15:33,440
Did he tell you who he is?

265
00:15:34,309 --> 00:15:35,370
No,

266
00:15:35,840 --> 00:15:37,340
You'll probably find out when you meet him,

267
00:15:37,710 --> 00:15:39,509
He told me to come alone,

268
00:15:39,879 --> 00:15:42,009
Of course you should meet him alone,

269
00:15:42,009 --> 00:15:44,049
We'll hide and watch you,

270
00:15:44,220 --> 00:15:46,720
We've done things like this so many times, you know,

271
00:15:47,889 --> 00:15:50,019
By the way, it's very strange,

272
00:15:50,149 --> 00:15:52,360
How did he call your number?

273
00:15:53,220 --> 00:15:54,929
Could he be someone who knows you well?

274
00:15:58,159 --> 00:16:00,360
Officer Moo, someone is here to see you,

275
00:16:06,240 --> 00:16:07,340
Hello,

276
00:16:09,009 --> 00:16:11,610
- You must be the chief, - Yes,

277
00:16:11,610 --> 00:16:14,379
Please take good care of Officer Moo,

278
00:16:14,379 --> 00:16:16,750
By the way, who are you?

279
00:16:16,750 --> 00:16:18,080
I'm a friend of hers,

280
00:16:18,080 --> 00:16:20,019
- I see, you're her friend, - Yes,

281
00:16:26,460 --> 00:16:27,830
What brings you here?

282
00:16:28,159 --> 00:16:29,529
You're almost done for the day, right?

283
00:16:29,789 --> 00:16:31,429
I'll wait outside, Come out when you're done,

284
00:16:31,529 --> 00:16:34,669
Then have a good day, everyone, Goodbye,

285
00:16:41,039 --> 00:16:42,870
He looks familiar,

286
00:16:43,370 --> 00:16:45,509
Get ready to leave for the day, Go on,

287
00:16:48,980 --> 00:16:50,409
Wait, Chief,

288
00:16:51,379 --> 00:16:54,419
It's getting chilly, How about we go for some hot mudfish soup?

289
00:16:54,419 --> 00:16:57,759
- Gosh, I don't want to, - I know a great place,

290
00:17:06,730 --> 00:17:09,970
- Cha Tae Jin speaking, - It's me, Let's meet,

291
00:17:10,170 --> 00:17:13,170
I must see you today even if you're busy,

292
00:17:24,019 --> 00:17:26,549
Honestly, I can't understand,

293
00:17:26,619 --> 00:17:29,420
I'm tall, and I'm quite handsome too,

294
00:17:29,650 --> 00:17:31,759
I'm also very rich,

295
00:17:32,119 --> 00:17:33,490
And there's more,

296
00:17:33,559 --> 00:17:35,359
I like you a lot,

297
00:17:35,359 --> 00:17:38,400
and I love Woo Ri as if she's my own child,

298
00:17:39,059 --> 00:17:43,029
Wouldn't you say I'm perfect husband material?

299
00:17:43,029 --> 00:17:46,000
You won't be able to find any men who fit all the criteria,

300
00:17:46,299 --> 00:17:48,470
So why are you being so foolish?

301
00:17:48,470 --> 00:17:50,579
When you see a good guy, you must snag him,

302
00:17:52,539 --> 00:17:53,680
You're right,

303
00:17:55,450 --> 00:17:57,680
It's strange that you're readily agreeing with everything I said,

304
00:17:58,450 --> 00:18:00,150
You always surprise me after being like this,

305
00:18:08,529 --> 00:18:09,660
Mr, Jin,

306
00:18:10,390 --> 00:18:13,200
Be honest with me, You liked that I picked you up,,,

307
00:18:13,200 --> 00:18:15,329
in front of Cha Tae Jin earlier, didn't you?

308
00:18:16,000 --> 00:18:18,039
Women seem to like things like that,

309
00:18:18,799 --> 00:18:23,039
Showing off a new boyfriend in front of an ex-boyfriend,

310
00:18:23,910 --> 00:18:29,210
I really liked Tae Jin a lot,

311
00:18:31,279 --> 00:18:33,519
My feelings for him were sincere, and I'm a faithful person,

312
00:18:34,589 --> 00:18:36,650
I can't jump into something new right away like this,

313
00:18:37,690 --> 00:18:39,319
And I don't even want to,

314
00:18:43,190 --> 00:18:44,259
I'm sorry,,,

315
00:18:44,630 --> 00:18:47,130
if I confused you in any way,

316
00:18:48,500 --> 00:18:52,799
Woo Ri grew up without a dad, and she likes you so much,

317
00:18:53,640 --> 00:18:55,809
I guess I didn't want to take that joy from her,

318
00:18:56,410 --> 00:18:58,680
- Officer Moo, - I told you,

319
00:18:59,740 --> 00:19:02,410
Woo Ri said that she wants you to be her dad,

320
00:19:04,750 --> 00:19:09,619
But I have no interest in becoming your wife,

321
00:19:13,859 --> 00:19:16,859
It means that all of this will only end up hurting Woo Ri more,

322
00:19:18,700 --> 00:19:21,500
I don't want her to end up being hurt like that because of me,

323
00:19:22,299 --> 00:19:26,099
So even if she calls you in tears,

324
00:19:26,700 --> 00:19:28,809
I don't want you to coddle her anymore,

325
00:19:30,779 --> 00:19:34,450
Like you said, we're family now,

326
00:19:36,680 --> 00:19:37,819
Please do it for me,

327
00:20:00,710 --> 00:20:02,470
Go ahead, You said you wanted to tell me something,

328
00:20:02,940 --> 00:20:05,380
Jin Dae Gab, that man,

329
00:20:05,940 --> 00:20:08,309
is much more dangerous than you think,

330
00:20:09,150 --> 00:20:11,980
You might get in huge trouble if you underestimate him,

331
00:20:13,650 --> 00:20:16,319
Why did you have to tell me this in person?

332
00:20:17,150 --> 00:20:20,220
Why? Are you afraid that I might make your life difficult?

333
00:20:22,190 --> 00:20:25,599
Right, You may end up in a difficult position because of me,

334
00:20:26,099 --> 00:20:29,329
So? Did you want to warn me to behave myself?

335
00:20:29,529 --> 00:20:32,369
No, you don't need to worry about me,

336
00:20:33,170 --> 00:20:34,410
Do whatever you want,

337
00:20:36,910 --> 00:20:40,079
But do not let your guard down,

338
00:20:40,680 --> 00:20:43,279
Chairman Jin is capable of doing anything,,,

339
00:20:43,920 --> 00:20:45,819
if it's for achieving his goal,

340
00:20:47,950 --> 00:20:50,049
Haven't you lived your life just like that?

341
00:20:50,789 --> 00:20:52,559
It seems to be the case according to my investigation,

342
00:20:52,920 --> 00:20:56,089
When it came to money, you didn't care whether people died or not,

343
00:20:56,859 --> 00:20:58,299
You were known as a godmother among loan sharks,

344
00:21:00,329 --> 00:21:01,400
Tae Jin,

345
00:21:01,470 --> 00:21:04,200
Don't come to see me again with these ridiculous excuses,

346
00:21:04,869 --> 00:21:05,869
Actually,

347
00:21:06,740 --> 00:21:09,470
I want you to stay far away from my family,

348
00:21:10,339 --> 00:21:12,539
That's the best thing you can do for us,

349
00:21:34,569 --> 00:21:36,700
Mom, why are you sitting out here?

350
00:21:39,339 --> 00:21:41,440
Tae Jin isn't home yet, you know,

351
00:21:42,210 --> 00:21:45,079
Mom, please, He's not a small child,

352
00:21:45,779 --> 00:21:48,250
I'll wait for him, You should go in,

353
00:21:48,450 --> 00:21:50,849
What if you catch a cold?

354
00:21:56,220 --> 00:21:59,359
Tae Jin broke up with that girl, right?

355
00:21:59,960 --> 00:22:02,759
Yes, that's what he told me,

356
00:22:03,160 --> 00:22:06,400
I ended up repeating what I did to you,

357
00:22:07,430 --> 00:22:10,599
I made him break up with the woman he likes,

358
00:22:22,809 --> 00:22:24,079
You're still awake?

359
00:22:24,849 --> 00:22:27,150
Why didn't you tell me?

360
00:22:27,650 --> 00:22:28,849
About what?

361
00:22:29,049 --> 00:22:30,420
Hee Jin told me,,,

362
00:22:30,920 --> 00:22:33,859
that Chairman Jin's son sent some thugs to vandalize her restaurant,

363
00:22:33,859 --> 00:22:35,059
Dad,

364
00:22:35,230 --> 00:22:38,930
We never know what other things he may do to us,

365
00:22:39,099 --> 00:22:42,170
And now, your mother has even told him that she wants to end it,

366
00:22:43,130 --> 00:22:45,569
She came to see Hee Jin and told her,,,

367
00:22:46,039 --> 00:22:48,009
that she'll be abroad for a while,

368
00:22:48,769 --> 00:22:51,240
so it looks like she's made up her mind to leave him,

369
00:22:52,640 --> 00:22:54,880
I'm worried, I'm concerned about you,

370
00:22:56,079 --> 00:22:58,079
but I also hope that nothing bad happens to your mother,

371
00:23:14,170 --> 00:23:16,000
I think it'd be better if we don't,,,

372
00:23:16,000 --> 00:23:18,670
I'm on my way to meet Officer Moo right now,

373
00:23:21,069 --> 00:23:22,140
What did you say?

374
00:23:23,210 --> 00:23:25,880
You keep avoiding me, What choice do I have?

375
00:23:26,240 --> 00:23:27,809
I have to find a way to save my own life,

376
00:23:29,150 --> 00:23:31,019
I found out that she is,,,

377
00:23:32,150 --> 00:23:34,549
the dead police officer's wife,

378
00:23:34,750 --> 00:23:37,220
Stop it right now unless you want to die!

379
00:23:37,920 --> 00:23:39,859
Wait until I contact you again,

380
00:24:02,210 --> 00:24:04,720
Mole, come out,

381
00:24:42,250 --> 00:24:43,390
Sweetie,

382
00:24:43,920 --> 00:24:45,059
Hey, you're here,

383
00:24:46,559 --> 00:24:47,630
Have you been waiting for long?

384
00:24:47,630 --> 00:24:49,329
- It's okay, I just got here, - Did you?

385
00:24:51,460 --> 00:24:52,559
What would you like to drink?

386
00:25:09,049 --> 00:25:11,420
We just wasted two hours waiting for him,

387
00:25:12,549 --> 00:25:15,220
Do you mind if I go home first?

388
00:25:15,549 --> 00:25:17,359
Today is my third child's birthday,

389
00:25:18,289 --> 00:25:20,059
Sure, Please go ahead,

390
00:25:20,420 --> 00:25:21,430
Okay,

391
00:25:21,630 --> 00:25:22,990
Officer Moo, good job,

392
00:25:23,359 --> 00:25:26,099
- Please send my congratulations, - Okay,

393
00:25:26,500 --> 00:25:27,630
See you later,

394
00:25:33,869 --> 00:25:35,839
It didn't seem like a prank call,

395
00:25:35,839 --> 00:25:39,180
If he's really an informant, he'll call you again,

396
00:25:39,740 --> 00:25:42,750
If not, he might be poking around,

397
00:25:44,150 --> 00:25:46,779
He sounded like he used a voice changer,

398
00:25:48,250 --> 00:25:50,049
I don't think he's just an informant,

399
00:25:50,559 --> 00:25:51,660
Then what?

400
00:25:52,819 --> 00:25:55,259
If he calls you again, call me immediately,

401
00:25:55,829 --> 00:25:57,700
Don't do anything alone, It's dangerous,

402
00:25:58,500 --> 00:25:59,829
You understand me, right?

403
00:26:00,160 --> 00:26:02,970
I got it, You be careful too,

404
00:26:03,529 --> 00:26:04,569
Don't worry,

405
00:26:13,910 --> 00:26:16,009
(Open the door,)

406
00:26:22,750 --> 00:26:23,859
Dad,

407
00:26:28,690 --> 00:26:31,500
You knew about Do Hyun, didn't you?

408
00:26:32,329 --> 00:26:34,130
- Yes, I did, - So,,,

409
00:26:34,829 --> 00:26:37,740
how does Officer Moo feel about him?

410
00:26:37,839 --> 00:26:39,440
Jae Hee told me,,,

411
00:26:40,140 --> 00:26:42,509
that she's not interested in Do Hyun,

412
00:26:45,440 --> 00:26:47,279
Why can't you answer me?

413
00:26:49,150 --> 00:26:51,980
- Is it that she likes him too,,, - No,

414
00:26:52,650 --> 00:26:56,119
Goong Hwa likes someone else,

415
00:26:56,349 --> 00:26:57,390
What?

416
00:26:58,220 --> 00:27:00,789
Are you saying that Do Hyun,,,

417
00:27:01,259 --> 00:27:04,599
is crazy about a girl who likes someone else?

418
00:27:05,160 --> 00:27:06,960
That stupid brat,

419
00:27:08,369 --> 00:27:10,299
All right, Who is he?

420
00:27:10,569 --> 00:27:12,539
He's a police officer she works with,

421
00:27:13,039 --> 00:27:15,140
A police officer? What's his name?

422
00:27:15,210 --> 00:27:19,809
He's Inspector Cha Tae Jin who works at her substation,

423
00:27:19,809 --> 00:27:20,980
Cha Tae Jin?

424
00:27:22,509 --> 00:27:24,480
Did you just say he's Cha Tae Jin?

425
00:27:24,980 --> 00:27:26,079
Dad,

426
00:27:27,390 --> 00:27:29,589
He's Cha Tae Jin?

427
00:27:30,519 --> 00:27:32,220
Cha Tae Jin,

428
00:27:34,460 --> 00:27:35,630
Dad,

429
00:28:03,089 --> 00:28:08,130
You're going to give all this to your son?

430
00:28:10,359 --> 00:28:11,430
No way,

431
00:28:13,500 --> 00:28:16,599
This is just the beginning, Jin Dae Gab,

432
00:28:20,740 --> 00:28:22,809
What are you doing here?

433
00:28:24,109 --> 00:28:25,539
Do you want to have a drink?

434
00:28:27,609 --> 00:28:29,880
I heard that it's Cha Tae Jin,

435
00:28:30,849 --> 00:28:33,579
The man whom that girl, Moo Goong Hwa, likes,

436
00:28:34,190 --> 00:28:38,019
Is that why you tried to hook Do Hyun up with her?

437
00:28:38,589 --> 00:28:43,859
Were you worried that your son might marry a single mom?

438
00:28:44,829 --> 00:28:46,829
- You're right, - What?

439
00:28:47,000 --> 00:28:49,230
Why? Is something wrong about that?

440
00:28:51,140 --> 00:28:54,269
It's been a while since we were on the same page like this,

441
00:28:54,539 --> 00:28:55,769
I didn't like it either,

442
00:28:56,210 --> 00:28:58,410
I didn't want Tae Jin marry a girl like her,

443
00:29:00,109 --> 00:29:04,420
I guess you're determined to take this to the end,

444
00:29:05,079 --> 00:29:08,390
What's there left for us to do? We're already over,

445
00:29:09,420 --> 00:29:12,019
We don't even need to get a divorce,

446
00:29:12,920 --> 00:29:15,029
It'll be over when I pack my stuff and leave this house,

447
00:29:17,029 --> 00:29:21,130
So are you planning to pack your bag and leave?

448
00:29:21,269 --> 00:29:22,599
Who says you can do that?

449
00:29:23,130 --> 00:29:27,099
Do you think I'll just watch you leave?

450
00:29:27,670 --> 00:29:32,009
Until I kick you out of this house,

451
00:29:32,509 --> 00:29:34,380
you'll never be able to leave,

452
00:29:35,779 --> 00:29:37,279
Like father, like son,

453
00:29:37,680 --> 00:29:39,420
Do Hyun said the same thing to me,

454
00:29:39,920 --> 00:29:43,619
He said as long as you want me here, I can't leave you,

455
00:29:45,619 --> 00:29:47,119
What are you going to do with me?

456
00:29:48,460 --> 00:29:49,660
Are you going to kill me?

457
00:29:50,759 --> 00:29:53,900
How are you going to stop me when I walk out on my own feet?

458
00:29:54,700 --> 00:29:57,339
You will never be able to stop me,

459
00:30:16,220 --> 00:30:20,119
Do Hyun, I really don't think this is right,

460
00:30:23,130 --> 00:30:25,000
I had a crush on Bo Ra for over 10 years,

461
00:30:25,599 --> 00:30:28,269
I endured the humiliation your family gave me because of that,

462
00:30:29,970 --> 00:30:31,240
But look at me now,

463
00:30:32,170 --> 00:30:37,009
Bo Ra doesn't care about my broken heart because she doesn't like me,

464
00:30:42,079 --> 00:30:45,150
I'm sure that it's the same for Officer Moo,

465
00:30:45,319 --> 00:30:49,750
No, The more aggressive you get, the more uncomfortable she must be,

466
00:30:49,819 --> 00:30:51,190
Shut your mouth,

467
00:30:53,359 --> 00:30:54,730
You know well,,,

468
00:30:55,359 --> 00:30:57,089
that she loves Cha Tae Jin,

469
00:31:00,200 --> 00:31:01,970
I said, shut your mouth!

470
00:31:03,970 --> 00:31:07,869
That woman will never look back on you,

471
00:31:07,869 --> 00:31:09,339
So please come to your senses,

472
00:31:09,339 --> 00:31:10,539
You're wrong,

473
00:31:11,509 --> 00:31:14,750
I know that she will love me one day,,,

474
00:31:15,809 --> 00:31:18,420
because I'll make it happen at all costs,

475
00:31:37,000 --> 00:31:38,170
Gosh,

476
00:31:38,670 --> 00:31:41,569
Thank you for your hard work,

477
00:31:42,740 --> 00:31:44,380
- Hello, - Hello,

478
00:31:44,509 --> 00:31:47,079
Officer Moo stepped out to buy something,

479
00:31:47,279 --> 00:31:49,779
- Please wait upstairs, - Okay,

480
00:31:50,819 --> 00:31:51,920
Thank you, I'll do that,

481
00:31:52,349 --> 00:31:54,089
- Please come this way, - This way please,

482
00:31:54,150 --> 00:31:56,190
- Hello, - Hello,

483
00:32:00,359 --> 00:32:03,430
- He's here again, - I know, And he's even drunk,

484
00:32:03,430 --> 00:32:05,859
- Where is Mr, Cha? - I think he's in the rest area,

485
00:32:07,900 --> 00:32:09,000
Gosh,

486
00:32:22,410 --> 00:32:23,509
Oh, boy,

487
00:32:24,420 --> 00:32:25,880
Gosh,

488
00:32:30,019 --> 00:32:33,789
We see each other often lately, Inspector Cha Tae Jin,

489
00:32:37,430 --> 00:32:39,960
Officer Moo said you guys had broken up,

490
00:32:47,670 --> 00:32:51,240
I want you to stop confusing her,,,

491
00:32:51,880 --> 00:32:54,950
by using the fact that you're her partner as an excuse,

492
00:32:55,450 --> 00:32:59,480
Please act like the gentleman you always are,

493
00:32:59,680 --> 00:33:00,819
You haven't changed,

494
00:33:02,190 --> 00:33:04,759
- You're still a coward, - What did you say?

495
00:33:04,759 --> 00:33:07,660
Do you think a coward like you can be a good match for Goong Hwa?

496
00:33:08,430 --> 00:33:10,029
I don't think so,

497
00:33:18,500 --> 00:33:22,140
Goong Hwa is more righteous than you think,

498
00:33:22,140 --> 00:33:23,539
and she's a police officer,

499
00:33:24,480 --> 00:33:25,839
So you better come to your senses,

500
00:33:52,839 --> 00:33:54,240
(Lovers in Bloom)

501
00:33:54,309 --> 00:33:56,509
Who's the friend of Officer Moo that came to see her?

502
00:33:56,509 --> 00:33:58,009
Officer Moo, were you two-timing Mr, Cha?

503
00:33:58,009 --> 00:33:59,940
Don't you think that is out of line?

504
00:33:59,940 --> 00:34:01,910
I think something happened to Mom,

505
00:34:01,910 --> 00:34:04,420
Her face turned pale, and she ran out frantically,

506
00:34:04,420 --> 00:34:06,079
Let's do as you said,

507
00:34:06,079 --> 00:34:07,990
Get out of my house,

508
00:34:07,990 --> 00:34:10,760
I'll make sure to catch that jerk, Mole,

509
00:34:10,760 --> 00:34:12,760
So please let me investigate the case with you,

510
00:34:12,789 --> 00:34:15,230
There was a murder in our jurisdiction a little while ago,

511
00:34:15,230 --> 00:34:18,699
and your father is helping the suspect, Mole, hide,

512
00:34:18,699 --> 00:34:20,030
It's none of your business,

513
00:34:20,030 --> 00:34:21,900
Where is that jerk right now?

