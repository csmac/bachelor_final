1
00:03:42,445 --> 00:03:43,926
No!

2
00:03:47,393 --> 00:03:49,509
You know, I'm playing and
I don't even know who...

3
00:03:49,510 --> 00:03:51,230
It was nothing.

4
00:03:57,831 --> 00:03:59,559
What happened?

5
00:04:17,160 --> 00:04:19,338
Just need the last touch, I'm there.

6
00:04:19,339 --> 00:04:20,534
Yeah, sure.

7
00:04:23,511 --> 00:04:25,717
I left it there for you.
So, what happened?

8
00:04:26,318 --> 00:04:28,099
The last touch!

9
00:04:34,271 --> 00:04:36,130
I pressed the wrong button.

10
00:05:56,896 --> 00:06:00,329
- How are you? Good?
- Have some mat�.

11
00:06:01,628 --> 00:06:02,892
Everything OK?

12
00:06:03,992 --> 00:06:05,737
Yeah, just check out...

13
00:06:07,449 --> 00:06:09,435
the rudder system oil.

14
00:06:10,777 --> 00:06:12,420
- Is it leaking a lot?
- Just a bit.

15
00:06:12,421 --> 00:06:14,617
See if we have
to fill it later on.

16
00:06:15,531 --> 00:06:16,668
It's a bit washed out.

17
00:07:04,646 --> 00:07:05,936
Hey!

18
00:07:07,622 --> 00:07:11,216
Are you crazy?
Don't sleep here!

19
00:08:14,545 --> 00:08:17,096
- How about some coffee?
- OK.

20
00:09:29,178 --> 00:09:32,379
- Hi, Nacho.
- Hi, captain, how's it going?

21
00:09:32,380 --> 00:09:34,805
- Fine, so far so good.
- Any news?

22
00:09:38,487 --> 00:09:43,839
- Seven hours navigation to get to
Ushuaia... - And the weather?

23
00:09:44,440 --> 00:09:49,895
Bad. Snow, heavy winds...
horrible, as usual.

24
00:09:51,035 --> 00:09:53,563
- Ok, fine, all's clear, then.
- Captain?

25
00:09:53,964 --> 00:09:56,257
- Do you have a minute?
- Yes, excuse me.

26
00:09:56,958 --> 00:10:00,881
- Yes, tell me.
- Look, I need to go ashore in Ushuaia.

27
00:10:00,882 --> 00:10:04,851
Was born there but I've been away a long
time. Want to see if my mother's alive.

28
00:10:05,265 --> 00:10:08,937
She used to live in a saw mill,
I don't know if she's still there.

29
00:10:10,438 --> 00:10:13,476
- All right. How much time do you need?
- A couple of days.

30
00:10:14,791 --> 00:10:17,088
It'll depend on how much time
we'll be at the port, but...

31
00:10:17,767 --> 00:10:21,014
fix it up with him, see
the paymaster... and...

32
00:10:21,015 --> 00:10:24,376
he'll give you all the details,
whatever you need.

33
00:10:24,677 --> 00:10:26,737
- OK, good luck.
- Thanks.

34
00:10:27,538 --> 00:10:29,780
- Right, take care of...
- Sure, no problem.

35
00:10:29,781 --> 00:10:32,544
Bye and thanks. Any problems,
just call me.

36
00:10:38,870 --> 00:10:42,357
Farrel, remember that the ship sails
on Thursday and waits for no one.

37
00:18:24,401 --> 00:18:25,801
See you!

38
00:29:05,067 --> 00:29:07,286
- Good morning, how's it going?
- Good.

39
00:29:07,287 --> 00:29:11,980
- Are you heading for Tolhuin?
- No, I'm waiting to load up.

40
00:29:11,981 --> 00:29:13,522
Are you heading that way?
- Yeah.

41
00:29:14,034 --> 00:29:18,644
Over the bridge, by the Pastoriza house,
they might be going that way.

42
00:29:18,645 --> 00:29:22,161
Do you know if the road
to the saw mill is open?

43
00:29:22,162 --> 00:29:25,913
I think it's open. It was closed
yesterday, but I think it's open now.

44
00:29:25,914 --> 00:29:28,455
- Right, thank you.
- Good luck to you.

45
00:30:36,878 --> 00:30:39,309
- Good morning!
- Hi, good morning.

46
00:30:46,900 --> 00:30:49,783
Don't know which one to play.
This one!

47
00:30:52,725 --> 00:30:55,823
- Have some coffee if you want.
- OK, thanks.

48
00:30:58,412 --> 00:31:00,892
- What did you play?
- The eight.

49
00:31:01,493 --> 00:31:03,205
Just my luck.

50
00:31:08,506 --> 00:31:10,589
What an idiot I am.

51
00:31:41,183 --> 00:31:43,550
Play a good card for a change.

52
00:31:52,220 --> 00:31:53,939
Just one good card.

53
00:32:00,442 --> 00:32:03,224
My hand. Look!

54
00:32:09,017 --> 00:32:10,656
You lose there.

55
00:32:11,057 --> 00:32:13,296
Don't search there!

56
00:32:16,594 --> 00:32:18,442
- Who deals?
- Me.

57
00:32:29,617 --> 00:32:32,266
- Bye, see you tomorrow.
- See you tomorrow.

58
00:32:34,502 --> 00:32:36,313
- Bye!
- Bye!

59
00:32:44,147 --> 00:32:45,169
Don't you cheat!

60
00:33:35,048 --> 00:33:37,853
Ready? You go up front, Mariela.

61
00:33:37,854 --> 00:33:39,805
- You go in the back.
- Got it.

62
00:33:40,525 --> 00:33:42,984
Let me know when you're getting off.

63
00:34:45,685 --> 00:34:46,815
Bye!

64
00:40:59,327 --> 00:41:02,499
Hi. Any place to eat around here?

65
00:41:05,872 --> 00:41:08,717
There's a cafeteria over there.

66
00:41:08,718 --> 00:41:10,563
Fine. Thanks.

67
00:41:51,927 --> 00:41:54,414
- Hi. - Hello.
- Anything to eat?

68
00:41:54,415 --> 00:41:57,873
Yeah, sure. I'll bring it right away.
Just have a seat.

69
00:41:57,874 --> 00:41:59,191
Fine. Thanks.

70
00:42:33,692 --> 00:42:34,861
Thanks.

71
00:43:05,468 --> 00:43:07,090
- Here you go.
- Thanks.

72
00:44:52,055 --> 00:44:53,633
Go ahead, base.

73
00:44:54,463 --> 00:44:57,972
<i>Good evening, Torres. Everything
all right over there?</i>

74
00:44:59,272 --> 00:45:02,098
Good evening. Yes, work is
normal over here.

75
00:45:02,099 --> 00:45:05,352
<i>Listen, I'm sending the truck tomorrow.</i>

76
00:45:05,954 --> 00:45:08,794
<i>How's the road?
Is it heavy with snow?</i>

77
00:45:09,814 --> 00:45:14,919
It snowed a bit last night,
but the road's in good shape.

78
00:45:15,981 --> 00:45:21,420
<i>Listen, tell Trujillo to gather the
sheep for tomorrow or the day after.</i>

79
00:45:24,374 --> 00:45:27,504
<i>How's the situation, Torres? Over.</i>

80
00:45:29,014 --> 00:45:34,584
I'll tell Velazquez to tell Trujillo to
gather the sheep tomorrow. Over.

81
00:45:35,417 --> 00:45:38,016
<i>Fine. Torres, do you need diesel oil?</i>

82
00:45:39,207 --> 00:45:42,601
Well, by what Zapater told me,
the diesel oil is fine.

83
00:45:42,602 --> 00:45:46,808
There's enough. Send 400 liters
next week.

84
00:45:48,856 --> 00:45:50,145
<i>How's Nazarena?</i>

85
00:45:50,446 --> 00:45:51,613
<i>How's her health?</i>

86
00:45:52,632 --> 00:45:56,784
She's... still getting worse, so...
there's no improvement.

87
00:45:58,898 --> 00:46:03,155
<i>Okay, understood. If it doesn't snow
a lot, I'll send the truck on Friday.</i>

88
00:46:04,258 --> 00:46:07,128
<i>Right. We'll expect the truck, then.</i>

89
00:46:35,553 --> 00:46:39,110
- Tell Torres to bring the food.
- I'll tell him right away.

90
00:46:44,178 --> 00:46:46,136
<i>Torres, you can serve the food.</i>

91
00:47:12,507 --> 00:47:14,154
Torres!

92
00:47:40,819 --> 00:47:43,125
Let's go, Analia. I'll walk you.

93
00:50:54,775 --> 00:50:56,277
You can go.

94
00:51:15,781 --> 00:51:17,854
What did you come for?

95
00:51:22,667 --> 00:51:26,938
As soon as you left, Analia was born.

96
00:51:30,033 --> 00:51:31,936
Nobody knows you here.

97
00:51:34,543 --> 00:51:37,114
Not even your mother. She's sick.

98
00:51:45,260 --> 00:51:47,298
Don't know why you came back.

99
00:52:04,602 --> 00:52:06,041
Nice legacy you left me.

100
00:52:11,400 --> 00:52:13,879
What are you looking for?

101
00:52:14,794 --> 00:52:17,711
I know that you can hear me.
Here's the mat� for you.

102
00:55:15,038 --> 00:55:17,955
- Hi. - Hi, how are you?
- Can I have one?

103
00:55:32,025 --> 00:55:33,777
Thanks for everything.

104
00:56:15,248 --> 00:56:17,175
Hi! How are you?

105
00:56:17,176 --> 00:56:19,545
I've brought you some fresh bread.

106
00:56:20,714 --> 00:56:22,350
Have some.

107
00:56:44,553 --> 00:56:46,317
What are you drawing?

108
00:56:46,318 --> 00:56:47,630
"H-hobellazang. "

109
00:57:13,861 --> 00:57:15,213
Give me money?

110
00:57:21,576 --> 00:57:23,209
Give me money?

111
00:57:52,601 --> 00:57:55,293
My legs hurt.

112
00:57:57,494 --> 00:57:58,688
And you?

113
00:58:03,285 --> 00:58:04,862
Do you know who I am?

114
00:58:08,852 --> 00:58:13,921
So many people come,
some known, some unknown.

115
00:58:14,222 --> 00:58:16,006
I'm Farrel.

116
00:58:16,507 --> 00:58:17,581
Farrel!

117
00:58:17,582 --> 00:58:19,111
Farren?

118
00:58:21,670 --> 00:58:25,647
I was this small, when you
were already grown...

119
00:58:53,188 --> 00:58:55,249
Did they call you?

120
00:59:15,299 --> 00:59:18,743
That's good enough! Fussing...

121
00:59:23,386 --> 00:59:27,514
When you're nice
and warm, you go out...

122
00:59:29,315 --> 00:59:31,747
and it's cold.

123
00:59:33,048 --> 00:59:34,835
Isn't that true?

124
00:59:38,602 --> 00:59:40,990
- I'm Farrel.
- Huh?

125
00:59:42,711 --> 00:59:44,264
How's that again?

126
00:59:44,265 --> 00:59:46,898
- Farrel!
- Oh!

127
01:00:05,570 --> 01:00:07,772
Your hands are cold.

128
01:00:18,477 --> 01:00:20,079
<i>Won't you give me money?</i>

129
01:00:35,998 --> 01:00:39,005
Here. Keep it safe so
nobody can take it.

130
01:02:25,296 --> 01:02:26,859
Wait.

131
01:02:31,422 --> 01:02:33,343
Do you still have the money?

132
01:02:34,360 --> 01:02:36,272
It's your money, stash it away.

133
01:02:36,512 --> 01:02:38,032
I'll give you something.

134
01:02:55,126 --> 01:02:56,222
Here.

135
01:02:59,590 --> 01:03:00,694
I'm leaving.

136
01:05:34,160 --> 01:05:36,050
Make yourself comfortable over there.

137
01:06:29,576 --> 01:06:31,873
Have a bit more, it's good for you.

138
01:06:47,496 --> 01:06:52,057
- Is the bread pushed in there?
- Yes, it's pushed.

139
01:06:53,458 --> 01:06:55,424
You need to eat.

140
01:07:07,741 --> 01:07:09,717
In a hurry to get the bread?

141
01:07:13,289 --> 01:07:14,728
Yes.

142
01:07:22,093 --> 01:07:23,854
Go get the food from Torres.

143
01:07:42,349 --> 01:07:43,748
A bit more.

144
01:07:46,698 --> 01:07:48,674
Good thing Farrel's gone.

145
01:08:59,585 --> 01:09:02,163
- Do you like music, Analia?
- Yeah.

146
01:09:07,610 --> 01:09:10,174
Analia, here you are.

147
01:09:31,714 --> 01:09:33,279
How about some gin?

148
01:10:02,539 --> 01:10:07,933
- How was the work?
- They've worked all day.

149
01:10:07,934 --> 01:10:10,142
But they need more logs,
they're trickling in.

150
01:10:10,143 --> 01:10:15,690
- Is the... coming on Saturday?
- Yes the truck's coming in on Saturday.

151
01:10:16,191 --> 01:10:17,733
That should fix it.

152
01:10:19,934 --> 01:10:21,202
How's Trujillo?

153
01:10:21,203 --> 01:10:25,304
All right, I guess.
Always behind the animals.

154
01:10:32,190 --> 01:10:34,223
He's taking care of Nazarena.

155
01:12:54,797 --> 01:12:56,978
Analia! Analia!

156
01:14:44,697 --> 01:14:46,705
D- did you catch anything?

157
01:14:46,706 --> 01:14:48,386
Nothing.

158
01:14:56,440 --> 01:14:58,741
Let's go. Let's go and
see the foxes, Analia.

159
01:15:03,142 --> 01:15:04,704
Walk slowly.

160
01:16:39,782 --> 01:16:41,871
<i>Let's go, Analia, I've caught a fox.</i>

161
01:16:43,372 --> 01:16:45,581
<i>Let's go. I caught a nice one.</i>

162
01:17:15,545 --> 01:17:20,425
You almost fell there,
you have to be careful.

163
01:17:25,149 --> 01:17:28,229
Come on, we still have to lock up
the sheep and it's getting late.

164
01:18:59,082 --> 01:19:01,081
We'll have to remove the mold.

165
01:19:03,298 --> 01:19:05,235
The one we brought is larger.

166
01:19:09,670 --> 01:19:12,084
Take the traps and go
and feed the sheep.

167
01:21:48,937 --> 01:21:52,834
Subtitles: scalisto for KG

