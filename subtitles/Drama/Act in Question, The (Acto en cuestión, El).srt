1
00:03:34,207 --> 00:03:42,160
BATTLE IN HEAVEN

2
00:06:03,407 --> 00:06:04,556
Shit!

3
00:06:04,727 --> 00:06:06,285
How did it happen?

4
00:06:08,047 --> 00:06:09,958
The day is about to break.

5
00:06:10,167 --> 00:06:14,399
I'll run over to the shop
and see what we can do.

6
00:07:38,527 --> 00:07:40,518
So, the baby didn't even cry?

7
00:07:42,207 --> 00:07:44,357
Just a little scream.

8
00:07:47,207 --> 00:07:49,118
What a fuck-up!

9
00:07:51,847 --> 00:07:53,917
Yeah, too bad about the money.

10
00:07:54,087 --> 00:07:55,839
I'm not talking about that.

11
00:07:58,287 --> 00:08:00,198
You're right... poor Vicky.

12
00:08:00,367 --> 00:08:03,564
Poor Vicky?
The baby died, not its mother.

13
00:08:03,727 --> 00:08:05,240
Understand?

14
00:08:05,927 --> 00:08:07,042
Yeah.

15
00:08:07,207 --> 00:08:09,767
Yeah, yeah...
You and your yeahs.

16
00:08:24,447 --> 00:08:25,562
Ma'am?

17
00:08:26,647 --> 00:08:27,875
Ma'am!

18
00:08:28,367 --> 00:08:30,642
- What's for sale?
- Everything there.

19
00:08:30,927 --> 00:08:32,485
How much are the clocks?

20
00:08:32,647 --> 00:08:34,205
Depends which one.

21
00:08:34,487 --> 00:08:36,318
How many hands do they have?

22
00:08:36,527 --> 00:08:37,846
Four.

23
00:08:38,007 --> 00:08:39,998
Are they authentic
''made in Taiwan''?

24
00:08:40,167 --> 00:08:41,998
They were shipped from Paris.

25
00:08:42,367 --> 00:08:44,005
Is the jelly free?

26
00:08:44,847 --> 00:08:46,678
No, you have to pay for it.

27
00:08:46,847 --> 00:08:48,075
How much is it?

28
00:08:48,247 --> 00:08:50,886
Depends, there's water ones
and milk ones.

29
00:08:51,047 --> 00:08:52,036
What flavors?

30
00:08:52,207 --> 00:08:55,324
Strawberry, eggnog,
toffee, coconut,

31
00:08:56,887 --> 00:09:00,197
aniseed, lemon, sherry...

32
00:09:01,007 --> 00:09:02,406
bubble gum...

33
00:09:11,487 --> 00:09:13,762
Hurry-up you bugger, I'm late!

34
00:11:28,167 --> 00:11:31,000
I'm going to the airport
to pick-up the general's daughter.

35
00:11:32,287 --> 00:11:34,005
I'll see you tonight.

36
00:11:34,607 --> 00:11:36,120
What's in the cakes?

37
00:11:37,607 --> 00:11:39,325
One moment, please.

38
00:11:39,607 --> 00:11:40,960
As usual.

39
00:11:44,367 --> 00:11:45,925
Sorry, what is it you wanted?

40
00:11:46,167 --> 00:11:48,317
- What's in the cakes?
- ''Sugus''.

41
00:11:48,567 --> 00:11:50,319
- How much are they?
- Forty pesos.

42
00:11:50,487 --> 00:11:51,636
I'll have one.

43
00:11:52,487 --> 00:11:54,205
Here's fifty.

44
00:11:54,927 --> 00:11:57,157
Here's your change. Thank you.

45
00:13:52,487 --> 00:13:53,806
Your jacket, mister!

46
00:14:01,167 --> 00:14:02,236
Is he drunk orwhat?

47
00:14:02,407 --> 00:14:03,522
Excuse me, please.

48
00:14:05,687 --> 00:14:07,405
Stop it, you damn pervert!

49
00:14:07,567 --> 00:14:09,159
What are you looking at?

50
00:14:16,607 --> 00:14:17,596
Thank you.

51
00:16:01,247 --> 00:16:02,600
Hello Marcos.

52
00:17:09,167 --> 00:17:10,964
They sent you to Cancun?

53
00:17:13,647 --> 00:17:15,239
What a pain in the ass.

54
00:17:17,447 --> 00:17:20,837
I wanted to see you so bad
I came back early.

55
00:17:24,407 --> 00:17:25,840
Well, to see you.

56
00:17:27,327 --> 00:17:28,555
Fine.

57
00:17:31,607 --> 00:17:33,199
What are you talking about?

58
00:17:42,007 --> 00:17:44,999
Well, if you interrogate me
I might become suspicious.

59
00:17:50,047 --> 00:17:51,400
OK, we'll see.

60
00:17:54,847 --> 00:17:56,963
I'll stay at your place
for the weekend then.

61
00:17:57,127 --> 00:18:00,358
I'll ask Marcos to tell my parents
I didn't show up.

62
00:18:01,247 --> 00:18:03,283
You won't be back before noon?

63
00:18:06,327 --> 00:18:08,636
In that case,
I'm going to work all night.

64
00:18:09,007 --> 00:18:11,999
And in the morning,
I'll come sleep at your place.

65
00:18:12,487 --> 00:18:14,603
Wake me up when you arrive.

66
00:18:17,927 --> 00:18:19,758
Yes, I really want to see you.

67
00:18:25,727 --> 00:18:27,524
That sounds good!

68
00:18:33,087 --> 00:18:34,315
Of course, Jaime.

69
00:18:39,607 --> 00:18:42,246
I won't hang up, darling.
But don't take long.

70
00:19:00,767 --> 00:19:02,598
Remember
that ugly uniform, Marcos?

71
00:19:02,767 --> 00:19:04,405
Of course, Ana.

72
00:19:05,127 --> 00:19:06,526
How could I forget?

73
00:19:07,447 --> 00:19:10,245
Yeah, but wasn't it much better
when I was there?

74
00:19:10,447 --> 00:19:12,517
- Yes, Ana...
- You're back, love?

75
00:19:14,287 --> 00:19:16,926
It's just that
we went past my old school...

76
00:19:17,087 --> 00:19:19,965
and I asked Marcos
ifhe remembered driving me there.

77
00:19:21,167 --> 00:19:22,646
What a pain.

78
00:19:24,727 --> 00:19:26,240
I hope you will!

79
00:20:01,047 --> 00:20:02,275
Really?

80
00:20:03,647 --> 00:20:05,638
Let's see
how you talk in bed then...

81
00:20:12,567 --> 00:20:14,000
See you tomorrow.

82
00:20:17,447 --> 00:20:18,675
Listen...

83
00:20:20,607 --> 00:20:22,086
I love you.

84
00:20:28,167 --> 00:20:30,362
Let's go to the boutique instead,
Marcos.

85
00:20:30,567 --> 00:20:32,364
I'll turn at the corner, Ana.

86
00:20:33,807 --> 00:20:35,559
Jaime's ice-skates are fixed, Ana.

87
00:20:35,727 --> 00:20:36,762
Did they do a good job?

88
00:20:36,927 --> 00:20:39,441
Great Ana,
I took them to a digital engineer.

89
00:20:42,607 --> 00:20:44,040
Here they are...

90
00:21:04,527 --> 00:21:06,358
Razor-sharp... He'll be happy.

91
00:21:07,007 --> 00:21:08,122
Pardon?

92
00:21:09,567 --> 00:21:11,876
Nothing,
just turn the music on, Marcos.

93
00:22:02,207 --> 00:22:04,596
Wake up,
you fucking piece of shit...

94
00:22:04,767 --> 00:22:06,962
and go fuck your mother
you faggot!

95
00:22:07,567 --> 00:22:09,000
Get moving, damn it!

96
00:22:17,327 --> 00:22:19,397
Now turn off the music, Marcos.

97
00:22:30,607 --> 00:22:32,325
Are you all right, Marcos?

98
00:22:33,127 --> 00:22:35,641
It's just my mother.
Her body's full of water.

99
00:22:36,567 --> 00:22:38,080
Is that all?

100
00:22:38,247 --> 00:22:39,521
Yes, Ana.

101
00:22:40,287 --> 00:22:42,801
Well, I've never seen you
so absent-minded.

102
00:22:43,327 --> 00:22:44,521
No, Ana.

103
00:22:45,407 --> 00:22:47,318
Well at the boutique,
you're coming in...

104
00:22:47,487 --> 00:22:49,443
so I can set you up
with one of my friends.

105
00:22:50,807 --> 00:22:52,638
Can we do it another day, Ana?

106
00:22:53,407 --> 00:22:56,001
Don't tell me
you're faithful to yourwife?

107
00:22:57,807 --> 00:23:00,560
Yes, Ana. But sometimes
I go out with other women.

108
00:23:01,687 --> 00:23:04,155
You've always wanted
to see the boutique, right?

109
00:23:04,807 --> 00:23:06,923
Yeah... whatever you say, Ana.

110
00:23:21,807 --> 00:23:23,206
Lie down.

111
00:23:52,607 --> 00:23:53,676
Ana!

112
00:23:54,567 --> 00:23:56,478
The fatso is out of order.

113
00:23:56,887 --> 00:23:58,923
He says
he only wants it with you.

114
00:24:34,567 --> 00:24:36,523
What are you doing, Marcos?

115
00:24:36,727 --> 00:24:38,797
I was just taking the condom off.

116
00:24:56,847 --> 00:24:59,566
You've worked for my dad
for 15 years.

117
00:25:00,007 --> 00:25:02,202
You've known me
since I was a child.

118
00:25:02,487 --> 00:25:04,717
You're the only one
who knows about this place...

119
00:25:04,887 --> 00:25:05,956
I invite you here...

120
00:25:06,127 --> 00:25:08,243
and you go telling my friends
you want to screw me.

121
00:25:08,407 --> 00:25:09,635
How's that?

122
00:25:09,807 --> 00:25:12,799
All I said was
itwas your idea, Ana.

123
00:25:12,967 --> 00:25:16,562
You think I didn't notice how
you stared at me at the airport?

124
00:25:19,807 --> 00:25:21,160
What's wrong?

125
00:25:23,487 --> 00:25:24,761
Tell me.

126
00:25:24,927 --> 00:25:26,679
The thing is my wife and I
kidnapped a baby...

127
00:25:26,847 --> 00:25:28,360
and he died this morning.

128
00:26:53,487 --> 00:26:54,920
It was an accident.

129
00:27:01,447 --> 00:27:03,722
Why are you telling me this,
Marcos?

130
00:27:22,847 --> 00:27:24,360
You asked me.

131
00:27:32,087 --> 00:27:34,362
Those things you shouldn't tell.

132
00:27:46,367 --> 00:27:50,280
Take my bags to Jaime's.
The maid will open the doorfor you.

133
00:27:52,087 --> 00:27:55,204
Take the car with you as well.
See you on Monday.

134
00:27:57,967 --> 00:27:59,719
Now leave, Marcos!

135
00:28:07,047 --> 00:28:08,958
Just don't tell anyone.

136
00:30:17,767 --> 00:30:18,756
Full service?

137
00:30:18,927 --> 00:30:19,962
Pardon? Yes.

138
00:31:17,287 --> 00:31:19,801
<i>Mexico loves you...</i>

139
00:31:20,087 --> 00:31:24,000
<i>You are our Virgin mother.</i>

140
00:31:31,287 --> 00:31:35,485
<i>We're going to heaven...</i>

141
00:31:35,647 --> 00:31:39,162
<i>and you hold the key.</i>

142
00:31:40,687 --> 00:31:43,599
<i>And we honor you...</i>

143
00:31:43,767 --> 00:31:46,759
<i>on this pilgrimage.</i>

144
00:32:17,847 --> 00:32:18,996
What do you think?

145
00:32:19,647 --> 00:32:21,319
They're a flock of sheep...

146
00:32:25,967 --> 00:32:27,320
How many are there?

147
00:32:27,647 --> 00:32:30,115
The pilgrims have been coming
for 3 days and nights.

148
00:32:32,447 --> 00:32:34,358
But I was talking about the car...

149
00:32:37,527 --> 00:32:39,119
and the engine is fine.

150
00:33:37,687 --> 00:33:39,279
We're almost there.

151
00:33:51,287 --> 00:33:52,959
Careful with Grandma.

152
00:34:03,527 --> 00:34:05,245
What's this music, son?

153
00:34:05,407 --> 00:34:07,204
Don't worry, Grandma.

154
00:34:08,887 --> 00:34:12,004
Mister! Put in 200 pesos, please!

155
00:34:12,687 --> 00:34:14,359
And change that music now!

156
00:34:52,207 --> 00:34:53,686
Irving, Irving...

157
00:34:55,887 --> 00:34:57,843
Wake up, your dad's arrived.

158
00:35:44,967 --> 00:35:47,322
Sorry for being angry
this morning.

159
00:36:17,607 --> 00:36:19,245
We've got to talk.

160
00:38:33,487 --> 00:38:35,682
Did you lose your glasses, love?

161
00:38:41,647 --> 00:38:43,683
They were all scratched anyway.

162
00:38:45,567 --> 00:38:47,762
Take your old pair then.

163
00:39:25,367 --> 00:39:26,800
I told Ana.

164
00:39:30,807 --> 00:39:32,479
Your boss's daughter?

165
00:39:33,687 --> 00:39:35,757
Are you fucking crazy, or what?

166
00:39:35,927 --> 00:39:37,406
I feel better.

167
00:39:48,567 --> 00:39:51,081
You're not well, are you love?

168
00:39:57,167 --> 00:39:58,725
I understand...

169
00:39:59,847 --> 00:40:02,645
Now you'll have to make sure
she keeps her mouth shut...

170
00:40:02,967 --> 00:40:05,276
We can't have
the princess talking.

171
00:40:06,967 --> 00:40:08,798
You better leave her out of this!

172
00:40:09,127 --> 00:40:11,277
You fucked up, you idiot!

173
00:40:29,607 --> 00:40:32,041
Vicky came to me crying today...

174
00:40:32,207 --> 00:40:35,677
she said she's had no news
from the kidnappers for 3 days...

175
00:40:36,127 --> 00:40:39,358
I'm afraid that if we don't call her
tonight, she'll go to the police.

176
00:40:45,887 --> 00:40:49,277
If her husband were still alive,
he'd have broken both our necks.

177
00:40:52,527 --> 00:40:54,757
Should we go
to Sunday's pilgrimage?

178
00:40:55,087 --> 00:40:57,396
Where do you get
those fucking ideas?

179
00:41:00,607 --> 00:41:03,963
After all the shit we've done...
don't you realize?

180
00:42:24,967 --> 00:42:27,037
He's not big enough
to come by himself?.

181
00:42:27,207 --> 00:42:28,959
Why don't you ask him?

182
00:42:29,127 --> 00:42:30,355
Stop it, Dad!

183
00:42:35,007 --> 00:42:36,759
Your pet is waiting for you.

184
00:42:37,527 --> 00:42:39,279
I brought your car Ana!

185
00:42:40,607 --> 00:42:42,837
Go prove you're a man, son.

186
00:42:46,567 --> 00:42:48,398
- Bye, handsome.
- Thank you.

187
00:42:52,927 --> 00:42:54,997
- Wait for me outside.
- Yes, Ana.

188
00:44:20,687 --> 00:44:23,326
Fuck off you faggots!

189
00:44:28,247 --> 00:44:30,920
I took her twice up the shitter!

190
00:44:31,087 --> 00:44:35,205
Tona, Juanita, come get
our stuff from the trunk.

191
00:44:35,887 --> 00:44:38,685
- What's the shitter?
- The shitter?

192
00:44:38,847 --> 00:44:41,236
You'll learn soon enough,
my dear...

193
00:45:14,127 --> 00:45:15,321
Watch out!

194
00:45:18,487 --> 00:45:19,556
Good morning, Juanito.

195
00:45:19,727 --> 00:45:21,046
What's up, Tonita?

196
00:45:22,407 --> 00:45:23,601
Hello, Tonita.

197
00:45:28,247 --> 00:45:31,319
Get inside Ines. What kind of time
is this to come home?

198
00:45:31,487 --> 00:45:34,365
- I'll tell your parents.
- Come on, Tonita.

199
00:45:40,487 --> 00:45:42,239
Good morning, fatso.

200
00:45:51,407 --> 00:45:53,079
What is it now, Marcos?

201
00:46:05,967 --> 00:46:08,322
I told you
I'd see you Monday, Marcos.

202
00:46:08,487 --> 00:46:10,239
I wanted to tell you
something, Ana.

203
00:46:10,767 --> 00:46:11,916
Good morning.

204
00:46:51,767 --> 00:46:54,235
He likes to listen
to the morning birds.

205
00:47:23,207 --> 00:47:26,324
What you really want
is to fuck me, isn't it Marcos?

206
00:47:26,927 --> 00:47:28,246
No, Ana.

207
00:47:29,207 --> 00:47:30,720
Where's my car?

208
00:47:30,887 --> 00:47:32,320
Up the road.

209
00:47:32,647 --> 00:47:34,160
We just came that way.

210
00:47:34,327 --> 00:47:36,045
You didn't ask me before.

211
00:49:20,607 --> 00:49:22,006
Calm down, Marcos!

212
00:50:14,647 --> 00:50:16,683
If I won a million in the lottery...

213
00:50:18,087 --> 00:50:19,918
I'd forget about you.

214
00:53:25,327 --> 00:53:27,795
You'll have to turn yourself in,
Marcos.

215
00:55:05,167 --> 00:55:07,203
You have to go now, Marcos.

216
00:56:35,367 --> 00:56:36,720
<i>Ladies & gentlemen...</i>

217
00:56:36,887 --> 00:56:38,878
<i>20 seconds from now the PUMAS</i>

218
00:56:39,047 --> 00:56:41,436
<i>will once again be crowned
champions of Mexico.</i>

219
00:56:42,207 --> 00:56:46,723
<i>One of the most prestigious
squads of the country...</i>

220
00:56:46,887 --> 00:56:50,596
<i>an institution
that loves expansive football...</i>

221
00:56:51,567 --> 00:56:56,925
<i>PUMAS is the home of artists
and imagination...</i>

222
00:56:59,207 --> 00:57:01,721
<i>Ladies & gentlemen
the PUMAS are champions...</i>

223
00:57:01,887 --> 00:57:03,605
<i>champions of Mexico!</i>

224
00:57:03,767 --> 00:57:06,156
<i>How our league has changed...</i>

225
00:57:06,327 --> 00:57:11,355
<i>now that financial speculation
has stopped...</i>

226
00:57:12,047 --> 00:57:18,202
<i>now that we've reduced
the number of foreign players...</i>

227
00:57:18,767 --> 00:57:21,725
<i>What's good for Mexican football,
is good for Mexico!</i>

228
00:57:21,887 --> 00:57:25,277
<i>And now overto Andre,
on the field with Vignatti...</i>

229
00:57:25,527 --> 00:57:28,405
Here is the great
Argentine keeper, Vignatti.

230
00:57:28,567 --> 00:57:30,762
Finally, champion with PUMAS...

231
00:57:30,927 --> 00:57:33,521
As they would say
in your country: ''refashion''.

232
00:57:33,887 --> 00:57:36,242
This is unreal.

233
00:57:36,407 --> 00:57:40,605
This is the best thing
that has ever happened to Vignatti...

234
00:57:40,767 --> 00:57:43,201
I want to thank my family...

235
00:57:43,367 --> 00:57:47,155
who convinced me
to come to Mexico.

236
00:57:47,327 --> 00:57:51,002
This is a huge league,
PUMAS is a major team...

237
00:57:51,287 --> 00:57:56,281
Now we'll go and celebrate
with our families in Argentina...

238
00:57:57,407 --> 00:58:01,320
with ourwives and friends...

239
00:58:04,247 --> 00:58:05,805
This is unreal...

240
00:58:10,287 --> 00:58:12,278
This is unreal...

241
00:58:13,367 --> 00:58:16,006
The best thing
that ever happened to Vignatti...

242
00:58:16,207 --> 00:58:18,038
The best thing.

243
00:58:23,287 --> 00:58:24,800
Dad! Dad!

244
00:58:25,447 --> 00:58:28,519
Hurry up, we're going
to the countryside!

245
00:58:28,927 --> 00:58:32,966
Marcos, my love,
don't make the family wait.

246
00:58:33,647 --> 00:58:35,763
What are you doing, Dad?

247
00:59:14,087 --> 00:59:15,725
Stop crying, Vicky.

248
00:59:15,967 --> 00:59:18,117
You'll see your baby very soon...

249
00:59:18,487 --> 00:59:22,116
We'll raise the money, Grandma
will sell the house. Don't worry...

250
01:00:24,607 --> 01:00:26,404
Tomorrow's the pilgrimage.

251
01:00:29,207 --> 01:00:31,402
Tomorrow I'm turning myself in
to the police...

252
01:00:53,287 --> 01:00:54,640
Forgive me.

253
01:00:57,567 --> 01:01:00,400
We'll be separated
but you'll always be in my heart.

254
01:01:05,567 --> 01:01:07,558
I couldn't live without you.

255
01:01:20,047 --> 01:01:22,277
Please wait one more day.

256
01:01:24,527 --> 01:01:26,483
Wait for the pilgrimage.

257
01:01:27,447 --> 01:01:29,802
I feel something's
going to happen there...

258
01:01:31,807 --> 01:01:33,718
Something's going to change...

259
01:01:35,887 --> 01:01:38,321
What's a single day,
for God's sake?

260
01:01:56,647 --> 01:01:58,444
OK. Now promise.

261
01:02:01,487 --> 01:02:03,045
Swear on our love.

262
01:02:17,927 --> 01:02:19,121
Thanks, love.

263
01:02:22,447 --> 01:02:24,278
Are you raising
the flag tomorrow?

264
01:02:24,447 --> 01:02:26,119
Yeah, I have to go twice.

265
01:02:26,647 --> 01:02:29,559
Well, after you've raised it
at dawn...

266
01:02:30,287 --> 01:02:31,766
come pick me up at home

267
01:02:31,927 --> 01:02:33,963
so we can go together
on the pilgrimage.

268
01:02:34,407 --> 01:02:37,877
Then at sunset I'll go with you
to bring the flag down.

269
01:02:38,807 --> 01:02:41,241
I haven't seen you in action
for a long time.

270
01:02:42,207 --> 01:02:43,356
OK.

271
01:02:45,207 --> 01:02:47,357
All these clouds
are making my eyes itch.

272
01:03:09,727 --> 01:03:12,400
Children, stop playing around!

273
01:03:12,567 --> 01:03:15,684
Who do you think
does the washing?

274
01:03:16,167 --> 01:03:19,762
Marcos! Take care of the children,
for God's sake!

275
01:03:43,127 --> 01:03:45,846
Come over here, children!
I can't see you!

276
01:03:46,007 --> 01:03:48,475
It's dangerous to play like that!

277
01:04:07,607 --> 01:04:10,804
Marcos, come back to the car!

278
01:09:22,127 --> 01:09:23,958
Good job, Marcos.
See you tomorrow.

279
01:09:24,127 --> 01:09:25,196
Yes, General.

280
01:09:25,367 --> 01:09:27,881
Thank you.
Have a good morning.

281
01:09:39,567 --> 01:09:40,886
Good morning, Marcos.

282
01:09:41,047 --> 01:09:42,366
You've come to see Ana?

283
01:09:42,527 --> 01:09:44,358
- Just for a minute, Jaime.
- Come in.

284
01:09:44,567 --> 01:09:45,716
Thank you.

285
01:09:46,007 --> 01:09:48,396
Were you on top or underneath
this morning?

286
01:09:49,727 --> 01:09:50,921
On top.

287
01:10:00,287 --> 01:10:02,437
Come in the living room.
Don't be stupid!

288
01:10:34,127 --> 01:10:35,685
I'll let Ana know.

289
01:11:57,807 --> 01:11:59,286
Hello, Marcos.

290
01:12:00,527 --> 01:12:02,757
Jaime is going to buy
the newspaper.

291
01:12:05,127 --> 01:12:07,880
Yes, Marcos. I'll be right back.

292
01:12:22,007 --> 01:12:24,567
I'm turning myself
in to the police now, Ana...

293
01:12:33,207 --> 01:12:35,004
Sit down then.

294
01:13:34,607 --> 01:13:36,996
Won't you mind us
being separated, Ana?

295
01:13:38,207 --> 01:13:41,279
No, Marcos.
I'll keep you in my heart.

296
01:13:53,927 --> 01:13:55,201
Goodbye.

297
01:15:01,167 --> 01:15:03,317
You forgot your jacket, Marcos.

298
01:17:05,567 --> 01:17:07,125
Take care, Marcos.

299
01:17:38,527 --> 01:17:39,562
Yes.

300
01:17:44,287 --> 01:17:46,084
What do you mean ''naked''?

301
01:17:52,967 --> 01:17:55,640
Thank you.

302
01:17:56,327 --> 01:17:57,965
Goodbye.

303
01:18:03,047 --> 01:18:05,003
The suspect's cellphone
was found, Inspector.

304
01:18:05,167 --> 01:18:07,476
He was seen half-naked
in the street 5 minutes ago...

305
01:18:07,647 --> 01:18:09,399
and he wanted
to join a pilgrimage.

306
01:18:09,567 --> 01:18:13,162
Some pilgrims showed him
the way to the Basilica...

307
01:18:13,327 --> 01:18:15,921
They sent him on a bus...

308
01:18:16,087 --> 01:18:18,237
as they thought he was a spastic.

309
01:18:25,007 --> 01:18:26,998
Ma'am, you're coming with us...

310
01:18:27,167 --> 01:18:29,442
We're going to find
your beloved fucker...

311
01:18:30,367 --> 01:18:33,677
And your child stays here
with the lieutenant.

312
01:19:01,207 --> 01:19:03,084
<i>Station calling Blue Wolf...</i>

313
01:19:05,927 --> 01:19:07,838
<i>We have some information
from the house.</i>

314
01:19:08,167 --> 01:19:09,646
<i>I copy you station.</i>

315
01:19:12,527 --> 01:19:15,564
<i>The kidnapper's son just
led us to a baby's body.</i>

316
01:19:18,047 --> 01:19:19,765
Ask for backup, damn it!

317
01:19:21,767 --> 01:19:24,839
<i>Jesus will help you change!</i>

318
01:19:25,327 --> 01:19:27,761
<i>This is the way of the light!</i>

319
01:19:28,087 --> 01:19:32,365
<i>Save yourself!
I have the key to salvation!</i>

320
01:19:33,487 --> 01:19:36,047
<i>You that are addicted to drugs!</i>

321
01:19:50,367 --> 01:19:51,766
<i>Those pills...</i>

322
01:19:51,927 --> 01:19:53,997
<i>They get you nowhere!</i>

323
01:19:57,007 --> 01:19:59,601
<i>There you are again brother!</i>

324
01:19:59,767 --> 01:20:04,045
<i>I will solve all your problems!</i>

325
01:20:04,607 --> 01:20:08,316
<i>You poor sheep! Jesus loves you!
Return to the path!</i>

326
01:20:08,487 --> 01:20:10,205
Take this. You are saved!

327
01:20:10,967 --> 01:20:12,958
<i>I have saved you!</i>

328
01:20:13,327 --> 01:20:17,559
<i>Yes, brothers!
This is the only proof!</i>

329
01:20:18,047 --> 01:20:20,197
<i>You have your medicine now!</i>

330
01:20:20,367 --> 01:20:22,358
<i>Go forward! No more drugs!</i>

331
01:20:22,527 --> 01:20:24,085
<i>No more alcohol!</i>

332
01:20:24,407 --> 01:20:25,760
<i>No more women!</i>

333
01:20:25,967 --> 01:20:27,320
<i>No more tits!</i>

334
01:20:28,287 --> 01:20:29,845
<i>No more tits!</i>

335
01:21:14,887 --> 01:21:16,320
We're here, Inspector.

336
01:21:18,927 --> 01:21:20,963
Inspector, how are you doing?

337
01:21:24,687 --> 01:21:26,359
I must inform you, Inspector...

338
01:21:28,567 --> 01:21:31,843
Inspector, I must inform you that
the girl who was stabbed...

339
01:21:32,207 --> 01:21:33,879
has just died.

340
01:21:42,447 --> 01:21:44,005
It's disgusting, Chief.

341
01:21:44,327 --> 01:21:46,045
Hello station...
Calling station.

342
01:21:46,327 --> 01:21:48,602
<i>Yes Officer.
We have information...</i>

343
01:21:48,767 --> 01:21:51,679
<i>The suspect has been
located near the Basilica.</i>

344
01:21:55,567 --> 01:21:57,876
<i>Beware, he's likely to be armed!</i>

345
01:23:24,007 --> 01:23:27,966
<i>We bring these gifts for you.</i>

346
01:23:30,127 --> 01:23:32,243
<i>Oh, father!</i>

347
01:23:32,407 --> 01:23:34,637
<i>This is our song.</i>

348
01:23:36,607 --> 01:23:38,598
<i>I've tried to live</i>

349
01:23:38,767 --> 01:23:41,406
<i>by your light...</i>

350
01:23:43,927 --> 01:23:48,045
<i>we love justice and peace.</i>

351
01:23:50,607 --> 01:23:53,167
<i>We know you will come...</i>

352
01:23:53,487 --> 01:23:54,556
On your feet...

353
01:23:54,727 --> 01:23:57,036
<i>We know you will come...</i>

354
01:23:58,287 --> 01:24:01,836
<i>and bring peace to the poor.</i>

355
01:24:48,887 --> 01:24:50,115
Please.

356
01:24:50,287 --> 01:24:51,686
Let me go in first.

357
01:24:57,527 --> 01:24:59,483
We've been waiting so long...

358
01:29:45,407 --> 01:29:46,920
I love you so, Ana.

359
01:29:49,767 --> 01:29:51,485
I love you too, Marcos.

360
01:34:03,327 --> 01:34:06,285
Subtitles: Benjamin Mirguet

361
01:34:11,887 --> 01:34:15,800
In memory of Alejandro Ferretis

