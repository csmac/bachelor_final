﻿1
00:00:05,630 --> 00:00:07,636
Subtitles provided by MBC

2
00:00:13,130 --> 00:00:15,230
I wish I were dead.

3
00:00:17,630 --> 00:00:19,730
Sungnyang.

4
00:00:27,130 --> 00:00:31,470
I wish I had died
on the Isle of Daechong.

5
00:00:40,400 --> 00:00:43,470
Why are you here?

6
00:00:44,130 --> 00:00:47,070
To speak with Your Majesty.

7
00:00:47,070 --> 00:00:49,800
To laugh at me?

8
00:00:50,800 --> 00:00:55,130
To say I brought it on myself?

9
00:00:55,370 --> 00:01:01,130
To tell me this is what I get
for betraying you?

10
00:01:03,100 --> 00:01:05,170
Go ahead. Laugh.

11
00:01:05,170 --> 00:01:07,130
Just like in Koryo.

12
00:01:07,130 --> 00:01:13,130
I'm a fool,
I'm worthless, say it!

13
00:01:19,770 --> 00:01:25,130
Don't get your hopes up.

14
00:01:25,530 --> 00:01:28,130
I will do nothing for you.

15
00:01:28,300 --> 00:01:34,130
For you... nothing.

16
00:01:37,130 --> 00:01:39,130
My Liege.

17
00:01:40,130 --> 00:01:44,300
My Liege, you're needed
at the inquisitor's office.

18
00:01:45,130 --> 00:01:48,130
They're planning the examination.

19
00:02:03,770 --> 00:02:05,730
Take her.

20
00:02:07,130 --> 00:02:09,030
What is this?

21
00:02:09,030 --> 00:02:10,130
Answer me. Unhand me.

22
00:02:10,130 --> 00:02:15,870
My lady, my lady.

23
00:02:22,000 --> 00:02:24,130
Episode 13

24
00:02:36,130 --> 00:02:39,130
You don't look well.

25
00:02:39,130 --> 00:02:43,830
Should you be up and about?

26
00:02:44,130 --> 00:02:47,130
You're worried about me?

27
00:02:47,130 --> 00:02:50,130
Thank you.

28
00:02:57,800 --> 00:03:03,130
She's not
an easy woman to break.

29
00:03:05,770 --> 00:03:07,430
Just wait.

30
00:03:07,430 --> 00:03:10,130
After the examination...

31
00:03:10,130 --> 00:03:13,570
she'll be a nun,
shaved head and all.

32
00:03:33,870 --> 00:03:39,130
His Royal Majesty.

33
00:03:56,430 --> 00:03:59,130
Why is she
being examined here?

34
00:03:59,130 --> 00:04:03,430
A fake pregnancy
cannot be investigated in private.

35
00:04:03,830 --> 00:04:06,130
We'll learn who put her up to it...

36
00:04:06,130 --> 00:04:09,270
even if it means
skinning her alive.

37
00:04:17,870 --> 00:04:20,130
Doctor, examine her.

38
00:04:40,130 --> 00:04:43,130
Report. Is she pregnant?

39
00:04:43,130 --> 00:04:48,070
M-my lord...

40
00:04:49,630 --> 00:04:52,130
Spit it out.

41
00:04:52,130 --> 00:04:54,270
You other doctors,
examine her.

42
00:05:07,130 --> 00:05:09,130
She is with child.

43
00:05:09,130 --> 00:05:13,670
The slippery pulse,
without question.

44
00:05:20,130 --> 00:05:21,300
Explain.

45
00:05:21,300 --> 00:05:23,470
It can't be.

46
00:05:23,470 --> 00:05:28,130
She had the powder
and the tonic both.

47
00:05:37,070 --> 00:05:41,130
What? Remove the licorice
from her tonic?

48
00:05:41,130 --> 00:05:45,730
Her ladyship
is urinating far too much.

49
00:05:45,730 --> 00:05:49,130
It may be from laurel powder.

50
00:05:49,130 --> 00:05:51,530
Laurel?

51
00:05:52,130 --> 00:05:58,100
Then Her Majesty's purpose
in ordering the tonic...

52
00:05:58,100 --> 00:06:01,670
To cause a miscarriage.

53
00:06:36,700 --> 00:06:38,730
My lady!

54
00:06:50,130 --> 00:06:52,130
Well?

55
00:06:52,130 --> 00:06:55,700
The child is well.

56
00:06:56,130 --> 00:07:01,130
Fortunately Yonhwa only slipped her
a small amount of powder.

57
00:07:01,130 --> 00:07:05,170
Go tell them
she'll agree to the examination.

58
00:07:05,170 --> 00:07:06,270
Not yet.

59
00:07:06,270 --> 00:07:07,130
Not yet?

60
00:07:07,130 --> 00:07:12,570
This is our chance to have
the Empress fall in her own trap.

61
00:07:12,570 --> 00:07:13,430
You...?

62
00:07:13,430 --> 00:07:18,800
This fight will never end
until one or the other falls.

63
00:07:18,970 --> 00:07:23,230
We have to end it here.

64
00:07:25,070 --> 00:07:26,130
Please don't.

65
00:07:26,130 --> 00:07:27,130
Don't?

66
00:07:27,130 --> 00:07:31,130
Her clothes
were soaked through with sweat.

67
00:07:31,130 --> 00:07:35,600
I will check
the pulse on her wrist.

68
00:07:35,600 --> 00:07:38,370
- Do so.
- Yes, Your Majesty.

69
00:07:49,930 --> 00:07:52,100
What is it?

70
00:07:52,100 --> 00:07:56,130
I can't find the slippery pulse.

71
00:07:57,130 --> 00:07:59,130
Mother.

72
00:08:07,970 --> 00:08:12,630
Congratulations
on the forthcoming child.

73
00:08:13,130 --> 00:08:16,630
Congratulations are not enough.

74
00:08:18,670 --> 00:08:25,130
My Liege, the royal house
has been held up to ridicule.

75
00:08:27,900 --> 00:08:33,130
Her Majesty must be held accountable
for spreading such malicious gossip.

76
00:08:33,130 --> 00:08:35,700
First I have a question for her.

77
00:08:35,700 --> 00:08:40,130
Why were you so certain
the pregnancy was faked?

78
00:08:42,800 --> 00:08:43,430
Ah, well...

79
00:08:43,430 --> 00:08:47,600
Her crime is clear.

80
00:08:52,130 --> 00:08:54,770
She foolishly listened to gossip.

81
00:08:54,770 --> 00:08:58,130
And so brought shame
on the imperial house.

82
00:08:58,130 --> 00:09:01,130
She must answer for that.

83
00:09:01,130 --> 00:09:05,130
How will she do that exactly?

84
00:09:08,130 --> 00:09:11,130
Groveling for mercy should do it.

85
00:09:15,130 --> 00:09:16,270
Father.

86
00:09:16,270 --> 00:09:19,930
Groveling isn't nearly enough.

87
00:09:19,930 --> 00:09:24,130
We must make
an example of the instigator...

88
00:09:24,130 --> 00:09:26,130
Your Majesty.

89
00:09:26,770 --> 00:09:31,430
The traditional groveling
is my only offer.

90
00:09:31,430 --> 00:09:35,900
Surely you don't want her exiled?

91
00:09:36,130 --> 00:09:40,000
Or do you?

92
00:09:42,130 --> 00:09:44,670
Enough.

93
00:10:18,130 --> 00:10:19,430
What is this?

94
00:10:19,430 --> 00:10:22,200
You can fool others but not me.

95
00:10:22,200 --> 00:10:24,370
You're behind all this.

96
00:10:24,370 --> 00:10:26,730
I'll have you...!

97
00:10:27,130 --> 00:10:30,100
You know what this is?

98
00:10:30,100 --> 00:10:32,130
What?

99
00:10:40,400 --> 00:10:43,330
My written promise
to send you from the palace.

100
00:10:43,330 --> 00:10:48,130
Your seal, if you please.

101
00:10:48,130 --> 00:10:49,630
Wha...?

102
00:10:49,630 --> 00:10:53,470
The letter is enough.
How dare you?

103
00:10:53,470 --> 00:10:54,770
It's fine.

104
00:10:54,770 --> 00:11:01,470
I'll seal anything if it means
getting rid of Lady Pak.

105
00:11:05,900 --> 00:11:10,370
Sungnyang, you...

106
00:11:11,100 --> 00:11:13,130
You wanted me dead?

107
00:11:13,130 --> 00:11:18,130
Fine. Let's see
which of us dies first.

108
00:11:21,130 --> 00:11:23,330
I'll scream.

109
00:11:24,030 --> 00:11:28,800
You don't want
Her Majesty's treason made public.

110
00:11:28,800 --> 00:11:31,130
Little fool.

111
00:11:31,430 --> 00:11:36,170
You would make us enemies
to avenge your king?

112
00:11:36,170 --> 00:11:38,130
This is not about the king.

113
00:11:38,130 --> 00:11:39,330
What then?

114
00:11:39,330 --> 00:11:45,130
You killed my mother.

115
00:11:46,170 --> 00:11:50,130
She died by your arrow.

116
00:11:50,270 --> 00:11:53,400
If you want to be rid of me...

117
00:11:54,130 --> 00:11:58,130
send me out of the palace.

118
00:11:58,130 --> 00:12:03,130
I'd be glad
to never see you again.

119
00:12:12,770 --> 00:12:14,400
I can't...

120
00:12:14,530 --> 00:12:17,130
I can't really...

121
00:12:17,130 --> 00:12:20,600
be falling for her?

122
00:12:51,930 --> 00:12:57,770
Your Majesty, it is time.

123
00:12:58,570 --> 00:13:03,300
- Your Majesty.
- Your Majesty.

124
00:13:04,230 --> 00:13:06,770
No tears.

125
00:13:07,130 --> 00:13:09,200
Your Majesty.

126
00:13:09,200 --> 00:13:14,230
I am the Yuan Empress.

127
00:13:15,530 --> 00:13:19,130
I can take the jeers of others
behind my back.

128
00:13:19,130 --> 00:13:25,170
But I will not have my retinue
pity me in front of me.

129
00:13:25,170 --> 00:13:28,130
Your Majesty.

130
00:13:28,470 --> 00:13:31,000
Lead the way.

131
00:13:31,000 --> 00:13:34,130
I will call on the Emperor.

132
00:13:50,130 --> 00:13:55,130
My Liege, the Empress is here.

133
00:14:12,630 --> 00:14:20,130
My Liege, I am to blame
for all that happened.

134
00:14:20,330 --> 00:14:25,800
Forgive me.

135
00:14:26,130 --> 00:14:28,430
My Liege!

136
00:14:42,030 --> 00:14:47,930
My Liege,
forgive your unworthy Empress.

137
00:14:47,930 --> 00:14:51,700
It will never happen again.

138
00:14:51,700 --> 00:14:55,270
Have mercy, My Liege.

139
00:15:09,630 --> 00:15:11,330
I can't take
much more of this.

140
00:15:11,330 --> 00:15:13,130
I'm taking her inside.

141
00:15:13,130 --> 00:15:15,130
- No.
- Father.

142
00:15:15,130 --> 00:15:17,300
Leave her.

143
00:15:18,230 --> 00:15:23,930
Sometimes you must lose
an arm to save the head.

144
00:15:23,930 --> 00:15:26,770
She must observe custom.

145
00:15:26,770 --> 00:15:29,630
It must be done.

146
00:15:43,900 --> 00:15:46,800
Tell her it's enough.

147
00:15:47,000 --> 00:15:48,130
No, My Liege.

148
00:15:48,130 --> 00:15:50,070
It's raining.

149
00:15:50,070 --> 00:15:51,970
If she should fall ill...

150
00:15:51,970 --> 00:15:56,130
First make Lady Pak
an imperial consort.

151
00:15:56,130 --> 00:16:00,370
Mother, we pushed El Temur
as far as we can.

152
00:16:00,370 --> 00:16:02,600
Influence ages.

153
00:16:02,600 --> 00:16:05,130
Just as people do.

154
00:16:05,130 --> 00:16:08,130
Influence cracks with age.

155
00:16:08,130 --> 00:16:11,130
Make her a consort
and if she bears a son...

156
00:16:11,130 --> 00:16:15,130
the court will flock to our side.

157
00:16:15,130 --> 00:16:17,230
Influence is fickle.

158
00:16:17,230 --> 00:16:21,130
It favors the young.

159
00:16:21,130 --> 00:16:25,130
It is the nature of power.

160
00:16:40,070 --> 00:16:43,700
Do not expect me to cry,
My Liege.

161
00:16:43,700 --> 00:16:46,470
I will not cry...

162
00:16:46,470 --> 00:16:51,500
until I have paid out
those who did this to me...

163
00:16:51,500 --> 00:16:54,130
a thousand times.

164
00:16:54,130 --> 00:16:58,130
I will not...will not...

165
00:16:58,130 --> 00:17:00,270
- Your Majesty.
- Your Majesty.

166
00:17:00,270 --> 00:17:01,170
Your Majesty.

167
00:17:01,170 --> 00:17:02,770
- Your Majesty.
- Your Majesty.

168
00:17:02,770 --> 00:17:05,870
- Your Majesty.
- Wake up, please.

169
00:17:05,870 --> 00:17:06,530
Majesty!

170
00:17:06,530 --> 00:17:09,130
- Your Majesty.
- Your Majesty.

171
00:17:09,130 --> 00:17:13,800
Your Majesty.
Well? Take her inside.

172
00:17:19,630 --> 00:17:23,170
Lady Pak, hear and obey.

173
00:17:28,130 --> 00:17:34,130
Lady Pak is an honorable woman,
an example to the Inner Court.

174
00:17:34,130 --> 00:17:38,700
- Your Majesty.
- She bears the Emperor's child...

175
00:17:38,700 --> 00:17:41,570
...and so the mandate of heaven.

176
00:17:41,830 --> 00:17:46,500
I, the Emperor,
make her a consort of the fifth rank.

177
00:17:46,500 --> 00:17:50,370
Hear and obey.

178
00:17:53,130 --> 00:18:00,130
At your service, My Liege.

179
00:18:19,430 --> 00:18:21,730
Tanasiri?

180
00:18:21,730 --> 00:18:24,130
She fainted.

181
00:18:29,300 --> 00:18:34,130
Lady Pak is an imperial consort.

182
00:18:34,130 --> 00:18:36,800
I'm sorry.

183
00:18:37,630 --> 00:18:42,130
This is all my fault.

184
00:18:42,130 --> 00:18:44,630
Moron!

185
00:18:45,930 --> 00:18:48,800
Your weakness disgusts me.

186
00:18:48,800 --> 00:18:53,170
Have you forgotten
you are the eldest son of our clan?

187
00:18:53,170 --> 00:18:56,830
Never show weakness.

188
00:18:56,830 --> 00:18:59,830
Not even to me.

189
00:18:59,830 --> 00:19:01,130
Understood?

190
00:19:01,130 --> 00:19:03,800
Understood, father.

191
00:19:16,170 --> 00:19:18,900
Why are you here?

192
00:19:19,930 --> 00:19:23,670
General Talahai
sent me with a message.

193
00:19:24,130 --> 00:19:27,130
Report. How goes the war?

194
00:19:27,130 --> 00:19:31,130
I regret to report...

195
00:19:31,130 --> 00:19:33,700
A total loss.

196
00:19:34,070 --> 00:19:37,130
What? Total...?

197
00:19:37,130 --> 00:19:38,900
Talahai alone survived.

198
00:19:38,900 --> 00:19:43,130
Bayan and Wang Yu
were killed.

199
00:19:49,600 --> 00:19:53,130
Why is it so quiet?

200
00:19:53,370 --> 00:19:56,130
Our attack must have failed.

201
00:19:56,130 --> 00:19:59,230
Talahai wouldn't
leave us here otherwise.

202
00:19:59,230 --> 00:20:06,070
Talahai may not
have led the attack last night.

203
00:20:06,070 --> 00:20:07,400
Not Talahai?

204
00:20:07,400 --> 00:20:11,970
Which begs the question
of who it really was.

205
00:20:11,970 --> 00:20:14,170
Well now.

206
00:20:14,470 --> 00:20:17,900
Look at you.

207
00:20:17,900 --> 00:20:23,130
Well, Yom the... Scum.

208
00:20:23,130 --> 00:20:24,470
Long time.

209
00:20:24,470 --> 00:20:30,130
You like my new hairdo?

210
00:20:32,130 --> 00:20:34,770
Wang Yu's 'dead' men.

211
00:20:34,770 --> 00:20:36,430
What did I tell you?

212
00:20:36,430 --> 00:20:39,130
Don't trust them, I said.

213
00:20:39,130 --> 00:20:41,130
Where's Wang Yu?

214
00:20:41,130 --> 00:20:45,870
Why? Does baby miss him?

215
00:20:46,070 --> 00:20:48,200
Open the door. Now.

216
00:20:48,200 --> 00:20:51,300
'Open the gate. Now.'

217
00:20:51,300 --> 00:20:54,030
Have it your way.

218
00:20:54,030 --> 00:20:56,400
Begin!

219
00:21:06,130 --> 00:21:10,330
They mean to kill us.

220
00:21:10,330 --> 00:21:12,170
General...

221
00:21:17,170 --> 00:21:19,130
Where are you taking us?

222
00:21:19,130 --> 00:21:22,130
If we're to be killed...

223
00:21:37,130 --> 00:21:40,130
What's all this then?

224
00:21:40,130 --> 00:21:45,570
My men want you dead.

225
00:21:47,000 --> 00:21:52,070
Proof that those
with whom we served...

226
00:21:52,070 --> 00:21:55,200
are worse than any enemy.

227
00:21:55,200 --> 00:21:59,470
Batolu was after the horses,
not the men.

228
00:22:00,130 --> 00:22:04,570
A fact you well knew.

229
00:22:05,130 --> 00:22:07,600
Did you plan this all along?

230
00:22:07,600 --> 00:22:12,130
You used us as bait.

231
00:22:12,130 --> 00:22:16,670
I merely did the same to you.

232
00:22:16,670 --> 00:22:20,130
This is mutiny. Treason.

233
00:22:20,130 --> 00:22:25,870
Have you already forgotten
who showed me the way?

234
00:22:25,870 --> 00:22:27,800
What?

235
00:22:29,100 --> 00:22:31,200
Survive?

236
00:22:31,200 --> 00:22:33,130
Dream on.

237
00:22:33,130 --> 00:22:36,370
It's impossible
to kill all the Turks.

238
00:22:36,370 --> 00:22:39,170
And for either of us,

239
00:22:39,170 --> 00:22:42,770
that is the
only way out of here.

240
00:22:45,830 --> 00:22:50,130
Our real enemy wasn't Batolu.

241
00:22:50,130 --> 00:22:52,130
It was him.

242
00:22:52,130 --> 00:22:54,730
Enough chatter.

243
00:22:54,730 --> 00:22:56,130
Kill us.

244
00:22:56,130 --> 00:22:58,300
General.

245
00:23:28,000 --> 00:23:30,300
Give them horses.

246
00:23:30,400 --> 00:23:33,130
We ride for the Capitol.

247
00:23:36,130 --> 00:23:39,770
There is no reason to spare us.

248
00:23:39,770 --> 00:23:44,130
Why are you doing it?

249
00:23:47,130 --> 00:23:53,130
In my place, you would kill us.

250
00:23:53,430 --> 00:23:57,770
But the men
of Koryo are not Mongols.

251
00:23:57,770 --> 00:23:59,900
And I am not you.

252
00:23:59,900 --> 00:24:02,330
Satisfied?

253
00:24:03,130 --> 00:24:06,130
And Batolu?

254
00:24:06,130 --> 00:24:08,930
The body.

255
00:24:47,430 --> 00:24:50,130
Why are you helping me?

256
00:24:50,130 --> 00:24:54,870
The only reason
we're fighting is the Mongols.

257
00:24:54,870 --> 00:24:58,830
The enemy
of my enemy is my friend.

258
00:24:58,830 --> 00:25:02,230
Friend, you say?

259
00:25:02,230 --> 00:25:05,200
Yes. Friend.

260
00:25:06,130 --> 00:25:11,130
The present doesn't matter.
The future does.

261
00:25:11,430 --> 00:25:13,570
For us there is no future.

262
00:25:13,570 --> 00:25:15,300
Nor for Koryo.

263
00:25:15,300 --> 00:25:20,570
Since we have no future,
why not build one together?

264
00:25:21,130 --> 00:25:25,130
The Yuan control the seas.

265
00:25:25,130 --> 00:25:28,600
The only thing they
do not control is the Silk Road.

266
00:25:28,600 --> 00:25:32,730
We are in this together.

267
00:25:32,730 --> 00:25:37,670
The Silk Road will give
us breathing space.

268
00:25:39,130 --> 00:25:41,400
I wanted to state my case.

269
00:25:41,400 --> 00:25:44,870
I don't expect an answer.

270
00:25:45,130 --> 00:25:47,130
My name is Yon Feisu.

271
00:25:47,130 --> 00:25:49,400
Mine is Wang Yu.

272
00:25:54,700 --> 00:25:56,600
Wang Yu.

273
00:25:56,600 --> 00:26:00,130
We will meet again soon.

274
00:26:03,430 --> 00:26:08,330
Chunsim, Onnyon,
the imperial kitchen is a disgrace.

275
00:26:08,330 --> 00:26:14,630
Once more
and we'll dock your pay.

276
00:26:16,130 --> 00:26:20,130
Hongdan, you're doing well.

277
00:26:23,970 --> 00:26:26,700
Are you always on the rag?

278
00:26:26,700 --> 00:26:27,130
Sir?

279
00:26:27,130 --> 00:26:31,670
Why else would you
be scowling all the time?

280
00:26:32,170 --> 00:26:38,130
Of all our rules, pleasing those
we serve is the most important.

281
00:26:41,900 --> 00:26:46,130
Nyang, take off your uniform.

282
00:26:47,670 --> 00:26:49,200
And put this on.

283
00:26:49,200 --> 00:26:50,130
This...

284
00:26:50,130 --> 00:26:54,130
You serve in the palace now.

285
00:26:54,130 --> 00:26:56,630
Nyang, congratulations.

286
00:26:56,630 --> 00:27:00,130
I'm so jealous.

287
00:27:00,130 --> 00:27:02,770
Good for you.

288
00:27:02,770 --> 00:27:05,130
Congratulations.

289
00:27:07,530 --> 00:27:10,530
Your new quarters.

290
00:27:10,870 --> 00:27:12,600
Your serve in the palace now.

291
00:27:12,600 --> 00:27:17,130
Your behavior and demeanor
must reflect that fact.

292
00:27:18,430 --> 00:27:22,000
What about Sungnyang?

293
00:27:22,130 --> 00:27:26,130
Sir, your orders.

294
00:27:26,130 --> 00:27:29,370
Never show weakness.

295
00:27:29,370 --> 00:27:32,300
Not even to me.

296
00:27:32,300 --> 00:27:34,870
Understood?

297
00:27:37,030 --> 00:27:38,130
Kill her.

298
00:27:38,130 --> 00:27:40,130
Yes, sir.

299
00:27:42,370 --> 00:27:45,500
For my father.
For my house.

300
00:27:46,770 --> 00:27:50,330
I won't
be sorry to see you dead.

301
00:27:52,830 --> 00:27:58,130
Sire, I can
leave the palace now.

302
00:27:58,400 --> 00:28:04,130
Wherever you are,
I will find you.

303
00:28:05,970 --> 00:28:07,800
Nyang.

304
00:28:07,930 --> 00:28:09,900
Nyang?

305
00:28:14,270 --> 00:28:15,670
Still awake?

306
00:28:15,670 --> 00:28:17,130
What is it?

307
00:28:17,130 --> 00:28:17,930
It's late.

308
00:28:17,930 --> 00:28:20,130
The head eunuch wants you.

309
00:28:20,130 --> 00:28:21,670
What for?

310
00:28:21,670 --> 00:28:23,400
How should I know?

311
00:28:23,400 --> 00:28:27,130
He said hurry, so hurry.

312
00:29:29,070 --> 00:29:30,900
Nyang.

313
00:29:31,730 --> 00:29:33,130
My lord.

314
00:29:33,130 --> 00:29:35,130
Come with me.

315
00:29:35,130 --> 00:29:39,130
The head eunuch wants me.

316
00:29:39,530 --> 00:29:42,170
As does the Emperor.

317
00:30:01,170 --> 00:30:04,130
Did you find Her Majesty's letter?

318
00:30:04,570 --> 00:30:07,230
It was not in her quarters.

319
00:30:11,800 --> 00:30:14,130
Search her.

320
00:30:27,130 --> 00:30:28,930
Give it back.

321
00:30:28,930 --> 00:30:31,370
Anything but that.

322
00:30:38,330 --> 00:30:40,130
My Liege.

323
00:30:40,230 --> 00:30:42,130
No.

324
00:30:42,130 --> 00:30:44,730
My Liege! No!

325
00:30:44,730 --> 00:30:46,970
My Liege!

326
00:30:47,700 --> 00:30:49,830
My Liege!

327
00:31:01,530 --> 00:31:05,100
What? She was
taken to the Emperor?

328
00:31:05,100 --> 00:31:07,730
Kill me, sir.

329
00:31:09,130 --> 00:31:13,130
And your man?
The one you ordered to kill her?

330
00:31:13,130 --> 00:31:17,600
You need not worry, sir.

331
00:31:27,470 --> 00:31:29,770
Wake him.

332
00:31:42,730 --> 00:31:44,630
No...

333
00:31:46,130 --> 00:31:49,130
He killed himself
by swallowing his own tongue.

334
00:31:49,130 --> 00:31:51,270
Killed himself...?

335
00:31:53,630 --> 00:31:56,130
Get him out of here.

336
00:32:05,330 --> 00:32:08,130
I won't have this.

337
00:32:08,500 --> 00:32:11,370
Start Sungnyang
in the imperial palace tomorrow.

338
00:32:11,370 --> 00:32:13,830
My Liege.

339
00:32:14,500 --> 00:32:18,130
I've had enough
of my so-called Empress.

340
00:32:18,130 --> 00:32:21,100
I'm going to her chambers.

341
00:32:25,400 --> 00:32:27,430
Your Majesty.

342
00:32:28,700 --> 00:32:33,830
Your Majesty,
the Emperor is on his way.

343
00:32:38,200 --> 00:32:43,330
Lie down.
You still have a fever.

344
00:32:43,330 --> 00:32:45,130
My mirror.

345
00:32:45,130 --> 00:32:47,130
Mirror?

346
00:32:47,400 --> 00:32:51,730
The doctor said you
should rest till the fever subsides.

347
00:32:51,730 --> 00:32:54,500
The Emperor
can't see me like this.

348
00:32:54,500 --> 00:32:57,130
Make-up and robes.

349
00:32:57,130 --> 00:32:59,130
Go.

350
00:33:02,130 --> 00:33:04,170
My Liege.

351
00:33:04,170 --> 00:33:07,130
How is she?

352
00:33:07,130 --> 00:33:09,900
Waiting inside.

353
00:33:24,100 --> 00:33:26,500
Welcome, My Liege.

354
00:33:28,130 --> 00:33:30,070
Please.

355
00:33:30,070 --> 00:33:33,130
Why did you do it?

356
00:33:33,400 --> 00:33:37,630
It was beneath you as Empress.

357
00:33:37,630 --> 00:33:40,130
So why?

358
00:33:40,300 --> 00:33:45,130
You really need to ask?

359
00:33:45,130 --> 00:33:46,870
I'm asking aren't I?

360
00:33:46,870 --> 00:33:52,130
No. Let me ask you this.

361
00:33:52,130 --> 00:33:55,130
How could you treat me this way?

362
00:33:55,130 --> 00:33:57,130
What way?

363
00:33:57,700 --> 00:34:01,100
I'm always alone.

364
00:34:02,470 --> 00:34:08,630
There hasn't been a day
since I came here...

365
00:34:09,230 --> 00:34:12,270
that I wasn't lonely.

366
00:34:12,770 --> 00:34:18,130
You made me this way.

367
00:34:18,130 --> 00:34:20,470
You see?

368
00:34:25,570 --> 00:34:32,530
So you're still not sorry.

369
00:34:35,130 --> 00:34:37,770
Sorry?

370
00:34:39,030 --> 00:34:47,100
Look, you ran to me,
did you not?

371
00:34:47,100 --> 00:34:54,370
You're here talking to me,
looking in my eyes.

372
00:34:55,130 --> 00:34:58,270
I have prayed for this day.

373
00:34:58,270 --> 00:35:00,600
Why would I be sorry?

374
00:35:00,600 --> 00:35:02,130
Empress!

375
00:35:02,130 --> 00:35:09,630
But this is not
quite what I prayed for.

376
00:35:11,570 --> 00:35:14,930
Without rancor.

377
00:35:15,730 --> 00:35:19,130
A soft look.

378
00:35:22,130 --> 00:35:25,930
A kind word, that's all.

379
00:35:28,670 --> 00:35:36,200
I resent my father
for sending me here.

380
00:35:37,400 --> 00:35:45,130
And I resent you.

381
00:36:41,170 --> 00:36:46,130
You are to attend the Emperor.

382
00:36:46,130 --> 00:36:48,130
Do as you're told.

383
00:36:48,130 --> 00:36:53,000
Always be within
three paces of his presence.

384
00:36:53,800 --> 00:36:56,270
Well, answer.

385
00:36:57,300 --> 00:36:59,770
I understand.

386
00:37:03,130 --> 00:37:07,130
My Liege, Nyang.

387
00:37:13,630 --> 00:37:15,470
Enter.

388
00:37:24,830 --> 00:37:27,130
Come closer.

389
00:37:32,130 --> 00:37:34,130
Closer.

390
00:37:39,130 --> 00:37:41,130
Closer.

391
00:37:46,130 --> 00:37:48,130
There.

392
00:37:50,130 --> 00:37:53,030
I'm going to draw you.

393
00:37:53,030 --> 00:37:59,030
No blinking,
no sneezing, no talking.

394
00:37:59,030 --> 00:38:02,130
- My Liege...
- I said don't move.

395
00:38:10,470 --> 00:38:15,700
Why didn't you find me
when you came here?

396
00:38:18,030 --> 00:38:21,130
Why hide that you're a woman?

397
00:38:21,900 --> 00:38:24,300
Did you hate me so much?

398
00:38:26,330 --> 00:38:33,030
Were you scared
I'd see you as a woman?

399
00:38:37,730 --> 00:38:41,870
That I would be attracted to you?

400
00:38:41,870 --> 00:38:46,700
Send me from the palace.

401
00:38:49,830 --> 00:38:52,730
- My Liege.
- No talking.

402
00:39:00,130 --> 00:39:06,130
The moon over
Egret Island shines all day

403
00:39:06,370 --> 00:39:10,500
To speed travelers on their way

404
00:39:10,500 --> 00:39:16,030
The wind blows,
the ships set sail

405
00:39:16,030 --> 00:39:22,130
I long to share
one last cup of wine

406
00:39:22,670 --> 00:39:27,130
But to no avail

407
00:39:28,400 --> 00:39:31,130
That's sad.

408
00:39:32,130 --> 00:39:36,170
But then,
parting is always sad.

409
00:39:36,170 --> 00:39:42,130
Let me leave the palace.

410
00:39:48,370 --> 00:39:51,970
Again? That again?

411
00:39:51,970 --> 00:39:57,370
I did everything you asked
on Daechong Island.

412
00:39:57,370 --> 00:40:00,670
Please do this for me.

413
00:40:00,670 --> 00:40:04,330
Why are you so eager to leave?

414
00:40:04,330 --> 00:40:07,130
To find my King.

415
00:40:15,130 --> 00:40:22,130
You still serve him, even here?

416
00:40:22,700 --> 00:40:25,130
Listen to me.

417
00:40:25,130 --> 00:40:29,130
You're here now, with me.

418
00:40:29,130 --> 00:40:32,130
He isn't your lord. I am.

419
00:40:32,130 --> 00:40:35,130
My body may be here.

420
00:40:35,130 --> 00:40:38,130
But I have only one lord.

421
00:40:38,130 --> 00:40:41,130
Everything changes in life.

422
00:40:41,130 --> 00:40:44,570
Your circumstances
have changed and so as your lord.

423
00:40:44,570 --> 00:40:52,130
A chick imprints on its mother
when it breaks out of its shell.

424
00:40:52,130 --> 00:40:56,900
It never changes after that.

425
00:41:00,130 --> 00:41:05,500
He is my lord first,
last and always.

426
00:41:05,500 --> 00:41:09,500
Enough! No more.

427
00:41:09,500 --> 00:41:11,130
I don't want to hear any more.

428
00:41:11,130 --> 00:41:14,800
- My Liege.
- That's all for today.

429
00:41:37,100 --> 00:41:40,570
El Temur has you marked.

430
00:41:41,530 --> 00:41:44,900
You're dead if you leave me.

431
00:41:44,900 --> 00:41:48,500
You're safest with me, you idiot.

432
00:41:56,970 --> 00:42:01,130
Your lord is the King of Koryo?

433
00:42:02,130 --> 00:42:04,900
He's dead.

434
00:42:07,000 --> 00:42:08,170
He's what?

435
00:42:08,170 --> 00:42:12,130
Your lord is dead.

436
00:42:12,770 --> 00:42:15,130
That can't be.

437
00:42:15,130 --> 00:42:18,730
He's not
one to die so easily.

438
00:42:18,730 --> 00:42:21,130
Ask Lord Zhang.

439
00:42:21,130 --> 00:42:25,130
We have word
that he was killed in action.

440
00:42:26,030 --> 00:42:31,430
Treat the Emperor like that again
and you'll have to deal with me.

441
00:42:41,130 --> 00:42:43,200
Sire.

442
00:42:44,130 --> 00:42:46,170
Wang Yu is dead?

443
00:42:46,170 --> 00:42:51,700
Not just him.
Bayan and Tal Tal as well.

444
00:42:51,700 --> 00:42:53,130
How did they die?

445
00:42:53,130 --> 00:42:57,130
Lord Zhang
didn't have the details.

446
00:43:01,130 --> 00:43:05,630
Does Sungnyang know
he's dead?

447
00:43:05,630 --> 00:43:07,630
Yes.

448
00:43:11,130 --> 00:43:14,230
She must be heartbroken.

449
00:43:38,130 --> 00:43:43,130
I will live
and come back for you.

450
00:43:43,130 --> 00:43:47,130
Stay alive for me.

451
00:43:47,330 --> 00:43:51,130
That's an order.
Understood?

452
00:43:51,700 --> 00:43:53,770
Sire.

453
00:44:10,030 --> 00:44:12,030
Sire.

454
00:44:13,800 --> 00:44:17,430
You were what gave me hope.

455
00:44:20,130 --> 00:44:23,300
How can I go on?

456
00:44:25,130 --> 00:44:28,170
Why should I?

457
00:44:31,130 --> 00:44:33,300
Sire.

458
00:45:09,970 --> 00:45:12,700
She's barely eating anymore.

459
00:45:12,700 --> 00:45:16,130
Does she miss
her king so much?

460
00:45:16,130 --> 00:45:18,130
Test His Majesty's food.

461
00:45:18,130 --> 00:45:20,000
Wait.

462
00:45:20,100 --> 00:45:25,470
Nyang will do it from now on.

463
00:45:27,130 --> 00:45:29,670
My Liege, custom dictates...

464
00:45:29,670 --> 00:45:32,130
Custom or not...

465
00:45:32,130 --> 00:45:35,130
that's how it is.

466
00:45:48,030 --> 00:45:50,970
I'm starving.

467
00:45:52,300 --> 00:45:55,130
Go on then.

468
00:45:57,970 --> 00:46:00,800
Well? Go on.

469
00:46:16,130 --> 00:46:18,130
Eat more than that.

470
00:46:18,130 --> 00:46:22,130
How else can you tell
if it's poisoned?

471
00:46:32,100 --> 00:46:34,130
And this.

472
00:46:34,430 --> 00:46:37,130
Test this too.

473
00:46:51,300 --> 00:46:55,430
More than that, go on.

474
00:46:58,130 --> 00:47:00,130
Well?

475
00:47:00,370 --> 00:47:02,900
It's fine.

476
00:47:03,130 --> 00:47:05,570
Enjoy.

477
00:47:12,130 --> 00:47:14,570
Here, my favorite.

478
00:47:14,570 --> 00:47:17,630
Test to see that it's all right.

479
00:47:18,130 --> 00:47:20,200
Go on.

480
00:47:27,130 --> 00:47:29,300
I'm paranoid.

481
00:47:29,300 --> 00:47:34,130
You have to test
everything for poison.

482
00:47:36,430 --> 00:47:40,630
I swear, take a real bite.

483
00:47:46,130 --> 00:47:50,700
Wake up.
You can't follow a dead man.

484
00:47:57,130 --> 00:47:59,030
Don't worry about me.

485
00:47:59,030 --> 00:48:01,130
You are my servant.

486
00:48:02,130 --> 00:48:05,330
You need my permission to die.

487
00:48:28,800 --> 00:48:32,130
I told you,
stay within three paces.

488
00:48:33,130 --> 00:48:35,130
Did you forget?

489
00:49:31,330 --> 00:49:34,200
Sungnyang.

490
00:49:34,670 --> 00:49:37,500
Nyang.

491
00:49:37,500 --> 00:49:39,130
Nyang... EEP!

492
00:49:39,130 --> 00:49:43,130
You scared me.

493
00:49:46,270 --> 00:49:51,130
Was that a smile?

494
00:50:01,500 --> 00:50:03,770
It's cold out.

495
00:50:03,900 --> 00:50:06,130
Come along.

496
00:50:23,770 --> 00:50:28,130
Daidu is just over that rise.

497
00:50:29,430 --> 00:50:31,130
It will be dark soon.

498
00:50:31,130 --> 00:50:34,130
We should make camp.

499
00:50:34,130 --> 00:50:38,970
I agree.
At least to rest the horses.

500
00:50:39,230 --> 00:50:42,130
You camp here.

501
00:50:42,130 --> 00:50:43,230
We'll go on.

502
00:50:43,230 --> 00:50:46,400
You haven't slept for two days.

503
00:50:46,400 --> 00:50:49,200
No one is chasing us.

504
00:50:53,530 --> 00:50:57,530
What's in Daidu anyway?
What's his rush?

505
00:50:57,530 --> 00:51:00,030
It's Sungnyang.

506
00:51:00,030 --> 00:51:01,900
Sungnyang?

507
00:51:02,030 --> 00:51:04,170
He's in the Capitol?

508
00:51:04,170 --> 00:51:07,400
Not he. Her.

509
00:51:07,400 --> 00:51:09,200
A girl.

510
00:51:09,930 --> 00:51:12,130
A girl?

511
00:51:26,130 --> 00:51:30,870
It's late.
We'll have to wait till morning.

512
00:51:30,870 --> 00:51:32,330
Wait a moment.

513
00:51:32,330 --> 00:51:36,370
Jombak is finding us
suitable quarters.

514
00:51:37,130 --> 00:51:39,130
It's him.

515
00:51:41,130 --> 00:51:41,930
Sire.

516
00:51:41,930 --> 00:51:44,000
Did you find something?

517
00:51:44,000 --> 00:51:46,300
There's a
courtesan house, but...

518
00:51:46,300 --> 00:51:48,630
But what?

519
00:51:48,630 --> 00:51:53,130
Wang Ko is staying there.

520
00:51:53,130 --> 00:51:54,570
Wang Ko?

521
00:51:59,930 --> 00:52:03,130
Scum!

522
00:52:04,530 --> 00:52:07,130
How dare you?

523
00:52:07,130 --> 00:52:09,130
Sire, please.

524
00:52:09,130 --> 00:52:13,170
They dare reject me?

525
00:52:13,170 --> 00:52:20,130
Even courtesans
are looking down on me!

526
00:52:20,130 --> 00:52:22,130
You've had enough.

527
00:52:22,130 --> 00:52:24,070
Silence!

528
00:52:24,070 --> 00:52:25,800
I don't need you.

529
00:52:25,800 --> 00:52:30,130
Leave, I don't care!

530
00:52:31,870 --> 00:52:33,770
Sire.

531
00:52:44,770 --> 00:52:50,470
So they're watching me, eh?

532
00:52:51,500 --> 00:52:53,600
Follow them.

533
00:52:58,130 --> 00:53:00,130
Sire.

534
00:53:02,130 --> 00:53:05,130
S-Sire.

535
00:53:05,130 --> 00:53:07,130
Wang Yu.

536
00:53:07,130 --> 00:53:10,130
Wang Yu is alive.

537
00:53:19,900 --> 00:53:22,200
If you're a ghost, go away.

538
00:53:22,200 --> 00:53:26,570
If you're Wang Yu, join me.

539
00:53:26,570 --> 00:53:29,270
If you're a drunk, go away.

540
00:53:29,270 --> 00:53:35,970
If you're Wang Ko, join me.

541
00:53:40,770 --> 00:53:42,930
What did I tell you?

542
00:53:42,930 --> 00:53:47,670
I said he's not easy to kill.

543
00:54:03,330 --> 00:54:09,130
You stole salt,
swore allegiance to El Temur...

544
00:54:09,130 --> 00:54:11,130
and this is what it got you.

545
00:54:11,130 --> 00:54:13,130
Look here.

546
00:54:13,130 --> 00:54:17,130
You lecturing your uncle now?

547
00:54:17,370 --> 00:54:21,130
The palace is scarier
than any battlefield.

548
00:54:21,130 --> 00:54:24,430
Doze off
and you'll lose your neck.

549
00:54:24,430 --> 00:54:28,130
You made it back.
Be on your guard.

550
00:54:28,130 --> 00:54:32,000
If it means bowing
and scraping like you...

551
00:54:32,570 --> 00:54:35,300
I wouldn't have bothered
coming back.

552
00:54:36,130 --> 00:54:39,070
Good luck with that.

553
00:54:43,400 --> 00:54:45,970
Nyang.

554
00:54:51,470 --> 00:54:52,370
Nyang?

555
00:54:52,370 --> 00:54:57,130
Nyang has taken ill.

556
00:55:00,270 --> 00:55:02,130
Nyang!

557
00:55:03,600 --> 00:55:07,130
Nyang... Nyang...

558
00:55:08,070 --> 00:55:09,830
She's burning up.

559
00:55:09,830 --> 00:55:11,130
The imperial physician!

560
00:55:11,130 --> 00:55:14,130
My Liege, for a servant?

561
00:55:14,130 --> 00:55:16,200
Get him now!

562
00:55:16,200 --> 00:55:18,130
My Liege.

563
00:55:18,470 --> 00:55:20,370
Nyang.

564
00:55:39,130 --> 00:55:41,100
Sungnyang.

565
00:56:16,800 --> 00:56:20,370
Sire... Sire!

566
00:56:21,900 --> 00:56:24,130
Sire!

567
00:56:26,130 --> 00:56:29,130
Stay with me.

568
00:56:31,130 --> 00:56:33,130
Sire...

569
00:56:34,130 --> 00:56:36,970
Sire!

570
00:56:44,130 --> 00:56:46,430
S-Sungnyang.

571
00:56:47,230 --> 00:56:51,130
Sungnyang,
that's right, I'm here.

572
00:56:53,700 --> 00:56:55,330
My Liege.

573
00:57:00,970 --> 00:57:03,130
I've been
looking all over for you.

574
00:57:03,130 --> 00:57:04,870
What is it now?

575
00:57:04,870 --> 00:57:07,930
General Bayan is alive.

576
00:57:09,130 --> 00:57:11,130
He's here?

577
00:57:11,130 --> 00:57:12,770
He's not dead?

578
00:57:12,770 --> 00:57:14,300
No, My Liege.

579
00:57:14,300 --> 00:57:18,130
He has returned victorious.

580
00:57:18,500 --> 00:57:20,270
And...

581
00:57:21,230 --> 00:57:23,300
Wang Yu?

582
00:57:28,130 --> 00:57:30,400
What about him?

583
00:57:30,400 --> 00:57:33,500
He rode back with him.

584
00:57:37,130 --> 00:57:41,900
He survived?

585
00:57:42,970 --> 00:57:45,130
Sire...

586
00:57:50,170 --> 00:57:52,170
Sire...

587
00:57:58,130 --> 00:58:00,130
Sire.

588
00:58:01,900 --> 00:58:19,130
Subtitles provided by MBC

589
00:58:19,130 --> 00:58:20,270
You've seen Sungnyang?

590
00:58:20,270 --> 00:58:24,130
Not another word.
Unless you're reading.

591
00:58:24,130 --> 00:58:27,830
The late Emperor
wrote a petition in blood?

592
00:58:28,130 --> 00:58:31,000
You thought it was suicide?

593
00:58:31,000 --> 00:58:33,770
One little sip,
drift off to sleep...

594
00:58:33,770 --> 00:58:37,130
and wake in a better world.

595
00:58:37,570 --> 00:58:42,100
I will do what I want,
how I want.

596
00:58:43,270 --> 00:58:44,130
My Liege.

597
00:58:44,130 --> 00:58:46,130
Would you act this way
if I were Wang Yu?

598
00:58:46,130 --> 00:58:50,130
His Majesty
will be waiting near the pavilion.

599
00:58:50,130 --> 00:58:55,130
Sungnyang,
whats keeping you?

