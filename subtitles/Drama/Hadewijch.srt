1
00:00:20,372 --> 00:00:23,667
Hadewijch

2
00:05:45,190 --> 00:05:50,180
Then I heard a terrible voice
speaking to me like a ghost

3
00:05:50,208 --> 00:05:53,165
"Behold who I am!"

4
00:05:53,479 --> 00:05:56,490
And I saw him whom I sought.

5
00:05:56,591 --> 00:06:00,274
His Countenance revealed itself
with such clarity

6
00:06:00,380 --> 00:06:06,857
that I recognized in it all the countenances
and all the forms that ever existed

7
00:06:06,858 --> 00:06:11,265
wherefrom he received honor and service in all right

8
00:06:11,689 --> 00:06:18,482
I saw why each one must receive his part
in damnation and in blessing

9
00:06:19,064 --> 00:06:23,059
I saw how each one must
be set in his place

10
00:06:23,206 --> 00:06:26,840
why some wander away from him
and return to him again

11
00:06:27,064 --> 00:06:30,586
and why others wander and never return

12
00:06:30,706 --> 00:06:38,842
and how some remained standing entirely still

13
00:06:38,915 --> 00:06:42,331
and almost devoid of consolation at all times.

14
00:06:42,960 --> 00:06:46,664
And others have remained in their
place since childhood

15
00:06:46,697 --> 00:06:51,821
have known themselves at their worth,
and have held out to the end.

16
00:06:52,521 --> 00:06:56,960
I recognized all these beings
there in that Countenance.

17
00:06:58,896 --> 00:07:02,340
In his right hand I saw
the gifts of his blessing

18
00:07:02,560 --> 00:07:08,466
and I saw in this hand Heaven opened,
and all those who will be with him eternally.

19
00:07:09,399 --> 00:07:16,309
In his left hand I saw the sword of the fearful stroke
with which he strikes all down to death.

20
00:07:16,947 --> 00:07:22,859
In this hand I saw Hell
and all its eternal company.

21
00:07:59,095 --> 00:08:01,345
I have to speak to you for a moment

22
00:08:02,463 --> 00:08:03,284
Come.

23
00:08:13,260 --> 00:08:14,577
You no longer eat?

24
00:08:20,571 --> 00:08:22,897
I am abstaining, Mother.

25
00:08:24,394 --> 00:08:26,777
Abstinence, but not martyrdom.

26
00:08:33,437 --> 00:08:37,604
Here, eat this.  Go on.

27
00:10:43,070 --> 00:10:49,363
I noticed a few weeks ago and I tried to
moderate her excessive behavior, but

28
00:10:49,577 --> 00:10:51,930
you've seen, she deprives herself of food

29
00:10:51,931 --> 00:10:53,941
and protection from the cold

30
00:10:54,722 --> 00:10:56,919
is she keeps this up she is going to die.

31
00:11:00,716 --> 00:11:03,654
She is strongly bound to
this behavior

32
00:11:05,627 --> 00:11:11,008
and I think it is a certain,
manifestation of His love

33
00:11:16,847 --> 00:11:19,646
It is necessary, for her sake

34
00:11:20,601 --> 00:11:23,589
to rediscover the outside world

35
00:11:23,820 --> 00:11:30,008
the world will give her more opportunities
than the monastery, to prove her love to God.

36
00:11:33,815 --> 00:11:36,265
Opportunities not of her choosing.

37
00:11:38,579 --> 00:11:40,773
Events

38
00:11:45,270 --> 00:11:47,974
It isn't necessary

39
00:11:48,615 --> 00:11:51,145
to be detached from the world.

40
00:11:52,331 --> 00:11:54,758
in order to be with God.

41
00:12:27,837 --> 00:12:29,270
Go get her.

42
00:12:53,729 --> 00:12:55,474
- Hello.
- Hi.

43
00:13:08,470 --> 00:13:10,103
You don't obey.

44
00:13:12,619 --> 00:13:14,640
you reject the rules,

45
00:13:18,325 --> 00:13:21,511
but the rules protect us

46
00:13:23,026 --> 00:13:24,634
and guide us.

47
00:13:26,071 --> 00:13:27,964
You have no humility.

48
00:13:29,369 --> 00:13:32,907
For the moment, for me,
you are a caricature of religion.

49
00:13:33,651 --> 00:13:35,187
God is not there.

50
00:13:40,155 --> 00:13:42,420
It is out of the question right now

51
00:13:42,668 --> 00:13:45,968
for you to speak your vows,
and for you to stay here.

52
00:13:50,563 --> 00:13:52,412
Return to the world.

53
00:13:54,399 --> 00:13:55,573
Who knows?

54
00:13:58,222 --> 00:14:00,746
in the practice of everyday life

55
00:14:03,489 --> 00:14:05,373
your real life

56
00:14:07,834 --> 00:14:13,439
your true desires, your true self
could perhaps be revealed.

57
00:14:15,960 --> 00:14:20,412
Go on, the convent doors will
never be closed to you.

58
00:17:13,114 --> 00:17:15,392
Winter has come

59
00:17:15,767 --> 00:17:17,381
the unknown mother

60
00:17:17,382 --> 00:17:20,721
pushes me even more the sadness of her face

61
00:17:22,193 --> 00:17:25,794
that you are well loved, and close to us

62
00:17:26,465 --> 00:17:29,636
there is no reason for your sadness.

63
00:17:31,111 --> 00:17:34,527
Doing well, I told you it is complete

64
00:17:34,684 --> 00:17:38,728
We are always here

65
00:17:38,929 --> 00:17:42,421
also the richness of your word.

66
00:17:42,622 --> 00:17:45,766
it is perfect, and perhaps
you have it yourself.

67
00:19:33,925 --> 00:19:35,916
Ah you're back?

68
00:19:36,117 --> 00:19:37,583
How's it going?

69
00:19:39,715 --> 00:19:40,587
What's up?

70
00:19:42,027 --> 00:19:44,095
- How much?
- Three months at least, I don't know...

71
00:19:44,196 --> 00:19:45,744
what about probation?

72
00:19:45,479 --> 00:19:47,804
- Papa has signed...
- Bitch!

73
00:19:47,806 --> 00:19:49,900
Yeah

74
00:20:22,812 --> 00:20:23,607
Not there

75
00:20:23,608 --> 00:20:25,373
listen, have a great day

76
00:20:25,374 --> 00:20:26,900
Thanks, Dad.

77
00:20:46,413 --> 00:20:47,184
Doggie

78
00:20:51,826 --> 00:20:52,953
Doggie

79
00:20:54,166 --> 00:20:54,962
Doggie

80
00:20:54,997 --> 00:20:56,261
Come here, dog!

81
00:20:56,531 --> 00:20:57,093
Come

82
00:20:58,266 --> 00:20:59,383
Good. Come!

83
00:20:59,819 --> 00:21:01,349
Do you want some?

84
00:21:03,018 --> 00:21:04,252
Such a good dog..

85
00:21:04,353 --> 00:21:06,473
Drink some water.
Yes.

86
00:21:09,629 --> 00:21:10,812
Ok dog.

87
00:21:11,053 --> 00:21:12,288
Stay there.

88
00:21:39,633 --> 00:21:40,431
Everything alright?

89
00:21:41,097 --> 00:21:42,463
Just well, my darling.

90
00:21:51,732 --> 00:21:53,412
- Thank you Miss.
- Oh no thanks.

91
00:21:53,852 --> 00:21:54,504
Thank you very much.

92
00:21:55,450 --> 00:21:56,271
Hello

93
00:21:58,245 --> 00:21:59,784
- Are you alone?
- Yes.

94
00:21:59,819 --> 00:22:01,620
Then I invite you to have a drink with us

95
00:22:02,657 --> 00:22:04,282
Yeah

96
00:22:04,697 --> 00:22:07,054
We are nice.
Ah, how kind, thanks.

97
00:22:07,958 --> 00:22:09,780
- This is Yassine.
- A pleasure.

98
00:22:09,883 --> 00:22:12,769
This is Mahmoud, and
I am Mahmet, And you?

99
00:22:12,791 --> 00:22:13,497
C�line.

100
00:22:13,506 --> 00:22:14,645
C�line, ah okay.

101
00:22:14,680 --> 00:22:16,156
- Are you from Paris?
- Yes.

102
00:22:16,839 --> 00:22:18,299
You are cute, it's normal.

103
00:22:19,580 --> 00:22:21,139
Apart from that,
what do you do here?

104
00:22:21,351 --> 00:22:22,999
- I'm a student.
- Cool.

105
00:22:23,225 --> 00:22:25,433
- What are you studying?
- Theology.

106
00:22:25,434 --> 00:22:26,734
That's Religion right?

107
00:22:26,816 --> 00:22:28,565
- Yes
- Oh, okay.

108
00:22:28,695 --> 00:22:31,002
- So you're a believer then?
- Yes.

109
00:22:31,337 --> 00:22:32,573
But no cross?

110
00:22:33,036 --> 00:22:34,706
- Pardon.
- But no cross?

111
00:22:34,870 --> 00:22:35,434
No.

112
00:22:35,435 --> 00:22:36,935
What the heck are you talking about.

113
00:22:37,071 --> 00:22:39,478
Ah, I don't know.
It's all good.

114
00:22:40,371 --> 00:22:42,028
She has impressed you

115
00:22:44,840 --> 00:22:47,641
It doesn't worry you, three guys...?

116
00:22:47,705 --> 00:22:49,260
Anyway, don't worry, we are nice.

117
00:22:49,330 --> 00:22:50,467
Yes, yes.

118
00:22:51,804 --> 00:22:53,236
...

119
00:22:54,522 --> 00:22:56,527
In fact is at the bottom of religion.

120
00:22:57,209 --> 00:22:57,888
Ah yes.

121
00:22:58,298 --> 00:22:59,746
You don't talk much, eh?

122
00:22:59,872 --> 00:23:04,100
- No, I just didn't respond.. In fact
- Is it too personal? You don't want to talk about it.

123
00:23:04,135 --> 00:23:05,574
No, not at all.

124
00:23:07,239 --> 00:23:09,399
- You are faithful and all that?
- Yes

125
00:23:09,634 --> 00:23:13,188
- Yes, in life you have to believe in things, have faith.
- Yes

126
00:23:14,066 --> 00:23:17,448
And other than that, 
you have fun, listen to music?

127
00:23:17,864 --> 00:23:20,129
- Yes
- It makes you happy right?

128
00:23:20,130 --> 00:23:20,812
- Yes.
- That's cool.

129
00:23:21,557 --> 00:23:23,455
We're on vacation now?

130
00:23:23,586 --> 00:23:25,763
Ah, yes yes.

131
00:23:26,626 --> 00:23:27,487
Okay.

132
00:23:27,841 --> 00:23:28,978
Not in every case.

133
00:23:28,979 --> 00:23:32,563
If you are interested, there's a concert there.
By the port there.

134
00:23:32,598 --> 00:23:34,345
- Yeah.
- Tonight if you want.

135
00:23:34,370 --> 00:23:37,768
It would be a pleasure if you would join us.
Saturday.

136
00:23:37,773 --> 00:23:39,987
If you want, sure.

137
00:23:40,506 --> 00:23:44,501
How cool, she doesn't know you and
she says yes right away.

138
00:23:44,509 --> 00:23:45,967
- Frankly she's cool.
- Shes cool, yeah.

139
00:23:46,068 --> 00:23:47,777
- That's rare in Paris.
- Oh yeah?


140
00:23:47,778 --> 00:23:48,591
- Yes.

141
00:23:48,654 --> 00:23:50,302
No cooking or cleaning

142
00:23:50,303 --> 00:23:53,068
Do you want to go somewhere else
because we know a place a little...

143
00:23:53,421 --> 00:23:54,928
a little nicer than here.

144
00:23:55,029 --> 00:23:57,944
Would you like? 
We can take a little walk over there.

145
00:23:58,260 --> 00:23:59,346
Yeah, if you want.

146
00:23:59,547 --> 00:24:01,166
- Friendly - Cool.
- Alright.

147
00:24:01,267 --> 00:24:02,130
Let's go.

148
00:24:06,705 --> 00:24:07,271
Good bye.

149
00:24:07,306 --> 00:24:07,726
Good bye.

150
00:29:02,692 --> 00:29:04,656
God be with you my love.

151
00:29:33,776 --> 00:29:35,844
What are you doing today?

152
00:29:36,720 --> 00:29:37,858
Praying.

153
00:29:42,075 --> 00:29:43,910
And your exams?

154
00:29:44,432 --> 00:29:47,042
Waiting for the results.

155
00:29:53,060 --> 00:29:54,839
Will you have lunch with us on Saturday?

156
00:31:36,735 --> 00:31:40,402
Imbued with love.
I salute you, love.

157
00:31:42,466 --> 00:31:44,906
I am impertinent and a fool

158
00:31:45,507 --> 00:31:50,306
Your power behind me,
the risk of succumbing to death.

159
00:35:57,608 --> 00:35:58,470
Hello

160
00:35:59,933 --> 00:36:01,755
Good morning, Who is it?

161
00:36:03,480 --> 00:36:04,588
Ah, yes, yes.

162
00:36:05,509 --> 00:36:06,445
How are you?

163
00:36:06,480 --> 00:36:07,689
Good, and you?

164
00:36:10,937 --> 00:36:13,585
Alright? Yes.

165
00:36:16,158 --> 00:36:19,141
If you want you can come eat at my house.

166
00:36:20,689 --> 00:36:22,864
If you want you can come eat at my house.

167
00:36:24,204 --> 00:36:25,278
Yes.

168
00:36:26,049 --> 00:36:30,807
I live on 17 St. Louis Island

169
00:36:31,009 --> 00:36:32,410
Do you know where that is?

170
00:36:35,152 --> 00:36:36,955
Okay, OK.

171
00:36:40,483 --> 00:36:41,504
See you later.

172
00:36:49,969 --> 00:36:51,944
You could do that one day.

173
00:36:53,009 --> 00:36:58,418
Then another day we get called for something else.
I'm a technocrat.

174
00:36:59,061 --> 00:37:00,618
Where do you live Yassine?

175
00:37:00,619 --> 00:37:02,479
I live in the suburbs.

176
00:37:10,198 --> 00:37:11,506
What is it?

177
00:37:11,807 --> 00:37:12,607
Nothing.

178
00:37:31,101 --> 00:37:34,700
Tell me, Yasin.
Do you have a job right now?

179
00:37:35,078 --> 00:37:36,089
No

180
00:37:36,430 --> 00:37:37,454
I haven't.

181
00:37:37,991 --> 00:37:41,052
Do you have some professional training perhaps?

182
00:37:41,053 --> 00:37:41,609
No.

183
00:37:42,243 --> 00:37:43,145
Ah, ok.

184
00:37:43,940 --> 00:37:45,448
Perhaps you live with someone, maybe

185
00:37:45,487 --> 00:37:47,964
whom you could recommend?

186
00:37:48,099 --> 00:37:50,146
In fact yes, I have my brother.

187
00:37:50,381 --> 00:37:53,699
- Ah, that's good.
- Dad, there's no need for this talk.

188
00:37:53,773 --> 00:37:55,390
- Ah listen
- Ah, OK.

189
00:37:55,391 --> 00:37:57,229
I'm just asking your friend some questions.

190
00:37:59,581 --> 00:38:01,483
You told me that you would drop me off.

191
00:38:01,954 --> 00:38:02,659
Excuse me.

192
00:38:13,990 --> 00:38:15,401
Well there it is.

193
00:38:17,288 --> 00:38:18,001
No big deal.

194
00:38:23,009 --> 00:38:23,813
Wait.

195
00:38:34,214 --> 00:38:35,354
The light is green, isn't it?

196
00:38:35,355 --> 00:38:37,942
Go on, get fucked! 
Pay attention you assholes!

197
00:38:41,484 --> 00:38:43,067
Crazy people.

198
00:38:45,371 --> 00:38:46,322
It's...

199
00:38:46,651 --> 00:38:48,539
Looking for shit or what?

200
00:38:50,240 --> 00:38:52,448
Frankly, it was magnificent
there at your house.

201
00:38:52,637 --> 00:38:54,430
Yes, I ate really well.

202
00:38:59,311 --> 00:39:01,432
Wait.
Give me a second.

203
00:39:02,383 --> 00:39:06,259
- You've seen where I come from.
- Frankly its royal, you see.

204
00:39:06,392 --> 00:39:07,651
Ah, yeah

205
00:39:08,854 --> 00:39:10,836
And above all your father was laughing

206
00:39:10,868 --> 00:39:12,805
thats pretty crazy.

207
00:39:12,806 --> 00:39:15,062
- That's good.
- He's a jerk.

208
00:39:15,583 --> 00:39:18,859
What do you mean he's your father right?

209
00:39:19,188 --> 00:39:21,236
That's what father's do.

210
00:39:23,627 --> 00:39:25,496
Why do you say such things?

211
00:39:41,092 --> 00:39:42,404
What's wrong?

212
00:39:45,095 --> 00:39:47,087
- Hey?
- It's nothing.

213
00:39:47,623 --> 00:39:48,884
Sorry, what are you looking for.

214
00:39:48,885 --> 00:39:51,968
No it's nothing, really

215
00:39:52,003 --> 00:39:54,872
- No no it was stupid on my part.

216
00:39:55,370 --> 00:39:57,658
It's nothing, trust me. It's nothing. See?

217
00:39:57,759 --> 00:40:00,278
Quiet.  See no problem.

218
00:40:00,379 --> 00:40:02,123
... No problem.

219
00:40:08,826 --> 00:40:10,978
A spell of love or what?

220
00:40:29,101 --> 00:40:30,176
What are you looking at?

221
00:40:30,267 --> 00:40:31,696
You see how he was looking at me?

222
00:40:33,598 --> 00:40:34,502
Yassine!

223
00:40:40,221 --> 00:40:42,578
Didn't you see the look he gave?

224
00:40:44,016 --> 00:40:44,834
What are you doing?

225
00:40:49,405 --> 00:40:50,975
Come on sweetie.

226
00:41:30,893 --> 00:41:32,371
He's there.

227
00:42:22,076 --> 00:42:23,737
Come, let's get a coffee.

228
00:42:40,992 --> 00:42:42,695
I did it...

229
00:42:42,730 --> 00:42:47,151
Because that guy looked at me funny, you know?

230
00:42:47,158 --> 00:42:49,961
As I am an Arab.  I got carried away.

231
00:42:55,334 --> 00:42:57,438
I understand, why you did that and everything.

232
00:42:57,495 --> 00:42:58,757
Sorry.  Frankly that..

233
00:42:59,491 --> 00:43:03,497
No, truly, I understand.
I agree with you.

234
00:43:04,823 --> 00:43:06,974
Well, do you...

235
00:43:09,159 --> 00:43:11,212
do you have a boyfriend?

236
00:43:11,222 --> 00:43:12,794
I don't have a boyfriend.

237
00:43:13,522 --> 00:43:14,933
I...

238
00:43:16,453 --> 00:43:17,875
I love...

239
00:43:17,910 --> 00:43:18,235
I love Christ.

240
00:43:18,664 --> 00:43:20,027
I love Christ.

241
00:43:20,028 --> 00:43:24,208
But we all have a
religion that we love.

242
00:43:24,209 --> 00:43:25,710
Yes but,

243
00:43:25,711 --> 00:43:28,310
I don't want to have a boyfriend, because...

244
00:43:28,311 --> 00:43:29,767
Why?

245
00:43:30,429 --> 00:43:32,515
I am here for Him.

246
00:43:32,616 --> 00:43:34,525
But we are all here for...

247
00:43:35,300 --> 00:43:36,868
for a cause.

248
00:43:39,384 --> 00:43:40,531
Yes, but...

249
00:43:43,081 --> 00:43:47,987
I am a virgin, and I want
to stay a virgin.

250
00:43:47,988 --> 00:43:51,886
I don't want to have sex with a guy
and everything.

251
00:43:52,825 --> 00:43:56,004
Why did I say that?

252
00:43:57,465 --> 00:44:01,911
It's not serious.  
I appreciate that you told me.

253
00:44:02,012 --> 00:44:03,037
No, but it's just that I can't...

254
00:44:06,779 --> 00:44:09,383
Frankly i'm impressed by what you are.

255
00:44:10,939 --> 00:44:12,556
But I really like you.

256
00:44:12,607 --> 00:44:14,092
I like you too.

257
00:44:14,860 --> 00:44:19,513
and also, I'm impressed by what you are
and what you've told me.

258
00:44:21,604 --> 00:44:23,078
Please don't get the wrong...

259
00:44:24,063 --> 00:44:27,193
... at the concert there, I got carried away.

260
00:44:28,074 --> 00:44:30,992
- I hope you don't mind.
- No, it's not serious.

261
00:44:31,463 --> 00:44:33,214
Don't worry about it.

262
00:44:37,771 --> 00:44:40,327
- I can't stay here
- Oh really?

263
00:44:40,328 --> 00:44:42,649
- You can't stay, are you sure?
- No, I can't.

264
00:44:42,650 --> 00:44:44,526
You know what,

265
00:44:44,527 --> 00:44:47,272
if you feel like coming over to my brother's house.

266
00:44:47,307 --> 00:44:50,620
- Yes, if you want.  Send me a text
- Okay

267
00:44:51,070 --> 00:44:54,221
- Thank you
- Later.

268
00:44:55,951 --> 00:44:57,528
Is that your new friend, Yassine?

269
00:44:57,685 --> 00:44:58,408
Ah, yes...

270
00:44:58,601 --> 00:45:00,090
She's cute.

271
00:45:00,091 --> 00:45:01,608
Good luck.

272
00:45:01,643 --> 00:45:02,505
Thanks.

273
00:45:18,889 --> 00:45:20,162
What are you doing?

274
00:45:21,211 --> 00:45:21,986
Nothing.

275
00:45:26,404 --> 00:45:27,579
Seen your mother?

276
00:45:28,560 --> 00:45:29,506
No.

277
00:45:32,777 --> 00:45:36,181
Ok, I have to go, see you tomorrow.

278
00:47:12,939 --> 00:47:13,579
Hello.

279
00:47:14,775 --> 00:47:16,112
Do you know Yassine?

280
00:47:16,174 --> 00:47:17,620
Yassine? Yassine who?

281
00:47:17,621 --> 00:47:18,978
Yassine Shire

282
00:47:19,174 --> 00:47:20,683
- Yassine Shire?
- He lives here.

283
00:47:20,684 --> 00:47:22,489
- Ah, yeah, he's not here.
- He's not here.

284
00:47:22,690 --> 00:47:24,762
- Do you where I can find him?
- At the mosque.

285
00:47:26,435 --> 00:47:28,539
Take the stairs and turn left.

286
00:47:28,640 --> 00:47:29,803
Thank you.

287
00:47:30,004 --> 00:47:30,804
Come on.

288
00:47:33,042 --> 00:47:35,273
Excuse me, is that your dog?

289
00:47:35,918 --> 00:47:37,697
He's not bad.

290
00:47:37,840 --> 00:47:38,849
No.

291
00:47:40,619 --> 00:47:41,401
Come.

292
00:49:03,448 --> 00:49:05,076
- Yassine.
- What are you doing here?

293
00:49:05,077 --> 00:49:08,463
- They told me you were here in the mosque.
- Don't come here.

294
00:49:08,577 --> 00:49:09,724
Come on, let's go.

295
00:49:10,038 --> 00:49:11,448
Don't lie.

296
00:49:11,601 --> 00:49:14,727
This is the mosque not my home.

297
00:49:14,863 --> 00:49:17,470
Yes, I live here
But it's not my place.

298
00:49:17,505 --> 00:49:18,489
Peace be upon you.

299
00:49:18,691 --> 00:49:19,995
Come on let's go.

300
00:49:21,343 --> 00:49:22,292
Come in.

301
00:49:25,737 --> 00:49:27,884
I want you to meet a friend.

302
00:49:27,919 --> 00:49:28,878
Hello

303
00:49:29,246 --> 00:49:30,204
Hi.

304
00:49:31,374 --> 00:49:33,535
- Do you drink tea?
- Yes.

305
00:49:33,570 --> 00:49:35,064
- Bring two glasses.
- Okay.

306
00:49:49,014 --> 00:49:51,373
- Please sit down.

307
00:49:55,648 --> 00:49:56,673
Aren't you going to serve.

308
00:50:01,697 --> 00:50:03,233
Pour me a little bit too.

309
00:50:12,819 --> 00:50:16,074
- So, is this the first time you've come here?
- Yes, yes, yes.

310
00:50:16,306 --> 00:50:18,392
- Do you like it?
- Yes.

311
00:50:18,816 --> 00:50:19,977
- It's pretty.

312
00:50:20,551 --> 00:50:22,556
- Have you seen our view?
- Yes, it's cool.

313
00:50:24,619 --> 00:50:25,572
- We can see Paris.

314
00:50:29,029 --> 00:50:30,455
- How do you find it?

315
00:50:30,914 --> 00:50:35,865
- Well, its different, but...
- Different? Than what?

316
00:50:36,073 --> 00:50:37,080
Then...

317
00:50:37,254 --> 00:50:39,297
...what I know.

318
00:50:40,295 --> 00:50:41,395
You are from Paris?

319
00:50:41,401 --> 00:50:43,514
Yes.

320
00:50:43,429 --> 00:50:45,959
Yassine told me that you
study theology?

321
00:50:45,994 --> 00:50:46,669
Yes.

322
00:50:46,704 --> 00:50:47,956
I am a Christian.

323
00:50:47,991 --> 00:50:51,316
Christian?
Do you know about Islam, a bit?

324
00:50:52,411 --> 00:50:53,720
No really.

325
00:50:53,755 --> 00:50:55,012
Not really?

326
00:50:56,487 --> 00:51:02,065
Well if you are interested, I teach a class 
on religious reflection. Yes.

327
00:51:02,903 --> 00:51:04,947
Tomorrow.
Tomorrow evening.

328
00:51:05,758 --> 00:51:08,536
If you are interested, you can come.

329
00:51:09,757 --> 00:51:12,022
You can laugh, that's my brother.

330
00:51:13,021 --> 00:51:14,213
Baa!

331
00:51:14,693 --> 00:51:15,610
Yes!

332
00:51:15,675 --> 00:51:16,962
Are you interested?

333
00:51:17,801 --> 00:51:19,830
Yes, of course.  Its...

334
00:51:20,359 --> 00:51:23,695
Where? What, what do you do?

335
00:51:25,207 --> 00:51:30,399
Tomorrow we will cover a topic that
you'd like to know,

336
00:51:31,002 --> 00:51:32,739
the notion of invisibility.

337
00:51:33,515 --> 00:51:34,775
That's right.

338
00:51:35,843 --> 00:51:39,899
A very important concept, I think for
all religions

339
00:51:40,132 --> 00:51:45,509
but I'll talk about it particularly as it regards to Islam.

340
00:51:49,099 --> 00:51:51,824
- You will be welcome.
- Well, thank you.

341
00:51:52,674 --> 00:51:54,613
Will you bring her?

342
00:51:54,967 --> 00:51:55,999
Will you bring her?

343
00:51:56,657 --> 00:51:59,254
- When? - Tomorrow
- Yes of course, tomorrow then.

344
00:52:02,263 --> 00:52:03,934
But right now I must go...

345
00:52:04,574 --> 00:52:07,320
- ... I have a meeting. - Yes
- I'll see you tomorrow.

346
00:52:07,671 --> 00:52:09,275
- Peace.
- Peace.

347
00:52:16,074 --> 00:52:17,333
So I thought that

348
00:52:17,730 --> 00:52:20,320
- I would introduce you like I said last time
- Yeah.

349
00:52:20,322 --> 00:52:21,761
He has a passion just like you.

350
00:52:22,165 --> 00:52:24,170
A love for Islam.

351
00:52:24,652 --> 00:52:27,445
and at the same time he is a good 
friend you can trust.

352
00:52:28,959 --> 00:52:30,379
- Do you like him?
- Yes.

353
00:52:30,414 --> 00:52:32,538
Yeah me too,
I'm happy.

354
00:53:02,450 --> 00:53:05,034
- I'll call you tomorrow, to take you to my brother's.
- Okay.


355
00:53:05,898 --> 00:53:07,053
See you later.

356
00:53:31,111 --> 00:53:31,869
Wait there.

357
00:53:42,156 --> 00:53:43,275
- Hello.
- Hello.

358
00:53:43,276 --> 00:53:47,084
-  Is this the place where Nassir's class meets?
- Yes back there.

359
00:53:47,290 --> 00:53:48,469
- In there?

360
00:53:53,670 --> 00:53:57,515
Well, what good is a notion if you forget
it one week to another?

361
00:53:58,108 --> 00:54:00,362
Surah 6: Cattle.

362
00:54:00,397 --> 00:54:02,121
Because it contains the key to mystery.

363
00:54:03,988 --> 00:54:06,016
Surah 72: The Jinn

364
00:54:06,251 --> 00:54:07,358
Pardon me.

365
00:54:08,099 --> 00:54:09,350
Ah, hello miss.

366
00:54:09,595 --> 00:54:10,289
Come in.

367
00:54:11,462 --> 00:54:14,642
We have a guest today.
She is not a Muslim but is welcome.

368
00:54:14,677 --> 00:54:15,947
Have a seat there.

369
00:54:16,611 --> 00:54:18,020
Perfect.

370
00:54:18,223 --> 00:54:19,511
Then I will continue:

371
00:54:19,961 --> 00:54:23,903
I will read Surah 72,
verses 26 and 27.

372
00:54:24,238 --> 00:54:29,279
He knows the unseen, nor does he make anyone 
acquainted with his secrets, except a messenger

373
00:54:32,333 --> 00:54:35,396
Thus, "al-ghaib" comes from the
verb "al-ghaiba", "ghaiba"

374
00:54:35,693 --> 00:54:38,210
which means "not here", "absent"

375
00:54:38,245 --> 00:54:41,242
We can translate it 
many ways into French:

376
00:54:41,277 --> 00:54:46,876
Absence, unknown, hidden, unseen.

377
00:54:46,955 --> 00:54:48,204
The invisible,

378
00:54:48,863 --> 00:54:53,871
what is this? God is both what is obvious
and also what is hidden.

379
00:54:54,045 --> 00:54:56,231
More visible and more invisible.

380
00:54:56,654 --> 00:55:00,822
How can we think that these
attributes are contradicting?

381
00:55:01,736 --> 00:55:07,553
God is present in his absence, it is when
he is retired that he is manifested the most. 

382
00:55:07,819 --> 00:55:12,213
And this is faith.  To have confidence in what is not seen.

383
00:55:12,298 --> 00:55:13,429
What is happening here?

384
00:55:16,401 --> 00:55:17,892
Are you okay or what?

385
00:55:19,940 --> 00:55:21,755
Keep yourself silent please.

386
00:55:24,596 --> 00:55:27,307
I'm talking about something else that
distracts your eyes.

387
00:55:28,888 --> 00:55:29,658
- Okay.

388
00:55:30,517 --> 00:55:32,535
How does God manifest himself?

389
00:56:24,087 --> 00:56:25,109
What's wrong?

390
00:56:25,742 --> 00:56:28,124
- I wasn't comfortable in there.
- Why?

391
00:56:28,988 --> 00:56:30,030
Sit.

392
00:56:30,817 --> 00:56:32,146
Please tell me.

393
00:56:36,602 --> 00:56:38,782
Did I say something wrong?

394
00:56:59,128 --> 00:57:00,516
Don't cry.

395
00:57:05,552 --> 00:57:07,557
Is it because of that young man?

396
00:57:10,369 --> 00:57:13,075
I can't bear to look at another.

397
00:57:14,334 --> 00:57:15,698
Another to whom?

398
00:57:30,628 --> 00:57:31,459
Who?

399
00:57:31,735 --> 00:57:33,466
Other than Christ.

400
00:57:39,433 --> 00:57:41,028
It's nice what you say.

401
00:57:46,301 --> 00:57:47,843
I miss him.

402
00:57:53,942 --> 00:57:55,395
What do you mean?

403
00:58:02,748 --> 00:58:04,005
If you love him.

404
00:58:06,464 --> 00:58:08,300
Yes I love him.

405
00:58:26,608 --> 00:58:29,829
I love him and I know that he loves me.

406
00:58:29,945 --> 00:58:33,462
He has come to me often

407
00:58:38,547 --> 00:58:42,140
and made me understand
what is love.

408
00:58:46,066 --> 00:58:47,892
That's what put you in this state?

409
00:58:49,403 --> 00:58:51,156
You should be happy.

410
00:58:51,341 --> 00:58:53,046
Yes, but then

411
00:58:54,134 --> 00:58:56,858
the world full of suffering.

412
00:58:57,557 --> 00:59:01,108
My heart... it makes me ill.

413
00:59:10,575 --> 00:59:12,228
I don't understand very well

414
00:59:12,709 --> 00:59:14,223
What's missing?

415
00:59:16,394 --> 00:59:18,747
His heart, his...

416
00:59:34,239 --> 00:59:36,247
It's not his heart that you love.

417
00:59:37,027 --> 00:59:38,758
It's everything.

418
00:59:41,543 --> 00:59:43,184
Him.

419
00:59:45,339 --> 00:59:46,736
You'll be fine.

420
00:59:48,421 --> 00:59:49,321
No.

421
00:59:50,718 --> 00:59:52,677
If you love him, he is there.

422
00:59:54,556 --> 00:59:56,699
Yes, he is there, I know.

423
01:00:03,692 --> 01:00:05,359
He is not absent.

424
01:00:06,463 --> 01:00:07,423
Yes.

425
01:00:09,388 --> 01:00:11,310
He is invisible.

426
01:00:12,482 --> 01:00:15,425
You know what I said earlier?

427
01:00:21,339 --> 01:00:23,714
He manifests where you are.

428
01:00:35,059 --> 01:00:37,623
If you have faith, if you love,

429
01:00:38,006 --> 01:00:40,673
then he is there, he is in you.

430
01:00:42,832 --> 01:00:45,829
It's happening here, now.

431
01:00:47,153 --> 01:00:49,320
We must do something.

432
01:00:51,897 --> 01:00:54,158
You must act if you have faith.

433
01:00:54,272 --> 01:00:56,768
you have to continue the Creator's work.

434
01:00:58,710 --> 01:01:02,771
He doesnt appear by staying still and suffering

435
01:01:03,426 --> 01:01:06,008
but from action in the world.

436
01:01:25,227 --> 01:01:26,652
Hey its red.

437
01:01:27,153 --> 01:01:28,214
Yassine!

438
01:02:24,944 --> 01:02:26,281
So what do you think.

439
01:02:28,070 --> 01:02:32,058
I'll see your brother, because
theres something I want to say.

440
01:02:33,007 --> 01:02:34,390
What will you say?

441
01:02:37,009 --> 01:02:39,364
About live with God.

442
01:02:41,149 --> 01:02:43,221
I don't think so.

443
01:02:51,513 --> 01:02:55,677
I am in love with God Yassine,
you know, you undertand.

444
01:02:56,321 --> 01:02:58,273
I am with him

445
01:03:01,035 --> 01:03:03,993
I feel him near me.

446
01:03:04,275 --> 01:03:06,515
Are you crazy or what?
God is there.

447
01:03:06,665 --> 01:03:08,726
No, he is not there.

448
01:03:13,905 --> 01:03:15,475
He isn't there Yassine.

449
01:03:22,159 --> 01:03:25,662
I miss his love.

450
01:03:31,667 --> 01:03:35,551
It's like when you are in love with someone,
you need to be with them.

451
01:03:38,435 --> 01:03:39,989
I'm not dead.

452
01:03:40,808 --> 01:03:43,030
But I can't live without his love.

453
01:03:53,623 --> 01:03:55,285
Do you love me or not?

454
01:04:01,300 --> 01:04:04,987
I just said, it is not an absence of a man
what i have is from God.

455
01:04:07,499 --> 01:04:09,005
I believe you

456
01:04:14,366 --> 01:04:15,766
I want...

457
01:04:50,767 --> 01:04:52,362
The struggle is important.

458
01:04:53,228 --> 01:04:54,425
The struggle?

459
01:04:55,067 --> 01:04:56,712
Political action.

460
01:04:58,613 --> 01:04:59,898
Not violent.

461
01:05:03,052 --> 01:05:04,607
Nassir, why use a bomb?

462
01:05:04,696 --> 01:05:06,410
And what do you want me to tell you?

463
01:05:07,848 --> 01:05:09,185
The truth.

464
01:05:10,024 --> 01:05:10,862
The truth?

465
01:05:10,863 --> 01:05:14,025
Violence is natural, that 
is the order of things.

466
01:05:14,857 --> 01:05:17,430
And, we respond with
our violence.

467
01:05:20,830 --> 01:05:22,638
What about the innocents and all that?

468
01:05:22,872 --> 01:05:25,030
What innocents?  Do you think
there are truly innocents

469
01:05:25,065 --> 01:05:28,253
in a democratic world where people
elect their representatives?

470
01:05:28,739 --> 01:05:31,972
You also could kill for the humiliations of the world

471
01:05:40,842 --> 01:05:42,612
and of love for God?

472
01:05:45,072 --> 01:05:47,139
God is truth and justice, Celine.

473
01:05:49,853 --> 01:05:51,568
Peace is against justice.

474
01:05:55,816 --> 01:05:56,983
It's our struggle.

475
01:06:04,610 --> 01:06:05,765
But love?

476
01:06:07,006 --> 01:06:08,486
It's our faith,

477
01:06:09,263 --> 01:06:10,686
our aspiration to Him

478
01:06:19,631 --> 01:06:21,256
That is the struggle, Celine.

479
01:06:26,424 --> 01:06:28,124
The men are soldiers.

480
01:06:30,738 --> 01:06:32,967
Why not be a martyr?

481
01:07:23,105 --> 01:07:24,368
It's here.

482
01:07:24,789 --> 01:07:26,011
Your shelter?

483
01:07:31,021 --> 01:07:32,647
Where I am bound.

484
01:07:33,571 --> 01:07:34,765
Hadewijch.

485
01:07:35,897 --> 01:07:36,977
Hadewijch.

486
01:07:38,648 --> 01:07:39,690
It's pretty.

487
01:07:49,611 --> 01:07:50,699
Come on.

488
01:08:59,930 --> 01:09:01,724
Its looks nice here.

489
01:09:22,696 --> 01:09:24,939
I'm with you Nassir.

490
01:09:25,949 --> 01:09:27,389
I'm ready.

491
01:09:33,263 --> 01:09:36,306
Come on. 
We have a long way back.

492
01:09:48,402 --> 01:09:50,129
One final tour?

493
01:09:52,370 --> 01:09:53,983
Okay, the last.

494
01:09:54,917 --> 01:09:57,425
Then we go,
we have nothing to do here.

495
01:11:49,122 --> 01:11:49,959
Yes

496
01:11:50,208 --> 01:11:51,695
I will go make the food.

497
01:11:53,291 --> 01:11:54,922
I'll come help you my sister.

498
01:12:00,861 --> 01:12:03,408
In fact, there isn't much to do.

499
01:12:03,443 --> 01:12:05,419
- Ok
- Thank you. It's very kind.

500
01:12:10,094 --> 01:12:13,408
- Can you look for the ticket in my room.
- I will.

501
01:12:29,943 --> 01:12:31,126
Its not there.

502
01:12:44,449 --> 01:12:46,016
How long for the food?

503
01:12:46,852 --> 01:12:49,489
- Three minutes
- Ok.

504
01:12:58,518 --> 01:13:00,382
I'm getting hungry.

505
01:13:07,254 --> 01:13:08,935
You ok?

506
01:13:16,765 --> 01:13:19,434
- Yeah
- Ok.

507
01:13:27,334 --> 01:13:28,932
You seem a little weird, eh?

508
01:13:46,131 --> 01:13:47,709
Do you want to break some eggs?

509
01:13:48,510 --> 01:13:50,053
No no no no no.

510
01:13:54,568 --> 01:13:55,821
What's wrong?

511
01:13:57,295 --> 01:13:58,609
I don't know.

512
01:14:09,327 --> 01:14:10,729
Go on. I'll bring it to the table.

513
01:15:11,543 --> 01:15:13,971
he just said there was a bombing.

514
01:15:15,315 --> 01:15:16,693
You'll see it

515
01:16:05,480 --> 01:16:06,678
So, my son...

516
01:16:06,991 --> 01:16:09,200
On Monday, Mom
I go back to work.

517
01:16:09,980 --> 01:16:11,166
Be better

518
01:16:11,563 --> 01:16:13,238
when it has passed.

519
01:16:14,241 --> 01:16:16,332
Because you are not interested in 
doing wrong.

520
01:16:16,357 --> 01:16:17,681
Yes

521
01:16:17,783 --> 01:16:19,035
- Yes
- Good

522
01:16:19,253 --> 01:16:23,529
Anyway, now you look after yourself, okay.

523
01:16:25,963 --> 01:16:27,290
I hope.

524
01:16:28,138 --> 01:16:30,285
Anyway you're not the first
or the last.

525
01:16:30,286 --> 01:16:31,652
Well thats it.

526
01:16:33,924 --> 01:16:35,630
- After all you haven't killed anyone.
- No.

527
01:16:35,665 --> 01:16:36,936
Good.

528
01:16:57,362 --> 01:16:59,103
Celine, don't cry.

529
01:16:59,723 --> 01:17:00,789
Don't cry.

530
01:17:01,645 --> 01:17:03,160
They've killed a little boy.

531
01:17:12,903 --> 01:17:15,984
Have you seen what I told you, have you?

532
01:17:16,241 --> 01:17:18,552
With your eyes, the humiliation.

533
01:18:03,768 --> 01:18:04,973
This is it.

534
01:18:52,072 --> 01:18:53,965
- Want some tea?
- Yes.

535
01:18:53,969 --> 01:18:55,491
Thank you.

536
01:19:14,421 --> 01:19:16,665
I've told him about you.

537
01:19:22,332 --> 01:19:23,784
Do you want anything?

538
01:19:32,113 --> 01:19:33,487
God,

539
01:19:36,107 --> 01:19:37,680
our God,

540
01:19:39,320 --> 01:19:41,226
who has guided me to you.

541
01:19:47,856 --> 01:19:49,487
I am ready.

542
01:19:52,251 --> 01:19:54,919
In the vision of my life and my faults

543
01:19:58,160 --> 01:20:04,023
hiding in the action, close to my sisters and brothers.

544
01:20:09,738 --> 01:20:11,152
God can be wine.

545
01:20:15,381 --> 01:20:18,895
Acting in God and for Him.

546
01:20:20,073 --> 01:20:21,045
In the struggle,

547
01:20:23,329 --> 01:20:24,866
combat.

548
01:20:30,362 --> 01:20:32,803
So that He may live for us all.

549
01:20:42,367 --> 01:20:44,296
I believe there is a light

550
01:20:45,079 --> 01:20:46,927
that guides us,

551
01:20:48,039 --> 01:20:49,666
and in my action,

552
01:20:50,067 --> 01:20:51,891
the weapons

553
01:20:53,174 --> 01:20:55,009
of love, for Him

554
01:21:04,089 --> 01:21:05,735
He chose me

555
01:21:06,742 --> 01:21:08,030
and lit me.

556
01:21:10,772 --> 01:21:13,113
so i will do so.

557
01:21:18,749 --> 01:21:20,333
I am His.

558
01:21:22,871 --> 01:21:31,721
and subject to the mystery of his love
and our union,

559
01:21:38,786 --> 01:21:41,049
who loves me well.

560
01:26:31,460 --> 01:26:33,010
Hadewijch.

561
01:27:28,960 --> 01:27:30,537
Come on young sisters.

562
01:27:32,000 --> 01:27:33,495
We are trapped.

563
01:27:35,321 --> 01:27:37,726
You know where we live...

564
01:30:50,265 --> 01:30:52,386
Someone wants to talk to you.

565
01:32:56,366 --> 01:32:57,585
Love

566
01:33:01,772 --> 01:33:03,374
your power divides us

567
01:33:04,313 --> 01:33:06,070
mine is the word.

568
01:33:07,404 --> 01:33:11,649
You are the light of the day
and my days in the nights.

569
01:33:17,206 --> 01:33:19,149
Why?

570
01:33:22,087 --> 01:33:23,703
Why?

571
01:33:24,399 --> 01:33:25,787
I'm with you

572
01:33:26,053 --> 01:33:29,164
always following you

573
01:33:30,711 --> 01:33:34,357
Why run away from me?

574
01:33:37,282 --> 01:33:38,550
Being further and further away.

575
01:33:43,058 --> 01:33:46,721
you make me love so greatly

576
01:33:57,786 --> 01:34:01,503
but you wont be with a
human creature.

