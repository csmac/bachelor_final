1
00:00:19,710 --> 00:00:25,035
You are beautiful just
as you were yesterday.

2
00:00:25,035 --> 00:00:29,214
No, you're more beautiful
than you were yesterday.

3
00:00:29,214 --> 00:00:34,200
When I say something like this.

4
00:00:34,200 --> 00:00:38,797
You always change subjects
pretending you didn't hear anything

5
00:00:38,797 --> 00:00:44,592
I had really sweet dreams yesterday

6
00:00:44,592 --> 00:00:51,279
I don't wanna tell you now.
Because I'm too shy.

7
00:00:51,280 --> 00:00:57,460
And also it's known not very good
to tell someone this kind of thing.

8
00:00:58,767 --> 00:01:02,162
Dream that I can never have again.

9
00:01:02,163 --> 00:01:04,513
That was really sweet

10
00:01:04,514 --> 00:01:08,427
I think you are just like the dream.

11
00:01:08,428 --> 00:01:11,845
Dream that I get to keep
thinking about all day.

12
00:01:11,846 --> 00:01:18,489
That was really sweet.
That's you.

13
00:01:26,873 --> 00:01:30,790
Lararara hmm.

14
00:01:30,790 --> 00:01:35,190
Lararara hmm.

15
00:01:37,960 --> 00:01:42,684
People say we're
really cute couple

16
00:01:42,684 --> 00:01:47,512
I know, she knows,
Actually I also think so.

17
00:01:47,512 --> 00:01:52,470
But sometimes
I get worried about you.

18
00:01:52,471 --> 00:01:56,428
And then I feel sad alone.

19
00:01:57,952 --> 00:02:06,420
Hmm, You're telling a lie.
With the look full of confidence.

20
00:02:08,100 --> 00:02:15,714
But that's so sweet to hear, That is.

21
00:02:16,982 --> 00:02:20,555
Dream, that I can never have again.

22
00:02:20,556 --> 00:02:22,671
That was really sweet

23
00:02:22,672 --> 00:02:26,927
I think you are just like the dream.

24
00:02:26,927 --> 00:02:30,322
Dream that I get to keep
thinking about all day.

25
00:02:30,322 --> 00:02:36,631
That was really sweet.
That's you.

26
00:02:37,562 --> 00:02:46,504
Well, I don't care.
Even if you're a sweet liar.

27
00:02:47,249 --> 00:02:56,896
Well, I don't care.
Cause I will make you believe.

28
00:02:58,686 --> 00:03:02,182
Dream, When you look
at me just like that

29
00:03:02,182 --> 00:03:04,533
I feel like you're so mine.

30
00:03:04,533 --> 00:03:08,452
That makes me want to fall asleep again.

31
00:03:08,452 --> 00:03:12,109
Dream that I get to keep
thinking about all day.

32
00:03:12,109 --> 00:03:18,417
Such a sweet dream.
That's you

