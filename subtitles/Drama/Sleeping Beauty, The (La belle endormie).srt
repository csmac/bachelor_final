1
00:01:19,101 --> 00:01:21,262
- Hey.
- Hi.

2
00:01:24,446 --> 00:01:26,640
- Your autograph, please.
- Sure.

3
00:01:31,932 --> 00:01:33,170
Thank you.

4
00:01:57,193 --> 00:01:58,515
Thanks for this.

5
00:02:18,767 --> 00:02:20,859
Good. That's it.

6
00:02:26,012 --> 00:02:27,194
Good.

7
00:02:44,970 --> 00:02:48,514
I'm pushing the air in now,
just a little.

8
00:02:48,568 --> 00:02:51,113
So... the pressure in the chest
will change.

9
00:03:11,412 --> 00:03:13,198
You're doing a great job.

10
00:04:04,368 --> 00:04:06,190
Do you need a ride, Lucy?

11
00:04:06,753 --> 00:04:08,002
No, thanks.

12
00:04:08,017 --> 00:04:09,702
Think I might do the dry cleaners'.

13
00:04:10,311 --> 00:04:12,963
- Take care.
- I am right here.

14
00:04:13,887 --> 00:04:14,805
See you.

15
00:04:15,706 --> 00:04:16,906
See you.

16
00:04:39,568 --> 00:04:41,401
Can I interest you in a line?

17
00:04:42,977 --> 00:04:44,495
Yeah, why not?

18
00:05:28,513 --> 00:05:29,807
Thanks.

19
00:05:31,102 --> 00:05:32,466
My pleasure.

20
00:05:37,021 --> 00:05:38,990
- This is Lucy.
- Hi.

21
00:05:39,721 --> 00:05:41,330
Hi, Lucy.

22
00:05:42,703 --> 00:05:44,030
Lucy in the sky.

23
00:05:44,874 --> 00:05:47,653
We were just talking about which
one of us is gonna fuck you.

24
00:05:48,643 --> 00:05:49,701
Really?

25
00:05:53,358 --> 00:05:55,348
Why don't you toss for it then?

26
00:06:00,377 --> 00:06:01,761
You in?

27
00:06:03,843 --> 00:06:05,193
Heads or tails?

28
00:06:07,634 --> 00:06:08,726
Head.

29
00:06:14,306 --> 00:06:15,397
Cunt.

30
00:06:15,991 --> 00:06:17,263
Head it is.

31
00:06:17,540 --> 00:06:20,584
Yes, my prince.
Did I say when?

32
00:06:21,569 --> 00:06:22,980
Did I say tonight?

33
00:06:24,032 --> 00:06:26,098
This year, next year.

34
00:06:28,392 --> 00:06:29,944
Fair enough.
We'll toss.

35
00:06:30,814 --> 00:06:32,314
Tonight or next year.

36
00:06:32,677 --> 00:06:34,204
Tonight...

37
00:06:35,081 --> 00:06:36,255
...head.

38
00:06:39,617 --> 00:06:41,677
- Tonight.
- It's my lucky night.

39
00:06:42,925 --> 00:06:45,249
Now, or in 5 hours?

40
00:06:47,752 --> 00:06:48,842
OK, now.

41
00:06:50,710 --> 00:06:51,801
Tails.

42
00:06:56,448 --> 00:06:57,933
Tails never fails.

43
00:07:02,930 --> 00:07:04,568
Let's go then, shall we?

44
00:07:09,724 --> 00:07:10,916
Bye.

45
00:07:24,124 --> 00:07:25,689
- Morning.
- Morning.

46
00:07:30,841 --> 00:07:32,346
Have you got the rent?

47
00:07:35,606 --> 00:07:36,815
The rent?

48
00:07:38,446 --> 00:07:40,036
The rent.

49
00:07:40,624 --> 00:07:45,959
As in the rent. The rent
is due, the rent is overdue.

50
00:07:47,238 --> 00:07:50,235
Here's some. Won't kill them to wait.

51
00:07:50,399 --> 00:07:52,589
You have no idea, do you?
It's her house.

52
00:07:55,517 --> 00:07:56,900
Is it your house?
f

53
00:07:58,340 --> 00:08:01,851
It's her parent's house. It's no
virtue on being born.

54
00:08:02,301 --> 00:08:04,045
Just pay the fucking rent.

55
00:08:05,429 --> 00:08:06,859
And while you're at it,
clean the fucking bathroom.

56
00:08:07,196 --> 00:08:08,603
As we all agreed, it's your turn.

57
00:08:09,075 --> 00:08:10,999
- I did clean the bathroom.
- You have to grout.

58
00:08:13,484 --> 00:08:14,418
Grout.
58f

59
00:08:16,006 --> 00:08:18,087
In between the tiles,
the black stuff.

60
00:08:21,225 --> 00:08:23,295
It will give me
great pleasure to grout.

61
00:09:29,351 --> 00:09:30,613
Lucy.

62
00:09:38,589 --> 00:09:40,682
Telephone.
It's your mother.

63
00:09:47,072 --> 00:09:48,872
- Line 3.
- Thank you.

64
00:09:55,386 --> 00:09:56,849
Hi, mom.

65
00:09:58,053 --> 00:09:59,696
How did you get this number?

66
00:10:01,878 --> 00:10:03,126
No.

67
00:10:06,041 --> 00:10:07,785
Do you want me to lose this job?

68
00:10:12,150 --> 00:10:14,794
OK, yes. I've got it in my hand.

69
00:10:16,335 --> 00:10:17,742
It's VISA.

70
00:10:18,923 --> 00:10:20,273
5464...

71
00:10:22,287 --> 00:10:24,032
6870...

72
00:10:25,887 --> 00:10:27,654
1390.

73
00:10:29,780 --> 00:10:31,839
The expiry is 04/15.

74
00:10:37,026 --> 00:10:38,432
Sorry?

75
00:10:40,378 --> 00:10:42,685
Yeah, I can see it.
It's 399.

76
00:10:48,490 --> 00:10:49,469
I have to go now, mom.

77
00:10:50,673 --> 00:10:53,283
I have to go.
OK, bye.

78
00:11:17,101 --> 00:11:19,250
- Hello, Lucy.
- Hello, Birdmann.

79
00:11:20,060 --> 00:11:21,950
- Come in.
- I bare gifts.

80
00:11:33,309 --> 00:11:34,659
Thanks.

81
00:11:38,968 --> 00:11:40,723
- So, how are you?
- I'm very well, thank you. And you?

82
00:11:41,342 --> 00:11:42,827
Oh yes, very well. Thank you.

83
00:11:43,840 --> 00:11:46,337
- How's the family?
- Very well, thank you. And yours?

84
00:11:46,787 --> 00:11:48,531
- Oh yes, very well.
- And how are the kids?

85
00:11:49,465 --> 00:11:51,366
Yes, they're fantastic.

86
00:11:52,724 --> 00:11:54,325
That's great.

87
00:11:59,259 --> 00:12:00,457
It's good to see you.

88
00:12:02,672 --> 00:12:04,372
You look beautiful.

89
00:12:04,980 --> 00:12:06,296
Thank you.

90
00:12:22,756 --> 00:12:24,338
I have something important to tell you.

91
00:12:27,853 --> 00:12:31,246
Do you remember that time...
on the beach.

92
00:12:32,387 --> 00:12:34,613
After we'd been to Andy's place,
that moment.

93
00:12:38,113 --> 00:12:39,711
I wanted to kiss you.

94
00:12:41,803 --> 00:12:43,817
You must have wanted me to kiss you.

95
00:12:46,990 --> 00:12:48,588
I couldn't because of my tongue.

96
00:12:51,153 --> 00:12:52,886
My tongue was furred.

97
00:12:54,393 --> 00:12:56,025
Furred and thick.

98
00:12:57,566 --> 00:12:59,085
Putrid.

99
00:12:59,512 --> 00:13:02,235
The asshole, of the asshole,
of the asshole.

100
00:13:08,457 --> 00:13:10,009
I couldn't kiss you.

101
00:13:12,563 --> 00:13:14,285
That's OK.

102
00:13:16,715 --> 00:13:18,616
I wanted you to know.

103
00:13:21,080 --> 00:13:23,363
I just want to love my friends.

104
00:13:29,417 --> 00:13:30,801
I know.

105
00:13:33,474 --> 00:13:35,875
It's not an unreasonable request.

106
00:13:41,732 --> 00:13:43,604
I'm so fucking tired...

107
00:13:44,887 --> 00:13:46,833
of watching Oprah.

108
00:14:21,386 --> 00:14:24,189
Hi. I'm Melissa. I'm calling about the ad
in the student paper.

109
00:14:29,893 --> 00:14:31,277
Slim.

110
00:14:33,474 --> 00:14:34,825
Pert.

111
00:14:40,855 --> 00:14:42,115
What should I wear?

112
00:14:44,512 --> 00:14:46,908
OK great. I look forward to seeing you.

113
00:14:48,078 --> 00:14:49,506
Goodbye.

114
00:15:47,881 --> 00:15:49,828
Are you sure you don't want
a cup of tea or coffee?

115
00:15:52,022 --> 00:15:54,542
- Water?
- No, I'm fine. Thanks.

116
00:16:03,058 --> 00:16:04,409
Yes, I think we're ready.

117
00:16:06,389 --> 00:16:07,693
Yes.

118
00:16:20,654 --> 00:16:22,230
- Hello.
- Hi.

119
00:16:22,804 --> 00:16:24,266
Please, come in.

120
00:16:25,043 --> 00:16:27,226
- Sit down.
- Thank you.

121
00:16:30,758 --> 00:16:31,951
Thank you for coming.

122
00:16:33,480 --> 00:16:36,080
Such a pleasure to see
such a unique beauty.

123
00:16:38,780 --> 00:16:40,445
Let me tell you how things
should proceed.

124
00:16:41,277 --> 00:16:43,325
I'll describe the job and then
if you're interested...

125
00:16:43,550 --> 00:16:45,699
we'll discuss particulars,
how does that sound?

126
00:16:46,605 --> 00:16:47,944
Yes. Good.

127
00:16:49,114 --> 00:16:51,938
We're looking for Silver service waitress
to work at private functions...

128
00:16:52,343 --> 00:16:54,030
in lingerie that we will supply.

129
00:16:54,638 --> 00:16:56,067
You will be working with
other girls...

130
00:16:56,584 --> 00:16:59,048
...some of whom will have
more responsibilities.

131
00:17:00,655 --> 00:17:02,287
There is room for promotion.

132
00:17:03,266 --> 00:17:05,516
The pay is $250/hour cash.

133
00:17:06,439 --> 00:17:08,453
You'll be engaged on a freelance basis.

134
00:17:09,769 --> 00:17:11,198
Job by job.

135
00:17:11,929 --> 00:17:14,056
Either one of us can terminate our
arrangement at any time...

136
00:17:14,461 --> 00:17:17,442
so please be sure to maintain another
more reliable source of income.

137
00:17:18,578 --> 00:17:20,018
I understand.

138
00:17:21,537 --> 00:17:25,368
We rely on mutual trust.
And discretion.

139
00:17:26,556 --> 00:17:29,313
And I'm obliged to tell you they're
heavy penalties...

140
00:17:31,232 --> 00:17:33,937
...very heavy penalties
for any breaches of discretion.

141
00:17:37,643 --> 00:17:40,275
- Am I clear?
- Yes.

142
00:17:41,806 --> 00:17:45,980
My sincere advice to you is to
use the money wisely.

143
00:17:47,880 --> 00:17:51,324
Think of it as a windfall,
pay off a student loan.

144
00:17:52,194 --> 00:17:54,658
Please, please do not think
of this as a career.

145
00:17:55,603 --> 00:17:58,697
Just work hard for a short
amount of time.

146
00:18:00,699 --> 00:18:02,667
Your vagina will not be penetrated.

147
00:18:04,041 --> 00:18:07,596
- Your vagina will be a temple.
- My vagina...

148
00:18:09,441 --> 00:18:10,870
...is not a temple.

149
00:18:12,715 --> 00:18:15,787
While that's quite true my darling,
you won?t ever be penetrated.

150
00:18:17,508 --> 00:18:20,805
Now. Will you stand up
for me please and strip.

151
00:18:31,110 --> 00:18:32,483
Thomas?

152
00:18:44,105 --> 00:18:45,679
Open your mouth, darling.

153
00:18:55,142 --> 00:18:56,638
Not pierced?

154
00:19:01,465 --> 00:19:02,950
Do you have any tattoos?

155
00:19:04,953 --> 00:19:06,156
Good.

156
00:19:07,552 --> 00:19:09,352
Stand your feet apart a few inches.

157
00:19:35,554 --> 00:19:36,758
What's this?

158
00:19:38,154 --> 00:19:39,796
I had a mole removed.

159
00:19:43,430 --> 00:19:44,983
Please, get dressed.

160
00:19:46,692 --> 00:19:50,440
I have some questions that
I need to ask you.

161
00:19:52,430 --> 00:19:53,532
Are you on any medication?

162
00:19:54,400 --> 00:19:55,637
Just the pill.

163
00:19:57,291 --> 00:19:58,394
Anything else?

164
00:19:59,260 --> 00:20:01,465
- No allergies? Antidepressants?
- No.

165
00:20:01,544 --> 00:20:04,122
- Prozac? Effexor?
- No.

166
00:20:05,508 --> 00:20:06,768
Are you a smoker?

167
00:20:07,758 --> 00:20:08,726
No.

168
00:20:09,547 --> 00:20:11,212
Maybe the occasional...

169
00:20:12,382 --> 00:20:15,059
...jazz cigarette.
Very rarely.

170
00:20:16,409 --> 00:20:18,953
Other drugs? Anything at all toxic?

171
00:20:20,314 --> 00:20:21,833
No, never.

172
00:20:22,497 --> 00:20:23,723
Why not?

173
00:20:24,139 --> 00:20:27,875
Throughout history humankind has used drugs.
Drugs are a form of...

174
00:20:28,931 --> 00:20:30,417
...grace.

175
00:20:31,419 --> 00:20:33,260
Aspirin for the soul.

176
00:20:34,523 --> 00:20:36,830
My mother is an alcoholic
with a violent temper.

177
00:20:38,067 --> 00:20:39,846
She runs an astrology hotline.

178
00:20:46,360 --> 00:20:49,588
We have a doctor that we'd
like you to see. For blood tests.

179
00:20:50,448 --> 00:20:52,979
- Will that be a problem?
- No, that's fine.

180
00:20:53,992 --> 00:20:56,107
You called us on a public telephone.
You have mobile?

181
00:20:56,872 --> 00:20:58,470
- Yes.
- Good.

182
00:20:59,010 --> 00:21:01,282
We'll cover all your expenses,
all your work-related expenses.

183
00:21:02,486 --> 00:21:03,634
Now.

184
00:21:04,871 --> 00:21:06,300
Silver service.

185
00:21:07,583 --> 00:21:09,810
- Are you familiar with Silver service?
- Yes.

186
00:21:12,286 --> 00:21:14,412
From what side does one serve the fish?

187
00:21:15,031 --> 00:21:17,551
- Right side.
- Left side.

188
00:21:19,880 --> 00:21:21,894
Thomas, take note.
Thomas is going to help you.

189
00:21:24,358 --> 00:21:27,002
You are very beautiful.
Very talented.

190
00:21:27,643 --> 00:21:29,691
But we're going to make you
even more beautiful.

191
00:21:30,343 --> 00:21:32,762
Even more talented.
Come.

192
00:21:36,873 --> 00:21:38,279
And your name...

193
00:21:39,404 --> 00:21:41,103
We'd like to call you Sara.

194
00:21:42,993 --> 00:21:45,818
OK. Thank you.

195
00:22:46,972 --> 00:22:48,200
Fuck!

196
00:22:50,033 --> 00:22:51,889
- You all right, darling?
- Yeah.

197
00:22:56,558 --> 00:22:59,078
- Would you like a lavender tissue?
- Sure.

198
00:23:06,920 --> 00:23:08,328
I'm sorry.

199
00:23:10,036 --> 00:23:11,263
Fuck.

200
00:23:20,610 --> 00:23:22,568
- Good afternoon.
- Good afternoon.

201
00:23:23,738 --> 00:23:26,224
- How are you?
- Very well, thank you. And how are you?

202
00:23:26,776 --> 00:23:28,092
Oh yes, fine. Thank you.

203
00:23:28,758 --> 00:23:30,266
OK sit down at the table.

204
00:23:30,963 --> 00:23:32,921
To what do I owe this
extraordinary pleasure?

205
00:23:33,967 --> 00:23:35,261
Nothing special.

206
00:23:36,341 --> 00:23:38,985
Just upgrading my skill set.

207
00:23:41,872 --> 00:23:43,294
Nothing special?

208
00:23:47,761 --> 00:23:49,268
White or white?

209
00:24:04,689 --> 00:24:06,369
Can we watch some porn?

210
00:24:08,582 --> 00:24:09,778
No.

211
00:24:17,519 --> 00:24:19,229
What have you done
to your fingers?

212
00:24:52,623 --> 00:24:53,849
Sara?

213
00:25:29,438 --> 00:25:31,894
Hi, Sara.
I'm Sophie.

214
00:25:33,794 --> 00:25:36,359
- You look lovely.
- Thanks.

215
00:25:37,491 --> 00:25:41,473
OK. Now, I need you to go into the
dressing room for me and fix your make up.

216
00:25:42,092 --> 00:25:43,971
You'll find a lipstick palette in there
and I want you to match...

217
00:25:45,085 --> 00:25:46,390
...an exact match...

218
00:25:47,042 --> 00:25:49,315
...match your lipstick to the
color of your labia.

219
00:25:52,792 --> 00:25:53,984
You're kidding me.

220
00:27:12,597 --> 00:27:13,869
It's not a game.

221
00:28:30,759 --> 00:28:33,122
You're OK? Good.

222
00:28:35,406 --> 00:28:36,385
Ready?

223
00:28:37,791 --> 00:28:39,107
Let's go.

224
00:29:18,855 --> 00:29:20,982
Thank you friends for coming
here tonight.

225
00:29:21,871 --> 00:29:24,547
As ever, it is wonderful to see you.

226
00:29:25,381 --> 00:29:26,855
Thank you.

227
00:29:30,815 --> 00:29:34,066
This evening we're serving
Beluga caviar with toasted brioche.

228
00:30:00,427 --> 00:30:01,811
Service.

229
00:30:07,436 --> 00:30:09,737
Galantine of quail
stuffed with black truffle...

230
00:30:09,738 --> 00:30:12,038
on the nest of fried leek
and herbs.

231
00:30:13,298 --> 00:30:14,445
Curious choice.

232
00:30:15,897 --> 00:30:18,519
I first tried that dish
many years ago.

233
00:30:19,295 --> 00:30:22,074
On the occasion of my 20th
wedding anniversary.

234
00:30:22,873 --> 00:30:25,224
But tonight, I've asked my chef
to do what he can...

235
00:30:25,933 --> 00:30:27,497
...to bring it back to life.

236
00:30:28,723 --> 00:30:31,930
Perhaps you'll be able to tell me
whether it really does taste so good.

237
00:30:32,819 --> 00:30:37,769
Or if it is the memories of Elizabeth
and that night that make it so special.

238
00:30:39,277 --> 00:30:40,683
To the dish itself.

239
00:30:42,922 --> 00:30:44,227
To the dish.

240
00:30:45,982 --> 00:30:47,478
To memories.

241
00:31:20,277 --> 00:31:22,494
- Brandy?
- Thank you.

242
00:31:28,040 --> 00:31:29,559
Such fair skin.

243
00:31:31,023 --> 00:31:32,226
Brandy?

244
00:31:49,214 --> 00:31:50,632
Thank you.

245
00:32:11,727 --> 00:32:12,998
Not to worry.

246
00:32:14,258 --> 00:32:15,833
Upsy daisy.

247
00:32:18,095 --> 00:32:19,546
No harm done.

248
00:32:31,101 --> 00:32:32,226
Sorry.

249
00:32:32,856 --> 00:32:34,769
Don't worry.
You did really well.

250
00:32:41,924 --> 00:32:43,612
Leave those for the cleaners.

251
00:32:45,704 --> 00:32:47,504
We won't be needing you
anymore tonight.

252
00:32:50,463 --> 00:32:51,858
Thanks.

253
00:35:59,543 --> 00:36:01,479
- You look lovely.
- Yeah, you too.

254
00:36:01,838 --> 00:36:03,571
Your hair looks great.

255
00:36:12,074 --> 00:36:13,672
Can I leave early tonight?

256
00:36:14,656 --> 00:36:16,340
- Just this once.
- Thank you.

257
00:36:40,293 --> 00:36:42,263
It gets a bit easier.

258
00:36:43,038 --> 00:36:44,367
It's never easy, though.

259
00:36:46,875 --> 00:36:48,304
How long have you been
doing it for?

260
00:36:50,779 --> 00:36:52,759
What kind of a question
is that?

261
00:36:56,854 --> 00:36:58,305
I'm sorry.

262
00:37:16,385 --> 00:37:18,095
- Thanks.
- No problem.

263
00:37:54,205 --> 00:37:55,172
May I?

264
00:37:56,118 --> 00:37:57,659
Yeah, be my guest.

265
00:38:02,868 --> 00:38:06,851
Some people fake their deaths.
I'm faking my life.

266
00:38:09,316 --> 00:38:11,036
You're doing a good job.

267
00:38:15,176 --> 00:38:16,594
I want to show you something.

268
00:38:32,019 --> 00:38:34,449
I would really love to
suck your cock.

269
00:38:39,951 --> 00:38:41,526
Hallelujah.

270
00:39:11,515 --> 00:39:12,786
Hi, Lucy.

271
00:39:15,565 --> 00:39:16,443
What time is it?

272
00:39:17,545 --> 00:39:18,761
It's morning.

273
00:39:32,138 --> 00:39:33,195
Can I get you anything?

274
00:39:34,017 --> 00:39:35,052
Coffee?

275
00:39:36,379 --> 00:39:37,786
Yeah, coffee.
Thanks.

276
00:39:45,515 --> 00:39:46,482
Hey, Birdmann?

277
00:39:47,709 --> 00:39:48,946
Will you marry me?

278
00:39:51,140 --> 00:39:52,063
Yes.

279
00:39:53,547 --> 00:39:55,021
Thank you.

280
00:39:55,609 --> 00:39:57,951
- Not at all.
- It's very kind of you.

281
00:40:00,519 --> 00:40:01,751
It's a pleasure.

282
00:40:13,269 --> 00:40:14,405
Thanks.

283
00:40:26,432 --> 00:40:27,766
I'm ready, I think.

284
00:40:30,831 --> 00:40:32,350
I think I'm nearly ready.

285
00:40:43,409 --> 00:40:45,187
We can get you back
into detox.

286
00:40:46,537 --> 00:40:48,299
I could, I'm flush.

287
00:40:50,617 --> 00:40:51,589
Thank you.

288
00:40:54,806 --> 00:40:56,708
I don't think I'd make it
this time.

289
00:41:01,962 --> 00:41:03,154
You could.

290
00:41:07,284 --> 00:41:08,787
Do you believe that?

291
00:41:14,158 --> 00:41:15,796
I don't know.

292
00:41:34,510 --> 00:41:36,052
Sandhill Dunnart.

293
00:41:37,459 --> 00:41:39,292
Nocturnal and endangered.

294
00:41:40,124 --> 00:41:41,936
A marsupial mouse.

295
00:41:42,926 --> 00:41:44,940
Thought to be extinct
until recently.

296
00:41:46,117 --> 00:41:48,056
Its main predators include
owls and bats.

297
00:41:50,441 --> 00:41:52,680
Little is known about the animal
due to its rarity.

298
00:41:54,458 --> 00:41:57,491
First they eat insects but
will also eat meat on occasion.

299
00:41:58,632 --> 00:42:00,320
Sharp, carnivore's teeth.

300
00:42:02,401 --> 00:42:04,516
Scared, Sandhill Dunnart will make
a loud noise...

301
00:42:05,236 --> 00:42:07,340
...as it moves into an
offensive position.

302
00:42:10,700 --> 00:42:12,088
What it really is...

303
00:42:13,854 --> 00:42:15,396
...the Sandhill Dunnart...

304
00:42:16,532 --> 00:42:20,515
is a miniature, a more
tenacious kangaroo.

305
00:42:34,173 --> 00:42:35,928
Hi, Thomas.
No time, no hear.

306
00:42:38,235 --> 00:42:40,237
I'd be happy to do what I can,
try me.

307
00:42:43,050 --> 00:42:44,310
Sorry?

308
00:42:48,900 --> 00:42:50,048
For how long?

309
00:42:53,254 --> 00:42:54,470
That's it?

310
00:42:56,933 --> 00:42:58,520
How much?

311
00:43:02,086 --> 00:43:05,417
OK yes.
Yeah, I understand.

312
00:43:06,007 --> 00:43:07,174
OK. Bye.

313
00:43:28,448 --> 00:43:30,125
- Hey.
- Good afternoon.

314
00:45:22,696 --> 00:45:25,216
Come in.
Come through.

315
00:45:25,891 --> 00:45:27,297
Welcome to my home.

316
00:45:27,961 --> 00:45:29,660
I hope the trip wasn't too grueling.

317
00:45:30,177 --> 00:45:32,202
- No, it was fine. Thank you.
- Wonderful.

318
00:45:33,091 --> 00:45:35,971
Perhaps you'd like a shower
to refresh you after your long drive.

319
00:45:37,164 --> 00:45:39,059
There's a bathroom at the end
of the corridor on the right.

320
00:45:39,094 --> 00:45:41,007
You'll find a robe behind the door.

321
00:45:43,195 --> 00:45:45,963
Oh, and try not to let your
hair get wet.

322
00:45:46,373 --> 00:45:47,491
Sure.

323
00:46:01,734 --> 00:46:02,994
I'm ready.

324
00:46:05,863 --> 00:46:08,180
- Are you feeling well?
- Pardon?

325
00:46:10,116 --> 00:46:11,477
Are you in good health?

326
00:46:12,332 --> 00:46:13,716
Yeah, I'm fine.

327
00:47:00,418 --> 00:47:03,107
You're going to sleep
and wake up.

328
00:47:03,624 --> 00:47:05,616
It'd be as if these hours
never existed.

329
00:47:06,673 --> 00:47:08,125
You won't even dream.

330
00:47:09,317 --> 00:47:12,152
For an hour or two after you wake
you'll feel, yes, slightly...

331
00:47:12,850 --> 00:47:14,436
...groggy, but then fine.

332
00:47:15,606 --> 00:47:16,923
Perfectly fine.

333
00:47:17,519 --> 00:47:19,297
Not nearly as bad as a hangover.

334
00:47:20,275 --> 00:47:22,132
Such a sleep works wonders.

335
00:47:23,268 --> 00:47:24,348
You'll feel...

336
00:47:25,946 --> 00:47:27,543
...profoundly restored.

337
00:48:19,780 --> 00:48:21,130
Thank you, Clara.

338
00:48:26,182 --> 00:48:29,185
A few months ago,
a strange thing happened.

339
00:48:31,244 --> 00:48:33,855
I was idling through my bookshelves,
when I noticed a book...

340
00:48:34,507 --> 00:48:36,960
...my brother's once given me
for my birthday.

341
00:48:37,387 --> 00:48:39,052
A collection of short stories.

342
00:48:39,997 --> 00:48:43,958
Well, I started to reread
one of those stories.

343
00:48:46,095 --> 00:48:49,516
It was about a man who one morning
wakes up and cannot...

344
00:48:49,898 --> 00:48:51,833
bring himself to get out of bed.

345
00:48:52,238 --> 00:48:55,760
He shuts his eyes is self defense.

346
00:48:57,504 --> 00:49:01,149
He reexamines his life.
He's seized with a restlessness.

347
00:49:01,970 --> 00:49:04,394
He packs his bags, cuts all ties,

348
00:49:04,395 --> 00:49:06,595
he can no longer live
among the people he knows.

349
00:49:06,899 --> 00:49:08,856
They paralyze him.

350
00:49:09,373 --> 00:49:12,051
He's monied, he goes to Rome.

351
00:49:12,917 --> 00:49:16,844
He wants to burrow under the earth
like a bulb, like a root.

352
00:49:17,440 --> 00:49:21,749
But even in Rome he cannot escape
people from his former life.

353
00:49:22,728 --> 00:49:26,699
So, he decides to return to the city
where he was born and educated...

354
00:49:27,251 --> 00:49:31,009
but which he can't quite
bring himself to call home.

355
00:49:33,022 --> 00:49:35,722
Well, the move doesn't help.

356
00:49:36,938 --> 00:49:40,493
He feels he has no more right
to return than a dead man.

357
00:49:41,393 --> 00:49:45,612
What can he do? He desires an extreme
solution to his conundrum.

358
00:49:46,366 --> 00:49:50,483
He aches for nothing less than
a new world, a new language.

359
00:49:52,666 --> 00:49:54,354
Nothing changes.

360
00:49:55,637 --> 00:49:59,135
Out of indifference, and because he
can't think of anything better to do...

361
00:50:00,182 --> 00:50:03,827
...he decides once more to
leave his hometown, to do some hitching.

362
00:50:04,077 --> 00:50:06,147
A man picks him up,
they ride off into the night...

363
00:50:06,148 --> 00:50:08,148
when "bang",
the car smacks into a wall.

364
00:50:08,285 --> 00:50:12,434
The driver dies,
our man is hospitalized, broken up.

365
00:50:15,381 --> 00:50:19,533
Months pass, his wounds heal.

366
00:50:20,850 --> 00:50:23,955
Now he wishes for life.

367
00:50:24,438 --> 00:50:29,029
He has a confidence in himself,
in things he doesn't have to explain.

368
00:50:29,479 --> 00:50:34,587
Things like the pores in his skin,
all things corporeal.

369
00:50:35,115 --> 00:50:39,717
He can't wait to get out of the hospital,
away from the infirm and the moribund.

370
00:50:41,765 --> 00:50:46,783
"I say unto thee, rise up and walk.
None of your bones are broken".

371
00:50:48,639 --> 00:50:50,011
The end.

372
00:50:51,936 --> 00:50:53,163
When I reread those words...

373
00:50:53,564 --> 00:50:56,064
"Rise up and walk.
None of your bones are broken"...

374
00:50:57,528 --> 00:50:59,642
I felt a tremendous sadness.

375
00:51:00,261 --> 00:51:03,726
Do you know what the opening
line of the story is?

376
00:51:05,189 --> 00:51:11,129
"When a man enters his 30th year
people will not stop calling him young".

377
00:51:12,693 --> 00:51:14,122
30.

378
00:51:14,842 --> 00:51:17,362
I'd been given the book
for my thirtieth birthday.

379
00:51:17,823 --> 00:51:20,928
"The Thirtieth Year"
by Ingeborg Bachmann.

380
00:51:21,941 --> 00:51:24,832
So I had heard.

381
00:51:25,901 --> 00:51:27,679
I had been told.

382
00:51:28,534 --> 00:51:32,450
I knew all along, even if
I didn't really know.

383
00:51:33,799 --> 00:51:38,007
The great true things are unsurprising.

384
00:51:40,392 --> 00:51:43,194
But what did I do back then?

385
00:51:44,668 --> 00:51:47,188
I carried on.

386
00:51:48,954 --> 00:51:51,879
I carried on dutifully.

387
00:51:53,242 --> 00:51:56,717
We were the happy couple, Elizabeth and I.
That's how people saw us.

388
00:51:57,415 --> 00:52:01,487
But in truth, I did not cherish my wife.

389
00:52:03,490 --> 00:52:05,504
And I did not cherish my friends.

390
00:52:08,283 --> 00:52:10,488
Or even my children.

391
00:52:12,997 --> 00:52:17,379
I just carried on.

392
00:52:19,927 --> 00:52:21,278
I was a success.

393
00:52:22,223 --> 00:52:23,730
I made my way.

394
00:52:24,225 --> 00:52:29,536
But with each step I cringed.
I was on the back foot, the defensive.

395
00:52:32,271 --> 00:52:33,898
And now...

396
00:52:36,278 --> 00:52:40,662
...tonight, for the first time
I say...

397
00:52:42,069 --> 00:52:44,139
...my bones are broken.

398
00:52:45,939 --> 00:52:47,694
Broken.

399
00:52:49,618 --> 00:52:53,545
One day I will need your help.

400
00:52:57,246 --> 00:53:02,354
All of my bones are broken.

401
00:53:09,424 --> 00:53:10,842
You're safe.

402
00:53:13,609 --> 00:53:17,761
There's no shame here.
No one can see you.

403
00:53:20,551 --> 00:53:23,274
But our rules must be respected.
No penetration.

404
00:53:24,252 --> 00:53:25,861
Thank you, Clara.

405
00:55:40,675 --> 00:55:41,767
Take care.

406
00:55:42,363 --> 00:55:44,073
You will feel better very soon.

407
00:55:45,176 --> 00:55:46,481
Thanks.

408
00:55:58,508 --> 00:56:00,139
Your instinct was right.

409
00:56:01,399 --> 00:56:03,076
We'll see.

410
00:56:47,939 --> 00:56:49,402
You really are a fuckwit.

411
00:56:51,955 --> 00:56:53,752
2 weeks and you're out.

412
00:56:55,156 --> 00:56:56,346
Adios amigo.

413
00:56:56,819 --> 00:56:58,562
Chinga tu madre.
(Fuck your mother)

414
00:57:03,310 --> 00:57:04,874
I'm really sorry.

415
00:57:07,262 --> 00:57:08,780
Me too.

416
00:57:34,308 --> 00:57:35,499
Good morning.

417
00:57:52,208 --> 00:57:53,850
Is this Excelon still available?

418
00:57:54,795 --> 00:57:58,936
That's right. Fantastic place.
Out of the city.

419
00:57:59,453 --> 00:58:01,489
I have keys, I can show you
if you like.

420
00:58:02,681 --> 00:58:03,987
No, it's fine.

421
00:58:04,516 --> 00:58:07,295
I'm sure the Excelon
will be excellent.

422
00:58:11,638 --> 00:58:14,366
- It is better to see it, though.
- No, it's OK.

423
00:58:17,368 --> 00:58:18,830
I have a friend in the building.

424
00:58:20,831 --> 00:58:23,228
Well, if that's the case,
then we can definitely do that.

425
00:58:24,265 --> 00:58:25,503
Do you have photo ID?

426
00:58:27,066 --> 00:58:28,945
Some paperwork.
You know how it is.

427
00:58:29,597 --> 00:58:31,735
A stitch in time saves nine.

428
01:00:06,626 --> 01:00:08,201
Hi, Thomas.
This is Sara.

429
01:00:08,662 --> 01:00:10,777
I was just wondering if
Clara is out of her meeting.

430
01:00:14,726 --> 01:00:16,369
Maybe you could give me a hand.

431
01:00:17,427 --> 01:00:20,172
Thing is, I have a new lease.

432
01:00:20,802 --> 01:00:23,209
I was just wondering if there's
any work available.

433
01:00:28,610 --> 01:00:30,882
I'd be really grateful if you
could keep me in mind.

434
01:00:32,277 --> 01:00:34,235
Thanks very much.
Bye, Thomas.

435
01:00:59,741 --> 01:01:01,563
Nearly done.

436
01:01:18,383 --> 01:01:19,766
It's OK.

437
01:01:35,798 --> 01:01:37,678
Here it comes,
it's all right.

438
01:01:39,928 --> 01:01:41,022
Sorry.

439
01:01:41,819 --> 01:01:43,607
I have an important call,
I really have to take it.

440
01:01:44,439 --> 01:01:46,204
I'll be back, Dr. Frankenstein.

441
01:01:46,931 --> 01:01:49,479
OK. Bad monster.

442
01:01:51,117 --> 01:01:52,245
Look.

443
01:01:52,827 --> 01:01:55,037
It's my bag. I'm coming back, OK?

444
01:02:18,002 --> 01:02:20,308
We have one rule.
No penetration.

445
01:02:23,473 --> 01:02:28,524
Only way I can get a hard on these days
is if I swallow a truck load of Viagra...

446
01:02:28,975 --> 01:02:32,503
...and then some beautiful woman
jams her fingers up my ass.

447
01:02:37,999 --> 01:02:40,159
I'm the one that needs
the penetration.

448
01:02:40,530 --> 01:02:42,364
I don't expect that
good fortune tonight.

449
01:02:42,533 --> 01:02:44,558
You're quite right.

450
01:02:46,358 --> 01:02:47,850
Thanks, Clara.

451
01:04:37,324 --> 01:04:39,740
Stretch your cunt, you little bitch.

452
01:05:57,537 --> 01:06:00,559
I'm gonna press your button
and make you all wet.

453
01:06:01,473 --> 01:06:05,087
Then I'm gonna fuck you
with my big dick.

454
01:06:08,965 --> 01:06:13,573
I'm gonna fuck you with my
horse's prick and make you fucking scream.

455
01:06:35,423 --> 01:06:37,741
You fucking little bitch.

456
01:06:47,753 --> 01:06:50,255
You fucking little whore.

457
01:07:27,374 --> 01:07:29,771
You fucking little bitch.

458
01:08:27,433 --> 01:08:28,698
Hi.

459
01:08:31,270 --> 01:08:33,409
OK, I'm coming.

460
01:08:40,364 --> 01:08:42,168
Excuse me.

461
01:09:16,490 --> 01:09:18,380
It's OK, I'm here.

462
01:09:19,292 --> 01:09:20,968
Hello, Lucy.

463
01:09:26,481 --> 01:09:28,754
Take your top off.

464
01:11:48,970 --> 01:11:52,120
I love drinking.
I'm really good at it.

465
01:11:53,965 --> 01:11:55,361
When did you last see him?

466
01:11:58,442 --> 01:12:01,083
About 3 or 4 months ago.
Took him out to dinner.

467
01:12:02,116 --> 01:12:04,406
He was wearing your old suit,
I think. The gray one.

468
01:12:08,336 --> 01:12:10,091
I haven't seen the Birdmann in...

469
01:12:12,071 --> 01:12:13,883
Well, when was the last time
I saw you?

470
01:12:15,649 --> 01:12:16,931
A year?

471
01:12:20,543 --> 01:12:22,872
They say it was 2 weeks before
his brother found him.

472
01:12:25,178 --> 01:12:26,900
2 weeks in this weather.

473
01:12:29,847 --> 01:12:31,850
Will you marry me?

474
01:12:33,764 --> 01:12:35,101
What?

475
01:12:36,992 --> 01:12:38,623
Why?

476
01:12:39,816 --> 01:12:41,323
I mean...

477
01:12:42,066 --> 01:12:43,697
Why now?

478
01:12:44,496 --> 01:12:46,262
You had your chance.

479
01:12:47,196 --> 01:12:48,625
Go on.

480
01:12:52,844 --> 01:12:54,149
Fuck you.

481
01:12:56,478 --> 01:12:58,143
Fuck you to death.

482
01:13:00,281 --> 01:13:01,957
That's one way to put it.

483
01:13:05,647 --> 01:13:07,110
I'm with Helen now.

484
01:13:12,522 --> 01:13:13,804
I can't believe you.

485
01:13:16,078 --> 01:13:17,528
I don't believe you.

486
01:13:18,563 --> 01:13:19,891
You don't believe you.

487
01:13:21,083 --> 01:13:22,433
Fuck it.

488
01:13:28,948 --> 01:13:30,624
Helen is courteous.

489
01:13:32,582 --> 01:13:34,348
You should try it sometime.

490
01:13:36,891 --> 01:13:38,297
Courtesy.

491
01:13:40,840 --> 01:13:42,595
Courtesy.

492
01:14:00,196 --> 01:14:01,446
You'll be safe here.

493
01:14:02,121 --> 01:14:04,798
There's no shame.
No one can see you.

494
01:14:05,530 --> 01:14:07,082
Thank you, Clara.

495
01:14:07,735 --> 01:14:10,165
I do ask that there?ll
be no penetration.

496
01:14:11,166 --> 01:14:13,862
I also ask that you take care
not to leave any marks.

497
01:14:15,104 --> 01:14:16,522
Thank you.

498
01:16:22,027 --> 01:16:24,097
We do fire people here, you know?

499
01:16:26,820 --> 01:16:28,463
Well?

500
01:16:31,433 --> 01:16:33,098
All right, you're fired.

501
01:16:35,460 --> 01:16:36,968
Thank you.

502
01:16:59,233 --> 01:17:00,212
Excuse me?

503
01:17:01,449 --> 01:17:02,811
Hello, excuse me?

504
01:17:04,285 --> 01:17:05,736
Hi. Can I get some help?

505
01:17:07,986 --> 01:17:09,314
What do you think of this one?

506
01:17:10,844 --> 01:17:12,610
Yeah, not bad.

507
01:17:13,769 --> 01:17:15,187
OK, I'll take it then.

508
01:17:16,470 --> 01:17:17,932
Cool. Up here.

509
01:17:29,295 --> 01:17:31,906
OK. Let's get going.

510
01:17:34,875 --> 01:17:39,320
We pick up with black 129,
making a bold move to attack his triangle.

511
01:17:40,569 --> 01:17:42,841
It's an impressive move,
unexpected.

512
01:17:43,786 --> 01:17:47,083
And then, what does the
master do?

513
01:17:48,006 --> 01:17:51,201
He counter attacks, with white
130 to the right.

514
01:17:52,495 --> 01:17:55,134
White 130?
Why did he do that?

515
01:17:56,342 --> 01:17:58,739
After 27 long minutes of deliberation...

516
01:17:59,470 --> 01:18:03,666
why did the master play a move,
that would result in his own defeat?

517
01:19:28,155 --> 01:19:29,104
Are you gonna come to the party?

518
01:19:29,590 --> 01:19:31,429
I can't. Have a big day tomorrow.

519
01:19:31,450 --> 01:19:33,710
- How big?
- I'm sleeping.

520
01:19:33,864 --> 01:19:37,154
What?
Live a little. Just a little.

521
01:19:39,192 --> 01:19:40,904
- What is it?
- I don't know.

522
01:19:43,716 --> 01:19:45,685
But it's something good for you.

523
01:19:47,397 --> 01:19:48,597
Why not?

524
01:19:53,291 --> 01:19:55,710
There you go.
Back from the dead.

525
01:19:56,554 --> 01:19:58,568
Fear of death is the number 1 hoax.

526
01:21:02,669 --> 01:21:04,229
Lucy, turn it off.

527
01:21:17,582 --> 01:21:18,707
Hello?

528
01:21:20,653 --> 01:21:24,197
Yeah. In 2 minutes. OK.

529
01:21:28,090 --> 01:21:32,152
You need to get some blinds.
Fucking hell.

530
01:22:09,097 --> 01:22:10,300
Lucy.

531
01:22:11,504 --> 01:22:12,922
Come back inside.

532
01:22:15,138 --> 01:22:16,736
- You're sure?
- She's fine.

533
01:22:59,700 --> 01:23:00,926
Can you pull over?

534
01:23:02,119 --> 01:23:03,502
Pull over!

535
01:23:52,314 --> 01:23:53,698
- Hello, darling.
- Hi.

536
01:23:56,038 --> 01:23:58,198
- Are you well?
- Fine, thank you.

537
01:24:00,415 --> 01:24:03,103
- You sure?
- Yeah, I'm fine.

538
01:24:05,320 --> 01:24:07,367
A shower always works wonders.

539
01:24:38,620 --> 01:24:40,094
I'd like to ask a favor.

540
01:24:48,014 --> 01:24:51,187
I need to see what goes on in there.
Just once.

541
01:24:56,070 --> 01:24:57,825
I understand. Believe me, I do.

542
01:24:59,591 --> 01:25:01,065
But my clients...

543
01:25:02,427 --> 01:25:07,073
How can I expose them,
to be frank, to blackmail?

544
01:25:07,996 --> 01:25:09,244
Just once.

545
01:25:10,178 --> 01:25:12,091
That's not the way that
we do things.

546
01:25:13,991 --> 01:25:16,006
- Please, Clara.
- I'm sorry.

547
01:25:28,993 --> 01:25:30,737
And I can assume you're quite well?

548
01:25:32,503 --> 01:25:34,303
Perfectly fine, thank you.

549
01:25:35,766 --> 01:25:37,409
And you're in good health?

550
01:25:38,624 --> 01:25:39,940
Yes.

551
01:25:47,737 --> 01:25:49,132
Good girl.

552
01:28:02,984 --> 01:28:04,492
Thank you, Clara.

553
01:28:48,573 --> 01:28:49,855
Are you sure?

554
01:29:14,280 --> 01:29:15,855
Thank you.

555
01:29:32,282 --> 01:29:33,497
Goodnight.

556
01:29:34,869 --> 01:29:36,670
Goodnight.

