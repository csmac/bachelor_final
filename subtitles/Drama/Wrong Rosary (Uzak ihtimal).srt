1
00:01:02,500 --> 00:01:04,600
Paola! Paola!

2
00:02:09,800 --> 00:02:10,600
Here you go.

3
00:02:10,700 --> 00:02:13,000
Thank you very much.
Good evening.

4
00:02:13,100 --> 00:02:14,200
Good evening.

5
00:02:27,600 --> 00:02:30,000
- Salaamun aleykum.
- Aleykum salaam.

6
00:02:45,000 --> 00:02:45,900
Mustafa, candy?

7
00:03:22,700 --> 00:03:24,400
Excuse me, which way is
Tophane?

8
00:03:24,800 --> 00:03:26,000
Follow the right hand side.

9
00:03:26,200 --> 00:03:27,800
It is 500 meters away.

10
00:03:52,400 --> 00:03:54,300
- Salaamun aleykum.
- Aleykum salaam.

11
00:04:28,200 --> 00:04:30,600
Salaamun aleykum.
Is lmam Ibrahim around?

12
00:04:32,000 --> 00:04:33,100
What is it you want from
lmam Ibrahim?

13
00:04:33,200 --> 00:04:34,300
I am the new muezzin.

14
00:04:35,600 --> 00:04:36,600
Welcome brother!

15
00:04:37,600 --> 00:04:40,100
Lmam Ibrahim is not here.
Come, take a sit. I will call him.

16
00:04:43,000 --> 00:04:44,700
- Do you want some tea?
- Yes, I'll have some.

17
00:04:56,700 --> 00:04:57,800
What is it, Sefik?

18
00:04:58,800 --> 00:05:00,600
Salaamun aleykum.
I am the new muezzin.

19
00:05:00,800 --> 00:05:02,400
Aleykum salaam and rahmetullah.

20
00:05:02,800 --> 00:05:04,200
Welcome. Welcome.

21
00:05:05,800 --> 00:05:07,300
Well, you are very young.

22
00:05:12,200 --> 00:05:14,200
- Is this your first service?
- Yes.

23
00:05:15,100 --> 00:05:16,200
Where are you from?

24
00:05:17,300 --> 00:05:18,600
Ankara, Beypazary.

25
00:05:20,400 --> 00:05:22,300
There was lmam Ebubekir
in Sincan.

26
00:05:22,400 --> 00:05:24,200
We are friends from the religious
high school.

27
00:05:25,800 --> 00:05:29,000
Both the mosque and the community
are very good here.

28
00:05:29,700 --> 00:05:32,000
You will be pleased and
comfortable.

29
00:05:33,300 --> 00:05:34,200
Here is your calendar and...

30
00:05:35,100 --> 00:05:36,800
...these are the keys to
the mosque.

31
00:05:43,400 --> 00:05:44,500
Which religous school
have you attend?

32
00:05:44,600 --> 00:05:45,500
Beypazari.

33
00:05:45,800 --> 00:05:48,200
- And university?
- Yes, theology.

34
00:05:49,000 --> 00:05:50,100
Very good. Masallah.

35
00:05:50,900 --> 00:05:52,100
Have you memorized the Koran?

36
00:05:52,500 --> 00:05:56,600
No. Only the last sections
I don't have the rest.

37
00:05:56,700 --> 00:05:59,100
We started working on it with
my father when I was a kid...

38
00:05:59,500 --> 00:06:01,000
...but then we could not keep up.

39
00:06:01,400 --> 00:06:03,300
- Your father?
- My father is an imam too.

40
00:06:04,800 --> 00:06:06,500
Unsalaried though, not designated.

41
00:06:38,700 --> 00:06:41,300
Imam Niyazi was staying here
before you.

42
00:06:41,900 --> 00:06:43,000
He was from Tokat.

43
00:06:43,700 --> 00:06:45,700
He asked to be reassigned
in his home town.

44
00:06:57,800 --> 00:06:59,600
The furniture he left behind is clean.

45
00:07:01,500 --> 00:07:02,600
There is a television too.

46
00:07:03,400 --> 00:07:04,900
Do not waste too much time with it.

47
00:07:05,600 --> 00:07:07,200
You may watch it from time to time.

48
00:07:26,500 --> 00:07:29,100
I brought this from home. Lokum.

49
00:07:31,800 --> 00:07:33,900
- I'll see you at morning prayer.
- Alright.

50
00:07:34,000 --> 00:07:35,800
- Salaamun aleykum.
- Aleykum salaam.

51
00:09:32,900 --> 00:09:33,900
Can you move there?

52
00:09:34,700 --> 00:09:37,000
I've just moved in to your next door.

53
00:09:37,400 --> 00:09:39,600
I need a screwdriver,
if you have one.

54
00:09:58,800 --> 00:10:00,000
Thank you.

55
00:16:11,500 --> 00:16:14,800
Come on, Musa.
You'll finish it later.

56
00:16:29,600 --> 00:16:33,200
Let me introduce.
Musa is the new muezzin.

57
00:16:33,300 --> 00:16:35,000
Welcome.

58
00:16:37,700 --> 00:16:40,300
Imam Davut is an old member
of our community.

59
00:16:45,900 --> 00:16:47,700
Will you have some?

60
00:16:47,800 --> 00:16:52,400
There is cheese, there is simit.
There is everything.

61
00:18:53,400 --> 00:18:54,900
Anybody there?

62
00:19:02,300 --> 00:19:05,000
I am stuck in elevator.
Anybody there?

63
00:19:23,100 --> 00:19:24,900
The elevatorjust stopped.

64
00:19:37,800 --> 00:19:39,000
Thank you.

65
00:20:54,600 --> 00:20:55,700
Good morning.

66
00:20:55,800 --> 00:20:57,700
Here is your screwdriver.

67
00:21:14,000 --> 00:21:15,600
Thank you for yesterday.

68
00:21:15,900 --> 00:21:18,000
If it was not for you, I would stay
stuck in the elevator.

69
00:21:18,200 --> 00:21:19,400
I am grateful.

70
00:22:09,700 --> 00:22:10,600
How may I help you?

71
00:22:10,800 --> 00:22:11,500
Good day.

72
00:22:12,700 --> 00:22:14,200
The church is closed right now.

73
00:22:14,300 --> 00:22:15,800
The afternoon service will be
at one o'clock.

74
00:22:15,900 --> 00:22:17,800
All right, thank you.

75
00:24:19,900 --> 00:24:21,800
Menakib�l Arifin
(Legends of the Sages)

76
00:24:23,900 --> 00:24:25,000
Excuse me?

77
00:24:25,700 --> 00:24:27,500
The book. Menakib�l Arifin.

78
00:25:03,700 --> 00:25:05,400
Looks like you know each other.

79
00:25:06,200 --> 00:25:08,300
Yes. She is my neighbor.

80
00:25:12,500 --> 00:25:14,800
How come you speak Ottoman?

81
00:25:17,200 --> 00:25:18,300
From school.

82
00:25:19,100 --> 00:25:20,100
Which school?

83
00:25:22,000 --> 00:25:23,200
The religious school.

84
00:25:23,800 --> 00:25:25,800
I am serving as a muezzin
in a nearby mosque.

85
00:25:25,900 --> 00:25:27,500
Is that so?

86
00:25:34,400 --> 00:25:36,400
Do you have an interest for books?

87
00:25:38,700 --> 00:25:40,200
Yes, a little.

88
00:25:57,200 --> 00:25:59,300
Then you should certainly
pay me a visit?

89
00:26:17,200 --> 00:26:18,600
- Thank you.
- You are welcome.

90
00:26:19,200 --> 00:26:20,500
You must have dropped it.

91
00:28:47,800 --> 00:28:50,400
- Salaamun aleykum.
- Alaykum Salaam.

92
00:28:50,600 --> 00:28:52,200
Do you remember me?
We met yesterday.

93
00:28:52,700 --> 00:28:54,000
- At the church.
- Yes.

94
00:28:54,200 --> 00:28:55,700
Please come in.

95
00:29:20,200 --> 00:29:21,300
Sorry to trouble you.

96
00:29:21,400 --> 00:29:23,600
Trouble? No! Please enjoy your tea.

97
00:29:36,400 --> 00:29:38,400
- Do you have sugar?
- Sugar?

98
00:29:38,500 --> 00:29:40,700
- Alright, I got it.
- It is in the kitchen.

99
00:30:07,000 --> 00:30:08,200
There are lots of books.

100
00:30:09,400 --> 00:30:11,000
Well, it makes a living.

101
00:30:12,700 --> 00:30:14,000
To whom are you selling these?

102
00:30:15,200 --> 00:30:16,800
To whoever is interested.

103
00:30:20,000 --> 00:30:21,200
Not an easy job.

104
00:30:24,400 --> 00:30:28,600
Actually I am looking for
an assistant.

105
00:30:29,800 --> 00:30:31,400
For the Ottoman texts.

106
00:30:32,800 --> 00:30:34,800
I would be very glad if
you are interested.

107
00:30:37,000 --> 00:30:39,400
Well you know, I am a muezzin.

108
00:30:40,700 --> 00:30:43,900
I would like to come between
prayer times, if it will be enough.

109
00:30:44,200 --> 00:30:45,600
It won't be a problem for me.

110
00:30:46,400 --> 00:30:48,600
- Also the pay will help you as well.
- Well, okay.

111
00:31:02,300 --> 00:31:03,800
What's up bro?
What are u doing here?

112
00:31:03,900 --> 00:31:05,600
- I have been assigned to Istanbul.
- Assigned?

113
00:31:06,700 --> 00:31:07,700
I became a muezzin.

114
00:31:08,000 --> 00:31:09,600
Very good bro, I hope it's best for you.

115
00:31:09,800 --> 00:31:11,000
Thank you.

116
00:31:11,400 --> 00:31:13,200
My dad told me to stop by
Hadji M�fit sometime.

117
00:31:13,300 --> 00:31:15,300
- So I brought his greetings.
- Very nice of you.

118
00:31:15,400 --> 00:31:17,800
He is around,
he should be here soon.

119
00:31:18,400 --> 00:31:20,800
What about you? Have you finished
memorizing Koran?

120
00:31:21,400 --> 00:31:22,700
- Yes, I have.
- Good.

121
00:31:25,300 --> 00:31:26,200
You rascal!

122
00:31:28,600 --> 00:31:29,700
I should get going.

123
00:31:29,800 --> 00:31:31,300
You make yourself comfortable,
have something to eat.

124
00:31:31,700 --> 00:31:33,100
Tevfik! Help my guest.

125
00:31:33,800 --> 00:31:34,800
There is no need brother.

126
00:31:36,300 --> 00:31:37,800
I should drop by a couple of places.

127
00:31:37,900 --> 00:31:40,500
I have some things
to be taken care of.

128
00:31:41,600 --> 00:31:42,800
Which mosque you said?

129
00:31:43,300 --> 00:31:45,500
Karabas Mustafa Pasha Mosque,
in Tophane.

130
00:31:46,200 --> 00:31:48,600
I hang around there a lot
to smoke narghile.

131
00:31:49,600 --> 00:31:51,200
- I will find you there.
- Anytime, brother.

132
00:31:52,100 --> 00:31:53,500
Write down your mobile number.

133
00:31:54,000 --> 00:31:55,200
I haven't got a phone yet.

134
00:31:59,300 --> 00:32:00,600
You call me then.

135
00:32:02,000 --> 00:32:04,000
If you will need anything
I am around.

136
00:32:04,600 --> 00:32:06,000
I am in a rush now.
Please excuse me.

137
00:32:06,600 --> 00:32:07,800
Yes, sure.
You take care of your business.

138
00:32:07,900 --> 00:32:09,300
Tevfik, look after here!

139
00:32:10,100 --> 00:32:11,500
- See you brother.
- I'll see you.

140
00:32:15,800 --> 00:32:17,000
I am waiting!

141
00:35:55,200 --> 00:35:56,500
Hello Mr. Suha.

142
00:35:57,300 --> 00:35:59,000
The books have not arrived yet.

143
00:35:59,800 --> 00:36:00,900
Sure.

144
00:36:01,200 --> 00:36:02,700
I'll bring it myself, sir.

145
00:36:04,200 --> 00:36:05,300
I am on it.

146
00:36:06,000 --> 00:36:08,000
No problem.

147
00:36:10,000 --> 00:36:11,200
Have a good day.

148
00:36:19,600 --> 00:36:22,200
- Hello dear L�tfi.
- Hello dear Yakup.

149
00:36:23,000 --> 00:36:24,800
Let me introduce.
Musa, Mr. L�tfi.

150
00:36:25,000 --> 00:36:26,000
Nice to meet you.

151
00:36:26,100 --> 00:36:28,100
- The books have arrived.
- Great.

152
00:36:28,400 --> 00:36:29,400
Come, have a seat.

153
00:36:32,900 --> 00:36:35,000
- Here is the delivery.
- Thank you.

154
00:36:37,300 --> 00:36:38,900
Musa is my new assistant.

155
00:36:39,000 --> 00:36:40,800
Is that so? How nice...

156
00:36:40,900 --> 00:36:43,000
- His Ottoman is very good.
- Great.

157
00:36:45,000 --> 00:36:46,600
These books are sold out.

158
00:36:46,700 --> 00:36:49,200
They are very difficult to find,
you know.

159
00:36:49,500 --> 00:36:50,700
You are right.

160
00:36:51,900 --> 00:36:55,700
Bukowski, Women.
Be careful!

161
00:37:03,700 --> 00:37:04,800
Musa is a muezzin.

162
00:37:05,200 --> 00:37:07,700
Is that so? Very nice.

163
00:37:18,600 --> 00:37:20,300
Please tell my greetings
to Miss. Anna.

164
00:37:36,800 --> 00:37:39,200
There is this bookseller
I sometimes visit.

165
00:37:39,500 --> 00:37:40,400
What bookseller bro?

166
00:37:40,900 --> 00:37:42,500
He buys and sells second hand books.

167
00:37:43,200 --> 00:37:44,000
So...

168
00:37:46,000 --> 00:37:47,800
So I work with him.

169
00:37:48,400 --> 00:37:49,600
Do you make some money?

170
00:37:54,900 --> 00:37:56,500
Why is Hadji M�fit get mad at you?

171
00:37:58,400 --> 00:37:59,000
I do not give a fuck.

172
00:37:59,100 --> 00:38:00,000
He was the same
when we were at the course.

173
00:38:00,200 --> 00:38:00,800
Don't you remember?

174
00:38:06,100 --> 00:38:08,300
- You aren't married, are you?
- No.

175
00:38:40,200 --> 00:38:41,700
- Hi.
- Hi.

176
00:38:50,100 --> 00:38:51,500
What if it gets broken again?

177
00:39:41,600 --> 00:39:42,800
Here you go.

178
00:40:02,000 --> 00:40:04,000
Why are they handing
the shop ever?

179
00:40:07,800 --> 00:40:09,500
Maybe the owner went bankrupt.

180
00:40:13,700 --> 00:40:15,200
All these books will be under priced.

181
00:40:20,600 --> 00:40:22,500
Yakup, this is thorn.

182
00:41:45,600 --> 00:41:49,800
- Come on have some simit?
- Thanks. I had breakfast.

183
00:41:56,700 --> 00:41:57,900
Will you excuse me, may I go?

184
00:41:58,800 --> 00:42:02,200
We expect you to dinner,
don't make plans.

185
00:42:02,500 --> 00:42:03,700
Ok, hopefully.

186
00:42:07,800 --> 00:42:10,100
Can you show some stuff
to your bookseller.

187
00:42:11,200 --> 00:42:13,500
Okay. I will make some tea.

188
00:42:17,900 --> 00:42:20,000
There is a good looking chick
in your apartment.

189
00:42:21,200 --> 00:42:22,100
What chick?

190
00:42:22,400 --> 00:42:25,400
I saw her when I was waiting for you.
She's a blonde.

191
00:42:25,800 --> 00:42:28,300
- She's my neighbor.
- Nice.

192
00:42:29,600 --> 00:42:32,000
- She's a Christian.
- Really?

193
00:42:32,900 --> 00:42:35,200
- Nun.
- No shit man!

194
00:42:36,000 --> 00:42:38,100
Her clothes were regular.

195
00:42:38,400 --> 00:42:40,800
Well, I wouldn't know.
I just saw her at the church.

196
00:42:40,900 --> 00:42:42,800
What were you doing
at the church?

197
00:42:44,300 --> 00:42:45,400
Is this the book?

198
00:42:46,900 --> 00:42:48,700
You are a complicated man.

199
00:42:52,800 --> 00:42:54,400
Well, honestly she is beautiful.

200
00:42:58,700 --> 00:43:00,800
Anyway you can
make her convert to Islam.

201
00:43:01,600 --> 00:43:05,200
You would acquire merit
at God's sight.

202
00:43:15,700 --> 00:43:17,100
Let him take a look at those.

203
00:43:18,600 --> 00:43:22,000
Okay. I will show them
to brother Yakup.

204
00:43:28,300 --> 00:43:30,200
I couldn't have finished
even one book.

205
00:43:31,200 --> 00:43:32,400
So finish it.

206
00:43:40,500 --> 00:43:44,200
- What about this Yakup?
- What about him?

207
00:43:45,200 --> 00:43:48,700
- Is he trustable?
- Yes of course he is.

208
00:43:51,300 --> 00:43:53,100
Everywhere is full of swindlers.
That's why I asked.

209
00:44:13,000 --> 00:44:14,000
We are coming.

210
00:44:15,500 --> 00:44:17,100
The prayer is just over.

211
00:44:17,400 --> 00:44:19,600
He is with me, we are coming.

212
00:44:22,400 --> 00:44:24,800
Women! As if we run away.

213
00:44:29,800 --> 00:44:32,200
So, have are you getting along with
the community at the mosque?

214
00:44:32,300 --> 00:44:34,400
Very good. Thankfully,
there has not been any problem.

215
00:44:34,800 --> 00:44:37,400
- They also liked you very much.
- I am glad.

216
00:44:40,600 --> 00:44:42,900
- Welcome.
- Thank you.

217
00:44:43,200 --> 00:44:46,200
- Make yourself comfortable.
- I am, thanks.

218
00:44:49,400 --> 00:44:52,100
- Are you engaged?
- I am not.

219
00:44:53,700 --> 00:44:56,200
I guess it will be my duty
to help you get married.

220
00:44:56,400 --> 00:44:57,600
As fortune wills.

221
00:44:59,400 --> 00:45:01,900
- Any candidate?
- How do you mean?

222
00:45:02,400 --> 00:45:03,600
I mean candidate.

223
00:45:07,400 --> 00:45:08,600
No.

224
00:45:08,800 --> 00:45:12,300
You say no in a way as if
there is a candidate.

225
00:45:12,900 --> 00:45:14,100
No, really there isn't.

226
00:45:17,400 --> 00:45:19,300
Come on know.
I got it!

227
00:45:19,700 --> 00:45:21,600
Who is she?
What is she like?

228
00:45:21,700 --> 00:45:22,800
Come on tell me.

229
00:45:26,400 --> 00:45:29,100
It is a fortunate affair Musa,
you shouldn't be ashamed.

230
00:45:29,200 --> 00:45:31,300
Is she religious?

231
00:45:33,300 --> 00:45:34,300
Yes.

232
00:45:34,900 --> 00:45:36,200
Masallah.

233
00:45:36,900 --> 00:45:39,000
You won't get hurt by a believer.

234
00:45:41,600 --> 00:45:44,300
- As fortune wills.
- As fortune wills.

235
00:46:24,800 --> 00:46:25,900
Good morning.

236
00:46:26,600 --> 00:46:28,300
Wellcome Musa.
How is it going?

237
00:46:28,400 --> 00:46:29,200
Thank you.

238
00:46:31,500 --> 00:46:33,200
I wanted to drop by
after the prayer.

239
00:46:33,700 --> 00:46:34,600
Good...

240
00:46:35,200 --> 00:46:36,800
I have some things to show you.

241
00:46:37,200 --> 00:46:38,500
Will you have a look at them?

242
00:46:38,600 --> 00:46:42,600
Put them on the table.
I'm coming.

243
00:46:50,100 --> 00:46:51,800
Are they worth anything?

244
00:46:55,000 --> 00:46:56,400
Where did you get these?

245
00:46:57,000 --> 00:46:59,100
I have a relative, he gave them to me.

246
00:47:00,200 --> 00:47:01,800
Take them back immediately.

247
00:47:02,500 --> 00:47:03,900
What is it?

248
00:47:04,800 --> 00:47:06,300
Do as I tell you.

249
00:47:07,800 --> 00:47:09,200
Alright, but I didn't get why?

250
00:47:09,700 --> 00:47:11,400
These pieces belong to the museum.

251
00:47:12,100 --> 00:47:14,600
Don't ever bring these kinds of
things to me without asking.

252
00:47:19,400 --> 00:47:21,200
Alright, I'll give them back tomorrow.

253
00:47:21,300 --> 00:47:22,600
Give them back today.

254
00:47:24,800 --> 00:47:26,700
Alright I will give them after
the noon prayer.

255
00:47:33,000 --> 00:47:35,000
Salamun aleykum.
This is Musa.

256
00:47:36,400 --> 00:47:37,600
I am fine.

257
00:47:38,000 --> 00:47:41,200
Look, I showed the papers
you gave me to brother Yakup.

258
00:47:42,900 --> 00:47:44,500
He is not interested.

259
00:47:47,000 --> 00:47:49,300
Yes, since he said that
I couldn't insist.

260
00:47:51,200 --> 00:47:52,800
When shall I give these back to you?

261
00:47:54,800 --> 00:47:56,300
Could it any be sooner?

262
00:47:57,700 --> 00:48:01,400
Alright.
I'll see you then.

263
00:48:09,000 --> 00:48:10,800
- How much?
- 2.25

264
00:49:02,000 --> 00:49:04,800
- Hi.
- Hi.

265
00:49:08,500 --> 00:49:09,300
Are yo headed home?

266
00:49:09,400 --> 00:49:13,600
Yes. No, I have things to do.
I will come later.

267
00:49:13,700 --> 00:49:15,800
- Have a good day.
- Have a good day.

268
00:49:48,700 --> 00:49:53,100
Police. Hands up!
Keep silent!

269
00:49:57,000 --> 00:49:58,400
- Are you Musa?
- Yes.

270
00:49:58,500 --> 00:49:59,400
Handcuffs!

271
00:50:03,400 --> 00:50:04,300
Wear these.

272
00:51:03,000 --> 00:51:04,000
Why didn't you sell
the mosque too?

273
00:51:04,400 --> 00:51:05,400
Swindler!

274
00:51:06,900 --> 00:51:07,300
Brother!

275
00:51:08,000 --> 00:51:09,200
Shut up!

276
00:51:26,000 --> 00:51:26,900
Yakup.

277
00:51:48,300 --> 00:51:50,100
I swear I didn't know anything.

278
00:52:04,100 --> 00:52:06,300
God is my witness, I swear
I am innocent, I swear.

279
00:52:06,400 --> 00:52:08,800
Okay, okay I know.

280
00:52:12,400 --> 00:52:14,200
You told me to take them back.

281
00:52:14,300 --> 00:52:16,800
So I called Ayhan, I told him
to come and take them.

282
00:52:18,700 --> 00:52:20,700
He was also stealing cars.

283
00:52:22,000 --> 00:52:26,100
They found one of those pages
in its truck.

284
00:52:30,200 --> 00:52:32,500
I swear this is the first time
I am hearing these.

285
00:52:33,200 --> 00:52:35,000
I haven't been seeing him
for years.

286
00:52:54,200 --> 00:52:55,300
We are going to my place.

287
00:52:55,800 --> 00:52:59,400
No, I would better go to
my own place.

288
00:52:59,900 --> 00:53:01,000
No, I can't accept that.

289
00:53:01,200 --> 00:53:03,200
I will make you some soup,
it will be good for you.

290
00:53:03,700 --> 00:53:04,900
You don't look well.

291
00:53:06,200 --> 00:53:08,000
Each day of old age is like this.

292
00:53:08,700 --> 00:53:10,800
We are going to my place brother,
I don't accept any objection.

293
00:53:12,400 --> 00:53:16,000
Musa, let me go to my place.

294
00:53:18,400 --> 00:53:20,900
Besides I can't make myself
comfortable anywhere else.

295
00:53:21,700 --> 00:53:23,900
I will be offended if you keep
saying anywhere brother.

296
00:53:24,600 --> 00:53:26,100
I spoke to lmam Ibrahim...

297
00:53:26,500 --> 00:53:27,700
...I will be at home anyway.

298
00:53:28,200 --> 00:53:29,800
Okay then, so be it.

299
00:53:57,800 --> 00:53:59,100
I couldn't even call you.

300
00:54:01,000 --> 00:54:03,400
I didn't know what to do.
I was too afraid and baffled.

301
00:54:03,600 --> 00:54:04,600
Allah allah!

302
00:54:05,100 --> 00:54:07,500
What kind of a man he was?
I was curious.

303
00:54:08,400 --> 00:54:11,800
I asked you to the neighbors,
they told me.

304
00:54:12,300 --> 00:54:13,100
Who told you?

305
00:54:13,400 --> 00:54:16,600
That non-Muslim woman living
in your next door.

306
00:54:17,000 --> 00:54:18,400
She said police was there.

307
00:54:19,000 --> 00:54:20,200
God protected me.

308
00:54:21,000 --> 00:54:22,500
Well, it is over now.

309
00:54:34,400 --> 00:54:36,100
Yakup, meal is ready.

310
00:54:50,100 --> 00:54:51,100
Yakup?

311
00:55:35,700 --> 00:55:38,500
Quick quick, something happened
to brother Yakup.

312
00:55:57,800 --> 00:56:00,000
Water!
A glass of water.

313
00:56:05,600 --> 00:56:07,700
There is nothing to worry
about for the time being.

314
00:56:07,800 --> 00:56:09,100
Good. Thank God.

315
00:56:09,200 --> 00:56:11,000
We opened the blocked vessel
with a stent.

316
00:56:11,700 --> 00:56:13,400
But in order to prevent
future complications...

317
00:56:14,000 --> 00:56:17,700
...he should take his medicines
and stay under cardiologic inspection.

318
00:56:17,800 --> 00:56:21,900
- So we will be here for a while.
- Yes for another day.

319
00:56:22,000 --> 00:56:25,200
The sand bags should stay
two more hours.

320
00:56:25,700 --> 00:56:27,300
Okay. Thank you very much.

321
00:56:40,400 --> 00:56:43,200
I shall go. I hope he'll
get better soon.

322
00:56:43,400 --> 00:56:44,300
Thank you.

323
00:56:49,200 --> 00:56:52,500
Thank you. What can I say,
thank you.

324
00:56:56,600 --> 00:57:00,000
- Clara.
- Clara. Musa.

325
00:57:11,800 --> 00:57:13,000
Clara.

326
00:57:42,800 --> 00:57:45,800
I brought it here when I went to
get your clothes.

327
00:57:52,700 --> 00:57:54,800
- Are you alright?
- I am fine.

328
00:57:54,900 --> 00:57:57,000
I am going to prayer,
do you need anything?

329
00:57:58,200 --> 00:57:59,000
Thank you.

330
00:57:59,900 --> 00:58:01,000
I will come back quickly.

331
00:58:10,000 --> 00:58:10,800
Hi.

332
00:58:40,600 --> 00:58:43,500
She is paralyzed but at least
she is alive.

333
00:58:43,600 --> 00:58:44,700
What was she doing for a living?

334
00:58:46,000 --> 00:58:47,800
- Nun.
- You said, my mother.

335
00:58:48,600 --> 00:58:50,800
She is not my real mother but
she is like a mother to me.

336
00:58:54,600 --> 00:58:56,800
- Welcome.
- Thank you.

337
00:58:58,400 --> 00:58:59,800
You are welcome too Musa.

338
00:58:59,900 --> 00:59:02,200
Clara made some soup for me.

339
00:59:02,500 --> 00:59:04,300
- Lf you could excuse me.
- But you've just came.

340
00:59:04,700 --> 00:59:07,000
- I would better go.
- At least have some coffee.

341
00:59:08,300 --> 00:59:10,600
- Alright.
- I'll make the coffee.

342
00:59:21,600 --> 00:59:24,700
Thanks to Yakup, I learned
how to make coffee too.

343
00:59:25,400 --> 00:59:26,800
If only I could drink it...

344
00:59:36,600 --> 00:59:39,700
That night, I don't know what
would I do if you weren't here.

345
00:59:59,600 --> 01:00:02,200
- Thank you for the coffee.
- You're welcome.

346
01:00:11,300 --> 01:00:12,700
I was so surprised to see her here.

347
01:00:12,800 --> 01:00:14,400
I thought I was at the wrong house.

348
01:00:15,900 --> 01:00:17,200
Yakup, if you love your God don't!

349
01:00:17,300 --> 01:00:19,000
I'm not saying anything Musa.

350
01:00:20,400 --> 01:00:22,600
When did she come?
The place was quite a mess.

351
01:00:22,700 --> 01:00:26,500
Right before you came.

352
01:00:41,400 --> 01:00:43,200
I couldn't make it to? Ile.

353
01:00:43,900 --> 01:00:45,100
I still have an unfinished
business there.

354
01:00:45,200 --> 01:00:47,500
Don't worry about that business.
We'll take care of it.

355
01:00:48,600 --> 01:00:50,400
I guess I missed that opportunity.

356
01:00:52,400 --> 01:00:53,800
It was a good job though.

357
01:01:04,600 --> 01:01:07,100
- Salaamun aleykum.
- Aleykum salaam.

358
01:01:23,200 --> 01:01:27,400
Musa, I will go to my own place,
I have a lot to do.

359
01:01:28,100 --> 01:01:30,900
No, I won't let you go before
you are fully recovered.

360
01:01:33,000 --> 01:01:34,500
I should also go to Sile.

361
01:01:37,400 --> 01:01:39,200
Mr. S�ha's books are at home.

362
01:01:40,900 --> 01:01:43,200
I can take whatever you need,
don't worry.

363
01:02:51,600 --> 01:02:53,300
Clara is my daughter.

364
01:02:59,800 --> 01:03:02,400
We were together with
her mother in Italy.

365
01:03:05,900 --> 01:03:08,100
Anita followed me back here.

366
01:03:08,700 --> 01:03:10,200
I found about that much later.

367
01:03:11,200 --> 01:03:14,400
Clara was born in a church
in Istanbul.

368
01:03:16,500 --> 01:03:20,000
Her mother died after she gave birth.

369
01:03:24,600 --> 01:03:27,300
I have been looking for
my daughter for a long time.

370
01:03:28,000 --> 01:03:29,400
Where were you back then?

371
01:03:30,500 --> 01:03:32,200
In the prison.

372
01:03:33,600 --> 01:03:34,600
Political.

373
01:03:51,500 --> 01:03:53,100
She is dead.

374
01:05:28,000 --> 01:05:30,400
You will be staying at Bianca's.

375
01:05:30,500 --> 01:05:31,800
They should call you soon.

376
01:05:38,700 --> 01:05:40,800
If you will have an emergent need
call me.

377
01:05:47,000 --> 01:05:49,400
- I should get going.
- Alright.

378
01:05:51,500 --> 01:05:53,000
Wait wait!

379
01:05:53,900 --> 01:05:56,200
There are some clothes there,
have a look at them.

380
01:05:56,700 --> 01:05:57,400
Ok.

381
01:06:43,400 --> 01:06:45,600
- Musa.
- Yes.

382
01:06:46,400 --> 01:06:49,800
- Shall we invite Clara to breakfast?
- Ok.

383
01:07:33,800 --> 01:07:36,400
- Is she coming?
- She is not at home.

384
01:07:37,200 --> 01:07:39,200
Her lights were off last night too.

385
01:07:55,700 --> 01:07:56,800
Clara is still not around.

386
01:09:04,200 --> 01:09:06,300
- May it be easy brother.
- Welcome.

387
01:09:15,000 --> 01:09:16,300
I am making some coffee.

388
01:09:18,900 --> 01:09:20,200
Why don't you play some music?

389
01:09:20,800 --> 01:09:22,000
I don't have a player.

390
01:09:24,300 --> 01:09:28,200
Then you take care of the coffee,
I will go shuffle the radio.

391
01:09:58,700 --> 01:10:00,000
Coffee is ready.

392
01:10:15,400 --> 01:10:16,600
It is really bitter.

393
01:10:17,200 --> 01:10:18,600
Drink, you'll get used to it.

394
01:10:24,800 --> 01:10:28,300
Yakup, are you going to tell Clara?

395
01:10:30,200 --> 01:10:31,000
I am scared.

396
01:10:34,400 --> 01:10:35,700
What if I tell her?

397
01:10:36,000 --> 01:10:37,600
- No.
- Why?

398
01:10:39,300 --> 01:10:41,000
I'm waiting for the right time.

399
01:10:46,000 --> 01:10:50,000
- What shall I fix for the dinner?
- I invited Clara this evening.

400
01:10:50,200 --> 01:10:51,200
Really?

401
01:10:57,700 --> 01:10:59,100
- Here you go sir.
- Thank you.

402
01:11:40,700 --> 01:11:41,600
Who wants cola?

403
01:11:43,000 --> 01:11:46,000
- I'll have some.
- I'll have some too.

404
01:11:58,700 --> 01:11:59,600
Here you go.

405
01:12:02,800 --> 01:12:03,800
Thank you.

406
01:12:10,600 --> 01:12:11,400
Do want some more.

407
01:12:18,300 --> 01:12:20,200
What time we are leaving
tomorrow morning?

408
01:12:20,500 --> 01:12:21,400
We'll see.

409
01:12:22,600 --> 01:12:23,900
I say we should go early
in the morning.

410
01:12:24,000 --> 01:12:24,900
Ok.

411
01:12:27,500 --> 01:12:28,700
It will be my first time in Sile.

412
01:12:29,300 --> 01:12:30,800
- You have never been there?
- No.

413
01:12:31,200 --> 01:12:32,400
Me neither.

414
01:13:17,400 --> 01:13:19,400
I believe you'll tell Clara today.

415
01:13:22,200 --> 01:13:24,600
- No, I can't.
- Why?

416
01:13:26,100 --> 01:13:27,200
I am scared.

417
01:13:30,100 --> 01:13:31,300
I will tell her then.

418
01:13:31,800 --> 01:13:32,800
No.

419
01:13:33,200 --> 01:13:34,200
Why?

420
01:13:37,900 --> 01:13:39,800
I'm waiting for the right time.

421
01:13:44,800 --> 01:13:46,000
Do you want some tea?

422
01:13:57,800 --> 01:14:00,100
I will pick you up from here
in two hours.

423
01:14:59,500 --> 01:15:01,200
Welcome. Do you want
your picture taken?

424
01:15:01,300 --> 01:15:02,100
No, thanks.

425
01:15:02,200 --> 01:15:04,600
Only one picture, as a souvenir.

426
01:15:06,800 --> 01:15:07,600
Ok, then.

427
01:15:08,400 --> 01:15:09,400
Can you move there?

428
01:15:12,600 --> 01:15:13,500
Can you get closer?

429
01:15:14,300 --> 01:15:15,400
A little more closer.

430
01:15:17,000 --> 01:15:18,000
Closer brother!

431
01:15:19,300 --> 01:15:23,300
- Alright come on now, take it.
- Ok. One, two...

432
01:15:26,600 --> 01:15:28,400
You are half of the frame.
Let me take another one?

433
01:15:30,700 --> 01:15:31,600
It's okay. No problem.

434
01:15:32,200 --> 01:15:34,700
Let me take another one.
Alright, it is okay, thank you.

435
01:15:46,800 --> 01:15:49,600
The color of the sea will be
different in fall.

436
01:15:51,900 --> 01:15:53,400
We will come back in fall then.

437
01:15:54,600 --> 01:15:55,400
I can't.

438
01:15:57,300 --> 01:15:58,000
Why?

439
01:16:00,000 --> 01:16:01,000
I will not be here.

440
01:16:02,600 --> 01:16:03,500
Why?

441
01:16:05,500 --> 01:16:08,000
I will go to Italy, to become a nun.

442
01:17:48,800 --> 01:17:49,700
Good night.

443
01:22:31,800 --> 01:22:34,400
- I will not be coming Musa.
- Why?

444
01:22:36,400 --> 01:22:37,600
That's the way...

445
01:23:36,100 --> 01:23:37,300
Good bye.

446
01:23:40,600 --> 01:23:41,500
Clara...
