1
00:00:19,886 --> 00:00:21,353
<i>Which floor?</i>

2
00:00:21,855 --> 00:00:23,618
<i>Third... fifth floor.</i>

3
00:00:23,723 --> 00:00:26,021
<i>- Let's go.</i>
<i>- This place sucks.</i>

4
00:00:30,730 --> 00:00:32,527
<i>- Here's Number 13.</i>
<i>- Yeah.</i>

5
00:00:32,632 --> 00:00:34,395
<i>- Two more floors to go.</i>
<i>- All right.</i>

6
00:00:43,309 --> 00:00:46,608
<i>- I can't catch my breath.</i>
<i>- Shit. You should quit smoking.</i>

7
00:00:49,516 --> 00:00:51,609
<i>Fourteen, 15.</i>

8
00:00:51,718 --> 00:00:53,652
<i>- Over there.</i>
<i>- Is it the one we just passed?</i>

9
00:00:57,557 --> 00:00:59,024
Ming Ding

10
00:01:01,227 --> 00:01:02,854
- Ming Ding, wake up.
- Someone's there.

11
00:01:03,730 --> 00:01:05,197
Ming Ding.

12
00:01:10,203 --> 00:01:12,694
- Damn. Everyone's still in bed.
- Who are you looking for?

13
00:01:12,806 --> 00:01:14,296
Ming Ding.

14
00:01:15,241 --> 00:01:16,708
Is Ming Ding here?

15
00:01:16,810 --> 00:01:18,368
What do you want?

16
00:01:18,478 --> 00:01:20,173
- What do you want?
- Out of the way.

17
00:01:22,415 --> 00:01:23,939
- Check that room.
- What are you doing?

18
00:01:24,050 --> 00:01:26,177
We're looking for Ming Ding.
None of your business.

19
00:01:31,091 --> 00:01:33,059
- Is that Ming Ding?
- No.

20
00:01:42,769 --> 00:01:45,431
- Ming Ding
- What are you doing?

21
00:01:49,075 --> 00:01:50,702
He's outside.

22
00:01:51,678 --> 00:01:53,612
- There.
- There he is.

23
00:01:53,713 --> 00:01:55,510
- Bring him out.
- Get up.

24
00:02:02,455 --> 00:02:04,582
Go to your room.
This is none of your business.

25
00:02:04,691 --> 00:02:06,283
Sit down.

26
00:02:09,629 --> 00:02:12,189
Ming Ding, do you know why we're here?

27
00:02:16,035 --> 00:02:17,935
You know why we're here today?

28
00:02:27,113 --> 00:02:29,547
Do you know that Mr. Jiang
wants to double your debt?

29
00:02:29,649 --> 00:02:31,276
I don't know what you're talking about.

30
00:02:31,818 --> 00:02:33,718
You don't know?

31
00:02:33,820 --> 00:02:35,651
You need an explanation?

32
00:02:38,191 --> 00:02:40,489
Okay, wait.

33
00:02:44,330 --> 00:02:46,525
I had to send some money
home to China.

34
00:02:47,367 --> 00:02:49,995
I've been here six months
and haven't sent a dime back to my wife.

35
00:02:51,271 --> 00:02:53,933
The whole reason I came here
was to make money.

36
00:02:54,040 --> 00:02:58,568
The interest Mr. Jiang charges
makes it impossible to save anything.

37
00:02:58,678 --> 00:03:00,145
That's your problem.

38
00:03:00,246 --> 00:03:02,214
When you borrow money,
you have to pay it back.

39
00:03:02,315 --> 00:03:05,011
The interest and due date were set.

40
00:03:05,552 --> 00:03:07,713
Mr. Jiang isn't running a charity.

41
00:03:08,254 --> 00:03:11,519
- I just couldn't make my last two payments.
- Stop acting.

42
00:03:13,693 --> 00:03:15,251
Where's the money?

43
00:03:15,995 --> 00:03:18,520
- In the refrigerator.
- The refrigerator?

44
00:03:18,631 --> 00:03:20,223
Are you trying to keep it fresh?

45
00:03:22,802 --> 00:03:24,497
There's no money.

46
00:03:24,604 --> 00:03:26,265
No, the other one.

47
00:03:52,232 --> 00:03:53,995
Got it.

48
00:03:58,805 --> 00:04:01,672
- How much is there?
- About a grand.

49
00:04:01,774 --> 00:04:03,503
Where is the rest?

50
00:04:03,610 --> 00:04:05,840
I told you I don't have it right now.

51
00:04:06,546 --> 00:04:08,241
Don't act.

52
00:04:08,348 --> 00:04:10,339
This is the deal.

53
00:04:10,450 --> 00:04:13,613
You give us $800 tonight

54
00:04:14,921 --> 00:04:17,515
or your debt is doubled.

55
00:04:22,362 --> 00:04:25,331
$800 by tonight, then we'll
talk about a new payment plan.

56
00:04:34,207 --> 00:04:37,404
A little lesson
so you'll never forget your payment.

57
00:04:37,510 --> 00:04:40,946
- Don't hit me.
- You won't forget to pay again.

58
00:04:41,047 --> 00:04:43,515
- Don't move.
- Don't hit me. Please.

59
00:04:46,152 --> 00:04:48,985
$800 tonight.

60
00:04:50,456 --> 00:04:51,923
Let's go.

61
00:04:53,092 --> 00:04:54,582
See you tonight.

62
00:07:18,771 --> 00:07:20,534
Keep it down.

63
00:07:22,208 --> 00:07:25,143
We only have $500.
Cheng asks you to be careful.

64
00:07:28,481 --> 00:07:30,381
- You're welcome.
- Oh, thank you.

65
00:10:01,267 --> 00:10:03,963
Damn. You trying to kill me?

66
00:10:04,837 --> 00:10:07,169
What's wrong?
A little edgy this morning?

67
00:10:07,273 --> 00:10:08,740
Nothing.

68
00:10:27,159 --> 00:10:29,787
Rough night?

69
00:10:31,597 --> 00:10:32,564
Me too.

70
00:10:33,532 --> 00:10:35,124
On the phone with my wife last night.

71
00:10:38,170 --> 00:10:39,637
Thousands of miles away,

72
00:10:39,739 --> 00:10:42,401
on the other side of the earth,
and we still find a way to fight.

73
00:10:43,442 --> 00:10:45,501
That stinking bitch said...

74
00:10:46,045 --> 00:10:47,808
she said...

75
00:10:48,814 --> 00:10:52,773
that I'm not focused.

76
00:10:54,253 --> 00:10:58,713
That stinking bitch.
Always yapping. So annoying.

77
00:11:00,626 --> 00:11:02,093
Is your wife the same?

78
00:11:10,503 --> 00:11:11,970
Oh, by the way.

79
00:11:26,886 --> 00:11:30,413
I just found a new phone card.

80
00:11:31,791 --> 00:11:35,420
250 minutes for two dollars. Here.

81
00:11:36,495 --> 00:11:40,591
It's the Asia Universal card.

82
00:11:42,635 --> 00:11:45,798
She's pretty cute, huh? Look.

83
00:11:50,576 --> 00:11:52,203
Are you all right?

84
00:11:54,747 --> 00:11:58,513
You know, calling cards
are a booming business.

85
00:11:59,151 --> 00:12:01,711
I heard about one guy who sells
20,000 of these things a day.

86
00:12:02,822 --> 00:12:04,653
20,000 cards a day.

87
00:12:04,757 --> 00:12:06,850
Rich overnight.

88
00:12:07,827 --> 00:12:10,455
I have to look into this.

89
00:12:11,130 --> 00:12:12,825
Make a killing.

90
00:12:17,436 --> 00:12:20,564
Let me ask you.

91
00:12:20,673 --> 00:12:22,300
How's your payment going?

92
00:12:26,712 --> 00:12:28,339
I'm almost done with it.

93
00:12:28,447 --> 00:12:30,881
Four to five months. Then I'm done.

94
00:12:32,284 --> 00:12:34,047
How long did it take you?

95
00:12:34,153 --> 00:12:35,643
Around four years,

96
00:12:35,755 --> 00:12:37,814
because I spent extra money

97
00:12:37,923 --> 00:12:39,390
on the trip to Europe.

98
00:12:39,492 --> 00:12:41,483
Then from England to here.

99
00:12:44,463 --> 00:12:46,158
Why do you ask?

100
00:12:46,766 --> 00:12:48,563
No reason. Just curious.

101
00:12:50,703 --> 00:12:53,001
Your payments going all right?

102
00:12:53,873 --> 00:12:55,363
All right.

103
00:13:23,869 --> 00:13:26,429
Looks like it will rain all day.

104
00:13:31,644 --> 00:13:33,976
You two are earlier than me today.

105
00:13:34,647 --> 00:13:36,581
- Time to work.
- Yep, time to work.

106
00:14:36,375 --> 00:14:40,744
Ma, defrost the shrimp
and don't forget to cut the broccoli.

107
00:14:45,451 --> 00:14:47,043
The keys.

108
00:15:09,642 --> 00:15:11,371
- Good morning.
- Morning.

109
00:15:13,412 --> 00:15:15,710
- Big Sister, good morning.
- Good morning.

110
00:15:27,493 --> 00:15:30,394
Big Sister, don't forget
to order pork tonight.

111
00:16:09,168 --> 00:16:11,329
Ma, get the flour.

112
00:16:14,807 --> 00:16:16,274
Young, get the MSG.

113
00:16:39,999 --> 00:16:43,264
Slowly...

114
00:16:47,373 --> 00:16:49,000
Slow down.

115
00:16:50,242 --> 00:16:51,869
What's wrong?

116
00:16:53,078 --> 00:16:54,045
Nothing.

117
00:16:54,146 --> 00:16:56,671
- Can't get it up? Impotent?
- Fuck you.

118
00:17:04,423 --> 00:17:05,981
Wait.

119
00:17:06,525 --> 00:17:08,083
What's wrong with you?

120
00:17:10,262 --> 00:17:12,560
Can you lend me some money?

121
00:17:13,098 --> 00:17:14,929
How much?

122
00:17:15,034 --> 00:17:16,501
$300.

123
00:17:17,369 --> 00:17:19,428
Why do you need to borrow money?

124
00:17:22,474 --> 00:17:25,102
It's an emergency.
Just lend me some money.

125
00:17:27,146 --> 00:17:29,410
I just sent money home this morning.

126
00:17:30,516 --> 00:17:32,677
I only have $150 left.

127
00:17:33,952 --> 00:17:35,977
Tell me why you need money.

128
00:17:37,423 --> 00:17:40,187
- Will you lend it to me or not?
- I didn't say I wouldn't.

129
00:17:40,292 --> 00:17:42,658
- Why do you need the money?
- Why so many questions?

130
00:17:44,063 --> 00:17:46,725
I owe money to a loan shark.

131
00:17:47,633 --> 00:17:49,760
Why do you owe them money?

132
00:17:49,868 --> 00:17:51,733
Why do you owe them money?

133
00:17:51,837 --> 00:17:54,067
- Are you gambling?
- No.

134
00:17:54,173 --> 00:17:55,640
I'll tell you another time.

135
00:17:55,741 --> 00:17:57,208
Let's go.

136
00:17:57,743 --> 00:18:01,270
Will you lend me the money or not?

137
00:18:10,556 --> 00:18:12,581
What are you going to do
for the other $150?

138
00:18:14,293 --> 00:18:17,854
Do you want to borrow
from Big Sister, Ma or Wei?

139
00:18:18,697 --> 00:18:20,164
Don't tell them.

140
00:18:20,265 --> 00:18:21,732
Why not?

141
00:18:21,834 --> 00:18:23,301
I just don't want them to know.

142
00:18:23,402 --> 00:18:26,166
Go. Just go.

143
00:18:38,717 --> 00:18:40,708
I got it.

144
00:19:12,417 --> 00:19:15,113
Big Sister,
when is your daughter coming to visit?

145
00:19:15,220 --> 00:19:16,881
Not for a while.

146
00:19:17,689 --> 00:19:20,817
- When is your wife coming over?
- I'm not married.

147
00:19:20,926 --> 00:19:22,826
Today you're not married, right?

148
00:19:38,010 --> 00:19:39,568
Small house special Lo-Mein.

149
00:19:39,678 --> 00:19:41,168
Make it spicy.

150
00:19:44,816 --> 00:19:47,444
So how did you get yourself into this?

151
00:19:47,986 --> 00:19:49,647
I borrowed money from those guys.

152
00:19:49,755 --> 00:19:51,222
Why?

153
00:19:52,324 --> 00:19:55,088
I wanted to pay back my family in full.

154
00:19:55,194 --> 00:19:57,185
What's your problem?

155
00:19:59,398 --> 00:20:01,730
Because they already had
my brother's debt to deal with.

156
00:20:01,833 --> 00:20:03,494
It's tough on them.

157
00:20:06,438 --> 00:20:07,905
What's the interest?

158
00:20:09,041 --> 00:20:10,633
Thirty percent.

159
00:20:10,742 --> 00:20:14,303
Thirty percent? Are you crazy?
Why didn't you do the math?

160
00:20:14,413 --> 00:20:16,881
I thought I did.

161
00:20:19,117 --> 00:20:21,415
I don't know what you're doing.

162
00:20:23,322 --> 00:20:28,658
Families are expected
to borrow money for the debt.

163
00:20:29,194 --> 00:20:30,661
You're not expected
to pay it back right away.

164
00:20:30,762 --> 00:20:33,128
You have to figure out a way
or you'll be in debt forever.

165
00:20:33,232 --> 00:20:35,132
I know.

166
00:20:48,180 --> 00:20:49,738
So, what are you going to do?

167
00:20:51,116 --> 00:20:55,382
The most tips
I've ever made in a day is $90.

168
00:20:56,622 --> 00:20:58,146
Take my deliveries today.

169
00:20:58,257 --> 00:21:00,623
- What?
- Make all the deliveries today.

170
00:21:03,095 --> 00:21:05,427
- No, I feel bad.
- It's all right.

171
00:21:05,530 --> 00:21:07,498
Do all the deliveries.

172
00:21:09,034 --> 00:21:10,501
Really?

173
00:21:10,602 --> 00:21:12,229
Really.

174
00:21:16,475 --> 00:21:18,875
I'm fine.

175
00:21:19,444 --> 00:21:22,743
Plus, I don't want to work in this weather.

176
00:21:25,951 --> 00:21:27,441
It's good that it's raining.

177
00:21:27,552 --> 00:21:30,043
More rain, more deliveries.

178
00:21:30,589 --> 00:21:33,251
The more rain, the better.

179
00:21:34,026 --> 00:21:37,018
- More deliveries, more money.
- Delivery.

180
00:24:46,518 --> 00:24:48,008
What are you doing there?

181
00:24:48,553 --> 00:24:50,145
You're taking up space.

182
00:25:35,534 --> 00:25:40,938
Two Mexican squid with extra shrimp.

183
00:25:55,887 --> 00:25:58,378
Did you know that-

184
00:25:58,490 --> 00:25:59,957
Big Sister.

185
00:26:00,058 --> 00:26:02,993
Eating too many carrots will turn
the whites of your eyes orange?

186
00:26:03,094 --> 00:26:05,562
Right, eat more.

187
00:26:07,999 --> 00:26:10,126
Why are you here all day?

188
00:26:10,869 --> 00:26:12,632
Don't you have deliveries to make?

189
00:26:12,737 --> 00:26:14,364
Ming is doing them.

190
00:26:14,472 --> 00:26:16,064
Why is Ming doing all of them?

191
00:26:16,174 --> 00:26:18,734
He needs the exercise to lose weight.

192
00:26:19,277 --> 00:26:22,269
- I think you're the one who needs exercise.
- Fuck you.

193
00:26:22,380 --> 00:26:23,847
Look at your stomach.

194
00:29:48,186 --> 00:29:50,416
So, did Lin-Xing call you again?

195
00:29:50,522 --> 00:29:52,615
Yeah, last night.

196
00:29:53,491 --> 00:29:54,958
Is he here?

197
00:29:55,059 --> 00:29:56,924
No, he's still in China.

198
00:29:57,829 --> 00:29:59,490
Has he gotten a green card yet?

199
00:29:59,597 --> 00:30:02,157
No, he's still trying.

200
00:30:02,934 --> 00:30:04,959
Getting the illegitimate marriage?

201
00:30:05,570 --> 00:30:08,471
He doesn't have the money for that.

202
00:30:08,573 --> 00:30:10,097
How much does that cost now?

203
00:30:10,208 --> 00:30:12,005
$40,000.

204
00:30:12,110 --> 00:30:13,600
$150,000?

205
00:30:13,711 --> 00:30:15,178
$40,000.

206
00:30:16,014 --> 00:30:17,538
$40,000.

207
00:30:17,649 --> 00:30:19,241
Isn't that expensive?

208
00:30:19,350 --> 00:30:21,181
No, that's cheap.

209
00:30:22,053 --> 00:30:23,680
Doesn't he have the money?

210
00:30:23,788 --> 00:30:27,781
He has 120,000
to 130,000 yuan.

211
00:30:27,892 --> 00:30:32,591
But he spends a lot of money
on those name-brand clothes.

212
00:30:32,697 --> 00:30:35,495
Just a pair of underwear
costs him around 600 yuan.

213
00:30:35,600 --> 00:30:37,397
Hey, don't knock it.

214
00:30:37,502 --> 00:30:40,562
Underwear is the most important
article of clothing on a person.

215
00:30:41,105 --> 00:30:42,800
It says the most about you.

216
00:30:42,907 --> 00:30:46,172
Boxers, tighty whities or thongs.

217
00:30:46,911 --> 00:30:49,436
Wei, are you wearing your thong today?

218
00:30:50,415 --> 00:30:52,144
Do your work.

219
00:30:52,250 --> 00:30:54,616
Just kidding.

220
00:33:27,872 --> 00:33:30,397
Seriously,
I've been saving for two months.

221
00:33:30,508 --> 00:33:32,135
Two months.

222
00:33:34,012 --> 00:33:36,412
- Big wonton soup.
- Two months?

223
00:33:36,514 --> 00:33:38,641
It will take you forever.

224
00:33:38,750 --> 00:33:40,217
No, not forever.

225
00:33:40,318 --> 00:33:42,445
Maybe a couple more years.

226
00:33:42,987 --> 00:33:45,512
How long did it take the boss
to get this place?

227
00:33:48,192 --> 00:33:49,853
Ten years.

228
00:33:49,961 --> 00:33:51,861
Ten years? Pretty good.

229
00:33:52,630 --> 00:33:56,566
Perfect. I've been working for four years.
Six more years and I'll be a boss too.

230
00:33:56,667 --> 00:34:00,000
I won't forget you guys
when I'm the big boss.

231
00:34:00,104 --> 00:34:02,664
Wei, you'll be my restaurant manager.

232
00:34:02,774 --> 00:34:05,607
No, not manager. The chef.

233
00:34:05,710 --> 00:34:07,268
We'll see.

234
00:34:08,579 --> 00:34:11,412
Ma, you'll be my chef. Screw Wei.

235
00:34:11,516 --> 00:34:14,314
Sounds good to me.

236
00:34:17,889 --> 00:34:20,517
- Delivery.
- Ask Ming.

237
00:34:21,092 --> 00:34:22,582
Ming is out on a delivery.

238
00:34:24,729 --> 00:34:27,323
Big Sister, you're coming with me, right?

239
00:34:27,432 --> 00:34:29,024
Go, go, go.

240
00:34:29,133 --> 00:34:33,797
Big Sister, you're going to be
my restaurant manager, right?

241
00:34:35,106 --> 00:34:38,337
There are too many orders,
I have to help. But I will take it slow.

242
00:35:57,188 --> 00:35:58,655
Why did you mess up my order?

243
00:35:58,756 --> 00:36:00,223
What are you talking about?

244
00:36:00,324 --> 00:36:03,293
I have to make two trips
because you messed up the order.

245
00:36:03,394 --> 00:36:05,658
Psycho, I don't know
what you're talking about.

246
00:36:05,763 --> 00:36:08,254
The delivery I just made.
The guy didn't want chicken.

247
00:36:08,900 --> 00:36:10,367
Ask Big Sister, I followed the order.

248
00:36:10,468 --> 00:36:11,935
I can't afford a mistake today.

249
00:36:12,036 --> 00:36:14,869
What are you talking about?
I'll redo your order.

250
00:36:14,972 --> 00:36:17,873
Are you deaf?
How did you mess up chicken and beef?

251
00:36:18,609 --> 00:36:21,339
I don't think you're cut out for this.
Why don't you go back to China?

252
00:36:21,445 --> 00:36:24,710
- You have a hearing problem?
- Watch your attitude.

253
00:36:24,815 --> 00:36:27,010
- Watch out, I'll smack you.
- Psycho.

254
00:36:27,118 --> 00:36:31,179
If you don't want to work,
go back to China. New guy.

255
00:36:31,289 --> 00:36:34,258
- What do you mean "new guy"?
- Don't fight.

256
00:36:35,893 --> 00:36:40,125
He didn't do it on purpose.
Just a little mistake.

257
00:36:40,231 --> 00:36:43,325
Stop fighting. Get out if you want to fight.
I can't hear a thing.

258
00:36:43,434 --> 00:36:46,198
He can't afford a mistake today.
What the hell does that mean?

259
00:37:55,273 --> 00:38:00,540
Ming, why do you look sad?
What's on your mind?

260
00:38:56,534 --> 00:38:58,001
Fuck.

261
00:39:06,043 --> 00:39:07,510
Fuck.

262
00:39:34,405 --> 00:39:35,872
How can you see it?

263
00:39:35,973 --> 00:39:38,407
Don't worry, I'll fix it for you.

264
00:39:38,509 --> 00:39:39,976
Just wait over there.

265
00:39:42,780 --> 00:39:44,680
Who broke it?

266
00:39:50,121 --> 00:39:52,646
It doesn't go down.

267
00:39:52,757 --> 00:39:54,987
What the hell. Is it broken?

268
00:39:58,262 --> 00:40:00,321
I'll fix it in no time.

269
00:40:00,431 --> 00:40:02,365
Okay, okay.

270
00:40:03,200 --> 00:40:05,225
Small fried rice, no vegetables.

271
00:40:33,130 --> 00:40:34,893
Only one dollar?

272
00:40:35,699 --> 00:40:37,360
Not even one dollar.

273
00:40:37,468 --> 00:40:38,992
Sometimes just a few coins.

274
00:40:39,103 --> 00:40:42,504
It'll get better later.

275
00:40:42,606 --> 00:40:45,074
To your next customer, say...

276
00:40:50,915 --> 00:40:52,974
What? I don't want to.

277
00:40:53,083 --> 00:40:57,417
He'll give you a bigger tip.

278
00:40:58,789 --> 00:41:03,556
You have to smile.

279
00:41:14,705 --> 00:41:17,401
Your eyes have to smile.
Your eyes are not smiling.

280
00:41:20,144 --> 00:41:23,671
Damn, you're crazy.

281
00:41:23,781 --> 00:41:27,615
You want to make money.
You have to try harder.

282
00:41:30,688 --> 00:41:32,155
You have to smile.

283
00:41:32,256 --> 00:41:33,746
Didn't you hear?

284
00:41:34,291 --> 00:41:37,089
How can I smile
when I'm in a bad mood?

285
00:41:39,530 --> 00:41:41,020
Big Sister always says...

286
00:41:45,803 --> 00:41:48,271
You're nuts.

287
00:41:49,306 --> 00:41:50,898
So you have to smile.

288
00:41:53,110 --> 00:41:54,577
No.

289
00:41:55,946 --> 00:41:57,937
I don't think that will help.

290
00:41:58,048 --> 00:42:00,915
- Trust me.
- Okay.

291
00:42:01,018 --> 00:42:04,181
- That's how I do it.
- I'll try my best next time.

292
00:42:04,288 --> 00:42:06,256
You have to do it.

293
00:42:07,291 --> 00:42:08,986
You never listen to me.

294
00:42:13,764 --> 00:42:15,789
Ming, delivery.

295
00:42:20,304 --> 00:42:22,363
Remember to smile...

296
00:42:24,575 --> 00:42:26,133
Fine, I will.

297
00:44:23,861 --> 00:44:25,419
I put it there.

298
00:44:26,130 --> 00:44:27,097
Why?

299
00:44:27,798 --> 00:44:30,824
Because there wasn't any room.

300
00:44:31,669 --> 00:44:32,636
Really?

301
00:44:32,736 --> 00:44:35,534
Really, there wasn't any space.

302
00:44:35,639 --> 00:44:37,129
Why didn't you make room?

303
00:44:37,675 --> 00:44:39,870
I told you there was no room.

304
00:44:42,012 --> 00:44:43,877
Make room if there's no room.

305
00:44:43,981 --> 00:44:46,677
What's the big deal?

306
00:44:46,784 --> 00:44:50,777
Guys, please. Every day.
I'm getting a headache here.

307
00:44:50,888 --> 00:44:54,016
It's not a big deal.
He just likes to fight.

308
00:44:54,858 --> 00:44:56,621
I like to fight with him.

309
00:44:56,727 --> 00:45:00,561
If he didn't mess up my system,
I wouldn't care.

310
00:45:02,132 --> 00:45:03,429
Is it that hard?

311
00:45:04,501 --> 00:45:06,628
Cooked food on the top.

312
00:45:07,438 --> 00:45:09,030
Raw beef on the second shelf.

313
00:45:09,139 --> 00:45:11,073
Raw chicken on the third shelf.

314
00:45:11,175 --> 00:45:13,700
Chopped vegetables on the bottom shelf.

315
00:45:15,879 --> 00:45:17,779
I know.
You've told me many times.

316
00:45:17,881 --> 00:45:20,247
But there was no space.

317
00:45:20,350 --> 00:45:22,011
Is your brain in your ass?

318
00:45:22,119 --> 00:45:23,586
Didn't you think of making room?

319
00:45:23,687 --> 00:45:27,919
Yes, I thought of it. Then I thought
it would be more fun to piss you off.

320
00:45:30,861 --> 00:45:34,627
My cousin should be arriving
in the States at the end of the month.

321
00:45:34,732 --> 00:45:38,361
- Really?
- Is he going to do it Ma style?

322
00:45:39,603 --> 00:45:43,733
"Don't send me back. Communism is so bad.
I can only have one kid."

323
00:45:47,678 --> 00:45:49,145
You don't understand.

324
00:45:49,246 --> 00:45:50,838
You guys got here before 9/11.

325
00:45:50,948 --> 00:45:53,849
It's much harder after 9/11.

326
00:45:53,951 --> 00:45:56,476
Ming, you got here after 9/11?

327
00:45:56,587 --> 00:45:57,554
Yeah.

328
00:45:58,589 --> 00:46:00,284
He didn't get caught.

329
00:46:00,824 --> 00:46:01,791
Really?

330
00:46:03,327 --> 00:46:05,727
Must've had a good smuggler.

331
00:46:06,897 --> 00:46:09,730
Where did you come in?

332
00:46:09,833 --> 00:46:12,734
- Canada.
- Really, Canada? Not bad.

333
00:46:12,836 --> 00:46:14,667
Ma wasn't that lucky.

334
00:46:14,772 --> 00:46:17,332
Ma's boat got nabbed
off the California coast.

335
00:46:17,441 --> 00:46:18,465
Yeah.

336
00:46:18,575 --> 00:46:22,033
We all had to apply for political asylum.

337
00:46:23,380 --> 00:46:25,041
So you got traveling papers?

338
00:46:25,149 --> 00:46:28,084
Yeah. While I wait for a hearing.

339
00:46:30,988 --> 00:46:32,615
When is the court date?

340
00:46:32,723 --> 00:46:34,418
End of the month.

341
00:46:34,525 --> 00:46:36,220
You're going to California?

342
00:46:37,060 --> 00:46:39,392
No. Are you kidding me?

343
00:46:39,496 --> 00:46:41,828
I'll wait for the next amnesty program
in a few years.

344
00:46:41,932 --> 00:46:44,560
No trip to any court.

345
00:46:45,102 --> 00:46:46,933
You'll wait forever.

346
00:46:48,272 --> 00:46:50,502
Small broccoli chicken.

347
00:46:56,914 --> 00:47:00,611
Broccoli chicken.

348
00:47:09,159 --> 00:47:13,755
Yesterday a black guy
was here to sell a bike again.

349
00:47:13,864 --> 00:47:15,957
- Which one?
- Which black guy?

350
00:47:16,066 --> 00:47:18,500
Number 100. Who knows?

351
00:47:18,602 --> 00:47:22,003
There was a black guy here last week.
He was trying to sell me a bike.

352
00:47:23,207 --> 00:47:28,144
The bike he tried to sell me
was the one stolen from me last time.

353
00:47:28,245 --> 00:47:30,042
- It was your bike?
- Yeah.

354
00:47:30,581 --> 00:47:32,071
It's all your fault.

355
00:47:32,182 --> 00:47:33,308
Why?

356
00:47:34,484 --> 00:47:35,746
Why?

357
00:47:36,320 --> 00:47:39,949
The blacks rip a bike off
from one restaurant

358
00:47:40,057 --> 00:47:41,752
and then sell it to another restaurant.

359
00:47:41,859 --> 00:47:45,386
And because you buy them,
you create the demand.

360
00:47:45,495 --> 00:47:47,053
Of course it's your fault.

361
00:47:48,031 --> 00:47:51,933
So we should spend $100 on a new bike
every time we get ripped off?

362
00:49:07,377 --> 00:49:09,868
Wonton soup. Large, not small.

363
00:49:36,840 --> 00:49:38,535
Boneless chicken and roast pork fried rice.

364
00:49:38,642 --> 00:49:41,202
Hurry up, I have other orders.

365
00:49:57,594 --> 00:49:59,619
No, I don't know.

366
00:49:59,730 --> 00:50:02,028
Those are extra.

367
00:50:06,470 --> 00:50:08,301
It's hot, open the door.

368
00:50:09,706 --> 00:50:13,301
Young, still sleeping?
Take this delivery.

369
00:50:14,111 --> 00:50:17,205
Ming is out and I have too many orders.
Take some deliveries.

370
00:50:31,228 --> 00:50:33,890
Fuck.

371
00:51:41,298 --> 00:51:42,856
Don't you knock first?

372
00:52:13,497 --> 00:52:14,987
One fried rice.

373
00:52:37,354 --> 00:52:38,844
Delivery.

374
00:54:24,427 --> 00:54:26,520
Ming, spread the menus.

375
00:54:26,630 --> 00:54:28,097
I don't have time today.

376
00:54:28,198 --> 00:54:30,359
You still have to do it.

377
00:55:36,599 --> 00:55:38,590
What's wrong with Ming today?

378
00:55:38,702 --> 00:55:40,533
Stop fighting with him!

379
00:55:41,171 --> 00:55:42,832
I'm not fighting with him.

380
00:55:43,773 --> 00:55:46,037
You were the same as him
when you first got here.

381
00:55:46,576 --> 00:55:49,704
Knew nothing.
Only thinking about the money.

382
00:55:49,813 --> 00:55:51,474
Just trying to be nice.

383
00:56:02,325 --> 00:56:04,088
I can take your picture.

384
00:56:04,194 --> 00:56:06,389
When you call me,
your picture will appear.

385
00:56:08,198 --> 00:56:10,689
- I don't know this brand.
- Your brand is shitty.

386
00:56:10,800 --> 00:56:12,495
I can't read it.

387
00:56:12,602 --> 00:56:14,797
My brand is better. Look. Picture.

388
00:56:14,904 --> 00:56:17,839
What's going on with you?
Did you fall into a puddle?

389
00:56:19,175 --> 00:56:20,802
Look at him.

390
00:56:54,811 --> 00:56:57,507
Ming, delivery. Next door.

391
00:57:39,789 --> 00:57:42,155
One hour to go.
You'll be fine.

392
00:57:42,759 --> 00:57:45,227
A boat straightens its course
when it gets to a bridge.

393
00:57:50,700 --> 00:57:52,292
It stopped raining.

394
00:58:28,171 --> 00:58:29,638
Two spring rolls.

395
00:58:31,040 --> 00:58:32,564
Fuck you.

396
01:00:57,453 --> 01:01:02,186
That bitch at 845 West End.

397
01:01:45,802 --> 01:01:48,600
Bald bastard, I'll slap you.

398
01:02:34,684 --> 01:02:37,209
One case of chicken.
Three cases of chicken wings.

399
01:02:37,320 --> 01:02:39,652
Two cases of broccoli.
Three pounds of bean sprouts.

400
01:02:39,756 --> 01:02:42,384
Three pounds of noodles.
One dozen scallions.

401
01:02:42,492 --> 01:02:43,959
One dozen packs of tofu.

402
01:02:44,060 --> 01:02:45,527
One hundred pounds of pork.

403
01:02:52,969 --> 01:02:54,436
Give me a cigarette.

404
01:03:01,144 --> 01:03:02,907
Where is Ming?

405
01:03:03,446 --> 01:03:04,913
I don't know.

406
01:03:05,014 --> 01:03:08,142
You've been yapping and smoking all day.
Don't you have deliveries?

407
01:03:08,885 --> 01:03:12,582
There's something wrong with my bike.
I have to get a new one tomorrow.

408
01:03:12,688 --> 01:03:15,054
Did he get himself into trouble?

409
01:03:19,028 --> 01:03:20,825
He has a big bruise. I saw it.

410
01:03:20,930 --> 01:03:22,591
Did he get into a fight?

411
01:03:23,132 --> 01:03:24,656
I don't know.

412
01:03:25,935 --> 01:03:27,402
How would I know?

413
01:03:27,503 --> 01:03:29,994
You're so close to him.
Why wouldn't you know?

414
01:03:31,240 --> 01:03:33,800
Since when do you care about Ming?

415
01:03:33,910 --> 01:03:35,502
Small broccoli chicken.

416
01:03:36,379 --> 01:03:38,574
Is he in trouble?

417
01:03:38,681 --> 01:03:40,512
It's none of your business.

418
01:03:40,616 --> 01:03:42,846
- Why do you have that attitude with me?
- Attitude?

419
01:03:42,952 --> 01:03:44,920
For three years,
I've had nothing but attitude from you.

420
01:03:45,021 --> 01:03:46,818
Don't talk to me about attitude.

421
01:03:53,696 --> 01:03:55,493
If Ming is in trouble...

422
01:03:55,598 --> 01:03:57,463
Hurry up, small broccoli chicken.

423
01:03:57,567 --> 01:04:00,058
Forget about it. There's no problem.

424
01:04:02,638 --> 01:04:04,196
Don't worry.

425
01:04:36,672 --> 01:04:39,072
Aren't you coming over to help?

426
01:05:09,138 --> 01:05:10,628
Fried rice.

427
01:05:15,177 --> 01:05:17,008
10 A, Next door.

428
01:05:17,113 --> 01:05:18,580
Number 250.

429
01:05:24,754 --> 01:05:27,882
Oh, this pack scared me.

430
01:06:40,696 --> 01:06:42,220
Who took the hot sauce?

431
01:06:43,032 --> 01:06:44,499
Not me.

432
01:06:44,600 --> 01:06:46,761
What the hell,
blaming me for everything.

433
01:06:46,869 --> 01:06:49,360
What's wrong with you?

434
01:07:32,882 --> 01:07:34,474
I told you.

435
01:07:35,017 --> 01:07:37,349
Things always work out.

436
01:07:38,788 --> 01:07:42,724
This is called, "A boat straightens
its course when it gets to a bridge."

437
01:07:43,259 --> 01:07:45,022
Do you agree with me?

438
01:07:45,127 --> 01:07:46,958
That's life.

439
01:07:47,063 --> 01:07:48,792
Philosophy.

440
01:08:11,787 --> 01:08:13,482
Sometimes...

441
01:08:14,390 --> 01:08:17,052
I wish I'd never come here.

442
01:08:18,427 --> 01:08:21,658
It would be great to be home
with my wife and my son.

443
01:08:25,034 --> 01:08:28,731
I got here before my son was born.

444
01:08:29,405 --> 01:08:31,737
He never saw his father.

445
01:08:38,547 --> 01:08:43,678
It will all pay off.

446
01:08:46,021 --> 01:08:47,648
I hope so.

447
01:09:07,076 --> 01:09:08,805
Don't think too much.

448
01:09:09,345 --> 01:09:11,472
Four years isn't that long.

449
01:09:12,848 --> 01:09:15,510
Look at me, I'm almost free of debt.

450
01:09:17,920 --> 01:09:21,822
I can bring my wife and kids here soon.

451
01:09:23,559 --> 01:09:25,754
You'll be okay, too.

452
01:09:26,662 --> 01:09:30,189
Time flies.

453
01:09:30,299 --> 01:09:31,994
Flies fast.

454
01:09:55,724 --> 01:09:57,624
Ming, your money.

455
01:09:57,726 --> 01:10:00,024
Young, your money.

456
01:10:27,723 --> 01:10:30,624
Young, only $15?

457
01:10:30,726 --> 01:10:32,193
That little?

458
01:10:32,294 --> 01:10:34,194
Ming, you made a lot of money.

459
01:10:34,296 --> 01:10:36,059
A lot of money.

460
01:12:12,027 --> 01:12:14,018
Ming, do you want to take this delivery?

461
01:12:21,737 --> 01:12:24,672
Two small broccoli chicken and a fried rice.

462
01:13:04,313 --> 01:13:06,508
- Noodle?
- No.

463
01:13:06,615 --> 01:13:08,207
- Wonton skin?
- We have that.

464
01:13:08,317 --> 01:13:11,081
- We don't have fries, right?
- Yes.

465
01:13:30,906 --> 01:13:33,033
We don't need flour? How about wine?

466
01:13:33,142 --> 01:13:34,803
Yes.

467
01:14:13,882 --> 01:14:16,316
Two bags of rice. Three tins of oil.

468
01:14:16,418 --> 01:14:19,251
One dozen bottles of wine.
One case of soy sauce.

469
01:14:19,354 --> 01:14:21,379
One case of flour.

470
01:14:21,490 --> 01:14:24,755
One case of MSG. Two cases of shrimp.

471
01:18:04,079 --> 01:18:05,808
- Are you going out?
- No, I don't want to.

472
01:18:05,914 --> 01:18:07,905
I've been in Chinatown
for more than 10 years.

473
01:18:08,016 --> 01:18:10,314
Not Chinatown.
You should go somewhere else.

474
01:18:10,419 --> 01:18:15,721
Like New Jersey or
the casinos in Connecticut.

475
01:18:15,824 --> 01:18:19,988
I don't have money for gambling.

476
01:18:20,095 --> 01:18:21,221
Gamble away.

477
01:19:32,501 --> 01:19:34,731
I told him I'm taking a day off.

478
01:19:36,772 --> 01:19:38,569
Is your daughter
also taking tomorrow off?

479
01:19:38,673 --> 01:19:40,231
Today and tomorrow.

480
01:19:41,409 --> 01:19:43,877
Her job is a little more relaxed.

481
01:19:43,979 --> 01:19:45,173
A little more relaxed.

482
01:19:45,280 --> 01:19:47,714
Working in a Japanese restaurant is easier.

483
01:19:48,250 --> 01:19:49,717
More money?

484
01:19:49,818 --> 01:19:52,412
Yeah, she makes more money.

485
01:19:52,521 --> 01:19:54,113
You should go work there then.

486
01:19:54,222 --> 01:19:55,689
I don't want to work there.

487
01:19:55,791 --> 01:19:58,692
- Why not?
- I don't like waitressing.

488
01:20:39,467 --> 01:20:40,957
Be here earlier tomorrow.

489
01:20:41,036 --> 01:20:42,503
How early?

490
01:20:42,604 --> 01:20:45,835
You have to make chicken halves
and chicken wings.

491
01:20:45,941 --> 01:20:47,408
Be here earlier tomorrow.

492
01:20:48,610 --> 01:20:51,170
Hey, Ming, shut the gate.

493
01:21:15,036 --> 01:21:16,503
Did you check the stoves and the lights?

494
01:21:16,605 --> 01:21:18,402
Yeah, everything is turned off.

495
01:21:18,506 --> 01:21:19,700
Okay. Let's go.

496
01:21:31,953 --> 01:21:34,945
- Good-bye.
- There goes another day.

497
01:21:36,057 --> 01:21:39,049
Tomorrow I'm off.
See you all the day after tomorrow.

498
01:21:39,160 --> 01:21:41,856
- Ming, are you coming?
- I have a stomachache. You go first.

499
01:21:41,963 --> 01:21:44,022
All right, hurry home and sleep.

500
01:21:44,132 --> 01:21:45,690
Don't think too much.
See you tomorrow.

501
01:22:35,483 --> 01:22:36,108
Hey.

502
01:22:38,954 --> 01:22:40,819
How much do you need?

503
01:22:40,922 --> 01:22:43,891
- What?
- How much do you need?

504
01:23:36,478 --> 01:23:37,945
How did you know?

505
01:23:38,046 --> 01:23:39,513
It wasn't hard to figure out.

506
01:23:43,018 --> 01:23:44,849
I'll pay you back.

507
01:23:44,953 --> 01:23:47,251
Don't worry.
Take your time.

508
01:23:47,355 --> 01:23:49,186
I've been there.
I know what it's like.

509
01:23:55,063 --> 01:23:58,123
All right, see you tomorrow.
I'm headed to the subway.

