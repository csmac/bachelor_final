1
00:01:04,520 --> 00:01:08,433
OUR FATHER

2
00:03:44,200 --> 00:03:45,679
Stop bugging me!

3
00:04:03,400 --> 00:04:06,073
- Calm down.
- Please, Tahir.

4
00:04:44,400 --> 00:04:45,958
Water's cut.

5
00:05:14,000 --> 00:05:15,069
What?

6
00:05:16,200 --> 00:05:19,112
That's strange.
Dad isn't in his room.

7
00:05:20,000 --> 00:05:22,309
Maybe he went
to buy cigarettes.

8
00:05:37,080 --> 00:05:40,629
He's always late, your Dad.
It was the same last time.

9
00:05:40,840 --> 00:05:43,559
- Don't talk about my Dad like that.
- It's true.

10
00:05:43,960 --> 00:05:45,439
Stop it, Amine.

11
00:05:46,760 --> 00:05:48,193
Forget it.

12
00:05:50,880 --> 00:05:52,632
I'm not playing with you anymore.

13
00:06:17,120 --> 00:06:20,430
Let's go to the field.
With their Dad there's no point.

14
00:06:46,760 --> 00:06:48,034
Hello, Mum.

15
00:06:52,560 --> 00:06:56,155
Where's Dad? He's supposed
to referee our football match.

16
00:06:57,080 --> 00:06:58,479
Say something!

17
00:07:17,640 --> 00:07:19,153
There's no water.

18
00:07:38,520 --> 00:07:39,873
What else?

19
00:07:42,640 --> 00:07:44,198
What do you want?

20
00:07:47,320 --> 00:07:49,788
I'm sick and tired of your father.

21
00:07:52,720 --> 00:07:55,393
Your much-loved father has left.

22
00:07:58,440 --> 00:08:01,238
What on earth did I do
to deserve this?

23
00:08:05,600 --> 00:08:08,672
Your father is irresponsible.

24
00:08:09,400 --> 00:08:11,277
Yes. Irresponsible!

25
00:08:39,960 --> 00:08:44,238
It's Saturday. Dad knew
he had to referee our game.

26
00:08:44,760 --> 00:08:46,876
Where do you think he's gone?

27
00:08:47,360 --> 00:08:50,636
I don't know.
He can't go far. He'll come back.

28
00:08:54,080 --> 00:08:55,798
What's "irresponsible"?

29
00:08:56,680 --> 00:08:58,398
- What?
- "Irresponsible"!

30
00:08:58,960 --> 00:09:00,791
Can't you shut up?

31
00:09:23,760 --> 00:09:27,355
Here it's the border
between Chad and Cameroon.

32
00:09:28,080 --> 00:09:31,277
Over this bridge,
you're already elsewhere...

33
00:09:41,840 --> 00:09:46,311
Everyone who doesn't fly
goes by here.

34
00:09:47,240 --> 00:09:49,390
Maybe our father is one of them.

35
00:11:05,280 --> 00:11:06,429
Does it hurt?

36
00:11:07,760 --> 00:11:09,557
Should I take you to the hospital?

37
00:11:11,040 --> 00:11:13,076
If it hurts, we have to go.

38
00:11:18,560 --> 00:11:19,993
Stop crying.

39
00:11:22,400 --> 00:11:23,833
Button up your shirt.

40
00:11:52,600 --> 00:11:55,239
Do you know what time it is?

41
00:11:57,040 --> 00:11:58,792
I haven't seen you all morning.

42
00:11:59,000 --> 00:12:00,991
You didn't think of me.

43
00:12:01,880 --> 00:12:03,313
Is that it?

44
00:12:04,200 --> 00:12:05,679
Listen to me, you!

45
00:12:06,960 --> 00:12:10,270
You disappear like that.
You don't think about me.

46
00:12:10,480 --> 00:12:12,630
You leave me here all alone.

47
00:12:12,840 --> 00:12:14,717
I don't matter at all to you.

48
00:12:15,000 --> 00:12:17,560
Do you want to leave?
Like your father?

49
00:12:17,760 --> 00:12:18,795
So go.

50
00:12:19,000 --> 00:12:21,468
Leave me.
I'll stay all alone.

51
00:12:21,680 --> 00:12:25,229
Since I don't matter at all to you,
I don't want to see you anymore.

52
00:13:35,920 --> 00:13:37,433
Irresponsible:

53
00:13:37,640 --> 00:13:40,871
One who is not legally responsible.

54
00:13:41,080 --> 00:13:43,992
Doesn't have to answer
for his acts.

55
00:13:44,200 --> 00:13:48,273
Who cannot be held
morally responsible.

56
00:13:48,480 --> 00:13:50,038
Who acts lightly.

57
00:13:50,240 --> 00:13:51,514
Did you understand?

58
00:13:52,840 --> 00:13:54,159
Of course!

59
00:13:54,960 --> 00:13:57,633
It's someone who isn't responsible.

60
00:13:59,400 --> 00:14:02,392
So, Dad's not responsible
for leaving?

61
00:14:11,160 --> 00:14:13,037
Can you read me the book?

62
00:14:13,240 --> 00:14:14,878
It's Dad who reads it to you...

63
00:14:15,080 --> 00:14:16,638
Please, big brother...

64
00:14:17,520 --> 00:14:21,149
- I'm not Dad.
- Please! Read it to me!

65
00:14:23,600 --> 00:14:24,874
All right then.

66
00:14:26,000 --> 00:14:27,399
Thanks.

67
00:14:30,560 --> 00:14:34,235
Once when I was six
I saw a magnificent picture

68
00:14:34,440 --> 00:14:38,115
in a book called
True Stories from Nature.

69
00:15:41,880 --> 00:15:42,995
Tahir.

70
00:15:47,280 --> 00:15:48,838
What do you want now?

71
00:15:49,360 --> 00:15:51,430
My chest hurts.

72
00:15:53,440 --> 00:15:55,317
It's hard to breathe.

73
00:15:58,400 --> 00:16:00,072
I'll go get Mum.

74
00:16:33,160 --> 00:16:34,593
What's wrong?

75
00:16:36,720 --> 00:16:39,109
What is it?

76
00:16:40,240 --> 00:16:41,593
Has Dad come back?

77
00:16:43,960 --> 00:16:44,631
So?

78
00:16:44,840 --> 00:16:45,989
Has he come back?

79
00:16:50,040 --> 00:16:51,029
Come with me.

80
00:17:24,960 --> 00:17:26,916
Amine is having an asthma attack.

81
00:17:27,600 --> 00:17:30,672
I'm going to give him an injection
so he can sleep tonight,

82
00:17:30,880 --> 00:17:33,110
and a prescription
for some medicine.

83
00:17:39,320 --> 00:17:40,719
All done.

84
00:18:04,520 --> 00:18:06,511
Achta, how is your husband?

85
00:18:06,720 --> 00:18:08,392
- My father...
- Be quiet...

86
00:18:08,640 --> 00:18:10,119
Did we speak to you?

87
00:18:11,320 --> 00:18:12,594
He's well.

88
00:18:13,800 --> 00:18:15,791
- And how's school?
- It's OK.

89
00:18:17,120 --> 00:18:21,318
Good luck. Come back in a week
to see how you're doing.

90
00:18:22,840 --> 00:18:23,989
Thank you.

91
00:18:24,200 --> 00:18:25,315
Goodbye.

92
00:19:04,840 --> 00:19:07,274
- Hello, Uncle Adoum.
- How are you?

93
00:19:07,520 --> 00:19:08,839
Sit down.

94
00:19:10,960 --> 00:19:12,473
Everything OK at home?

95
00:19:14,760 --> 00:19:15,636
It's not OK?

96
00:19:15,840 --> 00:19:17,398
You don't look very happy!

97
00:19:18,160 --> 00:19:19,070
Listen to this.

98
00:19:19,280 --> 00:19:21,999
It's my latest composition.

99
00:19:22,200 --> 00:19:23,952
Tell me what you think.

100
00:19:49,840 --> 00:19:52,149
No rest with you,
even Sunday!

101
00:19:52,360 --> 00:19:55,272
Easy when you don't even work!
Good-for-nothing!

102
00:19:56,240 --> 00:19:59,835
I told you:
We don't like artists here.

103
00:20:48,280 --> 00:20:51,590
But your father hasn't worked here
for a long time.

104
00:20:52,720 --> 00:20:54,278
He didn't say anything?

105
00:20:57,320 --> 00:20:58,435
Henriette?

106
00:21:01,520 --> 00:21:04,080
How long has it been since Mr Brahim
has worked here?

107
00:21:04,280 --> 00:21:06,555
Two years and three months I think.

108
00:21:07,360 --> 00:21:08,509
Sorry.

109
00:21:22,800 --> 00:21:25,598
He lied to us for two years.

110
00:21:29,000 --> 00:21:31,912
Everyday: "I'm going to work."

111
00:21:32,960 --> 00:21:34,393
All those lies!

112
00:22:32,560 --> 00:22:33,788
Want to go for a Coke?

113
00:22:34,000 --> 00:22:34,989
Coke?

114
00:22:35,200 --> 00:22:37,031
Let's go see a film instead.

115
00:23:26,760 --> 00:23:27,988
It's Dad...

116
00:23:40,720 --> 00:23:41,948
It's him...

117
00:23:42,680 --> 00:23:43,954
I swear...

118
00:23:44,840 --> 00:23:46,353
Look at his back.

119
00:23:48,160 --> 00:23:49,878
Dad, it's me, Amine...

120
00:23:50,840 --> 00:23:52,353
Look at me...

121
00:23:52,840 --> 00:23:54,478
Please, Dad.

122
00:23:57,880 --> 00:23:59,313
Hello, kids!

123
00:24:00,520 --> 00:24:01,839
How are you?

124
00:24:05,680 --> 00:24:06,908
Did you have fun?

125
00:24:31,920 --> 00:24:33,319
Off to bed!

126
00:24:51,120 --> 00:24:52,678
Do you know

127
00:24:53,040 --> 00:24:55,759
why roosters crow all the time?

128
00:24:57,160 --> 00:24:58,309
I don't know.

129
00:24:59,400 --> 00:25:01,960
They don't want us
to hear them farting...

130
00:25:02,160 --> 00:25:03,912
Roosters are very proud!

131
00:25:09,200 --> 00:25:10,997
Here's one that even farts at night!

132
00:25:48,600 --> 00:25:51,910
Once when I was six
I saw

133
00:25:52,960 --> 00:25:56,475
a magnificent picture in a book
about the primeval forest

134
00:25:57,240 --> 00:25:59,196
called True Stories from Nature.

135
00:26:00,920 --> 00:26:04,799
It was of a boa constrictor
swallowing an animal.

136
00:26:06,080 --> 00:26:08,355
Here is a copy of the drawing.

137
00:26:09,520 --> 00:26:13,035
In the book it said:
"Boa constrictors..."

138
00:26:26,480 --> 00:26:27,629
Are you OK, Tahir?

139
00:26:27,840 --> 00:26:29,432
Yes, I'm OK.

140
00:26:29,960 --> 00:26:31,234
Not coming to school?

141
00:26:31,440 --> 00:26:32,475
I am.

142
00:26:41,600 --> 00:26:43,477
Do you feel like going?

143
00:26:48,880 --> 00:26:49,710
Come on, let's go.

144
00:28:54,800 --> 00:28:57,678
Are you sure it's the right reel?

145
00:28:59,000 --> 00:29:00,956
Yes. It's written on it.

146
00:29:02,120 --> 00:29:03,439
Where?

147
00:29:10,200 --> 00:29:12,509
There's too much.
I've had enough.

148
00:29:16,800 --> 00:29:18,597
What are you doing there?

149
00:29:20,240 --> 00:29:21,958
You aren't at school?

150
00:29:22,160 --> 00:29:23,593
Teachers are on strike.

151
00:29:23,800 --> 00:29:25,950
They haven't been paid.

152
00:29:28,280 --> 00:29:29,599
And what's that?

153
00:29:29,800 --> 00:29:31,677
It's my friend...

154
00:29:33,080 --> 00:29:34,559
He lent it to me...

155
00:30:10,520 --> 00:30:11,714
Captain,

156
00:30:12,560 --> 00:30:14,710
forgive them.

157
00:30:15,360 --> 00:30:16,679
It's too much for me.

158
00:30:16,880 --> 00:30:18,472
It's the first time.

159
00:30:18,680 --> 00:30:20,511
They won't do it again.

160
00:30:23,000 --> 00:30:24,479
Say something!

161
00:30:26,680 --> 00:30:28,636
What's in there?

162
00:30:28,840 --> 00:30:30,398
What were you looking for?

163
00:30:31,000 --> 00:30:33,230
In any case, Madam,

164
00:30:33,840 --> 00:30:36,354
it's the first and last time.

165
00:30:37,440 --> 00:30:38,156
Listen!

166
00:30:39,920 --> 00:30:41,638
Your mother interceded for you.

167
00:30:41,840 --> 00:30:44,400
Do you know what you deserve?

168
00:30:45,600 --> 00:30:46,828
A beating!

169
00:30:47,600 --> 00:30:49,556
But your mother intervened,

170
00:30:50,240 --> 00:30:51,912
so I'm letting you go.

171
00:31:07,320 --> 00:31:08,912
Shame on me.

172
00:31:09,280 --> 00:31:11,953
A husband who leaves...
Children who steal.

173
00:31:12,160 --> 00:31:13,559
What disgrace!

174
00:31:32,480 --> 00:31:34,198
Tahir, Amine,

175
00:31:34,920 --> 00:31:36,069
my children.

176
00:31:36,760 --> 00:31:38,034
I love you.

177
00:31:38,240 --> 00:31:39,593
Forgive me.

178
00:31:39,960 --> 00:31:40,949
I'm fed up.

179
00:31:42,400 --> 00:31:46,313
You'll be in a place
that's not so bad.

180
00:31:47,880 --> 00:31:49,233
You'll be well-educated

181
00:31:49,440 --> 00:31:51,908
to become good boys.

182
00:33:28,000 --> 00:33:29,831
Don't worry Madam.

183
00:33:30,040 --> 00:33:33,953
I'll look after them well.
I'll make them into good people.

184
00:33:44,160 --> 00:33:45,354
My little one.

185
00:33:47,960 --> 00:33:49,916
Be patient.
God is looking over you!

186
00:33:50,120 --> 00:33:51,872
Nothing can happen to you here!

187
00:33:52,080 --> 00:33:54,036
I'll come and visit you often.

188
00:33:54,240 --> 00:33:55,514
Go on.

189
00:34:51,320 --> 00:34:53,117
Amine, come here.

190
00:34:58,960 --> 00:35:01,633
Why aren't you eating
with the others?

191
00:35:02,760 --> 00:35:03,988
Are you angry?

192
00:35:07,320 --> 00:35:08,469
Go on.

193
00:35:33,800 --> 00:35:36,633
Why did your mother
put you in here?

194
00:35:37,760 --> 00:35:39,193
It's hard!

195
00:35:39,440 --> 00:35:40,873
Too much suffering!

196
00:35:41,080 --> 00:35:42,672
And we are well off!

197
00:35:42,880 --> 00:35:44,313
In other Koran schools,

198
00:35:44,520 --> 00:35:49,674
children have to bring
money to the teachers.

199
00:35:51,320 --> 00:35:52,799
Real slavery!

200
00:37:51,400 --> 00:37:52,549
Be quiet!

201
00:37:53,680 --> 00:37:56,353
Let us pray for the new arrivals.

202
00:37:57,360 --> 00:38:00,955
May God make them into men
of peace and tolerance.

203
00:38:01,840 --> 00:38:04,593
And may He remove hatred
from their hearts.

204
00:38:05,440 --> 00:38:08,432
And may He guide them
on the road of Goodness.

205
00:38:35,160 --> 00:38:36,639
Amine, come here.

206
00:38:44,040 --> 00:38:45,314
Come closer.

207
00:38:49,280 --> 00:38:51,475
Eat, it's corn meal.

208
00:39:01,000 --> 00:39:02,115
Eat!

209
00:39:02,480 --> 00:39:04,835
You look like my son
who drowned.

210
00:39:07,800 --> 00:39:09,358
His name was Moussa.

211
00:39:16,280 --> 00:39:17,429
Come closer.

212
00:39:24,760 --> 00:39:25,954
Go away.

213
00:39:28,680 --> 00:39:30,113
Go on.

214
00:40:02,680 --> 00:40:04,318
Amine, you aren't sleeping?

215
00:40:06,080 --> 00:40:07,798
It's my asthma again.

216
00:40:11,000 --> 00:40:12,319
What's that?

217
00:40:13,000 --> 00:40:14,319
My medicine.

218
00:40:24,880 --> 00:40:28,759
Time for bed.
We get up early tomorrow.

219
00:40:29,080 --> 00:40:31,071
Can you read me this book?

220
00:41:04,000 --> 00:41:05,274
What's your name?

221
00:41:05,480 --> 00:41:07,914
Listen, I'm speaking.

222
00:41:38,640 --> 00:41:41,712
I'm sick of this hole.
Have to do everything to get out.

223
00:41:41,920 --> 00:41:43,239
Right!

224
00:41:43,440 --> 00:41:46,591
Have to get out. No matter what.

225
00:42:40,800 --> 00:42:43,189
Amine, you don't swim?

226
00:42:45,600 --> 00:42:46,316
So...

227
00:42:47,960 --> 00:42:49,439
He doesn't swim!

228
00:42:50,040 --> 00:42:51,268
I don't believe it!

229
00:42:52,800 --> 00:42:54,074
Shame on you!

230
00:43:25,000 --> 00:43:26,558
Come here. Where are you?

231
00:43:29,200 --> 00:43:30,713
Get out of there.

232
00:43:47,960 --> 00:43:50,758
In the name of God, stop.
That's enough!

233
00:44:19,280 --> 00:44:20,793
Take your medicine.

234
00:44:22,360 --> 00:44:23,554
Come on!

235
00:44:34,200 --> 00:44:37,795
He got you good, the teacher!
You have bruises everywhere.

236
00:44:42,360 --> 00:44:44,430
I don't want to stay here anymore.

237
00:44:45,840 --> 00:44:47,910
Neither do I, you know.

238
00:44:52,240 --> 00:44:54,595
It's getting late.

239
00:44:56,240 --> 00:44:59,437
We have to go to bed.
Tomorrow we get up early,

240
00:44:59,640 --> 00:45:02,359
and we're going to look for Dad.

241
00:45:02,560 --> 00:45:03,834
Good night.

242
00:45:10,920 --> 00:45:12,433
Here - your medecine.

243
00:45:34,560 --> 00:45:36,039
Where are they?

244
00:45:43,040 --> 00:45:44,598
I'm talking to you.

245
00:45:51,880 --> 00:45:53,438
They won't get away from me.

246
00:47:28,560 --> 00:47:29,879
It's honey.

247
00:47:53,200 --> 00:47:54,679
How do you feel?

248
00:47:54,920 --> 00:47:56,592
It hurts everywhere.

249
00:47:59,240 --> 00:48:01,276
As soon as the teacher lets me go,

250
00:48:02,200 --> 00:48:04,634
we're going to look for Dad.

251
00:48:05,200 --> 00:48:07,668
I'll run fast
and I'll avoid thorns.

252
00:48:18,000 --> 00:48:19,513
Who's that girl?

253
00:48:20,240 --> 00:48:22,800
I know her, she's a deaf-mute.

254
00:49:37,480 --> 00:49:38,595
Tahir,

255
00:49:39,480 --> 00:49:42,677
I want you to swear
never to run away again.

256
00:49:43,360 --> 00:49:44,793
I can let you go, you know.

257
00:49:45,000 --> 00:49:47,992
But I don't trust you anymore.

258
00:49:48,760 --> 00:49:51,354
If you swear,
you won't dare to do it again.

259
00:49:51,680 --> 00:49:54,069
If you lie,
your soul will be damned.

260
00:49:54,280 --> 00:49:57,352
And even after you die,
it will continue to wander.

261
00:49:57,760 --> 00:49:58,875
Do you understand?

262
00:49:59,080 --> 00:50:02,311
If you don't accept,
you'll stay tied up a month.

263
00:50:13,520 --> 00:50:16,193
Tahir, now that you are free,

264
00:50:16,400 --> 00:50:19,073
tomorrow, we get up and we leave.

265
00:50:20,920 --> 00:50:23,115
Tahir, look at me.

266
00:50:23,320 --> 00:50:25,151
Tahir, you agree?

267
00:50:30,040 --> 00:50:31,314
Agree?

268
00:50:31,640 --> 00:50:33,790
Tahir, why don't you answer?

269
00:51:12,560 --> 00:51:13,709
So, are you OK?

270
00:51:14,720 --> 00:51:16,915
- Where is Tahir?
- He's coming.

271
00:51:18,840 --> 00:51:20,319
Are you both OK?

272
00:51:20,520 --> 00:51:22,988
- Your motorbike is great!
- It's not mine.

273
00:51:23,200 --> 00:51:24,838
Tahir, are you OK?

274
00:51:28,560 --> 00:51:30,630
- And the others?
- Everyone is fine.

275
00:51:30,840 --> 00:51:31,909
And Mum?

276
00:51:32,960 --> 00:51:36,111
She's a bit tired but...
she's thinking of you.

277
00:51:36,320 --> 00:51:37,435
Did Dad come back?

278
00:51:37,960 --> 00:51:42,351
No, but he sent this.
He'll come back.

279
00:51:43,200 --> 00:51:44,838
You were talking about this?

280
00:51:45,040 --> 00:51:48,077
A friend lent it to me

281
00:51:48,280 --> 00:51:49,998
to come to see you.

282
00:51:50,400 --> 00:51:53,198
- Want to go for a ride?
- Now?

283
00:52:23,160 --> 00:52:25,799
He wrote to me too.
He's in Tangiers.

284
00:52:26,000 --> 00:52:27,353
Where's that?

285
00:52:30,440 --> 00:52:32,158
Is that where Dad is?

286
00:52:33,640 --> 00:52:35,278
The sea is beautiful!

287
00:52:35,480 --> 00:52:37,152
Yes, that's Tangiers.

288
00:52:37,960 --> 00:52:39,188
In Morocco.

289
00:52:45,240 --> 00:52:46,753
Is it far from here?

290
00:52:49,000 --> 00:52:51,230
Can we go there on foot?

291
00:52:51,480 --> 00:52:52,629
On foot?

292
00:53:02,800 --> 00:53:04,438
Is Mum OK?

293
00:53:04,640 --> 00:53:06,392
She's fine.

294
00:53:06,600 --> 00:53:08,158
She sends her love.

295
00:53:42,480 --> 00:53:44,391
Get out! Out!

296
00:53:55,160 --> 00:53:56,559
Isn't it beautiful?

297
00:53:56,760 --> 00:53:58,398
That's where Dad is!

298
00:55:21,240 --> 00:55:23,071
Tahir, what's going on?

299
00:55:39,520 --> 00:55:41,715
Tahir, tomorrow we're leaving.

300
00:55:47,520 --> 00:55:48,873
OK, Tahir?

301
00:55:49,080 --> 00:55:50,308
I'm not leaving anymore.

302
00:55:50,520 --> 00:55:51,714
Why?

303
00:55:51,920 --> 00:55:53,558
Why don't you want to anymore?

304
00:55:57,160 --> 00:55:58,673
What's wrong with you?

305
00:56:06,800 --> 00:56:08,870
Answer! Why won't you go?

306
00:56:13,040 --> 00:56:15,076
Why did you change your mind?

307
00:56:15,600 --> 00:56:16,749
Are you afraid?

308
00:56:19,760 --> 00:56:21,512
You don't want to see Dad again?

309
00:56:22,160 --> 00:56:24,674
If he loved us,
he wouldn't have left.

310
01:00:06,960 --> 01:00:08,473
Do you feel better, Amine?

311
01:00:14,760 --> 01:00:16,318
Did you take your medicine?

312
01:00:20,600 --> 01:00:21,715
Why?

313
01:00:22,880 --> 01:00:24,154
I can't find it.

314
01:00:24,360 --> 01:00:25,588
What?

315
01:00:32,720 --> 01:00:35,234
Lmpossible,
it can't have just disappeared.

316
01:00:52,080 --> 01:00:54,640
Who's seen Amine's medicine?

317
01:00:55,600 --> 01:00:56,953
Where is his medicine?

318
01:01:00,920 --> 01:01:02,319
I'm talking to you!

319
01:01:11,840 --> 01:01:13,398
Stubborn ass!

320
01:01:24,720 --> 01:01:27,075
Hassan, tell the truth.

321
01:01:40,560 --> 01:01:41,788
Have some honey.

322
01:01:42,120 --> 01:01:43,872
It's good for your lungs.

323
01:01:56,800 --> 01:02:00,156
Do you think Dad loves us?

324
01:02:02,440 --> 01:02:03,509
And Mum?

325
01:02:03,720 --> 01:02:05,153
Yes, she loves us.

326
01:02:08,440 --> 01:02:10,590
So why are we suffering?

327
01:02:13,520 --> 01:02:15,431
Can you read me the book?

328
01:02:25,040 --> 01:02:27,076
Why do you like that book so much?

329
01:02:27,600 --> 01:02:31,036
Because
I always fall asleep before the end.

330
01:02:31,240 --> 01:02:32,389
But tonight,

331
01:02:32,600 --> 01:02:35,478
I'll listen until the end.

332
01:02:39,400 --> 01:02:41,709
Once when I was six,
I saw

333
01:02:42,120 --> 01:02:45,430
a magnificent picture in a book
about the primeval forest

334
01:02:45,640 --> 01:02:47,676
called True Stories from Nature.

335
01:02:48,560 --> 01:02:51,996
It was of a boa constrictor
swallowing an animal.

336
01:02:52,520 --> 01:02:54,431
Here is a copy of the drawing.

337
01:02:55,120 --> 01:02:56,951
In the book it said:

338
01:02:57,160 --> 01:02:59,549
Boa constrictors
swallow their prey whole,

339
01:02:59,760 --> 01:03:00,875
without chewing it.

340
01:03:01,640 --> 01:03:03,676
After that they are not able to move

341
01:03:04,520 --> 01:03:07,034
and they sleep through
the six months of digestion.

342
01:05:48,040 --> 01:05:49,712
You're running away, aren't you?

343
01:05:52,520 --> 01:05:55,114
Don't take the same road again.

344
01:05:55,320 --> 01:05:57,038
Go by the river.

345
01:06:12,840 --> 01:06:14,114
Forgive me.

346
01:08:38,480 --> 01:08:39,879
Do you want to come with me?

347
01:08:57,960 --> 01:08:58,836
Me,

348
01:08:59,800 --> 01:09:01,358
I'm fifteen.

349
01:09:01,840 --> 01:09:02,955
And you?

350
01:09:07,640 --> 01:09:08,868
Twenty?

351
01:09:11,680 --> 01:09:13,079
But you're old!

352
01:10:35,120 --> 01:10:36,189
You and me

353
01:10:36,960 --> 01:10:38,473
will never be separated.

354
01:12:33,240 --> 01:12:36,277
What's happening to your mother
is rather unusual.

355
01:12:36,480 --> 01:12:38,835
She'll come through all right.

356
01:12:39,040 --> 01:12:41,031
We'll keep her for a while.

357
01:12:41,280 --> 01:12:44,590
She needs rest
and medical attention.

358
01:12:44,800 --> 01:12:45,755
With crazy people?

359
01:12:45,960 --> 01:12:48,076
No, not with crazy people.

360
01:12:48,360 --> 01:12:50,157
She isn't crazy.

361
01:12:50,360 --> 01:12:53,477
You should come and see her often.
Don't leave her alone.

362
01:13:37,800 --> 01:13:39,199
What are you doing there?

363
01:13:56,640 --> 01:13:58,949
Nothing's more important
than freedom.

364
01:13:59,160 --> 01:14:01,151
Mum, you're free now.

