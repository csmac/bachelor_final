1
00:04:12,118 --> 00:04:15,246
...we the members
of the People's Council,

2
00:04:15,822 --> 00:04:19,758
representing the Jews of Israel

3
00:04:19,826 --> 00:04:23,091
and the Zionist movement,

4
00:04:23,930 --> 00:04:27,889
are here assembled on the day
of the end of the British mandate.

5
00:04:28,801 --> 00:04:32,362
By virtue
of our natural and historic right,

6
00:04:33,072 --> 00:04:37,202
and based
on the United Nations resolution,

7
00:04:37,977 --> 00:04:40,309
we do hereby declare

8
00:04:40,847 --> 00:04:44,840
the establishment of a Jewish state
in the land of Israel

9
00:04:44,917 --> 00:04:47,408
to be known as the state of Israel.

10
00:05:55,321 --> 00:05:57,050
Who are you? Why are you here?

11
00:05:58,491 --> 00:06:01,221
We were attacked last night.
Bombs, fire.

12
00:06:02,362 --> 00:06:05,525
My mother pushed us
out the back door.

13
00:06:05,598 --> 00:06:07,361
We couldn't see.
Too much smoke.

14
00:06:07,667 --> 00:06:10,158
We called to each other.
Houses on fire.

15
00:06:10,236 --> 00:06:12,101
We heard shooting, bombs.

16
00:06:12,171 --> 00:06:14,731
Mohammed said
he saw tanks and soldiers.

17
00:06:15,041 --> 00:06:16,599
We ran and ran.

18
00:06:16,676 --> 00:06:18,337
And that's when Abdullah fell.

19
00:06:18,411 --> 00:06:20,971
Hamid said soldiers
came into his house.

20
00:06:21,047 --> 00:06:22,947
Killed his grandfather,
father and mother.

21
00:06:23,015 --> 00:06:24,676
His sisters, too.

22
00:06:24,751 --> 00:06:28,050
We're all from Deir Yassin.
Soldiers shoved us in a truck.

23
00:06:28,388 --> 00:06:29,650
Lots of smoke.

24
00:06:29,722 --> 00:06:30,814
People died.

25
00:06:30,890 --> 00:06:33,188
I don't know where my brothers are.

26
00:06:33,259 --> 00:06:34,988
Everybody died.

27
00:06:36,529 --> 00:06:37,655
What is your name?

28
00:06:37,730 --> 00:06:38,719
Zeina.

29
00:07:18,304 --> 00:07:20,067
Mama! Hello!

30
00:07:20,139 --> 00:07:21,766
Hello, Hind.

31
00:07:51,003 --> 00:07:53,733
Mama, don't worry.
They'll only stay a few days.

32
00:07:54,340 --> 00:07:57,070
Why don't you take them
to your grandfather's?

33
00:12:43,629 --> 00:12:44,789
One times one?

34
00:13:09,789 --> 00:13:10,847
Girls!

35
00:13:10,923 --> 00:13:13,687
All together and very politely,

36
00:13:13,759 --> 00:13:15,021
nice and loud:

37
00:13:15,094 --> 00:13:17,619
"Hello, Colonel Smith!"

38
00:14:23,863 --> 00:14:26,127
- Hi, Jamal.
- Hello, Miss Hind.

39
00:20:01,900 --> 00:20:03,595
Hey, beautiful.

40
00:20:03,669 --> 00:20:05,694
Can I buy you a drink?

41
00:20:12,377 --> 00:20:13,844
I don't drink.

42
00:20:14,413 --> 00:20:16,381
Your Hebrew's great.

43
00:20:18,183 --> 00:20:19,241
Where you from?

44
00:20:20,519 --> 00:20:22,111
What are you, stupid?

45
00:20:22,187 --> 00:20:24,212
I don't want to talk to you.

46
00:20:24,289 --> 00:20:25,381
Get lost.

47
00:20:25,457 --> 00:20:27,254
Go sit down or get out.

48
00:20:29,294 --> 00:20:30,488
She your woman?

49
00:20:30,562 --> 00:20:31,722
Out!

50
00:20:31,797 --> 00:20:33,492
Why do you let her dance?

51
00:21:51,176 --> 00:21:52,700
What are you staring at?

52
00:21:52,778 --> 00:21:54,075
A whore. An Arab whore.

53
00:26:39,731 --> 00:26:42,598
We can't wait for outside help.
No one will help us.

54
00:26:42,934 --> 00:26:45,960
We must do this ourselves.
This war is not over yet.

55
00:30:00,098 --> 00:30:02,430
I'm fine.
Take care of each other.

56
00:30:02,500 --> 00:30:04,161
Take care of yourself.

57
00:30:38,336 --> 00:30:40,770
Congratulations
to the husband and wife!

58
00:32:11,095 --> 00:32:12,392
Mama.

59
00:32:12,463 --> 00:32:15,125
Can I eat your kunafeh?

60
00:41:08,999 --> 00:41:10,091
Look at me.

61
00:41:10,167 --> 00:41:11,156
Daddy!

62
00:41:12,703 --> 00:41:14,102
Listen.

63
00:41:14,972 --> 00:41:17,202
Miral, your father will be back Friday.

64
00:41:17,274 --> 00:41:20,107
I want my daddy!

65
00:41:24,849 --> 00:41:27,283
I want to go home.

66
00:45:29,560 --> 00:45:32,495
You have 10 minutes to clear this area!

67
00:45:32,963 --> 00:45:35,193
This house will be destroyed.

68
00:45:35,265 --> 00:45:37,995
You have 10 minutes to clear the area!

69
00:45:38,068 --> 00:45:40,161
This house will be destroyed!

70
00:45:42,473 --> 00:45:44,771
It's a threat to national security!
Get out!

71
01:01:41,731 --> 01:01:43,130
Where are you going?

72
01:02:03,686 --> 01:02:05,313
It all looks the same.

73
01:02:09,392 --> 01:02:11,257
I can't read Arabic.
Can you?

74
01:02:12,428 --> 01:02:14,055
Just take one.

75
01:05:51,247 --> 01:05:52,771
Police! Open up!

76
01:06:24,146 --> 01:06:25,613
I am Miral Shahin.

77
01:17:21,703 --> 01:17:23,136
You translate for her!

78
01:21:11,533 --> 01:21:13,194
Where are you going, girls?

79
01:21:13,268 --> 01:21:14,257
Jerusalem.

80
01:21:16,905 --> 01:21:17,963
OK, go.

81
01:29:58,459 --> 01:29:59,926
Allah be with his soul.

82
01:30:00,428 --> 01:30:01,861
He was a good man.

83
01:30:01,929 --> 01:30:05,330
He lent this to me.
I return it to you.

