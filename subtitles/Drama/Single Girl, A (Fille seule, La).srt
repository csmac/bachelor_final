00:00:35:GIRL ON THE BRIDGE
00:01:42:Go on, Adele. Tell us.
00:01:45:Well...
00:01:47:I'm...
00:01:51:You're twenty-two...
00:01:53:No, I will be in two months.
00:01:56:And you dropped out of school|very young to start work.
00:02:01:Yes, but not really to start work.
00:02:03:I'd met someone I wanted to be with.
00:02:06:That's why I dropped...
00:02:09:I left home.
00:02:12:I preferred to live with a boy,|instead of my folks
00:02:15:so I grabbed the first available one.|Opportunity, I mean.
00:02:20:You needed to be free.
00:02:22:I don't know about free.
00:02:25:All I really wanted|was to sleep with him.
00:02:28:When I was a kid, I used to think
00:02:32:Iife starts when you make love.|Till then, you're nothing.
00:02:36:So I took off|with the first willing guy
00:02:39:so we could be together
00:02:41:and my life could start.
00:02:43:The problem was,|it wasn't a very good start.
00:02:47:Didn't you get along?
00:02:49:Why wasn't it a good start?
00:02:52:It's never good for me.|Things go from bad to worse.
00:02:56:I never pick the lucky number.
00:02:59:You know those curly,|sticky flypapers?
00:03:02:I'm like them.
00:03:04:I pick up all the crud around.
00:03:07:I'm like a vacuum cleaner,|picking up all the dirt left behind.
00:03:13:I never pick the lucky number.
00:03:17:Everything I try goes wrong.
00:03:20:Everything I touch turns sour.
00:03:22:How do you explain that?
00:03:24:You can't explain bad luck. It's like...
00:03:28:an ear for music.|You have it or you don't.
00:03:32:What happened with the boy?
00:03:35:Which one?
00:03:37:The first one.|The one you took off with.
00:03:41:Didn't it go all the way?
00:03:44:Sure it went all the way.
00:03:48:But you were disappointed.
00:03:51:Not at all! That's the problem.
00:03:55:If I'd enjoyed it less,|I might not be here now.
00:03:58:Anyhow.
00:04:00:Still, the first time|wasn't too comfortable.
00:04:03:Of course. It never is.
00:04:05:You were uncomfortable.
00:04:07:Because you were both very young.
00:04:10:No, we were in a gas station restroom.|It's not convenient.
00:04:14:Have you ever tried it?
00:04:17:It's not convenient,|especially on a highway.
00:04:21:I wanted to hitchhike.
00:04:22:I had this fantasy that love stories|always happened at the beach.
00:04:26:But hitchhiking was a bad idea.
00:04:28:It's not surprising, though.
00:04:30:My ideas are almost always bad.
00:04:34:It's classic.
00:04:35:I get carried away, I don't think.
00:04:40:If I hadn't got picked up,
00:04:42:I might've jumped in front of a truck.
00:04:46:Who picked you up?
00:04:48:I can't name him since he's married.
00:04:51:A psychologist.
00:04:52:He diagnosed I was depressed
00:04:55:and bent over backward to cure me.
00:04:58:So far backward,|I thought I was half pregnant.
00:05:01:Luckily, it was only appendicitis.
00:05:04:If you can call it lucky.
00:05:06:The anesthesiologist wasn't|my lucky break.
00:05:10:You had trouble|with the anesthesiologist?
00:05:13:No, he was nice.
00:05:15:He seemed so much in love,|I'd have followed him to China.
00:05:19:We got as far as Limoges.
00:05:22:Funny, isn't it,
00:05:24:how people can seem madly in love|when they're not?
00:05:28:It must be easy to fake.
00:05:32:He said I went to his head|like Cointreau.
00:05:35:I guess he got tired of Cointreau,|so he went to make a phone call.
00:05:39:Phone who?
00:05:40:I never found out. He never came back.
00:05:43:The restaurant had a back door,|which I didn't know,
00:05:47:so I waited for him till closing time.
00:05:52:The manager lived upstairs.
00:05:54:His room smelled greasy.
00:05:56:But he had soft, gentle hands.
00:05:59:Hands are tricky.|They can make you believe anything.
00:06:03:That's how I got my first job,
00:06:05:as a hostess at his place.
00:06:09:What does a hostess do?
00:06:12:At first, she welcomes people
00:06:15:and smiles at everyone.
00:06:17:As far as jobs go, it's a no-brainer
00:06:19:but you know|how smiles give people ideas
00:06:22:and Limoges is so full of lonely men.
00:06:25:You can't imagine!
00:06:26:The judge said it has|the most depressed people in France.
00:06:30:Which judge?
00:06:32:He comforted me|when they closed the place
00:06:35:because of the hostesses.
00:06:37:He was depressed, too.
00:06:41:Not that he comforted me for long.
00:06:44:Not even 1 5 minutes.
00:06:45:In a hotel room|with no pillows, TV or curtains.
00:06:49:But he wasn't that bad.
00:06:52:When he saw I was crying,
00:06:55:he gave me his hankie.
00:06:57:Then he left.
00:07:03:Maybe it's all I deserve.
00:07:06:It must be the law of nature.
00:07:09:Some people are born to be happy.|I get conned every day of my life.
00:07:17:I believe every promise I hear.
00:07:20:I've never achieved anything.
00:07:22:I've never been useful|or precious to anyone,
00:07:25:or happy, or even really unhappy.
00:07:28:I guess you're unhappy|when you lose something
00:07:32:but I've never had anything|except bad luck.
00:07:36:How do you see your future, Adele?
00:07:42:I don't know.
00:07:48:When I was little,
00:07:49:all I wanted to do was grow up.
00:07:53:As fast as I could.
00:07:57:But I can't see the point of it all.
00:08:00:Not anymore.
00:08:03:Getting older.
00:08:08:I see my future like a waiting room
00:08:12:in a big train station,|with benches and drafts.
00:08:17:Outside, hordes of people run by|without seeing me.
00:08:20:They're all in a rush,
00:08:22:taking trains and cabs...
00:08:25:They have somewhere to go,
00:08:27:someone to meet...
00:08:31:And I sit there, waiting.
00:08:36:Waiting for what, Adele?
00:08:54:For something to happen to me.
00:09:57:You look like a girl|who's about to make a mistake.
00:10:00:I'm okay, thanks.
00:10:02:I mean it. You look desperate.
00:10:05:You think so?
00:10:07:What are you playing?|Heads or tails?
00:10:10:Who are you out to impress?
00:10:12:Nobody. I never impressed anyone.|I'm not going to start now.
00:10:16:You're too young to be so sad.
00:10:19:Are you terminally ill?
00:10:21:Short a kidney? Liver? Leg?
00:10:24:No, I'm just short of...
00:10:27:a little guts. I'm scared it's cold.
00:10:30:Of course it's cold!|You think they heat it?
00:10:36:I must not think about it.
00:10:37:Right. Think happy thoughts.|They'll give you a push.
00:10:41:That'll be hard.|Happy thoughts aren't my specialty.
00:10:45:That's why I'm here, see?
00:10:47:You know what I see?
00:10:49:I see a waste ahead and I hate waste.
00:10:52:Waste of what?
00:10:53:You. You don't trash a good light bulb.
00:10:56:This one burned out long ago.
00:10:59:You're depressing me.
00:11:01:So bug off!|I'm at the end of my rope, okay?
00:11:04:What rope? Look at you.|Your rope's barely begun.
00:11:08:This is just a bad patch.
00:11:10:My whole life's a bad patch.|I'm stamped with the seal of failure.
00:11:14:You think it washes off?
00:11:17:I bet this is your first try, right?
00:11:20:Y es. I don't live on bridges.
00:11:24:I do.
00:11:26:Doing what? Trying to jump?
00:11:28:No, hiring people.
00:11:31:- Hiring who?|- Assistants.
00:11:34:Burned-out women|are my stock in trade.
00:11:36:I usually find them here...
00:11:38:or on high roofs, but in the spring.
00:11:41:In winter they prefer bridges.
00:11:44:- Like me.|- No.
00:11:47:Not like you.
00:11:49:They're basket cases,|cracked beyond repair.
00:11:54:What do you do to them?
00:11:57:Miss them, sometimes.|It's a question of balance.
00:12:08:Past the age of 40,|knife-throwing becomes erratic.
00:12:12:That's why I recruit on bridges.|I like to help.
00:12:19:If you really want to end it all,|I can take you on a trial basis.
00:12:23:No thanks. I'll manage on my own.
00:12:26:Sure. You'll still be here next week,|staring at your shoes.
00:12:30:You can't fool me|with your fancy propositions.
00:12:34:You think a sad girl on a bridge|is an easy target.
00:12:37:Yours for the asking!
00:12:39:Excuse me!
00:12:40:I never sleep with my targets.
00:12:43:That's your problem!|I'm through with fairy tales.
00:12:46:You want to jump,
00:12:47:jump.
00:12:49:Then where will you be?
00:12:51:I'll soon find out.
00:12:55:Are you dumb or what?
00:13:26:Breathe!
00:13:28:I don't want to! Get it?
00:13:30:- Name?|- Count my fingers!
00:13:32:Fuck your fingers!
00:13:34:You have hypothermia.
00:13:35:I don't! I'm immune.
00:13:37:- 3-42.|- What?
00:13:38:3 minutes 42.
00:13:40:Barcelona '74,|European record. Me!
00:13:42:Lie down!
00:13:43:Why did you jump?
00:13:44:Listen to her.
00:13:50:She leaves her body to science.
00:13:52:Take no notice. She's a joker.
00:13:54:- You're related?|- I'm her mother.
00:13:56:You rescued her?
00:13:59:It was so dark,
00:14:01:it's hard to say who rescued whom.
00:14:10:Been here long?
00:14:11:Two months.
00:14:14:Leave. They're all wacko.
00:14:30:Which bridge were you on?
00:14:33:A footbridge near the Eiffel T ower.
00:14:36:And you?
00:14:38:Solferino.
00:14:40:You're schizoid?
00:14:42:Manic-eccentric.
00:14:45:Your first jump?
00:14:48:With her, yes.
00:14:50:T ake a look at her.
00:14:51:With her eyes and ass,|would you drown yourself?
00:14:55:I don't see the connection.
00:14:57:Depends on the situation.
00:14:58:Situation?|Maybe for you, but look at her.
00:15:01:All that excess grief she's carrying.
00:15:04:Would it make you sick|to smile now and then?
00:15:08:You mean now?
00:15:09:Y es, now. You should be in a fridge|with a label on your toe.
00:15:13:I should've known.
00:15:15:- Known what?|- I'd flub it.
00:15:17:I can't even drown myself.|Story of my life!
00:15:20:Here we go.|Violins and handkerchiefs...
00:15:23:Keep trying.
00:15:25:You'll get there.
00:15:26:No use trying, with my lousy luck.|Goes to show.
00:15:30:Show what? What luck?
00:15:32:No! Come with me.
00:15:34:Let her get warm! She's not well.
00:15:37:Luck?|You think you catch it like a cold?
00:15:40:It takes faith! Willpower! Effort!
00:15:42:Go out and get it, for fuck's sake!
00:15:44:Where?|I don't even know what it looks like.
00:15:48:Me.
00:15:49:Want to see?
00:15:54:Where are you going?
00:16:08:Give me some sugar.
00:16:09:Sugar lumps! Three!
00:16:15:Like it?
00:16:18:- You can win it.|- How?
00:16:20:- Or you. You want faith?|- In what?
00:16:22:Luck!
00:16:23:Do you?
00:16:25:Yes or no?|Take your pick or go home.
00:16:27:But I'm on duty!
00:16:29:Focus on this sugar
00:16:30:as if your life depended on it.
00:16:44:Stand by.
00:17:05:Two to one. An easy start.
00:17:18:So?
00:17:20:It's loose. The strap's big.
00:17:21:Not that. Are you available?
00:17:24:I'll say!|I'm so available, it makes me dizzy.
00:17:28:I can give you 25%. Okay?
00:17:30:- Thanks.|- You're welcome.
00:17:32:25% of what?
00:17:33:My fees. They vary nightly,|often with good surprises.
00:17:37:Any congenital ailments?|Allergies, false limbs, deafness?
00:17:41:No, I'm normal.|Only my right eye's a bit weak.
00:17:45:Eyes don't matter.|See less, fear less.
00:17:48:You know your blood type?
00:17:51:AB, I think. Why?
00:17:53:Accidents. Bleeding can be harmless|if it's stopped in time.
00:17:57:You have your passport?
00:18:10:And this one.
00:18:12:What's this?|Are you moving?
00:18:22:You throw those?
00:18:24:What did you expect, teaspoons?
00:18:27:Car 1 2.|Please don't shake the trunk.
00:18:31:What's wrong? Out of juice?
00:18:34:No, it's not what I imagined.
00:18:37:It's not?
00:18:39:Those things can kill.
00:18:41:So can anything.|Toothpicks, paper clips... Looks deceive.
00:18:45:That stuff gives me the creeps!
00:18:47:But you're at the end of your rope!|You don't care.
00:18:51:- Do you?|- I don't know. I need to think.
00:18:54:Look at me, frankly. Do I scare you?
00:18:56:Frankly, you come pretty close.|It depends.
00:18:59:On what?
00:19:00:I'm thinking.|That's not a box of magic wands.
00:19:03:Look at this. Does it scare you?
00:19:07:Do you see it move? Is it shaking?
00:19:11:Remember, it's not the thrower|that counts. It's the target.
00:19:15:Something about you
00:19:17:tells me you have a special gift.|I mean it.
00:19:26:Would a demonstration reassure you?
00:20:23:What's the matter?
00:20:25:Stop! I've been hit.
00:20:28:Naturally. You're too tense.
00:20:30:This is mediocre.
00:20:33:My coat's ruined.
00:20:34:Where I'm taking you, it's always sunny.
00:20:37:With knives in my stomach,|I'll hardly care.
00:20:40:I've never hit anybody in the stomach.
00:20:43:Still, this is not going to work.
00:20:46:You have other plans?
00:20:47:Another bridge? Valium? A gun?
00:20:50:No, but that stuff...|I don't have the gift.
00:20:55:Trust me.
00:20:59:Please.
00:21:03:With your body and my skill,|we'll kill 'em.
00:21:06:Kill who?
00:21:20:Ticket, please.
00:21:22:Coat. Left pocket.
00:21:24:You're in the way.
00:21:26:And your marshmallow seats|compress my spine.
00:21:31:It's unbreakable
00:21:32:and waterproof to 250 meters.
00:21:34:You dive?
00:21:36:I'm starting out.|I began last night, but not deep.
00:21:40:You should always go easy|the first time.
00:21:43:T ake it gently, in stages.
00:21:46:Gently, in stages.
00:21:51:- Do you believe in luck?|- Y es.
00:21:54:Why?
00:21:56:Because you have cuddly breasts
00:21:58:and something's going to happen.
00:22:00:What kind of thing?
00:22:02:Something soft and warm
00:22:04:Iike intensive care.|My pulse is racing so fast,
00:22:08:I'm about to faint with desire for you.
00:22:38:- Busy!|- Doing what?
00:22:41:Open up.|You're screwing up again, big-time!
00:22:46:Aren't you done?
00:22:50:Excuse me. May l?
00:22:52:They're not done.|Rubbers slow things down.
00:22:56:If she uses them, that is...
00:22:59:If she uses them.
00:23:03:- Do you?|- What?
00:23:04:What do you think?
00:23:06:You get dirty with a stranger,|you use earplugs? A mouthguard?
00:23:09:He's not a stranger!|His pulse was racing.
00:23:13:So what?
00:23:14:It was beating too fast.|I get that too.
00:23:17:I wanted someone to hug me.
00:23:20:I needed a little tenderness.
00:23:22:Maybe I got carried away.|I didn't think.
00:23:25:We didn't think.
00:23:27:You're a perfect fit.|Not a brain cell between you.
00:23:31:I started it. I know it doesn't help,|it only fills the cracks...
00:23:35:- Who's cracked?|- She is. Look at her.
00:23:38:That'll do.|Go fill some other cracks.
00:23:41:Who are you?
00:23:42:A fairy. Can't you tell?
00:23:50:I'm not used to it yet.
00:23:51:To what?
00:23:52:Saying no.
00:23:54:I'll have to control myself.
00:23:57:It's like quitting smoking.
00:23:59:The first week is the toughest,|then you get over it.
00:24:02:Try chewing gum.
00:24:05:Somehow I can't stop.
00:24:08:Boys attract me like beautiful clothes.
00:24:11:I always want to try them on.
00:24:14:Am I abnormal?
00:24:15:Not especially.|You just need some guidance.
00:24:19:Where to? Wherever I go,|I seem to take the wrong road.
00:24:23:There's no wrong road,|only bad company.
00:24:27:I'll make you somebody.|Understand?
00:24:30:Somebody
00:24:31:who laughs and takes life with ease.
00:24:33:You'll be Cinderella, Farah Diba,|Queen of the Night...
00:24:37:What'll I do in the day?
00:27:21:Like him?|If you want to meet him, there's the john.
00:27:25:He smiled. I'm polite.
00:27:26:Your kind of politeness|leads straight to the sack.
00:27:30:You're so negative!
00:27:31:Not about everything.
00:27:34:Please try to stand up straight.
00:27:36:Arch your back and jut your chin.|Look determined.
00:27:41:T o do what?
00:27:43:T o move them.
00:27:44:The audience must fall in love with you.
00:27:47:That first knife must twist their guts.
00:27:49:Don't worry.|One look at you, they'll be twisted.
00:27:53:Put a dark line here.|Make yourself look anxious, tragic.
00:27:57:They love it.
00:27:58:Don't I look tragic enough already?
00:28:04:Pick an elevator.
00:28:05:The one on the right.
00:28:10:You have a gift, see?
00:28:21:Please sir, may I swipe|your credit card?
00:28:26:No swipes. Cash, tomorrow.
00:28:27:Sorry sir, we...
00:28:29:Seriously!
00:28:31:Seriously!
00:28:32:Would I sneak out with that?
00:29:50:Hi, Gabor! How's tricks?
00:29:54:Good to see you again.
00:30:01:Mrs. Vassiliev, born in Minsk in 1 907,|had 69 multiple births.
00:30:06:1 6 sets of twins,|7 of triplets and 4 of quadruplets.
00:30:09:How do you do it?
00:30:13:- Where's Kusak?|- Busy. Why?
00:30:15:- Who's before me?|- Who are you?
00:30:18:Gabor. Knives.
00:30:20:...by Leon Spinks in Chicago, lllinois.
00:30:23:I've no knives.
00:30:24:I do. Where's my dressing room?
00:30:26:Mr. Kusak!
00:30:28:- Did we book a knife act?|- Never.
00:30:30:Pardon me?
00:30:32:You here? Nobody booked you!
00:30:34:I canceled two galas in Oslo for this!
00:30:36:Mr. Jarvis personally chose
00:30:38:tonight's lineup. No knives.
00:30:41:Only novelty acts.
00:30:43:That's why I'm here. My act's new.
00:30:46:What's new about knife-throwing?
00:30:55:I throw blind.
00:30:57:Blind?
00:30:59:Blind.
00:31:03:He throws blind.
00:31:12:He agrees.
00:31:13:After the contortionist.
00:31:15:No way.
00:31:16:Never after a silent act.|And find me a sheet.
00:31:40:- Got an act?|- No, I'm the target.
00:31:44:Gabor does it on you?
00:31:46:- Does what?|- His act.
00:31:48:Knives? More like acupuncture!
00:31:51:Blind, especially.
00:31:53:You have a lovely body.
00:31:55:Why butcher it?
00:32:08:The Statue of Liberty is 46 meters high.
00:32:11:No! 7 1.
00:32:12:7 1 with the base, 46 without.
00:32:17:- Head capacity?|- 40, standing.
00:32:20:Is that you?
00:32:23:Irene?
00:32:25:How come you're here?|Nobody told me.
00:32:29:It's me.
00:32:31:You're back in France?
00:32:33:You left Glasgow?
00:32:37:You've changed.
00:32:39:Is it your eyes?
00:32:41:I'm so...
00:32:43:I never thought we'd meet again.
00:32:47:Neither did l.
00:32:49:I searched for you everywhere|from town to town. Even Madrid, once.
00:32:54:Somebody said you were on|at the Victoria.
00:32:57:For months I'd stop men in the street|who looked like you.
00:33:01:I took pills.|Got married twice, three times...
00:33:06:I lose track.
00:33:10:Remember your theory of luck?
00:33:15:`You don't take it, you make it.'
00:33:18:Your luck arrived when I left.
00:33:21:How I missed your hands!
00:33:24:They knew me so well.
00:33:27:Touch me.
00:33:29:To say goodbye.
00:33:31:Just once.
00:33:50:Excuse me.
00:33:51:What does 'blind' mean?
00:33:55:It means we'll wow them.
00:34:12:Shoulders out.
00:34:14:Chin up.
00:34:16:Was that your wife?
00:34:17:Feet apart.
00:34:20:Did you bring her luck?
00:34:22:No, I caught her.
00:34:23:She was Miss Cannonball.|She flew 1 00 yards.
00:34:25:She fell on me. I saved her life.
00:34:28:Like me. You save everybody!
00:34:30:Not like you.
00:34:33:Blind, do you shut your eyes?
00:34:36:Stand straight, breathe deep.|Leave the rest to me.
00:34:39:You've done it before?
00:34:41:Not completely.
00:34:43:I lacked the right target. You.
00:34:47:What have I done to you?
00:34:48:You inspire me.|I have faith in your luck.
00:34:51:You have it in you|like a horseshoe or a four-leaf clover...
00:34:55:but if you've lost faith,|there's the exit.
00:34:58:I won't blame you.
00:35:01:Which hand?
00:35:06:See what faith can do? Wear it.
00:35:09:If you have to die, do it in style.
00:36:28:Go on!
00:38:25:Take it off.
00:38:29:- Are you okay?|- Y es, fine.
00:38:31:You're pale. From stress?
00:38:33:At one point, I felt you stiff and wary.
00:38:37:It made me tense.
00:38:41:Have you ever felt|great fear and pleasure, both at once?
00:38:46:Y es.
00:38:50:T onight.
00:38:53:Did it feel good?
00:38:55:Naturally.
00:38:56:Naturally!
00:38:58:What?
00:39:00:Nothing.
00:39:04:Just tell me one thing.
00:39:08:Would it make you sick|to smile now and then?
00:39:33:No checks. Always cash.
00:39:36:Can you do the same thing|tomorrow in San Remo
00:39:39:with the same girl?
00:39:41:- Is it her or me you want?|- Both.
00:40:14:I was in the audience just now.
00:40:16:I felt my body catch fire.
00:40:21:I wanted you to pierce me.
00:40:23:You have such magnetic eyes!
00:40:25:You do hypnosis?|I'm aching to be hypnotized.
00:40:30:Which hand?
00:40:31:That one.
00:40:33:Sorry. You lost.
00:41:13:Everything going okay?
00:41:15:Need anything?
00:41:17:Drinks? Kleenex?
00:41:20:What do you want?
00:41:21:Just testing. Give me a number.
00:41:23:- 30.|- Not you, you.
00:41:26:- 0.|- 0. Take this.
00:41:28:Tonight's pay. Put it on 0.
00:41:30:- Now?|- Preferably.
00:41:33:Back in 1 5 minutes.|Can you hold out?
00:41:37:- How do you rate him?|- I don't.
00:41:39:Beware of athletes.|75% are morons
00:41:42:with flea-dicks.|You'd be disappointed.
00:41:44:- You're an athlete?|- I quit in time.
00:41:51:Build up your stake|but never bet small.
00:41:55:If in doubt, straddle.
00:41:57:- Straddle who?|- Your number.
00:41:59:Focus on it like a brother,|your only friend on earth.
00:42:03:Why don't you?
00:42:06:- I'm banned.|- From what?
00:42:08:The casino.
00:42:09:Also, because
00:42:11:if you're not full of holes by now
00:42:14:you must be good voodoo|and me with you.
00:42:19:What's the deal? 50-50?
00:42:20:Minus the hotel bill.
00:42:31:Place your bets.
00:42:48:No more bets.
00:42:57:Just one glass, sir?
00:43:07:You see!
00:43:15:Tough luck.
00:43:16:Mind your own business!
00:43:20:Come on. Let's go.
00:43:26:Gently...
00:43:31:Gently...
00:43:41:We'll get there.
00:43:55:Yes!
00:43:59:Your bill, sir.
00:44:01:Sorry, I'm broke.
00:44:03:Cleaned out. I gave it all to her.
00:44:05:Cheers!
00:44:44:What'll this buy me?|A boat? A house?
00:44:46:With your diving habit,|I'd spend it on flippers.
00:45:03:Here's your prop back.
00:45:05:You're returning to Paris?
00:45:12:What's this worth?
00:45:14:Like that? Not much.
00:45:15:You want the truth?
00:45:18:I misled you.
00:45:21:Luck always used to pass me by.
00:45:24:Other people had it.|I was always a piece short.
00:45:28:What piece?
00:45:39:They want us in ltaly tomorrow.|Interested?
00:45:43:- Don't miss him.|- Who?
00:45:45:T ootsie roll.|I told him 1 5 minutes.
00:46:55:Any special number?
00:46:57:How about 32 for a change?
00:47:27:Can you tell her|to stop staring at me like that?
00:47:51:We have to stick together|or it won't work!
00:47:55:Stick together? Look at the time!|You'll win more tomorrow.
00:47:58:Who says our luck won't run out|like it started?
00:48:02:I'm scared we'll lose it.
00:48:04:Aren't you?
00:48:06:You see!
00:48:07:No. I'm looking for a taxi.
00:48:13:You want to be sure it still works?
00:48:23:Try this. Raffle ticket.
00:48:44:What do I win?
00:49:28:- I underrated you.|- How?
00:49:30:You're a whole lucky horse!
00:49:33:Is that all luck's about?
00:49:35:Backing horses, winning cars,|picking numbers?
00:49:38:Is there nothing more to it?
00:49:41:Naturally.
00:49:42:What is it?
00:49:45:It beats me.
00:49:46:You're the expert, not me.
00:49:48:I'm no expert. I'm a stand-in.
00:49:51:Luck's a matter of life or death.
00:49:54:Didn't you know?
00:49:59:How about this?
00:50:01:What are you doing?
00:50:02:Fieldwork.
00:50:07:How do you do it?
00:50:09:- Place your bets!|- No more bets!
00:50:12:- What about corners?|- Corners?
00:50:20:So?
00:50:21:Are we alive?
00:50:22:No. Can't you feel|we're going to heaven?
00:50:25:It's not what I imagined.
00:50:27:Because it's late. Everything's shut.
00:51:17:You found my replacement?
00:51:19:He was suicidal!
00:51:21:We hit it off at once.
00:51:27:You're not the only lucky charm!
00:51:39:- Who are you calling?|- Knife fans.
00:51:41:Got any cash?
00:51:42:Gash? I have plenty.|Huge ones!
00:51:45:People live with no knives, you know?
00:51:47:No arms, no legs, no you...|but it's less fun.
00:51:51:So? What a dumb answer!
00:51:53:It's funny how you disconnect.
00:51:55:Can I get some privacy?|There's plenty of room around.
00:52:17:Forget knives!|Gold grows under my feet.
00:52:19:You're a workaholic!|Don't you get tired?
00:52:22:I owed you a watch.|Now we're even.
00:52:25:You owe me nothing.
00:52:34:Learn to lose|or you'll take winning for granted.
00:52:41:What's 'rainbow' in ltalian?
00:52:44:We should make a wish.
00:52:47:- Smile! We're on vacation.|- No. We're lost.
00:52:50:Not true! You're a pessimist.
00:52:52:Listen.|Just follow the sound of crickets.
00:53:05:How about dessert?
00:53:09:Would you like him for dessert|with a scoop of ice cream on top?
00:53:14:Train to catch?
00:53:15:Vacationers take trains.|The 8 :23 is good.
00:53:19:Good for who?
00:53:22:I should let him sit here|or you'll twist your head off.
00:53:26:No need. I'm only looking at him.
00:53:28:Admit I've held back lately.
00:53:31:Don't be shy. With luck|he'll take you standing on a table.
00:53:35:Shame to miss it.
00:53:40:What if he asked you|to go with him, now?
00:53:43:Would we be happy?
00:53:46:Who?
00:53:48:Him and me.
00:53:52:I'm going to tell you a story.
00:53:55:Long ago, I lived on the even side|of a street, at number 22.
00:53:59:I gazed at the houses across the street,
00:54:02:thinking the people were happier,
00:54:04:their rooms were sunnier,
00:54:07:their parties more fun.
00:54:09:But in fact their rooms|were darker and smaller
00:54:13:and they, too, gazed across the street.
00:54:17:Because...
00:54:19:We always think|that luck is what we don't have.
00:54:33:I'll wait at the station.
00:54:36:If you don't show,|I'll know you went.
00:54:39:Went where?
00:54:42:T o see if the other side's better.
00:55:00:It's not better! I'm sorry.
00:55:03:Sorry, it's not you.
00:55:05:It's not you I'm looking for.
00:55:08:What's the time?
00:55:38:T ails.
00:55:53:Heads.
00:55:54:What are you doing? Playing|with trains at your age? For what?
00:55:59:T o find the right track.
00:56:01:Look at me.
00:56:02:I believed what you said about luck.
00:56:04:Four leaf clovers,|easy life and Farah Diba.
00:56:07:I trusted you!
00:56:08:It's a dirty trick,|discouraging other people
00:56:12:with your pride and trains|and vanishing acts.
00:56:15:You're like a schoolmarm,
00:56:16:judging and preaching! Do this!
00:56:18:Do that! Chin up! Stand there!
00:56:21:I'm like a schoolmarm?
00:56:23:A bit.
00:56:26:Sulk, I don't care.
00:56:28:This wasn't my idea.|You got me here, you can't ditch me now.
00:56:32:I'm getting used to being lucky,|and to you.
00:56:40:You know what I want?
00:56:44:The same thing as me?
00:56:47:Right now. Anywhere.
01:00:10:- What did you tell them?|- We're ready.
01:00:13:Ready for what?
01:00:14:Anything, plus an international career.
01:00:17:Are you ready?
01:00:18:Ready!
01:00:47:The Wheel of Death!
01:00:51:- Wheel of what?|- Death.
01:00:53:A small variation. Cruise tourists|get bored. They need movement.
01:00:57:Right! Let's go.
01:02:39:The ship rolled.
01:03:27:- Shall we?|- Thanks anyway.
01:03:28:I'm not very good.
01:04:14:Thank you.
01:04:22:Got a light?
01:04:25:Here.
01:04:31:You found this?
01:04:33:In ltaly, beside the road.
01:04:36:Funny. It's mine.
01:04:41:T.P.
01:04:43:Takis Papadopoulos. That's me.
01:04:47:Take it back.
01:04:49:No, keep it.
01:04:51:My wife wants me to quit smoking.
01:04:54:I said OK to keep her happy.
01:05:03:She's ltalian, I'm Greek.
01:05:06:We barely understand each other.|No point arguing.
01:05:17:Are you cold?
01:05:18:Probably just seasick.
01:06:27:I brought your...
01:06:32:Here.
01:06:33:Thanks. There's no hurry.
01:06:36:Actually, there is.
01:06:39:What's up? Did you sleep badly?
01:06:44:I'm going. I'm leaving you.
01:06:50:For whom?
01:06:51:Mr. Right.|The man I've been waiting for.
01:06:54:He's taking me away. We're leaving.
01:07:13:- It can't be true.|- It is.
01:07:15:Not him!
01:07:16:He's a newlywed.|He's depressive.
01:07:18:He's Greek!
01:07:20:Nobody ever looked at me like he does.
01:07:23:Nobody asked me|which side of the bed I liked,
01:07:26:if I was hot or cold,|hungry or thirsty...
01:07:31:except you, maybe, on a good day.
01:07:33:No! I never asked you|which side of the bed you liked.
01:07:40:Left.
01:07:45:You...
01:07:46:and him are the only good things|I've ever had.
01:07:52:That's not a lot.
01:07:56:You and I won't always be together.
01:08:09:What do we do?|Shake hands? Kiss?
01:08:13:Forget each other.
01:08:20:No promises.
01:08:42:I'm sorry.
01:08:49:No problem.
01:09:09:You look like a girl|who's about to make a mistake.
01:11:51:Artist.
01:11:53:Cabaret artist.
01:11:57:I throw knives.
01:12:32:Whatever possessed you|to go with that guy?
01:12:36:Love strikes at random.
01:12:39:I felt he was so similar to me.
01:12:42:He seemed so sad.
01:12:45:He swore it was forever.
01:12:47:Forever?
01:12:49:T ake a good look|at your Greek shepherd.
01:12:54:You picked another bum, I can see.
01:12:56:I couldn't have known.
01:12:59:Known what?
01:13:00:The way it would turn out.|And so soon.
01:13:48:As far as forever goes,|it went really fast.
01:13:52:They took us to a Greek air base
01:13:55:and there he changed his mind.
01:14:00:And then?
01:14:02:Nothing.
01:14:03:I was given hot coffee|and a smile to cheer me up.
01:14:10:Oh no!
01:14:12:Yes.
01:14:15:I saw it all starting again like before.
01:14:18:- Before what?|- Before you.
01:14:35:That one.
01:14:40:Wrong.
01:14:42:See? It's run out.
01:14:44:Luck comes and goes, you know.
01:15:16:How's it going with you?
01:15:24:So-so.
01:15:35:Do you believe in the torn bill?
01:15:41:What bill?
01:15:42:The one in two halves|that were worthless apart.
01:15:46:Do you believe in it?
01:16:00:Are you there?
01:16:17:Are you there?
01:16:19:Y es, I'm here.
01:16:39:Still in showbiz?
01:16:40:You bet! Like never before.
01:16:57:Are you from Paris?
01:16:59:Did you come across a lost-looking|blonde with a waterproof watch
01:17:04:and a load of sadness?
01:17:55:Sorry, I need it|in case I get an attack of the blues.
01:18:01:We're all at risk.
01:18:05:You think I'm dumb?
01:18:07:I know it looks stupid.|Clinging to trinkets.
01:18:11:An old lighter,
01:18:12:a torn bill...
01:18:16:the look in her eye|on that bridge,
01:18:20:that night,|when I was trying to jump too.
01:18:33:Hang in there.
01:18:35:All it takes is a girl on a bridge|with big sad eyes.
01:18:43:I'll leave it on.
01:18:44:You never know.
01:18:46:She might be passing by.
01:18:50:The door!
01:19:14:Pay what you like.|A donut, a few dates, a cucumber...
01:22:08:You look like a guy|who's about to make a mistake.
01:22:23:What are you waiting for? High tide?
01:22:28:Not easy, is it?
01:22:31:You think you can empty your mind|and let go
01:22:34:but it doesn't work like that.
01:22:38:Also, bridges are busy places to jump off.
01:22:41:There's always someone|giving you second thoughts.
01:22:50:Did you break something?
01:22:54:All sorts of things.
01:22:56:I need a complete overhaul
01:22:59:but it's not worth it.|A new knife-thrower's cheaper.
01:23:04:What would I do with a new one?
01:23:15:Are you cold?
01:23:16:It's shaking.
01:23:18:It never shook. You dreamed it.
01:23:23:Maybe we both dreamed|and it wasn't so bad.
01:23:31:Ready to go?
01:23:36:Where?
01:23:40:Anywhere.
01:23:42:Wherever we go,|you'll find a few knives to throw at me.
01:24:06:Anyhow, we have no choice.
01:24:08:If I don't jump, you do.|We can't go on like this.
01:24:13:Like what?
01:24:18:Not being together.

