1
00:06:22,800 --> 00:06:24,597
Alright. Come on, Dalton.

2
00:06:25,920 --> 00:06:26,920
Come on.

3
00:06:26,920 --> 00:06:29,309
Give me your hand. Come here.

4
00:06:31,560 --> 00:06:34,520
OK, in you get.

5
00:06:34,520 --> 00:06:36,033
Go on.

6
00:06:40,520 --> 00:06:42,397
That's it. Go on.

7
00:06:45,840 --> 00:06:47,273
Come on, lads.

8
00:08:31,840 --> 00:08:35,435
There's freedomin work, lads. 
Freedom in work.

9
00:08:37,360 --> 00:08:39,999
Sooner you get this forest cut down,
the sooner we can go home.

10
00:08:43,480 --> 00:08:46,440
Great ships are built
on the shoulders of you and l.

11
00:08:46,440 --> 00:08:47,873
Great ships.

12
00:08:50,440 --> 00:08:52,080
Mathers!

13
00:08:52,080 --> 00:08:55,720
Got no tongue in your head today?
Eh? Surely not.

14
00:08:55,720 --> 00:08:58,400
Your man Mathers
has found himself homesick this day

15
00:08:58,400 --> 00:08:59,920
and mute it has made him.

16
00:08:59,920 --> 00:09:03,480
Poor lad. He misses his Scottish homeland.

17
00:09:03,480 --> 00:09:06,200
He'd rather be there
than with the likes of you.

18
00:09:25,240 --> 00:09:28,280
One...two...three.

19
00:10:20,800 --> 00:10:23,598
If this one sinks,
there'll be some fucking language.

20
00:10:30,160 --> 00:10:33,200
It was some meal
I was treated to last night.

21
00:10:33,200 --> 00:10:36,200
Soup, baked bread...

22
00:10:36,200 --> 00:10:39,040
..mutton.

23
00:10:39,040 --> 00:10:42,350
The likes I cannot recall
since I left the homeland.

24
00:10:44,000 --> 00:10:48,240
I was in no need
of my good friend the potato.

25
00:10:48,240 --> 00:10:50,800
But he poured forth.

26
00:10:50,800 --> 00:10:52,400
I wish myself deaf.

27
00:10:52,400 --> 00:10:56,598
He yaps more than a dog
with three heads.

28
00:10:58,080 --> 00:11:00,480
But I did manage to...

29
00:11:00,480 --> 00:11:03,440
..smuggle a little something to...

30
00:11:03,440 --> 00:11:06,480
There's not much, but...

31
00:11:06,480 --> 00:11:09,597
Well, it'll go wonderfully
with your bread there.

32
00:11:32,760 --> 00:11:34,671
It's there if anyone wants it.

33
00:11:58,800 --> 00:12:01,268
What are you smiling at now,
Bodenham?

34
00:12:03,480 --> 00:12:05,948
No law against smiling.

35
00:12:07,240 --> 00:12:11,279
Not yet, young Thomas.
Not yet.

36
00:12:20,440 --> 00:12:22,590
Where's that mongrel sailor - Greenhill?

37
00:12:26,000 --> 00:12:28,958
Greenhill, eh? Where is he?

38
00:12:35,920 --> 00:12:38,560
Where is he? Eh?

39
00:12:38,560 --> 00:12:42,560
You'll all feel the taste of it
if he's not found this minute.

40
00:12:42,560 --> 00:12:44,440
Arggh!

41
00:12:44,440 --> 00:12:47,720
There's not a boat to beseen on the harbour. 
Now's our chance.

42
00:12:47,720 --> 00:12:49,240
- Help! Help!
- Shut him up!

43
00:12:49,240 --> 00:12:52,880
You took your bleeding time.
If I had to listen to one more word...

44
00:12:52,880 --> 00:12:56,160
This is to remove your tongue
if you make noise with it. Understand?

45
00:12:56,160 --> 00:12:59,080
Take his clothes. Take his clothes!

46
00:13:04,880 --> 00:13:06,520
Keep fucking still!

47
00:13:06,520 --> 00:13:08,715
Pass the coat.
- Get off me!

48
00:13:12,400 --> 00:13:14,391
Get off me!

49
00:13:23,080 --> 00:13:26,311
Come on. Come on! Let's go!

50
00:13:29,040 --> 00:13:31,031
Let's go!

51
00:13:33,200 --> 00:13:35,360
Come on!

52
00:13:35,360 --> 00:13:38,989
Where will you go?
There's nothing out there.

53
00:13:40,080 --> 00:13:41,798
No! No!

54
00:13:45,440 --> 00:13:48,240
Fat...fucking...

55
00:13:48,240 --> 00:13:50,120
You hit him one more time...

56
00:13:50,120 --> 00:13:53,954
..one more time, and I swear to fuck
we leave you here with him!

57
00:13:55,400 --> 00:13:57,834
Grab everything! Quickly!

58
00:14:13,560 --> 00:14:15,800
There'll be supplieson the ship when we get there.

59
00:14:15,800 --> 00:14:17,358
We make our move at dusk.

60
00:14:32,480 --> 00:14:33,913
Run!

61
00:15:42,560 --> 00:15:44,600
What? Are they shooting each other?

62
00:15:44,600 --> 00:15:46,113
Shh.

63
00:15:47,320 --> 00:15:50,120
- We can't stay here.
- Can�t go back now.

64
00:15:52,720 --> 00:15:54,153
Quiet.

65
00:16:07,120 --> 00:16:08,600
We head east.

66
00:16:08,600 --> 00:16:12,673
If you don't want a noose
around your neck, follow me.

67
00:17:33,400 --> 00:17:37,520
- I'm bleeding cold.
- Be quiet.

68
00:17:41,000 --> 00:17:42,520
I need a fire.

69
00:17:42,520 --> 00:17:44,200
We can't, you nikey.

70
00:17:44,200 --> 00:17:46,720
I need the warm!

71
00:17:46,720 --> 00:17:50,320
- Punch that cunt, Mathers.
- You fucking try it.

72
00:17:50,320 --> 00:17:54,560
Ohh. The wee lad wants to be
a little bit of a fighter, does he?

73
00:17:54,560 --> 00:17:56,280
You're all talk, old man.

74
00:17:56,280 --> 00:17:58,800
You're the one fucking talking.
Why don't you just shut your mouth?

75
00:17:58,800 --> 00:18:01,200
You'll be talking
when you're left behind.

76
00:18:01,200 --> 00:18:03,720
Oh, why don't you go to fucking sleep?

77
00:18:03,720 --> 00:18:05,711
We're all fucking cold.

78
00:18:20,800 --> 00:18:24,190
- What are they saying?
- I don't know.

79
00:18:25,320 --> 00:18:28,080
Eh? You're fucking Irish.

80
00:18:28,080 --> 00:18:30,435
Go to fucking sleep.

81
00:20:51,640 --> 00:20:53,320
You might not see it in me,

82
00:20:53,320 --> 00:20:55,640
but I'm quite the ladies' man
in Hobart Town.

83
00:20:55,640 --> 00:20:57,440
Are you just?

84
00:20:57,440 --> 00:21:00,960
When you consider
the odds stacked against us.

85
00:21:00,960 --> 00:21:04,480
Not one complaint, I'll have you know.

86
00:21:04,480 --> 00:21:06,040
Is that right?

87
00:21:06,040 --> 00:21:07,600
I've found, in my time,

88
00:21:07,600 --> 00:21:12,400
that rubbing the flesh above the hole

89
00:21:12,400 --> 00:21:14,840
has a powerful effect on all women

90
00:21:14,840 --> 00:21:18,992
and brings a strong wetness
like an ocean.

91
00:21:22,440 --> 00:21:24,908
What did you fucking say?

92
00:21:25,960 --> 00:21:27,393
You alright?

93
00:21:30,080 --> 00:21:32,036
Bleeding Irish scum.

94
00:21:52,720 --> 00:21:55,640
�ooee!

95
00:21:55,640 --> 00:21:58,074
- Quiet!

96
00:22:10,520 --> 00:22:12,192
Where to now, Captain?

97
00:22:20,120 --> 00:22:22,918
At least we'll never be far
from the harbour.

98
00:22:37,560 --> 00:22:39,118
Little Brown.

99
00:22:41,400 --> 00:22:42,913
You right, Ned?

100
00:23:04,400 --> 00:23:05,833
Aye, it's fucking cold.

101
00:23:18,880 --> 00:23:20,680
Little Brown!
Swear to Christ!

102
00:23:20,680 --> 00:23:22,560
Hey! You leave him be there!

103
00:23:22,560 --> 00:23:25,677
I won't be hanged
on the account of an old lag.

104
00:23:27,880 --> 00:23:29,960
Swimming, is it, lads?

105
00:23:29,960 --> 00:23:31,560
Are you coming in, William?

106
00:23:31,560 --> 00:23:34,640
Look out, Dalton! There's
a great big snake in front of you!

107
00:23:34,640 --> 00:23:36,560
Don't you worry about this one!

108
00:23:36,560 --> 00:23:38,960
It's the wee little fella back there
that concerns me!

109
00:23:42,320 --> 00:23:43,880
In my fair opinion, lads,

110
00:23:43,880 --> 00:23:46,680
this spring is even better
than the last.

111
00:23:46,680 --> 00:23:49,840
That is the very truth itself,
William Kennerly!

112
00:23:49,840 --> 00:23:51,520
It is a fine spring!

113
00:23:51,520 --> 00:23:53,351
And better than the last, eh, Pearce?

114
00:23:54,440 --> 00:23:56,120
Aye, Mr Dalton,

115
00:23:56,120 --> 00:23:59,800
and...even...better
than the one before that!

116
00:24:01,440 --> 00:24:03,200
You bog trotters are all mad.

117
00:24:03,200 --> 00:24:08,320
 Oh! 
Jesus' mother, Mary of God. Joseph.

118
00:24:08,320 --> 00:24:11,835
All the heavenly host.
Fuck the cunt of it.

119
00:24:22,200 --> 00:24:26,840
Upon my soul, lads,
what a beautiful day!

120
00:24:26,840 --> 00:24:29,560
The sun is shining!

121
00:24:29,560 --> 00:24:32,160
We have our liberty!

122
00:24:32,160 --> 00:24:34,958
Ah! Christ, what a land this is!

123
00:24:36,240 --> 00:24:39,520
Hey! Kennerly!

124
00:24:39,520 --> 00:24:43,200
Free Ireland! Free Ireland!

125
00:24:43,200 --> 00:24:44,720
I'm right behind you!

126
00:24:44,720 --> 00:24:47,951
Use your muscles, man!
Use your muscles!

127
00:24:58,240 --> 00:24:59,720
And the door would fling open,

128
00:24:59,720 --> 00:25:01,240
and there she was, standing there

129
00:25:01,240 --> 00:25:03,600
with her wee little dugs
swinging back and forth like a...

130
00:25:03,600 --> 00:25:05,080
Don't say it.

131
00:25:06,120 --> 00:25:09,560
I've heard it too often and in
too great a detail for it to be false.

132
00:25:09,560 --> 00:25:10,788
No, it can't be.

133
00:25:13,320 --> 00:25:15,840
So, every morning without fail,

134
00:25:15,840 --> 00:25:19,120
she'd make me rub her cold legs

135
00:25:19,120 --> 00:25:21,680
and she would sing to me softly

136
00:25:21,680 --> 00:25:23,720
about the sea.

137
00:25:23,720 --> 00:25:27,360
But, by God, wouldn't she fart on me
when I was down there.

138
00:25:27,360 --> 00:25:30,600
A case of the six o'clock farts
would send my eyes burning.

139
00:25:30,600 --> 00:25:34,440
You're standing there
with this great big fucking arse.

140
00:25:34,440 --> 00:25:36,200
Christ almighty!

141
00:25:36,200 --> 00:25:39,360
Them were farts you could
smell through your ears.

142
00:25:39,360 --> 00:25:41,320
- What the hell are you talking about?
- Women, lad.

143
00:25:41,320 --> 00:25:44,280
If they didn't have cunts,
you'd throw stones at them.

144
00:25:51,760 --> 00:25:53,239
What?

145
00:25:54,840 --> 00:25:57,320
Oh, we were just relating

146
00:25:57,320 --> 00:26:00,160
as to what the two of you
were doing in the bushes.

147
00:26:00,160 --> 00:26:01,920
We were getting wood for the fire.

148
00:26:03,520 --> 00:26:06,800
- Do you think it funny?
- Aye.

149
00:26:06,800 --> 00:26:08,279
It has humour.

150
00:26:10,920 --> 00:26:12,876
One question, Robert.

151
00:26:14,840 --> 00:26:16,440
Is it true that Travers here

152
00:26:16,440 --> 00:26:18,640
was a ticket-of-leave man
before you met?

153
00:26:18,640 --> 00:26:20,232
I speak for myself.

154
00:26:23,200 --> 00:26:26,160
So, Robert, is it true that Travers here
was a ticket-of-leave man?

155
00:26:26,160 --> 00:26:27,640
No.

156
00:26:27,640 --> 00:26:30,040
I had no ticket of leave.

157
00:26:30,040 --> 00:26:33,320
I heard you had land.

158
00:26:33,320 --> 00:26:35,436
And sheep to tend.

159
00:26:38,440 --> 00:26:40,360
Is that true?

160
00:26:40,360 --> 00:26:42,640
Oh, I had a lease of land
promised to me, yeah.

161
00:26:42,640 --> 00:26:43,960
But time... It takes time...

162
00:26:43,960 --> 00:26:47,873
Here you are, huh? All the land
you could possibly desire.

163
00:26:52,520 --> 00:26:55,520
You must have been quite a catch,
Mr Greenhill.

164
00:28:13,000 --> 00:28:15,798
Come on. Come on, Ned.
I'll guide you through it.

165
00:28:17,520 --> 00:28:20,557
Come on. Come on, Ned.

166
00:28:24,920 --> 00:28:27,000
You're coming with us.

167
00:28:27,000 --> 00:28:29,560
Yeah. You're coming with us.

168
00:28:29,560 --> 00:28:31,240
You're coming with us.

169
00:28:31,240 --> 00:28:34,320
Leave him.
He's holding us back.

170
00:28:34,320 --> 00:28:35,753
Come on.

171
00:28:36,800 --> 00:28:38,233
Come on, Neddy.

172
00:28:39,560 --> 00:28:41,120
Come on.

173
00:28:41,120 --> 00:28:42,599
Just fucking leave him!

174
00:28:44,480 --> 00:28:45,913
Come on!

175
00:29:42,080 --> 00:29:45,480
�ooee!

176
00:30:24,040 --> 00:30:26,080
You'll be fine!

177
00:30:26,080 --> 00:30:27,513
- Fuck this.
- Wait!

178
00:30:28,560 --> 00:30:30,710
- Fuck this!
- My nose is fucking cold!

179
00:30:32,440 --> 00:30:35,760
We've got to stop here for Brown!

180
00:30:35,760 --> 00:30:37,360
Leave him!

181
00:30:37,360 --> 00:30:39,000
- Come on.
- Leave him.

182
00:30:39,000 --> 00:30:40,956
Wait!

183
00:30:42,440 --> 00:30:43,873
Wait!

184
00:30:45,280 --> 00:30:47,080
We have to stop here for the night!

185
00:30:47,080 --> 00:30:49,560
- Not here! No! I said leave him!
- Aye!

186
00:30:49,560 --> 00:30:51,118
We leave no-one!

187
00:30:52,200 --> 00:30:54,640
We rest here!

188
00:30:54,640 --> 00:30:56,551
We'll freeze up here!

189
00:30:58,080 --> 00:31:01,960
This is madness!
I'm heading to lower ground!

190
00:31:01,960 --> 00:31:05,400
I'm following him!
For once he's right!

191
00:31:05,400 --> 00:31:07,400
Pearce! Let's head down!

192
00:31:07,400 --> 00:31:09,356
Head down!

193
00:31:12,600 --> 00:31:14,033
Come on!

194
00:31:16,440 --> 00:31:18,160
Come on.

195
00:31:18,160 --> 00:31:20,390
Leave him!

196
00:32:15,400 --> 00:32:16,833
That the last of it?

197
00:32:25,760 --> 00:32:27,876
Where in hell are you taking us,
Captain?

198
00:32:31,640 --> 00:32:33,680
East.

199
00:32:33,680 --> 00:32:35,720
We'll hit the settled districts
before long.

200
00:32:35,720 --> 00:32:37,950
What do we eat?

201
00:32:40,480 --> 00:32:42,038
We've no food left.

202
00:32:43,080 --> 00:32:46,560
And I'm tired of carrying
these godforsaken pots.

203
00:32:46,560 --> 00:32:48,232
- And for what use?

204
00:32:58,720 --> 00:33:00,438
He said we were to escape by boat.

205
00:33:01,600 --> 00:33:03,352
Not to walk to our deaths!

206
00:33:04,600 --> 00:33:06,120
This fool doesn't know where he's going!

207
00:33:06,120 --> 00:33:09,200
You heard your man. We're headed east.

208
00:33:09,200 --> 00:33:10,633
Do you miss the flogging?

209
00:33:11,680 --> 00:33:14,353
Easy. Easy, lad. Easy.

210
00:33:24,560 --> 00:33:28,960
I witnessed a young chavy
take 200 lashes once.

211
00:33:28,960 --> 00:33:33,670
After the first 1 00, you could see
his spine poking through.

212
00:33:35,320 --> 00:33:38,357
So for the next 1 00
they had to whip his arse.

213
00:33:39,680 --> 00:33:41,671
Blood gushing like a tap.

214
00:33:43,840 --> 00:33:45,876
Never tried to escape after that.

215
00:33:50,720 --> 00:33:53,000
There's no going back.

216
00:33:53,000 --> 00:33:54,991
Not for any of us.

217
00:35:52,840 --> 00:35:55,200
Hey, what are you doing,
you mad English bastard?

218
00:35:55,200 --> 00:35:57,160
Shh!

219
00:35:57,160 --> 00:35:59,120
An animal?

220
00:35:59,120 --> 00:36:01,040
By Christ, lad, you're seeing things.

221
00:36:01,040 --> 00:36:02,960
Would you shut your fucking mouth?

222
00:36:02,960 --> 00:36:05,560
You'll never catch it.

223
00:36:05,560 --> 00:36:07,280
He'll never catch it.

224
00:36:07,280 --> 00:36:09,748
Them imaginations are too fast.

225
00:36:16,520 --> 00:36:18,080
Fuck you! Aarggh!

226
00:36:18,080 --> 00:36:20,320
- Hey. Hey.
- Do what you're fucking told, old man!

227
00:36:20,320 --> 00:36:21,880
Hey, you get your fucking hands off!

228
00:36:21,880 --> 00:36:25,160
Get your hands off!
Get your fucking hands off.

229
00:36:25,160 --> 00:36:27,360
Arggh!

230
00:36:27,360 --> 00:36:29,040
Bodenham!

231
00:36:29,040 --> 00:36:30,560
Stop it!

232
00:36:30,560 --> 00:36:32,960
No-one touches me!
-Stay back. Bodenham!

233
00:36:32,960 --> 00:36:35,280
Pearce! I will not fight.

234
00:36:35,280 --> 00:36:36,760
Fuck you!

235
00:36:36,760 --> 00:36:38,240
I will not fight!

236
00:36:45,560 --> 00:36:47,516
Pearce?

237
00:37:08,560 --> 00:37:12,280
It's oft-time

238
00:37:12,280 --> 00:37:16,920
When I slumber

239
00:37:16,920 --> 00:37:23,160
I have a pleasant dream

240
00:37:23,160 --> 00:37:28,837
With my pretty girl I be roving

241
00:37:32,280 --> 00:37:37,638
Down by a sparkling stream

242
00:37:39,160 --> 00:37:44,234
In Ireland I've been roving

243
00:37:45,800 --> 00:37:50,920
With her at my command

244
00:37:52,080 --> 00:37:57,160
But I wake up broken-hearted

245
00:37:57,160 --> 00:38:03,110
'Pon Van Diemen's Land.

246
00:38:10,200 --> 00:38:13,000
That was a beautiful song, William Kennerly.

247
00:38:13,000 --> 00:38:14,433
Aye.

248
00:38:17,120 --> 00:38:18,712
Did you have a pretty one back home?

249
00:38:21,320 --> 00:38:24,710
I hope not, for lonely she'll die.

250
00:38:27,920 --> 00:38:29,399
Yourself?

251
00:38:33,000 --> 00:38:34,479
A few.

252
00:38:39,480 --> 00:38:41,436
My sister, my mother...

253
00:40:31,200 --> 00:40:34,840
What about Pearce?
You have to do it.

254
00:40:34,840 --> 00:40:37,035
You have to do it soon.

255
00:40:38,720 --> 00:40:40,472
I'm hungry, Robert.

256
00:40:48,320 --> 00:40:50,709
Hmm. I know.

257
00:41:04,600 --> 00:41:07,910
�ooee!

258
00:41:23,280 --> 00:41:25,480
It's the law of the sea.

259
00:41:25,480 --> 00:41:27,920
I've seen the like done before.

260
00:41:27,920 --> 00:41:29,512
It tastes like pork.

261
00:41:30,560 --> 00:41:32,040
I couldn't.

262
00:41:32,040 --> 00:41:34,440
Don't be a fool.

263
00:41:34,440 --> 00:41:36,556
I'll have no hand in it.

264
00:41:39,080 --> 00:41:41,400
I'll not hang for it.

265
00:41:41,400 --> 00:41:45,000
I'll eat the first piece myself
but we all take part.

266
00:41:45,000 --> 00:41:46,433
We agreed?

267
00:41:51,000 --> 00:41:52,513
Pearce?

268
00:41:53,960 --> 00:41:55,393
What say you?

269
00:42:14,320 --> 00:42:15,753
Aye.

270
00:42:22,840 --> 00:42:24,751
Who then?

271
00:42:28,360 --> 00:42:29,360
Dalton.

272
00:42:29,360 --> 00:42:31,880
Dalton?!

273
00:42:31,880 --> 00:42:34,400
What about Bodenham?

274
00:42:34,400 --> 00:42:36,391
Are we agreed?

275
00:42:53,160 --> 00:42:55,390
Tell the others to get walking.

276
00:45:04,600 --> 00:45:06,511
What about a song, William?

277
00:45:09,680 --> 00:45:13,080
Fire in my belly.

278
00:45:13,080 --> 00:45:15,640
I'm so hungry I could eat my own arse.

279
00:45:20,040 --> 00:45:21,871
Sorry I am, William.

280
00:45:25,280 --> 00:45:26,793
Rest yourself.

281
00:46:14,760 --> 00:46:15,880
Fuck! Fuck!

282
00:46:19,480 --> 00:46:22,920
You fuck...

283
00:46:22,920 --> 00:46:24,717
Keep quiet!

284
00:46:26,520 --> 00:46:29,956
You animal!
You filthy English scum!

285
00:46:31,120 --> 00:46:34,032
- Goddamn you to hell!
- Not one more fucking word!

286
00:46:40,640 --> 00:46:43,360
We have to bleed him.
We can't eat him if we don't.

287
00:46:43,360 --> 00:46:45,555
Eat him?
-Pearce, come here.

288
00:46:48,800 --> 00:46:50,320
Mathers, grab his legs.

289
00:47:02,080 --> 00:47:03,513
Grab his legs.

290
00:47:11,960 --> 00:47:13,393
Fuck!

291
00:47:20,080 --> 00:47:22,360
Tie his legs.

292
00:47:22,360 --> 00:47:23,880
Tie his legs over there.

293
00:47:23,880 --> 00:47:25,313
Over there.

294
00:47:41,840 --> 00:47:44,000
Will you not eat?

295
00:47:44,000 --> 00:47:46,080
Never.

296
00:47:46,080 --> 00:47:48,560
They won't eat.

297
00:47:48,560 --> 00:47:50,357
Then they die.

298
00:47:52,320 --> 00:47:54,080
Eight days to get here.

299
00:47:54,080 --> 00:47:56,036
At least 1 0 to get back.

300
00:47:59,400 --> 00:48:00,880
Is there a man here

301
00:48:00,880 --> 00:48:04,111
who thinks he could live without food
for another 1 0 days?

302
00:49:15,600 --> 00:49:19,036
Stay with me. We're leaving.

303
00:49:32,960 --> 00:49:35,160
What?

304
00:49:35,160 --> 00:49:37,240
Oh, Christ, they've gone!

305
00:49:37,240 --> 00:49:38,720
- Settle down.
- They've gone!

306
00:49:38,720 --> 00:49:40,320
- What are you saying?
- They've gone, they've gone!

307
00:49:40,320 --> 00:49:41,753
They've gone!

308
00:49:42,920 --> 00:49:47,755
William Kennerly and Little Brown.
They've gone.

309
00:49:48,800 --> 00:49:50,280
Well, are we after them?

310
00:49:50,280 --> 00:49:53,600
If they make it back, we're as good
as hanged. Let's go after them.

311
00:49:53,600 --> 00:49:55,397
They'll be dead in two days.

312
00:49:59,760 --> 00:50:01,193
Come back!

313
00:50:03,320 --> 00:50:04,958
Come back!

314
00:50:07,920 --> 00:50:09,353
Kennerly!

315
00:50:11,480 --> 00:50:13,789
Little Brown! William Kennerly!

316
00:50:16,280 --> 00:50:17,800
William Kennerly, come back!

317
00:50:17,800 --> 00:50:20,075
William Kennerly!

318
00:50:22,800 --> 00:50:24,358
Little Brown!

319
00:50:44,800 --> 00:50:46,552
This could have been me.

320
00:50:47,920 --> 00:50:49,876
I was sleeping right next to him.

321
00:50:51,760 --> 00:50:53,637
No. No, it couldn't.

322
00:51:30,760 --> 00:51:33,080
No fucking way!

323
00:51:33,080 --> 00:51:34,880
He's right. We won't make it.

324
00:51:34,880 --> 00:51:36,791
Look at the bloody thing!

325
00:51:37,960 --> 00:51:39,440
We cross here.

326
00:51:39,440 --> 00:51:41,670
You're mad, the lot of you.

327
00:51:42,800 --> 00:51:44,280
We have to cross.

328
00:51:44,280 --> 00:51:45,800
I can't swim.

329
00:51:45,800 --> 00:51:47,280
Neither can Bodenham.

330
00:51:47,280 --> 00:51:48,440
You swim fine.

331
00:51:48,440 --> 00:51:49,919
Not in that.

332
00:52:47,840 --> 00:52:49,273
Here, stop here.

333
00:53:11,800 --> 00:53:13,472
Will you light the fire, lad?

334
00:53:14,520 --> 00:53:15,953
You light it?

335
00:53:20,600 --> 00:53:22,272
Bodenham.

336
00:53:24,560 --> 00:53:25,993
Bodenham.

337
00:54:08,200 --> 00:54:11,040
You didn't need to do that.

338
00:54:11,040 --> 00:54:13,240
He was just a kid.

339
00:54:13,240 --> 00:54:15,640
We have plenty of food.

340
00:54:15,640 --> 00:54:17,232
We rest tomorrow.

341
00:54:18,800 --> 00:54:20,518
Agreed?

342
00:54:30,600 --> 00:54:32,192
We'll need more wood.

343
00:55:50,040 --> 00:55:52,600
Easier than cutting sheep.

344
00:56:08,040 --> 00:56:11,000
Oh, Jesus fucking Christ!

345
00:56:11,000 --> 00:56:14,200
At last Dalton can rest in peace.

346
00:56:15,360 --> 00:56:16,793
Aargh!

347
00:56:54,680 --> 00:56:56,557
Pearce, hand me your axe.

348
00:56:59,760 --> 00:57:01,352
Hand me your axe!

349
00:57:04,400 --> 00:57:06,391
We need the kindling.

350
00:58:11,720 --> 00:58:13,676
No way.

351
00:58:14,720 --> 00:58:17,080
- Carry it.
- I'm not carrying it.

352
00:58:17,080 --> 00:58:18,680
Yes, you fucking are.

353
00:58:18,680 --> 00:58:20,796
Give me the axe and I'll carry the bag.

354
00:58:34,040 --> 00:58:35,871
Suit yourself.

355
00:59:53,680 --> 00:59:56,069
We have to get away
from Greenhill and Travers.

356
00:59:58,080 --> 01:00:02,949
If we don't, one of us will be
served as the others before long.

357
01:00:04,600 --> 01:00:07,560
I wouldn't be surprised if that was
their plan from the beginning.

358
01:00:07,560 --> 01:00:10,640
To come out here without food
is madness.

359
01:00:10,640 --> 01:00:13,200
Greenhill ain't mad.
He's a cunning fuck.

360
01:00:14,440 --> 01:00:16,480
We have to get away.

361
01:00:16,480 --> 01:00:18,232
You hear what I'm saying?

362
01:00:19,920 --> 01:00:21,520
I'm leaving.

363
01:00:21,520 --> 01:00:23,320
Are you with me or with them?

364
01:00:23,320 --> 01:00:25,231
You. I'm with you.

365
01:00:26,440 --> 01:00:28,237
We leave in the dark of morning.

366
01:00:47,480 --> 01:00:50,440
Just drink it.

367
01:00:50,440 --> 01:00:53,480
To brew the strength
from the leaves needs time.

368
01:00:53,480 --> 01:00:56,631
You've gone mad.

369
01:01:42,360 --> 01:01:43,918
Smells rotten.

370
01:01:46,640 --> 01:01:50,110
Soup. For the hunger.

371
01:02:17,040 --> 01:02:17,995
Arggh!

372
01:02:25,080 --> 01:02:27,753
Stay back, wife! Or I'll kill him dead!

373
01:02:31,600 --> 01:02:33,875
Oh, Christ, my head.

374
01:02:35,200 --> 01:02:36,633
Pearce.

375
01:02:48,640 --> 01:02:51,791
I'll sit for the night.

376
01:02:53,240 --> 01:02:57,074
If either of you come near me,
I will use the axe.

377
01:04:06,360 --> 01:04:09,113
I'm OK. Just...thirsty.

378
01:04:16,400 --> 01:04:18,595
We need to leave them behind.

379
01:04:26,720 --> 01:04:28,950
Have to keep an eye on them, Pearce.

380
01:04:30,400 --> 01:04:32,470
He's a taste for blood, that one.

381
01:04:40,720 --> 01:04:43,600
As soon as we lose sight of them...

382
01:04:43,600 --> 01:04:45,113
..that's when we make our move.

383
01:04:47,800 --> 01:04:49,677
Pearce.

384
01:05:02,400 --> 01:05:04,709
You miserable fuck.

385
01:05:22,680 --> 01:05:25,160
- Fucking kill this cunt!
-I can't.

386
01:05:25,160 --> 01:05:27,071
- I fucking can't.
- Do it!

387
01:05:28,600 --> 01:05:30,640
Born to whores and nothing!

388
01:05:30,640 --> 01:05:32,870
I've no fear of you - not one!

389
01:05:40,160 --> 01:05:43,118
I'll eat your fucking souls!

390
01:06:08,240 --> 01:06:11,755
Cooee!

391
01:06:16,160 --> 01:06:18,040
No, Pearce.

392
01:06:18,040 --> 01:06:19,792
You're not dead yet.

393
01:07:32,320 --> 01:07:34,800
I have the hunger
on me now, Robert.

394
01:07:34,800 --> 01:07:37,960
Do you feel it, Pearce?
Do you feel the hunger?

395
01:07:37,960 --> 01:07:40,080
I've not the need.

396
01:07:40,080 --> 01:07:41,991
How are you doing? Hungry, Pearce?

397
01:07:43,720 --> 01:07:46,480
I preferred Dalton to the rest.

398
01:07:46,480 --> 01:07:48,550
Tender, like lamb.

399
01:07:51,720 --> 01:07:54,560
Mathers gave me the wind of December.

400
01:07:54,560 --> 01:07:58,348
Fitting, as the mad cunt never shut up.

401
01:08:00,040 --> 01:08:03,032
- And Bodenham...
- Quiet your fucking mouth.

402
01:08:13,400 --> 01:08:15,800
You've a hungry look
on you, Pearce.

403
01:08:15,800 --> 01:08:18,600
You're in need of eating.

404
01:08:20,160 --> 01:08:21,275
Pearce.

405
01:08:25,080 --> 01:08:27,320
Pearce.

406
01:08:27,320 --> 01:08:29,117
Pearce. Pearce.

407
01:08:30,200 --> 01:08:31,680
Pearce.

408
01:08:31,680 --> 01:08:34,000
Pearce!

409
01:08:34,000 --> 01:08:36,760
Pearce.

410
01:08:37,920 --> 01:08:40,720
Arggh! Arggh! Robert! Fucking thing.

411
01:08:40,720 --> 01:08:42,840
Where is it? Fucking snake.

412
01:08:42,840 --> 01:08:44,432
Where is it? Hey.

413
01:08:46,120 --> 01:08:48,040
Fucking...get the fucker!

414
01:08:48,040 --> 01:08:49,996
- Ugh!
- Shh.

415
01:08:51,800 --> 01:08:53,756
Oh, fuck, Robert.

416
01:08:56,920 --> 01:08:59,798
You'll be fine. You'll be fine.

417
01:09:01,000 --> 01:09:02,831
Come on.

418
01:09:04,200 --> 01:09:06,120
Shh.

419
01:09:06,120 --> 01:09:08,315
Come on. Come on.

420
01:09:09,760 --> 01:09:12,718
Come here. Come here.

421
01:09:20,960 --> 01:09:22,518
Come on.

422
01:10:07,360 --> 01:10:10,240
Leave me. I won't make it.

423
01:10:10,240 --> 01:10:11,958
Go on without me, Robert.

424
01:10:22,880 --> 01:10:25,269
I don't like the way
that he looks at me.

425
01:10:58,640 --> 01:11:03,760
 Well met, well met, my own true love

426
01:11:03,760 --> 01:11:08,280
"Well met, well met," cried she

427
01:11:08,280 --> 01:11:12,920
I've just returned
from the salt, salt sea

428
01:11:12,920 --> 01:11:16,800
And it's all for the love of thee

429
01:11:16,800 --> 01:11:23,040
I could have married
the King's daughter there

430
01:11:23,040 --> 01:11:27,000
She would have married me

431
01:11:27,000 --> 01:11:32,320
I've forsaken the crown of gold

432
01:11:32,320 --> 01:11:36,360
All for the love of thee

433
01:11:36,360 --> 01:11:38,476
All for the... 


434
01:11:42,960 --> 01:11:44,720
I'm here.

435
01:11:44,720 --> 01:11:47,792
Oh, no.

436
01:12:08,400 --> 01:12:10,072
He'll not make it.

437
01:12:12,400 --> 01:12:15,870
All for the love of thee... 

438
01:12:45,840 --> 01:12:50,118
No, no! No!

439
01:12:51,560 --> 01:12:54,996
You'll be taking
your leaving of me now, Robert.

440
01:12:59,640 --> 01:13:02,040
You'll make it without me.

441
01:13:02,040 --> 01:13:05,510
Please, Robert. I'm so tired.

442
01:13:08,880 --> 01:13:10,552
I'm so tired.

443
01:13:12,120 --> 01:13:13,760
I need sleep.

444
01:13:14,760 --> 01:13:18,833
You can't...
Wait. Don't.

445
01:13:20,000 --> 01:13:22,036
Please, Robert. Please!

446
01:13:28,840 --> 01:13:30,319
No.

447
01:13:33,680 --> 01:13:36,320
You'll make it without me.

448
01:13:36,320 --> 01:13:38,038
I need sleep.

449
01:13:39,800 --> 01:13:42,360
You can't... Wait.

450
01:13:42,360 --> 01:13:45,360
Don't! Please, Robert.

451
01:13:45,360 --> 01:13:46,920
Please!

452
01:14:01,960 --> 01:14:04,872
Robert...

453
01:14:07,840 --> 01:14:08,795
No.

454
01:14:31,440 --> 01:14:32,873
Don't touch me.

455
01:14:38,200 --> 01:14:39,520
Pearce is going to...

456
01:14:39,520 --> 01:14:42,840
Pearce... Pearce is going to...

457
01:14:42,840 --> 01:14:44,840
Keep him away from me.

458
01:14:44,840 --> 01:14:46,640
- Shh.
- Don't leave me.

459
01:14:46,640 --> 01:14:48,280
Shh. Shh.

460
01:16:31,600 --> 01:16:33,880
Please...don't...

461
01:22:03,720 --> 01:22:05,358
Where now?

462
01:23:50,720 --> 01:23:52,153
Have you...

463
01:23:54,400 --> 01:23:57,472
Have you ever thought at it, Alex?

464
01:24:02,640 --> 01:24:05,560
This thing...

465
01:24:05,560 --> 01:24:08,632
This thing that's...

466
01:24:09,720 --> 01:24:13,679
It grows fat on itself. This is...

467
01:24:29,360 --> 01:24:32,716
It's...burning alive, Alex.
We're burning alive.

468
01:24:41,160 --> 01:24:43,840
Logs for the fire.

469
01:24:43,840 --> 01:24:45,751
Just logs for the fire, Alex.

470
01:24:56,080 --> 01:24:58,548
You don't fucking say much,
do you, Pearce?

471
01:25:06,160 --> 01:25:07,957
What brought you here?

472
01:25:14,000 --> 01:25:15,513
Shoes.

473
01:25:16,720 --> 01:25:18,551
Six pairs of shoes.

474
01:25:43,040 --> 01:25:45,031
We have to keep moving.

475
01:26:06,000 --> 01:26:07,911
We're close.

476
01:26:27,760 --> 01:26:30,080
�ooeeee!

477
01:29:06,240 --> 01:29:08,310
Hard of sleep, Robert?

478
01:29:10,120 --> 01:29:11,838
Aye.

479
01:31:27,360 --> 01:31:29,351
Get on with it.

480
01:31:33,360 --> 01:31:35,430
Arggh!

