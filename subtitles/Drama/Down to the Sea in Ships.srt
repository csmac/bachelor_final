﻿1
00:00:00,000 --> 00:00:05,438
Subtitles by NoTearSwoman

2
00:00:05,438 --> 00:00:08,378
- Min Jae! - Min Jae!
Subtitles by NoTearSwoman

3
00:00:08,378 --> 00:00:10,430
- Min Jae! Where are you? - Min Jae!
Subtitles by NoTearSwoman

4
00:00:10,430 --> 00:00:10,938
- Min Jae! Where are you? - Min Jae!

5
00:00:10,938 --> 00:00:12,578
- Min Jae! - Min Jae!

6
00:00:12,578 --> 00:00:15,538
- Min Jae! - Min Jae! Where are you?

7
00:00:15,578 --> 00:00:17,038
- Min Jae! - Min Jae!

8
00:00:17,308 --> 00:00:19,008
- Min Jae! - Min Jae!

9
00:00:19,038 --> 00:00:21,808
- Min Jae! - Where are you?

10
00:00:21,938 --> 00:00:24,708
- Min Jae! Where is he? - Oh, no.

11
00:00:27,732 --> 00:00:28,832
Kid.

12
00:00:31,372 --> 00:00:32,872
Do you want a candy?

13
00:00:33,602 --> 00:00:34,832
Ta-da.

14
00:00:38,232 --> 00:00:42,502
I'll give you all these if you come with me.

15
00:00:43,172 --> 00:00:44,702
Where to?

16
00:00:47,202 --> 00:00:51,572
Does he think he's the only one who employs people?

17
00:00:52,572 --> 00:00:56,172
This mess is all Mr. Kwon's fault.

18
00:00:57,432 --> 00:01:01,202
- Min Jae. Can you help us? - Have you seen a little boy?

19
00:01:01,242 --> 00:01:02,402
Soo In.

20
00:01:06,102 --> 00:01:08,502
What are you doing here?

21
00:01:08,932 --> 00:01:10,202
Dad.

22
00:01:11,132 --> 00:01:12,372
What's wrong?

23
00:01:14,372 --> 00:01:15,702
Min Jae...

24
00:01:17,172 --> 00:01:19,232
Min Jae's missing.

25
00:01:19,402 --> 00:01:22,532
What? Min Jae's missing?

26
00:01:26,902 --> 00:01:29,702
- Min Jae! - Min Jae!

27
00:01:29,902 --> 00:01:31,002
Min Jae!

28
00:01:31,402 --> 00:01:34,772
- Min Jae! - Min Jae!

29
00:01:35,002 --> 00:01:37,002
- Where are you, Min Jae? - Lady.

30
00:01:37,002 --> 00:01:38,702
Did you see a little boy?

31
00:01:38,832 --> 00:01:40,802
- No. - Oh dear.

32
00:01:40,802 --> 00:01:41,972
- Min Jae! - Min Jae!

33
00:01:41,972 --> 00:01:43,532
- Min Jae! - Where are you?

34
00:01:43,532 --> 00:01:46,232
Min Jae! Yoon Min Jae!

35
00:01:46,772 --> 00:01:48,172
Min Jae!

36
00:01:49,702 --> 00:01:50,872
Min Jae!

37
00:01:55,902 --> 00:01:58,572
Min Jae. Yoon Min Jae!

38
00:02:01,302 --> 00:02:03,732
- Dad! - Shoot.

39
00:02:03,732 --> 00:02:04,802
Let's go.

40
00:02:06,642 --> 00:02:07,702
Min Jae.

41
00:02:08,271 --> 00:02:10,201
Here you are.

42
00:02:11,172 --> 00:02:13,832
Who were those men?

43
00:02:13,832 --> 00:02:16,932
- They said they'd give me a candy. - What?

44
00:02:19,802 --> 00:02:21,072
Where did they go?

45
00:02:21,232 --> 00:02:23,372
You silly boy.

46
00:02:23,372 --> 00:02:26,142
You can't go off with just anyone.

47
00:02:26,702 --> 00:02:27,902
Oh dear.

48
00:02:28,472 --> 00:02:30,802
- Min Jae. - Soo In.

49
00:02:30,972 --> 00:02:32,072
Are you okay?

50
00:02:32,672 --> 00:02:34,632
Where did you go off to?

51
00:02:34,632 --> 00:02:36,602
Why didn't you watch him?

52
00:02:37,632 --> 00:02:39,732
If you brought him out here, keep him safe.

53
00:02:40,202 --> 00:02:42,832
You shouldn't leave him alone in this busy place.

54
00:02:43,132 --> 00:02:44,672
I'm sorry.

55
00:02:45,172 --> 00:02:48,172
What if you'd lost him forever?

56
00:02:48,332 --> 00:02:52,432
Dad, don't yell at Soo In.

57
00:02:53,072 --> 00:02:56,272
What? Okay, I won't.

58
00:02:57,102 --> 00:03:01,672
If this happens again, I will punish you properly, okay?

59
00:03:02,172 --> 00:03:03,232
Okay.

60
00:03:03,332 --> 00:03:04,802
What a scare.

61
00:03:04,802 --> 00:03:08,332
Come on, Min Jae. Let's go home.

62
00:03:09,732 --> 00:03:10,772
Come along.

63
00:03:19,802 --> 00:03:21,272
We're home.

64
00:03:23,332 --> 00:03:24,532
Listen.

65
00:03:24,802 --> 00:03:28,632
You can't just go off with strangers.

66
00:03:29,172 --> 00:03:32,772
If you want candy, tell Dad and I'll buy you some.

67
00:03:32,802 --> 00:03:34,602
- Okay? - Yes.

68
00:03:36,172 --> 00:03:38,572
Are you that happy?

69
00:03:38,572 --> 00:03:40,732
Yes, I'm so happy.

70
00:03:41,802 --> 00:03:44,502
You're so sweet. Now you can play.

71
00:03:44,502 --> 00:03:45,902
- Okay. - Good boy.

72
00:03:53,202 --> 00:03:55,572
Min Jae.

73
00:03:56,202 --> 00:03:59,532
Why do you have that? Give it to me.

74
00:04:00,972 --> 00:04:03,432
No. I will play with it.

75
00:04:04,372 --> 00:04:07,272
That isn't Daddy's. Give it to me.

76
00:04:07,272 --> 00:04:09,702
No. I want to play with it.

77
00:04:10,302 --> 00:04:13,502
Do you? Okay, then. But promise...

78
00:04:13,732 --> 00:04:15,902
to give it to me when I ask for it.

79
00:04:15,972 --> 00:04:18,602
You can't lose it either, okay?

80
00:04:18,802 --> 00:04:20,502
Okay, Dad.

81
00:04:22,442 --> 00:04:24,202
You cheeky boy.

82
00:04:37,442 --> 00:04:38,472
Are you okay?

83
00:04:38,472 --> 00:04:41,902
Your dad's really scary when he's angry.

84
00:04:42,202 --> 00:04:44,172
It was my fault.

85
00:04:44,832 --> 00:04:48,532
It wasn't. Min Jae walked off on his own.

86
00:04:49,172 --> 00:04:51,132
He's still little.

87
00:04:52,702 --> 00:04:54,902
I should've watched him.

88
00:04:58,102 --> 00:05:01,172
I was so scared earlier.

89
00:05:01,202 --> 00:05:03,132
I thought we'd lost him.

90
00:05:04,442 --> 00:05:06,032
It's fine now that we found him.

91
00:05:07,072 --> 00:05:08,942
Yes, it's a huge relief.

92
00:05:10,532 --> 00:05:14,002
The hairpin suits you really well. I made the perfect choice.

93
00:05:14,802 --> 00:05:17,272
Does it look nice on me too?

94
00:05:17,572 --> 00:05:18,602
Yes.

95
00:05:18,902 --> 00:05:21,102
Don't you ever lose that hairpin.

96
00:05:21,102 --> 00:05:23,502
It's a symbol of our friendship.

97
00:05:24,902 --> 00:05:27,272
Let's be friends forever, okay?

98
00:05:27,932 --> 00:05:28,972
Okay.

99
00:05:43,932 --> 00:05:46,772
No one would beat up a man like that...

100
00:05:46,772 --> 00:05:49,072
unless they were trying to kill him.

101
00:05:49,972 --> 00:05:52,132
What if he never wakes up?

102
00:05:52,972 --> 00:05:55,802
I don't think that would happen.

103
00:05:56,832 --> 00:05:58,832
The old sayings are always right.

104
00:05:59,132 --> 00:06:02,002
Make someone shed tears and you shed blood.

105
00:06:03,002 --> 00:06:04,772
He paid for what he did.

106
00:06:05,072 --> 00:06:07,832
You reap what you sow.

107
00:06:09,272 --> 00:06:13,232
Now that they found the swindler Mr. Kwon,

108
00:06:13,232 --> 00:06:16,202
will Dong Chul get his money back?

109
00:06:16,302 --> 00:06:18,832
I wonder. Do you know anything, Soon Ok?

110
00:06:18,832 --> 00:06:20,302
Did he say anything?

111
00:06:20,332 --> 00:06:23,032
Only that they have to wait until the man wakes up.

112
00:06:23,032 --> 00:06:26,472
I guess no one can take money from an unconscious man.

113
00:06:26,732 --> 00:06:30,372
Is it true that they don't know if he will wake up?

114
00:06:30,372 --> 00:06:32,502
Yes, apparently so.

115
00:06:32,572 --> 00:06:36,702
He needs to wake up if we're to get the money or punish him.

116
00:06:37,032 --> 00:06:39,072
What do you think happened to him?

117
00:06:39,332 --> 00:06:41,302
Was he robbed?

118
00:06:41,332 --> 00:06:43,272
He paid the price.

119
00:06:43,332 --> 00:06:46,672
He took from others and made them shed bloody tears.

120
00:06:46,872 --> 00:06:48,472
He's self-sufficient.

121
00:06:49,372 --> 00:06:51,602
What? "Self-sufficient"?

122
00:06:53,402 --> 00:06:55,672
You meant to say he reaped what he sowed, right?

123
00:06:57,632 --> 00:06:59,632
Yes, he reaped what he sowed.

124
00:07:00,402 --> 00:07:01,802
My tongue slipped.

125
00:07:01,802 --> 00:07:05,132
Your tongue slips a bit too often.

126
00:07:05,132 --> 00:07:08,002
What? Do you think I'm that ignorant?

127
00:07:08,002 --> 00:07:09,832
What do you take me for?

128
00:07:14,732 --> 00:07:16,772
You couldn't take a watch from a kid?

129
00:07:17,072 --> 00:07:18,132
I apologize.

130
00:07:18,572 --> 00:07:21,832
Do anything it takes to bring it to me tomorrow.

131
00:07:25,902 --> 00:07:27,672
Mr. Jang.

132
00:07:29,802 --> 00:07:32,502
Dong Chul's son has your watch...

133
00:07:32,502 --> 00:07:35,902
which means he didn't give it to the police.

134
00:07:36,032 --> 00:07:39,132
Perhaps he thinks it belongs to Mr. Kwon.

135
00:07:40,532 --> 00:07:41,572
So what?

136
00:07:41,972 --> 00:07:45,772
Instead of trying to take it back with force,

137
00:07:45,772 --> 00:07:47,172
why not wait and see?

138
00:07:48,032 --> 00:07:52,002
No one knows that we're involved in Mr. Kwon's beating.

139
00:07:52,172 --> 00:07:55,732
Trying to get the watch back could turn things against us.

140
00:07:58,602 --> 00:08:01,702
I think it would be better to decide what to do next...

141
00:08:01,872 --> 00:08:04,672
depending on Mr. Kwon's condition.

142
00:08:06,072 --> 00:08:09,002
Is he still unconscious?

143
00:08:09,832 --> 00:08:10,932
Yes.

144
00:08:11,232 --> 00:08:15,502
What if he wakes up and tells the police about us?

145
00:08:16,032 --> 00:08:18,532
I have a plan in case that happens.

146
00:08:19,402 --> 00:08:20,502
What plan?

147
00:08:20,502 --> 00:08:23,042
I will tell you about it then.

148
00:08:23,042 --> 00:08:26,542
You don't have to worry, sir.

149
00:08:30,702 --> 00:08:33,702
It would've been nice if you were Snow White.

150
00:08:34,202 --> 00:08:36,202
Don't you want to play the princess?

151
00:08:39,902 --> 00:08:42,232
Did your teacher tell Se Young...

152
00:08:42,332 --> 00:08:44,502
to play Snow White?

153
00:08:45,472 --> 00:08:47,902
No, Se Young said she wanted the role.

154
00:08:47,932 --> 00:08:49,772
Why didn't you do the same?

155
00:08:49,772 --> 00:08:52,472
I'd have said I wanted to be the princess.

156
00:08:54,232 --> 00:08:57,042
That way you get to wear the princess dress.

157
00:08:58,042 --> 00:09:01,372
I'm jealous of Se Young. She gets to wear a dress.

158
00:09:01,372 --> 00:09:03,572
I want to wear a pretty dress.

159
00:09:15,502 --> 00:09:18,132
(Snow White)

160
00:09:18,132 --> 00:09:21,702
The table's full of delicious-looking food.

161
00:09:28,702 --> 00:09:30,732
Will it be okay to eat a little?

162
00:09:35,372 --> 00:09:38,332
Oh, dear. I ate everything up.

163
00:09:46,802 --> 00:09:48,502
What's your next line?

164
00:09:50,432 --> 00:09:51,632
It's...

165
00:09:57,272 --> 00:10:00,172
"I'll ask for forgiveness when the owner comes back."

166
00:10:01,132 --> 00:10:02,932
- That's it. - Se Young.

167
00:10:03,432 --> 00:10:06,172
I told you to memorize everything by today.

168
00:10:06,232 --> 00:10:10,802
You're the main character and you still don't know your lines.

169
00:10:13,002 --> 00:10:14,332
This won't do.

170
00:10:15,102 --> 00:10:17,732
Soo In, you play Snow White.

171
00:10:18,672 --> 00:10:21,572
Se Young, you have two days to memorize the queen's lines.

172
00:10:21,772 --> 00:10:23,042
Miss!

173
00:10:27,202 --> 00:10:29,432
Se Young, wait for me.

174
00:10:42,102 --> 00:10:45,372
Hello, Se Young. You're back early.

175
00:10:47,532 --> 00:10:50,772
What's wrong with her? Is she upset?

176
00:10:59,802 --> 00:11:02,802
Se Young, have you still not memorized your lines yet?

177
00:11:04,432 --> 00:11:06,402
There are too many lines.

178
00:11:06,872 --> 00:11:11,172
Soo In seems to have memorized hers and yours as well.

179
00:11:12,232 --> 00:11:14,932
I told you to memorize everything by today.

180
00:11:15,102 --> 00:11:19,672
You're the main character and you still don't know your lines.

181
00:11:19,732 --> 00:11:20,902
This won't do.

182
00:11:21,832 --> 00:11:24,532
Soo In, you play Snow White.

183
00:11:25,432 --> 00:11:28,232
Se Young, you have two days to memorize the queen's lines.

184
00:11:30,402 --> 00:11:33,232
Se Young, look at this. Mommy had...

185
00:11:34,702 --> 00:11:35,872
What's wrong?

186
00:11:37,932 --> 00:11:40,002
I hate everything.

187
00:11:40,602 --> 00:11:43,132
- Se Young. - I hate it all.

188
00:11:43,732 --> 00:11:46,202
What's wrong? Why are you crying?

189
00:11:48,202 --> 00:11:49,672
Why are you crying?

190
00:11:49,732 --> 00:11:51,472
I lost it!

191
00:11:51,932 --> 00:11:52,972
What do you mean?

192
00:11:53,102 --> 00:11:57,032
My role. My teacher told Soo In to play Snow White.

193
00:11:57,102 --> 00:11:58,202
What?

194
00:11:58,872 --> 00:11:59,932
Why?

195
00:11:59,932 --> 00:12:02,402
Because I couldn't remember my lines.

196
00:12:03,572 --> 00:12:05,702
How could she do something like that?

197
00:12:06,302 --> 00:12:09,072
What about this dress? You can't wear it?

198
00:12:09,072 --> 00:12:10,702
I don't know!

199
00:12:23,672 --> 00:12:27,102
Dong Chul, check the chairs too. Some are wonky.

200
00:12:27,132 --> 00:12:30,232
Okay. Tell me if anything else needs fixing.

201
00:12:30,372 --> 00:12:33,232
I think that's all. Right, Soon Ok?

202
00:12:33,272 --> 00:12:35,902
Yes. Thank you.

203
00:12:39,072 --> 00:12:42,132
Soon Ok, I heard that Soo In came first in her last test.

204
00:12:43,732 --> 00:12:46,132
She never let go of 1st place since she started 4th grade.

205
00:12:46,132 --> 00:12:47,932
Who does she take after?

206
00:12:51,402 --> 00:12:55,372
Dong Chul, do you know that Soo In's her class captain?

207
00:12:55,772 --> 00:12:57,902
What? Class captain?

208
00:12:57,972 --> 00:13:01,972
See? Honestly, how could you care less about your own daughter?

209
00:13:02,132 --> 00:13:06,202
Love Soo In and Jung In half as much as you love Min Jae.

210
00:13:06,672 --> 00:13:08,372
I'm not that bad.

211
00:13:08,372 --> 00:13:11,972
What would you be like if Min Jae came first and was class captain?

212
00:13:11,972 --> 00:13:13,632
It's so obvious.

213
00:13:16,372 --> 00:13:17,502
Mom, mom.

214
00:13:19,572 --> 00:13:21,032
Hi, I'm back.

215
00:13:21,402 --> 00:13:23,432
Hi there.

216
00:13:23,902 --> 00:13:26,132
Why did you come here and not go home?

217
00:13:27,932 --> 00:13:29,772
Do you have something to brag about?

218
00:13:29,832 --> 00:13:32,002
You come here when you want to brag.

219
00:13:33,502 --> 00:13:36,332
Did you come first in another test?

220
00:13:37,572 --> 00:13:39,172
It's not that.

221
00:13:39,802 --> 00:13:42,532
I'm playing the lead in the Snow White play.

222
00:13:42,872 --> 00:13:44,772
What? What's that?

223
00:13:44,772 --> 00:13:47,972
The fourth-graders put on a play at the school festival.

224
00:13:48,202 --> 00:13:50,702
Wasn't Se Young going to be Snow White?

225
00:13:50,702 --> 00:13:53,002
- The teacher changed our roles. - Really?

226
00:13:53,272 --> 00:13:56,632
So you get the play the lead?

227
00:13:57,132 --> 00:13:59,532
Our whole family should be there.

228
00:14:00,472 --> 00:14:01,802
Will you come as well?

229
00:14:02,172 --> 00:14:04,372
Yes, of course.

230
00:14:04,502 --> 00:14:08,902
Good. No dad should skip a play when his daughter's the lead.

231
00:14:13,372 --> 00:14:16,672
That's right. I should be there.

232
00:14:17,032 --> 00:14:18,402
Really?

233
00:14:19,072 --> 00:14:22,432
Of course. You're playing the main character.

234
00:14:22,432 --> 00:14:24,032
I should be there.

235
00:14:32,432 --> 00:14:34,972
- I'm home. - Welcome home.

236
00:14:35,502 --> 00:14:36,832
Do you have good news?

237
00:14:37,272 --> 00:14:40,702
I got to play Snow White in the school play.

238
00:14:40,972 --> 00:14:43,702
Really? That's great news.

239
00:14:44,302 --> 00:14:47,872
Do you want me to make you a dress?

240
00:14:48,002 --> 00:14:50,202
- Can you? - Yes.

241
00:14:50,532 --> 00:14:52,932
I was a bit worried about that.

242
00:14:53,072 --> 00:14:56,102
I'm glad to play the lead but I didn't have a costume.

243
00:14:56,632 --> 00:14:58,232
Don't worry, I'll make you one.

244
00:14:58,772 --> 00:15:00,002
Thank you.

245
00:15:10,302 --> 00:15:13,702
Are you still in bed? Get up.

246
00:15:13,902 --> 00:15:17,132
- I don't want to. - Get up.

247
00:15:19,602 --> 00:15:23,732
I'm upset that Soo In always comes first in every test...

248
00:15:23,932 --> 00:15:25,832
and now she took your role.

249
00:15:26,172 --> 00:15:28,202
This is so upsetting.

250
00:15:28,832 --> 00:15:32,832
I told my parents to come and watch. This is such an embarrassment.

251
00:15:36,802 --> 00:15:40,632
Why don't you go and talk to Soo In?

252
00:15:40,872 --> 00:15:42,802
- About what? - What else?

253
00:15:42,972 --> 00:15:44,372
Ask if you can swap back.

254
00:15:44,632 --> 00:15:46,402
No. That hurts my pride.

255
00:15:46,402 --> 00:15:48,302
Pride isn't the issue here.

256
00:15:48,372 --> 00:15:50,632
We can't go back on the teacher's order.

257
00:15:50,632 --> 00:15:54,032
She won't have a choice if Soo In says she can't do it.

258
00:15:54,032 --> 00:15:56,272
You were so nice to Soo In.

259
00:15:56,272 --> 00:15:58,132
I'm sure she'll say yes.

260
00:16:00,632 --> 00:16:03,472
Go and talk to her.

261
00:16:26,602 --> 00:16:29,072
- Hello. - Se Young.

262
00:16:29,772 --> 00:16:31,802
Is Soo In at home?

263
00:16:32,672 --> 00:16:35,032
Just a moment. Soo In.

264
00:16:40,872 --> 00:16:41,932
Se Young.

265
00:16:47,632 --> 00:16:50,272
You want to play Snow White?

266
00:16:50,632 --> 00:16:53,102
Yes. It was my role to begin with.

267
00:16:53,202 --> 00:16:55,172
Our teacher told us to change roles.

268
00:16:55,242 --> 00:16:59,132
If you say you can't do it, she'll give us our old roles.

269
00:17:01,472 --> 00:17:03,702
Do you not want to switch back?

270
00:17:05,902 --> 00:17:09,372
So you get the play the lead?

271
00:17:09,801 --> 00:17:12,531
Our whole family should be there.

272
00:17:13,202 --> 00:17:14,432
Will you come as well?

273
00:17:14,602 --> 00:17:16,772
Yes, of course.

274
00:17:16,972 --> 00:17:21,372
Good. No dad should skip a play when his daughter's the lead.

275
00:17:21,902 --> 00:17:24,372
That's right. I should be there.

276
00:17:24,832 --> 00:17:27,132
- Really? - Of course.

277
00:17:27,432 --> 00:17:31,202
You're playing the main character. I should be there.

278
00:17:34,602 --> 00:17:35,772
Sorry.

279
00:17:36,572 --> 00:17:38,832
You won't give up your role?

280
00:17:40,972 --> 00:17:43,242
How could you do this to me?

281
00:17:43,272 --> 00:17:45,832
I let you borrow all my books...

282
00:17:46,032 --> 00:17:49,072
and my dad even got you a music box...

283
00:17:49,072 --> 00:17:51,102
and you can't give this up for me?

284
00:17:52,532 --> 00:17:53,672
Sorry.

285
00:17:53,672 --> 00:17:55,702
You aren't my friend anymore.

286
00:17:57,802 --> 00:17:59,002
Se Young.

287
00:18:02,802 --> 00:18:04,932
Se Young. Se Young.

288
00:18:12,902 --> 00:18:13,972
Yoon Soo In.

289
00:18:15,032 --> 00:18:17,132
How could you do this to me?

290
00:18:19,872 --> 00:18:22,432
Did you and Se Young fight?

291
00:18:23,372 --> 00:18:24,402
No.

292
00:18:25,002 --> 00:18:26,002
Then what?

293
00:18:27,072 --> 00:18:30,902
I think she's a bit angry with me.

294
00:18:30,972 --> 00:18:33,202
I see.

295
00:18:34,132 --> 00:18:37,272
You two are close friends.

296
00:18:37,372 --> 00:18:39,932
She'll get over it soon. Don't worry.

297
00:18:40,772 --> 00:18:43,172
- I'm done. - Okay.

298
00:18:45,932 --> 00:18:48,332
Thanks for making me the costume.

299
00:18:48,932 --> 00:18:51,202
Don't thank me. It's nothing.

300
00:18:52,742 --> 00:18:53,932
So...

301
00:18:55,272 --> 00:18:58,132
Do you want to come and watch the play?

302
00:18:59,502 --> 00:19:01,202
Can I come?

303
00:19:03,202 --> 00:19:05,242
It's a bit embarrassing though.

304
00:19:07,272 --> 00:19:09,002
Okay. I'll be there.

305
00:19:09,702 --> 00:19:11,632
- I hope you will. - Okay.

306
00:19:27,672 --> 00:19:30,472
How did it go? She said yes, didn't she?

307
00:19:31,202 --> 00:19:33,242
No. She refused.

308
00:19:33,372 --> 00:19:36,102
What? Did she really say no?

309
00:19:36,332 --> 00:19:37,902
She really did.

310
00:19:46,602 --> 00:19:48,202
It's nice and sturdy now.

311
00:19:48,302 --> 00:19:50,902
Dong Chul's good with his hands.

312
00:19:52,242 --> 00:19:54,742
Soon Ok, did you see Soo In's face?

313
00:19:55,002 --> 00:19:56,242
What face?

314
00:19:56,602 --> 00:20:00,072
The face she made when Dong Chul said he'd come and watch.

315
00:20:00,242 --> 00:20:03,672
She doesn't say so, but she likes her dad.

316
00:20:04,672 --> 00:20:07,202
You appreciate what I did, don't you?

317
00:20:08,572 --> 00:20:10,272
- What did you do? - I put pressure...

318
00:20:10,272 --> 00:20:13,032
on Dong Chul to watch Soo In's play with us.

319
00:20:15,372 --> 00:20:18,972
We need to force them together if they're to get close. Right?

320
00:20:20,302 --> 00:20:21,302
Right.

321
00:20:23,432 --> 00:20:26,632
How is Mr. Kwon. Is he still unconscious?

322
00:20:27,132 --> 00:20:30,872
Yes. He still hasn't woken up.

323
00:20:31,932 --> 00:20:33,202
And the investigation?

324
00:20:33,532 --> 00:20:37,172
It's still ongoing, but Mr. Kwon's still alive,

325
00:20:37,572 --> 00:20:41,002
so the police aren't doing much at the moment.

326
00:20:41,072 --> 00:20:42,072
They're actually...

327
00:20:42,572 --> 00:20:46,432
having trouble because they can't reach Mr. Kwon's family.

328
00:20:46,672 --> 00:20:50,402
They won't manage that soon because he never had a fixed address.

329
00:20:50,802 --> 00:20:52,972
Keep me updated on Mr. Kwon's condition...

330
00:20:53,102 --> 00:20:54,772
and the police.

331
00:21:05,102 --> 00:21:06,972
I wonder why...

332
00:21:07,202 --> 00:21:09,572
Mr. Kwon's still unconscious.

333
00:21:10,102 --> 00:21:13,672
The longer he's unconscious the less likely it is for him to wake.

334
00:21:13,672 --> 00:21:16,332
When he took off with all our money...

335
00:21:16,332 --> 00:21:18,972
I expected he'd do pretty well.

336
00:21:19,402 --> 00:21:22,302
He never had a fixed address and his family's scattered...

337
00:21:22,302 --> 00:21:24,432
and the police can't reach them.

338
00:21:25,032 --> 00:21:28,032
What do you think he'd been up to...

339
00:21:28,332 --> 00:21:29,872
after he left?

340
00:21:30,302 --> 00:21:32,932
He should've lived an honest life.

341
00:21:33,502 --> 00:21:35,032
What a pity.

342
00:21:38,772 --> 00:21:40,832
Soo In's such a horrible child.

343
00:21:41,132 --> 00:21:45,232
Your dad got her a new music box when her brother broke the old one.

344
00:21:45,232 --> 00:21:47,432
How could she turn down your request?

345
00:21:48,002 --> 00:21:49,402
Your teacher too.

346
00:21:49,402 --> 00:21:52,002
You have until the day of the play to memorize your lines.

347
00:21:52,102 --> 00:21:55,172
How could she take your role just because you struggled?

348
00:21:55,172 --> 00:21:56,802
This is so upsetting.

349
00:21:56,932 --> 00:21:59,702
Your dad gives so much to that school.

350
00:21:59,902 --> 00:22:02,272
How dare they. Right, honey?

351
00:22:03,432 --> 00:22:04,472
Honey.

352
00:22:05,772 --> 00:22:08,172
- What? - What are you thinking about?

353
00:22:08,602 --> 00:22:11,202
Nothing. Were you talking to me?

354
00:22:11,402 --> 00:22:14,302
Se Young lost her role to Soo In just because...

355
00:22:14,302 --> 00:22:15,972
she couldn't remember her lines.

356
00:22:17,102 --> 00:22:18,202
Soo In?

357
00:22:19,702 --> 00:22:22,502
Did she memorize all the lines, then?

358
00:22:24,672 --> 00:22:27,232
Why can't you do something she can?

359
00:22:27,672 --> 00:22:31,202
She's been doing better than you in all the tests this year.

360
00:22:32,902 --> 00:22:34,372
Honey.

361
00:22:35,702 --> 00:22:37,232
Se Young, eat...

362
00:22:38,032 --> 00:22:40,632
Did you have to upset her even further?

363
00:22:59,272 --> 00:23:01,972
You want to play Snow White?

364
00:23:02,232 --> 00:23:04,532
Yes. It was my role to begin with.

365
00:23:04,832 --> 00:23:06,572
Our teacher told us to change roles.

366
00:23:06,872 --> 00:23:10,672
If you say you can't do it, she'll give us our old roles.

367
00:23:12,972 --> 00:23:15,232
Do you not want to switch back?

368
00:23:16,372 --> 00:23:17,602
Sorry.

369
00:23:29,802 --> 00:23:32,772
Prince. You saved me.

370
00:23:33,032 --> 00:23:34,332
Thank you.

371
00:23:34,802 --> 00:23:38,532
Princess, come with me and live in my kingdom.

372
00:23:39,302 --> 00:23:42,202
I can't leave my friends behind.

373
00:23:42,502 --> 00:23:46,002
Then they can all come and live with us.

374
00:23:47,632 --> 00:23:50,102
The end. You're amazing, Soo In.

375
00:23:50,102 --> 00:23:51,932
How did you memorize all this?

376
00:23:51,932 --> 00:23:53,902
I couldn't even remember one line.

377
00:23:56,232 --> 00:23:59,102
Soo In, our stepmother said...

378
00:23:59,102 --> 00:24:02,032
she'd make your Snow White costume.

379
00:24:02,202 --> 00:24:03,472
Yes. Why?

380
00:24:03,572 --> 00:24:06,132
Can I try it on later?

381
00:24:07,002 --> 00:24:09,772
Yes. You can have it when the play's done.

382
00:24:09,832 --> 00:24:10,902
Can I?

383
00:24:16,102 --> 00:24:22,132
(Snow White)

384
00:24:22,132 --> 00:24:24,772
Oh dear. I ate everything up.

385
00:24:25,172 --> 00:24:27,372
I'll ask for forgiveness when the owner comes back.

386
00:24:28,602 --> 00:24:31,132
There are seven of everything.

387
00:24:32,172 --> 00:24:34,932
7 chairs, 7 cups.

388
00:24:35,202 --> 00:24:36,432
Even seven plates.

389
00:24:37,372 --> 00:24:38,672
This is so fun.

390
00:24:39,232 --> 00:24:41,232
Let's try this chair.

391
00:24:42,732 --> 00:24:45,832
It's so small I can barely sit in it.

392
00:24:48,772 --> 00:24:50,072
I'm sleepy.

393
00:24:50,772 --> 00:24:53,902
I'll nap until the owner comes back.

394
00:24:56,502 --> 00:24:57,732
Well done.

395
00:24:58,272 --> 00:25:00,732
Seven dwarfs, come in.

396
00:25:00,772 --> 00:25:02,002
- Okay. - Okay.

397
00:25:04,832 --> 00:25:07,202
Soo In's really good.

398
00:25:07,702 --> 00:25:11,872
I think she did much better than Se Young.

399
00:25:12,872 --> 00:25:14,932
Do you all remember where to sit?

400
00:25:14,932 --> 00:25:16,402
- Yes. - Dwarf One.

401
00:25:16,402 --> 00:25:18,772
- Do you remember your line? - Yes.

402
00:25:18,802 --> 00:25:22,802
You have to remember your lines and where you sit.

403
00:25:22,802 --> 00:25:24,572
- Okay. - Soo In.

404
00:25:24,732 --> 00:25:26,532
You did really well.

405
00:25:29,572 --> 00:25:32,472
Shall we practice the scene?

406
00:25:32,932 --> 00:25:36,002
Soo In, how are you so good? I'm so jealous.

407
00:25:36,232 --> 00:25:37,332
I'm not that good.

408
00:25:37,572 --> 00:25:41,732
You'll look like a real princess once you wear your dress.

409
00:25:42,332 --> 00:25:45,802
You should have played Snow White from the start.

410
00:25:46,132 --> 00:25:49,172
That's right. You're a much better princess.

411
00:26:03,172 --> 00:26:05,102
- Soo In. - Hello.

412
00:26:05,932 --> 00:26:08,532
- Are you going somewhere? - To the hanbok store.

413
00:26:09,032 --> 00:26:11,632
I steamed some potatoes. Eat them when you want.

414
00:26:11,832 --> 00:26:13,732
Okay. See you later.

415
00:26:13,832 --> 00:26:14,902
Let's go, Min Jae.

416
00:26:16,402 --> 00:26:18,372
I want to play with Soo In.

417
00:26:18,772 --> 00:26:21,702
You can't. She has to study.

418
00:26:21,702 --> 00:26:24,132
I want to play with Soo In.

419
00:26:25,102 --> 00:26:26,672
I can watch him.

420
00:26:26,772 --> 00:26:29,032
He'll disturb your studies.

421
00:26:29,102 --> 00:26:30,872
It's okay. Go on.

422
00:26:31,932 --> 00:26:33,532
Do you like her that much?

423
00:26:33,532 --> 00:26:34,932
Yes.

424
00:26:36,532 --> 00:26:38,872
- See you. - I won't be long.

425
00:26:38,932 --> 00:26:40,102
Okay.

426
00:26:42,772 --> 00:26:43,772
Let's go.

427
00:26:58,632 --> 00:27:01,102
Soo In, let's go to the seaside.

428
00:27:01,872 --> 00:27:04,432
No, you'll catch a cold.

429
00:27:04,672 --> 00:27:06,872
I'll wear something warm.

430
00:27:07,232 --> 00:27:08,532
Can we go?

431
00:27:09,432 --> 00:27:10,632
I said no.

432
00:27:10,632 --> 00:27:13,802
I want to play by the sea.

433
00:27:14,132 --> 00:27:16,202
Can we go, Soo In?

434
00:27:17,502 --> 00:27:19,732
- Do you want to go that much? - Yes.

435
00:27:25,202 --> 00:27:27,932
Slow down, you might trip up.

436
00:27:28,102 --> 00:27:31,102
You need to walk faster.

437
00:27:32,332 --> 00:27:33,632
Okay.

438
00:27:35,902 --> 00:27:39,672
What? The police got in touch with Mr. Kwon's wife?

439
00:27:39,932 --> 00:27:40,932
Yes.

440
00:27:41,372 --> 00:27:45,102
Then they might find out that the watch isn't his.

441
00:27:45,672 --> 00:27:47,032
What should we do?

442
00:27:47,802 --> 00:27:50,672
What do you think? Get that watch back.

443
00:27:50,672 --> 00:27:51,802
Get it!

444
00:28:16,672 --> 00:28:18,902
- Soo In! - What?

445
00:28:22,572 --> 00:28:24,332
I want to fly a kite.

446
00:28:24,332 --> 00:28:26,932
A kite? We don't have one.

447
00:28:26,932 --> 00:28:28,702
Let's build sandcastles.

448
00:28:29,172 --> 00:28:32,232
There a kite that Dad made at home.

449
00:28:33,172 --> 00:28:36,332
I want to fly a kite.

450
00:28:36,332 --> 00:28:38,572
Then let's go and get it.

451
00:28:38,772 --> 00:28:42,272
No. My legs hurt.

452
00:28:42,672 --> 00:28:44,202
You go and get it.

453
00:28:46,872 --> 00:28:50,302
I'll be right back. Wait for me here.

454
00:28:50,302 --> 00:28:53,202
Don't go anywhere else. Stay here, okay?

455
00:28:53,272 --> 00:28:56,332
Okay, I'll stay here and wait.

456
00:28:56,332 --> 00:28:58,602
Get back quick. Be really quick.

457
00:29:09,372 --> 00:29:10,572
Min Jae.

458
00:29:12,502 --> 00:29:13,972
Se Young.

459
00:29:14,432 --> 00:29:15,902
Why are you here?

460
00:29:16,072 --> 00:29:18,432
I'm going to fly a kite with Soo In.

461
00:29:18,932 --> 00:29:21,432
- Where is your sister? - Soo In?

462
00:29:22,002 --> 00:29:24,502
She went home to get the kite.

463
00:29:25,302 --> 00:29:27,102
You can fly it with us.

464
00:29:27,332 --> 00:29:29,702
No. I don't want to.

465
00:29:33,632 --> 00:29:35,502
Why didn't you watch him?

466
00:29:36,602 --> 00:29:39,032
If you brought him out here, keep him safe.

467
00:29:39,202 --> 00:29:41,802
You shouldn't leave him alone in this busy place.

468
00:29:42,202 --> 00:29:43,632
I'm sorry.

469
00:29:46,402 --> 00:29:49,302
- Min Jae. - What, Se Young?

470
00:29:52,102 --> 00:29:55,102
Is Soo In really waiting for me there?

471
00:29:55,472 --> 00:29:57,502
- Yes. - Really?

472
00:29:57,772 --> 00:30:00,272
Yes, really. Now come with me.

473
00:30:22,102 --> 00:30:23,632
Where is my sister?

474
00:30:24,172 --> 00:30:25,672
She's over there.

475
00:30:26,332 --> 00:30:28,802
You're lying! She isn't there!

476
00:30:29,232 --> 00:30:30,402
I'm going back!

477
00:30:31,232 --> 00:30:33,202
Min Jae! Stop!

478
00:30:33,302 --> 00:30:35,672
Stop right there! Min Jae!

479
00:30:39,932 --> 00:30:41,102
Soo In.

480
00:30:43,372 --> 00:30:46,102
Where are you going? To fly a kite?

481
00:30:46,232 --> 00:30:48,972
Yes. Min Jae wanted to.

482
00:30:49,502 --> 00:30:51,302
That sounds like fun.

483
00:30:51,632 --> 00:30:54,802
I used to play kite-fighting when I was a child.

484
00:30:54,902 --> 00:30:57,472
I have to go. Min Jae's waiting alone.

485
00:30:57,672 --> 00:31:00,432
Okay, hurry on. Bye.

486
00:31:02,202 --> 00:31:04,732
Soo In! Soo In!

487
00:31:04,932 --> 00:31:08,032
Where are you? Soo In!

488
00:31:08,402 --> 00:31:11,072
Min Jae! Stop where you are!

489
00:31:11,602 --> 00:31:14,372
Soo In! Soo In!

490
00:31:14,932 --> 00:31:17,672
- Min Jae, come back here! - Soo In!

491
00:31:18,672 --> 00:31:20,372
Come here.

492
00:31:20,402 --> 00:31:22,332
Let go! Let me go!

493
00:31:22,332 --> 00:31:23,672
Let go of me!

494
00:31:23,672 --> 00:31:25,532
Come with me!

495
00:31:25,572 --> 00:31:27,002
Let go!

496
00:31:27,032 --> 00:31:28,632
Come with me!

497
00:31:35,732 --> 00:31:37,802
Min Jae! Min Jae!

498
00:31:44,232 --> 00:31:46,572
Min Jae! Min Jae!

499
00:31:47,372 --> 00:31:49,772
Min Jae! Min Jae!

500
00:31:49,772 --> 00:31:52,272
- Save me! - Min Jae!

501
00:31:52,272 --> 00:31:53,472
Save me!

502
00:31:54,702 --> 00:31:55,832
Se Young!

503
00:31:56,232 --> 00:31:58,502
Min Jae! Min Jae!

504
00:31:58,872 --> 00:32:00,032
Min Jae!

505
00:32:00,632 --> 00:32:03,932
Min Jae! Min Jae, where are you?

506
00:32:08,102 --> 00:32:09,572
Save me!

507
00:32:10,032 --> 00:32:11,072
Min Jae!

508
00:32:11,472 --> 00:32:13,032
Save me!

509
00:32:14,902 --> 00:32:16,702
Save me!

510
00:32:18,202 --> 00:32:19,332
Save me!

511
00:32:21,702 --> 00:32:22,732
Save me!

512
00:32:23,932 --> 00:32:26,132
Min Jae! Min Jae!

513
00:32:26,172 --> 00:32:27,372
Soo In!

514
00:32:28,502 --> 00:32:29,572
Soo In!

515
00:32:32,072 --> 00:32:33,532
Save me!

516
00:32:34,602 --> 00:32:36,302
- Save me! - Min Jae!

517
00:32:36,372 --> 00:32:37,572
Min Jae!

518
00:32:38,432 --> 00:32:40,932
- Soo In. - Min Jae! Min Jae!

519
00:32:41,932 --> 00:32:45,002
- Min Jae. - Save me, Soo In.

520
00:32:45,302 --> 00:32:48,072
- Min Jae! - Soo In, save me.

521
00:32:50,402 --> 00:32:51,472
Min Jae!

522
00:32:55,032 --> 00:32:58,832
Someone, help! My brother's in the water!

523
00:32:58,832 --> 00:33:00,772
Save my brother!

524
00:33:01,432 --> 00:33:03,972
Min Jae. Min Jae!

525
00:33:11,602 --> 00:33:14,172
Mom, Mom. Min Jae...

526
00:33:14,232 --> 00:33:16,032
fell into the sea.

527
00:33:16,702 --> 00:33:18,902
- Min Jae! - Min Jae!

528
00:33:18,902 --> 00:33:20,232
Yoon Min Jae!

529
00:33:21,472 --> 00:33:23,502
Min Jae!

530
00:33:25,802 --> 00:33:28,132
Young Sun! Young Sun!

531
00:33:29,702 --> 00:33:32,832
Come here. Bring him back! Bring him back!

532
00:33:34,102 --> 00:33:36,402
- Mr. Yoon Dong Chul. - What?

533
00:33:37,372 --> 00:33:39,002
- Hello. - Hello.

534
00:33:39,002 --> 00:33:51,000
Subtitles by NoTearSwoman

