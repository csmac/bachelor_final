﻿1
00:00:18,960 --> 00:00:24,433
The Beekeeper.

2
00:00:26,600 --> 00:00:28,796
<i>You're lucky, listen...</i>

3
00:00:31,640 --> 00:00:33,358
<i>Doesn't it sound like a song?</i>

4
00:00:33,960 --> 00:00:35,792
<i>What is it?</i>

5
00:00:35,880 --> 00:00:39,953
<i>It's the virgins who would be queens</i>

6
00:00:40,040 --> 00:00:43,271
<i>beating against their prison wall,</i>

7
00:00:43,360 --> 00:00:45,715
<i>trying to break the wax doors down.</i>

8
00:00:45,800 --> 00:00:50,920
<i>But the guards stand watch
and patch them up again.</i>

9
00:00:51,000 --> 00:00:53,150
<i>Why don't they let them out?</i>

10
00:00:54,200 --> 00:00:59,195
<i>Because only the queen bee
is allowed out.</i>

11
00:00:59,280 --> 00:01:02,955
<i>Others are kept In case
something happens to the queen.</i>

12
00:01:03,040 --> 00:01:05,236
<i>Soon the drones will go for water.</i>

13
00:01:06,480 --> 00:01:08,391
<i>They got there before us.
See them?</i>

14
00:01:08,480 --> 00:01:09,800
<i>What are they doing?</i>

15
00:01:09,880 --> 00:01:11,439
<i>Waiting for the queen.</i>

16
00:01:11,520 --> 00:01:13,352
<i>When she comes</i>

17
00:01:13,440 --> 00:01:17,070
<i>they'll dance high up in the air
and she'll choose one of them...</i>

18
00:01:18,000 --> 00:01:20,150
<i>Is that the queen's dance?</i>

19
00:01:20,520 --> 00:01:22,796
<i>That's the queen's dance.</i>

20
00:01:24,360 --> 00:01:28,831
A film by Theo Angelopoulos.

21
00:01:30,920 --> 00:01:34,276
The storm's ruined It for us,
hasn't it, teacher?

22
00:01:34,360 --> 00:01:37,751
Still, spring is in the air.

23
00:01:39,320 --> 00:01:40,879
Are you leaving tonight?

24
00:01:40,960 --> 00:01:43,918
Yes.
Spring comes earlier in the south.

25
00:01:47,080 --> 00:01:49,037
I'll see you later, Lina.

26
00:01:53,480 --> 00:01:56,757
And the father of the bride?
Anna, where's Spyro?

27
00:01:56,840 --> 00:02:00,117
<i>- Come on, Spyro, old man!
- He's OK.</i>

28
00:02:02,280 --> 00:02:04,954
Smile please!
It's a wedding, you know!

29
00:02:31,320 --> 00:02:32,879
And where did you say they'll go?

30
00:02:32,960 --> 00:02:36,032
I told you, Aunt, our son-in-law
is stationed in Crete.

31
00:02:36,120 --> 00:02:37,599
Do you need any help?

32
00:02:37,680 --> 00:02:39,637
What was your son studying?

33
00:02:40,240 --> 00:02:42,470
Put on another tape, Stefano.

34
00:02:42,560 --> 00:02:45,393
- What do you want me to put on?
- Whatever you want.

35
00:02:45,480 --> 00:02:47,391
A bird!

36
00:02:49,480 --> 00:02:51,232
A bird!

37
00:03:40,440 --> 00:03:42,556
Don't frighten it.

38
00:03:42,640 --> 00:03:44,472
<i>Open a window.</i>

39
00:03:57,480 --> 00:04:00,040
I'm off, Spyro.
I'm going to get ready.

40
00:04:00,120 --> 00:04:03,078
You look tired. Shall we put it off?

41
00:04:04,320 --> 00:04:06,880
The others will have loaded up already.

42
00:04:06,960 --> 00:04:09,190
No, it's all right.
I'll be there on time.

43
00:04:11,440 --> 00:04:13,158
It's gone.

44
00:04:17,800 --> 00:04:19,473
<i>'</i> Bye, Spyro.
- Bye.

45
00:05:51,520 --> 00:05:53,079
What happened, Mum?

46
00:06:21,840 --> 00:06:24,116
What's going on?
I don't understand.

47
00:06:24,200 --> 00:06:26,077
She dropped the tray of glasses.

48
00:06:26,160 --> 00:06:27,594
It's good luck...

49
00:06:33,600 --> 00:06:36,911
What's this?
Why aren't you all dancing?

50
00:07:16,560 --> 00:07:18,358
- Did you get wet?
- Just my feet.

51
00:07:18,440 --> 00:07:21,398
- Is this the place?
- Yes. Can't you hear the music?

52
00:07:29,640 --> 00:07:31,790
Hello. We've just finished.

53
00:07:31,880 --> 00:07:33,314
- Are you the new teacher?
- Yes.

54
00:07:33,400 --> 00:07:36,791
It was mayhem. They all fought over
who would congratulate their old teacher.

55
00:07:36,880 --> 00:07:38,996
- <i>Our best wishes.</i>
- <i>Our best wishes.</i>

56
00:07:41,600 --> 00:07:42,715
Come on up.

57
00:07:45,480 --> 00:07:47,710
- How are you, Niko?
- Fine, sir.

58
00:07:47,800 --> 00:07:49,438
Do you still set your traps?

59
00:07:55,880 --> 00:07:58,474
They're still upset
about your resignation.

60
00:10:12,040 --> 00:10:13,269
Best wishes!

61
00:10:20,560 --> 00:10:25,680
First I find you, then I lose you

62
00:10:27,000 --> 00:10:30,755
I'd rather be alone...

63
00:10:30,840 --> 00:10:33,798
You've had one too many again.

64
00:11:07,960 --> 00:11:09,359
Spyro?

65
00:11:42,280 --> 00:11:43,600
Where were you?

66
00:11:43,680 --> 00:11:47,196
They were all asking about you.
I don't understand you, Father.

67
00:11:55,600 --> 00:11:57,796
We're leaving.

68
00:12:10,480 --> 00:12:15,509
I climbed up the pepper tree
to pick a peck of peppers.

69
00:12:15,600 --> 00:12:18,319
But the pepper tree
came crashing down.

70
00:12:18,520 --> 00:12:20,909
And cut my hand open.

71
00:13:00,880 --> 00:13:02,234
Goodbye.

72
00:13:28,720 --> 00:13:30,358
I'll go and get a cab.

73
00:14:25,640 --> 00:14:26,960
Well, it's over.

74
00:14:53,040 --> 00:14:54,474
What will you do?

75
00:15:00,080 --> 00:15:01,991
I'll go down to Athens

76
00:15:03,400 --> 00:15:05,311
with the boy for a while.

77
00:15:07,280 --> 00:15:09,157
Now, with his exams,

78
00:15:10,400 --> 00:15:13,040
he hasn't the time
to take care of himself properly.

79
00:15:15,320 --> 00:15:16,355
After that...

80
00:15:20,560 --> 00:15:21,709
I don't know.

81
00:15:25,160 --> 00:15:27,720
I might sell the house.

82
00:15:33,720 --> 00:15:36,599
Maybe I'll go back to work.

83
00:15:41,760 --> 00:15:43,319
What did I do wrong?

84
00:15:47,240 --> 00:15:48,469
Nothing.

85
00:15:55,360 --> 00:15:56,589
Nothing...

86
00:17:26,520 --> 00:17:27,919
Goodbye, Anna.

87
00:17:34,440 --> 00:17:35,839
Take care.

88
00:19:03,560 --> 00:19:05,870
<i>From SA</i> 3 <i>diary.</i>

89
00:19:07,760 --> 00:19:09,319
<i>March 21 st.</i>

90
00:19:10,360 --> 00:19:12,112
<i>Spring heather...</i>

91
00:19:12,520 --> 00:19:17,276
<i>orange trees, clover, thyme...</i>

92
00:19:20,240 --> 00:19:23,039
<i>The trail of the ﬂowers
as Father used to say.</i>

93
00:19:29,520 --> 00:19:30,874
Let's go.

94
00:19:35,240 --> 00:19:37,516
- <i>I '1! be at the coffee shop, OK?</i>
- <i>Yeah.</i>

95
00:21:20,720 --> 00:21:21,994
Damn!

96
00:21:31,880 --> 00:21:34,156
Congratulations.
Did everything go well?

97
00:21:34,240 --> 00:21:36,595
- <i>One more. Make It a double.</i>
- <i>Right a way.</i>

98
00:21:36,680 --> 00:21:39,798
I'm selling everything this year.
Packing it in.

99
00:21:39,880 --> 00:21:44,716
- I'm tired of it.
- Last year there were ten of us, right?

100
00:21:44,800 --> 00:21:48,555
All these years, chasing the flowers.

101
00:21:49,880 --> 00:21:52,030
<i>Departure - just like every spring.</i>

102
00:21:52,760 --> 00:21:54,751
<i>It's been raining for two days now.</i>

103
00:21:55,720 --> 00:21:58,155
<i>Condition of beehives - gocd.</i>

104
00:21:58,840 --> 00:22:03,516
<i>Only numbers 3, 4, 9, 20
and 31 need changing.</i>

105
00:22:04,400 --> 00:22:06,516
<i>Thinning out of the colonies.</i>

106
00:22:07,320 --> 00:22:09,197
<i>Ageing queens.</i>

107
00:22:11,480 --> 00:22:13,869
<i>Departures used to be
festive occasions.</i>

108
00:22:15,960 --> 00:22:17,837
<i>We looked at each other.</i>

109
00:22:19,520 --> 00:22:21,989
<i>Last year there were ten of us.</i>

110
00:22:23,840 --> 00:22:26,229
<i>Now even George says he's tired.</i>

111
00:22:28,240 --> 00:22:30,800
<i>Departure - just like every spring.</i>

112
00:22:38,000 --> 00:22:40,992
- Safe journey.
- Safe journey. Bye.

113
00:23:07,840 --> 00:23:09,558
Drop me off anywhere.

114
00:23:20,800 --> 00:23:22,029
Where are you going?

115
00:23:22,120 --> 00:23:24,634
Anywhere,
as long as it's out of here.

116
00:23:28,320 --> 00:23:30,675
I'll drop you off
at the first opportunity.

117
00:23:30,760 --> 00:23:33,479
Don't worry. No one's looking for me.

118
00:23:37,320 --> 00:23:38,754
Got a cigarette?

119
00:26:45,600 --> 00:26:46,829
Still here?

120
00:26:54,480 --> 00:26:55,515
Hungry?

121
00:27:00,440 --> 00:27:01,396
<i>Come on.</i>

122
00:27:42,120 --> 00:27:45,112
Don't worry. I told you,
no one's looking for me.

123
00:27:48,800 --> 00:27:50,279
Do you want anything else?

124
00:27:54,640 --> 00:27:56,119
Are you going far?

125
00:27:57,040 --> 00:27:58,917
Will you take me with you?

126
00:27:59,000 --> 00:28:00,559
I'm going to fill up.

127
00:30:12,600 --> 00:30:15,991
Present arms!

128
00:30:43,360 --> 00:30:45,715
- Are we there?
- We're there.

129
00:30:46,720 --> 00:30:48,199
Are you sending me away?

130
00:30:49,000 --> 00:30:51,435
No, but that's what we agreed.

131
00:31:02,920 --> 00:31:04,877
Yes, we agreed

132
00:31:04,960 --> 00:31:07,600
and if you change your mind
you won't live until Saturday.

133
00:31:07,680 --> 00:31:08,954
What did you say?

134
00:31:12,480 --> 00:31:13,834
Will you come back?

135
00:34:50,880 --> 00:34:52,473
What do you want from me?

136
00:34:53,320 --> 00:34:54,594
Look...

137
00:34:58,040 --> 00:34:59,553
I have nowhere to go.

138
00:35:07,840 --> 00:35:09,114
Shut the door.

139
00:37:43,920 --> 00:37:45,319
Are you asleep?

140
00:38:14,600 --> 00:38:16,238
Are you still here?

141
00:39:18,920 --> 00:39:20,399
Can you open it for me?

142
00:39:34,120 --> 00:39:37,590
I found a map of yours
and an itinerary.

143
00:39:40,960 --> 00:39:42,837
Will you go to all those places?

144
00:39:44,640 --> 00:39:45,960
It depends.

145
00:39:46,360 --> 00:39:47,430
<i>On what?</i>

146
00:39:49,480 --> 00:39:50,914
<i>On the weather...</i>

147
00:39:51,680 --> 00:39:52,829
and other things.

148
00:39:56,880 --> 00:39:58,553
When will you take off?

149
00:40:01,200 --> 00:40:02,713
I don't know yet.

150
00:40:09,720 --> 00:40:10,915
I like it.

151
00:40:12,320 --> 00:40:13,355
What?

152
00:40:14,160 --> 00:40:16,629
Just taking off...

153
00:40:16,720 --> 00:40:18,393
taking off all the time.

154
00:40:22,240 --> 00:40:23,355
May I?

155
00:41:23,760 --> 00:41:25,398
You don't talk much, do you?

156
00:41:52,600 --> 00:41:53,874
As you like.

157
00:42:07,880 --> 00:42:09,154
All right, Dad.

158
00:43:00,240 --> 00:43:01,639
I'm going for cigarettes.

159
00:43:04,360 --> 00:43:06,874
Can I have 100 drachmas? I'm broke.

160
00:45:10,440 --> 00:45:11,919
Were you asleep?

161
00:45:12,680 --> 00:45:14,273
Talk about a coincidence...

162
00:45:14,360 --> 00:45:16,078
Michalis and I grew up together.

163
00:45:16,160 --> 00:45:18,310
I ran into him on the beach
by chance.

164
00:45:18,400 --> 00:45:20,198
I almost didn't recognise him.

165
00:45:21,960 --> 00:45:25,999
Can he stay here tonight?
He's just been discharged.

166
00:45:26,080 --> 00:45:28,196
He's leaving in the morning.

167
00:45:29,240 --> 00:45:30,355
<i>OK?</i>

168
00:45:31,480 --> 00:45:35,439
Needless to say, I didn't find
anywhere selling cigarettes around here.

169
00:45:37,480 --> 00:45:40,950
Go to sleep, we won't be long...
I'm dead tired.

170
00:45:44,600 --> 00:45:46,318
Pack up your things.

171
00:46:29,400 --> 00:46:30,959
Have you heard that one?

172
00:50:04,720 --> 00:50:06,916
I climbed up the pepper tree.

173
00:50:09,960 --> 00:50:12,315
To pick a peck of peppers

174
00:50:16,440 --> 00:50:18,795
I climbed up the pepper tree...

175
00:50:56,160 --> 00:50:57,719
<i>1,200.</i>

176
00:50:58,160 --> 00:51:01,278
There were three of you last night,
Mr Spyro.

177
00:51:01,360 --> 00:51:05,274
Normally I would have to charge you extra
for the third person.

178
00:51:06,760 --> 00:51:08,239
The beehives...

179
00:51:08,320 --> 00:51:09,515
it's getting windy...

180
00:52:51,880 --> 00:52:54,713
It's 350 x 305 x 300 on the outside

181
00:52:54,800 --> 00:52:58,430
and on the inside it's 260 x 185 x 110.

182
00:52:59,560 --> 00:53:00,914
18 kg.

183
00:53:02,200 --> 00:53:05,477
Tonight I drink with my friends!

184
00:53:05,560 --> 00:53:06,880
I have to go.

185
00:53:10,960 --> 00:53:12,473
The very best stuff!

186
00:53:12,560 --> 00:53:15,029
I've been saving it
for this very occasion.

187
00:53:15,520 --> 00:53:16,954
Shall I lock up?

188
00:53:40,120 --> 00:53:42,760
He keeps returning to his past.

189
00:53:42,840 --> 00:53:46,993
You know, his childhood in Normandy...

190
00:53:47,080 --> 00:53:51,950
his coming to Greece in '48
and the years in prison.

191
00:53:59,760 --> 00:54:01,239
I was expecting you.

192
00:54:03,520 --> 00:54:07,798
I was looking at the tree
outside the window.

193
00:54:07,880 --> 00:54:09,279
Spring is in the air.

194
00:54:10,960 --> 00:54:12,633
And I said...

195
00:54:12,720 --> 00:54:16,679
"They're bound to come at any moment
to have a drink with me."

196
00:54:18,480 --> 00:54:20,391
How are you?

197
00:54:20,480 --> 00:54:22,153
Fine.

198
00:54:23,520 --> 00:54:24,874
And the wine?

199
00:54:24,960 --> 00:54:28,112
Not now... We'll do it when
you get out of here...

200
00:54:29,280 --> 00:54:31,317
- The wine.
- Let it go.

201
00:54:31,400 --> 00:54:33,516
That's not what we came for.

202
00:54:33,880 --> 00:54:35,109
The wine!

203
00:54:42,400 --> 00:54:44,550
- Good?
- The best.

204
00:55:03,400 --> 00:55:04,993
Yes, it is the best.

205
00:55:12,960 --> 00:55:15,679
The best!

206
00:55:34,840 --> 00:55:36,433
What's going on here?

207
00:55:44,440 --> 00:55:45,919
That's not how we used to part.

208
00:55:46,000 --> 00:55:48,150
How did you get in at this hour?

209
00:55:50,760 --> 00:55:53,070
I'd like to see the sea once more.

210
00:55:53,680 --> 00:55:57,150
We're not leaving. We'll be waiting
for you outside. Don't worry.

211
00:56:29,400 --> 00:56:32,153
That first time

212
00:56:32,240 --> 00:56:34,675
was a day bathed in sunshine.

213
00:56:36,800 --> 00:56:40,316
We were dazed by the infinite light.

214
00:56:41,560 --> 00:56:45,394
And none of our desires
could remain hidden.

215
00:56:48,600 --> 00:56:52,833
In vain we searched
for some shade.

216
00:57:22,960 --> 00:57:24,951
- Shall we go back?
- No.

217
00:57:26,800 --> 00:57:29,474
I am what I am, can't you see?

218
00:57:29,560 --> 00:57:32,678
Let's go and look at the sea.

219
00:57:37,080 --> 00:57:40,914
I've been dreaming of seeing it
for two months now.

220
00:58:09,160 --> 00:58:10,275
The sea!

221
00:58:24,920 --> 00:58:26,399
And Anna, how is she?

222
00:58:26,480 --> 00:58:28,517
What's come over you now?

223
00:58:28,600 --> 00:58:30,671
Anna was adorable.

224
00:58:35,680 --> 00:58:40,277
We were all in love with her
but he snatched her away.

225
00:58:45,600 --> 00:58:49,195
I had a hard time getting over it, but...

226
00:58:55,840 --> 00:58:57,513
I remember her...

227
00:59:00,520 --> 00:59:02,750
at the first school dance.

228
00:59:04,840 --> 00:59:08,754
She shone like a star!

229
00:59:15,480 --> 00:59:17,278
I remember you too.

230
00:59:18,880 --> 00:59:22,350
When you first came to the academy,
lost in the crowd.

231
00:59:22,440 --> 00:59:25,671
You were the only one who graduated.

232
00:59:25,760 --> 00:59:28,479
The only one who became...

233
00:59:29,880 --> 00:59:31,279
a teacher.

234
00:59:31,960 --> 00:59:33,394
Beekeeper...

235
00:59:33,480 --> 00:59:37,997
A beekeeper's son,
a beekeepeﬂs grandson.

236
00:59:38,080 --> 00:59:40,196
How many generations?

237
00:59:47,960 --> 00:59:49,678
And he made money.

238
00:59:52,600 --> 00:59:56,309
I wasted myself courting history.

239
00:59:56,400 --> 00:59:58,311
- Do you regret it?
- No!

240
01:00:00,240 --> 01:00:02,390
Here in this town...

241
01:00:05,120 --> 01:00:08,636
we used to dream
of changing the world.

242
01:00:17,840 --> 01:00:19,638
You down there!

243
01:00:20,880 --> 01:00:22,632
You out there!

244
01:00:46,240 --> 01:00:47,389
I'm cold.

245
01:00:48,000 --> 01:00:50,230
Who'll join me first?

246
01:00:56,680 --> 01:00:57,750
I'm cold.

247
01:01:15,440 --> 01:01:16,919
<i>March 30th.</i>

248
01:01:18,000 --> 01:01:19,911
<i>State of beehives</i> -

249
01:01:20,880 --> 01:01:22,598
<i>1 to 30- good.</i>

250
01:01:23,680 --> 01:01:26,798
<i>31 to 40- thinning out of the colonies.</i>

251
01:01:27,800 --> 01:01:31,316
<i>41 to 45- beehives empty.</i>

252
01:01:32,680 --> 01:01:36,150
<i>Small purchases -
medicine, sugar, wire netting.</i>

253
01:01:37,640 --> 01:01:40,200
<i>I caught sight of Dimitris in Lamia.</i>

254
01:01:41,040 --> 01:01:43,031
<i>He disappeared in the crowd.</i>

255
01:01:47,200 --> 01:01:49,111
<i>3rd-6th April.</i>

256
01:01:50,720 --> 01:01:53,792
<i>Loan', Istiaia, Atalanti.</i>

257
01:01:54,920 --> 01:01:56,957
<i>Agitation in the beehives.</i>

258
01:01:57,520 --> 01:01:59,557
<i>Constant movement.</i>

259
01:02:01,240 --> 01:02:03,311
<i>It's toe late for the orange trees.</i>

260
01:02:05,720 --> 01:02:08,519
<i>The past few days
I keep losing colonies.</i>

261
01:02:10,400 --> 01:02:12,277
<i>April 17th.</i>

262
01:02:14,000 --> 01:02:17,391
<i>I found an old photograph
taken at Maria's christening.</i>

263
01:02:19,320 --> 01:02:21,550
<i>Anna used to smile and say,</i>

264
01:02:22,040 --> 01:02:26,671
<i>"Don't worry, she's a Capricorn,
she'll make her own way. ".</i>

265
01:02:28,760 --> 01:02:31,195
<i>When she left home, I realised it.</i>

266
01:02:33,440 --> 01:02:36,796
<i>Maria, how did it all come apart?</i>

267
01:06:19,040 --> 01:06:21,316
Where did you disappear to?

268
01:06:23,360 --> 01:06:25,078
Was it something I did?

269
01:06:28,120 --> 01:06:29,838
Haven't you shaved?

270
01:06:29,920 --> 01:06:30,990
Wait.

271
01:06:35,520 --> 01:06:37,272
I looked for you afterwards.

272
01:06:37,360 --> 01:06:39,590
You'd vanished,
but I knew I'd find you

273
01:06:39,680 --> 01:06:41,557
somewhere along the way.

274
01:06:45,840 --> 01:06:49,037
Loannina, Tsaritsani, Andritsena...

275
01:06:49,120 --> 01:06:50,997
You remember the map?

276
01:06:52,000 --> 01:06:54,640
I wandered from one place to the next.

277
01:06:54,720 --> 01:06:55,994
Here and there.

278
01:06:56,080 --> 01:06:58,230
Sometimes I'd like it,
others not.

279
01:07:02,160 --> 01:07:03,912
You know...

280
01:07:04,920 --> 01:07:07,992
You're the only one
who's been good to me.

281
01:07:08,080 --> 01:07:09,593
No one else ever has.

282
01:07:51,400 --> 01:07:52,720
I missed you.

283
01:08:00,240 --> 01:08:01,878
I've got to go. I'm late.

284
01:09:02,320 --> 01:09:03,879
I've come to fetch you.

285
01:09:06,360 --> 01:09:08,476
I'm in the middle of cooking.

286
01:09:10,360 --> 01:09:11,509
Sit down.

287
01:09:43,360 --> 01:09:45,351
I came to fetch you, Anna.

288
01:11:56,240 --> 01:11:59,278
The boy will be home any minute.

289
01:12:02,800 --> 01:12:04,473
Wait a minute.

290
01:12:07,000 --> 01:12:10,231
I want to look at you in the light.
Just a moment.

291
01:12:23,320 --> 01:12:24,276
Goodbye.

292
01:17:05,880 --> 01:17:07,678
Where are we going?

293
01:17:07,760 --> 01:17:09,751
To the other end of the map.

294
01:17:33,000 --> 01:17:34,399
Not like this!

295
01:17:43,720 --> 01:17:45,199
Not like this!

296
01:18:47,760 --> 01:18:49,080
Not like that.

297
01:19:19,240 --> 01:19:20,469
Hello, Father!

298
01:19:23,320 --> 01:19:25,880
- How come?
- I was just passing.

299
01:19:25,960 --> 01:19:28,395
- Will you come in?
- I wouldn't want to impose.

300
01:19:28,480 --> 01:19:30,391
Let him grumble.

301
01:19:31,200 --> 01:19:33,760
I meant to come to the wedding.

302
01:19:33,840 --> 01:19:36,434
I even had a dress made.
He didn't let me.

303
01:19:36,520 --> 01:19:38,636
He's from Mani,
and they don't forget.

304
01:19:39,880 --> 01:19:43,111
We're expanding -
a supermarket on this side.

305
01:19:43,200 --> 01:19:45,157
We need permits, and all that.

306
01:20:07,760 --> 01:20:09,751
- We've work to do!
- Leave me alone.

307
01:20:11,480 --> 01:20:12,800
Won't you stay?

308
01:20:16,640 --> 01:20:19,234
I wanted to tell you I'm sorry

309
01:20:20,080 --> 01:20:22,071
for what happened when you left home.

310
01:20:22,800 --> 01:20:24,359
I've forgotten about it.

311
01:20:26,240 --> 01:20:28,277
I should have told you years ago.

312
01:20:29,880 --> 01:20:31,393
Lowed it to you.

313
01:20:32,360 --> 01:20:34,556
Did you pay that bank bill?

314
01:21:05,120 --> 01:21:07,270
Will you be long? I'm frozen!

315
01:21:14,360 --> 01:21:16,158
This calls for music!

316
01:22:05,440 --> 01:22:06,794
What is it, Father?

317
01:22:08,240 --> 01:22:09,594
What's wrong?

318
01:24:36,600 --> 01:24:38,193
<i>April 27th.</i>

319
01:24:40,120 --> 01:24:43,670
<i>Someone will ask me,
a woman, perhaps...</i>

320
01:24:43,760 --> 01:24:46,400
<i>"Who are you? What do you want?"</i>

321
01:24:48,160 --> 01:24:49,275
<i>I'M answer...</i>

322
01:24:50,280 --> 01:24:53,557
<i>"Nothing...
I was just passing by.</i>

323
01:24:54,960 --> 01:24:58,635
<i>"I used to live here many years ago. ".</i>

324
01:25:27,080 --> 01:25:29,196
Is this the other end of the map?

325
01:26:29,920 --> 01:26:31,149
I'm hungry.

326
01:26:36,240 --> 01:26:41,076
You can get something at the canteen.
I'll be right back, take the suitcase.

327
01:26:41,160 --> 01:26:43,117
I'll be right back.

328
01:27:35,360 --> 01:27:39,877
<i>I climbed up the pepper tree
to pick a peck of peppers.</i>

329
01:27:39,960 --> 01:27:43,157
<i>But the pepper tree came crashing down
and cut my hand open.</i>

330
01:27:43,240 --> 01:27:47,518
<i>Give me your
gold-embroidered handkerchief.</i>

331
01:27:47,600 --> 01:27:51,195
<i>To bandage my bleeding hand</i>

332
01:29:44,560 --> 01:29:45,994
I'm drunk.

333
01:29:47,080 --> 01:29:49,117
You left me and I got drunk.

334
01:30:17,200 --> 01:30:21,637
Mr "I Remember", look at me!
I don't remember anything.

335
01:32:51,360 --> 01:32:53,317
<i>Cine PANTHEON.</i>

336
01:33:32,840 --> 01:33:34,114
Who is it?

337
01:33:40,120 --> 01:33:42,077
- Spyro!
- Yes.

338
01:33:43,600 --> 01:33:46,513
You're back, you old rascal!

339
01:33:46,600 --> 01:33:49,194
Look at the state of the Pantheon!

340
01:33:51,360 --> 01:33:53,874
My brothers want to sell it.

341
01:33:53,960 --> 01:33:57,191
They say it's closed down anyway.

342
01:33:58,520 --> 01:34:00,238
But I won't let them.

343
01:34:01,760 --> 01:34:04,513
I come here every day

344
01:34:04,600 --> 01:34:07,069
and service the old Debrie.

345
01:34:07,160 --> 01:34:08,275
I'll be right down.

346
01:34:27,560 --> 01:34:31,315
When was the last time?
Carnival time, wasn't it?

347
01:34:33,640 --> 01:34:34,960
Let's go home.

348
01:34:36,800 --> 01:34:38,677
May I stay here tonight?

349
01:34:41,480 --> 01:34:43,835
- In here?
- May I?

350
01:34:45,320 --> 01:34:48,790
Won't you come home with me?
My old lady will be heartbroken.

351
01:34:50,640 --> 01:34:52,916
What happened? What's wrong?

352
01:34:53,000 --> 01:34:54,149
May I?

353
01:34:55,320 --> 01:34:57,960
All right, no questions asked.

354
01:34:58,280 --> 01:34:59,953
Are we staying here?

355
01:35:09,160 --> 01:35:12,357
It's better there.
There's wood flooring.

356
01:35:13,640 --> 01:35:17,838
<i>Only a skinny man like me
could fit into the projection room.</i>

357
01:35:49,160 --> 01:35:51,310
Just as if we'd robbed a bank.

358
01:36:07,080 --> 01:36:09,879
We've hardly had time
to say anything.

359
01:36:09,960 --> 01:36:11,473
Will I see you?

360
01:36:11,560 --> 01:36:12,959
Look what I found.

361
01:36:15,320 --> 01:36:17,118
My father's old mask.

362
01:36:19,040 --> 01:36:22,635
So you visited the old place?
Did you see?

363
01:36:23,160 --> 01:36:25,720
It's completely deserted.

364
01:36:25,800 --> 01:36:27,871
Do you have a cigarette?

365
01:36:43,280 --> 01:36:44,793
So I won't be seeing you?

366
01:40:40,000 --> 01:40:42,071
<i>Why are you so sad?</i>

367
01:40:43,400 --> 01:40:45,835
<i>It's not because of me, I know.</i>

368
01:40:49,720 --> 01:40:51,393
<i>Sing me a song.</i>

369
01:40:54,120 --> 01:40:55,679
I don't know many.

370
01:40:56,080 --> 01:40:57,115
<i>Just one.</i>

371
01:41:47,360 --> 01:41:48,714
<i>What's that?</i>

372
01:41:49,600 --> 01:41:51,113
<i>I forgot the words.</i>

373
01:41:51,200 --> 01:41:52,554
<i>Sing me another.</i>

374
01:41:53,201 --> 01:41:54,839
<i>I don't know any others.</i>

375
01:42:14,760 --> 01:42:15,989
DFESS me.

376
01:42:17,400 --> 01:42:19,152
I'm very tired.

377
01:42:51,640 --> 01:42:53,233
Let me go...

378
01:43:12,280 --> 01:43:13,998
Let go of me.

379
01:43:27,800 --> 01:43:29,632
Let me go.

380
01:43:49,280 --> 01:43:50,998
Let me go.

381
01:44:30,001 --> 01:44:31,674
Let me go!

382
01:45:40,160 --> 01:45:41,514
What time is it?

383
01:45:47,240 --> 01:45:48,719
Already dressed?

384
01:46:59,680 --> 01:47:01,432
I'm ready.

385
01:48:38,440 --> 01:48:40,113
Let me go.

386
01:57:08,480 --> 01:57:11,484
Anééoun - Flpouupuovfi - nﬂléTﬂTCl
Movie World Ltd. Greece(Kriti). <i>©</i> 2012