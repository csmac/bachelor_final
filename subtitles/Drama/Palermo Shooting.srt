1
00:01:34,680 --> 00:01:36,477
What day is it?

2
00:01:36,800 --> 00:01:38,438
I don't have a clue.

3
00:01:39,560 --> 00:01:42,632
Time never used to go by this way...

4
00:01:46,920 --> 00:01:49,514
As a child,
you felt things differently.

5
00:01:50,560 --> 00:01:53,472
Everything lasted so marvelously long!

6
00:01:54,880 --> 00:01:56,199
But now...

7
00:01:58,600 --> 00:02:00,352
I don 't sleep much anymore.

8
00:02:00,960 --> 00:02:04,350
As soon as I fall into a dream,
I wake up with a start.

9
00:02:05,200 --> 00:02:08,272
As if every sleep was a "little death ".

10
00:03:24,720 --> 00:03:25,630
You can do it!

11
00:03:26,120 --> 00:03:27,838
Don't think about it, just do it!

12
00:03:28,920 --> 00:03:30,319
T ake a deep breath...

13
00:03:30,640 --> 00:03:32,710
And then jump!

14
00:03:34,160 --> 00:03:35,149
One...

15
00:03:37,720 --> 00:03:38,869
two...

16
00:03:40,240 --> 00:03:40,831
and three!

17
00:03:45,800 --> 00:03:47,119
It worked Iast time.

18
00:03:47,280 --> 00:03:49,919
You're lucky
ifyou learned to swim as a kid.

19
00:03:50,080 --> 00:03:52,116
It'll spare you some embarrassment.

20
00:03:56,520 --> 00:03:58,112
My Mom's fault!

21
00:03:58,640 --> 00:04:00,358
She was afraid of water.

22
00:04:00,520 --> 00:04:02,556
So I was, too...

23
00:04:04,440 --> 00:04:08,069
Never was a problem then. But now it is.

24
00:04:50,440 --> 00:04:52,158
How's it going, boys?

25
00:04:53,600 --> 00:04:55,830
This one is coming along.

26
00:04:56,480 --> 00:04:58,516
In the foreground, Ieft and right,

27
00:04:58,680 --> 00:05:00,352
we added a couple of buildings.

28
00:05:00,520 --> 00:05:02,556
It makes it more dense.

29
00:05:04,120 --> 00:05:07,635
Back here
we added some more floors.

30
00:05:08,160 --> 00:05:09,559
Here, too.

31
00:05:11,120 --> 00:05:13,475
And the horizon is more distant

32
00:05:13,640 --> 00:05:16,837
and more multi-Iayered.
We've selected a couple of skies, too.

33
00:05:19,880 --> 00:05:22,075
No, I have another idea for the sky.

34
00:05:24,040 --> 00:05:25,951
Somehow...

35
00:05:30,440 --> 00:05:32,749
time has to come into play here.

36
00:05:34,320 --> 00:05:38,199
We'II put the sunrise from
the first series on the Ieft.

37
00:05:38,360 --> 00:05:40,316
The Arizona clouds in the middle

38
00:05:40,480 --> 00:05:43,040
and on the right,
the sunset we put aside once.

39
00:05:43,200 --> 00:05:44,997
From Western Australia?

40
00:05:45,760 --> 00:05:46,954
That one. Exactly.

41
00:05:48,120 --> 00:05:50,270
Pay attention to all the details.

42
00:05:50,920 --> 00:05:53,639
The balcony glass
has to reflect the morning sun.

43
00:05:53,800 --> 00:05:55,791
And the pool should reflect the clouds.

44
00:05:55,960 --> 00:05:56,551
Easy.

45
00:05:57,560 --> 00:05:58,913
But here's the best part:

46
00:06:01,880 --> 00:06:04,952
From here on,
right down into the city,

47
00:06:05,120 --> 00:06:06,951
it should gradually turn to night.

48
00:06:58,440 --> 00:07:00,112
You can stay in the house, Frances,

49
00:07:00,280 --> 00:07:02,748
but not forever.
We never agreed on that.

50
00:07:05,120 --> 00:07:08,237
You were the one who suggested it!

51
00:07:08,800 --> 00:07:10,950
You can'tjust throw me out!

52
00:07:11,360 --> 00:07:13,954
I have as much right to it as you do!

53
00:07:14,120 --> 00:07:16,475
It's not my problem any more.

54
00:07:56,400 --> 00:07:58,914
That's what Iawyers are for.
Good night.

55
00:08:01,640 --> 00:08:03,790
Frances is driving me crazy.

56
00:08:07,160 --> 00:08:09,196
Don't get drawn into it!

57
00:08:10,440 --> 00:08:11,634
Don't be so nice.

58
00:08:12,040 --> 00:08:13,439
It won't work with her.

59
00:08:13,600 --> 00:08:16,831
Throw her out, cut off her money!
She is such a bitch!

60
00:08:17,000 --> 00:08:18,718
That won't make it better.

61
00:08:20,320 --> 00:08:22,550
It's me. So?

62
00:08:23,120 --> 00:08:24,155
Are you ready?

63
00:08:24,320 --> 00:08:27,392
I can't tonight, Sylvie.
We'II have to postpone.

64
00:08:28,080 --> 00:08:29,274
I have to work all night.

65
00:08:33,000 --> 00:08:34,149
I know.

66
00:08:34,560 --> 00:08:35,959
I'm really sorry.

67
00:08:36,440 --> 00:08:37,839
I was Iooking forward to seeing you.

68
00:08:38,400 --> 00:08:42,996
It's been like that for a while.
You don't give a shit any more.

69
00:08:44,280 --> 00:08:46,316
We'II spend
a great week-end together soon.

70
00:08:46,480 --> 00:08:48,675
- When would that be?
- It's a promise. Don't be angry.

71
00:08:50,400 --> 00:08:52,277
I think of you. Bye!

72
00:08:54,680 --> 00:08:56,033
Who was that?

73
00:08:56,640 --> 00:08:57,993
Duisburg.

74
00:09:03,760 --> 00:09:05,557
Come, Iet's mingle with the crowd!

75
00:09:07,600 --> 00:09:09,955
We need to talk
about tenants for your "Cube."

76
00:09:11,440 --> 00:09:12,953
You can't Ieave it empty so Iong.

77
00:09:13,120 --> 00:09:15,111
Three galleries have called.

78
00:09:16,440 --> 00:09:17,555
By the way...

79
00:09:17,720 --> 00:09:20,359
I changed the code to 4712.

80
00:09:21,600 --> 00:09:22,316
You idiot.

81
00:09:22,480 --> 00:09:24,436
In case you need the office again...

82
00:09:28,920 --> 00:09:31,912
It's not cool
that you Iive in the place yourself now.

83
00:09:32,080 --> 00:09:34,310
It won't be for Iong.
I can't sleep anyway.

84
00:09:34,480 --> 00:09:35,515
You should try it.

85
00:09:35,680 --> 00:09:37,750
It feels Iike Iost time.

86
00:10:10,640 --> 00:10:13,632
How often do I stand here
and look down at the river?

87
00:10:13,800 --> 00:10:17,236
Yesterday, day before yesterday,
today, tomorrow,

88
00:10:17,400 --> 00:10:20,597
all like and the same endless day!

89
00:10:20,840 --> 00:10:22,319
Money's growing on trees again.

90
00:10:22,480 --> 00:10:25,233
Photography's booming.
But you have to be careful.

91
00:10:25,400 --> 00:10:28,597
The fashion shoots will ruin
your standing with the museums.

92
00:10:28,760 --> 00:10:31,035
- Did Harry tell you that?
- No. I am telling you.

93
00:10:36,600 --> 00:10:38,875
Julian, you're up early.

94
00:10:39,720 --> 00:10:41,551
We'II say hi to Milla.

95
00:11:17,560 --> 00:11:19,357
UFOs farther Ieft!

96
00:11:24,320 --> 00:11:26,390
Come forward, Mars man!

97
00:11:26,560 --> 00:11:27,993
Hans! Go tell him!

98
00:11:28,160 --> 00:11:29,798
Don't just stand around!

99
00:11:33,240 --> 00:11:34,468
The whole boat! More dynamic!

100
00:11:34,640 --> 00:11:35,789
Stay out of this!

101
00:12:01,240 --> 00:12:02,275
Water, water.

102
00:12:14,680 --> 00:12:16,033
Reflector up!

103
00:12:26,840 --> 00:12:28,239
When should we do it?

104
00:12:28,840 --> 00:12:29,795
No, today is bad.

105
00:12:32,240 --> 00:12:34,435
Hand off the support, please!

106
00:12:35,360 --> 00:12:36,509
It'II have to do!

107
00:12:40,440 --> 00:12:42,670
Don't forget
that you're still in the frame!

108
00:12:43,480 --> 00:12:45,630
Steelworker, come closer.

109
00:12:46,760 --> 00:12:48,193
Keep it quiet!

110
00:12:48,360 --> 00:12:51,432
You're not the only ones
who want to go home, yeah?

111
00:14:16,520 --> 00:14:19,159
So you think
I should work on it even more?

112
00:14:19,320 --> 00:14:22,232
Exactly.
Then it might become a real image.

113
00:14:25,960 --> 00:14:29,111
That's enough about ourphotos,
and your opinions of them.

114
00:14:31,760 --> 00:14:32,954
Now it's my turn.

115
00:14:33,120 --> 00:14:35,509
I want to tell you
what bothers me about your work.

116
00:14:36,200 --> 00:14:37,633
Fire away.

117
00:14:38,000 --> 00:14:40,230
You found my picture so "empty."

118
00:14:40,400 --> 00:14:42,994
I'd Iike to know what you feel
is "under the surface" here.

119
00:14:43,160 --> 00:14:45,913
Nothing.
Things are nothing but surface.

120
00:14:46,960 --> 00:14:48,837
It's not hard to understand.

121
00:14:54,040 --> 00:14:58,158
If there's nothing to unveil,
we don't need to photograph!

122
00:14:58,320 --> 00:14:59,639
We don't need to do a thing.

123
00:15:00,400 --> 00:15:03,233
We may as well just get drunk
and wait for our retirement.

124
00:15:03,400 --> 00:15:04,719
Or better yet, just die.

125
00:15:08,840 --> 00:15:10,671
This is still too sharp.

126
00:15:11,320 --> 00:15:13,197
Sao Paolo is hazier.

127
00:15:13,360 --> 00:15:15,476
We haven't adjusted the contrast yet.

128
00:15:15,640 --> 00:15:16,959
Get a move on!

129
00:15:26,920 --> 00:15:28,273
Can I take a picture of us?

130
00:15:36,760 --> 00:15:38,432
Can I have an autograph?

131
00:15:40,040 --> 00:15:41,439
What's your name?

132
00:18:07,120 --> 00:18:09,076
I could be dead now.

133
00:18:09,480 --> 00:18:11,232
But I'm not.

134
00:18:12,320 --> 00:18:15,198
I could feel twice as alive.

135
00:18:15,360 --> 00:18:16,759
But I don't.

136
00:20:23,120 --> 00:20:27,432
I have such a yarning
for things to change!

137
00:20:29,520 --> 00:20:31,511
Everything, if you ask me...

138
00:22:21,000 --> 00:22:23,275
How do you know you're dead?

139
00:22:24,440 --> 00:22:26,351
When you don 't dream any more?

140
00:22:26,800 --> 00:22:28,836
Or when all you do is dream?

141
00:22:35,960 --> 00:22:37,313
Lovely, isn't it?

142
00:22:40,600 --> 00:22:41,715
The Rhine.

143
00:22:42,000 --> 00:22:43,115
The meadows.

144
00:22:43,680 --> 00:22:44,829
The morning.

145
00:22:49,200 --> 00:22:50,758
Are those your sheep?

146
00:22:51,040 --> 00:22:54,510
These? Oh no.
I am only a temporary keeper.

147
00:22:54,680 --> 00:22:55,556
And the old man?

148
00:22:55,720 --> 00:22:56,914
Did he stop?

149
00:22:57,480 --> 00:22:59,436
He's sleeping it off...

150
00:23:00,040 --> 00:23:01,996
And I pay him well for it.

151
00:23:02,280 --> 00:23:03,759
You pay to do his work?

152
00:23:03,920 --> 00:23:04,591
Of course.

153
00:23:05,000 --> 00:23:06,956
I'm an early riser anyway.

154
00:23:07,680 --> 00:23:09,113
The Tokyo stock exchange!

155
00:23:10,080 --> 00:23:11,513
The dog does all the work.

156
00:23:12,600 --> 00:23:14,636
I just walk along.

157
00:23:16,920 --> 00:23:18,273
Why do you do it?

158
00:23:19,080 --> 00:23:20,399
Shepherding...

159
00:23:27,160 --> 00:23:28,718
For the peace of mind, or what?

160
00:23:28,880 --> 00:23:32,077
I enjoy howtime passes.

161
00:23:33,320 --> 00:23:34,799
And how does it pass?

162
00:23:36,960 --> 00:23:38,075
SIowly!

163
00:23:38,400 --> 00:23:39,913
Very slowly...

164
00:23:41,960 --> 00:23:46,272
It all depends on how much time
you give yourself to sense time.

165
00:23:46,680 --> 00:23:49,592
I tell you,
we can slow time down.

166
00:23:51,400 --> 00:23:53,595
Time doesn't care about us.

167
00:23:55,360 --> 00:23:57,271
Look at my tree.

168
00:23:57,920 --> 00:23:59,956
I've known it since I was five.

169
00:24:00,960 --> 00:24:02,837
How can you slow it down?

170
00:24:08,080 --> 00:24:10,469
Don't you ever imagine things

171
00:24:11,240 --> 00:24:14,471
that you believe
will happen again?

172
00:24:29,200 --> 00:24:30,474
I don't want to be nosey,

173
00:24:30,640 --> 00:24:34,997
but have you ever
Iost someone close to you?

174
00:24:39,480 --> 00:24:40,549
My mother.

175
00:24:42,080 --> 00:24:43,433
I'm sorry.

176
00:24:48,600 --> 00:24:49,999
Your mother...

177
00:24:55,200 --> 00:24:56,713
"Palermo."

178
00:24:57,840 --> 00:24:59,592
It comes from Greek.

179
00:25:00,920 --> 00:25:02,797
It was originally called "Panormos."

180
00:25:04,400 --> 00:25:05,355
"Great harbor."

181
00:25:11,760 --> 00:25:16,231
Did you ever imagine
how it was for your mother

182
00:25:16,400 --> 00:25:18,231
to see her house for the Iast time?

183
00:25:18,960 --> 00:25:21,520
Her Iast time at the hairdresser?

184
00:25:22,440 --> 00:25:25,352
There is always a Iast time, eventually.

185
00:25:25,520 --> 00:25:27,112
But we don't usually know.

186
00:25:29,000 --> 00:25:31,719
So you need to do everything
as though it were the Iast time.

187
00:25:35,120 --> 00:25:37,111
My Iast morning with the sheep.

188
00:25:37,280 --> 00:25:39,840
The Iast stranger I Iook in the eye.

189
00:25:41,120 --> 00:25:44,715
Your tear just now,
the Iast I'II ever see.

190
00:25:44,880 --> 00:25:46,996
You have to take everything
dead seriously.

191
00:25:47,400 --> 00:25:48,958
Just not yourself!

192
00:26:35,360 --> 00:26:36,713
Take care!

193
00:27:00,280 --> 00:27:02,430
Of course Milla will come.
She wanted it!

194
00:27:02,600 --> 00:27:04,158
You have to tell her right away.

195
00:27:04,320 --> 00:27:07,869
How can we organize it so fast?
Do you want to take the whole team?

196
00:27:08,040 --> 00:27:11,316
No, no big team.
An assistant, make-up, stylist.

197
00:27:11,480 --> 00:27:13,994
- How can I get you all on a flight?
- Harry's plane.

198
00:27:14,160 --> 00:27:17,391
- Ask him to Ioan it to us for a day.
- I'm not sure we can.

199
00:27:18,200 --> 00:27:21,158
Harry shares the plane.
Does it have to be now?

200
00:27:21,320 --> 00:27:23,151
Yes. This afternoon... to Palermo.

201
00:27:23,320 --> 00:27:24,116
Palermo?

202
00:27:24,280 --> 00:27:26,714
Giovanni has all the connections.
Let him organize it.

203
00:27:26,880 --> 00:27:28,598
Any idea where you want
to do the shoot?

204
00:27:28,760 --> 00:27:32,036
No, I was never there.
I'm just checking some pictures.

205
00:30:38,760 --> 00:30:40,318
What's missing?

206
00:30:42,240 --> 00:30:43,958
What's missing in my life is...

207
00:30:45,800 --> 00:30:47,677
What is missing, anyway?

208
00:32:47,840 --> 00:32:50,673
Pack everything up
and fly back as soon as you can.

209
00:35:09,880 --> 00:35:13,316
"23 calls in your absence"

210
00:35:16,800 --> 00:35:19,633
When was I really present lately?

211
00:35:20,160 --> 00:35:21,832
Actually "there"?

212
00:38:09,880 --> 00:38:11,757
Take it from me:.

213
00:38:12,440 --> 00:38:16,115
it's a shitty feeling when
everything falls apart all of a sudden.

214
00:42:54,280 --> 00:42:55,554
Don't move.

215
00:42:57,200 --> 00:42:58,235
Do you mind if...

216
00:43:00,000 --> 00:43:01,558
...if I finish my drawing?

217
00:43:21,920 --> 00:43:22,830
German?

218
00:43:29,600 --> 00:43:32,751
"Little Hans went alone..."

219
00:46:41,160 --> 00:46:43,435
Julian! How's it going?

220
00:46:43,640 --> 00:46:45,551
Good. You on vacation?

221
00:46:46,320 --> 00:46:48,390
The new photos ofMilla are great!

222
00:46:48,560 --> 00:46:51,233
They bought them, but did you
need to go all the way to Palermo?

223
00:46:51,400 --> 00:46:52,719
I think we did.

224
00:46:53,360 --> 00:46:54,839
When are you coming back?

225
00:46:55,400 --> 00:46:56,549
Don't know yet.

226
00:46:57,440 --> 00:46:59,829
It's the right place right now.

227
00:47:03,080 --> 00:47:04,877
Do you know what Palermo means?

228
00:47:05,040 --> 00:47:06,029
No idea.

229
00:47:06,680 --> 00:47:08,910
- The Mafia Capital?
- Very funny.

230
00:47:09,760 --> 00:47:12,832
It means "big harbor",
mother of all harbors.

231
00:47:14,360 --> 00:47:15,634
Call if you need anything.

232
00:47:17,000 --> 00:47:18,149
See you.

