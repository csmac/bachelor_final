﻿1
00:00:00,860 --> 00:00:09,250
<i>♫ I won't be sad, shaken or weak anymore ♫</i>

2
00:00:09,250 --> 00:00:17,180
<i>♫ It's okay even if I fall, because I can get up again ♫</i>

3
00:00:17,180 --> 00:00:25,160
<i>♫ Just like the stars in the sky and the birds in the clouds, I will rise ♫</i>

4
00:00:25,160 --> 00:00:32,450
<i>♫ Hold my hand and walk with me, happy days are coming ♫</i>

5
00:00:32,450 --> 00:00:35,100
<i> ♫ I'm okay ♫</i>

6
00:00:35,610 --> 00:00:37,440
- Episode 14 -

7
00:00:40,500 --> 00:00:44,640
Are you in insane? How could you think of going there by yourself?

8
00:00:44,640 --> 00:00:46,820
Are you possibly really Seo Ah Shin's saseng fan?

9
00:00:46,820 --> 00:00:48,350
How many times do I have to tell you?

10
00:00:48,350 --> 00:00:51,340
I went because I was invited to Seo Ah Shin's birthday party.

11
00:00:51,340 --> 00:00:52,890
Who would believe that?

12
00:00:52,890 --> 00:00:56,950
Unless he's crazy, why would he invite Jumping Girl?

13
00:00:56,990 --> 00:01:01,000
I can't even contact his manager. What am I supposed to do now?

14
00:01:02,420 --> 00:01:04,170
Forget it.

15
00:01:04,170 --> 00:01:07,290
I think I've been wasting my time.

16
00:01:11,140 --> 00:01:13,330
Un..unnie!

17
00:01:44,050 --> 00:01:47,150
Why are you doing this to my friend?

18
00:01:49,480 --> 00:01:51,150
What are you talking about?

19
00:01:51,150 --> 00:01:55,200
These paparazzi pictures. They look like they are in Seo Ah Shin's room.

20
00:01:55,200 --> 00:01:58,070
What are you planning exactly?

21
00:02:01,550 --> 00:02:04,470
I wanted to be with you more comfortably.

22
00:02:04,470 --> 00:02:08,340
Thanks to Nam Sang Ah, it's possible.

23
00:02:10,760 --> 00:02:13,790
We can't leave our scandal to spread.

24
00:02:13,790 --> 00:02:16,680
It's to distract people's attentions.

25
00:02:21,080 --> 00:02:24,600
I didn't want you to be harmed.

26
00:02:24,600 --> 00:02:28,680
I am sorry about your friend's accident.

27
00:02:28,680 --> 00:02:31,790
How? How are you going to repay her?

28
00:02:31,790 --> 00:02:34,880
It wasn't just once, but twice!

29
00:02:48,820 --> 00:02:52,730
What happened during Seo Ah Shin's birthday?

30
00:02:54,270 --> 00:02:59,870
Sang-ah said that she was definitely invited by his manager. I heard you were supposed to go.

31
00:03:00,710 --> 00:03:04,470
There was no birthday party, as far as I know.

32
00:03:04,470 --> 00:03:06,100
Then...

33
00:03:07,400 --> 00:03:12,690
Then... why did that punk did that to Seong Ah? Do you know anything?

34
00:03:17,760 --> 00:03:22,040
Hey, Yenny Lee! You know something, right?

35
00:03:24,650 --> 00:03:26,900
Where is he right now?

36
00:03:35,270 --> 00:03:37,570
I'll get the car and be right back.

37
00:03:50,570 --> 00:03:54,250
Who are you? Who are you that you do that to Sang-ah?

38
00:03:58,160 --> 00:04:00,120
And who are you?

39
00:04:04,170 --> 00:04:06,420
Don't mess with Sang Ah.

40
00:04:06,420 --> 00:04:08,600
'Cause she's my girl!

41
00:04:09,680 --> 00:04:11,670
I was doing it to protect my girl too.

42
00:04:11,670 --> 00:04:13,740
You...

43
00:04:27,690 --> 00:04:29,820
Hold on, hold on, why are you like this?

44
00:04:29,820 --> 00:04:31,620
Leave me be.

45
00:04:31,620 --> 00:04:33,620
Let go.

46
00:04:33,620 --> 00:04:35,310
No.

47
00:04:46,490 --> 00:04:51,230
Please empty your room by this month. I can't handle it because they do this non-stop.

48
00:04:51,230 --> 00:04:53,110
What is this, seriously?

49
00:04:55,310 --> 00:04:58,940
I am sorry. I will leave soon.

50
00:04:58,940 --> 00:05:01,790
What is this, seriously?

51
00:05:03,210 --> 00:05:05,020
I am sor...

52
00:05:15,510 --> 00:05:19,920
<i>Unnie. Should I go to Australia?</i>

53
00:05:22,060 --> 00:05:25,860
<i>It's just...I miss you.</i>

54
00:05:31,340 --> 00:05:33,240
Ga Eul?

55
00:05:34,900 --> 00:05:36,460
Look here.

56
00:05:39,580 --> 00:05:42,950
How old are you that you're fighting?

57
00:05:42,950 --> 00:05:45,330
Who did you fight with?

58
00:05:45,330 --> 00:05:48,030
I told you, I just fell.

59
00:05:52,150 --> 00:05:55,890
I... I'm going to Australia next month.

60
00:05:57,010 --> 00:06:02,300
I am going to help unnie and search up a school.

61
00:06:04,570 --> 00:06:07,150
I am fine.

62
00:06:07,150 --> 00:06:12,470
I talked to Yenny, and she does seemed to be worried about me a lot too.

63
00:06:14,820 --> 00:06:17,160
I want to leave temporarily.

64
00:06:20,720 --> 00:06:25,620
You said I can use this whenever I want to right?

65
00:06:25,620 --> 00:06:27,860
And there's no time limit, right?

66
00:06:28,720 --> 00:06:32,120
Here. Nam Sang Ah's free ticket.

67
00:06:33,050 --> 00:06:34,870
I want to use it now.

68
00:06:39,050 --> 00:06:41,850
Nam Sang Ah's free ticket<br>Time period: Forever
subtitles ripped by riri13

69
00:06:44,070 --> 00:06:45,550
Sang Ah...

70
00:06:49,500 --> 00:06:51,540
I like you.

71
00:06:52,610 --> 00:06:55,990
No, I love you.

72
00:06:57,220 --> 00:06:59,850
From the moment I first saw you,

73
00:07:01,100 --> 00:07:06,010
the time you drank the alcohol from the cold noodle bowl for me.

74
00:07:15,010 --> 00:07:18,480
Sang Ah, don't run away.

75
00:07:18,480 --> 00:07:22,430
I... will stay by your side.

76
00:07:55,410 --> 00:08:00,420
I proposed to Sang Ah. So she can't go to Australia.

77
00:08:02,020 --> 00:08:04,700
I didn't hear a reply yet but...

78
00:08:05,880 --> 00:08:09,300
Don't worry. It will turn out fine.

79
00:08:30,830 --> 00:08:33,130
Please leave for a moment.

80
00:08:33,880 --> 00:08:37,090
I need to say something to Seo Ah Shin.

81
00:08:57,590 --> 00:09:01,610
I...like you, Seo Ah Shin.

82
00:09:06,790 --> 00:09:10,900
Seo Ah Shin, do you like me too?

83
00:09:23,140 --> 00:09:24,860
What if I do?

84
00:09:25,700 --> 00:09:29,250
If you really like me, reveal it to the world.

85
00:09:29,250 --> 00:09:32,690
And don't make the wrong person be the victim.

86
00:09:32,690 --> 00:09:39,460
I like the tenderhearted Seo Ah Shin more than the star Seo Ah Shin.

87
00:09:39,460 --> 00:09:42,460
You know why I did that.

88
00:09:43,360 --> 00:09:45,750
I don't want you to get hurt.

89
00:09:45,750 --> 00:09:49,090
The moment I reveal that you are my girlfriend, the scar that you'll have...

90
00:09:49,090 --> 00:09:53,150
Hiding me from the world is the biggest scar for me.

91
00:09:53,150 --> 00:09:54,890
"I like Yenny Lee."

92
00:09:54,890 --> 00:09:59,370
"This girl is my girl." <br>Do you think I don't want to do that?

93
00:09:59,370 --> 00:10:02,930
That day, the fans...to you...

94
00:10:03,990 --> 00:10:06,050
What are you worrying about?

95
00:10:06,050 --> 00:10:08,930
I am a black belt.

96
00:10:08,930 --> 00:10:12,840
I am not scared of those fans at all.

97
00:10:26,520 --> 00:10:30,300
This is Seo Ah Shin's concert ticket.<br>Come with Sang Ah.

98
00:10:31,850 --> 00:10:35,700
Would she want to go? I don't feel like it either.

99
00:10:37,220 --> 00:10:41,220
I quit as his personal body guard.

100
00:10:42,490 --> 00:10:46,030
I want to give a last chance for him to apologize to Sang Ah.

101
00:10:46,790 --> 00:10:49,210
I don't know if it will work though.

102
00:10:56,720 --> 00:11:06,850
Subtitles by The Jumping Girl Team @Viki

103
00:11:15,110 --> 00:11:16,860
<i>What is happening?</i>

104
00:11:16,860 --> 00:11:18,000
<i>What is it?</i>

105
00:11:18,000 --> 00:11:21,540
<i>Don't be ridiculous, Seo Ah Shin!</i>

106
00:11:23,330 --> 00:11:25,030
<i>Who is that?</i>

