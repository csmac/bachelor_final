1
00:00:54,767 --> 00:00:57,850
~ All things bright and beautiful

2
00:00:57,933 --> 00:01:01,350
~ All creatures great and small

3
00:01:02,225 --> 00:01:05,475
~ All things wise and wonderful

4
00:01:05,558 --> 00:01:09,017
~ The Lord God made them all

5
00:01:09,475 --> 00:01:13,642
~ Each little flower that opens

6
00:01:13,725 --> 00:01:17,100
~ Each little bird that sings

7
00:01:17,183 --> 00:01:21,183
~ He made their glowing colours

8
00:01:21,267 --> 00:01:24,808
~ He made their tiny wings

9
00:01:25,975 --> 00:01:29,142
~ All things bright and beautiful

10
00:01:30,017 --> 00:01:33,017
~ All creatures great and small

11
00:01:34,100 --> 00:01:38,100
~ All things wise and wonderful

12
00:01:38,183 --> 00:01:41,392
~ The Lord God made them all

13
00:01:41,933 --> 00:01:46,433
~ The purple-headed mountain

14
00:01:46,516 --> 00:01:49,725
~ The river running by

15
00:01:50,475 --> 00:01:54,683
~ The sunset and the morning

16
00:01:54,766 --> 00:01:58,225
~ That brightens up the sky

17
00:01:59,516 --> 00:02:03,058
~ All things bright and beautiful

18
00:02:03,141 --> 00:02:06,766
~ All creatures great and small

19
00:02:07,933 --> 00:02:11,850
~ All things wise and wonderful

20
00:02:11,933 --> 00:02:15,433
~ The Lord God made them all

21
00:02:16,225 --> 00:02:20,350
~ The cold wind in the winter

22
00:02:20,433 --> 00:02:23,725
~ The pleasant summer sun

23
00:02:24,433 --> 00:02:28,641
~ The ripe fruits in the garden

24
00:02:28,725 --> 00:02:32,183
~ He made them every one

25
00:02:33,391 --> 00:02:37,016
~ All things bright and beautiful

26
00:02:37,100 --> 00:02:40,516
~ All creatures great and small...

27
00:05:47,808 --> 00:05:49,849
We're going now!

28
00:05:49,933 --> 00:05:51,974
Eine Minute, bitte.

29
00:05:52,058 --> 00:05:54,099
There's no time.

30
00:05:54,183 --> 00:05:56,224
Come on, you lot, get moving!

31
00:09:11,641 --> 00:09:13,057
I'm hungry.

32
00:09:13,141 --> 00:09:15,182
You're selfish!

33
00:09:20,307 --> 00:09:22,349
Will you stop it, boys? Stop it!

34
00:09:22,432 --> 00:09:24,474
Tommy, you're the eldest. Get up.

35
00:09:24,557 --> 00:09:26,682
Stop it! Oh, stop it, boys!

36
00:09:26,766 --> 00:09:28,807
Stop it! Stop!

37
00:09:28,891 --> 00:09:30,932
Stop it, son.

38
00:09:31,016 --> 00:09:33,057
Don't do that to the wee chap.

39
00:09:33,141 --> 00:09:36,807
Get up, Tommy! Get up, Tommy!

40
00:09:36,891 --> 00:09:39,391
Oh, son. Get up.

41
00:09:39,474 --> 00:09:41,307
Jamie, off the floor.

42
00:09:41,391 --> 00:09:43,099
Stop it, Tommy!

43
00:10:33,682 --> 00:10:35,724
Granny, where's my da and ma?

44
00:10:41,766 --> 00:10:42,974
Ma's dead.

45
00:10:46,682 --> 00:10:48,724
What does 'dead' mean?

46
00:10:48,807 --> 00:10:50,849
You go up to heaven.

47
00:10:52,766 --> 00:10:55,307
What's heaven?

48
00:10:55,391 --> 00:10:58,391
My teacher says
it's a beautiful house up in the sky.

49
00:11:00,391 --> 00:11:02,432
Go to sleep.

50
00:11:15,807 --> 00:11:19,099
Oh, my poor girls.

51
00:11:26,807 --> 00:11:29,557
What have they done to you?

52
00:11:38,432 --> 00:11:40,474
God curse them!

53
00:11:58,849 --> 00:12:00,599
Bitte, Junge.

54
00:12:31,724 --> 00:12:33,765
Apfel.

55
00:12:33,849 --> 00:12:36,432
- Apple.
- Apple.

56
00:12:41,015 --> 00:12:46,015
B is for Boy.

57
00:12:48,224 --> 00:12:53,099
- B is for B... Bo...
- Boy.

58
00:12:53,182 --> 00:12:55,224
- Boy.
- Boy.

59
00:13:00,140 --> 00:13:05,182
C is for Cat.

60
00:13:15,432 --> 00:13:17,140
Dog.

61
00:13:17,224 --> 00:13:18,515
Dog?

62
00:13:18,599 --> 00:13:20,015
Dog.

63
00:13:20,099 --> 00:13:22,140
- Dog.
- Good.

64
00:13:39,057 --> 00:13:41,099
Das ist dein Buch, Jamie.

65
00:13:41,182 --> 00:13:43,224
No, it's for you, Helmuth.

66
00:13:43,307 --> 00:13:45,349
Gehst du jetzt nach deine Mutter?

67
00:13:49,182 --> 00:13:51,224
Auf Wiedersehen, Jamie.

68
00:14:55,724 --> 00:14:57,765
There's a good girl. Come on.

69
00:16:12,848 --> 00:16:15,015
Hello, old one. How are you getting on?

70
00:16:17,890 --> 00:16:22,557
It's a long time since I've seen you, mind,
but I'll tell you, you're looking well, mind.

71
00:16:22,640 --> 00:16:24,432
How are you feeling?

72
00:16:24,515 --> 00:16:25,515
Hm?

73
00:16:27,890 --> 00:16:30,557
Come on,
you must have something to say to me.

74
00:16:30,640 --> 00:16:32,682
Oh, well.

75
00:16:32,765 --> 00:16:35,057
See what I've got for your birthday?

76
00:16:47,473 --> 00:16:49,515
How are you doing, son?

77
00:16:52,557 --> 00:16:54,598
Is it really my birthday?

78
00:16:54,682 --> 00:16:57,182
Do you no ken that, a big laddie like you?

79
00:17:04,515 --> 00:17:06,765
What are you going to do when you grow up,
son?

80
00:17:06,848 --> 00:17:09,223
I hope to God he'll not be anything like you!

81
00:17:09,307 --> 00:17:13,265
- Well, I think I've been no bad in the past.
- Just get out. Go on, get out.

82
00:17:13,348 --> 00:17:17,182
Go back to wherever you came from,
and take your birthday present with you!

83
00:17:17,265 --> 00:17:19,973
I brought him a bird because
I've been thinking about him.

84
00:17:20,057 --> 00:17:23,557
He's no needing anything from the likes of you!
Go on, get out!

85
00:17:23,640 --> 00:17:25,515
Right!

86
00:17:30,473 --> 00:17:32,307
What did you do that for?

87
00:17:32,390 --> 00:17:34,848
Because he's no earthly good to man or beast!

88
00:17:41,598 --> 00:17:42,765
Da!

89
00:17:46,348 --> 00:17:48,390
Da, come back!

90
00:17:50,015 --> 00:17:52,723
Da, come back!

91
00:19:13,765 --> 00:19:15,931
Just you leave my birthday cage alone!

92
00:19:16,931 --> 00:19:20,015
I'll take it away if you want,
but just leave it alone!

93
00:19:20,098 --> 00:19:22,140
Right?

94
00:19:22,223 --> 00:19:24,265
OK?

95
00:19:44,348 --> 00:19:46,473
You'll be all right with me, Joey, eh?

96
00:19:46,556 --> 00:19:48,598
You'll be all right with me, eh?

97
00:19:48,681 --> 00:19:50,723
What's wrong?

98
00:20:09,765 --> 00:20:11,806
Tommy, Granny's lost.

99
00:20:11,890 --> 00:20:13,931
I'm not bothered!

100
00:22:24,181 --> 00:22:26,223
Das... Das ist in Deutsch

101
00:22:26,306 --> 00:22:29,181
- Apfel.
- Apfel.

102
00:22:34,140 --> 00:22:36,181
Und das ist in Deutsch

103
00:22:36,973 --> 00:22:39,181
Junge. Kleiner Junge.

104
00:22:42,264 --> 00:22:44,306
I want something easy.

105
00:22:45,181 --> 00:22:47,556
Kleiner Junge, ich muss nach Hause gehen.

106
00:23:39,681 --> 00:23:41,598
Get away.

107
00:23:41,681 --> 00:23:45,806
If you don't let me play,
I'll tell Granny you've still got the canary.

108
00:23:45,889 --> 00:23:48,764
You're just jealous
because my dad gave it to me.

109
00:23:48,848 --> 00:23:51,014
- It's my dad, too.
- No, it's not.

110
00:23:51,098 --> 00:23:52,264
Yes, he is.

111
00:23:52,348 --> 00:23:54,389
I know a secret.

112
00:23:54,473 --> 00:23:56,514
What?

113
00:24:01,389 --> 00:24:03,014
Well?

114
00:24:03,098 --> 00:24:05,139
OK.

115
00:24:17,598 --> 00:24:20,806
A whippet?

116
00:24:30,598 --> 00:24:32,348
Jamie, your da lives over there.

117
00:24:32,431 --> 00:24:34,473
Where?

118
00:24:34,556 --> 00:24:36,181
There.

119
00:24:36,973 --> 00:24:39,348
- How do you know?
- 'Cos I do.

120
00:25:30,723 --> 00:25:32,473
Get lost, you.

121
00:25:32,556 --> 00:25:34,556
I got do'd through you.

122
00:25:56,806 --> 00:25:58,848
Aye, it makes a difference...

123
00:27:00,681 --> 00:27:02,722
I love you, Helmuth.

124
00:27:02,806 --> 00:27:04,639
Guter Junge.

125
00:27:16,722 --> 00:27:22,681
~ Thou'it aye be dear to me

126
00:27:23,306 --> 00:27:26,806
~ Entwined thou art

127
00:27:27,472 --> 00:27:30,431
~ Wi'many ties

128
00:27:31,014 --> 00:27:37,222
~ O' hame and infancy

129
00:27:38,181 --> 00:27:44,806
~ Thy leaves were aye the first of spring

130
00:27:45,597 --> 00:27:52,472
~ Thy flowers the summer's pride

131
00:27:53,472 --> 00:28:01,014
~ There was nae sic a bonny tree

132
00:28:01,806 --> 00:28:08,556
~ In a'the countryside

133
00:28:09,097 --> 00:28:15,722
~ Oh, rowan tree

134
00:29:23,931 --> 00:29:25,972
Give me that cat!

135
00:29:26,764 --> 00:29:28,181
It's my cat.

136
00:29:28,264 --> 00:29:31,556
I'm going to kill it!

137
00:29:31,639 --> 00:29:33,847
It's my cat.

138
00:29:33,931 --> 00:29:36,514
- It's my cat.
- I'm going to kill it.

139
00:31:39,639 --> 00:31:41,139
Sixpence!

140
00:31:48,805 --> 00:31:50,222
You're mine.

141
00:31:51,180 --> 00:31:55,597
I took you in and loved you
when nobody in the world cared.

142
00:31:57,722 --> 00:32:01,347
God in heaven, the dreams I had for you.

143
00:32:03,722 --> 00:32:05,972
She was a whore, son.

144
00:32:06,889 --> 00:32:08,930
All women are whores.

145
00:32:10,139 --> 00:32:11,555
You're a king!

146
00:32:16,930 --> 00:32:18,764
How can they know?

147
00:32:59,055 --> 00:33:01,097
Where to, hen?

148
00:33:19,680 --> 00:33:21,722
That will be one and three.

149
00:33:28,180 --> 00:33:31,055
Oh, it's all right. We can afford it.

150
00:33:45,972 --> 00:33:48,013
Granny, who's Mary?

151
00:34:00,597 --> 00:34:02,972
Granny, I don't want to go to the hospital.

152
00:34:05,888 --> 00:34:09,763
Oh, come on, come on. What a naughty girl!

153
00:34:09,847 --> 00:34:13,347
This is no way to behave
when you've got visitors, is it, now?

154
00:34:13,430 --> 00:34:15,013
No, it isn't.

155
00:34:15,805 --> 00:34:17,847
Come on, now.

156
00:34:18,597 --> 00:34:20,638
That's a girl.

157
00:34:20,722 --> 00:34:22,763
There we are.

158
00:34:22,847 --> 00:34:24,888
That's better now, isn't it?

159
00:34:24,972 --> 00:34:27,013
I'll make you look pretty.

160
00:34:28,055 --> 00:34:31,222
There we go. That's it now.

161
00:34:31,305 --> 00:34:33,347
That's better, isn't it?

162
00:34:33,430 --> 00:34:35,472
That's lovely.

163
00:34:40,263 --> 00:34:42,305
That's it.

164
00:34:46,097 --> 00:34:48,138
There we are, now.

165
00:34:51,888 --> 00:34:53,888
That's fine.

166
00:34:53,972 --> 00:34:56,513
Now, are you feeling more like yourself, eh?

167
00:34:56,597 --> 00:34:58,638
Going to give us a wee smile?

168
00:34:58,722 --> 00:35:01,805
That's a girl!

169
00:35:01,888 --> 00:35:04,222
That's it, now. Comfy?

170
00:35:06,763 --> 00:35:08,597
And what's your name?

171
00:35:09,347 --> 00:35:11,972
Jamie. You stole my apple.

172
00:35:12,055 --> 00:35:14,055
That's an awfully nice name.

173
00:35:14,138 --> 00:35:16,930
Would you like to say hello to your mother,
Jamie?

174
00:35:18,972 --> 00:35:21,013
That's a boy.

175
00:38:02,555 --> 00:38:06,596
~ It's a long way to Tipperary

176
00:38:07,346 --> 00:38:11,596
~ To the sweetest girl I know

177
00:38:12,305 --> 00:38:16,221
~ Goodbye, Piccadilly

178
00:38:16,888 --> 00:38:20,638
~ Farewell, Leicester Square

179
00:38:21,221 --> 00:38:25,263
~ It's a long, long way to Tipperary

180
00:38:25,721 --> 00:38:30,013
~ But my heart's right there

181
00:39:03,596 --> 00:39:05,638
There, you... you hold it.

182
00:39:07,221 --> 00:39:11,388
It's red... red and yellow.

183
00:39:11,471 --> 00:39:13,513
Nice start.

184
00:39:13,596 --> 00:39:15,638
Can you hold it all right?

185
00:39:15,721 --> 00:39:18,263
All the different colours of the flags.

186
00:39:19,055 --> 00:39:21,096
Ja, nice.

187
00:39:21,888 --> 00:39:26,013
- Nice kite. Watch, don't let it go too far.
We're going now!

188
00:39:28,055 --> 00:39:29,471
Jamie...

189
00:39:29,555 --> 00:39:31,596
I have got to go home now.

190
00:39:32,513 --> 00:39:34,555
Auf Wiedersehen, Jamie.

191
00:39:42,721 --> 00:39:45,555
Helmuth!

192
00:39:47,013 --> 00:39:50,930
Helmuth!

193
00:40:11,388 --> 00:40:14,554
What's the matter, Jamie?
Don't you feel well?

194
00:40:21,471 --> 00:40:23,513
Is it because of your dad?

195
00:40:30,888 --> 00:40:32,929
It'll be all right, Jamie.

196
00:40:33,013 --> 00:40:35,346
Me and Granny'll look after you.

197
00:42:05,013 --> 00:42:07,054
We'd better go and get your da.

