1
00:00:00,000 --> 00:00:06,500
MADAME BUTTERFLY
after the world famous opera in 6 acts.

2
00:00:06,501 --> 00:00:14,001
Characters:
Tokuyawa, the daimyo
O-Take-San, his daughter
The Bonze
Karan, the temple servant

3
00:00:14,002 --> 00:00:22,302
Karan, the temple servant
Kin-be-Araki, the teahouse keeper
Hanake, O-take-San's servant girl
Olaf J. Anderson
Eva

4
00:00:22,303 --> 00:00:29,003
Tokuyawa, the Daimyo
travels from Europe back to Japan.

5
00:00:43,504 --> 00:00:49,404
O-Take-San,
his daughter

6
00:01:24,305 --> 00:01:32,605
"Father, I am so happy
that You have returned ----
during Your absence I had to suffer
so much from the Bonze!"

7
00:02:43,106 --> 00:02:48,706
"O-Take-San, your father has brought
this for you from the foreign lands."

8
00:02:51,707 --> 00:02:54,407
The Bonze.

9
00:03:04,908 --> 00:03:08,208
Karan, the temple servant.

10
00:03:13,009 --> 00:03:23,609
"Noble Lord, Your servant is guarding
the Holy Shrine from
the trampling of foreign feet!"

11
00:04:03,110 --> 00:04:07,510
"May Buddha
bless Your return!"

12
00:04:34,411 --> 00:04:41,511
"You have taken
many foreign things to Lippon again."

13
00:04:50,612 --> 00:04:56,512
"I noticed from the gifts
for Your daughter."

14
00:05:02,013 --> 00:05:05,913
Olaf J. Anderson.

15
00:05:25,514 --> 00:05:34,014
...So, in a couple of days
O-Take-San will become
priestess in the 'Holy Forest'...

16
00:05:37,815 --> 00:05:43,015
"If my child wants to -
I shall not force her!"

17
00:05:46,216 --> 00:05:53,016
"You have lost your faith in Buddha
in those foreign lands!
Fear his wrath!"

18
00:06:37,117 --> 00:06:46,617
"That awful Bonze persecutes me, because
I have perceived his unclean purposes...
but I will pray to Buddha myself...
and Buddha will hear O-Take-San!"

19
00:07:25,618 --> 00:07:36,718
"Forgive Your poor servant, who is
not worthy to be Buddha's priestess...
but who wants to sacrifice
to the Noble One her greatest and
dearest treasures: her puppets."

20
00:07:52,619 --> 00:07:56,519
"Buddha will make sure
that You are punished."

21
00:08:31,420 --> 00:08:37,220
Three weeks later the festival
of the 'Falling Leaves' is celebrated.

22
00:08:54,421 --> 00:09:04,621
'May Buddha bless you!
The Mikade, his name be praised.
Thanks You for Your message about
the traitorous Daimyo, who, with foreign
doctrines wants to incite the people against
the sanctified person of the Mikado.
The rebellious one will not escape his punishment...'

23
00:10:08,422 --> 00:10:14,922
"An envoy from the Mikado
awaits You, master!"

24
00:11:08,923 --> 00:11:19,223
"The noble Mikado sends You this gift...
You know what it means...
within 24 hours You must bid life farewell!"

25
00:12:02,824 --> 00:12:11,124
"A gift from our merciful Lord,
the Mikado. A sign of his mercy!"

26
00:12:26,425 --> 00:12:30,525
"Go into the garden,
for our guests are missing You!"

27
00:14:43,726 --> 00:14:48,126
"O-Take-San has something
to tell You!"

28
00:15:13,827 --> 00:15:16,527
"The Daimyo is dead!"

29
00:15:28,728 --> 00:15:36,428
"As final sign of his devotion
to the Mikado, he gave him his life."

30
00:15:46,829 --> 00:15:57,129
"That is the palace of Daimyo Tokuyawa,
he is a favorite of the Mikado
and his house is one of
the most hospitable in Japan."

31
00:16:51,130 --> 00:16:59,330
"Your father was punished for your sins
by Buddha. Don't change your mind,
Buddha is strict, so serve him as priestess."

32
00:17:15,831 --> 00:17:24,231
"Behind this wall lies
the 'Holy forest',
where no European may enter!"

33
00:17:30,332 --> 00:17:39,632
"Prepare yourself for it, O-Take-San,
in a couple of days your initiation
as priestess will take place."

34
00:18:35,389 --> 00:18:43,833
"How did You get here, stranger?
Don't you know our strict laws?"

35
00:19:26,434 --> 00:19:34,634
"If You don't want to
fall into the hands of guards
You should leave quickly!"

36
00:19:43,335 --> 00:19:46,135
"Come tomorrow at the same hour..."

37
00:19:56,936 --> 00:20:09,036
"O-Take-San would be
a very good Geisha
for the keeper of the Teahouse."

38
00:20:35,837 --> 00:20:37,937
"....a normal garden...."

39
00:20:54,538 --> 00:21:02,238
"I cannot follow You,
for soon I will be consecrated
as Buddha's priestess."

40
00:22:17,739 --> 00:22:20,739
"...in the evening".

41
00:22:49,340 --> 00:22:55,940
"Come quickly, O-Take-San,
I want free you, but
promise me you will be quit!"

42
00:23:11,441 --> 00:23:18,741
"Olaf, you changed a lot.
Come with us to the Teahouse!"

43
00:24:19,042 --> 00:24:23,742
"A new geisha!"

44
00:25:23,943 --> 00:25:31,143
"If you pay well, I will bring you
the most beautiful geisha in Nagasaki."

45
00:26:04,899 --> 00:26:18,144
"Noble chief, You can only keep this geisha
if, according to the laws of the Yoshiwara,
you marry her for 999 days.
You have, however, the right
to divorce her, whenever you want."

46
00:26:20,045 --> 00:26:26,445
"I am willing."

47
00:26:35,046 --> 00:26:39,046
The next morning.

48
00:26:46,547 --> 00:26:48,947
"Karan!"

49
00:27:07,848 --> 00:27:12,148
"Where is O-Take-San?"

50
00:27:13,949 --> 00:27:18,549
"Bring me O-Take-San,
or I.."

51
00:27:20,350 --> 00:27:23,050
Some time later.

52
00:27:24,451 --> 00:27:45,151
"I can't approve the marriage of
Olaf Anderson with that Japanese girl, for it
won't be long before we return home!"

53
00:28:56,101 --> 00:29:06,952
"Holy Priest, after much effort
I've found O-Take-San.
She is married to a foreign marine officer
and lives in the Teahouse on the hill."

54
00:29:55,100 --> 00:29:59,540
"Be cursed,
an unfaithful one."

55
00:30:24,520 --> 00:30:26,954
The weeks later.

56
00:30:37,500 --> 00:30:41,855
"I went to collect
my inheritance..."

57
00:30:45,310 --> 00:30:58,056
"This illustrates the three virtues
of Japanese girls! They may hear nothing,
speak nothing and see nothing."

58
00:31:41,690 --> 00:31:50,157
"This is the sword with which my father
killed himself, by order of the Mikado!"

59
00:34:01,711 --> 00:34:10,953
"You shouldn't put so much faith
in your happiness, O-Take-San...
it could change quickly!"

60
00:34:15,990 --> 00:34:21,187
"Olaf will never leave me..!"

61
00:34:30,290 --> 00:34:37,388
"My dear Olaf, it is not nice of you,
to make your little wife sad."

62
00:34:48,590 --> 00:34:55,589
"That little O-Take-San
seems to really love you!"

63
00:36:10,846 --> 00:36:16,790
...and when the Honeymoon
was over...

64
00:36:24,791 --> 00:36:33,091
"Greetings O-Take-San, don't be sad...
I am travelling back to Europe, but...
I will return!"

65
00:37:16,030 --> 00:37:23,792
"Come back swiftly Olaf...
You do know... that you...
must come back!"

66
00:37:40,160 --> 00:37:46,393
The memories of the land
where the peaches grow.

67
00:38:18,694 --> 00:38:24,060
...some little geisha
somewhere...

68
00:38:48,740 --> 00:38:59,600
"Little Olav, repeat after me:	
Great Buddha, grant me my wish and make
O-Take-San happy again!"

69
00:39:12,001 --> 00:39:15,901
Lord Matahari.

70
00:39:57,124 --> 00:40:10,102
"In four weeks it will be four years ago,
that O-Take-San's husband left her.
According to our laws, she will be free again then,
and must return to the Yoshiwara!"

71
00:40:14,360 --> 00:40:21,103
"...and if she has no money
to buy herself free!"...

72
00:40:36,734 --> 00:40:41,660
"I want to know immediately
where O-Take-San lives!"

73
00:41:00,061 --> 00:41:17,600
Dear Olaf!
You probably still remember that
little geisha, who you married years ago.
The poor creature has taken the affair
very seriously, and has been waiting all these
years with her child in tender longing for you...

74
00:41:26,860 --> 00:41:33,270
"O-Take-San lives with the keeper
of the Teahouse, Kin-Be-Araki!"

75
00:41:44,460 --> 00:41:54,671
"The Bonze wants you to ask her
for the rent for the room... 
if O-Take-San cannot pay...
she must remain in your Teahouse!"

76
00:42:09,700 --> 00:42:14,030
"That stranger paid for three years, O-Take-San
has been living here with me for four years
now and if does not pay the rent,
she must leave this house immediately."

77
00:42:29,731 --> 00:42:39,690
"I have nothing, but my husband,
Olaf Jens Anderson, will be back soon,
and then you will certainly get the money!"

78
00:43:26,930 --> 00:43:32,691
"In my department I will not allow
the oppression of the poor!"

79
00:44:03,292 --> 00:44:09,660
"You won't be bothered by
that Landlord again, take this money!"

80
00:45:03,320 --> 00:45:09,680
"O-Take-San is beautiful...
and I don't want her to lack anything!"

81
00:45:27,581 --> 00:45:34,081
"Today I got the order to set sail
for Japan. Would you like to come along?"

82
00:45:37,382 --> 00:45:45,782
...and when the peach trees
were in bloom again...

83
00:45:52,440 --> 00:45:59,450
"O-Take-San, an emissary
from Lord Matahari awaits you!"

84
00:46:47,710 --> 00:46:52,400
"I love you, O-Take-San,
and if you want..."

85
00:46:55,601 --> 00:47:00,001
"O-Take-San, would be my wife?"

86
00:47:00,002 --> 00:47:09,602
"I do not want your wealth.
I am the wife of Olaf Anderson.
I'd rather die than be unfaithful to him!"

87
00:47:11,920 --> 00:47:21,320
"You are mistaken, O-Take-San...
Olaf Anderson is not your husband
and your mariage..."

88
00:47:23,200 --> 00:47:31,321
"I won't have you insult my husband,
he'll return soon to me and his child!"

89
00:48:07,800 --> 00:48:17,522
"A European ship has entered the harbor...
I can feel that... it's Olaf!"

90
00:48:25,780 --> 00:48:35,500
"Come, Hanake, let us decorate the house
with flowers for the reception!"

91
00:49:57,490 --> 00:50:00,101
After a night of waiting awake in vain...

92
00:51:39,002 --> 00:51:43,102
"I'm going to the consulate
to ask for him!"

93
00:52:17,103 --> 00:52:25,090
"O-Take-San's time is up. She must
go back to the Yoshiwara. The child
will be entrusted to the State."

94
00:53:03,391 --> 00:53:09,391
"Master, help! They want
to steal O-Take-San's child!"

95
00:53:11,760 --> 00:53:15,892
"Olaf Anderson, help for godsake!
They are trying to steal your child!"

96
00:53:48,693 --> 00:53:54,860
"...now that she is free again,
she must return to the 'Holy Forest'!"

97
00:54:04,861 --> 00:54:15,200
"Go away! You weren't concerned
 about Buddha's sacred service.
You just wanted to have your revenge!"

98
00:54:44,360 --> 00:54:57,101
"My gratitude for your protection, Lord. 
But I cannot become your wife...
I am the wife of Olaf Anderson!"

99
00:55:22,202 --> 00:55:30,270
"Come, it is at least your duty
to help that poor O-Take-San!"


100
00:55:53,690 --> 00:55:58,020
"He is coming, O-Take-San!
He is coming!"

101
00:56:19,080 --> 00:56:27,921
"Why doesn't he come himself...
I know... he has forgotten me...
it was Your will!"

102
00:57:08,422 --> 00:57:14,622
"He can come collect
the child himself, tell him that!"

103
00:57:46,720 --> 00:57:51,700
"Go inside and get your child!"

104
00:58:31,170 --> 00:58:37,360
"It's better to die with honour
than to live in shame!"

105
00:58:43,060 --> 00:58:46,260
"Where is O-Take-San?"

106
00:58:58,761 --> 00:59:00,261
"O-Take-San is dead!"

107
00:59:45,962 --> 00:59:48,062
THE END

108
00:59:48,063 --> 00:59:49,263
Translated by NdhW,
timing by sahua.

