﻿1
00:00:01,001 --> 00:00:02,878
[Jenna] Previously on Backstage...

2
00:00:02,961 --> 00:00:06,715
May I introduce one of Keaton's
most famous alumni, Maria Schiller.

3
00:00:06,798 --> 00:00:08,967
[Carly] You're the reason Maria got fired.

4
00:00:09,051 --> 00:00:10,260
Or did you forget?

5
00:00:11,803 --> 00:00:15,057
- You kissed my best friend.
- You kissed a backup dancer.

6
00:00:15,140 --> 00:00:17,935
Will you, Julie Maslany,

7
00:00:18,018 --> 00:00:20,229
- let me take you out?
- [Julie] I'd love that.

8
00:00:20,312 --> 00:00:21,355
Are you okay?

9
00:00:21,939 --> 00:00:23,565
No. It's just this.

10
00:00:23,649 --> 00:00:24,650
Sorry, I can't.

11
00:00:26,401 --> 00:00:27,778
We should do a duet at CAMDAs.

12
00:00:27,861 --> 00:00:30,239
Why should Beckett and Carly
be the only ones?

13
00:00:30,322 --> 00:00:32,967
- [Sasha] Because they're the best.
- [Jenna] We can be as good as them.

14
00:00:32,991 --> 00:00:34,326
I can't.

15
00:00:34,409 --> 00:00:36,828
[Jenna]
"Manic Muscle Transformation Cycle"?

16
00:00:37,412 --> 00:00:38,412
What is this junk?

17
00:00:39,540 --> 00:00:40,541
What is he eating?

18
00:00:40,624 --> 00:00:42,000
You don't know anything about it.

19
00:00:42,084 --> 00:00:44,169
I just did that lift
for the first time ever.

20
00:00:44,253 --> 00:00:45,337
I'm not stopping now.

21
00:00:47,547 --> 00:00:49,400
[man] Hope you've all had
a great morning, folks,

22
00:00:49,424 --> 00:00:51,718
and are enjoying the first half
of the CAMDA qualifiers.

23
00:00:52,302 --> 00:00:55,055
As of now, Appleton School of the Arts
sits in first place

24
00:00:55,138 --> 00:00:57,391
with a combined score of 63.

25
00:00:57,474 --> 00:00:59,810
Keaton is right on their tails
with 61 points

26
00:00:59,893 --> 00:01:02,646
and Tumpane rounds out
the top three with 56.

27
00:01:03,272 --> 00:01:04,731
On behalf of all of Keaton,

28
00:01:05,357 --> 00:01:08,443
thank you to our officials
and good luck to all of our competitors.

29
00:01:08,527 --> 00:01:10,904
["Kings and Queens" playing]

30
00:01:11,363 --> 00:01:15,409
♪ Moving through the crowd
Dancing, singing loud ♪

31
00:01:15,492 --> 00:01:19,705
♪ Out there we'll find our place
Tilt our heads and wear the crown ♪

32
00:01:19,788 --> 00:01:23,834
♪ Step into the scene
On the edge of dreams ♪

33
00:01:23,917 --> 00:01:27,754
♪ I know we'll rule this place
Once we pretend to be ♪

34
00:01:27,838 --> 00:01:31,591
♪ King and queens
King and queens ♪

35
00:01:31,675 --> 00:01:32,718
♪ King and queens... ♪

36
00:01:32,801 --> 00:01:35,887
Hey, um, nice... nice sparkles.

37
00:01:36,471 --> 00:01:37,597
"Nice sparkles"?

38
00:01:40,600 --> 00:01:41,601
[sighs]

39
00:01:41,685 --> 00:01:44,021
Well, good luck today.

40
00:01:44,271 --> 00:01:45,480
[footsteps approaching]

41
00:01:45,564 --> 00:01:47,357
Ooh, what was that about?

42
00:01:48,066 --> 00:01:51,153
- Are you two getting back together?
- No. Definitely not.

43
00:01:51,236 --> 00:01:53,298
If I didn't know any better,
I'd say you were nervous.

44
00:01:53,322 --> 00:01:54,781
My stomach is a mess.

45
00:01:55,741 --> 00:01:57,784
They say that's a sign of good luck,
right?

46
00:01:58,827 --> 00:02:00,954
- Whatever you need to tell yourself.
- [Julie sighs]

47
00:02:02,122 --> 00:02:05,042
You have nothing to be nervous about.
You're a defending champion.

48
00:02:05,125 --> 00:02:07,336
Sure. But I've never competed in dance.

49
00:02:07,419 --> 00:02:11,423
You're Julie Maslany, the one and only.
There's nothing you can't do.

50
00:02:12,132 --> 00:02:13,133
Except maybe sing.

51
00:02:13,216 --> 00:02:14,426
[chuckles]

52
00:02:14,509 --> 00:02:17,054
Thanks, Scarlett.
I really needed to hear that.

53
00:02:21,475 --> 00:02:22,475
[sighs]

54
00:02:23,518 --> 00:02:24,839
[Sasha] Do you smell that, Jenna?

55
00:02:25,187 --> 00:02:27,457
Sometimes my stomach
gets a bit nervous the day of competition.

56
00:02:27,481 --> 00:02:29,983
No. Not that. Victory.

57
00:02:30,067 --> 00:02:31,610
I can smell it.

58
00:02:31,693 --> 00:02:33,820
We're gonna own these noobs.
[growls] Whoa!

59
00:02:35,322 --> 00:02:38,075
Wow, there's a lot of "whoa!"

60
00:02:38,825 --> 00:02:41,546
- Sure everything's all right with you?
- Oh, yeah, better than okay.

61
00:02:42,120 --> 00:02:43,997
Seriously, J,
no matter what happens today,

62
00:02:44,081 --> 00:02:46,875
I'm so glad we're dancing together.
I really needed this.

63
00:02:48,210 --> 00:02:50,670
This is me.
See you on the dance floor, señorita.

64
00:02:50,754 --> 00:02:51,838
[speaks in Spanish]

65
00:02:52,214 --> 00:02:53,214
Hyah! Whoo!

66
00:03:16,446 --> 00:03:17,322
["Spark" playing]

67
00:03:17,406 --> 00:03:20,659
♪ My eyes are open
But I cannot see ♪

68
00:03:21,660 --> 00:03:26,123
♪ And I'm up and walking
Still feels like a dream ♪

69
00:03:27,541 --> 00:03:33,755
♪ Until our eyes adjust, I guess
We're only feeling in the darkness ♪

70
00:03:33,839 --> 00:03:35,966
♪ Oh, oh, oh ♪

71
00:03:36,049 --> 00:03:38,510
- ♪ We just need a spark ♪
- ♪ Oh, oh ♪

72
00:03:38,593 --> 00:03:41,388
- ♪ To light up the dark ♪
- ♪ Oh, oh ♪

73
00:03:41,471 --> 00:03:46,101
♪ I can feel you with me
I can feel you with me ♪

74
00:03:46,184 --> 00:03:48,729
- ♪ We just need a spark ♪
- ♪ Oh, oh ♪

75
00:03:48,812 --> 00:03:51,523
- ♪ To light up the dark ♪
- ♪ Oh, oh ♪

76
00:03:51,606 --> 00:03:56,653
♪ I can feel you with me
I can feel you with me ♪

77
00:03:56,737 --> 00:03:58,905
♪ We just need a spark ♪

78
00:04:01,491 --> 00:04:06,413
[man on PA system] Strings competition
has been moved to Music Room 212.

79
00:04:06,496 --> 00:04:09,958
That's the strings competition
has been moved to Music Room 212.

80
00:04:10,041 --> 00:04:11,209
Thank you.

81
00:04:11,293 --> 00:04:14,087
[muffled music playing on headset]

82
00:04:14,171 --> 00:04:15,088
Give it up already.

83
00:04:15,172 --> 00:04:16,923
You need to learn when enough is enough.

84
00:04:17,007 --> 00:04:18,609
I just need to listen to it one more time.

85
00:04:18,633 --> 00:04:20,552
You've been tweaking
the same section of reverb

86
00:04:20,635 --> 00:04:22,220
for the past two days.

87
00:04:22,304 --> 00:04:26,391
- It's mixed, mastered. Export already.
- Kit...

88
00:04:26,475 --> 00:04:27,851
[giggling] Maybe.

89
00:04:27,934 --> 00:04:31,104
Bianca, hey, you look stunning today.

90
00:04:31,730 --> 00:04:33,440
- [door closing]
- [laptop clicks]

91
00:04:34,232 --> 00:04:35,232
[sighs]

92
00:04:36,568 --> 00:04:39,112
You're right. I need to learn
when enough is enough.

93
00:04:40,155 --> 00:04:43,617
- The song, I exported it. It's done.
- But I...

94
00:04:46,286 --> 00:04:47,954
You're right. It's done.

95
00:04:54,795 --> 00:04:55,796
[clicks]

96
00:05:01,301 --> 00:05:06,640
Aren't you glad we're in production?
No weird warm-ups, no crazy costumes.

97
00:05:08,183 --> 00:05:10,769
No... whatever that is.

98
00:05:10,852 --> 00:05:12,479
[snorts]

99
00:05:15,649 --> 00:05:19,319
So judges' results won't be posted
till the end of the day.

100
00:05:19,402 --> 00:05:20,763
What do you want to do until then?

101
00:05:21,363 --> 00:05:24,991
I mean, we could go see
what Bianca is up to.

102
00:05:26,493 --> 00:05:28,537
No. Not that.

103
00:05:28,620 --> 00:05:30,664
- But you...
- No, Jax.

104
00:05:32,082 --> 00:05:33,375
[sighs]

105
00:05:34,793 --> 00:05:36,044
I got an idea.

106
00:05:43,885 --> 00:05:45,720
[upbeat music playing]

107
00:05:54,271 --> 00:05:55,188
Hi, excuse me.

108
00:05:55,272 --> 00:05:57,857
Julie Maslany.
Do I have time to run to the bathroom?

109
00:05:58,900 --> 00:06:00,277
[paper rustling]

110
00:06:00,443 --> 00:06:01,443
No.

111
00:06:02,070 --> 00:06:04,573
It's okay. It's probably just nerves.

112
00:06:05,865 --> 00:06:09,119
[audience clapping, cheering]

113
00:06:11,830 --> 00:06:14,249
- Ms. Maria.
- Oh, Julie!

114
00:06:14,332 --> 00:06:15,375
[chuckles] My gosh.

115
00:06:17,043 --> 00:06:20,547
Oh, I shouldn't be seen talking
to the competition.

116
00:06:21,298 --> 00:06:24,092
- Right, I'm sorry. I'll leave you...
- No.

117
00:06:24,175 --> 00:06:25,719
I'm kidding.

118
00:06:25,802 --> 00:06:27,220
- How are you?
- Great.

119
00:06:27,304 --> 00:06:28,638
- Super nervous.
- Mm-hm.

120
00:06:28,722 --> 00:06:29,973
- But great.
- Ah.

121
00:06:30,056 --> 00:06:33,518
Oh, I'm sorry. Julie, this is Katy.

122
00:06:33,602 --> 00:06:36,062
- Katy this is Ju...
- You're Julie Maslany.

123
00:06:36,146 --> 00:06:39,190
I've heard so much about you.
You went to Juilliard's summer program.

124
00:06:39,983 --> 00:06:41,526
How did you...?

125
00:06:41,610 --> 00:06:45,238
Visual artist turned Juilliard-worthy
dancer in like, five minutes?

126
00:06:45,322 --> 00:06:48,241
You're kind of a legend.
You must be insanely talented.

127
00:06:48,325 --> 00:06:50,118
More like insanely lucky.

128
00:06:50,201 --> 00:06:52,787
I saw you dance just now.
You have amazing feet.

129
00:06:53,413 --> 00:06:54,932
- [Katy] And she's nice too?
- [chuckles]

130
00:06:54,956 --> 00:06:57,876
Most of these girls won't even look
at another dancer during competition.

131
00:06:57,959 --> 00:07:01,463
I'm not sure if I'm supposed to say this,
but good luck.

132
00:07:02,047 --> 00:07:03,047
Same to you.

133
00:07:05,925 --> 00:07:08,053
The judges are having
a five-minute water break.

134
00:07:08,136 --> 00:07:10,555
- If you need to use the restroom, go now.
- I'm good.

135
00:07:12,849 --> 00:07:15,560
I'm not nervous anymore.
[sighs]

136
00:07:15,644 --> 00:07:18,647
Twenty six... [grunts] 27, twenty...

137
00:07:18,730 --> 00:07:19,731
[yelps] Agh!

138
00:07:24,778 --> 00:07:25,820
Here.

139
00:07:25,904 --> 00:07:26,988
Cramping.

140
00:07:27,072 --> 00:07:28,406
No, thank you.

141
00:07:29,032 --> 00:07:30,176
Come on, dude, you need water.

142
00:07:30,200 --> 00:07:31,743
I said I'm good.

143
00:07:35,872 --> 00:07:38,875
Sometimes, body builders
don't drink water before a competition.

144
00:07:40,293 --> 00:07:42,879
Dehydration shows off muscle definition.

145
00:07:44,756 --> 00:07:48,343
But Sasha's a dancer,
and dancers need water.

146
00:07:50,136 --> 00:07:52,597
I've worked way too hard
to lose this definition now.

147
00:07:52,681 --> 00:07:54,307
[grunts]

148
00:07:54,391 --> 00:07:55,517
[huffs]

149
00:07:55,976 --> 00:07:57,060
[grunts]

150
00:08:04,818 --> 00:08:06,820
["Fall In" playing]

151
00:08:18,248 --> 00:08:20,750
♪ Is something holding you back? ♪

152
00:08:22,919 --> 00:08:24,879
It's never too late for second chances.

153
00:08:26,756 --> 00:08:28,758
For me and Julie, I'm pretty sure it is.

154
00:08:29,467 --> 00:08:32,137
[chuckles] Let's be honest.
Your chances were slim to begin with.

155
00:08:34,681 --> 00:08:35,682
I finally figured it out.

156
00:08:37,183 --> 00:08:38,393
The break-up with Alya

157
00:08:39,310 --> 00:08:41,312
had more to do with her
than it did with me.

158
00:08:43,398 --> 00:08:45,108
You deserve a fresh start.

159
00:08:46,776 --> 00:08:48,445
So do you.

160
00:08:50,030 --> 00:08:51,740
♪ Completely ♪

161
00:08:52,907 --> 00:08:57,787
♪ You got to charge head first ♪

162
00:08:59,456 --> 00:09:03,835
♪ Fall in completely ♪

163
00:09:05,378 --> 00:09:07,922
♪ You got to charge ♪

164
00:09:08,006 --> 00:09:10,008
[applause]

165
00:09:10,800 --> 00:09:11,843
[boy] Good job!

166
00:09:15,388 --> 00:09:17,307
That was "cirque du so cray"!

167
00:09:17,390 --> 00:09:19,517
- Thank you, I think.
- [both chuckle]

168
00:09:24,147 --> 00:09:25,482
I totally got this.

169
00:09:26,566 --> 00:09:27,566
[Julie] Was that...?

170
00:09:28,818 --> 00:09:30,945
That girl just threw you
some serious shade.

171
00:09:35,492 --> 00:09:37,702
[Jax] That one kinda looks like a balloon.

172
00:09:37,786 --> 00:09:39,662
Surprised he didn't say Bianca.

173
00:09:40,413 --> 00:09:42,874
No way. It's clearly an eggplant.

174
00:09:45,668 --> 00:09:46,829
[Jax] Oh, yeah, you're right.

175
00:09:48,254 --> 00:09:50,381
- Hey, you know who'd love this?
- Jax.

176
00:09:50,465 --> 00:09:52,258
Spoke too soon.

177
00:09:52,842 --> 00:09:56,888
Sunshine, blue skies.
What's the point of it, Kit?

178
00:09:57,555 --> 00:10:00,266
What's the point of happiness
if it's just gonna be ripped away?

179
00:10:01,684 --> 00:10:04,187
That's it. [grunts]

180
00:10:05,396 --> 00:10:06,898
Go fight for her, Jax.

181
00:10:08,566 --> 00:10:11,653
- Wait, what about "enough is enough"?
- I take it back.

182
00:10:12,237 --> 00:10:14,989
Oh, no, no, no, no. Don't get me wrong.

183
00:10:15,406 --> 00:10:18,034
I clearly think
that you've lost a few marbles

184
00:10:18,118 --> 00:10:20,703
and it's probably
not gonna work out, but...

185
00:10:21,329 --> 00:10:23,540
you have to fight for her
like you fought for DJ D.

186
00:10:24,207 --> 00:10:26,042
Yeah, and look how well that worked out.

187
00:10:26,835 --> 00:10:29,003
What you did for Diamondmind
was incredible.

188
00:10:29,420 --> 00:10:31,381
You almost convinced me not to retire.

189
00:10:32,549 --> 00:10:33,842
I said, almost.

190
00:10:35,718 --> 00:10:37,720
Bianca must mean more to you
than some DJ.

191
00:10:37,804 --> 00:10:40,348
Of course, she does.
She means everything to me.

192
00:10:41,224 --> 00:10:44,310
She's the sun, the sky...

193
00:10:45,145 --> 00:10:47,480
[sighs] and the balloon-shaped clouds.

194
00:10:47,564 --> 00:10:50,483
If there's even
a slightest chance she says yes,

195
00:10:50,859 --> 00:10:52,735
you have to keep trying.

196
00:10:53,278 --> 00:10:54,821
You can't give up on her.

197
00:10:56,406 --> 00:10:59,117
I knew there was a romantic
hiding somewhere under that toque.

198
00:11:07,876 --> 00:11:10,044
I'm going to get Bianca Blackwell back.

199
00:11:12,755 --> 00:11:13,755
And Kit...

200
00:11:15,300 --> 00:11:16,885
you're not just some DJ.

201
00:11:17,635 --> 00:11:19,137
Go already.

202
00:11:38,072 --> 00:11:39,699
[indistinct chattering]

203
00:11:44,954 --> 00:11:48,708
Hey, I... I saw you dance.
It was amazing.

204
00:11:49,876 --> 00:11:52,337
Okay, this is not going well.

205
00:11:54,005 --> 00:11:56,883
Look... it's probably too late

206
00:11:56,966 --> 00:11:59,969
and if you wanna shut me down,
you have every right.

207
00:12:00,678 --> 00:12:03,264
But I have to try or I'll never know.

208
00:12:05,350 --> 00:12:08,102
Julie Maslany, will you go
on a second date with me?

209
00:12:10,855 --> 00:12:13,608
I can't deal with you right now.

210
00:12:20,031 --> 00:12:21,115
[Katy] Kit-Kat?

211
00:12:23,868 --> 00:12:25,870
Only one person calls me that.

212
00:12:27,163 --> 00:12:29,040
Katykins.

213
00:12:29,707 --> 00:12:34,337
When you spend two months on a tour bus,
you get nicknames.

214
00:12:36,089 --> 00:12:39,133
- Wow, you're at Keaton.
- Qualifiers.

215
00:12:39,217 --> 00:12:42,095
- I got through.
- Oh. Congrats.

216
00:12:42,553 --> 00:12:45,223
- I should get... [giggles]
- Is Jaxter around?

217
00:12:45,890 --> 00:12:47,100
Never heard of him.

218
00:12:47,767 --> 00:12:49,602
I thought you two were buds?

219
00:12:50,687 --> 00:12:51,729
Oh.

220
00:12:52,146 --> 00:12:54,816
You mean Jax...
Because you called him "Jaxter."

221
00:12:56,067 --> 00:12:57,735
Yeah, uh, he's around.

222
00:12:58,277 --> 00:13:01,489
If you see him first, don't tell him
I'm here. I wanna surprise him.

223
00:13:02,073 --> 00:13:03,157
Oh. [chuckles]

224
00:13:03,241 --> 00:13:06,369
You mean, don't tell Jax that the girl
he kissed over the summer is here

225
00:13:06,452 --> 00:13:09,831
on the same day that he's trying
to get back together with Bianca?

226
00:13:10,415 --> 00:13:11,916
He'll be surprised, all right.

227
00:13:31,561 --> 00:13:32,561
[sighs]

228
00:13:34,480 --> 00:13:35,480
Guess you saw.

229
00:13:36,441 --> 00:13:38,359
Sixth place is respectable.

230
00:13:38,860 --> 00:13:41,070
Especially for your first CAMDAs.

231
00:13:41,154 --> 00:13:42,154
Respectable?

232
00:13:42,697 --> 00:13:44,741
I didn't qualify for finals.

233
00:13:44,824 --> 00:13:46,659
How am I supposed to get into Juilliard?

234
00:13:47,243 --> 00:13:49,787
Because you didn't qualify
in one category?

235
00:13:49,871 --> 00:13:51,706
The most important category.

236
00:13:53,833 --> 00:13:57,462
Do you see where the girls are standing?
Right over there?

237
00:13:59,213 --> 00:14:02,675
That's where I was when I found out
I didn't get into Juilliard.

238
00:14:03,342 --> 00:14:05,762
Just because I didn't get in,
doesn't mean I gave up.

239
00:14:06,429 --> 00:14:09,390
But how do you keep going when
there are no guarantees it will work out?

240
00:14:10,183 --> 00:14:11,434
There is only one guarantee.

241
00:14:13,519 --> 00:14:17,065
If you quit now,
you'll never know what you're capable of.

242
00:14:18,733 --> 00:14:21,069
There's no shame
in getting knocked down, Julie.

243
00:14:21,861 --> 00:14:24,530
The bigger shame,
would be not getting back up.

244
00:14:27,158 --> 00:14:28,158
So...

245
00:14:30,244 --> 00:14:31,829
What's it gonna be?

246
00:14:37,251 --> 00:14:38,836
[indistinct chattering]

247
00:14:39,379 --> 00:14:42,840
- Does Sasha seem off to you?
- Everybody seems off to me.

248
00:14:42,924 --> 00:14:44,759
It's this competition,
it makes people crazy.

249
00:14:44,842 --> 00:14:46,344
Yeah, maybe.

250
00:14:46,969 --> 00:14:48,810
But they're working a bit hard
for warm-up, no?

251
00:14:49,889 --> 00:14:51,349
Maybe we're not working hard enough.

252
00:14:59,482 --> 00:15:00,358
- Whoa! Whoa!
- [Sasha grunts]

253
00:15:00,441 --> 00:15:01,567
[thumps, grunts]

254
00:15:01,651 --> 00:15:03,152
- Jenna!
- Hey, you okay?

255
00:15:03,820 --> 00:15:05,196
I'm okay.

256
00:15:05,279 --> 00:15:08,074
It sounded bad. You want me to get anyone?
Do you need ice?

257
00:15:08,157 --> 00:15:10,660
- I need to sit.
- Here. Come with me.

258
00:15:15,873 --> 00:15:19,085
Carly Catto and Beckett Bradstreet?
You're on deck.

259
00:15:19,669 --> 00:15:20,854
I've been watching you all day.

260
00:15:20,878 --> 00:15:22,880
You're cramped,
you've been overexerting yourself.

261
00:15:22,964 --> 00:15:24,465
- And now this.
- She shifted.

262
00:15:24,549 --> 00:15:26,801
I tried to break her fall.
What else could I do?

263
00:15:26,884 --> 00:15:28,405
I don't think you guys should compete.

264
00:15:28,469 --> 00:15:29,469
Wow.

265
00:15:29,720 --> 00:15:31,222
You're afraid we're gonna beat you.

266
00:15:31,931 --> 00:15:32,931
That's amazing.

267
00:15:33,349 --> 00:15:35,351
Carly Catto and Beckett Bradstreet?

268
00:15:36,060 --> 00:15:38,729
I'm sorry to disappoint you,
but Jenna and I are not dropping out.

269
00:15:38,813 --> 00:15:40,648
No, you're just dropping her.

270
00:15:40,731 --> 00:15:43,109
Catto and Bradstreet. Last call.

271
00:15:44,026 --> 00:15:45,026
That's us.

272
00:15:45,736 --> 00:15:46,946
Come on. Let's go.

273
00:15:51,784 --> 00:15:54,287
- You sure you're all right, J?
- Right as rain.

274
00:15:54,370 --> 00:15:56,622
I just got the wind knocked out of me.

275
00:15:57,331 --> 00:15:58,331
You okay?

276
00:15:59,876 --> 00:16:00,918
I hope so.

277
00:16:01,752 --> 00:16:03,421
- I'm okay.
- [sighs]

278
00:16:03,504 --> 00:16:06,048
- We got this.
- We got this.

279
00:16:06,132 --> 00:16:08,009
[audience applauding]

280
00:16:14,765 --> 00:16:16,726
["Love Don't Fight Fair" playing]

281
00:16:21,898 --> 00:16:24,609
♪ How am I gonna find a four-leaf clover ♪

282
00:16:25,485 --> 00:16:29,197
♪ Down here among the mess you made
Left here on my shoulders ♪?

283
00:16:29,280 --> 00:16:31,866
♪ And I can call these feelings
Into question ♪

284
00:16:31,949 --> 00:16:34,702
♪ Possession is nine-tenths of the law ♪

285
00:16:34,785 --> 00:16:38,039
It's just an expression I heard
Once before ♪

286
00:16:38,122 --> 00:16:42,752
♪ And cold feet
Can only get me so far ♪

287
00:16:43,252 --> 00:16:47,465
♪ I can make no promises
That I won't break ♪

288
00:16:48,049 --> 00:16:50,092
♪ Call it my mistake ♪

289
00:16:50,176 --> 00:16:54,472
♪ And I can't ever watch you
Being scared ♪

290
00:16:54,555 --> 00:16:57,975
♪ I've learned
That love don't fight fair ♪

291
00:17:00,144 --> 00:17:04,774
♪ I can make no promises
That I won't break ♪

292
00:17:04,857 --> 00:17:07,151
♪ Call it my mistake ♪

293
00:17:07,235 --> 00:17:11,364
♪ And I can't ever watch you
Being scared ♪

294
00:17:11,447 --> 00:17:14,367
♪ I've learned
That love don't fight fair ♪

295
00:17:21,415 --> 00:17:24,418
[applause]

296
00:17:32,510 --> 00:17:33,886
[shoes squeak, thumps]

297
00:17:33,970 --> 00:17:36,305
- [girls laughing]
- Jackson?

298
00:17:36,389 --> 00:17:38,590
All right, all right.
Not off to a great start, I agree,

299
00:17:38,641 --> 00:17:40,726
but please, please,
just hear me out.

300
00:17:44,939 --> 00:17:46,291
- I'll give it back. Thanks.
- [feedback squeals]

301
00:17:46,315 --> 00:17:47,316
Bianca Blackwell.

302
00:17:49,735 --> 00:17:53,197
I, Jackson Gardner... love you.

303
00:17:54,073 --> 00:17:56,284
And I am not gonna stop fighting for you.

304
00:17:57,201 --> 00:17:59,078
I made a mistake this summer,

305
00:18:00,246 --> 00:18:04,000
and I would do anything
to go back and erase it, but I can't.

306
00:18:04,125 --> 00:18:05,293
[mic snaps, squeals]

307
00:18:05,960 --> 00:18:08,963
I mean, unless anyone knows where
I can get my hands on a time machine.

308
00:18:09,839 --> 00:18:11,841
Anyone? No?

309
00:18:13,426 --> 00:18:15,052
I betrayed your trust.

310
00:18:15,928 --> 00:18:17,847
I don't deserve you.

311
00:18:18,514 --> 00:18:21,809
But if you just give me one more chance.
Just one more chance.

312
00:18:22,393 --> 00:18:25,354
I swear, I will never let you down again.

313
00:18:26,230 --> 00:18:27,982
You're a goof, Jackson Gardner.

314
00:18:29,442 --> 00:18:31,444
A goof who still has a girlfriend?

315
00:18:33,821 --> 00:18:35,781
Can't you see we're having
a moment here, Kit?

316
00:18:36,365 --> 00:18:37,366
Jax...?

317
00:18:38,701 --> 00:18:41,078
We hung out all summer,
and you never told me once

318
00:18:41,162 --> 00:18:42,747
you had a girlfriend.

319
00:18:45,166 --> 00:18:48,252
Hi. You must be the girlfriend.
I'm the mistake.

320
00:18:51,714 --> 00:18:53,591
Bianca, I don't... I...

321
00:18:55,509 --> 00:18:58,137
All summer? You told me it was one kiss.

322
00:18:58,220 --> 00:19:00,097
No, it was just one kiss, I swear.
I promise.

323
00:19:00,181 --> 00:19:01,265
Stop!

324
00:19:03,309 --> 00:19:04,727
We're over.

325
00:19:07,188 --> 00:19:08,773
We are so done.

326
00:19:21,744 --> 00:19:22,912
[plays chord]

327
00:19:35,341 --> 00:19:37,802
Hey, Miles. Can we talk?

328
00:19:38,219 --> 00:19:39,553
Of course.

329
00:19:41,389 --> 00:19:43,224
Listen, I'm sorry.

330
00:19:43,307 --> 00:19:46,560
- You don't need to apologize to me.
- No, I do.

331
00:19:46,644 --> 00:19:48,396
What happened before, it wasn't about you.

332
00:19:48,479 --> 00:19:51,023
- I was just upset about...
- We both make mistakes.

333
00:19:52,233 --> 00:19:53,401
Mostly me.

334
00:19:54,527 --> 00:19:56,153
So let's start over?

335
00:19:57,822 --> 00:20:00,366
Hi, I'm Miles. Nice to meet you.

336
00:20:00,991 --> 00:20:03,994
- Hi, Miles. I'm Rihanna.
- [chuckles]

337
00:20:07,873 --> 00:20:10,251
["You and I" playing]

338
00:20:13,462 --> 00:20:17,216
♪ Hey there,
Come close to the fire ♪

339
00:20:18,259 --> 00:20:23,764
♪ Feel the desire
Holding all the way ♪

340
00:20:23,848 --> 00:20:27,893
♪ I'm swimming up river
The love I give her ♪

341
00:20:28,561 --> 00:20:30,604
♪ The love I deliver ♪

342
00:20:31,605 --> 00:20:34,066
♪ All in a day ♪

343
00:20:34,150 --> 00:20:37,528
♪ Now you can see my hands are high ♪

344
00:20:37,611 --> 00:20:40,406
♪ I surrender to you
On the floor tonight ♪

345
00:20:40,489 --> 00:20:41,907
♪ Hey, hey ♪

346
00:20:43,075 --> 00:20:44,493
♪ Hey, hey ♪

347
00:20:45,286 --> 00:20:48,581
♪ So don't you leave me hanging by ♪

348
00:20:48,664 --> 00:20:51,417
♪ I believe in yours,
You believe in mine ♪

349
00:20:51,500 --> 00:20:54,336
♪ Don't tell that your hands are tied ♪

350
00:20:54,420 --> 00:20:57,715
♪ I believe in you, in you, in you and I ♪

351
00:21:02,261 --> 00:21:04,388
♪ For your love, girl ♪

352
00:21:05,097 --> 00:21:07,183
♪ I've been falling ♪

353
00:21:07,975 --> 00:21:10,311
♪ Isn't it obvious? ♪

354
00:21:10,936 --> 00:21:13,773
♪ 'Cause it is to me
And my friends agree ♪

355
00:21:13,856 --> 00:21:17,568
♪ They see my hands are high
I surrender to you... ♪

356
00:21:17,651 --> 00:21:20,571
This is it. This is everything
we've been working for.

357
00:21:20,654 --> 00:21:23,532
He's got me. I trust him.

358
00:21:24,116 --> 00:21:26,952
♪ I believe in yours,
You believe in mine ♪

359
00:21:27,036 --> 00:21:29,789
♪ Don't tell me that your hands are tied ♪

360
00:21:29,872 --> 00:21:34,001
♪ I believe in you, in you, in you and I ♪

361
00:21:34,084 --> 00:21:36,587
[applause, cheering]

362
00:21:44,970 --> 00:21:46,639
- We did it.
- You did it.

363
00:21:46,722 --> 00:21:48,599
- Finals, baby, here we come.
- [giggles]

364
00:21:48,682 --> 00:21:49,682
Hey, man.

365
00:21:53,479 --> 00:21:54,647
Respect.

366
00:21:56,732 --> 00:21:57,732
Jenna!

367
00:21:59,276 --> 00:22:00,517
[Beckett] Someone, get a medic!

368
00:22:00,569 --> 00:22:02,238
Someone, get a medic. Hey!

369
00:22:02,321 --> 00:22:04,323
["Love Don't Fight Fair" playing]

370
00:22:09,370 --> 00:22:12,081
♪ How am I gonna find a four-leaf clover ♪

371
00:22:12,957 --> 00:22:16,335
♪ Down here among the mess you made
Left here on my shoulders? ♪

372
00:22:16,418 --> 00:22:19,338
♪ And I can call these feelings
Into question ♪

373
00:22:19,421 --> 00:22:22,174
♪ Possession is nine-tenths of the law ♪

374
00:22:22,258 --> 00:22:25,761
It's just an expression I heard
Once before ♪

375
00:22:25,844 --> 00:22:30,224
♪ And cold feet
Can only get me so far ♪

