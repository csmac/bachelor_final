﻿1
00:00:00,000 --> 00:00:04,000
Subtitles provided by MBC

2
00:00:07,050 --> 00:00:08,720
[Episode 126]

3
00:00:20,000 --> 00:00:21,860
Yes, Sire?

4
00:00:21,860 --> 00:00:27,500
Is it true you put Pisang
in Kwanghae's medicine?

5
00:00:29,500 --> 00:00:31,100
Yes, Sire.

6
00:00:35,560 --> 00:00:40,530
You are relieved of duty.

7
00:00:40,530 --> 00:00:46,530
There was no other option at this point.

8
00:00:46,530 --> 00:00:53,530
Understand that I did
what I did to save him.

9
00:00:59,530 --> 00:01:04,530
Sire, I believe we can trust him.

10
00:01:09,460 --> 00:01:12,230
Your thoughts?

11
00:01:12,230 --> 00:01:17,530
It's not that I don't trust Doctor Heo.

12
00:01:17,530 --> 00:01:23,560
But I fear for the Crown Prince
in his current condition.

13
00:01:26,000 --> 00:01:32,260
Sire, please allow me
to continue treating him.

14
00:01:53,130 --> 00:01:57,530
Doctor, he is getting worse.

15
00:01:58,760 --> 00:02:04,360
Please administer
herbs to neutralize the Pisang.

16
00:02:05,830 --> 00:02:09,530
Doctor, if His Highness dies...

17
00:02:09,530 --> 00:02:14,430
...you will be framed for murder.

18
00:02:24,060 --> 00:02:28,700
We will give him
the same medicine in an hour.

19
00:02:28,700 --> 00:02:30,530
Doctor.

20
00:02:33,530 --> 00:02:38,260
How long? We have to stop him now.

21
00:02:39,930 --> 00:02:45,400
We'll trust him. It's our only choice now.

22
00:02:45,400 --> 00:02:48,360
He's working with
Prince Yeong-chang's side.

23
00:02:48,360 --> 00:02:50,300
How can we trust him?

24
00:02:50,300 --> 00:02:54,060
You saw the poison with your own eyes.

25
00:03:02,200 --> 00:03:03,700
Any improvement?

26
00:03:03,700 --> 00:03:06,530
We have to give it time.

27
00:03:06,530 --> 00:03:08,530
Give it time?

28
00:03:08,530 --> 00:03:12,430
Until you kill him, you mean?

29
00:03:12,430 --> 00:03:13,530
Enough.

30
00:03:13,530 --> 00:03:16,730
He's gotten worse since
the treatment started.

31
00:03:16,730 --> 00:03:19,900
He's getting better.

32
00:03:19,900 --> 00:03:24,230
Don't be fooled by the current symptoms.

33
00:03:24,230 --> 00:03:27,900
The Pisang is working on the malaria.

34
00:03:27,900 --> 00:03:33,530
The changes in temperature indicate
his body is fighting the disease.

35
00:03:33,530 --> 00:03:38,800
Now leave treatment to the professionals.

36
00:03:44,600 --> 00:03:49,530
Well, well, what's all the yelling about?

37
00:03:49,530 --> 00:03:51,060
Something you want?

38
00:03:51,060 --> 00:03:55,300
Something? The Crown Prince is ill.

39
00:03:55,300 --> 00:03:59,560
As loyal subjects
we're naturally concerned.

40
00:03:59,560 --> 00:04:02,560
We heard you yelling.

41
00:04:02,560 --> 00:04:06,200
May we see His Highness for ourselves?

42
00:04:06,200 --> 00:04:07,530
What?

43
00:04:09,260 --> 00:04:12,530
No one may see him.

44
00:04:12,530 --> 00:04:14,430
Oh?

45
00:04:14,430 --> 00:04:18,530
He must be worse than we heard.

46
00:04:20,530 --> 00:04:24,400
If you'll excuse me.

47
00:04:28,400 --> 00:04:32,330
You have a grave responsibility.

48
00:04:32,330 --> 00:04:38,130
We trust you are doing all you can.

49
00:04:40,530 --> 00:04:42,530
Sir.

50
00:04:47,330 --> 00:04:52,330
I hope he's better soon.

51
00:04:55,600 --> 00:04:59,160
Huh? Pisang?

52
00:05:00,660 --> 00:05:04,460
Poison in medicine? Are you kidding?

53
00:05:04,460 --> 00:05:09,530
I just did what I was told.

54
00:05:09,530 --> 00:05:13,530
How is the Crown Prince?

55
00:05:13,530 --> 00:05:16,760
Don't breathe a word of this.

56
00:05:16,760 --> 00:05:21,530
He's gotten worse.

57
00:05:21,530 --> 00:05:23,530
No...

58
00:05:23,530 --> 00:05:25,530
What about Jun?

59
00:05:25,530 --> 00:05:28,000
If the Prince dies...

60
00:05:28,000 --> 00:05:32,400
...he and I are as good as dead.

61
00:05:32,400 --> 00:05:35,660
I can't sleep or nothin'.

62
00:05:35,660 --> 00:05:40,530
There you are!

63
00:05:40,530 --> 00:05:43,530
I've been looking all over for you.

64
00:05:43,530 --> 00:05:44,900
Why? What is it?

65
00:05:44,900 --> 00:05:47,530
A word. Let's take a walk.

66
00:05:47,530 --> 00:05:50,630
- To where?
- Wherever.

67
00:05:50,630 --> 00:05:54,530
Honey-sweetness. Sweetie-honey.

68
00:05:54,530 --> 00:05:58,500
He's here, I brought him.

69
00:05:58,500 --> 00:06:03,830
Oh, it's been forever.

70
00:06:03,830 --> 00:06:08,800
You know my son-in-law?

71
00:06:08,800 --> 00:06:10,530
Well, bow.

72
00:06:10,530 --> 00:06:12,530
Sir.

73
00:06:12,530 --> 00:06:15,560
Great, great. Hey, On-nyeon.

74
00:06:15,560 --> 00:06:17,460
Listen.

75
00:06:17,460 --> 00:06:23,700
Have you heard of another way
into the royal pharmacy?

76
00:06:23,700 --> 00:06:27,530
Another way? Like sneaking in?

77
00:06:27,530 --> 00:06:29,530
No, no, that that.

78
00:06:29,530 --> 00:06:35,530
If Doctor Heo nominates him,
he can be a doctor too.

79
00:06:35,530 --> 00:06:40,530
I've heard of that happening.

80
00:06:40,530 --> 00:06:45,160
But it's for extremely gifted doctors.

81
00:06:45,160 --> 00:06:51,530
Your son-in-law is only good
at gambling and women.

82
00:06:51,530 --> 00:06:52,600
Only?

83
00:06:52,600 --> 00:06:56,530
He's Lord Gambler, Lord Letch.

84
00:06:56,530 --> 00:07:03,030
Are you any good at doctoring at all?

85
00:07:03,030 --> 00:07:07,730
I give great massages.

86
00:07:07,730 --> 00:07:09,430
Massages?

87
00:07:09,430 --> 00:07:12,400
Is that good to know?

88
00:07:12,400 --> 00:07:16,830
Moron. Pressure points are important.

89
00:07:16,830 --> 00:07:20,530
It might just be enough.

90
00:07:22,230 --> 00:07:26,130
Where did you learn it?

91
00:07:26,130 --> 00:07:32,460
From the masters at Unbong Temple.

92
00:07:32,460 --> 00:07:35,760
Is that a fact?

93
00:07:35,760 --> 00:07:41,530
All right. You can show me.

94
00:07:41,530 --> 00:07:43,160
This way.

95
00:07:43,160 --> 00:07:47,500
- Right.
- Good good.

96
00:07:47,500 --> 00:07:49,530
Excellent.

97
00:07:54,700 --> 00:07:58,730
Use my body as you see fit.

98
00:07:58,730 --> 00:08:00,960
If you please.

99
00:08:00,960 --> 00:08:02,360
Oh, right.

100
00:08:04,530 --> 00:08:09,030
Your coloring indicates poor blood flow.

101
00:08:09,030 --> 00:08:11,100
I'll take care of that.

102
00:08:11,100 --> 00:08:14,530
If I may?

103
00:08:14,530 --> 00:08:16,760
- First lower your head.
- And my arms.

104
00:08:16,760 --> 00:08:22,530
Then twist.
Now lower your head again.

105
00:08:31,530 --> 00:08:32,960
And again.

106
00:08:39,530 --> 00:08:42,330
And once more.

107
00:08:44,100 --> 00:08:47,530
Let go!

108
00:08:49,300 --> 00:08:51,300
Get off!

109
00:08:55,530 --> 00:09:03,530
A doctor? Doctor THIS!

110
00:09:06,960 --> 00:09:12,530
Nomination? Dream on!

111
00:09:15,830 --> 00:09:17,330
Wait...

112
00:09:17,330 --> 00:09:21,360
Hong-choon, my arm is broke...

113
00:09:21,360 --> 00:09:22,830
Wait.

114
00:09:32,530 --> 00:09:39,460
You're a stranger to me!
Never call me Father again!

115
00:09:39,580 --> 00:09:43,100
Not all poisons are harmful.

116
00:09:43,100 --> 00:09:46,530
Nor are all herbs beneficial.

117
00:09:46,530 --> 00:09:50,530
Used correctly, poisons can cure.

118
00:09:50,530 --> 00:09:55,230
Morning glory seeds are
not toxic in themselves.

119
00:09:55,230 --> 00:09:58,930
But they can induce abortion.

120
00:09:58,930 --> 00:10:02,400
Correct. Very well said.

121
00:10:02,400 --> 00:10:08,530
So the Pisang Doctor Heo used
may actually help the Prince?

122
00:10:08,530 --> 00:10:10,130
How did you hear about that?

123
00:10:10,130 --> 00:10:13,530
From the herbalists.

124
00:10:14,530 --> 00:10:17,530
It's the nature of the palace.

125
00:10:17,530 --> 00:10:20,200
He's not getting better,
if it's the Pisang...

126
00:10:20,200 --> 00:10:21,660
That's enough.

127
00:10:21,660 --> 00:10:24,600
Watch your mouth.

128
00:10:24,600 --> 00:10:26,530
Now listen to me.

129
00:10:26,530 --> 00:10:31,530
Not another word about the Prince's
deterioration. Understood?

130
00:10:31,530 --> 00:10:33,530
Ma'am.

131
00:10:50,200 --> 00:10:52,530
How is he?

132
00:10:53,730 --> 00:10:56,600
It's too early to tell.

133
00:10:56,600 --> 00:11:01,700
This was unwise.

134
00:11:01,700 --> 00:11:04,230
It will be fine.

135
00:11:04,230 --> 00:11:07,160
Just tend to your duties.

136
00:11:07,160 --> 00:11:09,530
Doctor.

137
00:11:16,200 --> 00:11:17,060
Doctor.

138
00:11:17,060 --> 00:11:18,530
Yes?

139
00:11:20,530 --> 00:11:24,230
Minister Jeong wants a word.

140
00:11:29,530 --> 00:11:31,830
What was that?

141
00:11:31,830 --> 00:11:35,530
You know exactly what.

142
00:11:35,530 --> 00:11:41,530
We know why you put
Pisang in his medicine.

143
00:11:41,530 --> 00:11:47,400
If this works out,
we'll take care of you, don't you worry.

144
00:11:47,400 --> 00:11:50,530
What are you saying?

145
00:11:53,300 --> 00:11:57,530
You call yourselves loyal ministers?

146
00:11:58,760 --> 00:12:03,130
The Pisang isn't poison, it's medicine.

147
00:12:05,160 --> 00:12:13,330
I'll help him, whatever it takes.

148
00:12:44,530 --> 00:12:50,900
If he does recover,
we're back where we started.

149
00:12:52,530 --> 00:12:53,530
My lord.

150
00:12:53,530 --> 00:12:57,630
There's one chance left.

151
00:12:57,630 --> 00:13:02,530
Doctor Heo is still giving him the Pisang.

152
00:13:02,530 --> 00:13:11,130
If we add a little more, it won't be
medicine, it will be poison.

153
00:13:12,530 --> 00:13:17,660
The Crown Prince will die of poison.

154
00:13:17,660 --> 00:13:24,200
And Doctor Heo will pay for it.

155
00:13:41,000 --> 00:13:42,530
Your Highness.

156
00:13:46,730 --> 00:13:49,660
Look after my boys.

157
00:13:49,660 --> 00:13:54,530
Not as their doctor.

158
00:13:54,530 --> 00:14:01,000
But as a friend, a guardian.

159
00:14:15,530 --> 00:14:16,800
Your Highness.

160
00:14:32,160 --> 00:14:35,530
Stay with us.

161
00:14:38,060 --> 00:14:48,530
I know how the Pisang has
increased the pain.

162
00:14:50,530 --> 00:14:55,530
But you can beat it.

163
00:14:55,530 --> 00:15:00,560
Beat it and get better.

164
00:15:06,530 --> 00:15:14,530
You trusted me, believed in me.

165
00:15:14,530 --> 00:15:18,000
And I believe in you.

166
00:15:20,530 --> 00:15:26,060
I know you can fight the pain...

167
00:15:26,060 --> 00:15:29,200
...and beat this.

168
00:15:34,530 --> 00:15:36,560
Your Highness.

169
00:15:36,560 --> 00:15:42,330
Just a little longer.

170
00:15:45,300 --> 00:15:48,530
A little longer.

171
00:15:52,530 --> 00:15:54,700
Your Highness.

172
00:16:12,130 --> 00:16:15,530
Is he improving?

173
00:16:15,530 --> 00:16:19,530
We'll know in a day or two.

174
00:16:19,530 --> 00:16:26,200
Something has come up
and I have to go.

175
00:16:26,200 --> 00:16:27,660
I'm sorry.

176
00:16:27,660 --> 00:16:31,200
No. You go on.

177
00:16:31,200 --> 00:16:33,160
Yes, sir.

178
00:17:21,960 --> 00:17:23,530
Who are you?

179
00:17:32,530 --> 00:17:33,860
What is this?

180
00:17:33,860 --> 00:17:36,530
Come along quietly.

181
00:17:38,600 --> 00:17:42,530
Why...? Let go...!

182
00:18:27,530 --> 00:18:29,960
Awake?

183
00:18:34,400 --> 00:18:37,200
Where am I?

184
00:18:37,200 --> 00:18:39,160
Why am I here?

185
00:18:39,160 --> 00:18:41,530
I'll keep it short.

186
00:18:41,530 --> 00:18:44,530
We have your parents.

187
00:18:48,530 --> 00:18:53,530
Do as we say or they're dead.

188
00:18:53,530 --> 00:18:56,530
What do you want?

189
00:19:19,930 --> 00:19:23,000
Sir, it's me.

190
00:19:23,000 --> 00:19:25,830
Come in.

191
00:19:37,200 --> 00:19:40,930
- Well?
- We're ready.

192
00:19:45,930 --> 00:19:48,530
Ma'am, it's Dol-swe.

193
00:19:48,530 --> 00:19:51,700
- Come in.
- Yes'm.

194
00:19:56,530 --> 00:19:58,530
Well? Did you find out?

195
00:19:58,530 --> 00:20:04,500
Yes'm. His Highness
still hasn't recovered.

196
00:20:04,500 --> 00:20:05,530
And Jun?

197
00:20:05,530 --> 00:20:10,960
He's spent three nights by his side.

198
00:20:10,960 --> 00:20:14,530
He's under such a strain.

199
00:20:14,530 --> 00:20:20,330
It's all anyone is talking about.

200
00:20:20,330 --> 00:20:24,600
Using Pisang as medicine...?

201
00:20:26,530 --> 00:20:30,530
Try not to worry.
He knows what he's doing.

202
00:20:30,530 --> 00:20:33,600
Quite right, quite right.

203
00:21:41,530 --> 00:21:43,530
Ma'am.

204
00:21:45,530 --> 00:21:49,530
Are you ill?

205
00:21:49,530 --> 00:21:51,060
No.

206
00:21:51,060 --> 00:21:54,960
Your color is off.

207
00:21:54,960 --> 00:21:57,530
It's nothing.

208
00:22:22,530 --> 00:22:23,530
Well?

209
00:22:26,960 --> 00:22:31,530
Did you add more Pisang?

210
00:22:33,530 --> 00:22:36,030
Did you?

211
00:22:41,530 --> 00:22:43,530
I couldn't.

212
00:22:43,530 --> 00:22:45,530
What?

213
00:22:45,530 --> 00:22:49,060
Keep this up and I'll go to the police.

214
00:22:49,060 --> 00:22:54,900
They're your parents. Suit yourself.

215
00:22:58,560 --> 00:23:02,530
Time is short. Now do it.

216
00:24:07,100 --> 00:24:09,530
All done.

217
00:24:09,530 --> 00:24:11,100
Thank you.

218
00:24:11,100 --> 00:24:14,930
Doctor Yu will be here in a minute.

219
00:24:18,860 --> 00:24:22,660
Oops, um, excuse me.

220
00:24:22,660 --> 00:24:25,530
Of course.

221
00:24:25,530 --> 00:24:27,530
Thanks.

222
00:24:27,530 --> 00:24:31,530
Ugh, my tummy...

223
00:25:15,530 --> 00:25:19,160
Did she add more Pisang?

224
00:25:19,160 --> 00:25:22,530
Not that I saw, sir.

225
00:25:28,500 --> 00:25:33,960
Well then. You'll have to do it.

226
00:25:33,960 --> 00:25:36,130
M-my lord.

227
00:25:36,130 --> 00:25:38,530
I can't get near it.

228
00:25:38,530 --> 00:25:41,060
Silence.

229
00:25:41,060 --> 00:25:47,000
Do it or die. Whatever it takes.

230
00:26:28,010 --> 00:26:30,270
Where's the pharmacist?

231
00:26:30,270 --> 00:26:36,170
He hasn't been home in days.
I'm helping out.

232
00:26:37,940 --> 00:26:40,330
Is it ready?

233
00:26:40,330 --> 00:26:42,360
Yes, Doctor.

234
00:27:57,530 --> 00:28:00,330
The Crown Prince's medicine?

235
00:28:00,330 --> 00:28:03,530
Doctor Yu took it.

236
00:29:03,530 --> 00:29:06,600
Your Highness.

237
00:29:06,600 --> 00:29:09,530
You're awake?

238
00:29:26,530 --> 00:29:28,530
How am I?

239
00:29:28,530 --> 00:29:31,530
The fever broke.

240
00:29:34,130 --> 00:29:42,530
Like you said.
The medicine did the trick.

241
00:29:45,430 --> 00:29:47,500
Your Highness.

242
00:30:14,530 --> 00:30:16,530
What is it?

243
00:30:19,100 --> 00:30:23,530
His Highness is awake.

244
00:30:41,530 --> 00:30:42,830
Doctor.

245
00:30:44,530 --> 00:30:47,300
I heard he was awake.

246
00:30:47,300 --> 00:30:52,360
Yes. He's resting now.

247
00:30:52,360 --> 00:30:54,700
How is he?

248
00:30:54,700 --> 00:30:57,700
The malaria broke.

249
00:31:00,530 --> 00:31:05,260
You've done well. Thank you.

250
00:31:13,530 --> 00:31:16,530
And his medicine?

251
00:31:21,530 --> 00:31:24,200
Do we keep administering it?

252
00:31:24,200 --> 00:31:26,960
Just to be sure?

253
00:31:49,530 --> 00:31:53,200
Your recovery may be difficult.

254
00:31:53,200 --> 00:31:54,600
His Majesty is ill.

255
00:31:54,600 --> 00:31:57,530
If the King dies, you may die with him.

256
00:31:57,530 --> 00:31:59,530
How dare you?

257
00:31:59,530 --> 00:32:01,430
I said move.

258
00:32:01,430 --> 00:32:03,200
My lord!

