1
00:02:22,575 --> 00:02:24,600
Peter.

2
00:02:27,614 --> 00:02:30,482
You could not watch even one hour with me?

3
00:02:31,151 --> 00:02:33,551
Master, what has happened to you?

4
00:02:33,654 --> 00:02:35,178
Should I call the others, Lord?

5
00:02:35,522 --> 00:02:39,652
No, John. I don't want them to see me like this.

6
00:02:39,994 --> 00:02:42,554
Are you in danger? Should we flee, Master?

7
00:02:43,164 --> 00:02:46,065
Stay here. Watch...

8
00:02:47,569 --> 00:02:48,695
pray.

9
00:03:03,786 --> 00:03:05,913
What is wrong with him?

10
00:03:06,789 --> 00:03:08,848
He seems afraid.

11
00:03:09,292 --> 00:03:12,785
He had spoken about danger while we ate...

12
00:03:13,363 --> 00:03:16,298
He mentioned betrayal and...

13
00:03:37,323 --> 00:03:38,381
Thirty.

14
00:03:38,957 --> 00:03:40,822
Thirty, Judas.

15
00:03:41,327 --> 00:03:45,730
That was the agreement between me... and you?

16
00:03:46,365 --> 00:03:47,457
Yes.

17
00:04:32,281 --> 00:04:34,146
Where?

18
00:04:35,451 --> 00:04:37,715
Where is he?

19
00:05:11,690 --> 00:05:14,750
Hear me, Father.

20
00:05:16,929 --> 00:05:21,832
Rise up, defend me.

21
00:05:26,773 --> 00:05:31,574
Save me from the traps they set for me.

22
00:05:52,835 --> 00:05:55,804
Do you really believe...

23
00:05:56,205 --> 00:06:00,471
that one man can bear...

24
00:06:00,576 --> 00:06:02,942
the full burden of sin?

25
00:06:03,612 --> 00:06:05,911
Shelter me, O Lord.

26
00:06:08,051 --> 00:06:09,780
I trust in You.

27
00:06:13,056 --> 00:06:16,355
In You I take refuge.

28
00:06:20,231 --> 00:06:24,691
No one man can carry this burden...

29
00:06:26,070 --> 00:06:27,594
I tell you.

30
00:06:28,506 --> 00:06:30,941
It is far too heavy.

31
00:06:31,443 --> 00:06:35,345
Saving their souls is too costly.

32
00:06:36,314 --> 00:06:39,715
No one. Ever.

33
00:06:40,986 --> 00:06:42,248
No.

34
00:06:43,788 --> 00:06:44,915
Never.

35
00:06:47,093 --> 00:06:51,427
Father, you can do all things.

36
00:06:54,567 --> 00:06:59,062
If it is possible, let this chalice pass from me...

37
00:07:02,676 --> 00:07:04,803
But let Your will be done...

38
00:07:07,648 --> 00:07:09,582
not mine.

39
00:07:37,446 --> 00:07:39,039
Who is your father?

40
00:07:45,855 --> 00:07:47,720
Who are you?

41
00:09:47,019 --> 00:09:48,350
Who are you looking for?

42
00:09:53,693 --> 00:09:56,161
We're looking for Jesus of Nazareth.

43
00:10:01,334 --> 00:10:02,358
I am he.

44
00:10:30,899 --> 00:10:32,561
Hail, Rabbi!

45
00:10:45,749 --> 00:10:47,148
Judas...

46
00:10:48,085 --> 00:10:52,988
you betray the Son of Man with a kiss?

47
00:12:53,219 --> 00:12:54,379
Peter!

48
00:12:57,056 --> 00:12:58,080
Put it down!

49
00:12:59,091 --> 00:13:04,052
Those who live by the sword shall die by the sword.

50
00:13:13,106 --> 00:13:14,438
Put it down.

51
00:13:48,645 --> 00:13:51,170
Malchus! Get up!

52
00:13:51,681 --> 00:13:53,479
We've got him. Let's go!

53
00:14:13,538 --> 00:14:15,802
What, Mary? What is it?

54
00:14:21,414 --> 00:14:22,381
Listen...

55
00:14:24,984 --> 00:14:29,944
"Why is this night different from every other night?"

56
00:14:33,994 --> 00:14:37,293
"Because once we were slaves...

57
00:14:37,664 --> 00:14:40,292
and we are slaves no longer..."

58
00:14:50,945 --> 00:14:51,969
They've seized him!

59
00:16:22,477 --> 00:16:26,379
Everyone you can, hear? To the courtyard of the High Priest.

60
00:16:27,315 --> 00:16:28,748
Quick! Go!

61
00:17:59,348 --> 00:18:01,043
Halt! Not so fast.

62
00:18:02,150 --> 00:18:04,641
This is not your party, you toothless vermin.

63
00:18:11,728 --> 00:18:12,888
Peter...

64
00:18:24,074 --> 00:18:28,067
Hey! What's going on here?

65
00:18:28,178 --> 00:18:29,941
In there! Stop them!

66
00:18:30,547 --> 00:18:32,071
They've arrested him!

67
00:18:32,583 --> 00:18:35,348
In secret! In the night!

68
00:18:35,720 --> 00:18:38,018
To hide their crime from you!

69
00:18:38,390 --> 00:18:39,482
Stop them!

70
00:18:39,891 --> 00:18:42,325
What are you yelling about, woman?

71
00:18:42,761 --> 00:18:44,023
Who have they arrested?

72
00:18:45,297 --> 00:18:46,491
Jesus.

73
00:18:46,898 --> 00:18:48,263
Jesus of Nazareth!

74
00:18:48,466 --> 00:18:49,764
Shut up!

75
00:18:57,276 --> 00:18:58,243
She's crazy.

76
00:18:59,045 --> 00:19:04,211
A criminal. Brought in for questioning, that's all.

77
00:19:05,485 --> 00:19:07,385
Broke the temple laws.

78
00:19:16,431 --> 00:19:18,331
I see.

79
00:19:19,367 --> 00:19:23,235
Better go tell him there's more trouble brewing.

80
00:19:23,771 --> 00:19:24,738
Tell who?

81
00:19:26,207 --> 00:19:27,765
Abenader, you idiot.

82
00:19:28,576 --> 00:19:29,908
Get going!

83
00:19:31,980 --> 00:19:32,844
Go!

84
00:20:16,194 --> 00:20:17,388
Jesus...

85
00:20:34,947 --> 00:20:36,074
Are you hungry?

86
00:20:39,052 --> 00:20:40,519
Yes, I am.

87
00:20:48,762 --> 00:20:51,755
This is certainly a tall table!

88
00:20:53,101 --> 00:20:54,068
Who is it for?

89
00:20:54,602 --> 00:20:55,899
A rich man.

90
00:20:58,506 --> 00:21:01,532
Does he like to eat standing up?

91
00:21:03,379 --> 00:21:06,906
No. He prefers to eat like... so.

92
00:21:08,350 --> 00:21:11,319
Tall table, tall chairs!

93
00:21:12,021 --> 00:21:14,512
Well, I haven't made them yet.

94
00:21:30,741 --> 00:21:32,675
This will never catch on!

95
00:21:33,711 --> 00:21:35,269
Oh, no you don't!

96
00:21:35,646 --> 00:21:39,013
Take off that dirty apron before you come in.

97
00:21:42,153 --> 00:21:44,213
And wash your hands.

98
00:22:11,352 --> 00:22:13,684
It has begun, Lord.

99
00:22:17,725 --> 00:22:19,420
So be it.

100
00:22:50,327 --> 00:22:51,794
Sir, there's trouble at...

101
00:22:51,895 --> 00:22:54,295
What, in the middle of the night, Abenader?

102
00:22:54,498 --> 00:22:55,624
I'm sorry.

103
00:22:55,766 --> 00:22:56,698
What's the problem?

104
00:22:57,568 --> 00:22:59,195
Trouble within the walls.

105
00:22:59,603 --> 00:23:02,766
Caiphas had some prophet arrested.

106
00:23:03,073 --> 00:23:03,768
Who?

107
00:23:04,976 --> 00:23:06,603
Some Galilean.

108
00:23:07,045 --> 00:23:09,639
The Pharisees apparently hate the man.

109
00:23:09,747 --> 00:23:12,511
A Galilean? Who are you talking about?

110
00:23:13,751 --> 00:23:16,515
Who is this beggar you bring to us...

111
00:23:17,423 --> 00:23:20,324
chained up like a condemned man?

112
00:23:20,993 --> 00:23:24,451
He's Jesus, the Nazarene troublemaker.

113
00:23:26,565 --> 00:23:28,294
You're Jesus of Nazareth?

114
00:23:29,902 --> 00:23:32,895
They say you're a king.

115
00:23:33,406 --> 00:23:34,964
Where is this kingdom of yours?

116
00:23:35,542 --> 00:23:38,477
What line of kings do you descend from?

117
00:23:38,745 --> 00:23:40,076
Speak up!

118
00:23:41,648 --> 00:23:45,107
You're just the son of some obscure carpenter, no?

119
00:23:47,054 --> 00:23:49,352
Some say you're Elijah.

120
00:23:50,224 --> 00:23:52,920
But he was carried off to heaven in a chariot!

121
00:23:54,695 --> 00:23:57,027
Why don't you say something?

122
00:23:57,766 --> 00:24:02,294
You've been brought here as a blasphemer!

123
00:24:03,739 --> 00:24:05,934
What do you say to that?

124
00:24:06,341 --> 00:24:08,206
Defend yourself.

125
00:24:13,883 --> 00:24:16,545
I have spoken openly to everyone.

126
00:24:16,986 --> 00:24:18,283
I've taught...

127
00:24:19,055 --> 00:24:23,958
in the temple where we all gather.

128
00:24:24,728 --> 00:24:27,526
Ask those who have heard what I have to say.

129
00:24:28,365 --> 00:24:31,823
Is that how you address the high priest?

130
00:24:32,736 --> 00:24:34,033
With arrogance?

131
00:24:48,453 --> 00:24:50,318
If I have spoken evil...

132
00:24:51,057 --> 00:24:53,423
tell me what evil I have said.

133
00:24:54,894 --> 00:24:58,557
But if not, why do you hit me?

134
00:25:01,300 --> 00:25:03,962
Yes, we'll listen to those who heard your blasphemies.

135
00:25:05,138 --> 00:25:06,105
Good!

136
00:25:08,909 --> 00:25:10,774
Let's hear them.

137
00:25:11,411 --> 00:25:14,346
He cures the sick by magic!

138
00:25:15,515 --> 00:25:17,746
With the help of devils!

139
00:25:18,953 --> 00:25:20,580
I've seen it.

140
00:25:21,689 --> 00:25:26,649
He casts out devils, with the help of devils.

141
00:25:31,667 --> 00:25:36,331
He calls himself the king of the Jews!

142
00:25:38,507 --> 00:25:41,533
No, he calls himself the son of God!

143
00:25:42,044 --> 00:25:45,139
He said he would destroy the temple...

144
00:25:46,049 --> 00:25:49,109
and rebuild it in three days!

145
00:25:50,920 --> 00:25:52,820
Worse!

146
00:25:53,523 --> 00:25:56,390
He claims he's the bread of life!

147
00:25:56,893 --> 00:26:01,627
And if we don't eat his flesh or drink his blood...

148
00:26:01,999 --> 00:26:04,559
we won't inherit eternal life.

149
00:26:05,536 --> 00:26:07,265
Silence!

150
00:26:08,372 --> 00:26:11,934
You're all under this man's spell.

151
00:26:12,477 --> 00:26:16,709
Either offer proof of his wrongdoing...

152
00:26:19,317 --> 00:26:21,114
or be quiet!

153
00:26:21,653 --> 00:26:25,818
This entire proceeding is an outrage.

154
00:26:26,492 --> 00:26:30,861
All I've heard from these witnesses is mindless contradiction!

155
00:26:42,843 --> 00:26:45,038
Who called this meeting anyway?

156
00:26:45,546 --> 00:26:47,343
And at this hour of the night?

157
00:26:47,648 --> 00:26:51,277
Where are the other members of the council?

158
00:26:51,820 --> 00:26:53,014
Get him out of here!

159
00:26:56,524 --> 00:26:57,548
Get out!

160
00:26:58,159 --> 00:27:01,890
A travesty! That's what this is, a beastly travesty!

161
00:27:10,139 --> 00:27:12,573
Haven't you anything to say?

162
00:27:13,342 --> 00:27:15,003
No answer to these accusations?

163
00:27:17,813 --> 00:27:21,807
I ask you now...

164
00:27:22,786 --> 00:27:24,811
Jesus of Nazareth...

165
00:27:26,022 --> 00:27:30,254
Tell us, are you the Messiah?

166
00:27:31,929 --> 00:27:35,831
The son of the living God?

167
00:27:47,613 --> 00:27:49,410
I AM...

168
00:27:50,482 --> 00:27:53,576
and you will see the Son of Man seated at the right hand of power...

169
00:27:54,987 --> 00:27:58,081
and coming on the clouds of heaven.

170
00:28:02,528 --> 00:28:04,689
Blasphemy!

171
00:28:06,799 --> 00:28:08,767
You heard him.

172
00:28:09,135 --> 00:28:12,196
There's no need for witnesses!

173
00:28:13,507 --> 00:28:18,467
Your verdict. What is your verdict?

174
00:28:20,648 --> 00:28:22,707
Death!

175
00:29:15,506 --> 00:29:18,771
Haven't I seen you in the company of the Galilean?

176
00:29:19,244 --> 00:29:21,644
Yes! You're one of his disciples!

177
00:29:22,348 --> 00:29:24,339
I recognize you!

178
00:29:24,750 --> 00:29:27,719
Quiet! I've never met the man. I don't know him.

179
00:29:28,487 --> 00:29:31,354
You are Peter! One of the disciples of Jesus.

180
00:29:36,196 --> 00:29:39,290
I don't know the man! You are wrong!

181
00:29:40,367 --> 00:29:41,493
Stop! Stop!

182
00:29:41,868 --> 00:29:43,165
I've seen you before!

183
00:29:43,603 --> 00:29:45,901
Stop him! He's one of them!

184
00:29:48,075 --> 00:29:50,703
You're wrong, damn you!

185
00:29:51,579 --> 00:29:54,480
I swear I don't know the man.

186
00:29:55,383 --> 00:29:58,682
I've never seen him before.

187
00:30:06,795 --> 00:30:09,263
Wherever you go, Lord...

188
00:30:09,965 --> 00:30:13,663
I will follow.

189
00:30:16,339 --> 00:30:20,799
To prison, even to death.

190
00:30:24,847 --> 00:30:27,316
Amen, I say to you...

191
00:30:29,920 --> 00:30:32,889
before the cock crows...

192
00:30:35,526 --> 00:30:38,461
three times you will deny me.

193
00:31:02,655 --> 00:31:03,986
Peter?

194
00:31:19,841 --> 00:31:22,366
No, no... I am unworthy!

195
00:31:23,478 --> 00:31:26,970
I have denied him, Mother!

196
00:31:28,082 --> 00:31:31,540
Denied him three times.

197
00:31:43,966 --> 00:31:47,835
Let's go! You take care of it.

198
00:31:51,674 --> 00:31:54,871
Let him pass. He's harmless.

199
00:31:56,880 --> 00:31:58,677
Release him!

200
00:32:00,417 --> 00:32:02,317
Take back the silver.

201
00:32:02,753 --> 00:32:04,186
Here!

202
00:32:06,557 --> 00:32:10,152
I've sinned, betrayed innocent blood.

203
00:32:12,830 --> 00:32:17,200
Take back your silver. I don't want it!

204
00:32:17,736 --> 00:32:22,696
If you think you've betrayed innocent blood, that's your affair.

205
00:32:23,942 --> 00:32:25,910
Take your money and go.

206
00:32:27,747 --> 00:32:28,736
Now go!

207
00:32:55,710 --> 00:32:57,405
What's wrong? Are you all right?

208
00:32:58,246 --> 00:33:00,271
Look at his mouth. Hey, can we look at it?

209
00:33:00,648 --> 00:33:02,809
You need help? Can we help?

210
00:33:03,885 --> 00:33:05,546
He's bleeding! Look! Blood!

211
00:33:08,424 --> 00:33:11,552
Leave me alone...

212
00:33:12,561 --> 00:33:13,721
you little satans!

213
00:33:14,863 --> 00:33:18,128
Aha! Cursing! Are you cursed?

214
00:33:18,400 --> 00:33:20,061
He's cursed!

215
00:33:20,369 --> 00:33:22,304
Yes, a curse! It's inside him, look!

216
00:33:34,885 --> 00:33:37,945
Watch out, it's like burning oil from his bones!

217
00:33:42,693 --> 00:33:46,493
Get away from me! Leave me alone!

218
00:38:04,909 --> 00:38:08,242
Don't condemn this Galilean.

219
00:38:08,579 --> 00:38:09,944
He's holy.

220
00:38:10,614 --> 00:38:12,775
You will only bring trouble on yourself.

221
00:38:13,117 --> 00:38:16,110
Do you want to know my idea of trouble, Claudia?

222
00:38:16,521 --> 00:38:20,753
This stinking outpost, that filthy rabble out there.

223
00:39:04,606 --> 00:39:07,473
Do you always punish your prisoners before they're judged?

224
00:39:07,842 --> 00:39:08,604
Governor...

225
00:39:08,910 --> 00:39:11,471
What accusations do you bring against this man?

226
00:39:13,282 --> 00:39:14,544
Well...

227
00:39:15,284 --> 00:39:19,220
If he weren't a malefactor, we wouldn't have brought him before you.

228
00:39:19,655 --> 00:39:21,213
That's not what I asked.

229
00:39:21,824 --> 00:39:24,157
Why don't you judge him according to your own laws?

230
00:39:24,494 --> 00:39:28,225
Consul, you know it's unlawful for us...

231
00:39:28,765 --> 00:39:31,996
to condemn any man to death.

232
00:39:32,802 --> 00:39:33,826
To death?

233
00:39:34,371 --> 00:39:37,671
What has this man done to merit such a penalty?

234
00:39:37,942 --> 00:39:39,705
He has violated our Sabbath, Consul.

235
00:39:42,313 --> 00:39:43,337
Go on...

236
00:39:43,447 --> 00:39:45,278
He has seduced the people...

237
00:39:45,650 --> 00:39:49,086
taught foul, disgusting doctrines.

238
00:39:51,256 --> 00:39:54,589
Isn't he the prophet you welcomed into Jerusalem only five days ago?

239
00:39:55,327 --> 00:39:57,056
And now you want him dead?

240
00:39:57,796 --> 00:39:59,787
Can any of you explain this madness to me?

241
00:40:09,509 --> 00:40:10,976
Your Excellency, please...

242
00:40:12,845 --> 00:40:17,909
So far the high priest hasn't told you this man's greatest crime.

243
00:40:18,919 --> 00:40:22,548
He has become the leader of a large and dangerous sect...

244
00:40:22,923 --> 00:40:25,721
who hail him as the Son of David!

245
00:40:27,194 --> 00:40:31,325
He claims that he is the Messiah...

246
00:40:31,800 --> 00:40:34,428
the king promised to the Jews.

247
00:40:35,737 --> 00:40:40,697
He has forbidden his followers to pay tribute to the emperor, Consul!

248
00:40:47,717 --> 00:40:49,184
Bring him here!

249
00:41:21,319 --> 00:41:22,217
Go!

250
00:41:28,360 --> 00:41:29,292
Drink.

251
00:41:42,776 --> 00:41:45,745
Are you the king of the Jews?

252
00:41:48,248 --> 00:41:50,877
Does this question come from you?

253
00:41:51,385 --> 00:41:56,345
Or do you ask me this because others have told you that is what I am?

254
00:41:58,092 --> 00:42:01,118
Why would I ask you that?

255
00:42:01,896 --> 00:42:03,386
Am I a Jew?

256
00:42:04,132 --> 00:42:09,092
Your high priests, your own people delivered you up to me.

257
00:42:09,771 --> 00:42:11,432
They want me to have you executed.

258
00:42:11,807 --> 00:42:13,968
Why? What have you done?

259
00:42:16,978 --> 00:42:18,776
Are you a king?

260
00:42:22,852 --> 00:42:25,650
My kingdom is not of this world.

261
00:42:26,088 --> 00:42:27,715
If it were...

262
00:42:27,990 --> 00:42:32,792
do you think my followers would have let them hand me over?

263
00:42:34,264 --> 00:42:35,663
Then you are a king?

264
00:42:38,302 --> 00:42:40,896
That is why I was born.

265
00:42:41,805 --> 00:42:44,604
To give testimony to the truth.

266
00:42:45,376 --> 00:42:50,336
All men who hear the truth hear my voice.

267
00:42:52,317 --> 00:42:53,784
Truth!

268
00:42:58,157 --> 00:43:00,352
What is truth?

269
00:43:26,420 --> 00:43:30,379
I have questioned this prisoner, and I find no cause in him.

270
00:43:41,303 --> 00:43:42,861
This man is a Galilean, is he not?

271
00:43:44,172 --> 00:43:45,161
He is.

272
00:43:45,574 --> 00:43:47,838
Then he is King Herod's subject.

273
00:43:48,877 --> 00:43:50,469
Let Herod judge him.

274
00:43:50,812 --> 00:43:51,438
Governor...

275
00:43:51,748 --> 00:43:52,806
Hand him over.

276
00:44:16,507 --> 00:44:18,203
Jesus of Nazareth!

277
00:44:18,843 --> 00:44:19,810
Where?

278
00:44:25,250 --> 00:44:26,512
Where is he?

279
00:44:33,759 --> 00:44:35,283
This...

280
00:44:36,829 --> 00:44:38,262
is Jesus of Nazareth?

281
00:44:44,637 --> 00:44:47,539
Is it true that you restore sight to the blind?

282
00:44:51,278 --> 00:44:53,041
Raise men from the dead?

283
00:45:10,064 --> 00:45:14,229
Where do you get your power?

284
00:45:20,376 --> 00:45:25,337
Are you the one whose birth was foretold?

285
00:45:32,055 --> 00:45:33,886
Answer me!

286
00:45:34,858 --> 00:45:36,291
Are you a king?

287
00:45:41,465 --> 00:45:43,092
How about me?

288
00:45:45,136 --> 00:45:47,832
Will you work a little miracle for me?

289
00:46:04,189 --> 00:46:07,819
Take this stupid fool out of my sight.

290
00:46:08,327 --> 00:46:11,353
He's not guilty of a crime, he's just crazy.

291
00:46:13,332 --> 00:46:16,597
Give him a fool's homage...

292
00:46:39,194 --> 00:46:41,025
What is truth, Claudia?

293
00:46:41,696 --> 00:46:46,464
Do you hear it, recognize it when it is spoken?

294
00:46:47,236 --> 00:46:49,431
Yes, I do.

295
00:46:51,640 --> 00:46:52,868
Don't you?

296
00:46:53,175 --> 00:46:54,802
How?

297
00:46:55,544 --> 00:46:57,478
Can you tell me?

298
00:47:06,723 --> 00:47:11,558
If you will not hear the truth, no one can tell you.

299
00:47:13,364 --> 00:47:15,127
Truth...

300
00:47:16,567 --> 00:47:19,798
Do you want to know what my truth is, Claudia?

301
00:47:20,938 --> 00:47:25,876
I've been putting down rebellions in this rotten outpost for eleven years.

302
00:47:26,678 --> 00:47:28,646
If I don't condemn this man...

303
00:47:29,014 --> 00:47:32,814
I know Caiphas will start a rebellion.

304
00:47:33,619 --> 00:47:37,055
If I do condemn him, then his followers may.

305
00:47:38,056 --> 00:47:39,854
Either way, there will be bloodshed.

306
00:47:40,393 --> 00:47:43,692
Caesar has warned me, Claudia. Warned me twice.

307
00:47:44,597 --> 00:47:48,260
He swore that the next time the blood would be mine.

308
00:47:49,035 --> 00:47:50,696
That is my truth!

309
00:47:55,609 --> 00:48:00,569
Herod refuses to condemn the man. They're bringing him back here.

310
00:48:01,215 --> 00:48:03,740
We're going to need reinforcements.

311
00:48:04,084 --> 00:48:06,349
I don't want to cause an uprising.

312
00:48:07,889 --> 00:48:09,857
There is already an uprising.

313
00:48:35,519 --> 00:48:39,148
King Herod found no cause in this man.

314
00:48:41,391 --> 00:48:44,155
Neither do I.

315
00:48:50,368 --> 00:48:53,531
Men! Attend to the crowd!

316
00:49:06,385 --> 00:49:07,750
Quiet!

317
00:49:08,220 --> 00:49:11,747
Have you no respect for our Roman procurator?

318
00:49:19,099 --> 00:49:23,433
As you know, every year I release a criminal back to you.

319
00:49:24,404 --> 00:49:27,568
We are now holding a notorious murderer...

320
00:49:28,643 --> 00:49:30,076
Barabbas.

321
00:49:46,862 --> 00:49:50,059
Which of the two men would you have me release to you?

322
00:49:50,365 --> 00:49:52,265
The murderer Barabbas?

323
00:49:52,534 --> 00:49:54,435
Or Jesus, called the Messiah?

324
00:49:55,038 --> 00:49:59,236
He is not the Messiah! He is an imposter! A blasphemer!

325
00:50:00,410 --> 00:50:02,241
Free Barabbas!

326
00:50:18,762 --> 00:50:23,723
Again I ask you: Which of these two men should I release to you?

327
00:50:24,302 --> 00:50:26,600
Free Barabbas!

328
00:50:42,555 --> 00:50:44,182
Free him.

329
00:51:18,327 --> 00:51:21,763
What would you have me do with Jesus the Nazarene?

330
00:51:22,831 --> 00:51:24,662
Have him crucified!

331
00:51:40,417 --> 00:51:41,442
No!

332
00:51:41,919 --> 00:51:43,546
I will chastise him...

333
00:51:44,321 --> 00:51:45,686
but then I will set him free.

334
00:51:50,428 --> 00:51:53,192
See to it that the punishment is severe, Abenader.

335
00:51:53,764 --> 00:51:56,928
But don't let them kill the man.

336
00:53:18,723 --> 00:53:20,816
My heart is ready, Father...

337
00:53:21,392 --> 00:53:23,360
my heart is ready.

338
00:57:47,344 --> 00:57:49,369
My son...

339
00:57:51,048 --> 00:57:55,508
when, where, how...

340
00:57:57,689 --> 00:58:01,420
will you choose to be delivered of this?

341
01:02:23,073 --> 01:02:25,406
If the world hates you...

342
01:02:26,277 --> 01:02:31,237
remember that it has hated me first.

343
01:02:36,988 --> 01:02:40,823
Remember also that no servant is greater than his master.

344
01:02:42,261 --> 01:02:47,358
If they persecuted me, they will persecute you.

345
01:02:49,335 --> 01:02:51,098
You must not be afraid.

346
01:02:51,938 --> 01:02:54,634
The Helper will come...

347
01:02:55,876 --> 01:03:00,142
who reveals the truth about God...

348
01:03:03,116 --> 01:03:05,915
and who comes from the Father.

349
01:03:12,827 --> 01:03:14,988
Stop!

350
01:03:17,165 --> 01:03:21,102
Enough! Your orders were to punish this man...

351
01:03:24,406 --> 01:03:27,307
not to scourge him to death!

352
01:03:33,917 --> 01:03:36,044
Take him away.

353
01:03:37,987 --> 01:03:39,318
Get going!

354
01:03:48,999 --> 01:03:50,489
Get him out of here!

355
01:05:22,233 --> 01:05:23,962
Your majesty...

356
01:05:24,502 --> 01:05:26,197
Take care of it.

357
01:05:27,071 --> 01:05:28,538
A beautiful rose bush.

358
01:05:32,477 --> 01:05:35,674
Look at him... king of the worms!

359
01:05:36,815 --> 01:05:38,942
Hail, wormy king!

360
01:05:41,520 --> 01:05:43,715
A color fit for a king!

361
01:08:00,102 --> 01:08:02,127
We're here to pay our respects.

362
01:08:04,474 --> 01:08:06,772
A leader for our brotherhood!

363
01:08:56,263 --> 01:08:57,958
Behold the man.

364
01:09:01,334 --> 01:09:03,427
Crucify him!

365
01:09:14,148 --> 01:09:16,981
Isn't this enough? Look at him!

366
01:09:17,251 --> 01:09:18,980
Crucify him!

367
01:09:33,435 --> 01:09:34,903
Shall I crucify your king?

368
01:09:36,706 --> 01:09:39,539
We have no king but Caesar!

369
01:09:42,145 --> 01:09:44,010
Speak to me.

370
01:09:44,213 --> 01:09:47,877
I have the power to crucify you, or else to set you free.

371
01:09:52,656 --> 01:09:55,124
You have no power over me...

372
01:09:56,827 --> 01:10:01,697
except what is given you from above.

373
01:10:04,502 --> 01:10:06,868
Therefore, it is he who delivered me to you...

374
01:10:08,006 --> 01:10:12,841
who has the greater sin.

375
01:10:14,981 --> 01:10:17,415
If you free him, Governor...

376
01:10:17,750 --> 01:10:21,652
you are no friend of Caesar's.

377
01:10:23,589 --> 01:10:25,648
You must crucify him!

378
01:11:56,851 --> 01:12:01,117
It is you who want to crucify him, not I. Look you to it.

379
01:12:01,891 --> 01:12:04,689
I am innocent of this man's blood.

380
01:12:23,714 --> 01:12:24,738
Abenader...

381
01:12:30,455 --> 01:12:32,980
Do as they wish.

382
01:13:06,626 --> 01:13:08,754
I am your servant, Father.

383
01:13:08,863 --> 01:13:11,491
Your servant, and the son of Your handmaid.

384
01:13:12,433 --> 01:13:17,393
Why do you embrace your cross, fool?

385
01:13:21,475 --> 01:13:24,240
All right, your highness, let's move!

386
01:14:59,381 --> 01:15:01,849
Help me get near him.

387
01:15:04,887 --> 01:15:07,185
This way.

388
01:15:56,709 --> 01:15:59,041
This way, Mother.

389
01:16:38,354 --> 01:16:39,912
Mother...

390
01:17:41,121 --> 01:17:42,554
Mother...

391
01:18:18,528 --> 01:18:20,428
I'm here...

392
01:18:23,934 --> 01:18:25,902
I'm here...

393
01:18:36,180 --> 01:18:40,640
See, mother, I make all things new.

394
01:19:11,252 --> 01:19:12,549
Who is that?

395
01:19:12,753 --> 01:19:13,481
Who?

396
01:19:15,890 --> 01:19:17,380
She's the Galilean's mother.

397
01:19:18,092 --> 01:19:19,184
Let's go.

398
01:19:21,762 --> 01:19:22,694
Come on!

399
01:20:33,640 --> 01:20:35,699
Don't fret, my daughter.

400
01:20:36,109 --> 01:20:38,304
Don't fret.

401
01:21:08,510 --> 01:21:09,875
Are you blind?

402
01:21:10,879 --> 01:21:14,247
Can't you see, he can't go on?

403
01:21:18,354 --> 01:21:19,821
Help him!

404
01:21:39,077 --> 01:21:40,374
You!

405
01:21:40,912 --> 01:21:45,440
Yes, you! Get over here!

406
01:21:52,024 --> 01:21:53,013
What do you want from me?

407
01:21:53,626 --> 01:21:56,789
This criminal can't carry his cross by himself anymore.

408
01:21:57,063 --> 01:21:59,293
You will help him! Now get going!

409
01:22:00,666 --> 01:22:04,261
I can't do that. It's none of my business. Get someone else!

410
01:22:04,871 --> 01:22:07,170
Help him! He is a holy man.

411
01:22:07,541 --> 01:22:09,338
Do as I tell you. Now move! Let's go!

412
01:22:14,047 --> 01:22:16,447
All right, but remember...

413
01:22:16,817 --> 01:22:20,549
I'm an innocent man, forced to carry the cross of a condemned man.

414
01:22:21,923 --> 01:22:24,391
Stay here. Wait for me.

415
01:24:20,350 --> 01:24:22,978
Permit me, my Lord.

416
01:24:52,985 --> 01:24:56,580
Who do you think you are?

417
01:24:56,955 --> 01:24:59,423
Get away from here.

418
01:25:00,960 --> 01:25:02,484
Impossible people!

419
01:25:11,871 --> 01:25:13,999
Someone stop this!

420
01:25:29,524 --> 01:25:33,119
Stop! Stop!

421
01:25:40,536 --> 01:25:43,528
Leave him alone!

422
01:25:50,613 --> 01:25:55,517
If you don't stop, I won't carry that cross one more step.

423
01:25:56,320 --> 01:25:59,289
I don't care what you do to me!

424
01:26:04,461 --> 01:26:07,431
All right, all right, let's get moving.

425
01:26:07,966 --> 01:26:10,764
We don't have all day. Let's go!

426
01:26:17,375 --> 01:26:19,002
Let's go...

427
01:26:19,811 --> 01:26:21,439
Jew!

428
01:28:59,416 --> 01:29:01,009
Almost there.

429
01:29:07,625 --> 01:29:09,650
We're nearly there.

430
01:29:24,276 --> 01:29:26,244
Almost done.

431
01:29:36,823 --> 01:29:41,123
You have heard it said...

432
01:29:42,730 --> 01:29:46,063
you shall love your neighbor and hate your enemy.

433
01:29:47,634 --> 01:29:49,795
But I say to you...

434
01:29:50,904 --> 01:29:55,865
love your enemies and pray for those who persecute you...

435
01:29:58,646 --> 01:30:01,877
For if you love only those who love you...

436
01:30:05,453 --> 01:30:08,184
what reward is there in that?

437
01:31:04,717 --> 01:31:07,049
I am the good shepherd.

438
01:31:08,455 --> 01:31:10,980
I lay down my life for my sheep.

439
01:31:11,724 --> 01:31:14,750
No one takes my life from me...

440
01:31:15,529 --> 01:31:17,497
but I lay it down of my own accord.

441
01:31:17,965 --> 01:31:20,092
I have power to lay it down...

442
01:31:20,668 --> 01:31:24,160
and power to take it up again.

443
01:31:26,740 --> 01:31:28,641
This command is from my Father.

444
01:32:09,020 --> 01:32:13,514
Get away now, you're free to go. Go on! Go on!

445
01:32:21,666 --> 01:32:25,364
You can get up now!

446
01:33:04,446 --> 01:33:07,643
Get up, your majesty.

447
01:33:08,450 --> 01:33:10,611
Can't you get up?

448
01:33:12,020 --> 01:33:14,352
We haven't got all day.

449
01:33:23,499 --> 01:33:25,023
Come on, move! We're ready.

450
01:33:25,335 --> 01:33:27,166
Get up, your highness.

451
01:35:11,649 --> 01:35:13,173
You are my friends.

452
01:35:15,119 --> 01:35:20,080
There is no greater love than for a man to lay down his life for his friends.

453
01:36:16,288 --> 01:36:21,513
I cannot be with you much longer, my friends.

454
01:36:21,791 --> 01:36:27,424
You cannot go where I am going.

455
01:36:27,931 --> 01:36:31,264
My commandment to you after I am gone is this...

456
01:36:33,069 --> 01:36:34,696
Love one another.

457
01:36:35,639 --> 01:36:38,370
As I have loved you...

458
01:36:39,076 --> 01:36:43,206
so love one another.

459
01:37:06,339 --> 01:37:08,967
You believe in me.

460
01:37:12,679 --> 01:37:15,307
You know that I am the Way...

461
01:37:16,182 --> 01:37:21,143
the Truth, and the Life.

462
01:37:23,123 --> 01:37:27,651
And no one comes to the Father but by me.

463
01:37:32,567 --> 01:37:34,694
Further out...

464
01:37:41,576 --> 01:37:45,035
Idiots! Let me show you how to do it.

465
01:37:45,414 --> 01:37:46,438
Like this!

466
01:38:05,002 --> 01:38:09,496
There! No, get in there. Hold the hand open like this.

467
01:38:10,107 --> 01:38:12,633
Father, forgive them...

468
01:39:02,563 --> 01:39:04,087
Father...

469
01:39:04,900 --> 01:39:08,802
Father my Father... my God...

470
01:39:12,574 --> 01:39:15,702
They don't know... they don't know...

471
01:39:19,615 --> 01:39:21,708
Stupid mongrels!

472
01:39:22,085 --> 01:39:25,816
Turn the wood over on its face, idiots!

473
01:40:54,017 --> 01:40:56,747
Take this and eat.

474
01:40:57,287 --> 01:41:01,690
This is my body which is given up for you.

475
01:42:46,170 --> 01:42:48,468
Take this and drink.

476
01:42:50,841 --> 01:42:53,709
This is my blood of the new covenant...

477
01:42:54,846 --> 01:42:59,078
which is given for you and for many, for the forgiveness of sins.

478
01:42:59,551 --> 01:43:01,519
Do this in memory of me.

479
01:43:31,151 --> 01:43:36,112
If you are the son of God, why don't you save yourself?

480
01:43:38,159 --> 01:43:40,127
Prove to us...

481
01:43:41,997 --> 01:43:45,057
you are who you say you are.

482
01:44:03,420 --> 01:44:06,821
You said you could destroy the temple...

483
01:44:06,957 --> 01:44:09,323
and rebuild it in three days...

484
01:44:10,227 --> 01:44:14,130
and yet you cannot come down from that cross.

485
01:44:14,332 --> 01:44:16,857
If he is the Messiah...

486
01:44:18,202 --> 01:44:22,639
I say let him come down from the cross...

487
01:44:23,908 --> 01:44:26,433
so that we may see and believe.

488
01:44:35,988 --> 01:44:39,048
Father, forgive them...

489
01:44:41,461 --> 01:44:45,522
they know not what they do.

490
01:44:50,270 --> 01:44:52,238
Listen...

491
01:44:53,607 --> 01:44:56,804
he prays for you.

492
01:45:11,693 --> 01:45:14,321
We deserve this, Gesmas...

493
01:45:15,464 --> 01:45:17,455
but he does not.

494
01:45:19,801 --> 01:45:21,326
I have sinned...

495
01:45:22,705 --> 01:45:26,539
and my punishment is just.

496
01:45:27,677 --> 01:45:31,636
You would be justified in condemning me.

497
01:45:33,049 --> 01:45:37,953
I ask only that you remember me, Lord...

498
01:45:38,322 --> 01:45:42,019
when you enter your kingdom.

499
01:45:43,994 --> 01:45:45,791
Amen, I tell you...

500
01:45:49,000 --> 01:45:51,628
on this day you shall be with me...

501
01:45:54,672 --> 01:45:56,299
in paradise.

502
01:49:10,850 --> 01:49:12,715
I thirst.

503
01:49:47,956 --> 01:49:50,425
Flesh of my flesh...

504
01:49:52,928 --> 01:49:55,920
heart of my heart...

505
01:49:57,667 --> 01:50:02,628
My son, let me die with you.

506
01:50:06,209 --> 01:50:07,904
Woman...

507
01:50:09,212 --> 01:50:12,648
behold your son.

508
01:50:19,891 --> 01:50:22,121
Son, behold...

509
01:50:27,598 --> 01:50:29,533
your mother.

510
01:50:39,611 --> 01:50:42,581
There is no one left.

511
01:50:43,750 --> 01:50:48,619
No one, Jesus!

512
01:50:58,866 --> 01:51:02,324
My God...

513
01:51:15,050 --> 01:51:19,009
why have you forsaken me?

514
01:51:37,708 --> 01:51:40,472
It is accomplished.

515
01:52:02,634 --> 01:52:06,264
Father, into your hands...

516
01:52:07,306 --> 01:52:11,538
I commend... my spirit.

517
01:54:04,299 --> 01:54:06,358
Cassius! Hurry!

518
01:54:06,935 --> 01:54:08,368
He's dead!

519
01:54:10,138 --> 01:54:11,605
Make sure!

