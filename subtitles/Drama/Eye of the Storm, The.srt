﻿1
00:00:28,907 --> 00:00:27,384
Lord Godless, the evil warlord
and Master of Godless Palace,

2
00:00:27,451 --> 00:00:27,103
always coveted power and
the massive land of China.

3
00:00:27,183 --> 00:00:31,520
Attempting to usurp the throne,
he sent his assistant, Eunuch Cho,

4
00:00:31,593 --> 00:00:34,146
to the Emperor's Palace
as an undercover agent

5
00:00:34,216 --> 00:00:38,173
and threatened the General in
Command to assist in his plot.

6
00:00:38,253 --> 00:00:42,051
Later on, he used his special
formulated poison to subdue

7
00:00:42,120 --> 00:00:46,056
a large number of martial arts masters,
including martial arts legend, Nameless.

8
00:00:46,118 --> 00:00:50,312
Considering the safety of Chu Chu,
Cloud also let himself fall into captivity.

9
00:00:50,393 --> 00:00:52,222
On the execution ground,

10
00:00:52,292 --> 00:00:55,312
Lord Godless has started to torture his hostages.

11
00:02:37,164 --> 00:02:40,134
Your Emperor has already given
up his throne to my father,

12
00:02:40,331 --> 00:02:42,088
Lord Godless!

13
00:02:44,995 --> 00:02:50,094
Whoever surrenders will receive
medication to restore your strength.

14
00:02:52,530 --> 00:02:57,394
Get your weapons back
and enjoy wealth with us.

15
00:02:58,236 --> 00:03:01,794
Or else, you will be executed.

16
00:03:16,272 --> 00:03:21,217
Nameless, give me your martial arts bible.

17
00:03:22,131 --> 00:03:27,801
Then, I will designate you
as my second-in-command.

18
00:03:30,206 --> 00:03:36,099
For hundreds of years, invaders
have often tried to take over China.

19
00:03:37,171 --> 00:03:40,425
But our country has never been defeated.

20
00:03:42,506 --> 00:03:46,763
You want to take over us
with your small troops.

21
00:03:47,869 --> 00:03:50,098
What a crazy dream.

22
00:04:04,376 --> 00:04:08,932
You go against me?
Kill!

23
00:04:18,040 --> 00:04:21,227
This the medication to counter
Lord Godless' poison.

24
00:05:50,480 --> 00:05:51,505
Heart!

25
00:05:59,058 --> 00:06:00,883
What a bunch of losers.

26
00:07:03,851 --> 00:07:08,976
The return of Wind and Cloud is worthless.

27
00:08:23,333 --> 00:08:26,863
My body is invincible.

28
00:08:28,426 --> 00:08:32,331
Let me break your unbeatable legend today.

29
00:10:29,607 --> 00:10:34,101
From now on I am the Martial Arts Legend

30
00:14:29,055 --> 00:14:31,024
How come your master is so badly wounded?

31
00:14:32,198 --> 00:14:34,321
Master has not fully recovered from poison.

32
00:14:34,393 --> 00:14:38,294
He was injured when he fought Lord Godless.

33
00:14:38,961 --> 00:14:41,297
My condition is really bad.

34
00:14:41,366 --> 00:14:44,152
I will not recover within a short time.

35
00:14:48,101 --> 00:14:51,254
Cloud, where are you going?

36
00:14:53,093 --> 00:14:56,417
I cannot bear the sacrifice of other people.

37
00:14:58,600 --> 00:15:00,186
Cloud!

38
00:15:02,093 --> 00:15:03,464
Cloud, think twice about taking revenge now.

39
00:15:03,595 --> 00:15:07,891
If we are careless, the sacrifice
of our friends will be wasted.

40
00:15:08,937 --> 00:15:13,415
Cloud, you and Wind both have the
potential to fight against Lord Godless.

41
00:15:14,229 --> 00:15:18,603
But both of you need to improve
your power and skills.

42
00:15:19,402 --> 00:15:24,384
Right now, only one person is able
to fight against Lord Godless.

43
00:15:24,596 --> 00:15:28,400
He is Piggy king's elder brother
in martial arts, Lord Wicked.

44
00:15:29,606 --> 00:15:33,190
Lord Wicked?
He is a strange person.

45
00:15:33,269 --> 00:15:35,288
He may not be willing to help.

46
00:15:46,906 --> 00:15:48,568
Enemies are coming.

47
00:15:49,332 --> 00:15:52,957
Let's split up.
I will draw the enemies away.

48
00:15:53,027 --> 00:15:57,223
Piggy king, please bring Wind and Cloud to
Sheng Si Men to ask Lord Wicked for help.

49
00:15:57,566 --> 00:15:59,036
Alright.

50
00:16:02,268 --> 00:16:03,131
Heart!

51
00:16:03,439 --> 00:16:07,899
Bring Earth and Sky with you
to catch Nameless and his group.

52
00:16:08,008 --> 00:16:11,494
Take out other martial arts schools on your way.

53
00:16:12,131 --> 00:16:15,160
Kill everyone who goes against me.

54
00:18:05,878 --> 00:18:08,178
Is he able to beat Lord Godless?

55
00:18:10,117 --> 00:18:11,233
He is actually a very strange person.

56
00:18:11,541 --> 00:18:13,440
I'm really not sure if he is willing to help.

57
00:18:13,953 --> 00:18:16,571
I hope he could consider the situation
of our country and make an exception.

58
00:18:33,880 --> 00:18:36,545
Leng Lao, long time no see.

59
00:18:38,921 --> 00:18:41,504
You just keep working.
Slow down and take a break.

60
00:18:41,944 --> 00:18:45,074
Enjoy your life.

61
00:18:49,548 --> 00:18:53,508
Hey, old man, I am here. Come out and see me.

62
00:19:15,614 --> 00:19:17,558
Lord Wicked,

63
00:19:19,554 --> 00:19:24,315
we come on behalf of Nameless

64
00:19:24,553 --> 00:19:27,216
and sincerely ask for your
help to save our country.

65
00:19:28,050 --> 00:19:32,184
Outsiders have invaded China
and killed our innocent people.

66
00:19:32,492 --> 00:19:38,079
A great evil is now present and
the situation is really critical.

67
00:19:39,956 --> 00:19:42,552
I've paid no attention to earthly matters for years.

68
00:19:43,020 --> 00:19:44,054
If Nameless the Legend is involved in this matter,

69
00:19:44,361 --> 00:19:46,380
Lord Godless will be defeated eventually.

70
00:19:46,926 --> 00:19:48,443
Please go.

71
00:19:49,995 --> 00:19:53,482
Nameless is badly wounded so he cannot fight back.

72
00:19:53,962 --> 00:19:55,514
That's why he asked us to come here.

73
00:19:56,190 --> 00:20:00,248
If our country falls into Lord Godless' hands,
what do you think will happen?

74
00:20:12,925 --> 00:20:14,294
Cloud...

75
00:20:27,161 --> 00:20:29,588
To show our sincerity,

76
00:20:30,000 --> 00:20:31,086
we will kneel here until
you agree to save the world

77
00:20:31,326 --> 00:20:34,224
and stop Lord Godless from killing innocents.

78
00:20:34,994 --> 00:20:42,116
Hey, old man, stop being so arrogant.

79
00:21:01,128 --> 00:21:03,596
Leng Lao, come and drink with me.
The food is good.

80
00:21:06,641 --> 00:21:11,192
Both of you haven't eaten since yesterday.

81
00:21:11,299 --> 00:21:13,129
Please eat something now.

82
00:21:17,266 --> 00:21:20,849
I don't need to eat.
Thank you.

83
00:21:23,198 --> 00:21:24,859
How about Wind?

84
00:21:25,567 --> 00:21:29,125
Uncle Lord Wicked, I am
the daughter of Knife King.

85
00:21:29,195 --> 00:21:31,399
I come to extend my greetings to you.

86
00:21:54,235 --> 00:21:56,332
Second Dream, why are you here?

87
00:22:00,136 --> 00:22:04,432
Piggy king, our country is in great danger.

88
00:22:04,970 --> 00:22:11,452
I hope to help Wind... and Cloud.

89
00:22:13,232 --> 00:22:17,333
Please ask the old man to come out and help.

90
00:22:28,274 --> 00:22:32,137
Uncle Lord Wicked, this is
Second Dream, please come out.

91
00:22:44,976 --> 00:22:49,060
Oh, Uncle Lord Wicked does not
like Second Dream anymore.

92
00:22:50,536 --> 00:22:52,510
Second Dream will also kneel here and will not eat,

93
00:22:53,064 --> 00:22:55,440
just like Wind, until Uncle Lord Wicked comes out.

94
00:23:04,136 --> 00:23:08,500
You finally grew up... How is your father?

95
00:23:09,499 --> 00:23:12,497
He is fine, thank you.

96
00:23:13,639 --> 00:23:17,198
Dad is longing to have a good fight with you.

97
00:23:18,207 --> 00:23:20,336
You have such a sweet mouth.

98
00:23:50,177 --> 00:23:52,942
Old man, what happened to your hands?

99
00:24:44,383 --> 00:24:47,285
I overestimated myself and
started training in the evil way.

100
00:24:47,455 --> 00:24:50,281
I thought I could control the evil spirit.
But I was wrong.

101
00:24:50,618 --> 00:24:54,976
I didn't want to kill innocents anymore

102
00:24:57,986 --> 00:25:05,475
and therefore I broke off both
my arms a few years ago.

103
00:25:11,653 --> 00:25:14,614
Now I am no longer as powerful as before.

104
00:25:23,690 --> 00:25:27,486
Supreme martial arts skills
cannot be learned in a short time.

105
00:25:27,957 --> 00:25:31,652
In this situation,
there is only one alternative.

106
00:25:31,652 --> 00:25:34,950
What is the alternative?

107
00:25:39,188 --> 00:25:42,088
We take the evil way to increase our power.

108
00:25:50,529 --> 00:25:51,882
It is very dangerous to take the evil way.

109
00:25:52,220 --> 00:25:54,188
You might be overwhelmed by evil power.

110
00:25:54,619 --> 00:25:56,443
But this is the only way
to increase your power

111
00:25:56,456 --> 00:25:58,279
within a short time
to combat Lord Godless.

112
00:25:59,085 --> 00:26:00,608
Who would like to give it a try?

113
00:26:01,058 --> 00:26:02,075
Me!

114
00:26:05,392 --> 00:26:09,016
Old man, why don't you recommend
another martial arts expert

115
00:26:09,317 --> 00:26:12,015
and ask him for help?

116
00:26:13,927 --> 00:26:16,054
Open your hands and let me see.

117
00:26:20,458 --> 00:26:21,620
Cloud...

118
00:26:22,258 --> 00:26:24,582
You are too aggressive, too extreme.

119
00:26:25,124 --> 00:26:27,552
The evil spirit will only boost your violence.

120
00:26:30,030 --> 00:26:31,424
Wind...

121
00:26:31,992 --> 00:26:33,188
You have good control over your power.

122
00:26:33,530 --> 00:26:35,016
Even if you enter the evil way,

123
00:26:35,297 --> 00:26:40,497
you would have a better chance of
returning from evil, just like me.

124
00:26:59,563 --> 00:27:04,221
Cloud, please be patient
and wait until I finish training.

125
00:27:04,500 --> 00:27:06,517
We will then fight Lord Godless together.

126
00:27:11,094 --> 00:27:14,542
But I hope you will promise me one thing.

127
00:27:16,498 --> 00:27:19,929
If I am overwhelmed by evil power

128
00:27:20,166 --> 00:27:23,490
and start to kill innocents,

129
00:27:24,263 --> 00:27:27,960
please be decisive and kill me right away.

130
00:27:47,668 --> 00:27:51,965
This is for you.

131
00:27:52,296 --> 00:27:54,992
Thank you for saving my life so many times.

132
00:28:38,577 --> 00:28:43,232
Okay, I won't bother you now.

133
00:28:45,170 --> 00:28:46,534
Thank you.

134
00:29:21,406 --> 00:29:23,235
No one, not even a genius,

135
00:29:23,478 --> 00:29:27,528
could learn top martial
arts skills in a few days.

136
00:29:28,179 --> 00:29:30,535
I understand it takes time and effort.

137
00:29:37,939 --> 00:29:41,110
It's hard to be righteous,
but it is easy to be evil.

138
00:29:42,004 --> 00:29:45,300
A thousand years of spiritual training
might still fall prey to evil.

139
00:29:46,644 --> 00:29:48,578
If you want to boost up all your power,

140
00:29:49,009 --> 00:29:52,950
you must devote yourself to the evil way.
Will you regret it?

141
00:29:53,314 --> 00:29:55,995
I am well prepared for this.
I will not regret it!

142
00:29:58,477 --> 00:30:02,908
I need your blood to blend
with water from the Evil Pool.

143
00:30:03,546 --> 00:30:09,974
From outside to inside,
you have to turn into evil.

144
00:30:42,954 --> 00:30:44,610
We finally meet.

145
00:30:52,946 --> 00:30:54,607
Wind...

146
00:30:57,291 --> 00:31:00,518
This letter comes from you.
I always keep it with me.

147
00:31:12,417 --> 00:31:16,605
"Listen to the wind and rain.
Let go of earthly troubles."

148
00:33:07,230 --> 00:33:11,181
Master asked you to meet him at Ling Yin Temple.

149
00:36:49,984 --> 00:36:53,576
You are able to block my styles

150
00:36:54,515 --> 00:36:59,638
and learn my styles so fast.

151
00:37:01,643 --> 00:37:05,412
It proves you are capable
of rescuing our country.

152
00:37:07,023 --> 00:37:09,636
I am going to transfer my power to you.

153
00:37:09,718 --> 00:37:15,413
I am not as great as you think.

154
00:37:17,422 --> 00:37:19,380
I kneeled in front of Lord Wicked

155
00:37:20,184 --> 00:37:22,678
just because I wanted to fight
for myself if I turned evil.

156
00:37:34,356 --> 00:37:38,410
Now I only have one tenth of my power left.

157
00:37:45,319 --> 00:37:51,481
To fight against Lord Godless,
Wind and Cloud must join hands.

158
00:37:52,485 --> 00:37:55,677
I can feel that, if Wind and Cloud join hands,

159
00:37:56,157 --> 00:37:59,580
an exceptional power will be created.

160
00:38:00,590 --> 00:38:03,549
The power will grow stronger and stronger.

161
00:39:03,525 --> 00:39:05,489
When the word "evil" is filled
with water from the pool,

162
00:39:06,019 --> 00:39:07,549
your training is done.

163
00:41:06,335 --> 00:41:09,631
On the map, the dragon's head
is pointing to Dragon Tomb,

164
00:41:10,068 --> 00:41:13,965
the place hidden with the secret of the
royal family and vital to the destiny of China.

165
00:41:15,072 --> 00:41:20,162
It's in a cave in the back garden of the Palace.

166
00:41:21,336 --> 00:41:23,569
The Emperor is the only person

167
00:41:24,108 --> 00:41:26,432
who knows the way inside the Dragon Tomb.

168
00:41:27,101 --> 00:41:31,441
If someone gets lost there,
he will be trapped inside forever.

169
00:41:33,610 --> 00:41:35,962
The Emperor is under our control.

170
00:41:36,305 --> 00:41:37,392
As soon as we locate the Dragon Tomb,

171
00:41:37,675 --> 00:41:40,188
our dream will come true.
China will be mine and my father's!

172
00:41:59,506 --> 00:42:04,636
Remember, you can't share the country.

173
00:42:05,677 --> 00:42:09,503
Master, if the Legend Nameless is still alive

174
00:42:09,744 --> 00:42:13,172
and Wind and Cloud are still around,

175
00:42:13,382 --> 00:42:16,470
I heard that they have
some special hidden power.

176
00:42:21,115 --> 00:42:24,039
Nameless has underestimated
the strength of my drug.

177
00:42:24,340 --> 00:42:30,175
He tried to use his special strike
before he had recovered.

178
00:42:32,040 --> 00:42:37,411
As for Wind and Cloud, they are nobody.

179
00:42:39,582 --> 00:42:42,278
But if they join forces...

180
00:42:56,582 --> 00:42:59,508
Only those persons who can locate the Dragon Tomb

181
00:43:00,373 --> 00:43:03,479
could share this country with me.

182
00:43:06,079 --> 00:43:07,345
Understood, Dad.

183
00:43:09,078 --> 00:43:12,238
Master, Nameless is hiding at Ling Yin Temple.

184
00:43:12,548 --> 00:43:14,578
Wind and Cloud are at Sheng Si Men.

185
00:43:15,514 --> 00:43:17,348
Send the carrier pigeons and
bring my message to Earth and Sky.

186
00:43:17,561 --> 00:43:19,415
Ask them to get rid of Nameless.

187
00:43:20,652 --> 00:43:23,667
Also, select a group of evil
warriors for Sheng Si Men.

188
00:43:24,146 --> 00:43:25,380
Yes, Master!

189
00:43:30,220 --> 00:43:33,445
I am going with you to search
for the Dragon Tomb.

190
00:43:34,512 --> 00:43:36,245
Bring along the Emperor.

191
00:43:40,383 --> 00:43:41,353
Son...

192
00:43:41,621 --> 00:43:43,573
Yes, I understand.

193
00:45:15,755 --> 00:45:17,691
Lead us to the Dragon Tomb.

194
00:45:18,725 --> 00:45:21,192
My father promises endless wealth

195
00:45:22,489 --> 00:45:24,218
and honor for the Royal Family.

196
00:45:26,359 --> 00:45:28,652
Wealth and honor
have always been mine.

197
00:45:29,189 --> 00:45:31,719
This land also belongs to me.

198
00:45:33,496 --> 00:45:35,552
It looks like my benevolence
to you is wasted for no purpose.

199
00:45:36,031 --> 00:45:37,228
Stop talking.

200
00:45:42,263 --> 00:45:43,751
Stand guard here.

201
00:45:45,101 --> 00:45:46,646
The rest of you, follow me.

202
00:45:48,767 --> 00:45:55,556
Go. Get inside.

203
00:46:43,667 --> 00:46:46,037
I am astonished you could realize

204
00:46:46,234 --> 00:46:50,214
and create 23 completely different new
sword styles within such a short time.

205
00:46:50,469 --> 00:46:55,026
Master, the new styles that I created

206
00:46:55,237 --> 00:46:56,602
are based on what you've taught me.

207
00:46:57,501 --> 00:46:59,304
May I ask you to name these styles?

208
00:47:01,800 --> 00:47:05,297
The new styles you created
are of divine superiority.

209
00:47:06,538 --> 00:47:09,689
No word could name them adequately.

210
00:47:16,775 --> 00:47:20,698
Maybe... there is only one word.

211
00:47:41,239 --> 00:47:48,036
Cloud, this is the name
of your new sword style.

212
00:47:49,802 --> 00:47:53,402
Master, what does it mean?

213
00:47:53,671 --> 00:47:57,661
I have never seen a word like this.

214
00:47:58,510 --> 00:48:01,235
This is a word I just created.

215
00:48:02,073 --> 00:48:05,633
It's a word that partly
resembles the character "cloud"

216
00:48:07,610 --> 00:48:11,004
and partly resembles a sword.

217
00:48:11,144 --> 00:48:13,436
This is exactly what it signifies.

218
00:48:14,243 --> 00:48:18,177
You can pronounce it as "Ba"!

219
00:48:21,777 --> 00:48:23,175
"Ba"

220
00:50:13,527 --> 00:50:16,108
Stop, please. What are you sweeping?

221
00:50:16,351 --> 00:50:18,012
I've watched you for a long time.

222
00:50:18,252 --> 00:50:20,348
There is nothing to sweep at all.

223
00:50:27,219 --> 00:50:29,649
You keep sweeping day and night.

224
00:50:29,649 --> 00:50:31,278
You are going to sweep the ground open.

225
00:50:34,156 --> 00:50:35,620
Every one has his own obsession.

226
00:50:36,222 --> 00:50:38,581
Can I ask you to stop fooling around?

227
00:50:44,059 --> 00:50:48,579
Wind is at his final stage of training.

228
00:50:49,189 --> 00:50:50,359
It is the critical moment
and he must not be disturbed.

229
00:50:50,371 --> 00:50:51,550
We cannot go inside either.

230
00:50:52,321 --> 00:50:57,270
Or else, before he turns into evil,
he may turn into a ghost

231
00:51:00,787 --> 00:51:06,189
Cloud, when are you coming back?

232
00:54:46,604 --> 00:54:54,098
Cloud, your new style is still immature

233
00:54:55,549 --> 00:54:58,346
and its power is not fully released.

234
00:55:00,677 --> 00:55:05,807
You'll be even more powerful in the future.

235
00:55:11,739 --> 00:55:18,146
Now, go to Sheng Si Men and see what's going on.

236
00:56:11,789 --> 00:56:13,518
Which path leads to the Dragon Tomb?

237
00:57:50,099 --> 00:57:57,464
China is mine.

238
00:58:30,304 --> 00:58:31,560
Lord Wicked...

239
00:58:35,165 --> 00:58:38,353
What happened?
Where is Wind?

240
00:58:38,699 --> 00:58:40,352
Wind didn't complete his training and

241
00:58:40,605 --> 00:58:42,789
came out earlier to save Second Dream.

242
00:58:45,329 --> 00:58:47,551
I am very puzzled as well.

243
00:58:48,361 --> 00:58:50,219
Theoretically, if the word is not
fully filled with water from the pool,

244
00:58:50,460 --> 00:58:52,265
the trainee should not leave
his training and come out.

245
00:58:53,135 --> 00:58:55,290
If he is disturbed at the
final stage of his training,

246
00:58:55,569 --> 00:58:58,125
he could be seriously hurt
and become totally disabled.

247
00:58:58,366 --> 00:59:00,262
Even worse, he could die right away.

248
00:59:02,236 --> 00:59:05,669
Wind is a very special person.

249
00:59:10,635 --> 00:59:12,494
It is possible

250
00:59:12,803 --> 00:59:15,598
if someone or something could wake him
up before he turns completely into evil,

251
00:59:15,827 --> 00:59:18,132
he could be saved and return to normal.

252
00:59:19,165 --> 00:59:23,191
That means Wind still has a chance?

253
00:59:27,870 --> 00:59:30,199
Where is he now?

254
00:59:36,268 --> 00:59:40,198
Cloud, what did you do with Nameless?

255
00:59:41,437 --> 00:59:43,670
He is my teacher now.
He transferred all his power to me,

256
00:59:44,606 --> 00:59:46,729
and led me to create a new sword style.

257
00:59:49,173 --> 00:59:51,297
The Emperor was captured by Lord Godless.

258
00:59:51,436 --> 00:59:53,764
They are seeking the Royal Secret at
the Cave in the Palace's back garden.

259
00:59:58,574 --> 01:00:00,155
The Royal Secret is hidden within Heaven Cave

260
01:00:00,465 --> 01:00:02,405
and it is related to the destiny of China.

261
01:00:02,708 --> 01:00:04,766
It must not fall into the hands of Lord Godless.

262
01:00:05,307 --> 01:00:08,203
Cloud, you must stop him!

263
01:00:09,502 --> 01:00:11,696
Lord Godless has a special skill
that makes him invincible.

264
01:00:12,265 --> 01:00:14,469
But I believe he has a weak point

265
01:00:14,709 --> 01:00:16,436
which is hiding behind his strongest point.

266
01:00:19,840 --> 01:00:21,357
I'll remember your words.

267
01:00:22,106 --> 01:00:25,598
I'll return as soon as possible.

268
01:00:50,716 --> 01:00:53,397
I think I know where Wind is.

269
01:01:13,785 --> 01:01:15,800
This is so graceful!

270
01:01:30,678 --> 01:01:32,404
Where is the Dragon Tomb?

271
01:01:36,581 --> 01:01:38,442
Is this the Dragon Tomb?

272
01:01:51,877 --> 01:01:56,141
What a cool and daring man.

273
01:01:58,515 --> 01:02:04,281
I heard it is very important for
Chinese people to leave progeny.

274
01:02:06,614 --> 01:02:11,713
If you don't talk,
I'll start to kill your sons.

275
01:02:12,151 --> 01:02:13,709
Stop that!

276
01:02:26,145 --> 01:02:30,244
Your Majesty, I...

277
01:02:58,390 --> 01:02:59,407
Son...

278
01:03:06,545 --> 01:03:07,577
Protect His Majesty.

279
01:03:14,593 --> 01:03:16,142
Your Majesty, please forgive us.

280
01:03:16,320 --> 01:03:20,445
We were forced to surrender.

281
01:03:20,688 --> 01:03:23,174
We are very sorry.

282
01:03:23,383 --> 01:03:25,780
I understand.

283
01:03:52,790 --> 01:03:54,655
Protect His Majesty... stand back.

284
01:03:55,122 --> 01:03:56,212
Bring him with us.

285
01:03:56,458 --> 01:03:57,682
Bring them with us.

286
01:04:34,198 --> 01:04:35,590
Cloud, you've made a great
improvement over these days.

287
01:04:35,903 --> 01:04:39,228
I might have underestimated you, I am so...

288
01:09:02,354 --> 01:09:05,443
This will be the tomb of Cloud.

289
01:12:02,698 --> 01:12:05,290
Lord Godless has special skills
that make him invincible.

290
01:12:05,542 --> 01:12:07,628
But I believe he has a weak point

291
01:12:07,908 --> 01:12:09,671
which is hiding behind his strongest point.

292
01:14:35,455 --> 01:14:36,537
Wind...

293
01:15:56,521 --> 01:15:58,249
Now Wind has taken the Dragon Bone.

294
01:15:58,528 --> 01:16:01,354
Take it back. This is my order.

295
01:16:02,528 --> 01:16:03,922
Yes, Your Majesty.

296
01:16:16,520 --> 01:16:21,655
I understand that the two of
you have saved our country.

297
01:16:22,624 --> 01:16:24,593
But you need to know,

298
01:16:24,892 --> 01:16:27,550
the Dragon Bone is the root of our nation.

299
01:16:28,259 --> 01:16:30,848
If it is lost, our country will fall.

300
01:17:52,864 --> 01:17:54,164
Wind...

301
01:17:59,291 --> 01:18:02,531
Finally I found you.

302
01:18:04,932 --> 01:18:09,499
Wind, do you remember,
I mentioned in my letter

303
01:18:12,306 --> 01:18:15,231
this is the place where we can
hide away from this earthly world?

304
01:18:23,606 --> 01:18:27,761
Wind... Wind...

305
01:19:12,913 --> 01:19:15,641
I still don't understand why Wind
appeared in the Dragon Tomb.

306
01:19:17,473 --> 01:19:20,493
It is your chi that brought
Wind to the Dragon Tomb.

307
01:19:20,772 --> 01:19:23,236
You are both from the same root.

308
01:19:23,409 --> 01:19:24,875
Wind moves clouds.

309
01:19:25,383 --> 01:19:26,781
Cloud changes and Wind appears.

310
01:19:30,912 --> 01:19:32,406
Is he already overwhelmed by evil spirit?

311
01:19:32,673 --> 01:19:34,613
I hope we can find Wind

312
01:19:35,541 --> 01:19:37,313
before he turns into evil,

313
01:19:37,551 --> 01:19:39,208
and before the Emperor gets him.

314
01:19:41,511 --> 01:19:43,698
Hey... Ghost Tiger told us
that Wind is at Nie Village.

315
01:19:43,977 --> 01:19:46,472
The Emperor has sent his men to kill him.

316
01:20:17,353 --> 01:20:20,606
Wind, surrender now

317
01:20:20,875 --> 01:20:22,538
and give back the Dragon Bone.

318
01:20:22,717 --> 01:20:25,346
Or else, don't regret.

319
01:20:25,716 --> 01:20:28,798
General, please give me more time.

320
01:20:29,277 --> 01:20:32,661
I will convince Wind
to return the Dragon Bone.

321
01:20:34,647 --> 01:20:41,734
Actually, Wind has not shown
any evil signs over these days.

322
01:20:43,918 --> 01:20:45,716
If you say he has been
good all these days,

323
01:20:45,944 --> 01:20:47,431
he has already recovered.

324
01:20:47,713 --> 01:20:48,913
Then he should give back the
Dragon Bone right away.

325
01:22:22,288 --> 01:22:24,185
Wind is gone.

326
01:22:56,525 --> 01:22:58,395
To throw the evil out,

327
01:22:58,596 --> 01:23:00,390
we need to get rid of the evil eye first.

328
01:23:00,827 --> 01:23:03,559
Besides, if you have the chance,
try to bring Wind into your mental state.

329
01:23:03,830 --> 01:23:05,493
To throw the evil out,

330
01:23:05,832 --> 01:23:07,794
we need to get rid of the evil eye first.

331
01:23:08,492 --> 01:23:11,195
Try to bring Wind into your mental state

332
01:23:11,364 --> 01:23:13,333
and tame his evil heart.

333
01:30:06,936 --> 01:30:08,365
Wind...

334
01:30:33,334 --> 01:30:34,730
Cloud...

335
01:30:39,633 --> 01:30:42,903
Wind, please stop!

336
01:30:48,567 --> 01:30:49,933
Go away.

337
01:30:51,836 --> 01:30:56,960
Wind... he is completely out of control.

338
01:31:04,742 --> 01:31:08,700
Wind, it's me, Second Dream...

339
01:31:09,568 --> 01:31:11,006
Do you remember me?

340
01:33:47,022 --> 01:33:48,710
Please stop!

341
01:34:11,627 --> 01:34:12,710
Cloud...

342
01:34:49,628 --> 01:34:51,312
Cloud...

343
01:34:53,664 --> 01:34:57,457
Cloud, how are you?

344
01:34:57,628 --> 01:35:00,584
Cloud, why do you look like this?

345
01:35:02,296 --> 01:35:05,751
Second Dream, has Wind turned evil?

346
01:35:18,888 --> 01:35:20,450
Wind!

347
01:35:21,530 --> 01:35:25,429
How can you hurt Cloud and Second Dream?

348
01:35:27,031 --> 01:35:29,430
Are you out of control?

349
01:35:32,601 --> 01:35:36,324
Wind, Lord Wicked said that

350
01:35:36,622 --> 01:35:39,622
as long as you don't turn
completely into evil,

351
01:35:39,764 --> 01:35:41,823
you can be saved and return to normal.

352
01:36:02,601 --> 01:36:05,826
Wind, can you hear me?

353
01:36:20,904 --> 01:36:22,023
Cloud, he...

354
01:37:30,035 --> 01:37:31,027
Chu Chu...

355
01:37:49,771 --> 01:37:51,335
This is for you.

356
01:37:52,377 --> 01:37:54,530
Thank you for saving my life so many times.

357
01:41:19,865 --> 01:41:22,486
If I am overwhelmed by evil power
and start to kill innocents,

358
01:41:22,729 --> 01:41:25,813
please kill me right away.

359
01:42:32,662 --> 01:42:33,629
Cloud...

360
01:43:16,731 --> 01:43:18,030
Wind...

361
01:43:18,574 --> 01:43:22,592
Second Dream...

362
01:45:15,379 --> 01:45:23,373
I said before, if I turn into evil,
ask Cloud to kill me.

363
01:45:28,118 --> 01:45:32,713
But why... why?

364
01:45:36,119 --> 01:45:39,514
Why?

365
01:45:43,980 --> 01:45:47,376
Cloud, why?

366
01:45:52,458 --> 01:45:54,546
I am sorry.

367
01:45:55,026 --> 01:45:56,920
I am sorry.

368
01:45:58,084 --> 01:45:59,044
Wind...

369
01:45:59,484 --> 01:46:00,717
Let me go!

370
01:46:00,856 --> 01:46:02,377
Don't!

371
01:46:03,614 --> 01:46:05,709
Why didn't you kill me?

372
01:46:06,353 --> 01:46:07,916
Why?

373
01:46:09,618 --> 01:46:12,409
Why didn't you kill me in the first place?

374
01:46:15,051 --> 01:46:19,040
Why didn't you kill me in the first place?