1
00:07:23,007 --> 00:07:24,406
Excuse me...

2
00:12:14,727 --> 00:12:16,558
-Good evening.
-Good evening.

3
00:12:17,367 --> 00:12:18,880
A whisky, please.

4
00:12:30,287 --> 00:12:31,322
A large one.

5
00:12:41,287 --> 00:12:43,676
-With ice?
-No ice, thanks.

6
00:13:21,127 --> 00:13:22,765
Another one, please.

7
00:13:35,607 --> 00:13:39,077
I just saw a distinguished-looking
woman leaving here,

8
00:13:39,247 --> 00:13:43,638
slim, pretty, an older woman.
Do you happen to know her?

9
00:13:45,247 --> 00:13:47,807
I was late to meet her
and I've lost track of her.

10
00:13:48,807 --> 00:13:51,677
-Can you hear what he's saying??
-No.

11
00:13:52,487 --> 00:13:54,000
And I'm not interested.

12
00:13:54,367 --> 00:13:57,482
Maybe he's asking about that one

13
00:13:57,767 --> 00:13:59,316
who was just in here...

14
00:14:01,527 --> 00:14:02,596
Probably.

15
00:14:04,007 --> 00:14:05,599
What do you care?

16
00:14:05,767 --> 00:14:08,884
Curiosity, my dear, curiosity...

17
00:14:10,447 --> 00:14:13,280
She's probably come back
to live in Paris.

18
00:14:13,607 --> 00:14:15,837
Sorry.
I have to go and serve the girls.

19
00:14:16,007 --> 00:14:18,362
Give me another one, please.

20
00:14:23,487 --> 00:14:24,476
Here you are...

21
00:14:36,167 --> 00:14:38,237
That lady isn't a regular.

22
00:14:38,407 --> 00:14:42,002
I don't know her, never seen her before.

23
00:14:42,447 --> 00:14:47,123
She came to leave a message for a friend
of hers, who rarely comes in here...

24
00:14:47,687 --> 00:14:48,881
And what's this friend's name?

25
00:14:49,047 --> 00:14:52,803
I know her face, but I don't know
her name. But hold on...

26
00:14:54,647 --> 00:14:56,922
Mathilde.
Her name's Mathilde.

27
00:14:57,087 --> 00:15:00,086
Mathilde.
Do you know her address?

28
00:15:00,247 --> 00:15:01,839
Who's? Mathilde's?

29
00:15:02,607 --> 00:15:05,360
No, the one I'm looking for.

30
00:15:08,967 --> 00:15:12,479
Hotel Regina.
I believe she lives there.

31
00:15:12,647 --> 00:15:15,395
Hotel Regina... Hotel Regina?

32
00:15:17,167 --> 00:15:18,361
Excuse me.

33
00:15:20,247 --> 00:15:22,886
You see?
You're a lucky man after all!

34
00:15:27,087 --> 00:15:31,000
Thanks very much.
Always at your service, sir.

35
00:15:31,647 --> 00:15:34,286
It was lucky I came in here.

36
00:15:34,447 --> 00:15:38,235
It doesn't always happen that way...
Thank you, sir.

37
00:17:19,367 --> 00:17:21,597
-Good morning.
-Good morning, Mr...

38
00:17:21,767 --> 00:17:23,758
-Husson.
-Mr Husson.

39
00:17:23,927 --> 00:17:27,920
I'm looking for Madame
S�verine Serizy. Is she in?

40
00:17:28,087 --> 00:17:29,964
Madame is still in her room.

41
00:17:32,247 --> 00:17:35,045
Can I have her room number?

42
00:17:35,647 --> 00:17:37,956
It's 224 on the second floor.

43
00:17:38,127 --> 00:17:39,765
The lifts are just there on the left.

44
00:18:05,607 --> 00:18:06,562
Nothing for me?

45
00:18:06,727 --> 00:18:10,197
Nothing, Madame,
but a man was here... a Mr Husson.

46
00:18:10,367 --> 00:18:13,165
-Husson?
-Yes he's gone up.

47
00:18:13,327 --> 00:18:16,319
Please don't tell him you saw me,
or where to find me.

48
00:18:28,887 --> 00:18:30,880
Madame wasn't in her room.

49
00:18:30,880 --> 00:18:32,880
Look she's just leaving.

50
00:19:54,967 --> 00:19:57,820
Good evening, sir.
A large whisky, no ice?...

51
00:19:57,820 --> 00:19:59,035
Good evening...

52
00:19:59,767 --> 00:20:01,598
-What's your name?
-Benedito.

53
00:20:01,767 --> 00:20:04,804
-Benedito?
-Yes, Benedito, at your service.

54
00:20:04,967 --> 00:20:06,446
Curious name.

55
00:20:07,007 --> 00:20:09,022
But you've got a good memory,
Benedito?

56
00:20:09,022 --> 00:20:10,522
Or do you know me from somewhere?

57
00:20:10,687 --> 00:20:13,599
I don't know you from anywhere.

58
00:20:13,767 --> 00:20:18,887
I've only been here once,
yesterday,

59
00:20:19,047 --> 00:20:21,117
and you know what I drink

60
00:20:21,287 --> 00:20:23,847
and you can tell me apart
from all the regulars?

61
00:20:24,207 --> 00:20:27,802
It's because
you're out of the ordinary,

62
00:20:27,967 --> 00:20:31,755
and like you said:
because you're not a regular.

63
00:20:31,927 --> 00:20:35,842
-Fine! I'll have my usual!
-Coming right up.

64
00:20:37,927 --> 00:20:39,363
Are you sure you don't know me?

65
00:20:39,363 --> 00:20:41,363
I don't know you from anywhere, sir.

66
00:20:41,527 --> 00:20:45,156
It's only because
you came in yesterday.

67
00:20:45,647 --> 00:20:47,365
I'm glad I did.

68
00:20:48,487 --> 00:20:50,284
You know why was I here last night?

69
00:20:51,527 --> 00:20:56,400
If not for the whisky and the lady's
address, then I don't know.

70
00:20:56,567 --> 00:21:00,845
Then I'll tell you a story
of what didn't happen.

71
00:21:01,007 --> 00:21:04,158
-Of what didn't happen?...
-Exactly.

72
00:21:06,127 --> 00:21:10,560
About fifty years ago.
It happened here in Paris

73
00:21:10,727 --> 00:21:13,878
between a couple
and the husband's best friend.

74
00:21:15,287 --> 00:21:19,519
This lady was young
and extremely attractive.

75
00:21:20,767 --> 00:21:22,650
-But she had a perversion.
-A perversion?...

76
00:21:22,767 --> 00:21:25,396
Yes, she was a pervert, a masochist

77
00:21:25,567 --> 00:21:27,285
and she had some
very strange adventures.

78
00:21:27,447 --> 00:21:31,486
And that made me very curious.

79
00:21:32,007 --> 00:21:33,201
Excuse me.

80
00:21:34,407 --> 00:21:36,284
A beer and an orange juice.

81
00:21:41,127 --> 00:21:42,401
A beer

82
00:21:43,207 --> 00:21:44,640
and an orange juice.

83
00:21:50,367 --> 00:21:52,323
She loved her husband,

84
00:21:53,287 --> 00:21:54,766
and surprisingly,

85
00:21:56,007 --> 00:22:00,478
it was this love that drove her
to betray him in secret.

86
00:22:01,327 --> 00:22:03,682
Really, that's very strange.

87
00:22:04,847 --> 00:22:06,803
And her most secret desire

88
00:22:07,447 --> 00:22:11,201
was to betray her husband
with his best friend.

89
00:22:11,767 --> 00:22:13,405
Such was her sadism.

90
00:22:13,567 --> 00:22:16,443
-Can you hear what they're saying?
-Not really.

91
00:22:17,327 --> 00:22:20,683
The guy's ignoring us.

92
00:22:20,847 --> 00:22:22,945
Who cares?

93
00:22:23,047 --> 00:22:27,403
-A guy like that.
-Well, I think he's good-looking.

94
00:22:27,687 --> 00:22:29,803
Can't you see he's
completely ignoring us?

95
00:22:29,967 --> 00:22:31,320
Well, that's true.

96
00:22:32,207 --> 00:22:35,085
She disguised her masochism.

97
00:22:35,247 --> 00:22:39,640
But it was the mask she needed
for her sadism.

98
00:22:40,087 --> 00:22:44,597
But it happened like you just said?

99
00:22:45,487 --> 00:22:48,365
And you knew about it?

100
00:22:48,647 --> 00:22:49,921
Yes, I did.

101
00:22:50,087 --> 00:22:53,318
She was aware I knew a fair bit
about what she was up to.

102
00:22:53,727 --> 00:22:58,840
Anyway, things like that happen,
although in different ways,

103
00:22:59,007 --> 00:23:01,999
with some people nowadays...

104
00:23:03,047 --> 00:23:07,086
-How do you know?
-From what I hear, of course.

105
00:23:07,607 --> 00:23:11,486
Because a lot of customers
feel the need to confess.

106
00:23:11,647 --> 00:23:14,115
And they only do it with a stranger.

107
00:23:14,287 --> 00:23:18,280
With somebody who seems
disinterested in what he's hearing.

108
00:23:18,447 --> 00:23:22,122
That way, they confess freely.

109
00:23:22,727 --> 00:23:27,005
And they seem relieved.

110
00:23:27,607 --> 00:23:33,244
It's a need,
to get it off their chests, to tell...

111
00:23:34,487 --> 00:23:37,684
-Tell what?
-Things like that...

112
00:23:38,607 --> 00:23:42,122
Betrayals and lies.

113
00:23:43,127 --> 00:23:44,446
Disillusionment!...

114
00:24:08,607 --> 00:24:10,723
Is Madame S�verine Serizy in ?

115
00:24:10,887 --> 00:24:12,445
Madame has gone.

116
00:24:13,007 --> 00:24:13,996
Gone?

117
00:24:14,167 --> 00:24:16,317
Yes, she's gone on a long trip.

118
00:24:16,487 --> 00:24:17,681
And where did she go?

119
00:24:17,847 --> 00:24:20,281
That I don't know
Madame didn't say.

120
00:26:28,847 --> 00:26:31,680
I've been wondering about
what you said yesteday.

121
00:26:31,847 --> 00:26:34,156
It goes beyond anything I've heard.

122
00:26:34,807 --> 00:26:38,800
Sorry if I'm being forward...

123
00:26:41,527 --> 00:26:44,121
Not at all.
I'm even glad to hear it.

124
00:26:44,447 --> 00:26:48,326
Were you the best friend
of that woman's husband?

125
00:26:49,527 --> 00:26:52,041
And you had sex with her?

126
00:26:53,087 --> 00:26:55,282
It doesn't matter who it was.

127
00:26:55,487 --> 00:26:57,205
What matters is that it happened.

128
00:26:57,967 --> 00:27:00,720
If the best friend
did have sex with her,

129
00:27:00,887 --> 00:27:03,765
it would have been the finest betrayal

130
00:27:04,007 --> 00:27:06,840
she could ever have wished on him...
because she loved her husband.

131
00:27:07,567 --> 00:27:10,001
But I don't think it came to that.

132
00:27:10,847 --> 00:27:16,241
But if she didn't do it physically,
she imagined it,

133
00:27:16,407 --> 00:27:17,601
with all three present.

134
00:27:18,447 --> 00:27:19,960
In the finest way.

135
00:27:21,087 --> 00:27:23,157
You say "the finest"!?

136
00:27:24,727 --> 00:27:28,201
The finest, to her taste,

137
00:27:28,367 --> 00:27:32,246
the pleasure of doing it with the best
friend of the husband she truly loved.

138
00:27:33,087 --> 00:27:34,440
And in his presence.

139
00:27:36,327 --> 00:27:39,000
She'd do that because
she loved her husband!?

140
00:27:39,207 --> 00:27:43,485
That's exactly what happened,
because doing it in total secrecy

141
00:27:43,647 --> 00:27:48,277
doubled her perverted
masochistic pleasure.

142
00:27:50,167 --> 00:27:54,718
Later, she'd enjoy remembering
these betrayals,

143
00:27:55,727 --> 00:28:00,039
while she tenderly caressed
her beloved husband.

144
00:28:01,087 --> 00:28:02,725
Do you mean...

145
00:28:03,327 --> 00:28:08,676
that her husband's best friend
was just an instrument to her?

146
00:28:08,887 --> 00:28:09,797
Precisely.

147
00:28:10,687 --> 00:28:12,518
Perfect, Mr Barman!

148
00:28:12,847 --> 00:28:15,998
If 'perfect' means what I think,

149
00:28:16,167 --> 00:28:18,840
I'd hardly say it was that.

150
00:28:19,167 --> 00:28:21,837
-Are you surprised?
-Very.

151
00:28:22,167 --> 00:28:25,045
It makes me think
there are men who like that!

152
00:28:25,487 --> 00:28:26,602
Like what?

153
00:28:26,807 --> 00:28:31,244
Subjecting themselves
to these perversities,

154
00:28:31,407 --> 00:28:35,397
or even to act that way
themselves, why not admit it?

155
00:28:37,527 --> 00:28:39,245
I agree,

156
00:28:39,767 --> 00:28:41,359
but that's not the case with me.

157
00:28:42,487 --> 00:28:45,718
So where are you in all this?

158
00:28:46,087 --> 00:28:47,281
My role...

159
00:28:50,167 --> 00:28:51,395
is to observe,

160
00:28:51,887 --> 00:28:53,684
even to challenge.

161
00:28:55,527 --> 00:28:58,246
But isn't that a kind of sadism too?

162
00:28:59,047 --> 00:29:01,686
No. It's self-defence.

163
00:29:02,567 --> 00:29:04,922
How is it self-defence?

164
00:29:06,367 --> 00:29:10,242
So you were involved after all?

165
00:29:10,447 --> 00:29:13,120
No, no.
It was a figure of speech.

166
00:29:14,447 --> 00:29:17,803
She knew that this best friend
knew all about her,

167
00:29:18,487 --> 00:29:20,955
and she knew
he could defend himself.

168
00:29:21,127 --> 00:29:24,915
She was in a sticky situation with him.

169
00:29:25,087 --> 00:29:30,559
He represented
the conscience she didn't have.

170
00:29:31,287 --> 00:29:35,758
He was even the one who gave her
directions to that brothel,

171
00:29:35,967 --> 00:29:39,596
so sure was he that she'd enjoy
to give herself to strangers.

172
00:29:40,887 --> 00:29:42,526
Some customers tell me

173
00:29:42,687 --> 00:29:45,484
that in their youth
there were a lot of those houses.

174
00:29:45,647 --> 00:29:49,242
They say things
have changed a lot since then.

175
00:29:49,447 --> 00:29:51,961
They even complain about
the mentality of people these days.

176
00:29:52,127 --> 00:29:55,642
That the values of their day
have been overthrown.

177
00:29:56,727 --> 00:30:00,561
Now, out of curiosity:

178
00:30:00,887 --> 00:30:03,685
did you go to those houses?

179
00:30:03,847 --> 00:30:05,007
Me? No.

180
00:30:05,007 --> 00:30:06,467
But those houses were well-known.

181
00:30:06,567 --> 00:30:10,560
And the friend figured
she'd follow his directions.

182
00:30:11,207 --> 00:30:13,437
-And did she?
-Oh yes!

183
00:30:14,327 --> 00:30:16,761
and one day he went along.

184
00:30:16,927 --> 00:30:19,760
-The husband's best friend?
-Yes.

185
00:30:20,327 --> 00:30:21,362
And she was there?

186
00:30:22,567 --> 00:30:24,717
When she saw him, she was worried.

187
00:30:25,567 --> 00:30:30,118
And did he have sex with her?

188
00:30:30,287 --> 00:30:32,847
He didn't want sex.

189
00:30:33,247 --> 00:30:37,638
She was afraid he'd spill the beans
to her husband.

190
00:30:38,127 --> 00:30:40,687
Her sado-masochism
would lose all its allure

191
00:30:40,847 --> 00:30:42,678
if her husband found out.

192
00:30:43,607 --> 00:30:45,802
-You understand?
-No.

193
00:30:47,207 --> 00:30:50,517
Hiding her perversion from her husband
gave her masochism its spice.

194
00:30:50,687 --> 00:30:55,363
Like hiding one's appetite
for the forbidden fruit from God.

195
00:30:57,767 --> 00:30:59,280
It worked like a vice;

196
00:31:00,407 --> 00:31:03,126
the bigger the secret,
the better the taste.

197
00:31:04,247 --> 00:31:07,603
Benedito, won't you
introduce us to this gentleman?

198
00:31:07,767 --> 00:31:10,486
This gentleman is here
on important business.

199
00:31:10,647 --> 00:31:13,207
He's here to get some
important information.

200
00:31:13,367 --> 00:31:16,200
Go back to your table
and order what you'd like.

201
00:31:16,367 --> 00:31:17,277
Thanks.

202
00:31:17,447 --> 00:31:21,486
And if you have time
come over and join us...

203
00:31:22,887 --> 00:31:24,036
Sweety.

204
00:31:28,447 --> 00:31:30,483
-Put it on my bill.
-Certainly, sir.

205
00:31:33,847 --> 00:31:36,680
So, girls, what would you like?

206
00:31:36,847 --> 00:31:40,044
A good-looking boy, like you...

207
00:31:41,887 --> 00:31:44,526
A whisky and an orange juice.

208
00:31:44,687 --> 00:31:46,006
The usual.

209
00:31:46,287 --> 00:31:48,755
If you know already...
why are you asking, darling?

210
00:31:48,927 --> 00:31:50,997
You'll see...

211
00:32:01,527 --> 00:32:04,357
Fine, fine...

212
00:32:07,447 --> 00:32:11,202
Here, your consolation.

213
00:32:11,487 --> 00:32:13,603
Isn't that what you wanted?

214
00:32:13,767 --> 00:32:18,716
What I want is a fit boy, like you...

215
00:32:19,367 --> 00:32:22,404
We appreciate quality, not quantity.

216
00:32:22,567 --> 00:32:26,242
Here you are, then... I'll serve you
with quantity and quality.

217
00:32:26,407 --> 00:32:30,600
You see, girl, we may just drink
till the end of our thirst.

218
00:32:31,447 --> 00:32:33,722
You know you're a sweet boy?

219
00:32:34,847 --> 00:32:37,361
Leave the boy alone - he's a pain!

220
00:32:37,527 --> 00:32:38,880
My pleasure

221
00:32:40,087 --> 00:32:41,998
is to serve... girls.

222
00:32:43,167 --> 00:32:44,885
You see, he likes us...

223
00:32:47,087 --> 00:32:49,999
If you knew what I've been hearing...

224
00:32:50,647 --> 00:32:54,925
If you knew what he's been
telling me... him and the others too.

225
00:32:55,087 --> 00:32:56,156
God!...

226
00:32:57,247 --> 00:32:59,158
You know what I'll tell you?

227
00:32:59,767 --> 00:33:01,120
You two?... You're angels!

228
00:33:01,967 --> 00:33:03,480
I've got to go...

229
00:33:04,727 --> 00:33:06,080
Are they prostitutes?

230
00:33:11,207 --> 00:33:12,322
Yes.

231
00:33:13,167 --> 00:33:15,362
But compared to the stories I hear,

232
00:33:15,647 --> 00:33:17,198
I'd sooner say they're...

233
00:33:17,198 --> 00:33:18,798
angels...

234
00:33:20,407 --> 00:33:24,286
They don't have husbands to cheat on,
or hide secrets from!

235
00:33:25,647 --> 00:33:29,003
That one who wanted to cheat on her
husband with her best friend,

236
00:33:29,167 --> 00:33:31,237
she had endless secrets.

237
00:33:31,407 --> 00:33:34,519
These poor souls are what they are,
they do business with their bodies,

238
00:33:34,719 --> 00:33:36,519
they don't delude anybody.

239
00:33:36,967 --> 00:33:39,686
You're a lot sharper
than you let on.

240
00:33:40,727 --> 00:33:42,285
It's not wisdom...

241
00:33:42,447 --> 00:33:46,004
it's because I listen.

242
00:33:46,807 --> 00:33:48,206
To confessions, eh?

243
00:33:49,367 --> 00:33:51,483
You can't imagine what I hear.

244
00:33:51,727 --> 00:33:55,766
I makes me think there are men
who really like perverse women...

245
00:33:55,927 --> 00:34:01,801
who mix the pleasure of cheating
on the husband, or lover, or lovers,

246
00:34:02,367 --> 00:34:04,961
with the thrill of the secret.

247
00:34:05,567 --> 00:34:08,886
-Secrets that are theirs alone.
-True

248
00:34:10,087 --> 00:34:13,238
But they get furious
when they're found out.

249
00:34:13,407 --> 00:34:15,602
They like to play innocent.

250
00:34:15,807 --> 00:34:18,799
What did he mean, "you're angels"?

251
00:34:19,607 --> 00:34:22,597
What did he mean?
That that's what we are:

252
00:34:23,367 --> 00:34:25,483
little angels!

253
00:34:28,247 --> 00:34:30,602
It's funny that you, so young,
are so wise already.

254
00:34:32,007 --> 00:34:34,077
It's what I hear.

255
00:34:34,887 --> 00:34:37,082
They feel a need to unburden themselves.

256
00:34:37,247 --> 00:34:42,480
But never to someone they know,
and even less to a friend.

257
00:34:42,647 --> 00:34:46,560
They confess to someone who listens
without hearing them,

258
00:34:46,727 --> 00:34:50,879
to a stranger,
or someone neutral like me,

259
00:34:51,207 --> 00:34:54,040
who doesn't know them
or won't see them again.

260
00:34:54,887 --> 00:34:59,326
It's as if they're talking to a wall,
or into a well.

261
00:34:59,527 --> 00:35:04,078
That way, they're sure
their confessions will die there.

262
00:35:04,487 --> 00:35:08,719
So they open up to me,
because they take me for deaf.

263
00:35:09,367 --> 00:35:12,120
So you see, I know without knowing.

264
00:35:12,687 --> 00:35:14,006
It's my destiny.

265
00:35:14,647 --> 00:35:15,966
Mine too.

266
00:35:16,607 --> 00:35:18,359
Do I know who you are?

267
00:35:18,687 --> 00:35:20,564
Or do you know who I am?

268
00:35:21,687 --> 00:35:26,397
After all, I only came in here
for a large whisky.

269
00:35:26,567 --> 00:35:27,682
With no ice.

270
00:35:29,087 --> 00:35:30,076
Here you go.

271
00:35:37,847 --> 00:35:39,917
He's ignoring us.

272
00:35:40,567 --> 00:35:42,637
He's no spring chicken anyway...

273
00:35:42,927 --> 00:35:44,042
Him?

274
00:35:44,567 --> 00:35:46,762
His best days might be gone...

275
00:35:46,927 --> 00:35:48,963
But he's sweet and good-looking.

276
00:35:49,127 --> 00:35:50,640
I even fancy some guys like that.

277
00:35:51,287 --> 00:35:54,404
There you go then! And I fancy
guys like that little barman.

278
00:35:54,567 --> 00:35:56,398
I already know that, girl!

279
00:35:57,247 --> 00:35:58,726
But don't kid yourself.

280
00:35:58,887 --> 00:36:01,321
A young boy like that
can have any woman he wants.

281
00:36:01,487 --> 00:36:04,047
-So he doesn't know what he's missing.
-I know.

282
00:36:04,207 --> 00:36:06,562
They say you're
something special in bed.

283
00:36:06,727 --> 00:36:08,080
The problem is getting them there.

284
00:36:08,080 --> 00:36:09,880
Yeah.

285
00:36:10,207 --> 00:36:13,358
But you've always helped me
to get them interested.

286
00:36:14,167 --> 00:36:15,805
Well, I'm your friend, aren't I?

287
00:36:16,367 --> 00:36:19,325
You're a good girl. So you are.

288
00:36:19,487 --> 00:36:22,047
It's my duty.
Well, well!

289
00:39:31,527 --> 00:39:33,882
-Shall I open the champagne?
-No, thank you.

290
00:40:40,647 --> 00:40:43,400
-Open the champagne.
-Yes, sir.

291
00:41:56,167 --> 00:41:57,805
As beautiful as ever!... my darling.

292
00:41:58,407 --> 00:42:00,079
What do you mean, "my darling"?

293
00:42:00,287 --> 00:42:01,800
I was never your darling.

294
00:42:01,967 --> 00:42:06,119
"Darling", only for your timeless beauty!
To your beauty!

295
00:42:07,607 --> 00:42:09,643
You're being too kind.

296
00:42:10,687 --> 00:42:12,837
Even so, to our health.

297
00:42:15,807 --> 00:42:19,482
We're not like we were years ago.
But we're both in good health.

298
00:42:19,647 --> 00:42:22,525
-So it seems...
-To our health.

299
00:42:27,567 --> 00:42:28,636
I shouldn't have come.

300
00:42:28,807 --> 00:42:32,561
But you did, and rather late.

301
00:42:32,847 --> 00:42:33,996
Yes, that's true.

302
00:42:34,607 --> 00:42:35,835
But I'm here now.

303
00:42:36,007 --> 00:42:37,918
Don't be so worried.

304
00:42:38,087 --> 00:42:40,555
There's nothing odd about this meeting.
The past is long gone.

305
00:42:40,727 --> 00:42:43,525
Long gone!

306
00:42:44,127 --> 00:42:45,879
That wonderful time
when we were young.

307
00:42:46,167 --> 00:42:48,397
living life our own way.

308
00:42:48,807 --> 00:42:50,957
And problems are part of life.

309
00:42:53,487 --> 00:42:55,045
You have regrets?

310
00:42:56,167 --> 00:42:59,842
Your husband had
his share of happiness.

311
00:43:00,007 --> 00:43:00,800
You gave him

312
00:43:00,800 --> 00:43:01,360
plenty of that.

313
00:43:06,527 --> 00:43:08,245
What's troubling you now?

314
00:43:08,687 --> 00:43:09,756
Everything...

315
00:43:10,087 --> 00:43:11,486
Quite everything.

316
00:43:12,447 --> 00:43:13,516
Nothing.

317
00:43:15,287 --> 00:43:19,280
An ill-spent past,
old age as a future...

318
00:43:20,527 --> 00:43:22,836
and the impossibility of putting right

319
00:43:23,367 --> 00:43:26,484
things we wish we'd done differently.

320
00:44:06,767 --> 00:44:08,758
I'll stay with the champagne,
thank you.

321
00:44:14,327 --> 00:44:17,956
A large whiskey, o ice.

322
00:44:42,367 --> 00:44:44,323
Leave the bottle, please.

323
00:49:23,807 --> 00:49:27,243
-Anything else, sir?
-No, thanks.

324
00:49:27,727 --> 00:49:30,400
Leave us now, please.

325
00:49:30,567 --> 00:49:32,637
Shall I change the candles?

326
00:49:32,807 --> 00:49:35,002
No...
It's fine like this.

327
00:49:57,327 --> 00:50:01,161
Would you mind
if we just have the candlelight?

328
00:50:01,327 --> 00:50:04,797
I think we'd be cosier like that,
more relaxed.

329
00:50:04,967 --> 00:50:07,037
More relaxed for what?

330
00:50:07,847 --> 00:50:09,644
For whatever we want.

331
00:50:09,807 --> 00:50:12,526
To recall our wickedness, for example.

332
00:50:12,687 --> 00:50:15,761
-May I?
-As you wish.

333
00:50:16,447 --> 00:50:17,846
It's all the same to me.

334
00:50:21,887 --> 00:50:24,879
You were right,
when you said wickedness.

335
00:50:25,647 --> 00:50:29,640
I see you keep it as a memorial
to all that wicked past.

336
00:50:30,007 --> 00:50:32,441
And that "memorial"...
is all I have left these days.

337
00:50:32,687 --> 00:50:33,802
Good,

338
00:50:34,807 --> 00:50:38,163
so you'll also remember
what I'm dying to know about the past...

339
00:50:38,647 --> 00:50:40,399
that weighs so heavily on me.

340
00:50:41,567 --> 00:50:44,559
You feel persecuted by sins!?

341
00:50:44,967 --> 00:50:46,082
Worse than that.

342
00:50:46,967 --> 00:50:49,959
It's the terrible
state of doubt I'm in!...

343
00:50:57,527 --> 00:51:00,325
Don't tell me
you've become a religious woman!

344
00:51:00,487 --> 00:51:02,876
I don't know if it's religion.

345
00:51:03,047 --> 00:51:07,723
I know, strange as it may seem to you,
I'm a different woman now.

346
00:51:08,047 --> 00:51:09,446
A religious woman!

347
00:51:09,927 --> 00:51:11,918
If you want to call it that...

348
00:51:12,247 --> 00:51:15,603
I think I might even end up
in some convent.

349
00:51:16,687 --> 00:51:18,086
As a nun?...

350
00:51:18,847 --> 00:51:22,044
I don't know yet
if I'll become a nun...

351
00:51:24,047 --> 00:51:26,356
-What?
-But why not?

352
00:51:26,727 --> 00:51:29,719
I never thought
you had such a vocation.

353
00:51:29,887 --> 00:51:31,764
Frankly, I'm amazed
to hear you say it!

354
00:51:31,927 --> 00:51:33,724
There are some other things
a lot more scandalous.

355
00:51:33,887 --> 00:51:34,797
What things, can you tell me?

356
00:51:34,967 --> 00:51:36,844
Our past lives, for example.

357
00:51:39,647 --> 00:51:44,402
To me, women were always
Nature's greatest enigma.

358
00:51:47,007 --> 00:51:48,235
Well.

359
00:51:51,407 --> 00:51:53,716
I want you to understand something:

360
00:51:54,607 --> 00:51:57,963
I'm not the woman you knew years ago.

361
00:51:58,127 --> 00:52:01,039
The one who needed
to be in love with one man

362
00:52:01,207 --> 00:52:03,767
in order to have sex with another man.

363
00:52:04,807 --> 00:52:08,766
And I really want you to understand
that I'm another person now

364
00:52:08,927 --> 00:52:12,044
and not the woman you knew
all those years ago.

365
00:52:12,607 --> 00:52:17,681
Maybe you're in denial
of your exotic past.

366
00:52:18,207 --> 00:52:19,117
No.

367
00:52:19,767 --> 00:52:21,837
I was fine,

368
00:52:22,527 --> 00:52:25,997
because my strange desires
didn't make me feel guilty.

369
00:52:27,087 --> 00:52:30,363
We're educated
in the Judaic-Christian system

370
00:52:30,647 --> 00:52:33,445
But I didn't feel capable
of following that system.

371
00:52:33,607 --> 00:52:36,280
Having children, for example.

372
00:52:39,407 --> 00:52:43,036
My sexuality was a kind of catharsis,

373
00:52:43,247 --> 00:52:48,756
a need to experiment with
all the perversions that gripped me...

374
00:52:50,007 --> 00:52:52,596
like a vice
from which there's no escape.

375
00:52:53,687 --> 00:52:58,916
The secret was your devotion
to your husband.

376
00:53:00,527 --> 00:53:02,006
Something was lost.

377
00:53:02,567 --> 00:53:06,116
You were widowed,
I don't think you got married again...

378
00:53:06,447 --> 00:53:12,247
I understand you. I'm like you,
in a way. I became an alcoholic.

379
00:53:12,247 --> 00:53:16,243
I've made alcohol my convent.

380
00:53:17,927 --> 00:53:20,361
The saints were sinners too.

381
00:53:21,567 --> 00:53:23,558
Even terrible sinners.

382
00:53:27,087 --> 00:53:31,396
Our lives will go out one day too,
like the flames of these candles.

383
00:53:31,567 --> 00:53:33,205
But before that happens...

384
00:53:33,727 --> 00:53:36,878
Tell me, are you really
going to a convent?

385
00:53:38,607 --> 00:53:40,563
Before that happens,

386
00:53:41,967 --> 00:53:46,199
I want you to tell me, like we
agreed yesterday in the street,

387
00:53:46,527 --> 00:53:49,166
what I'm dying to know.

388
00:53:52,367 --> 00:53:55,086
And as for the convent,
I want to tell you this:

389
00:53:55,487 --> 00:53:57,478
my youth has gone,

390
00:53:57,647 --> 00:54:00,957
along with a lot of my
unbalanced sexuality.

391
00:54:02,927 --> 00:54:07,045
I don't have any children,
I don't think I was born for that.

392
00:54:08,047 --> 00:54:11,562
The husband I loved,
who was my only love,

393
00:54:11,847 --> 00:54:15,999
and the only pretext
for my twisted masochism, is gone,

394
00:54:16,167 --> 00:54:17,654
it's all gone,

395
00:54:17,654 --> 00:54:20,204
just as these candles are going.

396
00:54:21,527 --> 00:54:25,279
That fire no longer burns in me.

397
00:54:27,207 --> 00:54:28,879
I'm a widow, and alone.

398
00:54:30,047 --> 00:54:31,036
Alone

399
00:54:31,887 --> 00:54:33,161
with my soul.

400
00:54:34,407 --> 00:54:36,566
It asks me for shelter.
Tell me:

401
00:54:36,967 --> 00:54:40,445
what better shelter could I give
my soul than a convent?

402
00:54:42,247 --> 00:54:46,525
But for my soul to deserve
to be truly at rest,

403
00:54:47,127 --> 00:54:49,641
I've got to know...

404
00:54:53,047 --> 00:54:54,162
To know...

405
00:54:54,607 --> 00:54:57,127
...if I told your husband

406
00:54:57,527 --> 00:55:00,439
the secrets I knew about you.

407
00:55:00,847 --> 00:55:02,963
You know that's exactly
what I want to know.

408
00:55:04,087 --> 00:55:08,438
To know if you told him my secrets,
which only you knew.

409
00:55:08,927 --> 00:55:12,044
I never saw you again,
after your last visit.

410
00:55:12,487 --> 00:55:15,838
And when I looked at my husband,

411
00:55:16,887 --> 00:55:19,401
I saw on his cheek

412
00:55:19,607 --> 00:55:21,635
stuck there, half way...

413
00:55:22,035 --> 00:55:24,635
a tear,
as it frozen,

414
00:55:25,447 --> 00:55:28,598
with an iron resistance,
on his expressionless face.

415
00:55:33,207 --> 00:55:37,405
If you don't tell me
what you said to him about me,

416
00:55:37,567 --> 00:55:40,081
I'll never know
the reason for that tear.

417
00:55:43,007 --> 00:55:45,077
Don't forget that you promissed me.

418
00:55:48,447 --> 00:55:52,326
And now that
the candles have gone out,

419
00:55:52,767 --> 00:55:55,235
it's the perfect time
to tell me everything,

420
00:55:55,687 --> 00:55:57,086
to confess everything to me.

421
00:55:59,607 --> 00:56:01,006
I need to know.

422
00:56:03,247 --> 00:56:05,681
I need to know, Mr Husson.

423
00:56:08,407 --> 00:56:11,843
My dear S�verine,
before I do what you ask,

424
00:56:14,127 --> 00:56:15,845
I have a surprise for you.

425
00:56:20,447 --> 00:56:22,085
A surprise?

426
00:56:22,527 --> 00:56:23,755
What surprise?

427
00:56:23,927 --> 00:56:26,519
Do you remember the little box

428
00:56:27,047 --> 00:56:32,165
that a certain oriental man opened,

429
00:56:32,687 --> 00:56:36,237
that made you very curious?

430
00:56:44,847 --> 00:56:47,156
That doesn't interest me any more,
it just annoys me.

431
00:56:48,087 --> 00:56:49,486
It was so long ago!

432
00:57:21,527 --> 00:57:25,759
Whether it interests you or annoys you,
I found this artefact by chance...

433
00:57:26,327 --> 00:57:27,646
Do you know where?

434
00:57:28,767 --> 00:57:31,281
In the shop where we met.

435
00:57:32,767 --> 00:57:37,204
This precious object, that the
young S�verine loved so much.

436
00:57:38,087 --> 00:57:40,806
Well, I'll keep it for my own use.

437
00:57:41,167 --> 00:57:43,761
Nobody's stopping you.

438
00:57:44,687 --> 00:57:46,882
But once and for all, believe me:

439
00:57:48,007 --> 00:57:49,486
That past doesn't matter
to me any more.

440
00:57:49,647 --> 00:57:53,560
Or have you still not grasped that
you're talking to another person now?

441
00:57:56,687 --> 00:57:58,996
So you don't care any more
about what I said to your husband?

442
00:57:59,767 --> 00:58:01,917
Quite the opposite.
That's all I want to know.

443
00:58:02,087 --> 00:58:05,159
Isn't that why we're here?

444
00:58:05,407 --> 00:58:08,001
All right then, here it is.

445
00:58:08,727 --> 00:58:12,163
But you have to think carefully
about what I'm going to say.

446
00:58:12,807 --> 00:58:15,560
Either I told your husband everything,

447
00:58:16,167 --> 00:58:18,397
or I told him nothing of what I knew.

448
00:58:19,927 --> 00:58:21,565
Now, you tell me...

449
00:58:23,407 --> 00:58:28,124
Which of these two confessions
is truthful, and which is a lie?

450
00:58:36,524 --> 00:58:38,124
S�verine!

451
00:59:15,407 --> 00:59:19,525
I've brought some more candles
to brighten up your dinner tonight.

452
00:59:27,447 --> 00:59:29,000
Would you like anything else,
Mr Husson?

453
00:59:29,000 --> 00:59:30,100
No thank you.

454
00:59:30,447 --> 00:59:33,405
-Shall I put the lights on?
-If you like.

455
01:00:11,327 --> 01:00:14,160
If there's nothing else,
may I leave?

456
01:00:14,727 --> 01:00:17,036
Of course.

457
01:00:19,687 --> 01:00:21,439
The lady's purse.

458
01:00:22,687 --> 01:00:25,247
The lady forgot her purse.

459
01:00:30,007 --> 01:00:31,599
-This is for you.
-Thank you.

460
01:00:33,447 --> 01:00:36,280
We thank you, sir.

461
01:00:51,687 --> 01:00:54,838
-What a strange guy.
-Yeah, strange guy.

462
01:01:27,567 --> 01:01:30,240
Yes, sir.
He's an oddball.

463
01:01:31,127 --> 01:01:32,845
He's a weirdo.

