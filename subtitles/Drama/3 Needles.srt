1
00:01:25,185 --> 00:01:26,311
- Now you are a man.
- Now I am a man.

2
00:01:43,236 --> 00:01:44,430
Now you are a man.

3
00:01:50,443 --> 00:01:52,536
- Now you are a man.
- Now I am a man.

4
00:02:12,966 --> 00:02:16,493
My dick feels like he bit it off.

5
00:02:18,004 --> 00:02:21,064
Shut up and relax, Huku.
You have to wait until it heals.

6
00:02:21,441 --> 00:02:24,171
You'll be someone totally new.

7
00:02:24,377 --> 00:02:26,140
We all will.

8
00:05:38,137 --> 00:05:39,627
Got a smoke?

9
00:05:56,689 --> 00:05:59,021
- Hi.
- What are you carrying?

10
00:05:59,292 --> 00:06:00,816
Just a shipment of rape seeds.

11
00:06:01,260 --> 00:06:02,818
To Yawlou.

12
00:06:03,463 --> 00:06:04,589
Show me.

13
00:06:04,731 --> 00:06:05,823
Okay.

14
00:06:36,562 --> 00:06:38,189
Take it out.

15
00:06:52,512 --> 00:06:54,173
Careful!

16
00:06:54,914 --> 00:06:56,313
Rape seeds?

17
00:06:56,449 --> 00:06:57,643
Yeah.

18
00:06:58,918 --> 00:07:01,182
I shouldn't open it then --

19
00:07:01,421 --> 00:07:04,015
- or they'll spill all over the place.
- Yeah.

20
00:07:06,459 --> 00:07:08,825
- Just make a couple of peepholes--
- Wait!

21
00:07:15,835 --> 00:07:19,362
Now I'm thinking maybe
we should open it after all.

22
00:07:45,898 --> 00:07:49,334
Die! Die! Die!

23
00:07:51,971 --> 00:07:52,869
Die!

24
00:07:53,940 --> 00:07:55,407
Die!

25
00:07:55,675 --> 00:07:56,937
Die!

26
00:08:07,420 --> 00:08:09,615
Anybody else?

27
00:08:16,762 --> 00:08:17,922
Die!

28
00:08:23,803 --> 00:08:25,031
No thank you.

29
00:08:27,607 --> 00:08:29,507
So-- who do you work for?

30
00:08:32,879 --> 00:08:35,313
Who do you sell to?

31
00:08:38,751 --> 00:08:42,778
You're illegally running blood.

32
00:08:44,023 --> 00:08:48,392
You need to give me a reason
not to lock you away forever.

33
00:18:03,482 --> 00:18:07,441
There's a pressing need
for blood products here and abroad.

34
00:18:07,719 --> 00:18:13,123
Gamma globulin, clotting factor
and other medicines...

35
00:18:13,592 --> 00:18:17,289
...are necessary for even the simplest surgeries.

36
00:18:17,529 --> 00:18:21,226
And sick people rely
on these products every day.

37
00:18:21,533 --> 00:18:29,440
The Provincial Health Office
has urged everyone to give.

38
00:18:35,080 --> 00:18:38,379
To reward the selfless act
of giving...

39
00:18:38,684 --> 00:18:43,144
...the company will compensate
you each with five dollars.

40
00:18:49,394 --> 00:18:51,760
People can give at the same time.

41
00:18:53,332 --> 00:18:55,698
The blood is taken by sterile needle.

42
00:18:56,235 --> 00:18:59,068
We can fit four samples
in a centrifuge.

43
00:18:59,171 --> 00:19:01,071
The centrifuge spins the blood

44
00:19:01,240 --> 00:19:03,606
and separates plasma
from red blood cells.

45
00:19:04,243 --> 00:19:07,542
The combined red cells
which the company doesn't need-

46
00:19:07,679 --> 00:19:10,239
-are split up
and returned to the donors.

47
00:19:23,929 --> 00:19:25,055
I'm Tong Sam. Hi.

48
00:19:25,164 --> 00:19:26,426
My wife.

49
00:19:29,468 --> 00:19:31,333
My daughter- Qi.

50
00:19:32,070 --> 00:19:34,538
Too young. She needs to be twelve.

51
00:19:35,174 --> 00:19:37,233
Fine, I'm twelve-and-a-half.

52
00:19:38,076 --> 00:19:40,704
We can't take you either.

53
00:19:43,115 --> 00:19:45,447
I've been waiting for hours.

54
00:19:46,752 --> 00:19:49,983
Come on, cut me a break.

55
00:19:50,756 --> 00:19:53,623
Sorry. You're obviously not healthy.

56
00:19:54,126 --> 00:19:56,594
It's not up to me, its policy.
For safety.

57
00:19:57,095 --> 00:19:59,757
People who get your blood
could get sick.

58
00:20:00,065 --> 00:20:01,464
It's just a cough.

59
00:20:01,767 --> 00:20:04,463
You'll buy Huoang's blood
but not mine?

60
00:20:04,636 --> 00:20:07,503
His is fifty-percent booze. People
who get his blood will get drunk.

61
00:20:07,639 --> 00:20:10,233
Look, I just hired your neighbor
Zaohao as a collector.

62
00:20:11,109 --> 00:20:14,044
Come back in a few weeks.

63
00:20:39,805 --> 00:20:41,170
Hello.

64
00:20:41,506 --> 00:20:42,598
How are you?

65
00:20:42,741 --> 00:20:44,333
I'm alive.

66
00:20:44,676 --> 00:20:48,942
I've reduced the afternoon meal
to 20 minutes...

67
00:20:49,047 --> 00:20:53,245
and reinstated the 15 hour day.

68
00:20:53,785 --> 00:20:55,218
You bastard.

69
00:20:56,355 --> 00:20:58,084
Recover quickly.

70
00:20:59,224 --> 00:21:03,388
Or I'll take away all the perks
you've won in the last 8 years.

71
00:21:07,299 --> 00:21:08,561
Sweets?

72
00:21:09,101 --> 00:21:11,069
No. Marijuana.

73
00:21:12,037 --> 00:21:14,198
Stay well, friend.

74
00:22:38,623 --> 00:22:42,423
Why am I paying you?

75
00:22:43,061 --> 00:22:47,760
You're not to come
to work if you're sick.

76
00:22:53,105 --> 00:22:56,131
I'm telling you it will pass.

77
00:22:59,344 --> 00:23:03,474
This girl
must have been lying to you.

78
00:23:03,749 --> 00:23:04,738
Careful!

79
00:23:07,786 --> 00:23:12,985
She probably sleeps around a lot

80
00:23:14,126 --> 00:23:18,756
and that's why you're sick.

81
00:23:19,631 --> 00:23:22,657
Times have changed, Bongile.

82
00:23:22,801 --> 00:23:25,599
they all found Christ...

83
00:23:27,072 --> 00:23:31,475
...and now you can't trust a woman
to be honest about her virtue.

84
00:27:15,567 --> 00:27:16,659
Who's winning?

85
00:33:49,660 --> 00:33:51,719
I need a new job.

86
00:33:52,930 --> 00:33:55,524
You got the best job in the world,
buddy.

87
00:33:57,035 --> 00:34:00,402
Ah, I told your mother
not to raise you catholic.

88
00:37:06,257 --> 00:37:09,283
We have some kind of problem.

89
00:37:10,194 --> 00:37:12,685
Your blood sample today.

90
00:39:43,881 --> 00:39:45,542
OUTBREAK!

91
00:42:10,027 --> 00:42:12,257
Is that the Mother?

92
00:42:12,730 --> 00:42:16,632
I'm surprised she would show
her face in church.

93
00:42:17,835 --> 00:42:21,498
None of them have any shame.

94
00:43:09,954 --> 00:43:14,516
they've really gone
to town this year.

95
00:43:14,825 --> 00:43:17,760
Everyone else has money to spend.

96
00:44:18,222 --> 00:44:20,053
I feel fine now.

97
00:44:20,190 --> 00:44:22,420
the lady you work for told me
to come back when I felt better.

98
00:44:22,559 --> 00:44:24,857
Well, I have no protocol
for changing your status.

99
00:44:24,995 --> 00:44:27,759
When Miss Jin comes back,
I'll ask her.

100
00:44:28,565 --> 00:44:29,862
When's that?

101
00:44:31,068 --> 00:44:33,298
Definitely within 6 months.
No longer.

102
00:44:33,504 --> 00:44:35,972
Everybody in town is making money
from you except me!

103
00:44:36,240 --> 00:44:38,105
Give me a break, Zaohao...
it's New Year.

104
00:44:38,242 --> 00:44:40,267
I need to clear my debts.

105
00:44:41,645 --> 00:44:42,703
Sorry, Sam.

106
00:44:44,948 --> 00:44:46,711
Fine. Do her then.

107
00:44:48,686 --> 00:44:49,653
She's not old enough.

108
00:44:49,720 --> 00:44:51,347
She's twelve.

109
00:44:52,389 --> 00:44:53,356
Since when?

110
00:44:53,424 --> 00:44:54,618
Since she was eleven.

111
00:44:56,827 --> 00:44:58,795
Why isn't she in junior high?

112
00:44:59,897 --> 00:45:01,694
she's not very bright.

113
00:45:03,367 --> 00:45:04,629
How do you feel?

114
00:45:04,702 --> 00:45:05,669
Good.

115
00:45:06,236 --> 00:45:08,864
- Not dizzy?
- Nope.

116
00:45:09,173 --> 00:45:11,004
Here. You get 10 percent.

117
00:45:13,744 --> 00:45:15,268
It's my blood!

118
00:45:15,746 --> 00:45:20,206
You can spend that.
the rest you invest in our field.

119
00:45:21,085 --> 00:45:24,748
We'll go back every ten days.
Soon can afford seed and fertilizer.

120
00:45:25,355 --> 00:45:27,823
Maybe an ox.

121
00:45:28,125 --> 00:45:30,753
What if I want the money for myself?

122
00:45:30,894 --> 00:45:33,192
then I'll tell them you're only ten.

123
00:45:34,431 --> 00:45:36,228
the ox is mine.

124
00:45:36,667 --> 00:45:37,634
Fine.

125
00:45:38,168 --> 00:45:41,831
Don't tell your mother, okay?

126
00:47:01,051 --> 00:47:04,748
That baby was born too early.

127
00:47:04,922 --> 00:47:07,948
Now he's always going
to be impatient.

128
00:49:04,341 --> 00:49:06,309
she caught your cold.

129
00:49:07,577 --> 00:49:10,045
I'll go to the herbalist.

130
00:49:14,818 --> 00:49:15,750
Hi.

131
00:49:16,086 --> 00:49:17,053
Hi.

132
00:49:17,321 --> 00:49:18,879
Beautiful day.

133
00:49:19,022 --> 00:49:21,286
Yes. I thought I'd take advantage.

134
00:49:24,094 --> 00:49:28,724
I think my wife caught your flu.

135
00:49:28,899 --> 00:49:32,391
My kid's in bed too.
That's why I needed the walk.

136
00:49:33,537 --> 00:49:34,469
Hi.

137
00:49:35,038 --> 00:49:37,233
You wanna buy a bike?

138
00:50:16,013 --> 00:50:22,077
For fever, hatchet needles
pressed with honey.

139
00:50:22,219 --> 00:50:23,345
Ok.

140
00:50:23,420 --> 00:50:24,910
Just a sec.

141
00:50:26,323 --> 00:50:28,291
All sold out.

142
00:50:29,526 --> 00:50:31,016
Well what do you actually have?

143
00:50:31,461 --> 00:50:34,259
I have a bit of orange seed
Echinacea left.

144
00:50:34,464 --> 00:50:36,261
I'll give you what I've got.

145
00:50:54,051 --> 00:50:57,179
This tastes like crap!

146
00:50:58,989 --> 00:51:01,048
If it tastes good it doesn't work.

147
00:52:51,635 --> 00:52:54,035
How did this happen to you?

148
00:52:55,438 --> 00:52:57,133
It's one of the plantation workers,

149
00:52:57,207 --> 00:52:58,401
... they have the virus.

150
00:52:59,242 --> 00:53:02,370
And one sure way to get rid of it
is to pass it on to a virgin.

151
00:53:03,113 --> 00:53:05,172
Who was the man, Undilewe?

152
00:53:55,165 --> 00:53:59,101
Frankly, I have no beliefs to impose.

153
00:53:59,736 --> 00:54:02,364
You exhume their wealth and
resources and leave them for dead.

154
00:54:02,539 --> 00:54:06,168
I'll be here with them long after
your mission's gone home, Father.

155
00:55:26,690 --> 00:55:30,319
Wow...this must be
the best year, ever.

156
00:58:27,537 --> 00:58:29,869
This girl annoys me.

157
00:58:48,958 --> 00:58:52,587
Just do what she wants
and she'll quit nagging you.

158
00:58:53,963 --> 00:58:56,898
I thought nuns were supposed
to be different than other girls.

159
00:59:10,513 --> 00:59:11,571
You gotta be kidding.

160
01:00:33,730 --> 01:00:35,721
Is she rating?

161
01:00:35,832 --> 01:00:37,356
I'm right here.

162
01:00:37,467 --> 01:00:40,527
Why are you asking him?
He doesn't feed me, my mother does.

163
01:00:46,476 --> 01:00:48,706
she seems tired today.

164
01:00:48,778 --> 01:00:51,770
she's fine.
she likes coming to see you.

165
01:00:52,115 --> 01:00:54,083
she gets to buy herself things.

166
01:00:56,452 --> 01:00:58,113
she has her own little job.

167
01:00:59,088 --> 01:01:01,283
I'm right here!

168
01:01:15,471 --> 01:01:19,407
Better seed. Fertilizer.

169
01:01:20,710 --> 01:01:23,975
It's going to be a good season,
thanks to you.

170
01:01:24,514 --> 01:01:27,005
Good. I'm thinking of retiring soon.

171
01:01:33,523 --> 01:01:36,959
It seems there's a way for you
to make money after all.

172
01:01:37,760 --> 01:01:39,660
You'll buy my blood?

173
01:01:40,096 --> 01:01:42,428
I need to transport the shipment
every couple of days.

174
01:01:42,665 --> 01:01:44,064
To Longhu.

175
01:01:44,567 --> 01:01:46,330
I'm too busy at the house.

176
01:01:46,769 --> 01:01:49,761
I thought of you first.

177
01:01:50,673 --> 01:01:51,662
To Longhu?

178
01:01:52,008 --> 01:01:53,873
That's two hours with a cart!

179
01:01:54,077 --> 01:01:56,375
And now you've got a water buffalo.

180
01:01:57,647 --> 01:01:59,706
Two hundred yuan every time.

181
01:03:02,512 --> 01:03:04,139
What do you want?

182
01:03:04,380 --> 01:03:06,075
My father is outside.

183
01:03:19,862 --> 01:03:21,227
Where's my cut?

184
01:03:21,931 --> 01:03:24,058
Sorry, this is MY job.

185
01:03:26,002 --> 01:03:28,630
But you're using MY buffalo.

186
01:04:07,543 --> 01:04:09,340
Let mommy work.

187
01:04:15,484 --> 01:04:17,349
You have to disguise it better.

188
01:04:18,221 --> 01:04:20,086
Here. Look at this.

189
01:04:28,698 --> 01:04:30,222
they peel right off
when they get there.

190
01:04:30,433 --> 01:04:31,957
SPlCY RED SAUCE

191
01:04:37,039 --> 01:04:39,269
You better hurry up.

192
01:06:11,100 --> 01:06:12,431
I couldn't make an appointment.

193
01:06:12,969 --> 01:06:14,061
It's an emergency.

194
01:06:15,171 --> 01:06:17,298
It costs money to see a doctor.

195
01:06:23,212 --> 01:06:25,043
How can you be sure you're sick?

196
01:06:26,215 --> 01:06:29,048
the father. He's sick.

197
01:06:29,518 --> 01:06:31,748
Sick how?

198
01:06:33,289 --> 01:06:35,780
like a skeleton.

199
01:06:39,295 --> 01:06:42,389
I can't give you medication
without a test first.

200
01:06:44,400 --> 01:06:46,197
I'm having contractions.

201
01:06:46,535 --> 01:06:47,968
How far apart?

202
01:06:54,410 --> 01:06:58,107
Six hours, four hours. I don't know.

203
01:06:59,115 --> 01:07:01,208
It started last night.

204
01:07:05,955 --> 01:07:08,446
You can't tell anyone
where you got it.

205
01:07:08,891 --> 01:07:12,019
You take it now...
You give some to the baby afterward.

206
01:07:12,128 --> 01:07:13,993
You can't breast feed.

207
01:07:14,797 --> 01:07:15,786
Okay.

208
01:07:22,071 --> 01:07:24,904
Where are you having this baby?

209
01:10:40,436 --> 01:10:43,098
I need to stay here a few days.

210
01:10:50,879 --> 01:10:53,074
Don't worry about the money.

211
01:10:54,917 --> 01:10:58,853
You'll work it off later.

212
01:12:08,857 --> 01:12:10,722
You selling these?

213
01:12:11,727 --> 01:12:13,558
Tell me -

214
01:12:14,396 --> 01:12:16,557
Are these the baskets
by the old woman on the hill?

215
01:12:16,632 --> 01:12:17,724
they are.

216
01:12:17,866 --> 01:12:19,231
All of them?

217
01:12:19,368 --> 01:12:20,335
How much for the lot?

218
01:12:20,469 --> 01:12:21,936
One-thousand, five-hundred.

219
01:12:22,071 --> 01:12:23,800
Ay, ay, ay.

220
01:12:23,939 --> 01:12:26,806
I would give you one thousand.

221
01:12:28,711 --> 01:12:30,372
I'll take them all.

222
01:12:31,046 --> 01:12:32,638
Ok, wrap them up.

223
01:12:48,097 --> 01:12:49,029
Huku!

224
01:12:49,765 --> 01:12:51,232
Yes, Granny?

225
01:12:51,867 --> 01:12:55,234
It's time for you to became a man.

226
01:14:03,705 --> 01:14:05,263
Granny?

227
01:14:05,974 --> 01:14:07,771
she never woke up since you left.

228
01:19:08,143 --> 01:19:11,044
I've come an awfully long way...

229
01:19:11,379 --> 01:19:14,246
walking for days with these children

230
01:19:14,716 --> 01:19:15,978
...to find our uncle.

231
01:19:16,118 --> 01:19:18,018
- What's his name?
- Philisande.

232
01:19:18,220 --> 01:19:19,619
Oh, I know Philisande.

233
01:19:19,788 --> 01:19:23,087
He hasn't been around here in years.

234
01:19:52,988 --> 01:19:53,920
Mr. Tong?

235
01:19:54,055 --> 01:19:55,818
You must be in shock.

236
01:19:55,957 --> 01:19:58,221
I don't know why they died.

237
01:19:59,928 --> 01:20:02,328
Was there no doctor?

238
01:20:02,764 --> 01:20:04,391
It happened very quickly.

239
01:20:05,133 --> 01:20:08,159
If your family is dead and buried...

240
01:20:08,303 --> 01:20:11,898
...what would you like
this office to do?

241
01:20:12,174 --> 01:20:15,769
Don't you want to know
what happened to them?

242
01:20:16,411 --> 01:20:19,244
Mr. Tong, I am curious,
to be honest.

243
01:20:19,414 --> 01:20:22,008
It could have been dozens
of infections.

244
01:20:22,184 --> 01:20:27,713
But curiosity isn't a reason
to disturb the dead.

245
01:20:28,757 --> 01:20:30,247
You won't do anything?

246
01:20:31,159 --> 01:20:33,252
I don't know what to do.

247
01:20:35,297 --> 01:20:36,525
Mr. Tong?

248
01:20:36,698 --> 01:20:40,725
I really am very sorry.

249
01:21:50,238 --> 01:21:52,069
You're made a lot of people happy,
Zaohao.

250
01:21:52,240 --> 01:21:54,708
You're our employee of the month.

251
01:21:55,043 --> 01:21:57,136
I like to help out.

252
01:22:16,164 --> 01:22:18,632
But there have been
some problems with the blood.

253
01:22:18,800 --> 01:22:21,291
I need to have the equipment
repaired.

254
01:22:21,937 --> 01:22:23,871
I've taken really good care of it!

255
01:22:24,139 --> 01:22:26,039
I clean it every week.

256
01:22:27,409 --> 01:22:28,876
When will it be back?

257
01:22:29,110 --> 01:22:30,702
As soon as we've had it serviced.

258
01:22:31,279 --> 01:22:32,211
Two days?

259
01:22:32,380 --> 01:22:33,347
Two weeks?

260
01:22:37,385 --> 01:22:39,649
I'll be in touch.

261
01:23:21,396 --> 01:23:23,023
Pack up your things.

262
01:23:24,366 --> 01:23:26,095
It's happened again.

263
01:23:28,203 --> 01:23:29,693
What are you doing?

264
01:23:31,873 --> 01:23:33,704
I packed up the van for you.

265
01:23:33,842 --> 01:23:35,901
I want you to drop us off
in Shanghai.

266
01:23:36,311 --> 01:23:38,040
I can't do this anymore.

267
01:23:39,114 --> 01:23:41,105
You're imagining things.

268
01:23:42,784 --> 01:23:44,513
No, we're making people sick.

269
01:23:44,652 --> 01:23:47,177
This is the third time.

270
01:23:47,922 --> 01:23:51,414
- Look at yourself.
- I give a pint a day! That's why.

271
01:23:51,559 --> 01:23:53,322
these people give too often,
that's all.

272
01:23:53,862 --> 01:23:57,923
It's greed, because they're poor
and they're all in debt.

273
01:23:58,266 --> 01:24:00,257
like you are to me.

274
01:24:00,468 --> 01:24:05,428
Just find a new village to set up,
and we'll meet up with you.

275
01:24:05,874 --> 01:24:07,933
We have to leave now.

276
01:24:08,176 --> 01:24:10,303
Before they figure it out.

277
01:24:11,413 --> 01:24:12,641
Come on.

278
01:24:22,290 --> 01:24:23,882
He'll ride with me.

279
01:26:12,667 --> 01:26:14,658
I just spoke
with your provincial rep.

280
01:26:14,836 --> 01:26:17,828
- He remembered your visit.
- He didn't know what to do.

281
01:26:18,006 --> 01:26:19,735
What is your complaint?

282
01:26:19,874 --> 01:26:21,603
My family died.

283
01:26:21,843 --> 01:26:23,674
then your complaint is with Buddha.

284
01:26:24,712 --> 01:26:27,647
Mr. Tong,
why have you come alI this way?

285
01:26:27,849 --> 01:26:30,079
they asked me to. My neighbors.

286
01:26:30,251 --> 01:26:32,549
they should let you get on
with your lift.

287
01:26:32,720 --> 01:26:34,187
they're sick.

288
01:26:34,355 --> 01:26:35,379
Your neighbors?

289
01:26:35,757 --> 01:26:36,724
the people in my village.

290
01:26:36,925 --> 01:26:39,325
- How many?
-All of them.

291
01:26:55,944 --> 01:27:01,246
When the Health Officials
have made an assessment...

292
01:27:01,816 --> 01:27:05,411
... they will implement a strategy
for all of you to follow.

293
01:31:44,599 --> 01:31:46,294
It was like half my time.

294
01:31:47,201 --> 01:31:50,170
Half my time spent worrying
about it.

295
01:31:51,806 --> 01:31:56,140
I got so freaked out,
I'd go get drunk.

296
01:31:56,878 --> 01:32:00,405
I would get so drunk, you know--

297
01:32:00,681 --> 01:32:04,640
--that I'd wake up with
a businessman

298
01:32:04,819 --> 01:32:09,654
or bus driver in my bed.

299
01:32:10,258 --> 01:32:12,749
And I wouldn't see a condom
anywhere.

300
01:32:13,194 --> 01:32:15,685
And I'd get paranoid that I got it.

301
01:32:16,197 --> 01:32:19,325
So I stop having sex and I go
for a test and I DON'T have it,

302
01:32:19,700 --> 01:32:22,225
and then I start worrying
about getting it again.

303
01:32:22,436 --> 01:32:25,234
And the whole thing
starts over again.

304
01:32:27,308 --> 01:32:29,503
It felt like-inevitable.

305
01:32:30,811 --> 01:32:35,248
So I went out and I got it.

306
01:32:35,650 --> 01:32:39,586
I went into chat rooms.

307
01:32:40,421 --> 01:32:44,585
I found guys who would...
give it to me.

308
01:32:44,859 --> 01:32:46,520
On purpose.

309
01:32:47,194 --> 01:32:48,684
And it worked.

310
01:32:48,829 --> 01:32:50,524
I got it.

311
01:39:56,190 --> 01:39:58,021
You have a guest.

312
01:39:59,460 --> 01:40:01,394
Thank you. You can go.

313
01:43:38,345 --> 01:43:40,279
I'm sorry.

314
01:47:47,861 --> 01:47:51,092
Hold him a few weeks.
No one will be pressing charges.

315
01:48:28,468 --> 01:48:31,062
Image my surprise
when this boy Huku...

316
01:48:31,404 --> 01:48:34,840
asks me for my teenage daughter,
to marry.

317
01:48:35,008 --> 01:48:38,535
And what does he offer for
compensation for such a big surprise?

318
01:48:38,712 --> 01:48:41,044
Such a great, bulky,
cumbersome surprise?

319
01:48:41,181 --> 01:48:45,618
A hog and her piglets, Sister.
Which I suspect he stole.

320
01:48:46,419 --> 01:48:49,081
I would not let such a cherished
girl go for less than 10 cows.

321
01:48:50,056 --> 01:48:51,546
Absolutely not.

322
01:48:52,893 --> 01:48:54,827
He has no mother, no father.

323
01:48:55,028 --> 01:48:59,089
they tell me you nuns
take care of him.

324
01:52:10,824 --> 01:52:12,792
Tell your neighbors
you're recovering.

325
01:52:12,892 --> 01:52:14,655
Tell them we're helping you.

326
01:52:25,038 --> 01:52:27,506
Why did you rush the barrier?

327
01:52:30,110 --> 01:52:31,839
You wanted to see the people
from the ministry?

328
01:52:33,747 --> 01:52:35,112
You shouldn't touch them.

329
01:52:38,618 --> 01:52:40,779
It's going to be all right.

330
01:52:41,221 --> 01:52:43,621
We're here to figure it out.

331
01:52:44,657 --> 01:52:47,558
then we'll be able to help you.

332
01:52:48,628 --> 01:52:51,062
This blood collection business...

333
01:52:51,231 --> 01:52:53,825
...do you know who set it up here?

334
01:52:56,770 --> 01:52:59,364
A lady in a white van.

335
01:53:16,790 --> 01:53:18,121
You have to stay inside.

336
01:53:18,324 --> 01:53:21,293
It's called House Arrest,
not Front Porch Arrest.

337
01:53:53,193 --> 01:53:55,855
You have a beautiful field.

338
01:53:56,329 --> 01:53:58,729
the weather's been petty good.

339
01:54:02,769 --> 01:54:05,795
We should go back to the house.
You could get in deep shit.

340
01:54:06,039 --> 01:54:10,032
Don't you have enough problems
without your crop going to seed?

341
01:54:10,810 --> 01:54:12,710
It's probably too late.

342
01:54:17,283 --> 01:54:18,648
On duty.

343
01:54:19,619 --> 01:54:20,881
I'll help you tomorrow...

344
01:54:21,221 --> 01:54:22,654
...in your paddy.

345
01:54:23,857 --> 01:54:25,620
Just let it go!

346
01:54:28,228 --> 01:54:30,856
I planted too much.

347
01:59:02,101 --> 01:59:05,628
Testing costs ten dollars.

348
01:59:14,180 --> 01:59:17,172
the whole thing's a scam.

349
01:59:19,085 --> 01:59:21,918
He only paid me five dollars
when he gave me this disease.

350
01:59:44,577 --> 01:59:47,011
- Don't I know you?
- I don't think so.

351
02:02:30,876 --> 02:02:36,781
DVD subtitles:  CNST, Montreal

