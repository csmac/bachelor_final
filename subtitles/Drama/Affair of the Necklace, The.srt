1
00:00:00,000 --> 00:00:00,000
Ground.

2
00:00:00,000 --> 00:00:00,000
Okay.

3
00:00:00,000 --> 00:00:00,000
Ten.

4
00:00:00,000 --> 00:00:00,000
Eleven.

5
00:00:00,000 --> 00:00:00,000
Twelve.

6
00:00:00,000 --> 00:00:00,000
Thirteen.

7
00:00:00,000 --> 00:00:00,000
Fourteen.

8
00:00:00,000 --> 00:00:00,000
Fifteen.

9
00:00:00,000 --> 00:00:00,000
Sixteen.

10
00:00:00,000 --> 00:00:00,000
Seventeen.

11
00:00:00,000 --> 00:00:00,000
Eighteen.

12
00:00:00,000 --> 00:00:00,000
Nineteen.

13
00:00:00,000 --> 00:00:00,000
Got it.

14
00:00:00,000 --> 00:00:00,000
- Good.
- You're slipping, Larry.

15
00:00:00,000 --> 00:00:00,000
Give me a break.
The guy's got a ten-digit PIN.

16
00:00:00,000 --> 00:00:00,000
- Check the basement first.
- You got it.

17
00:00:00,000 --> 00:00:00,000
Excuse me!

18
00:00:00,000 --> 00:00:00,000
- Is that it?
- We got it.

19
00:00:00,000 --> 00:00:00,000
She got it!

20
00:00:00,000 --> 00:00:00,000
- Is the expert here?
- Ready and waiting.

21
00:00:00,000 --> 00:00:00,000
- Let's see what you got.
- Okay.

22
00:00:00,000 --> 00:00:00,000
This is George French, head of forensics.

23
00:00:00,000 --> 00:00:00,000
- Hi, George.
- Hi.

24
00:00:00,000 --> 00:00:00,000
This is Dr Cornelius of Manhattanville.
Come in, Doc.

25
00:00:00,000 --> 00:00:00,000
Catherine Banning.

26
00:00:00,000 --> 00:00:00,000
- How do you do?
- Hello! Well, go to work, Doctor.

27
00:00:00,000 --> 00:00:00,000
Don't glare at me, Michael.

28
00:00:00,000 --> 00:00:00,000
So, are the laws of the United States
completely unknown to you?

29
00:00:00,000 --> 00:00:00,000
- Or is it cos you've lived in Morocco...
- Monaco.

30
00:00:00,000 --> 00:00:00,000
I don't care.
It's illegal entry, theft, trespassing.

31
00:00:00,000 --> 00:00:00,000
- I'm not a cop.
- Exactly. Cops know this won't hold up.

32
00:00:00,000 --> 00:00:00,000
- Don't you want him to go up for...
- My job is the painting.

33
00:00:00,000 --> 00:00:00,000
You've got a ghost here.

34
00:00:00,000 --> 00:00:00,000
What?

35
00:00:00,000 --> 00:00:00,000
Another painting underneath.

36
00:00:00,000 --> 00:00:00,000
Monet reused his canvases.

37
00:00:00,000 --> 00:00:00,000
Monet's unknown masterpiece,

38
00:00:00,000 --> 00:00:00,000
"Dogs at Cards".

39
00:00:00,000 --> 00:00:00,000
Where is that sack of shit?

40
00:00:00,000 --> 00:00:00,000
Where is he right now?

41
00:00:00,000 --> 00:00:00,000
Excuse me?

42
00:00:00,000 --> 00:00:00,000
I'm cutting in.

43
00:00:00,000 --> 00:00:00,000
It's all right, Anna.

44
00:00:00,000 --> 00:00:00,000
I'll see you later.

45
00:00:00,000 --> 00:00:00,000
It's a black and white ball.

46
00:00:00,000 --> 00:00:00,000
Oh, I'm sorry. I wasn't invited anyway.

47
00:00:00,000 --> 00:00:00,000
- You left yourself wide open.
- You're all flushed.

48
00:00:00,000 --> 00:00:00,000
How many people can forge a Monet?
Five? Maybe six?

49
00:00:00,000 --> 00:00:00,000
I'm sure I can't be arrested for a joke.

50
00:00:00,000 --> 00:00:00,000
- This was a little too good.
- You're just inches away.

51
00:00:00,000 --> 00:00:00,000
I am. You think
I'll just peck up crumbs you lay out?

52
00:00:00,000 --> 00:00:00,000
I can smell blood on my own.

53
00:00:00,000 --> 00:00:00,000
You smug son of a...

54
00:01:12,400 --> 00:01:14,470
Do you wanna dance

55
00:01:14,800 --> 00:01:16,830
or do you wanna dance?

56
00:02:47,160 --> 00:02:48,910
No!

57
00:02:49,040 --> 00:02:50,830
Get away! Get away!

58
00:02:52,440 --> 00:02:56,270
Oh! You are...
the most remarkable woman.

59
00:02:56,400 --> 00:02:58,780
You don't think
we're finished, do you?

60
00:03:42,000 --> 00:03:43,270
Good morning.

61
00:03:43,400 --> 00:03:45,230
- Good morning.
- Morning, Paul.

62
00:03:45,360 --> 00:03:48,030
- Can I get you anything else?
- Not a thing.

63
00:03:48,160 --> 00:03:50,910
- Thank you, Paul.
- Thank you, Paul.

64
00:03:51,040 --> 00:03:54,310
I suppose you didn't just run out for that.

65
00:03:55,400 --> 00:03:56,590
No.

66
00:03:57,280 --> 00:03:58,500
No.

67
00:03:58,800 --> 00:04:01,070
Damn. I hate being a foregone conclusion.

68
00:04:14,080 --> 00:04:15,670
You live very well.

69
00:04:16,280 --> 00:04:17,750
Thank you.

70
00:04:18,400 --> 00:04:20,390
It'd be a shame to lose it all.

71
00:04:20,520 --> 00:04:21,390
That depends

72
00:04:21,520 --> 00:04:23,390
on a very large presumption.

73
00:04:23,520 --> 00:04:25,110
Yes, it does.

74
00:04:25,840 --> 00:04:28,350
I won't back off, you know.

75
00:04:29,200 --> 00:04:30,950
Not even for a minute.

76
00:04:32,480 --> 00:04:35,070
I would be hugely disappointed if you did.

77
00:04:35,200 --> 00:04:37,470
How do porcupines mate?

78
00:04:37,600 --> 00:04:39,900
Old joke. Very carefully.

79
00:04:40,040 --> 00:04:42,990
Carefully. Or... unsuccessfully.

80
00:04:43,120 --> 00:04:45,390
Don't see many porcupines.

81
00:04:45,520 --> 00:04:48,550
Creatures with highly evolved
defence systems.

82
00:04:48,680 --> 00:04:50,430
Like porcupines?

83
00:04:50,560 --> 00:04:52,910
Like 42-year-old, successful

84
00:04:53,040 --> 00:04:54,750
self-involved loners.

85
00:04:56,200 --> 00:04:58,070
If you've found a female mirror image

86
00:04:58,200 --> 00:05:01,110
and think you'll form
a rewarding relationship...

87
00:05:01,240 --> 00:05:02,300
Think again?

88
00:05:12,400 --> 00:05:14,190
Good morning.

89
00:05:15,960 --> 00:05:17,950
Nice dress.

90
00:05:20,960 --> 00:05:23,550
Looks like it was a great party.

91
00:05:27,720 --> 00:05:30,550
You gonna be a clich�?

92
00:05:31,320 --> 00:05:33,270
Did you even think twice?

93
00:05:33,560 --> 00:05:35,710
- No.
- You knew what you were doing?

94
00:05:35,840 --> 00:05:40,700
My job. He likes me. He'll keep liking me.

95
00:05:40,840 --> 00:05:42,430
Keep him right next to me.

96
00:05:42,560 --> 00:05:43,670
And you don't care

97
00:05:43,800 --> 00:05:45,750
what that makes you.

98
00:05:49,560 --> 00:05:52,550
- I know what I'm doing.
- Do you really?

99
00:05:52,680 --> 00:05:55,060
This is just about money, Mike.

100
00:05:56,720 --> 00:05:59,280
Oh, oh, oh, he's got a problem.

101
00:06:17,840 --> 00:06:19,790
I could get used to this.

102
00:06:19,920 --> 00:06:22,350
- Just hold on now.
- Hold on why?

103
00:06:36,600 --> 00:06:38,470
- Take the stick.
- Oh, I'm not...

104
00:06:38,600 --> 00:06:40,430
- Take it.
- I'm not taking...

105
00:06:40,560 --> 00:06:42,230
- Put your hands on it.
- Oh, no!

106
00:06:47,760 --> 00:06:49,790
That's it. That's good.

107
00:06:49,920 --> 00:06:51,630
- That's good.
- Oh, my God!

108
00:06:54,440 --> 00:06:57,870
Head for that hill over there. Don't lose it.

109
00:06:58,000 --> 00:06:59,790
There you go.

110
00:07:01,840 --> 00:07:03,870
This is ridge-running. We're doing it.

111
00:07:04,000 --> 00:07:06,510
- Oh, this is great.
- I'm in your hands.

112
00:07:06,640 --> 00:07:10,470
- Just feel that. You feel it?
- I do.

113
00:07:13,920 --> 00:07:15,910
Left. To the left.

114
00:07:19,880 --> 00:07:23,750
We'll pick up a thermal
coming over the hill.

115
00:07:23,880 --> 00:07:26,590
Just like a hawk.

116
00:07:51,040 --> 00:07:52,550
Cows.

117
00:07:52,960 --> 00:07:56,790
Oh, that's okay. We're only about
four states from your car.

118
00:08:11,480 --> 00:08:12,390
That island

119
00:08:12,520 --> 00:08:14,150
isn't Manhattan.

120
00:08:14,280 --> 00:08:16,470
- It's not?
- No.

121
00:08:17,440 --> 00:08:19,670
I have appointments.

122
00:08:20,760 --> 00:08:22,110
Wanna keep them?

123
00:09:02,960 --> 00:09:05,030
Door's welded. Throw your leg over.

124
00:09:05,160 --> 00:09:07,150
Throw my leg over.

125
00:10:38,240 --> 00:10:39,950
This must go over.

126
00:10:40,080 --> 00:10:41,140
With whom?

127
00:10:41,280 --> 00:10:43,510
Whomever you bring here.

128
00:10:43,640 --> 00:10:46,750
I never bring anyone here.

129
00:10:54,600 --> 00:10:56,430
I bet they're all my size.

130
00:10:57,800 --> 00:11:02,150
Could be. Might be off a little
here and there, but I think they'll make do.

131
00:11:02,600 --> 00:11:04,710
I'll go make dinner.

132
00:11:15,640 --> 00:11:17,670
Come on!

133
00:11:17,800 --> 00:11:20,470
- You want some wine?
- Yes!

134
00:11:20,720 --> 00:11:23,280
It's beautiful!

135
00:11:35,680 --> 00:11:37,310
Want to see it?

136
00:11:38,640 --> 00:11:39,860
No.

137
00:11:40,560 --> 00:11:43,190
- You sure?
- I'm sure.

138
00:11:43,440 --> 00:11:45,190
Like a splash?

139
00:11:50,440 --> 00:11:52,390
Come here.

140
00:12:04,000 --> 00:12:05,910
Wanna see it?

141
00:12:10,320 --> 00:12:11,190
No.

142
00:12:14,640 --> 00:12:15,590
You sure?

143
00:12:16,000 --> 00:12:19,310
You think I believe you'd leave
your hard-stolen painting

144
00:12:19,440 --> 00:12:21,230
Iying around a Caribbean hut?

145
00:12:21,360 --> 00:12:23,310
What if I did?

146
00:12:23,440 --> 00:12:25,110
And that you'd tell me?

147
00:12:25,240 --> 00:12:26,710
Well, what if I trust you?

148
00:12:26,840 --> 00:12:29,140
You know you can't.

149
00:12:29,280 --> 00:12:31,790
You don't believe you could trust me?

150
00:12:31,920 --> 00:12:35,070
You know how likely I think that is?

151
00:12:45,840 --> 00:12:47,510
Excuse me.

152
00:13:04,960 --> 00:13:07,260
Should I open another bottle?

153
00:13:07,960 --> 00:13:09,870
Yes.

154
00:13:11,000 --> 00:13:13,750
- I think so.
- I think so.

155
00:13:14,080 --> 00:13:15,670
'85 wasn't very good.

156
00:13:15,800 --> 00:13:19,550
- No, it's not... No...
- Truce. A truce would be good.

157
00:13:19,680 --> 00:13:21,910
That'd be great. That'd be good.

158
00:13:27,840 --> 00:13:28,820
What was it?

159
00:13:30,520 --> 00:13:32,750
A nice little Renoir.

160
00:13:34,760 --> 00:13:35,980
Renoir.

161
00:13:41,400 --> 00:13:42,510
A nice little copy?

162
00:13:45,120 --> 00:13:46,990
We'll never know, will we?

163
00:13:47,120 --> 00:13:49,870
- Okay, I give up!
- Easy, easy.

164
00:13:50,560 --> 00:13:54,390
- I'll get that bottle.
- Oh, yeah, that would be good.

165
00:13:57,200 --> 00:13:59,870
Oh, you're not boring, I'll give you that.

166
00:14:33,600 --> 00:14:36,230
- You complimented me.
- How?

167
00:14:36,480 --> 00:14:39,110
- They were bankers.
- Who?

168
00:14:39,240 --> 00:14:41,700
Who? The suits.

169
00:14:42,840 --> 00:14:45,590
You're transferring assets.

170
00:14:45,720 --> 00:14:47,150
Getting ready to run.

171
00:14:47,280 --> 00:14:50,190
And suppose I did run?
Then what would you have?

172
00:14:50,320 --> 00:14:53,430
Not the painting,
not the $5 million fee, not me.

173
00:14:53,560 --> 00:14:55,230
Yes?

174
00:14:58,640 --> 00:15:01,200
Suppose I gave you ten?

175
00:15:02,440 --> 00:15:03,830
To fail?

176
00:15:11,240 --> 00:15:12,990
How would I hide it?

177
00:15:13,120 --> 00:15:15,070
I'd teach you.

178
00:15:24,920 --> 00:15:28,590
You really think there's
happy ever after for people like us?

179
00:15:32,600 --> 00:15:35,160
So just how big of a thief are you?

180
00:15:35,320 --> 00:15:37,910
Well, if you count Wall Street,

181
00:15:39,160 --> 00:15:40,870
pretty big.

182
00:15:44,800 --> 00:15:46,990
If you mean art,

183
00:15:49,640 --> 00:15:51,750
I'm just an amateur.

184
00:15:52,560 --> 00:15:54,470
Beginner's luck, huh?

185
00:15:55,520 --> 00:15:58,030
Gentleman jockey wins the Derby.

186
00:15:58,600 --> 00:16:00,630
Something like that.

187
00:16:05,160 --> 00:16:08,350
You got your hand caught
in the cookie jar now.

188
00:16:08,480 --> 00:16:10,390
How are you gonna get out of it?

189
00:16:10,600 --> 00:16:14,350
It's just a game, love.

190
00:16:15,880 --> 00:16:17,710
Just a game.

191
00:16:27,880 --> 00:16:29,950
- Nice tan.
- Thanks.

192
00:16:30,080 --> 00:16:32,070
I went to the beach for a couple of days.

193
00:16:32,200 --> 00:16:34,350
On the job?

194
00:16:34,480 --> 00:16:35,510
That's right.

195
00:16:36,160 --> 00:16:38,430
And did you pick anything... up?

196
00:16:38,560 --> 00:16:40,230
Well, he's got an ornament

197
00:16:40,360 --> 00:16:43,390
worn by Barbarossa
at his coronation in 1 1 52.

198
00:16:43,520 --> 00:16:46,310
Really? Stolen?

199
00:16:48,280 --> 00:16:49,390
No.

200
00:16:49,520 --> 00:16:51,900
So that's it, after two days

201
00:16:52,040 --> 00:16:54,150
and two nights?

202
00:16:55,320 --> 00:16:56,510
That was it.

203
00:16:57,120 --> 00:17:00,310
Would you like to know where he was
the night before you left?

204
00:17:00,440 --> 00:17:04,140
Or after he left you last night?

205
00:17:08,040 --> 00:17:09,430
Not really.

206
00:17:11,400 --> 00:17:13,750
Okay. Suit yourself.

207
00:17:14,920 --> 00:17:16,790
Michael.

208
00:17:33,280 --> 00:17:34,550
Oh,...

209
00:17:34,680 --> 00:17:35,710
she's

210
00:17:42,560 --> 00:17:44,630
striking.

211
00:17:45,160 --> 00:17:47,190
He seems to think so.

212
00:17:47,320 --> 00:17:49,430
Three dates in six days.

213
00:17:58,560 --> 00:17:59,870
Well,

214
00:18:00,000 --> 00:18:03,390
where does he find the time?

215
00:18:13,880 --> 00:18:15,670
You okay?

216
00:18:16,080 --> 00:18:17,470
Yeah.

217
00:18:19,160 --> 00:18:22,070
You know, I was okay once.

218
00:18:25,240 --> 00:18:27,670
My girlfriend got drunk.

219
00:18:28,120 --> 00:18:32,030
Stayed out all night,
came back in the morning... married.

220
00:18:33,880 --> 00:18:35,350
Yeah.

221
00:18:35,760 --> 00:18:39,030
I told people I didn't care... and then I

222
00:18:39,160 --> 00:18:43,750
fucked five women in three days,
flipped my car on an on-ramp,

223
00:18:44,520 --> 00:18:47,750
beat a suspect unconscious,
got suspended.

224
00:18:48,200 --> 00:18:50,150
But I was okay.

225
00:18:52,440 --> 00:18:54,510
Is there a point to this story?

226
00:19:00,200 --> 00:19:02,430
But he was with him.

227
00:19:03,240 --> 00:19:04,380
What's this?

228
00:19:04,520 --> 00:19:06,470
Photos of the Monet's borders.

229
00:19:07,520 --> 00:19:09,510
The borders?

230
00:19:09,640 --> 00:19:13,420
Insurers remove a painting from the frame
to photograph its borders.

231
00:19:13,560 --> 00:19:18,270
Borders are never seen in galleries
or auctions, so if a picture's stolen and

232
00:19:18,400 --> 00:19:20,910
someone makes a forgery,
the borders won't match.

233
00:19:21,040 --> 00:19:22,910
And if they do happen to match?

234
00:19:23,040 --> 00:19:26,110
Then the forger
was in the presence of the original.

235
00:19:26,240 --> 00:19:30,310
- So, if Crown's prank Monet is too good...
- Then we find the forger

236
00:19:30,440 --> 00:19:32,900
and nail the bastard.

237
00:19:33,040 --> 00:19:35,190
- How long have you had these?
- Five days.

238
00:19:35,320 --> 00:19:36,790
And you didn't open them?

239
00:19:36,920 --> 00:19:39,110
Yeah, well, they're open now.

240
00:19:55,720 --> 00:19:58,550
- It's beautiful.
- You're not going to say

241
00:19:58,680 --> 00:20:00,710
"I couldn't possibly"?

242
00:20:00,840 --> 00:20:02,190
No.

243
00:20:02,320 --> 00:20:03,540
I...

244
00:20:04,440 --> 00:20:06,950
I wouldn't say anything that boring.

245
00:20:13,680 --> 00:20:15,310
She's an old friend,

246
00:20:15,440 --> 00:20:17,950
but it'll be like
having dinner in a morgue.

247
00:20:18,080 --> 00:20:22,790
But you know, it occurred to me, I might
be able to bear it if you came with me.

248
00:20:23,920 --> 00:20:27,150
Tomorrow? That's so soon.

249
00:20:28,520 --> 00:20:30,430
I sense hesitation.

250
00:20:31,040 --> 00:20:32,350
No...

251
00:20:32,840 --> 00:20:33,790
No.

252
00:20:35,000 --> 00:20:39,350
Do you find my company monotonous?

253
00:20:39,480 --> 00:20:41,270
Cos I'd hate to think you could.

254
00:20:43,360 --> 00:20:45,350
What, be bored by you?

255
00:20:45,480 --> 00:20:47,350
Require a little variety?

256
00:20:52,080 --> 00:20:54,070
You're referring to Anna.

257
00:20:56,120 --> 00:20:58,790
They photographed me with Anna.

258
00:20:58,920 --> 00:21:01,990
- You know, that's your prerogative.
- I thought they were.

259
00:21:02,120 --> 00:21:04,110
I let it happen.

260
00:21:05,080 --> 00:21:06,710
- Want to know why?
- No.

261
00:21:06,840 --> 00:21:08,390
- I'll tell you.
- Don't.

262
00:21:08,520 --> 00:21:10,870
- I want to tell you.
- I don't want to know.

263
00:21:11,000 --> 00:21:12,830
Jimmy, I'd like to get out.

264
00:21:12,960 --> 00:21:14,390
- Keep driving.
- Stop!

265
00:21:15,000 --> 00:21:17,790
- I don't want to know why!
- Let me tell you!

266
00:21:17,920 --> 00:21:20,350
- No!
- Ah, shit.

267
00:21:21,880 --> 00:21:24,510
- Now ask me why!
- I don't want to know!

268
00:21:24,640 --> 00:21:28,180
- You're upset about it!
- Yeah, cos you wanted me to be upset!

269
00:21:28,320 --> 00:21:32,510
- I needed you to be upset!
- Ho-ho, he's sadistic! Who knew?

270
00:21:32,640 --> 00:21:35,910
- Did it occur to you I needed to know?
- Know what?!

271
00:21:36,040 --> 00:21:37,630
Know whether all it was to you

272
00:21:37,760 --> 00:21:38,820
was the painting?

273
00:21:45,000 --> 00:21:47,430
How else could I know?

274
00:21:49,920 --> 00:21:51,870
What about you?

275
00:21:54,080 --> 00:21:56,590
I can leave here tomorrow.

276
00:21:57,000 --> 00:21:58,590
So can you.

277
00:22:09,160 --> 00:22:12,590
- We'd be fugitives.
- Fugitives with means.

278
00:22:12,720 --> 00:22:15,180
All the difference in the world.

279
00:22:17,520 --> 00:22:19,310
I don't know.

280
00:22:19,600 --> 00:22:20,910
I don't know.

281
00:22:23,560 --> 00:22:29,310
Oh dear. Peter Pan decides to grow up,
and finds there's no place to land.

282
00:22:30,280 --> 00:22:33,590
The only sad part is,

283
00:22:33,720 --> 00:22:36,470
if she's anything like you,

284
00:22:36,600 --> 00:22:39,710
she won't know what she's lost
until it's gone.

285
00:22:46,400 --> 00:22:48,270
Hey! Let's go.

286
00:22:48,400 --> 00:22:49,310
Where?

287
00:22:49,440 --> 00:22:51,430
The borders matched perfectly.

288
00:22:51,920 --> 00:22:55,510
Then again, I'm sure you knew that,
didn't you?

289
00:22:56,120 --> 00:22:58,500
Come on. Let's go meet some forgers.

290
00:22:59,240 --> 00:23:03,750
I was never an lmpressionist.
Anyway, I pay taxes now.

291
00:23:03,880 --> 00:23:05,310
Doing what?

292
00:23:05,440 --> 00:23:08,950
Portraits.
Inserting the rich into old masterpieces.

293
00:23:10,320 --> 00:23:13,230
The wife's face on the Mona Lisa.

294
00:23:13,360 --> 00:23:15,950
From Brooklyn to Greenwich
in one generation.

295
00:23:16,080 --> 00:23:18,030
You've gotta have the paintings to match.

296
00:23:18,160 --> 00:23:19,830
So, who did it?

297
00:23:21,120 --> 00:23:22,750
The German.

298
00:23:23,560 --> 00:23:25,310
Heinrich...

299
00:23:25,440 --> 00:23:28,270
Knutzhorn.

300
00:23:30,440 --> 00:23:32,390
Art's a small world, Michael.

301
00:23:32,520 --> 00:23:34,590
So, who do you think did this?

302
00:23:42,800 --> 00:23:44,830
Frederick Golchan.

303
00:23:46,360 --> 00:23:48,150
He said you did.

304
00:23:48,280 --> 00:23:51,590
So you think if I want paint,
it just comes Fed Ex?

305
00:23:51,840 --> 00:23:54,870
And the guards just think it's therapy,

306
00:23:55,000 --> 00:23:59,270
while I madly copy a Monet
in my cell from the original?

307
00:24:07,520 --> 00:24:08,430
What?

308
00:24:14,680 --> 00:24:15,870
What?

309
00:24:16,000 --> 00:24:17,430
Nothing.

310
00:24:17,560 --> 00:24:18,590
What?!

311
00:24:19,840 --> 00:24:22,710
I was just wondering
about what he wouldn't say.

312
00:24:23,640 --> 00:24:25,590
Give.

313
00:24:26,080 --> 00:24:27,870
There's nothing.

314
00:24:28,040 --> 00:24:29,750
You know what?

315
00:24:29,880 --> 00:24:32,590
Life is full of shitty conflicts, okay? Give.

316
00:24:34,680 --> 00:24:39,030
I just wondered if there was
a connection between them, that's all.

317
00:24:39,280 --> 00:24:41,950
Between Crown and that old man?

318
00:24:43,280 --> 00:24:44,550
Yeah.

319
00:25:47,120 --> 00:25:51,670
- You are spooky. You are really spooky.
- They owned a gallery together,

320
00:25:51,800 --> 00:25:56,990
Crown and Knutzhorn. In Berlin, 1990.
Another one in Hamburg.

321
00:25:57,120 --> 00:25:59,070
And one in Paris in '94.

322
00:26:00,840 --> 00:26:03,590
You notice the one-man show
in Paris in '95?

323
00:26:04,560 --> 00:26:06,510
The artist is Knutzhorn.

324
00:26:06,640 --> 00:26:08,150
So?

325
00:26:08,800 --> 00:26:10,430
So look at the first name.

326
00:26:10,560 --> 00:26:12,630
Tyrol Knutzhorn.

327
00:26:12,760 --> 00:26:14,750
Yeah, we spoke to a Heinrich.

328
00:26:17,760 --> 00:26:19,950
You know what it was?

329
00:26:20,080 --> 00:26:22,790
The way he smiled at the painting?

330
00:26:23,520 --> 00:26:25,190
Pride.

331
00:26:25,560 --> 00:26:27,630
Paternal pride.

332
00:26:28,320 --> 00:26:30,350
I bet you it's his son.

333
00:26:30,480 --> 00:26:34,470
Son of a great forger,
who paints as well as Dad.

334
00:26:34,600 --> 00:26:36,630
Just hasn't been caught yet.

335
00:26:37,840 --> 00:26:40,710
Well, I assume if you look,

336
00:26:40,840 --> 00:26:44,350
you'll find Tyrol Knutzhorn
lives in New York.

337
00:26:49,880 --> 00:26:54,230
You know what?
l... I owe you an apology.

338
00:26:55,960 --> 00:26:59,110
I didn't think you had the chops
to see this through.

339
00:26:59,240 --> 00:27:00,950
You don't know me.

340
00:27:01,080 --> 00:27:04,950
Yeah, but I should have. Cos...
I bet you'd stand at the Pearly Gates

341
00:27:05,080 --> 00:27:08,950
and kick St Peter in the teeth
before you'd let someone play you.

342
00:27:13,240 --> 00:27:16,470
Harold, listen carefully
and please don't interrupt.

343
00:27:16,600 --> 00:27:20,790
If I had to be gone, and I mean
seriously gone, in about eight hours,

344
00:27:20,920 --> 00:27:23,550
how much could I take with me?

345
00:27:23,680 --> 00:27:26,110
- Are you okay?
- Harold...

346
00:27:26,240 --> 00:27:29,310
You'd take an enormous loss,
liquidating like this.

347
00:27:29,440 --> 00:27:34,150
I understand, and that can't be helped.
What could I leave with?

348
00:27:35,600 --> 00:27:38,190
Call me in an hour.
I'll have a number for you.

349
00:27:38,320 --> 00:27:40,270
Okay.

350
00:27:54,800 --> 00:27:56,750
Leaving us?

351
00:27:58,000 --> 00:27:59,310
No, you're not that lucky.

352
00:27:59,440 --> 00:28:02,230
I'm organising.

353
00:28:02,440 --> 00:28:04,630
No Knutzhorn has been
to see the old man.

354
00:28:05,680 --> 00:28:06,510
No?

355
00:28:10,120 --> 00:28:13,430
There was a Knudsen, though,
three times last month.

356
00:28:13,560 --> 00:28:15,940
Think that's a coincidence, or...?

357
00:28:16,080 --> 00:28:17,670
Could be.

358
00:28:18,480 --> 00:28:19,700
Yeah.

359
00:28:20,560 --> 00:28:22,030
Probably is.

360
00:28:31,720 --> 00:28:33,430
Look, here's an extra hundred.

361
00:28:33,560 --> 00:28:35,830
Just go, go!

362
00:28:36,680 --> 00:28:38,910
- Paul, is he here?
- He's in a meeting now.

363
00:28:39,120 --> 00:28:42,950
- It's very important.
- If you wait in the living room, I'll get him.

364
00:28:52,400 --> 00:28:54,350
Ma'am, wait.

365
00:29:02,320 --> 00:29:05,470
I know we have plug adapters,
I saw them.

366
00:29:05,600 --> 00:29:09,750
I just can't find them. Would you go
downstairs, Anna, and ask Paul...

367
00:29:37,840 --> 00:29:40,750
Well... we seem to have backtracked.

368
00:29:41,440 --> 00:29:43,390
- No!
- No.

369
00:29:44,840 --> 00:29:46,630
Take your hands off me!

370
00:29:47,080 --> 00:29:49,230
Come on,
what do you have that's fresh?

371
00:29:49,360 --> 00:29:51,430
Who's that girl and why is she here?

372
00:29:51,560 --> 00:29:53,750
Anna works for me.

373
00:29:56,680 --> 00:29:58,750
You never thought I was a fool before.

374
00:29:58,880 --> 00:30:01,710
I owe her money
and I wanted to pay her before I go.

375
00:30:01,840 --> 00:30:03,030
Oh, really?

376
00:30:03,160 --> 00:30:05,110
And what is it she does for you?

377
00:30:06,520 --> 00:30:08,430
I would be compromising her to say.

378
00:30:08,560 --> 00:30:10,430
Oh, Tommy, you're going away together.

379
00:30:10,560 --> 00:30:12,590
No, I'm going with you.

380
00:30:16,080 --> 00:30:19,750
Of all the things in the world
to take with a leap of faith,

381
00:30:21,880 --> 00:30:25,470
- how can I possibly trust you?
- I'm not gonna ask that.

382
00:30:25,600 --> 00:30:28,950
I'm going to trust you.
Isn't that what you wanted,

383
00:30:29,080 --> 00:30:30,950
my trust?

384
00:30:31,080 --> 00:30:33,190
Tomorrow afternoon

385
00:30:34,080 --> 00:30:35,950
I'll put the Monet back.

386
00:30:41,600 --> 00:30:44,430
Where? Hanging back
on the wall in the museum?

387
00:30:44,560 --> 00:30:45,990
Yes.

388
00:30:46,120 --> 00:30:48,990
You know, Tommy,
I'm all checkmated out.

389
00:30:49,960 --> 00:30:53,470
If the painting's back, we're free.
We're only for each other.

390
00:30:54,800 --> 00:30:57,070
Back on the wall, in the museum?

391
00:30:57,200 --> 00:31:00,350
Yes, and you'll meet me

392
00:31:00,520 --> 00:31:04,030
at four o'clock
at the Wall Street heliport,

393
00:31:04,160 --> 00:31:06,190
and we'll leave together.

394
00:31:06,680 --> 00:31:08,630
Or...

395
00:31:11,760 --> 00:31:14,710
Or you can have them at the museum
waiting for me.

396
00:31:18,440 --> 00:31:20,350
I'm trusting you.

397
00:31:35,120 --> 00:31:37,110
I can't... do that!

398
00:31:38,000 --> 00:31:41,590
Damn you, you son of a bitch! Get away!

399
00:32:26,640 --> 00:32:27,670
Hey.

400
00:32:28,560 --> 00:32:30,510
Hi.

401
00:32:32,720 --> 00:32:34,830
I need to talk to you.

402
00:32:40,960 --> 00:32:42,390
- Paretti.
- Yeah.

403
00:32:42,520 --> 00:32:44,470
You're in the lobby, right?

404
00:32:45,640 --> 00:32:49,390
Yeah, Mikey, I'm here.
Me and about 30 other guys.

405
00:33:02,920 --> 00:33:05,030
Let up on yourself.

406
00:33:05,160 --> 00:33:06,750
You did the right thing.

407
00:33:08,440 --> 00:33:10,870
Assuming the son of a bitch shows up.

408
00:33:26,520 --> 00:33:30,110
Mark, stay with the car.
Get a uniformed man on the fire escape.

409
00:33:30,240 --> 00:33:31,150
Got it.

410
00:33:31,280 --> 00:33:33,350
Detective!

411
00:33:35,520 --> 00:33:36,470
Yeah.

412
00:33:36,600 --> 00:33:39,190
Goddamn it.
Did she leave anything behind?

413
00:33:39,880 --> 00:33:41,830
Stake out the whole place.

414
00:33:41,960 --> 00:33:44,150
Maybe she'll come back. I don't know.

415
00:33:44,280 --> 00:33:45,470
No, no, no.

416
00:33:45,600 --> 00:33:48,230
Is Jack there? Put him on the phone.

417
00:33:48,360 --> 00:33:50,270
Get Jack!

418
00:33:51,680 --> 00:33:53,430
- What?
- The forger.

419
00:33:53,560 --> 00:33:57,180
You didn't think we just dropped that,
did you? Jack ran it down.

420
00:33:57,320 --> 00:34:00,860
It was a daughter.
She was under our noses the whole time.

421
00:34:01,000 --> 00:34:03,830
Tyrol Anna Knudsen, Knutzhorn,

422
00:34:03,960 --> 00:34:07,190
whatever.
Crown's known her since she was ten.

423
00:34:08,160 --> 00:34:12,830
When her father went to jail, he became
her guardian. Put her through college.

424
00:34:12,960 --> 00:34:16,110
Now she works for his company.
Jack, here's what I want you to do.

425
00:34:16,840 --> 00:34:17,900
He's in.

426
00:34:18,240 --> 00:34:19,350
Got him.

427
00:34:19,480 --> 00:34:21,470
I'll call you back.

428
00:34:21,960 --> 00:34:22,910
That's him.

429
00:34:23,040 --> 00:34:25,870
Overcoat and briefcase.

430
00:34:26,120 --> 00:34:27,870
Yeah, Mikey. We got him.

431
00:34:28,000 --> 00:34:31,150
Centre entrance. Grey overcoat.

432
00:34:31,600 --> 00:34:33,270
Bring up the lobby on the big monitor.

433
00:34:36,000 --> 00:34:37,350
Move in quietly.

434
00:34:37,480 --> 00:34:39,940
- We don't wanna make a scene.
- Tommy...

435
00:34:51,960 --> 00:34:53,990
What the hell's he doing?

436
00:34:54,120 --> 00:34:56,470
Almost like he wants
to make sure we see him.

437
00:35:01,720 --> 00:35:03,790
Let's play ball.

438
00:35:04,720 --> 00:35:07,510
There he goes. Bowler hat.
Move in and pick him up.

439
00:35:07,640 --> 00:35:10,350
Go. Move it.

440
00:35:10,760 --> 00:35:12,990
Quietly. Excuse me.

441
00:35:20,960 --> 00:35:22,470
Excuse me.

442
00:35:25,720 --> 00:35:27,790
Shit. He switched the painting.

443
00:35:27,920 --> 00:35:29,750
Stay with the painting.

444
00:35:29,880 --> 00:35:32,340
What? Look, I can't...

445
00:35:33,080 --> 00:35:35,150
Shit! There's another one.

446
00:35:45,640 --> 00:35:50,350
- There's guys with bowler hats...
- All over the goddamn place.

447
00:35:53,320 --> 00:35:55,550
- Did you warn him?
- What? No.

448
00:35:55,680 --> 00:35:56,950
- Did you warn him?
- No!

449
00:35:58,120 --> 00:36:01,710
- Then he knew you'd betray him.
- They're going for the staircases.

450
00:36:01,840 --> 00:36:05,310
Now we got four floors to keep track of.

451
00:36:06,520 --> 00:36:10,750
Stay with him. He disappears,
it'll be ten years before he resurfaces.

452
00:36:10,880 --> 00:36:13,150
- Is the lmpressionist Wing sealed off?
- Yes.

453
00:36:13,280 --> 00:36:15,740
- No way to get in?
- The gates are down.

454
00:36:15,880 --> 00:36:19,030
He can't return the painting to that room.

455
00:36:19,680 --> 00:36:22,140
- They're everywhere.
- What's the deal?

456
00:36:42,960 --> 00:36:45,190
He's going the other way!

457
00:36:55,880 --> 00:36:58,990
What? What? Look, I can't hear you.

458
00:37:24,080 --> 00:37:27,620
This is ridiculous.
He's got us running around in circles.

459
00:37:32,000 --> 00:37:33,870
Paretti's trying to reach you.

460
00:37:34,000 --> 00:37:35,830
- I can't...
- You're fucked. I'm coming.

461
00:37:51,680 --> 00:37:52,660
What do we do?

462
00:37:52,800 --> 00:37:55,870
- What do we do?
- Start arresting people. Come on.

463
00:37:57,400 --> 00:38:00,470
Excuse me, sir. One moment.

464
00:38:03,160 --> 00:38:04,990
Shit.

465
00:38:22,560 --> 00:38:24,030
I knew it.

466
00:38:24,160 --> 00:38:25,070
Excuse me.

467
00:39:24,120 --> 00:39:26,350
Get the civilians outta here.

468
00:39:41,880 --> 00:39:43,510
How do we get this gate open?

469
00:39:43,640 --> 00:39:46,630
Kill the sprinklers
in the lmpressionist Wing now.

470
00:40:13,200 --> 00:40:15,310
It's water paint.

471
00:40:19,040 --> 00:40:23,510
It's been here the whole time. From what?
One, two days after the robbery?

472
00:40:23,640 --> 00:40:26,830
Something's jammed in the track.

473
00:40:29,320 --> 00:40:32,150
He returned the thing
almost as soon as he stole it.

474
00:40:33,640 --> 00:40:35,350
Er, sir?

475
00:40:38,720 --> 00:40:40,550
Oh, Lord.

476
00:40:40,680 --> 00:40:45,350
Oh, dear, dear. This can't be.
This simply can't be.

477
00:40:45,480 --> 00:40:49,150
How the hell did he do that?

478
00:41:10,600 --> 00:41:12,470
Where are you going?

479
00:41:14,200 --> 00:41:16,310
Er, office.

480
00:41:17,120 --> 00:41:18,750
Write up my report.

481
00:41:18,880 --> 00:41:21,550
- The job's done, right?
- Yeah!

482
00:41:22,520 --> 00:41:26,670
So, the other painting. Why do you think
he chose that particular one?

483
00:41:28,120 --> 00:41:30,070
I have no idea.

484
00:41:31,680 --> 00:41:35,950
But, um, it's not insured by my people,
so I'm out of it.

485
00:41:37,040 --> 00:41:39,630
Obviously you'll still have to pursue it.

486
00:41:40,440 --> 00:41:42,070
I don't really give a shit.

487
00:41:45,480 --> 00:41:47,590
You don't care if you catch him?

488
00:41:47,720 --> 00:41:50,350
Well, I'll do what they tell me to do.

489
00:41:52,680 --> 00:41:54,790
- Did you ever care?
- Yeah!

490
00:41:54,920 --> 00:41:57,380
He pissed me off.

491
00:41:57,520 --> 00:41:58,830
But, look...

492
00:41:59,000 --> 00:42:01,110
The week before I met you,

493
00:42:01,240 --> 00:42:06,230
I nailed two real estate agents and
a guy who was beating his kids to death.

494
00:42:06,360 --> 00:42:09,070
So if some Houdini wants

495
00:42:09,200 --> 00:42:12,470
to snatch a few swirls of paint
that are only important

496
00:42:12,600 --> 00:42:15,190
to some very silly rich people,

497
00:42:16,240 --> 00:42:18,310
I don't really give a damn.

498
00:42:20,920 --> 00:42:22,950
You're a good man.

499
00:42:35,120 --> 00:42:38,550
Okay. Get out of here. And tell him

500
00:42:38,680 --> 00:42:39,950
I said hello.

501
00:42:41,120 --> 00:42:43,550
I've no idea what you're talking about.

502
00:42:43,680 --> 00:42:45,980
Go on.

503
00:42:46,200 --> 00:42:48,500
And don't stiff us for your phone bills.

504
00:42:48,640 --> 00:42:50,550
No.

505
00:42:54,400 --> 00:42:55,510
Taxi!

506
00:43:13,360 --> 00:43:16,980
You know, I'm just going to run. Thanks.

507
00:43:26,000 --> 00:43:27,510
Excuse me.

508
00:43:31,560 --> 00:43:33,510
Ma'am, you can't go out there!

509
00:43:39,000 --> 00:43:40,470
Tommy!

510
00:43:47,400 --> 00:43:49,150
You would be Catherine?

511
00:43:49,280 --> 00:43:50,630
Yes.

512
00:43:50,960 --> 00:43:53,520
He wanted you to have this.

513
00:43:59,960 --> 00:44:00,990
Good day, ma'am.

514
00:44:24,680 --> 00:44:26,630
Hi there.

515
00:44:26,760 --> 00:44:29,060
How can I help you?

516
00:44:29,640 --> 00:44:31,790
I have a reservation.

517
00:44:31,920 --> 00:44:34,830
Okay. Miss Banning.

518
00:44:37,200 --> 00:44:39,550
Will you be carrying any luggage today?

519
00:44:39,920 --> 00:44:41,060
No.

520
00:44:45,320 --> 00:44:47,190
Can you see that this

521
00:44:50,760 --> 00:44:55,150
gets to this man at police headquarters?
This should take care of it.

522
00:44:56,760 --> 00:44:58,710
It won't get there till morning.

523
00:44:58,840 --> 00:45:00,550
That's fine.

524
00:45:04,040 --> 00:45:08,510
Departures for international flights
are up the escalator to your left.

525
00:45:09,200 --> 00:45:10,230
Ma'am?

526
00:45:11,480 --> 00:45:13,030
Is everything all right?

527
00:45:14,920 --> 00:45:16,870
Everything's fine.

528
00:46:11,080 --> 00:46:14,470
You don't need to cry there, lassie.

529
00:46:28,680 --> 00:46:31,030
Did you set this up?

530
00:46:33,680 --> 00:46:35,190
Did you set this up?

531
00:46:36,440 --> 00:46:37,110
Did you?!

532
00:46:37,240 --> 00:46:41,550
Miss, please,
the seat belt sign is still on...

533
00:46:58,160 --> 00:47:00,310
I'll tell you what.

534
00:47:00,560 --> 00:47:02,830
You pull a stunt like that again,

535
00:47:04,280 --> 00:47:07,070
I'll break both your arms.

536
00:47:15,480 --> 00:47:19,070
Visiontext subtitles: Marc de Jongh

