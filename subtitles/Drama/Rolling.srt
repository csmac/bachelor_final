1
00:00:00,400 --> 00:00:02,800
<font face="Tempus Sans ITC" color="#008000">      Mekziko
Proudly Presents</font>

2
00:00:03,000 --> 00:00:04,400
<b><font face="Rage Italic">         Adele
Rolling In The Deep</font></b>

3
00:00:04,600 --> 00:00:09,000
There's a fire starting in my heart

4
00:00:09,200 --> 00:00:13,200
Reaching a fever pitch, it's bringing me out the dark

5
00:00:13,400 --> 00:00:18,000
Finally I can see you crystal clear

6
00:00:18,200 --> 00:00:22,400
Go 'head and sell me out and I'll lay your shit bare

7
00:00:22,600 --> 00:00:27,000
See how I leave with every piece of you

8
00:00:27,200 --> 00:00:31,600
Don't underestimate the things that I will do

9
00:00:31,800 --> 00:00:36,400
There's a fire starting in my heart

10
00:00:36,600 --> 00:00:38,800
Reaching a fever pitch

11
00:00:39,000 --> 00:00:40,800
And it's bring me out the dark

12
00:00:42,200 --> 00:00:46,800
The scars of your love remind me of us

13
00:00:47,000 --> 00:00:51,000
They keep me thinking that we almost had it all

14
00:00:51,200 --> 00:00:56,000
The scars of your love they leave me breathless

15
00:00:56,200 --> 00:00:58,000
I can't help feeling

16
00:00:58,200 --> 00:01:02,600
We could have had it all

17
00:01:02,800 --> 00:01:06,800
Rolling in the deep

18
00:01:07,000 --> 00:01:12,400
You had my heart inside of your hands

19
00:01:12,600 --> 00:01:14,000
But you played it

20
00:01:14,600 --> 00:01:17,200
To the beat

21
00:01:18,000 --> 00:01:22,000
Baby I have no story to be told

22
00:01:22,200 --> 00:01:24,400
But I've heard one of you

23
00:01:24,600 --> 00:01:27,000
And I'm gonna make your head burn

24
00:01:27,200 --> 00:01:31,000
Think of me in the depths of your despair

25
00:01:31,400 --> 00:01:33,600
Making a home down there

26
00:01:33,800 --> 00:01:35,600
It Reminds you of the home we shared

27
00:01:37,000 --> 00:01:41,600
The scars of your love remind me of us

28
00:01:41,800 --> 00:01:45,600
They keep me thinking that we almost had it all

29
00:01:46,000 --> 00:01:51,000
The scars of your love they leave me breathless

30
00:01:51,200 --> 00:01:53,000
I can't help feeling

31
00:01:53,200 --> 00:01:57,400
We could have had it all

32
00:01:58,000 --> 00:02:01,800
Rolling in the deep

33
00:02:02,000 --> 00:02:06,600
You had my heart inside of your hands

34
00:02:07,000 --> 00:02:09,000
But you played it

35
00:02:09,600 --> 00:02:12,000
To the beat

36
00:02:12,200 --> 00:02:15,600
We could have had it all

37
00:02:15,800 --> 00:02:20,400
Rolling in the deep

38
00:02:20,600 --> 00:02:25,000
You had my heart inside of your hands

39
00:02:25,200 --> 00:02:27,600
But you played it

40
00:02:27,800 --> 00:02:29,200
To the beat

41
00:02:31,000 --> 00:02:35,000
Throw your soul through every open door

42
00:02:36,000 --> 00:02:40,000
Count your blessings to find what you look for

43
00:02:40,200 --> 00:02:44,200
Turned my sorrow into treasured gold

44
00:02:44,400 --> 00:02:49,600
You pay me back in kind and reap just what you sow

45
00:02:52,600 --> 00:02:56,600
We could have had it all

46
00:02:57,000 --> 00:03:00,800
We could have had it all

47
00:03:02,000 --> 00:03:05,000
It all, it all it all,

48
00:03:06,000 --> 00:03:10,000
We could have had it all

49
00:03:11,000 --> 00:03:14,800
Rolling in the deep

50
00:03:15,000 --> 00:03:19,600
You had my heart and soul

51
00:03:20,400 --> 00:03:21,400
But you played it

52
00:03:23,000 --> 00:03:24,800
To the beat

53
00:03:25,000 --> 00:03:29,000
We could have had it all

54
00:03:29,200 --> 00:03:33,000
Rolling in the deep

55
00:03:33,600 --> 00:03:37,400
You had my heart and soul

56
00:03:39,000 --> 00:03:40,000
But you played it

57
00:03:40,200 --> 00:03:41,000
You played it

58
00:03:41,200 --> 00:03:42,000
You played it

59
00:03:42,200 --> 00:03:45,600
You played it to the beat