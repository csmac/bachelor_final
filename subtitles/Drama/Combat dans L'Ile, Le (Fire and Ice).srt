1
00:00:26,120 --> 00:00:32,880
"Fire and Ice"

2
00:02:34,240 --> 00:02:37,360
<i>And her ambition was to extend her realm...</i>

3
00:02:37,440 --> 00:02:39,560
<i>to all the regions of the known world.</i>

4
00:02:39,640 --> 00:02:42,600
<i>To this end, she gathered an army.</i>

5
00:02:42,680 --> 00:02:46,400
<i>And she bore a son and named him Nekron.</i>

6
00:02:46,480 --> 00:02:51,000
<i>And him she tutored in the black arts
and in the powers of the mind.</i>

7
00:02:51,520 --> 00:02:54,040
<i>And when Nekron came of age...</i>

8
00:02:54,120 --> 00:02:56,760
<i>and attained mastery of those powers...</i>

9
00:02:56,800 --> 00:03:00,360
<i>together they seized control
of the region of Ice.</i>

10
00:03:00,600 --> 00:03:02,760
<i>And from their castle called Ice Peak...</i>

11
00:03:02,840 --> 00:03:05,680
<i>they sent a giant glacier
rumbling southward.</i>

12
00:03:05,760 --> 00:03:10,240
<i>No village or people could stand
against its relentless onslaught.</i>

13
00:03:10,560 --> 00:03:13,960
<i>And so the remnants of humanity
fled south...</i>

14
00:03:14,040 --> 00:03:16,880
<i>and huddled for warmth
among the volcanoes...</i>

15
00:03:16,960 --> 00:03:21,400
<i>of a mountain region
ruled by a generous king named Jarol...</i>

16
00:03:21,440 --> 00:03:24,920
<i>from his fortress,
which men called Fire Keep.</i>

17
00:03:25,280 --> 00:03:29,000
<i>And still Nekron
pushed the Ice ever southward...</i>

18
00:03:29,080 --> 00:03:32,800
<i>into the temperate zone toward Fire Keep.</i>

19
00:03:32,880 --> 00:03:36,160
<i>And no one dared guess at the outcome...</i>

20
00:03:36,200 --> 00:03:39,160
<i>of a meeting on the field of battle...</i>

21
00:03:39,200 --> 00:03:41,280
<i>between Fire and Ice.</i>

22
00:04:14,800 --> 00:04:17,560
Hold your positions, men. Steady.

23
00:04:29,080 --> 00:04:32,240
Lam, it cannot be stopped.
Join your brother.

24
00:06:38,720 --> 00:06:40,800
You have done well, my son.

25
00:06:41,520 --> 00:06:45,000
The North Village has fallen. We have won.

26
00:06:45,440 --> 00:06:47,560
I want to thank you, Mother.

27
00:06:47,960 --> 00:06:51,960
The Great Plain is all that stands
between ourselves and Fire Keep.

28
00:06:52,560 --> 00:06:54,480
That is a long distance.

29
00:06:55,200 --> 00:06:57,760
A distance we may not have to travel.

30
00:06:58,480 --> 00:07:02,120
I am sending our envoys to Jarol
with our new demands.

31
00:07:02,400 --> 00:07:05,040
- He will not submit.
- Perhaps.

32
00:07:05,200 --> 00:07:09,000
You will present our demands to King Jarol
at Fire Keep and...

33
00:07:09,760 --> 00:07:12,680
give our royal regards
to his lovely daughter.

34
00:12:23,600 --> 00:12:26,280
But, Father, I want to be with you
when you meet Nekron's men.

35
00:12:26,360 --> 00:12:29,520
You will serve your people better here
with your studies, my daughter.

36
00:12:29,600 --> 00:12:30,920
Give me your love.

37
00:12:33,200 --> 00:12:34,920
You have it always, Father.

38
00:12:49,240 --> 00:12:52,360
All matter in our world
is from the natural bases...

39
00:12:52,400 --> 00:12:55,680
which are earth, air, fire and water.

40
00:12:58,080 --> 00:13:01,320
The four elements
from which all things are created.

41
00:13:02,000 --> 00:13:06,280
How can you talk about elements
when the war draws nearer to us ever hour?

42
00:13:06,680 --> 00:13:09,800
Princess Teegra, we all have our duty.

43
00:13:09,880 --> 00:13:12,400
Your father's duty is to be king and lead.

44
00:13:12,720 --> 00:13:17,400
Your brother's duty is to soldier.
And your duty is to learn.

45
00:13:18,200 --> 00:13:20,720
You mean my duty is to stay shut up...

46
00:13:20,800 --> 00:13:24,040
while all the men do the fighting
and share all the glory.

47
00:13:25,600 --> 00:13:27,600
We think that stinks.

48
00:13:29,440 --> 00:13:30,920
Don't we, Shaitan?

49
00:13:31,280 --> 00:13:34,840
If you peacefully surrender, my lord Nekron
will cease the destruction of...

50
00:13:34,920 --> 00:13:36,440
This is your message of peace?

51
00:13:36,520 --> 00:13:39,440
A demand for our total
and unconditional surrender?

52
00:13:39,800 --> 00:13:43,040
We call it an offer of alliance, Your Majesty.

53
00:13:43,120 --> 00:13:44,960
I call it blackmail.

54
00:13:45,040 --> 00:13:46,800
My lord Nekron's offer...

55
00:13:46,880 --> 00:13:51,080
To hell with Nekron and his offer.
We are free men, not slaves.

56
00:13:52,560 --> 00:13:54,560
King Jarol, be reasonable.

57
00:13:55,360 --> 00:13:58,640
My son, Taro, speaks more with his heart
than with his head.

58
00:13:58,680 --> 00:14:00,440
But he speaks for all of us.

59
00:14:00,520 --> 00:14:02,320
There can be no alliance.

60
00:14:02,400 --> 00:14:04,680
We will fight you to the death.

61
00:14:04,920 --> 00:14:09,000
Perhaps, you will soon change your mind.

62
00:14:58,160 --> 00:14:59,200
No!

63
00:15:26,480 --> 00:15:27,680
Teegra!

64
00:15:28,320 --> 00:15:29,560
Guards!

65
00:15:42,800 --> 00:15:45,800
Make for the glacier. Man the Dragonhawks.

66
00:15:45,920 --> 00:15:47,800
Hurry!

67
00:16:07,040 --> 00:16:09,920
The Dragonhawks will find her, Father.
They've got to.

68
00:16:10,880 --> 00:16:15,360
The night's so dark, the jungle so thick.
A bird couldn't find its own nest.

69
00:16:17,760 --> 00:16:21,400
- What do we do then, Father?
- We wait.

70
00:16:21,920 --> 00:16:24,960
- For Nekron's new demands?
- For his siege!

71
00:24:42,360 --> 00:24:46,240
<i>What is your report?
Have you done as I commanded?</i>

72
00:24:46,800 --> 00:24:50,160
<i>Have you captured the girl, Teegra,
Jarol's daughter?</i>

73
00:24:51,080 --> 00:24:52,520
<i>Where is she?</i>

74
00:24:53,000 --> 00:24:55,440
<i>Bring her forth that I may see her.</i>

75
00:24:56,280 --> 00:24:58,240
<i>- Gone.
- Gone?</i>

76
00:24:59,320 --> 00:25:00,600
<i>Gone!</i>

77
00:25:00,680 --> 00:25:04,080
<i>Escaped? You filth-spawned scum!</i>

78
00:25:08,800 --> 00:25:11,440
<i>What punishment does failure demand?</i>

79
00:25:13,200 --> 00:25:15,120
Death.

80
00:25:25,040 --> 00:25:29,160
<i>Find the girl.
Find her, or his punishment shall be yours.</i>

81
00:27:55,320 --> 00:27:57,040
Gods of my father.

82
00:29:57,120 --> 00:29:58,760
What do you want?

83
00:30:12,240 --> 00:30:14,200
If you want it, take it.

84
00:30:19,200 --> 00:30:20,800
I am not a thief.

85
00:31:57,200 --> 00:31:59,400
Not so close, you'll burn it.

86
00:31:59,800 --> 00:32:01,840
I've always lived close to a fire.

87
00:32:06,000 --> 00:32:09,240
Sometimes in the night, I'm so afraid.

88
00:32:27,120 --> 00:32:28,640
It's getting colder.

89
00:32:38,640 --> 00:32:41,640
- Cold comes from Nekron.
- I know.

90
00:32:53,120 --> 00:32:55,560
- Where are you going?
- I'm going home to my people.

91
00:32:59,120 --> 00:33:01,640
- Where is home?
- Far away, to the south.

92
00:33:02,320 --> 00:33:04,720
- I'm going south, too.
- Yeah?

93
00:33:04,760 --> 00:33:07,680
You can come along if you behave yourself.

94
00:33:08,240 --> 00:33:09,640
If I behave myself?

95
00:33:13,320 --> 00:33:14,360
No.

96
00:33:56,440 --> 00:33:57,520
Larn.

97
00:35:40,320 --> 00:35:44,000
You've caught me,
but you'll never hand me over to Nekron.

98
00:35:44,080 --> 00:35:45,920
You'll have to kill me first.

99
00:35:50,000 --> 00:35:52,000
Don't hunt for death, boy.

100
00:35:53,440 --> 00:35:55,280
It finds us all soon enough.

101
00:36:00,800 --> 00:36:05,200
The travois was to keep you
from killing yourself till you're all healed.

102
00:36:06,480 --> 00:36:08,360
You need rest and food.

103
00:36:13,960 --> 00:36:18,720
If you're gonna kill the Ice Lord, boy,
you better learn to live with pain.

104
00:36:22,080 --> 00:36:25,400
Ice Lord? You mean you're fighting Nekron?

105
00:36:25,760 --> 00:36:27,200
Him and his mother.

106
00:36:28,600 --> 00:36:30,560
That wolf bitch, Juliana.

107
00:36:42,520 --> 00:36:43,720
Teegra!

108
00:36:46,480 --> 00:36:48,000
They were here.

109
00:36:48,360 --> 00:36:50,720
Nekron's dogs, they got the girl.

110
00:36:51,720 --> 00:36:53,440
They took her west.

111
00:37:03,200 --> 00:37:04,800
Guard Teegra.

112
00:37:44,320 --> 00:37:46,720
They camped less than two miles from here.

113
00:37:46,760 --> 00:37:47,880
Come.

114
00:40:10,320 --> 00:40:13,560
I saw the girl before.
She is on the far side of the big tree.

115
00:40:15,080 --> 00:40:17,800
You go get her out,
and I'll distract the others.

116
00:40:18,200 --> 00:40:21,760
- There must be 50 of them.
- That sounds about right.

117
00:41:45,240 --> 00:41:47,480
- Where's the girl?
- I couldn't find her.

118
00:42:11,720 --> 00:42:13,800
- No!
- The horses!

119
00:42:14,080 --> 00:42:16,600
No, they'd be on our heels
before we could reach them.

120
00:42:16,640 --> 00:42:19,760
- But Teegra's still back there.
- Then we'll come back for her.

121
00:42:19,840 --> 00:42:22,360
First, we lead the Nekron's dogs
on a wild chase.

122
00:42:22,400 --> 00:42:23,440
Come.

123
00:43:13,480 --> 00:43:15,600
We'll take our stand up there. Come.

124
00:43:22,640 --> 00:43:23,680
Enough!

125
00:43:24,480 --> 00:43:27,200
I want you to go to Nekron,
to his mother, Juliana.

126
00:43:28,680 --> 00:43:30,720
- We must negotiate.
- With the enemy?

127
00:43:31,680 --> 00:43:33,080
No man is wholly evil.

128
00:43:33,160 --> 00:43:35,280
Surely there is some way
to appeal to Nekron.

129
00:43:35,360 --> 00:43:36,880
You expect his mercy?

130
00:43:38,280 --> 00:43:41,960
Father, do you know what he will do
if I enter his land under a flag of truce?

131
00:43:42,000 --> 00:43:43,600
We have no choice!

132
00:43:44,040 --> 00:43:45,000
We must take that risk.

133
00:43:45,080 --> 00:43:48,640
You must take that risk, my son,
for the sake of Fire Keep.

134
00:43:49,800 --> 00:43:51,560
For the sake of your sister.

135
00:43:52,920 --> 00:43:55,640
Go, ride swiftly.

136
00:44:20,520 --> 00:44:22,560
Welcome to my hearth.

137
00:44:22,600 --> 00:44:25,760
I am Roliel and these woods are my home.

138
00:44:26,280 --> 00:44:29,000
You've had a trying journey.

139
00:44:30,160 --> 00:44:32,600
Please let me give you some food and drink.

140
00:44:32,880 --> 00:44:35,400
First, you shall rest. Then we will talk.

141
00:44:37,160 --> 00:44:39,040
Poor, poor darling.

142
00:44:39,440 --> 00:44:42,720
Did Great Otwa frighten you?

143
00:44:42,880 --> 00:44:46,920
You mustn't be afraid.
He's as gentle as a child.

144
00:44:53,320 --> 00:44:55,720
I saw him twist a chain apart
with his bare hands.

145
00:44:55,800 --> 00:44:58,680
Yes, he is strong, my sweet.

146
00:44:59,800 --> 00:45:03,480
But he means well, my darling.
He brought you to me, did he not?

147
00:45:10,280 --> 00:45:11,400
I'm so tired.

148
00:45:11,480 --> 00:45:14,920
Yes, my poor, poor darling.

149
00:45:15,600 --> 00:45:17,880
Yes, you're so tired...

150
00:45:19,520 --> 00:45:21,040
and so beautiful.

151
00:45:21,600 --> 00:45:22,800
You must...

152
00:45:42,400 --> 00:45:44,120
Tell me your secrets.

153
00:45:48,240 --> 00:45:49,200
Nekron?

154
00:45:53,320 --> 00:45:57,840
The girl is wanted by Nekron? The bitch!

155
00:45:58,240 --> 00:46:00,680
She brings danger into my house!

156
00:46:06,640 --> 00:46:09,480
Perhaps I can turn this to my advantage.

157
00:46:10,960 --> 00:46:12,280
Otwa, my son.

158
00:46:13,840 --> 00:46:17,040
Go and bring the subhumans,
Nekron's dogs.

159
00:46:17,440 --> 00:46:19,800
Tell them we have the girl that he wants.

160
00:46:22,040 --> 00:46:24,880
Tell them he can have her, for a price.

161
00:46:53,400 --> 00:46:54,360
Kill.

162
00:46:58,520 --> 00:46:59,640
Get out of here.

163
00:46:59,720 --> 00:47:02,200
- No.
- The hell you won't.

164
00:47:02,360 --> 00:47:03,680
Go find the girl.

165
00:47:39,440 --> 00:47:41,360
Enter, my son. It is not barred.

166
00:47:46,560 --> 00:47:48,920
What's wrong with you?
Where are the dogs?

167
00:47:55,640 --> 00:47:57,040
You would do this...

168
00:48:50,280 --> 00:48:52,480
Why do the living...

169
00:48:52,560 --> 00:48:55,560
disturb the sleep of the dead?

170
00:48:57,760 --> 00:49:02,120
I followed a trail. I seek a girl, Teegra.

171
00:49:03,240 --> 00:49:04,440
A girl?

172
00:49:05,440 --> 00:49:09,440
Yes. She did this to me.

173
00:49:10,040 --> 00:49:11,840
She and Nekron.

174
00:49:14,400 --> 00:49:16,440
Cursed Nekron.

175
00:49:17,040 --> 00:49:18,520
Where can I find her?

176
00:49:21,480 --> 00:49:25,720
- Why should I answer?
- For revenge against Nekron.

177
00:49:28,760 --> 00:49:30,400
Nekron?

178
00:49:32,360 --> 00:49:35,840
Akatar. They take her to Akatar.

179
00:49:36,280 --> 00:49:38,960
And from there to the Icy King's realm.

180
00:49:39,880 --> 00:49:41,240
Avenge me.

181
00:49:42,600 --> 00:49:46,000
Avenge me!

182
00:49:47,880 --> 00:49:49,240
Akatar.

183
00:50:08,720 --> 00:50:10,320
Teegra!

184
00:50:27,120 --> 00:50:28,960
My Prince, all is arranged.

185
00:50:29,160 --> 00:50:31,840
I've hired a ship. It waits for us at the dock.

186
00:50:32,040 --> 00:50:34,040
Nekron waits at Ice Peak.

187
00:50:40,480 --> 00:50:44,200
Get the gangplanks aboard.
Move, you scum!

188
00:50:44,560 --> 00:50:47,120
We must be well on our way before sunrise.

189
00:51:00,360 --> 00:51:02,440
Why have you brought me this?

190
00:51:05,040 --> 00:51:07,120
This is your bride, my son.

191
00:51:09,840 --> 00:51:11,920
She will be mother to your sons.

192
00:51:12,000 --> 00:51:14,520
How thoughtful, Mother.

193
00:51:15,400 --> 00:51:19,040
I need no bride, and no sons.

194
00:51:20,040 --> 00:51:23,280
- But you need heirs.
- I need nothing!

195
00:51:23,720 --> 00:51:24,720
Nekron?

196
00:51:32,600 --> 00:51:35,000
Nekron, you're a great power in the world.

197
00:51:36,080 --> 00:51:38,680
You have all that any man ever wants
or needed.

198
00:51:40,400 --> 00:51:41,800
And yet you despair.

199
00:51:43,120 --> 00:51:45,080
For there is one thing you lack.

200
00:51:47,240 --> 00:51:50,560
One gift that only you can bestow
between our people.

201
00:51:52,240 --> 00:51:53,200
Peace.

202
00:51:54,240 --> 00:51:57,000
This is the gift that heals the heart
of the giver.

203
00:51:58,160 --> 00:52:01,440
Nekron, I extend my hand in friendship.

204
00:52:02,120 --> 00:52:04,560
I offer peace between our people.

205
00:52:05,200 --> 00:52:06,840
Will you not take my hand?

206
00:52:11,280 --> 00:52:13,040
Will you not call me friend?

207
00:52:51,520 --> 00:52:52,480
Woman...

208
00:52:53,240 --> 00:52:56,840
I spit on peace.

209
00:52:58,080 --> 00:53:00,320
I spit on you.

210
00:53:06,280 --> 00:53:09,920
Next time you present me
with one of your little sluts, Mother dear...

211
00:53:10,040 --> 00:53:12,240
I'll squash you like a bug.

212
00:53:16,280 --> 00:53:18,280
Get that garbage out of here.

213
00:54:11,440 --> 00:54:12,720
Don't look up.

214
00:54:12,760 --> 00:54:16,080
We are riders of Fire Keep.
We pay no heed to such trash.

215
00:55:29,240 --> 00:55:33,800
We have come, Lord Nekron,
to negotiate between our two peoples.

216
00:55:34,120 --> 00:55:36,800
And the return to Fire Keep
of my sister, Teegra...

217
00:55:36,880 --> 00:55:39,320
whom your warriors basely kidnapped.

218
00:55:39,440 --> 00:55:42,840
My father, King Jarol, has empowered me...

219
00:55:43,520 --> 00:55:45,800
to offer a peace between our kingdoms...

220
00:55:46,800 --> 00:55:50,000
in exchange for his continued sovereignty...

221
00:55:50,400 --> 00:55:52,000
and my sister's freedom.

222
00:55:52,240 --> 00:55:56,800
It seems to me that my envoys made
a similar offer not long since...

223
00:55:58,280 --> 00:56:01,480
and that it was spurned
by your illustrious sire.

224
00:56:01,680 --> 00:56:04,080
You know full well, Nekron,
that offer was made...

225
00:56:04,160 --> 00:56:06,440
before your minions kidnapped my sister.

226
00:56:06,600 --> 00:56:09,840
I fear, Good Prince,
I cannot offer peace to a people...

227
00:56:09,920 --> 00:56:13,560
who have refused to proclaim me
their rightful overlord.

228
00:56:16,440 --> 00:56:18,480
As to your sister...

229
00:56:19,520 --> 00:56:22,120
I must admit that until this moment...

230
00:56:22,200 --> 00:56:26,000
the idea of mating with her
filled me with loathing.

231
00:56:26,240 --> 00:56:28,120
Perhaps I should reconsider.

232
00:56:29,360 --> 00:56:32,400
Your sister, after all,
is not wholly unattractive.

233
00:56:34,280 --> 00:56:35,960
As lesser beasts go.

234
00:56:53,080 --> 00:56:54,600
Pig you are.

235
00:56:55,400 --> 00:56:57,520
And like a pig shall you die.

236
00:58:02,480 --> 00:58:04,240
No!

237
00:58:27,680 --> 00:58:29,360
No, I don't think so.

238
00:58:41,040 --> 00:58:42,400
You interest me.

239
00:58:42,840 --> 00:58:44,000
Guards!

240
00:58:45,200 --> 00:58:46,480
Bring him to me.

241
00:58:51,880 --> 00:58:53,040
Release him.

242
00:59:03,960 --> 00:59:07,000
- Why have you come seeking me?
- You killed my people.

243
00:59:07,640 --> 00:59:10,960
We've had to dispose of
so many undesirables of late.

244
01:00:36,320 --> 01:00:40,000
Let him rest. There is still fight in him.

245
01:01:33,920 --> 01:01:35,200
Larn.

246
01:01:39,120 --> 01:01:40,520
Teegra, is that you?

247
01:01:41,960 --> 01:01:44,800
- Teegra, are you all right?
- Yes. I'm trying to unlock the door.

248
01:01:44,880 --> 01:01:46,840
Never mind the door,
you've to get out of here.

249
01:01:46,880 --> 01:01:49,600
I'm not leaving without you.
Maybe I can get a key somehow.

250
01:01:49,640 --> 01:01:51,040
No, Teegra, don't.

251
01:04:32,280 --> 01:04:33,480
Is he certain?

252
01:04:34,000 --> 01:04:37,840
- It was my son you saw?
- They called him Prince Taro.

253
01:04:38,840 --> 01:04:39,800
He died.

254
01:04:42,200 --> 01:04:45,360
- What about Teegra?
- He's got her, too.

255
01:04:45,480 --> 01:04:47,120
Maybe she's still alive.

256
01:04:47,200 --> 01:04:48,480
It's a chance.

257
01:04:48,600 --> 01:04:51,840
Believe what you want,
but that glacier is a fact.

258
01:04:54,520 --> 01:04:57,280
For three days and nights
it's been pushing south.

259
01:04:58,040 --> 01:05:01,280
A thousand souls lost
because of Nekron's rage.

260
01:05:01,960 --> 01:05:04,840
Fire Keep is next. We must attack.

261
01:05:05,400 --> 01:05:07,120
I need the Dragonhawks.

262
01:05:07,320 --> 01:05:10,640
- The Dragonhawks are too few.
- Enough to get in with.

263
01:05:10,960 --> 01:05:13,800
One us might get through and destroy him.

264
01:05:15,120 --> 01:05:18,640
But if that glacier passes beyond the river,
I will have no choice.

265
01:05:19,480 --> 01:05:21,440
I will have to release the lava.

266
01:09:31,800 --> 01:09:33,440
Teegra.

267
01:10:01,960 --> 01:10:02,920
Larn.

268
01:10:11,440 --> 01:10:13,560
Nekron!

269
01:10:52,720 --> 01:10:56,000
Die! Die! Why don't you die?

270
01:14:04,160 --> 01:14:08,280
No, Larn! No, it's over, don't you see?
We have to start over.

271
01:14:52,680 --> 01:14:54,680
"FIRE and ICE"

272
01:15:19,681 --> 01:18:14,681
Subtitles by ARAVIND B
[by_agentsmith@yahoo.com]

