1
00:00:01,400 --> 00:00:06,952
Remembering:

2
00:04:39,960 --> 00:04:41,837
Like pure gold.

3
00:04:42,960 --> 00:04:44,757
A plentiful year, Matias.

4
00:04:44,920 --> 00:04:46,831
There will be others.

5
00:04:47,080 --> 00:04:51,232
Plenty, like the moon,
wanes when it's not waxing.

6
00:04:52,880 --> 00:04:55,110
You're tempting providence.

7
00:04:55,320 --> 00:04:57,311
May be, may be...

8
00:04:58,560 --> 00:05:02,075
But in serving you, Sir,
it's not my own good I'm after.

9
00:05:02,600 --> 00:05:04,750
I thought it was.

10
00:05:07,160 --> 00:05:10,914
Have you seen Master Paio lately?

11
00:05:12,000 --> 00:05:15,788
The story goes that this year he
doubled his yield of wine and grain.

12
00:05:16,600 --> 00:05:18,431
Doubled it?

13
00:05:19,720 --> 00:05:22,314
That's ploughed land for you.

14
00:05:22,720 --> 00:05:25,757
Sow, and up comes the wheat.

15
00:05:28,240 --> 00:05:30,993
What rich fare
for an old man's eyes!

16
00:05:33,360 --> 00:05:35,954
A father's eyes see no evil.

17
00:05:36,120 --> 00:05:39,157
That's many winters
under the belt.

18
00:05:39,680 --> 00:05:41,716
And each one frostier.

19
00:05:43,200 --> 00:05:47,671
Well, my Lord, an old wolf
does better in the sheep pen.

20
00:05:47,800 --> 00:05:52,078
May be, Matias, but he only
scares the she-lamb away.

21
00:05:53,000 --> 00:05:55,514
What a lovely lass she is.

22
00:05:55,720 --> 00:05:59,554
- Our joy, Sir.
- A joy without a doubt.

23
00:06:02,400 --> 00:06:06,393
She's stretched this summer.
There's none like her in these parts.

24
00:06:06,920 --> 00:06:08,956
None at all, Matias.

25
00:06:09,160 --> 00:06:11,071
There's no other treasure
like her.

26
00:06:11,240 --> 00:06:13,595
Lovely enough to tempt the Devil.

27
00:06:13,800 --> 00:06:15,836
To cuckold him too.

28
00:06:17,880 --> 00:06:20,678
It's a crying shame
not to make a fitting marriage.

29
00:06:22,080 --> 00:06:24,310
The moment is ripe.

30
00:06:25,120 --> 00:06:30,353
I've always heard it said
that praise without marriage

31
00:06:30,600 --> 00:06:32,192
makes the maiden wilt.

32
00:06:32,360 --> 00:06:36,114
It's not just any tooth
that will bite into my S�lvia.

33
00:07:34,960 --> 00:07:37,394
You have to be married, my child.

34
00:07:38,400 --> 00:07:41,198
Master Paio de Ayres
will be fitting for you.

35
00:07:41,400 --> 00:07:44,870
He's rich, very rich,

36
00:07:45,280 --> 00:07:47,555
and feeble-minded.

37
00:07:48,160 --> 00:07:51,277
Perhaps settling
into this new estate

38
00:07:53,240 --> 00:07:54,992
will not displease you.

39
00:07:55,160 --> 00:07:58,152
But if it does,

40
00:07:58,600 --> 00:08:01,353
you will always
be right and stronger

41
00:08:01,560 --> 00:08:04,154
in your will and thinking.

42
00:08:06,080 --> 00:08:09,470
I doubt not the honor
of your bearing,

43
00:08:09,680 --> 00:08:12,990
then, more than to your husband,

44
00:08:13,400 --> 00:08:15,960
you will be faithful
to these old bones.

45
00:08:57,440 --> 00:08:59,237
It will be fine pig-sticking
this year.

46
00:08:59,400 --> 00:09:01,868
The hog is born aloft
like a processional saint.

47
00:09:02,040 --> 00:09:04,679
- And all strussed up.
- Like a sausage.

48
00:09:04,800 --> 00:09:08,349
A sausage, and a well-filled one.
Look at the fat dripping out.

49
00:09:08,520 --> 00:09:11,956
I'd say he's purple and puffed
out like a wineskin.

50
00:09:13,520 --> 00:09:15,192
What a freak!

51
00:09:15,360 --> 00:09:18,670
So many hefty fellows
to accompany a sack of farts.

52
00:10:53,880 --> 00:10:55,598
He's here.

53
00:11:27,520 --> 00:11:30,193
Welcome to what is your home.

54
00:11:30,560 --> 00:11:32,710
Rest from the journey

55
00:11:32,920 --> 00:11:36,435
that from your appearance
must have been a long one.

56
00:11:36,600 --> 00:11:40,639
The partridges I gnawed on to while
away the journey did me no good.

57
00:11:40,800 --> 00:11:43,268
I stopped and I had a crap.
Now I'm better.

58
00:11:50,760 --> 00:11:54,355
Beautiful woods and gentle
little oak trees you have here.

59
00:11:54,520 --> 00:11:57,796
Sweat of my brow, my son.

60
00:11:58,480 --> 00:12:01,756
Now sup after such a heavy trip.

61
00:12:01,920 --> 00:12:05,037
Not forgetting the ham
that will pick me up.

62
00:12:13,520 --> 00:12:16,592
Apart from cunt there's no smell
like of smoked sausage.

63
00:12:22,840 --> 00:12:26,116
Be at home, my son.
How's that for grape nectar?

64
00:12:26,440 --> 00:12:30,877
It's got a kick. And the ham
is the finest, well seasoned.

65
00:12:31,520 --> 00:12:35,035
It's wild boar.
Matias here killed it.

66
00:12:36,000 --> 00:12:38,878
Eat and drink while I see to S�lvia.

67
00:12:39,000 --> 00:12:41,753
Go on. There's no hurry.

68
00:12:43,320 --> 00:12:47,518
Don't stand there glassy-eyed. Open
up the trunk and show the fineries.

69
00:12:48,200 --> 00:12:50,668
Hey, Matias, drink up!

70
00:12:53,280 --> 00:12:56,431
Will the girls not taste a morsel?

71
00:12:56,920 --> 00:12:58,876
You there!

72
00:12:59,080 --> 00:13:03,358
- Just a bite to melt in the mouth.
- I've already eaten, Sir.

73
00:13:05,880 --> 00:13:08,155
She's eaten, but she didn't fatten.

74
00:13:10,520 --> 00:13:13,159
That's Mistress Silvia's
half-sister.

75
00:13:13,360 --> 00:13:16,033
Half of the father or whoever
cuckolded him?

76
00:13:16,200 --> 00:13:18,191
Of Don Rodrigo!

77
00:13:18,560 --> 00:13:20,790
He can teach a lesson
to many a lad around here.

78
00:13:20,920 --> 00:13:24,879
A joke isn't an offence.
She's a nice half portion of flesh.

79
00:13:25,040 --> 00:13:27,474
Is the bride the same?

80
00:13:29,560 --> 00:13:32,438
Ham next to the bone is tastier.

81
00:13:33,560 --> 00:13:36,393
I won't dispute that, Matias.

82
00:13:36,920 --> 00:13:39,593
One must liven the spirit

83
00:13:40,480 --> 00:13:43,153
while the belly settles.

84
00:13:47,200 --> 00:13:50,033
Here, girl, take this kerchief.

85
00:13:51,400 --> 00:13:53,550
Coy little thing!

86
00:13:53,960 --> 00:13:56,520
Don't be asked twice, lass.

87
00:13:57,440 --> 00:14:00,432
And there's a lot more to come yet.

88
00:14:03,560 --> 00:14:07,838
For you, this is more lady-like.
Feel this for quality.

89
00:14:08,000 --> 00:14:11,470
This is for maids.
I'm too old for dancing.

90
00:14:11,720 --> 00:14:15,793
- Where's there's life there's hope.
- She's long in the tooth for it.

91
00:14:15,960 --> 00:14:18,349
Ask your wife
who has a squeeze of her!

92
00:14:18,480 --> 00:14:20,869
Peace and order in this house.

93
00:14:21,160 --> 00:14:25,915
For Matias,
homespun for a doublet.

94
00:14:26,120 --> 00:14:29,351
Such a weak physique
isn't worth your trouble.

95
00:14:29,560 --> 00:14:31,790
Let me wrap it round you,

96
00:14:32,000 --> 00:14:34,230
pull it in tight.

97
00:14:35,440 --> 00:14:39,035
Look at this, Matias...
It's dazzling!

98
00:14:40,280 --> 00:14:45,229
Stick out those little breasts...
Honey turns golden better in the sun.

99
00:14:45,920 --> 00:14:48,718
Don't be shy, little dove.
We're at home!

100
00:14:48,880 --> 00:14:52,270
A bit of color for sore eyes.

101
00:14:52,520 --> 00:14:54,829
Ah, here comes the bride!

102
00:15:00,080 --> 00:15:02,435
My daughter, S�lvia.

103
00:15:04,240 --> 00:15:08,028
They have often sung your praises.
And I haven't been led astray.

104
00:15:08,600 --> 00:15:11,831
- I entrust you with her, my son.
- She will be in good hands.

105
00:15:12,040 --> 00:15:14,793
Let me finish distributing
my modest gifts

106
00:15:14,960 --> 00:15:18,032
in gratitude for so honorable
a reception.

107
00:15:19,080 --> 00:15:23,039
For you, whom I already esteem
as my sister,

108
00:15:23,920 --> 00:15:27,879
some fine lace.
Look at the delicacy of the work.

109
00:15:29,320 --> 00:15:32,835
The work of fairies
for a fairy herself.

110
00:15:38,640 --> 00:15:41,712
I don't know how to thank you
for your kindness.

111
00:15:41,840 --> 00:15:44,912
You will know in time,
little sister.

112
00:15:45,560 --> 00:15:48,996
For you, father,
this gown from distant...

113
00:15:50,000 --> 00:15:51,831
Venice.

114
00:15:52,000 --> 00:15:56,278
It's too fine for a clod
from these wild parts.

115
00:15:56,840 --> 00:16:00,753
But I promise you it will shine
at your wedding feast.

116
00:16:03,360 --> 00:16:07,672
Finally, the gift for the bride.

117
00:16:08,400 --> 00:16:10,038
I was in difficulties,

118
00:16:10,200 --> 00:16:13,875
they told me the bridegroom
was the best gift for the bride.

119
00:16:14,280 --> 00:16:17,397
I didn't listen.
So I've brought this ring.

120
00:16:17,600 --> 00:16:20,910
Simple but precious.

121
00:16:21,520 --> 00:16:25,638
On your gentle finger I place it.

122
00:16:32,640 --> 00:16:34,596
I will keep it, Sir.

123
00:16:52,200 --> 00:16:54,839
Won't one of you girls
help me out with this morsel?

124
00:16:55,000 --> 00:16:57,275
We're not hungry yet.

125
00:16:58,040 --> 00:16:59,792
Not even a little wing?

126
00:16:59,920 --> 00:17:02,229
Don't you do anything
else but chew?

127
00:17:02,400 --> 00:17:04,630
When I sleep I'm not gnawing.

128
00:17:04,800 --> 00:17:08,395
But when I empty my bowels
I have to fill my belly again.

129
00:17:09,240 --> 00:17:11,435
With chicken,

130
00:17:12,560 --> 00:17:15,438
my delight is the thigh.

131
00:17:16,240 --> 00:17:19,676
I roll it between my fingers,
like this,

132
00:17:20,840 --> 00:17:23,070
and suck it...

133
00:17:25,000 --> 00:17:27,275
Until only the bone is left.

134
00:17:44,000 --> 00:17:47,276
Mistress Silvia's sister
is as slender as a willow.

135
00:17:47,400 --> 00:17:49,516
But she doesn't bend.

136
00:17:49,680 --> 00:17:51,875
Do you think I'm crossed-eyed?

137
00:17:52,240 --> 00:17:54,959
She's got no lack of breasts.

138
00:17:55,400 --> 00:17:58,597
And how well
she rolls her hips!

139
00:17:58,760 --> 00:18:00,239
Don Paio!

140
00:18:00,360 --> 00:18:03,272
Leave him be, S�lvia.
He's a bit silly.

141
00:18:04,480 --> 00:18:06,914
A respectable girl doesn't listen.

142
00:18:07,440 --> 00:18:09,908
I didn't mean any harm, girls.

143
00:18:12,320 --> 00:18:14,470
I didn't get any court education,

144
00:18:14,800 --> 00:18:17,872
and I've never had anything
to do with girls of your level.

145
00:18:18,040 --> 00:18:21,191
- What do you say to those you know?
- Not much.

146
00:18:21,440 --> 00:18:24,034
I lose my tongue
and I just manager to stutter:

147
00:18:24,200 --> 00:18:27,317
"get into the right position!"
- And do they?

148
00:18:27,440 --> 00:18:31,718
- Oh, yes. And why not?
- And you are going to marry...

149
00:18:32,720 --> 00:18:36,076
- Your father fixed everything.
- And don't you want to?

150
00:18:36,240 --> 00:18:41,394
Yes, but my fat carcass
doesn't please Mistress S�lvia.

151
00:18:42,560 --> 00:18:44,949
For a toad, a frog,
goes the saying.

152
00:18:45,120 --> 00:18:49,716
You're exaggerating. I shall honor
the commitments of this family.

153
00:18:50,200 --> 00:18:52,760
May God hear you, Mistress...

154
00:18:52,920 --> 00:18:56,469
For my part, I want to cover
this land with little Paios.

155
00:19:42,440 --> 00:19:46,149
Now that Don Paio
has left us for a while

156
00:19:47,080 --> 00:19:50,550
and peace has returned
to this household,

157
00:19:52,600 --> 00:19:55,717
I must announce my departure
for the court.

158
00:19:56,600 --> 00:19:59,160
What trials await you, father?

159
00:19:59,360 --> 00:20:01,396
Nothing serious.

160
00:20:02,320 --> 00:20:06,677
I want the King to honor your
wedding feast with his presence.

161
00:20:08,320 --> 00:20:11,153
I shall leave at daybreak.

162
00:20:12,240 --> 00:20:16,279
I leave the care of the house with
your sister who is well used to it.

163
00:20:16,400 --> 00:20:18,277
I am, Sir.

164
00:20:19,120 --> 00:20:21,680
If the weather doesn't upset things,

165
00:20:22,400 --> 00:20:26,712
this will be a fine year for wine.
- The grapes are turning golden.

166
00:20:27,360 --> 00:20:29,999
I hope to be back
for the grape harvest.

167
00:20:30,360 --> 00:20:32,510
But if I'm not,

168
00:20:32,680 --> 00:20:35,911
see to the workers
and the presses.

169
00:20:39,240 --> 00:20:43,552
I leave with pride and trust
in you.

170
00:20:44,240 --> 00:20:47,550
Go in peace, father.
Everything will go for the best.

171
00:20:47,720 --> 00:20:49,711
That's as I feel.

172
00:20:50,560 --> 00:20:52,994
Open the doors to no one.

173
00:20:53,920 --> 00:20:56,354
The roads are full of rogues

174
00:20:57,400 --> 00:21:00,392
and you are not well guarded.

175
00:21:03,280 --> 00:21:05,191
Your blessing.

176
00:21:06,280 --> 00:21:07,793
Your blessing.

177
00:21:07,960 --> 00:21:11,270
I bless you and the rest
of the household.

178
00:21:11,920 --> 00:21:14,718
Susana, take care of your sister.

179
00:21:15,400 --> 00:21:17,914
See that she doesn't get
overtired.

180
00:21:18,080 --> 00:21:20,958
Go in peace, Sir.
May God go with you.

181
00:21:21,520 --> 00:21:25,149
Salt in the meat,
cattle in their pens

182
00:21:26,480 --> 00:21:28,789
and the bolt on the door.

183
00:21:51,760 --> 00:21:53,671
Wake up, S�lvia.

184
00:22:11,640 --> 00:22:15,235
- What if we went to the river?
- There are shirts to wash...

185
00:22:17,200 --> 00:22:19,191
Let's go to the river.

186
00:22:45,640 --> 00:22:49,030
I could die from love, I could.

187
00:22:49,200 --> 00:22:51,156
Be quiet, witch.
Nobody dies from love.

188
00:22:51,320 --> 00:22:53,515
- Have you washed the underclothes?
- Yes.

189
00:22:53,680 --> 00:22:56,672
- And the shirts?
- I've washed them white.

190
00:22:56,960 --> 00:22:59,952
Ah, I could die of love, I could.

191
00:23:00,520 --> 00:23:03,273
Be quiet, broody hen.
May your mother marry you off.

192
00:23:03,520 --> 00:23:06,990
And the stains of blood on the shirt?
- It's bleached out in the sun.

193
00:23:07,160 --> 00:23:09,151
That's marriage for you.

194
00:23:09,280 --> 00:23:12,158
I could die of love, I could...

195
00:23:12,320 --> 00:23:14,515
Be quiet, witch, and wash!

196
00:23:14,680 --> 00:23:17,638
Is your lad jolly?
Does he play the pipes?

197
00:23:17,920 --> 00:23:20,832
You're more likely
to cry than die.

198
00:23:21,160 --> 00:23:24,675
You'll wash out men's seed
and afterbirth like the rest.

199
00:23:26,600 --> 00:23:28,750
He's all we needed...

200
00:23:28,920 --> 00:23:30,911
He comes and goes
and no wolf grabs him.

201
00:23:31,040 --> 00:23:33,235
- Change the tune.
- Is there a better one?

202
00:23:33,360 --> 00:23:36,909
I saw you coming in late.
Your father'll beat you.

203
00:23:37,040 --> 00:23:38,837
If he finds out!

204
00:23:39,000 --> 00:23:41,878
- Your mother knows.
- She holds her tongue.

205
00:23:42,040 --> 00:23:45,237
She knows who washes the
nappies and sees to the children.

206
00:23:45,920 --> 00:23:47,194
Woe is me!

207
00:23:47,360 --> 00:23:49,954
Be quiet, loud mouth.
You'll spread your woe.

208
00:23:50,120 --> 00:23:52,111
- I'm in love.
- Who isn't?

209
00:23:52,240 --> 00:23:55,152
You speak as if you were the only one.
- And I am.

210
00:23:55,840 --> 00:23:58,798
Wretch! What lover wants you?

211
00:23:59,000 --> 00:24:03,039
Mind the shirt or the wind will
take it, or the river will have it.

212
00:24:03,200 --> 00:24:06,112
- Never mind. I'm in love.
- Wretch!

213
00:24:19,640 --> 00:24:21,232
Sister,

214
00:24:22,760 --> 00:24:26,036
what light is this
that seems to fear light itself?

215
00:24:27,920 --> 00:24:32,277
- Never has day seemed so calm to me.
- Calm before what?

216
00:24:33,880 --> 00:24:36,189
Before night.

217
00:24:38,240 --> 00:24:41,789
The air is still and dark.

218
00:24:43,360 --> 00:24:46,557
- Perhaps night will not come.
- What ever next!

219
00:24:46,720 --> 00:24:49,632
Leave this gloomy stories
for the threshing ground.

220
00:24:51,200 --> 00:24:55,159
Nothing is more eerie than silence
when there's no man in the house.

221
00:24:55,280 --> 00:24:56,793
Be quiet.

222
00:24:57,920 --> 00:24:59,876
Do you ear that?

223
00:25:02,520 --> 00:25:07,355
No, nothing... It's the murmur
of the day passing.

224
00:25:10,040 --> 00:25:12,031
Where does the day go to?

225
00:25:12,200 --> 00:25:15,749
It passes into the night that gives
birth to the next day.

226
00:25:17,640 --> 00:25:19,835
Your fear is making me frightened.

227
00:25:20,000 --> 00:25:23,834
We are always frightened
at some time... The heart is dark.

228
00:25:25,040 --> 00:25:26,917
Someone is coming.

229
00:25:27,080 --> 00:25:30,311
May he come before night falls.

230
00:25:49,360 --> 00:25:52,113
Father told us
not to open to anyone.

231
00:25:56,840 --> 00:26:00,355
I am a pilgrim, lady, a traveler.

232
00:26:01,600 --> 00:26:04,637
Blessed be the roof that shelters me.

233
00:26:06,000 --> 00:26:09,197
- Bread and shelter.
- Poor man...

234
00:26:11,040 --> 00:26:14,874
- He looks God fearing.
- But he has a grim look.

235
00:26:20,600 --> 00:26:22,875
Let him in, sister dear.

236
00:26:23,040 --> 00:26:25,076
Bread and shelter.

237
00:26:31,800 --> 00:26:34,075
Come in, night is falling.

238
00:26:41,280 --> 00:26:43,999
The Lord be praised,
and you blessed.

239
00:26:58,680 --> 00:27:01,148
Have you come from Santiago?

240
00:27:01,560 --> 00:27:03,676
From Compostela.

241
00:27:03,840 --> 00:27:06,308
Have you been long
on a pilgrimage?

242
00:27:08,480 --> 00:27:10,596
And what have you seen?

243
00:27:11,880 --> 00:27:16,032
I have seen the anguish of men,
and the stars turning.

244
00:27:17,040 --> 00:27:20,032
Have you seen any miracles
or strange things?

245
00:27:21,200 --> 00:27:23,350
I have seen all that.

246
00:27:25,680 --> 00:27:28,274
And Christ's blood
spilt on the highways.

247
00:27:28,440 --> 00:27:33,309
Oh, suffering... God spare us
from such pain.

248
00:27:34,720 --> 00:27:37,393
The Lord knows
how to protect you.

249
00:27:39,880 --> 00:27:42,189
You are grave.

250
00:27:42,440 --> 00:27:46,035
You must be tired and with little
patience for our questions.

251
00:27:47,560 --> 00:27:50,916
Always question.
To question is to move forward.

252
00:27:52,920 --> 00:27:55,878
We have seen little
of the world in these parts.

253
00:27:57,800 --> 00:28:01,998
Only one world seen in the light
of a face, reflects all faces.

254
00:28:06,840 --> 00:28:09,559
You may rest here tonight.

255
00:28:11,400 --> 00:28:13,630
Heavenly thanks, lady.

256
00:28:16,160 --> 00:28:18,674
I shall leave at dawn
for distant lands.

257
00:28:18,840 --> 00:28:21,070
What route will you take?

258
00:28:21,960 --> 00:28:24,599
The route of the stars.

259
00:28:24,800 --> 00:28:27,439
May the Lord be with you.

260
00:28:31,840 --> 00:28:34,229
First take these oranges.

261
00:28:39,320 --> 00:28:41,880
In payment of your goodness.

262
00:28:43,000 --> 00:28:47,152
They are from sun-drenched lands,
sweet fruit of the light.

263
00:28:54,640 --> 00:28:57,438
God repay you
for such a precious gift.

264
00:29:06,960 --> 00:29:09,394
My sister and I will retire.

265
00:32:23,000 --> 00:32:26,436
<i>Sweet was the orange</i>

266
00:32:26,640 --> 00:32:30,155
<i>Sweet it was and not bitter</i>

267
00:32:31,480 --> 00:32:37,032
<i>Where did you bring it from?</i>
<i>So lovely and so cherished?</i>

268
00:32:38,360 --> 00:32:40,590
<i>Where did you bring it from?</i>

269
00:32:40,800 --> 00:32:45,954
<i>So lovely and so cherished?</i>

270
00:32:47,360 --> 00:32:51,035
<i>I brought it from the Moors</i>

271
00:32:51,200 --> 00:32:55,751
<i>Where it was well guarded.</i>

272
00:32:56,800 --> 00:32:59,109
<i>I brought it at night,</i>

273
00:32:59,280 --> 00:33:03,353
<i>In daylight I didn't dare.</i>

274
00:33:04,120 --> 00:33:06,156
<i>I brought it at night,</i>

275
00:33:06,360 --> 00:33:11,434
<i>In daylight I didn't dare.</i>

276
00:33:13,200 --> 00:33:16,510
<i>I brought it as a slave</i>

277
00:33:16,720 --> 00:33:21,236
<i>I didn't bring it as my love.</i>

278
00:33:22,400 --> 00:33:24,755
<i>I brought it barefoot</i>

279
00:33:24,920 --> 00:33:28,913
<i>From where it was shod.</i>

280
00:33:29,920 --> 00:33:31,956
<i>I brought it naked</i>

281
00:33:32,120 --> 00:33:37,513
<i>From where it was well-clad.</i>

282
00:33:37,680 --> 00:33:41,070
Susana, sister, wake up!

283
00:33:43,200 --> 00:33:46,272
What sleep is this
and what happiness,

284
00:33:46,480 --> 00:33:49,153
so blind and deaf
to my affliction?

285
00:34:54,520 --> 00:34:57,318
Open up! Return the light to me.

286
00:34:57,600 --> 00:34:58,874
What light?

287
00:34:59,040 --> 00:35:02,157
- The hand that is of my kin.
- And does it burn?

288
00:35:02,320 --> 00:35:05,232
It burns but it doesn't harm,
it is a blessed hand.

289
00:35:05,400 --> 00:35:07,152
- Open up!
- No!

290
00:35:07,320 --> 00:35:08,833
It must be the hand
of the Devil.

291
00:35:09,000 --> 00:35:12,310
No, lady. Open up
if you wish to know.

292
00:35:12,640 --> 00:35:16,679
I do. Put your hand through
the window and I will return it.

293
00:35:17,400 --> 00:35:19,118
May you be blessed.

294
00:35:40,000 --> 00:35:43,754
Jesus help us.
You're covered in blood!

295
00:35:46,280 --> 00:35:48,874
Have you been injured,
dearest sister?

296
00:35:56,640 --> 00:35:59,757
What hand is this lying here,
most Holy Virgin?

297
00:36:03,040 --> 00:36:05,508
Did you cut it off
with the sword?

298
00:36:07,400 --> 00:36:10,358
Horror of horrors!

299
00:36:13,880 --> 00:36:17,509
Woe is me that I couldn't help you
because of the drugged orange.

300
00:36:20,240 --> 00:36:22,595
And the one he gave to you?

301
00:36:24,760 --> 00:36:27,194
I can see you didn't eat it...

302
00:36:28,400 --> 00:36:31,710
Was that our Lord's will
for the salvation of us both?

303
00:36:56,840 --> 00:36:59,479
Ladies, was there some
trouble here?

304
00:36:59,760 --> 00:37:02,991
We heard howling and noises,
as of wolves or were-wolves.

305
00:37:08,760 --> 00:37:11,752
Be calm, I heard them too.

306
00:37:12,000 --> 00:37:14,878
It must have been the wind.
What else?

307
00:37:15,880 --> 00:37:18,155
There are signs
of hoof prints, Mistress.

308
00:37:18,320 --> 00:37:22,233
Horsemen perhaps,
sheltering from the storm.

309
00:37:22,400 --> 00:37:26,598
There are traces of blood
on the steps. Beware.

310
00:37:26,800 --> 00:37:30,315
Go, my friends, and many
thanks for your care.

311
00:37:30,920 --> 00:37:33,639
My father will know of your zeal.

312
00:37:34,320 --> 00:37:37,949
Go in peace, or you will disturb
my sister's sleep.

313
00:38:11,960 --> 00:38:14,872
Come out of your shock,
S�lvia. They've gone.

314
00:38:16,560 --> 00:38:18,869
What's done is done.

315
00:38:22,960 --> 00:38:26,589
Tell me there was no villainy.
That it was all a dream.

316
00:38:29,960 --> 00:38:32,679
My soul is steeped in blood,
S�lvia.

317
00:38:39,280 --> 00:38:41,475
Don't stay dumb.

318
00:38:41,880 --> 00:38:43,677
Speak to me.

319
00:38:47,720 --> 00:38:50,837
Tell me nothing happened.
Nothing.

320
00:38:51,120 --> 00:38:53,031
I order you to speak!

321
00:38:53,240 --> 00:38:55,913
Forgive me, I no longer know
what I'm saying.

322
00:39:00,880 --> 00:39:02,950
You're frozen...

323
00:40:48,760 --> 00:40:50,557
Let me lie you down.

324
00:42:32,240 --> 00:42:34,595
For the love of God, speak!

325
00:42:38,640 --> 00:42:40,995
Open your heart to me.

326
00:42:45,960 --> 00:42:49,157
Woe is me that I couldn't help
you in such an hour of need.

327
00:42:50,920 --> 00:42:53,115
I knew of nothing,

328
00:42:53,600 --> 00:42:56,319
as if death were on me.

329
00:43:00,920 --> 00:43:03,388
What will father say if he sees you
dumb and in shock

330
00:43:03,560 --> 00:43:05,869
as if you had seen Evil itself?

331
00:43:06,040 --> 00:43:07,359
I saw it.

332
00:43:07,520 --> 00:43:10,671
- Are you sure?
- As plain as I see myself.

333
00:43:12,040 --> 00:43:14,190
Don't touch me.

334
00:43:14,640 --> 00:43:17,712
Not another word on what happened.

335
00:43:17,920 --> 00:43:20,195
What is over is over.

336
00:43:20,800 --> 00:43:24,315
Don't touch me. I beg you.
Don't touch me.

337
00:45:05,080 --> 00:45:07,799
S�lvia! Father has arrived!

338
00:45:38,080 --> 00:45:41,277
Hey, do you want to scald me?

339
00:45:41,520 --> 00:45:43,590
Put in more cold!

340
00:45:50,320 --> 00:45:52,788
That's right. That's enough.

341
00:46:00,160 --> 00:46:04,711
Take the dust of the roads
and the pomp of the court off me.

342
00:46:08,640 --> 00:46:11,712
Look at my little S�lvia,

343
00:46:13,000 --> 00:46:15,753
still smelling of must.

344
00:46:17,160 --> 00:46:19,435
How good is it
to have you back, father.

345
00:46:19,640 --> 00:46:21,915
I can see the journey
went well.

346
00:46:22,400 --> 00:46:25,312
The returning
rather than the going.

347
00:46:26,120 --> 00:46:29,908
An old wolf only feels well
approaching his lair.

348
00:46:30,800 --> 00:46:34,076
- And what's been going on here?
- Nothing, father.

349
00:46:34,360 --> 00:46:38,638
The work and the days passing. We've
brought you a taste of the new wine.

350
00:46:39,640 --> 00:46:41,676
Well, bring it to me, then!

351
00:46:41,840 --> 00:46:45,594
Let's see if the daughter's wine
turns her father's head.

352
00:47:11,760 --> 00:47:13,637
Heavens!

353
00:47:15,200 --> 00:47:19,318
With a household such as this,
a man can die in peace.

354
00:47:21,160 --> 00:47:23,754
We will water your wedding
feast with this.

355
00:47:27,600 --> 00:47:30,433
The union of two honorable
houses

356
00:47:30,720 --> 00:47:34,030
can only mean greater prosperity
for all of us.

357
00:47:36,160 --> 00:47:38,151
Seated here at this table

358
00:47:38,320 --> 00:47:42,233
in harmony
we can enjoy this peace.

359
00:47:44,800 --> 00:47:46,916
Let winter come,

360
00:47:47,200 --> 00:47:49,794
but gently.

361
00:47:50,760 --> 00:47:54,639
Let death take me,
but in Holy peace.

362
00:48:02,520 --> 00:48:07,640
Yours is the fruit
of the seed I have sown.

363
00:48:10,200 --> 00:48:14,830
Before my eyes see their last,

364
00:48:15,800 --> 00:48:18,712
I want only the joy
of having next to me

365
00:48:19,680 --> 00:48:20,795
your son.

366
00:48:20,960 --> 00:48:24,794
Don't worry. We will give
you a fine nestful.

367
00:48:24,960 --> 00:48:26,678
I'm sure of that.

368
00:48:26,920 --> 00:48:29,115
To Don Rodrigo's health!

369
00:48:29,320 --> 00:48:32,039
Long life to Don Rodrigo!

370
00:48:34,680 --> 00:48:36,875
Life is for the young.

371
00:48:38,240 --> 00:48:40,276
A toast to the bridal pair!

372
00:49:14,920 --> 00:49:17,798
Enter and be at ease, Sir.

373
00:49:25,680 --> 00:49:29,514
Forgive my intrusion,
but I have ridden far.

374
00:49:29,840 --> 00:49:33,389
You arrive at a fine time.
The table is set.

375
00:49:33,640 --> 00:49:35,995
It would be an offence
to decline.

376
00:49:37,200 --> 00:49:40,476
When in Rome do as the Romans do.

377
00:49:42,360 --> 00:49:46,717
We are simple
but clean and honest folk.

378
00:49:51,960 --> 00:49:53,712
Have some of this ham.

379
00:49:53,880 --> 00:49:57,077
It's divine ham, not to taste it
would invite God's anger.

380
00:49:57,280 --> 00:50:00,397
- Some venison?
- It is not food that I lack, Sir.

381
00:50:01,160 --> 00:50:03,390
Fill his cup, my son.

382
00:50:15,040 --> 00:50:17,600
Wine made by my S�lvia.

383
00:50:33,560 --> 00:50:35,994
It is strong but smooth.

384
00:50:38,240 --> 00:50:40,834
And with a bouquet like no other.

385
00:50:41,560 --> 00:50:43,551
It has the soul
of the one who made it.

386
00:50:43,720 --> 00:50:45,950
Do you hear, S�lvia?

387
00:50:46,320 --> 00:50:49,278
My little pippin is blushing.

388
00:50:51,040 --> 00:50:55,955
The knight's kindness
praises the merits of a maiden

389
00:50:56,120 --> 00:50:58,190
without knowing her defects,

390
00:50:58,360 --> 00:51:01,158
leaving her grateful but confused.

391
00:51:02,160 --> 00:51:04,390
<i>Eppur si muove...</i>

392
00:51:05,040 --> 00:51:06,951
What a fine wedding feast!

393
00:51:07,120 --> 00:51:09,953
I'm proud to be groom
to such a gentle bride.

394
00:51:10,600 --> 00:51:12,875
To the bride and groom!

395
00:51:13,040 --> 00:51:15,076
Wine for everybody!

396
00:51:22,000 --> 00:51:27,552
Sir, it is not usual
to offend a guest

397
00:51:28,080 --> 00:51:30,640
with indiscreet questions,

398
00:51:30,840 --> 00:51:34,594
but we can all see

399
00:51:35,080 --> 00:51:37,878
that at this merry wedding

400
00:51:38,080 --> 00:51:41,117
you remain taciturn.

401
00:51:42,480 --> 00:51:45,950
Is it in my power
to lighten your thoughts?

402
00:51:46,400 --> 00:51:49,676
What ponderous reason is there
for your visit?

403
00:51:51,280 --> 00:51:53,714
I have come for your daughter.

404
00:51:56,280 --> 00:51:59,272
The conversation becomes bitter,
the air chilled.

405
00:52:11,520 --> 00:52:13,590
It is too late.

406
00:52:14,800 --> 00:52:19,510
My daughter is already given
and I do not go back on my word.

407
00:52:21,520 --> 00:52:25,274
Under different circumstances I would
be grateful for your consideration,

408
00:52:25,720 --> 00:52:30,077
but I feel you jest

409
00:52:30,960 --> 00:52:34,316
with the good will at this table.

410
00:52:35,440 --> 00:52:38,512
It is not my habit to jest
with a maiden's honor.

411
00:52:39,720 --> 00:52:42,996
I have come for your daughter
and will do everything to take her.

412
00:52:43,640 --> 00:52:46,712
- Name your conditions.
- Hand her over, father.

413
00:52:46,960 --> 00:52:51,078
He'll want to fight and I feel sick
at the approaching blood.

414
00:52:51,240 --> 00:52:54,391
Be quiet, no one will draw
your blood.

415
00:52:59,360 --> 00:53:01,316
Please, listen.

416
00:53:01,800 --> 00:53:05,509
My daughter brought up
a ferocious dragon

417
00:53:05,800 --> 00:53:08,109
that is entirely devoted to her.

418
00:53:08,320 --> 00:53:11,710
No human can overcome
this terrible monster.

419
00:53:12,280 --> 00:53:15,670
If you are prepared to pay
for your daring with your life,

420
00:53:16,320 --> 00:53:18,390
kill the dragon

421
00:53:18,960 --> 00:53:21,918
and my daughter is yours.

422
00:53:22,480 --> 00:53:26,189
At the first light of day
I will fight the dragon.

423
00:53:26,520 --> 00:53:30,513
Father, do not encourage
this bloody battle.

424
00:53:30,960 --> 00:53:34,794
Neither saint nor demon
can overcome the dragon.

425
00:53:39,240 --> 00:53:42,198
A joyous wedding feast
for such beauty.

