﻿1
00:02:13,600 --> 00:02:14,965
I'm home

2
00:02:17,971 --> 00:02:21,031
I should've listened to you
and taken my umbrella

3
00:02:21,741 --> 00:02:23,333
I'm soaking wet

4
00:02:25,078 --> 00:02:29,242
So I called him outside on his
lunch break and I told him,

5
00:02:29,415 --> 00:02:32,782
"There's lots of guys waiting
to take your place"

6
00:02:33,119 --> 00:02:36,054
Right at the busiest time of the lunch hour,

7
00:02:36,156 --> 00:02:38,886
he was just spacing out in the kitchen

8
00:02:39,259 --> 00:02:41,056
He's totally not helpful

9
00:02:41,995 --> 00:02:44,759
Who does he think runs that place?

10
00:02:48,501 --> 00:02:49,661
What?

11
00:02:50,870 --> 00:02:54,499
I think he's 3 years younger than me

12
00:02:55,575 --> 00:02:58,669
That's right, he's an only child

13
00:02:58,778 --> 00:03:02,305
His parents totally spoiled him
Unbelievable

14
00:03:03,349 --> 00:03:05,510
Well, who cares about him anyway

15
00:03:11,524 --> 00:03:15,790
What do you think?
It's so late already

16
00:03:16,296 --> 00:03:18,230
Skip the bath tonight?

17
00:03:34,480 --> 00:03:38,382
Nozomi...
You're beautiful

18
00:03:55,034 --> 00:03:56,661
Nozomi...

19
00:05:35,735 --> 00:05:37,202
Welcome

20
00:05:39,138 --> 00:05:42,471
Welcome
Yes, just a moment

21
00:05:45,678 --> 00:05:47,111
Welcome

22
00:05:52,652 --> 00:05:54,313
It's so cold

23
00:05:56,656 --> 00:05:58,317
You're beautiful today, too

24
00:06:02,061 --> 00:06:03,528
I'm off

25
00:07:49,402 --> 00:07:54,362
Beau-ti-ful

26
00:08:14,627 --> 00:08:19,894
Beau-ti-ful

27
00:08:37,049 --> 00:08:42,919
AIR DOLL

28
00:10:15,748 --> 00:10:18,683
Moe! Hey, wait!

29
00:10:19,818 --> 00:10:21,213
I forgot

30
00:10:21,387 --> 00:10:22,153
Off you go!

31
00:10:22,154 --> 00:10:23,849
I'm off!

32
00:10:26,826 --> 00:10:29,386
I'm off...

33
00:10:57,757 --> 00:11:01,750
Burnable
Burnable

34
00:11:02,094 --> 00:11:02,994
Burnable...

35
00:11:03,095 --> 00:11:04,995
Not burnable

36
00:11:06,666 --> 00:11:08,429
Burnable

37
00:11:09,035 --> 00:11:11,367
Burnable, not burnable

38
00:11:11,470 --> 00:11:13,836
Kishida! The stickers!

39
00:11:16,042 --> 00:11:17,839
Thank you

40
00:11:18,678 --> 00:11:20,509
Not burnable

41
00:11:21,280 --> 00:11:24,249
Not burnable
Burnable

42
00:11:24,417 --> 00:11:26,009
Burnable...

43
00:11:38,831 --> 00:11:40,731
Thank you

44
00:11:46,772 --> 00:11:48,501
Thank you

45
00:11:59,685 --> 00:12:01,744
Do you remember when that family

46
00:12:01,854 --> 00:12:05,415
was murdered in that
upper-class neighborhood?

47
00:12:05,524 --> 00:12:09,585
It says here it happened
on the 20th of last month

48
00:12:09,995 --> 00:12:13,396
A judge and his wife

49
00:12:13,499 --> 00:12:15,296
and their daughter, a pianist

50
00:12:15,901 --> 00:12:18,734
Each of them murdered in
their sleep, late at night

51
00:12:18,838 --> 00:12:21,839
The murderer was already arrested

52
00:12:22,008 --> 00:12:23,969
He was a failed student

53
00:12:24,310 --> 00:12:27,074
Her parents wouldn't let him date their daughter

54
00:12:38,724 --> 00:12:43,127
"Don't copy me

55
00:12:43,229 --> 00:12:47,689
"Mr. Echo

56
00:12:47,833 --> 00:12:49,994
"Mr. Copycat"

57
00:12:51,170 --> 00:12:52,762
Cold!

58
00:13:05,050 --> 00:13:06,415
Come here

59
00:13:07,486 --> 00:13:08,817
Rio!

60
00:13:10,856 --> 00:13:14,155
Yuzu! Yuzuki!
Come here

61
00:13:14,627 --> 00:13:16,857
Haruna!
Come!

62
00:13:16,962 --> 00:13:18,088
Yuuki!

63
00:15:38,837 --> 00:15:43,831
Cinema Circus
Video Rental Shop

64
00:16:44,803 --> 00:16:46,737
Are you looking for something?

65
00:16:49,441 --> 00:16:53,605
Help Wanted

66
00:17:06,158 --> 00:17:09,719
Here we are!

67
00:17:10,662 --> 00:17:12,960
This hill!

68
00:17:16,668 --> 00:17:19,432
Pretending to go straight!

69
00:18:05,217 --> 00:18:06,844
It warmed you up

70
00:18:40,052 --> 00:18:44,716
I found myself with a heart...

71
00:18:46,892 --> 00:18:52,353
With a heart I was not supposed to have

72
00:19:05,043 --> 00:19:07,637
Movies are made with film

73
00:19:07,746 --> 00:19:10,579
Light passes through it and on to a screen

74
00:19:11,650 --> 00:19:14,141
Film

75
00:19:14,287 --> 00:19:17,419
Right
You never watched a movie?

76
00:19:19,892 --> 00:19:24,727
That's just a substitute. You have to
watch movies in a movie theater

77
00:19:25,430 --> 00:19:27,728
Movie... theater?

78
00:19:28,000 --> 00:19:30,867
Kids these days are unbelievable

79
00:19:30,969 --> 00:19:33,403
What have you seen at a theater recently?

80
00:19:33,505 --> 00:19:34,802
Me?

81
00:19:35,307 --> 00:19:37,036
You know...

82
00:19:38,844 --> 00:19:40,106
There's this one or...

83
00:19:43,315 --> 00:19:45,647
But you can't beat

84
00:19:45,751 --> 00:19:48,711
"Battles Without Honor and Humanity"

85
00:19:48,921 --> 00:19:52,015
"Battles Without Honor
and Humanity..."

86
00:19:53,592 --> 00:19:58,427
Battles...

87
00:19:59,498 --> 00:20:01,227
Without...

88
00:20:07,172 --> 00:20:09,140
Don't worry, don't worry

89
00:20:10,209 --> 00:20:13,645
It'll be fine. Don't worry
about what your boss told you

90
00:20:14,546 --> 00:20:18,710
I don't see you that way at all

91
00:20:18,951 --> 00:20:20,714
So don't you worry

92
00:20:22,020 --> 00:20:25,319
I can't believe they keep

93
00:20:25,424 --> 00:20:28,291
that useless guy in charge

94
00:20:28,393 --> 00:20:31,487
What are they thinking?
Right, exactly

95
00:20:33,498 --> 00:20:35,762
Thank you very much

96
00:20:36,368 --> 00:20:39,235
Thank you...

97
00:20:55,587 --> 00:20:58,385
What are you looking for?

98
00:21:02,561 --> 00:21:04,222
What?

99
00:21:05,664 --> 00:21:07,222
What?

100
00:21:10,602 --> 00:21:13,332
Wasn't "Battles" great?

101
00:21:14,006 --> 00:21:17,942
"I've still got bullets left,
Yamamori san"

102
00:21:19,911 --> 00:21:22,641
Didn't you watch Part 1?

103
00:21:22,881 --> 00:21:26,317
Part 2 is "Hiroshima"
Want to rent both?

104
00:21:31,123 --> 00:21:34,115
What are you doing for Christmas, Nozomi?

105
00:21:34,860 --> 00:21:36,555
With your boyfriend?

106
00:21:39,298 --> 00:21:42,563
Your boyfriend, your favorite guy

107
00:21:45,304 --> 00:21:46,737
You have one, right?

108
00:21:47,539 --> 00:21:48,972
No

109
00:21:49,341 --> 00:21:50,535
Really?

110
00:21:51,209 --> 00:21:52,403
Yes

111
00:21:57,382 --> 00:21:58,940
The first time

112
00:21:59,084 --> 00:22:03,180
I asked my wife out to a movie "Love Story"

113
00:22:04,022 --> 00:22:07,583
I told a lie

114
00:22:08,694 --> 00:22:13,358
Because I found a heart, I told a lie

115
00:22:17,469 --> 00:22:19,937
"Planet of the Apes"

116
00:22:20,739 --> 00:22:23,173
"Purple Noon"

117
00:22:24,309 --> 00:22:27,107
Chaplin's "The Great Dictator"

118
00:22:29,047 --> 00:22:32,107
Junichi likes musicals

119
00:22:32,217 --> 00:22:36,483
Musicals are fun movies with singing and dancing

120
00:22:37,189 --> 00:22:42,388
"There's Something About Mary"
is supposed to be really funny

121
00:23:58,603 --> 00:24:00,798
The water's just right

122
00:24:06,044 --> 00:24:09,980
You've got to get all the way in

123
00:24:13,919 --> 00:24:17,480
Your only flaw is that your body's so cold

124
00:24:21,793 --> 00:24:24,523
Can you tell?

125
00:24:25,897 --> 00:24:28,889
Smells great
It's a popular shampoo

126
00:24:29,000 --> 00:24:31,867
I spent 100 Yen more than the usual shampoo

127
00:24:37,342 --> 00:24:39,970
100 Yen makes a big difference

128
00:24:41,580 --> 00:24:43,138
Great...

129
00:25:19,985 --> 00:25:23,443
Lucky you
You don't age

130
00:25:24,523 --> 00:25:28,983
I... found a white hair
down there

131
00:25:30,529 --> 00:25:32,224
What a shock

132
00:25:35,200 --> 00:25:38,260
I wonder how much longer
I can make love with you

133
00:25:56,488 --> 00:25:59,150
So damned pretty

134
00:26:01,526 --> 00:26:03,756
The Winter Triangle

135
00:26:07,766 --> 00:26:09,961
Don't make me repeat myself

136
00:26:10,669 --> 00:26:12,728
Can't you ever remember?

137
00:26:15,006 --> 00:26:16,803
There's Sirius

138
00:26:17,542 --> 00:26:19,339
And Procyon

139
00:26:19,678 --> 00:26:21,509
And Betelgeuse

140
00:26:24,716 --> 00:26:29,050
That's not true
You're beautiful, too

141
00:26:32,023 --> 00:26:34,992
No point being jealous of the stars

142
00:26:59,584 --> 00:27:03,020
I am an air doll

143
00:27:04,022 --> 00:27:08,015
A substitute for handling sexual desire

144
00:27:08,326 --> 00:27:13,457
Nozomi...
Always stay beside me

145
00:29:06,144 --> 00:29:08,738
Do you like it?

146
00:29:36,941 --> 00:29:39,933
What is up there?

147
00:29:41,246 --> 00:29:45,546
Up in the sky, there's air
And clouds float there

148
00:29:46,484 --> 00:29:49,942
At night, the moon and the stars come out

149
00:29:51,289 --> 00:29:55,487
It's invisible so you can't see it
But it's there

150
00:29:56,194 --> 00:29:59,721
You can't see it
But it's there

151
00:30:00,465 --> 00:30:02,990
Hard to understand

152
00:30:14,512 --> 00:30:17,276
Is that the ocean?

153
00:30:18,850 --> 00:30:20,784
Yes, that's right

154
00:30:25,623 --> 00:30:27,614
Have you never seen the ocean?

155
00:30:27,759 --> 00:30:29,021
No

156
00:30:30,528 --> 00:30:33,156
There's so many boats

157
00:30:49,280 --> 00:30:52,909
What does it mean
"To get old"?

158
00:30:54,152 --> 00:30:57,144
It means aging and getting closer to death

159
00:30:58,256 --> 00:30:59,348
Death?

160
00:30:59,457 --> 00:31:00,719
Yes

161
00:31:01,259 --> 00:31:03,227
Losing life

162
00:31:04,362 --> 00:31:06,159
Life...

163
00:31:08,333 --> 00:31:10,927
Even here, you can smell the sea

164
00:31:14,873 --> 00:31:17,740
This smell reminds me of my childhood

165
00:31:25,049 --> 00:31:28,746
Reminds me, too...

166
00:32:25,343 --> 00:32:27,709
You can eat your broccoli

167
00:32:29,981 --> 00:32:31,608
Aren't you going to eat?

168
00:32:34,552 --> 00:32:35,746
No

169
00:32:46,798 --> 00:32:50,529
Moe, that's not a comb

170
00:32:50,969 --> 00:32:52,994
But Ariel was doing it

171
00:32:54,405 --> 00:32:56,134
Who?

172
00:32:56,240 --> 00:32:58,834
A-ri-el

173
00:33:01,011 --> 00:33:04,274
Ariel...
Right, she was

174
00:33:05,450 --> 00:33:09,648
You don't know, Daddy
I saw it with Mommy

175
00:33:12,890 --> 00:33:14,089
Excuse me

176
00:33:14,192 --> 00:33:15,420
A beer

177
00:33:15,526 --> 00:33:18,393
And also, what I asked for before

178
00:33:18,496 --> 00:33:20,123
Yes, sir

179
00:33:23,668 --> 00:33:26,899
Hey, you ate your carrot?

180
00:33:27,005 --> 00:33:27,905
Yeah

181
00:33:30,308 --> 00:33:32,833
You can eat the rest then

182
00:33:52,196 --> 00:33:54,096
Hey, what happened?

183
00:33:54,198 --> 00:33:55,222
A blackout?

184
00:33:55,333 --> 00:33:56,732
Is it a blackout?

185
00:34:10,448 --> 00:34:11,972
What?

186
00:34:12,083 --> 00:34:13,311
A birthday

187
00:34:13,418 --> 00:34:15,215
A birthday?

188
00:34:17,121 --> 00:34:20,887
They're celebrating the day
she was born into this world

189
00:34:29,734 --> 00:34:32,828
Look, a present from your mom

190
00:34:34,372 --> 00:34:36,306
Blow them out

191
00:34:44,315 --> 00:34:49,275
Does everybody have one?
A birthday?

192
00:34:58,429 --> 00:34:59,953
What's that?

193
00:35:00,264 --> 00:35:01,856
White clover

194
00:35:02,600 --> 00:35:07,037
White clover...
White clover...

195
00:35:18,483 --> 00:35:20,383
That's a dandelion

196
00:35:21,752 --> 00:35:25,051
Is this about to be born, now?

197
00:35:25,156 --> 00:35:26,817
No, it's already wilted

198
00:35:29,760 --> 00:35:31,489
Poor thing

199
00:35:32,196 --> 00:35:34,255
But it has to die someday

200
00:35:35,266 --> 00:35:37,496
Otherwise the world would be overrun

201
00:35:41,105 --> 00:35:42,902
Hard to understand

202
00:36:41,999 --> 00:36:44,024
What else do you want to know?

203
00:36:59,250 --> 00:37:03,983
I want to know more about you

204
00:38:21,499 --> 00:38:23,558
"Candy..."

205
00:38:24,702 --> 00:38:27,933
"5,980 Yen"

206
00:39:00,438 --> 00:39:02,133
I'm home

207
00:39:07,611 --> 00:39:09,579
Were you lonely?

208
00:39:15,152 --> 00:39:18,610
Okabe suddenly took the day off

209
00:39:18,723 --> 00:39:22,386
What's he thinking...

210
00:39:25,096 --> 00:39:28,463
I'm an air doll

211
00:39:30,301 --> 00:39:33,896
A late model, cheap one...

212
00:39:58,162 --> 00:40:01,029
A girl called Ari or Mari or something,

213
00:40:01,165 --> 00:40:05,101
is combing her hair with a fork, like this...

214
00:40:09,874 --> 00:40:11,535
You don't know?

215
00:40:12,643 --> 00:40:14,211
No...

216
00:40:15,813 --> 00:40:19,146
That must be "The Little Mermaid"
It's over here

217
00:40:19,250 --> 00:40:22,048
You have it
Thanks

218
00:40:22,953 --> 00:40:24,716
I'm sorry...

219
00:40:26,223 --> 00:40:30,785
A movie with a super-bad cop in it

220
00:40:30,895 --> 00:40:35,855
Like a totally corrupt cop, making
bad drug deals, rotten to the core

221
00:40:36,167 --> 00:40:37,225
Rotten?

222
00:40:37,334 --> 00:40:39,495
Yeah, got anything?

223
00:40:39,670 --> 00:40:41,331
Then, how about,

224
00:40:41,439 --> 00:40:43,703
"Bad Lieutenant", Abel Ferrara

225
00:40:43,808 --> 00:40:44,934
Is it really bad?

226
00:40:45,042 --> 00:40:48,011
It's more than bad
Harvey Keitel is the worst

227
00:40:48,112 --> 00:40:49,044
The worst...

228
00:40:49,146 --> 00:40:51,376
Why don't you rent
"Donnie Brasco", too?

229
00:40:51,482 --> 00:40:53,609
Al Pacino plays a guy in the mafia

230
00:40:53,717 --> 00:40:55,548
- And Johnny Depp is a cop?
- Yeah

231
00:40:55,653 --> 00:40:57,814
- I already saw it
- You did

232
00:41:00,891 --> 00:41:02,222
Excuse me...

233
00:41:06,263 --> 00:41:08,527
You can wipe them off with this

234
00:41:09,266 --> 00:41:10,460
The lines

235
00:41:11,235 --> 00:41:12,634
Lines?

236
00:41:25,883 --> 00:41:27,077
Lines?

237
00:41:27,985 --> 00:41:29,077
"Dreams?"

238
00:41:29,186 --> 00:41:30,053
"Dream..."

239
00:41:30,054 --> 00:41:31,385
"Dream?"

240
00:41:32,556 --> 00:41:36,424
OK, how about Theo Angelopoulos'

241
00:41:36,894 --> 00:41:38,885
"The Beekeeper"

242
00:41:39,163 --> 00:41:40,425
Gelo?

243
00:41:40,531 --> 00:41:41,998
Angelo...

244
00:41:42,099 --> 00:41:45,557
Across the river there's a big DVD store

245
00:41:47,671 --> 00:41:50,697
You can't rent "Dream of Light"
You have to buy it

246
00:41:50,908 --> 00:41:54,207
"The Beekeeper" comes in a box set,

247
00:41:54,311 --> 00:41:57,337
so you probably can't rent it, even there

248
00:42:02,820 --> 00:42:04,185
Thanks

249
00:42:05,656 --> 00:42:07,317
Across the river...

250
00:42:10,160 --> 00:42:11,752
Thank you

251
00:42:12,763 --> 00:42:14,321
I'm sorry...

252
00:42:15,633 --> 00:42:18,033
I am useless...

253
00:42:28,579 --> 00:42:30,638
What's your name?

254
00:42:31,448 --> 00:42:33,780
Look at the flowers

255
00:42:33,884 --> 00:42:36,876
What's the flower called?

256
00:42:40,057 --> 00:42:41,649
Excuse me

257
00:42:44,461 --> 00:42:45,985
I'm sorry

258
00:42:46,497 --> 00:42:47,987
I'm sorry

259
00:42:54,805 --> 00:42:56,397
I'm sorry

260
00:43:02,313 --> 00:43:03,940
Excuse me

261
00:43:23,367 --> 00:43:27,861
Say, do you know a bug called the mayfly?

262
00:43:30,541 --> 00:43:32,509
The mayfly...

263
00:43:32,910 --> 00:43:37,370
Dies a day or 2 after it gives birth

264
00:43:37,915 --> 00:43:40,816
So its body is empty

265
00:43:40,918 --> 00:43:43,785
No stomach or intestines

266
00:43:44,288 --> 00:43:48,748
It's filled with eggs instead

267
00:43:49,893 --> 00:43:53,795
It's a creature that's born only to give birth

268
00:43:56,867 --> 00:43:59,859
Humans aren't so different

269
00:44:00,371 --> 00:44:02,202
Pointless...

270
00:44:05,209 --> 00:44:08,576
I'm empty, too...

271
00:44:12,416 --> 00:44:17,149
A marvelous coincidence I'm the same...

272
00:44:17,588 --> 00:44:19,351
I'm all empty

273
00:44:23,827 --> 00:44:26,796
I wonder if there are others

274
00:44:28,666 --> 00:44:32,966
These days, everybody is

275
00:44:33,070 --> 00:44:34,367
Everybody?

276
00:44:34,471 --> 00:44:37,133
Yes, especially,

277
00:44:38,142 --> 00:44:41,509
everyone living in this kind of city

278
00:44:44,882 --> 00:44:47,282
You're not the only one

279
00:44:54,525 --> 00:44:58,188
Say, do you know this poem?

280
00:44:59,496 --> 00:45:01,123
Poem?

281
00:45:01,832 --> 00:45:04,232
You don't know poems?

282
00:45:04,768 --> 00:45:06,258
Doesn't matter,

283
00:45:07,504 --> 00:45:09,529
"Life is..."

284
00:45:10,307 --> 00:45:12,332
Life is...

285
00:45:12,943 --> 00:45:16,879
Let's see, "life is..."

286
00:45:18,348 --> 00:45:21,215
Let's see, life is?

287
00:45:59,490 --> 00:46:01,117
It seems life

288
00:46:01,225 --> 00:46:05,594
is constructed in a way

289
00:46:05,696 --> 00:46:08,256
that no one can fulfill it alone

290
00:46:15,072 --> 00:46:16,505
Just as

291
00:46:16,807 --> 00:46:20,106
it's not enough for flowers

292
00:46:20,244 --> 00:46:22,405
to have pistils and stamens

293
00:46:22,713 --> 00:46:25,580
An insect or a breeze

294
00:46:25,682 --> 00:46:28,810
must introduce a pistil to a stamen

295
00:46:34,758 --> 00:46:39,923
Life contains its own absence,

296
00:46:40,230 --> 00:46:43,996
which only an Other can fulfill

297
00:46:50,941 --> 00:46:55,571
It seems the world is the summation of Others

298
00:46:56,313 --> 00:46:57,780
And yet,

299
00:46:58,148 --> 00:47:02,608
We neither know nor are told

300
00:47:02,719 --> 00:47:06,849
that we will fulfill each other

301
00:47:08,058 --> 00:47:11,289
We lead our scattered lives,

302
00:47:11,395 --> 00:47:14,887
perfectly unaware of each other...

303
00:47:16,934 --> 00:47:20,301
Welcome, would you like to buy a cake?

304
00:47:20,938 --> 00:47:23,634
The master has come home

305
00:47:23,740 --> 00:47:26,573
Welcome back, Master

306
00:47:26,677 --> 00:47:31,478
Or at times, allowed to find

307
00:47:31,748 --> 00:47:34,615
the Other's presence disagreeable

308
00:47:39,556 --> 00:47:41,353
Why is it,

309
00:47:41,458 --> 00:47:46,953
that the world is constructed so loosely?

310
00:47:50,834 --> 00:47:51,734
I'm Misato

311
00:47:51,835 --> 00:47:53,928
How old are you, Misato?

312
00:47:54,037 --> 00:47:54,937
I'm 24

313
00:47:55,038 --> 00:47:56,403
24...

314
00:47:56,506 --> 00:47:59,942
Misato, do you know a good restaurant nearby?

315
00:48:00,043 --> 00:48:01,601
I just started yesterday

316
00:48:01,712 --> 00:48:03,043
I understand

317
00:48:03,146 --> 00:48:04,135
What do you like?

318
00:48:04,248 --> 00:48:05,408
I like pasta

319
00:48:05,515 --> 00:48:07,244
I like pasta, too!

320
00:48:08,018 --> 00:48:09,986
I want Extra Rice

321
00:48:10,087 --> 00:48:12,146
I want Extra Rice, too

322
00:48:12,256 --> 00:48:15,350
Cancel the ham and cheese hamburg steak

323
00:48:15,459 --> 00:48:16,325
Lunch specials?

324
00:48:16,326 --> 00:48:18,317
More water with lots of ice

325
00:48:18,428 --> 00:48:19,417
Right away

326
00:48:19,529 --> 00:48:23,090
I'll take the lunch special instead

327
00:48:23,200 --> 00:48:24,462
The lunch special?

328
00:48:24,568 --> 00:48:27,036
The suspect, Kume, confessed that

329
00:48:27,137 --> 00:48:31,972
he had bought one gram from a foreigner
for 7,000 Yen when he traveled to Osaka

330
00:48:32,075 --> 00:48:34,635
So expensive...

331
00:48:42,252 --> 00:48:44,379
A horse fly,

332
00:48:45,289 --> 00:48:47,314
bathed in light,

333
00:48:47,424 --> 00:48:49,984
flies in close

334
00:48:50,093 --> 00:48:53,426
to a blooming flower

335
00:48:59,569 --> 00:49:01,901
I, too, might have been

336
00:49:02,005 --> 00:49:05,236
someone's horse fly

337
00:49:11,648 --> 00:49:13,707
Perhaps you, too,

338
00:49:14,217 --> 00:49:18,847
had once been my breeze

339
00:50:11,942 --> 00:50:14,467
No customers...

340
00:50:15,512 --> 00:50:17,002
No...

341
00:50:18,215 --> 00:50:20,547
I hope it'll be OK

342
00:50:21,785 --> 00:50:24,754
I like it here...

343
00:50:25,756 --> 00:50:27,451
This store

344
00:50:33,930 --> 00:50:36,558
What's the name of the movie adapted

345
00:50:36,733 --> 00:50:40,294
from a Stephen King novel,
starring River Phoenix when he was 15?

346
00:50:40,404 --> 00:50:42,395
"Stand by Me"

347
00:50:43,040 --> 00:50:44,166
Correct

348
00:50:44,274 --> 00:50:45,605
Great!

349
00:50:50,614 --> 00:50:52,343
OK, then,

350
00:50:52,449 --> 00:50:57,045
the Hollywood movie that became
Matsuda Yusaku's last film?

351
00:51:19,759 --> 00:51:21,021
Don't look...

352
00:51:55,128 --> 00:51:57,858
Air...
You need air...

353
00:51:57,964 --> 00:51:59,261
Where's the plug?

354
00:52:01,234 --> 00:52:02,895
Stomach

355
00:52:08,775 --> 00:52:10,208
Sorry

356
00:52:11,378 --> 00:52:12,811
Don't look...

357
00:52:14,714 --> 00:52:15,703
But...

358
00:53:27,020 --> 00:53:30,080
You're...
Fine now

359
00:53:31,724 --> 00:53:35,888
A little... while longer...

360
00:57:43,409 --> 00:57:45,070
I'm off

361
01:01:28,100 --> 01:01:29,931
Going to get old

362
01:01:31,671 --> 01:01:35,767
You see, I'm going to get old

363
01:01:48,587 --> 01:01:50,851
Were you surprised?

364
01:01:51,857 --> 01:01:53,051
Yeah

365
01:01:54,160 --> 01:01:55,026
But...

366
01:01:55,027 --> 01:01:59,726
But I hear there are plenty of others like me

367
01:02:00,700 --> 01:02:03,100
Someone said that

368
01:02:14,814 --> 01:02:16,372
Me, too...

369
01:02:20,019 --> 01:02:21,384
Really?

370
01:02:25,124 --> 01:02:26,955
I'm not so different

371
01:02:27,760 --> 01:02:30,160
I see
But...

372
01:02:31,330 --> 01:02:33,423
I guess you're right

373
01:02:34,333 --> 01:02:38,030
When I first met you, I sort of felt,

374
01:02:38,304 --> 01:02:40,898
that you were just like me

375
01:02:42,108 --> 01:02:43,302
A marv...

376
01:02:45,778 --> 01:02:49,714
I hit the jackpot at Pachinko

377
01:02:50,716 --> 01:02:54,550
A marvelous...
Coincidence

378
01:03:16,275 --> 01:03:17,503
What?

379
01:03:18,644 --> 01:03:20,305
Nothing

380
01:04:09,361 --> 01:04:13,798
So damned pretty

381
01:04:16,035 --> 01:04:19,630
There's Sirius
And Procyon

382
01:04:19,738 --> 01:04:21,797
How did you get an Osaka accent?

383
01:04:22,741 --> 01:04:24,538
An Osaka accent?

384
01:04:30,749 --> 01:04:34,241
A movie I saw a movie

385
01:04:35,588 --> 01:04:38,216
What was it called...

386
01:06:41,080 --> 01:06:44,345
The other day...
How did you feel?

387
01:06:45,584 --> 01:06:47,313
The other day?

388
01:06:50,189 --> 01:06:51,554
In the store

389
01:06:52,891 --> 01:06:57,487
When you breathed into me?

390
01:06:58,864 --> 01:07:00,229
No...

391
01:07:02,134 --> 01:07:05,001
When the air left your body

392
01:07:16,982 --> 01:07:18,540
Was it painful?

393
01:07:23,188 --> 01:07:24,587
Yes

394
01:07:25,824 --> 01:07:27,621
It was painful

395
01:07:49,481 --> 01:07:51,972
Having a heart

396
01:07:52,451 --> 01:07:54,976
was heartbreaking

397
01:08:05,798 --> 01:08:08,631
Misato's only virtue is that she's young

398
01:08:08,734 --> 01:08:11,032
They'll stop spoiling her soon

399
01:08:11,136 --> 01:08:15,004
She's just a part-time employee
That's all

400
01:08:15,107 --> 01:08:16,768
That's right

401
01:08:16,875 --> 01:08:19,673
She's not going to last long anyway

402
01:08:19,778 --> 01:08:22,406
You don't need to worry about her

403
01:08:22,514 --> 01:08:24,243
But...

404
01:08:24,383 --> 01:08:26,613
It'll be fine

405
01:08:26,952 --> 01:08:31,252
You know there's no one that can take your place

406
01:08:32,391 --> 01:08:34,120
Really?

407
01:08:36,228 --> 01:08:37,820
Really?

408
01:08:38,030 --> 01:08:42,990
7:18 PM, the 9th

409
01:08:44,069 --> 01:08:48,199
He said, "My pension's gone
You stole it, didn't you"

410
01:08:48,307 --> 01:08:51,140
He just kept accusing me over and over,

411
01:08:51,243 --> 01:08:53,803
and I just got so mad...

412
01:08:53,912 --> 01:08:55,743
I'm sorry

413
01:08:55,848 --> 01:08:57,941
No, no, that happened in Osaka

414
01:08:58,050 --> 01:09:01,315
The suspect confessed and has been arrested

415
01:09:01,420 --> 01:09:02,819
Don't worry

416
01:09:03,589 --> 01:09:07,855
it has nothing to do with you

417
01:09:11,430 --> 01:09:13,295
Nothing?

418
01:09:13,399 --> 01:09:16,061
Nothing at all

419
01:09:48,767 --> 01:09:52,760
I've got work I can't visit until spring

420
01:09:54,440 --> 01:09:57,898
Listen, did you come here while I was out?

421
01:09:59,111 --> 01:10:01,978
I told you not to clean up my stuff

422
01:10:02,381 --> 01:10:04,144
Did you throw it out?

423
01:10:04,283 --> 01:10:09,186
Well, it's not that important

424
01:10:44,323 --> 01:10:46,985
Why don't you give them to your neighbors?

425
01:10:47,092 --> 01:10:50,061
How strange

426
01:10:50,162 --> 01:10:54,963
Your sister's having her second baby next year

427
01:10:55,601 --> 01:10:58,308
You don't have to grow apples with us,

428
01:10:58,451 --> 01:11:00,698
but it's time you come home

429
01:11:55,093 --> 01:11:59,928
Can't you remember how to take orders?
You've worked here for years now

430
01:12:00,999 --> 01:12:04,162
It's fine with me if you want to quit

431
01:12:06,104 --> 01:12:08,834
There's lots of guys waiting to take your place

432
01:12:12,511 --> 01:12:16,948
What's so funny
It's revolting when you smile

433
01:12:17,749 --> 01:12:19,080
Sorry

434
01:12:22,154 --> 01:12:25,123
Movies for Crying

435
01:12:53,218 --> 01:12:54,810
Excuse me

436
01:12:55,253 --> 01:12:56,481
Hello

437
01:12:57,489 --> 01:12:59,457
How can I help?

438
01:13:00,392 --> 01:13:01,825
This one

439
01:13:23,215 --> 01:13:25,479
Thank you for your business

440
01:13:33,225 --> 01:13:35,693
Did you guys already do it?

441
01:13:37,496 --> 01:13:40,226
You did it with Junichi, right?

442
01:13:41,400 --> 01:13:45,234
You don't have to hide it
You're seeing him, right?

443
01:13:46,505 --> 01:13:47,699
No

444
01:13:48,740 --> 01:13:51,368
Is that your boyfriend?

445
01:13:53,011 --> 01:13:55,741
I saw you in the park

446
01:13:56,381 --> 01:13:58,747
You were with him

447
01:14:01,486 --> 01:14:04,819
I bet Junichi doesn't know about that guy

448
01:14:10,195 --> 01:14:13,653
You do this with anybody

449
01:14:14,132 --> 01:14:16,396
Girls these days...

450
01:14:23,075 --> 01:14:24,542
Harue...

451
01:14:36,955 --> 01:14:40,220
Hey, touch me
Here

452
01:14:41,793 --> 01:14:43,954
Hey, touch me...

453
01:14:44,563 --> 01:14:45,791
Here...

454
01:14:47,599 --> 01:14:49,032
Harue

455
01:15:12,224 --> 01:15:15,990
I am an air doll

456
01:15:21,066 --> 01:15:25,503
A substitute for handling sexual desire

457
01:15:29,074 --> 01:15:30,837
I am...

458
01:15:36,681 --> 01:15:40,617
Happy birthday to you

459
01:15:40,786 --> 01:15:44,586
Happy birthday to you

460
01:15:44,756 --> 01:15:48,522
Happy birthday, dear Nozomi

461
01:15:49,227 --> 01:15:50,785
Nozomi...

462
01:15:55,367 --> 01:15:57,858
How is it?

463
01:15:57,969 --> 01:15:59,459
Nozomi...

464
01:16:00,972 --> 01:16:03,634
The white star of the first magnitude, Procyon

465
01:16:03,742 --> 01:16:07,803
The triangle linking those 3 stars

466
01:16:07,913 --> 01:16:11,076
is called The Winter Triangle

467
01:16:11,183 --> 01:16:12,616
Got that?

468
01:16:13,718 --> 01:16:15,718
No, I'll get wet

469
01:16:19,858 --> 01:16:21,450
I have to pee

470
01:17:34,366 --> 01:17:35,765
What?

471
01:17:42,539 --> 01:17:44,106
Nozomi?

472
01:17:47,445 --> 01:17:52,007
You bought her a cake
Is it her birthday?

473
01:17:53,051 --> 01:17:54,313
No...

474
01:17:55,387 --> 01:17:58,948
I got one for you, too
On the day I welcomed you here

475
01:17:59,524 --> 01:18:02,550
I don't know...
I don't remember that

476
01:18:02,661 --> 01:18:04,356
I really did

477
01:18:04,763 --> 01:18:06,697
I took a picture

478
01:18:14,906 --> 01:18:16,168
I...

479
01:18:17,909 --> 01:18:20,139
I found a heart

480
01:18:22,547 --> 01:18:24,014
A heart?

481
01:18:24,215 --> 01:18:27,116
Yes, a heart

482
01:18:30,522 --> 01:18:31,784
Why?

483
01:18:35,226 --> 01:18:36,716
I don't know

484
01:18:41,566 --> 01:18:46,833
Tell me, what do you like about me?

485
01:18:50,175 --> 01:18:51,767
What do I like?

486
01:18:56,214 --> 01:18:57,909
You don't know, do you?

487
01:18:58,583 --> 01:19:03,452
Even the name, Nozomi,
is your old girlfriend's name

488
01:19:06,358 --> 01:19:09,327
You read my blog?

489
01:19:09,427 --> 01:19:14,330
I'm just a substitute for her

490
01:19:16,968 --> 01:19:18,959
But...

491
01:19:21,373 --> 01:19:23,102
I mean...

492
01:19:24,175 --> 01:19:28,805
Sure, at first, I was lonely without her

493
01:19:30,648 --> 01:19:33,082
But now, it's different

494
01:19:35,286 --> 01:19:37,151
No, what am I saying...

495
01:19:37,355 --> 01:19:39,323
Why me?

496
01:19:43,628 --> 01:19:45,118
Why?

497
01:19:48,767 --> 01:19:51,361
It doesn't have to be me, right?

498
01:19:52,270 --> 01:19:54,067
Right?

499
01:20:04,616 --> 01:20:05,947
The picture...

500
01:20:14,559 --> 01:20:18,655
Listen, will you do me a favor?

501
01:20:22,100 --> 01:20:24,762
Can you go back to being the old doll?

502
01:20:27,472 --> 01:20:28,939
The old doll?

503
01:20:30,208 --> 01:20:34,542
Yeah, the normal, plain old doll
Can't you?

504
01:20:38,083 --> 01:20:42,679
You wish I hadn't found a heart?

505
01:20:44,322 --> 01:20:45,414
Yeah

506
01:20:46,758 --> 01:20:48,623
It's just annoying

507
01:20:50,528 --> 01:20:53,129
This stuff annoys me, which is why

508
01:20:53,248 --> 01:20:55,659
I chose you in the first place

509
01:20:58,369 --> 01:21:00,997
I'm annoying...

510
01:21:02,307 --> 01:21:05,333
What a mean thing to say

511
01:21:05,443 --> 01:21:08,537
That's not what I mean, it's not

512
01:21:09,013 --> 01:21:12,107
You're not annoying, it's humans who are

513
01:21:12,217 --> 01:21:14,082
Nozomi!

514
01:21:21,526 --> 01:21:23,016
Nozomi!

515
01:21:42,714 --> 01:21:44,272
Nozomi!

516
01:23:05,296 --> 01:23:06,854
Thank you

517
01:23:40,832 --> 01:23:43,198
I hate dogs

518
01:23:45,036 --> 01:23:47,698
They get old so fast

519
01:23:48,706 --> 01:23:52,198
and die before you know it

520
01:23:54,746 --> 01:24:00,309
Having a dog just makes you lonely

521
01:24:05,723 --> 01:24:10,751
Is this you?

522
01:24:10,862 --> 01:24:12,022
Yes

523
01:24:12,363 --> 01:24:16,629
I used to be a substitute high school teacher

524
01:24:20,071 --> 01:24:21,868
A substitute?

525
01:24:22,440 --> 01:24:23,464
That's right

526
01:24:24,842 --> 01:24:29,211
I was always an empty substitute

527
01:24:35,019 --> 01:24:37,010
Were you lonely?

528
01:24:37,889 --> 01:24:40,619
I wonder...

529
01:24:43,294 --> 01:24:46,491
I can't remember anymore

530
01:24:51,235 --> 01:24:53,226
Do you mind,

531
01:24:55,106 --> 01:24:59,338
touching me?

532
01:25:31,209 --> 01:25:34,201
That feels so good

533
01:25:36,614 --> 01:25:40,345
Me, too... See?

534
01:25:43,187 --> 01:25:48,250
They always say, people with cold hands,

535
01:25:48,926 --> 01:25:52,919
have warm hearts

536
01:25:55,366 --> 01:25:58,062
Maybe it's just a superstition...

537
01:28:01,359 --> 01:28:02,690
Welcome home

538
01:28:06,230 --> 01:28:07,788
I'm home

539
01:28:13,604 --> 01:28:17,335
I made you, but I don't know the answer

540
01:28:20,011 --> 01:28:22,605
But I doubt even God knows

541
01:28:22,713 --> 01:28:26,205
why the humans he made have hearts

542
01:28:29,554 --> 01:28:31,920
Do you wish you'd never found a heart?

543
01:28:38,229 --> 01:28:41,357
I don't know, but...

544
01:28:43,401 --> 01:28:45,164
It hurts

545
01:29:32,783 --> 01:29:35,718
They all started out the same,

546
01:29:37,722 --> 01:29:42,352
but by the time they come back here,

547
01:29:45,096 --> 01:29:48,759
you can tell from their faces
whether they've been loved

548
01:29:54,405 --> 01:29:59,342
I think that must mean they all have hearts, too

549
01:30:08,219 --> 01:30:11,586
What will happen to them?

550
01:30:15,693 --> 01:30:19,094
Once a year, in spring, I throw them all away

551
01:30:21,065 --> 01:30:23,465
Unfortunately, they're "Not burnable garbage"

552
01:30:25,136 --> 01:30:28,731
Not burnable garbage

553
01:30:30,941 --> 01:30:35,537
After all, once we die, we're "Burnable garbage"

554
01:30:35,646 --> 01:30:37,807
It's not such a big difference

555
01:30:40,651 --> 01:30:44,143
Burnable garbage

556
01:31:19,023 --> 01:31:22,117
Can you tell me one thing?

557
01:31:25,062 --> 01:31:28,395
Was everything you saw in this world sad?

558
01:31:30,634 --> 01:31:34,832
Was there something, anything,
that was beautiful?

559
01:31:43,914 --> 01:31:45,541
I'm glad to hear that

560
01:31:50,054 --> 01:31:53,854
Thank you for having me

561
01:31:59,029 --> 01:32:01,429
No, thank you

562
01:32:03,501 --> 01:32:04,866
Off you go

563
01:32:07,138 --> 01:32:08,799
I'm off

564
01:32:29,927 --> 01:32:34,830
I don't mind if I'm a substitute
for someone else

565
01:32:37,635 --> 01:32:41,071
You're not a substitute for anyone

566
01:32:46,544 --> 01:32:48,774
I'll do anything for you

567
01:32:49,046 --> 01:32:53,346
That's what I was born for

568
01:32:54,518 --> 01:32:58,477
Whatever you want

569
01:33:00,524 --> 01:33:01,855
Really?

570
01:33:03,027 --> 01:33:04,187
Yes

571
01:33:05,362 --> 01:33:09,025
I can do whatever I want?

572
01:33:09,967 --> 01:33:12,868
Yes, whatever you want

573
01:33:17,775 --> 01:33:21,939
I couldn't ask anyone else, but...

574
01:33:23,013 --> 01:33:24,344
With you...

575
01:33:27,818 --> 01:33:31,083
Something only I can do?

576
01:33:33,057 --> 01:33:34,115
Yes

577
01:33:36,293 --> 01:33:37,487
What is it?

578
01:33:40,631 --> 01:33:42,496
I want to let your air out

579
01:33:45,769 --> 01:33:48,897
Let my air out?

580
01:33:51,275 --> 01:33:52,264
Yes

581
01:34:02,453 --> 01:34:03,353
But...

582
01:34:03,354 --> 01:34:04,912
Don't worry

583
01:34:07,424 --> 01:34:10,723
I'll breathe into you like before

584
01:34:12,162 --> 01:34:13,254
But...

585
01:34:15,165 --> 01:34:16,393
Why?

586
01:34:38,656 --> 01:34:42,956
Sure, do whatever you want

587
01:38:55,946 --> 01:38:58,278
Where's your plug?

588
01:39:32,783 --> 01:39:36,480
I'll fill you with my breath

589
01:39:42,926 --> 01:39:45,258
Just like you did for me

590
01:41:17,921 --> 01:41:20,185
I couldn't exhale

591
01:41:20,724 --> 01:41:26,060
my breath into Junichi

592
01:43:58,315 --> 01:43:59,577
You alone?

593
01:44:03,620 --> 01:44:05,144
You are alone

594
01:44:12,529 --> 01:44:14,190
What a jerk

595
01:44:27,978 --> 01:44:36,147
I found a heart

596
01:44:42,626 --> 01:44:44,457
Did you see the news?

597
01:44:44,561 --> 01:44:47,189
They found his body in the trash

598
01:44:47,297 --> 01:44:49,857
Just across the river

599
01:45:36,947 --> 01:45:38,744
Let's trade

600
01:45:42,953 --> 01:45:46,252
Moe, don't touch the garbage

601
01:45:46,356 --> 01:45:47,653
OK

602
01:45:48,992 --> 01:45:49,981
Off you go

603
01:45:50,093 --> 01:45:51,617
I'm off

604
01:45:55,545 --> 01:46:01,345
Subtitles: Arigon

605
01:46:13,316 --> 01:46:17,480
Mommy

606
01:49:03,386 --> 01:49:04,546
Hey!

607
01:49:05,555 --> 01:49:07,250
Moe! Your gym clothes!

608
01:52:00,997 --> 01:52:03,295
Beautiful

