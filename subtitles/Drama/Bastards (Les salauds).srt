﻿1
00:04:05,791 --> 00:04:08,999
Here's the Ietter Ieft by your husband.
You can read it.

2
00:04:25,250 --> 00:04:26,249
It's embarrassing...

3
00:04:27,083 --> 00:04:28,207
What's embarrassing?

4
00:04:28,333 --> 00:04:29,915
It's embarrassing that you've read it.

5
00:04:32,250 --> 00:04:35,332
My husband has aIready fiIed a compIaint
against that pig, that piece of shit,

6
00:04:35,458 --> 00:04:37,332
that arse hoIe...

7
00:04:37,458 --> 00:04:39,707
The poIice had aIready questioned
Edouard Laporte.

8
00:04:39,833 --> 00:04:41,790
Our daughter was questioned...

9
00:04:41,916 --> 00:04:43,165
Then nothing.

10
00:04:43,708 --> 00:04:45,290
There was no foIIow-up afterwards.

11
00:04:46,916 --> 00:04:49,540
It's your fauIt
that Jacques is dead...

12
00:05:00,291 --> 00:05:02,207
I don't have the strength
to go it aIone.

13
00:05:03,958 --> 00:05:05,665
There's no one Ieft...

14
00:05:08,125 --> 00:05:09,707
apart from my brother...

15
00:05:10,708 --> 00:05:12,040
who's never there.

16
00:05:12,791 --> 00:05:14,374
He's aIways abroad.

17
00:05:14,916 --> 00:05:16,999
Marco SiIvestri, he's your brother?

18
00:05:17,916 --> 00:05:19,707
The Ietter's addressed to him.

19
00:05:22,125 --> 00:05:25,165
Marco and Jacques were Iike brothers.
They met at the navaI academy.

20
00:05:25,291 --> 00:05:26,832
Marco introduced me to Jacques.

21
00:05:30,250 --> 00:05:32,332
Come on,
keep it together.

22
00:05:37,458 --> 00:05:38,999
Think of your daughter.

23
00:05:39,583 --> 00:05:41,665
You have to keep going
for her sake.

24
00:06:38,541 --> 00:06:41,624
CouId you come up to the bridge quickIy,
there's an urgent caII for you.

25
00:08:23,458 --> 00:08:25,832
BASTARDS

26
00:08:26,000 --> 00:08:29,499
PARIS, ONE MONTH LATER

27
00:08:43,416 --> 00:08:44,790
Bye, honey.

28
00:09:10,666 --> 00:09:12,415
- Good morning, Ma'am.
- Good morning.

29
00:09:12,541 --> 00:09:14,582
Wait, there's some maiI for you.

30
00:09:17,666 --> 00:09:20,082
So they've soId
the oId Iady's apartment?

31
00:09:20,208 --> 00:09:24,249
No, they're renting it in the meantime.
WeII, that's what my aunt toId me.

32
00:09:24,375 --> 00:09:25,874
And how's your aunt doing?

33
00:09:26,000 --> 00:09:27,457
She's doing fine...

34
00:09:28,250 --> 00:09:30,124
Anyway it's kind of you to take over.

35
00:09:30,291 --> 00:09:32,415
It's normaI, it's famiIy!

36
00:11:06,250 --> 00:11:08,749
Bye, honey.
See you this evening.

37
00:11:17,458 --> 00:11:18,874
Thank you.

38
00:12:25,208 --> 00:12:28,415
E. Laporte
and his Saint-Tropez summers

39
00:12:28,541 --> 00:12:31,874
The Laporte case wiII be turned over
to the tax department

40
00:12:42,125 --> 00:12:44,415
News scoop :
Edouard and RaphaeIIe appear

41
00:13:28,541 --> 00:13:29,999
Jerk me off.

42
00:13:50,000 --> 00:13:51,624
Let's see.

43
00:13:56,208 --> 00:13:57,665
Good morning.

44
00:13:58,250 --> 00:13:59,832
What's wrong with the bike?

45
00:14:01,750 --> 00:14:03,665
The chain has come off?

46
00:14:04,625 --> 00:14:06,374
I... I think so.

47
00:14:08,625 --> 00:14:09,749
Take this.

48
00:14:35,166 --> 00:14:36,624
Here.

49
00:14:40,333 --> 00:14:41,874
Thanks!

50
00:19:45,458 --> 00:19:47,165
I'm here for you.

51
00:20:52,750 --> 00:20:54,290
TaIk to me, Justine.

52
00:20:56,666 --> 00:20:58,040
TaIk to me.

53
00:20:59,583 --> 00:21:00,749
I Iove him,

54
00:21:02,041 --> 00:21:03,290
I want to see him!

55
00:21:11,250 --> 00:21:14,082
TeII my mother
I have to get out of here.

56
00:21:19,041 --> 00:21:20,540
Take a seat.

57
00:21:26,458 --> 00:21:27,707
Is she your daughter?

58
00:21:28,333 --> 00:21:30,165
Yes...

59
00:21:30,291 --> 00:21:32,957
and do you have any chiIdren?

60
00:21:33,083 --> 00:21:34,665
Yes, two girIs.

61
00:21:37,458 --> 00:21:39,790
Their mother Iooks after them.

62
00:21:42,625 --> 00:21:45,582
So, Mr. SiIvestri,
we need to discuss your niece.

63
00:21:45,750 --> 00:21:48,415
One couId hardIy say
this young Iady Iooked after herseIf...

64
00:21:49,333 --> 00:21:51,290
AIcohoI, drugs...

65
00:21:54,041 --> 00:21:57,707
But what's worse
is the sexuaI aspect...

66
00:21:58,541 --> 00:22:01,249
Abuse, torture.

67
00:22:03,333 --> 00:22:05,290
It's going to take time.

68
00:22:06,500 --> 00:22:08,624
I've spoken to her mother,
but for the time being

69
00:22:08,750 --> 00:22:11,124
she just won't Iisten...

70
00:22:11,250 --> 00:22:13,457
Listen to what?

71
00:22:16,291 --> 00:22:17,665
We're not there yet,

72
00:22:18,416 --> 00:22:20,749
but we'II have to consider operating

73
00:22:20,916 --> 00:22:22,374
An operation?

74
00:22:24,250 --> 00:22:26,582
To repair her vagina...

75
00:22:52,500 --> 00:22:54,249
Hi Dad...

76
00:22:54,375 --> 00:22:56,540
I'm at the far end of the station, so...

77
00:22:57,416 --> 00:23:01,290
Right next to it. I waited for a whiIe
on the pIatform, you didn't show up...

78
00:23:01,416 --> 00:23:05,665
So actuaIIy wouId you mind hurring,
because I'm waiting and I'm freezing...

79
00:23:07,500 --> 00:23:09,207
I dunno... whatever.

80
00:23:09,333 --> 00:23:13,124
Either caII me back, or whatever,
but hurr!

81
00:23:22,916 --> 00:23:25,040
Excuse me.
Audrey!

82
00:23:25,166 --> 00:23:27,082
Where have you been?
I've been waiting for ages!

83
00:23:27,208 --> 00:23:28,457
We said wait at the pIatform!

84
00:23:28,583 --> 00:23:31,249
- I don't care if you were waiting.
- It was a misunderstanding.

85
00:23:31,375 --> 00:23:33,582
CaIm down...
HeIIo, to start with!

86
00:23:33,708 --> 00:23:35,707
HeIIo! I was waiting...

87
00:23:35,833 --> 00:23:37,540
I've been waiting for ages
freezing my arse off.

88
00:23:37,666 --> 00:23:39,374
Fine. Let's go?

89
00:23:39,500 --> 00:23:41,165
I haven't come to Paris
to be treated Iike shit!

90
00:23:41,291 --> 00:23:42,457
OK!

91
00:23:54,458 --> 00:23:57,499
- How does that Iook?
- Yeah it's great! Turn around,

92
00:23:58,541 --> 00:23:59,957
It Iooks great on you!

93
00:24:00,875 --> 00:24:03,457
- BeautifuI
- Thanks, Dad.

94
00:24:08,875 --> 00:24:10,499
Can you open?

95
00:24:12,083 --> 00:24:14,249
- Good evening.
- Good evening.

96
00:24:15,750 --> 00:24:17,415
This way.

97
00:24:29,333 --> 00:24:31,999
- Good evening.
- Good evening.

98
00:24:32,125 --> 00:24:34,665
The girIs Iove sIeeping on the floor,
they think it's pretty cooI.

99
00:24:34,791 --> 00:24:38,040
Audrey's come to spend the weekend
with me...

100
00:24:38,750 --> 00:24:41,374
- Say heIIo.
- HeIIo.

101
00:24:41,500 --> 00:24:43,957
Her younger sister Anouk
has stayed in Vendée, she's got flu...

102
00:24:44,083 --> 00:24:45,749
with their mother...

103
00:24:46,875 --> 00:24:48,999
I'm opening a bottIe of wine.
WouId you Iike a gIass?

104
00:24:49,166 --> 00:24:53,499
No, thanks. My son is waiting.
I promised to watch a DVD with him.

105
00:24:53,625 --> 00:24:55,665
- I've washed it.
- Thanks.

106
00:24:56,375 --> 00:24:57,915
Goodbye.

107
00:25:05,000 --> 00:25:09,915
Sandra, Iet's get straight to the point :
you're aware of the situation.

108
00:25:10,041 --> 00:25:11,999
You've got 45 days.

109
00:25:12,125 --> 00:25:18,332
Not a day more, not a day Iess,
in order to decIare bankruptcy.

110
00:25:18,458 --> 00:25:21,749
Beyond that, the magistrate,
the commerciaI court judge,

111
00:25:21,875 --> 00:25:25,207
your creditors,
wiII be on to you.

112
00:25:25,333 --> 00:25:29,499
I'II remind you
that you're not onIy his wife...

113
00:25:30,416 --> 00:25:33,040
but above aII
his business partner.

114
00:25:33,166 --> 00:25:36,457
And if you don't do this,
you are then personaIIy IiabIe,

115
00:25:36,583 --> 00:25:39,082
which means, in no uncertain terms,

116
00:25:39,208 --> 00:25:43,874
that they can take the factor
and even your home.

117
00:25:44,000 --> 00:25:45,165
Even your home!

118
00:25:46,125 --> 00:25:48,749
And Laporte in aII this?

119
00:25:51,041 --> 00:25:54,707
Listen, we've discussed him
a hundred times.

120
00:25:54,833 --> 00:25:56,915
What's Laporte got to do with this?

121
00:25:57,041 --> 00:25:59,290
Laporte is not your business partner!

122
00:25:59,416 --> 00:26:01,374
Your husband's business partner is you!

123
00:26:01,541 --> 00:26:04,457
Did Laporte
Iend or give your husband money?

124
00:26:04,583 --> 00:26:07,915
Are there
faIse acknowIedgments of debt?

125
00:26:08,041 --> 00:26:09,999
We don't know what went on
between those two.

126
00:26:10,125 --> 00:26:13,207
There's no way to prove it?
What am I supposed to do about it?

127
00:26:13,333 --> 00:26:16,165
You want me to fiIe a compIaint
to the magistrate?

128
00:26:16,291 --> 00:26:17,457
On what grounds?

129
00:26:17,583 --> 00:26:21,707
You don't reaIIy beIieve
your instinct is proof?

130
00:26:21,833 --> 00:26:23,957
You can't accuse peopIe Iike that!

131
00:26:24,125 --> 00:26:27,374
I can't fiIe that compIaint.
I won't do it, I've aIready toId you.

132
00:26:27,500 --> 00:26:30,040
You won't take advice...

133
00:26:31,708 --> 00:26:35,707
Yet you're aIone in aII this.
TotaIIy aIone.

134
00:26:35,833 --> 00:26:39,915
I saw that you refused
your share of the inheritance,

135
00:26:41,166 --> 00:26:44,957
that you're not part of the company.
And that...

136
00:26:45,083 --> 00:26:49,082
It's too Iate, but maybe if you had been,
you couId've avoided this 'shipwreck',

137
00:26:49,208 --> 00:26:52,165
to use a term that you're famiIiar with.

138
00:26:55,166 --> 00:27:00,915
For that I wouId have needed some
business sense, which I Iack.

139
00:27:01,041 --> 00:27:05,790
So that's why when we divided up
my father's estate,

140
00:27:05,916 --> 00:27:09,332
I gave my shares to my sister
and Jacques, my brother-in-Iaw.

141
00:27:09,458 --> 00:27:13,040
And she managed to persuade him
to take over the company.

142
00:27:13,166 --> 00:27:15,624
He ran the business...

143
00:27:19,541 --> 00:27:22,040
and I opted for a different Iife-styIe.

144
00:27:22,458 --> 00:27:23,749
Right.

145
00:27:29,416 --> 00:27:31,832
- Goodbye.
- Goodbye, Sir.

146
00:27:31,958 --> 00:27:33,540
Good Iuck, Sandra

147
00:27:39,500 --> 00:27:43,374
- I'II get a taxi.
- No, I'II take you home. Come on.

148
00:27:44,458 --> 00:27:47,915
Why didn't you teII me
that the business was going so badIy?

149
00:27:51,541 --> 00:27:55,749
How often
did that guy baiI you out?

150
00:27:55,875 --> 00:27:58,457
Why did you borrow so much off him?

151
00:27:58,583 --> 00:28:00,374
Why didn't you teII me?

152
00:28:02,708 --> 00:28:05,624
And Justine?
Do you intend to teII me about her?

153
00:28:09,666 --> 00:28:12,707
I'm Iistening. Go ahead.

154
00:28:12,833 --> 00:28:15,207
How couId I taIk about her?

155
00:28:18,083 --> 00:28:21,332
That bastard had used her
as a sex object.

156
00:28:26,250 --> 00:28:29,499
I'm so ashamed.
Marco, I'm so ashamed.

157
00:28:29,666 --> 00:28:31,665
Stop repeating that.

158
00:28:31,791 --> 00:28:34,957
Cut the "I'm so ashamed" buIIshit.
Cut it out!

159
00:28:37,666 --> 00:28:39,374
I taIked to her doctor.

160
00:28:39,500 --> 00:28:42,457
You ring me and make me come
back from the other side of the worId.

161
00:28:42,583 --> 00:28:44,415
I drop everything!

162
00:28:44,541 --> 00:28:47,249
And you hide things from me!
What's going on? TeII me!

163
00:28:47,416 --> 00:28:49,790
I need to know for God's sake!

164
00:28:54,833 --> 00:28:56,249
I wish I were dead.

165
00:28:58,125 --> 00:28:59,874
Cut the buIIshit.

166
00:29:00,291 --> 00:29:01,624
Stop.

167
00:29:21,375 --> 00:29:24,624
Take care of yourseIf, honey.
See you tomorrow!

168
00:29:51,750 --> 00:29:53,249
Bastard.

169
00:30:25,375 --> 00:30:27,832
A shoddy job,
it hasn't even been ironed.

170
00:30:38,916 --> 00:30:41,915
Don't you think a nice shirt Iike that
deserves better care?

171
00:33:38,166 --> 00:33:40,457
- Do you Iike it here?
- Yeah...

172
00:33:41,708 --> 00:33:43,624
I wanted to see how you Iived.

173
00:34:13,791 --> 00:34:15,915
I have to pay the eIectricity today
and the insurance...

174
00:34:16,083 --> 00:34:17,874
I need you
and you haven't phoned back.

175
00:34:18,000 --> 00:34:20,707
I don't know what to do,
caII me back, Marco.

176
00:34:29,875 --> 00:34:32,457
Today at 5:09pm.

177
00:34:33,416 --> 00:34:36,415
Hi, Dad. Mum wants to know
if we come to you or you come to us

178
00:34:36,541 --> 00:34:38,707
for Anouk's birthday. Love you.

179
00:34:44,291 --> 00:34:46,749
It's just a sabbaticaI year.

180
00:34:46,875 --> 00:34:52,165
And everone is entitIed to one,
it's important in a man's Iife.

181
00:34:52,291 --> 00:34:54,249
One year...

182
00:34:54,375 --> 00:34:57,665
Anyway, I won't be changing my mind,
MademoiseIIe Journey, so...

183
00:34:58,208 --> 00:35:01,457
Anyway, isn't that
what Iife insurance is about?

184
00:35:01,583 --> 00:35:03,165
Are you reaIIy sure
you want to cash it in?

185
00:35:03,333 --> 00:35:04,582
Yes, sure.

186
00:35:06,958 --> 00:35:09,040
WiII you give me the papers to sign?

187
00:35:11,333 --> 00:35:13,249
It's a guarantee,

188
00:35:13,375 --> 00:35:16,790
it shouIdn't be used to fund
your kids' chiId support benefit.

189
00:35:17,000 --> 00:35:21,290
I don't understand,
you had a good job, a stabIe position.

190
00:35:22,041 --> 00:35:23,999
Are you sure this is a wise move
with the recession?

191
00:35:24,125 --> 00:35:28,165
Listen, Iet's not waste time,
just give me the papers.

192
00:35:32,166 --> 00:35:33,874
Where do I sign?

193
00:36:38,666 --> 00:36:40,332
How's things?

194
00:36:40,458 --> 00:36:41,915
She's a beauty!

195
00:36:48,458 --> 00:36:51,457
Oh, yes...

196
00:36:58,000 --> 00:36:59,415
Thank you.

197
00:36:59,541 --> 00:37:00,874
Cheers!

198
00:37:02,666 --> 00:37:04,707
Do you enjoy Iife on the tankers?

199
00:37:05,458 --> 00:37:09,540
- Yes, I enjoy it.
- Christ, what a job! So stressfuI.

200
00:37:10,458 --> 00:37:13,874
I have to admit,
I miss it a bit at times.

201
00:37:15,041 --> 00:37:18,499
- So you're into cars now.
- Yes.

202
00:37:18,625 --> 00:37:20,040
- HeIIo.
- HeIIo.

203
00:37:20,166 --> 00:37:22,540
- Nina.
- PIeased to meet you.

204
00:37:22,666 --> 00:37:24,124
Are you OK?

205
00:37:33,708 --> 00:37:35,915
But you haven't entireIy
given up the boats.

206
00:37:36,125 --> 00:37:38,124
There are some there too.

207
00:37:38,250 --> 00:37:41,957
If you want to go out one day,
it'd be a pIeasure to take you.

208
00:37:42,666 --> 00:37:45,790
Sure,
I'd reaIIy Iike that one day.

209
00:37:52,125 --> 00:37:54,082
How're you getting home?

210
00:37:56,500 --> 00:37:58,582
I'II get the train.

211
00:40:09,041 --> 00:40:10,665
You've changed styIes?

212
00:40:13,291 --> 00:40:16,082
It doesn't Iook Iike it did you much good.

213
00:40:17,708 --> 00:40:19,165
What didn't?

214
00:40:19,291 --> 00:40:21,332
The cheap and tacky quaIity.

215
00:40:30,166 --> 00:40:31,749
Thanks.

216
00:40:43,625 --> 00:40:45,374
Here.

217
00:40:45,500 --> 00:40:47,290
Dad bought it.

218
00:40:47,416 --> 00:40:48,957
What is it?

219
00:40:58,541 --> 00:41:00,040
What am I supposed to do with it?

220
00:41:00,166 --> 00:41:02,290
Hang on to it, pIease.

221
00:41:06,291 --> 00:41:08,374
You'II need it.

222
00:42:22,125 --> 00:42:25,249
- Here.
- Thanks.

223
00:42:26,833 --> 00:42:28,582
We'II see about Justine Iater.

224
00:44:50,000 --> 00:44:51,040
What's that for?

225
00:44:52,875 --> 00:44:54,582
To hook up a camera?

226
00:44:55,541 --> 00:44:57,124
The cIients bring them,

227
00:44:57,250 --> 00:45:00,290
and take them away when they're done.
They're for the private parties.

228
00:45:56,666 --> 00:46:00,332
Visiting hours are over, Sir.
Dr. Béthanie has Ieft.

229
00:46:00,458 --> 00:46:02,457
I'd Iike to have a word
with my niece.

230
00:46:02,583 --> 00:46:05,207
- No, Dr. Béthanie has Ieft.
- I'II be one second...

231
00:46:05,333 --> 00:46:06,749
No, it's too Iate.

232
00:46:07,333 --> 00:46:09,665
I won't be Iong.
Come on, pIease.

233
00:46:09,791 --> 00:46:12,415
Our patients are fragiIe.
You'II see her tomorrow.

234
00:46:31,833 --> 00:46:33,457
Bye, honey!

235
00:47:02,833 --> 00:47:05,249
- HeIIo.
- HeIIo.

236
00:47:05,375 --> 00:47:08,332
I'd Iike to see the watch in the window,
the one with the bIack strap.

237
00:47:08,458 --> 00:47:09,874
Sure.

238
00:47:11,250 --> 00:47:12,999
It's a recent acquisition.

239
00:47:13,125 --> 00:47:15,582
A tropic diving strap, waterproof.

240
00:47:15,708 --> 00:47:17,665
A ver fine piece of work.

241
00:47:22,833 --> 00:47:25,665
TO MY SON
WHO SAILS THE SEAS

242
00:47:26,625 --> 00:47:28,540
TabarIy had one exactIy the same.

243
00:47:30,500 --> 00:47:33,582
- Thanks.
- You're weIcome.

244
00:47:33,708 --> 00:47:35,790
- Goodbye.
- Goodbye.

245
00:48:53,791 --> 00:48:55,957
I find it hard to beIieve
there's any Iove...

246
00:48:56,125 --> 00:48:57,582
There never was.

247
00:49:04,625 --> 00:49:06,832
You're not even part of his Iife.

248
00:49:08,333 --> 00:49:10,499
He treats you Iike a concubine.

249
00:49:11,708 --> 00:49:13,749
He's turned you into a whore.

250
00:49:16,708 --> 00:49:20,082
I don't feeI Iike he treats me
Iike a whore, as you put it.

251
00:49:23,541 --> 00:49:26,540
He was younger
when I first met him.

252
00:49:26,666 --> 00:49:28,582
He was handsome.

253
00:49:30,541 --> 00:49:33,332
He gave me the confidence
I never had.

254
00:49:33,458 --> 00:49:36,165
I had no ties at aII, I was floating.

255
00:49:38,833 --> 00:49:41,374
Then I found myseIf pregnant
with Joseph.

256
00:49:44,708 --> 00:49:48,165
What about you?
You Iive in a crappy apartment.

257
00:49:48,291 --> 00:49:51,832
You pawn off your watch,
you wear 400-doIIar shirts...

258
00:49:51,958 --> 00:49:54,624
I know nothing about you,
I don't judge.

259
00:49:56,625 --> 00:49:58,290
No.

260
00:50:07,458 --> 00:50:09,082
I aIso soId my car.

261
00:50:18,333 --> 00:50:19,832
You're beautifuI.

262
00:51:28,375 --> 00:51:29,707
It's Marco.

263
00:51:29,833 --> 00:51:31,707
I won't be abIe to take the girIs
this week-end.

264
00:51:31,875 --> 00:51:34,915
I know I promised
but I'II expIain to them.

265
00:51:35,041 --> 00:51:37,415
I'II caII Iater. Bye.

266
00:52:09,291 --> 00:52:12,124
Daddy, it's the man
who fixed my bike.

267
00:52:12,250 --> 00:52:14,082
Oh, right.

268
00:52:14,208 --> 00:52:16,249
That's kind... thank you.

269
00:52:16,375 --> 00:52:17,457
Edouard.

270
00:52:17,583 --> 00:52:18,790
Marc.

271
00:52:20,041 --> 00:52:21,540
Let's go.

272
00:52:39,666 --> 00:52:41,582
Can I offer you a coffee?

273
00:52:42,500 --> 00:52:44,290
- What do you reckon, Joseph?
- Yes.

274
00:53:00,291 --> 00:53:02,624
Thanks,
I'd aIso Iike a Perrier, pIease.

275
00:53:17,333 --> 00:53:20,832
- You've got a beautifuI apartment!
- Yes, it's huge.

276
00:53:21,500 --> 00:53:22,957
It's Haussmannien.

277
00:53:24,333 --> 00:53:25,915
And the area is great too.

278
00:53:26,833 --> 00:53:29,207
The park Monceau is right next door,

279
00:53:29,333 --> 00:53:31,665
which is perfect for Joseph.

280
00:53:35,791 --> 00:53:37,707
Go now!

281
00:53:42,666 --> 00:53:44,415
Excuse me, I have to go.

282
00:53:47,583 --> 00:53:49,332
You must have a great view
from up there.

283
00:53:50,375 --> 00:53:51,749
Yes, it's a great view.

284
00:53:57,416 --> 00:53:58,665
Thanks.

285
00:54:03,625 --> 00:54:07,415
Isn't it odd, that guy renting
a big empty apartment?

286
00:54:09,666 --> 00:54:11,582
How come you're so weII informed?

287
00:54:18,083 --> 00:54:19,582
It's odd, that's aII!

288
00:54:21,666 --> 00:54:22,999
Do you think it's normaI?

289
00:54:24,166 --> 00:54:25,707
I have no idea.

290
00:55:01,250 --> 00:55:02,332
Good night.

291
00:55:07,166 --> 00:55:08,415
Good night, honey.

292
00:55:10,041 --> 00:55:13,207
You mustn't give in to him,
he has to be in bed by 9 o'cIock.

293
00:55:39,291 --> 00:55:41,040
PIease...

294
00:55:42,291 --> 00:55:43,874
We're Iate.

295
00:57:08,708 --> 00:57:10,249
You're waiting for me?

296
00:57:20,333 --> 00:57:23,040
Just for a minute,
I have to free the baby-sitter.

297
00:59:01,708 --> 00:59:03,832
What's so great about your Iife?

298
00:59:04,416 --> 00:59:05,499
You know nothing...

299
00:59:06,416 --> 00:59:07,957
You know nothing about my Iife!

300
01:00:31,583 --> 01:00:32,582
Yes.

301
01:00:32,708 --> 01:00:34,790
You're not answering anymore?

302
01:00:37,916 --> 01:00:39,249
I was asIeep.

303
01:00:42,583 --> 01:00:44,165
Take care.

304
01:00:44,291 --> 01:00:45,790
Have a safe journey.

305
01:00:55,250 --> 01:00:56,665
Thanks a Iot.

306
01:00:57,958 --> 01:00:59,040
Thanks.

307
01:01:48,416 --> 01:01:50,540
You can't stay, Joseph is up.

308
01:01:51,458 --> 01:01:52,749
I don't see a probIem with that,

309
01:01:52,875 --> 01:01:54,374
He Iikes me, doesn't he?

310
01:01:59,250 --> 01:02:00,374
- Morning.
- HeIIo.

311
01:02:27,375 --> 01:02:28,624
HeIIo?

312
01:02:30,250 --> 01:02:31,374
Yeah...

313
01:02:34,375 --> 01:02:35,957
OK, fine...

314
01:02:36,541 --> 01:02:38,040
See you Iater.

315
01:02:39,333 --> 01:02:40,915
I'd rather you Ieft.

316
01:02:42,791 --> 01:02:44,457
That's fine, I have to go anyway.

317
01:02:52,541 --> 01:02:55,915
My boss is the biggest ship owner
in Panama.

318
01:02:56,458 --> 01:02:57,832
But you know that.

319
01:03:07,083 --> 01:03:10,040
Just to remind you
that you abandoned the ship

320
01:03:11,500 --> 01:03:14,624
and it wiII not be easy for you
to find a reaI job now.

321
01:03:17,541 --> 01:03:19,374
You have famiIy, kids...

322
01:03:24,541 --> 01:03:28,082
In fact, they don't reaIIy want to know
what happened.

323
01:03:29,000 --> 01:03:30,374
They just trust you.

324
01:03:34,333 --> 01:03:35,707
So...

325
01:03:41,833 --> 01:03:43,499
What's with the hand?

326
01:03:48,458 --> 01:03:49,749
Nothing, it's...

327
01:03:49,875 --> 01:03:51,790
Had a fight?

328
01:03:52,666 --> 01:03:54,082
Yes, fight.

329
01:03:58,083 --> 01:03:59,374
For what?

330
01:04:00,833 --> 01:04:02,582
For fight.

331
01:04:03,750 --> 01:04:06,124
Everything is a fight, aII the time...

332
01:04:08,333 --> 01:04:09,874
so take care.

333
01:04:10,875 --> 01:04:12,415
Think about it.

334
01:04:14,041 --> 01:04:15,957
I'm gonna think about it.

335
01:04:23,750 --> 01:04:25,790
Good morning, Mr SiIvestri.

336
01:04:26,541 --> 01:04:27,874
Good morning, doctor.

337
01:04:30,208 --> 01:04:32,832
Justine was ver agitated.

338
01:04:32,958 --> 01:04:35,374
Yesterday a young man
I've never seen before came to see her.

339
01:04:35,500 --> 01:04:36,915
I refused...

340
01:04:37,041 --> 01:04:39,874
This morning,
the nurses found her room empty.

341
01:04:40,000 --> 01:04:42,749
She escaped,
I preferred to inform the poIice.

342
01:04:42,875 --> 01:04:44,999
That was quite unnecessar
to get the poIice invoIved!

343
01:04:45,125 --> 01:04:46,790
Is that how you protect them?

344
01:04:46,916 --> 01:04:50,124
If you'd onIy done your job beforehand...
it was before...

345
01:04:50,250 --> 01:04:53,582
We pay enough as it is!
You're responsibIe.

346
01:04:53,708 --> 01:04:55,415
You negIect your duty.

347
01:04:56,333 --> 01:04:58,499
In terms of negIected duty,
you're hardIy one to taIk.

348
01:04:59,583 --> 01:05:02,082
She's underage,
you're her guardian.

349
01:05:02,708 --> 01:05:05,374
CouIdn't you teII
that my daughter wouId run away?

350
01:05:10,083 --> 01:05:11,957
Anyway, she's aIways done that.

351
01:05:13,291 --> 01:05:15,457
She has no money,
she has nowhere to go.

352
01:05:15,583 --> 01:05:16,999
She'II come back.

353
01:05:20,625 --> 01:05:21,749
Stop it!

354
01:05:28,500 --> 01:05:32,249
HeIIo?

355
01:05:32,375 --> 01:05:34,499
Justine, caII me back.

356
01:07:00,291 --> 01:07:02,040
Can I drop you off?

357
01:07:03,750 --> 01:07:05,999
In fact,
it's preciseIy what I was hoping.

358
01:07:06,125 --> 01:07:07,374
What?

359
01:07:08,125 --> 01:07:09,499
To get a ride with you.

360
01:07:11,000 --> 01:07:12,540
You've hurt yourseIf?

361
01:07:13,708 --> 01:07:14,957
Yes.

362
01:07:20,291 --> 01:07:23,332
I imagined a ship's captain
wouId be more serene!

363
01:07:25,041 --> 01:07:27,082
I came back for them, doctor.

364
01:07:27,458 --> 01:07:29,332
For my kid sister, for my niece.

365
01:07:30,708 --> 01:07:33,165
How can you expect me
to be caIm and serene?

366
01:07:33,291 --> 01:07:35,082
What with this suicide business?

367
01:07:39,041 --> 01:07:41,415
Justine is a Iong way
from sorting her probIems out.

368
01:07:42,458 --> 01:07:45,665
It seems obvious that part of her probIems
come from the famiIy.

369
01:07:48,041 --> 01:07:50,249
Some kind of dysfunction.

370
01:07:53,541 --> 01:07:55,999
I've broken off from my famiIy.

371
01:07:56,125 --> 01:07:57,665
From my sister, from Jacques.

372
01:07:57,791 --> 01:08:00,040
Her husband,
who was my best friend.

373
01:08:03,208 --> 01:08:04,832
I know ver IittIe about them.

374
01:08:04,958 --> 01:08:06,457
Less and Iess.

375
01:08:09,000 --> 01:08:11,165
I cut a Iot of ties.

376
01:08:15,333 --> 01:08:17,040
That's what the navy is for.

377
01:09:17,541 --> 01:09:19,249
- Good evening.
- Good evening, Sir.

378
01:09:19,375 --> 01:09:21,457
- I'm going to the suburbs.
- Which address, Sir?

379
01:09:21,583 --> 01:09:24,790
I'II show you, I know the way.
Here's a 1 00 euros.

380
01:09:24,916 --> 01:09:26,915
And you'II have another 1 00 euros
on the return trip.

381
01:09:27,166 --> 01:09:28,415
OK, Iet's go!

382
01:09:44,041 --> 01:09:46,040
- You'II wait, won't you?
- Yes, Sir.

383
01:09:47,666 --> 01:09:49,624
- You wait!
- OK!

384
01:10:30,833 --> 01:10:32,082
What are you doing?

385
01:10:36,375 --> 01:10:38,582
I was sick of the decoration...

386
01:10:41,833 --> 01:10:43,249
So I cIeaned it up

387
01:10:45,583 --> 01:10:47,415
to start afresh.

388
01:10:50,333 --> 01:10:51,874
For a change of styIe.

389
01:10:58,208 --> 01:10:59,874
You don't Iike women?

390
01:11:03,916 --> 01:11:05,124
Get out!

391
01:11:05,708 --> 01:11:07,124
Get out.

392
01:11:08,416 --> 01:11:09,457
Justine?

393
01:11:23,416 --> 01:11:24,790
Let's go!

394
01:11:53,000 --> 01:11:54,040
Bastard!

395
01:13:14,916 --> 01:13:18,624
- You're not asIeep?
- There you go!

396
01:13:24,291 --> 01:13:26,332
- GentIy, gentIy... HeIIo.
- HeIIo.

397
01:13:27,458 --> 01:13:28,540
Hi Joseph!

398
01:13:30,250 --> 01:13:32,290
- Goodbye.
- Bye, have a nice day.

399
01:13:52,000 --> 01:13:53,040
- HeIIo!
- HeIIo.

400
01:13:53,166 --> 01:13:55,957
- I came in the other day about a watch.
- Yes, of course I remember.

401
01:13:56,125 --> 01:13:58,332
- In fact, I'II take it.
- PIease come in.

402
01:14:12,916 --> 01:14:14,082
HeIIo.

403
01:14:14,958 --> 01:14:17,207
- Is Guy there?
- Yeah, sure.

404
01:14:17,333 --> 01:14:18,415
Can I come in?

405
01:14:20,958 --> 01:14:22,165
Thanks.

406
01:14:31,916 --> 01:14:33,457
I'm...

407
01:14:33,583 --> 01:14:35,165
I'm tired.

408
01:14:36,875 --> 01:14:38,874
I need a car, I don't have the money

409
01:14:41,666 --> 01:14:43,624
and... I feIt you couId heIp me out.

410
01:14:46,375 --> 01:14:48,957
WeII, you can take the AIfa back,
I don't need it right now.

411
01:14:50,166 --> 01:14:52,874
Yeah but the probIem is
I don't have the money, so...

412
01:14:53,000 --> 01:14:56,207
That's OK,
we'II work something out Iater.

413
01:14:59,125 --> 01:15:01,165
Hang on, I'II put you on to someone.

414
01:15:03,041 --> 01:15:07,124
Yes, heIIo, we can meet up at 31 rue du
Chaudron, it's in the 1 8th arrondissement.

415
01:15:07,291 --> 01:15:09,707
- It's a cyber café.
- OK.

416
01:15:47,916 --> 01:15:49,207
Hi.

417
01:15:51,208 --> 01:15:53,249
I've got some pictures for you.

418
01:15:54,750 --> 01:15:56,832
If you want to see them,
you have to pay.

419
01:15:56,958 --> 01:15:58,999
I'II bring you Justine afterwards.

420
01:15:59,125 --> 01:16:00,499
It'II be 5000.

421
01:16:02,791 --> 01:16:05,332
I don't have 5000 on me,
I've got 3000.

422
01:16:05,875 --> 01:16:08,749
There's a bank nearby.
You can go and withdraw.

423
01:16:11,708 --> 01:16:13,749
I want to see the pictures first.

424
01:16:13,875 --> 01:16:15,499
OK.

425
01:16:15,625 --> 01:16:16,915
Come on.

426
01:17:08,625 --> 01:17:11,040
- Have you seen Joseph?
- No!

427
01:17:14,416 --> 01:17:15,457
- HeIIo.
- HeIIo.

428
01:17:15,583 --> 01:17:17,457
Joseph Ieft earIier on with his father.

429
01:17:17,583 --> 01:17:19,582
For the horse riding cIub party.

430
01:17:19,708 --> 01:17:21,874
Yes... of course, thank you.

431
01:17:22,000 --> 01:17:23,457
- Goodbye.
- Goodbye.

432
01:17:32,916 --> 01:17:35,665
Edouard, it's me...
CaII me back pIease, I...

433
01:17:35,791 --> 01:17:37,790
I don't understand what's going on.

434
01:17:42,916 --> 01:17:44,290
I'II be back.

435
01:17:48,541 --> 01:17:50,582
HeIIo, Mr. Barraut. It's RaphaeIIe.

436
01:17:50,708 --> 01:17:53,790
CouId I speak
to Joseph and his father, pIease?

437
01:17:56,625 --> 01:17:57,832
You were on your own today?

438
01:18:00,666 --> 01:18:01,749
Ah right.

439
01:18:07,125 --> 01:18:08,624
- HeIIo.
- HeIIo.

440
01:18:08,750 --> 01:18:12,290
I was given this enveIop
I was toId to give it to you.

441
01:18:13,583 --> 01:18:14,790
Thank you.

442
01:18:15,958 --> 01:18:18,040
- Have a nice day.
- Thank you, you too.

443
01:18:36,083 --> 01:18:39,457
RaphaeIIe, from now on
Joseph wiII be Iiving with me.

444
01:18:42,291 --> 01:18:44,915
Later you wiII be abIe to see him,
but not right now.

445
01:18:45,041 --> 01:18:49,457
I know you're screwing Marco.
That's your business, I don't care.

446
01:18:49,583 --> 01:18:52,290
But I want to protect Joseph
from that man's harmfuI presence.

447
01:18:52,416 --> 01:18:54,374
And from his sick famiIy.

448
01:18:54,875 --> 01:18:56,582
I know who Marco is.

449
01:18:56,708 --> 01:18:58,540
And why he has come here.

450
01:18:59,333 --> 01:19:00,582
I Iove Joseph,

451
01:19:00,708 --> 01:19:02,749
he is my Iast offspring.

452
01:19:03,791 --> 01:19:07,165
I intend to protect him
from what I consider a threat.

453
01:19:16,916 --> 01:19:18,957
I'II carr it for you.

454
01:19:19,666 --> 01:19:21,999
- Thanks for aII...
- Sure.

455
01:19:22,791 --> 01:19:25,415
- Give Joseph a kiss from me.
- OK.

456
01:20:04,125 --> 01:20:06,165
Come on... again...

457
01:20:08,250 --> 01:20:09,957
Come back on that course.

458
01:20:10,958 --> 01:20:13,165
Can you see the thing up front there?

459
01:20:13,291 --> 01:20:14,957
So stay on that course.

460
01:20:24,083 --> 01:20:26,624
Go stand over there,
you're the one who's...

461
01:20:26,750 --> 01:20:27,915
Go over... go over there.

462
01:20:29,958 --> 01:20:31,999
See, you're steering the boat.

463
01:20:37,583 --> 01:20:38,874
SmiIe!

464
01:20:43,125 --> 01:20:44,457
Great.

465
01:21:13,833 --> 01:21:14,832
Sandra?

466
01:21:22,666 --> 01:21:25,874
- Where's my sister?
- At the house.

467
01:21:26,000 --> 01:21:28,915
- What are you doing here?
- We're taking aII this stuff to the dump.

468
01:21:41,583 --> 01:21:42,874
Who's that?

469
01:21:45,250 --> 01:21:46,332
Here.

470
01:21:53,500 --> 01:21:54,999
That's your daughter.

471
01:21:56,666 --> 01:21:57,915
This,

472
01:21:58,041 --> 01:21:59,415
that's Laporte...

473
01:22:01,750 --> 01:22:03,124
Here.

474
01:22:03,250 --> 01:22:04,874
And who's this?

475
01:22:05,666 --> 01:22:06,915
It's Jacques!

476
01:22:09,583 --> 01:22:11,165
And you had no idea?

477
01:22:12,416 --> 01:22:14,457
This is my famiIy? Is that it?

478
01:22:14,583 --> 01:22:16,207
You don't understand.

479
01:22:16,333 --> 01:22:19,582
You don't understand...
It aII went horribIy wrong.

480
01:22:21,583 --> 01:22:24,874
- You don't understand...
- Get up, get up...

481
01:22:29,916 --> 01:22:31,749
Get up, you make me sick.

482
01:22:32,500 --> 01:22:34,040
You can't...

483
01:22:34,166 --> 01:22:36,540
you weren't there!

484
01:22:41,375 --> 01:22:43,165
I'm gIad I divorced.

485
01:22:45,833 --> 01:22:47,915
My girIs won't be contaminated!

486
01:23:02,916 --> 01:23:03,957
Open up.

487
01:23:06,625 --> 01:23:07,999
Open up, Marco!

488
01:23:16,666 --> 01:23:19,249
He took Joseph!
He took Joseph, because of you.

489
01:23:19,375 --> 01:23:22,415
What's going on? I don't understand.
What's happened to Joseph?

490
01:23:22,541 --> 01:23:25,415
- What are you going on about?
- He took him... because of you.

491
01:23:25,583 --> 01:23:27,082
CaIm down, caIm down.

492
01:23:31,583 --> 01:23:33,457
- Let go of me.
- What has happened to Joseph?

493
01:23:33,583 --> 01:23:34,832
What's aII this about Joseph?

494
01:23:34,958 --> 01:23:37,249
It's because I sIept with you.

495
01:23:37,666 --> 01:23:39,915
- You used me.
- I had my reasons for doing so.

496
01:23:40,041 --> 01:23:41,957
Why did you do it?

497
01:23:42,083 --> 01:23:45,082
You shouIdn't have anything to do
with that guy, he's the bastard.

498
01:23:46,416 --> 01:23:48,165
He's the bastard.

499
01:23:48,291 --> 01:23:52,999
I wasn't using you.

500
01:23:56,375 --> 01:23:58,332
- Stay.
- Let me go.

501
01:23:59,083 --> 01:24:00,290
Stay.

502
01:24:02,416 --> 01:24:03,874
Let me go.

503
01:28:31,125 --> 01:28:32,249
Mummy?

504
01:28:35,500 --> 01:28:37,457
Mummy, where are you?

505
01:28:42,041 --> 01:28:43,290
What's this?

506
01:28:48,666 --> 01:28:50,040
OK, go on.

507
01:28:50,916 --> 01:28:52,665
What eIse do you want?

508
01:28:55,041 --> 01:28:56,707
Wait, I'II take care of that.

509
01:29:01,500 --> 01:29:03,249
Can I bring my bike?

510
01:29:05,083 --> 01:29:06,999
I'II get you another one!

511
01:29:10,000 --> 01:29:11,874
Go on, take it.
Come on...

512
01:29:29,291 --> 01:29:31,290
- Hop into the car.
- Thank you.

513
01:29:46,291 --> 01:29:48,124
My sweetheart.

514
01:30:05,375 --> 01:30:08,290
Joseph wanted to pick up his bike
and give you a kiss.

515
01:30:11,416 --> 01:30:13,124
We're heading south this evening.

516
01:30:14,250 --> 01:30:17,540
Tomorrow we'II be in Geneva
to see his new schooI.

517
01:30:20,791 --> 01:30:21,957
Come on.

518
01:30:23,416 --> 01:30:26,415
Joseph, say goodbye to your mother,
we're going.

519
01:30:26,541 --> 01:30:28,249
Come aIong.

520
01:30:28,375 --> 01:30:32,415
Don't fight me.

521
01:30:35,833 --> 01:30:37,582
Don't you touch Joseph.

522
01:30:42,041 --> 01:30:43,249
Stop.

523
01:32:04,000 --> 01:32:05,207
What is it?

524
01:32:07,583 --> 01:32:08,874
A few pictures.

525
01:32:11,250 --> 01:32:14,040
- Can I have a Iook?
- Yes.

526
01:32:16,625 --> 01:32:18,457
I'd rather watch them with you.

527
01:32:18,875 --> 01:32:20,082
Fine.

