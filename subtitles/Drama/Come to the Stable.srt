1
00:02:02,122 --> 00:02:11,463
FEMALE PRISONER #701
SCORPION - BEAST STABLE

2
00:02:39,959 --> 00:02:44,396
"Flowers are beautiful"
- his words flatter you today.

3
00:02:45,064 --> 00:02:49,899
But once you're in full bloom,
he'll just toss you away.

4
00:02:50,036 --> 00:02:54,598
Foolish, foolish,
foolish woman's song...

5
00:02:54,741 --> 00:02:59,769
Her song of vengeance.

6
00:03:03,917 --> 00:03:08,286
"You cling to your dreams"
- they scorn your world of lies.

7
00:03:08,855 --> 00:03:13,724
So you try to wake up,
but you can't open your eyes.

8
00:03:13,960 --> 00:03:18,693
A woman, a woman,
a woman's heart is her song...

9
00:03:18,831 --> 00:03:23,791
Her song of vengeance.

10
00:03:28,041 --> 00:03:33,001
A bright red rose has thorns
that you might not see.

11
00:03:33,446 --> 00:03:38,247
I don't want to pierce you,
but how else will I get free?

12
00:03:38,384 --> 00:03:43,151
Burning, burning,
burning woman's song...

13
00:03:43,356 --> 00:03:48,589
Her song of vengeance.

14
00:03:50,230 --> 00:03:55,065
Shedding blood once a month
should help, and yet...

15
00:03:55,735 --> 00:04:00,536
Try as I might, I can't ever forget.

16
00:04:00,673 --> 00:04:05,303
A woman, a woman,
a woman's life is her song...

17
00:04:05,445 --> 00:04:11,042
Her song of vengeance.

18
00:05:02,101 --> 00:05:03,329
Big Brother...

19
00:05:05,238 --> 00:05:07,138
You've had enough, haven't you?

20
00:06:19,612 --> 00:06:21,170
Hey, mister!

21
00:06:24,684 --> 00:06:26,743
Hey, mister.

22
00:06:28,688 --> 00:06:31,156
Would you like to see something hot?

23
00:06:36,095 --> 00:06:39,997
20 yen for one match. 3 for 50 yen...
I'll give you a discount!

24
00:06:40,199 --> 00:06:41,188
Sure.

25
00:06:41,801 --> 00:06:42,927
Here you go.

26
00:06:46,439 --> 00:06:47,929
Hurry up, will you?

27
00:06:49,509 --> 00:06:53,468
Come on, come on, deeper!
I can't see anything!

28
00:06:54,480 --> 00:06:57,449
Goddamn you! If you want a
good look, give me more money!

29
00:06:57,550 --> 00:06:59,575
What the hell. Let's go.

30
00:12:00,853 --> 00:12:01,877
No!

31
00:12:04,256 --> 00:12:08,818
My brother can't help himself.
He doesn't know what he's doing.

32
00:12:08,994 --> 00:12:12,623
There was an accident at the factory...
it's left him brain-damaged!

33
00:12:20,873 --> 00:12:25,173
I don't believe you, either!
How could you do that?

34
00:12:25,277 --> 00:12:29,338
Don't I always give you
all the sex you want?

35
00:17:46,298 --> 00:17:48,027
Let me stay the night.

36
00:17:51,070 --> 00:17:54,369
I'm not going home to my brother.

37
00:18:11,256 --> 00:18:15,215
I don't care about him.
He can just starve to death!

38
00:18:23,669 --> 00:18:25,569
What? Is that wrong?

39
00:18:26,638 --> 00:18:28,765
Kill him for me, then.

40
00:18:29,341 --> 00:18:33,243
You would've the other day
if I hadn't stopped you...

41
00:18:34,246 --> 00:18:36,441
The cops are after you, anyway!

42
00:19:21,460 --> 00:19:22,654
Yuki...

43
00:19:26,298 --> 00:19:30,166
That's right. I'm pregnant.

44
00:19:33,272 --> 00:19:35,206
It's my brother's baby!

45
00:19:43,215 --> 00:19:44,113
Yuki!

46
00:22:26,511 --> 00:22:28,376
Hey, there, mister!

47
00:22:29,247 --> 00:22:31,306
Want to see something hot?

48
00:22:32,517 --> 00:22:33,211
Damn!

49
00:22:39,124 --> 00:22:43,356
Look at this bitch. Thinks she's
gonna do business here, huh?

50
00:22:45,330 --> 00:22:47,389
Goddamn you...

51
00:22:48,533 --> 00:22:51,696
- Caught her again, huh?
- Teach her a lesson!

52
00:22:51,803 --> 00:22:53,361
Ha! Serves you right!

53
00:22:53,505 --> 00:22:54,767
You, slut!

54
00:22:54,906 --> 00:22:56,771
Taking business away from us!

55
00:22:56,875 --> 00:22:57,842
You moron!

56
00:22:58,110 --> 00:23:00,738
Hey, the moron's calling her a moron!

57
00:23:00,912 --> 00:23:03,574
You're not really gonna
have that baby, are you?

58
00:23:03,682 --> 00:23:06,412
She doesn't even know who the father is!

59
00:23:11,189 --> 00:23:13,123
Goddamn you, bitch!

60
00:23:14,893 --> 00:23:15,621
Slut!

61
00:23:18,530 --> 00:23:19,963
Please, stop!

62
00:23:23,568 --> 00:23:24,592
Boss.

63
00:23:25,470 --> 00:23:26,767
What's going on?

64
00:23:26,872 --> 00:23:30,205
We caught her working our
territory without paying us.

65
00:23:30,308 --> 00:23:31,605
Give her to me.

66
00:24:03,942 --> 00:24:07,173
If you want to do business,
you've got to pay your dues.

67
00:24:07,512 --> 00:24:08,774
Understand?

68
00:26:59,484 --> 00:27:00,917
You get out.

69
00:27:03,989 --> 00:27:05,616
Go on, get out!

70
00:27:11,730 --> 00:27:13,664
I'm having this baby.

71
00:27:14,399 --> 00:27:16,526
Call me a beast if you want.

72
00:27:17,936 --> 00:27:19,961
I'm going to have this baby!

73
00:28:28,873 --> 00:28:32,468
Just as I thought!
You've got a great body.

74
00:28:34,846 --> 00:28:38,714
You're so starved for sex...
your whole body's on fire!

75
00:28:42,687 --> 00:28:45,315
Come on, I'll show you a good time.

76
00:28:56,434 --> 00:28:58,698
Look at you. Full of defiance!

77
00:29:04,242 --> 00:29:05,402
But listen...

78
00:29:06,711 --> 00:29:08,542
...I know who you are.

79
00:29:08,713 --> 00:29:11,113
WANTED
For Murder, Escaped Convict

80
00:29:18,790 --> 00:29:22,749
You can either be my woman or get
yourself handed over to the cops.

81
00:29:23,428 --> 00:29:26,363
You think it over tonight, got that?

82
00:31:35,193 --> 00:31:36,990
Tonight's the night.

83
00:32:35,286 --> 00:32:39,154
Hello? Hello? Who the hell is this?

84
00:32:39,357 --> 00:32:43,259
Better watch it, or you�re
going to lose your man.

85
00:34:49,120 --> 00:34:53,580
"Better watch it, or you're
going to lose your man."

86
00:36:23,948 --> 00:36:29,818
Better watch it, or I'm just going to
find myself another man to love.

87
00:36:30,521 --> 00:36:35,549
Who knew that anything else
could be so sublime?

88
00:36:38,396 --> 00:36:44,357
Those nights I swayed in your arms
like a shimmering wave of heat.

89
00:36:44,936 --> 00:36:52,308
Once in a while I shed gentle tears...

90
00:36:52,577 --> 00:36:58,846
Am I always that kind of woman to you?

91
00:36:59,450 --> 00:37:06,947
I just want to catch fire
and drive you insane...

92
00:37:54,472 --> 00:37:57,771
Tanida died this morning at the hospital.

93
00:37:57,975 --> 00:38:01,172
The cops are calling it
an accidental death...

94
00:38:01,712 --> 00:38:06,775
But his woman says
you cooked up the whole plot.

95
00:38:13,124 --> 00:38:15,752
How are you going to make up for this?

96
00:38:16,027 --> 00:38:19,155
He was a valued employee
of mine, you know!

97
00:38:19,297 --> 00:38:22,232
Listen, that kind of talk isn't
going to get you anywhere.

98
00:38:22,333 --> 00:38:25,894
She's got to pay us damages with
her body, if she's got no money...

99
00:38:26,037 --> 00:38:28,198
Just come right out and say it...

100
00:38:32,209 --> 00:38:33,267
Scorpion!

101
00:38:48,759 --> 00:38:50,283
Look, Katsu...

102
00:38:50,428 --> 00:38:54,660
Honey, you can just work the hell
out of this girl until she drops dead.

103
00:38:54,799 --> 00:38:57,791
No way is she ever going
to squeal on us to the cops.

104
00:38:57,935 --> 00:39:02,463
How could she? She was in prison
for murder, and now she's on the run.

105
00:39:02,606 --> 00:39:06,098
Next time she gets arrested,
she's going to Death Row for sure!

106
00:39:07,178 --> 00:39:08,338
Damn you, Matsu!

107
00:39:08,779 --> 00:39:12,271
You really made my life
hell when we were inside.

108
00:39:13,751 --> 00:39:17,152
Now it's my turn to do a number on you!

109
00:41:34,091 --> 00:41:37,288
CLINIC

110
00:41:41,999 --> 00:41:43,728
Work hard, now!

111
00:41:43,901 --> 00:41:46,495
Aren't you going a little too far?

112
00:41:48,539 --> 00:41:50,564
Just shut up and come.

113
00:41:51,575 --> 00:41:53,634
Hey, you're coming with us!

114
00:42:31,815 --> 00:42:36,013
Well... you've gotten so big
before anyone noticed.

115
00:42:36,153 --> 00:42:38,451
So about how far along is she?

116
00:42:39,490 --> 00:42:43,051
I'd say... six months or so.

117
00:42:45,129 --> 00:42:47,222
It's a little too late, huh?

118
00:42:49,099 --> 00:42:52,591
She's setting a bad example
for the other girls...

119
00:42:53,103 --> 00:42:55,571
What are you going to do about this?

120
00:42:55,906 --> 00:42:58,033
Calm down, now, honey.

121
00:43:00,611 --> 00:43:05,981
Shinobu, you're so far along,
you'd better go see a doctor.

122
00:43:06,417 --> 00:43:09,215
I'll take you to the hospital now.

123
00:43:09,820 --> 00:43:12,948
That's a good girl.
No need to be afraid.

124
00:43:13,223 --> 00:43:16,750
You're going to a doctor
who'll take care of your baby.

125
00:43:16,860 --> 00:43:19,727
No! It's my baby!

126
00:43:21,198 --> 00:43:24,599
Leave me alone!
I'm going to have this baby!

127
00:43:24,768 --> 00:43:28,636
Leave me alone!
I'm not going to any doctor!

128
00:43:28,839 --> 00:43:31,865
No! It's my baby!

129
00:43:32,309 --> 00:43:33,537
No!

130
00:43:34,311 --> 00:43:37,974
No! No! Leave me alone!

131
00:43:38,315 --> 00:43:40,340
No!

132
00:43:57,901 --> 00:43:59,435
O.R.

133
00:43:59,470 --> 00:44:00,732
Next.
O.R.

134
00:44:14,952 --> 00:44:15,919
No!

135
00:44:16,153 --> 00:44:18,713
No! No!

136
00:44:23,761 --> 00:44:25,490
No!

137
00:44:27,631 --> 00:44:29,394
No!

138
00:44:31,235 --> 00:44:34,830
No! Stop it! Stop it!

139
00:44:35,305 --> 00:44:38,069
Stop it!

140
00:45:49,279 --> 00:45:51,270
My baby!

141
00:48:50,394 --> 00:48:52,624
Thank you for coming.

142
00:48:52,729 --> 00:48:55,027
I'm Lieutenant Yamazaki.

143
00:49:31,601 --> 00:49:34,832
That woman... she'll
be coming back for us!

144
00:49:37,374 --> 00:49:40,969
You idiots, she got away
because you were so inept!

145
00:49:44,982 --> 00:49:46,916
Easy, Katsu.

146
00:49:47,551 --> 00:49:49,883
One girl... big fucking deal!

147
00:49:50,454 --> 00:49:52,081
What the hell?

148
00:49:52,956 --> 00:49:54,287
Goddammit!

149
00:49:54,391 --> 00:49:55,358
Bastard!

150
00:50:13,610 --> 00:50:16,738
You know that the doctor was murdered.

151
00:50:22,119 --> 00:50:24,587
And you're next on her list.

152
00:50:30,861 --> 00:50:32,158
Hey, lady.

153
00:50:32,362 --> 00:50:36,025
I'm sure you haven't forgotten how
dangerous Nami Matsushima can be.

154
00:50:36,800 --> 00:50:39,598
Don't fool yourselves,
she's no ordinary woman.

155
00:50:42,172 --> 00:50:44,265
Prisoner 701.

156
00:50:45,709 --> 00:50:48,610
Do you know what they
all called her in prison?

157
00:50:52,616 --> 00:50:54,447
The Scorpion.

158
00:50:54,985 --> 00:50:56,282
Goddammit!

159
00:50:56,620 --> 00:50:58,850
Out of my way!

160
00:50:59,022 --> 00:51:01,286
Mister, why don't you stop by?

161
00:51:01,591 --> 00:51:03,058
Move it!

162
00:51:03,193 --> 00:51:04,854
Out of my way!

163
00:51:05,695 --> 00:51:07,526
Goddammit!

164
00:51:08,198 --> 00:51:11,565
- Serves them right.
- That woman must be something else.

165
00:51:11,701 --> 00:51:15,228
Thanks to her, we can do
whatever we want here.

166
00:51:16,673 --> 00:51:17,605
Mister?

167
00:51:19,976 --> 00:51:23,173
Want me to show you a good time?
I won't charge you much.

168
00:51:58,215 --> 00:52:02,845
Escaped Convict Wanted For Murder
Emergency Warrant For Fugitive

169
00:54:41,244 --> 00:54:43,974
She's turning herself in for
forcing women into prostitution.

170
00:54:44,080 --> 00:54:47,516
I guess rotten prison food seems
more appealing than getting killed.

171
00:55:01,531 --> 00:55:03,396
Do you think she'll come?

172
00:55:59,689 --> 00:56:02,590
What a vengeful bitch...

173
00:56:04,694 --> 00:56:09,256
I've been possessed by
the soul of the dead girl.

174
00:56:20,243 --> 00:56:22,609
All right! Surround her!

175
00:56:25,682 --> 00:56:26,580
Hey!

176
00:56:37,460 --> 00:56:38,484
Hey!

177
00:56:50,073 --> 00:56:51,233
There!

178
00:58:04,747 --> 00:58:07,841
Don't go too far in!
Block off the exits!

179
00:58:07,951 --> 00:58:08,849
Yes, sir.

180
01:02:34,350 --> 01:02:35,578
- Any problems?
- No, sir.

181
01:02:35,685 --> 01:02:37,152
- I'm counting on you.
- Yes, sir.

182
01:03:21,564 --> 01:03:22,588
Scorpion?

183
01:03:25,535 --> 01:03:26,593
Scorpion?

184
01:03:29,405 --> 01:03:30,702
Scorpion!

185
01:03:45,688 --> 01:03:47,280
Scorpion?

186
01:03:49,258 --> 01:03:50,486
Scorpion!

187
01:03:52,228 --> 01:03:54,526
Scorpion!

188
01:04:02,104 --> 01:04:04,402
Scorpion!

189
01:04:08,945 --> 01:04:11,106
Scorpion!

190
01:04:15,451 --> 01:04:17,544
Scorpion!

191
01:04:20,756 --> 01:04:23,054
Scorpion!

192
01:04:28,865 --> 01:04:31,959
Scorpion!

193
01:04:42,278 --> 01:04:45,441
Scorpion!

194
01:04:48,951 --> 01:04:51,613
Scorpion!

195
01:04:51,921 --> 01:04:54,014
Scorpion!

196
01:05:45,207 --> 01:05:49,109
Light 4 or 5 of these matches...
and stick them between your legs.

197
01:05:49,211 --> 01:05:54,171
That'll warm you to the bone. That's
what we do whenever it's cold outside.

198
01:06:11,367 --> 01:06:15,167
That's it. That's the way.
Doesn't that warm you up?

199
01:06:42,665 --> 01:06:44,599
The cops are coming, so...

200
01:06:46,102 --> 01:06:47,091
Yuki...

201
01:06:48,537 --> 01:06:51,005
What about your baby?

202
01:08:13,355 --> 01:08:17,587
It's been a week now. There's no
food down there in the sewers...

203
01:08:17,860 --> 01:08:21,318
And I doubt she could've
survived all that rain.

204
01:09:23,159 --> 01:09:24,854
Detective Gondo...

205
01:09:42,378 --> 01:09:48,374
I understand you've locked up your
retarded brother in the closet.

206
01:09:52,421 --> 01:09:59,418
I wonder what will happen to that moron
unless you get home to feed him?

207
01:10:07,336 --> 01:10:12,364
Why don't you let us know
when you feel like cooperating?

208
01:11:14,169 --> 01:11:15,602
Scorpion?

209
01:11:19,942 --> 01:11:21,409
Scorpion!

210
01:11:23,245 --> 01:11:25,236
Scorpion!

211
01:11:54,843 --> 01:11:55,969
Yuki...

212
01:12:54,203 --> 01:12:56,000
All right, come along.

213
01:12:57,473 --> 01:12:58,440
Yes, sir!

214
01:14:21,089 --> 01:14:23,057
All right, go get her!

215
01:15:10,739 --> 01:15:12,229
Big Brother...

216
01:15:18,614 --> 01:15:19,945
Big Brother!

217
01:15:58,320 --> 01:15:59,753
About face!

218
01:16:20,776 --> 01:16:22,209
Get in there!

219
01:16:23,178 --> 01:16:26,375
Get a move on! Hurry the hell up!

220
01:16:35,123 --> 01:16:38,149
Hey, everybody. Here's a newcomer.

221
01:16:38,293 --> 01:16:40,454
Be nice to her. Go on.

222
01:16:43,599 --> 01:16:46,727
Hey Guard, what's the new girl in for?

223
01:16:46,868 --> 01:16:50,929
Arson. She's in for three months.

224
01:16:53,809 --> 01:16:57,336
Three months? That's
just a spit in the ocean.

225
01:16:57,746 --> 01:16:59,680
Three months, eh?

226
01:18:42,684 --> 01:18:55,529
Go to sleep, go to sleep...
drift away to sleep...

227
01:18:55,864 --> 01:19:08,470
Such a good little boy...
go to sleep now.

228
01:19:09,077 --> 01:19:20,853
The little boy's nanny,
where has she gone?

229
01:19:21,590 --> 01:19:32,728
She climbed that mountain,
and went back to her home.

230
01:19:38,440 --> 01:19:40,601
What the hell's going on?

231
01:19:45,514 --> 01:19:48,483
You're out of your mind.
Get down from there!

232
01:19:48,884 --> 01:19:50,408
- Get her down.
- Yes, sir.

233
01:19:50,652 --> 01:19:53,348
- Hand that over!
- Get down!

234
01:19:54,523 --> 01:19:57,117
Give him back! That's my baby!

235
01:19:59,861 --> 01:20:03,297
- That's my baby! That's Akio!
- Come with us!

236
01:20:03,465 --> 01:20:06,263
Give him back to me!
Give him back to me!

237
01:20:06,501 --> 01:20:08,298
Akio! Akio! Akio!

238
01:20:08,770 --> 01:20:11,295
Give me back my baby! Akio!

239
01:20:11,439 --> 01:20:14,431
Akio! Akio! Give me back my Akio!

240
01:20:14,576 --> 01:20:18,444
Give me back my baby!
Akio! Give me back my baby!

241
01:20:18,947 --> 01:20:23,179
Give him back! That's my baby!
Give him back to me!

242
01:21:10,098 --> 01:21:12,191
Help me! Help me!

243
01:21:13,535 --> 01:21:17,335
Doctor! Doctor!
The patient... the patient's...

244
01:21:18,073 --> 01:21:19,802
It's the Scorpion!

245
01:21:20,642 --> 01:21:22,041
The Scorpion...

246
01:21:22,677 --> 01:21:25,737
The Scorpion...
The Scorpion's going to kill me!

247
01:21:26,314 --> 01:21:28,373
The Scorpion's going to kill me!

248
01:21:42,330 --> 01:21:45,527
I'm telling you, her terror is abnormal.

249
01:21:45,667 --> 01:21:48,966
She keeps saying that
the Scorpion's going to kill her.

250
01:21:49,070 --> 01:21:54,804
She's imagining things. The woman
Katsu Samejima's terrified of is dead.

251
01:21:57,045 --> 01:22:00,537
Has anything out of the
ordinary happened to her?

252
01:22:00,649 --> 01:22:02,981
No, nothing out of the ordinary

253
01:22:03,084 --> 01:22:06,884
A new prisoner joined her ward,
but that's about it.

254
01:22:07,555 --> 01:22:09,523
- A newcomer?
- Yes.

255
01:22:12,627 --> 01:22:14,788
- The key to Cell 24.
- Yes, sir.

256
01:22:19,334 --> 01:22:20,699
After you.

257
01:22:22,103 --> 01:22:26,597
May I see her alone? There's something
I'd like to discuss in depth with her.

258
01:22:26,708 --> 01:22:30,075
I see. Then please, be my guest.

259
01:24:50,919 --> 01:24:54,047
The... alarm...

260
01:24:57,092 --> 01:24:59,890
The... alarm...

261
01:25:26,621 --> 01:25:28,816
I killed the Scorpion!

262
01:26:16,204 --> 01:26:22,404
10 days later, the Scorpion served
out her term, and was released.

263
01:26:23,178 --> 01:26:28,878
Nobody ever knew what became of her.

264
01:26:30,585 --> 01:26:35,545
I cannot die before I fulfill my fate.

265
01:26:37,091 --> 01:26:41,994
So I live on, driven only by my hate.

266
01:26:42,130 --> 01:26:47,124
A woman, a woman,
a woman's life is her song...

267
01:26:47,335 --> 01:26:52,932
Her song of vengeance.

268
01:26:59,380 --> 01:27:03,817
SCORPION - BEAST STABLE
THE END

