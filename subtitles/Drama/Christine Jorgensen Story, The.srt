1
00:00:10,474 --> 00:00:12,178
Richard, what are you doing?

2
00:00:12,346 --> 00:00:14,013
Shower.
I didn't know you were here.

3
00:00:14,181 --> 00:00:15,892
Well, I am.
Maybe you could knock.

4
00:00:16,336 --> 00:00:18,850
And do a little man-scaping,
for God's sakes.

5
00:00:20,072 --> 00:00:22,423
Sorry, but now that I'm not living
with new Christine anymore,

6
00:00:22,548 --> 00:00:24,404
there's no reason
to clear the brush.

7
00:00:25,822 --> 00:00:27,900
You know what?
I think it's time to move out.

8
00:00:28,445 --> 00:00:30,242
Maybe, but...
where would you go?

9
00:00:31,792 --> 00:00:32,749
You, Richard.

10
00:00:32,975 --> 00:00:35,034
You do remember
that you do have a home?

11
00:00:35,202 --> 00:00:36,436
My bachelor apartment?

12
00:00:36,837 --> 00:00:38,893
I got rid of that
our fifth year of marriage.

13
00:00:42,602 --> 00:00:44,943
I'm talking about the house
you bought with new Christine.

14
00:00:45,068 --> 00:00:46,423
New Christine banished me.

15
00:00:47,126 --> 00:00:48,451
She can't banish you.

16
00:00:48,576 --> 00:00:50,659
It's not like me
and the Catholic Church.

17
00:00:51,701 --> 00:00:54,679
I don't know. Sometimes the German
in new Christine takes over.

18
00:00:54,860 --> 00:00:57,056
If you ever see her in braids,
run for your life.

19
00:00:57,342 --> 00:00:59,100
Just do what we did
when we got divorced.

20
00:00:59,268 --> 00:01:01,102
- Sleep with an Asian woman?
- What?

21
00:01:06,012 --> 00:01:07,945
I'm saying
you should sell the house,

22
00:01:08,070 --> 00:01:09,768
and then you divide the money.

23
00:01:09,893 --> 00:01:11,800
Are you crazy?
In this market?

24
00:01:12,491 --> 00:01:14,109
What happened in the housing market?

25
00:01:14,234 --> 00:01:16,466
You miss one day
of reading the paper...

26
00:01:17,469 --> 00:01:19,571
I don't know.
She loves that house.

27
00:01:23,348 --> 00:01:25,910
You made the down payment.
She left you at the altar.

28
00:01:26,035 --> 00:01:28,283
She's young and beautiful,
and you look like this.

29
00:01:28,408 --> 00:01:30,066
You should make her pay.

30
00:01:31,502 --> 00:01:33,371
I sort of appreciate your honesty.

31
00:01:33,893 --> 00:01:37,555
- And thanks for letting me stay here.
- No. Don't do that. We're naked.

32
00:01:37,767 --> 00:01:38,948
I completely forgot.

33
00:01:39,588 --> 00:01:42,977
We're so used to each other, I can
stand here and not have sexual thought.

34
00:01:43,609 --> 00:01:45,228
I sort of appreciate your honesty.

35
00:01:45,978 --> 00:01:47,381
I'm just saying it's nice,

36
00:01:47,506 --> 00:01:48,335
normal.

37
00:01:48,460 --> 00:01:50,873
Plus, it's not like I'm seeing
something I've never seen before.

38
00:01:50,998 --> 00:01:52,153
What is that?

39
00:01:52,925 --> 00:01:54,570
It's called aging, Richard.

40
00:01:56,194 --> 00:01:58,117
And you should talk with those yams.

41
00:01:59,789 --> 00:02:01,579
No. There's something
under your boob.

42
00:02:01,993 --> 00:02:03,823
Are you sure
it's just not more boob?

43
00:02:04,742 --> 00:02:07,960
No, it's like a weird freckle or mole.
It doesn't look right.

44
00:02:10,978 --> 00:02:12,277
I'm dying.

45
00:02:13,258 --> 00:02:14,458
It's not fair.

46
00:02:16,603 --> 00:02:17,637
What the hell?

47
00:02:18,142 --> 00:02:19,513
Now you're excited?

48
00:02:22,143 --> 00:02:23,726
Me dying gets you going?

49
00:02:23,894 --> 00:02:26,004
Don't yell at me.
You'll only make it worse.

50
00:02:33,191 --> 00:02:35,029
Synch : So.

51
00:02:40,480 --> 00:02:42,439
I am having such a bad day.

52
00:02:42,742 --> 00:02:43,592
Not me.

53
00:02:43,837 --> 00:02:47,191
I got to open the gym by myself,
work all day by myself.

54
00:02:47,316 --> 00:02:49,560
I haven't been
to the bathroom in ten hours.

55
00:02:50,393 --> 00:02:51,843
I'm having a blast.

56
00:02:53,672 --> 00:02:55,673
I'm sorry, Barb.
How was your day?

57
00:02:56,343 --> 00:02:57,510
I went to the doctor,

58
00:02:57,919 --> 00:02:58,919
and he...

59
00:02:59,697 --> 00:03:01,222
would not see me.
Get this.

60
00:03:01,347 --> 00:03:05,059
We are apparently no longer covered
by Bloom's health insurance.

61
00:03:05,335 --> 00:03:07,685
Probably because
we're no longer owned by Bloom.

62
00:03:08,689 --> 00:03:10,776
What are we supposed to do?
People can't go around

63
00:03:10,901 --> 00:03:13,025
without health insurance.
This is America.

64
00:03:14,430 --> 00:03:17,238
And 45 million people
don't have health insurance.

65
00:03:20,486 --> 00:03:22,868
Man, you miss reading
the paper for two days.

66
00:03:24,053 --> 00:03:25,871
Why isn't anyone talking about this?

67
00:03:26,039 --> 00:03:29,392
Maybe because you get all your news
from Nicole Ritchie's blog.

68
00:03:30,758 --> 00:03:33,320
She's interesting
because she's famous, Barb.

69
00:03:36,701 --> 00:03:38,757
What is going on in this country?

70
00:03:38,882 --> 00:03:42,040
I mean, apparently, something
is happening in the housing market,

71
00:03:42,165 --> 00:03:44,542
doctors won't see you
without insurance,

72
00:03:44,667 --> 00:03:47,258
and I've got a freckle,
and I'm dying.

73
00:03:48,310 --> 00:03:51,438
Every month you think you're dying
or pregnant or going blind.

74
00:03:51,607 --> 00:03:52,895
No, this is different.

75
00:03:53,020 --> 00:03:54,676
It's right under my left breast.

76
00:03:55,553 --> 00:03:56,630
Which is tender.

77
00:03:57,455 --> 00:03:59,154
Oh, my God, I'm pregnant.

78
00:04:01,258 --> 00:04:02,688
And I'm dying.

79
00:04:03,931 --> 00:04:05,381
I'm not doing this.

80
00:04:06,312 --> 00:04:08,747
What's gonna happen to Ritchie
if I die? Who's gonna clean him?

81
00:04:08,916 --> 00:04:10,916
Who's gonna feed him?
Help him with his homework?

82
00:04:11,649 --> 00:04:12,978
You don't do that now.

83
00:04:14,519 --> 00:04:17,402
Maybe we shouldn't have left Bloom.
They took care of all the little things,

84
00:04:17,527 --> 00:04:20,217
like insurance
and maintenance and utilities.

85
00:04:20,386 --> 00:04:22,105
We shouldn't second-guess ourselves.

86
00:04:22,230 --> 00:04:25,620
Really, we are better off without
that homophobic, corporate monster.

87
00:04:27,984 --> 00:04:28,984
I'm blind.

88
00:04:30,896 --> 00:04:32,246
And I'm pregnant.

89
00:04:32,869 --> 00:04:34,256
And I'm dying.

90
00:04:39,439 --> 00:04:41,989
- Richard, what are you doing here?
- I need to talk to you.

91
00:04:42,884 --> 00:04:43,690
Come in.

92
00:04:47,830 --> 00:04:49,869
Just so you know,
people know I'm here.

93
00:04:50,917 --> 00:04:53,187
I'm remodeling a bathroom,
and if I don't show up in a year,

94
00:04:53,312 --> 00:04:54,902
people will start to get suspicious.

95
00:04:56,519 --> 00:04:59,323
Don't be silly. There's no reason
we can't act like adults.

96
00:05:00,912 --> 00:05:03,260
- You made some changes to the place.
- Do you like it?

97
00:05:03,709 --> 00:05:06,492
What's not to like?
A bunch of creepy dolls on my couch.

98
00:05:08,656 --> 00:05:11,093
I didn't want people thinking
there was a man living here.

99
00:05:11,339 --> 00:05:13,192
I don't think anyone
has ever thought that.

100
00:05:15,078 --> 00:05:16,452
So what brings you by?

101
00:05:17,853 --> 00:05:20,778
I thought it was time to figure out
what to do about the house.

102
00:05:21,126 --> 00:05:24,426
- I'm living in the house.
- No, I know, but... long term.

103
00:05:24,659 --> 00:05:26,358
Long term, I'm living in the house.

104
00:05:27,901 --> 00:05:29,495
What about selling the house?

105
00:05:29,664 --> 00:05:32,617
We'd lose money. Right now it's only
worth half of what we paid for it.

106
00:05:33,074 --> 00:05:34,458
Maybe I could buy you out.

107
00:05:34,732 --> 00:05:35,898
Two million dollars.

108
00:05:37,759 --> 00:05:38,906
That's not fair.

109
00:05:39,280 --> 00:05:42,139
This wouldn't be a 4 million dollar
house if you dipped it in gold,

110
00:05:42,264 --> 00:05:45,256
put it in the middle of Manhattan
and filled it with 3 million dollars.

111
00:05:46,759 --> 00:05:49,444
If life were fair, you would have paid
attention to me on our wedding day

112
00:05:49,569 --> 00:05:50,807
instead of old Christine.

113
00:05:51,255 --> 00:05:52,178
You're right.

114
00:05:52,645 --> 00:05:54,728
That was wrong, and I am so sorry.

115
00:05:55,703 --> 00:05:58,658
Although you shouldn't speak
too harshly about old Christine.

116
00:05:59,293 --> 00:06:00,526
She might have cancer.

117
00:06:01,393 --> 00:06:02,903
Oh, my God. What kind?

118
00:06:03,562 --> 00:06:04,493
Under boob.

119
00:06:07,380 --> 00:06:09,827
Under boob cancer?
Is that what her doctor called it?

120
00:06:09,996 --> 00:06:12,085
Well, she hasn't
actually seen a doctor yet,

121
00:06:12,210 --> 00:06:14,206
but I found this weird mole
on her in the shower.

122
00:06:14,478 --> 00:06:15,291
Get out.

123
00:06:18,000 --> 00:06:20,830
- We haven't decided on a fair price.
- Five million dollars.

124
00:06:22,042 --> 00:06:23,799
The shower was a total accident,

125
00:06:23,967 --> 00:06:26,024
and seeing old Christine naked
means nothing.

126
00:06:26,296 --> 00:06:29,054
I didn't even get excited
until I found out she might be dying.

127
00:06:31,389 --> 00:06:32,757
I want you out of here.

128
00:06:33,541 --> 00:06:34,733
I'm not going anywhere.

129
00:06:35,128 --> 00:06:38,354
This house is at least half mine.
You're being unreasonable.

130
00:06:39,225 --> 00:06:40,541
I'm being unreasonable?

131
00:06:40,859 --> 00:06:43,666
You just used your ex-wife's illness
to try to get your way.

132
00:06:43,865 --> 00:06:45,562
You made me think she was dying...

133
00:06:45,897 --> 00:06:46,850
of cancer.

134
00:06:47,499 --> 00:06:48,615
Dying, Richard.

135
00:06:50,936 --> 00:06:53,468
I'm going to have to ask you
to stop talking about this now.

136
00:06:58,795 --> 00:07:01,396
Look at this place.
It's like an airport bar.

137
00:07:02,088 --> 00:07:03,437
Why is it so crowded?

138
00:07:03,715 --> 00:07:06,633
Because the hospital can't turn anyone
away, even if they don't have insurance.

139
00:07:07,113 --> 00:07:09,213
None of these people have insurance?

140
00:07:10,121 --> 00:07:11,609
When did this become a thing?

141
00:07:12,667 --> 00:07:13,963
I'm gonna write a letter.

142
00:07:14,836 --> 00:07:16,268
- To who?
- The newspaper.

143
00:07:16,696 --> 00:07:18,339
What's the name of the newspaper?

144
00:07:19,766 --> 00:07:21,466
America's top newspaper.

145
00:07:23,053 --> 00:07:25,906
God, I can't wait here all day.
I've got to see a doctor.

146
00:07:26,191 --> 00:07:27,780
Good luck.
I've been here 3 hours.

147
00:07:28,846 --> 00:07:30,251
Look at your baby.

148
00:07:30,376 --> 00:07:31,702
He's so cute.

149
00:07:32,802 --> 00:07:33,840
How old is he?

150
00:07:34,086 --> 00:07:35,086
Two hours.

151
00:07:40,880 --> 00:07:42,461
I'm gonna die here.

152
00:07:42,629 --> 00:07:43,754
You are not dying.

153
00:07:43,922 --> 00:07:45,978
I wish people
wouldn't keep saying that.

154
00:07:46,103 --> 00:07:49,467
It cheapens what I'm going through.
And I know you're scared,

155
00:07:49,592 --> 00:07:52,383
but even in death,
I will be with you.

156
00:07:58,148 --> 00:07:59,641
That's where my water broke.

157
00:08:01,898 --> 00:08:04,736
Why is it every time I hang out
with you, I sit in something wet?

158
00:08:06,571 --> 00:08:08,196
A doctor.
I'm not waiting.

159
00:08:08,321 --> 00:08:09,421
Excuse me...

160
00:08:09,856 --> 00:08:11,447
What do you think of this?

161
00:08:12,340 --> 00:08:13,417
I've seen worse.

162
00:08:14,730 --> 00:08:15,786
Thank you, doctor.

163
00:08:17,041 --> 00:08:18,815
I'm not a doctor.
I'm a painter.

164
00:08:21,945 --> 00:08:23,245
Well... still...

165
00:08:23,641 --> 00:08:24,544
Thank you.

166
00:08:26,711 --> 00:08:28,799
God...
Can you believe that guy?

167
00:08:30,076 --> 00:08:30,930
That guy.

168
00:08:32,176 --> 00:08:34,407
I cannot stand here
in this line all day.

169
00:08:34,532 --> 00:08:36,406
I don't know how much time I've got.

170
00:08:39,042 --> 00:08:40,798
Excuse me.
Are you a real nurse?

171
00:08:41,695 --> 00:08:43,383
I need you to feel something...

172
00:08:43,674 --> 00:08:45,982
right there.
Does that seem all right to you?

173
00:08:46,325 --> 00:08:47,984
Nothing about this seems all right.

174
00:08:50,208 --> 00:08:53,201
Llisten, I have a very scary
looking freckle right here,

175
00:08:53,451 --> 00:08:55,246
and I need a doctor to look at it.

176
00:08:55,622 --> 00:08:56,491
Stat.

177
00:08:57,288 --> 00:08:59,917
- Fine. Let me see your insurance card.
- No, I don't have insurance.

178
00:09:00,042 --> 00:09:03,576
But I do happen to know the law
and I know you're obligated to treat me.

179
00:09:03,961 --> 00:09:07,249
You're right, that's the law. We are
obligated to provide a minimum of care.

180
00:09:08,199 --> 00:09:10,249
Yes, you are.
Minimum of care.

181
00:09:17,543 --> 00:09:19,501
Not cool, guys!

182
00:09:32,309 --> 00:09:34,406
They dumped me.
Can you believe it?

183
00:09:34,841 --> 00:09:38,068
I can't believe how many of
our conversations start out like this.

184
00:09:39,595 --> 00:09:40,516
Apparently,

185
00:09:40,641 --> 00:09:43,359
the hospital's definition
of "minimum of care"

186
00:09:43,484 --> 00:09:46,291
is driving me downtown
and leaving me there.

187
00:09:47,195 --> 00:09:49,529
I was the victim
of a death panel, Barb.

188
00:09:51,169 --> 00:09:53,160
You've got to stop
watching Stephen Colbert

189
00:09:53,285 --> 00:09:54,786
when you're half in the bag.

190
00:09:56,158 --> 00:09:59,073
A bum saw my butt,
and danced a jig while I cried.

191
00:10:00,600 --> 00:10:03,800
I can't believe how many of
our conversations end up like this.

192
00:10:04,813 --> 00:10:07,525
I'll tell you what I'm gonna do
about this health care crisis.

193
00:10:07,731 --> 00:10:10,395
I'm gonna make a documentary.
I'm gonna expose them all.

194
00:10:11,311 --> 00:10:13,670
- Who's "them all"?
- See the movie, OK?

195
00:10:14,279 --> 00:10:16,776
But everybody is going down.
I'm naming names.

196
00:10:17,159 --> 00:10:18,287
Name one name.

197
00:10:20,162 --> 00:10:21,000
Tom.

198
00:10:24,764 --> 00:10:27,000
There you are. What happened to you?
I've been so worried.

199
00:10:27,489 --> 00:10:30,036
So worried that you stopped
for a box of fried chicken?

200
00:10:31,852 --> 00:10:33,084
Hungry and worried.

201
00:10:34,915 --> 00:10:37,253
- Did you get biscuits?
- What am I, stupid?

202
00:10:38,594 --> 00:10:39,573
Excuse me.

203
00:10:39,765 --> 00:10:41,227
I was in the hospital.

204
00:10:41,352 --> 00:10:44,499
I laid on a stretcher
in a hallway for two hours

205
00:10:44,624 --> 00:10:47,302
and then I fell asleep
and I woke up in the morgue.

206
00:10:49,545 --> 00:10:50,357
Did you?

207
00:10:53,433 --> 00:10:55,056
But I saw that painter again.

208
00:10:55,181 --> 00:10:57,028
And he pretended he didn't know me.

209
00:10:58,534 --> 00:11:01,000
And I never got a doctor
to see me. And...

210
00:11:02,111 --> 00:11:04,163
I'm having a hard time.

211
00:11:06,500 --> 00:11:09,835
Settle down. I called a friend of mine
from medical school, Glenn.

212
00:11:10,973 --> 00:11:13,252
I do not have time
to date your friends.

213
00:11:13,377 --> 00:11:14,533
I am dying.

214
00:11:16,125 --> 00:11:18,085
I mean, all right,
I'll meet him for coffee.

215
00:11:19,522 --> 00:11:22,057
No, he's doing
a dermatology rotation.

216
00:11:22,448 --> 00:11:23,681
He said he'd see you.

217
00:11:24,468 --> 00:11:26,423
- Is he good?
- She means handsome.

218
00:11:27,918 --> 00:11:29,833
He's a guy.
I can't tell if he's handsome.

219
00:11:30,208 --> 00:11:32,003
OK, he's gorgeous.
Emerald eyes.

220
00:11:33,959 --> 00:11:36,809
But he's also a good doctor and he said
he'd take a look at you for free.

221
00:11:38,118 --> 00:11:39,477
Thank you, Matthew.

222
00:11:39,602 --> 00:11:41,199
You're saving my life.

223
00:11:45,790 --> 00:11:47,448
And there's the biscuits.

224
00:11:56,903 --> 00:11:58,065
You're still here?

225
00:11:59,313 --> 00:12:01,970
And this is where I'm staying.
Because this is my house.

226
00:12:02,320 --> 00:12:04,891
I made the down payment,
I laid the hardwood floors,

227
00:12:05,016 --> 00:12:07,284
I built the garage apartment
that's not up to code.

228
00:12:09,201 --> 00:12:11,188
This isn't funny anymore.
I want you to leave.

229
00:12:11,594 --> 00:12:14,357
If you want me out of here, you're going
to have to physically remove me.

230
00:12:14,654 --> 00:12:16,356
Fine.
You know I'm strong.

231
00:12:16,836 --> 00:12:18,575
My mother
was in the German Olympics.

232
00:12:20,734 --> 00:12:21,627
Stop it!

233
00:12:23,645 --> 00:12:25,295
Stiffen up, stiffen up.

234
00:12:37,873 --> 00:12:39,224
I know you're faking.

235
00:12:52,031 --> 00:12:53,020
Are you faking?

236
00:12:55,869 --> 00:12:57,065
I know you're faking.

237
00:13:05,710 --> 00:13:06,804
Fine, I'm faking.

238
00:13:08,765 --> 00:13:10,629
And guess what,
it's not the first time.

239
00:13:14,252 --> 00:13:15,502
Please go away.

240
00:13:15,846 --> 00:13:19,046
Never. I'm here to stay, and
there's nothing you can do about it.

241
00:13:21,820 --> 00:13:24,556
I didn't want to do this.
I was hoping it could be amicable.

242
00:13:24,921 --> 00:13:27,973
But if we're going to be living here,
I better introduce you to our roommate.

243
00:13:30,483 --> 00:13:31,816
Please be a hot girl.

244
00:13:37,470 --> 00:13:38,817
What's that?
Is that a cat?

245
00:13:39,180 --> 00:13:40,361
Is that a real cat?

246
00:13:41,871 --> 00:13:44,281
Get it out of here!
You know I'm allergic.

247
00:13:52,859 --> 00:13:54,021
It's a German cat?

248
00:14:00,563 --> 00:14:01,845
What did you want, mom?

249
00:14:03,611 --> 00:14:05,611
My firstborn, my little man...

250
00:14:07,092 --> 00:14:08,368
The love of my life.

251
00:14:12,042 --> 00:14:14,441
Everything is okay. I just want
to talk to you, sweetheart.

252
00:14:16,645 --> 00:14:18,683
Well, you're getting older, and...

253
00:14:19,045 --> 00:14:20,780
I may not always be here.

254
00:14:21,028 --> 00:14:22,541
So I wanted to...

255
00:14:23,983 --> 00:14:26,613
pass on to you some of the things
that I've learned.

256
00:14:29,613 --> 00:14:31,113
It doesn't matter...

257
00:14:31,801 --> 00:14:34,701
who you vote for on American Idol.
It's rigged.

258
00:14:38,080 --> 00:14:41,197
Make sure you always have
health insurance.

259
00:14:42,235 --> 00:14:44,993
And if you're lucky enough
to find somebody that you love,

260
00:14:45,118 --> 00:14:47,703
you make sure
that she has health insurance, too.

261
00:14:48,409 --> 00:14:50,459
Or he...
You know, that's fine.

262
00:14:50,665 --> 00:14:52,711
Make sure
that he has health insurance.

263
00:14:52,836 --> 00:14:54,782
Although, you know...
if he is a he,

264
00:14:54,907 --> 00:14:57,074
he probably already
does have health insurance,

265
00:14:57,199 --> 00:14:59,276
because you people
are all so organized.

266
00:15:01,239 --> 00:15:02,234
Can I go?

267
00:15:12,206 --> 00:15:13,495
Matthew, is that you?

268
00:15:16,893 --> 00:15:19,003
I was just going through
my bucket list,

269
00:15:19,128 --> 00:15:21,461
and I passed my wisdom on to Ritchie

270
00:15:21,629 --> 00:15:23,673
and now all I have to do
is lose 5 pounds

271
00:15:23,798 --> 00:15:25,319
and sleep with an Asian man.

272
00:15:27,007 --> 00:15:30,268
Relax. My friend's going to be
over here in a minute to examine you.

273
00:15:30,639 --> 00:15:33,159
He's not Asian, is he?
I could kill two birds with one stone.

274
00:15:34,667 --> 00:15:37,069
No, he is not Asian.
He is a gorgeous Caucasian man

275
00:15:37,194 --> 00:15:38,754
with the most beautiful skin.

276
00:15:39,657 --> 00:15:41,204
What is going on with me?

277
00:15:44,603 --> 00:15:47,060
Glenn's here.
Please, do not embarrass me.

278
00:15:52,557 --> 00:15:54,910
Glenn, this is my sister.
And Christine, this is Glenn.

279
00:15:57,009 --> 00:16:00,050
When Matthew said you were Caucasian,
he wasn't kidding.

280
00:16:06,849 --> 00:16:08,148
Had enough, Richard?

281
00:16:09,427 --> 00:16:10,917
I'm perfectly comfortable.

282
00:16:13,499 --> 00:16:16,558
If you get out of here right now
I won't rub this cat on your face.

283
00:16:17,096 --> 00:16:18,183
That could kill me.

284
00:16:18,478 --> 00:16:19,746
That doesn't bother me.

285
00:16:23,813 --> 00:16:25,800
Drop the cat
or I rip the doll's head off!

286
00:16:27,045 --> 00:16:28,995
That's right, bye-bye, dolly.

287
00:16:30,638 --> 00:16:33,158
Come on, Richard,
you don't have the stomach for that.

288
00:16:33,748 --> 00:16:34,784
Watch this.

289
00:16:34,952 --> 00:16:37,245
It moved!
I swear that doll moved!

290
00:16:39,291 --> 00:16:40,148
Forget it.

291
00:16:40,611 --> 00:16:41,637
Take the house.

292
00:16:42,159 --> 00:16:43,376
I'll live in my truck.

293
00:16:44,052 --> 00:16:46,437
The air conditioning isn't working
and the radio is stuck

294
00:16:46,562 --> 00:16:48,635
on a Christian rock station,
but I'll be fine.

295
00:16:50,137 --> 00:16:51,652
I don't wanna do this either.

296
00:16:52,816 --> 00:16:54,554
Before I hated you, I loved you.

297
00:16:55,044 --> 00:16:56,014
I love you, too.

298
00:16:56,931 --> 00:16:58,183
"Loved", Richard.

299
00:16:59,466 --> 00:17:00,716
I didn't hear the "d".

300
00:17:03,853 --> 00:17:05,698
There has to be something
we can figure out.

301
00:17:05,823 --> 00:17:08,383
This is a big place.
There's room enough for both of us.

302
00:17:09,172 --> 00:17:11,905
Do you think you could handle living
here without a romantic relationship?

303
00:17:12,278 --> 00:17:13,281
Without cats?

304
00:17:13,548 --> 00:17:14,653
Forget the cat.

305
00:17:15,480 --> 00:17:17,661
It's not even mine.
It's the neighbor's.

306
00:17:18,185 --> 00:17:19,621
You steal cats?

307
00:17:22,041 --> 00:17:23,814
A lot has changed since you left.

308
00:17:26,674 --> 00:17:29,820
Do you think you can handle living here
without a romantic relationship?

309
00:17:32,067 --> 00:17:33,247
I don't see why not.

310
00:17:33,836 --> 00:17:36,513
We can be friends and live in the same
house. I did it with old Christine.

311
00:17:36,682 --> 00:17:39,312
- We're talking about Christine again.
- No, never. I hate her.

312
00:17:41,023 --> 00:17:42,944
Don't say that.
She's dying, remember?

313
00:17:46,476 --> 00:17:47,982
Yeah, I remember.

314
00:17:54,252 --> 00:17:56,533
So Matthew tells me
you're a gym owner.

315
00:17:56,702 --> 00:17:58,038
Yeah, gym owner...

316
00:17:59,078 --> 00:18:00,613
Documentary filmmaker.

317
00:18:01,490 --> 00:18:03,957
I'm about to blow the lid
off the health care industry.

318
00:18:05,212 --> 00:18:06,712
But don't worry, you're safe.

319
00:18:06,948 --> 00:18:09,398
I'm only going after
the ugly doctors.

320
00:18:10,758 --> 00:18:13,758
Well, to tell you the truth,
only the ugly doctors are any good.

321
00:18:18,489 --> 00:18:21,194
So it doesn't really look like
anything I've seen before,

322
00:18:21,319 --> 00:18:23,842
but I also don't think you have
anything to be concerned about.

323
00:18:24,475 --> 00:18:25,836
Gosh, that's a relief.

324
00:18:26,147 --> 00:18:29,441
I'm just going to scrape a few cells
and send them to the lab to be sure.

325
00:18:30,012 --> 00:18:31,162
God, cells...

326
00:18:31,745 --> 00:18:32,637
Labs...

327
00:18:33,211 --> 00:18:34,155
Scrape...

328
00:18:35,573 --> 00:18:38,616
It's just a precaution. Trust me,
everything else looks great.

329
00:18:39,092 --> 00:18:41,079
In fact, everything else looks...

330
00:18:41,603 --> 00:18:42,724
really great.

331
00:18:44,032 --> 00:18:46,552
Are you speaking
as a doctor or has a...

332
00:18:47,252 --> 00:18:49,566
handsome man
with wonderfully warm hands?

333
00:18:50,339 --> 00:18:53,047
Well, I'm not a doctor yet,
and those are your hands.

334
00:18:56,058 --> 00:18:59,958
Actually, you know, I was a little
freaked out by this whole thing.

335
00:19:00,153 --> 00:19:01,553
But I feel like...

336
00:19:02,064 --> 00:19:04,556
you've given me my life back.
And...

337
00:19:05,054 --> 00:19:06,728
I'm ready to start living.

338
00:19:11,159 --> 00:19:13,358
I'm sorry.
That was totally out of line.

339
00:19:13,527 --> 00:19:14,648
Don't be silly.

340
00:19:15,758 --> 00:19:17,760
This is the reason
I'm becoming a doctor.

341
00:19:25,953 --> 00:19:26,915
Good morning...

342
00:19:27,471 --> 00:19:28,526
pretty partner.

343
00:19:30,194 --> 00:19:31,446
What's up with you?

344
00:19:31,571 --> 00:19:32,614
Are you drunk?

345
00:19:35,937 --> 00:19:37,037
Not anymore.

346
00:19:38,397 --> 00:19:39,510
You seem happy.

347
00:19:40,204 --> 00:19:41,346
Are you pregnant?

348
00:19:44,164 --> 00:19:45,009
Maybe.

349
00:19:47,164 --> 00:19:48,681
But I don't have cancer.

350
00:19:49,510 --> 00:19:52,330
My mole turned out to be
a piece of brownie.

351
00:19:56,399 --> 00:19:59,773
It popped right off when I was
making out with my new doctor boyfriend.

352
00:20:00,718 --> 00:20:01,615
A brownie?

353
00:20:02,122 --> 00:20:03,701
And a doctor boyfriend.

354
00:20:05,812 --> 00:20:07,592
But you know what, though,
seriously,

355
00:20:07,717 --> 00:20:10,302
I'm glad that I went through this,
because...

356
00:20:10,427 --> 00:20:12,459
I feel like I know
what's important now.

357
00:20:12,629 --> 00:20:15,130
I mean, life can't all be work,
work, work.

358
00:20:15,536 --> 00:20:17,403
I'd settle for just one "work".

359
00:20:18,700 --> 00:20:20,789
So you're giving up
on your health care cause?

360
00:20:21,109 --> 00:20:23,721
Let me tell you something
about his so-called health care crisis.

361
00:20:23,889 --> 00:20:26,759
If every American
were willing to go to third base

362
00:20:26,884 --> 00:20:29,581
with every premed student,
there'd be no crisis.

363
00:20:32,385 --> 00:20:34,774
Now I actually want to see
your documentary.

364
00:20:38,210 --> 00:20:39,434
I'm blind.

365
00:20:41,122 --> 00:20:42,532
And I'm pregnant.

366
00:20:43,900 --> 00:20:45,832
With a doctor boyfriend!

