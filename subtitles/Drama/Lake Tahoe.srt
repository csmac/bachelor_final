1
00:04:53,000 --> 00:04:55,100
Hi, Joaqu�n.

2
00:04:55,135 --> 00:04:57,200
Put Mom on.

3
00:04:59,400 --> 00:05:01,100
Where is she?

4
00:05:02,800 --> 00:05:04,900
I know, but where is she?

5
00:05:07,800 --> 00:05:11,800
No, don't bother.
It's all right. I'll call her later.

6
00:05:13,900 --> 00:05:15,400
No, don't wait for me.

7
00:05:16,800 --> 00:05:19,300
I don't know, Joaqu�n.
Make yourself a sandwich.

8
00:05:20,300 --> 00:05:22,200
I don't know.
Bread, ham, cheese...

9
00:05:23,200 --> 00:05:25,000
Bye, Joaqu�n.

10
00:05:26,000 --> 00:05:27,000
Bye.

11
00:05:47,400 --> 00:05:50,500
"LA RAYA" AUTO REPAIR

12
00:06:11,800 --> 00:06:15,800
"RENACIMIENTO" AUTO REPAIR

13
00:06:22,800 --> 00:06:25,000
Good morning.

14
00:06:25,100 --> 00:06:27,200
Excuse me.
Can you help me?

15
00:06:27,300 --> 00:06:28,800
We're closed.

16
00:06:28,900 --> 00:06:31,100
I crashed near here.

17
00:06:31,200 --> 00:06:32,700
We're closed.

18
00:06:34,000 --> 00:06:36,500
- Okay. Thanks.
- No problem.

19
00:06:46,900 --> 00:06:49,800
"DON HEBER'S" AUTO REPAIR

20
00:07:01,600 --> 00:07:03,400
Morning!

21
00:07:15,100 --> 00:07:16,300
Good morning!

22
00:07:36,500 --> 00:07:38,000
I didn't come to steal.

23
00:07:39,400 --> 00:07:42,300
You can tell that to the cops.

24
00:07:43,600 --> 00:07:45,500
Where's the phone?

25
00:07:58,700 --> 00:08:01,100
Here's the cable...

26
00:08:25,000 --> 00:08:29,200
Now where's the...?

27
00:08:39,300 --> 00:08:41,000
...phone book?

28
00:08:47,300 --> 00:08:48,700
Here we go.

29
00:08:50,600 --> 00:08:52,900
Police, police...

30
00:08:53,000 --> 00:08:54,600
I didn't come to steal.

31
00:08:54,700 --> 00:08:56,500
I heard you the first time.

32
00:08:58,500 --> 00:09:00,200
Police.

33
00:09:03,000 --> 00:09:04,500
20, 20...

34
00:09:27,500 --> 00:09:29,100
I crashed.

35
00:09:39,500 --> 00:09:41,000
Where?

36
00:09:42,600 --> 00:09:44,400
Near here, in the outskirts.

37
00:09:52,500 --> 00:09:54,100
What kind of car?

38
00:09:55,200 --> 00:09:57,100
A Nissan sedan.

39
00:10:05,800 --> 00:10:07,400
How did you crash?

40
00:10:09,000 --> 00:10:10,900
Into a post.

41
00:10:18,000 --> 00:10:20,000
Are you all right?

42
00:10:28,200 --> 00:10:30,900
The front or the side?

43
00:10:31,000 --> 00:10:32,800
The front.

44
00:10:40,400 --> 00:10:42,900
But the engine starts?

45
00:10:50,500 --> 00:10:54,300
It's probably
the distributor harness.

46
00:10:55,400 --> 00:10:59,100
Harness? Harness?

47
00:11:05,500 --> 00:11:07,000
Let's make it 100 pesos.

48
00:11:31,200 --> 00:11:34,100
No thanks.
I've already had breakfast.

49
00:11:36,000 --> 00:11:37,400
Sica.

50
00:12:48,000 --> 00:12:50,200
Okay. Let's look for the part.

51
00:13:00,500 --> 00:13:06,500
Look, it's like a little box with a switch
and it has cables coming out of it

52
00:13:06,535 --> 00:13:08,200
that connect to the distributor.

53
00:13:08,300 --> 00:13:12,400
I'm going to lie down on the porch
because I get tired very quickly.

54
00:13:12,500 --> 00:13:16,900
When you find it let me know
and I'll tell you how to install it.

55
00:13:17,000 --> 00:13:20,700
Come on, Sica.
Here, boy. Let's go.

56
00:13:42,300 --> 00:13:44,000
Sir.

57
00:13:51,600 --> 00:13:53,400
Sir.

58
00:14:12,000 --> 00:14:15,400
"ALEX" AUTO PARTS

59
00:14:58,000 --> 00:15:11,000
"OASIS" AUTO PARTS

60
00:15:32,800 --> 00:15:35,265
"ORIENTE" AUTO PARTS

61
00:15:35,300 --> 00:15:38,800
Do you have a harness
for a distributor?

62
00:15:43,000 --> 00:15:44,400
What make is your car?

63
00:15:44,500 --> 00:15:46,000
It's a Nissan sedan.

64
00:15:50,200 --> 00:15:53,300
Do you know
what the distributor's like?

65
00:15:53,400 --> 00:15:55,400
No. It's a harness.

66
00:15:55,500 --> 00:16:01,900
It's like a little box with cables
and things poking out of it.

67
00:16:06,400 --> 00:16:08,200
Sure. Hang on.

68
00:16:33,800 --> 00:16:35,800
Can you wait 10 minutes?

69
00:16:35,900 --> 00:16:38,100
The guy who knows will be back
in 10 minutes.

70
00:17:34,400 --> 00:17:37,900
Do you think he's going to be
much longer, this guy who knows?

71
00:17:37,935 --> 00:17:41,400
He should have been here by now,
but he'll definitely be back.

72
00:17:59,200 --> 00:18:01,800
This is Fidel, my son.

73
00:18:02,800 --> 00:18:04,200
Fidel.

74
00:18:28,100 --> 00:18:29,200
Come on.

75
00:18:29,300 --> 00:18:32,300
- I just need the part.
- Hop on.

76
00:18:32,400 --> 00:18:34,700
I'll install it for you and you'll have
your wheels in five minutes. Come on.

77
00:18:36,900 --> 00:18:38,400
See you in a bit.
I won't be long.

78
00:18:38,500 --> 00:18:39,800
- See you.
- See you.

79
00:19:54,600 --> 00:19:56,500
It'll set you back 300 pesos.

80
00:19:56,535 --> 00:19:57,665
300?

81
00:19:57,700 --> 00:19:59,765
Yep, 300.

82
00:19:59,800 --> 00:20:01,900
I was told 100 at a different shop.

83
00:20:02,000 --> 00:20:04,800
Come off it, man.
The part itself is worth more.

84
00:20:06,000 --> 00:20:07,800
But I don't have 300.

85
00:20:07,900 --> 00:20:09,500
How much do you have on you?

86
00:20:20,500 --> 00:20:22,400
112.

87
00:20:26,700 --> 00:20:29,865
Can't you get another
100 pesos somewhere?

88
00:20:29,900 --> 00:20:33,900
100 pesos and your car'll be
ready in five minutes.

89
00:20:41,100 --> 00:20:42,900
Hello. It's Juan speaking.

90
00:20:42,935 --> 00:20:45,000
Is Arturo there?

91
00:20:47,000 --> 00:20:48,600
Yes, thanks.

92
00:20:49,700 --> 00:20:52,000
Yes, thanks. We're fine.

93
00:20:52,100 --> 00:20:53,000
I'll tell her.

94
00:20:53,100 --> 00:20:55,600
Could you put
Arturo on, please?

95
00:20:59,200 --> 00:21:00,800
Hi, Arturo.

96
00:21:02,300 --> 00:21:03,090
Good, thanks.

97
00:21:03,100 --> 00:21:05,000
Hey, could you lend me
a 100 pesos?

98
00:21:06,600 --> 00:21:09,050
Yes, I'm near your house.

99
00:21:09,085 --> 00:21:11,500
Okay. I'm on my way. Bye.

100
00:21:37,600 --> 00:21:39,100
No, it's that way.

101
00:21:56,500 --> 00:21:58,800
Wait here for me.
I'll be right back.

102
00:21:59,800 --> 00:22:01,800
Hello.

103
00:22:01,900 --> 00:22:04,000
Hi, honey, how are you?

104
00:22:04,100 --> 00:22:06,000
Fine, fine, thanks.

105
00:22:06,035 --> 00:22:07,765
Is Arturo in?

106
00:22:07,800 --> 00:22:10,400
Yes, but... How's your mother?

107
00:22:10,500 --> 00:22:12,400
Good, good, thanks.

108
00:22:12,500 --> 00:22:13,600
And your brother?

109
00:22:13,700 --> 00:22:15,500
Him too. Sorry,
but I'm in a bit of a hurry.

110
00:22:15,600 --> 00:22:17,300
Sure. Go on in.
He's in his room.

111
00:22:17,400 --> 00:22:18,700
Juan!

112
00:22:18,800 --> 00:22:20,600
Are you all right?

113
00:22:20,635 --> 00:22:21,900
Yes.

114
00:22:54,700 --> 00:22:56,800
Oh, shit!

115
00:22:58,900 --> 00:23:01,000
No way!

116
00:23:02,200 --> 00:23:04,800
Come and take a look at this.

117
00:23:07,500 --> 00:23:11,500
This is generation one...

118
00:23:13,000 --> 00:23:16,400
and yours is generation two.

119
00:23:25,000 --> 00:23:25,800
Come on.

120
00:23:25,900 --> 00:23:26,865
Where are we going?

121
00:23:26,900 --> 00:23:29,700
Where do you think?
To find the part.

122
00:23:29,800 --> 00:23:31,000
Come on.

123
00:23:34,600 --> 00:23:35,600
Come on.

124
00:23:51,700 --> 00:23:53,900
What about the part?

125
00:24:14,500 --> 00:24:17,000
Have a look at this while I find it.

126
00:24:30,200 --> 00:24:31,700
Shi Yan Ming.

127
00:24:35,000 --> 00:24:36,600
Do you have the part or not?

128
00:24:36,700 --> 00:24:39,000
He's a Shaolin monk.

129
00:24:41,200 --> 00:24:45,500
There aren't many of those around.
Shaolin monks are a rarity.

130
00:24:48,600 --> 00:24:51,100
And they're real special.

131
00:24:51,200 --> 00:24:55,600
One in a million.

132
00:24:55,700 --> 00:24:58,700
Or more like one in a billion.

133
00:25:02,700 --> 00:25:04,300
Do you know him?

134
00:25:16,700 --> 00:25:24,200
Shaolin Workout,
written by Shi Yan Ming.

135
00:25:24,235 --> 00:25:25,500
Read it.

136
00:25:25,600 --> 00:25:27,300
I'll get one of my own later.

137
00:25:27,335 --> 00:25:28,400
Read it.

138
00:25:31,800 --> 00:25:32,565
Thanks.

139
00:25:32,600 --> 00:25:34,400
Don't thank me now.

140
00:25:34,500 --> 00:25:36,600
Thank me once you've read it.

141
00:25:36,700 --> 00:25:38,000
Breakfast is ready!

142
00:25:38,100 --> 00:25:39,665
Coming.

143
00:25:39,700 --> 00:25:41,300
What about my part?

144
00:25:41,335 --> 00:25:43,265
Oh, the part.

145
00:25:43,300 --> 00:25:45,300
I said breakfast is ready!

146
00:25:47,400 --> 00:25:49,700
Let's have breakfast and I'll find the part
for you when we're done.

147
00:25:54,200 --> 00:25:55,200
Come on.

148
00:26:11,900 --> 00:26:14,400
Don't you say grace
before you eat?

149
00:26:26,300 --> 00:26:27,200
Amen.

150
00:26:27,300 --> 00:26:28,400
Amen.

151
00:26:28,500 --> 00:26:29,300
Amen.

152
00:26:31,000 --> 00:26:33,300
Have you never heard
the word of God?

153
00:26:33,400 --> 00:26:34,565
Mom.

154
00:26:34,600 --> 00:26:36,200
David, please.

155
00:26:51,500 --> 00:26:57,500
Behold I tell you a mystery:
We all shall not sleep;

156
00:26:57,600 --> 00:27:01,700
but we shall all be changed,

157
00:27:01,800 --> 00:27:04,400
in a moment,

158
00:27:04,500 --> 00:27:09,600
in the twinkling of an eye,
at the last trumpet:

159
00:27:09,700 --> 00:27:19,600
For the trumpet shall sound
and the dead shall be raised incorruptible,

160
00:27:19,700 --> 00:27:24,100
and we shall be changed.

161
00:27:24,200 --> 00:27:30,850
For this corruption
must put on incorruption

162
00:27:30,885 --> 00:27:37,500
and this mortal
must put on immortality.

163
00:27:44,300 --> 00:27:46,600
What do you think?

164
00:27:48,300 --> 00:27:49,600
Can I use your bathroom?

165
00:27:49,700 --> 00:27:52,400
Sure. Take your time.

166
00:27:56,500 --> 00:27:58,200
You scared him.

167
00:27:58,300 --> 00:28:01,300
The word of God
never scares.

168
00:28:02,600 --> 00:28:04,400
Do whatever you like.

169
00:28:04,500 --> 00:28:08,500
I don't do whatever I like;
I do His will!

170
00:28:08,600 --> 00:28:11,300
Why can't you
leave people in peace?

171
00:28:11,400 --> 00:28:15,100
You said it. In peace
is exactly how I want to leave them.

172
00:30:02,700 --> 00:30:04,300
Where were you?

173
00:30:08,600 --> 00:30:10,200
Out.

174
00:30:12,500 --> 00:30:14,400
Are you going to watch the game?

175
00:30:14,435 --> 00:30:16,400
I dunno.

176
00:30:18,800 --> 00:30:20,200
Where's Mom?

177
00:30:25,600 --> 00:30:27,500
Is she still in the bathroom?

178
00:30:33,700 --> 00:30:36,100
You haven't cleaned up the yard.

179
00:30:37,400 --> 00:30:40,300
Don't tell me what I have to do.

180
00:30:40,400 --> 00:30:43,100
You promised him.

181
00:31:18,100 --> 00:31:19,100
Mom.

182
00:31:24,700 --> 00:31:26,000
Mom.

183
00:32:00,700 --> 00:32:01,800
Mom.

184
00:32:13,800 --> 00:32:15,300
Mom.

185
00:32:15,400 --> 00:32:16,700
Get out.

186
00:32:24,400 --> 00:32:26,500
It's full of smoke in here.

187
00:32:26,600 --> 00:32:29,200
Get out, Juan.

188
00:32:32,500 --> 00:32:34,200
Are you all right?

189
00:32:34,300 --> 00:32:36,300
Yes, I'm fine.

190
00:32:36,335 --> 00:32:38,300
Now get out.

191
00:32:43,300 --> 00:32:45,200
Mom.

192
00:32:45,300 --> 00:32:47,400
Get the hell out!

193
00:32:55,100 --> 00:32:57,500
Are you going to watch the game?

194
00:33:36,100 --> 00:33:36,900
Hi.

195
00:33:38,100 --> 00:33:40,300
He left the part for you.

196
00:33:40,335 --> 00:33:41,600
Can I have it?

197
00:33:44,600 --> 00:33:46,300
Can you hold him a minute?

198
00:34:07,800 --> 00:34:09,600
What did you do?

199
00:34:09,700 --> 00:34:10,700
Nothing.

200
00:34:10,800 --> 00:34:15,300
No, keep him until he's asleep.
Then you can give him back to me.

201
00:34:37,500 --> 00:34:39,200
You're going to wake him up.

202
00:34:39,300 --> 00:34:40,600
No. We'll keep it low.

203
00:34:52,700 --> 00:34:57,200
"I don't have time for anything
except fooling around.

204
00:34:57,300 --> 00:35:01,400
I'm not the kind to live worrying...

205
00:35:01,500 --> 00:35:04,000
...nah... nah. "

206
00:35:16,400 --> 00:35:21,300
I used to sing in the school choir,
but I was good.

207
00:35:21,400 --> 00:35:24,500
I wasn't the lead voice
or anything,

208
00:35:24,600 --> 00:35:28,400
but I always say you don't need
a good voice to sing.

209
00:35:28,500 --> 00:35:32,500
You need energy,
strength. Right?

210
00:35:39,300 --> 00:35:42,565
I think he's fallen asleep.

211
00:35:42,600 --> 00:35:44,600
Don't you want
to hear another track?

212
00:35:44,700 --> 00:35:47,200
I have to get going.

213
00:35:47,300 --> 00:35:48,800
Can you take him?

214
00:35:57,600 --> 00:35:58,390
Thanks.

215
00:35:58,400 --> 00:35:59,500
See you around.

216
00:37:18,800 --> 00:37:20,300
Let me see.

217
00:37:22,100 --> 00:37:24,400
This is the right one.

218
00:37:24,500 --> 00:37:27,100
Do you know where
the distributor is?

219
00:37:44,000 --> 00:37:47,000
Sica, settle down. Sica.

220
00:37:47,100 --> 00:37:49,700
It's all right. I like dogs.

221
00:37:52,200 --> 00:37:54,000
You like dogs?

222
00:37:58,700 --> 00:38:01,300
Hey, could you
do me a big favor?

223
00:38:03,100 --> 00:38:05,400
Could you take Sica for a walk?

224
00:38:07,100 --> 00:38:09,200
I haven't been able to
take him out for a while.

225
00:38:09,300 --> 00:38:10,700
I'm not up to it anymore,

226
00:38:10,800 --> 00:38:13,000
but he needs exercise.
It's an active breed.

227
00:38:14,100 --> 00:38:16,350
I'm in a bit of a hurry.

228
00:38:16,385 --> 00:38:18,600
Just once around the block.

229
00:38:18,700 --> 00:38:20,700
Can't he go out on his own?

230
00:38:21,700 --> 00:38:24,100
No. He doesn't like
going out on his own.

231
00:38:24,135 --> 00:38:25,700
He's very attached to people.

232
00:38:34,800 --> 00:38:37,400
Just say "Heel, Sica. "

233
00:38:39,100 --> 00:38:40,400
Heel!

234
00:38:41,600 --> 00:38:42,700
Heel!

235
00:38:44,400 --> 00:38:46,600
Heel, Sica!

236
00:38:51,100 --> 00:38:52,500
Heel!

237
00:38:58,900 --> 00:39:00,300
Sica!

238
00:39:02,800 --> 00:39:04,000
Sica!

239
00:39:05,300 --> 00:39:06,800
Sica!

240
00:39:18,000 --> 00:39:19,600
Sica!

241
00:39:22,700 --> 00:39:24,500
Sica!

242
00:39:37,900 --> 00:39:39,800
Sica!

243
00:39:43,000 --> 00:39:44,700
Sica!

244
00:40:33,000 --> 00:40:35,000
Haven't you fixed it yet?

245
00:40:39,800 --> 00:40:41,400
What part is it?

246
00:40:41,500 --> 00:40:43,900
The distributor harness.

247
00:40:46,200 --> 00:40:49,400
The distributor harness.

248
00:41:00,500 --> 00:41:02,500
Hey, what are you doing tonight?

249
00:41:03,500 --> 00:41:04,800
Nothing.

250
00:41:07,900 --> 00:41:10,300
Can I ask you a favor?

251
00:41:12,800 --> 00:41:18,300
I know we've only just met,
but I feel I can trust you.

252
00:41:20,000 --> 00:41:22,500
Could you babysit Fidel?

253
00:41:23,600 --> 00:41:25,600
Sorry, I can't.

254
00:41:26,900 --> 00:41:30,400
I've never seen anyone put him
to sleep the way you did.

255
00:41:32,300 --> 00:41:35,400
I told you... Jessy Bulbo
is playing tonight

256
00:41:35,500 --> 00:41:38,400
and I don't have anyone
to look after Fidel for me.

257
00:41:38,500 --> 00:41:41,000
I took him to a Rata Blanca concert
a couple of months ago

258
00:41:41,100 --> 00:41:43,400
and we nearly got crushed.

259
00:41:46,500 --> 00:41:47,400
No?

260
00:41:50,000 --> 00:41:51,800
Come on. Please?

261
00:41:56,800 --> 00:41:58,400
Thanks anyway.

262
00:42:36,900 --> 00:42:43,265
Open your eyes and look at me.

263
00:42:43,300 --> 00:42:49,700
Don't be afraid, come closer to me.

264
00:43:10,900 --> 00:43:12,400
Do you think he'll come?

265
00:43:12,500 --> 00:43:13,900
Who?

266
00:43:13,935 --> 00:43:15,265
David.

267
00:43:15,300 --> 00:43:16,900
Yeah, sure thing.

268
00:43:30,000 --> 00:43:32,000
Why did you run off like that?

269
00:43:33,400 --> 00:43:34,800
Let's go.

270
00:43:37,000 --> 00:43:38,800
I'll be back shortly.

271
00:43:39,900 --> 00:43:40,700
See you.

272
00:43:40,800 --> 00:43:41,700
Bye.

273
00:44:07,500 --> 00:44:08,800
Kill it.

274
00:44:19,000 --> 00:44:20,400
Try again.

275
00:44:27,500 --> 00:44:29,200
Stop.

276
00:44:29,300 --> 00:44:31,900
Come over here.

277
00:44:34,900 --> 00:44:39,800
This is the part that's screwed,
not the distributor harness.

278
00:44:43,400 --> 00:44:44,500
Is it expensive?

279
00:44:44,600 --> 00:44:46,300
Yes, it's expensive.

280
00:45:02,400 --> 00:45:04,565
What did I tell you?

281
00:45:04,600 --> 00:45:07,400
Didn't the expert tell you I'd fix it?

282
00:45:09,500 --> 00:45:13,400
"If you think something is impossible,
you will make it impossible. "

283
00:45:22,100 --> 00:45:23,500
Bruce Lee.

284
00:45:48,900 --> 00:45:49,900
Juan.

285
00:46:37,500 --> 00:46:38,800
Cheer up!

286
00:46:55,100 --> 00:46:57,000
Let's get out of here!

287
00:47:15,600 --> 00:47:16,900
Try it now.

288
00:47:20,000 --> 00:47:21,800
That's enough!

289
00:47:32,300 --> 00:47:33,400
Try again!

290
00:47:38,800 --> 00:47:41,000
Give your sensei a hug!

291
00:47:42,800 --> 00:47:43,965
What did I tell you, bro?

292
00:47:44,000 --> 00:47:47,500
Didn't I tell you I'd fix it?
I'm the guy who knows.

293
00:47:47,600 --> 00:47:48,300
Thanks.

294
00:47:49,500 --> 00:47:51,400
Hey, where do you
think you're going?

295
00:47:51,500 --> 00:47:52,965
I have to get the car back.

296
00:47:53,000 --> 00:47:55,700
No shit, man. I was going
to invite you to the movies.

297
00:47:55,800 --> 00:47:57,700
I can't.
I have to get the car back.

298
00:47:57,800 --> 00:47:59,400
You can take it back later.

299
00:48:00,500 --> 00:48:04,600
"Enter the Dragon" is playing, man!
It's only playing today.

300
00:48:05,900 --> 00:48:07,500
I can't.

301
00:48:08,800 --> 00:48:12,500
Tough. If you change your mind,
it's on at the Meteoro at 7:00.

302
00:48:12,535 --> 00:48:13,865
Okay.

303
00:48:13,900 --> 00:48:15,300
Do you want a ride?

304
00:48:15,335 --> 00:48:16,700
No. I have my bike.

305
00:48:16,800 --> 00:48:19,400
Okay. Thanks.

306
00:48:19,500 --> 00:48:21,600
You're welcome.

307
00:49:30,800 --> 00:49:32,700
Have you eaten?

308
00:49:55,500 --> 00:49:58,050
Cool! It's Dad's!

309
00:49:58,085 --> 00:50:00,600
Do you like it?

310
00:50:00,610 --> 00:50:01,380
Yeah.

311
00:50:01,400 --> 00:50:02,400
It's for you.

312
00:50:02,500 --> 00:50:03,600
Where did you get it?

313
00:50:03,700 --> 00:50:04,900
Somewhere.

314
00:50:13,000 --> 00:50:14,900
What's condolences?

315
00:50:18,400 --> 00:50:19,900
Why do you ask?

316
00:50:21,000 --> 00:50:24,300
People have been
calling all day,

317
00:50:24,400 --> 00:50:30,600
and when I answer, everyone says
to accept their condolences.

318
00:50:38,400 --> 00:50:39,500
Do you want a yogurt?

319
00:50:58,900 --> 00:51:02,400
Fucking hell!
It's screwed.

320
00:51:13,900 --> 00:51:15,800
Hang on.

321
00:51:22,400 --> 00:51:24,500
Joaqu�n, answer that!

322
00:51:24,600 --> 00:51:26,700
Don't.

323
00:51:31,300 --> 00:51:33,600
For the love of God,
answer it!

324
00:51:33,700 --> 00:51:34,800
Hello?

325
00:51:36,500 --> 00:51:37,700
No, he's not in.

326
00:51:39,100 --> 00:51:40,600
His son.

327
00:51:42,100 --> 00:51:45,700
No, we're not interested
in a timeshare.

328
00:51:48,400 --> 00:51:51,500
No, he can't come to
the phone right now.

329
00:51:51,600 --> 00:51:53,600
He just can't.

330
00:51:53,700 --> 00:51:56,400
Because he's fucking dead!

331
00:52:02,400 --> 00:52:03,500
I'll be back in a while.

332
00:52:03,600 --> 00:52:05,300
Where are you going?

333
00:53:51,700 --> 00:53:52,900
Sica!

334
00:53:55,500 --> 00:53:56,800
Sica!

335
00:55:38,900 --> 00:55:40,500
No.

336
00:55:58,800 --> 00:56:00,100
Drive!

337
00:56:04,900 --> 00:56:07,000
I said drive!

338
00:56:47,200 --> 00:56:49,200
It's your dog.

339
01:00:19,000 --> 01:00:20,700
Kick me!

340
01:00:22,700 --> 01:00:24,100
Kick me!

341
01:00:30,400 --> 01:00:31,800
What was that?

342
01:00:31,900 --> 01:00:33,065
An exhibition?

343
01:00:33,100 --> 01:00:36,700
We need emotional content.

344
01:00:38,000 --> 01:00:39,800
Try again.

345
01:00:46,500 --> 01:00:49,600
I said emotional content,

346
01:00:49,700 --> 01:00:54,200
not anger;
now try again, with me.

347
01:01:02,000 --> 01:01:03,100
That's it.

348
01:01:39,800 --> 01:01:43,000
Open is war,

349
01:01:43,100 --> 01:01:46,200
closed is peace.

350
01:03:42,700 --> 01:03:46,600
You can sleep in my bed.

351
01:03:46,700 --> 01:03:48,300
Do you have any brothers
or sisters?

352
01:03:48,335 --> 01:03:49,700
Yes. Joaqu�n.

353
01:03:50,800 --> 01:03:53,800
Well, look after him
as if he were your brother.

354
01:03:55,200 --> 01:03:58,000
I won't be late.
As soon as it's over,

355
01:03:58,035 --> 01:04:00,800
I'll be straight back.

356
01:04:12,900 --> 01:04:15,100
Chemo? What's up?

357
01:04:15,200 --> 01:04:18,600
Hey, it looks like I'm going
to be able to make it after all.

358
01:04:18,700 --> 01:04:20,200
Can you pick me up?

359
01:04:21,300 --> 01:04:22,900
What?

360
01:04:23,900 --> 01:04:25,700
Yesterday?

361
01:04:26,900 --> 01:04:28,600
Did you go?

362
01:04:30,600 --> 01:04:32,200
I bet it was.

363
01:04:33,600 --> 01:04:36,800
Tough luck.
Maybe next time.

364
01:04:36,900 --> 01:04:38,200
Bye.

365
01:04:54,600 --> 01:04:57,300
I'd better be going.

366
01:05:25,600 --> 01:05:27,800
Do you want some?

367
01:05:37,100 --> 01:05:39,100
OK then.

368
01:10:28,000 --> 01:10:30,000
I'm home.

369
01:12:21,000 --> 01:12:24,200
Did you fart?

370
01:12:24,300 --> 01:12:26,500
You dirty pig!

371
01:12:26,600 --> 01:12:28,600
No, don't go!

372
01:12:32,500 --> 01:12:35,200
I'm not going anywhere.

373
01:12:35,235 --> 01:12:37,900
Let's make hotcakes.

374
01:12:38,000 --> 01:12:40,000
Like dad used to?

375
01:12:51,400 --> 01:12:52,950
What's wrong?

376
01:12:52,985 --> 01:12:54,500
I have to pee.

377
01:12:54,600 --> 01:12:55,900
Then go pee.

378
01:12:56,000 --> 01:12:57,600
OK, but don't eat them all.

379
01:12:57,700 --> 01:12:59,400
I won't.

380
01:13:41,100 --> 01:13:43,600
What are you doing?
That's mine!

381
01:13:46,000 --> 01:13:47,600
What?

382
01:13:49,700 --> 01:13:52,500
It's missing something.

383
01:13:55,400 --> 01:13:57,400
Come with me.

384
01:14:22,700 --> 01:14:26,500
We never took it off.

385
01:14:26,600 --> 01:14:29,065
He hated the sight of it.

386
01:14:29,100 --> 01:14:31,800
Ever since we stuck it on,
he hated it.

387
01:14:54,600 --> 01:14:57,800
Do you remember
Lake Tahoe?

388
01:14:59,000 --> 01:15:03,000
We never went
to Lake Tahoe.

389
01:15:03,100 --> 01:15:06,700
Aunt Marta
brought it back for us.

390
01:15:10,100 --> 01:15:12,900
It looks pretty in the picture.

391
01:15:15,900 --> 01:15:18,600
I bet it is.

392
01:15:24,000 --> 01:15:27,600
Come on. Let's go.

393
01:16:13,400 --> 01:16:17,600
TO MY FATHER AND JON�S

