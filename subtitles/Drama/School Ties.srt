{1}{1}23.976
{25}{100}Downloaded From www.AllSubs.org
{3779}{3826}Hey, David!
{4195}{4256}Davey!|Oh, Davey, come here!
{4260}{4350}Edie, he's not going off to war,|only Massachusetts.
{4404}{4469}Here, Davey... For the trip.
{4473}{4561}Edie gives something away!|I'll have a heart attack!
{4580}{4662}Go and become a gentleman,|not like this riff-raff.
{4666}{4769}We ought to kick his butt|for breaking up the team.
{4773}{4838}Hey!|You want to stay in here?
{4860}{4910}If I had your brains, I'd go.
{4914}{5009}- His brains? Any brains!|- Look who's talking.
{5017}{5067}- I don't know.|- What?
{5071}{5190}- I don't know about that place.|- You know it's not this place.
{5263}{5331}- Oh, shit. Kocus.|- So what?
{5335}{5419}The night before last,|his sister gave me a hand job.
{5423}{5483}- Her idea.|- Yeah, right.
{5592}{5644}- Hey, Bear.|- Hey, Kocus.
{5667}{5789}We was thinking about going in there,|you know, eat something.
{5793}{5916}- I don't think it would be a good idea.|- Why not? They let Jews in there.
{5967}{6069}Your sister can come in,|only she'll have to wash her hands.
{6147}{6219}You got a Jew friend|with a smart mouth.
{6223}{6318}- I guess I do.|- It don't bother you they killed Jesus?
{6322}{6384}No. I didn't know the man.
{6398}{6497}It bothers the shit out of me,|you sheeny bastard!
{6501}{6571}Hey! Take it to the alley!
{6946}{7057}Yo, man! Come on!|Don't let him do that! Come on! Yeah!
{7080}{7126}Hit the Jew!
{7748}{7798}Give 'em hell, kid!
{7907}{7957}You're late.
{8421}{8471}Look at me.
{8482}{8564}- Goddamnit, David.|- I had to.
{8568}{8684}- Such an opportunity and you do this!|- I had to!
{8688}{8764}This is a school|two presidents went to.
{8768}{8867}They'll think you're a hoodlum.|They might send you back.
{8871}{8921}- Fine.|- Fine?
{8983}{9083}Take a look at this place.|You want this life?
{9100}{9193}He called me a sheeny bastard.|Should I walk away?
{9197}{9306}Yes. It ain't your problem.|You can't fight your way through life.
{9310}{9428}- You never got into any fights?|- Sure, but nobody handed me Harvard.
{9739}{9829}- You're going to miss the bus.|- We'll make it.
{9861}{9942}- Who did you fight with?|- Kocus.
{10073}{10120}OK, kids.
{10124}{10228}You can fit in or hack around|with a chip on your shoulder.
{10232}{10382}- All right. All right, I'll fit in.|- They came to you, not you to them.
{10386}{10501}You don't have to explain|nothing to nobody. Understand?
{10505}{10555}All right.
{10665}{10746}Right. Say goodbye|to your brother and sister.
{10750}{10826}- Goodbye, Sarah.|- Bye, David.
{10948}{10998}Bye, Petey.
{11182}{11239}You kids wait over there.
{11314}{11364}Change your shirt.
{11407}{11490}- What did the other guy look like?|- Worse.
{11664}{11761}- You're sounding like Grandpa.|- You should be so lucky.
{11863}{11913}Got to go, Dad.
{12088}{12142}Go. Make us proud.
{12302}{12354}- Bye.|- Bye.
{13801}{13867}- Hi, Coach.|- Welcome to Cabot.
{14039}{14113}I had a little accident.|It's nothing.
{14148}{14219}- Let me get this for you.|- Thanks.
{14656}{14725}- How was your trip?|- Fine. I slept.
{14729}{14795}- Good. How's your dad?|- Fine.
{14799}{14870}He's pretty excited about all this.
{14874}{14943}- What father wouldn't be?|- Yeah.
{14947}{15015}The team's looking forward|to meeting you.
{15019}{15107}Good. I'm looking forward|to meeting them, too.
{15792}{15914}- Jesus, this is a high school!|- Yeah. It's your high school.
{16483}{16564}- Thanks for the lift.|- I'll get your bag.
{16742}{16816}Thanks.|I'll see you at practice.
{16846}{16972}Just a minute. I meant to ask you...|Do you have any diet problems?
{16976}{17075}- Diet problems?|- Is there anything you can't eat?
{17119}{17208}- Turnips.|- Turnips. I can't eat them, either.
{17212}{17262}I'd better let you get settled.
{17266}{17373}- The kids will be curious about you.|- I'm curious about them.
{17377}{17478}Nobody comes here for just|their last year. It's unusual.
{17496}{17606}They're great kids, don't get me wrong.|But they're privileged.
{17610}{17690}They take a lot for granted|you and I never would.
{17694}{17771}Just play your cards|close to the vest.
{17775}{17823}What do you mean?
{17827}{17909}Don't tell people|more than they need to know.
{17953}{18019}- See you at practice.|- All right.
{19186}{19273}- Hi! Chris Reece, your roommate.|- Hi! David Greene.
{19277}{19372}Pleased to meet you. Rip Van Kelt,|Jack Connors, Charlie Dillon.
{19376}{19427}We're the big men on campus.
{19436}{19493}- It's true.|- Where you from?
{19507}{19559}- Scranton, PA.|- Scranton?
{19563}{19637}- It's in America, Connors.|- No shit.
{19641}{19719}You're the first ringer|St Matt's ever hired.
{19723}{19820}- Dillon...|- No, he is. It's an honour.
{19824}{19911}- Aren't you honoured?|- I hadn't thought about it.
{19915}{20024}- The best quarterback money can buy.|- Dillon, lay off!
{20092}{20173}Don't pay any attention to him.|Peanuts?
{20177}{20223}No problem.
{20260}{20335}Even in Scranton, a prick's a prick.
{20339}{20463}You don't have to be so sensitive.|Come on, it's not required here.
{20490}{20580}There was some talk about me|playing quarterback, so...
{20584}{20659}- Were you in an accident?|- I got into a fight.
{20663}{20723}- Seriously?|- A fistfight?
{20727}{20837}- Kind of a going-away fight.|- What, you mean like a rumble?
{20841}{20908}- Yeah, like a rumble.|- Over girls and stuff?
{20912}{21040}There's a place we hang out and these|motorcycle guys wanted to come in.
{21044}{21101}- Bikers.|- We wouldn't let them.
{21105}{21183}- You didn't want them on your turf?|- Right.
{21187}{21277}Everybody knows not to go|on someone else's turf.
{21281}{21377}Who knows what evil|lurks in the hearts of men?
{21381}{21434}The Shadow knows.
{21468}{21561}Crime does not pay.|The Shadow knows.
{21565}{21615}McGiv! My roomie!
{21691}{21759}David Greene,|our new quarterback.
{21763}{21824}- This is Mac.|- How do you do?
{21828}{21878}Hi.
{21915}{22051}Football is a game for bug squashers,|cretins and criminals. Don't you agree?
{22055}{22185}- Mac wants to play, but he's too frail.|- So we let him be student manager.
{22211}{22321}That's it! I'm going to kick|your smelly ass back to Greenwich.
{22370}{22435}I guess you didn't get a school tie|yet.
{22477}{22542}Here, you can wear this one.
{22546}{22653}I got extras because I'm always|dragging them through the soup.
{22657}{22705}Thanks.
{22781}{22883}You mind if I ask...|How did you wind up here?
{22900}{22992}- I'm not supposed to talk about it.|- Let me guess.
{22996}{23116}Coach McDevitt visited and said|you could get an alumni scholarship?
{23126}{23182}Good guess.|How did you know?
{23199}{23292}St Luke's has whipped us|for three years in a row.
{23296}{23386}The alumni are pissed.|They want to win real bad.
{23411}{23468}Not too much pressure, huh?
{23506}{23556}I want a good seat at the back.
{23560}{23628}It's different from public schools.
{23632}{23713}Chapel three times a week.|But it's not that bad.
{23717}{23767}No kidding?
{25231}{25287}Gentlemen of St Matthew's...
{25317}{25418}...welcome to the finest|preparatory school in the nation.
{25442}{25594}Welcome especially to our new boys.|I am Dr Bartram, your headmaster.
{25598}{25696}The rest of you may|conceivably remember me.
{25747}{25822}His annual joke.|Make the most of it.
{25826}{25907}Tomorrow begins the 193rd fall term.
{25911}{26026}No, I was not in office|when the first one began.
{26030}{26087}Some of you new boys may find
{26091}{26189}that academics and discipline|here are very demanding.
{26193}{26252}Much of what is policy here,
{26256}{26366}including our cherished Honour Code,|has been established,
{26370}{26469}not by me or your teachers,|but by your fellow students,
{26473}{26564}to be enforced|by your own tribunal of prefects,
{26568}{26652}as it has been|for the last two centuries.
{26672}{26726}We judge ourselves here,
{26730}{26831}and we judge ourselves|by the highest standards.
{26860}{26937}You, my boys, are among|the elite of the nation,
{26941}{27001}and we strive at St Matthew's
{27005}{27133}to prepare you for the responsibility|that comes with favoured position.
{27163}{27248}Today, more than ever,|this country needs an elite
{27252}{27336}that cares more for honour|than for advantage;
{27365}{27470}more for service|than for personal gain.
{27549}{27630}To that end,|let us beseech the help of God,
{27634}{27695}in whose name we pray.
{27838}{27951}Our Father, who art in Heaven,|Hallowed be Thy name...
{28880}{28977}- Decent hi-fi, Mac.|- Bought it from a friend back home.
{28981}{29051}- How much?|- I jewed him down to $30.
{29055}{29105}I'll give you $25 for it.
{29109}{29185}He always wants|something for nothing.
{29189}{29234}And he's not even Jewish.
{29298}{29376}- Evening, gentlemen.|- Evening, sir.
{29394}{29444}Evening.
{29473}{29586}Whose music is that?|And I use the term advisedly.
{29590}{29672}- That's the great sound of The Robins.|- No.
{29676}{29749}I mean the man who would purchase|such swill.
{29753}{29833}- That would be me.|- That would be I.
{29935}{30022}- Have you a name?|- McGivern.
{30026}{30083}- And you?|- Mr Cleary.
{30109}{30215}I happen to be the new housemaster.
{30253}{30316}Turn it off, please.
{30378}{30472}One's cultural environment|ought to be as important
{30476}{30555}as the air he breathes|and the food he eats.
{30862}{30993}- Surely, in your day, you had music?|- Yes, and my day has not passed.
{30997}{31047}Dave Brubeck, Ray Anthony,
{31051}{31111}Les Elgart, Mitch Miller,
{31115}{31174}Les Baxter, Roger Williams...
{31205}{31271}We'll have no problem.
{31275}{31338}Gentlemen, we all have to live here.
{31347}{31477}But we're not going to bring the jungle|into my house, thank you very much.
{32316}{32396}God, Greene,|where did you get the balls?
{32476}{32571}- You are in.|- I hope all the teachers aren't like him.
{32575}{32648}Most are OK.|Who'd you get for History?
{32652}{32717}- Gierasch.|- Me, too. He's tough.
{32721}{32766}- French?|- Renard.
{32770}{32850}Good. We'll get him talking|and go to sleep.
{32854}{32917}- You shower morning or night?|- Night.
{32921}{33011}Hurry up.|We'll just make it before lights out.
{34094}{34144}What a beautiful day.
{34252}{34307}Renard will be a breeze.
{34647}{34683}Well...
{34714}{34769}Well, well.
{34800}{34868}My musical upstairs neighbours.
{34955}{35032}Please, do sit down.
{35068}{35169}Monsieur Renard is cutting back|on his teaching load,
{35173}{35304}so I will have the pleasure|of teaching this section of French 4.
{36073}{36118}I don't understand...
{36677}{36727}You know how a team works.
{36731}{36803}I wasn't the quarterback|you wanted.
{36807}{36924}- You are our number one back-up.|- But I'd better play halfback.
{36928}{37028}It's our weakest spot now.|You'll make a great halfback.
{37032}{37138}You can run and you can block.|You've got all the stuff, kid.
{37142}{37233}- And I'll give it my best.|- I'm counting on it.
{37254}{37311}Listen up!
{37315}{37419}New face on the varsity this year.|David Greene.
{37423}{37493}- Greene comes from Pennsylvania.|- Hi.
{37506}{37551}Played quarterback.
{37555}{37663}Led his team to a championship win|last year in a tough league.
{37667}{37740}This year,|we're concentrating on passing.
{37744}{37794}McGivern has some new plays.
{37798}{37896}If captured, eat them.|If still alive, meet here tomorrow.
{37924}{37999}We'll run them through tomorrow.
{38003}{38091}Today, let's get out there|and warm up. Hit it!
{38532}{38582}- Hi, Coach.|- Dillon.
{38619}{38679}Hi, Dillon.|How was practice?
{38727}{38777}How was practice!
{39059}{39126}- Where's Greene?|- I don't know.
{39130}{39218}- I thought he'd be here.|- Save a place for him, huh?
{39232}{39307}- Like this...|- Will you get off me!
{40019}{40081}Thank you, God,|for your bountiful gifts...
{40092}{40157}- I'm holding my own.|- And French?
{40161}{40265}- Hate the teacher.|- Everybody hates at least one.
{40269}{40325}I'd better get back to the books.
{40329}{40391}- Talk soon.|- Don't forget Saturday.
{40395}{40460}- Saturday?|- It's Rosh Hashanah.
{40509}{40581}I've got a game against Winchester,|Pop.
{40585}{40667}It's a very holy day.|It goes back longer than us.
{40671}{40760}You show respect and get to temple.
{40764}{40828}Davey, do you hear me?|No excuses.
{40833}{40908}OK.|Sure, don't worry, Pop.
{40930}{41015}- I'll speak to you next week.|- OK. Bye.
{41116}{41156}Down!
{41238}{41278}Set!
{41308}{41353}Go! Go!
{41571}{41664}St Matt's, third and five,|on the Winchester 41.
{41690}{41730}Two!
{41734}{41772}Three!
{42183}{42298}St Matt's touchdown.|St Matt's six. Winchester nothing.
{42317}{42378}That boy's good.|What's his name?
{42382}{42482}Van Kelt. The boys call him Rip.|He's the team captain.
{42486}{42564}I think he means the boy|who threw the pass.
{42568}{42637}Oh. That's David Greene.
{43471}{43526}The score is tied seven apiece.
{43530}{43654}St Matt's ball, third and seven|on the Winchester forty-yard line.
{43756}{43796}Set!
{43810}{43850}Down!
{43872}{43918}Set... One!
{43975}{44015}Go!
{44246}{44293}Yeah!
{44347}{44406}Touchdown, St Matt's!
{44599}{44688}We've got a quarterback!|We've found our quarterback!
{44836}{44898}- Lights out!|- Lights out!
{45128}{45205}You go ahead, dear.|I'll be right there.
{45631}{45681}Who is it?
{45728}{45845}- David Greene, sir.|- What are you doing here, Greene?
{45849}{45893}Praying, sir.
{45897}{45977}I imagine your God allows prayer|during daylight.
{45981}{46109}I couldn't get away before.|It's Rosh Hashanah, Jewish New Year.
{46113}{46166}I know what Rosh Hashanah is.
{46170}{46247}And it ends at sunset|if I recall the custom.
{46251}{46384}Technically. But it wouldn't go over|too well if I said I couldn't play.
{46388}{46498}- My scholarship depends on football.|- Yes. I saw the game.
{46502}{46594}You seemed thoroughly concentrated|on the task.
{46598}{46648}Thank you, sir.
{46669}{46800}You people are very determined,|aren't you?
{46804}{46885}Sometimes we have to be, sir.
{46889}{46939}I seem to recall a blessing:
{46943}{47040}"Blessed are the meek,|for they shall inherit the earth."
{47044}{47114}I wonder how meek they'll be|when they do, sir.
{47118}{47186}Are you finished here, Mr Greene?
{47236}{47284}Yes, sir.
{47288}{47364}Then I suggest|you sneak back to your room.
{47368}{47455}I shall overlook|this evening's infraction.
{47530}{47587}- Mr Greene?|- Sir?
{47624}{47734}Was it worth it? Breaking a tradition|just to win a football game?
{47788}{47850}Your tradition or mine, sir?
{48115}{48215}The son of a bitch|gives us English to French!
{48219}{48293}The pig won't last here.|I'll bet you.
{48297}{48379}Won't last?|Connors, I flunked that test.
{48428}{48507}I'm getting sick|of these goddamned bells!
{48943}{48997}- Henry Vlll assumes throne?|- 1509.
{49001}{49111}- 1649, Mr Smith?|- Charles I was executed.
{49115}{49219}Correct. Which resulted|in the establishment of what?
{49223}{49301}- A commonwealth, Mr Gierasch.|- Very good.
{49305}{49406}Mr Dillon, when did|Mary Queen of Scots lose her head?
{49420}{49494}- 1687.|- Close. You're only a century off.
{49498}{49544}- Mr Greene?|- 1587.
{49548}{49652}Indeed. And what occurred|during the years 1553 to 1558?
{49656}{49713}- Mr Collins?|- Bloody Mary.
{49717}{49798}So it was.|Which resulted in what, Mr Reece?
{49802}{49863}Catholicism was restored.
{49867}{49915}How come, Mr Connors?
{49919}{49980}She married what's-his-name.
{50007}{50072}- Philip?|- Well, more or less.
{50102}{50155}August 9th, 1593?
{50169}{50219}Anybody?
{50244}{50306}The birth of lzaak Walton.
{50310}{50361}A personal favourite.
{50382}{50470}Mr Dillon, a literary event...
{50512}{50562}1611?
{50588}{50674}- McGivern?|- Publication of the King James Bible.
{50678}{50728}Correct.
{50755}{50830}You care to try for three, Mr Dillon?
{50859}{50949}I remind you, Mr Dillon,|this course has no shallow end.
{50953}{50998}Sink or swim.
{51012}{51106}If I don't get total tit tonight,|I'll cut my throat.
{51110}{51173}Sex is my only reason|for living.
{51186}{51250}Then be careful|you don't cut your hand!
{51286}{51348}Life isn't over yet, Mac.
{51352}{51428}When Princeton might accept|a C in French
{51432}{51509}and you're flunking French,|life is over.
{51513}{51600}Princeton isn't|the only lvy League school.
{51652}{51723}Someone explain|to our friend from Scranton.
{51727}{51828}Five generations of McGiverns have|gone to Princeton.
{51832}{51917}If I don't get in,|it means the others had cocks,
{51921}{51971}and I just have a wee-wee.
{51994}{52096}I have trouble sympathising.|Harvard wants monthly reports on me.
{52107}{52177}- How about you, Greene?|- Touch and go.
{52181}{52231}I'm getting a C in French.
{52235}{52353}Dillon's brother graduates this year.|The back-up quarterbacks are thumbs.
{52357}{52450}I wouldn't go to Harvard.|All those Jews and Communists.
{52454}{52541}- That's just the faculty.|- You are so full of shit.
{52545}{52592}Jew-lover!
{52596}{52680}So what if there are Jews?|They're not in the clubs.
{52684}{52796}- That's not the point.|- It is. You don't have to be with them.
{52800}{52877}- Why would you want to?|- I don't want to.
{52881}{52943}Then don't go to Harvard, Dillon.
{52947}{52997}- Help?|- How would you know?
{53001}{53051}- What?|- If you're with them?
{53055}{53179}Are you kidding? How would you|not know? It's hard to miss a heeb.
{53183}{53242}Oh, God, girls,|eat your hearts out!
{53246}{53375}Oh, no! You never mess with my hair!|I can't believe you did that!
{53390}{53448}No way! You're dead!
{53998}{54053}Hurry up, fellas. Come on.
{54826}{54911}Don't forget to make room|for the Holy Ghost.
{56346}{56434}- You can roll your tongue back up.|- She's beautiful!
{56438}{56524}Sally Wheeler.|Dillon says she's his girlfriend.
{56528}{56609}- Is she?|- I guess so. That's the word.
{57715}{57803}- Isn't that your new quarterback?|- David Greene.
{57968}{58041}He must be half nigger!|He can dance.
{58618}{58665}Introduce me.
{58682}{58741}- David!|- Excuse me.
{58885}{58953}You move as well on the floor|as the field.
{58957}{59031}- Sally Wheeler, David Greene.|- Dillon!
{59110}{59200}- I'll be back in a minute.|- No rush.
{59239}{59347}- I saw you dancing.|- I saw you dancing, too. With Dillon.
{59411}{59482}- Dillon's a great guy.|- He's fun.
{59486}{59532}Yeah.
{59635}{59698}Do you think he'll go to Harvard?
{59702}{59801}Is that what you do in your|spare time? Worry about Dillon?
{59860}{59920}I don't have any spare time.
{60067}{60119}This is a great song.
{60254}{60362}- Would you like to dance?|- Yeah, I would.
{60783}{60838}Are you two going steady?
{60862}{60922}No. Our families share some woods,
{60926}{61001}so we've known each other|since we were five.
{61005}{61055}We're thrown together a lot,
{61059}{61149}so everybody thinks we go steady,|but they're wrong.
{61231}{61325}I know how it is. My family shares|some woods in Pennsylvania.
{61329}{61379}With 300,000 other people.
{61456}{61530}You must think I'm a spoiled brat.
{61582}{61630}I think you're so pretty.
{61735}{61829}- I must sound like a real nosebleed.|- No.
{61833}{61922}- Don't tell your roommate.|- How do you know her?
{61926}{62000}I don't.|I just don't want her to know.
{62087}{62177}- You're pretty, too.|- Thanks for taking care of my girl.
{62181}{62229}Try the punch.
{62884}{62934}Maybe he won't show.
{62946}{63034}- I'm going to puke. My stomach hurts.|- You'll be OK.
{63038}{63129}25% of the grade!|That's crazy. That's just not right.
{63133}{63220}- I wonder who he'll call on first.|- Take a guess.
{63224}{63297}It all comes down to this one day.|Jesus!
{63301}{63399}- It doesn't. You're worked up.|- My mouth's all dried out.
{63403}{63457}Shit! I can't do this!
{66466}{66543}- That asshole Cleary!|- Sadistic shiteater!
{66547}{66620}- Mac was doing all right.|- Will he be OK?
{66624}{66768}- Let him alone. He'll be OK.|- Poor bastard. He really took it.
{66780}{66846}David, your sister's on the phone.
{66886}{66984}- Hello, Sarah, what's up?|- I didn't know you had a sister!
{66988}{67037}- Who is this?|- Sally Wheeler.
{67041}{67123}- Hi. Where are you?|- At school, at the dorm.
{67140}{67208}- Are you studying?|- Yeah. Chemistry.
{67212}{67272}- I hate Chemistry.|- Me, too.
{67309}{67384}- You ever go to Skip's Diner?|- Skip's Diner?
{67388}{67455}- Seen McGivern?|- Hold on... Haven't you?
{67459}{67539}Not since French class.|Where the hell is he?
{67543}{67649}- Check with Dillon and Van Kelt.|- All right. See you later.
{67653}{67741}- Hi... Skip's Diner?|- Yeah, it's in town.
{67745}{67794}Kind of a hang-out.
{67798}{67905}I thought if you weren't doing|anything tomorrow, we could go.
{67909}{67976}- What time?|- Ten o'clock?
{68313}{68420}He hasn't shown up.|I'm going to go look for him.
{69146}{69196}Lights out!
{69642}{69724}McGivern! Mac!|Come on, Mac, buddy! Where are you?
{69728}{69778}Mac!
{70312}{70392}- Hold on, I'll get the lights.|- McGivern?
{70475}{70521}Mac?
{70613}{70681}- McGiv!|- Mac!
{71521}{71581}- Let's tell somebody.|- Maybe he's drunk.
{71585}{71661}- Mac never drinks.|- Do you think he went home?
{71665}{71766}- He would have said something.|- Isn't that French class?
{71844}{71892}Come on, let's go.
{72193}{72244}- Mac!|- McGiv?
{72341}{72405}Come on. Hey!
{72443}{72501}Hey! Get some help.
{72992}{73048}Shh... Shh... It's OK.
{73098}{73202}Gentlemen, please! Please, gentlemen,|go back to bed. Please.
{73331}{73417}You did this!|You did this!
{73421}{73471}You rode him until he broke!
{73475}{73560}David, walk away!|Just walk away!
{73564}{73650}You wouldn't let up, would you?|You did this!
{73654}{73744}- Come on!|- I'll get you, Cleary! All right?
{73819}{73876}That was horrible.
{73880}{73975}But David, you can't|go after a teacher like that here.
{73979}{74033}It's the end if you do.
{74051}{74128}My friends back home|wouldn't believe this.
{74132}{74190}Over a failing grade in French!
{74194}{74307}Good grades. The right schools,|colleges, connections.
{74311}{74377}Those are the keys to the kingdom.
{74381}{74453}None of us goes off|and lives by his wits.
{74457}{74553}We do what they tell us|and they give us the good life.
{74567}{74631}Goddamn hope we like it|when we get it!
{74635}{74760}- What will happen to Mac?|- He's not coming back.
{74849}{74929}Man!|I've heard of nervous breakdowns,
{74933}{75051}but I always thought it happened|to women who were 40 years old.
{75055}{75115}Not to a kid my age.
{75119}{75262}When I was a sophomore,|there was this senior, William Whitton.
{75266}{75352}- He hung himself in the gym.|- Why?
{75387}{75456}- He didn't get into Harvard.|- Shit!
{75460}{75520}- Yeah.|- I want to go to Harvard.
{75524}{75614}I'll be goddamned|if I croak myself if I don't.
{75654}{75705}I envy you.
{75757}{75808}Me? Why?
{75863}{75953}Because if you get what you want,|you'll deserve it.
{75957}{76024}And if you don't, you'll manage.
{76052}{76162}You don't have to live up|to anybody else's expectations.
{76180}{76269}That's what draws people,|not that you're quarterback.
{76273}{76350}You're the most popular guy|on campus.
{76373}{76468}If my name weren't Dillon,|it would be different.
{76484}{76588}- Bullshit.|- David, don't forget my name's Dillon.
{76605}{76668}Son of Grayson Jr,|brother of Grayson III.
{76672}{76753}I'm a Dillon,|a part of those right connections.
{76757}{76822}People don't care about that.
{76846}{76896}You'll see.
{78393}{78443}Evening, sir.
{78766}{78809}Evening, sir.
{80655}{80745}- You don't smoke?|- No, I tried it, but it didn't take.
{80782}{80867}You're too good.|Is that to impress the mothers?
{80871}{80957}- What mothers?|- Of all your girlfriends.
{80961}{81032}No, too many to try.
{81070}{81139}We were talking|about Saturdays in Scranton.
{81148}{81198}Oh, yeah. Garbage day.
{81202}{81267}We had to haul our trash|to the dump.
{81304}{81354}Don't knock it.
{81358}{81425}One day, my father and I|see two guys.
{81429}{81507}One of them's got|a rope around his waist
{81511}{81591}and his buddy is lowering him|into the pit.
{81595}{81691}- What for?|- They were scavenging for tin cans.
{81695}{81763}- Who'd do that?|- That's what I said.
{81774}{81850}And my dad gave me|this long, hard look
{81854}{81940}and he said,|"Davey, it's an honest living."
{82028}{82078}I never forgot that.
{82152}{82241}- You're different from the other boys.|- How?
{82245}{82378}The others, like Dillon, you know|everything about them in two minutes.
{82382}{82451}- But you...|- Four minutes, easy.
{82527}{82586}You have a serious side.
{82717}{82788}Let's go. Move it.|Move it, move it!
{82792}{82860}- Wait.|- Whoa, whoa, whoa, whoa!
{82868}{82922}- Night, David.|- Can I call you?
{82926}{82976}You'd better!
