﻿1
00:00:21,271 --> 00:00:24,148
Starting with a relatively simple idea,

2
00:00:24,274 --> 00:00:28,774
Kafka plunges us into an incoherent,
absurd and surreal world.

3
00:00:29,112 --> 00:00:31,030
The idea is this:

4
00:00:31,156 --> 00:00:35,656
Bureaucrats, the system of administration
and its power crush the individual.

5
00:00:35,994 --> 00:00:38,788
The individual becomes
a choking victim of society

6
00:00:38,914 --> 00:00:40,874
when by chance - or misfortune -

7
00:00:40,999 --> 00:00:44,877
he is drawn
towards the gear of its system.

8
00:00:45,003 --> 00:00:48,673
From an article by Louis Chauvet
(Le Figaro)

9
00:00:55,180 --> 00:00:58,183
THE TRIAL

