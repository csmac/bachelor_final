1
00:02:29,224 --> 00:02:32,421
-Merry Christmas, Captain Ingram.
-Merry Christmas, Evans.

2
00:04:11,826 --> 00:04:14,294
The police told you about the chiId?

3
00:04:15,296 --> 00:04:16,490
Yes, they did.

4
00:04:16,699 --> 00:04:20,293
I'm very sorry, but we'll need
a formal identification.

5
00:04:20,603 --> 00:04:21,627
Yes, of course.

6
00:04:21,836 --> 00:04:23,463
I have to warn you...

7
00:04:24,372 --> 00:04:27,103
...most of the injuries
were to the face, I'm afraid.

8
00:04:28,511 --> 00:04:30,000
The police said...

9
00:04:30,612 --> 00:04:33,137
...that my son died on impact.
Is that correct?

10
00:04:33,348 --> 00:04:34,646
Not quite.

11
00:04:35,451 --> 00:04:37,612
Just after the ambulance arrived.

12
00:04:38,052 --> 00:04:40,247
-How long was that?
-He was unconscious.

13
00:04:40,456 --> 00:04:41,923
He wasn't aware of any pain.

14
00:04:42,124 --> 00:04:43,716
How long, doctor?

15
00:04:44,392 --> 00:04:46,122
Twenty minutes.

16
00:04:52,767 --> 00:04:55,259
-Have you ordered x-rays?
-They're on their way.

17
00:04:56,805 --> 00:04:59,673
-She'll need an urgent CT scan.
-That's organized.

18
00:05:01,410 --> 00:05:03,343
Decerebrate response?

19
00:05:03,778 --> 00:05:05,177
It's her husband.

20
00:05:08,684 --> 00:05:10,584
I want you to talk to her.

21
00:05:15,423 --> 00:05:16,982
Rae, it's John.

22
00:05:22,564 --> 00:05:23,896
Try it again.

23
00:05:24,567 --> 00:05:26,091
Rae, it's me.

24
00:05:27,970 --> 00:05:29,630
It's John, Rae.

25
00:05:37,779 --> 00:05:41,978
Down came the rain
And washed poor Wincy out

26
00:05:43,084 --> 00:05:47,454
Out came the sunshine
And dried up all the rain

27
00:05:47,956 --> 00:05:50,824
And incy-wincy spider

28
00:05:51,026 --> 00:05:53,255
Crawled up the spout again

29
00:05:54,163 --> 00:05:56,062
That's a funny one.

30
00:05:56,564 --> 00:05:58,259
Will you try it with Mummy now?

31
00:05:58,666 --> 00:05:59,759
No.

32
00:06:00,134 --> 00:06:01,625
Come on.

33
00:06:03,038 --> 00:06:07,168
Incy-wincy spider

34
00:06:07,375 --> 00:06:09,901
Crawled up the water....

35
00:06:10,112 --> 00:06:11,579
Spout!

36
00:06:13,014 --> 00:06:15,642
Down came the rain

37
00:06:17,151 --> 00:06:19,814
And washed poor Wincy out

38
00:06:22,757 --> 00:06:26,387
-Out came the sunshine
-He fell down!

39
00:06:26,595 --> 00:06:29,529
And dried up all the rain

40
00:06:30,298 --> 00:06:33,790
-Danny, darling, get back in your seat.
-I've got him.

41
00:06:34,002 --> 00:06:35,629
I saved him.

42
00:06:51,420 --> 00:06:52,944
No!

43
00:06:54,156 --> 00:06:55,144
No!

44
00:06:58,326 --> 00:07:00,420
It's okay. It's all right.

45
00:07:05,733 --> 00:07:07,031
I was holding him!

46
00:07:07,235 --> 00:07:09,136
I know, I know.

47
00:07:10,105 --> 00:07:11,867
John, I could smell his head.

48
00:07:12,074 --> 00:07:13,302
Stop it, Rae. Stop it.

49
00:07:13,509 --> 00:07:15,601
No, I can't!

50
00:07:16,377 --> 00:07:18,778
I'm so scared.

51
00:07:21,716 --> 00:07:23,309
I'm so scared.

52
00:07:23,519 --> 00:07:26,487
He's gone. He's gone, baby.

53
00:07:26,689 --> 00:07:28,713
We can never get him back.

54
00:07:31,026 --> 00:07:33,459
It's gonna take a long time.

55
00:07:35,163 --> 00:07:37,358
Now, you know that. They warned us.

56
00:07:38,867 --> 00:07:40,596
Let's go home.

57
00:07:41,569 --> 00:07:43,800
-Please?
-It'll be worse there.

58
00:07:44,005 --> 00:07:46,872
-I don't think I can get rid of it.
-Yes, you can.

59
00:07:47,076 --> 00:07:48,509
No, I can't.

60
00:07:48,911 --> 00:07:50,639
Don't say that.

61
00:07:50,846 --> 00:07:52,211
Now look...

62
00:07:54,983 --> 00:07:57,178
...we've got weeks and weeks.

63
00:07:58,187 --> 00:08:01,088
Calm days, calm sea.

64
00:08:01,423 --> 00:08:03,687
And we're gonna get strong.

65
00:08:03,992 --> 00:08:06,120
And when you're strong...

66
00:08:06,327 --> 00:08:09,422
...then we'll go home
and we'll start again.

67
00:08:09,630 --> 00:08:11,120
Won't we, Rae?

68
00:08:12,567 --> 00:08:15,093
Say it, Rae. Won't we?

69
00:08:18,574 --> 00:08:20,973
Say it, Rae. Won't we?

70
00:09:00,282 --> 00:09:01,647
What's wrong, Ben?

71
00:09:24,907 --> 00:09:26,033
Come, mate.

72
00:09:45,827 --> 00:09:47,192
What was it?

73
00:09:48,096 --> 00:09:49,860
It's nothing. Just the dawn.

74
00:09:50,399 --> 00:09:52,128
Is there any wind?

75
00:09:54,669 --> 00:09:56,068
Not a whisper.

76
00:10:47,923 --> 00:10:48,947
So...

77
00:10:49,390 --> 00:10:51,221
...what day is it today?

78
00:10:51,426 --> 00:10:55,225
Thursday or Friday.
One of those days with an "R" in it.

79
00:10:59,134 --> 00:11:00,692
Look at that.

80
00:11:01,602 --> 00:11:03,662
First boat in three weeks.

81
00:11:11,078 --> 00:11:13,274
She's been through some weather,
that's for sure.

82
00:11:14,582 --> 00:11:16,413
Is there anybody on deck?

83
00:11:16,618 --> 00:11:18,448
Not that I can see.

84
00:11:18,653 --> 00:11:19,916
I'll call them up.

85
00:11:21,023 --> 00:11:22,456
Do you have to?

86
00:11:23,057 --> 00:11:24,821
I like it like this.

87
00:11:25,293 --> 00:11:27,023
Just the two of us.

88
00:11:27,428 --> 00:11:28,793
So do I.

89
00:11:30,399 --> 00:11:31,865
But look at her, Rae.

90
00:11:32,900 --> 00:11:34,629
That boat's in trouble.

91
00:11:57,326 --> 00:12:00,158
Black schooner to my east.
Black schooner to my east.

92
00:12:00,361 --> 00:12:03,820
This is yacht Saracen,
Victor IKiIo 2 7 62.

93
00:12:04,032 --> 00:12:08,866
Position 1 61 6 south, 1 7 64 7 west.

94
00:12:09,071 --> 00:12:10,971
Do you read me? Over.

95
00:12:53,048 --> 00:12:54,037
John!

96
00:13:00,522 --> 00:13:01,510
John!

97
00:13:04,158 --> 00:13:07,094
I thought I saw something
between here and the boat.

98
00:13:07,696 --> 00:13:09,186
What is it? A turtle?

99
00:13:09,898 --> 00:13:11,990
No. It's bigger.

100
00:13:19,206 --> 00:13:21,265
-You see anything?
-Not yet. Hold on.

101
00:13:23,945 --> 00:13:25,309
There it is.

102
00:13:26,715 --> 00:13:28,148
It's a dinghy.

103
00:13:29,083 --> 00:13:30,072
Adrift?

104
00:13:31,186 --> 00:13:32,846
No, there's a man in it.

105
00:14:18,066 --> 00:14:20,966
Save it! We'll drop down to you!

106
00:14:28,876 --> 00:14:31,243
You hear me? Ship your oars!

107
00:14:31,679 --> 00:14:33,169
Get the fender, darling.

108
00:14:36,051 --> 00:14:36,948
Stand off!

109
00:14:44,326 --> 00:14:45,691
Take it easy.

110
00:15:15,389 --> 00:15:18,018
Get him some water, darling.
Not too much.

111
00:15:20,128 --> 00:15:22,119
-Here you are.
-Thanks.

112
00:15:27,501 --> 00:15:28,969
My name is Hughie Warriner.

113
00:15:31,840 --> 00:15:34,000
John Ingram. My wife, Rae.

114
00:15:35,576 --> 00:15:39,206
I'm 32 days outward bound from Papeete.
I'm heading for Fiji.

115
00:15:42,384 --> 00:15:44,648
I onIy bought her
about three months ago.

116
00:15:45,220 --> 00:15:49,782
My plan had been to cruise around
the Pacific, to sail around the Pacific.

117
00:15:50,325 --> 00:15:51,883
Who would have thought, right?

118
00:15:52,427 --> 00:15:53,758
What's your trouble?

119
00:15:53,961 --> 00:15:56,522
Trouble is she's going down,
that's the trouble.

120
00:15:57,065 --> 00:15:59,328
She's not gonna last the morning.

121
00:16:00,501 --> 00:16:01,399
Are you sure?

122
00:16:01,802 --> 00:16:03,668
Yeah, she opened up all over.

123
00:16:05,307 --> 00:16:07,001
Has the water hit your engine?

124
00:16:07,208 --> 00:16:08,335
My engine...

125
00:16:08,543 --> 00:16:13,071
...the radio, the fucking
bulkhead galleys.

126
00:16:13,280 --> 00:16:14,269
Everything.

127
00:16:16,051 --> 00:16:18,383
My onIy chance was
reaching you in the dinghy.

128
00:16:21,755 --> 00:16:25,851
See what Mr. Warriner needs, darling,
and get him to bed.

129
00:16:26,995 --> 00:16:28,052
All right.

130
00:16:28,596 --> 00:16:29,620
Where are you going?

131
00:16:31,032 --> 00:16:33,966
Well, I ought to get over there.

132
00:16:34,870 --> 00:16:36,427
How many people on board?

133
00:16:36,638 --> 00:16:39,129
Nobody. I'm alone. I'm it.

134
00:16:41,042 --> 00:16:45,068
You tried to take her across
the Pacific on your own?

135
00:16:45,647 --> 00:16:46,635
No.

136
00:16:50,118 --> 00:16:51,982
There were six of us.

137
00:16:55,557 --> 00:16:57,649
The others died 1 0 days ago.

138
00:17:01,328 --> 00:17:02,956
One by one.

139
00:17:03,498 --> 00:17:05,397
It all happened in a day.

140
00:17:30,057 --> 00:17:30,990
What happened?

141
00:17:33,228 --> 00:17:37,130
Have you ever thought about how life
can hinge on the smallest thing?

142
00:17:40,367 --> 00:17:43,666
I mean, ever since I was a kid,
I thought salmon was for cats.

143
00:17:44,372 --> 00:17:45,999
Was it canned salmon?

144
00:17:46,907 --> 00:17:47,896
Yeah.

145
00:17:52,314 --> 00:17:55,750
At first we thought it was
the tourist trots...

146
00:17:56,917 --> 00:18:00,615
...until Chantal looked
at her hand and said...

147
00:18:00,821 --> 00:18:02,618
..."Picasso should have painted it."

148
00:18:05,727 --> 00:18:07,354
It had seven fingers.

149
00:18:09,263 --> 00:18:10,788
Double vision, huh?

150
00:18:13,634 --> 00:18:15,432
Sounds like botulism.

151
00:18:16,203 --> 00:18:17,192
What's that?

152
00:18:17,705 --> 00:18:21,300
It's a lethal form of food poisoning.
It hits the nervous system.

153
00:18:22,344 --> 00:18:25,211
Yeah, well, I tried everything.

154
00:18:26,146 --> 00:18:27,239
There's no treatment.

155
00:18:28,817 --> 00:18:32,115
Not on a small boat there's not.
Not 1200 miles from land.

156
00:18:42,096 --> 00:18:43,961
When you're ready, Mr. Warriner....

157
00:18:49,336 --> 00:18:51,669
Better go and salvage your gear.

158
00:18:52,973 --> 00:18:56,000
My gear? You're looking at it.

159
00:18:56,344 --> 00:18:58,505
There's nothing to salvage.

160
00:18:58,712 --> 00:19:01,340
Everything was ruined by the water.

161
00:19:02,851 --> 00:19:04,078
There must be something.

162
00:19:04,752 --> 00:19:08,086
Give me a break.
I mean, you know what I had to do?

163
00:19:08,556 --> 00:19:09,921
I can guess.

164
00:19:10,125 --> 00:19:14,117
They've been dead for days!
You know what it does to a body?

165
00:19:14,328 --> 00:19:16,523
You've gotta try and stop
thinking about it.

166
00:19:16,730 --> 00:19:18,357
I mean, I....

167
00:19:18,566 --> 00:19:23,059
Fuck, I can't go back. All right?
I can't go back there right now.

168
00:19:24,271 --> 00:19:25,260
Ever.

169
00:19:33,080 --> 00:19:34,912
That's all right. I understand.

170
00:19:36,718 --> 00:19:39,881
Why don't you go to bed
and get as much sleep as you can?

171
00:19:41,756 --> 00:19:42,586
Thank you.

172
00:19:52,267 --> 00:19:55,827
Look, I'm really sorry
to ruin your day, you know?

173
00:19:57,337 --> 00:19:59,897
-Here?
-Yeah, straight ahead.

174
00:20:00,107 --> 00:20:03,338
Good night. Thanks again.

175
00:20:52,961 --> 00:20:54,086
You all right?

176
00:20:54,596 --> 00:20:55,585
Yeah.

177
00:20:56,131 --> 00:20:56,961
I'm fine.

178
00:20:57,164 --> 00:20:58,222
You sure?

179
00:21:05,240 --> 00:21:07,070
Some interesting stuff here.

180
00:21:08,209 --> 00:21:09,675
Listen to this.

181
00:21:10,345 --> 00:21:14,280
"South Sea Island Cruise. Four young
women wanted for photo assignment.

182
00:21:14,481 --> 00:21:17,178
Must be attractive and broad-minded."

183
00:21:20,387 --> 00:21:22,548
You don't like him, do you?

184
00:21:24,291 --> 00:21:25,759
I hadn't thought about it.

185
00:21:25,960 --> 00:21:28,519
John, imagine what he's been through.

186
00:21:28,730 --> 00:21:31,130
I'm sorry, Rae,
whichever way I turn it...

187
00:21:32,099 --> 00:21:33,567
...I just can't swallow it.

188
00:21:33,768 --> 00:21:35,234
Any reason?

189
00:21:35,435 --> 00:21:36,868
Not really. Just...

190
00:21:37,070 --> 00:21:38,867
...25 years at sea.

191
00:22:05,933 --> 00:22:07,458
I'm going onboard her.

192
00:22:07,669 --> 00:22:09,067
You can't do that.

193
00:22:09,269 --> 00:22:11,134
He's fast asleep. He won't even know.

194
00:22:11,338 --> 00:22:13,135
He will. He'll hear the engine.

195
00:22:13,441 --> 00:22:15,465
No, he won't. I'll take the dinghy.

196
00:22:16,310 --> 00:22:18,074
Load the gun and keep it with you.

197
00:22:18,278 --> 00:22:19,644
Are you serious?

198
00:22:19,847 --> 00:22:21,873
Just do as I say, please.

199
00:22:40,602 --> 00:22:41,728
Back soon.

200
00:22:41,935 --> 00:22:42,924
Bye.

201
00:22:52,680 --> 00:22:54,375
Wait a sec.

202
00:22:56,817 --> 00:22:58,410
Go on, take it.

203
00:23:11,865 --> 00:23:13,766
One, two....

204
00:23:24,311 --> 00:23:25,300
Okay.

205
00:23:26,146 --> 00:23:27,239
Go!

206
00:23:32,819 --> 00:23:34,048
Bring it back. Come on.

207
00:26:23,790 --> 00:26:24,779
Rae!

208
00:26:27,028 --> 00:26:30,862
Rae, I'm sick!
I gotta use the bathroom!

209
00:26:31,432 --> 00:26:34,162
Rae, I'm nauseated! Please!

210
00:26:37,637 --> 00:26:39,628
I don't wanna throw up in here!

211
00:26:40,974 --> 00:26:41,963
Rae!

212
00:29:12,058 --> 00:29:14,027
Jump, Rae! Jump!

213
00:29:16,063 --> 00:29:17,189
Jump!

214
00:29:18,531 --> 00:29:20,659
No! What are you doing?

215
00:29:26,007 --> 00:29:26,996
No!

216
00:30:05,712 --> 00:30:07,681
Jump, Rae! Jump!

217
00:30:15,722 --> 00:30:17,054
Get off, Rae!

218
00:30:17,258 --> 00:30:18,884
Leave it!

219
00:34:18,264 --> 00:34:19,391
Ben.

220
00:35:07,347 --> 00:35:08,644
Where's John?

221
00:35:15,755 --> 00:35:16,688
Now, what?

222
00:35:16,956 --> 00:35:18,947
John, my husband.

223
00:35:21,795 --> 00:35:22,853
Where is he?

224
00:35:24,030 --> 00:35:25,463
He's gone.

225
00:35:30,003 --> 00:35:31,561
What have you done to him?

226
00:35:33,606 --> 00:35:35,097
Nothing.

227
00:35:35,675 --> 00:35:37,507
He went onboard her.

228
00:35:39,179 --> 00:35:40,543
We've gotta go back.

229
00:35:41,114 --> 00:35:43,811
He's onIy got himseIf to blame, right?

230
00:35:44,018 --> 00:35:45,985
I mean, he should've trusted me.

231
00:35:46,219 --> 00:35:48,780
But that's always been my problem.

232
00:35:48,989 --> 00:35:52,048
-Don't you understand?
-Please don't shout.

233
00:35:52,358 --> 00:35:54,690
That's always been one of my problems.

234
00:35:54,894 --> 00:35:58,295
I can never tell people's motives
until it's too late.

235
00:35:58,498 --> 00:36:02,628
Let's just turn the boat around.

236
00:36:03,836 --> 00:36:06,237
Where shall we go?

237
00:36:06,440 --> 00:36:09,773
-Turn around!
-Look, you're being very aggressive.

238
00:36:09,976 --> 00:36:13,413
That could be a real problem
on this small boat.

239
00:36:30,297 --> 00:36:32,925
I was watching you
when you were sleeping.

240
00:36:34,434 --> 00:36:37,164
And I gotta tell you
that your face fascinates me.

241
00:36:40,574 --> 00:36:44,202
Even when you're 80, Rae,
you'll still be a beautiful woman.

242
00:36:50,117 --> 00:36:52,277
I studied art...

243
00:36:54,221 --> 00:36:55,619
...for a time.

244
00:36:56,356 --> 00:36:59,722
I found that painters,
when they approach their subjects...

245
00:36:59,927 --> 00:37:03,954
...they always look
at the face from behind.

246
00:37:04,164 --> 00:37:06,826
From the back,
to see what's holding it up.

247
00:37:09,635 --> 00:37:12,365
And you've got magnificent
bone structure.

248
00:37:19,545 --> 00:37:22,275
"Magnificent."
Shit, that sounds like something...

249
00:37:22,483 --> 00:37:24,280
...you'd say at a cocktail party.

250
00:37:26,119 --> 00:37:28,452
Should have given you my card,
for chrissake.

251
00:37:32,059 --> 00:37:34,721
I just knew we'd get along,
didn't you?

252
00:37:41,335 --> 00:37:42,597
What's wrong?

253
00:37:44,570 --> 00:37:47,005
Would you just tell me one thing?

254
00:37:50,309 --> 00:37:53,335
I just need to know
if that boat is sinking.

255
00:37:54,547 --> 00:37:55,844
Sinking?

256
00:37:57,251 --> 00:37:58,878
Well, past tense would do.

257
00:38:00,853 --> 00:38:03,083
But, yeah. Yes, it is.

258
00:38:03,456 --> 00:38:06,483
Why? Don't you believe me?

259
00:38:07,827 --> 00:38:10,728
Yeah, I believe you.
Of course I do.

260
00:38:10,931 --> 00:38:13,057
-That's why we've gotta go back.
-Back?

261
00:38:13,766 --> 00:38:15,597
-To get John.
-You mean, back there?

262
00:38:17,637 --> 00:38:18,934
Right now.

263
00:38:20,974 --> 00:38:23,670
No, Rae, no. Okay?
That's over.

264
00:38:23,876 --> 00:38:27,710
That's over. So let's erase that
one from memory and start again.

265
00:38:27,914 --> 00:38:29,677
There's no going back!

266
00:38:30,818 --> 00:38:32,648
No, don't say that.

267
00:38:32,853 --> 00:38:34,251
He's gonna drown!

268
00:38:34,621 --> 00:38:36,521
Now you're ruining it again.

269
00:38:36,722 --> 00:38:38,418
That boat is sinking!

270
00:38:38,692 --> 00:38:40,090
That's not my fault!

271
00:38:42,329 --> 00:38:43,853
What about those people?

272
00:38:47,134 --> 00:38:49,398
There wasn't any food poisoning,
was there?

273
00:38:55,108 --> 00:38:56,905
You wanna do this now?

274
00:38:58,110 --> 00:38:59,442
All right.

275
00:39:00,313 --> 00:39:02,509
They tried to kill me, Rae.

276
00:39:02,815 --> 00:39:06,411
They tried to suck the light out
of me, if you can grasp that.

277
00:39:06,619 --> 00:39:07,449
Who were?

278
00:39:07,653 --> 00:39:08,746
All of them!

279
00:39:10,757 --> 00:39:12,554
Nobody wants to kill you--

280
00:39:12,759 --> 00:39:13,588
What?

281
00:39:14,126 --> 00:39:15,492
Repeat that.

282
00:39:15,829 --> 00:39:18,764
-I said, nobody--
-Say the words. Come on!

283
00:39:18,965 --> 00:39:21,161
You mean I just imagined it.

284
00:39:23,103 --> 00:39:25,003
I just meant it must be a mistake.

285
00:39:25,204 --> 00:39:27,139
A mistake? Lady...

286
00:39:27,340 --> 00:39:30,469
...the mistake is that
you think I'm making this up!

287
00:39:30,777 --> 00:39:31,835
No, I don't.

288
00:39:32,179 --> 00:39:35,170
You sound so much like them
it's scary!

289
00:39:36,083 --> 00:39:39,313
Now, they were trying to kill me.
Do you understand?

290
00:39:42,989 --> 00:39:44,820
Yeah, I understand.

291
00:41:53,219 --> 00:41:56,677
Turn around. Don't look
at the bloody boat. Look at me!

292
00:41:56,889 --> 00:41:58,914
Okay, I'm sorry.

293
00:42:03,597 --> 00:42:07,123
What are you doing, Hughie?
Go away!

294
00:42:07,900 --> 00:42:12,463
-Okay, ladies, forget the home movie.
-Listen, if you turn around...

295
00:42:12,672 --> 00:42:14,536
...you'll get incredible silhouettes.

296
00:42:14,740 --> 00:42:19,143
When I want your advice on how
to take photographs, I'll ask you.

297
00:42:19,780 --> 00:42:22,304
I'll remember that,
but for now, Russell, please.

298
00:42:22,516 --> 00:42:26,110
If you move two steps to your left,
I can get you with the boat.

299
00:42:26,318 --> 00:42:28,786
We'll look at this someday and laugh.

300
00:42:29,556 --> 00:42:32,753
For God's sake, Hughie!
Get that bloody camera off me!

301
00:42:32,958 --> 00:42:36,052
I take the pictures, Hughie, baby.
You dance, I shoot.

302
00:42:36,262 --> 00:42:38,628
What the fuck is your problem?

303
00:44:02,481 --> 00:44:04,177
This is stereo, Ben.

304
00:44:07,387 --> 00:44:09,047
Low production...

305
00:44:09,322 --> 00:44:10,846
...garage music.

306
00:44:26,172 --> 00:44:27,434
Come on.

307
00:44:46,525 --> 00:44:50,393
Saracen, Saracen, this is Orpheus.
Do you read me? Over.

308
00:45:03,043 --> 00:45:04,567
Saracen, this is--

309
00:45:27,467 --> 00:45:28,434
Yes!

310
00:45:30,436 --> 00:45:31,630
John?

311
00:45:33,072 --> 00:45:34,005
Are you okay?

312
00:45:35,909 --> 00:45:37,137
Are you there?

313
00:45:38,143 --> 00:45:41,579
Yes, Rae, I'm here.
Can you hear me?

314
00:45:42,715 --> 00:45:44,342
Are you there?

315
00:45:46,186 --> 00:45:48,016
Rae, it's me.

316
00:45:50,923 --> 00:45:52,652
I'm not reading you.

317
00:45:56,463 --> 00:45:58,327
Is that you, John?

318
00:46:03,902 --> 00:46:05,130
Are you hurt?

319
00:46:09,141 --> 00:46:11,838
What about the boat?
It's sinking, isn't it?

320
00:46:16,483 --> 00:46:17,710
So that's it.

321
00:46:17,916 --> 00:46:22,376
No, you're not hurt and, no, the boat
isn't sinking? Is that right?

322
00:46:26,893 --> 00:46:28,485
Are you following?

323
00:46:32,998 --> 00:46:34,193
What should I do?

324
00:46:35,034 --> 00:46:38,003
I can't make him turn around.
He just won't.

325
00:46:39,706 --> 00:46:41,867
Do you want me to stop the boat?

326
00:46:46,179 --> 00:46:48,409
I don't know how.

327
00:46:50,250 --> 00:46:51,079
Come on.

328
00:46:55,722 --> 00:46:59,487
Don't worry. I'll work it out.

329
00:47:09,768 --> 00:47:12,135
You'll be here soon, won't you?

330
00:49:40,119 --> 00:49:44,146
Score one for Mrs. Ingram. She has
a hell of a season ahead of her.

331
00:49:51,563 --> 00:49:52,530
Ben, no!

332
00:49:57,170 --> 00:49:58,034
Leave it!

333
00:50:00,472 --> 00:50:01,302
Leave it there.

334
00:50:01,541 --> 00:50:03,269
Fetch, Ben.
Bring it here, boy.

335
00:50:03,476 --> 00:50:05,306
-Bring it here.
-Stay, Ben.

336
00:50:05,512 --> 00:50:06,876
Come on.

337
00:50:08,081 --> 00:50:10,208
Okay, drop it. Drop the key.

338
00:50:10,416 --> 00:50:11,440
Hold the key.

339
00:50:11,650 --> 00:50:12,878
-No, drop it!
-IKeep it.

340
00:50:13,085 --> 00:50:14,211
Over here. Come on.

341
00:50:15,788 --> 00:50:16,617
Tread.

342
00:50:17,257 --> 00:50:18,518
Backstroke.

343
00:50:20,226 --> 00:50:21,420
Come on.

344
00:50:27,500 --> 00:50:30,059
That's beautiful bone structure there.

345
00:50:40,847 --> 00:50:43,338
Good boy!
He's a champ, this one.

346
00:50:43,615 --> 00:50:46,311
Jump, Ben. Up.
Come on, climb.

347
00:51:06,105 --> 00:51:07,367
Thank you.

348
00:52:29,755 --> 00:52:31,018
Friends?

349
00:52:37,697 --> 00:52:39,027
Friends.

350
00:52:53,545 --> 00:52:58,414
-Talk to me, Russell.
-Come on. The camera's rolling.

351
00:52:58,617 --> 00:53:02,144
Lose that! Talk to me, Russell.
Who are you?

352
00:53:02,355 --> 00:53:03,481
Come on, drop the veil.

353
00:53:03,690 --> 00:53:06,284
-Drop the veil, sucker!
-I got the picture.

354
00:53:06,492 --> 00:53:07,686
I got the picture.

355
00:53:07,927 --> 00:53:11,623
Vietnam, Laos, Castro, Cambodia!

356
00:53:11,831 --> 00:53:14,856
Russell Bellows,
big brave war photographer.

357
00:53:15,068 --> 00:53:18,969
The man on the edge,
capturing the face of death.

358
00:53:19,172 --> 00:53:23,733
But now appearing
with his five new refugees.

359
00:53:23,943 --> 00:53:27,572
Five suckers on a sinking boat.
The ultimate session...

360
00:53:27,780 --> 00:53:32,342
...of moral decay. What 60-minute
wet dreams are made of. Am I right?

361
00:53:32,918 --> 00:53:35,251
Come on in, Russ.
The water's great.

362
00:53:35,454 --> 00:53:38,856
You got the bloated head.
How about the bloated belly?

363
00:53:40,259 --> 00:53:42,523
Come out from behind that camera.

364
00:55:01,239 --> 00:55:02,867
John, can you hear me?

365
00:55:07,045 --> 00:55:10,344
Saracen to the black schooner.
Come in, please.

366
00:55:13,920 --> 00:55:15,751
Come in, please.

367
00:55:22,128 --> 00:55:23,719
John, it's me.

368
00:55:25,565 --> 00:55:28,362
I got the key up
onto the deck, but he....

369
00:55:33,806 --> 00:55:35,831
What's wrong? Are you hurt?

370
00:55:40,313 --> 00:55:41,574
Is it the boat?

371
00:55:43,748 --> 00:55:45,717
Is it sinking?

372
00:55:52,592 --> 00:55:53,489
How long?

373
00:55:53,726 --> 00:55:55,125
Hours?

374
00:55:58,864 --> 00:56:00,559
How many? Two?

375
00:56:05,338 --> 00:56:06,532
Four?

376
00:56:10,076 --> 00:56:11,269
Six?

377
00:56:12,478 --> 00:56:15,777
I drain you of your power, Russell!
You've lost.

378
00:56:16,648 --> 00:56:19,117
Good night! Good night!

379
00:56:22,153 --> 00:56:23,280
Six?

380
00:56:34,699 --> 00:56:36,998
I'm gonna be there by sunset.

381
00:56:58,858 --> 00:57:00,449
I love you.

382
00:57:17,943 --> 00:57:20,742
Answer me, please. Answer me.

383
00:57:24,317 --> 00:57:26,217
Do you read me?

384
00:57:31,090 --> 00:57:33,023
Please.

385
01:01:05,137 --> 01:01:07,697
I've just gotta go to the bathroom.

386
01:01:11,076 --> 01:01:12,235
Sorry.

387
01:01:13,579 --> 01:01:16,480
You gotta do what you gotta do.

388
01:02:13,472 --> 01:02:17,067
-For later.
-I was gonna say, you read my mind.

389
01:02:37,128 --> 01:02:38,721
Friends, Rae.

390
01:02:52,878 --> 01:02:56,041
-I better put him up on the deck.
-Don't worry. Get out!

391
01:02:57,583 --> 01:02:59,175
-I better put him out.
-Come on!

392
01:03:00,686 --> 01:03:01,811
Go away.

393
01:06:00,498 --> 01:06:01,795
Where are you going?

394
01:06:02,000 --> 01:06:03,398
To get a drink.

395
01:06:23,856 --> 01:06:25,516
Isn't this great?

396
01:06:26,525 --> 01:06:29,085
-What?
-These lemons, they're just....

397
01:06:29,994 --> 01:06:33,021
Cruising around the South Pacific,
just the two of us.

398
01:06:38,503 --> 01:06:39,731
How do I look?

399
01:06:41,005 --> 01:06:42,996
-Great.
-How do I smell?

400
01:06:44,710 --> 01:06:46,735
Thank you for your honesty.

401
01:06:57,422 --> 01:07:02,188
What's missing from my life at this
moment are the soothing sounds...

402
01:07:03,594 --> 01:07:07,190
...of Julio Iglesias, Joni Mitchell
and the Broadway AIbum.

403
01:07:07,398 --> 01:07:10,391
Did you get these tapes
thrown in free with the knives?

404
01:07:11,335 --> 01:07:12,427
He finds it!

405
01:08:11,295 --> 01:08:12,126
That looks good.

406
01:08:14,498 --> 01:08:15,989
It is.

407
01:08:19,570 --> 01:08:20,935
Do you want some?

408
01:08:21,139 --> 01:08:22,605
Come on.

409
01:08:23,140 --> 01:08:24,471
You do. Come on, have it.

410
01:08:24,676 --> 01:08:25,734
Now, look....

411
01:08:26,144 --> 01:08:27,202
Do you want it?

412
01:08:27,845 --> 01:08:28,743
I want it.

413
01:08:28,980 --> 01:08:29,970
Thank you.

414
01:08:33,952 --> 01:08:35,748
I better go and get dressed.

415
01:08:37,521 --> 01:08:40,082
IKeep it. Make me another one.

416
01:09:19,331 --> 01:09:20,764
Room service.

417
01:09:28,273 --> 01:09:30,764
A squall's coming.
Better get the canopy down.

418
01:10:50,287 --> 01:10:51,118
Oh, shit.

419
01:11:01,632 --> 01:11:03,328
Friends, Rae.

420
01:11:04,069 --> 01:11:05,331
Friends!

421
01:23:23,006 --> 01:23:24,201
Don't make me.

422
01:23:25,743 --> 01:23:27,801
No one's gonna make you.

423
01:23:28,412 --> 01:23:29,640
Stay there!

424
01:23:29,947 --> 01:23:31,743
I just wanna talk!

425
01:28:04,889 --> 01:28:07,015
I found you.

426
01:30:40,109 --> 01:30:41,975
Fresh water.

427
01:30:42,680 --> 01:30:44,545
That's extravagant.

428
01:30:45,282 --> 01:30:47,546
No, you wanna hear extravagant?

429
01:30:50,253 --> 01:30:52,188
A steaming hot bath...

430
01:30:53,256 --> 01:30:55,953
...that you can slip into
up to your ears...

431
01:30:56,493 --> 01:30:58,926
...with coffee and a croissant.

432
01:30:59,162 --> 01:31:00,891
A croissant.

433
01:31:01,364 --> 01:31:02,662
Covered...

434
01:31:03,233 --> 01:31:05,792
...in bitter, dark marmalade.

435
01:31:07,570 --> 01:31:09,265
And mango.

436
01:31:09,472 --> 01:31:11,497
Really, really cold.

437
01:31:11,908 --> 01:31:13,569
Now you're talking.

438
01:31:57,988 --> 01:32:00,512
You know what I'd love for Iunch?

439
01:32:01,658 --> 01:32:04,182
Fresh asparagus.

440
01:32:07,597 --> 01:32:09,259
Then pasta.

441
01:32:09,899 --> 01:32:12,198
AngeI-hair pasta.

442
01:32:13,103 --> 01:32:17,971
With heaps of basiI
and garlic and olive oil.

443
01:32:23,012 --> 01:32:24,742
And apple pie.

444
01:32:29,886 --> 01:32:31,615
Have you got the toweI?

