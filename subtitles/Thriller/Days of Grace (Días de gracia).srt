﻿
1
00:47:38,362 --> 00:47:39,954
Please have pity.

2
00:47:42,042 --> 00:47:43,953
In the name of God.

3
00:50:24,151 --> 00:50:25,693
You idiot!

4
00:48:22,762 --> 00:48:26,152
Come out or we'll kill you all!

5
00:48:28,482 --> 00:48:30,950
Don't shoot! Don't shoot!

6
00:48:39,322 --> 00:48:41,199
We're Ossetian!

7
00:48:41,322 --> 00:48:43,358
We are friends!

8
00:49:14,962 --> 00:49:16,554
This is all they had on them.

9
00:49:16,682 --> 00:49:19,150
We found a second camera in the car.

10
00:50:43,562 --> 00:50:45,951
I can make the women talk.

11
00:50:46,082 --> 00:50:48,232
You will not touch my daughters!

12
00:50:48,362 --> 00:50:51,957
They are Ossetians like me.

13
00:50:52,082 --> 00:50:54,642
We are like you.

14
00:50:54,762 --> 00:50:56,673
I am a Cossack, old man.

15
00:50:57,362 --> 00:51:01,321
And the next time
you raise your voice at me,

16
00:51:01,442 --> 00:51:04,479
I will cut you open
while your daughters watch.

17
00:51:04,602 --> 00:51:07,321
Daniil. Daniil...

18
00:51:07,442 --> 00:51:09,000
Take the father first.

19
00:51:09,122 --> 00:51:10,714
No, you can't do this!

20
00:51:17,882 --> 00:51:19,873
If there is a video recording,
we have to find it.

21
00:51:20,002 --> 00:51:24,518
We don't need a PR nightmare
like the Americans in Iraq.

22
00:51:24,642 --> 00:51:26,633
Don't worry, we'll find it.

23
00:51:26,762 --> 00:51:30,437
Sir, we just received word that
the second invasion front has launched.

24
00:51:30,562 --> 00:51:31,836
Good.

25
00:58:47,104 --> 00:58:50,141
They already know everything.

26
00:58:50,264 --> 00:58:52,141
Where is the recording? Tell me!

27
00:58:55,944 --> 00:58:58,412
Tatia, do not play games with me!

28
00:58:58,544 --> 00:59:01,581
These men may be Ossetians,

29
00:59:01,704 --> 00:59:04,138
but they will not wait forever.

30
00:59:27,344 --> 00:59:29,904
May I begin, Colonel?

31
00:59:30,024 --> 00:59:31,616
Hold on.

32
00:59:35,744 --> 00:59:37,416
We must think of ourselves now.

33
00:59:37,544 --> 00:59:39,421
Not two strangers!

34
00:59:40,144 --> 00:59:44,057
You think they wouldn't leave us?
They will abandon us like the world has!

35
00:59:45,584 --> 00:59:48,496
Tatia, tell me where the recording is?

36
01:01:40,944 --> 01:01:42,935
We can use them as shields!

37
01:01:56,664 --> 01:01:57,460
Tatia!

38
01:01:58,584 --> 01:02:00,973
No, I can't trust you.

39
01:02:01,104 --> 01:02:03,982
Tatia, I'm your father...

40
01:02:04,104 --> 01:02:06,572
They need my help.

41
01:02:09,344 --> 01:02:12,814
No. You'll be safer with your friends.

42
01:02:17,984 --> 01:02:19,383
Medic!

43
01:02:21,024 --> 01:02:24,460
- Are you okay?
- It's nothing... just my arm.

44
01:02:24,584 --> 01:02:26,222
Medic I said!

45
01:02:26,344 --> 01:02:29,177
Get them!

46
01:02:58,504 --> 01:03:00,495
Get the vehicles started!

47
01:05:14,744 --> 01:05:16,382
That guy's crazy.

48
01:12:02,464 --> 01:12:04,739
You're abandoning your post?

49
01:12:04,864 --> 01:12:06,775
By order of the President.

50
01:12:06,904 --> 01:12:08,815
All troops are to fall back

51
01:12:08,944 --> 01:12:12,141
and report to defend Tbilisi.

52
01:12:13,184 --> 01:12:14,776
Gori is going to fall.

53
01:12:14,904 --> 01:12:18,453
I'm sorry. I have my orders.

54
01:16:13,504 --> 01:16:14,698
Friendlies!

55
01:16:16,384 --> 01:16:17,863
Hold your fire!

56
01:16:25,664 --> 01:16:27,575
Have you seen ground troops?

57
01:16:27,704 --> 01:16:29,615
Hear that?

58
01:16:36,104 --> 01:16:37,981
Defensive positions!

59
01:16:38,104 --> 01:16:39,503
Move out!

60
01:32:55,184 --> 01:32:58,062
Don't resist, Captain.

61
01:33:44,944 --> 01:33:46,855
Sir, they're not resisting.

62
01:38:42,384 --> 01:38:47,458
My husband, Mikheil Taboshvili.
Killed in Eredvi village.

63
01:38:47,584 --> 01:38:49,176
My father.

64
01:38:50,264 --> 01:38:54,098
My mother-in-law, Natalia Okropiridze.

65
01:38:54,224 --> 01:38:57,022
My mother, Natalia Okropiridze.

66
01:38:57,664 --> 01:39:03,022
As my parents were being evacuated
and they were crossing the road,

67
01:39:04,184 --> 01:39:10,054
a mine exploded in front of their car and
shrapnel hit her straight in the temple.

68
01:39:10,184 --> 01:39:12,823
She died on the spot.

69
01:39:15,544 --> 01:39:17,296
Zambakhidze Tamar, my mother.

70
01:39:17,424 --> 01:39:18,573
My wife.

71
01:39:18,704 --> 01:39:20,501
Our grandmother.

72
01:39:21,064 --> 01:39:24,454
My parents were tortured.
Their throats were slit.

73
01:39:27,304 --> 01:39:32,014
They were buried in our backyard,
and that's all we know.

74
01:39:33,784 --> 01:39:36,457
This is Nodar Otiashvili, my father.

75
01:39:37,744 --> 01:39:41,657
We used to live
in Kvemo Achabeti village.

76
01:39:44,024 --> 01:39:46,140
I am the son of Socrate Gogidze.

77
01:39:47,704 --> 01:39:52,175
I was told by the Red Cross

78
01:39:52,304 --> 01:39:54,454
that he was buried in our garden.

79
01:39:57,504 --> 01:39:59,301
He had been hanged.

80
01:40:03,024 --> 01:40:07,381
I am Marina Kakhbiashvili.
I used to live in Kehvi village.

81
01:40:08,744 --> 01:40:11,542
On August 8th, we left.

82
01:40:11,664 --> 01:40:15,179
My husband stayed there.
My son and my husband.

83
01:40:15,304 --> 01:40:19,217
My son left that night.
My husband stayed in the house.

84
01:40:30,464 --> 01:40:32,773
Ekaterine Papelishvili, my grandmother.

85
01:40:42,012 --> 01:40:43,890
Subs made by Paul C