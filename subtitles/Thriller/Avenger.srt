﻿1
00:00:08,355 --> 00:00:10,190
There's stardust over our heads
Zujyou ni wa hoshikuzu

2
00:00:10,190 --> 00:00:12,025
They fall into a bottomless pit
Ochiru wa naraku no soko

3
00:00:12,025 --> 00:00:13,959
When the curtain opens, it is the dark
Makuakishi ankoku

4
00:00:13,994 --> 00:00:15,796
Grand Guignol
GURANGINYORU

5
00:00:15,796 --> 00:00:17,696
Thanatos and Eros...
TANATOSU to EROSU ga

6
00:00:17,731 --> 00:00:19,599
Take one another's hands and dance in circles
Te wo tori odorimeguru?

7
00:00:19,599 --> 00:00:21,468
A carnival of desire
Yokubou no KARUNABARU

8
00:00:21,468 --> 00:00:24,971
Everyone is in the crowd
Daremo ga mure no naka

9
00:00:24,971 --> 00:00:26,873
Being lost in solitude
Kodoku ni fukeri

10
00:00:26,873 --> 00:00:32,470
And seeing forbidden dreams
lkenai yume wo miru

11
00:00:35,549 --> 00:00:39,252
While it has now been deserted,
lma wa haioku

12
00:00:39,252 --> 00:00:43,023
In the pleasure hall of the golden tower,
Ougontou no yuugijyou de

13
00:00:43,023 --> 00:00:46,760
Reminiscing sweet memories
Amaki tsuioku

14
00:00:46,760 --> 00:00:50,662
And searching from deep
inside your mind for them
Dokuro no ganka ni saguru

15
00:00:50,731 --> 00:00:54,501
The rusted short sword
Sabitsuku tanken wo

16
00:00:54,501 --> 00:00:58,267
Is picked up and pointed toward my chest
Hiroiage kono mune ni

17
00:00:58,305 --> 00:01:01,763
And every time it is held up over my head
Mukete kazasu tabi

18
00:01:01,808 --> 00:01:04,311
The red blood
Akaki chi ga

19
00:01:04,311 --> 00:01:09,078
Seethes from the pain of living
lkiru itami ni tagiru

20
00:01:09,249 --> 00:01:11,118
A gorgeous paradise
Gokusai no rakuen

21
00:01:11,118 --> 00:01:13,018
A dictator's garden
Dokusaisya no teien

22
00:01:13,053 --> 00:01:14,888
A moment lasts 1001 nights
Toki wa issen'ichiya

23
00:01:14,888 --> 00:01:16,790
In the capital of evil
Ma no miyako

24
00:01:16,790 --> 00:01:18,621
Within the grotesque city,
GUROTESUKU na machi ni

25
00:01:18,692 --> 00:01:20,455
You're a sleeping princess
Nemureru kimi wa oujyo

26
00:01:20,527 --> 00:01:22,396
The sacrifice of a young girl
Shoujyo SAKURIFAISU

27
00:01:22,396 --> 00:01:25,932
Is made with transparent wings
Toumei na tsubasa de

28
00:01:25,932 --> 00:01:27,734
The innocent soul
Muku naru tamashii

29
00:01:27,734 --> 00:01:31,170
Welcomes the daybreak
Reimei wo mukaeyo

30
00:02:13,313 --> 00:02:17,647
As of now, more than half the cities
have chosen their delegate gladiators.

31
00:02:18,852 --> 00:02:21,521
For the upcoming delegate
gladiator tournament...

32
00:02:21,521 --> 00:02:25,116
Currently, there are no
obstacles to holding it.

33
00:02:26,393 --> 00:02:28,258
What about the Barbaroi in question?

34
00:02:28,295 --> 00:02:31,856
After being spotted in Apollo City,
no information of significance has come in.

35
00:02:33,133 --> 00:02:34,434
And?

36
00:02:34,434 --> 00:02:38,768
There are Barbaroi gladiators
in the outskirts of each city now.

37
00:02:38,872 --> 00:02:41,775
Are you suggesting using them?

38
00:02:41,775 --> 00:02:43,800
What about the bounty?

39
00:02:44,444 --> 00:02:47,072
A letter of recommendation
for a delegate gladiator.

40
00:02:48,782 --> 00:02:50,511
Is something wrong?

41
00:02:54,120 --> 00:02:56,782
No, that'll be just fine.

42
00:03:53,680 --> 00:03:54,977
There's no need to worry.

43
00:04:18,705 --> 00:04:24,735
In front of the red moon,
you always look listless.

44
00:04:37,424 --> 00:04:42,195
The moon reminds me.
Reminds me of that moment.

45
00:04:42,195 --> 00:04:48,001
I have no regrets.
That was the result of logic.

46
00:04:48,001 --> 00:04:51,061
You're so strong, Volk.

47
00:04:51,871 --> 00:04:53,736
That's not it.

48
00:04:55,375 --> 00:04:57,570
I need to be strong.

49
00:04:58,712 --> 00:05:01,581
As I was then. As I am now.

50
00:05:01,581 --> 00:05:04,072
And, as I must be from now on.

51
00:05:28,842 --> 00:05:31,011
If you're going to attack
us while we're asleep,

52
00:05:31,011 --> 00:05:33,206
at least do a better job
of hiding your presence.

53
00:05:41,821 --> 00:05:46,226
So, it's coming again.
The delegate gladiator tournament.

54
00:05:46,226 --> 00:05:50,630
Limited natural resources, water, food.

55
00:05:50,630 --> 00:05:55,402
Everyone is offered
"The Abundant Fairness."

56
00:05:55,402 --> 00:05:58,171
If that's just a story presented in a dream,

57
00:05:58,171 --> 00:06:01,732
then the most reasonable way
to divide the resources is...

58
00:06:01,841 --> 00:06:03,069
Strength.

59
00:06:04,811 --> 00:06:08,008
That's what the delegate
galdiator tournament is for.

60
00:06:08,114 --> 00:06:12,949
Is that "right", Volk?

61
00:06:13,820 --> 00:06:17,483
It's not about what's "right"
and what's "wrong."

62
00:06:20,860 --> 00:06:24,125
If there's a path, then I'm just taking it.

63
00:06:28,301 --> 00:06:30,667
Sorry to have disturbed you.

64
00:06:33,873 --> 00:06:37,570
Vesta, there was a report from Serena...

65
00:06:37,610 --> 00:06:40,238
...concerning the doll
and the female Barbaroi.

66
00:06:40,714 --> 00:06:42,614
We know her name.

67
00:06:43,817 --> 00:06:46,547
It's Layla Ashley.

68
00:06:53,293 --> 00:06:59,132
If someone who's supposed
to be dead is somehow alive,

69
00:06:59,132 --> 00:07:04,092
then she's..."The Child of Destiny."

70
00:07:06,372 --> 00:07:08,533
To come in while someone's
asleep and get a cheap shot...

71
00:07:08,641 --> 00:07:12,839
How disgusting.
Well, serves them right.

72
00:07:18,852 --> 00:07:21,514
How many times is this, anyway?

73
00:07:22,055 --> 00:07:26,287
Well, there seem to be a lot
of enemies, but you have...

74
00:07:26,693 --> 00:07:29,992
Huh? Could it be...?

75
00:07:37,137 --> 00:07:42,439
Anyway, this isn't good
anymore. We can't use it.

76
00:08:44,470 --> 00:08:47,268
You stick out too much.

77
00:08:48,074 --> 00:08:51,202
Just leave it to me.

78
00:08:52,145 --> 00:08:55,171
There's no need to advertise one's presence.

79
00:08:56,115 --> 00:08:58,447
You understand, right?

80
00:09:06,092 --> 00:09:07,286
Welcome!

81
00:09:08,061 --> 00:09:09,996
What are you looking for?

82
00:09:09,996 --> 00:09:12,632
I want a tent. The best thing would be...

83
00:09:12,632 --> 00:09:15,735
...the kind used in a Lunar Storm.
You got one?

84
00:09:15,735 --> 00:09:19,505
A tent? I wonder if I had one.

85
00:09:19,505 --> 00:09:22,041
What are you going to use it for, anyway?

86
00:09:22,041 --> 00:09:25,245
If you plan on sleeping outside,
there are no guarantees with your life.

87
00:09:25,245 --> 00:09:28,314
Huh? I wonder if this is it.

88
00:09:28,314 --> 00:09:30,305
You know how to use it, right?

89
00:09:30,950 --> 00:09:35,021
Yeah. Looks like a very high
quality product. I'll take it!

90
00:09:35,021 --> 00:09:36,818
You sure are a whimsical person.

91
00:09:59,445 --> 00:10:03,006
Uh... I'll carry it, Mr. Speedy.

92
00:10:04,517 --> 00:10:06,542
Thanks to what was
said in my grandfather's will,

93
00:10:06,586 --> 00:10:09,749
I can't let a girl carry anything.

94
00:10:10,490 --> 00:10:12,651
Even if you are a "doll."

95
00:10:18,398 --> 00:10:20,298
Hey, hey. Those boots...

96
00:10:23,870 --> 00:10:27,840
Not to mention your mantle's falling apart, too.

97
00:10:27,840 --> 00:10:30,707
Amazing how you've come
this far looking like that.

98
00:10:37,717 --> 00:10:39,708
What? In Hermes City?

99
00:10:41,387 --> 00:10:42,922
Is that for certain?

100
00:10:42,922 --> 00:10:46,059
Yes. I heard from a security doll.

101
00:10:46,059 --> 00:10:48,761
Contact the gladiators
we've already dispatched.

102
00:10:48,761 --> 00:10:50,820
Once they're located, restrain them...

103
00:10:50,863 --> 00:10:53,499
...and report immediately to
the nearest government office.

104
00:10:53,499 --> 00:10:54,966
Yes, sir.

105
00:11:00,340 --> 00:11:02,035
Lord Volk.

106
00:11:21,127 --> 00:11:22,321
Thank you very much.

107
00:11:23,930 --> 00:11:26,160
Um... Mr. Speedy.

108
00:11:26,532 --> 00:11:28,193
What is it, Nei?

109
00:11:37,310 --> 00:11:39,412
I understand, Nei.

110
00:11:39,412 --> 00:11:42,472
Layla's mantle, too, right?

111
00:11:42,548 --> 00:11:45,039
It's falling apart.

112
00:11:46,152 --> 00:11:50,782
Yeesh, you're certainly a doll who
has a lot of concern for her master.

113
00:11:52,492 --> 00:11:54,527
Do you have a mantle?

114
00:11:54,527 --> 00:11:58,498
A mantle? I might have had one, I might not...

115
00:11:58,498 --> 00:12:00,796
Thank you very much, Mr. Speedy.

116
00:12:20,620 --> 00:12:23,111
If you're going to fight,
let's hurry and get it over with.

117
00:12:26,859 --> 00:12:30,830
You, hiding from being seen

118
00:12:30,830 --> 00:12:34,493
As far as traces of you are concerned

119
00:12:34,901 --> 00:12:38,838
Even if you're steeped in madder red

120
00:12:38,838 --> 00:12:41,441
You're not going to disappear

121
00:12:41,441 --> 00:12:43,042
Don't let her escape!
You're not going to disappear

122
00:12:43,042 --> 00:12:44,600
You're not going to disappear

123
00:12:53,820 --> 00:12:56,789
Why don't you throw them
away and wear the new pair?

124
00:12:56,789 --> 00:12:57,990
No.

125
00:12:57,990 --> 00:12:59,258
Why not?

126
00:12:59,258 --> 00:13:01,351
I want to show them to Ms. Layla first.

127
00:13:03,062 --> 00:13:06,032
There's a starving wolf...

128
00:13:06,032 --> 00:13:09,593
...inside the forrest of gluttony

129
00:13:09,669 --> 00:13:10,103
I don't think they're here to ask directions.

130
00:13:10,103 --> 00:13:13,106
I don't think they're here to ask directions.
I was looking for one,

131
00:13:13,106 --> 00:13:14,040
I was looking for one,

132
00:13:14,040 --> 00:13:18,306
One like you, who risks his life

133
00:13:19,212 --> 00:13:22,148
When I think of you

134
00:13:22,148 --> 00:13:26,052
With passionate expression

135
00:13:26,052 --> 00:13:31,290
Within one teardrop

136
00:13:31,290 --> 00:13:34,259
There is truth

137
00:13:34,927 --> 00:13:42,702
The vows lasting only one night

138
00:13:42,702 --> 00:13:46,939
Are tattoos which were carved in my heart

139
00:13:46,939 --> 00:13:50,042
It is a farewell to the life you live now

140
00:13:50,042 --> 00:13:51,978
Are you all right?
It is a farewell to the life you live now

141
00:13:51,978 --> 00:13:52,808
It is a farewell to the life you live now

142
00:13:54,180 --> 00:13:54,780
This doll is made too well.

143
00:13:54,780 --> 00:13:58,307
This doll is made too well.
The soul which takes flight

144
00:13:58,784 --> 00:14:02,855
Offering death and a lily

145
00:14:02,855 --> 00:14:05,258
They scatter magnificently

146
00:14:05,258 --> 00:14:06,993
Not good.
They scatter magnificently

147
00:14:06,993 --> 00:14:10,630
The divine winds blow in

148
00:14:10,630 --> 00:14:14,700
The season of sorrow will not end

149
00:14:14,700 --> 00:14:20,434
Oh, please welcome this body

150
00:14:21,340 --> 00:14:22,034
Ms. Layla!

151
00:14:23,142 --> 00:14:24,632
Stand back.

152
00:14:25,611 --> 00:14:27,078
There they are!

153
00:14:36,422 --> 00:14:39,789
It's not personal. This is just a chance
for me to make something of myself!

154
00:14:41,360 --> 00:14:44,523
Then try and kill me.

155
00:14:54,907 --> 00:14:58,811
Free this soul

156
00:14:58,811 --> 00:15:02,848
Offering this blood and these roses

157
00:15:02,848 --> 00:15:06,752
Live proudly

158
00:15:06,752 --> 00:15:10,745
Are the words of a departed soul

159
00:15:10,790 --> 00:15:14,860
For the sake of these beautiful days

160
00:15:14,860 --> 00:15:20,799
Please guide me

161
00:15:29,942 --> 00:15:31,102
Layla!

162
00:15:35,982 --> 00:15:36,882
Ms. Layla!

163
00:15:36,882 --> 00:15:37,483
Ms. Layla!
For the sake of these elegant days

164
00:15:37,483 --> 00:15:39,418
She'll be... all right.
For the sake of these elegant days

165
00:15:39,418 --> 00:15:40,820
For the sake of these elegant days

166
00:15:40,820 --> 00:15:47,020
Please guide me

167
00:15:57,837 --> 00:16:00,931
What a pathetic sight for
the gladiator who beat me.

168
00:16:07,780 --> 00:16:10,146
I can't let you die yet.

169
00:16:11,717 --> 00:16:13,252
You...

170
00:16:13,252 --> 00:16:15,812
Who are you? Are you a gladiator, too?

171
00:16:18,591 --> 00:16:21,727
Someone who's forgotten
what pride is isn't a gladiator!

172
00:16:21,727 --> 00:16:23,422
Say what?!

173
00:16:26,432 --> 00:16:30,664
Does Volk's bait taste that good?

174
00:16:31,070 --> 00:16:32,059
Volk...

175
00:16:39,378 --> 00:16:40,367
Over here.

176
00:16:47,253 --> 00:16:49,084
Kill me.

177
00:16:49,522 --> 00:16:53,288
Try... and kill me.

178
00:16:54,560 --> 00:16:56,228
Ms. Layla...

179
00:16:56,228 --> 00:16:58,423
Don't look at what's about to happen.

180
00:17:59,692 --> 00:18:01,853
That's thoughtful of you.

181
00:18:03,729 --> 00:18:05,287
Layla Ashley...

182
00:18:12,671 --> 00:18:14,332
Why?

183
00:18:14,840 --> 00:18:17,536
I'm the one who'll kill you.

184
00:18:49,008 --> 00:18:53,775
I won't ever forget the
humiliation at that very moment.

185
00:18:57,149 --> 00:19:00,312
Don't die. Not until you get killed by me.

186
00:19:08,027 --> 00:19:09,858
Annihilated?

187
00:19:10,329 --> 00:19:14,231
I understand.
Continue surveillance.

188
00:19:15,901 --> 00:19:17,368
Lord Volk...

189
00:19:28,080 --> 00:19:32,039
I'll change destiny.

190
00:19:49,869 --> 00:19:50,803
Thank you.

191
00:19:50,803 --> 00:19:53,033
I don't want your thanks.

192
00:19:53,973 --> 00:19:56,032
What I want is...

193
00:20:01,280 --> 00:20:03,544
I owe you one!
If you need anything,

194
00:20:03,582 --> 00:20:05,243
I'll come and help you out any time!

195
00:20:08,120 --> 00:20:09,178
Layla Ashley...

196
00:20:19,331 --> 00:20:23,563
Your life isn't the only
thing Volk seems to want.

197
00:20:59,972 --> 00:21:02,167
"Child of Destiny."

198
00:21:02,942 --> 00:21:05,308
A life born into a source of fate...

199
00:21:11,150 --> 00:21:15,484
Say. Volk is that Volk, right?

200
00:21:16,455 --> 00:21:17,922
Layla?

201
00:21:19,792 --> 00:21:21,054
Um...

202
00:21:40,879 --> 00:21:43,177
It's quite an uncomfortable ride, though.

203
00:22:30,929 --> 00:22:34,299
Using a microscope,
KEMIKARU SUKOOPU de

204
00:22:34,299 --> 00:22:37,670
I peeked into it until morning
Nozoite asamade

205
00:22:37,670 --> 00:22:39,501
There are no taboos
TABUU nado nai

206
00:22:39,538 --> 00:22:41,240
I can do as I please
Oki ni mesumama

207
00:22:41,240 --> 00:22:44,610
Loving it from an anatomical standpoint
ANATOMIKKU ni aishite

208
00:22:44,610 --> 00:22:47,980
The city is a gigantic
Tokai wa kyodaina

209
00:22:47,980 --> 00:22:51,450
Place to run experiments
Jikkenjyou nano

210
00:22:51,450 --> 00:22:53,218
Once you're no longer necessary
Fuyou ni nareba

211
00:22:53,218 --> 00:22:54,920
You'll immediately be erased
Suguni kesareru

212
00:22:54,920 --> 00:22:58,686
Hurridly living passionately
Jyounetsu ni iki isoide

213
00:22:58,757 --> 00:23:05,230
There's the Garden of Eden in the basement
Chikashitsu ni EDEN no sono

214
00:23:05,230 --> 00:23:15,071
The happiness you fight over is an illusion
Ubaiau koufuku wa maboroshi

215
00:23:15,441 --> 00:23:17,176
The Eve of tomorrow
Mirai no IVU

216
00:23:17,176 --> 00:23:18,911
Born from
MEFISUTOFERESU no

217
00:23:18,911 --> 00:23:22,314
A bone of Mephistopheles
Hone kara umareta

218
00:23:22,314 --> 00:23:25,751
To the androids who are incapable of crying
Namidamo nai ANDOROIDO no

219
00:23:25,751 --> 00:23:29,221
You are the goddess of depravity
Daraku no megami yo

220
00:23:29,221 --> 00:23:30,856
I'll take your apple and
Anata no ringo

221
00:23:30,856 --> 00:23:32,624
Lick it, bite into it,
Namesasete kajirasete

222
00:23:32,624 --> 00:23:36,526
And soak in the taste of sin
Fukumasete tsumi no aji wo

