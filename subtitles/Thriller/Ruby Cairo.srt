﻿1
00:00:20,133 --> 00:00:21,534
Here.

2
00:00:25,033 --> 00:00:27,767
Episode 86

3
00:00:27,968 --> 00:00:30,601
Are you happy?

4
00:00:30,601 --> 00:00:32,968
Do you like seeing me like this?

5
00:00:33,901 --> 00:00:36,133
Why would you say this?

6
00:00:36,133 --> 00:00:38,267
This is your fault.

7
00:00:38,267 --> 00:00:39,834
If you hadn't filmed us,

8
00:00:39,834 --> 00:00:42,200
none of this would've happened.

9
00:00:43,033 --> 00:00:45,067
This is all your fault,

10
00:00:45,067 --> 00:00:46,072
so fix it right now!

11
00:00:46,567 --> 00:00:47,701
You're...

12
00:00:50,000 --> 00:00:54,003
Yeah. I'm the idiot for trying to help.

13
00:00:55,367 --> 00:00:57,300
Don't go.

14
00:00:57,634 --> 00:01:00,400
Please, you have to save me.

15
00:01:00,004 --> 00:01:02,171
The head of the election committee says that

16
00:01:02,567 --> 00:01:07,901
if the uploader confesses that the video's a fake,

17
00:01:07,901 --> 00:01:08,910
then I'll still have a chance.

18
00:01:09,801 --> 00:01:11,734
It'll all go away.

19
00:01:11,734 --> 00:01:14,901
I could even get sympathy votes.

20
00:01:16,367 --> 00:01:19,434
So please, persuade Ruby.

21
00:01:19,434 --> 00:01:21,901
I beg you. Please?

22
00:01:28,868 --> 00:01:31,367
Now you want Ruby to lie?

23
00:01:31,367 --> 00:01:33,634
But it's not a lie.

24
00:01:33,634 --> 00:01:36,901
Ruby really did post the video.

25
00:01:36,901 --> 00:01:39,467
So please...

26
00:01:39,467 --> 00:01:41,801
Please, just talk to her.

27
00:01:41,801 --> 00:01:43,000
I won't ever forget it.

28
00:01:43,000 --> 00:01:45,434
I'll be indebted to both of you forever.

29
00:01:46,367 --> 00:01:48,634
You're asking me to kill Ruby again?

30
00:01:48,634 --> 00:01:49,701
What?

31
00:01:49,701 --> 00:01:52,100
Ruby's died several times over already,

32
00:01:52,001 --> 00:01:53,769
all because of you.

33
00:01:53,868 --> 00:01:58,300
She died when she came out of the coma,

34
00:01:58,003 --> 00:02:00,836
when she got her memory back,

35
00:02:01,133 --> 00:02:04,801
and when she decided to seek revenge.

36
00:02:05,701 --> 00:02:07,767
I'd rather die first.

37
00:02:07,767 --> 00:02:09,834
I can't do that to her.

38
00:02:10,467 --> 00:02:13,534
How heartbreaking.

39
00:02:14,267 --> 00:02:16,534
You're such a romantic, Insu.

40
00:02:16,534 --> 00:02:18,536
Don't you dare mess around.

41
00:02:18,734 --> 00:02:21,467
I won't let you get away with it.

42
00:02:21,467 --> 00:02:24,000
Whether you become a legislator,

43
00:02:24,000 --> 00:02:27,334
a president, or a criminal behind bars,

44
00:02:27,334 --> 00:02:29,300
you deal with it.

45
00:02:29,003 --> 00:02:31,005
I want no part in it.

46
00:02:39,001 --> 00:02:40,835
Hello.

47
00:02:42,002 --> 00:02:43,436
Gyeongmin.

48
00:02:43,634 --> 00:02:45,300
Are you okay?

49
00:02:48,534 --> 00:02:50,500
I better get going.

50
00:02:50,005 --> 00:02:52,439
Okay. Thank you.

51
00:03:06,868 --> 00:03:10,869
After all this, do you still plan to run?

52
00:03:15,001 --> 00:03:16,502
Stop this.

53
00:03:17,634 --> 00:03:21,642
No. I won't ever stop.

54
00:03:22,434 --> 00:03:24,133
I just met the head of the election committee,

55
00:03:24,133 --> 00:03:25,667
and we found a way out.

56
00:03:25,667 --> 00:03:26,801
Ruby.

57
00:03:26,801 --> 00:03:31,868
Gyeongmin, I'm going to win no matter what.

58
00:03:31,868 --> 00:03:33,601
I'm going to go for it.

59
00:03:35,033 --> 00:03:39,033
So don't worry. I'm Jeong Ruby.

60
00:03:49,834 --> 00:03:50,840
Vice President Bae is here.

61
00:03:51,434 --> 00:03:53,033
Okay.

62
00:03:55,267 --> 00:03:56,272
What is it?

63
00:03:56,767 --> 00:03:59,901
I was just passing by.

64
00:04:00,834 --> 00:04:04,000
That video's becoming an even bigger issue.

65
00:04:04,000 --> 00:04:05,701
Is Ruby okay?

66
00:04:05,701 --> 00:04:07,200
I heard there were protestors

67
00:04:07,002 --> 00:04:09,569
in front of the company building earlier.

68
00:04:10,634 --> 00:04:12,901
It doesn't even matter.

69
00:04:12,901 --> 00:04:15,968
I don't think I can do anything about it.

70
00:04:16,234 --> 00:04:19,234
You act like this doesn't concern you.

71
00:04:19,234 --> 00:04:21,133
If you keep this up...

72
00:04:23,467 --> 00:04:24,901
Forget it.

73
00:04:24,901 --> 00:04:28,901
Your marriage isn't any of my business.

74
00:04:28,901 --> 00:04:35,934
Sera, do you know any private detectives?

75
00:04:35,934 --> 00:04:37,567
Private detectives?

76
00:04:37,567 --> 00:04:39,634
Professionals who can find out about

77
00:04:39,634 --> 00:04:42,067
someone's past, personal life, secrets...

78
00:04:43,334 --> 00:04:46,567
Nevermind. What am I saying?

79
00:04:46,567 --> 00:04:49,000
Forget it. Pretend like I didn't say anything.

80
00:04:49,000 --> 00:04:50,567
I have to go.

81
00:04:53,701 --> 00:04:55,267
Gyeongmin.

82
00:04:59,434 --> 00:05:02,067
He's hanging by a thread.

83
00:05:02,067 --> 00:05:04,701
What is he thinking?

84
00:05:13,901 --> 00:05:15,901
Your phone...

85
00:05:17,234 --> 00:05:19,133
Oh, right.

86
00:05:23,467 --> 00:05:24,534
Hello?

87
00:05:24,534 --> 00:05:28,667
Ruby, it's me. I need to see you.

88
00:05:28,667 --> 00:05:29,675
I'm busy. I need to work.

89
00:05:30,467 --> 00:05:35,334
It'll only take a second. We need to talk.

90
00:05:41,734 --> 00:05:42,968
Did you have lunch?

91
00:05:42,968 --> 00:05:44,834
I'm busy.

92
00:05:45,934 --> 00:05:49,467
I don't suppose you took down the video.

93
00:05:49,467 --> 00:05:53,400
Didn't I tell you not to get your hopes up?

94
00:05:54,467 --> 00:05:59,934
Ruby, have mercy. I'm sorry.

95
00:05:59,934 --> 00:06:03,500
I feel terrible for what I did to you, so...

96
00:06:03,005 --> 00:06:04,672
We're done here.

97
00:06:05,167 --> 00:06:08,168
Don't leave yet. Listen to me.

98
00:06:09,002 --> 00:06:11,403
The police are starting an investigation soon.

99
00:06:11,601 --> 00:06:14,234
The might have started already.

100
00:06:14,234 --> 00:06:16,240
If they find out you uploaded the video

101
00:06:16,834 --> 00:06:19,400
you'll be arrested.

102
00:06:19,701 --> 00:06:21,734
So wouldn't it be best for everyone

103
00:06:21,734 --> 00:06:26,467
if you come clean now and ask for forgiveness?

104
00:06:26,467 --> 00:06:30,834
You can beat them to the punch,

105
00:06:30,834 --> 00:06:33,200
tell everyone the video's a fake

106
00:06:33,002 --> 00:06:34,010
that you uploaded out of anger

107
00:06:35,000 --> 00:06:37,267
and that this whole thing was a mistake.

108
00:06:37,968 --> 00:06:40,567
Is that the best your devious brain

109
00:06:40,567 --> 00:06:41,568
could come up with?

110
00:06:41,667 --> 00:06:47,667
If you do that, I'll do anything you ask.

111
00:06:47,667 --> 00:06:51,200
Save me. Please help me, Ruby.

112
00:06:51,002 --> 00:06:53,670
There is something I want.

113
00:06:53,868 --> 00:06:56,801
What? What is it?

114
00:06:56,801 --> 00:06:59,167
To trade my face for yours,

115
00:06:59,167 --> 00:07:01,400
and go back to the way things were.

116
00:07:02,667 --> 00:07:04,200
Don't fool yourself.

117
00:07:04,002 --> 00:07:06,569
Did you think this would get me to change?

118
00:07:06,767 --> 00:07:10,774
You think I'm scared of being arrested?

119
00:07:11,467 --> 00:07:16,468
I'm waiting for the police to come get me,

120
00:07:16,567 --> 00:07:21,234
because then, everyone will finally know

121
00:07:21,234 --> 00:07:24,701
about your lies and secrets.

122
00:07:24,701 --> 00:07:27,734
It'll be an epic scandal.

123
00:07:27,734 --> 00:07:29,167
The woman cocky enough to

124
00:07:29,167 --> 00:07:33,176
steal her sister's life, face, fiancee, marriage,

125
00:07:34,067 --> 00:07:36,068
and enter into politics,

126
00:07:36,167 --> 00:07:38,701
the infamous Jeong Runa.

127
00:07:38,701 --> 00:07:41,710
The mere thought brings me joy.

128
00:07:42,601 --> 00:07:45,868
I think that'd be a sweet revenge.

129
00:07:46,234 --> 00:07:47,067
You're insane.

130
00:07:47,067 --> 00:07:50,068
I'm not insane, I'm your sister.

131
00:07:51,001 --> 00:07:54,802
The fact that I'm still sane,

132
00:07:54,901 --> 00:07:57,901
is actually pretty insane.

133
00:07:59,133 --> 00:08:01,334
Everyday, it feels like my heart is being

134
00:08:01,334 --> 00:08:03,343
ripped out of me body multiple times.

135
00:08:04,234 --> 00:08:07,968
At this point, I'd rather just die.

136
00:08:10,033 --> 00:08:13,167
Thanks for turning me into this demon.

137
00:08:13,167 --> 00:08:17,173
I'm trying my best to become a monster

138
00:08:17,767 --> 00:08:23,100
a hundred times more terrifying than you.

139
00:08:24,534 --> 00:08:28,701
I imagine I'll see you in hell.

140
00:08:30,133 --> 00:08:33,701
Will the police finally arrive today?

141
00:08:33,701 --> 00:08:36,033
Or maybe tomorrow?

142
00:08:37,033 --> 00:08:39,500
Follow that twisted greed of yours

143
00:08:39,005 --> 00:08:42,373
to the bitter end.

144
00:08:42,868 --> 00:08:45,734
It'll probably lead you to the gates of hell.

145
00:09:09,367 --> 00:09:11,371
What is this, Jeong Ruby?

146
00:09:12,003 --> 00:09:16,571
Are you really trying to become a demon?

147
00:09:16,868 --> 00:09:18,934
Are you?

148
00:09:26,701 --> 00:09:31,567
No. No.

149
00:09:31,567 --> 00:09:33,200
This isn't right.

150
00:09:40,005 --> 00:09:41,739
That's all for today. See you tomorrow.

151
00:09:42,234 --> 00:09:43,667
Bye.

152
00:09:43,667 --> 00:09:46,673
This quaint and heartwarming house

153
00:09:47,267 --> 00:09:50,033
was a gift for a man's wife

154
00:09:50,033 --> 00:09:53,267
on her 60th birthday.

155
00:09:53,267 --> 00:09:54,275
I miss you.

156
00:09:59,801 --> 00:10:02,434
Me too. What are you doing right now?

157
00:10:02,434 --> 00:10:04,437
Scratching Uncle Daepung's back.

158
00:10:04,734 --> 00:10:08,901
Stop texting your girlfriend and focus.

159
00:10:08,901 --> 00:10:10,534
It's Soyeong, isn't it?

160
00:10:10,534 --> 00:10:13,538
You're already whipped.

161
00:10:14,634 --> 00:10:16,133
What's wrong with that?

162
00:10:16,133 --> 00:10:19,100
You always whine about how lonely you are.

163
00:10:19,001 --> 00:10:22,968
I know.

164
00:10:23,067 --> 00:10:25,070
When you get old, you need a woman

165
00:10:25,367 --> 00:10:27,901
who will scratch your back.

166
00:10:27,901 --> 00:10:30,968
But I have no wealth, no power.

167
00:10:30,968 --> 00:10:33,367
Where would I find a woman?

168
00:10:33,367 --> 00:10:34,374
Ms. Janderella.

169
00:10:36,133 --> 00:10:40,367
She was small, pudgy, and sweet.

170
00:10:40,367 --> 00:10:42,370
She was perfect.

171
00:10:47,701 --> 00:10:50,467
Enough already. Come on.

172
00:10:55,003 --> 00:10:57,736
See? There she goes again.

173
00:10:59,734 --> 00:11:02,000
Let her be. What can we do?

174
00:11:02,000 --> 00:11:03,601
Are you saying you'd be cool with

175
00:11:03,601 --> 00:11:06,767
Soyeong and Jiheok getting married. Is that it?

176
00:11:07,234 --> 00:11:09,000
Did they say they were getting married?

177
00:11:09,000 --> 00:11:11,004
They're just good friends.

178
00:11:11,004 --> 00:11:13,737
And you believe that? Do you?

179
00:11:14,133 --> 00:11:15,334
Why wouldn't I?

180
00:11:15,334 --> 00:11:17,267
You're so territorial.

181
00:11:17,267 --> 00:11:18,634
What?

182
00:11:18,634 --> 00:11:19,500
Before we got married,

183
00:11:19,005 --> 00:11:20,373
you were so territorial of me,

184
00:11:20,868 --> 00:11:22,801
and now you're territorial with Jihyeok?

185
00:11:22,801 --> 00:11:25,367
Why do you do that?

186
00:11:25,367 --> 00:11:28,369
What'll happen after we have our baby?

187
00:11:29,701 --> 00:11:30,968
Baby?

188
00:11:30,968 --> 00:11:33,300
Of course. Baby.

189
00:11:34,901 --> 00:11:38,467
We need to get ready for the baby.

190
00:11:39,734 --> 00:11:41,742
Stop it.

191
00:11:55,868 --> 00:11:58,801
Runa, you're home?

192
00:11:58,834 --> 00:12:01,200
Why are you so late?

193
00:12:01,934 --> 00:12:05,939
You just called me Runa, right?

194
00:12:20,005 --> 00:12:22,339
What's gotten into her, Gilja?

195
00:12:22,834 --> 00:12:23,842
I thought she'd changed,

196
00:12:24,634 --> 00:12:25,642
but she's back to her old self.

197
00:12:55,067 --> 00:12:56,601
Ruby.

198
00:13:01,004 --> 00:13:02,637
Ruby.

199
00:13:05,567 --> 00:13:08,634
Ruby? Ruby?

200
00:13:10,801 --> 00:13:14,267
I thought I was Runa. I'm Ruby now?

201
00:13:15,067 --> 00:13:17,071
You know my name's Ruby,

202
00:13:17,467 --> 00:13:20,801
why are you asking me to play Runa?

203
00:13:20,801 --> 00:13:23,734
Then what else can I do?

204
00:13:23,734 --> 00:13:26,734
Chorim and Dongpal were there.

205
00:13:26,734 --> 00:13:29,736
Should I have called you Ruby?

206
00:13:29,934 --> 00:13:33,133
Just let them all find out?

207
00:13:38,000 --> 00:13:39,801
Ruby.

208
00:13:41,634 --> 00:13:45,000
I don't know what to do either.

209
00:13:47,567 --> 00:13:53,200
What would you like me to do?

210
00:13:55,004 --> 00:13:58,171
Just tell me.

211
00:13:58,567 --> 00:14:01,334
I'll do as you say.

212
00:14:11,003 --> 00:14:16,337
It's all because I'm an idiot.

213
00:14:19,005 --> 00:14:23,005
I couldn't tell my own girls apart.

214
00:14:25,001 --> 00:14:29,235
I'm a complete fool.

215
00:14:32,234 --> 00:14:37,033
Even animals can tell their cubs apart.

216
00:14:38,033 --> 00:14:42,534
How could a person be this stupid?

217
00:14:46,234 --> 00:14:49,167
I'm sorry, Ruby.

218
00:14:52,002 --> 00:14:55,569
I wish I could die.

219
00:14:57,067 --> 00:15:00,901
I wish this was all a dream.

220
00:15:05,003 --> 00:15:08,870
My dear Ruby...

221
00:15:09,167 --> 00:15:13,133
You poor thing.

222
00:15:25,634 --> 00:15:27,801
Are you just going to stay in bed?

223
00:15:28,234 --> 00:15:29,801
Grandma and mom are visiting

224
00:15:29,801 --> 00:15:31,801
seniors that live alone today.

225
00:15:31,801 --> 00:15:33,734
Aren't you going?

226
00:15:34,004 --> 00:15:36,572
I have no time. I'm busy.

227
00:15:41,601 --> 00:15:43,634
I just don't get it.

228
00:15:43,634 --> 00:15:47,640
Your family doesn't care about what I do.

229
00:15:48,234 --> 00:15:51,400
But they want to drag me out to do that.

230
00:15:51,004 --> 00:15:52,938
What have they ever done for me?

231
00:15:53,334 --> 00:15:55,267
Ruby.

232
00:15:58,667 --> 00:16:01,300
While we're on this subject,

233
00:16:01,003 --> 00:16:03,304
take the video scandal for example,

234
00:16:03,601 --> 00:16:05,634
if the company had just helped me out,

235
00:16:05,634 --> 00:16:07,801
it wouldn't have become this serious.

236
00:16:07,801 --> 00:16:09,567
Are you kidding me?

237
00:16:09,567 --> 00:16:10,567
It's your job to prevent this kind of thing

238
00:16:10,567 --> 00:16:12,033
from happening in the first place.

239
00:16:12,033 --> 00:16:15,033
I already told you, I'm the victim.

240
00:16:15,033 --> 00:16:17,767
Some hater just uploaded a fake video...

241
00:16:17,767 --> 00:16:22,868
Why? Why would someone hate you?

242
00:16:22,868 --> 00:16:26,000
Have you ever thought about that?

243
00:16:26,000 --> 00:16:27,767
Have you thought about

244
00:16:27,767 --> 00:16:30,734
what you might have done wrong?

245
00:16:30,734 --> 00:16:34,367
Why bother? It's obvious.

246
00:16:34,367 --> 00:16:35,376
Since my ratings were so high,

247
00:16:36,267 --> 00:16:38,400
they launched a smear campaign.

248
00:16:38,004 --> 00:16:42,371
Are you doubting me?

249
00:16:42,767 --> 00:16:47,601
Do you actually think the video is real?

250
00:16:49,003 --> 00:16:52,137
Tell me. Are you doubting me?

251
00:16:52,434 --> 00:16:54,435
You don't trust me?

252
00:16:55,901 --> 00:16:58,909
I don't. I don't believe you.

253
00:17:00,000 --> 00:17:01,634
Gyeongmin.

254
00:17:02,167 --> 00:17:04,434
After we got married,

255
00:17:04,434 --> 00:17:07,200
how many times did you deceive me?

256
00:17:07,002 --> 00:17:09,436
Do you know how many times I had to

257
00:17:09,634 --> 00:17:12,601
forgive you and overlook what you did?

258
00:17:14,534 --> 00:17:17,968
The video was fabricated?

259
00:17:17,968 --> 00:17:21,300
You can fool everyone else, but not me.

260
00:17:21,003 --> 00:17:23,836
The woman in the video has your facial

261
00:17:24,133 --> 00:17:26,934
expressions, voice, and hand gestures.

262
00:17:26,934 --> 00:17:30,767
The blue blouse she's wearing in the video

263
00:17:30,767 --> 00:17:35,000
is the first gift I got for you after our marriage.

264
00:17:35,000 --> 00:17:38,133
I picked it out myself.

265
00:17:38,934 --> 00:17:41,968
I'm sure lots of people have that blouse.

266
00:17:41,968 --> 00:17:46,133
We'll find out soon enough

267
00:17:46,133 --> 00:17:49,142
because the truth always prevails.

268
00:17:59,801 --> 00:18:04,500
What do I do? What do I do?

269
00:18:04,005 --> 00:18:05,839
How can I fix this?

270
00:18:07,901 --> 00:18:10,834
I have to come up with a plan.

271
00:18:13,367 --> 00:18:16,368
Yes. The press conference.

272
00:18:16,467 --> 00:18:19,033
I have to persuade Jeong Ruby

273
00:18:19,033 --> 00:18:21,234
to do the press conference.

274
00:18:32,801 --> 00:18:34,734
What brings you here?

275
00:18:34,734 --> 00:18:37,701
Are you okay?

276
00:18:37,701 --> 00:18:39,300
I heard the protestors

277
00:18:39,003 --> 00:18:40,937
wreaked havoc yesterday.

278
00:18:41,234 --> 00:18:43,500
And your ratings have fallen too.

279
00:18:43,005 --> 00:18:44,013
After all this humiliation, what if

280
00:18:45,003 --> 00:18:47,137
the party turns its back on you?

281
00:18:47,434 --> 00:18:48,436
Be quiet.

282
00:18:48,634 --> 00:18:49,636
What?

283
00:18:54,634 --> 00:18:57,133
Mom, we need to talk.

284
00:19:00,067 --> 00:19:04,068
What? What are you talking about?

285
00:19:04,167 --> 00:19:05,534
A press conference?

286
00:19:05,534 --> 00:19:08,167
Please convince Runa.

287
00:19:08,167 --> 00:19:10,334
She won't even pick up my calls.

288
00:19:10,334 --> 00:19:15,868
I can't even approach the office to go see her.

289
00:19:15,868 --> 00:19:18,934
Mom, please talk to her.

290
00:19:18,934 --> 00:19:20,567
Tell her to hold a press conference

291
00:19:20,567 --> 00:19:21,576
and say that the video's not real

292
00:19:22,467 --> 00:19:24,400
and that it was fabricated.

293
00:19:25,067 --> 00:19:29,074
How can you be so selfish?

294
00:19:29,767 --> 00:19:32,500
You want me to tell Runa to sacrifice herself?

295
00:19:32,005 --> 00:19:35,272
I'll repay her once I'm elected.

296
00:19:35,767 --> 00:19:38,771
I'm serious. If you help me, I can win.

297
00:19:39,167 --> 00:19:41,300
I can become an assemblywoman.

298
00:19:42,002 --> 00:19:43,935
No you can't.

299
00:19:44,004 --> 00:19:46,472
Is this how it's going to be, mom?

300
00:19:46,868 --> 00:19:48,701
You want to watch me die?

301
00:19:48,701 --> 00:19:51,167
What? Die?

302
00:19:51,167 --> 00:19:54,434
Fine. Let's just go and kill ourselves.

303
00:19:54,434 --> 00:19:55,968
Let's just die.

304
00:19:55,968 --> 00:19:57,834
That'd actually bring me peace.

305
00:19:57,834 --> 00:19:59,267
If Runa didn't post that video,

306
00:19:59,267 --> 00:20:01,968
none of this would've happened.

307
00:20:04,033 --> 00:20:06,000
That vile witch.

308
00:20:07,667 --> 00:20:12,200
So that's not you in the video?

309
00:20:12,002 --> 00:20:16,403
It was you. Why would you do that?

310
00:20:16,601 --> 00:20:19,234
Tell me what happened.

311
00:20:19,234 --> 00:20:21,033
Just let it all out.

312
00:20:21,033 --> 00:20:23,567
What's the point?

313
00:20:23,567 --> 00:20:26,934
That's not what's important.

314
00:20:26,934 --> 00:20:29,133
If Runa doesn't hold the press conference,

315
00:20:29,133 --> 00:20:32,400
not only will I lose the election,

316
00:20:32,004 --> 00:20:34,538
my in-laws will kick me out.

317
00:20:34,934 --> 00:20:37,400
Is that what you want?

318
00:20:37,004 --> 00:20:38,238
No, right?

319
00:20:38,634 --> 00:20:39,636
Ruby.

320
00:20:39,834 --> 00:20:41,567
Call Runa right now.

321
00:20:41,567 --> 00:20:44,568
I don't have time. Hurry.

322
00:20:47,005 --> 00:20:49,739
Every time you visit,

323
00:20:50,234 --> 00:20:53,567
you bring me so many gifts.

324
00:20:53,567 --> 00:20:56,400
Thank you so much.

325
00:20:57,334 --> 00:21:00,337
It's so cold these days.

326
00:21:00,634 --> 00:21:03,767
Why don't you turn the heat on?

327
00:21:03,767 --> 00:21:06,934
I don't have the money for it.

328
00:21:06,934 --> 00:21:07,934
Huh?

329
00:21:07,934 --> 00:21:12,367
But I have this electric blanket,

330
00:21:12,367 --> 00:21:13,934
so it's okay.

331
00:21:15,002 --> 00:21:16,835
How's your health?

332
00:21:17,033 --> 00:21:18,667
Are you taking the supplements

333
00:21:18,667 --> 00:21:20,133
we sent you last time?

334
00:21:20,133 --> 00:21:23,767
Of course. I have them

335
00:21:23,767 --> 00:21:28,033
like snacks whenever I get hungry.

336
00:21:28,467 --> 00:21:30,334
Let me know if you need any more.

337
00:21:30,334 --> 00:21:32,167
I'll send you more.

338
00:21:32,167 --> 00:21:37,176
You're so handsome, and also so sweet.

339
00:21:40,133 --> 00:21:42,234
We need to get going.

340
00:21:42,234 --> 00:21:45,236
- We have another house to visit. / - Wait.

341
00:21:49,001 --> 00:21:52,368
Use this for cab fare.

342
00:21:53,067 --> 00:21:54,968
I have money, ma'am.

343
00:21:56,033 --> 00:21:58,167
You can't refuse your elders.

344
00:22:01,067 --> 00:22:03,801
Yeomin Party candidate Jeong Ruby's ratings
continue to fall due to video scandal

345
00:22:09,004 --> 00:22:10,013
I'm home, father.

346
00:22:13,601 --> 00:22:16,133
Did mother and grandmother go someplace?

347
00:22:16,701 --> 00:22:17,834
You didn't hear that

348
00:22:17,834 --> 00:22:20,400
they were visiting old folks today?

349
00:22:20,004 --> 00:22:22,038
You know how busy I am.

350
00:22:22,434 --> 00:22:23,567
So what?

351
00:22:23,567 --> 00:22:26,534
We have work around your schedule?

352
00:22:26,534 --> 00:22:27,541
It's not that...

353
00:22:28,234 --> 00:22:32,133
You disgrace us every single time!

354
00:22:36,067 --> 00:22:38,000
I'll be upstairs.

355
00:22:48,767 --> 00:22:50,934
Mr. Kim, it's me.

356
00:22:50,934 --> 00:22:51,943
Any progress on the favor I asked you?

357
00:22:52,834 --> 00:22:53,838
We have a specialist analyzing it.

358
00:22:54,234 --> 00:22:56,400
Please wait just a bit longer.

359
00:22:56,004 --> 00:22:59,838
Okay. I need it as soon as possible.

360
00:23:06,167 --> 00:23:09,168
So? Did you talk to Runa?

361
00:23:09,267 --> 00:23:12,000
No. Not yet.

362
00:23:13,701 --> 00:23:15,400
Is this how it's going to be, mom?

363
00:23:15,004 --> 00:23:18,071
You're the only one Runa listens to.

364
00:23:18,467 --> 00:23:20,133
Please, persuade her.

365
00:23:20,133 --> 00:23:23,868
Why should I do anything for you?

366
00:23:23,868 --> 00:23:25,767
I can't do it.

367
00:23:26,667 --> 00:23:27,968
Are you serious, mom?

368
00:23:27,968 --> 00:23:30,634
Do you want to watch me die?

369
00:23:31,467 --> 00:23:33,234
It's all up to you now.

370
00:23:33,234 --> 00:23:36,467
Tell Runa to hold a press conference!

371
00:23:36,968 --> 00:23:38,434
Ruby.

372
00:23:39,801 --> 00:23:43,133
I told you it's all your fault, didn't I?

373
00:23:43,133 --> 00:23:44,834
Don't forget that.

374
00:23:48,002 --> 00:23:49,503
It's Ruby.

375
00:23:49,701 --> 00:23:50,710
Soyeong, turn up the volume.

376
00:23:52,534 --> 00:23:54,542
Only 15 days left before the by-election.

377
00:23:55,334 --> 00:23:59,868
Candidates will start their campaigns tomorrow.

378
00:23:59,868 --> 00:24:01,534
Jeong Ruby, the only female candidate

379
00:24:01,534 --> 00:24:03,400
and the wife of JM Group's heir,

380
00:24:03,004 --> 00:24:05,271
refuses to drop out of the race

381
00:24:05,667 --> 00:24:07,968
despite the Yeomin Party's opposition

382
00:24:07,968 --> 00:24:09,500
in the wake of the video scandal.

383
00:24:09,005 --> 00:24:10,072
- Turn that off, Chorim. / - The Jeong camp

384
00:24:10,567 --> 00:24:11,574
- Why, Gilja? / - claims they will

385
00:24:12,267 --> 00:24:14,269
- I told you to turn it off! / - put...

386
00:24:16,004 --> 00:24:18,438
- Turn it off, Soyeong. / - Okay.

387
00:24:25,601 --> 00:24:31,534
To enter politics, you need to have integrity.

388
00:24:31,534 --> 00:24:34,033
That's why so many political nominees

389
00:24:34,033 --> 00:24:36,034
end up quitting after they're dissected

390
00:24:36,133 --> 00:24:38,968
and grilled at during their campaigns.

391
00:24:38,968 --> 00:24:40,167
Do you think Ruby

392
00:24:40,167 --> 00:24:43,968
would survive that kind of scrutiny?

393
00:24:45,734 --> 00:24:48,033
It's better to expose her

394
00:24:48,033 --> 00:24:50,734
and make her quit now, mom.

395
00:24:51,001 --> 00:24:54,468
Just as Ruby said,

396
00:24:54,567 --> 00:24:56,534
if the truth comes to light,

397
00:24:56,534 --> 00:24:58,542
Runa will suffer serious consequences.

398
00:24:59,934 --> 00:25:05,968
And yet she still drives herself to her own ruin.

399
00:25:12,534 --> 00:25:15,267
No. I should talk to her in person.

400
00:25:15,267 --> 00:25:18,400
Yes. I have to talk her out of it.

401
00:25:23,634 --> 00:25:26,100
Gilja, where are you going?

402
00:25:26,005 --> 00:25:31,072
Jeez. What's up with her these days?

403
00:25:32,567 --> 00:25:33,572
Just let her be.

404
00:25:34,067 --> 00:25:35,071
Ruby's video scandal

405
00:25:35,467 --> 00:25:37,701
must have really shaken her up.

406
00:25:37,701 --> 00:25:39,067
Oh man.

407
00:25:39,067 --> 00:25:42,934
I hope they find the culprit so I can

408
00:25:42,934 --> 00:25:44,801
beat him to a pulp.

409
00:25:44,801 --> 00:25:47,567
You're scaring me, Chorim.

410
00:25:47,567 --> 00:25:50,569
Our mom's one tough cookie.

411
00:25:51,003 --> 00:25:52,870
It's like I married a thug.

412
00:25:53,167 --> 00:25:56,200
Everything that comes out of your mouth

413
00:25:56,002 --> 00:25:58,603
makes me feel like I'm being mugged.

414
00:25:58,801 --> 00:26:01,834
What's wrong with what I said?

415
00:26:01,834 --> 00:26:03,000
If he gets caught,

416
00:26:03,000 --> 00:26:07,467
I'll knock all his teeth out.

417
00:26:18,000 --> 00:26:20,005
No reporters or protestors, right?

418
00:26:31,003 --> 00:26:34,005
Why'd you come? You could've just called.

419
00:26:36,968 --> 00:26:40,133
So? What did Runa say?

420
00:26:44,734 --> 00:26:47,868
It's cold. Let's get in the car first.

421
00:27:07,367 --> 00:27:08,371
What about Gyeongmin?

422
00:27:08,767 --> 00:27:10,133
He's sleeping.

423
00:27:11,267 --> 00:27:13,434
His grandmother and parents?

424
00:27:14,834 --> 00:27:16,868
Sleeping.

425
00:27:17,834 --> 00:27:19,467
What happened?

426
00:27:19,467 --> 00:27:22,133
So did you talk to Runa?

427
00:27:22,968 --> 00:27:28,367
Ruby, I don't think this is right.

428
00:27:28,367 --> 00:27:29,371
What's not right?

429
00:27:29,767 --> 00:27:32,100
It's already gotten out of hand.

430
00:27:32,001 --> 00:27:34,869
It makes no sense for you to continue.

431
00:27:36,367 --> 00:27:38,601
Your in-laws are against it too,

432
00:27:38,601 --> 00:27:40,634
and Gyeongmin doesn't support you either.

433
00:27:40,634 --> 00:27:41,643
You've come all this way to tell me that?

434
00:27:42,534 --> 00:27:44,801
What about the video?

435
00:27:46,133 --> 00:27:48,367
I don't know much about the internet, but...

436
00:27:48,367 --> 00:27:50,367
I told you it wasn't my fault.

437
00:27:50,367 --> 00:27:51,370
I told you that isn't me!

438
00:27:51,667 --> 00:27:53,673
Of course it's you!

439
00:27:54,267 --> 00:27:56,534
And that man is Insu.

440
00:27:58,367 --> 00:28:01,934
I already know you stole Ruby's face

441
00:28:02,367 --> 00:28:04,368
and deceived everyone.

442
00:28:10,634 --> 00:28:12,667
What are you talking about?

443
00:28:13,004 --> 00:28:15,171
You think you know something?

444
00:28:15,567 --> 00:28:17,801
What do you know?

445
00:28:22,634 --> 00:28:25,033
It's not me.

446
00:28:25,033 --> 00:28:28,734
It's not. It's not.

447
00:28:48,334 --> 00:28:52,901
Runa, how long did you plan to keep this up?

448
00:28:52,901 --> 00:28:56,767
No. No. I'm not Runa.

449
00:28:58,701 --> 00:29:01,500
Are you stupid? Are you crazy?

450
00:29:01,005 --> 00:29:02,473
Can't you see my face?

451
00:29:02,968 --> 00:29:06,400
I'm Jeong Ruby. I'm Jeong Ruby!

452
00:29:08,567 --> 00:29:11,000
When Ruby's memory came back,

453
00:29:11,000 --> 00:29:13,934
she must have been furious.

454
00:29:13,934 --> 00:29:15,942
If it had been me, I would've told everyone

455
00:29:16,734 --> 00:29:19,200
and made you pay.

456
00:29:19,002 --> 00:29:21,269
So how can you ask Ruby to hold a press

457
00:29:21,467 --> 00:29:24,801
conference and say the video was fake?

458
00:29:24,801 --> 00:29:27,100
Are you even human?

459
00:29:27,001 --> 00:29:28,769
How can you do such a thing

460
00:29:28,868 --> 00:29:31,434
and still call yourself a human being?

461
00:29:31,434 --> 00:29:33,067
What about you?

462
00:29:33,067 --> 00:29:35,868
Why didn't you do something about it?

463
00:29:35,868 --> 00:29:37,601
If you pitied her that much,

464
00:29:37,601 --> 00:29:40,434
why didn't you just expose me?

465
00:29:41,367 --> 00:29:44,701
How can you...

466
00:29:44,701 --> 00:29:46,767
It's because you knew!

467
00:29:46,767 --> 00:29:48,772
You knew we could never go back.

468
00:29:49,267 --> 00:29:52,000
Why? Because I married Gyeongmin

469
00:29:52,000 --> 00:29:54,167
and almost had his baby.

470
00:29:59,004 --> 00:30:00,010
Why'd you hit me?

471
00:30:01,000 --> 00:30:03,234
What gives you the right to slap me?

472
00:30:03,234 --> 00:30:05,267
Ever since we were young, you only hit me.

473
00:30:05,267 --> 00:30:08,133
Why do you hate me so much?

474
00:30:10,003 --> 00:30:14,370
How could you? How could you?

475
00:30:14,667 --> 00:30:18,100
How could you do such terrible things to Ruby?

476
00:30:19,002 --> 00:30:20,536
Ruby was so good to you!

477
00:30:20,734 --> 00:30:22,701
You were so precious to her!

478
00:30:25,033 --> 00:30:27,067
She not only took care of herself,

479
00:30:27,067 --> 00:30:29,033
she always had your back

480
00:30:29,033 --> 00:30:31,067
whenever you caused trouble.

481
00:30:31,067 --> 00:30:34,801
How could you do such an unspeakable thing?

482
00:30:34,801 --> 00:30:36,767
You and I should just kill ourselves.

483
00:30:36,767 --> 00:30:38,100
If you and I die right here and now,

484
00:30:38,001 --> 00:30:39,335
all this will come to an end.

485
00:30:39,434 --> 00:30:40,500
Let's die. Let's die.

486
00:30:40,005 --> 00:30:42,638
No. Why should I?

487
00:30:43,133 --> 00:30:44,467
If I wanted to die,

488
00:30:44,467 --> 00:30:45,968
I would've done it when I ran away.

489
00:30:45,968 --> 00:30:47,667
But you saw what happened.

490
00:30:47,667 --> 00:30:50,534
I wasn't destined to die in vain.

491
00:30:51,701 --> 00:30:56,200
I can't give up. No. I won't give up.

492
00:30:58,734 --> 00:31:01,500
Why did you become this way?

493
00:31:02,434 --> 00:31:03,439
How badly did I fail to raise you

494
00:31:03,934 --> 00:31:05,938
to make you turn out so wicked?

495
00:31:06,968 --> 00:31:09,977
Runa. Runa.

496
00:31:15,133 --> 00:31:17,834
Do you hate me that much?

497
00:31:17,834 --> 00:31:20,601
Do you want me to disappear?

498
00:31:20,601 --> 00:31:23,133
Is Ruby all you need?

499
00:31:25,001 --> 00:31:26,168
Ever since I was young,

500
00:31:26,267 --> 00:31:28,270
no one gave me any recognition.

501
00:31:28,567 --> 00:31:30,569
Nobody loved me.

502
00:31:31,234 --> 00:31:34,133
But now, everyone recognizes me.

503
00:31:34,133 --> 00:31:36,767
Everyone tells me how smart I am.

504
00:31:36,767 --> 00:31:40,500
So why are you getting in the way?

505
00:31:42,567 --> 00:31:44,100
I'm not the troublemaker, seductress

506
00:31:44,001 --> 00:31:46,010
and extortionist I used to be.

507
00:31:47,000 --> 00:31:50,634
I'll be the intelligent, capable, Jeong Runa

508
00:31:50,634 --> 00:31:54,133
that everyone in the world will recognize.

509
00:31:54,133 --> 00:31:58,133
No. I'm going to be even better

510
00:31:58,133 --> 00:32:00,142
than the Jeong Ruby you used to know.

511
00:32:01,033 --> 00:32:02,701
You just watch.

512
00:32:02,701 --> 00:32:04,868
No matter what you say,

513
00:32:04,868 --> 00:32:07,133
I'm Jeong Ruby now.

514
00:32:09,234 --> 00:32:12,133
I'm sorry.

515
00:32:12,133 --> 00:32:15,634
I'm sorry. It's all my fault.

516
00:32:18,000 --> 00:32:23,868
Please, Runa. I'm begging you.

517
00:32:23,868 --> 00:32:27,601
Please stop, okay?

518
00:32:30,634 --> 00:32:33,167
No. You stop.

519
00:32:33,167 --> 00:32:36,868
I'm not giving up, so talk to Ruby.

520
00:32:36,868 --> 00:32:38,200
If they find out the video is real,

521
00:32:38,002 --> 00:32:39,935
I'm done for.

522
00:32:40,133 --> 00:32:42,901
And if I go down, Ruby's coming with me.

523
00:32:42,901 --> 00:32:44,909
You and Aunt Chorim will be ruined too.

524
00:32:47,234 --> 00:32:49,238
Hurry up and make Ruby

525
00:32:49,634 --> 00:32:51,636
hold a press conference.

526
00:32:51,834 --> 00:32:59,367
If you don't, I'll really end everything.

527
00:33:06,000 --> 00:33:09,167
That's right. You raised me to be like this.

528
00:33:09,167 --> 00:33:12,171
You hated me for not being your real child.

529
00:33:12,567 --> 00:33:15,133
But I'm going to show you, mom.

530
00:33:15,133 --> 00:33:16,767
The ugly duckling

531
00:33:16,767 --> 00:33:19,634
will become a beautiful white swan.

532
00:33:52,834 --> 00:33:54,901
See you next time...

