﻿1
00:04:52,622 --> 00:04:55,348
Hey, this was your idea.

2
00:04:55,711 --> 00:04:58,790
- Give me your key.
- You don't have yours?

3
00:04:58,825 --> 00:05:01,884
- Uh-uh
- Here, I got it.

4
00:05:03,047 --> 00:05:06,464
Babe, that is the most obvious
place in the world, OK?

5
00:05:09,497 --> 00:05:12,047
Luckily we got nothing
worth stealing.

6
00:05:14,597 --> 00:05:17,297
Let's do it.

7
00:05:25,609 --> 00:05:27,230
Don't run so fast.

8
00:07:15,475 --> 00:07:17,175
I don't understand.

9
00:07:17,651 --> 00:07:19,449
What's going on?

10
00:07:22,922 --> 00:07:25,155
Why did you set the alarm?

11
00:07:25,190 --> 00:07:26,824
I didn't set it.

12
00:07:26,859 --> 00:07:32,055
- But I don't want to go running.
- I didn't set it, Amy.

13
00:07:33,852 --> 00:07:36,330
Well, a fucking elf must have set it.

14
00:08:05,129 --> 00:08:06,994
What are these?

15
00:08:45,369 --> 00:08:46,369
Jesus!

16
00:08:46,869 --> 00:08:48,369
Christ, Amy.

17
00:09:45,031 --> 00:09:47,142
Aww man...

18
00:09:49,763 --> 00:09:54,642
This is really not cool.
I got up at 5 in the morning...

19
00:09:54,677 --> 00:09:56,590
Just gotta help me out here.

20
00:10:18,441 --> 00:10:20,590
Did someone send this to you?

21
00:10:20,625 --> 00:10:22,410
To me?

22
00:10:22,445 --> 00:10:27,949
Yeah. One of your little undergrad students? Or
the professor, what-his-face with the nose-hairs?

23
00:10:27,984 --> 00:10:33,189
- No, I highly doubt it.
- Because I didn't make it.

24
00:10:34,640 --> 00:10:40,270
OK James, you have so much music,
every time I walk into your office,

25
00:10:40,305 --> 00:10:42,339
you are downloading something.

26
00:10:42,374 --> 00:10:44,743
I don't even make CDs anymore.

27
00:10:44,778 --> 00:10:49,189
OK.  So what do you think? Somebody
just made a CD and put it in the car?

28
00:10:49,091 --> 00:10:52,189
Wasn't this car door locked?

29
00:10:52,405 --> 00:10:56,889
Yes? OK
So all I know is that I didn't make it.

30
00:10:56,924 --> 00:10:59,139
I didn't make it either, Amy.

31
00:10:59,174 --> 00:11:00,502
So made it?

32
00:11:00,537 --> 00:11:03,339
Isn't it weird to you, at all?

33
00:11:04,391 --> 00:11:07,088
Yes, I don't know, maybe.

34
00:11:08,377 --> 00:11:10,562
You... You forget things sometimes.

35
00:11:10,897 --> 00:11:12,365
What's that supposed to mean?

36
00:11:12,455 --> 00:11:16,965
Nothing, I'm just saying that you
forget things sometimes.  You know?

37
00:11:17,288 --> 00:11:19,447
What? Like when I drink?

38
00:11:20,197 --> 00:11:23,401
James ... All I'm saying is that I bet

39
00:11:23,402 --> 00:11:27,535
all of this music is right
on your computer.

40
00:11:33,268 --> 00:11:35,068
No... No...

41
00:11:35,168 --> 00:11:38,793
Wait, what's that?

42
00:11:45,968 --> 00:11:49,418
- Are those the same songs?
- What?

43
00:11:49,453 --> 00:11:54,134
- See? I told you.
- What is this, your idea of ​​a joke?

44
00:11:55,024 --> 00:11:58,642
- What, you think that I put those on there?
- I know I sure as hell didn't.

45
00:11:59,161 --> 00:12:03,599
James, you have 8,000 songs on your
computer.  You probably just forgot.

46
00:12:05,335 --> 00:12:07,308
You think I have nothing better to do

47
00:12:07,409 --> 00:12:10,481
than to come in to your office,
sneak around and make a mix CD?

48
00:12:10,916 --> 00:12:13,114
Honestly, I don't know what it
is you do here all day Amy.

49
00:12:13,149 --> 00:12:17,910
You've been working on this afghan
embroidery thesis for 7 years...

50
00:12:18,245 --> 00:12:20,743
Or what is it eight?

51
00:12:22,864 --> 00:12:24,660
You're an asshole.

52
00:12:24,695 --> 00:12:27,084
- Relax, I was joking.
- No you're not.

53
00:12:27,903 --> 00:12:30,232
Hey, come on,
help me figure this out.

54
00:12:31,121 --> 00:12:34,871
- Nasty little shit.
- Amy, please.

55
00:12:42,867 --> 00:12:43,704
Amy.

56
00:12:46,970 --> 00:12:47,770
Hey.

57
00:12:51,667 --> 00:12:53,620
Come on, Amy.

58
00:14:48,012 --> 00:14:49,606
Is that you?

59
00:16:05,009 --> 00:16:05,809
Amy.

60
00:16:08,309 --> 00:16:09,858
Hey, you home?

61
00:16:28,858 --> 00:16:29,758
Amy.

62
00:16:50,708 --> 00:16:53,315
- Katherine, it's James.
- Yeah, what's up?

63
00:16:53,350 --> 00:16:56,933
- Is Amy there with you?
- No, why would she be here?

64
00:16:56,968 --> 00:16:59,644
- Are you sure?
- Of course I'm sure.

65
00:17:00,164 --> 00:17:04,507
Did she, ah...
say she was going away at all?

66
00:17:04,542 --> 00:17:07,216
- No...
- She didn't?

67
00:17:07,251 --> 00:17:08,345
No.

68
00:17:08,380 --> 00:17:11,869
I got this note from her,
saying that she

69
00:17:11,904 --> 00:17:13,978
... needed to clear her head.

70
00:17:14,013 --> 00:17:16,895
- Why?
- She didn't say.

71
00:17:16,930 --> 00:17:19,108
Did you guys have a fight
or something?

72
00:17:19,143 --> 00:17:21,744
No.  We...
We didn't have a fight.

73
00:17:22,285 --> 00:17:24,732
- Is she mad at you?
- Not that I know of.

74
00:17:24,767 --> 00:17:26,251
Not that you know of.
What does that mean?

75
00:17:26,224 --> 00:17:28,017
It means what it means,
not that I know of.

76
00:17:28,152 --> 00:17:30,543
Well, then James,
I don't know where she is.

77
00:17:31,229 --> 00:17:35,077
OK.  But if you talk to her,
can you tell her to call me?

78
00:17:35,078 --> 00:17:37,368
... Please. I'm worried.

79
00:17:37,403 --> 00:17:39,771
- What are you talking about?
- Can you just do that?

80
00:17:39,806 --> 00:17:41,868
- Fine.
- Thank you.

81
00:17:53,347 --> 00:17:56,502
<i>Hi, you've reached Amy's cell.
Leave a message.</i>

82
00:18:30,282 --> 00:18:31,182
Hello

83
00:18:32,080 --> 00:18:32,880
Amy

84
00:18:42,080 --> 00:18:42,880
Amy

85
00:18:46,157 --> 00:18:47,655
Amy, is that you?

86
00:19:25,525 --> 00:19:27,641
Hey kitty, you scared me,

87
00:19:27,676 --> 00:19:29,932
I thought I left you outside.

88
00:19:31,131 --> 00:19:32,938
Come here.

89
00:20:42,884 --> 00:20:44,605
Hello.

90
00:20:46,005 --> 00:20:46,805
Amy

91
00:21:07,939 --> 00:21:11,173
He likes the oldies.

92
00:21:11,923 --> 00:21:13,795
Who ... likes the oldies?

93
00:21:15,326 --> 00:21:17,213
Whoever made this.

94
00:21:17,248 --> 00:21:19,815
- You like this kind of stuff.
- I like everything.

95
00:21:20,508 --> 00:21:23,820
You really didn't, as some
kinda joke or something?

96
00:21:23,855 --> 00:21:25,612
Is this funny?

97
00:21:30,115 --> 00:21:33,646
- Did she pack a bag?
- A bag was packed.

98
00:21:33,681 --> 00:21:38,063
Ok. So there you go.
She's... she's clearing her head.
She's taking a vacation.

99
00:21:38,298 --> 00:21:40,031
From what?

100
00:21:40,131 --> 00:21:41,631
From...

101
00:21:44,202 --> 00:21:47,858
- You gotta admit you're not always the ...
- What?

102
00:21:49,691 --> 00:21:51,910
I don't know.

103
00:21:53,110 --> 00:21:55,208
I don't think Amy made this CD.

104
00:21:55,243 --> 00:21:58,101
- Who did then?
- I don't know.

105
00:22:00,295 --> 00:22:02,596
You think somebody's screwing
with your head?

106
00:22:02,986 --> 00:22:04,284
I don't know.

107
00:22:04,323 --> 00:22:09,084
OK. Do you have a crazy ex-girlfriend?
Somebody you work with?

108
00:22:11,880 --> 00:22:13,708
Bill Burrows, maybe?

109
00:22:17,979 --> 00:22:21,683
Yeah we were...
really hard on that guy.

110
00:22:22,426 --> 00:22:26,777
- We?
- No one laughed harder than you, Alex.

111
00:22:44,149 --> 00:22:46,747
You know...
The original version of this song,

112
00:22:48,514 --> 00:22:51,850
They don't say "Jill."
They say "Bill."

113
00:22:53,009 --> 00:22:54,732
His name was Bill.

114
00:23:29,120 --> 00:23:31,827
- Hey Cherrie.
- Hey.

115
00:23:33,237 --> 00:23:34,919
What's up?

116
00:23:34,954 --> 00:23:37,622
We're cool, right?

117
00:23:40,070 --> 00:23:41,495
Yeah

118
00:23:42,158 --> 00:23:44,372
Are...
Are you sure?

119
00:23:45,653 --> 00:23:47,372
Why wouldn't we be cool?

120
00:23:47,407 --> 00:23:50,547
There is no reason why.
I just, uh...

121
00:23:52,720 --> 00:23:54,047
I dunno.
Uhm...

122
00:23:56,199 --> 00:23:58,864
We're cool, right?

123
00:23:59,670 --> 00:24:01,342
Yeah.

124
00:24:01,920 --> 00:24:03,739
Get over yourself, James.

125
00:24:04,639 --> 00:24:10,207
Yeah.  That's ... uh ...
That's what I thought.

126
00:24:23,891 --> 00:24:25,701
Amy, it's me again.

127
00:24:26,726 --> 00:24:29,426
Look, If you don't want me to know
where you are, that's fine.

128
00:24:29,461 --> 00:24:31,856
But just let me know you're okay.
Alright?

129
00:24:33,218 --> 00:24:35,127
With everything going on right now,

130
00:24:35,162 --> 00:24:39,925
It would be really shitty of you if you
just took off on vacation or something.

131
00:24:42,327 --> 00:24:47,662
So just let me know you're OK.
That's it.  I'm worried about you.

132
00:25:43,701 --> 00:25:44,601
Hello?

133
00:26:47,099 --> 00:26:49,691
Real funny...

134
00:26:51,924 --> 00:26:53,922
Real god damn funny.

135
00:27:16,319 --> 00:27:17,224
Katherine

136
00:27:17,606 --> 00:27:18,924
I know you're up there.

137
00:27:44,664 --> 00:27:45,724
Are you in on it too?

138
00:27:45,754 --> 00:27:47,354
- What are you doing?
- Nice work!

139
00:27:57,367 --> 00:28:00,174
... Well, she's gotta be here because
I don't know where she is.

140
00:28:05,764 --> 00:28:10,399
My wife is missing and I know that
she is hiding out at your guys' place.

141
00:28:17,687 --> 00:28:20,575
- Was it you?
- Get out of my house.

142
00:28:20,775 --> 00:28:22,475
That's what you did.
You set it [garbled]

143
00:28:27,080 --> 00:28:28,475
... it's a really bad day for this ...

144
00:28:30,999 --> 00:28:33,475
Serves you right for
screwing around on her.

145
00:28:33,651 --> 00:28:36,346
That's what this is?
It's teach James a lesson?

146
00:28:36,381 --> 00:28:39,624
I told you to leave.

147
00:28:39,659 --> 00:28:42,323
You know what?
She hit me in the head.

148
00:28:42,358 --> 00:28:46,382
- You were hurting her.
- We were drunk.  We were fooling around.
Jesus!

149
00:28:50,150 --> 00:28:52,908
Stay away from me. Stay away
from my house.  Got it?

150
00:28:54,050 --> 00:28:57,198
I'm serious, stay away
from my house.

151
00:28:58,959 --> 00:29:01,305
Tell your sister I know
what she's up to.

152
00:30:19,968 --> 00:30:22,815
Hey Bill.
James.

153
00:30:23,588 --> 00:30:25,952
Good to see you.

154
00:30:28,177 --> 00:30:29,626
What's up?

155
00:30:29,661 --> 00:30:32,404
I was just ah ...
in the neighborhood,

156
00:30:32,439 --> 00:30:34,010
Seeing if you, I don't know ...

157
00:30:34,110 --> 00:30:37,910
If you're not busy, if you want to take a
little drive and grab a coffee or something.

158
00:30:38,045 --> 00:30:40,201
... Something I wanted to talk to
you about for a long time.

159
00:30:43,154 --> 00:30:45,467
You sure we can't get you some
breakfast?  Some eggs?

160
00:30:47,917 --> 00:30:50,485
- Coffee?
- Nah, I'm good.

161
00:30:52,047 --> 00:30:54,453
I was surprised I caught you at
home this time of day.

162
00:30:58,237 --> 00:31:00,155
I work evenings, mostly.

163
00:31:01,279 --> 00:31:03,032
Doing what?

164
00:31:04,504 --> 00:31:08,881
They got me down at the
humane society some nights.

165
00:31:08,916 --> 00:31:11,019
Oh yeah?
You an animal lover?

166
00:31:13,542 --> 00:31:16,680
Someone's gotta be there at nights.

167
00:31:16,715 --> 00:31:19,991
You know, in case there's
any freak outs.

168
00:31:21,116 --> 00:31:23,465
My cousin got me the job.

169
00:31:25,605 --> 00:31:29,464
Shit.  Sorry if I woke you
up then, working nights.

170
00:31:30,664 --> 00:31:36,666
- No, I don't sleep much.
- Why not?

171
00:31:39,388 --> 00:31:42,366
I heard you were in Afghanistan.

172
00:31:45,518 --> 00:31:47,546
Yeah ...
I remember you were all into

173
00:31:47,547 --> 00:31:51,963
military stuff in school.
Joined the cadets, right?

174
00:31:52,729 --> 00:31:55,413
I remember you got teased
a little bit for that.

175
00:31:56,913 --> 00:31:59,295
Yeah, I suppose.

176
00:32:01,865 --> 00:32:07,080
We were friends in third grade,
you and me.  If you remember...

177
00:32:07,299 --> 00:32:10,423
We would play Star Trek on
the playground.

178
00:32:10,458 --> 00:32:13,003
I'd always be Kirk and
you'd be Spock.

179
00:32:14,504 --> 00:32:16,554
Remember that?
You used to do the vulcan...

180
00:32:17,617 --> 00:32:20,462
Yeah...
Yeah, I got it.

181
00:32:22,874 --> 00:32:25,131
And then we just kind of
drifted apart.

182
00:32:28,741 --> 00:32:31,961
But still, I feel shitty for ...

183
00:32:33,862 --> 00:32:35,912
... For teasing,
You know?

184
00:32:35,947 --> 00:32:40,477
I feel uh ...
shitty for my part in that and ...

185
00:32:43,135 --> 00:32:47,078
it must make you angry,
to think back on those days.

186
00:32:48,556 --> 00:32:54,466
Now I, I know that it was mean.
It was ... really mean.

187
00:32:59,469 --> 00:33:01,174
It was just...

188
00:33:01,175 --> 00:33:06,859
... It was weird.
We were friends for a year.

189
00:33:06,894 --> 00:33:10,713
And then I spent the next ten
torturing you.

190
00:33:12,074 --> 00:33:15,138
It was just relentless.
I mean ...

191
00:33:17,339 --> 00:33:20,165
It must have just seemed
totally random to you.

192
00:33:20,611 --> 00:33:23,214
You know, why?
Why me?

193
00:33:23,414 --> 00:33:26,414
And I, I can't even answer that
question.  To be honest.

194
00:33:27,582 --> 00:33:31,866
It was just ...
our screwed up idea of having fun.

195
00:33:44,069 --> 00:33:45,850
Hey, are you okay?

196
00:33:45,885 --> 00:33:49,665
- Huh?  Yeah. Sorry.
- It's alright.

197
00:33:51,106 --> 00:33:53,995
Hey, it's good to take your
stuff home with you.

198
00:33:53,996 --> 00:33:57,308
But uh ... it's gotta get done.

199
00:33:57,912 --> 00:34:00,424
Yeah.
I know.

200
00:34:01,924 --> 00:34:05,235
I have a bit of a situation.

201
00:34:06,936 --> 00:34:08,711
... I will get it to you.

202
00:34:10,285 --> 00:34:13,530
What's the ETA on the
Myers account?

203
00:34:18,919 --> 00:34:22,211
- James!
- Yeah... Sorry.

204
00:34:22,246 --> 00:34:24,748
The Myers account?
When's that happening?

205
00:34:27,581 --> 00:34:30,464
Really sorry.
I have a bit of a situation at home.

206
00:34:31,565 --> 00:34:35,233
But I will have that to you
by the end of the day.

207
00:34:36,651 --> 00:34:38,813
- Yeah.
- I promise.
- OK

208
00:35:31,542 --> 00:35:33,616
Amy are you home?

209
00:35:40,931 --> 00:35:42,378
Aw, come on!

210
00:36:27,145 --> 00:36:30,426
Hey Diggs.
What are you doing?

211
00:36:37,410 --> 00:36:39,119
You alright, boy?

212
00:36:46,542 --> 00:36:48,451
So you don't think
this is your cat?

213
00:36:48,486 --> 00:36:52,030
No, he's different, skinnier.

214
00:36:53,230 --> 00:36:54,330
There! That.

215
00:36:54,341 --> 00:36:56,283
He's never done that before.

216
00:36:57,359 --> 00:37:00,453
- He's never growled before?
- Not like that, not at me.

217
00:37:02,629 --> 00:37:04,922
But otherwise, he
looks the same.

218
00:37:05,703 --> 00:37:09,118
Yeah. Like I said,
it's not just the cat,

219
00:37:09,153 --> 00:37:12,166
There's the CD, someone's been
messing around with my computer.

220
00:37:12,366 --> 00:37:13,866
Something is going on.

221
00:37:14,839 --> 00:37:17,665
This is really what concerns
me.  Right here.

222
00:37:25,349 --> 00:37:27,151
Is this her handwriting?

223
00:37:27,739 --> 00:37:29,241
Yeah, I think so.
But...

224
00:37:30,041 --> 00:37:30,841
So...

225
00:37:31,045 --> 00:37:35,156
- She needed to clear her head.
- Something is going on.

226
00:37:37,294 --> 00:37:39,416
I mean, really, Mr Deakin.

227
00:37:39,451 --> 00:37:42,298
Who's going go and
replace your cat?

228
00:38:46,641 --> 00:38:48,223
Hello...

229
00:38:52,651 --> 00:38:54,223
Anybody home?

230
00:39:14,524 --> 00:39:16,029
Anybody here?

231
00:39:24,457 --> 00:39:26,071
Hello?

232
00:39:38,991 --> 00:39:40,885
- What the hell are you doing?
- Jesus!

233
00:39:42,179 --> 00:39:44,714
- How did you get in here?
- I want to speak to Amy.

234
00:39:44,749 --> 00:39:48,248
- Come on, let's go.
- What are you doing?

235
00:39:48,283 --> 00:39:50,335
- Get out.
- What are you doing home on a work day?

236
00:39:50,532 --> 00:39:52,500
- Just get out of here.
- I want to speak to Amy.

237
00:39:52,535 --> 00:39:54,318
- Amy
- She's not here.
- Amy

238
00:39:55,000 --> 00:39:57,326
- Amy!
- Katherine, she's not here!

239
00:39:57,461 --> 00:40:01,073
You know what?  She's not answering her
phone, so I want to know what's going on.

240
00:40:01,901 --> 00:40:03,894
Just get out.  Go!

241
00:40:03,939 --> 00:40:05,194
- Amy!
- Go!

242
00:40:07,540 --> 00:40:09,450
I want to talk to
my sister, James.

243
00:40:09,485 --> 00:40:11,656
Yeah, and I want to talk
to my wife.  Get out!

244
00:40:14,301 --> 00:40:16,709
- Come on Katherine, get out of my house.
- Don't touch me!

245
00:40:16,744 --> 00:40:18,600
- What is the matter with you?
- Get out!

246
00:40:23,193 --> 00:40:26,319
- You're hurting my arm!
- Get the hell out of here then!

247
00:40:28,135 --> 00:40:30,001
You've done something,
haven't you?

248
00:40:31,101 --> 00:40:31,701
Listen...

249
00:40:31,787 --> 00:40:34,756
So why can't I go in?

250
00:40:34,891 --> 00:40:37,320
There is a situation.
I don't want you meddling.

251
00:40:37,355 --> 00:40:40,758
- Meddling in what?
- I'm handling it, OK?  Just go!

252
00:40:40,858 --> 00:40:42,358
Get your hands off me!

253
00:40:43,033 --> 00:40:45,107
I'm really calling the
police now, James!

254
00:40:45,142 --> 00:40:47,496
I called the police.
They don't do shit.

255
00:40:48,860 --> 00:40:50,357
I'm dealing with it.

256
00:41:22,218 --> 00:41:23,518
Jesus Christ!

257
00:41:53,123 --> 00:41:54,830
What the hell?

258
00:42:05,427 --> 00:42:07,420
Yes!
They cut his head off.

259
00:42:07,455 --> 00:42:09,462
But you said the fake
cat is still there?

260
00:42:09,562 --> 00:42:11,362
Yeah, it ...

261
00:42:12,095 --> 00:42:15,193
Yes, Amy's still gone.
I haven't heard from her.

262
00:42:15,228 --> 00:42:17,619
- Did you check with work?
- Yeah, yes.

263
00:42:17,806 --> 00:42:20,019
I called all her friends,
I checked with the school,

264
00:42:20,154 --> 00:42:22,558
I checked her email, I called the
airlines to see if she booked a flight.

265
00:42:22,658 --> 00:42:23,258
Nothing

266
00:42:23,393 --> 00:42:26,029
She asked you not to contact her
though, right?

267
00:42:26,064 --> 00:42:28,755
I told you, I don't think
she wrote that note.

268
00:42:31,940 --> 00:42:34,262
Listen, I need you to figure
out what's going on.

269
00:42:34,297 --> 00:42:36,349
Alright?
I need you to find my wife.

270
00:42:38,897 --> 00:42:42,649
- We can file a report.
- That's great.

271
00:42:42,684 --> 00:42:45,903
Without any real evidence,
it is all we can do.

272
00:42:46,357 --> 00:42:49,657
Listen...  I'll just handle
it myself then.

273
00:42:49,757 --> 00:42:51,757
Mr Deakin.

274
00:42:54,777 --> 00:42:58,623
Don't believe me?
I'll get you some evidence then.

275
00:43:42,362 --> 00:43:43,362
Amy?

276
00:43:43,387 --> 00:43:45,069
Amy, you there?

277
00:43:48,601 --> 00:43:50,416
Who is this?

278
00:44:06,656 --> 00:44:07,956
Jesus.

279
00:45:19,718 --> 00:45:21,706
Jesus Christ.

280
00:45:28,135 --> 00:45:30,170
Who the hell?

281
00:46:49,574 --> 00:46:51,673
Yeah, give me the police.

282
00:47:42,153 --> 00:47:43,951
Is anybody down there?

283
00:47:55,583 --> 00:47:58,352
Hello, is anybody down there?

284
00:48:13,224 --> 00:48:15,174
Alright, it takes a second.

285
00:48:19,378 --> 00:48:21,162
Right there.

286
00:48:23,503 --> 00:48:24,967
Who is it?

287
00:48:25,402 --> 00:48:27,383
I don't know who it is.

288
00:48:30,219 --> 00:48:34,985
Now watch this.

289
00:48:35,828 --> 00:48:38,629
He obviously got in here.

290
00:48:39,916 --> 00:48:42,370
He said they did something
to your cat.

291
00:48:43,416 --> 00:48:46,716
Did something?
He cut his head off.

292
00:48:48,689 --> 00:48:51,291
But there is still a cat.

293
00:48:51,326 --> 00:48:55,722
- Yeah there is a cat, it's a fake cat.
- It's a fake cat?

294
00:48:55,757 --> 00:49:00,216
- Yeah, he looks the same but it's ...
- But you're sure it's a fake?

295
00:49:00,251 --> 00:49:02,166
Yeah I'm sure. Come here.

296
00:49:05,246 --> 00:49:07,999
That's what my cat looks like now.

297
00:49:12,665 --> 00:49:16,117
Jesus Christ!
It's gone!

298
00:49:16,152 --> 00:49:18,215
What, you lost the head?

299
00:49:19,011 --> 00:49:22,632
No!  Whoever did this came
and they took it back.

300
00:49:22,667 --> 00:49:24,965
They took the cat head back?

301
00:49:25,575 --> 00:49:28,135
You saw the video. Someone's
been coming and going in here

302
00:49:28,170 --> 00:49:31,567
Right.  So you, you put the
cat head in the freezer?

303
00:49:32,492 --> 00:49:35,659
Yeah.  This hasn't come
up a lot in my life.

304
00:49:35,660 --> 00:49:39,643
Is there a different protocol for severed
cat head storage I should know about?

305
00:49:40,414 --> 00:49:42,904
I'm going to need you to
calm down, sir.  Alright?

306
00:49:42,939 --> 00:49:46,256
Calm down?  When's the last time
your cat was murdered?

307
00:49:46,291 --> 00:49:48,667
When's the last time your
wife just disappeared?

308
00:49:48,702 --> 00:49:51,382
There was a note though, right?
From your wife.

309
00:49:52,138 --> 00:49:57,971
Yeah...
What are you going to do?

310
00:49:58,871 --> 00:50:01,350
What are you going to
do about this?

311
00:50:02,479 --> 00:50:04,571
What would you like us to do
about it Mr Deakin?

312
00:50:05,815 --> 00:50:08,403
Take it seriously for starters.

313
00:50:08,438 --> 00:50:13,462
I can assure you we are taking the
disappearance of your wife very seriously.

314
00:50:16,571 --> 00:50:19,920
Did you or your wife have any
trouble with any neighbors lately?

315
00:50:20,520 --> 00:50:21,120
No.

316
00:50:21,323 --> 00:50:23,519
Anyone from work, school?

317
00:50:25,970 --> 00:50:26,682
No.

318
00:50:27,617 --> 00:50:30,668
OK.  Can you think of anyone who
might want to do this to you?

319
00:50:44,603 --> 00:50:48,139
The last time we met, I remembered
you were a big Jays fan.

320
00:50:48,913 --> 00:50:51,603
Got the whole team on there.
'93 championship team.

321
00:50:52,868 --> 00:50:57,651
- I know it.  It looks new.
- It's been conditioned.

322
00:50:57,686 --> 00:51:01,527
You're the first person to touch
it in probably 20 years.

323
00:51:10,102 --> 00:51:12,762
This must seem pretty
weird to you, huh?

324
00:51:13,852 --> 00:51:16,727
I'm just having a bad week.

325
00:51:19,554 --> 00:51:25,899
In fact you might say I have a new
found understanding for probably
what you went through in school.

326
00:51:26,149 --> 00:51:28,151
Just to be harassed, you know?

327
00:51:29,733 --> 00:51:33,386
Not know what's coming next.
Not know what ...

328
00:51:33,923 --> 00:51:36,093
horrible shit they're
going to do next.

329
00:51:43,857 --> 00:51:46,459
Look, I'm sorry about
all that stuff.

330
00:51:46,460 --> 00:51:50,036
You know?
I mean it Bill.

331
00:51:50,071 --> 00:51:53,904
Anything you need ever,
you let me know.

332
00:51:55,980 --> 00:52:01,155
I'm serious, man. I'm not that
guy from school, you know?  I ...

333
00:52:04,404 --> 00:52:08,606
That guy was a monster, I'm not him.

334
00:52:08,641 --> 00:52:10,727
Yeah.  Alright.

335
00:52:16,536 --> 00:52:18,203
Has anyone ever done this before?

336
00:52:19,815 --> 00:52:21,065
No.

337
00:52:23,390 --> 00:52:25,230
Apologized for bad stuff ...

338
00:52:25,265 --> 00:52:29,787
- No, nobody has ever done this before.
- Does it help?

339
00:52:35,638 --> 00:52:37,731
Bad stuff still happened.

340
00:52:51,446 --> 00:52:53,881
Let me ask you a question, Bill.

341
00:52:55,401 --> 00:52:57,229
Do you know where my wife is?

342
00:52:59,429 --> 00:53:04,677
- You don't know where your wife is?
- No, I don't.

343
00:53:05,599 --> 00:53:07,866
Well, how would I know
where your wife is?

344
00:53:10,530 --> 00:53:14,765
I'm not ...
I'm not screwing around here, OK?

345
00:53:15,121 --> 00:53:18,464
If you've got a problem with me,
let's keep it between you and me.

346
00:53:18,599 --> 00:53:20,788
Amy didn't do anything wrong, OK?

347
00:53:26,131 --> 00:53:30,023
- Something funny?
- I'm just gonna...

348
00:53:30,058 --> 00:53:34,449
You're not going anywhere Buck.
Where is Amy? Where the hell is she!

349
00:53:34,853 --> 00:53:37,978
You're still the same asshole
Deakin!  You know that?

350
00:53:41,028 --> 00:53:43,703
I'm wrong, I'm sorry OK?

351
00:53:44,104 --> 00:53:48,582
But you gotta understand what I'm
going through right now, alright?.

352
00:53:49,680 --> 00:53:53,778
I understand,
karma's a bitch.

353
00:54:07,855 --> 00:54:09,655
- I don't know.
- What do you mean you don't know?

354
00:54:09,690 --> 00:54:12,542
I don't know, Katherine.  I'll say
it again. I don't know where she is.

355
00:54:12,577 --> 00:54:16,105
As far as I know she could be right next
to you laughing her ass off right now.

356
00:54:16,140 --> 00:54:19,105
- James, I will bring the police.
- Bring the police over, Katherine!

357
00:54:19,140 --> 00:54:22,270
- Yeah, I will.
- They're not going to do anything.  Trust me.

358
00:54:22,305 --> 00:54:26,606
- Well I don't trust you, James.
- I'm, I'm hanging up the phone now

359
00:54:26,606 --> 00:54:28,426
Yeah, you better not
have done any...

360
00:54:42,284 --> 00:54:46,644
What's next?
What's next asshole!

361
00:54:52,221 --> 00:54:53,966
Bring it on ...

362
00:55:28,951 --> 00:55:30,351
Bill.

363
00:55:56,164 --> 00:55:57,564
Oh, Jesus.

364
00:56:10,464 --> 00:56:11,864
Amy.

365
00:56:44,600 --> 00:56:45,750
Open up Bill!

366
00:56:48,250 --> 00:56:50,100
Come on Bill!
Open the door!

367
00:57:04,415 --> 00:57:06,650
- What are you doing?
- Where is she?

368
00:57:06,685 --> 00:57:08,805
What do you want?

369
00:57:13,101 --> 00:57:18,086
Where she!
Is she in there?

370
00:57:24,901 --> 00:57:26,201
Amy!

371
00:57:27,901 --> 00:57:29,701
You got my fucking wife in here!

372
00:57:32,422 --> 00:57:33,922
I'm going to kill you!

373
00:57:34,022 --> 00:57:35,522
Piece of shit!

374
00:57:36,801 --> 00:57:38,801
- Where is she!
- Aaagh!

375
00:57:49,761 --> 00:57:50,861
Amy!

376
00:57:52,431 --> 00:57:53,531
Amy!

377
00:57:58,673 --> 00:57:59,773
Amy!

378
00:58:04,606 --> 00:58:07,595
I'm coming.
I'm coming!

379
00:58:34,023 --> 00:58:34,723
No.

380
00:58:35,944 --> 00:58:38,244
No.
No. No...

381
00:59:20,970 --> 00:59:23,115
Where the hell are you?

382
00:59:25,216 --> 00:59:27,338
Where the hell are you?

383
01:01:21,199 --> 01:01:24,768
- Hi.
- Is this a bad time, Mr Deakin?

384
01:01:25,518 --> 01:01:28,148
Well, huh, I was...

385
01:01:29,198 --> 01:01:33,337
Yeah.  Have you heard from Amy?
Anything on Amy?

386
01:01:33,372 --> 01:01:35,273
I'm afraid not.

387
01:01:35,947 --> 01:01:40,148
But we did get a call today,
however from a Bill Burrows.

388
01:01:40,783 --> 01:01:43,599
- What?
- He said you've been harassing him.

389
01:01:44,451 --> 01:01:47,649
- Today? He called?
- About an hour ago.

390
01:01:47,684 --> 01:01:50,660
- An hour ago?
- You're familiar with him then?

391
01:01:51,732 --> 01:01:57,099
Yeah, we went to school together,
I haven't seen him in years.

392
01:01:57,975 --> 01:02:02,350
Then you're suggesting he made
this all up out of the blue?

393
01:02:03,175 --> 01:02:05,649
No, I called him.

394
01:02:07,113 --> 01:02:11,649
Like I said, I was checking
people out.  From my past.

395
01:02:13,070 --> 01:02:15,605
- But you didn't see him?
- No.

396
01:02:18,358 --> 01:02:21,346
Do you mind if I come in
for a minute, Mr Deakin?

397
01:02:22,901 --> 01:02:26,350
- Why?
- Well, I just need to fill out a report.

398
01:02:27,597 --> 01:02:29,463
Well, I'm heading to work.

399
01:02:29,498 --> 01:02:31,603
If you prefer, we can go
down to the station.

400
01:02:34,904 --> 01:02:36,932
Yeah, sure.

401
01:02:41,133 --> 01:02:43,451
Why don't I go after you?

402
01:03:02,708 --> 01:03:05,701
This is really against my
better judgment, dude.

403
01:03:08,557 --> 01:03:12,156
You don't have to ...
Trust me, it's in there.

404
01:03:12,256 --> 01:03:13,496
Yeah.
OK. Thanks.

405
01:03:13,791 --> 01:03:17,908
- You're not going to use it, I hope?
- I don't know.

406
01:03:19,094 --> 01:03:22,783
- Well, you didn't get it from me, right?
- No, OK.

407
01:03:23,725 --> 01:03:25,654
You want to fill me in on
what is going on?

408
01:03:25,689 --> 01:03:29,214
I get calls every god damn day from
your sister in-law. She's freaking out.

409
01:03:29,249 --> 01:03:33,613
- Katherine called you?
- Yeah, she's worried about Amy.

410
01:03:35,703 --> 01:03:39,397
- You haven't spoken to her?
- Me? No.

411
01:03:40,128 --> 01:03:42,501
- Nothing happened?
- What do you mean?

412
01:03:42,536 --> 01:03:45,452
- Between the two of you?
- Like what?

413
01:03:45,487 --> 01:03:48,452
Anything I should be
worried about?

414
01:03:49,215 --> 01:03:53,854
Nah.  I'm dealing
with it, OK?

415
01:03:58,202 --> 01:04:01,125
- What about Bill?
- What about him?

416
01:04:01,854 --> 01:04:03,620
You eliminate him?

417
01:04:05,864 --> 01:04:07,652
What does that mean?

418
01:04:08,525 --> 01:04:11,403
From your list.  You said you
were going to go check him out.

419
01:04:15,605 --> 01:04:19,217
- Yeah, it's not Bill.
- Not Bill.

420
01:04:21,221 --> 01:04:23,217
No, not Bill.

421
01:05:29,308 --> 01:05:32,093
Whoever you are...

422
01:05:32,394 --> 01:05:34,270
I get the feeling you
can hear me.

423
01:05:35,065 --> 01:05:36,868
If you are somehow listening,

424
01:05:37,068 --> 01:05:38,468
I don't know why
you're doing this.

425
01:05:38,920 --> 01:05:41,330
I think you're playing some
kind of game with me.

426
01:05:42,668 --> 01:05:46,292
But I'm done.
I'm not playing any more. OK?

427
01:05:46,327 --> 01:05:48,118
If you want me, I'm here.

428
01:05:48,153 --> 01:05:51,614
If you want to play, then
come and see me.

429
01:07:16,893 --> 01:07:18,393
Hello...

430
01:07:18,428 --> 01:07:20,519
Need to talk to you.
Don't hang up.

431
01:07:20,719 --> 01:07:21,519
Don't...

432
01:07:21,691 --> 01:07:23,973
Hello.
Shit.

433
01:08:03,401 --> 01:08:05,866
Listen, I'm coming up.

434
01:08:07,666 --> 01:08:09,914
I just want to talk
to you, OK?

435
01:09:20,871 --> 01:09:21,671
Hey!

436
01:09:31,906 --> 01:09:32,706
Hey!

437
01:09:56,626 --> 01:10:00,135
Come on!
Come on!

438
01:10:45,640 --> 01:10:46,740
You sonafabitch!

439
01:10:47,078 --> 01:10:48,178
You sonafabitch!

440
01:10:49,621 --> 01:10:51,468
Come on, where'd you go?

441
01:11:20,678 --> 01:11:23,045
How would you feel about
me calling the police?

442
01:11:23,080 --> 01:11:27,927
White chevy van.
License plate EM36PC.

443
01:11:33,180 --> 01:11:35,132
Here's what were going to do.

444
01:11:36,415 --> 01:11:38,430
I'm going to come home
from work tomorrow.

445
01:11:39,176 --> 01:11:41,879
And Amy's going to be there,

446
01:11:41,880 --> 01:11:45,632
And everything's going to
be back to normal, OK?

447
01:11:46,237 --> 01:11:51,489
You're going to forget about me and I'm
going to forget about you. You hear me?

448
01:11:52,168 --> 01:11:55,679
Were going to move on, unless
you'd rather deal with the police.

449
01:11:55,714 --> 01:11:58,690
Because they could fry us both at
this point, if that's what you want.

450
01:12:03,323 --> 01:12:07,801
If you agree, then just
hang up the phone.

451
01:12:11,374 --> 01:12:15,179
I said if you agree ...

452
01:14:11,646 --> 01:14:12,746
Amy!

453
01:14:17,249 --> 01:14:18,349
Amy!

454
01:14:22,851 --> 01:14:23,951
Amy!

455
01:16:13,177 --> 01:16:14,677
Amy...

456
01:16:32,084 --> 01:16:33,484
Oh god...

457
01:17:58,176 --> 01:18:00,060
<i>Hello James.</i>

458
01:18:04,960 --> 01:18:09,513
Amy?
Amy it's Katherine.

459
01:18:10,860 --> 01:18:11,960
Mr Deakin.

460
01:18:15,930 --> 01:18:18,058
<i>I see you.</i>

461
01:18:25,624 --> 01:18:27,522
Deakin, you here?

462
01:19:16,132 --> 01:19:17,534
Son of a bitch!

463
01:19:29,613 --> 01:19:32,713
Nooo!  He killed her!

464
01:19:32,813 --> 01:19:34,379
Put the gun down now!

465
01:19:34,414 --> 01:19:37,024
- He killed her.
- No, It's not me.

466
01:19:38,648 --> 01:19:41,816
- Don't!
- She's dead.

467
01:19:48,613 --> 01:19:51,816
- Oh God, oh God!
- Katherine!

468
01:19:55,316 --> 01:19:58,716
- Oh God!
- No!  No!!!

469
01:20:02,774 --> 01:20:04,016
Shit.

470
01:26:20,646 --> 01:26:22,646
<< Transcribed by Substandard >>
<< July 27, 2016 >>
<< for bluray @ 23.976fps >>

