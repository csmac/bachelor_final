﻿1
00:03:43,707 --> 00:03:45,050
Yeah, I know that..

2
00:03:45,051 --> 00:03:47,349
and for every 9 stocks
that fell, 8 advanced..

3
00:03:47,350 --> 00:03:49,459
stop acting like it's
fucking Black Friday.

4
00:03:49,748 --> 00:03:52,886
Yeah.. alright, well then,
why don't you go back with that..

5
00:03:52,890 --> 00:03:56,440
huh, what's his name? That.. huh, asshole
who talked you into going on with Enron.

6
00:03:56,589 --> 00:03:58,907
Yeah..

7
00:03:58,940 --> 00:04:01,274
you know what?
Fuck you, too.

8
00:04:17,595 --> 00:04:18,969
Della?

9
00:04:22,491 --> 00:04:23,863
Della, honey?

10
00:04:26,395 --> 00:04:27,769
What?

11
00:04:28,348 --> 00:04:29,873
What's the matter?

12
00:04:30,394 --> 00:04:31,801
What's the matter?

13
00:04:31,867 --> 00:04:34,933
Honey, you wanna tell me
what it is you do all day?

14
00:04:37,371 --> 00:04:40,054
- What do you mean?
- Well..

15
00:04:41,339 --> 00:04:44,371
Honey,
look at the goddamn place, okay?

16
00:04:45,179 --> 00:04:49,487
There's kid-shit all over the place,
wall to goddamn wall.

17
00:04:50,267 --> 00:04:53,203
We do the best..
we do the best we can, I mean..

18
00:04:53,371 --> 00:04:57,678
The best you can? Why don't you
just do what I tell you to do?

19
00:04:57,679 --> 00:04:59,399
- Stop.
- Can I ask you a question?

20
00:04:59,400 --> 00:05:01,308
- What in the hell happened to you?
- Stop.

21
00:05:01,309 --> 00:05:03,898
You don't even try
to put yourself together anymore.

22
00:05:04,635 --> 00:05:08,123
I don't know what you
want me to say? We've got kids.

23
00:05:08,124 --> 00:05:12,372
Look, I want you to hear this,
I want you to keep the house clean!

24
00:05:14,043 --> 00:05:15,543
Oh.. god.

25
00:05:18,939 --> 00:05:20,497
Terry got an "A" in math.

26
00:05:20,498 --> 00:05:22,746
- Goddamn it, Della!
- Don't.

27
00:05:22,747 --> 00:05:25,371
- Don't what?
- No! It's her favorite doll.

28
00:05:25,372 --> 00:05:27,388
- What?
- You'll have to fix her.

29
00:05:27,389 --> 00:05:28,763
I have to what?

30
00:05:29,243 --> 00:05:32,155
- You'll have to fix her.
- I will have to what?

31
00:05:32,156 --> 00:05:33,398
You'll have..

32
00:05:34,331 --> 00:05:36,753
That's all you worry about.
Is these kids.

33
00:05:38,715 --> 00:05:42,424
I will have to what? Would you look
at me when I'm talking to you?

34
00:05:42,682 --> 00:05:44,056
Stop it!

35
00:05:45,243 --> 00:05:49,339
Why don't you stop it? And don't you
tell me what the fuck to do! Okay?

36
00:05:49,340 --> 00:05:51,705
- Stop it.
- You understand me?

37
00:05:51,803 --> 00:05:53,843
Do you hear what I'm saying to you?

38
00:05:55,130 --> 00:05:57,073
Don't break my arm.

39
00:05:57,851 --> 00:05:59,858
Don't tell me what to do!

40
00:06:06,939 --> 00:06:11,794
Why you fuck with me? And you do
that every fucking time?

41
00:06:19,611 --> 00:06:21,552
So, what're we supposed to do now?

42
00:06:23,387 --> 00:06:25,012
I'm.. um..

43
00:06:26,491 --> 00:06:28,432
I'm out of wrapping paper.

44
00:06:35,131 --> 00:06:37,104
It's okay.

45
00:06:37,883 --> 00:06:39,792
Come on, it's okay.

46
00:06:48,539 --> 00:06:51,258
Okay, it's okay!
How are you doing?

47
00:06:51,259 --> 00:06:54,356
I want a Nintendo Wii
for Christmas, mom.

48
00:06:54,939 --> 00:06:57,426
It's only a few hours
till Christmas.

49
00:06:57,500 --> 00:07:00,252
- Can I get an American Girl doll?
- Oh, guys.

50
00:07:00,253 --> 00:07:02,362
You wrote your letters
to Santa already.

51
00:07:02,363 --> 00:07:05,460
I don't think another letter
will make it to the North Pole.

52
00:07:07,387 --> 00:07:10,715
- We will be okay, right?
- We're always okay.

53
00:07:10,779 --> 00:07:12,762
We're always okay.

54
00:07:12,763 --> 00:07:15,926
- No matter what happens.
- No matter what happens.

55
00:07:21,971 --> 00:07:23,387
Okay.

56
00:07:23,506 --> 00:07:24,538
Alright..

57
00:07:24,539 --> 00:07:26,835
Okay.. I'll be back
in just a little while, okay?

58
00:07:26,836 --> 00:07:28,442
- Okay.
- Alrighty.

59
00:07:28,443 --> 00:07:32,090
Now, promise me one thing, that..
Listen.. listen, hey, you.. you..

60
00:07:32,091 --> 00:07:33,722
you have to brush your teeth.

61
00:07:33,723 --> 00:07:35,862
And I want you in the bed
after your treat, all right?

62
00:07:35,863 --> 00:07:37,114
- Okay
- Okay..

63
00:07:37,115 --> 00:07:38,964
I'll see you later.

64
00:07:41,595 --> 00:07:43,058
Mom!

65
00:07:44,731 --> 00:07:46,715
I made a secret drawing for you.

66
00:07:46,716 --> 00:07:50,262
- Oh, I love your drawings.
- Don't look at it yet!

67
00:07:52,187 --> 00:07:55,286
Okay. I'll save it for later.
How about that?

68
00:07:56,283 --> 00:07:57,657
Bye, sweetie.

69
00:10:36,252 --> 00:10:38,553
And you can kiss my ass, Kenneth.

70
00:10:51,484 --> 00:10:54,842
Cellular 411 connect.
How may I direct your call?

71
00:10:54,843 --> 00:10:59,066
- Spring Street Learning Academy, please.
- Hold, while I connect you.

72
00:10:59,067 --> 00:11:01,719
Thank you for using
411 Information.

73
00:11:05,532 --> 00:11:08,122
Learning Academy.
Meghan speaking.

74
00:11:08,123 --> 00:11:10,618
Hi, Meghan.
It's Della Myers.

75
00:11:10,619 --> 00:11:12,342
Hi.

76
00:11:12,443 --> 00:11:14,972
Are you calling about Spanish?
It's been cancelled.

77
00:11:14,973 --> 00:11:17,082
No, no, I knew about that.

78
00:11:17,083 --> 00:11:20,155
It's about next week's
mechanics class.

79
00:11:20,156 --> 00:11:24,602
Would you just pass on to Mike,
that I won't be there just this once?

80
00:11:24,603 --> 00:11:26,427
No problem.

81
00:11:26,428 --> 00:11:29,011
- I'll make sure he gets that.
- Great.

82
00:11:55,580 --> 00:11:57,627
- Hi. Della?
- Hey, what's up, Cassie?

83
00:11:57,628 --> 00:12:01,563
I'm over at the mall. What is
it we need to get for Pilates?

84
00:12:01,564 --> 00:12:05,453
One of those rubber bands.
I'm headed over there myself.

85
00:12:05,454 --> 00:12:09,116
Do yourself a favor. Turn around.
It's hell over here.

86
00:12:09,117 --> 00:12:11,282
Hey, my cell's dying.

87
00:12:11,283 --> 00:12:13,111
You know, I.. I.. damn!

88
00:12:13,112 --> 00:12:15,898
You know, I left.. I left
the charger in the other car.

89
00:12:15,899 --> 00:12:19,292
Silly you. Let me pick you up
something. Save you the trip.

90
00:12:19,293 --> 00:12:20,824
- No, I mean..
- Are you sure?

91
00:12:20,825 --> 00:12:23,547
No, no.. it's okay.

92
00:12:23,548 --> 00:12:27,834
Cassie.. um, can we meet..
huh, for a drink?

93
00:12:27,835 --> 00:12:30,770
You still there?
Your phone is breaking up.

94
00:12:32,155 --> 00:12:33,529
Damn.

95
00:12:42,428 --> 00:12:45,177
You're kidding me.
My God!

96
00:13:04,156 --> 00:13:05,530
Damn it!

97
00:13:05,691 --> 00:13:07,064
Damn!

98
00:13:11,451 --> 00:13:13,207
Come on.

99
00:13:13,275 --> 00:13:15,831
You think you're gonna
take my spot, think again!

100
00:13:15,832 --> 00:13:18,803
I've been waiting now
for 30 minutes! It's Christmas!

101
00:13:20,284 --> 00:13:23,134
Excuse me. You know where
I can find a parking space?

102
00:13:23,135 --> 00:13:24,668
I mean this is ridiculous!

103
00:13:24,669 --> 00:13:28,188
Ma'am, there is absolutely
nothing close by.

104
00:13:28,189 --> 00:13:30,363
Even the valet's backed up.

105
00:13:30,364 --> 00:13:33,561
- You might wanna try further up.
- Where? Over there?

106
00:13:33,562 --> 00:13:35,955
Yeah..
Now, please keep it moving.

107
00:13:37,084 --> 00:13:38,610
Let's go. Come on.

108
00:13:39,132 --> 00:13:40,440
Go!

109
00:13:51,868 --> 00:13:53,242
Come on!

110
00:13:54,684 --> 00:13:56,145
Jerk!

111
00:13:56,444 --> 00:13:57,817
What a jerk!

112
00:16:11,100 --> 00:16:12,473
Over there.

113
00:16:21,564 --> 00:16:22,872
Smile.

114
00:16:23,003 --> 00:16:24,283
Good girl.

115
00:16:24,284 --> 00:16:25,656
Merry Christmas!

116
00:16:35,547 --> 00:16:38,483
- Would you like to try Chanel No. 5?
- No, thank you.

117
00:16:38,520 --> 00:16:40,401
We're breaking up,
you hear me?

118
00:17:14,525 --> 00:17:17,108
- Can I help you with that?
- No.

119
00:17:17,852 --> 00:17:20,667
No, I was just looking.
Thank you.

120
00:17:24,955 --> 00:17:26,140
Hi.

121
00:17:26,141 --> 00:17:28,593
Could I have a hot tea, please?

122
00:17:30,781 --> 00:17:32,950
And one chocolate biscotti.

123
00:17:33,020 --> 00:17:35,410
Huh, milk with dark chocolate?

124
00:17:36,988 --> 00:17:38,268
No, just the tea.

125
00:17:38,269 --> 00:17:39,610
Name?

126
00:17:40,315 --> 00:17:41,560
Oh, Della.

127
00:17:41,561 --> 00:17:43,222
Two "L" s.

128
00:17:45,308 --> 00:17:47,478
2 dollars and 65 cents.

129
00:17:57,083 --> 00:17:58,458
Thank you.

130
00:18:11,452 --> 00:18:12,827
Della?

131
00:18:12,828 --> 00:18:14,202
Is that you?

132
00:18:14,876 --> 00:18:17,371
Lynn? Lynn Monroe.

133
00:18:17,372 --> 00:18:20,918
- Hi.
- I haven't seen you since college!

134
00:18:21,117 --> 00:18:22,459
Right.

135
00:18:22,460 --> 00:18:26,201
You married that handsome jock
and we never heard from you again.

136
00:18:28,637 --> 00:18:30,012
You live around here?

137
00:18:30,013 --> 00:18:33,116
We just bought this divine house
in Pleasant Valley. You know it?

138
00:18:33,117 --> 00:18:34,682
Yes, I do.

139
00:18:34,683 --> 00:18:37,915
It's perfect for the kids.
They practically walk to school.

140
00:18:37,916 --> 00:18:39,483
We've got a great backyard.

141
00:18:39,484 --> 00:18:41,371
We're thinking
of putting a pool in it.

142
00:18:41,916 --> 00:18:43,443
They'll love that!

143
00:18:44,540 --> 00:18:46,263
Enough about me.
How are you?

144
00:18:47,100 --> 00:18:48,315
Fine.

145
00:18:48,316 --> 00:18:52,309
I have two children,
Terry and Tammi.. Twins.

146
00:18:53,112 --> 00:18:54,363
Oh..

147
00:18:54,364 --> 00:18:57,213
We're celebrating tonight.
David just been promoted.

148
00:18:57,214 --> 00:19:00,500
So I.. had to buy this
gorgeous black teddy.

149
00:19:00,859 --> 00:19:02,427
Oh..

150
00:19:02,428 --> 00:19:04,849
It is.. It's gorgeous.

151
00:19:05,084 --> 00:19:08,283
- It's been nice talking to you.
- You too.

152
00:19:08,284 --> 00:19:10,357
I've got to get my hair done.

153
00:19:11,420 --> 00:19:12,828
- See you later.
- Yes.

154
00:19:12,957 --> 00:19:14,331
See you.

155
00:19:17,531 --> 00:19:18,611
Mitch!

156
00:19:18,843 --> 00:19:20,218
Here you are.

157
00:20:06,140 --> 00:20:08,562
- Which one would you like?
- That one.

158
00:20:08,699 --> 00:20:10,073
That one?

159
00:20:10,620 --> 00:20:11,993
That one?

160
00:20:14,141 --> 00:20:15,634
That's the one?

161
00:20:16,284 --> 00:20:18,237
- Thanks.
- Thank you. Merry Christmas.

162
00:20:18,238 --> 00:20:19,763
Merry Christmas!

163
00:20:24,412 --> 00:20:25,720
- Hello.
- Hi.

164
00:20:33,404 --> 00:20:35,739
21 dollars and 69 cents, please.

165
00:20:37,052 --> 00:20:38,742
- There you go.
- Thank you.

166
00:20:43,549 --> 00:20:45,851
I'm sorry.
It's been declined.

167
00:20:45,852 --> 00:20:47,673
Do you have another card?

168
00:20:47,958 --> 00:20:50,556
Um.. no, I don't.

169
00:20:51,072 --> 00:20:52,827
Um..

170
00:20:55,100 --> 00:20:56,376
Seventeen..

171
00:20:57,436 --> 00:20:59,094
twenty-one..

172
00:21:01,820 --> 00:21:02,931
There.

173
00:21:03,196 --> 00:21:06,589
It's Christmas Eve,
and the mall is now closed.

174
00:21:06,590 --> 00:21:08,443
Happy Holidays, everyone.

175
00:22:10,908 --> 00:22:12,119
Shit.

176
00:22:13,405 --> 00:22:16,023
Shit. What now?

177
00:22:19,453 --> 00:22:20,825
Excuse me?

178
00:22:20,828 --> 00:22:22,835
I've got a gun.

179
00:22:23,197 --> 00:22:26,165
How's about I aim it
at your pussy first?

180
00:22:31,740 --> 00:22:33,979
Why don't you just back the car up?

181
00:22:33,980 --> 00:22:35,834
That's not gonna happen.

182
00:22:36,381 --> 00:22:39,869
What.. what're you doing? Or what're
you playing? I don't know this game.

183
00:22:39,870 --> 00:22:41,275
Trust me, it's not a game.

184
00:22:41,276 --> 00:22:44,764
That's right, how about you
shut up, bitch, and eat a dick?

185
00:22:44,765 --> 00:22:47,901
When you're done eating, we're gonna
staple your bitch-mouth and close it.

186
00:22:47,902 --> 00:22:49,274
Right, Chuckie?

187
00:22:52,604 --> 00:22:55,259
"Chuckie"? Is that for real?

188
00:22:55,260 --> 00:22:58,204
At least your parents
had a sense of humor.

189
00:22:58,205 --> 00:22:59,868
I don't have any parents.

190
00:22:59,869 --> 00:23:01,460
I named myself.

191
00:23:01,980 --> 00:23:03,739
Do you have a beef
with us, honey?

192
00:23:03,740 --> 00:23:06,727
Because slipping an insinuating
note underneath my wipers..

193
00:23:06,728 --> 00:23:10,075
is an invitation to war
to me and my soldiers.

194
00:23:10,076 --> 00:23:12,189
You had to park
like an asshole now..

195
00:23:12,190 --> 00:23:14,043
we wouldn't be
standing here, would we?

196
00:23:14,044 --> 00:23:16,251
- Watch your mouth, lady!
- Get away from me!

197
00:23:16,252 --> 00:23:18,837
You know you're just digging
your grave deeper.

198
00:23:19,676 --> 00:23:22,202
- Where you going?
- Fuck you!

199
00:23:22,203 --> 00:23:24,988
- No way, fuck you.
- No, fuck you!

200
00:23:24,989 --> 00:23:26,747
Is any trouble here, ma'am?

201
00:23:26,748 --> 00:23:28,861
Not if these punks
move their car!

202
00:23:28,862 --> 00:23:30,715
Yeah, what's the deal, Barney Fife?

203
00:23:30,716 --> 00:23:34,742
Okay, boys. Let's back the car up
and let the lady out.

204
00:23:34,941 --> 00:23:36,598
How's that sound, squirt?

205
00:23:37,213 --> 00:23:40,375
No, we're not in the mood.
In fact..

206
00:23:40,957 --> 00:23:42,780
why don't you mind
your own fucking business..

207
00:23:42,781 --> 00:23:44,603
go direct some traffic
or something, huh?

208
00:23:44,604 --> 00:23:47,289
I'm gonna make this my business.

209
00:23:47,452 --> 00:23:51,165
Now, why don't you kids, pile back
into your piece-of-shit car..

210
00:23:51,166 --> 00:23:53,339
and get the fuck out of my mall?

211
00:23:53,340 --> 00:23:56,056
Didn't you hear what I said?
You hear me?

212
00:23:57,213 --> 00:23:59,084
You hear me, daddy?
Get the fuck outta here!

213
00:23:59,085 --> 00:24:00,891
- Put the gun down.
- Get the fuck outta here!

214
00:24:00,892 --> 00:24:03,448
- Put the gun down!
- I said, get the fuck outta here, bitch!

215
00:24:03,449 --> 00:24:06,420
- Put the gun down!
- You want me to put it down?

216
00:24:06,685 --> 00:24:08,658
Make me, bitch!
I said make me..

217
00:24:16,092 --> 00:24:18,132
- Shit.
- Motherfucker.

218
00:24:28,668 --> 00:24:30,095
She's getting away!

219
00:24:33,245 --> 00:24:36,923
Fuck, man! She's probably on the
phone with 5-0 already, Chuckie!

220
00:24:36,924 --> 00:24:40,224
What the fuck!
We're going down for this shit!

221
00:24:40,225 --> 00:24:41,662
Don't worry about it, nigga.
Don't worry! Don't worry!

222
00:24:41,663 --> 00:24:44,371
Don't fucking call me "nigga", Chuckie.
You ain't earned the right.

223
00:24:44,372 --> 00:24:45,906
That goes for all you niggers.

224
00:24:46,205 --> 00:24:48,091
Huey, Huey, take it easy, we got it.

225
00:24:48,092 --> 00:24:49,691
Take it easy.
We got it!

226
00:24:49,692 --> 00:24:51,870
The world's like this.
Come on, get in, get in the car!

227
00:24:51,871 --> 00:24:52,828
Now.

228
00:24:52,829 --> 00:24:55,131
Move over, Thomas!
Come on! Move!

229
00:24:55,132 --> 00:24:58,394
- The cop went down hard.
- Goddamn it, Thomas. Shut up!

230
00:25:42,941 --> 00:25:44,314
My God.

231
00:26:12,829 --> 00:26:14,201
Oh, God.

232
00:28:09,565 --> 00:28:11,058
Hell, no.

233
00:28:13,500 --> 00:28:14,620
- Damn!
- What?

234
00:28:14,621 --> 00:28:17,108
My new Keds they're all dirty, man!

235
00:28:26,301 --> 00:28:27,708
Get up there and push!

236
00:28:27,709 --> 00:28:30,773
The car's got nowhere else to go, Chuckie,
the road ends just up ahead.

237
00:28:31,164 --> 00:28:33,816
There's nothing but half build
houses and trees.

238
00:28:34,749 --> 00:28:36,348
Ditch the car, man.

239
00:28:36,349 --> 00:28:38,169
We've got a witness on the loose.

240
00:28:39,485 --> 00:28:40,635
Trust me.

241
00:28:40,636 --> 00:28:43,262
You won't hear a peep outta little
miss red, after tonight, alright?

242
00:28:43,263 --> 00:28:45,468
She's going down, execution style.

243
00:28:45,469 --> 00:28:47,226
What if she's gone already?

244
00:28:50,909 --> 00:28:52,664
She's not gone.

245
00:28:52,989 --> 00:28:54,842
There's no place for her to go.

246
00:28:58,429 --> 00:29:00,818
Damn. She took out that barrier.

247
00:29:05,117 --> 00:29:08,945
It's gonna be impossible to
find her, even if she is around.

248
00:29:09,597 --> 00:29:11,571
Come on, guys. Let's go!

249
00:29:13,309 --> 00:29:15,162
Maybe we can hotwire the truck.

250
00:29:43,805 --> 00:29:46,139
I think, I'll find you now, cunt!

251
00:29:52,445 --> 00:29:55,674
Well, we're caught up with them things,
like.. like in a soap opera.

252
00:29:55,964 --> 00:29:59,761
We need to just bail, blast out of town,
act like we was never even around.

253
00:29:59,869 --> 00:30:02,493
- Vingh, get over here.
- How much "Kill" you been smoking?

254
00:30:02,494 --> 00:30:05,231
Come on, man. No one cares
about a fucking Rent-A-Pig!

255
00:30:05,232 --> 00:30:07,933
He's just collateral damage
to the people at the mall.

256
00:30:07,934 --> 00:30:09,532
She's the only witness!

257
00:30:09,533 --> 00:30:11,579
Come on, if we hit the road
right now..

258
00:30:11,580 --> 00:30:14,011
we could be miles away
before they find you.

259
00:30:14,012 --> 00:30:16,157
We could be sipping cold beer
swimming in the Atlantic,

260
00:30:16,158 --> 00:30:17,788
just like you promised me.

261
00:30:19,453 --> 00:30:20,860
Huey, come here.

262
00:30:22,013 --> 00:30:23,387
Come here.

263
00:30:26,269 --> 00:30:28,925
Remember how I broke
you outta that home?

264
00:30:28,926 --> 00:30:31,380
I was there for you, right?

265
00:30:31,381 --> 00:30:32,347
I was there.

266
00:30:32,348 --> 00:30:34,256
You trust me, right?

267
00:30:34,620 --> 00:30:36,082
- Right?
- Yeah.

268
00:30:38,142 --> 00:30:41,820
All we gotta do is find that bitch..
& we're outta here, I promise. okay?

269
00:30:41,821 --> 00:30:43,100
That's it.

270
00:30:43,101 --> 00:30:44,627
There's her Ford!

271
00:30:56,221 --> 00:30:59,837
Somebody tell her mom
to lay out her black dress.

272
00:30:59,838 --> 00:31:01,308
Why? Is she dead?

273
00:31:01,309 --> 00:31:03,005
No, I looked,
she went through her trunk.

274
00:31:03,006 --> 00:31:04,183
She's here.

275
00:31:04,477 --> 00:31:06,167
She's hiding somewhere.

276
00:31:07,069 --> 00:31:10,515
She left her purse.
It's all in here. Her cell phone.

277
00:31:10,813 --> 00:31:12,284
Where are you?

278
00:31:12,285 --> 00:31:15,483
- Her battery gave out.
- We're gonna find you!

279
00:31:16,861 --> 00:31:18,005
There she is!

280
00:31:18,076 --> 00:31:19,984
Go that way! That way!

281
00:31:41,405 --> 00:31:42,746
Fuck!

282
00:31:44,317 --> 00:31:46,619
- You see her down there?
- No, man.

283
00:31:47,517 --> 00:31:50,485
- She must have gone around.
- She vanished.

284
00:31:50,972 --> 00:31:52,947
Check each one of these houses.

285
00:31:54,238 --> 00:31:56,692
Split up. Spread out!

286
00:32:12,925 --> 00:32:14,353
Della!

287
00:32:21,244 --> 00:32:23,066
We know who you are!

288
00:32:23,805 --> 00:32:25,777
You can't hide, honey.

289
00:32:44,798 --> 00:32:48,026
We're not gonna give up,
till we find you, Della.

290
00:32:51,964 --> 00:32:53,820
You're trapped, Della.

291
00:32:57,085 --> 00:32:59,027
We're gonna find you!

292
00:33:12,124 --> 00:33:14,513
Where's Little Red Riding Hood?

293
00:33:25,598 --> 00:33:28,379
You all hear that?
Check the house next door!

294
00:33:28,413 --> 00:33:29,788
Move it!

295
00:34:02,098 --> 00:34:03,250
Yeah.

296
00:34:24,542 --> 00:34:26,076
Ah'ha..

297
00:34:26,077 --> 00:34:28,380
There she is!

298
00:34:28,541 --> 00:34:31,441
- Hey there, we got you.
- You're dead, bitch.

299
00:34:31,774 --> 00:34:33,081
Time to say goodbye.

300
00:34:33,117 --> 00:34:34,520
Hey!

301
00:34:34,521 --> 00:34:35,863
Shh..

302
00:34:36,318 --> 00:34:38,870
You hear that?
Did you hear that?

303
00:34:39,198 --> 00:34:42,611
The sound of the waves!
You hear that? Sounds good, right?

304
00:34:43,038 --> 00:34:45,720
Yeah, Chuckie.
Sounds great.

305
00:34:49,918 --> 00:34:51,707
Don't worry.

306
00:34:51,773 --> 00:34:55,449
When we're done with you, we'll send
you back in your junk to mister..

307
00:34:57,213 --> 00:34:59,741
You won't play nice?
You fucking wanna play?

308
00:34:59,742 --> 00:35:01,946
Back off! Hold back!

309
00:35:02,205 --> 00:35:03,732
Give me a minute.

310
00:35:07,965 --> 00:35:09,817
I'm sorry about that.

311
00:35:10,972 --> 00:35:12,533
I'm sorry.

312
00:35:13,662 --> 00:35:15,089
You okay?

313
00:35:16,605 --> 00:35:17,884
Chill out.

314
00:35:17,885 --> 00:35:19,508
You all right?

315
00:35:19,838 --> 00:35:21,333
Hey, settle down!

316
00:35:21,533 --> 00:35:22,996
Settle down!

317
00:35:23,805 --> 00:35:26,195
You all right? Settle down.

318
00:35:26,654 --> 00:35:29,140
You must be wondering,
how we know your name?

319
00:35:29,693 --> 00:35:34,034
You messed up, Della.
You left your purse in the car.

320
00:35:34,621 --> 00:35:35,799
Give me that.

321
00:35:41,918 --> 00:35:44,340
Your photo
doesn't do you justice.

322
00:35:44,669 --> 00:35:45,853
He's right.

323
00:35:45,854 --> 00:35:49,300
- You look better in person, ma'am.
- You do.

324
00:35:50,013 --> 00:35:51,440
You look good.

325
00:35:54,494 --> 00:35:56,501
You're not gonna
need it anymore.

326
00:36:09,246 --> 00:36:11,318
What's in that box, Della?

327
00:36:11,931 --> 00:36:13,620
Huh?

328
00:36:14,142 --> 00:36:15,963
What's in the box?

329
00:36:16,541 --> 00:36:18,875
What's in the box, girl?

330
00:36:18,942 --> 00:36:22,235
What's up, Chuckie? Do you think
she's carrying some cash?

331
00:36:22,461 --> 00:36:24,369
Some jewelry or something?

332
00:36:24,765 --> 00:36:26,172
I don't know.

333
00:36:26,685 --> 00:36:28,147
I don't know.

334
00:36:29,277 --> 00:36:31,796
Maybe she cashed in
her life savings.

335
00:36:32,509 --> 00:36:34,199
Planning on running away?

336
00:36:34,749 --> 00:36:36,348
Start a new life?

337
00:36:36,349 --> 00:36:37,789
Is that it, Della?

338
00:36:37,790 --> 00:36:40,441
You got enough scratch
in there to buy us off?

339
00:36:40,513 --> 00:36:42,645
Huh?
Open the box.

340
00:36:42,717 --> 00:36:44,276
Open the box!

341
00:36:44,829 --> 00:36:46,460
- Open it!
- Okay.

342
00:36:46,461 --> 00:36:48,955
Why don't you take the box.
We're gonna kill her anyway.

343
00:36:48,989 --> 00:36:49,914
No..

344
00:36:50,909 --> 00:36:52,534
I'll open up the box.

345
00:36:59,389 --> 00:37:01,079
You know what, man?

346
00:37:01,725 --> 00:37:03,579
She ain't got nothing.

347
00:37:04,157 --> 00:37:06,392
Just take that pussy
and get going.

348
00:37:09,757 --> 00:37:11,130
I got her!

349
00:37:11,805 --> 00:37:13,213
Go down here!

350
00:37:13,214 --> 00:37:14,586
Look out, Huey!

351
00:37:19,357 --> 00:37:21,294
- What the fuck, nigga?
- Shut up!

352
00:37:21,295 --> 00:37:23,644
- What, Chuckie?
- I said, shut up!

353
00:37:24,285 --> 00:37:26,074
Don't use the N-word.

354
00:37:26,398 --> 00:37:28,404
You ain't representing.

355
00:37:31,583 --> 00:37:33,244
Huey?

356
00:37:33,501 --> 00:37:34,875
Huey?

357
00:37:35,933 --> 00:37:38,267
- What did you do?
- What'd you mean?

358
00:37:38,431 --> 00:37:40,119
- Yeah..
- Stop fucking around.

359
00:37:42,814 --> 00:37:44,979
- Hey!
- Huey?

360
00:37:47,229 --> 00:37:49,661
- What's up, brother?
- Please, forgive me.

361
00:37:49,662 --> 00:37:51,515
Quit goofing around.

362
00:37:51,933 --> 00:37:53,307
Huey?

363
00:37:53,405 --> 00:37:54,448
Hey!

364
00:37:57,496 --> 00:37:59,062
Huey?

365
00:37:59,415 --> 00:38:00,980
Hey, Huey.

366
00:38:04,029 --> 00:38:05,457
What's up?

367
00:38:08,381 --> 00:38:09,755
Oh, Fuck!

368
00:38:10,845 --> 00:38:12,090
Fuck! Fuck!

369
00:38:14,393 --> 00:38:15,638
Huey?

370
00:38:16,575 --> 00:38:19,228
- Huey?
- Hey, man, come on!

371
00:38:19,229 --> 00:38:21,566
What the fuck did you do?
Get off him!

372
00:38:21,567 --> 00:38:23,196
- I didn't kill him.
- Huey?

373
00:38:23,197 --> 00:38:25,149
You fucking
stepped on his face, fucker!

374
00:38:25,183 --> 00:38:26,492
Don't fucking touch me!

375
00:38:26,493 --> 00:38:29,875
- I didn't kill him! She did.
- Get the fuck off!

376
00:38:31,549 --> 00:38:34,200
God! Huey!

377
00:38:34,941 --> 00:38:38,104
- I'm gonna get that bitch!
- You let her go!

378
00:38:38,398 --> 00:38:40,634
You piece of shit!
Where the fuck is she?

379
00:38:55,133 --> 00:38:56,506
Fuck.

380
00:39:50,525 --> 00:39:52,379
Della!

381
00:39:54,813 --> 00:39:56,787
Della!

382
00:40:06,878 --> 00:40:08,569
Della!

383
00:40:44,476 --> 00:40:46,071
Della!

384
00:41:16,895 --> 00:41:18,682
Della!

385
00:42:04,852 --> 00:42:06,543
Della!

386
00:43:48,892 --> 00:43:50,420
What're you doing?

387
00:43:54,814 --> 00:43:56,186
It's a tribute.

388
00:43:56,382 --> 00:43:57,875
A gesture.

389
00:44:02,462 --> 00:44:05,592
That'll wake up every
fucking cop in Podunk county!

390
00:44:08,222 --> 00:44:09,596
It's for him.

391
00:44:10,621 --> 00:44:12,311
It's for Huey.

392
00:44:39,773 --> 00:44:41,747
I didn't mean it, Huey.

393
00:44:43,869 --> 00:44:45,332
What did you say?

394
00:44:47,230 --> 00:44:48,603
What?

395
00:44:48,830 --> 00:44:51,033
I.. I didn't mean to kill him.

396
00:44:51,230 --> 00:44:54,460
Get away from him.
Get the fuck away from him!

397
00:44:56,349 --> 00:44:58,258
Get the fuck away from him!

398
00:44:59,070 --> 00:45:02,998
Get away from him!
Get away! Get the fuck away!

399
00:45:03,358 --> 00:45:05,692
- Get away!
- Fuck you!

400
00:45:09,118 --> 00:45:10,971
I'm gonna fucking get you!

401
00:45:11,357 --> 00:45:12,983
Do you hear me, Della?

402
00:45:13,246 --> 00:45:14,870
I'm gonna fucking get you!

403
00:45:45,886 --> 00:45:47,193
God.

404
00:46:02,526 --> 00:46:05,176
MOMMY WE LOVE YOU

405
00:46:18,058 --> 00:46:19,912
God.

406
00:46:30,814 --> 00:46:32,189
Della!

407
00:46:46,462 --> 00:46:48,152
Della?

408
00:47:07,838 --> 00:47:10,075
- Wait.
- Huh?

409
00:47:10,111 --> 00:47:11,483
It's "Bulgari".

410
00:47:11,709 --> 00:47:14,749
- What's "Bulgari"?
- It's a perfume, dude.

411
00:47:14,750 --> 00:47:17,651
It smells more
like that No. 5 shit.

412
00:47:17,822 --> 00:47:20,956
- "Chanel".
- No, it's "Bulgari".

413
00:47:20,957 --> 00:47:22,964
My dad's bitch bathes in it.

414
00:47:43,133 --> 00:47:44,507
It's blood.

415
00:47:49,119 --> 00:47:50,612
It's her blood.

416
00:47:50,655 --> 00:47:52,246
Maybe she's hurt.

417
00:48:02,079 --> 00:48:03,506
This way.

418
00:48:08,414 --> 00:48:10,650
Go, go, go!

419
00:48:17,693 --> 00:48:19,286
Watch out, dude.

420
00:48:21,471 --> 00:48:22,844
Look out!

421
00:48:26,303 --> 00:48:27,675
Damn!

422
00:48:41,023 --> 00:48:43,001
- Oh, shit!
- What happened?

423
00:48:43,002 --> 00:48:45,395
I stepped in the fucking stream!

424
00:48:51,231 --> 00:48:54,333
- That is raw.
- Who cares, man?

425
00:48:54,334 --> 00:48:57,082
Nobody gives a shit
if we live or die.

426
00:48:57,406 --> 00:49:00,799
Quit playing with the fucking flashlight.
We are stumbling around in the dark!

427
00:49:00,800 --> 00:49:03,416
He who finds it, shines it, Chuckie.

428
00:49:03,711 --> 00:49:06,230
- Let's go!
- Where the hell is she?

429
00:49:06,239 --> 00:49:08,693
- Shut up and move it!
- Damn!

430
00:49:09,663 --> 00:49:11,387
Let's go, guys. Come on!

431
00:49:11,678 --> 00:49:13,141
There she is!

432
00:49:25,565 --> 00:49:27,288
Don't let her get away!

433
00:49:31,455 --> 00:49:32,763
Huey?

434
00:49:33,055 --> 00:49:34,549
Is that you, Huey?

435
00:49:34,783 --> 00:49:36,954
I saw him
in those trees, right there!

436
00:49:44,799 --> 00:49:46,173
Sure you did.

437
00:49:56,638 --> 00:49:58,678
Son of a bitch!

438
00:50:00,767 --> 00:50:03,806
I'm gonna get you now, bitch!

439
00:50:17,598 --> 00:50:19,158
You're ain't putting
anything over on me!

440
00:50:19,903 --> 00:50:21,627
You fucking bitch!

441
00:50:24,638 --> 00:50:26,393
Son of a bitch!

442
00:50:52,670 --> 00:50:54,643
Time's up, you bitch.

443
00:51:07,647 --> 00:51:09,020
God!

444
00:51:48,735 --> 00:51:50,108
Thomas!

445
00:51:50,109 --> 00:51:51,454
Shhh..

446
00:51:51,455 --> 00:51:52,827
Quiet.

447
00:51:58,879 --> 00:52:00,951
Thomas is gone, Chuckie.

448
00:52:01,342 --> 00:52:02,804
I can feel it.

449
00:52:04,734 --> 00:52:07,385
He's probably got her strung up
on her Penada by now.

450
00:52:08,799 --> 00:52:10,708
There's something over here.

451
00:52:11,646 --> 00:52:13,107
Looks like a body.

452
00:52:20,222 --> 00:52:23,036
It's a tree stump,
you dumb Wonton.

453
00:52:50,591 --> 00:52:51,963
Thomas..

454
00:53:03,838 --> 00:53:06,042
She's gonna kill both of us too.

455
00:53:06,110 --> 00:53:08,018
We're all gonna die here.

456
00:53:11,358 --> 00:53:14,205
No gun can kill
a spirit gone bad.

457
00:53:14,398 --> 00:53:17,333
She's gone bad.
She's gone bad.

458
00:53:18,719 --> 00:53:20,092
Gone bad.

459
00:53:22,816 --> 00:53:24,821
I'm gonna get this girl.

460
00:53:33,438 --> 00:53:35,127
I'm sorry.

461
00:53:36,703 --> 00:53:38,938
I'm so sorry.

462
00:53:49,600 --> 00:53:53,941
Where are you, God?
Where are you? Where are you?

463
00:53:54,686 --> 00:53:58,166
All I wanna do
is see my babies!

464
00:54:00,990 --> 00:54:04,951
Where are you?
Where are you?

465
00:54:15,135 --> 00:54:16,542
Fuck.

466
00:54:17,023 --> 00:54:19,446
Fuck, fuck!

467
00:54:59,262 --> 00:55:00,691
Over there, Chuckie!

468
00:55:09,246 --> 00:55:10,708
You're going down!

469
00:55:17,790 --> 00:55:19,252
Slow down, bitch.

470
00:55:21,119 --> 00:55:24,118
- Fuck off.
- Get outta my way, Vingh!

471
00:55:25,630 --> 00:55:27,125
Get up!

472
00:55:28,831 --> 00:55:31,350
- Get up Vingh!
- She's going now!

473
00:55:59,390 --> 00:56:02,937
I don't feel so good, Chuckie.
I think I got hypothermia.

474
00:56:03,006 --> 00:56:04,566
You'll be fine.

475
00:56:04,959 --> 00:56:07,545
It ain't natural how she
keeps getting away every time.

476
00:56:08,927 --> 00:56:10,650
Let's get this over with.

477
00:56:10,879 --> 00:56:13,627
You go up there.
I'll cover you.

478
00:56:13,790 --> 00:56:16,823
I ain't going up there without
a gun. End of story.

479
00:56:17,759 --> 00:56:19,930
I never pegged you for a pussy.

480
00:56:20,639 --> 00:56:24,160
Even when Thomas called it,
I always defended your ass.

481
00:56:24,161 --> 00:56:26,045
No one ever called me a pussy.

482
00:56:26,046 --> 00:56:27,810
She couldn't weight more than 120.

483
00:56:27,811 --> 00:56:30,508
If you'd fuck her,
you could break her in half.

484
00:56:30,542 --> 00:56:33,205
- Thomas never called me a pussy!
- Okay, fine.

485
00:56:33,566 --> 00:56:35,390
You don't wanna go? Don't go.

486
00:56:35,391 --> 00:56:39,416
I'm going up, I'm gonna go get
this bitch for Thomas, for Huey.

487
00:56:55,806 --> 00:56:57,181
If you go up.

488
00:56:57,504 --> 00:56:59,260
I'm gonna go around the side.

489
00:57:07,134 --> 00:57:08,987
On power..
No limits.

490
00:57:16,352 --> 00:57:18,236
On power..
No limits.

491
00:58:31,327 --> 00:58:32,854
Vingh?

492
00:58:34,876 --> 00:58:36,403
Vingh?

493
00:59:01,732 --> 00:59:03,259
Vingh?

494
00:59:03,650 --> 00:59:05,177
Vingh?

495
01:00:06,911 --> 01:00:08,372
Della?

496
01:00:19,006 --> 01:00:20,860
Della?

497
01:00:40,671 --> 01:00:43,006
It's just you and me left now.

498
01:00:46,431 --> 01:00:49,245
I saw a little trail
of your blood back there.

499
01:00:51,519 --> 01:00:53,406
You must be hurt.

500
01:01:01,312 --> 01:01:03,000
I'm hurting, too.

501
01:01:04,863 --> 01:01:06,870
You busted my collarbone.

502
01:01:09,823 --> 01:01:11,797
I know you can hear me.

503
01:01:13,472 --> 01:01:15,512
I can smell your sweat.

504
01:01:22,399 --> 01:01:24,057
I can smell your blood.

505
01:01:28,895 --> 01:01:30,749
I know you're close.

506
01:01:41,951 --> 01:01:45,626
After what went down tonight,
you and me got a special bond, Della.

507
01:01:47,968 --> 01:01:50,302
You might as well come out now.

508
01:01:55,327 --> 01:01:56,952
You want a cigarette?

509
01:01:59,039 --> 01:02:02,137
I'll save one for you,
for when you decide to come out.

510
01:02:06,526 --> 01:02:08,861
You are one tough bitch!

511
01:02:13,983 --> 01:02:15,707
And that is hot.

512
01:02:17,340 --> 01:02:18,872
Well, well.

513
01:02:25,055 --> 01:02:29,464
And they're really cute.
Twins, huh?

514
01:02:30,687 --> 01:02:33,816
A woman's purse can
tell her whole life story.

515
01:02:37,663 --> 01:02:41,240
You sure you don't want
a cigarette now, huh? Della?

516
01:02:42,591 --> 01:02:43,965
Honey?

517
01:02:45,182 --> 01:02:49,558
Now I know where they live.
Maybe I'll stop by and say hi.

518
01:02:49,591 --> 01:02:51,476
No.. No..

519
01:02:55,263 --> 01:02:57,750
I figured that'd help
to find your voice.

520
01:02:59,616 --> 01:03:01,022
Don't worry.

521
01:03:01,023 --> 01:03:03,258
I wouldn't hurt them.

522
01:03:04,672 --> 01:03:07,126
It's wrong to hurt kids, right?

523
01:03:08,543 --> 01:03:10,397
The game's over, Della.

524
01:03:12,415 --> 01:03:13,788
You won.

525
01:03:37,312 --> 01:03:39,166
You took him from me, Della.

526
01:03:41,152 --> 01:03:43,158
He was like my little brother.

527
01:03:50,559 --> 01:03:53,244
All he ever wanted to do
was see the ocean.

528
01:03:55,199 --> 01:03:57,239
But he never got to see it.

529
01:04:00,959 --> 01:04:02,869
No, he never got to go.

530
01:04:07,168 --> 01:04:09,075
They're all gone now.

531
01:04:23,006 --> 01:04:24,698
Let's see here.

532
01:04:25,952 --> 01:04:27,860
Monday, Pilates.

533
01:04:28,959 --> 01:04:31,096
Tuesday, Spanish.

534
01:04:32,192 --> 01:04:34,973
Wednesday, Mechanics?

535
01:04:39,136 --> 01:04:42,134
Thursday, Friday..

536
01:04:44,704 --> 01:04:46,459
Face it, Della.

537
01:04:47,648 --> 01:04:50,266
You're no suburban housewife.

538
01:04:54,656 --> 01:04:56,923
You bought into the big lie.

539
01:04:58,848 --> 01:05:00,505
You even tried to live it.

540
01:05:03,071 --> 01:05:04,445
Husband..

541
01:05:04,991 --> 01:05:06,365
house..

542
01:05:06,784 --> 01:05:08,157
security..

543
01:05:09,119 --> 01:05:10,941
every woman's dream.

544
01:05:14,494 --> 01:05:16,186
But not yours, Della.

545
01:05:18,175 --> 01:05:20,759
You know there's no
such thing as security.

546
01:05:30,880 --> 01:05:33,880
I reckon I know what you've been
dreaming about, Della.

547
01:05:34,944 --> 01:05:37,113
What you've been aching for..

548
01:05:37,312 --> 01:05:39,481
all these lonely years.

549
01:05:41,408 --> 01:05:43,196
The wind in your hair.

550
01:05:43,903 --> 01:05:46,237
The dirt of life
between your fingers.

551
01:05:47,680 --> 01:05:50,015
Taking what you want from life.

552
01:05:51,839 --> 01:05:53,268
Right, Della?

553
01:05:55,262 --> 01:05:57,717
Don't tell me you
haven't thought about it.

554
01:05:59,552 --> 01:06:01,241
It's in your blood.

555
01:06:03,232 --> 01:06:06,014
I bet your husband
doesn't treat you right, does he?

556
01:06:07,807 --> 01:06:09,596
He takes you for granted.

557
01:06:13,472 --> 01:06:15,609
You can't go back to that, Della.

558
01:06:17,663 --> 01:06:19,638
You don't wanna go back to that.

559
01:06:23,103 --> 01:06:25,273
Let's go pick up your kids.

560
01:06:26,368 --> 01:06:27,741
Right now.

561
01:06:29,823 --> 01:06:31,797
And we'll get rid of him.

562
01:06:32,320 --> 01:06:34,206
You can do it.

563
01:06:35,360 --> 01:06:37,116
I can do it.

564
01:06:38,525 --> 01:06:39,802
Della!

565
01:06:48,160 --> 01:06:49,534
It's okay.

566
01:06:57,559 --> 01:06:59,125
Della!

567
01:07:04,273 --> 01:07:05,839
Della!

568
01:07:10,495 --> 01:07:11,869
Come here.

569
01:07:28,031 --> 01:07:29,656
Hell of a night, huh?

570
01:07:40,212 --> 01:07:42,125
Hey! Hey!

571
01:07:49,407 --> 01:07:50,903
Close your eyes.

572
01:07:52,864 --> 01:07:54,391
Close your eyes.

573
01:08:00,448 --> 01:08:02,237
You're amazing, Della.

574
01:08:06,239 --> 01:08:07,700
So beautiful.

575
01:09:02,496 --> 01:09:04,600
You've been a bad girl.

576
01:09:05,736 --> 01:09:07,648
I don't care.

577
01:09:15,903 --> 01:09:17,528
You're a bad girl.

578
01:09:21,600 --> 01:09:22,974
Fuck me.

579
01:09:24,799 --> 01:09:26,172
Fuck me.

580
01:15:50,016 --> 01:15:53,179
You wanna tell me where in the
hell you've been all night, honey?

581
01:15:53,664 --> 01:15:55,703
Don't ever call me that again.

582
01:15:55,808 --> 01:15:57,150
What?

583
01:15:59,296 --> 01:16:02,711
You can't even take
your shoes off at the door?

584
01:16:02,977 --> 01:16:04,764
The Japanese do it!

585
01:16:05,345 --> 01:16:06,717
I do it.

586
01:16:44,096 --> 01:16:46,647
I see you got wrapping paper.
What'd you get me?

587
01:16:54,496 --> 01:16:56,666
What did you get me at the mall?

588
01:16:59,872 --> 01:17:01,399
Nothing.

589
01:17:01,404 --> 01:17:05,000
<font face="Century Gothic">
<b>SubText:
NoRMITA.326</b></font>

