1
00:02:43,430 --> 00:02:45,398
The inevitable always happens

2
00:02:48,168 --> 00:02:50,068
Sorry sir, you were saying something

3
00:02:51,038 --> 00:02:54,838
No doubts about your alacrity,
But the inevitable always happens

4
00:03:05,686 --> 00:03:07,654
You were saying something
about my alacrity

5
00:03:10,557 --> 00:03:17,690
All these security officers l�ve seen,
make me wonder,..

6
00:03:17,831 --> 00:03:20,265
whether you are men or machines

7
00:03:35,348 --> 00:03:37,043
He doesn�t understand Hindi, does he?

8
00:03:38,819 --> 00:03:40,218
No sir

9
00:03:45,092 --> 00:03:51,895
Ever wondered? ln the lndian
subcontinent, our part of Asia,..

10
00:03:52,099 --> 00:03:55,466
how many presidents,
how many prime ministers,..

11
00:03:55,602 --> 00:03:57,570
have been killed
in the last 50 years?

12
00:03:58,972 --> 00:04:01,805
ln our own country,
lndira Gandhi, Rajiv Gandhi

13
00:04:04,611 --> 00:04:08,775
ln Bangladesh, Mujibur Rahman,
ln Sri Lanka, Premdasa

14
00:04:09,683 --> 00:04:11,514
ln Pakistan, Zia-ur-Rehman

15
00:04:22,829 --> 00:04:26,265
The next name on this hit list
is our Prime Minister

16
00:04:28,034 --> 00:04:32,994
l am here gathering evidence, names,
of those behind the entire conspiracy

17
00:04:33,173 --> 00:04:39,578
Diplomacy was just a ruse,
The evidence is in a little floppy

18
00:04:40,213 --> 00:04:42,181
l�ll hand it over to you
when we reach the hotel

19
00:04:42,916 --> 00:04:47,717
lf something happens to me, you will
hand it over to the Prime Minister

20
00:07:53,006 --> 00:07:58,205
This 2-inch y 2-inch floppy is more
precious than your life or mine

21
00:11:14,807 --> 00:11:16,741
No one dies

22
00:11:19,479 --> 00:11:21,447
No one kills

23
00:11:24,217 --> 00:11:26,048
Not l,..

24
00:11:29,088 --> 00:11:31,056
l�m not the one who�s saying that

25
00:11:33,092 --> 00:11:35,060
lt�s there, written in the Gita

26
00:11:40,099 --> 00:11:44,729
l am merely a tool

27
00:11:47,640 --> 00:11:49,073
�Tool, merely

28
00:11:54,147 --> 00:11:56,115
ln this game of puppetry,..

29
00:11:56,249 --> 00:11:58,774
l am the hunter,..

30
00:12:01,287 --> 00:12:02,982
you are my prey

31
00:12:07,827 --> 00:12:09,658
what�s in a face?

32
00:12:12,365 --> 00:12:20,795
�What is in a face? Mere happenstance,
that you are a Manu verma,..

33
00:12:22,508 --> 00:12:25,375
and l, a Raghavan

34
00:12:31,351 --> 00:12:33,046
Therefore,..

35
00:12:47,967 --> 00:12:49,935
please, not to mind

36
00:13:15,028 --> 00:13:17,394
Good is bad
bad is good,..

37
00:13:17,697 --> 00:13:19,665
in the false light,
it�s all true

38
00:13:30,877 --> 00:13:35,712
�True-False, who knows what?
One�s a lesser evil than the other

39
00:13:36,082 --> 00:13:41,042
But they all look-walk the same,
it all seems to measure the same

40
00:13:41,587 --> 00:13:46,217
Set fire to Truth,
find a Lie burnt out

41
00:13:46,426 --> 00:13:49,054
Good is bad
bad is good,..

42
00:13:49,429 --> 00:13:52,057
in the false light,
it�s all true

43
00:13:57,870 --> 00:14:02,330
Black is black,
black as Death

44
00:14:03,009 --> 00:14:07,946
But life�s a kaleidoscope of colours,
to be diced, piece by piece

45
00:14:08,548 --> 00:14:12,985
�What�s unborn of sin,
in sin is bred

46
00:14:14,020 --> 00:14:18,389
Dregs spat out,
breed hunger

47
00:14:30,036 --> 00:14:34,666
�What was, is no more believed,
what the eye beholds, is unbelievable

48
00:14:35,208 --> 00:14:40,043
what was all pervading,..
no longer dwells on this earth

49
00:14:40,513 --> 00:14:45,212
�Trust your instincts,
it�s ugly

50
00:14:45,885 --> 00:14:48,046
Once what bred
in every festering wound,..

51
00:14:48,187 --> 00:14:50,553
is no more, nowhere,
you�re told

52
00:14:56,095 --> 00:14:58,859
Good is bad
bad is good,..

53
00:14:59,098 --> 00:15:01,259
in the false light,
it�s all true

54
00:15:28,428 --> 00:15:31,795
l stopped your transfer orders
on grounds of lapses in the,..

55
00:15:33,032 --> 00:15:36,229
Defence Minister�s security detail,
Home Ministry says, you want to see me

56
00:15:36,369 --> 00:15:37,996
ln private

57
00:15:55,254 --> 00:15:58,553
This file contains whatever the
late Defence Minister had to tell me

58
00:15:59,325 --> 00:16:02,817
But the floppy containing the details
which he wanted to give me,..

59
00:16:02,962 --> 00:16:04,930
isn�t yet in my possession

60
00:16:12,004 --> 00:16:13,938
Your name is the next on the hit list

61
00:16:16,375 --> 00:16:20,971
�Who is behind it, we can know only
from the Defence Minister�s assassin

62
00:16:25,017 --> 00:16:27,884
According to this note,
this information,..

63
00:16:28,020 --> 00:16:29,988
ought to remain only between
you and me?

64
00:16:31,023 --> 00:16:35,153
Because your staff is not beyond
the pale of suspicion, sir

65
00:16:48,307 --> 00:16:50,036
Anytime, Anywhere

66
00:17:20,206 --> 00:17:21,673
Mahadev, wash your hands

67
00:17:21,841 --> 00:17:23,934
l won�t

68
00:17:28,014 --> 00:17:29,777
Mahadev, wash your hands

69
00:17:30,016 --> 00:17:30,983
No,..

70
00:17:34,020 --> 00:17:34,987
l won�t

71
00:18:18,030 --> 00:18:19,793
Don�t wash

72
00:18:20,032 --> 00:18:21,897
Don�t

73
00:18:30,743 --> 00:18:31,903
No, l will

74
00:18:32,078 --> 00:18:36,174
You won�t?
- l will

75
00:18:36,549 --> 00:18:39,245
You won�t,
- l won�t

76
00:18:40,953 --> 00:18:42,181
l will

77
00:18:44,557 --> 00:18:46,047
where�s Raghavan?

78
00:18:47,093 --> 00:18:48,458
One minute

79
00:18:53,099 --> 00:18:54,191
Please hold

80
00:18:58,137 --> 00:18:58,967
l,..

81
00:18:59,138 --> 00:19:00,969
don�t hold

82
00:19:01,841 --> 00:19:04,776
Yakub goes on hold for no one

83
00:19:06,178 --> 00:19:07,577
l�ll wash

84
00:19:43,082 --> 00:19:44,049
One minute

85
00:19:52,258 --> 00:19:58,458
You killed the defence minister,
You got a big price

86
00:20:00,366 --> 00:20:03,665
won�t you give a piece
of the pie to Yakub?

87
00:20:06,072 --> 00:20:08,370
Threatening me?
- Not threat

88
00:20:10,076 --> 00:20:11,509
Tax

89
00:21:07,566 --> 00:21:10,000
Brother sends

90
00:21:51,577 --> 00:21:53,772
Brother sends

91
00:21:56,482 --> 00:21:58,109
A joker?

92
00:23:53,098 --> 00:23:55,066
No one gets killed

93
00:23:56,101 --> 00:23:58,069
No one kills

94
00:24:00,406 --> 00:24:02,340
l�m not the one who�s saying that

95
00:24:05,010 --> 00:24:06,944
lt�s there, written in the Gita

96
00:24:17,222 --> 00:24:19,850
l am merely a tool

97
00:24:31,937 --> 00:24:33,871
ln this game of puppetry,..

98
00:24:35,074 --> 00:24:38,840
l am the killer,..

99
00:24:39,078 --> 00:24:40,705
you are a corpse

100
00:24:45,884 --> 00:24:47,317
Not to mind

101
00:24:55,094 --> 00:24:56,721
Not to mind

102
00:25:12,011 --> 00:25:12,943
Tax

103
00:25:22,688 --> 00:25:23,985
Make sure no one�s left out

104
00:25:26,458 --> 00:25:28,892
�You�ve sent the jeep to the airport?
- Yes, if the flight isn�t late,..

105
00:25:29,028 --> 00:25:30,495
he should be arriving anytime

106
00:25:32,031 --> 00:25:34,499
Don�t touch anything
until Manu arrives

107
00:25:36,368 --> 00:25:37,995
what�s for breakfast?

108
00:25:42,107 --> 00:25:44,041
l don�t see the purpose
in transferring you,..

109
00:25:44,176 --> 00:25:47,543
from vlP security to Crime Branch,
- Just missing you

110
00:25:47,980 --> 00:25:51,609
Me? Or thieves, robbers and murderers?
- Pradhan, l don�t assume,..

111
00:25:51,750 --> 00:25:54,514
you woke me out of my sleep and hauled
me overnight from Delhi for no reason?

112
00:25:56,422 --> 00:25:58,390
what�s this?
- Piece of rubber

113
00:26:03,962 --> 00:26:05,725
Found at the place of crime

114
00:26:12,805 --> 00:26:14,739
Often used for making masks
for films

115
00:26:17,109 --> 00:26:20,977
Rameez was Yeda Yakub�s chauffeur,
an underworld collector

116
00:26:22,014 --> 00:26:23,982
Your underworld types
have interesting names

117
00:26:24,783 --> 00:26:28,651
Same with Yeda Yakub�s throat,
Same modus operandi

118
00:26:29,288 --> 00:26:31,256
Corpse is in the health club, Upstairs

119
00:26:35,160 --> 00:26:39,995
Looks like gang war, Pradhan,
You called me from Delhi for this?

120
00:26:40,365 --> 00:26:44,893
�The assassin himself sent me a message,
to have you called in, Urgently

121
00:26:45,404 --> 00:26:46,666
Message?

122
00:26:48,340 --> 00:26:49,705
with a signature

123
00:27:05,691 --> 00:27:08,660
Killing isn�t only his calling,
Pastime too, looks like

124
00:27:20,472 --> 00:27:24,932
One who leaves his signature
on his work is no simple killer

125
00:27:26,011 --> 00:27:27,945
He�s challenging us,
catch me if you dare

126
00:27:31,016 --> 00:27:34,884
l�m getting Supriya and Gita over here,
- Strange! For all my invitations,..

127
00:27:35,020 --> 00:27:37,887
you never found the opportunity,
And now for the sake of an assassin,..

128
00:27:38,023 --> 00:27:39,991
you�re ready to move en famille?

129
00:27:44,029 --> 00:27:48,796
Friends can be refused, Pradhan,
Not foes

130
00:27:52,905 --> 00:27:56,705
How much longer?
- Here he comes

131
00:28:54,099 --> 00:28:56,067
l can�t see the connection
between the two murders

132
00:28:56,435 --> 00:28:59,700
But the modus operandi is the same,
- Who might be his quarry now?

133
00:29:02,107 --> 00:29:05,770
Still at work? lf you will bring
the office to the dining table,..

134
00:29:05,911 --> 00:29:12,714
l might send dinner to office, right?
- Only the cheeses, Not the kebabs

135
00:29:12,918 --> 00:29:14,317
They�re great

136
00:29:20,626 --> 00:29:24,790
Manu, don�t you sometimes get
the feeling that we married cooks?

137
00:29:25,197 --> 00:29:26,721
And such pretty cooks

138
00:29:28,367 --> 00:29:31,996
�Why only cooks? Nurse, governess,..

139
00:29:33,071 --> 00:29:34,163
Gardener,..
- Washerwoman

140
00:29:34,406 --> 00:29:36,340
Secretary,..
- Housekeeper,..

141
00:29:36,575 --> 00:29:40,705
Accountant, all rolled-into-one,
- All-in-one, let�s eat

142
00:29:41,747 --> 00:29:43,044
where are the kids?

143
00:29:48,420 --> 00:29:52,379
These cartoons and video games
are for little kids, l hate it

144
00:29:53,091 --> 00:29:57,858
But Mummy thinks l�m mad about these,
like other kids

145
00:29:59,765 --> 00:30:03,997
�You know Dipu, scientists
have found a new solar system

146
00:30:07,639 --> 00:30:12,576
Just imagine, a whole new Sun!
lt�s planets

147
00:30:14,079 --> 00:30:18,709
Maybe even people will be there,
- Yes! Also Shah Rukh Khan

148
00:30:21,086 --> 00:30:24,954
why didn�t Manu marry all these years?
- That�s what my father asked him,..

149
00:30:25,090 --> 00:30:27,285
when he went to ask for my hand,
You know what he said?

150
00:30:27,526 --> 00:30:30,051
what,..?
- Guess

151
00:30:32,497 --> 00:30:34,465
Maybe he never found the right girl?
- No

152
00:30:34,700 --> 00:30:38,659
�Then,..?
- He said, he forgot

153
00:30:42,140 --> 00:30:44,267
He forgot to marry?
- He forgot

154
00:30:49,147 --> 00:30:51,115
That line you�ve never heard

155
00:30:51,650 --> 00:30:54,118
My father is so serious,
Even he broke out laughing

156
00:30:58,156 --> 00:31:03,150
How did you win him over?
- He was in love too

157
00:31:03,762 --> 00:31:08,722
But he never dared, Because my father
was his senior, And l was in college

158
00:31:09,301 --> 00:31:15,865
But l had decided, That gave him
the confidence to woo my father

159
00:31:16,975 --> 00:31:19,910
Pradhan, our kids are either
growing up too fast,..

160
00:31:20,078 --> 00:31:21,545
or maybe we just notice
things too late

161
00:31:23,081 --> 00:31:25,606
�Yes, they are growing up,
But in what conditions?

162
00:31:27,085 --> 00:31:35,049
The situation is going from bad to
worse, l feel scared telling him,..

163
00:31:35,193 --> 00:31:38,959
about honesty and integrity, Am l
making him too weak to face,..

164
00:31:39,097 --> 00:31:43,295
the world that is to come about?
ln the coming times of hypocrisy,..

165
00:31:43,435 --> 00:31:47,531
lies, dishonesty, cold-bloodedness,
will he be able to survive?

166
00:31:50,142 --> 00:31:53,509
Sometimes, l feel like retiring
and going back to the village

167
00:31:56,681 --> 00:32:01,209
�You can retire from a job, not from
life, lt�s the coming times,..

168
00:32:01,353 --> 00:32:08,316
�that we�re fighting for,
- Still, of late l miss my village

169
00:32:09,227 --> 00:32:14,995
Particularly, the rains,
- Let�s make it someday, together

170
00:32:16,802 --> 00:32:17,996
To watch the rains in the village

171
00:32:27,078 --> 00:32:30,605
l�ve put the word out to my informers,
ln the underworld,..

172
00:32:30,749 --> 00:32:32,182
things like this don�t stay hidden

173
00:32:32,384 --> 00:32:35,911
So what stays hidden?
Tell us some too

174
00:32:37,088 --> 00:32:39,056
what�s in the tiffin box?
Dipu hasn�t eaten?

175
00:32:40,091 --> 00:32:43,925
Everyone has eaten, This is
for me and this one

176
00:32:47,566 --> 00:32:50,729
�The two of them are locked in battle,
- Oh yes, they aren�t getting married

177
00:32:58,143 --> 00:32:59,838
Good night, Mister Forgot

178
00:33:02,013 --> 00:33:02,945
Forgot,..?

179
00:33:05,016 --> 00:33:06,176
Got it?

180
00:33:08,019 --> 00:33:09,782
Come over,
- Sure

181
00:33:19,431 --> 00:33:21,399
Are boys really such fools?

182
00:33:24,035 --> 00:33:26,003
Had l been a fool, would your Mummy
have married me?

183
00:33:26,605 --> 00:33:29,199
who knows? l wasn�t even born then

184
00:34:40,078 --> 00:34:42,012
This night

185
00:34:42,881 --> 00:34:44,849
ls mine

186
00:34:45,817 --> 00:34:48,047
Like the lizard, it crawls

187
00:34:51,256 --> 00:34:53,224
Like rolling Death�s dice,..

188
00:35:01,700 --> 00:35:06,637
lt swallows, it devours,..
this black, venomos night

189
00:35:07,072 --> 00:35:11,975
Blink,.., you will miss the link
this complicated night

190
00:35:12,577 --> 00:35:17,310
From silence it draws its might,..
this night that moves

191
00:36:11,102 --> 00:36:14,970
lsolation,.. it�s isolation

192
00:36:16,808 --> 00:36:20,437
�Your heart will forever be lonely,..
it�s crazy

193
00:36:37,962 --> 00:36:42,797
Every person is a ruin,..

194
00:36:43,168 --> 00:36:47,002
every heart isolates
another

195
00:36:54,079 --> 00:36:58,778
lt stuns, it strikes,..
this night of Death�s rolling dice

196
00:38:08,853 --> 00:38:12,983
The night turns darker suddenly,..

197
00:38:13,658 --> 00:38:17,992
my veil will slip out of his hands

198
00:38:35,180 --> 00:38:39,048
�They�re ashore;
and yet they drown

199
00:38:40,418 --> 00:38:44,377
lt�s the night that
brings the storm

200
00:38:51,095 --> 00:38:55,930
On bare bodies that speak,..
this is night that simmers

201
00:40:18,082 --> 00:40:19,845
l know who you are

202
00:40:48,546 --> 00:40:50,104
who are you?

203
00:40:57,155 --> 00:40:58,918
Your assassin

204
00:41:23,548 --> 00:41:25,243
Any last wishes?

205
00:41:30,088 --> 00:41:34,252
Only too die, nothing else

206
00:41:41,265 --> 00:41:46,066
You got a last wish?
- Only you, No other wish

207
00:42:15,833 --> 00:42:20,327
How have you been Hanif-Brother?
- All by your grace

208
00:42:33,084 --> 00:42:35,052
who killed Yeda Yakub?

209
00:42:40,825 --> 00:42:44,727
You are asking too big a price
for saving my life once

210
00:42:45,830 --> 00:42:46,797
Name

211
00:43:18,629 --> 00:43:20,392
when did you start smoking?

212
00:43:49,093 --> 00:43:51,061
Raghavan

213
00:43:52,196 --> 00:43:56,860
Dog gets card

214
00:43:59,137 --> 00:44:05,235
Dog, goes for the card

215
00:44:25,930 --> 00:44:29,388
Mahadev-Brother, dog goes
fetches card

216
00:44:30,034 --> 00:44:31,501
Fetches card

217
00:44:32,036 --> 00:44:33,799
Fetches

218
00:44:34,572 --> 00:44:36,506
Dog goes,..
- Fetches card

219
00:44:39,677 --> 00:44:40,769
won�t even die!

220
00:44:43,080 --> 00:44:45,708
Fetches card, Dog fetch card

221
00:44:47,084 --> 00:44:49,052
Dog fetch card

222
00:44:56,827 --> 00:45:01,924
Dog fetches card

223
00:45:05,102 --> 00:45:07,070
we�re playing another game

224
00:45:10,007 --> 00:45:11,770
One,..
- Two

225
00:45:12,009 --> 00:45:13,772
Three,..
- Four

226
00:45:14,145 --> 00:45:16,636
Eighty,..
- Ninety

227
00:45:17,014 --> 00:45:18,777
Count till,..
- A full hundred

228
00:45:19,016 --> 00:45:21,985
Have you counted,..
- Till a hundred?

229
00:45:22,486 --> 00:45:24,977
There�s the thief,..
- Getting away

230
00:46:00,024 --> 00:46:04,927
ln all, 18 Raghavans,
6 in lock-ups, serving life

231
00:46:06,964 --> 00:46:11,731
10 ordinary criminals,
One in death row, awaiting hanging

232
00:46:11,969 --> 00:46:13,732
That leaves one,
what about him?

233
00:46:15,906 --> 00:46:18,136
1983, Age 1 1

234
00:46:21,012 --> 00:46:23,708
She used to work, like me,
Raghu asked for money

235
00:46:24,015 --> 00:46:25,983
She refused, He burnt her alive

236
00:46:27,018 --> 00:46:28,451
Burnt his own mother alive

237
00:46:30,288 --> 00:46:35,248
Age 16, From remand home to
Arthur Road jail, warden�s murder

238
00:46:35,693 --> 00:46:43,998
Mr Deshmukh was a religious man,
He tried reading the Gita to the boys

239
00:46:45,369 --> 00:46:47,997
To reform them, Raghavan took
Gita lessons, But

240
00:46:49,073 --> 00:46:50,233
No one dies

241
00:46:50,408 --> 00:46:51,898
No one kills

242
00:46:52,076 --> 00:46:54,840
l�m not the one who�s saying that,
lt�s there, written in your Gita

243
00:46:56,414 --> 00:47:01,579
1990, Age 18

244
00:47:03,220 --> 00:47:07,657
Jailbreak, Missing ever since,
- Anyone else in the family?

245
00:47:08,959 --> 00:47:10,119
An only brother, was

246
00:47:10,261 --> 00:47:12,923
�That Madhavan? Poor fellow, he was mad

247
00:47:30,915 --> 00:47:32,314
Live till you can

248
00:51:13,737 --> 00:51:15,398
No one dies

249
00:51:18,242 --> 00:51:20,039
No one kills

250
00:52:19,303 --> 00:52:20,793
l�ll get an ambulance

251
00:52:30,881 --> 00:52:32,849
Raining, back home in the village

252
00:53:15,192 --> 00:53:17,160
No one dies

253
00:53:18,028 --> 00:53:19,996
No one kills

254
00:53:21,432 --> 00:53:26,995
�You are a soul, you aren�t born,
you don�t die

255
00:53:40,818 --> 00:53:44,515
As one discards used clothes
for the new

256
00:53:51,095 --> 00:53:56,328
Only form changes,
faces change

257
00:54:03,006 --> 00:54:05,941
�That, which we know as Death, is Life

258
00:54:06,176 --> 00:54:10,545
Of late l miss my village

259
00:54:11,582 --> 00:54:13,311
Particularly, the rains

260
00:54:22,092 --> 00:54:28,520
How are you related to Raghavan?
- As woman to man

261
00:54:28,732 --> 00:54:29,926
Husband or lover?

262
00:54:32,936 --> 00:54:34,164
Killer

263
00:54:34,404 --> 00:54:40,809
what else do you know about Raghavan?
- lt�s beyond my comprehension,..

264
00:54:41,078 --> 00:54:46,038
why the police is interested
in what l know of Raghavan

265
00:54:46,316 --> 00:54:47,943
Answer the question

266
00:54:55,559 --> 00:55:00,929
l didn�t ask that,
- You asked me what l know of him

267
00:55:01,732 --> 00:55:05,725
�What he does, where he goes, who he
meets, address, means of livelihood

268
00:55:11,108 --> 00:55:15,101
Raghavan never asked my age,
l never asked his income

269
00:55:16,814 --> 00:55:19,647
we never wasted time
over silly questions

270
00:55:23,020 --> 00:55:25,989
Do l still have to tell you
what kind of business?

271
00:55:28,325 --> 00:55:29,986
That�s apparent

272
00:55:33,030 --> 00:55:39,230
l know what you want to know from me,
what you don�t know is,..

273
00:55:39,403 --> 00:55:43,772
you�re wasting your time,
- You think you�re very smart?

274
00:55:44,308 --> 00:55:47,038
Raghavan thinks l�m beautiful too

275
00:55:48,946 --> 00:55:49,878
No,..?

276
00:55:56,587 --> 00:56:02,583
But l don�t know of too many girls
who�d care for a killer�s adulation

277
00:56:05,329 --> 00:56:08,924
�Yes, he might wash his bloody hands
before he touches you

278
00:56:11,602 --> 00:56:16,437
The expensive gifts he buys for you
are bloodstained

279
00:56:20,377 --> 00:56:26,976
l don�t know which Raghavan you�re
referring to, The Raghavan l know,..

280
00:56:27,184 --> 00:56:29,379
slakes my thirst with his fire

281
00:56:39,763 --> 00:56:41,788
Have you ever used fire
to slake your thirst, officer?

282
00:56:45,936 --> 00:56:50,896
lf you want, you can make life
easier for me, lf l want,..

283
00:56:51,074 --> 00:56:54,601
l can make life difficult for you,
l want Raghavan

284
00:56:57,814 --> 00:57:00,647
As much as l knew,
l�ve told you

285
00:57:12,362 --> 00:57:15,923
lf you don�t say, Narang will,
- Was l called here for time-pass?

286
00:57:17,501 --> 00:57:19,731
No, my interrogation continues

287
00:57:23,907 --> 00:57:30,710
�The question isn�t where Raghavan is,
Question is whether you know, or not

288
00:57:37,020 --> 00:57:39,147
Question isn�t whether
you will divulge

289
00:57:42,025 --> 00:57:43,993
Answer is, l know how
to make you talk

290
00:58:52,496 --> 00:58:56,057
No one dies, no one kills

291
01:02:48,765 --> 01:02:49,993
You�re gone

292
01:02:53,903 --> 01:02:55,063
You too

293
01:03:39,082 --> 01:03:43,951
l�m Arjun Srivastav, l�ve been deputed
in Mr Pradhan�s place

294
01:03:45,322 --> 01:03:49,725
l needn�t have been sent to
wrap up the rest of the case

295
01:03:50,260 --> 01:03:55,061
Any clerk can do it,
- l see

296
01:03:55,865 --> 01:04:03,931
Using an academy-topper for a cook
is a waste of talent, And why waste?

297
01:04:05,008 --> 01:04:07,101
l�d say, it�s an insult

298
01:04:10,814 --> 01:04:13,647
Had you called me in before
Raghavan was nabbed,..

299
01:04:13,817 --> 01:04:20,450
l�d show you what Arjun Srivastav is,
l�ve majored in criminal psychology

300
01:04:21,024 --> 01:04:22,992
Raghavan would�ve been in jail
in two days

301
01:04:26,029 --> 01:04:27,997
So you�ve been sent
to take Pradhan�s place?

302
01:04:29,632 --> 01:04:31,600
what to do? Had to come,
- Be seated

303
01:04:33,503 --> 01:04:35,994
List the names of people
you�ve shot till date

304
01:04:43,079 --> 01:04:45,604
l haven�t shot anyone,
- At least the names,..

305
01:04:45,749 --> 01:04:49,480
of those you�ve arrested,
- l haven�t arrested anyone

306
01:04:50,086 --> 01:04:53,852
l�ve just passed out of the academy,
- And you come to take Pradhan�s place?

307
01:04:55,258 --> 01:05:01,163
Pradhan had more scars on his body than
the number of years in your age

308
01:05:02,966 --> 01:05:07,835
�You�ve majored in criminal psychology?
ln one of the dark, dank alleys,..

309
01:05:08,004 --> 01:05:11,872
if you come across a Raghavan, he won�t
quiz you about criminal psychology

310
01:05:12,008 --> 01:05:16,945
He will tear you to pieces,
Far more difficult than nabbing him,..

311
01:05:17,113 --> 01:05:24,781
�will be holding Raghavan behind
the bars, and it�s nearly not to be

312
01:05:29,092 --> 01:05:32,323
The police officer�s job isn�t over
until crime is stamped out

313
01:05:34,364 --> 01:05:39,996
ln your place, Pradhan wouldn�t have
let anyone breathe in peace,..

314
01:05:40,203 --> 01:05:44,333
not you, not l,
until Raghavan hanged

315
01:06:17,540 --> 01:06:20,304
After killing the Defence Minister,
you took a floppy

316
01:06:27,016 --> 01:06:32,977
You�re a small cog in the wheels of
a huge conspiracy, Your role was,..

317
01:06:33,122 --> 01:06:37,991
smaller than the bullets which killed
the Defence Minister, But you know,..

318
01:06:38,127 --> 01:06:43,827
�which finger pulled the trigger,
whose the gun it was,..

319
01:06:44,033 --> 01:06:48,936
and the hand that ordered the gun,
And the hand behind that hand,..

320
01:06:49,072 --> 01:06:54,840
you surely know this much?
- l�m an assassin, Not a traitor

321
01:06:57,080 --> 01:07:04,577
ln my business, the client�s name
isn�t divulged, At no cost

322
01:07:09,058 --> 01:07:14,496
At least, you know,..?
You�re sure to hang

323
01:07:16,299 --> 01:07:20,429
Tell me where the floppy is,
You might be let off lightly

324
01:07:34,484 --> 01:07:36,452
Two sides of the coin

325
01:07:38,021 --> 01:07:39,989
Good and bad

326
01:07:41,257 --> 01:07:43,384
Pretty and ugly

327
01:07:44,360 --> 01:07:48,262
�We need each other, Manu verma

328
01:07:53,036 --> 01:07:55,004
we�ll meet in the office

329
01:07:56,339 --> 01:07:57,636
Supriya,..

330
01:08:01,010 --> 01:08:06,505
lsn�t that your wife�s name?
She�s pretty

331
01:08:08,017 --> 01:08:11,976
Had you not been here now, you�d be
in her arms right now, no?

332
01:08:25,435 --> 01:08:28,165
You aren�t a bad fellow,
You�re a demon

333
01:08:30,073 --> 01:08:35,705
You are where you are because it�s
dangerous for society to let you live

334
01:08:36,412 --> 01:08:37,709
wrong

335
01:08:39,082 --> 01:08:40,379
Absolutely wrong

336
01:08:41,517 --> 01:08:47,854
l am where l am because society
doesn�t want to see it�s own face

337
01:08:50,093 --> 01:08:52,960
Society doesn�t have the guts
to admit that,..

338
01:08:53,096 --> 01:08:55,462
one of society�s faces
is Raghavan too

339
01:08:56,132 --> 01:09:05,097
Abolish laws, set men free, And every
Manu verma might become a Raghavan

340
01:09:07,276 --> 01:09:10,473
Because every Manu verma wants
to become a Raghavan

341
01:09:10,680 --> 01:09:12,648
No Raghavan inside this Manu verma

342
01:09:15,084 --> 01:09:20,716
And till this Manu verma is alive,
no Raghavan can stay alive

343
01:09:26,029 --> 01:09:29,829
Look Raghavan, in a very short while,..

344
01:09:30,033 --> 01:09:34,663
you�ll be wearing a black hood

345
01:09:38,074 --> 01:09:41,601
And that nasty laugh of yours
will be buried forever

346
01:09:43,746 --> 01:09:48,615
A noose goes around your neck, Suddenly,
the ground beneath your feet slips away

347
01:09:48,751 --> 01:09:52,710
And you�re swinging in a hole blacker
than the blackest of your crimes

348
01:09:54,257 --> 01:09:57,454
Noose tightens,.. tightens

349
01:09:59,228 --> 01:10:05,599
�You�ll blabber, your lungs will fight
to breathe, but not a breath

350
01:10:07,036 --> 01:10:09,834
First of all, your heart,..

351
01:10:10,073 --> 01:10:11,665
stops

352
01:10:12,942 --> 01:10:14,876
Then your brain will stop working

353
01:10:17,246 --> 01:10:22,445
And then you, Raghavan,
will be gone for good

354
01:10:32,095 --> 01:10:33,392
Felt nice

355
01:10:36,632 --> 01:10:38,327
Felt very nice

356
01:10:40,103 --> 01:10:45,666
lt�s going to be just the way
you enjoyed telling me about it

357
01:10:49,045 --> 01:10:54,677
But Manu verma, one thing never ends

358
01:11:00,957 --> 01:11:05,860
There�s something that�ll never end

359
01:11:09,365 --> 01:11:10,992
See you at that gallows

360
01:11:17,840 --> 01:11:20,240
There�s something that never ends

361
01:11:47,136 --> 01:11:50,003
Thus it is proven that
the accused killed,..

362
01:11:50,139 --> 01:11:54,906
�the Defence Minister, Yeda Yakub,
Rameez and Pradhan

363
01:11:55,711 --> 01:12:00,774
l pray that the court to pronounce the
harshest sentence for Raghavan Ghatge

364
01:12:06,222 --> 01:12:09,988
ln view of evidence and
depositions by witnesses,..

365
01:12:10,693 --> 01:12:20,796
Raghavan Ghatge son of Sadanand Ghatge
is sentenced to hang until death

366
01:12:52,101 --> 01:12:55,832
That�s all there is to your law?

367
01:13:01,344 --> 01:13:07,772
lf l am to hang, does it make
any difference who hangs me?

368
01:13:08,918 --> 01:13:11,648
�Your hangman, or l myself�

369
01:13:14,357 --> 01:13:17,849
�This is a court, Raghavan,
this is my court

370
01:13:19,495 --> 01:13:23,989
l won�t allow you to ridicule my court,
Take this animal out of the court

371
01:13:24,133 --> 01:13:30,561
l�m an animal? And you are human just
because you carry that wooden hammer?

372
01:13:33,476 --> 01:13:35,000
why,..

373
01:13:37,647 --> 01:13:40,047
do you think your decision
is the last word?

374
01:13:42,084 --> 01:13:45,451
Outside this city lies a vast
jungle, Justice Chowdhury

375
01:13:46,889 --> 01:13:53,055
where the darkness reigns,
ln that court,..

376
01:13:55,765 --> 01:14:00,464
one day, you will stand
where l stand today

377
01:14:01,737 --> 01:14:06,333
And l�ll be where
you are sitting

378
01:14:12,014 --> 01:14:19,546
Until then, stay imprisoned in your
black cloak and await your trial

379
01:14:33,369 --> 01:14:38,898
lf life takes away,
so it gives too, l�m fine

380
01:14:53,089 --> 01:14:55,057
ln school to catch a thief?

381
01:14:56,659 --> 01:15:00,823
Got good news for you,
- Nabbed another thief?

382
01:15:05,301 --> 01:15:07,929
we�re going to see Granny
during the holidays

383
01:15:08,170 --> 01:15:09,933
�This time, we�re going,
- l don�t believe this

384
01:15:10,172 --> 01:15:12,106
Just make sure you buy tickets
for Gita and myself

385
01:15:12,341 --> 01:15:16,710
�What will you be doing here, Papa?
- Vanquishing the evil

386
01:15:32,595 --> 01:15:36,395
Go away, Evil

387
01:15:37,466 --> 01:15:41,402
Lord Ram has a bow and arrows

388
01:15:44,073 --> 01:15:45,506
Get lost

389
01:16:01,023 --> 01:16:10,261
Evil has many lives,
Evil is wiser

390
01:16:16,639 --> 01:16:19,972
Go to your City of Evil

391
01:16:23,012 --> 01:16:26,971
Go home and boast

392
01:16:34,023 --> 01:16:37,390
You don�t know The Evil�s powers

393
01:16:39,028 --> 01:16:42,589
The world will be vanquished

394
01:16:46,202 --> 01:16:50,400
You will be crushed

395
01:17:00,950 --> 01:17:04,909
You will be stamped out

396
01:17:09,959 --> 01:17:12,928
You talk too much

397
01:17:19,969 --> 01:17:22,938
l�m going to abduct you

398
01:17:28,010 --> 01:17:30,240
Then l shall wait for the Redeemer

399
01:17:40,890 --> 01:17:43,791
Evil! You are but Evil!

400
01:17:44,026 --> 01:17:47,553
Do not brag,.. you are not bigger
than the Redeemer

401
01:17:51,033 --> 01:17:57,370
l�ll set to fire your empire
with my tail

402
01:17:58,073 --> 01:18:00,940
Remember, you�re female,..
- Okay

403
01:18:01,210 --> 01:18:04,145
l�ll set afire your
Golden City of Sin

404
01:18:04,914 --> 01:18:12,787
�You have abducted Sita,
l�m going to tell Ram

405
01:18:16,759 --> 01:18:21,594
How will you cross the seas?
You will drown

406
01:18:23,566 --> 01:18:30,529
�The City of Sin is far away,
no phones are not working

407
01:18:44,420 --> 01:18:53,522
O�Redeemer, use the e-mail,
lnform my Ram quickly

408
01:19:16,518 --> 01:19:19,715
Let�s fight a war

409
01:19:20,022 --> 01:19:21,649
�Yes, let�s fight

410
01:19:23,192 --> 01:19:25,990
Let the world be cleansed of sin

411
01:19:29,031 --> 01:19:32,330
�The City of Sin
will thrive forever

412
01:19:34,203 --> 01:19:37,172
Evil will thrive
as long as virtue lives

413
01:19:47,082 --> 01:19:50,051
Evil will thrive

414
01:20:15,177 --> 01:20:16,667
who are you?

415
01:20:20,516 --> 01:20:22,040
Your killer

416
01:20:25,688 --> 01:20:27,656
Any last wish?

417
01:20:30,526 --> 01:20:33,586
�When you are with me,
l have no wishes

418
01:22:50,365 --> 01:22:53,857
lf you have come to see me at
the gates of Death, the purpose,..

419
01:22:54,002 --> 01:22:56,971
must be more precious than my death,
- Right, Your life

420
01:22:58,874 --> 01:23:02,970
Here�s your pardon,
with the President�s signature

421
01:23:04,746 --> 01:23:09,979
Tell me where the floppy is, Death
will be commuted to life, Look

422
01:23:16,024 --> 01:23:19,824
You�re here to strike a deal?
- l�m giving life,..

423
01:23:20,028 --> 01:23:23,464
to a convict like you,
How can it come for free?

424
01:23:24,199 --> 01:23:28,602
ln my business, the client�s name
isn�t divulged, whatever the price

425
01:23:31,073 --> 01:23:39,037
The price isn�t money,
lt�s life, You know what life means?

426
01:23:41,083 --> 01:23:45,850
Just now, when you met Nita
for the last time,..

427
01:23:45,988 --> 01:23:48,889
�when you held her hand,
when you felt her heartbeat,..

428
01:23:50,092 --> 01:23:53,061
her eyes, her lips,..

429
01:23:55,097 --> 01:23:58,555
it can all be yours, Not now,
ten years on, But you can have her

430
01:24:06,508 --> 01:24:08,942
No one knows anything about this deal

431
01:24:10,913 --> 01:24:14,781
Apart from the Prime Minister
and myself, no one knows anything

432
01:24:16,151 --> 01:24:18,984
�Your last hope, l am

433
01:24:35,804 --> 01:24:37,271
Look Raghavan,..

434
01:24:38,774 --> 01:24:45,475
the hangman is testing his ropes
with a dummy that weighs like you

435
01:24:46,081 --> 01:24:48,641
The doctor and the magistrate must
be here, The doctor has seen you

436
01:24:49,251 --> 01:24:53,278
He�s only waiting to certify your
corpse, The magistrate must be,..

437
01:24:53,422 --> 01:24:57,290
looking at his watch, He has to drop
his kids to school, The hangman,..

438
01:24:57,426 --> 01:25:02,261
must be turning the hood inside out,
His wife might be at home

439
01:25:02,965 --> 01:25:04,933
He might be in a hurry too

440
01:25:12,874 --> 01:25:17,243
After you�re hanged, everyone
will go back to their lives

441
01:25:19,381 --> 01:25:21,542
Only you won�t be here

442
01:25:23,085 --> 01:25:25,986
Live will be the same,
Only you won�t be

443
01:25:31,493 --> 01:25:32,460
So,..

444
01:25:33,895 --> 01:25:35,192
tell me,..

445
01:25:36,031 --> 01:25:37,658
where the floppy is

446
01:25:40,035 --> 01:25:43,004
They�re coming

447
01:26:44,132 --> 01:26:46,726
You can�t escape,.. you can�t!

448
01:27:05,020 --> 01:27:06,920
Are you all right, sir?

449
01:27:42,023 --> 01:27:43,991
No one kills

