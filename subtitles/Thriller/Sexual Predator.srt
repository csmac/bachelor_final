0
00:00:02,181 --> 00:00:07,181
<i>Bits by Snowdog</i>

1
00:03:57,181 --> 00:04:00,275
-You're looking good, Dutch.
-lt's been a long time, General.

2
00:04:00,384 --> 00:04:02,352
Come on inside.

3
00:04:03,621 --> 00:04:06,146
Eighteen hours ago,
we lost a chopper...

4
00:04:06,257 --> 00:04:09,658
carrying a cabinet minister and his
 aide from this charming little country.

5
00:04:09,760 --> 00:04:13,218
We've got a transponder
fixed on their position...

6
00:04:13,330 --> 00:04:16,629
<i>about here.</i>

7
00:04:16,734 --> 00:04:21,364
<i>This cabinet minister, does he always travel</i>
<i>on the wrong side of the border?</i>

8
00:04:23,140 --> 00:04:25,040
Apparently they strayed off course.

9
00:04:25,142 --> 00:04:27,838
And we're fairly certain
they're in guerrilla hands.

10
00:04:27,945 --> 00:04:32,041
So why don't you use the regular army?
What do you need us for?

11
00:04:32,149 --> 00:04:35,084
<i>Because some damn fool</i>
<i>accused you ofbeing the best.</i>

13
00:04:41,592 --> 00:04:44,493
Dillon!

14
00:04:44,595 --> 00:04:47,723
You son of a bitch!

15
00:04:52,737 --> 00:04:56,173
What's the matter? The C.I .A . got you
pushing too many pencils?

16
00:04:56,273 --> 00:04:58,173
Huh?

17
00:05:00,111 --> 00:05:02,272
Had enough?

18
00:05:02,379 --> 00:05:04,540
Make it easy on yourself, Dutch.

19
00:05:07,017 --> 00:05:11,010
- Okay, okay, okay!
- You never did know when to quit, huh?

20
00:05:11,122 --> 00:05:14,956
- Damn good to see you, Dutch.
- What is this fucking tie business?

21
00:05:15,059 --> 00:05:19,018
Forget about my tie. l heard about
that job you pulled off in Berlin.

22
00:05:19,130 --> 00:05:21,598
- Very nice, Dutch.
- Good old days.

23
00:05:21,699 --> 00:05:25,066
Like the good old days.
Then how come you passed on Libya, huh?

24
00:05:25,169 --> 00:05:29,128
- That wasn't my style.
- You got no style. You know that.

25
00:05:29,240 --> 00:05:31,708
Come on.
Why did you pass?

26
00:05:34,411 --> 00:05:36,777
We're a rescue team, not assassins.

27
00:05:39,850 --> 00:05:42,580
Now, what do we got to do?

28
00:05:42,686 --> 00:05:46,349
That cabinet minister is very important
to our operations in this part of the world.

29
00:05:46,457 --> 00:05:49,255
The general's saying a couple of our friends
are about to get squeezed.

30
00:05:49,360 --> 00:05:51,920
<i>We can't let that happen.</i>
<i>We need the best.</i>

31
00:05:52,029 --> 00:05:55,226
- That's why you're here.
- Go on.

32
00:05:55,332 --> 00:05:57,698
A simple setup.
One-day operation.

33
00:05:57,802 --> 00:05:59,702
We pick up their trail
at the chopper, run 'em down...

34
00:05:59,804 --> 00:06:03,433
grab those hostages and bounce back across
the border before anybody knows we were there.

35
00:06:03,541 --> 00:06:07,500
- What do you mean, " we''?
- l'm going in with you, Dutch.

36
00:06:09,680 --> 00:06:14,413
General, my team always works alone.
You know that.

37
00:06:14,518 --> 00:06:17,180
l'm afraid we all have
our orders, Major.

38
00:06:17,288 --> 00:06:19,188
<i>Once you reach your objective...</i>

39
00:06:19,290 --> 00:06:22,259
<i>Dillon will evaluate the situation</i>
<i>and take charge.</i>

41
00:06:39,810 --> 00:06:44,213
Roger. Over.

42
00:06:49,486 --> 00:06:51,454
Yeah, okay.


44
00:07:28,525 --> 00:07:31,426
<i>- Delta 1-0.</i>
<i>- Roger.</i>

45
00:07:31,528 --> 00:07:33,428
<i>Two, Leader.</i>

46
00:07:33,530 --> 00:07:37,159
Rendezvous points and radio freqs
are indicated and fixed.

47
00:07:37,268 --> 00:07:39,236
AWACS contact
on four-hour intervals.

48
00:07:39,336 --> 00:07:42,100
- Who's our backup?
- No such thing, old buddy.

49
00:07:42,206 --> 00:07:46,165
<i>This is a one-way ticket. Once we cross</i>
<i>that border, we're on our own.</i>

50
00:07:46,277 --> 00:07:50,441
This is getting better by the minute.

52
00:07:54,218 --> 00:07:57,915
- Roger 2-2. Roger.

53
00:07:58,022 --> 00:08:01,321
<i>Tango, Charlie, Delta 1-0.</i>

54
00:08:01,425 --> 00:08:03,393
<i>Two, Leader.</i>

55
00:08:06,864 --> 00:08:08,798
Hey, Billy.

56
00:08:08,899 --> 00:08:10,799
Billy!

57
00:08:10,901 --> 00:08:12,869
<i>The other day,</i> / <i>went up</i>
<i>to my girlfriend.</i> / <i>said...</i>

58
00:08:12,970 --> 00:08:15,268
"You know, l'd like a little pussy."

59
00:08:15,372 --> 00:08:19,706
She said, " Me too.
Mine's as big as a house!''

60
00:08:19,810 --> 00:08:22,973
You see, she wanted
a littler one, 'cause hers was--

61
00:08:25,316 --> 00:08:27,648
As big as a house.

62
00:08:34,224 --> 00:08:37,284
Get that stinking shit
out of my face.

63
00:08:39,697 --> 00:08:41,790
A bunch of slack-jawed
faggots around here!

64
00:08:41,899 --> 00:08:46,666
This stuff will make you a goddamn
sexual tyrannosaurus,just like me.

65
00:08:46,770 --> 00:08:50,069
Yeah. Strap this
on your sore ass, Blain.

67
00:08:59,416 --> 00:09:04,786
<i>That was in '7 2. North of Hue.</i>
<i>Me and Dutch both got one.</i>

68
00:09:27,144 --> 00:09:29,510
That's a real nasty habit
you got there.

70
00:09:37,154 --> 00:09:40,555
Right.

74
00:09:54,671 --> 00:09:56,571
<i>You got it, Leader.</i>

76
00:10:02,913 --> 00:10:05,177
Never knew how much
l missed this, Dutch.

77
00:10:05,282 --> 00:10:08,843
- You never were that smart.

78
00:10:20,264 --> 00:10:22,824
Hawkins, you're up.

79
00:10:22,933 --> 00:10:25,163
<i>Lines away.</i>

82
00:13:10,534 --> 00:13:12,627
The pilots have each
got one round in the head.

83
00:13:12,736 --> 00:13:15,330
Whoever hit it
stripped the shit out of it.

84
00:13:15,439 --> 00:13:18,431
- Took him out with a heat-seeker.
- There's something else, Major.

85
00:13:18,542 --> 00:13:21,511
- Hmm?
- This is no ordinary army taxi.

86
00:13:21,612 --> 00:13:24,445
lt looks more like
a surveillance bird to me.

87
00:13:24,548 --> 00:13:26,880
Pick up the trail yet?

88
00:13:26,984 --> 00:13:30,647
Billy's on it.
Heat-seeker, Dillon.

89
00:13:30,754 --> 00:13:33,450
That's pretty sophisticated for a bunch
of half-assed mountain boys.

90
00:13:33,557 --> 00:13:36,924
- Major!
- l guess they're getting better equipped every day.

91
00:13:37,027 --> 00:13:38,961
There were 1 2 guerrillas.

92
00:13:39,062 --> 00:13:42,691
They took the two men from the helicopter,
but there's something else.

93
00:13:42,799 --> 00:13:47,168
- What do you mean?
- Six men wearing U.S-issued army boots.

94
00:13:47,271 --> 00:13:50,434
They came in from the north,
and then they followed the guerrillas.

95
00:13:52,676 --> 00:13:54,644
Mean anything to you?

96
00:13:56,246 --> 00:13:58,305
Probably just another rebel patrol.

97
00:13:58,415 --> 00:14:01,578
They operate in here all the time.

98
00:14:01,685 --> 00:14:05,212
- Get ahead and see what you can find.
- Yes, sir.

100
00:14:35,018 --> 00:14:36,918
What's he got?

101
00:14:37,020 --> 00:14:40,217
Same business. Guerrillas hauling
two guys from the chopper...

102
00:14:40,324 --> 00:14:43,418
followed by men
with American equipment.

103
00:14:43,527 --> 00:14:45,586
Do you remember Afghanistan?

104
00:14:45,696 --> 00:14:48,426
l'm trying to forget it.
Come on. Let's go.

112
00:16:14,518 --> 00:16:16,486
Holy mother of God.

114
00:16:43,046 --> 00:16:45,207
Jim Hopper.

115
00:16:45,315 --> 00:16:47,943
Mac, cut 'em down.

116
00:16:58,528 --> 00:17:00,428
l knew these men.

117
00:17:00,530 --> 00:17:02,464
Green Berets out of Fort Bragg.

118
00:17:02,566 --> 00:17:04,761
What the hell were they doing here?

119
00:17:04,868 --> 00:17:08,167
l don't know, Dutch.
This is inhuman.

120
00:17:08,271 --> 00:17:11,001
Nobody told me there was
an operation in this area.

121
00:17:11,107 --> 00:17:14,838
- They shouldn't have been here.
- But somebody sent them.

122
00:17:18,715 --> 00:17:21,582
<i>The guerrillas skinned them?</i>

123
00:17:21,685 --> 00:17:23,778
<i>Why did they skin them?</i>

124
00:17:25,288 --> 00:17:27,756
Ain't no way for no soldier to die.

125
00:17:32,329 --> 00:17:35,730
<i>- What happened here, Billy?</i>
- Strange, Major.

126
00:17:35,832 --> 00:17:38,460
There was a firefight.

127
00:17:38,568 --> 00:17:40,695
They were shooting
in all directions.

128
00:17:42,072 --> 00:17:44,404
l can't believe thatJim Hopper
walked into an ambush.

129
00:17:44,508 --> 00:17:47,170
l don't believe he did.

130
00:17:47,277 --> 00:17:50,007
l can't find a single track.

131
00:17:50,113 --> 00:17:52,445
Just doesn't make sense.

132
00:17:52,549 --> 00:17:54,676
What about the rest
of Hopper's men?

133
00:17:54,784 --> 00:17:57,514
There's no sign, sir.

134
00:17:57,621 --> 00:17:59,589
They never left here.

135
00:18:01,691 --> 00:18:04,387
Hell, it's like
they just disappeared.

136
00:18:07,163 --> 00:18:09,154
Stick with the guerrilla trail.

137
00:18:09,266 --> 00:18:11,291
Let's get the hostages.

138
00:18:11,401 --> 00:18:13,562
We move, five-meter spread.

139
00:18:13,670 --> 00:18:15,570
No sound.

140
00:18:15,672 --> 00:18:18,903
lt's time to let Old Painless out the bag.

141
00:18:22,946 --> 00:18:25,244
Payback time.

145
00:19:56,373 --> 00:19:58,273
You're ghostin' us, motherfucker.

146
00:19:58,375 --> 00:20:00,275
l don't care who you are
back in the world.

147
00:20:00,377 --> 00:20:02,277
You give our position
one more time...

148
00:20:02,379 --> 00:20:06,179
l'll bleed you real quiet
and leave you here.

149
00:20:06,282 --> 00:20:08,250
Got that?

155
00:21:23,359 --> 00:21:25,725
- Fuck you.

158
00:21:54,858 --> 00:21:57,622
He killed one of the hostages.
We move.

159
00:21:57,727 --> 00:21:59,627
Mac, Blain-- the nest.

160
00:21:59,729 --> 00:22:02,698
Billy, Poncho-- the guard.
Hawkins, Dillon-- backup.

161
00:22:02,799 --> 00:22:04,767
As soon as they're set,
l hit the fuel dump.

164
00:23:03,560 --> 00:23:05,528
One down.

169
00:24:04,954 --> 00:24:07,184
What the hell's he doing?

170
00:24:20,837 --> 00:24:23,032
What the fuck?

173
00:24:33,683 --> 00:24:37,175
- Showtime, kid.

176
00:25:01,311 --> 00:25:03,973
<i>Targets at the center of the palapa!</i>

179
00:25:27,904 --> 00:25:29,838
Dutch, on your nine!

184
00:26:34,137 --> 00:26:35,832
Get that mother--

186
00:26:45,748 --> 00:26:47,477
Go!

189
00:27:21,150 --> 00:27:22,913
Stick around.

190
00:27:23,019 --> 00:27:25,214
Hostages are inside!

191
00:27:31,627 --> 00:27:33,686
Knock, knock.

193
00:27:42,505 --> 00:27:44,473
l got 'em!

194
00:27:44,574 --> 00:27:47,236
Hawkins, call in position
and situation.

195
00:27:47,343 --> 00:27:49,402
<i>Get Connor for the hook!</i>

196
00:27:49,512 --> 00:27:51,480
You got it, Major.

198
00:27:58,888 --> 00:28:01,049
Oh, shit.

199
00:28:01,157 --> 00:28:03,921
- Mac, any sign of the other hostage?
<i>- Yo!</i>

200
00:28:04,961 --> 00:28:06,861
Found the other guy.

201
00:28:06,963 --> 00:28:10,364
He's dead too.
And the kid from the chopper.

202
00:28:10,466 --> 00:28:13,128
But if they're Central American,
l'm a goddamn Chinaman.

203
00:28:13,236 --> 00:28:16,467
From the looks of it,
our cabinet minister was C.I .A .

204
00:28:16,572 --> 00:28:18,699
<i>Another thing, Major.</i>
<i>We were lucky.</i>

205
00:28:18,808 --> 00:28:23,142
<i>Other guys we waxed--</i>
<i>Russian military advisers.</i>

206
00:28:23,246 --> 00:28:25,407
Something pretty big
was gonna happen here.

207
00:28:25,515 --> 00:28:28,109
Good work, Mac.
Clear the area. No traces.

208
00:28:28,217 --> 00:28:31,152
- Get the men ready to move.
- All right.

209
00:28:38,928 --> 00:28:42,386
Son of a bitch is dug in
like an Alabama tick.

210
00:28:42,498 --> 00:28:44,466
You're hit.
You're bleeding, man.

211
00:28:44,567 --> 00:28:46,660
l ain't got time to bleed.

212
00:28:46,769 --> 00:28:49,294
Oh, okay.

213
00:28:52,041 --> 00:28:54,874
You got time to duck?

214
00:29:02,451 --> 00:29:04,510
This is goddamn beautiful.

215
00:29:07,456 --> 00:29:09,356
Goddamn jackpot.

216
00:29:09,458 --> 00:29:12,393
<i>This is more than</i>
<i>we ever thought we'd get.</i>

217
00:29:13,663 --> 00:29:16,791
- Man, we got those bastards.

218
00:29:16,899 --> 00:29:18,799
We got 'em.

219
00:29:18,901 --> 00:29:20,801
l think this is
what you're looking for.

220
00:29:20,903 --> 00:29:23,303
You set us up!

221
00:29:23,406 --> 00:29:28,275
lt's all bullshit, all of it--
the cabinet minister, the whole business.

222
00:29:28,377 --> 00:29:31,642
- You got us in here to do your dirty work.
- We stopped a major invasion.

223
00:29:31,747 --> 00:29:34,045
ln three days, they'd have been
across the border with this stuff!

224
00:29:34,150 --> 00:29:37,517
- Why us?
- Because nobody else could've pulled it off.

225
00:29:37,620 --> 00:29:39,850
You're pissed about the cover story.
l knew l couldn't get you in here without it.

226
00:29:39,956 --> 00:29:41,924
So what story did
you hand Hopper?

227
00:29:42,024 --> 00:29:45,152
Look, we've been looking
for this place for months.

228
00:29:45,261 --> 00:29:47,593
My men were in that chopper
when it got hit!

229
00:29:47,697 --> 00:29:49,824
Hopper's orders were to go in
and get my men, and he disappeared!

230
00:29:49,932 --> 00:29:52,264
He didn't disappear.
He was skinned alive!

231
00:29:52,368 --> 00:29:54,734
My orders were to get somebody in
who could crack these bastards!

232
00:29:54,837 --> 00:29:58,637
So you cooked up a story and dropped
the six of us in a meat grinder.

234
00:30:01,744 --> 00:30:04,338
What happened to you, Dillon?

235
00:30:04,447 --> 00:30:06,779
You used to be somebody
l could trust.

236
00:30:06,882 --> 00:30:09,680
l woke up.

237
00:30:09,785 --> 00:30:12,151
Why don't you?

238
00:30:12,255 --> 00:30:17,056
You're an asset,
an expendable asset...

239
00:30:17,159 --> 00:30:19,093
and l used you to get the job done.

240
00:30:19,195 --> 00:30:21,095
Got it?

241
00:30:23,399 --> 00:30:25,765
My men are not expendable.

242
00:30:27,637 --> 00:30:29,605
And l don't do this kind of work.

243
00:30:29,705 --> 00:30:32,606
<i>Major! Major,</i>
<i>we stepped into some bullshit here!</i>

245
00:30:35,711 --> 00:30:38,043
Air surveillance says we got guerrillas
all over the place.

246
00:30:38,147 --> 00:30:40,377
Can't be more than one, two miles away.
This place is going down.

247
00:30:40,483 --> 00:30:42,383
- How much time?
- Half an hour, maybe less.

248
00:30:42,485 --> 00:30:44,385
- Tell Mac we move in five.
- Yes, sir.

249
00:30:44,487 --> 00:30:47,285
She goes with us. She's too valuable.
She's got another whole network.

250
00:30:47,390 --> 00:30:50,382
She'll give away our position
any chance she gets. No deal.

251
00:30:50,493 --> 00:30:54,327
You're still under orders.
You want to make that call, or should l?

252
00:30:57,033 --> 00:31:00,469
She's your baggage.
You fall behind, and you're on your own.

255
00:31:09,412 --> 00:31:11,312
This place is too hot for a pickup.

256
00:31:11,414 --> 00:31:13,575
They won't touch us
till we get over the border.

257
00:31:13,683 --> 00:31:16,846
Hey, Billy, give me
a way out of this hole.

258
00:31:16,952 --> 00:31:19,250
The aerial says we are cut off.

259
00:31:19,355 --> 00:31:22,290
The only way out of here is that valley
that leads to the east.

260
00:31:22,391 --> 00:31:25,519
l wouldn't waste that
on a broke-dick dog.

261
00:31:25,628 --> 00:31:28,620
Not much choice.

262
00:31:28,731 --> 00:31:31,427
Poncho, take lead.
Double-time it.

268
00:31:51,587 --> 00:31:53,612
<i>-Dillon! Dillon!</i>
- Come on.

269
00:31:53,723 --> 00:31:56,123
Over here.

271
00:32:09,238 --> 00:32:11,934
- Turn around.
- Why?

272
00:32:22,618 --> 00:32:24,552
Thanks.

273
00:32:24,653 --> 00:32:27,144
Anytime.

274
00:32:33,262 --> 00:32:35,594
Billy! Billy!

275
00:32:37,032 --> 00:32:39,830
The other day,
l was going down on my girlfriend...

276
00:32:39,935 --> 00:32:42,495
and l said to her,
"Geez, you got a big pussy.

277
00:32:42,605 --> 00:32:44,505
Geez, you got a big pussy."

278
00:32:44,607 --> 00:32:46,507
She said, " Why did you
say that twice?''

279
00:32:46,609 --> 00:32:48,577
l said, " l didn't."

280
00:32:49,812 --> 00:32:52,610
See, it was 'cause--
'cause of the echo.

286
00:34:18,834 --> 00:34:20,961
<i>Over here. Over here. Over here.</i>

287
00:34:23,138 --> 00:34:26,471
<i>- Turn around. Turn around.</i>

288
00:34:26,575 --> 00:34:28,736
<i>Turn around.</i>
<i>Turn around. Over here.</i>

290
00:34:34,350 --> 00:34:36,318
<i>Anytime.</i>

292
00:34:50,266 --> 00:34:52,860
<i>Anytime.</i>

294
00:36:36,138 --> 00:36:38,538
- Goddamn!

295
00:36:39,575 --> 00:36:41,668
<i>Buddy, buddy, buddy, buddy!</i>

296
00:36:43,278 --> 00:36:47,271
l've seen some bad-ass bush before,
man, but nothing like this.

297
00:36:47,383 --> 00:36:49,283
l hear you.

298
00:36:49,385 --> 00:36:51,512
This shit's something.

299
00:36:51,620 --> 00:36:54,919
Makes Cambodia look like Kansas.

300
00:36:55,024 --> 00:36:58,653
Hey, <i>qué pasa, amigo.</i>
A little taste of home.

302
00:37:02,598 --> 00:37:06,364
You lose it here,
you're in a world of hurt.

303
00:37:14,410 --> 00:37:18,244
Come on, sweetheart.
Stop sandbaggin' it. Now, get up.

304
00:37:18,347 --> 00:37:21,373
Come on. Get up.
Would you get up?

307
00:37:29,925 --> 00:37:33,486
Maybe you'd better put her on a leash, agent man.

308
00:37:42,171 --> 00:37:45,106
Try it again, please.

310
00:38:48,871 --> 00:38:52,034
What's got Billy so spooked?

311
00:38:52,141 --> 00:38:54,041
Can't say, Major.

312
00:38:54,143 --> 00:38:56,941
Been acting squirrelly all morning.

313
00:38:57,045 --> 00:39:00,606
That damn nose of his, it's weird.

314
00:39:42,224 --> 00:39:44,818
What is it?

315
00:39:46,762 --> 00:39:49,526
Billy--

316
00:39:51,033 --> 00:39:53,467
What the hell is wrong with you?

317
00:39:57,439 --> 00:39:59,464
There's something in those trees.

319
00:40:36,178 --> 00:40:39,306
Do you see anything...

320
00:40:39,414 --> 00:40:41,314
up there?

321
00:40:41,416 --> 00:40:43,384
Nothing.

322
00:40:45,420 --> 00:40:47,388
What do you think?

323
00:40:53,295 --> 00:40:55,889
l guess it's nothing, Major.

328
00:41:50,986 --> 00:41:53,147
Please.

329
00:41:53,255 --> 00:41:55,553
Please!

335
00:43:07,429 --> 00:43:09,329
Billy, break left.

336
00:43:09,431 --> 00:43:12,025
- Mac, right.
- What the--

337
00:43:12,134 --> 00:43:15,729
<i>- This isn't her blood.</i>
- What the hell did you do to him?

338
00:43:15,837 --> 00:43:18,965
Major, you'd better
take a look at this.

339
00:43:19,074 --> 00:43:21,167
Did you find Hawkins?

340
00:43:21,276 --> 00:43:24,302
l-- l can't tell.

341
00:43:41,930 --> 00:43:44,524
- What in God's name?

342
00:43:44,633 --> 00:43:47,033
l think it's Hawkins.

343
00:43:47,135 --> 00:43:49,695
Where the hell is his body?

344
00:43:49,805 --> 00:43:52,273
There's no sign of it.

345
00:43:53,575 --> 00:43:56,043
Ask her what happened.

350
00:44:15,997 --> 00:44:19,262
She says the jungle,
it just came alive...

351
00:44:19,367 --> 00:44:21,301
- and took him.
- Bullshit!

352
00:44:21,403 --> 00:44:23,303
That's not what she said!

353
00:44:23,405 --> 00:44:25,305
What she said
doesn't make any sense.

354
00:44:25,407 --> 00:44:28,467
- Those sappers have been following us--
- They've been in front of us!

355
00:44:28,577 --> 00:44:32,707
- Hold it. Hold it!

356
00:44:32,814 --> 00:44:35,977
<i>Why didn't they take</i>
<i>his radio or his weapon?</i>

357
00:44:36,084 --> 00:44:38,052
Why didn't she escape?

358
00:44:42,090 --> 00:44:44,285
- Hopper.
- What?

359
00:44:45,927 --> 00:44:48,725
They did the same thing
toJim Hopper.

360
00:44:54,169 --> 00:44:56,433
l want Hawkins's body found.
Sweep pattern.

361
00:44:56,538 --> 00:45:00,269
<i>Double back.</i>
<i>Fifty meters. Let's go.</i>

364
00:46:49,884 --> 00:46:52,785
Come on in, you fuckers.

365
00:46:52,887 --> 00:46:55,651
Come on in.

366
00:46:55,757 --> 00:46:58,055
Old Painless is waiting.

369
00:47:26,855 --> 00:47:30,791
Sergeant!

371
00:47:34,596 --> 00:47:38,464
<i>- Motherfucker!</i>

379
00:49:08,022 --> 00:49:09,922
- What happened?
- l saw it.

380
00:49:10,024 --> 00:49:11,992
- You saw what?
- l saw it!

381
00:49:30,278 --> 00:49:32,269
Blain.

382
00:49:32,380 --> 00:49:36,248
No powder burns, no shrapnel.

383
00:49:36,351 --> 00:49:39,013
The wound's all fused, cauterized.

384
00:49:39,120 --> 00:49:41,179
What the hell could have
done this to a man?

385
00:49:41,289 --> 00:49:44,690
Mac. Mac, look at me!

386
00:49:44,793 --> 00:49:47,455
Who did this?

387
00:49:47,562 --> 00:49:51,259
l don't know, goddamn it.
l saw s-something.

388
00:49:53,868 --> 00:49:57,463
Not a thing.
Not a fucking trace.

389
00:49:57,572 --> 00:50:01,406
<i>No blood, no bodies.</i>
<i>We hit nothing.</i>

390
00:50:08,917 --> 00:50:12,478
Dillon, better get on the radio.

391
00:50:14,389 --> 00:50:16,289
Mac.

392
00:50:16,391 --> 00:50:18,757
- Sergeant!
- Yes, sir.

393
00:50:18,860 --> 00:50:20,760
l want a defensive position
above that ridge...

394
00:50:20,862 --> 00:50:22,796
mined with everything we've got.

395
00:50:25,467 --> 00:50:27,901
Put him in his poncho.
Take him with us.

396
00:50:28,002 --> 00:50:30,061
l got him.

397
00:50:46,087 --> 00:50:48,055
<i>Vámonos.</i>

399
00:51:33,167 --> 00:51:37,501
<i>Major,</i> / <i>set up flares,</i>
<i>frags and claymores.</i>

400
00:51:37,605 --> 00:51:41,405
Nothing's coming near this place
without tripping on something.

401
00:51:41,509 --> 00:51:43,477
<i>Thank you, Sergeant.</i>

402
00:51:45,313 --> 00:51:47,645
<i>Mac...</i>

403
00:51:47,749 --> 00:51:50,513
he was a good soldier.

404
00:51:51,786 --> 00:51:55,483
He was, um...

405
00:51:55,590 --> 00:51:57,558
my friend.

406
00:53:04,158 --> 00:53:06,786
Good-bye, bro.

411
00:54:13,161 --> 00:54:17,996
Blazer 1 , l repeat:
extraction necessary.

412
00:54:18,099 --> 00:54:20,033
Say again, Blazer 1 . Say again.

413
00:54:20,134 --> 00:54:23,262
- Request for extraction denied.

414
00:54:23,371 --> 00:54:27,705
The area is still compromised. Proceed
to sector 3,000 for prisoner extraction.

415
00:54:27,809 --> 00:54:30,903
- Priority, out. Next contact: 0930.

416
00:54:31,012 --> 00:54:34,004
Roger, Blazer 1 . 1 030 hours.

417
00:54:34,115 --> 00:54:37,448
Damn bastards.
They say we're still in too far...

418
00:54:37,552 --> 00:54:39,520
and they can't risk
comin' in after us.

419
00:54:39,620 --> 00:54:42,885
We're assets, Dillon.
Expendable assets.

420
00:54:42,990 --> 00:54:45,584
lt comes with the job.
l can accept it.

421
00:54:45,693 --> 00:54:50,027
Bullshit.
You're just like the rest of us.

422
00:54:50,131 --> 00:54:52,998
Shitload of good a chopper's
gonna do us in here anyhow.

423
00:54:57,371 --> 00:55:00,829
Sergeant? Sergeant! Sergeant!

424
00:55:00,942 --> 00:55:03,775
Who hit us today?

425
00:55:03,878 --> 00:55:08,872
l don't know. l only saw
one of them camouflaged.

426
00:55:08,983 --> 00:55:11,713
He was there.

427
00:55:11,819 --> 00:55:14,913
- Those eyes disappeared.
- What was that?

428
00:55:16,390 --> 00:55:20,224
Those eyes, they--
they disappeared.

429
00:55:20,328 --> 00:55:24,958
/ <i>know one thing, Major.</i>
/ <i>drew down and fired straight at it.</i>

430
00:55:25,066 --> 00:55:29,560
Capped off 200 rounds
in the mini-gun, full pack.

431
00:55:29,670 --> 00:55:33,766
Nothin'. Nothin' on this Earth
could've lived...

432
00:55:33,875 --> 00:55:38,107
not at that range.

433
00:55:39,680 --> 00:55:44,242
Mac, you take first watch,
then you get some rest.

434
00:55:44,352 --> 00:55:47,617
Ask her--
Ask her what she saw.

435
00:55:47,722 --> 00:55:50,816
Ask her
what happened to Hawkins.

436
00:55:50,925 --> 00:55:53,416
Go ahead. Ask her.

439
00:56:02,069 --> 00:56:05,596
She says the same fuckin' thing.
The jungle came alive and took him.

440
00:56:06,774 --> 00:56:09,641
Billy, you know somethin'.

441
00:56:09,744 --> 00:56:13,510
- What is it?
- l'm scared, Poncho.

442
00:56:13,614 --> 00:56:16,606
<i>Bullshit.</i>
<i>You ain't afraid of no man.</i>

443
00:56:16,717 --> 00:56:20,016
There's something out there
waiting for us...

444
00:56:21,656 --> 00:56:23,817
and it ain't no man.

445
00:56:31,799 --> 00:56:35,599
We're all gonna die.

446
00:56:35,703 --> 00:56:38,604
The man's losin' it.
He's losin' his cool.

447
00:56:38,706 --> 00:56:41,903
<i>There's nothin'but a couple of guys</i>
<i>runnin'around out there,</i>
<i>and we've gotta take 'em down.</i>

448
00:56:42,009 --> 00:56:45,001
You still don't understand,
Dillon, do you?

449
00:56:45,112 --> 00:56:49,208
Whatever it is out there,
it killed Hopper...

450
00:56:49,317 --> 00:56:52,286
and now it wants us.

452
00:56:56,657 --> 00:57:02,118
<i>Here we are again, bro.</i>
<i>Just you and me.</i>

453
00:57:02,230 --> 00:57:05,597
Same kind of moon.
Same kind of jungle.

454
00:57:05,700 --> 00:57:08,430
Real number 1 0 night.
Remember?

455
00:57:08,536 --> 00:57:13,735
Whole platoon, 32 men, chopped into meat,
and we walk out,just you and me.

456
00:57:13,841 --> 00:57:18,676
Nobody else. Right on top of'em.

457
00:57:18,779 --> 00:57:20,940
Not a scratch.
Not a fuckin' scratch.

458
00:57:21,048 --> 00:57:24,176
You know, whoever got you,
he'll come back again...

459
00:57:24,285 --> 00:57:27,379
and when he does,
l'm gonna cut your name right into him.

460
00:57:28,689 --> 00:57:32,420
l'm gonna cut your name into him.

463
00:57:50,878 --> 00:57:53,711
<i>- What the hell is it?</i>
- Mac!

465
00:57:56,284 --> 00:57:59,185
<i>- This way!</i>
- Where the hell are you? Where are you?

466
00:58:00,688 --> 00:58:04,784
<i>Mac! Mac!</i>

467
00:58:04,892 --> 00:58:08,555
<i>- Mac!</i>

469
00:58:11,232 --> 00:58:14,258
- Mac!
- Mac! Where are you?

470
00:58:14,368 --> 00:58:17,462
- Don't fuck with me!
<i>- Mac!</i>

471
00:58:17,571 --> 00:58:21,200
- Mac, where are you?

472
00:58:22,877 --> 00:58:26,108
-Jesus.
- Got you, motherfucker.

473
00:58:26,213 --> 00:58:29,273
Killed you, you fuck.

474
00:58:29,383 --> 00:58:32,375
<i>Jesus. You killed a pig.</i>

475
00:58:32,486 --> 00:58:35,455
<i>- What the--</i>
- Do you think you could've
found something bigger?

476
00:58:35,556 --> 00:58:38,423
- Yeah. Fuck you, Poncho. Fuck you.

477
00:58:40,061 --> 00:58:43,497
- Where's the girl?
- Aw, shit.

478
00:58:43,597 --> 00:58:46,930
Why the hell wasn't anybody watching her?

479
00:58:49,603 --> 00:58:53,437
- Why didn't she try to get away?

480
00:58:53,541 --> 00:58:56,305
<i>Look at her.</i>
<i>She's scared out ofher mind.</i>

481
00:58:56,410 --> 00:58:59,504
Major, you'd better
take a look at this.

482
00:59:05,853 --> 00:59:07,912
<i>Blain's body.</i> /<i>t's gone.</i>

483
00:59:08,022 --> 00:59:11,514
lt came in through the trip wires.

484
00:59:11,625 --> 00:59:15,254
Took it right out
from under our noses.

486
00:59:27,775 --> 00:59:31,006
That boar had to
set off a trip flare, Major...

487
00:59:31,112 --> 00:59:33,012
because there ain't no other tracks.

488
00:59:33,114 --> 00:59:37,517
How could anyone get through this,
carry out Blain without us knowing it?

489
00:59:37,618 --> 00:59:41,076
Why didn't he try and kill one of us?

490
00:59:41,188 --> 00:59:46,387
He came in to get the body.
He's killing us one at a time.

491
00:59:46,494 --> 00:59:49,691
<i>Like a hunter.</i>

492
01:00:05,379 --> 01:00:08,542
He's using the trees.

493
01:00:18,492 --> 01:00:22,656
- Yesterday. What did you see?
- You're wasting your time.

494
01:00:22,763 --> 01:00:25,857
No more games.

495
01:00:27,802 --> 01:00:31,829
l don't know what it was. lt--

496
01:00:31,939 --> 01:00:36,740
- Go on.
- lt changed colors, like the chameleon.

497
01:00:36,844 --> 01:00:39,335
lt uses the jungle.

498
01:00:39,447 --> 01:00:42,814
You saying that Blain and Hawkins
were killed by a fuckin' lizard?

499
01:00:42,917 --> 01:00:45,511
That's a bullshit psych job. There's two
or three men out there at the most.

500
01:00:45,619 --> 01:00:48,588
Fuckin' lizard!

501
01:00:50,291 --> 01:00:53,419
- What's your name?
- Anna.

502
01:00:58,232 --> 01:01:03,727
Anna, this thing
is hunting us, all of us.

503
01:01:04,839 --> 01:01:06,807
You know that.

504
01:01:09,310 --> 01:01:11,278
What the hell
do you think you're doin'?

505
01:01:11,378 --> 01:01:14,370
- We're gonna need everyone.
- l'm takin' her back.
We're out of here in five minutes.

506
01:01:14,482 --> 01:01:17,645
- You're not going yet.
- Look. The rendezvous is 1 0 to 1 2 miles away.

507
01:01:17,751 --> 01:01:21,278
- You think the chopper's gonna wait?
- Dillon, we make a stand now...

508
01:01:21,388 --> 01:01:24,084
or there will be nobody left
to go to the chopper.

509
01:01:24,191 --> 01:01:26,682
There is something else.

510
01:01:26,794 --> 01:01:30,628
When the big man was killed,
you must've wounded it.

511
01:01:30,731 --> 01:01:33,393
lts blood was on the leaves.

512
01:01:33,501 --> 01:01:36,436
lf it bleeds, we can kill it.

514
01:02:02,563 --> 01:02:05,430
Look out.

515
01:02:25,953 --> 01:02:29,480
You really think this Boy Scout
bullshit's gonna work?

516
01:02:29,590 --> 01:02:33,424
lt can see our trip wires.
Maybe it can't see this.

517
01:02:33,527 --> 01:02:36,758
lnstead of complaining,
maybe you should help.

519
01:03:36,170 --> 01:03:38,638
What makes you think
he's gonna come in through here?

520
01:03:38,739 --> 01:03:43,540
There are trip wires on every tree
for 50 yards. This is the only way in.

521
01:04:09,937 --> 01:04:12,633
When l was little, we found a man.

522
01:04:12,740 --> 01:04:16,267
He looked like-- like, butchered.

523
01:04:16,377 --> 01:04:19,471
The old women in the village
crossed themselves...

524
01:04:19,580 --> 01:04:23,141
and whispered crazy things,
said strange things.

526
01:04:27,221 --> 01:04:30,713
Only in the hottest years
this happens.

527
01:04:32,893 --> 01:04:36,090
And this year, it grows hot.

528
01:04:37,298 --> 01:04:40,062
We begin finding our men.

529
01:04:40,167 --> 01:04:43,568
We found them sometimes
without their skin...

530
01:04:43,671 --> 01:04:46,834
and sometimes
much, much worse.

532
01:04:52,413 --> 01:04:57,874
Means the demon
who makes trophies of man.

535
01:05:16,203 --> 01:05:19,138
So what are you gonna try next?
Cheese?

536
01:05:23,044 --> 01:05:25,376
Hey, Dutch! Dutch!

537
01:06:32,646 --> 01:06:34,773
- Shit!

546
01:07:10,484 --> 01:07:13,976
Got you, you mother!
l got you!

547
01:07:15,523 --> 01:07:19,892
- l'm comin'!
- Mac. Mac!

548
01:07:19,994 --> 01:07:22,394
- Get Ramirez on his feet and get to the chopper.
- Right.

549
01:07:22,496 --> 01:07:25,727
- Hold it, Dutch. l'm goin' after Mac.
- That's not your style, Dillon.

550
01:07:25,833 --> 01:07:28,734
l guess l picked up some bad habits from
you. Now, get your people out of here.

551
01:07:28,836 --> 01:07:31,134
You can't win this, Dillon.

552
01:07:32,573 --> 01:07:35,770
Maybe l can get even.

553
01:07:35,876 --> 01:07:37,935
<i>Dillon.</i>

554
01:07:41,582 --> 01:07:44,676
Just hold onto that damn chopper.

557
01:07:52,760 --> 01:07:57,322
- He's busted up pretty bad, Major.
- l can make it. l can make it.

558
01:07:57,431 --> 01:08:00,332
- Get the radio. Forget the rest.
- Right.

559
01:08:00,434 --> 01:08:03,164
- Come on, Poncho. Come on, Poncho.

560
01:08:03,270 --> 01:08:06,262
- Come on.

564
01:09:08,836 --> 01:09:11,805
<i>Turn around.</i>

565
01:09:13,540 --> 01:09:16,532
<i>Over here.</i>

566
01:09:20,381 --> 01:09:23,350
<i>Over here.</i>

567
01:09:29,657 --> 01:09:31,625
Mac?

568
01:09:38,699 --> 01:09:41,759
Shh.

569
01:09:44,638 --> 01:09:48,699
Out there. Past them trees.

570
01:09:51,512 --> 01:09:53,707
You see it?

571
01:09:57,751 --> 01:10:00,549
l see you.

572
01:10:08,696 --> 01:10:11,221
l see it.

573
01:10:11,332 --> 01:10:13,892
Yeah.

574
01:10:15,736 --> 01:10:17,931
l see it.

575
01:10:19,807 --> 01:10:22,708
You know, we can
get this thing, Mac.

576
01:10:22,810 --> 01:10:25,677
You work your way
down there toward him.

577
01:10:25,779 --> 01:10:28,771
l'm gonna loop around, get in back
of him, flush him toward you.

578
01:10:28,882 --> 01:10:31,817
<i>When</i> / <i>flush that son of a bitch,</i>
<i>you nail him.</i>

579
01:10:31,919 --> 01:10:34,012
l got a score to settle.

580
01:10:34,121 --> 01:10:37,352
We both got scores to settle.

583
01:12:00,741 --> 01:12:04,438
<i>-Vamos!</i>

584
01:12:04,545 --> 01:12:08,276
<i>Vamos!</i> Come on!
Quickly! Hurry up.

585
01:12:08,382 --> 01:12:10,577
- Don't. Leave it.

586
01:12:10,684 --> 01:12:14,882
lt didn't kill you because you
weren't armed. No sport.

587
01:12:46,153 --> 01:12:48,121
Mac.

588
01:12:48,222 --> 01:12:50,349
Mac.

589
01:12:57,731 --> 01:13:00,632
<i>- Anytime.</i>

594
01:14:15,275 --> 01:14:18,244
Let's go.

595
01:14:28,121 --> 01:14:30,282
Billy!

596
01:14:30,390 --> 01:14:33,325
<i>Billy, let's go!</i>

599
01:14:58,685 --> 01:15:01,518
Give me the weapon!

606
01:15:56,443 --> 01:15:59,435
- No!
- Run!

607
01:15:59,546 --> 01:16:01,514
Run!

608
01:16:05,619 --> 01:16:09,988
Run!

609
01:16:10,090 --> 01:16:14,959
<i>- Go! Get to the chopper!</i>

611
01:16:58,905 --> 01:17:01,567
Oh, shit!

616
01:20:50,637 --> 01:20:53,401
He couldn't see me.

639
01:35:40,860 --> 01:35:44,626
You're one ugly motherfucker.

642
01:36:02,781 --> 01:36:04,283
Bad idea.

643
01:36:04,283 --> 01:36:05,375
Bad idea.

648
01:38:03,035 --> 01:38:06,368
Come on. Come on.

649
01:38:06,472 --> 01:38:08,838
Do it. Do it!

651
01:38:11,443 --> 01:38:15,311
<i>Come on. Come on.</i>
<i>Kill me.</i> /<i>'m here. Kill me!</i>

652
01:38:16,949 --> 01:38:18,917
/<i>'m here! Kill me!</i>

653
01:38:19,018 --> 01:38:21,179
Come on! Kill me! l'm here!

654
01:38:21,286 --> 01:38:25,120
<i>Come on! Do it now! Kill me!</i>

662
01:39:44,670 --> 01:39:47,434
What the hell are you?

663
01:39:51,110 --> 01:39:54,204
What the hell...

664
01:39:54,313 --> 01:39:57,840
<i>are you?</i>

670
01:40:59,511 --> 01:41:01,672
Holy shit.

671
01:41:01,780 --> 01:41:03,975
- Check the battery.
<i>- Okay.</i>

672
01:41:04,082 --> 01:41:06,050
- Did you get it?
<i>- Got it!</i>

673
01:41:10,856 --> 01:41:13,017
What the--

674
01:41:15,460 --> 01:41:18,691
<i>My God.</i>


