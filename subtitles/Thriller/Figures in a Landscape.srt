1
00:04:42,607 --> 00:04:44,757
Come on, boy- Come on, now!

2
00:05:06,367 --> 00:05:08,164
Town boy, aren't you?

3
00:05:08,247 --> 00:05:11,000
Brought up on a bloody bottle,
I suppose-

4
00:05:15,327 --> 00:05:17,397
Everything out of a packet-

5
00:05:18,967 --> 00:05:21,606
Never saw a cow till you got out here-

6
00:05:35,087 --> 00:05:37,885
Want me to drag you behind me
in a bucket?

7
00:05:38,287 --> 00:05:42,724
When I was your age,
I used to do 22 miles before breakfast-

8
00:05:46,487 --> 00:05:47,966
You stay here-

9
00:05:48,287 --> 00:05:51,404
I'll get up there
and I'll come back with a bus-

10
00:05:51,567 --> 00:05:54,240
With all those women you had,
I suppose!

11
00:05:55,127 --> 00:05:58,483
All those dirty doorways-
Come on, Pocket Billiards!

12
00:06:04,047 --> 00:06:05,878
Want me to carry you?

13
00:06:06,567 --> 00:06:09,240
I must be old enough to be your father-

14
00:06:10,327 --> 00:06:13,637
You want to rest, don't you?
Want a bloody mattress?

15
00:06:16,567 --> 00:06:18,080
Come on, get your hands up-

16
00:06:18,167 --> 00:06:20,965
I can't get at them-
Come on, get them up-

17
00:06:43,727 --> 00:06:45,718
We've got to keep going-

18
00:06:53,207 --> 00:06:55,801
If I need your help, I'll ask for it-

19
00:06:57,727 --> 00:06:59,080
Hey, boy... -

20
00:07:01,287 --> 00:07:03,926
there's fresh water on the other side-

21
00:07:04,807 --> 00:07:06,206
- How do you know?
- Smelled it-

22
00:07:06,287 --> 00:07:10,075
They didn't teach you much
in that school of yours, did they?

23
00:07:16,607 --> 00:07:18,837
How far do you think they are?

24
00:07:20,647 --> 00:07:22,922
Ten years, the way you're going-

25
00:07:27,047 --> 00:07:29,356
Sorry I forgot your toothpaste-

26
00:09:04,167 --> 00:09:05,885
Not too much, boy-

27
00:10:07,807 --> 00:10:10,002
You've got to help me, boy-

28
00:10:14,407 --> 00:10:15,522
No-

29
00:10:34,007 --> 00:10:35,520
All right, then-

30
00:10:37,247 --> 00:10:38,600
Watch!

31
00:11:42,527 --> 00:11:44,438
He didn't have a knife-

32
00:11:51,447 --> 00:11:55,725
I shouldn't have had to do that
all by myself- That wasn't right-

33
00:12:00,767 --> 00:12:03,406
Wouldn't have been so messy
if you'd helped me-

34
00:12:03,487 --> 00:12:06,240
Hate a messy killing- That wasn't fair!

35
00:12:09,327 --> 00:12:13,923
What are you crying for,
you sentimental, useless, bloody idiot?

36
00:12:14,207 --> 00:12:16,198
Bloody hypocrite!

37
00:12:17,287 --> 00:12:19,562
I was the one who had to do it!

38
00:13:32,167 --> 00:13:35,364
He'd have died in his bloody bed
before Christmas-

39
00:13:38,847 --> 00:13:41,361
If you aren't going to help me,
what did you come for?

40
00:13:41,447 --> 00:13:44,007
Did you think this was going to be
a Boy Scout picnic?

41
00:13:49,287 --> 00:13:51,517
Do you think they'll find him?

42
00:13:56,847 --> 00:13:58,519
Got to get higher-

43
00:14:06,127 --> 00:14:07,799
Got to get higher-

44
00:14:13,047 --> 00:14:16,198
The sensible thing
is to find different clothes-

45
00:14:20,047 --> 00:14:23,881
The sensible thing is to find a village
and wait for the dark-

46
00:14:28,327 --> 00:14:31,444
Do you think
they'll try and take us back alive?

47
00:14:40,887 --> 00:14:43,845
Don't you want to stop
and polish your boots?

48
00:15:19,487 --> 00:15:21,557
I don't like it down there-

49
00:15:23,447 --> 00:15:25,165
You can't go around-

50
00:15:27,247 --> 00:15:28,726
It's too open-

51
00:15:31,167 --> 00:15:34,842
Let's just walk down the road-
They'll think we're workers-

52
00:16:00,807 --> 00:16:02,399
Bastard!

53
00:16:06,807 --> 00:16:08,160
Bastards!

54
00:18:37,127 --> 00:18:38,606
I'll kill you!

55
00:18:40,687 --> 00:18:42,040
Kill you!

56
00:19:09,567 --> 00:19:11,159
Playing with us-

57
00:19:13,367 --> 00:19:15,164
Playing a game-

58
00:19:18,687 --> 00:19:21,645
He can't have heard
about your goatherd yet-

59
00:19:27,647 --> 00:19:29,080
Just waiting-

60
00:19:35,367 --> 00:19:37,835
I didn't know he could do that-

61
00:19:39,407 --> 00:19:41,557
I didn't know anybody could-

62
00:19:44,607 --> 00:19:46,837
He could have taken my head off-

63
00:19:47,767 --> 00:19:50,486
I could have been running around
like a bloody chicken... -

64
00:19:50,567 --> 00:19:53,923
with my head bouncing
upon the ground in front of me-

65
00:20:09,927 --> 00:20:12,805
I suppose he's calling them up
on his radio-

66
00:20:16,327 --> 00:20:19,285
- What can we do, Ansell?
- Not much-

67
00:20:20,647 --> 00:20:23,161
Wait for the dark, and then go fast-

68
00:20:24,247 --> 00:20:25,600
Very fast-

69
00:20:42,487 --> 00:20:46,480
If I could've got my bloody hands free,
I'd have pulled him right in on top of me-

70
00:20:46,847 --> 00:20:49,122
I'd have rolled him right over-

71
00:20:52,407 --> 00:20:56,241
Jumped up on those bloody skids
and I'd have ploughed him in-

72
00:20:58,007 --> 00:21:00,475
- I might've tripped over it-
- What?

73
00:21:01,247 --> 00:21:02,566
My head-

74
00:21:03,567 --> 00:21:05,364
I could've kicked it-

75
00:21:13,367 --> 00:21:16,120
I could've scored a bloody goal with it-

76
00:21:19,567 --> 00:21:23,242
I'd like to kick my bloody head
right up into his Perspex-

77
00:22:29,567 --> 00:22:33,401
Didn't that old man have
anything on him at all? Not anything?

78
00:22:37,527 --> 00:22:41,520
If you stay here, I'll go and get a knife
and come straight back-

79
00:22:41,607 --> 00:22:43,996
I'll bury that bastard pilot yet-

80
00:22:45,927 --> 00:22:48,839
Let me go alone- Please-

81
00:22:58,207 --> 00:23:02,564
Ansell, keep behind me- Twenty paces-

82
00:24:14,127 --> 00:24:15,355
Ansell-

83
00:24:31,127 --> 00:24:33,516
Quiet, old fellow- Quiet-

84
00:24:48,287 --> 00:24:49,515
Quiet-

85
00:25:43,007 --> 00:25:44,838
What happened to you?

86
00:25:47,287 --> 00:25:48,561
Listen-

87
00:25:49,167 --> 00:25:52,523
He had a picture of his grandchildren
and a piece of dried tobacco-

88
00:25:52,607 --> 00:25:55,883
If you don't help me this time,
I'll kill you, too-

89
00:27:32,527 --> 00:27:34,006
That's enough-

90
00:27:34,607 --> 00:27:36,404
Watch while I drink-

91
00:29:22,007 --> 00:29:23,326
Get down!

92
00:29:25,927 --> 00:29:28,521
Got his best suit on, hasn't he?

93
00:30:29,087 --> 00:30:30,600
Don't hurt her-

94
00:30:50,727 --> 00:30:52,957
Don't touch anything in there-

95
00:32:10,327 --> 00:32:11,555
You pig!

96
00:32:31,047 --> 00:32:34,323
You stupid, half-witted,
half-brained, idiotic pig!

97
00:32:35,087 --> 00:32:37,555
I wish he had taken your head off-

98
00:32:38,087 --> 00:32:40,043
Stupid!

99
00:32:40,807 --> 00:32:42,365
Stupid idiot-

100
00:32:43,527 --> 00:32:45,882
Crazy, murdering pig-

101
00:32:47,687 --> 00:32:49,518
He should have taken your head off... -

102
00:32:49,607 --> 00:32:52,485
and put it into a bucket of vinegar
and sent it to a museum!

103
00:32:53,887 --> 00:32:55,400
What a fool!

104
00:32:57,487 --> 00:32:59,682
Do you have any plan at all?

105
00:33:17,807 --> 00:33:21,641
She'd never have done anything
if you'd left that bread alone-

106
00:33:33,727 --> 00:33:37,879
We're both going to be caught
tomorrow- You do know that, don't you?

107
00:33:43,407 --> 00:33:46,843
All right, then,
you can damn well give me half of it!

108
00:34:38,047 --> 00:34:39,526
Hang on-

109
00:34:41,647 --> 00:34:43,319
Split up the food-

110
00:34:46,247 --> 00:34:47,362
Why?

111
00:34:48,287 --> 00:34:51,085
Only needed you
till my hands were untied-

112
00:35:02,727 --> 00:35:04,399
You're quite sure?

113
00:35:06,127 --> 00:35:08,721
- You can have the razor-
- I don't want the razor-

114
00:35:08,807 --> 00:35:11,401
- I'm having the gun-
- I don't want the gun-

115
00:35:11,567 --> 00:35:14,923
You can have half the tins, the blanket... -

116
00:35:15,167 --> 00:35:17,840
suitcase, and the water-

117
00:35:19,087 --> 00:35:21,078
Plenty of water up there-

118
00:35:34,247 --> 00:35:36,044
Pockets big enough?

119
00:35:38,487 --> 00:35:40,876
Hey! One for the road?

120
00:35:50,527 --> 00:35:53,678
How are you going to open your tins,
country boy?

121
00:35:58,407 --> 00:35:59,635
Bastard!

122
00:36:01,607 --> 00:36:03,006
Bastard tin-

123
00:36:04,527 --> 00:36:06,438
Blooming country boy-

124
00:36:13,407 --> 00:36:15,318
All right, smarty-

125
00:36:17,447 --> 00:36:19,358
How do I open it, then?

126
00:36:34,527 --> 00:36:37,917
- What do you want for that?
- I want to come with you-

127
00:36:40,127 --> 00:36:43,437
- I give the orders, then-
- Yes-

128
00:36:44,287 --> 00:36:45,766
Give it to me-

129
00:37:13,727 --> 00:37:14,842
Hey... -

130
00:37:17,727 --> 00:37:19,206
you want some?

131
00:37:24,527 --> 00:37:25,642
Hey!

132
00:38:05,807 --> 00:38:06,956
Here-

133
00:38:09,007 --> 00:38:11,282
Always keep neat and tidy, boy-

134
00:38:19,367 --> 00:38:22,518
Be careful of your tender little face,
won't you?

135
00:38:26,527 --> 00:38:28,677
And don't forget down below-

136
00:38:29,407 --> 00:38:30,601
What?

137
00:38:30,847 --> 00:38:33,281
Don't want to get the rot, do you?

138
00:38:35,287 --> 00:38:38,324
- Are you serious?
- I've already done mine-

139
00:38:42,567 --> 00:38:46,196
You've got something worth saving
down there, haven't you?

140
00:38:57,247 --> 00:39:00,159
We'll have to eat at night
and drink in the middle of the day-

141
00:39:00,247 --> 00:39:01,282
Right-

142
00:39:01,367 --> 00:39:04,723
And any time you've got something
to say to me, I shall listen to you-

143
00:39:04,807 --> 00:39:06,126
Thank you-

144
00:39:12,367 --> 00:39:14,756
Sometimes I'm very stupid-

145
00:39:15,327 --> 00:39:16,999
I am- I know that-

146
00:39:20,207 --> 00:39:22,198
My wife has told me that-

147
00:39:22,927 --> 00:39:24,155
Oh, yes?

148
00:39:25,487 --> 00:39:26,806
Yes-

149
00:39:40,007 --> 00:39:42,885
- You see the main ridge?
- Yes-

150
00:39:42,967 --> 00:39:45,162
- You see that bit of rock like a fist?
- Yes-

151
00:39:45,247 --> 00:39:47,715
That's our objective today- Right?

152
00:40:27,127 --> 00:40:29,004
What's the matter now?

153
00:40:31,767 --> 00:40:34,565
I'm just following you,
like you told me to-

154
00:40:34,687 --> 00:40:36,757
If you kill me, there's no point-

155
00:40:36,847 --> 00:40:39,645
I'd just be lying here like that old man-

156
00:40:40,567 --> 00:40:44,640
And all you'll have is twice as many tins,
a suitcase, and a razor-

157
00:40:50,807 --> 00:40:51,922
Look-

158
00:40:54,287 --> 00:40:56,403
Later on, I might consider it-

159
00:40:56,487 --> 00:40:59,445
Consider a parting, a going of ways,
I might-

160
00:40:59,927 --> 00:41:01,360
But not now-

161
00:41:01,887 --> 00:41:03,605
Because I'm lonely-

162
00:41:07,967 --> 00:41:10,401
I'll consider it in the mountains-

163
00:41:13,367 --> 00:41:15,517
I don't think that thing will fire anyway-

164
00:41:16,767 --> 00:41:19,235
Funny if it had gone off backwards!

165
00:42:25,847 --> 00:42:27,246
You married?

166
00:42:29,327 --> 00:42:31,966
What chance did I have to get married?

167
00:42:34,087 --> 00:42:36,681
Think I wanted to get into this mess?

168
00:42:42,087 --> 00:42:44,601
A boy of your age ought to be married-

169
00:42:56,127 --> 00:42:58,595
Responsibility,
that's what you boys need-

170
00:42:58,687 --> 00:43:00,564
- That's all changed-
- What's all changed?

171
00:43:00,647 --> 00:43:03,161
They don't want to get married now,
not since the pill-

172
00:43:03,247 --> 00:43:05,477
You can have who you like,
you've only got to ask-

173
00:43:05,567 --> 00:43:09,162
Models, wives, nurses,
secretaries, physiotherapists-

174
00:43:19,607 --> 00:43:21,199
Think he saw us?

175
00:43:36,407 --> 00:43:40,719
What he could be doing, you see-
Go up ahead, keep there-

176
00:43:42,647 --> 00:43:44,285
He's coming back!

177
00:44:24,407 --> 00:44:25,601
Go!

178
00:44:38,527 --> 00:44:40,085
Right! Go!

179
00:44:56,487 --> 00:44:58,557
Anybody can have a war now-

180
00:44:59,447 --> 00:45:02,996
I mean, you just get a bit of equipment
and you start, don't you?

181
00:45:03,207 --> 00:45:05,243
I bet he fancies himself-

182
00:45:07,567 --> 00:45:10,718
Probably went to one of those schools-
One of those flying schools-

183
00:45:30,927 --> 00:45:33,919
What do you think you can hit
with that thing?

184
00:45:40,407 --> 00:45:41,556
Not too much-

185
00:45:41,647 --> 00:45:45,401
Petrol tank is behind that cabin-
You've got to hit it from the side-

186
00:45:46,367 --> 00:45:49,120
- Yes-
- You only get one chance-

187
00:45:49,847 --> 00:45:51,439
He'd be off like a bloody bat-

188
00:45:51,527 --> 00:45:54,803
But he doesn't know you've got it!
See what I mean?

189
00:46:02,087 --> 00:46:03,600
My wife... -

190
00:46:05,207 --> 00:46:07,641
she had to have her face built up-

191
00:46:08,527 --> 00:46:09,926
Plastic jaw-

192
00:46:11,247 --> 00:46:13,715
An Alsatian dog sprang
right across the bar at her... -

193
00:46:13,807 --> 00:46:15,877
just as we were saying good night-

194
00:46:16,087 --> 00:46:17,884
I'm sorry about that-

195
00:46:18,287 --> 00:46:19,879
It was terrible-

196
00:46:22,647 --> 00:46:24,444
Did you sue anybody?

197
00:46:25,407 --> 00:46:28,160
Couldn't- It was her uncle-

198
00:46:29,047 --> 00:46:32,323
Her uncle was the publican-
Couldn't sue her uncle-

199
00:46:34,367 --> 00:46:37,120
I bet he's no bloody good on the ground-

200
00:46:39,727 --> 00:46:41,524
Listen- This is what I'm going to do-

201
00:46:41,607 --> 00:46:43,723
I'm going to twist, run, shout, act it up-

202
00:46:43,807 --> 00:46:45,160
I'm going to go for that ridge-

203
00:46:45,247 --> 00:46:47,283
He'll come over me,
playing his little game-

204
00:46:47,367 --> 00:46:50,916
Then I'll bring him in to you, close-
Now, that's my plan- Understand?

205
00:46:51,007 --> 00:46:54,158
Only go for the petrol tank-
That's the only important thing-

206
00:46:54,247 --> 00:46:55,566
Stay here-

207
00:48:22,287 --> 00:48:24,357
My leg!

208
00:49:14,247 --> 00:49:17,956
I was lined right up on him!
I was lined... -

209
00:49:18,047 --> 00:49:20,197
right up on his bloody head!

210
00:49:21,567 --> 00:49:22,602
Hey, boy-

211
00:49:24,807 --> 00:49:26,035
In the last minute...

212
00:49:26,127 --> 00:49:29,597
- Off like a bloody bastard-
- What the hell do you mean?

213
00:49:30,487 --> 00:49:33,399
That's why I missed him-
I won't miss him next time-

214
00:49:33,487 --> 00:49:37,036
I'd never miss him again
if I got another chance, boy-

215
00:49:37,447 --> 00:49:38,960
What about this?

216
00:49:39,087 --> 00:49:40,998
Bloody Christmas present-

217
00:49:41,087 --> 00:49:42,600
If I'd had this... -

218
00:49:42,687 --> 00:49:46,362
I'd have wiped that bloody smile
right off his greasy face!

219
00:49:46,687 --> 00:49:48,598
He was laughing at you, boy-

220
00:49:56,407 --> 00:49:58,557
I hope this bastard is his brother-

221
00:49:58,647 --> 00:50:02,481
But it was your job to get that tank,
not to worry about me, you stupid idiot!

222
00:50:02,567 --> 00:50:05,684
No! He had something
in his hand, you see-

223
00:50:05,887 --> 00:50:08,879
Something in his pocket-
Might've gone off, boy-

224
00:50:09,047 --> 00:50:12,801
Might've been a bloody bomb-
Pity I missed him-

225
00:50:20,327 --> 00:50:22,238
4:22-

226
00:50:22,447 --> 00:50:24,881
July 17-

227
00:50:24,967 --> 00:50:28,039
It's a good watch, boy- It's a Swiss-

228
00:50:34,287 --> 00:50:36,596
Come on, boy-

229
00:50:40,967 --> 00:50:44,118
If I had hit that tank... -

230
00:50:44,447 --> 00:50:47,996
if I had, you might have gone up, too-

231
00:50:54,047 --> 00:50:56,402
We didn't think of that, did we?

232
00:50:57,767 --> 00:51:00,281
I'll tell you what, though, Mac-

233
00:51:00,367 --> 00:51:02,517
There'll be no more games
from now on-

234
00:51:02,607 --> 00:51:04,916
I'll tell you something, boy-

235
00:51:05,007 --> 00:51:08,363
He's not going to come in so close,
now I've got this-

236
00:51:52,367 --> 00:51:54,801
Maybe he's gone home for a coffin-

237
00:52:03,527 --> 00:52:05,358
Like to have a smoke?

238
00:52:06,847 --> 00:52:08,997
How many did he have on him?

239
00:52:09,607 --> 00:52:10,756
Three-

240
00:52:12,407 --> 00:52:13,999
Any photographs?

241
00:52:14,407 --> 00:52:16,557
Yeah, a picture of his girl-

242
00:52:17,047 --> 00:52:18,366
No money-

243
00:52:19,047 --> 00:52:21,277
Got his lighter- And a watch-

244
00:52:22,127 --> 00:52:23,958
I don't want to smoke-
You can have them-

245
00:52:24,047 --> 00:52:26,845
I'm not having one if you aren't, Ansell-

246
00:52:28,887 --> 00:52:30,206
All right-

247
00:52:46,447 --> 00:52:49,245
You can have this other one
in the morning-

248
00:52:51,207 --> 00:52:53,277
My hands are still shaking-

249
00:53:08,967 --> 00:53:10,719
It's good, isn't it?

250
00:53:11,407 --> 00:53:12,522
Yes-

251
00:53:18,807 --> 00:53:20,684
What time is it, then?

252
00:53:34,087 --> 00:53:35,759
It's not working-

253
00:53:41,807 --> 00:53:45,197
You know what we'll have to do?
We'll have to put these tins in order-

254
00:53:45,287 --> 00:53:47,517
- Why?
- So we know what they are in the dark-

255
00:53:47,607 --> 00:53:49,677
We have to know that-
We must balance our diet-

256
00:53:49,767 --> 00:53:53,043
Look, I've told you, I know what they are-
Come on-

257
00:53:53,567 --> 00:53:54,761
You do?

258
00:53:55,487 --> 00:53:57,239
There are only three kinds-

259
00:53:57,327 --> 00:53:59,079
Big ones are beans-

260
00:53:59,167 --> 00:54:02,603
Square ones are beef-
Medium, condensed milk-

261
00:54:03,087 --> 00:54:04,884
How do you know that?

262
00:54:07,087 --> 00:54:08,600
I smelled them-

263
00:54:09,807 --> 00:54:12,879
- I don't believe you-
- Oh, yes-

264
00:54:12,967 --> 00:54:15,435
I used to work in Fortnum & Mason,
you know-

265
00:54:16,127 --> 00:54:17,924
Best store in London-

266
00:54:18,247 --> 00:54:20,886
Only did it for the experience, of course-

267
00:54:21,087 --> 00:54:23,123
And what I could pick up-

268
00:54:23,207 --> 00:54:25,402
- What you could pick up?
- Yes-

269
00:54:26,247 --> 00:54:28,636
Used to get a lot of Norwegian giris
in there... -

270
00:54:28,727 --> 00:54:30,126
and Swedish-

271
00:54:31,127 --> 00:54:33,083
I got this Australian girl once-

272
00:54:33,167 --> 00:54:35,920
Took me to the Ritz- Paid for everything-

273
00:54:36,527 --> 00:54:38,279
Father's a composer-

274
00:54:39,207 --> 00:54:41,562
Lovely big baths they've got at the Ritz-

275
00:54:41,687 --> 00:54:43,837
You can do anything in them-

276
00:54:44,407 --> 00:54:47,080
I offered to pay- She wouldn't have it-

277
00:54:48,287 --> 00:54:49,959
Got any daughters?

278
00:54:51,687 --> 00:54:54,326
I'll bloody well keep my daughters
away from you-

279
00:54:54,407 --> 00:54:55,476
Why?

280
00:54:56,047 --> 00:54:58,117
I don't hold with all this-

281
00:54:59,127 --> 00:55:00,606
With all what?

282
00:55:01,567 --> 00:55:03,444
Hold with all what, Mac?

283
00:55:03,527 --> 00:55:05,518
You give me that opener-

284
00:55:09,887 --> 00:55:13,038
You say that these medium-size
are condensed milk?

285
00:55:13,167 --> 00:55:15,237
I've told you that already-

286
00:55:21,127 --> 00:55:22,242
Here-

287
00:55:37,527 --> 00:55:39,483
Quite right, aren't you?

288
00:55:42,327 --> 00:55:45,922
Why do you want to keep
your daughters away from me, Mac?

289
00:55:46,087 --> 00:55:48,043
I'm a very good teacher-

290
00:55:49,967 --> 00:55:51,685
How many are there?

291
00:55:53,967 --> 00:55:55,036
Two-

292
00:56:00,847 --> 00:56:03,407
They could do a lot worse, you know-

293
00:56:16,407 --> 00:56:20,320
You must have given that bastard pilot
such a shock-

294
00:56:37,087 --> 00:56:38,361
Ansell-

295
00:56:40,607 --> 00:56:44,122
I'm not having you mess about
with any of my daughters-

296
00:57:22,167 --> 00:57:24,397
Not flying so low now, is he?

297
00:57:33,847 --> 00:57:36,645
I wish to hell he didn't know I had this-

298
00:57:37,567 --> 00:57:39,080
He's all by himself-

299
00:57:39,207 --> 00:57:43,041
Yes, and he wants us all for himself,
too, I'll tell you that-

300
00:58:10,647 --> 00:58:11,762
Mac-

301
00:58:14,127 --> 00:58:16,083
What's between us and the mountains?

302
00:58:17,647 --> 00:58:18,636
Nothing-

303
00:58:18,767 --> 00:58:20,758
What do you mean, nothing?

304
00:58:21,087 --> 00:58:22,361
Villages-

305
00:58:23,367 --> 00:58:25,358
No soldiers? No patrols?

306
00:58:25,887 --> 00:58:27,366
Border patrol-

307
00:58:29,207 --> 00:58:31,118
You don't know, do you?

308
00:58:32,727 --> 00:58:33,762
No-

309
00:58:33,967 --> 00:58:36,959
You don't know anything about it,
do you, Mac?

310
00:58:37,447 --> 00:58:40,757
I know you're all right
once you get up in the snow-

311
00:58:42,647 --> 00:58:44,365
Who's done it, Mac?

312
00:58:44,767 --> 00:58:46,803
Who's ever done it before?

313
00:59:06,687 --> 00:59:10,805
Why don't we just dig ourselves
a couple of graves and go to sleep?

314
00:59:22,367 --> 00:59:25,564
Better cross over there
and get up the other side-

315
00:59:33,687 --> 00:59:34,756
What?

316
00:59:34,847 --> 00:59:36,200
Oh, Christ!

317
00:59:46,447 --> 00:59:48,483
It's not funny, you know!

318
00:59:55,927 --> 00:59:56,962
Oh, boy-

319
01:00:03,727 --> 01:00:06,605
I've got to keep my shirttails out of this-

320
01:00:39,927 --> 01:00:43,044
I know you're right,
but I hate that low ground-

321
01:00:43,847 --> 01:00:46,486
I know, Mac, but we've got no choice-

322
01:00:47,047 --> 01:00:48,560
Let's go fast-

323
01:00:52,607 --> 01:00:54,643
I hope we're not too sore-

324
01:00:56,247 --> 01:00:59,000
You were right
about that condensed milk-

325
01:02:09,687 --> 01:02:11,996
He's a bloody field marshal now-

326
01:02:13,047 --> 01:02:14,560
I lost my hat-

327
01:02:28,287 --> 01:02:30,323
We'll never get through them,
not both of us-

328
01:02:30,407 --> 01:02:31,840
Want to split?

329
01:02:32,887 --> 01:02:36,516
We'd better get down here and lie up-

330
01:02:40,407 --> 01:02:41,635
We've got to-

331
01:02:41,847 --> 01:02:44,202
- I can't-
- We won't find a better...

332
01:02:44,287 --> 01:02:47,563
I can't get down there
and let them walk right over me-

333
01:02:47,647 --> 01:02:48,875
Come on!

334
01:03:30,727 --> 01:03:32,399
Mac, get in there-

335
01:04:44,767 --> 01:04:47,520
Last time we took your advice,
we got bayonets up our arse-

336
01:04:47,607 --> 01:04:49,325
Whose fault's that?

337
01:04:51,207 --> 01:04:55,086
If that black skeleton comes in over
the top, he'll drop mustard gas on us!

338
01:04:55,167 --> 01:04:56,725
What would you suggest?

339
01:04:56,807 --> 01:04:58,479
Why should I have to suggest anything?

340
01:04:58,567 --> 01:05:00,364
You're the one
who makes the suggestions-

341
01:05:00,447 --> 01:05:02,278
You're the one
who goes in those stores... -

342
01:05:02,367 --> 01:05:03,800
making suggestions to young giris-

343
01:05:03,887 --> 01:05:06,447
You've got no right
to take advantage of them like that-

344
01:05:06,527 --> 01:05:08,085
I think you're stark, raving mad-

345
01:05:08,167 --> 01:05:10,840
Bloody sex maniac!
You ought to be in court-

346
01:05:10,927 --> 01:05:13,487
One of those fathers
ought to have locked you up-

347
01:05:13,567 --> 01:05:16,320
- I think you ought to be analysed-
- What?

348
01:05:27,407 --> 01:05:30,558
They should never have invented
that bloody pill-

349
01:05:31,087 --> 01:05:32,236
Right-

350
01:07:35,967 --> 01:07:37,639
- Are you hurt?
- No-

351
01:07:47,607 --> 01:07:50,917
Well, those idiots saw us come in here,
didn't they?

352
01:10:15,247 --> 01:10:18,717
Come on, boy! Come on!
Get yourself through the middle-

353
01:10:18,847 --> 01:10:20,246
Come on!

354
01:10:28,847 --> 01:10:30,644
Through here, Ansell!

355
01:11:14,767 --> 01:11:18,476
- Water!
- Wait! Wait till he can't see us-

356
01:11:31,327 --> 01:11:33,522
Hey, don't wait for me!

357
01:11:38,087 --> 01:11:42,603
Look what I've got-
I'll put it under his bastard pillow, boy!

358
01:11:42,727 --> 01:11:45,161
One of your little school friends-

359
01:11:46,127 --> 01:11:48,402
Be quiet, for God's sake-
Shut up, you maniac!

360
01:11:48,487 --> 01:11:52,844
If I find that bastard pilot...
Hey, you bastard!

361
01:11:55,767 --> 01:11:58,964
Ansell, that pilot's trying
to burn up everything-

362
01:12:03,567 --> 01:12:05,922
I can't take him up in the snow-

363
01:12:06,327 --> 01:12:11,276
I can't take him up there, boy-
Too cold up there- Go on, then- Go on!

364
01:12:12,087 --> 01:12:16,763
Goodbye- He's gone-
Goodbye, you little sweetheart-

365
01:13:08,687 --> 01:13:12,043
Where's your gun?

366
01:13:18,447 --> 01:13:21,564
Can you hear me?
Get that mud out of your ears-

367
01:13:23,007 --> 01:13:25,237
Get that muck out of your ears!

368
01:13:25,447 --> 01:13:26,960
Can you hear me now?

369
01:13:27,047 --> 01:13:30,164
- Yeah, I hear you-
- Where's your bloody gun?

370
01:13:32,527 --> 01:13:34,404
It was the bloody farmers, wasn't it?

371
01:13:41,447 --> 01:13:44,996
The farmers saved us, didn't they?
The poor old farmers-

372
01:13:45,727 --> 01:13:47,285
My face hurts-

373
01:13:48,327 --> 01:13:49,476
Yes... -

374
01:13:50,807 --> 01:13:52,718
well, you better... -

375
01:13:53,407 --> 01:13:55,125
put some mud on it-

376
01:13:55,527 --> 01:13:57,995
Then the blisters can't...

377
01:13:58,367 --> 01:13:59,356
See?

378
01:14:00,207 --> 01:14:01,879
Put that mud on it!

379
01:15:33,047 --> 01:15:34,799
You see him?

380
01:15:36,527 --> 01:15:38,119
This time... -

381
01:15:39,007 --> 01:15:40,759
you've got to do it-

382
01:15:42,087 --> 01:15:44,726
I'll cover you with the gun-

383
01:15:55,887 --> 01:15:57,206
All right?

384
01:16:01,967 --> 01:16:03,400
Go on, boy!

385
01:18:02,807 --> 01:18:04,957
That was your first-

386
01:18:20,607 --> 01:18:21,722
Here-

387
01:18:29,887 --> 01:18:32,117
It's here, boy, I tell you that-

388
01:18:56,327 --> 01:18:58,363
What we need is food, Mac-

389
01:19:04,327 --> 01:19:06,124
It's here, I know it-

390
01:20:32,207 --> 01:20:34,277
Unbolt the door!

391
01:20:35,247 --> 01:20:37,681
No bloody order- No bloody discipline-

392
01:20:37,767 --> 01:20:40,281
They buggered it up for him, didn't they?

393
01:20:40,447 --> 01:20:42,119
Wish I'd got him-

394
01:20:42,927 --> 01:20:45,157
Want to go back?

395
01:20:46,767 --> 01:20:49,440
Well, I got a drink out of it, anyway-

396
01:20:51,007 --> 01:20:54,044
He's probably down there with that thing
at this very minute... -

397
01:20:54,127 --> 01:20:55,640
crying over it-

398
01:21:41,807 --> 01:21:43,365
I've got to sleep-

399
01:21:43,447 --> 01:21:47,201
This'll keep the bastards in the holes
they were born in-

400
01:21:50,047 --> 01:21:51,878
How far is the snow?

401
01:21:52,847 --> 01:21:56,203
There isn't any snow- Snow's all melted-

402
01:21:58,287 --> 01:22:01,199
Listen! I've got to sleep-

403
01:22:03,727 --> 01:22:05,877
Well, you can't sleep here-

404
01:22:07,527 --> 01:22:09,677
Come on, get up-

405
01:22:11,047 --> 01:22:12,685
Give us your arm-

406
01:22:55,727 --> 01:22:57,922
Wish I hadn't lost my hat-

407
01:23:31,407 --> 01:23:33,841
I can see the bones in my fingers-

408
01:23:39,847 --> 01:23:41,326
The tips are salty-

409
01:23:41,407 --> 01:23:42,760
Don't look-

410
01:23:44,487 --> 01:23:47,445
We smell like animals- Filthy!

411
01:23:49,607 --> 01:23:51,040
Christ Jesus-

412
01:23:52,527 --> 01:23:54,802
What have we done to ourselves?

413
01:23:56,767 --> 01:23:58,280
All right, boy-

414
01:24:00,327 --> 01:24:03,797
Not living- We're dead!

415
01:24:05,447 --> 01:24:07,722
We should never have tried it!

416
01:24:10,567 --> 01:24:12,478
We crawl along... -

417
01:24:13,367 --> 01:24:15,597
slice with razors... -

418
01:24:19,807 --> 01:24:22,116
blow our noses with our fingers-

419
01:24:22,807 --> 01:24:26,356
Tear with our fingers!
Wipe ourselves with our fingers!

420
01:24:26,447 --> 01:24:28,483
All right, boy, all right-

421
01:24:29,607 --> 01:24:33,122
Oh, God! Oh, Jesus!

422
01:24:33,927 --> 01:24:35,155
Oh, Christ!

423
01:24:38,687 --> 01:24:40,678
We're worse than animals!

424
01:24:43,687 --> 01:24:46,565
All right, boy, don't you worry-

425
01:25:02,447 --> 01:25:04,961
My wife is five years older than me-

426
01:25:10,407 --> 01:25:12,875
It's the face I always look for, Ansell-

427
01:25:12,967 --> 01:25:15,276
The face, and then the legs-

428
01:25:15,367 --> 01:25:18,325
She's got a pretty face,
but she hasn't got good legs-

429
01:25:18,407 --> 01:25:22,446
Varicose veins-
That's because of the children-

430
01:25:24,167 --> 01:25:27,955
She's always worrying about her legs,
she keeps shaving them-

431
01:25:30,007 --> 01:25:32,475
The children haven't got good legs-

432
01:25:33,447 --> 01:25:35,961
Not as pretty as their mother, either-

433
01:25:36,047 --> 01:25:39,198
But they'll do all right,
because they've got her nature-

434
01:25:45,287 --> 01:25:47,039
What happened was... -

435
01:25:48,327 --> 01:25:51,444
she was always going out
with American soldiers-

436
01:25:55,967 --> 01:25:59,164
I took my father's raincoat
and I put on my father's hat... -

437
01:25:59,247 --> 01:26:03,160
and I went to a public telephone box
when I knew she was home-

438
01:26:03,807 --> 01:26:07,641
I called her up,
put on an American accent... -

439
01:26:09,087 --> 01:26:11,123
I lowered my voice... -

440
01:26:12,007 --> 01:26:14,521
and I said, "Meet me in the park... -

441
01:26:14,607 --> 01:26:17,246
"3:30, Sunday afternoon... -

442
01:26:17,487 --> 01:26:19,284
"by the bandstand-"

443
01:26:23,527 --> 01:26:25,199
Well, it was raining-

444
01:26:26,567 --> 01:26:29,877
No band- But she came there-

445
01:26:36,927 --> 01:26:38,485
I was 16-

446
01:26:39,207 --> 01:26:40,959
She was 21-

447
01:26:41,967 --> 01:26:44,401
Came there in a white mackintosh-

448
01:26:49,967 --> 01:26:54,404
When she saw me
in my father's raincoat and hat... -

449
01:26:56,007 --> 01:26:58,077
she laughed till she cried-

450
01:27:02,127 --> 01:27:04,402
She went away-

451
01:27:05,087 --> 01:27:06,759
I kept after her-

452
01:27:09,127 --> 01:27:11,880
Then the Americans went away-

453
01:27:21,687 --> 01:27:23,166
I had to join up myself-

454
01:27:23,247 --> 01:27:26,125
And two years later,
I came home on leave... -

455
01:27:26,887 --> 01:27:29,003
and I was drinking a lot-

456
01:27:29,407 --> 01:27:32,046
And one night
she had to help me home-

457
01:27:41,287 --> 01:27:43,118
I had to go out East-

458
01:27:44,807 --> 01:27:47,002
She wrote to me... -

459
01:27:48,247 --> 01:27:49,680
twice a week-

460
01:27:49,967 --> 01:27:52,356
I was away for a long time-

461
01:27:59,007 --> 01:28:00,201
When... -

462
01:28:01,767 --> 01:28:03,166
she was... -

463
01:28:03,927 --> 01:28:05,076
thirty-seven... -

464
01:28:06,767 --> 01:28:08,519
yes, 37... -

465
01:28:09,367 --> 01:28:11,437
and I was...

466
01:28:14,927 --> 01:28:16,121
Well... -

467
01:28:16,607 --> 01:28:17,881
anyway... -

468
01:28:19,127 --> 01:28:21,243
we went back to the park... -

469
01:28:22,127 --> 01:28:24,880
on a Sunday night... -

470
01:28:28,207 --> 01:28:29,606
and... -

471
01:28:30,847 --> 01:28:32,997
me and my wife, she...

472
01:28:34,207 --> 01:28:36,038
Well, it was her idea-

473
01:28:39,607 --> 01:28:42,280
I had her... -

474
01:28:43,287 --> 01:28:44,800
underneath... -

475
01:28:45,527 --> 01:28:47,006
bandstand-

476
01:29:03,247 --> 01:29:05,886
How long have you been with your wife?

477
01:29:07,967 --> 01:29:09,241
Years-

478
01:29:10,287 --> 01:29:12,084
My children are all...

479
01:29:13,887 --> 01:29:15,957
Children are all...

480
01:29:18,247 --> 01:29:20,078
I've got old-

481
01:29:33,487 --> 01:29:35,159
Yes, I've got old-

482
01:29:36,887 --> 01:29:38,878
Come on, Pocket Billiards-

483
01:32:55,247 --> 01:32:56,600
You see it?

484
01:32:59,567 --> 01:33:00,636
Yes-

485
01:33:08,807 --> 01:33:10,559
I never believed it-

486
01:33:13,967 --> 01:33:15,366
Whose is it?

487
01:33:20,847 --> 01:33:22,519
How do you mean?

488
01:33:23,407 --> 01:33:25,682
I don't know whose it is-

489
01:33:26,287 --> 01:33:27,845
Never did know-

490
01:33:35,567 --> 01:33:37,797
Well, it's not theirs-

491
01:33:40,127 --> 01:33:41,401
Come on-

492
01:33:46,927 --> 01:33:49,043
- They won't shoot at us?
- No-

493
01:33:50,367 --> 01:33:52,244
It's not theirs-

494
01:33:52,967 --> 01:33:54,844
Come on, Mac!

495
01:34:15,527 --> 01:34:17,006
Come on, Mac!

496
01:34:28,687 --> 01:34:30,757
We've made it!

497
01:34:38,647 --> 01:34:39,921
Come on!

498
01:34:59,247 --> 01:35:01,477
Maybe I could hold them back-

499
01:35:02,807 --> 01:35:05,275
There's no need- Come on, Mac-

500
01:35:20,687 --> 01:35:24,600
Do you know what? In all that time... -

501
01:35:26,007 --> 01:35:27,565
all that war... -

502
01:35:28,647 --> 01:35:31,366
she waited for me-

503
01:35:31,807 --> 01:35:34,116
She never went out with anyone else-

504
01:35:34,887 --> 01:35:38,846
Get up here, Mac! It's all over-

505
01:35:41,007 --> 01:35:42,645
We're free!

506
01:36:03,887 --> 01:36:05,240
Come on!

507
01:36:12,487 --> 01:36:14,557
Get up here, Mac! Come on!

508
01:36:26,847 --> 01:36:29,156
The day I was free... -

509
01:36:30,167 --> 01:36:31,600
she knew it-

510
01:36:34,127 --> 01:36:35,765
Nobody told her-

511
01:36:36,847 --> 01:36:39,122
Thirty thousand miles away... -

512
01:36:40,607 --> 01:36:42,677
she telephoned her mother-

513
01:36:44,287 --> 01:36:45,925
Get up here, Mac!

514
01:36:47,567 --> 01:36:48,795
Come on!

515
01:36:51,567 --> 01:36:53,398
Childhood sweethearts-

516
01:37:48,727 --> 01:37:50,001
Thin air-

517
01:37:50,927 --> 01:37:53,122
Air's thin up here, you see-

518
01:38:01,767 --> 01:38:04,440
"Thank God," she said to her mother-

519
01:38:15,207 --> 01:38:18,597
Great, black bat-

520
01:38:32,527 --> 01:38:35,121
Leave it, Mac- You've got no chance-

521
01:38:37,727 --> 01:38:39,365
Hey!

522
01:38:50,927 --> 01:38:54,124
Mac, what about me?

523
01:38:54,687 --> 01:38:57,520
Come on, then-

524
01:38:58,607 --> 01:39:00,359
Bastard!

525
01:39:25,607 --> 01:39:27,677
Say your bloody prayers!

