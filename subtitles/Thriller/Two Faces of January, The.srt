1
00:04:57,005 --> 00:04:58,422
It's occupied.

2
00:06:47,156 --> 00:06:48,573
One thousand drachmas.

3
00:06:50,368 --> 00:06:51,868
Come on, seven hundred.

4
00:06:52,495 --> 00:06:53,954
Okay.

5
00:07:10,721 --> 00:07:12,139
Well done.

6
00:22:49,576 --> 00:22:51,786
He's old enough to be her father.

7
00:23:09,513 --> 00:23:12,723
- What has he done?
- He just has to get out of the country.

8
00:23:12,808 --> 00:23:14,266
Yes, but why?

9
00:23:27,406 --> 00:23:30,116
Can you bring the passports
to one of the islands?

10
00:23:30,867 --> 00:23:32,535
I don't know. How much trouble is he in?

11
00:23:32,619 --> 00:23:35,413
Niko, he'll pay us.
We can make a lot of money.

12
00:23:48,218 --> 00:23:51,011
Are you joking?
This guy hasn't got a clue.

13
00:23:51,972 --> 00:23:55,099
This is bullshit.
I'm not getting involved.

14
00:36:12,587 --> 00:36:16,339
Police are investigating the death

15
00:36:16,424 --> 00:36:20,969
of an American national
at the Grand Hotel in Athens.

16
00:36:23,848 --> 00:36:26,725
The man, in his 50s, was discovered

17
00:36:26,809 --> 00:36:30,020
by hotel staff yesterday morning.

18
00:41:58,766 --> 00:42:02,560
Police are yet to name any suspects,
but it is believed

19
00:42:02,645 --> 00:42:06,439
they are focussing
their search in Athens

20
00:42:06,524 --> 00:42:11,486
and the border crossings
with Yugoslavia and Albania.

21
00:46:40,422 --> 00:46:42,215
Stop! Stop it!

22
00:46:42,299 --> 00:46:44,383
Can't you see he's drunk?

23
00:46:56,063 --> 00:46:57,438
What's going on here?

24
00:46:57,731 --> 00:46:59,065
Nothing.

25
00:49:14,534 --> 00:49:16,661
How soon can you get
the passports to Ir�klion?

26
00:49:16,745 --> 00:49:18,162
Tomorrow morning.

27
00:49:18,246 --> 00:49:19,664
I'll see you at Caf� Astir at eight.

28
00:49:19,748 --> 00:49:21,040
I'll be there.

29
00:49:21,458 --> 00:49:23,542
You didn't tell me
he's wanted for murder.

30
00:49:25,879 --> 00:49:28,047
He's got plenty of money.

31
00:52:04,538 --> 00:52:05,955
I can't wait for you.

32
00:52:08,208 --> 00:52:09,583
Sorry!

33
00:52:10,210 --> 00:52:11,877
The girl doesn't feel well.

34
00:52:15,090 --> 00:52:17,591
How far is it to Ir�klion?

35
00:52:17,717 --> 00:52:19,718
Five kilometres.
Past the ruins at Knossos.

36
01:06:26,273 --> 01:06:27,941
Are you all right?

37
01:06:28,150 --> 01:06:29,776
I had too much to drink.

38
01:06:32,571 --> 01:06:34,489
You should be careful in this weather.

39
01:06:34,573 --> 01:06:36,408
You could slip and fall overboard.

40
01:06:36,492 --> 01:06:37,909
Okay, I'll be careful.

41
01:07:27,460 --> 01:07:28,877
Where are you from?

42
01:07:28,961 --> 01:07:31,004
- Crete, sir.
- Documents?

43
01:07:37,678 --> 01:07:39,345
Did you visit Knossos?

44
01:07:39,764 --> 01:07:41,306
No, why?

45
01:07:41,724 --> 01:07:44,893
We're looking for someone
seen at the ruins.

46
01:07:45,478 --> 01:07:47,479
Go and speak to my colleague, please.

47
01:08:27,478 --> 01:08:29,104
Could you go and speak
to my colleague, please?

48
01:08:30,147 --> 01:08:31,564
Just over there, please.

49
01:08:40,699 --> 01:08:42,117
Were you travelling alone in Crete?

50
01:08:42,201 --> 01:08:44,744
No, I was visiting my mother.

51
01:08:45,579 --> 01:08:47,038
Did you visit Knossos?

52
01:08:47,748 --> 01:08:49,791
Why would I go there?
I was born in Crete.

53
01:08:50,668 --> 01:08:52,085
Where do you live now?

54
01:08:52,461 --> 01:08:53,878
Athens.

55
01:08:53,963 --> 01:08:55,588
What's your occupation?

56
01:08:55,673 --> 01:08:57,090
Musician.

57
01:10:55,167 --> 01:10:59,504
Reports indicate the victim was robbed

58
01:10:59,588 --> 01:11:04,050
as no valuables
or identification were found on her.

59
01:11:12,309 --> 01:11:15,436
A man in his 20s,
believed to be a Greek national

60
01:11:15,521 --> 01:11:19,315
was seen leaving the site
by a group of students.

61
01:14:42,769 --> 01:14:44,270
Attention, please!

62
01:14:44,605 --> 01:14:48,107
Passengers for Flight 180 to Istanbul

63
01:14:48,233 --> 01:14:50,443
should proceed to the boarding gate.

64
01:25:23,952 --> 01:25:26,286
We lost him on Aynicilar Street.

65
01:25:26,830 --> 01:25:29,540
He must be heading for Rustam Square.

