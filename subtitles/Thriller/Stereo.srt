1
00:00:00,999 --> 00:00:09,463
STEREO

2
00:01:35,591 --> 00:01:40,719
Now that we have some insight,
into the concept of experiential space

3
00:01:40,819 --> 00:01:45,031
we may consider interaction among
the experiential space continua

4
00:01:45,131 --> 00:01:48,236
of a highly unique group of individuals.

5
00:01:49,282 --> 00:01:53,715
In general, a study of the varying
dimensions of human experience

6
00:01:53,815 --> 00:01:56,836
in the context of man in his society

7
00:01:56,936 --> 00:02:00,311
is known as human social cybernetics.

8
00:02:01,251 --> 00:02:05,196
In our experiment,
eight Category-A subjects

9
00:02:05,296 --> 00:02:07,790
underwent pattern brain surgery

10
00:02:07,890 --> 00:02:13,121
whose program was developed within the
academy's organic computer dialectic system.

11
00:02:14,545 --> 00:02:18,468
The object of surgery was
to extend by a process called

12
00:02:18,568 --> 00:02:20,512
biochemical induction.

13
00:02:20,612 --> 00:02:24,714
The natural, electro-chemical
network of the human brain.

14
00:02:25,747 --> 00:02:30,874
This extension would provide each
subject with telepathic capabilites.

15
00:02:32,111 --> 00:02:35,901
A telepathist is one who can
communicate with other minds

16
00:02:36,001 --> 00:02:39,782
by means which do not involve
perception by the senses.

17
00:02:40,708 --> 00:02:44,910
Thus, telepathy is a form
of extra-sensory perception

18
00:02:45,010 --> 00:02:46,708
or ESP.

19
00:02:47,357 --> 00:02:52,789
Our subjects were to be kept in isolation
at the institute for three months.

20
00:02:52,889 --> 00:02:56,705
Where they were to prepare for
their first meeting as a group.

21
00:02:56,805 --> 00:03:02,388
This meeting was to take place at the academy
sanitorium in the Ontario north woods.

22
00:04:51,974 --> 00:04:54,048
Telepathic dependency

23
00:04:54,817 --> 00:04:57,730
is an extreme form of psychic addiction.

24
00:04:58,869 --> 00:05:02,691
It is essentially an
electro-chemical addiction.

25
00:05:02,791 --> 00:05:07,931
The dependent, surrending the
autonomy of his own nervous system

26
00:05:08,031 --> 00:05:11,452
to that of the onject of his dependency.

27
00:05:11,552 --> 00:05:16,087
And altering his patterns of
electro-chemical discharge

28
00:05:16,187 --> 00:05:18,519
to mimic those of his object.

29
00:05:21,138 --> 00:05:27,164
When the object of dependency is telepathically
unavailable for long periods of time

30
00:05:28,084 --> 00:05:30,483
the new nervous patterns

31
00:05:30,583 --> 00:05:34,010
find themselves suddenly
without a constant source

32
00:05:34,110 --> 00:05:37,165
of electro-morphological reinforcement...

33
00:05:38,312 --> 00:05:42,007
and, severe psychic disorientation

34
00:05:42,544 --> 00:05:45,649
begins to manifest itself in the dependent.

35
00:05:48,342 --> 00:05:50,971
At its most intense level

36
00:05:52,331 --> 00:05:55,607
a frustrated dependency can result in the

37
00:05:55,707 --> 00:05:59,889
irreversible destruction
of critical brain tissue.

38
00:06:02,250 --> 00:06:06,448
In as much as the establishment of
a partial telepathic dependency

39
00:06:06,548 --> 00:06:09,344
of the subject upon his researcher

40
00:06:09,948 --> 00:06:12,277
is an unavoidable part

41
00:06:12,769 --> 00:06:17,202
of the parapsychological
experimental gestalt.

42
00:06:18,523 --> 00:06:23,443
The means to maintain this
dependency at a controllable level

43
00:06:23,543 --> 00:06:27,193
must be considered an organic element

44
00:06:27,496 --> 00:06:30,193
forming that experimental gestalt.

45
00:06:34,679 --> 00:06:39,919
During the institute phase of
the induced telepathy series...

46
00:06:41,334 --> 00:06:42,386
our subject

47
00:06:42,486 --> 00:06:47,755
possessed of a statistically excessive,
dependency susceptability quotient,

48
00:06:48,568 --> 00:06:53,036
wounded himself in the
forehead, with a hand drill.

49
00:06:54,676 --> 00:06:58,723
The wound, a hole about
one-half an inch in diameter,

50
00:06:58,823 --> 00:07:02,018
completely penetrated the subject's skull

51
00:07:02,801 --> 00:07:07,479
and seemed to afford him the relief
of imagined cranial pressures.

52
00:07:07,579 --> 00:07:09,361
Temporary euphoria,

53
00:07:09,461 --> 00:07:13,693
and electro-chemical dissociation, which he sought.

54
00:08:31,443 --> 00:08:33,948
It will perhaps clarify these matters

55
00:08:34,048 --> 00:08:39,267
if we consider the specific configuration
of the Category-A subject.

56
00:08:41,070 --> 00:08:43,541
Category-A is a classification

57
00:08:43,641 --> 00:08:47,068
which finds its meaning within
the scientific metaphysics

58
00:08:47,168 --> 00:08:51,748
of the aphrodisiast and theorist
Luther Stringfellow.

59
00:08:52,987 --> 00:08:56,745
It is not what is usually considered
an objective classification.

60
00:08:56,845 --> 00:09:02,065
Based for example, upon physical
or psychological characteristics.

61
00:09:03,319 --> 00:09:07,667
Category-A, is rather a certain
field reaction of the researcher

62
00:09:07,767 --> 00:09:13,699
to an intensive period of personal interaction
with each of his potential subjects.

63
00:09:15,425 --> 00:09:20,901
There is also, therefore, the element
of an aesthetics of human form.

64
00:09:21,001 --> 00:09:24,697
in the Category-A system
of subject classification.

65
00:09:25,719 --> 00:09:31,970
Dr. Stringfellow made his name, as an
exponent of the existential organic approach

66
00:09:32,070 --> 00:09:37,207
to the sciences, long before his work
in the socio-chemistry of the erotic

67
00:09:37,307 --> 00:09:40,604
achieved its present
international eminence.

68
00:09:41,432 --> 00:09:43,542
A Category-A classification

69
00:09:43,642 --> 00:09:48,415
is the existential organic
methodology put into practice.

70
00:09:49,716 --> 00:09:52,378
Dr. Stringfellow
has repeatedly stressed

71
00:09:52,478 --> 00:09:55,935
the vital necessity for
the use of this approach.

72
00:09:56,035 --> 00:09:58,764
Particularly,
in the new social sciences.

73
00:09:58,864 --> 00:10:00,820
where variables are infinite

74
00:10:00,920 --> 00:10:05,057
and complexity is of the
highest possible order.

75
00:10:06,388 --> 00:10:08,358
The nature of erotic research

76
00:10:08,458 --> 00:10:12,260
requires that the sexual emotional
involvement of the researcher

77
00:10:12,360 --> 00:10:17,302
with each subject be taken
to its farthest possible extreme.

78
00:10:17,817 --> 00:10:20,562
It is only in this way that the researcher

79
00:10:20,662 --> 00:10:27,180
in whose consciousness, a total mosaic
of his particular research project exists

80
00:10:27,500 --> 00:10:33,612
can divine in each of his subjects
a color, a texture, and a shape.

81
00:10:34,682 --> 00:10:40,346
And so assign to that subject,
his proper place in the research mosaic.

82
00:11:58,206 --> 00:12:02,921
It was hoped that many important questions
would be clarified, if not answered,

83
00:12:03,021 --> 00:12:06,948
during the course of our experiment
with induced telepathy.

84
00:12:07,431 --> 00:12:12,165
Two of the eight subjects consented to
having portions of their larynx removed.

85
00:12:12,446 --> 00:12:15,526
Making it physically
impossible for them to speak.

86
00:12:15,626 --> 00:12:16,644
In addition

87
00:12:16,744 --> 00:12:22,246
large portions of the speech centers of the
brains of these subjects were obliterated

88
00:12:22,346 --> 00:12:27,198
so that the psychological faculty of
speech itself would be impaired

89
00:12:28,315 --> 00:12:30,397
Would the maturing and strengthening

90
00:12:30,497 --> 00:12:33,587
of the telepathic capicity in
these speechless subjects

91
00:12:33,687 --> 00:12:38,715
surpass that of the subjects who
still retained the ability to speak?

92
00:12:40,087 --> 00:12:41,510
It was anticipated

93
00:12:41,610 --> 00:12:47,356
that the telepathic experience would be much
more than simply reading another's mind.

94
00:12:47,456 --> 00:12:51,154
One telepathist would perhaps
be able to apprehend

95
00:12:51,254 --> 00:12:55,204
not only verbal patterns,
as they arose in the mind of another,

96
00:12:55,304 --> 00:12:58,246
but would also encounter simultaneously

97
00:12:58,346 --> 00:13:02,584
the germinative layers of memory,
learning, emotional response,

98
00:13:02,684 --> 00:13:08,280
and psycho-physiological impulse, which
actually generate thought and language.

99
00:13:09,477 --> 00:13:12,839
How dependent upon language is thought?

100
00:13:12,939 --> 00:13:18,235
Is abstract logical thought even
possible without language?

101
00:14:34,623 --> 00:14:38,295
The essence of the telepathic bond

102
00:14:39,226 --> 00:14:40,348
is the dominance

103
00:14:40,448 --> 00:14:44,950
of one of the particles forming
the telepathic conglomerate.

104
00:14:45,720 --> 00:14:50,409
Whether the conglomerate consists
of two telepathists or of a large

105
00:14:50,509 --> 00:14:52,741
socially cohesive group

106
00:14:53,153 --> 00:14:57,110
the principle of psychic
dominance remains constant.

107
00:14:57,402 --> 00:15:00,651
The dominant personality
is responsible for the

108
00:15:00,751 --> 00:15:04,166
suppression of the heterogeneous
elements which the

109
00:15:04,266 --> 00:15:06,487
conglomerate comprises.

110
00:15:07,060 --> 00:15:09,623
The homogeneous elements which remain

111
00:15:09,723 --> 00:15:12,905
are then driven together
to form the conglomerate

112
00:15:13,005 --> 00:15:18,142
by the laws of telepathic bonding
implicit in the Stringfellow Hypothesis.

113
00:15:19,149 --> 00:15:23,824
The principle of dominance thus
provides the agglutinizing impetus

114
00:15:23,924 --> 00:15:27,663
which induces the initial
conglomerative motion.

115
00:15:28,416 --> 00:15:32,061
As Stringfellow notes in defining
the nature of dominance

116
00:15:32,314 --> 00:15:37,017
the will of the newly formed
conglomerate must necessarily

117
00:15:37,311 --> 00:15:41,099
be a function of the will of
the dominant personality.

118
00:15:41,381 --> 00:15:46,943
Only after an indefinite period of
symbiotic telepathic cohesion

119
00:15:47,238 --> 00:15:53,046
can the dominant personality abnegate
its agglutinative role and encourage

120
00:15:53,146 --> 00:15:57,992
the emergence of a truly synthetic
conglomerate personality.

121
00:15:58,908 --> 00:16:02,060
Stringfellow's behavior
as parapsychologists

122
00:16:02,160 --> 00:16:06,857
suggests that perhaps that his foremost
post-operative research objective

123
00:16:06,957 --> 00:16:10,459
was to intuit which among his Category-A subjects

124
00:16:10,559 --> 00:16:15,413
was the one most capable of assuming
the role of psychic dominant.

125
00:16:15,704 --> 00:16:19,370
This subject was then provided
indirectly with certain

126
00:16:19,470 --> 00:16:22,226
historical and interpretive data

127
00:16:22,326 --> 00:16:24,611
concerning his fellow subjects.

128
00:16:24,905 --> 00:16:30,139
This data would naturally assure the immediate
social dominance of the chosen subject

129
00:16:30,239 --> 00:16:35,533
within the context of the experimental,
socially isolate gestalt

130
00:16:36,624 --> 00:16:42,172
The function of the dominant would be
then to select from his fellow subjects

131
00:16:42,272 --> 00:16:45,411
the one psychically most vulnerable.

132
00:16:45,511 --> 00:16:49,697
And by the application of subtle, social intimidations

133
00:16:49,797 --> 00:16:53,737
followed by a careful series
of potent symbolic gestures

134
00:16:53,837 --> 00:16:59,609
draw the subject into the field of his
psycho-telepathic dominance.

135
00:16:59,928 --> 00:17:04,316
Once a true conglomerate
exhibiting complete telepathic

136
00:17:04,416 --> 00:17:08,172
bonding has been established
between this primary couple

137
00:17:08,450 --> 00:17:13,569
the progression towards a larger and
more complex conglomerate may begin.

138
00:20:01,716 --> 00:20:03,833
In psychic research

139
00:20:03,933 --> 00:20:08,234
the emotional distance between
the researcher and his subject

140
00:20:08,334 --> 00:20:13,697
is inevitably diminished until it is no more
than the distance between any two persons.

141
00:20:14,485 --> 00:20:18,433
The acquiescence of the subject
of the demands of the researcher

142
00:20:18,533 --> 00:20:24,185
comes nothing more nor less,
than an individual act of faith of love.

143
00:20:25,708 --> 00:20:29,344
If there can be no love between
researcher and subject

144
00:20:29,594 --> 00:20:32,551
there can be no experimentation.

145
00:20:32,830 --> 00:20:35,814
In conventional theories
of scientific methodology

146
00:20:35,914 --> 00:20:37,905
an experiment proves its validity

147
00:20:38,005 --> 00:20:41,870
when it can be universally
repeated in every aspect.

148
00:20:42,676 --> 00:20:47,387
In psychic research, such an
approach is completely untenable.

149
00:20:47,871 --> 00:20:52,627
The existential circumstances of
any experiment in parapsychology

150
00:20:52,727 --> 00:20:55,670
are inextricably mixed with the individuals

151
00:20:55,770 --> 00:21:00,621
and the phenomological sets involved
in that particular experiment

152
00:21:00,889 --> 00:21:04,067
and cannot be abstracted
from those individuals

153
00:21:04,167 --> 00:21:06,428
or phenomological sets.

154
00:21:07,305 --> 00:21:12,065
The conditions of experiments
in parapsychology are unique,

155
00:21:12,324 --> 00:21:16,063
non-uniform and non-repeatable.

156
00:21:16,163 --> 00:21:21,383
A completely non-scientific burden
is therefore place upon the researcher

157
00:21:21,483 --> 00:21:26,702
for if the peronal relationship between
researcher and subject deteriorates

158
00:21:26,802 --> 00:21:31,195
the experiment cannot continue
along its natural course.

159
00:21:31,589 --> 00:21:35,296
The sensitivity of the experimental
parapsychological plexus

160
00:21:35,396 --> 00:21:36,980
demands new methods

161
00:21:37,080 --> 00:21:41,504
to help maintain the emotional
momentum in certain experiments.

162
00:21:41,815 --> 00:21:43,065
Proving notice

163
00:21:43,355 --> 00:21:46,855
that the motto of the Canadian
Academy for Erotic Inquiry

164
00:21:46,955 --> 00:21:51,735
has immediate relevance for the Stringfellow induced telepathy series.

165
00:21:51,835 --> 00:21:56,774
"Amor vincit omnia."
Or: "Love conquers all."

166
00:23:20,052 --> 00:23:24,351
We understand that the unique way
in which an individual perceives

167
00:23:24,451 --> 00:23:26,239
and reacts to his environment

168
00:23:26,339 --> 00:23:30,844
is a funstion of his own
experiential space continuum.

169
00:23:30,944 --> 00:23:36,074
When object events enter the experiential
space continuum of that individual

170
00:23:36,174 --> 00:23:40,291
they become an integral
organic part of that space.

171
00:23:40,620 --> 00:23:44,082
They are actually changed,
qualitatively modified

172
00:23:44,182 --> 00:23:47,607
in accordance with the
creative nature of that space.

173
00:23:47,707 --> 00:23:50,809
But we are now dealing with telepathists.

174
00:23:50,909 --> 00:23:52,200
In theory

175
00:23:52,546 --> 00:23:58,031
the experiential space continua of
two or more telepathists can merge,

176
00:23:58,282 --> 00:24:03,762
can blend together to an extent far beyond
the range of normal human experience.

177
00:24:04,895 --> 00:24:09,743
What would be the organic nature
of communal experiential space

178
00:24:09,843 --> 00:24:13,439
shared among eight psychosomatic entities?

179
00:24:13,539 --> 00:24:17,520
Would one person, one mode
of perception and reaction,

180
00:24:17,620 --> 00:24:22,278
one experiential space continuum
dominate this oversoul?

181
00:24:22,642 --> 00:24:27,524
Or, would each mind participate
in the synthesis of a uniform

182
00:24:27,624 --> 00:24:32,227
newly created emergent space,
unlike any of its constituents?

183
00:24:32,800 --> 00:24:37,483
How would the physical and social functioning
of members of this group be altered?

184
00:24:37,784 --> 00:24:42,004
Or, could they function in
recognisable modes at all?

185
00:25:43,351 --> 00:25:48,479
There is some evidence to suggest
the potential telepathic intensity

186
00:25:48,814 --> 00:25:54,350
varies indirectly as the square of the
distance between two telepathists.

187
00:25:54,693 --> 00:25:59,567
An increase in physical distance may
therefore be used by one telepathist

188
00:25:59,667 --> 00:26:05,260
as an effective defence against telepathic
intrusion on the part of another telepathist.

189
00:26:05,976 --> 00:26:09,417
A far more sophisticated
intrusion avoidance device

190
00:26:09,517 --> 00:26:11,985
known as schizophonetic partition

191
00:26:12,085 --> 00:26:17,619
was evolved by one of the female subjects
during the group isolation sanitorium phase.

192
00:26:18,645 --> 00:26:22,725
In order to subvert attempts
by fellow subjects to establish

193
00:26:22,825 --> 00:26:25,468
potentially intrusive telepathic rapport

194
00:26:25,568 --> 00:26:29,674
she completely separated her
telepathic, non-verbal self

195
00:26:29,774 --> 00:26:31,652
from her oral verbal self.

196
00:26:32,709 --> 00:26:36,094
The telepathic self functioned
as a false self

197
00:26:36,194 --> 00:26:40,213
diverting fellow telepathists
from the real or true self

198
00:26:40,507 --> 00:26:46,786
which manifested itself only in occasional,
deliberately confused, verbal utterances.

199
00:26:47,112 --> 00:26:51,070
She thus protected her true
self from telepathic intrusion

200
00:26:51,170 --> 00:26:54,941
by abandoning her telepathic
faculty to a false self.

201
00:26:55,258 --> 00:27:00,743
Not surprisingly, repeated telepathic
probes undertaken by other subjects

202
00:27:00,843 --> 00:27:05,939
could not discover the true nature of
her experiential space continuum.

203
00:27:06,241 --> 00:27:09,570
The danger inherent in
schizophonetic partition

204
00:27:09,670 --> 00:27:12,628
as a telepathic intrusion avoidance device

205
00:27:12,728 --> 00:27:15,224
is that the false telepathic self

206
00:27:15,324 --> 00:27:20,543
tends to become increasingly parasitic
on the true oral/verbal self.

207
00:27:20,643 --> 00:27:23,064
The true self begins to suffocate

208
00:27:23,164 --> 00:27:26,300
in as much as it is starved
of contact with the outside.

209
00:27:26,673 --> 00:27:29,888
And the false self gradually
becomes the only self

210
00:27:29,988 --> 00:27:32,502
to interact with other selves.

211
00:27:32,602 --> 00:27:36,057
In the instance of our
female schizophonetic subject

212
00:27:36,417 --> 00:27:39,847
the true self began to express
its moribond existence

213
00:27:39,947 --> 00:27:43,859
through the telepathic emission
of violent images of decay

214
00:27:44,196 --> 00:27:48,702
vampirisim, disentegation and necrophilia.

215
00:27:48,978 --> 00:27:53,780
Which sporadically interrupted the
functioning of the telepathic false self.

216
00:27:54,794 --> 00:27:59,905
The intensity and frequency of this
emission of morbid, telepathic images

217
00:28:00,167 --> 00:28:05,555
rapidly increased until it began to
create the same depressed mode

218
00:28:05,655 --> 00:28:07,494
in those close to her.

219
00:30:34,731 --> 00:30:37,685
Another important function
of our experiment

220
00:30:37,785 --> 00:30:42,231
was to be the practical testing
of the Stringfellow hypothesis.

221
00:30:42,892 --> 00:30:47,243
The Stringfellow hypothesis takes
the form of the following equation:

222
00:30:48,717 --> 00:30:52,066
The rate of telepathic flow
between two minds,

223
00:30:52,166 --> 00:30:53,945
is directly proportional

224
00:30:54,045 --> 00:30:58,197
to the intensity of the relationship
between the two minds.

225
00:30:58,297 --> 00:31:01,373
The units used to measure flow and intensity

226
00:31:01,473 --> 00:31:03,569
are taken from psycho-chemistry.

227
00:31:04,282 --> 00:31:07,514
In less mathematical terms, we may say

228
00:31:07,614 --> 00:31:11,920
that this hypothesis indicates that
even between mature telepathists

229
00:31:12,020 --> 00:31:14,823
there can be no telepathic communication

230
00:31:15,112 --> 00:31:18,783
until some sort of existential
emotional relationship

231
00:31:18,883 --> 00:31:22,348
has been established in
conventional sensory ways.

232
00:31:23,082 --> 00:31:28,110
Thus, among complete strangers
there would exist only a kind of noise,

233
00:31:28,442 --> 00:31:30,192
a short wave static

234
00:31:31,200 --> 00:31:35,181
The Stringfellow hypothesis
also suggests that here

235
00:31:35,281 --> 00:31:39,551
the potency of human eroticism
would play an important role.

236
00:31:39,873 --> 00:31:44,143
A strong sexual attraction
would be a substantial basis

237
00:31:44,243 --> 00:31:49,477
for the establishing of a geometerically
increasing rate of telepathic flow.

238
00:33:44,650 --> 00:33:49,512
The politics of experience
on the level of individuals

239
00:33:49,612 --> 00:33:53,307
is a correlative of the
politics of social groups.

240
00:33:54,465 --> 00:33:57,881
The politics of telepathic experience

241
00:33:57,981 --> 00:34:03,004
may be studied as a projection of the
power struggles among individuals

242
00:34:03,104 --> 00:34:07,644
into the extremities of psychic
potency and complexity,

243
00:34:07,744 --> 00:34:13,101
a projection that is, into the
future of human social evolution.

244
00:36:58,885 --> 00:37:04,197
The role of the induced telepathy series,
in the total Canadian social experiment,

245
00:37:04,603 --> 00:37:06,358
is theoretically secure.

246
00:37:07,164 --> 00:37:11,203
That role emerges out of
Dr. Stringfellow's concept

247
00:37:11,508 --> 00:37:13,319
the telepathic commune.

248
00:37:13,604 --> 00:37:17,840
A group characterized by the blending
of experiential space continua

249
00:37:18,171 --> 00:37:21,878
and the constant instantaneous
exchange of data

250
00:37:22,134 --> 00:37:24,856
among the nervous systems
forming the commune.

251
00:37:25,608 --> 00:37:30,357
Stringfellow indicates that at
the most basic structural level

252
00:37:30,650 --> 00:37:35,436
the telepathic unit,
which empasizes acts of faith and love,

253
00:37:35,736 --> 00:37:40,142
seems the most plausible replacement
of the obsolescent family unit.

254
00:37:41,329 --> 00:37:43,788
The communal telepathic experience

255
00:37:44,069 --> 00:37:47,919
translated into the larger
social national context

256
00:37:48,238 --> 00:37:53,472
would preumably carry with it intrinsic
qualities of willing reciprocal dependency

257
00:37:53,572 --> 00:37:56,392
and a mutual experiential creativity.

258
00:37:57,434 --> 00:38:01,147
The social responsibility
of telepathic prototypes

259
00:38:01,247 --> 00:38:03,917
is therefore of the utmost gravity.

260
00:38:04,329 --> 00:38:07,101
For it is only by their individual efforts

261
00:38:07,201 --> 00:38:12,634
that the creation of the first and most
critical telepathic communes can be realized.

262
00:41:35,220 --> 00:41:40,083
For those of exponentially and
maturing telepathic capacities

263
00:41:40,396 --> 00:41:45,598
the question of phenomenological
refinement must inevitably become one

264
00:41:45,698 --> 00:41:49,964
with the problem of the
internalization of the sensory.

265
00:41:50,064 --> 00:41:53,015
The telepathist faces the
obsolescence of the

266
00:41:53,115 --> 00:41:56,569
senses and the possible atrophy
of the human sensorium

267
00:41:56,669 --> 00:41:59,534
in all but its most mundane operations.

268
00:42:00,537 --> 00:42:03,911
During a certain phase in
the telepathist's evolution

269
00:42:04,011 --> 00:42:09,423
the sensory will attempt to
internalize to avoid its extinction

270
00:42:09,523 --> 00:42:12,317
on the level of intellectuality and emotion.

271
00:42:13,441 --> 00:42:16,203
This avoidance could take two forms.

272
00:42:16,486 --> 00:42:18,921
One would be the forced psychic injestion

273
00:42:19,021 --> 00:42:23,149
of the entire universe is conveyed
in terms of sensory information.

274
00:42:23,249 --> 00:42:26,209
This would partially manifest itself in

275
00:42:26,309 --> 00:42:30,282
in the instantaneous translation
into the vocabulary of the sensory

276
00:42:30,382 --> 00:42:35,043
of all purely psychic abstract
or extrasensory phenomena.

277
00:43:06,920 --> 00:43:11,100
Second form of avoidance would
be the reification of the psychic,

278
00:43:11,200 --> 00:43:17,157
the abstract or the extrasensory, and its
subsequent externalization or projection.

279
00:43:17,634 --> 00:43:22,763
The process begun by the subjectification
and displacement of the nonsensory

280
00:43:22,863 --> 00:43:26,142
would find a point of
equilibrium and complete

281
00:43:26,242 --> 00:43:30,030
itself only with the indescriminate
return or internalization

282
00:43:30,366 --> 00:43:33,796
of both the projective nonsensory information

283
00:43:34,073 --> 00:43:36,191
and its sensory screen,

284
00:43:36,509 --> 00:43:40,832
the passive external world as perceived by the senses.

285
00:43:49,367 --> 00:43:55,389
In both instances, the distinction between
the sensory and the nonsensory,

286
00:43:55,694 --> 00:43:59,212
the concrete and the abstract,
the psychic and the physical,

287
00:43:59,312 --> 00:44:01,029
would be lost utterly.

288
00:45:38,268 --> 00:45:41,385
These opposite poles of human sexuality

289
00:45:41,485 --> 00:45:46,464
are traditionally held to be
hetrosexuality and homosexuality.

290
00:45:48,098 --> 00:45:52,583
Yet this same bipolar structure
of sexuality is ignored

291
00:45:52,683 --> 00:45:55,742
when the question of normaility
and deviation arises.

292
00:45:56,946 --> 00:45:59,937
The norm is taken to be hetrosexuality

293
00:46:00,266 --> 00:46:03,372
and both bisexuality
and homosexuality

294
00:46:03,472 --> 00:46:06,182
are considered deviations from that norm.

295
00:46:07,543 --> 00:46:12,287
The primary justification for normality
of hetrosexuality is reproduction.

296
00:46:13,636 --> 00:46:18,616
Only a hetrosexual relationship can,
as yet, result in regeneration.

297
00:46:19,421 --> 00:46:23,352
This argument colapses however
once it is demonstrated

298
00:46:23,452 --> 00:46:28,540
that the sexuality involved in reproduction
represents only a very small,

299
00:46:28,927 --> 00:46:33,548
almost accidental segment of
the total human sexual spectrum.

300
00:46:34,802 --> 00:46:40,252
Academy research has established that
both hetrosexuality and homosexuality

301
00:46:40,352 --> 00:46:43,618
are equally, what might
be termed perversions,

302
00:46:44,173 --> 00:46:47,665
relative to the potential
human sexual field.

303
00:46:48,256 --> 00:46:53,727
In this context, the true norm is
an expanded form of bisexuality

304
00:46:54,011 --> 00:46:56,250
which we term omnisexuality.

305
00:46:57,575 --> 00:47:00,950
As an aphrodisiast, Dr. Stringfellow

306
00:47:01,050 --> 00:47:04,327
proposes the use of synthetic
aphrodisiac drugs

307
00:47:04,427 --> 00:47:07,179
to assist those who wish to attain

308
00:47:07,279 --> 00:47:10,327
a fully three dimensional sexuality.

309
00:47:11,255 --> 00:47:14,249
The proper use of psychic aphrodisiacs

310
00:47:14,349 --> 00:47:17,631
such as those being
developed by Dr. Stringfellow

311
00:47:17,731 --> 00:47:20,836
at the Canadian Academy for Erotic Inquiry

312
00:47:20,936 --> 00:47:24,724
is not to increase sexual
potency or fertility

313
00:47:24,987 --> 00:47:30,558
but to demolish the walls of psychological
restraint and social inhibition

314
00:47:30,658 --> 00:47:33,811
which restrict persons to a monosexuality

315
00:47:34,095 --> 00:47:38,645
or to a stunted bisexual
form of omnisexuality.

316
00:47:39,902 --> 00:47:44,146
A telepathist then, by virtue
of the omnisexual nature,

317
00:47:44,246 --> 00:47:46,779
of his experiential space continuum,

318
00:47:47,138 --> 00:47:53,378
may readily be seen to be the possible
prototype of three dimensional man.

319
00:50:33,921 --> 00:50:39,784
Certain unexpected result however,
threw the future of the project into doubt.

320
00:50:39,884 --> 00:50:44,929
When preliminary confrontations between
any two subjects were arranged

321
00:50:45,029 --> 00:50:49,980
the subjects not only reported no
instances of telepathic communication

322
00:50:50,080 --> 00:50:55,662
but quite emphatically avoided even the
most casual sensory contact with each other.

323
00:50:55,832 --> 00:50:59,895
At the same time,
electroencephalographic probes

324
00:50:59,995 --> 00:51:04,851
indicated that such telepathic
communication as was denied by the subjects

325
00:51:04,951 --> 00:51:10,396
was in fact taking place, and at
a remarkably high rate of flow.

326
00:51:10,826 --> 00:51:14,789
How could such flow exist among strangers?

327
00:51:14,950 --> 00:51:17,730
In an emergency program modification

328
00:51:17,830 --> 00:51:22,983
five of our eight subjects were
combined in an enforced community study.

329
00:51:23,083 --> 00:51:28,382
These five subjects, although psychochemically
the least volatile of the group

330
00:51:28,482 --> 00:51:32,802
almost immediately retreated
into a state of false incapsulation.

331
00:51:32,902 --> 00:51:38,385
Refusing to communicate in any way
with either researchers or each other.

332
00:51:38,548 --> 00:51:41,607
Before further investigations could be made

333
00:51:41,707 --> 00:51:44,881
two of the five subjects committed suicide

334
00:51:44,981 --> 00:51:47,056
in their residences at the institute.

335
00:51:47,156 --> 00:51:51,186
Another, pierced his skull
with an electric drill,

336
00:51:51,286 --> 00:51:54,375
an act of considerable symbolic significance.

337
00:51:54,475 --> 00:51:58,458
Careful separation of the remaining
subjects was maintained.

338
00:54:08,968 --> 00:54:14,825
The study of human erotic morphology
deals more directly with this question.

339
00:54:15,326 --> 00:54:19,039
Morphology is the study
of form and structure.

340
00:54:19,415 --> 00:54:22,800
Erotic morphology is the study of those

341
00:54:22,900 --> 00:54:26,559
forms which evoke an
erotic or sexual response

342
00:54:26,659 --> 00:54:30,122
when they are perceived or
otherwise communicated.

343
00:54:30,893 --> 00:54:37,003
Not only the shape of objects or body
parts, but also the morphology of motion,

344
00:54:37,103 --> 00:54:39,430
of movement in certain patterns,

345
00:54:39,530 --> 00:54:43,514
can convey erotic signals
of a very complex nature.

346
00:54:44,370 --> 00:54:50,546
Finally, the combination of shape plus
motion can attain an erotic significance

347
00:54:50,646 --> 00:54:55,436
which is contained in neither shape
nor motion when taken separately.

348
00:54:55,536 --> 00:55:00,179
But, the telepathist does not
have to perceive a breast or thigh

349
00:55:00,279 --> 00:55:02,264
or sexual motion directly.

350
00:55:02,364 --> 00:55:07,271
He can apprehend the thought of
a breast, thigh, or erotic motion

351
00:55:07,371 --> 00:55:09,664
in the mind of another telepathist.

352
00:55:09,764 --> 00:55:13,064
And this thought is received
with greater impact

353
00:55:13,164 --> 00:55:15,983
than the perception of the actual breast,

354
00:55:16,083 --> 00:55:20,049
thigh, or erotic motion
would itself have been.

355
00:55:20,806 --> 00:55:26,795
Thus, although both telepathists in our
hypothetical confrontation are male

356
00:55:26,895 --> 00:55:30,951
they have made an erotic
morphological communication

357
00:55:31,051 --> 00:55:35,282
which is essentially hetrosexual,
or male-female.

358
00:55:35,729 --> 00:55:37,956
We can begin to see why

359
00:55:38,056 --> 00:55:43,074
the telepathic experience is
essentially omnisexual in nature.

360
00:55:43,174 --> 00:55:48,598
Why the male-female bisexual
categories can no longer apply.

361
00:57:43,227 --> 00:57:48,134
In the sanitorium phase of
the induced telepathy series

362
00:57:49,677 --> 00:57:54,674
measures were introduced to
prevent further instances

363
00:57:55,520 --> 00:58:01,155
of interuption caused by
frustrated telepathic dependency.

364
00:58:02,582 --> 00:58:08,141
A single compact electromagnetic
receiver transmitter

365
00:58:08,660 --> 00:58:12,976
was provided for the subjects
undergoing primary group isolation.

366
00:58:13,746 --> 00:58:17,941
The receiver transmitter
was to feed the subjects

367
00:58:18,444 --> 00:58:20,298
reproductions

368
00:58:20,773 --> 00:58:25,696
of a variety of patterns of
electrochemical discharge

369
00:58:26,146 --> 00:58:30,318
unique to Dr. Stringfellow,
the group's common researcher.

370
00:58:31,542 --> 00:58:35,215
The prupose of these transmissions
from the institute

371
00:58:35,969 --> 00:58:40,418
was to ease the difficult transition from the

372
00:58:40,518 --> 00:58:43,210
inevitable telepathic dependency

373
00:58:43,617 --> 00:58:48,500
established individually between
Stringfellow and each subject

374
00:58:49,185 --> 00:58:55,077
to a group telepathic cohesion
and a group telepathic autonomy.

375
00:58:56,188 --> 00:59:00,858
The necessity of sharing the
transmitted electrochemical patterns

376
00:59:01,351 --> 00:59:05,375
either through the sharing of
the receiver transmitter itself

377
00:59:05,810 --> 00:59:10,610
or the telepathic sharing of the
experience of receiving the poatterns

378
00:59:11,864 --> 00:59:16,771
would provide a basis for the
formation of a telepathic commune,

379
00:59:17,667 --> 00:59:22,418
which did not depend upon
Stringfellow as a psychic cluster nucleus.

380
00:59:23,707 --> 00:59:27,688
Dr, Stringfellow indicated that he himself

381
00:59:27,788 --> 00:59:31,081
entered a severly depressive psychological

382
00:59:31,181 --> 00:59:35,014
mode upon separation from
the induced telepathy group.

383
00:59:35,487 --> 00:59:36,691
An event,

384
00:59:37,372 --> 00:59:40,990
suggesting that to some extent

385
00:59:42,477 --> 00:59:47,207
telepathic dependency
functions reciprocally.

386
00:59:47,748 --> 00:59:52,817
Even between telepathists
and non-paranormals.

387
01:00:41,701 --> 01:00:45,290
Five intensive months of work
with the remaining subjects

388
01:00:45,390 --> 01:00:50,838
revealed ways to gradually increase the
tolerance among them, or group exposure.

389
01:00:51,838 --> 01:00:54,550
It was found that the telepathic experience

390
01:00:54,650 --> 01:00:58,826
was likely to be an overwhelming
and extremely exhausting one

391
01:00:59,240 --> 01:01:01,678
verging on pain and hallucination.

392
01:01:02,517 --> 01:01:06,246
The most crucial problem for
the telepathist apparently

393
01:01:06,346 --> 01:01:10,750
was to develop within himself
the means to control and modulate

394
01:01:10,850 --> 01:01:15,613
the rate and quality of telepathic
flow between himself and another.

395
01:01:16,693 --> 01:01:20,023
The inexperienced telepathist
simply cannot cope

396
01:01:20,123 --> 01:01:24,998
with the complexity and effort involved
in the social telepathic situation.

397
01:01:26,243 --> 01:01:31,704
The first meeting of all the subjects as a
group took place at the Academy sanitorium

398
01:01:31,804 --> 01:01:33,911
somewhat later than originally planned.

399
01:01:34,882 --> 01:01:38,376
The sixth subject arrived
two weeks after the others

400
01:01:38,476 --> 01:01:43,127
when it was certain that he had fully
recovered from his self inflicted violence.

401
01:01:43,925 --> 01:01:46,655
It will be sometime before
the data on this period

402
01:01:46,755 --> 01:01:50,555
accumulated through electroencephalographic
information retrieval

403
01:01:50,655 --> 01:01:52,589
can fully evaluated.

