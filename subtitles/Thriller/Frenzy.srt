﻿1
00:02:17,221 --> 00:02:18,638
MAN: When I was a lad,

2
00:02:18,722 --> 00:02:24,101
a journey down
the rivers of England
was a truly blithe experience.

3
00:02:24,186 --> 00:02:29,607
"Bliss was it in
that dawn to be alive,"
as Wordsworth has it.

4
00:02:29,691 --> 00:02:32,151
Brooklime and flag iris,

5
00:02:32,236 --> 00:02:36,322
plantain and marsh marigolds
rioted on the banks.

6
00:02:36,573 --> 00:02:40,660
And kingfishers
swooped and darted about,

7
00:02:40,744 --> 00:02:43,788
their shadows racing
over the brown trout.

8
00:02:44,373 --> 00:02:46,290
Well, ladies and gentlemen,

9
00:02:46,375 --> 00:02:48,209
I'm happy to be
able to tell you

10
00:02:48,293 --> 00:02:50,419
that these ravishing sights

11
00:02:50,504 --> 00:02:53,506
will be restored to us again
in the near future,

12
00:02:53,632 --> 00:02:58,010
thanks to the diligent efforts
of your government and
your local authority.

13
00:02:58,095 --> 00:03:02,765
All the water above this point
will soon be clear.

14
00:03:02,850 --> 00:03:05,268
Clear of industrial effluent.

15
00:03:05,435 --> 00:03:07,520
Clear of detergents.

16
00:03:07,604 --> 00:03:10,481
Clear of the waste products
of our society,

17
00:03:10,566 --> 00:03:14,277
with which for so long
we have poisoned
our rivers and canals.

18
00:03:18,365 --> 00:03:22,201
Let us rejoice that
pollution will soon
be banished

19
00:03:22,286 --> 00:03:23,619
from the waters
of this river,

20
00:03:23,704 --> 00:03:25,872
and that there will
soon be no foreign...

21
00:03:25,956 --> 00:03:27,456
Look!

22
00:03:28,417 --> 00:03:29,458
What is it?

23
00:03:31,169 --> 00:03:32,378
It's a woman!

24
00:03:42,973 --> 00:03:44,140
What's that
'round her neck?

25
00:03:46,268 --> 00:03:47,643
She's been strangled!

26
00:03:47,853 --> 00:03:49,353
MAN 1: Looks like a tie.

27
00:03:49,438 --> 00:03:51,522
MAN 2: Yes, it's a tie,
all right.

28
00:03:51,648 --> 00:03:53,274
Another necktie murder.

29
00:03:54,651 --> 00:03:56,152
Come on.
Move out of the way.

30
00:03:56,361 --> 00:03:58,821
Please come away
from here, Sir George.

31
00:04:02,200 --> 00:04:03,367
It's another
necktie murder.

32
00:04:03,452 --> 00:04:05,453
What are the police
doing about it? That's
what I'd like to know.

33
00:04:05,537 --> 00:04:06,662
Why can't they
find him?

34
00:04:06,747 --> 00:04:08,205
He's a regular
Jack the Ripper.

35
00:04:08,290 --> 00:04:10,958
Not on your life.
He used to carve 'em up.

36
00:04:11,043 --> 00:04:13,085
Sent a bird's kidney
to Scotland Yard once,

37
00:04:13,170 --> 00:04:14,295
wrapped in a bit of
violet writing paper.

38
00:04:14,379 --> 00:04:16,088
That'll do, Herb.
I'm quite sure the lady

39
00:04:16,173 --> 00:04:17,340
doesn't want to hear
any more about it.

40
00:04:17,424 --> 00:04:18,674
Or was it
a bit of a liver?

41
00:04:20,719 --> 00:04:23,721
I say, it's not
my club tie, is it?

42
00:04:58,548 --> 00:05:00,675
FORSYTHE: Cheers,
Squadron Leader.

43
00:05:01,551 --> 00:05:02,760
Chin chin.

44
00:05:02,886 --> 00:05:04,136
Good morning.

45
00:05:05,097 --> 00:05:07,348
Look, it may come
as something of
a surprise to you, Blaney,

46
00:05:07,432 --> 00:05:10,476
but in this pub
we sell liquor,
we don't give it away.

47
00:05:10,560 --> 00:05:12,979
Still less do we expect
our employees to steal it.

48
00:05:13,397 --> 00:05:14,397
I was going to pay for it.

49
00:05:14,481 --> 00:05:15,564
Oh, yeah,
I'm sure you were.

50
00:05:15,649 --> 00:05:18,025
Well, that's the last drink
you're gettin' on this house.
Go on. Get out.

51
00:05:18,110 --> 00:05:19,443
I told you I was
going to pay for it.

52
00:05:19,528 --> 00:05:21,404
I always pay for my drinks.

53
00:05:21,488 --> 00:05:23,447
Even for your
watered-down gin.

54
00:05:23,532 --> 00:05:26,242
Now, don't come
the innocent with me,
you bastard!

55
00:05:26,326 --> 00:05:28,077
My stocks have been
well down this past month.

56
00:05:28,161 --> 00:05:29,328
Now, you watch
what you're saying!

57
00:05:29,413 --> 00:05:30,621
What, to a thief?

58
00:05:30,706 --> 00:05:32,540
What's going on?

59
00:05:32,624 --> 00:05:35,876
Our friend here says that
I've been pinching his booze.

60
00:05:35,961 --> 00:05:37,586
Ridiculous!
He always pays.

61
00:05:37,629 --> 00:05:39,547
How would you know?
Well, I work
with him, don't I?

62
00:05:39,631 --> 00:05:40,798
And what else?

63
00:05:40,924 --> 00:05:42,133
What's that
supposed to mean?

64
00:05:42,217 --> 00:05:44,677
Look, just keep out of this,
will you, Babs?
Come on, Blaney, outside.

65
00:05:44,761 --> 00:05:45,886
FORSYTHE: You're fired.
BABS: You can't do that.

66
00:05:45,971 --> 00:05:47,388
He never stole
nothing in his life.

67
00:05:47,472 --> 00:05:48,931
He puts the money
in the till.
I've seen him.

68
00:05:49,016 --> 00:05:51,183
A thief or a boozer,
it's all the same to me.

69
00:05:51,268 --> 00:05:53,352
I don't need
either one as a barman,

70
00:05:53,437 --> 00:05:54,812
quite apart from
the fact that half the time

71
00:05:54,896 --> 00:05:56,564
he's pulling your tits
instead of pulling pints.

72
00:05:56,690 --> 00:05:58,315
Now, look here...
He can't keep
his hands off you.

73
00:05:58,400 --> 00:05:59,817
The customers are
always talking about it.

74
00:05:59,901 --> 00:06:02,194
And what about you?
Always fingering me.

75
00:06:02,279 --> 00:06:04,989
You keep your
lying mouth shut, Babs,
or you can get out as well.

76
00:06:05,073 --> 00:06:06,365
I'm off.

77
00:06:06,950 --> 00:06:08,826
Keep the change!

78
00:06:14,708 --> 00:06:16,292
I'll send for
my things later.

79
00:06:16,376 --> 00:06:18,544
Just a minute!

80
00:06:18,628 --> 00:06:21,839
There's a little matter
of £10 I advanced you
on your salary.

81
00:06:21,923 --> 00:06:24,133
Are you planning to
steal that as well?

82
00:06:28,096 --> 00:06:30,806
There you are.
You know what
to do with them.

83
00:06:37,147 --> 00:06:39,231
You shouldn't let him
talk to you like that.

84
00:06:39,316 --> 00:06:40,316
I know.

85
00:06:40,400 --> 00:06:41,901
What are you
gonna do, love?

86
00:06:42,527 --> 00:06:43,861
I don't know.

87
00:06:43,945 --> 00:06:45,196
Another pub perhaps.

88
00:06:45,280 --> 00:06:46,697
Are you all right?

89
00:06:46,782 --> 00:06:49,158
You just gave him back
the 10 quid you borrowed.

90
00:06:49,242 --> 00:06:52,912
I had to.
He didn't think I had it.

91
00:06:52,996 --> 00:06:54,080
Don't worry,
I've got a bit left.

92
00:06:54,164 --> 00:06:56,082
Well, this is Covent Garden,
not the garden of love.

93
00:06:56,166 --> 00:06:58,000
How 'bout starting work?

94
00:06:58,335 --> 00:06:59,835
Oh, get stuffed.

95
00:07:03,548 --> 00:07:04,715
Look after yourself.

96
00:07:04,841 --> 00:07:06,008
I'll call you.

97
00:07:16,353 --> 00:07:18,104
Thanks, guv.

98
00:08:02,482 --> 00:08:03,941
Hello, Dick.
Hello, Bob.

99
00:08:04,025 --> 00:08:06,110
I was just coming over
for a quick one.

100
00:08:06,194 --> 00:08:08,988
Why aren't you back there
polishing the sausages
or watering the gin,

101
00:08:09,072 --> 00:08:12,032
or whatever it is
you do there
before opening time?

102
00:08:12,117 --> 00:08:14,577
I have just been
given the push.

103
00:08:15,579 --> 00:08:16,871
What for?

104
00:08:16,955 --> 00:08:19,290
You weren't pissing
in the beer again?

105
00:08:19,374 --> 00:08:21,083
Forsythe and I had
a set-to.

106
00:08:21,209 --> 00:08:22,918
Oh, him.

107
00:08:23,003 --> 00:08:25,296
You duffed him up, I hope.

108
00:08:25,380 --> 00:08:27,089
He's a bastard.

109
00:08:28,300 --> 00:08:30,843
He was on my back
right from the start.

110
00:08:30,927 --> 00:08:33,012
From squadron leader to barman
in one easy lesson!

111
00:08:33,096 --> 00:08:34,096
He wouldn't leave it alone.

112
00:08:34,806 --> 00:08:36,223
He's the boss'
brother-in-law,
isn't he?

113
00:08:36,308 --> 00:08:38,225
Forsythe?
Yeah, I think so.

114
00:08:38,602 --> 00:08:40,853
Brother-in-laws
are the worst.

115
00:08:40,937 --> 00:08:43,063
Or should I say,
"Brothers-in-law"?

116
00:08:45,567 --> 00:08:46,567
What are you gonna do now?

117
00:08:48,111 --> 00:08:49,278
I haven't decided yet.

118
00:08:49,654 --> 00:08:51,906
Well, if you're in schtuk,
you know where to come.

119
00:08:52,824 --> 00:08:53,866
Thanks.

120
00:08:54,409 --> 00:08:55,534
George!

121
00:08:56,328 --> 00:08:57,620
That's the last one.

122
00:08:57,787 --> 00:08:59,121
Thanks, Mr. Rusk.

123
00:09:02,792 --> 00:09:04,335
Why don't you go
and have a chat
with your ex?

124
00:09:04,419 --> 00:09:06,337
She's doin' all right,
isn't she?

125
00:09:06,421 --> 00:09:08,214
I haven't seen her
for ages, as you know.

126
00:09:08,298 --> 00:09:10,049
There's no use
opening all that up again.

127
00:09:10,133 --> 00:09:12,009
No, I suppose not.

128
00:09:12,093 --> 00:09:15,137
Well, as I say,
you can always rely on me.

129
00:09:16,973 --> 00:09:18,182
You're all right
for a few quid?

130
00:09:18,308 --> 00:09:19,516
Yes. Thanks all the same.

131
00:09:19,601 --> 00:09:20,684
'Cause if you're not...

132
00:09:20,769 --> 00:09:22,478
No, no. Really.
I just got paid.

133
00:09:23,188 --> 00:09:25,022
Well...

134
00:09:25,106 --> 00:09:26,190
Have some grapes.

135
00:09:26,274 --> 00:09:27,816
Here you are.
I'll get you a box.

136
00:09:32,489 --> 00:09:35,282
Finest muscats,
fresh in this morning.

137
00:09:35,367 --> 00:09:38,494
Here you are.
Take one of these back
to your girlfriend, Babs.

138
00:09:38,578 --> 00:09:42,248
Get her to peel you one.
"Beulah, peel me a grape."

139
00:09:42,332 --> 00:09:44,750
That's what my
ol' mum used to say
when I was a kid.

140
00:09:44,834 --> 00:09:46,126
Well, at least you
won't starve to death.

141
00:09:46,211 --> 00:09:47,211
(CHUCKLES)

142
00:09:49,547 --> 00:09:52,258
Are you sure
you don't need
a few bob?

143
00:09:52,342 --> 00:09:53,759
No, I'm okay.

144
00:09:53,843 --> 00:09:56,387
Well, you don't look okay.
Anything else the matter?

145
00:09:58,348 --> 00:10:00,432
No. What should be?

146
00:10:00,517 --> 00:10:01,934
I don't know.

147
00:10:02,852 --> 00:10:05,437
Well, remember,
anything I can do,
anytime, it's a pleasure.

148
00:10:05,522 --> 00:10:06,522
Ta.

149
00:10:06,606 --> 00:10:09,024
It won't be the same
in the Old Globe now.

150
00:10:09,109 --> 00:10:10,234
Well, Babs
is still there.

151
00:10:10,360 --> 00:10:11,485
Yeah, that's true.

152
00:10:11,569 --> 00:10:12,736
And she's
prettier than you.

153
00:10:13,280 --> 00:10:15,572
A matter of opinion.
Bye, now.

154
00:10:15,657 --> 00:10:18,075
Hey, wait a minute.
Here, give us your paper.

155
00:10:18,159 --> 00:10:20,411
Here you are.
This will make you
a fortune.

156
00:10:20,495 --> 00:10:23,497
This afternoon in the 3:00,
Coming Up.

157
00:10:23,581 --> 00:10:26,125
Never been
out before, but very
well-fancied at home.

158
00:10:26,376 --> 00:10:27,876
Now, this is
a four-horse race,

159
00:10:27,961 --> 00:10:29,461
and the other three
have all won before.

160
00:10:29,546 --> 00:10:31,797
So she'll start about
20-to-1, maybe more.

161
00:10:31,881 --> 00:10:33,007
20-to-1?

162
00:10:33,091 --> 00:10:34,550
Put your word on it.
She can't lose.

163
00:10:34,634 --> 00:10:37,428
A little birdie told me,
and my little
birdies are reliable.

164
00:10:37,512 --> 00:10:38,470
Well, thanks again, Bob.

165
00:10:38,555 --> 00:10:39,555
Anytime.

166
00:10:39,639 --> 00:10:40,973
Don't forget,
Bob's your uncle.

167
00:10:41,057 --> 00:10:42,975
Good morning, Mr. Rusk.

168
00:10:43,143 --> 00:10:45,185
Hello, Sergeant.
What's new?

169
00:10:45,270 --> 00:10:46,312
Not much.

170
00:10:46,396 --> 00:10:48,772
This necktie fellow's
giving them a bit of
a headache, though.

171
00:10:48,857 --> 00:10:50,816
Can't seem to get
a line on him.

172
00:10:50,900 --> 00:10:52,276
Have you tried
advertising?

173
00:10:52,360 --> 00:10:54,069
Oh, very funny.

174
00:10:54,529 --> 00:10:55,904
Look, you're one
for the birds.

175
00:10:56,031 --> 00:10:57,531
Ask 'em all if they've
ever had a near miss

176
00:10:57,615 --> 00:10:59,116
with a bloke like that,
would you?

177
00:10:59,284 --> 00:11:00,701
Or if any of their
girlfriends have.

178
00:11:00,785 --> 00:11:01,785
Sure.

179
00:11:01,870 --> 00:11:04,621
Mind you,
half of them haven't got
their heads screwed on right,

180
00:11:04,706 --> 00:11:07,041
let alone knowing
when they've been
screwed off.

181
00:11:07,125 --> 00:11:08,834
Have you met
my friend Dick B...

182
00:11:12,881 --> 00:11:14,923
Funny fella.

183
00:11:15,008 --> 00:11:16,717
Well, don't worry, Sergeant.
I'll put the word about.

184
00:11:16,801 --> 00:11:18,302
Thanks, Mr. Rusk.

185
00:11:26,603 --> 00:11:27,770
(PEOPLE TALKING INDISTINCTLY)

186
00:11:31,775 --> 00:11:33,442
A large brandy.

187
00:11:34,110 --> 00:11:35,361
What're you
gonna have, Doctor?

188
00:11:35,445 --> 00:11:37,363
A pint and a morsel of
cheese'll do me fine.

189
00:11:37,447 --> 00:11:39,656
Let me order us
a hot lunch.
We have plenty of time.

190
00:11:39,824 --> 00:11:40,991
Hello, Mr. Usher.

191
00:11:41,076 --> 00:11:42,409
What's good
today, Maisie?

192
00:11:42,660 --> 00:11:43,911
Stick to the shepherd's.
I would.

193
00:11:44,954 --> 00:11:47,373
Right. That's two
shepherd's pies,
then, please.

194
00:11:47,457 --> 00:11:49,208
And two pints.

195
00:11:49,542 --> 00:11:53,045
I see our
necktie murderer's
been up to it again.

196
00:11:53,129 --> 00:11:55,756
I saw the newspaper
headlines as we
came away from the court.

197
00:11:55,840 --> 00:11:56,840
Hmm.

198
00:11:57,217 --> 00:12:01,053
I wouldn't envy
the lot of any medical man
giving evidence at that trial.

199
00:12:01,137 --> 00:12:02,679
Why not?

200
00:12:02,764 --> 00:12:05,182
Why, surely
it's easier in these days

201
00:12:05,308 --> 00:12:07,768
of legally diminished
responsibility.

202
00:12:08,311 --> 00:12:10,521
In many cases
you may be right.

203
00:12:10,605 --> 00:12:12,439
But not here.

204
00:12:12,524 --> 00:12:14,024
The man who's
killing these women

205
00:12:14,109 --> 00:12:16,276
is a criminal,
sexual psychopath.

206
00:12:17,070 --> 00:12:19,488
And the legal profession
has never really known
how to treat them.

207
00:12:20,115 --> 00:12:23,784
I suppose you could
call them social misfits.

208
00:12:24,411 --> 00:12:27,121
We were just talking about
the tie murderer, Maisie.

209
00:12:27,205 --> 00:12:28,539
You better watch out.

210
00:12:28,623 --> 00:12:30,541
He rapes 'em first,
doesn't he?

211
00:12:31,960 --> 00:12:33,544
Yes, I believe he does.

212
00:12:33,628 --> 00:12:35,087
Well, I suppose
it's nice to know

213
00:12:35,171 --> 00:12:36,672
that every cloud
has a silver lining.

214
00:12:37,173 --> 00:12:38,173
Oh.

215
00:12:39,968 --> 00:12:42,803
On the surface,
in casual conversation,

216
00:12:42,887 --> 00:12:47,015
they appear as ordinary,
likable adult fellows.

217
00:12:47,100 --> 00:12:51,603
But emotionally
they remain as
dangerous children,

218
00:12:51,688 --> 00:12:56,275
whose conduct may
revert to a primitive,
subhuman level at any moment.

219
00:12:56,359 --> 00:12:57,317
Large brandy.

220
00:12:57,402 --> 00:12:59,319
You mean they'll
kill at any time,

221
00:12:59,404 --> 00:13:00,487
just as the mood
takes them?

222
00:13:00,572 --> 00:13:01,989
Exactly.

223
00:13:02,073 --> 00:13:04,450
And, being governed
by the pleasure principle,

224
00:13:04,534 --> 00:13:05,993
they're particularly
dangerous

225
00:13:06,077 --> 00:13:07,870
when their desires
are being frustrated.

226
00:13:09,330 --> 00:13:12,291
Are you deaf?
I distinctly said
a large brandy.

227
00:13:12,375 --> 00:13:14,168
There's scarcely enough
in that to cover the bottom.

228
00:13:14,252 --> 00:13:16,712
Actually, you can
make it a triple.

229
00:13:16,796 --> 00:13:19,214
I wonder if the police
have got any sort of
line on this fellow.

230
00:13:19,299 --> 00:13:21,467
Oh, I shouldn't think so.

231
00:13:21,551 --> 00:13:24,678
With psychopaths
there's usually
no linking motive.

232
00:13:24,762 --> 00:13:26,430
Let's hope
he slips up soon.

233
00:13:26,764 --> 00:13:29,016
In one way,
I rather hope he doesn't.

234
00:13:29,767 --> 00:13:32,269
Well, we haven't had
a good, juicy series

235
00:13:32,353 --> 00:13:34,062
of sex murders
since Christie.

236
00:13:34,147 --> 00:13:37,065
And they're so good
for the tourist trade.

237
00:13:37,150 --> 00:13:40,360
Foreigners somehow
expect the squares of
London to be fog-wreathed,

238
00:13:40,445 --> 00:13:44,406
full of hansom cabs
and littered with ripped
whores, don't you think?

239
00:13:48,703 --> 00:13:50,370
RUSK: Hey, Dick!

240
00:13:51,706 --> 00:13:52,831
What about
Coming Up then?

241
00:13:53,583 --> 00:13:56,001
No, I'm afraid
I haven't any time.
Thanks all the same.

242
00:13:56,085 --> 00:13:58,962
No, Coming Up, the horse.
It won by a mile.

243
00:13:59,422 --> 00:14:01,798
20-to-1!
What did I tell you?

244
00:14:04,761 --> 00:14:06,595
Made a fortune.
Thanks a lot.

245
00:14:06,679 --> 00:14:08,013
Anytime.

246
00:14:08,097 --> 00:14:09,431
Hey, wait a minute!

247
00:14:11,476 --> 00:14:14,853
This is my ma.
Ma, meet Dick Blaney,

248
00:14:14,938 --> 00:14:17,105
the best pilot who ever
pulled a pint of beer.

249
00:14:17,524 --> 00:14:18,524
Hello, Mrs. Rusk.

250
00:14:18,900 --> 00:14:20,609
Pleased to meet you,
I'm sure.

251
00:14:20,985 --> 00:14:24,112
She lives down in Kent,
in the Garden of England!

252
00:14:25,490 --> 00:14:26,698
Still got the grapes then?

253
00:14:26,783 --> 00:14:28,450
Oh, yeah. Keep 'em for later.

254
00:14:29,202 --> 00:14:31,787
You tell her to
take the pips out.
They're bad for the appendix.

255
00:14:32,080 --> 00:14:33,038
Ta-ta.

256
00:14:33,122 --> 00:14:34,289
Bye. Thanks again for the tip.

257
00:14:34,582 --> 00:14:36,625
I told you,
Bob's your uncle.

258
00:14:39,128 --> 00:14:41,213
20-to-1!

259
00:14:41,297 --> 00:14:45,425
20-to-bloody-1!
Christ! Damn it to hell!

260
00:15:16,499 --> 00:15:17,457
BARLING:
Well, my dears,

261
00:15:17,542 --> 00:15:20,586
I'm sure I can say
on behalf of Mrs. Blaney
as well as myself,

262
00:15:20,670 --> 00:15:22,129
that it's moments
like this that make

263
00:15:22,213 --> 00:15:24,047
all our little efforts
here so worthwhile.

264
00:15:24,132 --> 00:15:26,091
You mean,
you just don't
do it for the money?

265
00:15:26,175 --> 00:15:27,175
(LAUGHING)

266
00:15:27,844 --> 00:15:29,845
Of course,
Mrs. Davisson,
this is a business,

267
00:15:29,929 --> 00:15:32,014
and financial
considerations prevail.

268
00:15:33,016 --> 00:15:36,852
But our ultimate satisfaction
is the making happy
of two lonely people.

269
00:15:37,228 --> 00:15:40,188
Nice of you, Miss Barling.
Keep up the good work!

270
00:15:40,940 --> 00:15:43,275
Well, it's up to us now,
I guess, eh?

271
00:15:43,359 --> 00:15:45,319
Yeah. And good-bye,
Mr. Salt.

272
00:15:45,987 --> 00:15:47,863
Bye, Miss Barling.
Thank you.

273
00:15:48,156 --> 00:15:51,450
It's been our pleasure.
And I know you'll
both be very happy.

274
00:15:52,160 --> 00:15:54,369
After all, I know you're
both mad about beekeeping.

275
00:15:54,454 --> 00:15:55,912
And there's nothing
like a shared interest.

276
00:15:56,039 --> 00:16:00,834
I'm sure we will be.
Come on, Neville.
Best foot forward.

277
00:16:00,918 --> 00:16:02,502
SALT: I suppose
we should go straight

278
00:16:02,587 --> 00:16:04,171
and get the marriage
license, my dear.

279
00:16:04,380 --> 00:16:06,298
What's your rush?

280
00:16:06,799 --> 00:16:09,343
Let's go to my place first.

281
00:16:09,427 --> 00:16:12,137
Did you know, Neville,
that my late husband,
Mr. Davisson,

282
00:16:12,221 --> 00:16:15,223
was up at 5:30 every
morning of his life?

283
00:16:15,308 --> 00:16:17,476
And by the time
he brought me
my cup of tea,

284
00:16:17,560 --> 00:16:19,645
which he did
punctually at 9: 15,

285
00:16:19,729 --> 00:16:23,065
he would've
cleaned the whole house,
and he was so quiet about it,

286
00:16:23,358 --> 00:16:25,734
that in 14 years,
he never woke me once.

287
00:16:25,818 --> 00:16:27,653
Not once!

288
00:16:27,737 --> 00:16:30,155
Oh, a neat man,
was he, then?

289
00:16:30,239 --> 00:16:32,157
He liked a tidy place.

290
00:16:32,241 --> 00:16:33,950
So do I,
come to that.

291
00:16:34,911 --> 00:16:37,079
Dandruff.
We'll have to get you
something for that.

292
00:16:44,671 --> 00:16:46,463
Afternoon.

293
00:16:48,257 --> 00:16:50,258
You're new here,
aren't you?

294
00:16:51,719 --> 00:16:55,347
I've been here
for over a year now.
What can I do for you, sir?

295
00:16:55,431 --> 00:16:56,640
You can inform Mrs. Blaney

296
00:16:56,724 --> 00:16:58,934
that one of her less
successful exercises

297
00:16:59,018 --> 00:17:00,477
in matrimony has
come to see her.

298
00:17:00,561 --> 00:17:02,354
And who shall
I say is calling?

299
00:17:02,438 --> 00:17:03,730
Mr. Blaney.

300
00:17:03,981 --> 00:17:06,441
Or if you preferred it,
ex-Squadron Leader Blaney,

301
00:17:06,526 --> 00:17:09,403
late of the RAF and
Mrs. Blaney's
matrimonial bed.

302
00:17:09,487 --> 00:17:13,198
I see. Is Mrs. Blaney
expecting you?

303
00:17:13,282 --> 00:17:14,491
She must be.

304
00:17:14,575 --> 00:17:18,036
Everybody expects
a bad penny to turn up
sooner or later.

305
00:17:19,956 --> 00:17:23,709
Mrs. Blaney,
there's a Mr. Blaney
to see you.

306
00:17:23,793 --> 00:17:27,629
BRENDA: (ON SPEAKER)
Mr. Blaney? Send him in,
will you please, Monica?

307
00:17:33,302 --> 00:17:34,302
Hello, Brenda.

308
00:17:34,387 --> 00:17:35,887
Hello, Richard.

309
00:17:37,390 --> 00:17:38,807
What are you doing here?

310
00:17:39,434 --> 00:17:41,435
I just thought
I'd call around.

311
00:17:42,687 --> 00:17:44,604
Well, come in.
Take a seat.

312
00:17:44,689 --> 00:17:45,731
Thanks.

313
00:17:48,609 --> 00:17:49,651
It's good to see you.

314
00:17:50,778 --> 00:17:53,113
You too. You look fine.

315
00:17:54,323 --> 00:17:57,117
I'll be with you in a minute.
I've got to finish
writing up a few notes.

316
00:18:00,830 --> 00:18:02,330
How is everything
then, Brenda?

317
00:18:02,707 --> 00:18:03,832
You making a fortune?

318
00:18:03,916 --> 00:18:05,167
Things are going very well.

319
00:18:06,419 --> 00:18:08,712
Streets of London
thronged with lonely hearts

320
00:18:08,796 --> 00:18:10,046
beating a path
to your door?

321
00:18:10,131 --> 00:18:11,298
That's it.

322
00:18:12,967 --> 00:18:14,092
I'm amazed that in an age

323
00:18:14,177 --> 00:18:17,012
where practically everybody
considers marriage is hell

324
00:18:17,096 --> 00:18:18,597
that you can find
any clients.

325
00:18:18,681 --> 00:18:20,766
Okay, Richard,
if you've just come
around here to insult me,

326
00:18:20,850 --> 00:18:21,850
I think, perhaps you...

327
00:18:21,934 --> 00:18:23,268
I'm not insulting you,
for Christ's sake!

328
00:18:23,352 --> 00:18:25,103
Will you please
lower your voice.

329
00:18:25,188 --> 00:18:26,271
Why?

330
00:18:26,355 --> 00:18:29,441
I don't care if
Vinegar Joe out there
does hear me.

331
00:18:29,525 --> 00:18:31,610
Why don't you
get her married off,
by the way?

332
00:18:31,694 --> 00:18:34,613
Preferably
to a 700-pound
Japanese wrestler.

333
00:18:34,697 --> 00:18:37,282
That should iron out
some of her creases
a little.

334
00:18:38,951 --> 00:18:41,203
Monica, dear,
it's nearly 4:30.

335
00:18:41,287 --> 00:18:42,746
Why don't you take
the rest of the afternoon off?

336
00:18:42,830 --> 00:18:45,123
I'm sure there's
some shopping
you'd like to do.

337
00:18:45,208 --> 00:18:47,125
Well, thank you, Mrs. Blaney,

338
00:18:47,210 --> 00:18:48,710
if you're sure
you don't need me.

339
00:18:49,045 --> 00:18:51,296
I'm quite sure, thank you.
Good night.

340
00:18:56,344 --> 00:18:57,928
BRENDA: Why do you
always come to see me

341
00:18:58,012 --> 00:18:59,554
when you've had
too much to drink?

342
00:18:59,639 --> 00:19:02,265
BLANEY: I don't always come
to see you when I've had
too much to drink.

343
00:19:02,350 --> 00:19:04,142
As a matter of fact,
I don't always
come to see you.

344
00:19:04,227 --> 00:19:06,102
I haven't seen you
for over a year.

345
00:19:06,187 --> 00:19:09,356
You were half seas
over then, and violent.

346
00:19:09,649 --> 00:19:11,233
I do hope we're not going
to have a repeat of that.

347
00:19:11,317 --> 00:19:12,484
Me violent?

348
00:19:12,568 --> 00:19:14,110
Don't be bloody ridiculous.

349
00:19:14,195 --> 00:19:16,488
Would I raise a hand
to the goddess of love?

350
00:19:16,572 --> 00:19:20,534
What Brenda Blaney
brings together
let no man put asunder!

351
00:19:21,744 --> 00:19:24,579
I didn't say
you were violent to me.

352
00:19:25,414 --> 00:19:27,499
But you certainly
acted the fool

353
00:19:27,583 --> 00:19:29,709
and threw the furniture
about a bit.

354
00:19:37,134 --> 00:19:39,970
Just look at
the state you're in.

355
00:19:40,054 --> 00:19:41,137
Really!

356
00:19:41,222 --> 00:19:42,514
Oh, leave me alone.

357
00:19:42,598 --> 00:19:44,474
Bachelors are supposed
to be untidy, aren't they?

358
00:19:44,559 --> 00:19:49,187
I thought tidiness
was most women's dowry,
or don't you preach that here?

359
00:19:49,272 --> 00:19:51,857
Oh, we are bitter today.

360
00:19:52,775 --> 00:19:54,192
What's the matter?

361
00:19:54,277 --> 00:19:55,569
I'm sorry.

362
00:19:56,445 --> 00:19:59,656
I had a bad day,
that's all.
I lost my job.

363
00:20:00,783 --> 00:20:01,783
How?

364
00:20:02,702 --> 00:20:03,827
Well, I got fired, that's how.

365
00:20:03,911 --> 00:20:05,245
What do you think,
I mislaid it?

366
00:20:06,455 --> 00:20:08,290
For pinching
a glass of brandy.

367
00:20:09,625 --> 00:20:12,043
My employer thought
I wasn't going to put
the money in the till.

368
00:20:12,461 --> 00:20:13,461
Till?

369
00:20:14,714 --> 00:20:17,507
I was working,
until this morning
that is, as a barman.

370
00:20:18,801 --> 00:20:21,094
And another thing.
I was given
a very good horse

371
00:20:21,178 --> 00:20:24,180
by a great friend of mine,
one Bob Rusk.

372
00:20:24,265 --> 00:20:25,515
And it came in at 20-to-1,

373
00:20:25,600 --> 00:20:27,601
and I didn't have
enough cash on me
to back it.

374
00:20:29,312 --> 00:20:30,812
I'm sorry.

375
00:20:33,566 --> 00:20:35,609
Well, these things
always go in threes.

376
00:20:35,693 --> 00:20:37,861
I wonder what the rest
of the day has in store.

377
00:20:38,988 --> 00:20:40,196
Dinner with me, I hope.

378
00:20:44,619 --> 00:20:47,078
Well, that, of course,
would be delightful, but...

379
00:20:47,163 --> 00:20:48,371
I mean, of course, on me.

380
00:20:49,415 --> 00:20:50,665
We'll go to my club.

381
00:20:51,959 --> 00:20:54,586
But I must finish
these letters first.

382
00:20:54,670 --> 00:20:57,714
Here's the address,
in case you've forgotten it.

383
00:20:57,798 --> 00:21:01,760
How will it be
if we meet there
'round about 7:30?

384
00:21:01,844 --> 00:21:03,011
Fine.

385
00:21:04,347 --> 00:21:07,766
Thanks for a lovely evening.
It was great. I mean that.

386
00:21:07,850 --> 00:21:09,935
Thanks for joining me.

387
00:21:10,019 --> 00:21:13,063
It was a damn sight
better than
the leftovers at the Globe,

388
00:21:13,147 --> 00:21:15,231
not that I'm in for
any more of those,
of course.

389
00:21:15,316 --> 00:21:16,900
You ought to get
married again, Richard.

390
00:21:17,318 --> 00:21:18,526
Oh, no.

391
00:21:18,945 --> 00:21:21,738
You ought to know
I'm no good at it.
How long did we have?

392
00:21:22,323 --> 00:21:24,449
Nine years, was it?

393
00:21:24,533 --> 00:21:25,784
Ten.
Ah.

394
00:21:26,410 --> 00:21:27,911
Ten years.

395
00:21:27,995 --> 00:21:29,496
It was a good job
you got out when you did.

396
00:21:29,705 --> 00:21:31,915
I don't know.

397
00:21:31,999 --> 00:21:34,376
I suppose
I was lucky the agency
worked out for me.

398
00:21:34,460 --> 00:21:35,585
What you mean is,
that you're lucky

399
00:21:35,670 --> 00:21:37,629
you haven't had to rely on me
these past two years.

400
00:21:37,713 --> 00:21:38,672
I didn't say that.

401
00:21:38,756 --> 00:21:39,881
But you meant it.

402
00:21:40,383 --> 00:21:42,217
Look, Richard,
I suppose some people

403
00:21:42,301 --> 00:21:44,344
are good at organization
and others aren't.

404
00:21:44,637 --> 00:21:46,137
That's all I meant.

405
00:21:46,222 --> 00:21:47,639
And I'm not, I suppose.

406
00:21:47,723 --> 00:21:50,976
Rubbish! You know
what filthy luck I had.

407
00:21:51,060 --> 00:21:52,852
Was it my fault that
the roadhouse didn't go?

408
00:21:52,937 --> 00:21:53,937
It was going fine

409
00:21:54,021 --> 00:21:55,689
till they opened
the bloody motorway.
I know.

410
00:21:55,773 --> 00:21:57,941
Was it my fault
the council tore down
the riding stables?

411
00:21:58,025 --> 00:22:00,485
I know. I know life
can be very unfair.

412
00:22:01,570 --> 00:22:03,238
But you never
used to be
sorry for yourself.

413
00:22:03,906 --> 00:22:06,491
Where's the Richard Blaney
I married?

414
00:22:06,575 --> 00:22:09,703
Richard Blaney, EFC.
Do you remember
the citation?

415
00:22:10,538 --> 00:22:15,542
"For inspiring
leadership, skill,
and tenacity of purpose."

416
00:22:15,626 --> 00:22:17,794
You divorced him.
That's what happened to him.

417
00:22:17,878 --> 00:22:18,920
Now, you tell me,
what sort of skill

418
00:22:19,005 --> 00:22:21,673
do you need to
deal with shopkeepers
and interfering bureaucrats?

419
00:22:21,757 --> 00:22:23,425
(SHUSHING)
Everyone's listening.

420
00:22:23,759 --> 00:22:25,010
Well, let them.

421
00:22:25,094 --> 00:22:26,469
I'll bet they've
never had to tear down

422
00:22:26,554 --> 00:22:28,096
their own livelihood
with their own hands.

423
00:22:28,806 --> 00:22:30,015
It's all right for you.

424
00:22:30,099 --> 00:22:32,017
You just go
and build it up
somewhere else.

425
00:22:32,101 --> 00:22:34,227
You're like that.
You're good at business.

426
00:22:34,311 --> 00:22:36,730
I'll bet they're all
good at business here.

427
00:22:36,814 --> 00:22:39,190
I'll bet you're
making a fortune
out of that agency.

428
00:22:39,275 --> 00:22:42,110
And why not? If you
can't make love, sell it.

429
00:22:42,737 --> 00:22:45,155
The respectable kind,
of course.
The married kind!

430
00:22:47,116 --> 00:22:48,408
Now look what you've done.

431
00:22:48,492 --> 00:22:50,493
Oh, sir.
Let me help you.

432
00:22:50,578 --> 00:22:52,203
Leave me alone!

433
00:22:58,544 --> 00:23:02,005
I'm sorry.
I didn't mean that.

434
00:23:03,340 --> 00:23:05,759
Come on. We'd better go.

435
00:23:06,135 --> 00:23:08,094
Will you be all right?

436
00:23:08,637 --> 00:23:11,765
You did say you
hadn't enough money
to put on that horse.

437
00:23:11,849 --> 00:23:14,893
Don't worry.
You've done
enough for one day.

438
00:23:31,494 --> 00:23:32,911
You had a coat too,
didn't you?

439
00:23:32,995 --> 00:23:34,662
Yeah. That one there.

440
00:23:35,039 --> 00:23:36,831
Mine's the pink one.

441
00:23:37,500 --> 00:23:40,585
No, no, no.
That's not allowed.

442
00:23:40,669 --> 00:23:42,629
Rules of the club.

443
00:23:46,842 --> 00:23:48,593
Thank you.

444
00:24:01,148 --> 00:24:04,526
It's all right, Richard.
I've got it.
Here. Thank you.

445
00:24:04,610 --> 00:24:06,945
So, this is it, huh?

446
00:24:07,863 --> 00:24:09,739
Can I come in?

447
00:24:09,824 --> 00:24:11,991
I'd like to see
where you live.

448
00:24:12,076 --> 00:24:15,328
Go along now.
Run along home.
It's late.

449
00:24:15,412 --> 00:24:16,788
You call this late?

450
00:24:16,872 --> 00:24:18,706
It's late enough
for a working girl.

451
00:24:19,208 --> 00:24:23,128
Come on, Brenda.
I won't be long,
I promise.

452
00:24:23,212 --> 00:24:27,340
All right, but only
for a few minutes, mind.

453
00:24:27,424 --> 00:24:29,384
Okay, thanks.
I won't need you.

454
00:24:38,185 --> 00:24:39,185
(MAN 1 SNORING)

455
00:24:40,729 --> 00:24:41,729
(MAN 2 COUGHING)

456
00:25:02,793 --> 00:25:05,503
It just fell out of
your pocket, guvnor,
onto the floor.

457
00:25:05,588 --> 00:25:09,174
And I was just
putting it back
when you were asleep.

458
00:25:09,258 --> 00:25:12,468
It sort of jerked out,
like, on the floor.

459
00:25:12,553 --> 00:25:15,555
I was putting 'em
in a bit quiet, like,
so as not to waken you.

460
00:25:15,639 --> 00:25:18,516
You keep your hands
out of my pockets,
or I'll break your arms!

461
00:25:18,601 --> 00:25:21,227
Honestly, there's
nothing I detest

462
00:25:21,312 --> 00:25:23,563
more than someone
taking liberties

463
00:25:23,647 --> 00:25:26,107
with a fine gentleman
like yourself.

464
00:25:39,121 --> 00:25:40,997
Oh, it's you again,
Mr. Robinson.

465
00:25:41,123 --> 00:25:43,708
Yes, I'm afraid so.

466
00:25:44,251 --> 00:25:46,544
Well, I'm having
my lunch just now.

467
00:25:46,629 --> 00:25:47,587
If you want
an appointment,

468
00:25:47,671 --> 00:25:49,380
perhaps you'll be
good enough
to see my secretary.

469
00:25:51,217 --> 00:25:52,550
By the way,
how did you get in?

470
00:25:52,635 --> 00:25:54,510
Oh, no problem, really.

471
00:25:55,262 --> 00:25:57,055
Just a question
of using your head.

472
00:25:59,183 --> 00:26:02,727
I waited in the courtyard
till I saw her
go out to lunch.

473
00:26:03,812 --> 00:26:05,230
It all seems
a bit elaborate.

474
00:26:05,314 --> 00:26:07,649
Yeah, perhaps, but...

475
00:26:10,110 --> 00:26:12,320
You're the one
I wanted to see.

476
00:26:14,073 --> 00:26:16,908
I thought I'd already
explained to you that
we cannot help you.

477
00:26:16,992 --> 00:26:19,410
Oh, come on now.

478
00:26:19,495 --> 00:26:23,122
I know that you can be
most helpful when you try.

479
00:26:23,207 --> 00:26:26,376
Look, Mr. Robinson,
you want women of
a specific type.

480
00:26:27,670 --> 00:26:28,836
How shall I put it?

481
00:26:29,255 --> 00:26:31,339
Certain peculiarities
appeal to you,

482
00:26:31,423 --> 00:26:34,008
and you need women
to submit to them.

483
00:26:34,843 --> 00:26:37,845
Here we have, I'm afraid,
a very normal clientele.

484
00:26:38,305 --> 00:26:39,973
As I say,
we can do nothing for you.

485
00:26:40,557 --> 00:26:42,392
And now if
you'll kindly let me
get on with my lunch.

486
00:26:42,476 --> 00:26:45,311
I don't think
you're really trying
your best for me.

487
00:26:46,689 --> 00:26:50,525
I mean, if you can fix up
a lot of idiots,
then why not me? Hmm?

488
00:26:51,986 --> 00:26:54,320
I've explained.
You're different.

489
00:26:54,697 --> 00:26:55,822
How so?

490
00:26:56,865 --> 00:26:58,199
I have my good points.

491
00:26:59,618 --> 00:27:01,703
I like flowers and...

492
00:27:03,664 --> 00:27:05,290
Fruit.

493
00:27:06,250 --> 00:27:08,042
People like me.

494
00:27:09,378 --> 00:27:10,878
I've got things to give.

495
00:27:11,171 --> 00:27:12,797
I'm sorry.

496
00:27:13,674 --> 00:27:17,051
I thought matrimonial agencies
were supposed to
bring people together?

497
00:27:17,720 --> 00:27:19,053
Not people like you.

498
00:27:20,389 --> 00:27:22,390
Somehow I don't think
our clients would appreciate

499
00:27:22,474 --> 00:27:24,434
your conception of
a loving relationship.

500
00:27:29,857 --> 00:27:32,317
You're not the only
matrimonial agency,
you know.

501
00:27:32,401 --> 00:27:33,484
Then go elsewhere.

502
00:27:34,069 --> 00:27:36,070
Not that any
reputable agency
would service you.

503
00:27:36,155 --> 00:27:37,238
I've been elsewhere.

504
00:27:38,657 --> 00:27:40,491
But...

505
00:27:40,576 --> 00:27:43,786
This one, for me, is the best,

506
00:27:43,871 --> 00:27:47,332
because...

507
00:27:47,416 --> 00:27:48,708
I like you.

508
00:27:53,213 --> 00:27:56,257
You're my type of woman.

509
00:27:59,303 --> 00:28:00,720
Don't be ridiculous.

510
00:28:00,804 --> 00:28:02,388
I'm serious.

511
00:28:04,141 --> 00:28:08,436
I respect a woman like you,
and I know how to
treat you as well.

512
00:28:12,524 --> 00:28:15,777
You know, in my trade...

513
00:28:15,861 --> 00:28:17,028
We have a saying.

514
00:28:17,112 --> 00:28:18,905
We put it on the fruit.

515
00:28:19,698 --> 00:28:22,617
"Don't squeeze the goods
till they're yours."

516
00:28:23,285 --> 00:28:25,453
Now, that's me.

