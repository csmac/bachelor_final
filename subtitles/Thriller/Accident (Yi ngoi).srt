1
00:00:00,000 --> 00:00:01,959
<br><i>Episode 30

2
00:00:02,995 --> 00:00:04,846
<br>Take care, daughter.

3
00:00:05,749 --> 00:00:07,521
Brother Gou, take care!

4
00:00:07,521 --> 00:00:08,724
Take care.

5
00:00:08,724 --> 00:00:10,730
Take care my patrons.<br>Take care.

6
00:00:10,730 --> 00:00:11,666
With the livelihood you gave us

7
00:00:11,666 --> 00:00:16,050
we can obtain about 20,000 a year from those greedy rich officials.

8
00:00:16,050 --> 00:00:18,558
Then we split it with the commoners in Jiang Dong.

9
00:00:18,558 --> 00:00:21,269
In recent years, the name of your Bao Zi clan

10
00:00:21,269 --> 00:00:22,870
has almost caught up to us, Yi Zhi Mei.

11
00:00:22,870 --> 00:00:24,508
You're too modest brother Li.

12
00:00:24,508 --> 00:00:26,346
Take care!<br>Take care.

13
00:00:26,346 --> 00:00:28,251
Take care.<br>Take care.

14
00:00:29,856 --> 00:00:31,232
I'll take my leave.<br>Take care.

15
00:00:34,451 --> 00:00:35,142
Shi-

16
00:00:51,661 --> 00:00:53,266
What do you mean by this?

17
00:00:54,068 --> 00:00:55,171
What do you mean what do I mean?

18
00:00:55,171 --> 00:00:56,107
You're still denying it?

19
00:00:56,475 --> 00:00:59,917
My mother said she wants me to go back to E Mei to succeed her position.

20
00:01:01,722 --> 00:01:04,202
She wants you to go back to succeed. That's very good.

21
00:01:04,202 --> 00:01:04,769
You should go back..

22
00:01:04,769 --> 00:01:06,363
You want to break up Yi Zhi Mei?

23
00:01:06,363 --> 00:01:07,109
If I leave

24
00:01:07,109 --> 00:01:08,778
then Yi Zhi Mei will no longer be four people.

25
00:01:09,428 --> 00:01:10,686
Yea, it'll be three.

26
00:01:11,278 --> 00:01:12,625
Say it again.

27
00:01:15,504 --> 00:01:17,137
Your mother has been waiting her whole life.

28
00:01:17,137 --> 00:01:18,748
She's been waiting for you to go back to be with her.

29
00:01:18,748 --> 00:01:19,459
What do you mean?

30
00:01:19,459 --> 00:01:21,688
What do you mean what do I mean?<br>It's annoying.

31
00:01:22,756 --> 00:01:25,867
Last time, at E Mei...

32
00:01:25,867 --> 00:01:29,568
You told my mother that I'm your wife.

33
00:01:29,568 --> 00:01:30,929
What do you mean?

34
00:01:30,929 --> 00:01:34,366
In that situation, of course it's fake.<br>You know it very well.

35
00:01:35,079 --> 00:01:36,200
Fake...

36
00:01:38,817 --> 00:01:41,796
Right... it's all fake.

37
00:01:43,700 --> 00:01:47,369
You said that this is our home. Is that fake too?

38
00:01:49,738 --> 00:01:51,781
Did I think too much?

39
00:01:53,810 --> 00:01:56,889
Or is everything you said... all fake?!

40
00:02:00,683 --> 00:02:05,620
Your mother is waiting for you. Don't let her down. Go, hurry!

41
00:02:09,985 --> 00:02:14,258
Li Ge Xiao, you sure are sly.

42
00:02:22,967 --> 00:02:26,125
(Letter from Ying Wu Qiu: He kidnapped Hai Rui)

43
00:02:38,857 --> 00:02:40,311
What do you mean?

44
00:02:40,311 --> 00:02:42,022
What do you mean what do I mean?

45
00:02:42,022 --> 00:02:45,837
My mother said she wants me to go back to E Mei to succeed her position.

46
00:02:47,262 --> 00:02:50,760
She wants you to go back to succeed. That's good.<br>You should go.

47
00:02:50,760 --> 00:02:52,756
Say it again.

48
00:02:54,707 --> 00:02:56,836
Your mother has been waiting her whole life.

49
00:02:56,836 --> 00:02:58,933
She's been waiting for you to go back to be with her.

50
00:03:00,732 --> 00:03:01,534
San Niang!

51
00:03:01,534 --> 00:03:03,740
Don't be angry with ge ge.

52
00:03:03,740 --> 00:03:06,988
I'm not angry. I'm thinking he must be hiding something from me.

53
00:03:08,894 --> 00:03:10,498
He must have a reason to do this.

54
00:03:10,498 --> 00:03:13,139
I think ge ge is looking for an excuse to feel better.

55
00:03:13,139 --> 00:03:14,843
This counts as a good thing I guess.

56
00:03:16,047 --> 00:03:18,754
Ah, right (3x). I've experienced been hurt because of love.

57
00:03:18,754 --> 00:03:20,893
One should find a good method to heal from it.

58
00:03:20,893 --> 00:03:22,571
Can you two be more serious?

59
00:03:22,571 --> 00:03:23,774
When he can't solve a problem,

60
00:03:23,774 --> 00:03:25,380
he always tries to be composed.

61
00:03:25,380 --> 00:03:27,719
The more composed he is, the bigger the problem.

62
00:03:27,719 --> 00:03:28,624
You don't mean...

63
00:03:28,624 --> 00:03:31,061
that the problem this time is so serious that he must push you away?

64
00:03:31,061 --> 00:03:32,900
Correct. let's think this through fast.

65
00:03:32,900 --> 00:03:35,005
Why would he do this?<br>Not letting us help.

66
00:03:35,713 --> 00:03:38,268
I think it's best to ask him directly.

67
00:03:38,268 --> 00:03:39,986
If he'd tell us then he won't do this in the first place.

68
00:03:41,093 --> 00:03:43,004
Maybe he's met his match; a master of martial arts.

69
00:03:43,004 --> 00:03:45,273
That person wants to battle him, so he's afraid and doesn't want to drag you down.

70
00:03:45,273 --> 00:03:46,998
If you leave, then he can die without worries.

71
00:03:46,998 --> 00:03:49,885
Hu ge, how come I feel like you're right this time.

72
00:03:50,428 --> 00:03:52,200
Who do you guys think it is?

73
00:03:52,200 --> 00:03:55,218
It's impossible. Yan Song is long gone.

74
00:03:55,218 --> 00:03:57,029
That bastard Ying Wu Qiu

75
00:03:57,029 --> 00:03:58,346
has already been exiled.

76
00:03:58,346 --> 00:04:01,697
Even if the Turkics don't kill him, they'll at least cut off one of his legs or arms.

77
00:04:01,697 --> 00:04:03,786
Did you guess that, or are you sure?

78
00:04:03,786 --> 00:04:04,894
And also,

79
00:04:04,894 --> 00:04:06,991
it's not the first time we go against Ying Wu Qiu.

80
00:04:06,991 --> 00:04:09,870
His skills are far from ge ge's.

81
00:04:13,167 --> 00:04:15,211
Brother Zhang, I'll leave him to you for now.

82
00:04:15,211 --> 00:04:16,603
No problem.

83
00:04:16,603 --> 00:04:19,150
Brother Li, leave him to me. Go save your people.

84
00:04:19,150 --> 00:04:22,223
Bao Lai Ying, when we meet again,

85
00:04:23,059 --> 00:04:27,238
either you die or I die.

86
00:04:28,578 --> 00:04:31,272
Li Ge Xiao, don't you dare leave! don't leave!

87
00:04:31,272 --> 00:04:33,879
Come back! Li Ge Xiao, don't leave!

88
00:04:33,879 --> 00:04:35,876
Get back here!

89
00:05:00,854 --> 00:05:03,508
Young master, if we keep dragging him like this, he'll die.

90
00:05:03,508 --> 00:05:05,969
Brother Li has told us not to kill him.

91
00:05:05,969 --> 00:05:09,777
We are bandits, not philanthropists.

92
00:05:09,777 --> 00:05:11,814
How many things have done to us,

93
00:05:11,814 --> 00:05:14,143
and how many of our brothers have died in their hands?

94
00:05:14,143 --> 00:05:16,511
If we just let him go back like this,

95
00:05:16,511 --> 00:05:18,044
how can I face my brothers?

96
00:05:18,044 --> 00:05:21,016
If we let me go, I'll agree to anything.

97
00:05:21,016 --> 00:05:23,431
I'll give you whatever you want.

98
00:05:23,431 --> 00:05:24,824
if i helped

99
00:05:24,824 --> 00:05:28,500
you guys can become the world's strongest clan.

100
00:05:28,500 --> 00:05:31,086
Oh Ying Wu Qiu...

101
00:05:31,086 --> 00:05:33,648
you can never surpass Li Ge Xiao.

102
00:05:33,648 --> 00:05:36,202
And the reason is because you're like this.

103
00:05:36,202 --> 00:05:37,874
i dont believe it

104
00:05:37,874 --> 00:05:40,428
I don't believe that you won't be moved by silver and gold.

105
00:05:40,428 --> 00:05:43,028
Someone like you will never understand.

106
00:05:43,028 --> 00:05:45,668
There's something in this world

107
00:05:45,668 --> 00:05:48,130
that can not be surpassed even by the most alluring of things.

108
00:05:48,130 --> 00:05:50,366
I promised Li Ge Xiao not to kill you,

109
00:05:50,366 --> 00:05:55,746
but I still have to get revenge for my brothers.

110
00:05:56,859 --> 00:05:59,765
We, Bao Zi Bang, has been around many years,

111
00:05:59,765 --> 00:06:02,172
and so we've made some foreign friends.

112
00:06:02,172 --> 00:06:05,414
If you can defeat them and stay alive, then you're lucky!

113
00:06:05,414 --> 00:06:06,784
lets go

114
00:06:07,960 --> 00:06:09,267
Go

115
00:06:11,431 --> 00:06:14,244
stop running

116
00:06:14,244 --> 00:06:16,216
chase him

117
00:06:19,191 --> 00:06:22,834
dont run

118
00:06:22,834 --> 00:06:25,709
Look over there!<Br>Where is he?

119
00:06:32,851 --> 00:06:35,944
Let's look on that side!<br>Ok! Let's search!

120
00:06:39,052 --> 00:06:40,122
Go over there!

121
00:06:50,691 --> 00:06:52,027
He's over here!

122
00:06:53,935 --> 00:06:55,660
Stop!<br>Stay right there!

123
00:07:11,304 --> 00:07:14,329
Don't kill me! don't kill me!

124
00:07:14,329 --> 00:07:15,769
Listen to what I have to say!

125
00:07:16,269 --> 00:07:17,557
You guys want to attack China

126
00:07:17,557 --> 00:07:19,640
and wipe us out right?<br>I have an idea!

127
00:07:19,640 --> 00:07:21,458
What you're saying is alluring.

128
00:07:25,630 --> 00:07:26,798
With my status,

129
00:07:26,798 --> 00:07:29,910
you guys can attack our borders with your soldiers.

130
00:07:29,910 --> 00:07:32,418
Then the fall of the country will be soon.

131
00:07:33,339 --> 00:07:34,461
It'll be soon?

132
00:07:37,837 --> 00:07:39,523
Did someone follow you?

133
00:07:39,523 --> 00:07:42,634
You don't have to worry about the things I planned.

134
00:07:52,247 --> 00:07:54,895
This... isn't this a map of the border stations?

135
00:07:56,281 --> 00:07:57,960
You invited the Turkics?!

136
00:07:57,960 --> 00:08:00,367
Your family no longer has a supporter,

137
00:08:00,367 --> 00:08:02,178
so your only option is to find another one.

138
00:08:02,178 --> 00:08:04,647
But you're asking us to betray our country!

139
00:08:04,647 --> 00:08:07,062
This is your field of expertise.

140
00:08:08,076 --> 00:08:09,655
Which one did you invite?

141
00:08:09,655 --> 00:08:11,652
Ha La's people has assaulted our country for many years.

142
00:08:11,652 --> 00:08:14,492
I'm guessing you already know of them.

143
00:08:14,492 --> 00:08:16,536
General Ha La

144
00:08:20,097 --> 00:08:21,628
You're dragging us to hell!

145
00:08:21,628 --> 00:08:25,800
My son... this could be our way out.

146
00:08:25,800 --> 00:08:27,890
Are you dumb from old age?!

147
00:08:27,890 --> 00:08:30,398
The Turkics despise us Chinese.

148
00:08:30,398 --> 00:08:32,488
And they hate traitors even more!

149
00:08:32,488 --> 00:08:34,292
Even if we sell the map to them,

150
00:08:34,292 --> 00:08:35,971
they won't care about us!

151
00:08:35,971 --> 00:08:38,703
Besides, even if you're lucky to live after that,

152
00:08:38,703 --> 00:08:41,357
you can only become their slave.<br>Do you understand, father?!

153
00:08:42,658 --> 00:08:45,537
What... how can you do this to us?!

154
00:08:52,171 --> 00:08:53,357
You're right.

155
00:08:53,357 --> 00:08:55,382
The Turkics has always looked down at us Chinese.

156
00:08:55,382 --> 00:08:58,812
What you have to do is just to take this map

157
00:08:58,812 --> 00:09:01,187
to them and let them attack the borders right away!

158
00:09:01,187 --> 00:09:02,395
Then...

159
00:09:02,395 --> 00:09:04,438
if the Ming Dynasty is gone...

160
00:09:06,057 --> 00:09:07,728
What's it to you?

161
00:09:07,728 --> 00:09:09,500
Don't worry about me.

162
00:09:09,500 --> 00:09:11,397
Just use your clever brain

163
00:09:11,397 --> 00:09:14,748
to convince Ha La and keep your life.

164
00:09:21,166 --> 00:09:22,436
He's here.

165
00:09:28,220 --> 00:09:29,991
You... what are you doing?!

166
00:09:39,865 --> 00:09:44,536
I heard that someone wants to sell their country.

167
00:09:45,474 --> 00:09:51,963
That someone is actually... the great Yan who was all high and mighty.

168
00:09:54,203 --> 00:09:56,576
We're all old acquaintances,

169
00:09:56,576 --> 00:09:58,916
so if you have something to say, say it.

170
00:09:58,916 --> 00:10:01,155
Of course, minister Yan.

171
00:10:01,155 --> 00:10:03,662
All these years... you've embezzled quite a lot from the military funds.

172
00:10:03,662 --> 00:10:06,810
Also all those clever political essays you presented that neglected the frontier defense

173
00:10:06,810 --> 00:10:10,035
has caused the country to fall into chaos.

174
00:10:10,035 --> 00:10:11,523
My emperor

175
00:10:11,523 --> 00:10:15,166
really wants to record your glorious achievements

176
00:10:15,166 --> 00:10:19,545
in our great Han country's history books when we take control of the world.

177
00:10:21,462 --> 00:10:23,395
<br>What a fuss over nothing.

178
00:10:24,532 --> 00:10:25,917
What you want is

179
00:10:25,917 --> 00:10:28,518
the other half of the map, right?

180
00:10:28,518 --> 00:10:29,818
Minister Yan,

181
00:10:29,818 --> 00:10:34,091
your son, Shi Fan, has a way with his words.

182
00:10:34,861 --> 00:10:35,902
Yeah.

183
00:10:39,078 --> 00:10:41,010
You openly buy and I'll openly sell,

184
00:10:41,010 --> 00:10:45,793
but you should watch that mouth of yours.

185
00:10:47,483 --> 00:10:49,183
I'll tell you guys right now,

186
00:10:49,183 --> 00:10:52,527
you guys are nothing without us!

187
00:10:54,449 --> 00:10:56,289
And I'll also tell you,

188
00:10:56,289 --> 00:10:58,657
without the other half of the map,

189
00:10:58,657 --> 00:11:01,211
you guys will never be able to invade the capital city.

190
00:11:03,248 --> 00:11:04,787
Minister Yan,

191
00:11:04,787 --> 00:11:08,828
you should be glad to have a son like him.

192
00:11:08,828 --> 00:11:10,360
Of course.

193
00:11:10,360 --> 00:11:14,865
I want the other half of the map, so just name your price.

194
00:11:16,622 --> 00:11:21,034
100 taels of gold, 100 beauties, and a safe escape.

195
00:11:21,034 --> 00:11:22,086
Deal.

196
00:11:22,985 --> 00:11:26,675
What if Ying Wu Qiu holds something really important to threaten him?

197
00:11:27,520 --> 00:11:29,795
Stop guessing, stop guessing!

198
00:11:29,795 --> 00:11:32,953
My instinct tells me that the problem isn't solved yet.

199
00:11:32,953 --> 00:11:34,904
I'll go discuss this matter with official Hai.<br>Ok.

200
00:11:49,300 --> 00:11:51,761
Odd... how come all the lights are off?

201
00:12:16,228 --> 00:12:17,396
Are you ok?

202
00:12:17,396 --> 00:12:23,294
Official Hai... official Hai has been captured.

203
00:13:35,826 --> 00:13:38,009
Yan San Niang, it's best if you don't move.

204
00:13:38,009 --> 00:13:41,267
It's the same as suicide if you move.

205
00:13:41,267 --> 00:13:44,146
The candles will light the ropes,

206
00:13:44,146 --> 00:13:47,025
and no one can save you after that.

207
00:13:49,955 --> 00:13:51,089
What do you want to do?

208
00:13:51,089 --> 00:13:53,271
I want to give Li Ge Xiao a choice.

209
00:13:53,271 --> 00:13:56,429
I want to see if he'd rather protect his lover or his country.

210
00:13:56,429 --> 00:13:58,147
If he comes,

211
00:13:58,147 --> 00:14:00,516
the Turkics will invade the capital.

212
00:14:00,516 --> 00:14:02,338
Which do you think he'll choose?

213
00:14:02,338 --> 00:14:03,535
Despicable!

214
00:14:03,535 --> 00:14:05,996
Despicable? Not really.

215
00:14:07,157 --> 00:14:09,479
If you don't want to put Li Ge Xiao in a difficult position,

216
00:14:09,479 --> 00:14:14,401
and choose to suicide... I'll tell you right now. You'll drag someone down with you!

217
00:14:20,617 --> 00:14:22,289
Official Hai!

218
00:14:22,289 --> 00:14:24,340
If he doesn't come,

219
00:14:24,340 --> 00:14:28,844
you'll both die when the time's up.

220
00:14:53,032 --> 00:14:55,408
Brother Li. (3x)

221
00:14:56,330 --> 00:14:57,776
Please sit, brother Li.<br>Oh no, it's fine.

222
00:14:57,776 --> 00:14:59,287
No need to be so polite.

223
00:14:59,287 --> 00:15:01,677
Brother Li, please sit first.<br>Yeah.

224
00:15:08,504 --> 00:15:12,544
I came here to ask about official Hai's whereabouts.

225
00:15:12,544 --> 00:15:16,020
If I disturbed you guys in anyway, please forgive me.

226
00:15:16,020 --> 00:15:18,210
It must be Ying Wu Qiu's fault.

227
00:15:18,210 --> 00:15:19,593
How come?

228
00:15:21,265 --> 00:15:22,552
Qian Hu.<br>Who are you?!

229
00:15:22,552 --> 00:15:24,206
Stop right there!<br>Don't move!

230
00:15:24,206 --> 00:15:25,443
Qian Hu get out here!

231
00:15:25,443 --> 00:15:26,546
Come out!

232
00:15:28,685 --> 00:15:31,098
How bold of you Ying Wu Qiu!<br>Now surrender to us!

233
00:15:31,499 --> 00:15:33,405
I want to see Li Ge Xiao.

234
00:15:33,405 --> 00:15:36,012
Do you mean... when official Hai disappeared

235
00:15:36,012 --> 00:15:37,616
Wu Qiu appeared?

236
00:15:37,616 --> 00:15:39,956
Right at the exact time.<br>Who else could it be?

237
00:15:39,956 --> 00:15:41,227
What did he say?

238
00:15:41,227 --> 00:15:43,733
He said he won't say anything until he sees you.

239
00:15:46,647 --> 00:15:49,454
You are the highest in command here now, right?

240
00:15:49,454 --> 00:15:50,741
What do you think of this situation?

241
00:15:50,741 --> 00:15:52,320
Currently, we don't have a leader to guide us.

242
00:15:52,320 --> 00:15:54,903
Everyone here wants to have a capable person

243
00:15:54,903 --> 00:15:57,243
to lead us through this.

244
00:16:01,454 --> 00:16:02,908
I've left this place a long time ago.

245
00:16:02,908 --> 00:16:06,338
Brother, we are all very clear on what's black and what's white.

246
00:16:06,338 --> 00:16:09,271
Brother, we're willing to follow your orders.

247
00:16:10,867 --> 00:16:14,983
Ok then... I'll meet with Wu Qiu first, then I'll decide.

248
00:16:44,744 --> 00:16:45,912
Where is Lord Hai?

249
00:17:03,937 --> 00:17:05,881
Why are you letting him go?!<br>I want to kill him!

250
00:17:05,881 --> 00:17:07,367
Could there only be thoughts of murder

251
00:17:07,367 --> 00:17:09,317
in your heart?!

252
00:17:09,317 --> 00:17:10,989
What if a crazy dog bit you?

253
00:17:10,989 --> 00:17:14,240
Are you going to get on all fours and bite him back?!

254
00:17:14,240 --> 00:17:15,819
Remember this!

255
00:17:16,573 --> 00:17:20,324
Only forgiveness can defuse hatred.

256
00:17:21,145 --> 00:17:23,342
Because I believe

257
00:17:23,342 --> 00:17:26,917
if you insist on killing a person,

258
00:17:27,932 --> 00:17:30,076
and that person dies,

259
00:17:30,076 --> 00:17:32,770
the murderer will also die once.

260
00:17:34,274 --> 00:17:38,667
The best way is to let go.

261
00:18:09,032 --> 00:18:10,386
I'm so happy!

262
00:18:11,751 --> 00:18:13,358
You're so serious.

263
00:18:15,308 --> 00:18:18,188
You really almost killed me.

264
00:18:21,557 --> 00:18:25,200
I came to let you know everything.

265
00:18:28,762 --> 00:18:33,320
That dumb Zhang Zhong gave me to the Turkics.

266
00:18:33,320 --> 00:18:38,296
And that's how I got the best business deal ever.

267
00:18:39,793 --> 00:18:44,148
The Turkic soldiers will wipe the country out.

268
00:18:44,844 --> 00:18:48,606
Not only did you kill our master, you also sold your country.

269
00:18:48,606 --> 00:18:51,903
I'm not done yet.<br>There's something else.

270
00:18:51,903 --> 00:18:55,851
I have your girl.

271
00:18:59,119 --> 00:19:04,906
I want to see what the fearless Li Ge Xiao will do.

272
00:19:06,485 --> 00:19:08,621
There's no way you can confine Yan San Niang.

273
00:19:09,854 --> 00:19:11,965
I have my ways.

274
00:19:17,781 --> 00:19:21,764
The place where she's confined is filled with gun powder.

275
00:19:21,764 --> 00:19:24,875
She'll be blown into pieces in two hours.

276
00:19:24,875 --> 00:19:26,431
You're using the lives of everyone in this country

277
00:19:26,431 --> 00:19:28,653
to play a game of choice with me?

278
00:19:28,653 --> 00:19:30,982
You still don't understand me?

279
00:19:30,982 --> 00:19:34,233
I, Ying Wu Qiu, don't want anything.

280
00:19:34,233 --> 00:19:37,112
The only thing I desire is to win against you! (2x)

281
00:19:39,367 --> 00:19:43,707
Five years ago, you made a mistake in choosing between your girl and your country.

282
00:19:43,707 --> 00:19:45,882
Ru Yi died because of you!

283
00:19:45,882 --> 00:19:47,933
I want to see what you're going to do now.

284
00:19:47,933 --> 00:19:52,530
Yan San Niang is at a warehouse on the east side.

285
00:19:52,530 --> 00:19:55,735
The Turkics will attack from the west side.

286
00:19:55,735 --> 00:19:58,800
Two hours, two locations.

287
00:19:58,800 --> 00:20:01,726
And the only person that can appear is you.

288
00:20:01,726 --> 00:20:04,651
If you show up at the west side on time,

289
00:20:04,651 --> 00:20:08,924
my partners will not sell the map to the Turkics,

290
00:20:10,249 --> 00:20:13,614
but then... your woman will...

291
00:20:42,616 --> 00:20:44,450
Lao Li, San Niang is missing.

292
00:20:44,450 --> 00:20:46,756
I know. Ying Wu Qiu took her.

293
00:20:47,459 --> 00:20:49,598
We thought you drove San Niang away because we assumed

294
00:20:49,598 --> 00:20:51,703
that you were dealing with a tough problem.

295
00:20:52,705 --> 00:20:54,043
Lao Li, I'll tell you this.

296
00:20:54,043 --> 00:20:55,712
If San Niang can come back safely this time,

297
00:20:55,712 --> 00:20:57,593
you should treat her better.

298
00:20:58,261 --> 00:21:00,449
Don't worry, she'll be fine.

299
00:21:00,449 --> 00:21:01,370
That's true.

300
00:21:01,370 --> 00:21:03,376
There's nothing we, Yi Zhi Mei, can't do, right?

301
00:21:03,376 --> 00:21:05,671
Then what are we waiting for?<br>Let's hurry and save San Niang!<br> Wait a minute.

302
00:21:06,858 --> 00:21:09,091
You guys have a more important job to do.

303
00:21:30,720 --> 00:21:32,399
Lord Hai. I am sorry

304
00:21:32,399 --> 00:21:36,208
I really can't escape this time.<br>I can't save you.

305
00:21:36,208 --> 00:21:37,640
I know that mister Ge

306
00:21:37,640 --> 00:21:39,598
will definitely stop the Turkics from attacking the capital.

307
00:21:40,434 --> 00:21:44,102
Sorry for dragging you down with me.<br>I don't mind if I die,

308
00:21:44,102 --> 00:21:46,122
but if something happened to you...

309
00:21:46,122 --> 00:21:49,141
then this world will have one less person that tells the truth,

310
00:21:49,141 --> 00:21:54,078
You already did the best you can, so don't blame yourself, ok?

311
00:21:54,078 --> 00:21:58,011
If I wasn't so impulsive, then Ying Wu Qiu wouldn't have had his way.

312
00:21:58,011 --> 00:22:01,076
We're captured and became mister Ge's burden.

313
00:22:01,076 --> 00:22:04,606
Or maybe... if I left earlier,

314
00:22:04,606 --> 00:22:07,856
and not holding on so tightly...

315
00:22:07,856 --> 00:22:10,503
then this wouldn't have happened.

316
00:22:10,503 --> 00:22:13,754
So you were actually talking about love.

317
00:22:14,102 --> 00:22:18,375
Sigh... you two really take the well being of each other into consideration.

318
00:22:18,375 --> 00:22:20,093
We are partners.

319
00:22:20,093 --> 00:22:25,295
Could it be that you don't know the feelings that Li Ge Xiao has for you?

320
00:22:26,209 --> 00:22:28,360
I'm not a parasite in his stomach.

321
00:22:28,360 --> 00:22:29,892
He doesn't say anything.

322
00:22:29,892 --> 00:22:32,121
How can I know what he's thinking?

323
00:22:32,121 --> 00:22:34,629
Some things are not easily said face to face.

324
00:22:34,629 --> 00:22:38,391
Especially if it's to the person you have feelings for.

325
00:22:38,934 --> 00:22:40,527
What are you saying?

326
00:22:40,527 --> 00:22:44,892
Your self esteem is too low.

327
00:22:46,378 --> 00:22:49,397
Did he say something to you?

328
00:22:50,444 --> 00:22:55,573
It was when he lost you.

329
00:22:55,573 --> 00:22:57,431
What exactly is it that you plan to do?

330
00:22:57,431 --> 00:23:00,032
There's someone waiting for me in the palace.

331
00:23:00,032 --> 00:23:04,668
You mean San Niang?<br>No, you musn't. It's too dangerous.

332
00:23:04,668 --> 00:23:06,881
I don't have much time. You must believe me.

333
00:23:06,881 --> 00:23:08,507
This is my only chance.

334
00:23:08,507 --> 00:23:11,672
There's no way I can go along with this plan of yours.

335
00:23:12,547 --> 00:23:14,917
Listen up. I want to successfully infiltrate the palace,

336
00:23:14,917 --> 00:23:16,264
and this is the only way.

337
00:23:16,264 --> 00:23:19,661
No! This plan is too dangerous, too extreme!

338
00:23:19,661 --> 00:23:20,641
You know me.

339
00:23:20,641 --> 00:23:22,513
I'm always this extreme.

340
00:23:25,454 --> 00:23:27,025
After Ru Yi passed away,

341
00:23:27,025 --> 00:23:30,836
I thought alcohol was the only thing that can accompany me for the rest of my life.

342
00:23:31,705 --> 00:23:33,383
But then there came a plum flower,

343
00:23:33,383 --> 00:23:36,625
and that plum flower showed me that I'm not alone.

344
00:23:36,625 --> 00:23:40,269
There's someone that really cares about me and is concerned about my well being.

345
00:23:40,269 --> 00:23:42,207
That person is miss Yan.

346
00:23:42,207 --> 00:23:45,717
It's not that I didn't know

347
00:23:45,717 --> 00:23:47,895
her feelings for me all this time.

348
00:23:48,865 --> 00:23:50,502
But I don't know if my heart

349
00:23:50,502 --> 00:23:53,945
has enough room for someone else.

350
00:23:57,622 --> 00:23:59,427
I've already lost Ru Yi,

351
00:23:59,427 --> 00:24:01,499
so from now on

352
00:24:01,499 --> 00:24:04,113
I will not allow myself to lose another person.

353
00:24:04,113 --> 00:24:07,489
My only goal is to save San Niang,

354
00:24:07,489 --> 00:24:09,160
and I'm depending on you for this.

355
00:24:43,768 --> 00:24:48,314
I didn't notice that the road we walked were this long.

356
00:24:48,314 --> 00:24:51,060
Don't you see how lucky you are?

357
00:24:52,565 --> 00:24:55,740
Even my mother thinks we're a pair.

358
00:24:55,740 --> 00:24:59,383
but we're really just partners.<br>Isn't that funny?

359
00:25:00,720 --> 00:25:05,540
In actuality, I'm just a book worm.

360
00:25:05,540 --> 00:25:08,114
I don't really know much about love,

361
00:25:08,114 --> 00:25:11,726
But I understand a man's heart very well.

362
00:25:11,726 --> 00:25:17,105
Do you know? The person he cares about most right now is you.

363
00:25:20,656 --> 00:25:24,432
Two locations, and the only person that can appear is you.

364
00:25:24,432 --> 00:25:27,440
If you show up at the west side on time,

365
00:25:27,440 --> 00:25:31,418
then my partners will not sell the map to the Turkics,

366
00:25:32,521 --> 00:25:36,537
but then... your woman will...

367
00:25:41,552 --> 00:25:44,526
I know he will definitely come save you.

368
00:25:46,097 --> 00:25:49,139
I don't need him, and I don't want him to come.

369
00:25:49,139 --> 00:25:50,777
Why?

370
00:25:50,777 --> 00:25:53,357
Yi Zhi Mei can come this far

371
00:25:53,357 --> 00:25:56,365
because we have faith and courage.

372
00:25:56,365 --> 00:26:01,045
Drink, drink!<br>Cheers!

373
00:26:03,219 --> 00:26:05,424
Who are you?!

374
00:26:06,393 --> 00:26:10,979
Get off the stage!

375
00:26:10,979 --> 00:26:14,355
When the sky rains, the ground becomes slippery, and when one falls, one will crawl.

376
00:26:14,355 --> 00:26:15,792
So stop looking for excuses for yourself!

377
00:26:15,792 --> 00:26:17,664
I don't know how you came this far,

378
00:26:17,664 --> 00:26:18,934
but I'm going to remind you!

379
00:26:18,934 --> 00:26:20,705
the most difficult moment to endure

380
00:26:20,705 --> 00:26:22,175
is the moment right before you succeed.

381
00:26:22,175 --> 00:26:25,424
I know that you care for me.

382
00:26:30,639 --> 00:26:33,112
Every time there's a problem,

383
00:26:33,112 --> 00:26:35,552
he'll always have a plan to guide us through it.

384
00:26:37,258 --> 00:26:42,543
He always said that courage and faith can create miracles.

385
00:26:43,714 --> 00:26:45,820
That's the Li Ge Xiao I know.

386
00:26:45,820 --> 00:26:47,262
Well said.

387
00:27:16,450 --> 00:27:18,121
You... how come you're here?

388
00:27:30,228 --> 00:27:32,400
Sorry I came late.

389
00:27:32,835 --> 00:27:33,938
Why did you come?

390
00:27:33,938 --> 00:27:35,442
Why can't I come?

391
00:27:35,442 --> 00:27:36,779
Ying Wu Qiu said

392
00:27:36,779 --> 00:27:39,419
if you come here, then the Turkics will attack the capital.

393
00:27:40,461 --> 00:27:42,834
Don't worry about it.<br>Let's get out of this mess first.

394
00:27:42,834 --> 00:27:44,806
Save official Hai first.

395
00:27:52,837 --> 00:27:53,831
Li Ge Xiao!

396
00:28:00,223 --> 00:28:01,893
Li Ge Xiao you came.

397
00:28:02,663 --> 00:28:04,267
You really came!

398
00:28:05,269 --> 00:28:06,840
Then why did you sacrifice Ru Yi

399
00:28:06,840 --> 00:28:08,144
five years ago for your

400
00:28:08,144 --> 00:28:11,458
so called "Justice".<br>WHY?!

401
00:28:17,542 --> 00:28:20,951
Ru Yi, are u watching?

402
00:28:22,055 --> 00:28:24,294
This is the man that you loved so much.

403
00:28:24,294 --> 00:28:26,166
Look at what he's doing!

404
00:28:33,025 --> 00:28:38,139
You were so selfless... was it worth it?<br>Was it worth it?!

405
00:28:42,458 --> 00:28:44,261
Li Ge Xiao,

406
00:28:45,130 --> 00:28:47,637
I'm going to drag you to hell with me today to meet Ru Yi.

407
00:28:47,637 --> 00:28:49,810
Get ready to explain this to her!

408
00:28:51,048 --> 00:28:52,684
Let San Niang go.

409
00:28:55,359 --> 00:28:57,197
Let San Niang go.

410
00:29:06,764 --> 00:29:10,239
I'll do whatever you want.<br>I beg you.

411
00:29:29,538 --> 00:29:31,777
Ge Xiao, take care.

412
00:29:33,616 --> 00:29:34,619
San niang

413
00:29:57,721 --> 00:29:59,867
No matter what your decision is,

414
00:29:59,867 --> 00:30:01,572
I'll support you.

415
00:30:04,881 --> 00:30:07,154
What hurts Ying Wu Qiu the most

416
00:30:07,154 --> 00:30:09,059
is your forbearance for him.

417
00:30:09,661 --> 00:30:12,435
He fails, and you're high and mighty.

418
00:30:13,037 --> 00:30:15,522
That's the real reason why he's always in pursuit of you.

419
00:30:29,488 --> 00:30:33,538
Promise me that you'll come back safely.

420
00:31:17,004 --> 00:31:20,151
You finally took back everything from my hands.

421
00:31:20,151 --> 00:31:22,927
I've never obtained anything.

422
00:31:22,927 --> 00:31:24,497
I've only lost.

423
00:31:24,497 --> 00:31:27,205
Do you think I can kill you today?

424
00:31:27,205 --> 00:31:28,508
You can't.

425
00:31:28,815 --> 00:31:32,419
Because there's no way I'll let you go again today.

426
00:31:32,930 --> 00:31:34,230
I never understood why

427
00:31:35,033 --> 00:31:37,305
you're so sure that

428
00:31:37,305 --> 00:31:39,211
I won't give the map to the Turkics.

429
00:31:39,211 --> 00:31:41,157
Because I believe in you.

430
00:31:42,053 --> 00:31:44,224
Do you know why I keep

431
00:31:44,224 --> 00:31:46,330
letting you go again and again?

432
00:31:46,531 --> 00:31:48,053
Because in my heart,

433
00:31:48,053 --> 00:31:50,190
you are forever the Bao Lai Ying from the past.

434
00:31:50,190 --> 00:31:52,252
Never changed.

435
00:31:52,790 --> 00:31:54,462
The reason you worked for Yan Song

436
00:31:54,462 --> 00:31:57,466
all these years, kneeling down to him,

437
00:31:57,466 --> 00:32:00,709
was for the chance to personally get rid of them.

438
00:32:00,709 --> 00:32:04,191
Making him and his son miserable.

439
00:32:04,191 --> 00:32:08,035
Right. The person to avenge Ru Yi is me.

440
00:32:08,035 --> 00:32:09,974
And it can only be me.

441
00:32:16,258 --> 00:32:18,146
Are you satisfied now?

442
00:32:18,146 --> 00:32:19,865
Very satisfied.

443
00:32:19,865 --> 00:32:22,730
As expected of the general to keep his promise.

444
00:32:22,730 --> 00:32:25,222
And now it's your turn to keep the promise.

445
00:32:25,222 --> 00:32:28,999
Give me the other half of the map.

446
00:32:28,999 --> 00:32:31,004
Of course.

447
00:32:34,648 --> 00:32:36,397
What a nice pair of father and son

448
00:32:36,397 --> 00:32:39,935
that will sell even the land that their ancestors reside.

449
00:32:39,935 --> 00:32:44,715
It's not like my surname is Zhu.<br>This country isn't my ancestors', so I don't like it.

450
00:32:58,660 --> 00:33:00,100
Yan Song, you betrayed me?!

451
00:33:00,100 --> 00:33:04,409
You bastards! Our emperor personally came here today.

452
00:33:04,409 --> 00:33:06,815
You guys can't escape this time.

453
00:33:06,815 --> 00:33:08,859
So you guys should surrender!

454
00:33:09,302 --> 00:33:11,568
Even the heavens assist my country.

455
00:33:11,568 --> 00:33:14,432
Today, I will eliminate all of you who sell out their own country,

456
00:33:14,432 --> 00:33:16,986
and all those who are foreigners, in one fell swoop!

457
00:33:16,986 --> 00:33:19,486
I know you will definitely send someone

458
00:33:19,486 --> 00:33:21,462
to deliver a secret message to the emperor,

459
00:33:21,462 --> 00:33:23,508
to have the emperor personally go

460
00:33:23,508 --> 00:33:25,814
and witness Yan Song's doings, giving him no way out.

461
00:33:25,814 --> 00:33:29,591
The only thing I did was to send two extra people, just in case.

462
00:33:31,445 --> 00:33:32,766
Why...

463
00:33:34,404 --> 00:33:37,257
Why do you always guess my next step correctly?

464
00:33:37,257 --> 00:33:40,182
Because we were best of friends before.

465
00:34:24,485 --> 00:34:26,018
Fast, accurate, aggressive

466
00:34:26,018 --> 00:34:28,387
As expected of Li Ge Xiao's sword skills.

467
00:34:29,380 --> 00:34:31,051
But I'll tell you...

468
00:34:31,051 --> 00:34:34,516
I prepared something else just for today's fight.

469
00:35:20,439 --> 00:35:22,815
This is satisfying!<br>Satisfying!

470
00:35:22,815 --> 00:35:24,131
I won't stop until

471
00:35:24,131 --> 00:35:26,061
my blood runs dry.

472
00:36:22,817 --> 00:36:26,819
Li Ge Xiao, death doesn't mean defeat.

473
00:36:26,819 --> 00:36:30,527
Living is even more painful.

474
00:36:30,527 --> 00:36:33,125
I... I finally won.

475
00:36:34,093 --> 00:36:36,701
I can see Ru Yi

476
00:36:39,812 --> 00:36:42,459
before you.

477
00:37:14,521 --> 00:37:17,296
Everyone has life,

478
00:37:17,677 --> 00:37:21,097
but it doesn't mean that everyone understands life.

479
00:37:21,889 --> 00:37:23,233
The ones that doesn't understand life

480
00:37:23,528 --> 00:37:26,438
will see life as a punishment.

481
00:37:26,936 --> 00:37:29,410
I was like that before.

482
00:37:30,241 --> 00:37:34,704
I remember when I was younger, when my parents were still there,

483
00:37:35,006 --> 00:37:36,880
I asked them

484
00:37:36,980 --> 00:37:39,494
why I was named Li Ge Xiao.

485
00:37:40,179 --> 00:37:42,088
They wanted me to remember

486
00:37:42,251 --> 00:37:46,407
that I should be singing happily when someone leaves.

487
00:37:47,405 --> 00:37:49,425
I didn't understand it back then.

488
00:37:49,681 --> 00:37:51,462
I realized later

489
00:37:51,462 --> 00:37:55,834
that the reason you cry and become sad because of a departure

490
00:37:55,834 --> 00:37:59,874
is because you didn't do your best when you were together.

491
00:38:00,634 --> 00:38:03,683
The tears you shed are because of the heartache you feel when you realize that you took things for granted.

492
00:38:03,984 --> 00:38:06,508
If you cherish something when you have it,

493
00:38:06,508 --> 00:38:09,813
then you won't have any regrets when departure comes.

494
00:38:10,286 --> 00:38:12,708
Departure is welcoming

495
00:38:12,708 --> 00:38:15,409
the start of a new beginning.

496
00:38:15,843 --> 00:38:19,124
So isn't it worth us singing happily?

497
00:38:19,637 --> 00:38:23,939
Just like how we've faced all the setbacks and hardships along the way,

498
00:38:23,939 --> 00:38:27,058
The thing we're unable to surpass is not some violent revolution,

499
00:38:27,058 --> 00:38:30,130
but the thing we can not withstand is ourselves.

500
00:38:30,478 --> 00:38:33,320
My friend, continue to persevere for

501
00:38:33,320 --> 00:38:37,368
our loved ones, our dreams, and our destiny.

502
00:38:37,368 --> 00:38:39,557
How can we enjoy the fragrance of plum blossoms

503
00:38:39,557 --> 00:38:42,344
if we don't first undergo an extreme cold?

504
00:38:51,139 --> 00:38:53,414
Is it a thing for women to ruin the mood?

505
00:38:53,414 --> 00:38:56,422
Today is different from before. Do you still need this?

506
00:38:56,422 --> 00:38:59,712
Well I don't really need it, but I still need it.

507
00:39:00,866 --> 00:39:03,195
You should drink less.

508
00:39:03,195 --> 00:39:04,588
Don't say that I didn't tell you,

509
00:39:04,588 --> 00:39:07,096
being too controlling is another one of women's things.

510
00:39:07,096 --> 00:39:08,373
Well you had to be mine.

511
00:39:08,373 --> 00:39:10,510
If I don't manage you, then who'll I manage?

512
00:39:11,044 --> 00:39:13,737
This is bad, this is bad!

513
00:39:15,177 --> 00:39:17,399
Shan Xi is in turmoil.<br>The people there are in trouble.

514
00:39:17,399 --> 00:39:18,335
Looking for trouble.

515
00:39:18,335 --> 00:39:20,163
Then let's hurry and take action!

516
00:39:20,163 --> 00:39:23,397
<br>Alright. Let us Yi Zhi Mei get going!

