1
00:00:05,005 --> 00:00:08,998
A Kaijyu Theater Presents

2
00:00:12,012 --> 00:00:16,005
A Shinya TSUKAMOTO Film

3
00:00:19,019 --> 00:00:23,012
Starring: Tadanobu AS ANO

4
00:00:26,359 --> 00:00:35,700
VITAL

5
00:02:16,402 --> 00:02:17,494
Doctor?

6
00:02:20,173 --> 00:02:21,265
Mr. Takagi?

7
00:02:21,975 --> 00:02:23,169
Mr. Hiroshi Takagi?

8
00:02:32,352 --> 00:02:35,412
So, you don't remember?
Not even my face?

9
00:02:38,391 --> 00:02:39,380
I see.

10
00:02:40,894 --> 00:02:43,124
Never thought you'd forget this mug.

11
00:02:56,409 --> 00:02:57,398
I...

12
00:02:59,646 --> 00:03:02,012
I think everything's mixed up.

13
00:03:06,119 --> 00:03:07,279
You had an accident.

14
00:03:09,022 --> 00:03:11,582
You were out driving.

15
00:03:12,292 --> 00:03:17,696
A truck crashed into yours.
The driver admits it's his fault.

16
00:03:18,965 --> 00:03:23,129
I had you taken to
my old university hospital.

17
00:03:28,508 --> 00:03:31,306
We were the ones who ruined it.

18
00:03:32,378 --> 00:03:34,312
After all this time.

19
00:03:37,550 --> 00:03:41,452
He said himself that he'd
never make it as a doctor.

20
00:03:41,921 --> 00:03:43,320
I wonder why.

21
00:03:45,258 --> 00:03:49,661
Ever since he was young he told us
he wanted to be a doctor.

22
00:03:50,463 --> 00:03:54,490
But somewhere along the way
he became lost to us.

23
00:03:55,935 --> 00:03:59,029
He's forgotten all that now.

24
00:04:26,499 --> 00:04:27,523
You okay?

25
00:06:06,966 --> 00:06:09,935
"Doctors and Medicine"

26
00:06:17,610 --> 00:06:19,373
"Dissection"

27
00:06:39,665 --> 00:06:41,758
Come and see!

28
00:06:49,542 --> 00:06:51,407
You remember about this?

29
00:06:53,312 --> 00:06:56,042
Do you understand this?

30
00:07:05,858 --> 00:07:08,691
Well, take your time.

31
00:07:08,928 --> 00:07:10,691
No need to rush.

32
00:07:11,564 --> 00:07:13,589
<i>Medical College
Admissions Ceremony</i>

33
00:07:16,135 --> 00:07:19,366
Congratulations
on entering medical college!

34
00:07:56,909 --> 00:07:59,343
What do you want to live here for?

35
00:08:00,079 --> 00:08:03,378
Do you have to move out?

36
00:08:03,516 --> 00:08:06,246
Ah, and there's your college?

37
00:08:07,386 --> 00:08:09,718
I suppose it is near.

38
00:08:13,626 --> 00:08:16,754
Look, I'm worried.

39
00:08:17,430 --> 00:08:18,692
Does it have to be here?

40
00:08:29,775 --> 00:08:33,006
The ovum, a product of
almost pure chance...

41
00:08:33,145 --> 00:08:37,809
...by means of cellular growth,
divergence and migration...

42
00:08:37,950 --> 00:08:39,508
...creates an organism.

43
00:08:39,652 --> 00:08:43,986
This person experienced trauma
to the frontal lobe section.

44
00:08:44,123 --> 00:08:47,923
This area is responsible for
personality and memory.

45
00:08:48,127 --> 00:08:50,618
From this we can conclude the following:

46
00:08:50,763 --> 00:08:54,358
Human character is not a constant...

47
00:08:54,500 --> 00:08:57,992
The brain and spinal cord form
the central nervous system.

48
00:08:58,137 --> 00:09:00,799
Nerve cells are concentrated
in this area.

49
00:09:01,007 --> 00:09:02,406
I wonder, then...

50
00:09:02,608 --> 00:09:04,599
where the soul lies...

51
00:09:05,111 --> 00:09:08,308
Beneath this, however...

52
00:09:09,649 --> 00:09:12,709
...there is the vast realm
of the unconscious.

53
00:09:13,419 --> 00:09:16,946
It is here that our suppressed desires...

54
00:09:17,089 --> 00:09:22,823
...can cause deep mental conflict
as they strive to realize themselves.

55
00:10:00,199 --> 00:10:01,188
Hey!

56
00:10:03,102 --> 00:10:03,932
I, err...

57
00:10:19,819 --> 00:10:24,916
When streaking the petri sample
take your time, or you'll contaminate it.

58
00:10:26,025 --> 00:10:28,858
Very good, Mr. Takagi.

59
00:10:29,061 --> 00:10:33,293
Wow! Yours is excellent.
How do you do it?

60
00:10:33,499 --> 00:10:34,625
How's mine?

61
00:10:35,167 --> 00:10:35,997
What a mess!

62
00:10:36,135 --> 00:10:37,864
It's contaminated!

63
00:10:38,070 --> 00:10:42,166
What are you lot up to?
Back to your places.

64
00:10:44,310 --> 00:10:46,141
Stop joking around.

65
00:10:50,249 --> 00:10:52,740
Why are you telling me this
on the phone?

66
00:10:53,486 --> 00:10:55,716
And how can you say it so lightly?

67
00:10:57,189 --> 00:10:59,657
So my Ms. Yoshimoto is after Takagi?

68
00:11:00,559 --> 00:11:02,288
You can't take your eyes off him.

69
00:11:03,062 --> 00:11:06,554
Scary.
So you've been spying on us?

70
00:11:06,766 --> 00:11:08,097
What's wrong with you?

71
00:11:09,502 --> 00:11:11,902
Is it about the entrance exam?

72
00:11:12,338 --> 00:11:14,431
About Takagi being top?

73
00:11:14,740 --> 00:11:16,935
That's why you changed tack, eh?

74
00:11:18,778 --> 00:11:23,579
Or you approached me only to know
the result?

75
00:11:25,084 --> 00:11:28,747
For the price of sweets you should
be more careful.

76
00:11:30,423 --> 00:11:32,050
You sneak y BITCH!

77
00:12:25,177 --> 00:12:27,668
<i>I understand you want to be alone.</i>

78
00:12:28,013 --> 00:12:30,675
<i>But don't brood too much.</i>

79
00:12:31,283 --> 00:12:37,085
<i>I had some of your furniture taken
there, and put In the same place.</i>

80
00:12:37,590 --> 00:12:39,956
<i>I thought it would help you remember.</i>

81
00:12:45,431 --> 00:12:47,922
<i>First: Ikumi Yoshimoto
Second: Hiroshi Takagi</i>

82
00:12:48,267 --> 00:12:49,996
I'm third. Ikumi beat me again.

83
00:12:50,436 --> 00:12:53,633
Hiroshi, you're second.

84
00:12:54,306 --> 00:12:55,671
Excellent!

85
00:12:57,009 --> 00:12:58,271
How did you do it?

86
00:12:58,811 --> 00:13:00,039
Tell me your secret.

87
00:13:01,947 --> 00:13:03,676
Hey, everybody!

88
00:13:03,883 --> 00:13:05,441
Dr. Nakai has been found, dead.

89
00:13:05,651 --> 00:13:07,175
He hanged himself.

90
00:13:07,820 --> 00:13:11,813
It's still not officially announced yet.

91
00:13:12,158 --> 00:13:15,025
But I heard it in the research lab.

92
00:13:16,228 --> 00:13:17,559
Who's Dr. Nakai?

93
00:13:17,897 --> 00:13:19,455
The microbiology teacher.

94
00:13:20,332 --> 00:13:22,664
No, you're kidding.

95
00:13:48,727 --> 00:13:52,026
<i>So, dissection classes are starting, eh?</i>

96
00:13:52,331 --> 00:13:56,563
<i>It's like a doctors' Initiation ceremony.</i>

97
00:13:56,702 --> 00:14:01,833
<i>So far, what you've done
has been a walk in the park!</i>

98
00:14:02,041 --> 00:14:07,104
<i>Come this far In 3 years,
and that's the best of all.</i>

99
00:14:07,513 --> 00:14:09,913
<i>Since you started medicine</i>

100
00:14:10,049 --> 00:14:13,143
<i>I swear granddad's picture
has started smiling!</i>

101
00:15:05,371 --> 00:15:10,274
Medical history stems from wanting to
understand the body's mechanism.

102
00:15:10,843 --> 00:15:15,542
Dr. Genpaku Sugita was amazed by
the similarity in Dutch anatomical texts...

103
00:15:15,848 --> 00:15:19,944
and his texts.
So he worked on translating them.

104
00:15:20,085 --> 00:15:26,388
Eventually "Kauai Shinsho" was
published in 1774 in Tokyo.

105
00:15:27,192 --> 00:15:33,153
Trust your own eyes. This is
the key point, whatever the century.

106
00:15:33,532 --> 00:15:38,492
The truth is there for you to see.
You must embrace this concept.

107
00:15:39,939 --> 00:15:44,535
As students of medicine
you are very privileged.

108
00:15:45,044 --> 00:15:49,538
You have the opportunity to
explore the bodies before you.

109
00:15:50,282 --> 00:15:58,314
Treat them with respect at all times. We
owe thanks to them and their families...

110
00:15:58,891 --> 00:16:01,724
First, take off the vinyl sheet.

111
00:16:08,934 --> 00:16:12,529
Only remove the cover from
the area to be dissected.

112
00:16:12,805 --> 00:16:14,432
It protects the cadaver from drying.

113
00:16:14,707 --> 00:16:18,108
For today, remove the whole cover.

114
00:16:18,243 --> 00:16:20,973
Inspect your subject.

115
00:16:21,413 --> 00:16:24,314
Are there any operation scars?

116
00:16:24,583 --> 00:16:26,881
Make a thorough inspection.

117
00:16:44,636 --> 00:16:49,869
Make a long vertical incision here.

118
00:16:50,109 --> 00:16:52,873
Then make a lateral incision.

119
00:16:53,846 --> 00:16:57,543
Take this corner piece.

120
00:16:57,750 --> 00:16:59,877
Is it usual for a subject to be so young?

121
00:17:01,887 --> 00:17:04,287
No, it's very rare.

122
00:17:04,823 --> 00:17:06,552
I wonder how she died.

123
00:17:07,226 --> 00:17:09,091
You'll find out as you dissect.

124
00:17:31,717 --> 00:17:32,615
What's wrong?

125
00:17:34,486 --> 00:17:35,475
Because it's her neck?

126
00:17:38,090 --> 00:17:38,818
No.

127
00:18:12,458 --> 00:18:14,392
Don't peel too thickly.

128
00:18:14,827 --> 00:18:16,954
The tendons will stick to the skin.

129
00:18:17,629 --> 00:18:20,860
We use those when we say "ee".

130
00:18:21,133 --> 00:18:24,193
There, beneath the skin in the neck.

131
00:19:06,545 --> 00:19:08,513
She's hopeless.

132
00:19:08,714 --> 00:19:12,343
Remember her "spectacular" speech
in class?

133
00:19:12,484 --> 00:19:17,046
"Human body as being an antenna
and such?" What a joke.

134
00:19:19,992 --> 00:19:20,981
You want this?

135
00:20:09,741 --> 00:20:11,766
How many times did this heart beat?

136
00:20:12,244 --> 00:20:15,680
Seventy times a minute is 4200
times an hour.

137
00:20:15,814 --> 00:20:20,251
So in a day? Well, 24 times that.
Then times 365 for a year.

138
00:20:20,385 --> 00:20:23,183
How many times if you live to 80?

139
00:20:23,422 --> 00:20:25,890
And yet my TV broke after 6 years.

140
00:20:27,659 --> 00:20:30,184
Was anything wrong with this heart?

141
00:20:31,430 --> 00:20:34,160
There's something wrong in the lungs.

142
00:20:34,299 --> 00:20:38,235
See, I can't get my hand in very far.

143
00:20:44,843 --> 00:20:48,745
There are 2 or 3 white lumps in the liver.

144
00:20:49,281 --> 00:20:51,044
And look at the colon.

145
00:20:51,183 --> 00:20:53,743
This person had a colostomy.

146
00:20:54,052 --> 00:20:56,919
Cancer of the colon can easily move
to the liver.

147
00:20:57,856 --> 00:21:00,120
Why did this person die?

148
00:21:01,927 --> 00:21:03,485
Mr. Takagi?

149
00:21:04,496 --> 00:21:05,895
I don't know.

150
00:21:08,300 --> 00:21:11,098
We can't tell by the organs alone.

151
00:21:11,436 --> 00:21:13,563
We'll find out later.

152
00:21:14,840 --> 00:21:16,535
I wonder what she was like.

153
00:21:16,842 --> 00:21:18,867
Shame to die so young.

154
00:23:16,895 --> 00:23:18,988
I must have dozed off.

155
00:23:21,433 --> 00:23:23,401
I had an awful dream.

156
00:23:33,311 --> 00:23:35,711
I nearly remembered the dream.

157
00:23:39,084 --> 00:23:42,542
And why did I think of that?

158
00:23:44,289 --> 00:23:45,984
Who told me?

159
00:23:48,326 --> 00:23:50,317
That story about Martian robots.

160
00:23:55,500 --> 00:23:56,489
You want to hear?

161
00:23:59,538 --> 00:24:00,664
Do it to me.

162
00:24:03,842 --> 00:24:04,831
Me too.

163
00:24:31,470 --> 00:24:33,802
Most men are so fierce.

164
00:24:35,040 --> 00:24:37,031
You're better for me.

165
00:24:40,412 --> 00:24:42,539
Today was relaxing.

166
00:24:44,649 --> 00:24:47,345
Don't get me wrong.

167
00:24:48,587 --> 00:24:51,818
I'm not as easy as you may think.

168
00:24:58,897 --> 00:24:59,989
What's up?

169
00:25:02,100 --> 00:25:04,864
You're so distant after class today.

170
00:25:12,244 --> 00:25:15,441
It was me who killed Dr. Nakai.

171
00:25:17,782 --> 00:25:20,250
I dated him before college.

172
00:25:21,753 --> 00:25:26,315
An older friend introduced us.
I was asking about this school.

173
00:25:27,792 --> 00:25:30,920
He had a family, but he was kind to me.

174
00:25:32,264 --> 00:25:35,529
But things got wrong,
and I grew apart from him.

175
00:25:36,568 --> 00:25:39,969
He left no will, and no one
knew about us.

176
00:25:40,739 --> 00:25:43,207
I tried to think it wasn't my fault.

177
00:25:43,675 --> 00:25:48,612
My parents were always happy
to see me.

178
00:25:49,147 --> 00:25:53,948
Some funny-looking reporters were
telling us about war and such.

179
00:25:54,786 --> 00:25:58,552
In our house the sun was
always shining.

180
00:25:59,891 --> 00:26:01,449
So after a while...

181
00:26:02,794 --> 00:26:04,659
The fact that Dr. Nakai was dead...

182
00:26:06,398 --> 00:26:08,059
just didn't bother me.

183
00:26:11,937 --> 00:26:14,030
I wonder what it was like for him.

184
00:26:26,685 --> 00:26:27,709
Harder.

185
00:26:28,286 --> 00:26:29,685
Do it harder.

186
00:27:17,369 --> 00:27:18,358
What's up?

187
00:27:19,671 --> 00:27:21,070
Nothing. I'm okay.

188
00:27:24,009 --> 00:27:24,668
Watch this.

189
00:27:24,809 --> 00:27:25,639
What?

190
00:27:26,478 --> 00:27:27,502
"Bye bye"

191
00:27:27,646 --> 00:27:28,635
Leave it!

192
00:28:10,588 --> 00:28:12,954
I thought I'd get something
from drawing.

193
00:28:13,825 --> 00:28:15,224
Turns out I got nothing.

194
00:28:16,795 --> 00:28:21,164
They look real to me though.

195
00:28:23,401 --> 00:28:28,304
As for me, I'm not sure if I am 'real'.

196
00:28:28,440 --> 00:28:32,638
It's like as if I am sleeping and dreaming.

197
00:28:34,813 --> 00:28:36,337
So, you liked drawing.

198
00:28:39,584 --> 00:28:41,017
Are you going to be a doctor?

199
00:28:54,099 --> 00:28:58,399
Everyone thought I would be a doctor.

200
00:29:01,005 --> 00:29:03,735
My parents, relatives, even me.

201
00:29:20,458 --> 00:29:23,950
When I dozed off here the other day

202
00:29:24,863 --> 00:29:27,593
I felt like I'd slept for a long time.

203
00:29:31,402 --> 00:29:32,391
What?

204
00:29:34,038 --> 00:29:35,938
I'm remembering something else.

205
00:29:37,809 --> 00:29:39,709
I can't quite grasp it.

206
00:29:41,246 --> 00:29:42,304
I wonder what it is.

207
00:29:49,187 --> 00:29:53,556
When you're "there"
can you remember things from "here"?

208
00:29:56,194 --> 00:29:59,425
But if that were the case...

209
00:30:01,833 --> 00:30:02,822
I see.

210
00:30:05,203 --> 00:30:06,192
Look...

211
00:30:07,806 --> 00:30:11,139
I know we have to take things slowly.

212
00:30:13,111 --> 00:30:18,708
But she died in the car crash that day.

213
00:30:18,983 --> 00:30:23,079
Don't worry. It was not your fault.

214
00:30:25,256 --> 00:30:26,848
Her name was Ryoko.

215
00:30:27,592 --> 00:30:31,926
She passed away before you woke up.

216
00:30:33,665 --> 00:30:37,761
The body that I'm dissecting is...

217
00:30:39,170 --> 00:30:40,865
Ryoko's body.

218
00:30:42,173 --> 00:30:44,266
I don't think it can be a coincidence.

219
00:30:45,009 --> 00:30:49,673
I did hear she wanted to leave
her body to science.

220
00:30:49,881 --> 00:30:52,076
Other than that, I don't know.

221
00:30:52,450 --> 00:30:55,647
If what you think is true...

222
00:30:56,521 --> 00:30:58,386
That would be surprising.

223
00:30:59,557 --> 00:31:01,422
Thank you so much.

224
00:31:29,587 --> 00:31:30,918
Why have you come here?

225
00:31:32,523 --> 00:31:33,421
Just go away.

226
00:31:36,928 --> 00:31:39,988
Your parents were very sincere
about their grief.

227
00:31:41,733 --> 00:31:44,133
What I really want to see
is your sincerity.

228
00:31:45,770 --> 00:31:51,367
You were driving when it happened.

229
00:31:51,576 --> 00:31:54,170
I still feel you murdered my daughter.

230
00:31:56,014 --> 00:31:57,675
If you want to mourn her...

231
00:31:58,449 --> 00:32:00,849
...do it when you truly remember her.

232
00:32:05,256 --> 00:32:08,282
I'm starting to remember already.

233
00:32:10,595 --> 00:32:12,893
Right now, at college...

234
00:32:13,364 --> 00:32:15,332
I'm doing my dissection practice.

235
00:32:16,034 --> 00:32:20,061
There is something I need to know.

236
00:32:20,872 --> 00:32:22,499
If you can tell me...

237
00:32:24,108 --> 00:32:27,976
Ryoko is on the dissection table.

238
00:32:28,112 --> 00:32:29,670
I don't quite understand it.

239
00:32:29,881 --> 00:32:30,870
Are you serious?

240
00:32:32,750 --> 00:32:33,648
Well?

241
00:32:36,087 --> 00:32:37,418
Tell me it's not true.

242
00:32:40,191 --> 00:32:44,628
You want to know if it's her?

243
00:32:46,464 --> 00:32:47,362
Right?

244
00:32:50,668 --> 00:32:51,657
You heartless FUCK!

245
00:32:52,737 --> 00:32:54,500
How the hell should I know?

246
00:32:55,373 --> 00:32:58,501
Just before she died she told us.

247
00:32:58,643 --> 00:33:00,543
Leaving her body to science.

248
00:33:02,180 --> 00:33:05,911
We didn't know you even could.

249
00:33:06,417 --> 00:33:11,013
And now you think you're poking
around inside her?

250
00:33:11,723 --> 00:33:13,850
What kind of a quack's school is that?

251
00:33:14,092 --> 00:33:15,821
I'm getting on the phone.

252
00:33:16,427 --> 00:33:19,157
It's despicable!

253
00:33:20,031 --> 00:33:21,692
In fact, fuck the phone.

254
00:33:22,166 --> 00:33:23,599
I'll go there in person.

255
00:33:41,886 --> 00:33:43,012
Dr. Kashiwabuchi.

256
00:33:43,087 --> 00:33:43,985
Yes?

257
00:33:45,757 --> 00:33:46,746
What is it?

258
00:33:47,625 --> 00:33:49,092
Sorry, nothing.

259
00:33:52,964 --> 00:33:53,953
Sir.

260
00:33:55,166 --> 00:33:58,294
May I dissect my subject alone?

261
00:33:58,436 --> 00:33:59,425
Why?

262
00:34:00,304 --> 00:34:02,864
The others are making such a mess.

263
00:34:03,574 --> 00:34:06,907
No, you can't.
It's our first attempt.

264
00:34:07,512 --> 00:34:09,503
There are bound to be mistakes.

265
00:34:19,090 --> 00:34:19,988
What's the matter?

266
00:34:24,062 --> 00:34:24,824
What's wrong.

267
00:34:25,797 --> 00:34:26,627
Nothing.

268
00:34:36,607 --> 00:34:39,132
Today we will observe
the facial muscles.

269
00:34:39,277 --> 00:34:41,745
Take off the head covers.

270
00:35:12,777 --> 00:35:14,176
Where're we heading?

271
00:35:17,915 --> 00:35:18,904
Hiroshi.

272
00:35:21,652 --> 00:35:25,213
What would it be like
to smash into something?

273
00:35:27,959 --> 00:35:29,119
Would it hurt?

274
00:35:33,498 --> 00:35:35,523
I remembered the dream I had.

275
00:37:29,981 --> 00:37:30,970
You hate me!

276
00:37:32,516 --> 00:37:33,710
Well, why then?

277
00:37:35,186 --> 00:37:38,713
You've not made love to me yet. Why?

278
00:37:47,131 --> 00:37:48,894
Is there someone else?

279
00:37:52,637 --> 00:37:53,661
Is there?

280
00:38:09,153 --> 00:38:10,211
Dancing?

281
00:38:14,225 --> 00:38:16,557
She never danced when she was alive.

282
00:38:22,700 --> 00:38:25,294
Your memory is still mixed up.

283
00:38:25,703 --> 00:38:27,637
It isn't a memory.

284
00:38:28,739 --> 00:38:31,731
I was there with her.

285
00:38:32,243 --> 00:38:33,870
Not a memory of myself.

286
00:38:34,679 --> 00:38:38,479
I mean the real me. I was there.

287
00:38:48,492 --> 00:38:50,153
And what kind of "dance" was it?

288
00:38:51,162 --> 00:38:52,823
The one that Ryoko did.

289
00:39:12,183 --> 00:39:15,380
You make sure you take good care
of Ryoko's body.

290
00:39:15,886 --> 00:39:18,150
If not, the school will be in trouble.

291
00:39:18,889 --> 00:39:20,083
And I'll kill you.

292
00:39:21,292 --> 00:39:23,453
I carry a big knife all the time.

293
00:39:29,066 --> 00:39:29,964
Hey.

294
00:39:36,207 --> 00:39:39,438
My wife is ill.

295
00:39:42,813 --> 00:39:45,213
You should keep coming round.

296
00:39:46,150 --> 00:39:48,380
Tell us about your time with Ryoko.

297
00:40:11,142 --> 00:40:11,972
I'm going now.

298
00:40:12,109 --> 00:40:13,406
Do you have to?

299
00:40:16,680 --> 00:40:18,011
But where do you go?

300
00:40:19,450 --> 00:40:20,474
I'll see you again.

301
00:40:20,785 --> 00:40:22,343
Stay with me a little more.

302
00:40:25,790 --> 00:40:28,020
But we can't, right?

303
00:40:32,029 --> 00:40:33,018
Hiroshi!

304
00:40:40,304 --> 00:40:41,362
I'll come again soon.

305
00:40:59,824 --> 00:41:00,813
What is it?

306
00:41:01,592 --> 00:41:05,119
May we be moved to another group?

307
00:41:05,396 --> 00:41:06,226
Why?

308
00:41:06,363 --> 00:41:07,990
It's just that...

309
00:41:08,899 --> 00:41:09,888
Mr. Takagi?

310
00:41:11,268 --> 00:41:12,200
Yes.

311
00:41:13,070 --> 00:41:15,368
The other 3 haven't complained.

312
00:41:16,340 --> 00:41:18,535
It's just a matter of time.

313
00:41:27,184 --> 00:41:28,116
What?

314
00:41:30,588 --> 00:41:31,520
What's your problem?

315
00:41:56,780 --> 00:41:59,647
Mr. Takagi, put your gloves on.

316
00:42:01,919 --> 00:42:02,908
Mr. Takagi.

317
00:42:04,154 --> 00:42:08,853
Sir, I think there's an operation scar
here on the head.

318
00:42:10,294 --> 00:42:11,283
I see.

319
00:42:14,465 --> 00:42:16,558
And a blood clot on the other side.

320
00:42:17,368 --> 00:42:19,427
That could be the direct cause.

321
00:42:20,671 --> 00:42:22,764
A gradual hemorrhage, probably.

322
00:42:24,542 --> 00:42:26,874
Do you think
she regained consciousness?

323
00:42:27,378 --> 00:42:29,505
Between the operation and the
blood clot?

324
00:42:30,481 --> 00:42:33,109
It's quite possible.

325
00:42:57,608 --> 00:43:04,343
Our 4-month dissection program,
like the rainy season, will end soon.

326
00:43:04,782 --> 00:43:09,276
Make sure your inspection
has been detailed and thorough.

327
00:43:15,826 --> 00:43:17,589
You're making fine progress.

328
00:43:19,330 --> 00:43:22,299
I suppose studying hands-on
helps you remember.

329
00:43:23,033 --> 00:43:24,057
I guess.

330
00:43:25,703 --> 00:43:29,503
That's good.
I'm happy for you.

331
00:43:29,807 --> 00:43:35,143
But try not to get too absorbed.

332
00:43:35,846 --> 00:43:38,815
Dr. Kashiwabuchi was a little worried.

333
00:43:39,316 --> 00:43:44,447
You're becoming more distant
as time goes by.

334
00:43:49,393 --> 00:43:50,382
Dad...

335
00:43:53,030 --> 00:43:55,498
...when I saw her on the
dissection table...

336
00:43:55,699 --> 00:43:57,792
I couldn't understand at all.

337
00:43:58,535 --> 00:44:00,093
But now I know much more.

338
00:44:02,606 --> 00:44:05,507
It's like... she made her own way there.

339
00:44:11,281 --> 00:44:12,270
Hiroshi.

340
00:44:15,219 --> 00:44:16,777
Please come back to us.

341
00:46:29,353 --> 00:46:30,342
Ryoko?

342
00:46:40,364 --> 00:46:42,025
You strangle me too.

343
00:46:43,734 --> 00:46:44,962
Grab me!

344
00:46:46,970 --> 00:46:48,494
Please, Hiroshi.

345
00:46:49,373 --> 00:46:50,965
You want to die, right?

346
00:46:51,742 --> 00:46:53,607
Let's do it together!

347
00:46:55,145 --> 00:46:56,237
Strangle me!

348
00:46:58,515 --> 00:46:59,709
Strangle me!

349
00:49:23,894 --> 00:49:24,883
Do you have to go?

350
00:49:25,028 --> 00:49:26,017
I'll come back soon.

351
00:49:26,163 --> 00:49:26,993
No.

352
00:49:27,664 --> 00:49:28,756
No! No!

353
00:49:29,833 --> 00:49:32,529
Please don't go.

354
00:49:33,070 --> 00:49:34,128
What's the matter?

355
00:49:34,972 --> 00:49:36,303
I'll be back soon.

356
00:49:38,442 --> 00:49:39,568
You'll see me soon.

357
00:49:39,710 --> 00:49:44,374
No! No! Don't go!

358
00:50:02,799 --> 00:50:05,927
<i>Hello. Mr. Hiroshi Takagi?</i>

359
00:50:06,403 --> 00:50:07,301
Yes.

360
00:50:08,572 --> 00:50:11,097
<i>This Is the police station.</i>

361
00:50:11,842 --> 00:50:14,333
<i>We have a young woman here...</i>

362
00:50:14,711 --> 00:50:18,909
<i>She won't tell us anything,
not even her name.</i>

363
00:50:19,583 --> 00:50:23,781
<i>All she gave us was your name
and phone number.</i>

364
00:50:24,121 --> 00:50:28,251
<i>I'm sorry we had to call so late.</i>

365
00:50:34,931 --> 00:50:36,228
You are Mr. Takagi?

366
00:50:50,814 --> 00:50:52,247
So, you turned up.

367
00:50:54,518 --> 00:50:56,418
What's got into you?

368
00:50:57,854 --> 00:51:01,950
Why are you chasing a dead woman?

369
00:51:02,793 --> 00:51:04,988
What about those of us still living?

370
00:51:05,796 --> 00:51:10,563
All your happy, false memories.
What chance do I have against all those?

371
00:51:18,341 --> 00:51:20,036
Mr. Takagi.

372
00:51:20,177 --> 00:51:20,905
Please, Mr. Takagi.

373
00:51:23,613 --> 00:51:24,671
Please, step back!

374
00:51:24,815 --> 00:51:25,509
Be careful!

375
00:51:25,982 --> 00:51:26,744
Mr. Takagi!

376
00:51:26,883 --> 00:51:27,679
Look out!

377
00:52:20,904 --> 00:52:23,236
I'm confused about "time".

378
00:52:26,943 --> 00:52:28,843
What is my consciousness?

379
00:52:31,248 --> 00:52:35,514
Is it like the story of the
future robots?

380
00:52:37,954 --> 00:52:38,886
That's it.

381
00:52:39,823 --> 00:52:44,089
People programmed robots
with their memories.

382
00:52:44,528 --> 00:52:45,756
After that...

383
00:52:47,731 --> 00:52:50,529
...all the humans died out.

384
00:52:50,901 --> 00:52:52,960
A robot was stranded on Mars.

385
00:52:54,037 --> 00:52:59,065
Before the end there was a huge,
electrical memory surge.

386
00:53:02,812 --> 00:53:06,441
So, me being here,
and the times when I meet Ryoko...

387
00:53:06,583 --> 00:53:09,882
...it's like the robot is showing me.

388
00:53:10,020 --> 00:53:12,352
An electrical broadcast of
my final moments.

389
00:53:15,292 --> 00:53:16,418
Hiroshi.

390
00:53:17,794 --> 00:53:20,024
It's because your memory is returning.

391
00:53:20,931 --> 00:53:22,091
They're memories.

392
00:53:23,833 --> 00:53:25,198
As for Ryoko.

393
00:53:26,636 --> 00:53:29,161
Well, she's gone now.

394
00:53:29,372 --> 00:53:30,737
No, that's wrong.

395
00:53:31,041 --> 00:53:32,770
They aren't memories.

396
00:53:33,777 --> 00:53:37,440
It's not as clear-cut as that.

397
00:53:41,151 --> 00:53:42,379
I think...

398
00:53:44,454 --> 00:53:47,116
No good to think too much about her.

399
00:53:48,725 --> 00:53:50,192
And the dissection.

400
00:53:51,628 --> 00:53:53,858
You should stop doing it.

401
00:53:53,997 --> 00:53:54,827
No way.

402
00:53:54,965 --> 00:53:56,489
You've done your best.

403
00:53:57,567 --> 00:53:58,329
But it's time to give up.

404
00:53:58,468 --> 00:53:59,457
No!

405
00:54:01,404 --> 00:54:02,837
I don't care what you say.

406
00:54:04,608 --> 00:54:08,339
I'll tell Dr. Kashiwabuchi
to take Ryoko's body away.

407
00:54:08,945 --> 00:54:10,936
You do that, and I'll kill you.

408
00:55:12,309 --> 00:55:15,301
I wanted to think you had killed her.

409
00:55:17,947 --> 00:55:22,441
But the light had gone from her eyes
long before that.

410
00:55:25,155 --> 00:55:27,055
It was when she was in high school.

411
00:55:28,858 --> 00:55:30,655
She was fine before then.

412
00:55:33,997 --> 00:55:35,931
Why did she lose it, I wonder.

413
00:55:39,002 --> 00:55:42,665
Even we didn't know.
And we'll never know now.

414
00:55:46,576 --> 00:55:49,943
I only saw the light one other time.

415
00:55:51,648 --> 00:55:53,377
Just after the accident.

416
00:55:56,820 --> 00:56:00,415
It was after the operation
at the local hospital.

417
00:56:01,825 --> 00:56:05,886
Your dad suggested she be moved
to the university hospital.

418
00:56:08,498 --> 00:56:09,829
That was it.

419
00:56:11,301 --> 00:56:15,101
She was fully conscious
for 5 or 6 hours.

420
00:56:19,442 --> 00:56:23,208
It was then she mentioned
leaving her body to science.

421
00:56:26,282 --> 00:56:27,749
After she died...

422
00:56:29,085 --> 00:56:32,486
...her body should be taken to
the university hospital, she told.

423
00:56:34,858 --> 00:56:37,349
I've never seen her so serious.

424
00:56:40,296 --> 00:56:43,265
But that wasn't easy to accept for me.

425
00:56:44,934 --> 00:56:46,196
No way.

426
00:56:47,437 --> 00:56:51,999
Why should she want to inflict
more wounds, even after she died?

427
00:56:54,244 --> 00:56:57,941
I couldn't understand why
she wanted this.

428
00:57:01,284 --> 00:57:08,281
How could I agree to that when she
was still there, alive, in front of me?

429
00:57:13,129 --> 00:57:16,223
Soon after that she lost consciousness.

430
00:57:17,200 --> 00:57:20,260
A vegetable, according to the doctor.

431
00:57:24,908 --> 00:57:29,004
She was moved to a private room.

432
00:57:31,047 --> 00:57:34,744
My wife and I were beside her.

433
00:57:38,922 --> 00:57:41,789
It feels like a dream now.

434
00:57:43,693 --> 00:57:45,923
Just when I thought "It's all over"...

435
00:57:47,063 --> 00:57:52,558
I saw Ryoko sitting up in bed.

436
00:57:53,837 --> 00:57:55,896
I thought she had recovered.

437
00:57:59,042 --> 00:58:01,272
My wife was asleep.

438
00:58:04,147 --> 00:58:07,082
I saw the curtain at the window.

439
00:58:08,418 --> 00:58:11,979
It was moving gently in the wind.

440
00:58:14,891 --> 00:58:18,258
Ryoko looked so refreshed.

441
00:58:22,899 --> 00:58:24,924
She looked out of the window.

442
00:58:26,102 --> 00:58:28,536
Then she turned slowly to me.

443
00:58:31,040 --> 00:58:35,272
"Don't forget my request", she said.

444
00:58:37,013 --> 00:58:39,413
She spoke as plain as day.

445
00:58:46,289 --> 00:58:49,486
She died that same evening.

446
00:58:57,267 --> 00:58:58,962
When she's finally laid out...

447
00:59:00,236 --> 00:59:01,794
Put these in her coffin.

448
00:59:05,108 --> 00:59:06,132
It's okay now.

449
00:59:07,944 --> 00:59:09,969
You can forget about her.

450
00:59:10,747 --> 00:59:12,237
You must be tired, too.

451
00:59:13,416 --> 00:59:14,678
Not at all.

452
00:59:15,151 --> 00:59:16,880
I still have a long way to go yet.

453
00:59:19,822 --> 00:59:21,050
It's not right.

454
00:59:21,891 --> 00:59:24,724
I don't care that his
memory was affected.

455
00:59:25,395 --> 00:59:27,056
It's not natural.

456
00:59:28,498 --> 00:59:29,795
It's unforgivable.

457
00:59:40,410 --> 00:59:41,399
In a way...

458
00:59:43,313 --> 00:59:46,908
...it reminds me how you were
when they brought you here.

459
00:59:49,319 --> 00:59:52,755
How could they refuse
when you were so serious.

460
00:59:55,191 --> 00:59:59,389
All the memories of you
came flooding back.

461
01:00:01,064 --> 01:00:02,361
It was frightening.

462
01:00:07,203 --> 01:00:09,068
I have to finish up now.

463
01:00:16,546 --> 01:00:19,014
Just another minute.

464
01:00:21,217 --> 01:00:22,206
Well?

465
01:00:27,423 --> 01:00:30,256
All right, you can take her.

466
01:00:33,896 --> 01:00:36,296
No, hold on.
Just another minute.

467
01:00:37,900 --> 01:00:39,299
Why putting it off?

468
01:00:40,703 --> 01:00:43,263
I think it's better that
I take her now.

469
01:00:44,607 --> 01:00:47,075
We'll take good care of her here.

470
01:00:49,746 --> 01:00:54,649
If I'd known this would happen, I wouldn't
have asked you to bring her here.

471
01:00:56,085 --> 01:00:57,313
It's just coincidence...

472
01:00:58,421 --> 01:01:00,855
that she became Hiroshi's subject.

473
01:01:01,958 --> 01:01:05,291
Makes no difference you asked or not.

474
01:01:07,096 --> 01:01:09,690
I can't change the rules.

475
01:05:36,966 --> 01:05:39,935
I didn't want to die

476
01:05:43,439 --> 01:05:45,873
Where do I go?

477
01:05:46,943 --> 01:05:48,570
I'm scared.

478
01:05:49,912 --> 01:05:52,005
I'm scared to be alone.

479
01:05:53,082 --> 01:05:54,071
I'm sorry.

480
01:05:55,318 --> 01:05:56,410
I'm really sorry.

481
01:05:57,186 --> 01:05:58,676
I won't leave you.

482
01:05:59,255 --> 01:06:00,779
I'll always be by your side.

483
01:06:01,791 --> 01:06:04,624
I won't leave you alone.

484
01:06:30,619 --> 01:06:31,813
This is reality.

485
01:06:32,955 --> 01:06:34,320
This is real.

486
01:06:42,431 --> 01:06:45,298
I'm going to stay here.

487
01:06:46,502 --> 01:06:47,901
And we'll have a baby.

488
01:06:48,037 --> 01:06:49,095
Yes!

489
01:07:08,557 --> 01:07:09,785
Yeah!

490
01:08:11,954 --> 01:08:12,943
Ryoko?

491
01:08:33,542 --> 01:08:34,531
Hey!

492
01:09:09,678 --> 01:09:14,615
Our 4-month dissection program
is now over.

493
01:09:14,984 --> 01:09:18,545
Check that bones and organs
have been replaced.

494
01:09:19,255 --> 01:09:22,884
Check the kidneys are on
the correct side.

495
01:09:32,902 --> 01:09:35,427
Put the sash next to the hands.

496
01:09:44,446 --> 01:09:47,381
The hose and sandals go by the feet.

497
01:09:49,919 --> 01:09:53,650
The triangular cloth and the headdress
go by the head.

498
01:10:00,462 --> 01:10:04,296
Drape the kimono over the body.

499
01:10:09,872 --> 01:10:12,534
Place the cane next to the right hand.

500
01:10:15,044 --> 01:10:17,774
The laying out is now complete.

501
01:10:21,250 --> 01:10:24,014
Lay the flowers inside.

502
01:10:51,814 --> 01:10:55,113
Now place the lid on the coffin.

503
01:11:14,103 --> 01:11:16,765
I'll give out the coffin labels now.

504
01:11:16,972 --> 01:11:21,306
Group 1, number 796.
Mr. Satoru Kamata.

505
01:11:27,750 --> 01:11:32,414
Group 2, number 797.
Ms. Tamae Sugawara.

506
01:11:33,522 --> 01:11:37,891
Group 3, number 798.
Mr. Shuji Kinuta.

507
01:11:39,762 --> 01:11:44,495
Group 4, number 799.
Ms. Ryoko Ohyama.

508
01:12:22,771 --> 01:12:25,740
We will now close our eyes
and pay our last respects.

509
01:12:38,687 --> 01:12:40,518
The coffins will be taken now.

510
01:12:41,023 --> 01:12:43,719
Listen for your number carefully.

511
01:12:44,126 --> 01:12:45,650
There must be no mistakes.

512
01:13:48,524 --> 01:13:49,513
I'm sorry.

513
01:13:52,728 --> 01:13:53,854
Thank you.

514
01:14:35,404 --> 01:14:37,338
There is still some time.

515
01:14:37,473 --> 01:14:40,033
Please wait in the reception room.

516
01:14:40,175 --> 01:14:42,609
Let me take you to the room.

517
01:14:44,246 --> 01:14:45,611
Let's move.

518
01:14:46,181 --> 01:14:48,479
This way, please.

519
01:15:11,940 --> 01:15:14,500
We're ready, Mr. Ohyama.

520
01:15:14,877 --> 01:15:15,866
Okay.

521
01:15:22,718 --> 01:15:23,650
It's time.

522
01:15:26,188 --> 01:15:27,815
Is Hiroshi here?

523
01:15:30,459 --> 01:15:32,450
How's his memory?

524
01:15:33,962 --> 01:15:34,951
Much better.

525
01:15:36,231 --> 01:15:38,324
So will he be giving up medicine?

526
01:15:39,735 --> 01:15:44,900
We told him it's his own decision.

527
01:15:47,009 --> 01:15:48,169
And he said...

528
01:15:50,712 --> 01:15:52,771
he'll continue.

529
01:16:04,626 --> 01:16:05,718
It's time.

530
01:17:08,023 --> 01:17:10,924
It is time to lay the body to rest.

531
01:17:33,382 --> 01:17:35,543
It's okay.
You have to be brave.

532
01:18:16,491 --> 01:18:17,617
It is time to part.

533
01:18:18,093 --> 01:18:19,856
Let us pray.

534
01:18:38,280 --> 01:18:40,043
<i>You know, I often think...</i>

535
01:18:41,083 --> 01:18:46,385
<i>If you could see some part of your life
again, years after you die...</i>

536
01:18:46,588 --> 01:18:48,078
<i>...which part would you choose?</i>

537
01:18:50,792 --> 01:18:53,727
<i>The last images of the last Martian robot.</i>

538
01:18:54,730 --> 01:18:56,357
<i>Mankindís final memory.</i>

539
01:18:57,165 --> 01:19:00,464
<i>You still have so long to live,
so you can't answer properly.</i>

540
01:19:01,636 --> 01:19:02,762
<i>As for me...</i>

541
01:19:19,588 --> 01:19:23,684
<i>Ah, it smells wonderful.</i>

542
01:19:35,570 --> 01:19:36,832
Cast:

543
01:19:38,440 --> 01:19:42,604
Tadanobu AS ANO as Hiroshi Takagi

544
01:19:44,613 --> 01:19:47,605
Nami TSUKAMOTO as Ryoko Ohyama

545
01:19:47,816 --> 01:19:51,047
KIKI as Ikumi Yoshimoto

546
01:19:51,253 --> 01:19:53,881
Kazuyoshi KUSHID A as Hiroshi's father

547
01:19:54,022 --> 01:19:56,354
Go RIJYU as Dr. Nakai

548
01:19:56,558 --> 01:19:59,493
Jyun KUNIMURA as Ryoko's father

549
01:19:59,628 --> 01:20:02,791
Ittoku KISHIBE as Dr. Kashiwabuchi

550
01:21:09,531 --> 01:21:10,725
Staff:

551
01:21:11,600 --> 01:21:16,537
Directed by Shinya TSUKAMOTO

552
01:21:16,738 --> 01:21:21,300
Producers: Shinya TSUKAMOTO
Keiko and Kouichi KUS AKABE
Shinichi KAWAHARA

553
01:21:21,509 --> 01:21:26,537
Script, Camera, Artistic Direction and Editing
by Shinya TSUKAMOTO

554
01:21:26,848 --> 01:21:29,408
Sound Recording: Yoshiya OBARA

555
01:21:29,718 --> 01:21:32,380
Camera Operation: Takayuki SHID A

556
01:21:32,554 --> 01:21:35,045
Lighting: Keisuke YOSHID A

557
01:21:35,257 --> 01:21:38,886
Plastic remodeling of human body and parts:
Takashi OD A

558
01:21:39,060 --> 01:21:41,688
Music: Chu ISHIKAWA

559
01:21:41,863 --> 01:21:44,832
Assistant Directors:
Takeshi KOIDE, Hisakatsu KUROKI

560
01:21:45,033 --> 01:21:49,026
Ending Theme Song: 'blue bird'
Written, composed and sung by Cocco

561
01:21:49,170 --> 01:21:51,104
Foreign Affairs: Kiyo JOO

562
01:24:55,623 --> 01:24:58,285
English subtitles: Dean Gascoign

