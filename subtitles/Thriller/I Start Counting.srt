1
00:00:05,022 --> 00:00:06,289
(All the characters, organizations, places, and happenings,,,)

2
00:00:06,289 --> 00:00:07,758
(in this drama are all fake and are not based on a true story,)

3
00:00:14,097 --> 00:00:15,165
(Arrest Warrant)

4
00:00:15,165 --> 00:00:17,067
We have an arrest warrant for Jang Pil Sung,

5
00:00:17,067 --> 00:00:18,068
Take him away,

6
00:00:23,440 --> 00:00:24,908
Who are you?

7
00:00:24,908 --> 00:00:25,976
Get out!

8
00:00:27,010 --> 00:00:30,313
Congressman, I will protect you!

9
00:00:32,015 --> 00:00:33,083
Get out!

10
00:00:34,684 --> 00:00:35,986
Jang Pil Sung!

11
00:00:39,189 --> 00:00:40,424
No,

12
00:01:09,219 --> 00:01:10,287
What's wrong?

13
00:01:11,288 --> 00:01:12,989
The person Mr, Cha killed,,,

14
00:01:17,527 --> 00:01:18,628
is my mother,

15
00:01:23,133 --> 00:01:25,035
Congressman Lee Kwang Ho ordered me to murder,,,

16
00:01:25,135 --> 00:01:30,407
a mother and a son who doesn't have a place to live,

17
00:01:30,941 --> 00:01:33,176
- The woman's name is,,, - "The woman's name is,,,"

18
00:01:33,443 --> 00:01:34,878
- Choi,,, - "Choi,,,"

19
00:01:34,878 --> 00:01:36,179
- Yeon,,, - "Yeon,,,"

20
00:01:36,179 --> 00:01:37,481
- Mi, - "Mi",

21
00:01:41,451 --> 00:01:42,519
Mom,

22
00:01:43,120 --> 00:01:44,154
Mom!

23
00:01:44,888 --> 00:01:45,956
Mom!

24
00:01:47,057 --> 00:01:50,460
Mom! Mom!

25
00:02:19,256 --> 00:02:21,658
(Episode 33)

26
00:02:35,639 --> 00:02:36,706
Choi Yeon,,,

27
00:02:41,444 --> 00:02:42,479
Choi Yeon Mi,

28
00:02:43,813 --> 00:02:44,881
Do you know her?

29
00:02:50,754 --> 00:02:51,755
This woman,,,

30
00:02:51,755 --> 00:02:52,756
(The woman's name is Choi Yeon Mi,)

31
00:02:53,123 --> 00:02:55,158
20 years ago, on a rainy night,,,

32
00:02:57,527 --> 00:02:58,562
Why,,,

33
00:03:00,230 --> 00:03:01,531
did you kill her?

34
00:03:09,506 --> 00:03:10,707
I can't,,,

35
00:03:11,975 --> 00:03:13,410
tell you,

36
00:03:13,577 --> 00:03:16,213
I already know everything, Did Lee Kwang Ho tell you to kill her?

37
00:03:18,915 --> 00:03:20,116
What was the reason?

38
00:03:20,584 --> 00:03:22,586
Why did he order you to kill her?

39
00:03:24,287 --> 00:03:25,655
I don't know,

40
00:03:26,389 --> 00:03:27,857
I really don't,

41
00:03:29,226 --> 00:03:31,428
I never asked him why,

42
00:03:31,528 --> 00:03:34,898
And he never told me why,

43
00:03:35,732 --> 00:03:37,734
Then why didn't you kill the child?

44
00:03:38,835 --> 00:03:40,737
It says here that you were ordered to kill both of them,

45
00:03:40,737 --> 00:03:41,805
That's because,,,

46
00:03:46,943 --> 00:03:48,011
I figured that,,,

47
00:03:48,812 --> 00:03:52,716
the boy couldn't have done anything wrong,

48
00:03:53,683 --> 00:03:54,718
Then,,,

49
00:03:55,585 --> 00:03:56,686
what about Choi Yeon Mi?

50
00:03:58,355 --> 00:04:00,223
What did she do wrong?

51
00:04:00,757 --> 00:04:03,126
You said you didn't know why he wanted her killed!

52
00:04:03,126 --> 00:04:05,195
How could you kill her just because he told you to?

53
00:04:09,566 --> 00:04:10,634
I'm sorry,

54
00:04:11,067 --> 00:04:13,436
Sir, I'm sorry,

55
00:04:15,438 --> 00:04:18,475
Please forgive me, I have sinned,

56
00:04:18,475 --> 00:04:20,477
And please bless me with your grace,

57
00:04:20,710 --> 00:04:22,912
I'll live a kind life from now on,

58
00:04:22,912 --> 00:04:25,849
I pray in your name, Amen,

59
00:04:25,849 --> 00:04:27,350
Hey, Mr, Cha Gwang Tae,

60
00:04:30,120 --> 00:04:31,821
Where's Gook Su Ran?

61
00:04:33,423 --> 00:04:35,158
Please ask her to come,

62
00:04:35,492 --> 00:04:38,328
I'll tell her everything,

63
00:04:40,030 --> 00:04:42,065
- Gook Su Ran? - Yes,

64
00:04:43,533 --> 00:04:46,002
She saved my life,

65
00:04:49,572 --> 00:04:50,774
I'm not going to,,,

66
00:04:52,809 --> 00:04:54,377
say anything to anyone other than Gook Su Ran,

67
00:04:57,647 --> 00:05:01,117
Please forgive me, I have sinned,

68
00:05:01,117 --> 00:05:02,952
And please bless me with your grace,

69
00:05:02,952 --> 00:05:04,988
I'll live a kind life from now on,

70
00:05:15,732 --> 00:05:16,800
Dad,

71
00:05:19,436 --> 00:05:21,471
Please postpone the business operation in Tunisia,

72
00:05:21,471 --> 00:05:24,207
I can't believe I even think of you as my son,

73
00:05:36,986 --> 00:05:37,987
Here,

74
00:05:42,225 --> 00:05:43,293
Su Chil,

75
00:05:44,894 --> 00:05:49,265
I've been working with you as a cop for over 10 years,

76
00:05:50,567 --> 00:05:53,937
But I really can't hide how disappointed I am today,

77
00:05:57,240 --> 00:05:58,408
My gosh,

78
00:05:59,309 --> 00:06:01,511
It's okay, Things like this can happen, Drink up,

79
00:06:02,312 --> 00:06:04,614
Inspector Park, Sergeant Kim,

80
00:06:05,582 --> 00:06:06,950
I'm so sorry,

81
00:06:07,250 --> 00:06:10,787
We lost both Jang Pil Sung and General Manager Kwak,

82
00:06:11,988 --> 00:06:15,859
On top of that, I got beaten up, I'm so ashamed of myself,

83
00:06:21,531 --> 00:06:24,267
I'm really sorry, you guys,

84
00:06:24,267 --> 00:06:25,268
No,

85
00:06:26,936 --> 00:06:28,271
I'm the one who's sorry,

86
00:06:30,640 --> 00:06:32,409
What are you doing? Are you crying?

87
00:06:32,709 --> 00:06:34,144
I'm sorry,

88
00:06:34,577 --> 00:06:35,578
What?

89
00:06:36,279 --> 00:06:37,747
What's he saying?

90
00:06:37,947 --> 00:06:40,316
Just let him cry,

91
00:06:40,617 --> 00:06:44,087
A shot of soju is his limit,

92
00:06:44,254 --> 00:06:46,089
Is he acting like this because he's drunk?

93
00:06:48,358 --> 00:06:49,392
Drink up,

94
00:06:49,526 --> 00:06:52,061
I got so angry earlier,,,

95
00:06:52,061 --> 00:06:54,230
on our way to catch Lee Kwang Ho,

96
00:06:54,397 --> 00:06:56,366
I get that he was already rich,

97
00:06:56,366 --> 00:06:58,201
And I don't care that he used that money,,,

98
00:06:58,201 --> 00:07:00,570
to buy a land in the middle of Seoul and build a house there,

99
00:07:01,070 --> 00:07:02,205
But I don't get why,,,

100
00:07:02,605 --> 00:07:07,043
he uses our people's taxes to hire his bodyguards,

101
00:07:07,243 --> 00:07:11,648
Those guys who were protecting him are the same cops as us,

102
00:07:11,648 --> 00:07:15,185
Hey, be quiet, We're at a public place,

103
00:07:16,219 --> 00:07:17,287
Hey,

104
00:07:17,687 --> 00:07:20,757
That's how our country's law works,

105
00:07:21,591 --> 00:07:22,792
What did Mr, Cha say?

106
00:07:24,661 --> 00:07:26,896
I think Lee Kwang Ho ordered him to do it,

107
00:07:27,697 --> 00:07:29,299
But I don't think he knows why,

108
00:07:31,468 --> 00:07:35,205
Lee Kwang Ho wouldn't have told him why he wanted her dead,

109
00:07:35,738 --> 00:07:37,640
But there's one thing that bothers me,

110
00:07:38,808 --> 00:07:41,478
My mom didn't die because of a hit and run,

111
00:07:41,478 --> 00:07:42,512
Really?

112
00:07:43,079 --> 00:07:45,982
Then there must be a record of her accident, I'll look for it,

113
00:07:48,051 --> 00:07:49,752
Is there anything else,,,

114
00:07:50,453 --> 00:07:53,389
you remember about your mom?

115
00:07:54,023 --> 00:07:56,793
Anything that seems weird now that you think of it,

116
00:07:58,161 --> 00:08:01,364
When I think back of my childhood, everything was weird,

117
00:08:02,165 --> 00:08:05,468
But I never knew that it had anything to do with Lee Kwang Ho,

118
00:08:06,236 --> 00:08:07,637
What was weird?

119
00:08:09,305 --> 00:08:10,807
We were homeless,

120
00:08:17,580 --> 00:08:21,084
Mom, this place is clean, Let's sleep here tonight,

121
00:08:21,751 --> 00:08:23,253
Okay,

122
00:08:34,097 --> 00:08:36,266
You didn't brush your teeth,

123
00:08:36,266 --> 00:08:37,600
Here you go,

124
00:08:38,101 --> 00:08:41,838
You need to brush it like this 30 times,

125
00:08:41,838 --> 00:08:44,407
I know, You always tell me that,

126
00:08:45,475 --> 00:08:47,677
- Here, You should do it too, - Okay,

127
00:09:01,858 --> 00:09:05,128
Don't look at me like that, I didn't suffer at all back then,

128
00:09:05,528 --> 00:09:07,330
My mom was always with me by my side,

129
00:09:08,131 --> 00:09:12,068
We'd chat and have fun all night in public bathrooms,

130
00:09:12,402 --> 00:09:14,337
She also taught me how to read,

131
00:09:14,671 --> 00:09:17,407
Have you ever tried asking the police or city hall for help?

132
00:09:17,407 --> 00:09:20,376
There was no need to because we got arrested all the time,

133
00:09:21,210 --> 00:09:24,781
People always reported us for sleeping in a public bathroom,

134
00:09:25,381 --> 00:09:27,717
There were times when we got taken to the police station,

135
00:09:28,151 --> 00:09:29,218
And,,,

136
00:09:30,286 --> 00:09:33,756
some people from a welfare center also came and talked to us,

137
00:09:34,457 --> 00:09:36,859
They even got us a place to stay,

138
00:09:37,660 --> 00:09:40,763
But a few days later, my mom and I ran away from that place,

139
00:09:41,864 --> 00:09:45,068
Do you think she was trying to run away from Lee Kwang Ho?

140
00:09:45,068 --> 00:09:48,171
Now that I think about it, yes,

141
00:09:50,406 --> 00:09:51,874
I wonder why,

142
00:09:53,509 --> 00:09:56,613
And how did your mom know Lee Kwang Ho?

143
00:09:57,814 --> 00:09:58,815
I don't know,

144
00:10:03,219 --> 00:10:05,054
We have to find another way,

145
00:10:06,689 --> 00:10:07,690
Like what?

146
00:10:08,391 --> 00:10:09,525
We'll make Mr, Cha,,,

147
00:10:12,462 --> 00:10:13,863
remember,

148
00:10:26,376 --> 00:10:27,543
You're starting to grow on me,

149
00:10:28,144 --> 00:10:29,979
Because I've been looking at you all day,

150
00:10:31,581 --> 00:10:35,184
Chairman Gook, The 48 hours are almost up,

151
00:10:36,052 --> 00:10:39,589
Either you will be investigated without physical detention,,,

152
00:10:40,456 --> 00:10:43,493
or you will be given an arrest warrant,

153
00:10:43,493 --> 00:10:45,762
and be investigated in lockup,

154
00:10:46,529 --> 00:10:48,865
The proof I have is very useful,

155
00:10:48,998 --> 00:10:50,633
So they'll issue an arrest warrant right away,

156
00:10:51,901 --> 00:10:55,805
Are you ready to sleep in a lockup starting tonight?

157
00:11:05,081 --> 00:11:08,017
Will you two say "Stop!" again?

158
00:11:08,951 --> 00:11:12,422
I won't let you, I won't be fooled twice,

159
00:11:18,428 --> 00:11:19,462
What's that?

160
00:11:20,163 --> 00:11:21,764
It's Mr, Cha's notebook,

161
00:11:22,065 --> 00:11:23,866
It's a record of all the things he did for Lee Kwang Ho,

162
00:11:24,400 --> 00:11:28,471
I can't believe you brought this to me,

163
00:11:29,772 --> 00:11:32,075
You must've been worried that I'd go astray,

164
00:11:32,575 --> 00:11:34,844
I didn't bring it for you,

165
00:11:37,447 --> 00:11:39,248
It's for Chairman Gook,

166
00:11:41,984 --> 00:11:44,353
You put it on my desk which means it's for me,

167
00:11:44,353 --> 00:11:46,689
Don't deny it,

168
00:11:52,695 --> 00:11:54,363
This is amazing,

169
00:11:57,133 --> 00:11:58,401
Where's the other half?

170
00:11:59,635 --> 00:12:01,938
Where is Mr, Cha?

171
00:12:02,271 --> 00:12:04,640
If we get him to testify to back this up,,,

172
00:12:04,640 --> 00:12:05,775
That's why I'm here,

173
00:12:06,843 --> 00:12:07,944
Release her,,,

174
00:12:09,278 --> 00:12:10,713
for one day,

175
00:12:14,016 --> 00:12:16,586
He said he would tell us everything if he sees Chairwoman Gook,

176
00:12:18,988 --> 00:12:22,592
Release her to get an Alzheimer's patient to testify?

177
00:12:23,493 --> 00:12:25,595
He does tell truth time to time,

178
00:12:26,062 --> 00:12:29,265
General Manager Kwak has the rest of the notes,

179
00:12:29,532 --> 00:12:31,234
If we're going to do anything with the notebook,,,

180
00:12:31,234 --> 00:12:34,237
You're saying Chairwoman Gook has to be released,

181
00:12:36,105 --> 00:12:37,140
Yes,

182
00:12:40,476 --> 00:12:43,379
What if we get nothing out of her release?

183
00:12:44,213 --> 00:12:47,583
There should be some kind of collateral,

184
00:12:48,351 --> 00:12:49,418
I'll,,,

185
00:12:52,054 --> 00:12:53,656
give you want you want then,

186
00:12:55,858 --> 00:12:57,026
For example?

187
00:12:57,326 --> 00:12:58,694
Like a press conference,

188
00:13:00,029 --> 00:13:03,933
With proof that Manager Lee and the NIS had a long relationship,

189
00:13:04,200 --> 00:13:06,636
Then you'll have to be,,,

190
00:13:06,636 --> 00:13:08,337
put in jail as well,

191
00:13:09,305 --> 00:13:12,241
That's why you haven't talked all this time,

192
00:13:12,241 --> 00:13:14,243
Why would you bring yourself down now?

193
00:13:15,011 --> 00:13:17,213
I can't trust you,

194
00:13:25,755 --> 00:13:27,957
Gosh, you're making it difficult,

195
00:13:28,257 --> 00:13:30,059
I'll make sure to bring her back,

196
00:13:30,259 --> 00:13:33,329
No, I'll tag along,

197
00:13:34,263 --> 00:13:36,866
There is nothing else I can do at this time anyway,

198
00:13:42,672 --> 00:13:45,942
You should call General Manager Kwak first,

199
00:13:49,245 --> 00:13:53,282
I'm sorry, sir, I was trying to run away from the police,

200
00:13:53,416 --> 00:13:56,052
Thanks to these men I was safe,

201
00:13:56,786 --> 00:13:58,788
Anyway, thanks to you, sir,,,

202
00:13:58,855 --> 00:14:00,256
You may leave the room,

203
00:14:06,028 --> 00:14:08,397
I couldn't get the whole thing, This is all I have,

204
00:14:10,299 --> 00:14:11,334
That's fine,

205
00:14:12,001 --> 00:14:13,202
You should leave,

206
00:14:16,038 --> 00:14:17,106
Yes, sir,

207
00:14:39,328 --> 00:14:40,863
Mr, Cha,

208
00:14:42,131 --> 00:14:43,766
That little,,,

209
00:14:50,373 --> 00:14:51,574
It's me, Attorney An,

210
00:14:52,241 --> 00:14:53,876
There's something you should look into,

211
00:14:55,111 --> 00:14:56,846
(Erim Food)

212
00:15:03,352 --> 00:15:04,954
Where's the other half of the notebook?

213
00:15:05,154 --> 00:15:07,056
I heard you took it,

214
00:15:11,260 --> 00:15:12,828
You can tell me,

215
00:15:13,462 --> 00:15:15,564
Well,,, The thing is,,,

216
00:15:15,831 --> 00:15:18,801
The guards caught me, and I was brought to Manager Lee,

217
00:15:21,804 --> 00:15:23,439
I didn't mean to give it to him,

218
00:15:23,439 --> 00:15:25,041
Whether you meant to or not,

219
00:15:25,608 --> 00:15:28,678
what matters is that it's with Lee Kwang Ho now,

220
00:15:29,278 --> 00:15:30,680
You're right, but,,,

221
00:15:31,614 --> 00:15:33,282
I read what was written on it,

222
00:15:33,416 --> 00:15:34,817
and there wasn't anything crucial,

223
00:15:34,951 --> 00:15:36,152
That's possible,

224
00:15:36,919 --> 00:15:38,988
It was right before the presidential election,

225
00:15:38,988 --> 00:15:40,623
so he laid low at the time,

226
00:15:41,357 --> 00:15:45,494
All Mr, Cha did was delivering money,

227
00:15:48,631 --> 00:15:49,832
You can go,

228
00:15:51,801 --> 00:15:52,802
Okay,

229
00:15:57,273 --> 00:15:58,808
So that means,,,

230
00:15:59,675 --> 00:16:03,079
based on what Mr, Cha wrote in the notebook,

231
00:16:03,412 --> 00:16:07,083
we have to find cases that can bring Lee Kwang Ho down,

232
00:16:10,686 --> 00:16:13,356
Then which case will you go after first?

233
00:16:18,928 --> 00:16:20,196
(I buried a 3-year-old kid,)

234
00:16:21,964 --> 00:16:23,666
The kid who died?

235
00:16:25,267 --> 00:16:26,302
But I don't think,,,

236
00:16:27,136 --> 00:16:29,472
Lee Kwang Ho has anything to do with that case,

237
00:16:30,473 --> 00:16:33,476
It just says he buried a kid on this date,

238
00:16:33,909 --> 00:16:36,812
It says nothing about Lee Kwang Ho's order,

239
00:16:36,812 --> 00:16:38,280
Yes, he gave an order,

240
00:16:38,280 --> 00:16:39,949
Lee Kwang Ho killed the kid too,

241
00:16:40,149 --> 00:16:41,183
What?

242
00:16:41,817 --> 00:16:43,119
Who was that kid?

243
00:16:45,421 --> 00:16:47,056
The boy Lee Kwang Ho adopted,

244
00:16:48,090 --> 00:16:49,658
The real Lee Young Min,

245
00:16:51,127 --> 00:16:54,263
Then what about the Lee Young Min I put in the psychiatric ward?

246
00:16:54,463 --> 00:16:55,564
He's a fake,

247
00:16:56,832 --> 00:16:59,301
He couldn't say he killed the boy he adopted,

248
00:16:59,602 --> 00:17:02,271
So he secretly brought another boy to raise,

249
00:17:03,406 --> 00:17:06,275
So we find where the boy is buried,

250
00:17:06,275 --> 00:17:08,411
find his body, and,,, Wait,

251
00:17:10,446 --> 00:17:12,415
The statute of limitations already expired,

252
00:17:13,716 --> 00:17:15,451
We can't arrest him with that,

253
00:17:15,451 --> 00:17:16,485
However,

254
00:17:17,086 --> 00:17:18,721
if you can prove that,,,

255
00:17:18,721 --> 00:17:20,756
what's written here is true,

256
00:17:21,690 --> 00:17:25,194
then the public will want the other cases checked too,

257
00:17:25,494 --> 00:17:28,631
By manipulating media,

258
00:17:28,631 --> 00:17:30,299
you can agitate Lee Kwang Ho,,,

259
00:17:30,733 --> 00:17:33,269
and put pressure on the leaders of the prosecution office,

260
00:17:35,738 --> 00:17:38,841
Finding the dead body of the kid,,,

261
00:17:38,841 --> 00:17:40,743
will be the most effective way,

262
00:17:42,144 --> 00:17:45,081
We just need to find out where Mr, Cha buried him,

263
00:17:45,314 --> 00:17:46,315
But,,,

264
00:17:46,916 --> 00:17:50,186
how can we confirm this boy was the one Lee Kwang Ho adopted?

265
00:17:51,353 --> 00:17:52,688
I know,,,

266
00:17:55,057 --> 00:17:56,625
whom the kid's parents are,

267
00:17:57,193 --> 00:17:58,727
I guess you already found them,

268
00:18:00,362 --> 00:18:01,363
Who are they?

269
00:18:01,797 --> 00:18:05,034
I'll tell you once we find the dead body,

270
00:18:14,043 --> 00:18:15,177
A prosecutor,

271
00:18:16,112 --> 00:18:19,048
an officer, and the former Chief Director of NIS,

272
00:18:19,815 --> 00:18:22,051
What are they doing together?

273
00:18:22,384 --> 00:18:23,552
I don't know,

274
00:18:23,552 --> 00:18:25,521
You should know,

275
00:18:26,455 --> 00:18:28,924
I need to do something,,,

276
00:18:29,758 --> 00:18:31,927
to help Chairwoman Gook,

277
00:18:31,994 --> 00:18:34,697
Are you completely on Chairwoman Gook's side now?

278
00:18:35,598 --> 00:18:38,868
I can't side with Manager Lee all of a sudden,

279
00:18:39,735 --> 00:18:44,206
And it looks like the prosecution and the police are working with her,

280
00:18:44,707 --> 00:18:47,843
At a time like this, we should go with the government workers,

281
00:18:47,843 --> 00:18:49,612
What if we all get killed?

282
00:18:49,612 --> 00:18:52,314
That's why we have to do well, so we don't get killed,

283
00:18:52,781 --> 00:18:54,016
For example?

284
00:18:54,049 --> 00:18:58,087
For example, finding proof for Odong Blowfish House case,

285
00:18:59,622 --> 00:19:03,359
When I caught Jang Pil Sung and was bringing him to Manager Lee,

286
00:19:04,260 --> 00:19:06,028
I read all the notes,

287
00:19:07,129 --> 00:19:08,864
The one who killed Ms, Hong was,,,

288
00:19:09,598 --> 00:19:10,666
Manager Lee,

289
00:19:13,335 --> 00:19:16,605
He strangled her, Just in case they left any evidence behind,

290
00:19:16,605 --> 00:19:19,775
Mr, Cha clipped Ms, Hong's nails,

291
00:19:20,743 --> 00:19:23,245
I'm sure that Jang Pil Sung has them,

292
00:19:27,216 --> 00:19:29,652
Hey, Get a hold of yourself,

293
00:19:30,019 --> 00:19:32,988
Both of us will get killed if we don't pay attention,

294
00:19:33,522 --> 00:19:34,723
Brace yourself,

295
00:19:34,790 --> 00:19:35,791
Okay,

296
00:19:37,493 --> 00:19:39,161
Where can we find,,,

297
00:19:40,863 --> 00:19:41,931
Jang Pil Sung?

298
00:19:41,931 --> 00:19:43,499
We have to find him somehow,

299
00:19:44,099 --> 00:19:47,169
We'll have a better chance at finding him than the police,

300
00:19:47,336 --> 00:19:49,572
Because we know his cloned phone number,

301
00:19:50,906 --> 00:19:52,675
We'll inquire his call history,,,

302
00:19:52,841 --> 00:19:53,909
and wait,

303
00:19:54,710 --> 00:19:55,778
Okay,

304
00:19:59,748 --> 00:20:00,916
(Attorney An)

305
00:20:01,684 --> 00:20:03,152
It's An Tae Jung,

306
00:20:04,720 --> 00:20:06,989
(Attorney An)

307
00:20:08,891 --> 00:20:11,660
Kim Yoon Su came out with Chairwoman Gook,

308
00:20:11,994 --> 00:20:14,997
What in the world is she up to?

309
00:20:15,631 --> 00:20:17,833
Is she turning herself in and confessing to everything?

310
00:20:17,833 --> 00:20:19,468
I'm not sure of that yet,

311
00:20:19,935 --> 00:20:22,271
I just called General Manager Kwak and checked,

312
00:20:22,871 --> 00:20:25,307
Apparently, she didn't go anywhere near Erim Food,

313
00:20:30,980 --> 00:20:32,047
Yes,

314
00:20:33,916 --> 00:20:34,984
Okay,

315
00:20:38,254 --> 00:20:39,455
Chairwoman Gook,,,

316
00:20:40,456 --> 00:20:42,157
went back to the District Attorney's Office,

317
00:20:42,157 --> 00:20:45,628
That means she willingly went back there,,,

318
00:20:46,061 --> 00:20:48,430
after she made an outing with Kim Yoon Su,

319
00:20:49,031 --> 00:20:50,065
That makes no sense,

320
00:20:50,065 --> 00:20:53,435
He should either have an arrest warrant and start investigating,,,

321
00:20:53,736 --> 00:20:57,640
or let go of her if that's not the case,

322
00:21:01,210 --> 00:21:03,646
She's definitely up to something,

323
00:21:05,881 --> 00:21:08,350
Are you sure she didn't visit Erim Food?

324
00:21:11,887 --> 00:21:15,224
An Tae Jung! What are you doing right now?

325
00:21:15,391 --> 00:21:17,593
You can't even tell whether General Manager Kwak is reliable or not,

326
00:21:17,593 --> 00:21:19,395
How do you even call this a report?

327
00:21:19,628 --> 00:21:20,696
I'm sorry, sir,

328
00:21:21,230 --> 00:21:22,965
Keep an eye on him,,,

329
00:21:23,699 --> 00:21:25,901
so that he won't think about betraying me, Do you understand?

330
00:21:26,302 --> 00:21:27,369
Yes, sir,

331
00:21:36,445 --> 00:21:39,281
My gosh, it would've been great if everything went according to plan,

332
00:21:39,281 --> 00:21:41,150
Then Lee Kwang Ho would be,,,

333
00:21:41,150 --> 00:21:43,118
What can you do when luck isn't on your side?

334
00:21:46,355 --> 00:21:47,389
Chul Gi,

335
00:21:47,890 --> 00:21:52,328
To be honest, I think it's useless to try so hard to find evidence,

336
00:21:53,495 --> 00:21:55,898
They're all going to get released anyway,

337
00:21:55,898 --> 00:21:57,266
And if that's not the case,

338
00:21:57,800 --> 00:22:00,903
they'll just waste time by dragging on with their trials and stuff,

339
00:22:01,003 --> 00:22:02,404
So are you just going to give up?

340
00:22:03,806 --> 00:22:06,175
Shouldn't you at least look into your mom's case?

341
00:22:06,675 --> 00:22:08,877
The statute of limitations for that is already over,

342
00:22:09,311 --> 00:22:11,814
What's the point of looking into a case that's no longer valid?

343
00:22:12,681 --> 00:22:13,716
But,,,

344
00:22:14,116 --> 00:22:16,785
you still need to find out why he killed her,

345
00:22:21,023 --> 00:22:22,091
I,,,

346
00:22:24,793 --> 00:22:26,729
just want to kill Lee Kwang Ho,

347
00:22:29,331 --> 00:22:30,966
I want to kill him,

348
00:22:34,169 --> 00:22:36,572
- I wasn't going to say this, - Go ahead, and say it,

349
00:22:39,441 --> 00:22:41,710
"I want to kill Lee Kwang Ho," "I want him dead,"

350
00:22:42,478 --> 00:22:43,512
Just say it,

351
00:22:45,247 --> 00:22:46,615
Instead, just end it at that,

352
00:22:47,583 --> 00:22:49,618
It might seem like if you kill him,,,

353
00:22:50,119 --> 00:22:51,653
things will come to an end,

354
00:22:52,821 --> 00:22:53,889
but it won't,

355
00:22:56,392 --> 00:22:58,894
I also thought of killing him numerous times in my head,

356
00:23:00,896 --> 00:23:01,964
But,,,

357
00:23:02,598 --> 00:23:04,333
that's not winning,

358
00:23:05,667 --> 00:23:07,236
That's just losing,

359
00:23:11,106 --> 00:23:14,176
You should go to work tomorrow and work with your team,

360
00:23:14,309 --> 00:23:16,478
That's the only way you'll be able to catch him,

361
00:23:17,413 --> 00:23:19,415
I'll try looking into your mom's case separately,

362
00:23:21,917 --> 00:23:24,753
I think I'm going to sleep here tonight, That's okay, right?

363
00:24:36,024 --> 00:24:37,960
(The Mill)

364
00:24:41,096 --> 00:24:43,165
Lieutenant Jin isn't picking up,

365
00:24:44,166 --> 00:24:45,834
You could've just gone to work by yourself,

366
00:24:45,834 --> 00:24:48,570
Why did you bother coming all the way here?

367
00:24:52,007 --> 00:24:53,041
My gosh,

368
00:24:55,310 --> 00:24:58,380
Mom, you should've told me that someone's here,

369
00:24:58,380 --> 00:25:00,048
I was going to,

370
00:25:00,048 --> 00:25:02,050
But you were a bit too quick,

371
00:25:05,654 --> 00:25:08,824
I'll get going then,

372
00:25:08,991 --> 00:25:10,726
- Did you eat? - Pardon?

373
00:25:10,726 --> 00:25:12,828
- Did you eat breakfast? - No,

374
00:25:15,130 --> 00:25:16,965
It's okay, ma'am,

375
00:25:19,101 --> 00:25:21,570
It's not okay, Come inside,

376
00:25:22,638 --> 00:25:24,706
You guys are partners,

377
00:25:24,706 --> 00:25:28,243
so I'll be nice to my daughter's coworker,

378
00:25:28,610 --> 00:25:31,046
Come inside, I just need to heat the soup,

379
00:25:32,381 --> 00:25:33,682
It's,,, It's okay,

380
00:25:33,682 --> 00:25:34,850
Come inside,

381
00:25:35,417 --> 00:25:37,386
I also have something to tell you,

382
00:25:37,386 --> 00:25:38,387
Okay,

383
00:25:38,387 --> 00:25:40,455
Keep the door open even if you're just coworkers,

384
00:25:40,656 --> 00:25:42,057
A grown man and woman shouldn't,,,

385
00:25:50,666 --> 00:25:54,069
I looked everywhere for that after you left,

386
00:25:54,169 --> 00:25:55,304
This is,,,

387
00:25:55,437 --> 00:25:56,505
That's right,

388
00:25:57,039 --> 00:25:58,907
It's the record of your mom's accident,

389
00:25:59,408 --> 00:26:02,177
That record is from 20 years ago, so it's not in the best state,

390
00:26:03,345 --> 00:26:06,181
But look here, It says that the assailant is Cha Gwang Tae,

391
00:26:06,548 --> 00:26:09,117
He got caught in the crime scene, which means you were right,

392
00:26:11,286 --> 00:26:12,988
But the important thing is that,,,

393
00:26:12,988 --> 00:26:16,658
he got caught in the scene when he could've just drove away,

394
00:26:16,825 --> 00:26:19,595
If it was a hit-and-run, the cops would've started investigating,

395
00:26:19,895 --> 00:26:21,964
But I guess he didn't want my mom to get investigated,

396
00:26:22,264 --> 00:26:24,366
Bingo, That's exactly what I think,

397
00:26:24,733 --> 00:26:29,504
It means he didn't want the cops investigating your mom,

398
00:26:29,504 --> 00:26:30,539
And that reason,,,

399
00:26:30,539 --> 00:26:34,042
It'll be the same reason why Lee Kwang Ho tried to kill my mom,

400
00:26:34,610 --> 00:26:35,644
Exactly,

401
00:26:37,312 --> 00:26:40,582
But I couldn't find a single record about you,

402
00:26:43,585 --> 00:26:45,821
Maybe Cha Gwang Tae never mentioned me in his testimony,

403
00:26:46,254 --> 00:26:49,191
He left me alive when he had been ordered to kill me,

404
00:26:49,558 --> 00:26:51,727
So in order to keep that hidden,

405
00:26:51,960 --> 00:26:54,062
he could've just left that out of his testimony,

406
00:26:54,363 --> 00:26:55,931
I guess that could be a possibility,

407
00:26:56,331 --> 00:26:58,200
What did you do after that happened?

408
00:26:58,200 --> 00:26:59,301
I ran away,,,

409
00:26:59,568 --> 00:27:02,070
because some woman said she was going to send me to an orphanage,

410
00:27:03,105 --> 00:27:06,808
By any chance, was her name Kim Kkot Bi?

411
00:27:07,342 --> 00:27:09,077
I don't know, Why?

412
00:27:09,077 --> 00:27:12,114
Because it says here that someone came and made an agreement,

413
00:27:12,114 --> 00:27:13,348
Her name was Kim Kkot Bi,

414
00:27:13,348 --> 00:27:14,783
She claimed that she was your mom's friend,

415
00:27:14,783 --> 00:27:17,252
My mom's friend came and made an agreement?

416
00:27:17,252 --> 00:27:19,454
Yes, So from now on,

417
00:27:19,621 --> 00:27:23,025
we need to look into both your mom and Kim Kkot Bi,

418
00:27:24,092 --> 00:27:27,929
Let's leave that to Chul Gi, He said he'd look into it,

419
00:27:28,563 --> 00:27:32,968
We didn't tell Inspector Park every single detail,

420
00:27:34,903 --> 00:27:36,872
Don't you think we should?

421
00:27:37,839 --> 00:27:39,341
Let's wait until things become a bit clearer,

422
00:27:39,775 --> 00:27:43,845
Then, we can tell him that the woman who died back then was my mom,,,

423
00:27:44,246 --> 00:27:46,615
and that the boy who Lee Kwang Ho had ordered,,,

424
00:27:47,349 --> 00:27:49,317
to kill was me,

425
00:27:50,485 --> 00:27:53,989
Let's give Chul Gi everything we have about my mom's death,

426
00:27:54,690 --> 00:27:55,924
And let's see how things unravel,

427
00:27:56,625 --> 00:27:57,626
Okay,

428
00:28:02,564 --> 00:28:05,100
What were you guys doing? What's with the mood?

429
00:28:06,568 --> 00:28:07,669
Eat breakfast,

430
00:28:10,939 --> 00:28:12,074
It's tasty,

431
00:28:12,507 --> 00:28:14,009
Oh, okay,

432
00:28:14,009 --> 00:28:16,111
Gosh, don't touch him,

433
00:28:20,582 --> 00:28:22,684
Now, the first thing,,,

434
00:28:23,018 --> 00:28:26,054
we have to do is to catch Jang Pil Sung,

435
00:28:26,588 --> 00:28:29,891
According to Mr, Cha's memo,,,

436
00:28:30,759 --> 00:28:33,028
Hey, come over here,

437
00:28:33,428 --> 00:28:34,796
Look this way,

438
00:28:35,697 --> 00:28:38,967
Let's say she's Ms, Hong and I'm Lee Kwang Ho,

439
00:28:39,067 --> 00:28:41,603
He started off with choking her,

440
00:28:41,970 --> 00:28:43,772
See? This is what he did to her,

441
00:28:43,772 --> 00:28:45,974
Then she can't help but react to this,

442
00:28:46,074 --> 00:28:49,010
That's right, If she did this, it's highly possible that,,,

443
00:28:49,010 --> 00:28:52,013
Lee Kwang Ho's DNA was left in her nails,

444
00:28:52,013 --> 00:28:55,016
That's exactly why Mr, Cha cut her nails,

445
00:28:55,016 --> 00:28:56,384
Inspector Park!

446
00:28:57,085 --> 00:28:58,253
Gosh, I'm sorry,

447
00:29:02,858 --> 00:29:04,359
Are you okay? I'm sorry,

448
00:29:05,160 --> 00:29:06,261
My gosh,

449
00:29:07,496 --> 00:29:10,198
That means her nails has to be out there somewhere,

450
00:29:10,198 --> 00:29:13,034
Yes, Jang Pil Sung probably has them,,,

451
00:29:13,034 --> 00:29:16,404
since it could be used as crucial evidence,

452
00:29:16,404 --> 00:29:20,475
That's right, That's why we need to catch Jang Pil Sung first,

453
00:29:20,475 --> 00:29:23,712
Then why didn't he mention that when he suggested,,,

454
00:29:23,712 --> 00:29:25,180
Officer Oh to make a deal with him?

455
00:29:25,380 --> 00:29:28,784
I guess it's because Mr, Cha's notebook was enough to make a deal,

456
00:29:28,884 --> 00:29:30,218
That's exactly right,

457
00:29:30,519 --> 00:29:33,421
Judging from Jang Pil Sung's behavioral pattern,

458
00:29:33,421 --> 00:29:35,991
it's highly possible that he's keeping it so that,,,

459
00:29:36,224 --> 00:29:38,426
he can use it when he has no other choice,

460
00:29:38,426 --> 00:29:39,561
I knew it,

461
00:29:39,661 --> 00:29:43,565
That's why we've been searching all the nearby CCTV footages, Right?

462
00:29:43,565 --> 00:29:46,101
So please give us a hand, okay?

463
00:29:46,101 --> 00:29:48,036
No, you guys can finish that off,

464
00:29:48,036 --> 00:29:52,474
I want Lieutenant Jin and Officer Oh to look into Ms, Hong,

465
00:29:52,707 --> 00:29:56,211
We need to know about her to find Lee Kwang Ho's motive for murder,

466
00:29:56,211 --> 00:29:59,080
It'll be useless if we don't find out his motive behind all this,

467
00:29:59,181 --> 00:30:01,283
It's not like Lee Kwang Ho will tell us everything first,

468
00:30:01,316 --> 00:30:02,818
- Okay, - Get going,

469
00:30:03,485 --> 00:30:06,354
Are we going to stop investigating Mr, Cha?

