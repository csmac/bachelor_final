1
00:00:15,220 --> 00:00:17,406
Legend has it that it was written by the Dark Ones.

2
00:00:17,981 --> 00:00:19,822
Necronomicon ex Mortis,

3
00:00:20,858 --> 00:00:25,576
roughly translated, "Book of the Dead".

4
00:00:28,645 --> 00:00:33,133
The book served as a passageway to the evil worlds beyond..

5
00:00:38,235 --> 00:00:40,037
It was written long ago.

6
00:00:41,956 --> 00:00:43,989
When the seas ran red with blood.

7
00:00:45,792 --> 00:00:48,784
It was this blood that was used to ink the book.

8
00:00:54,538 --> 00:00:58,450
In the year 1300 AD, the book disappeared.

9
00:01:14,677 --> 00:01:16,479
"EVIL DEAD II"

10
00:01:31,824 --> 00:01:33,741
So what's this place like?

11
00:01:33,857 --> 00:01:37,501
Well it's a little run-down... but, uh, it's right up in the mountains.

12
00:01:37,693 --> 00:01:39,495
Are you sure it's deserted?

13
00:01:39,496 --> 00:01:44,406
Oh yeah... I think so.

14
00:02:23,725 --> 00:02:25,527
So what do you think kid?

15
00:02:28,520 --> 00:02:30,322
I love it Ash.

16
00:02:40,027 --> 00:02:41,829
I feel funny about being here.

17
00:02:41,830 --> 00:02:43,671
What if the people who own the place come home?

18
00:02:43,748 --> 00:02:45,589
They're not gonna come back.

19
00:02:45,666 --> 00:02:49,501
Even if they do we'll tell them the car
broke down or something like that.

20
00:02:49,617 --> 00:02:50,844
With your car, they'd believe it.

21
00:02:52,494 --> 00:02:55,370
Hey, what do you say we have some champagne hey baby?

22
00:02:56,330 --> 00:02:57,288
Sure.

23
00:02:58,248 --> 00:03:02,275
After all, I'm a man and you're a woman, at least last time I checked....

24
00:03:21,724 --> 00:03:24,792
Hey! There's a... There's a tape recorder here.

25
00:03:26,059 --> 00:03:27,017
See what's on it.

26
00:03:29,780 --> 00:03:30,738
This is Professor Raymond Knowby,

27
00:03:33,731 --> 00:03:35,533
Department of Ancient History, log entry number two.

28
00:03:36,493 --> 00:03:40,444
I believe I have made a significant find in the Castle of Candar.

29
00:03:41,288 --> 00:03:43,321
Having journeyed there with my wife Henrietta, my

30
00:03:48,116 --> 00:03:52,604
It was in the rear chamber of the castle
that we stumbled upon something remarkable.

31
00:03:53,870 --> 00:03:59,623
Morturom Demonto, the "Book of the Dead".

32
00:04:01,542 --> 00:04:06,221
My wife and I brought the book to this cabin where
I could study it undisturbed.

33
00:04:10,173 --> 00:04:11,975
It was here that I began the translations.

34
00:04:13,050 --> 00:04:14,852
The book speaks of a spiritual presence.,

35
00:04:15,812 --> 00:04:21,374
A thing of evil that roams
the forests and the dark bowers of man's domain..

36
00:04:22,640 --> 00:04:24,442
It is through the recitation of the book's passages...

37
00:04:25,402 --> 00:04:31,271
that this dark spirit is given
license to possess the living.

38
00:04:33,189 --> 00:04:37,485
Included here are the phonetic pronunciations of those passages.

39
00:04:39,902 --> 00:04:43,737
"Cunda astratta montose

40
00:04:45,655 --> 00:04:47,572
eargrets gutt

41
00:04:49,491 --> 00:04:57,162
nos veratoos canda amantos canda".

42
00:09:29,594 --> 00:09:31,435
It's gone. The sun's driven it away.

43
00:09:42,061 --> 00:09:43,902
Yeah. For now.
Gotta blow out of here for now...

44
00:09:57,405 --> 00:09:59,246
Join us.

45
00:10:53,026 --> 00:10:55,902
Gotta, I gotta get a grip on myself here..

46
00:13:53,317 --> 00:13:54,391
How'd the expedition go?

47
00:13:54,391 --> 00:13:57,076
Great. I found the pages of the Book of the Dead.

48
00:13:57,153 --> 00:14:00,720
Yeah, I got your telegram. Thanks. So what condition are they in?

49
00:14:00,989 --> 00:14:02,830
Take a look.

50
00:14:03,866 --> 00:14:04,824
They haven't aged a day in 3000 years.

51
00:14:04,825 --> 00:14:06,666
Maybe longer.

52
00:14:07,702 --> 00:14:08,660
When do we begin the translations?

53
00:14:08,661 --> 00:14:11,346
Tonight. Is everything all set with my father?

54
00:14:11,538 --> 00:14:14,031
Well, it should be but I haven't spoken with him in a week.

55
00:14:14,415 --> 00:14:16,256
There's no phone in the cabin.

56
00:14:17,407 --> 00:14:19,209
We'll take my car, it'll take us about
an hour to get there.

57
00:14:21,627 --> 00:14:25,462
Annie you hinted in your telegram that your father was onto
something in the first part of his translations.

58
00:14:25,923 --> 00:14:26,881
What has he found in the Book of the Dead?

59
00:14:26,882 --> 00:14:32,635
Probably nothing. But just possibly, a doorway to another world?

60
00:17:50,303 --> 00:17:52,105
Dance with me.

61
00:18:40,171 --> 00:18:41,973
Hello lover.

62
00:19:12,777 --> 00:19:14,579
Workshed.

63
00:19:45,383 --> 00:19:49,679
Even now we have your darling Linda's soul.

64
00:19:50,178 --> 00:19:52,863
She suffers in torment.

65
00:19:53,055 --> 00:19:57,734
You're going down. Chainsaw.

66
00:20:46,758 --> 00:20:50,018
Please Ash... please don't hurt me.

67
00:20:50,594 --> 00:20:54,429
You swore- you swore that we'd always be together.

68
00:20:56,348 --> 00:20:58,150
I love you.

69
00:21:01,143 --> 00:21:04,096
Yah! Your lover is mine
and now she burns

70
00:21:04,787 --> 00:21:06,781
in Hell.

71
00:23:33,623 --> 00:23:35,425
Oh yeah, alright... OK.

72
00:23:44,249 --> 00:23:46,090
I'm fine... I'm fine.

73
00:23:46,167 --> 00:23:49,619
I don't think so. We just cut up our girlfriend with a chainsaw.

74
00:23:49,926 --> 00:23:52,611
Does that sound fine?

75
00:24:50,419 --> 00:24:56,863
You bastards! You dirty bastards!

76
00:25:04,804 --> 00:25:07,680
Give me back my hand!

77
00:25:10,481 --> 00:25:14,508
Give me back my haaaaand!!

78
00:25:50,836 --> 00:25:52,677
Excuse me. Excuse me.

79
00:25:53,713 --> 00:25:55,554
Is this the road to the Knowby cabin?

80
00:25:56,513 --> 00:25:58,430
That's right. And you ain't going there.

81
00:25:59,467 --> 00:26:01,308
And why not?

82
00:26:11,933 --> 00:26:13,850
There must be another way in.

83
00:26:14,542 --> 00:26:15,500
There's gotta be another road or something.

84
00:26:15,769 --> 00:26:17,610
Sure ain't no road.

85
00:26:22,482 --> 00:26:26,241
Why the hell do you want to go up there for anyway?

86
00:26:31,113 --> 00:26:32,954
None of your business.

87
00:26:38,785 --> 00:26:41,393
Hey! I just remembered.

88
00:26:41,662 --> 00:26:44,462
Why, yeah... that's right. There is a trail.

89
00:26:44,539 --> 00:26:47,339
You could uh, follow Bobby Joe and me.

90
00:26:48,375 --> 00:26:50,216
Sounds alright to me.

91
00:26:50,293 --> 00:26:51,251
But it'll cost ya.

92
00:26:51,252 --> 00:26:53,093
How much?

93
00:26:53,170 --> 00:26:57,888
Forty fi...
Hundred bucks.

94
00:26:58,847 --> 00:27:03,641
Tell ya what, you take my bags and you got a deal.

95
00:27:06,596 --> 00:27:08,437
Sure.

96
00:28:55,921 --> 00:28:58,797
Ah. Ah. That's right. Who's laughing now?

97
00:29:07,429 --> 00:29:10,305
Who's laughing now?

98
00:29:57,297 --> 00:29:59,214
Jesus H. Christ... thought all she was talking about

99
00:29:59,713 --> 00:30:01,630
were those two Goddamn little bags.

100
00:30:11,682 --> 00:30:14,558
Here's your new home.

101
00:31:50,458 --> 00:31:51,416
Son of a....

102
00:32:13,589 --> 00:32:15,967
Gotcha didn't I you little sucker?!

103
00:32:48,113 --> 00:32:49,915
Old double barrel here...

104
00:32:50,031 --> 00:32:55,401
blow your butts to kingdom come. See if we don't...

105
00:35:46,485 --> 00:35:48,402
You little bastard!

106
00:35:51,280 --> 00:35:53,197
You gonna be alright honey?

107
00:35:53,198 --> 00:35:55,768
I-I don't know. I-I think so.

108
00:36:01,829 --> 00:36:01,829
You just sit still for a minute.
You know this son of a bitch?

109
00:36:01,829 --> 00:36:03,631
No we thought her father was going to be here.

110
00:36:03,747 --> 00:36:04,705
That's why we decided -

111
00:36:04,706 --> 00:36:06,508
Oh my God! Where are my parents?

112
00:36:19,091 --> 00:36:21,008
What the hell did you do to them?

113
00:36:22,927 --> 00:36:24,729
What the hell did you do to them?

114
00:36:26,763 --> 00:36:28,565
Annie, come here.

115
00:36:33,476 --> 00:36:35,278
We'll throw him in there.

116
00:36:39,230 --> 00:36:41,032
Crazy buck's gone blood seeking.

117
00:36:42,107 --> 00:36:45,827
Wait.
I made a mistake.

118
00:36:51,697 --> 00:36:53,614
Wait. Wait. Wait.

119
00:36:55,610 --> 00:36:56,492
I made a mistake.

120
00:36:56,492 --> 00:36:58,294
Damn right. Blackmail son of a bitch.

121
00:37:07,117 --> 00:37:08,958
I hope you rot down there.

122
00:37:18,625 --> 00:37:19,507
Oh shit!

123
00:37:19,508 --> 00:37:21,310
I know it hurts baby, but everything's gonna be fine.

124
00:37:26,221 --> 00:37:28,138
Now in about five minutes I'm gonna go fetch the sheriff and bring him-

125
00:37:29,098 --> 00:37:30,900
Checked all the other rooms.

126
00:37:31,016 --> 00:37:32,933
Folks aren't here. Maybe they never came.

127
00:37:32,934 --> 00:37:34,851
But these are my father's things.

128
00:37:37,728 --> 00:37:41,563
It's only been a few hours since I've translated and spoken aloud the first of the demon resurrection

129
00:37:41,641 --> 00:37:44,441
passages from the Book of the Dead.

130
00:37:45,400 --> 00:37:47,202
Shhh... Listen up. This is my father's voice.

131
00:37:50,195 --> 00:37:54,874
And now I fear that my wife has become host to a Candarian Demon.

132
00:37:56,908 --> 00:38:00,628
May God forgive me for what I have unleashed unto this earth.

133
00:38:01,703 --> 00:38:03,505
Last night Henrietta tried to...

134
00:38:05,156 --> 00:38:06,306
kill me.

135
00:38:08,416 --> 00:38:10,218
It's now October 1st, 4:33 PM.

136
00:38:13,211 --> 00:38:15,013
Henrietta is dead. I could not bring

137
00:38:17,047 --> 00:38:18,964
myself to dismember her corpse. But I dragged her down the steps...

138
00:38:25,678 --> 00:38:28,439
and I buried her. I buried her in the cellar.

139
00:38:29,514 --> 00:38:31,316
God help me,

140
00:38:31,432 --> 00:38:34,385
I buried her in the earthen floor of the fruit cellar.

141
00:38:42,058 --> 00:38:42,940
What the hell was that?

142
00:38:42,940 --> 00:38:44,742
Somebody's down there with him.

143
00:38:45,817 --> 00:38:46,852
No, can't be.

144
00:38:46,853 --> 00:38:47,735
Let's get the fuck out of here.

145
00:38:47,735 --> 00:38:56,250
Someone's in my fruit cellar.
Someone with a fresh soul!

146
00:38:58,361 --> 00:39:00,202
Let me out! There's something down here! Ah!

147
00:39:02,197 --> 00:39:02,197
Let him out!

148
00:39:02,197 --> 00:39:04,038
It's a trick, I know it!

149
00:39:04,038 --> 00:39:05,840
Let him out!!

150
00:39:05,956 --> 00:39:06,991
Move it! Open those chains up!

151
00:39:06,992 --> 00:39:08,833
Come to me.

152
00:39:10,751 --> 00:39:12,553
Ah! Help! Help! Help me!

153
00:39:12,669 --> 00:39:14,586
Come to sweet Henrietta. Hahaha.

154
00:39:21,376 --> 00:39:23,217
Hurry!

155
00:39:24,177 --> 00:39:26,094
Help! Help me please!

156
00:39:28,089 --> 00:39:29,930
I'll swallow your soul.

157
00:39:46,310 --> 00:39:48,151
Do something!

158
00:40:53,440 --> 00:40:55,281
There's something out there.

159
00:40:57,276 --> 00:41:01,994
That... that witch in the cellar is only part of it.

160
00:41:04,948 --> 00:41:12,159
It lives... out in those woods, in the dark...

161
00:41:13,579 --> 00:41:20,982
something... something that's come back from the dead.

162
00:41:21,251 --> 00:41:23,475
Plee! Please let's just get the hell out of here!

163
00:41:24,128 --> 00:41:25,969
We're going baby. We're going to get on that trail-

164
00:41:26,928 --> 00:41:28,845
Nobody's going out that door, not till daylight.

165
00:41:30,840 --> 00:41:32,681
Now you listen to me -

166
00:41:57,692 --> 00:41:59,533
Remember that song Annie? I used to sing it to you when you were a baby.

167
00:42:00,569 --> 00:42:02,410
Mother?

168
00:42:03,446 --> 00:42:05,287
Unlock these chains. Quickly!

169
00:42:13,995 --> 00:42:14,953
No.

170
00:42:14,954 --> 00:42:19,288
You were born September 2nd, 1962.

171
00:42:19,749 --> 00:42:26,385
I remember it well because it was snowing. so strange

172
00:42:26,462 --> 00:42:27,420
it would be snowing in September.

173
00:42:31,257 --> 00:42:33,174
That thing in the cellar is not my mother.

174
00:42:42,305 --> 00:42:46,140
We are the things that were and shall be again.

175
00:42:48,519 --> 00:42:49,401
Steps of the Book.

176
00:42:50,437 --> 00:42:52,278
We want what is yours.

177
00:42:53,314 --> 00:42:55,155
Life! Dead by dawn. Dead by dawn.

178
00:42:55,232 --> 00:42:56,190
Dead by dawn. Dead by dawn.

179
00:42:56,191 --> 00:42:58,032
Dead by dawn.

180
00:42:59,068 --> 00:43:00,909
Dead by dawn.

181
00:43:16,253 --> 00:43:17,211
Let me out.

182
00:43:17,289 --> 00:43:19,206
Thirsty son-of-a-bitch.

183
00:43:30,254 --> 00:43:33,591
Where you going? Help us you filthy coward!

184
00:43:54,689 --> 00:43:56,606
We live still!

185
00:44:36,041 --> 00:44:37,843
That's funny.

186
00:44:37,844 --> 00:44:39,685
What?

187
00:44:40,836 --> 00:44:42,638
That trail we came in here on?

188
00:44:44,557 --> 00:44:46,781
It just ain't there no more.

189
00:44:48,393 --> 00:44:54,070
Like, like the woods just swallowed her up.

190
00:45:16,204 --> 00:45:17,546
It's so quiet.

191
00:46:04,153 --> 00:46:08,871
Maybe something trying to force its way into our world.

192
00:46:44,431 --> 00:46:46,272
Hell no. You're the curious one.

193
00:47:06,603 --> 00:47:11,973
Hey. I'll go with you.

194
00:48:04,987 --> 00:48:08,822
Shit. I told you there weren't nothing in here no how.

195
00:48:33,871 --> 00:48:35,673
Holy Mother o' Mercy.

196
00:48:36,748 --> 00:48:37,706
Father?

197
00:48:37,707 --> 00:48:39,509
Annie.

198
00:48:39,510 --> 00:48:45,762
There is a dark spirit here that wants to destroy you.

199
00:48:46,338 --> 00:48:50,173
Your salvation lies there.

200
00:48:56,887 --> 00:49:02,525
In the pages of the book.
Recite the passages.

201
00:49:03,600 --> 00:49:13,650
Dispel the evil. Save my soul. And your own lives!

202
00:49:23,739 --> 00:49:26,615
Jake. You're holding my hand too tight.

203
00:49:28,611 --> 00:49:30,452
Baby, I ain't holding your hand.

204
00:49:50,591 --> 00:49:52,393
Hey? Where's Bobby Joe?

205
00:51:40,875 --> 00:51:43,445
Hey? Where the hell is she?

206
00:51:56,219 --> 00:51:58,136
We gotta go out there and find her.

207
00:51:58,214 --> 00:52:01,666
If she went out in those woods, you can forget about her.

208
00:52:07,727 --> 00:52:09,644
What's wrong?

209
00:52:10,604 --> 00:52:13,365
Felt like someone just walked over my grave.

210
00:52:18,276 --> 00:52:20,078
What's that picture?
What is that?

211
00:52:22,112 --> 00:52:26,791
In 1300 AD they called this man the, ah, hero from the sky.

212
00:52:28,825 --> 00:52:30,742
He was prophesied to have destroyed the Evil.

213
00:52:32,737 --> 00:52:36,496
Didn't do a very good job...  Can you find it?

214
00:52:38,415 --> 00:52:40,217
Here it is, two passages.

215
00:52:46,087 --> 00:52:47,889
Recitation of this first passage..

216
00:52:50,958 --> 00:52:54,717
will make this dark spirit manifest itself in the flesh.

217
00:52:57,594 --> 00:52:59,396
Why the hell would we want to do that?

218
00:52:59,512 --> 00:53:01,314
Recitation of this second passage creates

219
00:53:04,307 --> 00:53:06,109
a kind of rift in time and space.

220
00:53:09,102 --> 00:53:10,904
And the physical manifestation of this

221
00:53:12,056 --> 00:53:12,938
dark spirit can be forced back into the rift.

222
00:53:12,938 --> 00:53:14,740
At least that's the best translation that I can -

223
00:53:17,733 --> 00:53:21,453
That's right. I'm running the show now.

224
00:53:22,528 --> 00:53:24,829
We're going to go out there in them woods and look for Bobby Joe.

225
00:53:25,482 --> 00:53:27,323
Once we find her we're getting the hell out of here.

226
00:53:28,359 --> 00:53:31,427
No you idiot! You'll kill us all.

227
00:53:33,154 --> 00:53:34,995
She's dead by now.

228
00:53:35,072 --> 00:53:36,913
Don't you understand?

229
00:53:37,872 --> 00:53:41,132
With these pages, at least we have a chance.

230
00:53:44,662 --> 00:53:46,503
Bunch of mumbo jumbo bullshit.

231
00:53:49,457 --> 00:53:51,298
These pages don't mean squat.

232
00:53:58,088 --> 00:54:00,888
Besides, now you ain't got no choice.

233
00:54:01,847 --> 00:54:03,649
Now move!

234
00:54:12,473 --> 00:54:14,314
Move.

235
00:54:14,391 --> 00:54:16,232
Look. You're nuts.

236
00:54:18,150 --> 00:54:19,952
I said move!

237
00:54:25,899 --> 00:54:27,740
I'll blow your fucken head off.

238
00:55:12,889 --> 00:55:16,648
Hey. No trail. Where to now?

239
00:55:40,623 --> 00:55:41,658
You'll get us all killed!

240
00:55:41,659 --> 00:55:43,500
Shut up!

241
00:55:43,577 --> 00:55:44,535
Leave him alone!

242
00:55:44,536 --> 00:55:46,377
Get outta here...

243
00:56:00,839 --> 00:56:02,680
Where are you, girl?

244
00:56:32,486 --> 00:56:34,327
You're next. Annie!

245
00:59:16,473 --> 00:59:18,314
I'm sorry!

246
00:59:19,350 --> 00:59:21,191
Get me another room.
Get the axe.

247
00:59:21,268 --> 00:59:23,109
We'll kill it when it comes back.

248
00:59:26,063 --> 00:59:27,904
But first, pull this damn thing out of me!

249
00:59:39,489 --> 00:59:41,330
I can't breathe, I can't breathe. Hurry!

250
00:59:43,440 --> 00:59:45,242
I'm trying! I'm trying!

251
00:59:55,792 --> 00:59:59,551
Shut up! Shut up! Shut up!

252
01:00:24,677 --> 01:00:26,479
Check outside the windows.

253
01:00:29,472 --> 01:00:31,274
Check the windows, he's probably right out...

254
01:00:32,234 --> 01:00:34,151
Ahhh! Help me!

255
01:01:02,922 --> 01:01:04,763
Oh God! Ahhhhh!

256
01:02:34,985 --> 01:02:37,861
No! No wait!

257
01:02:39,895 --> 01:02:43,615
Listen to me! I'm alright now.

258
01:02:43,731 --> 01:02:45,533
That thing is gone!

259
01:02:58,001 --> 01:03:00,225
Damn it! I said I was alright!

260
01:03:00,993 --> 01:03:02,795
Are you listening to me?

261
01:03:04,829 --> 01:03:08,549
You hear what I'm saying?
I'm alright!

262
01:03:12,501 --> 01:03:14,303
I'm alright.

263
01:03:16,337 --> 01:03:20,633
OK, maybe you are. But for how long?

264
01:03:24,968 --> 01:03:27,729
If we're going to beat this thing, we need those pages.

265
01:03:31,681 --> 01:03:35,708
Then let's head down into that cellar and carve ourselves a witch.

266
01:04:22,507 --> 01:04:24,309
Groovy.

267
01:05:06,621 --> 01:05:08,423
Those pages are down there somewhere.

268
01:08:04,994 --> 01:08:08,829
"Nos veratos alamemnon conda."

269
01:09:02,533 --> 01:09:03,491
Let's go.

270
01:09:35,216 --> 01:09:37,057
I'll swallow your soul. I'll swallow your soul.

271
01:10:27,960 --> 01:10:29,801
Hey! I'll swallow your soul! I'll swallow your soul!

272
01:10:32,755 --> 01:10:34,596
Swallow this.

273
01:12:01,941 --> 01:12:03,858
I only completed the first of the passages

274
01:12:04,818 --> 01:12:06,659
and that was to make the Evil a thing of the flesh!

275
01:12:14,408 --> 01:12:16,249
Finish it!

276
01:12:17,285 --> 01:12:19,126
There's still the second passage.

277
01:12:23,998 --> 01:12:23,998
The one-

278
01:12:23,998 --> 01:12:26,031
the one to open the rift and send the Evil back!

279
01:12:26,875 --> 01:12:29,675
Well start reciting it! Now!

280
01:12:42,219 --> 01:12:44,060
Don't look Annie! Finish the passages!
Get rid of it!

281
01:13:05,235 --> 01:13:07,076
No!!!

282
01:14:00,857 --> 01:14:02,698
We've won. We've won. Victory is ours.

283
01:14:34,421 --> 01:14:36,338
You did it kid.

284
01:15:09,060 --> 01:15:10,977
By God... No!!!! For God's sake!

285
01:15:10,978 --> 01:15:12,780
How do you stop it?

286
01:16:27,698 --> 01:16:29,500
Slay the beast. It is a deadite!

287
01:16:30,460 --> 01:16:32,377
Run! Back to the castle!

288
01:17:07,016 --> 01:17:08,818
Hail he who has come from the skies

289
01:17:09,778 --> 01:17:11,695
to deliver us from the terrors of the deadites!

290
01:17:13,729 --> 01:17:15,531
Hail! Hail! Hail!

291
01:17:18,524 --> 01:17:20,326
No! No!! No!!! No!!!!!!

