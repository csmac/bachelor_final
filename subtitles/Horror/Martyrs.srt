1
00:00:16,927 --> 00:00:19,236
...

2
00:01:11,007 --> 00:01:14,124
Is it on?
Whenever you want.

3
00:01:14,767 --> 00:01:17,486
We're right in the middle of
the industrial site of Chamfors.

4
00:01:17,807 --> 00:01:21,516
This is where they found the child.

5
00:01:22,767 --> 00:01:24,359
Building 1.

6
00:01:25,367 --> 00:01:28,484
We've got permission
to make this movie.

7
00:01:28,647 --> 00:01:31,445
As a medical archive in favor

8
00:01:31,607 --> 00:01:34,041
of the pediatric ward of
the hospital.

9
00:01:37,927 --> 00:01:39,201
There you go.

10
00:01:40,807 --> 00:01:42,160
She lived there.

11
00:01:44,967 --> 00:01:47,435
She wasn't raped, that's for sure.

12
00:01:47,607 --> 00:01:50,838
It looks more like common abuse.

13
00:01:51,007 --> 00:01:55,319
Malnutrition, dehydration,
mild hypothermia.

14
00:01:56,087 --> 00:01:59,716
The hole allows the person to
do one's needs without standing up.

15
00:01:59,887 --> 00:02:02,879
The infection on the wrists proves
she was chained.

16
00:02:03,887 --> 00:02:06,037
How did she escaped?

17
00:02:06,207 --> 00:02:08,118
Lucie doesn't tell a thing
about what she went through.

18
00:02:58,727 --> 00:03:01,116
What's that? Orange juice?
Yes.

19
00:03:01,287 --> 00:03:03,198
Is it for her?
Yes.

20
00:03:03,367 --> 00:03:06,518
You're her mom, now?
Well yes, she got no one else.

21
00:03:53,327 --> 00:03:55,966
Don't worry, Anna, these gentlemen

22
00:03:56,247 --> 00:03:59,319
are really nice people.
Okay.

23
00:04:00,607 --> 00:04:02,677
Do you know why we show you this movie?

24
00:04:02,847 --> 00:04:03,962
Yes.

25
00:04:04,607 --> 00:04:08,077
Why do you think we need you?
Because...

26
00:04:08,247 --> 00:04:11,319
you want to get the people

27
00:04:11,487 --> 00:04:14,365
that did those things to Lucie
Yes, Anna.

28
00:04:15,447 --> 00:04:18,325
Lucie wants that too.
You must catch the bad people.

29
00:04:18,887 --> 00:04:20,923
What else did she say?
You talked to her for hours...

30
00:04:21,087 --> 00:04:22,315
She's afraid.

31
00:04:22,567 --> 00:04:25,320
Does she tell you any thing else?
Not much more.

32
00:04:25,607 --> 00:04:27,438
What does she remember?

33
00:04:27,607 --> 00:04:29,757
She doesn't know who it is.

34
00:04:29,927 --> 00:04:33,124
Sometimes she thinks she remembers something,
sometimes she doesn't.

35
00:04:34,047 --> 00:04:38,438
What do you think?
Do you think she will remember?

36
00:04:38,607 --> 00:04:42,998
It was dark and she couldn't see well.
She doesn't remember.

37
00:04:45,967 --> 00:04:47,082
Come in.

38
00:04:50,687 --> 00:04:52,723
Lucie Jurin didn't come down for lunch.

39
00:05:04,767 --> 00:05:05,961
Lucie?

40
00:05:26,927 --> 00:05:27,723
Anna!

41
00:05:29,967 --> 00:05:31,605
Don't tell anyone, please?

42
00:05:33,087 --> 00:05:35,920
I won't.
It wasn't me

43
00:05:36,607 --> 00:05:37,642
it wasn't me

44
00:05:37,887 --> 00:05:39,366
it wasn't me.

45
00:05:41,247 --> 00:05:42,475
Calm down.

46
00:05:57,407 --> 00:05:58,999
Do you want to sleep with me?

47
00:06:37,607 --> 00:06:38,562
Anna?

48
00:07:59,167 --> 00:08:02,637
15 YEARS LATER

49
00:08:28,007 --> 00:08:29,235
Give it back!

50
00:08:29,687 --> 00:08:30,802
Give it back!

51
00:08:33,287 --> 00:08:35,084
Stop it, both of you.

52
00:08:36,487 --> 00:08:37,602
Damn!

53
00:08:38,927 --> 00:08:41,282
Your girlfriend can barely write
a sentence.

54
00:08:43,207 --> 00:08:45,562
Stop, I said.
You're the worst!

55
00:08:46,607 --> 00:08:49,075
Don't touch me.
By the way, it's none of your business.

56
00:08:49,367 --> 00:08:51,642
What do you know?
You don't know what love is.

57
00:08:51,807 --> 00:08:53,525
You don't even have a boyfriend.

58
00:08:53,687 --> 00:08:56,406
And? Are we gonna eat breakfast or not?
Yeah, yeah.

59
00:08:56,687 --> 00:08:58,882
You're getting on my nerves.

60
00:09:01,047 --> 00:09:02,446
I hate you.

61
00:09:02,607 --> 00:09:04,040
CHAMPION 100 METRES BUTTERFLY

62
00:09:20,447 --> 00:09:22,199
So that's what you decided, huh?

63
00:09:22,487 --> 00:09:24,762
Well, it's my life isn't it?
Smartass...

64
00:09:24,927 --> 00:09:28,158
Did you talk about it with your mother?

65
00:09:28,567 --> 00:09:30,558
Yep.
And what did she say?

66
00:09:30,847 --> 00:09:32,565
She's more pissed about her low water pressure.

67
00:09:33,407 --> 00:09:36,558
She's not happy with either situation.
Are you happy?

68
00:09:37,967 --> 00:09:40,640
What? It's all good here!
"What? It's all good here!"

69
00:09:40,927 --> 00:09:42,645
All done.
Good job, honey.

70
00:09:44,047 --> 00:09:45,196
Here.

71
00:09:45,767 --> 00:09:47,598
A gift.

72
00:09:48,727 --> 00:09:51,002
Awwwww, Gabrielle.
Gross!

73
00:09:51,487 --> 00:09:53,000
This little guy got stuck
in the main pipe.

74
00:09:53,287 --> 00:09:55,403
This guy was causing our low water pressure.

75
00:09:56,007 --> 00:09:57,838
Toss it in the trash.

76
00:10:01,047 --> 00:10:03,356
Yeah, I'm being gross
but now that it's fixed we can replant the Azealeus.

77
00:10:04,687 --> 00:10:06,678
Did Antoine talk
about his big plans?

78
00:10:07,247 --> 00:10:09,044
Actually I was just asking him about what
you thought about it.

79
00:10:09,327 --> 00:10:13,400
No, you wondered if she was pissed off.
Be quiet, it's none of your business.

80
00:10:13,687 --> 00:10:16,884
I'm trying to help my brother.
You can't help your brother whose a visionary that's for sure.

81
00:10:18,727 --> 00:10:20,001
Antoine?!

82
00:10:20,167 --> 00:10:23,079
It's enough of an expensive school
to drop out after only being there for 3 months!

83
00:10:23,367 --> 00:10:25,198
Definitely not if you pay in advance.
TA-DA!

84
00:10:25,607 --> 00:10:28,440
Marie, you really need to butt out!
She think she's a swimming celebrity!

85
00:10:29,007 --> 00:10:31,475
Are you planning to make this a habit of yours?
The school?

86
00:10:31,767 --> 00:10:34,725
Keep in mind that you wouldn't have graduated
if we hadn't paid extra!

87
00:10:35,007 --> 00:10:37,077
What do you mean?
I made it!

88
00:10:37,367 --> 00:10:41,246
Oh-ho? "Made it" with a C average?
C'mon remind me that it's about 8 grand to push you through wasn't it?

89
00:10:41,407 --> 00:10:44,604
After each trimesters! But it wasn't skin off your nose was it?

90
00:10:44,767 --> 00:10:47,918
Aren't you tired of being such a pain in the ass? You little shit!

91
00:10:48,367 --> 00:10:50,562
Well listen! This school is a lot more work

92
00:10:51,527 --> 00:10:53,438
I-I.. wanted to go study something I really like.

93
00:10:53,607 --> 00:10:56,326
Just get your law degree

94
00:10:57,047 --> 00:10:59,959
Law?... Studying law is not my thing...
Laws open many doors!

95
00:11:00,127 --> 00:11:03,119
The problem is he wants to go and live with his girlfriend
Minerva!

96
00:11:03,407 --> 00:11:06,080
You know laws is not my thing!
Whatever, Antoine!

97
00:11:06,247 --> 00:11:09,956
As if anything is "your thing!"
For a week there you wanted to be a Pastry Chef, remember?

98
00:11:12,607 --> 00:11:13,881
Someone's just got served!

99
00:11:16,447 --> 00:11:17,721
Is that Minerva?

100
00:11:18,607 --> 00:11:21,440
Lighten up on your brother.
He always picks on me!

101
00:11:21,607 --> 00:11:24,679
So what? Cool it, it's the weekend
Take a break.

102
00:11:24,847 --> 00:11:26,485
You work in the factory or something?

103
00:11:32,007 --> 00:11:33,804
Dad?
Stay here, don't move!

104
00:11:51,007 --> 00:11:52,201
Don't move!

105
00:11:54,687 --> 00:11:56,040
Sit down!

106
00:12:04,767 --> 00:12:06,280
So, you how old?

107
00:12:06,927 --> 00:12:08,280
...18.

108
00:12:11,287 --> 00:12:13,164
Do you know what your parents did to me?

109
00:13:45,687 --> 00:13:47,598
Why did you do that to me?

110
00:13:51,607 --> 00:13:55,077
Why?!

111
00:14:01,847 --> 00:14:03,439
Why?!

112
00:14:43,967 --> 00:14:45,719
It's them.
Lucie?

113
00:14:45,887 --> 00:14:47,923
It's them, Anna.
You sure?

114
00:14:48,207 --> 00:14:50,277
They've changed, but it's definitely them.

115
00:14:50,447 --> 00:14:52,642
Are you positive?
It's been 15 years!

116
00:14:52,927 --> 00:14:54,679
You can tell it's them from the papers?
I CAN TELL!

117
00:14:55,207 --> 00:14:56,925
Then we'll call the cops.
There's no need to.

118
00:14:57,207 --> 00:14:58,686
What?
I did what I thought was right.

119
00:14:58,967 --> 00:15:00,844
You did WHAT?
What I had to do!

120
00:15:01,007 --> 00:15:03,441
We said you'd just observe them.
I'm sure they were the ones!

121
00:15:03,607 --> 00:15:07,316
I knew I should have come along, what have you done?
Don't you believe me?

122
00:15:10,407 --> 00:15:11,635
Where are you calling from?

123
00:15:12,407 --> 00:15:14,398
From the house.
From the house?

124
00:15:14,687 --> 00:15:17,042
Yes.
What's the address there?

125
00:15:20,087 --> 00:15:21,281
Lucie, give me the address!

126
00:17:25,847 --> 00:17:26,996
I did it...

127
00:17:42,567 --> 00:17:43,716
Look!

128
00:17:45,127 --> 00:17:46,560
I did it...

129
00:20:59,487 --> 00:21:02,285
It's me, Lucie, It's me!
Calm down. It's me!

130
00:21:02,607 --> 00:21:03,926
She's in there.

131
00:21:10,607 --> 00:21:14,122
She's in the house.
I know.

132
00:21:14,487 --> 00:21:16,603
She hurt me more than usual.

133
00:21:33,567 --> 00:21:34,795
Stay right here.

134
00:21:35,007 --> 00:21:36,235
Where are you going?

135
00:21:36,487 --> 00:21:38,557
I have to go and take a look.
She's still in there.

136
00:21:38,727 --> 00:21:40,524
I have to go!
She's in the house.

137
00:21:41,527 --> 00:21:44,439
Look at me, I have to go and take a look.
Wait for me.

138
00:22:56,367 --> 00:22:59,279
She hurt me worst than usual
I'm coming, Lucie.

139
00:23:02,367 --> 00:23:04,164
What am I going to do?

140
00:23:05,487 --> 00:23:06,966
I'm almost done.

141
00:23:47,887 --> 00:23:50,117
Got anymore shotgun shells?
They're gone, you won't be needing them.

142
00:23:50,487 --> 00:23:52,478
Maybe we should get moving?
Yeah!

143
00:23:52,767 --> 00:23:54,166
She'll be back soon.

144
00:23:54,847 --> 00:23:55,802
C'mon.

145
00:23:57,087 --> 00:24:00,079
They'll know that we did this
Cause you phoned me from here.

146
00:24:00,967 --> 00:24:03,197
I don't care.
I know.

147
00:24:04,287 --> 00:24:07,359
Really, I don't!
You'll care if they lock you up.

148
00:24:13,967 --> 00:24:16,162
What are we going to do?
Stay still.

149
00:24:18,447 --> 00:24:20,199
You're hurting me!
Sorry.

150
00:24:23,927 --> 00:24:25,758
What are we gonna do, Anna?

151
00:25:07,487 --> 00:25:08,476
Are you okay?

152
00:25:15,007 --> 00:25:17,396
Is this making you sick?

153
00:25:20,247 --> 00:25:21,965
Do you smell this?

154
00:25:22,447 --> 00:25:23,880
The foul stench?

155
00:25:24,047 --> 00:25:27,596
I smelled it every day
Every time she would bend over me I'd smell that. Everyday.

156
00:25:27,927 --> 00:25:29,201
Do you understand?

157
00:25:30,327 --> 00:25:32,636
It smelled different when she hit me.

158
00:25:41,527 --> 00:25:42,403
Look.

159
00:25:45,127 --> 00:25:46,196
It's them.

160
00:26:18,567 --> 00:26:20,000
What are you doing?

161
00:26:27,727 --> 00:26:29,683
Will we ever be free from this?

162
00:26:32,927 --> 00:26:35,282
Yeah... Sure.

163
00:28:49,767 --> 00:28:52,156
Please, if you wanna live be quiet.

164
00:28:59,207 --> 00:29:00,526
Anna, what are you doing?

165
00:29:03,247 --> 00:29:04,475
Are you okay?
Yes.

166
00:29:05,087 --> 00:29:06,486
You didn't answer?
I didn't hear you.

167
00:29:06,647 --> 00:29:08,717
I thought she was here.
No.

168
00:29:09,007 --> 00:29:11,077
You finished? Then let's go.

169
00:29:11,247 --> 00:29:13,283
Not yet, let's clean up.

170
00:29:13,567 --> 00:29:15,000
When we're done we go!
Yes, get dressed.

171
00:29:50,807 --> 00:29:51,842
Lucie!

172
00:30:19,687 --> 00:30:21,723
Open the door!
I can't it's locked!

173
00:30:23,407 --> 00:30:24,237
Open up!

174
00:31:03,687 --> 00:31:04,642
Quickly!

175
00:31:15,007 --> 00:31:16,645
What do you want?

176
00:31:17,567 --> 00:31:19,125
Leave me alone.

177
00:31:19,407 --> 00:31:20,601
l killed them all!

178
00:31:22,487 --> 00:31:24,318
Even the children!

179
00:31:24,487 --> 00:31:26,762
I even killed the children!

180
00:31:41,887 --> 00:31:44,117
Calm down! Calm down!

181
00:33:34,567 --> 00:33:36,000
Get away from there you little whore!

182
00:33:37,367 --> 00:33:38,880
I'm gonna kill you!

183
00:34:04,607 --> 00:34:05,596
Sorry.

184
00:34:07,407 --> 00:34:08,476
It's not my fault.

185
00:35:04,087 --> 00:35:05,361
Where are you?

186
00:35:15,927 --> 00:35:18,043
Anna, I know you're there!
ANNA!

187
00:35:21,487 --> 00:35:22,806
I'm coming, Lucie.

188
00:35:38,767 --> 00:35:39,882
What's your name?

189
00:35:40,447 --> 00:35:41,596
Gabrielle...

190
00:35:43,367 --> 00:35:44,959
I'm coming, Lucie!

191
00:35:45,447 --> 00:35:47,039
You have to help me, Gabrielle.

192
00:35:47,327 --> 00:35:49,602
Why is she doing this?

193
00:36:06,207 --> 00:36:08,004
I think she heard us.

194
00:36:19,287 --> 00:36:20,925
We have to move fast.

195
00:36:33,647 --> 00:36:35,717
Please, call an ambulance.

196
00:36:35,887 --> 00:36:37,559
For my children...

197
00:36:38,207 --> 00:36:39,083
Come on!

198
00:36:40,087 --> 00:36:42,885
I'll get you as far as the wood
Then you're on your own.

199
00:36:44,367 --> 00:36:46,835
No! Stop!

200
00:36:50,247 --> 00:36:51,441
Stop! Lucie!

201
00:37:32,207 --> 00:37:33,799
Why did you helped her?

202
00:37:34,887 --> 00:37:38,163
She didn't do anything, Lucie.
She was the one who hurt me!

203
00:37:38,687 --> 00:37:39,802
A LOOOT!

204
00:37:45,127 --> 00:37:46,879
And you want her to live, huh?!

205
00:38:12,127 --> 00:38:13,640
So, you have never believed me?

206
00:38:14,647 --> 00:38:15,716
Enough!

207
00:38:17,127 --> 00:38:20,358
So you think I'm insane too?
Just like the doctors.

208
00:38:24,287 --> 00:38:25,925
Did they ever do anything for me?

209
00:38:26,087 --> 00:38:28,555
Beside say I was crazy?
You're just like them.

210
00:38:35,647 --> 00:38:37,205
It's over, Anna!

211
00:38:52,487 --> 00:38:56,082
They're dead.
They won't hurt you anymore.

212
00:41:31,607 --> 00:41:33,438
Stooop!

213
00:41:34,927 --> 00:41:36,485
Nooooooooooooooooooooooooooo!

214
00:43:43,927 --> 00:43:45,758
Why are you calling me? Huh?

215
00:43:45,967 --> 00:43:49,642
I haven't heard from you in over two years
And now you call?

216
00:43:49,807 --> 00:43:52,037
Yes.
I'm angry at you, Anna.

217
00:43:52,207 --> 00:43:54,277
I'm not gonna lie, I'm furious with you.
Mama...

218
00:43:54,447 --> 00:43:56,563
You trying to make me feel guilty?

219
00:43:56,727 --> 00:43:57,876
No.

220
00:43:58,247 --> 00:44:01,523
Don't you think I ever feel bad?
Don't you think I'm not thinking about

221
00:44:01,687 --> 00:44:04,201
All everything and the way I've raised you?

222
00:44:05,127 --> 00:44:08,119
No, mama, that's not it.
What is it then?

223
00:44:08,967 --> 00:44:11,481
I wanted to talk to you, for real.

224
00:44:11,687 --> 00:44:14,679
So you wake up one day and
You decided you're missing your mother?

225
00:44:15,247 --> 00:44:19,081
Anna, you okay? You're not in trouble are you?
No, mama I'm fine!

226
00:44:19,247 --> 00:44:20,566
Are you sure? Where are you?

227
00:44:21,127 --> 00:44:22,355
Far away.

228
00:44:25,207 --> 00:44:27,004
You have a place? What do you do for a living?

229
00:44:27,167 --> 00:44:30,921
It's okay, Mama... I'm fine.
So tell me, are you still with her?

230
00:44:31,087 --> 00:44:31,963
No!

231
00:44:32,127 --> 00:44:34,561
My God! She's still influencing you.

232
00:44:34,727 --> 00:44:36,524
She's still running your life! I can tell.
Mama?!

233
00:44:38,087 --> 00:44:41,079
Anna, she's worthless, she's a slut.

234
00:44:41,367 --> 00:44:44,643
You need to break free of that witch!
Don't stay with her.

235
00:44:46,007 --> 00:44:46,962
Anna?

236
00:44:47,687 --> 00:44:48,676
Anna, you there?

237
00:50:09,527 --> 00:50:10,642
Don't move.

238
00:51:03,567 --> 00:51:05,080
I can't help you...

239
00:51:35,647 --> 00:51:37,126
Don't be afraid..

240
00:51:37,407 --> 00:51:39,079
Calm down.

241
00:53:01,487 --> 00:53:03,284
Calm down.

242
00:53:11,927 --> 00:53:13,326
Rest well.

243
00:53:39,687 --> 00:53:40,676
Forgive me.

244
00:53:45,127 --> 00:53:46,321
I'm so sorry.

245
00:54:45,727 --> 00:54:46,876
Stop it!

246
00:55:48,207 --> 00:55:49,879
Who's the girl on the couch?

247
00:55:50,527 --> 00:55:51,801
Who is it?

248
00:55:52,807 --> 00:55:54,206
Lucie...

249
00:55:54,567 --> 00:55:57,286
Lucie who?
Lucie Jurin.

250
00:55:57,567 --> 00:55:58,682
And you?

251
00:56:00,447 --> 00:56:01,766
And?

252
00:56:01,967 --> 00:56:03,241
Anna Assaoui.

253
00:56:03,807 --> 00:56:06,844
We're trying to reach the Belfonds for hours.

254
00:56:07,007 --> 00:56:09,567
The phone's off the hook.
That should never have happened.

255
00:56:10,367 --> 00:56:12,483
Can you tell me what went on here?

256
00:56:15,247 --> 00:56:16,999
What are you doing here?

257
00:58:43,527 --> 00:58:46,758
Lucie Jurin escaped from us 15 years ago.

258
00:58:48,687 --> 00:58:49,722
Correct?

259
00:58:51,167 --> 00:58:52,361
Right.

260
00:58:52,567 --> 00:58:56,242
Back then we were disorganized.

261
00:58:56,527 --> 00:58:59,803
Gabrielle shouldn't have let her escaped

262
00:59:00,247 --> 00:59:03,922
After 15 years the girl found her.
I'm impressed by her tenacity.

263
00:59:04,087 --> 00:59:07,124
But in the end Lucie is just a victim.

264
00:59:07,847 --> 00:59:09,644
Like all of the others.

265
00:59:10,807 --> 00:59:14,561
It's so easy to create a victim, young lady.
So easy.

266
00:59:15,807 --> 00:59:19,959
You lock someone up in a room with no light.
Soon he begins to suffer.

267
00:59:20,527 --> 00:59:22,597
Then you feed that suffering.

268
00:59:22,887 --> 00:59:25,765
Slowly, methodically, systematically.

269
00:59:26,047 --> 00:59:28,641
Coldly for a long time.

270
00:59:29,527 --> 00:59:33,361
You then witness your subject
Going through several mental states.

271
00:59:33,887 --> 00:59:36,321
The mental trauma starts to break them...

272
00:59:37,167 --> 00:59:39,761
And that small easily opened cracks.

273
00:59:40,047 --> 00:59:43,357
Make them see things that don't exists in our world.

274
00:59:44,287 --> 00:59:46,676
Your friend Lucie what was her vision?

275
00:59:48,727 --> 00:59:50,206
Did she see anything?

276
00:59:51,247 --> 00:59:52,919
Not even one monster?

277
00:59:54,087 --> 00:59:56,317
Things that wanted to hurt her?

278
00:59:58,807 --> 00:59:59,956
A dead girl.

279
01:00:00,607 --> 01:00:02,086
There you go.

280
01:00:02,527 --> 01:00:03,721
A dead girl.

281
01:00:04,727 --> 01:00:09,357
The girl you found Sarah Dutreuil.
She saw insects. Cockroaches, everywhere.

282
01:00:09,647 --> 01:00:13,037
They were crawling all over her, she would rather
Chop off her own arm than to go through that.

283
01:00:13,327 --> 01:00:16,080
People ignore the existence of suffering.

284
01:00:17,807 --> 01:00:19,604
That's how our world sees it.

285
01:00:20,487 --> 01:00:22,603
Yet everyone's a victim.

286
01:00:24,607 --> 01:00:26,723
Martyrs are very rare.

287
01:00:27,527 --> 01:00:30,803
A true martyr is a special person.

288
01:00:31,287 --> 01:00:33,357
They're exceptional beings.

289
01:00:33,647 --> 01:00:36,684
They withstand paralyzing pain, young lady
They survive total depravation.

290
01:00:36,967 --> 01:00:39,435
They carry all the sins of the world.

291
01:00:39,607 --> 01:00:43,361
It's their sacrifice
They transcends themselves completely

292
01:00:43,527 --> 01:00:47,236
Do you understand?
They're transfigured!

293
01:00:51,927 --> 01:00:54,600
Longsheng Province in 1912.

294
01:00:55,567 --> 01:00:57,159
She didn't believe in God.

295
01:00:57,327 --> 01:01:00,399
She stole a hen
And it costs her dearly.

296
01:01:00,767 --> 01:01:04,476
In the photograph you can see she's...
Still very alive. Look at her eyes.

297
01:01:07,727 --> 01:01:09,604
Jouans Lussac, 1945.

298
01:01:09,767 --> 01:01:12,201
This woman was a grocer.
She slept with a German.

299
01:01:12,367 --> 01:01:14,881
Back then the French were sensitive.
So she had to pay.

300
01:01:15,647 --> 01:01:18,605
Look at her eyes... She's still alive.

301
01:01:19,927 --> 01:01:23,397
1960, a hospital in Birmingham.

302
01:01:24,087 --> 01:01:26,647
Just an ordinary young housewife
An atheist.

303
01:01:26,807 --> 01:01:29,685
Riddled with terminal cancer
Morphine had lost its effect for her.

304
01:01:30,287 --> 01:01:31,686
Her eyes tell the story.

305
01:01:32,207 --> 01:01:35,199
This one was beaten up by her husband.
Jealousy...

306
01:01:35,487 --> 01:01:37,637
This one just spent 9 hours
In the wreckage of her car.

307
01:01:37,927 --> 01:01:40,725
This young leukemia patient
Is dying. The pains gnawing.

308
01:01:41,007 --> 01:01:45,000
This woman has a rare disease.
She suffered for nine hours.

309
01:01:45,287 --> 01:01:47,084
Look at their eyes. Look.

310
01:01:47,487 --> 01:01:49,603
All of them. Do you hear me?

311
01:01:50,487 --> 01:01:53,559
They were all alive when these
Pictures were taken.

312
01:01:53,847 --> 01:01:56,725
No one can tell me that the
Concept of materydom

313
01:01:57,007 --> 01:01:58,918
Is an invention of the religious!

314
01:01:59,087 --> 01:02:01,157
We've tried everything.
Even with children.

315
01:02:01,447 --> 01:02:03,881
And it has been proven that women

316
01:02:05,007 --> 01:02:07,680
Are much more responsive to
Transfiguration.

317
01:02:09,087 --> 01:02:10,759
Yes, young women.

318
01:02:12,367 --> 01:02:13,436
That's how it is!

319
01:16:25,087 --> 01:16:26,202
Lucie: Anna?

320
01:16:27,247 --> 01:16:28,362
Lucie: Are you there?

321
01:16:29,047 --> 01:16:30,321
Anna: I'm here.

322
01:16:30,487 --> 01:16:32,955
Lucie: Anna?
Anna: Yes, Lucie.

323
01:16:33,567 --> 01:16:35,523
Lucie: Why are you never afraid?

324
01:16:35,687 --> 01:16:37,245
Lucie: Sometimes I'm afraid.

325
01:16:37,447 --> 01:16:38,880
Lucie: You're not like me.

326
01:16:39,087 --> 01:16:40,998
Lucie: I didn't go through what did.

327
01:16:42,727 --> 01:16:44,683
Lucie What do you have to do to
Stop being afraid?

328
01:16:45,087 --> 01:16:47,078
Anna: Just let go...

329
01:16:47,687 --> 01:16:49,040
Lucie: Do you think so?

330
01:16:49,367 --> 01:16:50,880
Anna: Just... let go.

331
01:16:52,167 --> 01:16:54,397
Lucie: If I can't do it,
will you be there for me?

332
01:16:54,567 --> 01:16:55,443
Anna: Yes.

333
01:16:55,607 --> 01:16:57,199
Anna: I miss you.

334
01:18:28,767 --> 01:18:31,156
You're not afraid anymore, Anna.

335
01:18:36,927 --> 01:18:38,645
You're not afraid anymore, Anna.

336
01:18:43,487 --> 01:18:44,806
You're not afraid anymore...

337
01:19:15,447 --> 01:19:17,119
It will be okay, Anna.

338
01:19:17,527 --> 01:19:19,006
It will be okay.

339
01:19:19,687 --> 01:19:21,803
The suffering is almost over.

340
01:19:24,127 --> 01:19:25,799
There's one stage left.

341
01:19:26,727 --> 01:19:28,046
The last one.

342
01:19:28,767 --> 01:19:32,476
It will be okay.
You don't have to defend yourself anymore.

343
01:19:32,647 --> 01:19:34,126
It's okay, Anna.

344
01:23:22,847 --> 01:23:24,803
Is she still holding up?
Yeah, it's amazing.

345
01:23:58,687 --> 01:24:00,996
Michel!
What is it?

346
01:24:10,487 --> 01:24:12,603
Just now. When I went to feed her.
A few minutes ago.

347
01:24:12,767 --> 01:24:15,042
Are you sure?
Like I said...

348
01:24:15,327 --> 01:24:17,557
I've never seen an expression like that.

349
01:24:17,727 --> 01:24:21,640
She's liberated, completely liberated.
Her face is like...

350
01:24:21,807 --> 01:24:24,321
Miss, her eyes...

351
01:24:24,487 --> 01:24:27,081
She doesn't see what's happening around
Her anymore.

352
01:24:27,247 --> 01:24:30,398
But is she still...
She's still alive.

353
01:27:17,927 --> 01:27:19,519
Have you seen it, then?

354
01:27:23,407 --> 01:27:25,284
The other world?

355
01:28:53,207 --> 01:28:56,563
Yesterday afternoon in this very
House

356
01:28:57,167 --> 01:28:59,476
Around 12.15...

357
01:28:59,767 --> 01:29:02,156
Anna Assaoui became a martyr.

358
01:29:02,847 --> 01:29:06,396
Let us take a moment to pay our
Respect.

359
01:29:07,167 --> 01:29:09,806
Anna Assaoui is an exceptional being.

360
01:29:10,207 --> 01:29:12,277
In our 17 years of research.

361
01:29:12,567 --> 01:29:15,559
She's only the fourth to have reach this stage.

362
01:29:15,847 --> 01:29:19,123
And the first, yes, the first...

363
01:29:19,447 --> 01:29:22,359
To have reported what she has seen.

364
01:29:22,647 --> 01:29:25,719
Between 12:15 to 2:30

365
01:29:26,007 --> 01:29:29,204
Miss Assaoui has clearly
Seen what comes after death.

366
01:29:29,687 --> 01:29:30,961
You heard correctly.

367
01:29:31,247 --> 01:29:34,523
Her state of ecstasy 
Lasted for 2 hours and 15 minutes.

368
01:29:34,687 --> 01:29:37,963
It was not a near-death experience.

369
01:29:38,247 --> 01:29:40,681
What she experienced was an
Authentic martyrdom.

370
01:29:40,967 --> 01:29:43,276
At 2.30 she left that state.

371
01:29:43,967 --> 01:29:46,686
And at 3:05 she spoke.

372
01:29:47,087 --> 01:29:49,840
At this very moment she is still alive.

373
01:29:50,247 --> 01:29:51,999
But no longer communicating.

374
01:29:52,287 --> 01:29:55,324
Miss heard her testimony.

375
01:29:55,607 --> 01:29:58,201
And this testimony...

376
01:29:58,367 --> 01:30:00,722
Will be shared within a few moments.

377
01:30:10,927 --> 01:30:12,155
Miss...

378
01:30:12,687 --> 01:30:14,325
They are waiting.

379
01:30:14,847 --> 01:30:16,917
I'm coming, Etienne.

380
01:30:22,247 --> 01:30:24,397
So there IS something?

381
01:30:24,727 --> 01:30:26,319
Of course.

382
01:30:27,527 --> 01:30:28,846
Was it clear?

383
01:30:31,367 --> 01:30:32,846
Like crystal.

384
01:30:33,967 --> 01:30:35,366
And precise?

385
01:30:37,247 --> 01:30:40,478
I see no other interpretation, Etienne.

386
01:30:43,127 --> 01:30:44,845
Thank you, Miss.

387
01:30:52,207 --> 01:30:53,117
Etienne?

388
01:30:55,887 --> 01:30:57,286
Miss?

389
01:30:58,047 --> 01:31:01,244
Have you ever try to imagine
The other world?

390
01:31:02,047 --> 01:31:03,639
Are you okay?

391
01:31:05,847 --> 01:31:08,236
Well, have you?
No, I...

392
01:31:10,807 --> 01:31:11,876
Keep doubting, Etienne.

393
01:31:19,847 --> 01:31:22,042
martyr:

394
01:31:22,207 --> 01:31:24,163
noun

395
01:31:24,327 --> 01:31:27,205
From the Greek word ''marturos''

396
01:31:27,367 --> 01:31:31,485
Witness