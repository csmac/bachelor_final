1
00:16:39,500 --> 00:16:40,500
Hey!

2
00:24:23,280 --> 00:24:24,690
Have I intruded...

3
00:25:09,650 --> 00:25:11,940
No, I didn't.
I didn't.

4
00:25:14,690 --> 00:25:15,810
How dare you?!

5
00:25:16,020 --> 00:25:18,480
I loved that gold watch!

6
00:25:22,570 --> 00:25:25,850
All right, all right!
It wasn't a gold watch!

7
00:26:20,280 --> 00:26:21,060
Yeah?

8
00:26:23,440 --> 00:26:25,310
What...does she look like?

9
00:26:26,820 --> 00:26:27,810
Hair?

10
00:26:29,360 --> 00:26:31,640
Blond, stringy.

11
00:26:32,480 --> 00:26:36,310
It's dirty probably.
Never washes it.

12
00:26:39,570 --> 00:26:40,940
Eyes... blue.

13
00:26:41,730 --> 00:26:43,560
Big blue eyes.

14
00:26:44,570 --> 00:26:47,650
I think she's really very stupid, yes.

15
00:26:48,110 --> 00:26:51,060
She looks stupid.
Stupid girl, yes.

16
00:26:52,360 --> 00:26:55,440
Her nose has no character.

17
00:26:57,190 --> 00:26:58,650
A punk nose.

18
00:27:00,320 --> 00:27:03,020
Mouth is too tiny.
Narrow mouthed.

19
00:27:07,900 --> 00:27:10,300
Legs are alright.

20
00:27:12,150 --> 00:27:13,010
Bosom?

21
00:27:13,860 --> 00:27:16,560
She has no bosom.

22
00:27:18,270 --> 00:27:20,310
No bosom at all.

23
00:27:20,530 --> 00:27:21,900
Stop that!

24
00:27:24,820 --> 00:27:26,560
You're very rude.

25
00:27:34,190 --> 00:27:36,600
She has a very vivid imagination.

26
00:27:36,980 --> 00:27:39,350
You know, she hears things.

27
00:27:39,980 --> 00:27:44,440
Names... machine guns...
God knows what.

28
00:27:45,730 --> 00:27:48,140
She thinks there's a war going on.

29
00:27:49,150 --> 00:27:51,820
That's not all.
A little while ago...

30
00:27:52,030 --> 00:27:54,430
...she thought she saw a unicorn.

31
00:27:55,400 --> 00:27:58,400
Yes, I tell you.
Unicorn.

32
00:28:01,400 --> 00:28:03,190
Of course it doesn't exists.

33
00:28:03,940 --> 00:28:07,270
Anyway, it's a very insipid animal.

34
00:28:14,320 --> 00:28:16,850
Oh God, she's moving about again.

35
00:28:17,320 --> 00:28:18,730
Very curious.

36
00:28:19,440 --> 00:28:20,680
What can I do?

37
00:28:39,230 --> 00:28:41,310
She ran away...

38
00:35:19,660 --> 00:35:21,790
The lady upstairs... she's dead.

39
00:35:26,210 --> 00:35:28,740
But you don't hear me.
She's very dead.

40
00:37:27,790 --> 00:37:30,410
No, you don't understand.
My name is Lily.

41
00:37:54,500 --> 00:37:57,370
I see. Lily is your name too.

42
00:38:05,660 --> 00:38:08,070
Funny. You spell it differently.

43
00:38:18,620 --> 00:38:19,530
Who is he?

44
00:38:24,960 --> 00:38:26,120
I'm sorry.

45
00:38:26,960 --> 00:38:28,030
Your sister.

46
00:38:32,960 --> 00:38:35,410
Yes, of course.
Your sister Lily.

47
00:38:47,620 --> 00:38:49,200
But your mother is dead.

48
00:38:55,620 --> 00:38:58,160
She passed away, I tell you.

49
00:39:01,250 --> 00:39:05,290
She is dead! And I want you to know
that it was not my fault!

50
00:44:59,370 --> 00:45:00,780
Let me out!

51
00:45:04,700 --> 00:45:05,780
Open the door!

52
00:45:05,960 --> 00:45:07,070
Open it!

53
00:45:08,000 --> 00:45:10,120
You! It's all your fault!

54
00:45:09,970 --> 00:45:12,450
I'm not gonna stay on top here for very long. Do you understand?

55
00:45:12,660 --> 00:45:15,120
If you think that you can keep me here forever, you're wrong!

56
00:45:15,290 --> 00:45:17,110
Let me get out right away!
Right away! Let me out!

57
00:45:17,410 --> 00:45:19,040
Help!

58
00:45:19,290 --> 00:45:20,230
Someone!

59
00:45:20,500 --> 00:45:22,780
You're really awful!
I hate you!

60
00:45:23,000 --> 00:45:24,820
You stupid old bitch!

61
00:45:25,080 --> 00:45:27,400
All right! If that what you want,
don't let me out!

62
00:45:27,620 --> 00:45:30,370
Don't! I don't care!

63
00:45:34,660 --> 00:45:35,610
I hate you!

64
00:47:21,080 --> 00:47:22,080
That...

65
00:47:22,410 --> 00:47:23,950
- Hope.
- Hope.

66
00:47:24,250 --> 00:47:26,450
Night of love...

67
00:47:28,080 --> 00:47:29,870
around us.

68
00:47:33,080 --> 00:47:36,990
Eyes, days, stars shines for
all remembered...

69
00:47:38,160 --> 00:47:40,040
in one slow breath.

70
00:47:41,200 --> 00:47:42,700
- Merged.
- Merged...

71
00:47:42,910 --> 00:47:44,070
in one slow breath.

72
00:47:44,330 --> 00:47:46,950
- But...
- The day hailed away...

73
00:47:47,160 --> 00:47:48,540
with all his gleam.

74
00:47:49,500 --> 00:47:50,820
Mighty death.

75
00:47:51,370 --> 00:47:52,990
- Magical.
- Magical death...

76
00:47:53,410 --> 00:47:54,820
threatening my life.

77
00:47:56,580 --> 00:47:59,360
- How could my love die...

78
00:48:00,830 --> 00:48:04,110
How could the endlessly living perish?

79
00:48:04,910 --> 00:48:07,200
Yearning eternal blindness,

80
00:48:07,830 --> 00:48:10,830
where forever love and rapture await.

81
00:48:12,910 --> 00:48:14,820
Heart on heart.

82
00:48:17,250 --> 00:48:18,900
Mouth on mouth.

83
00:48:23,950 --> 00:48:25,610
All is illusion.

84
00:48:26,950 --> 00:48:28,990
Set us free of this world.

85
00:48:35,370 --> 00:48:38,820
Dream of fire endless promise.

86
00:48:39,580 --> 00:48:41,730
Our hearts wonder wish,

87
00:48:42,540 --> 00:48:45,070
As full death,

88
00:48:45,290 --> 00:48:48,400
men named it evermore,
beautiful delusion.

89
00:48:48,830 --> 00:48:50,780
Sweet awaited desire.

90
00:48:52,120 --> 00:48:53,450
Never waking.

91
00:48:54,160 --> 00:48:56,620
Never fearing,
nameless there.

92
00:48:56,910 --> 00:48:59,150
Each to each, belonging.

93
00:49:01,750 --> 00:49:04,450
With love alone,
all life source.

94
00:49:44,000 --> 00:49:46,700
Would you please tell me what's going on around here?

95
00:51:15,160 --> 00:51:15,860
What?

96
00:51:16,450 --> 00:51:18,110
They've taken the city.

97
00:51:20,080 --> 00:51:22,480
Oh wooden horse, my God.

98
00:51:23,540 --> 00:51:27,150
And Brian? Brian is dead you say?

99
00:51:27,580 --> 00:51:28,780
What about Helen?

100
00:51:30,200 --> 00:51:30,870
Hello?

101
00:51:32,750 --> 00:51:33,690
Hello?

102
00:51:37,080 --> 00:51:39,200
That radio gotta be fixed.

103
00:51:49,330 --> 00:51:50,110
The girl.

104
00:51:52,490 --> 00:51:56,440
She just lost her bloomers and
now she's eating the cheese.

105
00:51:56,790 --> 00:51:58,910
Of course, there was Christmas cheese.

106
00:52:14,000 --> 00:52:16,320
I know what you think about.

107
00:52:43,870 --> 00:52:45,450
I don't like it.

108
00:53:33,200 --> 00:53:35,410
She'll find out about the greeks.

109
00:54:14,830 --> 00:54:15,990
Come here dear!

110
00:54:16,240 --> 00:54:19,440
Come, come, my little one.
I'll explain everything to you now.

111
00:54:20,370 --> 00:54:23,150
Every single thing you want to know.

112
00:54:26,240 --> 00:54:27,360
Don't! Don't!

113
00:54:27,830 --> 00:54:29,450
There's nothing to see over there.

114
00:54:29,790 --> 00:54:30,820
Oh no.

115
00:54:31,160 --> 00:54:33,990
Please don't go near the window.
It's not safe!

116
00:54:35,950 --> 00:54:38,360
She's looking out, the silly little bitch!

117
00:54:38,620 --> 00:54:40,990
Damnit! I told you this would happen!

118
00:54:41,160 --> 00:54:43,150
She's a conniving little slut!
I always knew it...

119
00:55:00,370 --> 00:55:02,240
She's making a dash for the door.

120
00:55:02,410 --> 00:55:04,900
Of course it's closed!
She's trying to open it, she's kicking it.

121
00:55:05,120 --> 00:55:07,610
Now she's coming back, the little...

122
00:55:07,910 --> 00:55:09,070
She just fell...

123
00:55:10,330 --> 00:55:11,790
Now she's off the window...

124
00:55:46,410 --> 00:55:47,950
Have you seen a unicorn?

125
00:55:52,370 --> 00:55:54,280
Have you seen a unicorn?

126
00:56:47,040 --> 00:56:49,900
Have you seen a unicorn?

127
00:58:29,630 --> 00:58:31,500
Where is that unicorn?

128
00:58:38,630 --> 00:58:40,290
Answer me!

129
01:02:28,760 --> 01:02:31,330
Out, out, brief candle.

130
01:02:32,090 --> 01:02:35,840
Life's but a walking shadow.

131
01:02:40,000 --> 01:02:41,000
Hey!

132
01:03:56,380 --> 01:03:57,760
Stupid, stupid tree.

133
01:04:38,550 --> 01:04:39,790
Did you say something?

134
01:04:41,720 --> 01:04:43,510
I said you're mean.

135
01:04:45,090 --> 01:04:47,760
What do you mean 'mean'?

136
01:04:48,550 --> 01:04:50,510
Why are you keep running after me?

137
01:04:50,970 --> 01:04:52,790
I find it rather tedious.

138
01:04:53,220 --> 01:04:55,290
What is it you want exactly?

139
01:04:59,050 --> 01:05:01,420
I don't know really,
but I'm not mean.

140
01:05:03,220 --> 01:05:04,250
You're not?

141
01:05:04,720 --> 01:05:06,340
What about those poor children...

142
01:05:06,510 --> 01:05:08,000
I just saw you beating up?

143
01:05:08,220 --> 01:05:11,800
- I'm not...
- Shut up, let me speak!

144
01:05:12,090 --> 01:05:14,080
I saw how you kick that tree...

145
01:05:14,510 --> 01:05:18,960
and trampled those innocent flowers to death.
And you dare to say you're not mean?

146
01:05:21,300 --> 01:05:24,800
You should practice what you preach!
You're eating the flowers.

147
01:05:33,510 --> 01:05:34,920
Suppose we change the subject?

148
01:05:40,050 --> 01:05:41,880
I don't mean to be rude,

149
01:05:42,220 --> 01:05:43,960
but you're not very graceful.

150
01:05:44,800 --> 01:05:47,750
In my books, unicorns are slim and white.

151
01:05:49,720 --> 01:05:54,290
The most beautiful things in the world
are the most useless.

152
01:05:54,510 --> 01:05:57,180
Peacocks and lillies, for instance.

153
01:06:00,760 --> 01:06:01,840
You know,

154
01:06:02,300 --> 01:06:05,500
the old lady upstairs.
She didn't want me to see you.

155
01:06:07,300 --> 01:06:11,870
Oh Lord, you mean that babbling-beady-on-the-while upstairs.

156
01:06:12,180 --> 01:06:14,210
Don't pay any attention to her.

157
01:06:14,680 --> 01:06:16,330
She's not even real.

158
01:06:16,680 --> 01:06:21,000
What do you mean she's not real?
I touched her. I spoked to her. I even saw her die.

159
01:06:26,340 --> 01:06:28,800
I mean what I mean.

160
01:06:39,930 --> 01:06:44,470
My little one, I could give you some precious information...

161
01:06:44,840 --> 01:06:46,960
concerning that old hag.

162
01:07:00,010 --> 01:07:03,210
Lunatic driving baddie.
I'm leaving right away.

163
01:07:04,420 --> 01:07:07,710
And I won't be back in 154 years.

164
01:07:07,930 --> 01:07:09,300
Hey, come back!

165
01:07:09,550 --> 01:07:11,170
I like talking to you.

166
01:07:11,470 --> 01:07:13,010
Nobody talks to me here.

167
01:13:22,680 --> 01:13:24,150
Humphrey.

168
01:14:19,550 --> 01:14:20,750
What's the matter?

169
01:14:40,840 --> 01:14:42,380
What's the matter now?

170
01:14:56,470 --> 01:14:58,090
Who's Humphrey?

171
01:15:04,800 --> 01:15:07,630
- You mean the rat?
- Yes, I mean the rat.

172
01:15:08,840 --> 01:15:10,170
Did you have a fight?

173
01:15:11,390 --> 01:15:14,050
Yes, but that's nothing new.

174
01:15:14,470 --> 01:15:16,790
He's so opinionated and stubborn.

175
01:15:17,090 --> 01:15:20,210
I love him, you understand my sweet thing?

176
01:15:20,380 --> 01:15:23,830
We went in so many roads together.

177
01:15:24,090 --> 01:15:26,930
So many seasons I've seen us passed.

178
01:15:27,180 --> 01:15:30,380
So many hopes...

179
01:15:34,720 --> 01:15:38,050
I see the waves in the ocean.

180
01:16:04,970 --> 01:16:06,760
Hungry, hungry.

181
01:16:09,300 --> 01:16:10,930
Just a minute, please.

182
01:16:11,760 --> 01:16:12,840
Just a minute.

183
01:16:37,010 --> 01:16:39,000
I'm afraid I don't have much milk.

184
01:17:20,300 --> 01:17:21,760
Hey! Your friend's back.

185
01:17:50,390 --> 01:17:54,590
Willy boy, Willy boy,
Where are you going?

186
01:17:54,880 --> 01:17:58,420
I will go with you, if I may.

187
01:17:59,590 --> 01:18:03,540
I'm going to the meadow to see them...

188
01:18:03,840 --> 01:18:08,000
I'm going to help them make the hay.

189
01:18:09,670 --> 01:18:13,120
I'm going to help them fill the barn,

190
01:18:13,970 --> 01:18:18,130
with summer hay to lay upon.

191
01:18:27,510 --> 01:18:32,050
Nobody home

192
01:18:32,590 --> 01:18:37,460
No bread, no drink, no money
how we none

193
01:18:38,130 --> 01:18:43,210
yet we shall be bliss.

194
01:18:45,880 --> 01:18:48,250
Nobody home.

195
01:18:48,680 --> 01:18:53,380
No drink, no bread, no money
how we none.

196
01:18:54,550 --> 01:18:58,760
yet we shall be bliss.

197
01:35:27,030 --> 01:35:28,030
Hello?

198
01:35:30,280 --> 01:35:31,310
Can you hear me?

199
01:35:39,450 --> 01:35:40,310
Hello?

200
01:35:43,450 --> 01:35:44,730
Is nobody there?

201
01:35:54,660 --> 01:35:55,570
Hello?

202
01:35:56,360 --> 01:35:57,440
Can't you hear me?

203
01:35:59,860 --> 01:36:01,570
Anybody hear me?

204
01:38:58,990 --> 01:39:00,730
Just a minute, please.

