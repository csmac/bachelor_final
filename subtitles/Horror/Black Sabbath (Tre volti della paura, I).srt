1
00:00:35,540 --> 00:00:47,050
- l'M NOT SCARED -

2
00:01:47,420 --> 00:01:50,490
- Michele!
- Maria!

3
00:01:51,420 --> 00:01:52,890
l fell!

4
00:01:58,820 --> 00:01:59,960
Where are you going?

5
00:02:06,420 --> 00:02:09,370
- They broke.
- l told you not to come.

6
00:02:09,660 --> 00:02:10,690
Now what?

7
00:02:11,460 --> 00:02:13,290
We'll tape them.
Let's go.

8
00:02:14,660 --> 00:02:16,090
Come on, run!

9
00:02:26,340 --> 00:02:29,410
- How gross!
- They passed by here.

10
00:02:41,500 --> 00:02:42,530
There's a house!

11
00:02:49,140 --> 00:02:52,170
Congratulations,
you finally made it.

12
00:02:52,740 --> 00:02:54,570
My sister hurt herself.

13
00:02:56,940 --> 00:02:59,610
- The others?
- They're down there.

14
00:03:08,260 --> 00:03:11,130
- Who won?
- Skull.

15
00:03:18,620 --> 00:03:21,080
- This place is mine.
- Why?

16
00:03:21,180 --> 00:03:23,050
Because l saw it first.

17
00:03:24,580 --> 00:03:27,010
Finders keepers...

18
00:03:28,500 --> 00:03:29,930
Who's the rotten egg?

19
00:03:30,820 --> 00:03:33,570
You, because you came last.

20
00:03:36,020 --> 00:03:39,970
l had to stop
because my sister got hurt.

21
00:03:40,220 --> 00:03:43,450
- You lose.
- Who has to pay up?

22
00:03:45,860 --> 00:03:46,730
She does.

23
00:03:47,460 --> 00:03:52,010
No fair, why always me?
Let's vote on it.

24
00:03:52,180 --> 00:03:54,410
He can't always decide everything.

25
00:03:55,980 --> 00:03:59,050
Let's vote, l say you lost.

26
00:03:59,500 --> 00:04:00,850
- Me too.
- Me too.

27
00:04:01,300 --> 00:04:04,130
- Me too.
- Me too.

28
00:04:04,780 --> 00:04:05,650
See?

29
00:04:09,900 --> 00:04:11,040
So what do l have to do?

30
00:04:12,500 --> 00:04:16,450
- Show it to us.
- No way!

31
00:04:16,820 --> 00:04:19,450
- What does she have to show us?
- Shhh!

32
00:04:22,780 --> 00:04:25,340
- You lost, so do it.
- No.

33
00:04:26,060 --> 00:04:26,730
No?

34
00:04:33,780 --> 00:04:35,330
Aren't you going to say anything?

35
00:04:38,900 --> 00:04:41,970
Alright, but l won't play
with you anymore, jerks!

36
00:04:46,300 --> 00:04:49,840
- Are you crying?
- No.

37
00:05:33,380 --> 00:05:34,170
Stop!

38
00:05:37,540 --> 00:05:41,290
l'll pay up, l came last.

39
00:05:53,300 --> 00:05:54,440
Fuck!

40
00:05:57,220 --> 00:05:59,050
Walk across that.

41
00:06:30,220 --> 00:06:32,130
<i>He was the Lizard Man,..</i>

42
00:06:32,220 --> 00:06:34,050
<i>..he could walk on walls.</i>

43
00:06:46,740 --> 00:06:51,890
Then he became the Glass Man,
if he falls he breaks.

44
00:06:52,060 --> 00:06:54,090
- What are you saying?
- Nothing.

45
00:07:03,380 --> 00:07:05,680
Leap onto that branch
and climb down.

46
00:07:27,620 --> 00:07:28,370
<i>Michele,..</i>

47
00:07:29,620 --> 00:07:30,840
<i>..give me my glasses.</i>

48
00:07:34,540 --> 00:07:37,970
- l lost them.
- Mom will kill you.

49
00:07:38,820 --> 00:07:39,880
Wait here.

50
00:09:26,660 --> 00:09:29,890
A cave filled with gold and gems.

51
00:09:52,500 --> 00:09:56,040
- Did you find them?
- Run, we're late.

52
00:10:15,620 --> 00:10:16,680
There's Mom.

53
00:10:19,420 --> 00:10:22,490
Where'd you go?
l was searching for you!

54
00:10:22,740 --> 00:10:25,490
How many times l gotta tell you
to stay nearby?

55
00:10:25,940 --> 00:10:27,490
<i>Michele, this time....</i>

56
00:10:27,940 --> 00:10:30,500
<i>- We went....
- Home, right now!</i>

57
00:10:43,460 --> 00:10:44,810
Dad's here!

58
00:10:51,580 --> 00:10:52,290
Dad!

59
00:10:53,100 --> 00:10:56,720
You're heavy!
Daddy's doll, gimme a kiss.

60
00:10:57,180 --> 00:10:58,890
Where were you?

61
00:10:58,980 --> 00:11:01,050
Playing by the stream.
How long are you staying?

62
00:11:01,300 --> 00:11:05,250
For a bit. You broke 'em?

63
00:11:05,900 --> 00:11:08,170
- Again?
- She fell.

64
00:11:08,300 --> 00:11:10,250
We can fix them with tape.

65
00:11:10,500 --> 00:11:13,250
- Michele, what did l tell you?
- Never mind.

66
00:11:14,420 --> 00:11:15,850
Did you do your push-ups?

67
00:11:16,500 --> 00:11:19,250
- No, he didn't.
- Yes, l did.

68
00:11:19,820 --> 00:11:22,280
We'll see, if you don't beat me,
no present for you.

69
00:11:22,740 --> 00:11:26,280
- A present? What present?
- What present? - You'll see.

70
00:11:27,740 --> 00:11:28,770
So...

71
00:11:34,220 --> 00:11:37,450
Christ, are your muscles
made of butter?

72
00:11:38,940 --> 00:11:40,770
Come on.

73
00:11:41,740 --> 00:11:43,370
You're a sissy.

74
00:11:50,140 --> 00:11:51,890
Maria, help him out.

75
00:12:02,060 --> 00:12:04,490
Give us the present now!

76
00:12:05,140 --> 00:12:06,690
Anna, get the box.

77
00:12:13,820 --> 00:12:16,090
Easy!

78
00:12:21,340 --> 00:12:22,810
- A boat.
- No.

79
00:12:23,340 --> 00:12:25,690
lt ain't no boat, it's a gondola.

80
00:12:26,740 --> 00:12:30,090
- What's that?
- A boat you only find in Venice.

81
00:12:30,860 --> 00:12:33,210
- With only one oar?
- What's an oar?

82
00:12:33,740 --> 00:12:35,490
A stick that moves the boat.

83
00:12:36,980 --> 00:12:38,410
- Pretty!
- No.

84
00:12:40,060 --> 00:12:41,810
You can't play with this.

85
00:12:43,020 --> 00:12:50,210
<i>The Police
from the Artistic Heritage Unit..</i>

86
00:12:50,300 --> 00:12:52,930
<i>..recovered two precious
16th century sculptures..</i>

87
00:12:53,100 --> 00:12:55,330
<i>..attributed to Benvenuto Cellini.</i>

88
00:12:55,540 --> 00:12:57,970
Whose turn to get the wine?

89
00:12:58,060 --> 00:13:00,930
- Maria's. - l don't feel like it.
You go. - No!

90
00:13:01,500 --> 00:13:04,060
- lt's your turn.
- l'm not going.

91
00:13:04,300 --> 00:13:06,370
- Why not?
- My head hurts.

92
00:13:06,500 --> 00:13:08,130
That's what you always say!

93
00:13:08,220 --> 00:13:11,760
Shut up, let me hear!

94
00:13:11,900 --> 00:13:14,360
Knock it off! Go, Michele.

95
00:13:16,260 --> 00:13:18,010
lt's not fair.

96
00:13:19,140 --> 00:13:22,890
<i>After the defeat
of the fencing team in Hamburg....</i>

97
00:13:23,140 --> 00:13:24,810
What happened?

98
00:13:25,420 --> 00:13:27,450
lt's her turn to get the wine.

99
00:13:30,300 --> 00:13:35,490
ln war, know how they'd decide
who went on deadly missions?

100
00:13:43,540 --> 00:13:46,810
Whoever picks the broken one,
has to go.

101
00:13:59,260 --> 00:14:00,930
lt's your turn!

102
00:14:02,460 --> 00:14:06,000
You lost, go get the wine.

103
00:14:34,540 --> 00:14:36,730
<i>So they bury him..</i>

104
00:14:36,820 --> 00:14:39,010
<i>..and he remains
in the belly of the earth..</i>

105
00:14:41,660 --> 00:14:45,130
<i>..among secrets, corpses, bones,
skeletons, and darkness.</i>

106
00:19:01,340 --> 00:19:04,090
Where were you?
Your Mom's looking for you.

107
00:19:04,260 --> 00:19:06,010
They're mad.

108
00:19:06,580 --> 00:19:08,410
Are the shoes comfy?

109
00:19:08,660 --> 00:19:10,490
We'll buy them.

110
00:19:13,060 --> 00:19:17,330
Hope l can convince my Mom,
they're really cute.

111
00:19:18,580 --> 00:19:24,010
- Anna, what do you think?
- Let me see. What do you think?

112
00:19:26,380 --> 00:19:28,130
How much you want for these?

113
00:19:30,180 --> 00:19:33,650
- How much are they?
- lt's a good brand.

114
00:19:33,820 --> 00:19:35,650
l've got other models, too.

115
00:19:41,380 --> 00:19:44,650
Pino, your son's here.

116
00:19:48,900 --> 00:19:51,330
- Where were you?
- Wandering around.

117
00:19:51,500 --> 00:19:53,530
Wandering like hell!
You know what time it is?

118
00:19:53,620 --> 00:19:55,810
lt's almost 2!
No one knew where you were.

119
00:19:55,900 --> 00:19:57,120
Your Mom went looking for you.

120
00:19:57,220 --> 00:20:00,210
You gotta stop doing
whatever the fuck you please!

121
00:20:00,300 --> 00:20:03,290
lf you wanna do as you please,
then leave.

122
00:20:03,380 --> 00:20:04,570
Get lost.

123
00:20:07,740 --> 00:20:08,960
Get lost!

124
00:21:06,380 --> 00:21:07,890
Michele, come down!

125
00:21:08,060 --> 00:21:10,090
- What do you want?
- Dinner's ready.

126
00:21:10,260 --> 00:21:13,290
Tell them l'm not their son anymore,
l'm not coming back.

127
00:21:13,460 --> 00:21:16,020
- You're not my brother either?
- No!

128
00:21:16,140 --> 00:21:19,290
- Then can l have your comic books?
- No.

129
00:21:19,780 --> 00:21:21,000
That has nothing to do with it.

130
00:21:25,300 --> 00:21:27,370
Mom will get mad.

131
00:21:29,220 --> 00:21:31,850
- Where's Dad?
- He went out, he'll be back late.

132
00:21:34,340 --> 00:21:37,290
- What's for dinner?
- Mashed potatoes and eggs.

133
00:22:26,100 --> 00:22:28,660
Good morning!

134
00:22:28,740 --> 00:22:30,930
Did you fall out of bed?

135
00:22:31,020 --> 00:22:33,320
- l had lots on my mind.
- What was on your mind?

136
00:22:33,420 --> 00:22:35,250
Give daddy a kiss.

137
00:22:45,620 --> 00:22:49,570
- l thought you were mad.
- What was on your mind?

138
00:22:50,460 --> 00:22:53,610
- Tell us.
- Sad thoughts.

139
00:22:54,940 --> 00:22:58,480
Get ready 'cause tonight
we're gonna arm wrestle.

140
00:22:58,860 --> 00:22:59,810
Anna!

141
00:23:00,380 --> 00:23:03,290
Buy me some sausages.

142
00:23:18,660 --> 00:23:21,810
- Dad, when will you be back?
- Late!

143
00:23:33,380 --> 00:23:34,440
Michele.

144
00:23:36,580 --> 00:23:38,450
One, two, three, red light!

145
00:23:40,980 --> 00:23:42,040
Michele.

146
00:23:47,100 --> 00:23:48,970
- What are you doing?
- l'm not playing anymore.

147
00:23:49,500 --> 00:23:50,970
Want me to come along?

148
00:23:53,820 --> 00:23:55,250
Forget about him.

149
00:24:49,700 --> 00:24:51,650
Water...

150
00:24:52,900 --> 00:24:54,570
Are you alive?

151
00:24:59,420 --> 00:25:03,770
- Water... - What did you say?
l didn't understand.

152
00:25:08,140 --> 00:25:12,570
- Water.
- You're thirsty! You want water?

153
00:25:12,820 --> 00:25:15,200
- Thirsty.
- Wait.

154
00:26:28,780 --> 00:26:30,000
Hungry.

155
00:26:32,300 --> 00:26:36,450
You're hungry?
l don't have anything to eat.

156
00:26:49,020 --> 00:26:51,090
l have to go home now.

157
00:26:51,540 --> 00:26:54,690
lf you want,
l'll bring something tomorrow.

158
00:27:04,940 --> 00:27:07,010
Are you a child?

159
00:27:57,900 --> 00:27:59,120
What are you doing here?

160
00:27:59,500 --> 00:28:04,130
l know you.
You're Amitrano's son, right?

161
00:28:04,300 --> 00:28:08,250
<i>You better not come here no more.
lf l see you again....</i>

162
00:28:08,500 --> 00:28:09,850
See them things?

163
00:28:11,220 --> 00:28:15,370
They're always hungry,
they even eat bones.

164
00:28:55,380 --> 00:28:56,210
Michele...

165
00:28:56,780 --> 00:29:00,400
- What are you doing?
- Nothing, just playing.

166
00:29:01,980 --> 00:29:03,730
Put everything back.

167
00:29:22,300 --> 00:29:26,050
- l don't want anymore.
- Eat your meat.

168
00:29:26,140 --> 00:29:28,330
lt gives me a headache.

169
00:29:28,420 --> 00:29:31,450
- l can't take this anymore!
- Anna... lf she doesn't want it...

170
00:29:31,620 --> 00:29:33,170
No big deal.

171
00:29:53,460 --> 00:29:56,330
Someone's coming in a few days.

172
00:29:56,780 --> 00:29:59,730
So no fuss and no whining.

173
00:30:00,980 --> 00:30:02,410
Who is it?

174
00:30:04,780 --> 00:30:05,530
A friend.

175
00:30:06,660 --> 00:30:09,530
- What's his name?
- Sergio.

176
00:30:11,500 --> 00:30:12,850
What a funny name.

177
00:30:16,220 --> 00:30:19,970
A Dad and Mom
are expecting a baby.

178
00:30:20,620 --> 00:30:23,970
But instead, two are born,
one blond, one brunet.

179
00:30:24,500 --> 00:30:28,170
The brunet is normal,
the blond is crazy.

180
00:30:28,340 --> 00:30:32,040
When she nurses him,
he bites her and draws blood.

181
00:30:32,220 --> 00:30:33,730
The Mom doesn't want him anymore.

182
00:30:33,820 --> 00:30:36,120
So she tells the Dad to kill him.

183
00:30:36,220 --> 00:30:38,890
<i>The Dad takes him up
to a tall mountain,..</i>

184
00:30:38,980 --> 00:30:41,810
<i>..but he can't stab him
because it's a sin.</i>

185
00:30:43,620 --> 00:30:46,290
So he digs a hole
and puts him in it.

186
00:30:46,420 --> 00:30:49,690
He brings him water and food
so he won't die.

187
00:30:51,820 --> 00:30:52,960
<i>So he..</i>

188
00:30:57,940 --> 00:31:01,210
<i>..won't... die.</i>

189
00:31:25,980 --> 00:31:26,930
Hi, Michele.

190
00:31:39,220 --> 00:31:41,250
What do you need?

191
00:31:44,100 --> 00:31:46,370
What can l buy with 500 liras?

192
00:31:47,220 --> 00:31:49,450
Don't know,
depends on what you need.

193
00:31:49,740 --> 00:31:52,970
- Something good,
do you have chocolate cookies? - No.

194
00:31:53,740 --> 00:31:54,960
Sweets are over there.

195
00:31:58,740 --> 00:32:01,770
A doughnut, a chocolate bar...

196
00:32:04,700 --> 00:32:06,050
Or a Kit Kat.

197
00:32:11,020 --> 00:32:12,490
They're all good.

198
00:32:16,380 --> 00:32:18,810
- Assunta...
- What is it?

199
00:32:18,980 --> 00:32:22,600
lf someone's hungry,
what can they buy with 500 liras?

200
00:32:23,980 --> 00:32:26,330
They can buy bread.

201
00:32:26,780 --> 00:32:29,810
When someone's hungry,
they buy bread.

202
00:34:33,700 --> 00:34:36,050
l wanted to ask you:

203
00:34:36,700 --> 00:34:42,770
<i>..do you know Felice,
Skull's brother? Felice Natale.</i>

204
00:34:44,380 --> 00:34:45,600
l hate him.

205
00:34:49,300 --> 00:34:51,330
ls he the one who feeds you?

206
00:34:53,300 --> 00:34:56,920
<i>l saw him driving away from here,..</i>

207
00:34:57,100 --> 00:34:58,160
<i>..so l thought....</i>

208
00:35:11,220 --> 00:35:12,890
l'm going home now.

209
00:35:17,380 --> 00:35:18,250
Okay?

210
00:35:23,420 --> 00:35:24,450
Bye bye.

211
00:35:30,500 --> 00:35:32,050
Are you deaf?

212
00:35:53,460 --> 00:35:57,290
Listen. Sorry.
l thought of something.

213
00:35:58,060 --> 00:36:00,890
lf you won't eat the bread,
will you give it back?

214
00:36:17,100 --> 00:36:23,250
Let me explain. lf Felice comes
and finds it, he'll figure it out.

215
00:36:37,220 --> 00:36:39,170
Then l'll come get it.

216
00:37:58,940 --> 00:37:59,770
Listen...

217
00:38:01,620 --> 00:38:03,170
What's your name?

218
00:38:06,340 --> 00:38:10,690
What's your Dad's name?
Mine is named Pino.

219
00:38:12,260 --> 00:38:16,010
ls your Dad named Pino too,
by chance?

220
00:38:23,260 --> 00:38:27,330
- l'm leaving.
- Coons...

221
00:38:31,380 --> 00:38:32,730
Did you say something?

222
00:38:35,380 --> 00:38:36,810
l don't understand.

223
00:38:39,580 --> 00:38:42,250
- Coons...
- Coons?

224
00:38:43,980 --> 00:38:48,130
- What do you mean?
- Raccoons.

225
00:38:48,780 --> 00:38:52,250
<i>lf you leave the window open,
raccoons will come in..</i>

226
00:38:52,420 --> 00:38:57,170
<i>..and steal cakes, cookies...
- There are no coons here.</i>

227
00:38:59,020 --> 00:39:02,250
Raccoons can bite people.

228
00:39:07,940 --> 00:39:11,090
Did you eat
a piece of meat yesterday?

229
00:39:15,540 --> 00:39:17,010
lt's very important.

230
00:39:20,780 --> 00:39:22,930
Are you my Guardian Angel?

231
00:39:25,660 --> 00:39:30,010
l'm not an angel.
What's a Guardian Angel?

232
00:39:32,620 --> 00:39:34,850
The one who says true things.

233
00:39:36,900 --> 00:39:40,650
You can tell me,
l won't tell anyone.

234
00:39:42,100 --> 00:39:46,290
l'm not an angel.
l'm Michele.

235
00:39:46,620 --> 00:39:48,290
Michele Amitrano, fifth grade.

236
00:40:21,980 --> 00:40:23,650
Dead?

237
00:40:24,500 --> 00:40:28,040
- What?
- Am l dead?

238
00:40:28,620 --> 00:40:33,480
- Dead? What?
- l'm dead!

239
00:40:34,220 --> 00:40:39,770
l'm dead! l'm dead! l'm dead!

240
00:40:39,940 --> 00:40:47,090
l'm dead! l'm dead! l'm dead!

241
00:41:02,660 --> 00:41:04,930
<i>Remember when Mrs. Destani..</i>

242
00:41:05,020 --> 00:41:08,210
<i>..told us about the miracle
of Lazarus? - Yes.</i>

243
00:41:08,620 --> 00:41:12,450
When Lazarus came back to life,
did he know he'd been dead?

244
00:41:12,700 --> 00:41:15,850
No, l guess he thought he felt ill.

245
00:41:19,500 --> 00:41:22,890
Do you think only Jesus can bring
the dead back to life?

246
00:41:23,140 --> 00:41:26,680
Dunno, but my Aunt
told me a true story.

247
00:41:27,140 --> 00:41:31,370
Once, someone's son died
all mangled under a car.

248
00:41:31,460 --> 00:41:34,450
The Dad couldn't handle it,
he cried all day.

249
00:41:34,540 --> 00:41:39,450
He gave a psychic all his money
to bring his son back to life.

250
00:41:39,540 --> 00:41:43,210
So the psychic said: ''Go home,
your son will return tonight''.

251
00:41:43,940 --> 00:41:47,090
<i>The Dad waited,
but the son didn't return,..</i>

252
00:41:47,580 --> 00:41:49,210
<i>..so he went to bed.</i>

253
00:41:49,580 --> 00:41:51,490
Then he heard steps in the kitchen.

254
00:41:51,980 --> 00:41:54,730
<i>He got up
and saw his mangled son,..</i>

255
00:41:54,860 --> 00:41:56,730
<i>..with his brains dripping out.</i>

256
00:41:57,180 --> 00:42:01,010
His son said he hated him,
it was his fault he died.

257
00:42:01,300 --> 00:42:04,730
He left him in the street
to go chase after women.

258
00:42:05,980 --> 00:42:07,120
And then?

259
00:42:07,300 --> 00:42:10,330
So his Dad set him on fire
with gasoline.

260
00:42:10,780 --> 00:42:14,050
- He did good.
- Sure did.

261
00:42:43,340 --> 00:42:47,210
- What are you doing?
- l have to pee, go to sleep.

262
00:42:56,460 --> 00:43:00,000
- What do they say in Milan? - What
the hell do you want them to say?

263
00:43:00,140 --> 00:43:02,770
<i>They said it would end soon,..</i>

264
00:43:02,860 --> 00:43:05,290
<i>..it's been months! What's going on?
l don't understand!</i>

265
00:43:05,380 --> 00:43:09,050
You don't need to understand. The less
you think, the less you'll screw up.

266
00:43:09,140 --> 00:43:11,170
lt'll take time.

267
00:43:11,260 --> 00:43:13,050
lt might take six months.

268
00:43:13,140 --> 00:43:15,410
- Six months? - Six months,
a year, whatever it takes.

269
00:43:15,500 --> 00:43:17,210
My ass!

270
00:43:17,300 --> 00:43:20,210
Shut up!
They ain't telling us anything!

271
00:43:20,300 --> 00:43:21,570
You don't need to know anything.

272
00:43:21,700 --> 00:43:25,050
You just have
to keep the kid hidden.

273
00:43:25,340 --> 00:43:28,570
Hide him and stay cool.

274
00:43:29,140 --> 00:43:31,290
You've fucked up
over and over again.

275
00:43:31,420 --> 00:43:33,690
He's the stupidest of all.

276
00:43:34,140 --> 00:43:36,410
Like l said, you're not capable.

277
00:43:37,460 --> 00:43:39,290
Relax.

278
00:43:39,380 --> 00:43:41,680
Relax? They ain't paying!

279
00:43:41,860 --> 00:43:44,420
They said Carducci had money
coming out of his ass.

280
00:43:44,500 --> 00:43:45,170
Bullshit!

281
00:43:45,260 --> 00:43:47,090
Shut up!

282
00:43:48,380 --> 00:43:52,210
You're the worst here,
'cause you think.

283
00:43:52,380 --> 00:43:53,810
- Me?
- Yes.

284
00:43:56,220 --> 00:43:59,570
Fuck you, expert my ass.

285
00:43:59,820 --> 00:44:02,450
You're an idiot, let me tell you!

286
00:44:04,340 --> 00:44:06,800
The news!

287
00:44:07,180 --> 00:44:10,090
Anna, turn off the light.
Turn up the volume.

288
00:44:12,860 --> 00:44:15,490
<i>Good evening.
Human rights defense..</i>

289
00:44:15,660 --> 00:44:18,490
<i>..and patience in negotiations,
Carter says in Berlin.</i>

290
00:44:18,660 --> 00:44:21,890
ln Bonn tomorrow, the lndustrialized
Countries Summit begins.

291
00:44:22,140 --> 00:44:26,410
<i>Alarm in Trent for a toxic cloud....</i>

292
00:44:26,540 --> 00:44:27,370
l'll have one.

293
00:44:28,380 --> 00:44:30,970
They ain't gonna talk about it
today either.

294
00:44:31,060 --> 00:44:33,330
- They didn't yesterday.
- Well?

295
00:44:34,180 --> 00:44:35,530
l can't hear a thing.

296
00:44:37,780 --> 00:44:39,210
Here it is!

297
00:44:41,180 --> 00:44:43,890
<i>Throughout the whole region,
the search continues..</i>

298
00:44:43,980 --> 00:44:46,850
<i>..for little Filippo,
kidnapped in Milan.</i>

299
00:44:46,980 --> 00:44:51,920
The Police are following new leads
which seem to be promising.

300
00:44:52,180 --> 00:44:55,930
We will now air Luisa Carducci's
appeal to the kidnappers.

301
00:44:56,220 --> 00:44:58,600
What the fuck does she want?

302
00:44:58,700 --> 00:45:00,570
l'm Filippo's mother.

303
00:45:00,700 --> 00:45:03,050
l'm speaking
to my son's kidnappers.

304
00:45:03,820 --> 00:45:05,770
l implore you, don't hurt him.

305
00:45:05,900 --> 00:45:09,370
He's a good boy,
well mannered, and very shy.

306
00:45:09,740 --> 00:45:11,770
l implore you, treat him well.

307
00:45:13,140 --> 00:45:15,370
<i>The sum requested is steep,..</i>

308
00:45:15,620 --> 00:45:19,090
<i>..even for our family.
- They'll cough up the money.</i>

309
00:45:21,660 --> 00:45:24,530
You threatened to cut his ear off.

310
00:45:24,700 --> 00:45:26,410
l beg you not to do it.

311
00:45:27,580 --> 00:45:30,010
l must make another request:

312
00:45:30,580 --> 00:45:35,250
<i>..tell Filippo his Mom and Dad
love him dearly..</i>

313
00:45:35,340 --> 00:45:38,170
<i>..and always think of him.
- We'll cut off two ears!</i>

314
00:45:38,260 --> 00:45:38,930
Two!

315
00:45:51,420 --> 00:45:52,560
Who are you?

316
00:45:53,620 --> 00:45:56,290
l'm Michele, Michele Amitrano.

317
00:45:58,340 --> 00:46:00,090
Sounds like the name of a singer.

318
00:46:04,340 --> 00:46:06,090
l'm Materia Sergio.

319
00:46:07,740 --> 00:46:08,610
Pleased to meet you.

320
00:46:12,580 --> 00:46:16,330
Didn't they ever teach you to knock?

321
00:46:17,780 --> 00:46:18,730
Sorry.

322
00:46:24,180 --> 00:46:26,450
- Are you Pino's son?
- What?

323
00:46:26,580 --> 00:46:28,330
- Are you Pino's son?
- Yes.

324
00:46:35,580 --> 00:46:37,130
You're a quiet kid.

325
00:46:39,100 --> 00:46:41,050
l like quiet kids.

326
00:46:45,420 --> 00:46:46,850
Are you obedient, too?

327
00:46:49,460 --> 00:46:51,810
Then close the door and piss off.

328
00:46:51,900 --> 00:46:54,460
- What?
- Close the door and piss off!

