﻿1
00:00:17,500 --> 00:00:18,810
I can see ghosts.

2
00:00:18,811 --> 00:00:20,547
<i>No matter what happens,</i>

3
00:00:20,572 --> 00:00:21,981
<i>I'll protect you.</i>

4
00:00:21,982 --> 00:00:23,913
<i>Why is my father here?</i>

5
00:00:24,061 --> 00:00:26,781
<i>It appears that your dad was murdered.</i>

6
00:00:27,287 --> 00:00:31,460
<i>You killed Bong Pal's mother. It wasn't
enough, so you killed his father.</i>

7
00:00:31,461 --> 00:00:32,894
You've met his father before?

8
00:00:32,920 --> 00:00:34,471
It was at a temple
five years ago.

9
00:00:34,616 --> 00:00:37,392
<i>He asked me to give something to his son.</i>

10
00:00:37,393 --> 00:00:39,540
<i>He also asked me
not to show it to anyone.</i>

11
00:00:39,541 --> 00:00:42,204
<i>That is very important to Bong Pal.</i>

12
00:00:42,205 --> 00:00:44,725
I'm leaving tomorrow.
I will come back soon.

13
00:00:44,726 --> 00:00:46,504
Wait for me, Bong Pal.

14
00:00:46,505 --> 00:00:49,166
<i>You don't look so good.
Have some tea.</i>

15
00:00:51,788 --> 00:00:53,514
<i>[Episode 15]</i>
Monk Myung Chul.

16
00:00:55,308 --> 00:00:59,326
Did you know about it, by any chance?

17
00:00:59,327 --> 00:01:01,098
What do you mean?

18
00:01:03,930 --> 00:01:05,910
The detective came by.

19
00:01:07,895 --> 00:01:10,693
He asked me about Professor Joo.

20
00:01:13,518 --> 00:01:15,402
He said...

21
00:01:17,696 --> 00:01:22,180
that Professor Joo Hye Sung is
connected to my father's death.

22
00:01:23,380 --> 00:01:24,865
Bong Pal.

23
00:01:30,050 --> 00:01:33,038
Professor Joo Hye Sung is not human.

24
00:01:33,039 --> 00:01:39,096
Accurately speaking,
an evil spirit is inside his body.

25
00:01:39,792 --> 00:01:41,463
An evil spirit.

26
00:01:44,497 --> 00:01:47,410
The same evil spirit that came
out of your body a long time ago.

27
00:02:05,381 --> 00:02:06,983
What's wrong?

28
00:02:07,441 --> 00:02:10,326
I just feel dizzy.

29
00:02:11,631 --> 00:02:14,578
I will use the bathroom.

30
00:02:24,158 --> 00:02:28,384
That evil spirit has been
hanging around you and Hyun Ji.

31
00:02:29,751 --> 00:02:31,288
Hyun Ji?

32
00:02:31,289 --> 00:02:33,729
She has what he whats.

33
00:02:33,731 --> 00:02:36,231
- What is it?
- A weapon.

34
00:02:37,203 --> 00:02:41,015
The weapon that was used to get rid
of that evil spirit in your body.

35
00:02:41,949 --> 00:02:43,656
Do you remember?

36
00:02:46,086 --> 00:02:49,422
That weapon... was broken.

37
00:02:49,423 --> 00:02:51,055
It was.

38
00:02:51,609 --> 00:02:57,162
But it seems like your mother
made another one for your sake.

39
00:02:57,163 --> 00:03:01,616
Bong Pal, your mother
had a very strong soul.

40
00:03:01,618 --> 00:03:04,084
So much so that it shocked
even the shamanesses.

41
00:03:04,822 --> 00:03:06,591
- My mom?
- Yes.

42
00:03:07,481 --> 00:03:11,523
Some say it was a gift, but to
your mother, it was a nightmare.

43
00:03:11,524 --> 00:03:14,029
She decided not to accept the gift.

44
00:03:14,030 --> 00:03:17,398
- Why?
- She was pregnant with you back then.

45
00:03:18,467 --> 00:03:21,513
She wanted to raise
you like a normal person.

46
00:03:22,023 --> 00:03:24,245
The gods were angry.

47
00:03:24,246 --> 00:03:26,161
Among them, the evil
spirit was also there.

48
00:03:26,162 --> 00:03:29,942
- It was rejected, so...
- It tried to posess my body.

49
00:03:30,314 --> 00:03:31,796
Yes.

50
00:03:31,797 --> 00:03:34,321
That's when the misfortune all started.

51
00:03:34,322 --> 00:03:36,807
I eventually got rid of
the evil spirit from your body

52
00:03:36,808 --> 00:03:41,272
but your mother made another weapon
just in case it tried to harm you again.

53
00:03:41,274 --> 00:03:45,209
- With all her energy.
- My mom.

54
00:03:46,892 --> 00:03:50,769
But... how did Hyun Ji...

55
00:03:50,771 --> 00:03:53,778
Your father came looking
for that weapon at the temple

56
00:03:53,779 --> 00:03:56,776
and it seemed like
Hyun Ji also came to pray.

57
00:03:56,778 --> 00:03:59,715
Your father was being chased
by the evil spirit at that time.

58
00:03:59,716 --> 00:04:03,810
He was frantic and asked her
to look after the weapon.

59
00:04:03,812 --> 00:04:06,639
But she got into a car accident
on her way to give it to you.

60
00:04:07,118 --> 00:04:10,480
The weapon got lost in the middle

61
00:04:10,481 --> 00:04:14,577
and your father was back
to being chased again.

62
00:04:15,764 --> 00:04:19,692
My mom, my father, and Hyun Ji.

63
00:04:21,911 --> 00:04:25,340
- They all got hurt because of me.
- No. No, Bong Pal.

64
00:04:25,341 --> 00:04:29,257
It's not because of you.
It's because of that evil spirit.

65
00:04:30,757 --> 00:04:32,593
Where are you going?

66
00:04:36,262 --> 00:04:38,418
I need to go to Hyun Ji.

67
00:05:00,026 --> 00:05:02,296
<i>[My Love Bong Bong]</i>

68
00:05:11,317 --> 00:05:14,096
<i>The person you are trying
to call cannot be reached...</i>

69
00:06:17,211 --> 00:06:21,733
Hyun Ji... are you okay?

70
00:06:24,526 --> 00:06:25,526
Hyun Ji...

71
00:06:25,527 --> 00:06:27,803
Where is the item
you're supposed to give me?

72
00:06:32,385 --> 00:06:34,166
<i>Young lady.</i>

73
00:06:51,599 --> 00:06:54,942
Professor Joo, you're...

74
00:06:57,865 --> 00:06:59,865
You remember.

75
00:07:02,358 --> 00:07:05,468
It's best to bury bad memories.

76
00:07:26,154 --> 00:07:27,983
<i>Excuse me!</i>

77
00:07:34,351 --> 00:07:36,415
What brings you here?

78
00:07:36,926 --> 00:07:39,327
- Is Hyun Ji home?
- She's not home right now...

79
00:07:39,329 --> 00:07:42,778
- Didn't she go out to meet you?
- She hasn't returned yet?

80
00:07:42,779 --> 00:07:44,589
Is there something going on?

81
00:07:46,363 --> 00:07:48,137
No.

82
00:07:49,976 --> 00:07:52,728
Young man! Young man!

83
00:08:11,685 --> 00:08:13,761
- Never mind.
- Seriously!

84
00:08:13,762 --> 00:08:15,815
It's not him.

85
00:08:15,816 --> 00:08:19,723
You need to move more quickly.
You're not careful at all.

86
00:08:19,724 --> 00:08:21,916
Don't you know
the fundamentals of a stakeout?

87
00:08:21,917 --> 00:08:24,790
Is this necessary, huh?
What are we? Detectives?

88
00:08:24,791 --> 00:08:27,318
Didn't the monk tell
us to watch over him?

89
00:08:27,319 --> 00:08:29,804
And when did you ever listen so well?

90
00:08:29,805 --> 00:08:34,342
You know, you're not as
bright as I thought you were.

91
00:08:34,343 --> 00:08:37,531
We need to score points with the
monk if we want to work with Bong Pal.

92
00:08:37,532 --> 00:08:40,035
Why do you think he asked us to do this?

93
00:08:40,036 --> 00:08:42,398
It's because this person
is dangerous to Bong Pal!

94
00:08:42,400 --> 00:08:45,121
He must've seen us as very
strong and reliable colleagues

95
00:08:45,122 --> 00:08:47,632
to give us something
so important like this to take on.

96
00:08:48,204 --> 00:08:49,895
- Ah.
- Ah?

97
00:08:49,896 --> 00:08:51,315
I get it.

98
00:08:52,959 --> 00:08:54,518
Isn't that him?

99
00:08:55,659 --> 00:08:56,659
Right?

100
00:08:57,118 --> 00:08:59,258
He's... he's loading
something in the car.

101
00:09:03,734 --> 00:09:05,271
Let's follow him.

102
00:09:06,770 --> 00:09:08,618
Let's go.

103
00:09:27,223 --> 00:09:29,738
Hyun Ji! Hyun Ji!

104
00:09:51,859 --> 00:09:53,243
Hyun Ji.

105
00:10:56,497 --> 00:11:01,136
Right, the alarm went
off 30 minutes ago.

106
00:11:06,470 --> 00:11:09,878
We started the search and rescue.
We'll catch him soon.

107
00:11:09,879 --> 00:11:11,474
Don't worry so much.

108
00:11:11,475 --> 00:11:14,354
I'll call you right away
if something comes up, so go home.

109
00:11:15,032 --> 00:11:17,575
Bong Pal! Bong Pal!

110
00:11:17,576 --> 00:11:19,395
Goodness, hello.

111
00:11:24,633 --> 00:11:26,859
What's going on here?

112
00:11:26,861 --> 00:11:28,787
That bastard...

113
00:11:30,771 --> 00:11:33,028
took Hyun Ji.

114
00:11:35,954 --> 00:11:37,554
Goodness.

115
00:11:37,555 --> 00:11:39,862
Oh my dear Buddha.

116
00:11:41,052 --> 00:11:42,738
Oh goodness.

117
00:11:42,739 --> 00:11:44,954
Is your daughter Kim Hyun Ji?

118
00:11:44,955 --> 00:11:47,855
She is currently missing.

119
00:11:47,857 --> 00:11:51,007
- Are you okay?
- Are you okay?

120
00:11:58,108 --> 00:12:00,729
- Senior.
- Did you talk to the employees?

121
00:12:00,730 --> 00:12:04,472
Yes, I think he planned this all along.
He gave them time off a few days ago.

122
00:12:04,473 --> 00:12:08,232
And they know nothing about Professor
Joo. His cell phone is also turned off.

123
00:12:08,233 --> 00:12:10,472
It'll be hard to track him.
What should we do?

124
00:12:10,473 --> 00:12:13,060
Compare Park Ji Hoon's
and Professor Joo Hye Sung's DNA.

125
00:12:13,061 --> 00:12:15,355
Tell everyone this is an emergency.
He hasn't gone too far.

126
00:12:15,357 --> 00:12:16,767
Yes, sir.

127
00:12:16,768 --> 00:12:18,878
Who went to his mother's house?

128
00:12:18,879 --> 00:12:22,162
The local precinct agreed to cooperate.
They're probably there by now.

129
00:13:00,959 --> 00:13:03,182
Hey, be quiet.

130
00:13:27,182 --> 00:13:30,069
- What in the world is he doing?
- I don't know.

131
00:13:30,813 --> 00:13:33,367
He seems very suspicious.

132
00:13:33,368 --> 00:13:37,334
Seeing how he's changing
cars this late at night.

133
00:13:37,335 --> 00:13:40,883
It's always the good-looking
ones that surprise you.

134
00:13:40,885 --> 00:13:43,309
He's taking something out.
He's taking something out.

135
00:13:51,339 --> 00:13:53,575
Hyun... Hyun Ji!

136
00:14:39,720 --> 00:14:42,059
What are you doing over there?

137
00:14:46,204 --> 00:14:48,037
Nothing.

138
00:14:58,282 --> 00:15:00,205
Is this your car?

139
00:15:03,316 --> 00:15:05,318
Yes. Why do you ask?

140
00:15:05,319 --> 00:15:09,324
You can't park here for
two days without a contact number.

141
00:15:09,325 --> 00:15:10,843
Hurry up and remove it.

142
00:15:14,209 --> 00:15:17,703
I got so many complaints
because of this car.

143
00:15:19,456 --> 00:15:22,757
- Just go.
- Hurry up and remove it.

144
00:15:34,747 --> 00:15:36,757
Excuse me.

145
00:15:39,315 --> 00:15:40,729
Yes?

146
00:15:47,671 --> 00:15:50,613
I told you to just go.
Why do you keep bothering me?

147
00:17:23,882 --> 00:17:26,730
- What the heck?
- What are you doing? Let's go!

148
00:17:26,731 --> 00:17:29,394
What the hell!
This worthless piece of crap!

149
00:17:45,179 --> 00:17:47,257
Bong Pal, don't worry so much.

150
00:17:47,785 --> 00:17:50,539
That punk won't be able to touch
Hyun Ji until he finds the weapon.

151
00:17:53,630 --> 00:17:58,426
Why... is he so
obsessed with the weapon?

152
00:18:00,131 --> 00:18:03,419
He can completely possess you
once that weapon disappears.

153
00:18:03,420 --> 00:18:04,789
Me?

154
00:18:04,790 --> 00:18:07,755
It's because your mother's energy
has been handed down to you.

155
00:18:07,756 --> 00:18:11,433
That's why he's been lurking
around you for 10 years.

156
00:18:24,805 --> 00:18:26,622
Yes, Senior Il Rang.

157
00:18:29,036 --> 00:18:30,914
What did you say?

158
00:18:33,213 --> 00:18:34,801
Where?

159
00:18:34,802 --> 00:18:36,385
Okay.

160
00:18:52,981 --> 00:18:57,449
Senior, that camera is useless.
It's been broken for a long time.

161
00:18:57,450 --> 00:18:59,985
The students did not get
a good look at the license plate.

162
00:18:59,986 --> 00:19:01,851
Tracking him won't be easy.

163
00:19:01,852 --> 00:19:05,218
If he planned this as well, then he
probably has an escape location too.

164
00:19:05,219 --> 00:19:08,653
Call all of Professor Joo's contacts
and check all the streets and cameras.

165
00:19:08,654 --> 00:19:10,141
Yes, sir.

166
00:19:12,631 --> 00:19:14,090
Bong Pal.

167
00:19:14,689 --> 00:19:17,201
Let's go back home. You guys too.

168
00:19:17,202 --> 00:19:19,297
- Yes, sir.
- Yes, sir.

169
00:19:20,067 --> 00:19:21,995
- Bong Pal.
- Go without me.

170
00:19:21,996 --> 00:19:23,270
What about you?

171
00:19:24,120 --> 00:19:25,389
I have to find Hyun Ji.

172
00:19:25,390 --> 00:19:27,895
By what means?
How are you going to find her?

173
00:19:27,897 --> 00:19:29,652
I can't sit around and do nothing.

174
00:19:29,653 --> 00:19:32,383
He's after you.
You're the one who is in danger.

175
00:19:32,384 --> 00:19:34,210
Then what about Hyun Ji?

176
00:19:34,962 --> 00:19:38,190
- I'll find her.
- This happened because of me.

177
00:19:38,921 --> 00:19:41,149
- I have to do something.
- Bong Pal.

178
00:19:41,150 --> 00:19:45,969
We'll stay with Bong Pal.
Don't worry.

179
00:20:03,296 --> 00:20:04,795
<i>[Joo Hye Sung]</i>

180
00:20:04,796 --> 00:20:07,073
<i>[Kim Hyun Ji]</i>

181
00:20:13,167 --> 00:20:14,774
- Senior.
- Yeah?

182
00:20:14,775 --> 00:20:17,253
- Did you find something?
- Yes, I checked the cameras

183
00:20:17,254 --> 00:20:20,116
and there were several shots
captured of Professor Joo's car.

184
00:20:20,117 --> 00:20:21,725
We're in the process of
searching for it right now.

185
00:20:21,726 --> 00:20:23,770
We should be able to get its
location in a couple of hours.

186
00:20:23,772 --> 00:20:25,246
Let me know as soon
as you get the results.

187
00:20:25,247 --> 00:20:26,536
Yes, sir.

188
00:20:26,537 --> 00:20:27,682
Hello.

189
00:20:27,683 --> 00:20:29,660
Monk, you can't stay here.

190
00:20:29,661 --> 00:20:33,217
Go over there and we'll let you know if
we get any information on Professor Joo.

191
00:20:33,218 --> 00:20:35,565
- She's here.
- Hello.

192
00:20:35,997 --> 00:20:37,488
Please sit over here.

193
00:20:45,193 --> 00:20:47,298
Just a moment please.

194
00:20:47,299 --> 00:20:49,306
Who is that person?

195
00:20:49,307 --> 00:20:51,824
Professor Joo Hye Sung's mother.

196
00:20:58,524 --> 00:21:00,182
Let's go.

197
00:21:04,360 --> 00:21:09,100
Did Hye Sung kill all those people?

198
00:21:11,824 --> 00:21:14,003
We're still investigating.

199
00:21:14,603 --> 00:21:16,475
It's true.

200
00:21:17,346 --> 00:21:19,930
I heard what happened on the way.

201
00:21:21,892 --> 00:21:23,693
How...

202
00:21:25,778 --> 00:21:27,710
I know this is very difficult for you

203
00:21:27,711 --> 00:21:30,791
but please help us so the investigation
can move along as quickly as possible.

204
00:21:32,756 --> 00:21:37,224
I'm not sure if I can
be of any help to you.

205
00:21:43,315 --> 00:21:48,534
What was the last thing you
talked about with your son?

206
00:21:48,535 --> 00:21:50,652
I don't know.

207
00:21:52,963 --> 00:21:54,697
I understand.

208
00:22:04,629 --> 00:22:06,574
- Senior Il Rang.
- Yeah?

209
00:22:06,575 --> 00:22:09,126
Did you say
Professor Joo headed to the right?

210
00:22:09,128 --> 00:22:11,586
Yes, he went to the right.

211
00:22:13,397 --> 00:22:15,968
Professor Joo is looking for
the item that Hyun Ji has.

212
00:22:15,969 --> 00:22:20,045
And Hyun Ji got into a car accident
trying to give me that item.

213
00:22:21,581 --> 00:22:25,371
Then that item is probably in Seoul.

214
00:22:25,372 --> 00:22:26,737
That's right.

215
00:22:26,738 --> 00:22:28,904
Why did he come all the way here then?

216
00:22:28,905 --> 00:22:30,926
I think he's trying to lure us.

217
00:22:30,927 --> 00:22:33,280
He probably went to Seoul.

218
00:23:00,727 --> 00:23:02,706
- You fixed it?
- That's right.

219
00:23:02,707 --> 00:23:05,338
Who am I? We can use
this for 10 more years.

220
00:23:05,340 --> 00:23:09,034
What should we do? Where do we go?
Should we just head straight to Seoul?

221
00:23:10,448 --> 00:23:14,263
The detectives said that
he was prepared to run away, right?

222
00:23:14,264 --> 00:23:15,373
Yeah, why?

223
00:23:15,374 --> 00:23:18,625
- Don't you think he scoped out the area?
- Probably.

224
00:23:18,626 --> 00:23:20,438
Then...

225
00:23:25,431 --> 00:23:27,842
Hey, where are you going?

226
00:23:36,651 --> 00:23:38,129
Hey.

227
00:23:38,130 --> 00:23:40,828
What are you doing?

228
00:23:40,830 --> 00:23:42,882
Are you searching for the destination?

229
00:23:42,883 --> 00:23:45,971
I'm sure he already erased it. I swear,
you're slowly turning into an idiot.

230
00:23:45,972 --> 00:23:48,112
Of course he erased it.

231
00:23:48,113 --> 00:23:51,351
The destination can be deleted from the
screen, but it's still on the hard drive.

232
00:23:51,352 --> 00:23:53,912
If I do this right, I can even
find out where he went a week ago.

233
00:23:58,630 --> 00:24:01,633
It worked! I found it! I found it!

234
00:24:02,758 --> 00:24:05,489
- You found it.
- It's the coordinates.

235
00:24:05,490 --> 00:24:07,461
Somewhere in Gyungdo or Euido.

236
00:24:07,462 --> 00:24:11,104
If I input the coordinates here...

237
00:24:11,587 --> 00:24:12,660
Ta-da.

238
00:24:17,123 --> 00:24:19,720
Just who are you?

239
00:24:21,193 --> 00:24:25,173
Bong Pal, come quickly!

240
00:24:26,158 --> 00:24:28,597
These are the current destinations.

241
00:24:29,766 --> 00:24:33,257
That's the school
and that's the hospital.

242
00:24:34,125 --> 00:24:35,451
Where is that?

243
00:24:35,452 --> 00:24:38,678
I think this is Guil-dong.
I'll look for it.

244
00:24:44,196 --> 00:24:48,444
Ah, Guil-dong is where
that empty warehouse is!

245
00:24:49,605 --> 00:24:52,417
It's the perfect place to hide a body.

246
00:24:52,418 --> 00:24:54,332
Do you think Hyun Ji is there?

247
00:24:54,333 --> 00:24:55,878
Let's go for now.

248
00:25:34,527 --> 00:25:36,056
Thank you.

249
00:25:38,186 --> 00:25:42,191
I'm sorry that I couldn't help you.

250
00:25:42,192 --> 00:25:45,511
Not at all. I'm thankful
that you even came.

251
00:25:45,513 --> 00:25:47,441
I'll escort you home.

252
00:25:47,851 --> 00:25:51,880
No, I'll just go by myself.

253
00:26:11,013 --> 00:26:13,035
Are you okay?

254
00:26:14,029 --> 00:26:17,279
- Yes.
- Can we talk?

255
00:26:21,043 --> 00:26:22,690
I'm sorry.

256
00:26:23,327 --> 00:26:25,321
I'm sorry.

257
00:26:25,322 --> 00:26:29,471
- I have nothing to say.
- No, it's fine.

258
00:26:30,023 --> 00:26:32,356
I'm not doing this so I can
get an apology out of you.

259
00:26:32,357 --> 00:26:33,891
Then...

260
00:26:33,892 --> 00:26:37,987
There are some questions
I want to ask about your son.

261
00:26:37,988 --> 00:26:40,943
I told the detective everything.

262
00:26:42,229 --> 00:26:45,234
I won't know anything
even if you ask me.

263
00:26:45,235 --> 00:26:48,804
You can tell me what you
couldn't say to the detective.

264
00:26:50,563 --> 00:26:52,586
When did it start?

265
00:26:53,196 --> 00:26:55,273
When did your son change?

266
00:26:59,257 --> 00:27:02,770
I know there is an evil
spirit inside your son's body.

267
00:27:03,198 --> 00:27:05,056
When did it start?

268
00:27:09,632 --> 00:27:15,629
Hye Sung is who he is because of me.

269
00:27:15,630 --> 00:27:19,172
<i>You brat! You're just like your mother!</i>

270
00:27:19,173 --> 00:27:21,998
I told you not to look at me like that!

271
00:27:21,999 --> 00:27:27,433
- Don't look at me like that, punk!
- Hye Sung, run! Stop!

272
00:27:27,434 --> 00:27:29,568
Please stop!

273
00:27:40,620 --> 00:27:42,803
<i>Why are you crying?</i>

274
00:27:42,804 --> 00:27:44,979
<i>Should I help?</i>

275
00:27:45,687 --> 00:27:48,542
Leave me alone! Go away!

276
00:27:48,543 --> 00:27:50,640
Go away!

277
00:27:50,641 --> 00:27:54,792
<i>Should I save you from hell?
How about it?</i>

278
00:27:55,477 --> 00:27:58,278
<i>You have to save your mom too.
Don't you think so?</i>

279
00:28:02,315 --> 00:28:04,145
Mom?

280
00:28:07,625 --> 00:28:09,325
Mom.

281
00:28:23,027 --> 00:28:25,867
<i>Kill him, kill him, kill him!</i>

282
00:28:25,869 --> 00:28:27,682
<i>Kill him!</i>

283
00:28:34,951 --> 00:28:37,641
Hye Sung... Hye Sung!

284
00:28:38,979 --> 00:28:42,566
<i>Someone help!
Someone has fallen!</i>

285
00:28:46,128 --> 00:28:49,225
I should've taken better care of him.

286
00:28:49,226 --> 00:28:53,717
He wouldn't be what he
is now if I had done that.

287
00:29:08,251 --> 00:29:10,432
I'm home.

288
00:29:12,955 --> 00:29:14,847
Mom.

289
00:29:16,186 --> 00:29:18,142
Mom.

290
00:29:18,657 --> 00:29:20,230
Mom!

291
00:29:20,231 --> 00:29:23,105
<i>- Can you give me the number and address?
- Gosh, it's really hot today.</i>

292
00:29:23,106 --> 00:29:26,084
Why are there so many reports
on a hot day like this?

293
00:29:26,085 --> 00:29:29,956
- Yes, I understand.
- So many reports.

294
00:29:29,957 --> 00:29:31,754
No kidding.

295
00:29:32,671 --> 00:29:34,565
By the way, who is that kid?

296
00:29:34,566 --> 00:29:37,145
He came back from school
and his mom moved without him.

297
00:29:37,147 --> 00:29:39,020
- Did you call her?
- I did.

298
00:29:39,022 --> 00:29:40,815
She's coming.

299
00:29:53,124 --> 00:29:57,669
Ever since that day,
Hye Sung started to change.

300
00:29:57,670 --> 00:30:00,825
He went to live in
the dorms at his middle school.

301
00:30:02,492 --> 00:30:05,958
And he only called me once in a while.

302
00:30:10,926 --> 00:30:14,045
It's all my fault.

303
00:30:14,046 --> 00:30:16,855
I made him that way.

304
00:30:18,936 --> 00:30:21,405
I'm to blame.

305
00:31:14,256 --> 00:31:16,535
It's for one day.

306
00:31:16,536 --> 00:31:19,104
Why did Mom pack this much?

307
00:31:19,105 --> 00:31:21,266
It will be hard to carry this around.

308
00:31:45,756 --> 00:31:47,585
Now you're up.

309
00:31:50,805 --> 00:31:52,996
I've been waiting for so long.

310
00:31:54,236 --> 00:31:56,176
Why are you doing this?

311
00:32:01,696 --> 00:32:04,036
If you have your memory back,
you would know.

312
00:32:09,305 --> 00:32:11,496
Don't blame me.

313
00:32:13,655 --> 00:32:17,845
You shuld blame the man who
gave you the item and his son.

314
00:32:18,605 --> 00:32:21,236
They put you in hell
for the last five years.

315
00:32:40,496 --> 00:32:43,755
If you hadn't gotten that item

316
00:32:43,756 --> 00:32:46,486
we wouldn't have met this way.

317
00:32:47,986 --> 00:32:49,615
Don't you think so?

318
00:32:57,905 --> 00:32:59,796
Are you scared of me?

319
00:33:02,996 --> 00:33:07,046
No, I'm not scared of you at all.

320
00:33:07,565 --> 00:33:09,176
Good.

321
00:33:16,365 --> 00:33:19,635
Do you know what's scarier
than just feeling scared?

322
00:33:22,605 --> 00:33:25,105
It's being found out
that you're actually scared.

323
00:33:34,466 --> 00:33:38,796
See? You're already shaking.

324
00:33:41,705 --> 00:33:43,573
What are you?

325
00:33:44,285 --> 00:33:46,085
You're not human, are you?

326
00:33:47,355 --> 00:33:51,446
Not sure. What am I?

327
00:33:52,765 --> 00:33:55,145
Am I human or not?

328
00:33:58,136 --> 00:34:00,675
Depending on your answer

329
00:34:00,676 --> 00:34:03,426
I will be human or not.

330
00:34:11,306 --> 00:34:17,205
Where is... that item?

331
00:34:19,256 --> 00:34:22,025
<i>That is very important to Bong Pal.</i>

332
00:34:22,026 --> 00:34:27,335
If you remember,
please tell me, okay?

333
00:34:31,546 --> 00:34:33,065
What is it?

334
00:34:34,006 --> 00:34:35,895
You still don't remember?

335
00:34:37,676 --> 00:34:40,076
Then I will help you remember.

336
00:34:49,616 --> 00:34:52,124
<i>- Hello?</i>
- Mom.

337
00:34:52,125 --> 00:34:54,724
<i>Is this Hyun Ji? Hyun Ji!</i>

338
00:34:54,725 --> 00:34:57,085
- Mom!
<i>- Hello? Hyun Ji!</i>

339
00:34:58,866 --> 00:35:00,834
What do you think?

340
00:35:00,835 --> 00:35:02,558
Do you remember now?

341
00:35:07,176 --> 00:35:09,556
You'd better remember.

342
00:35:10,596 --> 00:35:12,755
If not

343
00:35:12,756 --> 00:35:16,136
your loved ones will die one by one.

344
00:35:38,386 --> 00:35:40,185
Here!

345
00:35:45,116 --> 00:35:46,757
Hyun Ji.

346
00:36:03,033 --> 00:36:04,136
Hyun Ji!

347
00:36:39,835 --> 00:36:42,375
- Hyun Ji!
- Kim Hyun Ji!

348
00:36:44,556 --> 00:36:46,446
- Hyun Ji!
- Kim Hyun Ji!

349
00:37:06,654 --> 00:37:08,392
<i>[Myungsung University]</i>

350
00:37:41,636 --> 00:37:42,722
As a boy, he probably wanted
to protect himself from

351
00:37:42,723 --> 00:37:47,355
the guilt from killing his dad
and feeling distant from his mom.

352
00:37:48,136 --> 00:37:52,414
His hatred made
the evil sprit come to him.

353
00:37:52,415 --> 00:37:55,295
The spirit used the boy's open wound.

354
00:37:55,296 --> 00:37:58,345
Do you think that professor
might still have his humanity?

355
00:37:58,346 --> 00:38:00,604
Not at all. They're one body now.

356
00:38:00,605 --> 00:38:01,817
Then what should we do now?

357
00:38:05,446 --> 00:38:06,965
Where is the stuff I gave you?

358
00:38:06,966 --> 00:38:08,995
Ah, hold on.

359
00:38:16,446 --> 00:38:20,705
If I think about how hard it was to
make this all the way out here...

360
00:38:25,705 --> 00:38:28,935
Wow, it's great work.

361
00:38:29,386 --> 00:38:31,296
Thank you.

362
00:38:31,906 --> 00:38:35,255
I really hope
this has the power of divine spirits.

363
00:38:35,256 --> 00:38:38,305
When you're not equipped
to borrow the power of divine spirits

364
00:38:38,306 --> 00:38:39,954
you won't be able to walk.

365
00:38:39,955 --> 00:38:42,006
Do you want to
not be able to walk forever?

366
00:38:42,966 --> 00:38:45,815
You nag so much
that I might just do that.

367
00:38:48,817 --> 00:38:51,055
What is it that you're going to do?

368
00:38:51,056 --> 00:38:53,135
I have to finish
what I couldn't 18 years ago.

369
00:38:53,136 --> 00:38:54,236
What?

370
00:38:55,716 --> 00:38:57,334
I will finish him.

371
00:38:57,335 --> 00:38:59,334
How are you going to do
that without weapons?

372
00:38:59,335 --> 00:39:01,104
Only you will end up dead.

373
00:39:01,105 --> 00:39:03,075
I will have the power of divine spirits.

374
00:39:03,076 --> 00:39:05,906
If I don't finish him,
Bong Pal will be in danger.

375
00:39:06,125 --> 00:39:08,175
Bye now.

376
00:39:08,176 --> 00:39:11,165
I hope you get married again.
I will go now.

377
00:39:28,335 --> 00:39:30,715
You'd better not do anything stupid

378
00:39:30,716 --> 00:39:32,946
if you care about your mother.

379
00:39:38,156 --> 00:39:40,046
Get out.

380
00:40:19,826 --> 00:40:23,546
Bong Pal, is it true
that the man is here?

381
00:40:23,547 --> 00:40:24,547
Yes.

382
00:40:26,640 --> 00:40:30,104
- Did you call the police?
- They should be on their way.

383
00:40:30,105 --> 00:40:32,534
Bong Pal, I will go in by myself.

384
00:40:32,535 --> 00:40:34,954
- You stay out here.
- No, I won't.

385
00:40:34,955 --> 00:40:37,124
It happened because of me.

386
00:40:37,125 --> 00:40:39,326
I will finish it.

387
00:40:40,676 --> 00:40:42,685
You two stay here.

388
00:40:55,585 --> 00:40:59,116
Excuse me, sir.
The subway has stopped running.

389
00:41:01,105 --> 00:41:04,025
Sir, I said we're closed.
Please leave.

390
00:41:04,026 --> 00:41:06,426
There are buses outside.
Take the bus.

391
00:41:54,765 --> 00:41:57,625
It hasn't been that long
since he was here.

392
00:42:10,046 --> 00:42:12,035
Bong Pal!

393
00:42:18,165 --> 00:42:20,116
Hyun Ji!

394
00:42:23,966 --> 00:42:25,656
Hyun Ji!

395
00:42:45,225 --> 00:42:46,560
Hyun Ji!

396
00:42:51,265 --> 00:42:53,236
Kim Hyun Ji!

397
00:43:14,725 --> 00:43:16,375
Hyun Ji!

398
00:43:29,355 --> 00:43:31,216
Hyun Ji!

399
00:44:06,326 --> 00:44:08,056
Bong Pal.

400
00:44:08,685 --> 00:44:11,064
- You didn't see her?
- No.

401
00:44:11,065 --> 00:44:13,365
If she's nowhere to be seen...

402
00:44:13,366 --> 00:44:15,366
Is this really the right place?

403
00:44:20,765 --> 00:44:22,585
This is the right place.

404
00:44:23,346 --> 00:44:27,454
Then maybe she's mistaken about
the location since it's been five years.

405
00:44:27,455 --> 00:44:29,314
If not, it's a mystery.

406
00:44:29,315 --> 00:44:31,155
I can't believe
she vanished without a trace.

407
00:44:31,156 --> 00:44:33,535
Five years ago? Five years...

408
00:44:34,558 --> 00:44:37,265
If it happened five years ago,
we're at the wrong place.

409
00:44:38,276 --> 00:44:41,284
If Hyun Ji came to
this station five years ago

410
00:44:41,285 --> 00:44:42,994
this wasn't the same station then.

411
00:44:42,995 --> 00:44:45,985
What do you mean
this wasn't the same station?

412
00:44:45,986 --> 00:44:48,295
It means that
there was a fire five years ago.

413
00:44:48,296 --> 00:44:50,764
They closed the station then
and rebuilt this station instead.

414
00:44:50,765 --> 00:44:52,545
He's right. There was a fire...

415
00:44:52,546 --> 00:44:55,136
- Then where is the original location?
- It's that...

416
00:44:56,285 --> 00:44:58,585
It may be near Exit Two.

417
00:46:29,366 --> 00:46:31,556
This feels very spooky.

418
00:46:33,546 --> 00:46:35,575
Wait. We should wait for the police.

419
00:46:35,576 --> 00:46:37,764
- That's right.
- Isn't it?

420
00:46:37,765 --> 00:46:39,585
True. You're right.

421
00:46:40,205 --> 00:46:42,656
Bong Pal, be careful.

422
00:46:43,343 --> 00:46:45,105
You too, Monk Myung Chul.

423
00:48:38,616 --> 00:48:41,035
You're no longer needed.

424
00:48:41,763 --> 00:48:43,455
I will let you rest now.

425
00:48:48,267 --> 00:48:49,716
Hyun Ji!

426
00:49:17,935 --> 00:49:20,405
I was going to come find you.

427
00:49:20,406 --> 00:49:22,915
Thank you for coming to me first.

428
00:50:07,915 --> 00:50:11,386
Do you know how long
I've been waiting for this moment?

429
00:50:13,975 --> 00:50:16,886
If I hadn't been
driven out of your body...

430
00:50:19,866 --> 00:50:22,466
this wouldn't have happened.

431
00:50:30,625 --> 00:50:32,986
Wait a little bit.

432
00:50:34,526 --> 00:50:37,296
You and I will become one body.

433
00:50:39,285 --> 00:50:41,256
No, you can't!

434
00:50:51,366 --> 00:50:54,245
Bong Pal!

435
00:51:01,035 --> 00:51:04,546
You evil spirit,
you're up to no good!

436
00:51:20,806 --> 00:51:24,011
Bong Pal, Hyun Ji.
Are you two okay?

437
00:51:26,426 --> 00:51:30,886
Hyun Ji, get Bong Pal out of here. Hurry!

438
00:51:32,875 --> 00:51:35,236
I won't let you do that.

439
00:51:49,225 --> 00:51:51,866
Will you really kill me?

440
00:51:52,966 --> 00:51:55,366
Then you're killing this man too.

441
00:51:56,815 --> 00:51:59,205
It means that you're taking a life.

442
00:51:59,306 --> 00:52:01,164
Sure.

443
00:52:01,165 --> 00:52:05,736
If I can end you, I shall do that.

444
00:52:07,725 --> 00:52:10,046
Don't kill yourself over this.

445
00:52:11,665 --> 00:52:17,276
I will end you today, you evil spirit.

446
00:52:29,205 --> 00:52:30,866
You bastard!

447
00:52:49,935 --> 00:52:53,355
This is why I told you
not to kill yourself over this.

448
00:52:55,716 --> 00:52:58,185
Now this is really over!

449
00:53:14,596 --> 00:53:16,756
You bastard!

450
00:53:29,705 --> 00:53:32,296
Monk Myung Chul. Are you okay?

451
00:53:39,395 --> 00:53:41,435
Hyun Ji.

452
00:53:41,955 --> 00:53:45,156
The weapon... the weapon...

453
00:54:33,056 --> 00:54:35,765
You stop that!

454
00:55:07,846 --> 00:55:09,596
Monk Myung Chul.

455
00:55:10,915 --> 00:55:13,576
It's done. It's done.

456
00:55:30,546 --> 00:55:33,866
Bong Pal. Bong Pal!

457
00:55:37,366 --> 00:55:39,385
Hyun Ji, find the weapon!

458
00:55:39,386 --> 00:55:42,716
It's the only thing
that will stop him, Hyun Ji.

459
00:55:52,306 --> 00:55:53,926
Monk Myung Chul!

460
00:56:14,850 --> 00:56:17,116
<i>[Let’s Fight Ghost]</i>

461
00:56:17,696 --> 00:56:18,994
<i>Is Hyun Ji leaving?</i>

462
00:56:18,995 --> 00:56:21,025
<i>Why do you exorcise, Monk Myung Chul?</i>

463
00:56:21,026 --> 00:56:24,445
<i>Do you know why I was against
you exorcising when I found out?</i>

464
00:56:24,446 --> 00:56:26,854
<i>It was because I could see
hatred in your eyes then.</i>

465
00:56:26,855 --> 00:56:29,465
<i>- You remember these people, right?
- I killed them.</i>

466
00:56:29,466 --> 00:56:32,175
<i>You can't see the world right
when you have so much anger in you.</i>

467
00:56:32,176 --> 00:56:33,845
<i>What is this?</i>

468
00:56:33,846 --> 00:56:36,355
<i>She may be a con artist.</i>

469
00:56:37,013 --> 00:56:39,084
<i>Not all ghosts are bad.</i>

470
00:56:39,085 --> 00:56:40,945
<i>Perhaps they just want your sympathy.</i>

471
00:56:40,946 --> 00:56:43,195
<i>I said get lost!</i>

472
00:56:43,196 --> 00:56:45,195
<i>Can you really do the job?</i>

473
00:56:45,196 --> 00:56:46,854
<i>It's up to you.</i>

474
00:56:46,855 --> 00:56:48,255
<i>Isn't this place
a restaurant for rice soup?</i>

475
00:56:48,256 --> 00:56:51,615
<i>I'm Goo Dae Hyung, a sophomore
in the Physical Education department.</i>

476
00:56:51,616 --> 00:56:56,155
<i>Making others happy makes you happiest.</i>

477
00:56:56,156 --> 00:56:57,635
<i>[My love Bong Bong]</i>

478
00:56:57,636 --> 00:57:01,414
<i>Hello, seniors.
I'm a freshman, Kim Hyun Ji.</i>

479
00:57:01,415 --> 00:57:03,216
<i>I'd appreciate your guidance.</i>

