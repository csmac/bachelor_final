1
00:00:00,440 --> 00:00:02,032
All this time.

2
00:00:02,120 --> 00:00:03,109
We won't have moved.

3
00:00:09,080 --> 00:00:10,638
(BIRDS FLUTTERING)

4
00:00:11,920 --> 00:00:13,148
SOLDIER: You got away
from the British?

5
00:00:14,480 --> 00:00:16,516
- How many of you?
- Just me.

6
00:00:16,600 --> 00:00:17,589
(GUNSHOT)

7
00:00:18,360 --> 00:00:20,920
- Leave them.
- What?

8
00:00:21,200 --> 00:00:24,237
- (GROANS) Get off!
- You'll get us both killed!

9
00:00:24,320 --> 00:00:25,309
(DEREK GRUNTS)

10
00:00:28,480 --> 00:00:31,278
How do we know it will end?

11
00:00:31,360 --> 00:00:33,669
It will. It has to.

12
00:00:34,400 --> 00:00:36,072
Maybe when there's no one left
to fight any more.

13
00:00:36,097 --> 00:00:56,097
<font color="#808000">The_Passing_Bells - S01 - E05</font>
<font color="#caca00">Re-Sync & Edit
Tharindu_Lakmal_Jeewantha</font>

14
00:01:10,320 --> 00:01:11,833
(EXPLOSIONS)

15
00:01:24,080 --> 00:01:25,069
(EXPLOSIONS)

16
00:01:26,920 --> 00:01:29,912
(PANTING)

17
00:01:30,440 --> 00:01:32,351
Push. Please.

18
00:01:32,880 --> 00:01:35,872
Please. You're nearly there.

19
00:01:36,120 --> 00:01:37,473
(JOANNA SCREAMING)

20
00:01:37,880 --> 00:01:38,869
Ahh!

21
00:01:39,720 --> 00:01:40,869
(EXPLOSION)

22
00:01:43,960 --> 00:01:45,473
(CRYING)

23
00:01:48,560 --> 00:01:49,549
Shh, shh.

24
00:01:57,920 --> 00:01:58,955
(WOMEN LAUGHING)

25
00:02:04,360 --> 00:02:05,509
(SOLDIERS SHOUTING INDISTINCTLY)

26
00:02:06,120 --> 00:02:07,155
(GUNFIRE AND EXPLOSIONS)

27
00:02:11,600 --> 00:02:13,875
(INHALING)

28
00:02:19,520 --> 00:02:21,078
(DISTANT GUNFIRE)

29
00:02:22,080 --> 00:02:23,559
(EXPLOSION)

30
00:02:24,960 --> 00:02:26,234
WOMAN 1: I'm sorry, that's all I've got.

31
00:02:27,080 --> 00:02:28,991
WOMAN 2: I've got more kids to feed
than you have.

32
00:02:29,080 --> 00:02:30,229
WOMAN 3: Get off, get off me!

33
00:02:30,360 --> 00:02:31,349
Ah!

34
00:02:37,960 --> 00:02:39,234
(GUNFIRE CONTINUES)

35
00:02:50,720 --> 00:02:51,709
DEREK: Tommy!

36
00:03:06,600 --> 00:03:10,070
It's a boy! They called him Thomas!

37
00:03:15,040 --> 00:03:16,029
It's a boy!

38
00:03:16,120 --> 00:03:19,556
(ALL CHEERING)

39
00:03:37,880 --> 00:03:40,713
SOLDIER: Move back, dig in.
Move back, dig in.

40
00:03:40,800 --> 00:03:42,711
We've done nothing but
go backwards for a month.

41
00:03:44,000 --> 00:03:47,470
All I'm saying is,
why don't we keep just going back,

42
00:03:47,680 --> 00:03:50,319
all the way home?
Just throw down our guns and go.

43
00:03:51,680 --> 00:03:53,113
Do you think I want to be here?

44
00:03:54,080 --> 00:03:55,991
(SHOUTING) Do you think
any of us want to be here?

45
00:03:57,400 --> 00:03:58,435
But we are!

46
00:04:00,200 --> 00:04:03,670
I haven't spent four years of my life
watching people that I love

47
00:04:03,880 --> 00:04:07,793
die all around me just to
drop my rifle and go home!

48
00:04:26,080 --> 00:04:27,069
Sorry, sorry.

49
00:04:35,120 --> 00:04:36,519
I've watched my friends die, too.

50
00:04:39,760 --> 00:04:41,079
I'm sick of it.

51
00:04:46,160 --> 00:04:47,593
I suppose I can smell the end of it.

52
00:04:50,520 --> 00:04:53,990
I'm just as frightened by that
as I am of fighting.

53
00:05:23,160 --> 00:05:24,275
We keep moving forward,

54
00:05:25,280 --> 00:05:26,872
but how come we haven't seen
any Germans?

55
00:05:27,240 --> 00:05:28,593
We will.

56
00:05:28,920 --> 00:05:30,194
I can't think what else
we're doing here.

57
00:05:31,120 --> 00:05:32,439
Pushing them back, that's all.

58
00:05:33,200 --> 00:05:35,031
They're saying that the Germans might
have had enough

59
00:05:35,120 --> 00:05:36,473
now the Americans have joined us.

60
00:05:37,600 --> 00:05:39,397
A bloke from the Irish Guards
said that he heard that

61
00:05:39,480 --> 00:05:41,471
some of them are giving up
and heading back home.

62
00:05:42,240 --> 00:05:43,275
Don't tell me,

63
00:05:43,720 --> 00:05:45,870
- "This'll all be over by Christmas."
- That's what they're saying!

64
00:05:46,000 --> 00:05:49,549
Yeah. And they said that in 1914, 1915

65
00:05:49,840 --> 00:05:51,273
and '16 and '17.

66
00:05:52,080 --> 00:05:53,308
All right but,

67
00:05:53,600 --> 00:05:55,431
what if they're right
what they're saying this time

68
00:05:55,600 --> 00:05:56,635
and we can go home?

69
00:05:57,640 --> 00:06:00,154
I'll kiss everyone I see
from here to Blighty.

70
00:06:01,840 --> 00:06:03,637
Well, maybe not everyone. (CHUCKLES)

71
00:06:04,480 --> 00:06:05,833
It's got to be over soon.

72
00:06:07,280 --> 00:06:09,794
- I don't know if I can do much more.
- We'll be all right.

73
00:06:10,080 --> 00:06:11,149
No, I mean it, Tommy.

74
00:06:12,520 --> 00:06:13,839
I think I'll go mad.

75
00:06:14,920 --> 00:06:17,832
Just stick with me, all right?
We'll go home together.

76
00:06:29,400 --> 00:06:30,753
- (BABY COOING)
- (JOANNA SINGING)

77
00:06:30,840 --> 00:06:32,398
DAVID: There's an article about Poland.

78
00:06:36,520 --> 00:06:38,476
"As revolution grows across Europe

79
00:06:38,720 --> 00:06:40,472
"and the armistice gets closer,

80
00:06:40,800 --> 00:06:43,473
"it is thought there could be
a new Peoples Republic of Poland

81
00:06:43,560 --> 00:06:44,709
"within weeks."

82
00:06:46,480 --> 00:06:48,072
Can I keep it to show my father?

83
00:06:51,360 --> 00:06:53,396
(EXPLOSIONS)

84
00:06:58,440 --> 00:07:00,351
Right, they're dug in 200 yards ahead,

85
00:07:00,440 --> 00:07:01,555
and we'll wire down

86
00:07:01,640 --> 00:07:03,835
till Artillery's gonna let them
know we're here.

87
00:07:05,640 --> 00:07:06,629
(EXPLOSIONS)

88
00:07:11,680 --> 00:07:13,432
(ALL SCREAMING)

89
00:07:14,080 --> 00:07:17,038
- (SOLDIER GIVING ORDERS)
- Coming, sir.

90
00:07:18,080 --> 00:07:19,069
Move yourselves!

91
00:07:19,760 --> 00:07:21,113
Move! Move! Move!

92
00:07:21,960 --> 00:07:22,995
Come on!

93
00:07:24,720 --> 00:07:25,789
Move yourself!

94
00:07:25,960 --> 00:07:27,871
We've got injured, come on!

95
00:07:28,200 --> 00:07:29,235
Move!

96
00:07:32,720 --> 00:07:35,792
"With Hungary and now Austria
out of the war,

97
00:07:35,920 --> 00:07:37,876
"the Germans are becoming isolated

98
00:07:38,640 --> 00:07:40,949
"and the armistice grows
nearer by the day."

99
00:07:41,040 --> 00:07:42,393
Joanna Braun. Joanna?

100
00:07:42,480 --> 00:07:44,118
"It is thought that German terms

101
00:07:44,200 --> 00:07:47,431
"will soon be presented
to the allied and associated governments

102
00:07:47,720 --> 00:07:49,233
"and finally...

103
00:07:51,200 --> 00:07:52,235
"the war will be over."

104
00:07:52,400 --> 00:07:53,879
(PEOPLE CHEERING)

105
00:07:54,200 --> 00:07:55,189
MAN: At last!

106
00:07:59,320 --> 00:08:00,673
Her dad's been killed.

107
00:08:02,640 --> 00:08:04,995
I'm not sure if we'll get
any more leaves, son.

108
00:08:05,320 --> 00:08:07,311
Not when top brass thinks
we've got them on the run

109
00:08:07,400 --> 00:08:09,470
and all we're hearin' is "push forward".

110
00:08:09,840 --> 00:08:10,989
I've got a baby, Sergeant.

111
00:08:11,160 --> 00:08:14,277
Yeah? Well, if this armistice happens,
we'll all be home for good soon.

112
00:08:14,840 --> 00:08:18,071
- Do you really think so?
- It'll happen. Just don't ask me when.

113
00:08:19,160 --> 00:08:21,913
Hopefully before that nipper
of yours starts school.

114
00:08:26,640 --> 00:08:27,959
I'll ask, all right?

115
00:08:33,800 --> 00:08:34,835
Sergeant,

116
00:08:36,520 --> 00:08:38,670
is there any chance of moving
Derek back to the supply trenches?

117
00:08:39,680 --> 00:08:40,715
He's not right.

118
00:08:41,160 --> 00:08:42,912
Go out there and find me a man who is.

119
00:08:51,680 --> 00:08:52,999
(EXPLOSIONS)

120
00:09:37,200 --> 00:09:38,713
I wasn't much use today, was I?

121
00:09:40,920 --> 00:09:43,514
You've marched miles
in the past few weeks.

122
00:09:43,600 --> 00:09:44,794
You're just tired, that's all.

123
00:09:45,840 --> 00:09:46,989
We all are.

124
00:09:47,840 --> 00:09:49,478
But you're doing brilliantly.

125
00:09:50,000 --> 00:09:51,718
"Head down, push on till
the job's done," remember?

126
00:09:52,880 --> 00:09:53,915
I try, Tommy.

127
00:09:55,560 --> 00:09:57,994
But every time a shell goes off
I jump out of my skin.

128
00:10:00,920 --> 00:10:02,399
My hands are always shaking.

129
00:10:04,160 --> 00:10:05,195
My nerves are shot.

130
00:10:08,960 --> 00:10:10,188
I don't want to die.

131
00:10:11,680 --> 00:10:13,398
Stay close to me, you'll be all right.

132
00:10:15,560 --> 00:10:16,675
Do you think I'm a coward?

133
00:10:18,080 --> 00:10:19,069
No.

134
00:10:20,280 --> 00:10:21,599
Don't ever say that.

135
00:10:24,200 --> 00:10:25,315
We're all scared.

136
00:10:26,760 --> 00:10:28,478
Right? Every single one of us.

137
00:10:32,840 --> 00:10:34,671
Maybe you're more used to it than I am.

138
00:10:37,040 --> 00:10:38,871
Sometimes my hands are shaking so much,

139
00:10:38,960 --> 00:10:41,599
I have to wedge my arm into the sandbags
just to fire me rifle.

140
00:10:44,240 --> 00:10:45,798
There's nothing wrong with being scared.

141
00:10:47,240 --> 00:10:48,593
It just means you're human.

142
00:10:50,680 --> 00:10:52,238
Being scared will keep you alive.

143
00:10:56,040 --> 00:10:57,075
Now get some sleep.

144
00:11:00,720 --> 00:11:02,039
Thanks, Tommy.

145
00:11:08,360 --> 00:11:09,918
Why are you always reading that?

146
00:11:13,360 --> 00:11:14,793
Reminds me of home, I suppose.

147
00:11:18,200 --> 00:11:19,792
It was the first book I ever got.

148
00:11:21,760 --> 00:11:23,193
I must have been about six.

149
00:11:26,120 --> 00:11:27,838
I mark off the ones I've seen.

150
00:11:28,760 --> 00:11:30,193
I sketch them sometimes, too.

151
00:11:34,000 --> 00:11:35,035
It's funny I...

152
00:11:37,920 --> 00:11:40,718
I don't think people realise
how beautiful birds are.

153
00:11:43,800 --> 00:11:45,199
You know, you see them out here,

154
00:11:47,640 --> 00:11:49,312
they even land on the barbed wire.

155
00:11:53,120 --> 00:11:54,633
Something so beautiful,

156
00:11:56,960 --> 00:11:58,393
in the middle of something so ugly.

157
00:12:15,640 --> 00:12:17,358
Do you think it's true what people
are saying?

158
00:12:21,000 --> 00:12:22,991
That the fighting will stop soon?

159
00:12:26,600 --> 00:12:27,635
We'll see.

160
00:12:32,120 --> 00:12:33,553
It's funny, innit?

161
00:12:35,800 --> 00:12:37,950
A hundred years from now
none of us will be here.

162
00:12:39,960 --> 00:12:41,075
And all this...

163
00:12:44,480 --> 00:12:45,959
will be ancient history.

164
00:12:51,360 --> 00:12:53,590
I wonder what people will make of it?

165
00:13:34,720 --> 00:13:35,755
It doesn't seem that long

166
00:13:35,840 --> 00:13:37,990
since I was putting his dad
to bed in the same room.

167
00:13:42,520 --> 00:13:43,555
I'm sorry.

168
00:13:44,360 --> 00:13:45,349
(BABY COOING)

169
00:13:47,360 --> 00:13:49,476
My father died for
something he believed in

170
00:13:49,560 --> 00:13:50,834
so I'm sad, but

171
00:13:51,640 --> 00:13:52,868
I'm proud, too.

172
00:13:56,520 --> 00:13:59,080
Just like little Thomas
will be when his dad comes home.

173
00:14:15,040 --> 00:14:17,429
All the officers have been
called back to the command centre.

174
00:14:19,640 --> 00:14:20,789
You think this is it?

175
00:14:21,800 --> 00:14:23,358
Something's happening.

176
00:15:16,040 --> 00:15:17,951
I can't believe he's coming home.

177
00:15:18,160 --> 00:15:21,072
- Nothing's been announced yet.
- The papers say it's over.

178
00:15:21,160 --> 00:15:24,630
Well, the fighting doesn't just stop,
they have to agree terms.

179
00:15:25,160 --> 00:15:26,798
The price may be more than
we're willing to pay.

180
00:15:26,880 --> 00:15:28,029
But we have no choice.

181
00:15:28,120 --> 00:15:30,839
Choice or not, they're politicians
so it's going to take time.

182
00:15:31,760 --> 00:15:34,320
They won't keep fighting, will they?
Not while they're talking.

183
00:15:34,400 --> 00:15:36,470
- Probably.
- But that's stupid!

184
00:15:36,560 --> 00:15:39,233
If everyone knows it's over,
why let more boys die?

185
00:15:39,320 --> 00:15:40,719
That's not the way they see it.

186
00:15:40,880 --> 00:15:43,189
Why not? Don't they have children?

187
00:15:55,640 --> 00:15:56,709
Writing home?

188
00:15:58,120 --> 00:16:00,190
Maybe if the rumours are true
I won't have to post it.

189
00:16:00,560 --> 00:16:02,755
I can take it myself. (CHUCKLING)

190
00:16:04,320 --> 00:16:06,276
But who knows
what we'll be going back to.

191
00:16:07,080 --> 00:16:08,308
Does it matter?

192
00:16:08,640 --> 00:16:09,675
As long as we're home.

193
00:16:09,760 --> 00:16:12,832
It won't be the home we left,
if we lose this war.

194
00:16:15,000 --> 00:16:16,319
There's no food,

195
00:16:17,160 --> 00:16:18,309
no jobs.

196
00:16:19,320 --> 00:16:20,719
My wife will be there.

197
00:16:21,920 --> 00:16:24,832
Everything else we can just
rebuild, in time.

198
00:16:29,960 --> 00:16:32,394
Right now I'd settle for
sitting in front of a warm fire.

199
00:16:56,200 --> 00:17:00,239
Kaiser Bill's chucked the towel in boys!
We've got them on the run!

200
00:17:00,480 --> 00:17:03,199
- What's that mean, Sergeant?
- Means he's abdicated.

201
00:17:03,480 --> 00:17:05,038
They'll be like headless chickens now.

202
00:17:05,200 --> 00:17:06,394
We'll be home by Christmas.

203
00:17:06,520 --> 00:17:07,589
(SOLDIERS CHATTERING)

204
00:17:07,680 --> 00:17:08,908
SOLDIER: We've heard that one before.

205
00:17:12,240 --> 00:17:13,275
Did you hear?

206
00:17:14,760 --> 00:17:15,988
I heard.

207
00:17:18,640 --> 00:17:19,675
So that's it, then.

208
00:17:20,560 --> 00:17:22,755
They haven't told us to start
packing up to go home yet.

209
00:17:23,080 --> 00:17:26,231
Yeah, but they will do, won't they?
If the Kaiser's gone,

210
00:17:26,520 --> 00:17:27,714
that's good isn't it?

211
00:17:28,640 --> 00:17:30,790
Yeah. It's good.

212
00:17:31,560 --> 00:17:33,118
It won't be long now.

213
00:17:33,840 --> 00:17:34,829
(EXPLOSION)

214
00:17:36,920 --> 00:17:39,115
All we need now
is for someone to tell that lot.

215
00:17:40,120 --> 00:17:43,476
(EXPLOSIONS)

216
00:18:06,840 --> 00:18:09,832
MAN: Place is agreed. Railway siding,
Compi�gne Forest.

217
00:18:17,320 --> 00:18:18,435
(SCREAMING)

218
00:18:19,840 --> 00:18:20,875
Come on!

219
00:18:21,440 --> 00:18:22,509
(GUNFIRE)

220
00:18:32,280 --> 00:18:33,429
(SHOUTING ORDERS)

221
00:18:34,120 --> 00:18:36,156
(EXPLOSIONS AND GUNFIRE)

222
00:18:44,560 --> 00:18:45,549
(GASPS)

223
00:18:51,440 --> 00:18:52,759
(GASPS)

224
00:19:28,880 --> 00:19:30,108
(DISTANT EXPLOSION)

225
00:19:31,600 --> 00:19:35,070
- Monday morning.
- Eh?

226
00:19:35,520 --> 00:19:37,988
Looked in my diary this morning
and saw it was Monday.

227
00:19:39,200 --> 00:19:42,033
Damn.
Missed Sunday roast again yesterday.

228
00:19:43,320 --> 00:19:45,470
Beef and Yorkshire pudding
swimming in gravy.

229
00:19:51,280 --> 00:19:53,919
Just think, back at home.

230
00:19:54,680 --> 00:19:57,035
Everyone going back to work
after the weekend,

231
00:19:58,120 --> 00:19:59,951
another week starting.

232
00:20:06,400 --> 00:20:07,753
It's quiet.

233
00:20:10,680 --> 00:20:12,750
They were shelling us
this time yesterday.

234
00:20:14,520 --> 00:20:16,556
Maybe they're having a lie-in.

235
00:20:19,640 --> 00:20:21,358
- Morning, boys.
- Sergeant.

236
00:20:22,240 --> 00:20:24,629
Looks like a shell's taken out
a section of the wire.

237
00:20:25,680 --> 00:20:27,272
It's your turn, Del.

238
00:20:28,720 --> 00:20:30,199
It's all right, Sergeant, I'll do it.

239
00:20:32,040 --> 00:20:33,519
Dead in line with the next elbow,

240
00:20:33,600 --> 00:20:35,670
you should be able to
pull it back together and tie it.

241
00:20:36,440 --> 00:20:38,396
It's all quiet over there
but keep your head down

242
00:20:38,480 --> 00:20:39,913
in case they've got a sniper on watch.

243
00:20:40,440 --> 00:20:41,668
Don't worry about that.

244
00:20:46,120 --> 00:20:47,951
You didn't have to do that,
it's my turn.

245
00:20:48,960 --> 00:20:50,393
You'd only get tangled up in the wire

246
00:20:50,480 --> 00:20:51,913
and I'd have to come and get you anyway.

247
00:20:53,600 --> 00:20:54,794
Thanks, Tommy.

248
00:21:04,800 --> 00:21:06,074
Give me a shout when dinner's ready.

249
00:22:36,800 --> 00:22:37,789
(GRUNTING)

250
00:22:39,720 --> 00:22:41,073
(INDISTINCT CONVERSATION)

251
00:22:59,960 --> 00:23:00,949
(HEAVY BREATHING)

252
00:23:02,000 --> 00:23:03,353
(CLICKING)

253
00:23:09,800 --> 00:23:10,835
(GRUNTS)

254
00:23:14,000 --> 00:23:15,069
Take this. Go!

255
00:23:25,200 --> 00:23:26,189
(GUNSHOT)

256
00:23:33,760 --> 00:23:34,749
(LABOURED BREATHING)

257
00:23:45,320 --> 00:23:48,312
SOLDIER: The war is over!
Germany surrendered!

258
00:23:48,337 --> 00:24:48,337
<font color="#808000">The_Passing_Bells - S01 - E05</font>
<font color="#caca00">Re-Sync & Edit
Tharindu_Lakmal_Jeewantha</font>

