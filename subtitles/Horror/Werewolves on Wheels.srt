﻿1
00:00:36,508 --> 00:00:39,206
You do realize why you're in this car,
do you not?

2
00:00:41,304 --> 00:00:43,840
You've been tasked by Mr. Durant
to build a bridge.

3
00:00:43,965 --> 00:00:47,060
Any man can lay track.
Am I correct?

4
00:00:47,185 --> 00:00:49,424
Yes, ma'am.
Pretty near.

5
00:00:49,590 --> 00:00:51,781
Then can you explain
why you've been spending

6
00:00:51,906 --> 00:00:53,733
all your energy at rail's end

7
00:00:53,858 --> 00:00:55,222
instead of at the bridge site?

8
00:00:55,388 --> 00:00:57,849
That will be remedied, ma'am.

9
00:00:58,015 --> 00:01:00,497
Thank you.

10
00:01:03,284 --> 00:01:05,691
You heard what the lady said.

11
00:01:05,857 --> 00:01:09,611
This the part where I tell you the men
don't like taking orders from a woman.

12
00:01:09,777 --> 00:01:11,418
They don't have to like it.

13
00:01:11,543 --> 00:01:13,887
No, they don't.

14
00:01:14,012 --> 00:01:16,205
But if you're looking to get back
in Mr. Durant's good graces,

15
00:01:16,330 --> 00:01:19,329
then pissing off the man
might not be the best tact.

16
00:01:19,495 --> 00:01:22,249
Doesn't matter,
as long as we get this railroad built.

17
00:01:22,415 --> 00:01:23,713
If you're not
careful, miss bell,

18
00:01:23,838 --> 00:01:26,266
you're going to start
sounding like a man.

19
00:01:26,391 --> 00:01:28,630
Right.

20
00:01:47,579 --> 00:01:50,235
All right! Move your carcasses!

21
00:01:56,537 --> 00:01:59,089
Step on it!

22
00:02:01,913 --> 00:02:04,916
You lot, for the gorge,

23
00:02:05,082 --> 00:02:08,378
make room for one more.
And as for the rest of you

24
00:02:08,544 --> 00:02:11,277
jackanapes, I expect to be able
to walk a mile of rail

25
00:02:11,402 --> 00:02:13,480
by the time I get back
from me lunch break.

26
00:02:15,426 --> 00:02:17,233
Change the numbers all you want,
that ain't the problem.

27
00:02:17,358 --> 00:02:18,952
56 men?

28
00:02:19,077 --> 00:02:20,603
23.

29
00:02:20,728 --> 00:02:24,586
And we've got another 46 coming in.
Another...

30
00:02:43,704 --> 00:02:46,708
That's got to be Fleming.
The sentry?

31
00:02:46,874 --> 00:02:49,723
You need to do something.

32
00:03:12,280 --> 00:03:14,945
Bring me the long rifle.

33
00:03:15,111 --> 00:03:16,696
There's too many!
They got the high ground.

34
00:03:16,862 --> 00:03:18,126
If you keep them occupied,

35
00:03:18,251 --> 00:03:20,033
we can loop back and try
and outflank 'em.

36
00:03:20,255 --> 00:03:22,745
Damn it, I said bring me
the long rifle right now!

37
00:03:29,480 --> 00:03:31,336
Get back.

38
00:04:03,298 --> 00:04:04,207
He shot Fleming!

39
00:04:47,078 --> 00:04:50,373
<font color="#3399FF">Sync and correction by Mlmlte</font>
<font color="#3399FF">www.addic7ed.com</font>

40
00:05:14,605 --> 00:05:16,315
{pub}Look at me.

41
00:05:16,440 --> 00:05:18,151
Look at me.
Look at me.

42
00:05:18,317 --> 00:05:20,352
Look at me.

43
00:05:20,477 --> 00:05:23,073
I see you.

44
00:05:47,638 --> 00:05:50,100
Damn, woman.

45
00:05:55,512 --> 00:05:59,308
What's so funny?

46
00:05:59,433 --> 00:06:01,844
Never thought I'd be glad

47
00:06:01,969 --> 00:06:04,313
Mr. Toole made foreman.

48
00:06:04,438 --> 00:06:07,900
Out early, home late.

49
00:06:08,025 --> 00:06:10,954
Don't speak his name.

50
00:06:11,445 --> 00:06:13,706
You know the rules.

51
00:06:13,831 --> 00:06:15,750
Right.

52
00:06:15,916 --> 00:06:18,494
'cause you live by rules.

53
00:06:27,127 --> 00:06:31,182
I want you to feel something.

54
00:06:31,348 --> 00:06:34,059
Damn, girl.

55
00:06:34,184 --> 00:06:36,229
Give a man a minute
to raise his spirit.

56
00:06:45,713 --> 00:06:47,866
You feel that?

57
00:06:50,200 --> 00:06:53,204
That little bump?

58
00:06:53,370 --> 00:06:55,874
You and me made that.

59
00:07:03,530 --> 00:07:05,967
How you know?

60
00:07:06,199 --> 00:07:09,552
Because I know.

61
00:07:24,935 --> 00:07:27,447
Say something.

62
00:07:38,374 --> 00:07:40,880
Elam Ferguson,
you look me in the eyes

63
00:07:41,046 --> 00:07:42,438
and you tell me what you're thinkin'.

64
00:08:02,806 --> 00:08:06,027
Don't you leave here
without saying something.

65
00:08:09,697 --> 00:08:12,598
I can't.

66
00:08:17,004 --> 00:08:18,497
See, I'm ask... asking you

67
00:08:18,622 --> 00:08:20,699
to look... look inside your heart.

68
00:08:20,824 --> 00:08:22,317
Inside your heart.

69
00:08:22,442 --> 00:08:25,204
Sorry, Reverend.
I'm afraid it's empty.

70
00:08:25,329 --> 00:08:28,280
Same as my wallet.

71
00:08:32,753 --> 00:08:34,754
Hey!

72
00:08:36,623 --> 00:08:38,550
All right now?

73
00:08:38,675 --> 00:08:40,626
Reverend.
Cullen!

74
00:08:40,936 --> 00:08:41,971
Could you...

75
00:08:42,096 --> 00:08:44,023
Could you spot a fellow
traveler a drink?

76
00:08:44,297 --> 00:08:45,732
Rye, barkeep.

77
00:08:46,401 --> 00:08:48,225
- Be right with you.
- Carl.

78
00:08:51,100 --> 00:08:54,233
Just the one.
You've got to read over a man today.

79
00:08:54,358 --> 00:08:57,703
Oh, you have to ask
that harlot who threw me out.

80
00:08:57,828 --> 00:08:59,247
You mean your daughter?

81
00:08:59,413 --> 00:09:00,572
Yeah.

82
00:09:00,697 --> 00:09:02,691
That sinning bitch

83
00:09:02,816 --> 00:09:04,460
stole my church.

84
00:09:04,585 --> 00:09:09,822
Yes, she's...
Your conduit to the Lord now.

85
00:09:14,044 --> 00:09:17,873
You mind me asking
where you been bedding down?

86
00:09:17,998 --> 00:09:20,092
I've been, uh,

87
00:09:20,217 --> 00:09:22,394
been sleeping in the cemetery.

88
00:09:22,519 --> 00:09:25,431
It's near freezing at night.
There's no cover.

89
00:09:25,556 --> 00:09:28,550
Yeah, I find the dead a comfort.

90
00:09:30,310 --> 00:09:32,864
What the hell?

91
00:09:33,030 --> 00:09:35,033
You can bunk with me
until you get yourself settled.

92
00:09:35,199 --> 00:09:37,276
Go on. Set him up.

93
00:09:37,401 --> 00:09:40,872
Carl, set them... set them up.

94
00:09:56,386 --> 00:09:58,097
What in the hell's going on?

95
00:09:58,222 --> 00:10:00,099
Why aren't y'all men at work?

96
00:10:00,224 --> 00:10:03,061
Turn around and get back on...

97
00:10:05,429 --> 00:10:07,690
Mr. Toole.

98
00:10:09,650 --> 00:10:12,111
You mind telling me
why these men ain't at work?

99
00:10:12,352 --> 00:10:14,646
With all due respect,

100
00:10:14,771 --> 00:10:16,699
we lost a man out there.

101
00:10:16,865 --> 00:10:19,485
- A mate.
- That's right!

102
00:10:19,610 --> 00:10:21,663
Then you finish your day's work.

103
00:10:21,895 --> 00:10:25,083
Then you come back in
and drink to his memory.

104
00:10:25,249 --> 00:10:28,377
We parlayed.
We all agree.

105
00:10:28,502 --> 00:10:29,795
It's not safe.

106
00:10:29,962 --> 00:10:31,413
It's about to get unsafe around here

107
00:10:31,538 --> 00:10:34,416
if you don't get
your ass back to work.

108
00:10:34,541 --> 00:10:36,719
I am afraid you'll
just have to shoot us.

109
00:10:37,127 --> 00:10:39,055
That's right!

110
00:10:53,626 --> 00:10:55,853
{pub}If we route South,

111
00:10:55,978 --> 00:10:58,522
then we can bridge the river here...

112
00:10:58,647 --> 00:11:01,859
And avoid the entire
issue of Sacred Land.

113
00:11:01,984 --> 00:11:02,579
Yes.

114
00:11:04,060 --> 00:11:05,646
At considerable expense.

115
00:11:05,771 --> 00:11:07,665
Spend a few more pennies now

116
00:11:07,790 --> 00:11:09,333
and keep your work force, Thomas.

117
00:11:09,458 --> 00:11:10,584
Pennies.

118
00:11:10,709 --> 00:11:12,463
We can't run from the Sioux.

119
00:11:12,629 --> 00:11:14,591
If it ain't this gorge,
it'll be the next one.

120
00:11:14,757 --> 00:11:17,885
- The men are afraid to work.
- Then we should find ones who ain't.

121
00:11:18,116 --> 00:11:20,972
They believe that if the Sioux
won't kill them, perhaps you will.

122
00:11:21,138 --> 00:11:22,223
Bullshit.

123
00:11:22,389 --> 00:11:24,684
Of course. When logic isn't on
your side, resort to the profane.

124
00:11:24,850 --> 00:11:26,834
All right, I'll give you facts.

125
00:11:26,959 --> 00:11:28,813
Here, here, here.

126
00:11:28,979 --> 00:11:31,105
May not be sacred,
but it's all Sioux territory.

127
00:11:31,230 --> 00:11:33,815
We've already established what's
on this side of the gorge!

128
00:11:34,443 --> 00:11:35,776
For once, let's be smart.

129
00:11:35,901 --> 00:11:37,905
Reroute and show the men that we can
steer them out of harm's way.

130
00:11:38,071 --> 00:11:40,241
The Indians' home's at stake,
young lady.

131
00:11:40,407 --> 00:11:41,515
They'll fight us all
the way to the Rockies.

132
00:11:41,640 --> 00:11:44,329
- If that's true, then we're lost!
- We turn tail first sign of trouble,

133
00:11:44,495 --> 00:11:45,686
yes, we are.
You don't...

134
00:11:45,811 --> 00:11:46,871
Whoa, whoa, whoa.

135
00:11:46,996 --> 00:11:49,584
What is your plan, Mr. Bohannon?

136
00:11:49,750 --> 00:11:51,492
Fight 'em here, fight 'em now.

137
00:11:51,617 --> 00:11:53,744
With what? That rabble?

138
00:11:53,869 --> 00:11:54,756
Them's the ones we got.

139
00:11:54,987 --> 00:11:56,464
They...

140
00:11:56,589 --> 00:12:00,051
They won't even work for you.

141
00:12:00,176 --> 00:12:02,837
You let me worry about that.

142
00:12:02,962 --> 00:12:05,391
Just don't reroute this railroad.

143
00:12:14,390 --> 00:12:17,768
Let me.

144
00:12:17,893 --> 00:12:20,604
Thank you.

145
00:12:20,729 --> 00:12:24,057
Your husband know about
what you told me earlier?

146
00:12:24,182 --> 00:12:25,726
No.

147
00:12:25,851 --> 00:12:28,329
Nobody knows but you and me.

148
00:12:28,454 --> 00:12:30,698
Good.

149
00:12:30,823 --> 00:12:34,585
I've spent my whole
life trying to get free.

150
00:12:34,710 --> 00:12:37,799
Then here come a woman...
A white woman...

151
00:12:38,030 --> 00:12:40,843
A married woman, spinning my head

152
00:12:41,009 --> 00:12:43,410
saying she's got my baby.

153
00:12:43,535 --> 00:12:46,747
I couldn't think of
nothin' to do but run.

154
00:12:46,872 --> 00:12:49,717
Then it set with me.

155
00:12:49,842 --> 00:12:51,418
And?

156
00:12:51,543 --> 00:12:54,438
Any harm come to you,
I go weak in the head.

157
00:12:54,563 --> 00:12:55,942
You are weak in the head.

158
00:12:56,181 --> 00:12:57,391
All the gold eagles in the world

159
00:12:57,516 --> 00:12:59,143
ain't worth a pinch of coon shit

160
00:12:59,268 --> 00:13:03,074
if you can't use it to help
the folk that you care about.

161
00:13:03,355 --> 00:13:05,649
Elam...

162
00:13:05,774 --> 00:13:08,769
What are you saying?

163
00:13:08,894 --> 00:13:11,165
You saying you care about me?

164
00:13:12,865 --> 00:13:15,617
I love you.

165
00:13:16,869 --> 00:13:18,965
I love you, woman.

166
00:13:33,251 --> 00:13:36,899
I need you to take
these double eagles

167
00:13:37,065 --> 00:13:39,610
and get yourself taken care of.

168
00:13:41,143 --> 00:13:42,947
Taken care of.

169
00:13:43,113 --> 00:13:44,822
You can't have no black baby.

170
00:13:44,947 --> 00:13:47,160
Your husband will kill
it or you or both of you.

171
00:13:47,432 --> 00:13:49,660
Elam, I used to be a whore.
If I want to get rid of this child,

172
00:13:49,785 --> 00:13:52,413
I got my ways.

173
00:13:52,538 --> 00:13:56,407
And you don't need to fear
for your freedom on my account.

174
00:14:00,663 --> 00:14:04,825
♪ A prouder man
I'd walk the land ♪

175
00:14:04,950 --> 00:14:08,328
♪ in health and peace of mind

176
00:14:08,453 --> 00:14:11,999
♪ if I might toil
and strive and moil ♪

177
00:14:12,124 --> 00:14:15,469
♪ nor cast one thought behind

178
00:14:15,594 --> 00:14:19,006
♪ but what would be
the world to me ♪

179
00:14:19,131 --> 00:14:22,359
♪ its wealth and rich array

180
00:14:22,484 --> 00:14:26,063
♪ if memory I lost of thee

181
00:14:26,188 --> 00:14:28,951
♪ my own dear galway...

182
00:14:50,681 --> 00:14:52,923
I come to pay my respects
to Mr. Fleming.

183
00:14:53,048 --> 00:14:54,811
He was a good man.

184
00:14:54,977 --> 00:14:57,878
A poor end he was met with.

185
00:14:58,003 --> 00:15:00,775
And he won't be the last, I'm afraid.

186
00:15:14,987 --> 00:15:18,373
For Mr. Michael Fleming...

187
00:15:24,580 --> 00:15:27,364
Um...

188
00:15:29,001 --> 00:15:32,637
He didn't ask for what he got...

189
00:15:32,973 --> 00:15:35,434
But he got it anyway.

190
00:15:37,843 --> 00:15:39,188
Life ain't fair.

191
00:15:39,354 --> 00:15:41,772
Ain't fair at all.

192
00:15:41,897 --> 00:15:44,725
Hell, most of us here in this room

193
00:15:44,850 --> 00:15:46,320
fought in a war
one side or the other.

194
00:15:46,568 --> 00:15:48,395
We all seen men fall.

195
00:15:48,520 --> 00:15:51,949
We prayed over 'em,
we drank over 'em.

196
00:15:52,074 --> 00:15:55,286
But we did not quit.

197
00:15:55,411 --> 00:15:59,542
The best way to honor
your friend is to carry on.

198
00:15:59,708 --> 00:16:01,919
Finish this road.

199
00:16:03,952 --> 00:16:05,996
Mr. Bohannon, Mr. Bohannon!

200
00:16:06,121 --> 00:16:08,799
No disrespect intended,

201
00:16:08,924 --> 00:16:11,888
but we won't die for your railroad.

202
00:16:12,054 --> 00:16:13,504
Not on your life!

203
00:16:13,629 --> 00:16:16,123
Your friend didn't have to die.

204
00:16:16,248 --> 00:16:17,435
You best have a point.

205
00:16:17,601 --> 00:16:20,227
We're all in the same position...

206
00:16:20,352 --> 00:16:23,357
The Sioux don't look
at any of us any different.

207
00:16:23,605 --> 00:16:25,432
They just aim to kill.

208
00:16:25,557 --> 00:16:28,569
That doesn't explain why you're
interrupting a private wake.

209
00:16:28,694 --> 00:16:31,322
We're willing to protect
the end of the rail

210
00:16:31,447 --> 00:16:32,950
and the bridge site.

211
00:16:33,116 --> 00:16:36,329
If the railroad give us guns,
we'll go out early and...

212
00:16:36,495 --> 00:16:38,495
Give 'em guns?

213
00:16:38,620 --> 00:16:41,115
Patrol for Sioux.

214
00:16:41,240 --> 00:16:44,168
Mr. Durant ain't going
to arm no ex-slaves

215
00:16:44,293 --> 00:16:45,869
and these men wouldn't
stand for it, neither.

216
00:16:45,994 --> 00:16:48,507
Damn right.
Aye!

217
00:16:48,747 --> 00:16:51,344
You best get yourself on out of here.

218
00:16:51,510 --> 00:16:54,013
Then you ain't getting
no work out of us.

219
00:16:55,671 --> 00:16:57,516
That's not a stand you want to take.

220
00:16:57,682 --> 00:17:00,895
We aren't working, neither.
Not until you protect us.

221
00:17:01,061 --> 00:17:02,855
We won't be sitting ducks out there.

222
00:17:04,229 --> 00:17:05,691
Dammit, y'all, listen to me.

223
00:17:05,857 --> 00:17:09,528
That road's going to get
built with or without you.

224
00:17:09,694 --> 00:17:12,312
If y'all don't go to work,
it's you who's going to lose.

225
00:17:12,437 --> 00:17:14,784
Now trust me on this.

226
00:17:14,950 --> 00:17:17,474
You'll lose and it'll get ugly.

227
00:17:24,567 --> 00:17:26,246
I'm sorry for your loss.

228
00:17:33,926 --> 00:17:39,223
♪ 'Tis far away I am today

229
00:17:39,348 --> 00:17:41,894
♪ from scenes I roamed a boy ♪

230
00:17:42,060 --> 00:17:49,068
♪ and long ago the hour
I know, I first saw Illinois ♪

231
00:17:49,341 --> 00:17:52,736
♪ but time nor tide
nor waters wide ♪

232
00:17:52,861 --> 00:17:56,356
♪ can wean my heart away

233
00:17:56,481 --> 00:17:59,610
♪ forever true it flies to you ♪

234
00:17:59,735 --> 00:18:03,531
♪ my dear old galway bay

235
00:18:17,251 --> 00:18:19,640
{pub}Hang him!

236
00:18:25,777 --> 00:18:27,648
They are burning me
at the stake out there

237
00:18:27,814 --> 00:18:30,109
and I had to come and find you?!

238
00:18:31,732 --> 00:18:33,693
Thank you.

239
00:18:33,818 --> 00:18:36,219
Yes, ma'am.

240
00:18:46,364 --> 00:18:50,076
What are they demanding?

241
00:18:50,201 --> 00:18:52,006
They want protection.

242
00:18:52,172 --> 00:18:53,988
Well, we can't guarantee their safety

243
00:18:54,113 --> 00:18:55,665
against the Sioux.
It's a hazard of the job.

244
00:18:55,790 --> 00:18:58,167
More beans.

245
00:18:58,292 --> 00:18:59,952
And the freedmen?

246
00:19:00,077 --> 00:19:02,224
They want to be armed.

247
00:19:02,390 --> 00:19:04,957
Give rifles to an army of ex-slaves?

248
00:19:05,082 --> 00:19:07,257
No, no.
I won't allow it.

249
00:19:07,382 --> 00:19:10,024
- That's what I told them.
- This is why we change the route.

250
00:19:10,190 --> 00:19:11,481
Avoid sacred land.

251
00:19:11,606 --> 00:19:13,069
Win the men back

252
00:19:13,235 --> 00:19:14,567
by taking them out of harm's way.

253
00:19:14,692 --> 00:19:17,198
You do that,
you'll lose this railroad.

254
00:19:17,364 --> 00:19:19,283
At least get
the freedmen back out there.

255
00:19:19,449 --> 00:19:21,023
They'll do what we tell them to do.

256
00:19:21,148 --> 00:19:22,953
All evidence to the contrary.

257
00:19:23,119 --> 00:19:25,828
Those Negroes...

258
00:19:25,953 --> 00:19:28,209
Are the property of this railroad,

259
00:19:28,375 --> 00:19:29,449
Mr. Bohannon.

260
00:19:29,574 --> 00:19:31,087
Property?

261
00:19:31,253 --> 00:19:34,153
Yes.
Most of the freedmen are criminals

262
00:19:34,278 --> 00:19:36,258
that we bought their sentences

263
00:19:36,424 --> 00:19:38,928
from the states where
they were serving time.

264
00:19:39,094 --> 00:19:40,510
Bought them, bought me,
it's all beside the point.

265
00:19:40,635 --> 00:19:42,098
They do not make demands.

266
00:19:42,264 --> 00:19:44,680
I told you when you hired me,
this was a war.

267
00:19:44,805 --> 00:19:49,685
Yes. And I expected you
to have a battle plan.

268
00:19:49,810 --> 00:19:51,187
Well?!

269
00:19:51,312 --> 00:19:53,940
Let me be clear.

270
00:19:54,065 --> 00:19:55,892
I can't do anything

271
00:19:56,017 --> 00:19:58,072
without your complete authority.

272
00:20:03,024 --> 00:20:05,913
This is yours to deal with.

273
00:20:06,079 --> 00:20:10,042
Then, yeah.
I've got a plan.

274
00:20:14,368 --> 00:20:18,121
Hear you're holding
that railroad hostage, huh?

275
00:20:19,590 --> 00:20:23,052
He'll use you as a sacrificial
lamb for them Injuns.

276
00:20:23,177 --> 00:20:25,304
You worry about your people.

277
00:20:25,429 --> 00:20:28,508
And I'm going to worry about mine.

278
00:20:28,633 --> 00:20:32,895
Boys get themself killed
playing with guns.

279
00:20:33,020 --> 00:20:36,432
They ain't never
going to see us as equals

280
00:20:36,557 --> 00:20:38,696
till we holding the weapons they got.

281
00:20:38,862 --> 00:20:41,907
Ohh.

282
00:20:45,076 --> 00:20:47,326
I suppose everybody looks the same

283
00:20:47,451 --> 00:20:49,278
once the maggots get to 'em.

284
00:20:49,403 --> 00:20:52,355
Mm-hmm.

285
00:21:27,942 --> 00:21:31,287
You'd be a vision
even if your whole body

286
00:21:31,412 --> 00:21:33,580
were covered in them things.

287
00:21:34,949 --> 00:21:38,110
Your beauty could never
be hidden from my eyes.

288
00:21:38,235 --> 00:21:41,080
Are you drunk, Mr. Toole?

289
00:21:41,205 --> 00:21:42,632
How did you know?

290
00:21:42,757 --> 00:21:49,255
You always wax poetic
when you got a noseful.

291
00:21:49,380 --> 00:21:50,643
Chitterlings again?

292
00:21:51,048 --> 00:21:52,686
Yeah.

293
00:21:52,852 --> 00:21:54,510
I had a hanking for them.

294
00:21:54,635 --> 00:21:56,512
Just can't seem to get enough.

295
00:21:56,637 --> 00:21:58,481
They're fattening you up a bit.

296
00:21:58,606 --> 00:22:01,467
I like me a woman
with a bit of girth.

297
00:22:01,592 --> 00:22:06,322
Oh, it's... it's strange
me wanting to eat so much

298
00:22:06,447 --> 00:22:08,974
with me being so sick
in the mornings of late.

299
00:22:09,099 --> 00:22:11,360
I'm sure it'll pass.

300
00:22:11,485 --> 00:22:13,540
Give us a kiss, woman.

301
00:22:14,405 --> 00:22:16,573
Mmm.

302
00:22:17,658 --> 00:22:21,587
Mr. Toole...

303
00:22:21,712 --> 00:22:23,592
I'm with child.

304
00:22:28,719 --> 00:22:30,557
And it ain't yours.

305
00:22:34,124 --> 00:22:36,636
I can't...

306
00:22:36,761 --> 00:22:38,837
I can't lie to you.

307
00:22:38,962 --> 00:22:43,192
You've been one of the most
halfway decent men I ever known,

308
00:22:43,317 --> 00:22:48,521
but this... this child
belongs to another.

309
00:23:01,952 --> 00:23:03,966
Does this mean you're leaving me?

310
00:23:21,941 --> 00:23:25,174
Seems you've got your work
cut out for you, boss man.

311
00:23:25,445 --> 00:23:28,187
It ain't nothing I can't handle.

312
00:23:28,312 --> 00:23:32,274
Can you send a message
to the council bluffs office?

313
00:23:32,399 --> 00:23:36,078
"Labor negotiations stalled." Stop.

314
00:23:36,203 --> 00:23:39,582
"Work halted." Stop.

315
00:23:39,707 --> 00:23:42,618
"Send 200 replacement workers

316
00:23:42,743 --> 00:23:44,737
on the next available train."

317
00:23:44,862 --> 00:23:47,373
Stop.
Got that?

318
00:23:47,498 --> 00:23:48,457
All right.

319
00:23:48,582 --> 00:23:50,554
There's gonna be bloodshed.

320
00:23:50,720 --> 00:23:51,677
What you care?

321
00:23:51,802 --> 00:23:54,346
You ain't no Freedman.

322
00:23:54,471 --> 00:23:56,810
You sure as hell
ain't Irish or German.

323
00:23:56,976 --> 00:23:59,897
I ain't cleaning up after your mess.

324
00:24:00,063 --> 00:24:04,523
Nah. You'll just do whatever
Mr. Durant wants you to.

325
00:24:04,648 --> 00:24:06,987
Won't you?

326
00:24:20,898 --> 00:24:25,444
There is a storm brewing in town.

327
00:24:25,569 --> 00:24:28,798
Oh, that's... that's a labor dispute

328
00:24:28,923 --> 00:24:31,500
it appears to me.

329
00:24:31,625 --> 00:24:33,636
Oh, more blood.

330
00:24:33,761 --> 00:24:36,422
More bodies.

331
00:24:36,547 --> 00:24:38,957
More coffins.

332
00:24:39,082 --> 00:24:41,313
Heh heh heh.

333
00:24:41,479 --> 00:24:43,941
Our jobs have never
been more secure. Hmm?

334
00:24:48,319 --> 00:24:52,574
Can I tell you a...
A secret, Reverend?

335
00:24:57,618 --> 00:25:00,696
War is coming.

336
00:25:00,821 --> 00:25:03,001
Yet again.

337
00:25:03,167 --> 00:25:08,153
And not this business of
workers sitting down on the job.

338
00:25:08,278 --> 00:25:12,124
A real war.

339
00:25:12,249 --> 00:25:15,044
Ahh!

340
00:25:15,169 --> 00:25:17,663
Now is the time...

341
00:25:17,788 --> 00:25:21,925
To see things clear.

342
00:25:24,845 --> 00:25:29,945
We must decide...

343
00:25:30,111 --> 00:25:32,812
Which side we are on.

344
00:25:32,937 --> 00:25:35,521
Hmm?

345
00:26:04,687 --> 00:26:06,190
Where do you think you're going?

346
00:26:06,356 --> 00:26:07,179
Out of me way.

347
00:26:07,304 --> 00:26:09,151
I'm gonna tell you this once.

348
00:26:10,691 --> 00:26:13,435
You lay a hand on her,

349
00:26:13,560 --> 00:26:15,311
I'm gonna kill you.

350
00:26:17,731 --> 00:26:20,242
Oh, you can thrash me?

351
00:26:20,367 --> 00:26:23,529
Fine.

352
00:26:23,654 --> 00:26:25,872
You're not a real man.

353
00:26:30,911 --> 00:26:33,572
Just so we're clear.

354
00:26:33,697 --> 00:26:39,168
Puffed up. Big gun.
You can go to hell.

355
00:26:40,704 --> 00:26:43,599
What kind of man are you?

356
00:26:43,724 --> 00:26:46,385
Most of us would give
our life to be a father,

357
00:26:46,510 --> 00:26:49,388
but look at you, huh?

358
00:26:49,513 --> 00:26:51,993
You didn't even stay
by her side before.

359
00:26:52,159 --> 00:26:54,226
And where are you now?

360
00:26:54,351 --> 00:27:01,328
In the middle of town flexing
your damn trigger finger.

361
00:27:08,615 --> 00:27:11,284
You're no father.

362
00:27:11,587 --> 00:27:14,550
You're just a coward.

363
00:27:19,426 --> 00:27:22,140
Good night, then, yellow belly.

364
00:27:48,799 --> 00:27:51,003
{pub}Reverend?

365
00:27:54,191 --> 00:27:57,512
You... you got any socks you want done?

366
00:28:01,913 --> 00:28:04,738
Why don't you go to
the starlight, huh?

367
00:28:04,863 --> 00:28:06,163
Get us a bottle.

368
00:28:10,521 --> 00:28:13,132
What's a train doing here
so late at night?

369
00:28:13,257 --> 00:28:14,851
Steel workers.

370
00:28:14,976 --> 00:28:18,104
Coming in to take over for
thems who won't go out.

371
00:28:18,229 --> 00:28:19,856
You're replacing the men?

372
00:28:19,981 --> 00:28:22,284
Laying rail ain't
no scholarly business.

373
00:28:22,450 --> 00:28:24,193
If the men don't want to work,

374
00:28:24,318 --> 00:28:26,362
there's hundreds more
where they came from.

375
00:28:26,487 --> 00:28:28,331
Now, you get us a bottle.

376
00:28:28,456 --> 00:28:30,206
Whatever's left,
you keep.

377
00:29:08,452 --> 00:29:10,123
You enjoying your view?

378
00:29:11,649 --> 00:29:14,044
Matter of fact, I ain't.

379
00:29:19,382 --> 00:29:20,659
Let's go!

380
00:29:40,194 --> 00:29:42,772
The men think they
stepping off that train

381
00:29:42,897 --> 00:29:44,440
and get jobs.

382
00:29:44,565 --> 00:29:46,326
If they can fight,
they might just have them.

383
00:30:27,224 --> 00:30:29,244
Boys are getting their ass kicked.

384
00:30:33,674 --> 00:30:35,075
Let's get them, boys!

385
00:31:23,497 --> 00:31:25,328
Don't do it.
Don't do it.

386
00:31:55,613 --> 00:31:58,834
You tell anybody else trying
to come out here what happened.

387
00:31:59,000 --> 00:32:00,025
You hear me?

388
00:32:02,036 --> 00:32:03,453
Get out of here!

389
00:32:03,754 --> 00:32:05,331
Go! Go!

390
00:32:05,456 --> 00:32:06,917
Go back where you came from!

391
00:32:24,841 --> 00:32:27,186
Let me see.

392
00:32:27,311 --> 00:32:29,572
What you got?

393
00:32:39,490 --> 00:32:41,900
We're willing to go back to work,

394
00:32:42,025 --> 00:32:44,336
under one condition.

395
00:32:44,461 --> 00:32:46,956
Maybe you don't understand.

396
00:32:47,081 --> 00:32:49,458
I could have another trainload
of workers here tomorrow.

397
00:32:49,583 --> 00:32:51,386
We understand.

398
00:32:54,422 --> 00:32:56,376
If the Negroes are
willing to put themselves

399
00:32:56,501 --> 00:32:59,268
between us and the Sioux,

400
00:32:59,393 --> 00:33:01,438
we're OK with them having guns.

401
00:33:05,900 --> 00:33:07,883
Hmm.

402
00:33:11,438 --> 00:33:13,856
Go get yourself some rest, Mr. Toole.

403
00:33:23,901 --> 00:33:26,445
Proud of yourself?

404
00:33:26,570 --> 00:33:28,498
Pride ain't got nothing
to do with it.

405
00:33:28,623 --> 00:33:31,092
So that was the plan?

406
00:33:31,258 --> 00:33:33,619
Now everybody's supposed to
get along happy ever after?

407
00:33:33,744 --> 00:33:36,622
Not quite yet.

408
00:33:38,365 --> 00:33:39,893
Now you need to get your
people back to work.

409
00:33:40,584 --> 00:33:42,995
Ain't no sense having a horse

410
00:33:43,120 --> 00:33:44,830
unless it's broke enough
so you can ride it.

411
00:33:44,955 --> 00:33:47,884
Except we ain't talking about horses.

412
00:33:48,009 --> 00:33:49,277
We talking about men.

413
00:33:49,443 --> 00:33:53,005
Same principle.

414
00:33:53,130 --> 00:33:55,674
You work for Mr. Durant now.

415
00:33:55,799 --> 00:33:57,183
Same as me.

416
00:33:59,303 --> 00:34:02,582
That means we're
the ones doing the riding.

417
00:34:31,152 --> 00:34:32,445
{pub}Y'all got a choice.

418
00:34:32,570 --> 00:34:35,065
You either get back to work...

419
00:34:35,190 --> 00:34:36,700
Or go back to prison.

420
00:34:36,866 --> 00:34:38,827
Says who?

421
00:34:40,445 --> 00:34:41,788
Boss man.

422
00:34:43,398 --> 00:34:46,626
Then we need to hear it
from the boss man.

423
00:34:46,792 --> 00:34:48,169
You're hearing it
just as good from me.

424
00:34:48,335 --> 00:34:51,498
I ain't hearing nothing
but some noise...

425
00:34:51,623 --> 00:34:53,883
Coming from the boss man's flunky.

426
00:34:56,461 --> 00:34:57,512
Look at you.

427
00:34:59,243 --> 00:35:00,306
What you've become.

428
00:35:04,269 --> 00:35:06,513
Get your ass up, boy!

429
00:35:08,890 --> 00:35:10,974
Get him, son.
Get him!

430
00:35:13,277 --> 00:35:20,283
♪

431
00:35:26,074 --> 00:35:28,043
Aah!

432
00:35:33,882 --> 00:35:36,760
Don't know if you're black,
don't know if you're white.

433
00:35:36,885 --> 00:35:38,962
You're nothing.
You hear me?

434
00:35:39,087 --> 00:35:40,330
You're nothing.

435
00:36:04,453 --> 00:36:05,955
You ain't nothing.

436
00:36:06,080 --> 00:36:08,374
You ain't nothing.

437
00:36:08,540 --> 00:36:10,085
Get your ass up.

438
00:36:10,251 --> 00:36:12,286
Get your ass up, boy.

439
00:36:18,676 --> 00:36:22,713
Aah!

440
00:36:23,548 --> 00:36:25,183
You bit me!

441
00:36:25,349 --> 00:36:26,643
You bit me!

442
00:36:49,324 --> 00:36:51,835
You're still my nigger.

443
00:37:16,568 --> 00:37:17,944
Strike's over.

444
00:37:19,604 --> 00:37:21,114
They gave in?

445
00:37:21,280 --> 00:37:24,434
If the freedmen agree
to go back out, no conditions,

446
00:37:24,559 --> 00:37:27,620
when the men sober up,
we'll give the Negroes rifles

447
00:37:27,745 --> 00:37:29,038
and send everybody
back out to the cut.

448
00:37:29,204 --> 00:37:30,273
But you said yourself

449
00:37:30,398 --> 00:37:31,749
we can't capitulate to their demands.

450
00:37:31,915 --> 00:37:33,585
Point is, we ain't.

451
00:37:33,751 --> 00:37:35,295
I am not arming the freedmen.

452
00:37:35,461 --> 00:37:37,505
You want this railroad built,
somebody's got to patrol that land.

453
00:37:37,755 --> 00:37:40,925
Now, you gonna let me do this or not?

454
00:37:42,760 --> 00:37:45,455
Well, if they want
to be Cannon fodder,

455
00:37:45,580 --> 00:37:46,890
hmm, so be it.

456
00:37:47,941 --> 00:37:50,310
But we have lost two days and 5 Miles.

457
00:37:50,476 --> 00:37:53,796
Those men fought to
the death for their jobs.

458
00:37:53,921 --> 00:37:55,899
We'll make up those
5 Miles in no times.

459
00:37:56,065 --> 00:37:59,194
So that's it.
We move forward.

460
00:37:59,360 --> 00:38:01,112
At what cost?

461
00:38:01,278 --> 00:38:02,989
You're the one said
building this railroad

462
00:38:03,114 --> 00:38:03,990
is the only thing that matters.

463
00:38:04,156 --> 00:38:06,276
Being smart and rerouting

464
00:38:06,401 --> 00:38:08,620
is not the same as being weak.

465
00:38:08,786 --> 00:38:11,614
You and me's gonna have to
agree to disagree.

466
00:38:11,739 --> 00:38:15,418
Yes. Because clearly it always
has to be on your terms,

467
00:38:15,543 --> 00:38:18,621
doesn't it?

468
00:38:18,746 --> 00:38:21,090
It don't work...

469
00:38:21,215 --> 00:38:24,134
If there's any question about that.

470
00:38:28,172 --> 00:38:30,725
Got yourself a real workforce now.

471
00:38:47,510 --> 00:38:48,761
Mrs. Toole,

472
00:38:52,332 --> 00:38:54,666
your husband has returned home.

473
00:39:47,259 --> 00:39:51,047
Reverend, you leaving me?

474
00:39:51,172 --> 00:39:54,601
Well, I see you've
bent the men to your will.

475
00:39:54,726 --> 00:39:56,811
I suppose I played a part in that.

476
00:39:56,977 --> 00:39:59,522
Yeah.

477
00:39:59,688 --> 00:40:02,108
Yeah.

478
00:40:13,244 --> 00:40:15,038
Is it true?

479
00:40:18,116 --> 00:40:21,067
I mean, do you believe
you're at war with the Sioux?

480
00:40:27,925 --> 00:40:30,511
Yes, sir, I do.

481
00:40:32,913 --> 00:40:35,099
And now you have the men
to do the job.

482
00:40:38,853 --> 00:40:40,772
I can't stay with you.

483
00:40:42,974 --> 00:40:45,109
I respect that.

484
00:40:57,996 --> 00:40:59,365
Rifle in your left hand,
cartridge in your right.

485
00:40:59,490 --> 00:41:01,334
Join the line.
Arms distance, please.

486
00:41:01,459 --> 00:41:03,653
You think this is unwise?

487
00:41:03,778 --> 00:41:05,505
Probably.

488
00:41:05,630 --> 00:41:08,341
But this is...

489
00:41:08,507 --> 00:41:11,010
Barrels down, gentlemen.

490
00:41:11,176 --> 00:41:13,388
But a necessary evil, Mr. Toole.

491
00:41:13,554 --> 00:41:16,724
You're speaking of
yourself then, Mr. Bohannon.

492
00:41:35,309 --> 00:41:36,619
Rifle in your left hand.

493
00:41:36,785 --> 00:41:38,829
Cartridges in your right.

494
00:41:43,375 --> 00:41:44,919
Congratulations.

495
00:41:49,824 --> 00:41:52,385
All right, gentlemen.
The rest of y'all,

496
00:41:52,551 --> 00:41:54,387
rifles in your left hand,
barrels facing the sky.

497
00:41:54,512 --> 00:41:57,327
Finger off the trigger.
Take one cartridge in your right hand.

498
00:41:57,452 --> 00:41:58,571
Sharp end is the business end.

499
00:41:58,696 --> 00:42:00,093
Load that cartridge
into the cartridge port

500
00:42:00,218 --> 00:42:01,711
on the side of your rifle,
business end first.

501
00:42:01,836 --> 00:42:02,878
Now, on the order to make ready,

502
00:42:03,003 --> 00:42:04,564
rack that lever all the way forward

503
00:42:04,855 --> 00:42:06,516
and all the way back,
loading the round into the chamber.

504
00:42:06,641 --> 00:42:08,718
And... make ready!

505
00:42:11,813 --> 00:42:13,773
You dropped your round.

506
00:42:13,898 --> 00:42:15,158
Pick it up.

507
00:42:16,317 --> 00:42:17,410
Start again.

508
00:42:17,576 --> 00:42:19,445
And... make ready!

509
00:42:32,717 --> 00:42:35,128
Boss man coming through.
Hold your fire.

510
00:42:35,253 --> 00:42:36,721
Boss man coming through!

511
00:42:43,210 --> 00:42:45,438
Anything moves...

512
00:42:45,604 --> 00:42:47,982
Shoot the shit out of it.

513
00:42:48,148 --> 00:42:50,109
<font color="#3399FF">Sync and correction by Mlmlte</font>
<font color="#3399FF">www.addic7ed.com</font>

