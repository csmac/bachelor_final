﻿1
00:00:00,000 --> 00:00:03,550
<i>Timing and subtitles brought to you by
Nightmare Teacher Team @ Viki</i>

2
00:00:09,030 --> 00:00:16,020
<i> Nightmare Teacher</i>
Episode 11

3
00:00:20,410 --> 00:00:24,890
<i>Nightmare Teacher</i>

4
00:00:28,880 --> 00:00:31,080
<i>Yosan High School</i>

5
00:00:49,950 --> 00:00:52,120
<i>It's time to wake up.</i>

6
00:01:05,130 --> 00:01:07,100
It was dark

7
00:01:08,170 --> 00:01:09,960
dark?

8
00:01:09,960 --> 00:01:14,400
An endless darkness in which
you don't know when it ends.

9
00:01:15,790 --> 00:01:19,780
Hurt, jealous, angry that will never end.

10
00:01:20,470 --> 00:01:22,560
Those kinds of feelings

11
00:01:25,010 --> 00:01:28,430
And I saw Teacher Bong Gu.

12
00:01:32,450 --> 00:01:34,920
I'm now remembering the
kids who disappeared,

13
00:01:36,120 --> 00:01:38,400
and why they did.

14
00:01:44,550 --> 00:01:48,380
A few days ago, I posted the names and photos
of the disappeared kids on the school feed,

15
00:01:48,380 --> 00:01:50,750
and someone who knows them appeared.

16
00:01:54,000 --> 00:01:56,510
<i>If you're curious, go to
special class at 5th floor.</i>

17
00:01:57,360 --> 00:02:01,820
Is there a special class in our school?
Why haven't I heard about it even once?

18
00:02:01,820 --> 00:02:06,010
If there was a special class I would
have already heard about it, right?

19
00:02:06,010 --> 00:02:09,530
Something's fishy. Gives me a bad feeling.

20
00:02:15,640 --> 00:02:17,530
<i>Have you arrived?</i>

21
00:02:17,530 --> 00:02:20,160
<i>I have, but what class
were you in second year?</i>

22
00:02:20,160 --> 00:02:22,100
<i>Class 3.</i>

23
00:02:22,100 --> 00:02:24,210
<i>Wow... You are in the same class with us?</i>

24
00:02:24,210 --> 00:02:25,610
<i> I never said I was a kid.</i>

25
00:02:25,610 --> 00:02:28,580
<i>What?</i>

26
00:02:38,840 --> 00:02:40,940
<i>Special Class</i>

27
00:02:49,940 --> 00:02:51,140
Those kids..

28
00:02:51,140 --> 00:02:53,750
They're the kids we saw in the photo

29
00:02:57,850 --> 00:02:59,800
<i>Well...</i>

30
00:02:59,800 --> 00:03:01,820
Is there anything wrong?

31
00:03:03,840 --> 00:03:05,930
<i>Hey, you guys...!</i>

32
00:03:05,930 --> 00:03:11,900
You guys made that weird feed or website or
whatever and threw off the school's atmosphere.

33
00:03:11,900 --> 00:03:13,560
I'm sorry.

34
00:03:13,560 --> 00:03:15,910
It wasn't done with bad intentions.

35
00:03:15,910 --> 00:03:18,190
If our Teacher Bong Gu didn't tell us,

36
00:03:18,190 --> 00:03:21,340
we wouldn't have known there
was a such a website.

37
00:03:22,030 --> 00:03:24,920
Hey. And you, Kang Ye Rim.

38
00:03:24,920 --> 00:03:28,110
Why are you hanging around with this kid?

39
00:03:28,110 --> 00:03:32,740
Ye Rim is... I mean, the Class
President has done nothing wrong.

40
00:03:32,740 --> 00:03:37,270
I was the one who dragged her into this and
I'm also the one who made the website.

41
00:03:47,680 --> 00:03:51,910
Let's just let them off the
hook this time, Teacher Kim.

42
00:03:51,910 --> 00:03:55,770
Teacher Han, I think you
are like a real angel.

43
00:03:56,690 --> 00:04:00,950
Kang Ye Rim, Seo Sang Woo. You
guys won't do this again.

44
00:04:00,950 --> 00:04:03,080
Also close that website.

45
00:04:03,080 --> 00:04:04,970
Okay

46
00:04:04,970 --> 00:04:08,770
But, since we can't just let this go past,

47
00:04:09,750 --> 00:04:12,700
how about we end this with you guys
writing a reflection of a few sentences?

48
00:04:13,500 --> 00:04:15,320
Thank you

49
00:04:53,230 --> 00:04:55,030
Kang Yerim

50
00:04:56,330 --> 00:05:00,030
Hey! Can't you hear?

51
00:05:08,050 --> 00:05:10,490
Don't you feel tired?

52
00:05:10,490 --> 00:05:14,930
<i>To pretend to be kind and
live as a model student?</i>

53
00:05:18,450 --> 00:05:23,300
Don't be surprised

54
00:05:38,480 --> 00:05:40,720
<i>Special Class</i>

55
00:05:56,580 --> 00:05:58,620
<i>Kids...</i>

56
00:06:21,460 --> 00:06:23,550
Why are you here?

57
00:06:25,520 --> 00:06:28,190
The kids that were sitting here yesterday,

58
00:06:29,550 --> 00:06:31,540
where did they all go?

59
00:06:32,480 --> 00:06:37,000
Isn't it time for them all to go home?

60
00:06:38,160 --> 00:06:42,630
Those kids... they aren't real.

61
00:06:51,940 --> 00:06:53,690
You mean like you?

62
00:06:54,400 --> 00:06:56,080
Huh?

63
00:06:56,080 --> 00:07:01,090
Kang Ye Rim... you also aren't
being seen as the real you.

64
00:07:01,960 --> 00:07:04,190
What are you teacher?

65
00:07:05,400 --> 00:07:07,460
Just who in the hell are you?

66
00:07:08,960 --> 00:07:15,370
That's what I wanted to say. Who am I?

67
00:07:26,390 --> 00:07:29,700
What could be your reason
for looking for those kids?

68
00:07:30,440 --> 00:07:34,130
Simply curiosity?

69
00:07:34,130 --> 00:07:38,150
Or responsibility as the Class President?

70
00:07:42,040 --> 00:07:45,410
Since we are classmates.

71
00:07:45,410 --> 00:07:49,340
Is that responsibility?

72
00:07:50,060 --> 00:07:54,900
Kang Ye Rim... I feel that you have
a different voice in your head.

73
00:07:54,900 --> 00:08:00,700
Those actions you're doing
right now seem pretentious.

74
00:08:03,010 --> 00:08:05,430
Is it a "kind-kid" complex?

75
00:08:08,270 --> 00:08:10,570
<i>Get out!</i>

76
00:08:11,530 --> 00:08:14,740
<i>But still no matter how much I want to
talk to him, he might not be available.</i>

77
00:08:14,740 --> 00:08:17,960
This is 881...

78
00:08:22,940 --> 00:08:28,280
I believe that I have a sense of
responsibility over those kids.

79
00:08:28,280 --> 00:08:34,050
The kids chose what they wanted.

80
00:08:34,050 --> 00:08:38,910
Also, they just received the results.

81
00:08:38,910 --> 00:08:43,340
But... what about you?

82
00:08:44,580 --> 00:08:45,480
I...

83
00:08:45,480 --> 00:08:50,140
Kang Ye Rim, what do you really want?

84
00:08:50,140 --> 00:08:55,110
I'm talking about the reason
you want to find those kids.

85
00:09:02,790 --> 00:09:07,500
<i>I am sick of it.</i>

86
00:09:20,440 --> 00:09:22,700
I don't know.

87
00:09:22,700 --> 00:09:25,460
Isn't that why there's something
called a consultation?

88
00:09:28,290 --> 00:09:30,010
The Mirror.

89
00:09:33,810 --> 00:09:39,290
You don't know if you can find what
you're looking for in the mirror.

90
00:09:39,290 --> 00:09:45,960
Also, just in case you're lucky
and find what you're looking for

91
00:09:45,960 --> 00:09:50,520
and get the kids back...

92
00:09:55,130 --> 00:09:58,380
You just have to be back before
all of the sand falls down.

93
00:09:58,380 --> 00:10:00,190
What if I don't?

94
00:10:00,190 --> 00:10:02,580
Then you'll just stay there.

95
00:10:04,010 --> 00:10:05,930
Make your decision.

96
00:10:07,870 --> 00:10:11,180
Will you go back into the mirror?

97
00:10:11,180 --> 00:10:15,760
Or forgetting everything and
going back to the classroom?

98
00:10:25,440 --> 00:10:26,990
I'll do it.

99
00:10:29,780 --> 00:10:33,530
Contract completed.

100
00:10:36,050 --> 00:10:40,950
Kang Ye Rim

101
00:11:24,090 --> 00:11:25,880
Guys

102
00:11:29,630 --> 00:11:31,530
Can't you see me?

103
00:11:32,990 --> 00:11:37,060
Guys! Can't you see here?

104
00:11:38,280 --> 00:11:42,900
Sang Woo. Seo Sang Woo, I'm here.

105
00:11:42,900 --> 00:11:45,740
Seo Sang Woo. I'm right here!

106
00:11:45,740 --> 00:11:50,220
Seo Sang Woo. Can't you see? Seo Sang...

107
00:11:50,220 --> 00:11:51,950
Sang Woo!

108
00:12:56,660 --> 00:12:58,520
Teacher Bong Gu.

109
00:13:02,340 --> 00:13:04,710
<i>Year 2 Class 3 </i>

110
00:13:04,780 --> 00:13:07,100
Ye Rim & Sang Woo.

111
00:13:54,640 --> 00:13:57,130
<i>Kang Ye Rim</i>

112
00:13:57,130 --> 00:13:58,780
You.

113
00:14:15,830 --> 00:14:23,730
If anything should happen to me

114
00:14:25,940 --> 00:14:28,770
<i>What? Looks like Teacher isn't
doing a consultation today.</i>

115
00:14:28,770 --> 00:14:33,010
-I really wanted to have a consultation from him.
-Me too.

116
00:14:35,740 --> 00:14:39,930
Ye Rim! Ye Rim!

117
00:14:39,930 --> 00:14:41,850
Did you see Ye Rim?

118
00:14:42,760 --> 00:14:44,400
Where could she be?

119
00:14:44,400 --> 00:14:47,810
Ah, Sang Woo. I heard that Ye
Rim received the consultation.

120
00:14:47,810 --> 00:14:50,080
- Really? 
- Yes.

121
00:14:50,080 --> 00:14:52,770
Where are you Kang Yerim?

122
00:14:52,770 --> 00:14:54,580
Seo Sang Woo.

123
00:14:57,800 --> 00:15:00,140
Did you miss me?

124
00:15:14,430 --> 00:15:16,790
Are alone here?

125
00:15:17,390 --> 00:15:23,160
No. The unnies and oppas in the classroom

126
00:15:23,160 --> 00:15:25,350
are scary.

127
00:15:25,350 --> 00:15:27,550
Where is that place?

128
00:15:42,530 --> 00:15:48,010
I don't know where this place is.

129
00:16:31,720 --> 00:16:33,590
What should I do?

130
00:16:37,190 --> 00:16:41,020
Unni, I see you every day.

131
00:16:41,020 --> 00:16:42,010
Me?

132
00:16:42,010 --> 00:16:44,250
I saw it everyday

133
00:16:44,250 --> 00:16:46,940
You looking into the mirror.

134
00:16:47,710 --> 00:16:50,760
You looked sad

135
00:16:50,760 --> 00:16:55,320
I thought that it would be nice 
if you could live happily

136
00:16:56,440 --> 00:17:02,520
Living doing whatever you want

137
00:17:06,210 --> 00:17:10,240
No. I am living happily

138
00:17:11,300 --> 00:17:13,030
Lies.

139
00:17:13,030 --> 00:17:15,370
You're not having one bit of fun.

140
00:17:15,370 --> 00:17:17,410
It's like that

141
00:17:17,410 --> 00:17:20,250
Unni, why do you study?

142
00:17:24,290 --> 00:17:28,430
Why do I study? Because I want
to go to a good college and--

143
00:17:28,430 --> 00:17:33,310
Then? What are you going
to be when you grow up?

144
00:17:39,660 --> 00:17:41,880
What do you mean what would I be?

145
00:17:42,630 --> 00:17:47,410
You're a coward. You're
wary of other people.

146
00:17:48,200 --> 00:17:50,960
You don't even know what you're
going to be when you grow up.

147
00:17:57,970 --> 00:18:00,180
Hey where are you going?

148
00:18:06,340 --> 00:18:08,340
Ah. I like it

149
00:18:09,750 --> 00:18:17,860
Okay, okay. Wife, I get that it's good, but
I do think it's better if you come down.

150
00:18:17,860 --> 00:18:19,550
Sang Woo.

151
00:18:21,030 --> 00:18:24,210
If you were going to die
tomorrow, what would you do?

152
00:18:24,960 --> 00:18:26,520
What?

153
00:18:27,150 --> 00:18:31,850
Mr. Seo Sang Woo, if you were going to
die tomorrow, what would you like to do?

154
00:18:34,370 --> 00:18:38,540
I'm not sure. What would I do?

155
00:18:38,540 --> 00:18:42,870
Ditch class and hang out with you all day?

156
00:18:44,480 --> 00:18:49,060
Then what about you Yerim?

157
00:19:03,790 --> 00:19:05,480
Pretty

158
00:19:09,580 --> 00:19:12,080
What this, Kang Yerim

159
00:19:27,800 --> 00:19:28,970
Me?

160
00:19:30,460 --> 00:19:32,760
Ye..Yerim

161
00:19:49,830 --> 00:19:53,730
What you wanted to do was this?

162
00:19:55,400 --> 00:19:59,690
There's a lot more than this, but
let's just enjoy what's now.

163
00:20:03,300 --> 00:20:06,330
Hey let's go together

164
00:20:09,470 --> 00:20:15,200
There... the scary unnies and oppas.

165
00:20:15,200 --> 00:20:16,740
Guys.

166
00:20:22,610 --> 00:20:30,430
<i>Timing and subtitles brought to you by
Nightmare Teacher Team @ Viki</i>

