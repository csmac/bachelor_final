﻿1
00:00:43,030 --> 00:00:46,469
Them

2
00:00:48,750 --> 00:00:52,189
This story is based on real events.

3
00:00:52,870 --> 00:00:58,069
Snagov, Romania
October 6th, 2002, 11:47pm.

4
00:00:59,630 --> 00:01:01,269
Hello.

5
00:01:01,270 --> 00:01:03,069
Yes, I picked her up.

6
00:01:03,070 --> 00:01:04,989
I know, I know.

7
00:01:04,990 --> 00:01:07,469
Talk to her yourself.

8
00:01:07,470 --> 00:01:10,949
Daddy wants to talk.
Quick, it's expensive.

9
00:01:12,150 --> 00:01:14,709
- Sanda?
- Can't you see I'm busy?

10
00:01:14,710 --> 00:01:16,549
Take this phone!

11
00:01:16,550 --> 00:01:18,549
He can call back.

12
00:01:18,550 --> 00:01:20,269
Take the phone, I swear!

13
00:01:20,270 --> 00:01:22,549
What? You'll punish me?

14
00:01:22,550 --> 00:01:25,949
Call us at home.
In a half an hour.

15
00:01:29,510 --> 00:01:32,709
What's with you?
Why are you like this?

16
00:01:39,590 --> 00:01:41,829
You need help.

17
00:01:41,830 --> 00:01:43,869
Excuse me?

18
00:01:54,470 --> 00:01:56,869
Sanda, are you OK?

19
00:01:57,470 --> 00:02:00,189
- Are you OK?
- Yes.

20
00:02:20,550 --> 00:02:22,909
What are you doing?

21
00:02:23,510 --> 00:02:26,029
- I saw someone.
- What?

22
00:02:26,030 --> 00:02:29,829
- Someone was on the road!
- Probably a dog.

23
00:02:37,510 --> 00:02:39,589
Can't we go now?

24
00:02:58,310 --> 00:03:00,629
Super...

25
00:03:21,270 --> 00:03:23,549
Try the engine, please.

26
00:03:39,350 --> 00:03:40,909
Try it again.

27
00:03:50,510 --> 00:03:53,949
Mum, what do I do?

28
00:04:03,590 --> 00:04:04,490
Mum...

29
00:04:44,830 --> 00:04:45,730
Mum?

30
00:05:38,150 --> 00:05:39,549
Mum!

31
00:06:45,230 --> 00:06:48,230
'Police station, hold the line...'

32
00:07:20,710 --> 00:07:22,469
'Police station...'

33
00:07:30,190 --> 00:07:32,909
'Police station, hold the line...'

34
00:08:40,430 --> 00:08:42,749
"But he was joyful..."

35
00:08:42,750 --> 00:08:45,949
French Junior
High School of Bucharest

36
00:08:45,950 --> 00:08:48,949
October 7, 2002, 4:32pm.

37
00:08:52,110 --> 00:08:54,109
"Because"

38
00:08:54,110 --> 00:08:56,349
"he was... free."

39
00:08:58,710 --> 00:09:02,269
"Because he was... free."

40
00:09:05,950 --> 00:09:08,749
Full stop. Good?

41
00:09:08,750 --> 00:09:11,549
See you next week.

42
00:09:11,550 --> 00:09:12,450
Bye.

43
00:09:17,790 --> 00:09:21,669
- Bye.
- Bye.

44
00:09:21,670 --> 00:09:23,269
Bye.

45
00:09:32,630 --> 00:09:34,949
Clémentine.

46
00:09:37,830 --> 00:09:40,029
Here.

47
00:09:42,510 --> 00:09:44,589
My curriculum from last year.

48
00:09:44,590 --> 00:09:45,949
Thanks.

49
00:09:45,950 --> 00:09:49,949
I had a hard time finding it.

50
00:09:50,550 --> 00:09:53,149
Thanks so much.
It'll be a great help.

51
00:09:53,710 --> 00:09:55,789
Are they that bad?

52
00:09:55,790 --> 00:09:57,829
Not at all.

53
00:09:57,830 --> 00:10:00,349
I need some time to adjust.

54
00:10:01,510 --> 00:10:03,709
How long have you been here?

55
00:10:03,710 --> 00:10:05,669
Nearly three months.

56
00:10:05,670 --> 00:10:08,309
You're managing well.

57
00:10:08,950 --> 00:10:12,349
I'd love to speak your language
like you speak mine.

58
00:10:12,510 --> 00:10:16,989
- My French is a catastrophe.
- Don't exaggerate.

59
00:10:18,630 --> 00:10:20,709
Enjoy your weekend.

60
00:10:20,710 --> 00:10:23,909
- Thanks again. Bye.
- Bye.

61
00:11:57,710 --> 00:12:00,549
- 'Hello?'
- It's me.

62
00:12:00,550 --> 00:12:02,629
What do you feel like tonight?

63
00:12:02,630 --> 00:12:05,949
- 'You.'
- That's all?

64
00:12:05,950 --> 00:12:07,869
'I wouldn't say no to some wine.'

65
00:12:07,870 --> 00:12:10,509
Wine, me... Anything else?

66
00:12:10,510 --> 00:12:11,949
'Whatever you rummage up.

67
00:12:11,950 --> 00:12:14,709
- 'How long?'
- Half an hour.

68
00:12:14,710 --> 00:12:17,189
- 'See you soon.'
- Bye.

69
00:14:52,790 --> 00:14:54,989
Lucas?

70
00:15:43,590 --> 00:15:46,269
- How are you?
- Fine.

71
00:15:57,910 --> 00:16:00,309
- What is it?
- Nothing.

72
00:16:00,510 --> 00:16:02,949
What's so funny?

73
00:16:03,950 --> 00:16:07,029
- Hope it's productive, at least.
- So-so.

74
00:16:09,870 --> 00:16:11,709
And you?

75
00:16:11,710 --> 00:16:15,349
They were irritating today.
A dictation calmed them down.

76
00:16:15,350 --> 00:16:19,349
- I'm calling it quits!
- Already?

77
00:16:20,510 --> 00:16:21,869
Listen up,

78
00:16:21,870 --> 00:16:24,949
you know a writer
never stops working.

79
00:16:24,950 --> 00:16:28,589
Here, for example,
I turn off the computer...

80
00:16:28,590 --> 00:16:30,909
I close the lid...

81
00:16:31,550 --> 00:16:33,949
I stand up...

82
00:16:34,630 --> 00:16:37,189
but in my head, I'm still working.

83
00:16:38,950 --> 00:16:41,229
Are you coming?

84
00:16:47,550 --> 00:16:50,029
- And now?
- I'm working.

85
00:16:55,550 --> 00:16:57,989
- And now?
- Still working...

86
00:16:59,910 --> 00:17:02,949
- And now?
- You can keep trying.

87
00:17:03,830 --> 00:17:06,229
- And now?
- Extreme concentration!

88
00:17:11,670 --> 00:17:13,949
Shit!

89
00:17:19,910 --> 00:17:22,229
You lose.

90
00:17:26,750 --> 00:17:29,069
Can I come in?

91
00:17:42,910 --> 00:17:45,309
Are you still there?

92
00:17:57,230 --> 00:17:59,709
- Well?
- It's delicious.

93
00:17:59,710 --> 00:18:02,149
Really?

94
00:18:03,950 --> 00:18:08,789
It's unusual, but it's interesting.

95
00:18:08,790 --> 00:18:11,069
Unusual? How?

96
00:18:14,750 --> 00:18:17,069
Particular.

97
00:18:21,830 --> 00:18:24,189
What exactly is in it?

98
00:18:24,190 --> 00:18:26,549
Everything.

99
00:18:26,550 --> 00:18:28,629
What do you mean?

100
00:18:28,630 --> 00:18:30,669
I threw in all the groceries.

101
00:18:30,670 --> 00:18:32,829
That explains it.

102
00:18:32,830 --> 00:18:36,149
It's all-inclusive.
Starter, main dish, dessert...

103
00:18:36,150 --> 00:18:39,229
- From soup to nuts.
- You don't like it?

104
00:18:39,790 --> 00:18:42,589
- Yes, I do.
- If you don't like it...

105
00:18:42,590 --> 00:18:46,349
Fine, that's enough.
Let's clear the table.

106
00:18:48,030 --> 00:18:49,829
That's the way it goes.

107
00:18:49,830 --> 00:18:52,149
- We're finished.
- It's not bad...

108
00:18:52,150 --> 00:18:54,949
Get out.
You've hurt me enough.

109
00:19:02,870 --> 00:19:05,149
- Are you sleeping?
- No.

110
00:19:06,670 --> 00:19:08,629
Yes, you're sleeping.

111
00:19:08,630 --> 00:19:10,829
I'm not sleeping.

112
00:19:17,750 --> 00:19:20,709
- It looks bad.
- Really?

113
00:19:20,710 --> 00:19:23,829
Really bad.
I think she's cheating on him.

114
00:19:23,830 --> 00:19:26,029
That's unfortunate.

115
00:19:31,710 --> 00:19:34,949
- Want me to go?
- No, it's my turn.

116
00:19:41,550 --> 00:19:43,349
It's so annoying.

117
00:19:43,510 --> 00:19:47,149
- Are you going or am I?
- It's all right. I'll go.

118
00:19:55,750 --> 00:19:57,949
Doggy...

119
00:20:03,150 --> 00:20:05,109
Doggy!

120
00:20:20,510 --> 00:20:24,069
Here you are.
I thought you lost your appetite.

121
00:20:24,750 --> 00:20:27,629
Stop barking.
It disturbs everyone.

122
00:20:27,630 --> 00:20:29,349
Easy, easy.

123
00:20:29,950 --> 00:20:31,749
That's it. Nothing left.

124
00:20:31,750 --> 00:20:34,189
Go on. Get lost!

125
00:20:36,670 --> 00:20:38,909
Go.

126
00:20:50,110 --> 00:20:52,269
The beast has been subdued.

127
00:20:54,590 --> 00:20:56,909
Let's go to bed?

128
00:20:57,830 --> 00:21:01,109
Come on.
Enough of this crap.

129
00:21:02,590 --> 00:21:04,269
I'm exhausted.

130
00:21:05,630 --> 00:21:07,629
Are you sleepy, Mr Sleepy?

131
00:21:07,630 --> 00:21:09,669
Aren't you sleepy?

132
00:21:09,670 --> 00:21:11,629
- You're never sleepy.
- Never.

133
00:21:11,630 --> 00:21:14,109
- How do you do it?
- I don't know.

134
00:21:15,550 --> 00:21:17,949
- What's your secret?
- A little secret.

135
00:21:17,950 --> 00:21:20,149
Tell me.

136
00:21:46,590 --> 00:21:49,349
I'm going downstairs
to work a bit, OK?

137
00:22:11,630 --> 00:22:14,029
This is such trash!

138
00:22:49,430 --> 00:22:50,330
Hello?

139
00:22:57,550 --> 00:22:59,829
Who's on the line?

140
00:23:01,510 --> 00:23:03,069
Hello?

141
00:26:32,590 --> 00:26:33,490
Lucas?

142
00:26:39,510 --> 00:26:40,454
Wake up.

143
00:26:58,310 --> 00:27:01,909
- What's going on?
- Didn't you hear it?

144
00:27:02,870 --> 00:27:05,949
- What?
- The noise outside.

145
00:27:15,870 --> 00:27:19,349
- What are you doing?
- Come on. Let's go down and see.

146
00:27:37,110 --> 00:27:39,269
Are you coming?

147
00:27:54,710 --> 00:27:56,669
- What are you doing?
- Nothing.

148
00:27:56,670 --> 00:27:58,349
Come on.

149
00:28:20,830 --> 00:28:23,309
Why the hell is my car there?

150
00:28:28,590 --> 00:28:30,269
Come see.

151
00:28:32,910 --> 00:28:35,109
Do you see?

152
00:28:37,710 --> 00:28:40,109
Don't be silly. I'll just check.

153
00:28:40,710 --> 00:28:42,909
Stay there.

154
00:28:51,870 --> 00:28:54,269
Anyone there?

155
00:29:00,030 --> 00:29:02,149
Is anyone there?

156
00:29:09,390 --> 00:29:12,909
- Lucas.
- Stay there!

157
00:29:34,710 --> 00:29:36,909
Don't move!

158
00:29:36,910 --> 00:29:39,109
Lucas, get inside!

159
00:29:44,870 --> 00:29:48,309
Hey, hey!

160
00:29:49,510 --> 00:29:51,309
Come back!

161
00:29:55,510 --> 00:29:57,189
Stop!

162
00:30:01,310 --> 00:30:04,069
They came into my house.

163
00:30:04,790 --> 00:30:07,189
No! I don't have them.

164
00:30:08,550 --> 00:30:11,189
They're in the car!

165
00:30:11,590 --> 00:30:14,909
- You really don't have the papers?
- No.

166
00:30:15,510 --> 00:30:19,029
We don't have them.
Isn't there anything you can do?

167
00:30:19,030 --> 00:30:22,469
- We'll handle it tomorrow.
- What about the former owner's name?

168
00:30:23,750 --> 00:30:25,709
OK...

169
00:30:25,710 --> 00:30:28,349
I'll call back tomorrow.

170
00:30:29,790 --> 00:30:33,269
What a fool.
I left everything in the car.

171
00:30:52,510 --> 00:30:54,909
What are you doing?

172
00:30:55,910 --> 00:30:57,829
Come on.

173
00:30:57,830 --> 00:31:00,229
I'll switch the meter on.

174
00:31:19,270 --> 00:31:22,309
- I can't see anything.
- Wait!

175
00:31:27,750 --> 00:31:29,949
Come on.

176
00:31:37,870 --> 00:31:40,149
Wait for me!

177
00:32:00,590 --> 00:32:02,269
Shit.

178
00:32:16,670 --> 00:32:19,149
Hurry, come see.

179
00:32:19,950 --> 00:32:22,229
What is it?

180
00:32:28,670 --> 00:32:30,909
Look.

181
00:32:33,550 --> 00:32:36,069
- What is it?
- Get down!

182
00:32:41,670 --> 00:32:44,109
- Wait.
- Stay there!

183
00:32:49,550 --> 00:32:52,349
- I'm coming with you.
- Stay there.

184
00:33:03,830 --> 00:33:06,229
Do you see something?

185
00:33:08,510 --> 00:33:12,069
- Lucas?
- There's nothing.

186
00:33:18,950 --> 00:33:21,349
Shit! Work, damn it!

187
00:33:25,830 --> 00:33:28,029
Stay there.

188
00:33:44,950 --> 00:33:47,149
Come on!

189
00:34:03,950 --> 00:34:06,149
Look!

190
00:34:06,750 --> 00:34:09,149
Someone's there.

191
00:34:21,470 --> 00:34:24,949
- Do you see anything?
- Nothing.

192
00:34:33,630 --> 00:34:35,349
What is it?

193
00:35:39,590 --> 00:35:42,269
- What do we do?
- I don't know.

194
00:35:43,510 --> 00:35:45,909
We've got to call the police.

195
00:35:59,630 --> 00:36:01,909
It'll be OK.

196
00:36:02,910 --> 00:36:06,789
- They're inside.
- No.

197
00:36:06,790 --> 00:36:09,909
They're in the house.

198
00:36:16,870 --> 00:36:19,149
I'll go downstairs.

199
00:36:20,830 --> 00:36:23,109
I'll go and see.

200
00:36:50,950 --> 00:36:53,269
Close the door.

201
00:37:00,750 --> 00:37:03,109
Close it.

202
00:37:03,230 --> 00:37:05,029
Close it!

203
00:40:58,630 --> 00:41:00,869
Lucas!

204
00:41:00,870 --> 00:41:03,749
Lucas! What's wrong?

205
00:41:03,750 --> 00:41:06,949
Answer me.
What's happening?

206
00:41:48,750 --> 00:41:51,309
Clem, open up!

207
00:41:51,510 --> 00:41:52,829
Open the door!

208
00:41:52,830 --> 00:41:55,189
Clem! Open the door!

209
00:41:56,870 --> 00:41:58,749
Open the door!

210
00:41:58,750 --> 00:42:01,909
I can't. It's jammed!

211
00:42:10,590 --> 00:42:12,909
Lucas... Shit!

212
00:42:14,550 --> 00:42:18,189
What happened?
What did they do to you?

213
00:42:22,870 --> 00:42:24,949
Pull it out.

214
00:42:24,950 --> 00:42:27,309
Don't move.

215
00:42:27,870 --> 00:42:30,189
This can't be happening!

216
00:42:34,870 --> 00:42:37,349
Go on.

217
00:42:37,950 --> 00:42:41,309
Go on. Pull it out.

218
00:42:49,550 --> 00:42:51,589
- I can't.
- Pull it out!

219
00:42:51,590 --> 00:42:53,349
Pull it out!

220
00:42:55,630 --> 00:42:58,149
Pull it out, damn it!

221
00:43:01,510 --> 00:43:03,189
Come on!

222
00:43:13,910 --> 00:43:16,909
Hold it tight!

223
00:43:21,590 --> 00:43:23,909
Hang on tight.

224
00:43:31,230 --> 00:43:33,229
Get up!

225
00:44:02,910 --> 00:44:05,189
- What are you doing?
- Wait.

226
00:44:14,550 --> 00:44:16,789
I'll look for a way out.

227
00:44:21,790 --> 00:44:23,989
Hurry up.

228
00:44:29,550 --> 00:44:31,349
I'll be quick.

229
00:44:55,550 --> 00:44:56,989
Clem!

230
00:45:16,750 --> 00:45:18,949
Answer me!

231
00:45:38,150 --> 00:45:40,309
Clem!

232
00:45:55,910 --> 00:45:58,109
Clem, answer me!

233
00:46:07,630 --> 00:46:10,029
What's happening?
Answer me.

234
00:49:18,950 --> 00:49:22,069
- Clem!
- Lucas!

235
00:49:22,950 --> 00:49:25,269
Hurry up. Get up!

236
00:49:29,790 --> 00:49:31,989
Quick!

237
00:49:33,630 --> 00:49:35,309
Hold it.

238
00:49:38,830 --> 00:49:41,029
Come on!

239
00:49:44,550 --> 00:49:45,494
Come on.

240
00:50:00,830 --> 00:50:03,029
Hurry up!

241
00:50:17,030 --> 00:50:19,309
Move it!

242
00:50:31,630 --> 00:50:33,309
Come on!

243
00:50:45,190 --> 00:50:47,269
Don't look back, run.

244
00:51:00,550 --> 00:51:01,494
Come on.

245
00:51:28,150 --> 00:51:31,029
Come on!

246
00:51:36,590 --> 00:51:39,109
This way.

247
00:51:44,910 --> 00:51:45,922
Get down.

248
00:52:08,510 --> 00:52:10,189
This way.

249
00:52:10,790 --> 00:52:13,109
Go on, go on.

250
00:52:24,630 --> 00:52:26,349
Lucas, try again!

251
00:52:26,510 --> 00:52:28,589
Get up!

252
00:52:28,590 --> 00:52:30,749
- You can do it. Try again.
- I can't make it.

253
00:52:30,750 --> 00:52:32,869
- Come with me!
- Go on.

254
00:52:32,870 --> 00:52:35,349
We won't make it otherwise.
Go on!

255
00:52:35,790 --> 00:52:38,789
They're coming.
Go and get help!

256
00:52:38,790 --> 00:52:41,909
Go on, Clem.
Go and get help!

257
00:52:41,910 --> 00:52:43,789
I'll hide!

258
00:52:43,790 --> 00:52:47,629
Go on. Go on, Clem.

259
00:52:47,630 --> 00:52:51,069
Hurry up! Hurry up, Clem!

260
00:56:24,670 --> 00:56:26,949
Shit.

261
00:57:31,830 --> 00:57:32,730
Clem!

262
01:02:55,950 --> 01:02:58,909
She can't breathe! Stop!

263
01:03:00,870 --> 01:03:03,109
You're hurting her. Stop!

264
01:03:06,750 --> 01:03:07,898
Let her go.

265
01:03:19,870 --> 01:03:22,189
You're hurting her.

266
01:03:41,510 --> 01:03:43,989
Come on. Get up!

267
01:03:49,710 --> 01:03:51,789
Shit!

268
01:03:51,790 --> 01:03:56,029
This way.
This way. Hurry up!

269
01:03:57,550 --> 01:03:59,349
Follow him.

270
01:04:00,510 --> 01:04:02,189
Come on!

271
01:04:04,670 --> 01:04:06,989
Hurry!

272
01:04:13,670 --> 01:04:16,029
Come on!

273
01:04:16,750 --> 01:04:19,029
Move it!

274
01:04:45,910 --> 01:04:48,989
- This way.
- Go on.

275
01:04:59,030 --> 01:05:02,229
Quick! Come on, climb!

276
01:05:09,950 --> 01:05:12,349
- Stop!
- What are you doing?

277
01:05:25,750 --> 01:05:28,149
Clem...

278
01:06:07,030 --> 01:06:08,749
Don't hit me.

279
01:06:08,750 --> 01:06:12,709
I didn't do anything!
We just want to play!

280
01:06:12,710 --> 01:06:16,069
Why won't you let us?
Let us!

281
01:06:17,550 --> 01:06:19,909
Will you let us?

282
01:06:25,590 --> 01:06:27,349
She's this way. Hurry!

283
01:06:46,750 --> 01:06:49,069
This way!

284
01:10:01,950 --> 01:10:04,509
Five days after the events,

285
01:10:04,510 --> 01:10:06,589
the bodies of Clémentine Sauveur

286
01:10:06,590 --> 01:10:08,709
and Lucas Medev

287
01:10:08,710 --> 01:10:11,909
were found by the police.

288
01:10:15,510 --> 01:10:16,949
After the investigation,

289
01:10:16,950 --> 01:10:20,829
the murderers were arrested
in the Snagov region.

290
01:10:20,830 --> 01:10:24,109
They were between
the age of 10 and 15.

291
01:10:27,590 --> 01:10:31,909
During the first interrogation,
the youngest amongst them declared:

