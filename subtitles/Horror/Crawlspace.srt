1
00:00:58,158 --> 00:00:59,625
Mr. Gunther?

2
00:02:16,136 --> 00:02:18,934
She can't talk.
I cut her tongue out.

3
00:02:25,612 --> 00:02:27,546
What a shame.

4
00:02:27,580 --> 00:02:29,411
I really liked you.

5
00:02:29,449 --> 00:02:31,007
Liked?

6
00:04:23,997 --> 00:04:25,521
So be it.

7
00:09:00,940 --> 00:09:02,601
Who's there?

8
00:09:42,415 --> 00:09:45,009
Don't stop undressing
on my account, lady.

9
00:09:45,051 --> 00:09:48,885
Really.
I don't mind at all.

10
00:09:50,256 --> 00:09:53,987
You just keep on peeling away.

11
00:09:54,026 --> 00:09:55,926
I like that red.

12
00:09:55,962 --> 00:09:59,022
Looks real hot.
Matches your lips.

13
00:09:59,065 --> 00:10:04,002
Ooh, I like this, too.

14
00:10:04,036 --> 00:10:07,802
Sweet. Kind of slutty.

15
00:10:10,710 --> 00:10:14,237
You know what I like better
than nasty red bras like this?

16
00:10:21,954 --> 00:10:24,650
I like it better
when they're off.

17
00:10:27,059 --> 00:10:28,856
Know what I'd like?

18
00:10:28,894 --> 00:10:31,590
I'd like it if, for once...

19
00:10:31,631 --> 00:10:37,501
you'd use the door instead
of climbing in the window.

20
00:10:52,918 --> 00:10:54,249
I'm gonna come.

21
00:10:54,286 --> 00:10:56,948
Not yet. I'm not ready.

22
00:10:58,624 --> 00:11:00,455
I don't think
I can hold it back, Sophie.

23
00:11:00,493 --> 00:11:03,053
You have to.
I'm not ready yet.

24
00:11:04,764 --> 00:11:06,356
Oh, I'm gonna come, sweetie.

25
00:11:06,399 --> 00:11:08,424
Oh, think of something.
I'm not ready.

26
00:11:08,467 --> 00:11:11,197
Think of what?
All I can think of is coming.

27
00:11:11,237 --> 00:11:12,534
I don't think
I can hold it back.

28
00:11:12,571 --> 00:11:14,937
Don't you dare come,
Hank Peterson.

29
00:11:17,343 --> 00:11:19,368
Are you thinking of something?

30
00:11:19,412 --> 00:11:22,506
- What are you thinking of?
- I don't know.

31
00:11:22,548 --> 00:11:27,485
- Alaskan zombies?
- Oh, that's good!

32
00:11:27,520 --> 00:11:30,318
I think I'm...
I think I'm getting close.

33
00:11:30,356 --> 00:11:32,517
You can come now.

34
00:11:41,100 --> 00:11:42,260
I can't...

35
00:11:46,338 --> 00:11:48,272
Hank.

36
00:11:53,379 --> 00:11:55,142
What?

37
00:11:55,181 --> 00:11:58,480
- Do me a favor?
- What?

38
00:11:58,517 --> 00:12:00,985
You won't tell anybody
about this, will you?

39
00:12:43,329 --> 00:12:46,662
Hello? I came to see about
the vacant apartment.

40
00:12:46,699 --> 00:12:48,963
It's been rented.

41
00:12:54,573 --> 00:12:55,733
Creep.

42
00:13:04,917 --> 00:13:06,475
No, really.
No, that's all right.

43
00:13:06,519 --> 00:13:10,285
I'll just...
I'll just pick it up myself.

44
00:13:10,322 --> 00:13:11,482
Thank you.

45
00:13:55,901 --> 00:13:58,927
Hi. Are you the owner?

46
00:13:58,971 --> 00:14:00,734
Um, I came to see about
the apartment.

47
00:14:00,773 --> 00:14:02,240
Is it still vacant?

48
00:14:02,274 --> 00:14:04,071
As a matter of fact, it is.

49
00:14:04,109 --> 00:14:06,634
May I take a look at it?

50
00:14:08,314 --> 00:14:09,645
Why not?

51
00:14:21,193 --> 00:14:24,128
Hey, do I know you
from somewhere?

52
00:14:24,163 --> 00:14:25,994
No, I don't think so.

53
00:14:27,499 --> 00:14:28,796
Are you sure?

54
00:14:28,834 --> 00:14:32,668
Yes, you have a face
I think I'd remember.

55
00:14:34,006 --> 00:14:35,997
You're a beautiful woman.

56
00:14:36,041 --> 00:14:40,171
Oh, why that's...
Uh, thank you. Um...

57
00:14:40,212 --> 00:14:42,271
My name is Lori Bancroft.

58
00:14:42,314 --> 00:14:45,909
You don't mind
if we don't shake?

59
00:14:45,951 --> 00:14:47,475
This way, please.

60
00:14:58,631 --> 00:15:00,599
Pretty girl.

61
00:15:00,633 --> 00:15:04,899
Yeah. Mr. Gunther always runs
to the pretty ones.

62
00:15:04,937 --> 00:15:08,464
What do you suppose
his secret is?

63
00:15:08,507 --> 00:15:10,270
How do you mean?

64
00:15:10,309 --> 00:15:12,834
Well, everyone
has a dark secret.

65
00:15:12,878 --> 00:15:15,574
I just wonder what his is.

66
00:15:24,323 --> 00:15:26,052
It's wonderful.

67
00:15:28,928 --> 00:15:30,156
I love it.

68
00:15:30,195 --> 00:15:35,929
It's just the right size.
It's affordable.

69
00:15:35,968 --> 00:15:38,698
It's not anything like
that creepy old haunted house...

70
00:15:38,737 --> 00:15:40,227
I lived in last year.

71
00:15:44,877 --> 00:15:47,141
What a nightmare.

72
00:15:47,179 --> 00:15:50,148
I'm convinced the man
across the hall was a vampire.

73
00:15:51,350 --> 00:15:56,515
Well, maybe he wasn't
a real vampire, but...

74
00:15:59,558 --> 00:16:01,685
You know what I like best
about this place?

75
00:16:01,727 --> 00:16:04,662
I have no vampires.

76
00:16:04,697 --> 00:16:06,221
That's good.

77
00:16:06,265 --> 00:16:10,497
No, I like it because it's
walking distance to school.

78
00:16:10,536 --> 00:16:13,403
You do rent
to college students, don't you?

79
00:16:13,439 --> 00:16:16,738
Mm-hmm. Yes, I do.

80
00:16:16,775 --> 00:16:20,939
If I find they are the right
choice for the apartment.

81
00:16:20,980 --> 00:16:22,709
I try to be selective.

82
00:16:22,748 --> 00:16:26,081
Well, that's good.
You should be...

83
00:16:26,118 --> 00:16:28,552
selective.

84
00:16:31,256 --> 00:16:34,191
This is a great kitchen.

85
00:16:44,336 --> 00:16:46,201
Will you take it?

86
00:16:47,639 --> 00:16:49,072
Pardon?

87
00:16:49,108 --> 00:16:50,905
The apartment...
will you take it?

88
00:16:50,943 --> 00:16:52,774
I need to know now.

89
00:16:54,113 --> 00:16:56,479
Sure, I'll take it.
It's perfect.

90
00:17:00,452 --> 00:17:02,113
It's a deal, then?

91
00:17:02,154 --> 00:17:05,123
Well, you bet.
It's a deal.

92
00:17:06,291 --> 00:17:11,058
May 8, 1986.

93
00:17:13,532 --> 00:17:18,265
I used to kill
in the name of science.

94
00:17:18,303 --> 00:17:21,329
Now I kill...

95
00:17:21,373 --> 00:17:27,642
because
I'm addicted to killing.

96
00:17:29,915 --> 00:17:33,180
It's the only way
I can feel alive.

97
00:17:38,557 --> 00:17:41,219
Now, Martha,
let's see, where was I?

98
00:17:47,566 --> 00:17:50,194
It's the part
where I talk about father...

99
00:17:50,235 --> 00:17:53,432
sending mother and me
to Argentina in 1944.

100
00:17:58,277 --> 00:18:03,579
June '53,
I finish medical school and...

101
00:18:03,615 --> 00:18:04,775
begin my practice.

102
00:18:08,654 --> 00:18:11,885
Martha, it isn't good
if you don't eat.

103
00:18:16,628 --> 00:18:19,597
Come here, please.

104
00:18:19,631 --> 00:18:20,859
You must eat, Martha.

105
00:18:20,899 --> 00:18:23,459
Come on, another one.

106
00:18:27,106 --> 00:18:30,132
Swallow it down. Come on.

107
00:18:57,469 --> 00:18:59,403
I can't kill you.

108
00:19:00,973 --> 00:19:03,032
Who would I talk to
if you are dead?

109
00:19:19,258 --> 00:19:20,725
Hi, Karl.

110
00:19:20,759 --> 00:19:22,659
Hey, could you give me a hand?

111
00:19:22,694 --> 00:19:23,854
Sure.

112
00:19:27,399 --> 00:19:28,889
You have a sweet tooth.

113
00:19:28,934 --> 00:19:32,165
- Oh, well, they were on sale.
- A sackful?

114
00:19:32,204 --> 00:19:34,604
Oh, why is it
I can never find my key?

115
00:19:34,640 --> 00:19:37,268
Oh, I have a master key.
I'll open your door.

116
00:19:37,309 --> 00:19:40,574
Oh, I'm such a goofball
sometimes.

117
00:19:42,414 --> 00:19:44,006
- Thanks.
- There you are.

118
00:19:44,049 --> 00:19:45,573
You're welcome.

119
00:19:45,617 --> 00:19:46,914
How about a glass of wine,
Karl?

120
00:19:46,952 --> 00:19:49,352
No, no, I couldn't.
I don't drink alcohol.

121
00:19:49,388 --> 00:19:50,650
It poisons the body.

122
00:19:50,689 --> 00:19:53,988
Oh, it's a small vice, Karl.

123
00:19:54,026 --> 00:19:56,654
Everybody has
at least a few vices.

124
00:19:56,695 --> 00:19:59,596
- Don't you have any vices?
- Like what?

125
00:19:59,631 --> 00:20:01,997
Oh, I don't know, the usual...

126
00:20:02,034 --> 00:20:04,400
drugs, gambling...

127
00:20:04,436 --> 00:20:08,600
fast cars, women.

128
00:20:10,375 --> 00:20:13,469
- Well?
- Well?

129
00:20:13,512 --> 00:20:15,343
Well, say something.

130
00:20:17,616 --> 00:20:20,244
I had women, OK?

131
00:20:20,285 --> 00:20:22,276
It didn't work out.

132
00:20:39,238 --> 00:20:41,866
Jessica here
is our most famous tenant.

133
00:20:41,907 --> 00:20:44,808
Really? What are you
famous for, Jessica?

134
00:20:44,843 --> 00:20:46,310
I'm a soap opera actress.

135
00:20:48,180 --> 00:20:50,444
I play a soap opera actress
on a soap opera.

136
00:20:50,482 --> 00:20:53,781
- Are you serious?
- I'm afraid so.

137
00:20:57,189 --> 00:20:59,953
Here, Lori,
try this on for size.

138
00:20:59,992 --> 00:21:03,359
- What is it?
- Tequila milk shake.

139
00:21:08,900 --> 00:21:12,734
- Gotta say no to that.
- Oh, come on. Be a sport.

140
00:21:18,477 --> 00:21:19,671
What do you think?

141
00:21:23,749 --> 00:21:26,377
It's the worst stuff
I ever tasted.

142
00:21:26,418 --> 00:21:30,013
Oh, it is, but, honey,
it does wonders for your woes.

143
00:21:32,224 --> 00:21:34,089
I just can't figure it out.

144
00:21:34,126 --> 00:21:38,187
It seems every time
I start sleeping with a guy...

145
00:21:38,230 --> 00:21:40,027
with any sort of regularity...

146
00:21:40,065 --> 00:21:41,726
he disappears on me...

147
00:21:41,767 --> 00:21:44,964
without so much as a "Been
nice knowing you, Harriet...

148
00:21:45,003 --> 00:21:46,994
"but I've got to be moving on."

149
00:21:47,039 --> 00:21:49,906
My latest fellow and me,
we were just starting...

150
00:21:49,941 --> 00:21:52,933
to get good at it
when he just up and whooshed.

151
00:21:52,978 --> 00:21:55,776
Gone, like a fart in a storm.

152
00:21:55,814 --> 00:22:00,080
I didn't mind that he was
undependable, unemployed...

153
00:22:00,118 --> 00:22:04,782
uncivilized, uncouth, uncool,
and lazy as a pig.

154
00:22:04,823 --> 00:22:08,224
From the sound of it, Harriet,
you were probably better off.

155
00:22:08,260 --> 00:22:13,425
Honey,
alone again is never better off.

156
00:22:13,465 --> 00:22:16,525
Anyway...

157
00:22:16,568 --> 00:22:20,902
enough of this poor little ol'
pitiful-as-a-dog me shit.

158
00:22:20,939 --> 00:22:22,531
Drink up.

159
00:22:24,176 --> 00:22:25,507
Man...

160
00:22:39,925 --> 00:22:41,620
That was close.

161
00:23:59,371 --> 00:24:01,236
Don't scare me like that.

162
00:24:01,273 --> 00:24:03,173
I'm sorry.
I didn't mean to.

163
00:24:03,208 --> 00:24:04,641
Mr. Gunther...

164
00:24:04,676 --> 00:24:08,578
I hate to bother you
this late at night, but...

165
00:24:08,613 --> 00:24:11,309
would you mind going down
to my apartment?

166
00:24:11,349 --> 00:24:14,216
- Sure.
- There's a strange noise.

167
00:24:14,252 --> 00:24:17,744
- Vampires?
- No, no, no. Not this time.

168
00:24:17,789 --> 00:24:19,313
More like The Exorcist.

169
00:24:19,357 --> 00:24:23,088
Would you like to come in,
Lori, for a moment?

170
00:24:26,231 --> 00:24:29,860
Thank you, Mr. Gunther.
It's... it's late.

171
00:24:29,901 --> 00:24:31,129
Are you sure?

172
00:24:32,471 --> 00:24:34,200
Well, I only came up here...

173
00:24:34,239 --> 00:24:37,299
because I was disturbed
by the noise.

174
00:24:37,342 --> 00:24:39,833
Oh, it was probably the rats.

175
00:24:39,878 --> 00:24:41,846
Isn't there something
you can do?

176
00:24:41,880 --> 00:24:44,144
- Sure, I'll take care of it.
- Thank you.

177
00:24:44,182 --> 00:24:45,877
Are you sure you don't want
to come in?

178
00:24:47,686 --> 00:24:48,880
Thank you, Mr. Gunther.

179
00:24:51,122 --> 00:24:52,316
Good night!

180
00:24:57,395 --> 00:24:59,192
Good night, Lori.

181
00:25:09,608 --> 00:25:12,771
I was chief surgeon
at the National Hospital...

182
00:25:12,811 --> 00:25:16,770
when I began
practicing euthanasia.

183
00:25:18,517 --> 00:25:24,217
As a doctor, I was able to end
their pain by mercy-killing.

184
00:25:24,256 --> 00:25:28,454
Then, in 1971...

185
00:25:28,493 --> 00:25:31,690
a very strange thing happened.

186
00:25:31,730 --> 00:25:35,291
I began reading
my father's diaries...

187
00:25:35,333 --> 00:25:38,325
and was shocked to learn
that euthanasia...

188
00:25:38,370 --> 00:25:42,329
was the same word
he used as an excuse...

189
00:25:42,374 --> 00:25:44,001
to kill Jewish people.

190
00:25:44,042 --> 00:25:47,136
Euthanasiengruppen.

191
00:25:49,447 --> 00:25:52,007
Thank God
I have a sense of humor.

192
00:25:53,552 --> 00:25:58,489
It makes life much easier
if you can laugh.

193
00:26:18,410 --> 00:26:21,811
We don't have to love forever

194
00:26:21,846 --> 00:26:24,440
Just tonight

195
00:26:26,384 --> 00:26:30,150
We don't have to say
sweet whispers

196
00:26:30,188 --> 00:26:32,452
Just tonight

197
00:26:34,626 --> 00:26:37,789
We don't have to be true lovers

198
00:26:37,829 --> 00:26:42,732
Just kiss me tonight

199
00:26:42,767 --> 00:26:45,759
We don't have
to hold each other

200
00:26:45,804 --> 00:26:49,399
Just tonight

201
00:26:49,441 --> 00:26:53,537
We don't have to say
"I love you"

202
00:26:53,578 --> 00:26:59,881
Just for tonight,
lovers tonight

203
00:26:59,918 --> 00:27:03,081
Hold me tonight

204
00:27:04,856 --> 00:27:07,347
Make me believe

205
00:27:07,392 --> 00:27:10,418
You need me tonight

206
00:27:10,462 --> 00:27:12,726
I'm coming, Sophie.

207
00:27:14,766 --> 00:27:19,794
Don't make me feel like a fool

208
00:27:22,073 --> 00:27:29,536
Don't think
that I would ever cry for you

209
00:27:29,581 --> 00:27:38,114
But why should I want
one more night with you

210
00:27:38,156 --> 00:27:42,115
When I don't really love you

211
00:27:43,728 --> 00:27:45,389
Anymore?

212
00:28:23,234 --> 00:28:26,294
Hank Peterson,
will you quit playing around...

213
00:28:26,337 --> 00:28:29,773
and just get in here?

214
00:28:29,808 --> 00:28:31,332
I'm hot.

215
00:29:15,987 --> 00:29:17,750
So be it.

216
00:30:39,237 --> 00:30:41,034
Dr. Gunther?

217
00:31:27,785 --> 00:31:31,482
My name is Joseph Steiner.
Do you remember me?

218
00:31:33,625 --> 00:31:35,092
May I come in?

219
00:31:35,126 --> 00:31:37,219
Please let me alone.

220
00:31:39,063 --> 00:31:43,727
Dr. Gunther, I have spent
three years looking for you.

221
00:31:43,768 --> 00:31:46,100
You should know by now
that I'm a very tenacious man.

222
00:31:46,137 --> 00:31:51,973
You were not easy to find,
but I found you.

223
00:31:52,010 --> 00:31:54,308
And I am not going to go away.

224
00:31:56,147 --> 00:31:57,614
Just a moment.

225
00:32:26,544 --> 00:32:28,409
You are a murderer, Dr. Gunther.

226
00:32:30,315 --> 00:32:32,374
Oh, I can't prove it.

227
00:32:32,417 --> 00:32:33,748
Not yet anyway.

228
00:32:33,785 --> 00:32:36,219
But during the five years
you were the chief resident...

229
00:32:36,254 --> 00:32:38,688
at the National Hospital
in Buenos Aires...

230
00:32:38,723 --> 00:32:41,317
sixty-seven people
admitted under your care...

231
00:32:41,359 --> 00:32:43,589
with routine illnesses died.

232
00:32:43,628 --> 00:32:47,655
And, as you know, Dr. Gunther...

233
00:32:47,699 --> 00:32:50,224
one of those sixty-seven
was my brother.

234
00:32:52,103 --> 00:32:53,695
Mr. Steiner...

235
00:32:55,974 --> 00:32:59,034
I'm a doctor
for twenty-seven years.

236
00:32:59,077 --> 00:33:01,944
I delivered babies,
I healed the wounded...

237
00:33:01,980 --> 00:33:05,746
and I cured the sick
every day of my life.

238
00:33:05,783 --> 00:33:07,751
Surely,
some of my patients died.

239
00:33:07,785 --> 00:33:09,218
One of them was your brother.

240
00:33:09,253 --> 00:33:12,484
I'm sorry about that,
but I am only a doctor.

241
00:33:27,205 --> 00:33:28,365
Do you recognize this man?

242
00:33:28,406 --> 00:33:33,207
Ehrenfuhrer SS Officer
Karl Werner Reitlinger.

243
00:33:33,244 --> 00:33:35,974
Principle designer of
the instruments of torture...

244
00:33:36,014 --> 00:33:40,451
used in the Einsatzgruppen in
over 100 Concentration Camps...

245
00:33:40,485 --> 00:33:42,976
executed in 1945...

246
00:33:43,021 --> 00:33:45,990
by the international
military tribunal...

247
00:33:46,024 --> 00:33:48,424
for crimes against humanity.

248
00:33:50,561 --> 00:33:56,261
This is a picture
of your father, isn't it?

249
00:33:56,300 --> 00:33:59,269
Mr. Steiner,
I'm going to ask you to leave.

250
00:33:59,303 --> 00:34:03,364
- Dr. Gunther...
- Mr. Steiner...

251
00:34:03,408 --> 00:34:05,842
I ask you to leave.
Good-bye.

252
00:35:03,768 --> 00:35:06,032
I will be watching you.

253
00:36:14,972 --> 00:36:21,775
December 8, 1973.

254
00:36:21,812 --> 00:36:25,873
I have been practicing
euthanasia as routinely...

255
00:36:25,917 --> 00:36:30,149
as I perform my other
everyday medical procedures.

256
00:36:33,558 --> 00:36:38,894
February 17, 1974.

257
00:36:38,930 --> 00:36:41,592
Due to a clerical error...

258
00:36:41,632 --> 00:36:44,123
I mistakenly terminated
the life...

259
00:36:44,168 --> 00:36:47,069
of a perfectly healthy man.

260
00:36:52,443 --> 00:36:58,279
A quick undetected injection,
and his heart stopped.

261
00:36:58,316 --> 00:37:01,308
To my surprise...

262
00:37:01,352 --> 00:37:05,982
I did not feel remorse.

263
00:37:09,460 --> 00:37:11,587
Instead,
I began to understand...

264
00:37:11,629 --> 00:37:13,927
the unusual sensations...

265
00:37:13,965 --> 00:37:17,366
my father described
in his writings.

266
00:37:19,637 --> 00:37:21,901
Killing is my heroin...

267
00:37:21,939 --> 00:37:24,271
my opiate...

268
00:37:24,308 --> 00:37:25,775
my fix.

269
00:37:30,514 --> 00:37:32,744
It gives me
a god-like sensation...

270
00:37:32,783 --> 00:37:35,946
that goes beyond that
special feeling doctors have...

271
00:37:35,987 --> 00:37:39,787
because they can save lives.

272
00:37:39,824 --> 00:37:43,726
When you can take life away
as easily as you can give it...

273
00:37:46,831 --> 00:37:52,497
that is a feeling
very few people ever experience.

274
00:38:04,815 --> 00:38:07,249
You're a really
difficult woman, Martha.

275
00:38:09,754 --> 00:38:11,813
You haven't even
touched your milk yet.

276
00:38:19,664 --> 00:38:21,495
Do you want some of it?

277
00:38:26,237 --> 00:38:28,205
Here you are.

278
00:38:51,996 --> 00:38:53,759
I'm a happy man, Martha.

279
00:39:01,238 --> 00:39:03,206
Just a sec.
Let me find the switch.

280
00:39:14,151 --> 00:39:16,312
Nice place.
It has your touch.

281
00:39:16,354 --> 00:39:18,049
Thank you.

282
00:39:19,357 --> 00:39:20,790
Can I fix you a drink?

283
00:39:23,060 --> 00:39:26,086
Tonight would be
a good night to drink...

284
00:39:26,130 --> 00:39:27,995
a lot.

285
00:39:29,867 --> 00:39:31,164
Do you go to the ballet often?

286
00:39:31,202 --> 00:39:35,104
About as often as
I'm audited by the IRS.

287
00:39:35,139 --> 00:39:38,165
- How often is that, Alfred?
- Too often.

288
00:39:38,209 --> 00:39:41,269
Both experiences
are terribly boring.

289
00:39:41,312 --> 00:39:44,907
Oh, is that what we were?
Bored?

290
00:39:44,949 --> 00:39:47,782
Sometimes culture confuses me...

291
00:39:47,818 --> 00:39:50,309
and I think
I'm having a good time.

292
00:39:50,354 --> 00:39:54,120
Anyway, that's the way I felt
tonight at the ballet.

293
00:39:57,161 --> 00:39:58,992
You enjoyed the ballet?

294
00:40:00,431 --> 00:40:03,696
Oh, no, not at all. You see,
that's what confused me.

295
00:40:03,734 --> 00:40:05,759
The ballet confused you?

296
00:40:05,803 --> 00:40:07,964
No, culture confuses me.

297
00:40:08,005 --> 00:40:09,973
I just wasn't certain
it was boring...

298
00:40:10,007 --> 00:40:12,498
until you told me
about the IRS.

299
00:40:12,543 --> 00:40:17,242
Jessica, I have no idea
what you're talking about...

300
00:40:17,281 --> 00:40:21,684
but I find you
absolutely charming.

301
00:40:21,719 --> 00:40:23,311
Aren't I?

302
00:40:37,701 --> 00:40:39,635
I like you, Jessica.

303
00:40:39,670 --> 00:40:42,036
I like you, too, Alfred.

304
00:40:52,016 --> 00:40:55,042
You remind me
of my Uncle Morris.

305
00:40:58,255 --> 00:41:00,485
What a rotten human being.

306
00:41:01,659 --> 00:41:03,718
Oh, I didn't mean
you were rotten, Albert.

307
00:41:03,761 --> 00:41:05,820
- I just meant...
- Alfred.

308
00:41:05,863 --> 00:41:10,027
Alfred. I mean, you're just,
like, uh, older, you know?

309
00:41:10,067 --> 00:41:13,867
Like my rotten Uncle Morris.

310
00:41:13,904 --> 00:41:17,533
I'm sorry.
Um, why don't l, uh...

311
00:41:17,575 --> 00:41:20,738
slip into something
more comfortable?

312
00:41:20,778 --> 00:41:22,643
Yes. Why don't you?

313
00:42:52,436 --> 00:42:54,370
What are you doing, Alfred?

314
00:42:56,740 --> 00:42:59,334
Well, l... I just...

315
00:42:59,376 --> 00:43:01,105
I heard this, um...

316
00:43:01,145 --> 00:43:04,114
l... I can't quite describe it.

317
00:43:04,148 --> 00:43:07,379
Well, it... it sounded
like a tap-tap-tapping.

318
00:43:07,418 --> 00:43:11,081
- Mice!
- Mice?

319
00:43:11,121 --> 00:43:12,554
Baby mice.

320
00:43:12,590 --> 00:43:15,150
You get used to it.

321
00:43:19,330 --> 00:43:20,558
Oh, boy...

322
00:43:39,917 --> 00:43:42,147
I like your cane, Alfred.

323
00:43:44,855 --> 00:43:47,688
It's long...

324
00:43:47,725 --> 00:43:49,488
slick...

325
00:43:51,729 --> 00:43:53,356
and hard.

326
00:43:59,236 --> 00:44:01,101
Nice ring.

327
00:44:03,140 --> 00:44:07,770
Thanks.
It has survived four wives.

328
00:44:07,811 --> 00:44:10,041
I'm rather fond of it myself.

329
00:44:11,115 --> 00:44:13,777
Just how rich are you, Alfred?

330
00:44:13,817 --> 00:44:18,083
Oh, does it matter to you
that I'm rich?

331
00:44:18,122 --> 00:44:24,118
Well, yes, it does matter to me
if you are rich or not.

332
00:44:24,161 --> 00:44:25,992
I wouldn't have gone out
with you in the first place...

333
00:44:26,030 --> 00:44:28,430
if you weren't.

334
00:44:28,465 --> 00:44:30,695
But don't be offended by that...

335
00:44:30,734 --> 00:44:34,135
because I don't go out with
anybody unless they're rich.

336
00:44:34,171 --> 00:44:39,768
Jessica,
you are absolutely an original.

337
00:44:39,810 --> 00:44:42,301
Yes, I am.

338
00:44:48,719 --> 00:44:51,882
- Now, what the hell is that?
- Uh, the baby mice.

339
00:44:53,157 --> 00:44:56,092
There. It stopped.

340
00:45:46,076 --> 00:45:48,374
Sorry, kitty.

341
00:45:54,985 --> 00:45:59,718
Jessica, I just cannot
make love with that sound.

342
00:45:59,757 --> 00:46:01,691
I'm really very sorry.

343
00:46:05,496 --> 00:46:07,293
And if I were you...

344
00:46:07,331 --> 00:46:11,631
I'd talk to your landlord
about hiring an exterminator.

345
00:47:37,821 --> 00:47:44,124
"May 9, 1986.

346
00:47:44,161 --> 00:47:50,828
"I am intrigued by
my own special relationship...

347
00:47:50,868 --> 00:47:53,462
"with death.

348
00:47:53,503 --> 00:47:58,440
"Ever since arriving
in America...

349
00:47:58,475 --> 00:48:04,744
"I have meticulously
documented my killings...

350
00:48:04,781 --> 00:48:13,086
"in order to explore
this addiction to death.

351
00:48:14,391 --> 00:48:19,556
"And lately
I have begun to wonder...

352
00:48:19,596 --> 00:48:27,002
"if I have fallen from grace...

353
00:48:27,037 --> 00:48:28,800
"with the world."

354
00:51:33,323 --> 00:51:36,053
Excuse me for disturbing you,
Miss Bancroft.

355
00:51:36,093 --> 00:51:39,256
My name is Josef Steiner.

356
00:51:39,296 --> 00:51:40,957
I spoke with you earlier
on the phone.

357
00:51:40,997 --> 00:51:44,763
Oh, right, right.
Come in.

358
00:52:07,090 --> 00:52:08,455
Do you mind if I smoke?

359
00:52:12,262 --> 00:52:14,890
Personally,
I can't stand smokers.

360
00:52:14,931 --> 00:52:17,525
They're a nasty hazard
to society.

361
00:52:17,567 --> 00:52:20,229
Well, then, why do you smoke?

362
00:52:20,270 --> 00:52:23,364
Compulsive personality type,
I suppose.

363
00:52:25,308 --> 00:52:27,572
As I told you earlier
on the phone, Miss Bancroft...

364
00:52:27,611 --> 00:52:32,173
I'm investigating your landlord
Dr. Karl Gunther...

365
00:52:32,215 --> 00:52:33,409
and part of
that investigation...

366
00:52:33,450 --> 00:52:35,145
involves getting
certain information...

367
00:52:35,185 --> 00:52:36,743
from some of his tenants.

368
00:52:36,786 --> 00:52:39,220
Yes, well,
I don't know if I can help you.

369
00:52:39,256 --> 00:52:42,054
As you can see, I just
moved in here a few days ago.

370
00:52:42,092 --> 00:52:44,720
You're Lori Bancroft,
age twenty-six...

371
00:52:44,761 --> 00:52:46,388
born in Tucson, Arizona.

372
00:52:46,429 --> 00:52:48,897
Journalism student
at Brown University.

373
00:52:48,932 --> 00:52:52,561
Single. Both parents living
in Santa Fe, New Mexico.

374
00:52:52,602 --> 00:52:55,400
Your father is editor
of the Santa Fe Chronicle.

375
00:52:58,575 --> 00:53:01,442
Your sister Kate died
when she was only four.

376
00:53:01,478 --> 00:53:04,914
Just exactly who are you
investigating, Mr. Steiner?

377
00:53:04,948 --> 00:53:08,042
I believe Dr. Karl Gunther
killed my brother.

378
00:53:08,084 --> 00:53:10,484
OK...

379
00:53:10,520 --> 00:53:13,546
Mr. Steiner or...

380
00:53:13,590 --> 00:53:15,080
whoever you are...

381
00:53:17,060 --> 00:53:18,652
I think you better leave.

382
00:53:20,830 --> 00:53:23,697
Miss Bancroft, I have spent
three years trying to prove...

383
00:53:23,733 --> 00:53:25,724
Dr. Karl Gunther
killed my brother.

384
00:53:25,769 --> 00:53:28,397
I just need a few more pieces
of the puzzle.

385
00:53:28,438 --> 00:53:29,905
Look, I don't know you...

386
00:53:29,940 --> 00:53:31,965
and I don't know anything
about Dr. Gunther...

387
00:53:32,008 --> 00:53:33,737
and I'd rather not get involved.

388
00:53:33,777 --> 00:53:35,335
Miss Bancroft, you are involved.

389
00:53:35,378 --> 00:53:36,936
There's something going on
in this building.

390
00:53:36,980 --> 00:53:38,447
I don't know what it is...

391
00:53:38,481 --> 00:53:40,415
but I think
you can help me find out.

392
00:53:40,450 --> 00:53:43,180
Mr. Steiner,
I don't know what kind of ax...

393
00:53:43,220 --> 00:53:45,051
you have to grind
with my landlord...

394
00:53:45,088 --> 00:53:46,988
but I'd just as soon
not be part of it.

395
00:53:47,023 --> 00:53:49,321
No. It should be painful.

396
00:54:01,671 --> 00:54:06,472
Miss Bancroft,
I don't mean to alarm you...

397
00:54:08,511 --> 00:54:12,709
but I would be concerned about
your safety in this building.

398
00:54:58,895 --> 00:55:00,192
Dr. Gunther?

399
00:55:42,072 --> 00:55:47,339
"May 9, 1986.

400
00:55:47,377 --> 00:55:50,437
"Like my father...

401
00:55:50,480 --> 00:55:53,040
"I am fascinated...

402
00:55:53,083 --> 00:55:57,986
"by the delicate balance
between life and death..."

403
00:56:01,991 --> 00:56:04,084
"good and evil."

404
00:57:14,764 --> 00:57:16,391
So be it.

405
00:58:19,696 --> 00:58:22,096
Heil, heil Hitler!

406
00:58:56,699 --> 00:58:59,361
I am my own God...

407
00:58:59,402 --> 00:59:01,893
my own jury...

408
00:59:01,938 --> 00:59:04,338
and my own executioner.

409
00:59:14,517 --> 00:59:16,348
Heil, Gunther.

410
01:00:34,129 --> 01:00:37,292
Who's swimming in your bathtub?

411
01:00:37,333 --> 01:00:38,766
What?

412
01:01:49,505 --> 01:01:52,770
Sophie,
we need to get out of here.

413
01:01:52,808 --> 01:01:56,801
Sweet whispers, just tonight

414
01:01:58,447 --> 01:02:02,178
We don't have to be true lovers

415
01:02:02,217 --> 01:02:06,415
Just kiss me tonight

416
01:02:06,455 --> 01:02:10,551
We don't have
to hold each other

417
01:02:10,592 --> 01:02:13,891
Just tonight

418
01:06:42,164 --> 01:06:44,860
Become a friend of the rats.

419
01:07:20,135 --> 01:07:21,796
Sieg heil!

420
01:07:36,351 --> 01:07:37,511
Get off!

421
01:15:40,568 --> 01:15:42,229
Oh, God!

422
01:15:56,150 --> 01:15:57,981
So be it.

