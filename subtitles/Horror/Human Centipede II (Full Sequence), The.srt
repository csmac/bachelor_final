1
00:03:40,500 --> 00:03:41,507
Hi!

2
00:03:41,542 --> 00:03:45,613
Yes um, I was wondering if
I could get driving directions...

3
00:03:45,614 --> 00:03:49,036
to a nightclub called  Bunker.

4
00:03:49,260 --> 00:03:50,836
Yes, Bunker.

5
00:03:51,739 --> 00:03:53,663
How exactly would I get there?

6
00:03:56,154 --> 00:03:57,367
It's Amy!

7
00:03:58,577 --> 00:04:00,374
Hello, sweetie!

8
00:04:00,606 --> 00:04:01,269
<i>OK.</i>

9
00:04:01,270 --> 00:04:03,930
How are you?
We miss you is strong!

10
00:04:04,423 --> 00:04:06,680
Yes, we wanted to be
here with us.

11
00:04:07,266 --> 00:04:07,806
Yes.

12
00:04:07,807 --> 00:04:11,768
And how far is it?

13
00:04:12,632 --> 00:04:14,688
No we're in y.

14
00:04:15,707 --> 00:04:18,034
Then we travel to Italy in a couple days.

15
00:04:18,035 --> 00:04:18,771
Great.

16
00:04:19,456 --> 00:04:20,858
Thanks.
Alveterzane!

17
00:04:22,636 --> 00:04:23,568
<i>I know.</i>

18
00:04:25,111 --> 00:04:27,617
Tell her we got her a present from Holland.

19
00:04:27,778 --> 00:04:29,846
Yeah we got you a present.

20
00:04:30,212 --> 00:04:33,091
No we can't tell you what it
is, it's a surprise.

21
00:04:35,205 --> 00:04:40,270
Oh yeah, the other thing is that
we met this cut  waiter.

22
00:04:40,740 --> 00:04:42,704
<i>Jenny thinks he's pretty cute</i>

23
00:04:44,563 --> 00:04:47,331
<i>Yeah, and he invited us to a party
tonight.</i>

24
00:04:47,805 --> 00:04:49,004
So we're going to go.

25
00:04:50,210 --> 00:04:52,338
But we should go.

26
00:04:52,391 --> 00:04:54,582
Maybe we can talkto you later.

27
00:04:55,019 --> 00:04:56,032
OK.

28
00:04:56,471 --> 00:04:57,570
<i>All right.
We miss you!</i>

29
00:04:57,571 --> 00:04:59,187
Bye, Anie!
Kisses!

30
00:04:59,188 --> 00:04:59,972
OK.

31
00:05:00,476 --> 00:05:01,860
Bye, sweetie!

32
00:05:15,310 --> 00:05:17,059
I think we were suppose to turn

33
00:05:17,060 --> 00:05:17,633
Turn!

34
00:05:18,117 --> 00:05:21,098
I thought you know exactly
where we sere going!

35
00:05:42,475 --> 00:05:44,151
Do we go left or right?

36
00:05:48,774 --> 00:05:51,377
I'm sorry, but I just don't know
where we are.

37
00:06:00,825 --> 00:06:03,291
What was that?

38
00:06:03,292 --> 00:06:05,450
I have no idea.

39
00:06:06,773 --> 00:06:10,130
You must have run over a pot whole or something.

40
00:06:10,642 --> 00:06:12,684
We need to go out
and check it out.

41
00:06:19,030 --> 00:06:20,837
Shit!

42
00:06:21,204 --> 00:06:23,542
Shit!

43
00:06:23,577 --> 00:06:25,883
Shit!

44
00:06:25,884 --> 00:06:27,770
Why?
Oh God!

45
00:06:27,771 --> 00:06:29,078
Okay, okay ...

46
00:06:30,809 --> 00:06:31,809
Okay.

47
00:06:32,149 --> 00:06:35,655
We call the rental car service.
get the papers.

48
00:06:36,121 --> 00:06:37,104
Okay.

49
00:06:49,534 --> 00:06:50,399
Okay.

50
00:06:54,412 --> 00:06:56,217
219,

51
00:06:56,718 --> 00:06:58,118
200,

52
00:06:58,382 --> 00:06:59,948
806.

53
00:07:04,005 --> 00:07:04,678
what...

54
00:07:04,679 --> 00:07:06,520
There's no signal.

55
00:07:06,521 --> 00:07:07,721
What?

56
00:07:07,722 --> 00:07:09,454
There's no signal!

57
00:07:10,090 --> 00:07:12,881
There's always a signal.

58
00:07:12,882 --> 00:07:15,593
Not out here in the middle of nowhere.

59
00:07:16,347 --> 00:07:18,147
Oh my God, Shit!

60
00:07:26,324 --> 00:07:28,500
Oh my God,
get back in the car.

61
00:07:35,362 --> 00:07:37,838
OK ... Now what?

62
00:07:37,839 --> 00:07:39,123
I don't know.

63
00:07:39,124 --> 00:07:41,303
You know how to change a tire?

64
00:07:41,304 --> 00:07:45,177
- No, I don't know how to change a tire.
- Neither do I.

65
00:07:45,428 --> 00:07:48,325
What are we suppose to walk
until we find a house or a person or something?

66
00:07:48,326 --> 00:07:49,225
Lindsay!

67
00:07:49,324 --> 00:07:53,020
Lindsay. I am not getting out and walking.

68
00:07:53,021 --> 00:07:55,950
Okay we're just going to sit here,
until the sun comes up.

69
00:07:55,951 --> 00:07:59,271
I have heels and shorts on!
I'm not going outside!

70
00:08:02,370 --> 00:08:04,282
Are those headlines?

71
00:08:11,102 --> 00:08:13,067
Okay.
role down your window.

72
00:08:13,068 --> 00:08:15,723
Are you kidding?!
I'm not going to just role down the window.

73
00:08:15,724 --> 00:08:18,073
Role down the window.
He can help.

74
00:08:32,228 --> 00:08:33,798
Yeah um, we need help.

75
00:08:34,522 --> 00:08:36,061
We have a flat tire.

76
00:08:42,902 --> 00:08:45,128
No, we speak English.

77
00:08:46,578 --> 00:08:48,541
Can you help us?

78
00:08:52,567 --> 00:08:54,063
Do not understand what he says.

79
00:08:56,859 --> 00:08:59,777
Um, can you call someone?

80
00:09:03,742 --> 00:09:06,029
"Ficken".
-Look it up.

81
00:09:06,030 --> 00:09:08,143
Hold on a second.

82
00:09:13,105 --> 00:09:14,377
Fucking!

83
00:09:14,917 --> 00:09:15,878
He said Fucking!

84
00:09:15,879 --> 00:09:17,829
- Okay. Bye.
- role your window up.

85
00:09:17,830 --> 00:09:18,615
Goodbye.

86
00:09:24,779 --> 00:09:26,836
- I'm sorry.
- I told you!

87
00:09:26,837 --> 00:09:27,910
Is your door locked?

88
00:09:27,911 --> 00:09:29,409
Yes, my door is locked.

89
00:09:32,647 --> 00:09:34,350
Why's he still staring at us?

90
00:09:35,763 --> 00:09:38,326
- Just don't look at him, okay?
- i'm not looking at him.

91
00:09:44,545 --> 00:09:46,277
I just want to get out of here.

92
00:10:20,745 --> 00:10:21,662
Okay.

93
00:10:21,663 --> 00:10:24,408
We need to j-just go!

94
00:10:24,409 --> 00:10:25,413
Gotta go from here.

95
00:10:25,414 --> 00:10:30,024
We need to walk and find someplace,
somebody who can help us.

96
00:10:30,025 --> 00:10:31,695
- Okay, but ...
- We have to!

97
00:10:31,696 --> 00:10:35,891
But if we find apalce in 10 minutes,
we are running back to this car.

98
00:11:06,168 --> 00:11:07,666
We came ...

99
00:11:07,667 --> 00:11:09,955
We came from over here, I swear.

100
00:11:10,951 --> 00:11:13,501
How do you know?
All trees look the same.

101
00:11:21,175 --> 00:11:24,253
Leaving the car
is the stupidest idea.

102
00:11:24,479 --> 00:11:26,411
We would have been waiting for hours, Jenny.

103
00:11:26,412 --> 00:11:29,438
Yeah we could have been waitng
and it not be freezing cold.

104
00:11:31,536 --> 00:11:33,585
We need to go fing help, Jenny.

105
00:11:33,586 --> 00:11:36,658
Seriously find help?
How are we going to fing help out here, Lindsay?

106
00:11:37,271 --> 00:11:40,363
You agreed to come along, Jenny.
This not just my fault.

107
00:11:40,364 --> 00:11:42,570
I didn't want to stay in the car by myself.

108
00:11:42,571 --> 00:11:44,275
We just got a little lost, okay?

109
00:11:44,276 --> 00:11:47,001
- A little lost, Lindsay?
- Yes!

110
00:11:47,002 --> 00:11:48,002
We are not a little lost!

111
00:11:48,509 --> 00:11:49,817
We're really lost!

112
00:11:49,818 --> 00:11:52,335
You know what, Lindsay?
I'm no longer walingg.

113
00:11:52,336 --> 00:11:54,131
I'm tired of walking, okay?

114
00:11:54,132 --> 00:11:56,257
- Jenny, trying!
- Trying what?

115
00:11:56,258 --> 00:11:59,297
- Lindsay, we've been down her for like an hour OK!
- Yeah I know!

116
00:11:59,298 --> 00:12:02,009
I'm tired and I'm cold.
I'M NOT MOVING!

117
00:12:02,010 --> 00:12:03,668
- I'M NOT MOVING!
- Stop it.

118
00:12:03,669 --> 00:12:04,544
No!

119
00:12:04,545 --> 00:12:05,798
- fine!
- fine!

120
00:12:05,799 --> 00:12:06,687
fine.

121
00:12:06,688 --> 00:12:08,974
- I'm staying right here.
- Good.

122
00:12:15,121 --> 00:12:16,296
What is it?

123
00:12:17,603 --> 00:12:19,115
My God is that a...

124
00:12:19,116 --> 00:12:20,742
Is it a house?

125
00:12:21,411 --> 00:12:22,638
Jenny!

126
00:12:22,639 --> 00:12:23,796
Jenny, look!

127
00:12:23,797 --> 00:12:25,537
I think it's a house!
I swear!

128
00:12:25,994 --> 00:12:28,676
- It's a light! Let's go.
- What?

129
00:12:28,677 --> 00:12:29,520
Let's go!

130
00:12:30,787 --> 00:12:33,430
OH! FOR SOME REASON I DON'T BELIEVE YOU!

131
00:12:43,162 --> 00:12:44,515
Oh thank God!

132
00:12:44,516 --> 00:12:46,152
Come on!

133
00:13:04,317 --> 00:13:05,595
Hello!

134
00:13:05,596 --> 00:13:07,126
Anybody home?

135
00:13:09,437 --> 00:13:10,814
Let's go to the front door!

136
00:13:13,209 --> 00:13:14,561
Hello!

137
00:13:18,646 --> 00:13:20,845
-Hello?
-Lindsay!

138
00:13:20,846 --> 00:13:21,975
Come here!

139
00:13:25,834 --> 00:13:27,308
- God!
- What?

140
00:13:27,309 --> 00:13:28,089
Look at the dog!

141
00:13:28,090 --> 00:13:30,318
- What about him?
- I don't like dogs.

142
00:13:30,319 --> 00:13:31,704
I know you don't like dogs.

143
00:13:36,738 --> 00:13:39,612
- Hello?
- Is anybody home?

144
00:13:42,166 --> 00:13:44,051
See somebody?

145
00:13:44,052 --> 00:13:44,668
No.

146
00:13:50,413 --> 00:13:51,727
Hi.

147
00:14:00,146 --> 00:14:01,578
- Hi.
- Hi.

148
00:14:02,832 --> 00:14:04,512
We got a flat tire.

149
00:14:04,513 --> 00:14:05,754
Can we come in ?

150
00:14:05,755 --> 00:14:08,869
Can we use your phone so we
can call the car company service?

151
00:14:09,847 --> 00:14:11,690
Are you alone?

152
00:14:12,596 --> 00:14:13,289
Yes.

153
00:14:13,290 --> 00:14:14,557
We're alone.

154
00:14:21,267 --> 00:14:23,086
come in.

155
00:14:53,179 --> 00:14:54,615
Have a seat.

156
00:15:15,653 --> 00:15:17,463
You're tourists.

157
00:15:18,804 --> 00:15:22,814
- We're on a road trip through Europe...
-It's a vacation.

158
00:15:22,815 --> 00:15:23,774
We're from New York.

159
00:15:31,752 --> 00:15:34,725
Can you call the emergency car service?

160
00:15:35,408 --> 00:15:36,644
For us ...

161
00:15:38,895 --> 00:15:41,185
Are you relatives?

162
00:15:42,064 --> 00:15:44,826
- No.
- No, we're friends.

163
00:15:48,911 --> 00:15:50,531
Very well.

164
00:15:53,368 --> 00:15:55,170
I'll make your phone call.

165
00:15:55,861 --> 00:15:56,851
Okay, thanks.

166
00:15:56,852 --> 00:15:58,834
Something to drink?

167
00:15:59,580 --> 00:16:03,530
- A water's fine.
- Yeah, just water.

168
00:16:28,528 --> 00:16:29,725
Hello.

169
00:16:42,154 --> 00:16:43,362
Well ...

170
00:16:43,736 --> 00:16:46,669
at least he's calling the car company.

171
00:16:57,415 --> 00:16:58,791
Here you go.

172
00:16:59,366 --> 00:17:00,560
Water ...

173
00:17:01,461 --> 00:17:02,761
Water ...

174
00:17:12,090 --> 00:17:14,790
They will arrive in half an hour.

175
00:17:16,391 --> 00:17:18,240
Maximum.

176
00:17:43,335 --> 00:17:46,109
You have a a really lovely home.

177
00:17:46,574 --> 00:17:48,390
You live herei with your wife?

178
00:17:51,244 --> 00:17:52,514
No.

179
00:17:55,718 --> 00:17:58,931
I don't like human beings.

180
00:18:04,858 --> 00:18:06,299
I'm sorry.

181
00:18:14,337 --> 00:18:15,570
I'll get you another one.

182
00:18:15,571 --> 00:18:18,770
- No, it's okay, it's okay.
- We can share.

183
00:18:20,858 --> 00:18:22,541
I'll fetch a towel.

184
00:18:27,771 --> 00:18:29,401
What just happened?

185
00:18:29,402 --> 00:18:30,545
I don't know.

186
00:18:35,166 --> 00:18:37,961
We need to get out of here right now.

187
00:18:37,962 --> 00:18:38,643
Yeah.

188
00:18:39,749 --> 00:18:41,440
It's freaking me out.

189
00:19:15,612 --> 00:19:18,885
You want to? just call a taxi
so we can go back to the hotel.

190
00:19:18,886 --> 00:19:19,892
Let's go back to the Hotel, okay.

191
00:19:19,893 --> 00:19:22,275
- We'll get the car in the morning car.
- Okay. Yeah.

192
00:19:27,780 --> 00:19:29,518
I am really tired.

193
00:20:06,457 --> 00:20:12,667
Listen, If you could just call a taxi service,
we're just going to go back to the hotel.

194
00:20:12,668 --> 00:20:16,344
No. I don't do another phone call.

195
00:20:20,192 --> 00:20:21,918
Can I call you then?

196
00:20:21,919 --> 00:20:23,053
No.

197
00:20:23,054 --> 00:20:24,467
- I'm tired.
- What?

198
00:20:25,072 --> 00:20:27,024
I'm tired.

199
00:20:27,205 --> 00:20:28,791
What's going on?
Look at me.

200
00:20:28,792 --> 00:20:30,091
It's a rape drug.

201
00:20:31,863 --> 00:20:32,323
What?

202
00:20:32,324 --> 00:20:34,615
Rohypnol.

203
00:20:34,616 --> 00:20:35,128
Oh my God! What!

204
00:20:35,129 --> 00:20:41,616
It causes, droziness, dizziness,
disorientation and memory loss.

205
00:20:41,617 --> 00:20:44,398
Are you kidding?!
What have you done?

206
00:20:45,285 --> 00:20:47,426
Jenny!
God!

207
00:20:47,427 --> 00:20:48,444
What is it?

208
00:22:53,593 --> 00:22:54,742
Jenny.

209
00:22:58,008 --> 00:22:59,196
Jenny.

210
00:23:01,561 --> 00:23:02,655
Jenny!

211
00:23:05,428 --> 00:23:08,003
Jenny!
Jenny, wake up!

212
00:23:08,004 --> 00:23:09,098
Jenny!

213
00:23:11,433 --> 00:23:12,704
God!

214
00:23:16,815 --> 00:23:17,892
Jenny.

215
00:23:17,893 --> 00:23:19,167
Lindsay!

216
00:23:22,969 --> 00:23:24,094
Jenny!

217
00:23:33,823 --> 00:23:35,310
What's happening?

218
00:23:35,345 --> 00:23:37,027
What are you doing?

219
00:23:37,062 --> 00:23:38,712
What's this?

220
00:23:39,090 --> 00:23:41,944
What the fuck are you doing to us?

221
00:23:42,444 --> 00:23:44,153
What are you doing?

222
00:23:44,154 --> 00:23:45,652
Jenny!

223
00:23:45,653 --> 00:23:47,369
Let us out!

224
00:24:02,695 --> 00:24:04,478
My friend ...

225
00:24:07,768 --> 00:24:09,583
You don't match.

226
00:24:11,185 --> 00:24:13,190
Have to kill you.

227
00:24:27,254 --> 00:24:29,244
Don't take it personally.

228
00:24:29,712 --> 00:24:33,261
What 's this?
What are you doing to us?

229
00:24:35,948 --> 00:24:39,451
What is this?
Stop!

230
00:24:42,401 --> 00:24:43,557
Jenny!

231
00:27:36,163 --> 00:27:37,083
Please ...

232
00:27:47,043 --> 00:27:49,252
It's gonig to be okay.

233
00:28:28,572 --> 00:28:32,717
I'm Dr. Joseph Heiter.

234
00:28:33,998 --> 00:28:43,089
Retired but still very well known as
the leading surgeon separating siamese twins.

235
00:28:47,591 --> 00:28:49,227
Six months,

236
00:28:49,228 --> 00:28:53,641
I designed a never seen operation,
not separating anymore...

237
00:28:53,642 --> 00:28:55,052
but creating.

238
00:28:55,053 --> 00:28:59,553
I transformed my three Rottweiler

239
00:28:58,553 --> 00:29:04,399
into a beautiful three hound construction.

240
00:29:04,400 --> 00:29:05,818
Good news!

241
00:29:06,953 --> 00:29:08,947
Your tissues match.

242
00:29:09,177 --> 00:29:10,182
So ...

243
00:29:10,666 --> 00:29:17,285
I will explain this spectacular operation
once.

244
00:29:17,973 --> 00:29:19,970
We start ...

245
00:29:20,730 --> 00:29:24,642
with cutting the ligament patelar.

246
00:29:24,988 --> 00:29:27,481
The ligaments of the knee caps.

247
00:29:28,011 --> 00:29:31,329
So knee extension...

248
00:29:32,200 --> 00:29:34,455
is no longer possible.

249
00:29:35,171 --> 00:29:37,705
Pulling from "B" and "C"

250
00:29:37,706 --> 00:29:43,057
The central incisors,
and lateral incisors and Canines.

251
00:29:43,058 --> 00:29:44,558
from the upper and lower jaws...

252
00:29:48,176 --> 00:29:51,186
The lips from"B" and "C"...

253
00:29:51,632 --> 00:29:55,180
and the anus of "A" and "B"...

254
00:29:55,951 --> 00:30:02,505
are cut circular along the boarder
between buttock and rectum.

255
00:30:03,436 --> 00:30:07,541
The mucus retains so.

256
00:30:09,028 --> 00:30:12,573
Two pet acellular grafts
are prepaired

257
00:30:13,453 --> 00:30:18,008
and lifted from the underlining tissue.

258
00:30:23,662 --> 00:30:25,601
The shape incisions.

259
00:30:25,602 --> 00:30:28,917
Below the chins, of "B" to "C"

260
00:30:28,918 --> 00:30:32,136
up to their cheeks.

261
00:30:32,996 --> 00:30:37,932
Connecting the circular skin parts...

262
00:30:37,933 --> 00:30:40,576
of anus and mouth.

263
00:30:40,577 --> 00:30:44,661
From "A" to "B"
and  "B" to "C".

264
00:30:44,662 --> 00:30:46,662
Connecting...

265
00:30:46,663 --> 00:30:52,063
the pet acellular grafts
to the chin/cheek incisions  .

266
00:30:58,030 --> 00:30:59,040
Creating ...

267
00:31:00,410 --> 00:31:03,299
a siamese triplet...

268
00:31:03,300 --> 00:31:04,751
connected

269
00:31:05,062 --> 00:31:07,894
by the gastrict system.

270
00:31:08,435 --> 00:31:11,646
Ingestion by "A"

271
00:31:12,475 --> 00:31:15,882
past through "B"

272
00:31:16,950 --> 00:31:20,821
and excretion by "C".

273
00:31:25,091 --> 00:31:28,018
A human centipede.

274
00:31:29,147 --> 00:31:31,753
First sequence.

275
00:31:43,745 --> 00:31:45,650
Here's your breakfast.

276
00:31:48,087 --> 00:31:50,488
general anesthesia.

277
00:32:24,511 --> 00:32:26,010
Lindsay!

278
00:32:26,011 --> 00:32:27,129
Jenny ...

279
00:32:45,567 --> 00:32:48,225
I want my mom!

280
00:33:14,533 --> 00:33:15,867
No!

281
00:33:59,112 --> 00:34:00,394
<i>Open up!</i>

282
00:34:01,125 --> 00:34:02,518
Please!

283
00:34:03,597 --> 00:34:06,990
Stop!
Why do this?

284
00:34:08,389 --> 00:34:10,184
Open!

285
00:34:10,922 --> 00:34:13,993
Need help!
You're a sick man!

286
00:34:14,653 --> 00:34:17,712
I am a sick man.

287
00:34:17,713 --> 00:34:19,213
<i>HAHAHAHAHA!</i>

288
00:34:20,670 --> 00:34:24,231
If you do not open immediately I will....

289
00:34:25,617 --> 00:34:27,834
cut your knees

290
00:34:27,835 --> 00:34:32,330
<i>and pull your teeth out
one by one!</i>

291
00:34:32,331 --> 00:34:34,401
<i>without anesthesia.</i>

292
00:34:34,436 --> 00:34:36,474
<i>It's your choice!</i>

293
00:34:39,420 --> 00:34:41,691
Open the door!

294
00:34:42,983 --> 00:34:46,447
<i>Open the door!</i>

295
00:35:25,978 --> 00:35:27,483
Stop!

296
00:35:32,281 --> 00:35:33,635
No!

297
00:36:23,719 --> 00:36:26,565
Please!
Please!

298
00:36:28,484 --> 00:36:30,868
Why do this?

299
00:36:30,869 --> 00:36:33,236
Let me and Jenny go.

300
00:36:33,237 --> 00:36:34,350
Please.

301
00:36:36,094 --> 00:36:37,976
We'll give you anything.

302
00:36:38,912 --> 00:36:41,191
Anything you want,
just let us go.

303
00:36:43,999 --> 00:36:45,626
Don't worry ...

304
00:36:45,627 --> 00:36:48,181
it's only a tranquilizer

305
00:36:48,953 --> 00:36:53,423
Keep your head very still.

306
00:36:54,354 --> 00:36:58,349
I don't want to loose one of yout precious eyes.

307
00:37:32,589 --> 00:37:34,380
Just kill me!

308
00:37:37,366 --> 00:37:38,743
Ahhhh.

309
00:37:53,121 --> 00:37:58,079
One of my Rottweilers
tried to flee

310
00:37:58,080 --> 00:38:00,913
just before the operation.

311
00:38:00,948 --> 00:38:03,746
After I caught the dog....

312
00:38:03,747 --> 00:38:06,974
He had to take the middle postion.

313
00:38:07,455 --> 00:38:08,481
in this postion...

314
00:38:08,682 --> 00:38:10,282
the healing pains...

315
00:38:10,882 --> 00:38:14,182
are twice as intense.

316
00:38:15,144 --> 00:38:18,993
Do you already regret your little escape.

317
00:38:19,413 --> 00:38:22,514
In fact I'm thankful for it.

318
00:38:23,529 --> 00:38:28,844
Because now I know definitely,
you are...

319
00:38:28,845 --> 00:38:32,846
the middle piece!

320
00:38:33,866 --> 00:38:37,083
Just kill me now.
I'd rather be dead.

321
00:38:37,084 --> 00:38:40,609
Game Over.

322
00:39:32,226 --> 00:39:34,368
Damn.

323
00:39:35,088 --> 00:39:37,543
power is cut of again..

324
00:39:37,720 --> 00:39:39,376
Sorry!

325
00:39:39,377 --> 00:39:40,377
Damn!

326
00:42:18,124 --> 00:42:19,991
I'm so sorry.

327
00:44:22,090 --> 00:44:23,812
Help!

328
00:49:15,044 --> 00:49:18,656
my sweet centipede ...

329
00:49:25,378 --> 00:49:27,449
Looks good.

330
00:49:29,406 --> 00:49:31,792
Heal well.

331
00:49:37,320 --> 00:49:40,351
Suffering will be over soon.

332
00:49:40,352 --> 00:49:42,950
Your in alot of pain, I know.

333
00:49:48,156 --> 00:49:49,264
Nice.

334
00:49:51,972 --> 00:49:53,859
Okay.

335
00:49:53,860 --> 00:49:55,763
better and better.

336
00:49:56,611 --> 00:49:59,177
My Lead ...

337
00:49:59,378 --> 00:50:01,378
My Lead.

338
00:50:02,364 --> 00:50:03,631
Hey, man.

339
00:50:04,483 --> 00:50:05,819
Hey, man.

340
00:50:14,242 --> 00:50:15,127
Up

341
00:50:17,520 --> 00:50:20,178
Come up!
Up Up Up

342
00:50:21,574 --> 00:50:23,764
Yes, come up!

343
00:50:23,765 --> 00:50:25,215
He and the, lift your.

344
00:50:25,216 --> 00:50:27,245
Up
Yeah, like that!

345
00:50:30,130 --> 00:50:33,140
Very good!
Yeah!

346
00:50:33,141 --> 00:50:35,143
I DID IT!!

347
00:53:56,143 --> 00:53:59,588
Take the newspaper and bring it to me.

348
00:54:00,851 --> 00:54:02,847
Yeah, good boy.

349
00:54:07,133 --> 00:54:08,250
Come.

350
00:54:09,858 --> 00:54:11,100
Come!

351
00:54:18,622 --> 00:54:21,839
Bring me the newspaper.

352
00:54:25,778 --> 00:54:27,483
Good boy.

353
00:54:30,577 --> 00:54:31,551
Come!

354
00:54:32,361 --> 00:54:33,643
Come!

355
00:54:43,705 --> 00:54:47,438
Then let's walk a little.

356
00:54:48,674 --> 00:54:49,952
Atention!

357
00:56:05,256 --> 00:56:08,393
You do this once more I graruntee...

358
00:56:09,268 --> 00:56:13,949
I'll pull your teeth out
one by one...

359
00:56:13,984 --> 00:56:17,977
you kamikaze shit hole.

360
00:56:56,153 --> 00:56:58,038
You want me to bite?

361
00:57:00,698 --> 00:57:03,113
Now you can bite me.

362
00:57:04,084 --> 00:57:06,268
Bite my boot.

363
00:57:06,651 --> 00:57:08,641
Bite my boot!

364
00:57:08,642 --> 00:57:11,130
Bite my boots!

365
00:57:13,826 --> 00:57:17,503
Mr. kamikaze is a chicken today.

366
00:57:31,421 --> 00:57:36,400
How dare you!
Turn your back on me?

367
00:57:57,427 --> 00:57:59,163
AHAHAHAHAHAH!

368
00:58:00,294 --> 00:58:03,195
Feed her!
FEED HER!

369
00:58:05,756 --> 00:58:09,868
Swallow it bitch!

370
00:58:09,869 --> 00:58:11,715
Swallow!

371
00:58:16,510 --> 00:58:21,228
Feed her!
FEED HER!

372
00:58:56,591 --> 00:58:58,704
I want to sleep.

373
00:58:59,964 --> 00:59:02,660
Have to sleep.

374
00:59:03,650 --> 00:59:07,210
Why didn't I dut your vocal chords?

375
00:59:09,831 --> 00:59:12,869
If you don't shut up...

376
00:59:18,513 --> 00:59:21,870
We will do a follow-up operation.

377
01:00:23,228 --> 01:00:27,002
Finally! You want to move your ass.

378
01:00:27,264 --> 01:00:29,207
That's fine with me.

379
01:00:31,122 --> 01:00:36,424
Maybe you even can escape.

380
01:00:49,350 --> 01:00:51,852
<i>Come!
Come!</i>

381
01:00:52,895 --> 01:00:54,870
After you, Please.

382
01:01:15,871 --> 01:01:21,071
AAAAAAAAAAAAAAAAAAAAAAAAAA!!!!!!!!!!!!!!!

383
01:01:22,619 --> 01:01:25,546
Teach you to mind me!

384
01:01:45,978 --> 01:01:48,323
Strong.

385
01:01:48,612 --> 01:01:50,917
Good.

386
01:01:58,231 --> 01:02:00,562
Constipated.

387
01:02:00,563 --> 01:02:03,370
Laxatives.

388
01:02:04,386 --> 01:02:07,061
Instant kind,
good stuff.

389
01:02:35,605 --> 01:02:39,293
End section, you are very sick.

390
01:02:41,083 --> 01:02:43,668
I think you're dying.

391
01:02:43,669 --> 01:02:46,402
I will have to replace you.

392
01:03:09,913 --> 01:03:12,390
Shut Up!

393
01:10:05,100 --> 01:10:09,261
I've got two strong and healthy replacements.

394
01:10:11,895 --> 01:10:14,139
So enjoy...

395
01:10:14,140 --> 01:10:17,895
your last moments with the Jen tail...

396
01:10:18,591 --> 01:10:25,406
Because when I'm back,
I'm mercy killing it.

397
01:10:26,112 --> 01:10:29,567
Preparing for a new operation...

398
01:10:29,568 --> 01:10:32,306
A quadruplet.

399
01:10:33,932 --> 01:10:35,959
See you.

400
01:19:20,283 --> 01:19:24,431
Eye for an eye...

401
01:19:25,991 --> 01:19:29,425
tooth for a tooth.

402
01:29:20,917 --> 01:29:25,917
(distant weeping)

403
01:29:27,018 --> 01:29:30,018
Subtitles by: COOKIE

