﻿1
00:00:02,853 --> 00:00:05,255
<i>Shortly before the
start of World War II,</i>

2
00:00:05,355 --> 00:00:08,559
<i>the German High Command
began a secret investigation</i>

3
00:00:08,659 --> 00:00:10,761
<i>into the powers of the supernatural.</i>

4
00:00:10,861 --> 00:00:16,433
<i>Ancient legend told of a race of warriors
who used neither weapons nor shields</i>

5
00:00:16,533 --> 00:00:20,646
<i>and whose superhuman power
came from within the earth itself.</i>

6
00:00:21,146 --> 00:00:26,193
<i>As Germany prepared for war, the SS
secretly enlisted a group of scientists</i>

7
00:00:26,293 --> 00:00:28,946
<i>to create an invincible soldier.</i>

8
00:00:29,546 --> 00:00:32,783
<i>It is known that the bodies
of soldiers killed in battle</i>

9
00:00:32,883 --> 00:00:35,869
<i>were returned to a secret
laboratory near Coblenz,</i>

10
00:00:35,969 --> 00:00:40,040
<i>where they were used in a
variety of scientific experiments.</i>

11
00:00:40,140 --> 00:00:42,793
<i>It was rumored that
toward the end of the war</i>

12
00:00:42,893 --> 00:00:47,047
<i>Allied forces met German squads
that fought without weapons,</i>

13
00:00:47,147 --> 00:00:49,550
<i>killing only with their bare hands.</i>

14
00:00:50,050 --> 00:00:53,137
<i>No one knows who they were
or what became of them,</i>

15
00:00:53,237 --> 00:00:57,303
<i>but one thing is certain,
of all the SS units,</i>

16
00:00:57,404 --> 00:01:03,212
<i>there was only one that the Allies
never captured a single member of.</i>

17
00:02:53,148 --> 00:02:56,642
I don't know how long that dinghy
floated around with me lying in it.

18
00:02:57,486 --> 00:03:02,189
All I can remember is the sound of
the water slapping against the side.

19
00:03:03,158 --> 00:03:06,687
Then I heard the engine
sound getting closer.

20
00:03:06,787 --> 00:03:09,731
That was when I realized
I was still alive.

21
00:03:09,831 --> 00:03:12,658
Get yourself down in there
and help me get her out.

22
00:03:17,297 --> 00:03:19,074
Hey, hey, don't do that.

23
00:03:19,174 --> 00:03:21,500
He's trying to help you!

24
00:03:23,720 --> 00:03:26,164
I couldn't understand
what they wanted.

25
00:03:26,264 --> 00:03:28,876
It didn't occur to me
I was being rescued.

26
00:03:28,976 --> 00:03:31,211
It's okay, it's okay,
we don't mean no hurt to you.

27
00:03:31,311 --> 00:03:33,888
Jonathan, fetch some water here.

28
00:03:39,486 --> 00:03:42,389
What you doing way out here
in that there boat, little lady?

29
00:03:42,489 --> 00:03:46,650
Here, some cool liquid for you,
make you feel right.

30
00:03:52,040 --> 00:03:54,491
You gonna tell us now
what you doing out here?

31
00:03:56,253 --> 00:03:58,280
I wish I could've told him.

32
00:03:58,380 --> 00:04:02,291
But it's only now that I
remember any of it at all.

33
00:04:19,526 --> 00:04:23,096
It was the second day on
one of those small dive boats

34
00:04:23,196 --> 00:04:25,140
that takes you around the islands.

35
00:04:25,240 --> 00:04:29,068
The engine had broken
down for the second time.

36
00:05:47,030 --> 00:05:49,558
Keep her at half speed.

37
00:05:49,658 --> 00:05:52,860
No sense putting any
strain on that crankshaft.

38
00:05:54,788 --> 00:05:57,114
Here's your course.

39
00:06:00,001 --> 00:06:03,280
Three, three, zero. Northwest?

40
00:06:03,380 --> 00:06:07,875
Three, three, zero, North-northwest.

41
00:06:09,052 --> 00:06:10,495
Listen, my friend,

42
00:06:10,595 --> 00:06:14,416
what we must have on this
boat is accurate navigation.

43
00:06:14,516 --> 00:06:16,543
A sailor's best friend.

44
00:06:16,643 --> 00:06:20,672
It's the way he makes his way around
without losing his boat and his crew.

45
00:06:20,772 --> 00:06:25,684
It isn't how good-looking he is,
it's his ability to navigate.

46
00:06:26,695 --> 00:06:28,346
Sorry.

47
00:06:28,446 --> 00:06:31,106
Don't apologize, navigate.

48
00:07:02,772 --> 00:07:05,015
Jesus, look at the sun.

49
00:07:31,343 --> 00:07:33,919
See if you can raise
someone on the radio.

50
00:08:00,372 --> 00:08:02,656
Cut the engine.

51
00:08:22,018 --> 00:08:25,137
- Do you hear that?
- Be quiet!

52
00:08:42,580 --> 00:08:45,199
Let's get the hell out of here.

53
00:09:18,908 --> 00:09:22,229
Norman, it's pitch black out there,
what do you expect to see?

54
00:09:22,329 --> 00:09:24,856
I don't like this one bit.

55
00:09:24,956 --> 00:09:27,567
I hope these people know
what they're doing.

56
00:09:27,667 --> 00:09:30,237
They must know what they are doing,
they're sailors, aren't they?

57
00:09:30,337 --> 00:09:33,622
So was the crew of the Titanic.

58
00:09:37,427 --> 00:09:39,079
Look Norman, if you're so worried,

59
00:09:39,179 --> 00:09:41,039
why don't you go upstairs and
help the captain steer the boat?

60
00:09:41,139 --> 00:09:43,041
I'm sure he'd be very grateful.

61
00:09:43,141 --> 00:09:45,634
I think I'll do just that.

62
00:09:50,398 --> 00:09:53,176
- Hey, how about a drink?
- No thank you.

63
00:09:53,276 --> 00:09:56,388
I'm going to speak to the
captain about this situation.

64
00:09:56,488 --> 00:09:57,847
Oh, relax.

65
00:09:57,947 --> 00:10:00,684
Besides, you won't get anything
out of him, I already tried.

66
00:10:00,784 --> 00:10:02,894
Well, don't you think
it's a bit unusual?

67
00:10:02,994 --> 00:10:05,272
You people ready for some dinner?

68
00:10:05,372 --> 00:10:08,365
Can anyone tell us what's
going on out there?

69
00:10:09,250 --> 00:10:12,570
I'm only the galley hand,
I don't know no more than you do.

70
00:10:12,670 --> 00:10:15,365
- Hey, Dobbs, how about a drink?
- Don't mind if I do.

71
00:10:15,465 --> 00:10:18,868
Yes, but you're an experienced seaman,
you... you must've encountered...

72
00:10:18,968 --> 00:10:20,787
No, I...

73
00:10:20,887 --> 00:10:23,422
take it neat, just the way it is.

74
00:10:26,351 --> 00:10:29,970
You mean to say you've never
seen anything like this?

75
00:10:30,855 --> 00:10:33,258
Can't says I ever have.

76
00:10:33,358 --> 00:10:35,719
Well, aren't you curious about it?

77
00:10:35,819 --> 00:10:38,478
Sea spits up what it can't keep down.

78
00:10:41,533 --> 00:10:44,568
We've been holding
steady for half an hour.

79
00:10:46,079 --> 00:10:49,774
Steer one six five and keep
her below eight knots.

80
00:10:49,874 --> 00:10:52,701
Take another reading
in a couple of hours.

81
00:10:53,837 --> 00:10:55,822
You have any idea where we are?

82
00:10:55,922 --> 00:10:57,949
Will when the weather clears.

83
00:10:58,049 --> 00:11:02,294
In the meantime, keep checking the
radio and keep your eyes peeled.

84
00:11:36,004 --> 00:11:39,824
By the looks on your faces,
maybe I shouldn't eat this.

85
00:11:39,924 --> 00:11:43,870
Oh, your cook Dobbs here just
told us one hell of a yarn.

86
00:11:43,970 --> 00:11:45,455
Oh?

87
00:11:45,555 --> 00:11:49,501
What was it this time?
Sea monsters or ghost ships?

88
00:11:49,601 --> 00:11:52,719
Well, as a matter of fact,
it was ghost ships.

89
00:11:53,730 --> 00:11:56,257
Up to his old tricks again.

90
00:11:56,357 --> 00:11:59,677
You don't believe any of these
stories have some truth to them?

91
00:11:59,777 --> 00:12:02,097
Look, miss,

92
00:12:02,197 --> 00:12:06,726
I could match you story for
story with logical evidence,

93
00:12:06,826 --> 00:12:08,353
but what does it prove?

94
00:12:08,453 --> 00:12:10,522
Men at sea often have hallucinations.

95
00:12:10,622 --> 00:12:14,567
They work hard, they have eyestrain,
lack of sleep, exhaustion.

96
00:12:14,667 --> 00:12:17,320
Sometimes, they're just plain stupid.

97
00:12:17,420 --> 00:12:20,573
Why, some say they're
more afraid of water

98
00:12:20,673 --> 00:12:22,534
than little old ladies
are of the dark.

99
00:12:22,634 --> 00:12:24,619
Now just a minute,

100
00:12:24,719 --> 00:12:29,416
you mean to say that what we
all saw out there is just a mirage?

101
00:12:29,516 --> 00:12:32,877
It was a minor underwater disturbance.

102
00:12:32,977 --> 00:12:35,722
It was a hot sky acting
on a cold current

103
00:12:35,822 --> 00:12:38,266
coming from a mile down below.

104
00:12:38,566 --> 00:12:40,176
Dobbs was just saying...

105
00:12:40,276 --> 00:12:43,430
Oh, if everything that
old fart said came true,

106
00:12:43,530 --> 00:12:47,434
both he and me would be
in the deep six by now.

107
00:12:47,534 --> 00:12:51,312
Given the condition of this boat,
I wonder that hasn't happened already.

108
00:12:51,412 --> 00:12:53,106
Norman, please.

109
00:12:53,206 --> 00:12:55,733
What's bothering you, mister?

110
00:12:55,833 --> 00:13:00,447
It seems to me, you're taking an
unnecessary risk with a boat like this.

111
00:13:00,547 --> 00:13:02,782
What do you mean "a boat like this?"

112
00:13:02,882 --> 00:13:06,202
I mean, a broken down old tub

113
00:13:06,302 --> 00:13:10,957
that should've been retired years ago,
along with her captain.

114
00:13:11,057 --> 00:13:15,670
How is it that a used-car salesman
knows so all fired much about boats?

115
00:13:15,770 --> 00:13:17,964
I know enough to see that this boat

116
00:13:18,064 --> 00:13:20,884
is not what you people
would call "shipshape."

117
00:13:20,984 --> 00:13:23,803
The accommodations aren't worth
the money at half the price.

118
00:13:23,903 --> 00:13:27,807
I think that you have a
responsibility to your passengers.

119
00:13:27,907 --> 00:13:31,478
- We should turn around right now.
- Set back?

120
00:13:31,578 --> 00:13:34,147
For one crankshaft? No chance.

121
00:13:34,247 --> 00:13:36,774
The first thing you'd want
is a refund of your money,

122
00:13:36,874 --> 00:13:39,444
and I've already spent
your money on her.

123
00:13:39,544 --> 00:13:43,406
No, we're gonna sit
right here, and you...

124
00:13:43,506 --> 00:13:48,126
you people are going to get your money's
worth whether you like it or not.

125
00:13:57,645 --> 00:13:59,513
Ladies.

126
00:14:05,320 --> 00:14:08,230
Now I understand why
we're the only passengers.

127
00:14:09,741 --> 00:14:14,020
I think it's time for us to take
matters into our own hands.

128
00:14:14,120 --> 00:14:15,230
Take over the ship?

129
00:14:15,330 --> 00:14:17,023
I mean nothing of the sort.

130
00:14:17,123 --> 00:14:19,108
There are two other
crew members on board.

131
00:14:19,208 --> 00:14:21,903
They may not be smart, but they
seem to have their wits about them.

132
00:14:22,003 --> 00:14:24,489
I'll admit he is a bit unusual,

133
00:14:24,589 --> 00:14:26,241
but I think you're overreacting a bit.

134
00:14:26,341 --> 00:14:28,701
Overreacting? Am I?

135
00:14:28,801 --> 00:14:30,245
All right.

136
00:14:30,345 --> 00:14:32,288
I warned you people.

137
00:14:32,388 --> 00:14:35,166
You're inviting trouble
by not taking any action.

138
00:14:35,266 --> 00:14:38,426
I've spoken my mind,
my conscience is clear.

139
00:14:41,981 --> 00:14:43,424
Beverly.

140
00:14:43,524 --> 00:14:45,809
I think I'll stay up a little longer.

141
00:14:47,487 --> 00:14:49,729
I'll be in the cabin.

142
00:15:51,050 --> 00:15:52,744
Hi.

143
00:15:52,844 --> 00:15:55,538
- You really gave me a scare.
- Sorry.

144
00:15:55,638 --> 00:15:58,506
I guess we're all a
little jumpy tonight.

145
00:16:00,059 --> 00:16:02,510
Do you always steer
the boat with your feet?

146
00:16:05,898 --> 00:16:08,051
You're not very talkative, are you?

147
00:16:08,151 --> 00:16:11,102
Captain doesn't like us
messing with the passengers.

148
00:16:13,990 --> 00:16:16,351
Do you stay up here
every night by yourself?

149
00:16:16,451 --> 00:16:18,978
Ben was supposed to relieve me.

150
00:16:19,078 --> 00:16:21,397
He needs the rest more than I do.

151
00:16:21,497 --> 00:16:24,525
So, I've got the whole
dogwatch to myself.

152
00:16:24,625 --> 00:16:26,110
Dogwatch?

153
00:16:26,210 --> 00:16:28,988
It's the middle of the night watch.

154
00:16:29,088 --> 00:16:30,782
I like it.

155
00:16:30,882 --> 00:16:33,375
I get a chance to be alone.

156
00:16:34,469 --> 00:16:36,913
Can you really tell where
we're going from this?

157
00:16:37,013 --> 00:16:38,665
Sure.

158
00:16:38,765 --> 00:16:40,166
How?

159
00:16:40,266 --> 00:16:42,968
Calculating your speed,
time, and distance.

160
00:16:44,312 --> 00:16:46,714
Where are we now?

161
00:16:46,814 --> 00:16:48,299
I don't know.

162
00:16:48,399 --> 00:16:50,385
What?

163
00:16:50,485 --> 00:16:53,520
Our compass has been
out most of the day.

164
00:17:32,026 --> 00:17:33,469
What's the matter?

165
00:17:33,569 --> 00:17:35,221
I don't know. Did you hear anything?

166
00:17:35,321 --> 00:17:37,772
I don't think so.

167
00:17:39,033 --> 00:17:41,067
Now I do.

168
00:18:37,633 --> 00:18:39,285
What the hell?

169
00:18:39,385 --> 00:18:41,621
Are you all right?

170
00:18:41,721 --> 00:18:43,880
Lend a hand here.

171
00:18:49,812 --> 00:18:51,422
Where is it?

172
00:18:51,522 --> 00:18:53,800
- Where's what?
- The freighter off the port bow.

173
00:18:53,900 --> 00:18:56,302
Freighter nothing.
What it'd do, fly? Where's it gone?

174
00:18:56,402 --> 00:18:59,347
Freighter coming down on us.
Running without lights.

175
00:18:59,447 --> 00:19:03,017
Ships don't come up out of nowhere.
You ran us onto something!

176
00:19:03,117 --> 00:19:05,353
He is telling the truth!
I saw it, too.

177
00:19:05,453 --> 00:19:07,188
What happened?

178
00:19:07,288 --> 00:19:10,566
Get back inside.
There's no danger here.

179
00:19:10,666 --> 00:19:12,985
Well, it sounded like
we hit something!

180
00:19:13,085 --> 00:19:16,030
Sure we hit something,
a school of canned tuna.

181
00:19:16,130 --> 00:19:20,743
Take that jacket off, we're not sinking.
Not yet anyway.

182
00:19:26,265 --> 00:19:30,552
Well, here's one goddamned
thing those bastards won't get.

183
00:19:32,420 --> 00:19:34,455
What the hell?!

184
00:19:35,107 --> 00:19:37,885
Has everyone gone crazy?!

185
00:19:37,985 --> 00:19:39,470
Don't you know?

186
00:19:39,570 --> 00:19:42,480
We've been hit by a ghost ship.

187
00:20:48,723 --> 00:20:50,882
It's over there.

188
00:20:57,773 --> 00:20:59,933
It was moving down on us, Dobbs.

189
00:21:00,943 --> 00:21:04,013
It ain't going no place now.

190
00:21:04,113 --> 00:21:07,523
But it was moving! I swear.

191
00:21:11,037 --> 00:21:12,396
Where's Ben?

192
00:21:12,496 --> 00:21:14,732
I wish I knew.

193
00:21:14,832 --> 00:21:17,109
What do you mean?

194
00:21:17,209 --> 00:21:19,445
Went looking for him
a little while ago.

195
00:21:19,545 --> 00:21:22,740
He wasn't in his cabin,
and he wasn't no place else.

196
00:21:22,840 --> 00:21:25,618
Come up on deck, and I found these.

197
00:21:25,718 --> 00:21:29,580
First off, I figured he was
checking the underside.

198
00:21:29,680 --> 00:21:31,040
Beats me.

199
00:21:31,140 --> 00:21:33,591
Clothes here, dinghy's still here.

200
00:21:35,061 --> 00:21:37,630
He's too old to go swimming
around these here currents!

201
00:21:46,614 --> 00:21:48,724
What's it look like?

202
00:21:48,824 --> 00:21:50,977
Well, the props and
shafts are all right.

203
00:21:51,077 --> 00:21:54,063
She's lying flat on the bottom
and I can't see everything.

204
00:21:54,163 --> 00:21:56,440
Are we gonna be able to
get out of here all right?

205
00:21:56,540 --> 00:21:59,151
We'll know as soon as the
tide comes in and lifts us off.

206
00:21:59,251 --> 00:22:02,279
Oh, swell, and then we can go
look for the captain, right?

207
00:22:02,379 --> 00:22:05,873
He's probably waiting for us to pick
him up on the other side of the island.

208
00:22:32,993 --> 00:22:35,730
I still don't see why we
can't stay in the boat.

209
00:22:35,830 --> 00:22:37,648
Surely you can pump the water out.

210
00:22:37,748 --> 00:22:39,150
The hull's been damaged.

211
00:22:39,250 --> 00:22:40,901
One wave, and she rolls right over.

212
00:22:41,001 --> 00:22:43,320
You'd have a lot of trouble
getting out of it then.

213
00:22:43,420 --> 00:22:45,531
You know, I keep asking myself

214
00:22:45,631 --> 00:22:48,701
if this is really happening,
if this isn't all a bad dream.

215
00:22:48,801 --> 00:22:52,503
Then I realize, it isn't a dream anyone
would believe, so it must be real.

216
00:23:08,404 --> 00:23:11,265
Ben! Ben!

217
00:23:11,365 --> 00:23:14,400
Where the hell are you?

218
00:23:21,083 --> 00:23:23,659
What the hell does
he think he's doing?

219
00:23:27,631 --> 00:23:30,583
He is trying to break his neck.

220
00:23:38,893 --> 00:23:41,128
Shit!

221
00:23:50,321 --> 00:23:53,516
Are you sure we're not
weighted down too heavily?

222
00:23:53,616 --> 00:23:55,684
Norman, leave him alone.
He knows what he's doing.

223
00:23:55,784 --> 00:23:57,645
Knows what he's doing, does he?

224
00:23:57,745 --> 00:23:59,855
Beverly, these people
haven't the slightest idea

225
00:23:59,955 --> 00:24:02,274
of how to boil water,
let alone sail in it.

226
00:24:02,374 --> 00:24:04,401
Sorry I said anything.

227
00:27:00,803 --> 00:27:03,129
Looks like our trip up
here was for nothing.

228
00:27:04,223 --> 00:27:06,757
This must've been one hell of a joint.

229
00:29:11,350 --> 00:29:13,043
Hey, Dobbsy!

230
00:29:13,143 --> 00:29:15,219
I found your room!

231
00:29:51,890 --> 00:29:53,375
Don't you ever get
enough of that stuff?

232
00:29:53,475 --> 00:29:55,718
Only once in a while.

233
00:29:57,771 --> 00:30:00,007
Then I don't usually remember it.

234
00:30:05,445 --> 00:30:07,813
Well, will you look at this?

235
00:30:17,040 --> 00:30:20,117
It's just a dusty old refrigerator.

236
00:30:20,919 --> 00:30:23,120
Dusty old refrigerator.

237
00:32:23,375 --> 00:32:26,535
Why have you come to this place?

238
00:32:31,842 --> 00:32:34,418
I am waiting.

239
00:32:36,179 --> 00:32:38,749
You will please answer my question.

240
00:32:38,849 --> 00:32:40,542
Who wants to know?

241
00:32:40,642 --> 00:32:42,836
That is not of importance.

242
00:32:42,936 --> 00:32:44,671
Why are you here?

243
00:32:44,771 --> 00:32:47,598
Our boat ran aground out on the reef.

244
00:32:48,483 --> 00:32:50,135
Where are you?

245
00:32:50,235 --> 00:32:53,771
I am near, but also far.

246
00:32:56,032 --> 00:32:59,436
We can't get her off.
The bottom's filled in.

247
00:32:59,536 --> 00:33:02,279
We saw this place and came up here.

248
00:33:03,081 --> 00:33:05,984
- How many are you?
- Why should we answer you?

249
00:33:06,084 --> 00:33:10,739
Because I live here and you
have entered uninvited.

250
00:33:10,839 --> 00:33:12,741
Look, we didn't mean to trespass.

251
00:33:12,841 --> 00:33:16,244
It didn't seem like anyone was here.

252
00:33:16,344 --> 00:33:18,705
We were running at
night and hit that wreck.

253
00:33:18,805 --> 00:33:20,957
We're alone, we need help.

254
00:33:21,057 --> 00:33:23,835
We don't mean to
cause you any trouble.

255
00:33:23,935 --> 00:33:26,387
What do you mean? There is no wreck.

256
00:33:28,482 --> 00:33:31,141
Sure there is. It's right
out there on the reef.

257
00:33:34,112 --> 00:33:36,188
Can you help us out?

258
00:33:37,949 --> 00:33:40,651
Are you still there?

259
00:33:41,453 --> 00:33:44,940
I don't like this one bit.
He sounds like a nut.

260
00:33:45,040 --> 00:33:49,236
This is some old hermit,
probably harmless.

261
00:33:56,760 --> 00:34:01,130
Perhaps you do need help,
but you cannot get it here.

262
00:34:02,474 --> 00:34:05,252
I have been the sole
occupant of this hotel

263
00:34:05,352 --> 00:34:08,588
and this island for many years now.

264
00:34:08,688 --> 00:34:13,218
You have left others at the beach?

265
00:34:13,318 --> 00:34:16,638
No, there's only us.

266
00:34:16,738 --> 00:34:20,016
There are no survivors from the wreck?

267
00:34:20,116 --> 00:34:22,144
No, it's all rusted out.

268
00:34:22,244 --> 00:34:24,062
It must've been there for years.

269
00:34:24,162 --> 00:34:26,314
It has a name?

270
00:34:26,414 --> 00:34:28,734
Yeah, the Pro something.

271
00:34:28,834 --> 00:34:32,028
- Protious?
- Might be.

272
00:34:32,128 --> 00:34:33,996
Do you know it?

273
00:34:36,258 --> 00:34:39,501
What about getting off the island?

274
00:34:41,221 --> 00:34:42,581
I've had enough of this!

275
00:34:42,681 --> 00:34:44,916
If he's gone, let's get out of here.

276
00:34:45,016 --> 00:34:49,087
- Think you can find your way?
- You're not coming?

277
00:34:49,187 --> 00:34:52,181
Just where are you
going to go from here?

278
00:37:01,361 --> 00:37:03,221
Where'd you find those?

279
00:37:03,321 --> 00:37:05,640
There's a whole room
full of them upstairs.

280
00:37:05,740 --> 00:37:09,943
- Any sign of our friend?
- Not a sound in the place.

281
00:37:11,079 --> 00:37:15,550
- What time is it?
- Why? Are you going someplace?

282
00:37:15,650 --> 00:37:18,528
I haven't been to bed this early
since I was five years old.

283
00:37:18,628 --> 00:37:20,363
How can he just fall asleep like that?

284
00:37:23,216 --> 00:37:25,500
He played very hard today.

285
00:39:18,540 --> 00:39:21,484
I know what you was
goddamned gonna say.

286
00:39:21,584 --> 00:39:24,828
It ain't hot now, right?

287
00:39:26,005 --> 00:39:29,534
It ain't hot now, so I can carry
them goddamned cans back here.

288
00:39:29,634 --> 00:39:33,163
Hey, not so loud, will ya?
You'll wake everybody.

289
00:39:33,263 --> 00:39:36,708
- I didn't say anything.
- Well you was gonna say it!

290
00:39:36,808 --> 00:39:38,668
Shut up, will ya?

291
00:39:38,768 --> 00:39:41,171
Least we can do is feed
these people now and then.

292
00:39:41,271 --> 00:39:43,221
I'm going, I'm going.

293
00:39:44,774 --> 00:39:46,634
You don't have to.
One of us can do it.

294
00:39:46,734 --> 00:39:49,686
None of you can do nothing,
that's what it comes down to.

295
00:39:52,365 --> 00:39:54,434
Not that I'm complaining, mind you.

296
00:39:54,534 --> 00:39:57,270
I'm only doing my job.

297
00:40:59,557 --> 00:41:01,758
Is anyone there?

298
00:41:09,400 --> 00:41:11,309
Who is it?

299
00:42:33,776 --> 00:42:36,186
And now, you have found me.

300
00:42:40,408 --> 00:42:42,108
Wait a second.

301
00:42:43,661 --> 00:42:46,064
I was hoping you would
give us some help.

302
00:42:46,164 --> 00:42:47,774
Of course.

303
00:42:47,874 --> 00:42:50,235
You must excuse the
manners of an old man

304
00:42:50,335 --> 00:42:52,487
who has lived by himself
for too many years.

305
00:42:52,587 --> 00:42:56,908
Not having people around,
they have grown rusted with disuse.

306
00:42:57,008 --> 00:43:00,620
All we want is for you to tell
us how we can get out of here.

307
00:43:00,720 --> 00:43:02,413
Yes.

308
00:43:02,513 --> 00:43:04,999
Yes, this is a most unfortunate
time for your visit.

309
00:43:05,099 --> 00:43:07,210
You and your friends
must leave immediately.

310
00:43:07,310 --> 00:43:10,546
Fine with me. How do we do it?

311
00:43:10,646 --> 00:43:14,300
There is a boat, a small boat,
but it can get you away from here.

312
00:43:14,400 --> 00:43:17,929
You're only a sail of two days
from the nearest inhabited island.

313
00:43:18,029 --> 00:43:20,265
If you hurry, the tide
will be favorable.

314
00:43:20,365 --> 00:43:22,183
What's the rush?

315
00:43:22,283 --> 00:43:25,603
It is better if you do not know that,
but I can tell you this.

316
00:43:25,703 --> 00:43:28,856
There is danger here,
danger in the water.

317
00:43:28,956 --> 00:43:32,242
If you are quick,
you can escape that danger.

318
00:43:33,127 --> 00:43:37,532
On the edge of the grounds is a
path which leads to a small canal.

319
00:43:37,632 --> 00:43:39,409
There, you will find the boat.

320
00:43:39,509 --> 00:43:41,786
I believe it is still sea-worthy.

321
00:44:41,779 --> 00:44:43,938
Help me, will ya?

322
00:45:00,756 --> 00:45:02,749
Come on, let's get out of here.

323
00:45:08,514 --> 00:45:10,249
Look at this.

324
00:45:10,349 --> 00:45:12,425
Jesus.

325
00:45:14,353 --> 00:45:16,047
What is it?

326
00:45:16,147 --> 00:45:19,759
It's German. Symbol for the SS.

327
00:45:19,859 --> 00:45:22,762
SS? What's he doing with that?

328
00:45:22,862 --> 00:45:25,063
Bet I know who to ask.

329
00:45:26,324 --> 00:45:29,025
Maybe we should ask them.

330
00:46:09,825 --> 00:46:12,110
I am here, gentlemen.

331
00:46:25,216 --> 00:46:27,368
Why is it you have not left?

332
00:46:27,468 --> 00:46:29,085
Because of this.

333
00:46:31,138 --> 00:46:32,707
Where did you find it?

334
00:46:32,807 --> 00:46:34,625
We found it in the hand of our cook

335
00:46:34,725 --> 00:46:37,211
when we pulled him out
of the creek down there.

336
00:46:37,311 --> 00:46:38,880
He is dead?

337
00:46:38,980 --> 00:46:40,548
Don't you know?

338
00:46:40,648 --> 00:46:43,342
Most sincerely, I do not.

339
00:46:43,442 --> 00:46:45,136
It seemed like he was drowned.

340
00:46:45,236 --> 00:46:47,597
Then I am afraid it is too late.

341
00:46:47,697 --> 00:46:50,940
- Too late for what?
- For any of us.

342
00:46:51,742 --> 00:46:53,978
You are, indeed, very stupid.

343
00:46:54,078 --> 00:46:57,523
I gave you a chance to save yourselves,
but you have refused to take it.

344
00:46:57,623 --> 00:46:59,150
So be it.

345
00:46:59,250 --> 00:47:01,652
Now, it is too late.

346
00:47:01,752 --> 00:47:05,031
- Did you kill Dobbs?
- Yes, I killed him.

347
00:47:05,131 --> 00:47:06,574
And your captain, too, perhaps,

348
00:47:06,674 --> 00:47:10,369
but not in the way you think.
It was indirect.

349
00:47:10,469 --> 00:47:12,788
What the hell is that
supposed to mean?

350
00:47:12,888 --> 00:47:14,999
You're not making any sense.

351
00:47:15,099 --> 00:47:17,293
If you will not be so impatient,

352
00:47:17,393 --> 00:47:20,678
maybe I will have the
opportunity to tell you.

353
00:47:23,399 --> 00:47:26,177
Well, try and make it fast.

354
00:47:26,277 --> 00:47:28,721
We are not alone on this island.

355
00:47:28,821 --> 00:47:32,225
So, those other two
we saw are with you?

356
00:47:32,325 --> 00:47:36,479
We Germans developed the
perfect weapon, a soldier.

357
00:47:36,579 --> 00:47:39,315
He was capable of fighting
under any conditions,

358
00:47:39,415 --> 00:47:42,276
adapting to any
environment or climate,

359
00:47:42,376 --> 00:47:46,030
equally at home in the Russian
winter or on the African desert.

360
00:47:46,130 --> 00:47:48,157
They were the most vicious

361
00:47:48,257 --> 00:47:50,576
and bloodthirsty of
all the SS divisions.

362
00:47:50,676 --> 00:47:54,872
The group under my command
was designed for the water,

363
00:47:54,972 --> 00:47:59,126
to man submarines which
would never have to surface.

364
00:47:59,226 --> 00:48:02,129
This is nothing but
a lot of double talk.

365
00:48:02,229 --> 00:48:04,924
We created the perfect soldier

366
00:48:05,024 --> 00:48:07,385
from cheap hoodlums and thugs

367
00:48:07,485 --> 00:48:11,389
and a good number of pathological
murderers and sadists as well.

368
00:48:11,489 --> 00:48:14,892
We called them the Toten Corps,

369
00:48:14,992 --> 00:48:16,435
the Death Corps.

370
00:48:16,535 --> 00:48:19,522
Creatures more horrible
than any you can imagine.

371
00:48:19,622 --> 00:48:23,734
Not dead, not alive,
but somewhere in-between.

372
00:48:23,834 --> 00:48:28,162
They were transported to any
scene of battle and let loose.

373
00:48:28,964 --> 00:48:31,492
But problems arose.
They could not be controlled.

374
00:48:31,592 --> 00:48:34,328
Their innate desire for violence

375
00:48:34,428 --> 00:48:37,290
made their behavior
unpredictable and erratic.

376
00:48:37,390 --> 00:48:39,917
There were even incidences when
they attacked their own soldiers.

377
00:48:40,017 --> 00:48:42,587
So they were withdrawn
for further study.

378
00:48:42,687 --> 00:48:46,340
Then the war was drawing to an end.

379
00:48:46,440 --> 00:48:49,969
I was ordered to remove my
group from possible capture.

380
00:48:50,069 --> 00:48:53,389
I took them to sea just before
all our ports were closed.

381
00:48:53,489 --> 00:48:55,975
We roamed the oceans for many weeks,

382
00:48:56,075 --> 00:48:58,352
waiting orders which never came.

383
00:48:58,452 --> 00:49:00,396
The war was lost.

384
00:49:00,496 --> 00:49:02,898
Somewhere near to this island,

385
00:49:02,998 --> 00:49:06,485
I sent the ship and her
cargo to the bottom.

386
00:49:06,585 --> 00:49:09,196
And here I have been ever since

387
00:49:09,296 --> 00:49:11,157
in voluntary exile.

388
00:49:11,257 --> 00:49:14,042
That's the ship?
The one out on the reef?

389
00:49:15,094 --> 00:49:17,747
And now, she has returned.

390
00:49:17,847 --> 00:49:22,460
You mean to tell us, these things have
been underwater all these years?

391
00:49:22,560 --> 00:49:25,004
What kind of story is this?

392
00:49:25,104 --> 00:49:27,173
You gonna believe this stuff?

393
00:49:27,273 --> 00:49:29,182
You may believe what you wish.

394
00:49:30,359 --> 00:49:33,269
What you do now is no
longer any concern of mine.

395
00:49:34,113 --> 00:49:36,272
I want you to leave.

396
00:49:37,283 --> 00:49:41,896
If I see any of you at all,
I will shoot on sight.

397
00:49:41,996 --> 00:49:44,155
I think that's pretty clear.

398
00:51:57,256 --> 00:51:59,165
Halt!

399
00:52:00,217 --> 00:52:01,834
Halt!

400
00:52:31,206 --> 00:52:33,025
Where do you think
you're going with them?

401
00:52:33,125 --> 00:52:35,611
You don't expect me to
leave them, do you?

402
00:52:35,711 --> 00:52:37,321
Don't be an ass, Norman.

403
00:52:37,421 --> 00:52:39,073
There's barely enough
room on here for us!

404
00:52:39,173 --> 00:52:42,375
When I want your opinion,
I'll ask for it.

405
00:52:44,303 --> 00:52:46,170
Are we taking you or the bags?

406
00:57:43,769 --> 00:57:45,553
What is it?

407
00:57:46,355 --> 00:57:48,674
Damn. Damn!
There's a goddamn mud bank.

408
00:57:48,774 --> 00:57:50,891
We'll have to push it across.
Everyone out!

409
00:58:13,840 --> 00:58:16,584
Rose! Get in the boat.
Steer the rudder.

410
00:58:43,287 --> 00:58:45,946
Keith! Keith!

411
00:58:57,050 --> 00:58:59,203
Are you crazy?
Let's get the hell out of here!

412
00:58:59,303 --> 00:59:01,962
Keith, we're sailing!

413
00:59:03,390 --> 00:59:05,834
Come about! Come about!

414
00:59:05,934 --> 00:59:07,885
What did you say?

415
00:59:10,522 --> 00:59:12,640
Help me.

416
00:59:26,121 --> 00:59:28,030
Take her in.

417
01:00:23,095 --> 01:00:25,998
Beverly. Beverly.

418
01:00:26,098 --> 01:00:27,881
Where's Beverly?!

419
01:00:34,189 --> 01:00:36,800
- I'm sure she's all right.
- Well, where are they?

420
01:00:36,900 --> 01:00:39,553
Beverly! Beverly!

421
01:00:39,653 --> 01:00:43,181
- Beverly! Beverly!
- Norman! Norman!

422
01:00:43,281 --> 01:00:45,267
- Shut up!
- All right.

423
01:00:45,367 --> 01:00:47,644
All right, all right, all right.

424
01:00:47,744 --> 01:00:49,980
All right, let's just think.
Let's just think.

425
01:00:50,080 --> 01:00:52,774
- No, let's just keep moving.
- Keep moving? Where, where?

426
01:00:52,874 --> 01:00:54,818
- We'll go to the hotel.
- To the hotel?

427
01:00:54,918 --> 01:00:57,529
- Yes. Come on.
- All right, all right, all right.

428
01:01:13,353 --> 01:01:15,554
Which way now?

429
01:01:16,857 --> 01:01:18,974
This way.

430
01:01:57,314 --> 01:01:59,515
Norman.

431
01:02:00,525 --> 01:02:02,393
Norman, wait!

432
01:02:17,042 --> 01:02:19,076
Norman!

433
01:02:19,878 --> 01:02:21,488
Norman!

434
01:05:32,696 --> 01:05:34,931
How's everything down here? All set?

435
01:05:35,031 --> 01:05:37,107
Set as we'll ever be.

436
01:05:44,791 --> 01:05:47,485
- Any sign of them?
- No, nothing.

437
01:05:47,585 --> 01:05:49,578
Beverly?

438
01:05:55,051 --> 01:05:57,203
Where's Chuck?

439
01:05:57,303 --> 01:05:59,247
I don't know.

440
01:05:59,347 --> 01:06:02,799
He just said he didn't wanna
come inside until he had to.

441
01:06:11,276 --> 01:06:13,178
I told you to stay inside.

442
01:06:13,278 --> 01:06:15,437
What are you doing up here anyway?

443
01:06:16,823 --> 01:06:20,275
I thought maybe he had something
up here that might help us.

444
01:06:25,248 --> 01:06:27,532
What's the matter?

445
01:06:28,668 --> 01:06:31,244
Nothing. I'm coming.

446
01:07:09,667 --> 01:07:11,277
What is it now?!

447
01:07:11,377 --> 01:07:14,948
I don't see what the percentage
is of hiding in a refrigerator!

448
01:07:15,048 --> 01:07:16,616
They hate the light, right?

449
01:07:16,716 --> 01:07:18,836
When do you think you're
most likely to see them?

450
01:07:20,303 --> 01:07:22,747
The refrigerator's got
walls two feet thick.

451
01:07:22,847 --> 01:07:25,132
I hope it'll be enough.

452
01:07:32,857 --> 01:07:34,342
You've got your nerve.

453
01:07:34,442 --> 01:07:36,761
We were all supposed to
be in here before dark.

454
01:07:36,861 --> 01:07:39,271
You've put us in danger!
Do you realize that?

455
01:07:44,994 --> 01:07:46,862
What's with him?

456
01:07:47,789 --> 01:07:50,991
Beverly, it's gonna be
a long night for all of us.

457
01:08:19,988 --> 01:08:21,772
Everybody ready for lights out?

458
01:08:55,982 --> 01:08:57,641
Chuck, do you feel all right?

459
01:09:01,571 --> 01:09:03,271
Chuck, what's the matter?

460
01:09:05,742 --> 01:09:07,519
You've got to let me out of here.

461
01:09:07,619 --> 01:09:09,312
Christ!

462
01:09:09,412 --> 01:09:11,981
Not now. We can't do it now.

463
01:09:12,081 --> 01:09:15,235
Just let me out.
You don't have to come.

464
01:09:15,335 --> 01:09:18,488
I can't stay in here any longer.

465
01:09:20,381 --> 01:09:23,243
I didn't want to come here.
He came and got me.

466
01:09:23,343 --> 01:09:25,161
I thought it would be okay.

467
01:09:25,261 --> 01:09:28,004
Sometimes, I can
control it, but this...

468
01:09:28,806 --> 01:09:32,544
Please, let me out!
I'll take my chances!

469
01:09:32,644 --> 01:09:34,087
It's too late!

470
01:09:34,187 --> 01:09:36,256
They'll find us.
You're gonna have to control it!

471
01:09:54,499 --> 01:09:57,443
You all stay inside!
I can't control it!

472
01:09:57,543 --> 01:10:00,405
I don't have any choice.
Now... now, get out of the way!

473
01:10:00,505 --> 01:10:03,199
- Put that thing down!
- Are you insane?!

474
01:10:03,299 --> 01:10:05,618
If he fires that thing in here,
we'll all suffocate to death!

475
01:10:05,718 --> 01:10:07,036
Let him go if he wants to!

476
01:10:07,136 --> 01:10:09,330
If I open and close that door,
one of them might hear it.

477
01:10:09,430 --> 01:10:13,251
- Then where will we be?
- Open the goddamn door!

478
01:10:13,351 --> 01:10:17,130
- Open the door, please.
- Open it, open it, open it!

479
01:10:17,230 --> 01:10:20,974
All right, just let me look.

480
01:10:23,361 --> 01:10:25,896
Open it! Open it!

481
01:10:32,120 --> 01:10:34,230
Give me the flashlight!

482
01:10:34,330 --> 01:10:36,316
Then we'll be left with
nothing but an oil lamp!

483
01:10:36,416 --> 01:10:37,692
You don't need it.

484
01:10:37,792 --> 01:10:40,153
I need all the help I can get
out there, now let me have it!

485
01:10:45,927 --> 01:10:47,331
My eyes!

486
01:10:54,559 --> 01:10:56,920
Help me! I can't see!

487
01:13:49,442 --> 01:13:51,351
- What is it?
- Boiler.

488
01:16:17,214 --> 01:16:19,158
Rose?

489
01:16:19,258 --> 01:16:21,118
Keith?

490
01:16:21,218 --> 01:16:23,086
Is that you?

491
01:17:52,184 --> 01:17:53,878
Come on.

492
01:17:53,978 --> 01:17:55,687
Come on.

493
01:19:40,000 --> 01:19:42,076
The oars!

494
01:22:31,547 --> 01:22:34,707
No!

495
01:22:42,933 --> 01:22:47,178
I don't know how long that dinghy
floated around with me lying in it.

496
01:22:48,355 --> 01:22:53,476
All I can remember is the sound of
the water slapping against the side.

497
01:22:55,988 --> 01:22:59,315
Then I heard the engine
sound getting closer.

498
01:23:00,117 --> 01:23:03,527
That was when I realized
I was still alive.

499
01:23:06,081 --> 01:23:10,368
I don't know how long that dinghy
floated around with me lying in it.

500
01:23:12,045 --> 01:23:17,583
All I can remember is the sound of
the water slapping against the side.

501
01:23:30,105 --> 01:23:33,891
It's only now,
I remember any of it at all.

