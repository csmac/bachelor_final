1
00:01:17,411 --> 00:01:22,124
Whoo! Whoo-hoo!
Oh, yeah!

2
00:01:22,207 --> 00:01:25,502
Okay, you're great.

3
00:01:25,586 --> 00:01:28,630
- You got the line?
- Yeah, come on up, slowpoke.

4
00:01:34,720 --> 00:01:38,307
Help!

5
00:01:39,683 --> 00:01:43,437
You okay? You know,
we're 50 miles from anybody.

6
00:01:43,520 --> 00:01:46,440
- Who are you screamin' for?
- You!

7
00:01:46,523 --> 00:01:48,942
Come on.

8
00:01:50,027 --> 00:01:52,321
Just pull me up!

9
00:01:54,740 --> 00:01:56,700
Rich?

10
00:01:58,368 --> 00:02:00,662
Just pull me up.

11
00:02:02,122 --> 00:02:07,127
All right, don't be a dick,
Richie. I need a pull.

12
00:02:07,211 --> 00:02:09,338
Rich?

13
00:02:09,421 --> 00:02:12,132
Rich?

14
00:02:13,884 --> 00:02:16,512
What...

15
00:05:41,592 --> 00:05:45,012
When you plant seed
into your own kin, you anger God.

16
00:05:45,095 --> 00:05:50,350
Still have not been able to
locate the two college students.

17
00:05:50,434 --> 00:05:54,354
Richard Stoker and Halley Smith
have been missing since last Monday...

18
00:05:54,438 --> 00:05:56,523
after failing to return
from a weekend of rock...

19
00:07:07,678 --> 00:07:10,264
Hey, how are you doin'?

20
00:07:10,347 --> 00:07:13,767
Do you have any idea
what's going on up here?

21
00:07:15,102 --> 00:07:17,521
Tractor jackknifed
about five mile up.

22
00:07:17,604 --> 00:07:19,815
Spilled chemicals and
shit all over the road.

23
00:07:19,857 --> 00:07:22,150
Any idea how long
it'll take to clean up?

24
00:07:22,234 --> 00:07:25,988
- Couple hours.
- Couple hours?

25
00:07:26,029 --> 00:07:29,157
You in a hurry?

26
00:07:29,199 --> 00:07:32,661
Yeah, I need to get
to Raleigh tonight.

27
00:07:32,744 --> 00:07:35,581
What you oughta do
is get back in your car...

28
00:07:35,664 --> 00:07:37,958
Fix your hair a couple
hundred more times...

29
00:07:38,041 --> 00:07:40,836
Thank you.

30
00:08:03,358 --> 00:08:07,029
Hey, Doris.
This is Chris Flynn.

31
00:08:07,112 --> 00:08:09,865
I have a 7:00 p.m.
Interview with Mr Keller.

32
00:08:09,948 --> 00:08:12,409
I'm, uh, running
about 30 minutes late.

33
00:08:12,492 --> 00:08:14,703
I was wondering if you could...

34
00:08:14,786 --> 00:08:17,748
Hello? Hello?

35
00:08:17,789 --> 00:08:20,542
I'm not gonna make this.

36
00:09:08,465 --> 00:09:11,635
Excuse me, sir.
Do you have a pay phone?

37
00:09:23,730 --> 00:09:27,609
Uh, this one's not working. Do you
have another phone I could use?

38
00:09:27,651 --> 00:09:30,654
Long distance?

39
00:09:30,696 --> 00:09:33,282
What isn't long distance
from here, right?

40
00:09:33,323 --> 00:09:35,284
You cuttin' wise
with me, son?

41
00:09:35,325 --> 00:09:38,787
No, sir, I'm just...
I'm runnin' behind...

42
00:09:38,871 --> 00:09:41,790
and I really need to make a call.

43
00:09:41,832 --> 00:09:44,626
Well, that'n there is my only phone.

44
00:09:44,710 --> 00:09:46,753
Right.

45
00:09:48,547 --> 00:09:52,801
Well, uh, the highway's
really jammed up.

46
00:09:52,843 --> 00:09:55,345
Do you know of another
route heading south?

47
00:09:55,429 --> 00:09:58,140
- No.
- No.

48
00:10:04,771 --> 00:10:07,357
Why is this Bear Mountain Road
dotted like this?

49
00:10:07,441 --> 00:10:10,152
- Dirt.
- Dirt road?

50
00:10:10,194 --> 00:10:13,447
'Fraid they ain't got
around to pavin' it yet.

51
00:10:13,530 --> 00:10:17,117
It looks like it runs into the highway
about 15, 20 miles, is that right?

52
00:10:17,201 --> 00:10:19,328
If you say so.

53
00:10:21,413 --> 00:10:24,708
It could work.
Thank you very much.

54
00:10:24,791 --> 00:10:26,793
You take care, okay?

55
00:10:38,222 --> 00:10:42,017
You're the one gonna
need to take care.

56
00:12:09,980 --> 00:12:12,316
We just got nailed, man.

57
00:12:12,399 --> 00:12:15,110
- Jesus Christ.
- Shit.

58
00:12:15,194 --> 00:12:17,446
- Goddamn drunken hillbilly!
- Hey. Hey, you all right?

59
00:12:17,487 --> 00:12:20,949
I'm so sorry. I was just...

60
00:12:20,991 --> 00:12:22,993
- Jesus.
- I was just comin' around the bend.

61
00:12:23,035 --> 00:12:25,829
- I didn't even see... I got distracted.
- My God, you could've died!

62
00:12:25,913 --> 00:12:28,624
- Are you guys all okay?
- Yeah, yeah, you wanna sit down?

63
00:12:28,707 --> 00:12:31,251
- You might be in shock.
- What were you doing driving so goddamn fast?

64
00:12:31,335 --> 00:12:34,463
- All right, all right.
- Why was your truck parked in the middle of the road?

65
00:12:34,546 --> 00:12:36,632
Because we blew out
our tires, asshole!

66
00:12:36,673 --> 00:12:40,469
- I'll pay for whatever damage was done.
- That's my mom's car!

67
00:12:40,511 --> 00:12:42,513
All right, Frannie.
The man said he'd pay for...

68
00:12:42,554 --> 00:12:45,307
Oh, looks like you killed
my bike here too.

69
00:12:45,390 --> 00:12:48,936
I'm sorry. L...
How did you blow out your tires?

70
00:12:49,019 --> 00:12:51,480
Someone left some barbed wire
in the middle of the road.

71
00:12:51,563 --> 00:12:54,024
I can't believe someone
just dropped it there.

72
00:12:54,107 --> 00:12:57,361
Nobody dropped anything.

73
00:12:57,444 --> 00:12:59,655
I just found this tied to a tree.
Somebody did this.

74
00:12:59,738 --> 00:13:02,658
Southern hospitality at its finest.

75
00:13:02,699 --> 00:13:05,327
- Redneck assholes.
- I'm gonna try and find a phone.

76
00:13:05,369 --> 00:13:08,163
I think I saw a gas station
like a couple miles back.

77
00:13:08,205 --> 00:13:10,874
- I'm gonna come with you.
- There is no phone.

78
00:13:10,958 --> 00:13:14,378
- I was just there.
- Why don't we just wait for
someone else to come along?

79
00:13:14,419 --> 00:13:16,713
What, like Speed Racer here?

80
00:13:16,797 --> 00:13:21,051
- I'm not going anywhere.
- Okay, you guys go, and
we'll just stay here, Francine.

81
00:13:21,134 --> 00:13:24,221
- And get high.
- Yeah. So what?

82
00:13:24,304 --> 00:13:26,515
- Careful. They don't like stoners.
- Who are you calling a stoner?

83
00:13:26,598 --> 00:13:28,809
- Your mom.
- Hey, what's your name?

84
00:13:28,892 --> 00:13:30,686
Chris Flynn.

85
00:13:30,769 --> 00:13:33,146
You hurt, Chris Flynn?

86
00:13:33,230 --> 00:13:36,233
- No, I'm fine.
- Good.

87
00:13:38,402 --> 00:13:41,196
- 'Cause you're the mule.
- Oh, no.

88
00:13:41,280 --> 00:13:44,491
You know, I can carry that. You just
had the whole car accident thing.

89
00:13:44,575 --> 00:13:46,952
Yeah, we'll just stay here
and maybe get hit again.

90
00:13:47,035 --> 00:13:49,163
- Are you guys sure you're cool?
- Yeah.

91
00:13:49,246 --> 00:13:51,540
Yeah, we'll be just fine.
Thank you.

92
00:13:51,623 --> 00:13:55,544
- Mess you up.
- Yeah, smoke it up there, uh, Skippy.

93
00:13:55,586 --> 00:13:58,714
- Yeah, have fun.
- I don't fuckin'believe this.

94
00:14:01,550 --> 00:14:03,844
Your mom's gonna kick your ass.

95
00:14:05,637 --> 00:14:08,265
God, I cannot get out of my head
what just happened.

96
00:14:08,348 --> 00:14:12,060
- Are you sure you're all right? Yeah?
- Yeah. I'm fine.

97
00:14:12,144 --> 00:14:14,479
What are you guys
doing out here?

98
00:14:14,563 --> 00:14:17,733
Uh, uh, oh, camping.
Uh, I don't know. Actually, we're lost.

99
00:14:17,774 --> 00:14:21,904
You know, a lot of people say...
who have been through similar
traumatic experiences...

100
00:14:21,987 --> 00:14:26,450
that the following moments kind of feel
like you're moving kind of in slow motion.

101
00:14:26,533 --> 00:14:28,785
- Do you feel like that?
- No.

102
00:14:28,869 --> 00:14:32,539
I should tell you that my voice
is fairly low and normal speed.

103
00:14:37,377 --> 00:14:41,381
- Pretty good.
- Mm-hmm.

104
00:14:45,052 --> 00:14:47,137
Where'd you get this?

105
00:14:47,221 --> 00:14:50,098
Um, I got it out of my
dad's room, actually.

106
00:14:50,182 --> 00:14:53,435
- You can finish it.

107
00:14:57,856 --> 00:15:00,275
I can't believe
they called us stoners.

108
00:15:00,359 --> 00:15:03,403
Dickheads.

109
00:15:03,487 --> 00:15:06,448
Are you sure
you know where we're going?

110
00:15:06,490 --> 00:15:09,284
Yeah. There was a map
at the gas station.

111
00:15:09,326 --> 00:15:11,411
Oh, I'm so tired already.

112
00:15:11,495 --> 00:15:14,665
I'm starving,
and I'm being eaten by ants.

113
00:15:14,748 --> 00:15:16,792
Are there any on my back?

114
00:15:16,875 --> 00:15:20,337
No, nothing there.
Oh, hey-ho! I call it. Uh, squirrel.

115
00:15:20,379 --> 00:15:23,841
- Well, you said you were hungry, Carly.
- I don't think it's a squirrel.

116
00:15:23,882 --> 00:15:26,885
- It's a mink.
- Really? How do you know?

117
00:15:26,969 --> 00:15:29,263
- Probably ran over it.

118
00:15:29,346 --> 00:15:33,600
In medical school, they make you
work on animal cadavers
before you get to the real thing.

119
00:15:33,684 --> 00:15:35,602
- It's a mink.
- Oh.

120
00:15:37,229 --> 00:15:39,439
It's a mink?

121
00:15:39,523 --> 00:15:42,109
I need to be in Raleigh by 7:00.

122
00:15:42,192 --> 00:15:45,153
Hmm. Better get
a move-on, then.

123
00:15:45,279 --> 00:15:47,781
You know, we should've
just taken her to New York.

124
00:15:47,865 --> 00:15:51,368
No. You know how she loves
this outdoor stuff.

125
00:15:51,451 --> 00:15:53,412
Yeah.

126
00:15:53,495 --> 00:15:56,331
If you ask me, though,
nature sucks.

127
00:15:56,373 --> 00:16:01,336
Well, the next time she gets dumped,
we'll take her to New York.

128
00:16:04,715 --> 00:16:07,009
Drop your pants.

129
00:16:07,092 --> 00:16:09,511
What?

130
00:16:09,553 --> 00:16:11,638
When do people
always show up, Evan?

131
00:16:11,722 --> 00:16:14,391
What are we doing?

132
00:16:14,474 --> 00:16:16,810
Consider it an experiment
in probability theory.

133
00:16:16,894 --> 00:16:18,812
Really?

134
00:16:21,398 --> 00:16:24,359
Mmm. I love you.

135
00:16:24,401 --> 00:16:26,361
I know.

136
00:16:26,403 --> 00:16:29,656
Now get them
trousers off, boy. Don't be a sissy.

137
00:16:29,740 --> 00:16:32,576
- All right.

138
00:16:34,411 --> 00:16:38,332
You know, I've been thinking
about this whole wedding thing...

139
00:16:38,415 --> 00:16:40,876
and I think we should
take a trip down to Mexico.

140
00:16:40,918 --> 00:16:43,962
- Mm-hmm.
- I'm talking about a long weekend...

141
00:16:44,046 --> 00:16:48,342
margaritas, the two of us
on a beach alone, sunset.

142
00:16:48,425 --> 00:16:52,804
And, you know,
I'm talking about just, just eloping.

143
00:16:52,888 --> 00:16:56,850
- You know what I mean?
- Uh, wait a second.

144
00:16:56,934 --> 00:16:59,269
You wanna know what I think?

145
00:16:59,353 --> 00:17:01,271
Yes, please.

146
00:17:01,355 --> 00:17:05,025
- I think if you ever
want to get in my pants again,
- Affirmative.

147
00:17:05,108 --> 00:17:07,778
This is the last time
you use the "E" word.

148
00:17:07,861 --> 00:17:11,865
Okay? Okay.

149
00:17:11,949 --> 00:17:14,117
Yeah, it's a disgusting word.

150
00:17:14,201 --> 00:17:16,286
Hey, guys, hold on.

151
00:17:19,373 --> 00:17:22,417
Hey, is anyone up there?

152
00:17:24,044 --> 00:17:26,129
Hello!

153
00:17:26,213 --> 00:17:30,050
- There must be people nearby.
- Anyone up there?

154
00:17:30,133 --> 00:17:34,096
- Somebody had to start it.

155
00:17:34,179 --> 00:17:36,139
Let's keep moving.

156
00:17:38,642 --> 00:17:40,561
Hello?

157
00:17:45,148 --> 00:17:47,401
Freaky.

158
00:17:51,446 --> 00:17:54,616
Oh, look at this.
It's fucked!

159
00:17:54,700 --> 00:17:58,245
Nothing. Nothing.

160
00:18:01,957 --> 00:18:03,959
Hey, did you find
anything to eat?

161
00:18:04,042 --> 00:18:06,628
They took all my Power Bars.

162
00:18:06,670 --> 00:18:09,464
Um, no.

163
00:18:09,506 --> 00:18:13,093
No. Sorry.

164
00:18:16,847 --> 00:18:20,642
Damn it. Scott and Carly
took all our friggin' sunscreen.

165
00:18:23,061 --> 00:18:25,272
Does he have any good CD's?

166
00:18:26,648 --> 00:18:30,485
Mmm. Definitely not.

167
00:18:34,781 --> 00:18:38,327
You know, I think this guy
must be some kind of doctor.

168
00:18:38,368 --> 00:18:42,372
Maybe we could get him to write us
some prescriptions when he gets back.

169
00:18:42,456 --> 00:18:45,584
He doesn't have
any smokes either.

170
00:18:45,667 --> 00:18:47,628
Almost out.

171
00:18:55,802 --> 00:18:57,763
Evan?

172
00:19:09,399 --> 00:19:11,944
Evan? Where are you?

173
00:19:13,820 --> 00:19:16,031
Hey, baby,
what are you doing?

174
00:19:16,073 --> 00:19:20,869
I'm just... amusing myself.
Whoooo!

175
00:19:20,911 --> 00:19:23,705
You, uh, getting
anything there, chatty?

176
00:19:23,789 --> 00:19:26,124
No, nothing.
I'm gonna miss this thing.

177
00:19:26,208 --> 00:19:30,379
- Why didn't you just fly?
- I put all of my money
into that car back there.

178
00:19:30,462 --> 00:19:34,883
Okay, forget eloping, but there is an idea we
should discuss about the band for our wedding.

179
00:19:34,967 --> 00:19:39,221
I think we should hire a... you know,
a Frank Sinatra-type, uh, lead singer...

180
00:19:39,304 --> 00:19:42,474
as opposed to
a James Brown-type lead singer...

181
00:19:42,558 --> 00:19:45,477
because, you know, really, let's be
honest, there's only oneJames Brown...

182
00:19:45,561 --> 00:19:48,897
and, you know, a faux James Brown
is really quite intolerable.

183
00:19:52,776 --> 00:19:56,530
Evan? Where are you?

184
00:19:56,613 --> 00:19:59,741
You pissing or something?

185
00:20:05,372 --> 00:20:07,291
Evan?

186
00:20:37,154 --> 00:20:41,450
You know, plus he'd be grinding his hips,
and my grandmother would be...

187
00:20:41,492 --> 00:20:46,622
yes, revolted
but also strangely fascinated
in a completely disturbing way...

188
00:20:46,705 --> 00:20:49,166
and, um...

189
00:20:51,335 --> 00:20:53,295
Baby?

190
00:20:55,839 --> 00:20:58,717
Carly, I wa...
I was kidding.

191
00:20:58,800 --> 00:21:01,428
Carly?

192
00:21:04,890 --> 00:21:07,809
- Evan?

193
00:21:34,419 --> 00:21:37,798
Sweetheart?
Hey, guys, hold up!

194
00:21:37,840 --> 00:21:41,385
Guys, hold up
for a second, all right?

195
00:21:41,468 --> 00:21:45,138
Baby, seriously,
this isn't funny.

196
00:21:46,974 --> 00:21:49,393
- Jesus God!

197
00:21:49,476 --> 00:21:52,396
- Baby!

198
00:21:52,479 --> 00:21:56,149
- L...
- Oh, I'm so sorry.

199
00:21:56,191 --> 00:21:59,069
I could've killed you, woman.
I'm-I'm extremely dangerous.

200
00:21:59,152 --> 00:22:01,864
You wanna know
why I love you so much?

201
00:22:01,947 --> 00:22:04,074
Because you're so fun
to play with.

202
00:22:04,158 --> 00:22:07,202
Is that why you love me?
That really scared me, baby.

203
00:22:07,286 --> 00:22:10,414
- I'm sorry.
- That really scared me.
Would you not do that?

204
00:22:10,497 --> 00:22:12,916
Hello.!

205
00:22:15,419 --> 00:22:17,880
Hello.!

206
00:22:21,049 --> 00:22:24,511
There's, uh, nothing
like stating the obvious, huh?

207
00:22:24,595 --> 00:22:28,891
- Hey, baby, look at this.

208
00:22:28,974 --> 00:22:33,729
Whoo! Hey, Carly, I think
this would be good time for you
to confront your fear of...

209
00:22:33,770 --> 00:22:37,065
Don't!
Okay, that is not funny.

210
00:22:37,107 --> 00:22:39,276
Look who's scared now.

211
00:22:42,946 --> 00:22:44,907
- Sorry.
- Whatever.

212
00:22:44,948 --> 00:22:47,951
Just get me to a motel room...

213
00:22:48,035 --> 00:22:50,204
run me a very hot bath...

214
00:22:50,245 --> 00:22:53,916
and be prepared to provide me
with a lot of orgasms.

215
00:22:53,999 --> 00:22:57,419
- Oh!
- I think they need to be alone.

216
00:22:57,461 --> 00:23:00,255
What do you expect?
They just got engaged.

217
00:23:00,339 --> 00:23:02,466
They're happy.
It's a hard thing to find.

218
00:23:02,549 --> 00:23:04,468
Yeah.

219
00:23:04,551 --> 00:23:06,595
- Got it?
- Limber tongue. Got it.

220
00:23:06,678 --> 00:23:08,597
You're stupid.

221
00:23:19,149 --> 00:23:21,944
Maybe we shouldn't
have left Francine and Evan.

222
00:23:21,985 --> 00:23:24,488
Don't worry. They're fine.

223
00:23:24,571 --> 00:23:27,658
- You guys okay?
- Super.

224
00:23:31,495 --> 00:23:34,331
Well, wait, guys.
This road isn't on here.

225
00:23:34,414 --> 00:23:37,292
That's because you don't have
the Redneck World Atlas.

226
00:23:37,376 --> 00:23:41,171
Uh, uh, uh, uh, uh. I think
we've just been saved, kids.

227
00:23:41,255 --> 00:23:43,382
- Can I get a...

228
00:23:51,056 --> 00:23:54,726
- Make this quick.

229
00:23:54,810 --> 00:23:57,437
Actually, maybe we
should keep walking.

230
00:23:57,521 --> 00:24:00,774
What? The next house is gonna have
a white picket fence?

231
00:24:00,816 --> 00:24:03,277
Yeah, if there is
a next house.

232
00:24:05,946 --> 00:24:08,156
- Hey.
- What?

233
00:24:25,507 --> 00:24:27,926
Hello?

234
00:24:28,010 --> 00:24:30,929
All right, this isn't right.

235
00:24:31,013 --> 00:24:32,931
Hello?

236
00:24:38,604 --> 00:24:41,231
Hello? Anybody home?

237
00:24:43,942 --> 00:24:46,028
- Whoa, baby! Could you... Shh!

238
00:24:46,111 --> 00:24:48,655
Hey, hey, what are you doing?

239
00:24:48,739 --> 00:24:52,534
I was gonna see if they had a phone.
You guys can wait out here if you want.

240
00:24:52,618 --> 00:24:55,204
You can't just go barging
into someone's house like that.

241
00:24:55,287 --> 00:24:58,040
Yeah, 'cause, you know,
I'm just thinkin'...

242
00:24:58,081 --> 00:25:00,751
West Virginia, trespassing...
not a great combination.

243
00:25:00,834 --> 00:25:03,086
Look, I need to pee.

244
00:25:03,170 --> 00:25:06,089
Well, I need to remind you
of a little movie called Deliverance.

245
00:25:09,384 --> 00:25:12,888
Guys, we got two wrecked cars.
I mean, we really need a phone.

246
00:25:21,522 --> 00:25:23,774
Hello?

247
00:25:23,857 --> 00:25:26,235
Hello?

248
00:25:26,318 --> 00:25:28,278
Carly.

249
00:25:34,076 --> 00:25:36,036
Hello?

250
00:25:51,844 --> 00:25:53,762
What the fuck?

251
00:25:53,846 --> 00:25:56,849
Let's just do
what we gotta do and get out ofhere.

252
00:25:59,852 --> 00:26:03,105
Hey, I am not sensing a phone here.

253
00:26:03,146 --> 00:26:05,399
Hello!

254
00:26:05,440 --> 00:26:08,068
Okay, who lives here?

255
00:26:08,110 --> 00:26:11,905
I don't know, but can you
help me find a bathroom?

256
00:26:11,947 --> 00:26:14,074
Baby, I think
this is the bathroom.

257
00:26:14,158 --> 00:26:18,912
Oh, fuck.

258
00:26:20,956 --> 00:26:23,792
Oh, my God.
Look at this place.

259
00:26:23,876 --> 00:26:26,628
Seriously, guys,
I think we should go.

260
00:26:26,712 --> 00:26:28,964
Help me find the bathroom.
Then we'll go.

261
00:26:29,006 --> 00:26:31,592
Baby, what if this place
belongs to some kind of cult?

262
00:26:31,633 --> 00:26:34,303
I read in Newsweek how
economically depressed places...

263
00:26:34,386 --> 00:26:37,598
are like breeding grounds for
all kinds of apocalyptic visionaries.

264
00:26:37,639 --> 00:26:40,434
Order of the Solar Temple, Church of
the Lamb of God, the Chijon family.

265
00:26:40,475 --> 00:26:42,936
- Remember them?
- Hey, brainiac!

266
00:26:42,978 --> 00:26:44,938
Shh!

267
00:26:46,148 --> 00:26:48,942
All right.
Maybe that door.

268
00:26:57,910 --> 00:27:00,329
- Christ!

269
00:27:00,370 --> 00:27:03,832
- You okay?
- It's just a paddle. It's all right.

270
00:27:07,669 --> 00:27:10,214
Whoa.

271
00:27:12,674 --> 00:27:15,302
God, look at this place.

272
00:27:15,344 --> 00:27:18,180
Yeah. It's like
the garage sale from hell.

273
00:27:18,263 --> 00:27:20,807
- Jesus.

274
00:27:22,893 --> 00:27:25,687
Can you believe this?

275
00:27:28,649 --> 00:27:32,236
Jesus, these guys
are ripping people off.

276
00:27:32,319 --> 00:27:36,740
I mean, this is like
$30,000 worth of stuff.

277
00:27:39,576 --> 00:27:42,037
- Chris.
- Mm-hmm.

278
00:27:42,120 --> 00:27:44,581
We should leave.

279
00:27:46,500 --> 00:27:49,044
This is kind of creepy.

280
00:27:53,048 --> 00:27:55,342
- Yeah, this is it.
- Okay. Be quick.

281
00:27:55,384 --> 00:27:58,053
- Okay!
- All right? Two minutes.

282
00:27:58,136 --> 00:28:00,222
All right?
I'll be here.

283
00:29:08,207 --> 00:29:10,125
Ew.!

284
00:29:52,334 --> 00:29:54,378
Guys, guys, check this out.

285
00:29:55,963 --> 00:29:57,297
Oh, no.

286
00:30:01,051 --> 00:30:04,137
Oh, God. Oh, God.
Scott. Scott!

287
00:30:04,179 --> 00:30:06,807
- - Scott, we have to go now!

288
00:30:08,392 --> 00:30:10,644
- Jess! Jessie!
- Jess!

289
00:30:10,686 --> 00:30:13,897
- Hey! Guys, what is it?
- We need to get back to your friends.

290
00:30:13,981 --> 00:30:15,148
- What about a phone?
- There is no phone. Shit!

291
00:30:15,148 --> 00:30:16,859
- What about a phone?
- There is no phone. Shit!

292
00:30:19,194 --> 00:30:22,531
There's a back door.
I saw a back door. Come on.

293
00:30:22,573 --> 00:30:24,533
- Go, go!

294
00:30:24,575 --> 00:30:29,371
- Okay, hurry. Shit! No!
- Shit!

295
00:30:29,454 --> 00:30:34,418
Scott.! Scott.!
It's not gonna budge.! Come on.!

296
00:36:47,666 --> 00:36:50,043
Go.

297
00:37:03,765 --> 00:37:05,309
Go.

298
00:37:20,407 --> 00:37:23,952
Go! Go!

299
00:37:24,036 --> 00:37:26,663
- Go! Go!
- Go! Go! Go! Go!

300
00:37:26,747 --> 00:37:29,583
- Go, you guys! Run!

301
00:37:29,625 --> 00:37:32,336
- Go quickly.
- Come on.

302
00:37:40,469 --> 00:37:42,971
- Where are they going?
- I don't know.

303
00:37:43,013 --> 00:37:46,433
Jesus, did that
really just happen?

304
00:37:46,475 --> 00:37:49,019
Do you think Evan's okay?

305
00:37:51,772 --> 00:37:54,525
- Please.
- I can't. I can't.

306
00:37:54,608 --> 00:37:56,693
Carly, come on!
Get up!

307
00:37:56,777 --> 00:38:00,030
You didn't see what
they did to Francine!

308
00:38:00,113 --> 00:38:04,159
- You didn't see what we saw!
- Carly! Carly! Sweet pea!

309
00:38:04,243 --> 00:38:06,787
Look at me. We are gonna
get out of this, I swear to you.

310
00:38:06,870 --> 00:38:10,374
We are. Okay? We're gonna find a road.
We're gonna get to a town.

311
00:38:10,457 --> 00:38:15,003
And we're going home.
We're gonna get married. All right?

312
00:38:15,087 --> 00:38:17,714
And we are never
going into the woods again.

313
00:38:19,216 --> 00:38:21,385
- All right?
- We gotta go.

314
00:38:21,468 --> 00:38:24,221
- Come on. This way!
- Let's go! Come on!

315
00:38:38,902 --> 00:38:40,863
What is this?

316
00:38:51,415 --> 00:38:53,500
Jesus.

317
00:38:55,252 --> 00:38:57,212
All these people.

318
00:38:58,964 --> 00:39:02,593
Baby. Fuck.

319
00:39:07,222 --> 00:39:10,559
How can they do this?
I don't understand.

320
00:39:10,642 --> 00:39:14,730
- How'd they get away with this?
- Oh, my God. All these people.

321
00:39:14,813 --> 00:39:18,734
Maybe one of them still runs.
Look for keys.

322
00:39:18,817 --> 00:39:21,069
None of these cars
are gonna run.

323
00:39:21,195 --> 00:39:24,907
Come on. Look at them.
Oh, fuck.

324
00:39:32,080 --> 00:39:34,249
Get down! Get down!

325
00:39:34,333 --> 00:39:36,126
- Come on!
- Okay.

326
00:40:09,993 --> 00:40:12,287
What do we do?

327
00:40:12,371 --> 00:40:14,581
- They left their truck running.
- So?

328
00:40:14,623 --> 00:40:17,251
- We gotta take it.
- How are we supposed to do that?

329
00:40:17,292 --> 00:40:20,295
One of us leads them
in the wrong direction...

330
00:40:20,379 --> 00:40:23,507
and the rest commandeer the truck
so they'll run for the runner.

331
00:40:25,259 --> 00:40:27,261
It's a classic military move.

332
00:40:27,302 --> 00:40:30,097
- Who goes?
- Me.

333
00:40:30,138 --> 00:40:33,934
Hey! Hey! Hey! Hey!

334
00:40:33,976 --> 00:40:36,812
Over here!

335
00:40:44,361 --> 00:40:47,865
- Oh, shit. Oh, God.

336
00:40:50,492 --> 00:40:52,786
- Go!

337
00:40:52,828 --> 00:40:56,456
Listen. Help him. Get to the truck.
I'll meet you there.

338
00:40:56,540 --> 00:40:58,292
I love you.

339
00:40:58,333 --> 00:41:02,296
Fuckers!
Asshole motherfuckers!

340
00:41:02,379 --> 00:41:04,464
Assholes!

341
00:41:07,050 --> 00:41:08,969
He's okay. Shh, shh.

342
00:41:20,063 --> 00:41:22,357
Come on.

343
00:41:29,948 --> 00:41:32,367
Here, here. You're okay.

344
00:41:34,328 --> 00:41:37,206
Go ahead. Go, go, go.
Go to the truck.

345
00:41:41,001 --> 00:41:43,879
- Come on. We gotta go.

346
00:41:43,921 --> 00:41:47,174
- I can't.! I can't.!
- Come on! Come on!

347
00:41:47,216 --> 00:41:49,927
Carly, get in!

348
00:42:14,952 --> 00:42:18,247
We gotta find Scott!
We have to find Scott!

349
00:42:28,924 --> 00:42:31,468
Where is he?
Where is he!

350
00:42:34,680 --> 00:42:38,267
There he is!
There he is! Scott!

351
00:42:38,350 --> 00:42:42,104
Run, Scott.!
We're over here.! Scott.!

352
00:42:42,145 --> 00:42:45,732
- Scott.! Come on.!
- Scott, let's go!

353
00:42:45,816 --> 00:42:49,319
- Come on!

354
00:42:49,403 --> 00:42:52,614
- Come on, Scott! Scott!
- Let's go.!

355
00:42:52,656 --> 00:42:54,950
- Scott, come on!
- Come on, Scott!

356
00:42:54,992 --> 00:42:57,786
- Come on, Scott!
- Scott! Scott! What are you doing?

357
00:42:57,828 --> 00:43:00,414
- Scott, what's wrong?
- Scott!

358
00:43:00,497 --> 00:43:02,541
Scott! No!

359
00:43:02,624 --> 00:43:05,502
- No!

360
00:43:05,586 --> 00:43:10,424
Let go of me.! Let me go.!

361
00:43:10,465 --> 00:43:13,594
Scott.!

362
00:43:13,677 --> 00:43:16,597
No!

363
00:43:18,932 --> 00:43:21,476
Drive! Drive!

364
00:43:21,560 --> 00:43:23,770
- No!
- Drive!

365
00:43:31,111 --> 00:43:33,030
Oh, God!

366
00:43:34,656 --> 00:43:37,159
What's happening?

367
00:44:02,059 --> 00:44:04,811
Oh, God, what's happening?

368
00:44:08,816 --> 00:44:13,821
Jess, please,
can we go back? Please!

369
00:44:13,904 --> 00:44:17,366
Carly, we have to
keep going. Okay?

370
00:44:21,370 --> 00:44:25,374
Scott.

371
00:44:42,766 --> 00:44:45,853
Are you sure this is
the road from that map?

372
00:44:45,936 --> 00:44:48,063
It's gotta go somewhere.

373
00:44:48,146 --> 00:44:50,607
This better get us out of here.

374
00:44:56,238 --> 00:44:58,115
Shit.

375
00:45:02,703 --> 00:45:05,581
- Shit!
- Come on!

376
00:45:07,291 --> 00:45:10,711
- Back it up.

377
00:45:13,422 --> 00:45:17,050
- Come on!
- It's not gonna go any farther.
Come on. We have to get out.

378
00:45:20,888 --> 00:45:22,973
We gotta find out
where we are.

379
00:45:23,056 --> 00:45:25,058
The highest ridge is up there.
Come on.

380
00:45:25,100 --> 00:45:28,103
What's the point?
They're gonna find us anyway.

381
00:45:28,145 --> 00:45:32,232
- Come on. Let's go, Carly.
- No. If he's going, I'm not.
He got us into this.

382
00:45:32,274 --> 00:45:35,694
Carly, look at me, okay?
Scott died protecting us.

383
00:45:35,777 --> 00:45:39,239
We need to keep ourselves alive,
or it was for nothing. Okay?

384
00:45:39,281 --> 00:45:41,575
We're all in this together, Carl.

385
00:45:41,617 --> 00:45:43,577
Come on.

386
00:45:43,619 --> 00:45:45,829
I want him back.

387
00:45:45,913 --> 00:45:48,081
Shh.

388
00:45:49,791 --> 00:45:51,793
I know, I know.

389
00:45:54,379 --> 00:45:56,924
- Carl, look at me. Okay? Look.

390
00:45:56,965 --> 00:45:59,426
We're gonna stay alive.
We're gonna get out of this.

391
00:45:59,468 --> 00:46:04,097
We're gonna find the police and
make sure those motherfuckers
are punished for this. Okay?

392
00:46:07,726 --> 00:46:09,978
Carly.

393
00:46:10,020 --> 00:46:12,981
Come on, Carly.
You can do it.

394
00:46:14,691 --> 00:46:17,194
It's gonna get dark soon.
Come on.

395
00:46:20,864 --> 00:46:23,158
We need to find
that road quick.

396
00:46:23,200 --> 00:46:25,160
I know.

397
00:46:55,190 --> 00:46:57,151
Just a little bit farther.

398
00:47:01,655 --> 00:47:03,574
Chris!

399
00:47:05,492 --> 00:47:08,620
- It's a bear trap.

400
00:47:43,655 --> 00:47:46,116
- What's up?
- I gotta rest.

401
00:47:55,918 --> 00:47:57,878
- Hey, guys.

402
00:47:57,961 --> 00:48:00,422
Look.

403
00:48:01,924 --> 00:48:05,594
Oh my God.
It's a watchtower.

404
00:48:10,182 --> 00:48:12,434
- Hello! Is anyone up there?
- Shh! Shh!

405
00:48:16,146 --> 00:48:18,941
- I don't think anyone's up there.
- Doesn't matter.

406
00:48:18,982 --> 00:48:21,944
It may have a phone or a radio.
Maybe we can see the road.

407
00:48:22,027 --> 00:48:23,612
You okay?

408
00:48:28,909 --> 00:48:30,911
Come on.

409
00:49:27,801 --> 00:49:30,679
Guys, I don't see
any roads or towns out there.

410
00:49:30,721 --> 00:49:33,265
Let's not stand by the window.
They might see us.

411
00:49:41,648 --> 00:49:44,026
- Hey.
- We're all gonna die.

412
00:49:45,819 --> 00:49:48,822
Carly, I need you to come help me
look around, please.

413
00:50:03,086 --> 00:50:05,255
Hey, Chris.

414
00:50:25,484 --> 00:50:27,861
- Jess.
- Thanks.

415
00:51:06,275 --> 00:51:09,153
Oh, my God.
It's a radio.

416
00:51:13,282 --> 00:51:15,617
Thing looks prehistoric.

417
00:51:15,659 --> 00:51:18,412
- Do you know how to use this?
- Uh, let me try.

418
00:51:18,453 --> 00:51:22,082
- It could still work.
- Don't move anything.

419
00:51:22,166 --> 00:51:24,960
- Keep it on the frequency they had it on.
- It works.

420
00:51:25,002 --> 00:51:28,130
Can anybody hear me? We have
an emergency. Can anybody hear me?

421
00:51:28,213 --> 00:51:30,924
Say "Mayday."

422
00:51:31,008 --> 00:51:34,011
Look, we have a huge emergency.
Please, does anybody hear me?

423
00:51:35,345 --> 00:51:37,431
Someone is injured.
We need help.

424
00:51:37,472 --> 00:51:40,517
Hello. Please, is anybody
out there that can hear me?

425
00:51:40,601 --> 00:51:43,687
- Hello.
- No.

426
00:51:46,690 --> 00:51:49,860
Get down.
Cover the light.

427
00:51:49,943 --> 00:51:51,945
Shh.

428
00:52:06,084 --> 00:52:08,504
I think they're passing us.

429
00:52:19,640 --> 00:52:21,558
This is Ranger Base Emergency.
Do you copy?

430
00:52:21,642 --> 00:52:25,562
- What is your position? Over.

431
00:52:25,646 --> 00:52:28,190
Do you copy?
What is your position? Over.

432
00:52:28,273 --> 00:52:31,693
- I don't know my position. Just help us.
- Shh.!

433
00:52:31,777 --> 00:52:35,030
I need another copy of that.
I repeat. What is your position?

434
00:52:35,113 --> 00:52:37,241
They're coming.

435
00:52:42,621 --> 00:52:44,748
We don't know our position.
We're in a watchtower.

436
00:52:44,832 --> 00:52:47,334
We must be north
of Bear Mountain Road.

437
00:52:47,417 --> 00:52:51,213
Someone is trying to kill us!
They've murdered our friends!
They're after us right now!

438
00:52:51,296 --> 00:52:54,007
I'm having
some trouble hearing you.

439
00:52:54,091 --> 00:52:56,343
If you're in the tower, stay there.
Do you copy?

440
00:52:56,385 --> 00:52:58,720
Yes, we copy! Just hurry, please!
This is an emergency!

441
00:52:58,804 --> 00:53:01,598
- Help me move this. Come on! Come on!
- We'll come to you.

442
00:53:01,682 --> 00:53:04,101
They're after us.!
Someone's been shot.!

443
00:53:04,184 --> 00:53:07,354
Our friends have been killed,
and we need help.! Please.!

444
00:53:07,396 --> 00:53:09,731
Fuck!

445
00:53:35,215 --> 00:53:37,134
What are they doing?

446
00:53:37,217 --> 00:53:39,219
I don't know.

447
00:53:42,931 --> 00:53:45,475
I think they're going down.

448
00:53:45,559 --> 00:53:47,144
Jess.

449
00:54:01,950 --> 00:54:05,287
Hello? Hello?
Is anybody there?

450
00:54:05,370 --> 00:54:07,581
Can you hear us? Hello?

451
00:54:37,986 --> 00:54:41,490
- Jesus, what are they doing?
- We're on fire.

452
00:54:41,573 --> 00:54:43,951
They're trying to burn us out.

453
00:54:45,994 --> 00:54:49,039
Oh, my God.

454
00:54:59,091 --> 00:55:01,510
Carly.!

455
00:55:01,593 --> 00:55:05,138
- Come here!
- Let go of me! Let go!

456
00:55:05,180 --> 00:55:08,559
I'd rather jump than burn to death!

457
00:55:10,269 --> 00:55:12,896
- Calm down.
- You're right.

458
00:55:12,980 --> 00:55:15,649
- What?
- She's right.

459
00:55:15,691 --> 00:55:19,319
- We have to jump. Those branches
are only a few feet down.
- That's more like 20 feet.

460
00:55:19,361 --> 00:55:23,031
I don't want to burn, and I don't want to
be chopped into pieces. We can do this.

461
00:55:23,115 --> 00:55:25,242
I'll go first.

462
00:55:25,325 --> 00:55:27,244
Chris. Hey.

463
00:55:42,134 --> 00:55:46,346
Oh.

464
00:56:15,125 --> 00:56:18,253
Aaah! Chris!
Chris! Aaah!

465
00:56:19,379 --> 00:56:22,549
- Chris!

466
00:56:47,407 --> 00:56:49,910
- You okay?
- Do you think they saw us?

467
00:56:49,993 --> 00:56:53,247
I don't know.

468
00:56:59,628 --> 00:57:02,256
We gotta move.
Come on.

469
00:57:02,339 --> 00:57:04,508
Shit. Shit.

470
00:57:22,609 --> 00:57:24,528
Carly, come on.

471
00:58:30,511 --> 00:58:33,972
I can do this.
I can do this. I can do this.

472
00:58:34,056 --> 00:58:36,850
I can...
I can do this.

473
00:58:38,852 --> 00:58:41,021
- Car...

474
00:58:58,413 --> 00:59:00,541
- Shh.

475
00:59:03,377 --> 00:59:06,964
- Come on. Come on.

476
00:59:27,609 --> 00:59:31,071
- He's coming.
- I know. I have a plan. Come on.

477
00:59:38,620 --> 00:59:40,622
Jessie, this way.

478
01:00:04,563 --> 01:00:06,607
- Can you hold this?
- Why?

479
01:00:06,732 --> 01:00:09,943
'Cause we're gonna knock this fucker
out of the tree. Can you hold it?

480
01:00:11,778 --> 01:00:14,948
- Yeah.
- All right. Don't let go
until I say so. Okay?

481
01:00:15,032 --> 01:00:18,952
- Where are you going?
- I'm gonna make sure he comes this way.

482
01:00:18,994 --> 01:00:21,705
- No. You can't move as quick as I can.
- Jessie.

483
01:00:21,788 --> 01:00:24,708
Jessie.

484
01:00:44,436 --> 01:00:47,564
I'm right here!
Come and get me!

485
01:00:50,943 --> 01:00:53,070
Where are you?

486
01:00:56,573 --> 01:00:58,617
Here I am!

487
01:01:31,275 --> 01:01:34,069
- What...

488
01:01:45,289 --> 01:01:48,041
- Hmm?

489
01:01:54,756 --> 01:01:57,634
- Hey!

490
01:03:36,525 --> 01:03:38,610
Sorry about your friends.

491
01:03:41,363 --> 01:03:44,992
Last Friday I came home,
and there's a message on my machine...

492
01:03:46,160 --> 01:03:49,163
from my boyfriend, who...

493
01:03:49,246 --> 01:03:52,457
says that we shouldn't
be together anymore.

494
01:03:55,711 --> 01:04:00,132
Within 20 minutes
of me... telling Carly...

495
01:04:02,634 --> 01:04:07,347
she had Scott and...
Evan and Francine...

496
01:04:07,389 --> 01:04:10,392
all blowing off work this week
to take me out here.

497
01:04:11,602 --> 01:04:14,062
That's just the kind
of friends they were.

498
01:04:17,399 --> 01:04:19,401
And now they're dead.

499
01:04:28,827 --> 01:04:31,079
It's not your fault.

500
01:04:32,873 --> 01:04:34,875
It's not.

501
01:05:00,651 --> 01:05:02,653
They're here!

502
01:05:10,410 --> 01:05:13,413
No, they're not.
You were dreaming.

503
01:05:15,290 --> 01:05:17,918
I wish.

504
01:06:01,962 --> 01:06:04,631
- How's your leg?
- It hurts.

505
01:06:04,715 --> 01:06:08,468
Which is good.
If it didn't hurt... Road.

506
01:06:08,552 --> 01:06:10,637
- What?
- Road. Look.

507
01:06:10,679 --> 01:06:14,349
- We gotta get down there.
- Take that slope right there.

508
01:06:18,145 --> 01:06:20,522
I can't believe it.

509
01:06:20,606 --> 01:06:24,151
- How are we gonna get you down?
- I'll get down...

510
01:06:25,527 --> 01:06:28,238
- Chris!
- Go!

511
01:06:28,322 --> 01:06:31,492
Chris! No! Chris!

512
01:06:32,576 --> 01:06:35,537
Chris.! Chris.!

513
01:06:35,621 --> 01:06:39,583
Chris! Get off of me!

514
01:06:42,377 --> 01:06:44,004
Jessie!

515
01:06:44,046 --> 01:06:46,924
Jessie!

516
01:06:55,974 --> 01:06:59,394
Hey, stop!
Oh, thank God!

517
01:06:59,436 --> 01:07:01,396
Are you crazy?

518
01:07:01,438 --> 01:07:04,274
- They took her. Come on. We gotta...
- Just calm down.

519
01:07:04,358 --> 01:07:07,528
- We gotta move.
- Are you one of them folks
who called me from the fire tower?

520
01:07:07,569 --> 01:07:11,365
- Will you shut up and listen.
People are dead!
- Dead? What people?

521
01:09:25,541 --> 01:09:28,418
Help.!

522
01:09:28,502 --> 01:09:30,546
No!

523
01:09:34,091 --> 01:09:37,177
- Mmm.
- No.

524
01:09:41,765 --> 01:09:43,851
Help me. Please.

525
01:09:45,227 --> 01:09:48,188
Help me. I'm sorry.
I'm sorry.

526
01:09:48,230 --> 01:09:52,860
I'm sorry. Yeah?
Can you help me?

527
01:09:52,901 --> 01:09:54,945
Please help me.

528
01:09:56,155 --> 01:09:58,240
No!

529
01:09:59,783 --> 01:10:01,285
No!

530
01:10:17,384 --> 01:10:22,973
Help.

531
01:11:20,030 --> 01:11:22,991
Somebody, help me!

532
01:11:26,745 --> 01:11:28,789
No!

533
01:11:51,103 --> 01:11:52,896
Help me!

534
01:12:47,826 --> 01:12:51,205
You're okay. Come on.

535
01:12:51,246 --> 01:12:54,708
Just stay with me. You're all right.
Come on. It's okay.

536
01:12:54,750 --> 01:12:57,377
Keep looking at me.
All right? You're okay.

537
01:12:57,419 --> 01:13:00,464
- Thank you.
- Shh. Everything's okay.

538
01:13:05,969 --> 01:13:08,472
Chris.

539
01:14:00,399 --> 01:14:02,818
Chris!

540
01:14:54,786 --> 01:14:57,706
Chris.

541
01:15:09,384 --> 01:15:12,095
Get off of me!

542
01:15:25,025 --> 01:15:27,027
Shoot them.

543
01:15:27,110 --> 01:15:29,363
I only got one shot left.

544
01:15:36,370 --> 01:15:41,041
Come on, you motherfuckers.
Just die.

