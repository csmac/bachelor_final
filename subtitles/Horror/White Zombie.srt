1
00:02:16,700 --> 00:02:18,100
Looks like a burial.

2
00:02:18,191 --> 00:02:19,900
In the road?

3
00:02:25,204 --> 00:02:27,700
Driver, what is it?

4
00:02:28,200 --> 00:02:30,361
It�s a funeral, Mademoiselle.

5
00:02:30,905 --> 00:02:32,515
They are afraid of the men

6
00:02:32,600 --> 00:02:34,124
who steal dead bodies.

7
00:02:34,630 --> 00:02:38,200
So they dig the grave
in the middle of the road

8
00:02:38,300 --> 00:02:41,700
where people
pass all the time.

9
00:02:50,697 --> 00:02:55,502
Well, that�s a cheerful introduction
for you to our West Indies.

10
00:03:30,280 --> 00:03:33,700
Do you know where is the house of
monsieur Beaumont?

11
00:03:57,901 --> 00:04:00,603
Zombies! Aller vite! Allez!

12
00:04:08,286 --> 00:04:10,700
It felt like hands touching me!

13
00:05:01,188 --> 00:05:02,726
Why did you drive like that, you fool?

14
00:05:02,872 --> 00:05:04,073
We might have been killed!

15
00:05:04,213 --> 00:05:08,000
Worse than that, Monsieur.
We might have been caught.

16
00:05:08,200 --> 00:05:11,000
Caught? By whom? Those men you spoke to?

17
00:05:11,419 --> 00:05:16,200
They are not men Monsieur,
they are dead bodies.

18
00:05:16,652 --> 00:05:17,555
Dead?

19
00:05:17,927 --> 00:05:20,800
Yes, Monsieur. Zombies.

20
00:05:21,617 --> 00:05:23,389
The living dead.

21
00:05:24,166 --> 00:05:29,000
Corpses taken from their graves
who are made to work in sugar mills

22
00:05:29,022 --> 00:05:31,704
and fields at nights.

23
00:05:36,040 --> 00:05:38,200
Look! Here they come!

24
00:05:48,300 --> 00:05:50,000
Look! Look!

25
00:06:02,071 --> 00:06:04,750
Excuse me please,
have you got a match?

26
00:06:07,200 --> 00:06:12,029
Did I frighten you? Ha, I�m sorry. I�m oddly
enough I suppose.

27
00:06:12,200 --> 00:06:13,700
No, it wasn�t you.

28
00:06:13,800 --> 00:06:16,186
Something happened back on
the road there.

29
00:06:16,394 --> 00:06:18,396
We stopped to speak
to some men.

30
00:06:18,709 --> 00:06:23,164
Our driver told us that they weren�t men at
all. He said they were corpses.

31
00:06:25,756 --> 00:06:27,629
Corpses?

32
00:06:28,400 --> 00:06:30,200
Surely you don�t believe it, do you?

33
00:06:30,200 --> 00:06:37,800
No. But I don�t know - Haiti is full of
nonsense and superstition.

34
00:06:38,000 --> 00:06:42,200
They�re always mixed up with a lot of
mysteries that will turn your hair grey.

35
00:06:42,800 --> 00:06:49,150
I�ve been a missionary here for, oh, thirty years,
and at times I don�t know what to think.

36
00:06:50,200 --> 00:06:51,500
Come, let�s go
in the house.

37
00:06:51,800 --> 00:06:53,000
Oh, yes, come, dear.

38
00:07:03,122 --> 00:07:04,700
Is Mr. Beaumont in?

39
00:07:05,200 --> 00:07:07,169
You are expected, Dr. Bruner.

40
00:07:08,422 --> 00:07:11,372
Yes, I�ve been sent for to marry someone.

41
00:07:12,200 --> 00:07:13,645
Maybe you...

42
00:07:27,917 --> 00:07:30,700
How long is it that you�ve
known Mr. Beaumont?

43
00:07:31,037 --> 00:07:32,071
Oh, only a few days.

44
00:07:32,100 --> 00:07:35,082
Madeline introduced him on
the docks in Port-au-Prince.

45
00:07:35,867 --> 00:07:36,633
Ah, and you?

46
00:07:36,941 --> 00:07:39,100
I met him on the ship
coming from New York.

47
00:07:39,200 --> 00:07:41,400
He was very kind during the voyage.

48
00:07:41,569 --> 00:07:44,200
Madeline and I planned to be married
the moment she arrived,

49
00:07:44,354 --> 00:07:47,202
but Mr. Beaumont
persuaded us to come here.

50
00:07:47,250 --> 00:07:49,450
And he promised to take me out
of the bank at Port-au-Prince

51
00:07:49,470 --> 00:07:51,700
and send me to New York as his agent.

52
00:07:53,478 --> 00:07:54,700
Strange.

53
00:07:56,832 --> 00:07:57,739
Very strange.

54
00:07:58,700 --> 00:07:59,700
You...

55
00:08:01,831 --> 00:08:04,200
I�ll tell Mr. Beaumont you are here.

56
00:08:14,400 --> 00:08:16,556
It�s all right, isn�t it, doctor?

57
00:08:17,200 --> 00:08:18,700
Oh, I guess so.

58
00:08:18,900 --> 00:08:22,828
You see I, I�ve only met Mr.
Beaumont once or twice.

59
00:08:22,964 --> 00:08:26,774
But he never struck me like the man
who would take the trouble

60
00:08:26,877 --> 00:08:30,200
to play fairy godfather to
a young couple like you...

61
00:08:32,200 --> 00:08:33,400
Unless...

62
00:08:34,700 --> 00:08:35,700
Unless what, sir?

63
00:08:50,700 --> 00:08:53,448
I suppose you�ll think I�m
a meddling old fool, but...

64
00:08:54,200 --> 00:08:57,200
You know, I�d feel a good deal better
if you�d clear out of this place

65
00:08:57,212 --> 00:08:58,700
after you�re married,

66
00:08:58,750 --> 00:09:01,200
and have nothing more
to do with Mr. Beaumont.

67
00:09:04,600 --> 00:09:07,939
The young people have arrived sir,
and Dr. Bruner.

68
00:09:08,200 --> 00:09:10,458
They are waiting in the reception hall.

69
00:09:11,675 --> 00:09:14,419
Show them to their rooms,
and tell them I�m out.

70
00:09:15,130 --> 00:09:16,124
No wait.

71
00:09:17,276 --> 00:09:19,100
Perhaps I�d better see them.

72
00:09:19,200 --> 00:09:21,100
It might look odd if I didn�t.

73
00:09:21,300 --> 00:09:23,200
Very odd, sir.

74
00:09:23,251 --> 00:09:27,200
Especially as Dr. Bruner is a trifle
skeptical as to your -

75
00:09:27,700 --> 00:09:28,450
motives, sir.

76
00:09:29,520 --> 00:09:31,258
Nevermind my motives.

77
00:09:32,405 --> 00:09:34,280
Has that other person sent word yet?

78
00:09:34,720 --> 00:09:35,852
No, sir. Not yet.

79
00:09:39,700 --> 00:09:41,100
He�s twenty-four hours late.

80
00:09:43,015 --> 00:09:46,700
I wish you�d keep away from
that man, sir.

81
00:09:47,000 --> 00:09:49,000
He'll make trouble for you.

82
00:09:49,101 --> 00:09:50,851
You needn't worry about that.

83
00:09:51,002 --> 00:09:52,102
I'm not affraid of him.

84
00:09:52,203 --> 00:09:54,003
I'm not easily frightened, sir.

85
00:09:54,204 --> 00:09:55,404
You should know that.

86
00:09:56,400 --> 00:09:59,000
But what you are planning is dangerous.

87
00:09:59,200 --> 00:10:01,542
Don�t you suppose I know that, Silver?

88
00:10:01,715 --> 00:10:04,700
You don�t seem to realize what
this girl means to me.

89
00:10:05,236 --> 00:10:08,400
Why, I�d sacrifice anything I
have in the world for her.

90
00:10:10,098 --> 00:10:12,100
Nothing matters
if I can�t have her.

91
00:10:20,430 --> 00:10:22,672
I think, ah, I think you�ll like Haiti.

92
00:10:22,786 --> 00:10:23,700
Most people that...

93
00:10:26,500 --> 00:10:29,920
Madeline!
I�m delighted to see you!

94
00:10:30,091 --> 00:10:31,600
Neil, you�re more than welcome.

95
00:10:31,670 --> 00:10:32,700
Thank you, sir.

96
00:10:33,783 --> 00:10:35,500
Doctor, it is very kind of you to come.

97
00:10:35,601 --> 00:10:37,150
I know what a busy man you are.

98
00:10:37,200 --> 00:10:40,506
No, ah, not at all. There is a native
family live out here

99
00:10:40,550 --> 00:10:42,361
that I�ve been trying to see for a long time.

100
00:10:42,636 --> 00:10:45,800
After this young couple are
safely married, I'll leave.

101
00:10:45,851 --> 00:10:48,302
But surely you will stay for
dinner after the ceremony?

102
00:10:48,406 --> 00:10:50,176
No, no, no. No, I must run along.

103
00:10:50,351 --> 00:10:54,500
That�s a great pity. We have something
very special prepared for this occasion.

104
00:10:55,474 --> 00:10:59,100
It was very good of you, Madeline,
to humour the whim of a lonely man.

105
00:10:59,243 --> 00:11:02,300
There was so little time
to prepare, I couldn�t do half

106
00:11:02,350 --> 00:11:04,094
the things I wanted for you.

107
00:11:04,200 --> 00:11:06,500
You�ve done more than enough already
Mr. Beaumont,

108
00:11:06,550 --> 00:11:09,996
for a comparative stranger...
Giving Neil a position in The States.

109
00:11:10,200 --> 00:11:10,725
Neil?

110
00:11:10,800 --> 00:11:12,526
Yes. Yes indeed.

111
00:11:12,606 --> 00:11:15,682
Oh, yes of course! I�m sure Neil
will make a very good agent.

112
00:11:15,759 --> 00:11:17,700
But you must be tired
after your drive.

113
00:11:17,803 --> 00:11:20,548
You get some rest.
Silver!

114
00:11:22,368 --> 00:11:24,166
Silver will show you to your rooms.

115
00:11:24,649 --> 00:11:25,621
This way please.

116
00:14:53,100 --> 00:14:56,016
Delighted to see you again,
monsieur Beaumont!

117
00:15:09,872 --> 00:15:11,438
Please.

118
00:15:14,534 --> 00:15:15,907
Please.

119
00:15:21,747 --> 00:15:24,251
I�m sorry to have kept
you waiting, monsieur.

120
00:15:28,154 --> 00:15:30,053
I�ve been on a journey

121
00:15:30,200 --> 00:15:32,778
seeking men for my mills.

122
00:15:33,487 --> 00:15:34,962
Men?

123
00:15:35,298 --> 00:15:37,600
They work faithfully...

124
00:15:37,650 --> 00:15:41,200
They are not worried
about long hours.

125
00:15:43,292 --> 00:15:48,108
You can make good use of
men like mine on your plantations.

126
00:15:48,204 --> 00:15:49,448
No.

127
00:15:50,826 --> 00:15:53,200
That�s not what I want.

128
00:15:55,393 --> 00:15:57,574
Then perhaps we should talk
about the young lady

129
00:15:57,600 --> 00:15:59,700
who came to your house this evening?

130
00:15:59,756 --> 00:16:02,700
You�ve seen her? When?

131
00:16:03,915 --> 00:16:06,055
The road, tonight.

132
00:16:18,470 --> 00:16:22,200
There was a young man with her.

133
00:16:22,498 --> 00:16:24,900
They are to be married, tonight.

134
00:16:24,914 --> 00:16:27,050
You waited too long to do anything.

135
00:16:27,700 --> 00:16:29,278
What do you want me to do?

136
00:16:29,482 --> 00:16:33,093
If she were to disappear,
for a month...

137
00:16:34,276 --> 00:16:38,400
What do you hope to gain
by her disappearance?

138
00:16:38,501 --> 00:16:39,700
Everything...

139
00:16:40,010 --> 00:16:41,000
Everything?

140
00:16:41,152 --> 00:16:44,057
Do you think she will forget
her lover in a month?

141
00:16:44,071 --> 00:16:48,025
Just give me a month.
One little month.

142
00:16:49,203 --> 00:16:53,700
Not in a month. Not even a year,
monsieur.

143
00:16:55,844 --> 00:17:05,700
I looked into her eyes. She is deep
in love. But not with you.

144
00:17:06,579 --> 00:17:08,617
They are to be married
within an hour!

145
00:17:08,860 --> 00:17:10,496
There must be a way!

146
00:17:21,495 --> 00:17:25,500
There is way.

147
00:17:28,200 --> 00:17:29,770
The cost...

148
00:17:31,271 --> 00:17:32,771
The cost is heavy!

149
00:17:34,109 --> 00:17:38,593
You give me what I want,
and you may ask anything.

150
00:18:02,488 --> 00:18:05,702
No! Not that!

151
00:18:29,557 --> 00:18:33,300
Only a pin point,
monsieur Beaumont,

152
00:18:33,320 --> 00:18:38,050
in a glass of wine
or perhaps a flower.

153
00:18:40,996 --> 00:18:44,532
Take it.
The time is very brief.

154
00:18:44,700 --> 00:18:47,788
You must do your share if
I am to help you.

155
00:18:50,661 --> 00:18:52,867
Keep it, monsieur.

156
00:18:56,531 --> 00:19:02,050
Keep it.
You may change your mind.

157
00:19:15,852 --> 00:19:19,363
Send me word when you use it.

158
00:19:34,100 --> 00:19:38,550
- I�ll find another way.
- There is no other way.

159
00:19:55,335 --> 00:19:58,545
They are driving away evil spirits!

160
00:19:58,700 --> 00:20:00,717
Close it, close it!

161
00:20:04,700 --> 00:20:06,203
Mademoiselle!

162
00:20:24,500 --> 00:20:27,694
I love you, Madeline, more than anything else
in this whole world, dear.

163
00:20:28,210 --> 00:20:30,711
Heaven or hell lies in this
little moment for me.

164
00:20:30,991 --> 00:20:35,135
You could raise me up to paradise or
you could blast my world into nothingness.

165
00:20:36,700 --> 00:20:38,700
I can make you the envy of every woman.

166
00:20:39,200 --> 00:20:41,569
I�d give my life to make you happy!

167
00:20:43,445 --> 00:20:46,187
Oh, listen to me dear
before it�s too late!

168
00:20:46,677 --> 00:20:48,509
Don�t, please.

169
00:20:48,642 --> 00:20:50,546
Don�t go into that room.

170
00:20:50,621 --> 00:20:53,000
We can be in Port-au-Prince
in half an hour.

171
00:20:53,300 --> 00:20:55,200
There�s a boat sailing at midnight.

172
00:20:56,424 --> 00:20:58,256
You�ve been so wonderful.

173
00:20:58,437 --> 00:21:01,103
Don�t spoil everything now.

174
00:21:04,978 --> 00:21:07,700
One last gift
before I lose you forever!

175
00:21:51,872 --> 00:21:55,500
We are gathered together here
in the sight of God,

176
00:21:55,600 --> 00:21:57,700
and in the face of this company,

177
00:21:58,100 --> 00:22:01,000
to join together this man, this woman

178
00:22:01,200 --> 00:22:03,000
in holy matrimony.

179
00:23:36,709 --> 00:23:38,500
This is the night of nights!

180
00:23:41,700 --> 00:23:45,382
A toast to the bride!
To beauty�s queen!

181
00:23:48,006 --> 00:23:49,950
Gladly, My Lord!

182
00:23:54,849 --> 00:23:57,757
Leave but a kiss
within the glass.

183
00:23:59,377 --> 00:24:02,900
Fair gypsy, read my fortune.

184
00:24:10,200 --> 00:24:12,450
What do you see in the glass?

185
00:24:14,171 --> 00:24:18,425
I see
happiness...

186
00:24:20,075 --> 00:24:25,289
I see love, far more than you
can bear.

187
00:24:26,045 --> 00:24:27,200
Is that all?

188
00:24:27,526 --> 00:24:29,200
No.

189
00:24:29,600 --> 00:24:31,368
I see...

190
00:24:38,700 --> 00:24:40,301
I see...

191
00:24:40,372 --> 00:24:41,070
What is it?

192
00:24:42,552 --> 00:24:44,588
I see death.

193
00:24:46,850 --> 00:24:47,800
Death?

194
00:24:52,146 --> 00:24:53,484
Madeline, what�s wrong?

195
00:24:54,225 --> 00:24:55,723
Madeline my dear, please.

196
00:25:05,631 --> 00:25:07,105
�, no...

197
00:25:11,203 --> 00:25:13,700
Madeline?
Madeline?

198
00:25:15,661 --> 00:25:16,855
Can�t we do something?

199
00:25:17,073 --> 00:25:19,200
Please, please.

200
00:25:19,789 --> 00:25:21,127
Madeline!

201
00:25:31,664 --> 00:25:34,855
Not my wife. My wife!

202
00:26:05,041 --> 00:26:07,488
Praise of our lord and saviour

203
00:26:07,859 --> 00:26:10,363
and the love of
god and the fellowship

204
00:26:10,400 --> 00:26:12,500
of his angels.

205
00:26:12,550 --> 00:26:16,200
Be with us ever more. Amen.

206
00:26:59,819 --> 00:27:03,101
Neil, Neil...

207
00:27:36,200 --> 00:27:36,977
Neil...

208
00:27:38,200 --> 00:27:38,977
Neil...

209
00:27:41,480 --> 00:27:42,544
Neil...

210
00:28:37,700 --> 00:28:38,368
Look!

211
00:28:50,800 --> 00:28:51,900
Zombies!

212
00:28:53,786 --> 00:28:56,700
Yes.
They are my servants.

213
00:28:57,200 --> 00:28:59,500
Did you think we could do it alone?

214
00:29:00,766 --> 00:29:03,204
In their lifetime they were my enemies.

215
00:29:05,394 --> 00:29:11,193
Ledot, the witch doctor.
Once my master.

216
00:29:11,305 --> 00:29:14,280
Secrets I tortured out of him.

217
00:29:14,922 --> 00:29:18,032
Von Gelder, the swine.
Swollen with riches.

218
00:29:18,204 --> 00:29:24,900
He fought against my spells until the last.
In him I have a struggling type.

219
00:29:26,561 --> 00:29:31,045
His Excellence, Richard.
Once minister of the interior.

220
00:29:32,767 --> 00:29:42,660
Scarpia, Brigand Chief. Marcquis,
Captain of Gendarmerie. And this...

221
00:29:43,065 --> 00:29:48,001
this is Chauvin. The high
executioner,

222
00:29:49,221 --> 00:29:51,703
who almost executed me!

223
00:29:56,072 --> 00:29:57,713
I took them

224
00:29:58,714 --> 00:30:04,000
just as we will take
this one.

225
00:30:04,058 --> 00:30:06,500
But what if they
regain their souls?

226
00:30:08,528 --> 00:30:11,102
They would tear me to pieces.

227
00:30:11,815 --> 00:30:17,076
But that, my friend,
shall never be.

228
00:31:31,282 --> 00:31:32,620
Madeline!

229
00:31:37,018 --> 00:31:38,200
Madeline!

230
00:31:43,829 --> 00:31:45,066
Madeline!

231
00:31:45,500 --> 00:31:46,704
Madeline!

232
00:33:12,391 --> 00:33:14,633
There�s two explanations
that strike me,

233
00:33:15,410 --> 00:33:18,300
either the body was stolen by the
members of a death cult

234
00:33:18,361 --> 00:33:23,100
that use human bones in their
ceremonies, or else...

235
00:33:23,200 --> 00:33:24,500
Or else what?

236
00:33:24,600 --> 00:33:26,200
She�s not dead.

237
00:33:30,775 --> 00:33:32,069
Not dead?

238
00:33:32,919 --> 00:33:34,154
Are you mad?

239
00:33:34,530 --> 00:33:36,096
I saw her die,

240
00:33:36,200 --> 00:33:37,700
the doctor signed the certificate.

241
00:33:37,720 --> 00:33:39,002
I saw them bury her.

242
00:33:39,500 --> 00:33:42,700
Now, wait a minute, wait a minute.
I�m not mad.

243
00:33:43,500 --> 00:33:46,400
But I�ve lived in these islands
for a good many years,

244
00:33:47,006 --> 00:33:52,200
and I�ve seen things with my eyes
that made me think I was crazy.

245
00:33:53,620 --> 00:33:57,900
There are superstitions in Haiti that
the natives brought here from Africa.

246
00:33:58,014 --> 00:34:02,906
Some of them can be traced
back as far as ancient Egypt,

247
00:34:04,067 --> 00:34:08,200
and beyond that yet, in the countries
that was old when Egypt was young.

248
00:34:08,259 --> 00:34:10,759
Yes, but what has that
to do with Madeline?

249
00:34:11,029 --> 00:34:12,629
I kissed her as she lay
there in the coffin.

250
00:34:14,374 --> 00:34:16,003
And her lips were cold.

251
00:34:18,000 --> 00:34:21,441
Let me explain. Now,
just a minute, I�ll explain.

252
00:34:22,242 --> 00:34:26,900
Wherever there is a superstition, you
will find there is also a practice.

253
00:34:27,000 --> 00:34:30,600
Now, do you remember what your driver told you
the night that he took you to Beaumont�s house?

254
00:34:30,635 --> 00:34:33,000
Oh, about those horrible
creatures we saw?

255
00:34:33,400 --> 00:34:36,324
He said they were corpses!
Taken from their graves.

256
00:34:36,532 --> 00:34:40,150
Yes - that�s the superstition!
Now for the practice.

257
00:34:40,182 --> 00:34:46,526
The ghouls that steal the dead corpses from their graves,
are supposed to put them there in the first place.

258
00:34:47,617 --> 00:34:49,893
Do you mean that
Madeline was murdered

259
00:34:50,201 --> 00:34:52,501
so that somebody could
steal her dead body?

260
00:34:52,706 --> 00:34:53,788
Aagh! Nonsense!

261
00:34:54,200 --> 00:34:56,570
No, no. Not her...

262
00:34:56,642 --> 00:35:00,200
Her body, yes; but not her dead body.
That�s what I meant.

263
00:35:01,065 --> 00:35:03,230
Well, surely you don�t
think she�s alive,

264
00:35:03,300 --> 00:35:05,063
in the hands of natives?

265
00:35:05,505 --> 00:35:07,586
Oh no, better dead than that!

266
00:35:08,189 --> 00:35:09,950
Excuse me please,
have you got a match?

267
00:35:12,482 --> 00:35:13,201
Thank you.

268
00:35:21,036 --> 00:35:23,002
You don�t believe that,
do you?

269
00:35:24,700 --> 00:35:29,201
Say, there�s been lots of people
that�s been pronounced dead

270
00:35:29,212 --> 00:35:31,560
that came alive again and
lived for years.

271
00:35:32,340 --> 00:35:34,948
Now, if nature can play
pranks like that,

272
00:35:35,225 --> 00:35:38,141
why isn�t it possible to play
pranks with nature?

273
00:35:38,412 --> 00:35:39,200
Oh, I don�t know.

274
00:35:41,736 --> 00:35:47,200
Your driver believed he saw
dead men, walking.

275
00:35:48,203 --> 00:35:53,036
He didn�t. What he saw was man alive

276
00:35:53,037 --> 00:35:55,207
and everything but this and this.

277
00:35:57,400 --> 00:36:00,700
Oh, the whole thing has me confused!

278
00:36:01,011 --> 00:36:02,700
I just can�t understand it.

279
00:36:02,701 --> 00:36:04,701
Um, I don�t blame you...

280
00:36:05,001 --> 00:36:06,001
I don�t blame you.

281
00:36:06,054 --> 00:36:08,950
I�ve been trying for years to get to
the bottom of these things.

282
00:36:09,000 --> 00:36:11,947
To separate what you call fact
from fiction.

283
00:36:12,708 --> 00:36:13,700
The law!

284
00:36:14,000 --> 00:36:18,200
The law of Haiti acknowledges the
possibility of being buried alive.

285
00:36:18,400 --> 00:36:20,000
Here it is in
the penal code.

286
00:36:21,004 --> 00:36:22,204
I�ll read it for you.

287
00:36:23,500 --> 00:36:24,373
It�s in French -

288
00:36:24,408 --> 00:36:25,410
do you speak French?

289
00:36:25,450 --> 00:36:26,200
No.

290
00:36:27,058 --> 00:36:29,200
Excuse me please,
have you got a match?

291
00:36:29,338 --> 00:36:30,911
Right here, here�s one.

292
00:36:30,952 --> 00:36:32,450
Oh, thank you. I didn�t see it there.

293
00:36:32,869 --> 00:36:35,601
I�ll translate it for you.
If you could spare me.

294
00:36:36,202 --> 00:36:37,002
Article...

295
00:36:38,003 --> 00:36:39,603
Article 2.49.

296
00:36:41,004 --> 00:36:42,104
The use...

297
00:36:42,700 --> 00:36:45,600
The use of drugs or other practices

298
00:36:45,707 --> 00:36:50,607
which produce lethargic coma,
or lifeless sleep,

299
00:36:51,259 --> 00:36:55,004
shall be considered attempted murder.

300
00:36:55,400 --> 00:36:56,000
Attempted!

301
00:36:56,050 --> 00:36:57,200
Yes I see.

302
00:36:58,702 --> 00:37:01,000
If the person has been
buried alive,

303
00:37:01,500 --> 00:37:04,350
the act should be considered murder

304
00:37:04,700 --> 00:37:07,036
no matter what result follows.

305
00:37:08,400 --> 00:37:09,500
Beaumont!

306
00:37:10,398 --> 00:37:12,300
Say, you said you couldn�t
understand

307
00:37:12,310 --> 00:37:14,400
why he was so interested in us.

308
00:37:14,762 --> 00:37:16,466
Do you think he did this?

309
00:37:16,742 --> 00:37:19,000
No. No, I think his natives would.

310
00:37:19,050 --> 00:37:21,150
Natives would be right.

311
00:37:21,200 --> 00:37:24,200
Of course if you want to, we
could go to Beaumont�s house first.

312
00:37:26,280 --> 00:37:30,400
If I could get my hands on the devil
that�s responsible for this,

313
00:37:31,439 --> 00:37:32,950
I�ll make him such an example

314
00:37:32,980 --> 00:37:36,700
that every witch doctor in Haiti
would be shaking in his sandals.

315
00:37:36,747 --> 00:37:39,526
But we can�t do this alone.

316
00:37:39,766 --> 00:37:41,200
Can�t the authorities help?

317
00:37:41,981 --> 00:37:46,400
The authorities, Neil, my boy,
you don�t know these islands.

318
00:37:46,500 --> 00:37:49,150
The native authorities are
afraid to meddle.

319
00:37:49,200 --> 00:37:52,500
I am not. I�ve got friends
among the natives.

320
00:37:52,852 --> 00:37:56,128
They�ll tell me things that no jandam
could ever get out of them.

321
00:37:57,404 --> 00:38:00,200
Because I am a preacher.

322
00:38:00,400 --> 00:38:01,923
They think I am a magician.

323
00:38:04,732 --> 00:38:12,900
Before we get through with this thing, we may uncover
sins that even the devil would be ashamed of.

324
00:38:16,000 --> 00:38:18,604
Oh, these witch doctors...

325
00:39:36,170 --> 00:39:36,704
Madeline...

326
00:40:28,969 --> 00:40:34,200
Foolish things, they can�t bring
back the light to those eyes.

327
00:40:37,528 --> 00:40:39,400
I was mad to do this,

328
00:40:41,283 --> 00:40:42,957
but if you had smiled on me,

329
00:40:43,300 --> 00:40:45,400
I�d have done anything for you.

330
00:40:45,750 --> 00:40:47,000
Given you anything.

331
00:40:49,580 --> 00:40:52,400
I thought that beauty
alone would satisfy,

332
00:40:54,703 --> 00:40:56,451
but the soul is gone...

333
00:40:57,500 --> 00:41:01,200
I can�t bear those
empty staring eyes.

334
00:41:04,732 --> 00:41:07,200
Oh, forgive me,
Madeline. Forgive me!

335
00:41:09,630 --> 00:41:11,700
I can�t bear it any longer.

336
00:41:12,145 --> 00:41:13,552
I must take you back.

337
00:41:16,943 --> 00:41:19,800
Back to the grave,
Monsieur?

338
00:41:21,940 --> 00:41:28,100
No, you must put the life back into her
eyes and bring laughter to her lips.

339
00:41:28,942 --> 00:41:31,102
She must be gay and
happy again!

340
00:41:31,701 --> 00:41:34,546
You paint a charming picture,
Monsieur.

341
00:41:35,224 --> 00:41:38,200
One that I should
like to see myself!

342
00:42:00,884 --> 00:42:02,220
You must bring her back.

343
00:42:03,333 --> 00:42:06,078
Aren�t you a trifle afraid,
Monsieur?

344
00:42:07,056 --> 00:42:10,800
How do suppose those
eyes will regard you

345
00:42:10,850 --> 00:42:13,500
when the brain is able
to understand?

346
00:42:47,000 --> 00:42:51,900
Better to see hatred in them
than that dreadful emptiness.

347
00:42:52,000 --> 00:42:53,400
Perhaps you�re right.

348
00:42:54,459 --> 00:42:57,700
It would be a pity to
destroy such a lovely flower.

349
00:43:00,564 --> 00:43:03,447
Let�s drink to the future
of this flower.

350
00:43:03,753 --> 00:43:04,700
A glass of wine!

351
00:43:07,609 --> 00:43:09,509
Silver, bring wine!

352
00:43:10,191 --> 00:43:11,723
We have a toast to drink.

353
00:43:40,113 --> 00:43:42,500
To the future,
Monsieur!

354
00:43:58,831 --> 00:44:03,200
Only a pin point,
Monsieur.

355
00:44:03,463 --> 00:44:04,669
In a flower...

356
00:44:05,976 --> 00:44:08,900
or perhaps in a glass of wine?

357
00:44:17,716 --> 00:44:20,220
You devil! What are you
trying to do to me?

358
00:44:21,408 --> 00:44:23,500
I have other plans for Mademoiselle.

359
00:44:24,997 --> 00:44:27,400
And I am afraid you might not agree.

360
00:44:33,048 --> 00:44:36,908
I have taken a fancy to you,
Monsieur!

361
00:44:38,516 --> 00:44:40,426
Silver! Silver!

362
00:45:48,200 --> 00:45:49,700
Don�t, don�t!

363
00:46:20,629 --> 00:46:23,140
To the future,
Monsieur!

364
00:46:50,316 --> 00:46:51,688
The vulture,

365
00:46:51,763 --> 00:46:52,213
you...

366
00:47:02,492 --> 00:47:06,206
No. Not that.
Not that!

367
00:47:12,186 --> 00:47:14,732
We ought to be picking up an old witch
doctor around here pretty soon.

368
00:47:14,907 --> 00:47:19,400
His name is Pierre, I�ve known
him for years. Bright old fellow.

369
00:47:19,780 --> 00:47:22,416
I don�t know just
where we�ll find him.

370
00:47:43,700 --> 00:47:44,700
Come, son.

371
00:47:45,467 --> 00:47:49,400
There are evil spirits in the road.

372
00:47:50,381 --> 00:47:53,708
I will give you an awonga.

373
00:47:55,209 --> 00:47:59,700
And here, this one
for the ox.

374
00:48:09,200 --> 00:48:13,100
Young man is sick with distress.

375
00:48:13,359 --> 00:48:17,546
Well hey, wait a minute, we can�t
afford to have you sick.

376
00:48:18,759 --> 00:48:21,739
Neil, why don�t you go over there,
we've got a hard day before us tomorrow.

377
00:48:25,636 --> 00:48:29,755
Now, now then Pierre, come on.
He�s gone. We can talk.

378
00:48:30,265 --> 00:48:33,650
It is a dangerous thing
you ask me to do.

379
00:48:33,854 --> 00:48:38,841
Well now listen here you know, we're old
friends, you and me, and I want to go on.

380
00:48:38,900 --> 00:48:42,200
Turn back before it is too late!

381
00:48:42,200 --> 00:48:44,986
Oh, no. I�ve come too
far to turn back now.

382
00:48:45,500 --> 00:48:49,700
I�m too old to go all this way with you.

383
00:48:50,200 --> 00:48:53,600
Well listen, can�t you get
somebody to go with us?

384
00:48:54,300 --> 00:48:58,100
My people all afraid of the mountain.

385
00:48:58,500 --> 00:48:59,500
Why?

386
00:48:59,760 --> 00:49:06,200
Because it is called the land of the living dead.

387
00:49:06,405 --> 00:49:09,806
Well, have, have you ever been there?

388
00:49:10,224 --> 00:49:17,757
I am the only man that has ever
come back from there alive.

389
00:49:20,055 --> 00:49:24,200
There is an evil spirit man

390
00:49:25,101 --> 00:49:27,701
that is called Murder.

391
00:49:28,562 --> 00:49:34,100
Come, I will tell you all about
what he did.

392
00:50:28,700 --> 00:50:29,700
Vulture.

393
00:50:40,627 --> 00:50:42,600
Just as old Pierre said.

394
00:50:43,473 --> 00:50:48,900
A cloud of vultures always hovers
over the house of the living dead.

395
00:50:51,496 --> 00:50:52,500
Madeline...

396
00:50:53,643 --> 00:50:55,000
Is she there?

397
00:50:55,726 --> 00:50:57,263
Oh, I must go and see her.

398
00:50:58,318 --> 00:51:04,200
Neil, my boy, please, please
lie down and rest. Please.

399
00:51:06,242 --> 00:51:08,200
You�ll feel stronger in
the morning.

400
00:51:12,766 --> 00:51:13,700
You rest.

401
00:51:14,777 --> 00:51:17,500
Let me go up and see
what I can do.

402
00:51:40,674 --> 00:51:42,976
Why is she so restless tonight?

403
00:51:44,029 --> 00:51:46,708
Perhaps she remembers something?

404
00:51:48,088 --> 00:51:52,900
They never remember anything
when they are like that.

405
00:51:52,950 --> 00:51:53,981
No?

406
00:51:54,763 --> 00:51:58,700
Because she�s cut off.

407
00:52:44,677 --> 00:52:45,241
Madeline!

408
00:52:46,824 --> 00:52:47,728
Madeline!

409
00:53:25,772 --> 00:53:27,473
No, no, I can�t,

410
00:53:27,703 --> 00:53:28,750
I can�t!

411
00:53:28,894 --> 00:53:31,500
You must, it�s your turn.

412
00:53:32,214 --> 00:53:33,200
Let�s run away!

413
00:53:35,637 --> 00:53:37,700
He might hear you!

414
00:53:38,032 --> 00:53:41,736
No way. I can�t stand it.
I am going to run away!

415
00:53:43,092 --> 00:53:46,505
He will find you
and make you like her.

416
00:55:00,137 --> 00:55:01,700
Can you still hear me?

417
00:55:08,727 --> 00:55:12,306
It is unfortunate you are
no longer able to speak.

418
00:55:14,005 --> 00:55:17,500
I should be interested to hear
you describe your symptoms...

419
00:55:29,200 --> 00:55:34,146
You see, you are the first
man to know

420
00:55:34,250 --> 00:55:35,600
what is happening.

421
00:55:40,692 --> 00:55:44,400
None of the others
did.

422
00:56:05,418 --> 00:56:09,026
You refused to shake hands once.

423
00:56:09,920 --> 00:56:11,150
I remember.

424
00:56:12,980 --> 00:56:14,100
Well, well.

425
00:56:15,388 --> 00:56:17,850
We understand each other better now.

426
01:02:19,657 --> 01:02:20,651
Madeline!

427
01:02:26,953 --> 01:02:27,642
Madeline!

428
01:02:52,030 --> 01:02:52,700
Madeline!

429
01:02:56,200 --> 01:02:58,600
Madeline!
I found you!

430
01:02:59,200 --> 01:03:01,812
You�re alive.
Alive!

431
01:03:02,497 --> 01:03:06,000
What�s the matter?
It�s I, Neil.

432
01:03:17,589 --> 01:03:21,300
Oh my darling, what
have they done to you?

433
01:03:42,000 --> 01:03:42,909
Who are you?

434
01:03:45,970 --> 01:03:47,204
And what are they?

435
01:03:47,478 --> 01:03:50,402
For you, my friend, they
are the angels of death.

436
01:04:26,361 --> 01:04:28,400
Come! Zombies!

437
01:04:28,540 --> 01:04:29,334
Duck!

438
01:04:49,600 --> 01:04:50,200
Look!

439
01:05:39,000 --> 01:05:42,192
Madeline, don�t you know
me, dear?

440
01:05:42,777 --> 01:05:43,843
It�s Neil.

441
01:05:44,285 --> 01:05:48,700
I could swear, for a moment
she recognized you.

442
01:05:52,234 --> 01:05:53,700
Come on, don�t let
him get away!

443
01:06:51,277 --> 01:06:54,100
Madeline, my darling!

444
01:07:04,356 --> 01:07:08,577
Neil, I...
I dreamed?

445
01:07:15,626 --> 01:07:19,820
Excuse me please,
have you got a match?

