1
00:01:02,280 --> 00:01:10,119
<i>In ignem iacto unguenta pretiosa,</i>

2
00:01:10,240 --> 00:01:12,595
<i>et mira sumio.</i>

3
00:01:37,480 --> 00:01:40,278
<i>Tibi supplex...</i>

4
00:01:40,400 --> 00:01:43,039
<i>donem honorem!</i>

5
00:01:43,160 --> 00:01:45,674
No!

6
00:01:45,800 --> 00:01:50,271
You promised!
You promised not our child!

7
00:01:53,280 --> 00:01:57,239
Whore! He is the one.

8
00:01:59,040 --> 00:02:01,952
You can't hurt him now, you bastard!

9
00:02:05,360 --> 00:02:07,749
His blood will flow.

10
00:02:08,840 --> 00:02:10,796
Come.

11
00:02:12,800 --> 00:02:14,756
Come.

12
00:02:16,840 --> 00:02:18,353
Come.

13
00:02:35,840 --> 00:02:38,798
Take him away! For ever!

14
00:02:40,240 --> 00:02:42,196
Do it now.

15
00:03:08,640 --> 00:03:11,791
You will take his place.

16
00:03:25,040 --> 00:03:29,591
Don't be afraid, little one.

17
00:03:32,520 --> 00:03:34,476
You'll be safe.

18
00:03:39,640 --> 00:03:41,471
Lucifer!

19
00:03:41,600 --> 00:03:43,636
<i>Rubrum e iugulo</i>

20
00:03:43,760 --> 00:03:50,393
<i>ut tibi supplex donem honorem.</i>

21
00:05:54,640 --> 00:05:59,634
<i>The child had been saved,</i>
<i>so it seemed.</i>

22
00:05:59,760 --> 00:06:04,914
<i>And I vowed that he would never know</i>
<i>the evil from whence he was spawned.</i>

23
00:06:05,040 --> 00:06:09,238
<i>And when his father</i>
<i>finally died a horrible death,</i>

24
00:06:09,360 --> 00:06:13,319
<i>I felt sure that the curse</i>
<i>had passed for ever.</i>

25
00:06:14,360 --> 00:06:16,999
I can't believe
you inherited this place.

26
00:06:17,120 --> 00:06:19,680
I know. Me neither.

27
00:06:19,800 --> 00:06:23,998
I suppose we'll just have to
get used to the good life.

28
00:06:24,120 --> 00:06:26,429
Do we get to hire
a maid and a cook?

29
00:06:26,560 --> 00:06:30,394
Are you kidding? It's gonna break me
just to keep the lawn mowed.

30
00:06:30,520 --> 00:06:35,150
Well, we could always sell it
and move to Westwood.

31
00:06:35,280 --> 00:06:40,400
- Very funny. Very funny.
- No, I like this place... I think.

32
00:06:40,520 --> 00:06:42,636
What's that?

33
00:06:42,760 --> 00:06:47,470
- I don't know. It looks...
- Like a grave.

34
00:06:47,600 --> 00:06:50,717
- What are you doing?
- I'm gonna take a look.

35
00:06:50,840 --> 00:06:53,115
No, don't. It's kinda creepy-looking.

36
00:06:53,240 --> 00:06:56,198
Come on, Becky. It's mine, isn't it?

37
00:07:01,520 --> 00:07:04,398
I don't know. Looks sorta old.

38
00:07:04,520 --> 00:07:08,035
Must have been hard growing up
without a family. I mean...

39
00:07:13,800 --> 00:07:18,874
Rebecca, this is Wolfgang.
He's sort of the caretaker around here.

40
00:07:19,000 --> 00:07:21,560
Wolfgang, this is Rebecca.

41
00:07:23,560 --> 00:07:25,516
Hi. I...

42
00:07:27,320 --> 00:07:29,880
You scared me.

43
00:07:32,840 --> 00:07:36,071
Well, we'll go back to the house.

44
00:07:36,200 --> 00:07:39,158
We'll see you later, Wolfgang.

45
00:07:41,880 --> 00:07:44,917
So Wolfgang's the only family
I've ever known.

46
00:07:45,040 --> 00:07:48,032
Who read all these books? Your dad?

47
00:07:48,160 --> 00:07:50,720
Yeah, I guess so.

48
00:07:54,880 --> 00:07:57,030
<i>The Secret World of Magic?</i>

49
00:08:00,080 --> 00:08:02,913
<i>The Black Magic Ritual.</i>

50
00:08:03,040 --> 00:08:06,828
<i>The Grimoire of Abra-Melin.</i>

51
00:08:17,040 --> 00:08:18,996
You all right?

52
00:08:20,000 --> 00:08:23,675
You probably scared him
more than he scared you.

53
00:08:23,800 --> 00:08:27,679
Why don't you start cleaning the upstairs
and I'll work on the downstairs?

54
00:08:27,800 --> 00:08:30,360
What do you say?

55
00:08:30,480 --> 00:08:36,157
OK. But if there are any more rats,
the deal's off.

56
00:11:04,840 --> 00:11:07,400
I've been thinking.

57
00:11:13,960 --> 00:11:16,520
About having a party.

58
00:11:19,880 --> 00:11:22,075
- Here?
- Sure. Why not?

59
00:11:22,200 --> 00:11:24,555
It's not like anybody
could wreck the place.

60
00:11:32,800 --> 00:11:34,756
Whoa!

61
00:11:36,000 --> 00:11:39,754
This is it! Eddie, Eddie.

62
00:11:39,880 --> 00:11:42,440
They got food.

63
00:11:43,560 --> 00:11:46,916
Eddie, these are my friends.
They love me, man.

64
00:11:47,040 --> 00:11:50,510
- And this is where they're at.
- That's good. That's real good.

65
00:11:50,640 --> 00:11:53,438
But what if they're not?

66
00:11:53,560 --> 00:11:56,711
Then I'll have to get some new friends.

67
00:12:12,480 --> 00:12:15,552
Man, you should have seen the quim
I poked last night.

68
00:12:15,680 --> 00:12:18,194
I'm so unhappy.

69
00:12:19,120 --> 00:12:24,638
Toad Boy needs sweetmeat for his tum.

70
00:12:24,760 --> 00:12:28,878
Will you knock off
that Toad Boy shit already? Jesus!

71
00:12:29,000 --> 00:12:35,189
- You don't love me. You just pretend.
- Shit, I can't talk to you, man.

72
00:12:35,320 --> 00:12:37,276
- Hey, Dick.
- Jon.

73
00:12:37,400 --> 00:12:41,951
- How you doing?
- All right. It's him I'm worried about.

74
00:12:43,640 --> 00:12:48,111
- Hey, Mark. How you doing?
- I'm miserable.

75
00:12:58,800 --> 00:13:01,951
So, wanna dance later?

76
00:13:02,760 --> 00:13:05,320
Think you can handle it?

77
00:13:06,600 --> 00:13:09,114
- Try me.
- I might.

78
00:13:17,520 --> 00:13:20,080
I went out with Mark last night.

79
00:13:20,920 --> 00:13:23,559
You're kidding?

80
00:13:23,680 --> 00:13:27,070
- You mean Toad Boy?
- Don't ask me why.

81
00:13:27,200 --> 00:13:32,832
No, things were really nice, they were,
but then he just got so weird.

82
00:13:34,040 --> 00:13:36,235
Do you think he's a virgin?

83
00:13:36,360 --> 00:13:38,316
I don't know.

84
00:13:40,240 --> 00:13:42,515
But I certainly know who isn't.

85
00:13:44,480 --> 00:13:48,632
Hey, Rebecca.
Can you lead me to the nearest beer?

86
00:13:48,760 --> 00:13:51,228
In the refrigerator.

87
00:13:59,360 --> 00:14:01,316
Thanks.

88
00:14:02,840 --> 00:14:06,674
So... who's your beautiful friend?

89
00:14:06,800 --> 00:14:10,349
- I'm Donna.
- They call me Dick.

90
00:14:10,480 --> 00:14:13,631
But you can call me... Dick.

91
00:14:16,040 --> 00:14:18,600
Break dance!

92
00:15:11,360 --> 00:15:13,316
I think I busted my head.

93
00:15:13,440 --> 00:15:17,752
Thank God.
I thought that you really hurt yourself.

94
00:15:21,600 --> 00:15:24,751
- What do you guys wanna do?
- We could play hide-and-go-seek.

95
00:15:24,880 --> 00:15:26,438
Yeah!

96
00:15:26,560 --> 00:15:29,199
What about Trivial Pursuit?

97
00:15:29,320 --> 00:15:32,596
- Yeah.
- Yeah.

98
00:15:32,720 --> 00:15:35,188
Why don't we just play charades?

99
00:15:35,320 --> 00:15:37,276
Yeah.

100
00:15:38,960 --> 00:15:41,679
- Strip poker.
- Yeah.

101
00:15:41,800 --> 00:15:43,756
No way.

102
00:15:43,880 --> 00:15:45,836
All right!

103
00:15:55,560 --> 00:15:59,633
I got an idea. Let's do a ritual.

104
00:16:05,440 --> 00:16:08,398
You guys, it's kinda spooky in here.

105
00:16:09,200 --> 00:16:14,911
Everybody listen up.
I want everybody to take a sip. Go ahead.

106
00:16:15,040 --> 00:16:19,989
I'm gonna draw a circle
and I want you all to stay inside it.

107
00:16:30,520 --> 00:16:32,476
Go on, man.

108
00:16:43,480 --> 00:16:46,278
So now what are you doin'?

109
00:16:46,400 --> 00:16:48,994
I'm gonna conjure up a spirit,

110
00:16:49,120 --> 00:16:54,035
and it's gonna appear
right here in this triangle.

111
00:16:54,160 --> 00:16:56,515
Yeah. OK.

112
00:16:57,880 --> 00:16:59,791
I believe you.

113
00:17:02,120 --> 00:17:04,759
Jonathan, how do you know
how to do that?

114
00:17:04,880 --> 00:17:08,839
I don't know. I must have read it in a book.

115
00:17:11,160 --> 00:17:16,314
Rebecca, take the rock
and put it at the north point of the circle.

116
00:17:16,440 --> 00:17:17,839
Where?

117
00:17:17,960 --> 00:17:22,431
Take the candle
and put it at the south point.

118
00:17:22,560 --> 00:17:24,516
Right there.

119
00:17:35,040 --> 00:17:36,871
<i>Yod hay vau hay.</i>

120
00:17:40,520 --> 00:17:42,431
<i>Yod hay vau hay.</i>

121
00:17:45,120 --> 00:17:48,112
<i>Yod hay vau hay.</i>

122
00:17:52,960 --> 00:17:54,712
<i>Yod hay vau hay.</i>

123
00:17:54,840 --> 00:17:56,910
You do the hokey-pokey and you turn...

124
00:17:57,040 --> 00:17:59,713
Hey, knock it off!

125
00:18:09,320 --> 00:18:11,436
<i>Ateh malkuth...</i>

126
00:18:13,360 --> 00:18:16,318
<i>ve-gedulah ve-geburah...</i>

127
00:18:16,840 --> 00:18:19,354
<i>le-olahm. Amen.</i>

128
00:18:20,320 --> 00:18:24,598
<i>Te nunc transforma in formam veram</i>

129
00:18:24,720 --> 00:18:28,315
<i>quod eludet species mentem meam.</i>

130
00:18:29,720 --> 00:18:32,075
Shut up, goddammit!

131
00:18:32,200 --> 00:18:35,829
I'm sorry. Jeez.

132
00:18:44,080 --> 00:18:47,834
O Surgat, I conjure thee...

133
00:18:49,200 --> 00:18:52,670
that thou come
before this circle immediately

134
00:18:52,800 --> 00:18:55,997
and agree to obey my orders.

135
00:18:56,120 --> 00:19:01,672
Come, I order thee,
in the name of the most Holy Trinity.

136
00:19:01,800 --> 00:19:07,238
I order thee to manifest thyself at once,
without injury to me or any in this room.

137
00:19:08,680 --> 00:19:12,036
Come, I order thee.

138
00:19:18,200 --> 00:19:24,070
Come, I order thee,
in the name of the most Holy Trinity.

139
00:19:26,600 --> 00:19:29,273
Come, I order thee.

140
00:19:43,880 --> 00:19:46,235
Well, that was fun.

141
00:19:47,080 --> 00:19:50,390
- Yeah, let's go get another beer.
- Nice try, Jon.

142
00:19:51,080 --> 00:19:54,675
- Where's Robin?
- I don't know.

143
00:19:54,800 --> 00:19:57,951
- Maybe the toad monster got her.
- I don't think that's funny.

144
00:19:58,080 --> 00:20:01,550
She's not the kind who would just leave.

145
00:20:01,680 --> 00:20:04,274
Honey, I think we'd better go find her.

146
00:20:04,400 --> 00:20:08,359
- Yeah, come on.
- No, wait. I need to dismiss the spirit.

147
00:20:08,480 --> 00:20:10,914
So do I. Where's the bathroom?

148
00:20:11,040 --> 00:20:13,395
Upstairs.

149
00:20:13,520 --> 00:20:15,476
Come on.

150
00:21:00,080 --> 00:21:02,435
- Robin?
- Hey, Robin!

151
00:21:05,520 --> 00:21:07,476
Jeez!

152
00:21:20,400 --> 00:21:22,356
Dude.

153
00:21:23,280 --> 00:21:25,032
- Dude.
- What?

154
00:21:25,160 --> 00:21:27,993
I don't think she's in here.

155
00:21:28,120 --> 00:21:31,078
Well, I don't think so either.

156
00:21:33,120 --> 00:21:37,750
- Hey, let's check out the closet.
- Check out the closet.

157
00:21:48,120 --> 00:21:50,076
You bozo!

158
00:21:58,880 --> 00:22:01,599
- Hey, Mike. Mike.
- What?

159
00:22:01,720 --> 00:22:04,280
Wasn't he over there?

160
00:22:10,840 --> 00:22:13,798
I don't think she's here.

161
00:22:23,360 --> 00:22:26,272
Robin! Hey, Robin. Where are you?

162
00:22:26,400 --> 00:22:29,358
Hey! Robin.

163
00:22:30,160 --> 00:22:32,515
Robin! Hey, come on.

164
00:22:36,360 --> 00:22:38,920
There she is.

165
00:22:42,080 --> 00:22:44,435
Hey, Robin, what are you doing?

166
00:22:44,560 --> 00:22:45,959
Robin.

167
00:22:46,080 --> 00:22:47,718
Robin?

168
00:22:47,840 --> 00:22:51,594
- You OK?
- Yeah, I guess so.

169
00:22:52,760 --> 00:22:58,073
Come on, little girl.
I'll have you feelin' better in no time.

170
00:23:02,360 --> 00:23:06,035
- I made a big decision today.
- What?

171
00:23:06,160 --> 00:23:10,790
Well... I decided to withdraw
from school this quarter.

172
00:23:13,000 --> 00:23:16,390
That's the stupidest thing I've ever heard.

173
00:23:16,520 --> 00:23:20,752
Listen, I just think
I should take some time off.

174
00:23:20,880 --> 00:23:23,678
Stay home and fix this place up.

175
00:23:23,800 --> 00:23:27,998
I walked around this place all day today.
I thought of these great things I could do.

176
00:23:28,120 --> 00:23:31,715
Terrific. Can't you just wait until
you've graduated to fix up the house?

177
00:23:31,840 --> 00:23:35,230
No, I can't wait.
I got this urge to do it now.

178
00:23:36,600 --> 00:23:43,073
Look, when I inherited this house,
I inherited a lot of responsibility.

179
00:23:43,200 --> 00:23:45,919
I'm just trying to do things right.

180
00:23:46,040 --> 00:23:48,918
I want this to be a nice home
for both of us.

181
00:23:49,040 --> 00:23:53,955
This place is gonna need work and
I can't do it if I'm going to school full time.

182
00:23:54,080 --> 00:23:56,833
You understand?

183
00:23:58,240 --> 00:24:00,595
Yeah, I understand.

184
00:24:03,840 --> 00:24:05,956
I just want good things for you.

185
00:24:08,360 --> 00:24:10,316
I know.

186
00:25:17,640 --> 00:25:19,756
<i>And so it had begun.</i>

187
00:25:19,880 --> 00:25:22,348
<i>The evil one willing the boy on,</i>

188
00:25:22,480 --> 00:25:26,393
<i>using the boy's curiosity</i>
<i>as a powerful weapon.</i>

189
00:25:26,520 --> 00:25:33,073
<i>I could but sit and wait, gripped</i>
<i>by the fear of events foreshadowed.</i>

190
00:27:39,760 --> 00:27:43,070
- Hey, kiddo.
- Hi. How did it go today?

191
00:27:43,200 --> 00:27:45,760
See for yourself.

192
00:27:48,800 --> 00:27:51,792
I can't believe it.

193
00:27:56,760 --> 00:28:00,355
You know something?
I'm really proud of you.

194
00:28:00,480 --> 00:28:02,596
Wait. There's more.

195
00:28:06,040 --> 00:28:07,996
Open it.

196
00:28:14,480 --> 00:28:17,040
It's really beautiful.

197
00:28:21,920 --> 00:28:23,876
- What is it?
- It's a talisman.

198
00:28:24,000 --> 00:28:26,389
It'll protect you from...

199
00:28:26,520 --> 00:28:29,956
- From what?
- From everything.

200
00:28:30,080 --> 00:28:35,598
- Even my physics professor?
- Mr. Bolton? Yes, especially him.

201
00:28:35,720 --> 00:28:38,996
You never take it off, OK?

202
00:28:39,120 --> 00:28:41,680
I promise. I love it.

203
00:29:07,680 --> 00:29:09,796
Jonathan?

204
00:29:10,520 --> 00:29:13,273
Dinner's ready!

205
00:29:13,400 --> 00:29:15,356
Jonathan?

206
00:29:18,160 --> 00:29:22,119
Are you OK? You look terrible.

207
00:29:23,720 --> 00:29:25,676
I'm fine.

208
00:29:25,800 --> 00:29:29,713
- Well, come and have dinner.
- No. I'm not eating.

209
00:29:31,160 --> 00:29:34,118
- Why not?
- I'm...

210
00:29:35,400 --> 00:29:37,755
I'm fasting.

211
00:29:39,000 --> 00:29:41,355
You're fasting?

212
00:29:48,080 --> 00:29:53,996
Jonathan, I don't know what you're up to,
but I just cooked dinner for us.

213
00:29:54,120 --> 00:29:57,669
So you can do
whatever the hell you wanna do.

214
00:30:04,000 --> 00:30:07,117
O Paimon, I conjure thee.

215
00:30:08,120 --> 00:30:11,271
Empowered with the strength
of the greatest power.

216
00:30:25,520 --> 00:30:30,196
I command thee, creature of the water,

217
00:30:31,160 --> 00:30:34,789
bring forth life unto this world.

218
00:30:36,200 --> 00:30:41,433
I command thee, come. At once!

219
00:31:21,720 --> 00:31:24,280
Welcome to this world.

220
00:31:28,040 --> 00:31:30,600
I am your master.

221
00:31:39,880 --> 00:31:42,917
You may roam about the grounds,

222
00:31:43,040 --> 00:31:47,670
but you must remain invisible
to all but me.

223
00:31:47,800 --> 00:31:51,759
Remain obedient to me at all times.

224
00:31:55,400 --> 00:31:57,595
Understood?

225
00:32:18,520 --> 00:32:24,550
I call upon thee, Vepar,
and thee, Procel, and thee, Ashtaroth.

226
00:32:25,920 --> 00:32:29,356
Bring forth the fury
of thy tempest before me.

227
00:32:29,480 --> 00:32:31,630
With these words:

228
00:32:31,760 --> 00:32:35,878
<i>non humus somnifera exsulat</i>

229
00:32:36,000 --> 00:32:38,230
<i>ignis et que erat.</i>

230
00:32:38,360 --> 00:32:40,715
<i>Somnifera facta sunt.</i>

231
00:32:43,000 --> 00:32:45,958
Come. Now!

232
00:32:46,080 --> 00:32:48,389
Show me thy might!

233
00:32:48,520 --> 00:32:53,071
Come now, that I might
anoint myself with thy power!

234
00:32:54,200 --> 00:32:57,431
Yes! Come now!

235
00:33:13,840 --> 00:33:15,831
Yes! Yes!

236
00:33:39,320 --> 00:33:42,676
Thank you, Vepar, Procel, Ashtaroth,

237
00:33:42,800 --> 00:33:46,793
for having appeared
and for having fulfilled my request.

238
00:33:46,920 --> 00:33:50,196
Thou mayest depart in peace.

239
00:33:50,320 --> 00:33:53,517
Ye shall return when I call upon thee.

240
00:34:01,440 --> 00:34:05,558
Now, indeed you are
a powerful instrument.

241
00:34:05,680 --> 00:34:07,079
Jonathan?

242
00:34:09,160 --> 00:34:11,913
What are you doing?

243
00:34:12,040 --> 00:34:16,318
Rebecca. You're home early.

244
00:34:16,440 --> 00:34:19,352
Would you please tell me
what's going on here?

245
00:34:27,400 --> 00:34:29,516
I get it.

246
00:34:31,080 --> 00:34:33,150
Oh, no. Don't change for me.

247
00:34:33,280 --> 00:34:35,794
I will leave you here
to do whatever you're doing,

248
00:34:35,920 --> 00:34:39,196
and I will go upstairs
and make dinner for myself,

249
00:34:39,320 --> 00:34:42,073
since you're not even eating these days.

250
00:34:42,200 --> 00:34:45,272
- No, Becky, wait.
- No, don't!

251
00:34:46,760 --> 00:34:50,514
Becky, listen to me!
Becky, come on, wait!

252
00:34:53,240 --> 00:34:55,196
Rebecca.

253
00:34:56,080 --> 00:34:58,435
Look, will you listen...

254
00:35:02,680 --> 00:35:05,035
Look, talk to me.

255
00:35:21,920 --> 00:35:24,309
What the hell's going on here, Jonathan?

256
00:35:24,440 --> 00:35:29,389
You tell me that you wanna drop out of
school so that you can fix up the house.

257
00:35:29,520 --> 00:35:31,590
- Right?
- Right.

258
00:35:31,720 --> 00:35:35,599
So I come home and I find you...

259
00:35:35,720 --> 00:35:38,871
in the middle of I don't even know what.

260
00:35:41,320 --> 00:35:43,072
- Why'd you lie?
- I didn't lie.

261
00:35:43,200 --> 00:35:46,988
I have been working on the house.
That's all I've been doing.

262
00:35:47,120 --> 00:35:50,999
I got sidetracked. I figure this stuff
has got something to do with my parents.

263
00:35:51,120 --> 00:35:55,113
I don't know who they were,
so I'm curious. Don't you understand that?

264
00:35:55,240 --> 00:35:59,552
No, I don't understand it. That's just
the problem. I don't understand.

265
00:35:59,680 --> 00:36:01,636
Don't do this to me.

266
00:36:01,760 --> 00:36:06,311
When I walked in that room,
I saw someone that I didn't even know.

267
00:36:06,440 --> 00:36:09,876
I saw... a stranger.

268
00:36:12,080 --> 00:36:14,355
Rebecca, please.

269
00:36:14,480 --> 00:36:18,155
Look, it's nothing. Honest.

270
00:36:19,360 --> 00:36:24,195
Look, I'll cut it out. I swear.

271
00:36:24,320 --> 00:36:26,675
Rebecca, I love you.

272
00:36:27,680 --> 00:36:30,035
You know that.

273
00:36:36,440 --> 00:36:39,398
I just get scared, that's all.

274
00:36:40,520 --> 00:36:43,910
It's the last time
you'll hear anything about it.

275
00:36:44,040 --> 00:36:45,996
I swear.

276
00:37:45,400 --> 00:37:47,356
Hi.

277
00:37:47,680 --> 00:37:49,636
Hi.

278
00:37:53,720 --> 00:37:56,518
- Rebecca.
- Hm?

279
00:37:56,640 --> 00:37:59,200
I love you.

280
00:37:59,320 --> 00:38:01,675
Well, I love you, too.

281
00:39:03,400 --> 00:39:07,359
What are you saying?

282
00:39:09,320 --> 00:39:12,471
What are you saying?

283
00:39:17,840 --> 00:39:20,673
What? Jonathan.

284
00:39:20,800 --> 00:39:22,870
What are you saying?

285
00:39:23,000 --> 00:39:25,639
Jonathan. What are you doing?

286
00:39:28,520 --> 00:39:31,671
Stop it! Jonathan!

287
00:39:31,800 --> 00:39:34,155
What are you doing?

288
00:39:41,960 --> 00:39:44,269
You bastard!

289
00:39:44,400 --> 00:39:47,392
- Becky.
- No, that's it. I've had it.

290
00:39:48,880 --> 00:39:51,155
- Listen...
- No, you listen to me.

291
00:39:51,280 --> 00:39:57,515
I've had it with your black magic,
your rituals, your lies, everything.

292
00:39:57,640 --> 00:39:59,995
Goodbye, Jonathan.

293
00:40:04,760 --> 00:40:08,355
<i>Leviatan, Ermogasa, Vmirteat, lorantga.</i>

294
00:40:08,480 --> 00:40:11,756
<i>Agtnaroi, Taetrimv, Asagomre, Nataivel.</i>

295
00:40:12,360 --> 00:40:15,750
<i>Leviatan, Ermogasa, Vmirteat, lorantga.</i>

296
00:40:15,880 --> 00:40:19,919
<i>Agtnaroi, Taetrimv, Asagomre, Nataivel!</i>

297
00:40:39,760 --> 00:40:43,469
- We are here, Master.
- What would you have of us?

298
00:40:43,600 --> 00:40:46,160
I will have you serve me.

299
00:40:48,600 --> 00:40:51,512
- That precisely...
- Is why you've called us, Master.

300
00:40:51,640 --> 00:40:54,200
- And we will.
- Most faithfully.

301
00:40:55,160 --> 00:40:58,948
Forsaking all others... you will obey me?

302
00:40:59,080 --> 00:41:02,038
- Unquestioningly.
- We are yours.

303
00:41:33,520 --> 00:41:35,476
Drink.

304
00:41:37,440 --> 00:41:41,228
- How may we drink?
- If you do not release us from the triangle.

305
00:41:41,360 --> 00:41:46,639
If you are worthy to serve me,
you will drink.

306
00:41:46,760 --> 00:41:50,389
If you are not worthy, you will burn.

307
00:41:51,000 --> 00:41:53,195
The choice is yours.

308
00:42:14,000 --> 00:42:16,958
- Me. Me first.
- No, me first.

309
00:42:28,800 --> 00:42:31,268
Excellent. Well done.

310
00:42:42,600 --> 00:42:44,556
What are your names?

311
00:42:44,680 --> 00:42:47,672
- I'm called Grizzel.
- And I am called Greedigut.

312
00:42:47,800 --> 00:42:50,394
- And who am I?
- You are Jonathan.

313
00:42:50,520 --> 00:42:53,432
Our master. What is it you wish, Master?

314
00:42:53,560 --> 00:42:56,677
Knowledge. And power.

315
00:42:59,360 --> 00:43:01,351
These things we can do.

316
00:43:01,480 --> 00:43:05,075
What about Rebecca?
Do you want her as well?

317
00:43:05,200 --> 00:43:09,591
- You know of Rebecca?
- We know of many things, O Master.

318
00:43:09,720 --> 00:43:13,713
- Yes, I want her.
- Then you shall have her.

319
00:43:13,840 --> 00:43:17,071
- As for the other things...
- There must be a master ritual.

320
00:43:17,200 --> 00:43:19,760
There must be seven others
besides yourself.

321
00:43:19,880 --> 00:43:22,394
When the moon is full.

322
00:43:22,520 --> 00:43:25,592
The ritual is fraught with danger, Master.

323
00:43:25,720 --> 00:43:27,472
- Don't speak of it.
- What?

324
00:43:27,600 --> 00:43:31,115
- Answer me now!
- The ritual is dangerous.

325
00:43:31,240 --> 00:43:34,312
But you must perform it
if you want knowledge.

326
00:43:34,440 --> 00:43:35,793
And power.

327
00:43:37,160 --> 00:43:38,832
And power.

328
00:44:25,320 --> 00:44:27,880
What do you want?

329
00:44:28,520 --> 00:44:31,910
You. I want you.

330
00:44:32,040 --> 00:44:36,431
I want you to come away with me now,
tonight, away from this house.

331
00:44:36,560 --> 00:44:39,791
No. Never.

332
00:44:39,920 --> 00:44:42,878
I could never leave this place.

333
00:44:49,680 --> 00:44:52,353
Your eyes.

334
00:44:52,480 --> 00:44:55,153
What have you done to your eyes?

335
00:44:55,280 --> 00:44:59,193
It's a sign. A sign of power.

336
00:45:01,640 --> 00:45:04,438
I don't want this.

337
00:45:04,560 --> 00:45:08,838
I love you, Jonathan, but I'm leaving.

338
00:45:08,960 --> 00:45:11,315
It's over!

339
00:45:14,120 --> 00:45:16,236
Becky, I love you.

340
00:45:27,040 --> 00:45:30,271
I'm so sorry, Jonathan.

341
00:45:30,400 --> 00:45:32,470
Please forgive me.

342
00:45:42,480 --> 00:45:44,755
<i>The evil one had him now.</i>

343
00:45:44,880 --> 00:45:49,556
<i>The things that would be unleashed</i>
<i>that night were to be horrific.</i>

344
00:45:49,680 --> 00:45:52,194
<i>And I was powerless to stop it.</i>

345
00:45:55,440 --> 00:45:58,671
What is that? Mushroom dip?

346
00:45:58,800 --> 00:46:01,155
There's a lot of it.

347
00:46:02,240 --> 00:46:05,550
- Nice house.
- It's really nice.

348
00:46:05,680 --> 00:46:08,114
Really grand. It's really special.

349
00:46:08,240 --> 00:46:11,391
Really wonderful.
You've invited me here and all that.

350
00:46:11,520 --> 00:46:15,559
You know what? You're a prince, an earl.
A prince among earls.

351
00:46:15,680 --> 00:46:18,353
King of the soir�e.

352
00:46:18,480 --> 00:46:20,471
Bad manners, dude.

353
00:46:23,280 --> 00:46:26,397
I'm sorry. Rebecca, I'm really sorry.

354
00:46:28,240 --> 00:46:31,835
- Small faux pas.
- Don't worry about it, Mark.

355
00:46:31,960 --> 00:46:36,238
You know what they say: a faux pas a day
helps keep your friends away.

356
00:46:47,680 --> 00:46:49,989
- I'm really sorry.
- Mark, it's OK.

357
00:46:50,120 --> 00:46:52,076
No, it's not...

358
00:47:14,000 --> 00:47:17,629
How come you and Jonathan
aren't eating?

359
00:47:19,040 --> 00:47:20,996
We ate already.

360
00:47:21,120 --> 00:47:24,078
Rebecca, why don't you sit down?

361
00:47:25,840 --> 00:47:28,400
All right, darling.

362
00:47:30,360 --> 00:47:34,592
Why are we wearing these ridiculous
glasses? I can barely see what I'm eating.

363
00:47:34,720 --> 00:47:39,635
It's all part of the evening I have planned.
It's gonna be great.

364
00:47:47,880 --> 00:47:49,836
A toast.

365
00:47:55,560 --> 00:47:59,553
To my friends. Forgive me.

366
00:47:59,680 --> 00:48:03,195
<i>Mesabim, Etosa.</i>

367
00:48:03,920 --> 00:48:05,478
<i>Boros, Azote,</i>

368
00:48:07,520 --> 00:48:09,476
Drink.

369
00:48:37,360 --> 00:48:39,715
<i>Queladim!</i>

370
00:48:43,040 --> 00:48:44,996
Come.

371
00:49:00,960 --> 00:49:05,351
<i>Recabustira, Cabustira, Bustira, Tira, Ta.</i>

372
00:49:05,480 --> 00:49:08,870
Hear me, all you hosts gathered here.

373
00:49:09,680 --> 00:49:11,910
Give me the knowledge.

374
00:49:12,040 --> 00:49:16,477
I will have the wisdom and the power!

375
00:49:19,240 --> 00:49:21,800
Answer me, my friends.

376
00:49:23,320 --> 00:49:25,436
<i>Beseder, Eliele.</i>

377
00:49:25,560 --> 00:49:27,198
<i>Sepped, Eliele.</i>

378
00:49:27,320 --> 00:49:29,629
<i>Beseder, Eliele.</i>

379
00:49:29,760 --> 00:49:31,352
<i>Sepped, Eliele.</i>

380
00:49:31,480 --> 00:49:33,596
<i>Beseder, Eliele!</i>

381
00:49:33,720 --> 00:49:35,438
<i>Sepped, Eliele.</i>

382
00:49:35,560 --> 00:49:38,552
<i>Beseder, Eliele!</i>

383
00:49:38,680 --> 00:49:40,398
<i>Sepped, Eliele.</i>

384
00:49:40,520 --> 00:49:43,830
<i>- Beseder, Eliele!</i>
<i>- Sepped, Eliele.</i>

385
00:49:43,960 --> 00:49:48,829
<i>- Beseder, Eliele!</i>
<i>- Sepped, Eliele.</i>

386
00:49:48,960 --> 00:49:53,397
<i>- Beseder, Eliele!</i>
<i>- Sepped, Eliele.</i>

387
00:49:53,520 --> 00:49:59,550
<i>Sepped, Eliele.</i>
<i>Sepped, Eliele. Sepped, Eliele.</i>

388
00:50:01,320 --> 00:50:05,711
By the force
of these terrible words of power...

389
00:50:06,360 --> 00:50:11,957
<i>Aglon! Tetragram! Retragrammaton!</i>

390
00:50:12,480 --> 00:50:16,519
<i>Soter Emanuel! Te invoco!</i>

391
00:50:18,360 --> 00:50:21,397
Aagh!

392
00:51:13,400 --> 00:51:17,154
A toast. To my friends.

393
00:51:25,920 --> 00:51:29,276
Rebecca and I are going to retire early.

394
00:51:29,400 --> 00:51:31,356
You're welcome to spend the night.

395
00:51:31,480 --> 00:51:34,313
The house is yours. Just pick a room.

396
00:51:35,560 --> 00:51:40,475
- Wine cellar.
- Oh! You madman. Yeah!

397
00:51:40,600 --> 00:51:44,354
- Good night, everyone.
- Good night.

398
00:51:49,280 --> 00:51:51,794
So... can't wait to pick a room.

399
00:52:27,000 --> 00:52:32,950
Children, your true master has returned,
and tonight you will do my bidding.

400
00:52:37,920 --> 00:52:43,392
Baby, I've been waiting
for this moment ever since we met.

401
00:52:43,520 --> 00:52:45,476
All right!

402
00:52:52,920 --> 00:52:55,480
Mark, would you hurry up?

403
00:53:05,080 --> 00:53:08,038
God, it's kinda spooky out here.

404
00:53:09,120 --> 00:53:13,955
Wasn't Rebecca acting kinda weird at
dinner? The way she looked at me when...

405
00:53:14,080 --> 00:53:16,036
- Gotcha!
- God!

406
00:53:17,480 --> 00:53:22,270
Come on. Let's sit down.
Come on over here. Come on.

407
00:53:22,400 --> 00:53:24,470
- Right here.
- That wasn't funny.

408
00:53:24,600 --> 00:53:26,556
Sorry.

409
00:53:31,560 --> 00:53:34,313
- Nice night.
- Spooky.

410
00:53:36,800 --> 00:53:39,872
Is that a little girl's tum?

411
00:53:40,000 --> 00:53:44,118
Don't, Mark! You're tickling me!
Please stop!

412
00:53:46,280 --> 00:53:50,239
Sounds like Toad Boy's on the attack.

413
00:54:01,400 --> 00:54:03,834
Mark!

414
00:54:03,960 --> 00:54:06,679
My... Mark, my bracelet.

415
00:54:06,800 --> 00:54:09,758
Don't worry. I shall retrieve it.

416
00:54:12,360 --> 00:54:14,316
I can't see anything.

417
00:54:28,680 --> 00:54:30,671
Yuck! I touched something weird.

418
00:54:35,640 --> 00:54:36,959
Mark.

419
00:54:46,560 --> 00:54:48,516
What the hell's in there?

420
00:55:18,600 --> 00:55:21,751
Man, that chick is really a screamer.

421
00:55:33,040 --> 00:55:35,429
Where are you going?

422
00:55:35,560 --> 00:55:37,835
Water. Want some?

423
00:55:37,960 --> 00:55:39,916
No, thanks.

424
00:55:44,960 --> 00:55:47,918
- I'll be back.
- I'll be waiting.

425
00:56:17,000 --> 00:56:20,356
Aagh!

426
00:56:39,080 --> 00:56:42,277
Who are you?

427
00:56:42,400 --> 00:56:44,356
Come here.

428
00:56:48,120 --> 00:56:51,635
No doubt about it, Mr. Dick.

429
00:56:51,760 --> 00:56:54,320
You are a lucky guy.

430
00:57:15,640 --> 00:57:18,200
Who are you?

431
00:57:41,520 --> 00:57:44,080
You got any matches?

432
00:57:45,080 --> 00:57:48,038
- Yeah. I got reserves.
- Great.

433
00:57:54,400 --> 00:57:56,960
You got any that work? Jeez!

434
00:57:58,520 --> 00:58:02,798
Well, there's gotta be some around here.

435
00:58:02,920 --> 00:58:04,876
I'll be back.

436
00:58:15,240 --> 00:58:19,870
I'm sorry, Rebecca.
I'm sorry I had to do it this way.

437
00:58:22,120 --> 00:58:24,429
But it worked.

438
00:58:24,560 --> 00:58:27,870
It'll all be worth it. You'll see.

439
00:58:29,960 --> 00:58:32,918
I just hope you can forgive me.

440
00:58:37,040 --> 00:58:38,996
Jonathan?

441
00:58:41,920 --> 00:58:43,876
What am I?

442
00:59:03,640 --> 00:59:08,475
- I'll tell you about it in the morning.
- Yeah, I'm so tired.

443
00:59:10,960 --> 00:59:12,916
Sleep now.

444
00:59:35,840 --> 00:59:37,956
Matches.

445
00:59:38,080 --> 00:59:40,275
Matches...

446
00:59:40,400 --> 00:59:42,630
Matches...

447
00:59:46,000 --> 00:59:47,956
Nah.

448
01:00:20,000 --> 01:00:22,560
No, don't do it here.

449
01:00:23,640 --> 01:00:26,996
- Outside.
- Yes, all right.

450
01:00:28,960 --> 01:00:30,916
Pull!

451
01:00:39,240 --> 01:00:41,196
Shit.

452
01:00:42,080 --> 01:00:44,036
Where's that guy?

453
01:00:51,480 --> 01:00:53,835
There it is. Get it.

454
01:00:55,360 --> 01:00:59,319
- I despise these creatures.
- Quiet!

455
01:01:26,200 --> 01:01:30,193
Malcolm is the master, blast his soul. He
always has been and will be until you rot.

456
01:01:30,320 --> 01:01:33,630
No, it is not so.
I cannot do this thing. I cannot.

457
01:01:33,760 --> 01:01:36,718
Ooh! Oh! Ow! Oh!

458
01:01:38,520 --> 01:01:41,830
Quiet. Quiet. He'll burn us both.

459
01:02:07,760 --> 01:02:11,435
Hey, dude. Don't bogart that joint.

460
01:03:43,880 --> 01:03:49,034
Come to me. Come to me, my children.

461
01:03:49,160 --> 01:03:51,390
All of you.

462
01:03:51,520 --> 01:03:54,080
Let me show you my love.

463
01:03:55,800 --> 01:03:57,756
Come to me now.

464
01:04:38,080 --> 01:04:39,672
Jonathan?

465
01:04:39,800 --> 01:04:42,360
Oh, boy. What a dream.

466
01:05:30,280 --> 01:05:32,396
Why, Jonathan? Why?

467
01:05:32,520 --> 01:05:34,476
Becky!

468
01:05:37,000 --> 01:05:38,513
No!

469
01:05:45,440 --> 01:05:47,396
Becky?

470
01:05:49,080 --> 01:05:51,036
No.

471
01:05:52,360 --> 01:05:54,316
Becky.

472
01:05:54,440 --> 01:05:56,396
My God.

473
01:05:59,040 --> 01:06:02,589
I can make you live. I've got the power.

474
01:06:20,920 --> 01:06:22,876
Becky.

475
01:06:28,040 --> 01:06:31,555
Where's my robe?
Where the hell's my robe?

476
01:07:09,960 --> 01:07:12,520
Aren't you going to greet your father?

477
01:07:13,440 --> 01:07:15,874
You did my bidding well, Jonathan.

478
01:07:16,720 --> 01:07:18,472
You...

479
01:07:18,600 --> 01:07:21,592
Grizzel, Greedigut.

480
01:07:21,720 --> 01:07:24,757
You tricked me. I'm your master.

481
01:07:24,880 --> 01:07:28,429
No! I am the master.

482
01:07:28,560 --> 01:07:33,429
I own them as I now own you.

483
01:07:35,040 --> 01:07:38,999
Come... and greet your father.

484
01:07:41,720 --> 01:07:45,076
Vepar, Procel, Ashtaroth. Help me now!

485
01:07:50,560 --> 01:07:54,678
Where are your gods now?
They too are mine.

486
01:07:59,200 --> 01:08:01,555
Not bad.

487
01:08:02,240 --> 01:08:05,869
Although at your age I was much better.

488
01:08:07,560 --> 01:08:09,551
You...

489
01:08:09,680 --> 01:08:12,035
You killed them.

490
01:08:13,400 --> 01:08:15,356
You killed Rebecca.

491
01:08:16,360 --> 01:08:18,078
Hm?

492
01:08:18,200 --> 01:08:20,760
Yes, I suppose I did.

493
01:08:21,840 --> 01:08:24,434
But it was you I really wanted, Jonathan.

494
01:08:24,560 --> 01:08:26,835
You were to resurrect me.

495
01:08:26,960 --> 01:08:31,909
And what should have happened
25 years ago will happen now.

496
01:08:34,080 --> 01:08:36,116
I will have your youth, Jonathan.

497
01:08:43,360 --> 01:08:47,194
Greater things
command all of us, Jonathan.

498
01:08:49,000 --> 01:08:52,709
I suppose that in another time,
another place,

499
01:08:52,840 --> 01:08:55,559
I could have loved you as my son.

500
01:09:00,720 --> 01:09:08,434
My soul is not my own,
and you must be my sacrifice to Lucifer.

501
01:09:21,120 --> 01:09:23,998
Again... a good effort.

502
01:09:25,800 --> 01:09:29,031
But it is your love
that weakens you, Jonathan.

503
01:09:30,600 --> 01:09:32,909
You can never oppose me.

504
01:09:37,440 --> 01:09:39,396
A shame.

505
01:09:41,120 --> 01:09:43,236
She's so pretty.

506
01:09:43,360 --> 01:09:46,318
May you rot!

507
01:09:51,200 --> 01:09:53,760
- Becky.
- Jonathan.

508
01:09:55,440 --> 01:09:57,476
Forgive me.

509
01:09:57,600 --> 01:10:00,319
I love you, Jonathan. Please.

510
01:10:03,160 --> 01:10:06,391
Take me away from here. Take me.

511
01:10:07,560 --> 01:10:10,279
Hold me.

512
01:10:10,400 --> 01:10:12,755
Kiss me.

513
01:10:14,520 --> 01:10:16,476
I love you.

514
01:10:16,600 --> 01:10:20,639
- Oh, Becky. I'm so sorry.
- It's all right, my love.

515
01:10:23,400 --> 01:10:25,709
Come to me.

516
01:10:25,840 --> 01:10:28,559
No, Master Jonathan. Don't listen.

517
01:10:28,680 --> 01:10:30,750
It's a trap, yes!

518
01:10:30,880 --> 01:10:33,110
No!

519
01:10:33,240 --> 01:10:36,676
You little worms!
You shall burn for this later.

520
01:10:36,800 --> 01:10:41,032
But now... I am tired of playing.

521
01:10:44,400 --> 01:10:46,960
You try my patience.

522
01:11:07,680 --> 01:11:12,390
You are my life! And I will have my life!

523
01:11:13,000 --> 01:11:15,753
Kiss me, my son.

524
01:11:15,880 --> 01:11:17,836
Kiss me.

525
01:11:17,960 --> 01:11:20,872
Let me drain the life from your lips.

526
01:11:26,640 --> 01:11:29,791
You will not have the boy's life!

527
01:11:29,920 --> 01:11:34,596
You are no match for me, old man.

528
01:12:10,440 --> 01:12:13,910
Die, evil one!

529
01:12:43,680 --> 01:12:47,912
Get out of here while you still can!

530
01:13:11,880 --> 01:13:13,836
Aagh!

531
01:13:30,480 --> 01:13:32,436
No. No.

532
01:13:35,400 --> 01:13:39,279
Dick, come on, wake up.
We gotta get outta here.

533
01:13:39,400 --> 01:13:42,631
- Come on! Let's go.
- Jonathan. How?

534
01:13:42,760 --> 01:13:44,716
Don't ask.

535
01:13:53,920 --> 01:13:57,196
- What's going on?
- Just get in the car.

536
01:13:57,320 --> 01:13:59,880
You got a cigarette on you?

537
01:14:24,440 --> 01:14:27,512
Would somebody mind telling me
what the hell is going on?

538
01:14:27,640 --> 01:14:31,189
It's a long story, Mike. But it's over now.

539
01:14:32,480 --> 01:14:36,155
Is it, Jonathan? Is it really?

540
01:14:36,280 --> 01:14:38,236
Yeah, really.

