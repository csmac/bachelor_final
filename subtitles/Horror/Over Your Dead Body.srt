﻿1
00:02:02,305 --> 00:02:18,105
<font color="#ffff80"><b>Fixed & Synced by bozxphd.Enjoy The Flick</b></font>

2
00:02:18,306 --> 00:02:20,225
"Yotsuya Kwaidan."

3
00:03:20,493 --> 00:03:22,245
- Good morning.
- Hi.

4
00:03:54,110 --> 00:03:55,820
Kosuke's behind us.

5
00:03:59,699 --> 00:04:04,077
Sorry about last night.
I was held up.

6
00:04:04,078 --> 00:04:06,164
I'll make it up to you.

7
00:04:46,454 --> 00:04:47,622
Hello.

8
00:04:48,539 --> 00:04:52,669
"Kosuke Hasegawa - In."

9
00:05:00,802 --> 00:05:02,303
Hello.

10
00:05:28,496 --> 00:05:30,581
- How do I look?
- Mean.

11
00:06:12,832 --> 00:06:17,003
"Yotsuya Kwaidan, The Play"

12
00:06:28,473 --> 00:06:30,391
<i>5 and 4. Odd number.</i>

13
00:06:52,705 --> 00:06:55,249
<i>Dealer rolls! Place your bets!</i>

14
00:07:01,130 --> 00:07:02,423
Hello, Sir.

15
00:07:06,302 --> 00:07:07,512
Yes?

16
00:07:09,055 --> 00:07:13,518
What's that scent on you?

17
00:07:14,685 --> 00:07:16,854
Is it blood?

18
00:07:32,662 --> 00:07:35,540
<i>Borrowed money should
be returned.</i>

19
00:07:36,958 --> 00:07:41,962
<i>But some people
forget to pay it back.</i>

20
00:07:41,963 --> 00:07:47,301
<i>A loan from a blind masseur
is taken lightly.</i>

21
00:07:53,641 --> 00:07:59,397
I'm not a noble samurai
like yourself, Sir.

22
00:08:01,274 --> 00:08:02,733
For me,

23
00:08:03,860 --> 00:08:06,529
I can only depend on money.

24
00:08:10,616 --> 00:08:12,994
True, I am a samurai.

25
00:08:13,953 --> 00:08:16,663
But without money or rank

26
00:08:16,664 --> 00:08:20,585
I feel like I'm in a dungeon
without sunlight.

27
00:08:29,051 --> 00:08:30,636
Mister.

28
00:08:33,473 --> 00:08:35,308
How about a good time?

29
00:08:36,976 --> 00:08:38,811
Not a bad idea...

30
00:08:45,318 --> 00:08:47,445
Look at your debauched face.

31
00:08:48,529 --> 00:08:49,529
Asshole!

32
00:08:51,240 --> 00:08:53,284
Give it back to me!

33
00:08:53,993 --> 00:08:55,535
Lovebirds.

34
00:08:55,536 --> 00:08:58,539
It's mine.
A patron gave it to me!

35
00:09:01,209 --> 00:09:03,794
<i>Give it back to me!</i>

36
00:09:07,215 --> 00:09:08,508
Sir.

37
00:09:12,053 --> 00:09:15,765
Who's your new woman?

38
00:09:49,507 --> 00:09:52,509
When I'm with you

39
00:09:52,510 --> 00:09:55,471
time passes in the
blink of an eye.

40
00:09:58,766 --> 00:10:00,518
Leaving so soon?

41
00:10:03,479 --> 00:10:07,148
If I'm not home before Father

42
00:10:07,149 --> 00:10:10,278
he'll never let me
leave the house.

43
00:10:18,828 --> 00:10:20,413
If you had a comb

44
00:10:21,664 --> 00:10:23,249
like this...

45
00:10:27,670 --> 00:10:30,214
I'd always be with you, Iwa.

46
00:11:02,705 --> 00:11:06,833
My daughter wants to
marry you but

47
00:11:06,834 --> 00:11:10,129
I won't leave my family fortune
to you.

48
00:11:15,760 --> 00:11:17,720
Since her mother died

49
00:11:18,638 --> 00:11:22,265
I've raised her on my own.

50
00:11:22,266 --> 00:11:27,521
<i>I won't let a sleazy man like you
have my precious daughter.</i>

51
00:11:33,569 --> 00:11:37,990
<i>Take this money and forget Iwa.</i>

52
00:11:38,991 --> 00:11:41,494
I know all about you.

53
00:11:43,329 --> 00:11:48,584
Did years of unemployment
rot your morals?

54
00:11:48,793 --> 00:11:52,254
You and your shifty
friends con people.

55
00:11:52,463 --> 00:11:54,715
You'd even kill for money!

56
00:11:54,924 --> 00:11:56,884
How despicable!

57
00:11:58,803 --> 00:12:03,808
You give samurai a bad name.
Shame on you!

58
00:12:07,728 --> 00:12:09,313
What amuses you?

59
00:12:22,952 --> 00:12:26,163
You think I'm worth so little.

60
00:12:27,289 --> 00:12:29,458
You're incorrigible.

61
00:12:37,717 --> 00:12:39,468
You brute!

62
00:12:49,186 --> 00:12:52,981
Nobody's listening to
your nonsense!

63
00:12:52,982 --> 00:12:55,151
You useless old fool!

64
00:12:57,319 --> 00:12:59,029
How dare you...

65
00:13:05,035 --> 00:13:06,662
Ah...

66
00:14:15,815 --> 00:14:17,399
You'll catch a cold.

67
00:14:27,952 --> 00:14:31,789
Is your father still missing?

68
00:14:34,124 --> 00:14:37,586
It's already been a month.

69
00:14:42,550 --> 00:14:46,846
Poor Iwa.
You must feel miserable.

70
00:14:48,097 --> 00:14:49,640
Oh, Iemon.

71
00:14:50,766 --> 00:14:55,144
I'm sure he's out
there somewhere.

72
00:14:55,145 --> 00:15:00,067
Maybe he got held up
by some important business.

73
00:15:04,947 --> 00:15:09,158
It breaks my heart to see
you so sad.

74
00:15:09,159 --> 00:15:10,952
But don't worry.

75
00:15:10,953 --> 00:15:14,915
I'll ask around for you.

76
00:15:18,502 --> 00:15:19,920
Iemon.

77
00:15:22,965 --> 00:15:24,925
When I'm with you...

78
00:15:26,135 --> 00:15:28,470
I feel that I can cope.

79
00:15:33,684 --> 00:15:37,813
I wish I was with
you all the time.

80
00:15:40,107 --> 00:15:41,483
Iwa.

81
00:15:43,652 --> 00:15:48,449
Do you mean that we should
get married?

82
00:15:52,161 --> 00:15:53,162
Yes.

83
00:15:55,998 --> 00:15:58,625
That is my wish too.

84
00:15:58,626 --> 00:16:02,171
But your father hasn't
given his approval.

85
00:16:04,465 --> 00:16:05,716
Right?

86
00:16:07,801 --> 00:16:13,264
As he has no sons to
continue the family's line,

87
00:16:13,265 --> 00:16:16,143
he should be happy to have a
successor.

88
00:16:18,562 --> 00:16:24,359
Being the head of this household
might help you

89
00:16:24,360 --> 00:16:27,363
find employment.

90
00:17:56,410 --> 00:17:59,620
<i>Starting a family...</i>

91
00:17:59,621 --> 00:18:01,915
<i>Is that happiness?</i>

92
00:18:04,334 --> 00:18:06,253
I guess so.

93
00:18:08,464 --> 00:18:09,965
For you, too?

94
00:18:12,968 --> 00:18:14,261
Maybe.

95
00:18:17,890 --> 00:18:19,308
What's funny?

96
00:18:32,071 --> 00:18:33,530
You'll catch a cold.

97
00:18:35,157 --> 00:18:36,658
You'll catch a cold.

98
00:19:16,198 --> 00:19:18,492
This is unbearable.

99
00:19:22,913 --> 00:19:25,082
I'm not worthy of you...

100
00:19:29,670 --> 00:19:31,964
I'm so sorry.

101
00:19:35,884 --> 00:19:37,969
Oh, God, sorry!

102
00:19:37,970 --> 00:19:40,388
Sorry. Are you OK?

103
00:19:40,389 --> 00:19:41,765
I'm fine.

104
00:19:46,228 --> 00:19:48,146
Let's have a break.

105
00:19:48,147 --> 00:19:49,648
<i>It's break time!</i>

106
00:20:02,619 --> 00:20:04,245
- Here.
- Thanks.

107
00:20:04,246 --> 00:20:06,290
You OK?

108
00:20:07,499 --> 00:20:11,587
Kayo, can you fill in for me?

109
00:20:15,048 --> 00:20:16,048
But...

110
00:20:16,383 --> 00:20:17,551
Please.

111
00:20:19,761 --> 00:20:20,761
Okay.

112
00:20:25,392 --> 00:20:26,810
I'll do my best.

113
00:20:46,580 --> 00:20:47,706
Iwa.

114
00:20:50,459 --> 00:20:54,838
I know that you resent me.

115
00:20:57,466 --> 00:20:59,509
That's not true.

116
00:21:01,803 --> 00:21:05,473
Since we got together
2 years ago

117
00:21:05,474 --> 00:21:10,062
I've never brought
any money home.

118
00:21:12,689 --> 00:21:15,441
"I gave you a baby boy."

119
00:21:15,442 --> 00:21:19,738
"Even though I've been sick
since his birth."

120
00:21:21,782 --> 00:21:24,909
"I'm the one who works"

121
00:21:24,910 --> 00:21:28,038
"and prepares the meals."

122
00:21:33,543 --> 00:21:35,837
It's written all over your face.

123
00:21:39,466 --> 00:21:40,550
<i>You in here?</i>

124
00:21:44,388 --> 00:21:47,641
Here, it'll relieve the pain.

125
00:21:48,558 --> 00:21:49,726
No, thanks.

126
00:21:53,730 --> 00:21:56,942
He kicked you
even though it's a rehearsal!

127
00:21:58,235 --> 00:21:59,403
Never mind.

128
00:22:01,613 --> 00:22:03,740
What do you see in him?

129
00:22:06,493 --> 00:22:08,578
How are your wife and kid?

130
00:22:09,621 --> 00:22:10,831
Good?

131
00:22:11,748 --> 00:22:15,418
We have our ups and downs...

132
00:22:15,419 --> 00:22:17,336
You look happy.

133
00:22:17,337 --> 00:22:18,839
Don't be deceived.

134
00:22:26,138 --> 00:22:29,725
Men are easily tempted.

135
00:22:32,477 --> 00:22:33,895
Just to be clear.

136
00:22:34,855 --> 00:22:38,024
Married men don't interest me.

137
00:22:38,025 --> 00:22:39,776
You're direct.

138
00:22:41,194 --> 00:22:43,405
Keep your mind on the job.

139
00:22:51,371 --> 00:22:56,668
Sometimes I wish that the play
was my real life.

140
00:22:59,796 --> 00:23:01,214
I don't.

141
00:23:22,652 --> 00:23:24,821
This is unbearable.

142
00:23:27,866 --> 00:23:29,826
I'm not worthy of you!

143
00:23:33,246 --> 00:23:35,082
I'm so sorry!

144
00:24:07,948 --> 00:24:11,660
Women really are something...

145
00:24:15,372 --> 00:24:18,999
So you know all the lines!

146
00:24:19,000 --> 00:24:21,377
Yes, just in case...

147
00:24:21,378 --> 00:24:23,713
You're so prepared!

148
00:24:45,318 --> 00:24:46,986
Thanks for that.

149
00:24:46,987 --> 00:24:48,655
Is your back OK?

150
00:24:49,739 --> 00:24:51,241
I'll get changed.

151
00:24:53,785 --> 00:24:55,078
You alright?

152
00:24:56,329 --> 00:24:57,581
Yes.

153
00:25:47,672 --> 00:25:52,135
I believe you're Mr.
Iemon Tamiya.

154
00:25:55,388 --> 00:25:57,098
And you are...?

155
00:25:58,475 --> 00:26:03,270
I'm a nanny at Mr. Ito's
residence for his girl, Ume.

156
00:26:03,271 --> 00:26:05,023
My name is Maki.

157
00:26:06,483 --> 00:26:11,363
Would you accompany me
to the Ito mansion?

158
00:26:14,533 --> 00:26:19,620
It would be in your interests
if you came.

159
00:26:19,621 --> 00:26:21,373
Master Kihei Ito

160
00:26:22,457 --> 00:26:26,795
wishes to speak with you
in person.

161
00:28:10,482 --> 00:28:15,861
I'll leave it to you,
Grandfather.

162
00:28:15,862 --> 00:28:17,155
Alright.

163
00:28:20,575 --> 00:28:23,578
Ume, I'm happy for you.

164
00:28:28,249 --> 00:28:30,710
Sir, step this way please.

165
00:28:48,728 --> 00:28:51,398
Thank you for agreeing to come.

166
00:28:52,691 --> 00:28:56,402
I apologize for bringing
you here like this.

167
00:28:56,403 --> 00:28:58,863
What can I do for you?

168
00:29:00,990 --> 00:29:06,120
What a shame a good
samurai like you isn't employed!

169
00:29:06,121 --> 00:29:09,457
I can tell that
you're a good samurai.

170
00:29:11,084 --> 00:29:15,463
I'd like to propose that you
become part of the Ito family.

171
00:29:18,174 --> 00:29:23,012
I wonder if you'd agree to be
Ume's husband.

172
00:29:29,853 --> 00:29:35,607
But I have a wife and a child.

173
00:29:35,608 --> 00:29:41,113
Besides Ms. Ume appears
to be quite young.

174
00:29:41,114 --> 00:29:42,782
She's sixteen.

175
00:29:46,161 --> 00:29:49,789
With clue respect I must decline
your proposition.

176
00:29:51,374 --> 00:29:56,504
I find your honesty
very gallant.

177
00:29:57,714 --> 00:30:02,384
Ume is certainly
childish at times

178
00:30:02,385 --> 00:30:06,055
but at 16 she's mature enough
to be a woman.

179
00:30:08,725 --> 00:30:12,561
<i>She saw you one day
when she was in town</i>

180
00:30:12,562 --> 00:30:17,107
<i>and she can't shake you
from her mind.</i>

181
00:30:17,108 --> 00:30:20,027
<i>She lost her parents
at a young age.</i>

182
00:30:20,028 --> 00:30:25,949
<i>She's so dear to me I'd
do anything for her.</i>

183
00:30:25,950 --> 00:30:29,828
<i>But Ume has never asked
anything of me.</i>

184
00:30:29,829 --> 00:30:32,081
Until now.

185
00:30:32,957 --> 00:30:36,211
Until she fell in love.

186
00:30:39,255 --> 00:30:40,381
Love...

187
00:30:41,049 --> 00:30:42,091
Yes.

188
00:30:46,763 --> 00:30:49,681
I know your position,

189
00:30:49,682 --> 00:30:53,436
but please consider my offer.

190
00:31:28,429 --> 00:31:29,430
<i>Mr. Tamiya.</i>

191
00:31:30,640 --> 00:31:35,103
<i>You deserve a life
worthy of a samurai.</i>

192
00:31:38,022 --> 00:31:40,816
If you marry my Ume

193
00:31:40,817 --> 00:31:44,778
it'll be easier to
find employment.

194
00:31:44,779 --> 00:31:47,699
Your wife will understand.

195
00:31:49,242 --> 00:31:55,289
A samurai's wife would
encourage her husband's career.

196
00:31:55,290 --> 00:31:59,210
<i>There's nothing to worry about.</i>

197
00:32:01,129 --> 00:32:05,216
<i>We've figured out a plan</i>

198
00:32:06,134 --> 00:32:09,429
to make things go smoothly.

199
00:32:11,764 --> 00:32:14,017
Let us handle it.

200
00:32:53,097 --> 00:32:54,474
<i>May I come in?</i>

201
00:33:03,191 --> 00:33:07,111
Miyuki, I'm clone for the day.

202
00:33:08,738 --> 00:33:10,740
I'll see you tomorrow.

203
00:33:16,871 --> 00:33:18,206
See you.

204
00:33:21,292 --> 00:33:22,502
Miyuki.

205
00:33:24,295 --> 00:33:28,424
I'd love to have a talk with you
one clay.

206
00:33:30,134 --> 00:33:34,681
I got into acting because I
was inspired by you.

207
00:33:39,769 --> 00:33:40,937
Oh.

208
00:33:45,108 --> 00:33:49,362
You're quite gutsy, aren't you?

209
00:33:54,117 --> 00:33:55,702
Thank you.

210
00:34:00,039 --> 00:34:01,457
I'll be going.

211
00:34:16,389 --> 00:34:17,682
See you.

212
00:34:19,559 --> 00:34:21,185
See you tomorrow.

213
00:34:34,824 --> 00:34:36,367
It's OK now.

214
00:34:49,797 --> 00:34:52,717
<i>Miyuki, are you in here?</i>

215
00:34:54,761 --> 00:34:57,138
The car's here.

216
00:36:39,949 --> 00:36:42,076
"Miyuki calling."

217
00:39:07,596 --> 00:39:08,889
What's wrong?

218
00:39:54,685 --> 00:39:55,853
Hi.

219
00:39:58,189 --> 00:40:00,691
I just came here for
some shirts.

220
00:40:01,275 --> 00:40:02,275
<i>Oh.</i>

221
00:40:02,360 --> 00:40:04,362
You want to shower?

222
00:40:06,572 --> 00:40:08,074
Why so nice?

223
00:40:09,367 --> 00:40:11,327
Coz you dropped by, Kosuke.

224
00:40:13,788 --> 00:40:16,082
We have a day off next week.

225
00:40:17,333 --> 00:40:18,709
Got any plans?

226
00:40:20,044 --> 00:40:21,921
I'm going to Izu.

227
00:40:22,588 --> 00:40:23,588
Work?

228
00:40:24,882 --> 00:40:29,428
I was invited by some
producers I worked with once.

229
00:40:35,434 --> 00:40:38,938
As soon as I'm done
I'll come by.

230
00:40:40,940 --> 00:40:42,274
I'll be waiting.

231
00:40:44,944 --> 00:40:48,239
That's a lot of pasta...!

232
00:40:49,532 --> 00:40:50,908
<i>Want some?</i>

233
00:41:20,563 --> 00:41:23,607
"Home Pregnancy Test."

234
00:41:41,834 --> 00:41:45,171
"Negative."

235
00:42:04,607 --> 00:42:06,442
Welcome home.

236
00:42:13,115 --> 00:42:14,992
How do you feel?

237
00:42:18,245 --> 00:42:19,955
Iemon...

238
00:42:29,215 --> 00:42:31,050
I wish I was...

239
00:42:34,845 --> 00:42:36,639
It's not decided yet but.

240
00:42:37,890 --> 00:42:40,392
I might be offered
an official position.

241
00:42:43,270 --> 00:42:45,523
Is that true?

242
00:42:49,860 --> 00:42:52,071
If Father was here...

243
00:42:53,781 --> 00:42:56,408
he would have been happy!

244
00:43:21,308 --> 00:43:26,146
You can wear this when
you report for duty.

245
00:43:29,108 --> 00:43:34,612
It's our family crest,
a ying-yang insignia.

246
00:43:34,613 --> 00:43:37,283
Where there's light
there's shadow.

247
00:43:38,450 --> 00:43:40,869
Where there's shadow
there's light.

248
00:43:43,622 --> 00:43:45,416
All elements in the universe

249
00:43:46,625 --> 00:43:48,836
throughout eternity

250
00:43:49,795 --> 00:43:53,382
have a light and a shady side.

251
00:44:13,902 --> 00:44:15,237
What is it?

252
00:44:20,367 --> 00:44:21,910
There, there.

253
00:44:31,754 --> 00:44:36,258
Iemon, he's smiling
at his daddy!

254
00:44:39,678 --> 00:44:41,263
Dear boy!

255
00:45:00,658 --> 00:45:05,579
Ms. Maki came all this way
to bring me a gift.

256
00:45:14,088 --> 00:45:16,131
Please thank her.

257
00:45:19,176 --> 00:45:24,889
Let me express my thanks
for my husband's good fortune.

258
00:45:24,890 --> 00:45:28,602
I appreciate
Master Ito's generosity.

259
00:45:30,479 --> 00:45:35,817
I hope he'll continue
to support Iemon's career.

260
00:45:35,818 --> 00:45:37,985
Thank you so much.

261
00:45:37,986 --> 00:45:39,863
I'll pass it on.

262
00:45:40,698 --> 00:45:42,865
It appears that

263
00:45:42,866 --> 00:45:48,705
your husband is preoccupied
with your health.

264
00:45:48,706 --> 00:45:52,917
So Master Ito acquired
some medicine abroad and

265
00:45:52,918 --> 00:45:56,587
he wanted to get it to you soon.

266
00:45:56,588 --> 00:46:00,092
Have one packet a day.

267
00:46:03,762 --> 00:46:07,640
I don't deserve
such expensive medicine...

268
00:46:07,641 --> 00:46:11,144
Not to worry.
It's for your husband.

269
00:46:11,145 --> 00:46:15,398
We hope he'll perform to the
best of his ability on the job.

270
00:46:15,399 --> 00:46:20,696
And that depends on
your good health.

271
00:46:25,826 --> 00:46:29,204
Thank you very much.

272
00:46:30,664 --> 00:46:36,253
Iwa, we have no reason
to reject their generosity.

273
00:46:44,136 --> 00:46:46,972
I don't know how to thank you.

274
00:46:48,807 --> 00:46:52,644
Look at that darling baby!

275
00:47:36,188 --> 00:47:38,481
What did you give her?

276
00:47:38,482 --> 00:47:42,986
Don't worry, it won't kill her.

277
00:47:45,489 --> 00:47:46,615
But...

278
00:47:52,496 --> 00:47:54,248
Gentlemen usually

279
00:47:55,499 --> 00:47:58,752
despise things that are ugly.

280
00:48:13,517 --> 00:48:15,227
<i>Are you in pain?</i>

281
00:48:16,061 --> 00:48:18,856
There, there.

282
00:48:25,153 --> 00:48:27,697
Does it still hurt?

283
00:48:27,698 --> 00:48:29,575
There, there...

284
00:48:37,165 --> 00:48:39,001
<i>Welcome home.</i>

285
00:48:46,800 --> 00:48:51,597
I nursed Ichinosuke
a while ago but

286
00:48:52,973 --> 00:48:54,933
he got sick and threw up.

287
00:48:56,310 --> 00:48:59,187
I wonder if it's the medicine...

288
00:49:00,439 --> 00:49:05,319
Are you saying that it's poison?

289
00:49:06,403 --> 00:49:08,280
No, I wouldn't!

290
00:49:09,907 --> 00:49:15,662
I've taken the medicine
for 2 days now but...

291
00:49:17,539 --> 00:49:18,665
What?

292
00:49:21,835 --> 00:49:26,548
I guess it'll start
working soon.

293
00:49:29,009 --> 00:49:31,136
It was so expensive!

294
00:49:31,929 --> 00:49:34,556
You have to finish it or

295
00:49:36,308 --> 00:49:38,936
it'd be disrespectful
to Master Ito.

296
00:49:44,358 --> 00:49:50,738
It is generous of you to
be so concerned for my health.

297
00:49:50,739 --> 00:49:54,742
You troubled yourself
to get me medicine.

298
00:49:54,743 --> 00:49:56,954
I'm so fortunate.

299
00:50:00,040 --> 00:50:01,667
Tell Master Ito

300
00:50:02,751 --> 00:50:06,588
how grateful I am
for his kindness.

301
00:51:26,752 --> 00:51:30,422
<i>That was the last packet.</i>

302
00:52:15,133 --> 00:52:18,136
<i>Thanks, everyone.
Let's finish for the day!</i>

303
00:52:19,930 --> 00:52:22,306
<i>We'll have a clay off tomorrow.</i>

304
00:52:22,307 --> 00:52:27,062
<i>And we'll resume rehearsals
at 2 pm in two days.</i>

305
00:52:43,078 --> 00:52:44,371
Kayo.

306
00:52:46,164 --> 00:52:51,419
Kosuke will stay over tonight so
I won't need a ride tomorrow.

307
00:52:51,628 --> 00:52:52,628
OK.

308
00:52:53,171 --> 00:52:56,924
If there's any change
let me know.

309
00:52:56,925 --> 00:53:00,594
In that case I'll call you.

310
00:53:00,595 --> 00:53:01,596
Good.

311
00:53:57,694 --> 00:54:01,323
<i>Sorry, I'm still in Izu.</i>

312
00:54:04,117 --> 00:54:05,202
Oh.

313
00:54:06,828 --> 00:54:08,580
It's not your fault.

314
00:54:14,753 --> 00:54:15,962
<i>Bye.</i>

315
00:54:19,925 --> 00:54:21,301
See you...

316
00:54:46,576 --> 00:54:49,204
<i>Me or her...?</i>

317
00:54:49,996 --> 00:54:51,373
<i>I love you.</i>

318
00:54:52,666 --> 00:54:54,334
If it's true

319
00:54:55,252 --> 00:54:57,003
marry me.

320
00:54:59,089 --> 00:55:03,801
We'll get married and
move to America.

321
00:55:03,802 --> 00:55:07,888
There's no reason for you
to work as an actor.

322
00:55:07,889 --> 00:55:11,893
You can help Dad
with his business.

323
00:55:13,270 --> 00:55:17,898
He owns many restaurants
and buildings.

324
00:55:17,899 --> 00:55:20,110
Acting is all I've known

325
00:55:20,986 --> 00:55:23,280
since childhood.

326
00:55:28,994 --> 00:55:30,495
I won't...

327
00:55:33,081 --> 00:55:35,583
I won't let Miyuki
have you back.

328
00:57:22,899 --> 00:57:24,484
About Iemon...

329
00:57:29,656 --> 00:57:32,283
What would have made him happy?

330
00:57:34,119 --> 00:57:36,496
<i>Nothing.</i>

331
00:57:37,163 --> 00:57:38,373
<i>See you tomorrow.</i>

332
00:57:54,848 --> 00:57:56,640
Hey, you.

333
00:57:56,641 --> 00:58:00,437
I'll have to ask you
to come a little sooner.

334
00:58:05,233 --> 00:58:06,818
Sorry.

335
00:58:09,154 --> 00:58:11,448
It's his fault.

336
00:58:15,702 --> 00:58:17,662
Uh...

337
00:58:20,707 --> 00:58:22,667
Uh...

338
00:58:51,362 --> 00:58:53,323
Uh...

339
01:02:15,608 --> 01:02:16,859
Miyuki!

340
01:02:32,083 --> 01:02:34,085
What's going on...?

341
01:02:40,550 --> 01:02:41,718
Miyuki?

342
01:02:51,144 --> 01:02:52,478
Hi, there.

343
01:03:09,829 --> 01:03:12,123
I'm not worthy of you...

344
01:03:13,291 --> 01:03:16,210
I'm so sorry.

345
01:03:17,837 --> 01:03:19,088
<i>What?</i>

346
01:03:20,381 --> 01:03:22,967
I'm not worthy of you...

347
01:03:23,885 --> 01:03:26,888
I'm so sorry.

348
01:03:32,226 --> 01:03:33,770
What are you saying?

349
01:03:41,194 --> 01:03:42,653
Let's sleep.

350
01:03:44,906 --> 01:03:46,157
You need a doctor.

351
01:03:49,786 --> 01:03:51,037
Let's go.

352
01:03:56,751 --> 01:03:58,628
I can sleep it off.

353
01:04:03,424 --> 01:04:04,967
No, you can't...

354
01:04:09,180 --> 01:04:10,305
We're going.

355
01:04:10,306 --> 01:04:12,683
I can't find our baby!

356
01:04:14,393 --> 01:04:17,021
I know he's in there.

357
01:04:18,981 --> 01:04:24,362
I looked all over but
I couldn't find him.

358
01:04:27,114 --> 01:04:28,114
What...?

359
01:04:29,659 --> 01:04:33,246
You know how important this play
is for us.

360
01:04:34,831 --> 01:04:37,834
We need some sleep.

361
01:04:47,885 --> 01:04:49,220
Or do you want to

362
01:04:50,096 --> 01:04:51,388
make love?

363
01:04:51,389 --> 01:04:52,431
That's enough!

364
01:05:01,732 --> 01:05:04,235
I'm not worthy of you...

365
01:05:06,070 --> 01:05:08,113
I'm so sorry.

366
01:05:08,114 --> 01:05:09,156
Go away...

367
01:05:11,117 --> 01:05:13,869
I'm not worthy of you...

368
01:05:13,870 --> 01:05:16,162
No, it's OK...

369
01:05:16,163 --> 01:05:18,541
I'm so sorry.

370
01:05:20,126 --> 01:05:23,587
I'm not worthy of you...

371
01:05:23,588 --> 01:05:25,505
Stay away!

372
01:05:25,506 --> 01:05:27,967
I'm not worthy of you...

373
01:05:28,843 --> 01:05:29,844
I'm so sorry.

374
01:06:46,671 --> 01:06:48,172
Hello.

375
01:06:49,799 --> 01:06:51,884
<i>You're early today.</i>

376
01:07:02,144 --> 01:07:04,354
<i>Listen, everyone.</i>

377
01:07:04,355 --> 01:07:10,527
We can't find Miyuki
so we'll use an understudy.

378
01:07:10,528 --> 01:07:13,030
Let's get ready, everyone.

379
01:07:14,198 --> 01:07:16,032
Fill in for her.

380
01:07:16,033 --> 01:07:17,535
Very well.

381
01:07:24,875 --> 01:07:26,377
Thank you.

382
01:07:32,925 --> 01:07:36,387
"Miyuki Goto - Out."

383
01:07:37,680 --> 01:07:40,682
When I'm with you

384
01:07:40,683 --> 01:07:43,644
time passes in the
blink of an eye.

385
01:07:46,439 --> 01:07:48,441
Leaving so soon?

386
01:07:51,235 --> 01:07:54,571
If I'm not home before Father

387
01:07:54,572 --> 01:07:58,200
he'll never let me
leave the house.

388
01:08:03,497 --> 01:08:07,460
If you had a comb like this...

389
01:08:09,962 --> 01:08:12,423
I'd always be with you, Iwa.

390
01:08:48,209 --> 01:08:50,419
It hurts...

391
01:08:53,172 --> 01:08:54,924
Oh, God...

392
01:09:02,098 --> 01:09:03,974
It hurts.

393
01:09:10,648 --> 01:09:12,858
It hurts.

394
01:11:09,308 --> 01:11:11,143
Good evening.

395
01:11:13,687 --> 01:11:16,190
<i>It's Takuetsu, the masseur.</i>

396
01:11:19,610 --> 01:11:21,237
<i>Ms. Iwa.</i>

397
01:11:23,197 --> 01:11:27,243
<i>How are you tonight?</i>

398
01:11:32,122 --> 01:11:34,708
It's kind of you to visit but

399
01:11:35,876 --> 01:11:39,421
I'd prefer to be alone
this evening.

400
01:11:42,049 --> 01:11:47,304
Iemon will be home soon
and he'll want his supper.

401
01:11:48,722 --> 01:11:50,766
Mr. Iemon?

402
01:11:52,518 --> 01:11:57,106
I wonder if he'll
come home tonight.

403
01:12:09,493 --> 01:12:10,953
Takuetsu...?

404
01:12:16,583 --> 01:12:18,252
Ms. Iwa!

405
01:12:20,546 --> 01:12:22,589
How dare you! Stop it!

406
01:12:25,467 --> 01:12:29,345
I've desired you, Ms. Iwa

407
01:12:29,346 --> 01:12:31,639
for a long time.

408
01:12:31,640 --> 01:12:33,183
Let me go!

409
01:12:48,073 --> 01:12:51,659
Beautiful Ms. Iwa...

410
01:12:51,660 --> 01:12:53,662
I said let go!

411
01:12:56,040 --> 01:12:58,000
Forget your husband.

412
01:12:59,251 --> 01:13:00,753
You see, Mr. Iemon...

413
01:13:01,795 --> 01:13:05,423
He knows about this.

414
01:13:05,424 --> 01:13:08,093
He said I can have you.

415
01:13:14,892 --> 01:13:18,270
I guess Iemon wanted
a better life.

416
01:13:19,521 --> 01:13:21,398
Samurai can be...

417
01:13:24,610 --> 01:13:27,321
<i>quite heartless.</i>

418
01:13:29,698 --> 01:13:32,325
<i>For a better life</i>

419
01:13:32,326 --> 01:13:38,290
<i>he sacrificed you and the baby.</i>

420
01:13:40,125 --> 01:13:42,293
He'll live in a mansion

421
01:13:42,294 --> 01:13:47,883
with a pretty, young girl
as his wife.

422
01:13:53,472 --> 01:13:54,723
Ah..

423
01:14:00,312 --> 01:14:01,814
Ms. Iwa...

424
01:14:03,190 --> 01:14:04,608
Ms. Iwa!

425
01:14:07,528 --> 01:14:09,363
Ms. Iwa.

426
01:14:11,573 --> 01:14:13,242
Ms. Iwa...

427
01:14:50,112 --> 01:14:52,448
<i>Filthy animals.</i>

428
01:14:56,326 --> 01:14:58,787
I caught you fornicating.

429
01:15:14,928 --> 01:15:17,931
Sir, this isn't what
you promised!

430
01:15:51,590 --> 01:15:53,258
Iemon!

431
01:16:02,935 --> 01:16:04,269
Masseur.

432
01:16:04,937 --> 01:16:06,063
I saw you.

433
01:16:06,939 --> 01:16:11,360
You tricked a married woman
into a shameless affair.

434
01:16:14,696 --> 01:16:18,616
And you're a wife who
let herself be tricked.

435
01:16:18,617 --> 01:16:20,702
It's immoral.

436
01:16:21,620 --> 01:16:23,747
I had to punish you both.

437
01:20:08,722 --> 01:20:09,973
Iemon.

438
01:20:29,117 --> 01:20:32,120
You're mine and mine only.

439
01:20:43,048 --> 01:20:44,674
What's wrong?

440
01:22:28,028 --> 01:22:29,029
Ume.

441
01:22:55,889 --> 01:22:56,932
Iwa...

442
01:23:02,896 --> 01:23:05,564
You've done me wrong.

443
01:23:05,565 --> 01:23:07,609
Iemon.

444
01:23:09,235 --> 01:23:11,780
You demon!

445
01:23:18,078 --> 01:23:19,078
Ume!

446
01:23:19,788 --> 01:23:23,792
There's nothing to worry about.

447
01:23:28,713 --> 01:23:33,385
We figured out a plan

448
01:23:34,761 --> 01:23:38,723
to make things go smoothly.

449
01:25:21,201 --> 01:25:23,661
You're so undignified...

450
01:25:33,505 --> 01:25:37,842
I think you've already
been in hell, Iemon.

451
01:26:00,949 --> 01:26:02,909
For the pain

452
01:26:09,207 --> 01:26:11,626
you inflicted on me!

453
01:27:04,762 --> 01:27:06,723
What the hell...!

454
01:27:09,225 --> 01:27:11,935
There wasn't even a breeze.

455
01:27:11,936 --> 01:27:13,897
It flew from over there.

456
01:27:16,024 --> 01:27:18,318
OK, where is his head?

457
01:27:19,277 --> 01:27:20,612
Well sir...

458
01:27:21,321 --> 01:27:23,114
We can't find it.

459
01:27:25,200 --> 01:27:26,743
What do you mean?

460
01:27:34,834 --> 01:27:35,960
Thank you.

461
01:27:43,009 --> 01:27:46,845
"Kosuke Hasegawa - Out."

462
01:27:46,846 --> 01:27:49,224
<i>He's late again.</i>

463
01:27:53,853 --> 01:27:55,855
OK, that's good.

464
01:27:56,648 --> 01:27:58,649
Come downstairs.

465
01:27:58,650 --> 01:28:00,193
See you down there.

466
01:28:01,361 --> 01:28:02,528
<i>Miyuki.</i>

467
01:28:03,780 --> 01:28:07,282
<i>Kosuke isn't here.
Do you know why?</i>

468
01:28:07,283 --> 01:28:10,786
No. He worries me.

469
01:28:10,787 --> 01:28:12,121
Do you know anything?

470
01:28:12,956 --> 01:28:14,666
No, I don't...

471
01:28:27,804 --> 01:28:29,222
<i>This play...</i>

472
01:28:32,225 --> 01:28:35,687
Make sure it goes ahead
no matter what.

473
01:28:37,146 --> 01:28:38,438
<i>Director!</i>

474
01:28:38,439 --> 01:28:40,023
Is he here?

475
01:28:40,024 --> 01:28:41,025
No.

476
01:28:43,528 --> 01:28:44,612
<i>The police?</i>

477
01:28:51,953 --> 01:28:53,288
<i>Miyuki.</i>

478
01:28:58,126 --> 01:29:02,212
If we're done early tonight
how about...

479
01:29:02,213 --> 01:29:03,506
a drink?

480
01:29:08,136 --> 01:29:09,429
Well?

481
01:29:15,893 --> 01:29:18,021
If we're done early

482
01:29:19,480 --> 01:29:21,316
you should go home.

483
01:29:27,697 --> 01:29:29,949
Too bad!

484
01:30:22,418 --> 01:30:26,381
Ebizo ICHIKAWA.

485
01:30:27,423 --> 01:30:31,427
Ko SHIBASAKI.

486
01:30:32,470 --> 01:30:36,432
Hideaki ITO.

487
01:33:42,535 --> 01:33:47,456
Screenplay by Kikumi Yamagishi.

488
01:33:50,793 --> 01:33:55,756
Directed by Takashi Miike

