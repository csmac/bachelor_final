1
00:00:19,586 --> 00:00:21,986
Presented by SHOW EAST

2
00:00:25,492 --> 00:00:28,154
produced by
Dada Film & Beautiful Pictures

3
00:00:36,369 --> 00:00:38,803
SHIM Hey-jin

4
00:00:40,740 --> 00:00:43,368
KIM Jin-geun

5
00:00:45,311 --> 00:00:47,711
PARK Woong
LEE Young-hee

6
00:00:49,749 --> 00:00:52,684
MOON Woo-bin
JUNG Na-yoon

7
00:01:28,154 --> 00:01:30,418
executive producer
KIM Dong-joo

8
00:01:36,096 --> 00:01:38,656
associated producer
YU Young-shik, KANG Sung-kyu
PARK Ki-hyung

9
00:01:43,536 --> 00:01:45,731
produced by
YU Young-shik

10
00:01:49,943 --> 00:01:52,810
directed by
PARK Ki-hyung

11
00:02:03,890 --> 00:02:09,123
ACACIA

12
00:02:32,619 --> 00:02:33,813
Hello.

13
00:02:33,887 --> 00:02:35,047
Oh, hi.

14
00:02:35,121 --> 00:02:37,089
I bet you had a hard time
being a judge

15
00:02:37,157 --> 00:02:40,456
Seems like Professor Kim
made it harder for you.

16
00:02:40,527 --> 00:02:42,688
No, I enjoyed it.

17
00:02:47,000 --> 00:02:50,697
How come this painting
didn't get awarded?

18
00:02:50,770 --> 00:02:52,704
People talk about it

19
00:02:53,640 --> 00:02:57,804
Even if he copied it,
he's still very talented.

20
00:02:59,112 --> 00:03:01,171
I think he had help.

21
00:03:02,448 --> 00:03:05,212
But what if he had
really done it all alone?

22
00:03:08,588 --> 00:03:12,991
Then he's a genius,
and he'll be praised elsewhere.

23
00:03:14,594 --> 00:03:16,357
There's a booklet
of the drawings.

24
00:03:16,429 --> 00:03:18,556
I'll go get it,
so please wait here.

25
00:04:26,099 --> 00:04:27,066
Did you eat dinner?

26
00:04:27,133 --> 00:04:28,964
I ate with the students
after the awards ceremony.

27
00:04:29,035 --> 00:04:30,366
Where's dad?

28
00:04:30,436 --> 00:04:31,698
Working in his workroom.

29
00:04:31,771 --> 00:04:34,934
- Want some tea?
- Sure, I'll go change first.

30
00:04:40,780 --> 00:04:42,077
Why are you giving me
that look?

31
00:04:42,815 --> 00:04:44,009
What look are you
talking about?

32
00:04:45,351 --> 00:04:47,376
You've got something to say,
so tell me.

33
00:04:48,554 --> 00:04:50,613
No, I was just thinking
that you're so pretty

34
00:04:51,224 --> 00:04:52,851
Come on, tell me.

35
00:04:54,227 --> 00:04:55,854
If you promise me
that you're not gonna be mad

36
00:04:55,928 --> 00:04:58,123
Are you having an affair
or something like that?

37
00:04:58,998 --> 00:05:00,124
What?

38
00:05:00,199 --> 00:05:02,497
Fine, you're not
Don't get mad at me

39
00:05:02,568 --> 00:05:03,694
Just tell me.

40
00:05:03,770 --> 00:05:05,101
I won't get mad.

41
00:05:08,241 --> 00:05:09,435
What if we...

42
00:05:11,344 --> 00:05:13,039
No, never mind.

43
00:05:14,013 --> 00:05:15,310
What is it?

44
00:05:16,482 --> 00:05:17,414
Is it about a baby?

45
00:05:17,483 --> 00:05:19,314
Are you saying adoption?

46
00:05:21,688 --> 00:05:23,155
How'd you know?

47
00:05:23,823 --> 00:05:26,621
That's the only issue
you hesitate to bring up to me

48
00:05:26,693 --> 00:05:29,719
If you cheated on me,
you'd never bring it up

49
00:05:31,030 --> 00:05:33,191
I'm not saying
we should adopt a child

50
00:05:34,033 --> 00:05:35,864
I just want us to give
it a thought

51
00:05:36,636 --> 00:05:38,831
You know it's not
our own decision.

52
00:05:38,905 --> 00:05:41,305
Dad said he'll
leave it up to us.

53
00:05:43,509 --> 00:05:45,534
You've already
discussed it with him?

54
00:05:49,248 --> 00:05:52,342
It just came up during dinner.

55
00:05:52,418 --> 00:05:55,182
Frankly, I wanted to know
his opinion about it

56
00:05:57,957 --> 00:05:58,981
What's wrong?

57
00:05:59,058 --> 00:06:01,549
If you've decided already,
why'd you beat around the bush?

58
00:06:01,627 --> 00:06:02,525
You don't need to ask my opinion

59
00:06:02,595 --> 00:06:04,153
All you have to do
is bring the kid.

60
00:06:04,230 --> 00:06:05,663
Go right ahead.

61
00:06:05,732 --> 00:06:07,893
Why are you saying like that?

62
00:06:08,601 --> 00:06:09,727
I didn't say anything wrong

63
00:06:09,802 --> 00:06:11,133
My job in this family is
to raise a kid

64
00:06:11,204 --> 00:06:13,798
that you and your father
bring home

65
00:06:13,873 --> 00:06:15,568
And that makes me
a nanny of your heir

66
00:06:15,641 --> 00:06:16,608
Good for you

67
00:06:16,676 --> 00:06:18,769
You'll get somebody
to do your memorial service

68
00:06:31,591 --> 00:06:32,853
Take a seat

69
00:06:36,696 --> 00:06:38,596
Are you working on a new project?

70
00:06:38,664 --> 00:06:39,688
No

71
00:06:40,500 --> 00:06:42,661
I'm just going through
my old works

72
00:06:43,803 --> 00:06:47,068
They feel different now

73
00:06:48,141 --> 00:06:50,075
I guess I'm still
attatched to them

74
00:06:51,711 --> 00:06:52,905
I always am

75
00:06:53,813 --> 00:06:55,405
It's always refreshing and gratifying
if you look

76
00:06:55,982 --> 00:07:00,316
at it with affection,
whether it's yours or not

77
00:07:01,988 --> 00:07:06,482
All that matter is
how much you care about it,

78
00:07:06,559 --> 00:07:08,959
whether it's art work or person

79
00:07:14,267 --> 00:07:16,064
I believe in both of you

80
00:07:18,237 --> 00:07:25,200
You two will have more days
to spend with a child than me.

81
00:08:27,540 --> 00:08:28,871
Sorry to keep you waiting.

82
00:08:28,941 --> 00:08:30,272
Please sit down.

83
00:08:36,649 --> 00:08:39,709
I bet you have a art teacher here

84
00:08:39,785 --> 00:08:40,877
Pardon?

85
00:08:41,787 --> 00:08:45,848
No, we just let children draw
what they want

86
00:08:46,526 --> 00:08:48,926
You mean, that boy drew
that tree by himself?

87
00:08:49,695 --> 00:08:51,322
He loves trees so much,

88
00:08:51,397 --> 00:08:53,695
he always sits alone
and keeps drawing them

89
00:08:55,034 --> 00:08:57,161
Is this your first adoption
counseling?

90
00:08:57,236 --> 00:08:58,328
Yes

91
00:08:58,404 --> 00:08:59,803
Ma'am

92
00:09:02,208 --> 00:09:04,904
May I talk with him?

93
00:09:35,775 --> 00:09:37,072
What's this?

94
00:09:37,977 --> 00:09:41,037
There's a bug on a tree

95
00:10:22,521 --> 00:10:24,455
Isn't he too old for us?

96
00:10:26,492 --> 00:10:27,550
I feel strange

97
00:10:27,627 --> 00:10:29,959
It feels like that I've
been waiting for him

98
00:10:30,696 --> 00:10:32,493
Doesn't it count most?

99
00:11:48,074 --> 00:11:50,372
Jin-sung, you really likes trees

100
00:11:51,143 --> 00:11:52,633
Let mommy help you

101
00:12:24,744 --> 00:12:27,679
'LEE Jin-sung'

102
00:12:33,886 --> 00:12:36,582
Jin-sung, don't you remember
what I taught you

103
00:12:36,655 --> 00:12:38,350
What's your new family name now?

104
00:12:43,162 --> 00:12:44,823
Lee, Jin-sung

105
00:12:46,699 --> 00:12:49,793
How many times did I tell you
that it's "Kim" Jin-sung?

106
00:12:49,869 --> 00:12:52,770
You're Kim, Jin-sing, got it?

107
00:13:11,190 --> 00:13:12,248
Jin-sung.

108
00:13:12,324 --> 00:13:14,451
I'll knit you something nice,
so stand up.

109
00:13:17,263 --> 00:13:18,252
Good boy.

110
00:13:20,966 --> 00:13:22,433
Now raise your head.

111
00:14:30,502 --> 00:14:32,697
This tree asked those ants to come

112
00:14:37,376 --> 00:14:42,006
Those ants are protecting
this arcacia tree

113
00:14:44,550 --> 00:14:47,747
If there's any animals or insects
that harass the tree,

114
00:14:48,387 --> 00:14:51,686
they attack them and
chase them away

115
00:14:53,225 --> 00:14:54,715
This tree used to have fragrance

116
00:14:54,793 --> 00:14:58,422
and sometimes we got some honey

117
00:14:59,665 --> 00:15:01,724
For some reason, it's been years
since the last time

118
00:15:01,800 --> 00:15:04,291
it had leaves and flowers

119
00:15:04,370 --> 00:15:06,668
It's because
that thing is hurting it.

120
00:15:17,983 --> 00:15:21,475
Then let's make it feel better.

121
00:15:22,655 --> 00:15:26,591
Can you go get the red toolbox
from my workroom?

122
00:15:36,302 --> 00:15:38,429
It won't hurt anymore.

123
00:15:41,140 --> 00:15:43,768
You must be cold without leaves.

124
00:15:43,842 --> 00:15:46,709
I wish you had leaves.

125
00:15:46,779 --> 00:15:48,872
Does the tree understand you?

126
00:15:58,023 --> 00:15:59,581
What are you drawing?

127
00:16:15,140 --> 00:16:16,437
What's this?

128
00:16:17,710 --> 00:16:18,938
Daddy.

129
00:16:25,684 --> 00:16:27,151
Me?

130
00:16:37,296 --> 00:16:38,456
Jin-sung.

131
00:16:38,530 --> 00:16:39,963
Know what this is?

132
00:16:40,766 --> 00:16:44,133
I bought this for you
Come check it out.

133
00:16:56,215 --> 00:16:58,080
Rock, paper, scissors!

134
00:16:58,984 --> 00:17:01,316
Rock, paper, scissors!

135
00:17:04,523 --> 00:17:06,081
That's not fair!

136
00:17:07,426 --> 00:17:09,656
Rock, paper, scissors.

137
00:17:10,963 --> 00:17:12,328
I said rock.

138
00:17:13,065 --> 00:17:15,056
It feels like you're pinching me

139
00:18:10,789 --> 00:18:12,222
Say grandmother.

140
00:18:12,291 --> 00:18:15,226
Honey, I'm sure that
you can do it, right?

141
00:18:16,995 --> 00:18:21,523
His teacher said that
his IQ is very high for his age

142
00:18:26,538 --> 00:18:28,870
Why does he hold on
to that nasty bug?

143
00:18:30,275 --> 00:18:32,266
You know kids are kids

144
00:18:32,344 --> 00:18:34,835
I used to catch worms
and play with them.

145
00:18:34,913 --> 00:18:36,403
Don't you remember?

146
00:18:37,416 --> 00:18:39,247
I hardly remember

147
00:18:41,386 --> 00:18:43,354
Want to go to your room?

148
00:18:50,929 --> 00:18:52,294
He's cute, huh?

149
00:18:53,432 --> 00:18:55,491
Did you really put him
on the family register?

150
00:18:55,567 --> 00:18:58,229
Why do you keep asking me
same question?

151
00:19:07,846 --> 00:19:08,710
What's that?

152
00:19:08,780 --> 00:19:10,213
Hold on to it.

153
00:19:26,865 --> 00:19:28,958
I got it from
a famous fortune teller.

154
00:19:29,902 --> 00:19:31,301
Always carry it with you.

155
00:19:32,437 --> 00:19:36,840
I know a lot of women
who had kids over forty.

156
00:19:36,909 --> 00:19:39,002
Please stop.

157
00:19:42,214 --> 00:19:44,409
Don't come here
if you want to act like this

158
00:19:45,951 --> 00:19:48,215
You're too young
to give up already.

159
00:19:48,287 --> 00:19:52,155
You can't deny that
blood is thicker than water

160
00:21:29,288 --> 00:21:30,812
Jin-sung!

161
00:21:43,068 --> 00:21:44,592
Jin-sung!

162
00:21:48,140 --> 00:21:50,040
Jin-sung!

163
00:22:11,930 --> 00:22:13,557
Jin-sung, what's wrong?

164
00:22:14,966 --> 00:22:16,490
Did something happen?

165
00:22:19,104 --> 00:22:20,833
Where are you going?

166
00:22:30,349 --> 00:22:31,714
I can't believe it.

167
00:22:31,783 --> 00:22:34,251
How could she say that
in front of him?

168
00:23:37,916 --> 00:23:39,713
It's an Acacia leaf.

169
00:23:41,553 --> 00:23:44,147
Since the tree is sick and weak,

170
00:23:44,956 --> 00:23:47,117
hang it on the tree often.

171
00:23:47,893 --> 00:23:49,326
So that it won't be lonely.

172
00:25:54,152 --> 00:25:55,483
Who are they?

173
00:25:56,054 --> 00:25:57,612
Mommy and daddy.

174
00:26:05,163 --> 00:26:06,790
Where are you, Jin-sung?

175
00:26:08,166 --> 00:26:10,930
Kim Jin-sung is drawing this.

176
00:26:25,016 --> 00:26:26,415
I'm sorry I was mad at you

177
00:26:27,085 --> 00:26:28,643
Mommy was wrong.

178
00:26:43,501 --> 00:26:46,732
I'll hang this on you everyday
until you get well.

179
00:26:49,074 --> 00:26:51,770
I hope you get better soon.

180
00:27:28,279 --> 00:27:29,268
Hey!

181
00:27:29,347 --> 00:27:30,473
Yes?

182
00:27:31,383 --> 00:27:32,748
Wait right there.

183
00:27:52,537 --> 00:27:53,799
I'm Min-jee.

184
00:27:53,872 --> 00:27:55,464
You're Jin-sung, right?

185
00:27:57,075 --> 00:27:59,635
How'd you know my name?

186
00:28:01,179 --> 00:28:03,875
I can hear everything
from your porch.

187
00:28:05,316 --> 00:28:06,783
How old are you?

188
00:28:06,851 --> 00:28:08,478
Six.

189
00:28:15,160 --> 00:28:17,128
I'm older than you.

190
00:28:17,195 --> 00:28:18,787
I'm eight.

191
00:28:20,331 --> 00:28:22,663
You always talk to this tree.

192
00:28:22,734 --> 00:28:24,565
Does it talk back?

193
00:28:26,337 --> 00:28:27,804
Don't you go to school?

194
00:28:27,872 --> 00:28:29,339
Nope.

195
00:28:30,208 --> 00:28:31,436
Why not?

196
00:28:31,509 --> 00:28:33,500
Cause I don't have
enough blood

197
00:28:35,814 --> 00:28:37,839
Know what a vampire is?

198
00:28:41,519 --> 00:28:43,248
That's what I am.

199
00:28:43,321 --> 00:28:45,551
You better be careful.

200
00:28:56,668 --> 00:28:58,397
Can you ride that well?

201
00:29:43,348 --> 00:29:45,942
Jin-sung,
where are you going?

202
00:29:46,017 --> 00:29:47,780
Wait for me.

203
00:29:49,554 --> 00:29:52,990
I'll be with you from now on.

204
00:29:57,729 --> 00:30:00,289
You're now a vampire, too.

205
00:30:16,548 --> 00:30:17,572
What was her name?

206
00:30:17,649 --> 00:30:19,048
Min-jee?

207
00:30:20,351 --> 00:30:22,080
Looks like they're having fun

208
00:30:24,122 --> 00:30:25,589
That's great

209
00:30:52,383 --> 00:30:54,146
Hello.

210
00:31:04,762 --> 00:31:05,888
Hello.

211
00:31:05,964 --> 00:31:07,329
Congratulations.

212
00:31:07,398 --> 00:31:08,888
Mi-sook never expected it.

213
00:31:08,967 --> 00:31:10,400
She's kind of stunned

214
00:31:10,468 --> 00:31:12,834
What are you talking about?
It's a blessing

215
00:31:12,904 --> 00:31:15,236
She's supposed to be
happy about it

216
00:33:51,529 --> 00:33:53,121
Did you do this?

217
00:33:55,666 --> 00:33:57,725
To burn the fan?

218
00:34:24,328 --> 00:34:26,193
Just leave it alone.

219
00:34:27,698 --> 00:34:31,225
I'll clean this up after I change

220
00:35:10,908 --> 00:35:13,001
Jin-sung, don't worry.

221
00:35:13,644 --> 00:35:18,206
Having a new baby won't
change anything

222
00:35:19,283 --> 00:35:22,309
You know how much I love you

223
00:35:34,932 --> 00:35:36,399
You can't help your
maternal instinct

224
00:35:37,568 --> 00:35:40,594
You can't help falling in love
with your own baby

225
00:35:40,671 --> 00:35:42,366
Besides, you have a baby boy

226
00:35:42,440 --> 00:35:43,964
Mother, please.

227
00:35:44,041 --> 00:35:47,568
My sweet baby,
it's time for your formular

228
00:35:49,847 --> 00:35:51,439
Let's take a picture
of Hae-sung.

229
00:35:51,516 --> 00:35:52,983
Mother, look this way.

230
00:35:53,050 --> 00:35:54,244
Hae-sung, it's daddy.

231
00:35:54,318 --> 00:35:55,580
Hae-sung!

232
00:35:56,721 --> 00:35:57,483
Good.

233
00:35:57,555 --> 00:35:59,216
Here I go.

234
00:35:59,290 --> 00:36:01,155
One, two...

235
00:36:01,893 --> 00:36:05,420
Hae-sung, look at daddy.

236
00:36:06,831 --> 00:36:07,855
Over here.

237
00:36:07,932 --> 00:36:12,028
One, two...

238
00:36:13,204 --> 00:36:15,695
Why don't you
sit down with Hae-sung?

239
00:36:15,773 --> 00:36:17,468
Okay, good.

240
00:36:17,542 --> 00:36:19,066
One, two...

241
00:36:21,245 --> 00:36:22,507
My boy Jin-sung.

242
00:36:22,580 --> 00:36:24,172
Let's take one of you, too.

243
00:36:24,248 --> 00:36:30,653
Come on Jin-sung,
look at daddy.

244
00:36:30,721 --> 00:36:31,881
Jin-sung, good.

245
00:36:31,956 --> 00:36:34,618
Okay, one, two...

246
00:36:55,780 --> 00:36:57,645
Don't cry.

247
00:37:04,622 --> 00:37:06,180
Jin-sung, stop that!

248
00:37:07,391 --> 00:37:09,382
What a rotten boy.

249
00:37:09,460 --> 00:37:11,087
What's going on?

250
00:37:11,662 --> 00:37:13,323
He's terrible.

251
00:37:13,397 --> 00:37:16,025
I think he was just playing

252
00:37:16,100 --> 00:37:17,567
Playing?

253
00:37:17,635 --> 00:37:19,899
Covering a baby's mouth
is playing?

254
00:37:21,305 --> 00:37:22,966
Look at his eyes

255
00:37:23,040 --> 00:37:24,735
He got some evil look
in his eyes

256
00:37:24,809 --> 00:37:26,276
He's not a normal boy

257
00:37:26,344 --> 00:37:28,073
You witch!

258
00:37:28,145 --> 00:37:29,942
You'll be spanked
if you say like that

259
00:37:33,618 --> 00:37:34,949
Jin-sung!

260
00:37:35,653 --> 00:37:37,416
Jin-sung!

261
00:37:44,895 --> 00:37:46,294
Jin-sung!

262
00:37:57,375 --> 00:37:59,639
Jin-sung, what's wrong?

263
00:39:31,635 --> 00:39:32,727
Where's Jin-sung?

264
00:39:32,803 --> 00:39:34,270
Probably in his room.

265
00:39:35,673 --> 00:39:37,231
We shouldn't let him
near the baby

266
00:39:40,444 --> 00:39:41,741
Don't overreact

267
00:39:41,812 --> 00:39:43,279
He's only a kid.

268
00:39:46,617 --> 00:39:51,611
You can never be too
careful about it

269
00:40:00,598 --> 00:40:02,065
You're up.

270
00:40:08,406 --> 00:40:11,307
Let's see what Jin-sung drew.

271
00:40:15,579 --> 00:40:17,410
Is this new?

272
00:40:18,582 --> 00:40:20,550
Nice work.

273
00:40:29,193 --> 00:40:30,057
Hae-sung must be up.

274
00:40:30,127 --> 00:40:31,992
Can you go check on him?

275
00:40:32,062 --> 00:40:33,529
Okay.

276
00:40:34,064 --> 00:40:35,895
Jin-sung, go wash up.

277
00:41:12,369 --> 00:41:14,337
You're really not going to eat?

278
00:41:16,841 --> 00:41:18,365
Aren't you hungry?

279
00:41:20,044 --> 00:41:22,103
You should look at me
when I talk to you.

280
00:41:22,179 --> 00:41:23,737
I'm not hungry.

281
00:41:25,382 --> 00:41:26,542
Come down and talk.

282
00:41:26,617 --> 00:41:30,212
I can't even stay
where I want?

283
00:41:59,617 --> 00:42:01,107
Looks like it's gonna rain
You'll be all right?

284
00:42:01,185 --> 00:42:02,652
I'll be alright.

285
00:42:03,387 --> 00:42:05,548
I'll be late,
so don't wait for me.

286
00:42:07,358 --> 00:42:08,484
Have a good time.

287
00:42:08,559 --> 00:42:10,026
Have a good time.

288
00:42:21,338 --> 00:42:22,532
What's wrong?

289
00:42:23,173 --> 00:42:25,937
I better chop it down
before Jin-sung wakes up.

290
00:42:27,411 --> 00:42:30,073
He'll form bad habits
as long as there's the tree

291
00:42:30,881 --> 00:42:32,508
He's just young.

292
00:42:32,583 --> 00:42:34,551
Jin-sung likes it.

293
00:42:34,618 --> 00:42:36,449
Just leave it alone.

294
00:43:58,902 --> 00:44:02,633
Okay, I'll call the pastor
once I'm ready.

295
00:44:07,244 --> 00:44:09,178
Mom, stop saying that.

296
00:44:09,246 --> 00:44:11,578
You think it's simple
to send him back?

297
00:44:14,551 --> 00:44:15,643
Fine

298
00:44:16,286 --> 00:44:17,913
I said fine

299
00:44:35,639 --> 00:44:37,300
What are you doing?

300
00:44:37,374 --> 00:44:38,671
I'm leaving here

301
00:44:39,576 --> 00:44:40,702
What did you say?

302
00:44:41,278 --> 00:44:43,041
I'm going to my mom!

303
00:44:44,448 --> 00:44:45,938
Your mom's right here.

304
00:44:46,517 --> 00:44:47,848
My mom?

305
00:44:49,153 --> 00:44:50,552
You?

306
00:44:55,225 --> 00:44:57,216
My mom is dead

307
00:44:59,963 --> 00:45:02,158
She died and became a tree.

308
00:45:09,473 --> 00:45:11,873
You just go get
another kid!

309
00:45:19,049 --> 00:45:20,311
Jin-sung!

310
00:46:06,130 --> 00:46:06,562
Don't die.

311
00:46:17,474 --> 00:46:19,874
You just go get
another kid!

312
00:47:08,559 --> 00:47:10,390
Call the police.

313
00:47:16,033 --> 00:47:17,694
What am I supposed to tell them?

314
00:47:21,371 --> 00:47:22,895
Report that he's missing.

315
00:47:24,074 --> 00:47:26,474
We can't just wait like this.

316
00:47:57,841 --> 00:47:58,967
Is this the police?

317
00:48:01,245 --> 00:48:02,610
I'd like to report
a missing person.

318
00:48:05,883 --> 00:48:07,316
It's my son.

319
00:48:07,384 --> 00:48:09,818
He rode off in
his bike last night.

320
00:48:12,256 --> 00:48:13,382
Kim, Jin-sung

321
00:48:15,792 --> 00:48:17,123
Jin-sung.

322
00:48:22,699 --> 00:48:26,567
He did something wrong,
and my wife yelled at him.

323
00:48:26,637 --> 00:48:28,730
But he hasn't come back.

324
00:48:32,542 --> 00:48:33,770
Yes,

325
00:48:39,149 --> 00:48:41,276
he's adopted

326
00:50:46,309 --> 00:50:48,277
How did that get in there?

327
00:50:54,017 --> 00:50:55,917
What are you trying to do?

328
00:51:00,290 --> 00:51:02,417
Think I did that on purpose?

329
00:51:02,492 --> 00:51:04,221
Then was it me?

330
00:51:05,262 --> 00:51:06,820
Did Hae-sung do it?

331
00:51:07,831 --> 00:51:09,958
Or did Jin-sung
come back and do it?

332
00:51:10,801 --> 00:51:12,496
Don't bring him up

333
00:51:37,027 --> 00:51:39,393
Then, who would this belong to?

334
00:51:41,865 --> 00:51:45,494
Who else got this
kind of shit in this house?

335
00:51:52,542 --> 00:51:55,170
Drop your ridiculous work.

336
00:51:57,013 --> 00:51:59,777
And pay attention to the kid.

337
00:52:28,578 --> 00:52:29,806
Min-jee.

338
00:52:38,221 --> 00:52:40,018
Did something happen to
next door?

339
00:52:50,667 --> 00:52:52,760
Min-jee, what's wrong?

340
00:54:10,680 --> 00:54:11,977
Hi, Hae-sung.

341
00:54:14,951 --> 00:54:16,680
Can I push it?

342
00:54:18,054 --> 00:54:19,043
Okay.

343
00:54:19,789 --> 00:54:21,279
But don't go outside.

344
00:54:24,661 --> 00:54:25,889
Who's she?

345
00:54:26,663 --> 00:54:28,028
She lives next door.

346
00:54:28,732 --> 00:54:30,461
She used to play with Jin-sung

347
00:54:31,701 --> 00:54:33,100
Hae-sung likes her.

348
00:54:35,872 --> 00:54:38,363
I was worried,
but I'm glad you look better.

349
00:56:08,698 --> 00:56:10,529
Watch your driving!

350
00:56:11,301 --> 00:56:14,168
Hey lady,
I said 'I'm sorry'

351
00:56:14,237 --> 00:56:15,363
You think that's all?

352
00:56:15,438 --> 00:56:16,996
You scared my child to death.

353
00:56:18,475 --> 00:56:20,340
Fuck, did I hit him?

354
00:56:20,410 --> 00:56:22,537
Did I hit him, you bitch?

355
00:56:23,113 --> 00:56:26,207
What kind of maniac are you?

356
00:56:26,916 --> 00:56:27,405
Crazy bastard.

357
00:56:27,484 --> 00:56:30,078
Did you call me a bastard?
You wanna fuck with me?

358
00:59:19,923 --> 00:59:21,356
Where have you been?

359
00:59:21,424 --> 00:59:23,654
I was so worried about you.

360
00:59:24,561 --> 00:59:26,051
You must be starved

361
00:59:26,129 --> 00:59:28,563
You should eat first
and let's talk later

362
00:59:50,219 --> 00:59:51,277
It hurts.

363
01:00:07,670 --> 01:00:09,194
Stop!

364
01:00:38,568 --> 01:00:40,661
Someone must've made him believe

365
01:00:42,105 --> 01:00:45,438
that his mother became a tree
after she died on a rainy day.

366
01:00:46,175 --> 01:00:51,203
So whenever it rained he'd
often run out of our orphanage.

367
01:02:36,285 --> 01:02:37,650
Jin-sung.

368
01:02:46,395 --> 01:02:48,056
Jin-sung.

369
01:02:59,342 --> 01:03:01,572
Jin-sung.

370
01:03:20,530 --> 01:03:22,088
It's so weird

371
01:03:22,799 --> 01:03:26,633
It never had a leaf
till last year.

372
01:04:10,313 --> 01:04:11,371
Mom!

373
01:04:11,447 --> 01:04:12,937
Mom, what's wrong?

374
01:04:13,015 --> 01:04:14,107
Mom!

375
01:04:56,959 --> 01:04:58,119
Mom.

376
01:05:01,998 --> 01:05:03,966
Jin-sung, you see.

377
01:05:06,435 --> 01:05:08,096
It's not that
he won't come back.

378
01:05:10,106 --> 01:05:11,971
Maybe he can't come back.

379
01:05:14,677 --> 01:05:16,110
Maybe...

380
01:05:17,713 --> 01:05:19,237
Maybe someone...

381
01:05:29,025 --> 01:05:30,287
Are you okay?

382
01:05:35,765 --> 01:05:38,325
Don't you need to be in the hospital?

383
01:05:38,401 --> 01:05:39,868
That's okay

384
01:05:41,604 --> 01:05:44,596
How's your mother doing?

385
01:05:44,674 --> 01:05:48,075
The hospital will call me
if she regains her consciousness

386
01:05:50,046 --> 01:05:51,843
I'll be out for a while.

387
01:05:52,915 --> 01:05:54,815
Lock the doors.

388
01:05:56,886 --> 01:05:58,820
And go get some rest.

389
01:05:58,888 --> 01:06:01,049
You look very tired.

390
01:08:44,353 --> 01:08:47,379
You know where Jin-sung is,
don't you?

391
01:08:49,959 --> 01:08:51,586
Where is he?

392
01:08:52,294 --> 01:08:54,319
Why do you ask me that?
You know it already

393
01:13:42,117 --> 01:13:43,311
You're crazy.

394
01:13:45,487 --> 01:13:47,421
Watch your language, you psycho

395
01:13:49,324 --> 01:13:51,451
It's you who's crazy.

396
01:13:52,394 --> 01:13:54,225
I miss Jin-sung.

397
01:13:54,296 --> 01:13:56,560
All I wish is for him
to come back.

398
01:14:09,411 --> 01:14:10,673
Crazy bitch.

399
01:14:11,747 --> 01:14:13,510
Don't you realize
that it's all your fault?

400
01:14:14,583 --> 01:14:16,813
That's bullshit!

401
01:14:21,957 --> 01:14:24,255
Clean that up
before I kill you.

402
01:15:06,001 --> 01:15:07,935
Don't touch Hae-sung ever!

403
01:15:10,372 --> 01:15:12,033
I smell blood on you.

404
01:15:12,708 --> 01:15:14,039
It's disgusting.

405
01:18:48,757 --> 01:18:49,985
Hello?

406
01:18:50,625 --> 01:18:52,559
Is Choi, Mi-sook there?

407
01:18:54,329 --> 01:18:55,660
This is she
Who's calling please?

408
01:18:57,199 --> 01:18:59,167
Pastor Jung from Hakdong Church.

409
01:18:59,234 --> 01:19:00,758
Oh, hello.

410
01:19:01,770 --> 01:19:03,101
How are you?

411
01:19:03,171 --> 01:19:05,139
The same as always.

412
01:19:05,707 --> 01:19:07,572
So how's your mother?

413
01:19:08,176 --> 01:19:09,541
She's in the same condition

414
01:19:10,212 --> 01:19:11,577
Anyway, how may I help you?

415
01:19:12,114 --> 01:19:17,108
The exhibition begins tomorrow,
and I haven't heard from you

416
01:19:17,686 --> 01:19:18,983
Exhibition?

417
01:19:19,054 --> 01:19:23,081
Yes, a fundraiser
to help remodel the church.

418
01:19:25,193 --> 01:19:28,060
I thought your mother
told you already.

419
01:19:28,130 --> 01:19:31,657
She said you'll call me
when you're ready

420
01:19:32,400 --> 01:19:36,166
Okay, I'll call the pastor
once I'm ready.

421
01:19:49,217 --> 01:19:53,153
Min-jee,
you know something, don't you?

422
01:19:56,091 --> 01:19:57,615
You know you do.

423
01:20:29,057 --> 01:20:30,615
Mr. Kim, Do-il?

424
01:20:33,028 --> 01:20:34,393
Who are you?

425
01:20:34,963 --> 01:20:36,396
I'm a police officer.

426
01:20:51,847 --> 01:20:53,144
What can I do for you?

427
01:20:54,749 --> 01:20:57,445
You know misses Park,
the principal of Hope Orphanage?

428
01:20:57,986 --> 01:21:00,011
She's my cousin.

429
01:21:01,890 --> 01:21:03,187
So?

430
01:21:03,258 --> 01:21:06,421
I was told that your adopted
son is missing

431
01:21:07,329 --> 01:21:08,455
Yes.

432
01:21:09,064 --> 01:21:11,259
We already reported that
he's missing

433
01:21:11,333 --> 01:21:12,595
I'm aware of it

434
01:21:12,667 --> 01:21:15,431
But, it's hard to reach you

435
01:21:17,105 --> 01:21:18,265
Listen,

436
01:21:24,379 --> 01:21:27,143
what are you trying to say here?

437
01:21:30,518 --> 01:21:36,479
You know what's been happening
since he's gone?

438
01:21:39,928 --> 01:21:41,896
Go see for yourself.

439
01:21:44,032 --> 01:21:48,401
You should see my wife first
before you talk to me

440
01:21:49,938 --> 01:21:52,930
I already did

441
01:21:54,376 --> 01:21:58,403
She said that you got something
to say about his disappearance

442
01:25:53,548 --> 01:25:55,539
Jin-sung told me to kill you.

443
01:26:01,556 --> 01:26:05,583
You're gonna kill Hae-sung as well,
aren't you?

444
01:26:08,730 --> 01:26:10,061
No way!

445
01:26:10,765 --> 01:26:12,062
You can't.

446
01:26:13,168 --> 01:26:14,635
I know everything now.

447
01:26:24,913 --> 01:26:26,175
Crazy bitch.

448
01:26:27,315 --> 01:26:28,942
What the hell
are you talking about?

449
01:26:29,017 --> 01:26:30,848
You killed him and buried him!

450
01:26:30,919 --> 01:26:32,443
You killed him!

451
01:27:14,329 --> 01:27:16,126
I should've cut down the tree.

452
01:27:17,398 --> 01:27:19,332
I told you I was going
to chop it down.

453
01:27:21,836 --> 01:27:23,804
But you stopped me.

454
01:27:24,906 --> 01:27:26,396
You told me to leave it alone!

455
01:27:28,710 --> 01:27:30,041
What are you talking about?

456
01:27:30,645 --> 01:27:31,873
Don't you remember?

457
01:27:32,780 --> 01:27:34,839
You forgot everything
that happened that day?

458
01:27:39,320 --> 01:27:41,811
It's you who killed Jin-sung,
not me.

459
01:27:42,457 --> 01:27:43,947
Mi-sook, it's you!

460
01:27:45,760 --> 01:27:48,820
Jin-sung would be alive if
I had cut it down that day.

461
01:27:50,331 --> 01:27:53,596
If you hadn't stopped me,
Jin-sung would be alive.

462
01:27:59,707 --> 01:28:02,039
It's not true!

463
01:28:19,661 --> 01:28:21,629
You wanted to forget all
about it that much?

464
01:28:24,766 --> 01:28:27,428
Is this how you wipe
your memory out?

465
01:28:28,603 --> 01:28:31,128
Blaming everything on me?

466
01:29:13,748 --> 01:29:15,010
Jin-sung!

467
01:29:19,020 --> 01:29:20,180
This isn't your mother.

468
01:29:20,254 --> 01:29:20,948
It's just a tree.

469
01:29:21,022 --> 01:29:22,182
It's my mom

470
01:34:59,527 --> 01:35:01,324
It's all your fault!

471
01:35:03,731 --> 01:35:05,130
Stop it!

472
01:35:18,846 --> 01:35:20,177
Please stop.

473
01:35:21,115 --> 01:35:22,548
It's hurting Jin-sung.

474
01:35:27,421 --> 01:35:30,857
Jin-sung is not dead

475
01:35:30,925 --> 01:35:34,861
I can hear Jin-sung's voice
from the tree.

476
01:35:43,437 --> 01:35:45,997
No, it'll bleed!

477
01:35:46,774 --> 01:35:48,071
It's only a tree

478
01:35:48,142 --> 01:35:49,473
A tree.

479
01:35:49,543 --> 01:35:50,874
It's not your mother.

480
01:35:51,879 --> 01:35:53,210
Now, it's just me
you should talk to

481
01:35:53,848 --> 01:35:55,406
You can't go up there anymore.

482
01:35:55,483 --> 01:35:56,677
I'm going to chop it down.

483
01:35:56,751 --> 01:35:58,013
Got it?

484
01:35:58,085 --> 01:35:59,177
No.

485
01:36:00,855 --> 01:36:02,482
Don't do it.

486
01:37:04,852 --> 01:37:06,444
Mommy.

487
01:37:12,693 --> 01:37:14,684
Call the police.

488
01:37:17,798 --> 01:37:19,925
Report a missing person.

489
01:37:20,501 --> 01:37:23,197
We can't just wait like this.

490
01:38:08,916 --> 01:38:11,111
Jin-sung, I'm sleepy.

491
01:38:12,920 --> 01:38:16,083
I'm going to sleep.

