﻿1
00:00:01,535 --> 00:00:06,307
Ju-on: A curse born when a person dies
in a powerful rage.

2
00:00:06,474 --> 00:00:09,643
It gathers where the dead person lived,
and becomes a “stain”.

3
00:00:09,810 --> 00:00:14,281
Anyone who touches it dies,
and a new curse is born.

4
00:00:16,217 --> 00:00:20,554
TOEI VIDEO and CELL Present

5
00:00:20,721 --> 00:00:24,592
An OZ Production

6
00:00:28,662 --> 00:00:32,666
Produced by Taka lchise

7
00:00:33,134 --> 00:00:37,605
Co-Producer
Toshinori Nishimae

8
00:00:41,042 --> 00:00:44,879
Akina Minami

9
00:00:45,446 --> 00:00:49,450
Hiroki Suzuki

10
00:00:52,820 --> 00:00:56,490
Mihiro

11
00:00:57,024 --> 00:00:58,025
Aimi Nakamura

12
00:00:58,192 --> 00:00:59,293
Marika Fukunaga

13
00:00:59,460 --> 00:01:01,028
Chie Amemiya

14
00:01:01,595 --> 00:01:02,663
Akiko Hoshino

15
00:01:02,830 --> 00:01:03,931
Takuji Suzuki

16
00:01:04,098 --> 00:01:05,566
Tsuyoshi Muro

17
00:01:09,036 --> 00:01:13,040
lchirota Miyakawa

18
00:01:16,043 --> 00:01:20,181
Original JU-ON Concept Created by
Takashi Shimizu

19
00:01:20,581 --> 00:01:24,552
Written and Directed by
Ryuta Miyake

20
00:01:55,683 --> 00:01:57,184
JU-ON

21
00:01:57,351 --> 00:02:01,322
WHITE GHOST

22
00:02:04,859 --> 00:02:10,664
FUMIYA

23
00:02:46,534 --> 00:02:49,503
CAKE SHOP NICOLA

24
00:03:03,083 --> 00:03:06,453
ISOBE

25
00:03:22,069 --> 00:03:23,837
Hello.

26
00:04:03,978 --> 00:04:05,913
Anyone home?

27
00:04:28,202 --> 00:04:30,537
Delivery from Cake Shop Nicola.

28
00:04:30,704 --> 00:04:34,074
Your Christmas cake.

29
00:04:35,009 --> 00:04:37,111
I'm tied up right now.

30
00:04:37,278 --> 00:04:39,413
I'll be right there.

31
00:04:39,580 --> 00:04:41,048
Right.

32
00:05:04,538 --> 00:05:06,907
Uh, excuse me?

33
00:05:10,511 --> 00:05:13,080
The price for the family size is...

34
00:05:13,247 --> 00:05:15,182
I'm tied up right now.

35
00:05:15,349 --> 00:05:17,518
I'll be right there.

36
00:05:30,464 --> 00:05:32,466
I'm tied up right now.

37
00:05:32,633 --> 00:05:34,902
I'll be right there.

38
00:05:46,080 --> 00:05:48,048
Are you OK?

39
00:05:58,992 --> 00:06:00,894
I'm coming in.

40
00:06:05,199 --> 00:06:08,435
Uh, you OK?

41
00:07:28,115 --> 00:07:29,817
Hold on!

42
00:08:41,188 --> 00:08:43,257
I'm tied up right now.

43
00:08:43,423 --> 00:08:45,626
I'll be right there.

44
00:09:25,132 --> 00:09:30,771
KASHIWAGI

45
00:09:41,782 --> 00:09:44,084
Here we are, Miss.

46
00:09:46,653 --> 00:09:48,221
Miss!

47
00:09:49,022 --> 00:09:51,224
That'll be 12 million yen.

48
00:09:51,558 --> 00:09:53,393
Just joking!

49
00:09:55,228 --> 00:09:58,432
You don't want me to drop you
at the school gate?

50
00:09:58,599 --> 00:10:03,170
Getting a taxi to school is
asking to be bullied!

51
00:10:03,503 --> 00:10:06,573
Why can't a man take his daughter
to school?

52
00:10:06,740 --> 00:10:08,075
Today's cram school,

53
00:10:08,241 --> 00:10:10,210
so have dinner before me.

54
00:10:10,377 --> 00:10:12,546
Take a day off from cram school.

55
00:10:12,713 --> 00:10:14,414
Don't be silly.

56
00:10:14,581 --> 00:10:16,249
It's Christmas Eve.

57
00:10:16,416 --> 00:10:18,218
You can't keep Santa waiting.

58
00:10:18,385 --> 00:10:20,487
Santa doesn't exist.

59
00:10:20,654 --> 00:10:21,688
He does.

60
00:10:21,855 --> 00:10:23,390
Where?

61
00:10:24,191 --> 00:10:25,959
Here!

62
00:10:28,528 --> 00:10:29,630
Bye.

63
00:10:29,796 --> 00:10:31,465
Take care.

64
00:10:34,067 --> 00:10:35,836
What's this?

65
00:10:36,503 --> 00:10:39,339
There's something on my bag.

66
00:10:41,141 --> 00:10:45,445
Must have been that customer
this morning.

67
00:10:46,380 --> 00:10:47,280
Sorry!

68
00:10:47,447 --> 00:10:50,117
Yuck! it's disgusting!

69
00:10:50,283 --> 00:10:51,952
Sorry.

70
00:10:54,588 --> 00:10:56,089
Bye.

71
00:10:58,725 --> 00:11:00,227
Akane!

72
00:11:03,797 --> 00:11:06,266
Merry Christmas.

73
00:11:09,036 --> 00:11:10,771
What the hell is this?

74
00:11:15,842 --> 00:11:16,376
Sheesh!

75
00:11:16,543 --> 00:11:24,618
Message from dispatch center.

76
00:11:24,785 --> 00:11:29,222
Yes! Car number 12.

77
00:11:29,389 --> 00:11:31,925
Kashiwagi-san. Where are you?

78
00:11:32,092 --> 00:11:34,261
Uh, right now I'm...

79
00:11:34,594 --> 00:11:36,530
Hold on!

80
00:11:36,863 --> 00:11:39,599
Don't give me a booking now.

81
00:11:39,766 --> 00:11:42,836
I'm just about to go home.

82
00:11:43,003 --> 00:11:45,972
The police were just here.

83
00:11:46,139 --> 00:11:47,240
The police?

84
00:11:47,407 --> 00:11:50,277
They want to talk to you.

85
00:11:50,444 --> 00:11:51,645
About what?

86
00:11:51,812 --> 00:11:53,547
I don't know.

87
00:11:53,714 --> 00:11:58,018
Something about
a customer this morning.

88
00:11:58,852 --> 00:12:01,788
A customer this morning?

89
00:12:10,797 --> 00:12:12,699
Kashiwagi-san?

90
00:12:13,300 --> 00:12:15,035
Kashiwagi-san?

91
00:12:16,136 --> 00:12:22,542
OK. Tell them I'll be back
as soon as I can.

92
00:12:22,976 --> 00:12:25,479
Roger. Thanks.

93
00:13:08,722 --> 00:13:14,895
AKANE

94
00:13:18,231 --> 00:13:21,601
SAKURADAI HIGHSCHOOL

95
00:14:16,857 --> 00:14:18,425
Akane.

96
00:14:18,992 --> 00:14:21,228
What's wrong? You OK?

97
00:14:21,394 --> 00:14:23,163
I'm fine.

98
00:14:23,496 --> 00:14:25,131
Kashiwagi-san!

99
00:14:25,699 --> 00:14:27,734
We've got a favor to ask you.

100
00:14:53,960 --> 00:14:55,528
It's not moving.

101
00:14:55,695 --> 00:14:57,964
Don't take your finger off!

102
00:14:59,599 --> 00:15:03,737
Have you really got special powers?

103
00:15:03,904 --> 00:15:05,305
Special powers?

104
00:15:05,472 --> 00:15:06,907
Since you were small,

105
00:15:07,073 --> 00:15:10,844
you saw and heard things, right?

106
00:15:13,613 --> 00:15:15,982
What a let down.

107
00:15:16,149 --> 00:15:19,619
Maybe we're calling the wrong person?

108
00:15:19,786 --> 00:15:22,255
You're the one who suggested a celeb!

109
00:15:22,422 --> 00:15:24,925
But not Jun Matsumoto, not him!

110
00:15:25,091 --> 00:15:28,595
What about Okada then?

111
00:15:28,762 --> 00:15:31,498
He's still alive! Someone dead,

112
00:15:31,665 --> 00:15:33,600
a dead person!

113
00:15:35,802 --> 00:15:38,438
How about Kashiwagi-sari's dad?

114
00:15:39,472 --> 00:15:40,173
Mayumi!

115
00:15:40,340 --> 00:15:42,475
He passed away, right?

116
00:15:42,842 --> 00:15:44,678
He's just missing.

117
00:15:44,844 --> 00:15:48,214
Ls that right.

118
00:15:49,049 --> 00:15:50,483
Sorry.

119
00:15:51,584 --> 00:15:54,120
It was 7 years ago.

120
00:16:01,194 --> 00:16:02,996
Shall we give up?

121
00:16:03,496 --> 00:16:04,931
Yeah.

122
00:16:06,166 --> 00:16:08,435
Ouija board, Ouija board.

123
00:16:08,601 --> 00:16:10,203
Stop!

124
00:16:14,774 --> 00:16:16,543
I'm not doing anything!

125
00:16:23,550 --> 00:16:24,884
Mi.

126
00:16:28,888 --> 00:16:30,390
Ra.

127
00:16:37,697 --> 00:16:39,132


128
00:16:39,332 --> 00:16:40,934
Mirai?

129
00:16:41,301 --> 00:16:44,938
Like mirai, the future?

130
00:16:45,105 --> 00:16:47,640
Maybe a person's name?

131
00:16:54,314 --> 00:16:56,016
What is it, Akane?

132
00:17:01,721 --> 00:17:02,389
Akane!

133
00:17:02,555 --> 00:17:04,524
Kashiwagi-san!

134
00:17:16,336 --> 00:17:17,904
She won't pick up.

135
00:17:20,073 --> 00:17:22,275
Maybe she's mad at us.

136
00:17:22,442 --> 00:17:24,811
Huh? Why?

137
00:17:25,111 --> 00:17:27,814
Because we talked about her dad.

138
00:17:27,981 --> 00:17:30,483
She said it was OK.

139
00:17:33,420 --> 00:17:34,387
What?

140
00:17:34,554 --> 00:17:36,222
I forgot something.

141
00:17:36,389 --> 00:17:38,425
I'll just go and get it.

142
00:17:38,591 --> 00:17:41,928
Hurry up!

143
00:19:06,579 --> 00:19:11,217
ISOBE

144
00:19:23,496 --> 00:19:26,566
Please come in.

145
00:19:26,733 --> 00:19:29,669
Wow! it's great!

146
00:19:29,836 --> 00:19:30,803
Nice, eh?

147
00:19:30,970 --> 00:19:32,772
Well, it's not bad.

148
00:19:32,939 --> 00:19:34,841
Mom, the garden's huge!

149
00:19:35,008 --> 00:19:38,211
Granma, look, this is your new home.

150
00:19:38,378 --> 00:19:41,247
Shall we see the garden?

151
00:19:41,748 --> 00:19:45,919
Uncle, it's awesome. Hurry up!

152
00:19:46,085 --> 00:19:47,720
I'm coming.

153
00:20:03,002 --> 00:20:04,204
Where's the bathroom?

154
00:20:04,370 --> 00:20:06,239
Through here.

155
00:20:09,042 --> 00:20:10,510
Roomy enough?

156
00:20:10,677 --> 00:20:13,580
Yes, it's wonderful.

157
00:20:51,184 --> 00:20:54,454
Hey, don't you think it's too cheap?

158
00:20:56,856 --> 00:20:59,892
I think something happened here.

159
00:21:00,793 --> 00:21:02,562
Something?

160
00:21:02,996 --> 00:21:06,132
Maybe someone died here.

161
00:21:10,903 --> 00:21:13,706
You really have no sense of humor.

162
00:21:24,350 --> 00:21:26,486
I want this room!

163
00:21:26,819 --> 00:21:28,821
OK, it's yours.

164
00:21:41,134 --> 00:21:43,936
This is a good room for you, Atsushi.

165
00:21:44,504 --> 00:21:45,538
Right.

166
00:21:45,705 --> 00:21:46,439
It's quiet.

167
00:21:46,606 --> 00:21:48,908
You'll be able to study here.

168
00:21:50,243 --> 00:21:53,680
This time, you must pass
the legal exams.

169
00:21:59,452 --> 00:22:02,021
Atsushi will have the upstairs room.

170
00:22:02,188 --> 00:22:03,856
Right, dad?

171
00:22:04,023 --> 00:22:07,193
We just looked at it.

172
00:22:07,393 --> 00:22:08,695
It's really quiet.

173
00:22:09,696 --> 00:22:11,364
Granma.

174
00:22:11,531 --> 00:22:14,801
Voila! it's finished!

175
00:22:20,573 --> 00:22:23,176
Careful! You'll squash it!

176
00:23:58,404 --> 00:23:59,739
Uncle.

177
00:24:00,907 --> 00:24:02,708
What's wrong?

178
00:24:35,341 --> 00:24:41,347
CHIHO

179
00:24:56,829 --> 00:25:01,601
AMANUMA POLICE STATION

180
00:25:02,702 --> 00:25:04,604
I'll be right there.

181
00:25:04,937 --> 00:25:06,806
I'll be right there.

182
00:25:08,007 --> 00:25:10,343
I'll be right there.

183
00:25:20,620 --> 00:25:22,255
Are you OK?

184
00:25:29,061 --> 00:25:30,596
Fumiya!

185
00:25:32,231 --> 00:25:33,699
Chiho.

186
00:26:17,510 --> 00:26:18,844
Here.

187
00:26:20,713 --> 00:26:22,515
Merry Christmas.

188
00:26:30,156 --> 00:26:31,824
Well, open it!

189
00:26:48,474 --> 00:26:50,076
Cute, huh?

190
00:27:06,525 --> 00:27:08,127
Sorry.

191
00:27:10,830 --> 00:27:13,566
I know how excited you were
about tonight.

192
00:27:14,533 --> 00:27:18,371
We can go out to eat another time.

193
00:27:18,537 --> 00:27:21,240
Christmas at home isn't bad either.

194
00:27:23,376 --> 00:27:25,378
Blow out the candles!

195
00:27:32,652 --> 00:27:35,087
Merry Christmas!

196
00:27:35,254 --> 00:27:36,889
Merry Christmas.

197
00:27:37,189 --> 00:27:39,225
Let's eat!

198
00:28:14,960 --> 00:28:16,562
Are you OK?

199
00:28:50,629 --> 00:28:52,531
I should rinse.

200
00:28:54,533 --> 00:28:56,202
It's fine.

201
00:31:11,837 --> 00:31:18,244
MIRAI

202
00:31:38,264 --> 00:31:39,899
Akane?

203
00:32:01,253 --> 00:32:02,988
Akane.

204
00:32:04,890 --> 00:32:06,292
Thanks.

205
00:32:11,130 --> 00:32:13,966
Shouldn't you be in bed?

206
00:32:14,133 --> 00:32:17,569
I'm fine. The fever's gone too.

207
00:32:17,736 --> 00:32:20,706
All thanks to this.

208
00:32:21,473 --> 00:32:23,776
Sorry I kept it all this time.

209
00:32:23,943 --> 00:32:26,545
Give it back when you're better.

210
00:32:27,880 --> 00:32:30,049
But it really worked eh?

211
00:32:30,215 --> 00:32:35,287
Like magic! I can see why
it's your good-luck charm.

212
00:32:41,961 --> 00:32:43,796
Granma.

213
00:32:44,029 --> 00:32:46,632
That's Atsushi's basketball.

214
00:32:46,799 --> 00:32:49,068
He'll get mad again.

215
00:32:57,476 --> 00:32:59,478
Hello.

216
00:33:16,428 --> 00:33:17,930
Sorry about that.

217
00:33:20,332 --> 00:33:21,934
Are you OK?

218
00:33:22,634 --> 00:33:24,036
I should be going.

219
00:33:24,203 --> 00:33:27,239
It's OK. Stay a little longer.

220
00:33:27,940 --> 00:33:30,743
It's better if someone else is here.

221
00:33:30,909 --> 00:33:33,812
But your uncle's here.

222
00:33:34,146 --> 00:33:36,215
That's the problem.

223
00:33:48,460 --> 00:33:50,195
You're joking?

224
00:33:52,264 --> 00:33:55,434
Did you tell your mom?

225
00:33:56,869 --> 00:33:58,737
Why not?

226
00:33:58,904 --> 00:34:02,908
I can't. Because we live together.

227
00:34:04,043 --> 00:34:06,745
Was he always like that?

228
00:34:06,912 --> 00:34:11,483
He suddenly changed when we moved here.

229
00:34:13,452 --> 00:34:15,554
And...

230
00:34:16,755 --> 00:34:18,891
When I get out of the bath,

231
00:34:19,058 --> 00:34:22,428
my underwear is gone.

232
00:34:38,811 --> 00:34:41,780
It wasn't me. Granma did it.

233
00:34:55,594 --> 00:34:58,397
You should go now.

234
00:34:58,564 --> 00:35:00,399
But...

235
00:35:06,905 --> 00:35:10,008
Mirai, this house!

236
00:35:10,175 --> 00:35:13,345
Stop it, uncle.

237
00:35:13,612 --> 00:35:16,181
Stop it! Stop!

238
00:35:16,482 --> 00:35:21,887
Mirai!
Akane!

239
00:35:46,778 --> 00:36:01,093
Granma! Granma!

240
00:36:11,770 --> 00:36:13,105
Akane!

241
00:36:14,039 --> 00:36:22,214
Akane! Akane!

242
00:36:33,325 --> 00:36:35,027
Sit down.

243
00:36:49,775 --> 00:36:51,843
I'll help you with your homework.

244
00:36:52,211 --> 00:36:53,745
I'm OK.

245
00:36:55,214 --> 00:36:57,249
That's not true, is it?

246
00:37:23,642 --> 00:37:26,545
You're sweating.

247
00:38:03,482 --> 00:38:05,183
What the hell?

248
00:38:05,350 --> 00:38:07,386
What's wrong with you?

249
00:38:08,086 --> 00:38:13,292
Let go! What's up with you?

250
00:38:13,458 --> 00:38:15,060
Granma!

251
00:38:41,119 --> 00:38:47,025
YASUKAWA

252
00:38:50,529 --> 00:38:53,765
AMANUMA POLICE STATION

253
00:39:58,663 --> 00:40:01,333
You still here, Kawabata?

254
00:40:03,034 --> 00:40:04,202
Did you hear?

255
00:40:04,369 --> 00:40:07,339
Chikada's had another baby.

256
00:40:07,506 --> 00:40:10,041
He's pretty virile for his age.

257
00:40:12,744 --> 00:40:14,379
I'll be right there.

258
00:40:16,148 --> 00:40:18,183
I'll be right there.

259
00:40:20,285 --> 00:40:22,587
I'll be right there.

260
00:40:23,955 --> 00:40:28,326
I'll be right there.

261
00:40:28,493 --> 00:40:31,463
What are you listening to, Kawabata?

262
00:40:31,630 --> 00:40:34,766
I'll be right there.

263
00:40:34,933 --> 00:40:36,701
Kawabata?

264
00:40:43,175 --> 00:40:45,010
Yasukawa-san?

265
00:40:48,079 --> 00:40:50,949
The family massacre in Nishiogi?

266
00:40:51,116 --> 00:40:53,685
It was 7 years ago.

267
00:40:54,252 --> 00:40:57,923
What does it have to do with that tape?

268
00:40:59,691 --> 00:41:02,794
The killer committed suicide.

269
00:41:04,296 --> 00:41:06,364
After killing the whole family,

270
00:41:06,531 --> 00:41:08,800
he hanged himself.

271
00:41:12,070 --> 00:41:18,643
He recorded that tape as he died.

272
00:41:24,883 --> 00:41:30,856
One of the detectives...

273
00:41:31,089 --> 00:41:33,258
named Makabe...

274
00:41:33,825 --> 00:41:40,365
Worked overtime transcribing the tape.

275
00:41:44,469 --> 00:41:49,074
Next day, back at the station...

276
00:41:49,441 --> 00:41:52,277
Only the tape was there.
Makabe was gone.

277
00:41:58,116 --> 00:41:59,818
I called him many times,

278
00:41:59,985 --> 00:42:02,387
but he never answered.

279
00:42:02,754 --> 00:42:08,593
I had to go looking for him
at his home.

280
00:42:13,665 --> 00:42:16,234
Makabe was dead.

281
00:42:20,105 --> 00:42:22,641
Hanged himself in the bathroom.

282
00:42:24,843 --> 00:42:27,445
You think it was because of the tape?

283
00:42:30,348 --> 00:42:32,651
What was on the tape?

284
00:42:34,753 --> 00:42:36,421
Yasukawa-san.

285
00:42:38,523 --> 00:42:40,191
A voice.

286
00:42:41,593 --> 00:42:44,863
The same voice I heard?

287
00:42:45,030 --> 00:42:49,067
No, another voice. A child's.

288
00:42:54,205 --> 00:42:55,907
Where are you going?

289
00:43:00,946 --> 00:43:02,514
I'm going to burn it.

290
00:43:02,681 --> 00:43:04,716
Ls that necessary?

291
00:43:04,883 --> 00:43:07,419
I already threw it away once.

292
00:43:09,654 --> 00:43:14,292
I dumped it 7 years ago.

293
00:43:17,128 --> 00:43:20,632
On a mountain many miles away.

294
00:43:24,069 --> 00:43:27,472
There's no way it could be here.

295
00:43:31,643 --> 00:43:33,345
Don't get involved.

296
00:43:34,980 --> 00:43:37,015
Don't ever get involved.

297
00:43:51,329 --> 00:43:53,164
NISHIOGI FAMILY MASSACRE

298
00:43:53,331 --> 00:43:55,467
FAMILY OF MURDERED

299
00:43:55,634 --> 00:43:57,402
CRIME COMMITTED RD EVENING?

300
00:43:57,569 --> 00:43:59,704
ELDEST SON MISSING

301
00:44:02,440 --> 00:44:06,444
NO SIGN OF STRUGGLE INSIDE

302
00:44:09,648 --> 00:44:12,117
DECAPITATED

303
00:44:34,472 --> 00:44:37,008
Come quickly.

304
00:44:38,543 --> 00:44:44,315
ATSUSHI

305
00:44:47,452 --> 00:44:48,753
EXAM RESULT: FAIL

306
00:44:48,920 --> 00:44:53,425
Just confirming our order.

307
00:44:53,591 --> 00:44:58,463
Yes, one family size cake, please.

308
00:44:58,897 --> 00:45:01,066
Thank you.

309
00:45:01,700 --> 00:45:05,770
The cake will arrive tomorrow morning.

310
00:45:06,604 --> 00:45:09,207
I didn't know he was that stupid.

311
00:45:09,374 --> 00:45:12,043
His mock exam results weren't
good apparently.

312
00:45:12,210 --> 00:45:13,344
I wasn't told.

313
00:45:13,511 --> 00:45:14,813
I told you.

314
00:45:14,979 --> 00:45:16,748
You did not!

315
00:45:16,915 --> 00:45:20,318
You're his mother.
You have to watch over him.

316
00:45:20,885 --> 00:45:22,754
I'm sorry.

317
00:45:22,921 --> 00:45:25,223
You don't need to apologize.

318
00:45:26,458 --> 00:45:29,360
You're not his real mother anyway.

319
00:45:33,565 --> 00:45:35,066
What's so funny?

320
00:45:35,233 --> 00:45:37,202
Nothing.

321
00:45:47,112 --> 00:45:48,780
What is it?

322
00:45:51,816 --> 00:45:53,418
No thanks.

323
00:46:06,030 --> 00:46:09,134
How long will it take him
to pass the exam?

324
00:46:09,300 --> 00:46:12,604
How much money
have we spent on him?

325
00:46:12,771 --> 00:46:15,373
It's so humiliating.

326
00:46:15,540 --> 00:46:17,375
What will other people think?

327
00:46:17,542 --> 00:46:19,611
It's always “other people”!

328
00:46:20,278 --> 00:46:22,147
You're no better!

329
00:46:22,313 --> 00:46:24,349
A baby at 16, crawling back home
after all that heartbreak.

330
00:46:24,516 --> 00:46:27,152
I didn't want to have a baby!

331
00:46:27,318 --> 00:46:29,754
I had a million other dreams!

332
00:46:29,921 --> 00:46:32,390
So why didn't you get rid of it then?

333
00:46:32,557 --> 00:46:34,859
How many times did I ask to!

334
00:46:47,138 --> 00:46:48,873
I'll be right there.

335
00:46:50,708 --> 00:46:52,577
I'll be right there.

336
00:46:54,679 --> 00:46:56,815
I'll be right there.

337
00:46:59,017 --> 00:47:01,119
Come quickly!

338
00:47:01,586 --> 00:47:03,488
I'm scared.

339
00:47:14,599 --> 00:47:16,701
Come here.

340
00:47:18,970 --> 00:47:21,940
Come here.

341
00:48:52,030 --> 00:48:55,099
Miho-san. I think grandma needs you.

342
00:48:55,266 --> 00:49:00,305
I'm tied up right now.

343
00:49:00,471 --> 00:49:02,407
I'll be right there.

344
00:49:18,489 --> 00:49:20,258
Are you hungry?

345
00:49:21,326 --> 00:49:23,127
Shall I make something?

346
00:49:36,741 --> 00:49:38,409
Miho-san?

347
00:49:54,158 --> 00:49:55,893
What?

348
00:50:08,239 --> 00:50:14,045
That hurts! Stop it! It hurts!

349
00:50:14,212 --> 00:50:23,388
No! No!

350
00:51:13,204 --> 00:51:18,409
No! No!

351
00:52:51,936 --> 00:52:57,141
Have you got any plans for Christmas?

352
00:53:02,380 --> 00:53:07,752
My daughter's in 4th grade
at elementary.

353
00:53:08,319 --> 00:53:10,922
I haven't got her a present yet.

354
00:53:11,088 --> 00:53:15,359
What do girls that age like?

355
00:53:57,034 --> 00:53:58,869
I'll be right there.

356
00:54:00,438 --> 00:54:02,740
I'll be right there.

357
00:54:04,609 --> 00:54:06,777
I'll be right there.

358
00:54:13,284 --> 00:54:15,419
Come quickly.

359
00:54:16,254 --> 00:54:18,322
I'm scared.

360
00:54:18,756 --> 00:54:21,225
Come here.

361
00:54:26,364 --> 00:54:28,199
I'm sorry.

362
00:54:30,034 --> 00:54:31,669
I'm sorry.

363
00:54:34,639 --> 00:54:36,440
I'm sorry.

364
00:55:14,712 --> 00:55:16,380
I'm home!

365
00:55:22,620 --> 00:55:23,821
“l'll be home late.

366
00:55:23,988 --> 00:55:25,890
Go ahead and eat.”

367
00:55:42,840 --> 00:55:44,508
Are you home?

368
00:55:56,620 --> 00:55:58,322
Mom?

369
00:56:11,869 --> 00:56:13,804
Akane.

370
00:56:23,481 --> 00:56:25,383
Akane.

371
00:56:29,220 --> 00:56:30,821
I'm sorry!

372
00:56:31,622 --> 00:56:44,802
I'm sorry!

373
00:58:46,790 --> 00:58:49,860
Executive Producer Kazuo Kato

374
00:58:50,027 --> 00:58:53,097
Produced by Taka lchise

375
00:58:53,264 --> 00:58:55,065
Co-Producer Toshinori Nishimae

376
00:58:55,232 --> 00:58:57,301
Original JU-ON Concept Created by
Takashi Shimizu

377
00:58:57,468 --> 00:58:58,269
Cinematography by
Koji Kanaya (J.S.C.)

378
00:58:58,435 --> 00:59:00,337
Art Director Shimpei Inoue

379
00:59:00,504 --> 00:59:02,540
Music by Gary Ashiya

380
00:59:02,706 --> 00:59:05,743
Edited by Yoshifumi Fukazawa

381
00:59:47,351 --> 00:59:50,621
An OZ Production

382
00:59:50,788 --> 00:59:56,327
TOEI VIDEO and CELL Present

383
00:59:58,362 --> 01:00:06,670
Written and Directed by
Ryuta Miyake

384
01:00:09,607 --> 01:00:15,245
C2009 TOEI VIDEO/CELL