1
00:01:31,034 --> 00:01:33,037
Well, let's see what we�ve
got in this thing.

2
00:01:34,200 --> 00:01:41,038
Hot dogs, beans, napkins
...oh wine!...

3
00:01:45,039 --> 00:01:46,039
... coke, ...

4
00:01:50,200 --> 00:01:51,200
... apples, ...

5
00:01:52,650 --> 00:01:56,200
... 3 sandwiches, you pig...

6
00:01:56,300 --> 00:01:57,800
Thank you dear,
I love you too.

7
00:02:00,040 --> 00:02:02,040
Oh, what's this?

8
00:02:05,041 --> 00:02:07,041
Looks like skipping stones.

9
00:02:08,042 --> 00:02:12,042
Oh those are arrowheads,
I found those right outside
the front door.

10
00:02:14,043 --> 00:02:16,043
You know this whole area
used to be Indian territory?

11
00:02:17,044 --> 00:02:18,044
Come on to the picnic Shelly.

12
00:02:18,045 --> 00:02:21,400
We might as well live like Injuns
as long as we're up here.

13
00:02:21,450 --> 00:02:24,046
These Indians don't go
on picnics.Come on!

14
00:02:24,400 --> 00:02:26,700
Would you be quiet, 
I got her just where I want her.

15
00:02:27,000 --> 00:02:28,600
Seven spaces in front of Boardwalk.

16
00:02:34,049 --> 00:02:36,500
Okay, but you're missing
 out on a good time.Come on!

17
00:02:37,050 --> 00:02:39,900
Well, we're coming to have fun
and all she wants to do is play Monopoly.

18
00:02:39,950 --> 00:02:41,450
Don't worry about it.

19
00:03:15,090 --> 00:03:16,090
Hey.

20
00:03:17,500 --> 00:03:19,050
I'm going to teach you
something interesting.

21
00:03:19,600 --> 00:03:20,400
See this one?
-Yes.

22
00:03:21,250 --> 00:03:23,500
... this is part of a hunting arrow,
okay?

23
00:03:26,500 --> 00:03:31,093
And this used to fit on a larger
 spear for hunting bigger game
like you know...deer.

24
00:03:32,750 --> 00:03:34,600
How come you know so much
about Indians?

25
00:03:35,200 --> 00:03:36,555
I'm an old boy scout.

26
00:03:39,400 --> 00:03:40,700
It's my duty to know!

27
00:03:41,097 --> 00:03:42,800
Besides, I grew up in this area,
you know I...

28
00:03:43,700 --> 00:03:47,098
It's all I ever heard was Indian
legends and stories...
you  know, the whole bit.

29
00:03:48,100 --> 00:03:49,700
You want to know something
interesting?

30
00:03:50,500 --> 00:03:51,500
For a change, you mean?

31
00:03:52,500 --> 00:03:53,850
You know this place we're staying?

32
00:03:55,000 --> 00:03:57,550
It used be part of an
old indian burial ground.

33
00:03:58,500 --> 00:03:59,800
Very sacred and holy.

34
00:04:00,800 --> 00:04:01,800
Ooh, scary!

35
00:04:02,400 --> 00:04:03,900
Well, is it cursed or something?

36
00:04:04,800 --> 00:04:05,300
Yeah.

37
00:04:07,000 --> 00:04:08,000
As a matter of fact, it is.

38
00:04:08,900 --> 00:04:09,900
Are you serious?

39
00:04:10,900 --> 00:04:11,400
Yeah.

40
00:04:12,900 --> 00:04:13,900
Don't worry about it.

41
00:04:14,250 --> 00:04:17,900
You're only cursed by the evil
spirits if you violate the graves
of the dead...

42
00:04:19,450 --> 00:04:21,100
... we're just going to be
eating hot dogs.

43
00:04:22,200 --> 00:04:23,700
Besides, I'm there to protect you.

44
00:04:24,500 --> 00:04:25,300
You?

45
00:04:26,150 --> 00:04:26,650
Yeah.

46
00:04:27,108 --> 00:04:30,108
You're going to protect me?
Yeah!

47
00:04:35,100 --> 00:04:35,600
Hey!

48
00:04:38,600 --> 00:04:41,800
Help me build a fire
 for the hot dogs...
why don't you see if you can
find some wood.-Okay.

49
00:05:52,110 --> 00:05:53,110
God ...

50
00:05:54,650 --> 00:05:56,111
... this stuff must be 200 
years old!

51
00:06:24,112 --> 00:06:25,250
Here's your firewood.

52
00:06:25,800 --> 00:06:26,800
That's as far as I go!

53
00:06:28,113 --> 00:06:30,113
Ellen, look at this!
What is it?

54
00:06:31,650 --> 00:06:33,000
It's an old Indian dagger.

55
00:06:34,150 --> 00:06:35,500
God, it's in perfect shape.

56
00:06:41,235 --> 00:06:42,483
Oh my!

57
00:06:42,800 --> 00:06:44,000
What's it doing here?

58
00:06:45,125 --> 00:06:47,000
Well, as far as I know ...

59
00:06:48,200 --> 00:06:50,000
... when the medicine
man of a tribe died, ...

60
00:06:50,650 --> 00:06:52,800
... they used to bury one
of his possessions with him...

61
00:06:53,127 --> 00:06:54,950
... so he could have it
in his next life.

62
00:06:55,400 --> 00:06:58,600
Oh, you mean we're having
a picnic over somebody's
dead body?

63
00:07:00,131 --> 00:07:02,500
Yeah, but all that's left now
is Tinga.

64
00:07:03,132 --> 00:07:05,500
The Indian spirit of the woods ...

65
00:07:06,000 --> 00:07:10,500
... who watches over and
protects the medecine man's
grave for all eternity.

66
00:07:11,800 --> 00:07:13,950
Oh hold me, I'm scared!

67
00:07:35,500 --> 00:07:37,500
Go directly to jail, do not pass...Fuckin'...

68
00:07:37,800 --> 00:07:39,500
Damn it, I hate this game!

69
00:07:39,600 --> 00:07:41,500
Oh really?
Because you're losing now.

70
00:07:41,600 --> 00:07:44,500
Oh, I'm sure. This game
is for fat old spastic people
that don't know any better...

71
00:07:44,550 --> 00:07:47,800
... I can't do any damn thing.
I hate this lousy game.

72
00:07:47,900 --> 00:07:50,000
Well, 20 minutes ago
you didn't think so.

73
00:07:50,140 --> 00:07:53,140
Yeah, well that was different either...
That was different.

74
00:07:53,141 --> 00:07:54,800
And why was that different?

75
00:07:55,142 --> 00:07:56,300
I was winning.

76
00:08:17,800 --> 00:08:18,500
Bruce?

77
00:08:23,800 --> 00:08:24,600
Bruce?

78
00:08:39,000 --> 00:08:40,500
Bruce, where are you?

79
00:09:06,500 --> 00:09:07,500
Bruce!

80
00:09:23,500 --> 00:09:25,500
Bruce has gone back to the
cottage without me.

81
00:10:20,500 --> 00:10:21,250
Who's there?

82
00:10:23,152 --> 00:10:26,750
Scotty? Scotty? 
Scotty, is that you?

83
00:10:28,155 --> 00:10:30,455
Please, who's there?

84
00:10:31,157 --> 00:10:33,157
Answer me!

85
00:12:51,160 --> 00:12:52,160
Scotty!

86
00:12:54,162 --> 00:12:57,162
Scotty! Shelly!
Open the door!

87
00:13:01,700 --> 00:13:03,167
Open up!

88
00:13:04,700 --> 00:13:06,167
Sorry, I locked her out.
Yeah, Jeez.

89
00:13:10,500 --> 00:13:12,500
Oh God. Who the...

90
00:13:12,510 --> 00:13:13,500
I guess you should 
get the door. Yeah...

91
00:13:43,600 --> 00:13:45,000
Alright, alright!

92
00:14:26,169 --> 00:14:30,169
He's dead, he's dead!
Who's dead? Bruce?

93
00:14:31,500 --> 00:14:32,500
I saw him ...

94
00:14:32,900 --> 00:14:36,172
... he was on the ground ...
he was all cut up.

95
00:14:37,500 --> 00:14:40,174
Oh God. Are you sure
it was Bruce?
Did you see his face?

96
00:14:42,175 --> 00:14:47,000
I was sleeping...
Ok, slow down.
...And I woke up
and then I started running...

97
00:14:47,150 --> 00:14:50,000
Alright here's some coffee ...
See if you can make
any sense out of her, I can't...

98
00:14:50,250 --> 00:14:53,350
It's ok Ellen, you were
having a nightmare...
you could've been completely!

99
00:14:56,800 --> 00:15:01,200
Alright, listen. Maybe Bruce
is hurt. I'm going
 to go outside and look for him.

100
00:15:01,600 --> 00:15:03,800
I want you to take care
of her. Be careful.
 I'll be back. You be careful.

101
00:15:06,000 --> 00:15:08,000
Let's go in the kitchen 
and get something to eat ...

102
00:15:08,200 --> 00:15:10,600
... then we'll get changed,
then I'll bet you in 30 seconds ...

103
00:15:10,650 --> 00:15:13,900
... Bruce and Scotty will come
 through the door laughing
their heads off...

104
00:15:14,000 --> 00:15:15,000
Don't worry about it.

105
00:15:29,200 --> 00:15:30,000
Bruce!

106
00:15:34,550 --> 00:15:35,350
Bruce!

107
00:15:38,190 --> 00:15:39,500
Where are they?

108
00:15:40,800 --> 00:15:42,300
They should be back by now!

109
00:15:44,000 --> 00:15:47,500
Well, they're probably
 rounding up the picnic
 basket and the blanket...
that's all.

110
00:15:56,400 --> 00:15:57,200
Bruce!

111
00:16:38,000 --> 00:16:39,350
Hey, there's some food
on the stove.

112
00:16:39,350 --> 00:16:41,500
Now you wait here, I'm going
to take a look myself.

113
00:16:41,550 --> 00:16:43,000
No, please! Don't go out there.

114
00:16:43,208 --> 00:16:44,850
I'm just going to step outside ...

115
00:16:45,200 --> 00:16:47,200
... and shine the light
into the woods.

116
00:16:56,150 --> 00:16:57,211
I won't go far.

117
00:17:22,500 --> 00:17:24,800
Join us!

118
00:18:30,000 --> 00:18:32,215
Join us!

119
00:20:10,000 --> 00:20:11,000
Oh, my God!

120
00:20:20,150 --> 00:20:22,800
He's coming!
He's coming, Scotty!

121
00:20:28,297 --> 00:20:29,797
Scotty, please, Scotty!!

122
00:20:34,337 --> 00:20:35,592
Please, Scotty!

123
00:20:48,836 --> 00:20:49,543
Scotty, please!

124
00:20:52,127 --> 00:20:53,294
Please, Scotty!

125
00:21:03,909 --> 00:21:04,650
Scotty!

126
00:21:30,175 --> 00:21:31,103
Scotty!

127
00:21:34,400 --> 00:21:36,200
What have I done?

128
00:21:36,450 --> 00:21:37,250
I saw.

129
00:21:38,800 --> 00:21:41,700
Quick, ... , cellar ...

130
00:21:43,000 --> 00:21:45,800
... gun! Hurry.

131
00:21:47,700 --> 00:21:49,200
You got to get the gun!
The gun!

132
00:24:42,000 --> 00:24:48,302
You have violated the
ancient ways!

133
00:24:50,303 --> 00:24:53,600
... and so must die ...

134
00:24:54,600 --> 00:24:56,304
to join...

135
00:26:57,306 --> 00:26:59,306
Join us!

136
00:27:06,500 --> 00:27:08,300
Join us!

137
00:28:35,200 --> 00:28:36,200
Die!

138
00:28:44,800 --> 00:28:45,800
Die!

139
00:29:34,700 --> 00:29:36,800
Bruce, Bruce...

140
00:29:39,200 --> 00:29:40,100
Shelly ...

141
00:29:42,200 --> 00:29:43,100
Let's go on a picnic ...

142
00:29:46,750 --> 00:29:47,650
Scotty ...

143
00:29:49,050 --> 00:29:50,000
and Shelly ...

144
00:29:56,050 --> 00:29:58,000
You got anything good?...

145
00:30:01,050 --> 00:30:02,000
We'll go on a picnic...

