1
00:00:00,425 --> 00:00:02,850
[Idol Drama Operation Team]
[Script Reading Day]

2
00:00:03,420 --> 00:00:05,930
[Before filming the drama,]
[the staff calls the members]

3
00:00:05,930 --> 00:00:08,535
[to coordinate with one another]

4
00:00:10,300 --> 00:00:13,825
[Whoa~]
[Writer Somi arrived first]

5
00:00:14,310 --> 00:00:15,980
[Serious Somi]

6
00:00:16,255 --> 00:00:18,430
[Same aged friends]
[Sohee and D.ana arrive~]

7
00:00:19,306 --> 00:00:20,856
Welcome~

8
00:00:21,081 --> 00:00:22,901
[As Sohee & D.ana arrive]
D.ana, please sit here.

9
00:00:22,901 --> 00:00:24,531
[Full serious mode comes out]

10
00:00:24,531 --> 00:00:25,996
Sohee...

11
00:00:26,325 --> 00:00:28,460
[Blurred vision for a 17 year old]
Over there.

12
00:00:28,460 --> 00:00:30,890
- Is that right? - Yes
[Somi is into role playing]

13
00:00:32,080 --> 00:00:34,945
You just have to listen to me.

14
00:00:35,290 --> 00:00:36,580
I don't think so.

15
00:00:36,580 --> 00:00:38,325
[Tsk!]
I'm in the CEO's seat...

16
00:00:38,815 --> 00:00:41,700
[You make no sense...]

17
00:00:41,700 --> 00:00:44,265
[Back to checking the script]

18
00:00:44,265 --> 00:00:47,010
- You checked the script already.
- I'm doing it now.

19
00:00:47,010 --> 00:00:49,550
[Like test day at school?]
I said I'd do a little, but I didn't.

20
00:00:50,300 --> 00:00:53,645
- Look at this. I'm ready.
- Earlier...

21
00:00:54,995 --> 00:00:57,390
You know? Just doing the action?

22
00:00:57,390 --> 00:01:00,340
Just underlining everything...
I'm like that.

23
00:01:00,340 --> 00:01:01,885
The page just gets messy.

24
00:01:02,150 --> 00:01:03,600
[Good at underlining]
Trying to look good.

25
00:01:05,028 --> 00:01:09,419
[Time to read the script]
[is approaching]

26
00:01:10,360 --> 00:01:12,890
[Writer Seulgi has arrived]
Role of Kang Seulgi.

27
00:01:15,610 --> 00:01:18,295
[Moon Byul has arrived]

28
00:01:18,515 --> 00:01:20,700
You posted some things on your script?

29
00:01:20,700 --> 00:01:24,165
I guess so~
[feat. post its]

30
00:01:25,355 --> 00:01:28,315
- I'm sorry~
- You came, YooA?

31
00:01:29,640 --> 00:01:31,740
I ran over.
[Rushed over from "Music Bank" rehearsals]

32
00:01:31,740 --> 00:01:34,640
You finished your rehearsal?
You have to go back then?

33
00:01:34,640 --> 00:01:36,685
- After this?
- Afterwards...

34
00:01:37,110 --> 00:01:41,560
[Bbang Dduk rushes over to her seat]
Su-Jeong.

35
00:01:41,955 --> 00:01:44,335
[PD] [Main Writer] [Drama Writer]

36
00:01:44,755 --> 00:01:47,330
[All members and staff have gathered]

37
00:01:47,580 --> 00:01:50,545
- You all read the script? - Yes.
- How was it?

38
00:01:50,960 --> 00:01:52,085
It was sad.

39
00:01:52,085 --> 00:01:54,240
[Seven writers felt sentimental]
[from reading their own stories]

40
00:01:54,240 --> 00:01:57,365
As I read the script,
I reminisced and thought a lot.

41
00:01:57,365 --> 00:01:58,650
Then I fell asleep...

42
00:01:58,650 --> 00:02:00,420
[What?]
You really fell asleep?

43
00:02:02,445 --> 00:02:04,375
- It was really fun.
- Yes, seriously.

44
00:02:04,375 --> 00:02:07,205
Everything we talked about
was included.

45
00:02:07,205 --> 00:02:10,325
It was cool that it all came together.

46
00:02:10,800 --> 00:02:14,080
[Before we get started]
In addition to the members,

47
00:02:14,338 --> 00:02:16,673
[Empty seats?]
two more actors are coming today.

48
00:02:17,567 --> 00:02:20,826
[Two empty seats]
[The guest stars are?]

49
00:02:20,826 --> 00:02:22,092
Who are they?

50
00:02:25,507 --> 00:02:28,218
[Special actors enter]

51
00:02:28,740 --> 00:02:29,885
Whoa! Teacher.

52
00:02:30,860 --> 00:02:32,050
[Who is it?]

53
00:02:32,590 --> 00:02:35,925
[Who's the teacher of]
[Idol Drama Operation Team?]

54
00:02:36,675 --> 00:02:38,695
[Warm welcome]
[Joyful round of applause]

55
00:02:39,240 --> 00:02:41,335
[The acting teacher for the members]

56
00:02:41,335 --> 00:02:43,725
[as well as the matchless actor,]
[Jang Won-Young, has arrived]

57
00:02:44,119 --> 00:02:47,895
[A week before the reading,]
[he became their acting mentor]

58
00:02:49,130 --> 00:02:51,505
[A big picture for the acting rookies]

59
00:02:51,505 --> 00:02:54,360
[Teaching them through fun activities]
[to bring our their emotions]

60
00:02:54,625 --> 00:02:57,465
[Improvising to bring out their own characters]

61
00:02:57,465 --> 00:02:59,345
[Teacher Jang, the big time actor]

62
00:02:59,345 --> 00:03:01,480
[Completely satisfied with the class]
An unexpected class...

63
00:03:01,480 --> 00:03:03,196
It was a meaningful time...

64
00:03:03,196 --> 00:03:07,151
I love you, teacher.
[A love confession? Random Somi]

65
00:03:08,094 --> 00:03:09,192
[Then]

66
00:03:09,192 --> 00:03:11,135
I think it would be great
[Leader Moon makes the request]

67
00:03:11,135 --> 00:03:14,685
if you came out in our drama.

68
00:03:14,685 --> 00:03:16,055
[Though Teacher Jang looked helpless]

69
00:03:16,725 --> 00:03:19,900
[he agreed to appear in the drama]

70
00:03:21,980 --> 00:03:23,220
[And]

71
00:03:23,489 --> 00:03:26,881
[One more special guest]
[who surprised the members]

72
00:03:27,530 --> 00:03:29,355
[Can't believe it]
[Chung Ha has arrived]

73
00:03:29,355 --> 00:03:32,213
[The I.O.I member who]
[gave off a fresh charm]

74
00:03:32,213 --> 00:03:35,093
[Cute cute]
[The one who made our hearts race]

75
00:03:36,011 --> 00:03:38,986
[The first I.O.I member]
[to make a solo debut]

76
00:03:39,725 --> 00:03:45,296
[Captivating the hearts of men]
[with her pure and feminine charms everyday]

77
00:03:45,922 --> 00:03:49,032
[Welcome to Idol Drama Operation Team]

78
00:03:50,410 --> 00:03:53,475
[Role of Director Jang]
[Role of Kim Chung Ha]

79
00:03:53,475 --> 00:03:54,590
Have you been well?

80
00:03:55,192 --> 00:03:58,217
[Special guests have brought up the mood~]

81
00:03:58,695 --> 00:04:00,562
Director Jang~

82
00:04:00,562 --> 00:04:04,505
[PD begins the script reading]
We'll briefly read over the script.

83
00:04:05,450 --> 00:04:07,425
I'm nervous.

84
00:04:07,720 --> 00:04:09,835
[Fidgets]
[Dazed~]

85
00:04:10,455 --> 00:04:13,780
[Nervous about the first reading...]

86
00:04:14,375 --> 00:04:16,955
We'll read it from the beginning.

87
00:04:17,650 --> 00:04:21,060
[#. Girls are nominated]
[for first place at "Music Bank"]

88
00:04:21,060 --> 00:04:24,270
[The Girl Next Door wakes up late]
[and rushes to get ready]

89
00:04:24,960 --> 00:04:26,430
Su-Jeong, wake up!
We're late!

90
00:04:26,800 --> 00:04:28,275
What do you mean...

91
00:04:28,450 --> 00:04:30,680
[Showing her acting skills]

92
00:04:30,680 --> 00:04:33,970
It's already morning.
You said you'd eat jokbal before.

93
00:04:33,970 --> 00:04:35,835
[Jokbal in the morning?]

94
00:04:35,835 --> 00:04:37,730
Oh right! My jokbal.

95
00:04:37,730 --> 00:04:40,210
[Su-Jeong loves jokbal in the drama]
I couldn't resist eating it last night.

96
00:04:40,210 --> 00:04:41,615
[Doing a good job]
Just one bite.

97
00:04:41,615 --> 00:04:43,185
Wake up, YooA.
We're late.

98
00:04:43,185 --> 00:04:46,250
[Good start amidst concern]
- What time is it? - Wake up, Somi. We're late!

99
00:04:46,250 --> 00:04:48,810
[#Great expressions]
[#Not acting, but reality]

100
00:04:49,228 --> 00:04:50,893
[Proud]
[Smiles]

101
00:04:50,893 --> 00:04:55,390
Sohee is still sleeping, but above her,

102
00:04:55,740 --> 00:04:58,110
[The highlight approaches]
we can hear Director Jang's voice.

103
00:04:58,405 --> 00:04:59,710
What are you doing...?

104
00:05:00,780 --> 00:05:04,095
What are you doing? You're still not awake?
[Jang is able to fall into character]

105
00:05:04,340 --> 00:05:06,600
[Goosebumps...]

106
00:05:06,600 --> 00:05:09,020
Do you think you can win like this?

107
00:05:09,245 --> 00:05:11,830
Your attitude is all wrong!

108
00:05:11,830 --> 00:05:12,795
What are you doing right now?

109
00:05:12,795 --> 00:05:16,760
Girl's Generation would already be
at the salon by now~

110
00:05:17,295 --> 00:05:20,630
[Can't help but admire]

111
00:05:21,162 --> 00:05:23,396
[Respect]
The line is so funny.

112
00:05:23,396 --> 00:05:25,231
[#. "Music Bank" Rehearsal]
On the way to "Music Bank."

113
00:05:25,536 --> 00:05:29,085
The "Music Bank" PD will be played
by actor, Shin Hyun-Joon.

114
00:05:30,035 --> 00:05:34,265
[Special actors appearing as]
[cameos for "Flower Road"]

115
00:05:34,810 --> 00:05:37,915
[Shin Hyun-Joon and EXO?!]

116
00:05:37,915 --> 00:05:41,525
The girls silently rejoice
in front of the door.

117
00:05:41,525 --> 00:05:45,335
They look at the sign on the next door,
which says Jeon So-Min.

118
00:05:45,335 --> 00:05:48,665
[Starting to get nervous]
[Her appearance is confirmed]

119
00:05:48,665 --> 00:05:50,915
[Starring as their senior]
She's done with rehearsals, right?

120
00:05:50,915 --> 00:05:52,800
Should we knock?

121
00:05:52,800 --> 00:05:55,640
[Jeon So-Min's line]
Who's making that noise outside my door?

122
00:05:55,640 --> 00:05:57,275
[Cold]
The Girl Next Door?

123
00:05:57,590 --> 00:05:59,950
[Role of a feisty senior]
Is that why you chatter like them?

124
00:05:59,950 --> 00:06:01,760
That's right. You're a first place nominee?

125
00:06:03,025 --> 00:06:04,755
We'll work hard today.

126
00:06:04,755 --> 00:06:06,810
We want to become singers
[Seulgi is good at everything]

127
00:06:06,810 --> 00:06:09,015
who can act well, too.
It'll be hard, right?

128
00:06:09,015 --> 00:06:11,900
Of course. It's difficult.
It's not easy to be like me.

129
00:06:11,900 --> 00:06:14,725
I'm not sure if you can do it
just with effort...

130
00:06:14,725 --> 00:06:18,215
[Suddenly]
Right? We can't get over it.

131
00:06:18,840 --> 00:06:21,210
We always envy and respect you.

132
00:06:21,210 --> 00:06:23,290
We will work even harder~

133
00:06:23,290 --> 00:06:25,760
[Scene after meeting Jeon So-Min]

134
00:06:25,760 --> 00:06:27,413
Will we be like that
when we meet our juniors?

135
00:06:27,413 --> 00:06:30,643
- I almost died holding my tongue.
- I'm old, too.

136
00:06:31,695 --> 00:06:32,940
A bit stronger.

137
00:06:33,315 --> 00:06:35,975
[Puts in emotions]
I'm old, too.

138
00:06:36,565 --> 00:06:38,670
[Relieved!]
[Moon Byul is doing well]

139
00:06:38,670 --> 00:06:41,657
The others have cute roles,

140
00:06:41,657 --> 00:06:44,857
but I think it'll be good if
Moon Byul's role balances that out.

141
00:06:44,857 --> 00:06:46,242
[Also Moon-crush in the drama]
Sounds good.

142
00:06:47,422 --> 00:06:49,726
Oh, turn on the monitor.
[Continuing the reading]

143
00:06:49,726 --> 00:06:51,496
Miss A went on stage.

144
00:06:51,496 --> 00:06:53,760
[#. Envying Miss A, who are on TV]
Suzy is so pretty.

145
00:06:53,760 --> 00:06:56,270
Why did my mom raise me like this?

146
00:06:56,270 --> 00:06:58,155
I still get negative comments.

147
00:06:58,155 --> 00:06:59,605
"She's a celebrity?"

148
00:06:59,605 --> 00:07:00,915
They call me,
"Bbang Dduk."

149
00:07:02,032 --> 00:07:05,870
[At the first meeting]
Fans call me Bbang Dduk.

150
00:07:05,870 --> 00:07:09,050
[Su-Jeong misinterpreted]
[the nickname that fans gave her]

151
00:07:09,050 --> 00:07:11,495
They might be saying that with bad intentions.

152
00:07:11,495 --> 00:07:13,270
Now, it's a term of endearment.

153
00:07:13,270 --> 00:07:15,755
It sounds like "Our Bbang Dduk."
[Is that how you felt?]

154
00:07:16,410 --> 00:07:18,865
[Came to like the nickname]

155
00:07:19,460 --> 00:07:23,145
[Staff inserted various sides of her story]

156
00:07:24,440 --> 00:07:26,790
[Cute]
Do I need to lose weight?

157
00:07:26,790 --> 00:07:29,605
They say fat woman haven't won the lottery yet.

158
00:07:30,474 --> 00:07:33,545
[#. Officially unfolding the story!]
[Unveiling their past]

159
00:07:34,625 --> 00:07:36,930
How annoying!
Are we rabbits?

160
00:07:37,200 --> 00:07:38,800
[Dieting is their biggest struggle]

161
00:07:38,800 --> 00:07:40,145
Let's order food.

162
00:07:40,145 --> 00:07:42,555
Okay. Let's order jokbal! Jokbal!

163
00:07:42,555 --> 00:07:44,270
[Jokbal is the best]

164
00:07:44,270 --> 00:07:46,425
What are you going to do about him?

165
00:07:46,425 --> 00:07:49,575
Don't worry.
I have an idea.

166
00:07:49,930 --> 00:07:53,430
[Director Jang comes into the practice room]

167
00:07:54,460 --> 00:07:57,660
[Nags more than a mother]
What are you doing right now?

168
00:07:58,630 --> 00:08:01,070
Your best isn't even good enough.

169
00:08:01,070 --> 00:08:03,966
You call this dancing?

170
00:08:04,370 --> 00:08:06,920
[Wait for us, jokbal]
[Moon Byul's hardcore acting beings]

171
00:08:07,255 --> 00:08:08,985
- Unnie!
- Moon Byul.

172
00:08:09,855 --> 00:08:10,905
What are you doing?

173
00:08:10,905 --> 00:08:13,235
I feel gassy...

174
00:08:13,235 --> 00:08:17,220
Oh my... You must be kidding me.

175
00:08:17,465 --> 00:08:19,120
[Totally my taste]

176
00:08:19,390 --> 00:08:21,400
[Laughs]
You were like this last time.

177
00:08:21,845 --> 00:08:23,415
I'll run to the pharmacy to buy medicine,

178
00:08:23,415 --> 00:08:24,680
so wait here.
[So funny]

179
00:08:24,680 --> 00:08:28,165
[His impersonations never fail]
A girl group that's gassy...

180
00:08:30,435 --> 00:08:32,060
You should be embarrassed.

181
00:08:32,635 --> 00:08:36,785
[#. Members finally get jokbal]
The moment they take a bite.

182
00:08:36,785 --> 00:08:37,863
What are you doing?

183
00:08:37,863 --> 00:08:40,093
[Director and CEO enter]
Can't you see the CEO is here?

184
00:08:40,093 --> 00:08:43,683
The CEO will be played by actor Im Won-Hee.

185
00:08:44,240 --> 00:08:47,120
[Recruited after filming]
["Strong Woman Do Bong-Soon"]

186
00:08:47,120 --> 00:08:49,945
[Just hearing about the cast]
[excites them]

187
00:08:50,735 --> 00:08:52,955
[What's their fate...?]
You ordered jokbal?

188
00:08:52,955 --> 00:08:56,805
If you weight more than 50kg
at the monthly evaluation, you're out.

189
00:08:58,375 --> 00:09:02,175
[The story D.ana shared at the meeting]
[was put into the script]

190
00:09:02,175 --> 00:09:05,495
We're allowed to leave once
we meet that target weight.

191
00:09:05,495 --> 00:09:06,680
What if you can't?

192
00:09:06,680 --> 00:09:10,225
If we can't, they kick us out
during the monthly evaluations.

193
00:09:10,483 --> 00:09:13,840
If you weight more than 50kg
at the monthly evaluation, you're out.

194
00:09:14,190 --> 00:09:16,800
[Members immersed by realistic script]
Why? You don't want to leave?

195
00:09:16,800 --> 00:09:17,900
Then don't eat!

196
00:09:17,900 --> 00:09:20,530
You eat jokbal, pizza, and chicken, and
do everything you want?

197
00:09:20,530 --> 00:09:23,705
How do you want to perform then?
Is that being a celebrity?

198
00:09:23,705 --> 00:09:24,950
Don't make others suffer,

199
00:09:24,950 --> 00:09:26,505
and just quit!
[Seriously saddened]

200
00:09:27,345 --> 00:09:29,520
[#. They exercise because of his scolding...]

201
00:09:29,520 --> 00:09:32,380
I'm saying this because I'm struggling,

202
00:09:32,380 --> 00:09:34,385
but can we eat a little bit?

203
00:09:34,670 --> 00:09:36,935
You're saying that right now?

204
00:09:36,935 --> 00:09:37,835
Yes.
[Unreasonable character]

205
00:09:39,050 --> 00:09:41,670
[Moon robot?]
Fine... I guess you'd say that.

206
00:09:41,670 --> 00:09:44,665
You have to say that last line
as if she's pathetic.

207
00:09:44,665 --> 00:09:47,895
[Giving immediate feedback]
[as they read the script]

208
00:09:47,895 --> 00:09:50,365
"I guess you'd say that."

209
00:09:50,365 --> 00:09:51,945
[Got it]

210
00:09:52,240 --> 00:09:56,550
[#. Upon Su-Jeong's wishes]
[she's given a hamburger]

211
00:09:56,550 --> 00:09:59,190
I'll only eat half. Just half.

212
00:09:59,190 --> 00:10:01,445
As soon as she takes a bite!
[That moment]

213
00:10:01,445 --> 00:10:04,645
[Director Jang's silhouette]
[comes in like a ghost]

214
00:10:05,435 --> 00:10:07,930
[These sad hunches are never wrong]

215
00:10:10,100 --> 00:10:12,965
[But this situations sounds so familiar]

216
00:10:12,965 --> 00:10:15,862
What do we do?
What if he comes?

217
00:10:16,479 --> 00:10:18,634
[YooA wrote this!]
They just make us diet.

218
00:10:18,634 --> 00:10:20,810
One day, I just really wanted to eat a hamburger.

219
00:10:20,810 --> 00:10:24,080
I was eating it on the second floor,
while looking outside the window

220
00:10:24,080 --> 00:10:27,235
our recruiting team leader walked down the street...

221
00:10:27,235 --> 00:10:29,330
[Saw the recruiting team leader]
[through the window]

222
00:10:29,330 --> 00:10:32,415
[Goosebumps]
[YooA contributed a relatable story]

223
00:10:32,415 --> 00:10:34,285
I got really scared,
so I picked up the burger...

224
00:10:34,285 --> 00:10:38,015
and I ate it in a bathroom stall.

225
00:10:40,185 --> 00:10:43,775
[Just like YooA's story, Moon Byul and]
[Su-Jeong hide in the bathroom]

226
00:10:44,430 --> 00:10:47,180
How much longer...

227
00:10:47,180 --> 00:10:49,995
[What are you saying...]

228
00:10:50,310 --> 00:10:53,795
[You don't have to be that quiet...]

229
00:10:54,400 --> 00:10:57,755
[Too absorbed by the script]

230
00:10:58,200 --> 00:11:00,805
[Cuteness overload]

231
00:11:01,900 --> 00:11:03,735
Let's exaggerate this part.

232
00:11:05,130 --> 00:11:07,955
[Not missing the cute ad-lib]
[by adding it in the script]

233
00:11:08,450 --> 00:11:10,125
[Nice nice]

234
00:11:10,845 --> 00:11:13,910
[#. Director Jang and his friend]
[come into the bathroom]

235
00:11:13,910 --> 00:11:17,050
[Don't know they're hiding]
- The CEO was so angry. - Why?

236
00:11:17,050 --> 00:11:20,500
[Nervous] They got caught
ordering jokbal behind our backs.

237
00:11:20,500 --> 00:11:23,141
What were you doing
when they were eating jokbal?

238
00:11:23,141 --> 00:11:29,515
One of the members was pretending to be sick.

239
00:11:29,800 --> 00:11:32,632
I could see through her,
but I just left.

240
00:11:32,632 --> 00:11:36,370
[He's actually on their side?] Why?

241
00:11:36,690 --> 00:11:39,000
[We didn't know, Director!]

