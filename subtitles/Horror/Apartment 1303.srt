1
00:02:52,560 --> 00:02:53,675
Hello?

2
00:02:53,880 --> 00:02:55,836
Yuka? How's it going?

3
00:02:56,040 --> 00:02:57,632
Good. I'm just getting everything
out of the boxes.

4
00:02:58,480 --> 00:03:00,391
Sorry I couldn't help today.

5
00:03:00,880 --> 00:03:03,758
No, that's all right. You're the one
who's had a rough day.

6
00:03:03,960 --> 00:03:06,394
It must be hard.
Is the funeral over?

7
00:03:06,800 --> 00:03:08,313
Yeah, it just finished.

8
00:03:09,000 --> 00:03:11,753
Grandma's safely
on her way to heaven.

9
00:03:12,520 --> 00:03:13,953
I'm heading straight to your place,
still in my funeral suit!

10
00:03:14,840 --> 00:03:16,956
Okay. I'll be waiting.

11
00:04:30,600 --> 00:04:33,114
BLUE PALACE HIRAOKA

12
00:04:33,320 --> 00:04:34,878
Good evening.

13
00:04:35,080 --> 00:04:37,310
What?
What's the matter?

14
00:04:47,120 --> 00:04:53,673
Hey! Yuka!
What are you doing?! don't!

15
00:05:00,480 --> 00:05:02,232
- No! Help!
- Don't! Yuka!

16
00:05:22,320 --> 00:05:23,469
We're here!

17
00:05:30,200 --> 00:05:33,954
Wow!
What a great building!

18
00:05:44,760 --> 00:05:48,673
Hey, there's a pool!
Great!

19
00:06:02,560 --> 00:06:05,518
THE MOVlNG SCHEDULE

20
00:06:05,720 --> 00:06:07,790
Don't! Stop it!

21
00:06:12,400 --> 00:06:14,675
Kenichiro!
Play this song next.

22
00:06:15,040 --> 00:06:19,636
- Let's have a barbecue on the beach!
- A barbecue! That's a Great idea!

23
00:06:20,720 --> 00:06:22,676
Yeah! A barbecue.

24
00:06:24,800 --> 00:06:26,756
Hello?
Oh, sis!

25
00:06:27,160 --> 00:06:29,116
Yeah, that's right.

26
00:06:29,320 --> 00:06:31,834
What? I'm fine.

27
00:06:32,040 --> 00:06:33,155
Big sister's checking up on her.

28
00:06:33,360 --> 00:06:37,638
How's Mom?
She is?

29
00:06:38,320 --> 00:06:40,993
Look after her, sis, okay?

30
00:06:41,400 --> 00:06:43,356
Oh, sis.

31
00:06:44,680 --> 00:06:46,636
I love you!
See you.

32
00:06:49,320 --> 00:06:51,709
Sayaka.
What did Mariko have to say?

33
00:06:52,240 --> 00:06:54,708
Not to go too crazy
when I've just moved in.

34
00:06:54,920 --> 00:06:57,195
My sister is such a drag!

35
00:06:57,800 --> 00:06:59,916
Maybe she's jealous
that her little sister's moved

36
00:07:00,120 --> 00:07:01,473
into a place as nice as this.

37
00:07:01,680 --> 00:07:04,035
She's going to be living alone
with your mother, isn't she?

38
00:07:04,640 --> 00:07:08,076
Yeah. But you know, isn't it weird
that the rent for a great place

39
00:07:08,280 --> 00:07:10,077
like this is so cheap?

40
00:07:10,280 --> 00:07:15,195
- It is weird! We could pay double.
- I agree.

41
00:07:15,720 --> 00:07:19,952
- Sayaka, are you sure this is okay?
- What do you mean, "Okay"?

42
00:07:22,080 --> 00:07:24,036
this is what she means.

43
00:07:29,200 --> 00:07:32,476
Uh-oh.
You're not mad, are you?

44
00:07:32,680 --> 00:07:35,592
Sorry.
It was a bad joke.

45
00:07:36,440 --> 00:07:38,112
No.

46
00:07:41,800 --> 00:07:43,756
Ouch!

47
00:07:44,720 --> 00:07:48,508
As a matter of fact,
that's exactly what it is.

48
00:07:49,000 --> 00:07:51,468
What do you mean,
that's what it is?

49
00:07:53,560 --> 00:07:58,236
They say this building is famous...
for ghosts.

50
00:07:58,920 --> 00:08:00,876
Ghosts?!

51
00:08:01,280 --> 00:08:03,236
That's right.

52
00:08:04,680 --> 00:08:08,958
It's, like, they say there's a woman
with long hair in a kimono

53
00:08:09,160 --> 00:08:12,675
who stands in the elevator hall...

54
00:08:13,920 --> 00:08:17,595
then when anyone comes up
in front of the elevator...

55
00:08:18,920 --> 00:08:21,753
the woman in the kimono says this...

56
00:08:22,680 --> 00:08:28,391
Is this the 13th floor?

57
00:08:39,880 --> 00:08:44,954
- And then when you say...
- Yes, this is the 13th floor.

58
00:08:47,240 --> 00:08:51,631
And you're about to get on
the elevator, suddenly...

59
00:08:57,240 --> 00:08:59,196
and then she says this...

60
00:08:59,400 --> 00:09:02,437
"give me back my eyes."

61
00:09:08,160 --> 00:09:11,869
What?
What does she say?

62
00:09:12,880 --> 00:09:14,836
What does she say?

63
00:09:24,440 --> 00:09:25,839
My eyes...

64
00:09:26,680 --> 00:09:29,911
My eyes...

65
00:09:30,640 --> 00:09:32,596
give them back!

66
00:09:35,520 --> 00:09:37,476
I got you!

67
00:09:40,120 --> 00:09:42,588
Yo, you're the most scared
of them all.

68
00:09:43,520 --> 00:09:47,308
So went the story
that was on TV the other night. Ta-da!

69
00:09:47,520 --> 00:09:49,670
Wha-aat?!

70
00:09:49,880 --> 00:09:52,599
You sure had me going!

71
00:09:53,720 --> 00:09:56,109
Anyway, it's got a great view, but it's
a long way from the train station

72
00:09:56,320 --> 00:09:57,594
and there aren't many buses
on this route,

73
00:09:57,800 --> 00:09:59,756
so there's hardly anyone
who wants to rent this place,

74
00:09:59,960 --> 00:10:01,188
is what the real-estate agent said.

75
00:10:05,720 --> 00:10:08,837
Kenichiro, change the music,
will you?

76
00:10:10,120 --> 00:10:12,076
That one scares me!

77
00:10:13,600 --> 00:10:16,512
Yo was more scared than anyone.
Get it together, dude!

78
00:10:16,720 --> 00:10:18,676
Yeah, you're a disgrace, Yo.

79
00:10:18,880 --> 00:10:19,949
Huh?
Where's Samantha?

80
00:10:20,160 --> 00:10:22,913
Yeah, I thought
she went to the bath room,

81
00:10:23,120 --> 00:10:25,350
but she hasn't come back,
has she?

82
00:10:29,400 --> 00:10:31,709
Samantha?

83
00:10:39,480 --> 00:10:41,436
Samantha?

84
00:10:51,520 --> 00:10:53,476
Samantha?

85
00:10:58,160 --> 00:11:01,994
Samantha,
what are you doing in here?

86
00:11:11,760 --> 00:11:13,955
What's the matter, Samantha?

87
00:11:20,160 --> 00:11:23,152
Hey, Sayaka, don't you
smell something  weird?

88
00:11:26,320 --> 00:11:29,232
Yeah, I do.
What a stink!

89
00:11:30,560 --> 00:11:33,120
Come on, time to eat.
Food!

90
00:12:07,720 --> 00:12:10,234
- Where was she?
- The tatami room.

91
00:12:12,800 --> 00:12:15,872
- Where's Sayaka?
- Over there.

92
00:12:34,120 --> 00:12:37,510
Sayaka, what are you doing?

93
00:12:41,120 --> 00:12:45,750
Sayaka, cut it out, will you?

94
00:12:50,520 --> 00:12:53,956
Hey... Sayaka?

95
00:13:13,680 --> 00:13:15,636
What's the matter with you?

96
00:13:16,080 --> 00:13:18,036
Sayaka?

97
00:13:27,240 --> 00:13:29,196
Sayaka?

98
00:13:29,800 --> 00:13:31,756
What are you doing?

99
00:13:34,360 --> 00:13:36,590
Well,

100
00:13:38,040 --> 00:13:41,077
I don't want my face to get
all smashed up, you know.

101
00:13:41,880 --> 00:13:43,836
What?

102
00:13:45,800 --> 00:13:47,756
Hey, Sayaka...

103
00:13:51,960 --> 00:13:53,916
Sayaka!

104
00:14:27,200 --> 00:14:29,156
Another one fell, huh?

105
00:15:34,880 --> 00:15:37,713
this must be hard
for you too, Lwata.

106
00:15:38,920 --> 00:15:40,990
I'm sorry you've been
put through all this.

107
00:15:48,200 --> 00:15:50,873
It must have been a real shock
to all of you.

108
00:15:51,080 --> 00:15:54,390
It happened right in front of you,
after all.

109
00:16:30,760 --> 00:16:35,754
- It wasn't suicide.
- What?

110
00:16:37,080 --> 00:16:39,389
Sayaka commit suicide?

111
00:16:40,720 --> 00:16:42,790
It'd never happen!

112
00:16:46,480 --> 00:16:48,596
Sayaka would never--

113
00:16:51,160 --> 00:16:54,277
she'd never kill herself.

114
00:16:57,160 --> 00:16:59,116
Don't!

115
00:17:04,000 --> 00:17:11,076
Sayaka was looking at me.
While she was falling...

116
00:17:12,400 --> 00:17:15,278
she was looking straight at me.

117
00:17:16,440 --> 00:17:18,954
The poor girl--

118
00:17:32,320 --> 00:17:34,276
Sayaka!

119
00:17:40,480 --> 00:17:42,436
Sis...

120
00:17:45,000 --> 00:17:51,997
Mom pushed me.

121
00:18:42,120 --> 00:18:46,079
Mother, how long
is this going to go on?

122
00:18:46,280 --> 00:18:49,795
Mother!
Snap out of it, will you?

123
00:18:52,320 --> 00:18:54,550
Sayaka...

124
00:18:59,160 --> 00:19:05,918
- What?
- Mariko, you're always like this.

125
00:19:06,720 --> 00:19:09,792
Aren't you sad that Sayaka's dead?

126
00:19:14,920 --> 00:19:19,152
What are you talking about?
What a horrible thing to say!

127
00:19:19,640 --> 00:19:21,596
Of course I'm sad!

128
00:19:21,800 --> 00:19:25,509
Why did it have to be her?
Why did it have to be her?

129
00:19:26,000 --> 00:19:28,389
Why her?
Why isn't she here?!

130
00:19:28,600 --> 00:19:31,034
Come on, Mother, please!

131
00:19:32,920 --> 00:19:35,195
Come on, please.

132
00:19:49,480 --> 00:19:52,870
Well, you'd better get going. You don't
want to keep everyone waiting.

133
00:19:53,760 --> 00:19:55,716
Thanks.

134
00:20:02,960 --> 00:20:05,872
Sayaka, phone as soon
as you get there.

135
00:20:06,760 --> 00:20:10,116
Yeah.
Okay then, bye.

136
00:20:23,360 --> 00:20:25,351
Sayaka...

137
00:20:28,560 --> 00:20:32,439
why would you do a stupid thing
like that?

138
00:21:20,680 --> 00:21:22,716
You didn't smoke, did you?

139
00:22:03,080 --> 00:22:05,196
That's a cute teddy bear.

140
00:22:10,680 --> 00:22:13,797
It was the big sister's from next door.

141
00:24:00,920 --> 00:24:07,314
THE MOVlNG SCHEDULE

142
00:24:11,640 --> 00:24:13,676
Here.

143
00:24:14,440 --> 00:24:16,715
- What's this?
- A moving schedule.

144
00:24:17,200 --> 00:24:19,634
Follow it and you'll get everything
done faster.

145
00:24:19,840 --> 00:24:24,436
That's one thing I'll say about you,
sis, you really handle details well.

146
00:24:24,840 --> 00:24:27,957
You'll make some man
a fine wife someday.

147
00:24:37,720 --> 00:24:45,354
Oh, damn.
Sis, I'm going to cry!

148
00:24:48,280 --> 00:24:49,759
Oh, come on!
What are you talking about?

149
00:24:49,960 --> 00:24:51,916
You don't have time for crying.

150
00:26:43,080 --> 00:26:45,036
Ow!

151
00:27:01,040 --> 00:27:03,429
Did she have earrings like this?

152
00:28:10,920 --> 00:28:14,071
UNKNOWN CALLER

153
00:28:31,440 --> 00:28:33,954
Ken!
The real-estate office just called!

154
00:28:34,160 --> 00:28:36,151
I got the apartment.

155
00:28:38,320 --> 00:28:42,074
They said the person ahead of me
had come in and canceled.

156
00:28:42,280 --> 00:28:44,874
It's hard to believe,
but I'm really lucky!

157
00:28:47,280 --> 00:28:49,236
Canceled?

158
00:28:52,360 --> 00:28:55,557
I just signed the contract.
What am I going to do?

159
00:28:55,760 --> 00:28:58,354
I'll be living alone for the first time.

160
00:28:59,640 --> 00:29:03,428
I found teddy! The poor thing
was stuck away in the closet.

161
00:29:07,840 --> 00:29:10,513
I've decided we're going to
live together!

162
00:29:17,760 --> 00:29:19,716
But that's--

163
00:29:36,280 --> 00:29:38,236
Hello.

164
00:29:39,640 --> 00:29:41,596
Your mother's out?

165
00:29:43,760 --> 00:29:45,716
Is that so?

166
00:29:58,800 --> 00:30:00,791
Do you know about what time
she might be home?

167
00:30:05,080 --> 00:30:07,310
You're taking care of the place
all alone till then?

168
00:30:07,520 --> 00:30:09,476
That's very brave.

169
00:30:14,520 --> 00:30:17,114
You said the big sister
in the next apartment

170
00:30:17,320 --> 00:30:19,675
gave you that teddy bear,
didn't you?

171
00:30:20,320 --> 00:30:22,276
Do you remember when?

172
00:30:30,320 --> 00:30:32,276
Aren't you scared?

173
00:30:34,120 --> 00:30:35,348
Sorry?

174
00:30:35,560 --> 00:30:37,915
Aren't you scared of that apartment?

175
00:30:38,120 --> 00:30:40,076
Why?

176
00:30:42,680 --> 00:30:46,389
all the women next door
died, you know.

177
00:30:49,440 --> 00:30:51,396
Why?

178
00:31:34,000 --> 00:31:38,152
Yeah, I'll stay here cleaning up
her stuff tonight.

179
00:31:39,320 --> 00:31:43,916
I have to be here tomorrow when they
come to take out the furniture.

180
00:31:46,880 --> 00:31:50,111
Well, yeah, but I'm tired, too.

181
00:31:50,600 --> 00:31:54,752
Mother, stop just thinking of yourself
all the time, will you?

182
00:31:57,280 --> 00:31:59,669
Oh, the police gave me
her phone back.

183
00:31:59,880 --> 00:32:02,599
They said they didn't need it anymore.

184
00:32:04,640 --> 00:32:08,679
Yes, they're calling it a suicide.

185
00:32:11,840 --> 00:32:14,559
Okay, bye.
You get some sleep, all right?

186
00:33:35,520 --> 00:33:37,476
Sayaka!

187
00:33:42,360 --> 00:33:46,433
Sayaka, is there something
you want to tell me?

188
00:34:04,440 --> 00:34:06,396
Sayaka...

189
00:39:50,320 --> 00:39:53,869
The Kanto region was shaken by an
earthquake at around 2:40 a.m.

190
00:39:54,560 --> 00:39:56,516
The epicenter was below Tokyo Bay,

191
00:39:56,720 --> 00:39:58,597
and it had a magnitude
of 4.6 on the Richter scale.

192
00:39:58,800 --> 00:39:59,755
An earthquake?

193
00:39:59,960 --> 00:40:04,476
On the Japanese scale, it registered
four in western Kanagawa Prefecture.

194
00:40:04,680 --> 00:40:07,672
Is that all?

195
00:41:37,400 --> 00:41:40,551
That looks fine.

196
00:41:46,080 --> 00:41:49,390
I'll take the key then.

197
00:41:59,040 --> 00:42:01,076
Thank you.

198
00:42:01,280 --> 00:42:04,192
Um...

199
00:42:04,400 --> 00:42:06,755
Yes?
Is there anything else?

200
00:42:11,720 --> 00:42:15,315
No.
Sorry to trouble you.

201
00:42:16,920 --> 00:42:19,593
this wasn't my sister's. Could I ask
you to take it away for me?

202
00:43:05,120 --> 00:43:06,917
Take good care of the teddy bear,
will you?

203
00:43:07,120 --> 00:43:10,237
My little sister treasured it.

204
00:43:11,400 --> 00:43:16,679
The big sister who gave me this
is still next door, you know.

205
00:43:19,640 --> 00:43:20,993
What do you mean?

206
00:44:11,840 --> 00:44:15,310
Detective Seiichi Sakurai

207
00:44:15,520 --> 00:44:17,556
The Kanagawa Prefecture Police

208
00:44:25,120 --> 00:44:33,073
Blue Palace Hiraoka Serial Suicides
Autopsy Report

209
00:44:36,440 --> 00:44:44,028
July 18th: Midorigawa Sayaka, 21,

210
00:44:44,240 --> 00:44:49,189
jumped from balcony, suicide

211
00:44:54,040 --> 00:44:57,999
Last year, September 21st:
Kawabara Yuka, 21,

212
00:44:58,200 --> 00:45:03,558
jumped from balcony, suicide

213
00:45:05,600 --> 00:45:07,716
Last year, February 15th:
Masuda Sanae, 24,

214
00:45:07,920 --> 00:45:09,433
committed suicide
by jumping from balcony

215
00:45:09,640 --> 00:45:10,993
Two years ago, November 30th
Endo Michiru, 26, committed suicide

216
00:45:11,200 --> 00:45:13,031
by jumping from balcony

217
00:45:13,240 --> 00:45:17,631
Two years ago, May 15th: Sugiuchi
Yukiyo, 29, committed suicide.

218
00:45:17,840 --> 00:45:20,912
So your sister is the fifth?

219
00:45:25,920 --> 00:45:29,435
this might take a while to explain.
Do you have time?

220
00:45:34,440 --> 00:45:37,193
Profiling a bizarre murderer: Hatred of
a too tightly organized society

221
00:45:37,400 --> 00:45:40,312
Mother's decomposed body
left in apartment of woman suicide

222
00:45:40,520 --> 00:45:42,431
Mother's body found
in woman's home

223
00:45:42,640 --> 00:45:43,789
Mummified Mummy!

224
00:45:44,000 --> 00:45:45,638
The tragic tale of a mother
and daughter in a seaside condo

225
00:45:45,840 --> 00:45:47,751
daughter jumps to death
from 13th floor

226
00:45:59,440 --> 00:46:04,753
Did this mother and daughter
live in that apartment?

227
00:46:06,200 --> 00:46:08,156
That's correct.

228
00:46:11,720 --> 00:46:14,473
After Sugiuchi Yukiyo died,
the body of her mother,

229
00:46:14,680 --> 00:46:18,036
Sugiuchi Sachiko, was found
in a closet in the tatami room.

230
00:46:18,240 --> 00:46:21,676
The body had been there for six
months Since the woman had died.

231
00:46:23,680 --> 00:46:25,636
in the closet?

232
00:46:28,000 --> 00:46:30,833
The story's a real tragedy.

233
00:46:31,440 --> 00:46:35,228
Several books came out
about it afterwards.

234
00:46:37,840 --> 00:46:42,152
It would appear that Yukiyo had
always been abused by her mother.

235
00:46:48,960 --> 00:46:51,952
She wasn't a heavy-on-the-makeup
kind of woman.

236
00:46:52,160 --> 00:46:56,153
She was very quiet,
and her clothes were real drab.

237
00:46:56,360 --> 00:46:59,670
As soon as work was done,
she'd head straight home.

238
00:46:59,880 --> 00:47:02,519
It was like she didn't want to mix
with people very much.

239
00:47:02,720 --> 00:47:05,632
She'd never come to Company
parties or events.

240
00:47:05,840 --> 00:47:09,674
I never knew she was looking
after her mother or anything.

241
00:47:09,880 --> 00:47:12,952
Her mother always made her a lunch,

242
00:47:13,160 --> 00:47:15,355
and she'd go out
on the roof and eat alone.

243
00:47:15,560 --> 00:47:18,393
I don't think she had, like,
a friend or anyone like that.

244
00:47:18,600 --> 00:47:24,357
A boyfriend?
I really doubt that.

245
00:47:24,560 --> 00:47:29,031
But suddenly she surprised everyone
by saying she was going to get

246
00:47:29,240 --> 00:47:33,756
her ears pierced. I remember
how happy she seemed then.

247
00:47:35,280 --> 00:47:37,236
After she separated
from her husband,

248
00:47:37,440 --> 00:47:39,908
Sugiuchi Sachiko apparently
lived a pretty wild life.

249
00:47:41,160 --> 00:47:44,311
The mother seems
to have been an alcoholic.

250
00:47:44,520 --> 00:47:46,636
When she was drunk,
there was no stopping her,

251
00:47:46,840 --> 00:47:50,958
and in the end
she had gone completely crazy.

252
00:47:51,160 --> 00:47:54,118
She abused her daughter
Yukiyo her whole life.

253
00:47:54,720 --> 00:47:58,190
Even made her eat dog food,
it would seem.

254
00:47:59,720 --> 00:48:01,438
That's horrible.

255
00:48:01,640 --> 00:48:06,430
Including the mother, Sugiuchi Sachiko,
six people have died in Unit 1303.

256
00:48:06,640 --> 00:48:09,473
That's in only three years.

257
00:48:09,680 --> 00:48:13,639
It's too many people to clear
it all away under coincidence.

258
00:48:15,600 --> 00:48:17,113
Miss Midorigawa,

259
00:48:17,320 --> 00:48:22,189
do you think there's any chance your
younger sister committed suicide?

260
00:48:23,080 --> 00:48:25,355
No. Definitely not.
Of course not.

261
00:48:26,200 --> 00:48:30,796
Any way you look at it,
it's not natural that any of the women

262
00:48:31,000 --> 00:48:33,434
who died here
would have committed suicide.

263
00:48:34,520 --> 00:48:38,195
But that's what the police
have called it.

264
00:48:38,400 --> 00:48:42,632
There's absolutely no evidence
of a mishap or foul play.

265
00:48:43,960 --> 00:48:46,315
As far as the police are concerned,

266
00:48:46,520 --> 00:48:49,557
there's no other conclusion
to draw but suicide.

267
00:48:54,360 --> 00:48:59,275
I found this in my sister's apartment,
but I don't think it's hers.

268
00:48:59,480 --> 00:49:01,596
You can have it.

269
00:49:01,800 --> 00:49:03,756
Fine.

270
00:49:14,360 --> 00:49:18,717
MIDORlGAWA

271
00:49:33,960 --> 00:49:35,916
I'm back.

272
00:49:56,880 --> 00:49:58,836
Mother?

273
00:50:15,400 --> 00:50:17,789
Mother!
What are you doing?!

274
00:50:18,000 --> 00:50:22,118
Mariko, give Sayaka back.

275
00:50:23,320 --> 00:50:25,276
Mother!

276
00:50:25,480 --> 00:50:27,755
Stop it! Let go!

277
00:50:31,200 --> 00:50:38,880
She's all alone in that room, she says.
It hurts, she says.

278
00:50:39,600 --> 00:50:42,751
It's raining and she's cold, she says.

279
00:50:44,280 --> 00:50:46,555
I got some blankets ready.

280
00:50:46,760 --> 00:50:51,231
You've got to go out there.
You've got to go out there for her!

281
00:51:05,160 --> 00:51:08,277
Mother, please!
Stop this!

282
00:51:08,480 --> 00:51:12,473
Sayaka!
Sayaka!

283
00:51:13,040 --> 00:51:17,875
Mother, Sayaka's not here anymore.

284
00:51:18,080 --> 00:51:22,835
- She's dead.
- No! Give Sayaka back!

285
00:51:26,440 --> 00:51:28,396
Mother...

286
00:51:34,560 --> 00:51:36,630
I'm still here.

287
00:51:40,640 --> 00:51:42,596
Why won't you see me?

288
00:51:43,880 --> 00:51:46,189
I'm just as hurt as you are.

289
00:51:50,640 --> 00:51:58,149
We can weep and wail all we want,
but it still won't bring Sayaka back.

290
00:51:59,880 --> 00:52:02,872
You've always been that way.

291
00:52:04,360 --> 00:52:06,316
What?

292
00:52:12,400 --> 00:52:15,472
Always blaming me, always critical...

293
00:52:17,400 --> 00:52:19,516
that's why I hate you.

294
00:52:25,600 --> 00:52:27,591
Why?

295
00:52:29,960 --> 00:52:32,076
Why would you say such a thing?

296
00:52:39,680 --> 00:52:41,636
That's awful!

297
00:52:48,000 --> 00:52:51,436
That's awful, Mother.

298
00:53:43,200 --> 00:53:45,395
The lamentation of Unit 1303:

299
00:53:48,400 --> 00:53:51,597
The love-hate hell of a Mother
daughter alone by the Sea

300
00:53:55,560 --> 00:53:57,516
Half a year later she was dead.
With the corpse of her mother beside her,

301
00:53:57,720 --> 00:53:59,676
What was the daughter thinking?

302
00:53:59,880 --> 00:54:03,190
Now we know the hair-raising truth,
sought since the incident occurred.

303
00:54:22,360 --> 00:54:24,316
July 18th
The Rotten Mother

304
00:54:24,520 --> 00:54:26,750
Pressure and Release
Finding Photographs

305
00:54:26,960 --> 00:54:28,916
Hatred of Society
Revenge

306
00:54:29,120 --> 00:54:30,269
Suicide
A Strange Way to live

307
00:54:52,000 --> 00:54:57,393
Yukiyo, this is our new home.

308
00:55:29,040 --> 00:55:31,918
Happy birthday.
Now blow out the candles.

309
00:55:39,240 --> 00:55:41,196
I'm back.

310
00:55:45,200 --> 00:55:47,156
Mother...

311
00:55:52,600 --> 00:55:55,114
Oh, you're back.

312
00:55:56,760 --> 00:56:00,036
Isn't that nice?
You were out having fun.

313
00:56:15,960 --> 00:56:19,430
What's all this about?

314
00:56:19,640 --> 00:56:23,269
My stomach was hurting a little.

315
00:56:25,080 --> 00:56:27,913
That's what you said last time.

316
00:56:30,520 --> 00:56:32,875
Don't you play stupid games with me!

317
00:56:33,320 --> 00:56:39,953
If you don't like
what your mother makes for you,

318
00:56:40,160 --> 00:56:43,869
then you can eat this!
There! Eat!

319
00:57:37,360 --> 00:57:40,033
I'm back.

320
00:58:32,600 --> 00:58:39,915
Die... die... die...

321
00:58:44,600 --> 00:58:47,160
What shall we play?

322
00:59:26,200 --> 00:59:28,760
What are those?

323
00:59:31,600 --> 00:59:39,518
How dare you defile the body
I gave you!

324
00:59:41,040 --> 00:59:43,634
What kind of a daughter
are you, anyway?!

325
00:59:43,840 --> 00:59:47,310
Ow! Stop it!

326
00:59:50,200 --> 00:59:52,156
Stop it, Mother!

327
01:00:02,520 --> 01:00:04,715
What are you doing?!
Yukiyo!

328
01:00:05,560 --> 01:00:07,915
What are you doing?!

329
01:00:19,360 --> 01:00:22,511
Murderer! Murderer!

330
01:00:31,360 --> 01:00:36,150
Mother... I'll always look after you.

331
01:00:38,280 --> 01:00:39,554
Please, Mother!

332
01:00:57,320 --> 01:01:00,073
Yukiyo...

333
01:01:23,400 --> 01:01:26,517
I'm sorry.
I'm sorry. Mother.

334
01:01:34,760 --> 01:01:37,320
Miss Sugiuchi!

335
01:01:37,920 --> 01:01:40,912
I know you're in there!

336
01:01:41,800 --> 01:01:44,792
Miss Sugiuchi!

337
01:01:46,000 --> 01:01:48,798
I know you're in there.

338
01:01:56,480 --> 01:02:00,473
Miss Sugiuchi, you're six months
late on your rent.

339
01:02:00,680 --> 01:02:04,195
I'm afraid I have to ask you
and your mother to leave.

340
01:02:04,400 --> 01:02:08,393
See? this is an eviction notice
from the Company.

341
01:02:09,200 --> 01:02:12,875
I'd like you out by the day
after tomorrow, if you don't mind.

342
01:02:13,080 --> 01:02:15,833
But where am I supposed to go?

343
01:02:16,040 --> 01:02:17,996
I don't have anywhere else to go.

344
01:02:19,040 --> 01:02:22,874
this is the only place
for my mother and me.

345
01:02:23,080 --> 01:02:28,279
If you want to live here,
you have to pay the rent.

346
01:02:28,480 --> 01:02:34,032
If you don't leave,
we'll use force and drive you out!

347
01:02:34,240 --> 01:02:37,073
That's what the Company says.

348
01:02:44,040 --> 01:02:48,875
If you need to earn some money,
there's lots of different ways to do it.

349
01:02:54,520 --> 01:02:57,159
You know what you
have to do, don't you?

350
01:02:58,880 --> 01:03:00,836
Mother...

351
01:03:03,520 --> 01:03:08,958
Yukiyo, you really are
a useless little girl, you know.

352
01:03:09,840 --> 01:03:14,550
You can't survive if I'm not here.

353
01:03:15,680 --> 01:03:17,750
You're miserable.

354
01:03:20,480 --> 01:03:22,596
That's not true.

355
01:03:25,000 --> 01:03:29,915
I'm quite happy
living quietly here with you this way.

356
01:06:10,200 --> 01:06:12,919
UNKNOWN CALLER

357
01:06:16,440 --> 01:06:18,396
Hello?

358
01:06:26,320 --> 01:06:31,633
Sis? I'm scared.

359
01:06:32,800 --> 01:06:36,031
Sayaka?!
Is that you?!

360
01:07:16,640 --> 01:07:18,596
Mother?

361
01:07:24,040 --> 01:07:25,996
Mother!

362
01:07:35,560 --> 01:07:38,120
I'm going to where Sayaka is.
Mother.

363
01:07:39,080 --> 01:07:41,230
To where Sayaka is?

364
01:07:53,480 --> 01:07:55,436
Welcome!

365
01:07:55,640 --> 01:07:59,349
Cold beer! Cold beer!

366
01:08:03,480 --> 01:08:05,436
It's so hot!

367
01:08:05,640 --> 01:08:08,108
It's hard work dressing up
like a maid at the beach, isn't it?

368
01:08:08,320 --> 01:08:10,675
Yeah, but our shop's
number one in sales, you know.

369
01:08:10,880 --> 01:08:13,713
- Seriously?
- Seriously. The manager's ecstatic.

370
01:08:13,920 --> 01:08:15,353
Great!

371
01:08:15,560 --> 01:08:20,076
I thought the swimsuit place
would beat us out, though.

372
01:08:20,280 --> 01:08:22,236
Thanks.

373
01:08:22,440 --> 01:08:23,668
Hey, you're coming
to that party tonight, aren't you?

374
01:08:23,880 --> 01:08:25,552
Sure! 6:00, right?

375
01:08:25,760 --> 01:08:29,150
Where was it again?

376
01:08:29,360 --> 01:08:32,318
The Blue Palace Hiraoka, it says.

377
01:08:32,520 --> 01:08:34,476
What, it's not at a bar?

378
01:08:34,680 --> 01:08:37,433
In a condo?
Isn't that sort of suspicious?

379
01:08:40,160 --> 01:08:43,232
What?
There's someone in there already?!

380
01:08:44,600 --> 01:08:47,956
Yep. Just for the season, though.

381
01:08:48,160 --> 01:08:53,154
They said it was a group of students
working at the beach for the summer.

382
01:08:53,360 --> 01:08:54,873
I see...

383
01:08:55,080 --> 01:08:57,878
Well, I think it's a bit fast,

384
01:08:58,080 --> 01:08:59,798
but the Company can't just
let it sit empty like that.

385
01:09:00,000 --> 01:09:01,956
Um...

386
01:09:04,240 --> 01:09:06,879
- Are they girls?
- I'm sorry?

387
01:09:07,080 --> 01:09:08,433
These students.

388
01:09:09,080 --> 01:09:12,390
Oh no.

389
01:09:14,480 --> 01:09:17,950
Boys. All of them.

390
01:09:20,120 --> 01:09:22,236
I see.

391
01:10:01,280 --> 01:10:04,192
Terribly sorry to keep you waiting.

392
01:10:11,800 --> 01:10:15,998
You've already got new people
in that apartment, don't you?

393
01:10:16,200 --> 01:10:18,555
Oh,

394
01:10:18,760 --> 01:10:22,116
that's a seasonal rental.

395
01:10:22,320 --> 01:10:23,878
They're only temporary.

396
01:10:25,360 --> 01:10:28,591
Several people have committed
suicide in the apartment, haven't they?

397
01:10:28,800 --> 01:10:31,268
Not just my sister.

398
01:10:33,320 --> 01:10:38,269
I haven't really
heard anything about that...

399
01:10:40,280 --> 01:10:44,193
You didn't tell my sister
about that, did you?

400
01:10:44,400 --> 01:10:46,356
Mr. Kawakami!

401
01:10:46,560 --> 01:10:50,553
Did you call Mr. Kobayashi on that
Blue Palace condo thing for me?

402
01:10:50,760 --> 01:10:53,035
Oh, yeah.
What was it about?

403
01:10:53,240 --> 01:10:54,992
The wallpaper color.

404
01:10:55,200 --> 01:10:58,909
Oh, right.
I'll get right on it.

405
01:11:01,520 --> 01:11:05,718
Anyway, I'm afraid I'm not the person
to talk to on that.

406
01:11:09,440 --> 01:11:12,113
You don't think there's anything
wrong with renting out an apartment

407
01:11:12,320 --> 01:11:15,392
where people have died and not
saying anything about it?!

408
01:11:59,120 --> 01:12:01,315
Ow!
What the hell?

409
01:13:28,480 --> 01:13:31,552
Chug! Chug! Chug!

410
01:13:35,280 --> 01:13:37,032
What's the matter?

411
01:13:37,240 --> 01:13:39,674
I just have the feeling
that there's someone watching me.

412
01:13:39,880 --> 01:13:41,677
Don't say things like that!

413
01:13:41,880 --> 01:13:43,598
Sorry, sorry.

414
01:13:47,600 --> 01:13:51,434
I read a book about Sugiuchi Yukiyo.

415
01:13:51,640 --> 01:13:54,598
Oh, you did?

416
01:13:55,320 --> 01:13:59,279
There were parts of it that
I felt were almost about me.

417
01:14:01,080 --> 01:14:03,719
My mother started suffering
from depression

418
01:14:03,920 --> 01:14:06,559
right after my father died.

419
01:14:06,760 --> 01:14:09,115
She did, did she?

420
01:14:10,600 --> 01:14:14,673
I always intended to do everything
I could to support her,

421
01:14:14,880 --> 01:14:17,599
but somewhere along the line
things just went bad between us.

422
01:14:17,800 --> 01:14:20,598
It was like my younger sister was
the only one she saw anymore.

423
01:14:20,800 --> 01:14:25,635
So once she was gone,
our house would have been...

424
01:14:34,360 --> 01:14:36,669
I'm so tired.

425
01:14:49,960 --> 01:14:56,308
I've spent far too long worrying
about my mother.

426
01:15:10,560 --> 01:15:13,120
I'm sorry.

427
01:15:13,320 --> 01:15:16,198
What am I saying?

428
01:15:16,400 --> 01:15:18,152
That's all right.

429
01:15:18,360 --> 01:15:21,113
A cop hears all sorts of things.

430
01:15:26,000 --> 01:15:28,195
By the way, about these...

431
01:15:28,400 --> 01:15:30,436
- These are...
- That's right.

432
01:15:30,640 --> 01:15:32,631
this is the one you picked up
in that apartment,

433
01:15:32,840 --> 01:15:36,389
and this one was found
with Sugiuchi Yukiyo's body.

434
01:15:36,600 --> 01:15:39,068
I knew it!
It was in the book.

435
01:15:39,280 --> 01:15:41,714
It said there was a wound
on her body,

436
01:15:41,920 --> 01:15:43,956
as if an earring had been
pulled out of her ear.

437
01:15:44,160 --> 01:15:46,799
And then did you know all those girls
who committed suicide

438
01:15:47,000 --> 01:15:48,319
had something in common?

439
01:15:48,520 --> 01:15:50,272
What is it?

440
01:15:50,480 --> 01:15:53,313
Since Sugiuchi Yukiyo,
all of the girls

441
01:15:53,520 --> 01:15:56,592
who've moved into Unit 1303
have just left home and begun to live

442
01:15:56,800 --> 01:16:00,634
alone for the first time.

443
01:16:00,840 --> 01:16:02,796
What difference does that make?

444
01:16:04,360 --> 01:16:07,636
Nothing, probably.

445
01:16:07,840 --> 01:16:12,550
It's just that you're still living
with your mother.

446
01:16:18,920 --> 01:16:22,708
Don't worry.
I won't be jumping.

447
01:16:22,920 --> 01:16:25,639
No, I don't think so.

448
01:16:34,800 --> 01:16:36,438
What is it?

449
01:16:48,120 --> 01:16:49,678
Miss Midorigawa?

450
01:17:01,200 --> 01:17:04,510
Could you let me
hold onto those earrings?

451
01:17:05,880 --> 01:17:08,155
Hold onto them?

452
01:17:08,360 --> 01:17:10,191
Yes.

453
01:17:16,040 --> 01:17:19,191
The gyu-tan game!
Yay!

454
01:17:19,400 --> 01:17:21,118
- Gyu!
- Gyu!

455
01:17:21,320 --> 01:17:22,639
Gyu!

456
01:17:22,840 --> 01:17:24,592
- Gyu!
- Gyu!

457
01:17:24,800 --> 01:17:26,870
Gyu.

458
01:17:27,080 --> 01:17:28,991
- Gyu!
- Gyu!

459
01:17:29,200 --> 01:17:30,553
Gyu!

460
01:17:30,760 --> 01:17:33,513
- Gyu!
- Wrong!

461
01:17:33,720 --> 01:17:35,358
Okay, you lose.

462
01:17:35,560 --> 01:17:38,028
Chug! Chug! Chug!

463
01:17:39,400 --> 01:17:42,358
What happened?
Did the power go out?

464
01:17:42,560 --> 01:17:44,437
What?

465
01:17:44,640 --> 01:17:45,993
What's the matter?

466
01:17:46,200 --> 01:17:48,509
Breaker!
Go look at the fuse box.

467
01:17:48,720 --> 01:17:50,153
What's that smell?

468
01:17:50,360 --> 01:17:52,749
Something stinks!
What's that smell?

469
01:19:20,880 --> 01:19:22,711
What's the matter?

470
01:19:22,920 --> 01:19:24,512
Sorry?

471
01:19:24,720 --> 01:19:26,915
You look a little pale.

472
01:19:27,120 --> 01:19:30,112
Oh... no, I'm fine.

473
01:19:30,320 --> 01:19:32,311
Would you like to go back
to the condominium?

474
01:19:32,520 --> 01:19:35,159
No, Thanks. I've already spoken
to the superintendent.

475
01:19:35,360 --> 01:19:39,717
And it looks like there's already
someone else in that apartment.

476
01:19:39,920 --> 01:19:43,913
She may already
be back home, anyway.

477
01:19:44,120 --> 01:19:46,554
We'll keep an eye out
for your mother here.

478
01:19:48,240 --> 01:19:51,312
It might be a good idea
if you checked back home.

479
01:19:51,520 --> 01:19:53,909
Yes, I'll do that.

480
01:19:56,400 --> 01:20:00,996
I'm sorry about all this, Mr. Sakurai.

481
01:20:02,440 --> 01:20:04,908
You've had a rough time,
too, haven't you?

482
01:20:29,360 --> 01:20:32,193
- Who? Who is it?
- I'm afraid I wouldn't know.

483
01:20:32,400 --> 01:20:35,039
What are you doing?!
Stop that!

484
01:20:37,640 --> 01:20:41,952
Yes, that's right.
It looks like three people have jumped

485
01:20:42,160 --> 01:20:44,196
from a balcony on the 13th floor.

486
01:21:39,720 --> 01:21:41,790
Mother!

487
01:21:53,760 --> 01:21:57,799
Mother, what are you doing here?

488
01:22:14,800 --> 01:22:18,759
Die!
Die! Die! Die! Die! Die!

489
01:22:33,440 --> 01:22:35,556
Mother!

490
01:22:59,800 --> 01:23:01,518
Mother!

491
01:23:56,800 --> 01:23:59,394
You're Yukiyo, aren't you?

492
01:24:15,800 --> 01:24:17,995
Why?!

493
01:24:20,520 --> 01:24:23,432
Why did you kill my sister?!

494
01:24:34,240 --> 01:24:36,754
My sister...

495
01:24:38,120 --> 01:24:40,554
all those other girls you killed
in this apartment...

496
01:24:40,760 --> 01:24:43,752
what did they ever do?!

497
01:25:20,800 --> 01:25:24,190
Hang on!
You can't fall!

498
01:25:27,200 --> 01:25:29,191
You mustn't fall!

499
01:25:30,240 --> 01:25:33,357
Please! Come up!
You can't fall!

500
01:25:44,200 --> 01:25:46,634
That's right!
You can do it!

501
01:27:44,200 --> 01:27:46,509
No!

502
01:28:32,240 --> 01:28:36,995
Why does everyone just come
walking into my home?

503
01:28:38,160 --> 01:28:39,718
What?

504
01:28:40,920 --> 01:28:44,879
Mother said I wasn't to let
anyone in here;

505
01:28:45,080 --> 01:28:49,358
that everybody else was evil.
Mother was right.

506
01:28:49,560 --> 01:28:53,553
But I let other people in here.
all those girls...

507
01:28:53,760 --> 01:28:55,432
Mother threw them out.

508
01:28:55,640 --> 01:29:00,156
They were evil, so she told me to kill
them-- kill them all! kill them!

509
01:29:04,800 --> 01:29:07,917
These are yours, aren't they?

510
01:29:14,440 --> 01:29:18,228
My earrings.

511
01:29:18,440 --> 01:29:21,512
Yukiyo, stop this!

512
01:29:21,720 --> 01:29:26,157
I'll look after you and your mother's
apartment for you.

513
01:29:26,360 --> 01:29:29,591
- You're lying!
- It's true! I'll live here.

514
01:29:29,800 --> 01:29:32,633
I'll live here together with my mother!

515
01:29:34,280 --> 01:29:37,431
Won't that be enough?

516
01:29:40,320 --> 01:29:43,118
Yukiyo...

517
01:29:44,240 --> 01:29:48,870
you'll be my younger sister.

518
01:30:30,720 --> 01:30:33,632
Miss Midorigawa!
Miss Midorigawa!

519
01:30:39,800 --> 01:30:41,870
Miss Midorigawa!

520
01:30:42,960 --> 01:30:44,951
Miss Midorigawa!

521
01:31:56,160 --> 01:31:59,755
Yukiyo! No!

522
01:32:01,040 --> 01:32:04,510
My mother only has
one girl... me!

523
01:32:39,200 --> 01:32:41,589
Sis...

524
01:32:41,800 --> 01:32:44,234
Sayaka.

525
01:32:45,920 --> 01:32:47,911
Miss Midorigawa!

526
01:33:22,280 --> 01:33:24,430
There goes another one.

