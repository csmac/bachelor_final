1
00:01:50,360 --> 00:01:53,160
Don't go to sleep.
He's waiting for you.

2
00:01:53,320 --> 00:01:54,520
Don't go to sleep.

3
00:03:06,960 --> 00:03:09,040
Hey! What's going on?

4
00:03:10,520 --> 00:03:11,880
Oh, man.

5
00:03:12,960 --> 00:03:15,080
-Well?
-He's here.

6
00:03:15,400 --> 00:03:17,240
Who's here?

7
00:03:18,120 --> 00:03:19,720
He came out of the closet.

8
00:03:19,960 --> 00:03:22,640
Tim, nobody's here.
What are you talking about?

9
00:03:26,440 --> 00:03:27,600
Him.

10
00:03:30,560 --> 00:03:31,760
Okay.

11
00:03:36,000 --> 00:03:37,720
Tim.

12
00:03:38,240 --> 00:03:40,520
It's just a story. Okay? He's not real.

13
00:03:45,200 --> 00:03:47,480
Well, let's have a look around, then,
shall we?

14
00:03:51,440 --> 00:03:52,720
No.

15
00:03:59,440 --> 00:04:00,840
Nothing back here.

16
00:04:05,920 --> 00:04:07,400
Okay.

17
00:04:10,280 --> 00:04:11,920
Hello.

18
00:04:16,080 --> 00:04:17,240
Nope.

19
00:04:19,080 --> 00:04:20,840
Nobody home.

20
00:04:22,160 --> 00:04:23,440
Just us.

21
00:04:44,480 --> 00:04:46,120
No!

22
00:05:17,560 --> 00:05:19,560
This is gonna get ugly.

23
00:05:20,240 --> 00:05:22,360
No more vodka.

24
00:05:22,800 --> 00:05:25,800
Hey, Tim. Let me have
one of those green things.

25
00:05:28,120 --> 00:05:30,680
Careful. No more for Pam.

26
00:05:30,880 --> 00:05:33,520
Hey. It's my party.

27
00:05:33,680 --> 00:05:35,280
You still on for this weekend?

28
00:05:35,440 --> 00:05:37,880
No, thought I'd wrangle up
a dinner invitation...

29
00:05:38,040 --> 00:05:41,440
...from one of those art-department
girls. Like this one right here.

30
00:05:41,640 --> 00:05:43,400
-Here you go.
-Oh, hey, thank you.

31
00:05:43,560 --> 00:05:44,720
-Thank you.
-Thank you.

32
00:05:44,880 --> 00:05:48,800
Hey, Scotty, did Tim tell you about
meeting my parents tomorrow?

33
00:05:48,960 --> 00:05:50,720
Meeting the folks? Sounds serious.

34
00:05:50,880 --> 00:05:53,760
Yeah, if he can survive the weekend
with Jessica's father.

35
00:05:53,960 --> 00:05:56,880
Oh, come on. Dad's not that bad.
Don't listen to that.

36
00:05:57,040 --> 00:05:59,920
-He tried to drown you.
-You shouldn't have told her.

37
00:06:00,080 --> 00:06:02,080
I was 7.
He was teaching me to swim.

38
00:06:02,240 --> 00:06:03,720
By throwing you in a lake?

39
00:06:03,920 --> 00:06:06,520
You know, this might be
a good time for a toast.

40
00:06:06,680 --> 00:06:08,520
-Yeah?
-Yeah. I'd say.

41
00:06:08,680 --> 00:06:10,320
Excuse me, everyone.

42
00:06:10,480 --> 00:06:12,800
When we come back
from Thanksgiving on Monday...

43
00:06:12,960 --> 00:06:14,640
...things will be a lot different.

44
00:06:14,800 --> 00:06:16,160
For starters...

45
00:06:16,320 --> 00:06:18,480
...it'll be a lot quieter.

46
00:06:18,760 --> 00:06:20,760
But it definitely won't be as much fun.

47
00:06:21,120 --> 00:06:23,320
The Daily News is lucky to get you.

48
00:06:23,480 --> 00:06:25,320
Cheers.

49
00:06:29,720 --> 00:06:31,960
Okay, break it up.

50
00:06:32,120 --> 00:06:34,160
-Be good, Jess.
-I will.

51
00:06:34,320 --> 00:06:36,000
I'm gonna miss you.

52
00:06:36,200 --> 00:06:38,080
All of you.

53
00:06:38,560 --> 00:06:40,760
I'm gonna miss you too.

54
00:06:42,200 --> 00:06:45,120
So I told my parents I'd be there by 11.

55
00:06:45,280 --> 00:06:47,280
All right, I'll get your coat.

56
00:06:47,520 --> 00:06:49,240
Thank you.

57
00:07:16,680 --> 00:07:17,880
Hey.

58
00:07:18,040 --> 00:07:19,600
-Hey, Jess.
-Hey.

59
00:07:23,040 --> 00:07:25,000
Not getting weird on me again,
are you?

60
00:07:25,160 --> 00:07:29,040
No, no. I just spaced out for a second.

61
00:07:32,640 --> 00:07:34,080
Okay.

62
00:07:34,880 --> 00:07:36,040
Where's your car?

63
00:07:36,200 --> 00:07:37,400
It wouldn't start.

64
00:07:37,560 --> 00:07:39,880
-Is it gonna make it?
-I got a guy looking at it.

65
00:07:40,040 --> 00:07:42,720
If you think you can use
the "car broke down" excuse...

66
00:07:42,920 --> 00:07:46,240
-...to get out of this, you're mistaken.
-I'll be there, I promise.

67
00:07:46,440 --> 00:07:47,880
Okay.

68
00:07:48,800 --> 00:07:50,000
So you want a ride home?

69
00:07:50,560 --> 00:07:52,960
No, I'm good. I'll cut through the park.

70
00:07:53,120 --> 00:07:56,280
-Besides, I've seen your driving.
-Yeah, okay.

71
00:07:59,040 --> 00:08:01,840
-Be careful.
-I'll be fine.

72
00:08:06,680 --> 00:08:08,440
See you tomorrow.

73
00:08:52,960 --> 00:08:55,480
I don't--
Who would have done this?

74
00:08:55,680 --> 00:08:57,080
Who would've taken...?

75
00:08:57,240 --> 00:08:59,440
Who would--?
Who would take him?

76
00:08:59,600 --> 00:09:02,920
Why would they do this?
Where is he?

77
00:10:01,520 --> 00:10:03,520
Tim, it's Uncle Mike.

78
00:10:03,680 --> 00:10:07,520
I know you're having Thanksgiving
with your girlfriend and all...

79
00:10:07,680 --> 00:10:11,680
...but it'd be nice if you could make it
out to see your mom this weekend.

80
00:10:11,840 --> 00:10:16,000
She's not doing so well, and, you
know, it'd be good for her to see you.

81
00:10:16,160 --> 00:10:21,680
And, hey, I got some stuff
for you to sign too, house stuff.

82
00:10:21,840 --> 00:10:24,840
All right, hope I see you this weekend.

83
00:10:50,800 --> 00:10:53,040
-Hey, you made it.
-Hey.

84
00:10:53,240 --> 00:10:54,760
Hey.

85
00:10:55,840 --> 00:10:58,560
-Were the directions okay?
-Perfect. Here.

86
00:10:58,720 --> 00:10:59,920
-Come on.
-Wait, wait.

87
00:11:00,080 --> 00:11:02,480
Shouldn't we wait for the bellhop
or something?

88
00:11:02,640 --> 00:11:05,960
Shut up. It's their house, not mine.

89
00:11:06,120 --> 00:11:07,520
What's with the tie?

90
00:11:07,680 --> 00:11:10,240
I figured, meeting the family.
Why not, right?

91
00:11:10,400 --> 00:11:13,840
Come on. Let's put your stuff
in the guestroom.

92
00:11:14,040 --> 00:11:17,040
So Jessica tells us you two met
at the magazine.

93
00:11:17,200 --> 00:11:20,960
-Are you in the art department?
-I'm an associate editor.

94
00:11:21,120 --> 00:11:24,240
-Sounds impressive.
-It's just a glorified fact-checker.

95
00:11:24,400 --> 00:11:26,760
-Tim's very good at his job.
-How about family?

96
00:11:26,920 --> 00:11:29,360
Got any family nearby?
Any brothers, sisters?

97
00:11:29,560 --> 00:11:31,600
No, sir. No brothers or sisters,
just me.

98
00:11:31,760 --> 00:11:34,000
-How about your folks?
-Let the boy eat.

99
00:11:34,200 --> 00:11:36,080
-We're just talking, Mother.
-Dad.

100
00:11:37,280 --> 00:11:39,440
The family thing's kind of
complicated, sir.

101
00:11:39,640 --> 00:11:42,000
This stuffing is delicious, Chelsea.

102
00:11:42,200 --> 00:11:45,160
Tim's had kind of a strained
relationship with his parents.

103
00:11:45,320 --> 00:11:47,640
-Tim's father ran out when he was 8.
-Jess.

104
00:11:47,800 --> 00:11:50,160
Oh, that's sad.

105
00:11:50,320 --> 00:11:54,160
Then he had to go live with his uncle,
in a tiny room in the back of his bar.

106
00:11:55,560 --> 00:11:57,200
Well, what about your mother?

107
00:11:59,080 --> 00:12:03,160
She sort of had a tough time
after my dad left.

108
00:12:04,680 --> 00:12:06,960
It was pretty hard on both of us.

109
00:12:10,000 --> 00:12:13,040
Is there anything else
you wanted to know, Dad?

110
00:12:14,080 --> 00:12:16,200
Be down in a minute, Mom.

111
00:12:16,720 --> 00:12:18,200
You have fun down there?

112
00:12:18,360 --> 00:12:20,760
Oh, come on. They could use
a little shaking up.

113
00:12:20,920 --> 00:12:22,840
You see my sister's face
when I said...

114
00:12:23,000 --> 00:12:25,480
...you lived in a tiny room
in the back of a bar?

115
00:12:25,640 --> 00:12:27,680
It wasn't in the back of a bar,
you know.

116
00:12:27,840 --> 00:12:31,280
I know. I just like messing with them.

117
00:12:31,440 --> 00:12:34,920
Is that why you wanted me here,
to freak out your family?

118
00:12:35,600 --> 00:12:37,200
No.

119
00:12:37,880 --> 00:12:39,200
No.

120
00:12:39,360 --> 00:12:41,680
Hey, I'm sorry, okay?

121
00:12:41,840 --> 00:12:43,120
Tell you what...

122
00:12:43,280 --> 00:12:45,160
...I gotta go down, say good night...

123
00:12:45,360 --> 00:12:47,760
...but I'll sneak in later
and make it up to you.

124
00:12:50,360 --> 00:12:52,040
Okay?

125
00:12:54,520 --> 00:12:56,240
In that case...

126
00:12:56,720 --> 00:12:58,800
...l'll put on something naughty.

127
00:12:59,600 --> 00:13:02,000
You do that.

128
00:15:09,160 --> 00:15:11,240
I like this sneaking-around thing.

129
00:15:11,400 --> 00:15:13,120
It's kind of dirty.

130
00:15:15,760 --> 00:15:16,960
You all right?

131
00:15:19,840 --> 00:15:22,080
Timmy.

132
00:15:22,240 --> 00:15:24,120
You can't keep running away.

133
00:15:26,280 --> 00:15:28,640
-Look at me.
-Mom?

134
00:15:28,840 --> 00:15:31,440
Why, what's the matter with you?
Look at me!

135
00:15:31,600 --> 00:15:33,120
Look at me.

136
00:15:33,320 --> 00:15:34,880
Look at me.

137
00:15:36,280 --> 00:15:38,600
You're a bad little boy.

138
00:15:41,040 --> 00:15:44,000
Tim? Baby, what are you doing
on the floor?

139
00:15:45,520 --> 00:15:47,160
Tim, what is it?

140
00:15:47,320 --> 00:15:49,200
Jessica.

141
00:15:50,160 --> 00:15:52,120
Oh, God, I had
the most messed-up dream.

142
00:15:52,640 --> 00:15:53,800
What was it?

143
00:15:54,720 --> 00:15:56,440
-I gotta go see my mom.
-Wait.

144
00:15:56,600 --> 00:15:58,160
-There's something wrong.
-What?

145
00:15:58,320 --> 00:16:00,640
-I know it.
-Tim. Tim, no. Hey.

146
00:16:00,800 --> 00:16:03,600
-Listen, let's get back in bed, and....
-I just gotta--

147
00:16:03,760 --> 00:16:06,200
-Where's my phone?
-Tim, what is wrong with you?.

148
00:16:06,360 --> 00:16:08,120
-God.
-Where's my phone?

149
00:16:08,280 --> 00:16:09,960
Just leave it.

150
00:16:11,480 --> 00:16:13,080
Hello.

151
00:16:13,240 --> 00:16:15,720
-Hey, Uncle Mike, I got your message.
-Hey.

152
00:16:15,880 --> 00:16:18,240
Is everything okay?
I'm gonna see Mom tomorrow.

153
00:16:18,400 --> 00:16:20,360
I thought you were here
for the weekend.

154
00:16:22,520 --> 00:16:24,960
-What?
-Tim.

155
00:16:26,880 --> 00:16:28,200
Okay.

156
00:16:31,400 --> 00:16:32,800
Okay.

157
00:16:34,440 --> 00:16:36,480
-Tim.
-I have to go.

158
00:16:36,640 --> 00:16:40,760
-You--? Tim, what is it?
-Look, I just have to go.

159
00:17:04,800 --> 00:17:07,240
So the funeral's today.

160
00:17:07,400 --> 00:17:09,000
Yeah, this afternoon.

161
00:17:09,160 --> 00:17:11,600
I'm so sorry, Tim.

162
00:17:11,760 --> 00:17:13,280
Me too.

163
00:17:19,680 --> 00:17:22,800
That's fantastic. That's really strong.

164
00:17:22,960 --> 00:17:24,880
What's that you've got, Peter?

165
00:17:25,080 --> 00:17:29,360
Oh, that rainbow is beautiful.
How many colors in there?

166
00:17:29,520 --> 00:17:31,920
I'm thinking of going by
the old house.

167
00:17:33,440 --> 00:17:35,800
-Really?
-My uncle's been fixing the place up...

168
00:17:36,000 --> 00:17:38,680
...to sell since Mom went
to the hospital last year.

169
00:17:38,880 --> 00:17:41,760
I felt like I should be the one
to go through her things.

170
00:17:44,840 --> 00:17:48,720
I should've done more,
but it's something, right?

171
00:17:48,880 --> 00:17:51,560
Listen to me, Tim.
Something happened in that house...

172
00:17:51,720 --> 00:17:53,240
...but it wasn't supernatural.

173
00:17:53,440 --> 00:17:55,960
There's nothing in there
but memories.

174
00:17:56,280 --> 00:18:00,400
You dealt with your father's leaving
the best way that you could.

175
00:18:00,560 --> 00:18:02,120
But you were 8.

176
00:18:02,280 --> 00:18:05,960
Tim, you've been coming here
for what, 15 years?

177
00:18:06,120 --> 00:18:07,920
Look around you.

178
00:18:08,080 --> 00:18:09,560
There's only children here.

179
00:18:11,120 --> 00:18:14,240
Dr. Matheson to Observation.

180
00:18:14,400 --> 00:18:16,960
I'm sorry. I have to go.

181
00:18:20,960 --> 00:18:22,240
Go home, Tim.

182
00:18:22,520 --> 00:18:24,200
Spend one night in that house.

183
00:18:24,360 --> 00:18:26,080
It will help.

184
00:18:44,480 --> 00:18:46,240
It's okay. It's okay.

185
00:18:46,400 --> 00:18:49,520
Hey, we need some help in here!

186
00:18:50,280 --> 00:18:53,080
Hey, the nurse is on the way.
It's okay, it's all right.

187
00:18:53,240 --> 00:18:55,200
What? What is it?

188
00:19:21,400 --> 00:19:24,080
Just relax now.
It's gonna be all right.

189
00:19:24,240 --> 00:19:26,040
It's all right.

190
00:19:26,200 --> 00:19:28,600
Everything's gonna be all right.

191
00:20:17,360 --> 00:20:20,840
And in Paul's letter
to the Corinthians, he says:

192
00:20:21,000 --> 00:20:23,280
"We shall not all fall asleep...

193
00:20:23,440 --> 00:20:25,560
...but we will all be changed...

194
00:20:25,720 --> 00:20:29,120
...in an instant, in the blink of an eye."

195
00:20:29,280 --> 00:20:32,200
"When my tongue rejoices,
my body will rest in peace...

196
00:20:32,400 --> 00:20:34,800
...because you will not abandon me
to the grave.

197
00:20:34,960 --> 00:20:38,480
For you, O Lord, have delivered
my soul to death...

198
00:20:38,640 --> 00:20:43,040
...my eyes from tears,
my feet from stumbling."

199
00:20:43,200 --> 00:20:45,360
"Thou knowest the secrets...."

200
00:20:59,600 --> 00:21:00,840
"....spare us.

201
00:21:01,040 --> 00:21:03,160
Suffer us not in our last hour."

202
00:21:03,520 --> 00:21:05,360
"Dust thou art...

203
00:21:05,520 --> 00:21:09,560
...and unto dust thou shalt return."

204
00:21:11,440 --> 00:21:13,240
Thanks for doing all this.

205
00:21:13,400 --> 00:21:15,440
She was my sister.

206
00:21:16,480 --> 00:21:18,720
When are you heading back?

207
00:21:18,880 --> 00:21:22,440
Actually, I might stay in town tonight.
In the house.

208
00:21:23,680 --> 00:21:26,240
-I thought you didn't like that house.
-Yeah, well.

209
00:21:26,400 --> 00:21:29,840
Tim, the place is a mess. I've been
doing a whole lot of work there--

210
00:21:30,000 --> 00:21:32,360
I won't mess anything up.

211
00:21:33,840 --> 00:21:35,400
Is that Katie?

212
00:21:35,600 --> 00:21:36,960
Yeah.

213
00:21:37,720 --> 00:21:39,080
How long has it been?

214
00:21:39,240 --> 00:21:41,200
-A long time.
-You should go and say hi.

215
00:21:41,360 --> 00:21:43,360
No, I don't think so.

216
00:21:44,000 --> 00:21:46,400
All right. Suit yourself.

217
00:21:47,720 --> 00:21:49,480
Hey, come here.

218
00:21:53,520 --> 00:21:56,360
Here you go. You might need these.

219
00:21:58,760 --> 00:22:01,000
Thanks for everything.

220
00:22:01,160 --> 00:22:03,040
It's good seeing you again, Tim.

221
00:27:37,240 --> 00:27:38,720
Timmy.

222
00:27:39,120 --> 00:27:42,440
You know, it's your cat,
and you're supposed to feed her.

223
00:27:45,840 --> 00:27:49,920
Come on, Timmy, just turn
the light on. I'm not coming up there.

224
00:27:50,080 --> 00:27:52,320
He's scared of his damn closet.

225
00:27:52,480 --> 00:27:54,560
You know why he's so scared,
don't you?

226
00:27:54,720 --> 00:27:56,560
-Why?
-Because you freaked him out...

227
00:27:56,720 --> 00:27:58,480
...with that stupid story of yours.

228
00:27:58,640 --> 00:28:00,760
You've gotta be kidding.
It was a story.

229
00:28:00,920 --> 00:28:03,880
-My dad did the same thing to me.
-Look how you turned out.

230
00:28:04,040 --> 00:28:07,360
-It was nothing, okay?
-He's got a very vivid imagination.

231
00:28:07,520 --> 00:28:09,440
Mary, this is ridiculous. I was just--

232
00:28:09,600 --> 00:28:12,040
You're gonna put that boy
in therapy.

233
00:28:12,200 --> 00:28:13,760
-Just calm down, okay?
-Timmy.

234
00:30:03,920 --> 00:30:05,120
Cody. Cody.

235
00:30:08,960 --> 00:30:10,160
Kate!

236
00:30:19,240 --> 00:30:20,600
Kate!

237
00:30:21,400 --> 00:30:23,280
-Cody.
-Are you all right?

238
00:30:23,440 --> 00:30:25,120
Yeah.

239
00:30:25,320 --> 00:30:28,240
I think I just hit my head.

240
00:30:29,280 --> 00:30:31,320
He's never done that before.

241
00:30:32,320 --> 00:30:35,000
Do you wanna come in,
put some ice on that?

242
00:30:35,880 --> 00:30:37,160
Sure.

243
00:30:46,280 --> 00:30:48,120
I saw you at the funeral.

244
00:30:48,280 --> 00:30:50,360
I wasn't sure you'd remember me.

245
00:30:53,280 --> 00:30:54,880
I'm real sorry about your mom.

246
00:30:56,840 --> 00:30:58,480
Yeah, me too.

247
00:31:03,000 --> 00:31:06,960
-How about that ice?
-Yeah.

248
00:31:08,400 --> 00:31:10,120
-Sorry.
-What are you trying to do?

249
00:31:10,280 --> 00:31:12,320
-I'm sorry.
-I'm in pain.

250
00:31:15,480 --> 00:31:16,800
So....

251
00:31:17,800 --> 00:31:19,520
So....

252
00:31:19,720 --> 00:31:21,360
So how's the city?

253
00:31:21,840 --> 00:31:23,800
It's all right.

254
00:31:24,000 --> 00:31:26,360
It's been a little crazy lately, but....

255
00:31:26,640 --> 00:31:28,280
You got a girlfriend?

256
00:31:30,400 --> 00:31:32,480
Actually, yeah. Yeah, I do.

257
00:31:38,120 --> 00:31:39,600
How about you?

258
00:31:40,720 --> 00:31:43,920
-Do I have a girlfriend?
-No.

259
00:31:44,080 --> 00:31:46,520
-No, I don't.
-You know what I mean.

260
00:31:46,720 --> 00:31:48,560
No, I don't have a boyfriend.

261
00:31:48,960 --> 00:31:52,000
Just me and my dad.

262
00:31:52,160 --> 00:31:55,600
I'm sure some shrink could have
a field day with that.

263
00:31:57,640 --> 00:32:00,120
So how long are you staying around?

264
00:32:00,280 --> 00:32:01,760
I think just tonight.

265
00:32:01,920 --> 00:32:05,440
I wanted to go through my mom's
things, you know, pictures and stuff.

266
00:32:05,600 --> 00:32:08,360
-I found a goofy one of you and me.
-Goofy?

267
00:32:08,520 --> 00:32:10,840
-I don't remember being goofy.
-I'll show you.

268
00:32:11,000 --> 00:32:14,720
-It's actually kind of cute.
-No, I don't think. Cute, I believe.

269
00:32:15,080 --> 00:32:18,280
Goofy, that's crazy talk.

270
00:32:18,440 --> 00:32:21,160
I mean, if you said you saw
a picture that--

271
00:32:21,320 --> 00:32:26,120
You know, that was stunning
or, you know, gorgeous...

272
00:32:26,280 --> 00:32:30,360
...okay, you know, I've heard that
a thousand times.

273
00:32:30,520 --> 00:32:32,480
He took Dad, and now he's gonna
take me.

274
00:32:32,640 --> 00:32:37,960
Timmy, nobody took your dad.
Okay?

275
00:32:41,040 --> 00:32:42,960
Did you find it?

276
00:32:43,280 --> 00:32:44,640
What?

277
00:32:44,800 --> 00:32:46,560
Goofy photo?

278
00:32:48,360 --> 00:32:49,760
Here.

279
00:32:51,240 --> 00:32:54,360
Look at us. Even you're adorable.

280
00:32:54,560 --> 00:32:56,040
Thanks.

281
00:32:56,200 --> 00:32:58,560
You know, I was crushed
when you left.

282
00:32:59,040 --> 00:33:01,440
It broke my little heart.

283
00:33:01,600 --> 00:33:04,720
And it broke my heart
you couldn't throw a baseball.

284
00:33:04,880 --> 00:33:08,160
You were the one
that was scared of closets.

285
00:33:08,320 --> 00:33:10,520
-I told you that?
-Yeah.

286
00:33:10,680 --> 00:33:12,360
I told you closets were nothing.

287
00:33:12,520 --> 00:33:16,040
It's the thing under the bed
you had to be worried about.

288
00:33:16,200 --> 00:33:19,640
All right, I have to go home
and start Dad's dinner.

289
00:33:23,320 --> 00:33:25,880
I bet you don't have any food
in the house, do you?

290
00:33:26,040 --> 00:33:28,400
-I'll come back with some.
-You don't have to.

291
00:33:28,560 --> 00:33:31,840
I know. It's frightening how
domestic I've become, though.

292
00:33:34,960 --> 00:33:36,320
Bye.

293
00:37:33,000 --> 00:37:34,480
Hello.

294
00:37:43,320 --> 00:37:44,760
Hello.

295
00:37:56,440 --> 00:37:57,880
Hey.

296
00:37:59,000 --> 00:38:00,480
You all right?

297
00:38:09,720 --> 00:38:11,240
Wanna come out?

298
00:38:31,700 --> 00:38:33,700
What were you doing in there?

299
00:38:38,220 --> 00:38:40,540
You were at my mother's funeral,
weren't you?

300
00:38:41,340 --> 00:38:43,220
My dad knew her.

301
00:38:44,540 --> 00:38:46,420
What's your name?

302
00:38:47,100 --> 00:38:48,540
Franny.

303
00:38:49,660 --> 00:38:52,740
So, Franny, wanna tell me why you're
following me around?

304
00:38:54,620 --> 00:38:56,780
I wanted to talk to you.

305
00:38:58,860 --> 00:39:00,500
I wanted to ask you something.

306
00:39:01,300 --> 00:39:02,580
All right.

307
00:39:06,100 --> 00:39:08,260
Wanna ask me now?

308
00:39:14,620 --> 00:39:16,140
Is it true?

309
00:39:16,580 --> 00:39:18,220
Is what true?

310
00:39:18,940 --> 00:39:21,420
The Boogeyman took your dad.

311
00:39:28,500 --> 00:39:33,220
Look, I don't know where you
heard that, but it's just a story.

312
00:39:35,460 --> 00:39:38,060
There's no such thing
as the Boogeyman.

313
00:39:43,500 --> 00:39:45,660
I was trying to make sense of things.

314
00:39:46,420 --> 00:39:50,140
My dad left. I was upset.
That's all that happened. Okay?

315
00:39:54,500 --> 00:39:58,340
It's late. Your parents will worry.
Where do you live? I'll give you a ride.

316
00:39:58,500 --> 00:40:00,140
Next to the park.

317
00:40:01,020 --> 00:40:03,260
It's all right. I've got my own ride.

318
00:40:07,460 --> 00:40:09,540
Hey, Franny.

319
00:40:09,900 --> 00:40:11,660
Count to five.

320
00:40:12,460 --> 00:40:16,340
When you're afraid,
close your eyes and count to five.

321
00:40:17,860 --> 00:40:20,020
Sometimes it works for me.

322
00:40:20,940 --> 00:40:23,300
What happens when you get to six?

323
00:41:02,540 --> 00:41:05,340
-You gotta be brave. Come on, Tim.
-No, Dad.

324
00:41:06,380 --> 00:41:08,420
Dad. Let me out.

325
00:41:08,580 --> 00:41:11,740
Tim, stop it. Okay? There is nothing
in there. Just trust me.

326
00:41:11,900 --> 00:41:14,820
Trust me. There is nothing in there.

327
00:41:15,540 --> 00:41:19,260
-"Just count to five like I told you."
-Count to five like I told you, okay?

328
00:41:19,420 --> 00:41:21,700
He's here. He's here.

329
00:41:26,420 --> 00:41:30,340
One. Two. Three.

330
00:41:30,500 --> 00:41:34,020
-Good boy.
-Four. Five.

331
00:41:40,780 --> 00:41:43,700
He's here. He's here.

332
00:44:36,700 --> 00:44:39,380
Tim. Where--?
Where are you going?

333
00:44:39,540 --> 00:44:41,700
-We have to go.
-What happened to your face?

334
00:44:41,860 --> 00:44:44,220
-Please. We have to go.
-I called your uncle.

335
00:44:44,380 --> 00:44:46,780
-I am so sorry about--
-I want to go now.

336
00:44:55,900 --> 00:45:00,260
Look, I'm sorry for losing it back there.
I just needed to get out of that house.

337
00:45:00,820 --> 00:45:02,820
Hey, it's okay.

338
00:45:05,860 --> 00:45:07,940
It's-- It's really late.

339
00:45:08,780 --> 00:45:13,740
Maybe we should stop somewhere
and get some rest, you know?

340
00:45:54,260 --> 00:45:55,860
How are you doing?

341
00:46:02,100 --> 00:46:06,980
Tim, come on. I drove two and
a half hours to be with you.

342
00:46:14,300 --> 00:46:16,540
I can't do this anymore, okay?

343
00:46:17,620 --> 00:46:20,580
It's too much, and I'm too tired.

344
00:46:26,340 --> 00:46:31,100
Look, can't we just forget
all about the bad stuff...

345
00:46:31,860 --> 00:46:33,860
...just for one night?

346
00:46:34,100 --> 00:46:36,820
Just try to have some fun?

347
00:46:39,700 --> 00:46:42,860
Pretend that nothing else is out there.

348
00:46:49,340 --> 00:46:52,180
Listen, why don't you go grab
some ice.

349
00:46:52,340 --> 00:46:54,340
I'll get the bath ready.

350
00:46:55,260 --> 00:46:58,340
We can raid the minibar
and have our own little...

351
00:46:58,500 --> 00:47:01,460
..."forget about the world" party.

352
00:47:03,060 --> 00:47:04,580
Okay?

353
00:47:07,220 --> 00:47:09,700
-All right.
-Good.

354
00:47:16,660 --> 00:47:18,220
Good.

355
00:47:32,260 --> 00:47:34,140
Hey, Tim.

356
00:48:08,140 --> 00:48:10,020
Hello.

357
00:48:11,020 --> 00:48:12,940
Anybody home?

358
00:48:14,220 --> 00:48:15,700
Tim.

359
00:48:18,380 --> 00:48:19,780
Hello.

360
00:48:43,380 --> 00:48:45,740
Brought you some food.

361
00:49:04,260 --> 00:49:06,140
I'm coming up.

362
00:49:08,500 --> 00:49:10,140
Are you decent?

363
00:49:43,380 --> 00:49:45,660
Nice lighting.

364
00:49:49,740 --> 00:49:51,660
Tim. Hello.

365
00:49:51,820 --> 00:49:54,020
I'll make us a vodka Red Bull.

366
00:49:58,980 --> 00:50:01,020
Tim.

367
00:50:02,540 --> 00:50:04,940
I might have made these a little strong.

368
00:50:05,100 --> 00:50:09,020
Listen, Jess. I just wanna thank you
for putting up with me.

369
00:50:17,540 --> 00:50:19,300
Jessica?

370
00:50:25,860 --> 00:50:27,620
Jessica?

371
00:50:42,180 --> 00:50:43,860
Jessica?

372
00:50:44,340 --> 00:50:46,220
Jessica?

373
00:50:48,180 --> 00:50:50,460
Jessica?

374
00:51:12,420 --> 00:51:15,020
Come on, Tim. This isn't funny.

375
00:52:17,620 --> 00:52:20,020
How long were you
waiting in there?

376
00:52:21,620 --> 00:52:23,860
You scared the crap out of me.

377
00:52:26,460 --> 00:52:29,020
Come on, I made you some food.
Come on.

378
00:52:30,140 --> 00:52:33,340
How did l--? Where's Jessica?

379
00:52:34,740 --> 00:52:37,420
-Who's Jessica?
-We went to the hotel.

380
00:52:37,580 --> 00:52:41,740
-The motel. And then--
-What are you talking about?

381
00:52:44,060 --> 00:52:46,900
-I have to go.
-Tim. Tim.

382
00:52:47,580 --> 00:52:49,980
-What the hell is going on?
-Come with me.

383
00:52:50,140 --> 00:52:51,820
I need you to see what I see.

384
00:52:52,780 --> 00:52:56,820
-What's wrong? Where are we going?
-I don't know. I don't know anything.

385
00:52:56,980 --> 00:53:00,500
I don't know where I've been
or what I've done. I just don't know.

386
00:53:07,500 --> 00:53:10,220
-Oh, God.
-What?

387
00:53:11,100 --> 00:53:14,660
That's it. That's the motel.

388
00:53:35,420 --> 00:53:37,660
What are we doing here?

389
00:53:38,700 --> 00:53:41,300
Maybe you should go to the front desk
and ask if....

390
00:54:50,540 --> 00:54:52,140
Jessica?

391
00:55:01,420 --> 00:55:02,820
I was here.

392
00:55:15,980 --> 00:55:17,300
What happened, Tim?

393
00:55:25,140 --> 00:55:26,820
We were here.

394
00:55:31,660 --> 00:55:37,660
I don't know. I went to get ice,
and I came back to--

395
00:55:48,820 --> 00:55:52,940
-He took her.
-What? Who took her?

396
00:55:55,060 --> 00:55:57,180
You won't believe me.

397
00:56:02,580 --> 00:56:05,060
Where did the blood
come from, Tim?

398
00:56:07,820 --> 00:56:10,060
Who do you think took your friend?

399
00:56:16,380 --> 00:56:18,500
Tim, if something happened....

400
00:56:20,140 --> 00:56:23,620
-lf you accidentally did something--
-I didn't hurt Jessica.

401
00:56:25,380 --> 00:56:27,140
Are you sure?

402
00:56:30,700 --> 00:56:33,540
Everybody told me I was
making it up.

403
00:56:34,860 --> 00:56:40,180
For 15 years. Ever since my dad left,
telling me over and over.

404
00:56:43,740 --> 00:56:45,700
But I was right.

405
00:56:48,620 --> 00:56:50,980
I don't wanna be, but I am.

406
00:56:53,540 --> 00:56:55,140
Tim.

407
00:56:58,380 --> 00:56:59,820
Tim.

408
00:57:18,060 --> 00:57:19,780
Good night, Tim.

409
00:57:43,740 --> 00:57:45,940
Kate! Kate, please open the door!

410
00:57:47,500 --> 00:57:51,460
-Kate! Kate. Come out.
-Go home. Get off the porch.

411
00:57:51,620 --> 00:57:53,420
-Listen to me.
-You need to get out.

412
00:57:53,740 --> 00:57:55,660
Don't grab me.

413
00:57:57,540 --> 00:57:59,860
-He's in your house.
-My dad is in my house.

414
00:58:00,020 --> 00:58:02,580
He's upstairs, probably
waiting up for me.

415
00:58:03,620 --> 00:58:05,100
Tim...

416
00:58:05,900 --> 00:58:08,500
...I think maybe you're sick.

417
00:58:09,220 --> 00:58:12,740
And I wish I could help you,
but I can't.

418
00:58:12,900 --> 00:58:15,820
Now, if you don't go home,
I'm calling the police.

419
00:58:15,980 --> 00:58:18,700
It wasn't your dad. I saw him.

420
00:58:19,220 --> 00:58:21,340
I'm going inside now.

421
00:58:22,220 --> 00:58:23,780
Go home.

422
00:58:25,020 --> 00:58:26,860
You need help, Tim.

423
00:59:16,300 --> 00:59:18,020
Couldn't sleep either?

424
00:59:19,300 --> 00:59:22,860
I was looking for you.
You said you lived by the park.

425
00:59:24,260 --> 00:59:28,620
Sometimes when I think he's
in my house, I come out here.

426
00:59:28,780 --> 00:59:31,340
Sometimes all night,
until the sun comes up.

427
00:59:32,060 --> 00:59:34,180
And everything's okay again.

428
00:59:35,780 --> 00:59:38,020
What if your parents come
to check in on you?

429
00:59:38,180 --> 00:59:40,100
I won't be there.

430
00:59:48,860 --> 00:59:50,620
I told you a lie.

431
00:59:51,180 --> 00:59:54,460
The story about the night
my father disappeared.

432
00:59:54,660 --> 00:59:56,100
I know.

433
01:00:07,620 --> 01:00:09,420
He took my dad.

434
01:00:12,420 --> 01:00:14,660
Tonight he took a friend of mine.

435
01:00:17,140 --> 01:00:19,900
He's gonna keep taking people.

436
01:00:20,060 --> 01:00:21,980
Everyone who means
something to me.

437
01:00:24,580 --> 01:00:26,340
I need your help.

438
01:00:29,620 --> 01:00:30,900
Why?

439
01:00:31,980 --> 01:00:34,580
Because we're the only ones
who believe.

440
01:00:37,860 --> 01:00:39,980
I have to go home.

441
01:00:56,300 --> 01:00:59,860
-Hello?
-It's Kate Houghton.

442
01:01:00,060 --> 01:01:05,100
Listen, I was just with Tim...

443
01:01:05,820 --> 01:01:09,740
...and I'm worried about him.
He's acting kind of....

444
01:01:10,540 --> 01:01:15,780
Could you go over there
and just check in on him?

445
01:01:16,660 --> 01:01:19,460
I'm sorry if I woke you up.

446
01:01:22,980 --> 01:01:26,420
Dad? Is that you?

447
01:01:30,500 --> 01:01:33,380
Franny. Franny.

448
01:01:34,900 --> 01:01:39,140
I opened your pack.
I saw your pictures and the articles.

449
01:01:39,300 --> 01:01:42,420
-I just need to ask you something.
-Okay.

450
01:01:42,580 --> 01:01:44,420
How do you stop him?

451
01:01:48,020 --> 01:01:49,700
I'm not sure.

452
01:01:53,820 --> 01:01:55,740
I know that house.

453
01:01:56,100 --> 01:01:58,140
I've been here before. Come on.

454
01:03:03,820 --> 01:03:06,580
I used to come talk to the man
who lived here.

455
01:03:10,260 --> 01:03:12,580
Everyone said he was crazy.

456
01:03:15,300 --> 01:03:17,460
He said his daughter was taken.

457
01:03:19,380 --> 01:03:21,100
What are all these words?

458
01:03:22,900 --> 01:03:24,460
I'm not sure.

459
01:03:26,860 --> 01:03:28,900
He sealed up everything.

460
01:03:30,580 --> 01:03:32,500
All the closets and doors.

461
01:03:42,340 --> 01:03:44,660
He was trying to understand it.

462
01:03:47,380 --> 01:03:49,540
Trying to give it a name.

463
01:04:19,140 --> 01:04:20,940
He sat here.

464
01:04:21,860 --> 01:04:23,180
Waiting.

465
01:04:29,460 --> 01:04:31,020
I don't want to be here.

466
01:04:47,380 --> 01:04:50,420
He didn't seal this one.
This was the door.

467
01:04:52,780 --> 01:04:54,580
He was trying to bring him out.

468
01:04:55,460 --> 01:04:56,740
Franny.

469
01:04:56,940 --> 01:04:59,460
We have to go back to the house.

470
01:05:13,420 --> 01:05:14,980
My dad was trying to find me.

471
01:05:16,460 --> 01:05:18,340
Trying to beat it.

472
01:05:21,980 --> 01:05:23,660
You have to go home.

473
01:05:23,820 --> 01:05:26,420
You have to go to the place
where it first started.

474
01:05:26,580 --> 01:05:28,540
That's where you face him.

475
01:05:29,060 --> 01:05:30,460
My dad got too scared.

476
01:05:32,300 --> 01:05:33,900
He couldn't face him.

477
01:05:44,220 --> 01:05:46,020
I wanna help you.

478
01:05:49,340 --> 01:05:52,140
You can't.
You can only help yourself.

479
01:06:14,700 --> 01:06:16,260
Tim.

480
01:06:20,260 --> 01:06:22,140
Hey, Tim.

481
01:06:23,220 --> 01:06:25,220
You okay, buddy?

482
01:06:25,860 --> 01:06:27,340
Kate called.

483
01:06:27,500 --> 01:06:30,100
Said things are getting
a little loose around here.

484
01:07:00,420 --> 01:07:01,980
Uncle Mike?

485
01:07:02,700 --> 01:07:04,140
Uncle Mike?

486
01:07:06,500 --> 01:07:08,220
Uncle Mike!

487
01:07:11,420 --> 01:07:13,300
Hey, Mike!

488
01:07:17,860 --> 01:07:19,460
He took him.

489
01:10:15,460 --> 01:10:17,620
He's waiting for you.

490
01:11:06,420 --> 01:11:10,220
Tim, what are you doing?
Get in here.

491
01:11:10,420 --> 01:11:12,140
Jessica?

492
01:12:10,460 --> 01:12:12,180
Jessica.

493
01:12:48,060 --> 01:12:50,180
Jessica?

494
01:12:51,180 --> 01:12:52,620
Tim?

495
01:12:53,140 --> 01:12:57,700
Mike. Uncle Mike, it's me.
What are you doing?

496
01:13:22,540 --> 01:13:25,060
Mike, Mike, I'm here.

497
01:13:27,220 --> 01:13:30,820
Can you hear me?
All right, all right, he's gone.

498
01:13:31,020 --> 01:13:33,380
I'm gonna save you. Come on!

499
01:13:35,980 --> 01:13:37,220
All right, hold on.

500
01:13:39,500 --> 01:13:42,140
I'm gonna cut you out, okay?
I'm not gonna hurt you.

501
01:13:59,060 --> 01:14:00,980
--check in on him?

502
01:14:01,540 --> 01:14:02,860
Thanks.

503
01:14:03,300 --> 01:14:05,060
I'm sorry if I woke you up.

504
01:14:07,980 --> 01:14:10,460
Dad? Is that you?

505
01:14:26,500 --> 01:14:28,660
What was that? What was that?

506
01:14:33,180 --> 01:14:35,860
I have to stop this.
I have to go after him.

507
01:14:36,020 --> 01:14:40,020
He's leading me somewhere. If I'm not
around, he won't bother you, okay?

508
01:14:42,660 --> 01:14:44,460
Tim.

509
01:14:56,980 --> 01:14:58,820
I don't understand.

510
01:14:59,420 --> 01:15:01,100
Tim!

511
01:15:06,140 --> 01:15:08,380
Help! Come on, come on!

512
01:15:09,020 --> 01:15:11,140
Help! Get him off! No!

513
01:15:30,140 --> 01:15:31,620
I gotta get you out of here.

514
01:15:31,780 --> 01:15:34,740
-Kate, come on! Come on, go, go!
-What? What?

515
01:15:35,820 --> 01:15:38,980
What happened? Come on!

516
01:15:39,620 --> 01:15:41,020
We gotta get out of here!

517
01:15:44,020 --> 01:15:46,940
Come on! Come on, help me!
Come on, Tim!

518
01:16:00,540 --> 01:16:01,820
Oh, God.

519
01:16:07,180 --> 01:16:09,100
Go to the place where
it first started.

520
01:16:09,260 --> 01:16:10,500
One.

521
01:16:11,340 --> 01:16:12,500
Two.

522
01:16:14,100 --> 01:16:15,460
-Three.
-Dad.

523
01:16:16,260 --> 01:16:17,460
Four.

524
01:16:18,900 --> 01:16:21,260
-Five.
-My dad got too scared.

525
01:16:21,420 --> 01:16:22,940
You have to face him.

526
01:16:24,620 --> 01:16:25,980
Six.

527
01:16:31,780 --> 01:16:34,700
Tim, is he gone?

528
01:16:39,020 --> 01:16:40,580
Tim?

529
01:16:50,220 --> 01:16:51,980
What are you doing?

530
01:17:29,180 --> 01:17:30,700
Come on!

531
01:17:45,780 --> 01:17:47,220
Tim!

532
01:18:20,260 --> 01:18:22,060
He's gone.

533
01:19:08,980 --> 01:19:10,860
Is it over?

534
01:19:13,220 --> 01:19:14,660
Yeah.

535
01:19:16,220 --> 01:19:18,060
Yeah, it's over.

536
01:19:20,980 --> 01:19:23,020
He's not coming back.

537
01:24:57,580 --> 01:24:59,420
Mom?

538
01:24:59,580 --> 01:25:01,260
Mom?

539
01:25:02,460 --> 01:25:04,660
Could you close the closet door?

