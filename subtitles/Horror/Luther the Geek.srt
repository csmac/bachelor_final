1
00:00:13,000 --> 00:00:16,000
Throughout America
in the early 20th century,

2
00:00:16,300 --> 00:00:17,900
circus carnivals criss-crossed

3
00:00:18,200 --> 00:00:20,700
the continental United States.

4
00:00:21,000 --> 00:00:24,800
Carny barkers hawked and
baited a curious public

5
00:00:25,200 --> 00:00:27,800
to come and see the
sideshow freaks.

6
00:00:28,200 --> 00:00:30,600
There was everything
from the fat lady

7
00:00:31,000 --> 00:00:34,000
to the wild man of Borneo.

8
00:00:34,300 --> 00:00:38,800
But of all the bizarre acts,
the strangest was the geek.

9
00:00:40,200 --> 00:00:42,800
The geek was a man
so down on his luck,

10
00:00:43,200 --> 00:00:45,000
he'd do anything for a drink.

11
00:00:45,300 --> 00:00:47,400
But, to earn that
shot of cheap whiskey,

12
00:00:47,800 --> 00:00:51,000
he had to bite the
head off a live chicken

13
00:00:51,400 --> 00:00:54,600
and drink its blood in
front of a stunned audience.

14
00:00:55,700 --> 00:00:56,900
The geek.

15
00:00:57,200 --> 00:00:59,000
An American phenomenon.

16
00:01:28,300 --> 00:01:30,300
Geek. Geek.

17
00:01:34,000 --> 00:01:36,200
Geek. Come on, geek.

18
00:01:37,600 --> 00:01:39,500
Come on, geek. Hey, geek.

19
00:01:43,800 --> 00:01:45,700
Geek, come on, geek.

20
00:02:05,400 --> 00:02:07,300
Hey geek! Come on geek!

21
00:02:17,200 --> 00:02:19,400
Geek, geek. Come on, geek.

22
00:02:36,100 --> 00:02:38,200
Geek, come on, geek.

23
00:02:49,800 --> 00:02:51,100
Ugh.

24
00:03:52,900 --> 00:03:54,600
Luther.

25
00:03:57,000 --> 00:04:00,700
Luther! Luther, you little turd.

26
00:04:02,200 --> 00:04:03,500
I'm going to beat you bloody.

27
00:04:05,900 --> 00:04:07,700
Luther is the story

28
00:04:08,000 --> 00:04:10,500
of a different kind of geek.

29
00:05:36,800 --> 00:05:40,000
Next case, state
prisoner #879632.

30
00:05:43,000 --> 00:05:45,200
Luther watts.

31
00:05:46,700 --> 00:05:50,200
Gentlemen, I feel strongly
about this particular case.

32
00:05:50,600 --> 00:05:52,800
Based on my panel's interview
at Dixon correctional

33
00:05:53,200 --> 00:05:56,000
with the prisoner i
must press for parole.

34
00:05:57,000 --> 00:05:59,700
Why? Why in this case?

35
00:06:00,000 --> 00:06:01,700
Because Luther watts
is a model prisoner

36
00:06:02,000 --> 00:06:05,200
and has been since
incarceration twenty years ago.

37
00:06:05,500 --> 00:06:07,900
His institutional adjustment
has been remarkable,

38
00:06:08,200 --> 00:06:09,600
given his childhood.

39
00:06:09,900 --> 00:06:11,200
Jesus h. Christ.

40
00:06:11,500 --> 00:06:13,600
What hogwash.

41
00:06:14,400 --> 00:06:17,400
I feel Luther has more than
paid his debt to society.

42
00:06:17,800 --> 00:06:18,800
Debt?

43
00:06:19,100 --> 00:06:20,900
Is that like taxes, Ms. Butler?

44
00:06:21,200 --> 00:06:23,000
Very clever.

45
00:06:23,400 --> 00:06:25,300
But we're talking
about a man's life.

46
00:06:25,600 --> 00:06:27,600
What about the three
people he murdered?

47
00:06:27,900 --> 00:06:29,600
Has he paid off
his debt to them?

48
00:06:30,300 --> 00:06:31,900
That really isn't the issue.

49
00:06:32,300 --> 00:06:34,700
We are trying to
rehabilitate prisoners.

50
00:06:35,000 --> 00:06:36,200
Luther wants a second chance.

51
00:06:36,500 --> 00:06:37,600
And the victims' families?

52
00:06:37,800 --> 00:06:39,500
Has he paid off his debt
to them, Ms. Butler?

53
00:06:39,800 --> 00:06:41,500
That was
twenty-five years ago.

54
00:06:41,900 --> 00:06:43,800
Luther was just a teenage boy.

55
00:06:44,100 --> 00:06:45,100
Bullshit.

56
00:06:45,200 --> 00:06:46,600
He's a killer.

57
00:06:47,300 --> 00:06:47,900
I know it, you know it.

58
00:06:48,200 --> 00:06:49,900
We all know it.

59
00:06:52,800 --> 00:06:56,100
Watts has been
a model prisoner.

60
00:06:56,600 --> 00:06:59,000
His institutional
adjustment is remarkable.

61
00:06:59,300 --> 00:07:02,300
In fact, in the background
file from menard psychiatric,

62
00:07:02,600 --> 00:07:03,600
{y:i}it states

63
00:07:03,800 --> 00:07:07,000
Jason, we've all
read the reports.

64
00:07:08,600 --> 00:07:10,000
Sorry-

65
00:07:11,200 --> 00:07:12,500
seems to me, the question is

66
00:07:12,800 --> 00:07:16,800
do we let this watts character
back out into the real world?

67
00:07:17,200 --> 00:07:19,600
Yes, I think we do.

68
00:07:20,200 --> 00:07:21,500
You're really
convinced of this?

69
00:07:21,600 --> 00:07:24,100
Luther watts has earned
the right to a second chance.

70
00:07:27,200 --> 00:07:30,000
You know what the guards
used to call old Luther?

71
00:07:31,200 --> 00:07:32,200
The geek.

72
00:07:32,500 --> 00:07:34,200
Because he liked to bite
his victims on the neck

73
00:07:34,200 --> 00:07:36,000
and watch them bleed to death.

74
00:07:37,300 --> 00:07:40,200
Human beings have
the capacity to change.

75
00:07:40,600 --> 00:07:42,800
You will admit that
much, Mr. Walsh?

76
00:07:45,000 --> 00:07:48,000
Should have hung the bastard
when they had the chance.

77
00:07:48,400 --> 00:07:49,400
You're disgusting.

78
00:07:49,700 --> 00:07:51,800
I don't know how you ever
got appointed to this board.

79
00:07:52,100 --> 00:07:53,100
{y:i}' Up yours.

80
00:07:53,200 --> 00:07:54,200
Bleeding heart bitch.

81
00:07:55,400 --> 00:07:56,400
All right, Walsh.

82
00:07:56,500 --> 00:07:59,200
There's no need for
that kind of talk.

83
00:08:03,100 --> 00:08:07,600
I suggest we bring state
prisoner ♪879632 to a vote.

84
00:08:10,200 --> 00:08:11,600
I vote for parole.

85
00:08:12,000 --> 00:08:13,100
Against.

86
00:08:15,700 --> 00:08:16,700
Against.

87
00:08:23,500 --> 00:08:26,400
I vote, uh

88
00:08:30,200 --> 00:08:31,400
for.

89
00:08:35,100 --> 00:08:38,400
The board votes
three to two in favor.

90
00:08:40,300 --> 00:08:41,300
Parole granted.

91
00:08:43,300 --> 00:08:47,800
Next case, state
pfls0ner♪524391.

92
00:10:20,100 --> 00:10:22,000
You gonna pay for these?

93
00:10:27,200 --> 00:10:28,900
Listen, buddy.

94
00:10:29,200 --> 00:10:30,600
You'd better get
out of the store.

95
00:10:30,900 --> 00:10:32,400
We're going to call the cops.

96
00:10:33,700 --> 00:10:35,400
Billy, call the
police right now.

97
00:10:36,800 --> 00:10:39,800
We're going to escort this
gentleman out of the store.

98
00:10:40,100 --> 00:10:41,600
Now, come on, buddy.

99
00:10:48,700 --> 00:10:50,200
Do you mind if i
get in front of you?

100
00:10:50,500 --> 00:10:51,600
No, go on.

101
00:10:52,000 --> 00:10:54,100
You're sure?

102
00:10:56,700 --> 00:10:58,400
My mom's always
forgetting something.

103
00:10:58,800 --> 00:11:00,700
I'm the one who gets stuck.

104
00:11:01,000 --> 00:11:03,100
Hey, moms aren't so bad.

105
00:11:06,300 --> 00:11:08,000
I forgot the pop.

106
00:11:08,300 --> 00:11:09,800
Watch my cart.

107
00:11:10,800 --> 00:11:13,000
Moms are a real
pain in the ass.

108
00:11:14,100 --> 00:11:15,900
Car 101 from Sterling.

109
00:11:16,200 --> 00:11:17,900
Cancel 10-10 at
kroger supermarket.

110
00:11:18,300 --> 00:11:20,000
Go 10-6.

111
00:12:39,100 --> 00:12:40,800
What's going on?

112
00:12:56,300 --> 00:12:57,900
10-10 in
progress, kroger parking lot.

113
00:12:58,300 --> 00:12:59,500
Handle 1039.

114
00:13:33,200 --> 00:13:33,800
Coming through, people.

115
00:13:34,200 --> 00:13:35,200
Move.

116
00:13:35,500 --> 00:13:36,900
Back up, back up, come on, back up.

117
00:13:41,200 --> 00:13:42,900
Oh no.

118
00:14:53,000 --> 00:14:54,200
Whew.

119
00:18:08,100 --> 00:18:09,600
Argh.

120
00:18:10,800 --> 00:18:12,100
Oh.

121
00:18:29,300 --> 00:18:31,200
No, no.

122
00:18:42,500 --> 00:18:45,000
Oh, no.

123
00:18:53,700 --> 00:18:54,700
"Hm?

124
00:20:06,100 --> 00:20:07,700
No.

125
00:20:12,900 --> 00:20:14,800
You don't want me.

126
00:20:16,200 --> 00:20:17,600
Don't touch me.

127
00:22:25,000 --> 00:22:27,000
Sure feels good to be home.

128
00:22:38,600 --> 00:22:39,600
Mom?

129
00:22:39,700 --> 00:22:41,100
I'm home.

130
00:22:43,200 --> 00:22:44,800
Watch out.

131
00:22:45,200 --> 00:22:46,200
That's nice.

132
00:22:46,300 --> 00:22:47,900
Real homey.

133
00:22:48,200 --> 00:22:50,000
I bet my mom forgot her keys.

134
00:22:50,300 --> 00:22:52,800
She's always forgetting
something since my dad died.

135
00:22:53,100 --> 00:22:54,900
Hey, mom?

136
00:22:55,200 --> 00:22:57,000
Is anybody home?

137
00:23:01,100 --> 00:23:04,100
You know, it's been four
months since I've seen her.

138
00:23:04,500 --> 00:23:06,900
Come on, you're a
big-time college girl.

139
00:23:07,200 --> 00:23:10,000
Maybe she wants Jim
to look at the corn.

140
00:23:10,800 --> 00:23:12,800
He's the guy who works our farm.

141
00:23:13,700 --> 00:23:15,000
I'm thirsty.

142
00:23:15,300 --> 00:23:17,000
Would you like a
pop or something?

143
00:23:17,400 --> 00:23:18,800
Or something.

144
00:23:19,100 --> 00:23:20,400
Maybe.

145
00:23:23,800 --> 00:23:26,400
Hey, you like
banana splits, right?

146
00:23:26,800 --> 00:23:28,000
I'm on a diet.

147
00:23:31,800 --> 00:23:33,100
Great.

148
00:23:33,400 --> 00:23:35,000
God, now I've got
to go take a shower.

149
00:23:35,400 --> 00:23:35,800
Great.

150
00:23:36,100 --> 00:23:37,100
I'll be right back.

151
00:25:40,900 --> 00:25:42,400
Beth?

152
00:25:42,700 --> 00:25:44,200
I'm in here.

153
00:26:18,300 --> 00:26:20,200
Relax, relax.

154
00:27:10,000 --> 00:27:11,400
Hgy-

155
00:27:14,800 --> 00:27:17,200
don't forget to
wash behind your ears.

156
00:29:32,200 --> 00:29:34,000
Don't stop, don't stop.

157
00:29:37,200 --> 00:29:38,200
Damn.

158
00:29:39,800 --> 00:29:43,000
Rob? What is going on?

159
00:29:43,400 --> 00:29:45,000
Someone's stealing my bike.

160
00:29:46,600 --> 00:29:48,800
Who's out there?

161
00:29:53,200 --> 00:29:54,700
Where are my pants?

162
00:29:55,000 --> 00:29:56,100
Rob.

163
00:29:59,900 --> 00:30:01,300
Hey, that's my bike.

164
00:30:25,300 --> 00:30:26,800
Damn.

165
00:31:24,500 --> 00:31:26,000
Please.

166
00:31:27,000 --> 00:31:28,600
Help.

167
00:31:32,700 --> 00:31:34,900
Help me.

168
00:31:50,400 --> 00:31:51,600
Mom.

169
00:31:52,600 --> 00:31:54,000
What happened?

170
00:31:54,400 --> 00:31:55,800
Who did this to you?

171
00:31:58,100 --> 00:31:59,200
God.

172
00:32:02,600 --> 00:32:03,900
Oh, thank you.

173
00:32:04,200 --> 00:32:06,000
God.

174
00:32:06,800 --> 00:32:07,400
Quick, quick.

175
00:32:07,700 --> 00:32:08,700
Untie these.

176
00:32:08,900 --> 00:32:09,600
Get these things off me.

177
00:32:09,900 --> 00:32:10,900
Listen, it's okay.

178
00:32:11,000 --> 00:32:12,400
Rob's here.

179
00:32:12,700 --> 00:32:13,700
Listen to me, Beth.

180
00:32:14,000 --> 00:32:15,800
Untie these knots.

181
00:32:16,100 --> 00:32:17,100
Beth.

182
00:32:17,400 --> 00:32:18,100
Okay-

183
00:32:18,400 --> 00:32:19,000
Beth, you hear me?

184
00:32:19,400 --> 00:32:20,400
Yes.

185
00:32:21,000 --> 00:32:22,000
Quickly.

186
00:32:22,100 --> 00:32:24,400
I can't.

187
00:32:26,200 --> 00:32:27,400
I can't get them.

188
00:32:27,700 --> 00:32:28,200
(Huff)'-

189
00:32:28,600 --> 00:32:30,700
hurry, honey, come on.

190
00:32:34,400 --> 00:32:35,400
It won't budge.

191
00:32:35,900 --> 00:32:36,900
Come on, baby.

192
00:32:37,000 --> 00:32:38,000
It won't budge.

193
00:32:44,500 --> 00:32:45,500
The 939-

194
00:32:45,600 --> 00:32:46,000
put the gag back in my mouth.

195
00:32:46,300 --> 00:32:48,400
Beth, put the gag on me.

196
00:32:50,000 --> 00:32:51,000
Goddamnit, put the

197
00:33:08,700 --> 00:33:09,700
come on, quick.

198
00:33:15,200 --> 00:33:17,600
Now, hide.

199
00:37:07,800 --> 00:37:08,800
Hey, mister.

200
00:37:19,700 --> 00:37:21,800
It was an accident.

201
00:37:22,200 --> 00:37:24,400
God, he jumped up
out of nowhere.

202
00:37:24,800 --> 00:37:26,400
Are you crazy?

203
00:38:52,300 --> 00:38:54,100
Qh_

204
00:39:21,300 --> 00:39:22,900
god.

205
00:39:28,500 --> 00:39:30,100
Oh, god.

206
00:39:30,400 --> 00:39:31,600
Beth.

207
00:39:32,200 --> 00:39:35,200
Beth, Beth!

208
00:39:36,300 --> 00:39:38,600
Beth! Beth, snap out of it.

209
00:39:39,800 --> 00:39:42,700
Beth! I need you to help me.

210
00:39:44,600 --> 00:39:47,800
Beth, snap out of it!

211
00:39:48,100 --> 00:39:50,200
Beth. I need you to help me.

212
00:39:50,600 --> 00:39:53,200
Do you hear me?

213
00:39:53,500 --> 00:39:54,500
He's gone.

214
00:39:54,700 --> 00:39:55,400
He's gone for now,
do you hear me?

215
00:39:55,700 --> 00:39:57,000
That bastard is gone.

216
00:39:57,400 --> 00:39:59,600
But he could come back.

217
00:39:59,900 --> 00:40:01,800
Do you know what I'm saying?

218
00:40:02,200 --> 00:40:03,900
Mommy, is rob dead?

219
00:40:04,200 --> 00:40:06,000
I'm sure he's okay.

220
00:40:06,300 --> 00:40:08,000
But, he had on rob's boots.

221
00:40:08,300 --> 00:40:11,000
We can't help
rob unless I get free.

222
00:40:12,000 --> 00:40:13,000
Beth, go downstairs.

223
00:40:13,300 --> 00:40:14,700
Get the scissors
out of the closet.

224
00:40:15,000 --> 00:40:16,700
Anything to cut
these things off.

225
00:40:19,000 --> 00:40:20,400
Hurry up, damn it.

226
00:40:53,200 --> 00:40:55,100
Did you find them?

227
00:40:56,000 --> 00:40:57,400
" No!

228
00:41:33,100 --> 00:41:34,200
Beth?

229
00:41:35,300 --> 00:41:36,600
Rob.

230
00:41:45,000 --> 00:41:46,600
No, no don't hurt him.

231
00:41:47,000 --> 00:41:48,700
Beth, help me.

232
00:41:50,700 --> 00:41:53,000
Beth, you can't leave me.

233
00:41:53,300 --> 00:41:55,300
Don't leave me like this.

234
00:41:57,400 --> 00:41:57,700
{y:i}Ok, mom.

235
00:41:58,000 --> 00:41:59,000
I'm sorry.

236
00:41:59,200 --> 00:42:00,300
I'm going to try to stall him.

237
00:42:00,400 --> 00:42:02,200
No, no, Beth.

238
00:42:02,500 --> 00:42:03,500
Come back here.

239
00:42:03,600 --> 00:42:07,400
Beth, come back here.

240
00:42:20,200 --> 00:42:21,200
Beth?

241
00:42:21,400 --> 00:42:23,400
Are you in there?

242
00:42:27,200 --> 00:42:29,000
Rob.

243
00:42:33,400 --> 00:42:34,400
Rob.

244
00:43:16,100 --> 00:43:17,600
Who is this guy?

245
00:43:18,300 --> 00:43:19,900
I don't know.

246
00:43:27,600 --> 00:43:29,800
' Hey, you $968k English?

247
00:43:38,100 --> 00:43:39,100
You know, English?

248
00:43:39,400 --> 00:43:42,000
Like, what the fuck is going on?

249
00:43:50,800 --> 00:43:53,200
Jesus, he looks like a chicken.

250
00:43:55,100 --> 00:43:56,600
This is crazy.

251
00:44:13,600 --> 00:44:15,200
Oh, no.

252
00:44:45,800 --> 00:44:47,500
Hello in there.

253
00:44:54,800 --> 00:44:57,300
Just a minute.

254
00:45:11,600 --> 00:45:13,000
Ma'am.

255
00:45:13,400 --> 00:45:15,700
You left the lights
on to your car.

256
00:45:18,500 --> 00:45:20,500
Is everything okay?

257
00:45:21,000 --> 00:45:23,400
Yeah, I'm fine.

258
00:45:25,800 --> 00:45:29,000
Jim giles asked me to
look in on Mrs. Lawson.

259
00:45:29,300 --> 00:45:31,800
Is she your mother?

260
00:45:32,200 --> 00:45:34,100
Hmm. Oh, yeah.

261
00:45:34,400 --> 00:45:36,200
Mrs. Lawson's my mom.

262
00:45:39,800 --> 00:45:41,600
You alone here?

263
00:45:44,300 --> 00:45:46,100
Um yeah, I mean, no.

264
00:45:46,400 --> 00:45:49,300
Really everything's all right.

265
00:45:53,000 --> 00:45:57,600
Truth is, I just broke
up with my boyfriend,

266
00:45:58,100 --> 00:46:01,200
so I'm just a little messed up.

267
00:46:05,100 --> 00:46:06,200
Mr. giles?

268
00:46:06,600 --> 00:46:07,900
He works our farm.

269
00:46:08,300 --> 00:46:10,000
Yeah, when I told
Jim about Luther watts,

270
00:46:10,200 --> 00:46:12,800
he asked me to stop by.

271
00:46:14,300 --> 00:46:16,000
Is your phone working?

272
00:46:16,300 --> 00:46:17,500
Sure, I mean, I think so.

273
00:46:17,800 --> 00:46:19,600
Well, maybe I should check.

274
00:46:19,900 --> 00:46:21,900
Um, no, really.

275
00:46:22,200 --> 00:46:24,000
It's okay.

276
00:46:24,300 --> 00:46:25,500
My mom's taking a nap.

277
00:46:29,300 --> 00:46:31,400
Who is Luther watts?

278
00:46:32,600 --> 00:46:34,300
Paroled killer.

279
00:46:34,700 --> 00:46:36,900
There's a two-state
alarm out for his arrest.

280
00:46:38,200 --> 00:46:42,000
Look, I don't want to
frighten you or your mother,

281
00:46:42,800 --> 00:46:45,500
it's just that this watts character is
extremely dangerous.

282
00:46:48,500 --> 00:46:50,100
Just be careful.

283
00:46:51,800 --> 00:46:53,900
I'll tell my mom,

284
00:46:54,200 --> 00:46:55,200
when she wakes up.

285
00:47:04,400 --> 00:47:05,900
Don't go.

286
00:47:09,700 --> 00:47:12,500
Could I get you a cup of coffee?

287
00:47:13,200 --> 00:47:14,900
I'm on duty, ma'am.

288
00:47:18,100 --> 00:47:20,100
My name's Edwards.

289
00:47:20,400 --> 00:47:21,700
Mark Edwards.

290
00:47:24,000 --> 00:47:26,100
Don't forget those lights.

291
00:48:47,000 --> 00:48:47,500
Stop-

292
00:48:47,800 --> 00:48:48,800
I didn't say anything.

293
00:48:53,400 --> 00:48:55,600
Don't hurt me.

294
00:49:18,600 --> 00:49:21,000
Mom, help me.

295
00:51:22,500 --> 00:51:24,400
No.

296
00:51:29,500 --> 00:51:31,000
Oh, no.

297
00:51:39,000 --> 00:51:40,800
Beth.

298
00:51:41,100 --> 00:51:43,100
Mommy?

299
00:51:43,400 --> 00:51:45,100
Oh, Beth.

300
00:52:00,200 --> 00:52:02,200
No, no.

301
00:52:02,500 --> 00:52:04,300
Oh, Beth.

302
00:52:23,000 --> 00:52:24,500
Oh.

303
00:52:24,800 --> 00:52:26,700
I thought I'd come back
for that cup of coffee.

304
00:52:26,800 --> 00:52:27,200
Help me.

305
00:52:27,600 --> 00:52:28,600
You've got to help me.

306
00:52:28,700 --> 00:52:30,100
This maniac just
killed my daughter.

307
00:52:30,400 --> 00:52:30,900
Please help me.

308
00:52:31,300 --> 00:52:32,000
Qkay, okay.

309
00:52:32,300 --> 00:52:33,300
Calm down.

310
00:52:33,400 --> 00:52:34,000
That's right.

311
00:52:34,300 --> 00:52:34,900
You kill him.

312
00:52:35,200 --> 00:52:36,300
You kill that son of a bitch.

313
00:52:36,400 --> 00:52:36,900
Hey, hey.

314
00:52:37,200 --> 00:52:37,800
Take it easy.

315
00:52:38,200 --> 00:52:39,200
Promise me.

316
00:52:39,400 --> 00:52:39,800
Just promise me that
you'll kill him.

317
00:52:40,100 --> 00:52:41,100
Shut up.

318
00:52:43,400 --> 00:52:44,400
Oh, no.

319
00:52:44,800 --> 00:52:45,800
Just show me where he is.

320
00:52:46,100 --> 00:52:47,600
I'll do the rest.

321
00:53:20,600 --> 00:53:22,100
Damn.

322
00:54:16,600 --> 00:54:18,100
Freeze.

323
00:54:59,600 --> 00:55:01,300
Son of a bitch.

324
00:55:48,100 --> 00:55:49,500
Telephone?

325
00:55:51,300 --> 00:55:52,800
Wait.

326
00:55:53,200 --> 00:55:54,800
The keys to your car.

327
00:56:39,200 --> 00:56:41,100
Battery's dead.

328
00:56:44,200 --> 00:56:45,800
Okay.

329
00:56:46,100 --> 00:56:48,400
We'll go inside,
we'll wait him out.

330
00:56:48,700 --> 00:56:50,200
Morning's soon enough
to go for help.

331
00:57:05,600 --> 00:57:07,200
Get the lights out there.

332
00:57:21,800 --> 00:57:23,400
You got a gun in there?

333
00:57:26,200 --> 00:57:28,000
Don't be afraid to use it.

334
00:57:33,800 --> 00:57:35,000
Don't leave me.

335
00:57:37,300 --> 00:57:38,700
Lock the doors, you hear?

336
00:57:39,000 --> 00:57:41,100
And stay put til I get back.

337
01:00:17,800 --> 01:00:18,900
Shit.

338
01:00:57,900 --> 01:01:00,600
Alright, chicken man.

339
01:01:00,900 --> 01:01:04,100
Colonel Sanders wants
to fry your ass.

340
01:03:56,100 --> 01:03:57,100
Come on.

341
01:03:57,300 --> 01:03:58,800
Come on, chickenshit.

342
01:04:00,200 --> 01:04:01,200
Buck.

343
01:04:01,300 --> 01:04:02,700
Buck buck buck.

344
01:04:09,200 --> 01:04:10,800
(Gunshon

345
01:04:11,500 --> 01:04:13,200
ahh.

346
01:04:14,900 --> 01:04:16,100
(Gunshon

347
01:08:04,500 --> 01:08:05,500
(gunshon

348
01:08:13,400 --> 01:08:15,000
damn it.

349
01:09:24,200 --> 01:09:25,400
Oof.

350
01:15:58,300 --> 01:16:02,800
Buck. Buck.

351
01:16:27,400 --> 01:16:31,600
Buck. Buck buck.

352
01:16:42,800 --> 01:16:44,400
Oh.

