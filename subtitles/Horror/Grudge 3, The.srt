1
00:01:29,250 --> 00:01:30,593
Good morning.

2
00:01:32,700 --> 00:01:35,318
Peter, are you okay?

3
00:01:41,275 --> 00:01:43,217
You're up early today.

4
00:02:47,576 --> 00:02:48,524
CURSE - GRUDGE

5
00:04:04,146 --> 00:04:05,553
Hi.

6
00:04:06,803 --> 00:04:08,712
Excuse me.

7
00:04:14,834 --> 00:04:16,078
Hi, Emma.

8
00:04:16,434 --> 00:04:18,408
It's good to see you again.

9
00:04:23,122 --> 00:04:24,813
Do you remember me?

10
00:04:25,937 --> 00:04:27,279
It's Yoko.

11
00:07:58,341 --> 00:08:01,275
- Sorry. Did I wake you?
- Bottom drawer.

12
00:08:10,724 --> 00:08:12,033
Thank you.

13
00:08:12,964 --> 00:08:15,681
- You know me so well.
- Come back to bed.

14
00:08:15,747 --> 00:08:18,911
I can't. I'm gonna be late
for my architecture midterm.

15
00:08:19,459 --> 00:08:21,020
Check your watch.

16
00:08:28,163 --> 00:08:30,846
I set the clock an hour ahead last night.

17
00:08:35,106 --> 00:08:37,113
Maybe you know me too well.

18
00:08:43,330 --> 00:08:46,178
I'm really glad you decided
to come here with me.

19
00:08:46,242 --> 00:08:48,959
- Me, too.
- You sure?

20
00:08:49,409 --> 00:08:51,743
Of course. Why?

21
00:08:52,481 --> 00:08:56,158
- It's a big deal, you know.
- Lots of people go abroad.

22
00:08:57,281 --> 00:09:00,663
Besides, you've been dreaming
about coming here forever.

23
00:09:00,801 --> 00:09:03,289
I'm just glad we could be together.

24
00:09:09,953 --> 00:09:12,800
- Don't you have class today?
- Nope.

25
00:09:12,864 --> 00:09:15,384
Just have to pick up a book
I left at the Care Center.

26
00:09:15,456 --> 00:09:19,864
Come on, you could spare 10 minutes.
I promise I'll get you to class on time.

27
00:09:59,901 --> 00:10:03,643
- Where are you going?
- Come here. I wanna show you something.

28
00:10:09,980 --> 00:10:11,736
It's a Buddhist ritual.

29
00:10:11,804 --> 00:10:15,546
The incense smoke carries the prayers
to the spirits of their ancestors.

30
00:10:15,612 --> 00:10:17,368
It's to help them find peace.

31
00:10:20,924 --> 00:10:23,509
They must have lost someone they loved.

32
00:10:29,339 --> 00:10:30,768
Excuse me.

33
00:10:32,412 --> 00:10:33,786
Come on.

34
00:10:43,771 --> 00:10:45,745
- Karen?
- Yes, Alex?

35
00:10:46,491 --> 00:10:50,167
- Are you free this afternoon?
- Just a test to study for. Why?

36
00:10:51,482 --> 00:10:54,613
Your wish has been granted.
Your first visit alone.

37
00:10:55,097 --> 00:10:58,512
Just make sure she has what she needs,
and help out around the house.

38
00:10:58,585 --> 00:11:01,968
It's Yoko's case, but she didn't show up
for work this morning.

39
00:11:02,714 --> 00:11:05,877
I can't get her on the phone.
She must be sick or something.

40
00:11:05,945 --> 00:11:08,149
And she has the damn house key.

41
00:11:09,369 --> 00:11:11,027
What's this phrase?

42
00:11:12,217 --> 00:11:16,341
That's "severe lethargy with mild dementia. "

43
00:11:17,944 --> 00:11:21,010
Apparently,
she sleeps through most of the day.

44
00:11:22,232 --> 00:11:25,493
Her daughter-in-law doesn't work,
so she'll probably be there.

45
00:11:26,456 --> 00:11:29,772
It's an English-language house,
so you should be fine.

46
00:11:30,263 --> 00:11:31,670
Here's the address.

47
00:11:31,735 --> 00:11:33,645
You can use the wall chart
if you need help...

48
00:11:33,719 --> 00:11:36,785
and don't forget to bring a map, okay?

49
00:11:40,312 --> 00:11:42,832
Don't worry, Karen. You're ready.

50
00:13:59,535 --> 00:14:00,844
Hello?

51
00:14:21,293 --> 00:14:22,820
Is anybody here?

52
00:14:41,868 --> 00:14:45,130
Oh, my God. Are you all right? Come here.

53
00:15:04,363 --> 00:15:05,672
Okay, Emma.

54
00:15:06,922 --> 00:15:10,053
My name's Karen Davis.

55
00:15:11,466 --> 00:15:16,005
I'm from the Care Center.
I'm substituting for Yoko, just for today.

56
00:15:56,648 --> 00:15:57,989
Hello?

57
00:17:21,410 --> 00:17:25,371
Yes, he was shut in the closet.
There was tape all around the door.

58
00:17:27,266 --> 00:17:28,989
They're not home yet.

59
00:17:30,562 --> 00:17:34,042
I don't know.
Emma still hasn't said a word to me.

60
00:17:36,161 --> 00:17:38,463
No, he won't come down.

61
00:17:38,978 --> 00:17:42,906
I know. I'm sorry, Alex. Okay.

62
00:17:43,009 --> 00:17:45,343
Just get here as soon as you can.

63
00:19:38,586 --> 00:19:40,080
Toshio.

64
00:19:55,961 --> 00:19:58,994
<i>Matthew and Jennifer aren't around.
Leave a message.</i>

65
00:19:59,769 --> 00:20:03,925
<i>Hey, guys, it's Susan.
Matt, are you there? Pick up.</i>

66
00:20:04,760 --> 00:20:07,924
<i>Okay, I'm leaving work now,
so you can try my cell...</i>

67
00:20:07,992 --> 00:20:10,261
<i>or give me a call at home later.</i>

68
00:20:11,864 --> 00:20:13,936
<i>I just want to make sure Mom's okay.</i>

69
00:20:14,008 --> 00:20:16,375
<i>So just call me when you can, okay?</i>

70
00:20:54,454 --> 00:20:56,276
Who are you talking to?

71
00:21:03,029 --> 00:21:05,647
I just want her to leave me alone.

72
00:21:09,397 --> 00:21:12,463
You need to get some rest, okay?

73
00:21:36,147 --> 00:21:38,475
LOOKING FOR OCCUPANT
Happy Home Sect. Ltd.

74
00:22:01,138 --> 00:22:03,625
Welcome. What do you think?

75
00:22:06,513 --> 00:22:07,822
Your shoes.

76
00:22:09,521 --> 00:22:12,271
- Even in your own house?
- Even in your own house.

77
00:22:16,560 --> 00:22:18,415
It's perfect for Mom.

78
00:22:19,281 --> 00:22:22,095
She won't even have to deal with the stairs.

79
00:22:30,832 --> 00:22:33,734
- Hey, where's Mom?
- I'll look for her.

80
00:22:48,687 --> 00:22:49,897
Mom?

81
00:22:51,726 --> 00:22:53,155
Where are you?

82
00:23:02,285 --> 00:23:05,929
Come on, Mom.
You know what the doctor said about stairs.

83
00:23:07,470 --> 00:23:09,957
- Is she okay?
- Yeah.

84
00:23:10,029 --> 00:23:11,458
Hey, Mom.

85
00:23:13,869 --> 00:23:15,113
What?

86
00:23:16,013 --> 00:23:18,599
Mom, are you all right?

87
00:24:36,968 --> 00:24:38,494
Suzuki-san?

88
00:24:40,168 --> 00:24:42,818
- We'll take it.
- Okay.

89
00:25:06,055 --> 00:25:07,364
Morning.

90
00:25:10,726 --> 00:25:14,435
I hope Mom's okay.
She's been sleeping ever since we got here.

91
00:25:15,173 --> 00:25:16,864
Not at night.

92
00:25:18,438 --> 00:25:21,634
I've told you, Matthew,
you can sleep through anything.

93
00:25:21,701 --> 00:25:23,490
I'm sorry, sweetheart.

94
00:25:23,557 --> 00:25:27,135
I'm sure it was just the move.
She'll get back on schedule.

95
00:25:28,453 --> 00:25:32,194
- Have you seen my travel mug?
- I haven't unpacked it yet.

96
00:25:36,356 --> 00:25:39,171
Maybe you should say something
to the helper.

97
00:25:39,236 --> 00:25:41,243
- What's her name?
- Yoko.

98
00:25:41,540 --> 00:25:44,223
Who knows?
Maybe she can suggest something.

99
00:25:44,292 --> 00:25:45,634
Maybe.

100
00:25:48,515 --> 00:25:50,173
I miss seeing this.

101
00:25:53,283 --> 00:25:56,317
I guess now we're
gonna have to talk in the morning.

102
00:26:00,035 --> 00:26:01,823
Hey, kiddo, you okay?

103
00:26:04,834 --> 00:26:07,485
Jen? Sorry, honey, are you all right?

104
00:26:10,658 --> 00:26:12,065
What's up?

105
00:26:16,641 --> 00:26:18,681
I went for a walk yesterday.

106
00:26:19,042 --> 00:26:20,471
Just to explore.

107
00:26:22,625 --> 00:26:24,414
And I got so lost.

108
00:26:25,889 --> 00:26:29,751
And I couldn't find anyone
who spoke English that could help me.

109
00:26:34,017 --> 00:26:37,116
This will get easier soon. I promise.

110
00:26:40,065 --> 00:26:41,341
Or else?

111
00:26:45,185 --> 00:26:48,501
Or else I tell the company
it's just not working out.

112
00:26:48,992 --> 00:26:52,734
And we're going back to the States
with or without my old job.

113
00:26:54,816 --> 00:26:57,979
They can find somebody else
to crunch their numbers.

114
00:26:59,391 --> 00:27:01,911
But until then...

115
00:27:02,975 --> 00:27:06,323
promise me you'll give it a good try,
okay, Jen?

116
00:27:07,455 --> 00:27:08,982
Deal.

117
00:28:23,610 --> 00:28:26,806
If you want something,
all you have to do is say so.

118
00:29:27,382 --> 00:29:28,909
Who's there?

119
00:30:14,292 --> 00:30:15,535
Hey, Jen?

120
00:30:35,954 --> 00:30:37,841
What the hell...

121
00:30:42,130 --> 00:30:43,504
Mom?

122
00:30:47,282 --> 00:30:49,005
Are you all right?

123
00:30:52,177 --> 00:30:54,032
Mom, where's Jennifer?

124
00:31:04,561 --> 00:31:06,087
Come on.

125
00:31:12,784 --> 00:31:14,245
You scared...

126
00:31:17,232 --> 00:31:19,598
Jen? Honey?

127
00:31:22,352 --> 00:31:25,515
What is it? What's wrong?

128
00:31:29,103 --> 00:31:32,518
What happened? Honey, what is it?

129
00:31:39,279 --> 00:31:41,897
I'm gonna call an ambulance, all right?

130
00:31:46,606 --> 00:31:49,323
Who are you? What are you doing here?

131
00:33:10,857 --> 00:33:13,507
Excuse me, Mrs. Williams.

132
00:33:52,646 --> 00:33:53,595
INVESTIGATION

133
00:34:09,157 --> 00:34:13,729
I am Det. Nakagawa.
This is Det. Igarashi, my assistant.

134
00:34:15,173 --> 00:34:17,311
Is Karen okay?

135
00:34:18,436 --> 00:34:20,094
She is very shaken.

136
00:34:20,324 --> 00:34:23,902
We would like her to stay
in the hospital tonight under evaluation.

137
00:34:25,380 --> 00:34:27,322
Do you know these people?

138
00:34:32,483 --> 00:34:35,745
Yeah, that's Matthew Williams
and his wife, Jennifer.

139
00:34:37,859 --> 00:34:40,893
He's the son of the woman
Karen came here to see.

140
00:34:42,787 --> 00:34:45,339
When was the last time you saw them?

141
00:34:47,971 --> 00:34:50,142
When they came in to register.

142
00:34:51,043 --> 00:34:53,050
It's standard procedure...

143
00:34:53,314 --> 00:34:55,419
although the visits were arranged
by his employer.

144
00:34:55,490 --> 00:34:58,141
- I think he works at...
- We've spoken with his employer.

145
00:34:58,210 --> 00:35:00,544
He hasn't shown up for work today.

146
00:35:03,010 --> 00:35:06,654
If you could please come by tomorrow
and make a statement.

147
00:35:07,938 --> 00:35:10,327
The address is there at the bottom.

148
00:35:15,361 --> 00:35:18,361
- Yoko.
- I'm sorry?

149
00:35:23,008 --> 00:35:24,896
Karen's a substitute.

150
00:35:25,249 --> 00:35:27,769
Yoko's the girl
who's normally in charge of Emma.

151
00:35:28,608 --> 00:35:30,975
She's also been missing from work.

152
00:35:31,744 --> 00:35:33,270
For how long?

153
00:35:35,583 --> 00:35:39,096
Since yesterday.
I think I saw her bike outside.

154
00:36:01,791 --> 00:36:06,297
<i>Hey, guys, it's Susan.
Matt, are you there? Pick up.</i>

155
00:36:07,326 --> 00:36:10,806
<i>Okay, I'm leaving work now,
so you can try my cell...</i>

156
00:36:10,878 --> 00:36:12,950
<i>or give me a call at home later.</i>

157
00:36:13,021 --> 00:36:14,548
<i>I'm just a little worried about Mom.</i>

158
00:36:14,622 --> 00:36:16,629
<i>I just want to make sure she's okay.</i>

159
00:36:16,702 --> 00:36:19,091
<i>So just call me when you can, okay?</i>

160
00:36:20,349 --> 00:36:24,145
<i>Thursday, 8:27 p. m. End of messages.</i>

161
00:39:33,969 --> 00:39:35,879
It's okay. I've got you.

162
00:39:39,185 --> 00:39:42,927
Your boss told me what happened.
I'm sorry.

163
00:39:48,016 --> 00:39:50,405
I'm not even sure what did happen.

164
00:39:51,696 --> 00:39:54,184
An old woman passed away in her sleep.

165
00:39:54,960 --> 00:39:58,441
- It's sad, but that's all it is.
- Is that how they're saying she died?

166
00:40:06,255 --> 00:40:07,816
That house.

167
00:40:09,968 --> 00:40:12,455
There was something...

168
00:40:19,726 --> 00:40:21,613
It's okay, Karen.

169
00:40:27,086 --> 00:40:30,632
<i>Matthew and Jennifer aren't around.
Leave a message.</i>

170
00:40:32,270 --> 00:40:36,809
Hey, guys, it's Susan.
Matt, are you there? Pick up.

171
00:40:38,477 --> 00:40:42,187
Okay, I'm leaving work now,
so you can try my cell...

172
00:40:42,254 --> 00:40:44,425
or give me a call at home later.

173
00:40:45,037 --> 00:40:46,498
I'm just a little worried about Mom.

174
00:40:46,573 --> 00:40:49,061
I just want to make sure she's okay.

175
00:40:49,132 --> 00:40:52,132
So just call me when you can, okay?

176
00:42:04,457 --> 00:42:06,082
Matthew, stop it.

177
00:42:32,518 --> 00:42:34,079
Is anyone there?

178
00:43:11,780 --> 00:43:13,274
Excuse me!

179
00:43:13,764 --> 00:43:17,310
Please. There was something on the...

180
00:43:27,299 --> 00:43:30,616
- Sit down, please.
- Please, just help.

181
00:47:08,726 --> 00:47:09,674
Hello?

182
00:47:10,006 --> 00:47:10,988
<i>It's me.</i>

183
00:47:16,630 --> 00:47:18,898
<i>- Are you all right?
- Yeah, I'm downstairs.</i>

184
00:47:19,477 --> 00:47:21,364
<i>What number are you again?</i>

185
00:47:21,429 --> 00:47:23,600
1601. I'll buzz you in.

186
00:47:56,531 --> 00:47:58,636
I don't know what you're up...

187
00:48:58,671 --> 00:49:00,427
How are you feeling?

188
00:49:00,975 --> 00:49:03,658
Okay. Tired.

189
00:49:06,319 --> 00:49:08,653
- You are an exchange student?
- Yes.

190
00:49:10,094 --> 00:49:13,094
And you have been volunteering
at the Care Center for...

191
00:49:13,198 --> 00:49:16,580
Three months.
I needed a social welfare credit.

192
00:49:18,382 --> 00:49:21,480
You said this was the first time
you'd been in that house?

193
00:49:22,957 --> 00:49:23,906
Yeah.

194
00:49:27,309 --> 00:49:29,578
About the Japanese boy
you said you saw there...

195
00:49:29,645 --> 00:49:32,395
- Did you find him?
- No, not yet.

196
00:49:34,316 --> 00:49:36,967
You said the boy had been taped
into the closet?

197
00:49:37,549 --> 00:49:38,531
That's right.

198
00:49:39,372 --> 00:49:42,755
And there was a book, a journal.

199
00:49:42,989 --> 00:49:44,647
Did it belong to the boy?

200
00:49:45,452 --> 00:49:48,518
I don't think so.
I think it belonged to a woman.

201
00:49:51,724 --> 00:49:53,480
The writing looked feminine.

202
00:49:54,924 --> 00:49:57,542
Did you speak with the boy
after you opened the closet?

203
00:49:57,611 --> 00:49:58,560
Briefly.

204
00:49:58,956 --> 00:50:02,021
I asked his name. He said it was Toshio.

205
00:50:10,059 --> 00:50:11,815
One more question, please.

206
00:50:14,987 --> 00:50:17,092
We found this in the hallway.

207
00:50:20,330 --> 00:50:21,639
This is him.

208
00:50:21,706 --> 00:50:22,917
This is the boy.

209
00:50:28,522 --> 00:50:30,180
Detective Nakagawa?

210
00:50:30,794 --> 00:50:32,965
The whole time I was in that house...

211
00:50:34,057 --> 00:50:35,944
I felt something was wrong.

212
00:50:40,329 --> 00:50:42,020
What happened there?

213
00:50:50,376 --> 00:50:53,638
The bodies of the son and daughter-in-law...

214
00:50:53,704 --> 00:50:56,486
of the woman you were caring for
were found in the attic.

215
00:50:57,896 --> 00:51:01,889
It seems that the son killed his wife,
and then himself.

216
00:51:10,215 --> 00:51:13,248
Please excuse me for a moment.

217
00:52:33,762 --> 00:52:35,551
What are you doing here?

218
00:52:35,682 --> 00:52:39,326
I thought they weren't gonna let you go
until tonight. Did you escape?

219
00:52:40,097 --> 00:52:42,301
I just had to get out of there.

220
00:52:44,705 --> 00:52:46,494
I didn't wanna be alone.

221
00:52:49,217 --> 00:52:50,559
Okay, Karen.

222
00:52:52,353 --> 00:52:53,946
Hold on a second.

223
00:53:22,943 --> 00:53:24,536
What's wrong?

224
00:53:30,622 --> 00:53:31,964
Talk to me.

225
00:53:37,470 --> 00:53:40,023
I think I saw something in that house.

226
00:53:42,750 --> 00:53:45,116
Emma and I were alone in that room...

227
00:53:47,582 --> 00:53:50,167
but I think there was something else
there with us.

228
00:53:52,413 --> 00:53:53,755
Karen, what is it?

229
00:54:03,260 --> 00:54:04,984
I just wanna go home.

230
00:55:28,951 --> 00:55:30,064
Hello?

231
00:56:15,957 --> 00:56:17,680
What happened, Yoko?

232
01:00:11,078 --> 01:00:14,394
<i>My name is Karen Davis.
I'm really sorry to bother you.</i>

233
01:00:15,366 --> 01:00:18,049
I was wondering
if I could ask you a few questions.

234
01:00:18,118 --> 01:00:20,387
<i>Questions about what?</i>

235
01:00:20,773 --> 01:00:23,042
I realize this is really awkward.

236
01:00:24,134 --> 01:00:25,792
I'm here because...

237
01:00:28,421 --> 01:00:31,421
I need to ask you a few questions
about your husband.

238
01:00:34,181 --> 01:00:36,515
This is all his stuff from school.

239
01:00:40,292 --> 01:00:42,300
This is from our first date.

240
01:01:46,656 --> 01:01:47,604
My God.

241
01:01:50,304 --> 01:01:51,384
<i>Peter!</i>

242
01:01:52,224 --> 01:01:53,565
Hey, Shimizu.

243
01:01:55,584 --> 01:01:58,879
- Happy Monday.
- Yeah, right.

244
01:01:59,903 --> 01:02:01,278
Here's your mail.

245
01:02:03,424 --> 01:02:06,293
- Another one?
- Yeah.

246
01:02:07,103 --> 01:02:08,478
I have no idea.

247
01:02:08,543 --> 01:02:10,932
She says she used to be
in one of my classes...

248
01:02:11,006 --> 01:02:13,854
but I don't know who the hell she is.

249
01:02:14,238 --> 01:02:16,540
- Thanks.
- No problem.

250
01:02:17,278 --> 01:02:19,188
- Have a good one.
- Thanks.

251
01:03:50,617 --> 01:03:51,795
Hi.

252
01:06:15,376 --> 01:06:19,053
Detective Nakagawa,
I need to speak with you.

253
01:06:21,168 --> 01:06:24,910
The woman who was murdered in that house
three years ago knew this man.

254
01:06:25,328 --> 01:06:28,045
He committed suicide
the morning after she died.

255
01:06:28,783 --> 01:06:31,434
I don't believe he killed himself.

256
01:06:33,039 --> 01:06:35,973
I don't understand. What's happening?

257
01:06:38,638 --> 01:06:40,296
Three years ago...

258
01:06:40,943 --> 01:06:43,364
three of my colleagues, my friends...

259
01:06:44,783 --> 01:06:46,822
began investigating a case.

260
01:06:48,206 --> 01:06:51,501
Two died mysteriously,
and one disappeared.

261
01:06:53,870 --> 01:06:57,186
They had been investigating the murders
at that house.

262
01:06:58,541 --> 01:07:01,770
Are you saying the detectives died
because of that house?

263
01:07:03,917 --> 01:07:07,113
I've been in that house. So have you!

264
01:07:14,156 --> 01:07:15,717
It is said in Japan...

265
01:07:15,788 --> 01:07:18,952
that when a person dies
in extreme sorrow or rage...

266
01:07:20,364 --> 01:07:21,890
the emotion remains...

267
01:07:22,924 --> 01:07:25,226
becoming a stain upon that place.

268
01:07:27,787 --> 01:07:30,504
The memory of what happened
repeats itself there.

269
01:07:33,099 --> 01:07:35,433
Death becomes a part of that place...

270
01:07:36,939 --> 01:07:39,077
killing everything it touches.

271
01:07:42,059 --> 01:07:44,360
Once you have become a part of it...

272
01:07:46,282 --> 01:07:48,900
it will never let you go.

273
01:07:54,090 --> 01:07:55,367
I'm sorry.

274
01:08:12,713 --> 01:08:14,054
Corpses abandoned in garbage bag

275
01:08:14,120 --> 01:08:15,582
INVESTIGATION - KEEP OUT

276
01:08:47,911 --> 01:08:52,221
METROPOLITAN POLICE DEPARTMENT
KEEP OUT

277
01:11:02,366 --> 01:11:03,708
<i>Karen, it's me.</i>

278
01:11:03,934 --> 01:11:06,268
<i>Look, where have you been all day?</i>

279
01:11:07,422 --> 01:11:09,145
<i>I'm really worried about you.</i>

280
01:11:09,214 --> 01:11:10,621
<i>The Care Center called.</i>

281
01:11:10,878 --> 01:11:12,788
<i>Alex and Yoko are dead.</i>

282
01:11:14,398 --> 01:11:16,252
<i>I found your research and...</i>

283
01:11:16,830 --> 01:11:18,739
<i>Did you go to that house?</i>

284
01:11:21,661 --> 01:11:23,221
<i>I'm coming to find you.</i>

285
01:12:27,418 --> 01:12:29,490
- Did you get my message?
- Doug!

286
01:13:13,559 --> 01:13:14,639
Don't worry...

287
01:13:14,838 --> 01:13:16,300
It isn't any problem.

288
01:13:19,478 --> 01:13:21,387
I'll call you before I leave.

289
01:13:25,654 --> 01:13:26,766
Ten minutes.

290
01:13:27,382 --> 01:13:29,324
Yeah, I'll be home soon.

291
01:13:30,549 --> 01:13:32,076
Yeah, love you, too.

292
01:13:46,805 --> 01:13:48,692
Guess your mom's gonna be late.

