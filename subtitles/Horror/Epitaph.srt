1
00:00:17,851 --> 00:00:20,945
STUDIO 2.0 presents

2
00:00:21,788 --> 00:00:24,951
In Association with
KTB and KOFIC

3
00:00:25,825 --> 00:00:28,953
A Film Dorothy Production

4
00:01:53,279 --> 00:01:56,942
October, 1979

5
00:02:00,787 --> 00:02:05,520
This footage is
a record of the brain surgery

6
00:02:05,658 --> 00:02:10,595
performed in 1942
on General Hirai

7
00:02:10,730 --> 00:02:13,392
of the Japanese army.

8
00:02:13,533 --> 00:02:18,266
Dr. Kim Dong-Won
performed the operation,

9
00:02:18,404 --> 00:02:22,238
being the best neurosurgeon

10
00:02:22,375 --> 00:02:26,106
respected by the Japanese
medical society.

11
00:02:27,280 --> 00:02:29,339
That's all for today.

12
00:02:29,482 --> 00:02:30,449
JIN GOO

13
00:02:30,583 --> 00:02:32,642
As I mentioned before,

14
00:02:32,785 --> 00:02:36,243
today's footage
will be on the test.

15
00:02:36,389 --> 00:02:38,016
DONG-KYU LEE

16
00:02:38,158 --> 00:02:43,061
If you weren't paying attention,
get help from my assistant.

17
00:02:43,196 --> 00:02:44,163
KIM TAE-WOO

18
00:02:44,297 --> 00:02:47,232
Tell your friends at the protest,

19
00:02:47,367 --> 00:02:49,358
they'll be failing for sure...

20
00:02:49,502 --> 00:02:51,436
KIM BO-KYUNG

21
00:02:51,571 --> 00:02:53,436
...if they miss next week's test.

22
00:02:55,341 --> 00:02:57,002
It's Dad.

23
00:02:57,143 --> 00:02:59,703
Let's have lunch together.

24
00:02:59,846 --> 00:03:01,939
Oh, already?

25
00:03:02,081 --> 00:03:05,141
Are you busy?

26
00:03:05,285 --> 00:03:08,254
Dinner?
No, it's okay.

27
00:03:08,388 --> 00:03:11,983
I can take care of myself.
Don't come.

28
00:03:13,259 --> 00:03:16,160
Stubborn girl.

29
00:03:16,296 --> 00:03:18,161
The desk?

30
00:03:18,298 --> 00:03:20,562
Yes, what is it?

31
00:03:27,874 --> 00:03:32,140
It's really my old album.

32
00:03:35,048 --> 00:03:38,347
They found it at Anseng Hospital?

33
00:03:39,219 --> 00:03:43,155
After all these years?

34
00:03:47,760 --> 00:03:50,251
When are they tearing it down?

35
00:03:50,396 --> 00:03:52,125
The hospital.

36
00:03:53,566 --> 00:03:55,625
Oh, tonight.

37
00:03:55,768 --> 00:04:00,068
Since it's the weekend.

38
00:04:03,176 --> 00:04:06,873
All right.
See you at home, then.

39
00:04:07,013 --> 00:04:08,776
Okay.

40
00:04:17,490 --> 00:04:20,926
Directed by Jung Brothers

41
00:04:22,428 --> 00:04:25,295
You should get anything
you left inside.

42
00:04:25,431 --> 00:04:28,628
After tonight,
it will all be gone.

43
00:05:37,870 --> 00:05:39,098
Are you lonely, Dad?

44
00:05:40,206 --> 00:05:41,730
Huh?

45
00:05:44,444 --> 00:05:46,708
Of course not.

46
00:05:51,951 --> 00:05:53,612
The coffee tastes good.

47
00:05:53,753 --> 00:05:55,914
Dad.

48
00:05:56,055 --> 00:05:58,819
Don't be so hard on yourself.

49
00:06:00,059 --> 00:06:02,391
You know Stepmom
had heart problems

50
00:06:02,528 --> 00:06:05,588
even before the marriage.

51
00:06:05,732 --> 00:06:09,327
Yes, but your mother and all...

52
00:06:09,469 --> 00:06:12,302
Mom passed away
while having me.

53
00:06:12,438 --> 00:06:15,566
Stepmom had a chronic disease.

54
00:06:15,708 --> 00:06:17,676
Neither was your fault.

55
00:06:18,544 --> 00:06:20,603
It's just...

56
00:06:20,747 --> 00:06:23,910
...they both passed away
like that,

57
00:06:24,050 --> 00:06:26,848
within less than a year
of marrying me.

58
00:06:26,986 --> 00:06:28,715
Dad!

59
00:06:29,856 --> 00:06:30,948
I know. I know.

60
00:06:43,703 --> 00:06:47,662
I'll be back
from the in-laws' tomorrow.

61
00:06:47,807 --> 00:06:49,968
Then I'll stop by
with some food.

62
00:06:50,109 --> 00:06:55,843
Don't come so often.
I can just hire help.

63
00:06:56,749 --> 00:06:58,341
Otherwise, I'll miss you too much.

64
00:06:59,552 --> 00:07:01,747
I'll come by again with Yoon-Hee.

65
00:07:04,690 --> 00:07:09,821
That was the last time
I saw my daughter.

66
00:07:11,030 --> 00:07:14,727
I thought
I would forget with time.

67
00:07:14,867 --> 00:07:18,462
I thought
I had lived a quiet life.

68
00:07:18,604 --> 00:07:22,904
I see that those four days
37 years ago

69
00:07:23,042 --> 00:07:28,105
had made my life
an endless nightmare.

70
00:07:29,315 --> 00:07:30,942
On that night,

71
00:07:31,083 --> 00:07:32,812
I died.

72
00:07:39,592 --> 00:07:41,192
EPITAPH

73
00:07:46,499 --> 00:07:48,831
In February 1942,

74
00:07:48,968 --> 00:07:52,665
just as I began
studying medicine,

75
00:07:52,805 --> 00:07:55,638
the Japanese attacked
Pearl Harbor,

76
00:07:55,775 --> 00:07:59,142
and imperialism was at its peak.

77
00:07:59,278 --> 00:08:01,769
But inside Anseng Hospital,

78
00:08:01,914 --> 00:08:07,045
it was as quiet
as the eye of a storm.

79
00:08:09,856 --> 00:08:12,723
Do ghosts really appear
in the mirrors?

80
00:08:12,859 --> 00:08:13,848
Sure.

81
00:08:13,993 --> 00:08:16,427
Staring at mirrors in the morgue?

82
00:08:16,562 --> 00:08:17,859
Scary.

83
00:08:17,997 --> 00:08:21,228
But you better not
break that mirror.

84
00:08:21,367 --> 00:08:22,425
- Why?
- Why?

85
00:08:22,568 --> 00:08:24,536
Hey, the rounds.

86
00:08:24,670 --> 00:08:26,035
Let's go.

87
00:08:45,091 --> 00:08:46,786
Lucky bastard.

88
00:08:47,693 --> 00:08:49,593
What?

89
00:08:49,729 --> 00:08:53,756
While some of us
stay up for weeks,

90
00:08:53,900 --> 00:08:56,198
someone's going
to sit back and relax.

91
00:08:56,335 --> 00:08:58,895
But sure, you're
in high society now.

92
00:08:59,305 --> 00:09:01,273
Keep your voices down.

93
00:09:05,011 --> 00:09:06,239
Jung-Nam Park.

94
00:09:07,046 --> 00:09:08,946
You're on morgue duty
this whole week.

95
00:09:09,081 --> 00:09:11,049
- A week?
- A week?

96
00:09:11,183 --> 00:09:17,247
The refrigerating system is faulty,
so make sure to check it often.

97
00:09:17,390 --> 00:09:19,483
- Lucky.
- Congrats, Jung-Nam.

98
00:09:20,326 --> 00:09:24,353
Director's orders,
so don't fuss over it.

99
00:09:24,497 --> 00:09:25,725
Yes, sir.

100
00:09:27,066 --> 00:09:30,695
Aoi is back from Tokyo.

101
00:09:32,438 --> 00:09:33,405
I see.

102
00:09:34,140 --> 00:09:37,507
Do you remember her?

103
00:09:40,947 --> 00:09:43,040
Only that I saw her
when I was young.

104
00:09:44,183 --> 00:09:48,313
Right.
She's all grown up now.

105
00:09:48,688 --> 00:09:51,521
She's grown so beautifully.

106
00:09:53,125 --> 00:10:00,031
My husband and your parents
would have cared much for her.

107
00:10:00,600 --> 00:10:04,696
I'm just sorry
that they couldn't see

108
00:10:04,837 --> 00:10:07,271
two such fine young people marry.

109
00:10:07,406 --> 00:10:11,502
I'm always thankful for your care.

110
00:10:12,211 --> 00:10:15,840
Taking me in
after my parents passed away

111
00:10:15,982 --> 00:10:17,540
and supporting my education.

112
00:10:17,683 --> 00:10:21,244
I only did
what I thought was proper.

113
00:10:21,387 --> 00:10:25,414
I truly think of you as my own son,

114
00:10:25,558 --> 00:10:28,584
and not just
because of the betrothal.

115
00:10:29,261 --> 00:10:30,660
Thank you.

116
00:10:31,530 --> 00:10:37,025
I know it seems a bit rushed,
but we should have it in Spring.

117
00:10:38,604 --> 00:10:42,335
I'd like for you two
to marry right away,

118
00:10:42,475 --> 00:10:47,412
but it will take some time
to prepare things on our side.

119
00:10:48,781 --> 00:10:51,579
Anyway, that's my decision.

120
00:10:55,721 --> 00:10:56,915
Yes, Ma'am.

121
00:10:57,623 --> 00:10:59,090
AUTOPSY LAB

122
00:11:14,273 --> 00:11:17,538
Time of death
was about 30 hours ago.

123
00:11:18,010 --> 00:11:20,843
Judging by the odor, isn't it longer?

124
00:11:23,082 --> 00:11:27,610
But, of course, you must be right.

125
00:11:35,127 --> 00:11:37,687
A soldier was killed
while delivering documents

126
00:11:37,830 --> 00:11:39,263
from Busan to Seoul.

127
00:11:40,499 --> 00:11:44,697
The body was mangled,
but the papers left untouched.

128
00:11:46,772 --> 00:11:49,900
Who in the world
would do something like this?

129
00:11:51,711 --> 00:11:53,110
What was the motive?

130
00:11:56,716 --> 00:12:00,083
He's only been
in Korea for one week.

131
00:12:03,222 --> 00:12:06,851
Dr. Kaneda,
I'd like a word with you.

132
00:12:09,395 --> 00:12:12,421
A Japanese soldier was murdered,

133
00:12:12,565 --> 00:12:17,935
so the press will be determined
to get any information out of you.

134
00:12:18,871 --> 00:12:19,997
Therefore,

135
00:12:20,139 --> 00:12:24,075
don't comment on anything
in front of the reporters

136
00:12:24,210 --> 00:12:26,144
besides your medical opinion.

137
00:12:26,278 --> 00:12:30,738
I'll handle the difficult questions.

138
00:12:32,218 --> 00:12:34,049
As I mentioned before,

139
00:12:34,186 --> 00:12:38,850
the victim was stabbed
by a sharp and long metal object.

140
00:12:38,991 --> 00:12:40,686
Doctor!

141
00:12:40,826 --> 00:12:44,660
What led the killer
to mangle the body?

142
00:12:44,797 --> 00:12:46,594
Looking at how the body
was stabbed

143
00:12:46,732 --> 00:12:49,098
many times
after the victim had died,

144
00:12:49,235 --> 00:12:56,198
it seems the killer may have had
much hostility toward the victim.

145
00:13:02,114 --> 00:13:08,644
That student will need time
to get used to autopsy.

146
00:13:09,622 --> 00:13:15,686
By the way, are you ready
for your first lecture?

147
00:13:15,961 --> 00:13:19,897
Everyone says
they're a tough crowd,

148
00:13:20,032 --> 00:13:22,500
worse than us back in the day.

149
00:14:08,781 --> 00:14:10,408
I'm so tired.

150
00:14:16,255 --> 00:14:17,552
How's autopsy?

151
00:14:18,858 --> 00:14:21,884
I threw up everything
I ate this morning.

152
00:14:22,428 --> 00:14:24,293
Wimp.

153
00:14:24,430 --> 00:14:26,921
We've all been there.

154
00:14:27,466 --> 00:14:30,435
Never mind that.
What's with the murder case?

155
00:14:30,569 --> 00:14:33,629
Guys, another body just came in.

156
00:14:33,772 --> 00:14:34,739
Anyone seen it?

157
00:14:34,874 --> 00:14:35,932
Another one?

158
00:14:36,075 --> 00:14:37,269
The teenage girl?

159
00:14:37,409 --> 00:14:38,933
So you saw it.

160
00:14:39,078 --> 00:14:40,841
This girl's body just came in,

161
00:14:40,980 --> 00:14:44,245
all frozen up
like a piece of meat.

162
00:14:44,383 --> 00:14:46,977
I tell you, it's perfect.

163
00:14:47,119 --> 00:14:48,814
This one is a real looker.

164
00:14:48,954 --> 00:14:53,391
Almost like an angel
fallen from heaven.

165
00:15:40,339 --> 00:15:42,967
Put the mirror away.

166
00:15:49,782 --> 00:15:51,807
The door left open,

167
00:15:52,918 --> 00:15:55,546
and the bodies
still lying around...

168
00:15:57,489 --> 00:15:58,547
Jung-Nam Park.

169
00:16:01,427 --> 00:16:02,451
Yes, sir?

170
00:16:03,862 --> 00:16:05,386
You're scared,

171
00:16:06,799 --> 00:16:07,925
aren't you?

172
00:16:08,901 --> 00:16:10,528
No, sir.

173
00:16:16,976 --> 00:16:20,309
Clean these bodies up
and put them in.

174
00:16:20,913 --> 00:16:22,505
Yes, sir.

175
00:16:22,648 --> 00:16:25,947
Those chemicals will spill over.
Line them up right.

176
00:16:26,452 --> 00:16:27,544
Yes, sir.

177
00:16:28,120 --> 00:16:30,850
Don't waste time
doing stupid things.

178
00:19:34,439 --> 00:19:36,873
GHOSTS

179
00:19:40,512 --> 00:19:41,706
Professor.

180
00:19:43,482 --> 00:19:48,215
Then does that mean
you believe that ghosts exist?

181
00:20:12,110 --> 00:20:14,544
Don't you like the ring?

182
00:20:21,420 --> 00:20:22,978
I used to have one like this.

183
00:20:27,993 --> 00:20:31,360
A keepsake from my parents.

184
00:20:32,431 --> 00:20:40,065
But I lost it last year
while moving to Seoul, like an idiot.

185
00:20:41,873 --> 00:20:44,364
To be honest,

186
00:20:44,509 --> 00:20:47,808
I never thought
of becoming a doctor.

187
00:20:49,648 --> 00:20:51,775
You see, I liked to draw.

188
00:20:55,087 --> 00:20:57,988
In a few months,

189
00:20:58,123 --> 00:21:01,024
I'm marrying
the director's daughter,

190
00:21:01,159 --> 00:21:03,593
and I've never even met her.

191
00:21:06,965 --> 00:21:12,733
When I look at you,
you just look so happy.

192
00:21:13,772 --> 00:21:17,936
It's like your best years
have been sealed forever.

193
00:21:34,526 --> 00:21:35,493
ANSENG HOSPITAL

194
00:21:35,627 --> 00:21:38,596
Why are they coming out
in this weather?

195
00:21:39,765 --> 00:21:42,097
Why not? It's snowing.

196
00:21:42,234 --> 00:21:44,862
It's nice.

197
00:21:45,904 --> 00:21:47,428
Idiot.

198
00:21:48,707 --> 00:21:52,006
That's strange,
isn't it a bit late for snow?

199
00:21:52,144 --> 00:21:53,133
Jung-Nam!

200
00:21:53,278 --> 00:21:56,509
Why didn't you bring
your dead friends?

201
00:21:57,449 --> 00:22:01,909
Man, I just love blackouts.

202
00:22:03,955 --> 00:22:05,855
Please...

203
00:22:05,991 --> 00:22:08,721
Don't let them fix it,

204
00:22:08,860 --> 00:22:11,658
just for one night!

205
00:22:12,464 --> 00:22:15,399
Hey, the lights!
Damn it.

206
00:22:15,534 --> 00:22:17,092
No!

207
00:23:07,352 --> 00:23:08,478
I'm sorry.

208
00:23:08,620 --> 00:23:09,746
Jung-Nam!

209
00:23:09,888 --> 00:23:12,721
Leave us at least
a few bottles to use!

210
00:23:12,858 --> 00:23:15,725
Jung-Nam Park, are you okay?

211
00:23:15,861 --> 00:23:17,192
Yes, sir.

212
00:23:17,896 --> 00:23:21,923
She needs to realize
it's not her fault.

213
00:23:22,067 --> 00:23:26,026
Then she'll come
to accept her parents' death.

214
00:23:26,171 --> 00:23:27,536
I'm sorry, sir.

215
00:23:27,672 --> 00:23:30,732
Let's try medications
and psychoanalysis treatment.

216
00:23:30,876 --> 00:23:32,969
Yes, sir.

217
00:24:15,554 --> 00:24:16,782
Jung-Nam?

218
00:24:20,325 --> 00:24:22,691
Here you are.

219
00:24:22,828 --> 00:24:25,524
I was looking for you.

220
00:24:32,103 --> 00:24:38,372
She's beautiful, isn't she?

221
00:24:39,077 --> 00:24:40,442
Yes, Ma'am.

222
00:24:40,912 --> 00:24:42,675
They still haven't found the body

223
00:24:42,814 --> 00:24:44,941
of the man
she committed suicide with.

224
00:24:48,019 --> 00:24:51,420
It took over four days
just to search.

225
00:24:52,924 --> 00:24:55,051
How cold she must have been...

226
00:24:57,729 --> 00:25:01,961
Anyway, what was the time
when you were born?

227
00:25:02,100 --> 00:25:06,298
Oh, about 2 a.m.
On June 3rd, 1920.

228
00:25:06,438 --> 00:25:10,670
I see, 2 a.m.
On June 3rd, 1920.

229
00:25:10,809 --> 00:25:16,441
That was why I came to see you,
but here I am rambling on.

230
00:26:02,460 --> 00:26:04,792
What's that sound?

231
00:26:04,930 --> 00:26:07,194
It's so creepy.

232
00:26:07,332 --> 00:26:09,857
Who is sobbing
at this time of night?

233
00:26:10,735 --> 00:26:13,431
It's coming from the funeral home.

234
00:28:13,958 --> 00:28:16,927
Where did the drawing go?

235
00:33:13,057 --> 00:33:17,960
I don't know
how to thank you, Sister.

236
00:33:18,763 --> 00:33:20,663
Am I being greedy

237
00:33:20,798 --> 00:33:25,861
by marrying her dead soul
to his living body?

238
00:33:26,871 --> 00:33:28,361
Perhaps so.

239
00:33:28,506 --> 00:33:37,107
But I can't let her soul reunite
with the man that made her die.

240
00:34:07,412 --> 00:34:11,849
This child was brought in
two days ago,

241
00:34:11,983 --> 00:34:14,679
after barely surviving
a car accident.

242
00:34:16,888 --> 00:34:19,015
What's wrong with you?

243
00:34:19,157 --> 00:34:21,148
Get over here and grab her!

244
00:34:27,999 --> 00:34:29,057
Come on!

245
00:34:33,171 --> 00:34:34,160
Look for injuries!

246
00:34:34,305 --> 00:34:35,272
Yes, sir!

247
00:34:35,406 --> 00:34:39,274
2 days ago

248
00:34:44,348 --> 00:34:45,474
Hold her down!

249
00:35:02,834 --> 00:35:05,325
Poor girl.

250
00:35:05,470 --> 00:35:09,065
She has to survive,
at least for her mother's sake.

251
00:35:10,575 --> 00:35:12,304
That's her mother?

252
00:35:14,245 --> 00:35:15,803
Yes.

253
00:35:26,624 --> 00:35:28,819
There aren't any injuries.

254
00:36:38,763 --> 00:36:39,787
It's like a miracle.

255
00:36:42,967 --> 00:36:44,798
A miracle?

256
00:36:44,936 --> 00:36:45,960
What is?

257
00:36:47,471 --> 00:36:50,497
She survived without even a bruise

258
00:36:50,641 --> 00:36:55,101
in a crash
that took her whole family.

259
00:36:57,348 --> 00:36:59,646
They say life is up to fate.

260
00:37:00,551 --> 00:37:03,452
Things aren't always
for us to decide.

261
00:37:08,125 --> 00:37:10,821
There it goes again.

262
00:38:38,716 --> 00:38:40,581
Mommy?

263
00:39:30,434 --> 00:39:33,130
If she wasn't hurt at all,

264
00:39:33,270 --> 00:39:37,036
then why the nightmares
and aphasia?

265
00:39:37,408 --> 00:39:43,108
Just seeing her parents die
is enough shock for a little girl.

266
00:39:43,247 --> 00:39:49,584
And she was the sole survivor,
which might make her feel guilty.

267
00:40:40,871 --> 00:40:42,338
Mommy.

268
00:41:27,451 --> 00:41:29,146
I love you.

269
00:41:30,754 --> 00:41:34,053
I love you, too.

270
00:41:42,600 --> 00:41:45,194
Since it's your first day,
we'll stop here.

271
00:41:50,207 --> 00:41:51,674
Asako.

272
00:41:52,810 --> 00:41:57,338
Can you remember
what you told me?

273
00:42:01,685 --> 00:42:04,552
It's okay.

274
00:42:08,792 --> 00:42:12,091
When I was five years old,

275
00:42:12,229 --> 00:42:17,098
there was a deserted house
a few blocks from my house.

276
00:42:17,234 --> 00:42:23,605
My brother and I went there
to take some eggs

277
00:42:23,741 --> 00:42:26,904
that a pheasant had laid
inside the well.

278
00:42:27,044 --> 00:42:29,774
He told me to stay back,

279
00:42:29,914 --> 00:42:33,145
but I didn't listen
and leaned over,

280
00:42:33,284 --> 00:42:35,275
and I fell down the well.

281
00:42:35,886 --> 00:42:40,789
It was deeper than I thought,
and the water had all dried up.

282
00:42:42,927 --> 00:42:49,196
I kept crying and calling for him
because my right leg was hurting.

283
00:42:49,333 --> 00:42:54,999
He tried to climb down a rope,
but he fell in, as well.

284
00:42:55,906 --> 00:42:59,967
I was so scared,
but he kept telling me

285
00:43:00,110 --> 00:43:03,477
that we were sure to be rescued

286
00:43:03,614 --> 00:43:05,912
and not to worry.

287
00:43:06,951 --> 00:43:12,116
I did what he said
and survived on eggs and water.

288
00:43:12,256 --> 00:43:17,387
And just like he had said,
people came to rescue us.

289
00:43:17,528 --> 00:43:21,191
But they said

290
00:43:21,332 --> 00:43:24,130
that he was already dead,

291
00:43:25,869 --> 00:43:29,771
still holding on to my hand.

292
00:43:31,175 --> 00:43:32,972
People still say...

293
00:43:33,777 --> 00:43:35,802
...on that night...

294
00:43:35,946 --> 00:43:39,404
...he was the reason I survived.

295
00:43:41,952 --> 00:43:44,978
I know what you're going through.

296
00:43:47,424 --> 00:43:49,415
I was in that place before.

297
00:43:51,929 --> 00:43:53,863
Try to feel better, Asako.

298
00:43:57,234 --> 00:43:59,566
Everything will be fine.

299
00:44:20,758 --> 00:44:21,918
Koshiro?

300
00:44:33,003 --> 00:44:34,197
Koshiro!

301
00:44:37,708 --> 00:44:38,697
Koshi...

302
00:46:08,899 --> 00:46:09,866
Koshiro!

303
00:46:23,847 --> 00:46:26,714
From now on,
you should call me Dad.

304
00:46:33,090 --> 00:46:35,183
- Hurry up, Han!
- Yes, sir!

305
00:46:45,536 --> 00:46:47,766
Asako.

306
00:46:47,905 --> 00:46:50,931
Let's have one more talk together,

307
00:46:51,074 --> 00:46:54,669
and you won't have
nightmares anymore.

308
00:46:57,281 --> 00:46:59,909
It's just like yesterday.

309
00:47:00,050 --> 00:47:01,449
Okay.

310
00:47:01,585 --> 00:47:03,815
Just relax.

311
00:47:09,726 --> 00:47:10,920
Don't worry.

312
00:47:11,862 --> 00:47:14,854
I'll be right here, next to you.

313
00:47:19,036 --> 00:47:23,996
Right now,
you're going to meet that man.

314
00:47:24,141 --> 00:47:27,542
The man that you love.

315
00:47:28,212 --> 00:47:33,809
Let's go now
to the time you first met him.

316
00:47:36,286 --> 00:47:37,253
Over there.

317
00:47:42,659 --> 00:47:44,354
Asako.

318
00:47:44,494 --> 00:47:46,860
It's your new dad.

319
00:47:55,339 --> 00:47:57,773
Let's say hello.

320
00:48:16,727 --> 00:48:18,695
I'm Koshiro Onji.

321
00:48:18,829 --> 00:48:20,296
It's nice to meet you.

322
00:49:17,454 --> 00:49:20,355
I love you too, Koshiro!

323
00:49:20,657 --> 00:49:22,215
I love you too.

324
00:49:24,628 --> 00:49:27,995
Asako, you should call him Dad.

325
00:49:33,670 --> 00:49:35,137
I'm going to sleep with Koshiro.

326
00:49:37,607 --> 00:49:40,132
Stop fooling around, Asako.

327
00:49:40,277 --> 00:49:42,541
Koshiro is driving.

328
00:49:42,679 --> 00:49:45,204
That's enough.

329
00:49:45,349 --> 00:49:46,782
Sit down.

330
00:49:47,951 --> 00:49:49,942
I wish you would just go away.

331
00:49:54,257 --> 00:49:56,919
Asako, come on.

332
00:49:57,060 --> 00:49:59,460
What are you doing?

333
00:49:59,596 --> 00:50:03,965
Why do you get to sleep with him,
and why do you get to love him?

334
00:50:05,902 --> 00:50:08,302
I hate you! Just go away!

335
00:50:08,438 --> 00:50:09,962
I'm going to live with Koshiro!

336
00:50:15,779 --> 00:50:17,406
Asako!

337
00:51:09,933 --> 00:51:10,900
It's...

338
00:51:12,936 --> 00:51:16,099
It's all my fault.

339
00:51:28,919 --> 00:51:31,149
It's not your fault, Asako.

340
00:51:37,894 --> 00:51:40,829
The emotions
trapped in her subconscious

341
00:51:40,964 --> 00:51:43,524
have been brought out and resolved.

342
00:51:43,667 --> 00:51:45,794
She also has regained
her consciousness.

343
00:51:47,304 --> 00:51:50,740
I think I've done everything I can.

344
00:51:53,076 --> 00:51:54,407
Ma'am?

345
00:51:55,545 --> 00:51:57,240
I see.

346
00:51:58,448 --> 00:52:01,645
That's good to hear.
Good work.

347
00:52:02,552 --> 00:52:06,648
Asako's mother
can rest in peace now.

348
00:52:07,524 --> 00:52:11,756
Please take care of her
from now on.

349
00:52:11,895 --> 00:52:13,192
I am counting on you.

350
00:52:14,498 --> 00:52:16,591
Yes, Ma'am.

351
00:52:28,111 --> 00:52:31,171
You'll be okay now, Asako.

352
00:52:31,948 --> 00:52:35,384
I'll finish up my rounds quickly,
and then I'll stay here with you.

353
00:52:35,519 --> 00:52:37,919
Everything will be fine, okay?

354
00:52:47,430 --> 00:52:49,557
She was fine just a minute ago.

355
00:52:49,699 --> 00:52:52,224
- Any emergency measures?
- Done, sir!

356
00:52:54,638 --> 00:52:56,003
Go on ahead!

357
00:53:10,320 --> 00:53:11,287
Doctor!

358
00:53:14,191 --> 00:53:16,250
- Get a syringe ready.
- Yes, sir.

359
00:53:16,660 --> 00:53:18,025
Asako?

360
00:53:20,830 --> 00:53:22,388
Asako! Come back!

361
00:53:37,347 --> 00:53:39,577
Open your eyes! Asako!

362
00:53:59,936 --> 00:54:02,166
Mom...

363
00:54:07,010 --> 00:54:10,138
It's okay.

364
00:54:12,382 --> 00:54:18,446
It's not your fault, Asako.

365
00:54:46,416 --> 00:54:47,383
Doctor.

366
00:54:48,118 --> 00:54:50,086
Asako!

367
00:54:50,620 --> 00:54:52,520
Sir!

368
00:54:53,923 --> 00:54:56,619
She's gone.

369
00:55:01,231 --> 00:55:04,667
You did your best, sir.

370
00:55:18,982 --> 00:55:23,646
They say death heals all wounds.

371
00:55:25,255 --> 00:55:27,280
In that last moment,

372
00:55:27,424 --> 00:55:31,292
what could have given her
such peace?

373
00:55:31,795 --> 00:55:35,492
Perhaps Asako
had stayed a bit longer

374
00:55:35,632 --> 00:55:39,363
in order to find that peace.

375
00:57:46,463 --> 00:57:47,987
AUTOPSY LAB

376
00:57:53,236 --> 00:57:57,138
3 days ago

377
00:58:09,486 --> 00:58:11,852
Time of death
was about 30 hours ago.

378
00:58:17,660 --> 00:58:19,685
A soldier was killed
while delivering documents

379
00:58:19,829 --> 00:58:21,854
from Busan to Seoul.

380
00:58:23,366 --> 00:58:27,598
The body was mangled,
but the papers left untouched.

381
00:58:29,506 --> 00:58:34,273
Who in the world
would do something like this?

382
00:58:34,744 --> 00:58:37,144
What was the motive?

383
00:58:42,352 --> 00:58:44,946
I still didn't know back then

384
00:58:45,088 --> 00:58:51,425
the fact that my wife
didn't have a shadow.

385
00:58:54,063 --> 00:59:00,332
That student will need time
to get used to autopsy.

386
00:59:01,204 --> 00:59:07,109
By the way, are you ready
for your first lecture?

387
00:59:07,777 --> 00:59:11,736
Everyone says
they're a tough crowd,

388
00:59:11,881 --> 00:59:15,339
worse than us back in the day.

389
00:59:18,788 --> 00:59:20,153
In those times,

390
00:59:20,290 --> 00:59:25,557
all scientifically
inexplicable phenomenon

391
00:59:25,695 --> 00:59:28,061
was accounted for as superstition,

392
00:59:28,197 --> 00:59:31,564
but with various
cultural characteristics.

393
00:59:31,701 --> 00:59:34,033
For example, in the East,

394
00:59:34,170 --> 00:59:38,129
being haunted
or possessed was actually

395
00:59:38,274 --> 00:59:42,643
a cultural interpretation
of multiple personality disorder.

396
00:59:44,147 --> 00:59:49,676
What about the ghosts
in haunted houses?

397
00:59:51,120 --> 00:59:52,712
It's an interesting question.

398
00:59:55,925 --> 00:59:57,525
Ghosts.

399
01:00:06,669 --> 01:00:11,368
They're believed to be
the souls of the dead.

400
01:00:14,811 --> 01:00:18,338
A western scientist
did something interesting,

401
01:00:18,481 --> 01:00:21,678
where he attempted
to weigh a person

402
01:00:21,818 --> 01:00:24,048
just before and after death.

403
01:00:24,787 --> 01:00:29,724
You would expect
the weight to be the same.

404
01:00:29,859 --> 01:00:31,258
But oddly enough,

405
01:00:31,394 --> 01:00:38,732
there was a minute difference
between the two measurements.

406
01:00:39,702 --> 01:00:43,069
Based upon the result,
some people say

407
01:00:43,206 --> 01:00:45,800
that this difference
is the weight of the soul.

408
01:00:47,110 --> 01:00:49,977
It's certainly difficult
to explain scientifically.

409
01:00:50,113 --> 01:00:51,341
Professor!

410
01:00:52,548 --> 01:00:56,780
Then does that mean
you believe that ghosts exist?

411
01:00:58,521 --> 01:01:02,890
Well, probably not the ones
in haunted houses.

412
01:01:03,826 --> 01:01:06,795
Like most of you,
I haven't actually seen them.

413
01:01:07,563 --> 01:01:09,963
However,

414
01:01:10,099 --> 01:01:12,897
I would like to believe
that we have souls.

415
01:01:13,036 --> 01:01:16,199
It would be too depressing
if we didn't.

416
01:01:16,339 --> 01:01:18,364
Wouldn't you agree?

417
01:06:04,627 --> 01:06:07,619
It was a Korean soldier
that escaped four days ago.

418
01:06:31,153 --> 01:06:34,247
It's the same as the previous cases.

419
01:06:34,390 --> 01:06:38,793
Yes, both the weapon and method.

420
01:06:41,831 --> 01:06:43,662
He was just a child.

421
01:06:44,000 --> 01:06:46,992
A serial killer
after Japanese soldiers...

422
01:06:49,171 --> 01:06:54,302
Until the investigation progresses,
please keep this case quiet.

423
01:06:55,644 --> 01:06:57,475
You too, Jung-Nam.

424
01:06:57,613 --> 01:06:58,875
Yes, sir.

425
01:06:59,482 --> 01:07:00,642
Jung-Nam!

426
01:07:00,783 --> 01:07:03,343
Why didn't you bring
your dead friends?

427
01:07:03,486 --> 01:07:07,183
Someone made some rice snacks.

428
01:07:07,323 --> 01:07:10,156
Try some, Doctor.

429
01:07:43,459 --> 01:07:44,926
In-Young,

430
01:07:46,762 --> 01:07:49,697
did you go anywhere last night?

431
01:07:50,332 --> 01:07:51,560
Pardon?

432
01:07:52,835 --> 01:07:54,427
Never mind.

433
01:07:56,305 --> 01:08:01,368
Did you have any strange dreams?

434
01:08:03,679 --> 01:08:05,203
I'm so tired these days.

435
01:08:05,347 --> 01:08:09,374
I can't remember how I got to bed,
let alone my dreams.

436
01:08:15,758 --> 01:08:17,589
Do you like it that much?

437
01:08:18,461 --> 01:08:23,364
These two,
don't they look like us?

438
01:08:23,499 --> 01:08:25,967
When we're outside in the garden.

439
01:08:32,508 --> 01:08:33,839
I'll say.

440
01:08:35,377 --> 01:08:39,074
Even the streetlight looks the same.

441
01:08:40,749 --> 01:08:42,910
Hold on.

442
01:10:09,305 --> 01:10:10,272
Honey?

443
01:10:10,406 --> 01:10:11,373
Wait.

444
01:10:13,342 --> 01:10:14,673
Just stay still.

445
01:10:44,940 --> 01:10:46,032
Honey.

446
01:10:46,175 --> 01:10:48,643
Walk slowly towards me.

447
01:11:19,775 --> 01:11:21,436
You...

448
01:12:23,272 --> 01:12:24,398
In-Young.

449
01:12:29,645 --> 01:12:32,239
One year ago,

450
01:12:32,381 --> 01:12:35,578
In-Young died in Japan.

451
01:12:36,352 --> 01:12:38,820
How could I have forgotten?

452
01:12:42,758 --> 01:12:44,487
And...

453
01:12:45,961 --> 01:12:50,557
How can she be standing before me?

454
01:13:45,654 --> 01:13:51,286
Dear, don't ever leave me.

455
01:13:53,228 --> 01:13:57,961
I'll be with you even after I die.

456
01:13:58,834 --> 01:14:05,797
Can you promise me
that you'll never leave me?

457
01:14:11,046 --> 01:14:14,709
Why would I ever leave you?

458
01:14:14,850 --> 01:14:20,618
I'll stand by you,
even in the afterlife.

459
01:16:59,615 --> 01:17:01,173
In-Young.

460
01:17:10,192 --> 01:17:12,683
Don't do it.

461
01:17:16,031 --> 01:17:18,522
I know...

462
01:17:20,402 --> 01:17:22,802
...that you're in pain.

463
01:17:28,410 --> 01:17:29,877
But...

464
01:17:31,546 --> 01:17:35,038
...we were always together.

465
01:17:36,318 --> 01:17:37,910
Stop now, honey.

466
01:17:39,721 --> 01:17:43,623
I'll take care of this.

467
01:17:53,568 --> 01:17:56,469
Give that to me.

468
01:18:08,817 --> 01:18:10,580
Now!

469
01:18:11,553 --> 01:18:12,520
In-Young!

470
01:18:12,654 --> 01:18:14,053
No!

471
01:18:56,598 --> 01:18:57,656
Nurse Choi!

472
01:18:57,799 --> 01:18:59,391
Nurse Choi!

473
01:19:38,473 --> 01:19:39,906
In-Young.

474
01:20:10,639 --> 01:20:14,040
Do you like it that much?

475
01:20:17,746 --> 01:20:19,907
What's on your mind?

476
01:20:20,549 --> 01:20:22,847
I was waiting for you,

477
01:20:24,686 --> 01:20:26,984
worrying if you had left me.

478
01:20:32,627 --> 01:20:34,026
In-Young.

479
01:20:34,162 --> 01:20:36,392
Let's go back to Japan.

480
01:20:37,265 --> 01:20:38,755
Your arm...

481
01:20:52,714 --> 01:20:53,738
What happened?

482
01:20:57,586 --> 01:20:59,144
Why are these cuts?

483
01:21:39,027 --> 01:21:40,187
In-Young.

484
01:21:48,403 --> 01:21:49,631
Major!

485
01:21:51,706 --> 01:21:54,368
It's a nurse from Anseng Hospital.

486
01:21:54,509 --> 01:21:59,173
Why did the killer leave her body
on such a busy road?

487
01:21:59,314 --> 01:22:02,215
In order to conceal it, of course.

488
01:22:02,350 --> 01:22:05,547
It was dark and foggy that night,

489
01:22:05,687 --> 01:22:07,951
and if you're in a hurry,

490
01:22:09,858 --> 01:22:16,923
this may look like quite a cliff
when looking down from up there.

491
01:22:18,266 --> 01:22:20,359
But...

492
01:22:22,070 --> 01:22:23,765
...why is it a nurse this time?

493
01:22:24,239 --> 01:22:26,799
Since it wasn't a Japanese soldier,

494
01:22:26,942 --> 01:22:31,276
maybe someone copied
the serial killer.

495
01:22:31,813 --> 01:22:33,974
No.

496
01:22:34,115 --> 01:22:36,879
It doesn't look different to me.

497
01:22:37,018 --> 01:22:38,212
It's the same killer.

498
01:22:42,457 --> 01:22:45,517
It was next to the body, sir.

499
01:22:53,535 --> 01:22:58,472
Was the Korean soldier
wearing a jacket?

500
01:22:58,606 --> 01:23:00,597
No, he wasn't, sir.

501
01:23:06,081 --> 01:23:09,949
This is Dong-Won Kim
from Anseng Hospital.

502
01:23:13,121 --> 01:23:20,755
I'm the culprit
of the recent murders.

503
01:23:21,096 --> 01:23:22,757
Please come and arrest me.

504
01:23:24,632 --> 01:23:29,365
- If Kim shows up, keep him here.
- Yes, sir!

505
01:23:34,976 --> 01:23:37,877
You're sure it was Dong-Won Kim?

506
01:23:38,013 --> 01:23:39,105
Yes.

507
01:23:40,648 --> 01:23:41,876
Let's go.

508
01:24:20,588 --> 01:24:27,721
It may be hard to understand,
but there are two people inside me.

509
01:24:27,862 --> 01:24:30,092
Dong-Won Kim and a murderer.

510
01:24:31,032 --> 01:24:32,499
In short,

511
01:24:32,634 --> 01:24:35,967
the culprit
of the recent serial killings

512
01:24:36,104 --> 01:24:38,197
is me, Dong-Won Kim.

513
01:24:38,339 --> 01:24:41,399
Dr. Dong-Won Kim?

514
01:24:43,578 --> 01:24:45,375
Furthermore,

515
01:24:45,513 --> 01:24:49,210
do not believe anything I say or do.

516
01:24:49,350 --> 01:24:53,878
I state again, the serial killer...

517
01:24:54,022 --> 01:24:55,887
...is me.

518
01:25:13,575 --> 01:25:16,305
Go and search the hospital.

519
01:25:16,444 --> 01:25:18,207
I will go meet Dr. Kaneda.

520
01:25:18,346 --> 01:25:19,745
Yes, sir.

521
01:25:35,930 --> 01:25:37,591
Dr. Kaneda.

522
01:25:42,470 --> 01:25:44,267
Dr. Kaneda.

523
01:26:04,192 --> 01:26:05,784
Dr. Kaneda.

524
01:26:10,565 --> 01:26:11,532
Dr. Kaneda.

525
01:26:14,235 --> 01:26:15,600
What brings you here?

526
01:26:16,104 --> 01:26:18,868
I guess Dr. Kim isn't in?

527
01:26:19,007 --> 01:26:23,034
He hasn't come home yet.
He's running late today.

528
01:26:23,178 --> 01:26:25,476
I see.

529
01:26:25,613 --> 01:26:30,243
How long have you two
been married?

530
01:26:31,519 --> 01:26:33,817
Almost a year now.

531
01:26:33,955 --> 01:26:36,321
I see.

532
01:26:37,559 --> 01:26:40,824
Come in and have a seat.
I'll get you a cup of tea.

533
01:27:10,458 --> 01:27:12,392
Insane.

534
01:27:14,162 --> 01:27:15,993
Completely insane.

535
01:27:18,700 --> 01:27:19,894
Doctor.

536
01:27:21,169 --> 01:27:22,568
Yes?

537
01:27:23,571 --> 01:27:27,735
I've heard that back in Japan,
Dr. Kim...

538
01:27:44,392 --> 01:27:46,451
Yes, the culprit got away.

539
01:27:46,594 --> 01:27:49,028
Send somebody
to Anseng Hospital now.

540
01:27:50,965 --> 01:27:52,694
Yoshida.

541
01:27:52,834 --> 01:27:54,199
Yes, sir!

542
01:27:55,937 --> 01:27:57,871
Gather up all of the patients.

543
01:27:58,006 --> 01:27:59,200
Yes, sir!

544
01:28:05,079 --> 01:28:08,571
Who else is in the hospital
right now?

545
01:28:08,716 --> 01:28:11,913
The patients
and the director...

546
01:28:12,053 --> 01:28:15,352
One intern went to do an autopsy.

547
01:28:15,790 --> 01:28:17,485
Autopsy?

548
01:28:17,625 --> 01:28:18,592
Yes.

549
01:28:22,930 --> 01:28:26,161
What in the?

550
01:28:31,372 --> 01:28:32,339
Nurse Choi!

551
01:28:36,711 --> 01:28:38,906
I never ordered an autopsy.

552
01:28:39,047 --> 01:28:40,480
Damn it!

553
01:28:48,923 --> 01:28:50,117
What's going on?

554
01:28:50,258 --> 01:28:51,657
Woo-Shik!

555
01:28:53,961 --> 01:28:56,191
- It's Dr. Lee!
- Doctor!

556
01:28:56,764 --> 01:28:59,255
What happened?

557
01:29:00,668 --> 01:29:02,863
- A hit and run.
- What do we do?

558
01:29:03,004 --> 01:29:05,097
None of the doctors are in.

559
01:29:05,873 --> 01:29:07,135
Get him to emergency.

560
01:29:07,275 --> 01:29:08,867
Jung-Nam, get the director.

561
01:29:09,010 --> 01:29:09,999
Okay.

562
01:29:19,887 --> 01:29:20,854
Aoi.

563
01:29:22,023 --> 01:29:24,992
I already miss you so.

564
01:29:47,782 --> 01:29:50,012
I love you.

565
01:30:13,775 --> 01:30:14,742
Ma'am.

566
01:30:24,852 --> 01:30:26,843
Put it down.

567
01:30:28,322 --> 01:30:30,187
Put that knife down now!

568
01:30:35,663 --> 01:30:36,823
I said, now!

569
01:30:50,311 --> 01:30:51,505
Major.

570
01:30:53,748 --> 01:30:56,808
Please make me stop.

571
01:30:57,852 --> 01:31:04,849
In-Young, my dead wife,
is inside me.

572
01:31:05,626 --> 01:31:07,890
Please.

573
01:31:08,095 --> 01:31:09,995
Please, just stop me!

574
01:31:10,631 --> 01:31:12,826
You're not Dong-Won Kim.

575
01:31:12,967 --> 01:31:15,993
Your husband
died last year in Japan!

576
01:31:18,105 --> 01:31:21,438
He's just a false image
you created!

577
01:31:22,210 --> 01:31:25,270
You are In-Young Kim!

578
01:31:57,311 --> 01:32:01,008
They're believed to be
the souls of the dead.

579
01:32:23,204 --> 01:32:27,197
However, I would like to believe
that we have souls.

580
01:32:43,591 --> 01:32:47,550
It would be too depressing
if we didn't.

581
01:32:48,496 --> 01:32:50,396
Wouldn't you agree?

582
01:33:10,952 --> 01:33:12,977
Give it up now.

583
01:33:28,636 --> 01:33:29,864
Stop!

584
01:34:31,165 --> 01:34:33,656
How lonesome...

585
01:35:24,685 --> 01:35:28,485
I haven't seen you ever since.

586
01:35:29,690 --> 01:35:36,721
But I know that you
were always by my side.

587
01:35:55,583 --> 01:35:57,210
Tell me.

588
01:35:59,487 --> 01:36:04,550
If you were going to leave me
all alone,

589
01:36:08,229 --> 01:36:15,192
then why didn't you just
take me with you that night?

590
01:38:40,180 --> 01:38:45,743
Back then, we believed
everything would last forever.