﻿1
00:03:13,604 --> 00:03:16,364
Forgive me.
Ok

2
00:03:17,605 --> 00:03:21,405
Which of you nuns
violated my first commandment?

3
00:03:23,646 --> 00:03:28,606
You do not steal from Chavo.
No.

4
00:03:47,529 --> 00:03:49,610
Did you steal from Chavo?

5
00:03:52,050 --> 00:03:53,570
You can tell me, mother.

6
00:03:54,731 --> 00:03:56,412
I won't hurt you.

7
00:03:57,492 --> 00:04:00,212
Yes or no?

8
00:04:01,773 --> 00:04:03,013
No.

9
00:04:09,414 --> 00:04:11,134
Now, you!

10
00:04:13,935 --> 00:04:17,935
Did you steal from Chavo?
No? You lie to me ?

11
00:04:18,335 --> 00:04:20,896
- You lie to Chavo?
- No.

12
00:04:21,404 --> 00:04:22,524
Hey-hey-hey-hey!!

13
00:04:23,177 --> 00:04:24,616
Where d'you go?

14
00:04:26,896 --> 00:04:29,618
Where are you going?
Come here.

15
00:04:54,822 --> 00:04:56,223
Take her.

16
00:04:58,023 --> 00:04:59,542
For your troubles.

17
00:05:04,104 --> 00:05:06,665
Thank you.
Let's go.

18
00:05:24,228 --> 00:05:27,028
From Father Bernardo.

19
00:05:30,589 --> 00:05:32,429
Nice.

20
00:05:37,590 --> 00:05:40,831
Go ahead. Pick the one you want.
It's on the house.

21
00:05:45,271 --> 00:05:46,631
You want the special?

22
00:06:06,476 --> 00:06:08,275
Very good.

23
00:06:10,076 --> 00:06:11,636
I go get her ready for you.

24
00:06:27,559 --> 00:06:29,018
Will you hurry up and stick this bitch?!

25
00:06:29,118 --> 00:06:31,735
Got a guy waiting to pay
triple for this holy piece of ass.

26
00:06:31,920 --> 00:06:35,361
- Who knew nun pussy was in such demand?
- I told you for the last time.

27
00:06:35,921 --> 00:06:37,641
She can't handle anymore.

28
00:06:37,921 --> 00:06:41,242
- Use a different girl.
- No, no, no.

29
00:06:41,361 --> 00:06:43,214
He already knows that she's a real nun.

30
00:06:43,314 --> 00:06:46,221
Not just some filthy street whore dressed up like one.

31
00:06:46,523 --> 00:06:48,276
- I can't help you.

32
00:06:50,968 --> 00:06:53,732
I pay you good money
to do this. So do it!

33
00:06:53,964 --> 00:06:57,045
I may be a lot of things
but I'm no murderer.

34
00:06:58,245 --> 00:07:01,606
Fine.
I'll do it.

35
00:07:46,174 --> 00:07:48,574
That motherfucker Padre Bernardo...

36
00:07:50,534 --> 00:07:52,215
God bless him.

37
00:07:54,415 --> 00:07:57,776
You think that's funny?
Come, laugh with me.

38
00:08:02,777 --> 00:08:05,857
Damn!
Another one.

39
00:08:24,741 --> 00:08:27,622
You dont care how much
you pay for a nun...

40
00:09:02,068 --> 00:09:04,468
Get rid of them. Now.

41
00:10:41,687 --> 00:10:45,806
I was certain God came to me.

42
00:10:45,927 --> 00:10:48,887
He said: I have a better purpose in life.

43
00:10:49,807 --> 00:10:51,488
God didn't talk to you.

44
00:10:52,088 --> 00:10:55,489
It was just a bad reaction
about that shit in your body.

45
00:10:56,689 --> 00:11:00,690
- What did God tell you.
- He told me to kill.

46
00:11:00,810 --> 00:11:05,050
- To kill who?
- All  who sin against him.

47
00:11:05,170 --> 00:11:08,291
God told you to start going
around killing people?

48
00:11:09,011 --> 00:11:11,452
Shit.
You must have been tripping badly..

49
00:11:11,972 --> 00:11:15,013
Because, see, I gave you the good stuff.

50
00:11:15,652 --> 00:11:17,693
He wanted me to go away.

51
00:11:18,253 --> 00:11:21,014
To purify my mind
only with his words.

52
00:11:21,734 --> 00:11:25,295
Only then could I be prepared
to do his vengeance

53
00:11:36,016 --> 00:11:37,497
Here they are.

54
00:11:47,218 --> 00:11:48,579
For you.

55
00:11:53,660 --> 00:11:56,180
If you're going to be doing the Lord's work...

56
00:11:57,060 --> 00:11:59,581
You'll need the Lord's tools.

57
00:12:02,021 --> 00:12:04,981
Careful, hey, they are loaded.

58
00:12:09,782 --> 00:12:11,343
What else did God tell you?

59
00:12:13,103 --> 00:12:15,543
He told me He was going to test me.

60
00:12:17,344 --> 00:12:20,624
To see if I was worthy
to take on his mission.

61
00:12:22,105 --> 00:12:26,505
- What kind of test?
- I would have to show no mercy.

62
00:12:28,866 --> 00:12:32,706
Even to someone who
show mercy on me.

63
00:13:33,158 --> 00:13:35,238
Everyone?

64
00:13:47,001 --> 00:13:48,641
Bless you, Father.

65
00:13:49,321 --> 00:13:50,521
Goodbye, Carlitos.

66
00:13:50,721 --> 00:13:52,121
Bueno Suerte.

67
00:14:16,405 --> 00:14:17,726
Carlitos!

68
00:14:22,127 --> 00:14:24,247
You have a customer over here.

69
00:14:27,168 --> 00:14:28,528
Carlitos?

70
00:14:45,531 --> 00:14:48,292
Let God, who has enlightened every heart...

71
00:14:48,412 --> 00:14:51,932
...teach you to know your sins
and trust in his mercy.

72
00:14:52,252 --> 00:14:56,773
Bless me, Father, for I have sinned.
It's been one day since my last confession.

73
00:14:57,374 --> 00:15:02,094
What sins troubles you so much
that you must return after one day?

74
00:15:04,535 --> 00:15:07,335
Just business as usual at our monastery.

75
00:15:12,576 --> 00:15:17,097
The nuns carried the dope.
Bikers and priests were counting the money.

76
00:15:31,140 --> 00:15:35,781
No one was to know that death was about to
make an appeareance in this house of God.

77
00:16:16,068 --> 00:16:17,948
Stranger killed the priest?

78
00:16:19,149 --> 00:16:22,349
Patience, padre, I'm getting to the good part.

79
00:16:30,190 --> 00:16:34,351
The stranger leaned in and whispered into his ear.

80
00:16:36,392 --> 00:16:39,552
This must have put the fear of God into him,

81
00:16:39,832 --> 00:16:43,353
because all he could say was,
Carlitos.

82
00:16:45,033 --> 00:16:47,354
And that's all the stranger needed to hear.

83
00:16:48,114 --> 00:16:51,434
The stranger pushed the priest into his chair..

84
00:16:51,874 --> 00:16:53,315
..turned and walked away.

85
00:16:53,595 --> 00:16:57,355
But the priest had one more hail Hail Mary up his sleeve.

86
00:16:57,956 --> 00:17:00,127
He reached for a gun...

87
00:17:01,080 --> 00:17:05,259
Bang... Bang...
Bang-bang.

88
00:17:06,957 --> 00:17:10,077
- Priest killed the stranger?
- No.

89
00:17:11,198 --> 00:17:12,958
Stranger killed the priest?

90
00:17:14,239 --> 00:17:16,799
What happened to the priest?

91
00:17:18,000 --> 00:17:19,279
Four shots.

92
00:17:20,359 --> 00:17:21,720
Forehead...

93
00:17:22,681 --> 00:17:23,761
Sternum...

94
00:17:24,760 --> 00:17:25,761
Shoulder...

95
00:17:27,281 --> 00:17:28,641
Shoulder.

96
00:17:30,882 --> 00:17:33,643
Sign of the Cross.

97
00:17:33,882 --> 00:17:36,002
What happened to the stranger?

98
00:17:53,606 --> 00:17:58,407
Came in like a demon,
then vanished like an angel.

99
00:18:04,568 --> 00:18:08,169
Bullshit. I've heard nothing of this.
How do I know it's even true?

100
00:18:10,249 --> 00:18:14,170
Who the fuck are you?
Who sent you? Talk to me!

101
00:18:14,969 --> 00:18:16,192
Who the hell are...

102
00:18:21,771 --> 00:18:25,732
Some nutjob is running around
whacking priests left and right?

103
00:18:25,852 --> 00:18:30,453
Why the hell am I paying protection every month
for when you people can't protect a goddamn thing?

104
00:18:31,093 --> 00:18:34,333
Stop your worrying.
You are still alive, aren't you?

105
00:18:34,414 --> 00:18:36,974
Don't give me that. I fucking
used my men, for Christ's sake.

106
00:18:37,374 --> 00:18:41,775
- How do I know I'm not next on his list?
- I said we take care of it.

107
00:18:44,816 --> 00:18:46,576
Hello? Hello?

108
00:18:51,216 --> 00:18:55,457
Bullshit! Ther's a goddamned vigilante
running around whacking priests...

109
00:18:55,658 --> 00:18:58,698
...and all they can tell me
is they'll take care of it?

110
00:18:58,818 --> 00:19:02,178
Take care of it how?
Like they took care of Father Bernardo?

111
00:19:02,298 --> 00:19:07,099
Where is the world coming to when a man
of the cloth can't even do God's work?

112
00:19:07,220 --> 00:19:12,180
It's a goddamned shame.
Bernardo was a good earner.

113
00:19:12,301 --> 00:19:14,701
May God have mercy on his soul.

114
00:19:44,466 --> 00:19:48,747
- Hello. How are you?
- There's something wrong. You can look at it, please?

115
00:19:53,348 --> 00:19:56,549
- What a shit hole!
- Penny, watch your mouth!

116
00:19:56,669 --> 00:19:58,669
- But it is.
- Come here.

117
00:19:58,989 --> 00:19:59,949
What?

118
00:20:00,789 --> 00:20:03,790
- That's for the attitude, little missy.
- Dad!

119
00:20:04,110 --> 00:20:07,190
I can fit my finger in your radiator.
It's busted.

120
00:20:09,671 --> 00:20:12,792
Hey man, let's take a little bit of this.
They won't miss it.

121
00:20:13,392 --> 00:20:15,352
That's how people get killed.

122
00:20:18,393 --> 00:20:21,834
I was only kidding anyway.
I'm going to see what Rigo doin.

123
00:20:21,913 --> 00:20:23,354
You do that.

124
00:20:25,674 --> 00:20:29,434
Just miss my friends, you know.
I could be on the beach, at the ocean...

125
00:20:29,554 --> 00:20:31,315
- Look at all this sand you have out here.
- Hey, Kick-Stand?

126
00:20:31,355 --> 00:20:32,435
What?

127
00:20:32,796 --> 00:20:34,676
Come check this shit out.

128
00:20:41,437 --> 00:20:44,478
- Wonder who they are.
- Where's Dad?

129
00:20:51,759 --> 00:20:55,400
- Ladies!
- Get your hands off her!

130
00:20:55,600 --> 00:20:56,999
Stanley!

131
00:21:14,683 --> 00:21:17,804
- What happened?
- I was just fucking around with my buddy.

132
00:21:18,324 --> 00:21:19,724
Your buddy?

133
00:21:23,365 --> 00:21:27,325
Why are you crying?
It's okay. Come.

134
00:21:27,445 --> 00:21:30,045
Come, come.

135
00:21:48,329 --> 00:21:52,610
- We do not want any trouble
- Trouble? Do we look like trouble?

136
00:21:54,250 --> 00:21:57,811
- So you are a bunch of nice guys.
- Yes, yes. We are very very nice guys.

137
00:22:01,412 --> 00:22:04,292
But ... we do have a problem

138
00:22:04,532 --> 00:22:09,053
when people come to our establishment
and they dont want to pay respect.

139
00:22:11,734 --> 00:22:13,814
I do pay respect.

140
00:22:28,817 --> 00:22:30,617
Do I look a puta?

141
00:22:32,217 --> 00:22:33,907
- No!
- He called me a puta, no?

142
00:22:34,007 --> 00:22:35,544
- Yeah.
- Yeah, he called you a fucking whore.

143
00:22:39,419 --> 00:22:41,539
Why you trying to pay
me with puta money?

144
00:22:46,861 --> 00:22:51,221
Want to see the puta?
There she is.

145
00:22:51,741 --> 00:22:53,101
Show him a puta, guys.

146
00:23:19,430 --> 00:23:24,869
You know, you could...
You could grow some balls.

147
00:23:26,228 --> 00:23:29,428
Be a man and go over there and
save her from the bad guys.

148
00:23:31,228 --> 00:23:36,230
I won't stop you. I promise.
Come on, you can do it. Go!

149
00:23:37,470 --> 00:23:39,630
Be a man.
Go! Save her!

150
00:24:08,876 --> 00:24:09,836
Hello.

151
00:24:11,476 --> 00:24:16,237
I'm sorry about your daddy.
Guess I'll have to be your papa.

152
00:24:19,637 --> 00:24:20,958
You're cute.

153
00:24:22,357 --> 00:24:25,438
- Chavo, phone call.
- Who is it?

154
00:24:25,918 --> 00:24:29,480
- It's him.
- What does he want?

155
00:24:29,599 --> 00:24:31,319
He wants to talk to you.

156
00:24:32,919 --> 00:24:36,240
- How does he sound?
- He sounds serious.

157
00:24:38,801 --> 00:24:40,841
I come back for you, mami

158
00:24:48,702 --> 00:24:52,694
- Stop! Stop!
- Come here, bitch!

159
00:24:57,725 --> 00:25:01,444
Chavo's Repair. You wreck'em,
we don't give a shit.

160
00:25:03,086 --> 00:25:06,966
Father Bernardo is dead.
And so are Lobo and Chinaco.

161
00:25:09,686 --> 00:25:11,567
It was a goddamned massacre.

162
00:25:11,767 --> 00:25:15,528
They took nothing. No money, no drugs.
It was strictly personal.

163
00:25:16,967 --> 00:25:19,848
Round up your men and
get the word out.

164
00:25:25,529 --> 00:25:27,770
Orale jefe, we have a surprise for you.

165
00:25:28,290 --> 00:25:31,451
And she's sweet. We checked.
Ain't that right Kick Stand?

166
00:25:31,530 --> 00:25:32,651
Sho'nuff.

167
00:25:33,410 --> 00:25:36,731
- Would you like to pop her cherry?
- Yes.

168
00:25:37,916 --> 00:25:38,940
I do...

169
00:25:43,452 --> 00:25:46,933
But I can't.
We have work to do.

170
00:25:48,854 --> 00:25:53,454
Muchachos... We are in the
middle of a holy war.

171
00:25:55,535 --> 00:25:59,456
And besides, my mama always said to me.

172
00:26:01,856 --> 00:26:04,456
"If you want a sweet nice ripe peach...

173
00:26:04,537 --> 00:26:07,698
...you have to wait a while, falling under the tree.

174
00:26:08,537 --> 00:26:12,138
Until it ripens and it's
nice and good and sweet.

175
00:26:17,619 --> 00:26:22,259
My papa said: "Blacker the
berry, sweeter the juice."

176
00:26:25,941 --> 00:26:29,181
That's what he said-- Hey!
Chavo? What are you doing?

177
00:26:29,341 --> 00:26:33,462
You're talking about my mama at the same time
as your dirty stinking motherfucking father?

178
00:26:33,582 --> 00:26:34,983
I didn't say nothing.

179
00:26:35,058 --> 00:26:36,314
Hey! Chavo!

180
00:26:36,662 --> 00:26:40,503
You ever talk again about my mama, I'm gonna
blow your dick off and beat you to death with it!

181
00:26:40,624 --> 00:26:44,184
- Chavo, I didn't say nothing!
- Never talk about my mama!

182
00:26:44,304 --> 00:26:47,544
If anybody else wants
to talk about my mama...

183
00:26:47,664 --> 00:26:50,065
...be prepared to lose his dick!

184
00:26:57,946 --> 00:27:00,507
- Rigo!
- Yes.

185
00:27:00,907 --> 00:27:04,428
- Do you like this one?
- Yes.

186
00:27:05,708 --> 00:27:07,588
Do you want me to give her to you?

187
00:27:10,229 --> 00:27:11,829
- Yes.
- Okay.

188
00:27:13,189 --> 00:27:14,750
Give her to him.

189
00:28:17,361 --> 00:28:20,682
Not now. Go away. Leave me alone.

190
00:28:22,402 --> 00:28:24,082
Shit!

191
00:28:26,043 --> 00:28:28,444
Hello, Chavo.

192
00:28:28,923 --> 00:28:31,083
You done a great job
taking care of my place.

193
00:28:31,804 --> 00:28:33,884
I am. I made you a lot of money.

194
00:28:37,085 --> 00:28:39,286
Listen up.

195
00:28:40,886 --> 00:28:44,486
There's a vigilante on the streets.
going around and killing the bad guys.

196
00:28:45,806 --> 00:28:49,087
In case you guys forgot, we are the bad guys.

197
00:28:51,488 --> 00:28:56,088
Alright. Anybody comes through
that door, I want to know about it.

198
00:28:57,249 --> 00:28:58,289
Understand?

199
00:29:00,569 --> 00:29:02,489
Go! Go!

200
00:29:03,970 --> 00:29:07,011
Alright, alright, I'm on it !

201
00:29:19,493 --> 00:29:20,893
My hat.

202
00:29:34,615 --> 00:29:38,216
In witness pleasuring one another in God's house.

203
00:29:38,296 --> 00:29:42,657
That kind of blasphemy will not
be tolerated in my church.

204
00:30:24,024 --> 00:30:28,585
- Father Carlitos is there. And he is scared.
- Good.

205
00:30:39,428 --> 00:30:42,908
Why don't we just call the police
and let's them take care of it?

206
00:30:43,989 --> 00:30:46,709
Believers didn't ask for any help.

207
00:30:46,869 --> 00:30:49,909
When Moses came down from the mountain
and asked ask then who is on their side.

208
00:30:51,549 --> 00:30:54,310
They responded by taking arms
the name of the Lord.

209
00:30:54,470 --> 00:30:56,711
Slayed all who oppose the Lord.

210
00:30:57,190 --> 00:30:58,831
Spared no one.

211
00:30:59,191 --> 00:31:00,592
Are you ready for this?

212
00:31:00,712 --> 00:31:03,792
Are you ready going with guns blazing...

213
00:31:03,872 --> 00:31:06,216
...and shooting up a church
and killing more priests?

214
00:31:07,030 --> 00:31:08,850
I'm not ready for this.

215
00:31:09,513 --> 00:31:13,513
You know what they did to us.
You know what they would do to others like us.

216
00:31:14,594 --> 00:31:19,515
They must be punished, Angelina. They must
be punished by the hand of true believers.

217
00:31:22,875 --> 00:31:24,116
Look at me.

218
00:31:24,235 --> 00:31:28,997
How am I supposed to be the hand of God,
if I can't even hold my own hands still?

219
00:31:31,677 --> 00:31:35,678
All I can think is going back
at the monastery, in my filthy cell...

220
00:31:35,838 --> 00:31:38,918
...where they can stick a needle
in my arm and I can forget.

221
00:31:50,440 --> 00:31:52,401
Mother Magda told me you died.

222
00:31:55,521 --> 00:31:56,881
I did die.

223
00:32:14,525 --> 00:32:17,366
I was dead.

224
00:32:19,006 --> 00:32:22,727
When I was in death's arms,
the Lord spoke to me.

225
00:32:23,687 --> 00:32:27,408
Giving me his calling.

226
00:32:28,528 --> 00:32:32,688
One I wasn't ready for.

227
00:32:32,808 --> 00:32:35,329
One I'll have to prepare for.

228
00:32:36,770 --> 00:32:40,250
- What did the Lord say?
- It wasn't words.

229
00:32:41,890 --> 00:32:46,691
It was intense emotion of being alive
one that spoke to me.

230
00:32:47,451 --> 00:32:50,412
How did you escape death?

231
00:32:50,531 --> 00:32:55,253
The Lord sent an angel to guide me
back to this Earth to protect me.

232
00:32:57,813 --> 00:33:01,773
- Like the way you're protecting me now?
- Yes.

233
00:33:02,973 --> 00:33:06,854
Now that we found each other,
We will be whole again.

234
00:33:28,299 --> 00:33:31,900
There is no easy road to become a nun.

235
00:33:35,180 --> 00:33:40,341
It is not a life of refuge
from the outside world.

236
00:33:40,421 --> 00:33:44,021
It is a life of sacrifice.

237
00:33:44,861 --> 00:33:50,143
When times become unbearable,
we take comfort in knowing...

238
00:33:50,742 --> 00:33:55,543
...that the sacrifices that we make
are made in the name of the Lord.

239
00:33:55,943 --> 00:33:57,864
Jesus Christ.

240
00:34:00,665 --> 00:34:05,945
When you pass through those doors,
you will be born into a new life.

241
00:34:07,146 --> 00:34:11,746
You'll enter with nothing and you will be
 absolved of sins without questions.

242
00:34:12,146 --> 00:34:16,868
We do not ask nor do we speak
of our past lifes upon entry.

243
00:34:19,148 --> 00:34:23,629
To make a closer union
with the crucified Lord...

244
00:34:23,709 --> 00:34:30,790
You will be submitted to...
exercises and tasks in humility.

245
00:34:32,791 --> 00:34:37,831
If you question these trial,
you do not belong with us.

246
00:34:45,153 --> 00:34:50,034
To be obedient in all
things until death...

247
00:34:50,114 --> 00:34:52,554
...for the love of God.

248
00:34:53,074 --> 00:34:55,955
Is to be perfect nun.

249
00:35:46,765 --> 00:35:48,204
You want me to take care of it?

250
00:35:48,484 --> 00:35:51,965
No.
I am in the mood for a good jerk-off story.

251
00:35:53,005 --> 00:35:56,526
- I'll take it.
- It Could be the priests's killer.

252
00:35:58,126 --> 00:36:01,607
The Good Book is all the protection I need.

253
00:36:33,533 --> 00:36:35,893
May God who has enlightened every soul..

254
00:36:36,014 --> 00:36:39,894
..teach you to know your sins
and trust in his mercy.

255
00:36:40,014 --> 00:36:43,174
Forgive me, Lord,
for I'm about to sin.

256
00:36:43,295 --> 00:36:46,415
I'm not the Lord, he only works through me.

257
00:36:46,535 --> 00:36:48,336
Now tell me what is so heavy on your heart..

258
00:36:48,415 --> 00:36:51,336
..you must come to confession
at this unearthy hour?

259
00:36:53,137 --> 00:36:57,177
I'm not here to confess any sins.
I'm here to commit them.

260
00:36:58,098 --> 00:37:01,818
You must have balls in steel to come to my house.

261
00:37:03,778 --> 00:37:07,379
Do you know who your dealing with?
How much money comes through this church?

262
00:37:07,499 --> 00:37:10,779
Kiilling a couple of thugs and a priest
is not going to stop anything.

263
00:37:10,860 --> 00:37:14,780
- You have no idea what you're up against.
- Just tell me who you work for.

264
00:37:14,941 --> 00:37:17,381
and you won't have to suffer the
same fate as your brothers.

265
00:37:17,460 --> 00:37:21,461
I work for the Lord.
He is my sheperd!

266
00:38:05,430 --> 00:38:08,110
Make yourself comfortable.

267
00:38:49,318 --> 00:38:50,398
Fuck.

268
00:38:54,959 --> 00:38:56,439
Is always the same.

269
00:39:14,482 --> 00:39:18,003
Go around.

270
00:39:21,324 --> 00:39:24,725
Is nothing sacred any more?

271
00:39:28,085 --> 00:39:31,606
Don't you cover up.
It's alright. It'll take a minute

272
00:40:02,880 --> 00:40:03,855
Right back.

273
00:40:15,934 --> 00:40:19,615
- I need a room.
- By the hour or by the night?

274
00:40:23,295 --> 00:40:28,376
- By the night.
- That will be 30 dollars, cash.

275
00:40:35,193 --> 00:40:37,013
Alright, that's thirty.

276
00:40:41,299 --> 00:40:43,019
Lucky room number seven.

277
00:40:46,365 --> 00:40:47,485
Thanks...

278
00:40:48,560 --> 00:40:50,448
Jesus...

279
00:46:07,119 --> 00:46:08,057
Halfbreed.

280
00:46:12,140 --> 00:46:12,915
That's you.

281
00:46:16,480 --> 00:46:19,041
- You good?
- Yah.

282
00:46:21,762 --> 00:46:23,922
How can you get to count everybody's
money, including your own?

283
00:46:23,922 --> 00:46:26,042
What's your problem?
You want to count my money?

284
00:46:26,483 --> 00:46:27,843
- Nah, man. I'm just saying.
- Count it.

285
00:46:28,282 --> 00:46:31,363
- Count it.
- No, that's alright. It's good.

286
00:46:39,284 --> 00:46:40,484
Rigo.

287
00:46:50,327 --> 00:46:53,247
Go handle to your business, man.

288
00:47:00,889 --> 00:47:05,690
- Chavo's repair. This is Rigo.
- Get me Chavo on the phone. Now!

289
00:47:06,490 --> 00:47:08,970
Ok. Ok.

290
00:47:12,531 --> 00:47:16,451
- Carlitos too?
- Those bastards killed him in his own church.

291
00:47:18,211 --> 00:47:22,973
- He should have prayed harder.
- You need to put an end to this, right now.

292
00:47:25,413 --> 00:47:27,293
OK.

293
00:47:27,734 --> 00:47:32,094
If you want us to track down this hellraiser,
 it's going to be double the regular price.

294
00:47:32,175 --> 00:47:37,175
- I don't have that kind of money.
- Hey, Padre, don't fuck with me.

295
00:47:37,295 --> 00:47:39,696
I've already lost two men
in your holy war.

296
00:47:40,616 --> 00:47:44,097
Besides, I know Jesus has deep
pockets for this kind of shit.

297
00:47:44,576 --> 00:47:49,698
If you want double the pay, then
I want all this done in half the time.

298
00:47:49,897 --> 00:47:54,139
Meet Mother Magda at Carlitos his church.
She is expecting you.

299
00:48:03,660 --> 00:48:04,740
Carlitos...

300
00:48:07,021 --> 00:48:10,582
That didn't sound good.

301
00:48:25,504 --> 00:48:26,704
This way.

302
00:48:28,898 --> 00:48:30,018
Hey-hey-hey. No-no.

303
00:48:30,265 --> 00:48:34,106
You guys stay here and make sure
we don't have any unwanted visitors.

304
00:48:55,185 --> 00:48:59,547
- What do you think?
- They're dead alright.

305
00:49:00,631 --> 00:49:04,711
You can stop them. Make them pay.

306
00:49:06,152 --> 00:49:09,033
You know, you're not exactly the forgiving type, Magda.

307
00:49:09,153 --> 00:49:11,308
You know, turn the other
cheek, love thy enemy...

308
00:49:11,805 --> 00:49:13,830
And all that other
bullshit you preach.

309
00:49:14,153 --> 00:49:19,315
Listen, you brute. They called me
up from my monastery to fix this.

310
00:49:20,115 --> 00:49:23,715
And I expect it to be resolved
as soon as possible.

311
00:49:23,795 --> 00:49:27,276
I heard you the first time, OK ?
I know, I know you want it fast.

312
00:49:27,715 --> 00:49:29,316
You want a miracle.

313
00:49:29,716 --> 00:49:31,076
That's your business.

314
00:49:31,556 --> 00:49:36,517
But by the looks of things,
your business is not doing very well.

315
00:49:40,518 --> 00:49:43,118
Now. Are here any witnesses?

316
00:49:43,719 --> 00:49:48,360
Sister Mary was the first one
to alert us of the situation.

317
00:49:49,399 --> 00:49:52,401
- Then get her ass in here.
- This won't do any good.

318
00:49:52,440 --> 00:49:55,721
Sister Mary took a vow of silence 20 years ago.

319
00:49:56,922 --> 00:50:00,322
I keep her around just because she knows
how to keep a big mouth shut.

320
00:50:02,482 --> 00:50:05,162
We'll see about that. Bring her.

321
00:50:58,053 --> 00:50:59,413
Who did this?

322
00:51:03,734 --> 00:51:05,734
Who killed these men?

323
00:51:10,095 --> 00:51:14,136
OK.
I am going to ask you one more time nicely.

324
00:51:15,056 --> 00:51:19,737
If you do not want to talk, I'm going to let
Kick Stand take over the questioning.

325
00:51:23,218 --> 00:51:25,337
Who killed these men?

326
00:51:29,578 --> 00:51:31,738
OK. It's your choice.

327
00:51:35,899 --> 00:51:37,340
I'll be right back.

328
00:51:40,820 --> 00:51:46,222
Mary, tell me what you saw now.

329
00:51:46,782 --> 00:51:50,422
Mary what did you see?

330
00:51:51,502 --> 00:51:55,663
It's alright. You can talk.
I won't tell anyone.

331
00:51:57,383 --> 00:51:58,824
Just between us.

332
00:51:59,464 --> 00:52:03,225
Mary, they are going to come back here..

333
00:52:03,304 --> 00:52:05,545
.. then I can not stop them.

334
00:52:06,225 --> 00:52:08,385
What did you see?

335
00:52:11,586 --> 00:52:15,947
Kick Stand, get your dick over here.
Now!

336
00:52:28,389 --> 00:52:29,949
Who killed them, Mary?

337
00:52:30,950 --> 00:52:33,270
You know who it was, don't you?

338
00:52:35,071 --> 00:52:39,471
You've seen them before, haven't you?
What did they look like?

339
00:52:45,432 --> 00:52:48,832
Kick Stand, go to work.

340
00:52:50,300 --> 00:52:53,661
I'm gonna nail you harder than
they nailed Jesus to the cross.

341
00:53:40,363 --> 00:53:42,163
Kick Stand, enough.

342
00:54:04,727 --> 00:54:06,567
It was a nun.

343
00:54:09,248 --> 00:54:12,369
- With guns.
- A nun?

344
00:54:15,449 --> 00:54:18,810
Looks like one of your
penguins flew the coop, Magda.

345
00:54:19,730 --> 00:54:23,571
- What kind of guns did she have?
- Big guns.

346
00:54:25,571 --> 00:54:30,052
I looked into her eyes
and saw the devil in them.

347
00:54:36,926 --> 00:54:38,396
So what are we going
to do now, boss?

348
00:54:39,013 --> 00:54:43,002
Well, first we're going to round
up the troops. And then...

349
00:54:44,134 --> 00:54:47,774
...we are going nun-hunting.

350
00:54:51,775 --> 00:54:54,056
Halfbreed! Quit fucking around!
Get up here!

351
00:55:06,858 --> 00:55:08,979
We're looking for a nun.

352
00:55:09,824 --> 00:55:11,321
A nun with big guns.

353
00:55:12,112 --> 00:55:15,633
You mean, "big guns" like in big tits?

354
00:55:16,500 --> 00:55:17,160
I said guns.

355
00:55:20,393 --> 00:55:23,793
You better wisen up or you're
gonna end up here in a pine box.

356
00:55:29,230 --> 00:55:31,951
Get the word out on the streets.

357
00:55:49,222 --> 00:55:51,583
Hey, Kick Stand, go check that out.

358
00:56:07,749 --> 00:56:09,990
What about her?
She is a nun.

359
00:56:12,718 --> 00:56:14,279
Yes she is.

360
00:56:24,815 --> 00:56:27,456
Are you a killer?

361
00:56:29,936 --> 00:56:32,937
No, no, no. Not this one.

362
00:56:34,177 --> 00:56:36,577
I don't think she can even hold a gun
much less shoot one.

363
00:56:37,424 --> 00:56:38,784
C'mon. Let's go.

364
00:56:41,928 --> 00:56:43,131
Be back for you later.

365
00:57:40,509 --> 00:57:43,549
Oh, my prodigal daughter returns.

366
00:57:43,669 --> 00:57:48,390
When they didn't find your body in Valambrosa
I thought perhaps it was you causing all this trouble.

367
00:57:48,470 --> 00:57:53,512
And now that I see you here before me,
I know, I know my words are true.

368
00:57:53,871 --> 00:57:56,392
The only thing you're shooting 
up is in your arms.

369
00:57:56,392 --> 00:57:59,273
I knew you wouldn't last
a week outside of here.

370
00:58:00,993 --> 00:58:05,554
Are you looking for someone?
Well, take a good look at him.

371
00:58:06,833 --> 00:58:10,114
God punishes those who
fail to serve his will!

372
00:58:33,919 --> 00:58:36,840
- A delivery from Magda.
- What the hell am I suppose to do with her?

373
00:58:37,000 --> 00:58:39,079
I thought we were done
with this nun bullshit.

374
00:58:39,759 --> 00:58:42,080
Magda said to give her to Chavo
and when he is finished with her

375
00:58:42,180 --> 00:58:43,743
 just throw her out with
the rest of the trash.

376
00:58:44,081 --> 00:58:46,881
- Put it in that booth.
- Alright.

377
00:59:08,245 --> 00:59:09,405
Nice rack.

378
00:59:11,646 --> 00:59:13,741
- Hi, Chavo.
- Hello, love.

379
00:59:14,532 --> 00:59:18,532
Hey, Kick Stand. Go tell Beverly
to bring me some beers, please.

380
00:59:27,009 --> 00:59:28,609
Come with me.

381
00:59:36,210 --> 00:59:37,410
Just like I remember.

382
00:59:40,531 --> 00:59:43,011
- Will you dance for me, Mamacita?
- Of course.

383
00:59:45,332 --> 00:59:47,812
Come on, come here.

384
01:00:00,895 --> 01:00:02,615
Thanks, Beverly.

385
01:00:08,016 --> 01:00:11,617
Listen, if you see a crazy chick like that,
you shoot first and ask questions later.

386
01:00:11,697 --> 01:00:13,017
That's Chavo's orders.

387
01:00:15,178 --> 01:00:19,418
- Halfbreed, would you like a beer?
- What the fuck you think, bitch? Yeah.

388
01:00:22,339 --> 01:00:26,619
I think God is trying to tell me something.
Everywhere I go, I see nuns.

389
01:00:27,420 --> 01:00:29,221
What the fuck you
yapping about, Beverly?

390
01:00:29,300 --> 01:00:33,220
The girl in the picture. I saw one checking in
at the Palms Hotel, last night.

391
01:00:33,781 --> 01:00:37,221
Really dirty looking one too. Might've
had, I dunno, a drinking problem.

392
01:00:37,341 --> 01:00:41,622
You saw this chick, a nun, checking
in that piece-of-shit motel last night?

393
01:00:41,742 --> 01:00:43,943
I was with Butch. Ask her,
she'll tell you the same thing.

394
01:00:45,703 --> 01:00:48,664
God help you if you're wrong about
this, Beverly, God fucking help you!

395
01:01:19,335 --> 01:01:20,455
God...

396
01:01:23,550 --> 01:01:28,191
- What do you want? I'm in the middle of something.
- <i>I need you to wake the fuck up and pay attention.</i>

397
01:01:29,471 --> 01:01:32,231
Yeah, well now that you got my undivided attention,

398
01:01:32,631 --> 01:01:35,632
What the fuck is so important you need
to wake me up in the middle of the morning?

399
01:01:35,792 --> 01:01:38,232
It's four o'clock in the afternoon, you stupid bitch.

400
01:01:39,874 --> 01:01:42,434
- Did you checked in a nun, last night?
- <i>No</i>.

401
01:01:42,838 --> 01:01:44,096
Beverly, you fucking cunt!

402
01:01:44,196 --> 01:01:46,174
<i>I'm gonna kick your fucking
ass when I get off the phone!</i>

403
01:01:46,835 --> 01:01:49,995
Leave her the fuck alone,
you red nigger. Jesus.

404
01:01:50,115 --> 01:01:53,836
I didn't checked her in last night.
I checked her in this morning at 3AM.

405
01:01:55,156 --> 01:01:57,036
<i> Butch, now is not the
time to fuck with me.</i>

406
01:01:59,636 --> 01:02:02,237
Room seven.
I checked her in myself.

407
01:02:02,957 --> 01:02:06,038
Don't let that fuckin' bitch leave.
We're on our way.

408
01:03:27,893 --> 01:03:28,934
Is she dead?

409
01:03:28,934 --> 01:03:32,854
No, but Beverly saw a nun checking
in to that Palm Motel last night.

410
01:03:33,854 --> 01:03:38,455
I want to see this little hellraiser myself.
Kick Stand, you get over there.

411
01:03:38,535 --> 01:03:41,975
You bring her back to me, alive.
Go.

412
01:03:53,138 --> 01:03:56,978
You let me leave now.
I won't have to kill you.

413
01:03:57,098 --> 01:04:00,099
In case you haven't noticed,
I'm the one with the gun.

414
01:04:00,219 --> 01:04:01,659
You do not have to go through this.

415
01:04:03,180 --> 01:04:06,340
There are no sins unforgivable by God.

416
01:04:07,220 --> 01:04:11,821
- If you just ask for his forgiveness.
- What do you know about sins?

417
01:04:11,941 --> 01:04:15,061
Are you even a real nun
or just some whore playing dress-up?

418
01:04:16,862 --> 01:04:21,743
I've taken my vows and was ordained
at my church in the name of the Lord.

419
01:04:22,783 --> 01:04:27,064
You worship whores are so high and mighty.
Who are you to preach?

420
01:04:27,904 --> 01:04:30,465
What do you know about
half the sins you condemn?

421
01:04:31,425 --> 01:04:36,666
- I too am a sinner.
- What are your sins, my precious sister?

422
01:04:40,387 --> 01:04:44,827
- I am a fornicator
- Big fucking deal.

423
01:04:44,947 --> 01:04:48,428
Half of you penguins are
getting poked by the preacher.

424
01:04:48,548 --> 01:04:50,520
That may be true.

425
01:04:52,226 --> 01:04:55,075
But, my sins of the flesh...

426
01:04:55,869 --> 01:04:59,670
are that of the fairer sex.

427
01:05:14,913 --> 01:05:18,073
Get back on the bed, bitch.

428
01:05:22,828 --> 01:05:25,426
Shh....

429
01:05:31,595 --> 01:05:33,036
We will sin today.

430
01:05:36,957 --> 01:05:38,877
And ask for forgiveness tomorrow.

431
01:05:47,439 --> 01:05:50,119
Such is the beauty of the faith.

432
01:06:22,357 --> 01:06:26,918
- I thought that you...
- You thought, you thought? You thought wrong.

433
01:06:27,526 --> 01:06:28,928
You said I can be forgiven.

434
01:06:29,408 --> 01:06:32,644
You want to be forgiven?
Ask for it. Come on, come on!

435
01:06:32,744 --> 01:06:36,968
- Ask for it! Ask for it!
- Please... please...

436
01:06:41,889 --> 01:06:44,768
- Please...
- Shh....

437
01:06:50,051 --> 01:06:52,011
I absolve you...

438
01:06:59,634 --> 01:07:01,034
It's ok...

439
01:07:06,214 --> 01:07:09,974
I absolve you...

440
01:07:11,774 --> 01:07:14,095
of all your sins.

441
01:07:20,056 --> 01:07:23,536
But there's no penance
for the hellbound!

442
01:07:49,982 --> 01:07:53,902
- Where the fuck have you been all day?
- Over at Titty Flicker's.

443
01:07:54,342 --> 01:07:58,784
They got this new girl over there.
Man, I'm telling you: She is it!

444
01:07:58,903 --> 01:08:03,104
She wears this nun outfit. Makes your
dick just pop out of your pants.

445
01:08:03,224 --> 01:08:05,052
You've seen a nun
at the Titty Flicker club?

446
01:08:06,092 --> 01:08:07,212
Yeah...

447
01:08:07,345 --> 01:08:12,186
- How long has she been there, asshole?
- Papa said she just stopped there.

448
01:08:13,065 --> 01:08:16,307
Get the fuck out of my face!

449
01:08:22,707 --> 01:08:25,868
- What now?
- Butch is dead.

450
01:08:25,988 --> 01:08:29,349
That nun worked over real good.

451
01:08:29,578 --> 01:08:31,352
<i> Chavo's going to be pissed.</i>

452
01:08:34,749 --> 01:08:39,150
Sorry, boss. But I got bad news.
Butch is dead.

453
01:08:39,270 --> 01:08:44,391
- Bloody mess over there, man.
- Goddamn, that bitch is crazy!

454
01:08:44,511 --> 01:08:48,033
Prospect said there's a nun
over the Titty Flicker right now.

455
01:08:48,833 --> 01:08:50,593
Come on.

456
01:09:01,955 --> 01:09:04,715
- Tity Flicker's.
- Hey, man, it's Halfbreed.

457
01:09:04,835 --> 01:09:09,916
- I bet that you're looking for that nun.
- Tell Papa Chavo's on his way.

458
01:09:10,036 --> 01:09:11,593
<i>Yeah, I'll get that bitch cleaned up.</i>

459
01:09:12,563 --> 01:09:14,903
- Now what?
- Chavo's on his way.

460
01:09:14,927 --> 01:09:17,095
- And...?
- And, get that bitch out of the booth...

461
01:09:17,195 --> 01:09:18,809
and clean her up before he gets here.

462
01:09:25,119 --> 01:09:26,720
What's the matter, Halfbreed?

463
01:09:30,200 --> 01:09:32,920
Not now, bitch.

464
01:09:38,882 --> 01:09:42,442
Wake up!
And put your clothes back on.

465
01:10:13,888 --> 01:10:16,427
<i>My heart will not allow
me to wait any longer.</i>

466
01:10:17,646 --> 01:10:20,513
<i>I pray to the Lord, He
will safely reunite us.</i>

467
01:10:21,089 --> 01:10:23,387
<i>If not in this world,
then in heaven above.</i>

468
01:13:08,365 --> 01:13:13,105
♪I've got the joy, joy, joy,♪
♪joy down in my heart...♪

469
01:13:13,472 --> 01:13:17,391
♪Down in my heart.♪
♪Down in my heart.♪

470
01:13:17,583 --> 01:13:22,230
♪I've got the joy, joy, joy,♪
♪joy down in my heart...♪

471
01:13:23,232 --> 01:13:26,463
♪Down in my heart to stay.♪

472
01:13:26,845 --> 01:13:31,316
♪And if the devil doesn't like♪
♪it, he can go fuck himself...♪

473
01:13:31,416 --> 01:13:35,245
♪Go fuck himself.♪
♪Go fuck himself.♪

474
01:13:35,345 --> 01:13:39,591
♪And if the devil doesn't like♪
♪it, he can go fuck himself...♪

475
01:13:39,691 --> 01:13:43,211
♪Because Magda is here to stay.♪

476
01:13:47,247 --> 01:13:50,848
Get your filthy hands off me.

477
01:13:51,489 --> 01:13:52,604
Where's Angelina?

478
01:13:54,248 --> 01:13:58,570
She is just playing at the Titty Flicker
theater with some old friends of yours.

479
01:14:00,650 --> 01:14:04,371
Killing me is not going
to make things right.

480
01:15:15,024 --> 01:15:19,184
Kick Stand, go tell Papa
to bring in a nun.

481
01:15:39,180 --> 01:15:41,957
Hello again, killer.

482
01:15:42,508 --> 01:15:44,446
I know you're not the
nun I'm looking for.

483
01:15:45,989 --> 01:15:49,510
But, I do believe you have
something to do with this.

484
01:15:49,630 --> 01:15:54,871
- Papa, leave us alone for a while.
- Gotta take a dump anyway.

485
01:17:44,113 --> 01:17:45,529
Look! The nun!

486
01:18:03,685 --> 01:18:07,835
Put the gun down, nice and slow.

487
01:18:15,356 --> 01:18:16,756
Now walk!

488
01:19:30,711 --> 01:19:33,431
Who the hell are you?

489
01:19:41,541 --> 01:19:42,683
Fucking bitch!

490
01:20:31,141 --> 01:20:33,316
No, no, no, calm down.

491
01:20:33,416 --> 01:20:34,675
Get off her.

492
01:20:40,444 --> 01:20:43,484
I'm not touching her. It's okay.

493
01:20:44,685 --> 01:20:47,685
Come on.

494
01:20:55,046 --> 01:20:57,007
Are you okay?

495
01:20:57,606 --> 01:21:01,368
- I will be.
- No, no, no ...

496
01:21:03,328 --> 01:21:05,088
No, no.

497
01:21:08,649 --> 01:21:10,249
No! No, no, no.

498
01:22:05,886 --> 01:22:06,750
Everyone?

499
01:22:10,660 --> 01:22:14,381
No. I'll take care of it myself.

500
01:22:19,662 --> 01:22:21,062
Father...

501
01:22:22,463 --> 01:22:26,583
Are you ready to take on this new
mission in the name of our Lord?