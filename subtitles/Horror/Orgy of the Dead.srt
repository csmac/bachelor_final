﻿1
00:00:11,578 --> 00:00:14,172
(dramatic music)

2
00:00:15,682 --> 00:00:18,879
(doors creaking)

3
00:00:19,186 --> 00:00:21,655
(dramatic music)

4
00:00:42,109 --> 00:00:43,599
- I am Criswell!

5
00:00:43,877 --> 00:00:47,040
For years I have told
the almost unbelievable,

6
00:00:47,347 --> 00:00:52,012
related the unreal, and shown
it to be more than a fact.

7
00:00:52,319 --> 00:00:54,947
Now I tell a tale of
the threshold, people,

8
00:00:55,222 --> 00:00:57,589
so astounding that
some of you may faint.

9
00:00:57,891 --> 00:01:01,088
This is a story of those
in the twilight time,

10
00:01:01,395 --> 00:01:03,056
once human, now monsters,

11
00:01:04,364 --> 00:01:07,163
in a void between the
living and the dead.

12
00:01:07,467 --> 00:01:11,131
Monsters to be pitied,
monsters to be despised.

13
00:01:13,240 --> 00:01:15,038
A night with the ghouls,

14
00:01:15,342 --> 00:01:17,106
the ghouls reborn,

15
00:01:17,377 --> 00:01:20,745
from the innermost
depths of the world.

16
00:01:21,048 --> 00:01:23,312
(eerie music)

17
00:03:01,314 --> 00:03:03,646
- We sure picked the wrong
night to find a cemetery.

18
00:03:03,950 --> 00:03:05,281
Let's turn back.

19
00:03:05,585 --> 00:03:07,178
- No, it's on a night like this

20
00:03:07,454 --> 00:03:09,650
when the best
ideas come to mind.

21
00:03:09,956 --> 00:03:12,220
- But does it have
to be in a cemetery?

22
00:03:12,492 --> 00:03:14,256
- You wouldn't understand.

23
00:03:14,528 --> 00:03:16,223
Seeing a cemetery
on a night like this

24
00:03:16,530 --> 00:03:20,865
can stir in the mind the best
ideas for a good horror story.

25
00:03:21,168 --> 00:03:24,103
- But there's so many wonderful
things to write about, Bob.

26
00:03:24,371 --> 00:03:26,703
- Sure there are, and
I've tried them all;

27
00:03:27,007 --> 00:03:30,272
plays, love stories,
westerns, dog stories.

28
00:03:31,845 --> 00:03:34,940
Huh, now there was a good one,
that dog story all about--

29
00:03:35,215 --> 00:03:38,150
- But horror stories, why
all the time horror stories?

30
00:03:38,452 --> 00:03:42,548
- Shirley, I wrote for years
without selling a single word.

31
00:03:42,823 --> 00:03:45,121
My monsters have
done well for me.

32
00:03:45,392 --> 00:03:46,757
You think I'd give that up

33
00:03:47,060 --> 00:03:50,894
just so I could write about
trees, or dogs, or daisies?

34
00:03:52,032 --> 00:03:53,295
(chuckles) Daisies.

35
00:03:53,567 --> 00:03:55,160
That's it, I'll write
about my creatures

36
00:03:55,469 --> 00:03:57,733
who are pushing up the daisies.

37
00:03:59,406 --> 00:04:01,101
Your puritan upbringing
holds you back

38
00:04:01,408 --> 00:04:02,739
from my monsters,

39
00:04:03,043 --> 00:04:05,478
but it certainly doesn't
hurt your art of kissing.

40
00:04:05,746 --> 00:04:09,080
- That's life, my
kisses are alive.

41
00:04:09,382 --> 00:04:12,181
- Who's to say my
monsters aren't alive?

42
00:04:12,486 --> 00:04:14,978
(upbeat music)

43
00:04:18,391 --> 00:04:22,259
All of my books and stories
are based on fact, or legend.

44
00:04:22,562 --> 00:04:24,496
That's perhaps why
they're more interesting

45
00:04:24,765 --> 00:04:26,893
and sell in the top spots.

46
00:04:27,200 --> 00:04:29,280
- Well, fact or not, I don't
see how we're gonna find

47
00:04:29,536 --> 00:04:32,233
an ancient cemetery in
these mountains tonight.

48
00:04:32,539 --> 00:04:34,166
I can't see a thing.

49
00:04:34,441 --> 00:04:35,408
Let's turn back.

50
00:04:36,610 --> 00:04:38,704
- There's an old
cemetery on this road.

51
00:04:39,012 --> 00:04:40,673
I've been there before.

52
00:04:48,421 --> 00:04:49,461
- I'm getting the jitters.

53
00:04:49,589 --> 00:04:51,250
Let's turn back.

54
00:04:51,558 --> 00:04:54,027
- Okay, just as soon
as I find a place

55
00:04:54,294 --> 00:04:56,388
wide enough to turn around.

56
00:05:00,734 --> 00:05:01,826
- Not so fast.

57
00:05:06,873 --> 00:05:07,806
Not so fast!

58
00:05:10,410 --> 00:05:13,277
(brakes screeching)

59
00:05:13,580 --> 00:05:16,675
(Shirley screaming)

60
00:05:16,950 --> 00:05:19,078
(crashing)

61
00:05:21,555 --> 00:05:24,490
(crickets chirping)

62
00:05:31,131 --> 00:05:33,498
(somber music)

63
00:06:03,430 --> 00:06:05,762
- It is said on clear nights,

64
00:06:06,032 --> 00:06:08,899
beneath the cold
light of the moon,

65
00:06:09,202 --> 00:06:11,136
howls a dog and a wolf,

66
00:06:11,438 --> 00:06:15,204
and creeping things
crawl out of the slime.

67
00:06:16,476 --> 00:06:20,344
It is then the ghouls feast
in all their radiance.

68
00:06:21,948 --> 00:06:24,747
It is on nights like
this most people

69
00:06:25,018 --> 00:06:28,511
prefer to steer clear
of, uh, burial grounds.

70
00:06:32,359 --> 00:06:34,726
It is on nights like this,

71
00:06:35,028 --> 00:06:37,395
that the creatures
are said to appear,

72
00:06:37,697 --> 00:06:38,493
and to walk!

73
00:06:47,807 --> 00:06:51,607
The day is gone, the night
is upon us, and the moon,

74
00:06:51,878 --> 00:06:54,313
which controls all
of the underworld,

75
00:06:54,581 --> 00:06:57,676
once again shines in
radiant contentment.

76
00:06:59,586 --> 00:07:03,420
Come forth, come forth,
oh Princess of Darkness.

77
00:07:07,160 --> 00:07:09,857
(dramatic music)

78
00:07:57,944 --> 00:08:00,106
Time seems to stand still.

79
00:08:00,413 --> 00:08:04,350
Not so the ghouls, when a
night of pleasure is at hand!

80
00:08:06,386 --> 00:08:09,356
If I am not pleased by
tonight's entertainment,

81
00:08:09,622 --> 00:08:13,616
I shall banish their souls
to everlasting damnation!

82
00:08:23,103 --> 00:08:26,368
(gong tolling)

83
00:08:26,639 --> 00:08:28,300
And who is to be first?

84
00:08:31,578 --> 00:08:35,071
(chanting and drum beating)

85
00:08:48,661 --> 00:08:50,993
- One who loved flames.

86
00:08:51,297 --> 00:08:54,232
Her lovers were
killed by flames.

87
00:08:54,501 --> 00:08:55,798
She died in flames.

88
00:10:45,912 --> 00:10:49,280
(drum banging and
crickets chirping)

89
00:10:49,582 --> 00:10:52,950
(chanting and drum beating)

90
00:12:08,962 --> 00:12:11,727
(somber music)

91
00:12:28,047 --> 00:12:31,642
One who prowls the lonely
streets at night in life

92
00:12:31,918 --> 00:12:35,013
is bound to prowl
them in eternity.

93
00:12:35,321 --> 00:12:37,688
(Sultry music)

94
00:14:11,351 --> 00:14:13,683
- [Criswell] Ahh, the
curiosity of youth,

95
00:14:13,986 --> 00:14:15,852
on the road to ruin.

96
00:14:16,155 --> 00:14:18,317
May it ever be so adventurous.

97
00:14:19,359 --> 00:14:20,849
- I'm so frightened.

98
00:14:21,160 --> 00:14:24,323
- Well we certainly
can't stay here, come on.

99
00:14:24,630 --> 00:14:25,256
- Where?

100
00:14:25,531 --> 00:14:26,862
- In there.

101
00:14:27,166 --> 00:14:28,429
- It frightens me.

102
00:14:28,701 --> 00:14:31,727
- Silly, there's nothing
in there to be afraid of.

103
00:14:32,038 --> 00:14:34,234
- Then, then what's that music?

104
00:14:34,540 --> 00:14:36,736
- That's what I wanna find out.

105
00:14:37,043 --> 00:14:38,408
- We have to get help.

106
00:14:38,711 --> 00:14:41,043
- What help can we possibly
find in the cemetery

107
00:14:41,347 --> 00:14:43,042
at this time of night?

108
00:14:43,349 --> 00:14:45,113
- Well, something's
making that music.

109
00:14:45,385 --> 00:14:47,911
- I'm not sure I care
to find out what it is.

110
00:14:48,221 --> 00:14:50,986
- All right, don't worry,
I'll be right beside you.

111
00:14:51,290 --> 00:14:55,921
- What help will that do if
something in there isn't dead?

112
00:14:56,195 --> 00:14:58,527
- Not dead, in a cemetery?

113
00:14:58,831 --> 00:15:01,926
- I can't imagine anything
dead playing that music.

114
00:15:02,201 --> 00:15:03,965
- Well, it's probably
just the caretaker,

115
00:15:04,237 --> 00:15:06,831
and that's exactly
who we want to find.

116
00:15:07,140 --> 00:15:08,050
They'll have a telephone.

117
00:15:08,074 --> 00:15:08,905
Come on.

118
00:15:09,208 --> 00:15:11,734
(Sultry music)

119
00:18:07,987 --> 00:18:11,582
- Could it be some kind
of college initiation?

120
00:18:11,857 --> 00:18:13,621
- It's an initiation all right,

121
00:18:13,893 --> 00:18:16,692
but not of a college
as you and I know them.

122
00:18:16,996 --> 00:18:18,964
Nothing alive looks like that.

123
00:18:19,231 --> 00:18:21,165
- Can't we get out of here?

124
00:18:21,434 --> 00:18:23,232
- I'm not sure.

125
00:18:23,536 --> 00:18:25,026
- What do you mean?

126
00:18:26,038 --> 00:18:28,006
- I'm not sure of myself.

127
00:18:28,274 --> 00:18:32,802
It's just a feeling I've
had since the crash, like,

128
00:18:33,112 --> 00:18:35,945
like I feel a cold
chill all over.

129
00:18:36,248 --> 00:18:36,840
Now this.

130
00:18:41,120 --> 00:18:43,487
(Sultry music)

131
00:19:36,575 --> 00:19:38,839
- I would see for approval,

132
00:19:39,111 --> 00:19:43,105
the one who in life worshiped
gold above all else.

133
00:19:46,819 --> 00:19:50,278
(chiming dramatic music)

134
00:20:10,176 --> 00:20:13,202
- Look!
- Be careful, they'll see you.

135
00:20:15,448 --> 00:20:18,145
(seductful music)

136
00:25:56,121 --> 00:25:57,611
- Throw gold at her.

137
00:26:28,654 --> 00:26:29,348
More gold.

138
00:26:50,442 --> 00:26:51,136
More gold!

139
00:27:03,755 --> 00:27:04,449
More gold!

140
00:27:14,266 --> 00:27:16,598
(laughing)

141
00:27:19,972 --> 00:27:22,669
(seductful music)

142
00:27:37,956 --> 00:27:40,152
(laughing)

143
00:27:41,393 --> 00:27:44,090
(seductful music)

144
00:27:52,471 --> 00:27:55,600
For all eternity,
she shall have gold.

145
00:27:58,744 --> 00:28:01,543
(dramatic music)

146
00:28:24,303 --> 00:28:26,567
(laughing)

147
00:28:30,943 --> 00:28:33,139
(bubbling)

148
00:29:21,226 --> 00:29:24,161
(crickets chirping)

149
00:29:31,570 --> 00:29:34,267
(dramatic music)

150
00:31:17,275 --> 00:31:19,437
And both couldn't help
but remember a line

151
00:31:19,711 --> 00:31:23,409
from one of Bob's stories,
a sudden wind howls.

152
00:31:24,883 --> 00:31:27,477
The night things
are all about me,

153
00:31:27,786 --> 00:31:31,654
every shadow, a beckoning
invitation to disaster.

154
00:31:33,024 --> 00:31:36,654
I know I should think of other
things, of pleasant things,

155
00:31:36,962 --> 00:31:38,123
but I can't.

156
00:31:38,397 --> 00:31:41,731
How can I think of other
things, of pleasant things,

157
00:31:42,033 --> 00:31:45,833
when I am in a place surrounded
by shadows and objects,

158
00:31:46,138 --> 00:31:49,836
which can take any shape,
here in the darkness,

159
00:31:51,376 --> 00:31:54,038
any shape my mind can conceive?

160
00:32:29,414 --> 00:32:30,404
Bring 'em in!

161
00:32:57,742 --> 00:32:59,039
They are live ones?

162
00:32:59,311 --> 00:33:00,801
- [Mummy] Yes, Master.

163
00:33:01,112 --> 00:33:03,103
- Live ones where only
the dead should be?

164
00:33:03,415 --> 00:33:07,875
- [Mummy] Yes, Master, and
we caught them, him and me.

165
00:33:08,153 --> 00:33:10,986
(Wolfman howling)

166
00:33:12,891 --> 00:33:15,053
- You shall both be rewarded.

167
00:33:17,662 --> 00:33:20,154
My dear empress of the night,

168
00:33:20,465 --> 00:33:23,093
put these intruders to the test.

169
00:33:26,071 --> 00:33:29,063
- But they are
not yet one of us.

170
00:33:29,341 --> 00:33:32,208
- A situation easily remedied.

171
00:33:32,511 --> 00:33:35,037
- 'Fe them that they may watch.

172
00:33:36,181 --> 00:33:39,116
(crickets chirping)

173
00:33:41,520 --> 00:33:44,455
(suspenseful music)

174
00:34:28,033 --> 00:34:29,023
- Fiend, fiend!

175
00:34:32,304 --> 00:34:35,239
- To love the cat,
is to be the cat.

176
00:34:54,693 --> 00:34:56,161
(crickets chirping)

177
00:34:56,428 --> 00:34:58,795
(whip snapping)

178
00:35:04,102 --> 00:35:08,232
(Playful music
and whip snapping)

179
00:35:52,717 --> 00:35:55,550
- A pussycat is
born to be whipped.

180
00:38:30,909 --> 00:38:32,877
It will please me very much

181
00:38:33,144 --> 00:38:36,842
to see the slave girl
with her tortures!

182
00:38:37,148 --> 00:38:41,142
(whip snapping and
woman groaning)

183
00:38:52,564 --> 00:38:55,309
Torture, torture,
it pleasures me!

184
00:38:55,333 --> 00:38:55,810
Torture, torture,
it pleasures me!

185
00:38:55,834 --> 00:39:03,834
Torture, torture,
it pleasures me!

186
00:45:08,773 --> 00:45:11,208
(gong ringing)

187
00:45:13,211 --> 00:45:14,679
- I'm so frightened.

188
00:45:14,979 --> 00:45:16,777
- You've got a right to be.

189
00:45:17,048 --> 00:45:19,540
We're trapped by
a bunch of fiends.

190
00:45:19,850 --> 00:45:21,443
- Those creatures!

191
00:45:21,719 --> 00:45:23,710
- Don't let them hear you.

192
00:45:24,822 --> 00:45:26,517
- What can we do?

193
00:45:26,824 --> 00:45:28,019
- I don't know.

194
00:45:28,326 --> 00:45:30,658
I just don't know,
but don't give up.

195
00:45:30,962 --> 00:45:32,896
We're not finished yet.

196
00:45:33,731 --> 00:45:35,859
Easy Shirley, easy!

197
00:45:36,167 --> 00:45:38,431
Panic won't do us any good!

198
00:45:38,703 --> 00:45:39,795
Let me think.

199
00:45:40,071 --> 00:45:42,267
We've got to stall for time.

200
00:45:43,841 --> 00:45:45,502
- I'm afraid I'll faint.

201
00:45:45,810 --> 00:45:48,609
- Whatever you
do, don't do that.

202
00:45:48,879 --> 00:45:51,348
(dramatic music)

203
00:46:38,796 --> 00:46:41,595
(screaming)

204
00:46:41,899 --> 00:46:44,391
(dramatic music)

205
00:46:50,875 --> 00:46:51,569
- Hold!

206
00:46:57,048 --> 00:46:58,709
Let her continue to learn.

207
00:46:58,983 --> 00:47:02,920
The time is not yet right
that they should join with us.

208
00:47:42,326 --> 00:47:43,851
And what is this?

209
00:47:44,161 --> 00:47:45,686
- A symbol, Master.

210
00:47:45,996 --> 00:47:47,157
- What kind of symbol?

211
00:47:47,465 --> 00:47:49,832
- She loved the bull
ring and the matador.

212
00:47:50,134 --> 00:47:52,501
She danced to their destruction.

213
00:47:52,803 --> 00:47:55,773
Now she dances to
her own destruction.

214
00:47:56,040 --> 00:47:57,701
Her dance is of skulls.

215
00:48:07,151 --> 00:48:09,677
(salsa music)

216
00:49:03,374 --> 00:49:06,469
- She came to us on
the day of the dead.

217
00:49:06,744 --> 00:49:10,738
- El dia de los muertos, a
celebration in her country.

218
00:49:12,950 --> 00:49:15,578
(mariachi music)

219
00:51:08,065 --> 00:51:10,056
- Her dance has pleased me.

220
00:51:19,310 --> 00:51:22,245
(island jazz music)

221
00:51:42,866 --> 00:51:45,028
- With the loss of her lover,

222
00:51:45,336 --> 00:51:48,931
this one cast herself
into the volcano's fire.

223
00:52:51,669 --> 00:52:52,966
- She was?

224
00:52:53,270 --> 00:52:55,967
- As I said, a
worshiper of snakes,

225
00:52:56,940 --> 00:52:58,931
and of smoke, and flames.

226
00:53:00,177 --> 00:53:02,009
- Oh, yes, a religion of sorts.

227
00:53:02,312 --> 00:53:04,144
- It would seem so, Master.

228
00:53:59,837 --> 00:54:02,135
- [Mummy] I don't like snakes.

229
00:54:02,406 --> 00:54:05,103
I remember the one
Cleopatra used.

230
00:54:05,409 --> 00:54:09,107
Cute little rascal until it
flicked out that red tongue,

231
00:54:09,413 --> 00:54:11,780
and those two sharp fangs.

232
00:54:12,082 --> 00:54:14,016
You'd never think
such a little thing

233
00:54:14,318 --> 00:54:16,582
packed such a big wallop.

234
00:54:16,854 --> 00:54:18,879
- [Wolfman] Argh, hurt her?

235
00:54:19,189 --> 00:54:21,715
- [Mummy] Hell it killed her!

236
00:54:22,025 --> 00:54:24,722
(Wolfman growling)

237
00:54:39,877 --> 00:54:42,471
We had lots of snakes
in my ancient Egypt,

238
00:54:42,746 --> 00:54:44,236
slimy, slinky things.

239
00:54:45,582 --> 00:54:48,279
(Wolfman howling)

240
00:54:50,954 --> 00:54:52,115
When I was alive,

241
00:54:52,422 --> 00:54:55,619
they were the things
nightmares were made of.

242
00:54:55,926 --> 00:54:57,621
(Wolfman growling)

243
00:54:57,928 --> 00:55:00,693
(island jazz music)

244
00:56:55,812 --> 00:56:58,645
(Wolfman growling)

245
00:56:58,916 --> 00:57:01,613
(island jazz music)

246
00:58:37,614 --> 00:58:38,706
- She pleases me.

247
00:58:38,982 --> 00:58:42,418
Permit her to live in
the world of the snakes.

248
00:58:42,686 --> 00:58:46,486
Now, I will talk to the
Wolfman and the Mummy.

249
00:58:46,790 --> 00:58:48,383
- As you wish, Master.

250
00:58:52,162 --> 00:58:53,042
- [Mummy] He wants us.

251
00:58:53,263 --> 00:58:55,459
Ah, what do you suppose for?

252
00:58:55,766 --> 00:58:57,200
(Wolfman growling)

253
00:58:57,467 --> 00:58:59,595
Did you do something to
get us into trouble again?

254
00:58:59,870 --> 00:59:00,894
(Wolfman growling)

255
00:59:01,204 --> 00:59:02,682
Did you howl off-key
at the moon again?

256
00:59:02,706 --> 00:59:03,298
- Uh-uhn

257
00:59:04,674 --> 00:59:06,319
- [Mummy] I can't remember
doing anything wrong either.

258
00:59:06,343 --> 00:59:07,333
- Come!

259
00:59:07,644 --> 00:59:10,739
- [Mummy] We better go
before we make him mad.

260
00:59:14,618 --> 00:59:17,747
We are your servants, Master.

261
00:59:18,021 --> 00:59:19,352
- Of course you are.

262
00:59:19,656 --> 00:59:22,819
- [Mummy] Have we in some
way made you angry, Master?

263
00:59:23,126 --> 00:59:24,646
- [Criswell] You know
better than that.

264
00:59:24,795 --> 00:59:27,492
- We don't know of
anything, Master.

265
00:59:27,798 --> 00:59:29,459
- [Criswell] Then
why do you shake so?

266
00:59:29,733 --> 00:59:32,168
- It's not often
an emperor like you

267
00:59:32,469 --> 00:59:34,563
calls on creatures like us.

268
00:59:34,838 --> 00:59:37,671
- Well rest easy, I'm not
angry with either of you.

269
00:59:37,974 --> 00:59:41,672
- Ah, then it is some
service you desire of us.

270
00:59:43,213 --> 00:59:45,204
- You are the keepers
of the damned.

271
00:59:45,515 --> 00:59:46,949
You two know them all,

272
00:59:47,217 --> 00:59:49,879
and I am tired of this
usual type of entertainment.

273
00:59:50,187 --> 00:59:51,985
I want a decided change.

274
00:59:53,657 --> 00:59:56,183
- The moon, is soon gone!

275
00:59:56,493 --> 00:59:57,517
There is little time left

276
00:59:57,828 --> 01:00:01,287
for the remainder of
the evening's pleasures.

277
01:00:05,068 --> 01:00:07,059
- Yes, yes, yes I know all that.

278
01:00:07,370 --> 01:00:09,668
- At the first sight of
the morning sun's rays,

279
01:00:09,940 --> 01:00:11,135
we must be gone.

280
01:00:12,509 --> 01:00:14,353
- I suppose most of the
others will have to wait

281
01:00:14,377 --> 01:00:17,472
for their judgment until
after the next full moon.

282
01:00:17,747 --> 01:00:19,875
- It would seem so, Master.

283
01:00:26,223 --> 01:00:29,523
- Ah, but I declare there
is still time for something.

284
01:00:29,793 --> 01:00:31,913
- [Mummy] There are one or
two which should complement

285
01:00:32,129 --> 01:00:33,927
the night's
entertainment, Master.

286
01:00:34,231 --> 01:00:35,198
- Ah, good.

287
01:00:35,465 --> 01:00:38,594
Then I will see them,
dismiss the rest.

288
01:00:38,902 --> 01:00:41,599
- [Mummy] Yes, sir, Master.

289
01:00:41,905 --> 01:00:44,374
(dramatic music)

290
01:00:52,782 --> 01:00:54,546
- The ropes are coming loose.

291
01:00:54,818 --> 01:00:55,738
- [Shirley] Be careful!

292
01:00:55,952 --> 01:00:58,114
- I am, it's our only chance.

293
01:00:58,421 --> 01:01:00,446
- Nothing is worth your life.

294
01:01:00,757 --> 01:01:02,877
- My guess is if we don't
take the chance pretty soon,

295
01:01:03,126 --> 01:01:05,618
we're not going to have
much life left anyway.

296
01:01:05,929 --> 01:01:08,728
- I'm frightened,
I'm so frightened.

297
01:01:11,735 --> 01:01:15,569
- [Bob] Hold on just a
little longer, Shirley.

298
01:01:15,839 --> 01:01:18,433
- Be careful, oh
please be careful.

299
01:01:18,742 --> 01:01:20,062
We'll never get
out of here alive,

300
01:01:20,110 --> 01:01:22,306
I know it, I just feel it.

301
01:01:22,612 --> 01:01:23,636
- You do?

302
01:01:23,947 --> 01:01:25,608
- Yes I feel it in my bones.

303
01:01:25,916 --> 01:01:27,213
- You're talking nonsense.

304
01:01:27,484 --> 01:01:28,576
- Oh, no I'm not.

305
01:01:28,852 --> 01:01:31,651
These heathens probably
have an open grave for us.

306
01:01:31,955 --> 01:01:34,549
- They wouldn't dare put
us in the same grave.

307
01:01:34,824 --> 01:01:35,814
Or would they?

308
01:01:36,126 --> 01:01:38,094
- [Shirley] I should
hope not, I hate you!

309
01:01:38,361 --> 01:01:39,726
- That sudden?

310
01:01:40,030 --> 01:01:41,327
- [Shirley] Yes, that sudden.

311
01:01:41,631 --> 01:01:43,176
If it weren't for you
we wouldn't be hunting

312
01:01:43,200 --> 01:01:45,225
for an old cemetery
on a night like this.

313
01:01:45,535 --> 01:01:47,299
It's all your fault.

314
01:01:47,604 --> 01:01:49,698
- And I thought you loved me.

315
01:01:51,308 --> 01:01:55,404
- Not yet, I perhaps have other
plans for such a pretty one.

316
01:01:57,514 --> 01:02:01,542
No matter, I will tell you
when and if you may have her.

317
01:02:03,720 --> 01:02:06,712
(suspenseful music)

318
01:02:21,972 --> 01:02:24,100
- The Wolfman informs
me that the next one

319
01:02:24,374 --> 01:02:26,308
is the woman who
murdered her husband

320
01:02:26,576 --> 01:02:28,408
on their wedding night.

321
01:02:28,712 --> 01:02:31,807
Now she dances
with his skeleton.

322
01:02:32,082 --> 01:02:34,710
(ominous music)

323
01:03:30,407 --> 01:03:33,342
(upbeat jazz music)

324
01:08:36,212 --> 01:08:39,477
- Have you not enjoyed
the evening's festivities?

325
01:08:39,749 --> 01:08:43,743
Ah, that will soon change
when you become one of us.

326
01:08:45,588 --> 01:08:48,285
(Wolfman howling)

327
01:08:53,262 --> 01:08:54,889
It would seem that the Wolfman

328
01:08:55,198 --> 01:08:56,723
would have you for his own.

329
01:08:57,033 --> 01:08:59,559
(screaming)

330
01:08:59,869 --> 01:09:01,200
I have promised both the Wolfman

331
01:09:01,471 --> 01:09:02,961
and the Mummy a reward.

332
01:09:03,272 --> 01:09:06,731
It could be that
you are that reward.

333
01:09:07,043 --> 01:09:09,375
(screaming)

334
01:09:09,646 --> 01:09:12,638
You need not worry,
not just now anyway.

335
01:09:12,949 --> 01:09:14,314
- Leave her alone, you fiend!

336
01:09:14,617 --> 01:09:15,607
- Fiend is it!

337
01:09:15,918 --> 01:09:17,249
You will not be so fortunate.

338
01:09:17,553 --> 01:09:21,148
Your existence will
cease within moments.

339
01:09:21,424 --> 01:09:24,291
No one wishes to
see a man dance.

340
01:09:24,594 --> 01:09:28,758
And you, my dear, will
entertain for centuries to come.

341
01:09:29,065 --> 01:09:30,726
- If I could get
my hands on you!

342
01:09:31,000 --> 01:09:33,264
- Oh, you could do nothing!

343
01:09:33,569 --> 01:09:35,560
I can save you much pain.

344
01:09:35,838 --> 01:09:37,135
- Leave her alone, I tell you!

345
01:09:37,440 --> 01:09:40,740
- [Criswell] I do not
joke in my proposals.

346
01:09:41,010 --> 01:09:43,945
- She is to be mine,
it is so spoken!

347
01:09:47,517 --> 01:09:50,680
- The Princess of Darkness
would have you for her own

348
01:09:50,987 --> 01:09:53,354
to join us through extreme pain.

349
01:09:56,292 --> 01:09:59,785
Yet I am inclined for
one as lovely as you,

350
01:10:00,096 --> 01:10:02,121
to be more lenient.

351
01:10:02,432 --> 01:10:04,423
I have but to touch
you with my finger

352
01:10:04,701 --> 01:10:06,328
and it would mean
the end of you,

353
01:10:06,636 --> 01:10:09,731
all over, quickly
and painlessly.

354
01:10:10,006 --> 01:10:10,939
- No, no, no!

355
01:10:13,276 --> 01:10:14,766
- I repulse you?

356
01:10:15,044 --> 01:10:19,038
Very well, it seems you
have chosen your own fate.

357
01:10:19,348 --> 01:10:22,443
Live with it, I should
say die with it!

358
01:10:29,459 --> 01:10:32,292
- I've got the ropes loose now.

359
01:10:37,033 --> 01:10:39,001
I've got my hands free.

360
01:10:39,302 --> 01:10:42,294
Be careful, don't change
your expression too much.

361
01:10:42,572 --> 01:10:44,700
They must not catch on.

362
01:10:45,007 --> 01:10:46,475
- What can we do against them?

363
01:10:46,743 --> 01:10:48,233
- I don't know, yet.

364
01:10:48,544 --> 01:10:50,069
We'll just have to
watch our chance.

365
01:10:50,379 --> 01:10:52,313
When it comes, I'll
know what to do.

366
01:10:52,582 --> 01:10:54,607
I hope I'll know what to do.

367
01:10:54,917 --> 01:10:56,797
- I still don't know what
ever made me go steady

368
01:10:57,086 --> 01:10:58,417
with a crackpot writer like you.

369
01:10:58,721 --> 01:11:01,315
- All right, put it on heavy.

370
01:11:01,591 --> 01:11:02,888
- My old boyfriend Tommy

371
01:11:03,192 --> 01:11:05,320
would never have gotten
me in a mess like this.

372
01:11:05,595 --> 01:11:07,290
At least he's got brains.

373
01:11:07,597 --> 01:11:08,621
- Him!

374
01:11:08,931 --> 01:11:10,763
I'll bet he sleeps
with all the lights on.

375
01:11:11,067 --> 01:11:11,761
- Maybe so.

376
01:11:15,071 --> 01:11:17,096
- [Mummy] I could make
her another Cleopatra.

377
01:11:17,406 --> 01:11:18,601
- [Wolfman] Argh ha ha!

378
01:11:18,908 --> 01:11:22,367
- [Mummy] Without
the snake, of course.

379
01:11:22,678 --> 01:11:25,147
- Well?
- Ah, she will be yours.

380
01:11:25,414 --> 01:11:27,348
- When?
- At your discretion,

381
01:11:27,617 --> 01:11:30,314
but first, I desire
more entertainment.

382
01:11:30,620 --> 01:11:32,281
- The moon is almost gone!

383
01:11:32,588 --> 01:11:34,613
- Ah, there is yet time.

384
01:11:34,924 --> 01:11:37,950
- At the first sight of
the morning's rays' light.

385
01:11:38,261 --> 01:11:40,628
- I know the laws of the night.

386
01:11:40,930 --> 01:11:44,298
I state there is still
time for yet another.

387
01:11:44,600 --> 01:11:47,262
- I would have time
for my own pleasures!

388
01:11:47,570 --> 01:11:50,267
- Your own pleasure
comes only after mine,

389
01:11:50,573 --> 01:11:52,371
when I desire it.

390
01:11:52,642 --> 01:11:55,737
I am the sole ruler
of the dark world.

391
01:11:56,045 --> 01:11:59,379
There is no one to
challenge my authority here.

392
01:11:59,649 --> 01:12:02,414
My word is the
law, all powerful.

393
01:12:02,718 --> 01:12:07,155
No one is to challenge
that authority, no one!

394
01:12:07,423 --> 01:12:09,016
Is that understood?

395
01:12:09,292 --> 01:12:10,919
It is my command.

396
01:12:11,227 --> 01:12:12,888
- I understand, Master.

397
01:12:14,230 --> 01:12:16,221
- Then see that it
does not happen again,

398
01:12:16,499 --> 01:12:19,525
in the penalty of
everlasting despair.

399
01:12:19,836 --> 01:12:21,031
Now are there others?

400
01:12:21,337 --> 01:12:23,305
- There are others.

401
01:12:23,606 --> 01:12:25,734
- Well then let us proceed.

402
01:12:37,920 --> 01:12:40,446
- [Princess] She lived
as a zombie in life,

403
01:12:40,756 --> 01:12:44,454
so she will remain
forever a zombie in death.

404
01:12:46,529 --> 01:12:48,224
- Easy, Shirley, easy!

405
01:12:50,499 --> 01:12:53,264
(ominous music)

406
01:14:34,570 --> 01:14:37,665
(discordant jazz music)

407
01:16:01,657 --> 01:16:03,682
- The moon sinks
lower into the hills!

408
01:16:03,993 --> 01:16:05,427
We must hurry to the finish.

409
01:16:05,694 --> 01:16:07,662
- I will decide the conclusion.

410
01:16:07,963 --> 01:16:09,003
- You had the Mummy cancel

411
01:16:09,298 --> 01:16:10,842
all the others scheduled
for this session.

412
01:16:10,866 --> 01:16:12,391
- Then cancel my order.

413
01:16:12,701 --> 01:16:14,499
- The moon is almost gone!

414
01:16:14,804 --> 01:16:16,772
- There is yet time.

415
01:16:17,039 --> 01:16:19,133
Don't you want
your own pleasure?

416
01:16:19,408 --> 01:16:21,502
- Oh, if there is only time!

417
01:16:21,811 --> 01:16:23,506
- Ah, there is always time!

418
01:16:23,813 --> 01:16:27,078
All in good time,
there is always time.

419
01:16:27,349 --> 01:16:30,842
You shall have your
pleasures, that I decree!

420
01:16:39,929 --> 01:16:44,059
- All others were but
infinitesimal bits of fluff,

421
01:16:44,366 --> 01:16:45,561
compared to her.

422
01:16:50,172 --> 01:16:54,302
This one would have died for
feathers, furs, and fluff,

423
01:16:55,411 --> 01:16:56,606
and so she did.

424
01:16:58,047 --> 01:16:58,090
(sultry jazz music)

425
01:16:58,114 --> 01:17:00,879
(sultry jazz music)

426
01:24:18,186 --> 01:24:20,951
- The time, is short.
- For what?

427
01:24:21,957 --> 01:24:23,982
- Your pleasures of course!

428
01:24:24,293 --> 01:24:26,728
- You mean?
- You may take her now.

429
01:24:26,995 --> 01:24:28,463
- Is there time?

430
01:24:28,730 --> 01:24:30,562
- You better hope there is.

431
01:24:30,866 --> 01:24:32,994
- Thank you, Master, thank you!

432
01:24:33,302 --> 01:24:36,397
- Now hurry, hurry,
I will watch!

433
01:24:36,672 --> 01:24:39,937
Your desires may be
my pleasure also,

434
01:24:40,208 --> 01:24:44,839
our fitting climax to an
evening's entertainment.

435
01:24:45,147 --> 01:24:46,410
You must hurry now.

436
01:24:47,883 --> 01:24:51,183
(suspenseful jazz music)

437
01:26:21,943 --> 01:26:24,571
(dramatic music)

438
01:27:28,043 --> 01:27:31,069
(lighthearted music)

439
01:28:07,282 --> 01:28:09,751
- Easy, Miss, everything's
gonna be all right.

440
01:28:10,051 --> 01:28:11,177
- Bob, Bob?

441
01:28:11,453 --> 01:28:15,447
- He's right here beside
you, he'll be all right too.

442
01:28:23,298 --> 01:28:25,266
- Where are they,
where did they go?

443
01:28:25,567 --> 01:28:26,447
They tried to kill me.

444
01:28:26,601 --> 01:28:27,568
- What's this you say?

445
01:28:27,869 --> 01:28:28,909
Who was going to kill you?

446
01:28:29,204 --> 01:28:31,070
- The ghouls, they all
turned into skeletons.

447
01:28:31,373 --> 01:28:33,205
- Take it easy, Miss.

448
01:28:33,475 --> 01:28:34,965
- What she needs is a good rest.

449
01:28:35,277 --> 01:28:37,405
- It's true, I tell
you, it's true!

450
01:28:37,712 --> 01:28:39,680
They all turned into skeletons.

451
01:28:39,948 --> 01:28:41,438
She, she cut me here.

452
01:28:43,552 --> 01:28:45,247
- Probably was
bruised in the crash.

453
01:28:45,554 --> 01:28:48,353
- You know, you two
were very lucky.

454
01:28:52,327 --> 01:28:55,092
- I love you Bob, I really do.

455
01:28:57,165 --> 01:28:57,961
Forgive me?

456
01:28:59,467 --> 01:29:03,301
- There's nothing to
forgive, it was all a dream.

457
01:29:03,605 --> 01:29:04,629
- You love me then?

458
01:29:04,940 --> 01:29:06,271
- Of course I do.

459
01:29:15,817 --> 01:29:17,251
- Quick, quickly.

460
01:29:21,323 --> 01:29:23,018
Easy does it, careful.

461
01:29:46,114 --> 01:29:49,106
That's pretty bad
there, sure enough.

462
01:29:51,152 --> 01:29:53,348
- As it is with all
the night people,

463
01:29:53,655 --> 01:29:57,319
they are destroyed by the
first rays of the sun,

464
01:29:57,626 --> 01:29:59,424
but upon the first appearance

465
01:29:59,694 --> 01:30:01,526
of the deep shadows
of the night,

466
01:30:01,830 --> 01:30:03,958
and when the moon is full,

467
01:30:04,232 --> 01:30:08,032
they will return to
rejoice in their evil lust,

468
01:30:10,071 --> 01:30:12,335
and take back with
them any mortal

469
01:30:12,641 --> 01:30:15,110
who might happen along.

470
01:30:15,377 --> 01:30:17,243
Yes, they were lucky,
those two young people.

471
01:30:17,545 --> 01:30:19,309
May you be so lucky.

472
01:30:19,581 --> 01:30:23,347
But do not trust to luck
at the full of the moon.

473
01:30:23,652 --> 01:30:25,313
When the night is dark,

474
01:30:25,587 --> 01:30:28,352
make a wide path around
the unholy grounds,

475
01:30:28,657 --> 01:30:30,250
of the night people.

476
01:30:30,525 --> 01:30:33,688
Who can say that we
do not exist, can you?

477
01:30:34,930 --> 01:30:37,922
But now, we return
to our graves,

478
01:30:38,233 --> 01:30:40,099
and you may join us soon!

479
01:30:41,569 --> 01:30:44,436
(dramatic music)

