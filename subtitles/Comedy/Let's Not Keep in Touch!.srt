﻿1
00:00:06,820 --> 00:00:08,419
Gosh, I'm so full.

2
00:00:08,419 --> 00:00:10,090
Didn't we eat a lot?

3
00:00:10,090 --> 00:00:13,260
- I need to be on a diet. - You always say that.

4
00:00:15,529 --> 00:00:17,800
Do it just like how you practiced.

5
00:00:18,199 --> 00:00:21,840
No more, no less. Do it just like how you practiced.

6
00:00:23,640 --> 00:00:26,370
I heard that today's young entrepreneur competition...

7
00:00:26,370 --> 00:00:28,640
was suggested by Three-Part Baton.

8
00:00:29,739 --> 00:00:31,039
Three-Part Baton?

9
00:00:32,149 --> 00:00:34,450
- Is it Kim Min Kyu? - The person in charge...

10
00:00:34,450 --> 00:00:37,019
is a new Social Contribution Team manager named Ye Ri El.

11
00:00:37,019 --> 00:00:40,850
He authorized it in order to encourage her.

12
00:00:40,950 --> 00:00:42,990
They're engaged.

13
00:00:43,560 --> 00:00:46,460
She's Director Ye Sung Tae's only daughter.

14
00:00:46,789 --> 00:00:48,829
Why is she marrying Three-Part Baton?

15
00:00:49,329 --> 00:00:52,070
There's also the chairman's son, Hwang Yoo Cheol.

16
00:00:52,630 --> 00:00:55,070
I heard that they were friends since childhood.

17
00:00:55,469 --> 00:00:58,539
Wouldn't CEO Hwang be the better man?

18
00:00:59,170 --> 00:01:01,039
Three-Part Baton is handsome.

19
00:01:01,039 --> 00:01:05,009
He's rich, and he's our best director.

20
00:01:05,009 --> 00:01:07,379
That makes no difference.

21
00:01:07,480 --> 00:01:10,079
He has absolutely no manners, and he's a psycho.

22
00:01:10,079 --> 00:01:11,150
Goodness.

23
00:01:11,689 --> 00:01:14,719
- CEO Hwang is amazing. - Right.

24
00:01:15,920 --> 00:01:17,260
Gosh, that startled me.

25
00:01:23,129 --> 00:01:24,329
What's wrong with her?

26
00:01:24,530 --> 00:01:25,569
Let's go.

27
00:01:29,000 --> 00:01:30,439
I know him.

28
00:01:31,069 --> 00:01:33,569
Min Kyu is better than him in every way.

29
00:01:35,579 --> 00:01:37,409
I can't connect to wi-fi.

30
00:01:38,810 --> 00:01:41,750
I need to connect the phone to the internet.

31
00:01:43,049 --> 00:01:46,549
Pardon me. Do you know the password to the wi-fi?

32
00:01:47,819 --> 00:01:49,019
Internet password?

33
00:01:49,620 --> 00:01:51,429
It's KM123456.

34
00:01:52,790 --> 00:01:53,829
Thank you.

35
00:01:54,329 --> 00:01:55,400
Hold on.

36
00:02:03,000 --> 00:02:05,340
Are you here for the young entrepreneur competition?

37
00:02:06,610 --> 00:02:07,670
Yes.

38
00:02:07,670 --> 00:02:08,980
The competition is that way.

39
00:02:10,839 --> 00:02:11,910
Thank you.

40
00:02:13,910 --> 00:02:16,450
Thank goodness, he's oblivious.

41
00:02:17,619 --> 00:02:19,450
That luggage seems familiar.

42
00:02:20,089 --> 00:02:23,160
I'll explain the screening rules again.

43
00:02:23,489 --> 00:02:27,529
Once you set up your new, start-up item at your booth,

44
00:02:27,899 --> 00:02:30,499
the members of my team and the judges...

45
00:02:30,499 --> 00:02:33,600
will go around and take a look at your items.

46
00:02:33,899 --> 00:02:37,839
Please explain your items freely and confidently.

47
00:02:38,309 --> 00:02:41,980
Each of them could give you a vote.

48
00:02:42,380 --> 00:02:44,140
There won't be separate interviews.

49
00:02:44,140 --> 00:02:47,920
We'll apply the employees' votes and the judges' scores by 50:50...

50
00:02:47,920 --> 00:02:49,880
to determine the final winner.

51
00:02:50,079 --> 00:02:52,190
The result will be announced on Wednesday.

52
00:02:52,850 --> 00:02:54,559
You may already be...

53
00:02:54,920 --> 00:02:57,119
someone's one and only special person.

54
00:02:58,089 --> 00:03:02,529
These Heart Balls were inspired by my father's words.

55
00:03:05,070 --> 00:03:06,829
When you touch my Heart Ball,

56
00:03:06,829 --> 00:03:10,040
you'll think of someone you want to share your feelings with.

57
00:03:12,040 --> 00:03:15,239
If you reopen the final screening,

58
00:03:15,940 --> 00:03:18,179
I'd like to give you this Heart Ball.

59
00:03:20,450 --> 00:03:22,450
I hope that...

60
00:03:23,420 --> 00:03:26,089
your one and only will appear in your life.

61
00:03:31,559 --> 00:03:34,290
I'm coming to the final screening. Empty out the second-floor lobby.

62
00:03:34,899 --> 00:03:37,999
We have to postpone the research demonstration.

63
00:03:38,100 --> 00:03:39,829
Director Kim is on his way.

64
00:03:43,369 --> 00:03:44,610
Could you speed up, sir?

65
00:03:45,640 --> 00:03:46,709
Min Kyu can't come.

66
00:03:47,410 --> 00:03:48,610
He can't see Ji A.

67
00:03:49,440 --> 00:03:51,209
If she gets caught...

68
00:03:55,049 --> 00:03:56,850
Many of you proposed items...

69
00:03:56,850 --> 00:04:00,749
that were unique and applicable to current trends.

70
00:04:01,390 --> 00:04:03,519
We'll be responsible...

71
00:04:03,519 --> 00:04:06,529
and do our best to make fair judgments.

72
00:04:07,429 --> 00:04:08,929
And today, the CEO...

73
00:04:09,200 --> 00:04:12,470
of KM Financial will join us as the judge.

74
00:04:13,570 --> 00:04:17,070
He's the one who'll be giving one of you 100,000 dollars.

75
00:04:17,569 --> 00:04:19,610
Please welcome him with a round of applause.

76
00:04:27,150 --> 00:04:29,280
(Final Screening of New Start-up Item Contest)

77
00:04:33,819 --> 00:04:35,360
(Final Screening of New Start-up Item Contest)

78
00:05:01,080 --> 00:05:02,749
(Final Screening of New Start-up Item Contest)

79
00:05:02,749 --> 00:05:07,020
Today, 1 out of 10 young people dreams of entrepreneurship.

80
00:05:07,749 --> 00:05:09,989
However, their survival rate over five years...

81
00:05:09,989 --> 00:05:13,330
is only 19.9 percent. That's the reality.

82
00:05:13,530 --> 00:05:15,360
Therefore, KM Financial...

83
00:05:15,830 --> 00:05:18,429
won't just end our support with a mere compensation.

84
00:05:18,429 --> 00:05:20,230
Instead, we'll be continuously managing...

85
00:05:20,230 --> 00:05:22,470
and supporting your business systematically.

86
00:05:22,840 --> 00:05:26,509
That's how we aim to support your dream.

87
00:05:27,910 --> 00:05:29,040
Thank you.

88
00:05:43,489 --> 00:05:44,590
Director Kim.

89
00:05:51,270 --> 00:05:52,999
(Heart Ball, Jo Ji A)

90
00:05:54,569 --> 00:05:57,100
What's that doing there? I'm sure that's Ji A's invention.

91
00:05:57,369 --> 00:06:00,939
Excuse me. Can you tell me the Wi-Fi password?

92
00:06:01,239 --> 00:06:02,379
Then...

93
00:06:02,710 --> 00:06:05,009
was that lady with the afro my sister?

94
00:06:06,049 --> 00:06:07,710
I hope she's doing well.

95
00:06:07,949 --> 00:06:11,189
What if she's all tense and stuttering?

96
00:06:11,189 --> 00:06:13,150
What if people gather around...

97
00:06:13,150 --> 00:06:16,319
other business booths instead of hers? I'm sure that won't happen.

98
00:06:20,960 --> 00:06:23,330
Today's fortune doesn't seem to be that great.

99
00:06:24,059 --> 00:06:26,400
- Hello. We're Kind Wedding. - Please take a look at this.

100
00:06:26,400 --> 00:06:29,139
- Have a look. - These are handmade.

101
00:06:29,139 --> 00:06:32,540
- Welcome. - The grill we invented can be...

102
00:06:32,540 --> 00:06:33,840
used to grill meat or seafood.

103
00:06:33,840 --> 00:06:38,410
Have you ever felt like you're all alone?

104
00:06:39,249 --> 00:06:41,650
(Heart Ball)

105
00:06:41,650 --> 00:06:46,790
When phone calls, texts, and emails aren't enough to console you,

106
00:06:46,790 --> 00:06:51,020
You can... Try putting your hand on this Heart Ball.

107
00:06:51,759 --> 00:06:53,790
What's that for?

108
00:06:53,790 --> 00:06:56,960
It seems useless. We can use our cell phones.

109
00:06:57,299 --> 00:06:59,230
What's that? How corny.

110
00:06:59,230 --> 00:07:01,170
I wonder how she made it to the final screening.

111
00:07:01,170 --> 00:07:03,600
- She must have connections. - I guess so.

112
00:07:03,600 --> 00:07:05,170
What a useless piece of junk.

113
00:07:05,170 --> 00:07:07,040
What's that for?

114
00:07:17,080 --> 00:07:19,749
You need to be more loud.

115
00:07:19,749 --> 00:07:22,389
You always yell so loudly whenever you fight with me.

116
00:07:22,819 --> 00:07:26,259
Why are you sitting down? You have strong legs.

117
00:07:37,869 --> 00:07:39,670
(New Start-up Item Contest)

118
00:07:41,540 --> 00:07:42,910
You can't come here.

119
00:07:42,910 --> 00:07:44,549
No. Why are you here?

120
00:07:44,549 --> 00:07:45,949
Did Kim Min Kyu recognize Ji A?

121
00:07:45,949 --> 00:07:47,009
No.

122
00:07:47,009 --> 00:07:49,520
She disguised herself into a crazy woman.

123
00:07:49,520 --> 00:07:52,489
She seemed to have done everything she could not to get caught by me.

124
00:07:52,489 --> 00:07:54,689
I see. I'm relieved to hear that.

125
00:07:54,689 --> 00:07:57,020
How did you know that Ji A made it to the final screening?

126
00:07:57,119 --> 00:08:00,090
Are you seeing her again?

127
00:08:00,090 --> 00:08:03,129
I want to.

128
00:08:05,270 --> 00:08:06,470
Does Ji A know that?

129
00:08:06,470 --> 00:08:09,639
I'm about to tell her now. Is she doing well?

130
00:08:10,069 --> 00:08:11,470
She's not doing so well.

131
00:08:16,009 --> 00:08:19,549
But Ji A has something stronger than luck.

132
00:08:19,549 --> 00:08:22,619
Does she have the support of someone powerful?

133
00:08:22,619 --> 00:08:23,619
What does she have?

134
00:08:23,619 --> 00:08:25,790
She has something she got from her father.

135
00:08:25,790 --> 00:08:28,819
An inheritance that no one can ever take away from her.

136
00:08:30,520 --> 00:08:32,989
(Heart Ball, Jo Ji A)

137
00:08:36,899 --> 00:08:38,470
How disappointing.

138
00:08:38,930 --> 00:08:41,570
She was confident when she wrote the letter.

139
00:08:53,249 --> 00:08:55,850
My loving daughter, Ji A.

140
00:09:08,999 --> 00:09:11,129
I'm good with cars,

141
00:09:11,430 --> 00:09:14,169
but you're good with people.

142
00:09:16,539 --> 00:09:17,810
I'm...

143
00:09:18,509 --> 00:09:20,470
good with people?

144
00:09:21,710 --> 00:09:24,039
Just like how I'm good at fixing cars,

145
00:09:24,310 --> 00:09:28,580
you'll become someone who's good at doing necessary things for others.

146
00:09:29,379 --> 00:09:31,080
I'm sure of that.

147
00:09:33,950 --> 00:09:35,560
I believe in you.

148
00:09:40,629 --> 00:09:42,159
Dad.

149
00:10:11,330 --> 00:10:14,090
Do you have a person you love?

150
00:10:14,090 --> 00:10:16,899
Is there someone you'd like to console?

151
00:10:17,230 --> 00:10:19,029
Regardless of wherever you may be,

152
00:10:19,029 --> 00:10:23,499
this Heart Ball will allow you to send your love to that person.

153
00:10:23,499 --> 00:10:27,669
You can send your love without anyone's knowing.

154
00:10:28,340 --> 00:10:30,539
Are you saying this will light up regardless of where I may be?

155
00:10:30,539 --> 00:10:31,749
Yes, that's right.

156
00:10:31,749 --> 00:10:34,180
I'm going on a business trip to the States next week.

157
00:10:34,180 --> 00:10:36,419
If I touch this whenever I miss my daughter...

158
00:10:36,419 --> 00:10:40,820
Then your daughter's Heart Ball will light up right away.

159
00:10:40,820 --> 00:10:43,190
- Will it work even from Europe? - It'll work even from Africa.

160
00:10:43,190 --> 00:10:45,889
My gosh, I'd love to send this to my son...

161
00:10:45,889 --> 00:10:48,999
who's studying abroad in New Zealand right now.

162
00:10:48,999 --> 00:10:50,830
He's having a hard time adjusting,

163
00:10:50,830 --> 00:10:53,430
so he calls me every night crying, saying that he misses me.

164
00:10:53,430 --> 00:10:55,800
Then this will be the perfect gift.

165
00:10:55,800 --> 00:10:58,509
Every time this Heart Ball lights up,

166
00:10:58,509 --> 00:11:01,610
your son will know how much you miss him...

167
00:11:01,610 --> 00:11:04,710
right away. How great is that?

168
00:11:04,710 --> 00:11:06,149
This is incredible.

169
00:11:06,879 --> 00:11:08,980
I really want this to become an actual product in the market.

170
00:11:09,720 --> 00:11:13,090
I agree with you. My gosh, take a look at this.

171
00:11:13,090 --> 00:11:15,489
My gosh, this is great. Look at this.

172
00:11:15,489 --> 00:11:17,060
- How incredible. - This is so cool.

173
00:11:17,060 --> 00:11:20,730
This Heart Ball allows people to send their love to someone else.

174
00:11:20,730 --> 00:11:23,730
- Do you just touch it? - Yes, that's right.

175
00:11:23,730 --> 00:11:26,669
I guess it lights up every time you touch it.

176
00:11:26,669 --> 00:11:29,239
It lets the other person know that you miss them.

177
00:11:29,899 --> 00:11:31,710
- This is great. - My goodness.

178
00:11:31,710 --> 00:11:35,379
Hey, come over here. Take a look at this Heart Ball.

179
00:11:35,379 --> 00:11:37,379
I'm not sure if people know,

180
00:11:37,710 --> 00:11:41,350
but that Heart Ball was invented based on quantum physics.

181
00:11:41,980 --> 00:11:42,980
My sister knew that?

182
00:11:42,980 --> 00:11:44,419
Ji A didn't know that in her head.

183
00:11:45,190 --> 00:11:48,590
- She knew it in her heart. - Yes, that's right.

184
00:11:48,820 --> 00:11:50,119
In a way,

185
00:11:50,690 --> 00:11:52,330
you could say she's smarter than me.

186
00:11:53,289 --> 00:11:54,430
My sister?

187
00:11:54,430 --> 00:11:56,560
So please don't look down on her from now on.

188
00:11:56,560 --> 00:11:59,169
- When did I ever... - I looked down on her.

189
00:12:00,100 --> 00:12:03,999
I wasn't able to see that side of her.

190
00:12:03,999 --> 00:12:07,169
That's the mistake I made four years ago.

191
00:12:08,080 --> 00:12:10,180
You probably did the same thing.

192
00:12:11,210 --> 00:12:12,810
Ji A always used to say that...

193
00:12:13,350 --> 00:12:16,779
you and I are very much alike.

194
00:12:17,889 --> 00:12:19,749
- Incredible. - How innovative.

195
00:12:19,749 --> 00:12:21,960
- Good work. - Thank you.

196
00:12:21,960 --> 00:12:24,019
- Thank you. - Bye.

197
00:12:24,019 --> 00:12:26,730
- Hello, CEO Hwang. - Yes, hello.

198
00:12:26,730 --> 00:12:28,899
- What's your name? - It's Mink.

199
00:12:28,899 --> 00:12:30,560
What a coincidence.

200
00:12:30,560 --> 00:12:32,930
You used to have a puppy named Mink.

201
00:12:34,100 --> 00:12:36,700
- You remember that? - Of course.

202
00:12:36,700 --> 00:12:38,769
You cried so much when she died.

203
00:12:38,769 --> 00:12:40,610
You didn't even come to school for a few days.

204
00:12:41,440 --> 00:12:43,739
(Mink)

205
00:13:01,930 --> 00:13:04,830
- Ms. Ye. - Yes?

206
00:13:04,830 --> 00:13:06,430
You need to come with me.

207
00:13:06,970 --> 00:13:09,269
Okay. I'll be right back.

208
00:13:09,269 --> 00:13:10,269
Okay.

209
00:13:12,710 --> 00:13:14,110
Please do your best until the end.

210
00:13:14,110 --> 00:13:15,509
- Okay, bye. - We'll vote.

211
00:13:15,509 --> 00:13:16,739
Thank you.

212
00:13:18,649 --> 00:13:22,419
I noticed how popular your item was earlier.

213
00:13:22,519 --> 00:13:26,019
I think you can look forward to the voting results.

214
00:13:26,619 --> 00:13:29,119
Oh, thank you.

215
00:13:29,119 --> 00:13:31,360
Please do your best until the end.

216
00:13:31,360 --> 00:13:32,389
Okay.

217
00:13:33,889 --> 00:13:35,399
So this is for one person, is it?

218
00:13:44,269 --> 00:13:45,739
What if that person...

219
00:13:46,269 --> 00:13:49,180
is the person who made you suffer the most?

220
00:13:56,550 --> 00:13:59,820
This will only remind you of the pain you felt that day.

221
00:14:00,289 --> 00:14:03,619
What are you supposed to do then? Do you have to...

222
00:14:04,659 --> 00:14:06,129
just throw it away?

223
00:14:07,389 --> 00:14:10,560
Or would it be right to break it into pieces?

224
00:14:11,470 --> 00:14:13,200
Input this information.

225
00:14:13,200 --> 00:14:17,499
Those two people are your master's enemies.

226
00:14:17,499 --> 00:14:19,310
They're the worst enemies ever.

227
00:14:19,840 --> 00:14:21,310
I dreamed of...

228
00:14:22,279 --> 00:14:24,980
getting betrayed by humans.

229
00:14:27,879 --> 00:14:29,050
"Hey."

230
00:14:29,050 --> 00:14:31,649
"Why would you stay friends with a jerk who gives you a hard time?"

231
00:14:31,649 --> 00:14:33,489
"You can always make new friends."

232
00:14:36,389 --> 00:14:37,960
That's what...

233
00:14:38,190 --> 00:14:41,930
my dad said to me when I got hurt...

234
00:14:41,930 --> 00:14:43,999
by a mean friend.

235
00:14:44,570 --> 00:14:50,139
He said we need to learn how to say goodbye to old friends...

236
00:14:50,139 --> 00:14:52,009
and meet new ones just like how the river flows.

237
00:14:57,710 --> 00:14:59,950
I like how your father thinks.

238
00:15:00,009 --> 00:15:03,720
I'd love to meet him one day and listen to his lecture.

239
00:15:04,649 --> 00:15:06,350
He passed away.

240
00:15:07,419 --> 00:15:10,820
My dad passed away when I was in high school.

241
00:15:10,820 --> 00:15:12,960
And my mom passed away when I was two.

242
00:15:13,190 --> 00:15:15,629
I didn't ask about that.

243
00:15:15,860 --> 00:15:17,830
I wanted to tell you.

244
00:15:20,269 --> 00:15:21,499
You're just like...

245
00:15:22,499 --> 00:15:24,100
someone I know.

246
00:15:27,340 --> 00:15:28,810
When I touch this,

247
00:15:29,379 --> 00:15:31,379
I'll think of someone?

248
00:15:34,009 --> 00:15:35,019
What if I don't?

249
00:15:35,879 --> 00:15:36,919
You will.

250
00:15:37,480 --> 00:15:39,149
You definitely will.

251
00:15:42,919 --> 00:15:44,989
I like your confidence.

252
00:15:48,529 --> 00:15:50,830
Take one, and stand over there.

253
00:15:59,269 --> 00:16:00,310
Farther.

254
00:16:01,340 --> 00:16:03,440
Keep going. Farther.

255
00:16:23,464 --> 00:16:28,464
<font color="#80ffff">[VIU Ver] E17 I'm Not a Robot "You Can Send Aji 3 Away Now"</font>
<font color="#ff80ff">-= Ruo Xi =-</font>

256
00:16:49,919 --> 00:16:51,659
I realized after we broke up.

257
00:16:52,460 --> 00:16:53,659
I thought...

258
00:16:54,330 --> 00:16:56,330
I knew everything about Ji A,

259
00:16:56,659 --> 00:16:58,399
but I knew nothing correctly.

260
00:16:59,369 --> 00:17:00,629
Maybe that's why...

261
00:17:01,769 --> 00:17:04,909
I gave Aji 3 Ji A's face.

262
00:17:05,969 --> 00:17:07,240
I wanted to know her.

263
00:17:08,740 --> 00:17:10,080
I was curious.

264
00:17:11,750 --> 00:17:12,979
I don't want to...

265
00:17:13,709 --> 00:17:15,719
go on making this stupid mistake.

266
00:17:16,750 --> 00:17:19,550
Now, I want to get to know her properly...

267
00:17:21,159 --> 00:17:22,320
and see her properly.

268
00:17:22,990 --> 00:17:24,729
Don't worry about Aji 3's face.

269
00:17:25,790 --> 00:17:27,389
Once the testing phase is over,

270
00:17:28,000 --> 00:17:29,159
I'll change it immediately.

271
00:17:32,100 --> 00:17:33,169
Help me.

272
00:17:33,600 --> 00:17:35,070
This is between you two.

273
00:17:35,399 --> 00:17:38,570
If I ever get involved, that'll be when you made her cry.

274
00:17:40,139 --> 00:17:41,139
I'll remember that.

275
00:17:41,139 --> 00:17:43,740
Don't let Director Kim know about today.

276
00:17:44,310 --> 00:17:46,750
I don't want anything that could cause...

277
00:17:46,750 --> 00:17:49,280
a misunderstanding between him and your team.

278
00:17:49,820 --> 00:17:50,949
What do you mean?

279
00:17:50,949 --> 00:17:52,490
Director Kim...

280
00:17:53,219 --> 00:17:54,619
is fighting alone.

281
00:17:55,060 --> 00:17:57,360
Against Chairman Hwang and CEO Hwang...

282
00:17:57,590 --> 00:18:00,189
who want to sell Santa Maria to the Bold Group for cheap.

283
00:18:08,500 --> 00:18:11,169
Welcome. How much gas do you want?

284
00:18:11,409 --> 00:18:12,469
Fill her up.

285
00:18:13,969 --> 00:18:15,580
How will you pay?

286
00:18:23,979 --> 00:18:25,389
I'll swipe right away.

287
00:18:36,330 --> 00:18:37,729
Your credit card and receipt.

288
00:18:38,830 --> 00:18:39,929
Thank you.

289
00:18:43,770 --> 00:18:45,010
Bye, Ji A.

290
00:18:45,010 --> 00:18:46,939
- Bye. - Well done.

291
00:18:46,939 --> 00:18:48,240
- Good job. - Bye.

292
00:18:48,240 --> 00:18:49,610
Bye.

293
00:18:50,240 --> 00:18:53,179
- Good job. - Well done.

294
00:18:55,520 --> 00:18:57,479
Well done, Jo Ji A.

295
00:18:58,219 --> 00:18:59,290
Good for you.

296
00:19:03,860 --> 00:19:05,530
It's really over now.

297
00:19:08,360 --> 00:19:09,899
With him too.

298
00:19:16,000 --> 00:19:17,070
Stay there.

299
00:19:17,300 --> 00:19:20,369
- Get the dog. - I'm allergic to dog hair!

300
00:19:21,139 --> 00:19:22,240
Hey.

301
00:19:22,439 --> 00:19:25,379
Get off. Get off. Let go. I said let go.

302
00:19:26,010 --> 00:19:28,320
It's an allergy to human contact!

303
00:19:29,350 --> 00:19:30,820
We should go on a date.

304
00:19:36,719 --> 00:19:39,729
If he's really allergic to humans...

305
00:19:54,369 --> 00:19:56,110
How do you explain that?

306
00:19:57,179 --> 00:20:00,810
A patient improved because he found someone he could trust.

307
00:20:03,149 --> 00:20:04,379
It was you.

308
00:20:05,520 --> 00:20:06,590
It was because...

309
00:20:07,290 --> 00:20:08,689
you were with me.

310
00:20:09,119 --> 00:20:12,090
From now on, whenever I meet someone, you must be in view.

311
00:20:13,189 --> 00:20:15,699
I can't live without you.

312
00:20:16,459 --> 00:20:17,560
You are now...

313
00:20:19,399 --> 00:20:20,869
my most precious...

314
00:20:21,669 --> 00:20:22,740
treasure.

315
00:20:27,110 --> 00:20:28,139
No way.

316
00:20:30,840 --> 00:20:33,550
That's what I meant to him?

317
00:20:37,619 --> 00:20:38,689
That's why...

318
00:20:39,719 --> 00:20:41,689
he called me his treasure?

319
00:21:14,790 --> 00:21:16,590
(Patients must stay within boundaries.)

320
00:21:27,270 --> 00:21:28,300
Oh dear.

321
00:21:28,300 --> 00:21:30,770
- Are you okay? - Yes, I am. Sorry.

322
00:21:30,770 --> 00:21:32,540
- Thanks a lot. - Sure.

323
00:22:00,770 --> 00:22:01,899
For 15 years,

324
00:22:06,139 --> 00:22:07,740
you did so well.

325
00:22:11,679 --> 00:22:13,709
I was wondering why the numbers were so low.

326
00:22:13,709 --> 00:22:16,219
I had hope, but how? So suddenly?

327
00:22:16,580 --> 00:22:19,449
Did something happen that made you happy?

328
00:22:33,199 --> 00:22:34,270
I did...

329
00:22:36,340 --> 00:22:38,709
call out to someone with my heart.

330
00:22:38,709 --> 00:22:40,610
The first love you want to marry?

331
00:22:41,010 --> 00:22:42,139
Are you that happy?

332
00:22:42,139 --> 00:22:44,709
Now, you can marry her. How great is that?

333
00:22:44,709 --> 00:22:46,580
Aji 3 did something great.

334
00:22:46,879 --> 00:22:49,350
You can send Aji 3 away now.

335
00:22:49,350 --> 00:22:50,919
You can meet people freely.

336
00:22:51,550 --> 00:22:52,949
What if we were to...

337
00:22:54,250 --> 00:22:55,959
stay together like right now?

338
00:22:56,689 --> 00:22:57,929
That's nonsense.

339
00:22:58,929 --> 00:23:00,959
Is it nonsense?

340
00:23:06,770 --> 00:23:08,040
You'll get married.

341
00:23:08,300 --> 00:23:11,909
What woman would like it if a female robot hung out with her husband?

342
00:23:11,909 --> 00:23:13,310
She'd freak out and run off.

343
00:23:15,280 --> 00:23:17,040
It's because you got attached.

344
00:23:17,909 --> 00:23:19,080
I see why.

345
00:23:19,550 --> 00:23:21,419
It was the first time...

346
00:23:22,250 --> 00:23:25,290
in 15 years that you had a friend.

347
00:23:26,189 --> 00:23:28,889
You were so pleased that a robot liked you.

348
00:23:29,990 --> 00:23:32,990
How happier would you be if a human liked you?

349
00:23:33,760 --> 00:23:34,830
Aji 3...

350
00:23:35,959 --> 00:23:38,899
made me very happy as well.

351
00:23:39,929 --> 00:23:41,639
That was an illusion.

352
00:23:42,770 --> 00:23:46,169
Do you know how happy meeting your better half makes you?

353
00:23:46,540 --> 00:23:49,280
Receiving unconditional love from a complete stranger.

354
00:23:49,840 --> 00:23:52,810
Not minding giving what's yours to that stranger.

355
00:23:53,580 --> 00:23:56,020
Watching each other grow old.

356
00:23:56,020 --> 00:23:59,389
I really want you to experience that happiness.

357
00:24:00,790 --> 00:24:02,919
Only true love can give you that.

358
00:24:03,560 --> 00:24:05,860
Do you know when you feel that you're alive?

359
00:24:07,330 --> 00:24:08,760
It's when you love.

360
00:24:09,830 --> 00:24:12,369
When you touch and hug a loved one.

361
00:24:12,800 --> 00:24:14,699
I hope with my whole heart that...

362
00:24:14,699 --> 00:24:17,869
you get to feel how your bodies respond to each other.

363
00:24:19,669 --> 00:24:23,310
Until yesterday, you practiced meeting people with a robot.

364
00:24:23,740 --> 00:24:27,050
You were boxed in, and Aji 3 opened a door for you.

365
00:24:27,179 --> 00:24:29,050
Now, you must come into the world.

366
00:24:30,050 --> 00:24:31,290
Into the crowd.

367
00:24:58,050 --> 00:25:00,780
If you don't have an appointment, you must wait.

368
00:25:00,780 --> 00:25:02,379
Okay, I'll wait.

369
00:25:02,619 --> 00:25:04,649
I'll put you on the list, then.

370
00:25:04,649 --> 00:25:05,850
Thank you.

371
00:25:18,030 --> 00:25:19,300
What's up?

372
00:25:19,729 --> 00:25:21,500
Is the final screening over yet?

373
00:25:21,770 --> 00:25:23,139
It's over.

374
00:25:23,139 --> 00:25:24,340
Where are you?

375
00:25:24,340 --> 00:25:26,209
I stopped by somewhere.

376
00:25:26,209 --> 00:25:28,209
Call me back when you're free.

377
00:25:28,780 --> 00:25:30,080
I have something to tell you.

378
00:25:31,610 --> 00:25:35,320
- Here you go. - Thank you.

379
00:25:42,090 --> 00:25:44,219
That was an illusion.

380
00:25:44,219 --> 00:25:48,129
Until yesterday, you practiced meeting people with a robot.

381
00:25:48,330 --> 00:25:51,830
You were boxed in, and Aji 3 opened a door for you.

382
00:25:51,830 --> 00:25:54,070
Now, you must come into the world.

383
00:25:54,639 --> 00:25:56,169
Into the crowd.

384
00:26:14,090 --> 00:26:15,320
Help me.

385
00:26:16,619 --> 00:26:17,719
Baek Gyoon.

386
00:26:21,459 --> 00:26:23,699
What brings you here today?

387
00:26:26,100 --> 00:26:27,899
You came here two days ago.

388
00:26:28,100 --> 00:26:30,040
Has her condition gotten worse?

389
00:26:30,040 --> 00:26:31,409
No.

390
00:26:31,409 --> 00:26:34,479
I didn't come here for my niece today.

391
00:26:35,280 --> 00:26:37,209
I have...

392
00:26:37,209 --> 00:26:40,449
more questions about being allergic to human contact.

393
00:26:40,449 --> 00:26:41,520
What's your question?

394
00:26:41,520 --> 00:26:43,919
This is what you told me.

395
00:26:43,919 --> 00:26:46,689
The condition might improve...

396
00:26:46,689 --> 00:26:49,389
once you find someone you can trust and depend on.

397
00:26:49,719 --> 00:26:52,929
What did you mean exactly?

398
00:26:55,129 --> 00:26:57,199
Let me show you a video.

399
00:26:57,199 --> 00:26:59,229
It'll be easier to understand.

400
00:26:59,899 --> 00:27:01,600
Hi, I'm Jonny Brown.

401
00:27:01,600 --> 00:27:04,139
I got human allergy about 8 years ago.

402
00:27:04,139 --> 00:27:06,610
Since then, I lived alone.

403
00:27:06,610 --> 00:27:11,010
It was so bad that I couldn't even be in the same space with others.

404
00:27:11,209 --> 00:27:15,350
Johnny uploaded this video to make others understand his condition.

405
00:27:15,949 --> 00:27:19,090
Tilda was his nurse who later became his girlfriend.

406
00:27:19,090 --> 00:27:21,959
Soon after, his condition got better.

407
00:27:22,419 --> 00:27:25,229
Tilda was the cure for Johnny.

408
00:27:25,229 --> 00:27:29,860
When I get over it, I'll go out to the world with Tilda.

409
00:27:43,639 --> 00:27:45,679
For a long time,

410
00:27:48,820 --> 00:27:51,389
he must have been so scared.

411
00:27:51,619 --> 00:27:55,659
It took me 15 years to build the castle of cards.

412
00:27:56,290 --> 00:27:59,929
I started building it with my father when I was a child.

413
00:28:00,260 --> 00:28:03,800
I finished building it recently by myself.

414
00:28:04,229 --> 00:28:05,600
He must have been...

415
00:28:08,169 --> 00:28:10,169
so lonely.

416
00:28:17,580 --> 00:28:19,149
He must have...

417
00:28:22,580 --> 00:28:25,020
missed people so much.

418
00:28:27,719 --> 00:28:29,889
We can imagine how happy he was...

419
00:28:29,889 --> 00:28:31,590
when he met someone like Tilda.

420
00:28:34,899 --> 00:28:37,800
I bet Tilda was happy too.

421
00:28:39,199 --> 00:28:41,869
She was the cure...

422
00:28:43,000 --> 00:28:46,340
for the person she loved.

423
00:28:52,179 --> 00:28:54,010
Doctor.

424
00:28:54,879 --> 00:28:57,889
Is it possible...

425
00:28:58,419 --> 00:29:00,189
for the cure...

426
00:29:00,990 --> 00:29:04,629
to be something other than a human being?

427
00:29:05,229 --> 00:29:06,790
For example,

428
00:29:07,389 --> 00:29:11,669
can it be a doll that looks exactly like a human?

429
00:29:11,770 --> 00:29:15,439
Or could it be a robot?

430
00:29:16,540 --> 00:29:20,939
Does that mean the robot...

431
00:29:21,479 --> 00:29:23,179
is the cure?

432
00:29:25,479 --> 00:29:26,709
Do you know...

433
00:29:27,379 --> 00:29:29,580
Doctor Hong Baek Gyoon?

434
00:29:49,740 --> 00:29:50,899
Hello.

435
00:30:07,020 --> 00:30:08,820
What are those flowers for?

436
00:30:08,820 --> 00:30:10,360
Could they be...

437
00:30:10,959 --> 00:30:13,159
- for me? - Could they be?

438
00:30:13,530 --> 00:30:14,729
No way.

439
00:30:15,260 --> 00:30:16,899
Come on, I'll accept them.

440
00:30:16,899 --> 00:30:19,330
I'm sorry, but these aren't for you.

441
00:30:19,330 --> 00:30:21,369
You're too shy.

442
00:30:25,040 --> 00:30:26,110
What is it?

443
00:30:28,010 --> 00:30:30,310
Why are you smiling?

444
00:30:30,409 --> 00:30:32,550
Aren't you very grateful to me?

445
00:30:32,949 --> 00:30:35,219
You're so grateful that you feel shy.

446
00:30:35,219 --> 00:30:38,850
Stop talking nonsense, and get a drink.

447
00:30:39,320 --> 00:30:41,119
- I'd like the same, please. - Okay.

448
00:30:41,119 --> 00:30:42,290
Copycat.

449
00:30:42,520 --> 00:30:44,530
I'd like it in a bigger size.

450
00:30:44,530 --> 00:30:45,790
Okay, sir.

451
00:30:49,399 --> 00:30:51,570
- Here you go. - Thank you.

452
00:30:52,830 --> 00:30:54,300
All right.

453
00:30:55,399 --> 00:30:57,270
Congratulations on full recovery,

454
00:30:57,909 --> 00:30:59,139
Min Kyu.

455
00:31:03,740 --> 00:31:07,080
- It's Ji A. - Ji A, Ji A, my dear Ji A

456
00:31:07,080 --> 00:31:08,919
- How did the screening go? - Did you do well?

457
00:31:08,919 --> 00:31:11,320
Of course she did with support from a great team.

458
00:31:11,320 --> 00:31:13,619
What's with the look on your face?

459
00:31:13,719 --> 00:31:15,820
Did you ruin it?

460
00:31:18,560 --> 00:31:19,929
Who are you?

461
00:31:26,229 --> 00:31:27,699
Who are you, sir?

462
00:31:31,969 --> 00:31:33,939
Excuse me, sir.

463
00:31:43,979 --> 00:31:45,350
My goodness.

464
00:31:45,520 --> 00:31:47,320
I asked who you were!

465
00:31:47,419 --> 00:31:49,419
What's going on, Ji A?

466
00:31:50,119 --> 00:31:53,629
Did you know about it, too?

467
00:31:56,830 --> 00:31:58,369
Did you all know?

468
00:31:58,369 --> 00:32:00,669
Wait, what are you talking about?

469
00:32:00,669 --> 00:32:03,699
What's going on here?

470
00:32:03,699 --> 00:32:06,310
I told you to send a robot, not a human being.

471
00:32:06,310 --> 00:32:08,540
Do you realize what you've done?

472
00:32:08,540 --> 00:32:11,179
You could have killed someone!

473
00:32:13,280 --> 00:32:15,419
Where's Hong Baek Gyoon right now?

474
00:32:17,679 --> 00:32:19,590
(Pai)

475
00:32:29,129 --> 00:32:30,929
Drink more.

476
00:32:32,030 --> 00:32:33,929
I already did.

477
00:32:34,030 --> 00:32:35,469
Tell me what's on your mind.

478
00:32:36,439 --> 00:32:38,270
You told me...

479
00:32:40,570 --> 00:32:44,139
to train the robot.

480
00:32:46,110 --> 00:32:47,879
To share feelings...

481
00:32:49,350 --> 00:32:51,389
and input emotions.

482
00:32:51,389 --> 00:32:52,389
I did.

483
00:32:52,389 --> 00:32:54,320
When I did what you told me...

484
00:32:54,320 --> 00:32:56,520
You're fully recovered.

485
00:32:58,389 --> 00:33:00,090
I fell in love.

486
00:33:02,229 --> 00:33:03,560
What did you say?

487
00:33:07,270 --> 00:33:09,139
I fell in love...

488
00:33:14,369 --> 00:33:15,909
with Aji 3.

489
00:33:17,379 --> 00:33:18,550
I love her.

490
00:33:18,974 --> 00:33:21,674
(Episode 18 will air shortly.)

