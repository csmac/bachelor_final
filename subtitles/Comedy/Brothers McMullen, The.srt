1
00:02:31,484 --> 00:02:34,635
Standing in the door,
l could see. . .

2
00:02:34,946 --> 00:02:36,937
. . .the lights on a drop zone.

3
00:02:37,198 --> 00:02:42,113
And l had to assume that was
our drop zone, way ahead of us.

4
00:02:42,495 --> 00:02:45,009
And. . . .

5
00:02:45,290 --> 00:02:48,407
So we had the red light.

6
00:02:48,710 --> 00:02:52,339
l had everybody standing up,
ready to jump.

7
00:02:52,672 --> 00:02:56,506
So when the plane started
to get hit, and suddenly. . .

8
00:02:56,843 --> 00:03:00,882
. . .the pilot gives me the green light,
l'm out the door.

9
00:03:01,222 --> 00:03:06,057
Got such an opening blast
or opening shot from the prop blast. . .

10
00:03:06,436 --> 00:03:12,193
. . .that it broke this chinstrap
that we had on the helmet liner.

11
00:03:12,609 --> 00:03:16,602
That's when l lost this famous leg bag
everybody talks about.

12
00:03:16,946 --> 00:03:22,418
Just from the shock of the opening.
lt just flew right off my foot.

13
00:03:22,827 --> 00:03:25,295
We came from the sky.

14
00:03:25,580 --> 00:03:31,655
We hit, and in any direction you went,
there would be enemy. You knew it.

15
00:03:32,086 --> 00:03:35,476
And that was all part of what
you accepted.

16
00:03:35,798 --> 00:03:39,393
How do you prepare yourself mentally?

17
00:03:39,719 --> 00:03:42,631
Each man must do that himself.

18
00:03:42,931 --> 00:03:46,765
Each man must prepare
himself mentally. . .

19
00:03:47,101 --> 00:03:49,899
. . .to make that jump.

20
00:03:50,188 --> 00:03:54,147
ln the back of your mind,
you wonder what's gonna happen.

21
00:03:54,484 --> 00:03:57,476
You know you've been trained
and trained. . .

22
00:03:57,779 --> 00:04:01,692
. . .and what your job is
and what you're supposed to do.

23
00:04:02,033 --> 00:04:07,744
That's what you gotta think about.
We lost a lot of people that night.

24
00:04:08,164 --> 00:04:13,875
But you try to put
it all out of your mind.

25
00:05:08,766 --> 00:05:10,836
Smith!

26
00:05:11,102 --> 00:05:13,741
Give me a light.

27
00:05:43,801 --> 00:05:46,554
Lip! Did Evans make it?

28
00:05:46,846 --> 00:05:51,601
Yes, sir.
He's in Lt. Meehan's stick, sir.

29
00:05:59,859 --> 00:06:03,090
Lt. Meehan. Lt. Meehan.

30
00:06:03,404 --> 00:06:06,282
Will could use some help.

31
00:06:13,873 --> 00:06:15,386
Will!

32
00:06:15,625 --> 00:06:18,776
Will, hey! Will!
l need your help here.

33
00:07:50,303 --> 00:07:52,817
Get ready!

34
00:07:53,097 --> 00:07:55,327
Stand up!

35
00:07:56,517 --> 00:07:58,473
Hook up!

36
00:08:00,521 --> 00:08:03,513
Equipment check!

37
00:08:07,862 --> 00:08:10,979
-Sound off for equipment check!
-Ten okay!

38
00:08:11,282 --> 00:08:13,238
-Nine okay!
-Eight okay!

39
00:08:13,493 --> 00:08:15,085
-Seven okay!
-Six okay!

40
00:08:15,328 --> 00:08:16,966
-Five okay!
-Four okay!

41
00:08:17,205 --> 00:08:19,116
-Three okay!
-Two okay!

42
00:08:19,374 --> 00:08:20,648
One okay!

43
00:08:56,452 --> 00:08:58,602
Jesus Christ!
Let's go, let's go!

44
00:08:58,871 --> 00:09:02,147
-Does that light look green to you?
-Let's go!

45
00:09:02,458 --> 00:09:04,688
l'm hit!

46
00:09:16,180 --> 00:09:20,139
Jesus! Tell Meehan
to get them out of there!

47
00:09:31,571 --> 00:09:33,448
Two o'clock!

48
00:09:33,698 --> 00:09:35,768
Oh, Jesus Christ!

49
00:10:01,225 --> 00:10:02,783
-Go!
-He won't jump, sir!

50
00:10:03,019 --> 00:10:05,738
-Don't listen!
-He's staying on the plane!

51
00:10:06,022 --> 00:10:08,616
He's not jumping.

52
00:10:08,900 --> 00:10:12,336
-He's okay! Go, sir! Go!
-All right!

53
00:10:12,654 --> 00:10:14,963
Son of a bitch.

54
00:10:28,378 --> 00:10:29,936
Where's the goddamn DZ?

55
00:10:30,463 --> 00:10:34,979
-Maybe three minutes on this bearing!
-We get lower, we won't need. . .

56
00:10:35,343 --> 00:10:37,254
. . .any frigging parachutes!

57
00:10:37,512 --> 00:10:40,504
-Slow down!
-We gotta get some altitude!

58
00:10:40,807 --> 00:10:43,640
They can't jump at this speed!

59
00:10:43,935 --> 00:10:46,085
We there?

60
00:10:47,146 --> 00:10:48,374
Oh, God!

61
00:10:48,606 --> 00:10:50,244
Oh, no!

62
00:10:53,236 --> 00:10:55,466
Let's go!

63
00:11:44,329 --> 00:11:47,082
-Flash.
-Shit.

64
00:11:47,373 --> 00:11:49,807
That's not the correct reply,
trooper.

65
00:11:50,084 --> 00:11:55,283
-l say ''flash,'' you say ''thunder. ''
-Yes, sir. Thunder, sir.

66
00:11:58,801 --> 00:12:01,156
Musette bag.

67
00:12:04,057 --> 00:12:08,175
Coach? Sir, it's Hall, sir.
l was on the basketball team.

68
00:12:08,519 --> 00:12:10,874
-Leg bag?
-Prop blast got it, sir.

69
00:12:11,147 --> 00:12:14,742
-And my radio and batteries with it.
-Mine too.

70
00:12:15,443 --> 00:12:18,435
Landed somewhere behind those trees.

71
00:12:20,073 --> 00:12:22,303
Okay.

72
00:12:23,284 --> 00:12:25,479
Follow me.

73
00:12:31,834 --> 00:12:34,064
To hell with that!

74
00:12:56,109 --> 00:12:58,623
Wait until they reload.

75
00:13:01,322 --> 00:13:03,358
Go.

76
00:13:06,494 --> 00:13:10,567
-Aren't you D Company?
-Able, sir.

77
00:13:11,457 --> 00:13:14,130
Means one of us
is in the wrong zone.

78
00:13:14,419 --> 00:13:17,536
Yeah, or both of us.

79
00:13:18,673 --> 00:13:22,586
-Do you have a weapon, sir?
-Just my knife.

80
00:13:25,346 --> 00:13:28,304
Do you have any idea
where we are, sir?

81
00:13:30,935 --> 00:13:32,926
Some.

82
00:13:35,106 --> 00:13:38,143
-So you're a radioman.
-Yes, sir.

83
00:13:38,443 --> 00:13:43,392
l was until l lost my radio on the jump.
l'll get chewed out for that.

84
00:13:43,781 --> 00:13:49,458
lf you were in my platoon, l'd say you
were a rifleman first, radioman second.

85
00:13:49,871 --> 00:13:54,820
Maybe you could say that to my leader.
When we find him. lf we find him.

86
00:13:55,209 --> 00:13:56,881
lt's a deal.

87
00:13:57,128 --> 00:14:02,566
First, l need your help. Locate some
landmarks to get our bearings.

88
00:14:02,967 --> 00:14:06,801
Keep your eyes peeled
for buildings, farmhouses. . .

89
00:14:07,138 --> 00:14:11,654
. . .bridges, roads, trees.

90
00:14:15,104 --> 00:14:17,857
l wonder if the rest
are as lost as we are.

91
00:14:18,149 --> 00:14:22,427
We're not lost, private.
We're in Normandy.

92
00:15:00,024 --> 00:15:04,893
-Who's that?
-Lt. Winters? ls that you?

93
00:15:09,158 --> 00:15:11,547
-Sit down. Any weapon?
-No, sir.

94
00:15:11,828 --> 00:15:15,980
l hit the prop blast, no more leg bag.
l have a knife and TNT.

95
00:15:16,332 --> 00:15:19,051
-The 82nd boys got M-1 s though.
-82nd?

96
00:15:19,335 --> 00:15:22,532
-Where are we?
-Sir, l saw a sign back there.

97
00:15:22,839 --> 00:15:25,717
lt said ''Sainte-Mere-Eglise. ''

98
00:15:32,098 --> 00:15:34,134
Flashlight?

99
00:15:34,601 --> 00:15:35,636
Raincoat?

100
00:15:35,852 --> 00:15:38,889
-You got a raincoat?
-Yeah, yeah.

101
00:16:01,920 --> 00:16:06,755
Seven kilometers from our objective.
Four hours from our deadline.

102
00:16:07,133 --> 00:16:10,921
So, we got a lot of walking
ahead of us.

103
00:16:11,262 --> 00:16:14,299
You'll stick with us
until we find your unit.

104
00:16:14,599 --> 00:16:16,396
Let's go.

105
00:16:16,643 --> 00:16:19,521
-Where are we going?
-Causeway 2, Utah Beach.

106
00:16:19,812 --> 00:16:23,441
Germans in the fields.
We need to clear it for our boys.

107
00:16:23,775 --> 00:16:28,565
Five of us won't secure a road.
That looey don't even have a weapon.

108
00:16:28,947 --> 00:16:31,780
l don't recall railroads
near our objective.

109
00:16:32,075 --> 00:16:35,306
This is the spur line,
parallel to the river.

110
00:16:35,620 --> 00:16:38,453
-We'll see a road and bridge.
-How do you know?

111
00:16:38,748 --> 00:16:42,024
Because l studied
the sand tables, all right?

112
00:16:45,672 --> 00:16:48,823
Probably a frigging train or--

113
00:16:49,133 --> 00:16:51,772
-Flash.
-Thunder.

114
00:16:52,637 --> 00:16:54,673
Lieutenant, is that you?

115
00:16:54,931 --> 00:16:56,603
-Malarkey.
-Sir. Sir.

116
00:16:56,849 --> 00:16:59,124
-Glad to see you, sir.
-Hey, fellas.

117
00:16:59,394 --> 00:17:01,828
-Everybody okay?
-Good to see you.

118
00:17:02,105 --> 00:17:03,424
-Guarnere?
-Sir.

119
00:17:03,648 --> 00:17:06,367
You and Hall up front.

120
00:17:06,651 --> 00:17:08,881
Who the hell is Hall?

121
00:17:41,436 --> 00:17:45,031
Lipton. Go. Guarnere.

122
00:18:06,461 --> 00:18:08,053
Wait for my command.

123
00:18:31,069 --> 00:18:33,344
Grenade!

124
00:18:50,672 --> 00:18:53,425
That's enough, Guarnere!

125
00:18:55,051 --> 00:18:58,566
-Everyone okay?
-Yes, sir.

126
00:18:59,347 --> 00:19:04,740
Next time l say wait for my command,
you wait for my command, sergeant.

127
00:19:05,144 --> 00:19:07,180
Yes, sir.

128
00:19:13,778 --> 00:19:17,168
Here you go, lieutenant.
Kraut weapon.

129
00:19:20,535 --> 00:19:23,254
Fine, Quaker.

130
00:19:27,667 --> 00:19:30,420
-What's that guy's problem?
-Gonorrhea.

131
00:19:30,712 --> 00:19:31,940
Really?

132
00:19:32,171 --> 00:19:35,447
His name, dummy.
Guarnere, gonorrhea, get it?

133
00:19:35,758 --> 00:19:38,795
So besides a shitty name,
what's his problem?

134
00:19:39,095 --> 00:19:41,893
None of your fucking business, cowboy.

135
00:19:42,181 --> 00:19:45,218
-All right, let's move out.
-Sir.

136
00:19:45,518 --> 00:19:47,748
Quietly.

137
00:20:04,871 --> 00:20:08,784
-You see him? He just sat there.
-He didn't have a weapon.

138
00:20:09,125 --> 00:20:13,118
-What's he gonna do, shout?
-Shouts at me for killing Krauts.

139
00:20:13,463 --> 00:20:16,261
He just wanted you to wait
for his command.

140
00:20:16,549 --> 00:20:19,382
Joe, he don't even drink.

141
00:20:36,653 --> 00:20:39,247
Lipton! Wynn!

142
00:21:18,528 --> 00:21:22,567
Anybody need supplies or ammo,
now's the time to get it.

143
00:21:26,953 --> 00:21:29,308
McDowell!
You okay?

144
00:21:29,581 --> 00:21:32,015
-Yes, sergeant.
-Well, let's go.

145
00:21:41,968 --> 00:21:46,041
l promised my brother l'd bring
him a Luger. l got first dibs.

146
00:21:46,389 --> 00:21:48,584
Check that one.

147
00:21:53,104 --> 00:21:55,379
-The Navy!
-Landings have started.

148
00:21:55,648 --> 00:21:57,684
-Move it out.
-Right on time.

149
00:21:57,942 --> 00:22:01,252
-Yeah? Tell that to him, cowboy.
-My name's Hall.

150
00:22:01,571 --> 00:22:04,529
-That so?
-Let's move out.

151
00:22:04,824 --> 00:22:08,373
-Learn to return his fire.
-lt ain't about you.

152
00:22:08,703 --> 00:22:12,378
-He heard his brother--
-Malarkey, shut your yap.

153
00:22:13,333 --> 00:22:17,326
His brother got it at Cassino.
Found out before we jumped.

154
00:22:30,683 --> 00:22:31,911
Morning, sir.

155
00:22:32,143 --> 00:22:35,055
-Battalion will be happy to see you.
-Where?

156
00:22:35,355 --> 00:22:36,834
At the farm, sir.

157
00:22:37,065 --> 00:22:40,694
Top of the morning to you, fellas.
Enjoying the war?

158
00:22:41,027 --> 00:22:43,302
Where you from, son?

159
00:22:44,155 --> 00:22:46,669
Eugene, Oregon.

160
00:22:46,950 --> 00:22:49,623
Eugene?
You gotta be kidding me.

161
00:22:49,911 --> 00:22:52,948
Popeye, did you hear this?
l'm from Astoria.

162
00:22:53,248 --> 00:22:56,684
-You don't say.
-Yeah. Orange Street.

163
00:22:57,001 --> 00:23:01,040
-Why are you in a Kraut uniform?
-Volksdeutsche.

164
00:23:01,381 --> 00:23:04,976
-Come again?
-My family answered the call.

165
00:23:05,301 --> 00:23:08,418
All true Aryans should return
to the Fatherland.

166
00:23:08,721 --> 00:23:11,474
-Joined up in '41 .
-You're shitting me.

167
00:23:11,766 --> 00:23:16,635
Hey, Malarkey! Stop fraternizing
with the enemy! Get over here!

168
00:23:17,021 --> 00:23:20,138
-What got you to Eugene?
-l was born in Eugene.

169
00:23:20,441 --> 00:23:21,840
Really?

170
00:23:34,539 --> 00:23:36,530
Popeye.

171
00:23:36,791 --> 00:23:39,146
Hey, Popeye!

172
00:23:40,211 --> 00:23:42,771
-Hey, Harry!
-Carry on.

173
00:23:43,047 --> 00:23:45,515
Easy Company.

174
00:23:46,217 --> 00:23:48,333
-Hey, Popeye.
-Good to see you.

175
00:23:48,595 --> 00:23:52,110
-This here's Hall, Able Company.
-Known as Cowboy.

176
00:23:52,432 --> 00:23:54,309
-You from Texas?
-Manhattan.

177
00:23:54,559 --> 00:23:56,436
Lt. Winters!

178
00:23:58,563 --> 00:24:01,441
-What's the holdup?
-Not sure.

179
00:24:02,317 --> 00:24:06,026
Five'll get you ten
it's got something to do with that.

180
00:24:06,362 --> 00:24:07,875
lt's good to see you.

181
00:24:08,114 --> 00:24:10,833
Yeah, you too, Buck.
What's the situation?

182
00:24:11,117 --> 00:24:14,951
Not good. Ninety percent of
the men are unaccounted for.

183
00:24:15,288 --> 00:24:19,076
-Lt. Meehan?
-No one's seen him.

184
00:24:19,417 --> 00:24:20,850
lf he is missing. . .

185
00:24:21,085 --> 00:24:25,397
. . .wouldn't that put you in line
to be the next commander of Easy?

186
00:24:28,426 --> 00:24:31,020
Hey, Lt. Speirs.

187
00:24:32,639 --> 00:24:35,358
How many men of Dog Company
got assembled?

188
00:24:35,642 --> 00:24:38,918
-Maybe 20.
-You the only officer that made it?

189
00:24:39,228 --> 00:24:41,184
So far.

190
00:24:41,439 --> 00:24:45,478
Still waiting for orders.
You got some cigarettes?

191
00:24:45,818 --> 00:24:47,888
Yeah.

192
00:24:49,614 --> 00:24:50,729
Keep the pack.

193
00:24:50,949 --> 00:24:53,509
Yeah, so l was in the plant in '39.

194
00:24:53,785 --> 00:24:58,540
l was at Monarch tooling propeller
shafts. What are the chances?

195
00:24:58,915 --> 00:25:02,749
You and me a hundred miles apart,
working at the same job.

196
00:25:03,086 --> 00:25:06,635
-Hey, Malark! We're waiting on you.
-Yeah, l'm coming.

197
00:25:06,965 --> 00:25:09,240
l gotta run.
l'll see you around.

198
00:25:09,509 --> 00:25:13,468
Yeah, see you around.

199
00:25:23,856 --> 00:25:25,733
Zigaretten?

200
00:25:27,610 --> 00:25:29,362
Here you go.

201
00:25:32,365 --> 00:25:34,356
Danke.

202
00:25:36,369 --> 00:25:37,518
Danke.

203
00:25:40,081 --> 00:25:41,799
Thank you.

204
00:25:51,301 --> 00:25:53,531
Shit.

205
00:25:53,803 --> 00:25:55,998
Easy Company?

206
00:25:56,264 --> 00:25:58,141
Easy?

207
00:25:58,600 --> 00:26:01,876
Hey, any of you guys know
where Easy Company is?

208
00:26:02,937 --> 00:26:05,326
-You seen Lt. Meehan?
-No, not yet.

209
00:26:05,607 --> 00:26:09,441
Well, Maj. Strayer wants
Easy Company's CO up front.

210
00:26:09,777 --> 00:26:11,847
-That means you, Dick.
-Come on.

211
00:26:12,113 --> 00:26:13,910
Son of a bitch.

212
00:26:16,367 --> 00:26:19,882
Hey, Malark, where's the best chow?

213
00:26:20,204 --> 00:26:22,479
ln Berlin.

214
00:26:26,836 --> 00:26:29,555
-Let's round them up.
-Okay. Get them.

215
00:26:29,839 --> 00:26:32,512
-And the MGs?
-l'd say around here, sir.

216
00:26:32,800 --> 00:26:35,030
But l can't be sure.

217
00:26:35,678 --> 00:26:40,229
There's Kraut 88s. Ahead
and to the right about 300 yards.

218
00:26:40,600 --> 00:26:44,309
Through the gardens. They're
between us and Causeway 2.

219
00:26:44,646 --> 00:26:46,955
Firing on the boys landing at Utah.

220
00:26:47,232 --> 00:26:49,223
-Can Easy handle it?
-Yes, sir.

221
00:26:49,484 --> 00:26:53,318
My guess is, they're doing
some terrible damage there.

222
00:26:53,988 --> 00:26:57,503
Does the major know we
only have 1 2 Easy Company men?

223
00:26:57,825 --> 00:26:59,622
No.

224
00:26:59,869 --> 00:27:04,101
The 88s we've heard
have been spotted in a field. . .

225
00:27:04,457 --> 00:27:06,687
. . .down the road a ways.

226
00:27:06,960 --> 00:27:09,599
Major Strayer wants us
to take them out.

227
00:27:09,879 --> 00:27:13,792
There are two guns that we know of,
firing on Utah Beach.

228
00:27:14,133 --> 00:27:16,601
Plan on a third and a fourth. . .

229
00:27:16,886 --> 00:27:20,401
. . .here and here.
The Germans are in the trenches. . .

230
00:27:20,723 --> 00:27:25,353
. . .with access to the entire battery.
Machine gun covering the rear.

231
00:27:25,728 --> 00:27:30,597
We establish a base of fire and move
under it with two squads of three.

232
00:27:31,150 --> 00:27:33,664
How many Krauts
you think we're facing?

233
00:27:33,945 --> 00:27:38,018
-No idea.
-No idea?

234
00:27:39,826 --> 00:27:43,421
We'll take some TNT along with us.
Despite the guns.

235
00:27:43,746 --> 00:27:45,782
-Your responsibility.
-Yes, sir.

236
00:27:46,040 --> 00:27:49,794
Liebgott, take the first machine gun,
with Petty A-gunner.

237
00:27:50,128 --> 00:27:53,882
Plesha, Hendrix, you take the other.
Who does that leave?

238
00:27:54,340 --> 00:27:58,299
Compton, Malarkey,
Toye, Guarnere. Okay.

239
00:27:58,636 --> 00:28:01,833
We'll be making the main assault.
Understood?

240
00:28:02,140 --> 00:28:03,334
Yes, sir.

241
00:28:03,558 --> 00:28:05,628
Let's pack it up, boys.

242
00:28:06,936 --> 00:28:09,769
Shouldn't you be with
the Able Company guys?

243
00:28:10,064 --> 00:28:12,498
See you around, Hall.

244
00:28:17,614 --> 00:28:22,813
Lipton, when you see we've captured
the first gun, l want your TNT fast.

245
00:28:23,202 --> 00:28:24,351
Yes, sir.

246
00:28:25,830 --> 00:28:29,140
Okay. Just weapons and ammo,
drop everything else.

247
00:28:29,459 --> 00:28:32,815
Got spare ammo in a pack
or a musette bag, bring it.

248
00:28:33,129 --> 00:28:37,122
Lieutenant, sir? l was wondering,
you need an extra hand?

249
00:28:37,467 --> 00:28:39,935
-Ain't you Sink's jeep driver?
-So?

250
00:28:40,219 --> 00:28:41,288
Oh, shit.

251
00:28:41,512 --> 00:28:43,548
What's your name, trooper?

252
00:28:43,806 --> 00:28:46,559
-Lorraine, sir.
-You're with me, Lorraine.

253
00:28:46,851 --> 00:28:49,001
-Compton, second squad.
-Yes, sir!

254
00:28:49,270 --> 00:28:53,388
All right, you heard the word.
Let's move! Let's move!

255
00:29:00,406 --> 00:29:02,158
Fire!

256
00:29:28,851 --> 00:29:30,842
Go.

257
00:29:32,814 --> 00:29:35,089
Three cannons.

258
00:29:41,197 --> 00:29:44,428
Petty, we've got enfilading fire.

259
00:29:44,742 --> 00:29:47,700
All right, let's go. Stay down.

260
00:29:59,924 --> 00:30:05,078
MG42s. l'll draw fire to the right of
the truck. Take two men and hit them. . .

261
00:30:05,471 --> 00:30:06,984
. . .from the left. Go.

262
00:30:07,223 --> 00:30:08,861
Right.

263
00:30:14,230 --> 00:30:18,781
Take Ranney, envelop right, give
covering fire. Lorraine, machine gun.

264
00:30:19,152 --> 00:30:21,541
Don't give away your position early.

265
00:30:21,821 --> 00:30:25,177
l want TNT as soon as we've
captured the first gun.

266
00:30:25,491 --> 00:30:27,641
-Go.
-Yes, sir.

267
00:30:45,178 --> 00:30:46,167
Go.

268
00:30:52,644 --> 00:30:55,112
l can't see nothing.

269
00:31:44,862 --> 00:31:47,092
Come on, Buck.

270
00:32:14,267 --> 00:32:15,495
Shit!

271
00:32:21,065 --> 00:32:22,657
Let's go! Let's go!

272
00:32:23,568 --> 00:32:25,126
Follow me!

273
00:32:34,287 --> 00:32:36,323
Come on!

274
00:32:38,249 --> 00:32:41,639
Fuck! My ass!

275
00:32:45,632 --> 00:32:47,623
Sorry, sir.

276
00:32:50,845 --> 00:32:52,756
Jesus Christ!

277
00:32:56,935 --> 00:32:58,607
Fucking jeep jockey.

278
00:32:58,853 --> 00:33:03,563
Shit! l'm sorry, sir.
l screwed up.

279
00:33:05,652 --> 00:33:07,722
Grenade!

280
00:33:07,987 --> 00:33:10,706
Joe! Toye! Roll on! Roll on!

281
00:33:10,990 --> 00:33:13,299
Get off!

282
00:33:15,078 --> 00:33:16,067
Shoot!

283
00:33:16,287 --> 00:33:18,403
One lucky bastard, Joe.

284
00:33:26,839 --> 00:33:30,673
Guarnere! Malarkey! Lorraine!
Secure that gun!

285
00:33:33,429 --> 00:33:36,819
-Compton, covering fire!
-Yes, sir!

286
00:33:48,278 --> 00:33:51,395
-Where you hit, Pop?
-l fucked up my ass, sir.

287
00:33:51,698 --> 00:33:53,848
Your ass?

288
00:33:55,952 --> 00:33:57,544
-Shit.
-How bad is it?

289
00:33:57,787 --> 00:34:03,384
l'm sorry, sir. l didn't mean to
fuck up. l don't think it's too bad.

290
00:34:04,586 --> 00:34:07,100
-Can you make it back yourself?
-Yes.

291
00:34:07,380 --> 00:34:09,132
Let's move out.

292
00:34:09,382 --> 00:34:13,739
Here we go. Drop your weapon, Pop,
drop your weapon. Come on.

293
00:34:14,095 --> 00:34:16,450
One, two, three!

294
00:34:16,723 --> 00:34:18,361
Get down, Dick, get down!

295
00:34:19,142 --> 00:34:21,656
Pop!

296
00:34:21,936 --> 00:34:24,291
Popeye! Get down!

297
00:34:25,565 --> 00:34:29,080
Winters! One o'clock!

298
00:34:29,402 --> 00:34:30,596
Grenade!

299
00:34:30,820 --> 00:34:35,530
Toye! Let's go! Get out of there!
Get out of there!

300
00:34:35,909 --> 00:34:37,979
Toye!

301
00:34:38,494 --> 00:34:41,486
-Joe!
-Jesus Christ.

302
00:34:41,789 --> 00:34:44,587
Fucking twice.

303
00:34:47,337 --> 00:34:50,135
Jesus Christ!

304
00:34:51,716 --> 00:34:54,105
Lorraine! Move! Go!

305
00:34:54,385 --> 00:34:56,580
Malark!

306
00:35:02,852 --> 00:35:05,810
Ranney!
Let's move! They got the first gun!

307
00:35:13,780 --> 00:35:16,419
-There's the second gun.
-Grenades first.

308
00:35:16,699 --> 00:35:18,530
-Then keep going. Go!
-Right.

309
00:35:25,124 --> 00:35:27,035
Okay.

310
00:35:28,253 --> 00:35:30,767
Go!

311
00:35:39,264 --> 00:35:40,743
Shut up.

312
00:35:40,974 --> 00:35:42,646
-No make dead.
-Shut up.

313
00:35:42,892 --> 00:35:46,487
-No make dead.
-Shut the fuck up!

314
00:35:52,026 --> 00:35:55,143
Toye, stay down.
Stay down!

315
00:35:55,446 --> 00:35:58,836
-Compton!
-All right. Cover for me.

316
00:36:01,119 --> 00:36:04,668
They're so confused,
they're firing on the third gun!

317
00:36:04,998 --> 00:36:07,466
Better blow this
before they realize it.

318
00:36:07,750 --> 00:36:12,585
-l'm gonna find Lipton.
-All right. Toye! Cover the lieutenant!

319
00:36:15,800 --> 00:36:18,439
-One of the dead Krauts has a Luger!
-So?

320
00:36:18,720 --> 00:36:21,234
Keep your head low, Petty.
Move it!

321
00:36:21,514 --> 00:36:26,190
-Have a little suppressing fire!
-Jesus Christ!

322
00:36:26,561 --> 00:36:28,836
Malarkey!

323
00:36:30,440 --> 00:36:32,476
-Now you stop firing?
-Stay low!

324
00:36:32,734 --> 00:36:35,771
-Shit.
-Christ, they must think he's a medic.

325
00:36:36,070 --> 00:36:38,106
He's gonna need a damn medic.

326
00:36:38,364 --> 00:36:40,514
Okay, okay, okay.

327
00:36:40,783 --> 00:36:42,739
Malarkey!

328
00:36:44,412 --> 00:36:47,449
-Stay low!
-What the hell's he doing?

329
00:36:47,749 --> 00:36:49,341
Come on!

330
00:36:54,464 --> 00:36:59,936
Forgot your frigging Luger? Want l
should go get it for you? Stupid mick!

331
00:37:02,305 --> 00:37:06,218
-Where's Lipton with that TNT?
-Don't know, sir.

332
00:37:06,559 --> 00:37:08,675
-You'll be all right.
-l'm sorry.

333
00:37:08,937 --> 00:37:10,256
-Don't be.
-Sarge?

334
00:37:10,480 --> 00:37:12,869
-Yeah?
-Think this is a ticket home?

335
00:37:13,149 --> 00:37:16,459
-Maybe.
-Shit, l just got here.

336
00:37:22,784 --> 00:37:24,012
Hiya, Cowboy!

337
00:37:24,244 --> 00:37:27,441
Shut your fucking guinea trap,
Gonorrhea.

338
00:37:27,747 --> 00:37:30,864
-He's all right.
-You got a whole Kraut platoon.

339
00:37:31,167 --> 00:37:33,237
-More, captain.
-You need help?

340
00:37:33,503 --> 00:37:37,098
l need ammo, sir!
Lots of it. And TNT!

341
00:37:37,423 --> 00:37:42,099
-l got TNT, sir!
-Good job, private!

342
00:37:46,641 --> 00:37:48,871
Jesus.

343
00:37:49,394 --> 00:37:50,668
Hall! Ready?

344
00:37:50,895 --> 00:37:53,568
l don't have any way
to set it off, sir.

345
00:38:11,583 --> 00:38:14,416
Fire in the hole!

346
00:38:18,047 --> 00:38:23,565
Plesha! Covering fire. Third gun here.
Lorraine, grab more potato mashers. . .

347
00:38:23,970 --> 00:38:28,088
. . .and follow me!
Malarkey, Hall, you two. Go!

348
00:38:46,576 --> 00:38:49,727
-Running low on ammo, sir!
-And you, Malarkey?

349
00:38:50,038 --> 00:38:51,949
Okay.

350
00:38:52,206 --> 00:38:56,438
-Got enough to take the third gun?
-We'll see! Malarkey, let's go!

351
00:38:56,794 --> 00:38:58,910
Toye! Cover us!

352
00:38:59,172 --> 00:39:00,321
Okay, Hall.

353
00:39:00,548 --> 00:39:02,664
Keep your head down.

354
00:39:02,926 --> 00:39:06,885
Hall!
Leave your TNT!

355
00:39:14,729 --> 00:39:16,128
Fire in the hole!

356
00:39:22,487 --> 00:39:26,036
Malarkey, cover the front.
Go to the cannon. Go!

357
00:39:57,397 --> 00:40:00,195
l'm looking
for Battalion Headquarters!

358
00:40:00,483 --> 00:40:03,236
Are you kidding?
lt's back thataway!

359
00:40:03,528 --> 00:40:05,564
You mean over--?

360
00:40:07,949 --> 00:40:10,543
Fire in the hole!

361
00:40:22,505 --> 00:40:24,302
TNT!

362
00:40:24,549 --> 00:40:27,188
-TNT!
-Don't need it!

363
00:40:27,468 --> 00:40:30,107
-What?
-Don't need it!

364
00:40:31,014 --> 00:40:35,007
-Hall!
-Hall?!

365
00:40:35,768 --> 00:40:38,726
Where'd he come from?

366
00:40:41,441 --> 00:40:44,319
Winters!
Hester said you needed ammo!

367
00:40:44,611 --> 00:40:49,287
Malarkey!
As much as you can for everyone.

368
00:40:49,657 --> 00:40:52,694
Mind if D Company takes
a shot at the next gun?

369
00:40:52,994 --> 00:40:55,792
-All yours!
-Let's go, Dog Company!

370
00:40:56,080 --> 00:40:59,834
-Let's get them, D Company!
-Compton!

371
00:41:00,376 --> 00:41:03,015
-Who is that, Speirs?
-Out! Get out!

372
00:41:08,509 --> 00:41:10,943
What's he doing out of the trench?

373
00:41:11,220 --> 00:41:14,132
-What the hell's he doing?
-Too late!

374
00:41:14,599 --> 00:41:16,908
Oh, Jesus.

375
00:41:21,022 --> 00:41:22,216
Sir!

376
00:41:22,440 --> 00:41:26,149
Had a little trouble getting
through that first field.

377
00:41:27,528 --> 00:41:30,167
-Oh, Christ.
-We need it at the next gun.

378
00:41:30,448 --> 00:41:34,361
-Once it's blown, pull out. Go!
-Yes, sir.

379
00:41:36,037 --> 00:41:41,236
Compton, police them both, then
pull out! Lorraine! Toye! Move out!

380
00:41:49,425 --> 00:41:51,985
Move out! Move out! Move it out!

381
00:41:52,262 --> 00:41:53,456
MGs first.

382
00:41:54,514 --> 00:41:57,074
Fall back to your original positions.

383
00:41:57,350 --> 00:42:01,104
Everyone else,
maintain your base of fire.

384
00:42:05,775 --> 00:42:09,404
Okay! Back to battalion! Go! Go!

385
00:42:20,373 --> 00:42:24,844
They were 1 05s. We disabled them
and pulled out. There's maybe. . .

386
00:42:25,211 --> 00:42:28,567
. . .40 Krauts manning three
MG42s to the rear.

387
00:42:28,881 --> 00:42:30,678
-Hey, Pop.
-Coming through!

388
00:42:30,925 --> 00:42:33,803
-Forty?
-Yes, sir.

389
00:42:34,095 --> 00:42:36,563
We killed maybe
around 20, so. . .

390
00:42:36,848 --> 00:42:39,237
. . .probably there's 40 left.

391
00:42:39,517 --> 00:42:44,193
l think we need artillery or maybe
mortars might do the job.

392
00:42:56,910 --> 00:42:59,504
-Going my way?
-Sure.

393
00:43:00,246 --> 00:43:02,077
Sir.

394
00:43:02,332 --> 00:43:05,961
Careful, don't hurt yourself.

395
00:43:06,294 --> 00:43:10,003
-Nice ride you got here, Nix.
-Straight from Utah Beach.

396
00:43:10,340 --> 00:43:13,776
We should put them to work
before they're missed.

397
00:43:20,433 --> 00:43:23,743
2nd Battalion had
secured Sainte-Marie-du-Mont.

398
00:43:24,062 --> 00:43:29,261
Elements of the 4th Division began
to move men and material inland.

399
00:43:32,028 --> 00:43:36,943
The 1 01st Airborne, including Easy
Company, was scattered over Normandy.

400
00:43:37,325 --> 00:43:42,683
Success of the invasion was uncertain.
We had an hour to rest and find food...

401
00:43:43,081 --> 00:43:47,677
...before we had to move south
and secure the town of Culoville.

402
00:43:51,214 --> 00:43:55,253
-l don't wanna die in this truck.
-You're stepping on my legs!

403
00:43:55,593 --> 00:43:59,711
-Jesus, let me out of here.
-Light. Light discipline.

404
00:44:00,056 --> 00:44:01,967
Guarnere, close that flap.

405
00:44:02,225 --> 00:44:05,137
Let the Krauts cook
their own goddamn food.

406
00:44:05,812 --> 00:44:07,643
-How we doing, Malark?
-Good.

407
00:44:07,897 --> 00:44:08,886
-Good?
-Yeah.

408
00:44:09,107 --> 00:44:11,382
The lrish don't know how to cook.

409
00:44:11,651 --> 00:44:16,520
lf you have a reservation someplace
else, l'd be happy to go with you.

410
00:44:16,906 --> 00:44:19,818
-Thank you.
-Shit.

411
00:44:24,914 --> 00:44:28,065
Jesus Christ, give me some air.

412
00:44:34,549 --> 00:44:36,983
Oh, God.

413
00:44:37,635 --> 00:44:39,751
-Evening.
-Hello, sir.

414
00:44:40,013 --> 00:44:44,291
-Something die in here?
-Yeah, Malarkey's ass.

415
00:44:45,852 --> 00:44:49,845
-Any word on Lieutenant Meehan, sir?
-No, not yet.

416
00:44:52,442 --> 00:44:55,559
Don't that make you our
commanding officer, sir?

417
00:44:55,862 --> 00:44:57,580
Yeah, it does.

418
00:45:00,450 --> 00:45:04,443
-Sir.
-Joe, the lieutenant don't drink.

419
00:45:06,831 --> 00:45:09,425
lt's been a day of firsts.

420
00:45:15,256 --> 00:45:17,087
Don't you think, Guarnere?

421
00:45:20,345 --> 00:45:22,654
Yes, sir.

422
00:45:24,641 --> 00:45:25,710
Carry on.

423
00:45:25,934 --> 00:45:27,572
-Night, sir.
-Good night.

424
00:45:27,810 --> 00:45:30,722
-Oh, sergeant?
-Sir.

425
00:45:31,022 --> 00:45:34,014
l'm not a Quaker.

426
00:45:36,903 --> 00:45:40,259
-He's probably a Mennonite.
-What's a Mennonite?

427
00:45:44,619 --> 00:45:46,849
Hey!

428
00:45:47,330 --> 00:45:48,968
Dick!

429
00:45:49,207 --> 00:45:54,156
You know that map you found?
lt had every Kraut gun in Normandy.

430
00:45:56,297 --> 00:45:58,367
Here.

431
00:45:59,968 --> 00:46:02,562
Don't ever get a cat.

432
00:46:02,845 --> 00:46:05,154
What's on your mind?

433
00:46:05,431 --> 00:46:08,025
l lost a man today.

434
00:46:08,309 --> 00:46:09,708
Hall.

435
00:46:09,936 --> 00:46:13,485
Thanks.
A John Hall. New Yorker.

436
00:46:13,815 --> 00:46:16,204
Got killed today at Brecourt.

437
00:46:16,484 --> 00:46:19,635
-l never knew him.
-Yeah, you did.

438
00:46:19,946 --> 00:46:24,019
Radio op. 506th basketball team,
Able Company.

439
00:46:25,785 --> 00:46:27,696
He was a good man.

440
00:46:27,954 --> 00:46:32,266
Man.
Not even old enough to buy a beer.

441
00:46:36,629 --> 00:46:39,063
Not hungry.

442
00:46:40,508 --> 00:46:42,658
Hey, Dick.

443
00:46:43,678 --> 00:46:46,146
l sent that map up to Division.

444
00:46:46,431 --> 00:46:49,343
l think it's gonna do some good.

445
00:47:30,725 --> 00:47:35,753
That night I thanked God for
seeing me through that day of days.

446
00:47:36,147 --> 00:47:39,856
And prayed I would make
it through D + 1.

447
00:47:40,693 --> 00:47:44,686
And if somehow I managed
to get home again...

448
00:47:45,073 --> 00:47:47,951
...I promised God and myself...

449
00:47:48,243 --> 00:47:51,872
... that I would find
a quiet piece of land someplace...

450
00:47:52,205 --> 00:47:56,039
...and spend
the rest of my life in peace.