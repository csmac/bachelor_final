﻿1
00:00:02,373 --> 00:00:04,873
So I've been thinking
a lot about it, and...

2
00:00:04,908 --> 00:00:08,144
I'd really like to start calling you
"Shorty" in public.

3
00:00:08,178 --> 00:00:11,116
By the way, my office is throwing
a party tomorrow, and I would...

4
00:00:11,151 --> 00:00:13,050
- I'd really like for you to be there.
- Well, I have a photo shoot,

5
00:00:13,085 --> 00:00:14,485
but I think I could
come after that.

6
00:00:14,489 --> 00:00:16,123
Oh, yeah,
well, that sounds good.

7
00:00:16,157 --> 00:00:17,291
Who's that?

8
00:00:17,326 --> 00:00:18,995
Hmm?

9
00:00:19,029 --> 00:00:20,398
It's my doctor.

10
00:00:20,433 --> 00:00:22,168
Uh, D-Dr. Cover.

11
00:00:22,202 --> 00:00:25,875
Doc-Doctor Wyland Cover.
He's my oncologist.

12
00:00:25,910 --> 00:00:28,044
- He's an oncologist.
- Hey,

13
00:00:28,079 --> 00:00:29,547
you guys know
anything about cats?

14
00:00:29,581 --> 00:00:31,049
Yeah, I'm trying
to score some points with Daisy,

15
00:00:31,085 --> 00:00:32,753
so I promised to watch
her cat tonight.

16
00:00:32,787 --> 00:00:34,420
They carry disease,
they're obsessed with my nipples,

17
00:00:34,455 --> 00:00:36,522
and they are unwelcome
in this loft.

18
00:00:36,556 --> 00:00:39,390
Did you just say that cats are
obsessed with your nipples?

19
00:00:39,424 --> 00:00:41,224
I've told you this story before.

20
00:00:46,268 --> 00:00:49,738
I think I would have remembered
that story.

21
00:00:49,772 --> 00:00:51,206
No cats.

22
00:00:51,240 --> 00:00:53,441
Come on, bangs,
you sons of bitches.

23
00:00:53,475 --> 00:00:54,942
- Here, let me do 'em, Jess.
- What?

24
00:00:54,976 --> 00:00:57,709
- Nick, Nick, stop.
- What?

25
00:00:57,743 --> 00:00:59,242
- Stop, you're gonna ruin it.
- I'm not gonna ruin...

26
00:00:59,277 --> 00:01:01,008
I was... I was making
'em look prettier.

27
00:01:01,043 --> 00:01:03,177
- No, you weren't.
- Then they look perfect as is.

28
00:01:03,212 --> 00:01:04,445
Can I ask you a question?

29
00:01:04,480 --> 00:01:05,913
Absolutely... we having
this thing right now?

30
00:01:05,948 --> 00:01:07,847
- Do I look okay?
- Do you look okay?

31
00:01:07,881 --> 00:01:09,247
- Yeah.
- Hell yeah!

32
00:01:09,282 --> 00:01:10,880
I want to put it on you.

33
00:01:10,915 --> 00:01:12,414
On? Put it on me?

34
00:01:12,448 --> 00:01:14,549
Yeah, on... I stand by that.

35
00:01:14,583 --> 00:01:16,417
I'd like to put it on you.

36
00:01:17,286 --> 00:01:18,719
What is wrong, Jess?

37
00:01:18,753 --> 00:01:20,954
I've been at this new school
for a week now

38
00:01:20,988 --> 00:01:22,722
and I still haven't made
any friends.

39
00:01:22,756 --> 00:01:24,725
There's, like,
a clique of teachers,

40
00:01:24,759 --> 00:01:27,028
and they run the school.

41
00:01:27,162 --> 00:01:30,131
♪ You keep bumpin' me
against the wall ♪

42
00:01:30,165 --> 00:01:31,933
♪ Yeah, I know
I let you slide before ♪

43
00:01:31,967 --> 00:01:35,135
♪ Hey, but until you seen me,
trust me ♪

44
00:01:35,169 --> 00:01:36,803
♪ You ain't seen bouncin' back ♪

45
00:01:36,837 --> 00:01:39,774
♪ You keep bumpin' me
against the wall ♪

46
00:01:39,809 --> 00:01:41,477
I just want them to like me.

47
00:01:41,511 --> 00:01:42,979
I know it sounds dumb, but...

48
00:01:43,013 --> 00:01:44,482
Leave it to me.
I'll fix it.

49
00:01:44,516 --> 00:01:46,951
You're Nick Miller's girl now.
You're my old lady.

50
00:01:46,985 --> 00:01:48,885
- What?
- Whatever you need, you got it.

51
00:01:48,919 --> 00:01:50,265
Why are you talking like
a James Taylor song?

52
00:01:50,333 --> 00:01:51,457
I don't know.

53
00:01:51,492 --> 00:01:53,990
Hey, either of you guys
know anything about cats?

54
00:01:54,024 --> 00:01:56,625
Oh, my God, Schmidt has
the greatest cat story!

55
00:01:56,960 --> 00:01:59,161
- Ugh, I was there... it's disgusting.
- You shut your damn mouth, Jess.

56
00:01:59,196 --> 00:02:00,163
Thought it was cute...

57
00:02:00,197 --> 00:02:01,630
Oh, grow up!

58
00:02:01,665 --> 00:02:03,843
Nothing came out.

59
00:02:04,665 --> 00:02:06,500
♪ Who's that girl? ♪

60
00:02:06,535 --> 00:02:08,538
♪ <i>Who's that girl ♪
♪ It's Jess ♪</i>

61
00:02:08,639 --> 00:02:12,922
3.02 - Nerd
Sync and corrected by <font color="#FF0000">Elderfel</font>
www.addic7ed.com

62
00:02:14,124 --> 00:02:16,490
You know what? I've never
actually seen you this happy.

63
00:02:16,524 --> 00:02:19,238
- Who are you?
- I don't know, it's weird.

64
00:02:19,273 --> 00:02:21,133
I've never seen you smile.
Ever.

65
00:02:21,168 --> 00:02:23,284
This is the first time.
It's not bad.

66
00:02:23,319 --> 00:02:25,236
- I mean, it's not great.
- Yeah, I agree.

67
00:02:25,271 --> 00:02:26,516
Somewhere right in the middle.

68
00:02:26,551 --> 00:02:28,356
- Hey, can I ask you something?
- Of course.

69
00:02:28,391 --> 00:02:30,204
Hypothetically speaking,
what if you met somebody

70
00:02:30,238 --> 00:02:34,174
that you... that you liked,
uh, just as much as Jess?

71
00:02:34,209 --> 00:02:35,609
Schmidt, stop.

72
00:02:35,644 --> 00:02:37,111
Okay, you picked Cece,
so let Elizabeth go.

73
00:02:37,145 --> 00:02:38,779
Initially I said
"hypothetically," so just...

74
00:02:38,813 --> 00:02:40,681
Do you know a place I can get
cheap school supplies?

75
00:02:40,715 --> 00:02:42,283
I only have $15

76
00:02:42,317 --> 00:02:43,884
and a halfie...
$15 and a halfie.

77
00:02:43,918 --> 00:02:45,963
- A halfie?
- Yeah, it's a half.

78
00:02:45,998 --> 00:02:47,020
It got ripped, so it's a half.

79
00:02:47,055 --> 00:02:48,289
No one's gonna accept
that as currency.

80
00:02:48,323 --> 00:02:50,257
Hey, what's up, guys?

81
00:02:50,292 --> 00:02:52,026
- Did you press the button?
- I thought you pressed the button!

82
00:02:52,060 --> 00:02:53,861
What is wrong with you?!

83
00:02:53,895 --> 00:02:56,730
- What did you do when you walked in?
- Talked to you.

84
00:02:56,765 --> 00:02:58,299
Guys, I'm gonna go pick up
Daisy's cat.

85
00:02:58,333 --> 00:03:00,434
You know, you don't just ask anybody
to take care of your cat.

86
00:03:00,468 --> 00:03:01,589
That's a very serious thing,

87
00:03:01,603 --> 00:03:04,104
so I decided
I'm gonna go over there

88
00:03:04,139 --> 00:03:06,774
and I'm gonna ask Daisy
to be my girlfriend.

89
00:03:06,808 --> 00:03:08,108
- You are?
- Whoa!

90
00:03:08,143 --> 00:03:09,410
- Yeah.
- Major move!

91
00:03:09,444 --> 00:03:11,812
- Good stuff.
- Look at you, man.

92
00:03:11,846 --> 00:03:14,087
So I'm just gonna go over there
and I'm gonna tell Daisy,

93
00:03:14,115 --> 00:03:18,752
"Listen, my heart is a two-man bike,
and I want you in the rear."

94
00:03:18,787 --> 00:03:20,888
What do you think?

95
00:03:20,922 --> 00:03:23,666
Winston, don't say that.
You want her in your...?

96
00:03:23,701 --> 00:03:25,059
You can say "the rear."
You don't want her in your rear.

97
00:03:25,093 --> 00:03:26,727
No, don't say
"the rear" at any time.

98
00:03:26,761 --> 00:03:27,828
- You guys are just jealous.
- What?

99
00:03:27,862 --> 00:03:29,463
You guys are gonna steal
my swag.

100
00:03:29,497 --> 00:03:31,799
I wouldn't even know how
to begin to steal a swag.

101
00:03:32,762 --> 00:03:34,967
- I thought you pressed the button.
- I've been way over here.

102
00:03:35,002 --> 00:03:37,771
- I-I haven't pressed...
- I'll press the button.

103
00:03:37,806 --> 00:03:41,475
Last night,
I rectified our budget

104
00:03:41,509 --> 00:03:46,180
while taking a long soak
in my new Jacooz.

105
00:03:46,214 --> 00:03:49,750
My elbows are as soft as babies.

106
00:03:49,784 --> 00:03:51,810
I love a soft elbow.

107
00:03:51,845 --> 00:03:53,446
How about you bring
that noise over here.

108
00:03:54,321 --> 00:03:55,255
Good meeting.

109
00:03:55,590 --> 00:03:56,924
Great, well...

110
00:03:56,958 --> 00:03:58,792
Uh...

111
00:04:01,162 --> 00:04:03,197
- Foster's such a dork.
- He needs to get laid.

112
00:04:03,231 --> 00:04:04,798
I got laid.

113
00:04:05,500 --> 00:04:07,468
This morning.

114
00:04:07,502 --> 00:04:09,103
From my boyfriend.

115
00:04:09,137 --> 00:04:10,437
So...

116
00:04:10,472 --> 00:04:13,474
coitus no-one-interruptus.

117
00:04:13,508 --> 00:04:17,177
That... because we finished, so...

118
00:04:17,212 --> 00:04:19,012
Why are you talking to us?

119
00:04:19,047 --> 00:04:21,381
- You have owl eyes.
- Yeah.

120
00:04:23,852 --> 00:04:24,818
Hey!

121
00:04:24,853 --> 00:04:25,819
Trust me.

122
00:04:25,854 --> 00:04:27,287
What's up, teachers?

123
00:04:27,322 --> 00:04:30,057
Who wants free school supplies?

124
00:04:30,091 --> 00:04:32,635
- What are you doing here?
- Point to the ones who don't like you...

125
00:04:32,727 --> 00:04:34,528
- I can handle this myself.
- I'm fixing it for you.

126
00:04:34,562 --> 00:04:36,330
And then she... Yeah.

127
00:04:36,364 --> 00:04:39,333
Say, listen, why don't you guys
come to my bar later for free drinks.

128
00:04:39,367 --> 00:04:40,601
Teachers don't pay.

129
00:04:41,669 --> 00:04:43,036
We'll think about it.

130
00:04:43,071 --> 00:04:44,471
Okay, they'll think about it.

131
00:04:44,506 --> 00:04:46,006
Bye.

132
00:04:46,040 --> 00:04:47,975
Oh, my God.

133
00:04:48,009 --> 00:04:49,999
- That was genius.
- It's a no-brainer.

134
00:04:50,034 --> 00:04:51,845
Whoa, that's an amazing move,
sex partner.

135
00:04:51,880 --> 00:04:53,480
Sex part...

136
00:04:53,515 --> 00:04:56,083
With the promotion, uh,
I got a new office,

137
00:04:56,117 --> 00:04:58,886
so I don't have to look at Bethany's
mismanaged eyebrows all day.

138
00:04:58,920 --> 00:05:01,522
Do you like it?
It's a perfect two-thirds replica

139
00:05:01,556 --> 00:05:03,090
of Don Draper's
office from Mad Men,

140
00:05:03,124 --> 00:05:04,758
and you're my sexy Peggy.

141
00:05:04,793 --> 00:05:06,393
Good God,
at least shut the door.

142
00:05:06,428 --> 00:05:07,928
My tiny credenza is in the way.

143
00:05:07,962 --> 00:05:08,929
Hey, Elizabeth,

144
00:05:08,963 --> 00:05:10,431
are you gonna be coming
to the party tomorrow night?

145
00:05:10,465 --> 00:05:12,499
Party? What party?

146
00:05:12,534 --> 00:05:13,834
The big office party.

147
00:05:13,868 --> 00:05:16,270
Schmidt,
did you not invite her?

148
00:05:16,304 --> 00:05:19,440
I was just about to
invite you to go.

149
00:05:19,474 --> 00:05:21,341
Cool, it's a date.

150
00:05:21,376 --> 00:05:22,709
- Bye, baby.
- Bye.

151
00:05:22,744 --> 00:05:24,244
Mwah, mwah.

152
00:05:25,814 --> 00:05:27,247
How... how dare you interfere

153
00:05:27,282 --> 00:05:28,882
with my personal life,
you crone.

154
00:05:28,917 --> 00:05:30,584
I don't...
I don't kick up a fuss

155
00:05:30,618 --> 00:05:33,120
when you massage yourself
with your yoga ball.

156
00:05:33,154 --> 00:05:35,289
Oh, I'm sorry,
were you gonna invite Cece?

157
00:05:35,323 --> 00:05:38,792
Beth, please. I am in love
with both of these women.

158
00:05:38,827 --> 00:05:41,128
Do I feel bad
about lying to them?

159
00:05:41,162 --> 00:05:43,297
Yes. I'm a Sagittarius.

160
00:05:43,331 --> 00:05:45,199
Congrats on your new office.

161
00:05:45,233 --> 00:05:47,267
Gina gave birth on that carpet.

162
00:05:53,408 --> 00:05:55,375
Consider this closed.

163
00:05:55,410 --> 00:05:58,078
Right, so, yeah, um...

164
00:05:58,112 --> 00:06:02,049
Um, so, Daisy, what I was
really trying to say...

165
00:06:02,083 --> 00:06:04,051
is, uh...

166
00:06:04,085 --> 00:06:05,452
Is your shower running?

167
00:06:05,487 --> 00:06:06,787
Yeah, I was just about
to take a shower.

168
00:06:06,821 --> 00:06:08,055
- What's going on?
- Okay.

169
00:06:08,089 --> 00:06:11,992
Um, Daisy, I think
what we have is real,

170
00:06:12,026 --> 00:06:14,328
and, uh, I want us
to be exclusive...

171
00:06:14,362 --> 00:06:18,832
just you and me in the rear.

172
00:06:18,867 --> 00:06:21,568
Yeah, that sounds great.

173
00:06:21,603 --> 00:06:24,037
Awesome, um...

174
00:06:25,807 --> 00:06:28,473
Sorry, that's mine.

175
00:06:28,508 --> 00:06:29,443
So, Furguson gets

176
00:06:29,477 --> 00:06:31,478
his wet food in the morning,
dry food at night.

177
00:06:35,817 --> 00:06:37,818
Somebody in there?

178
00:06:37,852 --> 00:06:38,852
No.

179
00:06:40,655 --> 00:06:41,855
No treats after midnight.

180
00:06:43,725 --> 00:06:47,995
"Oh, my name is Principal Foster,
and this is

181
00:06:48,029 --> 00:06:49,129
the Jacooz."

182
00:06:49,163 --> 00:06:50,430
"The Jacooz."

183
00:06:50,465 --> 00:06:52,266
He's so nice, don't you think?

184
00:06:52,300 --> 00:06:54,924
He's such a nice boss.

185
00:06:56,671 --> 00:06:59,339
Y'all want some drinks?

186
00:06:59,374 --> 00:07:00,641
- Yeah, okay.
- Yep.

187
00:07:00,675 --> 00:07:02,543
- Yeah. Thank, thank you.
- Thank you.

188
00:07:02,577 --> 00:07:04,611
What do you think?
I think it's going really well.

189
00:07:04,646 --> 00:07:05,879
Okay, I just realized
what's happening.

190
00:07:05,914 --> 00:07:07,548
It's high school over there,

191
00:07:07,582 --> 00:07:09,883
and they're the cool kids,
and you're the nerd.

192
00:07:09,918 --> 00:07:11,451
I was never a nerd.

193
00:07:11,486 --> 00:07:13,787
♪ On western wings
the fowls doth fly ♪

194
00:07:13,821 --> 00:07:16,456
♪ On briny swells
the fishes doth swim ♪

195
00:07:16,491 --> 00:07:18,392
Oh, like you were so cool.

196
00:07:19,427 --> 00:07:21,228
Stall!

197
00:07:21,262 --> 00:07:22,796
Nick! Nick! Nick!

198
00:07:25,567 --> 00:07:27,167
The Miller Sack Pack.

199
00:07:27,201 --> 00:07:30,170
- Miller Sack Pack?
- They made shirts.

200
00:07:30,204 --> 00:07:32,205
You know what? I'm not
explaining my coolness to you.

201
00:07:32,240 --> 00:07:34,841
I was cool. Listen to me.
You want to be in with them?

202
00:07:34,876 --> 00:07:37,344
Roll with the punches,
go with the flow.

203
00:07:37,378 --> 00:07:39,313
Make fun of your boss a little bit.

204
00:07:39,347 --> 00:07:40,681
Don't follow your instincts.

205
00:07:40,715 --> 00:07:42,349
I wouldn't sing.

206
00:07:42,383 --> 00:07:45,185
But most importantly,
follow my lead.

207
00:07:45,219 --> 00:07:48,155
And by "follow my lead,"
I mean drink.

208
00:07:48,189 --> 00:07:49,723
And I mean drink...

209
00:07:49,757 --> 00:07:51,758
a lot.

210
00:07:51,793 --> 00:07:54,595
Drinking to be cool, Nick?
That's not a real thing.

211
00:07:54,629 --> 00:07:57,431
That's the only thing in
the world I know to be true.

212
00:07:58,800 --> 00:08:00,601
Schools are for fools!

213
00:08:00,635 --> 00:08:01,735
They are.

214
00:08:01,769 --> 00:08:04,271
Sometimes firemen are women!

215
00:08:04,305 --> 00:08:06,440
Sometimes! Sometimes!

216
00:08:10,537 --> 00:08:13,618
Hey, there she is.
Ol' Toilet Pants,

217
00:08:13,660 --> 00:08:17,181
the girl who bet herself six dollars
she could dance in a toilet bowl.

218
00:08:17,216 --> 00:08:20,669
♪ Oh, hey, yeah, yeah, yeah ♪

219
00:08:20,704 --> 00:08:22,938
♪ What's going on? ♪

220
00:08:22,973 --> 00:08:24,540
I'm cool.

221
00:08:24,574 --> 00:08:26,375
You are cool,
but you still got to go to work,

222
00:08:26,376 --> 00:08:27,877
'cause you're running late,
so up and at 'em.

223
00:08:27,911 --> 00:08:30,246
Hey, Nick, I got to talk to you.
I have a real problem.

224
00:08:30,280 --> 00:08:32,672
- Oh, my God. Did you kill Jess?
- No, no, no, no.

225
00:08:32,707 --> 00:08:34,850
She just got really drunk last night,
and she passed out.

226
00:08:34,885 --> 00:08:36,552
Hey, guys.

227
00:08:36,586 --> 00:08:38,420
- Are you crying, buddy?
- No.

228
00:08:38,455 --> 00:08:39,722
Hey, did he kill Jess?

229
00:08:39,756 --> 00:08:42,258
No, I didn't kill Je...

230
00:08:42,292 --> 00:08:43,859
Okay, remember
when I was so hungover,

231
00:08:43,894 --> 00:08:45,455
I almost missed
my flight for Christmas?

232
00:08:45,462 --> 00:08:46,595
Ah, count it down.

233
00:08:46,630 --> 00:08:48,797
- Three, two, one.
- Two, one.

234
00:08:48,832 --> 00:08:52,101
♪ I believe I can fly ♪

235
00:08:52,135 --> 00:08:54,703
♪ I believe
I can touch the sky ♪

236
00:08:54,738 --> 00:08:58,207
♪ I think about it
every night and day ♪

237
00:08:58,241 --> 00:09:01,810
♪ Spread my wings
and fly away ♪

238
00:09:01,845 --> 00:09:03,479
♪ I believe I can soar ♪

239
00:09:03,513 --> 00:09:05,114
♪ Soar... ♪

240
00:09:05,148 --> 00:09:06,649
- I'm up, I'm up, I'm up.
- Oh, she's awake.

241
00:09:06,683 --> 00:09:08,479
That was beautiful.
Where are my tights?

242
00:09:08,514 --> 00:09:10,074
Uh, in the bathroom
where you barfed.

243
00:09:10,609 --> 00:09:12,254
You sounded
really good there, man.

244
00:09:12,288 --> 00:09:13,589
- Start the band.
- What did you do to her?

245
00:09:13,623 --> 00:09:15,524
I just told her that I thought
drinking would make her cool.

246
00:09:15,558 --> 00:09:17,359
What are you,
a 14-year-old hockey player?

247
00:09:17,394 --> 00:09:19,354
You're supposed to protect her,
not encourage her.

248
00:09:19,362 --> 00:09:21,864
Okay, I am protecting her.
She's my old lady now.

249
00:09:21,898 --> 00:09:24,266
- By turning her into you?
- I'm not...

250
00:09:24,301 --> 00:09:27,202
Yeah, Daisy cheating on me.

251
00:09:28,805 --> 00:09:30,873
Wait, we're using real honey?

252
00:09:30,907 --> 00:09:32,107
So sticky.

253
00:09:32,142 --> 00:09:34,376
Hey, babe!
What a nice surprise!

254
00:09:34,411 --> 00:09:36,078
I came to tell you

255
00:09:36,112 --> 00:09:38,380
that you can't come with me
to the party tonight.

256
00:09:38,415 --> 00:09:42,347
They just told me that it's
employees only. No sig oths.

257
00:09:42,382 --> 00:09:43,752
Just say "significant others."

258
00:09:43,787 --> 00:09:45,754
Maybe you have that kind of time,
but I'm on a tight sched.

259
00:09:45,789 --> 00:09:48,290
Looks like we're gonna go late
here anyway, so no worries.

260
00:09:48,325 --> 00:09:50,125
All right... Shorty.

261
00:09:51,428 --> 00:09:53,329
All right, bring in the bees!

262
00:09:53,363 --> 00:09:54,863
Oh, wait.
The bees are real, too?

263
00:09:54,898 --> 00:09:56,365
Oh, hell no!

264
00:09:56,399 --> 00:09:58,300
Nick, I need your help, okay?

265
00:09:58,335 --> 00:09:59,735
Now, I am no plumber,

266
00:09:59,769 --> 00:10:02,805
but I'm 99% sure that there
was a guy in her bathroom.

267
00:10:02,839 --> 00:10:04,773
So, she agreed
to be exclusive with you...

268
00:10:04,808 --> 00:10:07,743
while the guy she is
obviously sleeping with

269
00:10:07,777 --> 00:10:11,280
was washing his size 15
ding-dong in the shower.

270
00:10:11,314 --> 00:10:13,582
And now you're taking care
of her cat.

271
00:10:13,616 --> 00:10:15,150
Those are the bullet points,
yes.

272
00:10:15,185 --> 00:10:18,187
You know what you got to do
to get yourself out of this situation.

273
00:10:18,222 --> 00:10:20,597
Break up with Daisy!

274
00:10:20,632 --> 00:10:23,275
Yeah. Yeah, I mean, you're
making some very good points.

275
00:10:23,310 --> 00:10:25,603
You know, break up with Daisy.
That's an option.

276
00:10:25,607 --> 00:10:27,753
I was thinking, like,
I'll kill her cat.

277
00:10:27,831 --> 00:10:28,831
Winston.

278
00:10:30,433 --> 00:10:32,134
Yeah, I'm-I'm gonna
kill her cat first.

279
00:10:32,168 --> 00:10:33,535
- Hey.
- I'm gonna kill you.

280
00:10:37,507 --> 00:10:39,341
Hey, it's little Toilet Pants.

281
00:10:39,376 --> 00:10:40,509
Hey, Toilet Pants!

282
00:10:40,543 --> 00:10:43,087
♪ Toilet Pants! ♪

283
00:10:43,122 --> 00:10:44,780
Please stop shouting.

284
00:10:44,814 --> 00:10:45,814
You are so awesome.

285
00:10:45,849 --> 00:10:48,150
I want to show you something.

286
00:10:48,184 --> 00:10:49,952
Oh!

287
00:10:49,986 --> 00:10:51,220
Oh. Oh. Oh.

288
00:10:52,555 --> 00:10:54,423
You're one of us now.

289
00:10:54,457 --> 00:10:57,259
You never have to drink
out of a paper cup again.

290
00:10:57,293 --> 00:10:58,894
Fun mugs.

291
00:11:00,230 --> 00:11:03,298
This one makes me look
like I have a...

292
00:11:03,333 --> 00:11:04,967
- mustache.
- Mustache.

293
00:11:05,001 --> 00:11:06,001
Mustache.

294
00:11:10,240 --> 00:11:12,207
- What's up, Winston?
- Oh, hey, Nick.

295
00:11:12,242 --> 00:11:15,210
You know, I couldn't figure out
the best way to kill Furguson,

296
00:11:15,245 --> 00:11:17,179
so I decided
to just let him choose

297
00:11:17,213 --> 00:11:19,114
how he wants to die.

298
00:11:19,149 --> 00:11:21,650
If only I could get him
to leave that damn sunbeam.

299
00:11:23,053 --> 00:11:25,580
- You're gonna murder a cat?
- Yeah.

300
00:11:25,615 --> 00:11:27,279
- This is a joke, right?
- No.

301
00:11:27,314 --> 00:11:30,059
- This is one of your pranks?
- An eye for an eye, Nick.

302
00:11:30,093 --> 00:11:31,460
A cat for a cat.

303
00:11:31,494 --> 00:11:33,128
But what's the other cat?

304
00:11:33,163 --> 00:11:35,531
My heart.

305
00:11:35,565 --> 00:11:37,566
Where's that big flashlight
you can hit people with?

306
00:11:37,600 --> 00:11:40,235
Also, do we have any meat pieces
that might distract a dog?

307
00:11:40,270 --> 00:11:42,404
Winston,
don't do anything stupid.

308
00:11:42,439 --> 00:11:44,673
- Jess, what are you doing?
- Oh, yeah, yeah. Cool, cool. It's nothing.

309
00:11:44,707 --> 00:11:46,719
We're just breaking
into Foster's backyard,

310
00:11:46,754 --> 00:11:49,078
and we are gonna put
our butts in his Jacuzzi.

311
00:11:49,112 --> 00:11:50,494
You're gonna put your butts
in his Jacuzzi?

312
00:11:50,530 --> 00:11:52,618
- Isn't it awesome?
- Uh, no, it's not awesome. It's a crime.

313
00:11:52,716 --> 00:11:54,650
It's a very stupid crime,
but it's a crime.

314
00:11:54,684 --> 00:11:56,652
Hey, do you have any makeup
or costumes that might

315
00:11:56,686 --> 00:11:57,986
make someone look like a bush?

316
00:11:58,021 --> 00:11:59,521
You are about to commit a felony.

317
00:11:59,556 --> 00:12:02,558
Oh, my God, Nick!
You said roll with it!

318
00:12:02,592 --> 00:12:04,259
You're not the kind of person
who could break

319
00:12:04,294 --> 00:12:05,928
into the principal's house
and get away with it.

320
00:12:05,962 --> 00:12:07,689
You're the kind of person
who gets caught,

321
00:12:07,724 --> 00:12:09,765
and it ends up on the Internet
in a funny way.

322
00:12:09,799 --> 00:12:13,001
My God!
That is the cutest cat!

323
00:12:13,036 --> 00:12:14,336
Uh, don't get attached.

324
00:12:14,370 --> 00:12:16,171
Okay, both of you,
knock it off!

325
00:12:16,206 --> 00:12:17,606
Winston!

326
00:12:17,640 --> 00:12:19,274
Look, Nick, you're right.

327
00:12:19,309 --> 00:12:21,254
I'm a nerd.
I've never fit in.

328
00:12:21,289 --> 00:12:22,910
If I met you in high school,

329
00:12:22,945 --> 00:12:25,199
you never would have even
noticed me.

330
00:12:25,234 --> 00:12:26,682
That's just because
I didn't go to class.

331
00:12:26,716 --> 00:12:29,518
- I wouldn't have seen you.
- I know it's stupid, but I'm doing it.

332
00:12:29,552 --> 00:12:31,687
- I'm not letting you go.
- Oh, you're not letting me?

333
00:12:31,721 --> 00:12:32,881
No.

334
00:12:32,992 --> 00:12:34,892
If for some reason
I don't come back tonight,

335
00:12:34,927 --> 00:12:38,779
I'll be at the Appleton Suites
under the name Suzuki Sinclair.

336
00:12:38,814 --> 00:12:40,596
- Ask for Charles.
- Don't... Hey, hey.

337
00:12:40,630 --> 00:12:42,397
We're not done with this conversation,
young lady!

338
00:12:42,432 --> 00:12:44,299
- Well, we are done!
- Oh, yeah, then where are you going?!

339
00:12:44,334 --> 00:12:46,068
- I'm getting a flashlight!
- Oh, a hammer, huh?

340
00:12:46,102 --> 00:12:47,703
- Hey, Winston, Winston.
- Good way to go. Thr...

341
00:12:47,737 --> 00:12:49,638
- Don't hurt that cat.
- Uh-huh.

342
00:12:49,672 --> 00:12:52,374
- Hey, Furguson? Furguson?
- You scared him. Don't do this.

343
00:12:52,408 --> 00:12:54,143
- Furguson?
- Winston!

344
00:12:54,177 --> 00:12:56,311
Winston, Winston,
stop what you... Stop!

345
00:12:56,346 --> 00:12:58,046
Don't do that scary walk!

346
00:12:58,081 --> 00:13:00,616
Do not do that scary walk
that you do that scares me!

347
00:13:00,650 --> 00:13:02,251
Hey, Jess? Jess?

348
00:13:02,285 --> 00:13:04,483
Je...?
Winston, don't kill that...!

349
00:13:04,518 --> 00:13:06,655
Jess!
I don't know what to do!

350
00:13:12,063 --> 00:13:13,523
- Oh, good Lord.
- What's wrong?

351
00:13:13,558 --> 00:13:15,891
Nothing. You know what?
Let me show you the stairwell

352
00:13:15,925 --> 00:13:17,659
where they got the idea
for the escalator.

353
00:13:19,996 --> 00:13:22,564
Shorty! My love.
My only love.

354
00:13:22,598 --> 00:13:24,918
I'm sorry. I just... I quit that job,
and I had to see you.

355
00:13:24,934 --> 00:13:27,035
How emotional.
Let's go talk about it in private

356
00:13:27,069 --> 00:13:29,404
in a perfect two-thirds replica
of Don Draper's office.

357
00:13:29,438 --> 00:13:31,439
You stay here.
I'm gonna get you a drink.

358
00:13:31,474 --> 00:13:33,742
No, no, no, please,
you-you... you stay here.

359
00:13:35,912 --> 00:13:36,978
- So...
- Oh, God, what?

360
00:13:37,013 --> 00:13:39,414
They're both here.
The gas has been spilled.

361
00:13:39,448 --> 00:13:40,882
All we need is a match.

362
00:13:40,917 --> 00:13:42,332
What, are you narrating a book?

363
00:13:49,959 --> 00:13:52,427
Here it comes.

364
00:13:52,461 --> 00:13:55,297
The long nap.

365
00:13:58,868 --> 00:14:01,303
Oh, wow. Tell you what,
man, we both need to just

366
00:14:01,337 --> 00:14:02,604
cool off and have a drink.

367
00:14:02,638 --> 00:14:04,406
You smoke?
I'm just kidding.

368
00:14:04,440 --> 00:14:06,308
But, man,
that'd be cute if you did.

369
00:14:07,944 --> 00:14:09,778
What is wrong with me?

370
00:14:09,812 --> 00:14:12,447
It...

371
00:14:12,481 --> 00:14:15,350
Oh, Furguson.

372
00:14:15,384 --> 00:14:18,420
So, listen, I've been dreaming
about this fantasy, and I think

373
00:14:18,454 --> 00:14:20,822
this party might be
the perfect place to try it out.

374
00:14:20,856 --> 00:14:22,791
Okay, but I can't get wet.

375
00:14:22,825 --> 00:14:26,094
No water. So, listen, you and I
are gonna be sexy strangers.

376
00:14:26,128 --> 00:14:29,464
Yeah? Now, your stranger is
the early-to-bed type,

377
00:14:29,498 --> 00:14:31,866
and your job has you
on Hong Kong time.

378
00:14:31,901 --> 00:14:33,601
But the most important thing is,

379
00:14:33,636 --> 00:14:36,171
you're the first person
to leave the party.

380
00:14:36,205 --> 00:14:37,305
Whew. I like.

381
00:14:39,842 --> 00:14:41,376
- Wait, is that Cece?
- Is that who?

382
00:14:41,410 --> 00:14:42,544
Huh? No.

383
00:14:42,578 --> 00:14:45,714
- Cece. No.
- No, Schmidt, that's definitely Cece.

384
00:14:49,819 --> 00:14:52,387
Sorry, in between modeling jobs,
she does catering.

385
00:14:52,421 --> 00:14:54,489
You mind if I go over there
and say hello?

386
00:14:54,523 --> 00:14:55,557
I think it's only right.

387
00:14:55,591 --> 00:14:57,425
Eh.

388
00:14:59,662 --> 00:15:01,162
- Hey.
- Hey, I know you told me to stay

389
00:15:01,197 --> 00:15:03,064
in the office,
but Beth insisted that I...

390
00:15:03,099 --> 00:15:04,799
Stay away from that woman.
She is patient zero

391
00:15:04,834 --> 00:15:06,528
in this office
for spreading HPV.

392
00:15:06,563 --> 00:15:07,669
Here. Put on this apron.

393
00:15:07,703 --> 00:15:09,671
No, why-why would I do that?
That's crazy.

394
00:15:09,705 --> 00:15:12,040
My boss was asking, so I
told her you were a caterer.

395
00:15:12,074 --> 00:15:14,042
So... just... It's a good idea.

396
00:15:14,076 --> 00:15:16,344
- Okay.
- Just... Yes.

397
00:15:16,379 --> 00:15:17,979
You really want me here,
don't you?

398
00:15:18,014 --> 00:15:20,115
So badly.

399
00:15:21,751 --> 00:15:23,518
Can we just come back
when we have a plan?

400
00:15:23,552 --> 00:15:24,853
Are you afraid?

401
00:15:24,887 --> 00:15:26,254
No, I'm not afraid.

402
00:15:26,288 --> 00:15:27,689
Rose, give me a boost.

403
00:15:27,723 --> 00:15:29,224
Here's Rose.
I got you.

404
00:15:29,258 --> 00:15:31,493
Oh, my... Oh, my God.

405
00:15:31,527 --> 00:15:32,894
You got to help me a little.

406
00:15:32,928 --> 00:15:34,229
That's a hell of a grip, Rose.

407
00:15:35,264 --> 00:15:36,731
I see the Jacuzzi!

408
00:15:36,766 --> 00:15:38,433
Didn't stick the landing!

409
00:15:38,467 --> 00:15:39,601
You're doing great.

410
00:15:39,635 --> 00:15:42,570
I'm gonna do a loop
to not attract suspicion.

411
00:15:42,605 --> 00:15:45,106
- All right.
- All right.

412
00:15:46,108 --> 00:15:47,742
Schmidt. I'm right here.

413
00:15:47,777 --> 00:15:50,011
Oh. Do I know you?

414
00:15:50,046 --> 00:15:51,446
Right.

415
00:15:51,480 --> 00:15:52,714
The fantasy.

416
00:15:53,748 --> 00:15:56,116
Boy, I got to go.

417
00:15:56,619 --> 00:15:59,254
I have a 5:00 a.m.
conference call with...

418
00:15:59,288 --> 00:16:01,056
the Asians.

419
00:16:01,090 --> 00:16:03,458
Are you leaving soon, stranger?

420
00:16:03,492 --> 00:16:04,959
You mean "Corporal"?

421
00:16:04,994 --> 00:16:07,295
And, no, I'm gonna stick around
and throw down one more beer.

422
00:16:07,329 --> 00:16:09,964
Sweet.

423
00:16:09,999 --> 00:16:11,599
Oh, hey.

424
00:16:11,634 --> 00:16:12,967
Don't tell me you're leaving.

425
00:16:13,002 --> 00:16:14,369
Whew, great news.

426
00:16:14,403 --> 00:16:16,905
My boss is in her office
completely passed out.

427
00:16:16,939 --> 00:16:19,240
- We finally have the night to ourselves.
- Thank God.

428
00:16:19,275 --> 00:16:21,109
It's getting a little weird here,
right?

429
00:16:21,143 --> 00:16:23,278
Dressed up like this.

430
00:16:25,114 --> 00:16:27,182
That Elizabeth?

431
00:16:33,989 --> 00:16:37,659
Okay, look, Cece,
I-I got to tell you something.

432
00:16:37,693 --> 00:16:40,624
Does Elizabeth... work here?

433
00:16:40,963 --> 00:16:42,497
Yes.

434
00:16:42,531 --> 00:16:44,432
Hey.

435
00:16:44,467 --> 00:16:46,101
We were just talking about you.

436
00:16:46,135 --> 00:16:47,502
The food was good.

437
00:16:47,536 --> 00:16:48,837
Yeah.

438
00:16:48,871 --> 00:16:51,172
I was thinking of
taking some home.

439
00:16:51,207 --> 00:16:52,407
You deserve it.

440
00:16:52,441 --> 00:16:54,109
Okay, well, I'm heading off.

441
00:16:54,143 --> 00:16:57,545
The Asian markets
wait for no one.

442
00:17:00,449 --> 00:17:01,483
Wow.

443
00:17:01,517 --> 00:17:04,152
She was ice cold to you.

444
00:17:04,186 --> 00:17:06,754
I'm gonna go get a drink.

445
00:17:07,289 --> 00:17:09,290
What a night.

446
00:17:17,439 --> 00:17:18,983
- I got this!
- Open the damn gate!

447
00:17:19,058 --> 00:17:20,710
- Everything's under control!
- Jess, hey!

448
00:17:20,791 --> 00:17:21,891
Don't move! I'm coming over!
Rose, move!

449
00:17:21,926 --> 00:17:23,626
Jess, hey. I'm coming.

450
00:17:23,661 --> 00:17:25,862
- I got you.
- Whoa, hey, Rose!

451
00:17:25,896 --> 00:17:28,498
Ease up on the throttle.
Those are my precious goods.

452
00:17:28,532 --> 00:17:29,866
Did you follow me here?

453
00:17:29,900 --> 00:17:30,700
Here I come!

454
00:17:30,734 --> 00:17:33,303
- Nick.
- Hey.

455
00:17:33,337 --> 00:17:36,272
Hey, Jess. Jess, hey.
You okay?

456
00:17:36,307 --> 00:17:37,607
- Yeah.
- I'm sorry I tried

457
00:17:37,641 --> 00:17:39,976
to stick my nose in this.
I just...

458
00:17:40,010 --> 00:17:43,279
I just... I came here
because I'm your old man now.

459
00:17:43,314 --> 00:17:47,650
And if you're gonna do something
that's obviously very stupid...

460
00:17:47,685 --> 00:17:49,652
then I'm gonna do it with you.

461
00:17:55,326 --> 00:17:56,292
Oh.
Oh, my God.

462
00:17:56,327 --> 00:17:58,027
- You go there.
- Ow.

463
00:17:58,062 --> 00:18:00,029
- What's going on out here?
- Hey.

464
00:18:00,064 --> 00:18:02,332
- Who are you?
- No, I-I used to live here.

465
00:18:02,366 --> 00:18:04,000
I was just doing a visit
around the neighborhood.

466
00:18:04,034 --> 00:18:06,169
That's my old bedroom
right there.

467
00:18:06,203 --> 00:18:07,704
That's a bathroom.

468
00:18:07,738 --> 00:18:08,838
To you maybe it is.

469
00:18:08,873 --> 00:18:10,673
But to me and my four brothers,

470
00:18:10,708 --> 00:18:12,876
- that's a home.
- Nick.

471
00:18:12,910 --> 00:18:14,444
Nick, I can't let you do this.

472
00:18:14,478 --> 00:18:16,379
Mostly 'cause you're so bad at it.

473
00:18:16,413 --> 00:18:17,780
- Is that you, Ms. Day?
- Yes.

474
00:18:17,815 --> 00:18:19,616
It is. I'm so sorry.

475
00:18:19,650 --> 00:18:21,885
I trespassed on your property.

476
00:18:21,919 --> 00:18:26,956
And I would not be surprised
if you fired me.

477
00:18:26,991 --> 00:18:30,393
I think I know
what's really going on here.

478
00:18:30,427 --> 00:18:34,564
You heard about
my new... Jacooz.

479
00:18:36,267 --> 00:18:38,201
And you wanted a taste.

480
00:18:38,235 --> 00:18:40,069
♪ And say a little prayer
for right ♪

481
00:18:40,104 --> 00:18:43,139
♪ You know that if we are
to stay alive ♪

482
00:18:43,174 --> 00:18:46,576
Come on in, kids.
The water's hot!

483
00:18:46,610 --> 00:18:48,144
♪ Doo-doo-doo-doo-doo ♪

484
00:18:49,180 --> 00:18:50,480
♪ Doo-doo-doo... ♪

485
00:18:50,514 --> 00:18:52,615
Hey, babe.
Thanks for watching him.

486
00:18:52,650 --> 00:18:54,651
He can be a real pain
in the ass, huh?

487
00:18:54,685 --> 00:18:56,219
Yeah...

488
00:18:56,253 --> 00:18:57,987
Was there a dude
in your shower yesterday?

489
00:18:58,022 --> 00:19:00,256
Yes.

490
00:19:00,291 --> 00:19:01,624
Hmm.
How do you know him?

491
00:19:01,659 --> 00:19:02,959
From sex.

492
00:19:02,993 --> 00:19:05,595
But it was from sex before
you and I were exclusive.

493
00:19:05,629 --> 00:19:08,398
Have you had sex with him since?

494
00:19:08,432 --> 00:19:09,399
No.

495
00:19:09,433 --> 00:19:10,800
Oh, wait. Yes.

496
00:19:10,834 --> 00:19:12,168
Last night.

497
00:19:12,203 --> 00:19:15,605
Look, um...
I really liked you, Daisy.

498
00:19:15,639 --> 00:19:18,308
You know, I wanted this
to be a thing, but, uh...

499
00:19:18,743 --> 00:19:20,643
I deserve better.

500
00:19:22,313 --> 00:19:24,180
And so does Furguson.

501
00:19:24,215 --> 00:19:25,181
Hmm?

502
00:19:25,216 --> 00:19:26,883
I'm keeping the cat.

503
00:19:28,886 --> 00:19:30,486
I got your cat!

504
00:19:36,561 --> 00:19:39,362
Well, Schmidt, you win.

505
00:19:39,396 --> 00:19:41,965
They both really love you.

506
00:19:41,999 --> 00:19:44,834
So... enjoy your office.

507
00:19:48,205 --> 00:19:50,440
- Hey! Toilet Pants!
- Oh!

508
00:19:50,474 --> 00:19:52,675
Toilet Pants!

509
00:19:52,710 --> 00:19:54,677
Thanks for taking the bullet
for us the other night.

510
00:19:54,712 --> 00:19:57,780
How long were you stuck
in that Jacuzzi for?

511
00:19:57,815 --> 00:19:59,949
I don't really want
to talk about it, but, um,

512
00:19:59,984 --> 00:20:03,553
at one point, he did grab
my foot with his toes

513
00:20:03,587 --> 00:20:05,822
like it was a hand.

514
00:20:05,856 --> 00:20:06,823
What are you doing tonight?

515
00:20:06,858 --> 00:20:08,558
'Cause we got a liquid
form of Ecstasy,

516
00:20:09,093 --> 00:20:11,227
and we're gonna grade
papers together. What?

517
00:20:11,261 --> 00:20:12,295
I would love to,

518
00:20:12,329 --> 00:20:15,298
but I actually think
I'm gonna hang out...

519
00:20:15,332 --> 00:20:17,300
with my old man.

520
00:20:17,334 --> 00:20:19,168
That's... weird.

521
00:20:19,203 --> 00:20:20,370
Cool.

522
00:20:20,404 --> 00:20:22,672
- See you.
- Weirdo.

523
00:20:32,149 --> 00:20:34,083
Hi.

524
00:20:34,118 --> 00:20:35,885
So I hear you're interested
in joining the Sack Pack.

525
00:20:35,919 --> 00:20:37,687
I might be.

526
00:20:37,721 --> 00:20:39,355
Then perhaps this will
wet your whistle

527
00:20:39,390 --> 00:20:41,391
of what's fun
about being a member.

528
00:20:43,527 --> 00:20:45,328
- Just leave it.
- Yeah.

529
00:20:45,362 --> 00:20:47,897
You are terrible at Hacky Sack.

530
00:20:47,931 --> 00:20:49,332
I think I'm retiring.

531
00:20:55,139 --> 00:20:57,273
Yeah, I just want to say...

532
00:20:57,308 --> 00:20:59,208
I would've noticed you.

533
00:21:10,120 --> 00:21:11,521
Want to go do it
in the teachers' lounge?

534
00:21:11,555 --> 00:21:12,855
- Yes.
- You do?

535
00:21:12,890 --> 00:21:14,524
Oh, I'm gonna do it
with a teacher!

536
00:21:16,593 --> 00:21:19,162
Shh.

537
00:21:22,967 --> 00:21:25,736
Okay, something is
definitely coming out.

538
00:21:29,139 --> 00:21:30,506
I think we should go.

539
00:21:35,565 --> 00:21:41,457
Sync and corrected by <font color="#FF0000">Elderfel</font>
www.addic7ed.com

