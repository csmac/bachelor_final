1
00:00:02,207 --> 00:00:04,767
Testing, testing. One, two, three, testing.
Say hello.

2
00:00:04,847 --> 00:00:07,645
-Hello, hello.
-Hello, hello, hello.

3
00:00:14,247 --> 00:00:17,922
There are two magicians,
Harry Kane and Karl Allen,

4
00:00:18,007 --> 00:00:20,521
who were quite a successful
magician double-act.

5
00:00:21,087 --> 00:00:23,362
Oh, man, we rock! We rock so hard.

6
00:00:23,447 --> 00:00:24,880
And then they fell out

7
00:00:24,967 --> 00:00:27,606
because one of them was having an affair
with the other one's wife.

8
00:00:27,687 --> 00:00:28,756
Where's Karl?

9
00:00:29,327 --> 00:00:31,045
Oh, I don't know.

10
00:00:32,807 --> 00:00:34,843
What the... Karl!

11
00:00:34,927 --> 00:00:37,122
Okay, look, so maybe this has happened,

12
00:00:37,247 --> 00:00:40,319
but let's just, for the next hour,
pretend it hasn't happened.

13
00:00:40,407 --> 00:00:42,238
How would you feel about that?
Does that sound like a plan?

14
00:00:42,327 --> 00:00:44,602
When the betrayed magician finds this out,

15
00:00:44,687 --> 00:00:46,757
there's an awful catastrophe
that happens onstage.

16
00:00:46,847 --> 00:00:50,362
I accidentally cut my wife's head off
in a guillotine trick.

17
00:00:53,647 --> 00:00:54,682
Total accident.

18
00:00:54,807 --> 00:00:58,720
But Karl, played by Rob,
thinks it's deliberate.

19
00:00:58,807 --> 00:01:01,037
We join the action about four years later,

20
00:01:01,127 --> 00:01:04,517
when they've gone their separate ways
and they're both struggling slightly.

21
00:01:04,607 --> 00:01:05,676
What about in the shop?

22
00:01:05,767 --> 00:01:08,998
I could dem a few tricks.
I might be something of an attraction.

23
00:01:09,087 --> 00:01:10,805
Not in a good way, Harry.

24
00:01:10,887 --> 00:01:12,081
Our lives are a bit screwed.

25
00:01:12,167 --> 00:01:15,523
As a sort of last throw of the die,
we decide to get back together

26
00:01:15,607 --> 00:01:18,121
to try and win a magic competition,
win a bit of cash.

27
00:01:18,207 --> 00:01:19,435
It would be strictly business.

28
00:01:19,527 --> 00:01:21,722
In, win, split the money, goodbye.

29
00:01:21,807 --> 00:01:26,756
No bar chat, bonhomie,
or late-night laugh sessions.

30
00:01:27,407 --> 00:01:28,635
If you're interested,

31
00:01:28,727 --> 00:01:31,924
please state the word "interested"
at this point.

32
00:01:32,927 --> 00:01:33,962
Interested.

33
00:01:34,047 --> 00:01:35,799
Good. Then, I was thinking for the prelims,

34
00:01:35,887 --> 00:01:38,447
we could do the cut-down routine,
the 10-minuter.

35
00:01:38,527 --> 00:01:41,997
If you're on board please state,
"See you there, Harry,"

36
00:01:42,087 --> 00:01:43,600
and we will see each other there.

37
00:01:44,247 --> 00:01:45,646
"See you there, Harry."

38
00:01:45,927 --> 00:01:48,043
-Likewise. Goodbye.
-Goodbye.

39
00:01:48,127 --> 00:01:49,196
Likewise.

40
00:01:49,287 --> 00:01:52,199
Andrew O'Connor, the director,
came up with the idea

41
00:01:52,287 --> 00:01:54,926
and sort of worked it out
with a few magician mates.

42
00:01:55,007 --> 00:01:59,205
We wrote a treatment of that
over about eight months.

43
00:01:59,287 --> 00:02:02,518
We really liked the idea of doing a movie
about magicians.

44
00:02:02,607 --> 00:02:05,838
It seemed like a very original
and funny, interesting area

45
00:02:05,927 --> 00:02:07,201
that hadn't been done before.

46
00:02:07,287 --> 00:02:11,758
It was fairly clear that we wanted to go
with Robert and David as the stars.

47
00:02:11,847 --> 00:02:15,556
Even though the script wasn't
originally written for them,

48
00:02:15,647 --> 00:02:17,558
in a funny sort of way
I think it probably was,

49
00:02:17,647 --> 00:02:21,765
because their voices were very much in their
heads as they were writing the characters.

50
00:02:21,847 --> 00:02:25,442
Both of us were basically half expecting
Ben Stiller and Owen Wilson

51
00:02:25,527 --> 00:02:29,361
to step in at some point to replace us,
but they didn't do that.

52
00:02:30,287 --> 00:02:33,677
Fortunately, David and I are still
cheap enough to be in this film.

53
00:02:33,767 --> 00:02:37,157
No, that's not the truth.
That's not at all the truth.

54
00:02:37,247 --> 00:02:38,965
They write for my voice very well,

55
00:02:39,047 --> 00:02:42,357
which is great 'cause it means
I don't have to do much acting,

56
00:02:42,447 --> 00:02:44,642
or, you know,
put on a Geordie accent or anything.

57
00:02:44,727 --> 00:02:48,197
Sam and Jesse somehow manage to find
real bits of Robert and David

58
00:02:48,287 --> 00:02:50,005
and pull those into the characters.

59
00:02:50,087 --> 00:02:52,760
So, this is nice. You've got a nice flat.

60
00:02:52,927 --> 00:02:56,124
I bet you say that to all the men
whose flats you go round to.

61
00:02:56,767 --> 00:02:58,883
Not that you go round
to loads of men's flats.

62
00:02:58,967 --> 00:03:00,958
Rob and I have been working together,

63
00:03:01,047 --> 00:03:04,005
although it was initially just pissing around
when we were students,

64
00:03:04,087 --> 00:03:09,115
but what we now call "working together,"
for maybe 13 years now.

65
00:03:09,207 --> 00:03:12,005
Well, weeks go by where we basically
can't stand the sight of each other.

66
00:03:12,087 --> 00:03:14,396
-Yeah, it's quite boring.
-Yeah.

67
00:03:14,487 --> 00:03:17,524
Well, actually, the hate is kind of fun,
in a way. It keeps us sort of lively.

68
00:03:17,607 --> 00:03:18,642
Oh, yeah.

69
00:03:18,727 --> 00:03:21,924
David is just this personification
of Middle England, you know.

70
00:03:22,007 --> 00:03:25,443
If you wanted to remake Hancock,
I think David would be brilliant in that.

71
00:03:25,967 --> 00:03:27,958
And Robert's sort of cooler,

72
00:03:28,047 --> 00:03:30,959
and sort of, you know,
that slacker generation thing that he has.

73
00:03:31,047 --> 00:03:33,402
So they're very, very different,
both as people and as actors,

74
00:03:33,487 --> 00:03:37,241
but somehow they manage
to complement each other perfectly.

75
00:03:37,327 --> 00:03:38,806
It's quite a relief to hear you say that.

76
00:03:38,887 --> 00:03:42,323
It's very nice to be able to know so clearly

77
00:03:42,407 --> 00:03:44,557
the actors who're going to be
reading your lines,

78
00:03:44,647 --> 00:03:47,798
because then you can feel very comfortable
writing for them

79
00:03:47,887 --> 00:03:49,479
and then know
what was going to make them funny.

80
00:03:49,567 --> 00:03:51,558
Sometimes people can get a bit carried away

81
00:03:51,647 --> 00:03:53,877
with the difference
between film and TV comedy.

82
00:03:53,967 --> 00:03:56,481
In general, it seems pretty similar,
but you have to be aware

83
00:03:56,567 --> 00:04:00,640
that your face might be suddenly
the size of Mount Rushmore.

84
00:04:00,727 --> 00:04:02,922
You know, have to calm down the gurning.

85
00:04:03,007 --> 00:04:04,759
Otherwise, it's just going to hurt people.

86
00:04:04,847 --> 00:04:09,284
You'd better shut your mouth, Karl.
Just shut up and apologise.

87
00:04:09,367 --> 00:04:10,482
Shut up and apologise?

88
00:04:10,567 --> 00:04:14,685
Our characters' relationship is
that of two friends who have fallen out.

89
00:04:14,807 --> 00:04:18,925
And they worked together for years,
and so there's a lot of chemistry.

90
00:04:19,047 --> 00:04:21,800
Harry is a very repressed man.

91
00:04:21,887 --> 00:04:27,757
He's got deep feelings of unhappiness
and unfulfilled ambitions and guilt.

92
00:04:27,847 --> 00:04:31,760
A slightly autistic kind of
socially-incompetent,

93
00:04:31,847 --> 00:04:34,520
but very technically-able, geeky magician.

94
00:04:34,607 --> 00:04:35,642
And in the last five years

95
00:04:35,727 --> 00:04:38,082
have there been
any major changes to your circumstances?

96
00:04:38,167 --> 00:04:40,078
I am widowed. I'm a widower.

97
00:04:40,167 --> 00:04:43,398
And do you mind if I ask, for the form,
how she died?

98
00:04:43,487 --> 00:04:46,320
Right. She... She was...

99
00:04:47,087 --> 00:04:50,159
-You know.
-No. Sorry.

100
00:04:50,767 --> 00:04:54,077
I actually cut her head off with a guillotine.

101
00:04:54,167 --> 00:04:55,202
Shit!

102
00:04:55,327 --> 00:04:59,240
So I think he's got a lot of excuses
for being a little bit tentative with the world.

103
00:04:59,327 --> 00:05:01,238
A lot of double-acts,
they're very different people.

104
00:05:01,327 --> 00:05:03,716
Harry's much more technically minded,

105
00:05:04,207 --> 00:05:08,519
more of a guy who'd be more comfortable
in his workshop than onstage.

106
00:05:08,687 --> 00:05:12,236
And Karl's much more of a showman,
much more ambitious.

107
00:05:12,327 --> 00:05:13,999
You must admit, it is going well.

108
00:05:14,087 --> 00:05:18,444
Maybe. Maybe, so far,
the whole thing isn't a total fucking fiasco.

109
00:05:18,527 --> 00:05:21,519
I play Karl Allen, who used to be a magician,

110
00:05:21,607 --> 00:05:24,440
but since he split up
with David's character, Harry,

111
00:05:24,527 --> 00:05:26,961
has moved more into mentalism.

112
00:05:27,047 --> 00:05:28,719
I'm sorry, but I'm in the middle of a meeting.

113
00:05:28,807 --> 00:05:31,241
I would like you to dip into your brain
and think of a word.

114
00:05:31,327 --> 00:05:32,840
Would you both please leave immediately?

115
00:05:32,927 --> 00:05:34,326
A word, a simple word.

116
00:05:34,407 --> 00:05:36,125
Okay, dickhead.

117
00:05:36,207 --> 00:05:38,243
-My word is "dickhead".
-Perfect.

118
00:05:38,447 --> 00:05:41,803
There's a hierarchy that, you know,
people who can do very clever...

119
00:05:41,887 --> 00:05:45,596
Manipulating cards and deft things
with their hands, they're sort of at the top.

120
00:05:45,687 --> 00:05:48,406
And then mediums who pretend
to communicate with the dead

121
00:05:48,487 --> 00:05:49,840
are kind of near the bottom.

122
00:05:49,927 --> 00:05:52,487
Medium? You're going to do psychic?

123
00:05:52,567 --> 00:05:55,161
It's with a very modern twist.

124
00:05:55,247 --> 00:05:56,805
Oh, my God!

125
00:05:58,327 --> 00:05:59,442
Is...

126
00:06:00,087 --> 00:06:03,477
Is there a John in tonight?

127
00:06:04,567 --> 00:06:06,398
A Mr John something?

128
00:06:09,527 --> 00:06:11,279
Seriously, no Johns?

129
00:06:16,167 --> 00:06:21,878
All of the characters in the script are so
carefully written and carefully contrived

130
00:06:21,967 --> 00:06:25,642
that they needed to be cast with just
as much care and attention as the leads.

131
00:06:25,727 --> 00:06:28,639
Linda is the female lead.

132
00:06:28,727 --> 00:06:33,323
And she's the girl who has to fall in love
with David Mitchell's character, Harry.

133
00:06:33,407 --> 00:06:38,117
She's quite, sort of, up for it.
She's quite fun.

134
00:06:38,247 --> 00:06:41,557
Doesn't take life too seriously,
doesn't take herself too seriously,

135
00:06:41,647 --> 00:06:42,966
just wants to kind of have a bit of a laugh.

136
00:06:43,047 --> 00:06:45,607
Which is why she's interested
in becoming a magician's assistant.

137
00:06:45,687 --> 00:06:48,645
You see, I really, really want to do this.

138
00:06:48,927 --> 00:06:51,043
I brought a CD to audition. It's not magic.

139
00:06:51,127 --> 00:06:53,277
It's just a dance routine
me and Karen used to do

140
00:06:53,367 --> 00:06:54,402
in the stockroom.

141
00:06:54,487 --> 00:06:58,480
She just does a dance to Gay Bar,
which is kind of fun.

142
00:06:58,567 --> 00:07:02,003
So, if you just want to imagine
that there's two of us

143
00:07:02,567 --> 00:07:05,525
standing next to each other
just doing pretty much the same thing.

144
00:07:05,607 --> 00:07:07,723
That's my audition, basically,

145
00:07:07,807 --> 00:07:10,480
to try and persuade Harry
to let me be his magician's assistant,

146
00:07:10,567 --> 00:07:12,876
having had no experience.

147
00:07:26,527 --> 00:07:31,157
I meet Linda when I'm demonstrating
knives in a shop.

148
00:07:31,247 --> 00:07:32,885
Bloody hell! Are you all right?

149
00:07:32,967 --> 00:07:35,435
Of course, with a knife like this,

150
00:07:36,527 --> 00:07:38,802
you're never likely
to be depressed or lonely.

151
00:07:38,887 --> 00:07:40,366
I'm making a complaint.

152
00:07:40,447 --> 00:07:43,723
Darren Boyd, who's been making me laugh
for a very long time,

153
00:07:43,807 --> 00:07:45,718
and he plays Otto brilliantly.

154
00:07:45,887 --> 00:07:50,756
I am Mind Monger,
and I am about to enter your brain.

155
00:07:50,847 --> 00:07:52,644
I don't know. The beard just feels wrong.

156
00:07:52,727 --> 00:07:54,319
Then let's lose the beard, man.

157
00:07:54,407 --> 00:07:56,557
You're not married to the beard.

158
00:07:56,807 --> 00:07:59,640
As your agent, that's why I'm here,
help you make these hard choices.

159
00:07:59,727 --> 00:08:04,801
He has an unbending enthusiasm
and a genuine passion, shall we say,

160
00:08:04,887 --> 00:08:08,675
for his involvement with Karl.

161
00:08:08,767 --> 00:08:10,883
-Harry?
-Oh, hi, Tony.

162
00:08:10,967 --> 00:08:12,923
What the hell? Are you off to the Shield?

163
00:08:13,007 --> 00:08:14,679
Why are you taking a ferry, you big loser?

164
00:08:14,767 --> 00:08:19,716
Steve Edge plays Tony White,
the sort of lecherous comedian.

165
00:08:20,087 --> 00:08:22,760
He's a card magician
and just spends his time

166
00:08:22,847 --> 00:08:25,566
travelling the country, trying to
sleep with as many women as he can.

167
00:08:25,647 --> 00:08:29,196
I'm Tony White.
Don't believe the rumours. They're all true.

168
00:08:29,487 --> 00:08:30,920
Except for the duck thing.

169
00:08:31,007 --> 00:08:34,761
I mean, do I actually look
like the sort of bloke who'd suck off a duck?

170
00:08:36,767 --> 00:08:38,997
The marvellous Peter Capaldi
is Mike Francis,

171
00:08:39,087 --> 00:08:42,397
the, sort of, judge of the magic competition.

172
00:08:45,247 --> 00:08:46,362
This is sick.

173
00:08:47,607 --> 00:08:49,723
This is so sick
it might actually make the papers.

174
00:08:49,807 --> 00:08:51,877
He likes to maintain the standards

175
00:08:52,567 --> 00:08:55,843
that he's come to expect from magicians
the world over.

176
00:08:55,927 --> 00:08:59,237
He's been terrific.
Had his hair sort of bouffed up, and...

177
00:09:01,767 --> 00:09:05,282
Obviously, that's not his whole performance
but, you know, it just helps.

178
00:09:05,407 --> 00:09:08,524
He's great. He's very, very funny.
He's very, very right wing,

179
00:09:10,247 --> 00:09:11,839
which is great fun to play.

180
00:09:12,287 --> 00:09:14,847
Thank you.
Tony White showing you a new flower trick

181
00:09:14,927 --> 00:09:17,441
and also his distinctive ring piece.

182
00:09:18,047 --> 00:09:20,607
Don't worry, right over the kids' heads.

183
00:09:20,687 --> 00:09:21,722
Anyhow...

184
00:09:21,807 --> 00:09:25,243
And Andrea Risborough, playing Dani,
who's very funny

185
00:09:25,447 --> 00:09:30,646
as the slightly credulous,
Geordie girlfriend of Karl's.

186
00:09:30,887 --> 00:09:35,324
Dani is a really sweet and trusting person.

187
00:09:35,407 --> 00:09:36,476
She's not stupid,

188
00:09:37,167 --> 00:09:38,680
but she can be fairly naive.

189
00:09:38,767 --> 00:09:40,086
So, what would you like?

190
00:09:40,487 --> 00:09:43,047
Come on. You know.

191
00:09:44,767 --> 00:09:49,397
A pint for me, please,
and a white wine for the lady.

192
00:09:49,487 --> 00:09:52,081
Amazing. White wine. Wow.
Yeah, you got me.

193
00:09:52,527 --> 00:09:54,677
That is exactly what I drink.

194
00:09:54,767 --> 00:09:57,565
White wine, or red wine.

195
00:09:58,247 --> 00:09:59,396
Or a lager.

196
00:09:59,967 --> 00:10:04,802
Andrew, which is unusual,
is an incredibly collaborative,

197
00:10:04,887 --> 00:10:07,765
easy-going, intelligent director.

198
00:10:07,967 --> 00:10:10,686
It's very interesting when you work with,
essentially, first-time directors.

199
00:10:10,767 --> 00:10:12,962
They're enthusiastic
about what you're achieving,

200
00:10:13,047 --> 00:10:15,197
they're incredibly thankful
for what you're trying to do.

201
00:10:15,287 --> 00:10:17,243
I'm much more involved with things.

202
00:10:17,327 --> 00:10:19,636
Rather than somebody saying to me,
"This is what I'd like to do,"

203
00:10:19,727 --> 00:10:22,366
it's much more like, "Hey, James,
how do we achieve this?"

204
00:10:22,447 --> 00:10:25,325
He knows what he wants
and he knows when he's got it.

205
00:10:25,447 --> 00:10:28,757
And he's very good at conveying
how to give it to him.

206
00:10:28,887 --> 00:10:29,922
-And cut.
-Not bad.

207
00:10:30,007 --> 00:10:31,235
We're going to do one last take.

208
00:10:31,327 --> 00:10:34,285
He's just very good at talking to actors,
it turns out.

209
00:10:34,367 --> 00:10:36,801
He gives you very detailed notes.

210
00:10:36,887 --> 00:10:40,084
He is more or less at one
with the funny bit in every scene

211
00:10:40,167 --> 00:10:42,078
and how to bring that out of you.

212
00:10:42,167 --> 00:10:43,964
He also loves the gags.

213
00:10:47,567 --> 00:10:51,196
Andrew comes from a magic background.

214
00:10:51,287 --> 00:10:53,721
Was, as I'm sure he'll tell you
in his interview,

215
00:10:53,807 --> 00:10:55,798
Young Magician of the Year when he was 18.

216
00:10:55,887 --> 00:10:58,879
Have I mentioned that I was
the Young Magician of the Year at 18?

217
00:10:58,967 --> 00:11:02,801
Andrew is a walking magic encyclopaedia.

218
00:11:02,887 --> 00:11:07,085
From the ages of 14 to 22,
I earned a living as a professional magician,

219
00:11:07,167 --> 00:11:10,921
doing sort of working men's clubs
and theatres and tours and hotels.

220
00:11:11,007 --> 00:11:12,679
And I was sort of cocky and, you know,

221
00:11:12,767 --> 00:11:14,439
got people up
and took the mickey out of them.

222
00:11:14,527 --> 00:11:16,722
And I even spoke with a Northern accent,
and I wasn't even Northern.

223
00:11:16,807 --> 00:11:17,956
It was mental, you know.

224
00:11:18,047 --> 00:11:20,959
"Take a card, put it back. Is it the six
of clubs? Thank you very much."

225
00:11:21,047 --> 00:11:22,036
It was ridiculous.

226
00:11:22,127 --> 00:11:26,166
The year I won it, Noel was second.
That man there's called Scott Penrose.

227
00:11:26,247 --> 00:11:28,681
Scott is our magic consultant on our movie.

228
00:11:28,767 --> 00:11:31,281
We managed to make the film authentic.

229
00:11:31,527 --> 00:11:35,725
Quite simply, we have a fantastic magic
consultant on the film called Scott Penrose,

230
00:11:35,807 --> 00:11:39,720
who is there to make sure
that everything we do is good and correct.

231
00:11:39,807 --> 00:11:42,037
So, every trick that you see in the film,

232
00:11:42,127 --> 00:11:46,564
Scott has either built, designed,
looked after, taught magicians.

233
00:11:46,647 --> 00:11:50,765
So if the magic isn't very good in the film,
it's absolutely Scott's fault.

234
00:11:52,007 --> 00:11:53,645
This looks amazing!

235
00:11:53,727 --> 00:11:55,683
The tricks that you will see
in the Magicians movie...

236
00:11:55,767 --> 00:11:57,485
A lot of classic tricks.

237
00:11:57,567 --> 00:12:00,559
The guillotine illusion, where a blade
passes through someone's neck.

238
00:12:00,647 --> 00:12:02,205
Or not, as the case may be.

239
00:12:02,287 --> 00:12:03,686
We're going with the guillotine, Harry,

240
00:12:03,767 --> 00:12:05,883
because I want to win, yeah?

241
00:12:05,967 --> 00:12:07,082
I'm not messing about.

242
00:12:07,167 --> 00:12:08,600
A lot of smaller magic as well.

243
00:12:08,687 --> 00:12:12,362
As I said, David is doing the multiplying
billiard balls,

244
00:12:12,447 --> 00:12:13,436
which is a classic trick.

245
00:12:13,527 --> 00:12:15,836
I learnt how to make them turn
into three and then four.

246
00:12:15,927 --> 00:12:19,476
But that very simple,
not very spectacular thing,

247
00:12:19,567 --> 00:12:22,206
the joke of which is that it looks a bit shit,

248
00:12:22,287 --> 00:12:26,519
actually took more practice than all
the levitation, guillotining,

249
00:12:26,607 --> 00:12:28,643
sub trunk stuff all put together.

250
00:12:28,727 --> 00:12:31,161
You'll see some dove productions,
linking rings.

251
00:12:31,247 --> 00:12:33,283
You'll see some card manipulations.

252
00:12:33,447 --> 00:12:36,439
You'll see Jessica levitated.

253
00:12:36,527 --> 00:12:39,405
It feels very uplifting.

254
00:12:41,127 --> 00:12:42,242
Yeah.

255
00:12:42,967 --> 00:12:45,435
Actually, it's a bit tight
round my bum, Harry.

256
00:12:45,527 --> 00:12:47,483
Could you just get a finger in there
and give it a waggle?

257
00:12:47,567 --> 00:12:49,159
Under the strap, obviously.

258
00:12:49,767 --> 00:12:50,802
Not in my bum!

259
00:12:51,967 --> 00:12:55,277
Sure, yes, I'll just get my hands in there,
under there.

260
00:12:55,407 --> 00:12:59,195
The guillotine illusion is quite a scary prop.

261
00:12:59,487 --> 00:13:02,126
It's designed not to cut anybody's head off,

262
00:13:02,207 --> 00:13:04,767
although, at the same time,
it will cut things as well.

263
00:13:04,847 --> 00:13:06,724
Well, as you know,
the actors have learnt some tricks.

264
00:13:06,807 --> 00:13:08,684
The movie's peppered with some
real magicians.

265
00:13:08,767 --> 00:13:10,883
There's a character in the movie
called Wolfgang

266
00:13:10,967 --> 00:13:13,117
who puts out a cigarette on his tongue.

267
00:13:13,207 --> 00:13:15,482
And that is played by a guy called Pat Page.

268
00:13:15,567 --> 00:13:17,956
He's 74, one of
the world's greatest magicians.

269
00:13:18,047 --> 00:13:21,244
And Pat sold me my first
magic tricks when I was 13.

270
00:13:21,327 --> 00:13:24,956
He came into a magic shop I was working in
at the time and bought some tricks,

271
00:13:25,047 --> 00:13:26,765
and I've known him ever since.

272
00:13:26,847 --> 00:13:30,203
He gives me the odd job occasionally
for old time's sake, you know.

273
00:13:30,527 --> 00:13:31,642
Nice work, Wolfgang.

274
00:13:31,727 --> 00:13:35,322
It's the kind of trick
that no one would ever dare do today.

275
00:13:36,007 --> 00:13:39,761
There's no method to that, apparently.
He just takes the pain.

276
00:13:39,847 --> 00:13:41,075
Sick bastard.

277
00:13:41,167 --> 00:13:43,317
People don't know that there are
things called magic conventions

278
00:13:43,407 --> 00:13:45,204
where you can go and buy tricks.

279
00:13:45,287 --> 00:13:47,881
So we set up a magic convention
in the movie,

280
00:13:47,967 --> 00:13:50,800
and we invited real magic dealers
from around the country

281
00:13:50,887 --> 00:13:54,118
to come and set up their real stands
and demonstrate their real tricks.

282
00:13:54,207 --> 00:13:56,277
One, one, six, take two.

283
00:13:57,807 --> 00:14:00,640
And background action.

284
00:14:01,087 --> 00:14:02,076
Action!

285
00:14:02,167 --> 00:14:06,365
Linda, Jessica Stevenson's character,
is strolling around the convention,

286
00:14:06,447 --> 00:14:09,996
checking out some tricks.
She finds the fake turd.

287
00:14:10,087 --> 00:14:14,956
Genuine fake turd based on
the 1947 Ted Krueger original design.

288
00:14:15,047 --> 00:14:16,685
It's a classic.

289
00:14:17,367 --> 00:14:18,561
Is it magic?

290
00:14:18,647 --> 00:14:21,320
It's on the borderline,
where magic meets novelty.

291
00:14:21,407 --> 00:14:24,604
This whole world,
this whole magician's world,

292
00:14:24,727 --> 00:14:29,198
of not-the-best-taste kind of presentation
and clothes and all this sort of stuff,

293
00:14:29,287 --> 00:14:31,801
I thought was just a great world to explore.

294
00:14:31,887 --> 00:14:36,278
And also it has this slightly geeky
kind of element to it.

295
00:14:36,367 --> 00:14:39,006
I just thought that
they caught that perfectly in the script.

296
00:14:39,087 --> 00:14:43,285
It's great to see the very strange
and secluded world

297
00:14:43,367 --> 00:14:47,724
of the magicians and the amateur magicians
doing their, frankly, terrible tricks.

298
00:14:47,807 --> 00:14:51,004
Quick shake. Turns white.

299
00:14:53,567 --> 00:14:57,526
It is a real community of people
who are obsessed with one thing.

300
00:14:57,607 --> 00:14:59,325
And they come apart

301
00:14:59,807 --> 00:15:00,876
as if by magic.

302
00:15:00,967 --> 00:15:03,561
The thing that struck me
particularly down there

303
00:15:03,647 --> 00:15:06,286
was that magic, in some ways,
is show business

304
00:15:06,367 --> 00:15:07,880
for people without any charisma.

305
00:15:07,967 --> 00:15:10,162
You know, these are people
with no natural talent,

306
00:15:10,247 --> 00:15:12,636
but they've found a way to be onstage.

307
00:15:12,727 --> 00:15:14,957
I didn't realise the Magic Circle
is actually a place.

308
00:15:15,047 --> 00:15:18,198
They go down... They've got a club there,
and they go down to the bar,

309
00:15:18,287 --> 00:15:20,323
and they talk to each other
about magic tricks.

310
00:15:20,407 --> 00:15:23,604
-It's not like Stonehenge.
-No, it's more of a pub.

311
00:15:23,687 --> 00:15:27,362
Stonehenge, some people believe,
is an actual magic circle.

312
00:15:27,807 --> 00:15:29,638
Those people are morons.

313
00:15:30,447 --> 00:15:32,403
One, two, three, four.

314
00:15:35,527 --> 00:15:39,156
We've gone as far as we possibly can
with two comedy actors

315
00:15:39,247 --> 00:15:40,760
that aren't magicians.

316
00:15:40,847 --> 00:15:44,920
So we have spent a couple of months
training them to do magic,

317
00:15:45,127 --> 00:15:47,641
and they've worked out surprisingly well.

318
00:15:47,727 --> 00:15:51,003
Really, the essence is just getting
the persona as magicians

319
00:15:51,087 --> 00:15:53,760
rather than just the successful execution
of the trick.

320
00:15:53,847 --> 00:15:58,045
The real challenge was
to get them into the mindset of a magician

321
00:15:58,127 --> 00:15:59,355
and acting like them.

322
00:15:59,447 --> 00:16:01,756
It's very funny watching everybody
learning how to do the magic

323
00:16:01,847 --> 00:16:03,485
because they all look vaguely nervous.

324
00:16:03,567 --> 00:16:06,445
I mean, it's not exactly like Keanu Reeves,

325
00:16:06,527 --> 00:16:10,998
spending three months to learn karate,
or kung-fu, or sword fighting.

326
00:16:11,087 --> 00:16:13,362
But it... Yeah, it is a bit like that.

327
00:16:13,447 --> 00:16:18,646
The trick I have is the Flowers of Love,
which is a vanishing bouquet of flowers.

328
00:16:18,727 --> 00:16:20,479
The Flowers of Love.

329
00:16:20,567 --> 00:16:26,517
And much like human love, one minute
it's there, but by morning, 'tis gone.

330
00:16:26,807 --> 00:16:30,004
David was given much easier magic tricks
to learn than I was,

331
00:16:30,087 --> 00:16:32,726
and so it looks like he picked it up quicker.

332
00:16:32,807 --> 00:16:36,277
I had to learn the
rolling-a-coin-across-your-hand thing.

333
00:16:36,367 --> 00:16:38,756
Bend the knuckle, that's a tip,
that's the first tip.

334
00:16:38,847 --> 00:16:41,315
And then I spent four weeks practising this.

335
00:16:41,407 --> 00:16:45,559
And the idea is to get the coin to roll
gracefully across your knuckles,

336
00:16:45,647 --> 00:16:49,765
pass it under and then transfer it,
and it goes round in an even circle.

337
00:16:49,847 --> 00:16:52,964
After four weeks, this is how well I can do it.

338
00:16:57,087 --> 00:17:00,204
The evidence is
we were both given some tricks,

339
00:17:00,287 --> 00:17:04,360
I mastered the ones I was given,
Rob didn't master the ones he was given.

340
00:17:04,447 --> 00:17:05,960
-All of them.
-I honestly gave up.

341
00:17:06,047 --> 00:17:07,844
The people who can do it are freaks.

342
00:17:07,927 --> 00:17:10,077
And then it turns out that most magicians
are only good at it

343
00:17:10,167 --> 00:17:12,556
because they practise for hours and years.

344
00:17:12,647 --> 00:17:16,003
-I mean, where's the fun in that?
-Yeah, Gandalf doesn't practise.

345
00:17:16,087 --> 00:17:17,566
Oh, my God! You are kidding!

346
00:17:18,087 --> 00:17:20,362
Well, we're here in beautiful Skegness,

347
00:17:20,447 --> 00:17:23,757
looking at the pier,
which stops as soon as it gets to the water.

348
00:17:23,847 --> 00:17:28,477
We got to film in the fantastic
Nottingham Theatre Royal.

349
00:17:28,567 --> 00:17:33,277
Gorgeous theatre.
We did all of the final magic show there.

350
00:17:33,367 --> 00:17:35,244
The theatre world in Nottingham,
where we are today,

351
00:17:35,327 --> 00:17:37,887
is amazing and looks brilliant on film.

352
00:17:37,967 --> 00:17:41,164
We got a whole audience of local people

353
00:17:41,247 --> 00:17:45,001
to come in and be the audience,
and they were very well-behaved.

354
00:17:45,087 --> 00:17:46,884
I started to get quite badly heckled.

355
00:17:46,967 --> 00:17:48,878
I went out and started chatting to them all.

356
00:17:48,967 --> 00:17:51,720
Nobody else did.
I was the only one who fell for it.

357
00:17:51,807 --> 00:17:54,275
Poor bastards had to stay there
for about eight hours,

358
00:17:54,367 --> 00:17:57,279
clapping and doing various things
they were ordered to do.

359
00:17:57,367 --> 00:17:58,880
It became quite addictive actually,
after awhile.

360
00:17:58,967 --> 00:18:00,958
It was really quite fun chatting to everyone.

361
00:18:01,047 --> 00:18:03,845
And I thought I saw Johnny Depp
in the audience, but it wasn't him.

362
00:18:05,327 --> 00:18:06,806
It wasn't him.

363
00:18:08,047 --> 00:18:12,723
We are on a lovely beach called
Queens Park Beach in Mablethorpe,

364
00:18:12,807 --> 00:18:14,718
on the east coast of England.

365
00:18:14,807 --> 00:18:18,163
Most of the scenes on the beach were fun.
It's a nice working environment.

366
00:18:18,247 --> 00:18:21,364
Who wouldn't want to work
on a beach most of the time?

367
00:18:21,447 --> 00:18:23,961
I suppose that's the attraction
of being in Baywatch.

368
00:18:24,047 --> 00:18:25,719
I'd have to ask the Hoff.

369
00:18:25,967 --> 00:18:31,439
I love the scenes on the beach,
where Harry tries to piss on Karl's head.

370
00:18:32,047 --> 00:18:33,162
No, I don't like that.

371
00:18:33,247 --> 00:18:36,717
He's buried up to his neck in sand,
on the beach, for some sort of trick.

372
00:18:36,807 --> 00:18:38,877
He's carrying out a Blaine-style stunt.

373
00:18:38,967 --> 00:18:42,323
The Sandathon, or Sandurance,
as I think he calls it.

374
00:18:42,407 --> 00:18:43,601
Here's the pitch,

375
00:18:43,687 --> 00:18:45,564
you don't need food.

376
00:18:45,647 --> 00:18:47,956
You are living off your brain.

377
00:18:48,047 --> 00:18:50,356
The first thing
that occurred to me was wasps.

378
00:18:50,447 --> 00:18:54,565
And that if I was buried up to my head,
there would be wasps around.

379
00:18:54,647 --> 00:18:55,796
That was my first thinking.

380
00:18:55,887 --> 00:18:58,355
"I hope they're going to have
some expert swatters."

381
00:18:58,447 --> 00:19:02,918
It turned out to be okay.
I was actually in a wooden crate on a stool.

382
00:19:03,327 --> 00:19:04,362
So my head was poking up,

383
00:19:04,447 --> 00:19:07,564
but I was actually sitting down
fairly comfortably.

384
00:19:07,647 --> 00:19:09,478
But it wasn't very nice to
be in there for very long.

385
00:19:09,567 --> 00:19:10,556
'Cause the wind gets up,

386
00:19:10,647 --> 00:19:13,081
and then you got sand
whipping in your face.

387
00:19:13,167 --> 00:19:15,044
And then the sun's boiling you.

388
00:19:15,127 --> 00:19:17,357
Harry, David Mitchell's character,

389
00:19:17,447 --> 00:19:20,007
is furious with him,
and comes storming down to the beach.

390
00:19:20,087 --> 00:19:22,601
Dirty tricks! You had to resort to dirty tricks.

391
00:19:22,687 --> 00:19:23,676
Harry?

392
00:19:23,847 --> 00:19:27,601
I come, and shout, and scream at him
and threaten to piss on his head.

393
00:19:27,687 --> 00:19:31,885
Urine. How's that?
Lovely warm urine, all over your head.

394
00:19:31,967 --> 00:19:34,117
And I didn't even have to get
my real knob out.

395
00:19:34,207 --> 00:19:35,322
He did get a fake knob out.

396
00:19:35,407 --> 00:19:38,001
-A huge, stunt, Hollywood knob.
-Just to try and put me off.

397
00:19:38,087 --> 00:19:40,157
-'Cause obviously my own penis...
-It didn't work.

398
00:19:40,247 --> 00:19:42,715
-It's too funny. It's too funny.
-...it's not very filmic.

399
00:19:42,807 --> 00:19:45,116
Harry, look, don't. Just let's talk about this.

400
00:19:46,247 --> 00:19:48,158
Damn! I can't go now!

401
00:19:48,567 --> 00:19:52,082
I think that's a richly comic moment
and I enjoyed playing that.

402
00:19:52,167 --> 00:19:54,442
Well, it's funny. Just a funny British film.

403
00:19:54,527 --> 00:19:57,837
The little details and nuances
are deeply embedded within the film.

404
00:19:57,927 --> 00:20:00,441
I think what you have
in the two central characters is...

405
00:20:00,527 --> 00:20:03,963
Are two characters
that you can really invest in.

406
00:20:04,047 --> 00:20:05,036
Hey, Harry!

407
00:20:05,127 --> 00:20:09,120
It's a really warm tale, you know,
and it has a great, filmic story.

408
00:20:09,207 --> 00:20:11,084
Everyone's really, kind of, funny.

409
00:20:11,167 --> 00:20:12,156
Thank you.

410
00:20:12,247 --> 00:20:15,045
There's lots of very, very good
comic turns in it.

411
00:20:15,127 --> 00:20:17,197
It's just that it's a really good night out.

412
00:20:17,287 --> 00:20:20,802
And that maybe it'll encourage
you to learn a card trick.

413
00:20:20,887 --> 00:20:22,206
What are you talking about?

414
00:20:23,487 --> 00:20:25,398
That was a joke.
You don't really want to learn card tricks.

415
00:20:25,487 --> 00:20:29,196
It's a story that makes sense,
and you are engaged by the characters,

416
00:20:29,287 --> 00:20:32,165
having to laugh with and at them.

417
00:20:32,247 --> 00:20:35,125
Lot of laughs, as well as drama.

418
00:20:35,807 --> 00:20:37,081
And, cut!

419
00:20:37,567 --> 00:20:38,556
Blimey.

420
00:20:40,247 --> 00:20:44,160
Well, I don't think that anyone
has ever seen a final quite like that,

421
00:20:44,247 --> 00:20:47,796
and I do not, in any way,
mean that in a positive sense.

