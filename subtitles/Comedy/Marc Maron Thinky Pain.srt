﻿1
00:00:04,076 --> 00:00:05,359
Okay.

2
00:00:09,332 --> 00:00:12,166
God damn it! Just load!

3
00:00:20,047 --> 00:00:21,289
What the...?

4
00:00:23,262 --> 00:00:25,429
Son of a bitch.

5
00:00:29,552 --> 00:00:32,970
♪ Won't fall for it ♪

6
00:00:33,055 --> 00:00:35,272
♪ You can't see ♪

7
00:00:35,308 --> 00:00:37,691
♪ And you can't tell ♪

8
00:00:37,727 --> 00:00:42,529
♪ I just can't drink
from the poisoned well ♪

9
00:00:42,564 --> 00:00:44,064
Sync and corrections by explosiveskull
www.addic7ed.com

10
00:00:44,191 --> 00:00:45,940
<i>You have reached T-West.</i>

11
00:00:45,942 --> 00:00:49,139
<i>Press 1 for Spanish,
3 for English.</i>

12
00:00:49,939 --> 00:00:53,157
<i>Press 1 for phone.
Press 2 for Internet.</i>

13
00:00:53,209 --> 00:00:54,742
<i>Press ...</i>

14
00:01:01,167 --> 00:01:02,666
- Hey, man.
- Hey, Bernie.

15
00:01:02,718 --> 00:01:04,585
Yeah, you got a tire.

16
00:01:04,637 --> 00:01:06,387
Yeah, yeah.

17
00:01:06,422 --> 00:01:08,639
<i>Press 1 for billing.
Press 2 for customer service.</i>

18
00:01:08,674 --> 00:01:10,341
<i>Press 3 for technical help.
Press...</i>

19
00:01:11,560 --> 00:01:13,510
Hey, man,
you're being watched.

20
00:01:14,930 --> 00:01:16,981
Oh, God damn it.

21
00:01:17,016 --> 00:01:19,817
When the hell
did he get here?

22
00:01:19,852 --> 00:01:23,520
Uh, 4:15 A.M.,
7:15 Eastern.

23
00:01:23,572 --> 00:01:25,906
We've actually been
watching each other.

24
00:01:27,243 --> 00:01:28,742
We have an understanding.

25
00:01:28,778 --> 00:01:30,995
All right, all right.
Well, that's... that's my dad.

26
00:01:31,030 --> 00:01:33,497
Huh.
You have a dad.

27
00:01:33,532 --> 00:01:34,999
That's nice.

28
00:01:35,034 --> 00:01:38,168
Long story short,
<i>I</i> had one.

29
00:01:39,038 --> 00:01:42,122
Yeah? Well, that's mine,
and that's his house.

30
00:01:42,174 --> 00:01:44,842
<i>Press 1 for hardware issues.
Press 2 for connection issues.</i>

31
00:01:45,928 --> 00:01:47,261
You need a tire?

32
00:01:48,130 --> 00:01:49,513
No, I'm good on tires.

33
00:01:49,548 --> 00:01:52,049
I'm using
the barter system.

34
00:01:52,101 --> 00:01:54,718
All right. Okay. Well,
I'm still on the money system.

35
00:01:55,971 --> 00:01:57,388
- <i>Press 1 for ...</i>
- Come... come on!

36
00:01:59,141 --> 00:02:01,525
I-I just... I don't have time to
deal with this shit right now.

37
00:02:01,560 --> 00:02:03,694
I don't <i>want</i> to deal with it.
He's just sitting out there.

38
00:02:03,729 --> 00:02:05,979
He wants me to go out there
and see what's going on.

39
00:02:06,032 --> 00:02:08,449
Classic
bullshit power play.

40
00:02:08,484 --> 00:02:11,869
Yeah. Or maybe he just pulled up
and had a heart attack.

41
00:02:11,904 --> 00:02:13,954
'Cause that's
how <i>my</i> dad died.

42
00:02:15,374 --> 00:02:17,574
But that was on a boat.

43
00:02:17,626 --> 00:02:18,625
Short story long.

44
00:02:18,661 --> 00:02:20,411
<i>Press 1 for...</i>

45
00:02:20,463 --> 00:02:22,046
C-Come on, come on, come on!

46
00:02:22,081 --> 00:02:24,214
I need to talk to a person!
A person!

47
00:02:24,250 --> 00:02:26,216
A person, God damn it!

48
00:02:26,252 --> 00:02:27,918
Ah. You're ordering
a pizza.

49
00:02:27,970 --> 00:02:31,088
What? No, I'm on the phone with
my bullshit Internet company.

50
00:02:31,140 --> 00:02:35,392
Man, you got to steer clear
of that shit!

51
00:02:35,428 --> 00:02:38,062
You know, like, they watch you.
They hunt you.

52
00:02:38,097 --> 00:02:39,930
That's why
I carry an antenna.

53
00:02:39,982 --> 00:02:43,317
They can't track your stuff
through an antenna.

54
00:02:45,020 --> 00:02:47,020
Okay. All right.
Well, I need Wi-Fi.

55
00:02:47,073 --> 00:02:48,572
Thanks for waiting.
How can I help you?

56
00:02:48,607 --> 00:02:50,357
Yes. Hello? Hi.

57
00:02:50,409 --> 00:02:53,110
Yes, my Internet's been
going on and off for two days.

58
00:02:53,162 --> 00:02:54,828
I'd like
to get it fixed now.

59
00:02:54,864 --> 00:02:57,247
I'm very sorry, sir. Please hold
while I check your signal.

60
00:02:57,283 --> 00:02:58,365
A sconce.

61
00:02:59,785 --> 00:03:01,585
I could really use
a sconce.

62
00:03:01,620 --> 00:03:02,953
No, Bernie.

63
00:03:03,005 --> 00:03:05,539
I-I-I don't want a tire,
and I like my sconce.

64
00:03:05,591 --> 00:03:07,341
I-I-I-I'll sweeten
the deal.

65
00:03:07,376 --> 00:03:09,259
I'll throw in
a bunch of bricks

66
00:03:09,295 --> 00:03:11,545
and a piece of a bird bath...
beautiful.

67
00:03:11,597 --> 00:03:12,629
Oh, here we go.

68
00:03:15,384 --> 00:03:18,385
Mr. Maron, I'm going to need you
to restart your modem.

69
00:03:18,437 --> 00:03:20,721
Yeah, okay.
I can do that.

70
00:03:26,445 --> 00:03:28,112
Where...
where's my son?

71
00:03:28,147 --> 00:03:29,530
Oh, he's inside,

72
00:03:29,565 --> 00:03:32,232
giving the feds access
to all his information.

73
00:03:32,284 --> 00:03:33,984
I just want
to say goodbye to my son.

74
00:03:34,036 --> 00:03:35,652
Oh, what does that
even mean?

75
00:03:37,206 --> 00:03:39,907
And I always wanted you
to have this.

76
00:03:39,959 --> 00:03:41,458
That looks new.

77
00:03:43,045 --> 00:03:45,212
Yeah, well, I got it
a couple weeks ago.

78
00:03:45,247 --> 00:03:47,164
And it means a lot.

79
00:03:47,216 --> 00:03:48,882
That's bullshit.

80
00:03:48,918 --> 00:03:50,000
Excuse me?

81
00:03:50,052 --> 00:03:51,585
No, no.
Not... not... not you.

82
00:03:51,637 --> 00:03:55,005
I have a giant, sad,
annoying baby on my lawn.

83
00:03:55,057 --> 00:03:56,807
We can send out
a repairman tomorrow

84
00:03:56,842 --> 00:03:59,092
between noon and 4:00 P.M.

85
00:03:59,145 --> 00:04:00,811
Okay, great.

86
00:04:00,846 --> 00:04:02,146
What are you doing?

87
00:04:02,181 --> 00:04:04,565
You're just gonna
drop that and leave?!

88
00:04:04,600 --> 00:04:07,818
Sir, can you write down
the 10-digit repair code?

89
00:04:07,853 --> 00:04:11,772
Yeah, yeah.
Just give me the number.

90
00:04:11,824 --> 00:04:16,695
4-7-4-4-3-2-8-9-8-9.

91
00:04:17,830 --> 00:04:19,746
Okay. Thanks.

92
00:04:22,117 --> 00:04:24,001
God damn it!

93
00:04:27,206 --> 00:04:30,457
I'll trade you
the knife for the tire.

94
00:04:30,509 --> 00:04:32,426
No.

95
00:04:34,430 --> 00:04:36,046
The data light is out.

96
00:04:36,098 --> 00:04:37,965
Yeah,
it keeps doing that.

97
00:04:38,017 --> 00:04:39,983
Oh. There it...

98
00:04:40,019 --> 00:04:41,969
Back on.

99
00:04:42,021 --> 00:04:43,387
E-E-E... Off again.

100
00:04:43,439 --> 00:04:45,856
Right. That's why
you're here, man.

101
00:04:45,891 --> 00:04:48,275
Man.

102
00:04:48,310 --> 00:04:52,946
Your cable from your router
to your modem...

103
00:04:52,982 --> 00:04:54,448
it's all chewed on.

104
00:04:54,483 --> 00:04:56,033
What do you have...
rats?

105
00:04:56,068 --> 00:04:58,869
No. I have cats.

106
00:05:01,824 --> 00:05:03,240
Boom!

107
00:05:03,292 --> 00:05:05,375
Nailed it.

108
00:05:18,224 --> 00:05:19,806
Shit!

109
00:05:19,842 --> 00:05:21,592
Hey! Cable dude!

110
00:05:21,644 --> 00:05:24,511
Hey! Hey, stop!

111
00:05:24,563 --> 00:05:26,513
Come on! Hey! Hey!

112
00:05:28,567 --> 00:05:30,817
I can't upload my shit!

113
00:05:34,939 --> 00:05:36,355
Well, he didn't fix it,
all right?

114
00:05:36,357 --> 00:05:37,585
He just left
two minutes ago,

115
00:05:37,587 --> 00:05:39,588
and now it's doing
the same thing it was before.

116
00:05:39,590 --> 00:05:42,120
I'm sorry to hear that.
Do you have your repair number?

117
00:05:42,172 --> 00:05:43,738
Yeah, I do.
I do have it.

118
00:05:43,774 --> 00:05:48,176
Uh, 4-7-4-4-3-2-8-9-8-9.

119
00:05:48,228 --> 00:05:50,061
Nope, it's not coming up.

120
00:05:50,096 --> 00:05:52,346
I'll give you another one.

121
00:05:52,398 --> 00:05:53,815
- Ready?
- Yes.

122
00:05:53,850 --> 00:05:56,350
- "A" as in "apropos"...
- Okay.

123
00:05:56,402 --> 00:05:57,902
..."S" as in "shame."

124
00:05:57,937 --> 00:05:58,986
Wait... what?

125
00:05:59,022 --> 00:06:00,521
..."4" as in "4"...

126
00:06:00,573 --> 00:06:01,656
What?

127
00:06:01,691 --> 00:06:03,574
...7, 8...

128
00:06:03,610 --> 00:06:05,326
"L" as in "llama"...

129
00:06:05,361 --> 00:06:07,662
"O" as in "orifice"...

130
00:06:07,697 --> 00:06:09,163
1.

131
00:06:09,199 --> 00:06:11,082
Now I'm gonna send you
up to Tier 2.

132
00:06:11,117 --> 00:06:13,034
Great, because whatever's
happening at this level

133
00:06:13,086 --> 00:06:14,502
is clearly not working.

134
00:06:24,214 --> 00:06:25,847
Hello, Mr. Maron.

135
00:06:25,882 --> 00:06:28,432
How may I provide you
with excellent service today?

136
00:06:28,468 --> 00:06:30,434
- You can fix my Internet.
- Very good.

137
00:06:30,470 --> 00:06:32,303
First,
let me to apologize

138
00:06:32,355 --> 00:06:34,856
for the problems
what you've been experiencing.

139
00:06:34,891 --> 00:06:36,891
Whatever.
Let's just fix it.

140
00:06:36,943 --> 00:06:38,442
And I'm going to help you
with that.

141
00:06:38,478 --> 00:06:40,061
My name is Jeff.

142
00:06:41,314 --> 00:06:43,231
Really?
What's your real name?

143
00:06:43,283 --> 00:06:44,354
Jeffrey.

144
00:06:44,390 --> 00:06:46,367
All right, I think we both
know what's happening here.

145
00:06:46,402 --> 00:06:48,819
If we could just
focus on your Internet,

146
00:06:48,872 --> 00:06:51,656
would you most kindly
restart your modem for me?

147
00:06:51,708 --> 00:06:54,292
No, I've done that already.
That's not the issue.

148
00:06:54,327 --> 00:06:56,410
And how am I gonna
trust you, Jeffrey,

149
00:06:56,462 --> 00:06:58,412
if you are lying
about who you are?

150
00:06:58,464 --> 00:07:01,999
Mr. Maron, I'm going to
schedule a specialist

151
00:07:02,051 --> 00:07:04,835
to come to your home tomorrow
between 8:00 A.M. and noon

152
00:07:04,888 --> 00:07:06,387
if it is most convenient.

153
00:07:06,422 --> 00:07:08,256
Is there
anything else, sir?

154
00:07:08,308 --> 00:07:11,592
Yeah... just that
you're full of shit, Jeff.

155
00:07:15,732 --> 00:07:18,399
What an asshole.

156
00:07:27,777 --> 00:07:30,244
I want
my Internet fixed <i>now.</i>

157
00:07:30,280 --> 00:07:31,362
No bullshit.

158
00:07:31,414 --> 00:07:33,030
That's why I'm here.

159
00:07:33,082 --> 00:07:35,249
Yeah, well,
I want it <i>actually</i> fixed.

160
00:07:35,285 --> 00:07:36,617
None of this, like,

161
00:07:36,669 --> 00:07:39,086
"some shit that makes it
seem fixed" business.

162
00:07:39,122 --> 00:07:41,372
Look, that last guy
was a consultant.

163
00:07:41,424 --> 00:07:43,507
I actually work
for T-West.

164
00:07:43,543 --> 00:07:46,544
I'm gonna get your Internet
running today.

165
00:07:46,596 --> 00:07:48,546
Yeah, I'll believe it
when I see it, pal.

166
00:07:48,598 --> 00:07:50,798
Hey. Look at me.

167
00:07:52,101 --> 00:07:53,601
I got this.

168
00:07:59,642 --> 00:08:01,726
Yeah, great. The last guy
showed me the lights, too.

169
00:08:01,778 --> 00:08:03,227
But when he left,
they all went out.

170
00:08:04,397 --> 00:08:07,148
I'm just
getting started, Marc.

171
00:08:07,200 --> 00:08:08,899
Can I... call you "Marc"?

172
00:08:08,952 --> 00:08:10,618
Yeah, I guess.

173
00:08:10,653 --> 00:08:13,487
Thank you, Marc.

174
00:08:13,539 --> 00:08:14,538
Thank you.

175
00:08:15,575 --> 00:08:16,874
So, the modem is good.

176
00:08:16,909 --> 00:08:19,159
Now let's go check out
that utility box.

177
00:08:20,129 --> 00:08:21,545
Okay, can we...
can we...

178
00:08:21,581 --> 00:08:23,297
can we do it
with less winking?

179
00:08:23,333 --> 00:08:25,082
Could...
Would that be possible?

180
00:08:26,135 --> 00:08:28,002
It's just past
this RV here.

181
00:08:28,054 --> 00:08:30,554
Oh, Jesus Christ.

182
00:08:30,590 --> 00:08:31,589
Hey!

183
00:08:31,641 --> 00:08:33,090
What's going on?!

184
00:08:37,263 --> 00:08:38,262
Dad!

185
00:08:39,849 --> 00:08:40,982
Come on!

186
00:08:41,017 --> 00:08:42,433
Talk to me!

187
00:08:42,485 --> 00:08:44,018
Hey, hey!

188
00:08:44,070 --> 00:08:46,187
Where are you...
What are you doing? What are...

189
00:08:54,580 --> 00:08:56,113
Hey, hold this for a second,
will you?

190
00:08:56,165 --> 00:08:57,531
Huh? Yeah.

191
00:09:00,837 --> 00:09:02,420
You trust this guy?

192
00:09:02,455 --> 00:09:05,172
Yeah, it's the cable guy.
It's his job to fix it.

193
00:09:06,426 --> 00:09:09,293
There's too many wires,
man.

194
00:09:09,345 --> 00:09:11,012
Wires are liars!

195
00:09:11,047 --> 00:09:12,596
Wires are wires.

196
00:09:12,632 --> 00:09:14,799
But I understand
the apprehensiveness.

197
00:09:14,851 --> 00:09:19,737
I don't think you <i>do</i>
understand my apprehensiveness.

198
00:09:20,857 --> 00:09:22,106
Well,
everything looks good

199
00:09:22,141 --> 00:09:23,907
with your modem
and your utility box.

200
00:09:23,959 --> 00:09:25,192
They are primo.

201
00:09:25,228 --> 00:09:27,028
All right, so what the hell are
we gonna do about my Internet?

202
00:09:27,063 --> 00:09:29,613
Well, I'm gonna have
one of our specialists come out,

203
00:09:29,649 --> 00:09:31,399
a guy who knows
a lot more than me.

204
00:09:31,451 --> 00:09:33,784
Shouldn't be too hard to find
a guy like that, should it?

205
00:09:37,457 --> 00:09:38,906
Hey.

206
00:09:38,958 --> 00:09:40,708
We're gonna
get this done.

207
00:09:41,878 --> 00:09:43,461
'Cause I care about you.

208
00:09:43,496 --> 00:09:48,082
T-West cares about you.

209
00:09:51,337 --> 00:09:54,472
It's a sign, man.

210
00:09:54,507 --> 00:09:56,307
Antennas.

211
00:10:04,817 --> 00:10:07,685
All right, what's going on, Dad?
Talk to me.

212
00:10:10,323 --> 00:10:12,907
"The memories flicker
across my mind

213
00:10:12,942 --> 00:10:15,109
like brown leaves
in the wind."

214
00:10:16,662 --> 00:10:19,280
"I wait for the twist.
It comes like a fist.

215
00:10:19,332 --> 00:10:20,364
Another bris.

216
00:10:20,416 --> 00:10:22,833
Everything is amiss.

217
00:10:22,869 --> 00:10:26,087
The waves
crash against my soul.

218
00:10:26,122 --> 00:10:28,539
The murky filled bowl

219
00:10:28,591 --> 00:10:31,459
is a dirty koi pond."

220
00:10:34,964 --> 00:10:36,630
Yeah.

221
00:10:36,682 --> 00:10:37,715
Yeah.

222
00:10:37,767 --> 00:10:40,267
Exactly.

223
00:10:40,303 --> 00:10:41,685
He gets it.

224
00:10:41,721 --> 00:10:43,554
All right, okay. Okay.
You know what?

225
00:10:43,556 --> 00:10:45,306
It's garbage,
and it's manipulative.

226
00:10:45,358 --> 00:10:47,191
What's the point
of living?

227
00:10:48,611 --> 00:10:50,644
My life's been
a disaster.

228
00:10:50,696 --> 00:10:52,863
Disaster's exciting!

229
00:10:52,899 --> 00:10:55,032
You don't know
what you got

230
00:10:55,067 --> 00:10:58,369
until it's ripped open
and the juice is loose!

231
00:11:00,156 --> 00:11:02,323
All right, well,
you both seem to have

232
00:11:02,375 --> 00:11:04,875
an equal level of bullshit
going on,

233
00:11:04,911 --> 00:11:06,961
so I'm gonna go inside,

234
00:11:06,996 --> 00:11:10,414
and you two guys
can talk tires and poetry.

235
00:11:10,466 --> 00:11:12,716
And maybe when you stop crying,
you can come inside

236
00:11:12,752 --> 00:11:14,969
and we'll try to have
a conversation of some kind.

237
00:11:16,005 --> 00:11:17,838
Yeah, sounds reasonable.

238
00:11:17,890 --> 00:11:20,007
Okay. Perfect.

239
00:11:22,061 --> 00:11:22,763
So, we start out in life

240
00:11:22,765 --> 00:11:25,079
with these people who we have
these intense feelings about

241
00:11:25,115 --> 00:11:27,665
and they're bigger than life
to us, and they're just people.

242
00:11:27,700 --> 00:11:30,000
And then we grow up,
and sometimes you change roles,

243
00:11:30,035 --> 00:11:31,972
where you're ending up
taking care of these people

244
00:11:31,974 --> 00:11:33,003
who are
so important to you.

245
00:11:33,056 --> 00:11:34,012
- I know.
- It's hard, right?

246
00:11:34,047 --> 00:11:35,157
And if you don't
have good boundaries...

247
00:11:35,158 --> 00:11:36,524
And you're not alone.

248
00:11:36,559 --> 00:11:39,076
Obviously, a lot of people
deal with complex family issues.

249
00:11:39,111 --> 00:11:41,245
I know. And they just go on
and on and on.

250
00:11:41,280 --> 00:11:43,697
They do go on. They go on...
That's across the life-span.

251
00:11:43,733 --> 00:11:45,315
They're things
that just keep going

252
00:11:45,351 --> 00:11:46,450
and we have to
keep contending with them.

253
00:11:46,502 --> 00:11:48,202
I know, but, like...
Okay, so, at some point,

254
00:11:48,254 --> 00:11:50,037
it's like
things don't work out,

255
00:11:50,089 --> 00:11:51,138
and then you just
have to deal with it

256
00:11:51,190 --> 00:11:52,456
and hope
you don't suffer too much.

257
00:11:52,508 --> 00:11:53,641
Well, you have to <i>accept.</i>

258
00:11:53,693 --> 00:11:55,709
And then acceptance can lead
to happiness sometimes.

259
00:11:55,761 --> 00:11:57,127
Oh, I don't know
about happiness.

260
00:11:57,180 --> 00:11:58,512
But, you know,
I have to accept

261
00:11:58,548 --> 00:12:00,047
that this podcast
might not be posted

262
00:12:00,099 --> 00:12:01,382
because
my Internet is broken.

263
00:12:01,434 --> 00:12:02,800
There's nothing
I can do about it

264
00:12:02,852 --> 00:12:05,519
because my Internet company
is a monopoly.

265
00:12:05,555 --> 00:12:07,972
And that is
a lot like family, too.

266
00:12:08,024 --> 00:12:10,024
Is it?
Who's your Internet company?

267
00:12:10,059 --> 00:12:13,143
T-West. But that's... that's...
that's getting us way off topic.

268
00:12:13,196 --> 00:12:15,196
You're telling me
that you're just accepting

269
00:12:15,231 --> 00:12:16,730
that you have
garbage Internet?

270
00:12:16,782 --> 00:12:20,067
No, I-I actually have
a pretty good situation.

271
00:12:20,119 --> 00:12:22,403
Really?
What do you mean?

272
00:12:22,455 --> 00:12:24,572
They gave me a special number,
and if I have a problem, I call,

273
00:12:24,624 --> 00:12:25,823
and they take care
of everything.

274
00:12:25,875 --> 00:12:27,658
What?!
You have a special number?

275
00:12:27,710 --> 00:12:29,410
They gave you
a number to call?

276
00:12:29,462 --> 00:12:31,161
Can I have the number,
please?

277
00:12:32,298 --> 00:12:34,582
Well, they...
Marc, they gave it to <i>me.</i>

278
00:12:34,634 --> 00:12:36,834
I-I don't think I can just
give it out to anyone.

279
00:12:36,886 --> 00:12:37,919
What... what?
You're better than me?

280
00:12:37,971 --> 00:12:40,387
You're higher than me
on the celebrity power rankings?

281
00:12:40,423 --> 00:12:42,840
Dr. Drew is higher than me?
I think we're equals at least.

282
00:12:42,892 --> 00:12:44,308
I... Marc, I think
this is about something

283
00:12:44,343 --> 00:12:46,010
far more
than just the Internet.

284
00:12:46,062 --> 00:12:48,646
Just 'cause I have family issues
doesn't mean I can't be upset

285
00:12:48,681 --> 00:12:51,432
that Dr. Drew has
some magic bat-phone to T-West.

286
00:12:51,484 --> 00:12:53,150
I don't think
that's exactly what I'm saying.

287
00:12:53,185 --> 00:12:54,235
What are you saying?

288
00:12:57,273 --> 00:12:58,822
I don't have the number.

289
00:13:07,500 --> 00:13:08,532
Dad!

290
00:13:08,584 --> 00:13:09,617
What?!

291
00:13:09,669 --> 00:13:12,453
All right.
Just checking.

292
00:13:25,882 --> 00:13:28,049
I'm dying, son.
I'm dying.

293
00:13:29,386 --> 00:13:30,969
Okay.

294
00:13:31,004 --> 00:13:32,837
How do you know
you're dying?

295
00:13:32,889 --> 00:13:34,139
I <i>know.</i>

296
00:13:35,675 --> 00:13:38,259
This is
exactly what you do.

297
00:13:38,311 --> 00:13:40,845
Do you know
how many times in my life

298
00:13:40,897 --> 00:13:42,897
you've told me
that you're dying?

299
00:13:42,933 --> 00:13:44,482
This time,
I can feel it.

300
00:13:44,518 --> 00:13:45,600
Oh, really?

301
00:13:45,652 --> 00:13:48,068
What does dying feel like?

302
00:13:48,405 --> 00:13:50,321
Dark.

303
00:13:51,942 --> 00:13:53,608
I never thought
I would say this,

304
00:13:53,610 --> 00:13:56,444
but I'm...
I'm actually grateful

305
00:13:56,496 --> 00:13:59,697
to have to deal with
my shitty Internet right now.

306
00:14:06,957 --> 00:14:09,123
Oh, look...
it's the specialist.

307
00:14:09,125 --> 00:14:10,291
Modem's in here.

308
00:14:10,293 --> 00:14:12,126
I don't need to see it.

309
00:14:12,128 --> 00:14:13,795
What?

310
00:14:13,797 --> 00:14:15,797
You've had two guys
look at it.

311
00:14:15,799 --> 00:14:17,902
Forget it.
It's pointless.

312
00:14:20,554 --> 00:14:23,915
I was reading over
your repair report.

313
00:14:25,225 --> 00:14:27,642
And, uh...

314
00:14:27,644 --> 00:14:30,395
It's... it's shit.

315
00:14:30,447 --> 00:14:34,732
It's... it's just shit.

316
00:14:34,784 --> 00:14:37,819
You know what I was thinking
about when I was reading this?

317
00:14:37,821 --> 00:14:39,621
"Cool hand Luke."

318
00:14:39,656 --> 00:14:41,706
The system's
not gonna change.

319
00:14:41,741 --> 00:14:44,492
They need you to break.
They need you to give in.

320
00:14:45,879 --> 00:14:48,213
And if they can't,
they'll kill you.

321
00:14:49,916 --> 00:14:52,834
Okay, well, that's...
that's pretty intense.

322
00:14:52,836 --> 00:14:55,553
But, uh, I just
want my Internet to work.

323
00:14:55,589 --> 00:14:59,757
You think T-West cares
if you have a good connection?

324
00:14:59,809 --> 00:15:00,892
No, I don't.

325
00:15:00,927 --> 00:15:03,678
You're goddamn right,
they don't.

326
00:15:03,680 --> 00:15:06,330
The whole setup
is a joke.

327
00:15:06,382 --> 00:15:08,683
So, I'm... I'm taking it
you're not a company man.

328
00:15:12,272 --> 00:15:14,689
I'm gonna tell you something

329
00:15:14,741 --> 00:15:17,191
that nobody
on your side of the fence

330
00:15:17,244 --> 00:15:18,576
is supposed to know.

331
00:15:18,612 --> 00:15:19,944
Come here.

332
00:15:22,282 --> 00:15:25,199
It's the node.

333
00:15:25,201 --> 00:15:28,253
It's the goddamn node.

334
00:15:30,290 --> 00:15:32,206
The node, huh?

335
00:15:32,208 --> 00:15:34,876
The node.

336
00:15:34,878 --> 00:15:37,045
That's it right there.
That's the node.

337
00:15:37,097 --> 00:15:38,096
Ahhhh.

338
00:15:38,131 --> 00:15:39,547
What's it do?

339
00:15:39,549 --> 00:15:41,716
It's a redistribution
point.

340
00:15:41,718 --> 00:15:45,270
The signal goes into the node,
and then it sends the Internet

341
00:15:45,355 --> 00:15:47,605
out to everybody
in the neighborhood.

342
00:15:47,641 --> 00:15:49,057
Oh.
And it's broken?

343
00:15:49,109 --> 00:15:51,109
Nope... overloaded.

344
00:15:51,144 --> 00:15:53,061
Oh, we got
an overloaded node.

345
00:15:53,063 --> 00:15:54,562
Yep.
Needs to be split.

346
00:15:54,614 --> 00:15:56,064
We need
a node splitter?

347
00:15:56,116 --> 00:15:57,231
Yep.

348
00:15:57,233 --> 00:15:58,733
But that could
take weeks.

349
00:15:58,735 --> 00:15:59,951
Why?

350
00:15:59,986 --> 00:16:02,236
Because T-West doesn't
split the node

351
00:16:02,238 --> 00:16:04,405
unless it's at 87%.

352
00:16:04,407 --> 00:16:06,457
Yours is at 83%.

353
00:16:06,493 --> 00:16:09,294
So I just have to live with
an overloaded node?

354
00:16:09,329 --> 00:16:10,828
That's it.

355
00:16:12,465 --> 00:16:14,582
Yeah. Bullshit.
They don't care.

356
00:16:14,634 --> 00:16:15,917
It's total bullshit.

357
00:16:15,969 --> 00:16:17,585
- Well, hey, Bernie.
- Hey.

358
00:16:17,637 --> 00:16:19,971
- Will you hold that for me for one second?
- Sure.

359
00:16:20,006 --> 00:16:21,806
What's happening?

360
00:16:26,429 --> 00:16:28,596
Even trade.

361
00:16:28,598 --> 00:16:29,847
Sure. Whatever.

362
00:16:29,899 --> 00:16:31,316
I just broke the chains.

363
00:16:32,485 --> 00:16:34,268
I'm a free man.

364
00:16:34,321 --> 00:16:35,520
Thank you.

365
00:16:35,572 --> 00:16:36,688
Yeah.

366
00:16:36,740 --> 00:16:38,856
Hoo-hoo-hoo!

367
00:16:51,871 --> 00:16:53,171
Ahhhhh!

368
00:16:53,206 --> 00:16:54,288
There.

369
00:16:54,341 --> 00:16:55,673
There!

370
00:17:00,714 --> 00:17:02,296
God damn it.

371
00:17:14,227 --> 00:17:16,194
Good evening.
This is Tier 3.

372
00:17:16,229 --> 00:17:18,312
I didn't even know
there <i>was</i> a Tier 3.

373
00:17:18,365 --> 00:17:20,648
Do you need my, uh,
10-digit repair number?

374
00:17:20,700 --> 00:17:22,984
Nope. I'm looking at your report
right now.

375
00:17:23,036 --> 00:17:24,369
Oh, my God.

376
00:17:24,404 --> 00:17:26,487
I can't believe
what you've been through.

377
00:17:26,489 --> 00:17:28,489
Look,
I know about the node.

378
00:17:28,491 --> 00:17:30,041
I need a node splitter.

379
00:17:30,076 --> 00:17:33,828
Yeah, I don't know
what a node splitter is.

380
00:17:33,880 --> 00:17:36,381
Sorry.
Nothing more we can do.

381
00:17:36,416 --> 00:17:39,550
Wait! Wait!
I know Dr. Drew.

382
00:17:39,586 --> 00:17:43,221
In that case, you can expect
a call from Tier 4.

383
00:17:44,224 --> 00:17:46,257
Hello?

384
00:17:46,309 --> 00:17:48,342
Hello?

385
00:17:59,572 --> 00:18:00,855
Dad?

386
00:18:03,109 --> 00:18:05,359
Dad?

387
00:18:05,361 --> 00:18:06,828
Hello?

388
00:18:40,891 --> 00:18:42,057
Hello?

389
00:18:42,092 --> 00:18:44,560
Hey, man.
How's it going?

390
00:18:44,595 --> 00:18:46,011
Who's this?

391
00:18:46,063 --> 00:18:48,564
Oh, come on, Marc.
You know who this is.

392
00:18:48,599 --> 00:18:50,516
This is Tier 4.

393
00:18:50,568 --> 00:18:52,768
Hey.

394
00:18:52,820 --> 00:18:55,070
Do you need
my 10-digit repair number?

395
00:18:55,105 --> 00:18:56,405
No.

396
00:18:56,440 --> 00:18:58,574
Why do you give it out
if you never need it?

397
00:18:58,609 --> 00:19:02,194
Well, to give people something
to hold on to... hope.

398
00:19:02,196 --> 00:19:04,530
Yeah,
I'm not feeling that.

399
00:19:04,532 --> 00:19:07,866
Things aren't always
what they seem, Marc.

400
00:19:07,868 --> 00:19:11,954
Sometimes what's going on
is deeper and more impactful.

401
00:19:12,006 --> 00:19:14,373
The real truth
is underneath.

402
00:19:14,425 --> 00:19:15,874
Okay.

403
00:19:15,926 --> 00:19:17,543
All right, I get that.

404
00:19:17,595 --> 00:19:19,545
But I still need Internet.

405
00:19:19,547 --> 00:19:20,879
<i>Everyone</i> needs Internet.

406
00:19:20,931 --> 00:19:22,464
But that doesn't mean

407
00:19:22,516 --> 00:19:24,766
that what you're feeling
right now is about the Internet,

408
00:19:24,802 --> 00:19:27,052
because it's not.

409
00:19:27,054 --> 00:19:31,056
It's about a deep-rooted need
for justice

410
00:19:31,108 --> 00:19:33,892
and fear
of not being in control.

411
00:19:33,944 --> 00:19:36,445
Yeah.
I understand that.

412
00:19:36,480 --> 00:19:39,731
It's about
something deeper.

413
00:19:39,733 --> 00:19:44,286
You need to stop
yelling at your girlfriends.

414
00:19:44,321 --> 00:19:47,289
You need to make up
with your father.

415
00:19:47,324 --> 00:19:49,324
And you need
to stop watching porn.

416
00:19:49,376 --> 00:19:51,627
You don't watch a lot,
but you watch enough.

417
00:19:51,662 --> 00:19:53,629
I don't watch
anything that weird.

418
00:19:53,664 --> 00:19:57,082
A couple of weeks ago,
it got a little weird.

419
00:19:57,134 --> 00:19:59,935
Well, yeah, but, you know, when
you get into the Baltic stuff,

420
00:19:59,970 --> 00:20:01,470
you really don't know
what's gonna happen.

421
00:20:01,505 --> 00:20:03,422
And then... I-I don't know.

422
00:20:03,424 --> 00:20:05,007
All right,
so, what's your point?

423
00:20:05,059 --> 00:20:07,593
Y-You... you want me
to engage in my life

424
00:20:07,595 --> 00:20:09,595
and...
and be a better person?

425
00:20:09,597 --> 00:20:12,648
Yeah.
Be a stand-up guy.

426
00:20:14,151 --> 00:20:15,601
Okay.

427
00:20:15,603 --> 00:20:16,985
Done.

428
00:20:17,021 --> 00:20:18,937
Okay.

429
00:20:18,989 --> 00:20:20,822
I just split the node.

430
00:20:21,825 --> 00:20:23,358
You can just do it
from there?

431
00:20:23,410 --> 00:20:24,993
Yeah.

432
00:20:25,029 --> 00:20:27,579
I'm the node splitter.

433
00:20:42,763 --> 00:20:45,847
Sometimes
I feel completely hopeless.

434
00:20:45,883 --> 00:20:47,633
Yeah.

435
00:20:47,685 --> 00:20:49,968
Me too.

436
00:20:49,970 --> 00:20:51,353
But not right now.

437
00:20:51,388 --> 00:20:53,438
Everything's all right.

438
00:20:54,892 --> 00:20:56,725
It's good to see you,
Dad.

439
00:21:00,614 --> 00:21:02,314
Shit, yes!

440
00:21:02,316 --> 00:21:03,398
Internet's working.

441
00:21:03,450 --> 00:21:05,784
Well, now.
How about that?

442
00:21:06,965 --> 00:21:15,465
Sync and corrections by explosiveskull
www.addic7ed.com

