﻿1
00:00:01,667 --> 00:00:03,666
- Yeah! Touchdown!
- Yes! Jets, baby!

2
00:00:03,691 --> 00:00:05,339
Are we celebrating something?

3
00:00:05,364 --> 00:00:07,565
Yes. Jets up by 14.

4
00:00:07,661 --> 00:00:08,838
Look alive, Chale.

5
00:00:11,725 --> 00:00:13,499
Why did I come in here?

6
00:00:14,829 --> 00:00:16,360
Why did I throw that?

7
00:00:16,385 --> 00:00:18,271
He really shouldn't have thrown that.

8
00:00:18,296 --> 00:00:19,938
Move your hands up, dude.

9
00:00:19,963 --> 00:00:21,823
Why won't my hands move?

10
00:00:22,061 --> 00:00:23,431
Maybe he'll surprise me.

11
00:00:23,456 --> 00:00:26,536
Even seals catch balls,
and they don't have arms.

12
00:00:26,561 --> 00:00:28,568
Ooh, do I have an ice
pack in the fridge?

13
00:00:28,593 --> 00:00:30,904
No pressure. No pressure.

14
00:00:31,194 --> 00:00:33,394
This will tell me everything
I need to know about you.

15
00:00:33,626 --> 00:00:35,997
Don't close your eyes.
Don't close your eyes!

16
00:00:36,195 --> 00:00:38,295
Come on, Chale. One time.

17
00:00:39,097 --> 00:00:40,503
I got it!

18
00:00:40,528 --> 00:00:42,074
Way to go, Chale!

19
00:00:42,099 --> 00:00:43,702
I love you so much!

20
00:00:43,727 --> 00:00:45,994
Wow! He caught it.

21
00:00:47,305 --> 00:00:48,512
- Ohh!
- Ohh!

22
00:00:48,537 --> 00:00:49,783
I'll get the ice pack.

23
00:00:50,277 --> 00:00:52,692
That's a purple face tomorrow.

24
00:00:59,134 --> 00:01:00,700
Hey.

25
00:01:00,725 --> 00:01:02,358
Hey, babe. How was work?

26
00:01:02,383 --> 00:01:04,517
Ugh. Horrible.

27
00:01:04,542 --> 00:01:05,806
I'm sorry.

28
00:01:05,831 --> 00:01:07,928
Listen. You know how our
grill is on its way out?

29
00:01:07,953 --> 00:01:08,832
I've been looking online.

30
00:01:08,857 --> 00:01:11,109
They have these, like,
smoker/grill combos.

31
00:01:11,134 --> 00:01:13,635
They're pricy, but, man,
what a game changer.

32
00:01:13,660 --> 00:01:18,043
Uh, honey,
I just told you my day was horrible.

33
00:01:18,068 --> 00:01:19,158
Yeah.

34
00:01:20,727 --> 00:01:22,511
I just told you we could
be making pulled pork

35
00:01:22,536 --> 00:01:24,185
in our own backyard.

36
00:01:24,511 --> 00:01:26,004
All right. What happened?

37
00:01:26,029 --> 00:01:28,424
Okay, well, first of all,
I wake up this morning

38
00:01:28,449 --> 00:01:30,632
and one of my wisdom
teeth is just throbbing.

39
00:01:30,657 --> 00:01:31,737
I mean, it's got to come out,

40
00:01:31,762 --> 00:01:34,824
but Dr. Schneider can't
even see me until Thursday.

41
00:01:34,849 --> 00:01:35,605
Ohh.

42
00:01:35,630 --> 00:01:37,860
And then I get an e-mail
from my principal,

43
00:01:37,885 --> 00:01:39,793
and you know how I want to move up

44
00:01:39,818 --> 00:01:41,895
from nursing at the middle
school to the high school?

45
00:01:41,920 --> 00:01:43,520
No, but go on.

46
00:01:45,534 --> 00:01:49,102
Well, I have seniority,
but Gwen in my office,

47
00:01:49,127 --> 00:01:51,004
you know,
the one who found out she's 1/10 French,

48
00:01:51,029 --> 00:01:52,338
and all of a sudden, she's got an accent

49
00:01:52,363 --> 00:01:53,868
and she's double-kissing people.

50
00:01:54,536 --> 00:01:57,752
She went behind my back
to try to get the job.

51
00:01:57,777 --> 00:01:58,832
That's ridiculous.

52
00:01:58,857 --> 00:02:00,207
I know!

53
00:02:00,672 --> 00:02:02,111
This one's over 1,000 bucks,

54
00:02:02,136 --> 00:02:03,979
and it doesn't even have a rotisserie.

55
00:02:05,684 --> 00:02:06,948
Honey...

56
00:02:07,595 --> 00:02:09,988
I'm trying to tell you
how horrible my day was,

57
00:02:10,013 --> 00:02:11,427
and you are looking at grills

58
00:02:11,452 --> 00:02:13,286
that we absolutely cannot afford?

59
00:02:13,310 --> 00:02:15,693
I get it.
We can pay to have your teeth taken out,

60
00:02:15,719 --> 00:02:19,866
but the minute I want to
smoke some meat, we're broke.

61
00:02:21,017 --> 00:02:23,677
Hey, you're going to work?
I thought you had class.

62
00:02:23,702 --> 00:02:25,108
Yeah, well, we're short a bartender,

63
00:02:25,133 --> 00:02:26,815
and Enzo needs me to
pick up extra shifts,

64
00:02:26,840 --> 00:02:28,760
but my friend is taking notes,
so it's all good,

65
00:02:28,785 --> 00:02:30,610
but I got to go, so I
love you, guys, okay?

66
00:02:30,634 --> 00:02:31,832
Love you.

67
00:02:34,262 --> 00:02:36,427
Did you hear what Kendra just said?

68
00:02:36,735 --> 00:02:39,630
I think we established
listening isn't my strong suit.

69
00:02:40,790 --> 00:02:42,927
She's missing class.

70
00:02:42,952 --> 00:02:45,534
Okay, Enzo's was supposed
to be a part-time job

71
00:02:45,559 --> 00:02:48,249
so that she could earn a
little extra spending money.

72
00:02:48,273 --> 00:02:49,614
We got to talk to her.

73
00:02:49,639 --> 00:02:51,030
All right. We will tonight.

74
00:02:51,055 --> 00:02:52,579
No. It's more important than that.

75
00:02:52,604 --> 00:02:55,226
You know what?
Can you please go down there right now?

76
00:02:55,251 --> 00:02:57,066
I would, but my jaw, it just...

77
00:02:57,091 --> 00:02:59,745
It hurts every time I move my mouth.

78
00:03:00,724 --> 00:03:02,726
Don't you think that's a
little message from God

79
00:03:02,751 --> 00:03:04,648
telling you to...

80
00:03:05,963 --> 00:03:08,085
telling you to...

81
00:03:13,190 --> 00:03:15,533
Listen, sweetheart, I got to talk to
you about something very important.

82
00:03:15,558 --> 00:03:16,844
Okay. Dad, for the last time,

83
00:03:16,869 --> 00:03:19,163
I do not have the power to
name a sandwich after you.

84
00:03:20,140 --> 00:03:23,093
All I'm asking is that you drop
a note in the suggestion box.

85
00:03:23,118 --> 00:03:24,980
Okay, Dad, I got to get these pies out.

86
00:03:25,005 --> 00:03:26,912
I'm not... I wanted to talk to
you about something else anyway.

87
00:03:26,937 --> 00:03:28,006
I got to go!

88
00:03:29,127 --> 00:03:31,702
Oh, wait. Guy! My beer. Seriously?

89
00:03:31,727 --> 00:03:34,027
It's right there.
Kevin, just hand that to him, please.

90
00:03:34,052 --> 00:03:35,484
- Ah.
- Thanks.

91
00:03:35,509 --> 00:03:36,835
Yeah, Enzo's.

92
00:03:36,860 --> 00:03:38,239
Oh, yeah. I'm checking on it now.

93
00:03:38,264 --> 00:03:41,204
Sorry. I didn't mean to snap like that.

94
00:03:41,229 --> 00:03:43,625
I'm just having one of
those days, you know?

95
00:03:43,650 --> 00:03:45,136
I hear ya.

96
00:03:45,421 --> 00:03:46,705
Big promotion at work,

97
00:03:46,730 --> 00:03:49,128
this buddy of mine's trying
to steal it from under me,

98
00:03:49,153 --> 00:03:51,002
and he doesn't have any talent.

99
00:03:51,028 --> 00:03:52,926
He's just got a big mouth.

100
00:03:53,807 --> 00:03:55,811
Look, I don't know your situation,

101
00:03:55,836 --> 00:03:58,663
but, uh,
I'm sure your boss will figure it out.

102
00:03:58,688 --> 00:04:02,011
Just remember this...
Smart is always quiet.

103
00:04:02,036 --> 00:04:03,379
Idiots are always loud.

104
00:04:03,404 --> 00:04:04,894
Hey! We need more ice!

105
00:04:05,236 --> 00:04:06,796
See there?

106
00:04:06,821 --> 00:04:08,547
Yeah, thanks. All right.

107
00:04:08,572 --> 00:04:10,002
I needed that. There you go, buddy.

108
00:04:10,027 --> 00:04:13,094
Oh, uh, I-I-I-I can't make change.

109
00:04:13,119 --> 00:04:15,032
Keep it. It's yours.

110
00:04:17,324 --> 00:04:18,678
Excuse me.

111
00:04:18,703 --> 00:04:21,204
What do recommend...
the meatball hero or the calzone?

112
00:04:21,229 --> 00:04:25,311
I recommend you lose the "or"
and slap an "and" on that.

113
00:04:25,692 --> 00:04:26,758
You know what?

114
00:04:26,783 --> 00:04:28,469
We'll take two of each to go,

115
00:04:28,494 --> 00:04:30,342
and we'll have two beers while we wait.

116
00:04:30,367 --> 00:04:31,466
I like it.

117
00:04:31,491 --> 00:04:33,713
Enzo, these guys want a couple of beers.

118
00:04:40,347 --> 00:04:42,741
Hey, there he is. So, how'd it go?

119
00:04:42,766 --> 00:04:44,315
- Unbelievable.
- Really?

120
00:04:44,339 --> 00:04:45,913
Yeah. I got the job.

121
00:04:47,071 --> 00:04:49,638
Wh... What? W-What job?

122
00:04:49,663 --> 00:04:51,384
Bartender. Enzo's.

123
00:04:51,409 --> 00:04:53,854
He needed someone, and I was there.

124
00:04:53,879 --> 00:04:56,640
What's up? What's up?

125
00:04:58,940 --> 00:05:01,195
Okay. Hold up.

126
00:05:01,220 --> 00:05:02,915
Okay, so, I sent you down there

127
00:05:02,940 --> 00:05:04,821
to make sure Kendra
wasn't working too much,

128
00:05:04,846 --> 00:05:06,731
and now you work there, too?

129
00:05:06,756 --> 00:05:08,802
I'll make money for my smoker, right?

130
00:05:08,827 --> 00:05:11,338
I'll check up on her.
It's like eyes on the inside.

131
00:05:11,989 --> 00:05:13,736
I guess... so.

132
00:05:13,761 --> 00:05:16,272
And get this, my employee number 007.

133
00:05:16,297 --> 00:05:18,105
No one took it. How awesome is that?

134
00:05:18,130 --> 00:05:19,301
- Nobody.
- Yeah.

135
00:05:19,326 --> 00:05:22,004
No, babe. I-I'll update our Facebook.

136
00:05:22,029 --> 00:05:23,455
Already did.

137
00:05:27,477 --> 00:05:28,749
I don't know.

138
00:05:28,804 --> 00:05:31,595
I'm just starting to worry that
Jeff has commitment issues.

139
00:05:31,620 --> 00:05:32,556
Well, let's see.

140
00:05:32,582 --> 00:05:35,585
He's got two cellphones,
he comes home covered in glitter,

141
00:05:35,610 --> 00:05:37,968
and we both know it ain't from
working in the craft store.

142
00:05:38,802 --> 00:05:40,374
Well, what do I do?

143
00:05:40,399 --> 00:05:43,398
Ding, ding.
I think you got to call the bellhop.

144
00:05:43,423 --> 00:05:44,585
Bellhop?

145
00:05:44,610 --> 00:05:46,391
'Cause you ain't just got a boyfriend,

146
00:05:46,416 --> 00:05:48,591
you got extra baggage.

147
00:05:48,909 --> 00:05:50,857
You know what? I think you're right.

148
00:05:50,882 --> 00:05:52,131
Yeah.

149
00:05:52,463 --> 00:05:55,773
Ugh. Give me the stiffest drink you got.

150
00:05:55,798 --> 00:05:57,903
All right. Straight whiskey?

151
00:05:57,928 --> 00:05:59,148
Yeah.

152
00:05:59,433 --> 00:06:01,391
You know what?
Throw some Coke in there, too.

153
00:06:01,416 --> 00:06:02,381
Okay.

154
00:06:02,406 --> 00:06:04,015
And a lot of ice.

155
00:06:04,041 --> 00:06:05,630
Hey, you know what?
Just give me a Sprite.

156
00:06:07,152 --> 00:06:09,311
Somebody had a tough day, huh?

157
00:06:09,336 --> 00:06:10,689
Yeah, well, you know,
I've been trying to get

158
00:06:10,714 --> 00:06:12,660
that fire-safety gig at Citi Field.

159
00:06:12,685 --> 00:06:16,016
I e-mailed my résumé,
but I got the standard automated reply,

160
00:06:16,041 --> 00:06:17,869
so I guess that's done.

161
00:06:17,894 --> 00:06:20,390
What if I told you I knew the
guy who could get you that job?

162
00:06:20,612 --> 00:06:21,938
Really? Who?

163
00:06:21,963 --> 00:06:23,253
You.

164
00:06:24,236 --> 00:06:26,591
That's weird,
because I mentioned me in the e-mail

165
00:06:26,616 --> 00:06:28,286
and they still said no.

166
00:06:28,952 --> 00:06:33,073
Kyle, you need to look at
failure as the sprinkles

167
00:06:33,098 --> 00:06:36,559
that make the cupcakes of life so sweet.

168
00:06:37,554 --> 00:06:41,845
You know, you're saying that
so slow like it's important,

169
00:06:42,117 --> 00:06:44,066
but I'm still not understanding.

170
00:06:44,708 --> 00:06:48,080
You go down there and you
put a face with the name.

171
00:06:48,258 --> 00:06:50,785
Oh. Well, why didn't you say that?

172
00:06:50,810 --> 00:06:53,641
What's this thing with all
the sprinkles and cupcakes?

173
00:06:54,222 --> 00:06:55,835
I have my process.

174
00:06:57,413 --> 00:06:59,437
- Hey, babe.
- Hi.

175
00:06:59,462 --> 00:07:02,888
Sorry I'm so late.
Enzo's was crazy tonight.

176
00:07:02,913 --> 00:07:04,280
Well, you missed dinner.

177
00:07:04,305 --> 00:07:05,409
Oh, yeah? What'd you make?

178
00:07:05,434 --> 00:07:06,665
Vegetable stir-fry.

179
00:07:06,690 --> 00:07:09,208
Hmm. Sorry.
Checked out when I heard "vegetable."

180
00:07:11,500 --> 00:07:14,477
Very funny. Hey, how is Kendra doing?

181
00:07:14,723 --> 00:07:17,277
Well, she doesn't tip me out as
well as the other waitresses,

182
00:07:17,303 --> 00:07:21,313
but... I'm gonna bring it up to
her at the next staff meeting.

183
00:07:25,088 --> 00:07:27,021
Yo, Timbo, you got Kev. What's up?

184
00:07:27,046 --> 00:07:29,936
Uh, hey, babe,
who is calling you after 11:00 at night?

185
00:07:29,961 --> 00:07:31,775
Uh, hold on a second, Tim.

186
00:07:32,066 --> 00:07:33,470
He's one of my regulars, all right?

187
00:07:33,495 --> 00:07:35,439
He's up for a promotion,
and one of his friends

188
00:07:35,464 --> 00:07:36,563
is trying to steal it from him.

189
00:07:36,588 --> 00:07:37,827
Oh.

190
00:07:37,852 --> 00:07:39,659
You do realize that's
the exact same problem

191
00:07:39,684 --> 00:07:41,250
I told you I was having at work?

192
00:07:41,275 --> 00:07:42,975
No. But if it is, I'd listen in,

193
00:07:43,000 --> 00:07:45,032
'cause I'm about to drop some knowledge.

194
00:07:45,698 --> 00:07:47,856
Tim, here's what you tell your boss...

195
00:07:49,288 --> 00:07:51,665
Well, I dumped my
boyfriend, like you said.

196
00:07:51,690 --> 00:07:53,190
All right. And how's that make you feel?

197
00:07:53,215 --> 00:07:54,066
Great!

198
00:07:54,091 --> 00:07:55,743
But now all my friends
are saying I should

199
00:07:55,768 --> 00:07:59,308
get back with him because,
well, he can be really sweet.

200
00:07:59,333 --> 00:08:01,776
Ooh. So is a grizzly
bear when it's sleeping.

201
00:08:01,801 --> 00:08:03,724
Dump them, too.

202
00:08:03,953 --> 00:08:05,542
Hey, Tim!

203
00:08:05,567 --> 00:08:06,800
I did what you said.

204
00:08:07,208 --> 00:08:09,620
It worked like a charm.
I got the promotion.

205
00:08:09,645 --> 00:08:10,798
That's awesome.

206
00:08:10,823 --> 00:08:12,021
Only thing is, they want to move me

207
00:08:12,046 --> 00:08:13,424
to a satellite office in Jersey.

208
00:08:13,449 --> 00:08:15,593
You... You want to be in Jersey?

209
00:08:15,618 --> 00:08:18,114
Well, no.
I'd rather be at the office in the city,

210
00:08:18,139 --> 00:08:19,336
but, I mean, that'll be okay, right?

211
00:08:19,361 --> 00:08:20,258
Wrong.

212
00:08:21,572 --> 00:08:24,527
I got you this far.
I'll get you the rest of the way.

213
00:08:24,552 --> 00:08:26,166
You tell them it's a no-go on Jersey.

214
00:08:26,191 --> 00:08:28,133
Really? I mean, it is a promotion.

215
00:08:28,158 --> 00:08:30,703
Yeah. It's a promotion for your job,

216
00:08:30,728 --> 00:08:33,991
but a demotion for your heart.

217
00:08:34,238 --> 00:08:36,227
Wow.

218
00:08:36,252 --> 00:08:38,325
Yeah. I felt it, too.

219
00:08:41,172 --> 00:08:43,703
All right. Come on, guys. Look alive.

220
00:08:43,728 --> 00:08:46,083
You stood us up, man. Not cool.

221
00:08:46,108 --> 00:08:49,184
I had to work, but, hey,
I left you a motivational voicemail.

222
00:08:49,312 --> 00:08:53,357
You only miss 100% of the
shots you don't take? What?

223
00:08:53,382 --> 00:08:57,864
Yeah. Your brother missed
100% of the shots he did take.

224
00:08:58,733 --> 00:09:01,951
I'm a defensive specialist.
You knew that going in.

225
00:09:01,976 --> 00:09:04,437
Guys, guys, let's just...
Let's get out of here.

226
00:09:04,462 --> 00:09:06,741
He's tearing us apart. Come on.

227
00:09:07,425 --> 00:09:09,109
Hey, uh, Kyle, wait up.

228
00:09:09,134 --> 00:09:11,991
How's it going with the
job down at Citi Field?

229
00:09:12,016 --> 00:09:14,065
Well, I went down
there, and they told me

230
00:09:14,091 --> 00:09:17,833
I needed to fill out a résumé online,
so back where I started.

231
00:09:17,858 --> 00:09:19,173
And you're okay with that?

232
00:09:19,198 --> 00:09:20,913
What am I supposed to do?

233
00:09:20,939 --> 00:09:22,217
I'll tell you what you're gonna do.

234
00:09:22,242 --> 00:09:24,443
Tomorrow,
the Operations Manager at Citi Field

235
00:09:24,468 --> 00:09:27,354
walks into his office,
and who's sitting there?

236
00:09:27,379 --> 00:09:28,736
His secretary?

237
00:09:30,174 --> 00:09:32,053
No. You.

238
00:09:32,078 --> 00:09:34,578
Because you don't let
security or fences stop you.

239
00:09:34,603 --> 00:09:36,022
Those are just obstacles.

240
00:09:36,103 --> 00:09:38,524
One way or the other,
you get in that office,

241
00:09:38,549 --> 00:09:40,686
and you let them know
you are not leaving

242
00:09:40,711 --> 00:09:42,335
until you get what you came for,

243
00:09:42,361 --> 00:09:44,112
'cause that's what a go-getter does.

244
00:09:44,397 --> 00:09:46,855
See, normally,
that would be a list of things

245
00:09:46,880 --> 00:09:48,534
that would get me arrested.

246
00:09:49,345 --> 00:09:51,405
But the way you say it

247
00:09:51,430 --> 00:09:53,906
sounds like I'm gonna get the job.

248
00:09:55,745 --> 00:09:59,272
Nice to meet you,
Head of Fire Safety at Citi Field.

249
00:10:00,666 --> 00:10:02,079
I just got chills.

250
00:10:09,237 --> 00:10:11,857
Wow. That smoker's awesome.
I can't believe it.

251
00:10:11,882 --> 00:10:13,207
How did you know to get the right one?

252
00:10:13,232 --> 00:10:14,993
Uh, well, your screen saver

253
00:10:15,018 --> 00:10:16,884
used to be a picture of me and the kids,

254
00:10:16,909 --> 00:10:19,987
and then I noticed it changed
to the Smoke Pro 4000.

255
00:10:20,012 --> 00:10:24,745
So... bit of a gut punch,
but message received.

256
00:10:25,111 --> 00:10:28,720
All right. This round is
bratwurst, sausage, and ribs.

257
00:10:28,745 --> 00:10:30,379
Oh, and, by the way, the mystery spice

258
00:10:30,404 --> 00:10:32,229
from the last round was habanero.

259
00:10:32,272 --> 00:10:35,404
That answer, again... habanero.

260
00:10:35,982 --> 00:10:39,885
Mr. Gable, I'm afraid I'm
starting to get the meat sweats.

261
00:10:40,176 --> 00:10:42,097
Yeah, I can't eat anymore.

262
00:10:42,122 --> 00:10:43,221
Never thought I'd say this,

263
00:10:43,245 --> 00:10:44,681
but I please go do my homework?

264
00:10:44,706 --> 00:10:45,581
Yeah.

265
00:10:45,775 --> 00:10:47,469
I need to lie down.

266
00:10:47,494 --> 00:10:48,997
Okay, well, don't go sleep,

267
00:10:49,022 --> 00:10:50,955
or Mr. Gable might smoke and eat you.

268
00:10:52,165 --> 00:10:54,032
You know what, hon?
I got to get to work anyway, so...

269
00:10:54,057 --> 00:10:56,314
What? You're going to Enzo's?

270
00:10:56,339 --> 00:10:58,419
Honey, I thought you were done.
You got the smoker.

271
00:10:58,444 --> 00:11:00,344
Babe,
that's kind of why I got you the smoker.

272
00:11:00,369 --> 00:11:01,951
But that's not what it's about anymore.

273
00:11:01,976 --> 00:11:04,443
Look. I don't need to make
the money anymore, but it's...

274
00:11:04,624 --> 00:11:06,309
You know, it's...
I'm connecting with these people.

275
00:11:06,334 --> 00:11:08,241
And I didn't realize this,
but I have a gift.

276
00:11:08,266 --> 00:11:10,037
Okay.

277
00:11:10,443 --> 00:11:13,185
Honey, you missed Sara's lacrosse game,

278
00:11:13,210 --> 00:11:15,311
I feel like I've only seen
you for 10 minutes this week,

279
00:11:15,336 --> 00:11:17,347
and do even remember that
you have to pick me up

280
00:11:17,372 --> 00:11:19,053
- at the dentist's tomorrow?
- Yeah, of course, I do.

281
00:11:19,079 --> 00:11:21,281
You're getting hung up on the
three or four balls I'm dropping.

282
00:11:21,307 --> 00:11:24,247
You got to focus on the
100 I got in the air.

283
00:11:24,766 --> 00:11:27,652
Wow! That is awesome.
I got to remember that for Phyllis.

284
00:11:27,948 --> 00:11:29,884
So, ladies, don't bog down

285
00:11:29,909 --> 00:11:32,619
on the three or four
balls you drop in life.

286
00:11:32,644 --> 00:11:36,120
Instead,
focus on the 100 you keep in the air

287
00:11:36,483 --> 00:11:39,262
and shine like diamonds in the night.

288
00:11:39,287 --> 00:11:41,997
Excuse me.

289
00:11:43,439 --> 00:11:44,905
Enzo's.

290
00:11:44,930 --> 00:11:47,816
Where are you? I need you to pick me up.

291
00:11:47,841 --> 00:11:49,803
Hey. This is a business,
all right, buddy?

292
00:11:49,828 --> 00:11:51,741
I'm a cop, and I know
how to trace a call.

293
00:11:51,766 --> 00:11:53,165
But it's me!

294
00:11:53,190 --> 00:11:54,189
It's me!

295
00:11:54,214 --> 00:11:55,976
Get help, freak!

296
00:11:59,160 --> 00:12:00,723
Look, I'm really sorry, all right?

297
00:12:00,748 --> 00:12:03,711
It was crazy there!
I didn't even feel my phone buzzing.

298
00:12:03,736 --> 00:12:05,756
By the way, I got to say,
without the back teeth,

299
00:12:05,781 --> 00:12:07,598
your cheek bones are popping.

300
00:12:08,018 --> 00:12:10,073
I ain't hating the swollen lips, either.

301
00:12:10,621 --> 00:12:13,863
It's like, "Is that Donna Gable,
or is that Gisele?"

302
00:12:13,888 --> 00:12:16,404
It's Donna Gable.

303
00:12:17,012 --> 00:12:19,091
And don't try to be
nice right now, okay?

304
00:12:19,116 --> 00:12:21,255
The job has to end. I'm not joking.

305
00:12:21,280 --> 00:12:24,997
Com on. I messed up, all right?
But don't make me quit. Please.

306
00:12:25,022 --> 00:12:27,120
Get another tooth pulled out tomorrow.
I'll pick you up.

307
00:12:27,145 --> 00:12:29,701
Do you hear yourself right now?

308
00:12:29,726 --> 00:12:31,618
It's like you're addicted to this job.

309
00:12:31,644 --> 00:12:33,869
I'm not addicted.
I can stop any time I wanted to.

310
00:12:33,894 --> 00:12:34,960
Stop now.

311
00:12:34,985 --> 00:12:36,350
I don't want to.

312
00:12:36,856 --> 00:12:38,322
You have to.

313
00:12:38,419 --> 00:12:39,485
Fine.

314
00:12:39,510 --> 00:12:40,857
But not yet.

315
00:12:41,928 --> 00:12:43,277
'Cause I don't want to.

316
00:12:43,302 --> 00:12:48,787
'Cause I don't want to.

317
00:12:49,575 --> 00:12:50,980
Ah, there he is.

318
00:12:51,005 --> 00:12:53,397
The place is packed.
Everybody's waiting for you.

319
00:12:53,422 --> 00:12:56,816
Actually, Enzo, I got some bad news.

320
00:12:56,896 --> 00:12:57,956
I got to quit.

321
00:12:57,981 --> 00:13:01,967
No. Those people need you. I need you.

322
00:13:02,325 --> 00:13:03,940
All right. One more time.

323
00:13:05,857 --> 00:13:08,464
Okay, Dad. Teleprompter's ready to go.

324
00:13:08,489 --> 00:13:10,063
Here is your headset.

325
00:13:10,089 --> 00:13:12,797
I am so blessed to
have a father like you.

326
00:13:12,954 --> 00:13:14,634
All right.

327
00:13:21,001 --> 00:13:24,136
Ladies and gentlemen,
please welcome fill-in bartender

328
00:13:24,161 --> 00:13:25,991
and full-time life changer,

329
00:13:26,016 --> 00:13:30,644
the inspirational speaker
from Massapequa, Kevin Gable!

330
00:13:34,030 --> 00:13:35,630
Wow!

331
00:13:35,655 --> 00:13:38,428
Wow!

332
00:13:38,453 --> 00:13:41,251
Wow. Thank you.

333
00:13:41,277 --> 00:13:42,776
Thank you!

334
00:13:42,801 --> 00:13:44,497
Thank you all!

335
00:13:44,522 --> 00:13:46,003
Thank you all!

336
00:13:48,655 --> 00:13:52,580
Just six days ago, a man came to me.

337
00:13:53,064 --> 00:13:55,064
He was having trouble at work.

338
00:13:55,193 --> 00:13:57,653
Today, that man is a millionaire.

339
00:13:57,678 --> 00:13:58,877
Tim, stand on up.

340
00:13:58,902 --> 00:14:00,039
Tim, get on up!

341
00:14:04,869 --> 00:14:06,836
Now, that is what's possible

342
00:14:06,861 --> 00:14:09,578
when we all flip that
switch in our heads.

343
00:14:09,603 --> 00:14:11,436
We all have it. You have it.

344
00:14:11,461 --> 00:14:12,843
You have it. You have it. You have it.

345
00:14:12,868 --> 00:14:14,098
You, you, you, you, you.

346
00:14:14,130 --> 00:14:15,251
I'm not sure about you.

347
00:14:15,276 --> 00:14:17,858
I'm kidding. He's got it.

348
00:14:17,883 --> 00:14:19,628
He's got it.

349
00:14:19,652 --> 00:14:22,367
Now, I've asked you all to
fill out your dream cards,

350
00:14:22,393 --> 00:14:25,886
and what I'm gonna do now is
show you how we get those dreams

351
00:14:25,911 --> 00:14:27,843
from your head to the paper...

352
00:14:27,869 --> 00:14:29,132
To the universe.

353
00:14:29,157 --> 00:14:30,982
Oh! To the universe! Do you hear that?

354
00:14:31,007 --> 00:14:33,234
That's Kendra.
Give it up for her. Let her know.

355
00:14:33,259 --> 00:14:35,116
All right. Now, follow me,

356
00:14:35,141 --> 00:14:37,716
because it's time to make
those dreams a reality.

357
00:14:37,740 --> 00:14:39,337
Let's go.

358
00:14:39,677 --> 00:14:43,527
The man you're about to
meet is a loser's loser.

359
00:14:43,953 --> 00:14:47,265
He lives in a garage much like, say,

360
00:14:47,290 --> 00:14:49,774
a lawnmower or an old pair of skis.

361
00:14:50,123 --> 00:14:51,872
Hello. I am Chale.

362
00:14:53,958 --> 00:14:55,683
Now, according to his dream card,

363
00:14:55,708 --> 00:14:57,341
what's holding him back is fear.

364
00:14:57,366 --> 00:15:01,854
Well, today, we'll make
that fear disappear.

365
00:15:06,227 --> 00:15:07,951
Step on the coals, Chale.

366
00:15:08,364 --> 00:15:10,508
But they are on fire.

367
00:15:10,533 --> 00:15:11,508
Look at me.

368
00:15:11,533 --> 00:15:13,221
You can do it, son.

369
00:15:13,972 --> 00:15:16,887
I can do it.

370
00:15:27,944 --> 00:15:29,804
Hey, here is your last check.

371
00:15:29,829 --> 00:15:31,641
Uh, I got a little gravy on it,

372
00:15:31,666 --> 00:15:34,750
but you can still make out the numbers.

373
00:15:35,409 --> 00:15:38,362
Hey, Dad, I'm sorry
this didn't work out.

374
00:15:38,459 --> 00:15:40,448
Are you kidding me? It's my fault.

375
00:15:40,473 --> 00:15:42,089
I got distracted.

376
00:15:42,114 --> 00:15:44,301
Please don't that happen
to you, all right?

377
00:15:44,326 --> 00:15:45,584
Just focus on your studies,

378
00:15:45,609 --> 00:15:47,581
and if Mom asks,
I told you that two weeks ago.

379
00:15:48,022 --> 00:15:49,540
- Okay.
- All right.

380
00:15:49,565 --> 00:15:50,831
- Love you.
- All right. Love you.

381
00:15:50,856 --> 00:15:52,046
Okay.

382
00:15:52,419 --> 00:15:55,034
Kevin, you were right.

383
00:15:55,059 --> 00:15:57,942
I'm done looking out,
and I'm ready to look in.

384
00:15:57,967 --> 00:15:59,331
Can you talk?

385
00:16:00,735 --> 00:16:03,266
Actually, you know, I don't...
I don't even work here anymore.

386
00:16:03,560 --> 00:16:04,618
Um...

387
00:16:05,137 --> 00:16:09,095
but I guess I could take a
couple seconds and we could talk.

388
00:16:09,120 --> 00:16:11,158
- Yeah?
- Yeah. All right.

389
00:16:13,008 --> 00:16:15,392
So, you think I should try to reconnect

390
00:16:15,417 --> 00:16:17,037
with my biological father?

391
00:16:17,352 --> 00:16:20,420
Beth, some people run away to hide.

392
00:16:20,652 --> 00:16:23,253
Some run away to be found.

393
00:16:25,472 --> 00:16:27,169
He moved to Bangkok.

394
00:16:27,808 --> 00:16:29,743
I'm gonna say he's a hider.

395
00:16:30,017 --> 00:16:32,227
Excuse me.

396
00:16:32,252 --> 00:16:33,281
Okay.

397
00:16:33,307 --> 00:16:35,670
Hello? Hey, where are you?

398
00:16:35,695 --> 00:16:37,376
You were supposed to
be home an hour ago.

399
00:16:37,401 --> 00:16:40,211
I'm... I'm at Enzo's, but don't worry.
I quit.

400
00:16:40,236 --> 00:16:41,736
The job was just getting in the way

401
00:16:41,761 --> 00:16:43,492
of me connecting with
the people that need me.

402
00:16:43,517 --> 00:16:45,695
Oh, you are out of control.

403
00:16:45,720 --> 00:16:48,204
Honey, relax. I figured
it out, all right?

404
00:16:48,229 --> 00:16:49,824
Look, I'm not a bartender.

405
00:16:49,893 --> 00:16:51,433
I'm a life coach.

406
00:16:51,458 --> 00:16:53,213
A life coach?

407
00:16:53,238 --> 00:16:55,528
And don't worry.
I can see clients at the house now.

408
00:16:55,553 --> 00:16:57,567
By the way, do we have room
in the backyard for hot coals?

409
00:16:59,373 --> 00:17:02,499
Ooh, Phyllis is here.
Oh, she's in tears. I got to go. Okay.

410
00:17:02,524 --> 00:17:04,796
Phyllis, just grab a gin fizz.
I'll meet you at the booth.

411
00:17:04,821 --> 00:17:06,756
I want to hear everything.

412
00:17:13,605 --> 00:17:14,837
What's going on?

413
00:17:14,862 --> 00:17:18,110
First of all,
everyone in this room loves you.

414
00:17:21,102 --> 00:17:22,328
Okay.

415
00:17:22,527 --> 00:17:28,352
But, honey, you have a problem,
and this in an intervention.

416
00:17:33,458 --> 00:17:36,025
Um, Kyle, would you
like to start us off?

417
00:17:36,050 --> 00:17:37,856
Absolutely.

418
00:17:45,395 --> 00:17:49,333
Kevin, you're my
brother, and I love you.

419
00:17:50,089 --> 00:17:52,222
I followed your advice
and broke into the office

420
00:17:52,247 --> 00:17:55,722
of the Director of
Operations at Citi Field.

421
00:17:56,262 --> 00:17:59,069
I did not get the job.

422
00:17:59,831 --> 00:18:02,189
In fact, I was thrown
out of the building.

423
00:18:02,641 --> 00:18:06,676
Now, I am no longer allowed
to purchase tickets to games.

424
00:18:06,987 --> 00:18:11,029
They also have my picture up
at all the entrances and exits.

425
00:18:11,488 --> 00:18:14,095
Not cool, bro. Get help. I love you.

426
00:18:16,421 --> 00:18:17,965
Guys.

427
00:18:18,131 --> 00:18:22,093
Kevin, I don't even know
who you are anymore, man.

428
00:18:22,198 --> 00:18:25,082
You know, this new, helpful,
positive attitude you have,

429
00:18:25,107 --> 00:18:28,012
it just... it makes
me want to punch you.

430
00:18:29,779 --> 00:18:32,972
If I get any more late night, crazy-ass,

431
00:18:32,997 --> 00:18:34,800
Confucius messages on my phone,

432
00:18:34,824 --> 00:18:36,488
I'm gonna have to block your number.

433
00:18:36,513 --> 00:18:38,446
I have to admit,
I kind of like the messages.

434
00:18:38,471 --> 00:18:39,523
Thank you, Mott.

435
00:18:39,548 --> 00:18:40,764
Come on, Mott.

436
00:18:40,789 --> 00:18:42,633
It's the same stuff you
get from a fortune cookie.

437
00:18:42,658 --> 00:18:44,625
Not true. Each of those are tailor-made

438
00:18:44,650 --> 00:18:46,049
specifically for your needs.

439
00:18:46,074 --> 00:18:47,907
Dad, that's not true.

440
00:18:48,096 --> 00:18:49,446
You're pulling them from the Internet

441
00:18:49,471 --> 00:18:51,115
and testing them on me at work.

442
00:18:51,626 --> 00:18:52,949
She's a liar.

443
00:18:53,728 --> 00:18:55,294
Chale?

444
00:18:55,793 --> 00:18:57,232
I have to be honest.

445
00:18:57,257 --> 00:18:59,807
I just came in here to watch TV.

446
00:19:00,237 --> 00:19:03,204
You guys just can't handle it,
but I help people.

447
00:19:03,229 --> 00:19:03,981
That's what I do.

448
00:19:04,006 --> 00:19:05,814
Well, we thought you might say that.

449
00:19:06,032 --> 00:19:07,671
Tim? Liz?

450
00:19:07,696 --> 00:19:10,097
Oh, good. I'm glad you guys are here.

451
00:19:10,122 --> 00:19:12,278
They can back me up. Tim, tell them.

452
00:19:12,489 --> 00:19:15,677
You told me to tell my boss
it was my way or the highway.

453
00:19:15,702 --> 00:19:19,084
Now I'm living in my PT
Cruiser just off the highway.

454
00:19:19,912 --> 00:19:22,974
And you told me to dump my
boyfriend and my friends.

455
00:19:22,999 --> 00:19:25,730
Now I'm alone in my
apartment with my cat.

456
00:19:25,840 --> 00:19:27,273
You know what I'm hearing?

457
00:19:27,298 --> 00:19:29,995
Someone who's lonely and someone
who needs a place to stay.

458
00:19:30,020 --> 00:19:31,781
Peanut butter, meet jelly.

459
00:19:33,254 --> 00:19:36,544
Sweetheart, please accept this help.

460
00:19:36,712 --> 00:19:39,235
If you don't, all of these people

461
00:19:39,260 --> 00:19:41,594
don't want to be around you anymore.

462
00:19:44,484 --> 00:19:46,083
A frown when it's turned upside down...

463
00:19:46,108 --> 00:19:47,846
No, no.

464
00:19:51,740 --> 00:19:53,622
In life, you're gonna find a
path less traveled you should...

465
00:19:53,647 --> 00:19:54,959
Don't... Don't do it.

466
00:19:54,984 --> 00:19:57,352
I'm gonna have to return the smoker.

467
00:20:00,175 --> 00:20:01,994
Okay. I got a problem.

468
00:20:03,642 --> 00:20:05,674
But I don't know what to do.

469
00:20:07,881 --> 00:20:11,606
- Synced and corrected by martythecrazy -
- www.addic7ed.com -

470
00:20:14,711 --> 00:20:17,997
Wow, babe. That's like the 50th
one in a row that you've caught.

471
00:20:18,022 --> 00:20:20,265
Yes. I can catch a ball.

472
00:20:21,919 --> 00:20:25,603
I just wanted to prove to you
that I'm not some uncoordinated idiot.

473
00:20:26,928 --> 00:20:30,323
You know, it might be my
dad who makes you nervous.

474
00:20:30,538 --> 00:20:33,820
It was a freak accident.
The light got in my eyes.

475
00:20:34,982 --> 00:20:38,667
Okay. Baby, you know, it's okay.
He can intimidate people.

476
00:20:38,692 --> 00:20:41,874
Darling, he does not intimidate me.

477
00:20:42,786 --> 00:20:44,319
What's up, guys?

478
00:20:44,343 --> 00:20:45,382
Aah!

479
00:20:47,215 --> 00:20:50,413
Oh, no! Not again!
Mom, can you get the ice pack?

480
00:20:50,438 --> 00:20:52,844
And a little club
soda. We got a bleeder.

