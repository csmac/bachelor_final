﻿1
00:31:14,918 --> 00:31:18,714
Sir, the interrogation
reports on the American prisoners...

2
00:31:18,964 --> 00:31:21,425
...captured in our victory at Kasserine.

3
00:31:21,508 --> 00:31:23,218


4
00:31:23,469 --> 00:31:27,097
They are not very good soldiers,
these Americans.

5
00:31:29,183 --> 00:31:31,185
I'm not so sure...

6
00:31:32,311 --> 00:31:34,313
...after only one battle.

7
00:31:36,482 --> 00:31:40,778
Their tanks were no match for our guns.

8
00:31:41,278 --> 00:31:43,405
And neither was their leadership, sir.

9
00:31:47,895 --> 00:31:51,649
At Kasserine they were not
under American command.

10
00:31:51,899 --> 00:31:54,401
They were under the
British General Anderson.

11
00:31:55,110 --> 00:32:00,532
British commanders and American troops...
the worst of everything!

12
00:32:01,367 --> 00:32:05,204
I remind you that Montgomery
is a British Commander.

13
00:32:06,080 --> 00:32:08,832
And he has driven us half
way across Africa.

14
00:32:09,625 --> 00:32:13,629
Anyway, we have met the Americans
for the first time and defeated them.

15
00:32:14,004 --> 00:32:15,673
I'm optimistic.

16
00:32:15,756 --> 00:32:18,842
You can afford to be an optimist, I can't.

17
00:32:30,688 --> 00:32:34,733
There's an opportunity here now
for us to mount an offensive.

18
00:39:42,953 --> 00:39:45,330
Captain Steiger reporting, General.

19
00:39:46,081 --> 00:39:47,582
Come with me, Steiger.

20
00:40:01,805 --> 00:40:05,767
Field Marshal Rommel...
I hope you are feeling better...

21
00:40:07,811 --> 00:40:12,899
Captain Steiger has been assigned
to research General Patton.

22
00:40:12,983 --> 00:40:16,153
Very well. What do you have for me?

23
00:40:22,993 --> 00:40:24,870
General Patton comes from
a military family.

24
00:40:24,953 --> 00:40:27,289
His grandfather was a hero
of the American Civil War.

25
00:40:27,372 --> 00:40:32,377
He was educated at the
Virginia Military Institute and West Point.

26
00:40:32,669 --> 00:40:35,547
You're not telling me anything
about the man.

27
00:40:36,590 --> 00:40:39,468
He writes poetry and believes
in reincarnation.

28
00:40:40,260 --> 00:40:43,180
He's one of the richest officers
in the American Army.

29
00:40:43,930 --> 00:40:46,975
He prays on his knees,
but curses like a stable boy.

30
00:40:47,684 --> 00:40:50,729
He has one standing order.
Always take the offensive, never dig in

31
00:40:59,488 --> 00:41:02,282
In 15 minutes we meet with the Fuhrer.

32
00:41:02,991 --> 00:41:06,578
He will want to know how you plan
to deal with Patton's forces.

33
00:41:11,291 --> 00:41:13,335
I will attack and annihilate him!

34
00:41:15,545 --> 00:41:17,547
Before he does the same to me.

35
00:54:39,473 --> 00:54:42,393
It is obvious that
North Africa will soon be lost.

36
00:54:42,476 --> 00:54:45,271
We must now anticipate
the enemy's next move.

37
00:54:45,688 --> 00:54:50,776
I shall expect a staff report within 24 hours.
That will be all.

38
00:54:57,575 --> 00:55:01,120
Steiger, you have said nothing.

39
00:55:01,329 --> 00:55:03,122
I wasn't asked anything.

40
00:55:03,205 --> 00:55:07,835
I'm asking you now.
You think Patton will attack Sardinia?

41
00:55:09,211 --> 00:55:10,338
And why not?

42
00:55:11,630 --> 00:55:15,801
Patton, sir... is a military historian.

43
00:55:16,385 --> 00:55:21,057
He knows that Sicily, not Sardinia,
has always been the key to Italy.

44
00:55:21,807 --> 00:55:26,896
If Patton has his way he will attack
Sicily at Syracuse, as the Athenians did.

45
00:55:26,979 --> 00:55:29,106
Steiger, this is the twentieth century!

46
00:55:29,315 --> 00:55:32,985
But you must understand, sir...
Patton is a sixteenth-century man

47
00:55:33,444 --> 00:55:34,278
May I read an example?

48
00:55:37,990 --> 00:55:42,411
“On a dark street in New York 1922,
wearing white tie and tails...

49
00:55:42,620 --> 00:55:46,540
“...Patton saw three men pushing
a young girl into the back of a truck.

50
00:55:46,749 --> 00:55:49,251
“He leaped out of his car,
produced a revolver...

51
00:55:49,460 --> 00:55:53,005
“...and forced the men at gunpoint
to release the girl”.

52
00:55:55,716 --> 00:55:58,969
It turned out that the girl was the
fiancée of one of the men.

53
00:55:59,887 --> 00:56:02,348
They were merely helping
her into the truck.

54
00:56:07,269 --> 00:56:09,063
What could be more revealing?

55
00:56:09,313 --> 00:56:11,148
I don't know what you're talking about.

56
00:56:11,899 --> 00:56:17,029
Don Quixote encounters six merchants
of Toledo and saves Dulcinea's virtue!

57
00:56:17,613 --> 00:56:18,989
Who the devil is Dulcinea?

58
00:56:19,865 --> 00:56:20,991
Don't you see, sir?

59
00:56:21,283 --> 00:56:25,746
Patton is a romantic warrior
lost in contemporary times.

60
00:56:26,163 --> 00:56:28,249
The secret of Patton is the past.

61
00:56:29,583 --> 00:56:34,046
He'll urge an attack on Sicily
because that's what the Athenians did!

62
01:04:13,672 --> 01:04:16,800
Here's the gangster Patton,
landing at Gela with his Seventh Army.

63
01:04:17,426 --> 01:04:19,887
This film was captured after the landing.

64
01:04:24,558 --> 01:04:26,685
I didn't realize he was so tall.

65
01:04:27,603 --> 01:04:29,104
Over six feet.

66
01:04:38,530 --> 01:04:40,616
He's constantly giving
personal commands.

67
01:04:40,824 --> 01:04:44,912
Obviously they now have two prima donnas
in Sicily... Montgomery and Patton!

68
01:04:45,287 --> 01:04:47,623


69
01:04:54,171 --> 01:04:56,340
There's another three-star general.

70
01:04:56,924 --> 01:05:00,344
General Bradley...Commander
of the American ll Corps.

71
01:05:00,802 --> 01:05:02,554
He looks like a common soldier

72
01:05:03,305 --> 01:05:05,557
He is most capable, yet unpretentious.

73
01:05:05,849 --> 01:05:07,351
Unusual for a general.

74
01:05:09,436 --> 01:05:10,437
Sorry...

75
01:10:31,457 --> 01:10:33,084
Sir, the Americans have taken Palermo!

76
01:10:33,668 --> 01:10:34,794
Damn!

77
01:46:03,420 --> 01:46:04,421
Malta?

78
01:46:04,630 --> 01:46:05,673
Yes, sir.

79
01:46:06,298 --> 01:46:08,425
Malta as a base...

80
01:46:09,927 --> 01:46:12,930
...then southern Greece.
That is possible.

81
01:46:14,098 --> 01:46:15,974
Get me Field Marshal Keitel.

82
01:47:36,430 --> 01:47:39,433
There is only one
reason for him to be in Cairo.

83
01:47:39,725 --> 01:47:43,103
To confer with the Greek and
Yugoslav governments in exile.

84
01:47:43,479 --> 01:47:47,024
Let the Italians garrison Italy,
it's their country.

85
01:47:47,483 --> 01:47:52,070
We'll need our German troops to
reinforce Crete and the Greek coast...

86
01:47:52,279 --> 01:47:54,531
...if Patton strikes from Egypt!

87
01:48:01,914 --> 01:48:03,999
I have some new information, sir.

88
01:48:04,583 --> 01:48:09,421
Patton is under severe criticism.
He may even be court-martialed.

89
01:48:10,047 --> 01:48:11,882
He slapped an enlisted man.

90
01:48:12,007 --> 01:48:14,426
You believe their newspapers?

91
01:48:15,427 --> 01:48:19,014
Would they sacrifice their best commander
because he slapped a soldier?

92
02:04:55,760 --> 02:04:57,178
What's this activity near Coutances?

93
02:04:57,762 --> 02:05:01,224
Enemy armored forces driving
through our defenses at Lessay.

94
02:05:01,307 --> 02:05:06,646
“American tanks moving rapidly,
slicing through to the rear areas”.

95
02:05:07,271 --> 02:05:10,066
This sounds like Patton, Field Marshal.

96
02:05:10,399 --> 02:05:11,567
Patton is in England.

97
02:05:12,109 --> 02:05:13,319
Do we know this?

98
02:05:14,070 --> 02:05:17,907
The landing in Normandy is
merely a diversionary maneuver.

99
02:05:18,825 --> 02:05:22,203
The real invasion will come
at Calais and Patton will lead it.

100
02:05:22,495 --> 02:05:27,166
The Fuhrer says that the Fifteenth Army
is not to be moved to Normandy.

101
02:05:27,375 --> 02:05:30,461
Those men are sitting on the beach at
Calais throwing pebbles at each other...

102
02:05:30,670 --> 02:05:34,215
...while our men are being
slaughtered in Normandy.

103
02:05:34,465 --> 02:05:38,761
The Fifteenth Army is waiting for
Patton at Calais and he will land there.

104
02:05:41,305 --> 02:05:45,226
You seem perfectly willing to
accept this nonsense, Jodl.

105
02:05:46,310 --> 02:05:47,311
Why?

106
02:05:50,356 --> 02:05:53,609
Because I am not prepared
to dispute the Fuhrer.

107
02:36:14,637 --> 02:36:16,305

This is the end...

108
02:36:16,889 --> 02:36:18,224
...the end.

109
02:36:22,728 --> 02:36:27,274
Hurry, Steiger. I want everything destroyed.
Papers, maps, everything!

110
02:36:27,441 --> 02:36:31,570
Everything will be destroyed, General,
that I can promise you.

111
02:36:31,821 --> 02:36:36,826
I'll never let the Russians take me!
I'll kill myself, like the Fuhrer!

112
02:36:39,704 --> 02:36:41,288
He, too, will be destroyed.

113
02:36:41,622 --> 02:36:45,584
The absence ofwar will kill him.

114
02:36:48,462 --> 02:36:50,548
The pure warrior...

115
02:36:53,134 --> 02:36:56,053
...a magnificent anachronism...