1
00:00:13,700 --> 00:00:19,600
FAITHFUL HEART

2
00:01:39,000 --> 00:01:42,700
Marie was a picked girl ...

3
00:02:18,500 --> 00:02:25,100
... that the father and the mother Rochon raised hard, overwhelming her with the work.

4
00:04:42,400 --> 00:04:48,000
More than the hard work, Marie suffered for the wishes of this man

5
00:04:48,200 --> 00:04:51,600
that did not inspire him more than fear.

6
00:05:07,600 --> 00:05:13,800
The whole neighborhood feared Petit Paul that he did not have the best reputations.

7
00:05:33,400 --> 00:05:41,100
But true love inspired poor Marie every night.

8
00:07:00,700 --> 00:07:03,200
Where are you going like this?

9
00:07:05,500 --> 00:07:08,600
I'm going to look for oil.

10
00:09:55,500 --> 00:10:01,100
Petit Paul did not have a completely calm conscience.

11
00:12:15,300 --> 00:12:20,800
I'm scared. The father and mother want to give me Petit Paul.

12
00:12:41,700 --> 00:12:45,800
I need to change the air. I take the girl.

13
00:12:45,900 --> 00:12:50,000
We'll have fun at my sister's house, in a little town I know.

14
00:14:04,200 --> 00:14:07,900
I have something to tell you, boss!

15
00:15:03,400 --> 00:15:07,300
I know Marie is not your daughter ...

16
00:15:37,200 --> 00:15:45,400
It's Jean, a guy who walks with compliments. It is said that he and your Marie understand each other.

17
00:15:53,300 --> 00:15:57,000
We love each other. I'm going to marry Marie.

18
00:17:33,900 --> 00:17:39,100
Forever

19
00:17:53,400 --> 00:18:00,200
You take those of Villadiego, ungrateful! Pick up your things, you go with Petit Paul.

20
00:18:46,400 --> 00:18:54,100
Jean waited in vain for her friend in her usual place.

21
00:25:04,500 --> 00:25:08,900
Do you know where Marie, the cabaret maid is?

22
00:25:22,300 --> 00:25:25,500
She left ... she left ...

23
00:27:03,600 --> 00:27:08,000
Let's get married in the little horses.

24
00:27:31,700 --> 00:27:34,600
She's gone ... with Petit Paul.

25
00:38:54,800 --> 00:39:00,300
Petit Paul had escaped the persecutions of justice.

26
00:39:00,500 --> 00:39:05,800
Jean, held to be guilty, had just served one year in prison.

27
00:39:55,400 --> 00:39:59,800
Faithful heart ...

28
00:41:00,000 --> 00:41:04,400
And in the afternoon, nothing ...

29
00:41:05,200 --> 00:41:08,900
One morning, finally ...

30
00:43:04,100 --> 00:43:08,500
Because of that child, he did not dare to address her.

31
00:43:41,200 --> 00:43:44,500
But he followed her ...

32
00:45:37,300 --> 00:45:39,400
It's her son?

33
00:46:29,200 --> 00:46:32,000
Who does She live with?

34
00:46:35,000 --> 00:46:40,000
With Petit Paul ... but he makes her quite miserable.

35
00:47:54,900 --> 00:47:57,300
I  have no money...

36
00:49:04,200 --> 00:49:06,500
Hurry, go quickly!

37
00:50:11,800 --> 00:50:17,400
Go away! Go away! If he finds you here, he will kill us all! He always comes drunk!

38
00:52:08,900 --> 00:52:12,700
Petit Paul  coming! And he is drunk!

39
00:52:37,900 --> 00:52:43,000
Go away! May he be healed first! We can leave with him!

40
00:54:22,500 --> 00:54:26,800
But soon, they starting gossip.

41
00:55:04,800 --> 00:55:06,900
Look at this.

42
00:55:23,100 --> 00:55:26,200
Gossip.

43
00:56:10,400 --> 00:56:18,300
Oh! I have something to tell you that will amaze you. You don't know ...

44
00:56:34,000 --> 00:56:37,200
Jean has been seen at your wife's house.

45
00:58:18,400 --> 00:58:25,000
They have seen Jean here! If I find it again, I will give you what you deserve!

46
00:59:18,400 --> 00:59:21,100
Come on, the soup!

47
00:59:49,300 --> 00:59:55,900
I don't have time to cook. I spent the whole morning with the child in the hospital.

48
01:00:08,000 --> 01:00:10,800
I'll go eat somewhere else.

49
01:02:03,900 --> 01:02:12,500
When Petit Paul was absent, the invalid made Jean a signal.

50
01:03:22,100 --> 01:03:28,300
While Marie recovered the hope of seeing her son alive ...

51
01:09:15,000 --> 01:09:17,600
... Petit Paul, on the other hand ...

52
01:12:20,300 --> 01:12:24,700
Jean has just been reunited with your wife. Now he is in his house.

53
01:14:00,300 --> 01:14:05,300
Jean is at Marie's house. Even letters are written

54
01:14:05,400 --> 01:14:07,600
of love for the walls of the neighborhood.

55
01:15:27,900 --> 01:15:30,100
It's necessary to leave.

56
01:26:07,000 --> 01:26:10,500
Love makes everything forget.

57
01:26:39,000 --> 01:26:42,800
Forever

