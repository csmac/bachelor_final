﻿1
00:00:03,586 --> 00:00:05,588
This ain't for the best

2
00:00:05,588 --> 00:00:08,758
My reputation's never been worse, so

3
00:00:08,758 --> 00:00:14,055
You must like me for me

4
00:00:14,055 --> 00:00:16,641
We can't make any promises

5
00:00:16,641 --> 00:00:18,852
Now, can we, babe?

6
00:00:18,852 --> 00:00:23,356
But you can make me a drink

7
00:00:23,356 --> 00:00:24,983
Dive bar on the East Side

8
00:00:24,983 --> 00:00:26,025
Where you at?

9
00:00:26,025 --> 00:00:27,527
Phone lights up my nightstand

10
00:00:27,527 --> 00:00:28,486
In the black

11
00:00:28,486 --> 00:00:30,071
Come here, you can meet me

12
00:00:30,071 --> 00:00:33,491
In the back

13
00:00:33,491 --> 00:00:35,118
Dark jeans and your Nikes

14
00:00:35,118 --> 00:00:36,244
Look at you

15
00:00:36,244 --> 00:00:38,371
Oh damn, never seen that color blue

16
00:00:38,371 --> 00:00:42,625
Just think of the fun things we could do

17
00:00:42,625 --> 00:00:44,335
'Cause I like you

18
00:00:44,335 --> 00:00:46,129
This ain't for the best

19
00:00:46,129 --> 00:00:49,340
My reputation's never been worse, so

20
00:00:49,340 --> 00:00:52,677
You must like me for me

21
00:00:52,677 --> 00:00:54,596
Yeah, I want you

22
00:00:54,596 --> 00:00:57,098
We can't make any promises

23
00:00:57,098 --> 00:00:59,225
Now, can we, babe?

24
00:00:59,225 --> 00:01:03,521
But you can make me a drink

25
00:01:03,521 --> 00:01:06,024
Is it cool that I said all that?

26
00:01:06,024 --> 00:01:08,526
Is it chill that you're in my head?

27
00:01:08,526 --> 00:01:12,113
'Cause I know that it's delicate

28
00:01:12,113 --> 00:01:13,615
<i>Delicate</i>

29
00:01:13,615 --> 00:01:16,201
Is it cool that I said all that?

30
00:01:16,201 --> 00:01:18,661
Is it too soon to do this yet?

31
00:01:18,661 --> 00:01:22,749
'Cause I know that it's delicate

32
00:01:22,749 --> 00:01:25,919
Isn't it, Isn't it, Isn't it?

33
00:01:25,919 --> 00:01:27,837
Isn't it?

34
00:01:27,837 --> 00:01:30,924
Isn't it, Isn't it, Isn't it?

35
00:01:30,924 --> 00:01:32,300
Isn't it

36
00:01:32,300 --> 00:01:34,135
Delicate

37
00:01:34,135 --> 00:01:35,845
Third floor on the west side

38
00:01:35,845 --> 00:01:37,096
Me and you

39
00:01:37,096 --> 00:01:38,848
Handsome, you're a mansion with a view

40
00:01:38,848 --> 00:01:40,809
Do the girls back home touch you

41
00:01:40,809 --> 00:01:44,187
Like I do?

42
00:01:44,187 --> 00:01:45,605
Long night, with your hands

43
00:01:45,605 --> 00:01:46,815
Up in my hair

44
00:01:46,815 --> 00:01:49,275
Echoes of your footsteps on the stairs

45
00:01:49,275 --> 00:01:50,318
Stay here, honey

46
00:01:50,318 --> 00:01:53,363
I don't wanna share

47
00:01:53,363 --> 00:01:55,114
'Cause I like you

48
00:01:55,114 --> 00:01:56,825
This ain't for the best

49
00:01:56,825 --> 00:02:00,119
My reputation's never been worse, so

50
00:02:00,119 --> 00:02:03,540
You must like me for me

51
00:02:03,540 --> 00:02:05,333
Yeah, I want you

52
00:02:05,333 --> 00:02:07,919
We can't make any promises

53
00:02:07,919 --> 00:02:10,004
Now, can we, babe?

54
00:02:10,004 --> 00:02:14,300
But you can make me a drink

55
00:02:14,300 --> 00:02:16,803
Is it cool that I said all that?

56
00:02:16,803 --> 00:02:19,305
Is it chill that you're in my head?

57
00:02:19,305 --> 00:02:22,892
'Cause I know that it's delicate

58
00:02:22,892 --> 00:02:24,394
Delicate

59
00:02:24,394 --> 00:02:27,021
Is it cool that I said all that?

60
00:02:27,021 --> 00:02:29,482
Is it too soon to do this yet?

61
00:02:29,482 --> 00:02:33,570
'Cause I know that it's delicate

62
00:02:33,570 --> 00:02:36,739
Isn't it, Isn't it, Isn't it?

63
00:02:36,739 --> 00:02:38,658
Isn't it?

64
00:02:38,658 --> 00:02:41,744
Isn't it, Isn't it, Isn't it?

65
00:02:41,744 --> 00:02:43,121
Isn't it

66
00:02:43,121 --> 00:02:45,123
Delicate

67
00:02:45,123 --> 00:02:47,625
Sometimes I wonder

68
00:02:47,625 --> 00:02:50,336
When you sleep

69
00:02:50,336 --> 00:02:55,258
Are you ever dreaming of me?

70
00:02:55,258 --> 00:03:00,430
Sometimes when I look into your eyes

71
00:03:00,430 --> 00:03:02,932
I pretend you're mine

72
00:03:02,932 --> 00:03:06,728
All the damn time

73
00:03:06,728 --> 00:03:07,770
'Cause I like you

74
00:03:07,770 --> 00:03:09,939
Is it cool that I said all that?

75
00:03:09,939 --> 00:03:12,442
Is it chill that you're in my head?

76
00:03:12,442 --> 00:03:16,029
'Cause I know that it's delicate

77
00:03:16,029 --> 00:03:17,530
Delicate! Yeah! I want you

78
00:03:17,530 --> 00:03:20,116
Is it cool that I said all that?

79
00:03:20,116 --> 00:03:22,577
Is it too soon to do this yet?

80
00:03:22,577 --> 00:03:26,039
'Cause I know that it's delicate

81
00:03:26,039 --> 00:03:26,998
Delicate

82
00:03:26,998 --> 00:03:28,041
'Cause I like you

83
00:03:28,041 --> 00:03:29,834
Is it cool that I said all that?

84
00:03:29,834 --> 00:03:31,002
Isn't it

85
00:03:31,002 --> 00:03:31,794
Is it chill that you're in my head?

86
00:03:31,794 --> 00:03:32,921
Isn't it, Isn't it?

87
00:03:32,921 --> 00:03:34,881
'Cause I know that it's delicate

88
00:03:34,881 --> 00:03:36,799
Isn't it

89
00:03:36,799 --> 00:03:37,842
Delicate! Yeah! I want you

90
00:03:37,842 --> 00:03:39,969
Is it cool that I said all that?

91
00:03:39,969 --> 00:03:41,012
Isn't it

92
00:03:41,012 --> 00:03:41,971
Is it too soon to do this yet?

93
00:03:41,971 --> 00:03:43,097
Isn't it, Isn't it?

94
00:03:43,097 --> 00:03:45,058
'Cause I know that it's delicate

95
00:03:45,058 --> 00:03:46,225
Isn't it

96
00:03:46,225 --> 00:03:47,625
<i>Delicate</i>

