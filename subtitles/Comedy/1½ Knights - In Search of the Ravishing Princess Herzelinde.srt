﻿1
00:00:08,600 --> 00:00:10,900
Can I have your last hug?

2
00:00:21,170 --> 00:00:22,860
I used to think that

3
00:00:24,440 --> 00:00:30,050
I need to bear it being with you in the palace.

4
00:00:31,140 --> 00:00:32,120
But now,

5
00:00:33,690 --> 00:00:35,560
I can't stand it anymore.

6
00:00:36,850 --> 00:01:07,500
<i>English Subtitle by ingfakirata, P'Ann, Eveyy_jj, Clishe, Nanadc1 
>> facebook.com/ingfakirataJJ <<</i>

7
00:01:08,370 --> 00:01:12,050
What's the reason of the Forest Palace's renovation?

8
00:01:12,850 --> 00:01:14,730
I always thought that you like it.

9
00:01:16,450 --> 00:01:19,150
I'm concerned that it is quite old, not modern enough.

10
00:01:19,500 --> 00:01:21,150
I would like to change it, Your Majesty.

11
00:01:22,300 --> 00:01:23,600
But I disagree.

12
00:01:24,160 --> 00:01:26,420
Do you have any valuable memories in it, Your Majesty?

13
00:01:28,300 --> 00:01:34,450
That palace has the memories of every Crown Prince including you as well.

14
00:01:35,300 --> 00:01:36,500
No.

15
00:01:37,690 --> 00:01:39,840
It should only be you.

16
00:01:40,700 --> 00:01:43,900
But please don’t worry. I'll try to preserve its condition as much as possible.

17
00:01:44,880 --> 00:01:47,620
I'll change only the parts that need changes.

18
00:01:53,600 --> 00:01:58,450
And do you know who was the guy the Princess went out with?

19
00:01:59,920 --> 00:02:01,800
The Princess went out with his father, Your Majesty.

20
00:02:03,840 --> 00:02:05,640
Do you believe that?

21
00:02:06,530 --> 00:02:10,750
I trust my wife more than the thief who stole the car, Your Majesty.

22
00:02:19,720 --> 00:02:23,200
Nevertheless, this palace belongs to the Crown Prince.

23
00:02:23,920 --> 00:02:26,930
It is my right to do whatever I want.

24
00:02:28,000 --> 00:02:29,420
I just come to inform you.

25
00:02:31,060 --> 00:02:32,420
Excuse me, Your Majesty.

26
00:02:42,800 --> 00:02:45,260
Why does he want to break that palace down?

27
00:02:45,900 --> 00:02:49,130
The Prince used to ask me about

28
00:02:49,980 --> 00:02:54,290
the history of that palace, Your Majesty.

29
00:02:59,400 --> 00:03:04,350
I can never understand what is in his mind.

30
00:03:14,730 --> 00:03:19,750
Why does the Prince suddenly want to renovate that palace?

31
00:03:20,250 --> 00:03:22,000
I don’t know as well.

32
00:03:22,280 --> 00:03:24,450
Suddenly, the order is announced.

33
00:03:28,340 --> 00:03:31,990
Letting him use that palace frequently is already too much.

34
00:03:34,200 --> 00:03:37,760
Definitely, I will not let him do whatever he wants.

35
00:03:46,100 --> 00:03:47,620
Prince Inn.

36
00:03:52,400 --> 00:03:56,400
How dare you order to destroy the Forest Palace!

37
00:03:57,580 --> 00:04:00,410
Don’t you know how much His Majesty loves that place?

38
00:04:01,080 --> 00:04:02,400
Strange!

39
00:04:03,540 --> 00:04:05,800
There are only two persons who disagree

40
00:04:06,580 --> 00:04:07,690
His Majesty

41
00:04:08,920 --> 00:04:10,690
and The Royal Consort.

42
00:04:12,140 --> 00:04:15,800
It seems this palace is not that simple palace after all.

43
00:04:19,920 --> 00:04:21,530
People know well

44
00:04:21,530 --> 00:04:27,500
that the Forest Palace is the palace for every Crown Prince.

45
00:04:28,490 --> 00:04:32,320
As well as it is an important place for the country's history.

46
00:04:33,480 --> 00:04:37,450
Some parts of history don't deserve to be memorialized.

47
00:04:40,220 --> 00:04:43,170
Just by thinking of the hidden story of that palace,

48
00:04:45,100 --> 00:04:47,040
I feel nauseated.

49
00:04:47,900 --> 00:04:49,780
That I almost want to vomit it out.

50
00:04:51,740 --> 00:04:53,680
What are you talking about?!!

51
00:04:59,880 --> 00:05:01,770
Do you think others don’t know?

52
00:05:02,570 --> 00:05:07,750
That the Crown Princess flirted with the second Crown Prince until he's crazy for her,

53
00:05:09,040 --> 00:05:12,560
and he almost had an affair with his own brother’s wife?

54
00:05:14,050 --> 00:05:19,350
I wonder why the late Crown Prince passed away by car accident.

55
00:05:20,420 --> 00:05:23,340
Probably because he found out such disgusting affair,

56
00:05:23,340 --> 00:05:25,400
until he didn't want to live anymore.

57
00:05:27,640 --> 00:05:28,970
You don't know anything!

58
00:05:29,650 --> 00:05:31,970
How dare you speak to me like this?

59
00:05:36,000 --> 00:05:37,900
This matter is easy to figure out.

60
00:05:39,560 --> 00:05:40,750
No matter what,

61
00:05:41,920 --> 00:05:48,050
I will abolish such disgusting place from Bhutin land.

62
00:05:49,540 --> 00:05:51,320
Excuse me.

63
00:05:58,440 --> 00:05:59,970
Cocky kid.

64
00:06:01,700 --> 00:06:06,500
You'll never know that you are burning yourself.

65
00:06:11,660 --> 00:06:16,150
I have to thank the Royal Consort for covering Nakhun's matter.

66
00:06:17,020 --> 00:06:20,570
You are blamed like this, it might be giving you a hard time.

67
00:06:22,720 --> 00:06:24,340
I'm alright, Your Highness.

68
00:06:31,820 --> 00:06:35,900
I'm glad that you two become good friends.

69
00:06:39,680 --> 00:06:44,550
Nakhun had to stay alone outside the palace all this time.

70
00:06:45,840 --> 00:06:49,100
It's only me who was beside him.

71
00:06:53,280 --> 00:06:57,100
When I was out of the palace, I sometimes spared him the time.

72
00:06:59,620 --> 00:07:02,320
But life inside the palace is different.

73
00:07:03,850 --> 00:07:05,600
How is it different?

74
00:07:09,100 --> 00:07:10,150
In the palace,

75
00:07:10,490 --> 00:07:12,300
there are many regulations.

76
00:07:13,140 --> 00:07:17,000
As well as complying with traditions and culture.

77
00:07:17,650 --> 00:07:20,290
We are in front of people's eyes all the time.

78
00:07:22,210 --> 00:07:24,010
Private life?

79
00:07:24,760 --> 00:07:26,300
Forget it?

80
00:07:29,560 --> 00:07:31,620
When I was the Crown Princess,

81
00:07:32,960 --> 00:07:34,620
I was so stressed,

82
00:07:34,620 --> 00:07:38,370
til sometimes, I wanted to run away from here.

83
00:07:39,120 --> 00:07:41,120
But you couldn't do it, right?

84
00:07:44,600 --> 00:07:47,320
I'm not a brave person.

85
00:07:49,840 --> 00:07:54,050
All I could do was being patient for my kid.

86
00:07:58,460 --> 00:08:00,410
When I was out of the palace,

87
00:08:01,320 --> 00:08:03,410
I felt a bit glad.

88
00:08:07,260 --> 00:08:08,530
Think about it.

89
00:08:09,600 --> 00:08:13,900
Life with freedom is so sweet.

90
00:08:17,120 --> 00:08:19,410
To follow your dream.

91
00:08:19,850 --> 00:08:21,800
To forget about the regulations.

92
00:08:23,610 --> 00:08:26,700
To love the one I want to love.

93
00:08:36,650 --> 00:08:38,800
Actually I'm a bit lucky.

94
00:08:39,760 --> 00:08:43,800
The former Crown Prince loved me so much.

95
00:08:50,000 --> 00:08:52,080
But if I had to be you,

96
00:08:53,520 --> 00:08:54,600
to live

97
00:08:56,640 --> 00:08:59,000
with someone who doesn't love me at all,

98
00:09:01,850 --> 00:09:03,520
I couldn't bear to stay here for sure.

99
00:09:08,770 --> 00:09:10,700
I probably had divorce a long time ago.

100
00:09:16,040 --> 00:09:18,240
Don’t you know you are so strong?

101
00:09:19,780 --> 00:09:21,720
I admire you for it.

102
00:09:31,500 --> 00:09:32,820
Your Highness.

103
00:09:34,820 --> 00:09:36,260
You are here.

104
00:09:38,650 --> 00:09:41,600
I have to thank again, Your Highness.

105
00:09:44,800 --> 00:09:47,690
So, I'm leaving.

106
00:09:51,810 --> 00:09:53,370
See you Nakhun.

107
00:10:04,840 --> 00:10:06,440
What did you talk about, Mom?

108
00:10:09,010 --> 00:10:11,130
I'm helping you now.

109
00:10:25,460 --> 00:10:27,520
Have you ever had any dream?

110
00:10:27,520 --> 00:10:29,260
If you are thinking about this matter.

111
00:10:30,320 --> 00:10:33,700
I suggest you to give up.

112
00:10:34,300 --> 00:10:36,900
Tell me what is in your mind?!

113
00:10:58,650 --> 00:11:06,850
<i>"This will be the last chance.  If you tell them what you truly feel.
People will feel you and have pity on you."  *Nakhun</i>

114
00:11:17,610 --> 00:11:18,880
Inn.

115
00:11:20,560 --> 00:11:22,490
Have you ever had any dream?

116
00:11:23,740 --> 00:11:25,000
Yes, I have.

117
00:11:27,780 --> 00:11:29,330
But it's no meaning.

118
00:11:30,400 --> 00:11:32,160
If you are thinking about this matter.

119
00:11:33,200 --> 00:11:36,700
I suggest you to stop.

120
00:11:37,370 --> 00:11:38,400
Why?

121
00:11:39,640 --> 00:11:41,400
Whenever we have to stay together.

122
00:11:44,800 --> 00:11:46,520
whatever you dream about,

123
00:11:47,580 --> 00:11:49,360
It's impossible.

124
00:11:59,160 --> 00:12:01,010
You should admit

125
00:12:04,170 --> 00:12:06,410
that our marriage this time

126
00:12:08,210 --> 00:12:11,130
is changing our lives forever.

127
00:12:12,690 --> 00:12:13,860
Nope.

128
00:12:15,200 --> 00:12:16,500
No need.

129
00:12:17,170 --> 00:12:18,980
We don’t have to stay together.

130
00:12:19,860 --> 00:12:23,850
We got married and we can get divorce either.

131
00:12:26,760 --> 00:12:27,700
I know,

132
00:12:29,730 --> 00:12:31,610
you think about this all the time.

133
00:12:33,130 --> 00:12:34,200
I know

134
00:12:34,970 --> 00:12:36,530
you don’t want to stay here.

135
00:12:37,450 --> 00:12:44,050
But when I said that our marriage could change our lives forever,

136
00:12:46,940 --> 00:12:48,500
it's either to stay together

137
00:12:49,740 --> 00:12:51,330
or divorce.

138
00:12:54,090 --> 00:12:58,050
If you are still thinking about the divorce,

139
00:13:03,240 --> 00:13:05,610
I can make it happen.

140
00:13:12,130 --> 00:13:13,700
Your Highness!

141
00:13:14,180 --> 00:13:16,140
Everything is ready.

142
00:13:35,560 --> 00:13:37,230
Please believe me once.

143
00:13:40,230 --> 00:13:42,040
We are going to get an interview.

144
00:13:43,450 --> 00:13:45,040
I don’t know what you are thinking about

145
00:13:46,500 --> 00:13:47,340
but...

146
00:13:49,580 --> 00:13:50,820
I beg you,

147
00:13:53,810 --> 00:13:55,480
don’t speak about the divorce at all.

148
00:14:19,890 --> 00:14:21,970
Here comes an important question

149
00:14:21,970 --> 00:14:26,350
which people of Bhutin would like to know that

150
00:14:26,800 --> 00:14:32,800
about the heir, what is your plan about this, Your Highness?

151
00:14:35,320 --> 00:14:38,750
Probably we have to wait until our graduations.

152
00:14:39,880 --> 00:14:45,000
Those people are all waiting to see little prince and princess,Your Highness.

153
00:14:45,560 --> 00:14:46,950
And the last question,

154
00:14:47,100 --> 00:14:52,460
I would like to know about your life after marriage.

155
00:14:52,460 --> 00:14:54,970
How is it going, Your Highness?

156
00:15:16,300 --> 00:15:17,500
I...

157
00:15:19,950 --> 00:15:24,600
love my princess with all my heart.

158
00:15:28,610 --> 00:15:33,900
Although both of you have just met each other not so long, Your Highness?

159
00:15:35,530 --> 00:15:40,100
At our first meeting, I didn't believe in myself.

160
00:15:40,760 --> 00:15:44,900
It's the feeling which seems impossible.

161
00:15:48,490 --> 00:15:49,930
But it has already happened.

162
00:15:51,340 --> 00:15:53,400
So romantic, Your Highness.

163
00:15:53,780 --> 00:15:56,680
How about you, your highness?

164
00:15:58,460 --> 00:15:59,400
Divorce...

165
00:15:59,400 --> 00:16:01,810
You are saying “Divorce”, aren't you?

166
00:16:04,610 --> 00:16:07,000
No, I'm not.

167
00:16:08,160 --> 00:16:10,520
I didn't mean that.

168
00:16:11,220 --> 00:16:12,690
As we know from the very start,

169
00:16:14,250 --> 00:16:16,520
the Princess was just a normal student

170
00:16:17,130 --> 00:16:20,360
who suddenly needs to spend her life in the palace.

171
00:16:21,600 --> 00:16:23,090
At the beginning,

172
00:16:23,560 --> 00:16:26,400
she felt really confused for a period of time.

173
00:16:26,400 --> 00:16:30,100
But why does this come to the point of divorce?

174
00:16:31,420 --> 00:16:32,840
Okay.

175
00:16:32,840 --> 00:16:39,850
Actually we sometimes both used to talk about the divorce but...

176
00:16:47,740 --> 00:16:49,440
She is strong enough

177
00:16:50,700 --> 00:16:54,650
struggling with patience.

178
00:16:57,860 --> 00:17:03,350
She knows that dreams and freedom

179
00:17:04,650 --> 00:17:06,370
only exist outside the palace.

180
00:17:10,210 --> 00:17:14,950
I want you all to continue cheering her on

181
00:17:15,680 --> 00:17:20,300
to overcome this situation same as what I'm doing right now.

182
00:17:23,480 --> 00:17:24,690
Thank you.

183
00:17:31,130 --> 00:17:34,700
How did you let this clip release out?!

184
00:17:35,060 --> 00:17:36,760
I apologize, Your Majesty.

185
00:17:38,570 --> 00:17:42,060
And what is in Princess’ mind? She dared speak about that divorce!

186
00:17:42,410 --> 00:17:44,400
Is she planning to overthrow the dynasty?

187
00:17:45,800 --> 00:17:47,800
Please calm down, father.

188
00:17:47,800 --> 00:17:50,050
It seemed the Princess had no bad intention.

189
00:17:50,400 --> 00:17:52,980
She might be excited and it's just a slip of the tongue.

190
00:17:53,840 --> 00:17:56,320
Such an important matter to be slipped out?

191
00:17:59,320 --> 00:18:01,700
So what really happened?

192
00:18:49,780 --> 00:18:53,850
His Majesty wants your attendance now, Your Highness.

193
00:19:21,920 --> 00:19:24,540
Tell me what are you thinking?!

194
00:19:27,380 --> 00:19:29,510
I have done wrong, Your Majesty.

195
00:19:31,160 --> 00:19:34,260
Does this matter relate to the man who stayed with you that night?

196
00:19:35,800 --> 00:19:38,900
And can you tell me now who was that guy?

197
00:19:42,630 --> 00:19:45,040
I absolutely can't tell you, Your Majesty.

198
00:19:46,090 --> 00:19:47,540
You are such...

199
00:19:48,440 --> 00:19:51,350
You know well, right? If the truth is you have an affair,

200
00:19:51,650 --> 00:19:56,400
your penalty is to be discharged from the position and be deported from the palace.

201
00:19:59,500 --> 00:20:02,240
I don't have any affair, Your Majesty.

202
00:20:03,020 --> 00:20:03,860
Alright!

203
00:20:04,380 --> 00:20:06,380
No matter what, I will know the truth anyway.

204
00:20:07,130 --> 00:20:08,380
And when the time comes,

205
00:20:09,140 --> 00:20:11,380
don’t blame me for being heartless.

206
00:20:13,180 --> 00:20:14,750
What's wrong with this two?

207
00:20:15,370 --> 00:20:20,350
They got married for a long time but they always have problems.

208
00:20:20,880 --> 00:20:23,290
How can I support him to be the King?

209
00:20:25,720 --> 00:20:27,410
After this issue,

210
00:20:27,410 --> 00:20:30,100
I believe Inn will adjust himself.

211
00:20:34,040 --> 00:20:35,400
By the way,

212
00:20:38,400 --> 00:20:41,420
Inn doesn't have any qualification of being Crown Prince at all.

213
00:20:42,440 --> 00:20:45,700
Nakhun is better.

214
00:20:50,660 --> 00:20:55,700
Will you support Prince Nakhun to be the Crown Prince?

215
00:20:59,500 --> 00:21:01,510
Seriously speaking, Rachawadee.

216
00:21:02,320 --> 00:21:08,900
Prince Nakhun is more suitable to be the next King than Inn.

217
00:21:14,450 --> 00:21:20,850
Is it because your heart is still related to the Royal Consort, isn't it?

218
00:21:22,180 --> 00:21:24,290
What are you talking about, Rachawadee?

219
00:21:25,700 --> 00:21:27,700
Do you really think I don’t know?

220
00:21:30,420 --> 00:21:32,580
I will never accept this.

221
00:21:33,580 --> 00:21:37,260
If someone contends the Crown Prince position from my son,

222
00:21:39,010 --> 00:21:41,800
I swear to protect it at my best!

223
00:22:09,250 --> 00:22:11,440
I have something to talk to you.

224
00:22:14,540 --> 00:22:18,850
What kind of relationship do you have with Nakhun?.

225
00:22:19,580 --> 00:22:25,700
Prince Nakhun and I are just friends, Your Majesty.

226
00:22:27,280 --> 00:22:29,210
You think carefully before you answer me!

227
00:22:30,290 --> 00:22:32,720
This matter is very important to the dynasty.

228
00:22:34,930 --> 00:22:36,420
Yes, Your Majesty.

229
00:22:39,340 --> 00:22:45,650
Prince Nakhun and I are really just friends.

230
00:22:47,010 --> 00:22:48,980
You think only that, don't you?

231
00:22:51,780 --> 00:22:52,980
Yes.

232
00:22:54,640 --> 00:22:56,460
I can confirm, Your Majesty.

233
00:22:58,480 --> 00:23:00,050
How about Prince Nakhun?

234
00:23:00,810 --> 00:23:02,520
Does he think the same as you?

235
00:23:10,240 --> 00:23:11,480
Khun Ratree!

236
00:23:12,680 --> 00:23:17,250
From now on, don’t let Prince Nakhun meet the Princess at all.

237
00:23:17,760 --> 00:23:19,080
Yes, Your Majesty.

238
00:23:40,890 --> 00:23:42,000
Well done, Nakhun.

239
00:23:42,000 --> 00:23:43,950
If you have anything new, you propose to me again.

240
00:23:44,330 --> 00:23:45,220
Yes, Your Majesty.

241
00:23:45,780 --> 00:23:47,130
Your Majesty,

242
00:23:48,450 --> 00:23:55,600
the Great Britain has already acknowledged the cooperation of the travel and movies production.

243
00:23:57,080 --> 00:24:01,250
Prince Harry himself will come for the treaty signing.

244
00:24:01,650 --> 00:24:09,200
The Government would like to invite Your Majesty to be the President in this treaty-signing ceremony.

245
00:24:13,090 --> 00:24:15,760
May I propose, Your Majesty?

246
00:24:17,080 --> 00:24:21,400
It'll be good to allow the Crown Prince to accompany you, Your Majesty.

247
00:24:23,500 --> 00:24:25,300
I made my decision.

248
00:24:25,300 --> 00:24:27,720
Nakhun will go with me.

249
00:24:31,010 --> 00:24:32,780
But this is the big event.

250
00:24:33,210 --> 00:24:37,450
It'd better for the Crown Prince to accompany you, Your Majesty.

251
00:24:38,340 --> 00:24:39,250
I have my own reason.

252
00:24:39,730 --> 00:24:43,620
Firstly, this treaty has been made possible by Prince Nakhun.

253
00:24:45,140 --> 00:24:46,140
Secondly,

254
00:24:46,610 --> 00:24:49,010
the Crown Prince has some things to deal with.

255
00:24:49,820 --> 00:24:53,010
You have to find out who's that guy the Princess stayed with that night.

256
00:24:54,500 --> 00:24:56,780
If you are unable to take care of your own wife,

257
00:24:57,380 --> 00:24:59,260
you can't do anything else.

258
00:25:03,340 --> 00:25:05,010
Get yourself ready for this, Nakhun.

259
00:25:06,020 --> 00:25:07,330
Yes, Your Majesty.

260
00:25:31,280 --> 00:25:33,020
Why didn't you speak out?

261
00:25:39,980 --> 00:25:41,650
I'm the Crown Prince.

262
00:25:43,660 --> 00:25:45,840
I will not abash myself to speak such matter.

263
00:25:49,640 --> 00:25:51,050
Abash?

264
00:25:52,680 --> 00:25:56,530
If you take good care of your wife’s heart better than this,

265
00:25:57,280 --> 00:26:00,020
She won't be hurt like this.

266
00:26:03,670 --> 00:26:06,700
you are the one who must take care of your heart anyway.

267
00:26:08,850 --> 00:26:10,510
Restrain your mind.

268
00:26:11,290 --> 00:26:13,510
It's a qualification of being a Prince, isn't it?

269
00:26:17,540 --> 00:26:24,700
For a Prince who never cares about his wife's heart, he is not a Prince as well.

270
00:26:57,880 --> 00:26:59,290
Princes Nakhun.

271
00:27:05,450 --> 00:27:08,550
Think carefully before doing it.

272
00:27:10,140 --> 00:27:13,610
Don't think on fighting over for the Crown Prince position at all!

273
00:27:14,240 --> 00:27:15,610
And I’m warning on you.

274
00:27:17,060 --> 00:27:23,320
Stop doing things that will discredit the dynasty by doing unsuitable matter to the Princess.

275
00:27:24,300 --> 00:27:27,600
I never think about fighting over the Crown Prince position.

276
00:27:29,220 --> 00:27:30,760
But about the Princess,

277
00:27:31,760 --> 00:27:34,210
I can't restrain myself, Your Majesty.

278
00:27:36,180 --> 00:27:37,570
Prince Nakhun.

279
00:27:38,570 --> 00:27:40,850
Do you know what are you saying?

280
00:27:44,850 --> 00:27:46,930
I apologize.

281
00:27:47,450 --> 00:27:49,120
Excuse me, Your Majesty.

282
00:28:07,420 --> 00:28:09,050
I don't want it.

283
00:28:10,160 --> 00:28:12,440
It's your desire.

284
00:28:13,080 --> 00:28:14,460
Not mine!

285
00:29:00,520 --> 00:29:01,960
Your Highness.

286
00:29:05,360 --> 00:29:06,810
What's matter?

287
00:29:07,500 --> 00:29:11,160
I saw your face and I'm worried about you.

288
00:29:11,640 --> 00:29:19,520
Are you worried that His Majesty let Prince Nakhun go to the event instead of you?

289
00:29:20,980 --> 00:29:22,400
I think that

290
00:29:23,050 --> 00:29:25,400
if you show your real intention,

291
00:29:26,300 --> 00:29:30,420
I believe that his majesty will see it soon.

292
00:29:32,090 --> 00:29:34,020
No matter how good I am,

293
00:29:35,930 --> 00:29:37,880
he probably won't see me better than Nakhun.

294
00:29:37,900 --> 00:29:40,420
Don't think like that Your Highness.

295
00:29:59,520 --> 00:30:01,600
You have been following His Majesty for a long time,

296
00:30:02,890 --> 00:30:05,290
it's impossible for you to not know this.

297
00:30:07,720 --> 00:30:09,090
Your Highness.

298
00:30:09,960 --> 00:30:12,090
This story is a long time ago.

299
00:30:16,320 --> 00:30:18,200
But seeing from the last picture,

300
00:30:19,620 --> 00:30:21,260
I am sure that

301
00:30:22,370 --> 00:30:25,420
time can't delete this story from him.

302
00:30:28,420 --> 00:30:30,870
But it's just a picture.

303
00:30:34,180 --> 00:30:40,040
I can't remember the last time His Majesty took a picture of mom

304
00:30:42,050 --> 00:30:43,520
or maybe he never did.

305
00:30:44,330 --> 00:30:45,260
So

306
00:30:46,400 --> 00:30:48,120
II'm not surprised

307
00:30:49,840 --> 00:30:51,560
why he loves Nakhun more.

308
00:30:51,560 --> 00:30:53,040
Your Highness.

309
00:30:54,610 --> 00:30:56,380
Therefore, don't stop me

310
00:30:57,090 --> 00:30:59,240
from destroying that dishonorable palace.

311
00:31:23,140 --> 00:31:24,620
Welcome, Your Highness.

312
00:31:34,610 --> 00:31:38,480
The Royal Consort summoned me, what do you want me to do?

313
00:31:46,720 --> 00:31:50,490
According to the ancient tradition, there is one custom

314
00:31:51,160 --> 00:31:55,060
that is called a Royal Pardon.

315
00:31:55,730 --> 00:31:57,680
The one who misbehaves to the Royal Family

316
00:31:57,920 --> 00:32:00,610
is going to get down on their knees in front of the King

317
00:32:00,610 --> 00:32:02,840
asking for the Royal Pardon

318
00:32:03,180 --> 00:32:06,520
until the King forgives.

319
00:32:09,280 --> 00:32:11,780
Until the King forgives?

320
00:32:12,260 --> 00:32:13,300
Yes, Your Highness.

321
00:32:17,090 --> 00:32:21,250
And is there anyone who did it successfully?

322
00:32:39,730 --> 00:32:42,660
This way is may be a bit hard.

323
00:32:43,730 --> 00:32:47,080
It's up on how much determination you have.

324
00:32:49,570 --> 00:32:52,560
The progress of the project is going well.

325
00:32:52,640 --> 00:32:54,960
- The staff think that it will be finished this week. 
 - Your Highness!

326
00:32:55,320 --> 00:32:56,410
You can't enter.

327
00:32:56,930 --> 00:32:57,730
Get out of my way.

328
00:32:57,730 --> 00:32:59,000
Your Highness.

329
00:33:18,160 --> 00:33:24,080
I ask to kneel down in front of the King to ask for a Royal Pardon.

330
00:33:28,300 --> 00:33:29,500
The Royal Consort,

331
00:33:30,260 --> 00:33:31,660
go back to your palace immediately.

332
00:33:32,490 --> 00:33:38,360
I will go back if Your Majesty will pardon me.

333
00:33:42,740 --> 00:33:46,480
Do you think from what you did, I'll forgive you?!

334
00:33:47,440 --> 00:33:50,680
What you have done is too much.

335
00:33:50,880 --> 00:33:55,400
About the interview and that night. You were with who? You didn't answer me.

336
00:33:55,820 --> 00:33:57,400
Moreover, you used your father as an excuse.

337
00:33:57,980 --> 00:34:02,480
A crazy excuse like that, you think that Bhutin's people will believe?!

338
00:34:03,240 --> 00:34:06,250
Your Majesty, please calm down.

339
00:34:08,340 --> 00:34:12,100
How did you know the Royal Pardon custom?

340
00:34:13,600 --> 00:34:14,860
Who told you?

341
00:34:16,410 --> 00:34:20,640
The latest person who knelt down here took 3 days.

342
00:34:23,930 --> 00:34:27,240
No matters how long, I will do it.

343
00:34:29,300 --> 00:34:30,400
For your mistakes,

344
00:34:31,360 --> 00:34:35,140
two hundred days are not even enough!

345
00:34:38,080 --> 00:34:39,340
Your Majesty.

346
00:34:46,400 --> 00:34:47,560
Prince Inn.

347
00:34:48,010 --> 00:34:51,010
tell the Royal Consort to stop doing this.

348
00:34:53,080 --> 00:34:56,200
Why is she causing troubles non-stop?

349
00:34:57,880 --> 00:34:59,330
Grandma.

350
00:35:02,300 --> 00:35:05,720
Nakhun, let Khaning stay with Prince Inn.

351
00:35:15,700 --> 00:35:16,680
Mom,

352
00:35:17,800 --> 00:35:20,260
it's you who told Khaning to do this, isn't it?

353
00:35:21,980 --> 00:35:24,010
I just gave her an advice,

354
00:35:24,810 --> 00:35:28,120
she decided everything by herself.

355
00:35:29,290 --> 00:35:32,060
But the Royal Pardon custom is very tough.

356
00:35:33,040 --> 00:35:34,760
She can't endure it.

357
00:35:35,680 --> 00:35:38,600
Don't underestimate the Royal Consort.

358
00:36:24,960 --> 00:36:26,830
Pardon me, Your Majesty.

359
00:36:27,290 --> 00:36:28,240
What's the matter?

360
00:36:30,450 --> 00:36:34,240
I feel so worried about the Royal Consort.

361
00:36:38,440 --> 00:36:44,360
Can I make a little set of food for the Royal Consort?

362
00:36:48,690 --> 00:36:49,820
Ratree.

363
00:36:51,000 --> 00:36:53,920
Only a few people know about this custom

364
00:36:54,850 --> 00:36:58,570
but if one dares to use it,

365
00:36:59,480 --> 00:37:04,840
no one can interfere unless His Majesty forgives.

366
00:37:40,600 --> 00:37:43,210
The dinner this evening looks delicious, Mother.

367
00:37:47,660 --> 00:37:49,320
I can't eat.

368
00:37:55,500 --> 00:37:57,900
No one can pressure me about this!

369
00:37:58,450 --> 00:38:00,720
Everyone knows how this custom works!

370
00:38:01,500 --> 00:38:04,290
How wrong the Royal Consort is, everyone knows even more!

371
00:38:05,100 --> 00:38:07,290
That night, who she went out with...

372
00:38:07,290 --> 00:38:08,920
if she doesn't tell the truth,

373
00:38:09,050 --> 00:38:11,330
don't hope that I'll forgive her!

374
00:38:18,090 --> 00:38:19,370
That night,

375
00:38:20,720 --> 00:38:23,360
the one who was with the Royal Consort is me.

376
00:38:31,770 --> 00:38:33,020
Nakhun!

377
00:38:33,410 --> 00:38:35,060
What did you say?

378
00:38:35,060 --> 00:38:36,040
Nakhun.

379
00:38:36,730 --> 00:38:38,730
Nakhun, you probably didn't...

380
00:38:40,060 --> 00:38:42,040
That night, the Royal Consort was very upset.

381
00:38:42,160 --> 00:38:48,720
I just followed her to comfort her as a friend.

382
00:38:53,540 --> 00:38:56,250
Nakhun, answer me now.

383
00:38:56,720 --> 00:38:59,800
Do you have any inappropriate thoughts towards the Royal Consort?

384
00:38:59,800 --> 00:39:02,680
No, Your Majesty.  No of course!

385
00:39:05,170 --> 00:39:07,600
Right, Nakhun?

386
00:39:08,690 --> 00:39:10,300
No, right?!

387
00:39:11,780 --> 00:39:13,930
No, right?!

388
00:39:17,270 --> 00:39:19,770
I love the Royal Consort with all my heart.

389
00:39:32,640 --> 00:39:35,090
- Queen Mother. 
- Mother.

390
00:39:52,260 --> 00:39:53,770
What the heck did you do?

391
00:39:54,170 --> 00:39:56,020
You know both Khaning and you will be in big trouble.

392
00:39:57,100 --> 00:39:59,960
It's better than to let Khaning get in trouble now

393
00:40:01,240 --> 00:40:03,280
but it will get more complicated.

394
00:40:06,240 --> 00:40:08,520
I don't care what you think

395
00:40:08,520 --> 00:40:11,600
but I will tell Khaning that everything is over!

396
00:40:20,200 --> 00:40:21,360
Khaning,

397
00:40:25,440 --> 00:40:26,840
Khaning.

398
00:40:26,850 --> 00:40:27,940
Stand up.

399
00:40:30,020 --> 00:40:31,400
It's over.

400
00:40:33,000 --> 00:40:35,060
I told everybody the truth.

401
00:40:37,600 --> 00:40:39,120
What truth?

402
00:40:39,780 --> 00:40:43,220
I told them that I was with with you that night.

403
00:40:48,160 --> 00:40:50,580
What the hell did you do?

404
00:40:56,360 --> 00:40:59,100
I can't endure seeing you hurt because of me.

405
00:41:01,300 --> 00:41:02,490
Nakhun,

406
00:41:05,160 --> 00:41:07,340
you misunderstood.

407
00:41:09,140 --> 00:41:11,450
I didn't do it for you.

408
00:41:13,450 --> 00:41:19,640
I did ask His Majesty to forgive me.

409
00:41:25,840 --> 00:41:34,000
Because I don't want Prince Inn to misunderstand.

410
00:41:38,720 --> 00:41:40,010
I...

411
00:41:45,090 --> 00:41:51,920
never think of you more than a friend.

412
00:42:01,060 --> 00:42:03,880
Nakhun, follow me now!

413
00:42:06,460 --> 00:42:08,330
I said now!

414
00:42:19,720 --> 00:42:21,250
What were you thinking?

415
00:42:28,260 --> 00:42:29,560
Answer me!

416
00:42:30,600 --> 00:42:31,980
What were you thinking

417
00:42:32,480 --> 00:42:35,200
or you weren't thinking that's why you said that?

418
00:42:40,040 --> 00:42:41,810
It's over,

419
00:42:43,650 --> 00:42:48,720
the throne that you want is gone

420
00:42:49,530 --> 00:42:52,600
because of your infatuation.

421
00:42:56,220 --> 00:42:58,250
I never want the throne

422
00:43:00,440 --> 00:43:02,490
and I am not infatuated with her.

423
00:43:04,600 --> 00:43:06,250
I love her.

424
00:43:11,770 --> 00:43:13,360
Nakhun...

425
00:43:16,240 --> 00:43:18,820
be conscious.

426
00:43:20,020 --> 00:43:23,920
Think carefully if it's worth it or not?

427
00:43:27,010 --> 00:43:30,840
If you are the Crown Prince,

428
00:43:32,280 --> 00:43:35,210
you will have whatever you want.

429
00:43:37,140 --> 00:43:38,740
Better than her,

430
00:43:39,460 --> 00:43:40,930
more talented than her,

431
00:43:41,760 --> 00:43:43,690
more suitable than her.

432
00:43:44,720 --> 00:43:48,730
You can have any woman in this world.

433
00:43:49,620 --> 00:43:52,450
But I don't want anyone else.

434
00:43:54,290 --> 00:43:57,440
I just want the one I love.

435
00:44:00,800 --> 00:44:02,610
Oh.. Nakhun.

436
00:44:04,540 --> 00:44:07,570
And what will you use love for?

437
00:44:09,090 --> 00:44:12,090
What can love do for you?

438
00:44:15,430 --> 00:44:19,360
What I'm getting for you

439
00:44:19,580 --> 00:44:22,240
is the throne!

440
00:44:26,460 --> 00:44:28,020
I don't want it.

441
00:44:29,180 --> 00:44:31,480
It's your desire.

442
00:44:32,090 --> 00:44:33,490
Not mine!

443
00:44:33,850 --> 00:44:35,300
Not mine!

444
00:44:36,350 --> 00:44:39,420
It's a desire of others' properties.

445
00:44:40,040 --> 00:44:42,420
It's selfishness.

446
00:44:44,660 --> 00:44:49,240
And what about you wanting the Royal Consort, doesn't she belong to other?!!

447
00:44:50,670 --> 00:44:52,670
I found her first.

448
00:44:53,870 --> 00:44:56,130
I found her before Prince Inn.

449
00:44:57,870 --> 00:45:00,320
My love happened before all these.

450
00:45:03,580 --> 00:45:04,930
I will not abandon it.

451
00:45:06,940 --> 00:45:08,820
Oh... Nakhun.

452
00:45:09,930 --> 00:45:13,140
Nakhun. Don't you hear what she said?

453
00:45:14,130 --> 00:45:17,810
She doesn't love you, Nakhun! She doesn't love you!!

454
00:45:20,810 --> 00:45:22,540
I heard

455
00:45:23,370 --> 00:45:24,830
and I understand

456
00:45:26,240 --> 00:45:28,840
but I will not be like what you are.

457
00:45:32,840 --> 00:45:34,010
What?!

458
00:45:35,020 --> 00:45:37,490
I won't abandon my love.

459
00:45:39,420 --> 00:45:41,210
If Khaning doesn't love me,

460
00:45:41,780 --> 00:45:45,220
I will try to understand and leave here.

461
00:45:46,010 --> 00:45:49,690
But I won't let your obsession hurt anyone anymore.

462
00:46:10,060 --> 00:46:12,580
Nakhun... Nakhun!!

463
00:46:12,580 --> 00:46:13,720
Come back!

464
00:46:13,720 --> 00:46:14,780
<i>English Subtitle by ingfakirata, P'Ann, Eveyy_jj, Clishe, nanadc1 
>>  facebook.com/ingfakarataJJ  <<</i>
Come back!

465
00:46:14,780 --> 00:46:15,770
<i>English Subtitle by ingfakirata, P'Ann, Eveyy_jj, Clishe, nanadc1 
>>  facebook.com/ingfakarataJJ  <<</i>

466
00:46:15,770 --> 00:46:17,780
Come back now!
<i>English Subtitle by ingfakirata, P'Ann, Eveyy_jj, Clishe, nanadc1 
>>  facebook.com/ingfakarataJJ  <<</i>

467
00:46:17,780 --> 00:46:25,080
<i>English Subtitle by ingfakirata, P'Ann, Eveyy_jj, Clishe, nanadc1 
>>  facebook.com/ingfakarataJJ  <<</i>

468
00:46:25,080 --> 00:46:26,330
Khaning!
<i>English Subtitle by ingfakirata, P'Ann, Eveyy_jj, Clishe, nanadc1 
>>  facebook.com/ingfakarataJJ  <<</i>

469
00:46:26,330 --> 00:46:26,930
<i>English Subtitle by ingfakirata, P'Ann, Eveyy_jj, Clishe, nanadc1 
>>  facebook.com/ingfakarataJJ  <<</i>

470
00:46:26,930 --> 00:46:27,700
Khaning!
<i>English Subtitle by ingfakirata, P'Ann, Eveyy_jj, Clishe, nanadc1 
>>  facebook.com/ingfakarataJJ  <<</i>

471
00:46:27,700 --> 00:46:29,340
<i>English Subtitle by ingfakirata, P'Ann, Eveyy_jj, Clishe, nanadc1 
>>  facebook.com/ingfakarataJJ  <<</i>

472
00:46:29,340 --> 00:46:30,160
Khaning!
<i>English Subtitle by ingfakirata, P'Ann, Eveyy_jj, Clishe, nanadc1 
>>  facebook.com/ingfakarataJJ  <<</i>

473
00:46:30,160 --> 00:46:31,130
<i>English Subtitle by ingfakirata, P'Ann, Eveyy_jj, Clishe, nanadc1 
>>  facebook.com/ingfakarataJJ  <<</i>

474
00:46:31,130 --> 00:46:32,330
Be patient!
<i>English Subtitle by ingfakirata, P'Ann, Eveyy_jj, Clishe, nanadc1 
>>  facebook.com/ingfakarataJJ  <<</i>

475
00:46:32,330 --> 00:46:33,240
<i>English Subtitle by ingfakirata, P'Ann, Eveyy_jj, Clishe, nanadc1 
>>  facebook.com/ingfakarataJJ  <<</i>

476
00:46:33,240 --> 00:46:34,770
The Principal Secretary,
<i>English Subtitle by ingfakirata, P'Ann, Eveyy_jj, Clishe, nanadc1 
>>  facebook.com/ingfakarataJJ  <<</i>

477
00:46:34,770 --> 00:46:36,000
where is His Majesty?
<i>English Subtitle by ingfakirata, P'Ann, Eveyy_jj, Clishe, nanadc1 
>>  facebook.com/ingfakarataJJ  <<</i>

478
00:46:36,000 --> 00:46:36,530
<i>English Subtitle by ingfakirata, P'Ann, Eveyy_jj, Clishe, nanadc1 
>>  facebook.com/ingfakarataJJ  <<</i>

479
00:46:36,530 --> 00:46:38,770
He went to the Forest Palace, Your Majesty.
<i>English Subtitle by ingfakirata, P'Ann, Eveyy_jj, Clishe, nanadc1 
>>  facebook.com/ingfakarataJJ  <<</i>

480
00:46:38,770 --> 00:46:40,260
<i>English Subtitle by ingfakirata, P'Ann, Eveyy_jj, Clishe, nanadc1 
>>  facebook.com/ingfakarataJJ  <<</i>

481
00:46:40,260 --> 00:46:42,240
If you don't know how to feed your child,
<i>English Subtitle by ingfakirata, P'Ann, Eveyy_jj, Clishe, nanadc1 
>>  facebook.com/ingfakarataJJ  <<</i>

482
00:46:42,240 --> 00:46:42,780
<i>English Subtitle by ingfakirata, P'Ann, Eveyy_jj, Clishe, nanadc1 
>>  facebook.com/ingfakarataJJ  <<</i>

483
00:46:42,780 --> 00:46:44,880
then you can let me do it instead.
<i>English Subtitle by ingfakirata, P'Ann, Eveyy_jj, Clishe, nanadc1 
>>  facebook.com/ingfakarataJJ  <<</i>

484
00:46:44,880 --> 00:46:47,610
<i>English Subtitle by ingfakirata, P'Ann, Eveyy_jj, Clishe, nanadc1 
>>  facebook.com/ingfakarataJJ  <<</i>

485
00:46:47,610 --> 00:46:50,250
Then don't give them a time.
<i>English Subtitle by ingfakirata, P'Ann, Eveyy_jj, Clishe, nanadc1 
>>  facebook.com/ingfakarataJJ  <<</i>

486
00:46:50,250 --> 00:46:52,140
<i>English Subtitle by ingfakirata, P'Ann, Eveyy_jj, Clishe, nanadc1 
>>  facebook.com/ingfakarataJJ  <<</i>

487
00:46:52,140 --> 00:46:54,660
Do it immediately!
<i>English Subtitle by ingfakirata, P'Ann, Eveyy_jj, Clishe, nanadc1 
>>  facebook.com/ingfakarataJJ  <<</i>

488
00:46:54,660 --> 00:46:55,130
<i>English Subtitle by ingfakirata, P'Ann, Eveyy_jj, Clishe, nanadc1 
>>  facebook.com/ingfakarataJJ  <<</i>

489
00:46:55,130 --> 00:47:01,160
From the behavior of the Royal Consort, there is only a way left,
<i>English Subtitle by ingfakirata, P'Ann, Eveyy_jj, Clishe, nanadc1 
>>  facebook.com/ingfakarataJJ  <<</i>

490
00:47:01,160 --> 00:47:02,960
<i>English Subtitle by ingfakirata, P'Ann, Eveyy_jj, Clishe, nanadc1 
>>  facebook.com/ingfakarataJJ  <<</i>

491
00:47:02,960 --> 00:47:05,360
which is a permanent deportation.
<i>English Subtitle by ingfakirata, P'Ann, Eveyy_jj, Clishe, nanadc1 
>>  facebook.com/ingfakarataJJ  <<</i>

492
00:47:05,360 --> 00:47:08,840
<i>English Subtitle by ingfakirata, P'Ann, Eveyy_jj, Clishe, nanadc1 
>>  facebook.com/ingfakarataJJ  <<</i>

