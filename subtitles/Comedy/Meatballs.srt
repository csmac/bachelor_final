{1366}{1420}Attention...
{1422}{1504}Camp North Star senior staff...
{1506}{1547}counselors...
{1549}{1589}and counselors-in-training...
{1645}{1725}please rise for our national anthem.
{2503}{2563}How about that anthem, huh?
{2565}{2641}I don't know about you, but I just get|a great, big lump in my throat...
{2642}{2689}every time I hear it.
{2690}{2790}It's 7:05 in the a.m. And...
{2812}{2878}it's, wow, 43 degrees...
{2880}{2954}on the old Camp North Star|weather dial.
{2956}{3033}That is kind of nippy|for a June 25, isn't it?
{3056}{3090}I'm Tripper Harrison...
{3145}{3197}I'm your head counselor...
{3199}{3246}and this is my wake-up show.
{3247}{3320}I'll be coming at you every morning|about this time...
{3322}{3380}hoping to make|your summer camp experience...
{3382}{3430}the best available...
{3432}{3474}in this price range.
{3510}{3576}Of course, across the lake...
{3578}{3616}over at Camp Mohawk...
{3646}{3713}they won't be getting up|for another hour or so.
{3714}{3797}And when they do, they'll be awakened|by servants bearing croissants...
{3798}{3835}and caf� au lait.
{3837}{3887}Whoops, I hope I didn't hit anybody.
{3889}{3947}But here at Camp North Star|this morning we're gonna be having...
{3949}{3996}a delicious gruel breakfast.
{3998}{4062}And don't forget to ask|for seconds because...
{4064}{4128}it's all the gruel you can eat.
{4156}{4202}Are you ready for the summer
{4229}{4299}Are you ready for the good times
{4301}{4387}Are you ready for the birds and bees|the apple trees
{4389}{4443}And a whole lotta foolin' around
{4526}{4575}Are you ready for the hot nights
{4600}{4689}Are you ready for the fireflies|the moonlit skies
{4690}{4745}And a whole lotta foolin' around
{4774}{4845}No more pencils, no more books
{4846}{4921}No more teachers' dirty looks
{4922}{4993}No more math and history
{4994}{5061}Summertime has set us free
{5238}{5317}Are you ready for the all-day fun|in the sun
{5318}{5386}'Cause the summer is ready for you
{6044}{6119}Okay, everybody, I'm waiting.|People, settle down.
{6289}{6337}Okay, that's better.
{6338}{6412}There's a lot of new people this year,|so before I go any further...
{6414}{6452}I want to introduce myself.
{6454}{6526}- My name's Morty Melnick.|- Hi, Mickey!
{6528}{6600}The name is Morty, not Mickey.
{6602}{6683}The buses are leaving tomorrow morning|at 6:30 to pick up the kids.
{6685}{6739}It's important you get|your bus assignments...
{6741}{6781}to know which kids|you're in charge of.
{6951}{6996}Don't argue with me, Bradley.
{6997}{7062}You can get rabies|from a raccoon too.
{7064}{7106}- Tell him, Maury.|- That's right.
{7108}{7171}Don't forget to put paper|on the toilet seat covers...
{7173}{7214}'cause you can get|a terrible infection...
{7216}{7270}I meant to ask you,|did you bring smokes?
{7272}{7316}No, I couldn't. I forgot.
{7317}{7390}My mom found the carton in my suitcase.|She had a shit-fit.
{7392}{7442}- Why don't we get some.|- Yeah.
{7444}{7494}Mommy. Mommy.
{7496}{7536}Where's my mommy?
{7537}{7620}Tripper, I'm looking forward|to some action this summer.
{7621}{7680}I hope you and your gentlemen|can supply it for me.
{7681}{7772}I could supply you, but the guy|you gotta watch out for is Spaz.
{7773}{7836}- Spaz?|- He's a sex machine.
{7861}{7905}Are you kidding me?
{7906}{7968}He couldn't wake me up|with a trumpet and a drum.
{7970}{8068}Well, I went out with him one night,|and he got us six nurses by himself...
{8070}{8133}and four of them couldn't report|to work the next morning.
{8258}{8296}You gonna be okay?
{8320}{8401}Hey, I'll see you|at the end of the summer, okay?
{8716}{8778}This bus does not have|a safety inspection sticker.
{8780}{8872}- It's a fine old bus.|- No, it's not! It is a piece of junk.
{8873}{8932}I'm gonna tell my father,|and he's gonna report you...
{8933}{8992}- to the Department of Motor Vehicles...|- Roxanne!
{8993}{9061}And you're going to jail,|behind bars.
{9086}{9145}Just like last year,|we'll hate it.
{9147}{9217}Camp is lousy, and you're a dork.
{9219}{9280}You're right.|You sure are one.
{9282}{9317}- Hiya.|- Come on.
{9318}{9365}- What do you want?|- Are you having trouble?
{9366}{9459}- I can't get these kids on the bus!|- That's because I'm not goin'!
{9461}{9541}Come here. I wanna have|a word with you, all right?
{9542}{9623}- You guys, you better get on the bus.|- Why?
{9625}{9708}If you don't, I'm gonna beat|the pants off of ya!
{9709}{9744}Not bad, huh?
{10135}{10201}- Watch it. That stuff's expensive.|- Very good, sir.
{10328}{10374}Hey, Spaz. How you doin', buddy?
{10409}{10462}- Good to see ya.|- Give that back.
{10464}{10536}- We're doin' you a favor.|- Give it back!
{10537}{10580}Wanna make us, Spaz?
{10648}{10682}Yeah.
{10684}{10777}- Give him back the milkshake.|- All right. Here you go.
{10905}{10976}Hey, we'll see you|at the basketball game, Spaz.
{11045}{11088}Mohawk jerks.
{11089}{11178}These children are going to the most|glamorous of all summer camps...
{11180}{11210}Camp Mohawk.
{11212}{11254}There's a two-year waiting list...
{11256}{11316}and every child has to be voted in.
{11317}{11412}On top of all that, it costs|$1,000 a week to go to Camp Mohawk.
{11413}{11492}The question is:|Is it worth $1,000 a week?
{11493}{11565}It sure is.|It's the best darned camp there is.
{11567}{11621}Well, are you connected|with Camp Mohawk?
{11623}{11692}I think so.|I'm the program director, Jerry Aldini.
{11693}{11760}Well, how do you justify|$1,000 a week?
{11762}{11804}We have some special programs.
{11806}{11868}We're doing "Shakespeare in the Round"|again this year.
{11870}{11934}Our political round table.|Henry Kissinger will appear.
{11936}{12021}Yasir 'Arafat is gonna come out, spend|a weekend with the kids, rap with 'em.
{12023}{12065}That's amazing!
{12067}{12137}The kids wanted animals, so this year|each camper will stalk and kill...
{12139}{12195}his own bear|in our private wildlife preserve.
{12197}{12258}Are you sure|the children can hack that?
{12260}{12346}We'll see. The real excitement is|gonna come at the end of the summer...
{12348}{12388}during Sexual Awareness Week.
{12389}{12451}We import 200 hookers from|around the world, and each camper...
{12453}{12545}armed with only a thermos of coffee|and $2,000 cash...
{12546}{12598}tries to visit|as many countries as he can.
{12600}{12664}The winner, of course, is named|King of Sexual Awareness Week...
{12665}{12744}and is allowed to rape and pillage|the neighboring towns until camp ends.
{12746}{12781}That's incredible!
{12782}{12872}What do you expect for $1,000 a week?|Hey, you have a good summer.
{13830}{13937}You must be the short, depressed kid|we ordered. Glad you made it.
{13985}{14023}Want half of this?
{14217}{14303}Come on inside, Frankenstein.|I'll buy you a cocktail.
{14305}{14413}All the 12-year-old girls here finally?|It's gonna be a terrific year.
{14436}{14480}This ground is too slippery.
{14481}{14567}I could fall and get whiplash|or a broken neck, a serious injury.
{14569}{14628}- A guy could die from that.|- Phil, he's all yours.
{14630}{14672}Thanks, Tripper.|Are you Rudy Gerner?
{14674}{14708}I'm Phil.
{14709}{14752}- Gotta get this...|- Hi, Morty.
{14754}{14827}- Hi, Trip. You got the rules?|- No. I'm lookin' for them.
{14829}{14880}Here they go. Very important.
{14882}{14960}All your CIT's have gotta know those|inside-out, backwards, whatever.
{14962}{15020}Post 'em up,|make sure they know 'em.
{15127}{15157}Hey, Larry.
{15236}{15270}What?
{15272}{15341}- I think that chick is staring at me.|- Which chick? Where?
{15343}{15373}Over there.
{15421}{15475}Hey, she is starin' at ya.
{15477}{15557}You better go talk to her.|She probably wants it.
{15758}{15800}What's cookin', good-lookin'?
{15802}{15844}Your fly's undone.
{15982}{16019}Come on.
{16053}{16119}Hey, Fink, come on.|Off your knees.
{16121}{16205}Spaz, I was watching you. Looked like|you had a chance there for a second.
{16207}{16245}Thanks, Trip.
{16285}{16356}- My man, my main man.|- Hey, Tripper.
{16358}{16403}Eyes right, gentlemen.
{16524}{16600}All right, now.|Just right, left, right, left.
{16601}{16636}All right.
{16672}{16768}These are the camp rules.|Morty would like you to look at these.
{16770}{16841}They'll be in here|if you want to check these out later.
{16842}{16890}And now, gentlemen...|and you are...
{16892}{16969}I'll take you to your cabin assignments|for the first week. Let's go!
{16997}{17062}This is the six-year-olds' cabin.
{17064}{17134}All they know how to do|is eat and wet their beds.
{17136}{17207}You guys will teach them|everything else. Crockett, take it.
{17209}{17290}I don't want it. No six-year-olds.|Aw, come on, Trip.
{17292}{17339}My man. Good luck.
{17341}{17412}Put the comic book away|and get your shirt.
{17413}{17464}Take the shirt by the shoulders.
{17466}{17520}One fold sleevie...
{17521}{17566}two fold sleevie.
{17568}{17636}- Presto! It's done.|- How's this, Mike?
{17637}{17728}That's not bad. Why don't we have|a nap and come back later and try it.
{17729}{17774}- No. Forget it.|- Come on.
{17776}{17868}Do I have to count to three?|One, two...
{17869}{17922}Two's good enough.|Come on, Steve.
{18094}{18124}What's that?
{18157}{18219}It's Ernie, my pet frog.|He's a jumper.
{18245}{18323}- How come he doesn't hop?|- He's tired, I guess.
{18324}{18388}- No, he's dead.|- No, he's sleeping.
{18390}{18449}For sure he's dead.
{18465}{18528}Mister, could you help me|wake up my frog?
{18576}{18615}He looks tired.
{18616}{18685}Why don't we let him sleep|a little longer.
{18687}{18732}You take that nap now...
{18734}{18800}and I'll take care of the frog.
{18848}{18894}This is the 14-year-old girls' cabin.
{18896}{18967}They've got the drive and the equipment,|but they don't have the experience.
{18969}{19048}They better not get it from you guys.|Not this summer, anyway.
{19049}{19088}Roxanne!
{19090}{19148}Just the person I wanted to see.
{19150}{19200}Later, Tripper. Say, Christmas?
{19226}{19275}A.L., that's your cabin.
{19276}{19325}You got the jailbait. Watch 'em.
{19327}{19357}- Women.|- Thanks.
{19386}{19483}- See you later.|- I want you to know I'm deeply hurt.
{19484}{19527}I mean that sincerely.
{19528}{19591}Glad to hear it. Excuse me.
{19736}{19784}Hi, Hardware.
{19786}{19840}- Hi, Larry.|- Hi, Finkle. Hi, Hard.
{20096}{20187}Hey, guess what, guys.|Liza finally got her period.
{20206}{20285}- You don't have to advertise it.|- They gotta know.
{20287}{20368}You better watch it,|'cause now you can get pregnant.
{20369}{20456}I heard about this girl. She got|pregnant without doing it with anyone.
{20458}{20535}What do you mean?|You can't get pregnant without doing it.
{20536}{20604}No, stupid. She didn't do it.|She almost did it.
{20606}{20652}Oh, my God.
{20653}{20728}Hi, guys.|I got your cabin this week.
{20730}{20776}Go on. Ask A.L.
{20802}{20836}Ask me what?
{20838}{20899}Can a girl get pregnant|if she almost does it with a guy?
{20901}{20938}Almost?
{20940}{20992}I don't think so. No.
{20994}{21028}That's a relief.
{21084}{21171}Spaz-alapolis, you're in here|with Phil and the 12-year-olds.
{21173}{21238}- This here is Rudy.|- Rudy, how's it goin'?
{21240}{21317}Watch out for this guy.|He's done time for car theft.
{21319}{21353}My kind of guy.
{21355}{21447}You could be a bad influence|on the other children. Watch it.
{21448}{21523}We'll see you guys at dinner.|Watch out for the ear, Rudy!
{21524}{21563}- See you later, Spaz.|- Bye-bye.
{21670}{21748}Hi, guys. I'm Spaz.|I'm your CIT this week.
{21832}{21899}I want to introduce you|to a new guy...
{21900}{21983}Hey, bozo, this is my bunk,|so shove off.
{22058}{22139}Settle down! Come on, guys.|Settle down.
{22165}{22226}Let's see. Everything's put away.|That's good.
{22228}{22279}Everybody has a bunk.|That's good.
{22281}{22330}Everybody's settled in.|That's good.
{22332}{22403}Everybody's been deloused|and fingerprinted. That's good.
{22405}{22496}Phil, look. It's the first day of camp,|and we're bored already.
{22498}{22542}What are we gonna do?
{22544}{22588}Relax, Peter. Relax.
{22590}{22683}Okay, guys, after lunch|we got a big game with Cabin B-11.
{22684}{22760}And I really want to win it, okay?|So what do you say?
{22762}{22800}- We're gonna kill 'em!|- What?
{22826}{22852}I can't hear you.
{22946}{23035}Attention, all campers.|Afternoon swim schedule is as follows:
{23036}{23123}Advanced Dolphins, report to the dock|for survival swimming and IQ testing.
{23125}{23200}All Senior Silverfish,|meet on the beach for nude sunbathing.
{23202}{23286}Junior Salmon, Trout and Herring,|report to the nearest delicatessen.
{23288}{23348}Six-year-old Tadpoles,|report to the swamp.
{23350}{23423}And all Lobsters, get outta here!|You're a menace!
{23819}{23889}- David says it's very important...|- Hi, girls.
{23891}{23973}I'm really looking forward|to working with all of you this summer.
{23975}{24023}I just hope there's enough of me|to go around.
{24025}{24083}Lance, please! We're eating!
{24344}{24379}Aw, crap!
{24380}{24447}Do I have to stack the dishes again?
{24448}{24523}I don't have the energy.|I'm weak from hunger.
{24550}{24588}Less flap, more stack.
{24590}{24660}Gossip! Gossip! We want gossip!
{25161}{25239}We have a gossip update.|This is the news.
{25241}{25339}It seems as though last summer's|hottest couple have split.
{25341}{25432}I can't tell you their names,|but her initials are A. L...
{25434}{25485}and he's the hottest CIT on wheels.
{25519}{25592}We all know they spent|most of last summer in the bushes.
{25594}{25667}The question is:|Will true love bloom again?
{25668}{25753}And speaking of CIT's,|there's a certain girl named Wendy...
{25755}{25835}who's giving|all the guys wet dreams.
{25939}{25985}Okay, Jody.|Thank you very much.
{25987}{26071}Everybody, when you've finished, all the|cabins can go to their other activities.
{26138}{26212}- I'll see you in a bit.|- Guys, let's play ball.
{26214}{26246}All right!
{26282}{26352}Let's move. We gotta get back|to the cabin for a rest period.
{26354}{26416}If I wanted to rest,|I would've stayed in Cleveland.
{26418}{26464}You and Trip are playing defense.
{26466}{26564}- We're defense, buddy!|- Guys, let's go. We can win this game!
{26566}{26616}Hardware, you're doomed.
{26618}{26670}Louder, louder
{26792}{26843}Come on! Come on, people!
{26936}{26961}Let's kill 'em!
{27072}{27108}Rudy, get it back the other way!
{27290}{27372}Do we have to have this guy|on our team?
{27374}{27428}- He's totally useless.|- He sucks!
{27430}{27483}I know. Come on.|Let's regroup.
{28268}{28315}Can I have some fries, please?
{28530}{28588}Gerner! How's it going?
{28656}{28744}This is the best damn food|in the whole Two Pines area.
{28746}{28800}Not surprised you found this place.
{28802}{28891}I had you pegged for a gourmet|first time I met you.
{28892}{28964}You know, that's a smart move,|bringing a suitcase.
{28966}{29036}You don't want to be leaving valuable|socks and underwear around camp...
{29038}{29107}where people can rustle around in 'em|when you're out on the town.
{29108}{29152}Thank you.
{29154}{29192}You like ketchup?
{29234}{29284}I'm going away.
{29286}{29328}You goin' to Vegas?
{29330}{29414}If you're going to Vegas, I would be|up for it because I love that town.
{29416}{29483}I'm a party guy.|I love that town.
{29485}{29532}I don't think they want me around.
{29534}{29612}You talkin' about|the soccer heads back there?
{29614}{29672}Well, that's life|in the fast-paced...
{29674}{29760}slam-bang, live-on-the-razor's-edge,|laugh-in-the-face-of-death...
{29762}{29812}world of junior league soccer.
{29814}{29887}I never played the game before.|I tried to tell 'em.
{29928}{29995}What? You tried to tell...
{29997}{30029}And they...
{30031}{30074}Who was it?
{30076}{30170}I'll get 'em.|I'll get 'em with this Swiss Army knife.
{30172}{30224}The Swiss trained me to kill,|and I will do it.
{30226}{30296}I will grab these guys by the neck,|take the toothpick...
{30298}{30354}and stick it|right in between their teeth.
{30356}{30410}Then I'll slap them around the head|a couple of times.
{30412}{30499}They'll go out for a couple of seconds.|I'll go for the corkscrew.
{30500}{30560}I'll grab 'em|and take that corkscrew...
{30562}{30616}and I will stick it|right into their voice box.
{30618}{30660}I will twist that mofo.
{30662}{30734}I will twist it into his voice box|and rip it out.
{30736}{30804}He'll talk like this|for the rest of his life.
{30806}{30909}I don't want to hurt anyone.|I just want them to like me.
{30945}{30987}Why?
{30989}{31065}You make one good friend a summer...
{31067}{31114}and you're doing pretty well.
{31236}{31331}Look, if you have trouble, come to me,|tell me, and I'll help you.
{31428}{31466}What is that?
{31468}{31520}Is that yes or no?|Yes is like this...
{31522}{31581}and no is just twisting,|twisting, twisting, okay?
{31583}{31629}- It was yes.|- This one? Definitely.
{31631}{31701}- Yes, one of those.|- It wasn't a side-to-side one?
{31703}{31771}You can hurt your neck doing that.
{31772}{31846}Have you got wheels,|'cause I need a lift back to camp?
{31848}{31919}My folks won't let me drive|'cause they caught me drinkin'.
{31920}{31991}They don't let me out of the house|anymore 'cause I'm a jerk.
{31992}{32054}They're gonna send me|to military school.
{32080}{32135}You forgot something.
{32136}{32199}Thanks for the fries.
{32506}{32544}Jesus!
{32573}{32629}- Christ, Hardware.|- What're you doing?
{32631}{32716}Why don't you warn me when you're|gonna play with your dynamite?
{32800}{32844}What are you doing, Hardware?
{32846}{32934}I'm hooking up this heavy duty outlet|for our new air conditioner.
{32935}{32990}An air conditioner?|That's great!
{32991}{33054}- Where is it?|- Morty's office.
{33056}{33105}He loves the heat.
{33186}{33264}An air conditioner|just doesn't get up and walk away.
{33266}{33322}Any idea where it might be, Tripper?
{33323}{33370}I have no idea.
{33371}{33419}Maybe it was stolen, Morty.
{33421}{33474}Of course it was stolen...
{33475}{33516}but I'm gonna find it.
{33518}{33588}Will you two go over|the schedules, please?
{33615}{33650}Ready?
{33695}{33782}Monday, Wednesday and Friday,|2:00 to 4:00 p. M:
{33783}{33832}Junior boys and junior girls.
{33871}{33939}Tuesday and Thursday,|2:00 to 4:00 p. M:
{33941}{33986}Junior boys.
{33987}{34035}Thursday and Friday,|4:00 to 5:00...
{34036}{34078}- Roxanne.|- What?
{34079}{34137}I have to tell you this as a friend.
{34139}{34217}I can see right down your blouse.
{34219}{34268}I can see everything too.
{34270}{34340}Tripper, is there something|seriously wrong with you?
{34342}{34428}Roxanne, I have what doctors call|"very active glands."
{34430}{34492}You're the only person I've told.|My folks don't even know.
{34494}{34540}Maybe you can have surgery|without them knowing.
{34570}{34636}The heck with surgery!|Let's wrestle.
{34638}{34689}I know, let's not, okay, Tripper?
{34730}{34775}The Atomic Skull Crusher!
{34808}{34887}Let's not do this, okay?|Let's just stop right now.
{35179}{35246}- Shark-infested waters!|- Don't do that one!
{35365}{35399}Let's stop, okay?
{35446}{35510}I'm sorry about that blouse thing,|but somebody had to tell you.
{35511}{35571}Get off me right now,|or I start screaming.
{35573}{35660}At the count of three, I guess, huh?|One, two... Help!
{35714}{35761}Help me! Somebody!
{35763}{35815}- Help! Get off me!|- Tripper, get off.
{35834}{35869}What the hell's goin' on?
{35970}{36014}She attacked me!
{36015}{36090}- Give me a break.|- She came on to me like an animal.
{36258}{36320}Would you get a hold of yourself!
{36440}{36526}It's great! You can't even see it|from outside because of the bushes.
{36527}{36571}This is it.
{36573}{36634}Now, in five seconds...|we're talkin' five...
{36636}{36695}we are gonna have|the coolest cabin in camp.
{36696}{36740}All right!
{36742}{36792}- Yes!|- Thank you, Mickey.
{36794}{36848}All right. Let's do it.
{36850}{36911}- Are you ready? Are you ready?|- I'm ready.
{36913}{36998}All right.|Five, four, three...
{37019}{37058}two, one.
{37059}{37084}Do it!
{37234}{37280}- Hardware?|- Yeah, Crockett?
{37282}{37316}You're a dick.
{39218}{39256}Okay, pit stop.
{39258}{39300}Change the tires.
{39524}{39566}Not bad.
{39568}{39640}You got pretty good form.|It's real good.
{39642}{39672}Really?
{39692}{39760}Yeah. You know,|it's your first time out.
{39762}{39800}It's okay.
{39802}{39911}You want to get into shape, though, you|gotta come out every morning with me.
{39913}{39959}I'll make you into an animal.
{40018}{40081}Top physical condition,|you gotta pay the price.
{40270}{40308}Attention, campers.
{40310}{40395}Remember, this is killer bee season|in the north woods.
{40397}{40445}So don't provoke|any flying yellow things.
{40447}{40494}You're just asking for trouble.
{40711}{40783}I told Candace I was gonna christen|my boat "The Candace"...
{40785}{40855}and she just about had|a heart attack.
{40856}{40925}I don't think she knew|how much in love we really were.
{40967}{41049}- Crockett, can you help me a sec?|- Sure, Candace.
{41077}{41111}Hurry up. Come on.
{41257}{41293}What are you doin'?
{41322}{41404}This is the first time|I've ever kidnapped a guy!
{41406}{41465}- No kidding?|- You'll be released unharmed!
{41488}{41592}I just want you long enough to tell you|that I'm really attracted to you.
{41594}{41646}- Yeah?|- Yeah!
{41647}{41706}I think I'm special too.
{41707}{41788}You're the only one in this camp|I really want to spend time with.
{41790}{41837}Really?|Well, what about Lance Cashman?
{41839}{41877}- Lance?|- Yeah, Lance!
{41879}{41948}Lance Cashman is a total jerk-off!
{41990}{42022}All right!
{42023}{42074}She likes me! She likes me!
{42127}{42167}Jerk-off?
{42169}{42208}I don't jerk off.
{42210}{42296}- I don't know how to do this.|- Just throw it up and hit it.
{42701}{42741}I'm sorry.
{42743}{42795}- Why don't we serve?|- Yeah.
{42868}{42952}Did you see the way she handed you|the racket? She wants it.
{42981}{43039}You guys ready?|Here it comes.
{43074}{43112}I got it. It's mine!
{43114}{43186}Get it! Get it!|Come on! Lob it!
{43188}{43238}I got it! I got it!
{43267}{43330}I thought you said|you had it, meatball!
{43332}{43378}Why don't you serve, then?
{43458}{43488}Ready?
{43570}{43612}I got it! I got it!
{43707}{43777}Attention. Here's an update|on tonight's dinner.
{43779}{43850}It was veal.|I repeat, veal.
{43851}{43928}The winner of tonight's mystery meat|contest is Jeffrey Corbin...
{43930}{43998}who guessed "some kind of beef."
{43999}{44082}Congratulations, Jeffrey. You just won|a brand-new Chrysler Cordoba.
{44083}{44156}You can pick it up at Morty's office.
{44210}{44244}You bet.
{44275}{44312}Five.
{44370}{44414}I'll see that five.
{44472}{44511}Want a card?
{44580}{44632}I'll take one. Eight.
{44664}{44698}Fifteen.
{44738}{44790}Eighteen. I'll stick.
{44864}{44880}Twenty.
{45002}{45036}You shark!
{45038}{45094}Playing some sort of system,|aren't you?
{45096}{45162}Been watchin' the cards,|marking the cards.
{45336}{45386}Burp.
{45486}{45528}From the diaphragm. See?
{45607}{45645}Really?
{45717}{45755}From the diaphragm.
{45839}{45888}That's not from the diaphragm.
{45890}{45924}Ante up.
{45967}{46050}- What do you do in the off season?|- Go to school.
{46082}{46115}Twenty.
{46194}{46264}What are you,|some sort of madman?
{46266}{46342}Is that what they teach you|in that school of yours? Twenty?
{46344}{46386}I'll see your 20.
{46388}{46462}I hope you walk outta here|with nothin'.
{46494}{46551}I think I like history the most.
{46553}{46650}It's fun to imagine that I'm in|some other time, in a different place.
{46652}{46712}Shut up and look at your cards.
{46797}{46827}Blackjack.
{47022}{47059}Nineteen.
{47061}{47099}Twenty-one.
{47142}{47202}I've had it. That's it.
{47204}{47254}Card monster. Get out!
{47356}{47430}I think you're pretty tired.|I think you ought to go home and sleep.
{47432}{47468}This was great.
{47470}{47528}You're the guy walkin' home|with all the peanuts.
{47530}{47605}If I were walking home with the peanuts,|it'd be incredible, sensational!
{47647}{47690}Are you going to bed?
{47691}{47798}No. I've called an organizational|meeting of the CIT's for a little later.
{47799}{47843}No midgets allowed. Sorry.
{47889}{47935}Enough of this playing for peanuts.
{47937}{48011}Tomorrow night we play|for real stakes... zucchini.
{48054}{48108}Children starving in India,|and you're walking...
{48110}{48167}around with a whole sombrero|full of peanuts.
{48169}{48213}I hope you sleep well.
{48254}{48289}Good night, Tripper.
{48567}{48624}Attention, all campers.
{48626}{48692}It's now 9:30.
{48694}{48732}And that's lights-out time...
{48734}{48782}9:30, as you know.
{48784}{48827}Tomorrow is parents day...
{48829}{48891}and you must look rested,|or Morty will be sent...
{48893}{48935}to the state penitentiary.
{49224}{49288}- What are you doin'?|- Cut the light!
{49365}{49422}- Come on. What, is it breakfast?|- Hardware.
{49423}{49478}Get up! Up! Up!
{49511}{49551}Who's the puppy?
{49622}{49654}What?
{49656}{49723}All right, gentlemen.|Big event.
{49746}{49785}Operation Late Night Excitement.
{49787}{49839}Another panty raid, Trip?
{49841}{49907}No questions, dogface!
{49909}{49969}Dress up and move out!
{50845}{50883}All right. Now.
{51086}{51161}Don't wake him. Tie him gently.|He needs his sleep.
{51226}{51280}Honor guard.
{51282}{51328}Prepare to raise the casket.
{51671}{51725}It won't fit, Trip.
{51727}{51762}Spin him.
{51802}{51843}All right.
{52096}{52152}Company, rotate.
{52276}{52317}Right face.
{53372}{53418}Morning, Mr. Melnick!
{53419}{53454}Hey, Mickey!
{53834}{53884}Lots of room.|Looks fine, Dad.
{53886}{53934}Looks good, Dad. Come on.
{53936}{53970}Looks fine, Dad.
{54048}{54086}Sorry, Dad.
{54330}{54402}You don't have to run with me today.|I know you got things to do.
{54404}{54480}I'd rather run.|Takes my mind off sex.
{54506}{54554}Is your dad coming up today?
{54556}{54600}He can't.
{54602}{54641}Mine can't, either.
{54643}{54706}He's gotta mow the lawn.
{54708}{54769}Mind hanging out with me today?
{54771}{54805}No.
{54858}{54916}If you let me
{54948}{55058}I could be your good friend
{55060}{55146}I know that if you let me
{55174}{55262}We could walk together
{55264}{55384}We're not so different, you know
{55386}{55515}Though we may have different dreams
{55517}{55605}- Say, got any kids in your family?|- No.
{55607}{55657}Did you ever do any time, prison?
{55698}{55762}- You know how to hot-wire a car?|- Not yet.
{55764}{55802}You'll learn in class.
{55804}{55864}Did you read in the paper today|about the Polish terrorist?
{55866}{55928}Tragedy. They sent this guy|to blow up a car...
{55930}{55986}and the guy burns his mouth|on a tailpipe.
{55988}{56038}That's an old one.|You'll get that a lot up here.
{56040}{56082}Rattlesnake! Look out!
{56447}{56489}Shh! Quiet. Quiet. Shh!
{56649}{56729}"...her head from side to side|against the dank jungle floor..."
{56762}{56794}Hey, relax.
{56796}{56884}"In the distance the ocean undulated|with increasing fervor."
{56886}{56934}- I can hear them.|- Yeah, yeah, yeah!
{56936}{56989}This book is really disgusting.
{56990}{57056}Well, it sounds great to me.
{57058}{57106}I don't believe it.
{57108}{57156}"She could no longer|contain the plea."
{57158}{57192}Oh, my God!
{57194}{57268}Oh, you little hunk of honey bunch!
{57314}{57344}Take me!
{57381}{57412}Make me a woman.
{57413}{57490}"She realized she was uttering sounds|from deep within herself...
{57492}{57547}noises unlike any|she had ever made before."
{57570}{57624}Shut up! Shut up! Shut up!
{57686}{57774}"...the hungry personification|of passion and pain.
{57776}{57836}Closer and closer...
{57838}{57943}the stranger came|towards the entangled pair."
{57979}{58013}- What?|- Take me now!
{58015}{58069}- I said I'm getting a boner!|- Will you shush!
{58119}{58184}Wait a minute.|I think I hear something.
{58349}{58402}I'll check it out.
{58404}{58437}Quiet.
{58489}{58524}They can hear us.
{58631}{58692}I don't hear anything.
{58693}{58778}- I'm gettin' outta here.|- I bet they heard us. Get out of here.
{58780}{58826}Go on. Get out.
{58828}{58887}Spaz, I'm stuck.|I'm having trouble.
{58889}{58933}Come on. Spaz, help me.
{58934}{58972}I'm having trouble!
{58974}{59016}Spaz, what are you doing?
{59041}{59092}Spaz, you're rippin' my face off!
{59152}{59192}Let me outta here.
{59217}{59249}What are you doing?
{59461}{59505}What, are you a homo or what?
{59694}{59788}Attention. Bus for the Camp Mohawk|basketball game leaving in 15 minutes.
{59789}{59901}And there is a very fat pair of pants|hanging on the flagpole this morning.
{60143}{60195}See you at Camp Mohawk, Morty!
{60549}{60613}Okay, guys!|One, two, three!
{60615}{60651}Camp Mohawk!
{60653}{60700}Death, injury, blood and pain!
{60701}{60752}Mohawk, Mohawk, win again!
{60980}{61044}Look out, Mohawks!|North Star's comin' through!
{61374}{61475}I don't know, Trip. Those Mohawks|look better than last year.
{61477}{61561}Guys, it's important for us|to hustle at both ends of the court.
{61563}{61609}We gotta make the offense work.
{61611}{61681}We gotta play that big, tough "D."|We gotta contain their big men inside.
{61683}{61762}We gotta crash the boards at offense.|We can't give them the baseline.
{61764}{61832}But more important than the score|of this game...
{61833}{61908}is to score at the big social|at our place tonight.
{61909}{61990}So I want you to go out there|and protect your balls at any cost.
{61992}{62031}All right!
{62033}{62107}- Bruce Lee, patron of self-defense...|- Pray for us!
{62109}{62155}- Hey! Let's go!|- All right!
{62182}{62251}- Let's go!|- Go get 'em, guys. Thank you.
{62253}{62323}Come on, Spaz. Let's go, Spaz.|Sink it, babe.
{62349}{62396}- Sorry, guys.|- Fink, over here.
{62397}{62449}This is how it's done.
{62476}{62532}Looks good.|Hardware, it looks good.
{62581}{62619}All right!
{62769}{62813}- Try it again.|- Another one.
{62854}{62931}You know, our guys|don't look so good.
{62933}{62968}They're not.
{63085}{63125}Go, North Star, go!
{63159}{63218}Hurt 'em again!|Hurt 'em again!
{63384}{63459}Hey, Wheels, you're goin' home|in an ambulance.
{63461}{63517}- Nice shirt, Crockett.|- Thanks.
{63518}{63556}- All right, guys.|- Mook.
{63558}{63646}Let's go. Let's play some basketball.|Here we go.
{63888}{63949}Hey, nice-goin', Mohawks.|Nice-goin'.
{63951}{63988}Assholes.
{64044}{64090}- Am I all right?|- Great try.
{64092}{64126}I think we're in big trouble.
{64128}{64184}That Crockett's really cute.
{64185}{64259}Yeah, but he can't play basketball|worth a shit.
{64418}{64492}There are five guys.|They play offense and defense...
{64494}{64541}but they play|at different ends of the court.
{64542}{64580}They switch sides at halftime.
{64614}{64650}Wheels!
{64709}{64752}Hardware!
{64767}{64809}- You all right?|- Yeah.
{64811}{64885}- Help him, Spaz. Come on.|- Easy, easy. Come on.
{64907}{64960}Now you know why|they call that guy Rhino.
{64986}{65069}- His nose is bleeding.|- It's gonna get even bigger now.
{65070}{65103}Jackie!
{65105}{65147}- Hardware, you okay?|- Yeah.
{65149}{65217}Hardware Renzetti, girls.|It's just a nosebleed.
{65219}{65309}Looks like he's gonna be|at the social tonight.
{65311}{65380}It really doesn't look any worse.|Really.
{65381}{65430}- Really?|- Go out there and kill 'em.
{65756}{65818}Hey, Larry, you scored!
{65845}{65933}Way to go! Okay, you guys, feed Fink.|He's our hot man.
{65935}{65972}Here, fella!
{65973}{66008}All right! Yeah!
{66052}{66121}Come on. Let's go.|Let's kill 'em.
{66153}{66221}- Come on, guys! That's it.|- Oh, sure.
{66329}{66372}Do it again, Larry!
{66712}{66746}We can beat 'em anyway!
{66748}{66818}It's all right, it's okay!|We can beat 'em anyway!
{66997}{67058}Okay, the zone's not working.
{67060}{67136}They're a little bit too good|to cover man-to-man...
{67137}{67190}and we can't shoot for shit.
{67192}{67261}What kind of talk's that|for a coach?
{67263}{67354}I'm not gonna lie. There's no way|we're gonna beat this team.
{67356}{67400}What are we gonna do, Trip?
{67402}{67455}- We're gonna lose.|- What?
{67481}{67563}But we can lose with some self-respect.|Here's the idea.
{67849}{67880}Yea, fellas!
{67933}{67975}Nice team spirit.
{67977}{68048}I'll talk to you after the game.
{68049}{68124}- Late as usual.|- Where were you, young man?
{68126}{68172}Okay, guys, a little teamwork now.
{68642}{68704}Spaz, get in the bus!|Hurry up!
{68829}{68922}This is the proudest moment|in North Star history!
{69473}{69514}Forget it, Spaz.
{69516}{69558}They all want me.
