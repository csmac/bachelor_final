1
00:00:02,135 --> 00:00:03,159
All right, officers.

2
00:00:03,336 --> 00:00:07,636
One beef and cheese deluxe, french fries
and orange-flavored beverage.

3
00:00:07,807 --> 00:00:10,002
Thank you, my brother.
Knock it down for me.

4
00:00:10,176 --> 00:00:11,643
Boo-yow!

5
00:00:11,811 --> 00:00:13,369
All right, it is knocked down.

6
00:00:14,647 --> 00:00:16,205
And for you, large man.

7
00:00:17,150 --> 00:00:21,280
One plain wiener, no bun, no sauerkraut,
no chili, no mustard...

8
00:00:21,454 --> 00:00:23,319
...no pickle, no cheese, no relish--

9
00:00:23,490 --> 00:00:25,890
No tip, give it to me.

10
00:00:27,027 --> 00:00:28,187
Excuse me to inquire...

11
00:00:28,361 --> 00:00:33,526
...but this meager meal seems insufficient
for a man of your tremendous girth.

12
00:00:33,700 --> 00:00:36,032
Well, I'm just trying
to drop a couple of pounds.

13
00:00:36,403 --> 00:00:38,371
May I suggest you move to my country...

14
00:00:38,538 --> 00:00:42,235
...where everyone is fashionably thin,
due to lack of food.

15
00:00:44,844 --> 00:00:45,868
New diet?

16
00:00:46,046 --> 00:00:48,446
I read about it in a magazine
at the dentist office.

17
00:00:48,848 --> 00:00:50,509
-Men's Fitness?
-No.

18
00:00:50,917 --> 00:00:52,885
-Esquire?
-No.

19
00:00:53,053 --> 00:00:54,418
-Maxim?
-No.

20
00:00:55,422 --> 00:00:57,947
-Sports Illustrated?
-Modern Bride, okay?

21
00:00:58,124 --> 00:00:59,955
Hey. That's none of my business.

22
00:01:01,628 --> 00:01:04,495
You better get married quick
because you're starting to show.

23
00:01:04,664 --> 00:01:07,724
It happens to be a good diet
and I'm losing weight.

24
00:01:07,901 --> 00:01:12,065
I'm going to the gym three times a week
and a half hour every morning on the bike.

25
00:01:12,238 --> 00:01:13,569
Well, be careful with that.

26
00:01:13,740 --> 00:01:17,699
I read an article that said those bicycles
wreak havoc on a pair of testicles.

27
00:01:18,111 --> 00:01:22,480
What are we talking about, a little
numbness or that time I sat on my Taser?

28
00:01:22,649 --> 00:01:26,050
Well, apparently, the angle of the seat,
it restricts blood flow...

29
00:01:26,219 --> 00:01:28,653
...causing low sperm count
and/or possible impotence.

30
00:01:28,822 --> 00:01:32,155
-Really? I never heard that.
-You ain't gonna read that in Modern Bride.

31
00:01:33,259 --> 00:01:36,057
Pick up this month's copy
of American Balls.

32
00:01:36,830 --> 00:01:40,322
Hey, how about we hit a couple of clubs up
after we get off, huh?

33
00:01:40,500 --> 00:01:43,765
Have a few drinks, maybe find
some bad girls who wanna cop some pleas.

34
00:01:43,937 --> 00:01:45,234
Or even better, please some cops?

35
00:01:45,405 --> 00:01:46,667
[LAUGHS]

36
00:01:47,640 --> 00:01:49,505
Do you listen to yourself when you talk?

37
00:01:51,111 --> 00:01:52,601
No, thanks, I got an OA meeting.

38
00:01:52,779 --> 00:01:55,748
You're kidding me.
Overeaters Anonymous on a Friday night?

39
00:01:55,915 --> 00:01:56,939
That is pathetic.

40
00:01:57,117 --> 00:01:59,813
Oh, I'm pathetic?
Which one of us lives with his grandma?

41
00:01:59,986 --> 00:02:03,786
I'm over there because she's old and frail
and needs somebody to look after her.

42
00:02:03,957 --> 00:02:06,289
She mows the lawn, Carl.

43
00:02:06,459 --> 00:02:09,792
After I start it up for her
and push her in the right direction.

44
00:02:10,130 --> 00:02:13,156
Fine. Whatever you need
to live with yourself...

45
00:02:13,333 --> 00:02:14,960
...and your grandma.

46
00:02:15,602 --> 00:02:19,038
I would shoot you right now, but I don't
have enough chalk to outline your body.

47
00:02:20,807 --> 00:02:21,967
[SlNGlNG]
She's a brick

48
00:02:22,142 --> 00:02:25,373
Uh, uh, uh, house, uh, uh, uh

49
00:02:25,712 --> 00:02:28,613
She's mighty, mighty
Just letting it all hang out

50
00:02:28,781 --> 00:02:33,650
Because she's a brick
Hmm, hmm, house, hmm, hmm, hmm

51
00:02:33,820 --> 00:02:35,845
Shake it down, shake it down
Shake it down, down

52
00:02:36,022 --> 00:02:37,887
Shake it down, shake it down
Shake it down, down

53
00:02:38,057 --> 00:02:39,524
Shake it down, shake it

54
00:02:39,692 --> 00:02:41,956
Ma, do you have to eat that
in front of me?

55
00:02:42,128 --> 00:02:44,619
Oh, I'm sorry, baby, want a bite?

56
00:02:44,797 --> 00:02:48,824
What do you think I'm doing
on this machine here, making butter?

57
00:02:49,169 --> 00:02:51,831
Oh, baby,
why are you punishing yourself?

58
00:02:52,238 --> 00:02:55,139
Face it, you're a big-boned girl.

59
00:02:55,308 --> 00:02:57,242
You're always gonna be a big-boned girl.

60
00:02:57,410 --> 00:02:59,537
Bones don't jiggle, Mom.

61
00:02:59,712 --> 00:03:02,875
You just gotta accept the fact
that you got your daddy's genes.

62
00:03:03,349 --> 00:03:07,115
I mean, if you had a turkey leg in one hand
and the other hand down your pants...

63
00:03:07,287 --> 00:03:09,517
...I'd swear he was risen from the dead.

64
00:03:10,890 --> 00:03:12,790
God, this is moist.

65
00:03:12,959 --> 00:03:15,553
Do I smell your double chocolate
blackout cake?

66
00:03:15,728 --> 00:03:17,025
Fresh out of the oven.

67
00:03:17,197 --> 00:03:18,721
Ha-ha-ha, give me.

68
00:03:18,898 --> 00:03:20,729
JOYCE:
Mmm.

69
00:03:20,900 --> 00:03:21,924
VlCTORlA:
Mmm.

70
00:03:22,101 --> 00:03:23,864
Oh, Mom, this is incredible.

71
00:03:24,037 --> 00:03:26,904
-This time, instead of milk, I used pudding.
-Mm-hm.

72
00:03:27,874 --> 00:03:31,105
Well, bravo. That must be why
it's so smooth and creamy.

73
00:03:31,277 --> 00:03:34,508
For God's sakes,
why are you doing this to me?

74
00:03:34,681 --> 00:03:36,979
-What? What are we doing?
-What are we doing?

75
00:03:37,951 --> 00:03:39,077
Never mind.

76
00:03:40,053 --> 00:03:43,716
I've burned off my, uh, 12 calories.

77
00:03:45,091 --> 00:03:46,558
Just gonna go to my OA meeting.

78
00:03:46,726 --> 00:03:49,786
Oh, Mol, you're never gonna meet
a cute guy at the chub club.

79
00:03:51,231 --> 00:03:53,893
I'm not going to meet guys.
I'm going for support.

80
00:03:54,067 --> 00:03:58,197
Not that I don't get
more than enough right here.

81
00:03:58,371 --> 00:04:00,032
Whoa, where'd that come from?

82
00:04:00,206 --> 00:04:02,265
Where did that come from?
Are you kidding me?

83
00:04:02,442 --> 00:04:05,934
Okay, I'm trying to watch my diet,
I'm trying to exercise...

84
00:04:06,112 --> 00:04:08,171
...and you two are trying
to make me fatter.

85
00:04:08,348 --> 00:04:11,078
You're not fat. You're big-boned.

86
00:04:11,618 --> 00:04:13,449
And I'm just trying to get you laid.

87
00:04:14,320 --> 00:04:16,083
Ugh.

88
00:04:16,623 --> 00:04:19,183
Why don't you take her
to one of those lesbo clubs?

89
00:04:19,359 --> 00:04:21,759
-Now, they seem to like the beefy gals.
-Hmm.

90
00:04:43,383 --> 00:04:45,248
You really didn't have to come with me.

91
00:04:45,418 --> 00:04:47,818
Hey, you said you wanted
to be supported.

92
00:04:47,987 --> 00:04:49,420
-I'm supporting.
-Okay, ah--

93
00:04:49,589 --> 00:04:52,057
-Where are you going? The meeting's here.
-Go ahead.

94
00:04:52,225 --> 00:04:54,853
I'm gonna sneak into the little girls' room
and light up a fatty.

95
00:04:56,362 --> 00:04:58,193
No offense, fellas.

96
00:04:59,999 --> 00:05:02,627
-Hi, my name's Mike. I'm an overeater.
GROUP [lN UNlSON]: Hi, Mike.

97
00:05:02,802 --> 00:05:04,929
I had a pretty fair week. I lost 3 pounds.

98
00:05:05,104 --> 00:05:06,366
MAN 1 : Hey.
MAN 2: Yeah.

99
00:05:06,539 --> 00:05:09,997
Then I took off my shirt
and I found it right about here.

100
00:05:10,176 --> 00:05:13,441
Anyway, diet-wise,
I did have one tiny setback this week.

101
00:05:13,613 --> 00:05:15,205
I was at the grocery store...

102
00:05:15,381 --> 00:05:18,714
...and they were having a sale
on those fun-size candies for Halloween.

103
00:05:18,885 --> 00:05:22,548
Uh, I picked up a bag, figuring I'm gonna
need something for the trick-or-treaters.

104
00:05:22,722 --> 00:05:25,384
Before I know it, I'm standing
in front of the checkout...

105
00:05:25,558 --> 00:05:28,652
...with 19 of the fun bastards
jammed in my mouth, trying to say:

106
00:05:28,828 --> 00:05:30,125
"Plastic."

107
00:05:30,296 --> 00:05:31,558
[LAUGHlNG]

108
00:05:31,731 --> 00:05:34,598
Hallelujah, right?

109
00:05:36,469 --> 00:05:38,460
Then, of course,
the self-loathing kicks in.

110
00:05:38,638 --> 00:05:41,163
Voices in my head start telling me
what a loser I am...

111
00:05:41,341 --> 00:05:43,172
...how no one's ever gonna love me...

112
00:05:43,343 --> 00:05:47,302
...how I'm gonna spend the last few years
of my life alone in a dark apartment...

113
00:05:47,480 --> 00:05:49,573
...my only companions,
six or seven cats...

114
00:05:49,749 --> 00:05:53,583
...that made the mistake of wandering
into my gravitational field.

115
00:05:54,554 --> 00:05:57,421
The bottom line is,
I know those voices are lying.

116
00:05:57,590 --> 00:06:00,889
And I know all I gotta do
is work this program one day at a time.

117
00:06:01,060 --> 00:06:03,927
That's why I start every morning
by getting on my knees...

118
00:06:04,097 --> 00:06:07,794
...and asking God to give me
the strength to stay on my diet.

119
00:06:07,967 --> 00:06:12,028
And then I pray that he gives me the
strength to get my fat ass up off the floor.

120
00:06:12,438 --> 00:06:13,496
MlKE:
Anyway, thank you.

121
00:06:17,510 --> 00:06:18,943
Hey.

122
00:06:20,546 --> 00:06:23,106
Damn it, one of y'all got to inhale.

123
00:06:24,984 --> 00:06:26,508
Hey.

124
00:06:27,019 --> 00:06:28,145
What are you doing here?

125
00:06:28,321 --> 00:06:31,085
Now that your meeting's over,
you might wanna have some fun.

126
00:06:31,257 --> 00:06:33,555
Sure. Wanna go get some TCBY?

127
00:06:33,726 --> 00:06:38,129
Hell, yeah. Couple of single guys on a
Friday night, let's get all up in some yogurt.

128
00:06:38,765 --> 00:06:40,733
Hey, I really liked your share.

129
00:06:40,900 --> 00:06:41,958
Oh, thank you.

130
00:06:42,135 --> 00:06:44,467
-You're pretty funny.
-lf everybody's laughing...

131
00:06:44,637 --> 00:06:46,400
...they won't try to kill
and cook each other.

132
00:06:46,572 --> 00:06:48,836
[MOLLY CHUCKLES]

133
00:06:49,008 --> 00:06:50,475
-Molly.
-Mike.

134
00:06:50,643 --> 00:06:53,203
Hey, there you are.

135
00:06:54,180 --> 00:06:55,477
Where did you go?

136
00:06:55,648 --> 00:07:00,915
Went looking for a snack machine and all l
found were these crackers up in the church.

137
00:07:01,087 --> 00:07:02,111
Really dry.

138
00:07:03,489 --> 00:07:07,152
Do you know of a good place around here
where I could get a pizza?

139
00:07:07,894 --> 00:07:09,589
What do you think?

140
00:07:10,530 --> 00:07:12,088
This is my sister, Victoria.

141
00:07:12,265 --> 00:07:14,426
-Hi, Victoria.
-Hello, Victoria.

142
00:07:14,600 --> 00:07:16,625
Officer Carl MacMillan, Chicago PD.

143
00:07:16,803 --> 00:07:18,600
Whoa, you're a cop?

144
00:07:18,771 --> 00:07:20,568
-Yeah, Mike and l, we're both cops.
-Ha, ha.

145
00:07:20,840 --> 00:07:23,206
Great. I love cops.

146
00:07:23,376 --> 00:07:24,468
Cops are great.

147
00:07:24,644 --> 00:07:26,009
I'm not high.

148
00:07:27,680 --> 00:07:31,013
Anyhoo, uh, I'm a fourth-grade teacher...

149
00:07:31,184 --> 00:07:35,348
...and I'd love to have a police officer
come and speak to my class.

150
00:07:35,521 --> 00:07:37,887
Oh, absolutely.
Just contact the department.

151
00:07:38,057 --> 00:07:39,581
They'll send somebody right over.

152
00:07:39,759 --> 00:07:41,021
Oh, all right.

153
00:07:41,194 --> 00:07:45,494
Or you can give Officer Biggs your number
and he can come talk to your class himself.

154
00:07:45,932 --> 00:07:46,956
Even better.

155
00:07:47,600 --> 00:07:52,833
Oh, uh, okay. Well, are you looking for, like,
general information or scared straight?

156
00:07:53,005 --> 00:07:56,566
Well, they're 9 years old,
so scare the hell out of them.

157
00:07:56,742 --> 00:07:58,471
[MlKE CHUCKLES]

158
00:07:58,644 --> 00:08:02,876
Kidding. Just be funny and charming
like you were in the meeting.

159
00:08:03,616 --> 00:08:05,481
-Okay.
-Great. Call me.

160
00:08:06,052 --> 00:08:07,679
-I will.
-All right.

161
00:08:10,690 --> 00:08:13,682
Has it been a long time since I've talked?

162
00:08:14,160 --> 00:08:17,152
Come on. Come on. Come on.

163
00:08:19,098 --> 00:08:21,896
Tell me why we let them go.
The pothead had jungle fever...

164
00:08:22,068 --> 00:08:25,003
...and you could've gotten the other one
with a taffy apple.

165
00:08:25,605 --> 00:08:28,631
I'm not letting her go.
I just don't wanna come off as desperate.

166
00:08:28,808 --> 00:08:31,641
-Yeah, but you are desperate.
-No. I'm lonely and miserable.

167
00:08:31,811 --> 00:08:33,745
There's a difference.

168
00:08:33,913 --> 00:08:35,540
And what is that difference?

169
00:08:35,715 --> 00:08:38,479
Shut up. You live with your grandma.

170
00:08:43,322 --> 00:08:47,759
I wish these uniforms were a little
more flattering, you know? Blousier.

171
00:08:47,927 --> 00:08:49,986
Who you trying to fool?

172
00:08:55,835 --> 00:08:57,769
I mean, you met her at an OA meeting.

173
00:08:57,937 --> 00:09:01,395
She sticks her hand under there and doesn't
find a man bra, she's gonna be ecstatic.

174
00:09:03,075 --> 00:09:06,704
You know, uh, I was thinking about
asking her to go to the aquarium with me.

175
00:09:06,879 --> 00:09:09,279
-The aquarium? The first date?
-Yeah. Uh-huh.

176
00:09:09,448 --> 00:09:10,710
-Let me ask you something.
-What?

177
00:09:10,883 --> 00:09:12,077
You ever been on a second date?

178
00:09:13,519 --> 00:09:14,781
[TOlLET FLUSHES]

179
00:09:19,692 --> 00:09:21,853
-Hold it right there, fella.
-Yes, sir?

180
00:09:22,028 --> 00:09:24,258
-You wanna go to jail?
-No, sir.

181
00:09:24,430 --> 00:09:26,091
Then get over there
and wash your hands.

182
00:09:28,634 --> 00:09:29,931
[CHATTERlNG]

183
00:09:30,102 --> 00:09:32,332
All right, people, settle down.

184
00:09:32,505 --> 00:09:34,530
I want you to give your attention...

185
00:09:34,707 --> 00:09:38,370
...and your respect to Officer Michael Biggs
of the Chicago Pol--

186
00:09:38,544 --> 00:09:40,102
Shut up!

187
00:09:42,815 --> 00:09:44,749
Chicago Police Department.

188
00:09:46,986 --> 00:09:49,420
-Officer Biggs?
-Okay.

189
00:09:49,589 --> 00:09:50,817
-Uh, thank you.
-Mm-hmm.

190
00:09:51,123 --> 00:09:53,887
First, I'd like to say,
there are a lot of misconceptions...

191
00:09:54,060 --> 00:09:56,460
...about what it is a police officer
actually does.

192
00:09:56,629 --> 00:09:58,597
My dad says you guys are all on the take.

193
00:09:58,764 --> 00:10:00,823
-Gary, I told you not--
-No, no, that's okay.

194
00:10:01,000 --> 00:10:03,798
Gary, that's one of the misconceptions
I was talking about.

195
00:10:04,103 --> 00:10:07,504
On TV and in the movies, they like
to make you think there's corruption--

196
00:10:07,673 --> 00:10:10,972
My mom says you guys only go
after people of color.

197
00:10:12,011 --> 00:10:15,640
-Well, jeez, that's not true at all.
-You calling my mom a liar?

198
00:10:15,815 --> 00:10:18,841
Uh, why don't we save
all of our questions for the end?

199
00:10:19,018 --> 00:10:20,315
You know what? This is good.

200
00:10:20,486 --> 00:10:23,683
I mean, questions lead to a give-and-take
and stimulate discussion.

201
00:10:23,856 --> 00:10:26,324
How can you be a cop and be so fat?

202
00:10:27,760 --> 00:10:30,422
You know, maybe we should save
the questions till after.

203
00:10:30,596 --> 00:10:32,291
-Hey, I have a question.
-You don't.

204
00:10:32,465 --> 00:10:34,592
-Oh, but I do.
-No, you don't.

205
00:10:34,767 --> 00:10:38,635
What made you wanna become
a police officer in the first place?

206
00:10:38,804 --> 00:10:40,829
Oh, that's a great question.

207
00:10:41,374 --> 00:10:45,276
Well, I became a police officer
because my dad was a police officer.

208
00:10:45,711 --> 00:10:50,580
He worked the same beat for 30 years:
A five-block radius in the Wicker Park area.

209
00:10:50,750 --> 00:10:55,449
And since our last name is Biggs,
he used to refer to it as Biggs' Mile.

210
00:10:56,055 --> 00:10:59,183
As a kid, I always thought
he was saying "big smile."

211
00:11:01,394 --> 00:11:03,123
I remember watching him leaving.

212
00:11:03,295 --> 00:11:07,391
His uniform would always be pressed
and his shoes shined like mirrors.

213
00:11:07,566 --> 00:11:11,559
I'd say to him, "Dad, where are you going?"
And he'd say, "Biggs' Mile, son."

214
00:11:11,737 --> 00:11:16,003
And since I thought he was saying
"big smile," I'd give him a big smile.

215
00:11:16,742 --> 00:11:20,644
Anyway, everybody in that neighborhood
loved him because he kept the place safe.

216
00:11:20,813 --> 00:11:23,213
And he treated everyone with respect.

217
00:11:23,382 --> 00:11:27,682
He made police work look like a pretty noble
profession right up until the very end.

218
00:11:27,853 --> 00:11:29,150
What happened? Did he die?

219
00:11:29,321 --> 00:11:32,256
No, he fell in love with a prostitute,
divorced my mom and moved to Tampa.

220
00:11:35,027 --> 00:11:37,621
-Tore the whole family apart.
MOLLY: Ahem.

221
00:11:39,265 --> 00:11:42,166
I probably should've stopped
at "noble profession."

222
00:11:44,170 --> 00:11:47,537
I really appreciate you doing this.
I think the kids got a lot out of it.

223
00:11:47,707 --> 00:11:50,676
Hey, they seem like a good group,
except the one with the mouth.

224
00:11:50,843 --> 00:11:52,105
He's on a hell-bound train.

225
00:11:52,878 --> 00:11:55,608
Gary? Yeah.
I caught him sniffing the dry-erase markers.

226
00:11:55,781 --> 00:11:56,873
That's not gonna end pretty.

227
00:11:57,049 --> 00:11:58,311
[MlKE CHUCKLES]

228
00:11:58,484 --> 00:12:01,044
Okay, well, um,
thanks again for inviting me.

229
00:12:01,220 --> 00:12:03,347
You're welcome.
It was nice of you to come.

230
00:12:03,522 --> 00:12:05,319
Oh, it was my pleasure.

231
00:12:05,491 --> 00:12:06,515
So...

232
00:12:07,860 --> 00:12:09,885
...what made you wanna become
a teacher?

233
00:12:10,062 --> 00:12:11,120
[OBJECT CRASHES]

234
00:12:11,297 --> 00:12:13,731
[KlDS SHOUTlNG]

235
00:12:13,899 --> 00:12:15,799
I really don't remember.

236
00:12:16,302 --> 00:12:17,792
I gotta go.

237
00:12:18,104 --> 00:12:21,198
Oh. Oh, okay. Bye.

238
00:12:21,841 --> 00:12:23,274
Bye.

239
00:12:23,442 --> 00:12:24,773
[MOLLY CLEARS THROAT]

240
00:12:25,644 --> 00:12:28,545
-Hey, would you like to go out with me?
MOLLY: Shut up!

241
00:12:31,751 --> 00:12:33,844
Now, that was pathetic.

242
00:12:34,420 --> 00:12:36,581
Really? It didn't look as smooth as it felt?

243
00:12:37,757 --> 00:12:39,088
I'd lie to you if you want...

244
00:12:39,258 --> 00:12:42,193
... but I thought our relationship
had grown beyond that.

245
00:12:48,267 --> 00:12:55,173
Two meatball subs,
large curly fries and chocolate malt.

246
00:12:56,208 --> 00:12:57,732
Welcome back.

247
00:12:59,345 --> 00:13:00,403
Thanks, Samuel.

248
00:13:00,913 --> 00:13:02,346
What is this world coming to?

249
00:13:02,515 --> 00:13:05,814
Thirty-eight dollars to get lipstick
out of suede pants?

250
00:13:06,118 --> 00:13:08,712
-Whoa, what the hell are you doing?
-I'm having dinner.

251
00:13:08,888 --> 00:13:11,982
No, this isn't dinner.
This is suicide with meatball bullets.

252
00:13:12,491 --> 00:13:13,583
Come on. Give that back.

253
00:13:13,759 --> 00:13:16,421
You just lost 3 and a half pounds.

254
00:13:16,595 --> 00:13:19,462
Oh, big deal.
My farts weigh 3 and a half pounds.

255
00:13:19,865 --> 00:13:23,426
You don't have to tell me.
I ride in the car with you every day.

256
00:13:23,602 --> 00:13:27,595
Samuel, throw this away and bring
my partner a chicken breast on wheat toast?

257
00:13:27,773 --> 00:13:30,003
Throw it away? I don't think so.

258
00:13:30,176 --> 00:13:32,440
I will airdrop it to my village.

259
00:13:34,547 --> 00:13:37,948
There. Now, you may never have sex
without paying for it...

260
00:13:38,117 --> 00:13:40,210
... but you're still on your diet.

261
00:13:42,621 --> 00:13:44,282
Thanks.

262
00:13:44,924 --> 00:13:46,619
Come here. Give me some love.

263
00:13:51,997 --> 00:13:54,557
Sweet Jesus, it's like hugging a futon.

264
00:13:58,737 --> 00:14:03,299
I don't know. I guess I've had food issues
for as long as I can remember.

265
00:14:03,475 --> 00:14:08,003
Once, my dad had to write a check for $280
to the Girls Scouts of America...

266
00:14:08,180 --> 00:14:09,807
...for "unaccounted for cookies."

267
00:14:09,982 --> 00:14:11,574
[CHUCKLES]

268
00:14:12,418 --> 00:14:14,318
God bless him, he never said a word.

269
00:14:14,486 --> 00:14:17,011
Never made me feel bad about it.

270
00:14:17,189 --> 00:14:19,657
Of course he weighed about
a thousand pounds himself, so....

271
00:14:19,825 --> 00:14:20,849
[MOLLY SlGHS]

272
00:14:21,026 --> 00:14:23,824
Anyway, I know I'm never gonna be
a size two.

273
00:14:23,996 --> 00:14:27,363
I mean, and that's fine
because I happen to like who I am.

274
00:14:27,533 --> 00:14:29,763
There's nothing wrong with me
as a person.

275
00:14:29,935 --> 00:14:32,460
I'm smart, I'm funny, I recycle.

276
00:14:32,638 --> 00:14:33,798
[CHUCKLES]

277
00:14:33,973 --> 00:14:37,306
I just wanna learn to control my eating...

278
00:14:37,476 --> 00:14:41,378
...you know, and not keel over
in a White Castle drive-through like my dad.

279
00:14:41,547 --> 00:14:43,947
And it was his third lap.

280
00:14:46,352 --> 00:14:49,844
That's it. Oh, and I would love to be able
to walk into a nightclub...

281
00:14:50,022 --> 00:14:53,617
...without having every queen in the room
leaping on me like I'm a gay pride float.

282
00:14:53,792 --> 00:14:55,851
[CHUCKLlNG]

283
00:14:56,028 --> 00:14:57,620
Thank you.

284
00:14:59,098 --> 00:15:01,066
-Okay, who wants to share next?
-Hi, Molly.

285
00:15:01,233 --> 00:15:03,667
-I didn't see you there.
-Yeah, I lost another pound.

286
00:15:03,836 --> 00:15:05,098
-I'm becoming a rail.
-Ha-ha-ha.

287
00:15:06,805 --> 00:15:09,865
-Can I talk to you outside?
-Sure.

288
00:15:10,042 --> 00:15:12,442
-ls everything okay?
-Yeah, yeah, it's fine.

289
00:15:12,611 --> 00:15:14,135
I just, um....

290
00:15:14,613 --> 00:15:16,103
Well, uh, ahem...

291
00:15:16,282 --> 00:15:19,445
...I wanted to tell you that, uh....

292
00:15:20,953 --> 00:15:22,420
I like fish.

293
00:15:24,056 --> 00:15:25,751
All right.

294
00:15:26,058 --> 00:15:29,721
What I mean to say is
I'm sort of an amateur ichthyologist.

295
00:15:29,895 --> 00:15:32,261
So all types of fish?

296
00:15:32,431 --> 00:15:34,296
-Hey, you know that word.
-I'm a teacher.

297
00:15:34,466 --> 00:15:36,957
You'd be surprised at the words I know.

298
00:15:37,202 --> 00:15:40,000
Anyway, I like to go to the aquarium
on my days off...

299
00:15:40,172 --> 00:15:43,733
... because there's something soothing
about watching those giant creatures...

300
00:15:43,909 --> 00:15:46,742
...float around each other,
all graceful and weightless.

301
00:15:46,912 --> 00:15:48,675
Sounds like my water aerobics class.

302
00:15:48,847 --> 00:15:50,041
[CHUCKLES]

303
00:15:50,215 --> 00:15:51,910
That's funny.

304
00:15:52,084 --> 00:15:56,248
Anyway, uh, uh, I was wondering
if you weren't doing anything, if we could--

305
00:15:56,422 --> 00:15:58,856
Whoa! Oh!

306
00:15:59,024 --> 00:16:00,651
-Are you okay?
-Oh, I'm fine.

307
00:16:00,826 --> 00:16:01,884
You landed pretty hard.

308
00:16:02,061 --> 00:16:05,519
Oh, no, no, I'm good,
but I don't think that table's up to code.

309
00:16:05,698 --> 00:16:09,134
Oh, uh, is your finger supposed
to be pointed in that direction?

310
00:16:09,735 --> 00:16:13,171
-Whoa.
-No, it is not.

311
00:16:13,539 --> 00:16:15,200
Oh, that's bad.

312
00:16:16,642 --> 00:16:19,202
I should probably skedaddle over
to the emergency room.

313
00:16:19,378 --> 00:16:21,243
Yeah. Do you need somebody
to drive you?

314
00:16:21,413 --> 00:16:24,780
Oh, no, no, no. Life of a police officer.
Tuck and roll. Duck and cover.

315
00:16:24,950 --> 00:16:28,386
Don't vomit. Just get to the car.
Don't vomit.

316
00:16:34,760 --> 00:16:36,591
You ever gonna call that OA girl back?

317
00:16:36,762 --> 00:16:39,060
No, that, uh, that ship has sailed.

318
00:16:39,231 --> 00:16:41,131
I'm just gonna move on.

319
00:16:41,300 --> 00:16:44,792
-Mind if I tell you where you went wrong?
-Oh, would you?

320
00:16:45,237 --> 00:16:48,172
Women don't wanna hear a grown man
going on about ichthyology.

321
00:16:48,340 --> 00:16:50,638
It's got the word "ick" built right into it.

322
00:16:52,011 --> 00:16:54,275
You know, if you went with me
just once...

323
00:16:54,446 --> 00:16:57,711
...you would see how beautiful
and sensuous these creatures really are.

324
00:16:57,883 --> 00:17:02,286
Ain't no lady in the world gonna whip her
undies off hearing that kind of jibber-jabber.

325
00:17:02,621 --> 00:17:05,715
WOMAN [OVER RADlO]:
Got an 831 at 9425 Cicero Avenue.

326
00:17:05,891 --> 00:17:06,983
Handle code two.

327
00:17:08,093 --> 00:17:10,755
MlKE:
Car 79 in the vicinity.

328
00:17:10,929 --> 00:17:12,089
WOMAN:
Roger, Car 79.

329
00:17:13,499 --> 00:17:15,490
Thought about one of them
mail order brides?

330
00:17:15,667 --> 00:17:16,964
Here we go.

331
00:17:17,136 --> 00:17:18,763
Those women are very hot.

332
00:17:18,937 --> 00:17:23,203
Back when they were commies, they all
weighed 300 pounds and had mustaches.

333
00:17:23,542 --> 00:17:25,510
Democracy's what cleaned them
bitches up.

334
00:17:27,446 --> 00:17:28,606
And here's another idea:

335
00:17:28,781 --> 00:17:30,840
Maybe you could move to Japan.

336
00:17:31,016 --> 00:17:32,643
A big man is like a god over there.

337
00:17:33,352 --> 00:17:37,015
Do you ever actually stop to breathe or do
you have, like, a blowhole under your hat?

338
00:17:38,223 --> 00:17:39,315
[RlNGS DOORBELL]

339
00:17:39,491 --> 00:17:42,892
Seriously, yank your underwear up into your
crack and get you some sumo groupies.

340
00:17:45,898 --> 00:17:47,297
-Mike.
-Molly.

341
00:17:47,666 --> 00:17:49,031
What are you doing here?

342
00:17:49,201 --> 00:17:52,295
Uh, somebody called the cops
and we're the cops.

343
00:17:53,272 --> 00:17:55,467
Yeah, we called, like,
an hour and a half ago.

344
00:17:55,641 --> 00:17:58,701
Did you guys get lost or stop to make out
under a bridge somewhere?

345
00:17:59,845 --> 00:18:01,312
-Calm down, Mom.
JOYCE: Pssh.

346
00:18:01,480 --> 00:18:04,745
Seriously, though, did you guys stop
to get pizza or something?

347
00:18:05,417 --> 00:18:06,850
Wow, what are the odds, huh?

348
00:18:07,019 --> 00:18:08,418
Odds, my sweet mocha ass.

349
00:18:08,587 --> 00:18:10,817
This is a miracle of biblical proportions.

350
00:18:11,924 --> 00:18:12,948
How do you figure?

351
00:18:13,125 --> 00:18:16,720
God couldn't bring Molly to the mountain,
so he brought the mountain to Molly.

352
00:18:17,729 --> 00:18:19,663
I'm the mountain, right?

353
00:18:19,832 --> 00:18:22,596
You're the freaking Himalayas.
Get in there.

354
00:18:22,968 --> 00:18:25,493
It's gonna be okay, Mom. It's just stuff.

355
00:18:25,671 --> 00:18:29,505
Just stuff? Is that what you call
the diamond necklace your father gave me?

356
00:18:29,808 --> 00:18:31,105
Oh, God, I'm so sorry.

357
00:18:31,276 --> 00:18:35,007
Did you know he had to take a second job
to afford that thing?

358
00:18:35,180 --> 00:18:37,512
And I had to give him a third job
to get it.

359
00:18:39,685 --> 00:18:40,709
[SlGHS]

360
00:18:40,886 --> 00:18:44,413
-You think you'll find our things?
-I doubt it. We barely found your house.

361
00:18:44,590 --> 00:18:45,852
Hey.

362
00:18:46,158 --> 00:18:48,422
We're gonna do our best
to recover your property.

363
00:18:48,594 --> 00:18:53,031
-But we're gonna need to fill out a report.
VlCTORlA: They stole my water pipe.

364
00:18:53,465 --> 00:18:55,626
Luckily, I was a Girl Scout.

365
00:18:58,103 --> 00:18:59,593
Hi.

366
00:19:01,607 --> 00:19:03,074
-Apple?
-Uh, no, thanks.

367
00:19:03,242 --> 00:19:05,574
-They test our urine for fruit.
-Oh.

368
00:19:07,079 --> 00:19:09,877
Okay, can I get a list
of some of the missing items?

369
00:19:10,048 --> 00:19:15,543
Well, they took the TV, a couple of iPods,
uh, the computer, my mom's jewelry.

370
00:19:15,721 --> 00:19:18,087
I need serial numbers off the TV,
the computer.

371
00:19:18,257 --> 00:19:20,589
Any photographs you might have
of the property?

372
00:19:20,759 --> 00:19:22,386
And would you like to go out
with me sometime?

373
00:19:23,962 --> 00:19:26,760
What? Are you asking me out now?

374
00:19:27,032 --> 00:19:28,624
That is correct, ma'am.

375
00:19:29,201 --> 00:19:31,396
You had every chance to ask me out...

376
00:19:31,570 --> 00:19:34,596
...and you chose to ask me out
in the middle of a crime scene?

377
00:19:34,773 --> 00:19:38,834
I realize this is not the most opportune
moment, but I'm quite smitten with you.

378
00:19:39,077 --> 00:19:42,945
I'd hate to see this moment pass by without
telling you you're a remarkable woman...

379
00:19:43,115 --> 00:19:47,575
...with a lot of interesting qualities I find
both attractive and appealing, ma'am.

380
00:19:49,521 --> 00:19:52,285
Okay. That was pretty cute.

381
00:19:52,457 --> 00:19:53,924
So is that a yes or a no?

382
00:19:54,726 --> 00:19:56,250
Is it gonna go on the report?

383
00:19:56,828 --> 00:19:58,796
Depends on what your answer is.

384
00:20:00,232 --> 00:20:01,927
I would love to go out with you.

385
00:20:02,100 --> 00:20:04,625
Then, yes, I am writing that down.

386
00:20:07,005 --> 00:20:08,529
She better be good to him.

387
00:20:10,842 --> 00:20:12,400
[JOYCE GASPS]

388
00:20:12,578 --> 00:20:14,978
What do you want? I'm a sensitive guy.

389
00:20:43,642 --> 00:20:45,633
[English - US - SDH]

