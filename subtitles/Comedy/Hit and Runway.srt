﻿1
00:00:30,168 --> 00:00:32,908
Mr. Thumbs Up, I'm home.

2
00:01:32,938 --> 00:01:34,138
What brings you here?

3
00:01:37,138 --> 00:01:39,638
My dad said you can use
this for a reference.

4
00:01:40,138 --> 00:01:41,177
What is this?

5
00:01:41,178 --> 00:01:42,977
I think he found out
you've been writing songs.

6
00:01:42,978 --> 00:01:46,178
These are materials he's been
collecting regarding songwriting.

7
00:01:46,278 --> 00:01:49,718
He said there are music notes of
famous songwriters in there too.

8
00:01:50,088 --> 00:01:52,387
Has CEO Lee always been so considerate?

9
00:01:52,388 --> 00:01:55,758
It runs in our family. We may not
look considerate, but we are.

10
00:01:56,488 --> 00:01:58,658
I know CEO Lee is like that,

11
00:01:59,228 --> 00:02:01,098
but I don't know about you.

12
00:02:01,428 --> 00:02:04,398
Right, I had my TV debut today.

13
00:02:04,838 --> 00:02:07,207
I know. That's why I've been
so busy all day long...

14
00:02:07,208 --> 00:02:10,208
writing news releases for
you two in the office.

15
00:02:11,778 --> 00:02:14,347
The audience seemed to like us,

16
00:02:14,348 --> 00:02:16,207
and the director complimented us too.

17
00:02:16,208 --> 00:02:19,248
Great job, Ji Hoon.

18
00:02:20,988 --> 00:02:22,348
Were you eating?

19
00:02:22,588 --> 00:02:25,257
Yes, do you want to join?
It's enough for two.

20
00:02:25,258 --> 00:02:28,058
No, I just ate. I have to go.

21
00:02:28,258 --> 00:02:29,428
To practice?

22
00:02:29,658 --> 00:02:31,158
This is a hard-won chance,

23
00:02:31,258 --> 00:02:34,098
and I want to do my best
regardless of the result.

24
00:02:34,928 --> 00:02:37,138
Gosh, you're all grown up.

25
00:02:38,698 --> 00:02:41,138
- You should eat. Bye.
- Okay.

26
00:02:50,748 --> 00:02:52,318
- Goodbye.
- Bye.

27
00:03:04,258 --> 00:03:05,828
You did well, Ji Hoon.

28
00:03:07,598 --> 00:03:09,828
You did well. That was good enough.

29
00:03:35,228 --> 00:03:36,288
Mr. Thumbs Up.

30
00:03:39,428 --> 00:03:40,628
Are you eating all right?

31
00:03:45,738 --> 00:03:47,038
I'm sure it's night there too.

32
00:04:02,288 --> 00:04:03,318
Welcome...

33
00:04:05,518 --> 00:04:06,788
What brings you here?

34
00:04:07,088 --> 00:04:09,458
Why would I come to a bakery?
I came to get something.

35
00:04:09,728 --> 00:04:12,208
Can you make a recommendation for
a cake that's not too sweet?

36
00:04:25,078 --> 00:04:26,808
You can take this one.

37
00:04:27,278 --> 00:04:29,908
- Is this good?
- This is the most expensive.

38
00:04:30,178 --> 00:04:31,978
- You're rich.
- Gosh.

39
00:04:32,778 --> 00:04:35,348
You're the one who stole
our most popular singer.

40
00:04:35,648 --> 00:04:37,917
Because of that, we took a huge hit.

41
00:04:37,918 --> 00:04:40,238
You should've treated him better
while you had the chance.

42
00:04:43,658 --> 00:04:45,357
Hey, Young Jae.

43
00:04:45,358 --> 00:04:47,367
Hey, Bo Hee. It's been a while.

44
00:04:47,368 --> 00:04:50,537
You should stop by more often.
Our bread is really good.

45
00:04:50,538 --> 00:04:52,368
Oh, right. I should.

46
00:04:52,838 --> 00:04:55,768
Can you give me an
assortment of your bread?

47
00:04:55,938 --> 00:05:00,107
Hey, you are supposed to
pick out what you want.

48
00:05:00,108 --> 00:05:01,308
Oh, really?

49
00:05:03,508 --> 00:05:06,747
Is Gwang Jae good to you? If he
does anything wrong, call me.

50
00:05:06,748 --> 00:05:08,118
I'll do away with him.

51
00:05:09,218 --> 00:05:11,388
- Who are you to say that?
- What?

52
00:05:12,658 --> 00:05:14,227
I'm her neighbor.

53
00:05:14,228 --> 00:05:16,528
You're barely a neighbor.

54
00:05:18,398 --> 00:05:19,398
Here you are.

55
00:05:21,028 --> 00:05:22,798
- What's this?
- It's on the house.

56
00:05:24,238 --> 00:05:26,837
By the way, let me use
your recording studio.

57
00:05:26,838 --> 00:05:29,067
I have no time to reserve
a studio near here.

58
00:05:29,068 --> 00:05:31,537
So I'll use yours just for a while.

59
00:05:31,538 --> 00:05:34,208
You make such a brazen...

60
00:05:34,478 --> 00:05:35,508
demand.

61
00:05:37,118 --> 00:05:38,218
We'll see.

62
00:05:38,778 --> 00:05:39,847
You didn't even give me a bag.

63
00:05:39,848 --> 00:05:41,048
- Just leave.
- Bye.

64
00:05:46,458 --> 00:05:48,957
Be nice to Young Jae.

65
00:05:48,958 --> 00:05:51,397
He acts a bit sassy,

66
00:05:51,398 --> 00:05:53,397
but he's not a bad guy.

67
00:05:53,398 --> 00:05:55,327
What? Be nice?

68
00:05:55,328 --> 00:05:57,798
Young Jae and I are doing great right now.

69
00:06:02,768 --> 00:06:05,608
(5-year-old Ji Hoon. What a pretty boy.)

70
00:06:12,478 --> 00:06:14,788
Gosh, I'm tired.

71
00:06:18,458 --> 00:06:19,658
Man.

72
00:06:32,498 --> 00:06:33,498
Could you...

73
00:06:34,568 --> 00:06:36,868
put this...

74
00:06:37,878 --> 00:06:38,878
in the ice cream?

75
00:06:39,208 --> 00:06:40,707
I'm going to propose.

76
00:06:40,708 --> 00:06:41,878
Please and thank you.

77
00:06:42,808 --> 00:06:45,778
I'm really good at this.

78
00:06:51,158 --> 00:06:52,418
This is delicious.

79
00:06:58,258 --> 00:07:00,598
- Why are you staring?
- What?

80
00:07:00,828 --> 00:07:02,328
It's nothing. Go ahead and eat.

81
00:07:03,128 --> 00:07:05,238
- Eat up. It's good.
- Okay.

82
00:07:31,028 --> 00:07:32,128
Excuse...

83
00:07:33,768 --> 00:07:35,128
- I...
- What?

84
00:07:36,228 --> 00:07:37,338
What is it?

85
00:07:40,708 --> 00:07:41,708
I...

86
00:07:43,538 --> 00:07:45,978
need to go to the hospital.

87
00:07:46,238 --> 00:07:48,478
What? Hospital?

88
00:07:48,648 --> 00:07:51,948
Why did you swallow this?
That was a close call.

89
00:07:53,178 --> 00:07:55,948
It'll come out after a day or two.

90
00:07:56,388 --> 00:07:57,488
Do you see it here?

91
00:07:58,418 --> 00:08:00,428
Goodness. Isn't that...

92
00:08:01,288 --> 00:08:02,488
a ring?

93
00:08:10,598 --> 00:08:12,468
I'm sorry. Excuse me.

94
00:08:16,868 --> 00:08:19,808
I've swallowed and kept these
words in for 20 years.

95
00:08:21,008 --> 00:08:22,378
Maybe that's why...

96
00:08:23,378 --> 00:08:25,418
I swallowed the ring too.

97
00:08:26,178 --> 00:08:27,918
I don't have much,

98
00:08:28,918 --> 00:08:30,588
but I can make you happy.

99
00:08:32,118 --> 00:08:33,118
Will you...

100
00:08:34,458 --> 00:08:35,458
marry me?

101
00:08:51,708 --> 00:08:52,808
Thanks.

102
00:08:55,078 --> 00:08:56,678
My answer...

103
00:09:01,388 --> 00:09:02,448
is yes.

104
00:09:10,258 --> 00:09:11,757
Thank you.

105
00:09:11,758 --> 00:09:12,898
Congratulations.

106
00:09:14,198 --> 00:09:16,098
I hope it'll come out tonight.

107
00:09:21,068 --> 00:09:22,608
Hey.

108
00:09:23,408 --> 00:09:25,378
Should we have had a wedding?

109
00:09:25,578 --> 00:09:27,307
Later. We'll take it slow.

110
00:09:27,308 --> 00:09:30,207
Mal Sook is in our family registry.

111
00:09:30,208 --> 00:09:32,217
We're officially a family.

112
00:09:32,218 --> 00:09:35,148
Goodness. You're so impatient.

113
00:09:35,448 --> 00:09:37,888
- Should I look into it?
- I want to...

114
00:09:38,018 --> 00:09:39,918
have a proper wedding.

115
00:09:40,218 --> 00:09:42,388
I'll have it in the biggest hall...

116
00:09:42,688 --> 00:09:45,128
in the biggest hotel.

117
00:09:46,028 --> 00:09:47,128
Is that right?

118
00:09:47,398 --> 00:09:50,028
Then we'll do it slowly.

119
00:09:51,438 --> 00:09:52,868
Take your time.

120
00:09:53,038 --> 00:09:55,807
Your new business is the
most important right now.

121
00:09:55,808 --> 00:09:59,578
And stop coming down to the bakery.

122
00:09:59,878 --> 00:10:01,077
Well...

123
00:10:01,078 --> 00:10:03,177
A CEO should be in his office.

124
00:10:03,178 --> 00:10:04,178
Don't you think so?

125
00:10:04,618 --> 00:10:05,618
Is that right?

126
00:10:06,518 --> 00:10:08,487
By the way, about your album,

127
00:10:08,488 --> 00:10:10,517
I'm looking into it.

128
00:10:10,518 --> 00:10:12,687
Now that I have my own company,

129
00:10:12,688 --> 00:10:13,958
I can support you.

130
00:10:15,658 --> 00:10:16,688
No.

131
00:10:17,428 --> 00:10:18,728
Let's do that slowly too.

132
00:10:19,258 --> 00:10:20,368
What?

133
00:10:21,598 --> 00:10:23,998
I want to be a good mom and a good wife...

134
00:10:24,438 --> 00:10:25,898
first.

135
00:10:26,168 --> 00:10:27,408
After doing that,

136
00:10:27,808 --> 00:10:30,108
if I still want to be a singer,

137
00:10:30,408 --> 00:10:31,608
I'll let you know.

138
00:10:32,038 --> 00:10:34,948
You have to back me up then.

139
00:10:35,708 --> 00:10:36,808
Okay?

140
00:10:37,648 --> 00:10:38,748
Okay.

141
00:10:38,848 --> 00:10:40,988
Just tell me when you're ready.

142
00:10:56,628 --> 00:10:57,738
Mr. Thumbs Up!

143
00:10:59,338 --> 00:11:00,338
Mr. Thumbs Up!

144
00:11:00,608 --> 00:11:01,708
Mr. Thumbs Up!

145
00:11:02,108 --> 00:11:03,208
Mr. Thumbs Up.

146
00:11:04,238 --> 00:11:05,238
Mr. Thumbs Up!

147
00:11:17,318 --> 00:11:18,318
Mr. Thumbs Up.

148
00:11:19,758 --> 00:11:21,158
Why aren't you back?

149
00:11:23,558 --> 00:11:25,358
Did you forget me already?

150
00:11:37,508 --> 00:11:39,508
Friday at 11am will work.

151
00:11:40,308 --> 00:11:41,508
The location is...

152
00:11:41,848 --> 00:11:44,218
It's in Sangam? Yes, I know where that is.

153
00:11:45,448 --> 00:11:47,547
How will you conduct the interview?

154
00:11:47,548 --> 00:11:49,518
Your Americano is ready.

155
00:11:49,948 --> 00:11:51,288
Please wait a minute.

156
00:11:51,688 --> 00:11:53,088
Okay.

157
00:11:58,828 --> 00:11:59,828
Thank you.

158
00:12:06,868 --> 00:12:08,608
(Tuna Mayo Gimbap)

159
00:12:13,038 --> 00:12:15,778
(Triangular Gimbap)

160
00:12:17,448 --> 00:12:18,648
I'm sorry.

161
00:14:02,848 --> 00:14:05,317
When I woke up, I was on the street...

162
00:14:05,318 --> 00:14:06,758
in front of the World Entertainment.

163
00:14:07,428 --> 00:14:09,488
I guess I was laying there for a while.

164
00:14:11,128 --> 00:14:12,228
Then...

165
00:14:12,598 --> 00:14:14,998
what about Hyun Jae in 1994?

166
00:14:18,768 --> 00:14:21,138
You make him sound like a different person.

167
00:14:21,568 --> 00:14:22,868
He's also me.

168
00:14:23,808 --> 00:14:24,808
Well...

169
00:14:25,438 --> 00:14:28,208
You two don't seem like the same person.

170
00:14:30,178 --> 00:14:31,178
You're right.

171
00:14:31,378 --> 00:14:34,248
He's me, but he didn't seem like me.

172
00:14:36,048 --> 00:14:39,058
It's because we lived different times...

173
00:14:39,318 --> 00:14:40,638
and experienced different things.

174
00:14:40,928 --> 00:14:41,928
Then...

175
00:14:42,288 --> 00:14:44,628
what happened to him?

176
00:14:48,498 --> 00:14:49,628
I'm not sure.

177
00:14:54,668 --> 00:14:57,908
I guess he didn't want things
to change because of him.

178
00:14:58,678 --> 00:14:59,878
How do you know?

179
00:15:01,548 --> 00:15:02,648
Because I'm him.

180
00:15:02,848 --> 00:15:03,918
I just have a feeling.

181
00:15:06,278 --> 00:15:09,588
In any case, you're a really...

182
00:15:09,888 --> 00:15:10,888
weird person.

183
00:15:11,488 --> 00:15:13,828
What about you? What did
you do while I was gone?

184
00:15:13,988 --> 00:15:15,928
A lot of things happened.

185
00:15:16,028 --> 00:15:17,128
Did it?

186
00:15:17,728 --> 00:15:19,328
I have plenty of time on my hands.

187
00:15:19,498 --> 00:15:20,728
Tell me everything.

188
00:15:21,128 --> 00:15:23,938
I'm a great speaker,

189
00:15:24,168 --> 00:15:28,138
but I'm such a great listener too.

190
00:15:33,608 --> 00:15:35,008
Mr. Thumbs Up.

191
00:15:35,948 --> 00:15:39,448
You didn't get lost and
found your way back well.

192
00:15:45,458 --> 00:15:49,628
I'm not sure, but I guess
this is where I belong.

193
00:15:50,728 --> 00:15:51,958
I like it here more too.

194
00:15:53,898 --> 00:15:55,628
Oh, right.

195
00:16:02,038 --> 00:16:03,978
I think it's thanks to
the compass you gave me.

196
00:16:05,878 --> 00:16:07,248
Look.

197
00:16:08,308 --> 00:16:11,278
(Roar, Moo)

198
00:16:21,758 --> 00:16:23,228
It's delicious, isn't it?

199
00:16:25,358 --> 00:16:27,328
I'm getting more addicted to it.

200
00:16:27,528 --> 00:16:29,568
It wasn't this good in the beginning.

201
00:16:30,168 --> 00:16:32,328
- Do you have a place to stay?
- I should look for it.

202
00:16:32,438 --> 00:16:35,037
Then stay here until you find a place.

203
00:16:35,038 --> 00:16:38,637
I can't do that again
after how much I owe you.

204
00:16:38,638 --> 00:16:40,508
You didn't owe me much.

205
00:16:43,108 --> 00:16:45,417
Of course not. I'm the Yoo Hyun Jae.

206
00:16:45,418 --> 00:16:47,347
I never cross the line.

207
00:16:47,348 --> 00:16:49,518
Exactly. I trust you too.

208
00:16:52,018 --> 00:16:53,158
Do you want more rice?

209
00:16:53,388 --> 00:16:55,787
What? Yes, a little more, please.

210
00:16:55,788 --> 00:16:56,828
Sure.

211
00:17:00,568 --> 00:17:02,067
- Mr. Thumbs Up.
- What?

212
00:17:02,068 --> 00:17:04,368
- I'll go wash up.
- Okay.

213
00:17:15,048 --> 00:17:16,178
Is she taking a shower?

214
00:17:17,878 --> 00:17:21,218
Then again, she said she
was going to wash up.

215
00:17:28,728 --> 00:17:30,398
I don't know. Stop thinking about it.

216
00:17:36,198 --> 00:17:37,898
Why am I hearing the sound so clearly?

217
00:17:55,488 --> 00:17:58,318
Hyun Jae, you're better than this.

218
00:17:58,818 --> 00:18:01,428
Stop thinking about it.

219
00:18:12,838 --> 00:18:14,108
Gosh, Hyun Jae.

220
00:18:16,208 --> 00:18:18,538
You're much better than this.

221
00:18:19,478 --> 00:18:21,748
Stop it and be yourself.

222
00:18:52,108 --> 00:18:54,548
You should wash up too. Go in and wash up.

223
00:18:57,078 --> 00:18:58,118
What?

224
00:18:59,048 --> 00:19:00,788
Oh, well...

225
00:19:01,618 --> 00:19:05,618
I think I need to find a
place to stay quickly.

226
00:19:06,858 --> 00:19:09,328
I wasn't like this, but I don't
know what's wrong with me.

227
00:19:09,528 --> 00:19:10,658
Is that so?

228
00:19:11,828 --> 00:19:13,128
Hey, by the way,

229
00:19:14,468 --> 00:19:16,698
why do you look so tired all of a sudden?

230
00:19:18,038 --> 00:19:22,038
Isn't it some kind of side
effect of time traveling?

231
00:19:24,808 --> 00:19:27,448
Whatever. I barely won against myself.

232
00:19:28,478 --> 00:19:31,418
Right, I need to see Gwang Jae.

233
00:19:32,148 --> 00:19:33,148
You do?

234
00:19:33,748 --> 00:19:35,188
You'll be back soon, right?

235
00:19:36,188 --> 00:19:37,258
Yes.

236
00:19:48,098 --> 00:19:49,798
Will he be all right?

237
00:19:52,768 --> 00:19:54,968
Why did he drink so much water?

238
00:19:55,808 --> 00:19:58,038
Can you go back and forth...

239
00:19:58,638 --> 00:20:00,348
as you please?

240
00:20:01,248 --> 00:20:04,917
It's not as I please. I almost died.

241
00:20:04,918 --> 00:20:06,318
I'm never doing it again!

242
00:20:08,248 --> 00:20:11,088
Anyway, I wonder where
someone like you came from.

243
00:20:12,318 --> 00:20:13,728
It surprises me too.

244
00:20:18,128 --> 00:20:21,228
Oh, right. What about your treatment?

245
00:20:21,398 --> 00:20:23,997
Will it be cured with regular treatments?

246
00:20:23,998 --> 00:20:25,868
Oh, that?

247
00:20:26,338 --> 00:20:28,667
One of my fans became a professor.

248
00:20:28,668 --> 00:20:30,038
She's going to help me.

249
00:20:31,138 --> 00:20:33,708
But then again, those little kids...

250
00:20:34,148 --> 00:20:35,778
must be grownups by now.

251
00:20:37,248 --> 00:20:38,378
I guess.

252
00:20:40,748 --> 00:20:44,788
Anyway, I'm glad you're back.
Help me in our company.

253
00:20:44,888 --> 00:20:47,528
Train the kids and write songs.

254
00:20:48,228 --> 00:20:52,228
Wait a minute. You're young
enough to be an idol singer.

255
00:20:52,358 --> 00:20:54,098
Can't you debut again?

256
00:20:54,528 --> 00:20:57,638
Just once is enough to be on stage.

257
00:20:58,168 --> 00:21:00,568
I plan to work comfortably backstage.

258
00:21:02,168 --> 00:21:03,908
Sure, that's fine.

259
00:21:05,638 --> 00:21:08,547
By the way, I don't see Mr. Lee.

260
00:21:08,548 --> 00:21:11,848
He went on a kindergarten
picnic with Mal Sook today.

261
00:21:12,418 --> 00:21:14,318
What? Is he all right?

262
00:21:15,218 --> 00:21:18,317
He said he'd be the first
patient to defeat dementia.

263
00:21:18,318 --> 00:21:20,927
He exercises like crazy these days.

264
00:21:20,928 --> 00:21:24,727
He was determined to win
all the prizes today.

265
00:21:24,728 --> 00:21:26,028
He talked so big as he left.

266
00:21:27,628 --> 00:21:28,628
Then again,

267
00:21:29,398 --> 00:21:32,898
he's naturally great in being so stubborn.

268
00:21:33,638 --> 00:21:36,638
Dementia might give up and run away first.

269
00:21:38,008 --> 00:21:39,178
I know.

270
00:21:42,448 --> 00:21:45,948
But why did you help me out like so?

271
00:21:47,818 --> 00:21:51,018
- I was your manager.
- You were more than a manager.

272
00:21:54,088 --> 00:21:56,328
I don't know.

273
00:21:57,198 --> 00:21:58,628
You seemed lonely.

274
00:21:59,858 --> 00:22:02,298
- What?
- That's how I felt.

275
00:22:03,368 --> 00:22:06,238
I thought that you were like that
because you were lonely like me.

276
00:22:07,038 --> 00:22:11,108
That thought helped me
bear with your temper.

277
00:22:16,608 --> 00:22:17,948
Let's keep getting along.

278
00:22:18,118 --> 00:22:20,178
You should address me CEO Lee now.

279
00:22:21,118 --> 00:22:23,348
I didn't say I'd work for your company.

280
00:22:23,448 --> 00:22:26,387
I'll be a freelancer. Do you get it?

281
00:22:26,388 --> 00:22:30,228
Look at you trying to
use your petty tricks.

282
00:22:32,958 --> 00:22:34,228
Hyun Jae.

283
00:22:34,728 --> 00:22:35,768
What?

284
00:22:35,868 --> 00:22:37,468
Don't go anywhere far away now.

285
00:22:38,638 --> 00:22:42,208
Let's grow old together
at least from now on.

286
00:22:43,008 --> 00:22:44,038
Okay?

287
00:22:49,108 --> 00:22:51,078
All right, let's do that.

288
00:22:56,218 --> 00:22:57,258
One and...

289
00:23:04,628 --> 00:23:06,858
- Hold on. I think someone's here.
- What?

290
00:23:08,528 --> 00:23:10,868
Who? No one's here.

291
00:23:11,068 --> 00:23:13,038
Is that so? Oh, right.

292
00:23:13,338 --> 00:23:15,707
You said so much about your stage fright,

293
00:23:15,708 --> 00:23:17,138
but you were meant for the stage.

294
00:23:17,778 --> 00:23:20,178
I had no idea either,

295
00:23:20,448 --> 00:23:22,177
but I guess I was.

296
00:23:22,178 --> 00:23:24,147
How did you hold it in all these years?

297
00:23:24,148 --> 00:23:26,248
Look who's talking.

298
00:23:27,388 --> 00:23:30,357
Should we make Korea go crazy for us?

299
00:23:30,358 --> 00:23:33,858
Let's take this opportunity
and do just that.

300
00:23:34,328 --> 00:23:36,128
- We're JB!
- JB!

301
00:23:39,528 --> 00:23:40,868
- Hello!
- Hello!

302
00:23:41,968 --> 00:23:43,028
Hello, guys.

303
00:23:44,738 --> 00:23:46,538
I'll meet up with my old
friends and go back.

304
00:23:46,698 --> 00:23:48,868
They're all excited after
seeing me on television.

305
00:23:49,708 --> 00:23:52,278
- Don't drink alcohol.
- All right.

306
00:23:59,878 --> 00:24:01,488
What? You...

307
00:24:04,958 --> 00:24:06,658
I'm back again.

308
00:24:09,428 --> 00:24:10,898
I knew you'd be back.

309
00:24:12,958 --> 00:24:15,728
Why do you always say you knew it?

310
00:24:16,568 --> 00:24:19,168
I heard you debuted. Congratulations.

311
00:24:19,368 --> 00:24:22,508
Thanks. Are you going
to stay here for good?

312
00:24:23,008 --> 00:24:25,578
- Yes.
- By Woo Seung's side?

313
00:24:27,778 --> 00:24:30,578
Well, yes.

314
00:24:33,118 --> 00:24:35,948
- Where's Drill?
- He went out to meet his friends.

315
00:24:39,988 --> 00:24:42,558
I met your dad.

316
00:24:44,458 --> 00:24:47,328
I wanted to tell you about your dad.

317
00:24:48,268 --> 00:24:49,798
About why he made that choice.

318
00:24:54,838 --> 00:24:59,378
At that critical moment, he
kept on asking about you.

319
00:25:00,178 --> 00:25:02,078
I told him how nice of a kid you are.

320
00:25:05,148 --> 00:25:06,578
I showed him your picture too.

321
00:25:07,418 --> 00:25:09,058
I told him you were debuting as a singer.

322
00:25:09,788 --> 00:25:12,218
- What did he say?
- He was proud of it.

323
00:25:12,688 --> 00:25:13,928
Was he?

324
00:25:16,628 --> 00:25:17,798
Here.

325
00:25:19,258 --> 00:25:21,228
Your dad wanted me to give this to you.

326
00:25:25,198 --> 00:25:26,598
Isn't it superb?

327
00:25:27,268 --> 00:25:29,638
Yes, it's superb.

328
00:25:42,288 --> 00:25:43,288
Hey.

329
00:25:44,288 --> 00:25:45,688
Let's make this great.

330
00:25:46,818 --> 00:25:48,628
Yes, let's make this great.

331
00:25:50,498 --> 00:25:52,797
No matter how much I think about this,

332
00:25:52,798 --> 00:25:54,898
don't you think this...

333
00:25:55,028 --> 00:25:56,868
is really strange?

334
00:25:57,598 --> 00:26:00,268
Yes, it's really strange.

335
00:26:03,238 --> 00:26:05,278
Hey, cheers.

336
00:26:10,208 --> 00:26:11,218
Hey.

337
00:26:11,678 --> 00:26:12,678
What are you doing here?

338
00:26:15,148 --> 00:26:16,148
I...

339
00:26:16,448 --> 00:26:17,487
was waiting for you.

340
00:26:17,488 --> 00:26:19,164
Why were you standing out here?

341
00:26:19,188 --> 00:26:20,388
I got nervous.

342
00:26:20,988 --> 00:26:22,428
I thought you would disappear again.

343
00:26:23,028 --> 00:26:24,128
What?

344
00:26:24,628 --> 00:26:25,628
Give me your arm.

345
00:26:25,758 --> 00:26:26,858
My arm?

346
00:26:42,048 --> 00:26:43,248
This is too much.

347
00:26:43,448 --> 00:26:45,518
You can't go anywhere...

348
00:26:45,678 --> 00:26:47,218
without me anymore.

349
00:26:48,918 --> 00:26:51,187
(Woo Seung's slave, Hyun Jae)

350
00:26:51,188 --> 00:26:53,928
I'll pay you back someday.

351
00:27:12,508 --> 00:27:13,608
Part-timer.

352
00:27:15,648 --> 00:27:17,118
You scared me.

353
00:27:17,578 --> 00:27:18,648
Hey.

354
00:27:19,878 --> 00:27:21,087
Why are you in there?

355
00:27:21,088 --> 00:27:22,547
I live here now.

356
00:27:22,548 --> 00:27:24,087
- What?
- I was looking for a room,

357
00:27:24,088 --> 00:27:25,717
and I thought I'd check this place out.

358
00:27:25,718 --> 00:27:27,227
And it turns out this house was empty.

359
00:27:27,228 --> 00:27:30,258
Anyway, let's have a great
time as neighbors, okay?

360
00:27:30,898 --> 00:27:31,958
Are you seriously...

361
00:27:32,698 --> 00:27:34,267
- How did you...
- Come on.

362
00:27:34,268 --> 00:27:35,927
I won't bother you, don't worry.

363
00:27:35,928 --> 00:27:37,368
Bye. See you later, neighbor.

364
00:27:41,338 --> 00:27:43,007
When will you eat? What
are you going to eat?

365
00:27:43,008 --> 00:27:44,878
I'm not curious at all. Bye.

366
00:27:48,478 --> 00:27:50,077
Do you want to eat out tonight?

367
00:27:50,078 --> 00:27:51,147
You can always eat alone,

368
00:27:51,148 --> 00:27:53,717
but it's a crime, a serious
crime to eat alone...

369
00:27:53,718 --> 00:27:56,148
with a superb next door neighbor like me.

370
00:27:57,718 --> 00:27:58,988
You're really...

371
00:27:59,618 --> 00:28:01,458
You're really weird.

372
00:28:01,958 --> 00:28:06,528
Come on, you mean that I'm really superb.

373
00:28:15,738 --> 00:28:17,837
It looks like you two
are pretty popular now.

374
00:28:17,838 --> 00:28:20,907
We're a duo born to live off of popularity.

375
00:28:20,908 --> 00:28:22,307
Of course, man.

376
00:28:22,308 --> 00:28:23,308
JB.

377
00:28:24,548 --> 00:28:26,177
Right, I heard you two moved.

378
00:28:26,178 --> 00:28:28,988
Oh, MC Drill and I moved into a studio.

379
00:28:29,388 --> 00:28:32,517
Then what about this place? Is it empty?

380
00:28:32,518 --> 00:28:33,787
For the time being.

381
00:28:33,788 --> 00:28:34,888
Really?

382
00:28:35,488 --> 00:28:38,398
Woo Seung, how is your
songwriting coming along?

383
00:28:38,828 --> 00:28:40,158
I've been doing it,

384
00:28:40,698 --> 00:28:42,828
but it's not going as well as I thought.

385
00:28:43,368 --> 00:28:45,368
I thought so. You look more like...

386
00:28:45,768 --> 00:28:48,308
a civil servant.

387
00:28:48,638 --> 00:28:49,837
If you write a great song,

388
00:28:49,838 --> 00:28:51,338
you have to give it to us first.

389
00:28:52,238 --> 00:28:53,338
Okay.

390
00:28:55,208 --> 00:28:56,728
Don't you have anything that's colder?

391
00:28:57,778 --> 00:28:59,218
Of course, I do.

392
00:29:01,248 --> 00:29:02,348
Here.

393
00:29:02,888 --> 00:29:05,358
- My gosh, it's so cold.
- Nice.

394
00:29:05,858 --> 00:29:06,988
Awesome.

395
00:29:08,958 --> 00:29:10,398
- Nice.
- Awesome.

396
00:29:10,798 --> 00:29:12,557
- Thumbs up.
- Thumbs up.

397
00:29:12,558 --> 00:29:15,498
- Thumbs up.
- Thumbs up.

398
00:29:16,028 --> 00:29:19,438
- Thumbs, up, up, up, up.
- What was that?

399
00:29:19,738 --> 00:29:23,038
We talked and laughed for
a long time that night.

400
00:29:23,368 --> 00:29:26,308
Our time now may not seem like much.

401
00:29:26,508 --> 00:29:28,507
Everything we have may seem...

402
00:29:28,508 --> 00:29:30,448
like it's nothing and trivial.

403
00:29:30,718 --> 00:29:31,977
However, like a poet once said,

404
00:29:31,978 --> 00:29:33,278
the greatest poem...

405
00:29:33,478 --> 00:29:35,088
has not been written yet,

406
00:29:35,418 --> 00:29:37,018
and the greatest song...

407
00:29:37,188 --> 00:29:38,858
has not been sung yet.

408
00:29:39,518 --> 00:29:42,258
Our best days are the days
we haven't lived yet.

409
00:29:43,128 --> 00:29:44,898
That's why the present...

410
00:29:45,158 --> 00:29:47,398
and everything we do now
are precious to us.

411
00:29:48,228 --> 00:29:51,838
We still have our best hit coming.

412
00:29:52,438 --> 00:29:53,838
We did this the last time.

413
00:29:53,938 --> 00:29:55,267
- Let's go.
- Let's go!

414
00:29:55,268 --> 00:29:56,738
- Let's go.
- Let's go!

415
00:30:24,668 --> 00:30:26,598
(The Great Yoo Hyun Jae)

416
00:30:36,978 --> 00:30:38,578
(Roar, Moo)

417
00:30:48,528 --> 00:30:52,098
This is pretty superb.

418
00:31:16,588 --> 00:31:17,818
Hey, neighbor.

419
00:31:18,388 --> 00:31:20,928
What? How did you know I moved in here?

420
00:31:21,928 --> 00:31:24,828
Have you forgotten that I
work for World Entertainment?

421
00:31:26,098 --> 00:31:28,368
How dare you run away without telling me?

422
00:31:30,538 --> 00:31:32,137
I didn't run away.

423
00:31:32,138 --> 00:31:34,038
Do you know how expensive that place was?

424
00:31:34,208 --> 00:31:36,168
Are you sure you aren't
trying to run away again?

425
00:31:36,268 --> 00:31:37,878
I can't go anywhere without you.

426
00:31:42,178 --> 00:31:44,718
Why do you have things on your face?

427
00:31:46,018 --> 00:31:47,264
Do I have something on my face?

428
00:31:47,288 --> 00:31:48,288
Right here.

429
00:32:40,808 --> 00:32:43,568
(The Best Hit)

430
00:32:45,738 --> 00:32:47,108
I didn't mean to do that.

431
00:32:47,278 --> 00:32:51,418
Woo Seung doesn't have beer.

432
00:32:54,488 --> 00:32:56,088
Can you please say "Cut"?

433
00:33:02,458 --> 00:33:04,728
Are you the one who has been scaring me...

434
00:33:05,498 --> 00:33:07,468
every single night?

435
00:33:08,098 --> 00:33:09,898
Wait, that's not right.
How should I do this?

436
00:33:10,898 --> 00:33:12,168
Can you help me out?

437
00:33:15,608 --> 00:33:16,608
Cut.

438
00:33:18,238 --> 00:33:19,338
Look into my eyes.

439
00:33:19,508 --> 00:33:20,708
Nonsense.

440
00:33:21,178 --> 00:33:22,684
He can use the storage by himself.

441
00:33:22,708 --> 00:33:24,818
Hey, what...

442
00:33:25,278 --> 00:33:26,817
He can use the storage by himself.

443
00:33:26,818 --> 00:33:28,388
Hey, what about me?

444
00:33:31,088 --> 00:33:32,088
Hey!

445
00:33:34,988 --> 00:33:36,188
I'm sorry.

446
00:33:36,488 --> 00:33:38,128
Are you serious?

447
00:33:38,758 --> 00:33:41,128
250 joules. Charge 250 joules.

448
00:33:41,428 --> 00:33:42,788
Was I looking too into the camera?

449
00:33:43,598 --> 00:33:44,838
Let's go, let's go, let's go.

450
00:33:45,068 --> 00:33:47,908
I'm Choi Woo Seung. In the
end, Woo Seung will win.

451
00:33:53,878 --> 00:33:55,047
I'm sorry.

452
00:33:55,048 --> 00:33:58,117
You jerk. This is so embarrassing.

453
00:33:58,118 --> 00:34:00,548
I raised her so preciously.

454
00:34:02,488 --> 00:34:03,488
Who did that?

455
00:34:05,658 --> 00:34:08,157
Being fixed is... My gosh, I'm sorry.

456
00:34:08,158 --> 00:34:09,287
I'm so sorry.

457
00:34:09,288 --> 00:34:11,357
(Thank you for watching The Best Hit.)

458
00:34:11,358 --> 00:34:13,257
- I have a line.
- I'm sorry.

459
00:34:13,258 --> 00:34:15,668
(Thank you for watching The Best Hit.)

460
00:34:15,868 --> 00:34:23,678
("Our best days have yet to
be lived." Nazim Hikmet)

461
00:34:24,408 --> 00:34:25,477
Stop it.

462
00:34:25,478 --> 00:34:26,977
My gosh, Tae Hyun.

463
00:34:26,978 --> 00:34:29,007
I heard you walking out.

464
00:34:29,008 --> 00:34:30,517
- Did you?
- Yes, we can hear you.

465
00:34:30,518 --> 00:34:31,518
Cut.

