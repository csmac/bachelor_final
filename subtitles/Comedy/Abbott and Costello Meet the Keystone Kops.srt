1
00:00:43,320 --> 00:00:48,725
[Meet Me @1006]

2
00:01:36,220 --> 00:01:38,087
I met you in order to save you.

3
00:01:38,087 --> 00:01:41,153
I can't think of a better
explanation than destiny.

4
00:01:41,153 --> 00:01:44,619
I think I'll sleep on it.
I'll answer you tomorrow.

5
00:01:45,819 --> 00:01:47,554
Zhou Da Jun, I'm you from the future.

6
00:01:47,554 --> 00:01:50,019
Ask for leave and stay home tomorrow.

7
00:01:50,019 --> 00:01:52,019
Someone may sneak into your place.

8
00:02:07,054 --> 00:02:08,119
Where's Jia Le?

9
00:02:09,986 --> 00:02:11,887
Oh, the merging is over.

10
00:02:11,887 --> 00:02:13,819
How did it go?

11
00:02:13,819 --> 00:02:16,954
I know. You told her with masculinity...

12
00:02:16,954 --> 00:02:18,520
"Don't worry, Jia Le."

13
00:02:18,520 --> 00:02:21,187
"I'll get you back from Death."

14
00:02:21,187 --> 00:02:26,853
Then you gave her a kiss
before the merging ended.

15
00:02:26,853 --> 00:02:29,253
Are you still an adolescent?

16
00:02:29,253 --> 00:02:31,485
Grow up, will you?

17
00:02:31,485 --> 00:02:33,120
What do you have on Zhang De Pei?

18
00:02:33,120 --> 00:02:35,853
I went to the training center.

19
00:02:35,853 --> 00:02:38,019
But he didn't even show up today..

20
00:02:38,019 --> 00:02:39,552
Then I went to his place.

21
00:02:39,552 --> 00:02:43,353
I rang the bell, but no one answered.

22
00:02:43,353 --> 00:02:45,153
I'll have to go again tomorrow.

23
00:02:45,752 --> 00:02:48,786
We've wasted another day.
Time is running out.

24
00:02:52,976 --> 00:02:54,656
[Tie]

25
00:03:00,985 --> 00:03:02,218
I have to get this.

26
00:03:05,786 --> 00:03:06,985
Not bad.

27
00:03:07,919 --> 00:03:09,286
Let me feel it.

28
00:03:09,286 --> 00:03:10,718
It's cool.

29
00:03:11,353 --> 00:03:14,218
A new hairstyle changes your mood.

30
00:03:17,187 --> 00:03:21,387
You've never met me
in such a quiet place.

31
00:03:22,651 --> 00:03:25,253
I can't go to the pub anymore.

32
00:03:26,052 --> 00:03:30,552
Although I'm on probation
for abandoning a corpse...

33
00:03:31,319 --> 00:03:35,718
some still think I'm the
killer and try to avoid me.

34
00:03:35,718 --> 00:03:37,052
It's normal.

35
00:03:38,086 --> 00:03:43,286
But I think I'm better than I used to be.

36
00:03:44,485 --> 00:03:46,218
You got too many girls.

37
00:03:46,218 --> 00:03:49,286
God is punishing you and
forcing you to take a break.

38
00:03:59,286 --> 00:04:03,718
Didn't you tell me you
were in love with a girl?

39
00:04:03,718 --> 00:04:05,620
What happened?

40
00:04:05,620 --> 00:04:09,120
When did I tell you
that I was in love? Nonsense!

41
00:04:09,120 --> 00:04:11,319
I can see it from your face.

42
00:04:12,218 --> 00:04:16,286
I'd help you if I weren't
busy with the lawsuit.

43
00:04:18,286 --> 00:04:21,718
So? What does the girl
you love look like?

44
00:04:21,718 --> 00:04:23,287
Was she that good?

45
00:04:25,520 --> 00:04:30,652
I realized that she wasn't
who I had expected.

46
00:04:33,253 --> 00:04:34,421
So I gave up on her.

47
00:05:00,453 --> 00:05:04,020
Xiao Wei, are you available this weekend?

48
00:05:04,020 --> 00:05:05,820
I have something important to tell you.

49
00:05:16,087 --> 00:05:20,686
Xiao Wei, can you have
dinner with me this weekend?

50
00:05:22,852 --> 00:05:24,287
Cheers.

51
00:05:28,220 --> 00:05:30,787
Are you drunk?

52
00:05:30,787 --> 00:05:33,619
- No, I'm not.
- Really?

53
00:05:33,619 --> 00:05:37,787
- One plus one equals three.
- Really?

54
00:05:47,520 --> 00:05:52,986
Are you feeling something?

55
00:06:30,719 --> 00:06:31,919
Naughty girl.

56
00:06:33,628 --> 00:06:38,928
[Bo]

57
00:06:43,719 --> 00:06:47,587
You don't answer the phone?
Are you with a girl?

58
00:06:47,587 --> 00:06:50,054
Yeah, you're forcing me
to go to your place.

59
00:07:02,885 --> 00:07:03,953
Tie?

60
00:07:05,253 --> 00:07:06,385
Tie?

61
00:07:14,344 --> 00:07:17,953
The door's open.

62
00:07:18,953 --> 00:07:20,852
And the music's loud.

63
00:07:22,686 --> 00:07:25,885
Tie? I'm coming in.

64
00:07:26,554 --> 00:07:29,751
You'd better get dressed now.

65
00:07:30,554 --> 00:07:31,919
I'm coming in now.

66
00:07:33,587 --> 00:07:34,953
Are you drunk?

67
00:07:38,520 --> 00:07:40,253
Yeah, you're drunk.

68
00:08:09,421 --> 00:08:11,385
Is he your type?

69
00:08:11,986 --> 00:08:13,486
Of course not.

70
00:08:13,486 --> 00:08:17,520
I don't like those boys
who think they're popular.

71
00:10:34,052 --> 00:10:37,819
What kind of girl was she? Just tell me.

72
00:10:41,420 --> 00:10:43,487
I thought she was an angel.

73
00:10:45,454 --> 00:10:50,586
But a fallen angel has a dark side too.

74
00:10:51,787 --> 00:10:54,320
That's what those girls are like.

75
00:10:54,320 --> 00:10:57,153
You're such a neat freak in love.

76
00:10:57,153 --> 00:11:00,487
Everyone has a dark side,
including you and me.

77
00:11:00,487 --> 00:11:03,386
But I don't think you're
a bad guy because of that.

78
00:11:03,386 --> 00:11:05,086
Really?

79
00:11:09,795 --> 00:11:12,219
Don't jump to conclusion.

80
00:11:13,521 --> 00:11:15,686
What? You're frightening me.

81
00:11:16,987 --> 00:11:18,719
Just kidding.

82
00:11:28,287 --> 00:11:31,287
Judging from my experience

83
00:11:31,287 --> 00:11:34,586
even a silly girl can be complicated.

84
00:11:34,586 --> 00:11:36,521
But you don't have to get to know her.

85
00:11:36,521 --> 00:11:38,119
Like you can play a video game

86
00:11:38,119 --> 00:11:40,018
without knowing how it was designed.

87
00:11:40,018 --> 00:11:42,420
You can have a good time playing it.

88
00:11:44,320 --> 00:11:48,386
Was Xiao Wei a toy for
you to kill time with?

89
00:11:50,686 --> 00:11:53,619
You're weird today.

90
00:11:53,619 --> 00:11:57,954
Yeah, if you're comparing her to a toy

91
00:11:57,954 --> 00:12:03,719
she was one I wanted
to cherish for life.

92
00:12:06,153 --> 00:12:08,018
But...

93
00:12:10,186 --> 00:12:12,920
So I'm upset now.

94
00:12:12,920 --> 00:12:15,018
They haven't caught the killer.

95
00:12:15,018 --> 00:12:18,052
I don't know what's
wrong with the police.

96
00:13:34,454 --> 00:13:35,454
Hello?

97
00:13:35,454 --> 00:13:37,619
What time is it? Where are you?

98
00:13:38,386 --> 00:13:40,153
I'm almost at the office.

99
00:13:40,153 --> 00:13:44,018
I'm sitting on your
motorbike near your place.

100
00:13:44,018 --> 00:13:46,086
Yeah, I'm heading out now.

101
00:13:46,086 --> 00:13:47,386
Hold on a second.

102
00:13:59,153 --> 00:14:02,153
Zhou Da Jun, I'm you from the future.

103
00:14:05,086 --> 00:14:09,052
Get rid of the black coat
that you wore today.

104
00:14:52,287 --> 00:14:53,851
What do you want, sir?

105
00:14:53,851 --> 00:14:55,787
I want the surveillance video.

106
00:14:56,586 --> 00:14:58,652
I'm the owner of this building.

107
00:15:17,354 --> 00:15:19,420
He did go out that night.

108
00:15:30,119 --> 00:15:31,886
2:02 a.m.

109
00:15:43,386 --> 00:15:46,487
Why was he so nervous? He even threw up.

110
00:16:24,320 --> 00:16:25,320
Hey, Jia Le.

111
00:16:26,186 --> 00:16:27,287
Here.

112
00:16:30,719 --> 00:16:32,954
These paintings are all unfinished.

113
00:16:32,954 --> 00:16:36,219
It's been hard for me to calm down.

114
00:16:36,219 --> 00:16:39,521
Even if I do

115
00:16:39,521 --> 00:16:42,787
I don't want to go on.
I want to start anew.

116
00:16:48,552 --> 00:16:50,819
If you can start anew...

117
00:16:51,787 --> 00:16:55,619
you won't have to apologize to me, right?

118
00:17:00,851 --> 00:17:05,686
By the end of the interview,
you said sorry to me.

119
00:17:06,954 --> 00:17:09,619
I'm the one who should say sorry.

120
00:17:10,819 --> 00:17:12,886
Why do you have to say sorry?

121
00:17:19,554 --> 00:17:22,752
I think you knew all along
who killed Cheng Hao.

122
00:17:22,752 --> 00:17:25,386
But you didn't want to tell the truth.

123
00:17:25,386 --> 00:17:27,920
That's why you felt sorry.

124
00:17:29,786 --> 00:17:32,554
What makes you think that?

125
00:17:37,452 --> 00:17:38,586
Ji Rou...

126
00:17:41,652 --> 00:17:43,153
What are you doing?

127
00:17:44,185 --> 00:17:49,286
On the day of the murder,
you threw the butt away.

128
00:17:54,118 --> 00:17:58,452
So you think I was trying
to hide evidence?

129
00:17:58,452 --> 00:18:01,819
And the owner of
the butt is the real killer?

130
00:18:01,819 --> 00:18:04,252
Zhang De Pei doesn't smoke.

131
00:18:04,252 --> 00:18:06,420
There was someone else at the scene.

132
00:18:14,120 --> 00:18:16,054
On that day...

133
00:18:18,118 --> 00:18:23,321
was it a cigarette like
this that you saw?

134
00:18:27,220 --> 00:18:29,585
These are my cigarettes.

135
00:18:29,585 --> 00:18:32,518
I use them sometimes for inspiration.

136
00:18:46,885 --> 00:18:49,986
If the cigarette butt was yours

137
00:18:49,986 --> 00:18:52,452
why did you move it?

138
00:18:52,452 --> 00:18:54,118
Did I?

139
00:18:54,118 --> 00:18:58,518
To be honest, I don't
remember doing that.

140
00:18:58,518 --> 00:19:00,085
I don't believe you.

141
00:19:05,184 --> 00:19:07,552
Cheng Hao knew that I smoke.

142
00:19:07,552 --> 00:19:10,318
When I'm in a bad mood, I paint

143
00:19:10,318 --> 00:19:13,750
and do household chores,
putting stuff away.

144
00:19:13,750 --> 00:19:16,518
But he can't stand up for me now.

145
00:19:22,085 --> 00:19:25,352
You even want Cheng Hao to
back you up when you lie.

146
00:19:25,352 --> 00:19:27,184
And Ke Zhen Yu too.

147
00:19:27,184 --> 00:19:30,786
He tries to defend you and you
don't tell him the truth.

148
00:19:32,618 --> 00:19:38,152
You use the men who love you
to solve your problems?

149
00:19:38,152 --> 00:19:42,085
You get a sense of achievement from that?

150
00:19:43,385 --> 00:19:48,152
Many people's futures
depend on this case, Ji Rou.

151
00:19:49,452 --> 00:19:52,251
Are you jealous of me?

152
00:19:53,750 --> 00:19:55,652
It's all right.

153
00:19:55,652 --> 00:20:00,750
I'm used to people being jealous of me.

154
00:20:00,750 --> 00:20:05,352
But do you think it's easy for a widow

155
00:20:05,352 --> 00:20:10,552
to mourn for her husband
while being slammed

156
00:20:10,552 --> 00:20:12,618
at the same time?

157
00:20:15,018 --> 00:20:20,018
I'm trying to keep calm
and answer your questions

158
00:20:20,018 --> 00:20:24,220
because I see you as Cheng Hao's sister.

159
00:20:24,220 --> 00:20:25,852
You're family.

160
00:20:40,385 --> 00:20:42,652
Don't be so ungrateful.

161
00:20:44,618 --> 00:20:49,719
I'm not buying your
tricks as a journalist.

162
00:20:49,719 --> 00:20:53,220
You think that I killed my husband.

163
00:20:53,220 --> 00:20:55,952
You think that I'm having
an affair with De Pei.

164
00:20:55,952 --> 00:20:59,184
Now you think that I had an accomplice.

165
00:21:00,552 --> 00:21:04,486
If you see my brother smoking

166
00:21:04,486 --> 00:21:06,918
will you think he's the killer?

167
00:21:06,918 --> 00:21:08,486
You saw him beating Cheng Hao.

168
00:21:08,486 --> 00:21:12,152
You can make up a story about
his grudge against him.

169
00:21:12,152 --> 00:21:15,385
It's what you journalists
are good at, isn't it?

170
00:21:35,952 --> 00:21:38,719
You haven't seen me smoking. Is it weird?

171
00:22:22,719 --> 00:22:24,885
Can I talk to you, Miss Cheng?

172
00:22:32,352 --> 00:22:37,452
Believe it or not,
I was trying to help you.

173
00:22:37,452 --> 00:22:42,684
You're best friends. It must
be hard for you to ask her.

174
00:22:45,251 --> 00:22:49,220
But don't you think she
was dodging my questions?

175
00:22:49,220 --> 00:22:51,652
I'm not talking to you about this.

176
00:22:51,652 --> 00:22:53,818
I have some personal questions for you.

177
00:22:56,719 --> 00:22:59,986
I admit that I've misjudged you.

178
00:23:01,184 --> 00:23:03,053
About what?

179
00:23:03,053 --> 00:23:06,220
I talked to the future me
on the phone yesterday.

180
00:23:06,220 --> 00:23:08,184
I realized that he...

181
00:23:09,419 --> 00:23:11,251
is really in love with you.

182
00:23:15,184 --> 00:23:21,085
I told you to talk to
the future you about it.

183
00:23:21,085 --> 00:23:22,585
You don't have to tell me.

184
00:23:22,585 --> 00:23:27,618
Do you know that you're going
to die soon, Miss Cheng?

185
00:23:29,618 --> 00:23:32,251
If the girl that he loves dearly is dead

186
00:23:32,251 --> 00:23:34,585
do you know what he's going to do?

187
00:23:36,518 --> 00:23:38,852
To tell you the truth, I don't know.

188
00:23:38,852 --> 00:23:40,618
The girl that Mu Si Ming told you about

189
00:23:40,618 --> 00:23:41,885
committed suicide in college

190
00:23:41,885 --> 00:23:44,085
and I lost myself for a long time.

191
00:23:44,085 --> 00:23:48,486
But you're his Miss Right.

192
00:23:48,486 --> 00:23:49,952
If you're...

193
00:23:51,552 --> 00:23:54,818
I don't know what he's going to do.

194
00:23:58,585 --> 00:24:04,652
I don't think he'll be so single-minded.

195
00:24:07,085 --> 00:24:09,286
Are you happy about it?

196
00:24:13,053 --> 00:24:17,452
Would you be happy if you
knew you were going to die?

197
00:24:17,452 --> 00:24:19,518
Okay, a piece of advice for you.

198
00:24:19,518 --> 00:24:21,719
You don't have much time.

199
00:24:21,719 --> 00:24:23,053
If you love him

200
00:24:23,053 --> 00:24:25,286
just give him a romantic
night and then disappear

201
00:24:25,286 --> 00:24:27,118
letting him know that you're not serious.

202
00:24:27,118 --> 00:24:29,585
If you don't, just ignore him.

203
00:24:29,585 --> 00:24:33,152
He'll know that it's
just wishful thinking.

204
00:24:33,152 --> 00:24:35,385
What the hell are you thinking?

205
00:24:35,385 --> 00:24:39,552
Are you arranging a one-night
stand for yourself?

206
00:24:39,552 --> 00:24:41,352
Why do I have to do that?

207
00:24:41,352 --> 00:24:44,786
You'll break his heart
if he loves you so much.

208
00:24:44,786 --> 00:24:46,952
Just say no to him.

209
00:24:46,952 --> 00:24:49,885
Time will mend all wounds.
He'll forget you.

210
00:24:49,885 --> 00:24:55,452
I don't want you to ruin
my future, Miss Cheng.

211
00:24:58,552 --> 00:25:00,184
Please.

212
00:25:18,618 --> 00:25:21,585
What did he mean by that?

213
00:25:24,385 --> 00:25:26,719
Even if he knows it...

214
00:25:27,852 --> 00:25:31,251
he will...

215
00:27:30,018 --> 00:27:31,885
Did he light a cigarette here?

216
00:29:53,486 --> 00:29:58,148
[Hsinchu Detention Center, Agency of
Corrections, Ministry of Justice]

217
00:30:23,419 --> 00:30:24,818
Hi, Dad.

218
00:30:27,385 --> 00:30:29,018
Yeah, I've seen Ji Rou.

219
00:30:35,952 --> 00:30:40,085
What do you want me to do?
I have a new lawyer.

220
00:30:45,652 --> 00:30:49,552
Yeah, I see. Please have faith in me.

221
00:30:53,786 --> 00:30:54,952
Yeah.

222
00:31:47,118 --> 00:31:48,184
Hello?

223
00:31:51,786 --> 00:31:53,585
The DNA matched.

224
00:31:55,184 --> 00:31:56,385
Yeah, thanks.

225
00:32:11,750 --> 00:32:15,852
It was you who called me, Wu Han Wen.

226
00:32:56,065 --> 00:32:58,351
[Private Number]

227
00:33:00,184 --> 00:33:01,885
Hello?

228
00:33:01,885 --> 00:33:05,286
I know what you did on
the morning of September 19th.

229
00:33:07,719 --> 00:33:10,518
Who is this? Is this a joke?

230
00:33:10,518 --> 00:33:11,885
Who the hell are you?

231
00:33:11,885 --> 00:33:13,818
Hold on a second, please.

232
00:33:17,441 --> 00:33:21,066
[Private Number]

233
00:33:33,585 --> 00:33:35,318
Did you get the picture?

234
00:33:37,684 --> 00:33:39,818
A volunteer at the animal shelter told me

235
00:33:39,818 --> 00:33:42,719
that Xiao Wei's parents
want the killer badly.

236
00:33:42,719 --> 00:33:47,852
But no one knows that you're
the killer, Zhou Da Jun.

237
00:33:48,352 --> 00:33:52,318
I'm sorry, but I don't know
what you're talking about.

238
00:33:52,318 --> 00:33:54,419
Really?

239
00:33:54,419 --> 00:33:56,885
Let's put it this way.

240
00:33:56,885 --> 00:34:01,885
At 12:40 that morning, you
went out in a black coat.

241
00:34:01,885 --> 00:34:04,986
You came back home at 2 a.m.

242
00:34:04,986 --> 00:34:06,918
but you didn't notice

243
00:34:06,918 --> 00:34:11,251
that the cuff was stained with
the pretty girl's blood.

244
00:34:12,786 --> 00:34:15,752
So it was you who broke into my place.

245
00:34:15,752 --> 00:34:18,252
I'm sorry, but there's no
bloodstain on my coat.

246
00:34:18,252 --> 00:34:20,051
It's good to be confident

247
00:34:20,051 --> 00:34:23,218
but please think harder.

248
00:34:23,218 --> 00:34:27,085
Maybe it was stained when
you pulled out the knife?

249
00:34:31,459 --> 00:34:33,410
Damn, you crazy bastard!

250
00:34:44,185 --> 00:34:45,884
Are you feeling guilty already?

251
00:35:25,689 --> 00:35:30,946
[Private Number]

252
00:35:32,252 --> 00:35:35,884
What do you want? Why did
you send me the pictures?

253
00:35:35,884 --> 00:35:38,185
Don't you recognize it?

254
00:35:38,185 --> 00:35:40,986
It was Xiao Wei's hairbrush.

255
00:35:40,986 --> 00:35:44,551
I went to her house and asked her parents

256
00:35:44,551 --> 00:35:46,384
to give me one of her hairs.

257
00:35:47,486 --> 00:35:51,085
Think about it. If I
give that to the police

258
00:35:51,085 --> 00:35:54,252
and they compare the DNA
with the bloodstain

259
00:35:54,252 --> 00:35:56,218
what do you think will happen?

260
00:36:05,352 --> 00:36:06,352
Yeah.

261
00:36:08,017 --> 00:36:11,453
All right. What do you want?

262
00:36:15,553 --> 00:36:17,419
[Taipei International Airport]

263
00:36:18,986 --> 00:36:22,151
At 8 a.m. tomorrow morning,
I want you to pick up something.

264
00:36:54,741 --> 00:36:57,738
[Private Number]

265
00:37:00,118 --> 00:37:02,852
- Hello?
- Have you seen it?

266
00:37:06,517 --> 00:37:07,517
What is it?

267
00:37:08,953 --> 00:37:11,319
It's one of Jiang Cheng Hao's trophies.

268
00:37:11,319 --> 00:37:13,884
Please give it to Ke Zhen Yu

269
00:37:13,884 --> 00:37:17,585
and tell him that it's the murder weapon.

270
00:37:17,585 --> 00:37:20,419
The police don't know where
it is, but you found it.

271
00:37:22,384 --> 00:37:25,085
But it's not the real murder weapon.

272
00:37:25,085 --> 00:37:27,752
This is ridiculous.

273
00:37:27,752 --> 00:37:29,585
Yes, it is.

274
00:37:29,585 --> 00:37:33,752
But you're the best friend
and assistant to the lawyer.

275
00:37:33,752 --> 00:37:35,118
I think it'll be easy for you

276
00:37:35,118 --> 00:37:38,051
to make him believe it's
the real murder weapon.

277
00:37:42,718 --> 00:37:46,085
And I'm sorry.

278
00:37:46,085 --> 00:37:50,585
The handbag doesn't match your outfit.

279
00:38:00,819 --> 00:38:02,718
Quit searching.

280
00:38:02,718 --> 00:38:06,419
If you do this for me, I'll
never bother you again.

281
00:39:00,752 --> 00:39:01,752
Come in.

282
00:39:11,118 --> 00:39:13,618
He's the one you said can give us a clue?

283
00:39:14,785 --> 00:39:17,651
Tell us. What's the important clue?

284
00:39:18,352 --> 00:39:21,185
Wait. Can I get the money first?

285
00:39:22,718 --> 00:39:25,051
You won't get a cent if
you don't tell us now.

286
00:39:25,051 --> 00:39:26,453
It's up to you.

287
00:39:29,884 --> 00:39:34,151
I was drinking with a bunch of friends

288
00:39:34,151 --> 00:39:36,819
and I heard it from
a drunk friend of mine.

289
00:39:36,819 --> 00:39:38,419
He said...

290
00:39:40,218 --> 00:39:42,685
A judo coach name Lin
hired him and his brothers

291
00:39:42,685 --> 00:39:46,051
to kill someone named
Jiang Cheng Hao, but they failed.

292
00:39:46,051 --> 00:39:49,685
Then Coach Lin hired someone
else nicknamed Big Head

293
00:39:49,685 --> 00:39:52,517
to kill Jiang Cheng Hao in his home.

294
00:39:52,517 --> 00:39:57,319
But Coach Lin got busted for
the drugs and ran away.

295
00:39:57,319 --> 00:40:00,218
Big Head didn't get the money,
and he had to run away.

296
00:40:00,218 --> 00:40:03,685
He wanted my friend to get
rid of the murder weapon.

297
00:40:03,685 --> 00:40:05,517
It was a huge trophy.

298
00:40:07,718 --> 00:40:09,384
We've been here all afternoon.

299
00:40:09,384 --> 00:40:11,585
Are you sure your metal detector works?

300
00:40:16,819 --> 00:40:18,017
What's in your pocket?

301
00:40:19,618 --> 00:40:20,618
Yeah, it works.

302
00:40:21,252 --> 00:40:23,585
Then the informant lied to us.

303
00:40:23,585 --> 00:40:26,585
We've been here all afternoon.
There's no trophy here.

304
00:40:28,185 --> 00:40:32,151
Something was wrong with him.
He deceived us.

305
00:40:33,585 --> 00:40:34,884
It's weird.

306
00:40:34,884 --> 00:40:37,384
He said the trophy was cleaned and buried

307
00:40:37,384 --> 00:40:39,319
in the hills around Jiang's house

308
00:40:39,319 --> 00:40:42,285
near some satellite station.
I think it's here.

309
00:40:44,486 --> 00:40:46,618
Maybe someone found it before us?

310
00:40:46,618 --> 00:40:48,479
Go over there, Zhen Yu,
and I'll search here.

311
00:41:04,685 --> 00:41:07,051
I have something here, Zhen Yu.

312
00:41:13,151 --> 00:41:15,486
Be gentle. Don't damage the evidence.

313
00:41:23,453 --> 00:41:24,453
Come on!

314
00:41:33,986 --> 00:41:35,819
I've found it at last.

315
00:41:35,819 --> 00:41:38,319
I have a chance to turn the tables.

316
00:41:44,384 --> 00:41:49,785
Now we'll analyze
the fingerprints and the blood.

317
00:41:49,785 --> 00:41:51,986
Then we'll have sufficient evidence.

318
00:41:54,085 --> 00:41:56,053
What's wrong?

319
00:41:56,053 --> 00:41:58,252
When I asked Ji Rou about
the cigarette butt yesterday

320
00:41:58,252 --> 00:42:00,185
she was unusually indifferent.

321
00:42:00,185 --> 00:42:03,919
She blamed us for suspecting
her, not Cheng Jia Le.

322
00:42:09,085 --> 00:42:11,852
With the trophy,
we'll finally find out the truth.

323
00:42:12,618 --> 00:42:14,718
Maybe Ji Rou has been telling the truth.

324
00:42:14,718 --> 00:42:17,685
Jiang Cheng Hao was killed by a gangster.

325
00:42:17,685 --> 00:42:19,819
There was no one else at the scene.

326
00:42:22,585 --> 00:42:24,486
Go get the camera, Da Jun.

327
00:42:36,118 --> 00:42:37,651
Are you all right?

328
00:42:39,517 --> 00:42:42,218
Stop that. You scared me.

329
00:42:42,919 --> 00:42:43,953
I'm fine.

330
00:42:45,819 --> 00:42:47,218
Thank goodness.

331
00:42:47,218 --> 00:42:51,185
That complacent idiot didn't
suspect the fake trophy.

332
00:42:51,185 --> 00:42:54,986
So everything will happen
earlier, but it will happen.

333
00:42:54,986 --> 00:42:58,151
The merging won't be affected.

334
00:42:58,151 --> 00:42:59,352
Yes!

335
00:43:05,752 --> 00:43:07,218
What is it, Da Jun?

336
00:43:09,585 --> 00:43:11,151
Nothing.

337
00:43:13,151 --> 00:43:14,651
I was thinking...

338
00:43:16,352 --> 00:43:18,953
Even if we can turn back the clock

339
00:43:18,953 --> 00:43:21,384
we won't necessarily change everything.

340
00:43:24,486 --> 00:43:29,352
Can we really change our fate?

341
00:43:31,986 --> 00:43:33,819
You'll have to ask Him.

342
00:43:35,185 --> 00:43:38,551
But when you have the chance

343
00:43:38,551 --> 00:43:41,486
you try your best to
change your fate, right?

344
00:43:43,185 --> 00:43:45,419
For example, we're not idiots

345
00:43:45,419 --> 00:43:48,185
but we believed the fake
information so easily.

346
00:43:48,185 --> 00:43:49,453
And...

347
00:43:50,551 --> 00:43:52,118
I don't want to admit it, but...

348
00:43:53,085 --> 00:43:58,085
I think Ji Rou is hiding
the truth to protect someone.

349
00:44:00,752 --> 00:44:02,185
Who do you think it is?

350
00:44:04,419 --> 00:44:05,953
Think about it.

351
00:44:05,953 --> 00:44:09,017
If the murder was caused by the drugs

352
00:44:09,017 --> 00:44:12,819
Ji Rou doesn't know Lin Bo Yu that well

353
00:44:12,819 --> 00:44:14,618
while Zhang De Pei is
her old acquaintance.

354
00:44:14,618 --> 00:44:16,486
But I don't think

355
00:44:16,486 --> 00:44:19,919
that Ji Rou cares for
Zhang De Pei that much.

356
00:44:21,185 --> 00:44:22,785
There's only one person

357
00:44:22,785 --> 00:44:26,185
Ji Rou would be willing to protect...

358
00:44:27,453 --> 00:44:28,933
and risk being suspected as a killer.

359
00:44:29,852 --> 00:44:31,051
Who's that?

360
00:44:34,218 --> 00:44:35,551
Wu Han Wen.

361
00:44:38,453 --> 00:44:39,718
Oh no!

362
00:44:39,718 --> 00:44:43,352
A girl can't even dress herself
properly before a date.

363
00:44:43,352 --> 00:44:44,919
I think you have a nice figure.

364
00:44:44,919 --> 00:44:47,352
It'll be good to get a bit sexy.

365
00:44:48,517 --> 00:44:50,285
I won't avoid you anymore.

366
00:44:50,285 --> 00:44:51,285
Come home now.

367
00:44:54,785 --> 00:44:57,118
There's something new in my memory.

