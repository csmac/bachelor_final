﻿1
00:00:01,053 --> 00:00:02,553
What do you think?

2
00:00:02,554 --> 00:00:04,321
What do <i>you</i> think?

3
00:00:04,323 --> 00:00:06,290
I don't like that
parts of it are purple.

4
00:00:06,292 --> 00:00:08,592
They were grape popsicles.

5
00:00:08,594 --> 00:00:10,060
It's tacky.

6
00:00:10,062 --> 00:00:11,728
Who cares about that?

7
00:00:11,730 --> 00:00:12,830
It's cool.

8
00:00:12,831 --> 00:00:14,848
Color matters, Joe!

9
00:00:17,335 --> 00:00:18,569
What do you think?

10
00:00:18,571 --> 00:00:20,003
What do <i>you</i> think?

11
00:00:20,005 --> 00:00:21,371
I think it's awesome.

12
00:00:21,373 --> 00:00:23,707
I mean, I really like
where we put the game room.

13
00:00:23,709 --> 00:00:25,142
Game room?

14
00:00:25,144 --> 00:00:28,045
That's not a game room, Joe.
That's a spa.

15
00:00:28,047 --> 00:00:31,214
Okay, for you and <i>your</i> wife,
it can be a spa.

16
00:00:31,216 --> 00:00:34,551
But for me and <i>my</i> wife,
it's gonna be a game room.

17
00:00:34,553 --> 00:00:36,086
Who's your wife?

18
00:00:36,088 --> 00:00:37,187
Alyssa Milano.

19
00:00:37,189 --> 00:00:38,322
Who's your wife?

20
00:00:38,324 --> 00:00:40,290
Bette Midler.

21
00:00:40,292 --> 00:00:42,559
Is that the lady
that lives on the corner?

22
00:00:42,561 --> 00:00:44,394
Oh, my God.
You know nothing.

23
00:00:46,197 --> 00:00:47,564
What do you think?

24
00:00:47,566 --> 00:00:50,601
What do <i>you</i> think?
I think it's great.

25
00:00:50,603 --> 00:00:53,070
It reminds me of that house where we
used to play Nintendo when we were kids.

26
00:00:53,072 --> 00:00:54,671
Remember? Remember?
It was that... what was that kid's name?

27
00:00:54,673 --> 00:00:57,341
It was like Tim something,
right? Tim something Irish.

28
00:00:57,343 --> 00:00:59,009
Close.
Phillip Schlemenson.

29
00:00:59,011 --> 00:01:02,212
That's it. That's this house.
I hated that house.

30
00:01:02,214 --> 00:01:03,580
It was great.

31
00:01:03,582 --> 00:01:05,649
I made out with Phillip's sister
in that house.

32
00:01:05,651 --> 00:01:08,118
I made out with Phillip
in that house.

33
00:01:09,420 --> 00:01:11,121
Oh,
you never forget your first.

34
00:01:11,123 --> 00:01:13,256
You just called him
"Tim something Irish."

35
00:01:13,258 --> 00:01:15,492
Wasn't my first.

36
00:01:16,761 --> 00:01:18,528
[ Imagine dragons'
"on top of the world" plays ]

37
00:01:21,633 --> 00:01:24,368
♪ 'cause I'm on top
of the world, hey! ♪

38
00:01:24,370 --> 00:01:26,670
♪ I'm on top of the world,
hey! ♪

39
00:01:26,672 --> 00:01:29,106
♪ waiting on this
for a while now ♪

40
00:01:29,108 --> 00:01:31,742
♪ been dreaming of this
since a child ♪

41
00:01:31,744 --> 00:01:33,610
♪ I'm on top of the world

42
00:01:33,611 --> 00:01:36,264
<font color="#00ffff">Sync & corrections by Rafael UPD</font>
<font color="#00ffff">www.addic7ed.com/</font>

43
00:01:42,802 --> 00:01:44,302
♪

44
00:01:44,303 --> 00:01:45,703
to think about our work
for a little while longer...

45
00:01:45,705 --> 00:01:47,538
No. No, no, no, no.
No, no. That's your problem.

46
00:01:47,540 --> 00:01:49,507
You think too much. Sometimes
you just got to go with your gut.

47
00:01:49,509 --> 00:01:51,475
No. No. That's your problem.
You only go with your gut.

48
00:01:51,477 --> 00:01:53,822
That's why you have a tattoo
of Clay aiken on your ass.

49
00:01:56,315 --> 00:01:58,382
I'm telling you, my gut instinct
has served us very well.

50
00:01:58,384 --> 00:01:59,850
We have a new business
because of it.

51
00:01:59,852 --> 00:02:03,187
I found our secretary on
a subway platform because of it.

52
00:02:03,189 --> 00:02:05,056
And it is the reason

53
00:02:05,058 --> 00:02:07,758
why my boyfriend
is a gorgeous jewish doctor.

54
00:02:07,760 --> 00:02:09,193
He's a nurse, Louis.

55
00:02:09,195 --> 00:02:11,262
Wyatt is mennonite
and a nurse.

56
00:02:11,264 --> 00:02:14,298
They're gonna promote him
to jewish doctor any day now.

57
00:02:14,300 --> 00:02:16,367
I'm pretty sure
it doesn't work that way.

58
00:02:16,369 --> 00:02:17,568
[ Chuckles ]
Agree to disagree.

59
00:02:17,570 --> 00:02:18,703
Hey, Joe,
you got a phone call.

60
00:02:18,705 --> 00:02:20,338
May I just say, rosanna,

61
00:02:20,340 --> 00:02:22,239
that the way you're presenting
your bosom this morning

62
00:02:22,241 --> 00:02:24,575
is sure to make Bernardo
the envy of every shark

63
00:02:24,577 --> 00:02:25,812
at the rumble.

64
00:02:27,479 --> 00:02:29,113
And may I just say,

65
00:02:29,115 --> 00:02:31,485
joke, joke, joke, gay, gay, gay,
I will cut you.

66
00:02:32,517 --> 00:02:34,719
It's your girlfriend
on line two.

67
00:02:34,721 --> 00:02:36,520
All right, just tell her
I'll call her back.

68
00:02:36,522 --> 00:02:38,155
Call her back?

69
00:02:38,157 --> 00:02:40,291
What? We have...
We have work to do.

70
00:02:40,293 --> 00:02:42,293
In the year that you and Ali
have been dating,

71
00:02:42,295 --> 00:02:44,495
I have never seen you
not take her phone call.

72
00:02:44,497 --> 00:02:46,130
Ah, it's nothing.

73
00:02:46,132 --> 00:02:48,299
Meaning,
"ah, it's something."

74
00:02:48,301 --> 00:02:49,433
Eh, it's no big deal.

75
00:02:49,435 --> 00:02:51,235
Meaning,
"eh, it's a huge deal."

76
00:02:51,237 --> 00:02:52,536
Oh, okay.
Okay, okay. Fine.

77
00:02:52,538 --> 00:02:54,305
Fine. Uh...

78
00:02:54,307 --> 00:02:56,640
I, uh, I think I have to
break up with her.

79
00:02:56,642 --> 00:02:57,842
What? Oh, my God.

80
00:02:57,844 --> 00:02:59,744
Why didn't you
bring this up sooner?

81
00:02:59,746 --> 00:03:02,046
Because I didn't want my drama
to get in the way of our work.

82
00:03:02,048 --> 00:03:05,416
Oh, my God. It's so funny. I always feel
like our work gets in the way of my drama.

83
00:03:05,418 --> 00:03:06,817
Well, tell me...
What happened?

84
00:03:06,819 --> 00:03:09,520
So, last night,
I picked her up after work.

85
00:03:09,522 --> 00:03:12,623
She's been really stressed out lately
because the store is in trouble,

86
00:03:12,625 --> 00:03:15,259
and she has a lot riding on some buyer
that's coming by to see her today.

87
00:03:15,261 --> 00:03:17,361
If this story isn't about you
or me in the next 30 seconds,

88
00:03:17,363 --> 00:03:18,763
I'm going to eat my fist.

89
00:03:18,765 --> 00:03:20,431
[ Chuckles ]

90
00:03:20,433 --> 00:03:21,432
No, no, don't.
Don't. Don't.

91
00:03:21,434 --> 00:03:23,200
It's too easy.
Yeah.

92
00:03:23,202 --> 00:03:25,770
Anyway, so, she had
a couple glasses of wine.

93
00:03:25,772 --> 00:03:27,171
You mean four.
It was three.

94
00:03:27,173 --> 00:03:29,673
It was four.
Girlfriend likes her liquor.

95
00:03:29,675 --> 00:03:32,676
Then out of nowhere, she tells
me she wants to get married.

96
00:03:32,678 --> 00:03:35,146
And she wants kids...
Like, now.

97
00:03:35,148 --> 00:03:36,847
So there's this "play me
or trade me" ultimatum

98
00:03:36,849 --> 00:03:38,382
on the table,

99
00:03:38,384 --> 00:03:40,117
and I'm just...
I'm not ready for that.

100
00:03:40,119 --> 00:03:41,786
Oh! I can't believe
what I'm hearing.

101
00:03:41,788 --> 00:03:45,189
This is crazy.
You know how much I love Ali.

102
00:03:45,191 --> 00:03:48,392
I mean, for God's sake,
I introduced the two of you.

103
00:03:48,394 --> 00:03:50,094
No, you didn't.

104
00:03:50,096 --> 00:03:53,297
Well, I had a lot to do
with you two getting together.

105
00:03:53,299 --> 00:03:56,022
You had nothing to do with it. You
were out of the country when we met.

106
00:03:56,023 --> 00:03:57,835
Well, I've told enough people
that I set you two up,

107
00:03:57,837 --> 00:03:59,906
so when legend becomes fact,
print the legend.

108
00:04:01,675 --> 00:04:03,709
It's just, you know,
it's just...

109
00:04:03,711 --> 00:04:06,012
Hey, okay, okay, okay, okay.
What is your gut telling you?

110
00:04:06,014 --> 00:04:07,580
My gut is telling me

111
00:04:07,582 --> 00:04:09,015
that there's lots of reasons
that I should break...

112
00:04:09,017 --> 00:04:10,850
Stop. That's what your head
is telling you.

113
00:04:10,852 --> 00:04:12,551
What is your <i>gut</i>
telling you?

114
00:04:12,553 --> 00:04:14,687
Ugh.
My gut is telling me

115
00:04:14,689 --> 00:04:17,490
that she is the sexiest,
most beautiful woman...

116
00:04:17,492 --> 00:04:19,792
Stop. That's what your Schmeckle
is telling you.

117
00:04:21,294 --> 00:04:24,463
Aim for the middle now.
What is your <i>gut</i> telling you?

118
00:04:24,465 --> 00:04:25,831
It's... it's telling me

119
00:04:25,833 --> 00:04:27,867
that, you know... it's...
It's telling me that...

120
00:04:27,869 --> 00:04:30,536
That's your answer,
Joe.

121
00:04:30,538 --> 00:04:33,973
It's not saying,
"I-I-I-I-I-I have to marry her""

122
00:04:36,810 --> 00:04:38,878
and if that's the case,
then you... you...

123
00:04:38,880 --> 00:04:40,880
Oh, God, I can't believe
I'm saying this.

124
00:04:40,882 --> 00:04:43,616
[ Sighs ]
You got to let her go.

125
00:04:43,618 --> 00:04:45,685
It's not fair to her.

126
00:04:45,687 --> 00:04:49,055
Mm. Oh, my God, Louis,
I think you're right.

127
00:04:49,057 --> 00:04:52,358
My Schmeckle's gonna hate me
for this.

128
00:04:56,329 --> 00:04:57,730
Hey.

129
00:04:57,732 --> 00:04:58,931
I was literally
just calling you!

130
00:04:58,933 --> 00:05:00,433
What are you doing here?

131
00:05:00,435 --> 00:05:01,801
Uh, we need to talk.

132
00:05:01,803 --> 00:05:03,502
Wait, wait.
I have the most amazing news.

133
00:05:03,504 --> 00:05:04,970
Do you remember
that night we went out

134
00:05:04,972 --> 00:05:06,806
and Louis had, like,
four vodkas on the rocks?

135
00:05:06,808 --> 00:05:07,973
Oh, he had one.
Four.

136
00:05:07,975 --> 00:05:09,308
Maybe two.
It was four.

137
00:05:09,310 --> 00:05:10,676
Girlfriend likes
her liquor.

138
00:05:10,678 --> 00:05:13,279
Anyway, I took an ice cube
out of his drink

139
00:05:13,281 --> 00:05:15,448
and said, "this would make
a really cute ring," remember?

140
00:05:15,450 --> 00:05:18,284
I just remember him yelling,
"it's flu season.

141
00:05:18,286 --> 00:05:19,752
Stop fingering my mojito."

142
00:05:19,754 --> 00:05:22,321
[ Laughs ]
Yeah. He did.

143
00:05:22,323 --> 00:05:23,489
But I designed one,

144
00:05:23,491 --> 00:05:25,624
and now
it's gonna be featured

145
00:05:25,626 --> 00:05:28,761
in the largest jewelry catalogue
in New York.

146
00:05:28,763 --> 00:05:32,531
Wow. Ali.
That is fantastic.

147
00:05:32,533 --> 00:05:33,966
Raise the, uh, uh,
raise the roof.

148
00:05:33,968 --> 00:05:36,268
[ Squeals ]

149
00:05:36,270 --> 00:05:38,337
Louis, why did you tell juanita
at the dry cleaner's

150
00:05:38,339 --> 00:05:39,338
that I'm a doctor?

151
00:05:39,340 --> 00:05:40,573
Oh, yeah, that. Right.

152
00:05:40,575 --> 00:05:43,042
Well, I mean, I-I, you know,
I just...

153
00:05:43,044 --> 00:05:45,544
She needed a doctor's note
to get out of jury duty.

154
00:05:45,546 --> 00:05:48,008
You don't... you don't
see anything wrong with that?

155
00:05:48,009 --> 00:05:50,476
A woman who wears cosmic
radiance by Britney Spears

156
00:05:50,478 --> 00:05:52,779
should not
serve on a jury.

157
00:05:52,781 --> 00:05:54,547
That's not what
I'm talking about, Louis.

158
00:05:54,549 --> 00:05:56,049
I mean, I'm not a doctor.
I'm a nurse.

159
00:05:56,051 --> 00:05:57,617
Yeah, right. That.

160
00:05:57,619 --> 00:05:59,485
Well, you know, I mean,
I just... I said "doctor"

161
00:05:59,487 --> 00:06:01,903
because, you know, it's...
It's... it's easier to say.

162
00:06:03,591 --> 00:06:05,792
Easier to say?
How is that easier to say?

163
00:06:05,794 --> 00:06:07,060
Doctor. Doctor.

164
00:06:07,062 --> 00:06:09,329
Nice, hard consonants.
Clicks right out.

165
00:06:09,331 --> 00:06:11,898
As opposed to "urrrr."

166
00:06:11,900 --> 00:06:14,667
You know,
that's a slow-motion word.

167
00:06:14,669 --> 00:06:17,904
Elphaba, stare blankly
if you agree with me.

168
00:06:19,607 --> 00:06:21,574
See?

169
00:06:21,576 --> 00:06:23,409
Louis, are you embarrassed
that I'm a nurse?

170
00:06:23,411 --> 00:06:25,345
[ Scoffs ]
Wyatt, my love, come on.

171
00:06:25,347 --> 00:06:28,047
I find it upsetting that
you would even ask such a thing.

172
00:06:28,049 --> 00:06:30,650
Of course I am!

173
00:06:30,652 --> 00:06:32,051
How could you not
know that?

174
00:06:32,053 --> 00:06:33,653
I just thought it was
part of your schtick.

175
00:06:33,655 --> 00:06:36,104
Sweetheart,
I <i>am</i> my schtick.

176
00:06:37,391 --> 00:06:40,460
I just feel like everything
is falling into place.

177
00:06:40,462 --> 00:06:42,996
Well, look, I just wanted to
talk to you about last night.

178
00:06:42,998 --> 00:06:44,998
Oh, I'm sorry about that.

179
00:06:45,000 --> 00:06:47,133
It's just I was stressed out
about losing the store,

180
00:06:47,135 --> 00:06:49,602
and then I started stressing out
about losing you, and...

181
00:06:49,604 --> 00:06:51,070
Just forget
it ever happened.

182
00:06:51,072 --> 00:06:52,672
Okay,
but you did say that...

183
00:06:52,674 --> 00:06:54,574
No, no, no. No. No.

184
00:06:54,576 --> 00:06:56,843
My life is so good
right now.

185
00:06:56,845 --> 00:06:58,544
It's perfect.

186
00:06:58,546 --> 00:07:00,513
I love you, Joe, and the last
thing that I would ever want

187
00:07:00,515 --> 00:07:02,015
is for you to do anything

188
00:07:02,017 --> 00:07:04,883
because you feel pressured
into it, so...

189
00:07:07,588 --> 00:07:09,220
What did you want to say?

190
00:07:16,623 --> 00:07:19,238
Oh, my God.
What are you doing?!

191
00:07:19,240 --> 00:07:21,173
I'm going with my gut.

192
00:07:21,175 --> 00:07:24,743
Ali landau,
will you marry me?

193
00:07:24,745 --> 00:07:26,045
Really?
Yeah.

194
00:07:26,047 --> 00:07:28,747
Yes!
Yes, I will marry you!

195
00:07:28,749 --> 00:07:29,882
Oh, my God.

196
00:07:29,884 --> 00:07:31,984
Mm.

197
00:07:31,986 --> 00:07:34,153
Gosh, why would he do it?
What's better than Ali?

198
00:07:34,155 --> 00:07:35,888
Well,
she wants to get married,

199
00:07:35,890 --> 00:07:38,224
and she gave him a "play me
or trade me" ultimatum.

200
00:07:38,226 --> 00:07:39,658
Wow.

201
00:07:39,660 --> 00:07:40,993
A "play me or trade m""
ultimatum.

202
00:07:40,995 --> 00:07:42,094
Yeah.
Yeah.

203
00:07:42,096 --> 00:07:43,596
Yeah.

204
00:07:43,598 --> 00:07:45,898
What is a "play me or trade me"
ultimatum?

205
00:07:45,900 --> 00:07:47,700
I have no idea.

206
00:07:47,702 --> 00:07:49,902
I was just going along because
I didn't want to seem gay.

207
00:07:49,904 --> 00:07:51,670
Wyatt, my love,
you're a male nurse

208
00:07:51,672 --> 00:07:52,872
who DVRs everything on bravo.

209
00:07:52,874 --> 00:07:53,906
That ship has sailed.

210
00:07:53,908 --> 00:07:55,207
Oh, God, I just realized

211
00:07:55,209 --> 00:07:56,942
she's gonna be in my yoga class
tomorrow.

212
00:07:56,944 --> 00:07:59,378
That'll be so awkward
for me.

213
00:08:00,680 --> 00:08:05,718
Okay, let's come out of
child's pose into warrior I.

214
00:08:05,720 --> 00:08:08,187
Did you talk to Joe
last night?

215
00:08:08,189 --> 00:08:09,588
No.

216
00:08:09,590 --> 00:08:11,524
Oh, well,
we were kind of busy.

217
00:08:11,526 --> 00:08:12,958
I assume you know
what happened.

218
00:08:12,960 --> 00:08:14,059
[ Sighs ] I do.

219
00:08:14,061 --> 00:08:15,761
How you doing? You okay?

220
00:08:15,763 --> 00:08:17,263
You seem okay.

221
00:08:17,265 --> 00:08:19,265
Okay? I'm thrilled.

222
00:08:19,267 --> 00:08:20,966
Oh, come on, baby girl.
It's me.

223
00:08:20,968 --> 00:08:22,601
What?

224
00:08:22,603 --> 00:08:24,737
You don't think I am?
Of course I'm thrilled.

225
00:08:24,739 --> 00:08:26,705
And to tell you the truth,
a little relieved.

226
00:08:26,707 --> 00:08:28,541
I got you.
We're doing survive and thrive.

227
00:08:28,543 --> 00:08:30,109
Good for you,
you brave little toaster.

228
00:08:30,111 --> 00:08:34,547
Down dog into plank
into upward dog.

229
00:08:34,549 --> 00:08:36,515
Okay, what are you
talking about?

230
00:08:36,517 --> 00:08:39,251
I just want to say for
the record it was not my idea.

231
00:08:39,253 --> 00:08:41,820
I would hope not. I think
he was a fool to do what he did.

232
00:08:41,822 --> 00:08:44,623
What? I could have told you
he wasn't ready to get married.

233
00:08:44,625 --> 00:08:47,927
I tell everyone in my life,
"run everything past me first."

234
00:08:47,929 --> 00:08:50,405
Even yoga-wear decisions...
Hint, hint.

235
00:08:52,165 --> 00:08:54,533
What do you mean
he wasn't ready to get married?

236
00:08:54,535 --> 00:08:55,901
He proposed.

237
00:08:55,903 --> 00:08:58,971
What? What happened
to "play me or trade me"?

238
00:08:58,973 --> 00:09:01,006
What is that
supposed to mean?

239
00:09:02,943 --> 00:09:04,076
[ Elevator bell dings ]

240
00:09:04,078 --> 00:09:05,711
Is he in?

241
00:09:05,713 --> 00:09:08,080
Does he seem upset?
Oh, he's in a good mood.

242
00:09:08,082 --> 00:09:09,949
He came in early, and he
just started working. Why?

243
00:09:09,951 --> 00:09:12,251
What'd you do?

244
00:09:12,253 --> 00:09:13,652
It's bad.

245
00:09:13,654 --> 00:09:15,221
Do you need the girls?

246
00:09:15,223 --> 00:09:16,922
Yes, please.

247
00:09:17,991 --> 00:09:18,991
Thank you.

248
00:09:18,993 --> 00:09:20,559
[ Clears throat ]

249
00:09:21,962 --> 00:09:23,896
Hey. What you doing here?
I thought yoga goes till 9:00.

250
00:09:23,898 --> 00:09:24,997
I wasn't feeling it.

251
00:09:24,999 --> 00:09:27,266
Okay, well, I got news.

252
00:09:27,268 --> 00:09:28,867
I got newer news.
No, just listen to me.

253
00:09:28,869 --> 00:09:30,536
Go ahead.

254
00:09:30,538 --> 00:09:32,605
So, I did what you said.

255
00:09:32,607 --> 00:09:34,173
I went with my gut.

256
00:09:34,175 --> 00:09:36,775
I mean, I was ready
to break up with her.

257
00:09:36,777 --> 00:09:40,879
But then she looked at me,
and she told me she loved me,

258
00:09:40,881 --> 00:09:43,616
and she just...
She took the pressure off.

259
00:09:43,618 --> 00:09:45,618
And in that instant,
I knew...

260
00:09:45,620 --> 00:09:46,885
Rosanna:
Joe, Ali's on line one.

261
00:09:46,887 --> 00:09:48,187
We're working.
He'll call back.

262
00:09:48,189 --> 00:09:49,588
No, I'm taking it.
What are you doing?

263
00:09:49,590 --> 00:09:50,856
Well, we're working.
He'll call back.

264
00:09:50,858 --> 00:09:53,009
No, I'll take it.
Don't be weird.

265
00:09:53,960 --> 00:09:56,195
Hi, honey.

266
00:09:56,197 --> 00:09:57,830
Yeah, he's right here.
Why?

267
00:09:59,599 --> 00:10:02,801
He did what, now?

268
00:10:05,639 --> 00:10:07,940
Well, well, yes, yes,
but... but I...

269
00:10:07,942 --> 00:10:09,208
[ Stammers ]

270
00:10:09,210 --> 00:10:11,944
Right.
No, I know that. I know that.

271
00:10:13,947 --> 00:10:15,781
It was gonna be
a longer conver...

272
00:10:15,783 --> 00:10:17,850
I think we should meet up.
Should we meet?

273
00:10:17,852 --> 00:10:19,585
Or maybe...
I have no interest...

274
00:10:19,587 --> 00:10:21,253
Okay, well,
I don't want any trouble.

275
00:10:21,255 --> 00:10:23,088
I don't want any trouble.
You know what I mean?

276
00:10:24,791 --> 00:10:26,091
Okay, bye.

277
00:10:26,093 --> 00:10:27,693
[ Receiver clicks ]

278
00:10:28,662 --> 00:10:30,396
What'd she say?

279
00:10:31,831 --> 00:10:34,926
I'm going to kick
your aiken.

280
00:10:43,152 --> 00:10:44,286
What were you thinking?

281
00:10:44,288 --> 00:10:46,087
I was...
You know, it's funny.

282
00:10:46,089 --> 00:10:47,856
No, you don't... don't
even answer that question!

283
00:10:47,858 --> 00:10:50,025
That is a ridiculous question,
because you don't think!

284
00:10:50,027 --> 00:10:52,661
You just vomit
whatever's in your head!

285
00:10:52,663 --> 00:10:54,996
You're a mental bulimic!

286
00:10:56,232 --> 00:10:58,733
Do you understand what you
just did today? Do you?

287
00:10:58,735 --> 00:11:00,402
The engagement's off.
She doesn't trust me.

288
00:11:00,404 --> 00:11:02,637
Oh, I can't believe I did this.
I-I'm a terrible person.

289
00:11:02,639 --> 00:11:04,105
I hate myself!

290
00:11:04,107 --> 00:11:06,808
You're not really gonna make
this about you, are you?

291
00:11:06,810 --> 00:11:08,043
What have I done?

292
00:11:08,045 --> 00:11:10,946
I just destroyed
my best friend's life!

293
00:11:10,948 --> 00:11:12,814
You poor thing.

294
00:11:13,749 --> 00:11:14,950
How can I help?

295
00:11:14,952 --> 00:11:16,685
Maybe some tea.
That'd be nice.

296
00:11:16,687 --> 00:11:18,219
Yeah? You want some lemon,
sugar?

297
00:11:18,221 --> 00:11:20,021
Yeah, lemon's good.
Stevia if you got it.

298
00:11:20,023 --> 00:11:21,957
I'm not getting you
a cup of tea, you schmuck!

299
00:11:21,959 --> 00:11:23,625
Oh, God!

300
00:11:23,627 --> 00:11:26,027
I can't do this
with you anymore, Louis.

301
00:11:26,029 --> 00:11:27,862
Oh, come on.
Joe, we'll get through this.

302
00:11:27,864 --> 00:11:29,731
No, this isn't cute.

303
00:11:29,733 --> 00:11:32,801
This isn't like when you bought
me that parrot just for giggles.

304
00:11:32,803 --> 00:11:35,103
I thought it would give you
an air of whimsy.

305
00:11:35,105 --> 00:11:37,005
No. No, that... that's not
what it gave me.

306
00:11:37,007 --> 00:11:38,707
You know what it gave me?

307
00:11:38,709 --> 00:11:40,108
It gave me
a bathroom floor

308
00:11:40,110 --> 00:11:41,876
that looks like
a Jackson pollock painting.

309
00:11:41,878 --> 00:11:43,345
I'm sorry!
Oh, yeah, yeah.

310
00:11:43,347 --> 00:11:46,214
That's what you said
when you totaled my dad's car...

311
00:11:46,216 --> 00:11:48,650
With my mom's car!

312
00:11:48,652 --> 00:11:51,336
Gays can't drive stick.
Ironic, right?

313
00:11:52,755 --> 00:11:53,888
I wasn't thinking!

314
00:11:53,890 --> 00:11:55,757
Yeah.
No, t-that's the problem.

315
00:11:55,759 --> 00:11:57,258
You don't think.

316
00:11:57,260 --> 00:11:59,127
You never think, and I
always end up getting hurt.

317
00:11:59,129 --> 00:12:00,829
Oh, come on.
All of those things were silly.

318
00:12:00,831 --> 00:12:03,632
Yeah, but this isn't silly.
This time you crossed a line.

319
00:12:03,634 --> 00:12:05,166
And I'm done.

320
00:12:05,168 --> 00:12:06,668
What are you saying?

321
00:12:06,670 --> 00:12:08,136
I'm saying that...

322
00:12:08,138 --> 00:12:10,238
The cost of being friends
with you

323
00:12:10,240 --> 00:12:12,807
has now officially
outweighed the benefits.

324
00:12:12,809 --> 00:12:15,410
I'm saying that
this partnership is...

325
00:12:18,114 --> 00:12:20,949
This friendship is...

326
00:12:20,951 --> 00:12:22,717
Is over.

327
00:12:22,719 --> 00:12:24,386
No. No, no, no, no. Wait.
Joe, Joe, Joe, wait, wait, wait.

328
00:12:24,388 --> 00:12:25,920
Think about this!

329
00:12:25,922 --> 00:12:27,955
No, I'm going with my gut
on this one, too.

330
00:12:31,412 --> 00:12:33,413
There you go.
Thank you.

331
00:12:33,415 --> 00:12:35,215
You're Louis' boyfriend,
aren't you?

332
00:12:35,217 --> 00:12:36,349
Yes, I am.

333
00:12:36,351 --> 00:12:38,819
He talks about you
all the time.

334
00:12:38,821 --> 00:12:40,821
My mother would be so happy
if I was dating a doctor.

335
00:12:40,823 --> 00:12:43,089
[ Chuckles ]
Yes. So would Louis.

336
00:12:44,392 --> 00:12:45,425
Oh, hey, Joe.

337
00:12:45,427 --> 00:12:46,560
Oh, hey, Wyatt.

338
00:12:46,562 --> 00:12:48,261
Why aren't you at work?

339
00:12:48,263 --> 00:12:51,264
Uh, let's just say
I had to get out of the office.

340
00:12:51,266 --> 00:12:53,800
Oh, hey, I heard
you broke up with Ali.

341
00:12:53,802 --> 00:12:55,168
Gosh, that's terrible.

342
00:12:55,170 --> 00:12:57,404
Actually, I proposed to Ali,
and she accepted.

343
00:12:57,406 --> 00:12:59,005
Well, gosh,
that's wonderful.

344
00:12:59,007 --> 00:13:00,507
Yeah. And then Louis
went and told her

345
00:13:00,509 --> 00:13:02,309
that I was gonna break up
with her.

346
00:13:02,311 --> 00:13:03,977
And now
the engagement's off.

347
00:13:03,979 --> 00:13:05,660
Well, gosh,
that's terrible.

348
00:13:07,415 --> 00:13:08,915
Are there gonna be
any more?

349
00:13:08,917 --> 00:13:11,885
No.
No, that's where it ends.

350
00:13:11,887 --> 00:13:13,987
All right, I guess
I'll see you around.

351
00:13:13,989 --> 00:13:15,822
Huh?

352
00:13:15,824 --> 00:13:17,190
Hey, Wyatt?

353
00:13:17,192 --> 00:13:18,258
Yeah?

354
00:13:18,260 --> 00:13:19,426
How do you do it?

355
00:13:19,428 --> 00:13:21,361
Two hours in the gym
every morning.

356
00:13:21,363 --> 00:13:24,345
The rest
is just good genes.

357
00:13:27,068 --> 00:13:29,302
Oh, I can't believe
he sent you here.

358
00:13:29,304 --> 00:13:31,304
He didn't. Oh, please.
Of course he did.

359
00:13:31,306 --> 00:13:32,505
You two do everything
for each other.

360
00:13:32,507 --> 00:13:34,174
You're a perfect couple.

361
00:13:34,176 --> 00:13:36,877
In fact, why did I even think
about marrying Joe

362
00:13:36,879 --> 00:13:38,245
in the first place?

363
00:13:38,247 --> 00:13:40,313
What's the point?
He's already married to you.

364
00:13:40,315 --> 00:13:42,282
Not anymore.

365
00:13:42,284 --> 00:13:43,416
What are you talking about?

366
00:13:43,418 --> 00:13:45,285
I really blew it this time,
Ali.

367
00:13:45,287 --> 00:13:47,153
[ Sighs ]
Our friendship's over.

368
00:13:47,155 --> 00:13:48,989
Give me a break.

369
00:13:48,991 --> 00:13:51,291
This is the guy who called you,
giggling, from my bathroom

370
00:13:51,293 --> 00:13:53,860
after we had sex
for the first time.

371
00:13:53,862 --> 00:13:55,896
You were totally
out of our league.

372
00:13:55,898 --> 00:13:57,378
We were so excited.

373
00:13:58,382 --> 00:13:59,582
How do you deal with Louis?

374
00:13:59,584 --> 00:14:01,284
I mean,
doesn't he drive you crazy?

375
00:14:01,286 --> 00:14:02,919
Well, that's easy.

376
00:14:02,921 --> 00:14:05,021
No one in the world
loves like Louis.

377
00:14:05,023 --> 00:14:07,357
I mean, I question
a lot of his decisions,

378
00:14:07,359 --> 00:14:10,093
but one thing I never question
is his heart.

379
00:14:10,095 --> 00:14:12,062
And if you keep that
in mind,

380
00:14:12,064 --> 00:14:14,331
you can forgive
all the other stuff.

381
00:14:14,333 --> 00:14:16,499
Oh, speaking of hearts,
I got to get back to work.

382
00:14:16,501 --> 00:14:19,869
I'm in the cardiac wing today.
That's why I got a heart on.

383
00:14:19,871 --> 00:14:22,372
Wow.
[ Chuckles ]

384
00:14:22,374 --> 00:14:25,408
I can give you one
if you want.

385
00:14:25,410 --> 00:14:27,243
No,
I don't think you can.

386
00:14:30,348 --> 00:14:31,982
Ali, listen.

387
00:14:31,984 --> 00:14:34,818
We were 12 when Joe's parents
got divorced, okay?

388
00:14:34,820 --> 00:14:37,187
And... and he was so sad

389
00:14:37,189 --> 00:14:40,023
that he stopped going to school
for like a month.

390
00:14:40,025 --> 00:14:42,425
And every day,
I would go over to his house,

391
00:14:42,427 --> 00:14:44,127
and we would
do his homework,

392
00:14:44,129 --> 00:14:47,163
and I would tell him
which girls were getting boobs.

393
00:14:49,200 --> 00:14:51,301
And I'd make him a panini.

394
00:14:51,303 --> 00:14:54,170
Because I had to make sure

395
00:14:54,172 --> 00:14:57,841
that my best friend wasn't
gonna get held back a grade.

396
00:14:57,843 --> 00:14:59,509
It's a very nice story,

397
00:14:59,511 --> 00:15:01,311
but I'm not sure what it has to
do with... Ali, listen to me.

398
00:15:01,313 --> 00:15:03,213
You've got to marry him
because I need to know

399
00:15:03,215 --> 00:15:05,115
if I'm not gonna be around
to take care of him,

400
00:15:05,117 --> 00:15:07,156
at least you will.

401
00:15:09,020 --> 00:15:12,122
He was <i>this</i> close
to breaking up with me.

402
00:15:12,124 --> 00:15:13,857
He's not ready to...
He's ready.

403
00:15:13,859 --> 00:15:16,326
But he's not certain that...
He's certain.

404
00:15:16,328 --> 00:15:19,162
Louis, I just think that if he doesn't...
Ali, you're the most wonderful person

405
00:15:19,164 --> 00:15:21,097
that's ever
come into Joe's life.

406
00:15:21,099 --> 00:15:22,932
You're hotter than he is.

407
00:15:22,934 --> 00:15:24,434
You're smart.

408
00:15:24,436 --> 00:15:26,803
You're a bad cook,
but with a little effort,

409
00:15:26,805 --> 00:15:28,638
you could learn
to be a mediocre cook.

410
00:15:30,007 --> 00:15:34,411
And what's more important than
all of that... you love Joe.

411
00:15:35,446 --> 00:15:38,348
And he loves you
more than...

412
00:15:38,350 --> 00:15:42,024
More than almost anything else
in the world.

413
00:15:46,957 --> 00:15:48,197
What are you doing?

414
00:15:49,427 --> 00:15:51,895
Ali landau...

415
00:15:51,897 --> 00:15:55,965
Will you do me the honor
of marrying my best friend?

416
00:15:55,967 --> 00:15:58,068
This is so inappropriate.

417
00:15:58,070 --> 00:16:00,537
Just... just... just...
Just answer the question.

418
00:16:01,472 --> 00:16:03,306
I'll think about it.

419
00:16:04,442 --> 00:16:05,575
Thank you.

420
00:16:10,147 --> 00:16:11,815
[ Sighs ]

421
00:16:11,817 --> 00:16:14,084
Louis, what kind of
freaky 12-year-old

422
00:16:14,086 --> 00:16:17,018
knows how to make
a panini?

423
00:16:21,390 --> 00:16:22,691
[ Door opens ]

424
00:16:22,693 --> 00:16:24,659
Oh, you're still here.

425
00:16:24,661 --> 00:16:26,194
Thought you'd be gone.

426
00:16:26,196 --> 00:16:30,065
Just came in to, uh...
Pack up a couple of boxes.

427
00:16:30,067 --> 00:16:32,367
Sure.

428
00:16:36,772 --> 00:16:38,273
[ Sighs ]

429
00:16:38,275 --> 00:16:40,775
I'm really sorry
that I...

430
00:16:41,744 --> 00:16:43,078
I'm just sorry.

431
00:16:43,080 --> 00:16:44,212
Yeah, me too.

432
00:16:45,581 --> 00:16:47,248
[ Cellphone rings ]

433
00:16:47,250 --> 00:16:49,451
[ Sighs ]

434
00:16:49,453 --> 00:16:51,486
[ Ringing continues ]

435
00:16:53,789 --> 00:16:55,657
Oh, hey.

436
00:16:55,659 --> 00:16:58,193
Yeah, he's right here.
Why?

437
00:16:59,762 --> 00:17:01,563
He did what, now?

438
00:17:03,032 --> 00:17:05,166
Well, that's all kinds
of inappropriate.

439
00:17:06,635 --> 00:17:09,337
Oh, he did?

440
00:17:09,339 --> 00:17:10,338
Oh.

441
00:17:10,340 --> 00:17:11,673
You will?

442
00:17:13,776 --> 00:17:16,144
You have no idea how happy
you're making me right now.

443
00:17:20,349 --> 00:17:22,717
Well, I mean,
I don't know, you know.

444
00:17:22,719 --> 00:17:25,387
He's such a pain in the ass,
you know?

445
00:17:27,156 --> 00:17:29,491
All right, well, listen,
you're my fiancée now,

446
00:17:29,493 --> 00:17:33,395
so, uh... if you want me
to forgive him, I, uh, I will.

447
00:17:33,397 --> 00:17:35,296
I-I will forgive him.

448
00:17:36,699 --> 00:17:38,533
I love you, too.

449
00:17:38,535 --> 00:17:40,602
Yes. More than him.

450
00:17:41,604 --> 00:17:42,604
Bye.

451
00:17:42,606 --> 00:17:43,738
[ Chuckles ]

452
00:17:44,673 --> 00:17:46,040
Who was that?

453
00:17:46,042 --> 00:17:47,175
Wrong number.

454
00:17:55,065 --> 00:17:57,649
So now that we're engaged,

455
00:17:57,650 --> 00:17:59,385
whose apartment
are we going to move into,

456
00:17:59,387 --> 00:18:01,320
and whose apartment
are we going to get rid of?

457
00:18:01,322 --> 00:18:03,055
P.S.... It's yours, Ali.

458
00:18:03,057 --> 00:18:05,457
Okay.
<i>We</i> are not engaged.

459
00:18:05,459 --> 00:18:08,527
<i>We</i> are engaged.
Do <i>we</i> understand that?

460
00:18:08,529 --> 00:18:10,262
We do not.
We have been waiting

461
00:18:10,264 --> 00:18:12,331
to plan Joe's wedding
for our entire life,

462
00:18:12,333 --> 00:18:14,333
and we are not going to
let you ruin it

463
00:18:14,335 --> 00:18:16,168
with a barefoot-on-the-beach
theme.

464
00:18:16,170 --> 00:18:17,436
I wasn't.
Weren't you?

465
00:18:17,438 --> 00:18:18,704
I was.
Yes.

466
00:18:18,706 --> 00:18:21,240
Guys, I'd like
to acknowledge something.

467
00:18:22,517 --> 00:18:25,310
Two people who really love
each other came together today.

468
00:18:25,312 --> 00:18:27,346
And it almost didn't happen,

469
00:18:27,348 --> 00:18:30,182
but this relationship
is truly meant to be.

470
00:18:30,184 --> 00:18:33,118
And I have no doubt
that it will last forever.

471
00:18:33,120 --> 00:18:34,853
Aw. Thank you.
You drive-a me crazy.

472
00:18:34,855 --> 00:18:36,422
Hey, come here!
You do!

473
00:18:36,424 --> 00:18:38,090
I'm worth it!
I'm worth it.

474
00:18:38,092 --> 00:18:39,291
With that hat
I hate.

475
00:18:39,293 --> 00:18:41,126
Do we need to worry
about this?

476
00:18:41,128 --> 00:18:42,761
I think we just
need to accept the fact

477
00:18:42,763 --> 00:18:45,664
that there are four people
at this table but three couples.

478
00:18:45,666 --> 00:18:48,300
Huh. I think
that's kind of nice.

479
00:18:48,302 --> 00:18:50,869
Oh,
I still have my heart on.

480
00:18:52,439 --> 00:18:54,306
It's not <i>that</i> nice.

481
00:18:54,308 --> 00:18:55,641
[ Laughter ]

482
00:18:55,643 --> 00:18:58,410
♪ I'm on top of the world

483
00:18:58,411 --> 00:18:59,911
<font color="#00ffff">Sync & corrections by Rafael UPD</font>
<font color="#00ffff">www.addic7ed.com/</font>

