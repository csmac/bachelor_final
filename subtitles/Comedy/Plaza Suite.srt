﻿1
00:00:01,420 --> 00:00:04,212
<i>Zack, Zack, Zack!</i>

2
00:00:10,017 --> 00:00:12,253
Now performing

3
00:00:12,288 --> 00:00:13,914
a backwards
body-dunking,

4
00:00:14,034 --> 00:00:17,727
grab-a-fish
bungee jump...

5
00:00:17,761 --> 00:00:21,131
Zack Martin!

6
00:00:21,166 --> 00:00:23,834
- All: Woo!
- Look.

7
00:00:23,869 --> 00:00:25,937
I... I can't let
you do this...

8
00:00:25,971 --> 00:00:28,837
Even though I would love to
inherit your single cabin.

9
00:00:28,871 --> 00:00:30,403
Cody, please.

10
00:00:30,438 --> 00:00:33,003
If Houdini's brother
didn't let him do stunts,

11
00:00:33,038 --> 00:00:34,670
where would he be?

12
00:00:34,704 --> 00:00:36,904
Alive.

13
00:00:36,938 --> 00:00:38,906
Bad example.
Now stand back.

14
00:00:38,940 --> 00:00:40,744
I'm about to make history.

15
00:00:40,777 --> 00:00:43,313
Boy!

16
00:00:43,347 --> 00:00:45,486
This is dangerous and stupid.

17
00:00:45,517 --> 00:00:47,385
What are you thinking?

18
00:00:47,420 --> 00:00:48,986
I'll take this one.

19
00:00:49,021 --> 00:00:51,358
Nothing.

20
00:00:51,390 --> 00:00:53,523
I'm not letting
you do this, Zack.

21
00:00:53,557 --> 00:00:55,390
Kirby, Kirby, please.

22
00:00:55,424 --> 00:00:58,861
If Houdini's security guard
didn't let him do stunts,

23
00:00:58,895 --> 00:01:00,362
where would he be?

24
00:01:00,396 --> 00:01:03,030
Alive.

25
00:01:03,064 --> 00:01:05,164
I need a new hypothetical.

26
00:01:05,199 --> 00:01:07,333
Get off of that.

27
00:01:07,368 --> 00:01:09,170
Fine.

28
00:01:09,204 --> 00:01:11,506
- Man, this is stuck.
- ( Kirby muttering )

29
00:01:11,540 --> 00:01:13,275
On duty, I'm a large man trying

30
00:01:13,309 --> 00:01:15,545
to climb on a little table
and unhook a bungee.

31
00:01:15,579 --> 00:01:18,149
- Let me help you.
- No, I don't need your help.

32
00:01:18,183 --> 00:01:20,918
( Screaming )

33
00:01:34,233 --> 00:01:36,301
<i>( Splashes ) Ooh!</i>

34
00:01:41,007 --> 00:01:42,908
Kirby, are you okay?

35
00:01:44,844 --> 00:01:47,377
I hate Sushi.

36
00:01:47,411 --> 00:01:50,478
<i>♪ Oh ay oh, oh ay oh ♪</i>

37
00:01:52,484 --> 00:01:54,749
<i>♪ come along with me ♪</i>

38
00:01:54,784 --> 00:01:56,551
<i>♪ and let's</i>
<i>head out to see ♪</i>

39
00:01:56,585 --> 00:01:58,318
<i>♪ what this world has ♪</i>

40
00:01:58,353 --> 00:02:00,886
<i>♪ for you and for me now ♪</i>

41
00:02:00,920 --> 00:02:02,688
<i>♪ whichever way</i>
<i>the wind blows ♪</i>

42
00:02:02,722 --> 00:02:05,057
<i>- ♪ we say... ♪</i>
<i>- ♪ Hey-ho, let's go! ♪</i>

43
00:02:05,091 --> 00:02:07,359
- <i>♪ oh ay oh ♪</i>
- <i>♪ This boat's rocking ♪</i>

44
00:02:07,394 --> 00:02:10,963
- <i>♪ oh ay oh ♪</i>
- <i>♪ Ain't no stopping us now ♪</i>

45
00:02:10,997 --> 00:02:13,732
<i>♪ 'cause we're living</i>
<i>the suite life ♪</i>

46
00:02:13,767 --> 00:02:16,268
- <i>♪ oh ay oh ♪</i>
- <i>♪ This boat's rocking ♪</i>

47
00:02:16,303 --> 00:02:19,806
- <i>♪ oh ay oh ♪</i> - <i>♪ Rocking
the whole</i> <i>world round ♪</i>

48
00:02:19,840 --> 00:02:23,943
<i>♪ and we're living</i>
<i>the suite life now ♪</i>

49
00:02:23,977 --> 00:02:25,945
<i>♪ hey ho!</i> <i> Oh ay oh ♪</i>

50
00:02:25,979 --> 00:02:27,446
<i>♪ let's go! ♪</i>

51
00:02:31,317 --> 00:02:34,619
Ah-ah-ah-ah!
You can't go in there.

52
00:02:34,654 --> 00:02:36,621
Why not?
This is my cabin.

53
00:02:36,656 --> 00:02:38,623
Not at the moment.

54
00:02:38,657 --> 00:02:41,592
Right now
it's a production studio.

55
00:02:41,626 --> 00:02:45,562
- Show business.
- What?

56
00:02:45,597 --> 00:02:48,499
London's doing
a webisode of "Yay Me!

57
00:02:48,533 --> 00:02:51,202
Starring London Tipton!"

58
00:02:51,236 --> 00:02:53,036
So you can't go in.

59
00:02:53,073 --> 00:02:56,339
But I have to get in there
to do "tomorrow's homework,

60
00:02:56,374 --> 00:02:59,276
by Bailey Pickett!"
Now out of my way.

61
00:02:59,310 --> 00:03:00,777
Ow ow ow.

62
00:03:00,812 --> 00:03:02,579
That's right, London lovers.
Believe it or not,

63
00:03:02,614 --> 00:03:04,581
someone actually wears this...

64
00:03:04,615 --> 00:03:06,515
In public.
( Laughs )

65
00:03:06,549 --> 00:03:08,148
- London?
- ( Gasps )

66
00:03:08,183 --> 00:03:10,416
Hey, audience.
We have a surprise guest.

67
00:03:10,450 --> 00:03:13,316
It's Bailey...

68
00:03:13,351 --> 00:03:15,483
- Pickett.
- Pick what?

69
00:03:15,517 --> 00:03:17,816
Do I have a boog hanging?

70
00:03:17,850 --> 00:03:19,750
London.

71
00:03:19,784 --> 00:03:22,252
What are you doing
with my skirt?

72
00:03:22,286 --> 00:03:24,423
Oh, it's a new
segment on "Yay Me!"

73
00:03:24,458 --> 00:03:26,791
Called "outfit
or tablecloth?"

74
00:03:26,825 --> 00:03:28,292
Wave to the world.

75
00:03:29,861 --> 00:03:32,929
Will you stern it?
London, my granny made this skirt for me.

76
00:03:32,964 --> 00:03:35,698
Aww! So tell us,

77
00:03:35,733 --> 00:03:37,233
why does grammy hate you?

78
00:03:37,267 --> 00:03:39,834
She does not hate me.

79
00:03:39,869 --> 00:03:42,971
She loves me. In fact, she
calls me her piggly wiggly,

80
00:03:43,006 --> 00:03:45,173
and then we snort together.
( Snorting )

81
00:03:46,942 --> 00:03:49,077
That thing isn't
still on, is it?

82
00:03:49,110 --> 00:03:53,012
Yep, and that snort was almost as
funny as our earlier segment...

83
00:03:53,013 --> 00:03:56,447
"parachute or
Bailey's underpants?"

84
00:03:56,482 --> 00:03:58,282
- My what? London...
- I'm sorry.

85
00:03:58,316 --> 00:04:00,250
We're out of time.

86
00:04:00,284 --> 00:04:02,886
But you can watch
those embarrassing clips

87
00:04:02,920 --> 00:04:05,221
over and over and over
and over again...

88
00:04:05,255 --> 00:04:09,457
At www.yaymestarringlondontipton.com

89
00:04:12,359 --> 00:04:14,293
and clear.

90
00:04:14,327 --> 00:04:18,263
London, I have
a problem with this show.

91
00:04:18,297 --> 00:04:20,865
I know, my hair
and makeup person stinks.

92
00:04:20,899 --> 00:04:24,334
Hey, I did my best.

93
00:04:24,369 --> 00:04:27,537
I meant I would
prefer you not mock

94
00:04:27,572 --> 00:04:28,973
my wardrobe to the world

95
00:04:29,007 --> 00:04:30,876
or lock me out of the room

96
00:04:30,910 --> 00:04:32,277
- when I need to get...
- An autograph?

97
00:04:32,312 --> 00:04:34,947
All right.

98
00:04:34,982 --> 00:04:37,017
You just signed my math book.

99
00:04:37,051 --> 00:04:38,184
And now it's worth something.

100
00:04:43,390 --> 00:04:45,357
Sorry, boys.
This is senior bingo.

101
00:04:45,392 --> 00:04:48,060
Nobody under 75 allowed.

102
00:04:48,094 --> 00:04:50,362
We're not here
to play bingo, Kirby.

103
00:04:50,396 --> 00:04:53,333
We're here because, you know, I
felt bad about the bungee thing,

104
00:04:53,367 --> 00:04:54,968
and I wanted to make
it up to you.

105
00:04:55,002 --> 00:04:57,036
You brought me a sandwich?

106
00:04:57,070 --> 00:04:58,871
Uh, no.

107
00:05:00,273 --> 00:05:02,607
Anyway, we were checking
the job listing board,

108
00:05:02,642 --> 00:05:04,809
which I do every
hour on the hour,

109
00:05:04,844 --> 00:05:08,145
since I <i>hate</i> being towel boy...
Thank you, Zack.

110
00:05:08,179 --> 00:05:10,012
You're welcome.

111
00:05:10,046 --> 00:05:13,248
And we noticed an opening for
executive security guard.

112
00:05:13,282 --> 00:05:15,415
And we thought you would
be perfect for it.

113
00:05:15,449 --> 00:05:18,049
Oh, thanks, boys, but
I'm happy with my job.

114
00:05:18,084 --> 00:05:20,485
- Never a dull moment.
- ( Crunches )

115
00:05:20,519 --> 00:05:23,254
( Groans )
See?

116
00:05:23,288 --> 00:05:26,156
Danger around every corner.

117
00:05:26,191 --> 00:05:28,892
Look, this is a great
opportunity for you, Kirby.

118
00:05:28,927 --> 00:05:31,828
I know, but the extra cash would
bump me into a higher tax bracket,

119
00:05:31,863 --> 00:05:33,796
and I don't think my
accountant would recommend it.

120
00:05:33,831 --> 00:05:35,931
I'm your accountant...

121
00:05:37,099 --> 00:05:40,701
- And I recommend it.
- The truth is...

122
00:05:40,735 --> 00:05:43,270
I don't meet
the job requirements.

123
00:05:43,304 --> 00:05:46,541
- I never finished high school.
- Really?

124
00:05:46,575 --> 00:05:48,744
Yeah, I had to leave early
to help support my family.

125
00:05:48,779 --> 00:05:50,946
Lucky.

126
00:05:50,981 --> 00:05:54,816
Kirby, it's never too
late to get your diploma.

127
00:05:54,850 --> 00:05:57,018
You could take
the g.E.D. Test.

128
00:05:57,053 --> 00:05:59,288
Yeah, but it's been years
since I studied that stuff.

129
00:05:59,322 --> 00:06:01,624
How am I supposed to catch up?

130
00:06:03,891 --> 00:06:06,259
Are you doing okay, Kirby?

131
00:06:06,293 --> 00:06:08,594
As soon as I can feel my
legs, I'll let you know.

132
00:06:08,628 --> 00:06:11,997
Well, good morning,

133
00:06:12,031 --> 00:06:13,598
every... Whoa.

134
00:06:14,800 --> 00:06:17,934
( Chuckles ) Uh, nice
to see you, Kirby.

135
00:06:17,969 --> 00:06:19,903
If you're here to arrest Zack,

136
00:06:19,937 --> 00:06:22,038
could you do it during lunch?

137
00:06:22,072 --> 00:06:25,106
Actually, I'm clean.

138
00:06:25,140 --> 00:06:27,108
Kirby is your new student.

139
00:06:27,142 --> 00:06:29,076
Just for a few
refresher classes

140
00:06:29,111 --> 00:06:31,278
until he can pass his high
school equivalency test.

141
00:06:31,312 --> 00:06:34,413
- Well, I guess...
- I brought you an apple.

142
00:06:34,447 --> 00:06:36,515
Oh, how nice.

143
00:06:36,549 --> 00:06:39,450
You can come to my class
anytime you want.

144
00:06:39,485 --> 00:06:42,720
It's here in my lunch bag.

145
00:06:42,755 --> 00:06:45,523
Could you hold that?

146
00:06:47,125 --> 00:06:49,293
And hold my chips.

147
00:06:49,327 --> 00:06:50,927
I know it's in here somewhere.

148
00:06:50,961 --> 00:06:53,262
You have two
foot-long sandwiches?

149
00:06:53,296 --> 00:06:54,765
Actually, they're 11 inches.

150
00:06:54,798 --> 00:06:56,231
I'm trying to cut down.

151
00:06:56,265 --> 00:06:58,099
There you go.

152
00:06:58,133 --> 00:07:00,734
Aha, here it is.

153
00:07:09,411 --> 00:07:11,677
Thank you for making
the trip, Kirby.

154
00:07:13,080 --> 00:07:14,915
And maybe your
kindness will rub off

155
00:07:14,949 --> 00:07:16,950
on the rest of the class.

156
00:07:16,985 --> 00:07:19,752
Hey, I brought you a chocolate
cupcake just last week.

157
00:07:19,786 --> 00:07:21,453
No, you snuck in
a chocolate cupcake,

158
00:07:21,487 --> 00:07:23,020
and I confiscated it.

159
00:07:23,055 --> 00:07:25,056
You still ate it.

160
00:07:25,090 --> 00:07:28,092
Well, it was delicious.

161
00:07:28,127 --> 00:07:31,728
Thank you. I'm known
for my frosting.

162
00:07:36,666 --> 00:07:39,134
Hey, Bailey.
You mind if I join you?

163
00:07:39,168 --> 00:07:41,870
( Giggles )
Sure, holden.

164
00:07:43,072 --> 00:07:45,540
What Bailey doesn't
know is we switched out

165
00:07:45,574 --> 00:07:47,176
<i>her bland</i>
<i>tuna fish sandwich</i>

166
00:07:47,210 --> 00:07:48,643
with a howlin' hot
habañero hoagie.

167
00:07:48,677 --> 00:07:50,678
( Giggles )

168
00:07:51,780 --> 00:07:54,415
So, beautiful day today, huh?

169
00:07:54,451 --> 00:07:57,250
Uhm, hot.

170
00:07:57,284 --> 00:07:59,318
Well, I guess it is
a little warm.

171
00:07:59,353 --> 00:08:00,853
Hot hot.

172
00:08:00,888 --> 00:08:03,155
Oh, thanks.
I guess the body mist is working.

173
00:08:03,190 --> 00:08:06,327
( Spits, grunting )

174
00:08:07,396 --> 00:08:10,598
See?
Howling hot!

175
00:08:10,632 --> 00:08:12,767
Water water.

176
00:08:12,802 --> 00:08:15,103
Empty. Empty!

177
00:08:21,678 --> 00:08:23,612
Boo you!

178
00:08:23,646 --> 00:08:25,914
You've just been
boo-youed.

179
00:08:25,949 --> 00:08:27,916
It's the newest
segment on "Yay Me!"

180
00:08:27,951 --> 00:08:30,552
Wave to the world.

181
00:08:30,587 --> 00:08:33,455
Viewers, you asked for
more wacky hayseed Bailey

182
00:08:33,489 --> 00:08:35,624
and now you got it.

183
00:08:35,658 --> 00:08:37,659
So Bailey, how was your hoagie?

184
00:08:37,693 --> 00:08:41,295
Howwible. Wondon, I am
vewy angwy about thith.

185
00:08:41,329 --> 00:08:43,563
I have no idea
what you're saying,

186
00:08:43,597 --> 00:08:45,364
and you're in my shot.

187
00:08:45,399 --> 00:08:47,901
And now for our next segment:

188
00:08:47,935 --> 00:08:49,902
Beauty tips with London Tipton.

189
00:08:49,937 --> 00:08:52,271
You might wanna
stay here for this.

190
00:08:55,842 --> 00:08:57,576
All right, Kirby, I broke out

191
00:08:57,610 --> 00:08:59,578
a few of my flash cards
to help you study.

192
00:08:59,612 --> 00:09:02,347
My math may be rusty,

193
00:09:02,382 --> 00:09:05,083
but I think that's
more than a few.

194
00:09:05,117 --> 00:09:07,685
Oh, all of this information
is going to be on the test...

195
00:09:07,720 --> 00:09:10,688
And more.

196
00:09:10,722 --> 00:09:12,322
You're going to need to learn it all.

197
00:09:14,526 --> 00:09:17,428
No, he won't.
He just needs to pass,

198
00:09:17,462 --> 00:09:19,696
which means a third of
this is a waste of time.

199
00:09:19,730 --> 00:09:21,564
( Gasps )

200
00:09:21,599 --> 00:09:25,067
You just knocked the age of
enlightenment all over the floor.

201
00:09:25,102 --> 00:09:27,469
And half of this
is common knowledge.

202
00:09:27,503 --> 00:09:29,170
Cody: <i>Ow.</i>

203
00:09:29,205 --> 00:09:32,973
You just hit me with the
industrial revolution.

204
00:09:33,008 --> 00:09:34,975
Plus the test is
multiple choice,

205
00:09:35,009 --> 00:09:37,010
so just by guessing

206
00:09:37,044 --> 00:09:39,846
- you'll get a bunch right.
- Cody: <i>Ow.</i>

207
00:09:39,881 --> 00:09:43,116
The middle ages are
no less painful.

208
00:09:43,150 --> 00:09:46,251
This is pretty much
all you'll need.

209
00:09:46,285 --> 00:09:49,722
"If found, return
to Cody Martin."

210
00:09:49,756 --> 00:09:52,757
- Here you go.
- Oh, thank you.

211
00:09:52,791 --> 00:09:55,492
Look, I'm sure Kirby
wants to do more

212
00:09:55,526 --> 00:09:56,827
than just pass.

213
00:09:56,861 --> 00:09:59,061
Yeah, I kinda
wanna get an "a."

214
00:09:59,095 --> 00:10:01,264
You know, so my mama can
put it on the fridge.

215
00:10:01,298 --> 00:10:02,900
Would you forget the cards?

216
00:10:04,235 --> 00:10:06,203
Without the cards,

217
00:10:06,237 --> 00:10:09,238
how is Kirby supposed to
remember crucial facts,

218
00:10:09,273 --> 00:10:12,274
like "when Columbus
discovered America"?

219
00:10:12,309 --> 00:10:14,909
Easy...
football.

220
00:10:14,944 --> 00:10:16,311
Come again?

221
00:10:16,345 --> 00:10:18,312
The answer is 1492.

222
00:10:18,346 --> 00:10:20,914
I just think of the
quarterback Brian greasy,

223
00:10:20,948 --> 00:10:22,582
who attended Christopher
Columbus high school,

224
00:10:22,617 --> 00:10:24,417
wears #14

225
00:10:24,452 --> 00:10:26,885
and has 92 career
interceptions.

226
00:10:26,919 --> 00:10:28,719
1492.

227
00:10:28,753 --> 00:10:31,489
Wow, Zack.
You really are smart.

228
00:10:31,523 --> 00:10:34,258
Oh yeah? And how is that
gonna help you in botany?

229
00:10:34,293 --> 00:10:37,895
There's no quarterback
named Phil O'Dendron.

230
00:10:37,929 --> 00:10:41,029
I have a whole different
system for botany.

231
00:10:41,063 --> 00:10:43,665
- What's that?
- I get a "d."

232
00:10:46,502 --> 00:10:49,303
( Whispering )
Good morning, Internet.

233
00:10:49,337 --> 00:10:51,438
Welcome to another installment

234
00:10:51,472 --> 00:10:54,309
of the hit series, "Yay Me!
Starring London..."

235
00:10:54,341 --> 00:10:56,175
- <i>( Bailey snoring )</i>
- Shh.

236
00:10:56,209 --> 00:10:58,176
( Whispering )
"...Tipton."

237
00:10:58,210 --> 00:11:00,477
Today, due to the
overwhelming response

238
00:11:00,512 --> 00:11:02,344
to "Yay Me's" "Boo You,"

239
00:11:02,379 --> 00:11:05,712
we'll once again be pranking
my snoring roommate, Bailey.

240
00:11:11,987 --> 00:11:14,590
Cock-a-doodle-doo!
( Laughing )

241
00:11:14,624 --> 00:11:17,192
Is it my morning
to milk the cows?

242
00:11:17,227 --> 00:11:20,931
- Boo you!
- ( All laughing )

243
00:11:20,965 --> 00:11:23,934
- What's going on?
- Woody, zoom in on her head.

244
00:11:23,968 --> 00:11:26,503
She's got crazy robot parts.

245
00:11:26,537 --> 00:11:29,373
Wait, wave to the world.
Hi, world.

246
00:11:29,407 --> 00:11:32,276
So how does it feel to
be boo-youed again?

247
00:11:32,310 --> 00:11:35,012
It feels lousy...
Again.

248
00:11:35,047 --> 00:11:37,615
I'm sorry. I don't understand
your crazy robot talk.

249
00:11:37,649 --> 00:11:39,683
( Laughs )

250
00:11:39,717 --> 00:11:42,152
Understand this.

251
00:11:42,186 --> 00:11:44,888
( Camera shatters )

252
00:11:47,825 --> 00:11:50,326
Woody, that camera's
coming out of your salary.

253
00:11:50,361 --> 00:11:52,161
I'm getting a salary?!

254
00:11:55,864 --> 00:11:58,731
- So Kirby, are you ready?
- Absolutely.

255
00:11:58,766 --> 00:12:01,633
I have five number two
pencils freshly sharpened,

256
00:12:01,668 --> 00:12:05,301
and six Swiss cheese
sandwiches freshly toasted.

257
00:12:05,336 --> 00:12:08,903
Now the high school equivalency
test is rather long.

258
00:12:08,937 --> 00:12:12,372
It covers five subjects,
14 sub-subjects,

259
00:12:12,406 --> 00:12:15,240
and ends with
a 2,000-word essay.

260
00:12:15,274 --> 00:12:17,007
Well, I know what
the first word is.

261
00:12:17,041 --> 00:12:19,374
"Dang."

262
00:12:19,408 --> 00:12:21,408
Well, don't worry.

263
00:12:21,442 --> 00:12:23,309
You'll do great.

264
00:12:23,343 --> 00:12:24,977
Oh, I'm not worried.

265
00:12:25,011 --> 00:12:28,312
What would make you
think I was worried?

266
00:12:28,346 --> 00:12:30,313
Sit tight.
I'll get a bucket.

267
00:12:32,249 --> 00:12:35,283
Man, I hope Kirby's
gonna be okay.

268
00:12:35,317 --> 00:12:37,650
I mean, seven hours...
that's a long test.

269
00:12:37,685 --> 00:12:39,651
Don't worry.
They only give seven hours

270
00:12:39,686 --> 00:12:42,618
to people
who need it, like you.

271
00:12:42,653 --> 00:12:46,120
He'll be out
before you know it.

272
00:12:46,154 --> 00:12:48,454
( Babbling )

273
00:12:52,558 --> 00:12:54,492
Or maybe sooner.

274
00:12:54,526 --> 00:12:57,795
Oh, poor Kirby.

275
00:12:57,829 --> 00:13:00,864
He couldn't get past
the first question.

276
00:13:00,898 --> 00:13:04,934
- What was it?
- "Put name here."

277
00:13:04,968 --> 00:13:07,470
I hate that one.

278
00:13:14,943 --> 00:13:18,211
Kirby, what happened to you?

279
00:13:18,246 --> 00:13:20,680
I was sitting there all
ready to take the test,

280
00:13:20,714 --> 00:13:24,316
- then I realized I have testophobia.
- ( Zack gasps )

281
00:13:26,985 --> 00:13:30,020
How long do you have, big guy?

282
00:13:30,054 --> 00:13:32,988
No, he means he's
afraid of taking tests.

283
00:13:33,023 --> 00:13:35,590
I guess the truth is I'm
just afraid to fail.

284
00:13:35,624 --> 00:13:37,457
That fear takes over
my whole body,

285
00:13:37,492 --> 00:13:40,159
I get all sweaty,
my leg twitches

286
00:13:40,193 --> 00:13:42,360
and my eyes start blinking like
a movie projector, just...

287
00:13:42,395 --> 00:13:44,562
( Babbling )

288
00:13:46,536 --> 00:13:50,069
Kirby, a lot of people
are scared to fail.

289
00:13:50,105 --> 00:13:52,772
You just need to find
a way to relax your mind

290
00:13:52,806 --> 00:13:54,807
and conquer your fear.

291
00:13:54,842 --> 00:13:57,143
Well, I don't know, guys.

292
00:13:57,178 --> 00:13:58,912
Afraid? You?

293
00:13:58,946 --> 00:14:00,914
I mean, this is
coming from the man

294
00:14:00,948 --> 00:14:02,816
who caught that guy trying
to sneak out of the buffet

295
00:14:02,850 --> 00:14:04,750
with his pants full of shrimp.

296
00:14:04,785 --> 00:14:07,485
That guy was five.

297
00:14:07,520 --> 00:14:10,790
My point is you
weren't scared then

298
00:14:10,824 --> 00:14:13,126
and you shouldn't
be scared now.

299
00:14:13,160 --> 00:14:14,327
You're right.

300
00:14:14,361 --> 00:14:16,962
I have nothing to be afraid of.

301
00:14:16,996 --> 00:14:19,030
Oh, walker lady.
Nuh-uh, run.

302
00:14:20,432 --> 00:14:22,866
Oh, Bailey, do you like this outfit?

303
00:14:22,901 --> 00:14:25,670
- Nope.
- Are you still mad at me?

304
00:14:25,704 --> 00:14:27,238
Yep.

305
00:14:27,273 --> 00:14:29,341
Normally, I like
one-word responses...

306
00:14:29,376 --> 00:14:31,979
being easier
to understand and all...

307
00:14:32,013 --> 00:14:34,549
but come on, give me
a long, boring story

308
00:14:34,583 --> 00:14:36,350
about how back on the farm

309
00:14:36,385 --> 00:14:38,386
the chicken dated the
cow and they broke up

310
00:14:38,420 --> 00:14:39,754
because they couldn't agree on
how to raise their children.

311
00:14:39,788 --> 00:14:41,755
London, don't you realize

312
00:14:41,790 --> 00:14:44,091
you embarrassed me in front
of holden, the whole school

313
00:14:44,125 --> 00:14:45,494
and the entire world?

314
00:14:45,526 --> 00:14:47,961
Not the <i>entire</i> world.

315
00:14:47,995 --> 00:14:49,829
We only got 37 million hits.

316
00:14:51,065 --> 00:14:53,333
Okay, I'm sorry
I embarrassed you,

317
00:14:53,367 --> 00:14:55,669
and I promise
I won't do it anymore.

318
00:14:55,703 --> 00:14:57,871
That's what you would say if you
were going to "boo you" me again.

319
00:14:57,906 --> 00:15:00,742
- But I'm not.
- And you would say that

320
00:15:00,776 --> 00:15:03,277
- to throw me off guard.
- Well, I'm not that smart.

321
00:15:03,312 --> 00:15:04,847
But now that I said it, you're
smart enough to retain it.

322
00:15:04,881 --> 00:15:06,382
Retain what?

323
00:15:06,416 --> 00:15:08,050
Boy, you're good.

324
00:15:09,987 --> 00:15:11,820
Good at what?

325
00:15:14,124 --> 00:15:16,457
Okay, Kirby, to help you relax

326
00:15:16,492 --> 00:15:18,692
and get over your testophobia,

327
00:15:18,726 --> 00:15:22,163
we will be channeling
the three-toed sloth:

328
00:15:25,367 --> 00:15:27,402
Controlling our breathing,

329
00:15:27,436 --> 00:15:29,470
slowing our heart rate.

330
00:15:29,505 --> 00:15:33,173
Now let us harness

331
00:15:33,207 --> 00:15:35,409
the methodical grace

332
00:15:35,443 --> 00:15:37,711
of the loggerhead turtle.

333
00:15:37,745 --> 00:15:41,682
( Mimics turtle groan )

334
00:15:42,916 --> 00:15:46,384
Nuh-uh, big man don't
flow like that.

335
00:15:46,419 --> 00:15:50,121
Contorting yourself
is not relaxing.

336
00:15:50,155 --> 00:15:52,556
Relaxing is lying on a sofa

337
00:15:52,591 --> 00:15:54,725
eating cheese
doo-dahs.

338
00:15:54,759 --> 00:15:56,226
Oh, I can do
doo-dahs.

339
00:15:56,260 --> 00:15:58,796
I can do doo-dahs
all the doo-dah day.

340
00:15:58,830 --> 00:16:00,764
Don't question the turtle.

341
00:16:00,798 --> 00:16:02,798
He <i>needs</i> the turtle.

342
00:16:02,833 --> 00:16:05,367
No, he needs cheesy snacks.

343
00:16:05,401 --> 00:16:08,168
Who uses cheesy
snacks to study?

344
00:16:08,203 --> 00:16:09,636
Really smart mice!

345
00:16:09,670 --> 00:16:11,971
How do you wanna relax, Kirby?

346
00:16:12,005 --> 00:16:14,606
- Yeah, Kirby, how?
- ( Babbling )

347
00:16:16,075 --> 00:16:18,410
( Screams )

348
00:16:21,947 --> 00:16:23,814
Holden?

349
00:16:23,848 --> 00:16:27,583
Wow, this invitation
really was from you.

350
00:16:27,617 --> 00:16:30,419
Yeah, I thought
it would be nice

351
00:16:30,453 --> 00:16:32,720
- to have dinner together.
- Are you sure?

352
00:16:32,754 --> 00:16:36,622
Last time we ate together, I
hawked jalapeno on your shirt.

353
00:16:36,657 --> 00:16:38,958
I'll risk it.

354
00:16:47,500 --> 00:16:50,835
( Giggles ) Okay.

355
00:16:50,872 --> 00:16:52,503
You know, since the
first time I saw you,

356
00:16:52,538 --> 00:16:54,004
I wanted to ask you out.

357
00:16:54,038 --> 00:16:55,770
- Really?
- <i>Yeah, really.</i>

358
00:16:55,804 --> 00:16:57,571
I think you're adorable.

359
00:16:57,605 --> 00:17:00,573
( Giggles ) Okay.

360
00:17:00,607 --> 00:17:03,642
So when London came to me and
set this whole thing up,

361
00:17:03,676 --> 00:17:05,810
- I was thrilled.
- Wait a minute.

362
00:17:05,844 --> 00:17:08,045
- London set this up?
- Yeah, and wasn't that nice of her?

363
00:17:08,079 --> 00:17:10,682
- I knew it. Where's the camera?
- What camera?

364
00:17:10,716 --> 00:17:12,618
Is it in one of these rolls?

365
00:17:12,653 --> 00:17:14,687
No, it's not in any of the rolls.
What are you doing?

366
00:17:14,721 --> 00:17:17,088
Ow!

367
00:17:17,122 --> 00:17:19,956
Maybe it's in the butter.

368
00:17:19,990 --> 00:17:21,891
There's nothing in the butter.

369
00:17:21,925 --> 00:17:24,627
Okay, what's going on?

370
00:17:24,661 --> 00:17:26,729
Oh, I get it...
flower-cam.

371
00:17:28,331 --> 00:17:30,132
Bailey, what are you doing?

372
00:17:30,166 --> 00:17:33,069
Oh, I'm sorry.
Did I ruin your stupid show?

373
00:17:33,103 --> 00:17:36,105
Well, boo you, London!
Boo <i>you!</i>

374
00:17:36,139 --> 00:17:37,706
But this isn't
"boo you."

375
00:17:37,741 --> 00:17:39,308
It's not a prank.

376
00:17:39,342 --> 00:17:41,008
I arranged this dinner
to make it up to you.

377
00:17:41,042 --> 00:17:44,242
- Huh?
- You see?

378
00:17:44,277 --> 00:17:46,043
Holden really likes you.

379
00:17:46,077 --> 00:17:48,746
I did, before I became
a human buffet.

380
00:17:51,746 --> 00:17:55,081
So this was a real date?

381
00:17:55,082 --> 00:17:58,317
Yeah, I was trying to be nice.

382
00:17:58,352 --> 00:18:00,085
Gee, for some reason,
after becoming

383
00:18:00,120 --> 00:18:01,620
the download dork of the day,

384
00:18:01,655 --> 00:18:03,188
that thought didn't
occur to me.

385
00:18:03,223 --> 00:18:05,324
I don't know what to do.

386
00:18:05,359 --> 00:18:07,594
I mean, you get mad at me when I
make you the star of my show,

387
00:18:07,628 --> 00:18:09,028
you get mad at me when I set
you up with a cute guy...

388
00:18:09,063 --> 00:18:11,263
You know, there's just
no pleasing you.

389
00:18:11,297 --> 00:18:13,731
You're right, London.
This is all <i>my</i> fault.

390
00:18:13,766 --> 00:18:16,532
No! Not all of it.

391
00:18:17,634 --> 00:18:20,068
A small part of it was mine.

392
00:18:20,102 --> 00:18:21,501
I'm sorry.

393
00:18:21,535 --> 00:18:23,569
Thank you.

394
00:18:23,603 --> 00:18:25,403
But boy, did you blow it with holden.

395
00:18:25,438 --> 00:18:27,938
And he is <i>cute.</i>

396
00:18:27,972 --> 00:18:30,640
And available.

397
00:18:35,111 --> 00:18:37,345
Now when Kirby comes
in to take his test,

398
00:18:37,379 --> 00:18:40,347
he will be relaxed by the soothing
sounds of trickling water.

399
00:18:40,382 --> 00:18:43,016
( Clicks )

400
00:18:43,050 --> 00:18:45,884
Bloop.

401
00:18:45,919 --> 00:18:48,153
Are you trying to relax him

402
00:18:48,187 --> 00:18:49,987
or make him go potty?

403
00:18:50,021 --> 00:18:52,756
Now this baby...

404
00:18:52,790 --> 00:18:54,224
( moans )

405
00:18:54,258 --> 00:18:56,958
This baby is what he needs.

406
00:18:56,993 --> 00:18:59,460
- And check this out.
- ( Beeps )

407
00:18:59,494 --> 00:19:02,495
Turn that off.

408
00:19:02,530 --> 00:19:03,829
He can't hear my waterfall.

409
00:19:03,864 --> 00:19:05,998
Oh, come on.

410
00:19:06,032 --> 00:19:09,601
There's no way he can be
tense sitting in this thing.

411
00:19:09,635 --> 00:19:12,804
- Kirby, right on time.
- ( Beeps )

412
00:19:12,839 --> 00:19:15,307
Actually, I just came to tell
miss Tutweiller I can't do this.

413
00:19:15,342 --> 00:19:16,909
Sure you can.

414
00:19:16,943 --> 00:19:19,812
You know this material inside out.
For example,

415
00:19:19,847 --> 00:19:21,981
name a place where Portuguese
is the main language

416
00:19:22,015 --> 00:19:23,548
other than Portugal.

417
00:19:23,583 --> 00:19:26,551
That's easy.
Angola, Brazil, Cape Verde,

418
00:19:26,586 --> 00:19:29,154
Guinea-Bissau,
Mozambique, Macau

419
00:19:29,188 --> 00:19:31,056
and Bento's Brazilian barbeque.

420
00:19:31,091 --> 00:19:33,959
Best yucca fries
this side of Sao Paolo.

421
00:19:35,695 --> 00:19:37,363
And if you went to Vietnam,

422
00:19:37,397 --> 00:19:39,298
you would exchange
your dollars for what?

423
00:19:39,333 --> 00:19:41,233
Dongs, and you better
exchange it this week,

424
00:19:41,267 --> 00:19:43,168
'cause the dong is strong.

425
00:19:44,770 --> 00:19:48,072
And I'm sure you know what the
national dance of Bulgaria is.

426
00:19:48,106 --> 00:19:50,041
Who doesn't?
It's the trakijsko horo,

427
00:19:50,074 --> 00:19:52,141
and a lot of people
do it in place,

428
00:19:52,175 --> 00:19:53,975
but you're really supposed
to travel to the right.

429
00:19:54,010 --> 00:19:55,776
Digga-digga-da! Digga-digga-da!
Digga-digga-da!

430
00:19:55,811 --> 00:19:58,278
Otherwise, you're doing
the divatensko

431
00:19:58,313 --> 00:20:00,380
and you just look silly.

432
00:20:00,414 --> 00:20:03,049
You see?
You know all the answers.

433
00:20:03,083 --> 00:20:06,051
Yeah, but I still don't think I
can make it through that test.

434
00:20:06,085 --> 00:20:08,287
- You just did.
- Huh?

435
00:20:08,321 --> 00:20:10,356
The world studies
section anyway.

436
00:20:10,390 --> 00:20:12,224
I don't understand.

437
00:20:12,258 --> 00:20:14,258
You see, Cody and I realized
that instead of helping you,

438
00:20:14,293 --> 00:20:16,160
we only made you more nervous.

439
00:20:16,194 --> 00:20:18,995
So we figured the best way
to get you to pass the test

440
00:20:19,029 --> 00:20:21,329
was to trick you into thinking
you weren't taking it.

441
00:20:21,364 --> 00:20:23,632
Miss Tutweiller saw you
answer all those questions

442
00:20:23,666 --> 00:20:25,367
<i>on a video feed.</i>

443
00:20:25,401 --> 00:20:27,402
And all these years,
I had no idea

444
00:20:27,436 --> 00:20:29,304
I had no idea I was doing
the trakijsko horo wrong.

445
00:20:29,338 --> 00:20:31,038
Oh yeah, it's just the hips.

446
00:20:31,072 --> 00:20:33,808
- You gotta go to the right.
- I know, I saw.

447
00:20:33,843 --> 00:20:35,677
So we borrowed one
of London's webcams

448
00:20:35,711 --> 00:20:38,513
and hid it in this apple.

449
00:20:38,548 --> 00:20:40,182
Wow, a fruit-cam.

450
00:20:41,483 --> 00:20:43,751
Now that's clever, boys.

451
00:20:43,785 --> 00:20:46,288
- Thanks, guys.
- ( Both groaning )

452
00:20:47,555 --> 00:20:49,889
If all of you believe
in me this much,

453
00:20:49,924 --> 00:20:51,925
then how can I not
believe in myself?

454
00:20:51,959 --> 00:20:55,165
Miss Tutweiller, bring on
the rest of that test.

455
00:20:55,198 --> 00:20:56,565
- <i>( Chair beeps )</i>
- Miss Tutweiller?

456
00:20:56,599 --> 00:20:57,966
In a minute.

457
00:20:58,000 --> 00:21:00,101
This feels <i>good.</i>

458
00:21:01,448 --> 00:21:03,120
Ladies and gentlemen,

459
00:21:03,240 --> 00:21:07,637
I give you the graduating class
of seven seas high.

460
00:21:14,019 --> 00:21:15,974
They grow up so fast...

461
00:21:16,620 --> 00:21:18,845
You think they'll come home
for the holidays?

462
00:21:20,542 --> 00:21:23,830
I hope not. Thanksgiving.
I can't cook so much food.

463
00:21:25,589 --> 00:21:29,523
I will now read the names of
the graduates in alphabetical order.

464
00:21:29,744 --> 00:21:31,257
Kirby Morris...

465
00:21:35,634 --> 00:21:36,617
That's it.

466
00:21:38,628 --> 00:21:40,600
As valedictorian,

467
00:21:40,720 --> 00:21:43,507
most likely to succeed and
best looking in my class,

468
00:21:43,859 --> 00:21:46,588
I would like to thank my teacher,
miss Tutweiller,

469
00:21:46,945 --> 00:21:49,796
and my two twin tutors,
Zack and Cody.

470
00:21:50,436 --> 00:21:52,615
Without you guys, I would have
never had the courage

471
00:21:52,735 --> 00:21:54,747
to get my diploma and earn my promotion.

472
00:21:55,071 --> 00:21:57,161
Make room on the ***, mama.

473
00:22:01,481 --> 00:22:03,460
Oh, dear. That was a rental.

474
00:22:05,332 --> 00:22:08,403
I am so proud. Come here, little guy.

475
00:22:10,551 --> 00:22:12,174
Thanks, guys.

476
00:22:15,137 --> 00:22:16,772
Just one more question.

477
00:22:17,018 --> 00:22:17,990
What's that?

478
00:22:18,110 --> 00:22:19,883
When's the prom?

479
00:22:21,781 --> 00:22:30,912
Sync by dr.jackson
www.addic7ed.com

