1
00:01:00,363 --> 00:01:04,670
WOMAN OF THE DUNES

2
00:05:14,050 --> 00:05:16,535
What a terrible place to live

3
00:05:42,722 --> 00:05:44,397
Are you inspecting?

4
00:05:46,342 --> 00:05:47,580
lnspecting?

5
00:05:50,170 --> 00:05:53,604
ls there an inspection?

6
00:05:54,624 --> 00:05:57,651
Well, if you're not inspecting...

7
00:06:01,032 --> 00:06:03,145
l'm collecting insects

8
00:06:05,193 --> 00:06:10,531
My speciality is these sand insects

9
00:06:13,143 --> 00:06:15,733
Then you're not from the government

10
00:06:16,472 --> 00:06:20,778
The government? No, l'm
a schoolteacher

11
00:06:23,047 --> 00:06:27,136
A teacher... l see

12
00:07:47,650 --> 00:07:51,157
One needs so many corroborating
certificates

13
00:07:52,770 --> 00:07:55,723
ldentity card, usage permit,
contract...

14
00:07:56,348 --> 00:07:59,522
...union card, letters of
recommendation...

15
00:08:00,925 --> 00:08:06,294
...income and custody certificates,
even a pedigree certificate

16
00:08:15,658 --> 00:08:18,799
Aren't there anymore certificates?

17
00:08:21,109 --> 00:08:23,846
Have l overlooked any?

18
00:08:27,560 --> 00:08:29,744
No more to certify?

19
00:08:30,681 --> 00:08:34,697
Who knows the last certificate?

20
00:08:35,883 --> 00:08:39,346
Certificates are limitless, it seems

21
00:08:41,084 --> 00:08:44,112
There's no end to them

22
00:08:46,952 --> 00:08:51,852
You criticised me, you said
I argued too much

23
00:08:53,195 --> 00:08:55,607
But these are facts, not theories

24
00:09:03,599 --> 00:09:05,461
What now, Professor?

25
00:09:06,470 --> 00:09:10,454
Well, l'll come back again tomorrow

26
00:09:12,295 --> 00:09:14,636
But the last bus has gone

27
00:09:17,497 --> 00:09:21,077
Well, can't l stay somewhere?

28
00:09:23,033 --> 00:09:24,708
ln this village?

29
00:09:27,901 --> 00:09:30,637
Maybe along the main road somewhere

30
00:09:32,437 --> 00:09:35,828
-You're walking?
-l'm in no hurry

31
00:09:37,681 --> 00:09:42,466
You needn't go so far; l'll be glad
to help if l can

32
00:09:44,090 --> 00:09:47,668
This is a poor village, but
if you're not fussy...

33
00:09:48,667 --> 00:09:52,944
Really? That would give me more time
tomorrow

34
00:09:53,785 --> 00:09:58,281
And l'd very much like to stay in
a house in such a village

35
00:10:40,977 --> 00:10:44,077
Careful, there are deep holes

36
00:10:59,788 --> 00:11:04,210
Aunt, here's a guest for you,
hurry up

37
00:11:06,695 --> 00:11:09,141
l'm just coming

38
00:11:11,148 --> 00:11:14,728
-Are you sure?
-lt's no trouble at all

39
00:11:21,969 --> 00:11:26,682
-A rope ladder?
-lt must seem strange to you

40
00:11:29,625 --> 00:11:32,040
lt's really quite an adventure

41
00:11:32,539 --> 00:11:36,191
Mind how you go, don't look up

42
00:11:37,325 --> 00:11:39,842
You'll get sand in your eyes

43
00:12:32,048 --> 00:12:34,202
This way, please

44
00:12:52,855 --> 00:12:54,499
Welcome, guest

45
00:12:54,894 --> 00:12:56,643
Pleased to meet you

46
00:13:20,779 --> 00:13:22,204
Dinner will be ready soon

47
00:13:24,484 --> 00:13:27,292
Could l have a bath first?

48
00:13:27,854 --> 00:13:29,747
Yes, but...

49
00:13:30,309 --> 00:13:32,026
Never mind, then

50
00:13:32,473 --> 00:13:34,773
The day after tomorrow

51
00:13:37,467 --> 00:13:41,046
That's hopeless, l've only got
three days leave

52
00:13:41,711 --> 00:13:43,242
l'm sorry

53
00:14:31,774 --> 00:14:35,717
-Only one lamp?
-l'll soon be finished here

54
00:14:37,477 --> 00:14:39,587
You miss electricity

55
00:14:58,200 --> 00:15:03,391
Shellfish broth and bream -
local food is best

56
00:15:07,897 --> 00:15:10,965
-Why the umbrella?
-To keep the sand off

57
00:15:16,677 --> 00:15:21,682
-ls the roof damaged?
-No, it's the same with a new roof

58
00:15:23,336 --> 00:15:24,647
Why, then?

59
00:15:25,499 --> 00:15:28,861
lt's more pesky than a gribble

60
00:15:30,993 --> 00:15:32,272
Gribble?

61
00:15:33,740 --> 00:15:37,028
lt makes holes in wood

62
00:15:38,358 --> 00:15:40,991
A termite

63
00:15:41,646 --> 00:15:44,746
No, it's this big and scaly

64
00:15:47,347 --> 00:15:49,938
That's pri'conus insularis

65
00:15:57,419 --> 00:16:00,852
lsn't it red, with long antennae?

66
00:16:01,663 --> 00:16:05,534
No, bronze, like a grain of rice

67
00:16:07,364 --> 00:16:10,289
chalcophora japonica, perhaps

68
00:16:12,942 --> 00:16:18,133
lf it gets a hold, it will rot away
a huge beam

69
00:16:20,639 --> 00:16:24,219
-chalcophora japonica?
-No, the sand

70
00:16:28,088 --> 00:16:29,618
How can sand...

71
00:16:30,169 --> 00:16:32,468
Well, it draws water

72
00:16:34,331 --> 00:16:38,971
lmpossible. Sand is a naturally dry
substance

73
00:16:39,991 --> 00:16:42,248
Still, it rots things

74
00:16:44,486 --> 00:16:47,773
Don't talk rubbish.
Use your common sense

75
00:16:53,266 --> 00:16:57,209
A desert is a desert because it's dry

76
00:17:00,258 --> 00:17:03,618
Have you ever heard of a damp desert?

77
00:17:04,335 --> 00:17:06,084
Some tea, please

78
00:17:31,385 --> 00:17:33,904
Still, it does rot things

79
00:17:40,748 --> 00:17:45,534
lf you leave sand on clogs,
they rot within a month

80
00:17:53,857 --> 00:17:55,752
What nonsense

81
00:17:57,228 --> 00:18:00,111
Wood rots, and so does sand

82
00:18:01,140 --> 00:18:06,915
The roof of a buried house became
soil rich enough to grow cucumbers

83
00:18:10,170 --> 00:18:12,511
Well, never mind

84
00:18:28,356 --> 00:18:30,656
-Enough?
-Yes, thank you

85
00:18:34,099 --> 00:18:35,920
That was good

86
00:18:47,209 --> 00:18:49,216
lt is serious

87
00:18:51,577 --> 00:18:57,237
When the wind blows, one or two feet
of sand drift in a night

88
00:19:33,109 --> 00:19:35,991
Even the bedding is damp

89
00:19:37,812 --> 00:19:38,978
No others?

90
00:19:41,891 --> 00:19:46,313
-No family?
-No, l'm alone

91
00:19:54,458 --> 00:19:59,026
There was a storm. The sand
swallowed my husband and daughter

92
00:20:00,535 --> 00:20:01,960
Swallowed?

93
00:20:02,323 --> 00:20:07,983
lt roared down like a waterfall.
So they went out

94
00:20:10,396 --> 00:20:13,539
To rescue the chicken house and...

95
00:20:15,349 --> 00:20:16,952
They were buried?

96
00:20:19,177 --> 00:20:20,967
Such a wind

97
00:20:25,837 --> 00:20:30,258
That's terrible... really terrible

98
00:20:41,275 --> 00:20:42,877
lt's the sand

99
00:20:59,710 --> 00:21:01,209
Doing research?

100
00:21:04,746 --> 00:21:08,804
Have you seen this insect here?

101
00:21:11,280 --> 00:21:13,621
lt's called the blister-beetle

102
00:21:20,102 --> 00:21:25,512
l'm collecting the genus on this trip;
it has many species

103
00:21:26,760 --> 00:21:30,412
My name will be in the encyclopaedia
if l find a new one

104
00:21:32,253 --> 00:21:36,196
My only talent is to hold
insects' tails

105
00:21:42,034 --> 00:21:45,394
The tools for the helper

106
00:21:50,273 --> 00:21:55,391
-So there is someone else
-You do say nice things

107
00:21:56,265 --> 00:21:59,293
Didn't he talk about a helper?

108
00:21:59,886 --> 00:22:01,894
He meant you

109
00:22:02,342 --> 00:22:06,357
-Me? Why me?
-lt's all right, take no notice

110
00:22:07,335 --> 00:22:09,010
Some misunderstanding?

111
00:22:10,374 --> 00:22:14,868
-Do you need the lamp?
-Well, l'd rather have it

112
00:22:17,157 --> 00:22:20,402
,
Never mind, l'm used to the work
anyway

113
00:22:37,715 --> 00:22:39,754
This is filthy

114
00:24:52,673 --> 00:24:54,388
No

115
00:25:22,594 --> 00:25:26,318
-Can l help?
-lt's not right on the first day

116
00:25:27,005 --> 00:25:29,117
What do you mean, ''first day''?

117
00:25:30,250 --> 00:25:32,362
l'm only staying tonight

118
00:25:36,660 --> 00:25:38,481
You always clear sand at night?

119
00:25:38,906 --> 00:25:42,995
lt's easier when the sand is damp

120
00:25:49,644 --> 00:25:51,391
The basket

121
00:27:12,207 --> 00:27:14,393
What a job they have

122
00:27:15,496 --> 00:27:19,585
ln our village, the Native Place
spirit is strong

123
00:27:20,406 --> 00:27:22,227
What spirit?

124
00:27:23,819 --> 00:27:26,160
Love of One's Native Place

125
00:27:28,230 --> 00:27:31,039
l see. That's fine

126
00:27:37,967 --> 00:27:40,746
-When do you stop?
-At dawn

127
00:27:43,751 --> 00:27:46,852
The sand doesn't wait for us

128
00:28:18,500 --> 00:28:20,945
What a ruin

129
00:32:48,208 --> 00:32:50,986
That's funny, the ladder's gone

130
00:34:19,970 --> 00:34:24,652
Madam... please get up

131
00:34:39,362 --> 00:34:44,190
Excuse me, but where's the ladder?
It's disappeared

132
00:34:52,387 --> 00:34:57,465
l must leave now; l've only
three days leave

133
00:35:05,371 --> 00:35:08,108
lt was a rope ladder

134
00:35:09,699 --> 00:35:12,040
lt can't be put up from here

135
00:35:18,065 --> 00:35:20,477
You were in league with them

136
00:35:52,397 --> 00:35:53,749
l'm sorry

137
00:35:54,977 --> 00:35:57,567
What do you mean, ''sorry''?

138
00:36:03,216 --> 00:36:04,965
What do you mean?

139
00:36:06,171 --> 00:36:07,596
l'm sorry

140
00:36:08,418 --> 00:36:13,276
That means nothing. l have to make
the most of my time

141
00:36:15,785 --> 00:36:18,634
Don't you understand yet?

142
00:36:19,321 --> 00:36:21,984
What is there to understand?

143
00:36:26,021 --> 00:36:31,796
But, really, life here is too hard
for a woman alone

144
00:36:33,055 --> 00:36:34,772
What's that to do with me?

145
00:36:38,673 --> 00:36:43,791
The winds are coming;
there'll be sandstorms

146
00:36:46,455 --> 00:36:48,682
l'm your prisoner?

147
00:36:49,243 --> 00:36:50,449
l'm sorry

148
00:36:50,907 --> 00:36:54,924
Listen, l'm not a tramp, you know

149
00:36:55,651 --> 00:36:58,940
l've got a respectable job,
a registered address

150
00:37:00,521 --> 00:37:05,379
When the police find me, you'll be
charged with illegal confinement

151
00:37:21,870 --> 00:37:26,395
The sand will bury the house,
if we don't clear it

152
00:37:27,986 --> 00:37:31,972
Then let it. Why involve me?

153
00:37:37,890 --> 00:37:40,991
The next house will be in danger
if this is buried

154
00:37:41,720 --> 00:37:45,445
So it's buried. You, too

155
00:37:48,794 --> 00:37:54,600
Why do you slave to cling to such
a place? Are you insane?

156
00:38:05,774 --> 00:38:09,831
Why be faithful to the villagers?

157
00:38:10,934 --> 00:38:13,783
l've no sympathy for such
old, ''noble'' legends

158
00:38:14,471 --> 00:38:19,111
lf the village is afraid of
sand erosion, plant trees

159
00:38:19,922 --> 00:38:24,053
They proved it's much cheaper this way

160
00:38:27,913 --> 00:38:33,323
Call someone who's in authority here.
l can tell him a few things

161
00:38:36,818 --> 00:38:42,155
Come on. How do you contact
people outside?

162
00:38:43,850 --> 00:38:46,660
Bang on a tin can?

163
00:38:51,925 --> 00:38:53,381
Say something

164
00:39:42,569 --> 00:39:45,378
At least l'm not going against nature...

165
00:39:45,941 --> 00:39:49,113
...l'm not trying to make
a gentle slope steep

166
00:42:50,629 --> 00:42:52,668
Guest, are you awake?

167
00:43:17,927 --> 00:43:20,040
How do you feel?

168
00:43:20,591 --> 00:43:22,890
Not too bad

169
00:43:23,921 --> 00:43:25,565
l'll wipe your back

170
00:43:56,588 --> 00:44:00,241
That hurts, l tell you

171
00:44:09,072 --> 00:44:09,979
Shall l rub it?

172
00:44:10,612 --> 00:44:13,464
lt's no joke. My spine may be broken

173
00:44:14,026 --> 00:44:16,948
lf you're so kind, why not call
a doctor?

174
00:44:26,135 --> 00:44:27,811
ls my underwear dry?

175
00:44:28,215 --> 00:44:31,982
lt's better not to wear underwear
when you're asleep

176
00:44:33,501 --> 00:44:35,509
You'll get sand rash

177
00:44:38,287 --> 00:44:40,585
Sand draws water

178
00:45:03,006 --> 00:45:05,492
Don't make a sound

179
00:45:41,917 --> 00:45:46,265
You'll have to put up with it.
Now l've trapped you

180
00:45:47,452 --> 00:45:49,314
lt's your own fault

181
00:45:52,196 --> 00:45:54,827
A human being is not a dog

182
00:45:56,065 --> 00:45:59,092
lt's not the same as chaining up
a dog

183
00:46:52,080 --> 00:46:54,338
What are you doing?

184
00:46:55,076 --> 00:46:57,303
Still in bed, you two?

185
00:47:02,191 --> 00:47:04,636
l won't let go until you pull this up

186
00:47:05,229 --> 00:47:09,099
She's roped up indoors. Pull this up
if you want to save her

187
00:48:18,514 --> 00:48:22,312
No. They haven't won yet

188
00:48:23,882 --> 00:48:28,814
The battle has just begun. It's them
who are going to be in trouble

189
00:49:15,901 --> 00:49:18,315
Why should I be nervous?

190
00:49:19,438 --> 00:49:22,247
lt's me who's pinning them down,
after all

191
00:49:23,766 --> 00:49:26,429
This is real/y a good e Kperi'ence

192
00:49:26,971 --> 00:49:30,664
When I get home I might write
a story about all this

193
00:50:42,751 --> 00:50:44,978
Only cheap sake

194
00:50:47,288 --> 00:50:52,000
So they want me to drink to my
success; how considerate

195
00:51:00,813 --> 00:51:05,525
There's been a coup d'état.
It seems no one can stand still

196
00:51:10,342 --> 00:51:11,768
A cigarette?

197
00:51:12,423 --> 00:51:16,076
No, they make your throat dry

198
00:51:17,293 --> 00:51:19,924
-Water?
-Not yet

199
00:51:21,703 --> 00:51:23,057
Just ask

200
00:51:24,824 --> 00:51:28,144
l'm not doing this out of
personal spite

201
00:51:30,277 --> 00:51:31,661
Are you in pain?

202
00:51:34,438 --> 00:51:39,224
You must be careful. That ration
comes only once a week

203
00:51:41,888 --> 00:51:43,093
Ration?

204
00:51:43,469 --> 00:51:49,315
Only where there are men.
The village council pays for it

205
00:52:00,239 --> 00:52:05,826
Have other men been trapped like this?
l'm only curious

206
00:52:08,520 --> 00:52:11,476
We're very short of hands, you see

207
00:52:12,848 --> 00:52:14,305
What men?

208
00:52:15,554 --> 00:52:20,777
Well, about last autumn there was
the picture postcard man

209
00:52:23,085 --> 00:52:27,070
They say he was a salesman for
people who make them

210
00:52:28,371 --> 00:52:29,724
Any others?

211
00:52:30,535 --> 00:52:33,489
Since the beginning of the year...

212
00:52:35,195 --> 00:52:38,963
...a student on some sort of survey

213
00:52:41,354 --> 00:52:44,819
l hear he's with a neighbour

214
00:52:48,138 --> 00:52:50,843
l suppose he hasn't a ladder, either

215
00:52:54,548 --> 00:52:57,980
Our young people won't stay here

216
00:53:03,994 --> 00:53:07,021
They get more money in the cities

217
00:53:08,613 --> 00:53:13,065
After all, they've got cinemas
and restaurants there

218
00:53:14,481 --> 00:53:16,083
This is ridiculous

219
00:53:20,889 --> 00:53:22,709
What's the matter?

220
00:53:24,177 --> 00:53:28,234
Sorry, but will you please scratch
my right ear?

221
00:53:41,032 --> 00:53:42,311
Like this?

222
00:54:23,769 --> 00:54:27,349
l'm sorry, but will you give me
some water

223
00:54:32,092 --> 00:54:35,527
Some water. l beg you

224
00:54:53,858 --> 00:54:56,636
Not much left; when's the ration?

225
00:54:59,892 --> 00:55:02,483
That's enough

226
00:55:14,748 --> 00:55:17,006
lt's all your own fault

227
00:55:25,943 --> 00:55:27,513
The cords

228
00:55:28,772 --> 00:55:30,156
Want them off?

229
00:55:33,309 --> 00:55:39,635
l don't pity you, but l just can't
stand your misery

230
00:55:41,340 --> 00:55:47,447
But there's one condition. you won't
shovel the sand, do you promise?

231
00:55:49,331 --> 00:55:53,461
l promise. l'll promise anything

232
00:56:13,343 --> 00:56:18,533
lf l suffer, you'll suffer,
just remember that

233
00:56:28,366 --> 00:56:30,010
Where to?

234
00:58:58,763 --> 00:59:00,655
What can we do?

235
00:59:02,342 --> 00:59:07,783
We haven't moved the sand;
we've left it for two nights

236
00:59:08,833 --> 00:59:11,352
What can we do about water?

237
00:59:12,704 --> 00:59:17,676
Why don't you help a little?
l was kind enough to untie you

238
00:59:19,571 --> 00:59:24,542
Well, then, we must work

239
00:59:25,562 --> 00:59:26,916
Stop joking

240
00:59:27,394 --> 00:59:30,566
Who's got the right to force us
into a deal like that?

241
00:59:35,508 --> 00:59:38,578
There's such a thing as the
right man for the right job

242
00:59:42,707 --> 00:59:46,506
l'm a teacher and something of
a scholar

243
00:59:48,077 --> 00:59:51,583
Surely they can think of a
better way to put me to use

244
00:59:55,360 --> 00:59:57,950
For example, we could...

245
00:59:59,354 --> 01:00:02,642
...think of some way to exploit
the sand's appeal

246
01:00:04,723 --> 01:00:08,415
After all, that's what brought me
all the way here

247
01:00:10,965 --> 01:00:12,713
l'm serious

248
01:00:14,460 --> 01:00:19,318
That's probably why that postcard
salesman came, too

249
01:00:21,577 --> 01:00:25,593
You advertise it and develop it...

250
01:00:28,152 --> 01:00:30,961
...as a new tourist area

251
01:00:32,855 --> 01:00:38,628
But for a tourist area you need
a hot spring

252
01:00:40,885 --> 01:00:45,307
Well, that was just an idea, of course

253
01:00:49,667 --> 01:00:55,253
The sand might be good for
some kind of crop

254
01:00:56,326 --> 01:01:00,340
The Young People's Association
grows a few things

255
01:01:01,277 --> 01:01:04,305
Things like peanuts and tulips

256
01:01:05,606 --> 01:01:07,717
They said the tulip bulbs...

257
01:01:10,224 --> 01:01:14,604
...were so big they showed them at
an agricultural fair in Tokyo

258
01:01:15,677 --> 01:01:16,882
That's enough!

259
01:01:21,212 --> 01:01:25,049
A monkey could learn such work

260
01:01:26,580 --> 01:01:29,316
A monkey? Could it?

261
01:02:00,912 --> 01:02:02,659
lt's bad for you

262
01:02:18,432 --> 01:02:20,325
My breath's on fire

263
01:02:42,818 --> 01:02:44,097
lt's useless

264
01:02:47,063 --> 01:02:48,489
This sand

265
01:02:52,514 --> 01:02:56,167
lt could swallow up a city,
a country even

266
01:03:02,377 --> 01:03:03,979
Did you know that?

267
01:03:04,375 --> 01:03:09,566
A Roman town called Sabrata...
and a town in The Rubiyat...

268
01:03:11,823 --> 01:03:17,016
...were buried by tiny particles
one-eighth of a millimetre across

269
01:03:21,187 --> 01:03:25,026
You can't fight it. It's hopeless

270
01:03:34,212 --> 01:03:35,888
Don't!

271
01:03:41,163 --> 01:03:43,274
l'll make a ladder

272
01:05:32,191 --> 01:05:34,783
Shall l brush off the sand?

273
01:05:37,769 --> 01:05:43,834
But the women in Tokyo,
aren't they prettier?

274
01:05:46,008 --> 01:05:47,288
Ridiculous!

275
01:05:51,377 --> 01:05:52,583
l'll do it

276
01:11:27,106 --> 01:11:28,926
Goddam it!

277
01:11:32,557 --> 01:11:34,378
My blood will rot

278
01:12:59,866 --> 01:13:02,457
What a dirty trick

279
01:13:12,184 --> 01:13:17,959
What must l do? Please do something!
My blood will rot!

280
01:13:19,758 --> 01:13:23,919
Well, if we begin to work...

281
01:13:26,084 --> 01:13:31,973
All right, l'm beaten.
l'll let them win!

282
01:15:25,728 --> 01:15:30,731
Guest, there's water.
They're sending water

283
01:16:38,762 --> 01:16:42,487
Guest, it's easier if you hold
the shovel lower down

284
01:16:43,174 --> 01:16:46,097
Use your left hand; keep a balance

285
01:16:49,457 --> 01:16:51,100
l'm tired

286
01:17:02,191 --> 01:17:05,771
Rest, you're not used to this yet

287
01:17:06,519 --> 01:17:12,148
All my muscles are aching.
l'm exhausted

288
01:17:13,178 --> 01:17:15,914
You can't help it, with new work

289
01:17:16,507 --> 01:17:20,304
Will l get used to it?

290
01:17:21,418 --> 01:17:23,092
Oh, yes

291
01:17:25,329 --> 01:17:31,031
l don't understand. Don't you
find this pointless?

292
01:17:32,695 --> 01:17:36,753
Are you living to clear sand,
or clearing sand to live?

293
01:17:38,314 --> 01:17:42,151
Of course, this place isn't as
interesting as Tokyo...

294
01:17:42,849 --> 01:17:45,876
l'm not talking about Tokyo

295
01:17:51,755 --> 01:17:55,812
How can you stand being shut in
like this?

296
01:17:57,414 --> 01:17:59,932
But it's my home

297
01:18:04,323 --> 01:18:06,622
Then assert your rights

298
01:18:12,396 --> 01:18:15,902
l'm just going to wait for help

299
01:18:17,432 --> 01:18:19,034
Help?

300
01:18:20,636 --> 01:18:25,536
Of course. Someone will go to my room
before long

301
01:18:26,962 --> 01:18:31,602
They'll find a book with
the pages left open

302
01:18:32,413 --> 01:18:35,191
Money in my suit pockets

303
01:18:40,236 --> 01:18:42,827
They'll become suspicious

304
01:18:43,441 --> 01:18:46,176
But l suppose this is your life

305
01:18:47,852 --> 01:18:51,690
But, you see, the bones
are buried here, too

306
01:18:54,551 --> 01:18:58,204
My husband and daughter

307
01:18:59,670 --> 01:19:01,419
We'll find them

308
01:19:02,084 --> 01:19:05,809
Then you can leave. l'll help you

309
01:19:06,869 --> 01:19:10,812
You know how to make them
drop the ladder down

310
01:19:12,071 --> 01:19:15,464
What is there special for me
to do outside?

311
01:19:16,151 --> 01:19:17,940
You can walk

312
01:19:18,397 --> 01:19:19,896
Walk?

313
01:19:20,312 --> 01:19:24,109
That's right. It's wonderful
to walk where you like

314
01:19:26,513 --> 01:19:30,018
lsn't it tiring to walk
without doing anything?

315
01:19:31,382 --> 01:19:35,438
Shut up! Even a chained dog
goes mad

316
01:19:36,958 --> 01:19:38,674
Are you human?

317
01:19:49,443 --> 01:19:53,823
lf there were no sand
no one would bother about me

318
01:19:56,974 --> 01:20:00,480
That's true. For you, too

319
01:21:07,720 --> 01:21:11,153
-Look, here's one
-Stop it!

320
01:21:12,797 --> 01:21:15,794
This is what l came out here
Iooking for

321
01:21:17,500 --> 01:21:19,726
A kind of tiger-beetle

322
01:21:29,817 --> 01:21:32,190
-Got any scissors?
-Scissors?

323
01:21:33,356 --> 01:21:35,873
l just want to cut my whiskers

324
01:21:38,057 --> 01:21:40,430
l have some somewhere

325
01:21:57,326 --> 01:21:59,042
They're not very sharp

326
01:21:59,448 --> 01:22:03,870
-They're pretty rusty
-The sand does it

327
01:22:52,881 --> 01:22:56,533
They don't work.
They won't close properly

328
01:23:01,913 --> 01:23:05,199
We'll ask for a razor when
they bring the ration

329
01:23:07,613 --> 01:23:10,828
See? The sand in my beard
makes it all scratchy

330
01:23:14,023 --> 01:23:17,673
That sand just makes a mess
of everything

331
01:23:34,040 --> 01:23:38,419
-Go ahead!
-Thank you

332
01:23:40,572 --> 01:23:44,806
-What's your man doing?
-He's got stomach-ache

333
01:23:45,567 --> 01:23:47,866
Have you been at it too much?

334
01:25:20,615 --> 01:25:25,547
-A bath?
-No, l'm very tired today

335
01:25:29,521 --> 01:25:33,578
-l'd like one
-Not when you're ill

336
01:25:35,347 --> 01:25:41,381
l'm very sweaty. l just can't
get to sleep at all

337
01:25:43,754 --> 01:25:45,719
l'll boil water, then

338
01:25:46,167 --> 01:25:49,308
-l'll help
-No, l'll do it

339
01:26:25,118 --> 01:26:28,480
-What are you thinking about?
-Nothing much

340
01:26:32,318 --> 01:26:35,054
Let's take a photograph of you

341
01:26:35,605 --> 01:26:37,353
l've never...

342
01:26:40,807 --> 01:26:42,409
Don't fuss

343
01:26:58,244 --> 01:27:00,980
-You want to go home
-Why?

344
01:27:03,863 --> 01:27:06,525
l think you'd like a radio

345
01:27:10,645 --> 01:27:14,370
-You could hear about the world
-Nonsense!

346
01:27:16,180 --> 01:27:19,904
But you could hear about everything,
about Tokyo

347
01:27:21,215 --> 01:27:25,014
l only collected insects because of
useless things

348
01:27:27,209 --> 01:27:32,765
One's name in an encyclopaedia, that's
worth more than everyday stupidities

349
01:27:35,241 --> 01:27:40,432
But a radio would take your mind
off things

350
01:27:45,644 --> 01:27:49,919
l know, let's drink the sake

351
01:27:50,930 --> 01:27:53,155
But you're ill

352
01:27:53,634 --> 01:27:56,516
-To help sleep
-l'm sleepy enough without it

353
01:28:00,293 --> 01:28:02,300
Oh, come on

354
01:28:09,823 --> 01:28:12,122
Really, l can't

355
01:28:19,186 --> 01:28:21,631
Come on, it's all right

356
01:28:23,015 --> 01:28:25,824
-lt's boiling...
-First a drink

357
01:28:26,676 --> 01:28:28,497
You're a terrible man

358
01:28:32,461 --> 01:28:34,208
Right down

359
01:30:12,004 --> 01:30:14,043
That's better

360
01:30:17,706 --> 01:30:21,763
You are cruel, when l'm so tired

361
01:41:37,363 --> 01:41:42,773
Help! Someone help me!

362
01:42:02,332 --> 01:42:04,371
Grab this

363
01:42:10,490 --> 01:42:13,735
l'm sorry. Please pull me out

364
01:42:14,443 --> 01:42:18,021
No, it's not like pulling up a carrot

365
01:42:20,019 --> 01:42:23,671
We'll save you. Just hold that board

366
01:42:26,928 --> 01:42:29,227
lt's a good thing we came along

367
01:42:30,215 --> 01:42:35,406
This is ''Salty Bean Paste''.
Even dogs won't come near it

368
01:42:36,831 --> 01:42:40,151
l wonder how many have fallen in

369
01:42:43,198 --> 01:42:45,279
Sometimes strangers hike here

370
01:42:45,738 --> 01:42:49,576
We might dig up lots of
valuable things

371
01:42:51,064 --> 01:42:54,019
Two or three cameras, at least

372
01:44:05,514 --> 01:44:09,020
-Be quiet
-l'm sorry

373
01:44:18,415 --> 01:44:20,307
Shall l make some tea?

374
01:44:21,826 --> 01:44:23,178
What's the time?

375
01:44:23,824 --> 01:44:27,767
l don't know... about 8.30...

376
01:44:34,728 --> 01:44:36,183
l failed

377
01:44:36,808 --> 01:44:38,118
Yes

378
01:44:43,217 --> 01:44:45,995
Complete and utter failure

379
01:44:49,459 --> 01:44:51,062
Have you...

380
01:44:53,371 --> 01:44:55,046
...a wife?

381
01:44:57,782 --> 01:44:59,238
ln Tokyo

382
01:45:00,737 --> 01:45:02,484
That's none of your business

383
01:45:08,977 --> 01:45:11,276
But it's too bad

384
01:45:13,928 --> 01:45:19,005
lf l'd succeeded, l was going to buy
a radio and send it to you

385
01:45:20,837 --> 01:45:23,834
-A radio?
-You wanted one...

386
01:45:27,787 --> 01:45:31,583
No. You don't have to do that

387
01:45:33,239 --> 01:45:36,453
l can do piecework and buy one here

388
01:45:39,231 --> 01:45:41,196
l failed

389
01:45:43,393 --> 01:45:45,692
Shall l dry you?

390
01:45:49,384 --> 01:45:51,902
l should have known the geography

391
01:46:03,617 --> 01:46:05,957
But they'll be looking for me

392
01:46:07,654 --> 01:46:11,929
There will be my friends,
the co-operative...

393
01:46:14,604 --> 01:46:17,745
...the Board of Education
and the PTA

394
01:46:20,471 --> 01:46:22,261
They won't leave me to my fate

395
01:46:24,757 --> 01:46:27,098
l'll wash you

396
01:46:48,519 --> 01:46:49,466
What's that?

397
01:46:54,678 --> 01:46:58,768
Hope. A crow trap

398
01:47:05,499 --> 01:47:07,799
l put bait here...

399
01:47:08,787 --> 01:47:12,843
...and then the crow gets sucked down
with the sand

400
01:47:13,906 --> 01:47:16,715
But crows are crafty

401
01:47:17,733 --> 01:47:19,846
Not when they're hungry

402
01:47:26,390 --> 01:47:29,459
But why is it hope?

403
01:47:30,926 --> 01:47:32,423
Hope because...

404
01:47:34,047 --> 01:47:38,281
l'll tie a message to the crow's leg,
asking for help

405
01:47:41,412 --> 01:47:45,792
lt hasn't come yet. It's almost
three months now

406
01:47:48,404 --> 01:47:51,619
-What hasn't?
-Help

407
01:48:01,680 --> 01:48:04,779
Maybe they thought you ran away

408
01:48:06,091 --> 01:48:07,546
Ran away?

409
01:48:10,543 --> 01:48:13,393
That you hated your job

410
01:48:15,120 --> 01:48:16,505
lmpossible

411
01:48:19,782 --> 01:48:25,816
l left my bank book. l left
a book open on my desk

412
01:48:27,107 --> 01:48:30,830
My whole room is shouting for help

413
01:48:32,349 --> 01:48:33,879
What's there?

414
01:48:35,138 --> 01:48:38,093
-Where?
-ln Tokyo

415
01:48:39,466 --> 01:48:41,693
Why not go and see?

416
01:48:43,461 --> 01:48:44,990
lt's too much

417
01:48:56,319 --> 01:48:58,213
Here's the ration

418
01:49:03,312 --> 01:49:06,192
Wait a moment

419
01:49:12,008 --> 01:49:18,552
Will you let me out a little,
just half an hour a day?

420
01:49:22,996 --> 01:49:25,992
l want to see the sea.
l won't run away

421
01:49:26,991 --> 01:49:29,030
Twenty minutes... ten minutes...

422
01:49:29,529 --> 01:49:34,211
l've been good. It's three months
now... l'll go mad

423
01:49:36,604 --> 01:49:39,891
l'll discuss it with the village
council

424
01:49:40,765 --> 01:49:43,688
l don't mind if there's a guard

425
01:51:39,119 --> 01:51:40,721
You're diligent

426
01:51:41,449 --> 01:51:44,371
l'll soon earn 1,000 yen

427
01:52:16,739 --> 01:52:17,976
That's enough

428
01:52:27,142 --> 01:52:30,357
l don't want to die in a ditch

429
01:53:02,765 --> 01:53:06,052
What's a radio worth in this place?

430
01:53:07,884 --> 01:53:11,940
They're not your friends.
It's everyone for himself

431
01:53:17,330 --> 01:53:19,775
You're being exploited

432
01:53:20,991 --> 01:53:24,717
You wag your tail, but just you wait

433
01:53:26,027 --> 01:53:29,200
We'll be deserted, left to ourselves

434
01:53:29,815 --> 01:53:32,593
No, because of the sand

435
01:53:33,143 --> 01:53:38,440
Sand? What use is it?
Nothing but trouble

436
01:53:39,718 --> 01:53:43,151
We sell it. To builders

437
01:53:44,130 --> 01:53:47,271
lt's against the law to use
such salt sand

438
01:53:48,209 --> 01:53:51,278
lt's a secret. We sell it half price

439
01:53:51,870 --> 01:53:55,523
But the buildings would soon collapse

440
01:53:58,279 --> 01:54:00,318
That's not my business

441
01:54:03,397 --> 01:54:07,559
Well, that's not your business.
Yes, that's true

442
01:54:09,391 --> 01:54:15,091
But somebody must profit by it.
Why help them?

443
01:54:16,465 --> 01:54:18,472
lt's a co-operative

444
01:54:19,753 --> 01:54:23,258
People like me are treated very well

445
01:54:37,563 --> 01:54:39,531
l'll sieve them

446
01:54:43,432 --> 01:54:49,392
lt's like building a house on water,
ignoring the existence of boats

447
01:54:50,964 --> 01:54:55,239
You can't get the idea of a house
out of your head

448
01:54:57,040 --> 01:55:00,692
Haven't you been wanting to go home,
too?

449
01:55:26,087 --> 01:55:27,834
Even if it's false...

450
01:55:29,500 --> 01:55:31,986
...as long as there's hope...

451
01:55:33,119 --> 01:55:36,626
...something different may happen
tomorrow

452
01:55:40,236 --> 01:55:41,838
l'm always afraid

453
01:55:45,106 --> 01:55:47,842
ln the morning, when l go to sleep...

454
01:55:48,475 --> 01:55:53,189
...l'm afraid of being alone again
when l wake up

455
01:55:55,468 --> 01:55:58,973
l'm afraid, really afraid...

456
01:56:26,263 --> 01:56:28,780
Here, put them in this

457
01:57:03,009 --> 01:57:06,005
What now? The basket already?

458
01:57:07,171 --> 01:57:10,167
l hear you want to see the sea

459
01:57:12,372 --> 01:57:15,950
Will you let me?

460
01:57:18,615 --> 01:57:21,641
lt's not out of the question

461
01:57:22,359 --> 01:57:28,133
l beg you. Just half an hour a day,
l won't escape

462
01:57:30,349 --> 01:57:35,426
Then let us see you doing ''it''.
The two of you

463
01:57:37,175 --> 01:57:41,263
Outdoors, where we can all watch you

464
01:57:43,083 --> 01:57:44,291
''lt''?

465
01:57:49,326 --> 01:57:52,280
What men and women do together. ''it''

466
01:58:24,866 --> 01:58:30,566
We all discussed your request.
We all agreed what you must do

467
01:59:12,556 --> 01:59:14,522
What now?

468
01:59:15,760 --> 01:59:18,133
lt's ridiculous

469
01:59:19,382 --> 01:59:22,118
But it's a chance, at least

470
01:59:23,750 --> 01:59:26,268
lgnore them

471
01:59:29,285 --> 01:59:33,562
We could, that's one way, but
we needn't take it seriously

472
01:59:34,571 --> 01:59:37,162
What? Are you a sex maniac?

473
02:01:46,117 --> 02:01:48,634
You live like a sow, anyway

474
02:03:02,438 --> 02:03:05,466
Pretending will do

475
02:05:46,901 --> 02:05:48,430
But how?

476
02:05:54,434 --> 02:05:55,818
Funny...

477
02:06:05,627 --> 02:06:09,758
There hasn't been a drop of rain
for three weeks

478
02:06:45,162 --> 02:06:47,275
Capillary action

479
02:06:48,990 --> 02:06:50,490
That's it

480
02:07:04,347 --> 02:07:07,665
Maybe l can develop a water supply

481
02:08:59,371 --> 02:09:04,084
Surface evaporation probably
draws up water

482
02:09:05,947 --> 02:09:08,391
The sand is a pump

483
02:09:10,108 --> 02:09:13,105
l'm living on a suction pump

484
02:09:16,724 --> 02:09:20,449
Perhaps I can make a really
ecient reservoir

485
02:10:19,313 --> 02:10:22,268
lt's awful. l'm choking

486
02:10:23,018 --> 02:10:25,171
lt's nearly December

487
02:11:23,650 --> 02:11:25,325
Still trying?

488
02:11:26,938 --> 02:11:30,662
l'll catch a crow easily
when food is scarce

489
02:11:32,474 --> 02:11:35,209
Really, men are...

490
02:11:38,299 --> 02:11:41,805
l want to be the trapper for once

491
02:11:48,246 --> 02:11:52,667
We can hear the weather forecast
when we get the radio

492
02:12:18,333 --> 02:12:19,717
l'm afraid

493
02:12:42,011 --> 02:12:44,196
l'll go and bury this

494
02:13:29,079 --> 02:13:30,900
Until tomorrow

495
02:13:50,220 --> 02:13:52,737
ls anyone there?

496
02:14:09,654 --> 02:14:12,650
l don't want to

497
02:14:20,556 --> 02:14:21,868
-What's wrong?
-A pain

498
02:14:44,070 --> 02:14:45,525
A child?

499
02:14:52,309 --> 02:14:55,524
-Since when?
-Since October

500
02:15:00,382 --> 02:15:02,973
lt hurts? Do you feel sick?

501
02:15:07,832 --> 02:15:11,961
That's it, it's probably a miscarriage

502
02:15:12,866 --> 02:15:14,219
You know?

503
02:15:15,114 --> 02:15:18,621
He used to shoe horses for a vet

504
02:15:19,650 --> 02:15:21,398
Get a doctor

505
02:15:23,354 --> 02:15:25,319
He'll be too late

506
02:15:26,141 --> 02:15:27,962
Doctors today...

507
02:15:28,389 --> 02:15:31,125
Go and get the van

508
02:16:09,920 --> 02:16:13,719
l'm afraid. Has the wind dropped?

509
02:16:16,080 --> 02:16:18,670
Almost died down

510
02:16:28,398 --> 02:16:31,353
-What now?
-Wrap her up

511
02:16:52,326 --> 02:16:53,680
A radio

512
02:17:00,733 --> 02:17:02,305
A radio

513
02:17:05,227 --> 02:17:06,829
Only now...

514
02:17:07,850 --> 02:17:09,598
Want to hear it?

515
02:17:21,375 --> 02:17:22,946
Want it?

516
02:17:23,331 --> 02:17:25,483
Tuck it in the bedding

517
02:17:33,317 --> 02:17:35,065
Have you got another board?

518
02:17:36,897 --> 02:17:39,456
Take one from the bench outside

519
02:17:54,459 --> 02:17:56,539
The one underneath

520
02:18:35,907 --> 02:18:37,801
What did you say?

521
02:18:49,306 --> 02:18:52,772
Nothing... Iater

522
02:19:12,321 --> 02:19:13,996
l don't want to...

523
02:24:52,274 --> 02:24:55,489
l needn't run off yet

524
02:24:57,892 --> 02:25:02,678
My return ticket's open. I can
use it whenever i like

525
02:25:08,213 --> 02:25:13,476
Anyway, there's my reservoir
experiment, it's very exciting

526
02:25:16,203 --> 02:25:19,708
The villagers must be the first
to know of it

527
02:25:22,112 --> 02:25:24,849
l'll tell them today or tomorrow

528
02:25:25,816 --> 02:25:28,261
As for escaping...

529
02:25:31,476 --> 02:25:35,127
...l'll think about that afterwards

530
02:25:44,127 --> 02:25:46,353
Judgement : Court of Family Affairs

531
02:25:46,914 --> 02:25:51,482
Concerning the disappearance of
Junpei Niki, born 7th March, 1926

532
02:25:52,283 --> 02:25:59,264
The Court acknowledges his absence
for seven years, since August, 1956

533
02:26:00,439 --> 02:26:06,214
The verdict of the Court
is as follows.

534
02:26:07,265 --> 02:26:10,770
Junpei Niki is adjudged to be
a missing person

535
02:26:11,426 --> 02:26:16,065
Certined 5th October, 1963.
Judge of Domestic Relations