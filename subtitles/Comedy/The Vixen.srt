1
00:00:08,290 --> 00:00:10,550
___

2
00:00:11,450 --> 00:00:13,120
Just give me the necklace!

3
00:00:40,630 --> 00:00:43,430
Mari. Mari?

4
00:00:44,640 --> 00:00:45,990
You're okay?

5
00:00:50,960 --> 00:00:52,560
You sure you want a coffee?

6
00:00:52,790 --> 00:00:55,350
After what just happened,
I could use something a little stiffer.

7
00:00:55,820 --> 00:00:57,290
Coffee's good.

8
00:00:58,020 --> 00:00:59,520
Thanks, Chuck.

9
00:01:01,210 --> 00:01:02,880
I'm beat.

10
00:01:02,970 --> 00:01:06,980
If you want to stay and I hope you will,
your room's how you left it.

11
00:01:26,660 --> 00:01:31,120
The girls at school...
They said my mommy was a junkie.

12
00:01:31,930 --> 00:01:33,930
That's why she gave me up.

13
00:01:34,940 --> 00:01:39,050
- What's a junkie? - It doesn't
matter, because it's not true.

14
00:01:39,530 --> 00:01:41,620
Those girls,
they were just being nasty.

15
00:01:43,190 --> 00:01:45,900
Then why did my
mommy give me up?

16
00:01:46,530 --> 00:01:49,870
You were just a baby, when the
foster agency placed you with us.

17
00:01:50,510 --> 00:01:54,580
You didn't even have a name.
But you did have something.

18
00:01:56,090 --> 00:01:59,090
Something we've been waiting to
give you until you were old enough.

19
00:02:09,100 --> 00:02:11,940
- What is it?
- It belonged to your mother.

20
00:02:11,990 --> 00:02:15,710
Chuck asked a friend of his about it
and he said it was an "Anansi Totem".

21
00:02:15,740 --> 00:02:19,610
- What's that? - I don't know,
sweetie. But I have a feeling,

22
00:02:19,640 --> 00:02:23,940
that one day it might help you
find the answers to all your questions.

23
00:02:37,940 --> 00:02:41,620
<i>- Dr. Macalester.</i> - Uh, office
hours were over 20 minutes ago.

24
00:02:42,380 --> 00:02:44,580
I'm Mari McCabe.
I called earlier.

25
00:02:45,100 --> 00:02:47,910
Of course, my apologies.
Come in.

26
00:02:48,550 --> 00:02:50,910
You said you had an artifact
for me to look at.

27
00:02:51,060 --> 00:02:55,150
Yes. Though...
I'm not sure it's an artifact.

28
00:02:58,450 --> 00:03:02,410
<i>Normally, this is where I'd say,
"My eyes are up here".</i>

29
00:03:03,210 --> 00:03:07,370
This is extraordinary.
Where did you find this?

30
00:03:07,620 --> 00:03:09,810
I guess you could say
it's a family heirloom.

31
00:03:09,840 --> 00:03:12,950
I don't really know anything about it.
I... I don't even know where it's from.

32
00:03:13,250 --> 00:03:17,200
That type of stone is indigenous
to the M'Changa province, in Zambesi.

33
00:03:17,230 --> 00:03:20,710
- And it has someting to do
with "Anansi", right? - Anansi.

34
00:03:20,740 --> 00:03:24,250
The trickster God. According to legend,
he fashioned a totem, which he gave...

35
00:03:24,280 --> 00:03:26,380
To a warrior named Tantu.

36
00:03:27,180 --> 00:03:30,070
When I was a kid I looked up
Anansi in the encyclopedia.

37
00:03:30,100 --> 00:03:31,610
I'm sorry to waste your time.

38
00:03:31,640 --> 00:03:35,140
I honestly don't know what I was
expecting to learn when I came in here.

39
00:03:35,170 --> 00:03:36,670
What did you want to learn?

40
00:03:38,110 --> 00:03:43,250
- I don't know. I... It's stupid.
- Oh, I have three PhDs so...

41
00:03:43,340 --> 00:03:45,660
How about you let me be
the judge of what's stupid.

42
00:03:46,900 --> 00:03:49,100
I've had this thing
since I was 10

43
00:03:49,520 --> 00:03:52,000
and I always thought it was
just a piece of junk jewelry.

44
00:03:52,030 --> 00:03:55,410
But lately, some things
have happened to me.

45
00:03:55,540 --> 00:03:58,820
And now, I think it might be the key
to finding out more about my family.

46
00:03:58,850 --> 00:04:03,250
- Who's your family? - That would
be the 52 million dollar question.

47
00:04:03,650 --> 00:04:05,550
Sorry to waste your time.

48
00:04:05,750 --> 00:04:10,130
It's said the totem grants it's wearer
access to the "Ashe" of the animal kingdom.

49
00:04:10,160 --> 00:04:15,350
It means life force,
power, essence.

50
00:04:15,660 --> 00:04:19,370
The totem harnesses the Ashe of
any animal the wearer conjures up.

51
00:04:19,570 --> 00:04:22,750
- What?
- You know, speed of a cheetah,

52
00:04:23,010 --> 00:04:26,910
agility of an antelope,
strength of an elephant, etc.

53
00:04:28,790 --> 00:04:31,620
Relax. All of this
is just a myth.

54
00:04:31,750 --> 00:04:36,480
- In reality, that pendant is just a finely
polished piece of stone. - Too bad.

55
00:04:37,060 --> 00:04:39,920
If it was magic, I might be
able to get something for it online.

56
00:04:40,120 --> 00:04:41,840
Thanks for your time.

57
00:04:47,990 --> 00:04:51,510
<i>You know the McCabe woman?</i> The one who
tuned up the three gang-bangers you hired

58
00:04:51,540 --> 00:04:54,910
to steal the Anansi totem off her.
She just walked out of my office.

59
00:04:55,110 --> 00:04:58,480
That certainly is a fortunate
turn of events.

60
00:04:58,510 --> 00:05:01,390
<i>I can get her back here anytime
you want. But in return...</i>

61
00:05:01,600 --> 00:05:03,670
Relax, Dr. Macalester.

62
00:05:03,700 --> 00:05:08,990
Once I have the totem in hand, you can
consider your next expedition fully founded.

63
00:05:09,120 --> 00:05:11,590
And the girl?
What'll happen to her?

64
00:05:12,190 --> 00:05:14,940
Don't worry about
Mari McCabe, Professor.

65
00:05:14,970 --> 00:05:18,270
I need her alive.
For now.

66
00:05:19,800 --> 00:05:22,800
Made by vbalazs91

