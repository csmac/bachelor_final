﻿1
00:00:00,000 --> 00:00:02,048
<b>1x20 - Doppleganger</b>

2
00:00:02,467 --> 00:00:03,917
<i>♪ Beware ♪</i>

3
00:00:06,642 --> 00:00:07,993
<i>♪ Beware ♪</i>

4
00:00:10,563 --> 00:00:12,152
<i>♪ Beware ♪</i>

5
00:00:12,796 --> 00:00:14,783
<i>♪ Beware the bat ♪</i>

6
00:00:17,294 --> 00:00:19,785
<i>Katana: Previously on
Beware the Batman...</i>

7
00:00:19,983 --> 00:00:22,861
Hi, Major.
It's time for you to come back.

8
00:00:22,863 --> 00:00:24,142
[Grunting]

9
00:00:24,270 --> 00:00:25,902
Batman needs you.

10
00:00:26,254 --> 00:00:27,726
Bruce needs you.

11
00:00:27,918 --> 00:00:28,947
I need you.

12
00:00:29,422 --> 00:00:31,164
It appears you've missed me.

13
00:00:31,984 --> 00:00:35,558
<i>Sync and corrections by masaca
- www.addic7ed.com -</i>

14
00:00:37,624 --> 00:00:39,574
[Grunting]

15
00:00:43,255 --> 00:00:45,367
I'm so sorry, my dear.

16
00:00:45,369 --> 00:00:47,735
That's no way to treat a lady.

17
00:00:48,088 --> 00:00:49,063
This is!

18
00:00:51,384 --> 00:00:51,869
[Phone ringing]

19
00:00:51,903 --> 00:00:52,856
<i>Barbara: Are you okay?</i>

20
00:00:53,004 --> 00:00:54,343
I'm a little busy, Barbara.

21
00:00:54,368 --> 00:00:55,170
[Gunshots]

22
00:00:55,865 --> 00:00:56,719
But no.

23
00:01:04,119 --> 00:01:07,062
Um, that's quite a butter knife.

24
00:01:07,064 --> 00:01:09,975
What do you think
the Bat-prentice has in mind?

25
00:01:10,103 --> 00:01:12,183
I think she's going to knight us.

26
00:01:12,185 --> 00:01:14,775
Oh, then we shall bow.

27
00:01:19,160 --> 00:01:20,567
[Gunshots]

28
00:01:20,568 --> 00:01:21,943
[Gas hissing]

29
00:01:28,791 --> 00:01:31,703
Oh, dear, she's not as good as the Bat.

30
00:01:31,705 --> 00:01:32,822
I concur.

31
00:01:32,824 --> 00:01:35,575
But it's nice to have
a woman around the house.

32
00:01:35,576 --> 00:01:36,535
[Laughing]

33
00:01:37,847 --> 00:01:39,862
[Coughing]

34
00:01:41,719 --> 00:01:42,730
<i>Barbara: What happened?</i>

35
00:01:43,543 --> 00:01:44,660
They got away.

36
00:01:45,081 --> 00:01:48,919
- Barbara: Should I call Batman?
- No, he's been a little... preoccupied.

37
00:01:51,256 --> 00:01:52,694
[Grunting]

38
00:02:00,215 --> 00:02:01,911
[Growling]

39
00:02:04,696 --> 00:02:06,742
[Screeching]

40
00:02:06,872 --> 00:02:07,986
[Gasps]

41
00:02:17,976 --> 00:02:19,862
Perhaps it's time to take a break.

42
00:02:20,119 --> 00:02:23,383
Get out of the cave. Get
some sunlight, see friends.

43
00:02:23,544 --> 00:02:25,309
I only have one real friend.

44
00:02:26,168 --> 00:02:28,233
And even you chose to walk away from me.

45
00:02:28,919 --> 00:02:30,711
You are not the reason I left.

46
00:02:30,713 --> 00:02:32,342
Are we gonna talk about that?

47
00:02:32,344 --> 00:02:34,230
You disappeared for almost eight months.

48
00:02:34,232 --> 00:02:36,687
But I came back when you
needed me, as promised.

49
00:02:37,367 --> 00:02:40,182
I will tell you where I was,
what I was doing, in time.

50
00:02:40,343 --> 00:02:42,679
But right now we should
be more concerned with you.

51
00:02:43,127 --> 00:02:44,093
And Batman.

52
00:02:44,632 --> 00:02:45,975
I'm afraid, Alfred.

53
00:02:46,455 --> 00:02:48,918
Afraid of what might happen
if I put the suit back on.

54
00:02:49,687 --> 00:02:52,579
Then don't. Let Katana
look after Gotham.

55
00:02:52,728 --> 00:02:54,836
And let Bruce Wayne enjoy his life.

56
00:02:55,128 --> 00:02:57,882
I'm sure there's someone who'd like
to pal around with a billionaire.

57
00:03:01,688 --> 00:03:03,255
Harvey: The people want a strong leader.

58
00:03:03,257 --> 00:03:05,719
Mayor Grange let this city get soft.

59
00:03:05,720 --> 00:03:08,055
Time for a new Mayor to toughen Gotham up.

60
00:03:08,057 --> 00:03:08,887
Batman!

61
00:03:09,336 --> 00:03:09,976
What?

62
00:03:10,001 --> 00:03:12,449
The key to me winning
this election is Batman.

63
00:03:12,536 --> 00:03:13,751
Once I'm Mayor, I don't care

64
00:03:13,752 --> 00:03:15,735
what vigilante menace
wants to run around Gotham

65
00:03:15,737 --> 00:03:17,047
in a Halloween costume.

66
00:03:17,049 --> 00:03:18,582
But this is an election year.

67
00:03:18,743 --> 00:03:20,402
So, what exactly do you want from me?

68
00:03:21,688 --> 00:03:23,350
Grange has been on leave for a month.

69
00:03:23,735 --> 00:03:26,903
The writing's there. I mean,
with the right push he's done.

70
00:03:27,864 --> 00:03:29,719
It's the perfect time
to go negative with my

71
00:03:29,720 --> 00:03:31,543
campaign ads, but it's expensive.

72
00:03:31,959 --> 00:03:33,623
What I need is money.

73
00:03:33,816 --> 00:03:35,702
Can I count on Bruce Wayne's deep pockets?

74
00:03:35,927 --> 00:03:38,263
- I don't know, Harvey.
- You don't know!

75
00:03:38,265 --> 00:03:40,087
Why do you think I brought
the great Dane Lisslow,

76
00:03:40,089 --> 00:03:41,399
head of my Special Crimes Unit?

77
00:03:41,752 --> 00:03:43,319
It's not great, just Dane.

78
00:03:43,479 --> 00:03:46,230
Because if you don't write
a check, he'll arrest you.

79
00:03:47,703 --> 00:03:48,738
I'm kidding.

80
00:03:48,763 --> 00:03:49,867
[Laughs]

81
00:03:53,176 --> 00:03:54,774
You've studied Goju Ryu.

82
00:03:54,967 --> 00:03:57,311
- How can you tell?
- The scars on your hands.

83
00:03:58,007 --> 00:03:59,798
You spend a lot of time training.

84
00:03:59,992 --> 00:04:01,494
I've broken a lot of fingers.

85
00:04:01,783 --> 00:04:03,062
Few of them my own.

86
00:04:03,064 --> 00:04:03,570
[Giggles]

87
00:04:03,595 --> 00:04:05,590
I don't know how many
times I sprained a finger

88
00:04:05,592 --> 00:04:07,382
trying to get the governor on the phone.

89
00:04:09,399 --> 00:04:10,903
Have you ever studied Savate?

90
00:04:10,904 --> 00:04:12,438
Only for all of college.

91
00:04:14,679 --> 00:04:16,150
[Grunting]

92
00:04:25,912 --> 00:04:26,776
Ow!

93
00:04:27,159 --> 00:04:28,295
Why are we doing this again?

94
00:04:28,664 --> 00:04:31,542
If you're going to help, even if
it's mostly with your computer,

95
00:04:31,735 --> 00:04:33,462
you should know how to defend yourself.

96
00:04:33,719 --> 00:04:35,446
I know how to defend myself.

97
00:04:35,768 --> 00:04:36,897
[Grunts]

98
00:04:38,135 --> 00:04:41,334
After the Blackgate incident and all
the trouble with the Iron Cortex,

99
00:04:41,336 --> 00:04:44,151
Batman wants me to train you.
Consider it payback.

100
00:04:44,631 --> 00:04:47,094
Cool, now can you let go of my neck?

101
00:04:48,823 --> 00:04:51,638
Oh, I was thinking about
giving myself a code name.

102
00:04:51,799 --> 00:04:53,399
Something mysterious like...

103
00:04:53,912 --> 00:04:54,808
Oracle.

104
00:04:58,743 --> 00:05:01,718
I broke this in Israel
in a Krav Maga tournament.

105
00:05:02,071 --> 00:05:03,414
Good one.
Jeet kune do.

106
00:05:03,512 --> 00:05:05,271
Broke my clavicle in three places.

107
00:05:05,272 --> 00:05:06,902
Got my medal, then passed out.

108
00:05:08,087 --> 00:05:09,719
What is that smell?

109
00:05:10,359 --> 00:05:12,918
It's Parijat bark.
What do you think?

110
00:05:13,143 --> 00:05:14,553
Ah, it's terrible.

111
00:05:14,776 --> 00:05:16,038
That's how you know it's good.

112
00:05:16,063 --> 00:05:17,062
[Giggles]

113
00:05:17,815 --> 00:05:20,118
If you guys are done drinking your mulch,

114
00:05:20,120 --> 00:05:21,556
can we get back to me?

115
00:05:21,849 --> 00:05:24,727
Okay, Harvey, send me your
proposal, and I'll give it some...

116
00:05:30,263 --> 00:05:32,822
Ah, excuse me, I need to leave.

117
00:05:32,824 --> 00:05:34,486
- Is something wrong?
- No, I...

118
00:05:34,680 --> 00:05:37,463
I got some bad bark.
Dinner's on me.

119
00:05:39,255 --> 00:05:42,359
Um, what's your most expensive desert?

120
00:05:44,184 --> 00:05:44,870
[Woman gasps]

121
00:06:07,287 --> 00:06:09,430
[Screeching]

122
00:06:12,833 --> 00:06:13,984
[Grunts]

123
00:06:16,225 --> 00:06:18,304
[Growling]

124
00:06:29,825 --> 00:06:32,480
I know what I saw.
It was a bat.

125
00:06:32,482 --> 00:06:34,091
A human size Man Bat.

126
00:06:34,530 --> 00:06:36,416
Something clearly attacked him, Alfred.

127
00:06:36,897 --> 00:06:38,086
You saw his clothes.

128
00:06:38,274 --> 00:06:41,761
Maybe what you thought you saw
was just a mild hallucination.

129
00:06:41,763 --> 00:06:43,232
I had the same thought until I checked out

130
00:06:43,234 --> 00:06:45,645
all the Gotham crime
reports from previous nights.

131
00:06:45,794 --> 00:06:48,543
There have been several break-ins
at chemical supply companies in

132
00:06:48,544 --> 00:06:51,292
the last month, including one very
close to where I encountered...

133
00:06:51,841 --> 00:06:52,650
what I saw.

134
00:06:52,683 --> 00:06:55,078
Pyg and Toad were breaking
into a chemical factory.

135
00:06:55,489 --> 00:06:56,881
It can't be a coincidence.

136
00:06:58,082 --> 00:06:58,664
Bruce...

137
00:06:58,689 --> 00:07:01,016
Batman needs to get
back out there, Alfred.

138
00:07:01,281 --> 00:07:03,168
Considering your current condition,

139
00:07:03,170 --> 00:07:05,280
- are you sure that's wise?
- No.

140
00:07:05,282 --> 00:07:07,360
But when has Batman
ever listened to reason?

141
00:07:11,553 --> 00:07:14,560
According to the reports, the only
thing stolen after each break-in

142
00:07:14,562 --> 00:07:17,184
had a direct relationship
to DNA sequencing.

143
00:07:17,186 --> 00:07:19,264
Specifically animal DNA.

144
00:07:19,266 --> 00:07:20,153
<i>Katana: Makes sense.</i>

145
00:07:20,154 --> 00:07:22,816
<i>It fits with Pyg and
Toad's pro-animal pathology.</i>

146
00:07:23,489 --> 00:07:26,720
<i>Batman: Tapper chemicals is a leading
distributer of sequenced animal DNA.</i>

147
00:07:27,233 --> 00:07:29,952
<i>There's a 68 percent chance
this is the next target.</i>

148
00:07:42,242 --> 00:07:44,096
[Groans]

149
00:07:49,985 --> 00:07:50,987
Not this time.

150
00:07:56,353 --> 00:07:58,240
Wow, he wasn't kidding.

151
00:07:58,625 --> 00:07:59,712
It is a Man Bat.

152
00:08:29,441 --> 00:08:31,008
[Groans]

153
00:08:34,433 --> 00:08:35,744
[Screams]

154
00:08:56,065 --> 00:08:57,824
- Katana: Are you okay?
- Batman: I'm fine.

155
00:08:57,953 --> 00:09:00,896
Get him, it, into the Batmobile.

156
00:09:06,177 --> 00:09:08,196
Is that what I think it is?

157
00:09:08,450 --> 00:09:11,624
<i>Professor Pyg: Batman,
with a Bat Man.</i>

158
00:09:12,321 --> 00:09:14,816
How pathetically redundant.

159
00:09:14,818 --> 00:09:18,272
Sorry, Batman, 
but we'd like our pet back.

160
00:09:18,274 --> 00:09:22,272
Mister Toad, would you be
so kind as to call him?

161
00:09:24,449 --> 00:09:27,776
<i>Mister Toad: Come here, boy.
Who's the good Man Bat?</i>

162
00:09:31,328 --> 00:09:33,888
Okay, freaks, everyone on the ground.

163
00:09:34,017 --> 00:09:36,992
In these clothes, I think not.

164
00:09:37,313 --> 00:09:39,353
Mister Toad, an exit.

165
00:09:46,753 --> 00:09:48,416
[Engine revving]

166
00:09:57,281 --> 00:09:59,456
[Gunshots]

167
00:10:09,441 --> 00:10:12,160
On closer inspection,
these capsules appear to serve

168
00:10:12,162 --> 00:10:14,485
as a hypodermic for the chemical inside.

169
00:10:15,553 --> 00:10:16,910
[Groaning]

170
00:10:17,122 --> 00:10:18,023
Where am I?

171
00:10:19,169 --> 00:10:20,272
He can talk.

172
00:10:20,865 --> 00:10:22,432
Of course I can talk.

173
00:10:22,434 --> 00:10:24,544
Now that you got those things off my neck.

174
00:10:24,801 --> 00:10:26,816
Why were you working with Pyg and Toad?

175
00:10:26,818 --> 00:10:29,024
- What are they planning?
- Working with?

176
00:10:29,441 --> 00:10:31,371
No, you've got it all wrong.

177
00:10:31,618 --> 00:10:33,184
I wasn't working with them.

178
00:10:33,313 --> 00:10:34,702
I was their first victim.

179
00:10:39,873 --> 00:10:41,979
<i>Man Bat: My name is
Dr. Kirk Langstrom.</i>

180
00:10:42,434 --> 00:10:45,570
<i>I was a research scientist trying
to break down the DNA of bats.</i>

181
00:10:45,889 --> 00:10:48,064
<i>I was hoping to develop
a life-saving</i>

182
00:10:48,066 --> 00:10:49,813
<i>serum from their immune system,</i>

183
00:10:50,082 --> 00:10:52,008
<i>which is very resistant to disease.</i>

184
00:10:52,865 --> 00:10:55,469
<i>But others had a
different plan for my work.</i>

185
00:11:04,641 --> 00:11:07,528
<i>Pyg and Toad stole
my research and perverted it,</i>

186
00:11:10,561 --> 00:11:12,912
<i>forcing me to test the serum on myself.</i>

187
00:11:17,697 --> 00:11:20,000
[Growling]

188
00:11:20,545 --> 00:11:22,335
[Glass shattering]

189
00:11:23,457 --> 00:11:25,568
<i>Turning me into what you see before you.</i>

190
00:11:25,985 --> 00:11:28,944
<i>Not an animal, and no longer a man either.</i>

191
00:11:32,033 --> 00:11:35,200
Professor Pyg implanted
those capsules into my neck,

192
00:11:35,202 --> 00:11:37,982
which allowed them to take
control of my mind and body.

193
00:11:38,305 --> 00:11:40,913
<i>Alfred: I'm not surprised.
According to the Batcomputer,</i>

194
00:11:40,914 --> 00:11:43,522
the substance in these
capsules is Scopolamine.

195
00:11:43,746 --> 00:11:45,280
The devil's breath.

196
00:11:45,281 --> 00:11:48,544
A toxin made from Nightshade that
creates a loss of mental control.

197
00:11:48,642 --> 00:11:51,512
The victim becomes mentally bonded
to the first voice it hears.

198
00:11:51,649 --> 00:11:53,004
Sounds like a zombie.

199
00:11:53,442 --> 00:11:54,407
Exactly.

200
00:11:54,658 --> 00:11:58,176
They forced me to rob chemical
depots to create more of my serum,

201
00:11:58,273 --> 00:12:01,920
substituting bat DNA
for the DNA of other animals.

202
00:12:02,208 --> 00:12:04,288
They want to make an animal army

203
00:12:04,290 --> 00:12:06,729
to take back Gotham for all animal kind.

204
00:12:07,073 --> 00:12:08,442
Can you lead us to them?

205
00:12:09,058 --> 00:12:11,456
Yes, but Pyg and Toad got the last

206
00:12:11,457 --> 00:12:13,920
of the DNA they needed
at Tapper Chemicals.

207
00:12:14,113 --> 00:12:16,064
We might already be too late.

208
00:12:23,585 --> 00:12:24,228
[Gasps]

209
00:12:29,281 --> 00:12:32,096
Let the experiments commence.

210
00:12:37,079 --> 00:12:40,268
<i>Professor Pyg: Now, I hope
you're not allergic to cat hair. </i>

211
00:12:40,696 --> 00:12:43,508
Because you're going to be
covered in it soon.

212
00:12:43,800 --> 00:12:45,777
And one of you lucky girls

213
00:12:46,008 --> 00:12:49,575
is going to have the honor
of becoming Mrs. Toad.

214
00:12:49,975 --> 00:12:51,670
Now, Mister Toad,

215
00:12:51,894 --> 00:12:55,318
this is the most important
decision of your life.

216
00:12:55,927 --> 00:12:57,814
Chose with your heart.

217
00:12:57,975 --> 00:12:59,958
Absolutely.

218
00:13:02,327 --> 00:13:02,875
[Panting]

219
00:13:02,900 --> 00:13:05,559
Eenie, meenie, miney...

220
00:13:09,207 --> 00:13:10,422
...mo.

221
00:13:13,847 --> 00:13:15,081
Would you do me the honor

222
00:13:15,447 --> 00:13:19,510
of making me the happiest
man-phibian on Earth?

223
00:13:20,438 --> 00:13:22,230
[Screams]

224
00:13:22,232 --> 00:13:24,437
That's frightened for "yes."

225
00:13:24,631 --> 00:13:25,802
[Sighs]

226
00:13:30,359 --> 00:13:31,957
<i>Langstrom: This is where they held me.</i>

227
00:13:31,959 --> 00:13:34,870
If they have hostages,
that's where they'll be.

228
00:13:37,174 --> 00:13:38,103
<i>Katana: Are we too late?</i>

229
00:13:38,128 --> 00:13:38,861
<i>[Woman screaming]</i>

230
00:13:40,215 --> 00:13:40,734
No.

231
00:13:41,399 --> 00:13:43,670
[Whimpering]

232
00:13:56,982 --> 00:14:00,984
Professor Pyg: That's rude.
How do you expect us to create a race

233
00:14:01,009 --> 00:14:05,276
of human-animal soldiers
if you insist on interrupting?

234
00:14:07,415 --> 00:14:09,813
Why don't we settle this like gentlemen?

235
00:14:09,815 --> 00:14:12,347
That would be dreadfully dull.

236
00:14:20,566 --> 00:14:21,474
[Gasps]

237
00:14:21,499 --> 00:14:22,592
Ssh!

238
00:14:27,063 --> 00:14:27,877
[Whimpers]

239
00:14:27,902 --> 00:14:28,877
Follow me.

240
00:14:36,407 --> 00:14:41,206
Our army is deserting. Mister
Toad, get them back in formation!

241
00:14:44,118 --> 00:14:45,462
[Grunting]

242
00:14:51,638 --> 00:14:53,494
Now, where were we?

243
00:15:07,958 --> 00:15:09,196
[Sirens] It's the police.

244
00:15:10,135 --> 00:15:12,246
That's Gotham PD, not the SCU.

245
00:15:12,470 --> 00:15:13,853
Stay in the shadows.

246
00:15:22,199 --> 00:15:25,334
Put your weapons down! I've got
people here who need help.

247
00:15:25,336 --> 00:15:28,854
We got a call saying Pyg and Toad
were holding hostages at the old zoo.

248
00:15:29,303 --> 00:15:30,889
A call? From who?

249
00:15:31,415 --> 00:15:33,537
Someone calling themselves "Oracle".

250
00:15:36,214 --> 00:15:37,717
[Sirens blaring]

251
00:15:41,878 --> 00:15:43,541
[Grunting]

252
00:15:43,703 --> 00:15:45,861
You are spoiling my wedding day.

253
00:15:54,487 --> 00:15:58,326
Now, be a good bat
and fetch my future bride.

254
00:16:09,495 --> 00:16:11,190
[Grunting]

255
00:16:13,175 --> 00:16:15,162
That wasn't sporting.

256
00:16:16,886 --> 00:16:19,413
Now, let me show you something

257
00:16:19,415 --> 00:16:21,750
I didn't learn in medical school.

258
00:16:26,998 --> 00:16:31,261
Okay, I will give you one last chance

259
00:16:31,832 --> 00:16:33,142
to surrender.

260
00:16:37,623 --> 00:16:38,882
Stay down.

261
00:16:43,926 --> 00:16:47,126
Oh, he's ruined these trousers.

262
00:16:48,343 --> 00:16:49,387
Sweetums.

263
00:16:50,390 --> 00:16:51,529
We are home.

264
00:17:25,910 --> 00:17:27,427
[Growling]

265
00:17:28,503 --> 00:17:29,837
Langstrom, fight it!

266
00:17:38,323 --> 00:17:41,793
<i>Professor Pyg: Do you,
Mister Toad, take this soon</i>

267
00:17:41,794 --> 00:17:45,263
<i>to be non-human to be your
unlawfully wedded wife?</i>

268
00:17:45,879 --> 00:17:46,890
I do.

269
00:17:47,671 --> 00:17:50,165
And do you,
soon to be non-human,

270
00:17:50,167 --> 00:17:54,005
take Mister Toad to be your
unlawfully wedded husband?

271
00:17:54,806 --> 00:17:56,950
[Groaning softly]

272
00:17:56,952 --> 00:17:58,838
That means I do.
Keep going.

273
00:17:58,840 --> 00:18:00,694
If there is anyone who thinks

274
00:18:00,696 --> 00:18:03,670
these two should not be united, speak now

275
00:18:03,862 --> 00:18:05,569
or forever hold your...

276
00:18:12,375 --> 00:18:13,431
The weddings off.

277
00:18:13,975 --> 00:18:15,751
Sit down and behave.

278
00:18:16,919 --> 00:18:18,678
- Professor Pyg: Yes, Sir.
- Mister Toad: Yes, Sir.

279
00:18:27,702 --> 00:18:28,757
[Sirens]

280
00:18:31,254 --> 00:18:33,430
Can't believe I almost married that guy.

281
00:18:33,814 --> 00:18:36,679
Ew. And that he
licked my neck.

282
00:18:36,824 --> 00:18:38,972
It's safe to come out now,
Dr. Langstrom.

283
00:18:41,367 --> 00:18:43,759
I don't know how I
could ever repay you both.

284
00:18:44,023 --> 00:18:46,377
Continue your research, find a cure.

285
00:18:46,680 --> 00:18:48,885
How, when I look like this?

286
00:18:49,367 --> 00:18:51,448
<i>Batman: You're still
Kirk Langstrom inside.</i>

287
00:18:52,310 --> 00:18:53,680
That hasn't changed.

288
00:18:54,039 --> 00:18:56,405
As long as you
remember who you really are,

289
00:18:56,407 --> 00:18:58,550
there's nothing to fear from the Man Bat.

290
00:19:01,206 --> 00:19:04,193
If I need your help how
will I find you again?

291
00:19:04,599 --> 00:19:06,701
You won't, we'll find you.

292
00:19:25,014 --> 00:19:26,057
[Beeping]

293
00:19:26,648 --> 00:19:27,989
Are you two done?

294
00:19:27,991 --> 00:19:30,517
There's only so much macho I can take.

295
00:19:33,686 --> 00:19:37,333
Thank you, Alfred. To Harvey
Dent, Gotham's next Mayor.

296
00:19:37,686 --> 00:19:39,286
What?
You're backing me?

297
00:19:40,247 --> 00:19:41,645
This should answer your question.

298
00:19:43,894 --> 00:19:44,744
Wow!

299
00:19:45,079 --> 00:19:46,517
Well, I am a billionaire.

300
00:19:47,863 --> 00:19:49,813
Dane, take a look at this.

301
00:19:50,774 --> 00:19:52,981
<i>Dane: Yup, those are a lot of zeroes.</i>

302
00:19:54,806 --> 00:19:56,306
Excuse me, gentlemen.

303
00:19:57,430 --> 00:20:00,150
Mayor? When I told
you to make friends,

304
00:20:00,152 --> 00:20:02,032
I didn't mean Harvey Dent

305
00:20:02,033 --> 00:20:04,281
- and his "Pitbull".
- I like Dane.

306
00:20:04,282 --> 00:20:07,261
His technique is a little
sloppy, but he can keep up.

307
00:20:07,928 --> 00:20:09,494
He kind of reminds me of...

308
00:20:10,199 --> 00:20:11,829
- ...me.
- Wonderful.

309
00:20:12,054 --> 00:20:15,158
Two Bruce Waynes.
Just what the world needs.

310
00:20:15,767 --> 00:20:17,560
That still doesn't explain Dent.

311
00:20:17,943 --> 00:20:20,629
You know what they say,
keep your friends close...

312
00:20:21,046 --> 00:20:22,595
but your enemies closer.

313
00:20:26,680 --> 00:20:31,000
<i>Sync and corrections by masaca
- www.addic7ed.com -</i>

