﻿1
00:00:08,409 --> 00:00:09,341
Hi, sweetie.

2
00:00:09,343 --> 00:00:11,643
Um, where are you guys?

3
00:00:11,645 --> 00:00:13,645
Call me when you get this.

4
00:00:21,489 --> 00:00:22,421
Hi, it's me again.

5
00:00:22,423 --> 00:00:25,090
Your cell's going straight
to voicemail.

6
00:00:25,092 --> 00:00:27,126
Hope everything's okay.

7
00:00:27,128 --> 00:00:28,527
Please call.

8
00:00:37,438 --> 00:00:38,470
Sara.

9
00:00:38,472 --> 00:00:39,838
A- are you there?

10
00:00:39,840 --> 00:00:42,541
Please, pick up, sweetie.

11
00:00:42,543 --> 00:00:46,545
Sara.

12
00:02:50,971 --> 00:02:51,870
Hello!

13
00:02:51,872 --> 00:02:52,871
- Sara...
- Frank...

14
00:02:52,873 --> 00:02:54,506
And Adam
aren't here at the moment.

15
00:02:54,508 --> 00:02:57,442
But please leave your name
and number after the beep.

16
00:02:58,679 --> 00:02:59,444
Hi, sweetie.

17
00:02:59,446 --> 00:03:00,846
Hope your date night went well.

18
00:03:00,848 --> 00:03:01,847
We had a fun night here.

19
00:03:01,849 --> 00:03:03,849
Adam loves watching
all the cars

20
00:03:03,851 --> 00:03:05,317
passing by the condo.

21
00:03:05,319 --> 00:03:06,919
Listen, I called
my doctor friend,

22
00:03:06,921 --> 00:03:10,556
and she's not sure if Adam's
getting enough protein.

23
00:03:10,558 --> 00:03:11,924
He looks very pale.

24
00:03:11,926 --> 00:03:13,559
And hardly any baby fat.

25
00:03:13,561 --> 00:03:15,694
Oh, by the way,
I tried calling your cell,

26
00:03:15,696 --> 00:03:17,829
but I kept getting
a busy signal.

27
00:03:17,831 --> 00:03:19,631
It's probably bad reception.

28
00:03:19,633 --> 00:03:21,833
Oh, sweetie, we really miss you.

29
00:03:21,835 --> 00:03:22,868
I hope you can stay for lunch

30
00:03:22,870 --> 00:03:25,370
when you come to pick up Adam
tomorrow morning.

31
00:03:25,372 --> 00:03:29,241
I know your father will want
to spend time with his daughter.

32
00:03:29,243 --> 00:03:31,410
Anyway, give me a call
when you get home

33
00:03:31,412 --> 00:03:32,477
if it's not too late, okay?

34
00:03:32,479 --> 00:03:36,615
Love you.
Can't wait to see you, honey.

35
00:06:42,668 --> 00:06:46,138
Here's my two favorite people
in the whole, wide world.

36
00:06:46,140 --> 00:06:49,007
- Look, Adam, it's daddy.
- Say hi to daddy!

37
00:06:49,009 --> 00:06:52,144
Adam, baby,
can you say "da-da"?

38
00:06:52,146 --> 00:06:53,812
Da-da?

39
00:06:55,082 --> 00:06:57,582
And here's my gorgeous wife.
Aw.

40
00:06:57,584 --> 00:07:00,986
Those beautiful eyes
I wake up to every sing...

41
00:10:47,947 --> 00:10:50,382
Hello!

42
00:10:50,384 --> 00:10:51,383
- Sara...
- Frank...

43
00:10:51,385 --> 00:10:53,051
And Adam
aren't here at the moment.

44
00:10:53,053 --> 00:10:55,987
But please leave your name
and number after the beep.

45
00:10:57,190 --> 00:10:58,223
Sara Mcandrews,

46
00:10:58,225 --> 00:10:59,257
this is deputy Sanders calling

47
00:10:59,259 --> 00:11:01,292
from the county sherriff's
office.

48
00:11:01,294 --> 00:11:02,160
I got your message.

49
00:11:02,162 --> 00:11:03,228
My apologies for calling
so late.

50
00:11:03,230 --> 00:11:07,165
It's been one of those days,
and we're severely understaffed.

51
00:11:07,167 --> 00:11:08,366
So regarding this trespasser

52
00:11:08,368 --> 00:11:10,068
you saw on your property
this morning,

53
00:11:10,070 --> 00:11:12,003
rest assured,
you have nothing to worry about.

54
00:11:12,005 --> 00:11:14,472
It's probably just a drifter
passing through.

55
00:11:14,474 --> 00:11:16,107
In any case,
I'll drop by tomorrow,

56
00:11:16,109 --> 00:11:18,376
make sure everything's
all right, and...

57
00:11:18,378 --> 00:11:20,745
Well, I'll have one
of those delicious brownies

58
00:11:20,747 --> 00:11:22,881
that everyone's
been talking about.

59
00:11:22,883 --> 00:11:24,949
We're here for your safety,
okay?

60
00:11:24,951 --> 00:11:27,852
Hope you folks have
a great night.

61
00:18:20,332 --> 00:18:21,299
- Hey, honey.
- What?

62
00:18:21,301 --> 00:18:22,300
You know, next time we go out,

63
00:18:22,302 --> 00:18:24,235
would you mind just leaving
one light on?

64
00:18:24,237 --> 00:18:25,736
No, it's supposed to look
like we're home.

65
00:18:25,738 --> 00:18:28,372
Yeah, but, like, we didn't
move in to a lighthouse.

66
00:18:28,374 --> 00:18:30,708
- Yeah, you're funny.
- Such a jokester.

67
00:18:30,710 --> 00:18:31,509
- Am I?
- Yeah.

68
00:18:31,511 --> 00:18:33,211
I'm not as funny
as whatever you want to call

69
00:18:33,213 --> 00:18:34,745
that dance thing thing
you made me sit through.

70
00:18:34,747 --> 00:18:36,314
What was that?
It was awful.

71
00:18:36,316 --> 00:18:37,148
Oh, come on.

72
00:18:37,150 --> 00:18:38,916
It wasn't that bad.

73
00:18:38,918 --> 00:18:40,585
- It was horrible.
- What?

74
00:18:40,587 --> 00:18:42,386
Are you always this difficult?

75
00:18:42,388 --> 00:18:46,157
No, I thought it was really
beautiful, and...

76
00:18:46,159 --> 00:18:48,326
- You are a philistine,
you know that?

77
00:18:48,328 --> 00:18:51,863
It involves a lot more skill
than one of your football games.

78
00:18:51,865 --> 00:18:54,799
As in, a gaggle
of hairless men

79
00:18:54,801 --> 00:18:57,702
running around,
rolling on top of one another,

80
00:18:57,704 --> 00:18:59,670
on a large steel table...

81
00:18:59,672 --> 00:19:00,671
Yeah, exactly.

82
00:19:00,673 --> 00:19:01,706
Naked, hairless men?

83
00:19:01,708 --> 00:19:04,208
No, men taken over
by the spirit of dance.

84
00:19:04,210 --> 00:19:06,911
- And they weren't all hairless.
- Ugh, please.

85
00:19:06,913 --> 00:19:09,947
Now I can't eat
in the cafeteria anymore.

86
00:19:09,949 --> 00:19:12,250
Why?
Because they just installed

87
00:19:12,252 --> 00:19:13,885
the exact same kind of table.

88
00:19:13,887 --> 00:19:14,652
Oh, no.

89
00:19:14,654 --> 00:19:15,586
I'm gonna be obsessing

90
00:19:15,588 --> 00:19:16,954
over every little stain
on the thing.

91
00:19:16,956 --> 00:19:18,656
Well, maybe
in between meetings,

92
00:19:18,658 --> 00:19:20,424
you and Tony can try it out.

93
00:19:21,261 --> 00:19:24,295
You two would make
a terrific duo.

94
00:19:25,264 --> 00:19:27,431
Oh, God.

95
00:19:27,433 --> 00:19:29,433
I'd definitely pay
to see that.

96
00:19:29,435 --> 00:19:30,902
You guys would probably draw
quite the crowd

97
00:19:30,904 --> 00:19:33,804
if we didn't live
in the middle of nowhere.

98
00:19:33,806 --> 00:19:35,973
Sweetheart, you know...

99
00:19:35,975 --> 00:19:37,875
Our son's gonna thank you
for it later.

100
00:19:37,877 --> 00:19:40,845
He'll tell you
in his deep, manly voice...

101
00:19:40,847 --> 00:19:43,781
He'll say, "thanks, ma.

102
00:19:43,783 --> 00:19:46,450
"Thanks for following pa
to the countryside.

103
00:19:46,452 --> 00:19:48,719
"I'm a lucky boy, you know?

104
00:19:48,721 --> 00:19:49,954
"I can see that now.

105
00:19:49,956 --> 00:19:52,223
I learned
what it was like to"...

106
00:19:52,225 --> 00:19:53,824
Live like a peasant.

107
00:19:56,296 --> 00:19:57,762
"To breath the fresh air,

108
00:19:57,764 --> 00:20:00,431
"you know, to ride my bike
in the forest,

109
00:20:00,433 --> 00:20:01,966
"to be able to practice
my football skills

110
00:20:01,968 --> 00:20:04,335
"in my huge backyard
without being bothered

111
00:20:04,337 --> 00:20:09,941
by the riffraff from your old
disgusting neighborhood. "

112
00:20:12,811 --> 00:20:16,247
- Mm.
- Hey.

113
00:20:16,249 --> 00:20:18,482
Nothing beats a real home.

114
00:20:18,484 --> 00:20:21,719
Now, what do you say to that,
city girl?

115
00:20:21,721 --> 00:20:23,487
- Mm.
- Hmm?

116
00:20:23,489 --> 00:20:27,858
I say that I need

117
00:20:27,860 --> 00:20:30,328
a glass of milk.

118
00:20:32,030 --> 00:20:33,631
All right.

119
00:20:34,733 --> 00:20:37,468
Whoops.

120
00:20:55,887 --> 00:20:59,824
- Honey, could you please check
the messages?

121
00:21:06,666 --> 00:21:08,933
Two new messages.
Hi, sweetie.

122
00:21:08,935 --> 00:21:10,534
Hope your date night went well.

123
00:21:10,536 --> 00:21:11,535
We had a fun night.

124
00:21:11,537 --> 00:21:13,537
Oh, Adam loves watching
all the cars

125
00:21:13,539 --> 00:21:14,772
passing by the condo.

126
00:21:14,774 --> 00:21:16,774
Listen, I called
my doctor friend...

127
00:21:16,776 --> 00:21:17,508
Sure you did.

128
00:21:17,510 --> 00:21:20,611
And she's not sure if Adam's
getting enough protein.

129
00:21:20,613 --> 00:21:22,046
He looks very pale.

130
00:21:22,048 --> 00:21:23,581
And hardly any baby fat.

131
00:21:23,583 --> 00:21:25,650
Oh, by the way,
I tried calling your cell,

132
00:21:25,652 --> 00:21:27,718
but I kept getting
a busy signal.

133
00:21:27,720 --> 00:21:29,787
It's probably bad reception.

134
00:21:29,789 --> 00:21:31,722
Oh, sweetie, we really miss you.

135
00:21:31,724 --> 00:21:32,757
I hope you can stay for lunch

136
00:21:32,759 --> 00:21:34,859
when you come to pick up Adam
tomorrow morning.

137
00:21:34,861 --> 00:21:38,729
I know your father will want
to spend time with his daughter.

138
00:21:38,731 --> 00:21:40,898
Anyway, give me a call
when you get home

139
00:21:40,900 --> 00:21:42,967
if it's not too late, okay?
Critters.

140
00:21:42,969 --> 00:21:45,569
- Love you.
Can't wait to see you, honey.

141
00:21:46,506 --> 00:21:48,506
What am I, chopped liver?

142
00:21:48,508 --> 00:21:49,540
Sara Mcandrews,

143
00:21:49,542 --> 00:21:50,574
this is deputy Sanders calling

144
00:21:50,576 --> 00:21:52,610
from the county sherriff's
office.

145
00:21:52,612 --> 00:21:53,477
I got your message.

146
00:21:53,479 --> 00:21:55,780
My apologies for calling
so late...

147
00:21:57,950 --> 00:21:59,583
You know...

148
00:22:00,585 --> 00:22:02,987
S- she can't help herself,
can she, right?

149
00:22:02,989 --> 00:22:04,622
She has to find something
to worry about.

150
00:22:04,624 --> 00:22:06,791
No, she is the one
that needs to see a doctor,

151
00:22:06,793 --> 00:22:08,926
'cause she always wants us
to feel guilty

152
00:22:08,928 --> 00:22:10,528
or worried about something.

153
00:22:10,530 --> 00:22:11,362
You know, but I...

154
00:22:11,364 --> 00:22:13,064
I know exactly
what she's trying to do.

155
00:22:13,066 --> 00:22:14,598
She can't stand
that her little angel

156
00:22:14,600 --> 00:22:17,501
is now independent
and so far away from her.

157
00:22:17,503 --> 00:22:19,537
You know, t-tomorrow...

158
00:22:22,107 --> 00:22:23,040
Mm.

159
00:22:23,042 --> 00:22:26,644
Tomorrow, tomorrow, tomorrow.

160
00:22:26,646 --> 00:22:31,482
Tonight is just you and me...
- Mm-hmm.

161
00:22:31,484 --> 00:22:32,383
Alone.

162
00:22:32,385 --> 00:22:34,719
So why don't you stop talking?

163
00:22:34,721 --> 00:22:37,154
- Mmm.
- Mmm.

164
00:22:57,677 --> 00:22:59,744
Honey.
What?

165
00:22:59,746 --> 00:23:02,046
I have a surprise for you.

166
00:23:02,048 --> 00:23:03,881
What's her name?

167
00:23:04,516 --> 00:23:06,150
Seriously?

168
00:23:07,652 --> 00:23:09,019
What's her name?

169
00:23:09,021 --> 00:23:11,222
Why don't you go serve me
something to drink

170
00:23:11,224 --> 00:23:12,957
instead of being such a perv.

171
00:23:14,660 --> 00:23:15,559
What are you doing?

172
00:23:15,561 --> 00:23:17,495
- I just want to make sure
he's okay.

173
00:23:17,497 --> 00:23:19,130
She said he was fine, baby.

174
00:23:19,132 --> 00:23:21,565
Don't worry so much.

175
00:23:31,743 --> 00:23:33,511
I'm your baby tonight.

176
00:23:34,480 --> 00:23:37,681
Baby, I really have
to have a shower.

177
00:23:37,683 --> 00:23:40,985
So why don't you just stay here

178
00:23:40,987 --> 00:23:44,522
and maintain your concentration?

179
00:23:48,760 --> 00:23:51,629
Can I spy on you
in the shower?

180
00:23:51,631 --> 00:23:53,998
Don't even think about it.

181
00:24:57,596 --> 00:24:59,930
<i>I know she will</i>

182
00:25:00,832 --> 00:25:03,000
<i>I'm sure she will</i>

183
00:25:11,878 --> 00:25:15,012
<i>I know she will</i>

184
00:25:15,014 --> 00:25:18,616
<i>Oh, yes she will</i>

185
00:25:18,618 --> 00:25:21,218
<i>I'm sure she will</i>

186
00:25:25,992 --> 00:25:27,658
Critters?

187
00:25:27,660 --> 00:25:28,659
<i>When I put my arms</i>

188
00:25:28,661 --> 00:25:30,895
kitty.

189
00:25:31,964 --> 00:25:34,865
<i>Arms around her</i>

190
00:25:34,867 --> 00:25:38,302
<i>You hear her holler</i>

191
00:25:38,304 --> 00:25:41,805
<i>You hear her moan</i>

192
00:25:43,109 --> 00:25:45,309
<i>I know she will</i>

193
00:25:45,311 --> 00:25:48,178
<i>whoa, yes she will</i>

194
00:25:48,180 --> 00:25:49,680
critters.

195
00:25:49,682 --> 00:25:53,183
Critters, come here.
Ooh.

196
00:25:53,185 --> 00:25:55,386
Good little baby.

197
00:25:57,390 --> 00:25:59,590
Are you pouting?

198
00:25:59,858 --> 00:26:03,160
<i>I'm sure she will</i>

199
00:26:03,162 --> 00:26:06,063
<i>Let me come back home</i>

200
00:26:10,636 --> 00:26:14,138
<i>I know she will</i>

201
00:26:14,140 --> 00:26:17,741
<i>I'm sure she will</i>

202
00:26:17,743 --> 00:26:22,212
<i>I know she will</i>

203
00:26:22,214 --> 00:26:23,280
Frank.

204
00:26:24,350 --> 00:26:25,616
Hello?

205
00:26:25,618 --> 00:26:27,151
- Hi, mom.
- Did I wake you?

206
00:26:27,153 --> 00:26:29,887
- Oh, no, honey, hi.
- I'm up.

207
00:26:29,889 --> 00:26:31,422
So how's Adam?

208
00:26:31,424 --> 00:26:32,990
Sleeping like a baby.

209
00:26:32,992 --> 00:26:36,393
<i>Home late</i>

210
00:26:36,395 --> 00:26:37,361
<i>oh, I just may</i>

211
00:26:37,363 --> 00:26:38,295
I don't understand

212
00:26:38,297 --> 00:26:41,031
why we're always having
this conversation.

213
00:26:41,033 --> 00:26:43,067
Did you call the police?

214
00:26:43,069 --> 00:26:44,101
Yes.

215
00:26:44,103 --> 00:26:47,304
I left a message for the
sherriff. Left a message?

216
00:26:47,306 --> 00:26:50,007
What kind of godforsaken place
did you move to?

217
00:26:50,009 --> 00:26:51,342
Listen, make sure
the doors are locked.

218
00:26:51,344 --> 00:26:54,912
- Yep, they are locked,
and all of the lights are on.

219
00:26:54,914 --> 00:26:57,014
- And how about that husband
of yours?

220
00:26:57,016 --> 00:26:58,816
Is he doing anything
about it?

221
00:26:58,818 --> 00:26:59,817
What is that supposed to mean?

222
00:26:59,819 --> 00:27:01,285
Well, he's your husband,
isn't he?

223
00:27:01,287 --> 00:27:05,022
- Look, mom, he is doing
the best that he can.

224
00:27:08,260 --> 00:27:10,427
You know what?
It's probably nothing.

225
00:27:10,429 --> 00:27:13,397
I'm not even sure
that I saw what I saw.

226
00:27:13,399 --> 00:27:15,933
Honey, if you don't feel safe
over there,

227
00:27:15,935 --> 00:27:17,267
you'll always have a place here.

228
00:27:17,269 --> 00:27:21,038
You can stay with us as long
as you like, okay?

229
00:27:21,040 --> 00:27:22,106
Okay.

230
00:27:22,108 --> 00:27:23,207
I've been really tired lately,

231
00:27:23,209 --> 00:27:25,809
so I'm gonna go and sleep
for 100 years.

232
00:27:25,811 --> 00:27:27,878
Oh, you do that,
sleeping beauty.

233
00:27:27,880 --> 00:27:29,680
I'll see you tomorrow morning.

234
00:27:29,682 --> 00:27:30,848
Mm-hmm.

235
00:27:30,850 --> 00:27:32,049
Give Adam a kiss for me.

236
00:27:32,051 --> 00:27:34,151
- Don't worry.
I will.

237
00:27:34,153 --> 00:27:36,220
Sweet dreams, darling.

238
00:27:36,222 --> 00:27:37,755
Love you.

239
00:27:37,757 --> 00:27:39,023
Night.

240
00:27:39,025 --> 00:27:42,393
<i>I know she will</i>

241
00:27:42,395 --> 00:27:45,929
<i>Oh, yes she will</i>

242
00:27:45,931 --> 00:27:48,298
<i>Let me come back home</i>

243
00:27:49,367 --> 00:27:52,102
- glad to know
I can still surprise you.

244
00:27:52,104 --> 00:27:54,772
Well, I didn't hear you.

245
00:27:54,774 --> 00:27:57,808
<i>A votre service, madame.
Oh.</i>

246
00:27:57,810 --> 00:28:00,110
Wow, I didn't realize
the waiters around here

247
00:28:00,112 --> 00:28:02,312
had such casual uniforms.

248
00:28:02,314 --> 00:28:03,147
Well, you know,

249
00:28:03,149 --> 00:28:06,417
it helps me maintain
my concentration.

250
00:28:06,419 --> 00:28:08,886
Mm.

251
00:28:08,888 --> 00:28:10,487
Oh, and by the way,

252
00:28:10,489 --> 00:28:11,288
is it possible

253
00:28:11,290 --> 00:28:15,125
that my surprise is waiting
in the bedroom?

254
00:28:15,127 --> 00:28:16,894
You little shit.

255
00:28:16,896 --> 00:28:19,363
You're gonna totally ruin
your surprise.

256
00:28:19,365 --> 00:28:21,231
- Come on.
- Come here.

257
00:28:21,233 --> 00:28:21,965
No.

258
00:28:21,967 --> 00:28:24,435
- Oh, just give me one kiss.
- No.

259
00:28:24,437 --> 00:28:25,869
- I deserve it.
- Mm-mm.

260
00:28:25,871 --> 00:28:28,038
Get lost. - Let
me come in. - Go.

261
00:28:28,040 --> 00:28:29,073
- Oh, I get it.
- I get it.

262
00:28:29,075 --> 00:28:31,475
You need to practice, uh,
your choreography, right?

263
00:28:31,477 --> 00:28:35,913
Okay, just remember that it's
f- r-a-n-k.

264
00:28:35,915 --> 00:28:37,147
That's really funny, babe.

265
00:28:37,149 --> 00:28:38,882
- I'm gonna go out
in the living room

266
00:28:38,884 --> 00:28:40,184
and rearrange the furniture,
okay?

267
00:28:40,186 --> 00:28:42,820
You know, I'm gonna move things
around.

268
00:28:42,822 --> 00:28:45,055
I'll see you soon, kitten.

269
00:28:47,025 --> 00:28:50,127
<i>Everywhere I go</i>

270
00:28:58,237 --> 00:29:01,839
<i>Yeah, everywhere I go</i>

271
00:29:02,942 --> 00:29:06,343
<i>I see that same old 158</i>

272
00:29:11,884 --> 00:29:15,052
<i>I know I'm billed out, baby</i>

273
00:29:15,054 --> 00:29:16,019
<i>Whoo, lord</i>

274
00:29:16,021 --> 00:29:19,056
<i>Uncle Sam say,
"bill, don't be late"</i>

275
00:29:35,373 --> 00:29:39,510
<i>Yes, I've got
my questionnaire, babe</i>

276
00:29:39,512 --> 00:29:43,213
<i>And I found that
old number of mine</i>

277
00:29:47,486 --> 00:29:48,886
<i>Now</i>

278
00:29:48,888 --> 00:29:51,355
<i>I guess I well
to start moving</i>

279
00:29:51,357 --> 00:29:52,222
<i>Ooh, baby</i>

280
00:29:52,224 --> 00:29:55,926
<i>There ain't no need
of crying</i>

281
00:30:00,566 --> 00:30:03,934
<i>All you young men</i>

282
00:30:03,936 --> 00:30:07,271
<i>I mean, come on
and follow me</i>

283
00:30:23,222 --> 00:30:28,158
<i>Yes, all you young men</i>

284
00:30:30,329 --> 00:30:32,095
go away.

285
00:30:34,366 --> 00:30:36,033
Go away.

286
00:30:37,168 --> 00:30:38,468
<i>Lord, Whoo</i>

287
00:30:38,470 --> 00:30:42,072
<i>Lord, now, boys,
why can't we?</i>

288
00:31:02,094 --> 00:31:04,561
<i>I had a woman</i>

289
00:31:04,563 --> 00:31:07,164
<i>I had a broken heart</i>

290
00:31:07,166 --> 00:31:09,299
<i>Even though I wish
she'd tell me</i>

291
00:31:09,301 --> 00:31:11,034
<i>What that woman left me for</i>

292
00:31:11,036 --> 00:31:14,438
<i>That's what I been wonderin'</i>

293
00:31:14,440 --> 00:31:18,108
<i>I been wonderin'
all day long</i>

294
00:31:25,284 --> 00:31:29,286
<i>Maybe I will have
to wonder on</i>

295
00:31:33,458 --> 00:31:35,192
<i>And I'll try</i>

296
00:31:35,194 --> 00:31:37,961
<i>To make me feel good</i>

297
00:31:37,963 --> 00:31:39,529
<i>Say my gal would be good</i>

298
00:31:39,531 --> 00:31:42,466
<i>If she just only
understood...</i>

299
00:31:42,468 --> 00:31:47,113
<i>Understood...</i>

300
00:32:16,968 --> 00:32:18,635
What the fuck?

301
00:33:20,098 --> 00:33:22,199
S- Sara.

302
00:34:11,416 --> 00:34:14,551
Can you turn the music
back on, honey?

303
00:34:21,559 --> 00:34:22,859
Frank.

304
00:34:25,630 --> 00:34:27,364
Ugh...

305
00:34:27,366 --> 00:34:30,567
Better not be asleep.

306
00:35:21,453 --> 00:35:22,719
Aah.

307
00:35:24,555 --> 00:35:26,723
Oh, shit.

308
00:35:30,862 --> 00:35:32,696
Aah.

309
00:35:43,442 --> 00:35:44,608
Critters.

310
00:36:12,670 --> 00:36:14,337
Frank?

311
00:36:17,308 --> 00:36:18,675
Honey?

312
00:36:30,589 --> 00:36:31,888
Frank?

313
00:36:46,604 --> 00:36:47,904
Frank?

314
00:36:50,241 --> 00:36:51,708
Frank?

315
00:41:33,190 --> 00:41:34,724
Frank!

316
00:41:36,928 --> 00:41:38,628
Frank!

317
00:41:46,638 --> 00:41:48,571
Frank!

318
00:41:49,206 --> 00:41:50,106
Frank!

319
00:43:30,007 --> 00:43:33,743
Why are you doing this to us?

320
00:43:37,214 --> 00:43:41,684
What did you do to my husband?

321
00:43:46,724 --> 00:43:49,058
I'm sorry.
I'll calm down.

322
00:43:49,060 --> 00:43:51,794
I promise.

323
00:44:29,299 --> 00:44:32,068
Where's my husband?

324
00:44:32,070 --> 00:44:34,704
I want to see him.

325
00:44:34,706 --> 00:44:38,007
Please.

326
00:44:38,009 --> 00:44:41,010
Let me see my husband.

327
00:48:24,368 --> 00:48:25,968
Frank!

328
00:48:37,515 --> 00:48:39,215
Frank!

329
00:48:58,535 --> 00:49:01,237
What the fuck do you want
with us?

330
00:49:01,239 --> 00:49:03,406
You fucking psycho!

331
00:49:03,408 --> 00:49:06,208
I'm gonna fucking kill you.

332
00:49:07,745 --> 00:49:11,414
What did you do to my husband?

333
00:51:55,645 --> 00:51:57,580
Shh.

334
00:52:02,220 --> 00:52:03,953
Mr. Mcandrews?

335
00:52:03,955 --> 00:52:05,754
M.T.S. Security.

336
00:52:05,756 --> 00:52:06,789
Are you there?

337
00:52:09,559 --> 00:52:11,227
Hello?

338
00:52:13,597 --> 00:52:15,898
Mr. Mcandrews?

339
00:52:20,538 --> 00:52:22,404
Mr. Mcandrews?

340
00:52:22,539 --> 00:52:25,274
The silent alarm was activated.

341
00:52:26,610 --> 00:52:27,743
Mr...

342
00:52:27,745 --> 00:52:30,779
Mr. Mcandrews, can you hear me?

343
00:52:30,781 --> 00:52:31,747
I just stopped by to check

344
00:52:31,749 --> 00:52:34,750
and make sure everything's okay.

345
00:52:40,257 --> 00:52:41,624
Hello?

346
00:52:42,192 --> 00:52:43,792
Anybody home?

347
00:52:48,498 --> 00:52:50,366
Mr. Mcandrews!

348
00:52:50,767 --> 00:52:53,502
I can see your lights are on.

349
00:52:53,504 --> 00:52:55,237
Are you home?

350
00:52:56,973 --> 00:53:00,576
Yeah, I am missing
the end of the game for this.

351
00:53:00,578 --> 00:53:02,711
Mr. Mcandrews!

352
00:53:48,491 --> 00:53:51,427
- Good evening, Mr. Mcandrews.
- M.T.S. Security here.

353
00:53:51,429 --> 00:53:53,295
Yeah, your silent alarm
was activated,

354
00:53:53,297 --> 00:53:56,298
and I am currently
on the premises...

355
00:53:56,300 --> 00:53:58,734
At your door, actually.

356
00:54:03,540 --> 00:54:06,008
Yeah, well, I've checked
around the house,

357
00:54:06,010 --> 00:54:06,976
and everything seems fine.

358
00:54:06,978 --> 00:54:08,877
I will have to notify
the local police, however.

359
00:54:08,879 --> 00:54:11,947
That's a standard procedure
whenever we're called out

360
00:54:11,949 --> 00:54:12,915
for no good reason, so...

361
00:54:12,917 --> 00:54:15,651
If you could please get in touch
with us.

362
00:54:23,426 --> 00:54:25,861
You go ahead and have yourself
a good evening...

363
00:54:25,863 --> 00:54:27,596
Wherever you are, asshole.

364
00:54:38,909 --> 00:54:41,410
Wait.

365
00:56:38,495 --> 00:56:41,196
You have reached the king
county sheriff's department.

366
00:56:41,198 --> 00:56:42,798
All deputies
are currently on patrol.

367
00:56:42,800 --> 00:56:44,633
Please,
somebody's in my house.

368
00:56:44,635 --> 00:56:45,567
He's trying to kill me.

369
00:56:45,569 --> 00:56:48,103
Please hold as we attempt
to transfer your call

370
00:56:48,105 --> 00:56:49,104
to the officer on duty.

371
00:56:49,106 --> 00:56:50,539
Are you fucking serious?

372
00:56:50,541 --> 00:56:54,143
555-8699.

373
00:57:23,473 --> 00:57:24,573
Deputy Sanders here.

374
00:57:24,575 --> 00:57:25,507
- Hello?
- Hello.

375
00:57:25,509 --> 00:57:27,609
Hello, this is Sara,
Sara Mcandrews.

376
00:57:27,611 --> 00:57:28,577
Oh, hey, Mrs. Mcandrews.

377
00:57:28,579 --> 00:57:30,779
I don't know
if you got my message earlier...

378
00:57:30,781 --> 00:57:32,114
He's in the house.

379
00:57:32,116 --> 00:57:33,048
'Scuse me?

380
00:57:33,050 --> 00:57:34,249
He's in my house.

381
00:57:34,251 --> 00:57:34,983
Say again.

382
00:57:34,985 --> 00:57:36,585
Someone here is
trying to kill me.

383
00:57:36,587 --> 00:57:38,187
- I'm sorry.
- Ma'am, try to remain calm.

384
00:57:38,189 --> 00:57:40,956
Who is trying to kill you?
- I don't know.

385
00:57:40,958 --> 00:57:41,790
A fucking psycho.

386
00:57:41,792 --> 00:57:43,826
Mcandrews, are you able
to get somewhere safe?

387
00:57:43,828 --> 00:57:45,761
He's blocked out
all the exits.

388
00:57:45,763 --> 00:57:47,863
I don't know if we can get out.

389
00:57:47,865 --> 00:57:48,897
He's gonna find me.

390
00:57:48,899 --> 00:57:50,766
Miss Mcandrews,
I need you to tell me

391
00:57:50,768 --> 00:57:51,767
exactly where you are.

392
00:57:51,769 --> 00:57:53,569
Where is your family?

393
00:57:53,571 --> 00:57:54,837
Frank is dead.

394
00:57:54,839 --> 00:57:56,839
- All right.
- I'm on my way, ma'am.

395
00:57:58,609 --> 00:58:00,809
Gonna die.

396
00:58:00,811 --> 00:58:02,611
- Are you in a safe place,
Mrs. Mcandrews?

397
00:58:02,613 --> 00:58:04,513
I can't hear you...
Speak up.

398
00:58:04,515 --> 00:58:06,248
Hello?

399
00:58:06,250 --> 00:58:08,884
Fuck!

400
01:05:01,030 --> 01:05:05,233
Here's my two favorite people
in the whole, wide world.

401
01:05:05,235 --> 01:05:06,968
- Look at him!
- It's daddy!

402
01:05:06,970 --> 01:05:11,139
Say hi to daddy! Adam,
baby, can you say, "da-da"?

403
01:05:11,141 --> 01:05:13,141
Da-da?

404
01:05:13,143 --> 01:05:15,043
Here's my gorgeous wife.

405
01:05:15,045 --> 01:05:16,578
Aw.

406
01:05:16,580 --> 01:05:17,479
Those beautiful eyes

407
01:05:17,481 --> 01:05:19,281
I wake up to
every single morning.

408
01:05:19,283 --> 01:05:22,083
I am the luckiest man.

409
01:05:27,123 --> 01:05:28,623
I love you, baby girl.

410
01:05:28,625 --> 01:05:30,592
I love you more.

411
01:05:30,594 --> 01:05:31,660
I want you.

412
01:05:32,596 --> 01:05:33,662
- Let me know if...
- It is.

413
01:05:33,664 --> 01:05:36,698
Turn it off.
Turn off the camera.

414
01:05:37,199 --> 01:05:41,303
Here's my two favorite people
in the whole, wide world.

415
01:05:41,305 --> 01:05:42,737
- Look at him!
- It's daddy!

416
01:05:42,739 --> 01:05:44,706
Say hi to daddy!

417
01:06:08,497 --> 01:06:09,497
Aah!

418
01:06:28,452 --> 01:06:32,554
Leave me alone!

419
01:06:34,524 --> 01:06:37,492
Go away!

420
01:06:37,494 --> 01:06:38,693
Stop it!

421
01:06:42,199 --> 01:06:43,698
Please.

422
01:06:44,533 --> 01:06:47,469
I called the police!

423
01:06:51,207 --> 01:06:53,742
I called the police.

424
01:11:23,779 --> 01:11:27,415
I had a lovely time.

425
01:12:34,718 --> 01:12:37,985
You have reached the king
county sheriff's department.

426
01:12:37,987 --> 01:12:39,921
All deputies
are currently on patrol.

427
01:12:39,923 --> 01:12:42,790
Please hold as we attempt
to transfer your call

428
01:12:42,792 --> 01:12:43,624
to the officer on duty.

429
01:12:43,626 --> 01:12:48,696
For all medical emergencies,
dial 555-8699.

430
01:17:00,983 --> 01:17:04,652
Sanders.
It's Sanders.

431
01:17:04,654 --> 01:17:06,287
Michael...

432
01:17:06,289 --> 01:17:07,755
It's Sanders.

433
01:17:07,757 --> 01:17:08,856
I been...

434
01:17:08,858 --> 01:17:10,725
I been shot.

435
01:17:10,727 --> 01:17:11,993
I went to Mcandrews house.

436
01:17:11,995 --> 01:17:14,061
You got to get out here.

437
01:17:14,063 --> 01:17:16,297
We got two victims down.

438
01:17:16,299 --> 01:17:18,199
It's a husband and wife.

439
01:17:18,201 --> 01:17:20,801
Get an ambulance out here
right away.

440
01:17:20,803 --> 01:17:23,804
The perpetrator's male.

441
01:17:23,806 --> 01:17:25,172
6'1".

442
01:18:20,396 --> 01:18:22,496
<i>I'm the night watchman</i>

443
01:18:22,498 --> 01:18:26,834
<i>And I don't be seen
in the day</i>

444
01:18:30,772 --> 01:18:33,841
<i>I'm the night watchman</i>

445
01:18:33,843 --> 01:18:37,478
<i>And I don't be seen
in the day</i>

446
01:18:39,948 --> 01:18:43,217
<i>I declare
I do my work at night</i>

447
01:18:43,219 --> 01:18:48,089
<i>Whoo, lord, and I declare
I'm happy and gay</i>

448
01:18:51,359 --> 01:18:53,994
<i>'round factories
and saw mills</i>

449
01:18:53,996 --> 01:18:58,032
<i>Babe, you'll always see
me there</i>

450
01:19:01,036 --> 01:19:04,171
<i>Yeah, baby</i>

451
01:19:04,173 --> 01:19:08,509
<i>I declare
you'll always see me there</i>

452
01:19:10,879 --> 01:19:14,815
<i>Now, don't be scared
to call me</i>

453
01:19:14,817 --> 01:19:19,386
<i>Whoo, lord, gal, I got plenty
of time to spare</i>

454
01:19:23,191 --> 01:19:25,292
<i>I'm known in this country</i>

455
01:19:25,294 --> 01:19:29,130
<i>To be the best
night watchman in town</i>

456
01:19:32,901 --> 01:19:35,469
<i>Mmm</i>

457
01:19:35,471 --> 01:19:40,174
<i>I declare I'm the best
night watchman in town</i>

458
01:19:42,244 --> 01:19:46,113
<i>Now, if you'll clock me
punch in</i>

459
01:19:46,115 --> 01:19:50,885
<i>Whoo, lord I'll punch in
on my next go round</i>

460
01:19:54,156 --> 01:19:56,624
<i>I've got
a lot of competition</i>

461
01:19:56,626 --> 01:20:00,528
<i>Baby, but I can't lose</i>

462
01:20:03,398 --> 01:20:07,568
<i>Yeah, lot of competition</i>

463
01:20:07,570 --> 01:20:11,539
<i>No, but I declare
I can't lose</i>

464
01:20:15,510 --> 01:20:18,045
<i>I know I've got to win</i>

465
01:20:18,047 --> 01:20:22,183
<i>Whoo, lord, I declare
with this key I use</i>

466
01:20:25,887 --> 01:20:28,222
<i>I makes it around</i>

467
01:20:28,224 --> 01:20:32,326
<i>Yo my clocks, baby,
every night</i>

468
01:20:36,464 --> 01:20:38,599
<i>I makes it around</i>

469
01:20:38,601 --> 01:20:42,503
<i>I'm mean to my clocks
every night</i>

470
01:20:45,273 --> 01:20:49,143
<i>I declare I love
to punch clocks, baby</i>

471
01:20:49,145 --> 01:20:52,580
<i>Whoo, lord, when the moon
is shining bright</i>