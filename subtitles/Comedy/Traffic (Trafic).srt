1
00:02:51,119 --> 00:02:51,383
Ford goes from here to there.

2
00:03:45,876 --> 00:03:47,537
Good morning.

3
00:04:04,797 --> 00:04:07,994
I'm ready when you are, sir.

4
00:04:12,872 --> 00:04:13,839
Well?

5
00:04:16,409 --> 00:04:19,003
- The boss!
- What are you waiting for?

6
00:04:19,211 --> 00:04:21,179
Where's the foreman?

7
00:04:22,615 --> 00:04:24,048
Here, sir.

8
00:04:24,250 --> 00:04:28,880
I'm counting on you
to speed the work up.

9
00:04:29,088 --> 00:04:31,648
- We have no blueprint.
- No blueprint?

10
00:04:32,925 --> 00:04:34,722
We can't just improvise.

11
00:04:52,478 --> 00:04:54,173
Mr. Morel.

12
00:04:54,380 --> 00:04:56,439
Hello, Mr. Morel.

13
00:04:56,649 --> 00:04:58,879
We've run into a few problems,

14
00:04:59,084 --> 00:05:03,214
but our model will be ready
for the show in Amsterdam.

15
00:05:06,459 --> 00:05:10,054
- Your name, please?
- Maria Kimberley.

16
00:05:30,787 --> 00:05:34,518
It's every man for himself here!
What a madhouse!

17
00:05:39,062 --> 00:05:42,395
I see you've considered
every detail carefully.

18
00:06:21,965 --> 00:06:24,456
Give me that!
It's the wrong design!

19
00:06:28,505 --> 00:06:30,803
Where's the new design?

20
00:06:36,282 --> 00:06:39,547
We have no measurements
for the booth!

21
00:06:39,752 --> 00:06:43,017
That's your job!
Call Amsterdam!

22
00:06:43,222 --> 00:06:46,817
Time's running out.
I have no measurements.

23
00:06:47,993 --> 00:06:49,620
Get me Amsterdam.

24
00:06:54,828 --> 00:06:57,524
I handle public relations.
That's my job.

25
00:06:56,531 --> 00:06:58,226
Go right ahead.

26
00:06:56,306 --> 00:06:59,036
Doesn't he look handsome?

27
00:06:57,843 --> 00:06:59,743
Morning, sir.
He's so nice.

28
00:06:57,413 --> 00:07:03,283
<i>A call from Altra
for the expo organizer. </i>

29
00:07:09,689 --> 00:07:13,022
- What's that?
- I didn't say anything!

30
00:07:15,862 --> 00:07:18,558
Hold the line.
I don't understand.

31
00:07:26,974 --> 00:07:30,842
They're giving measurements,
but I don't understand. Please come.

32
00:07:50,789 --> 00:07:55,021
You need a check, right?
I just have to call Dad.

33
00:07:55,227 --> 00:07:58,025
We can get by for now.

34
00:08:30,973 --> 00:08:32,770
It's 6:00.

35
00:08:35,878 --> 00:08:40,713
Gentleman, you can't leave!
We have to finish tonight!

36
00:08:42,518 --> 00:08:47,512
The camping car must leave
for Amsterdam tomorrow!

37
00:09:00,971 --> 00:09:04,407
That goes in the station wagon.
The boxes go in the truck.

38
00:09:04,608 --> 00:09:06,166
Then say so!

39
00:09:06,810 --> 00:09:11,213
- Don't break anything!
- Just following orders.

40
00:09:15,152 --> 00:09:17,677
How can I work
with these people?

41
00:09:24,762 --> 00:09:27,663
- Go on!
- Easier said than done!

42
00:09:27,865 --> 00:09:28,923
Come on.

43
00:09:29,133 --> 00:09:31,761
More brochures?
We won't run out!

44
00:09:31,969 --> 00:09:35,564
See there?
Check out our "public relations."

45
00:09:35,940 --> 00:09:39,603
- On the roof.
- Move it forward so it's level.

46
00:09:39,810 --> 00:09:43,507
Careful, that's my car.

47
00:09:46,317 --> 00:09:48,581
All aboard for Amsterdam!

48
00:09:55,628 --> 00:09:57,596
What'd you expect?

49
00:10:00,032 --> 00:10:03,490
You gotta be kidding!
I don't believe it!

50
00:10:07,606 --> 00:10:10,666
You speak English?

51
00:10:19,819 --> 00:10:21,582
Allow me to introduce...

52
00:10:21,787 --> 00:10:25,518
- We've met.
- Hello. So you're our PR?

53
00:10:29,362 --> 00:10:32,593
- To Amsterdam!
- See ya, Marcel!

54
00:11:38,335 --> 00:11:41,395
You go on to Amsterdam!

55
00:11:57,422 --> 00:11:58,912
Well?

56
00:12:05,897 --> 00:12:07,524
Hey, watch it!

57
00:12:39,031 --> 00:12:41,659
Order some white wine.

58
00:12:45,037 --> 00:12:48,529
Waiter, may I use
the telephone?

59
00:12:47,942 --> 00:12:51,002
I'd like
the Amsterdam Car Show.

60
00:12:54,347 --> 00:12:57,111
The manager's wife
is at the beauty salon.

61
00:12:58,285 --> 00:12:59,377
<i>Telephone</i>

62
00:12:59,587 --> 00:13:04,217
<i>for Mr. Van den Brink
from Altra. </i>

63
00:13:05,193 --> 00:13:07,889
That's up and that's down.

64
00:13:08,296 --> 00:13:12,255
Sorry, no.
Other way.

65
00:13:12,467 --> 00:13:14,332
You men figure it out.

66
00:13:20,875 --> 00:13:22,900
Convention center here.

67
00:13:23,111 --> 00:13:24,908
Good morning.

68
00:13:26,414 --> 00:13:28,644
Good morning, sir.

69
00:13:31,352 --> 00:13:33,513
This is Altra calling...

70
00:13:33,988 --> 00:13:37,116
to say that we're running late.

71
00:13:37,525 --> 00:13:40,653
No, this is public relations.

72
00:13:42,130 --> 00:13:45,099
<i>Bartender, a glass of
Cotes du Rhone, please.</i>

73
00:13:46,668 --> 00:13:48,431
<i>Two Cotes du Rhone.</i>

74
00:13:50,038 --> 00:13:52,438
It's not my fault!

75
00:14:31,582 --> 00:14:33,174
Here she is.

76
00:14:34,619 --> 00:14:38,749
What's wrong? We'll never make it
at this rate! I'll get a mechanic!

77
00:14:38,956 --> 00:14:41,857
Fine! We'll stay here.

78
00:14:51,969 --> 00:14:54,335
- What's wrong?
- It's out of gas!

79
00:14:54,538 --> 00:14:58,201
- Impossible!
- You left the motor running.

80
00:14:58,876 --> 00:15:02,175
Of course!
I've had enough cranking!

81
00:16:41,815 --> 00:16:45,979
Don't bother!
We're out of gas!

82
00:16:45,787 --> 00:16:49,985
I've had it! I'm going to Amsterdam!
This isn't my job!

83
00:16:50,191 --> 00:16:53,957
Well, it isn't my job either!

84
00:17:14,119 --> 00:17:16,349
Out of gas?
It happens.

85
00:17:16,555 --> 00:17:18,614
You were lucky.
It's a nice day.

86
00:17:20,058 --> 00:17:22,549
Let's see... one gallon.

87
00:17:24,129 --> 00:17:26,097
Here. Thanks.

88
00:17:28,901 --> 00:17:31,267
You too?
It's one of those days.

89
00:17:31,703 --> 00:17:35,002
You've been running.
You came through the fields?

90
00:17:35,207 --> 00:17:37,641
Yes, it's shorter.

91
00:17:54,795 --> 00:17:57,127
Twenty-five francs.

92
00:19:06,236 --> 00:19:09,728
<i>Slim down fast
with Slim-Thin! </i>

93
00:21:36,359 --> 00:21:38,452
<i>The Cyclone 70.</i>

94
00:21:38,661 --> 00:21:42,290
<i>A new raincoat... 
especially made for the sun. </i>

95
00:23:01,582 --> 00:23:05,951
It's the clutch.
All this stopping and starting.

96
00:23:06,453 --> 00:23:08,887
This time it's serious.

97
00:24:30,975 --> 00:24:34,467
- Look at that!
- Let's help him.

98
00:24:34,414 --> 00:24:36,780
For the Altra booth.

99
00:24:37,884 --> 00:24:44,847
<i>Will the Altra representative
please come to the phone. </i>

100
00:24:45,058 --> 00:24:47,117
- Telephone?
- Over there.

101
00:24:51,565 --> 00:24:54,193
Hello? Yes, it's me.

102
00:24:54,735 --> 00:24:57,397
What's that?
I can't hear you!

103
00:24:57,398 --> 00:25:00,901
Broke down?
You're still in Belgium?

104
00:25:00,903 --> 00:25:02,239
...in the show,
Mr. Van den Brink?

105
00:25:02,443 --> 00:25:06,743
This year we expect to have...

106
00:25:09,917 --> 00:25:13,114
530 vehicles

107
00:25:14,088 --> 00:25:18,582
on display
in four different showrooms.

108
00:25:28,467 --> 00:25:30,560
Damn it!

109
00:26:02,904 --> 00:26:05,873
It's the most important
car show in Europe.

110
00:26:06,073 --> 00:26:10,305
Yes, we've already
beat Brussels,

111
00:26:10,511 --> 00:26:15,244
and we'll beat London
and Paris next year. Just watch!

112
00:26:17,251 --> 00:26:20,186
Jeez, look at that!

113
00:26:20,388 --> 00:26:22,822
They hit my car!

114
00:26:23,024 --> 00:26:26,551
That's too much!
I can't leave my car for a minute!

115
00:26:26,761 --> 00:26:29,093
I'll do something about this!

116
00:26:34,036 --> 00:26:36,800
Now there's something!
You see that?

117
00:26:37,005 --> 00:26:39,940
How many cylinders is that?

118
00:26:40,142 --> 00:26:42,303
Look at those accessories.

119
00:26:42,511 --> 00:26:44,308
Well-equipped!

120
00:26:44,513 --> 00:26:46,174
We've got a good spot.

121
00:26:46,381 --> 00:26:48,849
Uh-oh, cross-traffic!

122
00:26:49,051 --> 00:26:50,348
Ultrachic!

123
00:27:18,316 --> 00:27:21,342
Careful there!

124
00:27:21,553 --> 00:27:25,216
That's the scenery
for our model.

125
00:27:26,358 --> 00:27:30,124
And our model
is the camping car.

126
00:27:33,465 --> 00:27:37,060
You made it!
They called for you.

127
00:27:39,371 --> 00:27:42,101
They had a breakdown!
You must call them back.

128
00:27:45,176 --> 00:27:48,839
You call that working?
Pick it up a bit!

129
00:27:49,247 --> 00:27:51,545
You're not here to nap.

130
00:27:51,750 --> 00:27:53,581
Get lost!

131
00:27:54,986 --> 00:27:59,787
Stop! What are you doing!
Don't touch that!

132
00:27:59,991 --> 00:28:02,983
That thing ran in
the Monte Carlo Rally, you idiot!

133
00:28:04,130 --> 00:28:08,396
I'm public relations.
Get me this number immediately.

134
00:28:07,504 --> 00:28:09,472
I'll take it there.

135
00:28:16,109 --> 00:28:18,100
Get her!

136
00:28:18,311 --> 00:28:23,271
Information.
You're wanted in showroom No. 1.

137
00:28:43,770 --> 00:28:46,136
Go ahead! It'll fit!

138
00:29:31,988 --> 00:29:36,789
Francois, you watch the booth.
I'm going to the border.

139
00:29:53,911 --> 00:29:55,879
Get out of here, quick!

140
00:30:19,371 --> 00:30:21,430
The crowds are pouring in.

141
00:30:21,640 --> 00:30:24,302
Sir, why have you come
to the show?

142
00:30:24,509 --> 00:30:26,977
Because I like cars.
I love sports cars.

143
00:30:27,179 --> 00:30:29,340
I trust you won't be
disappointed.

144
00:30:32,756 --> 00:30:35,281
<i>This year's models
will be an international success. </i>

145
00:30:35,892 --> 00:30:38,156
<i>There's a huge variety
of models. </i>

146
00:30:40,061 --> 00:30:42,928
<i>Colors are subdued, 
with lots of warm tones, </i>

147
00:30:43,131 --> 00:30:46,589
<i>and quality and style
are uniformly high. </i>

148
00:30:47,301 --> 00:30:49,360
<i>Visitors stream by, </i>

149
00:30:49,570 --> 00:30:53,700
<i>their eyes aglow at the sight
of these gleaming vehicles, </i>

150
00:30:53,908 --> 00:30:57,241
<i>imagining themselves
behind the wheel</i>

151
00:30:57,445 --> 00:31:01,142
<i>en route to their holiday
destination. </i>

152
00:31:26,841 --> 00:31:30,277
- Everything okay now?
- It should be.

153
00:31:30,745 --> 00:31:32,440
Try it out.

154
00:31:32,980 --> 00:31:34,709
Yeah, it feels better.

155
00:31:34,916 --> 00:31:38,408
I did what I could.
I didn't have much time.

156
00:31:39,253 --> 00:31:41,414
Good-bye, and thanks.

157
00:32:12,021 --> 00:32:13,454
<i>Commercials... </i>

158
00:32:13,656 --> 00:32:15,783
<i>keep you informed. </i>

159
00:32:34,778 --> 00:32:36,939
Are you nuts?

160
00:32:48,427 --> 00:32:49,951
Hurry!

161
00:33:07,579 --> 00:33:09,012
Hurry!

162
00:33:09,381 --> 00:33:11,349
Quick, the license number!

163
00:33:12,918 --> 00:33:16,319
I was looking
somewhere else!

164
00:33:16,521 --> 00:33:19,388
It was a French truck
marked "Altra."

165
00:33:34,073 --> 00:33:35,802
<i>Attention, attention. </i>

166
00:33:36,008 --> 00:33:42,140
<i>Immediately apprehend
a blue truck with the name "Altra. "</i>

167
00:33:42,348 --> 00:33:44,976
<i>It did not stop
at border control. </i>

168
00:34:21,421 --> 00:34:23,389
I'm just the driver.

169
00:34:23,590 --> 00:34:26,889
May I see
what you have in back?

170
00:34:27,093 --> 00:34:29,687
You want my papers?

171
00:34:29,996 --> 00:34:32,294
The car show in Amsterdam.

172
00:35:00,864 --> 00:35:03,264
Do you really need that?

173
00:35:07,037 --> 00:35:10,473
Could you tell me
where the Altra booth is?

174
00:35:10,674 --> 00:35:13,609
I don't understand.

175
00:35:16,180 --> 00:35:19,149
Come see the latest model.
No obligation.

176
00:35:20,149 --> 00:35:22,640
I'm a vendor myself.
I have a booth here.

177
00:35:25,552 --> 00:35:27,383
Sorry.

178
00:35:27,758 --> 00:35:29,817
Careful, please.

179
00:35:32,430 --> 00:35:34,660
Have a look.

180
00:35:34,667 --> 00:35:36,726
Very comfortable.

181
00:35:42,941 --> 00:35:49,369
<i>Will someone from DAF
please call the operator. </i>

182
00:35:49,281 --> 00:35:52,011
Isn't this a nice car?

183
00:35:58,057 --> 00:35:59,649
What do you think?

184
00:35:59,859 --> 00:36:01,554
It's all right.

185
00:36:16,942 --> 00:36:20,878
Yes, this is Altra.

186
00:36:22,681 --> 00:36:23,841
Hold on.

187
00:36:24,049 --> 00:36:25,744
- May I?
- Take it!

188
00:36:25,951 --> 00:36:27,475
Boss!

189
00:36:27,953 --> 00:36:30,444
It's for you. It's urgent.

190
00:36:30,656 --> 00:36:32,521
- Telephone.
- What is it?

191
00:36:32,725 --> 00:36:34,454
They're still not here.

192
00:36:34,660 --> 00:36:38,289
Our model's not here?
You should have said so.

193
00:36:39,266 --> 00:36:43,600
What? Yes, I'm general
manager of Altra.

194
00:36:43,971 --> 00:36:46,963
Police?
I didn't call the police!

195
00:36:53,014 --> 00:36:55,073
Hey, where'd you go?

196
00:37:57,983 --> 00:37:59,644
Stop there.

197
00:37:59,852 --> 00:38:03,185
We must inspect your cargo.

198
00:38:03,389 --> 00:38:07,985
You're all in on it!
Cops are the same the world over!

199
00:38:10,764 --> 00:38:12,959
Open that.

200
00:38:13,433 --> 00:38:15,196
I understand.

201
00:38:17,270 --> 00:38:18,862
It's an exhibition model.

202
00:38:19,072 --> 00:38:20,699
Slowly.

203
00:38:23,843 --> 00:38:27,210
- Other side.
- Hold your horses!

204
00:38:27,414 --> 00:38:29,405
- Watch my feet!
- Come on.

205
00:38:29,616 --> 00:38:31,550
Let me do my job, sir!

206
00:38:33,186 --> 00:38:35,677
Careful! Stop.

207
00:38:35,889 --> 00:38:38,790
- Not so fast!
- That's good.

208
00:38:56,276 --> 00:38:57,368
Who does she think she is?

209
00:38:57,811 --> 00:39:01,076
What's inside?

210
00:39:01,153 --> 00:39:04,281
Wait a minute.
We'll show you.

211
00:39:04,490 --> 00:39:06,583
You'll see.

212
00:39:15,328 --> 00:39:20,425
It's a standard model
decked out for camping.

213
00:39:22,302 --> 00:39:24,167
Keep going.

214
00:39:26,973 --> 00:39:29,874
Carefully designed.
Very practical.

215
00:39:30,477 --> 00:39:34,413
- What's under there?
- That's the bumper.

216
00:39:35,648 --> 00:39:40,449
You pull like this, see?
A chair.

217
00:39:40,687 --> 00:39:43,554
Wait. Here's a table.

218
00:39:43,694 --> 00:39:45,184
Not bad, eh?

219
00:39:45,396 --> 00:39:49,093
- What's that?
- For coffee, and a cigarette lighter.

220
00:39:53,504 --> 00:39:56,405
Built-in cigarette lighter.
Very nice.

221
00:39:58,842 --> 00:40:01,811
Watch this. Look.

222
00:40:02,012 --> 00:40:04,378
Soap for your hands.

223
00:40:06,452 --> 00:40:08,511
Let's have it analyzed.

224
00:40:10,556 --> 00:40:13,024
Of course the papers
are in order!

225
00:40:14,993 --> 00:40:16,620
You think I'm a thief?

226
00:40:16,829 --> 00:40:20,026
I need this analyzed.

227
00:40:20,232 --> 00:40:22,166
May I look in your purse?

228
00:40:25,170 --> 00:40:29,664
This is customs.
Let's see what's in here.

229
00:40:32,778 --> 00:40:35,804
- An atomizer.
- Perfume?

230
00:40:36,014 --> 00:40:38,141
Of course it's perfume!

231
00:40:38,350 --> 00:40:40,341
Check all of it.

232
00:40:41,354 --> 00:40:44,585
That's what I call a smash-up.

233
00:40:44,991 --> 00:40:48,791
Here are the papers.
This is your citation.

234
00:40:48,995 --> 00:40:51,156
Go with the sergeant.

235
00:40:52,666 --> 00:40:56,534
No, go with the sergeant.
Other way.

236
00:40:57,671 --> 00:40:59,502
Let's continue

237
00:40:59,706 --> 00:41:01,765
our inspection, sir.

238
00:41:01,975 --> 00:41:03,636
Watch.

239
00:41:03,843 --> 00:41:05,674
Simple, right?

240
00:41:09,752 --> 00:41:11,845
Fantastic!

241
00:41:12,689 --> 00:41:14,680
Very interesting.

242
00:41:15,725 --> 00:41:17,625
You push it back in.

243
00:41:17,894 --> 00:41:20,522
Ingenious, right?
Come this way.

244
00:41:20,730 --> 00:41:23,665
Get inside, and no tricks.

245
00:41:26,636 --> 00:41:29,161
I'm gonna lose my mind!

246
00:41:30,873 --> 00:41:34,934
Mr. Hulot, please help me!
We've got to do something!

247
00:41:37,178 --> 00:41:41,114
Just throw me
in jail right now!

248
00:42:08,946 --> 00:42:10,470
Yes, sir?

249
00:42:11,849 --> 00:42:14,682
Authorize... French customs.

250
00:42:21,559 --> 00:42:24,926
The papers for the French car
are not in order.

251
00:42:25,129 --> 00:42:27,324
The stamp's no good either.

252
00:42:28,866 --> 00:42:31,130
This is no good either.

253
00:42:32,136 --> 00:42:35,196
There's a mistake
here too, see?

254
00:42:36,007 --> 00:42:37,975
With your permission, sir.

255
00:42:38,611 --> 00:42:44,106
See... old permit.

256
00:42:44,317 --> 00:42:48,253
Wrong form. Understand?

257
00:42:55,295 --> 00:42:56,853
To the right.

258
00:43:08,875 --> 00:43:10,570
A shower.

259
00:43:10,777 --> 00:43:14,406
I'll show you.
But we need water pressure.

260
00:43:14,614 --> 00:43:17,014
Just a second.

261
00:43:17,217 --> 00:43:19,310
Watch out!
You'll get soaked!

262
00:43:19,519 --> 00:43:22,886
Start the engine
for hot water.

263
00:43:29,696 --> 00:43:31,664
What's that there?

264
00:43:34,133 --> 00:43:36,067
This here...

265
00:43:49,983 --> 00:43:51,610
It doesn't work.

266
00:43:57,757 --> 00:44:01,090
- I didn't do anything!
- Explain it inside.

267
00:44:01,030 --> 00:44:04,261
A few more questions, sir.
What's that?

268
00:44:04,467 --> 00:44:06,435
Oil pressure.

269
00:44:29,525 --> 00:44:31,220
There we go.

270
00:44:31,427 --> 00:44:34,021
Even too hot.
Turn it off.

271
00:44:35,865 --> 00:44:38,629
Come on. Careful.

272
00:44:53,884 --> 00:44:55,852
Have a seat over here.

273
00:45:01,792 --> 00:45:06,229
Authorization
for the camping car.

274
00:45:07,497 --> 00:45:11,490
- What's this?
- Come see.

275
00:45:13,671 --> 00:45:15,263
See?

276
00:45:15,414 --> 00:45:17,109
And that's that.

277
00:45:22,185 --> 00:45:24,119
What's this mean?

278
00:45:24,721 --> 00:45:27,519
Fine!
I'll just put down anything!

279
00:45:32,657 --> 00:45:36,149
It's an overhead light too.

280
00:45:36,595 --> 00:45:41,464
Good idea, eh?
Mr. Hulot is our designer.

281
00:45:44,369 --> 00:45:48,032
- What's in there?
- Ah, that! How do you call it?

282
00:45:51,445 --> 00:45:53,436
I'll show you.

283
00:45:56,050 --> 00:45:58,018
Yes, it's connected.

284
00:46:00,153 --> 00:46:02,747
That's not my area.

285
00:46:02,956 --> 00:46:04,321
It's water.

286
00:46:14,304 --> 00:46:17,137
Sweetheart, are you okay?

287
00:46:18,040 --> 00:46:19,507
It's so cold in here.

288
00:46:19,708 --> 00:46:23,508
- This is awful.
- I don't care, baby.

289
00:46:29,559 --> 00:46:32,960
Suspenders.
A little French ingenuity.

290
00:46:37,367 --> 00:46:39,562
Impressive, eh?

291
00:46:39,769 --> 00:46:43,136
Shall I give you a hand?

292
00:46:50,640 --> 00:46:52,608
Look at this.

293
00:46:52,808 --> 00:46:55,174
It's for camping.

294
00:47:10,059 --> 00:47:13,586
For God's sake,
it's just a twin bed!

295
00:47:13,796 --> 00:47:16,162
I'll show you!

296
00:47:16,766 --> 00:47:18,859
Very comfortable, eh?

297
00:47:19,101 --> 00:47:21,433
Watch this.

298
00:47:21,637 --> 00:47:24,231
Ignition. Watch out.

299
00:47:26,309 --> 00:47:28,368
Very nice, gentlemen!

300
00:47:28,744 --> 00:47:30,974
Works well, right?

301
00:47:32,615 --> 00:47:35,277
It was a big hit
at the car show.

302
00:47:39,062 --> 00:47:42,293
See? Even a working TV.

303
00:47:48,375 --> 00:47:52,471
I don't know why,
but I love this decor.

304
00:47:59,320 --> 00:48:02,187
Hurry! To your posts!

305
00:48:07,370 --> 00:48:08,837
Go!

306
00:48:10,245 --> 00:48:12,338
Where are they going?

307
00:48:15,118 --> 00:48:18,144
Maybe we can get going again.

308
00:48:18,355 --> 00:48:20,186
Car... leave?

309
00:48:20,390 --> 00:48:22,381
Car not go. Stay.

310
00:48:22,593 --> 00:48:25,221
Well, that's that.

311
00:48:38,570 --> 00:48:41,664
- The car show is open!
- It's not going anywhere.

312
00:48:41,873 --> 00:48:43,864
Thanks for understanding!

313
00:48:44,093 --> 00:48:46,994
I don't know
what to do anymore!

314
00:48:47,196 --> 00:48:50,188
I should never have
taken this job!

315
00:48:53,799 --> 00:48:58,793
I've always been a success
wherever I've done public relations.

316
00:48:58,740 --> 00:49:00,298
At ease!

317
00:49:17,876 --> 00:49:20,868
They should have at least
tried to get us visas.

318
00:49:23,648 --> 00:49:27,084
We'll never get to Amsterdam!

319
00:49:27,853 --> 00:49:29,480
Give me your suitcase.

320
00:49:37,295 --> 00:49:39,058
Ready?

321
00:49:54,346 --> 00:49:56,837
Was there a call from Paris?

322
00:50:15,018 --> 00:50:21,617
<i>Will someone from Morris
please call the operator. </i>

323
00:50:49,787 --> 00:50:52,119
Yes? What's going on?

324
00:50:51,723 --> 00:50:53,953
What?
The police station!

325
00:50:53,726 --> 00:50:55,921
Francois,
get my briefcase.

326
00:50:56,962 --> 00:51:00,329
Just a moment.
Go ahead.

327
00:51:00,533 --> 00:51:03,263
I'll be right there.

328
00:51:03,769 --> 00:51:04,861
Francois...

329
00:51:04,871 --> 00:51:06,896
turn off those birds!

330
00:51:51,351 --> 00:51:53,581
Where's a taxi stand?

331
00:51:53,587 --> 00:51:55,054
Out in front.

332
00:52:57,555 --> 00:53:00,820
You can sleep and eat
in the great outdoors.

333
00:53:02,093 --> 00:53:04,653
Thank you.
So we can go?

334
00:53:06,169 --> 00:53:08,137
The papers are
in order now.

335
00:53:11,309 --> 00:53:12,936
Hurry up!

336
00:56:05,850 --> 00:56:08,819
I heard a "ping"!

337
00:56:09,687 --> 00:56:11,917
Just great!

338
00:56:13,158 --> 00:56:16,924
Not even tied down,
of course.

339
00:56:21,101 --> 00:56:23,069
What happened?

340
00:56:23,737 --> 00:56:25,705
Why didn't you stop?

341
00:56:57,504 --> 00:56:59,563
What did you...

342
00:56:59,573 --> 00:57:04,203
It's nothing serious.
I can manage.

343
00:57:04,196 --> 00:57:06,494
I'll make a phone call.

344
00:58:14,819 --> 00:58:17,344
No, take a left.

345
00:58:24,863 --> 00:58:26,626
Forward.

346
00:58:32,370 --> 00:58:35,362
No, right.

347
00:58:46,419 --> 00:58:50,753
What are you doing? We're looking
for a mechanic to fix the car.

348
00:58:52,259 --> 00:58:55,956
- We need a mechanic.
- Let's go!

349
00:59:15,388 --> 00:59:17,481
Left.

350
00:59:19,059 --> 00:59:21,289
Straight ahead.

351
00:59:21,494 --> 00:59:22,961
Follow me, Marcel!

352
00:59:56,092 --> 00:59:58,788
No, the doorbell's broken.

353
00:59:58,995 --> 01:00:00,622
Ah, broken.

354
01:00:02,932 --> 01:00:07,596
My wife's hard of hearing.

355
01:00:36,033 --> 01:00:38,831
What's going on?
What are you doing?

356
01:00:39,036 --> 01:00:41,004
What is it?

357
01:00:42,272 --> 01:00:45,241
An accident?
I'll be right there.

358
01:00:46,583 --> 01:00:48,210
Thanks.

359
01:00:49,386 --> 01:00:51,877
- Okay?
- Yes, fine.

360
01:00:53,023 --> 01:00:54,786
I'm all right.

361
01:00:55,659 --> 01:00:57,923
What happened to you?

362
01:00:58,195 --> 01:01:01,289
That car, of course!
And this whole thing came down!

363
01:01:01,498 --> 01:01:04,160
This is terrible!
Come inside.

364
01:01:05,636 --> 01:01:08,469
You look just awful!

365
01:01:17,577 --> 01:01:19,545
Anyone home?

366
01:01:26,654 --> 01:01:29,316
- Who is it?
- Go back to bed.

367
01:01:29,523 --> 01:01:31,957
- Accident. Truck.
- French?

368
01:01:33,427 --> 01:01:36,055
- Great.
- We're going to Amsterdam.

369
01:01:36,263 --> 01:01:38,857
- Wanna have a look?
- May I?

370
01:01:38,800 --> 01:01:41,769
I can fix it.
- You can?

371
01:01:42,704 --> 01:01:44,171
No idea.

372
01:01:47,741 --> 01:01:49,834
You fix tonight?

373
01:01:49,877 --> 01:01:52,004
Tomorrow? No sooner?

374
01:01:59,887 --> 01:02:01,912
You sleep here.

375
01:02:03,324 --> 01:02:05,952
No, ma'am,
you're not sleeping here.

376
01:02:16,571 --> 01:02:18,334
Here I am.

377
01:02:18,906 --> 01:02:21,431
Get the luggage?
This feels familiar...

378
01:02:21,642 --> 01:02:23,303
Bring your bags.

379
01:02:33,354 --> 01:02:35,845
A quick housecleaning!

380
01:02:35,857 --> 01:02:37,324
Go ahead, ma'am.

381
01:02:37,526 --> 01:02:40,927
You'll sleep here
like a princess.

382
01:02:44,199 --> 01:02:46,258
Pass me the other one,
please.

383
01:02:56,144 --> 01:03:00,012
Mr. Piton will sleep
with his mistress.

384
01:03:00,215 --> 01:03:02,410
Maybe I could...
- Good night.

385
01:03:02,419 --> 01:03:06,378
Good night then.
Back to my palace.

386
01:03:26,276 --> 01:03:27,868
Need some help?

387
01:03:39,523 --> 01:03:41,582
Do you know a Mr. Schriffel?

388
01:03:41,792 --> 01:03:43,454
Hey, what about me?

389
01:03:43,461 --> 01:03:47,420
I have his suitcase.
Can you help me?

390
01:03:47,532 --> 01:03:49,124
Of course I can.

391
01:03:49,234 --> 01:03:50,963
- You know him?
- He's my father.

392
01:04:04,069 --> 01:04:07,402
- It's nothing. 
- You're just saying that. 

393
01:04:08,384 --> 01:04:11,184
- Were you drinking again?
- Just two beers. 

394
01:04:12,388 --> 01:04:14,017
That's what you always say. 

395
01:04:14,059 --> 01:04:17,119
- You want your slippers? 
- No. 

396
01:04:31,713 --> 01:04:34,944
I'll make up the bed. 

397
01:04:36,150 --> 01:04:40,348
I'll have to do everything tomorrow, 
with your leg like that. 

398
01:04:40,555 --> 01:04:43,649
It'll be better tomorrow. 

399
01:04:43,858 --> 01:04:45,655
You say that now. 

400
01:05:26,102 --> 01:05:28,263
Is this the place?

401
01:05:31,574 --> 01:05:33,269
Let me help you.

402
01:05:39,315 --> 01:05:42,307
- That's your name?
- Yes, my nickname.

403
01:05:42,519 --> 01:05:44,851
Very, very charming.

404
01:05:46,156 --> 01:05:49,592
Peter, we can't stay
out here all night.

405
01:05:50,160 --> 01:05:52,458
Be reasonable.

406
01:05:54,063 --> 01:05:56,725
Come on.
It's very late.

407
01:05:56,933 --> 01:06:00,630
Look!
What a beautiful night, darling!

408
01:06:00,837 --> 01:06:03,863
The silence, the stars!

409
01:06:05,343 --> 01:06:09,143
Peter, let me go now,
all right?

410
01:06:09,347 --> 01:06:12,441
I have a job to do.
You understand?

411
01:06:12,650 --> 01:06:15,448
- I love your soft hands.
- That's sweet.

412
01:06:15,653 --> 01:06:19,020
Maria, stay here tonight.

413
01:06:21,426 --> 01:06:23,792
I absolutely have to go!

414
01:06:23,995 --> 01:06:25,360
Let me go!

415
01:06:27,432 --> 01:06:30,094
You're out of your mind!

416
01:06:31,503 --> 01:06:34,267
You're all the same.
Get lost!

417
01:07:09,412 --> 01:07:11,346
Are you fixing the...

418
01:07:21,189 --> 01:07:24,215
What a mess!
My wife never straightens up!

419
01:07:25,127 --> 01:07:27,152
Do me a favor.

420
01:07:27,963 --> 01:07:30,090
Find me one just like this.

421
01:09:03,096 --> 01:09:06,156
Come now, quickly!

422
01:09:08,735 --> 01:09:11,226
Please leave quickly.

423
01:09:11,471 --> 01:09:12,631
Come on.

424
01:09:12,839 --> 01:09:14,431
Back up.

425
01:09:16,409 --> 01:09:18,309
Come on.

426
01:09:18,511 --> 01:09:20,035
The chair!

427
01:09:23,182 --> 01:09:25,446
No, clear out.

428
01:09:25,651 --> 01:09:28,313
This isn't a picnic.

429
01:10:12,066 --> 01:10:16,230
Why'd you put it there?
And don't tell me how to do my job.

430
01:10:37,059 --> 01:10:38,720
Good morning.

431
01:10:39,995 --> 01:10:43,362
- A little baby!
- Say hi.

432
01:10:44,066 --> 01:10:45,033
Quiet!

433
01:10:49,338 --> 01:10:50,930
Sit.

434
01:10:51,140 --> 01:10:53,108
Go lie down.
Good dog.

435
01:10:55,045 --> 01:10:56,307
Sit!

436
01:10:58,115 --> 01:11:00,845
Stay right there.
Don't go outside.

437
01:11:01,184 --> 01:11:03,152
You never know with that dog.

438
01:11:16,267 --> 01:11:18,098
You need a tire?

439
01:11:18,469 --> 01:11:22,132
I've got everything.
Special stuff.

440
01:12:00,680 --> 01:12:02,648
Keep the dog quiet!

441
01:12:32,746 --> 01:12:34,509
I'll be right back.

442
01:12:45,328 --> 01:12:47,455
I brought a picnic too.

443
01:13:47,990 --> 01:13:51,756
Take the handbrake off
and push, guys.

444
01:14:01,546 --> 01:14:05,744
If you speak Dutch,
I won't understand a word!

445
01:14:05,950 --> 01:14:07,645
Makes me want to puke.

446
01:14:07,852 --> 01:14:10,320
What's that mean, "puke"?

447
01:14:13,524 --> 01:14:17,221
Get in the kitchen
and fix us some eggs.

448
01:14:22,533 --> 01:14:26,867
- Joke, fix us some eggs.
- Coming right up.

449
01:14:49,258 --> 01:14:51,783
Wait a minute,
you big baby.

450
01:15:00,837 --> 01:15:04,238
Damn it!
Mr. Hulot, the phone number!

451
01:15:09,379 --> 01:15:10,869
This is very serious.

452
01:15:11,080 --> 01:15:13,844
I must call
the car show right now.

453
01:16:02,601 --> 01:16:04,569
Maria, what's wrong?

454
01:16:59,222 --> 01:17:01,190
That takes the cake!

455
01:17:04,027 --> 01:17:06,052
But what do I care?

456
01:17:41,705 --> 01:17:44,902
Thank you.
You're so kind.

457
01:17:47,210 --> 01:17:50,702
Come look! Astronauts!

458
01:17:52,716 --> 01:17:54,513
What?

459
01:18:04,060 --> 01:18:07,120
A spaceship and astronauts.

460
01:18:08,999 --> 01:18:10,728
Unbelievable!

461
01:18:12,702 --> 01:18:15,068
Amazing! Eh, Tony?

462
01:19:27,849 --> 01:19:29,009
Listen...

463
01:19:29,217 --> 01:19:31,185
we have to hurry!

464
01:19:31,986 --> 01:19:33,749
What's going on here?

465
01:19:33,955 --> 01:19:35,946
I've had it with you, Marcel!

466
01:19:36,157 --> 01:19:38,489
All excuses and no work!

467
01:19:38,693 --> 01:19:42,356
Hey, you!
What do we owe you?

468
01:19:45,199 --> 01:19:46,723
Joke, the bill!

469
01:19:47,401 --> 01:19:49,426
Do you have our bill?

470
01:19:49,637 --> 01:19:51,229
Here it is.

471
01:20:05,353 --> 01:20:06,786
Darn it!

472
01:20:07,054 --> 01:20:09,522
Mr. Hulot, please help me.

473
01:20:18,435 --> 01:20:19,902
Two hundred.

474
01:20:27,577 --> 01:20:28,942
To clean...

475
01:20:35,485 --> 01:20:36,713
What?

476
01:20:37,354 --> 01:20:39,151
No, nothing.

477
01:20:49,299 --> 01:20:51,529
Marcel, right there.

478
01:20:53,770 --> 01:20:56,364
No, there.

479
01:20:56,573 --> 01:20:58,700
Well, which one is it?

480
01:21:17,196 --> 01:21:18,959
You've got to...

481
01:21:29,642 --> 01:21:33,408
Are we going?
Who's in charge here?

482
01:21:36,415 --> 01:21:38,042
Pull!

483
01:21:38,684 --> 01:21:40,777
Keep it coming straight.

484
01:21:41,156 --> 01:21:42,714
My God!

485
01:21:48,896 --> 01:21:51,126
Give me a hand here.

486
01:21:51,899 --> 01:21:54,265
That's really something.
You've got customers.

487
01:25:29,383 --> 01:25:33,046
- The show's over.
- Over? No!

488
01:25:35,656 --> 01:25:37,920
No, it was the 6th.

489
01:25:40,427 --> 01:25:43,555
- Here's your bill.
- Call Francois for me.

490
01:25:46,167 --> 01:25:49,466
What is this bill?
We didn't use this much electricity!

491
01:25:49,670 --> 01:25:54,073
What's the meaning of this?
300,000 francs!

492
01:25:55,910 --> 01:25:58,242
What's all this about?

493
01:25:59,246 --> 01:26:03,205
Did you light the booth day and night?
I'm talking to you!

494
01:26:03,417 --> 01:26:07,080
Get me the manager.
I've had enough of this!

495
01:26:09,390 --> 01:26:11,722
Please come with me.

496
01:26:11,926 --> 01:26:14,451
I have no intention
of paying this!

497
01:26:16,463 --> 01:26:18,624
We never even
occupied the space.

498
01:26:18,833 --> 01:26:23,600
That car is specially designed,
a product of French imagination!

499
01:26:23,804 --> 01:26:26,500
Let's find a compromise.

500
01:26:26,707 --> 01:26:29,835
You used our facilities!

501
01:26:30,044 --> 01:26:32,740
Our invention was
never even put on display!

502
01:26:34,048 --> 01:26:36,073
We can't make
an exception, sir!

503
01:26:36,283 --> 01:26:39,150
Those are our rules.
You're in the wrong.

504
01:26:39,353 --> 01:26:44,017
You read the rules
and approved the contract.

505
01:28:01,283 --> 01:28:05,014
We can work this out.
Trust me.

506
01:28:05,221 --> 01:28:07,018
I'll handle this.

507
01:28:07,223 --> 01:28:10,420
Francois, you could have
tried to help.

508
01:28:11,961 --> 01:28:13,428
Just a moment.

509
01:28:26,677 --> 01:28:28,440
Hulot!

510
01:28:28,945 --> 01:28:33,507
I'm fed up,
and you know what that means.

511
01:28:33,717 --> 01:28:38,177
Take a cab, a plane, a train...
I don't care!

512
01:28:38,388 --> 01:28:40,948
Here, take it!

513
01:28:41,158 --> 01:28:43,149
You're fired!

514
01:28:44,429 --> 01:28:46,795
Easy, ladies and gentlemen!

515
01:28:46,998 --> 01:28:48,625
Yes, we're taking orders!

516
01:28:48,833 --> 01:28:50,801
We're a big success!

517
01:29:09,389 --> 01:29:11,380
I'll walk with you.

