1
00:00:11,072 --> 00:00:20,072
Buster Keaton in "Battling Butler"

2
00:00:23,103 --> 00:00:25,703
Directed by Buster Keaton

3
00:01:57,283 --> 00:02:01,621
"Alfred, your father thinks
a hunting and fishing trip -

4
00:02:01,633 --> 00:02:05,983
- in the mountains might
do you a lot of good."

5
00:02:12,389 --> 00:02:19,177
"Yes - get a camping outfit -- go out
and rough it. Maybe it will make -

6
00:02:19,189 --> 00:02:25,989
- a man out of you if you have to
take care of yourself for a while."

7
00:02:31,204 --> 00:02:32,904
"Arrange it."

8
00:03:22,093 --> 00:03:28,081
And so Alfred went out to rough it,
leaving the city behind -

9
00:03:28,093 --> 00:03:34,093
- there was no other place to leave it.

10
00:05:33,555 --> 00:05:38,165
"There doesn't seem to be
a thing here to shoot."

11
00:06:33,052 --> 00:06:35,262
"Isn't she pretty?"

12
00:11:28,804 --> 00:11:31,554
"This is my papa and brother."

13
00:12:45,465 --> 00:12:48,765
"Have you any more brothers or fathers?"

14
00:15:36,019 --> 00:15:39,919
CHAMPION BIG FAVORITE
OVER BATTLING BUTLER

15
00:15:40,700 --> 00:15:42,533
ALFRED "BATTLING" BUTLER
New Arena Will Open Monday -

16
00:15:42,545 --> 00:15:44,390
- Large Crowd Expected,
Every Ticket Has...

17
00:15:46,705 --> 00:15:49,985
"Some prize fighter has
taken your name, sir."

18
00:15:55,790 --> 00:15:58,590
"Arrange to stop it."

19
00:16:02,762 --> 00:16:04,700
Advice to the Lovesick
by Beatrice Faircatch -

20
00:16:04,712 --> 00:16:06,662
- H.H.R. - I have outlined the following
proposal, with the answers...

21
00:16:11,287 --> 00:16:15,287
"I'd like to marry that
pretty little mountain girl."

22
00:16:17,808 --> 00:16:20,008
"Shall I arrange it?"

23
00:16:48,171 --> 00:16:51,611
"Mr. Butler would like to marry you."

24
00:16:58,906 --> 00:17:04,206
"That jellyfish couldn't take care
of himself -- let alone a wife."

25
00:17:11,427 --> 00:17:14,827
"We don't want any
weaklings in our family."

26
00:17:21,301 --> 00:17:22,501
"Weakling!

27
00:17:22,555 --> 00:17:25,305
You don't know who he is."

28
00:17:33,718 --> 00:17:35,218
CHAMPION BIG FAVORITE
OVER BATTLING BUTLER

29
00:17:35,464 --> 00:17:39,464
MR. ALFRED BUTLER
ALFRED "BATTLING" BUTLER
New Arena Will Open Monday...

30
00:17:43,938 --> 00:17:46,638
"What's he doing here - training?"

31
00:18:01,077 --> 00:18:04,115
Advice to the Lovesick
by Beatrice Faircatch -

32
00:18:04,127 --> 00:18:07,177
- H.H.R. - I have outlined the following 
proposal, with the answers you may expect:

33
00:18:07,298 --> 00:18:08,729
He - "Do you think you could learn to love me?" 
She- "Why - what do you mean?" 

34
00:18:08,741 --> 00:18:10,183
He - "I'm crazy about you." 
She- "This is so sudden."

35
00:18:10,195 --> 00:18:11,631
He - "You're the only girl I ever loved." 
She- "Give me time to think."

36
00:18:11,643 --> 00:18:13,091
He - "No, I must know now." 
She- "Have you ever proposed to any other girl?"

37
00:18:13,103 --> 00:18:14,537
He - "Don't you care for me just a little?" 
She- "I don't know."

38
00:18:14,549 --> 00:18:15,994
He - "It means everything to me." 
She- "What will father say?"

39
00:18:16,006 --> 00:18:17,445
He - "Will you marry me?" 
She- "Do you really mean it?"

40
00:18:17,457 --> 00:18:18,908
He - "Yes."   She- "Yes."
Then use your own Judgment.

41
00:19:12,747 --> 00:19:16,647
"Do you think you could learn to love me?"

42
00:19:19,018 --> 00:19:22,518
"I have."

43
00:19:39,966 --> 00:19:43,526
"When are you going to fight again?"

44
00:19:49,768 --> 00:19:52,068
"When I'm drafted."

45
00:19:55,743 --> 00:19:59,143
"I mean, who do you fight next?"

46
00:20:12,842 --> 00:20:14,242
"Yes."

47
00:20:27,175 --> 00:20:30,675
"Sir, it's time to take your liniment."

48
00:20:58,663 --> 00:21:03,751
"These people will never know
the difference - the champion -

49
00:21:03,763 --> 00:21:08,863
- will win and no one will ever
hear of Battling Butler again."

50
00:21:35,707 --> 00:21:42,217
"If you're going to fight tomorrow 
you'll have to hurry to catch the train."

51
00:21:46,582 --> 00:21:48,782
"Arrange the train."

52
00:22:26,514 --> 00:22:31,414
"In this corner we have Battling Butler."

53
00:22:40,556 --> 00:22:42,716
Butler's trainer and manager.

54
00:22:50,071 --> 00:22:51,671
"- the Lightweight Champion."

55
00:23:06,198 --> 00:23:09,138
"The Alabama Murderer
challenges the winner."

56
00:25:51,699 --> 00:25:55,199
"I'm champion."

57
00:26:00,700 --> 00:26:05,500
"That means you cannot go
back to the mountains, sir."

58
00:26:07,462 --> 00:26:15,262
"You're wrong, I'm going back and tell her
the truth. I'd rather lose her that way."

59
00:27:01,519 --> 00:27:05,219
WELCOME BATTLING BUTLER

60
00:27:33,432 --> 00:27:35,832
WELCOME BATTLING BUTLER

61
00:27:57,400 --> 00:28:02,100
WELCOME BATTLING BUTLER
World's Lightweight Champion

62
00:30:39,616 --> 00:30:45,004
BATTLING BUTLER TO FIGHT ALABAMA MURDERER
New Champ to Defend Title Thanksgiving -

63
00:30:45,016 --> 00:30:50,416
Starts Training at Silver Hot Springs tomorrow

64
00:30:55,306 --> 00:30:58,806
"That means you have to leave right away."

65
00:31:02,950 --> 00:31:04,550
"Arrange it."

66
00:31:08,872 --> 00:31:11,272
"I'll hurry and pack my things."

67
00:31:19,120 --> 00:31:21,880
"I'm sorry, but you can't go."

68
00:31:27,229 --> 00:31:31,667
"I want you to know me as I
am -- not as the brutal, -

69
00:31:31,679 --> 00:31:36,129
- blood-thirsty beast that
I am when fighting."

70
00:31:38,169 --> 00:31:45,069
"Promise me you will never come near
the training camp or to see me fight."

71
00:32:26,035 --> 00:32:28,835
"Where do we go from here?"

72
00:32:31,006 --> 00:32:34,889
"We'd better go to Butler's
training camp, sir, -

73
00:32:34,901 --> 00:32:38,796
- so that you can answer
her letters from there."

74
00:32:44,534 --> 00:32:51,134
SILVER HOT SPRINGS
Hotel Register
Alfred Battling Butler & Wife.

75
00:33:30,033 --> 00:33:32,233
NO ADMITTANCE

76
00:33:37,312 --> 00:33:43,212
"Drive carefully. These country folks
may not be used to city speed."

77
00:36:08,788 --> 00:36:11,908
Alfred Battling Butler & Wife.
Alfred Butler and Man.

78
00:36:39,351 --> 00:36:41,351
NO ADMITTANCE

79
00:36:57,238 --> 00:36:58,888
"How's your heel?"

80
00:37:01,001 --> 00:37:02,461
"He's all right."

81
00:38:03,923 --> 00:38:07,263
"Let's go out for a little road work."

82
00:39:37,529 --> 00:39:38,999
Eight miles later.

83
00:40:02,754 --> 00:40:09,154
"I'm glad you came to see me --
now you must go right back home."

84
00:40:30,605 --> 00:40:37,005
"I won't go home! I'm going to stay
right here and help you train."

85
00:42:38,819 --> 00:42:41,099
"Compliments of Mr. Butler."

86
00:43:40,797 --> 00:43:44,267
"That woman says she's
married to you, too."

87
00:44:05,707 --> 00:44:11,760
"-- so you see, sir, it would wreck
his home if she knew the truth. -

88
00:44:11,772 --> 00:44:17,837
- Won't you be a good
fellow and fix it for him."

89
00:44:43,602 --> 00:44:48,430
"My dear, we are not the only
Butlers here. This is -

90
00:44:48,442 --> 00:44:53,282
- the Lightweight Champion, Mr.
Battling Butler, and his wife."

91
00:45:25,224 --> 00:45:28,104
"Don't forget your date Thanksgiving."

92
00:45:32,429 --> 00:45:34,129
"What date?"

93
00:45:35,982 --> 00:45:38,182
"With the Alabama Murderer."

94
00:45:50,383 --> 00:45:55,521
"If he wants to be Battling Butler
let him fight the Alabama Murderer, -

95
00:45:55,533 --> 00:46:00,683
- and he'll never flirt
with anybody else's wife."

96
00:46:05,695 --> 00:46:10,295
"I'm through - get him in
shape the best you can."

97
00:47:15,181 --> 00:47:19,781
"Don't you think it advisable,
sir, to give this up?"

98
00:47:21,673 --> 00:47:28,173
"No, Martin, I'd rather be killed by the
Alabama Murderer than have her know."

99
00:48:42,319 --> 00:48:44,529
"Put up your hands."

100
00:58:44,193 --> 00:58:52,963
By dinner time the only thing Alfred
had on his mind was his stomach.

101
00:59:37,885 --> 00:59:45,153
And so for three weeks Alfred trained
and starved and suffered and ached -

102
00:59:45,165 --> 00:59:52,445
- and bled and groaned and hoped and
prayed and - and then came the storm.

103
00:59:53,453 --> 00:59:56,416
OLYMPIC Auditorium 
THANKSGIVING NIGHT NOV. 26, 1925

104
00:59:56,428 --> 00:59:59,403
CHAMPIONSHIP BOUT
BATTLING BUTLER vs. ALABAMA MURDERER
15 ROUNDS

105
01:00:53,179 --> 01:00:59,579
"I persuaded her to stay at the hotel
- so she won't see you fight."

106
01:02:44,988 --> 01:02:49,278
"I thought it best to prepare
for the worst, sir."

107
01:04:11,628 --> 01:04:14,908
"See that she gets a good seat."

108
01:04:22,409 --> 01:04:29,739
"I hope you win -- father and brother wrote
that they've bet all their money on you."

109
01:05:12,330 --> 01:05:13,830
"Butler!"

110
01:05:13,884 --> 01:05:15,424
"BUTLER!"

111
01:05:35,801 --> 01:05:38,571
"That was Battling Butler - he fought!"

112
01:05:46,706 --> 01:05:52,276
"You didn't think we'd throw away a
championship just to get even with you."

113
01:06:32,333 --> 01:06:34,333
"Thanks - for saving me."

114
01:06:39,614 --> 01:06:42,814
"I've been saving you for three weeks."

115
01:10:11,377 --> 01:10:13,577
"That was Battling Butler."

116
01:10:15,873 --> 01:10:19,573
"I lied to you -- I'm not even a fighter."

117
01:10:36,304 --> 01:10:38,040
"I'm glad."

118
01:11:12,033 --> 01:11:14,333
THE END
Subtitle by Reza Fa
V. 2
