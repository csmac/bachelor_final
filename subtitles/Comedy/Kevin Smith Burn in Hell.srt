﻿1
00:00:06,192 --> 00:00:08,505
So, how's your gig going
down at the retirement home?

2
00:00:08,530 --> 00:00:09,846
Oh, it's crazy.

3
00:00:09,871 --> 00:00:11,737
This one old guy thinks
I'm his son-in-law.

4
00:00:12,676 --> 00:00:14,237
The other day, he took a swing at me.

5
00:00:14,376 --> 00:00:16,856
Missed me by like six feet,
fell into another guy.

6
00:00:16,881 --> 00:00:18,859
They shattered like
a bag of light bulbs.

7
00:00:20,214 --> 00:00:22,700
Yeah, my mom's getting fragile, too.

8
00:00:22,961 --> 00:00:24,610
I think this is gonna be the last winter

9
00:00:24,635 --> 00:00:26,186
I let her shovel the driveway.

10
00:00:28,722 --> 00:00:30,221
Well, I should get going.

11
00:00:30,358 --> 00:00:32,492
Didi's got me volunteering
at a soup kitchen.

12
00:00:32,542 --> 00:00:33,994
They make me wear a hairnet.

13
00:00:34,019 --> 00:00:35,433
A hairnet.

14
00:00:36,742 --> 00:00:38,308
Oh, by... by the way, guys, I got this.

15
00:00:38,333 --> 00:00:39,658
- Don't worry about it.
- Can't help you.

16
00:00:39,683 --> 00:00:41,786
I'm into a couple of Girl
Scouts for a lot of cookies.

17
00:00:41,811 --> 00:00:43,274
All right.

18
00:00:47,359 --> 00:00:48,601
Jimmy?

19
00:00:49,010 --> 00:00:50,528
- Hey!
- Hey!

20
00:00:50,561 --> 00:00:53,008
How are you, buddy?! Good to see you!

21
00:00:53,033 --> 00:00:54,137
What?

22
00:00:54,405 --> 00:00:57,928
I haven't seen you
since you retired, man.

23
00:00:57,953 --> 00:00:59,668
You look amazing. Look at you.

24
00:00:59,693 --> 00:01:01,379
Get out of here. What
have you been up to?

25
00:01:01,404 --> 00:01:02,897
Uh, you know, this and that.

26
00:01:02,922 --> 00:01:03,850
Don't be modest.

27
00:01:03,875 --> 00:01:06,743
I see the Soaring Eagle Security shirt.

28
00:01:06,768 --> 00:01:09,228
You're the guys who took down bin Laden,
right?

29
00:01:10,899 --> 00:01:11,836
All right.

30
00:01:11,861 --> 00:01:13,643
I know. No, no, I get it.
You can't talk about it.

31
00:01:13,668 --> 00:01:15,307
You're very funny, but you know what?

32
00:01:15,332 --> 00:01:16,758
I only got to work one day a week.

33
00:01:16,783 --> 00:01:18,368
So while you're out
patrolling the streets,

34
00:01:18,393 --> 00:01:20,240
now I'm sitting home in front of the TV

35
00:01:20,265 --> 00:01:21,747
interrogating a bag of Fritos.

36
00:01:21,772 --> 00:01:23,779
Oh. Ooh, that sounds good.

37
00:01:23,804 --> 00:01:25,012
Without pants.

38
00:01:25,508 --> 00:01:27,526
Sounds good with a side of weird.

39
00:01:27,551 --> 00:01:28,686
Yeah.

40
00:01:28,711 --> 00:01:30,349
We had some good times, didn't we?

41
00:01:30,374 --> 00:01:32,957
It was great, man. 10 years together.

42
00:01:32,982 --> 00:01:35,661
That squad car's got to feel a
little lonely without me, huh?

43
00:01:35,686 --> 00:01:37,501
Yeah, well, you know, I get by.

44
00:01:37,526 --> 00:01:39,493
Yo, Jimmy, let's roll, man.

45
00:01:39,518 --> 00:01:41,823
Hey, this is my new
partner, Nicky Dawson.

46
00:01:41,848 --> 00:01:44,453
Nick, this is my old
partner, Kevin Gable.

47
00:01:44,651 --> 00:01:46,285
I'll go get the food.

48
00:01:46,310 --> 00:01:48,278
I'm the guy.

49
00:01:48,433 --> 00:01:51,766
Now you got a face to go with
all the stories and stuff, so...

50
00:01:51,791 --> 00:01:53,391
Stories?

51
00:01:53,596 --> 00:01:55,902
Christmas Eve,
all the motorcycles on the roof?

52
00:01:55,927 --> 00:01:56,787
That was me.

53
00:01:57,226 --> 00:01:58,331
No.

54
00:01:58,907 --> 00:02:01,420
How about breaking up
the burglary at, uh...

55
00:02:01,445 --> 00:02:02,892
At Galaxy Bagels?

56
00:02:02,917 --> 00:02:03,979
No.

57
00:02:04,592 --> 00:02:06,859
Me chasing that crazy
deer through the mall?

58
00:02:06,884 --> 00:02:08,317
It was epic. Are you kidding me?

59
00:02:08,342 --> 00:02:10,046
That sounds awesome, but no.

60
00:02:11,159 --> 00:02:13,059
It was awesome.

61
00:02:13,084 --> 00:02:15,217
But he didn't tell
you anything about me?

62
00:02:15,242 --> 00:02:16,877
What,
is this your first day working with him?

63
00:02:16,902 --> 00:02:18,917
No, uh, it's been a few months now,

64
00:02:18,942 --> 00:02:20,115
Uh, since the last guy retired.

65
00:02:20,147 --> 00:02:22,321
Yeah, that's me.
That's the guy he's talking about.

66
00:02:22,660 --> 00:02:24,547
Oh, wait a sec. I know who you are.

67
00:02:24,572 --> 00:02:26,643
You're the guy who shot
2-under at Fox Creek.

68
00:02:26,963 --> 00:02:28,336
That was Rodriguez.

69
00:02:28,361 --> 00:02:29,594
Oh.

70
00:02:29,619 --> 00:02:31,440
He sounds awesome.

71
00:02:34,250 --> 00:02:35,867
Hey, babe.

72
00:02:35,892 --> 00:02:36,991
Hey, how was your day?

73
00:02:37,016 --> 00:02:37,917
It was all right.

74
00:02:37,942 --> 00:02:41,285
I, uh... I ran into
Jimmy Landers at Enzo's.

75
00:02:41,310 --> 00:02:42,902
Oh, I love Jimmy. How's Jimmy?

76
00:02:42,927 --> 00:02:43,770
Ooh.

77
00:02:43,795 --> 00:02:45,826
Um, he's good, you know.

78
00:02:45,851 --> 00:02:47,778
He, uh, got a new partner.

79
00:02:47,803 --> 00:02:49,292
A rookie.

80
00:02:49,317 --> 00:02:51,357
Oh, yikes. That's got
to be rough for him.

81
00:02:51,382 --> 00:02:52,406
You'd think, yeah.

82
00:02:52,431 --> 00:02:56,069
But, uh... seems to be working
it out, though, you know?

83
00:02:56,199 --> 00:02:57,652
- Let me ask you a question.
- Okay.

84
00:02:57,677 --> 00:02:58,931
You remember Teddy O'Boyle?

85
00:02:58,956 --> 00:03:00,275
Yeah, he was your first partner.

86
00:03:00,300 --> 00:03:01,348
Exactly.

87
00:03:01,374 --> 00:03:02,906
Okay, that was 20 years ago.

88
00:03:02,931 --> 00:03:05,477
I-I was with the guy
for less than a year,

89
00:03:05,502 --> 00:03:06,714
and you know about him.

90
00:03:06,739 --> 00:03:08,547
Well, yeah,
but he was up on corruption charges,

91
00:03:08,572 --> 00:03:09,740
so he was in the news a lot.

92
00:03:09,765 --> 00:03:11,812
That's not the point.
Okay, I told you about Teddy.

93
00:03:11,837 --> 00:03:13,236
Jimmy never mentions me.

94
00:03:13,362 --> 00:03:15,599
This new guy, he already took
him to his dad's country club

95
00:03:15,624 --> 00:03:16,919
and got him his own locker.

96
00:03:16,944 --> 00:03:19,528
I had to change my shorts
in the parking lot.

97
00:03:19,553 --> 00:03:21,891
Oh, I've seen that.
That's a rough 90 seconds.

98
00:03:22,757 --> 00:03:24,623
Um...

99
00:03:24,812 --> 00:03:27,035
well, so, I mean, are you jealous?

100
00:03:27,242 --> 00:03:29,507
I'm not jealous.
Guys don't get jealous of other guys.

101
00:03:29,532 --> 00:03:31,289
- Oh.
- You know, we're not emotional.

102
00:03:31,314 --> 00:03:32,666
It's just I don't know
why he would do this.

103
00:03:32,691 --> 00:03:33,929
I just...

104
00:03:35,858 --> 00:03:38,430
Okay. Well, since we've
ruled out jealousy,

105
00:03:38,455 --> 00:03:40,684
um, what is it?

106
00:03:40,709 --> 00:03:42,397
It's just weird, you know?

107
00:03:42,422 --> 00:03:45,112
I mean, I was with the guy all the time.

108
00:03:45,137 --> 00:03:46,545
You know, we talked about everything.

109
00:03:46,570 --> 00:03:48,170
It's the most important
relationship of my life.

110
00:03:48,195 --> 00:03:48,931
- And...
- Hmm.

111
00:03:48,956 --> 00:03:51,606
Except for you.
I knew it as I was saying it.

112
00:03:52,580 --> 00:03:53,625
Okay.

113
00:03:53,650 --> 00:03:56,695
Well, can I offer you
some advice, you know,

114
00:03:56,975 --> 00:03:58,846
as your second-most-important?

115
00:03:58,871 --> 00:04:02,173
Okay, see, that right there...
That's why he's number 1.

116
00:04:02,198 --> 00:04:02,927
It's that attitude.

117
00:04:02,952 --> 00:04:04,551
Look, babe, I know it hurts.

118
00:04:04,591 --> 00:04:07,313
But it's probably just out of sight,
out of mind.

119
00:04:07,465 --> 00:04:09,352
You know, you got to
get back on his radar.

120
00:04:09,377 --> 00:04:10,832
You should call him.

121
00:04:10,857 --> 00:04:12,436
I was just with him this afternoon.

122
00:04:12,461 --> 00:04:14,303
I can't call him now.
It's gonna look desperate.

123
00:04:14,328 --> 00:04:15,641
Well, then you should do the thing

124
00:04:15,666 --> 00:04:17,729
where you pretend to call someone else,

125
00:04:17,754 --> 00:04:20,688
but you wind up calling
him "by mistake."

126
00:04:22,097 --> 00:04:24,485
That actually could work. Okay, uh...

127
00:04:24,510 --> 00:04:27,010
You know what?
I will just pretend to call Duffy.

128
00:04:27,035 --> 00:04:28,049
- Okay.
- That's what I'll do

129
00:04:28,074 --> 00:04:29,929
and, uh... here we go.

130
00:04:33,016 --> 00:04:35,042
Hey, Duff. What...

131
00:04:35,335 --> 00:04:36,534
Uh, never mind.

132
00:04:37,142 --> 00:04:38,745
I actually called Duffy.

133
00:04:43,458 --> 00:04:44,986
Hey, Duff, it's Kev.

134
00:04:45,011 --> 00:04:47,004
I think you dialed the
wrong number, buddy.

135
00:04:47,029 --> 00:04:48,262
It's... It's Jimmy.

136
00:04:48,287 --> 00:04:49,948
Oh, man, my bad.

137
00:04:49,973 --> 00:04:52,668
My stupid fingers all over the phone.

138
00:04:53,008 --> 00:04:56,501
Uh, anyway, uh,
it was great seeing you today.

139
00:04:56,526 --> 00:04:57,581
Great seeing you, too.

140
00:04:57,606 --> 00:04:59,558
Listen, we're about
to raid a crack house.

141
00:04:59,583 --> 00:05:02,366
So, uh, I might have
to jump in a second.

142
00:05:02,391 --> 00:05:03,847
Oh... Oh, okay, ju... Real quick.

143
00:05:03,872 --> 00:05:04,632
I was just wondering if you wanted

144
00:05:04,657 --> 00:05:06,528
to get something on the books for,
like, tomorrow.

145
00:05:07,283 --> 00:05:09,357
Yeah, yeah, yeah. Sounds good.

146
00:05:09,382 --> 00:05:10,793
Uh, what do you got in mind?

147
00:05:10,818 --> 00:05:13,928
Uh, you know, something
fun like, you know,

148
00:05:13,953 --> 00:05:17,546
just ch... chopping down a
tree or something, you know?

149
00:05:17,948 --> 00:05:20,098
Uh, are we gonna go
log rolling after that?

150
00:05:20,123 --> 00:05:21,646
What are you talking about?

151
00:05:21,819 --> 00:05:23,187
Petting... Or petting cages.

152
00:05:23,212 --> 00:05:24,978
We can go to p... Batting cages.

153
00:05:25,003 --> 00:05:26,366
We can go to batting cages.

154
00:05:26,391 --> 00:05:27,453
Okay, yeah.

155
00:05:27,478 --> 00:05:29,029
Anyways, I better go.

156
00:05:29,054 --> 00:05:30,988
Okay, man, I'll... I'll, uh...
I'll just see you tomorrow.

157
00:05:31,013 --> 00:05:32,479
Okay, sounds good, bro.

158
00:05:32,524 --> 00:05:35,276
Nick, come on! Save
some crackheads for me!

159
00:05:36,507 --> 00:05:38,053
Who taught you to hold a bat?

160
00:05:38,078 --> 00:05:39,588
What is this? Hands together!

161
00:05:39,613 --> 00:05:41,540
Oh, okay, well, I guess never learned.

162
00:05:41,565 --> 00:05:44,144
I must have been too busy
at the petting cages.

163
00:05:50,879 --> 00:05:52,913
Okay, I am not a pro,
but I think you got

164
00:05:52,938 --> 00:05:55,110
to swing the bat in
order to hit the ball.

165
00:05:55,135 --> 00:05:57,000
Just getting my timing down, guy.

166
00:05:57,025 --> 00:05:57,642
Okay.

167
00:05:58,824 --> 00:06:00,290
All right, I just saw the problem.

168
00:06:00,315 --> 00:06:01,395
I can solve this.

169
00:06:01,420 --> 00:06:03,576
You... You just suck.

170
00:06:03,831 --> 00:06:05,364
All right, big shot.
Let's see what you got.

171
00:06:05,389 --> 00:06:06,750
All right, you want me to go?

172
00:06:06,775 --> 00:06:07,974
I'll go.

173
00:06:08,523 --> 00:06:11,311
You know who would love this? Nick.

174
00:06:11,336 --> 00:06:12,769
Yeah, but you know what
it is with big guys.

175
00:06:12,794 --> 00:06:14,345
Honestly,
it's the hand-and-eye coordination.

176
00:06:14,370 --> 00:06:15,623
They don't usually have that.

177
00:06:15,648 --> 00:06:17,503
Actually, he's not here because he's at

178
00:06:17,528 --> 00:06:19,095
a children's hospital right now.

179
00:06:19,120 --> 00:06:20,873
He's entertaining the kids with this...

180
00:06:20,898 --> 00:06:22,808
This superhero character he does...

181
00:06:22,833 --> 00:06:24,143
Police Man.

182
00:06:25,086 --> 00:06:26,336
Police Man?

183
00:06:26,361 --> 00:06:27,782
How long it take him to
come up with that one?

184
00:06:27,807 --> 00:06:30,688
Yeah, one of the sick
kids named him that.

185
00:06:32,405 --> 00:06:34,324
It's actually pretty good.
I mean, it gets to the point.

186
00:06:34,349 --> 00:06:35,300
It's good.

187
00:06:35,409 --> 00:06:37,385
By the way, nice gloves.
Where'd you get those?

188
00:06:37,450 --> 00:06:40,085
Actually, uh, Nicky boy got them for me.

189
00:06:40,110 --> 00:06:42,041
Talk about buying a friend.

190
00:06:43,169 --> 00:06:45,312
Wow. Is somebody a little threatened

191
00:06:45,337 --> 00:06:47,372
by the new man in my life?

192
00:06:48,425 --> 00:06:51,034
No, I just got a weird
vibe about this guy.

193
00:06:52,181 --> 00:06:55,107
Hey, I was thinking...
I'm doing a charity event

194
00:06:55,132 --> 00:06:56,732
where I have to give a speech about

195
00:06:56,757 --> 00:06:59,557
people in your life that inspire you.

196
00:06:59,729 --> 00:07:01,667
I would love for you to be there.

197
00:07:01,814 --> 00:07:04,059
Really? I mean, yeah.
I... I think I can go.

198
00:07:04,084 --> 00:07:05,612
I need you to be there.

199
00:07:05,867 --> 00:07:07,394
Yeah, I'm there.

200
00:07:07,779 --> 00:07:09,955
Hey! You guys are still here.

201
00:07:09,980 --> 00:07:10,988
Big boy!

202
00:07:11,246 --> 00:07:12,939
You guys mind if I get a couple swings?

203
00:07:12,964 --> 00:07:14,230
Hit it, baby.

204
00:07:14,255 --> 00:07:17,044
Need some, uh... Need
some batting gloves?

205
00:07:17,069 --> 00:07:18,827
Uh, no, I'm good.

206
00:07:33,157 --> 00:07:35,085
Yeah, it's pretty self-evident that

207
00:07:35,110 --> 00:07:38,091
the, uh,
kid's got some hand-eye coordination.

208
00:07:38,499 --> 00:07:41,547
Yeah, he's okay, but,
uh, I got to tell you...

209
00:07:41,572 --> 00:07:43,667
Okay, that one went through
the cage right there.

210
00:07:43,928 --> 00:07:45,551
So, we should be back by 10:00

211
00:07:45,576 --> 00:07:47,653
unless we decide to go
out for a beer after.

212
00:07:47,763 --> 00:07:49,204
Okay, well, don't worry about me.

213
00:07:49,229 --> 00:07:50,644
It's your night. Have fun.

214
00:07:51,570 --> 00:07:53,980
Hey, what do you think?

215
00:07:54,466 --> 00:07:57,151
Um, well, you look
nice, but I think you...

216
00:07:57,176 --> 00:07:58,530
I think you should untuck your shirt.

217
00:07:59,332 --> 00:08:00,359
I can't untuck.

218
00:08:00,384 --> 00:08:02,248
Once I tuck, I-I'm tucked, that's it.

219
00:08:02,785 --> 00:08:04,809
I untuck now, it'll be smooth up top,

220
00:08:04,834 --> 00:08:06,747
and the bottom will look
like a Japanese fan.

221
00:08:09,337 --> 00:08:10,568
Then you're tucked.

222
00:08:10,640 --> 00:08:12,264
- Okay.
- Uh, no, you look fine.

223
00:08:12,289 --> 00:08:13,808
I don't want fine, okay?

224
00:08:13,833 --> 00:08:16,330
It's like...
Look, you don't even know me, right?

225
00:08:16,355 --> 00:08:17,786
You're out with your girlfriends,

226
00:08:17,811 --> 00:08:19,858
and you just see me, and I'm,
like, across the room.

227
00:08:19,884 --> 00:08:21,587
- I'm like, "What's up?"
- Okay.

228
00:08:21,612 --> 00:08:23,242
What are you thinking?
Just what comes to mind?

229
00:08:23,267 --> 00:08:25,554
Uh, well, I'm wondering why your
thumbs are in your belt loop,

230
00:08:25,579 --> 00:08:27,561
and then I wonder why
me and my girlfriends

231
00:08:27,586 --> 00:08:28,872
are at a truck stop.

232
00:08:31,749 --> 00:08:32,982
Different scenario.

233
00:08:33,007 --> 00:08:34,816
No, you...
you're out at the club, all right?

234
00:08:34,841 --> 00:08:36,818
And I walk across the
dance floor, right?

235
00:08:36,843 --> 00:08:38,817
Music's thumping, and I'm just like,

236
00:08:38,842 --> 00:08:41,409
"Hey, what's up, girl?"

237
00:08:41,656 --> 00:08:44,912
And you just see me from
behind all dressed in purple,

238
00:08:44,937 --> 00:08:46,163
- and he just gives you one of those.
- Oh.

239
00:08:46,188 --> 00:08:47,688
What happens then?

240
00:08:47,713 --> 00:08:48,961
Quick... what comes to your mind?

241
00:08:48,986 --> 00:08:51,281
Um, uh, retired plumber going
for dinner on a cruise ship.

242
00:08:51,306 --> 00:08:52,648
No. Boy!

243
00:08:52,673 --> 00:08:54,286
I don't know what you want me to say.

244
00:08:54,311 --> 00:08:56,438
Look, I'm just thinking,
I don't know, more like, uh,

245
00:08:56,463 --> 00:08:58,934
ex-athlete who blew out his knee and,
you know,

246
00:08:58,960 --> 00:09:01,060
still has some muscle through
the shirt you can kind of see,

247
00:09:01,085 --> 00:09:02,280
like, "Whoa, look at the ripples."

248
00:09:02,305 --> 00:09:04,389
- Oh, okay. Yeah.
- Right?

249
00:09:04,414 --> 00:09:05,880
No, yeah, I could see that.

250
00:09:05,905 --> 00:09:08,489
I mean, and then...
Oh, and maybe you, uh,

251
00:09:08,514 --> 00:09:10,537
tried out once for a
semi-pro football team.

252
00:09:10,562 --> 00:09:11,728
Yes, that's perfect. Keep going.

253
00:09:11,753 --> 00:09:13,514
But your knee never healed right,

254
00:09:13,539 --> 00:09:15,739
and then you packed on a few,
and now you drive a limo.

255
00:09:16,932 --> 00:09:18,726
You just like to dig, don't you?

256
00:09:18,969 --> 00:09:21,016
I'm sorry. You look nice.

257
00:09:21,041 --> 00:09:23,932
You do, but you should put on a jacket.

258
00:09:23,957 --> 00:09:25,172
Jacket? Why?

259
00:09:25,196 --> 00:09:28,048
Yeah, well, you said Jimmy
was doing a speech, right,

260
00:09:28,073 --> 00:09:30,119
about people who inspire him.

261
00:09:30,406 --> 00:09:32,519
What if he calls you up? Huh?

262
00:09:32,924 --> 00:09:34,273
You know, you might be right.

263
00:09:34,298 --> 00:09:35,026
Yeah.

264
00:09:35,051 --> 00:09:36,513
He did say he needs me there.

265
00:09:36,538 --> 00:09:37,287
Needs you.

266
00:09:37,312 --> 00:09:38,975
Wow. All right, quick impression...

267
00:09:39,000 --> 00:09:39,934
- What do you see when you see me?
- Okay.

268
00:09:39,959 --> 00:09:41,985
- Right now, go, boom.
- Uh, small-town minister.

269
00:09:42,010 --> 00:09:45,040
I love you so much.

270
00:09:50,420 --> 00:09:51,395
Hey, Father Philip.

271
00:09:51,420 --> 00:09:53,086
Sorry I'm late. Parking was crazy.

272
00:09:53,111 --> 00:09:55,155
They got me way down by the Wendy's.

273
00:09:56,314 --> 00:09:57,815
Tucked in, huh?

274
00:09:57,967 --> 00:10:00,239
Yes. Is that... Is that all right?

275
00:10:00,264 --> 00:10:01,494
- Good look or...
- No, it's fine.

276
00:10:01,519 --> 00:10:03,748
It's just...
You know, it's surprising considering

277
00:10:03,773 --> 00:10:06,763
when you came to midnight
mass with your Jets jersey.

278
00:10:07,956 --> 00:10:09,055
Hey.

279
00:10:09,314 --> 00:10:11,337
"Have you met Jimmy Lander" new partner?

280
00:10:11,362 --> 00:10:12,616
What a great guy?

281
00:10:12,641 --> 00:10:14,104
Mm, he's okay.

282
00:10:14,129 --> 00:10:15,240
Okay?

283
00:10:15,265 --> 00:10:17,511
He donated an altar rail to the church.

284
00:10:17,536 --> 00:10:20,270
Built it himself out
of Spanish driftwood.

285
00:10:22,708 --> 00:10:25,808
All right, everybody,
thank you so much for coming out.

286
00:10:28,025 --> 00:10:29,933
So, what is a hero?

287
00:10:32,065 --> 00:10:33,998
Hey, Father Philip, when I go up there,

288
00:10:34,023 --> 00:10:35,905
is that second microphone
already on or...

289
00:10:35,930 --> 00:10:38,764
Actually, I... I think
I'm going up, so...

290
00:10:38,789 --> 00:10:40,188
Well, he asked me to be here.

291
00:10:40,213 --> 00:10:42,808
He actually said he needed
me to be here, so...

292
00:10:42,833 --> 00:10:44,304
Oh. Oh, that's cool.

293
00:10:44,329 --> 00:10:45,912
Maybe he's gonna ask
both of us to go up.

294
00:10:45,937 --> 00:10:47,674
Yeah, maybe. Or just me, either way.

295
00:10:47,699 --> 00:10:50,361
And that's what I'm here to talk about.

296
00:10:50,768 --> 00:10:52,181
Heroes.

297
00:10:52,206 --> 00:10:53,569
Look at me... I'm tearing up.

298
00:10:53,594 --> 00:10:55,180
I said I wasn't gonna do that.

299
00:10:55,205 --> 00:10:57,224
And... And here I am.

300
00:10:59,337 --> 00:11:01,232
I know a lot about heroes

301
00:11:01,257 --> 00:11:03,334
because I've been fortunate enough

302
00:11:03,359 --> 00:11:06,334
to have one ride right next to me.

303
00:11:06,440 --> 00:11:08,989
Do me a favor. Just, uh, hold my stuff.

304
00:11:09,014 --> 00:11:11,156
I don't want my pockets looking
bulky when I'm up there, okay?

305
00:11:11,181 --> 00:11:12,692
Mm. Warm lip balm.

306
00:11:12,718 --> 00:11:15,019
So please help me welcome

307
00:11:15,044 --> 00:11:19,212
a man who knows the
true meaning of loyalty.

308
00:11:19,237 --> 00:11:20,337
This is it.

309
00:11:20,362 --> 00:11:21,900
An incredible human being.

310
00:11:21,925 --> 00:11:23,149
Wow.

311
00:11:23,174 --> 00:11:25,366
A fine-looking gentleman.

312
00:11:25,391 --> 00:11:26,319
Oh, thank you.

313
00:11:26,344 --> 00:11:29,292
An amazing, amazing partner.

314
00:11:31,487 --> 00:11:33,341
- Give it up for the one...
- Yeah.

315
00:11:33,366 --> 00:11:34,809
- And only...
- Here we go.

316
00:11:34,834 --> 00:11:36,272
Nick Dawson.

317
00:11:36,297 --> 00:11:38,472
Uh, y-you go first. I just...
I'll come up in a second.

318
00:11:41,194 --> 00:11:42,584
You want your stuff?

319
00:11:42,609 --> 00:11:44,170
No, no, no, Father, I'm... I'm going up.

320
00:11:44,195 --> 00:11:46,008
Just he's bringing him first.

321
00:11:46,302 --> 00:11:48,501
Hey, I don't think so.

322
00:11:48,666 --> 00:11:51,556
This is kind of their moment.

323
00:11:51,735 --> 00:11:54,869
And that's a good thing
'cause humility...

324
00:11:54,894 --> 00:11:57,399
Father, excuse me.
I'm sorry. I... I just can't hear him.

325
00:11:57,534 --> 00:11:59,140
It's the middle of the night.

326
00:11:59,165 --> 00:12:00,654
We're on this call. Is it dangerous?

327
00:12:00,679 --> 00:12:03,173
Of course it's dangerous.
You're terrified.

328
00:12:03,337 --> 00:12:05,620
It's your life you're thinking about!

329
00:12:06,592 --> 00:12:10,763
But you got backup knowing
this man is next to you.

330
00:12:10,788 --> 00:12:12,173
You understand me?

331
00:12:12,198 --> 00:12:14,213
The only thing running
through my mind...

332
00:12:14,394 --> 00:12:16,836
How do I get you back to your family?

333
00:12:16,861 --> 00:12:18,853
Did you hear this boy?!

334
00:12:19,397 --> 00:12:21,185
Hey, that was nice.

335
00:12:21,210 --> 00:12:23,444
Sorry. It just, uh,
felt a little written to me.

336
00:12:23,469 --> 00:12:25,589
That's all I'm saying.

337
00:12:25,614 --> 00:12:28,230
I'm just kind of more of an
off-the-cuff type of guy.

338
00:12:28,255 --> 00:12:30,303
But, uh, I would just like
to say first off, everybody,

339
00:12:30,328 --> 00:12:32,762
hello, and what a great
turnout here at St. Rocco's.

340
00:12:33,818 --> 00:12:36,412
This is my ex-partner, Kevin Gable.

341
00:12:36,437 --> 00:12:38,975
Yeah, of, uh, 10 years,
we road together, right?

342
00:12:39,000 --> 00:12:39,932
I mean, you guys have
been riding together,

343
00:12:39,957 --> 00:12:42,076
what, three months now?

344
00:12:42,635 --> 00:12:44,099
How do you... How do you know?

345
00:12:44,124 --> 00:12:47,414
Yeah, thanks, Kevin,
for coming and squeezing me in

346
00:12:47,439 --> 00:12:48,763
in between go-kart rides.

347
00:12:48,788 --> 00:12:50,490
That was very, very cool of you.

348
00:12:50,515 --> 00:12:51,814
So, you're not a cop anymore?

349
00:12:51,839 --> 00:12:52,564
No, no, no, no.

350
00:12:52,589 --> 00:12:54,196
I am, uh...
Once you're a cop, you're always a cop.

351
00:12:54,221 --> 00:12:55,935
I'm just retired. That's all.

352
00:12:55,959 --> 00:12:57,486
Then why'd you tuck in?

353
00:12:57,571 --> 00:12:59,727
It's not the best look for you.

354
00:13:00,558 --> 00:13:03,023
Uh, I really don't think
this is about wardrobe.

355
00:13:03,048 --> 00:13:05,909
It's more about being a hero.
That's what it's... Okay?

356
00:13:05,934 --> 00:13:07,518
Can you arrest people?

357
00:13:07,737 --> 00:13:08,846
Technically, no.

358
00:13:08,871 --> 00:13:11,998
Uh, I certainly have had my
share of arrests in the day.

359
00:13:12,022 --> 00:13:15,737
Um, right? I actually arrested
one lady not too far from here.

360
00:13:15,762 --> 00:13:20,439
She was a, uh... Boy, how do I
say this without sounding judgy?

361
00:13:20,984 --> 00:13:23,278
She was a prostitute.

362
00:13:23,302 --> 00:13:26,109
She was. She was, uh...

363
00:13:27,002 --> 00:13:29,974
She was a lady of the
night. It's out there.

364
00:13:30,296 --> 00:13:33,029
But, uh, and not
top-shelf, either, right?

365
00:13:33,054 --> 00:13:34,165
She was not, I mean...

366
00:13:34,190 --> 00:13:36,157
Looked a lot like Larry Bird.

367
00:13:37,500 --> 00:13:39,367
Oh, boy.

368
00:13:45,168 --> 00:13:46,668
Hey.

369
00:13:47,037 --> 00:13:48,136
How'd it go?

370
00:13:52,328 --> 00:13:55,044
I think total humiliation sums it up.

371
00:13:55,955 --> 00:13:58,460
How'd the tuck-in go?

372
00:14:01,906 --> 00:14:05,813
I don't know how great
this new partner is.

373
00:14:05,838 --> 00:14:07,829
He sounds pretty great, but...

374
00:14:09,003 --> 00:14:11,278
that doesn't mean that you're not, too.

375
00:14:11,446 --> 00:14:14,638
You know, and if Jimmy doesn't see that,
then that is his loss.

376
00:14:15,490 --> 00:14:17,434
You always know what to say.

377
00:14:17,605 --> 00:14:20,603
That's...
Right there, that's why I love you.

378
00:14:20,628 --> 00:14:22,875
That's why I married you.

379
00:14:23,199 --> 00:14:25,933
You're the most important
relationship in my life.

380
00:14:25,958 --> 00:14:27,699
Aww.

381
00:14:27,724 --> 00:14:29,836
It just doesn't make sense.

382
00:14:32,896 --> 00:14:33,974
Yeah.

383
00:14:34,842 --> 00:14:36,241
I'm sorry, babe.

384
00:14:36,362 --> 00:14:38,151
Listen, you'll feel
better in the morning.

385
00:14:38,176 --> 00:14:40,376
But how about I go upstairs and draw us

386
00:14:40,401 --> 00:14:41,767
a nice, hot bath right now?

387
00:14:41,802 --> 00:14:43,215
All right, I'll be up in a minute,
all right?

388
00:14:43,240 --> 00:14:45,280
- Okay. I love you.
- I love you.

389
00:14:56,583 --> 00:14:59,221
Man, buddy, that was tough to watch.

390
00:15:00,146 --> 00:15:04,356
And I once saw a guy get
impaled on a picket fence.

391
00:15:05,840 --> 00:15:07,228
What are you doing here?

392
00:15:07,253 --> 00:15:09,432
Uh, Father Philip wanted
me to return this to you.

393
00:15:10,037 --> 00:15:12,188
He also wanted me to tell
you he used the lip balm,

394
00:15:12,213 --> 00:15:13,897
so maybe you should scissor off the top.

395
00:15:13,922 --> 00:15:15,417
Yeah.

396
00:15:16,201 --> 00:15:18,225
You gonna let me in?

397
00:15:18,266 --> 00:15:20,399
Why'd you invite me there tonight, huh?

398
00:15:20,545 --> 00:15:22,312
You just wanted me to
look like an idiot?

399
00:15:22,347 --> 00:15:24,424
Well, you pretty much
did that on your own.

400
00:15:24,449 --> 00:15:28,147
But, uh,
I guess it was a little bit of payback.

401
00:15:28,336 --> 00:15:29,935
Payback? For what?

402
00:15:29,988 --> 00:15:31,242
For what?

403
00:15:31,267 --> 00:15:33,278
You retired. You never call me.

404
00:15:33,303 --> 00:15:35,856
You're running around
with the guys having fun.

405
00:15:36,078 --> 00:15:37,511
Well, you're having fun, too, right?

406
00:15:37,537 --> 00:15:40,531
You're out there with your partner,
Mr. Perfect.

407
00:15:40,704 --> 00:15:43,116
You can do plenty of
stuff that guy can't.

408
00:15:43,141 --> 00:15:44,474
Really?

409
00:15:44,745 --> 00:15:47,011
No, he can pretty much do everything.

410
00:15:48,439 --> 00:15:51,412
I mean, honestly, most calls

411
00:15:51,437 --> 00:15:53,222
I don't even get out
of the squad car now.

412
00:15:53,247 --> 00:15:55,104
I'm like, "You got this one?"
He's like, "Yep."

413
00:15:55,129 --> 00:15:57,517
I'm like, "All right,
I'll fill out the paperwork."

414
00:15:58,176 --> 00:16:01,228
So, all the stories about him are true?

415
00:16:01,253 --> 00:16:04,548
I mean, he really saved those
puppies from that burning cab?

416
00:16:04,574 --> 00:16:06,021
Yeah, seven of them.

417
00:16:06,122 --> 00:16:08,085
He took them home, and he trained them.

418
00:16:08,783 --> 00:16:10,964
Four of them are guide dogs now.

419
00:16:11,601 --> 00:16:14,661
One of them's an actor and just
did a movie with Judi Dench.

420
00:16:15,210 --> 00:16:18,475
I saw the trailer. He
was incredible in it.

421
00:16:18,537 --> 00:16:21,020
He's better than me at everything.

422
00:16:21,045 --> 00:16:23,364
No. He's better than me at everything!

423
00:16:23,935 --> 00:16:26,516
It used to drive me crazy,
but you know what?

424
00:16:26,541 --> 00:16:29,108
I figured out something
we're both better at.

425
00:16:29,133 --> 00:16:30,499
What's that?

426
00:16:30,780 --> 00:16:32,382
Okay, picture this.

427
00:16:32,407 --> 00:16:33,973
It's late at night.

428
00:16:34,127 --> 00:16:37,531
We're driving on the
mean streets of Syosset.

429
00:16:41,203 --> 00:16:43,299
A song comes on the radio.

430
00:16:43,474 --> 00:16:44,940
And I turn it up.

431
00:16:44,965 --> 00:16:46,390
- There it is.
- Right?

432
00:16:46,415 --> 00:16:47,266
Uh-huh.

433
00:16:47,291 --> 00:16:48,789
And we both know what's about to happen.

434
00:16:48,814 --> 00:16:49,485
Oh, yeah.

435
00:16:49,510 --> 00:16:50,984
I don't even have to look at you

436
00:16:51,009 --> 00:16:53,120
because I know you're gonna be there.

437
00:16:53,145 --> 00:16:54,656
You're right there.

438
00:17:03,378 --> 00:17:07,065
♪ You don't bring me flowers ♪

439
00:17:07,090 --> 00:17:10,756
♪ You don't sing me love songs ♪

440
00:17:10,781 --> 00:17:13,409
♪ You hardly talk to me anymore ♪

441
00:17:13,434 --> 00:17:18,515
♪ When I come through the
door at the end of the day ♪

442
00:17:20,119 --> 00:17:23,352
♪ I remember when ♪

443
00:17:23,590 --> 00:17:26,641
♪ You couldn't wait to love me ♪

444
00:17:26,989 --> 00:17:30,417
♪ Used to hate to leave me ♪

445
00:17:31,147 --> 00:17:37,922
♪ Now after lovin'
me late at night ♪

446
00:17:37,947 --> 00:17:42,448
♪ When it's good for you, baby ♪

447
00:17:42,473 --> 00:17:44,973
♪ And you're feeling all right ♪

448
00:17:44,998 --> 00:17:50,204
♪ Well, you just roll over ♪

449
00:17:50,230 --> 00:17:56,181
♪ And turn out the light ♪

450
00:17:56,693 --> 00:18:04,553
♪ You don't bring
me flowers anymore ♪

451
00:18:07,731 --> 00:18:10,089
♪ It used to be so natural ♪

452
00:18:10,114 --> 00:18:12,202
♪ Used to be ♪

453
00:18:12,227 --> 00:18:14,566
♪ To talk about forever ♪

454
00:18:14,591 --> 00:18:15,790
♪ Mm-mm-mm ♪

455
00:18:15,815 --> 00:18:19,023
♪ But "used to be's"
don't count anymore ♪

456
00:18:19,048 --> 00:18:23,728
♪ They just lay on the floor
till we sweep them away ♪

457
00:18:23,753 --> 00:18:27,441
♪ You don't bring me flowers ♪

458
00:18:27,466 --> 00:18:30,933
♪ You don't say you need me ♪

459
00:18:31,433 --> 00:18:36,499
♪ You don't sing me love songs ♪

460
00:18:36,524 --> 00:18:41,794
♪ Anymore ♪

461
00:18:47,695 --> 00:18:49,395
That was amazing!

462
00:18:49,658 --> 00:18:51,686
When you went low, I took it up high.

463
00:18:51,711 --> 00:18:52,618
I can't believe it.

464
00:18:52,643 --> 00:18:53,777
- We have to record the
- Yes.

465
00:18:53,786 --> 00:18:54,468
Next time we do this.

466
00:18:54,493 --> 00:18:55,940
Without a doubt.
We should get clothes, though.

467
00:18:55,965 --> 00:18:56,900
We got to look a little bit better.

468
00:18:56,925 --> 00:18:57,773
- Yes, yes.
- You know what I'm saying?

469
00:18:57,798 --> 00:18:58,753
Coordinating outfits.

470
00:18:58,779 --> 00:19:00,008
I'm telling you, we're
onto something here.

471
00:19:00,033 --> 00:19:02,849
Do you understand the happiness
we just brought to these people?

472
00:19:02,874 --> 00:19:07,303
- I think we should do this.
- ♪ Nessun dorma ♪

473
00:19:07,574 --> 00:19:11,251
♪ Nessun dorma ♪

474
00:19:11,502 --> 00:19:15,515
♪ Dilegua, o notte ♪

475
00:19:15,540 --> 00:19:20,572
♪ Tramontate, stelle ♪

476
00:19:20,597 --> 00:19:27,463
♪ Tramontate, stelle ♪

477
00:19:27,919 --> 00:19:35,082
♪ All'alba vincero' ♪

478
00:19:35,116 --> 00:19:39,127
♪ Vincero' ♪

479
00:19:39,152 --> 00:19:50,879
♪ Vincero' ♪

480
00:20:01,782 --> 00:20:03,448
Oh, man, he is good.

481
00:20:03,604 --> 00:20:06,213
His performance... You just buy into it.

482
00:20:06,238 --> 00:20:07,078
Yep.

483
00:20:07,103 --> 00:20:08,575
It's like he's not even acting.

484
00:20:08,600 --> 00:20:09,590
Yep.

485
00:20:10,036 --> 00:20:11,835
I mean, the fact that he's
working with Judi Dench

486
00:20:11,860 --> 00:20:13,376
doesn't hurt, either.

487
00:20:15,813 --> 00:20:17,437
You hear that? Right on cue.

488
00:20:17,462 --> 00:20:18,876
Yeah.

489
00:20:19,285 --> 00:20:21,174
By the way, how sick are these?

490
00:20:21,199 --> 00:20:22,981
Incredible.
You don't even have to chew them.

491
00:20:23,006 --> 00:20:24,221
They just, like, melt in your mouth.

492
00:20:24,246 --> 00:20:25,678
Right. Like, in each bite,

493
00:20:25,703 --> 00:20:27,837
you get a little bit of sweet,
you get a little bit of hot.

494
00:20:27,862 --> 00:20:29,716
- Mm-hmm.
- These things are amazing.

495
00:20:29,741 --> 00:20:30,413
The best.

496
00:20:30,438 --> 00:20:33,025
How are the wings, boys?

497
00:20:34,003 --> 00:20:38,203
- Synced and corrected by martythecrazy -
- www.addic7ed.com -

498
00:20:38,575 --> 00:20:39,806
Eh, they're all right.

499
00:20:39,831 --> 00:20:41,441
I've had better.

500
00:20:41,466 --> 00:20:42,965
Yeah. Thank you.

