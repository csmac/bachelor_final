﻿1
00:00:00,346 --> 00:00:06,383
:Joseph M. Schenck Presents
"Buster" Keaton
in
Convict 13 [1920]

2
00:00:06,559 --> 00:00:11,002
Written and directed by
"BUSTER" KEATON
and
EDDIE CLINE

3
00:00:11,805 --> 00:00:18,371
Golf - the game that brings
out the beast in men.

4
00:00:19,200 --> 00:00:24,023
Written Subtitle by:
Jaffar AlBori

5
00:04:24,460 --> 00:04:25,572
"Fore!"

6
00:04:55,400 --> 00:04:56,385
GUARD

7
00:05:58,300 --> 00:05:59,353
WARDEN

8
00:06:22,464 --> 00:06:27,517
"She loves me, she loves me not..."

9
00:07:37,364 --> 00:07:44,518
"You shouldn't have gone to
all the trouble just for me."

10
00:07:50,405 --> 00:07:55,237
"I want you to meet my father."

11
00:08:00,676 --> 00:08:05,725
"You are to be hanged today."

12
00:08:19,831 --> 00:08:25,598
"This is the guy we're
hanging today."

13
00:08:35,314 --> 00:08:40,390
"Don't worry. it'll work just fine."

14
00:10:22,867 --> 00:10:24,183
CHAMPION
HEAVY WEIGHT
HANGMAN
1916-17-20

15
00:11:31,525 --> 00:11:41,528
"Sorry. boys. We'll fix it and
tomorrow we'll hang two of
to make up for this."

16
00:13:56,753 --> 00:14:00,994
"Nice day we're having. eh?"

17
00:14:07,990 --> 00:14:15,207
"If you want to see who's boss,
just try me!"

18
00:16:34,650 --> 00:16:35,657
ASSISTANT
WARDEN

19
00:16:40,706 --> 00:16:45,540
"The riot starts at three o'clock"

20
00:20:18,236 --> 00:20:19,106
THE END