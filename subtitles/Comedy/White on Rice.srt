1
00:00:17,183 --> 00:00:20,589
Japan-The Feudal Era.

2
00:00:26,794 --> 00:00:26,794
Hordes of Masterless Samurai
roam the countyside...

3
00:00:30,430 --> 00:00:35,774
...seeking employment
and horizontal refreshment.

4
00:01:05,665 --> 00:01:08,437
Ambush at Blood-Trail Gate

5
00:02:21,142 --> 00:02:21,142
This movie sucks.

6
00:02:22,775 --> 00:02:24,578
This is the worse movie l've ever seen.

7
00:02:24,879 --> 00:02:26,882
Be quiet! This is his big scene!

8
00:02:27,815 --> 00:02:30,319
My big brother's a movie star!

9
00:02:30,450 --> 00:02:32,654
This is the highlight of my whole career.

10
00:02:32,820 --> 00:02:36,393
Even better than working in
the tuna canney?

11
00:02:36,623 --> 00:02:38,393
Hey! Be nice.

12
00:02:38,658 --> 00:02:39,593
Ouch.

13
00:03:03,449 --> 00:03:04,617
l love this movie.

14
00:04:05,345 --> 00:04:05,345
This is it.

15
00:04:06,346 --> 00:04:08,216
Best scene in the whole movie.

16
00:05:26,693 --> 00:05:31,803
(Haiku) Looking back at twelve
Iong years together...

17
00:05:32,066 --> 00:05:33,835
Tak.

18
00:05:41,541 --> 00:05:44,747
See you tonight.

19
00:07:02,122 --> 00:07:04,593
You think she'd go out with me?

20
00:07:05,591 --> 00:07:08,062
Maybe if you took her husband along.

21
00:08:12,159 --> 00:08:15,732
Hey! What do you think you're doing?

22
00:08:19,233 --> 00:08:22,806
Do you really think l'd hide
your birthday presents in here?

23
00:08:22,969 --> 00:08:22,969
l thought l had 'em for sure.

24
00:08:25,004 --> 00:08:26,072
Put your seatbelt on.

25
00:08:31,777 --> 00:08:33,680
l need to talk to you about something.

26
00:08:34,914 --> 00:08:37,151
ls Tak upset with me?

27
00:08:38,951 --> 00:08:38,951
l'm not worried.

28
00:08:40,720 --> 00:08:40,720
Why won't Tak give me a break?

29
00:08:43,789 --> 00:08:47,229
He was probably just like me
before he met you.

30
00:08:55,269 --> 00:08:55,269
Pizza's here!

31
00:08:56,602 --> 00:08:57,971
-Again?

32
00:08:58,804 --> 00:08:59,939
Can you grab the phone?

33
00:09:27,934 --> 00:09:29,069
Wow, is this Ramona?

34
00:09:31,238 --> 00:09:32,205
Yep.

35
00:09:36,008 --> 00:09:37,778
She's not a child anymore...

36
00:09:47,588 --> 00:09:49,157
How's the apartment search going?

37
00:09:51,258 --> 00:09:53,027
Pretty well.

38
00:09:53,092 --> 00:09:54,862
Not bad at all.

39
00:09:55,161 --> 00:09:57,566
Actually really well because
l stopped looking.

40
00:10:03,035 --> 00:10:06,174
Well, l've been thinking a lot lately...

41
00:10:06,772 --> 00:10:09,978
...and l've decided that until
l get remarried...

42
00:10:10,109 --> 00:10:12,614
...l should remain here.

43
00:10:14,046 --> 00:10:15,349
...son of a....

44
00:10:15,781 --> 00:10:16,749
Good idea.

45
00:10:17,384 --> 00:10:17,384
These things take time.

46
00:10:19,118 --> 00:10:20,053
Thank you!

47
00:10:20,287 --> 00:10:22,924
l'm taking my time, playing the field...

48
00:10:23,255 --> 00:10:26,260
l can find somebody better than my ex-wife.
Easily.

49
00:10:35,167 --> 00:10:36,303
He's weird!

50
00:10:36,736 --> 00:10:38,940
No matter how you look at it,
it's not normal!

51
00:10:39,071 --> 00:10:41,008
Don't you think you're overreacting?

52
00:10:41,140 --> 00:10:46,283
No, l'm just angy that our son
has to share a bunk bed with his uncle.

53
00:10:46,746 --> 00:10:49,985
So he should live in his car again?

54
00:10:50,116 --> 00:10:52,153
What about the guest room?

55
00:10:52,251 --> 00:10:54,288
But Ramona's coming!

56
00:10:54,820 --> 00:10:54,820
So let's stum him in the attic!

57
00:10:56,689 --> 00:10:56,689
lf that doesn't work,
give him to the neighbors.

58
00:10:58,358 --> 00:11:00,194
Or the neighbors' neighbon.

59
00:11:29,822 --> 00:11:29,822
Can't we just get Hajime remarried...

60
00:11:29,855 --> 00:11:29,855
Can't we just get Hajime remarried...

61
00:11:33,427 --> 00:11:37,200
...and out of here so
we can have some peace?

62
00:11:38,465 --> 00:11:41,804
l'm going to set him up with Shiho.

63
00:11:43,002 --> 00:11:45,874
That tall girl?

64
00:11:47,474 --> 00:11:49,878
l dunno about that.

65
00:11:51,478 --> 00:11:53,882
Goodnight.

66
00:11:55,382 --> 00:11:57,786
l'll find someone better than you.

67
00:14:34,974 --> 00:14:36,109
Let me talk to Aiko.

68
00:14:36,877 --> 00:14:38,112
She went out.

69
00:14:38,410 --> 00:14:38,410
Are you sure?
l really need to talk to her.

70
00:14:40,881 --> 00:14:42,083
She's not here!

71
00:14:42,314 --> 00:14:43,516
What do you want?

72
00:14:45,652 --> 00:14:47,054
Hello?

73
00:16:17,476 --> 00:16:18,712
Did you wash your hands?

74
00:16:41,600 --> 00:16:43,671
Come say hi to Shiho.

75
00:16:47,072 --> 00:16:49,543
Nice to meet you, Hajime.
l'm Shiho.

76
00:16:51,643 --> 00:16:53,346
...nice to meet you too.

77
00:16:54,546 --> 00:16:56,016
Call me Jimmy.

78
00:16:56,115 --> 00:16:57,751
You go by Jimmy?

79
00:16:58,217 --> 00:17:00,354
That's a funny name for a Japanese.

80
00:17:00,687 --> 00:17:02,523
Yes, it is.

81
00:17:04,691 --> 00:17:06,093
Can l speak with you in private?

82
00:17:07,493 --> 00:17:08,696
What were you thinking?

83
00:17:08,762 --> 00:17:09,763
You're so cruel!

84
00:17:10,562 --> 00:17:13,334
Just relax. Get to know her.

85
00:17:13,532 --> 00:17:14,467
She's enonnous!

86
00:17:20,439 --> 00:17:23,111
l don't want to get to know
something that big.

87
00:17:23,208 --> 00:17:25,011
She'll crush me!

88
00:17:25,177 --> 00:17:28,216
You should thank me.
She's way out ofyour league.

89
00:17:29,348 --> 00:17:32,387
She's gonna think l'm weird.
And short!

90
00:17:32,551 --> 00:17:32,551
...Well, don't be weird...

91
00:17:34,253 --> 00:17:36,256
and don't be short.

92
00:18:00,847 --> 00:18:04,253
What kind of work do you do?

93
00:18:04,716 --> 00:18:07,621
Do you eat a lot of protein?

94
00:18:09,756 --> 00:18:12,828
Soy. Customer service.

95
00:18:13,225 --> 00:18:14,828
Oh, really?

96
00:18:15,160 --> 00:18:16,629
ls that fun?

97
00:18:17,296 --> 00:18:17,296
The office ceiling is vey low, so l like it.

98
00:18:19,398 --> 00:18:22,137
Much more comfortable that way.

99
00:18:23,202 --> 00:18:23,202
Hajime! Eat your vegetables.

100
00:18:25,404 --> 00:18:26,572
You like this stum, right?

101
00:18:35,747 --> 00:18:40,256
Aiko tells me you like dinosaurs.

102
00:18:41,520 --> 00:18:44,592
Yes, it's something of a hobby.

103
00:18:45,724 --> 00:18:47,327
Do you have a favorite dinosaur?

104
00:18:48,293 --> 00:18:49,428
Me?

105
00:18:49,628 --> 00:18:49,628
Favorite dinosaur? l've never really

106
00:18:51,831 --> 00:18:51,831
Let me guess!
-Okay.

107
00:18:54,199 --> 00:18:55,534
The Brontosaurus.

108
00:18:55,634 --> 00:18:58,339
You know, the huge one with
the really long neck.

109
00:18:59,271 --> 00:19:03,579
l bet it's really convenient to be able
to reach things in high places.

110
00:19:04,776 --> 00:19:06,379
What are you tying to say?

111
00:19:06,578 --> 00:19:07,413
Nothing.

112
00:19:08,747 --> 00:19:10,650
This is just one of Hajime's little jokes.

113
00:19:10,817 --> 00:19:12,452
What are you tying to say, shorty?

114
00:19:12,718 --> 00:19:14,354
See? l told you!

115
00:19:15,621 --> 00:19:17,224
lt's always like this.

116
00:19:18,290 --> 00:19:20,627
Men are always afraid of my height.

117
00:19:20,726 --> 00:19:25,635
No...Hajime's just a little intimidated
because you're pretty.

118
00:19:25,865 --> 00:19:25,865
Yeah, and l wouldn't
play basketball with you either.

119
00:19:28,902 --> 00:19:30,671
l bet you can dunk, right?

120
00:19:33,205 --> 00:19:34,774
What? What did l say?

121
00:19:36,575 --> 00:19:38,545
Shiho! l'm sory, sweetie! l'm sory!

122
00:19:38,710 --> 00:19:39,846
Are you okay?

123
00:22:30,415 --> 00:22:34,656
How did an old fogey like Tak convince you
to mary him?

124
00:22:35,520 --> 00:22:36,788
What do you mean?

125
00:22:37,023 --> 00:22:40,329
Well, he isn't handsome

126
00:22:40,425 --> 00:22:45,034
...and he dresses like a grandpa.
-Hey! Watch it.

127
00:22:45,630 --> 00:22:46,965
Sory.

128
00:22:47,133 --> 00:22:49,103
He must have some secret though.

129
00:22:49,534 --> 00:22:51,437
There's no big secret!

130
00:22:51,903 --> 00:22:55,910
He just took me to things
he was interested in.

131
00:22:57,143 --> 00:23:01,351
Museums, concerts...

132
00:23:01,747 --> 00:23:06,490
He would even play the violin for me back then.

133
00:23:06,885 --> 00:23:09,790
That's it? You gave it up for that?

134
00:23:11,456 --> 00:23:13,827
lt was fun!

135
00:23:14,493 --> 00:23:16,864
lt really was fun.

136
00:23:17,130 --> 00:23:17,130
lt was fun? It WAS fun?

137
00:23:19,998 --> 00:23:21,835
Oh, shut up.

138
00:26:53,712 --> 00:26:57,820
Can you believe it? Almost 12 years.

139
00:26:59,084 --> 00:27:01,923
Time sure flies.

140
00:27:03,021 --> 00:27:08,097
Why don't we go someplace
for our annivenay?

141
00:27:08,193 --> 00:27:13,604
How about Ishigaki island?
Just the mo of us.

142
00:27:17,168 --> 00:27:18,337
Hello?

143
00:27:21,272 --> 00:27:21,272
We can't leave Hajime in charge.

144
00:27:24,242 --> 00:27:27,949
You remember the last time?

145
00:27:29,247 --> 00:27:33,889
Oh right. That carpet still smells awful.

146
00:31:05,296 --> 00:31:08,035
l'm not so sure how l feel about this.

147
00:31:08,099 --> 00:31:11,472
Why? Education is never a bad thing.

148
00:31:13,371 --> 00:31:14,540
How's he going to pay for it?

149
00:31:16,975 --> 00:31:19,914
l told him we would...help.

150
00:31:21,312 --> 00:31:22,982
Perfect.

151
00:31:45,236 --> 00:31:49,077
This coffin is just the right size for Hajime.

152
00:31:53,344 --> 00:31:55,181
What? It is.

153
00:31:56,147 --> 00:31:57,984
He can't do anything by himself.

154
00:31:58,983 --> 00:32:00,619
lts just like having a dead guy around.

155
00:32:28,513 --> 00:32:28,513
We're brother and sister, you know.

156
00:32:30,415 --> 00:32:33,387
We're more alike than you think.
-No you're not!

157
00:32:34,285 --> 00:32:36,322
Please don't say that.

158
00:32:38,289 --> 00:32:38,289
But he's fun!

159
00:32:40,325 --> 00:32:44,266
l know he's a handful, but...

160
00:32:44,429 --> 00:32:48,370
...isn't it fun having him around?

161
00:32:49,267 --> 00:32:51,270
l'm not fun?

162
00:32:52,370 --> 00:32:54,373
You don't think l'm fun?

163
00:37:22,573 --> 00:37:25,412
l'm vey impressed with Ramona.

164
00:37:25,509 --> 00:37:28,581
What do you mean?

165
00:37:28,713 --> 00:37:31,953
Nothing, l just think...she's great.

166
00:37:32,316 --> 00:37:35,855
The last couple ofyears
must have been tough for her.

167
00:37:35,921 --> 00:37:36,888
Why?

168
00:37:37,022 --> 00:37:41,263
She had a hard time after Tim
and left town for a while.

169
00:37:41,359 --> 00:37:43,730
So l was just a little worried about her.

170
00:37:43,828 --> 00:37:45,264
Oh, l see.

171
00:37:47,732 --> 00:37:49,235
Wait, what about Tim?

172
00:37:49,867 --> 00:37:51,570
Don't you remember?

173
00:37:52,370 --> 00:37:54,907
They dated all through high school.

174
00:37:55,274 --> 00:37:59,281
Eveyone thought they would
get married eventually.

175
00:37:59,510 --> 00:38:02,515
Wait...is this the same Ramona?

176
00:38:02,847 --> 00:38:02,847
l never got the whole stoy...

177
00:38:04,983 --> 00:38:04,983
but her mother said Ramona
needed to move on

178
00:38:08,686 --> 00:38:11,558
so she left Tim for grad school.

179
00:38:12,390 --> 00:38:14,727
lt was pretty sad. Poor Tim.

180
00:38:15,626 --> 00:38:17,563
No! Not poor Tim!

181
00:38:17,695 --> 00:38:20,400
Why is this the first time
l'm hearing about this?

182
00:38:20,966 --> 00:38:20,966
Don't yell, he'll hear you!

183
00:38:23,035 --> 00:38:24,737
l don't care!

184
00:38:25,770 --> 00:38:25,770
Wait...who'll hear me?

185
00:38:27,371 --> 00:38:28,272
Tim!

186
00:38:29,074 --> 00:38:31,545
He's downstain with Ramona.

187
00:38:32,910 --> 00:38:34,046
WHAT?

188
00:38:42,920 --> 00:38:44,690
Did you finish your homework?

189
00:44:27,264 --> 00:44:30,371
And the police took 45 minutes to show up!

190
00:44:30,435 --> 00:44:33,206
l could've walked to the station by then.

191
00:44:39,977 --> 00:44:42,247
Did you ask Bob about his math test?

192
00:44:42,813 --> 00:44:44,315
He had a math test?

193
00:44:48,218 --> 00:44:50,656
l'm sure he's fine.

194
00:44:51,855 --> 00:44:54,159
He can take care of himself.

195
00:44:54,758 --> 00:44:54,758
The one l'm worried about...

196
00:44:56,428 --> 00:44:58,665
...is the other, bigger kid!

197
00:46:52,309 --> 00:46:57,920
Sometimes l wish l could...
be closer to him.

198
00:46:58,817 --> 00:47:00,152
Really?

199
00:47:01,486 --> 00:47:01,486
Even though we live
in the same house...

200
00:47:04,421 --> 00:47:07,193
...sometimes l feel like l'm
living with a stranger.

201
00:47:10,294 --> 00:47:11,997
What about this...

202
00:47:12,229 --> 00:47:14,266
Take him out, just the two
of you now and then.

203
00:47:14,398 --> 00:47:16,401
One week do something that he likes...

204
00:47:16,501 --> 00:47:20,341
the next, do something you like to do.

205
00:47:20,871 --> 00:47:22,473
You don't mind being left alone like that?

206
00:47:23,006 --> 00:47:25,511
Of course not! l've got plenty
of work to do.

207
00:47:27,444 --> 00:47:31,151
l don't know how you'll hold up...

208
00:47:31,616 --> 00:47:34,922
Get ready to spend lots of time
at the dinosaur museum.

209
00:47:36,855 --> 00:47:38,224
Really? Him too?

210
00:47:38,355 --> 00:47:39,925
Of coune!

211
00:47:40,157 --> 00:47:44,131
Ever since he was a kid, that's all
he ever talks about!

212
00:47:44,494 --> 00:47:45,495
Wait a minute.

213
00:47:47,866 --> 00:47:49,368
Aren't we talking about Bob?

214
00:48:56,634 --> 00:48:59,606
Let's skip the concert and
go home early.

215
00:49:00,638 --> 00:49:04,177
Jimmy and Bob are out for Halloween,
so we'll have the place to ourselves...

216
00:49:05,275 --> 00:49:06,577
That's a good idea.

217
00:49:06,977 --> 00:49:09,682
l actually need to catch up
on a bunch of invoices.

218
00:49:10,981 --> 00:49:13,118
Let's go home.

219
00:49:15,954 --> 00:49:17,456
Should we get this to go?

220
00:51:23,346 --> 00:51:23,346
Hey gorilla!

221
00:51:23,380 --> 00:51:24,750
Hey gorilla!

222
00:51:25,415 --> 00:51:27,051
How was trick-or-treating?

223
00:51:27,684 --> 00:51:28,719
All right.

224
00:51:29,453 --> 00:51:31,356
How'd his costume work out?

225
00:51:35,659 --> 00:51:37,764
l thought he was going as a ghost.

226
00:51:53,343 --> 00:51:54,611
We have a problem!

227
00:51:54,744 --> 00:51:56,214
l forgot!
-Forgot what?

228
00:51:56,546 --> 00:51:59,184
l forgot l was supposed to take him.

229
00:51:59,516 --> 00:52:00,651
Bob's gone!

230
00:52:03,855 --> 00:52:05,490
He just left Bob here?

231
00:53:09,252 --> 00:53:11,890
l have done eveything for you.
Eveything!

232
00:53:12,489 --> 00:53:16,797
The one time l ask you for anything,
you go and lose my son!

233
00:53:30,607 --> 00:53:30,607
Where did you go?

234
00:53:32,910 --> 00:53:34,746
Eveyone was womied!

235
00:58:41,684 --> 00:58:46,125
Remember when he worked on
a crab boat in Alaska?

236
00:58:46,723 --> 00:58:47,724
He was gone forever.

237
00:58:47,891 --> 00:58:48,892
That's true...

238
00:58:49,260 --> 00:58:51,897
lt's just been so long with no word...

239
00:58:52,296 --> 00:58:54,065
l can't help but woy!

240
00:58:56,132 --> 00:58:57,535
Hold on just a second!

241
00:59:02,639 --> 00:59:03,974
So how's school going?

242
00:59:07,043 --> 00:59:08,178
l see.

243
00:59:10,613 --> 00:59:16,691
What else were you doing today?

244
01:04:16,587 --> 01:04:16,587
For all of the many troubles
l've caused you...

245
01:04:20,489 --> 01:04:24,063
...l offer my sincerest apologies.

246
01:04:25,094 --> 01:04:27,398
You've said that six times already.

247
01:04:29,365 --> 01:04:31,035
Where are you living right now?

248
01:04:31,635 --> 01:04:33,037
ln an apartment.

249
01:04:35,171 --> 01:04:39,379
Actually...in the closet at work.

250
01:04:41,277 --> 01:04:42,546
l see.

251
01:04:45,414 --> 01:04:46,884
The reason l've come here today...

252
01:04:47,416 --> 01:04:53,629
l've landed ajob showing dinosaurs
to tourists in Montana.

253
01:04:54,457 --> 01:05:00,268
Until l start, l'd love to see Aiko and Bob

254
01:05:00,363 --> 01:05:04,605
And l'd like to move back in.

255
01:05:05,201 --> 01:05:06,871
One last time.

256
01:06:05,194 --> 01:06:07,398
Your sister won't be home until later...

257
01:06:10,032 --> 01:06:13,706
...so be prepared for the water works.

258
01:06:17,540 --> 01:06:18,943
And one more thing.

259
01:06:19,676 --> 01:06:21,078
Don't forget our agreement.

260
01:06:23,346 --> 01:06:24,481
Yes, sir.

261
01:06:25,281 --> 01:06:26,951
l've got something to attend to.

262
01:06:29,151 --> 01:06:30,319
l'll be back later.

263
01:06:40,664 --> 01:06:43,168
Let's get to work.

264
01:09:31,700 --> 01:09:32,535
Not again.

265
01:09:36,805 --> 01:09:37,640
What's going on?

266
01:09:38,340 --> 01:09:39,608
l was just tying to make cuy!

267
01:09:42,478 --> 01:09:44,850
Hajime, you moron.
What have you done?

268
01:09:45,781 --> 01:09:48,353
l told you to clean the house,
not burn it down!

269
01:09:53,522 --> 01:09:54,357
Look at this!

270
01:09:55,859 --> 01:09:57,161
l'm on to you.

271
01:09:57,961 --> 01:10:01,233
You screw up eveything on purpose!

272
01:10:02,598 --> 01:10:03,433
But that's the last time!

273
01:10:07,703 --> 01:10:08,738
Are you okay?

274
01:10:09,338 --> 01:10:11,408
My leg...it's broken...

275
01:10:16,645 --> 01:10:17,680
You bastard!

276
01:10:18,215 --> 01:10:19,250
-Hold still, okay?

277
01:10:20,382 --> 01:10:21,417
Hey! Cut it out!

278
01:13:18,660 --> 01:13:20,630
Don't woy. l saved his life.

279
01:13:20,929 --> 01:13:22,899
And l found a job!

280
01:13:37,646 --> 01:13:38,814
Honey!

281
01:13:40,048 --> 01:13:42,051
Honey, can you hear me?

282
01:13:44,853 --> 01:13:46,856
How do you feel?
Are you in pain?

283
01:14:49,785 --> 01:14:52,122
You could have told me where you went,
you jerk.

284
01:14:52,721 --> 01:14:52,721
l'm not ajerk.

285
01:14:53,822 --> 01:14:57,094
After all, l just saved the day.

286
01:14:57,559 --> 01:15:00,831
That's true. Good job.

287
01:15:03,999 --> 01:15:08,575
lt hasn't been easy you know.
Since Nana left.

288
01:15:09,137 --> 01:15:09,137
You don't undentand.

289
01:15:11,241 --> 01:15:14,580
You have Tak and Bob...

290
01:15:14,943 --> 01:15:16,646
But l have nobody!

291
01:15:19,815 --> 01:15:22,754
l thought that Ramona was
the one for me..

292
01:15:25,721 --> 01:15:27,758
l know, l know...

293
01:17:11,026 --> 01:17:15,067
The place looks great, Aiko!

294
01:17:15,263 --> 01:17:17,100
Thanks! You really think so?

295
01:17:17,699 --> 01:17:19,969
Hope they paid well.

296
01:17:20,936 --> 01:17:24,142
Big brother...how's your stomach?

297
01:17:24,673 --> 01:17:27,210
You should be more careful
in the kitchen.

298
01:17:28,944 --> 01:17:31,316
Did you bring them a present?
-l sure did.

299
01:17:31,780 --> 01:17:34,050
That's unexpected. What is it?

300
01:17:36,218 --> 01:17:38,690
-C'mon, tell me!
-lt's a surprise.

301
01:19:25,495 --> 01:19:29,836
Hey...who's that girl with Hajime?

302
01:19:59,928 --> 01:20:01,831
lt's freezing out here.

303
01:20:02,030 --> 01:20:05,336
l can't believe it's fall again.

304
01:20:06,502 --> 01:20:08,772
Are you sure you don't need a ride?

305
01:20:08,937 --> 01:20:11,107
My taxi will be here soon.

306
01:20:12,440 --> 01:20:16,247
Be sure to tell Bob l'll pay back
evey cent.

307
01:20:16,378 --> 01:20:18,081
Don't wory about that.

308
01:20:18,815 --> 01:20:21,921
Just don't get fired.

309
01:20:22,918 --> 01:20:25,188
My dream job.

310
01:20:25,887 --> 01:20:25,887
l hope you're getting a good wage.

311
01:20:28,558 --> 01:20:30,294
Are they giving you insurance?

312
01:20:31,259 --> 01:20:33,196
lt's a little complicated.

313
01:20:33,461 --> 01:20:35,031
What is?

314
01:20:35,864 --> 01:20:38,836
l'm a docent. Avolunteer.

315
01:20:39,167 --> 01:20:42,807
You're joking, right?
-Don't wory!

316
01:20:43,840 --> 01:20:47,881
l'll start om strong, all business.

317
01:20:48,043 --> 01:20:50,447
They'll give me a paying position
in no time.

318
01:20:50,613 --> 01:20:53,250
Where will you sleep?
What will you eat?

319
01:20:53,415 --> 01:20:53,415
l've never really thought about that.

320
01:20:55,317 --> 01:20:59,091
But don't wory, l always land on my feet.

321
01:21:00,255 --> 01:21:01,992
This is my destiny.

322
01:21:02,558 --> 01:21:06,532
From this day on, l'm a new man.

323
01:21:36,958 --> 01:21:38,895
You're taking this car the whole way?

324
01:21:39,060 --> 01:21:42,366
Don't wory. We'll be fine.

