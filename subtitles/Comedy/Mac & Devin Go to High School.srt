﻿1
00:00:00,500 --> 00:00:02,150
We're gonna need a killer band.

2
00:00:02,169 --> 00:00:04,357
And that starts with
a serious musical director.

3
00:00:04,382 --> 00:00:05,835
Hired someone already!

4
00:00:05,867 --> 00:00:08,206
Hey Uncle Joe! What's up, buddy?

5
00:00:08,750 --> 00:00:12,414
I want serious work like the
detective novels I optioned.

6
00:00:14,398 --> 00:00:15,530
She's obviously on the verge

7
00:00:15,565 --> 00:00:16,531
of another break down.

8
00:00:16,566 --> 00:00:17,666
You think?

9
00:00:17,901 --> 00:00:19,918
Lookin' good, Joseph!

10
00:00:20,842 --> 00:00:23,009
Oh, what jail did you break out of?

11
00:00:23,573 --> 00:00:25,615
Yes sir, I am aware of the situation

12
00:00:25,640 --> 00:00:27,479
and I am fully prepared to take over

13
00:00:27,504 --> 00:00:28,960
all of Sam's duties.

14
00:00:30,539 --> 00:00:33,242
♪ It's the middle of the night! ♪

15
00:00:34,039 --> 00:00:36,593
♪ And you're feeling all right! ♪

16
00:00:37,421 --> 00:00:39,421
♪ Hello! ♪

17
00:00:40,375 --> 00:00:44,576
♪ It's the Joey Show! ♪

18
00:00:44,611 --> 00:00:46,328
Come on!

19
00:00:46,363 --> 00:00:47,996
Then the guitars come in and I say,

20
00:00:48,032 --> 00:00:50,115
"Ladies and gentlemen, Joey McIntyre!"

21
00:00:50,167 --> 00:00:52,734
And my musical director,
Chris Kirkpatrick!

22
00:00:52,753 --> 00:00:53,618
I love it, man.

23
00:00:53,670 --> 00:00:55,048
We're like a two-man super group, bro.

24
00:00:55,072 --> 00:00:56,004
It's unbelievable!

25
00:00:56,040 --> 00:00:57,693
Let's not print out the T-shirts yet,

26
00:00:57,718 --> 00:01:00,029
but man, they picked the
right guy for the job!

27
00:01:00,054 --> 00:01:01,576
Yeah, I really owe you
a big thanks, dude.

28
00:01:01,595 --> 00:01:02,744
I need this, bro.

29
00:01:02,763 --> 00:01:04,889
I mean, I lost everything investing

30
00:01:04,914 --> 00:01:07,279
in a professional hacky sack franchise.

31
00:01:07,304 --> 00:01:09,779
- Who does that?
- Don't beat yourself up, dude.

32
00:01:09,804 --> 00:01:11,449
Orlando Sacks were a good team!

33
00:01:11,474 --> 00:01:13,234
They were oh and 63.

34
00:01:14,091 --> 00:01:15,724
Thanks for trying to
cheer me up, but no.

35
00:01:15,759 --> 00:01:17,350
Hey, it's all in the past, bro.

36
00:01:17,375 --> 00:01:19,061
Let's concentrate on
making a great show!

37
00:01:19,096 --> 00:01:20,779
And take care of my tax debt.

38
00:01:20,814 --> 00:01:22,864
So here's to the first day of
the rest of my life!

39
00:01:22,900 --> 00:01:23,565
Bam!

40
00:01:23,600 --> 00:01:24,421
Thanks, man.

41
00:01:24,539 --> 00:01:27,552
♪ It's the middle of the night ♪

42
00:01:27,604 --> 00:01:28,887
♪ And you're feeling all right ♪

43
00:01:28,939 --> 00:01:30,304
Fired?!

44
00:01:31,731 --> 00:01:33,013
I'm fired!

45
00:01:33,894 --> 00:01:35,237
I'm Joey McIntyre.

46
00:01:35,262 --> 00:01:37,140
You know, the little guy from
New Kids on the Block.

47
00:01:37,164 --> 00:01:39,171
Since then, I've had my ups.

48
00:01:39,664 --> 00:01:41,516
And I've had my downs.

49
00:01:41,552 --> 00:01:43,268
But I know I can make it
with the love of

50
00:01:43,304 --> 00:01:45,387
my family, hard work,

51
00:01:45,439 --> 00:01:47,522
and maybe better management.

52
00:01:48,125 --> 00:01:50,327
synced and corrected by susinz
*www.addic7ed.com*

53
00:01:51,320 --> 00:01:52,186
Please don't.

54
00:01:52,221 --> 00:01:53,741
Because Sam really wouldn't want that,

55
00:01:53,772 --> 00:01:55,906
and you know, that's not
feng shui actually.

56
00:01:56,570 --> 00:01:57,859
Soozie!

57
00:01:58,570 --> 00:02:00,640
You know, I was looking through
Sam's files last night

58
00:02:00,664 --> 00:02:02,713
and I realized you have not
taken a vacation day

59
00:02:02,748 --> 00:02:04,250
in quite a while!

60
00:02:04,402 --> 00:02:06,155
You need some R&R, girl!

61
00:02:06,180 --> 00:02:08,146
Oh no, I get all my rest and relaxation

62
00:02:08,182 --> 00:02:10,476
on the weekends when
I clean Sam's pool.

63
00:02:10,929 --> 00:02:13,140
Wow. As your interim boss,

64
00:02:13,210 --> 00:02:16,804
I insist that you start
using your-woo...

65
00:02:16,824 --> 00:02:20,192
278 vacation days immediately!

66
00:02:20,244 --> 00:02:21,660
But I don't want to go on vacation.

67
00:02:21,695 --> 00:02:22,810
Who's gonna water Sam's cactus?

68
00:02:22,830 --> 00:02:23,811
It's plastic.

69
00:02:23,831 --> 00:02:24,579
Who's gonna pretend
to water Sam's cactus?

70
00:02:24,615 --> 00:02:25,615
Soozie!

71
00:02:25,867 --> 00:02:27,282
I'm booking you on a cruise.

72
00:02:27,317 --> 00:02:28,583
Maybe you'll meet a guy!

73
00:02:28,619 --> 00:02:30,007
- Ouch.
- Or whatever.

74
00:02:31,601 --> 00:02:33,781
Oh, hey Joe, can't talk right now.

75
00:02:33,938 --> 00:02:35,531
I have to go on vacation.

76
00:02:35,867 --> 00:02:37,189
You fired Chris?!

77
00:02:37,224 --> 00:02:39,242
Joe, I can see you're upset.

78
00:02:39,277 --> 00:02:41,560
But we're pivoting the
network in a new direction.

79
00:02:41,579 --> 00:02:43,395
And Chris no longer fit
the vision for the show.

80
00:02:43,414 --> 00:02:45,564
You guys said I was the
vision for the show.

81
00:02:45,583 --> 00:02:46,616
You are.

82
00:02:46,668 --> 00:02:49,368
But think of me as the
vision behind the vision.

83
00:02:49,403 --> 00:02:50,963
And I have some
exciting new initiatives

84
00:02:51,005 --> 00:02:53,320
that I can't wait to implement.

85
00:02:53,492 --> 00:02:55,812
You're acting like
Sam's not coming back.

86
00:02:56,078 --> 00:02:58,379
Sam showed up to work buck naked, Joe.

87
00:02:58,414 --> 00:02:59,775
She's gonna be in treatment for awhile.

88
00:02:59,799 --> 00:03:00,998
Well then why fire Chris?

89
00:03:01,033 --> 00:03:02,277
I mean, we had a great rehearsal.

90
00:03:02,301 --> 00:03:03,350
We sound amazing together!

91
00:03:03,386 --> 00:03:05,002
I'm sure you did but we have to

92
00:03:05,021 --> 00:03:05,836
make some changes now.

93
00:03:05,855 --> 00:03:06,971
The sponsor dropped out...

94
00:03:07,006 --> 00:03:08,222
Whoa, whoa, whoa, whoa.

95
00:03:08,274 --> 00:03:09,339
We lost the sponsor?

96
00:03:09,358 --> 00:03:10,774
I wore the diaper for nothing?

97
00:03:10,828 --> 00:03:11,990
Well not for nothing.

98
00:03:12,015 --> 00:03:13,517
Shrimpy got a three picture deal off

99
00:03:13,542 --> 00:03:15,185
that Vine of you falling on your face.

100
00:03:15,210 --> 00:03:16,937
Yeah, my kids watched
that about 300 times.

101
00:03:17,460 --> 00:03:19,016
Hey, listen, Joe.

102
00:03:19,125 --> 00:03:20,662
I hate to say this,
but Sam really did us

103
00:03:20,686 --> 00:03:22,156
prison style on this one.

104
00:03:22,570 --> 00:03:23,867
But luckily you have me!

105
00:03:24,173 --> 00:03:26,340
And I'm gonna give your
a show a facelift.

106
00:03:26,492 --> 00:03:28,175
Speaking of which,
meet your new producer,

107
00:03:28,210 --> 00:03:30,177
Tiffany Kerwinburn.

108
00:03:30,212 --> 00:03:32,346
This must be Joey McKid on the block!

109
00:03:32,381 --> 00:03:34,131
This is my husband, Howard by the way.

110
00:03:34,166 --> 00:03:35,640
Don't encourage him.

111
00:03:35,801 --> 00:03:37,051
Oh, good!

112
00:03:37,103 --> 00:03:39,031
You have an enormous head,

113
00:03:39,218 --> 00:03:40,888
you need a large head for daytime.

114
00:03:40,940 --> 00:03:41,722
Spoiler alert!

115
00:03:41,757 --> 00:03:43,190
We're moving the show to daytime!

116
00:03:43,225 --> 00:03:44,725
- Daytime?
- Yeah, daytime.

117
00:03:44,777 --> 00:03:46,343
- Yeah, daytime.
- Daytime?

118
00:03:46,362 --> 00:03:48,289
- Yes.
- What's confusing, it's daytime?

119
00:03:50,093 --> 00:03:52,843
Daytime? They can't do that, Joe.

120
00:03:53,169 --> 00:03:54,668
Yeah, I'm walking in right now.

121
00:03:54,704 --> 00:03:56,170
I'll take care of it, all right.

122
00:03:56,756 --> 00:03:59,131
Hi, I'm Alex Pike,
Joey McIntyre's manager.

123
00:03:59,156 --> 00:04:00,240
They're expecting me.

124
00:04:00,276 --> 00:04:02,926
And may I say that
blouse looks lovely on you.

125
00:04:02,962 --> 00:04:04,296
- A word.
- Whoa!

126
00:04:04,445 --> 00:04:06,475
You're in violation of
article fourteen,

127
00:04:06,500 --> 00:04:08,832
section twelve of the
employee rules of conduct.

128
00:04:08,851 --> 00:04:10,365
Um... I'm not an employee.

129
00:04:10,390 --> 00:04:12,303
- Do you have a Comfy pass?
- Duh.

130
00:04:12,338 --> 00:04:13,365
Well by excepting said pass,

131
00:04:13,389 --> 00:04:15,272
you've entered into a
defacto employee contract.

132
00:04:15,308 --> 00:04:17,867
And at Comfy, we have a zero
tolerance policy for...

133
00:04:18,093 --> 00:04:19,367
sexual harassment.

134
00:04:19,726 --> 00:04:22,240
I didn't harass her,
I just complimented her blouse.

135
00:04:22,265 --> 00:04:23,920
Let's refrain from using the b-word any

136
00:04:23,945 --> 00:04:25,835
further in this conversation, okay?

137
00:04:26,187 --> 00:04:28,018
Blouse?

138
00:04:28,125 --> 00:04:30,648
You'll have to come with me for
gender sensitivity training.

139
00:04:31,328 --> 00:04:32,456
Okay.

140
00:04:32,491 --> 00:04:34,358
I have a meeting with Joey McIntyre.

141
00:04:34,377 --> 00:04:35,718
Ever heard of him? Big deal.

142
00:04:35,743 --> 00:04:36,860
Is it gonna take very long?

143
00:04:36,879 --> 00:04:38,468
It's gonna take as long as it takes.

144
00:04:40,607 --> 00:04:41,906
Will there be snacks?

145
00:04:42,702 --> 00:04:44,953
Listen Joey, there's an entire world

146
00:04:44,978 --> 00:04:46,773
of lonely women sitting at home,

147
00:04:46,851 --> 00:04:48,505
desperate for something
pretty to look at

148
00:04:48,524 --> 00:04:49,973
while they do their laundry.

149
00:04:50,179 --> 00:04:51,906
And that is you.

150
00:04:52,344 --> 00:04:54,445
- Just look at those baby blues.
- Ooh!

151
00:04:54,480 --> 00:04:57,013
I signed up for late
night, not daytime!

152
00:04:57,041 --> 00:04:59,708
Joe, late night was never the right fit
for your brand.

153
00:04:59,735 --> 00:05:02,319
You're bright and happy
like ray of sunshine!

154
00:05:02,354 --> 00:05:03,186
Not right now.

155
00:05:03,205 --> 00:05:04,843
Well, maybe this will help.

156
00:05:05,541 --> 00:05:07,240
We focus grouped this and people just

157
00:05:07,265 --> 00:05:08,875
loved the logo.

158
00:05:10,671 --> 00:05:12,346
I look sixty-five in that thing.

159
00:05:12,507 --> 00:05:14,264
Well you have been around for awhile.

160
00:05:14,343 --> 00:05:15,576
Not that long!

161
00:05:15,601 --> 00:05:18,490
Joe, I think the gray makes
you look distinguished!

162
00:05:18,515 --> 00:05:21,796
Look, daytime viewers need
a father figure to look up to!

163
00:05:21,945 --> 00:05:22,977
Like Ellen.

164
00:05:23,351 --> 00:05:25,012
You know, when Maury went gray

165
00:05:25,031 --> 00:05:26,687
we knew we had ratings gold.

166
00:05:26,978 --> 00:05:27,978
Maury Povich?

167
00:05:28,007 --> 00:05:30,384
Tiffany has done over
three thousand hours

168
00:05:30,420 --> 00:05:31,719
of tabloid TV.

169
00:05:31,755 --> 00:05:34,172
Maury, Montel, Sally Jessy Raphael.

170
00:05:34,224 --> 00:05:35,556
I've worked with all the greats!

171
00:05:35,592 --> 00:05:37,458
So what do you want me to do?
DNA tests now?

172
00:05:37,477 --> 00:05:40,294
No, of course not.

173
00:05:40,313 --> 00:05:42,230
No, DNA is Maury's thing!

174
00:05:42,265 --> 00:05:44,148
Nobody does DNA like Maury.

175
00:05:44,184 --> 00:05:45,803
No, you'll be doing things like talking

176
00:05:45,828 --> 00:05:48,757
to sister wives or women who
marry inanimate objects.

177
00:05:48,750 --> 00:05:51,251
Sounds like water cooler TV to me!

178
00:05:51,286 --> 00:05:52,647
This is the worst idea you guys have

179
00:05:52,671 --> 00:05:55,472
come up with yet,
and that's saying a lot!

180
00:05:55,507 --> 00:05:57,390
Oh, trust me, we could come up with

181
00:05:57,426 --> 00:05:59,559
ideas way crazier than this.

182
00:05:59,595 --> 00:06:01,094
Oh I have one!

183
00:06:02,130 --> 00:06:03,312
Oh it went away.

184
00:06:03,632 --> 00:06:05,164
Oh there it is again!

185
00:06:05,189 --> 00:06:06,250
Look at me.

186
00:06:07,081 --> 00:06:08,187
And look mad.

187
00:06:09,137 --> 00:06:10,103
Yeah, that'll work.

188
00:06:10,138 --> 00:06:11,734
That's good, that's good stuff.

189
00:06:16,261 --> 00:06:17,335
Oh!

190
00:06:18,780 --> 00:06:20,820
Come on, I need that phone, that's Joe.

191
00:06:22,551 --> 00:06:25,418
As VP of finance,
head of human resources

192
00:06:25,453 --> 00:06:28,287
and senior office supply manager;

193
00:06:28,307 --> 00:06:30,223
It is advisable that
you follow my instructions

194
00:06:30,258 --> 00:06:31,524
to the letter.

195
00:06:31,835 --> 00:06:35,117
If you're in this room it means you've
done something unacceptable.

196
00:06:37,747 --> 00:06:40,664
I photocopied my Mrs. Beasley.

197
00:06:41,586 --> 00:06:42,586
Great.

198
00:06:42,638 --> 00:06:43,804
What did you do?

199
00:06:44,523 --> 00:06:46,585
- I honestly don't know.
- No talking!

200
00:06:46,953 --> 00:06:48,608
I am now going to show you a film

201
00:06:48,644 --> 00:06:50,977
on inappropriate office behavior.

202
00:06:51,012 --> 00:06:53,513
It's approximately
147 minutes in length.

203
00:06:53,532 --> 00:06:54,481
Oh what?

204
00:06:54,516 --> 00:06:56,882
And yes, there will be a quiz.

205
00:06:58,286 --> 00:07:02,328
Hey... It's for you.

206
00:07:03,539 --> 00:07:04,578
Oh!

207
00:07:06,109 --> 00:07:07,773
Mrs. Beasley, I presume?

208
00:07:08,269 --> 00:07:09,552
Pleased to meet you.

209
00:07:10,232 --> 00:07:12,015
Alex, where the hell are you?
Call me back!

210
00:07:12,067 --> 00:07:14,097
Buddy, wait up. Chris! Come on.

211
00:07:14,122 --> 00:07:15,330
We're gonna straighten this out.

212
00:07:15,354 --> 00:07:17,187
Yeah? Tell that to the IRS.

213
00:07:17,239 --> 00:07:19,239
I trusted you, McIntyre!

214
00:07:19,274 --> 00:07:20,323
Come on, Chris.

215
00:07:20,921 --> 00:07:23,159
- Boy band code.
- Screw the code!

216
00:07:23,195 --> 00:07:24,507
Screw you!

217
00:07:25,015 --> 00:07:27,037
And by the way, you were pitchy.

218
00:07:27,062 --> 00:07:29,271
Hey! Hey, don't be like that!

219
00:07:29,296 --> 00:07:30,750
Hey, we're gonna fix this.

220
00:07:30,786 --> 00:07:32,002
I am gonna fix this.

221
00:07:32,037 --> 00:07:32,985
Joe.

222
00:07:33,005 --> 00:07:34,005
It's me, Soozie.

223
00:07:34,271 --> 00:07:35,460
Yeah, hey.

224
00:07:35,835 --> 00:07:37,323
Have you seen Alex?

225
00:07:37,342 --> 00:07:39,592
No, I've been too busy
packing for this vacation.

226
00:07:39,750 --> 00:07:41,875
Sam would never let me go on vacation.

227
00:07:43,585 --> 00:07:44,585
I miss her.

228
00:07:45,062 --> 00:07:47,289
I never thought I'd say this
but I miss Sam too.

229
00:07:47,835 --> 00:07:49,507
Watch out for Paige, Joe.

230
00:07:49,617 --> 00:07:51,187
She can't be trusted.

231
00:07:52,343 --> 00:07:53,440
Watch out for her.

232
00:07:53,475 --> 00:07:54,937
Watch out.

233
00:07:55,193 --> 00:07:56,414
Watch out.

234
00:07:56,595 --> 00:07:59,132
Watch out for this couch,
it comes out of nowhere.

235
00:07:59,314 --> 00:08:02,875
I was never here, oh!

236
00:08:05,120 --> 00:08:06,286
Unbelievable.

237
00:08:06,321 --> 00:08:09,570
Honey, you will not believe I am...

238
00:08:10,852 --> 00:08:13,554
Honey, look who
I ran into at spin class!

239
00:08:21,820 --> 00:08:23,632
Have a cupcake, Joe!
I got coconut.

240
00:08:23,657 --> 00:08:25,739
A little birdy told me
it was your favorite.

241
00:08:25,791 --> 00:08:26,689
Yeah, I'll pass.

242
00:08:26,708 --> 00:08:28,691
So you guys just randomly met up at a

243
00:08:28,710 --> 00:08:29,876
spin class, huh?

244
00:08:29,911 --> 00:08:31,327
I know, so weird right?

245
00:08:31,362 --> 00:08:33,218
Joe, your wife, totally kicked my ass

246
00:08:33,243 --> 00:08:34,312
on the spin bike.

247
00:08:34,695 --> 00:08:36,694
Do you remember that
guy in the second to last row?

248
00:08:36,718 --> 00:08:37,929
Spikey shoe!

249
00:08:38,781 --> 00:08:40,648
Hmm, inside joke huh?

250
00:08:41,031 --> 00:08:43,005
Oh that's that annoyed Joe face you

251
00:08:43,024 --> 00:08:44,107
were telling me about!

252
00:08:44,142 --> 00:08:45,775
- Like hmm.
- What are you?

253
00:08:46,296 --> 00:08:47,359
Eh.

254
00:08:48,070 --> 00:08:49,457
He doesn't even know he's doing it.

255
00:08:49,481 --> 00:08:51,842
- That's it, right there!
- So good, God!

256
00:08:51,867 --> 00:08:53,625
Honey, we're just messing with you.

257
00:08:53,734 --> 00:08:55,885
Paige told me about the
move to daytime,

258
00:08:55,921 --> 00:08:57,257
what a great idea.

259
00:08:57,328 --> 00:08:59,139
She told you about that, huh?

260
00:08:59,174 --> 00:09:01,046
Yeah, I can totally see you in daytime.

261
00:09:01,078 --> 00:09:02,792
I said the same thing!

262
00:09:02,827 --> 00:09:04,761
Your husband is
exactly the kind of thing

263
00:09:04,796 --> 00:09:06,676
people want to have on
in the middle of the day.

264
00:09:06,992 --> 00:09:09,539
Just behind them, all
the time in the house,

265
00:09:09,623 --> 00:09:10,843
doing stuff.

266
00:09:12,102 --> 00:09:13,570
Can I talk to you for a second?

267
00:09:15,190 --> 00:09:16,190
Just...

268
00:09:18,027 --> 00:09:19,640
Go ahead, I'll be right here.

269
00:09:21,265 --> 00:09:22,914
Thanks, thanks for that.

270
00:09:24,257 --> 00:09:25,715
Do you see what's happening here?

271
00:09:25,750 --> 00:09:27,517
I'm hanging with
someone from spin class.

272
00:09:27,536 --> 00:09:29,736
You're a pawn in
Paige's sick little game!

273
00:09:29,788 --> 00:09:30,820
I invited her!

274
00:09:30,855 --> 00:09:33,790
She wanted you
to that's how good she is.

275
00:09:34,109 --> 00:09:37,375
Okay, I'm going back in the kitchen now

276
00:09:37,726 --> 00:09:39,296
with the evil one.

277
00:09:39,827 --> 00:09:41,335
Yeah, she is evil.

278
00:09:46,320 --> 00:09:47,485
In this exercise, you're working

279
00:09:47,504 --> 00:09:49,421
on a very exciting accounting project.

280
00:09:49,456 --> 00:09:50,655
Please keep in mind
the handout we completed

281
00:09:50,674 --> 00:09:53,593
in the eyes up here workbook.

282
00:09:53,703 --> 00:09:56,812
This will account for
one fifth of your final grade.

283
00:09:57,539 --> 00:09:58,617
Begin.

284
00:09:59,599 --> 00:10:00,599
Okay...

285
00:10:00,643 --> 00:10:01,643
Um...

286
00:10:02,269 --> 00:10:03,952
Good morning, Miss Lundquist...

287
00:10:05,507 --> 00:10:06,788
Stop! What is that?

288
00:10:06,840 --> 00:10:08,929
No gender specific pronouns.

289
00:10:08,954 --> 00:10:11,601
Just say that, you don't
need to... horn it.

290
00:10:13,783 --> 00:10:17,531
Good morning random
person who works at Comfy.

291
00:10:17,863 --> 00:10:22,796
Hi... Dick from accounting.

292
00:10:23,924 --> 00:10:26,765
This accounting it really hard, Dick.

293
00:10:28,961 --> 00:10:30,671
Please, just call me Richard.

294
00:10:31,231 --> 00:10:33,415
So about that exciting
accounting project

295
00:10:33,467 --> 00:10:34,875
we've been working on...

296
00:10:35,495 --> 00:10:38,109
I'm gonna need you to...

297
00:10:39,873 --> 00:10:41,440
Boink me?

298
00:10:41,475 --> 00:10:43,158
Oh son of a Beasley!

299
00:10:43,193 --> 00:10:46,234
God what are you... Ow!

300
00:10:47,581 --> 00:10:49,069
We'll have to start from the beginning.

301
00:10:49,093 --> 00:10:50,093
Yeah.

302
00:10:52,586 --> 00:10:53,485
And he was like

303
00:10:53,504 --> 00:10:55,871
"Oh my God my butt too
big for the seat!"

304
00:10:55,923 --> 00:10:57,233
And what's wrong with doing daytime?

305
00:10:57,257 --> 00:10:59,091
I do not want to do daytime, Donnie.

306
00:10:59,126 --> 00:11:00,542
And I can't get a hold of Alex.

307
00:11:00,594 --> 00:11:02,390
If you don't like the
way things are going,

308
00:11:02,671 --> 00:11:04,296
you just got to take control!

309
00:11:04,331 --> 00:11:05,497
How am I gonna take control when

310
00:11:05,516 --> 00:11:07,766
everyone's against me,
even my own wife.

311
00:11:08,367 --> 00:11:10,318
Maybe you should listen
to her once in awhile.

312
00:11:10,354 --> 00:11:12,604
In this business, Joe,
the only person you can

313
00:11:12,623 --> 00:11:14,322
count on to get things done is yourself!

314
00:11:14,375 --> 00:11:15,273
You know this.

315
00:11:15,292 --> 00:11:17,442
Oh babe you've got like five minutes
before Bachelor's starting.

316
00:11:17,461 --> 00:11:18,742
Will you stop it?

317
00:11:19,046 --> 00:11:21,546
- Joey's in a real pickle.
- Oh I want your pickle.

318
00:11:21,581 --> 00:11:23,415
Joe, it's like I always say,

319
00:11:23,434 --> 00:11:25,265
"When things are starting to slip,

320
00:11:25,421 --> 00:11:27,519
you have to be the one
to steer the ship."

321
00:11:27,554 --> 00:11:30,022
I'm sure you can find
somebody to help you out.

322
00:11:30,057 --> 00:11:32,148
There is one person.

323
00:11:36,029 --> 00:11:38,055
Welcome to Soozie's car.

324
00:11:38,080 --> 00:11:39,615
Please fasten your seatbelt.

325
00:11:39,640 --> 00:11:42,506
Travel time today is
approximately one hour

326
00:11:42,531 --> 00:11:44,264
and forty-seven minutes.

327
00:11:44,306 --> 00:11:46,046
We are experiencing some mild traffic

328
00:11:46,071 --> 00:11:48,218
but hope to get you
to your destination on time.

329
00:11:48,243 --> 00:11:50,415
You know, Soozie, you could
have just told me where Sam is.

330
00:11:50,439 --> 00:11:51,634
You didn't have to drive me yourself.

331
00:11:51,658 --> 00:11:53,231
Well actually I can't tell you where

332
00:11:53,243 --> 00:11:54,828
Sam is, she made me pinky promise.

333
00:11:54,853 --> 00:11:56,736
So when we get to
Pleasantries Rehab Center,

334
00:11:56,771 --> 00:11:57,854
I'll just point.

335
00:11:57,873 --> 00:11:58,822
Oh she's at Pleasantries?

336
00:11:58,857 --> 00:11:59,937
Oh you're good!

337
00:11:59,962 --> 00:12:01,658
You could be a TV detective!

338
00:12:01,693 --> 00:12:03,101
Hmm, tell me about it.

339
00:12:03,126 --> 00:12:04,292
Some music for the drive?

340
00:12:04,496 --> 00:12:07,163
Eh, oh.

341
00:12:07,182 --> 00:12:08,331
All New Kids, huh?

342
00:12:08,350 --> 00:12:09,968
Oh of course not!

343
00:12:10,179 --> 00:12:11,937
I have all your solo albums too.

344
00:12:12,048 --> 00:12:13,609
Meet Joe Mac is my favorite.

345
00:12:14,564 --> 00:12:17,890
Why don't we just have a little
silence for the first.

346
00:12:20,233 --> 00:12:21,382
No thank you!

347
00:12:24,399 --> 00:12:27,167
Here we go Joe!

348
00:12:37,145 --> 00:12:38,820
And he never really loved me.

349
00:12:39,722 --> 00:12:43,789
And then he said
he wasn't my real father.

350
00:12:45,068 --> 00:12:47,168
That's what you sound like, Nancy.

351
00:12:47,204 --> 00:12:49,921
Blah, blah, blah, blah, I cut myself.

352
00:12:50,192 --> 00:12:52,742
Wahh! I eat pesticides.

353
00:12:52,777 --> 00:12:55,778
My grandmother and I are in
a sexual relationship.

354
00:12:55,813 --> 00:12:57,313
I talk to fruit.

355
00:12:57,332 --> 00:12:59,482
I have a hand that I'm married to,

356
00:12:59,501 --> 00:13:01,117
my literal hand!

357
00:13:01,152 --> 00:13:03,601
Good God, I'm surrounded
by crazy people!

358
00:13:07,388 --> 00:13:08,500
Nancy, love.

359
00:13:08,812 --> 00:13:11,201
Tell me you see a boy band-er
and a garden gnome

360
00:13:11,226 --> 00:13:12,398
walking this way.

361
00:13:13,258 --> 00:13:14,328
Is that her?

362
00:13:14,816 --> 00:13:16,065
So...

363
00:13:16,101 --> 00:13:18,084
This place seems to be
treating you well.

364
00:13:18,359 --> 00:13:21,475
Joseph, Pleasantries Montecito

365
00:13:21,500 --> 00:13:23,164
has truly been an awakening.

366
00:13:23,189 --> 00:13:24,779
I feel fantastic!

367
00:13:24,992 --> 00:13:27,085
And I've met so many interesting people

368
00:13:27,427 --> 00:13:28,676
You see that woman over there?

369
00:13:29,578 --> 00:13:31,992
She used to eat nothing but sand paper.

370
00:13:32,017 --> 00:13:34,483
Huh, now that's a hit show.

371
00:13:34,502 --> 00:13:36,669
Speaking of the show, I just really...

372
00:13:36,705 --> 00:13:38,838
And I've really learned
to listen to people.

373
00:13:38,873 --> 00:13:41,641
You know, just really, really hear them.

374
00:13:42,179 --> 00:13:45,445
Most importantly, I've just
learned to be present.

375
00:13:46,164 --> 00:13:49,789
Soozie, this is very
difficult for me to admit.

376
00:13:49,814 --> 00:13:50,945
But uh...

377
00:13:51,084 --> 00:13:53,882
I've been informed apparently
you have feelings.

378
00:13:54,273 --> 00:13:55,179
Oh my God.

379
00:13:55,241 --> 00:13:58,459
And I need to recognize that.

380
00:13:58,484 --> 00:14:00,317
I've been waiting for
this day my whole...

381
00:14:00,441 --> 00:14:02,601
Fantastic, we are equals!

382
00:14:03,953 --> 00:14:06,416
Chop, chop, go get mama
her scream pillow, huh?

383
00:14:06,451 --> 00:14:08,179
- Okay.
- Hey-ya!

384
00:14:09,087 --> 00:14:10,873
So Sam, about the show...

385
00:14:10,898 --> 00:14:12,672
Oh b-b-b-b-Joseph.

386
00:14:13,414 --> 00:14:16,376
I really need to concentrate
on me and my healing,

387
00:14:16,428 --> 00:14:17,961
not you and your little show.

388
00:14:17,996 --> 00:14:21,148
Mm. I understand, and
I hate to ask you to

389
00:14:21,173 --> 00:14:25,184
step away from whatever this is.

390
00:14:25,228 --> 00:14:27,361
But we need you back at
the Comfy Channel.

391
00:14:27,389 --> 00:14:28,195
Oomph.

392
00:14:28,220 --> 00:14:30,034
With Paige in charge,
it's going down in flames,

393
00:14:30,058 --> 00:14:31,808
the whole network, down in flames.

394
00:14:31,843 --> 00:14:33,578
They cancelled, "I Sued My Cat"
by the way.

395
00:14:33,603 --> 00:14:35,111
Well really the cats were never gonna

396
00:14:35,146 --> 00:14:36,531
be found guilty so.

397
00:14:36,776 --> 00:14:38,593
She can't do what you do.

398
00:14:39,671 --> 00:14:40,992
Well you got that right.

399
00:14:41,453 --> 00:14:43,732
She certainly doesn't
have the savoir faire

400
00:14:43,757 --> 00:14:45,523
to pull off this caftan.

401
00:14:45,548 --> 00:14:47,290
She's taking the show to daytime.

402
00:14:47,325 --> 00:14:48,492
Daytime?

403
00:14:49,424 --> 00:14:50,554
Daytime!

404
00:14:52,818 --> 00:14:53,851
Soozie!

405
00:14:54,063 --> 00:14:54,779
Soozie!

406
00:14:54,859 --> 00:14:55,960
Yeah. Where's my pillow?

407
00:14:55,985 --> 00:14:57,023
Coming!

408
00:15:15,094 --> 00:15:16,094
Okay...

409
00:15:16,554 --> 00:15:18,888
Meet me in the laundry
room in ten minutes.

410
00:15:18,923 --> 00:15:20,914
I found a way out
through the septic system.

411
00:15:23,421 --> 00:15:25,127
We can just go through the front door.

412
00:15:25,146 --> 00:15:26,146
Sure. Yup.

413
00:15:27,941 --> 00:15:30,101
- Good work, Soozie.
- Thank you.

414
00:15:36,630 --> 00:15:38,265
Bernice Lundquist.

415
00:15:40,862 --> 00:15:43,429
Um does this mean I'm allowed
to use the copier again?

416
00:15:43,591 --> 00:15:44,976
No, it does not.

417
00:15:46,483 --> 00:15:47,898
Alex Pike.

418
00:15:53,324 --> 00:15:54,440
Congratulations, graduates.

419
00:15:54,459 --> 00:15:56,137
You have successfully
completed six and a half hours

420
00:15:56,161 --> 00:15:57,531
of sensitivity training.

421
00:15:57,556 --> 00:15:59,289
Your certificate is valid for one year

422
00:15:59,314 --> 00:16:01,062
- pending good behavior.
- Yeah!

423
00:16:02,116 --> 00:16:03,367
I'm a graduate!

424
00:16:03,976 --> 00:16:05,296
Me too!

425
00:16:05,805 --> 00:16:07,984
Oh I wish my mom could see me right now.

426
00:16:09,648 --> 00:16:11,724
Bernice, would you and Mrs. Beasley

427
00:16:11,760 --> 00:16:14,453
care to have a little
celebratory drinky-poo?

428
00:16:14,515 --> 00:16:16,548
Oh...

429
00:16:16,573 --> 00:16:17,573
Okay.

430
00:16:19,035 --> 00:16:22,078
Do you think I could fit
this entire thing in my mouth?

431
00:16:31,425 --> 00:16:34,031
My Teen Choice Awards surfboard?!

432
00:16:34,504 --> 00:16:37,071
I haven't even had a
chance to ride that yet!

433
00:16:37,145 --> 00:16:38,460
Come on!

434
00:16:44,336 --> 00:16:47,304
Everyone stop what
you're doing this instant!

435
00:16:48,191 --> 00:16:49,690
What have you done to my network.

436
00:16:49,725 --> 00:16:51,809
And what are you doing to my office?

437
00:16:51,844 --> 00:16:54,311
Sam, they let you out early!

438
00:16:54,346 --> 00:16:55,980
How did that happen?

439
00:16:56,015 --> 00:16:59,076
Apparently they let
me out just in time, Paige.

440
00:16:59,101 --> 00:17:02,467
Joseph is not and never will be daytime,

441
00:17:02,492 --> 00:17:03,498
not on my watch.

442
00:17:03,523 --> 00:17:04,765
Thank you, Sam.

443
00:17:05,851 --> 00:17:08,281
Well, it's too bad you feel that way.

444
00:17:08,765 --> 00:17:12,281
Because corporate is
totally gushing over this idea.

445
00:17:13,609 --> 00:17:14,498
They are?

446
00:17:14,533 --> 00:17:16,967
As a matter of fact,
I just finished telling them

447
00:17:16,992 --> 00:17:19,220
about how this whole daytime
thing originated with you.

448
00:17:19,851 --> 00:17:21,382
Yeah, huh.

449
00:17:21,407 --> 00:17:22,756
Wait a minute. Wait a minute.

450
00:17:22,809 --> 00:17:24,842
After all, you were
the one who championed Joe

451
00:17:24,877 --> 00:17:27,645
as the perfect antidote to
Kathie Lee and Hoda.

452
00:17:28,085 --> 00:17:30,731
That definitely
sounds like me saying that.

453
00:17:31,187 --> 00:17:33,498
Paige, I know what you're trying to do,

454
00:17:33,523 --> 00:17:35,069
and it's not gonna work!

455
00:17:35,104 --> 00:17:37,238
They are so impressed
by the way we re-org'd

456
00:17:37,273 --> 00:17:39,521
the show they're thinking
about bringing you back

457
00:17:39,546 --> 00:17:40,859
to the New York office!

458
00:17:41,601 --> 00:17:43,577
- The Parent Network?
- Yes!

459
00:17:44,237 --> 00:17:46,976
Oh, Soozie, it's all happening.

460
00:17:47,289 --> 00:17:50,273
They're calling us back
to the mothership!

461
00:17:50,531 --> 00:17:51,418
Yay!

462
00:17:51,454 --> 00:17:53,632
Beam us up!

463
00:17:54,207 --> 00:17:56,539
Whoa, whoa, whoa, whoa, whoa,
whoa, whoa, whoa, whoa, whoa.

464
00:17:56,647 --> 00:17:58,039
No, no, no, no wait a second.

465
00:17:58,064 --> 00:18:01,023
I'm sorry Joseph.
But we have to stay the course.

466
00:18:01,048 --> 00:18:03,078
However, if we hit an iceberg,

467
00:18:03,103 --> 00:18:05,492
then Paige will go down with the ship.

468
00:18:05,527 --> 00:18:06,527
Sam!

469
00:18:06,609 --> 00:18:09,959
I just spent two hours with
you in Soozie's New Kids car

470
00:18:09,994 --> 00:18:12,218
of you going on and on about how the

471
00:18:12,243 --> 00:18:14,460
Joey Mac Show belongs at late night!

472
00:18:14,485 --> 00:18:16,632
That was before I
heard my brilliant idea

473
00:18:16,667 --> 00:18:18,467
to move it to daytime!

474
00:18:18,502 --> 00:18:20,386
Daytime is my middle name!

475
00:18:20,562 --> 00:18:22,882
Thank me later, I found our first guest.

476
00:18:23,203 --> 00:18:26,203
Allan lives his life as a dog.

477
00:18:26,373 --> 00:18:27,538
Don't you, Allan?

478
00:18:27,563 --> 00:18:28,895
It's true, I do.

479
00:18:29,129 --> 00:18:31,463
I should like to go for a walk please.

480
00:18:31,482 --> 00:18:33,773
Amazing!

481
00:18:33,798 --> 00:18:35,184
I don't know who this woman is

482
00:18:35,236 --> 00:18:36,648
but she speaks my language.

483
00:18:36,673 --> 00:18:39,039
Tiffany, you are a daytime genius!

484
00:18:39,087 --> 00:18:40,087
Walkies.

485
00:18:42,696 --> 00:18:44,140
Stop! Would you stop!

486
00:18:45,000 --> 00:18:46,585
Are you kidding me?

487
00:18:46,953 --> 00:18:47,882
I'm done!

488
00:18:47,907 --> 00:18:49,498
Yes, we are done here.

489
00:18:49,533 --> 00:18:51,917
Moving to daytime,
I'm glad we all agree.

490
00:18:51,953 --> 00:18:53,453
No we don't all agree.

491
00:18:53,718 --> 00:18:57,122
It's sad that it took
a man living his life

492
00:18:57,157 --> 00:18:59,195
as a dog for me to get to this point.

493
00:18:59,762 --> 00:19:01,382
- I quit.
- No.

494
00:19:01,510 --> 00:19:02,214
No!

495
00:19:02,239 --> 00:19:03,572
He's allergic to ratings?

496
00:19:03,597 --> 00:19:04,601
Paige!

497
00:19:04,891 --> 00:19:07,984
If he doesn't come back,
you don't come back either.

498
00:19:08,085 --> 00:19:10,296
Joe, let's talk about this!

499
00:19:11,113 --> 00:19:12,171
Joe!

500
00:19:12,558 --> 00:19:14,725
I'm sorry I think I just blanked out

501
00:19:14,750 --> 00:19:16,200
there for a moment.

502
00:19:16,444 --> 00:19:18,648
Did Joey just say he quit?

503
00:19:21,958 --> 00:19:24,804
Oh now that's unpleasant.

504
00:19:25,479 --> 00:19:26,479
Oops.

505
00:19:26,572 --> 00:19:28,255
Howard, get the pooper scooper.

506
00:19:28,280 --> 00:19:31,078
In my defense, I
did say I had to go walkies.

507
00:19:32,476 --> 00:19:34,046
Bad dog.

508
00:19:40,427 --> 00:19:41,710
I won!

509
00:19:41,735 --> 00:19:44,552
I won! I won!

510
00:19:44,722 --> 00:19:45,789
Thanks, loser!

511
00:19:46,040 --> 00:19:47,406
Oh come on!

512
00:20:06,243 --> 00:20:08,945
♪ It's the middle of the night ♪

513
00:20:09,472 --> 00:20:12,031
♪ And I'm feeling all right ♪

514
00:20:13,463 --> 00:20:14,953
♪ Hello ♪

515
00:20:16,287 --> 00:20:19,671
♪ It's The Joey Show ♪

516
00:20:22,366 --> 00:20:25,078
synced and corrected by susinz
*www.addic7ed.com*

