1
00:01:27,787 --> 00:01:31,883
This film is based on a true story

2
00:02:10,096 --> 00:02:12,860
Paul Krantz and Guenter Scheller
hereby describe the course of events,

3
00:02:12,899 --> 00:02:16,357
so no misunderstandings may arise.

4
00:02:17,704 --> 00:02:19,763
Tonight we will take revenge

5
00:02:20,874 --> 00:02:24,970
Revenge on those we love
and who betrayed our love.

6
00:02:26,813 --> 00:02:31,409
Then we, Guenter and I,
will depart this life with a smile.

7
00:02:31,951 --> 00:02:33,976
That's the whole truth.

8
00:02:34,020 --> 00:02:39,287
"That's the whole truth."
That's better than any confession.

9
00:02:45,265 --> 00:02:49,497
You've become quite a celebrity
in a rather short time.

10
00:02:50,937 --> 00:02:53,337
"Youths, Weary of Life,
Form Suicide Club"

11
00:02:53,373 --> 00:02:59,278
"Two of Four Members Found Shot Dead"

12
00:02:59,312 --> 00:03:03,009
"19-Year-Old Ringleader in Custody"

13
00:03:03,049 --> 00:03:05,210
"Pathological Motives Suspected"

14
00:03:05,251 --> 00:03:08,618
Tell me about the girl.
Did you...

15
00:03:09,088 --> 00:03:13,115
...you love her that much...
Hilde?

16
00:03:20,466 --> 00:03:21,933
You write here...

17
00:03:23,203 --> 00:03:27,606
"I think that love drove me
to the ultimate step"

18
00:03:27,640 --> 00:03:33,806
"There are girls of such...

19
00:03:34,147 --> 00:03:37,480
...of such intensity
that one is powerless to resist"

20
00:03:38,084 --> 00:03:39,915
What difference does it make now?

21
00:03:44,057 --> 00:03:46,787
Did this "suicide club" really exist?

22
00:03:51,197 --> 00:03:54,564
Were you the leader,
or was your friend, Scheller?

23
00:03:56,636 --> 00:03:58,433
What does it matter?

24
00:04:04,444 --> 00:04:07,880
How come you're still alive?

25
00:04:16,489 --> 00:04:20,755
"Then we, Guenter and I,

26
00:04:20,793 --> 00:04:23,990
will depart this life with a smile"

27
00:04:26,499 --> 00:04:28,490
You don't know anything

28
00:04:30,737 --> 00:04:35,902
LOVE IN THOUGHTS

29
00:04:40,413 --> 00:04:44,509
Dear Universe,
It all started at school three days ago

30
00:04:44,550 --> 00:04:47,883
FRIDAY
We had detention

31
00:04:48,721 --> 00:04:51,747
That Friday was the first time Guenter
invited me to the Scheller's summer house

32
00:04:51,791 --> 00:04:55,488
His parents were away and I was
looking forward to the weekend

33
00:04:55,528 --> 00:04:57,553
OM SUICIDE

34
00:04:59,532 --> 00:05:02,524
Mainly I wanted to see
his sister Hilde again

35
00:05:05,238 --> 00:05:06,500
And Guenter?

36
00:05:07,140 --> 00:05:10,473
I think he was already far away by then

37
00:05:29,529 --> 00:05:30,621
Gentlemen...

38
00:05:31,764 --> 00:05:33,061
You can leave now

39
00:05:43,276 --> 00:05:44,402
It's beautiful

40
00:05:56,856 --> 00:05:59,450
Bang, bang, old man.
You're dead

41
00:06:49,175 --> 00:06:54,112
I hope my sister's here already.
So do I

42
00:06:55,314 --> 00:06:58,613
Hilde!

43
00:07:04,056 --> 00:07:05,421
Hilde-girl?

44
00:07:13,032 --> 00:07:14,590
Maybe she's not here

45
00:07:17,503 --> 00:07:18,902
Maybe she's at the beach?

46
00:07:25,778 --> 00:07:27,507
I warned you!
Where's Hans?

47
00:07:30,183 --> 00:07:31,582
What's eating you?

48
00:07:32,218 --> 00:07:35,847
Come on, stop it!
If I find that guy, I'll kill him

49
00:07:46,933 --> 00:07:51,063
I haven't seen him in two weeks.
Don't give me that

50
00:07:52,038 --> 00:07:57,237
I can smell the cook in this house.
I fixed myself something to eat

51
00:08:07,753 --> 00:08:10,381
What's he doing here?
Hello, Hilde

52
00:08:10,423 --> 00:08:13,984
This is Paul Krantz, the poet.
You know each other

53
00:08:14,026 --> 00:08:17,894
We've met.
That's possible

54
00:08:26,539 --> 00:08:28,404
This is like paradise

55
00:08:34,113 --> 00:08:36,775
I bet you've never even looked
into one of these books

56
00:08:39,552 --> 00:08:42,112
So what?
I'm just saying

57
00:08:42,889 --> 00:08:45,949
If I had this many books...
You can have them

58
00:08:46,459 --> 00:08:48,017
They're all yours

59
00:08:53,733 --> 00:08:55,064
What's this?

60
00:08:57,403 --> 00:09:01,737
No idea. A weird horoscope
contraption of my mother's

61
00:09:03,743 --> 00:09:05,404
Do you know how it works?

62
00:09:06,045 --> 00:09:08,707
No. Hilde does, though

63
00:09:20,026 --> 00:09:22,551
We talked about Jakob von Hoddis.
The poet

64
00:09:23,262 --> 00:09:24,786
And Georg Heym

65
00:09:26,465 --> 00:09:30,925
The time we met.
You still want to talk about them?

66
00:09:43,082 --> 00:09:44,982
Is Hans your boyfriend?

67
00:09:49,789 --> 00:09:52,758
What makes you say that?
Well, the way Guenter acted...

68
00:09:55,127 --> 00:09:59,393
Hans, Hans...
Hans is just Hans

69
00:09:59,532 --> 00:10:02,000
I thought they were such good friends

70
00:10:04,337 --> 00:10:10,298
These days all he talks about is you.
He seems to think you're a genius

71
00:10:20,386 --> 00:10:22,445
Want to row across the lake with me?

72
00:11:27,787 --> 00:11:29,311
Dinner's ready!

73
00:11:47,139 --> 00:11:48,367
I'd like to read something of yours

74
00:11:52,111 --> 00:11:56,047
None of it's very good yet.
Maybe some other time

75
00:11:56,682 --> 00:11:58,115
I'd like to now

76
00:12:14,366 --> 00:12:15,890
"To Love"

77
00:12:16,836 --> 00:12:20,397
I didn't ask you to stay.
But you stayed

78
00:12:22,041 --> 00:12:26,410
I never asked you who you were
or what you wanted

79
00:12:26,579 --> 00:12:28,308
You were simply there

80
00:12:28,681 --> 00:12:30,876
What do you want from me, love?

81
00:12:31,851 --> 00:12:36,015
Was I not stronger alone?
And did I ever need you?

82
00:12:37,323 --> 00:12:40,315
But stay a bit longer.
Not long,

83
00:12:40,793 --> 00:12:44,229
Just until they stop asking
how I am

84
00:12:44,396 --> 00:12:46,694
And I stop saying, "Fine"

85
00:12:47,833 --> 00:12:50,597
Everyone can tell at a glance:
You are here

86
00:12:51,403 --> 00:12:54,133
If you ever leave me

87
00:12:55,274 --> 00:12:57,265
I will go with you

88
00:12:59,945 --> 00:13:03,244
And? What do you say?

89
00:13:05,985 --> 00:13:08,010
Would you really?

90
00:13:09,522 --> 00:13:13,185
If I loved the person, yes.
You believe in true love?

91
00:13:13,225 --> 00:13:14,783
Can you think up another poem?

92
00:13:33,445 --> 00:13:36,005
Now?
Yes. One just for me

93
00:13:37,416 --> 00:13:38,781
What?

94
00:13:53,566 --> 00:13:57,662
Why the smile?

95
00:14:01,740 --> 00:14:06,643
A comely leg shows off a hem of pink.
What a lovely, saucy sight

96
00:14:08,247 --> 00:14:13,378
Wonder what it might conceal, I think?
When I dare to ask, she pales in fright

97
00:14:18,924 --> 00:14:20,949
My, my, Herr Krantz.

98
00:14:21,293 --> 00:14:22,988
You're a naughty one

99
00:14:38,510 --> 00:14:40,171
You want me to leave?

100
00:14:42,548 --> 00:14:45,449
No, no.
I'm pretty tired anyway

101
00:14:48,554 --> 00:14:50,249
I'll return the favour

102
00:15:04,503 --> 00:15:06,664
Did I promise you too much?

103
00:16:06,498 --> 00:16:07,829
Paul...

104
00:16:10,302 --> 00:16:11,735
Come with me

105
00:18:03,782 --> 00:18:07,047
SATURDAY

106
00:18:12,391 --> 00:18:13,915
I'm going into town.

107
00:18:14,660 --> 00:18:17,094
Why? 
- I'm meeting someone...

108
00:18:18,831 --> 00:18:20,025
... Elli?

109
00:18:25,404 --> 00:18:29,568
Oh, stay here with Paul and me.
- You have a crush on him?

110
00:18:31,376 --> 00:18:32,638
No.

111
00:18:33,879 --> 00:18:38,976
He's a friend from school.
What about you? Do you like him?

112
00:18:39,618 --> 00:18:44,920
We'll see.
- Hilde.

113
00:18:45,290 --> 00:18:47,952
Yes?
- Promise to be back by 8:00.

114
00:18:48,093 --> 00:18:51,722
Why?
- I have a surprise for you.

115
00:18:52,464 --> 00:18:54,932
Bring Elli, Lottchen, Rosa, 
and the others, too.

116
00:18:56,101 --> 00:18:58,001
Yes, Great Manitou.

117
00:18:59,238 --> 00:19:01,399
By 8:00 at the latest!

118
00:19:20,893 --> 00:19:23,361
How's the water?
- Is Hilde up?

119
00:19:23,896 --> 00:19:26,865
She's in town.
She'll be back tonight

120
00:19:27,466 --> 00:19:30,196
Besides, she left something for you.

121
00:19:32,037 --> 00:19:34,528
Let me see.
Come on, let me see!

122
00:19:42,514 --> 00:19:48,544
This book with your heart you wrought.
A poet's soul you have, that's sure.

123
00:19:49,354 --> 00:19:55,020
Dark ideas to capture you have sought,
but the rhymes are clear and pure.

124
00:19:56,495 --> 00:20:01,933
Yet you are young and to me it seems,
that you are really just a boy

125
00:20:02,668 --> 00:20:08,334
Who's known love only in his dreams,
And tarried too long to savour its joy

126
00:20:09,241 --> 00:20:13,905
You'll fail to win the girl you sought
If your ardour in poems alone you show

127
00:20:14,313 --> 00:20:19,649
What use is love only in thought?
When the time comes, you won't know

128
00:20:20,652 --> 00:20:25,919
You should save your bullet, you know.
That's no reason the trigger to pull

129
00:20:26,425 --> 00:20:32,125
Far too many tears would flow
It's just not worth it for such bull

130
00:20:41,440 --> 00:20:44,671
What does she mean,
"Tarried too long to savour its joy?"

131
00:20:44,910 --> 00:20:47,470
That you won't deliver
when it comes down to it

132
00:20:48,480 --> 00:20:50,880
My sister's not one for theory

133
00:20:55,053 --> 00:20:57,112
You think I should try
a more direct approach?

134
00:20:57,155 --> 00:20:59,350
Yes

135
00:21:03,295 --> 00:21:04,694
NO TRESPASSING

136
00:21:09,201 --> 00:21:10,964
It's pretty obvious.
She writes:

137
00:21:11,003 --> 00:21:13,767
"When the time comes,
you won't show"

138
00:21:18,543 --> 00:21:19,908
"You won't know"

139
00:21:21,480 --> 00:21:23,880
Maybe she can tell you're a virgin

140
00:21:27,552 --> 00:21:29,042
Does she have a boyfriend?

141
00:21:30,822 --> 00:21:34,485
I hope not.
Then why did she go to town today?

142
00:21:36,194 --> 00:21:39,789
Cause she knows you're hot for her
and will wait for her all day

143
00:21:40,232 --> 00:21:44,168
She wants you crawling to Berlin,
begging for a moment with her

144
00:21:44,936 --> 00:21:45,925
She's crazy

145
00:21:49,174 --> 00:21:52,905
Hilde likes you, trust me.
She has a thing for proles

146
00:22:09,594 --> 00:22:11,084
Hey, jackass!

147
00:22:33,251 --> 00:22:37,051
May I speak to Hans, please?
From the kitchen

148
00:22:44,429 --> 00:22:45,623
It's me

149
00:22:49,768 --> 00:22:53,670
I was out in the country.
With my brother, who else?

150
00:22:55,440 --> 00:22:59,774
Your son's repeated unexcused absences
from class are a matter of concern

151
00:23:01,380 --> 00:23:02,540
Is that so?

152
00:23:11,223 --> 00:23:13,384
Don't make such a fuss.
I'm coming

153
00:23:13,825 --> 00:23:14,917
See you later

154
00:23:22,134 --> 00:23:34,103
Elli! Get changed,
we're going to the Mokka Efti!

155
00:23:34,279 --> 00:23:35,678
What, so early?

156
00:23:36,648 --> 00:23:38,479
Fine, I'll be right over!

157
00:23:50,562 --> 00:23:52,928
The Mokka Efti is no fun anymore.

158
00:23:54,800 --> 00:23:56,461
Who's coming tonight?

159
00:23:56,501 --> 00:23:59,664
Some of Guenter's friends.
Rosa. Lotte... and Paul

160
00:24:00,539 --> 00:24:04,202
He's coming, too?
Yes. Nice chap. Handsome, too

161
00:24:04,409 --> 00:24:08,971
Maybe a bit shy, but...
He stayed the night with us

162
00:24:09,114 --> 00:24:12,709
And?
Well, you know me

163
00:24:19,558 --> 00:24:24,052
Maybe you'll find one, too.
Well, you know me

164
00:24:26,531 --> 00:24:28,999
Well, look who's here.
Oh, it's you

165
00:24:30,001 --> 00:24:32,731
You're not dancing.
To that music?

166
00:24:35,006 --> 00:24:38,567
May I join you ladies?
Go ahead. I was just leaving anyway

167
00:24:38,877 --> 00:24:42,438
Perhaps you'd care to join us?
Some other time, maybe

168
00:24:46,985 --> 00:24:50,148
What about you?
Just a teeny little glass?

169
00:24:51,089 --> 00:24:53,182
At the next table...

170
00:24:54,893 --> 00:24:55,757
All right

171
00:25:04,135 --> 00:25:06,262
Hey, doll.
Hey, soup boy

172
00:25:08,773 --> 00:25:11,333
I'd almost given up on you

173
00:25:16,781 --> 00:25:18,305
Ten minutes, okay?

174
00:25:24,322 --> 00:25:27,018
Where were you last night?
Wouldn't you like to know

175
00:25:29,528 --> 00:25:31,758
Today I won't let you off so easy

176
00:25:34,499 --> 00:25:38,595
I'll come over later.
You can't

177
00:25:39,871 --> 00:25:41,930
I have to go back to the country.
I promised Guenter

178
00:25:43,441 --> 00:25:50,074
Now that your parents are finally away,
your jealous brother has to spoil it

179
00:25:50,882 --> 00:25:53,316
Tonight... I'm staying

180
00:25:53,752 --> 00:25:55,481
I'm serious.
So am I

181
00:26:03,261 --> 00:26:04,922
Are you in love with Hans?

182
00:26:08,967 --> 00:26:10,434
I don't know

183
00:26:11,469 --> 00:26:13,460
Such things quickly pass

184
00:26:17,609 --> 00:26:19,133
Never to return

185
00:26:32,257 --> 00:26:33,884
Maybe it's true...

186
00:26:35,760 --> 00:26:39,526
...that people are only truly happy
once in their lives

187
00:26:42,167 --> 00:26:43,566
Just once

188
00:26:46,671 --> 00:26:49,367
And then they are punished for it

189
00:26:52,410 --> 00:26:54,435
For the rest of their lives

190
00:26:56,114 --> 00:27:00,244
The punishment is that
they can never forget that one moment

191
00:27:04,956 --> 00:27:06,924
I think it's best...

192
00:27:08,393 --> 00:27:11,226
...to say goodbye at the right time

193
00:27:13,231 --> 00:27:15,859
Namely when you're the happiest

194
00:27:17,068 --> 00:27:18,695
Precisely then

195
00:27:24,175 --> 00:27:26,166
At the zenith

196
00:27:34,352 --> 00:27:36,252
How quiet it is here

197
00:27:51,403 --> 00:27:53,098
Dear posterity,

198
00:27:53,471 --> 00:27:56,770
These are the statutes
of our suicide club

199
00:27:57,542 --> 00:28:00,272
The members are
Paul Krantz and Guenter Scheller

200
00:28:01,179 --> 00:28:05,206
First: The name of the suicide club
shall be Fehou

201
00:28:05,884 --> 00:28:10,617
Second: Love is the only reason
for which we are willing to die

202
00:28:11,289 --> 00:28:15,419
Third: Love is the only reason
for which we would be willing to kill

203
00:28:16,528 --> 00:28:22,160
Therefore we swear to end our lives
once we no longer feel any love

204
00:28:22,200 --> 00:28:27,832
And we will take all those along
who robbed us of our love.

205
00:29:27,766 --> 00:29:29,063
At last!

206
00:29:29,768 --> 00:29:33,704
I thought we'd have to bore
your friend with jokes all afternoon

207
00:29:34,005 --> 00:29:35,632
Oh, it wasn't that bad

208
00:29:36,708 --> 00:29:38,232
It's quiet today

209
00:29:38,276 --> 00:29:41,006
Could it be that you're
not too unhappy about that?

210
00:29:42,280 --> 00:29:45,909
At least that way you know
you'll be safe in this... jungle

211
00:29:46,050 --> 00:29:49,508
I'm afraid I don't understand.
It doesn't take a Freudian

212
00:29:49,687 --> 00:29:52,485
to see that your many
suitors scare you

213
00:29:52,724 --> 00:29:55,420
That's why you prefer
to attack, right?

214
00:29:57,128 --> 00:30:01,565
I don't know what you're talking about.
I can't believe that

215
00:30:01,599 --> 00:30:04,591
I don't care. I have better things to do
than listen to you

216
00:30:10,542 --> 00:30:14,103
Come, we must leave Herr Zipfer
to his insights

217
00:30:15,280 --> 00:30:18,044
Excuse me, Hilde-girl.
I didn't mean to offend you

218
00:30:20,685 --> 00:30:24,052
By the way,
your fear is quite understandable

219
00:30:28,526 --> 00:30:30,619
What a bang! Not bad

220
00:30:32,230 --> 00:30:33,527
They're crazy!

221
00:30:46,911 --> 00:30:51,678
Would you stop that, you idiots!
Tell him to put on different music

222
00:30:51,716 --> 00:30:53,843
Don't drink, open the bottles!

223
00:30:53,885 --> 00:30:56,012
Why open them if we can't drink?

224
00:30:58,122 --> 00:30:59,419
Watch, guys

225
00:31:05,330 --> 00:31:06,490
Go ahead

226
00:31:16,541 --> 00:31:18,031
Coward

227
00:31:18,509 --> 00:31:21,478
It was just a bit of fun.
Nothing happened, after all

228
00:31:21,512 --> 00:31:23,207
This is not a toy, Jango

229
00:31:28,152 --> 00:31:30,245
If any of us is ready tonight...

230
00:31:31,823 --> 00:31:36,055
If he's really happy...
He can go

231
00:31:38,863 --> 00:31:40,854
And the others will help him do it

232
00:31:42,400 --> 00:31:45,961
Why when he's happy?
Why not?

233
00:31:58,750 --> 00:32:00,149
All right, everybody

234
00:32:01,319 --> 00:32:03,617
Won't be easy to close that door again

235
00:32:04,188 --> 00:32:06,156
My parents haven't been here in ages

236
00:32:11,195 --> 00:32:12,856
Say, where's your sister, anyway?

237
00:32:14,232 --> 00:32:16,496
No idea.
She should be here by now

238
00:32:28,046 --> 00:32:32,244
I came to pick you up.
Sweet of you. Have you met Elli?

239
00:32:34,552 --> 00:32:38,010
We met once. You were with Guenter.
We did?

240
00:32:38,990 --> 00:32:41,925
Never mind. Elli's used to that

241
00:33:23,267 --> 00:33:24,894
What's wrong?

242
00:33:28,272 --> 00:33:29,933
Are you okay?

243
00:33:32,343 --> 00:33:35,335
If you love someone, you love only him

244
00:33:35,380 --> 00:33:38,315
But if you limit yourself to one,
he'll soon try to tell you what to do

245
00:33:38,349 --> 00:33:44,879
You're at his mercy. If he doesn't want
you to study or work, too bad for you

246
00:33:45,623 --> 00:33:50,686
You can like many fellows at a time.
This for one reason, that for another

247
00:33:51,195 --> 00:33:54,892
I heard if you can't decide,
you become a lesbian

248
00:33:57,468 --> 00:34:02,963
The emancipated ones are lesbian, right?
Right. And cola turns your feet brown

249
00:34:03,508 --> 00:34:06,477
You're... a silly little one

250
00:34:06,878 --> 00:34:10,974
The word "naive" doesn't even cover it.
This needs more sugar

251
00:34:11,215 --> 00:34:14,412
I'm liberated, and for me, a handful of
men won't do. I want a whole land-full

252
00:34:17,355 --> 00:34:20,449
You think committing equals weakness

253
00:34:21,192 --> 00:34:22,625
You're talking nonsense

254
00:34:23,661 --> 00:34:26,687
And later?
What about later?

255
00:34:26,731 --> 00:34:32,431
No one will be there to take care of you.
Parroting what that idiot said, huh?

256
00:34:32,470 --> 00:34:33,994
It's true, though

257
00:34:35,573 --> 00:34:37,234
Oh. is that sweet!

258
00:34:38,609 --> 00:34:40,634
Well, if you ask me,

259
00:34:41,312 --> 00:34:44,406
love as we know it won't exist
20 years from now

260
00:34:44,682 --> 00:34:48,277
We will progress
beyond petty possessiveness

261
00:34:48,319 --> 00:34:51,482
We'll live in a free society
and all love each other

262
00:34:52,323 --> 00:34:54,587
It's already like that in Russia

263
00:34:56,160 --> 00:34:58,151
But that's awful! Why?

264
00:35:37,835 --> 00:35:40,099
You have no political consciousness

265
00:35:40,605 --> 00:35:44,974
You do whatever your parents tell you
. That's not true!

266
00:35:45,810 --> 00:35:47,505
What do you know, anyway?

267
00:35:49,046 --> 00:35:53,745
Hey, am I happy now? Do I have to go?

268
00:35:54,285 --> 00:35:57,379
Hello, Guenter!

269
00:36:57,348 --> 00:36:59,873
Hope I'm not disturbing you.
What are you doing here?

270
00:37:00,618 --> 00:37:03,143
I heard rumours of pretty girls

271
00:37:03,287 --> 00:37:05,187
Are you alone?
On the contrary

272
00:37:05,223 --> 00:37:08,351
Look what I brought along

273
00:37:12,463 --> 00:37:15,398
The green fairy!
Where did you get that?

274
00:37:15,633 --> 00:37:21,265
Wouldn't you like to know.
Anyone up for a little trip tonight?

275
00:38:40,818 --> 00:38:45,118
What shall we drink to? Paul

276
00:38:52,663 --> 00:38:54,062
Try this

277
00:38:55,499 --> 00:38:57,467
To the beauty of youth

278
00:38:58,836 --> 00:39:00,235
To us

279
00:41:16,106 --> 00:41:17,903
Have you ever had a girlfriend?

280
00:41:19,577 --> 00:41:22,375
That was a sad story.
I see

281
00:41:24,615 --> 00:41:28,142
Want to hear it anyway?
In that case, no

282
00:41:31,255 --> 00:41:33,519
One, two...

283
00:41:33,858 --> 00:41:35,416
And let it flow

284
00:41:53,143 --> 00:41:57,477
Sadly, it's simple. People come
in two kinds: those who love

285
00:41:57,815 --> 00:42:01,717
and those who are loved.
like my sister

286
00:42:02,253 --> 00:42:06,383
That's not the problem.
What is, then?

287
00:42:08,492 --> 00:42:10,653
How long have you known Paul?

288
00:42:16,967 --> 00:42:18,525
I have no idea

289
00:42:19,436 --> 00:42:20,698
For...

290
00:42:23,641 --> 00:42:25,233
I don't remember

291
00:42:26,710 --> 00:42:28,701
Which kind is he?

292
00:42:30,414 --> 00:42:32,507
A bit of both, maybe

293
00:42:38,889 --> 00:42:40,516
What a beautiful night

294
00:42:51,802 --> 00:42:56,762
If we could stop time now...
Right now, at this very moment

295
00:42:59,476 --> 00:43:03,776
It's perfect.
There's everything we could want

296
00:43:12,623 --> 00:43:14,716
Let's just go somewhere

297
00:43:58,102 --> 00:44:02,300
I told you not to show your face again!
Oh, come on

298
00:44:02,806 --> 00:44:04,797
And leave Hilde alone!

299
00:44:07,177 --> 00:44:08,872
Get out!

300
00:44:10,481 --> 00:44:11,880
Get out!

301
00:44:18,022 --> 00:44:19,887
Get out of my life

302
00:44:43,947 --> 00:44:48,213
Hey, what's going on?
There's thirsty people out here

303
00:44:50,354 --> 00:44:52,515
Go away, Bittner!
Guenter?

304
00:44:56,493 --> 00:44:57,687
Go on, scram

305
00:44:58,362 --> 00:44:59,852
Get lost

306
00:45:00,164 --> 00:45:01,529
Got you

307
00:45:44,842 --> 00:45:46,469
You're so beautiful

308
00:45:50,914 --> 00:45:53,508
I never want to belong to one guy only

309
00:45:54,084 --> 00:45:55,745
Not even you

310
00:46:53,610 --> 00:46:56,579
What use is love in thoughts

311
00:47:12,329 --> 00:47:16,595
What are you doing up there?
The view is good from here

312
00:47:21,071 --> 00:47:22,663
Want to come up?

313
00:47:40,023 --> 00:47:45,427
I still have some left, if you like.
Did you drink all that by yourself?

314
00:48:02,713 --> 00:48:07,946
When was it that we met?
This last winter. At the Mokka Efti

315
00:48:08,285 --> 00:48:11,152
With Hilde and Guenter. And Hans

316
00:48:12,522 --> 00:48:14,353
You were there that time?

317
00:48:20,898 --> 00:48:24,994
We went to a movie afterward,
but you didn't come along

318
00:48:26,536 --> 00:48:28,367
You were short on money...

319
00:48:31,275 --> 00:48:33,675
But the others didn't notice that

320
00:48:49,860 --> 00:48:51,293
Shit!

321
00:49:03,907 --> 00:49:06,876
Hey, Hilde.
Not in bed yet?

322
00:49:07,010 --> 00:49:10,104
How did <you> get here?
Took the zeppelin

323
00:49:34,271 --> 00:49:37,434
Oh, you two know each other?

324
00:50:21,752 --> 00:50:25,449
You're right.
It really is nice up here

325
00:50:40,170 --> 00:50:42,570
What, Hilde,
no one loves you anymore?

326
00:50:43,774 --> 00:50:45,401
No one loves me anymore, either

327
00:50:47,244 --> 00:50:48,438
And no wonder

328
00:50:49,346 --> 00:50:52,543
Want a ride to the city?
Why would I go to the city?

329
00:50:53,383 --> 00:50:57,979
The party is past its prime.
You and your smart-aleck comments

330
00:50:59,556 --> 00:51:02,525
The city, anyone?
It's going to rain soon anyway

331
00:51:03,460 --> 00:51:08,591
Has anyone seen Paul?
Yeah, a while ago. With you, I think

332
00:51:12,469 --> 00:51:14,699
Oh, God. My car

333
00:51:21,745 --> 00:51:23,178
Hey, you old lush

334
00:51:55,612 --> 00:51:56,874
Hey, Paul

335
00:51:57,347 --> 00:51:58,905
How are you?

336
00:51:59,950 --> 00:52:02,612
Hey, Paul, where were you?
Swimming

337
00:52:03,653 --> 00:52:05,211
How about you, Paul?

338
00:52:06,323 --> 00:52:09,417
Sorry, what?
I can tell your future

339
00:52:10,927 --> 00:52:13,828
Come on, this is your chance

340
00:52:18,201 --> 00:52:20,726
I don't believe in that stuff.
Afraid?

341
00:52:21,271 --> 00:52:24,331
Nonsense.
Says you

342
00:52:26,176 --> 00:52:27,507
Sit down

343
00:52:34,951 --> 00:52:40,082
Only one question. The answer's
inside you, I just help you find it

344
00:52:40,457 --> 00:52:44,086
Just one question.
So consider it well

345
00:52:44,494 --> 00:52:47,691
Ask if you should become a teacher.
Right

346
00:52:47,898 --> 00:52:51,493
How is that supposed to work?
It's a...

347
00:52:51,668 --> 00:52:54,831
...horoscope-mobile.
It's controlled by your subconscious

348
00:52:54,871 --> 00:52:56,463
Pipe down, everyone

349
00:52:57,407 --> 00:52:59,898
It works through a kind of magnetism

350
00:53:00,410 --> 00:53:02,037
You have your question?

351
00:53:14,157 --> 00:53:15,556
Yes

352
00:53:16,693 --> 00:53:18,285
Give me your hands

353
00:53:25,669 --> 00:53:29,765
You have to concentrate.
Close your eyes

354
00:53:32,642 --> 00:53:34,576
Everyone be quiet

355
00:53:36,413 --> 00:53:38,608
Now pose your question

356
00:54:52,956 --> 00:54:54,617
It's the Hermit

357
00:54:55,191 --> 00:54:57,386
What did he get?
The Hermit

358
00:54:57,427 --> 00:55:03,093
What does that mean?
Obvious. You'll be alone a long time

359
00:55:10,173 --> 00:55:12,573
What's that got to do with my question?

360
00:55:14,377 --> 00:55:16,106
That's your fate

361
00:55:40,637 --> 00:55:42,468
What was your question?

362
00:55:44,574 --> 00:55:47,008
Whether you'll outlive me, Macke

363
00:55:57,220 --> 00:55:58,915
That's a bunch of baloney

364
00:56:15,939 --> 00:56:18,237
Why do you all fall for her?

365
00:56:37,093 --> 00:56:38,993
I'd sleep with you

366
00:56:43,633 --> 00:56:45,464
Over there, in the woods

367
00:59:43,079 --> 00:59:45,479
I know you have a crush on Hilde

368
00:59:47,483 --> 00:59:49,678
But there's no need to pity me

369
01:00:05,902 --> 01:00:07,631
I don't

370
01:01:35,091 --> 01:01:36,820
You can go on ahead

371
01:02:08,991 --> 01:02:12,085
SUNDAY

372
01:04:23,759 --> 01:04:25,420
I'm going to school

373
01:04:30,666 --> 01:04:33,066
There's no school today, Elli

374
01:05:27,690 --> 01:05:28,952
Does the shirt fit you?

375
01:05:32,395 --> 01:05:34,659
I'm kind of hungry, actually

376
01:05:35,932 --> 01:05:39,663
Yeah, I'll go look in the kitchen.
I'm sure I'll find something

377
01:05:41,337 --> 01:05:42,964
Anyone else want anything?

378
01:05:44,173 --> 01:05:47,165
Hilde, can you come with me?
Don't feel like it

379
01:05:47,543 --> 01:05:49,340
We have guests.
Come on, please

380
01:06:04,327 --> 01:06:07,785
Listen, loser.
You can forget about my girl

381
01:06:07,997 --> 01:06:10,192
She doesn't like schoolboys.
Oh, yeah?

382
01:06:13,669 --> 01:06:18,106
Sorry, but could you maybe
look after Paul a bit?

383
01:06:19,141 --> 01:06:20,972
So he isn't so...

384
01:06:21,110 --> 01:06:24,671
Are you crazy?
Don't get all in a lather

385
01:06:25,781 --> 01:06:27,806
Yesterday was swell, wasn't it?

386
01:06:30,453 --> 01:06:33,422
And besides, I have the feeling
he's got a crush on you

387
01:06:33,789 --> 01:06:35,051
So what if he does?

388
01:06:36,659 --> 01:06:39,355
You're really...
I'm serious, Hilde

389
01:06:40,529 --> 01:06:43,191
This is my last chance with Hans

390
01:06:46,235 --> 01:06:50,604
I'm going to bed.
What? At 5.00 P.M.? Why?

391
01:06:50,639 --> 01:06:52,106
I'll sleep in the master bedroom

392
01:07:08,691 --> 01:07:13,321
Here, take this.
Now we can relax a bit

393
01:07:14,797 --> 01:07:17,789
Where's your sister?
She went to bed

394
01:07:18,100 --> 01:07:20,466
Why did she do that?
She was tired

395
01:07:22,371 --> 01:07:25,067
You mean she's not coming back?

396
01:07:26,575 --> 01:07:30,102
So? We can ask the others
to come by after school

397
01:07:30,146 --> 01:07:33,343
It's Sunday.
Or we can ring at Elli's

398
01:07:33,916 --> 01:07:37,283
She lives two doors down.
Crap idea

399
01:07:53,702 --> 01:07:55,135
What's the matter with you two?

400
01:08:41,784 --> 01:08:44,218
Ask Guenter what time it is

401
01:08:46,422 --> 01:08:48,413
Go on, ask him.
What for?

402
01:08:49,692 --> 01:08:51,182
Just do it

403
01:08:52,661 --> 01:08:54,629
It's really hilarious

404
01:08:59,902 --> 01:09:01,392
What time is it?

405
01:09:16,986 --> 01:09:20,114
I've got to take a leak
Again?

406
01:09:20,422 --> 01:09:23,186
Why? What time is it?

407
01:09:35,971 --> 01:09:38,531
Paul, don't you want to hit the sack?

408
01:09:41,277 --> 01:09:43,643
You can sleep here if you like

409
01:09:45,681 --> 01:09:47,672
You can finish your beer first.

410
01:09:49,785 --> 01:09:52,276
You're making such a fool of yourself

411
01:09:54,456 --> 01:09:56,253
You think so?

412
01:10:28,958 --> 01:10:30,448
It's me

413
01:11:44,733 --> 01:11:46,826
What was that?
Hilde...

414
01:11:50,806 --> 01:11:52,535
Never mind them

415
01:11:53,809 --> 01:11:57,472
I'll protect you, don't worry

416
01:12:21,270 --> 01:12:23,534
I think I love you, Hans

417
01:12:32,815 --> 01:12:34,544
The zenith

418
01:12:41,490 --> 01:12:44,687
If you go, I'm going with you

419
01:13:00,008 --> 01:13:03,808
Paul Krantz and Guenter Scheller
hereby describe the course of events,

420
01:13:03,846 --> 01:13:06,508
so no misunderstandings may arise

421
01:13:08,217 --> 01:13:10,777
Tonight we will take revenge

422
01:13:11,353 --> 01:13:15,813
Revenge on those we love
and who betrayed our love

423
01:13:17,326 --> 01:13:21,888
Then we, Guenter and I,
will depart this life with a smile

424
01:13:23,866 --> 01:13:25,834
That's the whole truth

425
01:13:30,406 --> 01:13:32,237
Dear Universe,

426
01:13:33,175 --> 01:13:36,201
A tiny piece of the whole will die

427
01:13:36,245 --> 01:13:37,940
Don't be upset

428
01:13:39,481 --> 01:13:43,008
You will barely feel the loss
of one cell being extinguished

429
01:13:44,153 --> 01:13:46,314
Time rolls on

430
01:13:49,091 --> 01:13:51,719
What is such a little life?

431
01:13:53,529 --> 01:13:56,828
A brief flicker in the world,

432
01:13:57,633 --> 01:14:00,193
then merely ashes and dust

433
01:15:49,311 --> 01:15:50,972
I'm leaving

434
01:15:57,819 --> 01:16:00,151
It's just not worth it

435
01:16:11,500 --> 01:16:13,661
I'm going home

436
01:16:17,372 --> 01:16:19,738
You left long ago

437
01:16:46,034 --> 01:16:49,128
Hilde, open the door

438
01:16:57,079 --> 01:16:58,740
What happened to you?

439
01:17:07,656 --> 01:17:09,089
Where is he?

440
01:17:10,292 --> 01:17:12,726
If you mean Hans, he's long gone

441
01:17:38,387 --> 01:17:40,685
What did you do that for?

442
01:17:44,359 --> 01:17:47,624
Morning. Oh, you're not dressed yet.
Morning

443
01:17:49,097 --> 01:17:50,689
Hello. Elli

444
01:17:51,099 --> 01:17:52,259
Hello

445
01:17:52,300 --> 01:17:53,699
Come

446
01:18:03,979 --> 01:18:07,244
What's wrong?
They've been acting crazy all night

447
01:18:37,245 --> 01:18:40,908
I had to hide Hans.
He stayed the night

448
01:18:41,316 --> 01:18:43,284
I have something to tell you, too

449
01:20:53,545 --> 01:20:55,500
Guenter Scheller was my friend

450
01:20:56,513 --> 01:20:58,388
probably the only one

451
01:21:00,532 --> 01:21:02,667
That crazy night we agreed

452
01:21:03,582 --> 01:21:06,152
that the four of us would commit suicide

453
01:21:13,637 --> 01:21:19,229
The fact that...that I later didn't

454
01:21:23,290 --> 01:21:25,858
...that I later didn't believe in that possibility anymore

455
01:21:29,372 --> 01:21:32,749
doesn't change the fact that our thoughts were wrong

456
01:21:36,154 --> 01:21:37,680
it doesn't change anything

457
01:21:41,506 --> 01:21:43,505
The court will now withdraw for discussion

458
01:21:43,852 --> 01:21:45,583
The hall shall be cleared during that time

459
01:22:04,379 --> 01:22:06,000
The verdict:

460
01:22:07,480 --> 01:22:12,744
The defendant Paul Krantz will spend three weeks
in jail for possession of illegal firearms

461
01:22:13,887 --> 01:22:16,785
He is cleared of all other accusations

462
01:22:17,439 --> 01:22:21,009
The time he already spent in jail during
investigation will be taken into account

463
01:23:04,041 --> 01:23:06,589
Supplement by Guenter Scheller...

464
01:23:07,580 --> 01:23:09,051
Dear space...

465
01:23:09,546 --> 01:23:12,315
When we don't exist anymore,
nobody shall miss us.

466
01:23:12,667 --> 01:23:15,296
Not even one tear shall be shed for us

467
01:23:16,330 --> 01:23:20,158
If anyone may want to remember us,
he shall do it with joy.

468
01:23:21,173 --> 01:23:22,257
Because it's like this:

469
01:23:23,181 --> 01:23:25,532
we did the only right thing

470
01:23:26,870 --> 01:23:28,683
...we lived

471
01:23:30,799 --> 01:23:34,838
In 1931 'Die Mietskaserne', the first novel by Paul Krantz,
was published under the synonym Ernst Erich Noth.

472
01:23:35,466 --> 01:23:40,113
On the 10th of May 1933 the novel was burned
by the Nazis during their book burnings.

473
01:23:40,820 --> 01:23:46,734
Paul Krantz fled from Germany on the 5th of March 1933
and went into exile.

474
01:23:48,939 --> 01:23:52,734
Hilde Scheller suffered what could be called a
witch hunt after these events.

475
01:23:53,625 --> 01:23:59,242
Because of the obscene coverage about her
she was forced to leave the city.

476
01:23:59,911 --> 01:24:01,510
She went on to become a librarian.

477
01:24:03,670 --> 01:24:05,719
Elli never got married.

