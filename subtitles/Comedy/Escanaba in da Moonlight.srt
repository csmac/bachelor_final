﻿1
00:00:00,070 --> 00:00:05,960
<i>KoreanDramaX.com</i>

2
00:00:05,960 --> 00:00:10,760
This drama is a fiction motivated by the people in the history. So it's different from the actual history.

3
00:00:31,450 --> 00:00:33,350
Byeong Yeon.

4
00:00:39,110 --> 00:00:41,930
Arrest him immediately!

5
00:00:44,680 --> 00:00:48,640
No one, no one move!

6
00:00:59,140 --> 00:01:02,800
His Highness's safety has to be top priority!

7
00:01:02,800 --> 00:01:05,020
Everyone, make way!

8
00:02:40,220 --> 00:02:46,500
<i> Although it'll be a very difficult fight, I need to know what kind of society the White Cloud Society is and</i>

9
00:02:46,500 --> 00:02:50,860
<i> what the wall that separates me and the people is.</i>

10
00:02:52,570 --> 00:02:55,640
<i> Now, I must find out.</i>

11
00:02:57,950 --> 00:03:01,710
<i> It'll probably be over that wall.</i>

12
00:03:02,790 --> 00:03:04,340
<i> Huh?</i>

13
00:03:06,770 --> 00:03:11,270
<i> The nation you want to create will be over that wall.</i>

14
00:03:13,770 --> 00:03:15,550
<i> Is that it?</i>

15
00:03:17,500 --> 00:03:23,810
<i> I really wish to see it, Your Highness.</i>

16
00:03:24,540 --> 00:03:28,580
<i> If you really want to see it, I'll really need your help.</i>

17
00:03:32,760 --> 00:03:39,790
Please forgive my rudeness, Your Highness.

18
00:03:42,000 --> 00:03:43,940
Byeong Yeon.

19
00:03:45,530 --> 00:03:52,400
I believe that the world the White Cloud Society is waiting for

20
00:03:53,950 --> 00:03:56,620
isn't different from the nation you dream of, Your Highness.

21
00:04:02,270 --> 00:04:05,820
Over the wall that separates you and the people,

22
00:04:09,460 --> 00:04:12,460
the nation that you will create,

23
00:04:16,850 --> 00:04:19,270
I really want to see it.

24
00:04:43,010 --> 00:04:44,980
Don't withdraw your sword!

25
00:04:47,200 --> 00:04:54,160
The moment you put you sword down, I'll lose you.

26
00:05:01,450 --> 00:05:02,730
Your Highness--

27
00:05:02,730 --> 00:05:04,870
Don't withdraw your sword!

28
00:05:08,810 --> 00:05:10,550
This is a command.

29
00:05:15,810 --> 00:05:23,410
For...not being able to obey your command,

30
00:05:26,920 --> 00:05:29,360
I apologize, Your Highness.

31
00:05:32,350 --> 00:05:33,970
No...

32
00:05:49,390 --> 00:05:51,910
No! No...

33
00:06:03,340 --> 00:06:05,330
Stop!

34
00:06:45,320 --> 00:06:47,230
Byeong Yeon!

35
00:06:51,070 --> 00:06:52,940
Move aside.

36
00:06:58,340 --> 00:07:02,310
Y-Your Majesty! Escort His Majesty!

37
00:07:07,390 --> 00:07:09,540
I told you to move aside!

38
00:07:55,120 --> 00:07:57,010
Byeong Yeon.

39
00:08:01,060 --> 00:08:03,190
Don't forget that

40
00:08:06,720 --> 00:08:13,690
if I could trust only one person in this world,

41
00:08:17,540 --> 00:08:20,140
without change, that person would still

42
00:08:24,020 --> 00:08:26,170
be you.

43
00:08:31,120 --> 00:08:32,950
For...

44
00:08:37,550 --> 00:08:48,780
trusting me...thank you <i> (informal).</i>

45
00:09:15,870 --> 00:09:17,900
Byeong Yeon.

46
00:09:25,940 --> 00:09:31,860
Byeong Yeon! Byeong Yeon...

47
00:09:59,000 --> 00:10:01,800
<i> I wish that in my last moment,</i>

48
00:10:01,800 --> 00:10:06,100
<i> I could just be your friend.</i>

49
00:10:21,200 --> 00:10:23,000
Stop.

50
00:10:26,600 --> 00:10:30,500
Why are you leaving the palace at this hour?

51
00:10:36,750 --> 00:10:39,070
Open the door.

52
00:10:45,000 --> 00:10:50,000
It's a Royal Command to transport the criminal to the Royal Investigative Office immediately.

53
00:10:50,000 --> 00:10:54,200
Head Eunuch. I understand.

54
00:10:54,200 --> 00:10:56,800
Open the door.

55
00:11:03,400 --> 00:11:06,100
They're traitors! Close the palace door!

56
00:11:16,400 --> 00:11:18,500
Hurry!

57
00:11:19,800 --> 00:11:21,600
Hurry!

58
00:11:26,600 --> 00:11:28,600
What are you doing? Come out, hurry!

59
00:11:28,600 --> 00:11:31,500
I'll buy some time.

60
00:11:32,400 --> 00:11:34,300
Head Eunuch.

61
00:11:35,600 --> 00:11:38,400
What are you doing? Escort them away, hurry!

62
00:11:39,200 --> 00:11:41,000
-I apologize.<br>-Wait...

63
00:12:10,000 --> 00:12:12,200
We have to leave now.

64
00:12:31,200 --> 00:12:35,200
Hey, wait. We just need a minute.

65
00:12:44,500 --> 00:12:51,800
Look here, I'm his close friend. Would you allow me to at least take his corpse?

66
00:12:51,800 --> 00:12:54,400
Wait!

67
00:13:07,200 --> 00:13:09,000
Byeong Yeon...

68
00:13:45,600 --> 00:13:52,300
<i> Episode 17 <br> The End for the Beginning</i>

69
00:13:54,300 --> 00:13:58,300
Your Highness, did you cough?<br><i> One month later</i>

70
00:14:00,000 --> 00:14:05,480
Your Highness, did you cough?

71
00:14:07,000 --> 00:14:12,040
Your Highness, did you cough?

72
00:14:13,800 --> 00:14:15,600
Your--

73
00:14:18,600 --> 00:14:20,500
Your Highness...

74
00:14:22,600 --> 00:14:27,000
You couldn't have coughed, because you're not here.

75
00:14:27,940 --> 00:14:32,770
You wouldn't have gone to the library either...it's fine.

76
00:14:32,800 --> 00:14:38,800
I can just accept the scolding and the punishment, don't be worried.

77
00:14:39,700 --> 00:14:42,180
Now, let's go to get scolded.

78
00:14:57,380 --> 00:15:01,390
This is your first time visiting our House, isn't it?

79
00:15:02,200 --> 00:15:03,800
Is it?

80
00:15:04,800 --> 00:15:09,400
I've had too many drunk days that I cannot remember.

81
00:15:09,400 --> 00:15:13,400
From now on, please come often.

82
00:15:15,200 --> 00:15:17,200
I'll think about it.

83
00:15:22,270 --> 00:15:27,380
The Crown Prince skipped the Palace Meeting again?

84
00:15:27,400 --> 00:15:32,200
At first, he went against command to execute the criminal on the torture grounds,

85
00:15:32,200 --> 00:15:37,600
and now, he's taking his duties as the Crown Prince so lightly.

86
00:15:38,200 --> 00:15:39,400
Your Majesty.

87
00:15:39,400 --> 00:15:44,500
Those royal petitions are only the 1st batch of the

88
00:15:44,500 --> 00:15:47,200
royal petitions from the Confucian scholars who believe that the Crown Prince should be dethroned.

89
00:15:47,200 --> 00:15:50,400
I said that I wouldn't talk about that matter anymore.

90
00:15:50,400 --> 00:15:52,300
You have to.

91
00:15:53,200 --> 00:15:58,800
Please do not danger this nation's peaceful politics anymore, Your Majesty.

92
00:16:00,320 --> 00:16:06,130
The Crown Prince isn't the one who has endangered this nation's peaceful politics.

93
00:16:09,200 --> 00:16:14,200
You accused him of being connected to the White Cloud Society when you weren't even certain, object to whatever he wants to do,

94
00:16:14,200 --> 00:16:18,800
and whenever you dislike something, you ordered the Confucian scholars to write royal petitions for him to be kicked out of the palace.

95
00:16:18,800 --> 00:16:22,300
That's why I'm saying this.

96
00:16:23,400 --> 00:16:29,800
Your Majesty, the Crown Prince has now lost all respect and sense of duty.

97
00:16:29,800 --> 00:16:34,800
Isn't this obvious as he visits the Courtesan Houses more often than he visits the Palace Meeting Hall?

98
00:16:35,600 --> 00:16:41,400
Do not complicate this nation's political affairs any longer. I believe that dethroning the Crown Prince

99
00:16:41,400 --> 00:16:45,800
and establishing a new Crown Prince is the right thing to do.

100
00:16:46,600 --> 00:16:49,400
-Please reconsider. <br>-Please reconsider.

101
00:16:49,400 --> 00:17:00,000
Please reconsider. <i> Please reconsider. Please reconsider.</i>

102
00:17:05,800 --> 00:17:07,800
-Please come back again, My Lord!<br>-Please come back again, My Lord!

103
00:17:10,400 --> 00:17:12,200
<i>Let's go back in.</i>

104
00:17:12,200 --> 00:17:14,000
Look here.

105
00:17:17,000 --> 00:17:19,400
Look at me!

106
00:17:29,300 --> 00:17:33,200
Is the Prince growing up well?

107
00:17:33,200 --> 00:17:38,200
Yes, of course. He's getting healthier by the day.

108
00:17:38,200 --> 00:17:41,200
Yes, he has to.

109
00:17:41,200 --> 00:17:48,500
He'll become the Crown Prince too, of course he has to be healthy. That is also a huge advantage.

110
00:17:49,800 --> 00:17:51,100
What?

111
00:17:53,000 --> 00:17:57,400
Father, w-what did you just say?

112
00:17:57,400 --> 00:18:00,100
Why are you so shocked?

113
00:18:00,100 --> 00:18:06,600
The Crown Prince's quarters will need a new owner soon, so you should prepare.

114
00:18:06,600 --> 00:18:08,400
Are you serious?

115
00:18:08,400 --> 00:18:13,400
Yes, it will be finalized soon.

116
00:18:13,400 --> 00:18:17,200
Yes. Yes, father.

117
00:18:29,800 --> 00:18:35,600
<i> This is regarding the newborn infant snuck out of the palace through the back doors.</i>

118
00:18:35,600 --> 00:18:42,000
<i> If that child wasn't dead but alive, would the story change?</i>

119
00:18:45,600 --> 00:18:48,800
<i> Your Highness, it's Eunuch Seong!</i>

120
00:18:48,800 --> 00:18:50,700
Come in.

121
00:19:15,000 --> 00:19:20,000
Byeong Yeon, I'm here.

122
00:19:27,800 --> 00:19:34,400
When will you reply to my greeting?

123
00:20:18,900 --> 00:20:23,140
Is Byeong Yeon all right?

124
00:20:23,970 --> 00:20:27,900
His physical wounds are healing.

125
00:20:30,180 --> 00:20:33,510
Perhaps he wants to rest a bit longer.

126
00:20:33,510 --> 00:20:37,070
He's had such a exhausting life.

127
00:20:38,560 --> 00:20:42,980
Perhaps it's because he's preparing to face you.

128
00:20:46,900 --> 00:20:52,360
Is there no word about Ra On?

129
00:20:53,910 --> 00:20:55,750
Yes.

130
00:20:59,300 --> 00:21:01,700
Are you disappointed?

131
00:21:02,700 --> 00:21:04,950
No.

132
00:21:04,950 --> 00:21:10,010
I'm enjoying myself having lost the sense of time.

133
00:21:11,620 --> 00:21:14,080
Game of chasing tail.

134
00:21:17,530 --> 00:21:25,030
I am paying attention to all of Kim family's activities including land registrations, money transactions and people.

135
00:21:25,030 --> 00:21:26,740
Yes.

136
00:21:27,670 --> 00:21:33,560
However, the hunt is not over when you grab hold of the tail.

137
00:21:33,600 --> 00:21:38,920
They could cut the tail off and escape. Please be careful.

138
00:21:39,720 --> 00:21:43,240
Yes, I will keep that in mind.

139
00:21:48,080 --> 00:21:50,620
I'm all prepared.

140
00:21:50,620 --> 00:21:56,870
Please prepare yourself to fill the big hole that will come about at the Royal Court.

141
00:22:08,630 --> 00:22:11,030
Did you look into it?

142
00:22:12,240 --> 00:22:14,800
That is...

143
00:22:14,800 --> 00:22:17,100
Oh dear.

144
00:22:19,510 --> 00:22:21,830
Will you not tell me straight?

145
00:22:21,830 --> 00:22:29,060
Your highness, Sir Kim Yoon Seong was right.

146
00:22:29,060 --> 00:22:31,110
What?

147
00:22:33,190 --> 00:22:39,190
Are you saying that child is really alive?

148
00:22:39,190 --> 00:22:41,230
Yes...

149
00:22:41,230 --> 00:22:47,800
I saw the child being taken care of by the giesha.

150
00:23:06,660 --> 00:23:11,400
Being dethroned does not necessarily lead to unhappiness.

151
00:23:11,400 --> 00:23:18,280
Some have lived a long life enjoying hunting and such.

152
00:23:20,760 --> 00:23:26,980
In some way, that might be a better life than being on the throne.

153
00:23:29,400 --> 00:23:36,640
It does sound attractive but there is one thing that's holding me back.

154
00:23:43,110 --> 00:23:46,950
It is you Prime Minister.

155
00:23:49,090 --> 00:23:55,690
I don't think I can leave the Court as long as you are here.

156
00:24:17,210 --> 00:24:19,260
Grandpa.

157
00:24:22,220 --> 00:24:24,280
Was there no one tailing you?

158
00:24:24,280 --> 00:24:29,170
No. I've checked a few times but it was fine.

159
00:24:29,170 --> 00:24:31,070
I see.

160
00:24:33,620 --> 00:24:36,400
It must've been tough on you.

161
00:24:38,590 --> 00:24:41,900
I heard from mother.

162
00:24:41,900 --> 00:24:46,420
Is Hyung Kim really here?

163
00:24:47,990 --> 00:24:51,840
Is he really alive?

164
00:24:52,960 --> 00:24:57,330
He didn't gain consciousness yet, but he's recovering.

165
00:25:00,340 --> 00:25:05,080
That's a relief. What a relief it really is.

166
00:25:07,280 --> 00:25:09,520
Follow me.

167
00:25:21,970 --> 00:25:23,830
Hyung Kim.

168
00:25:24,740 --> 00:25:27,730
It's me, Sam Nom.

169
00:25:28,610 --> 00:25:33,180
You're still not going to wake up even though I'm here?

170
00:25:38,050 --> 00:25:43,910
It must have been so hard for you to repress and hide yourself all this time.

171
00:25:45,080 --> 00:25:48,630
Is that why you're resting for so long?

172
00:25:53,550 --> 00:25:58,080
Fall might pass at this rate!

173
00:25:59,180 --> 00:26:04,680
You have get up before all the leaves fall off the trees, ok?

174
00:26:17,860 --> 00:26:25,820
Hyung Kim, when it snows for the first time, how the palace look?

175
00:26:28,780 --> 00:26:36,420
When the snow falls on the big court yard at the Repentance Hall and on its low roof

176
00:26:37,790 --> 00:26:41,150
it must be beautiful.

177
00:26:42,750 --> 00:26:44,900
It is beautiful.

178
00:26:49,690 --> 00:26:51,930
Very very much.

179
00:26:53,620 --> 00:26:58,110
Hyung Kim. Are you awake?

180
00:26:58,810 --> 00:27:04,150
You are such a pain. Talking your head off still.

181
00:27:08,690 --> 00:27:10,580
Hyung Kim.

182
00:27:11,340 --> 00:27:14,360
It hurts, it hurts...

183
00:27:36,970 --> 00:27:44,620
♬ Will my heart show through those clouds and the moonlight ♬

184
00:27:44,620 --> 00:27:52,190
♬ I hide at the foot of a faded wall ♬

185
00:27:52,190 --> 00:27:59,690
♬ Even if a nameless wild flower joins me ♬

186
00:27:59,690 --> 00:28:01,360
♬ Why am l just shedding tears by myself? ♬

187
00:28:01,360 --> 00:28:06,490
<i>The one who has more to give than anyone else in Jeseon.</i>

188
00:28:06,490 --> 00:28:10,720
<i>I, Hong Sam Nom, give you this love.</i>

189
00:28:10,720 --> 00:28:16,960
♬ I miss you, because I miss you, you're someone I can't forget anymore ♬

190
00:28:16,960 --> 00:28:22,360
<i>Do you by chance know what Crown Prince's nickname is?</i>

191
00:28:22,360 --> 00:28:26,210
<i>Geez, what is it?</i>

192
00:28:26,210 --> 00:28:27,660
<i>Flower Crown Prince?</i>

193
00:28:27,660 --> 00:28:30,200
<i>The Poop Pavilion.</i>

194
00:28:30,200 --> 00:28:33,140
<i>I'd heard there is no one who doesn't know!</i>

195
00:28:33,140 --> 00:28:35,590
<i>Come on!</i>

196
00:28:38,550 --> 00:28:40,870
<i>You laugh? Geez!</i>

197
00:28:40,870 --> 00:28:45,200
<i>I thought you had a problem smiling!</i>

198
00:29:19,790 --> 00:29:24,300
Were you disappointed that it was me?

199
00:29:24,300 --> 00:29:26,060
No.

200
00:29:26,960 --> 00:29:30,100
I was just a bit surprised.

201
00:29:30,100 --> 00:29:34,740
Actually, I suffered a heartache recently by myself.

202
00:29:34,740 --> 00:29:37,390
Because of the wedding ceremony?

203
00:29:37,390 --> 00:29:43,820
I've found out who is in your heart.

204
00:29:49,200 --> 00:29:53,510
I've become a Crown Princess on a deal

205
00:29:53,510 --> 00:29:59,590
so it would be laughable for me to expect anything else.

206
00:30:02,050 --> 00:30:06,830
So, I've decided to concentrate on my main role.

207
00:30:09,120 --> 00:30:13,400
Though I am unable to comfort you like that woman,

208
00:30:13,400 --> 00:30:19,380
I will remain as your assistant so you can remain as the Crown Prince,

209
00:30:22,800 --> 00:30:28,970
because I'm the one who will remain at your side until the very end, Your Highness.

210
00:30:56,830 --> 00:30:59,450
What have you summoned me here for?

211
00:30:59,450 --> 00:31:01,990
Are you asking because you don't know?

212
00:31:03,180 --> 00:31:06,120
The Crown Prince will be dethroned soon.

213
00:31:06,120 --> 00:31:11,830
When will you stop being so nonchalant of the competition between you and the Prince on who will become the new Crown Prince?

214
00:31:12,770 --> 00:31:18,390
Your Highness the Queen, no, Aunt.

215
00:31:20,020 --> 00:31:24,010
The reason why I am keeping quiet despite knowing the truth...

216
00:31:24,010 --> 00:31:29,720
Though I do not wish to acknowledge, is due to this lame reason of our bloodline.

217
00:31:32,420 --> 00:31:36,160
Even so, I thought you would want to see her for once...

218
00:31:38,820 --> 00:31:44,190
I'm talking about your child, His Majesty's child, that you so pitifully abandoned because of your thirst for power.

219
00:31:47,380 --> 00:31:49,660
Shut up.

220
00:31:49,660 --> 00:31:52,720
Confess it yourself.

221
00:31:52,720 --> 00:31:57,960
That's my last act of consideration I will bestow onto you.

222
00:32:20,030 --> 00:32:27,020


223
00:32:49,270 --> 00:32:52,470
Your Highness, we cannot delay any longer.

224
00:32:52,470 --> 00:32:55,190
Let's hurry and enter the courts.

225
00:32:55,890 --> 00:32:58,420
Wait just for a while.

226
00:32:59,800 --> 00:33:05,380
Your Highness, the Ambassador Jeong Deok Ho has arrived.

227
00:33:13,260 --> 00:33:15,840
So, how did it go?

228
00:33:15,840 --> 00:33:19,420
He was arrested at the pier on the moment he was about to escape from the capital.

229
00:33:19,420 --> 00:33:24,770
After confirming, it was indeed the person who gathered the assassins that time.

230
00:33:27,970 --> 00:33:30,430
Alright.

231
00:33:30,430 --> 00:33:32,160
Let's go.

232
00:33:35,740 --> 00:33:39,480
Without any apology or punishment for the various scandals so far

233
00:33:39,480 --> 00:33:44,460
Now he even goes to the kisaeng and to the gambling.

234
00:33:44,460 --> 00:33:48,660
Not fulfilling the duty of the crown prince and dropping the honor of the royal family,

235
00:33:48,660 --> 00:33:52,050
an order should be made so that his behaviors like this to be stopped immediately.

236
00:34:00,710 --> 00:34:05,920
<i> Your Majesty! The Crown Prince has arrived!</i>

237
00:34:05,920 --> 00:34:07,870
Let him in.

238
00:34:34,790 --> 00:34:39,900
Crown Prince, did you leave the palace again?

239
00:34:39,900 --> 00:34:42,030
Yes, Your Majesty.

240
00:34:43,980 --> 00:34:46,490
Where did you go?

241
00:34:47,690 --> 00:34:53,390
As the Prime Minister said, I went to Courtesan Houses and gambling centers, Your Highness.

242
00:35:02,250 --> 00:35:06,700
They say that you can only be a good ruler if you listen to the cries of the people.

243
00:35:06,700 --> 00:35:10,770
I can't hear anything if I just remain inside the palace.

244
00:35:10,770 --> 00:35:16,500
Thus, I spent the entire night with the people and listened to various worries and advice.

245
00:35:16,500 --> 00:35:21,570
Are those citizens courtesans and gamblers?

246
00:35:25,000 --> 00:35:30,300
That's right. Why? Is that wrong?

247
00:35:31,320 --> 00:35:36,770
Your Majesty, this is something that cannot be allowed.

248
00:35:36,770 --> 00:35:39,640
From ministers and officers of six ministries

249
00:35:39,640 --> 00:35:43,420
the letters of asking to dethrone the Crown Prince is showering.

250
00:35:44,060 --> 00:35:51,220
Your majesty, I ask you earnestly to dethrone the Crown Prince and expel him.

251
00:35:51,220 --> 00:35:57,720
Please consider it

252
00:35:57,720 --> 00:36:01,410
Please consider it

253
00:36:07,810 --> 00:36:11,010
I understand all your intentions.

254
00:36:11,010 --> 00:36:14,730
But even if it's time for me to go,

255
00:36:14,730 --> 00:36:20,160
I was thinking of telling some interesting stories I heard while going around the brothels and gambling dens.

256
00:36:22,140 --> 00:36:24,640
Would you listen to them once?

257
00:36:26,730 --> 00:36:29,720
I ate it well.

258
00:36:32,790 --> 00:36:38,280
Why are you so thin? Was it because your mother wasn't there when you were recovering from birth--

259
00:36:39,270 --> 00:36:43,480
Mother? Watch your mouth.

260
00:36:44,810 --> 00:36:49,110
I beg your pardon, Your Highness.

261
00:36:50,910 --> 00:36:53,420
Where is the child?

262
00:36:54,430 --> 00:36:56,550
Child?

263
00:36:57,670 --> 00:37:01,520
That child that the courtesans took in and are now raising, where is she?

264
00:37:01,520 --> 00:37:05,360
Your Highness, why are you looking for that child?

265
00:37:07,870 --> 00:37:10,200
Could it be...

266
00:37:12,090 --> 00:37:16,480
No...It cannot be, right?

267
00:37:17,550 --> 00:37:19,980
Where did she go?

268
00:37:21,030 --> 00:37:28,810
Just now, someone who said they were from the palace took her.

269
00:37:28,810 --> 00:37:31,050
From the palace?

270
00:37:45,150 --> 00:37:48,870
Crown Prince, what are you doing now?

271
00:37:51,360 --> 00:37:55,860
Your Majesty, please grant me some time.

272
00:37:58,490 --> 00:38:00,450
Go ahead.

273
00:38:05,870 --> 00:38:10,590
Please look at these drawings carefully to see if these faces are familiar.

274
00:38:17,670 --> 00:38:22,880
But...why are all their eyes closed?

275
00:38:22,880 --> 00:38:27,460
Well...What do you think?

276
00:38:31,730 --> 00:38:35,850
It's because they are all dead.

277
00:38:43,000 --> 00:38:48,550
Some times it's because they opened their mouths wrongly; for some it's even before they opened their mouths.

278
00:38:48,550 --> 00:38:53,300
It's probably because the dead do not speak.

279
00:38:53,300 --> 00:38:59,060
I was surprised too. They left quite many evidences.

280
00:39:02,370 --> 00:39:05,880
I traced the corpses of the assassins who raided the East Palace,

281
00:39:05,880 --> 00:39:12,170
and found out who first gathered them together, gave them money, and gave them their orders.

282
00:39:14,820 --> 00:39:18,050
Did we not already find out that that was the doing of the White Cloud Society?

283
00:39:18,050 --> 00:39:20,850
We'll know if we ask them.

284
00:39:20,850 --> 00:39:22,750
Bring him in.

285
00:39:47,480 --> 00:39:52,440
Is the person who told you to gather the assasins in here?

286
00:40:07,200 --> 00:40:09,170
Yes, Your Highness.

287
00:40:12,160 --> 00:40:14,190
Point him out.

288
00:40:25,370 --> 00:40:26,980
It is them.

289
00:40:37,040 --> 00:40:42,270
Who has sent all of the farmlands to these men?

290
00:40:42,270 --> 00:40:46,880
After thorough investigation, it was the Minister of Personnel's.

291
00:40:46,880 --> 00:40:50,670
These are the Minister's secret books.

292
00:40:57,280 --> 00:41:02,690
Your Highness, this does not make sense.

293
00:41:02,690 --> 00:41:08,440
Hurry up and tell us, who's the one who told you to blame the assassination on the White Cloud Society?

294
00:41:08,440 --> 00:41:12,360
It was... the Minister of Personnel and Minister of Finance!

295
00:41:12,360 --> 00:41:15,420
I have committed an unforgivable sin, Your Highness!

296
00:41:21,730 --> 00:41:26,600
Ministers, is what the Crown Prince speaking the truth?!

297
00:41:26,600 --> 00:41:33,350
Your Majesty, how can you believe the words of Crown Prince who is surrounded by gisaengs and gamblers?!

298
00:41:33,350 --> 00:41:36,310
This is all...baseless accusation!

299
00:41:36,310 --> 00:41:39,360
Are you still trying to make excuses?!

300
00:41:39,360 --> 00:41:41,030
Listen, Royal Investigative Officer.

301
00:41:41,030 --> 00:41:41,920
Yes.

302
00:41:41,920 --> 00:41:46,440
Imprison those two immediately at the Royal Investigative Office and investigate their crimes thoroughly!

303
00:41:46,440 --> 00:41:47,730
Yes, Your Highness.

304
00:41:47,730 --> 00:41:50,970
Your Majesty, I am wronged!

305
00:41:50,970 --> 00:41:55,360
Let go! Are you not letting go?

306
00:41:55,360 --> 00:41:58,830
Your Majesty, I have been maligned!

307
00:41:58,830 --> 00:42:05,280
Your Majesty! Let go, let go! I am wronged!

308
00:42:29,140 --> 00:42:34,200
<i>You said that when the tail has been caught, it doesn't mean that the hunt is over, right?</i>

309
00:42:35,730 --> 00:42:41,520
<i>If I want to stop it from escaping by cutting off its own tail, what should I do?</i>

310
00:42:42,410 --> 00:42:47,290
<i>It is of course not easy to catch a tiger with a large body.</i>

311
00:42:48,440 --> 00:42:52,420
<i>First its right hand, then its left.</i>

312
00:42:52,420 --> 00:42:56,250
<i>Until you finally cut off its head,</i>

313
00:42:56,870 --> 00:42:59,470
<i>you must never let your guard down.</i>

314
00:43:14,140 --> 00:43:18,830
There is one more thing I want to show you, Prime Minister.

315
00:43:27,230 --> 00:43:32,200
This is the face of a palace maid who died without a sound a while ago.

316
00:43:34,430 --> 00:43:38,670
It's said that she had given birth to a child right before her death.

317
00:43:40,960 --> 00:43:45,380
However, the child was nowhere to be found.

318
00:43:46,530 --> 00:43:49,040
What might be the hidden story?

319
00:43:50,660 --> 00:43:54,810
Are you not curious about this woman?

320
00:44:11,810 --> 00:44:14,290
<i>So pretty!</i>

321
00:44:15,440 --> 00:44:17,930
<i>Peek-a-boo! Peek-a-boo!</i>

322
00:44:18,620 --> 00:44:22,230
<i>Oh there, there.</i>

323
00:44:22,230 --> 00:44:24,370
<i>Oh...</i>

324
00:44:24,370 --> 00:44:27,980
<i>There, there.</i>

325
00:44:27,980 --> 00:44:31,770
<i>Peek-a-boo! Peek-a-boo!</i>

326
00:44:32,460 --> 00:44:38,370
<i>The child...was taken by somebody supposedly from the palace.</i>

327
00:44:46,570 --> 00:44:48,860
The Crown Prince...a child?

328
00:44:48,860 --> 00:44:55,140
Yes. For some unavoidable reasons he needed to take care of her.

329
00:44:55,140 --> 00:44:59,930
He left her to the East Palace court ladies but I found her so adorable after seeing her

330
00:44:59,930 --> 00:45:03,020
so I begged him to take her with me for a while.

331
00:45:04,220 --> 00:45:06,020
I see...

332
00:45:07,340 --> 00:45:09,480
Please take a look.

333
00:45:12,870 --> 00:45:15,850
Is she not elegant?

334
00:45:37,450 --> 00:45:39,930
Does it not hurt?

335
00:45:41,130 --> 00:45:44,260
Yes, Your Highness.

336
00:45:44,260 --> 00:45:47,060
Why did you sleep for so long?

337
00:45:47,750 --> 00:45:51,040
I guess I did not have the face to see you, Your Highness.

338
00:45:53,810 --> 00:45:59,340
You protected the woman I love and I, didn't you?

339
00:46:02,330 --> 00:46:08,190
More than anything else...Thank you for coming back like this.

340
00:46:22,560 --> 00:46:31,070
That person comes here occasionally and looks after me.

341
00:46:36,960 --> 00:46:42,240
Once I have finished all preparations, I'll bring her back to my side.

342
00:46:45,450 --> 00:46:49,170
I'll make it so that we can laugh, chat,

343
00:46:49,900 --> 00:46:52,750
and be happy like in the past, by my side.

344
00:46:56,420 --> 00:46:58,830
We're almost there.

345
00:47:01,240 --> 00:47:08,990
When the time comes. Shall the 3 of us go back to Repentance Hall

346
00:47:09,910 --> 00:47:11,880
and have a drink?

347
00:47:47,420 --> 00:47:49,920
What did you say?

348
00:47:49,920 --> 00:47:53,450
I asked you why that child must go up on the King's throne.

349
00:47:53,450 --> 00:47:58,070
He doesn't have the blood of a Lee anyway.

350
00:48:02,440 --> 00:48:05,090
How could you say such sophistry...

351
00:48:06,410 --> 00:48:09,970
Please do not insult the royal family of Joseon.

352
00:48:09,970 --> 00:48:16,140
There is nothing more of an insult than trying to foul the royal family's blood.

353
00:48:16,140 --> 00:48:21,770
That child is definitely a prince of this country that I have given birth to.

354
00:48:21,770 --> 00:48:26,510
Yes. So that everyone in the palace believes it,

355
00:48:26,510 --> 00:48:29,370
so that nobody suspects,

356
00:48:29,370 --> 00:48:33,130
you will have to put in effort from now on.

357
00:48:35,150 --> 00:48:36,900
Father.

358
00:48:36,900 --> 00:48:39,970
In the case you are unable to do so,

359
00:48:39,970 --> 00:48:43,480
there will be no way for you to avoid death.

360
00:48:44,870 --> 00:48:47,040
Prime Minister.

361
00:48:50,430 --> 00:48:52,410
Please sit.

362
00:49:00,200 --> 00:49:03,540
Did you think you would survive, Prime Minister?

363
00:49:04,580 --> 00:49:09,490
Are you threatening me now?

364
00:49:10,470 --> 00:49:16,260
I have no choice. What kind of person are you, Prime Minister?

365
00:49:16,260 --> 00:49:20,550
Are you not a person whose lifeline is tied to me?

366
00:49:20,550 --> 00:49:26,210
You are the one who sat such a vulgar woman like myself on this position.

367
00:49:27,300 --> 00:49:33,920
But for that very reason your lifeline is tied to mine, do you really not know?

368
00:49:35,430 --> 00:49:37,830
How could you

369
00:49:37,830 --> 00:49:43,450
place a rootless child who doesn't even know of her own father's face raised by Gisaengs onto the Queen's position?

370
00:49:45,240 --> 00:49:49,560
Is there a bigger sin of treason in insulting the royal family of Joseon

371
00:49:49,560 --> 00:49:51,960
than <i>this</i>?!

372
00:49:52,760 --> 00:49:57,090
Are you not going to shut that mouth?

373
00:49:59,260 --> 00:50:02,250
Be forewarned if you are going to mess with me,

374
00:50:04,250 --> 00:50:08,400
you'd better be firm in your resolve.

375
00:50:19,880 --> 00:50:25,120
When I was young and naive I was very envious of you.

376
00:50:26,230 --> 00:50:29,770
No matter how capable and powerful I become

377
00:50:29,770 --> 00:50:33,060
I would always be a mere servant.

378
00:50:34,310 --> 00:50:38,380
I know. That's why,

379
00:50:38,380 --> 00:50:41,800
I was always hungry for a friend.

380
00:50:43,570 --> 00:50:46,500
No matter how close we seemed

381
00:50:47,460 --> 00:50:48,790
to you all

382
00:50:48,790 --> 00:50:52,190
you are always the Crown Prince we had to bow our heads to.

383
00:50:53,000 --> 00:50:55,800
Is that why

384
00:50:55,800 --> 00:50:59,060
we can always be what we are now?

385
00:51:01,470 --> 00:51:07,340
You, Byeong Yeon and I.

386
00:51:09,790 --> 00:51:13,120
Why can we not avoid fighting each other?

387
00:51:15,850 --> 00:51:18,530
I've always wanted to run away

388
00:51:18,530 --> 00:51:22,140
from my grandfather and

389
00:51:22,140 --> 00:51:24,850
the position of the oldest legitimate son of Kim family.

390
00:51:26,930 --> 00:51:29,420
Though I'm not sure if you would believe me.

391
00:51:31,660 --> 00:51:33,480
I do.

392
00:51:36,600 --> 00:51:40,230
I too wanted to throw away many a times

393
00:51:40,230 --> 00:51:43,500
only if I could.

394
00:51:53,090 --> 00:51:57,030
I am going to do what I have to do.

395
00:51:58,850 --> 00:52:02,680
Please do what you think is right.

396
00:52:02,680 --> 00:52:04,450
But,

397
00:52:06,180 --> 00:52:09,390
just in case you get hurt,

398
00:52:09,390 --> 00:52:11,650
it worries me.

399
00:52:14,360 --> 00:52:19,830
I too worry...that I might become wanting to protect my family.

400
00:52:26,040 --> 00:52:30,750
Therefore, even if something happens

401
00:52:31,870 --> 00:52:35,030
I think we don't need to feel sorry.

402
00:52:35,030 --> 00:52:38,260
For both you and I.

403
00:52:40,990 --> 00:52:43,940
It's not like we started not knowing it could happen.

404
00:53:18,020 --> 00:53:20,920
(8 years ago)

405
00:53:25,370 --> 00:53:27,370
<i>Did you have a look?</i>

406
00:53:27,370 --> 00:53:31,890
<i>Yes, I just looked at the Crown Prince very closely.</i>

407
00:53:31,890 --> 00:53:34,300
<i>Right, what do you think?</i>

408
00:53:34,300 --> 00:53:40,020
<i>He certainly has the face that gives an energy of royalty.</i>

409
00:53:40,020 --> 00:53:44,970
But...there is one more thing...

410
00:53:44,970 --> 00:53:48,490
What is it? Don't hesitate and tell me.

411
00:53:48,490 --> 00:53:55,020
<i>That...is. He's fated to have a short life.</i>

412
00:53:55,020 --> 00:53:58,670
<i>Further, one of his friends</i>

413
00:53:58,670 --> 00:54:01,950
<i>has an extraordinary energy.</i>

414
00:54:04,130 --> 00:54:05,470
<i>Yoon Seong</i>

415
00:54:05,470 --> 00:54:07,760
<i>Uh, grandpa!</i>

416
00:54:07,760 --> 00:54:12,220
<i>A warm personality and a strength as great as a general.</i>

417
00:54:12,220 --> 00:54:16,420
<i>he undoubtedly has the true face of a great king.</i>

418
00:54:16,420 --> 00:54:19,590
<i>An embroidery of a crane on the hat...</i>

419
00:54:22,660 --> 00:54:26,970
<i>If we are to end the reign of the Lee Dynasty...</i>

420
00:54:26,970 --> 00:54:30,360
<i>we need to find a new owner worthy of the throne.</i>

421
00:54:47,590 --> 00:54:49,720
Did you call for me?

422
00:55:32,060 --> 00:55:36,510
Your Highness. The Crown Princess is here.

423
00:55:37,990 --> 00:55:40,050
Please have her come in.

424
00:56:02,060 --> 00:56:05,070
What brings you at this late hour?

425
00:56:05,070 --> 00:56:10,210
Your highness, I've been told that the royal doctor has been bringing you a tonic

426
00:56:10,210 --> 00:56:13,980
to ease your fatigue but you've been refusing them each time.

427
00:56:50,560 --> 00:56:52,850
Please drink it.

428
00:56:53,800 --> 00:56:56,260
Crown Princess.

429
00:56:56,260 --> 00:57:02,050
Please help me to fulfill my duty as a Crown Princess, your highness.

430
00:57:10,060 --> 00:57:16,120
<i>KoreanDramaX.com </i>

431
00:57:25,060 --> 00:57:27,980
Wait a minute, your highness. Do not drink it!

432
00:58:12,800 --> 00:58:16,480
Your highness. Your highness. Your highness.

433
00:58:16,480 --> 00:58:20,220
Your highness. Your highness. Your highness.

434
00:58:20,220 --> 00:58:24,100
Your highness. Your highness.

435
00:58:28,060 --> 00:58:35,930
♬ <i>I miss you and miss you so much I could never forget you.</i> ♬

436
00:58:35,930 --> 00:58:42,820
♬ <i>My tears are calling for you.</i> ♬

437
00:58:42,820 --> 00:58:48,490
♬ <i>I love you and love you so much I can't let you go.</i> ♬

