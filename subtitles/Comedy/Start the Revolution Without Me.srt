﻿1
00:00:10,599 --> 00:00:11,658
Chairwoman Gook!

2
00:00:11,898 --> 00:00:13,259
(Final Episode)

3
00:00:13,259 --> 00:00:14,828
I think you should watch the news.

4
00:00:16,799 --> 00:00:17,868
Prosecutor Kim...

5
00:00:17,868 --> 00:00:20,909
released a recording file of an ex-NIS agent...

6
00:00:20,909 --> 00:00:24,738
claiming that former president Lee ordered him to commit murder.

7
00:00:33,949 --> 00:00:36,549
What do you think you're doing right now?

8
00:00:37,159 --> 00:00:40,428
How can you release false information on the news?

9
00:00:41,189 --> 00:00:44,129
You should have a solution to this situation!

10
00:00:44,558 --> 00:00:45,629
Shut it!

11
00:00:45,928 --> 00:00:48,729
Call my attorneys, and tell them to come see me right now!

12
00:00:51,839 --> 00:00:52,998
(Defendant Subpoena)

13
00:00:57,638 --> 00:00:58,778
Who...

14
00:00:59,439 --> 00:01:02,348
in the world is behind this, and what is that person trying to do?

15
00:01:12,559 --> 00:01:13,659
(Prosecutor Kim Yoon Su claims that...)

16
00:01:13,659 --> 00:01:14,728
(former president, Lee Kwang Ho, had instigated murder.)

17
00:01:15,859 --> 00:01:17,928
You should release all the evidence regarding Lee Kwang Ho's crimes...

18
00:01:18,529 --> 00:01:19,859
to the press.

19
00:01:20,059 --> 00:01:21,728
How do you plan on doing that?

20
00:01:21,769 --> 00:01:22,898
I'm going to use...

21
00:01:25,068 --> 00:01:26,698
Lee Kwang Ho's live press conference.

22
00:01:27,338 --> 00:01:28,338
(Prosecutor Kim Yoon Su claims that...)

23
00:01:28,338 --> 00:01:29,409
(former president, Lee Kwang Ho, had instigated murder.)

24
00:01:36,609 --> 00:01:39,919
Prosecutor Kim finally made a big move.

25
00:01:40,519 --> 00:01:43,549
Now, Lee Kwang Ho just has to hold a press conference. Then...

26
00:01:49,288 --> 00:01:50,329
Anyway,

27
00:01:51,098 --> 00:01:52,098
I'll go now.

28
00:01:53,698 --> 00:01:57,269
Get better quickly and come visit me next time.

29
00:02:14,689 --> 00:02:15,719
(Seoul Metropolitan Police Agency)

30
00:02:18,018 --> 00:02:20,559
Gosh, you really must be one of a kind.

31
00:02:20,559 --> 00:02:22,959
Lieutenant Jin came all the way here to welcome you.

32
00:02:22,959 --> 00:02:24,559
I got stabbed...

33
00:02:24,828 --> 00:02:27,198
and received poisoned water from him, so this is the least I can do.

34
00:02:27,469 --> 00:02:30,298
On top of that, you also dared to threaten Inspector Park's family...

35
00:02:30,399 --> 00:02:32,268
for such a long time.

36
00:02:36,268 --> 00:02:37,638
Tell me the truth.

37
00:02:38,239 --> 00:02:39,739
Who told you to keep...

38
00:02:40,649 --> 00:02:42,779
Han Cha Kyung and Han Kang as hostages?

39
00:02:44,848 --> 00:02:45,879
Money.

40
00:02:49,848 --> 00:02:51,119
What are you staring at?

41
00:02:52,219 --> 00:02:53,559
There are so many people...

42
00:02:54,089 --> 00:02:56,288
who think of money more importantly than people's lives...

43
00:02:57,459 --> 00:02:58,758
in this country.

44
00:03:00,899 --> 00:03:02,568
Yes, you're right.

45
00:03:03,129 --> 00:03:06,768
Unfortunately, you were caught by us.

46
00:03:07,108 --> 00:03:09,768
We already know you committed numerous crimes.

47
00:03:09,768 --> 00:03:11,709
It's one or the other for you.

48
00:03:12,309 --> 00:03:14,608
Either you rot in prison for the rest of your life,

49
00:03:15,409 --> 00:03:17,279
or the one who hired you...

50
00:03:17,279 --> 00:03:19,548
will get scared and do what they did to Baek Kyung.

51
00:03:21,788 --> 00:03:22,788
I'll see you around.

52
00:03:23,689 --> 00:03:25,189
We'll get...

53
00:03:25,189 --> 00:03:27,288
to the bottom of your crimes!

54
00:03:27,929 --> 00:03:29,228
You'll be punished.

55
00:03:35,029 --> 00:03:36,029
Let's go.

56
00:03:36,228 --> 00:03:37,369
Be quiet.

57
00:03:38,068 --> 00:03:40,298
You have got to be kidding me.

58
00:03:46,978 --> 00:03:48,048
Yes?

59
00:03:59,689 --> 00:04:01,589
I'm glad it suits you.

60
00:04:02,328 --> 00:04:03,899
What wouldn't it suit me?

61
00:04:03,999 --> 00:04:05,429
I look good in anything.

62
00:04:06,358 --> 00:04:08,068
Why did you suddenly ask me out for dinner?

63
00:04:08,228 --> 00:04:12,339
No reason. We're partners, but I never took you out.

64
00:04:14,239 --> 00:04:15,638
What should I eat?

65
00:04:16,838 --> 00:04:19,979
Hey, I heard Lee Kwang Ho will have a press conference.

66
00:04:20,109 --> 00:04:22,479
How is it going exactly the way we wanted?

67
00:04:23,249 --> 00:04:25,078
It's not by chance.

68
00:04:25,179 --> 00:04:26,948
Lee Kwang Ho is only doing what he always does.

69
00:04:27,489 --> 00:04:30,119
That's why we planned it around the press conference.

70
00:04:32,859 --> 00:04:33,859
Jin Young.

71
00:04:34,828 --> 00:04:36,789
Let's not talk about Lee Kwang Ho today.

72
00:04:37,729 --> 00:04:39,028
Let's talk about something else.

73
00:04:41,468 --> 00:04:42,499
Let's go.

74
00:04:47,169 --> 00:04:48,939
Is it true Lee Kwang Ho instigated the murder?

75
00:04:48,939 --> 00:04:50,309
I guess so.

76
00:04:50,408 --> 00:04:52,008
Forward the information we received right away...

77
00:04:52,008 --> 00:04:53,408
to the media team, okay?

78
00:04:58,018 --> 00:04:59,419
(Lee Kwang Ho Emergency Press Conference)

79
00:05:03,549 --> 00:05:04,619
Are you here?

80
00:05:06,859 --> 00:05:09,629
Where is Officer Oh? Is he not here?

81
00:05:09,729 --> 00:05:10,999
Didn't he come with you?

82
00:05:13,729 --> 00:05:14,768
What about the evidence?

83
00:05:16,869 --> 00:05:19,599
We will now start the press conference.

84
00:05:19,939 --> 00:05:23,439
(Lee Kwang Ho Emergency Press Conference)

85
00:05:23,439 --> 00:05:25,838
(Announcement regarding the recent controversy)

86
00:05:36,249 --> 00:05:37,419
Hello,

87
00:05:38,419 --> 00:05:39,718
everyone.

88
00:05:40,989 --> 00:05:44,499
I am a former president,

89
00:05:45,398 --> 00:05:46,999
but also a citizen just like you.

90
00:06:02,679 --> 00:06:03,778
Please, excuse me.

91
00:06:06,119 --> 00:06:07,549
I can confidently say...

92
00:06:08,718 --> 00:06:10,018
that I devoted my whole life...

93
00:06:10,758 --> 00:06:13,888
for the development and glory of this country.

94
00:06:14,789 --> 00:06:19,229
However, the police and prosecution have been...

95
00:06:19,528 --> 00:06:23,429
investigating while crossing the line...

96
00:06:23,698 --> 00:06:25,068
You fool.

97
00:06:33,008 --> 00:06:35,249
It has been used as a tool for revenge politics.

98
00:06:35,379 --> 00:06:37,379
It's a pity.

99
00:06:37,919 --> 00:06:40,448
You must get rid of someone if he's a problem.

100
00:06:41,148 --> 00:06:42,489
What is this?

101
00:06:42,489 --> 00:06:43,549
What?

102
00:06:44,489 --> 00:06:45,518
That's right.

103
00:06:46,458 --> 00:06:48,328
That's why you killed them.

104
00:06:49,258 --> 00:06:50,398
Choi Yeon Mi,

105
00:06:51,059 --> 00:06:52,359
Jin Jung Gil,

106
00:06:53,729 --> 00:06:54,869
Hong Mi Ok...

107
00:06:55,299 --> 00:06:56,429
I did.

108
00:06:57,169 --> 00:06:59,299
And Oh Il Seung.

109
00:06:59,599 --> 00:07:01,039
Geum Byul too.

110
00:07:01,439 --> 00:07:02,939
I told you I killed them.

111
00:07:03,539 --> 00:07:07,179
If I hadn't, I wouldn't be where I am.

112
00:07:07,508 --> 00:07:09,578
Is what we're hearing true?

113
00:07:09,578 --> 00:07:11,249
Is that your voice?

114
00:07:12,518 --> 00:07:14,318
They're all lies. It's a fabrication.

115
00:07:14,318 --> 00:07:15,818
- Sir! - Please answer us!

116
00:07:15,818 --> 00:07:17,859
- Was that your voice? - Over here.

117
00:07:20,258 --> 00:07:25,559
(Lee Kwang Ho Emergency Press Conference)

118
00:07:36,869 --> 00:07:39,939
I'm Officer Oh Il Seung of the Hidden Crime Investigation Team...

119
00:07:40,179 --> 00:07:41,508
at the Seoul Metropolitan Police.

120
00:07:43,349 --> 00:07:46,349
I'm the one who had the conversation with former president Lee Kwang Ho.

121
00:07:46,948 --> 00:07:48,349
It has no legal force.

122
00:07:49,518 --> 00:07:51,359
He abducted me!

123
00:07:51,658 --> 00:07:52,689
Really?

124
00:07:52,689 --> 00:07:53,828
It's the truth.

125
00:07:54,458 --> 00:07:56,388
I recorded it after abducting him.

126
00:07:59,528 --> 00:08:03,838
I'm not here to fight for the legitimacy of this recording.

127
00:08:03,999 --> 00:08:06,939
I'm here to tell you that former president Lee Kwang Ho...

128
00:08:07,468 --> 00:08:09,169
abducted and instigated the murder...

129
00:08:09,939 --> 00:08:11,838
of an officer.

130
00:08:11,908 --> 00:08:13,338
And who would that be?

131
00:08:13,849 --> 00:08:15,049
His name...

132
00:08:15,749 --> 00:08:16,778
was Oh Il Seung.

133
00:08:16,778 --> 00:08:18,249
- What? - Oh Il Seung?

134
00:08:20,588 --> 00:08:21,888
I'm a fake.

135
00:08:29,828 --> 00:08:31,059
Three months ago,

136
00:08:31,599 --> 00:08:33,499
former president Lee and the NIS...

137
00:08:33,599 --> 00:08:35,229
decided to murder the witness...

138
00:08:35,869 --> 00:08:37,469
of a 100-million-dollar bribe, Oh Il Seung.

139
00:08:38,138 --> 00:08:39,798
In order to cover up the murder,

140
00:08:40,268 --> 00:08:42,638
they gave me his identity.

141
00:08:42,739 --> 00:08:44,408
Then, who are you?

142
00:08:48,949 --> 00:08:49,949
I'm...

143
00:08:50,749 --> 00:08:53,819
the suspect of the 2007 Odong Blowfish House Murder.

144
00:08:55,489 --> 00:08:56,888
I'm a death-row convict named Kim Jong Sam.

145
00:08:57,319 --> 00:08:58,489
What did he say?

146
00:08:58,489 --> 00:08:59,719
Is he crazy?

147
00:08:59,888 --> 00:09:00,928
Really?

148
00:09:01,258 --> 00:09:02,489
Who is Kim Jong Sam?

149
00:09:02,489 --> 00:09:04,428
He's Kim Jong Sam?

150
00:09:04,428 --> 00:09:05,599
Hold on.

151
00:09:05,829 --> 00:09:08,599
Aside from Oh Il Seung,

152
00:09:09,199 --> 00:09:10,638
former president Lee Kwang Ho is behind the murders...

153
00:09:11,038 --> 00:09:12,069
of Choi Yeon Mi,

154
00:09:12,768 --> 00:09:13,839
Jin Jung Gil,

155
00:09:14,138 --> 00:09:15,209
Hong Mi Ok,

156
00:09:15,768 --> 00:09:17,239
and Geum Byul.

157
00:09:17,438 --> 00:09:21,479
I know of all the information regarding their murders.

158
00:09:23,178 --> 00:09:24,319
I am a witness.

159
00:09:30,918 --> 00:09:33,319
I'm Prosecutor Kim Yoon Su of Seoul Central Prosecutors' Office.

160
00:09:33,658 --> 00:09:35,629
The testimony you just heard...

161
00:09:35,758 --> 00:09:37,798
is based on truth.

162
00:09:38,028 --> 00:09:41,428
We have evidence and testimony to support the charges.

163
00:09:44,339 --> 00:09:47,908
Lee Kwang Ho. For murdering and instigating the murder...

164
00:09:47,908 --> 00:09:51,408
of Oh Il Seung and four others,

165
00:09:52,938 --> 00:09:54,148
you're under arrest.

166
00:09:55,079 --> 00:09:56,548
He's getting arrested?

167
00:09:56,548 --> 00:09:57,719
Gosh.

168
00:10:00,918 --> 00:10:02,349
You should handcuff him yourself.

169
00:10:03,619 --> 00:10:05,518
The late Officer Oh...

170
00:10:06,119 --> 00:10:07,729
would want this too.

171
00:10:22,469 --> 00:10:23,538
I've been framed.

172
00:10:24,138 --> 00:10:26,438
This is a conspiracy! Conspiracy!

173
00:10:26,609 --> 00:10:27,778
Conspiracy, I tell you!

174
00:10:30,109 --> 00:10:31,619
You have the right to remain silent...

175
00:10:31,619 --> 00:10:33,479
and the right to an attorney.

176
00:10:33,989 --> 00:10:35,018
Let's go.

177
00:10:44,129 --> 00:10:45,499
I'll do it.

178
00:11:01,079 --> 00:11:02,178
I'm sorry.

179
00:11:03,278 --> 00:11:04,879
This was the only way.

180
00:11:05,249 --> 00:11:06,719
I figured as much.

181
00:11:08,648 --> 00:11:10,558
At least you're Kim Jong Sam now.

182
00:11:14,758 --> 00:11:18,058
It's nice to see you again, Kim Jong Sam.

183
00:11:22,398 --> 00:11:23,768
It's shocking that a former president...

184
00:11:23,768 --> 00:11:26,638
is charged for instigating murder.

185
00:11:26,638 --> 00:11:27,869
Please say something.

186
00:11:30,908 --> 00:11:33,808
That's what it means to be a president of Korea.

187
00:11:35,048 --> 00:11:37,418
Out of all the previous presidents,

188
00:11:38,319 --> 00:11:41,018
who hasn't killed anyone?

189
00:11:41,849 --> 00:11:44,719
They torture people to their deaths or shoot them.

190
00:11:44,989 --> 00:11:46,688
Some get framed before being murdered.

191
00:11:47,089 --> 00:11:48,158
However,

192
00:11:49,928 --> 00:11:51,599
I'm innocent.

193
00:11:52,028 --> 00:11:53,298
Lee Kwang Ho!

194
00:11:53,599 --> 00:11:55,398
- It's a gun! - Run!

195
00:11:55,668 --> 00:11:57,569
- Watch out! - My goodness!

196
00:12:03,609 --> 00:12:04,839
You're innocent?

197
00:12:05,579 --> 00:12:08,508
What a joke. You're a murderer!

198
00:12:14,749 --> 00:12:15,819
Manager Lee!

199
00:12:17,188 --> 00:12:18,859
Watch out.

200
00:12:19,359 --> 00:12:20,359
What is going on?

201
00:12:40,079 --> 00:12:41,148
Die!

202
00:12:41,709 --> 00:12:44,349
You deserve to die, Lee Kwang Ho!

203
00:12:45,819 --> 00:12:47,119
Lee Kwang Ho!

204
00:12:47,819 --> 00:12:49,219
Lee Kwang Ho!

205
00:12:51,658 --> 00:12:54,258
In 1992, he ordered the murder and the disposal of the corpse...

206
00:12:54,258 --> 00:12:56,258
of the adoptee, Lee Young Min.

207
00:12:56,959 --> 00:13:00,768
In 1997, he instigated the murder of Choi Yeon Mi.

208
00:13:00,999 --> 00:13:02,199
In 2007,

209
00:13:02,199 --> 00:13:05,038
he attempted to murder Hong Mi Ok, and hid the evidence.

210
00:13:05,668 --> 00:13:08,469
Accepted illegal campaign fund from Jin Jung Gil,

211
00:13:08,668 --> 00:13:10,239
then blackmailed and instigated his murder.

212
00:13:10,239 --> 00:13:11,339
- Hurry. - Yes, sir.

213
00:13:11,339 --> 00:13:15,079
In 2017, he ordered the kidnapping of Han Kang,

214
00:13:15,808 --> 00:13:18,819
and instigated the murders of Cha Gwang Tae, Jang Pil Sung,

215
00:13:19,249 --> 00:13:24,058
Oh Il Seung, Kim Jong Sam, and Geum Byul.

216
00:13:24,359 --> 00:13:26,658
In addition, embezzlement of NIS' special project funds,

217
00:13:27,058 --> 00:13:30,829
leaked state funds and flew assets abroad.

218
00:13:31,298 --> 00:13:33,829
Close acquaintances have collected the evidence...

219
00:13:33,829 --> 00:13:36,329
to prove he was responsible for the said charges.

220
00:13:36,599 --> 00:13:40,168
All are under arrest and going under interrogation.

221
00:13:48,908 --> 00:13:51,749
(Public Prosecutors' Office)

222
00:14:10,699 --> 00:14:11,869
Thank you.

223
00:14:11,869 --> 00:14:13,499
It was nothing.

224
00:14:13,839 --> 00:14:15,569
- Take care. - Bye.

225
00:14:45,739 --> 00:14:47,109
Good work, Jong Sam.

226
00:14:49,308 --> 00:14:50,479
Also, thank you.

227
00:14:53,849 --> 00:14:54,879
Hey.

228
00:14:56,379 --> 00:14:58,278
You have to go back to prison, don't you?

229
00:15:01,849 --> 00:15:02,989
It's okay.

230
00:15:04,658 --> 00:15:06,688
I'll be released after a couple of winters.

231
00:15:09,388 --> 00:15:11,428
We promised to see the spring rain together,

232
00:15:12,558 --> 00:15:13,699
but it's snowing.

233
00:15:20,808 --> 00:15:22,908
Next time, we'll see the spring rain together.

234
00:15:27,079 --> 00:15:29,508
By then, everyone will know...

235
00:15:31,319 --> 00:15:32,479
that you...

236
00:15:39,688 --> 00:15:40,758
are innocent.

237
00:15:48,069 --> 00:15:52,469
According to Section 1, Article 435 of the Code of Criminal Procedure,

238
00:15:52,469 --> 00:15:56,839
we'll start the retrial of the Odong Blowfish House Murder case.

239
00:16:03,349 --> 00:16:04,678
What's wrong? Do you feel strange?

240
00:16:05,418 --> 00:16:07,518
Does he look too different from when you last saw him?

241
00:16:08,589 --> 00:16:09,849
Is he sick?

242
00:16:10,888 --> 00:16:11,888
Yes.

243
00:16:12,058 --> 00:16:13,758
Is he sleeping because he's sick?

244
00:16:14,288 --> 00:16:15,528
When will he get better?

245
00:16:16,058 --> 00:16:18,558
I'm not sure. We don't know.

246
00:16:27,268 --> 00:16:29,408
Are you upset because of him?

247
00:16:31,479 --> 00:16:33,008
Because your friend...

248
00:16:34,548 --> 00:16:36,148
won't wake up?

249
00:16:36,908 --> 00:16:38,249
Of course, I'm upset.

250
00:16:41,749 --> 00:16:44,459
I want to play with him, but we can't.

251
00:16:50,158 --> 00:16:51,229
Then,

252
00:16:53,158 --> 00:16:55,028
why are we here?

253
00:16:56,329 --> 00:16:58,768
If he's sleeping,

254
00:16:59,898 --> 00:17:01,768
you can't play with him.

255
00:17:03,538 --> 00:17:04,638
Kang.

256
00:17:06,809 --> 00:17:09,379
There is something you should know.

257
00:17:12,119 --> 00:17:13,248
Hey,

258
00:17:14,289 --> 00:17:16,049
do you know your dad's name?

259
00:17:20,488 --> 00:17:22,029
I know you know his name.

260
00:17:24,359 --> 00:17:25,599
Kang?

261
00:17:27,369 --> 00:17:30,369
Kang Chul Gi.

262
00:17:32,198 --> 00:17:34,708
Yes. Kang Chul Gi.

263
00:17:38,579 --> 00:17:39,609
Say hello.

264
00:17:44,119 --> 00:17:46,379
Kang Chul Gi. He's your dad.

265
00:18:28,988 --> 00:18:31,928
I think Kang was startled.

266
00:18:32,398 --> 00:18:34,928
Of course. It's all very sudden to him.

267
00:18:36,599 --> 00:18:37,998
He'll be fine.

268
00:18:38,668 --> 00:18:42,609
He'll be really happy later on.

269
00:18:43,779 --> 00:18:47,708
He was waiting so long for Chul Gi. It's his dad.

270
00:18:51,119 --> 00:18:52,718
Su Chil, I...

271
00:18:52,718 --> 00:18:54,918
No, no. No.

272
00:18:55,488 --> 00:18:56,819
I didn't say anything.

273
00:18:56,819 --> 00:18:59,289
I'm fine. I mean it.

274
00:19:00,829 --> 00:19:01,888
I'm really fine.

275
00:19:04,398 --> 00:19:08,069
I'm sure it's the same with you.

276
00:19:09,029 --> 00:19:10,069
Well...

277
00:19:10,698 --> 00:19:12,698
How can we not visit when Chul Gi is sick?

278
00:19:13,339 --> 00:19:15,438
Right? I can understand.

279
00:19:15,869 --> 00:19:16,938
I'm fine.

280
00:19:17,309 --> 00:19:18,678
Well, I'm not.

281
00:19:20,509 --> 00:19:23,349
I'm not okay with you being fine.

282
00:19:24,879 --> 00:19:26,218
What kind of father...

283
00:19:26,218 --> 00:19:28,748
is okay with their child getting another father?

284
00:19:29,619 --> 00:19:31,918
It's not normal to be okay.

285
00:19:31,918 --> 00:19:33,488
Why do you keep on saying you're fine?

286
00:19:33,488 --> 00:19:34,559
It makes me sad.

287
00:19:35,688 --> 00:19:36,789
Are you sad?

288
00:19:40,299 --> 00:19:41,398
I'm sad too.

289
00:19:45,968 --> 00:19:50,539
I'm not sad because Kang knows who his father is.

290
00:19:52,279 --> 00:19:54,349
It's just that everything that's happening...

291
00:19:54,849 --> 00:19:56,009
is reasonable.

292
00:19:56,009 --> 00:19:57,549
That makes me sad.

293
00:20:01,418 --> 00:20:03,119
It's natural for Kang...

294
00:20:03,519 --> 00:20:05,759
to call Chul Gi his dad.

295
00:20:07,688 --> 00:20:11,198
It's natural for you to worry...

296
00:20:11,529 --> 00:20:12,698
about Chul Gi too.

297
00:20:13,529 --> 00:20:16,569
They're all very reasonable, and that makes me sad.

298
00:20:19,998 --> 00:20:21,009
Also,

299
00:20:21,769 --> 00:20:22,869
it's...

300
00:20:24,938 --> 00:20:26,379
all very natural, but...

301
00:20:32,279 --> 00:20:34,789
It makes me sad that I'm upset about it.

302
00:20:36,448 --> 00:20:38,418
Am I getting in the way?

303
00:20:39,718 --> 00:20:41,759
Would everything be better if I wasn't around?

304
00:20:41,759 --> 00:20:43,188
It all makes me sad.

305
00:20:55,138 --> 00:20:56,769
I would die without you.

306
00:20:59,039 --> 00:21:01,009
How can I live without you?

307
00:21:02,908 --> 00:21:04,519
Same with Kang.

308
00:21:07,148 --> 00:21:08,488
Didn't you know that?

309
00:21:35,279 --> 00:21:37,178
Are you really Kang Chul Gi?

310
00:21:49,658 --> 00:21:50,759
Are you awake?

311
00:21:51,559 --> 00:21:53,928
I'm Kang. Han Kang.

312
00:21:55,228 --> 00:21:56,369
Do you know who I am?

313
00:21:57,299 --> 00:21:58,698
I know you too.

314
00:22:00,609 --> 00:22:03,309
I know you're my dad. Kang Chul Gi.

315
00:22:19,759 --> 00:22:22,529
(Three years later)

316
00:22:22,529 --> 00:22:23,589
Are you doing well?

317
00:22:28,099 --> 00:22:29,698
Who sent you?

318
00:22:31,069 --> 00:22:32,168
General Manager Kwak.

319
00:22:37,779 --> 00:22:39,178
You look the same, Assistant Manager Ki.

320
00:22:43,109 --> 00:22:44,178
What are you doing these days?

321
00:22:44,978 --> 00:22:47,289
I'm looking for a job.

322
00:22:47,789 --> 00:22:48,819
Good.

323
00:22:51,559 --> 00:22:53,359
You should go visit Chairwoman Gook.

324
00:22:54,458 --> 00:22:57,259
She doesn't have anyone to visit her.

325
00:23:04,269 --> 00:23:05,738
You can go if you have nothing else to say.

326
00:23:07,609 --> 00:23:09,309
When I was with the NIS,

327
00:23:10,908 --> 00:23:12,238
I didn't know...

328
00:23:14,408 --> 00:23:16,509
what we were doing was so terrible.

329
00:23:18,549 --> 00:23:20,178
Because everyone was doing it,

330
00:23:21,289 --> 00:23:22,988
so I just thought...

331
00:23:24,718 --> 00:23:26,289
I had to do the same.

332
00:23:28,658 --> 00:23:30,928
But we shouldn't have been doing the things we did.

333
00:23:32,228 --> 00:23:35,269
Chairwoman Gook. There's probably...

334
00:23:36,898 --> 00:23:38,839
a lot of people like me.

335
00:23:40,609 --> 00:23:43,438
People who do as they are told.

336
00:23:46,079 --> 00:23:47,478
By following your boss' orders,

337
00:23:48,309 --> 00:23:50,478
you can be an ordinary public servant,

338
00:23:51,918 --> 00:23:54,218
or you can turn into a murderer.

339
00:23:55,049 --> 00:23:56,188
Go.

340
00:23:57,718 --> 00:23:59,059
Never come back.

341
00:24:03,059 --> 00:24:04,099
Okay.

342
00:24:05,898 --> 00:24:06,928
Well...

343
00:24:08,369 --> 00:24:10,129
Take care, Chairwoman Gook.

344
00:24:37,129 --> 00:24:38,259
Inspector Cho.

345
00:24:38,259 --> 00:24:40,529
- Hello. - Hello, everyone.

346
00:24:40,529 --> 00:24:41,698
Hi.

347
00:24:42,629 --> 00:24:45,638
Inspector Cho, you didn't have to bring this.

348
00:24:45,638 --> 00:24:46,738
How are you doing?

349
00:24:46,839 --> 00:24:49,708
I'm doing well. I'm a bit bored...

350
00:24:49,708 --> 00:24:50,978
after retirement though.

351
00:24:51,339 --> 00:24:52,339
Anyway,

352
00:24:52,339 --> 00:24:53,438
I heard your son graduated.

353
00:24:53,478 --> 00:24:55,009
Yes. He got first prize...

354
00:24:55,349 --> 00:24:56,849
- in origami. - Really?

355
00:24:56,849 --> 00:24:58,049
- In origami? - Yes.

356
00:24:58,319 --> 00:24:59,349
Have some.

357
00:24:59,349 --> 00:25:00,519
- Okay. - Here you go.

358
00:25:01,418 --> 00:25:02,948
- Yes? - It's good.

359
00:25:03,549 --> 00:25:04,988
- Take more. - It's delicious.

360
00:25:05,259 --> 00:25:06,759
- What? - It's good.

361
00:25:06,859 --> 00:25:08,658
Okay.

362
00:25:08,658 --> 00:25:10,428
- Sir. - Yes?

363
00:25:10,428 --> 00:25:11,759
- We have to move. - Why?

364
00:25:11,759 --> 00:25:13,329
It's Gyeong Soo's gang.

365
00:25:13,329 --> 00:25:16,369
Why do they have to make trouble now?

366
00:25:16,629 --> 00:25:18,668
Inspector Cho. Nothing has changed here.

367
00:25:18,668 --> 00:25:19,869
We're always like this.

368
00:25:19,938 --> 00:25:21,638
- You should go. - Okay.

369
00:25:22,039 --> 00:25:24,279
Gosh. Lieutenant Jin.

370
00:25:24,279 --> 00:25:25,279
- Yes? - Why don't you...

371
00:25:25,279 --> 00:25:27,009
go to Officer Oh?

372
00:25:27,009 --> 00:25:28,049
- Okay. - Wait up.

373
00:25:28,049 --> 00:25:29,478
Tell him I said hi.

374
00:25:29,478 --> 00:25:30,549
- Okay. - Bye.

375
00:25:30,549 --> 00:25:31,748
Say hi to him for me.

376
00:25:31,748 --> 00:25:33,478
- Okay. - For me too.

377
00:25:33,478 --> 00:25:35,789
Take care. Get those criminals.

378
00:25:35,789 --> 00:25:39,559
The blue waters of the green valley! Do not boast of your speed!

379
00:25:40,019 --> 00:25:41,759
Good times.

380
00:25:41,859 --> 00:25:44,559
How is Detective Kang doing?

381
00:25:46,599 --> 00:25:51,039
(Hwang Elementary School)

382
00:25:51,599 --> 00:25:52,698
Dad!

383
00:25:54,208 --> 00:25:56,638
Kang. Let's go home now.

384
00:25:57,978 --> 00:26:01,208
Do you really have to go camping with me?

385
00:26:01,579 --> 00:26:03,448
I hate being cold.

386
00:26:05,948 --> 00:26:08,918
Hey. Are you challenging me?

387
00:26:09,119 --> 00:26:11,359
Stop right there.

388
00:26:11,658 --> 00:26:19,059
(Foster moral sense)

389
00:26:33,948 --> 00:26:35,448
4297. Hey.

390
00:26:35,908 --> 00:26:38,819
What are you doing? It's cold. Let's go inside.

391
00:26:39,648 --> 00:26:41,089
You should get ready.

392
00:26:42,488 --> 00:26:43,549
I'm coming.

393
00:26:44,759 --> 00:26:45,789
I'm coming now.

394
00:26:48,188 --> 00:26:49,329
Let's go.

395
00:27:02,609 --> 00:27:03,738
Nice shot.

396
00:27:07,648 --> 00:27:09,349
Nice two shots.

397
00:27:09,349 --> 00:27:10,648
Stop it.

398
00:27:11,619 --> 00:27:13,079
"Stop it."

399
00:27:15,688 --> 00:27:16,789
Gosh.

400
00:27:17,388 --> 00:27:20,289
Stop kicking up a fuss.

401
00:27:21,359 --> 00:27:23,589
I can't finish my business because of you.

402
00:27:24,158 --> 00:27:26,599
Hey, Pig. I told you to stay put.

403
00:27:27,468 --> 00:27:30,869
Hey, Rapist. I told you to be quiet.

404
00:27:30,898 --> 00:27:33,869
Rapist? Are you crazy?

405
00:27:34,569 --> 00:27:36,238
Don't you know who I am?

406
00:27:36,408 --> 00:27:39,208
I'm Song Gil Choon. I'm the scariest man...

407
00:27:39,208 --> 00:27:41,678
in 21st century Korea.

408
00:27:41,809 --> 00:27:44,619
There's no way it's you.

409
00:27:47,888 --> 00:27:49,019
No way.

410
00:27:49,019 --> 00:27:51,188
- Don't mess with me. - "Don't mess with me."

411
00:27:53,289 --> 00:27:54,388
You punk.

412
00:27:56,129 --> 00:27:57,129
Bring it here.

413
00:27:57,329 --> 00:27:58,898
- "Bring it here." - Come here.

414
00:27:58,898 --> 00:28:00,359
- "Come here." - Bring it here.

415
00:28:00,569 --> 00:28:01,569
"Bring it here."

416
00:28:02,129 --> 00:28:03,369
Pig is walking on two feet.

417
00:28:03,698 --> 00:28:04,869
- Hey. - Why you...

418
00:28:04,869 --> 00:28:06,269
- Shouldn't you be crawling? - Hey.

419
00:28:06,269 --> 00:28:07,468
Bring it here now.

420
00:28:07,468 --> 00:28:08,908
"Bring it here now."

421
00:28:08,908 --> 00:28:11,509
Come here, punk.

422
00:28:11,509 --> 00:28:12,908
- Hey. - You punk.

423
00:28:13,408 --> 00:28:14,779
I'll count to three.

424
00:28:14,779 --> 00:28:16,579
1. 1 and a half.

425
00:28:19,248 --> 00:28:20,718
Two.

426
00:28:20,718 --> 00:28:22,349
What's with all the noise?

427
00:28:23,248 --> 00:28:25,789
Kim Jong Sam, are you ready to be released?

428
00:28:28,188 --> 00:28:29,228
Yes, sir.

429
00:28:33,928 --> 00:28:35,029
Kim Jong Sam.

430
00:28:37,438 --> 00:28:38,468
Come back again.

431
00:28:41,109 --> 00:28:43,738
He shouldn't come back. He should become a new man,

432
00:28:43,738 --> 00:28:46,208
and do something for the betterment of Korea.

433
00:28:49,448 --> 00:28:51,279
Speak for yourself.

434
00:28:51,718 --> 00:28:53,918
I was just wishing you well.

435
00:29:00,759 --> 00:29:02,688
"Speak for yourself."

436
00:29:04,559 --> 00:29:06,059
What did you say?

437
00:29:06,329 --> 00:29:07,968
Stop.

438
00:29:07,968 --> 00:29:10,099
Stop it.

439
00:29:10,099 --> 00:29:12,099
(Musan Prison)

440
00:29:15,069 --> 00:29:18,339
(Restricted Area)

441
00:29:18,738 --> 00:29:23,918
(Establish law and order)

442
00:29:51,809 --> 00:29:53,109
Have you been waiting long?

443
00:29:53,339 --> 00:29:54,379
Yes.

444
00:29:55,049 --> 00:29:56,379
I almost froze to death.

445
00:29:56,648 --> 00:29:59,978
Why did you come all the way here. It's a hassle.

446
00:30:00,589 --> 00:30:01,688
Don't say that.

447
00:30:09,658 --> 00:30:10,658
Let's go.

448
00:30:12,799 --> 00:30:14,829
- Go where? - Anywhere.

449
00:30:16,228 --> 00:30:17,498
Let's just go.

450
00:30:27,039 --> 00:30:28,309
It's cold.

451
00:30:28,708 --> 00:30:30,349
Walk faster.

452
00:31:01,879 --> 00:31:04,279
(Doubtful Victory)

