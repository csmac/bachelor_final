﻿1
00:00:19,579 --> 00:00:21,079
What happened?

2
00:00:32,729 --> 00:00:34,629
(World Entertainment)

3
00:00:47,719 --> 00:00:49,189
(Episode 31)

4
00:00:50,678 --> 00:00:51,748
You're awake.

5
00:00:52,648 --> 00:00:53,947
How are you feeling?

6
00:00:53,948 --> 00:00:54,948
What?

7
00:00:55,748 --> 00:00:56,818
Yes.

8
00:00:57,248 --> 00:00:59,317
Why am I here?

9
00:00:59,318 --> 00:01:02,257
It seems as though you
fell down the stairs.

10
00:01:02,258 --> 00:01:03,457
Fell down the stairs?

11
00:01:03,458 --> 00:01:05,027
You were unconscious in
front of a building,

12
00:01:05,028 --> 00:01:07,158
and people nearby called
an ambulance for you.

13
00:01:07,528 --> 00:01:09,868
In front of a building...

14
00:01:12,038 --> 00:01:13,867
For how long was I unconscious?

15
00:01:13,868 --> 00:01:15,808
For quite a long time.

16
00:01:16,038 --> 00:01:18,008
It seemed you were very tired.

17
00:01:18,308 --> 00:01:19,854
You talked in your sleep too.

18
00:01:19,878 --> 00:01:21,547
Maybe you dreamed about the Olympics,

19
00:01:21,548 --> 00:01:23,508
but you kept saying, "Woo Seung".

20
00:01:24,208 --> 00:01:26,518
I said "Woo Seung" in my sleep?

21
00:01:30,648 --> 00:01:33,087
Am I hurt a lot?

22
00:01:33,088 --> 00:01:35,257
We'll need to go through more checkups,

23
00:01:35,258 --> 00:01:38,598
but fortunately, you don't have
any serious external injuries.

24
00:01:48,238 --> 00:01:49,967
- What is this for?
- There was a minor bruise...

25
00:01:49,968 --> 00:01:51,208
on your head.

26
00:01:51,838 --> 00:01:54,177
Anyway, it's important to get rest,

27
00:01:54,178 --> 00:01:55,248
so do that.

28
00:01:56,008 --> 00:01:57,178
And...

29
00:01:58,878 --> 00:02:01,318
can I get your autograph here?

30
00:02:04,018 --> 00:02:06,518
- What?
- I'm a huge fan of yours.

31
00:02:07,058 --> 00:02:08,787
Why don't you perform anymore?

32
00:02:08,788 --> 00:02:10,258
Nurse Park.

33
00:02:11,098 --> 00:02:12,258
Me first.

34
00:02:13,098 --> 00:02:15,627
Well, my daughter...

35
00:02:15,628 --> 00:02:18,068
is a huge fan of yours.

36
00:02:18,598 --> 00:02:19,598
Here.

37
00:02:30,348 --> 00:02:32,178
Then was it all...

38
00:02:33,248 --> 00:02:34,688
a dream until now?

39
00:03:01,548 --> 00:03:03,548
(Roar, Moo)

40
00:03:21,768 --> 00:03:24,028
(Roar, Moo)

41
00:03:34,378 --> 00:03:35,448
Hi, Hyun Jae.

42
00:03:37,008 --> 00:03:39,048
You aren't scheduled for a treatment today.

43
00:03:40,118 --> 00:03:42,748
Are you here for the result of
a checkup you did yesterday?

44
00:03:43,288 --> 00:03:45,588
You'll have to wait a few more days though.

45
00:03:46,388 --> 00:03:47,458
Yesterday?

46
00:03:48,558 --> 00:03:50,328
I came to the hospital yesterday?

47
00:03:50,458 --> 00:03:51,458
Pardon?

48
00:03:52,128 --> 00:03:54,427
Yes. You came to the hospital...

49
00:03:54,428 --> 00:03:56,028
and had a checkup yesterday.

50
00:03:58,368 --> 00:03:59,568
It's my notebook.

51
00:04:03,208 --> 00:04:05,878
(I want to have it!)

52
00:04:07,378 --> 00:04:09,777
I told you not to touch my stuff.

53
00:04:09,778 --> 00:04:11,058
Your stuff? What are you saying?

54
00:04:11,378 --> 00:04:13,148
That's my watch.

55
00:04:14,488 --> 00:04:15,717
What? How is this yours? It's mine.

56
00:04:15,718 --> 00:04:18,018
Why is it yours? It's mine!

57
00:04:18,488 --> 00:04:20,958
Is there another Yoo Hyun Jae here?

58
00:04:23,388 --> 00:04:24,997
I'm sorry, but what date is it today?

59
00:04:24,998 --> 00:04:26,727
It's 17th today.

60
00:04:26,728 --> 00:04:29,027
Do you mean June 17?

61
00:04:29,028 --> 00:04:31,198
Sure, it's June now.

62
00:04:31,738 --> 00:04:32,798
Is it the year 1993 now?

63
00:04:33,138 --> 00:04:34,138
What?

64
00:04:34,708 --> 00:04:37,137
What are you talking about?

65
00:04:37,138 --> 00:04:38,807
It's 1994.

66
00:04:38,808 --> 00:04:40,278
1994?

67
00:04:43,248 --> 00:04:44,248
Hey.

68
00:04:45,348 --> 00:04:47,377
Is Yoo Hyun Jae dead?

69
00:04:47,378 --> 00:04:51,717
Yoo Hyun Jae died on June 17, 1994...

70
00:04:51,718 --> 00:04:54,488
when he was 24 years old.

71
00:04:59,358 --> 00:05:03,268
(The Best Hit)

72
00:05:23,848 --> 00:05:24,958
What?

73
00:05:25,158 --> 00:05:26,988
You shouldn't move around yet.

74
00:05:28,328 --> 00:05:29,428
I'm a bit busy now.

75
00:05:30,328 --> 00:05:31,358
Wait.

76
00:05:43,608 --> 00:05:45,838
Pick up. Please pick up.

77
00:05:47,478 --> 00:05:48,548
Hello?

78
00:05:48,708 --> 00:05:50,678
Gwang Jae, it's me, Hyun Jae.

79
00:05:54,788 --> 00:05:56,388
What's wrong with you?

80
00:05:56,818 --> 00:05:58,018
Why do you keep...

81
00:05:58,818 --> 00:05:59,888
I...

82
00:06:01,158 --> 00:06:03,257
already made it clear...

83
00:06:03,258 --> 00:06:05,298
that I don't want to talk to you like this.

84
00:06:05,658 --> 00:06:07,927
Hey, we made up already.

85
00:06:07,928 --> 00:06:08,928
What?

86
00:06:09,098 --> 00:06:12,698
- When did I?
- So what happened was...

87
00:06:13,198 --> 00:06:15,968
Gosh, I'll explain later. Anyway,

88
00:06:16,708 --> 00:06:18,307
I need your help.

89
00:06:18,308 --> 00:06:19,577
I can't go to where you are now,

90
00:06:19,578 --> 00:06:20,707
so you need to come here.

91
00:06:20,708 --> 00:06:25,047
Gosh, you're upsetting
me after a long time.

92
00:06:25,048 --> 00:06:26,547
Hyun Jae, you...

93
00:06:26,548 --> 00:06:29,948
I only have you whom I
can rely on right now.

94
00:06:30,188 --> 00:06:31,787
Please come to Gom Cafe.

95
00:06:31,788 --> 00:06:32,868
I know I'm being shameless,

96
00:06:33,058 --> 00:06:34,658
but please help me this one last time.

97
00:06:36,128 --> 00:06:37,957
Why would I go there seriously?

98
00:06:37,958 --> 00:06:40,158
Hey, forget it.

99
00:06:40,828 --> 00:06:43,268
Just sleep already if you have
nothing to do. I'm hanging up.

100
00:06:53,978 --> 00:06:54,978
My gosh.

101
00:06:56,078 --> 00:06:57,224
Was that Hyun Jae?

102
00:06:57,248 --> 00:06:59,947
Have you guys been keeping
in touch behind our backs?

103
00:06:59,948 --> 00:07:01,848
That's not it.

104
00:07:02,018 --> 00:07:04,588
Forget it.

105
00:07:05,388 --> 00:07:06,457
Where were we?

106
00:07:06,458 --> 00:07:08,328
What? Well...

107
00:07:09,128 --> 00:07:12,358
This came to my mind as
I was writing a song.

108
00:07:12,498 --> 00:07:14,397
It's not because I can't think of an idea,

109
00:07:14,398 --> 00:07:17,098
but I've never been interested
in dance songs from the start.

110
00:07:17,228 --> 00:07:19,698
What if we release a ballad album?

111
00:07:21,668 --> 00:07:24,038
Are you listening to me?

112
00:07:25,578 --> 00:07:26,908
Hey, Gwang Jae!

113
00:07:27,308 --> 00:07:29,248
What? Yes.

114
00:07:30,008 --> 00:07:32,017
How can you not be interested in me...

115
00:07:32,018 --> 00:07:33,677
regardless of Hyun Jae's existence?

116
00:07:33,678 --> 00:07:36,088
What are you talking about?

117
00:07:36,288 --> 00:07:38,617
You're the star singer...

118
00:07:38,618 --> 00:07:40,218
of World Entertainment now.

119
00:07:40,458 --> 00:07:42,958
You're only saying that because
I'm all you have left.

120
00:07:43,758 --> 00:07:46,427
Whatever. What I'm saying is...

121
00:07:46,428 --> 00:07:48,028
I need to head out for a second.

122
00:07:48,868 --> 00:07:50,767
We're in the middle of an
important conversation here.

123
00:07:50,768 --> 00:07:51,997
Let's talk next time.

124
00:07:51,998 --> 00:07:53,838
You said I'm the star singer.

125
00:07:54,138 --> 00:07:55,567
You're the star singer.

126
00:07:55,568 --> 00:07:57,068
But let's talk later.

127
00:08:01,108 --> 00:08:03,508
I'm pretty sure that
call was from Hyun Jae.

128
00:08:05,278 --> 00:08:08,818
Are they trying to pull something again?

129
00:08:13,518 --> 00:08:14,628
Welcome.

130
00:08:32,578 --> 00:08:34,608
You said you were going
somewhere for a while.

131
00:08:34,878 --> 00:08:37,407
I thought it was somewhere
nice and awesome.

132
00:08:37,408 --> 00:08:39,377
I guess you really went
somewhere for a while.

133
00:08:39,378 --> 00:08:40,677
Hey, it's so nice...

134
00:08:40,678 --> 00:08:42,638
to see you again when I
saw you just a while ago.

135
00:08:43,048 --> 00:08:44,558
What are you saying?

136
00:08:45,058 --> 00:08:46,557
When did we see each other?

137
00:08:46,558 --> 00:08:47,727
There's something.

138
00:08:47,728 --> 00:08:49,557
Goodness, and...

139
00:08:49,558 --> 00:08:52,558
what's wrong with your
hairstyle and clothes?

140
00:08:53,058 --> 00:08:54,998
For goodness' sake.

141
00:08:57,668 --> 00:08:59,298
There's a reason for that too.

142
00:08:59,968 --> 00:09:02,037
Anyway, I'm glad to see you.

143
00:09:02,038 --> 00:09:04,677
I was going to ask you to meet
because I had to talk to you.

144
00:09:04,678 --> 00:09:07,747
Before that, I have to
find someone in a hurry.

145
00:09:07,748 --> 00:09:11,048
Who? Who are you looking for in a hurry?

146
00:09:11,678 --> 00:09:14,118
- Yoo Hyun Jae.
- What?

147
00:09:16,118 --> 00:09:19,058
- I need to find myself.
- What are you talking about?

148
00:09:19,218 --> 00:09:21,888
- Are you drunk?
- No.

149
00:09:22,028 --> 00:09:23,828
Where can I find him now?

150
00:09:25,128 --> 00:09:26,558
- I'm leaving.
- Hey, wait.

151
00:09:28,268 --> 00:09:30,397
I know this sounds weird,

152
00:09:30,398 --> 00:09:33,067
but you know a lot of people.

153
00:09:33,068 --> 00:09:34,937
I know you can find him
quickly if you help me.

154
00:09:34,938 --> 00:09:37,067
What on earth are you saying?

155
00:09:37,068 --> 00:09:38,738
Why are you looking for yourself?

156
00:09:38,878 --> 00:09:41,477
I found you. You're right here.
Sitting here.

157
00:09:41,478 --> 00:09:42,608
No, it's...

158
00:09:45,978 --> 00:09:51,187
Okay, so someone is apparently
pretending to be me.

159
00:09:51,188 --> 00:09:53,158
Someone who looks exactly like me.

160
00:09:54,118 --> 00:09:55,757
- What?
- So find him for me.

161
00:09:55,758 --> 00:09:59,328
Find out where Hyun Jae is. I
know he's out there somewhere.

162
00:09:59,428 --> 00:10:01,568
He's not me, but there is another Hyun Jae.

163
00:10:07,068 --> 00:10:10,008
What are they talking about?
I can't hear them.

164
00:10:25,788 --> 00:10:27,628
What happened? Did you find him?

165
00:10:29,558 --> 00:10:33,798
Like you said, I guess there
is someone like you out there.

166
00:10:33,898 --> 00:10:36,067
Really? Where is he right now?

167
00:10:36,068 --> 00:10:37,198
Well...

168
00:10:38,268 --> 00:10:41,708
It looks like he was
recently in the studio.

169
00:10:42,438 --> 00:10:44,637
He worked there today morning.

170
00:10:44,638 --> 00:10:45,838
And then?

171
00:10:45,978 --> 00:10:48,078
He went to Paju to do something.

172
00:10:48,608 --> 00:10:51,178
Paju? Why?

173
00:10:52,848 --> 00:10:55,918
He apparently had something
to do with his family.

174
00:10:56,148 --> 00:10:57,388
His family?

175
00:11:02,288 --> 00:11:03,988
- I need your car.
- What?

176
00:11:09,928 --> 00:11:14,138
I have no idea why I always
let you have your way.

177
00:11:15,668 --> 00:11:17,168
You're my only friend.

178
00:11:18,578 --> 00:11:20,738
I'm not your friend.

179
00:11:36,588 --> 00:11:39,127
Did you forget something?

180
00:11:39,128 --> 00:11:40,128
Pardon?

181
00:11:41,698 --> 00:11:45,198
Oh, when did I walk out of this room?

182
00:11:46,338 --> 00:11:48,608
- Just now.
- Really?

183
00:11:49,638 --> 00:11:52,108
Then what did I do here?

184
00:11:52,708 --> 00:11:54,577
- Pardon?
- I must've done something.

185
00:11:54,578 --> 00:11:56,577
You wanted to move your parents' grave.

186
00:11:56,578 --> 00:11:59,918
- What?
- You said you can't take care...

187
00:12:00,118 --> 00:12:02,748
- of your parents' grave anymore.
- Did I?

188
00:12:04,858 --> 00:12:07,588
Oh, right. I did.

189
00:12:08,658 --> 00:12:12,098
And did I say where I was going next?

190
00:12:12,558 --> 00:12:13,558
No.

191
00:12:20,698 --> 00:12:24,907
Excuse me, is there a lake nearby?

192
00:12:24,908 --> 00:12:26,577
There is a lake nearby.

193
00:12:26,578 --> 00:12:29,908
There's a rather large
lake named Lake Suryeon.

194
00:12:30,448 --> 00:12:31,718
Lake Suryeon?

195
00:12:33,678 --> 00:12:35,188
Lake Suryeon?

196
00:12:35,548 --> 00:12:39,857
We found the last traces of the
popular singer, Yoo Hyun Jae.

197
00:12:39,858 --> 00:12:42,427
Yesterday afternoon, a month
after his disappearance,

198
00:12:42,428 --> 00:12:45,027
a car owned by Yoo Hyun
Jae was found deserted...

199
00:12:45,028 --> 00:12:48,128
near a lake in Paju, Gyeonggi Province.

200
00:12:48,968 --> 00:12:50,668
You can't go to Lake Suryeon.

201
00:12:51,838 --> 00:12:52,868
Thank you.

202
00:13:18,728 --> 00:13:21,768
I don't think I can take
care of you two anymore.

203
00:13:23,798 --> 00:13:26,638
You wanted me to live near you two, right?

204
00:13:36,078 --> 00:13:38,418
I think I'll probably see you very soon,

205
00:13:39,948 --> 00:13:42,288
so don't be disappointed
that I'm doing this.

206
00:13:54,268 --> 00:13:56,204
This is an update on the typhoon.

207
00:13:56,228 --> 00:14:00,268
We can assume that Seoul and
Gyeonggi Province are effected.

208
00:14:00,368 --> 00:14:01,807
Yes, we can.

209
00:14:01,808 --> 00:14:06,208
Many of you must remember the twin
typhoon Kaola from last year.

210
00:14:06,538 --> 00:14:10,308
We have another twin typhoon,
even though it's much smaller.

211
00:14:50,818 --> 00:14:52,188
Who are you?

212
00:14:52,988 --> 00:14:55,528
So there really is another Hyun Jae.

213
00:14:56,028 --> 00:14:57,487
Wow, thumbs up.

214
00:14:57,488 --> 00:15:00,328
Who on earth are you?

215
00:15:00,628 --> 00:15:04,267
Don't be surprised. I'm you.

216
00:15:04,268 --> 00:15:06,638
Wait, should I say that you're me?

217
00:15:07,338 --> 00:15:08,538
What?

218
00:15:08,668 --> 00:15:10,038
I know it's hard to believe,

219
00:15:11,108 --> 00:15:12,838
but I'm Hyun Jae from 1993.

220
00:15:13,608 --> 00:15:15,748
From 1993?

221
00:15:15,848 --> 00:15:19,647
I'm from the night in 1993 which
always wanted to go back to.

222
00:15:19,648 --> 00:15:23,217
I went to the future and came back
because of this weird typhoon.

223
00:15:23,218 --> 00:15:26,157
This is actually pretty huge
if you actually go through it,

224
00:15:26,158 --> 00:15:29,328
but if you say it out loud, it
does sound quite odd, right?

225
00:15:30,528 --> 00:15:31,928
So what I mean is...

226
00:15:36,868 --> 00:15:37,938
Anyway.

227
00:15:39,998 --> 00:15:44,108
I guess you came here because
of our parents' ashes.

228
00:15:44,938 --> 00:15:47,977
In that case, this doesn't seem like...

229
00:15:47,978 --> 00:15:49,698
the worst case scenario which I thought of.

230
00:15:50,478 --> 00:15:51,848
What are you talking about?

231
00:15:51,978 --> 00:15:54,888
It says that you disappeared in this place.

232
00:15:55,048 --> 00:15:58,188
So I wanted to prevent
you from coming here.

233
00:15:58,388 --> 00:16:01,828
But I didn't think I would see you here.

234
00:16:02,958 --> 00:16:05,798
I guess you really can't
change your destiny.

235
00:16:13,968 --> 00:16:14,968
Hey.

236
00:16:19,378 --> 00:16:22,107
Are you saying that you're me?

237
00:16:22,108 --> 00:16:23,348
That's what I'm telling you.

238
00:16:23,478 --> 00:16:25,647
I had no idea what was going on,

239
00:16:25,648 --> 00:16:28,647
but I figured some stuff out from
the note you left in the bank.

240
00:16:28,648 --> 00:16:31,287
A note? What are you talking about?

241
00:16:31,288 --> 00:16:33,608
The money and note which you
left in the safe in the bank.

242
00:16:34,058 --> 00:16:36,458
You were going to give
it to Gwang Jae, right?

243
00:16:36,858 --> 00:16:39,228
Yes, I was thinking about that.

244
00:16:40,458 --> 00:16:41,668
How did you know?

245
00:16:42,768 --> 00:16:45,167
For one thing, I'm you.

246
00:16:45,168 --> 00:16:47,067
And I'm from the future.

247
00:16:47,068 --> 00:16:51,208
No wait, I'm originally from the past.

248
00:16:51,738 --> 00:16:53,478
Anyway, that's that.

249
00:16:53,808 --> 00:16:57,208
So you're saying that you're me from 1993.

250
00:16:58,348 --> 00:17:02,017
I'm from that night in 1993,
when we received the golden cup.

251
00:17:02,018 --> 00:17:04,558
That exact night which you
wanted to go back to.

252
00:17:06,758 --> 00:17:09,688
Right, that's the night...

253
00:17:11,028 --> 00:17:13,698
when I grew apart from everyone.

254
00:17:14,498 --> 00:17:17,028
I thought all of the money was mine,

255
00:17:18,068 --> 00:17:19,398
but I guess it's not.

256
00:17:23,408 --> 00:17:24,478
So,

257
00:17:27,208 --> 00:17:28,747
how do they live?

258
00:17:28,748 --> 00:17:29,947
What?

259
00:17:29,948 --> 00:17:31,748
You said you saw the future.

260
00:17:33,518 --> 00:17:34,688
What about Bo Hee?

261
00:17:35,418 --> 00:17:36,418
How's Gwang Jae?

262
00:17:36,419 --> 00:17:39,818
Oh, right. It's...

263
00:17:40,118 --> 00:17:42,858
Well, it's a long story.

264
00:17:43,458 --> 00:17:46,558
But I am a superb storyteller.

265
00:17:47,028 --> 00:17:48,868
Let's see, how should I begin?

266
00:17:52,238 --> 00:17:55,668
Gwang Jae, Bo Hee, and
Sunday were living together.

267
00:17:56,138 --> 00:17:59,008
Young Jae became a huge success.

268
00:18:04,618 --> 00:18:07,024
Who on earth is that?

269
00:18:07,048 --> 00:18:08,888
He kind of looks like Hyun Jae.

270
00:18:13,018 --> 00:18:14,528
What's with the weather?

271
00:18:14,928 --> 00:18:16,788
What if I get struck by lightning?

272
00:18:18,528 --> 00:18:22,168
Our fans are still the
same even after 20 years.

273
00:18:22,968 --> 00:18:25,538
They are still superb and awesome.

274
00:18:26,738 --> 00:18:31,178
They still remember you, and
they still love our songs.

275
00:18:32,978 --> 00:18:34,038
Really?

276
00:18:41,648 --> 00:18:43,648
(Roar, Moo)

277
00:18:44,718 --> 00:18:46,388
I guess we don't have much time.

278
00:18:47,058 --> 00:18:49,688
- What?
- Nothing.

279
00:18:50,228 --> 00:18:51,698
About Ji Hoon.

280
00:18:53,258 --> 00:18:54,738
Can you tell me a bit more about him?

281
00:18:55,368 --> 00:18:56,368
What?

282
00:18:56,598 --> 00:18:58,638
I told you.

283
00:18:58,898 --> 00:19:00,498
He's a great guy.

284
00:19:01,868 --> 00:19:04,338
He must be superb...

285
00:19:05,178 --> 00:19:06,338
since he's my son.

286
00:19:07,408 --> 00:19:08,408
Yes.

287
00:19:08,409 --> 00:19:10,878
He's superb.

288
00:19:11,878 --> 00:19:14,318
He's just too good.

289
00:19:14,918 --> 00:19:16,918
He's also very smart.

290
00:19:17,858 --> 00:19:20,188
He's pretty good at
dancing and singing too.

291
00:19:20,318 --> 00:19:22,528
Good enough to be on stage.

292
00:19:23,258 --> 00:19:25,897
You'd think he'd be conceited,

293
00:19:25,898 --> 00:19:27,404
but he isn't.

294
00:19:27,428 --> 00:19:28,468
What about school?

295
00:19:29,768 --> 00:19:30,827
Is he a good student?

296
00:19:30,828 --> 00:19:33,968
He went to the best university in Korea.

297
00:19:34,968 --> 00:19:37,438
He must be smart just like me.

298
00:19:39,008 --> 00:19:40,178
I miss them.

299
00:19:40,808 --> 00:19:42,148
Gwang Jae.

300
00:19:43,148 --> 00:19:44,418
Bo Hee.

301
00:19:47,018 --> 00:19:48,218
And Ji Hoon.

302
00:19:52,518 --> 00:19:53,688
Oh, my goodness!

303
00:19:54,358 --> 00:19:55,758
That scared me.

304
00:19:56,188 --> 00:19:57,427
What's up with the lightning?

305
00:19:57,428 --> 00:19:58,928
It's because of the typhoon.

306
00:19:59,698 --> 00:20:00,798
Typhoon?

307
00:20:02,068 --> 00:20:03,368
Is it...

308
00:20:03,828 --> 00:20:05,168
the twin typhoon?

309
00:20:05,368 --> 00:20:06,398
I don't know.

310
00:20:08,668 --> 00:20:12,277
Wait. The radio said it was.

311
00:20:12,278 --> 00:20:14,178
Yes, it's the twin typhoon.

312
00:20:15,348 --> 00:20:16,348
What about it?

313
00:20:16,448 --> 00:20:17,548
What time is it?

314
00:20:18,018 --> 00:20:19,778
I left my watch at home.

315
00:20:24,758 --> 00:20:27,088
How long will it take to
go to World Entertainment?

316
00:20:28,688 --> 00:20:30,857
- Why do you ask?
- I don't know what will happen...

317
00:20:30,858 --> 00:20:32,098
but let's try.

318
00:20:32,258 --> 00:20:33,698
What are you talking about?

319
00:20:34,368 --> 00:20:35,727
I'll explain later.

320
00:20:35,728 --> 00:20:36,928
Start the car first.

321
00:20:41,908 --> 00:20:43,337
What's wrong with this?

322
00:20:43,338 --> 00:20:44,608
I brought a car.

323
00:20:44,738 --> 00:20:46,078
Let's take that. Hurry.

324
00:21:02,188 --> 00:21:03,528
Oh, I don't know.

325
00:21:14,168 --> 00:21:15,208
(The Great Yoo Hyun Jae)

326
00:21:35,688 --> 00:21:41,428
(World Entertainment)

327
00:21:44,768 --> 00:21:46,737
What? The rain stopped.

328
00:21:46,738 --> 00:21:48,068
There isn't much time.

329
00:21:50,308 --> 00:21:51,478
We have three minutes left.

330
00:21:51,708 --> 00:21:53,108
We have to go to the rooftop now.

331
00:21:53,208 --> 00:21:56,078
- Why?
- There might be one more chance.

332
00:21:56,678 --> 00:21:57,778
Quickly.

333
00:22:01,848 --> 00:22:06,118
(World Entertainment)

334
00:22:36,848 --> 00:22:38,358
We've got nothing to lose.

335
00:22:38,818 --> 00:22:40,218
Ride this and slide down.

336
00:22:40,388 --> 00:22:41,988
- What?
- This could...

337
00:22:42,188 --> 00:22:43,957
send us to another world.

338
00:22:43,958 --> 00:22:45,028
What are you saying?

339
00:22:47,598 --> 00:22:49,428
This is how I traveled through time...

340
00:22:49,628 --> 00:22:50,698
on the rooftop.

341
00:22:52,738 --> 00:22:54,538
We don't have time. Hurry.

342
00:22:58,808 --> 00:23:00,078
In the future,

343
00:23:00,408 --> 00:23:01,577
your illness...

344
00:23:01,578 --> 00:23:03,678
can be cured.

345
00:23:14,488 --> 00:23:15,588
No.

346
00:23:16,288 --> 00:23:17,364
What do you mean no?

347
00:23:17,388 --> 00:23:19,198
I don't want that.

348
00:23:19,698 --> 00:23:20,798
What?

349
00:23:22,298 --> 00:23:24,298
This is my time.

350
00:23:25,098 --> 00:23:26,268
Think about it.

351
00:23:26,568 --> 00:23:29,468
I'm Yoo Hyun Jae who's
living in this world.

352
00:23:31,038 --> 00:23:33,338
If the future you're from really exists,

353
00:23:34,808 --> 00:23:37,148
then there's something I should do here.

354
00:23:39,278 --> 00:23:41,848
Whatever I need to finish,

355
00:23:42,948 --> 00:23:44,248
I want to do it here.

356
00:23:46,288 --> 00:23:47,727
There's a reason...

357
00:23:47,728 --> 00:23:49,328
I'm here like this.

358
00:23:56,298 --> 00:23:58,798
If you meet Ji Hoon again,

359
00:24:03,338 --> 00:24:04,578
give this to him.

360
00:24:11,148 --> 00:24:13,178
You're dying.

361
00:24:15,218 --> 00:24:16,788
I'm Yoo Hyun Jae.

362
00:24:18,018 --> 00:24:19,988
I won't go down that easily.

363
00:24:22,858 --> 00:24:24,298
You said there isn't much time left.

364
00:24:33,668 --> 00:24:36,038
Go back to the time you belong.

365
00:24:37,568 --> 00:24:38,678
Hey.

366
00:24:39,378 --> 00:24:40,538
Yoo Hyun Jae.

367
00:24:41,348 --> 00:24:42,348
And...

368
00:24:46,948 --> 00:24:48,488
thanks for coming.

369
00:24:50,418 --> 00:24:51,658
Why you...

370
00:25:24,048 --> 00:25:26,417
Ji Hoon, aren't you ready yet?

371
00:25:26,418 --> 00:25:27,927
Why do you have to go so early?

372
00:25:27,928 --> 00:25:31,257
Public TV makes you do long rehearsals,

373
00:25:31,258 --> 00:25:32,458
so it starts early.

374
00:25:33,798 --> 00:25:35,628
- Gosh.
- What's taking them so long?

375
00:25:36,198 --> 00:25:38,537
Hey. It's your first public TV appearance.

376
00:25:38,538 --> 00:25:39,997
You can't be late.

377
00:25:39,998 --> 00:25:41,808
We were practicing.

378
00:25:42,338 --> 00:25:44,307
Madam, no, mister.

379
00:25:44,308 --> 00:25:45,478
No.

380
00:25:46,638 --> 00:25:47,648
Miss. No!

381
00:25:48,678 --> 00:25:50,948
Sir, I'm sorry.

382
00:25:52,418 --> 00:25:53,547
Are you okay?

383
00:25:53,548 --> 00:25:56,217
I think you look a bit pale.

384
00:25:56,218 --> 00:25:58,118
Yes, I'm okay.

385
00:25:59,318 --> 00:26:00,518
My heart is...

386
00:26:02,958 --> 00:26:04,398
My heart is...

387
00:26:05,628 --> 00:26:08,097
Hey. Where's my heart?

388
00:26:08,098 --> 00:26:09,598
Here.

389
00:26:10,198 --> 00:26:12,037
Hey. Don't be nervous.

390
00:26:12,038 --> 00:26:13,837
Just be yourselves.

391
00:26:13,838 --> 00:26:14,838
Okay.

392
00:26:15,708 --> 00:26:18,014
I'm so jealous. They'll be on public TV.

393
00:26:18,038 --> 00:26:20,537
Sir, you're being so unfair.

394
00:26:20,538 --> 00:26:22,177
You never put us on TV.

395
00:26:22,178 --> 00:26:25,377
They're getting attention. That's why.

396
00:26:25,378 --> 00:26:27,048
Just wait a bit longer.

397
00:26:27,148 --> 00:26:28,517
What are you doing? Get in.

398
00:26:28,518 --> 00:26:29,618
Wait a minute.

399
00:26:32,088 --> 00:26:33,088
There.

400
00:26:33,358 --> 00:26:34,727
My son is so handsome.

401
00:26:34,728 --> 00:26:36,758
Okay. You're ready. Go now.

402
00:26:36,858 --> 00:26:38,327
- Bye.
- Good luck.

403
00:26:38,328 --> 00:26:39,797
- Okay.
- Ji Hoon!

404
00:26:39,798 --> 00:26:42,467
Mal Sook. He's busy. What is it?

405
00:26:42,468 --> 00:26:43,528
(World Entertainment)

406
00:26:43,968 --> 00:26:46,267
What's this? You want my autograph?

407
00:26:46,268 --> 00:26:47,298
No.

408
00:26:47,468 --> 00:26:49,737
If you see Celestial Boys,

409
00:26:49,738 --> 00:26:51,277
- get their autographs.
- What?

410
00:26:51,278 --> 00:26:52,937
You have to get...

411
00:26:52,938 --> 00:26:54,477
every one of their autographs.

412
00:26:54,478 --> 00:26:55,907
Okay?

413
00:26:55,908 --> 00:26:57,248
Okay.

414
00:26:58,148 --> 00:26:59,248
And...

415
00:27:03,848 --> 00:27:05,264
This is my favorite thing.

416
00:27:05,288 --> 00:27:07,088
It'll protect you.

417
00:27:08,058 --> 00:27:09,587
- Thanks.
- Where's mine?

418
00:27:09,588 --> 00:27:10,957
You should protect me.

419
00:27:10,958 --> 00:27:11,997
Where's mine?

420
00:27:11,998 --> 00:27:14,297
Stop it. We have to go. Get in.

421
00:27:14,298 --> 00:27:15,697
- Bye.
- Bye now.

422
00:27:15,698 --> 00:27:17,528
- Go for it.
- Good luck.

423
00:27:18,038 --> 00:27:19,668
- You can do it.
- Good luck.

424
00:27:22,668 --> 00:27:24,638
I'm hungry.

425
00:27:24,778 --> 00:27:27,477
Ma'am. There must be something
wrong with my stomach.

426
00:27:27,478 --> 00:27:29,748
Mal Sook.

427
00:27:31,218 --> 00:27:32,747
I told you to call me Mom,

428
00:27:32,748 --> 00:27:34,217
not ma'am.

429
00:27:34,218 --> 00:27:35,618
That's right.

430
00:27:36,018 --> 00:27:37,018
Mom.

431
00:27:37,148 --> 00:27:40,357
Should we go make some delicious noodles?

432
00:27:40,358 --> 00:27:41,557
Okay, Mom!

433
00:27:41,558 --> 00:27:42,588
Let's go.

434
00:27:46,558 --> 00:27:49,327
It feels weird to see them...

435
00:27:49,328 --> 00:27:51,538
debut as singers.

436
00:27:51,768 --> 00:27:53,067
Their song is pretty awesome, right?

437
00:27:53,068 --> 00:27:56,708
Yes. It's pretty high up the music chart.

438
00:27:56,938 --> 00:27:59,478
They need to become a big hit.

439
00:27:59,608 --> 00:28:01,708
That would be nice.

440
00:28:02,108 --> 00:28:04,978
I want to be a big hit too.

441
00:28:06,078 --> 00:28:07,648
Hello. Long time no see.

442
00:28:08,148 --> 00:28:09,247
Well...

443
00:28:09,248 --> 00:28:10,447
- Hello.
- Hello.

444
00:28:10,448 --> 00:28:11,557
Welcome.

445
00:28:11,558 --> 00:28:14,057
Of course they can do variety shows.

446
00:28:14,058 --> 00:28:17,057
- Why did you say welcome?
- I'm not in the office...

447
00:28:17,058 --> 00:28:18,257
- Hello.
- Welcome.

448
00:28:18,258 --> 00:28:20,657
I'll come see you after the show.

449
00:28:20,658 --> 00:28:21,668
Okay.

450
00:28:21,798 --> 00:28:25,437
I guess a lot of producers
think you're great.

451
00:28:25,438 --> 00:28:27,198
This is it.

452
00:28:29,138 --> 00:28:30,308
JB.

453
00:28:32,978 --> 00:28:35,078
(Music Top Ten, JB)

454
00:28:36,978 --> 00:28:39,178
Gosh. Mr. Lee...

455
00:28:39,378 --> 00:28:42,118
came up with a fantastic name.

456
00:28:43,288 --> 00:28:44,958
(Music Top Ten, JB)

457
00:28:52,228 --> 00:28:53,328
Yes, Director.

458
00:28:53,698 --> 00:28:55,498
Sure, hold on.

459
00:29:02,038 --> 00:29:04,068
I can do this.

460
00:29:04,808 --> 00:29:06,008
Don't be nervous.

461
00:29:07,378 --> 00:29:08,408
Old Man.

462
00:29:08,908 --> 00:29:10,647
I didn't expect I'd see you here.

463
00:29:10,648 --> 00:29:12,417
Do you have a shooting today as well?

464
00:29:12,418 --> 00:29:15,047
What are you saying? Our song
is the top one candidate.

465
00:29:15,048 --> 00:29:18,088
Really? But you debuted only recently.

466
00:29:18,218 --> 00:29:21,128
They say we're within the
top five girl groups.

467
00:29:21,258 --> 00:29:24,019
That's not important though. Winning
first place is what's important.

468
00:29:25,458 --> 00:29:27,398
By the way, what's wrong with him?

469
00:29:27,928 --> 00:29:30,568
I think he's nervous. He has stage fright.

470
00:29:32,238 --> 00:29:35,738
Ji Hoon, it's not working. Shall I
go to the toilet one more time?

471
00:29:37,838 --> 00:29:39,107
Are you insane?

472
00:29:39,108 --> 00:29:42,677
- Idiot, be a predator.
- What?

473
00:29:42,678 --> 00:29:45,147
There aren't any place
for herbivore on stage.

474
00:29:45,148 --> 00:29:47,417
Think of my slap if you get distracted.

475
00:29:47,418 --> 00:29:49,518
It'll burn until you come down the stage.

476
00:29:50,348 --> 00:29:52,657
I have to go now. I'll see
you often from now on.

477
00:29:52,658 --> 00:29:54,358
- Bye.
- Bye.

478
00:29:56,458 --> 00:29:58,027
How is it? Is her slap effective?

479
00:29:58,028 --> 00:29:59,798
Not at all! It's just the pain on my back.

480
00:30:02,868 --> 00:30:03,898
JB.

481
00:30:04,298 --> 00:30:05,737
It's time for you to go on stage.

482
00:30:05,738 --> 00:30:07,068
- Okay.
- Okay.

483
00:30:11,238 --> 00:30:12,877
Drill, are you ready?

484
00:30:12,878 --> 00:30:15,408
Ji Hoon, I'll rely on you.

485
00:30:15,548 --> 00:30:16,648
Let's go.

486
00:30:45,478 --> 00:30:47,238
Hello, it's World Entertainment.

487
00:30:47,338 --> 00:30:49,408
The CEO isn't here at the moment.

488
00:30:51,018 --> 00:30:53,848
I think he's out of reach
because of JB's schedule.

489
00:30:54,218 --> 00:30:57,088
Sure, I'll make sure to send the message.

490
00:30:58,688 --> 00:31:00,158
Hi, Ms. Choi.

491
00:31:01,758 --> 00:31:02,998
Are you back from your workout?

492
00:31:05,358 --> 00:31:06,857
Yes, because I'm on break.

493
00:31:06,858 --> 00:31:10,028
I thought you were writing songs.
Shouldn't you be busy then?

494
00:31:12,338 --> 00:31:14,938
That's... I'm working on it,

495
00:31:15,208 --> 00:31:16,767
but I get distracted often
and it's not going well.

496
00:31:16,768 --> 00:31:20,737
Can you stop calling me Ms. Choi?
We have no other employees anyway.

497
00:31:20,738 --> 00:31:22,447
Still, you're the team manager.

498
00:31:22,448 --> 00:31:25,078
But I'm the only employee this company has.

499
00:31:25,378 --> 00:31:28,417
It's says you're a team manager
on your business card.

500
00:31:28,418 --> 00:31:31,188
The CEO made it, not me.

501
00:31:31,558 --> 00:31:32,658
Whatever.

502
00:31:33,658 --> 00:31:36,458
Don't you have any songs you wrote?

503
00:31:38,258 --> 00:31:39,857
Why would I write songs?

504
00:31:39,858 --> 00:31:42,728
I already know. I heard
you're studying composition.

505
00:31:43,128 --> 00:31:44,838
Show me if you have any.

506
00:31:46,598 --> 00:31:49,108
If I show them to you,

507
00:31:50,508 --> 00:31:51,577
will you copy them?

508
00:31:51,578 --> 00:31:54,408
Gosh, seriously. You just hit my weak spot.

509
00:31:56,978 --> 00:32:00,178
I'm going to have some food. I
can't think since I'm hungry.

510
00:32:00,478 --> 00:32:03,048
- Tell the CEO that I was here.
- Okay.

511
00:32:04,958 --> 00:32:09,028
Oh, right. Did you get
any calls from Master?

512
00:32:10,328 --> 00:32:11,358
What?

513
00:32:13,428 --> 00:32:14,527
No.

