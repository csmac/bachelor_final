﻿1
00:00:26,292 --> 00:00:29,128
<i>Like to have your picture taken?</i>

2
00:00:29,161 --> 00:00:30,328
<i>No, maybe tomorrow.</i>

3
00:00:30,329 --> 00:00:31,563
<i>Where do you come from?</i>

4
00:00:31,564 --> 00:00:33,398
<i>From Sweden.</i>

5
00:00:33,399 --> 00:00:35,867
<i>The champion, what's your name?</i>

6
00:00:35,868 --> 00:00:38,269
<i>- Harry.
- Harry... It takes two seconds</i>

7
00:00:38,270 --> 00:00:39,871
Don't worry, it's free.
No problem.

8
00:00:39,872 --> 00:00:42,407
I make you the beauty picture
together. Come on.

9
00:00:42,408 --> 00:00:46,111
Come this way. Here.
It takes just two minutes.

10
00:00:46,112 --> 00:00:47,712
Look this way.
Very nice.

11
00:00:47,713 --> 00:00:51,783
Are you a champion ski?
Yes, you are.

12
00:00:51,784 --> 00:00:54,152
Are you
the world champion, Harry?

13
00:00:54,153 --> 00:00:55,720
Come next to your brother.

14
00:00:55,721 --> 00:00:58,156
<i>Madame, next to your daughter.</i>

15
00:00:58,157 --> 00:01:00,358
<i>And mister, next to your wife.</i>

16
00:01:00,359 --> 00:01:04,863
You stay two minutes.
Look at me this way.

17
00:01:04,864 --> 00:01:06,498
I want a beauty smile together.

18
00:01:06,499 --> 00:01:08,733
Mister, come next to your wife
like this.

19
00:01:08,734 --> 00:01:11,169
Very nice. Look at me...

20
00:01:11,170 --> 00:01:14,472
Stand up together.
One, two, like this.

21
00:01:14,473 --> 00:01:15,340
Smile. Good one.

22
00:01:15,341 --> 00:01:17,408
Another one like this.
Look at me...

23
00:01:17,409 --> 00:01:20,712
<i>Very nice. Another one
like this. One, two...</i>

24
00:01:20,713 --> 00:01:23,548
<i>Very good. Now...</i>

25
00:01:23,549 --> 00:01:27,218
Mister, can you put your arm
like this?

26
00:01:27,219 --> 00:01:31,391
Very good. And together. Can you
put your brother like this?

27
00:01:34,894 --> 00:01:37,362
Yes, very good.
Look at me together.

28
00:01:37,363 --> 00:01:40,465
Harry, it's okay for you?
Champion ski!

29
00:01:40,466 --> 00:01:41,900
One, two...
Look at me this way.

30
00:01:41,901 --> 00:01:42,901
Okay, smile.

31
00:01:42,902 --> 00:01:45,637
Very nice.
One more like this.

32
00:01:45,638 --> 00:01:46,871
Another one.

33
00:01:46,872 --> 00:01:49,941
And now, mister,
put your head on your wife.

34
00:01:49,942 --> 00:01:52,644
Head on head together,
put your head on.

35
00:01:52,645 --> 00:01:53,545
Yes, very good.

36
00:01:53,546 --> 00:01:56,282
One, two, one, two... Smile.
Good one!

37
00:03:45,758 --> 00:03:47,493
Mm-hmm.

38
00:04:38,544 --> 00:04:39,612
Woman:

39
00:05:45,511 --> 00:05:46,879
Tomas:

40
00:06:00,426 --> 00:06:04,162
- It's nice.
- Yes, this is nice.

41
00:06:04,163 --> 00:06:07,767
The light and everything
is perfect on the pictures.

42
00:06:20,179 --> 00:06:23,716
- Oh, his eyes...
- He's very beautiful.

43
00:06:29,488 --> 00:06:31,622
These are good together.
Like this.

44
00:06:31,623 --> 00:06:33,191
- What's her name?
- Vera.

45
00:06:33,192 --> 00:06:34,827
- And his?
- Harry.

46
00:06:44,903 --> 00:06:46,906
Tomas:

47
00:06:53,946 --> 00:06:55,847
Woman:

48
00:06:55,848 --> 00:06:56,849
Tomas:

49
00:07:31,884 --> 00:07:32,984
Vera:

50
00:07:32,985 --> 00:07:34,487
Tomas:

51
00:07:48,867 --> 00:07:50,736
Tomas:

52
00:07:54,006 --> 00:07:55,741
Tomas:

53
00:09:18,190 --> 00:09:20,760
Woman:

54
00:11:05,997 --> 00:11:07,800
Tomas:

55
00:11:41,133 --> 00:11:42,835
Mmm.

56
00:11:45,137 --> 00:11:47,139
Vera:

57
00:11:50,442 --> 00:11:52,243
Woman:

58
00:11:52,244 --> 00:11:54,013
Tomas:

59
00:12:10,095 --> 00:12:11,262
Harry:

60
00:12:11,263 --> 00:12:14,166
Harry:

61
00:12:56,474 --> 00:12:57,910
Man:

62
00:12:59,211 --> 00:13:00,312
Man:

63
00:13:01,046 --> 00:13:02,047
Woman:

64
00:13:03,114 --> 00:13:05,317
Woman:

65
00:13:09,020 --> 00:13:10,356
Woman:

66
00:13:19,231 --> 00:13:21,367
Child:

67
00:13:30,208 --> 00:13:31,343
Vera:

68
00:13:33,011 --> 00:13:34,847
Woman:

69
00:13:48,260 --> 00:13:49,962
Man:

70
00:13:56,468 --> 00:13:57,970
Tomas:

71
00:17:15,233 --> 00:17:16,301
Vera:

72
00:17:41,593 --> 00:17:44,495
Harry:

73
00:17:44,496 --> 00:17:47,433
Vera:

74
00:18:42,654 --> 00:18:44,888
Oh, hello...

75
00:18:44,889 --> 00:18:46,924
Sorry...

76
00:18:46,925 --> 00:18:48,792
Not much skiing today.

77
00:18:48,793 --> 00:18:51,828
Yes... Later we will...

78
00:18:51,829 --> 00:18:53,732
- Bye-bye.
- Thank you. Bye.

79
00:19:02,674 --> 00:19:04,409
Hi.

80
00:19:19,691 --> 00:19:20,926
Tomas:

81
00:19:22,393 --> 00:19:24,494
Ebba:

82
00:19:24,495 --> 00:19:26,265
Tomas:

83
00:19:42,780 --> 00:19:44,550
Hmm?

84
00:19:57,562 --> 00:19:58,730
Huh?

85
00:20:21,552 --> 00:20:23,488
Tomas:

86
00:20:29,827 --> 00:20:32,231
Tomas:

87
00:20:40,571 --> 00:20:43,842
Vera:

88
00:20:50,448 --> 00:20:52,684
Ebba:

89
00:21:06,998 --> 00:21:09,268
Harry:

90
00:23:15,593 --> 00:23:18,663
- Hello, darling.
- Hi, sweetie.

91
00:23:31,776 --> 00:23:32,776
Good?

92
00:23:33,678 --> 00:23:36,079
So, how was your day?

93
00:23:36,080 --> 00:23:38,882
Um... It was fantastic.

94
00:23:38,883 --> 00:23:40,951
I had a great day.

95
00:23:40,952 --> 00:23:44,754
I met this one.
I mean... yeah.

96
00:23:44,755 --> 00:23:48,158
Yeah, I mean we bumped
into each other this morning

97
00:23:48,159 --> 00:23:51,728
at the queue to the lift.

98
00:23:51,729 --> 00:23:56,233
And I guess we were both
longing for someone...

99
00:23:56,234 --> 00:23:58,168
Just kidding.

100
00:23:58,169 --> 00:23:59,536
No, we had a great day.

101
00:23:59,537 --> 00:24:03,673
Good snowboarding
and really nice talks.

102
00:24:03,674 --> 00:24:07,177
You told me
that you're very religious.

103
00:24:07,178 --> 00:24:11,114
- I... didn't say that.
- Yes, you did.

104
00:24:11,115 --> 00:24:14,251
Not that I'm very religious, no.

105
00:24:14,252 --> 00:24:16,887
Yeah, you did.
It's all right.

106
00:24:16,888 --> 00:24:19,222
What's wrong with that?

107
00:24:19,223 --> 00:24:23,660
I told... "this one"
that I'm not an atheist.

108
00:24:23,661 --> 00:24:27,097
Excuse me?
Who the fuck is "this one"?

109
00:24:27,098 --> 00:24:29,232
You said it before.

110
00:24:29,233 --> 00:24:31,134
- Did I?
- Yes, you did.

111
00:24:31,135 --> 00:24:32,903
"I met this one..."

112
00:24:32,904 --> 00:24:34,204
It's a joke.

113
00:24:34,205 --> 00:24:37,841
Anyway... Did you guys
brave the crowds today?

114
00:24:37,842 --> 00:24:38,809
Um...

115
00:24:38,810 --> 00:24:42,612
- What did you say?
- "Brave the crowds..."

116
00:24:42,613 --> 00:24:44,548
There were a lot of people
on the mountain today.

117
00:24:44,549 --> 00:24:48,151
So I meant, "Were you brave
enough to get out there?"

118
00:24:48,152 --> 00:24:50,620
Yeah, it was great.

119
00:24:50,621 --> 00:24:53,723
It was great,
we skied all morning...

120
00:24:53,724 --> 00:24:56,026
...and actually,
we went home after lunch,

121
00:24:56,027 --> 00:25:00,499
because we had some
kind of experience, actually.

122
00:25:05,269 --> 00:25:08,171
Well, we were sitting
at the top restaurant,

123
00:25:08,172 --> 00:25:10,607
with this magnificent view...

124
00:25:10,608 --> 00:25:11,875
It's a nice spot.

125
00:25:11,876 --> 00:25:14,210
And they had a...

126
00:25:14,211 --> 00:25:16,046
Yeah, we saw an avalanche.

127
00:25:16,047 --> 00:25:18,081
- An avalanche?
- Really?

128
00:25:18,082 --> 00:25:20,183
No shit!

129
00:25:20,184 --> 00:25:22,686
Yeah, but I mean...

130
00:25:22,687 --> 00:25:24,955
Sorry, it just sounds
very strange

131
00:25:24,956 --> 00:25:28,124
when we say it in English.
"An avalanche."

132
00:25:28,125 --> 00:25:31,761
- What should I say?
- No, it's...

133
00:25:31,762 --> 00:25:35,665
No, anyway... We're sitting
there and it was...

134
00:25:35,666 --> 00:25:39,102
It was, like,
a controlled avalanche.

135
00:25:39,103 --> 00:25:40,270
So, it was...

136
00:25:40,271 --> 00:25:41,805
A controlled avalanche, yeah...

137
00:25:41,806 --> 00:25:44,941
But quite quickly
it grew kind of big.

138
00:25:44,942 --> 00:25:48,345
I've never seen
such a big avalanche.

139
00:25:48,346 --> 00:25:50,947
And it was like...

140
00:25:50,948 --> 00:25:52,849
For a moment it looked
like it would...

141
00:25:52,850 --> 00:25:56,286
- smash into the restaurant.
- Wow.

142
00:25:56,287 --> 00:25:58,221
It was quite shocking.

143
00:25:58,222 --> 00:26:01,925
Yeah, when I talk about it
I still get these goose bumps.

144
00:26:01,926 --> 00:26:06,096
- What did you do?
- There's not much to do.

145
00:26:06,097 --> 00:26:08,264
It was horrifying, actually.

146
00:26:08,265 --> 00:26:10,967
- It scared you?
- Poor thing.

147
00:26:10,968 --> 00:26:14,371
- Kids are all right?
- Yeah, everyone is fine.

148
00:26:14,372 --> 00:26:16,906
You got a bit afraid,

149
00:26:16,907 --> 00:26:20,243
but it wasn't that...

150
00:26:20,244 --> 00:26:23,813
It was controlled and
they know what they're doing...

151
00:26:23,814 --> 00:26:26,716
He got so scared
that he ran away from the table.

152
00:26:26,717 --> 00:26:28,852
What?

153
00:26:28,853 --> 00:26:30,787
No! I did not.

154
00:26:30,788 --> 00:26:33,123
- No, no...
- Come on.

155
00:26:33,124 --> 00:26:35,225
No, no, no...

156
00:26:35,226 --> 00:26:37,027
You ran away from the table.

157
00:26:37,028 --> 00:26:39,831
What? No, I did not.

158
00:26:43,934 --> 00:26:44,868
Yes, you did.

159
00:26:44,869 --> 00:26:47,973
Oh, okay.
No. I did not.

160
00:27:04,689 --> 00:27:06,923
Huh?

161
00:27:06,924 --> 00:27:08,193
Tomas:

162
00:27:33,250 --> 00:27:36,820
Yeah, I think we're
still, like...

163
00:27:36,821 --> 00:27:39,789
under the influence from it.

164
00:27:39,790 --> 00:27:41,893
It's kind of shaky.

165
00:27:42,927 --> 00:27:46,930
Can you not run in ski boots?

166
00:27:46,931 --> 00:27:47,631
What?

167
00:27:47,632 --> 00:27:51,067
Can you not run in ski boots?

168
00:27:51,068 --> 00:27:52,103
I mean...

169
00:27:53,804 --> 00:27:56,372
Tomas says you cannot run
in ski boots.

170
00:27:56,373 --> 00:27:58,108
It's ridiculous!

171
00:27:58,109 --> 00:28:01,811
But isn't this a situation
that kind of comes really quick?

172
00:28:01,812 --> 00:28:03,947
How do you know how to react?

173
00:28:03,948 --> 00:28:05,482
You cannot run in ski boots?

174
00:28:05,483 --> 00:28:08,319
Not "run" run... No.

175
00:28:16,527 --> 00:28:18,528
You know,
I haven't skied in years,

176
00:28:18,529 --> 00:28:21,799
so I suppose I'm really not
the person to ask.

177
00:28:24,268 --> 00:28:26,805
Oh shit... This is...

178
00:28:39,416 --> 00:28:41,920
Lovely, lovely.

179
00:28:55,866 --> 00:28:58,269
Shall I take your wine?

180
00:30:01,165 --> 00:30:02,801
Hmm?

181
00:32:28,612 --> 00:32:30,248
Okay.

182
00:35:17,347 --> 00:35:19,417
Ebba:

183
00:35:29,393 --> 00:35:32,261
- Can we have some privacy?
- What do you want?

184
00:35:32,262 --> 00:35:35,531
Can we please have some privacy?
We're just having a talk.

185
00:35:35,532 --> 00:35:36,534
Please?

186
00:35:39,670 --> 00:35:40,670
Yes?

187
00:36:09,700 --> 00:36:10,466
Hi.

188
00:36:10,467 --> 00:36:12,470
Woman:

189
00:36:32,422 --> 00:36:33,791
Harry:

190
00:36:59,616 --> 00:37:01,619
- Yeah.
- Harry:

191
00:37:44,695 --> 00:37:47,063
Hey, watch out!

192
00:37:47,064 --> 00:37:49,667
Give us some warning
next time, yeah?

193
00:37:52,069 --> 00:37:55,539
Say "bar down" or something.

194
00:38:01,945 --> 00:38:04,282
- You okay, hon?
- Yeah.

195
00:39:50,687 --> 00:39:52,390
Wow.

196
00:41:06,129 --> 00:41:07,731
Ebba:

197
00:41:51,775 --> 00:41:54,678
No?

198
00:42:15,699 --> 00:42:16,767
Tomas:

199
00:45:02,465 --> 00:45:04,935
Tomas:

200
00:45:08,371 --> 00:45:09,740
Harry:

201
00:45:34,097 --> 00:45:36,331
Woman:

202
00:45:36,332 --> 00:45:39,168
Man:

203
00:45:39,169 --> 00:45:42,005
Woman:

204
00:46:11,201 --> 00:46:12,201
Ebba:

205
00:46:13,469 --> 00:46:15,137
Man:

206
00:46:15,138 --> 00:46:17,341
Ebba:

207
00:47:35,385 --> 00:47:37,519
Okay.

208
00:47:37,520 --> 00:47:41,024
Ebba:

209
00:47:49,032 --> 00:47:51,266
Fanny:

210
00:47:51,267 --> 00:47:53,070
Ebba:

211
00:47:58,408 --> 00:47:59,542
Ebba:

212
00:48:19,362 --> 00:48:21,899
Fanny:

213
00:48:32,342 --> 00:48:36,246
Tomas:

214
00:49:26,329 --> 00:49:27,531
Mats:

215
00:52:13,396 --> 00:52:14,664
Um...

216
00:54:51,487 --> 00:54:55,325
Ebba:

217
00:55:31,060 --> 00:55:36,766
Ebba:

218
00:56:02,858 --> 00:56:03,858
Tomas:

219
00:57:16,232 --> 00:57:18,701
Shh, shh.

220
00:57:20,970 --> 00:57:27,208
Ebba:

221
00:57:27,209 --> 00:57:32,882
Mats:

222
00:58:45,154 --> 00:58:48,091
Mats:

223
00:59:46,081 --> 00:59:49,686
Mats:

224
01:01:01,457 --> 01:01:05,028
Ebba:

225
01:01:36,058 --> 01:01:38,094
Tomas:

226
01:02:03,018 --> 01:02:04,854
Ebba:

227
01:02:07,389 --> 01:02:08,256
Hmm?

228
01:02:08,257 --> 01:02:12,829
Ebba:

229
01:02:14,997 --> 01:02:16,132
Tomas:

230
01:02:21,270 --> 01:02:23,504
Tomas:

231
01:02:23,505 --> 01:02:26,042
Mats:

232
01:04:44,313 --> 01:04:46,949
Mm.

233
01:07:52,868 --> 01:07:55,171
Third floor.

234
01:09:56,458 --> 01:09:58,460
Fanny:

235
01:10:02,764 --> 01:10:05,899
Mats:

236
01:10:05,900 --> 01:10:08,604
Fanny:

237
01:10:21,883 --> 01:10:23,586
Fanny:

238
01:10:38,800 --> 01:10:43,239
Fanny:

239
01:10:46,675 --> 01:10:48,277
Mats:

240
01:12:49,898 --> 01:12:51,500
Mats:

241
01:13:42,617 --> 01:13:43,886
Okay.

242
01:13:44,686 --> 01:13:46,555
Okay.

243
01:13:50,925 --> 01:13:52,659
Okay.

244
01:13:52,660 --> 01:13:53,962
Tomas:

245
01:15:29,891 --> 01:15:31,059
Okay.

246
01:15:51,279 --> 01:15:53,549
Tomas:

247
01:16:09,364 --> 01:16:12,868
Mats:

248
01:16:38,893 --> 01:16:41,296
Harry:

249
01:16:49,370 --> 01:16:51,139
Ebba:

250
01:17:27,341 --> 01:17:29,911
Vera:

251
01:17:32,880 --> 01:17:36,251
Harry:

252
01:19:12,146 --> 01:19:14,182
Tomas:

253
01:19:20,054 --> 01:19:22,355
Mats:

254
01:19:22,356 --> 01:19:23,925
Tomas:

255
01:19:53,321 --> 01:19:55,290
Mats:

256
01:19:57,325 --> 01:19:58,994
Huh?

257
01:20:30,258 --> 01:20:32,260
Tomas:

258
01:20:41,369 --> 01:20:43,136
Tomas:

259
01:20:43,137 --> 01:20:44,873
Mats:

260
01:20:55,116 --> 01:20:56,484
Mats:

261
01:22:00,448 --> 01:22:02,017
Hello, guys...

262
01:22:03,451 --> 01:22:05,185
- Are you having fun?
- What?

263
01:22:05,186 --> 01:22:08,221
- Are you having fun?
- Yeah, sure.

264
01:22:08,222 --> 01:22:10,590
Great place.

265
01:22:10,591 --> 01:22:11,825
Yeah, yeah...

266
01:22:11,826 --> 01:22:16,464
I just came to tell you that
my friend sitting over there...

267
01:22:18,199 --> 01:22:22,203
She thinks you're the most
good-looking man in this bar.

268
01:22:23,371 --> 01:22:25,238
Okay. Thank you.

269
01:22:25,239 --> 01:22:27,574
You're welcome.
Have a good time.

270
01:22:27,575 --> 01:22:31,279
- You too.
- Cheers.

271
01:22:50,297 --> 01:22:53,333
Excuse me,
I had to come back again...

272
01:22:53,334 --> 01:22:54,334
Uh-huh?

273
01:22:54,936 --> 01:22:57,337
I made a mistake...
She didn't mean you.

274
01:22:57,338 --> 01:23:00,275
She meant someone else,
my friend.

275
01:23:00,775 --> 01:23:03,109
She didn't mean you.

276
01:23:03,110 --> 01:23:05,378
- Okay.
- I'm sorry, it was my fault.

277
01:23:05,379 --> 01:23:08,381
- No, it's fine. Thank you.
- Just have a good time.

278
01:23:08,382 --> 01:23:13,119
Listen, it was my fault.
I'm really sorry for this.

279
01:23:13,120 --> 01:23:16,356
I was actually
pointing at someone else, so...

280
01:23:16,357 --> 01:23:22,097
- Let's go. Take it easy.
- But just... don't worry.

281
01:23:30,704 --> 01:23:32,138
Hey, hey, hey.

282
01:23:32,139 --> 01:23:34,808
- Hello... Are you funny?
- No, no.

283
01:23:34,809 --> 01:23:39,214
Think we're funny? What?

284
01:23:39,747 --> 01:23:42,348
Did we do anything?

285
01:23:42,349 --> 01:23:45,320
- We just came to apologize...
- Yeah, yeah...

286
01:23:47,288 --> 01:23:48,623
Take it easy.

287
01:23:51,525 --> 01:23:55,063
I didn't talk to you!
I didn't fucking talk to you!

288
01:30:46,240 --> 01:30:47,675
Ebba:

289
01:31:05,792 --> 01:31:09,497
Harry:

290
01:31:14,001 --> 01:31:15,870
Okay, um...

291
01:33:06,079 --> 01:33:07,179
Tomas:

292
01:33:07,180 --> 01:33:09,617
Ebba:

293
01:33:21,795 --> 01:33:24,699
Ebba:

294
01:34:06,306 --> 01:34:07,642
Ebba:

295
01:35:55,282 --> 01:35:57,151
Ebba:

296
01:36:38,225 --> 01:36:39,927
Ebba:

297
01:37:05,518 --> 01:37:07,188
Ebba:

298
01:37:09,089 --> 01:37:10,090
Tomas:

299
01:37:13,360 --> 01:37:15,362
Ebba:

300
01:38:16,089 --> 01:38:17,324
Ebba:

301
01:38:57,497 --> 01:38:59,466
Vera:

302
01:39:19,386 --> 01:39:20,454
Harry:

303
01:39:31,231 --> 01:39:33,399
Vera:

304
01:39:33,400 --> 01:39:36,268
Harry:

305
01:39:36,269 --> 01:39:37,537
Vera:

306
01:40:07,267 --> 01:40:10,471
Ebba:

307
01:42:57,704 --> 01:42:58,705
Ebba:

308
01:43:14,454 --> 01:43:16,456
Tomas:

309
01:43:31,471 --> 01:43:32,606
Tomas:

310
01:44:18,051 --> 01:44:20,754
Ebba:

311
01:47:18,931 --> 01:47:20,000
Vera:

312
01:48:05,645 --> 01:48:06,244
Tomas:

313
01:48:06,245 --> 01:48:07,279
Vera:

314
01:48:07,280 --> 01:48:08,648
Tomas:

315
01:48:32,939 --> 01:48:34,808
Harry:

316
01:48:37,009 --> 01:48:38,678
Tomas:

317
01:48:43,182 --> 01:48:44,818
Yeah.

318
01:48:56,195 --> 01:48:57,195
Vera:

319
01:50:34,026 --> 01:50:35,662
Woman:

320
01:50:54,346 --> 01:50:55,782
Yeah?

321
01:50:57,917 --> 01:51:01,721
Can you please
drive a little bit more careful?

322
01:51:28,214 --> 01:51:30,283
Woman:

323
01:51:50,039 --> 01:51:53,238
I want to get out of here,
can you open the doors, please?

324
01:51:53,239 --> 01:51:54,973
Can you open the doors, please?

325
01:51:54,974 --> 01:51:57,911
She's really scared here.
Come on...

326
01:52:00,980 --> 01:52:03,016
Man:

327
01:52:23,269 --> 01:52:24,370
Vera:

328
01:52:27,106 --> 01:52:30,108
Open the doors, please.
I want to get out of here!

329
01:52:30,109 --> 01:52:31,811
Open the doors!

330
01:52:33,949 --> 01:52:37,148
Open the fucking doors! I want
to get out of here. Please!

331
01:52:37,149 --> 01:52:38,785
Tomas:

332
01:52:47,159 --> 01:52:48,259
Hey, hey, hey!

333
01:52:48,260 --> 01:52:49,260
Hey, hey, hey!

334
01:52:49,662 --> 01:52:53,865
Calm down!
Everybody calm down!

335
01:52:53,866 --> 01:52:55,433
Everybody calm down!

336
01:52:55,434 --> 01:52:58,303
Act civilized,
or people will get hurt.

337
01:52:58,304 --> 01:53:00,104
One at a time, please.

338
01:53:00,105 --> 01:53:03,341
Let the women
and children out first.

339
01:53:03,342 --> 01:53:04,778
Please.

340
01:53:05,377 --> 01:53:08,281
Everybody, nice and gentle...

341
01:53:10,082 --> 01:53:11,082
Very good.

342
01:53:12,885 --> 01:53:14,619
Very good. Nice and gentle...

343
01:53:14,620 --> 01:53:17,924
Be aware,
going out on the road there.

344
01:53:59,265 --> 01:54:00,934
Ebba:

345
01:54:15,581 --> 01:54:18,418
Ebba:

346
01:54:48,314 --> 01:54:51,985
Mats:

347
01:54:55,321 --> 01:54:57,689
Fanny:

348
01:54:57,690 --> 01:54:59,325
Ebba:

349
01:55:15,207 --> 01:55:17,243
Ebba:

350
01:56:21,640 --> 01:56:23,109
Ebba:

351
01:56:23,675 --> 01:56:28,079
Mats:

352
01:56:28,080 --> 01:56:31,384
- Want a smoke?
- No, thank you.

353
01:56:37,423 --> 01:56:40,558
Actually, can I take one?

354
01:56:40,559 --> 01:56:43,997
- Yes, of course.
- Thank you.

