1
00:00:04,585 --> 00:00:10,942
Buster Keaton in The Saphead

2
00:00:11,942 --> 00:00:24,001
Directed by Herbert Blache
Written by June Mathis

3
00:00:25,928 --> 00:00:30,149
"The New Henrietta" was a
Broadway play in which...

4
00:00:30,161 --> 00:00:34,394
Douglas Fairbanks scored
a major success in 1913.

5
00:00:34,394 --> 00:00:38,600
It was first filmed with him
1915 under the name THE LAMB.

6
00:00:38,612 --> 00:00:42,829
When it was to be
remade by Metro in 1920,...

7
00:00:42,829 --> 00:00:48,406
Fairbanks himself suggested Buster Keaton
for his former role.

8
00:00:48,457 --> 00:00:52,395
Keaton was then under an
exclusive contract with...

9
00:00:52,407 --> 00:00:56,357
Joseph M. Schenck who agreed
to loan him to Metro.

10
00:00:56,357 --> 00:01:02,557
It was Keaton's first appearance
in a feature-length film.

11
00:01:02,639 --> 00:01:07,229
THE SAPHEAD'S great success earned Keaton
star billings in his subsequent films.

12
00:02:21,916 --> 00:02:31,390
A little street where money is everything-
and everything is money.

13
00:02:49,782 --> 00:02:55,688
Nicholas Van Alstyne- known, loved, and
feared as "The Old Nick of The Street."

14
00:03:14,306 --> 00:03:17,420
Old Nick's very private
secretary -- "Mr. Musgrave."

15
00:03:22,395 --> 00:03:25,461
"You tell him that Jim Hardy
from Arizona is here."

16
00:03:57,315 --> 00:04:01,690
"I used to pal with this old boy out west."

17
00:04:16,244 --> 00:04:19,071
"What the blazes brought you east, Jim?"

18
00:04:25,295 --> 00:04:29,799
"The Henrietta mine -- she's the all-fired
biggest proposition I ever seen."

19
00:04:44,140 --> 00:04:47,668
"Jim, you know more about a mine
than anybody on earth -- except me --"

20
00:04:54,842 --> 00:04:58,061
"I want a broker -- send for Watson Flint."

21
00:05:03,863 --> 00:05:08,915
"If you say the Henrietta is that big --
watch me gamble on her!"

22
00:05:32,146 --> 00:05:35,541
Mark Turner had succeeded in
marrying Old Nick's daughter -

23
00:05:35,541 --> 00:05:38,673
- but had never succeeded at anything else.

24
00:05:38,882 --> 00:05:42,002
MARK TURNER
BROKER

25
00:05:56,688 --> 00:06:01,211
His morning mail as usual was
all bills and no business.

26
00:06:04,864 --> 00:06:09,029
"There's a girl outside that
wants to see you personal."

27
00:06:29,386 --> 00:06:38,705
<i>I am destitute and very ill. I must
see you.</i> Henrietta Reynolds

28
00:06:46,604 --> 00:06:48,399
"Your wife's outside, sir."

29
00:07:05,500 --> 00:07:10,935
"Go back and tell her she'll hear
from me in a day or two."

30
00:08:10,793 --> 00:08:14,096
"I came to invite you to lunch."

31
00:08:21,368 --> 00:08:24,444
"I'm afraid you'd find me rotten company."

32
00:08:36,948 --> 00:08:41,672
"I work like a dog, Rose --
but I don't get anywhere."

33
00:08:56,418 --> 00:09:00,162
"Your father's commissions are
worth a fortune to a broker -

34
00:09:00,162 --> 00:09:03,086
- but he never gives me an order."

35
00:09:07,211 --> 00:09:13,285
"I'll have a talk with Dad about you
-- when he gets home this afternoon."

36
00:09:27,430 --> 00:09:30,402
Old Nick's castle.

37
00:09:37,536 --> 00:09:41,390
Bertie Van Alstyne, Esquire,
his father's hope and pride,

38
00:09:41,390 --> 00:09:43,310
breakfasts early -- in the afternoon.

39
00:10:09,262 --> 00:10:14,112
"I want to talk to you about
the way you treat Mark."

40
00:11:18,183 --> 00:11:24,335
<i>I am destitute and very ill. I must
see you.</i> Henrietta Reynolds

41
00:11:34,130 --> 00:11:41,044
"You keep away -- I'll see her,
and try to get your letters back."

42
00:11:49,092 --> 00:11:54,111
"If you'll stop talking I'll see
what I can do for your Mark."

43
00:12:00,267 --> 00:12:06,401
Bertie had been in love for years and
decided something must be done about it.

44
00:12:17,305 --> 00:12:23,367
Nick's ward, Agnes, was the girl
Bertie loved. He had confided this -

45
00:12:23,367 --> 00:12:26,856
- to his sister and his valet --
but had never mentioned it to her.

46
00:12:39,773 --> 00:12:45,482
"Agnes has telegraphed that she's
coming home from school today."

47
00:12:48,257 --> 00:12:51,946
"Don't get excited ----
you'll have plenty of time to meet her ---

48
00:12:51,946 --> 00:12:56,105
- the train doesn't arrive until 6:30."

49
00:13:34,959 --> 00:13:39,426
"What's happened to you lately?
Getting home at daylight -

50
00:13:39,426 --> 00:13:42,883
- and having a picture of Henrietta,
the dancer, in your room--"

51
00:13:48,707 --> 00:13:53,807
"I am leading a fast life because
I am in love with Agnes."

52
00:14:06,552 --> 00:14:10,178
How to win the modern girl
by Judith Blakely

53
00:14:14,047 --> 00:14:21,380
How to win the modern girl. THE Modern Girl has
no use for the old-fashioned man. She prefers -

54
00:14:21,392 --> 00:14:28,736
- sports to saints. Few girls now-a-days can
resist a dashing, gambling, drinking devil.

55
00:14:58,800 --> 00:15:03,270
Agnes was to arrive at the Grand Central -

56
00:15:03,270 --> 00:15:06,500
- so Bertie went to meet her
at the Pennsylvania.

57
00:15:19,298 --> 00:15:21,441
Six-thirty at the Grand Central.

58
00:16:08,861 --> 00:16:15,580
Agnes was an orphan --
whom Nick has brought up as his own child.

59
00:16:47,019 --> 00:16:49,398
"Nobody met me."

60
00:16:53,788 --> 00:16:57,617
"The saphead! I suppose he
forgot all about you."

61
00:17:34,917 --> 00:17:40,711
"Ask Hutchins to let me know
as soon as Mr. Bertie comes in."

62
00:17:45,419 --> 00:17:50,815
"Mr. Bertie is never home before
three in the morning, Miss."

63
00:18:32,294 --> 00:18:35,936
"I was waiting for someone
-- but she didn't come."

64
00:18:47,170 --> 00:18:48,670
Trash

65
00:18:52,284 --> 00:19:02,354
The Gay White Way
failing to cheer Bertie - his friends
try the side streets.

66
00:21:28,736 --> 00:21:33,224
"I'm having rotten luck --
lend me a couple of thousand."

67
00:21:42,472 --> 00:21:44,999
"Are those things worth
two thousand dollars?"

68
00:21:51,238 --> 00:21:53,868
"What are they made of?"

69
00:21:57,619 --> 00:22:02,959
"I'm awfully tired of this --
would you mind very much if I stop?"

70
00:22:18,301 --> 00:22:21,484
"Your winnings, Mr. V.
-- thirty-eight thousand."

71
00:22:23,033 --> 00:22:26,035
"Are you quite sure you can spare it?"

72
00:22:51,395 --> 00:22:57,240
Bertie sees a chance to get
a real reputation as a sport --

73
00:23:14,389 --> 00:23:16,471
"Will you see to arresting me, please?"

74
00:24:47,073 --> 00:24:50,425
Morning

75
00:24:56,527 --> 00:25:00,730
RAID ON GAMBLING PALACE.
SON OF NICHOLAS VAN ALSTYNE
AMONG THOSE PRESENT.

76
00:25:00,742 --> 00:25:07,341
Young Men of Prominent Families
Given Free Ride to Police Station.

77
00:25:08,899 --> 00:25:11,953
"But they might come and
take him to jail!"

78
00:25:41,881 --> 00:25:44,464
"Is it true you were in that
dreadful place?"

79
00:25:54,870 --> 00:25:57,294
"I was there because I love Agnes."

80
00:26:05,384 --> 00:26:08,682
"If you love Agnes, why
didn't you meet her?"

81
00:26:10,004 --> 00:26:12,169
"I did -- but she didn't come."

82
00:26:31,094 --> 00:26:33,713
"Bertie -- you've changed somehow."

83
00:27:21,909 --> 00:27:27,794
"I heard you tell Rose that you
loved me -- and I love you, too."

84
00:27:38,540 --> 00:27:42,656
"I've tricked you into loving
me -- when you know the truth -

85
00:27:42,656 --> 00:27:46,433
- I'm afraid you won't
care for me any more."

86
00:27:52,233 --> 00:27:53,632
"Tell me --"

87
00:27:57,342 --> 00:27:58,857
"I'm good!"

88
00:28:04,816 --> 00:28:11,186
"I've tried my best to get over it --
but I can't -- and I still kneel down -

89
00:28:11,186 --> 00:28:16,038
- and say my prayers every morning --
before I go to bed."

90
00:28:35,167 --> 00:28:38,369
"Bertie, who is Henrietta?"

91
00:28:40,398 --> 00:28:45,676
"I bought her picture-
but I never met her."

92
00:30:04,707 --> 00:30:06,747
"Agnes, you'd better go."

93
00:30:23,810 --> 00:30:27,666
"You've got to get out and
shift for yourself --

94
00:30:27,666 --> 00:30:30,539
- this is the last check you'll get
from me!"

95
00:30:33,544 --> 00:30:37,144
(Cashier's Check)
Bankers Trust Company
10,000 $   -  Nicholas Van Alstyne

96
00:30:41,256 --> 00:30:45,714
"How can you be so cruel!
I love Bertie,

97
00:30:45,714 --> 00:30:49,060
whether you do or not,
and I'm going to be his wife!"

98
00:30:56,459 --> 00:31:00,774
"If you've had the impertinence
to ask Agnes to marry you--"

99
00:31:05,734 --> 00:31:09,755
"-- I suppose I've got to give
you a little more."

100
00:31:16,547 --> 00:31:21,147
Bankers Trust Company
1,000,000 $

101
00:31:42,847 --> 00:31:44,772
"Not another cent!"

102
00:31:55,435 --> 00:32:00,325
"Not another word about marrying
Agnes until you go to work ----

103
00:32:00,325 --> 00:32:03,743
- and make something of yourself!"

104
00:32:15,100 --> 00:32:21,726
Poor Bertie-- cut off with a million --
moved into humble quarters at the Ritz.

105
00:32:37,695 --> 00:32:42,855
Watson Flint -- Nick's broker, and a
friend of the family.

106
00:33:06,112 --> 00:33:11,104
"I've got to go into business
like you and Mark Turner."

107
00:33:50,258 --> 00:33:58,063
<i>Dearest Agnes, you wanted me
to go into business so I have gone.</i>

108
00:34:06,736 --> 00:34:11,771
"I bought a seat on the Stock Exchange
for a hundred thousand dollars."

109
00:34:13,237 --> 00:34:16,733
"A hundred thousand dollars
for a seat?"

110
00:34:20,477 --> 00:34:23,349
"Well, furniture is way
up now-a-days."

111
00:34:27,815 --> 00:34:30,967
"I want to show you an
engagement ring."

112
00:35:23,348 --> 00:35:26,006
"You asked me to look
after it, sir."

113
00:36:11,279 --> 00:36:17,151
"If father won't let us marry, perhaps
you'll keep it to remember me by."

114
00:36:20,328 --> 00:36:25,474
"No matter what Uncle Nick says, I'll
marry you whenever you want me --"

115
00:36:33,114 --> 00:36:37,754
"We can't now, Bertie --
we must make arrangements first --

116
00:36:37,754 --> 00:36:41,317
- let's say next Tuesday!"

117
00:36:57,487 --> 00:37:00,807
"Leon, we're to be married
secretly Tuesday evening --

118
00:37:00,830 --> 00:37:03,461
- please make all the arrangements."

119
00:37:17,888 --> 00:37:21,408
"It would be a wonderful
surprise for her --

120
00:37:21,501 --> 00:37:24,541
- if we could only get it
ready in time."

121
00:37:31,930 --> 00:37:33,373
"We must!"

122
00:37:41,915 --> 00:37:50,943
You can never be sure a woman will
be on time -- even for her wedding.

123
00:38:23,498 --> 00:38:26,233
"I'm going to marry Bertie tonight."

124
00:39:04,726 --> 00:39:08,851
"But, Agnes -- you mustn't run away
-- we'll have the wedding here!"

125
00:39:25,203 --> 00:39:30,555
"Get out one of her little white dresses
-- and she can wear my wedding veil."

126
00:39:38,339 --> 00:39:42,277
"I'll manage father, and send
for the Reverend Murray Hilton."

127
00:39:51,319 --> 00:39:55,117
"I took you into business
because Rose wanted me to -

128
00:39:55,117 --> 00:39:58,112
- and you're making good!"

129
00:40:03,107 --> 00:40:08,047
"I've given you power of attorney because
I'm going on a cruise Wednesday -

130
00:40:08,047 --> 00:40:12,567
- and you'll be in charge
while I'm away."

131
00:40:25,658 --> 00:40:29,376
"Bertie and Agnes are going
to be married tonight --"

132
00:40:44,191 --> 00:40:46,292
"Perhaps they've locked her up."

133
00:41:19,550 --> 00:41:25,745
"All right -- let 'em be married in here
-- or down cellar -- or on the roof --"

134
00:41:27,539 --> 00:41:29,155
"-- but I won't see them."

135
00:41:49,514 --> 00:41:51,841
"I have called for Agnes."

136
00:42:14,236 --> 00:42:16,472
"I'm through with you --"

137
00:42:19,097 --> 00:42:22,039
"-- and as for Agnes, she
has disobeyed me --"

138
00:42:57,097 --> 00:42:59,717
"Now ask Bertie to forgive you."

139
00:43:23,656 --> 00:43:25,406
"We're going to be married here!"

140
00:44:19,737 --> 00:44:22,147
For the Minister

141
00:44:26,207 --> 00:44:28,494
"Anything else?"

142
00:45:04,830 --> 00:45:11,356
"I'll put one in every pocket -- then when
the minister asks me -- there you are!"

143
00:45:19,001 --> 00:45:22,701
(Address)
Henrietta Reynolds
518 S. Vine St, Omaha, Nebraska

144
00:45:26,077 --> 00:45:30,603
"If I die -- promise me you'll put
these into Mrs. Turner's hands."

145
00:47:30,726 --> 00:47:32,250
"I want Mrs. Turner!"

146
00:47:40,775 --> 00:47:47,653
"Henrietta is dead -- and I gave her my
sacred word I'd put these into your hands."

147
00:48:21,689 --> 00:48:27,815
<i>A dying mother appeals to you -
Have mercy on my little one. -</i>

148
00:48:27,815 --> 00:48:34,121
<i>- The letters from her father which I send
with this will tell you the rest.</i>
Henrietta Reynolds

149
00:48:54,697 --> 00:48:59,946
"I'm sorry this blow has come, Rose
-- I've tried to keep it from you --"

150
00:49:30,140 --> 00:49:36,013
"Bertie, these belong to you
---- they're from Henrietta."

151
00:51:27,435 --> 00:51:36,048
Bertie's valet had planned a great
surprise for the bridal couple.

152
00:53:36,787 --> 00:53:43,387
Tortured by thoughts of Agnes
-- Bertie hopes a visit to -

153
00:53:43,450 --> 00:53:48,731
- the Stock Exchange may
help him to forget.

154
00:54:16,475 --> 00:54:20,328
"This is a new member
-- Mr. Van Alstyne."

155
00:54:34,002 --> 00:54:38,803
"Mr. Turner wants to see you at
Van Alstyne's office right away."

156
00:54:56,473 --> 00:55:00,354
"Do all these seats cost a
hundred thousand dollars?"

157
00:55:44,291 --> 00:55:47,998
HE'S NEW
MAKE HIM WELCOME

158
00:57:12,701 --> 00:57:17,693
Knowing that the truth about Henrietta
Reynolds must soon come to light,

159
00:57:17,693 --> 00:57:22,809
Mark has induced Nick to go on
his yachting trip as planned.

160
00:57:41,977 --> 00:57:46,992
With Nick out of the way, Mark sees
a chance to clean up a fortune -

161
00:57:46,992 --> 00:57:51,343
- before his relations with Henrietta
Reynolds are discovered.

162
00:58:05,352 --> 00:58:08,569
"Mr. Van Alstyne notified
you that I would be in -

163
00:58:08,569 --> 00:58:11,134
- complete authority here during
his absence -- ?"

164
00:58:27,374 --> 00:58:31,064
"I want you to sell 'Henrietta
Mine' until you have -

165
00:58:31,064 --> 00:58:33,820
- knocked the price down
fifty points!"

166
00:58:41,854 --> 00:58:45,907
"I can't accept a commission
of that size without security."

167
00:59:02,059 --> 00:59:06,961
"Great heavens, Turner! -- these securities
belong to your father-in-law!"

168
00:59:10,468 --> 00:59:13,583
"While he is away they are mine."

169
00:59:32,896 --> 00:59:36,889
Bertie's introduction to the Floor
grows more and more informal.

170
01:00:02,692 --> 01:00:09,384
"All they do here is knock off hats --
but I enjoy it -- it occupies the mind."

171
01:00:19,842 --> 01:00:25,986
"I'm going out to get some new ones
so I can play it again."

172
01:00:38,925 --> 01:00:41,371
The raid on Henrietta.

173
01:00:52,909 --> 01:00:58,886
"They've started a raid on 'Henrietta'
-- you ought to be on the Floor!"

174
01:01:01,781 --> 01:01:05,045
"When I want your advice
I'll send for you!"

175
01:01:13,388 --> 01:01:16,778
"Mr. Tildon of the National City is here."

176
01:01:27,008 --> 01:01:30,082
BIG SLUMP IN HENRIETTA MINING STOCK

177
01:01:43,925 --> 01:01:49,839
"Mr. Van Alstyne won't return for a week
-- and Mr. Turner is in charge."

178
01:01:52,996 --> 01:01:57,821
"Tell Turner if we don't receive collateral
by three o'clock we'll be forced to -

179
01:01:57,821 --> 01:02:01,392
- throw Van Alstyne's
holdings on the market!"

180
01:02:23,426 --> 01:02:26,293
"Thank God you've come, sir!"

181
01:02:27,571 --> 01:02:29,917
"Thank God!
I was seasick--"

182
01:02:34,150 --> 01:02:37,250
...PA           ...HEN               ...INP
     200...67          200...37...        ...

183
01:02:38,862 --> 01:02:44,051
"Call up Mr. Tildon -- and send to
the Stock Exchange for Mark Turner!"

184
01:03:01,709 --> 01:03:05,140
"Mr. Turner hasn't been
on the Floor today!"

185
01:03:07,980 --> 01:03:11,042
"Mark Turner is responsible for
this raid, sir."

186
01:03:16,004 --> 01:03:21,139
"Because he's a cheat -
I found out he was responsible for -

187
01:03:21,139 --> 01:03:25,148
- that Henrietta girl, and put
the blame on Mr. Bertie-"

188
01:03:32,826 --> 01:03:36,987
"I'm sorry to hurry you --
but it's two-thirty,

189
01:03:36,987 --> 01:03:40,951
and your securities must be in the
bank before the market closes."

190
01:04:00,532 --> 01:04:04,863
"It's almost three o'clock.
There's nothing on earth I can do."

191
01:05:19,630 --> 01:05:26,086
"You're too late. Flint's on the Floor now,
knocking the last breath out of -

192
01:05:26,086 --> 01:05:30,320
- the 'Henrietta' -- and I can buy your
holdings for next to nothing!"

193
01:05:50,617 --> 01:05:52,758
"Get the police!"

194
01:06:06,377 --> 01:06:13,004
"All the police in the world can't prevent
your losing control of the Henrietta Mine -

195
01:06:13,004 --> 01:06:15,947
- when the gong rings in the
Stock Exchange!"

196
01:06:40,021 --> 01:06:43,121
-HENRIETTA
-HENRIETTA
-HENRIETTA

197
01:06:48,149 --> 01:06:49,449
-HENRIETTA!
-HENRIETTA!!
-HENRIETTA!!

198
01:06:53,004 --> 01:06:55,434
-HENRIETTA
-HENRIETTA
-HENRIETTA

199
01:07:01,524 --> 01:07:07,770
"Sir, you're insulting me!-- and I demand
that you stop yelling that woman's name!"

200
01:07:08,414 --> 01:07:10,014
HENRIETTA!

201
01:07:29,777 --> 01:07:31,577
HENRIETTA

202
01:08:22,265 --> 01:08:26,047
"They're all yelling that
woman's name at me --

203
01:08:26,047 --> 01:08:28,439
- and I can't make them stop!"

204
01:08:47,288 --> 01:08:52,215
"Bertie, listen! -- I'll tell you
something that will stop them!"

205
01:08:54,153 --> 01:08:57,400
"Every time you hear a
man yelling 'Henrietta' -

206
01:08:57,412 --> 01:09:00,671
- you grab him and
say -- 'I TAKE IT'!"

207
01:09:13,022 --> 01:09:17,908
It seems to Bertie that
the whole Stock Exchange -

208
01:09:17,908 --> 01:09:21,196
- is taunting him by yelling
Henrietta's name.

209
01:10:39,637 --> 01:10:43,384
"She's up forty points and
going like a rocket!"

210
01:11:53,346 --> 01:11:56,646
Van Alstyne's Henrietta Pounded to a Pulp

211
01:12:08,568 --> 01:12:13,268
...HEN                                   LAST
      7000...94...97....... 105.....

212
01:12:41,136 --> 01:12:44,687
"I've been saved, Flint!
-- saved by a miracle!"

213
01:12:46,663 --> 01:12:50,650
"It was no miracle -- it was Bertie!
He's the latest member of -

214
01:12:50,650 --> 01:12:53,433
- the Stock Exchange!"

215
01:13:03,773 --> 01:13:08,240
"He's bought a cottage on Long Island
-- this is his address."

216
01:14:53,280 --> 01:14:57,584
"I have been on the Floor of
the Stock Exchange."

217
01:15:11,069 --> 01:15:14,163
"Why, my boy -- you've saved 'Henrietta'!"

218
01:15:21,109 --> 01:15:25,052
"I mean the Henrietta
mine! Now get dressed,

219
01:15:25,052 --> 01:15:28,485
because I've sent for
Agnes and the minister."

220
01:16:46,919 --> 01:16:50,020
THE END

221
01:16:50,420 --> 01:16:53,440
Subtitle By : Reza Fa

