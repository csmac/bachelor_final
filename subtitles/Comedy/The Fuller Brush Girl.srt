﻿1
00:00:53,046 --> 00:00:58,046
Sync and corrections by explosiveskull
www.addic7ed.com

2
00:01:10,297 --> 00:01:12,299
Wake up! Wake up! Wake up!

3
00:01:13,508 --> 00:01:14,342
Why?

4
00:01:14,551 --> 00:01:16,136
It's time for Sunday brunch!

5
00:01:16,344 --> 00:01:19,556
Oh, come back
when it's time for Sunday dinner.

6
00:01:23,059 --> 00:01:25,687
But Mom made
her famous blueberry pancakes.

7
00:01:25,896 --> 00:01:30,567
And she used duck eggs for more nutrition
and a deeper flavor profile.

8
00:01:32,903 --> 00:01:35,113
What? I like my cooking shows.

9
00:01:35,990 --> 00:01:37,908
- Come on!
- No, no, no.

10
00:01:37,992 --> 00:01:39,409
- Come on!
- No.

11
00:01:39,493 --> 00:01:40,786
Come on!

12
00:01:41,411 --> 00:01:42,454
Here, Tommy.

13
00:01:42,537 --> 00:01:46,583
Enjoy your breakfast bottle,
the most important bottle of the day.

14
00:01:52,840 --> 00:01:54,716
- Morning, Mom.
- There he is!

15
00:01:54,800 --> 00:01:59,179
My beautiful firstborn baby boy.

16
00:01:59,387 --> 00:02:01,306
Have a seat right at the head of the table

17
00:02:01,389 --> 00:02:03,809
and while you wait
for your favorite pancakes,

18
00:02:04,018 --> 00:02:06,770
enjoy a pre-breakfast milkshake.

19
00:02:08,856 --> 00:02:11,149
- You're being weird.
- I'm not being weird.

20
00:02:11,232 --> 00:02:15,195
Can't a mom make her beautiful
firstborn son a pre-breakfast milkshake?

21
00:02:16,822 --> 00:02:18,198
Okay, that was kind of weird.

22
00:02:19,867 --> 00:02:22,619
- Can I have some hot fudge on my pancakes?
- Sure, honey.

23
00:02:22,828 --> 00:02:24,329
Okay, you're up to something.

24
00:02:26,540 --> 00:02:31,127
I got her up.
Aunt Stephanie is not a morning person.

25
00:02:33,839 --> 00:02:37,676
No. No, Aunt Stephanie is a night person.

26
00:02:38,927 --> 00:02:42,222
You know, when the clubs are going off,
bottles are popping,

27
00:02:42,305 --> 00:02:43,557
people are hooking up...

28
00:02:46,393 --> 00:02:48,603
...their Xboxes.

29
00:02:49,437 --> 00:02:53,191
- Aunt Steph, let's go get some juice.
- I don't want any juice.

30
00:02:54,651 --> 00:02:57,237
Then help me pick out my juice.

31
00:02:58,989 --> 00:03:01,199
She's such a good juice picker-outer.

32
00:03:06,080 --> 00:03:10,208
I still haven't told the boys
Kimmy and Ramona are moving in today.

33
00:03:10,876 --> 00:03:12,127
Chicken.

34
00:03:12,210 --> 00:03:16,798
- Okay, I'm a little chicken.
- No, I'm talking about the fried chicken.

35
00:03:18,299 --> 00:03:21,887
The boys have been through so many changes
and now here comes another one,

36
00:03:21,970 --> 00:03:24,431
and I just don't know
how they're going to react.

37
00:03:24,514 --> 00:03:29,019
So don't say a word until
I find the right moment to break the news.

38
00:03:29,103 --> 00:03:30,270
Sorry,

39
00:03:30,353 --> 00:03:31,563
I spilled the beans.

40
00:03:32,230 --> 00:03:33,565
You told them?

41
00:03:33,648 --> 00:03:36,777
No, I spilled the beans
when I was reaching for the chicken.

42
00:03:39,237 --> 00:03:41,990
Mom, please don't make me do this.

43
00:03:42,240 --> 00:03:45,201
Moving in with D.J. and those boys
is a terrible idea.

44
00:03:45,410 --> 00:03:49,581
D.J. really needs us and you love her.
She's your godmother.

45
00:03:49,790 --> 00:03:51,583
Really? I have a godmother?

46
00:03:53,501 --> 00:03:55,545
So, like, three wishes?

47
00:03:56,421 --> 00:03:58,632
That's a fairy godmother.

48
00:03:58,840 --> 00:04:02,844
Do you really want me to change schools
and be the outcast loner kid,

49
00:04:02,928 --> 00:04:05,764
all to move in
with the whitest family in America?

50
00:04:07,265 --> 00:04:09,935
The Fullers are like albino polar bears

51
00:04:10,018 --> 00:04:12,312
drinking milk

52
00:04:12,520 --> 00:04:14,230
in a snowstorm,

53
00:04:14,314 --> 00:04:16,775
watching <i>Frozen.</i>

54
00:04:18,735 --> 00:04:20,070
That's pretty white.

55
00:04:32,582 --> 00:04:34,168
I am not going in there.

56
00:04:34,250 --> 00:04:37,295
There is nothing you can do
to make me walk inside that house.

57
00:04:42,050 --> 00:04:43,135
<i>Ramona?</i>

58
00:04:44,052 --> 00:04:46,805
That was so evil.

59
00:04:48,849 --> 00:04:50,142
But extremely effective.

60
00:05:03,446 --> 00:05:05,573
Mom, just tell me what's going on.

61
00:05:05,657 --> 00:05:07,742
Um... Okay.

62
00:05:07,826 --> 00:05:10,996
You might think this is bad news,
but it really is good news,

63
00:05:11,079 --> 00:05:14,249
just hiding in not-so-great news.

64
00:05:14,457 --> 00:05:17,878
It's like cauliflower
filled with jellybeans.

65
00:05:19,713 --> 00:05:22,049
Come on, Mom. How bad can it be?

66
00:05:22,841 --> 00:05:25,177
I've waited my whole life to say this...

67
00:05:25,260 --> 00:05:27,179
home, sweet home.

68
00:05:33,894 --> 00:05:34,895
No.

69
00:05:36,146 --> 00:05:37,313
Yes.

70
00:05:37,939 --> 00:05:40,150
Come here, sister-wife.

71
00:05:44,196 --> 00:05:46,656
So, now you all know the good news.

72
00:05:46,865 --> 00:05:48,909
Kimmy and Ramona are here to help us out.

73
00:05:48,992 --> 00:05:51,245
And we really appreciate it,
don't we, boys?

74
00:05:51,452 --> 00:05:55,331
Works for me. The more people
to drive me around, the merrier.

75
00:05:56,917 --> 00:05:59,961
Jellybean-filled cauliflower my butt!

76
00:06:03,090 --> 00:06:05,633
Ooh, the mouth on that kid!

77
00:06:07,552 --> 00:06:09,763
Oh, I feel at home already.

78
00:06:13,934 --> 00:06:16,561
Oh, that's sweet.
You kept Ramona's turkey art?

79
00:06:16,770 --> 00:06:18,521
No, that's mine.

80
00:06:20,523 --> 00:06:24,278
This is so cool.
I'm living in Jesse and Becky's old room.

81
00:06:24,485 --> 00:06:26,446
I was never allowed up here before.

82
00:06:28,364 --> 00:06:31,201
I think Becky was always
a little jealous of me.

83
00:06:32,160 --> 00:06:34,370
Yeah, that was definitely the reason.

84
00:06:35,914 --> 00:06:38,292
This will be the new
international headquarters

85
00:06:38,374 --> 00:06:40,961
for Gibbler Style party planning.

86
00:06:41,628 --> 00:06:44,380
My Fathead will look great right here.

87
00:06:53,140 --> 00:06:54,599
Gotta love it!

88
00:06:57,560 --> 00:06:58,728
So where's my room?

89
00:06:58,812 --> 00:07:02,565
Oh, right through this door.
This is where Nicky and Alex used to live.

90
00:07:08,905 --> 00:07:10,824
What do you think? Pretty cozy.

91
00:07:11,491 --> 00:07:13,743
Were Nicky and Alex hobbits?

92
00:07:16,246 --> 00:07:18,165
Maybe I should go live with Papa.

93
00:07:18,832 --> 00:07:22,961
You know your papa is always on the road,
so you're stuck with your mama.

94
00:07:23,837 --> 00:07:26,714
Or you can give me a nice apartment
and I can raise myself.

95
00:07:26,798 --> 00:07:29,884
Ramona, do your homework.
Ramona, clean your room.

96
00:07:30,093 --> 00:07:32,095
Ramona, listen to Ramona.

97
00:07:32,179 --> 00:07:34,681
Wow, this mom thing is so easy.

98
00:07:35,974 --> 00:07:38,018
I got an idea from when I was a kid.

99
00:07:38,101 --> 00:07:41,229
This might work out great
for Jackson and Max, and for you.

100
00:07:41,313 --> 00:07:44,149
What if I have the boys share a room,

101
00:07:44,232 --> 00:07:47,319
and you could have
your own bedroom downstairs?

102
00:07:47,402 --> 00:07:50,071
Private, big closet...

103
00:07:50,280 --> 00:07:51,489
I'm listening.

104
00:07:52,573 --> 00:07:53,992
That was it.

105
00:07:55,869 --> 00:07:57,954
Or stay up here with me.

106
00:07:58,163 --> 00:07:59,331
It will be so much fun.

107
00:07:59,414 --> 00:08:02,209
We'll have mother-daughter
slumber parties every night.

108
00:08:03,459 --> 00:08:05,253
Let's go check out that other bedroom.

109
00:08:12,427 --> 00:08:14,304
Jackson, perfect timing.

110
00:08:14,971 --> 00:08:17,849
I baked your favorite... chocolate cake.

111
00:08:17,932 --> 00:08:19,517
Mom, Mom, Mom,

112
00:08:19,726 --> 00:08:23,688
if this is another bribe about Kimmy
and Ramona moving in, I'm cool.

113
00:08:23,897 --> 00:08:26,482
I can just go hide in my room.

114
00:08:28,735 --> 00:08:30,570
My boy cave.

115
00:08:31,571 --> 00:08:33,448
About your boy cave,

116
00:08:34,032 --> 00:08:35,992
there's something else I need to tell you.

117
00:08:36,076 --> 00:08:37,618
Well, it's really not bad news.

118
00:08:37,827 --> 00:08:39,537
I wouldn't say it's great news.

119
00:08:40,496 --> 00:08:42,457
I wouldn't say terrible.

120
00:08:42,540 --> 00:08:44,334
I wouldn't say wonderful.

121
00:08:44,959 --> 00:08:50,257
Jackson! Your room is under attack!
This is not a drill!

122
00:08:52,759 --> 00:08:55,095
Really got to work
on getting to the point.

123
00:08:55,887 --> 00:08:57,013
My stuff!

124
00:08:57,097 --> 00:08:59,724
Excuse me.
I found some blue and white paint.

125
00:08:59,933 --> 00:09:02,769
I want my room the same colors
as the flag of my people.

126
00:09:06,314 --> 00:09:08,858
You gave away my room?

127
00:09:09,609 --> 00:09:11,986
To give you a much bigger one.

128
00:09:13,988 --> 00:09:15,949
In fact, it's so big,

129
00:09:16,032 --> 00:09:19,202
there's enough room for you and Max.

130
00:09:19,411 --> 00:09:22,789
- Now I'm living with Max?
- I'm living with Jackson?

131
00:09:22,872 --> 00:09:26,418
This is the best day of my life!

132
00:09:30,588 --> 00:09:32,340
Okay, here's the game plan...

133
00:09:32,424 --> 00:09:34,550
Ramona is moving into Jackson's room,

134
00:09:34,634 --> 00:09:37,053
the baby is going into Max's old room,

135
00:09:37,262 --> 00:09:41,724
and the two of you are moving
across the hall into the...

136
00:09:42,850 --> 00:09:44,311
good room.

137
00:09:45,853 --> 00:09:48,398
Awesome! Give me a hug, roomie!

138
00:09:50,942 --> 00:09:52,444
Come on, Jackson.

139
00:09:52,527 --> 00:09:54,946
This will be a really good thing
for you and Max.

140
00:09:55,029 --> 00:09:59,033
- It will bring you closer together.
- How much closer can we be?

141
00:10:00,160 --> 00:10:01,911
Let's find out in the good room.

142
00:10:07,583 --> 00:10:09,585
Hey, that cake was for the boys.

143
00:10:10,128 --> 00:10:11,421
Hm? Oh.

144
00:10:11,505 --> 00:10:13,673
We know. We saved them each a slice.

145
00:10:15,091 --> 00:10:16,050
Ooh.

146
00:10:18,219 --> 00:10:20,388
I've got some bad news.

147
00:10:20,596 --> 00:10:22,849
Great, another cake!

148
00:10:24,434 --> 00:10:27,395
As if today weren't crazy enough,
my boss just called.

149
00:10:27,479 --> 00:10:29,730
There's an emergency
down at the pet clinic.

150
00:10:29,814 --> 00:10:31,774
- I wish I didn't have to go...
- Fear not.

151
00:10:31,858 --> 00:10:35,653
This is exactly why Kimmy and I are here...
to make your life easier.

152
00:10:35,862 --> 00:10:39,282
- We are Team D.J.
- And Team D.J.'s got your back.

153
00:10:39,491 --> 00:10:43,537
Oh, thank you, guys, so much. I don't know
what I'd do if you weren't here.

154
00:10:44,162 --> 00:10:45,455
Team D.J. on three.

155
00:10:51,378 --> 00:10:53,547
- One, two...
- Well, hold on.

156
00:10:53,754 --> 00:10:56,049
Is it one, two,
and then we say "Team D.J." on three?

157
00:10:56,132 --> 00:10:58,385
Or is it one, two, three,
and then we say it?

158
00:11:01,095 --> 00:11:04,057
That would be going on four.
Nobody goes on four.

159
00:11:05,141 --> 00:11:06,684
What am I, Canadian?

160
00:11:07,477 --> 00:11:09,270
I'm going to let Team D.J. work this out.

161
00:11:09,354 --> 00:11:11,272
I've been putting up with you
for 25 years.

162
00:11:11,356 --> 00:11:13,941
- You have put up with me?
- Why can't you do simple math?

163
00:11:14,775 --> 00:11:16,152
It's moving day!

164
00:11:16,236 --> 00:11:19,322
I'm moving in with Jackson!
It's moving day!

165
00:11:19,531 --> 00:11:22,617
I'm moving in with Jackson!

166
00:11:22,825 --> 00:11:25,245
Ah, there's my favorite little brother.

167
00:11:25,328 --> 00:11:26,704
Oh, no offense, baldy.

168
00:11:28,831 --> 00:11:30,250
You're a smart kid, right?

169
00:11:30,458 --> 00:11:32,960
I've been told I'm very bright for my age.

170
00:11:34,504 --> 00:11:39,300
Well, then you understand
that you and me in the same room,

171
00:11:39,384 --> 00:11:40,801
it's never going to work.

172
00:11:41,010 --> 00:11:43,679
But Mom said living together
would bring us closer.

173
00:11:43,888 --> 00:11:45,640
Moms lie.

174
00:11:47,392 --> 00:11:50,645
Consider this...
in my room, I'm in charge of you,

175
00:11:50,853 --> 00:11:55,609
but in this room,
you are in charge of Tommy.

176
00:11:57,444 --> 00:12:00,447
- Don't you want to be The Man?
- Of course I do.

177
00:12:00,530 --> 00:12:04,367
I've wanted to be The Man
ever since I was The Baby.

178
00:12:05,701 --> 00:12:09,539
Well, then it's settled.
You're staying here. Knuckles.

179
00:12:15,295 --> 00:12:18,465
What is that nastiness?

180
00:12:20,007 --> 00:12:24,095
- Um, uh, I don't smell a thing.
- It's Tommy's diaper!

181
00:12:25,721 --> 00:12:29,183
No way am I living in this stinkatorium!

182
00:12:33,187 --> 00:12:37,567
Thanks a lot, kid.
I was this close. This close.

183
00:12:46,660 --> 00:12:48,161
I want my room back.

184
00:12:48,953 --> 00:12:54,875
Jaxito, moving here was my mom's idea,
not mine. So we're both victims.

185
00:12:56,043 --> 00:12:58,546
I'm just the victim
with the private bedroom.

186
00:12:58,630 --> 00:13:00,840
And have you seen the city view?

187
00:13:01,466 --> 00:13:03,843
Uh, yeah. I used to live here.

188
00:13:08,306 --> 00:13:09,307
Oh, hey, Jackson.

189
00:13:10,057 --> 00:13:12,686
Oh, hey, Aunt Steph.

190
00:13:14,479 --> 00:13:16,856
Here's a once-in-a-lifetime opportunity.

191
00:13:17,064 --> 00:13:21,861
Imagine the thrill of sharing a room
with your adorable nephew, Max.

192
00:13:22,570 --> 00:13:24,280
I'm imagining it, okay?

193
00:13:24,364 --> 00:13:27,450
Now you imagine me saying,
"No, thank you."

194
00:13:29,619 --> 00:13:30,953
Hey, now.

195
00:13:39,420 --> 00:13:42,089
Hey. Aren't you driving down to LA
with your stuff?

196
00:13:42,173 --> 00:13:44,008
Isn't Becky waiting for you down there?

197
00:13:44,217 --> 00:13:46,761
Yeah, she is
but I forgot my lucky guitar, Priscilla.

198
00:13:46,844 --> 00:13:49,013
I left it here so I wouldn't forget it,
but forgot.

199
00:13:49,222 --> 00:13:51,725
I'm starting to think
this guitar is not so lucky.

200
00:13:52,767 --> 00:13:54,435
Uncle Jesse, you've got to help me.

201
00:13:54,519 --> 00:13:56,854
They want me to share a room
with a seven-year-old.

202
00:13:57,063 --> 00:13:59,482
I've got the fix.
You and Max, go hug it out.

203
00:14:00,900 --> 00:14:03,819
We already did that.
I just got him off of me.

204
00:14:04,571 --> 00:14:07,281
Well, you know, we used to hug it out
every day in the '80s.

205
00:14:08,616 --> 00:14:12,328
Sometimes it was so sweet,
you could hear violin music.

206
00:14:13,413 --> 00:14:15,707
- Uncle Jesse? I got this.
- You got chicken?

207
00:14:15,790 --> 00:14:17,542
- We got chicken.
- You got this.

208
00:14:19,252 --> 00:14:22,338
Jackson, when I was five,
my dad forced me to move in with D.J.,

209
00:14:22,422 --> 00:14:24,591
and then he gave my bedroom
to Uncle Jesse.

210
00:14:24,799 --> 00:14:28,052
But when I lived there it was
wall-to-wall with girly pink bunnies.

211
00:14:28,261 --> 00:14:30,846
You know, that could explain
my love for the theatre.

212
00:14:31,055 --> 00:14:32,973
- Proceed.
- Thank you.

213
00:14:33,182 --> 00:14:37,311
Anyway, D.J. was furious so she moved down
to the garage. Of course, she got caught.

214
00:14:37,395 --> 00:14:40,690
If she wanted her own room, she should
have run away, rented an apartment.

215
00:14:41,357 --> 00:14:43,150
- Would that have worked?
- I doubt it.

216
00:14:43,234 --> 00:14:45,069
At age ten,
her credit score was really low.

217
00:14:49,574 --> 00:14:50,866
But it all turned out fine

218
00:14:50,950 --> 00:14:54,161
because D.J. realized how lucky she was
to live with me.

219
00:14:54,245 --> 00:14:55,413
Do you get the lesson here?

220
00:14:55,580 --> 00:15:00,918
You know what? I do. I'm going to go
upstairs right now and bond with Max.

221
00:15:02,211 --> 00:15:04,213
Thanks, guys.

222
00:15:05,131 --> 00:15:08,593
Steph, you know, for your first aunt talk,
I think you crushed it.

223
00:15:08,676 --> 00:15:10,219
- I did, didn't I?
- You'd better.

224
00:15:10,303 --> 00:15:14,056
I gave you, like, 1,000 uncle talks,
except you forgot two important words.

225
00:15:14,140 --> 00:15:15,809
"Have mercy."

226
00:15:17,602 --> 00:15:19,562
No. "How rude."

227
00:15:21,230 --> 00:15:24,442
All right, Jesse has left the building.

228
00:15:25,985 --> 00:15:27,945
But first, Jesse has to pee.

229
00:15:32,283 --> 00:15:36,203
Diaper changing is simple.
Just remove, refresh and replace.

230
00:15:36,412 --> 00:15:39,332
Then wait for Tommy to reload.

231
00:15:41,125 --> 00:15:45,087
My friends are skiing in St. Moritz
right now, and I'm here changing diapers.

232
00:15:45,880 --> 00:15:46,881
Selfie.

233
00:15:48,842 --> 00:15:50,510
They're going to be so jealous.

234
00:15:51,260 --> 00:15:52,386
Okay, let's do this.

235
00:15:52,595 --> 00:15:54,514
Wait. I'm not ready.

236
00:15:57,975 --> 00:16:00,019
Unleash the Kraken!

237
00:16:01,354 --> 00:16:03,481
All right. Here we go.

238
00:16:04,315 --> 00:16:07,026
You know, I haven't done this
since Michelle was a baby.

239
00:16:07,861 --> 00:16:11,405
I changed so many of Michelle's diapers,
I feel like there were two of her.

240
00:16:14,742 --> 00:16:17,745
Here comes the tricky part.
Watch out for the fire hose!

241
00:16:17,829 --> 00:16:18,954
Yes.

242
00:16:19,038 --> 00:16:22,458
Huh... Boom! Drop the mic.

243
00:16:27,171 --> 00:16:28,214
Not me.

244
00:16:31,509 --> 00:16:32,510
Uh-oh...

245
00:16:37,640 --> 00:16:39,141
Hello?

246
00:16:40,476 --> 00:16:42,770
- <i>Stephanie?</i>
- Um, Deej...

247
00:16:42,978 --> 00:16:45,940
- Hey. What's up?
<i>- I'm just checking in.</i>

248
00:16:46,148 --> 00:16:49,485
- How is it going with Tommy?
- We've never been closer.

249
00:16:56,534 --> 00:16:57,744
What was that?

250
00:16:59,370 --> 00:17:01,539
Oh, you know, I'm playing <i>Angry Birds</i>

251
00:17:01,622 --> 00:17:02,623
and I just shot a duck.

252
00:17:05,167 --> 00:17:06,627
There's a duck in <i>Angry Birds?</i>

253
00:17:06,836 --> 00:17:08,212
It might have been a goose.

254
00:17:08,295 --> 00:17:11,298
Oh, look,
I'm getting a really important phone call.

255
00:17:11,507 --> 00:17:14,134
- From who?
- Not sure. It might be a butt dial.

256
00:17:14,218 --> 00:17:15,929
Okay, bye, Deej.

257
00:17:18,514 --> 00:17:19,682
Quick thinking.

258
00:17:19,766 --> 00:17:20,892
Well, gotta go.

259
00:17:22,268 --> 00:17:23,102
Me, too.

260
00:17:24,228 --> 00:17:26,355
Have fun getting that phone back.

261
00:17:28,524 --> 00:17:31,402
♪<i> I said, a hunk, a hunk of burning love </i>♪

262
00:17:31,611 --> 00:17:34,488
<i>♪ Yeah, I'm just a hunk
A hunk of burning love ♪</i>

263
00:17:34,572 --> 00:17:37,533
<i>♪ Wow, I'm just a hunk
A hunk of burning love ♪</i>

264
00:17:37,617 --> 00:17:41,036
That song is so good, I could listen to it
a thousand times, and I think I will.

265
00:17:42,455 --> 00:17:47,126
<i>♪ Lordy, lordy
Feel my temperature rising ♪</i>

266
00:17:48,878 --> 00:17:50,170
Siri, call Becky.

267
00:17:50,797 --> 00:17:53,758
<i>Please leave</i>
<i>your message after the tone.</i>

268
00:17:53,841 --> 00:17:55,468
Hey, Babe, I just want to tell you...

269
00:17:55,551 --> 00:17:59,263
<i>♪ Girl, girl, girl
You're going to set me on fire! ♪</i>

270
00:17:59,346 --> 00:18:00,681
Stop!

271
00:18:03,476 --> 00:18:06,938
- What are you doing here?
- I'm moving to LA with my favorite uncle.

272
00:18:07,146 --> 00:18:10,650
Know what you're not doing?
Moving to LA with your favorite uncle.

273
00:18:10,733 --> 00:18:13,068
But if I'm in the truck,
you can use the carpool lane.

274
00:18:13,277 --> 00:18:16,489
Oh, that would save some time...
No, get up here.

275
00:18:19,325 --> 00:18:20,743
All right.

276
00:18:23,621 --> 00:18:25,289
Is this about losing your bedroom?

277
00:18:25,498 --> 00:18:28,584
No, it's not just that.
I mean, it's everything.

278
00:18:28,668 --> 00:18:31,004
I used to live in a house full of guys.

279
00:18:31,086 --> 00:18:34,799
Now, there's women everywhere.
I'm outnumbered.

280
00:18:35,008 --> 00:18:38,970
Living with women, you'll learn valuable
lessons, like how to talk to them.

281
00:18:39,053 --> 00:18:41,430
Take Becky and her terrible cooking,
for instance.

282
00:18:41,514 --> 00:18:45,142
She makes this lasagna that tastes like...
What's the word...? Sweatpants.

283
00:18:46,602 --> 00:18:48,521
And I just keep feeding it to the dog

284
00:18:48,604 --> 00:18:51,190
because I don't know
how to tell her how disgusting it is.

285
00:18:51,273 --> 00:18:52,859
Now our dog weighs about 300 pounds.

286
00:18:56,195 --> 00:18:58,280
It was still recording.

287
00:18:58,364 --> 00:19:01,116
She heard my whole...
She's going to kill me.

288
00:19:01,909 --> 00:19:05,287
Not if we hide out in Mexico.
We could fight bulls together.

289
00:19:05,496 --> 00:19:09,333
No, it can't work because
I don't look good in matador culottes.

290
00:19:09,416 --> 00:19:12,461
Text your mom, tell her you're with me
and we're on our way home.

291
00:19:14,088 --> 00:19:16,590
Oh, come on, kid.
We can have some fun.

292
00:19:16,674 --> 00:19:17,717
Watch this.

293
00:19:18,384 --> 00:19:21,054
<i>- ♪ ...hunk, a hunk of burning love ♪
- ♪ Ooh ♪</i>

294
00:19:21,136 --> 00:19:23,973
♪ <i>I said, a hunk, a hunk of burning love </i>♪

295
00:19:24,057 --> 00:19:26,017
<i>- ♪ Ooh ♪</i>
- ♪ <i>I said, a hunk, a hunk of... </i>♪

296
00:19:26,100 --> 00:19:27,977
- Get into it.
<i>- ♪ Ooh ♪</i>

297
00:19:28,061 --> 00:19:30,521
♪ <i>I said, a hunk, a hunk of burning love </i>♪

298
00:19:30,604 --> 00:19:31,647
♪ <i>Yeah! </i>♪

299
00:19:34,859 --> 00:19:36,652
- Here you go, guys.
- Thank you.

300
00:19:37,570 --> 00:19:39,030
- Hi, kids.
- Hi, Mom.

301
00:19:39,739 --> 00:19:41,615
Look, it's Team D.J.,

302
00:19:41,824 --> 00:19:45,160
the most reliable co-parents
on the planet.

303
00:19:48,497 --> 00:19:51,667
I had a text from Jackson earlier
that had me a little concerned,

304
00:19:51,876 --> 00:19:55,004
but it sounds like everything
went perfectly fine today.

305
00:19:55,212 --> 00:19:56,839
Fabulous.

306
00:19:56,923 --> 00:19:58,549
And flawless.

307
00:20:00,968 --> 00:20:03,888
- So where is Jackson?
- Oh, he's upstairs somewhere.

308
00:20:04,097 --> 00:20:06,140
You know, he has been quiet as a mouse

309
00:20:06,223 --> 00:20:09,060
ever since I solved
your whole bedroom fiasco.

310
00:20:10,978 --> 00:20:15,775
Boy, I wish I had
your amazing parenting skills.

311
00:20:15,983 --> 00:20:17,818
Don't feel bad. I know you do your best.

312
00:20:20,446 --> 00:20:23,240
Hey, Jackson, get in here!

313
00:20:23,449 --> 00:20:24,784
Here I am.

314
00:20:26,285 --> 00:20:29,122
Jackson, tell Team D.J. where you've been.

315
00:20:29,204 --> 00:20:31,999
Um, I was in Uncle Jesse's truck.

316
00:20:32,083 --> 00:20:34,085
I made it all the way to Fresno.

317
00:20:34,168 --> 00:20:36,503
This kid's got the bladder of a champ.

318
00:20:38,547 --> 00:20:43,510
All while Team D.J.
was being fabulous and flawless.

319
00:20:44,303 --> 00:20:46,680
Deej, I am so sorry.

320
00:20:47,347 --> 00:20:48,515
Frothy cocktail?

321
00:20:51,310 --> 00:20:55,439
On the bright side, three out of four
of the kids were right here all day.

322
00:20:58,151 --> 00:21:00,319
Okay, I think we need a family meeting.

323
00:21:01,236 --> 00:21:04,907
I know that look.
I feel sorry for Team D.J.

324
00:21:13,916 --> 00:21:16,961
- I can go home. I'm not in trouble, right?
- No, I am.

325
00:21:17,044 --> 00:21:18,754
It's my first day and I blew it.

326
00:21:18,838 --> 00:21:21,174
Don't be so hard on yourself.
Remember my first day?

327
00:21:21,381 --> 00:21:22,883
I changed Michelle on a meat rack,

328
00:21:22,967 --> 00:21:25,552
and you and D.J. were go-go dancing
on top of amplifiers,

329
00:21:25,636 --> 00:21:27,387
eating sundaes and pizza all night.

330
00:21:28,305 --> 00:21:29,890
You were so clueless.

331
00:21:29,974 --> 00:21:31,100
It was awesome.

332
00:21:32,392 --> 00:21:34,979
Well, I learned and so will you.
I love you, Steph.

333
00:21:35,188 --> 00:21:36,856
- You, too.
- Goodbye, everybody!

334
00:21:37,064 --> 00:21:38,024
Bye!

335
00:21:39,942 --> 00:21:41,694
Uh-oh. Text from Becky...

336
00:21:41,777 --> 00:21:45,489
"Dear Jesse, me and the fat dog
are enjoying my lasagna.

337
00:21:46,115 --> 00:21:48,701
Your dinner is in the doghouse
where you'll be sleeping."

338
00:21:48,909 --> 00:21:51,578
Actually, it's worth it
not to have to eat that lasagna.

339
00:21:54,207 --> 00:21:57,126
Okay, first things first...

340
00:21:57,918 --> 00:22:01,505
I don't want to be a person who wags her
finger while saying "first things first",

341
00:22:01,588 --> 00:22:02,840
so I'll put this thing away.

342
00:22:05,009 --> 00:22:06,886
Jackson, what were you thinking?

343
00:22:07,094 --> 00:22:10,848
Mom, you should know. You ran away
when Stephanie moved into your room.

344
00:22:11,057 --> 00:22:12,641
How would you even know that?

345
00:22:18,189 --> 00:22:19,899
Okay, in my defense,

346
00:22:19,982 --> 00:22:25,071
that was a small part of a much larger
inspirational aunt-nephew teaching moment

347
00:22:25,154 --> 00:22:27,447
gone horribly wrong.

348
00:22:29,283 --> 00:22:33,871
To your point, I ran away to the garage.
You left the county.

349
00:22:35,248 --> 00:22:38,959
Hey, let's not quibble over
whose runaway was better, okay?

350
00:22:43,214 --> 00:22:45,299
I know I shouldn't have left like that.

351
00:22:45,966 --> 00:22:47,509
I just got mad.

352
00:22:47,593 --> 00:22:52,056
I mean, first dad dies,
then we all have to move in here.

353
00:22:52,265 --> 00:22:56,393
Now there's more people moving in here,
and to top it all off, I lose my room.

354
00:22:59,479 --> 00:23:00,898
Jax.

355
00:23:04,068 --> 00:23:06,862
I know this is just tough...

356
00:23:07,613 --> 00:23:10,657
but you have a house
full of people who love you.

357
00:23:12,243 --> 00:23:14,870
And as far as your room goes, I get it.

358
00:23:15,537 --> 00:23:18,540
I thought moving in with this squirt
was going to ruin my life.

359
00:23:20,751 --> 00:23:23,129
But we got really close sharing that room.

360
00:23:24,297 --> 00:23:26,006
And now you're here for me.

361
00:23:28,926 --> 00:23:32,138
You may not see it now, but living
with your brother is a blessing.

362
00:23:33,306 --> 00:23:34,431
Are you sure?

363
00:23:37,143 --> 00:23:38,894
I'm here for you, Bro.

364
00:23:40,854 --> 00:23:44,150
Like the little bird that eats
the parasites off the rhino's back.

365
00:23:47,069 --> 00:23:49,446
Can anyone explain
what this kid's talking about?

366
00:23:53,492 --> 00:23:56,912
It's a classic symbiotic relationship.

367
00:23:59,456 --> 00:24:03,585
Ramona, I know this is hard on you, too,
with your dad and I separated,

368
00:24:03,669 --> 00:24:05,963
but as much as I'm helping D.J.,

369
00:24:06,630 --> 00:24:07,756
D.J.'s helping me.

370
00:24:09,133 --> 00:24:11,051
I'm new at this single-parent thing.

371
00:24:11,844 --> 00:24:14,096
We're all figuring this out.

372
00:24:14,763 --> 00:24:18,058
So everybody has to do their part
to make this new family work.

373
00:24:18,267 --> 00:24:23,272
That means respect, compromise,
and honesty, okay?

374
00:24:23,480 --> 00:24:26,108
I'm in. But I was never out.

375
00:24:27,818 --> 00:24:28,819
Jackson?

376
00:24:29,028 --> 00:24:30,779
Yeah, okay.

377
00:24:31,446 --> 00:24:32,614
Ramona?

378
00:24:33,907 --> 00:24:36,660
I can't believe
I'm going to say this,

379
00:24:36,743 --> 00:24:39,079
but if you're so desperate
to have your room back,

380
00:24:39,163 --> 00:24:40,873
I'll go live in the hobbit hole.

381
00:24:41,081 --> 00:24:43,334
Yes!

382
00:24:43,542 --> 00:24:45,127
That is so sweet of you,

383
00:24:45,336 --> 00:24:48,339
but I think Jackson and Max
are going to love living together.

384
00:24:48,547 --> 00:24:50,132
Oh, thank God!

385
00:24:53,010 --> 00:24:54,094
Hey, Deej,

386
00:24:54,178 --> 00:24:56,805
in the spirit of honesty, um...

387
00:24:56,889 --> 00:25:01,227
I lost my phone in your baby's diaper
and I butt-answered your call.

388
00:25:02,602 --> 00:25:03,437
What?

389
00:25:04,146 --> 00:25:07,066
Oh, like you've never lost a phone
in a diaper before.

390
00:25:08,275 --> 00:25:11,611
- No.
- Once, I lost a watch in some egg salad.

391
00:25:13,739 --> 00:25:17,910
You know what we need?
A group hug! Come on!

392
00:25:21,872 --> 00:25:24,250
Sorry, I forgot my lucky sunglasses.

393
00:25:25,251 --> 00:25:26,919
I knew there would be a hug.

394
00:25:29,296 --> 00:25:31,257
Is it just me or do you hear violins?

395
00:25:37,576 --> 00:25:42,576
Sync and corrections by explosiveskull
www.addic7ed.com

