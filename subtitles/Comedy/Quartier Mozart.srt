﻿1
00:00:08,075 --> 00:00:11,676
- synced and corrected by chamallow -
- www.addic7ed.com -

2
00:00:11,796 --> 00:00:14,530
♪♪ [piano playing]

3
00:00:14,650 --> 00:00:16,259
Fuck her.

4
00:00:16,379 --> 00:00:18,050
Fuck her.

5
00:00:18,650 --> 00:00:20,228
[chuckles]

6
00:00:20,348 --> 00:00:21,649
Oh...

7
00:00:21,650 --> 00:00:23,698
[laughing]

8
00:00:23,729 --> 00:00:25,144
Oh...

9
00:00:26,210 --> 00:00:28,124
I don't understand.

10
00:00:28,482 --> 00:00:29,482
I don't...

11
00:00:30,306 --> 00:00:33,328
But can't I even go out
to play with my friends?

12
00:00:33,448 --> 00:00:34,751
Stay with it.

13
00:00:34,871 --> 00:00:38,007
Go back further.

14
00:00:39,237 --> 00:00:42,559
I don't want to practice anymore!

15
00:00:42,679 --> 00:00:45,409
- Mommy!
- Shh...

16
00:00:46,183 --> 00:00:48,161
- Okay.
- [grunts]

17
00:00:48,281 --> 00:00:49,733
We're going to come back now.

18
00:00:49,782 --> 00:00:51,858
- Slowly.
- [grunts]

19
00:00:51,907 --> 00:00:55,791
Three, two, one.

20
00:00:55,911 --> 00:00:57,818
[gong sounds]

21
00:01:02,867 --> 00:01:05,065
I'm proud of you, Winslow.

22
00:01:05,367 --> 00:01:08,673
Well, we're chipping away at it.

23
00:01:08,713 --> 00:01:10,529
I think we made some progress.

24
00:01:10,936 --> 00:01:13,330
Two tickets to my performance

25
00:01:13,370 --> 00:01:15,381
the opening night of the season.

26
00:01:15,389 --> 00:01:16,521
Thank you.

27
00:01:18,394 --> 00:01:20,674
Thank you very much.

28
00:01:29,468 --> 00:01:30,754
Woman: Winslow Elliott

29
00:01:30,779 --> 00:01:33,816
is the absolute greatest
pianist in the world,

30
00:01:33,824 --> 00:01:35,403
and you want me to fire him?

31
00:01:35,523 --> 00:01:38,082
No. No, no. I never said fire him.

32
00:01:38,123 --> 00:01:41,086
I said, let's postpone his
performance for another time.

33
00:01:41,135 --> 00:01:44,099
- It happens. Happens, happens.
- Bullshit!

34
00:01:44,115 --> 00:01:46,721
I had to beg Winslow Elliott to do this.

35
00:01:46,729 --> 00:01:48,122
No one wants to work with you,

36
00:01:48,122 --> 00:01:49,840
because you keep changing the program.

37
00:01:49,848 --> 00:01:51,403
They all think you're insane,

38
00:01:51,443 --> 00:01:52,827
- and they're right.
- I'm not...

39
00:01:52,860 --> 00:01:55,050
- Ow. I'm not insane.
- You look insane.

40
00:01:55,067 --> 00:01:56,736
Your face looks crazy.

41
00:01:56,856 --> 00:01:58,983
You're gonna scare the audience
right out of their seats.

42
00:01:59,024 --> 00:02:00,384
You're talking about my face?

43
00:02:00,504 --> 00:02:03,096
I'm talking about the truth,
and you're talking about my face?

44
00:02:03,112 --> 00:02:05,669
- My face!
- Posters, ads, billboards!

45
00:02:05,701 --> 00:02:08,583
They all say "Winslow
Elliott plays Rachmaninoff. "

46
00:02:08,608 --> 00:02:09,992
Yes, but it also says Rodrigo.

47
00:02:10,220 --> 00:02:12,671
And Rodrigo, and Rod...
Rodrigo! It's full of Rodrigo!

48
00:02:12,711 --> 00:02:13,802
Rodrigo is what they're gonna get.

49
00:02:13,843 --> 00:02:15,293
Doing what, playing the harmonica?

50
00:02:15,293 --> 00:02:16,979
I don't know. I don't know yet.

51
00:02:16,987 --> 00:02:18,477
- That creep, Ivan, was right.
- [muttering]

52
00:02:18,509 --> 00:02:19,780
I never should have let Thomas go.

53
00:02:19,900 --> 00:02:21,620
Hey, no, Gloria, no, no, no.

54
00:02:21,740 --> 00:02:24,340
Please, please, okay, okay, okay.

55
00:02:24,356 --> 00:02:25,895
Look into my eyes.

56
00:02:27,328 --> 00:02:29,868
I accepted this job,

57
00:02:30,650 --> 00:02:32,954
because I listened to my blood.

58
00:02:33,321 --> 00:02:35,250
I'm gonna find the piece.

59
00:02:36,650 --> 00:02:38,140
Believe in me.

60
00:02:42,073 --> 00:02:44,076
I do believe in you.

61
00:02:47,650 --> 00:02:50,631
Oh, God help me, I'm
going down with the ship.

62
00:02:50,834 --> 00:02:52,520
And, you know, Winslow Elliott,

63
00:02:52,837 --> 00:02:54,702
you know, he's a professional.

64
00:02:54,822 --> 00:02:56,021
He'll understand.

65
00:02:56,436 --> 00:02:59,188
- You don't know Winslow Elliott.
- [knocking on door]

66
00:02:59,308 --> 00:03:00,784
[sighs]

67
00:03:01,650 --> 00:03:03,732
- Come in.
- [door opens]

68
00:03:03,781 --> 00:03:06,818
Ms. Windsor, this came for you.

69
00:03:07,079 --> 00:03:08,935
It's from Thomas's wife.

70
00:03:09,465 --> 00:03:11,704
I guess these are instructions.

71
00:03:11,753 --> 00:03:13,244
Instructions?

72
00:03:25,082 --> 00:03:26,450
You're late.

73
00:03:26,570 --> 00:03:28,624
No. I'm not. It's 6:30. I'm on time.

74
00:03:28,632 --> 00:03:30,855
Early is on time, on time is late,

75
00:03:30,887 --> 00:03:32,963
and late is unacceptable.

76
00:03:33,208 --> 00:03:34,958
- You have the check?
- Yes.

77
00:03:34,983 --> 00:03:36,229
Come in.

78
00:03:36,693 --> 00:03:38,126
Let's get started.

79
00:03:43,069 --> 00:03:45,328
Turn to page 16, please.

80
00:03:55,863 --> 00:03:57,858
Are you embarrassed to play it?

81
00:03:58,367 --> 00:04:00,616
Was it more embarrassing
than dropping your oboe

82
00:04:00,636 --> 00:04:03,028
and shouting motherfucker in
front of the whole orchestra?

83
00:04:03,038 --> 00:04:05,328
No, that was very embarrassing.

84
00:04:05,674 --> 00:04:07,547
Play it.

85
00:04:08,890 --> 00:04:11,650
♪♪

86
00:04:15,924 --> 00:04:17,650
[dialing]

87
00:04:18,682 --> 00:04:19,831
Hi.

88
00:04:19,842 --> 00:04:21,979
Yeah, it's me. Yes.

89
00:04:22,099 --> 00:04:23,862
Yes, of course, I'm sure.

90
00:04:23,862 --> 00:04:26,417
I hate New Jersey for
that exact reason. Hey!

91
00:04:26,537 --> 00:04:28,931
Stop articulating with
the back of your throat.

92
00:04:29,051 --> 00:04:31,699
- Yes, a new student. God help me.
- ** [stops]

93
00:04:31,720 --> 00:04:33,735
- ** [resumes]
- I know.

94
00:04:33,855 --> 00:04:35,058
Norman...

95
00:04:35,964 --> 00:04:38,649
♪♪ [continues]

96
00:04:38,650 --> 00:04:40,045
Get your tongue off

97
00:04:40,045 --> 00:04:42,335
the roof of your mouth, for Christ's sake.

98
00:04:42,455 --> 00:04:44,371
Yeah, you should see this girl's reeds.

99
00:04:44,391 --> 00:04:46,600
They're atrocious. [laughs]

100
00:04:46,720 --> 00:04:48,646
I know! Exactly!

101
00:04:55,375 --> 00:04:57,604
"Dear Gloria and Cynthia,

102
00:04:57,724 --> 00:05:01,095
thank you so much for
reading this humble letter.

103
00:05:01,215 --> 00:05:04,281
The contents are of
great importance to us."

104
00:05:04,912 --> 00:05:06,775
Keep going.

105
00:05:08,281 --> 00:05:11,528
"Cynthia, you cheap, two-bit,
home-wrecking slut... "

106
00:05:11,843 --> 00:05:14,530
- These are her words, not my words.
- Understood.

107
00:05:14,825 --> 00:05:17,348
"Your only redeeming quality

108
00:05:17,359 --> 00:05:20,656
is that you have satisfied the animal urges

109
00:05:20,880 --> 00:05:23,730
which Thomas daily tried to impose upon me.

110
00:05:23,850 --> 00:05:25,664
And Gloria,

111
00:05:25,784 --> 00:05:28,056
you deceitful,

112
00:05:28,066 --> 00:05:30,997
power-hungry bitch...

113
00:05:32,544 --> 00:05:34,610
You are perhaps worse.

114
00:05:35,862 --> 00:05:39,699
You betrayed the man who
in each in our own way

115
00:05:40,147 --> 00:05:43,047
we care deeply for. "

116
00:05:45,582 --> 00:05:47,913
"Thomas is in Cuba.

117
00:05:49,226 --> 00:05:52,045
He ran out of his medication two weeks ago.

118
00:05:52,076 --> 00:05:55,607
Yesterday, he telegrammed
demanding a divorce.

119
00:05:56,106 --> 00:05:59,169
I believe that he is having
a full-blown breakdown

120
00:05:59,200 --> 00:06:02,558
and must be brought home
to receive medical care.

121
00:06:02,823 --> 00:06:05,591
I am unable to do this for obvious reasons.

122
00:06:05,602 --> 00:06:07,942
Perhaps, you, Cynthia,

123
00:06:08,675 --> 00:06:13,815
with your courtesan ways,
could prevail upon him.

124
00:06:14,416 --> 00:06:17,693
Sincerely, Claire Pembridge."

125
00:06:19,077 --> 00:06:20,736
Shit.

126
00:06:20,856 --> 00:06:22,711
Shit, indeed.

127
00:06:23,667 --> 00:06:27,077
Male Voice: No excuses! Keep going!

128
00:06:27,650 --> 00:06:28,673
Male Voice: Faster!

129
00:06:28,793 --> 00:06:30,269
- Oh, fuck you!
- Harder!

130
00:06:30,389 --> 00:06:32,142
- Fuck you.
- Keep going!

131
00:06:32,158 --> 00:06:34,414
- At it again.
- What is all that?

132
00:06:34,439 --> 00:06:36,792
Bunch of get-well stuff for Rodrigo.

133
00:06:36,833 --> 00:06:37,866
I thought it was from Alex.

134
00:06:37,891 --> 00:06:39,470
Nope. We're not talking about that.

135
00:06:39,590 --> 00:06:40,903
Male Voice: No excuses!

136
00:06:40,911 --> 00:06:43,069
Whoa, this one's actually for me.

137
00:06:43,189 --> 00:06:45,781
- Plank in three, two, one!
- Oh, fuck.

138
00:06:45,805 --> 00:06:47,296
How much longer is this?

139
00:06:47,336 --> 00:06:48,949
You mean your five-minute workout?

140
00:06:48,997 --> 00:06:50,138
- Yes.
- Stay strong!

141
00:06:50,178 --> 00:06:53,386
"Teach me, show me, promise
not to waste your time.

142
00:06:53,419 --> 00:06:54,551
Call me. Marlon. "

143
00:06:54,575 --> 00:06:57,987
I'm confused. Does he
mean "call me Marlon, "

144
00:06:58,011 --> 00:07:00,266
or "call me, Marlon"?

145
00:07:00,282 --> 00:07:02,383
- Holy shit.
- What?

146
00:07:02,503 --> 00:07:05,152
It's a check for $1,000
for two oboe lessons.

147
00:07:05,152 --> 00:07:06,887
- Lunges!
- I can't accept this.

148
00:07:06,935 --> 00:07:08,710
- Go!
- Why not?

149
00:07:08,742 --> 00:07:11,723
- Teach him, show him.
- Yes!

150
00:07:11,771 --> 00:07:13,123
Call him Marlon.

151
00:07:13,243 --> 00:07:14,995
- Let me see that.
- You've got this!

152
00:07:15,044 --> 00:07:16,876
You're right, you're right.

153
00:07:16,917 --> 00:07:19,465
I should teach him and show
him and call him Marlon.

154
00:07:19,585 --> 00:07:22,373
- Let's cash this shit today.
- You are a warrior!

155
00:07:22,493 --> 00:07:24,074
Go out and own your day!

156
00:07:24,123 --> 00:07:26,452
Hi, may I speak with Mr. Guggenheim?

157
00:07:54,119 --> 00:07:57,050
[footsteps]

158
00:08:02,676 --> 00:08:06,153
[footsteps]

159
00:08:21,459 --> 00:08:24,462
- You know what that means?
- A duel?

160
00:08:24,492 --> 00:08:26,223
No, that means a slap in the face,

161
00:08:26,223 --> 00:08:28,523
just like what you did to me.

162
00:08:29,232 --> 00:08:31,342
Maestro, do you know what it's like

163
00:08:31,352 --> 00:08:33,612
to feel the kind of humiliation

164
00:08:33,632 --> 00:08:35,607
that makes you wanna run away from home

165
00:08:35,607 --> 00:08:37,979
and go hide in a neighbor's closet

166
00:08:38,009 --> 00:08:40,971
and bury your face in the warmth

167
00:08:40,992 --> 00:08:43,791
and the darkness of the sweaters

168
00:08:43,801 --> 00:08:47,943
and the coats and the dresses on hangers?

169
00:08:48,767 --> 00:08:51,383
Yes, but no.

170
00:08:51,393 --> 00:08:56,024
Mr. Elliott, I idolize you.
I grew up with your music.

171
00:08:56,034 --> 00:08:57,938
Your Rachmaninoff No. 3

172
00:08:58,058 --> 00:09:00,704
is immense. I can only apologize...

173
00:09:00,714 --> 00:09:02,455
I don't want your apology.

174
00:09:02,575 --> 00:09:05,477
I want an explanation, a reason.

175
00:09:05,477 --> 00:09:07,564
Why was I dismissed like this?

176
00:09:07,564 --> 00:09:10,893
Why have you disgraced me so violently?

177
00:09:11,219 --> 00:09:14,669
And what am I going to tell my mother?

178
00:09:15,856 --> 00:09:21,223
She was really looking
forward to coming here.

179
00:09:21,529 --> 00:09:26,332
And you of all people should
understand what goes into this.

180
00:09:26,452 --> 00:09:28,072
The preparation,

181
00:09:28,192 --> 00:09:29,893
the... the endurance,

182
00:09:30,013 --> 00:09:31,827
iron weights.

183
00:09:31,947 --> 00:09:35,013
[vocalizing]

184
00:09:35,594 --> 00:09:37,833
Now give me that reason.

185
00:09:38,077 --> 00:09:40,489
I only follow my intuition.

186
00:09:41,558 --> 00:09:43,349
I listen to my heart.

187
00:09:43,469 --> 00:09:44,815
- Your heart?
- Yes.

188
00:09:44,845 --> 00:09:47,278
You don't have the
courage to know your heart.

189
00:09:47,309 --> 00:09:49,752
You change your mind at every turn.

190
00:09:49,752 --> 00:09:52,509
And each day, you drag more and more people

191
00:09:52,540 --> 00:09:55,868
into the vortex of your own confusion.

192
00:10:01,924 --> 00:10:05,618
I think you need to examine yourself.

193
00:10:05,842 --> 00:10:07,654
Know thyself!

194
00:10:07,774 --> 00:10:10,911
To thine own self be true.

195
00:10:11,031 --> 00:10:13,696
I can see it so clearly now.

196
00:10:13,699 --> 00:10:18,331
You need the truth even more than I do.

197
00:10:18,901 --> 00:10:21,232
But I'm a little bit grown up.

198
00:10:21,730 --> 00:10:23,471
Am I too late?

199
00:10:23,695 --> 00:10:26,809
You're never too late.

200
00:10:28,173 --> 00:10:33,170
We have nothing but time.

201
00:10:33,290 --> 00:10:34,931
What are you doing?

202
00:10:35,409 --> 00:10:40,559
We're going back into
your subconscious. That is,

203
00:10:40,679 --> 00:10:42,870
if you'll permit me.

204
00:10:42,990 --> 00:10:45,037
Yeah, yeah, yeah.

205
00:10:45,933 --> 00:10:48,111
Yes. Take me with you...

206
00:10:48,231 --> 00:10:50,085
into myself.

207
00:10:50,340 --> 00:10:52,192
You're safe now.

208
00:10:52,312 --> 00:10:54,075
You're going to go back.

209
00:10:54,421 --> 00:10:56,456
Slowly.

210
00:10:58,634 --> 00:11:00,856
I hear music.

211
00:11:04,701 --> 00:11:08,856
♪♪ [Cuban music]

212
00:11:10,767 --> 00:11:13,856
[kids shouting]

213
00:11:16,436 --> 00:11:19,632
[kids laughing, shouting]

214
00:11:25,138 --> 00:11:27,489
_

215
00:11:27,609 --> 00:11:30,013
_

216
00:11:30,041 --> 00:11:32,544
_

217
00:11:34,661 --> 00:11:36,167
_

218
00:11:37,673 --> 00:11:39,373
_

219
00:11:39,393 --> 00:11:42,671
_

220
00:11:42,791 --> 00:11:44,626
_

221
00:11:44,746 --> 00:11:46,346
_

222
00:11:46,466 --> 00:11:48,910
_

223
00:11:54,478 --> 00:11:57,328
[both speak Spanish]

224
00:11:57,448 --> 00:12:00,473
_

225
00:12:00,677 --> 00:12:03,283
- Pembridge.
- White hair.

226
00:12:03,618 --> 00:12:05,855
[speaks Spanish]

227
00:12:06,478 --> 00:12:09,135
Is he staying here? Thomas Pembridge.

228
00:12:09,255 --> 00:12:12,239
[speaks Spanish]

229
00:12:12,359 --> 00:12:14,611
- Big blue eye?
- Sí, sí.

230
00:12:14,632 --> 00:12:17,878
_

231
00:12:17,998 --> 00:12:19,914
The fisherman beach.

232
00:12:20,034 --> 00:12:21,858
Getting crazy.

233
00:12:21,978 --> 00:12:24,432
Fisherman's Beach. Okay.

234
00:12:24,799 --> 00:12:26,427
Gracias.

235
00:12:26,547 --> 00:12:28,452
Here you go.

236
00:12:30,254 --> 00:12:32,208
Take care.

237
00:12:35,516 --> 00:12:38,579
Well, I promise to be
a good student, teacher.

238
00:12:38,699 --> 00:12:41,724
This is the one that I got. Is it any good?

239
00:12:41,844 --> 00:12:44,218
A Lorée Royal 125?

240
00:12:44,238 --> 00:12:46,508
Are you kidding me? This is my dream axe.

241
00:12:46,508 --> 00:12:48,624
- Oh, good.
- You even went for the gold keys.

242
00:12:48,635 --> 00:12:51,444
Sure, 'cause it's gold.
It's the best, I guess.

243
00:12:51,564 --> 00:12:53,174
Congratulations.

244
00:12:53,294 --> 00:12:56,522
You have decided to learn a
member of the double-reed family.

245
00:12:56,756 --> 00:12:59,698
The oboe first appeared
in the mid 17th century

246
00:12:59,698 --> 00:13:02,355
and was originally called the hautbois.

247
00:13:02,385 --> 00:13:03,850
- [chuckles]
- Your accent is

248
00:13:03,871 --> 00:13:05,896
- parfait.
- Merci.

249
00:13:06,016 --> 00:13:08,981
So you want to ease it in.
You never wanna force it

250
00:13:09,011 --> 00:13:11,505
or go too fast. Okay.

251
00:13:11,729 --> 00:13:13,713
The most important thing about the oboe

252
00:13:13,734 --> 00:13:16,838
that makes it so magical is the reed.

253
00:13:16,848 --> 00:13:18,731
This will make or break you.

254
00:13:18,741 --> 00:13:20,105
It's just a little piece of wood.

255
00:13:20,125 --> 00:13:21,967
Oh, it's so much more than that.

256
00:13:21,998 --> 00:13:23,443
Try it.

257
00:13:26,456 --> 00:13:29,214
- [blows tone]
- You're a natural.

258
00:13:29,334 --> 00:13:30,761
How come you're smiling?

259
00:13:30,782 --> 00:13:32,868
I love how much you know about the oboe.

260
00:13:33,173 --> 00:13:34,364
It's amazing.

261
00:13:34,484 --> 00:13:36,838
I'm putty in your hands.

262
00:13:42,100 --> 00:13:44,856
[labored breathing]

263
00:13:44,976 --> 00:13:47,453
[footsteps echoing]

264
00:13:54,538 --> 00:13:56,855
[violin playing distantly]

265
00:13:56,856 --> 00:13:59,855
[stopwatch ticking]

266
00:13:59,856 --> 00:14:02,855
[metronome clicking]

267
00:14:02,856 --> 00:14:06,579
[faintly] I only follow my
intuition. I listen to my heart.

268
00:14:08,930 --> 00:14:11,321
Man whispers: Sibelius.

269
00:14:13,856 --> 00:14:15,856
[laughing]

270
00:14:17,184 --> 00:14:19,856
[violin playing]

271
00:14:21,275 --> 00:14:25,326
[woman moaning]

272
00:14:28,308 --> 00:14:30,856
[woman laughing]

273
00:14:31,331 --> 00:14:32,329
[heart beating]

274
00:14:32,349 --> 00:14:34,181
No.

275
00:14:34,211 --> 00:14:36,451
Anna Maria.

276
00:14:38,832 --> 00:14:41,326
[tape reversing]

277
00:14:44,664 --> 00:14:45,540
Whoa.

278
00:14:45,570 --> 00:14:47,148
- What happened?
- [gasping]

279
00:14:48,257 --> 00:14:50,660
You're not supposed to
come back by yourself.

280
00:14:50,660 --> 00:14:53,062
- I'm supposed to bring you back.
- Well...

281
00:14:53,072 --> 00:14:54,506
Well, what happened?

282
00:14:54,516 --> 00:14:57,529
You folded inside yourself.

283
00:14:57,539 --> 00:15:00,226
You said a lot of powerful things.

284
00:15:00,257 --> 00:15:05,509
They were mostly in
Spanish, but I got the gist.

285
00:15:05,530 --> 00:15:08,420
You kept mentioning a name... a woman.

286
00:15:08,540 --> 00:15:09,927
Yes.

287
00:15:10,212 --> 00:15:13,164
She was your mother?

288
00:15:13,284 --> 00:15:14,497
No.

289
00:15:14,752 --> 00:15:15,526
No?

290
00:15:15,646 --> 00:15:18,253
Are you sure? It usually is.

291
00:15:18,762 --> 00:15:20,615
Try to remember.

292
00:15:20,818 --> 00:15:23,587
It's such an important detail.

293
00:15:26,060 --> 00:15:28,187
Anna Maria.

294
00:15:30,020 --> 00:15:34,050
Anna Maria has to play the
Sibelius violin concerto.

295
00:15:34,447 --> 00:15:38,021
- Sibelius violin concerto?
- Yes.

296
00:15:38,632 --> 00:15:41,855
Yes, I heard it clearly through an

297
00:15:41,856 --> 00:15:43,924
old loudspeaker.

298
00:15:44,291 --> 00:15:46,031
Oh, Mr. Winslow Elliott,

299
00:15:46,151 --> 00:15:48,994
you've played me like a symphonic poem.

300
00:15:49,910 --> 00:15:54,032
I'm forever in your debt
once again, once again.

301
00:15:54,032 --> 00:15:56,190
- Thank you.
- Oh!

302
00:15:58,959 --> 00:16:02,583
I think we've made some progress.

303
00:16:03,723 --> 00:16:07,651
- [blows tone]
- Good, good.

304
00:16:07,905 --> 00:16:10,856
[continues]

305
00:16:11,173 --> 00:16:13,473
- Really good.
- I'm terrible.

306
00:16:13,493 --> 00:16:15,325
- I'm sorry.
- No, you're not.

307
00:16:15,346 --> 00:16:18,033
There's really nothing to be
embarrassed about. You know,

308
00:16:18,063 --> 00:16:19,773
nobody is good their first time.

309
00:16:19,893 --> 00:16:22,165
You just have to remember to loosen up.

310
00:16:22,186 --> 00:16:23,427
And focus on your breathing.

311
00:16:23,427 --> 00:16:26,134
Make sure your breathing is
coming from, like, right here.

312
00:16:26,254 --> 00:16:29,279
[deep breath]

313
00:16:30,083 --> 00:16:32,035
- You're very nurturing.
- [chuckles]

314
00:16:32,155 --> 00:16:34,641
I think this is a bad idea, though.

315
00:16:34,641 --> 00:16:36,249
Don't be ridiculous.

316
00:16:36,269 --> 00:16:37,499
You are inventing yourself,

317
00:16:37,500 --> 00:16:40,227
and I am here to guide
you through that process.

318
00:16:40,347 --> 00:16:42,047
Just, um, soak your reed,

319
00:16:42,049 --> 00:16:44,319
and make sure it's wet.

320
00:16:44,686 --> 00:16:46,294
I'm not sure it's wet enough.

321
00:16:46,599 --> 00:16:48,065
And then what?

322
00:16:48,370 --> 00:16:51,535
And then get ready to
make some beautiful music.

323
00:16:54,924 --> 00:16:56,960
- I am ready.
- [chuckles]

324
00:16:57,080 --> 00:16:59,117
- Good.
- I'm gonna use the bathroom first.

325
00:16:59,402 --> 00:17:01,000
- Yeah, of course.
- Great. Where is it?

326
00:17:01,120 --> 00:17:02,395
It's just back thereto the left.

327
00:17:02,405 --> 00:17:04,380
- Okay.
- Yeah.

328
00:17:06,527 --> 00:17:07,382
This way?

329
00:17:07,392 --> 00:17:09,387
- Mm-hmm.
- Okay.

330
00:17:11,708 --> 00:17:15,779
[gargling]

331
00:17:15,899 --> 00:17:17,112
[spits]

332
00:17:17,232 --> 00:17:19,402
[shower running]

333
00:17:19,522 --> 00:17:22,099
[Marlon scatting]

334
00:17:32,145 --> 00:17:34,760
- Are the cops here?
- The cops? What are you talking about?

335
00:17:34,760 --> 00:17:36,837
Where is he? What the fuck is going on?

336
00:17:36,873 --> 00:17:37,915
Honestly, I have no idea.

337
00:17:37,928 --> 00:17:38,834
This his coat?

338
00:17:38,873 --> 00:17:40,169
Okay, whoa, whoa.

339
00:17:40,208 --> 00:17:42,482
Will you please slow down
and explain what is going on.

340
00:17:42,521 --> 00:17:44,429
Two phones. He's got two phones.

341
00:17:44,462 --> 00:17:47,732
That check? Good luck cashing
it, because it's counterfeit.

342
00:17:47,852 --> 00:17:50,474
I spoke with my mom. There are
no Guggenheims in Connecticut.

343
00:17:50,487 --> 00:17:51,198
What?

344
00:17:51,211 --> 00:17:54,246
Okay, you cannot just
go through Marlon's shit.

345
00:17:54,259 --> 00:17:56,011
Yes, I can, because it's not Marlon's shit.

346
00:17:56,131 --> 00:17:59,379
Nor is it Xavier Roseau's shit.

347
00:17:59,424 --> 00:18:02,531
Nor is it Marlon Suleiman's shit.

348
00:18:02,812 --> 00:18:04,967
- What are you saying?
- What I'm saying is that

349
00:18:04,987 --> 00:18:06,915
there is an imposter in our bathroom

350
00:18:06,948 --> 00:18:09,156
taking a shower for some reason.

351
00:18:11,208 --> 00:18:13,938
Marlon: Ah, Hailey, your
water pressure's amazing!

352
00:18:13,964 --> 00:18:15,227
Motherfucker.

353
00:18:17,455 --> 00:18:19,917
- You better have some...
- Oh, my God!

354
00:18:19,976 --> 00:18:21,884
[stammering]

355
00:18:23,187 --> 00:18:25,995
I clearly misread the signals.

356
00:18:26,115 --> 00:18:27,858
What the fuck?

357
00:18:27,904 --> 00:18:29,845
- Oh, dear.
- Yeah, the gig is up.

358
00:18:29,845 --> 00:18:31,662
- We know who you aren't.
- Okay.

359
00:18:31,682 --> 00:18:35,394
Well, whatever you think
you know, I can explain it.

360
00:18:35,420 --> 00:18:37,349
Hailey, if you would ask your friend

361
00:18:37,368 --> 00:18:39,310
- with the broom to leave, we could just...
- No!

362
00:18:39,362 --> 00:18:41,792
She stays! You, Marlon,

363
00:18:41,818 --> 00:18:44,327
or whatever the fuck your name is, you go!

364
00:18:45,304 --> 00:18:46,210
Okay.

365
00:18:46,496 --> 00:18:48,920
Take your time. The cops will
be here in about four minutes.

366
00:18:48,939 --> 00:18:52,020
You know what? You have no manners.

367
00:18:52,059 --> 00:18:53,701
Yeah, calm down. Shh.

368
00:18:53,734 --> 00:18:55,864
All right? Don't do
anything you'd be ashamed of.

369
00:18:55,864 --> 00:18:57,688
I'm not the one hiding
my penis under an oboe.

370
00:18:57,877 --> 00:18:59,967
I needed to leave something
to the imagination.

371
00:18:59,980 --> 00:19:01,228
- Go!
- Shh!

372
00:19:01,348 --> 00:19:03,263
You're just embarrassing yourself now.

373
00:19:03,383 --> 00:19:05,426
I have to say good night to my hostess.

374
00:19:05,817 --> 00:19:08,911
Hailey, I know you can't believe

375
00:19:09,031 --> 00:19:11,960
anything I could say right now, so...

376
00:19:12,012 --> 00:19:13,764
Did you really wanna learn the oboe?

377
00:19:13,953 --> 00:19:15,191
Or was this all just about...

378
00:19:15,243 --> 00:19:17,177
No, I did. I wanted to learn the oboe,

379
00:19:17,297 --> 00:19:19,289
but I learned this thing's insane,

380
00:19:19,409 --> 00:19:20,793
and no one can play it.

381
00:19:21,933 --> 00:19:22,715
Well,

382
00:19:22,943 --> 00:19:24,051
you can.

383
00:19:24,454 --> 00:19:25,575
Take it.

384
00:19:25,770 --> 00:19:26,826
For the mother ship.

385
00:19:27,080 --> 00:19:28,220
For real?

386
00:19:28,904 --> 00:19:30,330
Gold keys.

387
00:19:34,544 --> 00:19:36,721
Yeah, that's probably a good idea.

388
00:19:36,747 --> 00:19:37,867
Yep. Thanks.

389
00:19:38,245 --> 00:19:39,353
Thank you.

390
00:19:44,928 --> 00:19:47,019
♪♪ [Latin music]

391
00:19:47,058 --> 00:19:49,976
[men singing in Spanish]

392
00:20:00,040 --> 00:20:03,179
Man: Whoo-hoo! Yeah! Woo-hoo-hoo!

393
00:20:11,856 --> 00:20:15,334
[shouts in Spanish]

394
00:20:29,692 --> 00:20:31,086
Yeah!

395
00:20:34,291 --> 00:20:35,737
Thanks.

396
00:20:38,518 --> 00:20:40,694
Cynthia, darling,

397
00:20:41,534 --> 00:20:43,860
what the fuck are you doing here?

398
00:20:43,980 --> 00:20:45,358
All right, don't answer that.

399
00:20:45,478 --> 00:20:47,396
Hey, guys! Hey, guys,

400
00:20:47,516 --> 00:20:48,959
this is Cynthia. I was telling you,

401
00:20:48,972 --> 00:20:51,741
this is the greatest
cellist in New York City.

402
00:20:51,760 --> 00:20:54,515
She could have had a major solo career,

403
00:20:54,534 --> 00:20:56,515
but she decided just to walk away.

404
00:20:57,055 --> 00:20:58,606
But it's not too late.

405
00:20:58,726 --> 00:21:00,668
It's never too late.

406
00:21:00,788 --> 00:21:02,258
- Thomas, I'd like to speak with you
- [whistles]

407
00:21:02,260 --> 00:21:04,304
- in private.
- Yo! Yo! Huh?

408
00:21:04,305 --> 00:21:06,962
- In private.
- You must be exhausted.

409
00:21:07,082 --> 00:21:09,027
- Thomas?
- Yeah. Thank you.

410
00:21:09,275 --> 00:21:10,890
Gracias. Gracias.

411
00:21:11,305 --> 00:21:13,304
First we drinkie, then we talkie, huh?

412
00:21:13,868 --> 00:21:14,858
Listen,

413
00:21:15,313 --> 00:21:18,245
I detoxed Manhattan out of my system.

414
00:21:18,428 --> 00:21:20,630
No stress, ambition, noise.

415
00:21:20,750 --> 00:21:23,021
No meaningless bullshit.

416
00:21:23,217 --> 00:21:24,031
Gone!

417
00:21:24,064 --> 00:21:25,281
You know my secret?

418
00:21:25,346 --> 00:21:26,871
Coconut water. Here.

419
00:21:26,871 --> 00:21:28,643
Just drink it quickly.

420
00:21:28,689 --> 00:21:30,981
That way the vitamins won't evaporate.

421
00:21:31,101 --> 00:21:32,819
- I'm not thirsty.
- Take it in one...

422
00:21:32,851 --> 00:21:34,564
- Can we go someplace else?
- Darling,

423
00:21:34,601 --> 00:21:36,108
these are my boys.

424
00:21:36,228 --> 00:21:39,314
- There is no someplace else.
- Okay.

425
00:21:40,108 --> 00:21:42,213
- Oh!
- You're a fucking asshole.

426
00:21:42,333 --> 00:21:44,076
You disappear. You don't call.

427
00:21:44,100 --> 00:21:45,132
I don't know what the
hell's happened to you.

428
00:21:45,134 --> 00:21:48,041
I come down here, and you're
playing the fucking bongos?

429
00:21:48,269 --> 00:21:51,233
[chuckles] You're a fucking spitfire.

430
00:21:51,249 --> 00:21:52,862
I fly home tonight.

431
00:21:53,098 --> 00:21:55,027
You're a selfish prick.

432
00:21:55,272 --> 00:21:59,091
Okay, guys, mama's just a
little bit angry with papa.

433
00:21:59,211 --> 00:22:01,282
So, uh... Hey, um...

434
00:22:01,402 --> 00:22:04,761
[clears throat] Two coconuts to go.

435
00:22:05,576 --> 00:22:08,576
Hang on there! Cynth!

436
00:22:22,084 --> 00:22:23,723
Wow.

437
00:22:23,843 --> 00:22:25,717
You finished it.

438
00:22:26,074 --> 00:22:30,124
Tommy's been doing a little more
than just beating on the bongos, baby.

439
00:22:30,244 --> 00:22:31,692
After all these years.

440
00:22:31,702 --> 00:22:33,197
You know, Cynth, I can't believe it.

441
00:22:33,208 --> 00:22:35,518
Look, it just poured out of me.

442
00:22:35,518 --> 00:22:37,980
I mean, you know it took me
10 years to write the overture.

443
00:22:38,001 --> 00:22:41,930
Well, I came down here, and
boom, I finished it in a week.

444
00:22:42,050 --> 00:22:44,963
It was like a higher
power was guiding my hand

445
00:22:45,083 --> 00:22:46,551
over the whole thing.

446
00:22:46,671 --> 00:22:48,495
I don't know, Cynth,

447
00:22:48,709 --> 00:22:51,864
it's just this place. I just love it here.

448
00:22:53,116 --> 00:22:55,417
This is great. It is.

449
00:22:55,537 --> 00:22:57,747
But you know what goes up

450
00:22:58,124 --> 00:23:00,719
- must come down.
- Yes!

451
00:23:00,839 --> 00:23:02,809
I thought we'd put that to the test

452
00:23:02,835 --> 00:23:03,988
before your flight.

453
00:23:04,020 --> 00:23:05,206
[chuckles]

454
00:23:05,233 --> 00:23:07,297
Oh, come on! It's a joke!

455
00:23:07,310 --> 00:23:09,949
- [Man shouting in Spanish]
- Oh, shut up!

456
00:23:09,994 --> 00:23:13,264
I had to put up with your
prostitutas for the last two weeks!

457
00:23:13,329 --> 00:23:15,277
- Come here.
- That asshole.

458
00:23:16,391 --> 00:23:18,059
Look. Look.

459
00:23:18,756 --> 00:23:20,729
I came here to bring you back.

460
00:23:21,179 --> 00:23:22,403
But here's the thing.

461
00:23:23,445 --> 00:23:25,706
I'm not even sure who you are right now.

462
00:23:29,875 --> 00:23:31,314
Claire gave me these for you.

463
00:23:31,434 --> 00:23:33,613
Claire? Oh.

464
00:23:33,733 --> 00:23:36,422
She means well, but this is shite.

465
00:23:36,542 --> 00:23:37,777
Poison.

466
00:23:39,659 --> 00:23:42,193
Just like the New York Symphony.

467
00:23:42,551 --> 00:23:44,401
You gotta get out of there, Cynth,

468
00:23:44,642 --> 00:23:46,212
as soon as you can.

469
00:23:48,823 --> 00:23:50,752
Why don't you stay here with me?

470
00:23:51,957 --> 00:23:53,286
I would,

471
00:23:53,598 --> 00:23:56,302
but I'm allergic to coconuts.

472
00:23:58,477 --> 00:23:59,884
Come home soon.

473
00:24:07,089 --> 00:24:09,059
I don't care how many anti-bacterial

474
00:24:09,101 --> 00:24:11,003
micro-fungal pro sanitizers you use,

475
00:24:11,019 --> 00:24:12,858
you couldn't pay me to play
that thing with my mouth.

476
00:24:12,879 --> 00:24:14,853
Yeah, well, lucky for you,
you don't know how to play.

477
00:24:14,911 --> 00:24:15,875
Dry cloth?

478
00:24:16,703 --> 00:24:18,745
Do you not feel, like,
slightly odd about keeping it?

479
00:24:18,782 --> 00:24:20,324
No, Marlon misused her,

480
00:24:20,366 --> 00:24:22,982
and it is my duty as a
musician to look after her.

481
00:24:23,243 --> 00:24:24,181
Wet wipes.

482
00:24:25,104 --> 00:24:27,079
- Her?
- It's a girl.

483
00:24:27,350 --> 00:24:29,112
Just have to find a name for her.

484
00:24:30,909 --> 00:24:32,593
I was thinking Kimberly.

485
00:24:33,499 --> 00:24:35,576
♪♪

486
00:24:49,106 --> 00:24:50,435
Do you like the Mozart?

487
00:24:50,555 --> 00:24:52,197
Oh, I kind of prefer the Bach.

488
00:24:52,447 --> 00:24:53,318
Yeah?

489
00:24:53,959 --> 00:24:55,063
I find it too salty.

490
00:24:55,084 --> 00:24:57,851
Well, the Bach has the bacon which I like,

491
00:24:57,877 --> 00:25:01,394
but, of course, you can get
the Mozart with the bacon.

492
00:25:01,899 --> 00:25:03,822
That's a Salieri.

493
00:25:05,796 --> 00:25:07,787
- I have to thank you.
- Well,

494
00:25:08,089 --> 00:25:09,735
shall we shake hands on it?

495
00:25:09,975 --> 00:25:12,143
Are you... are you sure?

496
00:25:12,367 --> 00:25:15,124
'Cause I heard that you don't
like doing that kind of thing.

497
00:25:15,181 --> 00:25:19,298
Oh, please. I don't know
how these rumors get started.

498
00:25:22,852 --> 00:25:26,406
We are gonna play together one day.

499
00:25:44,822 --> 00:25:48,022
♪♪ [Latin guitar]

500
00:26:05,052 --> 00:26:08,856
[singing in Spanish]

501
00:26:30,472 --> 00:26:34,250
- synced and corrected by chamallow -
- www.addic7ed.com -

