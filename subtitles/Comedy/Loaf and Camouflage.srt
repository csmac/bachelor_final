1
00:00:25,000 --> 00:00:29,250
Oh, it's you. Eh?
No!

2
00:01:11,520 --> 00:01:13,750
Lovely cheese.

3
00:01:16,980 --> 00:01:19,180
I could just wash it...

4
00:01:21,020 --> 00:01:23,730
I was just coming, lad.

5
00:01:30,280 --> 00:01:32,670
Pop away, Gromit

6
00:01:34,300 --> 00:01:36,400
With you in a jiffy!

7
00:01:37,600 --> 00:01:39,270
Hi de ho.

8
00:01:41,950 --> 00:01:44,150
Oh, no.

9
00:01:44,730 --> 00:01:46,420
Excellent.

10
00:01:56,950 --> 00:01:58,980
Fill her up, lad.

11
00:02:05,310 --> 00:02:11,270
Lovely cuppa, Gromit,
but a slightly dieselly aftertaste perhaps.

12
00:02:12,040 --> 00:02:13,700
How's that breakfast coming up?

13
00:02:15,160 --> 00:02:18,940
Well done, lad. Very well done.

14
00:02:20,680 --> 00:02:22,270
Thanks, dog.

15
00:02:24,610 --> 00:02:28,940
Oh. Oh, dear. Another baker.

16
00:02:29,100 --> 00:02:33,670
Battered with his own rolling pin.
Would you credit it?

17
00:02:34,610 --> 00:02:40,660
Still looking on the bright side,
I suppose it means more business for us,
eh Gromit?

18
00:02:44,390 --> 00:02:45,870
We're on a roll, lad.

19
00:02:51,960 --> 00:02:55,160
Good days work, lad.
We're bang on...

20
00:02:57,150 --> 00:02:58,830
...target.

21
00:03:02,550 --> 00:03:07,270
<i># I'm light as a feather.
I'm the Bake-O-Lite girl. #</i>

22
00:03:09,980 --> 00:03:12,780
Gromit, did you see
who that was?

23
00:03:13,530 --> 00:03:15,140
She's in trouble.

24
00:03:22,960 --> 00:03:25,930
Here, Gromit. Take the wheel.

25
00:03:35,690 --> 00:03:37,290
Don't fret, madam.

26
00:03:37,290 --> 00:03:41,730
Tea cakes, lad.
The whole meal fruit on me knees.

27
00:03:54,100 --> 00:03:56,800
I should have tried
the granary rolls.

28
00:03:59,100 --> 00:04:00,300
Oh, egg.

29
00:04:14,420 --> 00:04:18,950
Oh, dear. Are you all right, miss?
Madam?

30
00:04:19,260 --> 00:04:21,120
Oh, I do apologise.

31
00:04:21,870 --> 00:04:23,380
It's an honor to be of help.

32
00:04:23,540 --> 00:04:26,350
I must get those brake seen to.

33
00:04:26,650 --> 00:04:28,350
We're so grateful,
aren't we, Fluffles?

34
00:04:29,990 --> 00:04:33,000
- Fluffles.
- Oh, it was nothing.

35
00:04:33,480 --> 00:04:36,110
What a lovely little doggie.

36
00:04:37,200 --> 00:04:39,570
My name's Piella. Piella Bakewell.

37
00:04:39,970 --> 00:04:41,780
I know who you are, miss.

38
00:04:42,070 --> 00:04:45,750
# Light as a feather,
you're the Bake-O-Lite girl. #

39
00:04:46,500 --> 00:04:48,130
Oh, that's me.

40
00:04:48,310 --> 00:04:51,890
I'm Wallace. I'm in bread myself.

41
00:04:52,550 --> 00:04:54,060
Oh, really?

42
00:04:56,810 --> 00:05:01,150
- Are you still ballooning, Ms. B--
- I do beg your pardon?

43
00:05:01,340 --> 00:05:04,500
Oh, no, no. I mean
the Bake-O-Lite balloon.

44
00:05:04,580 --> 00:05:10,360
- Do you still fly it?
- Oh, I see. No. Not any more.

45
00:05:11,600 --> 00:05:15,550
Well, back to the grind.
As it were.

46
00:05:15,750 --> 00:05:18,040
Good bye, Ms. Bakewell.

47
00:05:18,740 --> 00:05:20,510
Oh, I'd rather say
au revoir.

48
00:05:20,510 --> 00:05:24,110
Oui, oui, madame,
and bon apetit.

49
00:05:25,580 --> 00:05:26,570
Bye-ee.

50
00:05:31,360 --> 00:05:36,290
The Bake-O-Lite girl.
Oh, fancy that, Gromit.

51
00:05:36,420 --> 00:05:40,640
It's not every day you meet
the girl of your dreams, is it?

52
00:05:43,050 --> 00:05:47,230
Oh well, this isn't gonna put
bread on the table, is it?

53
00:05:49,110 --> 00:05:50,740
Oh, hallo.

54
00:05:51,180 --> 00:05:53,780
We were just passing by
going for a walk...

55
00:05:53,780 --> 00:05:57,670
...and Fluffles insisted on dropping in
hoping you would join us.

56
00:05:57,900 --> 00:06:01,710
Please say yes. She'd be
so disappointed, wouldn't you, Fluffy?

57
00:06:02,890 --> 00:06:05,690
Yes, Fluffy.

58
00:06:07,540 --> 00:06:11,200
Oh, well, if you insist.
But I'm in me work things.

59
00:06:11,700 --> 00:06:15,460
I like a man in uniform.
Come on. Walkies.

60
00:06:15,540 --> 00:06:19,190
Manage without me, won't you, lad?

61
00:06:31,960 --> 00:06:34,530
Oh, cripes.

62
00:06:44,670 --> 00:06:47,270
Mr. Wallace, you are cheeky.

63
00:07:53,280 --> 00:07:57,180
Oh, makes some change,
doesn't it, my fudgecake?

64
00:07:57,470 --> 00:07:59,780
Gromit's gonna love this.

65
00:08:00,810 --> 00:08:03,880
Well I thought you could do
with a woman's touch around the house.

66
00:08:03,890 --> 00:08:06,210
You naughty slovenly boys.

67
00:08:06,340 --> 00:08:08,410
What do you think, Gromit?

68
00:08:08,490 --> 00:08:11,860
You wouldn't know it was
our place, would you, lad?

69
00:08:59,060 --> 00:09:01,860
Fluffles, where are you?

70
00:09:03,440 --> 00:09:06,360
Same time tomorrow,
my apple strudel.

71
00:09:10,140 --> 00:09:15,900
Love is a many splendored thing, Gromit.
But it doesn't half tire you out.

72
00:09:15,930 --> 00:09:17,980
I'm cream crackered.

73
00:09:18,650 --> 00:09:24,420
Oh, my egg. Piella's purse.
I must return it forthwith.

74
00:09:29,110 --> 00:09:31,910
Oh, uh, Gromit?

75
00:11:56,930 --> 00:12:00,220
Funny. I'm sure I heard something.

76
00:12:02,780 --> 00:12:05,900
Oh, there it 'tis.

77
00:12:07,740 --> 00:12:10,060
It must have been there all along.

78
00:12:13,210 --> 00:12:16,660
Early night, Fluffles?
Big day tomorrow.

79
00:12:17,110 --> 00:12:20,400
Our final baker is nicely
potted off.

80
00:12:27,500 --> 00:12:31,150
Good night, Fluffles.
Sweet dreams.

81
00:13:23,060 --> 00:13:26,380
Hello, stranger.
Where have you been?

82
00:13:26,570 --> 00:13:29,520
Hey, wait. Hold your horses.

83
00:13:30,220 --> 00:13:32,280
I've got something
to tell you first, old pal.

84
00:13:34,790 --> 00:13:36,660
Haven't we, dearest?

85
00:13:38,910 --> 00:13:41,420
Of course,
my little cheesecake.

86
00:13:42,110 --> 00:13:46,370
Wallace and I are engaged
to be married.

87
00:13:48,390 --> 00:13:51,350
Til' death do us part.

88
00:13:55,240 --> 00:13:58,330
I think congratulations
might be in order, lad.

89
00:13:59,300 --> 00:14:03,680
Oh, I can see he dying to
give me a great big kiss.

90
00:14:12,950 --> 00:14:16,030
I know we're going to get on
like a house on fire.

91
00:14:16,280 --> 00:14:19,120
One big happy family.

92
00:14:49,650 --> 00:14:55,060
- Hello, my vanilla slice.
- Come in, my sponge cake.

93
00:14:57,960 --> 00:15:02,290
- What's going on?
-  You have to forgive him.

94
00:15:02,420 --> 00:15:04,980
He's been a bit
security conscious of late.

95
00:15:06,660 --> 00:15:09,550
Well you can't be too careful
these days, can you?

96
00:15:12,070 --> 00:15:15,290
- Not with a cereal killer on the loose.
- Oh, yes.

97
00:15:21,820 --> 00:15:24,550
How about a nice spot
of cock-a-leekie soup?

98
00:15:24,650 --> 00:15:28,000
Oh, smashing. I've got just
the bread to go with it.

99
00:15:30,470 --> 00:15:33,080
What the...? That dog!

100
00:15:37,910 --> 00:15:42,450
- Smells delicious.
- I do hope you like it, my shortcrust.

101
00:15:42,650 --> 00:15:44,830
It's my own special recipe.

102
00:15:47,970 --> 00:15:49,570
What the...? Hey!

103
00:15:50,020 --> 00:15:54,080
What do you playing at, lad?
This is getting ridiculous.

104
00:15:54,440 --> 00:15:57,310
Oh, Wallace. He just wants
a bit of attention. That's all.

105
00:15:58,250 --> 00:16:03,730
Now, my little poochy woochy,
let Auntie Piella sort you out.

106
00:16:10,770 --> 00:16:13,210
He bit me!

107
00:16:14,640 --> 00:16:17,430
I was just trying to help and
he bit me, Wallace.

108
00:16:18,210 --> 00:16:24,330
Gromit, how dare you bite
my betrothed? That's very impolite.

109
00:16:24,540 --> 00:16:29,870
Oh, don't be too hard on him, Wallace, please.
Just a little punishment, that's all.

110
00:16:34,830 --> 00:16:37,810
I'm surprised at you, Gromit.
I really am.

111
00:16:37,880 --> 00:16:41,100
Wallace, my sugar dumpling,
have you got a mo'?

112
00:16:41,400 --> 00:16:43,000
On my way, my cupcake.

113
00:16:43,830 --> 00:16:46,370
You'll not leave this kitchen
til you've done every last one.

114
00:16:47,000 --> 00:16:53,740
I don't know. Taking a bite
out of my lovely fianc�e.
It really is the limit.

115
00:16:55,600 --> 00:17:00,480
I'm such a silly sausage.
It just sort of fell off my foot.

116
00:17:01,000 --> 00:17:05,020
Stay well back, my precious.
Leave it to me.

117
00:17:05,370 --> 00:17:09,500
Oh, you're so brave, Wallace,
my minced pie.

118
00:17:20,620 --> 00:17:21,990
Got it.

119
00:17:27,810 --> 00:17:29,440
Are you all right, my flower?

120
00:17:29,740 --> 00:17:31,050
Oh, flour.

121
00:17:31,680 --> 00:17:33,640
Get it? Flour?

122
00:17:33,890 --> 00:17:35,930
Get your hands off me.

123
00:17:36,090 --> 00:17:42,090
I hate flour, I hate bread
and I hate bakers,

124
00:17:42,290 --> 00:17:45,290
utter and complete fruitcake.

125
00:17:46,890 --> 00:17:49,550
That's a bit steep,
isn't it, my sweet?

126
00:17:50,090 --> 00:17:54,330
Fluffles, I want a word with you.
Back home.

127
00:18:06,300 --> 00:18:09,190
Oh, thanks, old pal.

128
00:18:09,900 --> 00:18:11,940
I just don't get it.

129
00:18:12,810 --> 00:18:16,910
One minute they love bakers
and the next minute they hate them.

130
00:18:18,360 --> 00:18:21,580
I know I'm not a fruitcake,
am I, lad?

131
00:18:25,720 --> 00:18:29,690
I suppose you can't be
everybody's cup of tea, can you?

132
00:18:42,040 --> 00:18:46,120
I am so sorry, Wallace.
So so sorry.

133
00:18:46,360 --> 00:18:48,360
I don't know what came over me.

134
00:18:48,580 --> 00:18:50,690
Apart from the flour, of course.

135
00:18:53,770 --> 00:18:57,710
Let's forget about it.
Here's a cake to celebrate.

136
00:18:58,120 --> 00:19:00,950
Whoops. Must be my keys.

137
00:19:02,290 --> 00:19:03,350
Celebrate?

138
00:19:03,430 --> 00:19:07,430
Us. Getting back together again,
you gooseberry fool.

139
00:19:07,930 --> 00:19:09,500
Oh, yes. Yes, of course.

140
00:19:10,720 --> 00:19:13,120
We could have that
with our four o'clock tea.

141
00:19:13,210 --> 00:19:18,090
- Won't you join us?
- I would, but Fluffles isn't feeling too well.

142
00:19:18,720 --> 00:19:20,860
Why don't you two celebrate?
Must fly!

143
00:19:23,070 --> 00:19:26,930
Roll on four o'clock, eh.
This'll go down a treat.

144
00:19:59,080 --> 00:20:01,480
Got you, you meddling mutt.

145
00:20:01,920 --> 00:20:04,100
So nice of you to come.

146
00:20:04,430 --> 00:20:07,540
Pity you'll miss
your master's tea party.

147
00:20:07,880 --> 00:20:10,260
It'll go off with a bang.

148
00:20:13,560 --> 00:20:17,060
Oh, I say.
Get the kettle on, Gromit.

149
00:20:22,090 --> 00:20:24,040
I'll deal with you two later.

150
00:20:26,670 --> 00:20:30,390
Come on, lad.
What's keeping you?

151
00:20:32,220 --> 00:20:38,520
At last. My thirteenth.
My baker's dozen.

152
00:20:41,460 --> 00:20:42,850
What?

153
00:20:49,790 --> 00:20:52,670
Curse that balloon. And curse
that prevailing southwesterly.

154
00:20:53,000 --> 00:20:55,700
They will be there in no time.

155
00:20:57,080 --> 00:21:00,180
Oh! Strike a light...

156
00:21:09,510 --> 00:21:12,680
There you are.
I think these matches are a bit...

157
00:21:14,100 --> 00:21:15,300
...damp.

158
00:21:16,190 --> 00:21:19,150
It's one of those joke candles, lad.

159
00:21:25,850 --> 00:21:29,180
Where're you going with that
c-a-a-a-ke?

160
00:21:32,700 --> 00:21:36,080
Gromit, it's a bomb.
The cake's a bomb.

161
00:21:37,510 --> 00:21:42,320
Wait a minute. You don't think
Piella could be--

162
00:21:42,890 --> 00:21:47,150
...the cereal killer? Well done, Wallace.
Sharp as a brick.

163
00:21:47,240 --> 00:21:50,670
Now do exactly as I say
or Fluffles get snuffled.

164
00:21:56,210 --> 00:22:00,310
You've crossed me once too often
you treacherous little --

165
00:22:02,010 --> 00:22:04,110
Get that thing away, lad.

166
00:22:23,570 --> 00:22:26,530
That's it, lad. Use your loaf.

167
00:22:37,280 --> 00:22:39,600
That has put a spanner in the works.

168
00:22:42,170 --> 00:22:44,380
But, Piella, you're the Bake-O-Lite girl.

169
00:22:45,000 --> 00:22:48,250
Was the Bake-O-Lite girl.
I ate too much you see.

170
00:22:49,460 --> 00:22:53,170
- Oh, really?
- I couldn't ride the ballon any more.

171
00:22:54,620 --> 00:22:57,500
- So they droped me.
- What a blow.

172
00:22:58,970 --> 00:23:03,340
Me! A curse on bakers
and their loathsome confections.

173
00:23:03,410 --> 00:23:04,860
Gromit!

174
00:23:21,100 --> 00:23:22,910
Well done, lad.

175
00:23:24,400 --> 00:23:25,820
Lass?

176
00:23:35,510 --> 00:23:38,310
Come to mummy, Fluffy Wuffy.

177
00:23:46,440 --> 00:23:48,570
Atta girl, go for the knockout.

178
00:23:52,770 --> 00:23:56,360
Don't worry about me, lad.
I'm fully in control.

179
00:24:07,410 --> 00:24:08,420
Yes.

180
00:24:16,110 --> 00:24:17,200
No.

181
00:24:19,500 --> 00:24:21,820
Oh, dear.

182
00:24:37,630 --> 00:24:39,910
Anyone seen the bomb?

183
00:24:42,330 --> 00:24:44,690
What? What?

184
00:24:47,090 --> 00:24:49,800
Bomb voyage, Wallace.

185
00:24:51,880 --> 00:24:54,590
Your bum is as good as toasted.

186
00:24:57,840 --> 00:25:01,200
Gromit, I've got a bomb
in me pants.

187
00:25:01,620 --> 00:25:05,230
Help me, Gromit. Do something.

188
00:25:13,990 --> 00:25:15,230
It tickles!

189
00:25:36,550 --> 00:25:40,970
What a relief. Oh, evening, sisters.

190
00:25:44,020 --> 00:25:48,140
I will be back to get you, Wallace.
I will have my baker's dozen.

191
00:25:49,270 --> 00:25:52,810
But, Piella, the balloon won't hold you.

192
00:25:53,050 --> 00:25:57,690
They cant just drop me.
I'm as light as a feather.

193
00:25:57,770 --> 00:26:02,790
- I'm the Bake-O-Lite girl. Noooo!
- No!

194
00:26:17,440 --> 00:26:23,740
Farewell, my angel cake. You'll
always be my Bake-O-Lite girl.

195
00:26:31,200 --> 00:26:36,000
I think I need a cup of tea
after all that.
Care to join me you two?

196
00:27:05,380 --> 00:27:12,020
Oh, never mind, lad. We've both been
through the mill this week, haven't we?

197
00:27:13,300 --> 00:27:18,810
But at least yours wasn't the bread-hating
baker-murdering cereal killer...

198
00:27:18,970 --> 00:27:20,690
...like mine.

199
00:27:21,450 --> 00:27:25,670
Tell you what, lad.
Let's go and deliver some bread. Eh?

200
00:27:26,600 --> 00:27:28,500
That'll cheer us up.

201
00:27:50,880 --> 00:27:55,790
Hey. Always room for a small one.

202
00:28:12,990 --> 00:28:18,480
# And they called it puppy love #

203
00:28:19,000 --> 00:28:22,660
Both paws on the wheel, lad.
Concentrate.

