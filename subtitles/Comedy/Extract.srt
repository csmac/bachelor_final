{1}{1}23.976
{2}{106}Are you getting what you deserve?|Call me! Joe Adler, the big gun.
{112}{140}- Hey, Cliff.|- Hey.
{346}{415}I cannot believe|that my wife slept with that guy.
{441}{478}I thought you said|it wouldn't bother you.
{480}{542}Well, I guess|I didn't think about it long enough.
{544}{614}It was so easy for him, too.|I can't stand that.
{616}{657}Maybe I'm just lousy in the sack.
{659}{694}No, man. It's not your fault.
{696}{776}I told you,|my man Brad just crushes ass, dude.
{803}{862}I'm sorry if, you know... I'm sorry.
{864}{930}That's all right. That's all right.|Apparently you're right.
{943}{987}What is it with women?|They say they don't care about looks,
{989}{1019}they just want a guy|who's smart and funny,
{1021}{1066}but they always just end up laughing
{1068}{1118}at whatever the good-looking,|stupid guy says.
{1120}{1163}I know.
{1165}{1238}Guess what else? Little prick|was at my house again today.
{1240}{1288}- For real? Why?|- Why do you think?
{1343}{1430}Unreal. You hire a guy to do a job|on one day,
{1432}{1554}and he comes back the next day,|drops it in your wife again for free.
{1556}{1627}Yeah, well, I'm not letting him do it|for free. I am paying him.
{1629}{1663}You are? How much?
{1673}{1716}The same, I guess. Why?
{1738}{1857}Well, I mean,|should I contact Brad for my 20 bills
{1859}{1928}and invoice him,|or should I go to you, or...
{1965}{1998}Don't even worry about that right now.
{2000}{2042}We can deal|with the economics of it later.
{2044}{2099}You can just table that.|That's not what's important.
{2101}{2165}What's important is that|you should be going out right now
{2167}{2239}and calling Cindy, guilt-free.|Conscience clear.
{2241}{2299}Yeah. I don't know about that.|She's at that party.
{2301}{2366}- Look. You know what you need to do?|- I don't need any more drugs.
{2368}{2463}Dude. I know I kid around a lot|and I'm a little bit of a character,
{2465}{2542}but I'm serious right now.|This is real advice.
{2544}{2592}Okay? So, I want you to listen to me.
{2609}{2675}You should try smoking a little pot.
{2698}{2779}- That's a drug.|- It's not a drug. It's a flower.
{2785}{2883}It has healing properties.|Stress is a killer.
{2885}{2927}Okay, I get paranoid when I smoke pot.
{2929}{2989}Not if you smoke a little, bro.
{2999}{3030}You know what you need to do?
{3032}{3067}You need to hang out|with my boy Willie.
{3069}{3160}He's a great guy. He's the one|who gave me that horse tranquilizer.
{3204}{3371}Dude, the Atlantians gave mankind|the secret 10,000 years ago.
{3382}{3467}See, drugs don't give you a hangover,|man. You know what I mean?
{3469}{3583}People are just now starting to use|that shit. Embrace it.
{3585}{3650}That's how we're going to solve|modern problems, man.
{3652}{3751}Wisdom of the ancients.|Ask Willie, bro. He's a shaman.
{3781}{3808}Right on.
{4211}{4241}- All right, man.|- No.
{4243}{4315}I'm really kind of a lightweight. If you've|got a beer, or even some wine...
{4317}{4351}- It's not really my thing.|- Bullshit.
{4353}{4419}Come on, Joel.|Man, he thinks he gets paranoid.
{4421}{4467}- I do get paranoid.|- Okay.
{4482}{4530}You familiar with the Graffix bong?
{4532}{4575}- You ever used one before?|- No.
{4577}{4636}Okay. Put your thumb right here.
{4638}{4716}Hey! You put your right thumb here,|on the carburetor.
{4718}{4865}Now, when I tell you to let go,|I want you to let go and inhale hard.
{4880}{4914}Okay. Exhale!
{4940}{4973}- I'm going to let you guys do this.|- Bullshit!
{4975}{5003}This doesn't work for me.|Just give mine to Dean.
{5005}{5051}- Exhale hard!|- I get paranoid, so I don't...
{5053}{5090}Exhale!
{5131}{5176}Put your lips on it. Yeah.
{5229}{5322}Go! Inhale! Go! Go! Harder!
{5365}{5397}Let go!
{5399}{5430}Inhale!
{5537}{5576}Yeah.
{5638}{5706}Oh, yeah.
{5838}{5924}Yeah. It's Jesus. You see him?
{6264}{6294}Fuck.
{6696}{6739}I thought I heard somebody|say something.
{6980}{7010}Jesus Christ.
{7012}{7064}Did you see the look on his face?
{7066}{7142}Man, you do get paranoid|when you get stoned.
{7144}{7174}You do, dude.
{7176}{7218}Did you get paranoid|when he jumped at you?
{7220}{7250}I told you guys.
{7264}{7325}Hey, dude, man.|You should call that girl now.
{7327}{7351}Well, yeah, what time is it?
{7353}{7434}Time for you to call her|and finally get laid, man. Come on!
{7436}{7473}- All right.|- It's 12:30, man.
{7475}{7518}She's probably at home now.
{7593}{7627}What does she look like?
{7629}{7756}She's... She has dark hair.|She's kind of pretty,
{7758}{7832}and, you know, working-class looking.
{7893}{7931}What do you mean by that?
{8078}{8166}That she's kind of slutty-looking?
{8294}{8350}Kind of. Yeah.
{8444}{8485}That's how I like them.
{8516}{8553}Kind of nasty. Yeah.
{8572}{8622}You guys have the same taste, man.
{8656}{8712}Yeah. Hope I don't wake her up.
{8791}{8815}Busy.
{8817}{8862}At least you know she's home, man.
{8864}{8921}I don't know what kind of person|doesn't have call waiting.
{8951}{8995}I don't have call waiting.
{9077}{9133}I hate call waiting.
{9152}{9205}Every time I hear that clicking,
{9248}{9337}I put my fist|right through someone's skull!
{9556}{9607}Are you joking? He's playing with you.
{9609}{9715}You do get paranoid|when you are stoned, huh?
{9744}{9858}Seriously, though, I hate call waiting.
{9860}{9889}Okay, man.
{9904}{9990}You're too stoned.|I'm gonna dial the number for you.
{10246}{10328}I'll tell you|why you're getting a busy signal.
{10421}{10469}This is my number.
{10609}{10702}Enough. I don't know why|you're trying to get me so paranoid.
{10704}{10736}You're gonna make me cry.
{10757}{10783}Hey.
{10820}{10846}Joel?
{10853}{10917}Hey. What are you doing here?
{10965}{11043}Joel, man, I'm not proud of you today.
{11045}{11070}Guys, seems like...
{11072}{11114}- you guys will have a lot to work out.|- Hey, man.
{11116}{11162}I don't want to be a 4th wheel.
{11175}{11226}Thanks for having me. It's been cool.
{11251}{11285}See you guys later.
{11479}{11510}Kind of nasty?
{11848}{11878}Shit.
{11948}{11994}My God. Joel, are you all right?|What happened?
{11996}{12124}I'm fine. I'm fine. Just got|my ass kicked. Just back to sleep.
{12139}{12191}- But...|- Honey, just...
{12193}{12227}By who? What happened?
{12229}{12332}Some guy at Sidelines, just...|Night-night.
{12336}{12394}My God, you should go to the hospital|so you can get checked out, maybe.
{12396}{12443}No, no, I just want to|go to sleep right now.
{12475}{12560}- Are you sure you're okay?|- Honey, just don't worry about it, okay?
{12562}{12634}People get their asses kicked|every day. Not a big deal. Back to bed.
{12705}{12758}- Can I get you some ice?|- I'm set.
{12848}{12932}Hi, Joel. Brian wants to see you.|My God.
{12945}{12979}What happened to your face?
{12981}{13021}Car door. Slammed it.
{13093}{13129}Hey, did you talk to Step yet?
{13131}{13157}Not returning my calls.
{13159}{13255}Oh, shit. We got trouble.|He did talk to a lawyer.
{13264}{13369}Joe Adler, that personal-injury lawyer|you see on all the bus-stop bench ads.
{13371}{13434}He's got that freak-show hairdo.|I mean, you got to see this guy.
{13436}{13476}He is a human turd.
{13532}{13577}Shit. What happened to you?
{13579}{13635}I fell down some stairs. Joe Adler?
{13637}{13687}Yeah. I mean,|this could be a real problem.
{13689}{13727}We could be really screwed here.
{13740}{13827}And we got to hire a couple of|new people because Cindy's quitting.
{13829}{13892}And did you know|she's been going out with Step?
{13964}{13989}What?
{13997}{14022}Can you believe that?
{14024}{14053}- Step?|- Yeah.
{14064}{14114}- Our Step?|- I know. Weird.
{14116}{14143}And they didn't even meet here.
{14145}{14223}They met, like, at some grocery store|or something, totally random.
{14228}{14290}You know, how a jimmy-john|little dingle berry like him
{14292}{14344}ends up with a girl like Cindy|is beyond me.
{14401}{14506}And if it's okay with you,|I'd like to go ahead and fire Hector.
{14541}{14566}Fire Hector? Why?
{14568}{14658}Well, you know, what's-her-face's|purse and dinkus' wallet.
{14676}{14724}It's got to be him. Who else could it be?
{14790}{14838}No. Do not fire Hector.
{14840}{14903}- Why not?|- Just... Just trust me on this.
{14932}{14970}Okay. If you say so.
{14995}{15099}Jenny, I don't feel very well.|I'm gonna go home early.
{15287}{15314}Shit.
{15485}{15511}What the...
{15568}{15639}What the hell are you doing|on my street again?
{15641}{15701}Dude, you weren't supposed|to be here for another 4 hours.
{15703}{15765}What... What happened to your face?
{15775}{15808}Same thing that's...
{15810}{15887}Your face is going to look like|my face if I ever...
{15889}{16004}Actually, your face is going to|look worse than mine if...
{16023}{16142}Listen, if I ever catch you anywhere|near my house or my wife again,
{16144}{16205}at the very least, you will be arrested.
{16240}{16301}For what? Suzie let me in.
{16380}{16484}Listen, man, I know she's your wife|and shit and whatever.
{16496}{16587}You might as well know,|I think we're in love.
{16633}{16728}In love? Are you stupid.
{16736}{16825}Stupid Brad. Stupid, dumb, dumb Brad.
{16827}{16923}You think that she is in love with you?|You don't even know her, okay?
{16925}{17006}You're nothing but a little piece of ass.|That's it.
{17046}{17078}I don't think so, dude.
{17080}{17128}You don't think so, dude?|Did you ever think
{17130}{17178}that she doesn't even know|that I paid you to have sex with her?
{17180}{17206}Did you ever think about that?
{17208}{17257}Did that ever skitter across|your little tiny brain?
{17259}{17293}How about I go home|and I tell her right now
{17295}{17363}that you did it all for money?|What about that, ding-ding?
{17365}{17390}Shut up. You wouldn't do that.
{17392}{17450}- Really? Watch this, slut.|- No!
{17452}{17509}- Here I go.|- No, wait! Please!
{17522}{17567}Don't! Man!
{17593}{17716}Listen, man, me and your wife|got something really special going on.
{17718}{17762}Please don't mess it up for me.
{17764}{17844}Don't mess it up?|You're talking about my wife!
{17847}{17917}My house! My pool! Stupid ass.
{17919}{17979}No, wait! Come on!|You can't do this to me!
{17981}{18016}Yes, I can!
{18140}{18175}Hi. You're home early.
{18177}{18225}So, how was that new pool cleaner?
{18227}{18259}What do you mean?
{18261}{18301}What do you think I mean?
{18380}{18443}Did he do a good job cleaning the pool?
{18493}{18518}Well, I...
{18520}{18594}Did he get all the leaves?|'Cause it doesn't look really clean.
{18596}{18686}I'm looking right at it,|and it looks pretty goddamn filthy.
{18704}{18775}Yeah, yeah.|It doesn't look like he did a good job.
{18777}{18809}I think he did a horrible job,
{18811}{18875}and I don't think|we're going to be using him anymore.
{18877}{18909}- Okay.|- Okay?
{18937}{18994}Are you sure you're feeling okay, Joel?|Can I take you to the doctor?
{18996}{19021}Nope!
{19048}{19075}- Do you want some aspirin?|- No, thank you!
{19077}{19103}How about some raisins?
{19153}{19177}www.frendz4m.com
{19201}{19225}www.frendz4m.com
{19249}{19273}www.frendz4m.com
{19297}{19321}www.frendz4m.com
{19560}{19604}- Hey, Step.|- Joel! Come on in.
{19606}{19633}Great.
{19647}{19690}This is my half-brother, Phil.
{19729}{19776}- Well, let's go out back.|- All right.
{19784}{19845}Hey, hey! Close the door.
{19865}{19891}Sorry.
{20052}{20104}So, you're dating Cindy?
{20128}{20192}Yes, sir. She's my girlfriend.
{20201}{20312}We might even get married after all this|lawsuit settlement stuff gets settled.
{20379}{20440}Step, you might want to be|careful with her.
{20462}{20520}What do you mean?|Does she got an STD?
{20580}{20636}I just... Step, I'm not sure|how to put this,
{20638}{20692}but, you know,|you have got a lot of money
{20694}{20784}coming your way from the settlement.|You might want to be careful with her.
{20800}{20945}Joel, I know Cindy has got|her problems, but she means well.
{20952}{21008}She's the best thing|that's ever happened to me.
{21037}{21066}Step, I don't want to tell you what to do,
{21068}{21132}but I just want you|to think about this, you know.
{21151}{21228}If you go beyond the settlement,|you continue with the lawsuit,
{21230}{21274}you could bankrupt that company.
{21276}{21319}And people are going to lose their jobs,
{21321}{21368}a lot of people are gonna be|pissed off at you, so...
{21370}{21418}No, I ain't worried about that.
{21420}{21483}I can kick anybody's ass|at that whole company.
{21500}{21525}Yeah.
{21527}{21612}Well, I'm gonna kick somebody's ass|if you don't close that screen door!
{21640}{21669}Brad, we can't. I'm sorry.
{21671}{21725}- We have to stop doing this.|- Why?
{21727}{21793}Because it's not right,|and I feel horrible,
{21795}{21858}and you have to start cleaning the pool.
{21928}{22046}But I've never felt this way|about anyone before,
{22048}{22101}and I've been with a lot of chicks.
{22141}{22278}Okay, look, Brad.|I love my husband. I really do.
{22280}{22385}And this was a mistake. I'm sorry.|I don't know what got into me.
{22387}{22534}I was bored and frustrated,|and I was not feeling particularly pretty.
{22536}{22588}But we just... We have to stop this.
{22599}{22681}This has made me realize|I really want to make my marriage work.
{22702}{22735}I'm sorry.
{22743}{22813}This doesn't mean that we can't still|see each other, though, right?
{22852}{22911}Yes, that's...|That's exactly what it means.
{22932}{22960}Why?
{22993}{23058}Because of everything that I just said.
{23093}{23133}Do you need me to say it again?
{23187}{23222}Can I see you tomorrow?
{23231}{23275}- No.|- Can I see you Thursday?
{23277}{23314}- No.|- Can I see you Friday?
{23316}{23376}Okay. You know what?|You have to go. Come on.
{23420}{23496}- I'll call you tomorrow.|- Okay. Okay. Just... Okay.
{23498}{23534}We'll talk about it later.
{23591}{23644}Oh, my God. What a moron.
{23749}{23785}Hey. Are you ready?
{23787}{23848}The meeting with Step's lawyer,|or lawyers.
{23856}{23915}- It's today, remember? Now.|- Shit.
{23917}{23976}I should fire all 3 of you
{23978}{24067}because you laughed at me|when I bought those bus-bench ads.
{24069}{24152}But this Step guy? He's the Holy Grail.
{24167}{24275}You see, if both his balls|had been knocked clean off,
{24277}{24347}he'd be a good case,|but not a great case.
{24349}{24418}A man with no balls is no man at all.
{24420}{24477}And a jury will never feel|they can walk in the shoes
{24479}{24545}of a ball-less, neutered, he-she freak.
{24557}{24663}But Step? He's got one ball, barely.
{24665}{24820}To a jury he's still a man, and that|manhood is hanging on by a thread.
{24829}{24909}I'm telling you,|this guy is a Powerball jackpot.
{24911}{24943}The fucking...
{24972}{25028}Hello. I'm Joe Adler.
{25055}{25110}- Hi, Joe. Brian. This is Joel.|- Nice to see you.
{25112}{25136}- How are you?|- Hi.
{25140}{25167}You see those guys up there?
{25169}{25221}I heard those are the guys|from General Mills.
{25223}{25292}Look, if they're gonna sell this place|and cash out and leave me with no job,
{25294}{25335}then I should get some|of that cash, man.
{25337}{25364}I mean, it's only fair.
{25366}{25413}I've been working here for,|like, 2 years.
{25415}{25477}It's bullshit, man. I work my ass off.
{25479}{25534}Remember, Hector, I was telling you|how hard I worked my ass off?
{25536}{25633}I've been working here 14 years.|We can get jobs at GemCo like that.
{25635}{25690}And at GemCo,|all the employees are owners.
{25692}{25734}It even says so on the nametags.
{25736}{25781}But the thing is,|I don't want to work at GemCo.
{25783}{25819}GemCo sucks, man.
{25821}{25871}They make you do calisthenics. Yeah.
{25873}{25942}That's right.|We could work at Southwest Airlines.
{25944}{25989}But I don't want to work|at Southwest, either.
{25991}{26036}They make you do the limbo.
{26038}{26070}That is messed up.
{26072}{26134}Yeah, I would be the laughingstock|of the grindcore community.
{26136}{26181}If we quit, this place couldn't run.
{26183}{26242}They wouldn't be able to sell this place|for anything.
{26244}{26350}Well, that's why what we should do is,|is we should all go on strike,
{26352}{26430}demand stock in the company|before it sells.
{26440}{26481}If General Mills knew|that we were on strike,
{26483}{26560}they would not buy this place|until that strike was over.
{26575}{26661}See, that way, Joel and Brian|would have to give us stock.
{26713}{26786}Yeah. We should do it.|We should strike.
{26788}{26836}That's what I've been saying|this whole time.
{26864}{26902}We've gone through everything,
{26904}{26986}and the only way we would even|consider a settlement
{26988}{27066}would be to the tune of the number|you see on the bottom of Page 18.
{27068}{27107}Does everyone have this?
{27201}{27226}What?
{27228}{27278}We don't have this kind of money,|not even close.
{27280}{27332}Not in cash, you don't. Of course not.
{27344}{27430}But if you were to sell off your assets,|the property lease, the equipment...
{27432}{27490}Are you... Are you shitting me?|This would bankrupt us.
{27492}{27530}This is what I've been trying|to tell you about, Joel.
{27532}{27581}If you think that I'm just going to give up
{27583}{27630}this entire company|that I've built from the ground up...
{27632}{27680}How about what my client gave up?
{27682}{27760}His testicles! How about that?
{27769}{27816}In fact, I'll make a deal with you.
{27835}{27894}We will drop this case right now
{27896}{28013}if you come over here|and put your testicles right here
{28015}{28072}and let me slam this door like this!
{28100}{28174}Go ahead. We can settle this right now.
{28176}{28258}Call it even.|I will drop this case right now
{28260}{28361}if you let me slam your balls|in this door,
{28363}{28422}because that's what happened|to my client!
{28424}{28496}Yep. Those are definitely the guys|from General Mills.
{28507}{28589}Now, you see that? That is|a negotiation going on right there.
{28591}{28650}That is definitely|what that looks like to me.
{28652}{28699}What are we going to do?|I got car payments.
{28701}{28744}They're going to take away my Geo.|They've done it before.
{28746}{28802}We got to act fast.|They could be about to close the deal.
{28804}{28868}Yeah, and leave us out in the cold.
{28923}{28957}Are we going to lose our jobs?
{28959}{28996}We very well could, Hector.
{28998}{29057}We need to do one of those walkouts.|That's what we need to do.
{29059}{29131}All right. Who's in? We need a show|of force. Come on! It's now or never!
{29133}{29161}Come on, people.
{29163}{29232}Let's go! Come on! Let's do this! Yeah!
{29275}{29306}- Let's go.|- Let's do it.
{29386}{29499}I will gladly come down on that price|if you go over there right now
{29501}{29537}and slam your testicles in that door...
{29540}{29640}I don't want to slam my testicles in|the door. I want you to be reasonable.
{29642}{29713}- You won't even budge one penny?|- Like I said, if you slam your...
{29715}{29768}I'll slam your balls in that door!
{29826}{29878}I'm sorry. Did you just threaten me?
{29880}{29936}- Why don't you give it a rest?|- We need to cool off for just a minute.
{29938}{29968}We'll be right back.
{30034}{30077}I don't need this. I'm going home.
{30365}{30392}Yeah, what?
{30453}{30479}What?
{30507}{30544}What's up, Joel?
{30597}{30659}Basically, we were...|We were thinking...
{30704}{30826}We were just thinking...|Well, Rory had initially pointed out
{30828}{30900}that you guys are doing this deal|with General Mills,
{30927}{31048}and we just think that...|Well, we were considering the idea...
{31050}{31122}We think that maybe|we should get a piece of it.
{31124}{31206}And, you know,|so what we decided is...
{31208}{31263}What everybody has decided is|that if we don't,
{31265}{31362}then what we're gonna do is,|we're gonna...
{31364}{31447}Hang on. You want a piece? Right?
{31458}{31514}- Yeah.|- Yeah, if we could.
{31516}{31570}How about the whole|goddamn company?
{31572}{31598}- Sure.|- You got it!
{31600}{31634}Everybody gets the whole place!
{31636}{31693}You guys run it, okay?|Everybody's in charge.
{31695}{31789}As of now. In fact, you guys can pay for|the new conveyor belt and the lawsuit.
{31791}{31826}You can go meet with Adler, okay?
{31828}{31916}He might slam your balls in the door,|but at least you'll be the boss. Okay?
{32096}{32169}So, was that good, or...
{32225}{32293}I was going to say more,|but you interrupted me, and I...
{32385}{32448}Our lunch buffet features all|the food you love at a great...
{32478}{32542}Vandella will make the turn,|he moves to second.
{32544}{32593}He's got himself a double.|Big turn at second.
{32595}{32645}He throws on the brakes.|He'll stay there.
{32652}{32706}Well, he just turned on that pitch.|What can we say?
{32708}{32761}I think 4 innings|is all Berot can handle,
{32763}{32846}and he hasn't handled|the 4th inning. With 2 doubles...
{32848}{32900}Listen, Joel,|I need to tell you something.
{32988}{33074}You've been really busy at the factory|for the last couple years,
{33076}{33149}and ever since|I started working at home,
{33151}{33216}it seems like, I don't know,|maybe I just wasn't feeling...
{33218}{33282}as pretty as I used to feel, and I just...|- You banged the pool cleaner.
{33284}{33311}- What?|- Is that what you're trying to tell me?
{33313}{33353}You had sex with Brad.
{33391}{33435}- You knew?|- Yeah, I knew. I hired him.
{33437}{33481}Yeah, I know you hired him,|but how did you know that...
{33483}{33570}I didn't hire him to clean the pool.|I hired him to have sex with you.
{33572}{33596}What?
{33645}{33701}Wait... Why?
{33760}{33814}I hired him to have an affair with you,
{33816}{33910}because I wanted to have an affair|and not feel guilty,
{33912}{34017}but I was very, very drunk, and I was|on some kind of a horse tranquilizer.
{34019}{34099}Brad was getting paid?|You paid Brad this whole time?
{34101}{34139}All 15 times?
{34188}{34216}15?
{34218}{34271}- Yes.|- Jesus Christ.
{34273}{34355}Has it even been 15 days?
{34381}{34458}Unbelievable.|How can you even sit down?
{34460}{34523}Wait, you wanted to have an affair|with another woman?
{34534}{34581}Yes, but I didn't.
{34583}{34634}Why didn't you just talk to me about it?
{34636}{34687}I was going to, but, you know,
{34708}{34785}Dean, you know, he gave me some pill.
{34802}{34834}God, you asshole.
{34836}{34874}- I'm the asshole?|- Yes! I'm leaving.
{34876}{34908}Suzie, technically|I didn't even do anything.
{34910}{34950}You could've just said,|"No, thanks, just clean the pool. "
{34952}{34998}None of this would've happened|if you hadn't hired him.
{35000}{35060}Hey, guys. Glad I caught you.
{35062}{35124}You guys hardly ever answer the door.
{35126}{35178}- Now's not a really good time.|- I'm leaving.
{35180}{35254}When do you think|would be a good time, you think?
{35256}{35302}I don't know. I don't know.
{35304}{35350}- See, the thing is...|- How about tomorrow?
{35352}{35383}- You want to come by tomorrow?|- Great.
{35385}{35425}- Okay?|- What time?
{35445}{35491}I don't know. Just sometime tomorrow|would be great.
{35493}{35584}Okay, great. So, if you could go and|have that check, that would be great.
{35586}{35628}- It's 110.|- Okay.
{35630}{35706}- That's 2 plates at $55 a plate.|- Okay, okay.
{35708}{35749}- I'm going to close this.|- Okay.
{37137}{37169}- Joel?|- Hi.
{37171}{37255}Hey, what...|How did you know that I was here?
{37257}{37322}I'm actually staying here, too.|I thought I saw you, so...
{37324}{37350}Come in. Sorry.
{37352}{37435}Do you want something to drink?|I have soda.
{37444}{37472}No, thanks.
{37489}{37556}Listen, I am so sorry|about what happened with Willie.
{37558}{37632}He gets crazy.|That's why I had to move out,
{37634}{37691}so, have a seat.
{37760}{37810}That's Mary's purse, right?
{37817}{37880}Right there. That's Mary's purse, right?
{37889}{38017}No, that's my bag. So, wait. How are|things at work? How is everybody?
{38131}{38247}This is Mary's purse, and you stole it.
{38258}{38286}Am I right?
{38312}{38407}Joel, that's my bag.|It probably just looks like hers.
{38420}{38462}You know,|there's a lot of them out there.
{38464}{38508}I have never seen another one.
{38532}{38593}Or another teddy in a fucking tutu.
{38663}{38741}Now, what are you doing with Step?
{38755}{38812}You're the only reason|that he's suing this company, right?
{38814}{38906}Do you have any idea how much work|I put into building up that company?
{38908}{38959}Do you have any clue? Do you care?
{38995}{39040}I'm gonna go. I'm gonna take this.
{39042}{39081}If it really is your bag,|you can call the police,
{39083}{39133}you can tell them that I stole it.|All right?
{39164}{39202}On second thought,|I'm gonna call them right now
{39204}{39252}and I'm gonna tell them|that you stole it.
{39320}{39384}- Joel, please don't.|- Why?
{39398}{39454}Joel, please don't call the police.|I'm on probation,
{39456}{39510}and I will go to jail|for a really long time.
{39512}{39538}Well, maybe you should have|thought about that
{39540}{39583}before you started|ripping off my employees.
{39585}{39641}Joel, please? Listen, I promise you
{39643}{39696}I will leave Step alone|and he will drop the lawsuit.
{39698}{39730}How do I know that?
{39749}{39783}You can keep the bag.
{39785}{39862}If I don't leave Step alone,|if he doesn't drop the lawsuit,
{39864}{39908}then you can turn me in.
{40012}{40094}Yeah, that...|And Sylvia's wallet and Jim's watch?
{40096}{40142}You got those? Can I have those, too?
{40261}{40361}I guess that works. I mean, great.
{40407}{40469}I bet you weren't even|into food flavoring, were you?
{40512}{40581}What is your deal?|How do you end up like this?
{40637}{40663}Hey?
{40685}{40731}Hey? This is what I'm talking about.
{40733}{40809}This manipulation here with the tears.|I'm not gonna fall for that.
{40811}{40838}Forget it.
{40867}{40956}You better not be faking it, okay,|because I'm actually feeling bad.
{40972}{40997}Hello?
{41088}{41120}Are you faking it?
{41192}{41333}Well, I'm sorry. You know, I just...|You know, I just...
{41354}{41422}I was just curious about|how somebody ends up like this.
{41424}{41457}All right. Just...
{41548}{41638}Very sorry. Okay? Don't worry about it.
{41680}{41750}I just thought I'd just ask how a...
{42316}{42374}Cindy? Cindy?
{42423}{42450}Shit.
{42890}{43012}{Y:i}Who were you thinking of when|we were making love last night?
{43064}{43169}{Y:i}Was it a good-looking stranger|or a close friend of mine?
{43239}{43376}{Y:i}You didn't want to quit|when we was into it last night
{43459}{43528}Joel hasn't been in all morning.|Can you believe that?
{43541}{43591}If we come in late, we get in trouble.
{43695}{43754}And Hector didn't steal it. Cindy did.
{43824}{43855}Can you believe that?
{43857}{43935}Blaming Cindy just to protect Hector.|Typical.
{44083}{44155}We really do need to go|to the grocery store, Step.
{44233}{44273}You heard from Cindy lately?
{44275}{44347}It's been over 3 days since|she borrowed your truck.
{44367}{44413}Maybe we ought to call the cops|or something...
{44415}{44505}Look, she'll be back, all right?|She wouldn't steal my truck.
{44507}{44534}Okay.
{44632}{44686}That's the last bottle of Pepsi.
{44688}{44780}Maybe I'll just call Domino's|and have them deliver some Pepsi.
{44836}{44914}Still, if she didn't steal your truck
{44916}{44988}and she didn't get into an accident,|what do you reckon...
{44990}{45071}Shut up about Cindy, already,|before I kick your fat ass!
{45128}{45177}Domino's. Hello, Mr. Wilkinson.
{45179}{45203}Hello.
{45308}{45332}- Hey.|- Hey, Brian.
{45334}{45366}Step's here to see you.
{45399}{45463}Is he? Great. Bring him in.
{45472}{45519}He's outside at the loading dock.
{45521}{45631}He wants to talk to you alone out there|for some reason. You know?
{45633}{45665}Man to man.
{45753}{45836}I'm sick and tired of dealing|with that Adler fellow.
{45854}{45960}Truth is, I just want my old life back.|I just wanna get back to work.
{45987}{46066}You know? I'm a working man.|That's what I do.
{46078}{46162}The problem is,|if I bankrupt the company,
{46184}{46240}there won't be a job for me|to go back to.
{46261}{46312}You don't have to bankrupt|the company.
{46328}{46402}Well, if I drop the lawsuit,|you'll sell the company,
{46404}{46514}and the new company, well, who'd|want to hire somebody with one ball?
{46524}{46656}Well, you know, listen, I don't think|they're actually allowed to ask,
{46658}{46695}so you're okay.
{46751}{46802}You know, I'm a working man, too,|Step. You know?
{46804}{46907}I make extract. That's what I do.|You know? Vanilla.
{46946}{47024}Cherry. Root beer. S'mores.
{47043}{47102}And a lot of people don't think that|that's very cool,
{47104}{47180}but I think that it is pretty cool, so,
{47220}{47272}why would I want to sell this place?
{47292}{47392}I think that I just got distracted|with Dean and the drugs
{47394}{47461}and the gigolos and...
{47544}{47620}You know, what I'm saying is|that I'm thinking about maybe
{47622}{47664}not selling the company.
{47684}{47742}That is, if there isn't a lawsuit.
{47762}{47812}There is going to be|that insurance money, Step,
{47814}{47854}so what do you think?
{47876}{47916}Yeah, that sounds fair to me.
{47969}{48007}But under one condition.
{48037}{48068}What's that?
{48080}{48128}You make me floor manager.
{48293}{48327}Hold the line!
{48356}{48388}Thank you.
{48416}{48444}Okay.
{48475}{48535}Everybody gather around! Listen up!
{48644}{48782}Okay. First of all, I want to say that|I've decided not to sell the company.
{48805}{48836}All right?
{48838}{48954}And, secondly, I'm making Step here|the new floor manager.
{48970}{49044}Anybody doesn't like that,|I hear they're hiring over at GemCo.
{49064}{49129}Remember, though, at GemCo,|the owner doesn't know your names.
{49131}{49162}You'll probably never even meet him.
{49164}{49231}He's, like,|in some corporate office somewhere.
{49247}{49280}Here, you know, I'm just upstairs.
{49282}{49351}You can come up there and you can|tell me if you got a problem, okay?
{49353}{49389}That's all. Okay?
{49406}{49442}Step, the floor is yours.
{49510}{49538}You're not selling?
{49566}{49597}I'm not selling.
{49607}{49658}You better start learning|their names, Brian.
{49723}{49765}- You just go off to work and do...|- Yeah.
{49767}{49867}Hey! You two get back to work!|Quit your yapping.
{50052}{50096}Dejen de parlotear.
{50398}{50428}Hey, man.
{50474}{50540}Look, I know you probably|want to kick my ass.
{50542}{50643}I just wanted to tell you that|I'm not gonna see Suzie anymore,
{50645}{50684}so you don't have to worry.
{50696}{50736}She's really into you, dude.
{50748}{50826}I guess that's why|she married you and shit.
{50840}{50867}Whatever.
{50880}{50964}You're a really lucky guy,|and I'm sorry if I messed shit up.
{50974}{51007}Why'd you have to get hung up|on Suzie?
{51009}{51046}You could have any girl you want.
{51048}{51141}Yeah, but I want Suzie.|I can't have her.
{51144}{51187}You had her 15 times.
{51212}{51244}Well, yeah.
{51278}{51362}But she doesn't love me, dude.|It's just not the same.
{51376}{51451}- Anyway, I just wanted to tell you that.|- All right. You told me.
{51485}{51527}All right. Later.
{51720}{51839}Hey, so, I was wondering,|I quit my landscaping job,
{51841}{51933}and I don't really think I'm cut out|for the whole gigolo thing.
{51935}{52061}You know? So, do you think|you might have, like, an opening
{52063}{52134}or whatever at the extract plant?
{52136}{52184}I'm not going to give you a job. Okay?
{52218}{52246}Yeah. Sorry.
{52401}{52490}Or just come by the office|and fill out an application.
{52492}{52532}I'll see what I can do, okay?
{52552}{52594}Cool. Thanks, man.
{52666}{52702}There he goes.
{52717}{52776}Johnny Horse-cock. Rolling.
{52820}{52882}You know, I'm starting to think|this might have been a mistake.
{52884}{52947}- Really?|- Maybe it was my fault, you know?
{52949}{52991}- Maybe it was your fault.|- No, it's yours.
{52993}{53043}A lot of blame to go around here.
{53045}{53149}I think there are some people|who just aren't meant to do drugs, Joel.
{53176}{53227}I think you're one of those people, man.
{53393}{53421}Hello?
{53438}{53465}Shit.
{53521}{53580}Why are you cleaning|the pool yourself?
{53582}{53623}New guy didn't work out?
{53625}{53668}Yeah, no, didn't work out.
{53696}{53756}Boy, it's just hard|to get good help, huh?
{53758}{53785}Yeah.
{53855}{53928}Hey. Did you get a chance|to write that check?
{53962}{54042}Nathan, Joel and I are not going|to that dinner. Okay?
{54067}{54137}Gee, I wish you would have told me that|before I went and bought those tickets.
{54139}{54200}Joel never agreed to it,|and neither did I.
{54236}{54303}Well, it sure sounded like|you guys were coming.
{54305}{54354}I mean, Leslie was going|to talk to you about it,
{54356}{54400}but you don't return our calls.
{54415}{54494}I mean, I already bought those tickets,|and they're non-refundable.
{54516}{54586}I really wish you guys had been|a little more clear with me.
{54636}{54691}Well, then,|let me be clear with you now.
{54711}{54799}When we say things like,|"I don't think so,"
{54801}{54842}or "I'm not sure,"
{54844}{54954}or we close the door in your face,|that means, "No. "
{54956}{54992}Why can't you get that?
{55005}{55054}- Well, it...|- Shut up! Okay.
{55056}{55141}Let me be even more clear|with you, Nathan.
{55143}{55192}We don't like you.
{55203}{55252}Is that clear enough for you?
{55254}{55343}You're dull. You talk too much.|You never listen.
{55345}{55382}You're always in our yard.
{55384}{55442}I don't know what the hell|you're barbecuing over there,
{55444}{55481}but it stinks.
{55483}{55573}You lay out in your front yard|and listen to your car radio!
{55575}{55654}You are the worst neighbor|in the world.
{55656}{55744}We don't like you. Is that clear enough|for you, Nathan? Is that clear?
{55746}{55777}Shut up!
{55784}{55814}Nathan?
{55838}{55896}Nathan? Nathan?
{55946}{55970}www.frendz4m.com
{55994}{56018}www.frendz4m.com
{56042}{56066}www.frendz4m.com
{56090}{56114}www.frendz4m.com
{56672}{56704}- Hi.|- Hi.
{56764}{56796}Are you okay?
{56856}{56914}I think I might have killed him, Joel.
{56916}{56990}The last thing he heard was me|yelling at him to shut up.
{56992}{57059}Well, he did talk a lot.
{57061}{57158}Yeah. He was going on and on.|I finally lost it.
{57160}{57233}I'm sure it wasn't your fault.|Probably just a coincidence, you know.
{57235}{57300}I don't know. I just...|I still feel really guilty about it.
{57307}{57412}I actually told Leslie I would go|to that Rotary Club dinner tonight.
{57436}{57493}She said Nathan|would have wanted it that way.
{57495}{57536}I said I'd go to that, too.
{57684}{57769}So, who was this woman that you|wanted to have the affair with, anyway?
{57771}{57862}She was just some criminal drifter.
{57933}{57958}It wasn't that great.
{57960}{58039}You know, I thought that she was|really into food flavoring and...
{58041}{58078}- Really?|- Yeah, I should have known better.
{58080}{58120}Nobody's into food flavoring.
{58141}{58199}Well, I don't know.|I mean, you guys sell a lot of that stuff.
{58201}{58238}People are into it.
{58391}{58434}You should know, by the way,
{58460}{58496}I hired Brad.
{58520}{58586}Yeah, I know. You told me that,|remember? The whole...
{58588}{58674}No, sorry,|not for the sex thing or the pool.
{58682}{58736}I mean that I hired him for the factory.
{58752}{58808}Why? He's a total moron.
{58810}{58874}Yeah, I know.|This is going to sound kind of weird,
{58876}{58941}but I felt sorry for him.
{58963}{59008}Yeah. So did I.
{59088}{59147}Well, my car's over here, so,
{59168}{59240}I guess I'll see you at the Rotary Club?
{59286}{59323}Yeah, I'll see you there.
{59428}{59458}Suzie?
{59490}{59538}Should we maybe just take one car?
{59566}{59611}Yeah. We probably should.
{59630}{59658}Yeah.
{59713}{59788}I thought that was|a surprisingly tasteful funeral.
{59790}{59888}Yes, it was. You know, when|I was looking down on him, I thought,
{59900}{59979}"This might be the longest|I've ever seen him with his mouth shut. "
{59981}{60040}Yeah. I didn't expect|that many mourners.
{60051}{60092}There were a lot of people there.
{60094}{60119}Yeah.
{60190}{60291}You have been a friend of mine,|rainy day woman
{60338}{60401}That woman of mine, she ain't happy
{60411}{60490}Till she finds something wrong|and someone to blame
{60492}{60532}If it ain't one thing,|it's another one on the way
{60609}{60640}{Y:i}Hey. You've reached Cindy.
{60642}{60678}{Y:i}I'm not here, so leave a message.
{60763}{60797}Where the hell is she?
{60860}{60937}How long does it take to get some|goddamned cigarettes around here?
{61063}{61099}Here you are, sir.
{61161}{61196}What the fuck?
{62497}{62541}Rainy day woman
{62560}{62662}I've never seem to see you for|the good times or the sunshine
{62699}{62800}You have been a friend of mine,|rainy day woman
{62810}{62837}Woman
