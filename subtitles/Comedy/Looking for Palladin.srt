1
00:00:00,100 --> 00:00:04,827
Sync and corrected by <font color="#FF0000">Elderfel</font>
www.addic7ed.com

2
00:00:05,611 --> 00:00:06,979
Fuck! Look at this thing.

3
00:00:07,080 --> 00:00:08,847
- It's huge!
- (Laughs)

4
00:00:08,948 --> 00:00:10,582
- I'm so excited.
- Yeah.

5
00:00:10,683 --> 00:00:12,717
I didn't think I was
gonna be this excited.

6
00:00:12,818 --> 00:00:15,254
I'm so glad we are done with
that fucking game, right?

7
00:00:15,355 --> 00:00:16,588
Yes. Congratulations, by the way.

8
00:00:16,689 --> 00:00:18,122
You too. Look at that view!

9
00:00:18,223 --> 00:00:20,324
- Now that is impressive.
- This is amazing!

10
00:00:20,426 --> 00:00:23,261
- Oh.
- Ohh.

11
00:00:23,362 --> 00:00:25,129
- Ahoy.
- Hello there.

12
00:00:25,230 --> 00:00:27,198
You know what? I'm gonna put the
whole Richie thing behind me.

13
00:00:27,299 --> 00:00:29,267
I'm just gonna
have fun and geek out.

14
00:00:29,368 --> 00:00:31,202
We should get
totally drunk in this.

15
00:00:31,303 --> 00:00:33,804
Oh my God!

16
00:00:33,906 --> 00:00:35,806
This is incredible!

17
00:00:35,908 --> 00:00:37,541
Oh, it's my perfect utopia.

18
00:00:37,642 --> 00:00:39,743
- Sailors giving out free drinks.
- (Laughs)

19
00:00:39,844 --> 00:00:42,546
Hello, sir. Thank you so much.
You've made my night.

20
00:00:42,647 --> 00:00:43,680
- Cheers.
- Cheers.

21
00:00:43,782 --> 00:00:45,383
- We fucking deserve this.
- Abso-fucking-lutely.

22
00:00:45,483 --> 00:00:47,351
ANNOUNCER:
Welcome to "Naval Destroyer"!

23
00:00:47,452 --> 00:00:49,453
(Cheering)

24
00:00:51,790 --> 00:00:53,290
Congratulations.

25
00:00:53,391 --> 00:00:55,826
But they keep telling us
that we have to expand

26
00:00:55,927 --> 00:00:57,360
our demographic,

27
00:00:57,462 --> 00:00:59,095
and then they force us
to make a game

28
00:00:59,196 --> 00:01:00,631
where you can't even
play as a female.

29
00:01:00,732 --> 00:01:02,633
And I'm a guy
and I always play as a female.

30
00:01:02,734 --> 00:01:04,367
- But that's...
- And before you say it,

31
00:01:04,469 --> 00:01:05,969
- it's not because I'm gay.
- Okay.

32
00:01:06,070 --> 00:01:08,504
- There's nothing to be ashamed of, buddy.
- I'm not ashamed.

33
00:01:08,605 --> 00:01:12,508
And FYI, this is the gayest game
in the history of the world.

34
00:01:12,609 --> 00:01:14,044
(Laughing)
Right?

35
00:01:14,145 --> 00:01:15,912
I mean, first of all,

36
00:01:16,013 --> 00:01:17,847
there's a lot of seamen in it.

37
00:01:17,949 --> 00:01:20,250
And from over here,
the title of the poster

38
00:01:20,351 --> 00:01:21,784
looks like "Anal Destroyer."

39
00:01:21,885 --> 00:01:24,454
Now you guys made it,
so it's not...

40
00:01:24,555 --> 00:01:27,457
So why do you play as the woman?

41
00:01:27,558 --> 00:01:29,592
Well...

42
00:01:29,693 --> 00:01:30,860
maybe it is because I'm gay,

43
00:01:30,961 --> 00:01:32,929
but not for the reason
you might think.

44
00:01:33,029 --> 00:01:35,331
Women are the outsiders in games,
and I relate to that.

45
00:01:35,432 --> 00:01:36,599
Gay people get it.

46
00:01:36,700 --> 00:01:38,367
Does that answer your question?

47
00:01:39,970 --> 00:01:41,736
(Mock British accent)
"Why do you play as a woman?"

48
00:01:41,838 --> 00:01:43,072
(Laughs)

49
00:01:43,173 --> 00:01:47,176
- British people are awful.
- The worst.

50
00:01:47,277 --> 00:01:48,477
(Dance music playing)

51
00:01:48,578 --> 00:01:49,611
Okay, move a little to the left.

52
00:01:49,712 --> 00:01:51,413
- You're blocking my view.
- All right, jeez.

53
00:01:51,514 --> 00:01:52,581
What should we talk about,
all right?

54
00:01:52,582 --> 00:01:54,083
We want to seem nonchalant.

55
00:01:54,184 --> 00:01:55,184
Let's talk about the Middle East.

56
00:01:55,285 --> 00:01:56,952
No, he can't even hear us anyway.

57
00:01:57,052 --> 00:01:58,820
Oh, shit. Okay.

58
00:01:58,921 --> 00:02:00,221
What?

59
00:02:00,322 --> 00:02:03,592
He has got a very gay laugh.

60
00:02:03,693 --> 00:02:05,560
- Very gay laugh.
- You can't have a gay laugh.

61
00:02:05,661 --> 00:02:06,962
Yes, you can. Believe me.

62
00:02:07,062 --> 00:02:08,563
No no! You have to be subtle.

63
00:02:08,664 --> 00:02:10,899
Shit.

64
00:02:11,000 --> 00:02:13,901
- Less teeth.
- Okay. All right, bring it down.

65
00:02:14,003 --> 00:02:16,371
(Clears throat)
Oh my God.

66
00:02:16,472 --> 00:02:17,738
He is so gay.

67
00:02:17,839 --> 00:02:20,708
He is gay gay gay.

68
00:02:20,809 --> 00:02:22,510
The gayest thing
I've ever seen on two legs,

69
00:02:22,611 --> 00:02:24,345
I'm sure of it.
How much you want to bet?

70
00:02:24,446 --> 00:02:25,579
Two dollars.

71
00:02:25,680 --> 00:02:27,382
- Two dollars?
- Yeah.

72
00:02:27,483 --> 00:02:30,317
I'll bet you a thousand dollars
that man is gay.

73
00:02:30,418 --> 00:02:31,919
- All right, deal.
- Okay.

74
00:02:32,020 --> 00:02:33,253
All right. Oh shit.
Fuck. He's on the move.

75
00:02:33,354 --> 00:02:34,855
He's on the move.
I'm gonna go get him.

76
00:02:34,956 --> 00:02:36,724
- Calm down. Deep breaths.
- Okay.

77
00:02:36,825 --> 00:02:38,191
- Keep it together.
- (Exhales) All right.

78
00:02:38,292 --> 00:02:40,794
Cheers. You're about to
owe me a thousand dollars.

79
00:02:40,896 --> 00:02:41,929
All right.

80
00:02:47,368 --> 00:02:48,935
(Sighs)

81
00:02:49,037 --> 00:02:51,971
(Militaristic drumming)

82
00:02:56,710 --> 00:02:58,945
You gonna try it out?

83
00:02:59,047 --> 00:03:03,215
Yeah.

84
00:03:03,316 --> 00:03:05,251
- Do you want to play?
- Sure.

85
00:03:07,988 --> 00:03:08,988
Whoa.

86
00:03:10,658 --> 00:03:11,891
Are these things real?

87
00:03:11,992 --> 00:03:13,560
Yeah, I think so.

88
00:03:13,661 --> 00:03:14,994
That's cool, right?

89
00:03:17,665 --> 00:03:19,231
Real torpedoes.

90
00:03:19,332 --> 00:03:21,900
Ooh! It's cold.

91
00:03:21,901 --> 00:03:22,968
(Chuckles)

92
00:03:23,070 --> 00:03:24,703
(Clear throat)

93
00:03:26,706 --> 00:03:28,240
Comfy?

94
00:03:28,341 --> 00:03:29,575
Let's do this.

95
00:03:29,676 --> 00:03:31,377
(Punching, grunting on TV)

96
00:03:31,478 --> 00:03:35,047
You... are... seriously good.

97
00:03:35,147 --> 00:03:36,381
Well, I fucking should be.

98
00:03:36,482 --> 00:03:39,184
We have mandatory
play day every Friday.

99
00:03:39,285 --> 00:03:41,053
- Ah!
- Uh-huh.

100
00:03:41,153 --> 00:03:42,588
Didn't see that coming.

101
00:03:42,689 --> 00:03:45,057
Good... fucking move.

102
00:03:45,157 --> 00:03:46,592
Thank you.

103
00:03:46,693 --> 00:03:48,594
So...

104
00:03:48,695 --> 00:03:52,263
did you buy my theory from earlier?

105
00:03:52,364 --> 00:03:54,599
Oh, about who plays who?

106
00:03:54,700 --> 00:03:56,267
Sure.

107
00:03:56,368 --> 00:03:57,635
Okay.

108
00:03:57,736 --> 00:04:00,871
So, um...

109
00:04:00,973 --> 00:04:02,607
who do you...

110
00:04:02,708 --> 00:04:04,542
you know...

111
00:04:04,643 --> 00:04:05,876
Oh, play as?

112
00:04:05,978 --> 00:04:07,145
Yeah.

113
00:04:07,246 --> 00:04:10,214
Do you play as the...
Fuck you.

114
00:04:10,315 --> 00:04:12,049
- Do you play as the...
- As the woman?

115
00:04:12,150 --> 00:04:13,618
Well, you know, the lady.

116
00:04:13,719 --> 00:04:16,220
(Mock British accent) Do you play
as the British lady or the bloke?

117
00:04:16,321 --> 00:04:17,554
Which one is it?

118
00:04:17,656 --> 00:04:18,990
(Chuckles)

119
00:04:20,325 --> 00:04:21,993
You're asking if I'm gay,
aren't you?

120
00:04:23,662 --> 00:04:26,230
- Yes.
- Uh-huh.

121
00:04:26,331 --> 00:04:28,565
I am.

122
00:04:28,666 --> 00:04:32,236
I am... gay.

123
00:04:32,337 --> 00:04:34,904
Okay.

124
00:04:35,006 --> 00:04:37,907
That's cool.

125
00:04:38,009 --> 00:04:39,243
(Chuckles)

126
00:04:39,344 --> 00:04:41,377
Cool? Okay. Is that what it is?

127
00:04:41,478 --> 00:04:44,247
(Laughs)
Yeah.

128
00:04:44,348 --> 00:04:47,517
You know, we never have
any gay guys around here.

129
00:04:47,618 --> 00:04:51,521
Not that I'm suggesting that
we're gonna hook up, of course.

130
00:04:51,622 --> 00:04:53,189
Of course.

131
00:04:53,290 --> 00:04:54,924
Unless, you know,

132
00:04:55,026 --> 00:04:56,526
I let you win a couple more games...

133
00:04:56,627 --> 00:04:58,561
Well, I have a boyfriend.

134
00:04:58,662 --> 00:05:00,663
Right.

135
00:05:03,300 --> 00:05:05,000
I was just trying
to be funny anyway.

136
00:05:05,101 --> 00:05:07,537
Fuck you, fuck you.

137
00:05:07,638 --> 00:05:09,872
So you're based in Seattle, yeah?

138
00:05:09,973 --> 00:05:11,874
- Was.
- Okay.

139
00:05:11,975 --> 00:05:13,108
Live here now.

140
00:05:13,210 --> 00:05:15,878
I'm setting up a new team

141
00:05:15,979 --> 00:05:17,312
now that you guys have launched.

142
00:05:17,413 --> 00:05:19,081
At MDG?

143
00:05:19,182 --> 00:05:20,215
Yeah.

144
00:05:20,316 --> 00:05:21,750
I start tomorrow.

145
00:05:21,851 --> 00:05:23,919
As in you could be my new boss?

146
00:05:24,020 --> 00:05:26,321
(Grunting on TV)

147
00:05:28,658 --> 00:05:30,025
(Sighs)

148
00:05:30,126 --> 00:05:32,227
Well, that sounds very formal.

149
00:05:32,328 --> 00:05:35,564
But... yeah.

150
00:05:35,665 --> 00:05:36,998
I suppose.

151
00:05:40,002 --> 00:05:42,904
It's true. Dean just told me.

152
00:05:43,005 --> 00:05:45,240
He starts tomorrow, and he's picking
his team for "The Infinite."

153
00:05:45,341 --> 00:05:48,576
Oh, fuck.
Owen, you have no idea.

154
00:05:48,677 --> 00:05:50,244
I'm sure you were fine.

155
00:05:50,345 --> 00:05:53,181
No, I asked out my boss
while straddling a torpedo.

156
00:05:53,281 --> 00:05:55,249
(Laughs)
And he said no.

157
00:05:55,350 --> 00:05:57,585
I don't know why you're laughing.

158
00:05:57,686 --> 00:05:59,587
If I don't get on his team,
neither do you.

159
00:05:59,688 --> 00:06:01,956
(Seriously)
You need to apologize.

160
00:06:03,958 --> 00:06:06,527
(Sighs)
(Distant laughter)

161
00:06:06,628 --> 00:06:08,195
Fuck.

162
00:06:09,297 --> 00:06:10,698
(Trolley bell dings)

163
00:06:12,634 --> 00:06:14,568
DORIS:
Okay, you are freaking me out.

164
00:06:14,669 --> 00:06:16,403
- Seriously.
- Why, because I'm in a good mood?

165
00:06:16,504 --> 00:06:19,540
Yes. I don't think
I've seen you this perky

166
00:06:19,641 --> 00:06:22,542
since you dragged me
to see "Miss Congeniality,"

167
00:06:22,643 --> 00:06:24,144
and that was a very long time ago.

168
00:06:24,245 --> 00:06:25,746
What can I say?
I'm in a good mood.

169
00:06:25,847 --> 00:06:27,547
Oh, stop it.
(Laughs)

170
00:06:27,648 --> 00:06:29,216
You know what?
(Humming)

171
00:06:29,316 --> 00:06:33,419
In some weird fucked up way,
I think seeing Ethan has kind of...

172
00:06:33,520 --> 00:06:36,089
not helped me, because I don't want
to give that motherfucker credit.

173
00:06:36,190 --> 00:06:38,891
- Yeah, exactly. And you shouldn't.
- Yeah, but it has,

174
00:06:38,992 --> 00:06:41,026
and you're just gonna
have to embrace that.

175
00:06:41,128 --> 00:06:43,429
Oh, I will. I will.
It's like a lunar eclipse.

176
00:06:43,530 --> 00:06:45,431
I'm gonna get my shades
and a picnic.

177
00:06:45,532 --> 00:06:47,367
I'm gonna take this in.
It's gonna make me go blind.

178
00:06:47,468 --> 00:06:49,702
Why the picnic?
I don't understand.

179
00:06:49,803 --> 00:06:51,804
(Dance music playing)

180
00:06:56,676 --> 00:06:59,578
I think I'm finally gonna do it.

181
00:06:59,679 --> 00:07:01,513
Do what?

182
00:07:01,614 --> 00:07:03,282
You know.

183
00:07:05,952 --> 00:07:07,186
Peri-peri?

184
00:07:07,287 --> 00:07:09,188
Yep.

185
00:07:09,289 --> 00:07:10,689
The restaurant?

186
00:07:12,024 --> 00:07:13,024
Where are you, really?

187
00:07:15,361 --> 00:07:16,961
Yeah, and don't say I'm crazy.

188
00:07:18,898 --> 00:07:21,400
No, I wasn't gonna say that.

189
00:07:21,501 --> 00:07:24,469
I think that's a great idea. I do.
I think it sounds amazing.

190
00:07:24,570 --> 00:07:26,504
But... and I hate to say this, but...

191
00:07:27,773 --> 00:07:28,907
How?

192
00:07:30,642 --> 00:07:31,976
I have my plans.

193
00:07:34,646 --> 00:07:35,980
She's killing me.

194
00:07:39,651 --> 00:07:41,152
Where'd you get these placemats?

195
00:07:41,253 --> 00:07:43,220
I stole them... from Target.

196
00:07:43,322 --> 00:07:45,889
In Oakland a week and
she's already a hood rat.

197
00:07:45,990 --> 00:07:48,225
Well, you know, I gots
to take care of my man.

198
00:07:48,326 --> 00:07:51,761
So, uh, I want to skip work today
and stay home and practice.

199
00:07:51,862 --> 00:07:53,363
You want to hang out?
I want to play you something.

200
00:07:53,464 --> 00:07:56,899
I can't. After spending three weeks
putting up Stina's piece of shit,

201
00:07:57,000 --> 00:07:58,668
now I gotta take it
back down again.

202
00:08:01,071 --> 00:08:02,839
Mmm! I was, um...

203
00:08:02,940 --> 00:08:05,174
talking to Pauline
about her gallery space,

204
00:08:05,275 --> 00:08:07,710
and she... What?

205
00:08:07,811 --> 00:08:09,178
Don't even think about it, Franklin.

206
00:08:09,279 --> 00:08:10,847
No no.

207
00:08:10,948 --> 00:08:12,481
She was just asking if you had
anything you wanted to show,

208
00:08:12,582 --> 00:08:13,682
so I just thought I'd ask.

209
00:08:13,783 --> 00:08:15,852
Come on. There's no way
she asked you that.

210
00:08:15,953 --> 00:08:17,987
She did. I'm asking you.

211
00:08:18,088 --> 00:08:19,989
If you don't have anything to
show, it's not a big deal.

212
00:08:20,090 --> 00:08:21,957
- Well, I don't. You know that I don't.
- I know you don't.

213
00:08:22,959 --> 00:08:25,193
But maybe you could.

214
00:08:25,294 --> 00:08:26,395
We could clear out the shed.

215
00:08:26,496 --> 00:08:27,929
I never use that
stupid weight bench.

216
00:08:28,030 --> 00:08:29,531
Yeah, maybe you should.

217
00:08:29,632 --> 00:08:31,199
Yeah.

218
00:08:31,300 --> 00:08:33,301
I need to work out more.

219
00:08:37,106 --> 00:08:39,107
(People chattering)
(Knocks)

220
00:08:44,347 --> 00:08:46,014
KEVIN:
...by your department.

221
00:08:46,115 --> 00:08:48,216
So I'm...

222
00:08:49,318 --> 00:08:51,219
I'm doing that right now.

223
00:08:51,320 --> 00:08:52,854
Uh, so...

224
00:08:53,990 --> 00:08:56,557
When it comes to the...

225
00:08:56,658 --> 00:08:57,759
Do you know what?
Do you have an extension?

226
00:08:57,860 --> 00:08:59,126
I need to call you back.

227
00:08:59,227 --> 00:09:00,495
Great. I will call you back later.

228
00:09:00,596 --> 00:09:02,497
Thank you so much.
Okay, bye-bye.

229
00:09:02,598 --> 00:09:04,164
- Hello, Patrick.
- Hi.

230
00:09:04,266 --> 00:09:05,966
Um...

231
00:09:06,067 --> 00:09:07,835
- Good morning.
- Good morning.

232
00:09:07,936 --> 00:09:11,171
Listen, I hope you don't mind.
I just wanted to come in

233
00:09:11,273 --> 00:09:13,207
and apologize for last night.

234
00:09:13,308 --> 00:09:14,841
For what?

235
00:09:14,943 --> 00:09:17,378
Well, you know,
I was a little drunk,

236
00:09:17,478 --> 00:09:19,513
and I hope that I
didn't come across as...

237
00:09:19,614 --> 00:09:21,048
As...?

238
00:09:21,149 --> 00:09:23,517
Well, as you know,
one of those guys

239
00:09:23,618 --> 00:09:25,286
that...
(Inhales sharply)

240
00:09:28,956 --> 00:09:30,924
Yeah, it's fine. It's fine.

241
00:09:31,025 --> 00:09:32,258
I should have told you
I was starting here

242
00:09:32,359 --> 00:09:33,727
before you went too far.

243
00:09:33,828 --> 00:09:35,161
Thank you.

244
00:09:35,262 --> 00:09:36,663
Listen, I would also love it

245
00:09:36,764 --> 00:09:39,533
if you could consider me
and my lead artist Owen

246
00:09:39,634 --> 00:09:41,835
for the new game.
I know you guys are just getting started...

247
00:09:41,936 --> 00:09:43,870
Yeah, it's not early days,
unfortunately.

248
00:09:43,971 --> 00:09:46,339
We've got a very short
development window.

249
00:09:46,440 --> 00:09:47,540
That's perfect.

250
00:09:47,641 --> 00:09:48,874
- Then I'm your man.
- Yeah?

251
00:09:48,976 --> 00:09:50,676
Well, it will be 24/7.
Would you be okay with that?

252
00:09:50,777 --> 00:09:52,078
- Absolutely.
- Are you sure?

253
00:09:52,179 --> 00:09:53,413
Yes.

254
00:09:53,513 --> 00:09:54,814
Because I was checking out
your performance earlier.

255
00:09:54,915 --> 00:09:56,882
- Well then you'll see...
- Do you realize we log

256
00:09:56,984 --> 00:09:59,285
all of your internet activity here?

257
00:09:59,386 --> 00:10:00,686
- What?
- And it seems

258
00:10:00,787 --> 00:10:03,689
you've been visiting
certain websites... a lot.

259
00:10:03,790 --> 00:10:07,359
Uh, OKCupid...

260
00:10:07,461 --> 00:10:09,027
and... what is it?

261
00:10:09,128 --> 00:10:10,896
Is it ManCunt?

262
00:10:10,997 --> 00:10:12,431
Uh, ManHunt, actually.

263
00:10:12,532 --> 00:10:14,166
I was just showing Owen.
It was just educational.

264
00:10:14,267 --> 00:10:16,034
Sorry about that.
I read that wrong.

265
00:10:16,135 --> 00:10:17,569
Uh, no, that's fine.

266
00:10:17,670 --> 00:10:19,704
I mean, I wouldn't
worry about it too much.

267
00:10:19,805 --> 00:10:21,573
Just... just be careful

268
00:10:21,674 --> 00:10:23,141
what you look at during work hours.

269
00:10:23,242 --> 00:10:24,876
Sends a certain message.
(Cellphone ringing)

270
00:10:24,978 --> 00:10:26,979
- Shit.
- Take it. I've got to make a call anyway.

271
00:10:27,080 --> 00:10:29,414
It was good to see you again.
(Stammers)

272
00:10:35,354 --> 00:10:37,355
Okay, that was really bad timing.

273
00:10:38,357 --> 00:10:39,924
No, not today.

274
00:10:40,025 --> 00:10:42,259
No, I can't. Today's not good.

275
00:10:42,361 --> 00:10:44,596
(Sighs)

276
00:10:44,697 --> 00:10:46,698
Okay, I'll be there.

277
00:10:48,701 --> 00:10:49,834
(Sighs)

278
00:10:49,935 --> 00:10:51,434
(Drill whirring)

279
00:10:53,372 --> 00:10:55,939
STINA: You've got to be
more careful with this.

280
00:10:56,041 --> 00:10:57,274
I was careful.

281
00:10:57,376 --> 00:10:59,076
Seriously,
I don't want to get to London

282
00:10:59,177 --> 00:11:00,211
and find that nothing fits

283
00:11:00,312 --> 00:11:01,678
and it all looks like shit.
(Scoffs)

284
00:11:01,779 --> 00:11:03,547
(Drill whirrs)

285
00:11:03,648 --> 00:11:05,416
If you have something
to say about the work,

286
00:11:05,517 --> 00:11:06,683
you should just say it.

287
00:11:06,784 --> 00:11:10,988
Come on, Stina. There's no point in
commenting on a finished piece.

288
00:11:12,524 --> 00:11:15,225
It's not your best work.

289
00:11:15,326 --> 00:11:18,328
If I'm really being honest,
I don't like it.

290
00:11:19,998 --> 00:11:21,899
STINA:
Why?

291
00:11:22,000 --> 00:11:23,099
(Sighs)
Look at it.

292
00:11:23,200 --> 00:11:25,101
It's a bunch of
furniture stacked up.

293
00:11:25,202 --> 00:11:27,570
It's got no meaning,
it has no perspective.

294
00:11:27,671 --> 00:11:31,141
It's like a chair apocalypse.

295
00:11:33,477 --> 00:11:35,578
Four days before I show.
Thank you for that.

296
00:11:35,679 --> 00:11:39,382
I'm sorry.
I was just trying to be honest.

297
00:11:39,483 --> 00:11:42,919
I really look forward to seeing
some of your work in the future.

298
00:11:43,020 --> 00:11:45,922
Oh wait, that would mean you would
have to make something, wouldn't it?

299
00:11:46,023 --> 00:11:47,924
Fuck off, Stina, really.

300
00:11:48,025 --> 00:11:49,358
Actually, yeah.

301
00:11:50,360 --> 00:11:51,427
We're done.

302
00:11:51,528 --> 00:11:54,329
Done? What do you mean
"we're done"?

303
00:11:54,431 --> 00:11:56,165
I mean we're done.

304
00:11:58,701 --> 00:12:02,171
I need somebody working for me
who actually wants to work for me.

305
00:12:06,642 --> 00:12:09,545
Fine. Good luck in London.

306
00:12:09,645 --> 00:12:12,247
I'm sure they'll love
your fucking chairs.

307
00:12:12,348 --> 00:12:13,782
(Drill clatters)

308
00:12:16,752 --> 00:12:18,553
Oh, then you have
a serious misunderstanding

309
00:12:18,654 --> 00:12:20,622
- of the word "flattery."
- He took the time to search through

310
00:12:20,723 --> 00:12:23,424
- your internet records.
- I know. Hasn't that been banned?

311
00:12:23,525 --> 00:12:26,695
I know I signed some Facebook campaign
having something to do with privacy.

312
00:12:26,796 --> 00:12:29,363
And what's wrong with having
a life outside of work?

313
00:12:29,465 --> 00:12:31,432
This is San Francisco.
That's why I live here.

314
00:12:31,533 --> 00:12:33,101
If I didn't want to have a life,
I'd move to LA.

315
00:12:33,202 --> 00:12:34,502
Yeah, then you could
hang out with Ethan

316
00:12:34,603 --> 00:12:37,105
and use Eastern Philosophy
to justify being a dick.

317
00:12:37,206 --> 00:12:39,240
I just wish I could
control my stupid mouth.

318
00:12:39,341 --> 00:12:41,909
Um, wasn't this lunch
supposed to be about me?

319
00:12:42,010 --> 00:12:43,111
Oh my God, yes.

320
00:12:43,212 --> 00:12:44,579
I'm so sorry, I'm so sorry.

321
00:12:44,679 --> 00:12:47,247
I think it's an amazing idea.

322
00:12:47,348 --> 00:12:49,249
- Really?
- Of course I do!

323
00:12:49,350 --> 00:12:50,918
I'm excited for you.
It's about fucking time.

324
00:12:51,019 --> 00:12:52,119
It's what you've always wanted.

325
00:12:52,220 --> 00:12:53,587
- Yeah.
- Right?

326
00:12:53,688 --> 00:12:55,255
Tell me how much you love chicken.

327
00:12:55,356 --> 00:12:56,389
Oh my God.

328
00:12:56,491 --> 00:12:58,926
I fucking love chicken.

329
00:12:59,027 --> 00:13:00,527
- Right?
- It's the queen of meats.

330
00:13:00,629 --> 00:13:02,862
- It is the queen of meats.
- It's the meat to beat.

331
00:13:02,963 --> 00:13:04,197
(Laughs)
It's the meat to beat.

332
00:13:04,298 --> 00:13:05,865
I gotta get to work.
Will you walk me?

333
00:13:05,966 --> 00:13:07,934
- Sure.
- All right.

334
00:13:08,035 --> 00:13:10,036
(Cellphone buzzes)

335
00:13:14,308 --> 00:13:15,975
(Sighs)

336
00:13:24,317 --> 00:13:26,218
Crappy day?

337
00:13:26,319 --> 00:13:29,221
I got fired, so...

338
00:13:29,322 --> 00:13:31,157
Would you like a job?

339
00:13:31,258 --> 00:13:33,559
(Scoffs)
No.

340
00:13:33,661 --> 00:13:34,894
Then it's a good day.

341
00:13:34,995 --> 00:13:37,663
(Cellphone ringing)

342
00:13:40,567 --> 00:13:42,901
Hi.

343
00:13:43,002 --> 00:13:44,903
Yep.

344
00:13:45,004 --> 00:13:47,239
I can do that.
Just text me your address.

345
00:13:47,340 --> 00:13:49,575
It's 220.

346
00:13:49,676 --> 00:13:51,910
An hour.

347
00:13:52,011 --> 00:13:54,012
Great. I'll see you later.

348
00:13:56,348 --> 00:13:58,583
That's a healthy hourly rate.

349
00:13:58,685 --> 00:14:01,519
Well, people are into beards.

350
00:14:01,620 --> 00:14:04,522
You should consider a career move.

351
00:14:04,623 --> 00:14:06,858
I'd lose the icing though.

352
00:14:06,959 --> 00:14:09,194
Oh.

353
00:14:09,295 --> 00:14:11,196
(Chuckles)

354
00:14:11,297 --> 00:14:13,864
So, um...

355
00:14:13,965 --> 00:14:15,166
you're a...?

356
00:14:16,635 --> 00:14:18,869
Sex worker?

357
00:14:18,970 --> 00:14:21,206
You can say it.

358
00:14:21,307 --> 00:14:23,541
Do you want my card?

359
00:14:23,642 --> 00:14:26,510
I just got new ones
printed in matte finish.

360
00:14:26,611 --> 00:14:29,213
Oh my God.
That's so professional.

361
00:14:29,314 --> 00:14:31,348
Well, some people
are good with numbers,

362
00:14:31,449 --> 00:14:32,883
they become accountants.

363
00:14:32,984 --> 00:14:34,218
I'm useless with numbers

364
00:14:34,319 --> 00:14:36,553
but very good at sex.

365
00:14:36,654 --> 00:14:38,089
So...

366
00:14:38,190 --> 00:14:40,290
You just... you put it out there.

367
00:14:40,391 --> 00:14:43,560
You got no qualms
about telling strangers?

368
00:14:43,661 --> 00:14:46,296
This is what I do.

369
00:14:46,397 --> 00:14:48,732
If I was embarrassed about it,
I wouldn't do it.

370
00:15:00,945 --> 00:15:02,846
TAJ:
It's a hard fucking sell, Dom.

371
00:15:02,947 --> 00:15:05,248
Does anyone even know
what Portuguese chicken is?

372
00:15:05,349 --> 00:15:06,716
- It's a peri-peri.
- I know that.

373
00:15:06,817 --> 00:15:09,185
- Does anyone else?
- That's what Twitter's for.

374
00:15:09,286 --> 00:15:10,854
Look what happened to Todd,

375
00:15:10,955 --> 00:15:12,455
and he already had a restaurant.

376
00:15:12,556 --> 00:15:14,857
And that one lasted, what,
for like three months?

377
00:15:14,958 --> 00:15:16,425
That's because nobody
even likes meatballs.

378
00:15:16,526 --> 00:15:18,994
Who wants to go out to dinner
and eat fucking meatballs?

379
00:15:19,095 --> 00:15:21,196
I'm not saying yours
is not a good idea.

380
00:15:21,298 --> 00:15:24,867
Taj, I get we need investors.
All right? I do.

381
00:15:24,968 --> 00:15:27,536
But that's why
I need you on board.

382
00:15:27,638 --> 00:15:28,871
Come on.

383
00:15:28,972 --> 00:15:30,873
This is our moment.
We talked about this.

384
00:15:30,974 --> 00:15:32,141
Dom.

385
00:15:32,242 --> 00:15:34,877
I'm sorry, buddy.
I just can't risk it.

386
00:15:34,978 --> 00:15:36,979
If they find out that
I'm leaving, I'm screwed.

387
00:15:38,648 --> 00:15:40,149
But if you come up with the money...

388
00:15:40,250 --> 00:15:42,251
Yeah, I get it. I get it.

389
00:15:44,654 --> 00:15:47,789
I get it. It's okay.

390
00:15:51,761 --> 00:15:53,561
(Sighs)

391
00:15:53,663 --> 00:15:55,563
We need to figure that shit out.

392
00:15:55,665 --> 00:15:57,332
You know, I heard
he already hired Joseph?

393
00:15:58,467 --> 00:15:59,701
B.O. Joe?

394
00:15:59,802 --> 00:16:02,170
- Yeah.
- Oh, God.

395
00:16:02,271 --> 00:16:04,105
All right. You know what?

396
00:16:04,206 --> 00:16:06,240
If we don't make it
on Kevin's game,

397
00:16:06,341 --> 00:16:08,176
then we'll just look
at it as an opportunity.

398
00:16:08,277 --> 00:16:12,180
Maybe this corporate environment
is choking our creativity anyway.

399
00:16:12,281 --> 00:16:14,816
This could be our opportunity
to take control of our lives.

400
00:16:14,917 --> 00:16:16,851
Are you talking about
being unemployed?

401
00:16:16,952 --> 00:16:18,119
Are you crazy?

402
00:16:18,220 --> 00:16:19,453
I'm Asian, all right?

403
00:16:19,554 --> 00:16:22,823
Our DNA rends itself apart
when we lose our jobs.

404
00:16:22,924 --> 00:16:25,058
It's scary and ugly.

405
00:16:25,159 --> 00:16:27,528
We should avoid it.

406
00:16:27,629 --> 00:16:29,530
Well...

407
00:16:29,631 --> 00:16:32,199
We'll figure something out.

408
00:16:32,300 --> 00:16:34,635
(Typing)

409
00:16:35,970 --> 00:16:38,104
(Sighs)

410
00:16:47,982 --> 00:16:49,015
(Sighs)

411
00:16:50,351 --> 00:16:52,452
(Lock clicks)

412
00:16:52,553 --> 00:16:54,554
(Silverware clinking)

413
00:17:01,928 --> 00:17:03,596
Hello?

414
00:17:07,934 --> 00:17:10,102
(Indistinct chatter)

415
00:17:12,939 --> 00:17:14,173
(Chuckles)
Hey.

416
00:17:14,274 --> 00:17:15,841
What the fuck are you doing here?

417
00:17:15,942 --> 00:17:17,176
I had a bad fucking day.

418
00:17:17,277 --> 00:17:18,844
Oh, good.

419
00:17:18,945 --> 00:17:21,513
Me too.

420
00:17:21,514 --> 00:17:22,581
Want to get drunk?

421
00:17:22,682 --> 00:17:25,184
No, I had too much
to drink last night.

422
00:17:25,285 --> 00:17:26,852
You want to get Thai food?

423
00:17:26,952 --> 00:17:28,953
- Yeah.
- Yes.

424
00:17:31,290 --> 00:17:33,191
Your feet smell.

425
00:17:33,292 --> 00:17:34,860
- No, please. Stop.
- (Laughs)

426
00:17:34,960 --> 00:17:37,962
- I'm gonna order.
- Okay.

427
00:17:41,634 --> 00:17:43,868
PATRICK:
Kevin Matheson.

428
00:17:43,969 --> 00:17:46,871
Oh, I can't believe
he has a Wikipedia page.

429
00:17:46,972 --> 00:17:48,806
I found him. Here he is.

430
00:17:48,907 --> 00:17:50,607
This is kind of
a really good picture of him.

431
00:17:50,708 --> 00:17:52,209
He doesn't normally
look this good, but yeah.

432
00:17:52,310 --> 00:17:54,411
- What do you think?
- You want to fuck him?

433
00:17:54,512 --> 00:17:55,879
No, I do not want to fuck him.

434
00:17:55,980 --> 00:17:57,881
He's an asshole
and he made me feel stupid.

435
00:17:57,982 --> 00:17:59,616
'Cause he wouldn't fuck you?

436
00:17:59,717 --> 00:18:02,485
No, because he's an asshole
and he made me feel stupid.

437
00:18:02,586 --> 00:18:04,354
And more than that, he's got
the wrong impression of me,

438
00:18:04,455 --> 00:18:06,155
and I fucking hate that.

439
00:18:06,256 --> 00:18:07,490
It seems like all I do lately

440
00:18:07,591 --> 00:18:09,592
is give people the wrong impression.

441
00:18:11,462 --> 00:18:12,796
I got fired today.

442
00:18:15,265 --> 00:18:16,833
Really?

443
00:18:16,934 --> 00:18:19,502
How is that not
the first thing you told me?

444
00:18:19,603 --> 00:18:20,703
What are you gonna do?

445
00:18:20,805 --> 00:18:22,171
Okay, stop asking so many questions.

446
00:18:22,272 --> 00:18:23,772
Okay. It's just...

447
00:18:24,908 --> 00:18:26,608
major news. Jesus.

448
00:18:29,947 --> 00:18:31,247
I don't know.

449
00:18:32,615 --> 00:18:34,616
Would you have sex with him?

450
00:18:37,620 --> 00:18:39,521
- Yeah.
- Really?

451
00:18:39,622 --> 00:18:42,190
With those ears?
I mean, those ears are big.

452
00:18:42,291 --> 00:18:44,960
Yeah, he's like the white Will Smith.

453
00:18:46,395 --> 00:18:48,396
(Pop music playing)

454
00:18:58,040 --> 00:19:00,640
(Showers running)

455
00:19:28,403 --> 00:19:30,504
AGUSTIN:
I mean, he owned what he does.

456
00:19:30,605 --> 00:19:32,839
Oh, so you want to be
a prostitute now?

457
00:19:32,940 --> 00:19:35,509
It doesn't seem like
a very smart career move.

458
00:19:35,610 --> 00:19:37,511
It'd be nice to make
some money though.

459
00:19:37,612 --> 00:19:38,845
Yeah, that's true.

460
00:19:38,946 --> 00:19:43,517
Hey, do you think that
people fetishize thick thighs?

461
00:19:43,618 --> 00:19:46,119
If I was a prostitute,
that could be my thing.

462
00:19:46,220 --> 00:19:47,721
He was proud of what he does.

463
00:19:47,822 --> 00:19:50,590
If I'm being honest, it made
me totally fucking jealous.

464
00:19:50,691 --> 00:19:51,925
I'm good.

465
00:19:52,026 --> 00:19:55,061
Jealous because people
pay him for sex?

466
00:19:55,162 --> 00:19:57,196
When was the last time you heard
me call myself an artist?

467
00:19:57,297 --> 00:19:58,865
Oh my God.
You say it all the time.

468
00:19:58,966 --> 00:20:00,867
No, I don't. I don't ever say it.

469
00:20:00,968 --> 00:20:02,201
I mean, not out loud.

470
00:20:02,302 --> 00:20:04,537
I don't think that really matters.

471
00:20:04,638 --> 00:20:06,339
When's the last time
you heard me say out loud

472
00:20:06,440 --> 00:20:08,207
that I'm a level designer?

473
00:20:08,308 --> 00:20:09,742
Yeah, but that's different.

474
00:20:09,843 --> 00:20:11,077
You don't want to be known
as a level designer,

475
00:20:11,178 --> 00:20:12,711
because you want to
be more than that.

476
00:20:12,812 --> 00:20:15,048
True.

477
00:20:15,149 --> 00:20:19,317
I can't call myself an artist,
'cause I don't do shit.

478
00:20:21,988 --> 00:20:23,989
(Sighs)

479
00:20:25,992 --> 00:20:27,993
- You know what?
- What?

480
00:20:30,997 --> 00:20:32,765
I don't know if either of us are...

481
00:20:33,934 --> 00:20:36,000
very good at being
who we think we are.

482
00:20:38,003 --> 00:20:40,338
Maybe we need to
try a little harder.

483
00:20:45,511 --> 00:20:47,512
(Pop music playing)

484
00:20:57,022 --> 00:20:59,256
LYNN:
Is that to keep the energy up?

485
00:20:59,357 --> 00:21:01,993
It's been a long day.

486
00:21:02,094 --> 00:21:03,094
Sorry.

487
00:21:06,966 --> 00:21:08,865
For what?

488
00:21:08,967 --> 00:21:12,303
No one really likes to talk in
these places anymore, do they?

489
00:21:14,973 --> 00:21:16,540
Did they ever?

490
00:21:16,641 --> 00:21:18,875
Sure, they used to.

491
00:21:18,977 --> 00:21:21,479
They had bands sometimes.
Food.

492
00:21:21,580 --> 00:21:24,547
Still had sex,
but it was friendlier.

493
00:21:24,649 --> 00:21:25,983
(Chuckles)

494
00:21:29,320 --> 00:21:32,489
You always lived in the city?

495
00:21:32,590 --> 00:21:34,491
Well, I wasn't born here, of course,

496
00:21:34,592 --> 00:21:37,494
but... I heard the siren,

497
00:21:37,595 --> 00:21:39,395
and west I came.
(Laughs)

498
00:21:41,999 --> 00:21:44,234
I bet it was cool back then.

499
00:21:44,335 --> 00:21:46,569
"Back then."

500
00:21:46,670 --> 00:21:48,238
Suddenly feel like I'm 103.

501
00:21:48,339 --> 00:21:49,905
- Oh, I'm sorry. (Both laugh)
- No.

502
00:21:50,007 --> 00:21:55,512
No, but it was. It really was.
And then it wasn't.

503
00:21:57,681 --> 00:21:59,915
I'm Lynn.

504
00:22:00,017 --> 00:22:01,517
Dom.

505
00:22:01,618 --> 00:22:03,185
Very formal, isn't it?

506
00:22:03,286 --> 00:22:04,520
(Chuckles)

507
00:22:04,621 --> 00:22:07,523
Seeing as how we're both
naked underneath these towels.

508
00:22:07,624 --> 00:22:08,958
Yeah.

509
00:22:11,294 --> 00:22:13,862
I have the florist shop on Castro.

510
00:22:13,963 --> 00:22:15,531
Oh, shit. "Buds"?

511
00:22:15,632 --> 00:22:16,665
Yeah, that's me.

512
00:22:16,766 --> 00:22:19,201
Oh, man, you're like an institution.

513
00:22:19,302 --> 00:22:21,403
That makes me sound like
the Gay Man's Chorus.

514
00:22:21,504 --> 00:22:22,671
Everyone likes them in principle,

515
00:22:22,772 --> 00:22:24,873
but no one wants to
hear them actually sing.

516
00:22:24,974 --> 00:22:28,210
We use your flowers at Zuni.

517
00:22:28,311 --> 00:22:29,878
- I work there.
- I know.

518
00:22:29,979 --> 00:22:32,213
- I recognized you.
- Really?

519
00:22:32,314 --> 00:22:34,983
Yeah, you're somewhat
of an institution too.

520
00:22:39,989 --> 00:22:41,890
(Sighs)

521
00:22:41,991 --> 00:22:44,092
Well, he's looking very determined.

522
00:22:48,363 --> 00:22:50,698
Yes, he is.

523
00:22:53,001 --> 00:22:54,335
You should go for it.

524
00:22:57,005 --> 00:22:59,240
(Exhales)

525
00:22:59,341 --> 00:23:00,508
I think I will.

526
00:23:00,609 --> 00:23:01,942
(Chuckles)

527
00:23:07,281 --> 00:23:10,283
We should grab lunch
or something sometime.

528
00:23:11,619 --> 00:23:13,286
LYNN:
You know where I am.

529
00:23:23,964 --> 00:23:25,965
(Sighs)

530
00:23:40,513 --> 00:23:42,281
(Mouse clicks)

531
00:23:47,654 --> 00:23:49,989
(Exhales)

532
00:24:02,269 --> 00:24:04,270
(Train rattling)

533
00:24:39,971 --> 00:24:42,206
- Hey.
- Hey.

534
00:24:42,307 --> 00:24:45,876
- Can I talk to you for a minute?
- Yeah.

535
00:24:45,977 --> 00:24:48,545
I want you to know that I
do care about this job.

536
00:24:48,646 --> 00:24:49,880
A lot.

537
00:24:49,981 --> 00:24:51,982
This is all I've ever wanted to do.

538
00:24:52,083 --> 00:24:53,217
Okay, good.

539
00:24:53,318 --> 00:24:56,887
But I also want to have a life,

540
00:24:56,988 --> 00:24:58,888
and I'm pretty sure
that I can do both

541
00:24:58,989 --> 00:25:01,157
and not let you down.

542
00:25:01,258 --> 00:25:02,492
What's this?

543
00:25:02,593 --> 00:25:04,828
It's a new build
for "Naval Destroyer"...

544
00:25:04,929 --> 00:25:07,364
- Mm-hmm?
- ...with a playable female character.

545
00:25:07,465 --> 00:25:09,098
Okay.

546
00:25:10,601 --> 00:25:13,136
Take a seat.

547
00:25:13,237 --> 00:25:14,571
Um...

548
00:25:16,941 --> 00:25:20,109
You do know I was kind of
fucking with you before?

549
00:25:20,211 --> 00:25:21,277
What do you mean?

550
00:25:21,278 --> 00:25:23,246
You were always gonna
be on my team.

551
00:25:23,347 --> 00:25:25,781
- Really?
- Yeah.

552
00:25:27,017 --> 00:25:29,618
But commitment looks good on you.

553
00:25:31,488 --> 00:25:34,690
All right.
(Keyboard clicking)

554
00:25:36,159 --> 00:25:39,043
Let's play.

555
00:25:41,565 --> 00:25:44,722
Sync and corrected by <font color="#FF0000">Elderfel</font>
www.addic7ed.com

