1
00:00:16,933 --> 00:00:19,536
You've said that you didn't kill Jae Hee at court, right?

2
00:00:19,536 --> 00:00:24,875
That Jae Hee had an affair, and that person would have killed Jae Hee.

3
00:00:24,875 --> 00:00:27,978
We can't be here. Your wife is here!

4
00:00:27,978 --> 00:00:29,679
She's not my wife.

5
00:00:29,679 --> 00:00:31,648
Then who is that woman?

6
00:00:31,648 --> 00:00:33,250
Mom!

7
00:00:33,250 --> 00:00:38,455
You just tell me who she was with. I'll catch him with my own hands.

8
00:00:38,455 --> 00:00:41,858
I'll take you out of this place.

9
00:00:41,858 --> 00:00:46,730
She went to Cha Min Jae's house because she suspected him the most.

10
00:00:48,532 --> 00:00:53,670
After checking, Jae Hee started changing two years ago.

11
00:00:53,670 --> 00:00:57,441
After that, she met him there.

12
00:00:57,441 --> 00:01:03,013
I think Jae Hee's husband used his mother and lover to catch the person who killed Jae Hee.

13
00:01:03,013 --> 00:01:07,284
All the men related to the students at the studio were suspected to be Jae Hee's lover

14
00:01:07,284 --> 00:01:10,821
Then the reason why she set up a hair salon is because of Se Yeon, your husband?

15
00:01:10,821 --> 00:01:12,989
Hello?

16
00:01:14,825 --> 00:01:18,261
Do you know how long I waited for you? Why did you do that?

17
00:01:18,261 --> 00:01:20,797
I thought of the insurance money.

18
00:01:20,797 --> 00:01:23,934
We can payback all the debts then we can...

19
00:01:23,934 --> 00:01:27,637
We can live without worrying about money, so how I could I appear?

20
00:01:27,637 --> 00:01:31,708
Han Sang Hoon, he probably went through my things to find my fingerprint.

21
00:01:31,708 --> 00:01:33,610
Kim Young Dae came looking for you, didn't he?

22
00:01:33,610 --> 00:01:37,547
Are you sure my husband is Jae Hee's lover?

23
00:01:37,547 --> 00:01:41,251
You chose the wrong person. Hwang Dong Seok.

24
00:01:41,251 --> 00:01:43,753
Han Jeong Won's husband Chef Hwang Dong Seok.

25
00:01:43,753 --> 00:01:46,423
Your husband...

26
00:01:46,423 --> 00:01:49,059
Don't trust him, ever.

27
00:01:49,059 --> 00:01:53,230
I need that money to save our Ye Rin.

28
00:01:53,230 --> 00:01:56,099
Just live as a dead man.

29
00:01:56,099 --> 00:01:57,434
So you agree with it?

30
00:01:57,434 --> 00:02:01,171
He said to hand over all the insurance money except for the cost of Ye Rin's surgery.

31
00:02:02,339 --> 00:02:04,608
Are you ready to leave?

32
00:02:04,608 --> 00:02:06,810
Do you know you're trespassing?

33
00:02:06,810 --> 00:02:08,545
Trespassing?

34
00:02:08,545 --> 00:02:10,580
I'm the official wife

35
00:02:10,580 --> 00:02:14,151
and you are his cheap mistress!

36
00:02:18,622 --> 00:02:20,891
I told you to do only what I said.

37
00:02:23,927 --> 00:02:26,129
Kim Young Dae.

38
00:02:30,033 --> 00:02:31,902
Where is Kim Young Dae?

39
00:02:31,902 --> 00:02:35,472
Let's talk.

40
00:02:35,472 --> 00:02:36,606
Did you wake up?

41
00:02:36,606 --> 00:02:38,842
Why did you kill? There was no reason to.

42
00:02:38,842 --> 00:02:40,777
There was a time I went to Se Yeon's cafe.

43
00:02:40,777 --> 00:02:44,548
But I got caught by the lady at the hair salon.

44
00:02:44,548 --> 00:02:46,416
Don't hurt Se Yeon.

45
00:02:46,416 --> 00:02:50,787
Don't worry about that. She's very important to me.

46
00:02:50,787 --> 00:02:55,458
A few years ago, many people died of Malathion poisoning in China.

47
00:02:55,458 --> 00:02:57,928
It was a fraud for insurance money.

48
00:02:57,928 --> 00:02:59,429
What is that?

49
00:02:59,429 --> 00:03:01,598
What have you done to our Ye Rin?

50
00:03:01,598 --> 00:03:04,568
I'll tell the insurance company that Kim Young Dae is alive!

51
00:03:04,568 --> 00:03:07,737
Tell them! Tell them your husband is alive!

52
00:03:07,737 --> 00:03:10,540
I'll let you die in prison.

53
00:03:11,641 --> 00:03:16,746
He said my lips smell like olive oil.

54
00:03:16,746 --> 00:03:20,217
I'm that kind of person to him.

55
00:03:20,217 --> 00:03:22,986
Mistress

56
00:03:23,920 --> 00:03:29,693
Ye Rin! Smile!

57
00:03:29,693 --> 00:03:32,829
Over there!

58
00:03:36,633 --> 00:03:40,837
Not long ago we reported an uprise of insurance scams

59
00:03:40,837 --> 00:03:43,707
Another insurance scam has taken place again.

60
00:03:43,707 --> 00:03:49,579
A young daughter was administered a pesticide called Malathion to claim insurance money.

61
00:03:49,579 --> 00:03:52,349
The culprit was arrested today.

62
00:03:55,919 --> 00:04:00,156
14 hours ago

63
00:04:02,325 --> 00:04:03,793
Episode 10

64
00:04:11,134 --> 00:04:16,406
What did you do all night? You told the PD you were on broadcast, but you didn't answer your phone all night.

65
00:04:18,141 --> 00:04:21,011
Do you know how worried I was?

66
00:04:26,650 --> 00:04:28,518
What are you doing?

67
00:04:38,828 --> 00:04:41,197
What are you doing!!

68
00:05:12,228 --> 00:05:16,299
Are you like this because of this? What is this?

69
00:05:16,299 --> 00:05:20,770
She liked me all by herself! I didn't care for her!

70
00:05:20,770 --> 00:05:26,710
Then why did you try so hard not to lose the bracelet from Na Yoon Jeong?

71
00:05:32,649 --> 00:05:34,818
Are you sure it's still nothing?

72
00:05:36,619 --> 00:05:39,522
Hey! Speak up!

73
00:05:40,423 --> 00:05:42,325
What about you!

74
00:05:45,228 --> 00:05:47,797
You had an affair with him.

75
00:05:56,639 --> 00:05:58,508
Yes, I did.

76
00:06:00,143 --> 00:06:05,782
You knew about it, so you wanted me to be miserable by playing that video in the living room.

77
00:06:05,782 --> 00:06:08,218
What kind of husband would be okay with seeing that?

78
00:06:08,218 --> 00:06:10,754
I did it because I was mad!

79
00:06:10,754 --> 00:06:15,792
Then how could someone who was that mad be happy when you found out I was pregnant?

80
00:06:15,792 --> 00:06:18,495
You already knew everything!

81
00:06:21,231 --> 00:06:22,966
Our baby...

82
00:06:25,635 --> 00:06:28,905
does it not matter to you whose baby it is?

83
00:06:28,905 --> 00:06:31,307
As long as the show goes on?

84
00:06:31,307 --> 00:06:35,378
I don't care whose baby it is. I can raise it!

85
00:06:35,378 --> 00:06:39,015
Stop being so greedy!

86
00:06:51,027 --> 00:06:55,999
I've been meaning to say this for a long time.

87
00:06:59,536 --> 00:07:05,141
Please make food that people can eat.

88
00:07:06,342 --> 00:07:09,546
Not food that's just pretty to look at.

89
00:07:09,546 --> 00:07:13,082
But you know, real food that satisfies.

90
00:07:13,082 --> 00:07:16,252
The kind of food you used to make before.

91
00:07:16,252 --> 00:07:21,925
- You were so happy when people enjoyed your food. - Isn't that right?

92
00:07:27,030 --> 00:07:31,100
To come this far,

93
00:07:31,100 --> 00:07:35,839
I've seen all kind of dirty things.

94
00:07:38,041 --> 00:07:42,479
You know that best.

95
00:07:42,479 --> 00:07:45,048
Now we can do the show

96
00:07:46,149 --> 00:07:49,052
and enjoy our life together

97
00:07:50,220 --> 00:07:52,822
Is that so wrong?

98
00:08:02,332 --> 00:08:08,671
Honey, this isn't you.

99
00:08:10,640 --> 00:08:13,243
This isn't the you I used to know.

100
00:08:36,232 --> 00:08:38,401
Is Hwang Dong Seok at home?

101
00:08:42,138 --> 00:08:43,540
What is it?

102
00:08:43,540 --> 00:08:46,376
I just need to ask a couple questions.

103
00:08:46,376 --> 00:08:48,411
You don't need to worry.

104
00:08:49,646 --> 00:08:51,781
What's wrong with my husband?

105
00:08:51,781 --> 00:08:55,184
Honey!

106
00:08:56,019 --> 00:08:57,253
Let go!

107
00:08:57,253 --> 00:09:00,690
Hwang Dong Seok, you're being arrested for the murder of Na Yoon Jeong.

108
00:09:00,690 --> 00:09:03,760
You have the right to engage an attorney.

109
00:09:32,422 --> 00:09:37,427
Framed

110
00:10:49,532 --> 00:10:51,267
Hello?

111
00:11:16,325 --> 00:11:18,428
Do you recognize this?

112
00:11:21,431 --> 00:11:22,832
Where did you..

113
00:11:22,832 --> 00:11:25,501
It was found in the victim's kitchen drawer

114
00:11:25,501 --> 00:11:27,970
along with your husband's fingerprint impression.

115
00:11:27,970 --> 00:11:29,672
My husband's fingerprint?

116
00:11:29,672 --> 00:11:31,607
Yes.

117
00:11:31,607 --> 00:11:34,577
I think the victim intentionally lifted it earlier

118
00:11:34,577 --> 00:11:37,280
to plant it at Na Yoon Jeong's crime scene.

119
00:11:37,280 --> 00:11:42,518
Don't call her the victim yet. My husband is not capable of killing anyone.

120
00:11:55,031 --> 00:11:58,768
Do you recognize it?

121
00:11:59,869 --> 00:12:04,307
If I recognize this, what will happen to Dong Seok?

122
00:12:04,307 --> 00:12:07,844
Hwan Dong Seok and Baek Jae Hee were lovers. This was already revealed.

123
00:12:07,844 --> 00:12:13,149
We've secured the CCTV video of him arguing with the victim at the restaurant. He had more than enough motivation to kill her.

124
00:12:13,149 --> 00:12:18,221
As evidence, we found her cell phone in the kitchen drawer at his restaurant.

125
00:12:18,221 --> 00:12:21,991
The last thing we need to find out is where did the victim lift Kim Yeong Dae's fingerprint?

126
00:12:21,991 --> 00:12:25,628
There was no fingerprint found on this watch. It's possible the victim it wiped it off.

127
00:12:25,628 --> 00:12:29,966
So we need your confirmation

128
00:12:29,966 --> 00:12:32,702
if this belonged to your husband?

129
00:12:47,717 --> 00:12:49,418
Yes.

130
00:12:49,418 --> 00:12:51,821
It is my husband's watch.

131
00:13:10,173 --> 00:13:14,677
So your husband is not a accused anymore since

132
00:13:14,677 --> 00:13:17,313
Han Sang Hoon was not found at the site of the abandoned condo.

133
00:13:17,313 --> 00:13:20,716
I wonder why my husband's fingerprint was planted at the crime scene.

134
00:13:20,716 --> 00:13:27,290
Maybe to add more confusion to the case? A dead man's fingerprint can do that.

135
00:13:33,696 --> 00:13:35,598
What did Dong Seok say? Did he confess to the crime?

136
00:13:35,598 --> 00:13:39,135
He admitted his relationship with Jae Hee but...

137
00:13:39,135 --> 00:13:41,838
denies all charges to the murder.

138
00:13:44,240 --> 00:13:46,242
Dong Seok...

139
00:13:46,242 --> 00:13:48,744
Why do you think he did that?

140
00:13:50,479 --> 00:13:52,748
What are you saying?

141
00:13:55,751 --> 00:14:00,623
What about my husband...do you really think that he killed someone?

142
00:14:07,864 --> 00:14:10,399
I'll go.

143
00:14:10,399 --> 00:14:12,034
Jeong Won!

144
00:14:22,678 --> 00:14:26,883
Don't worry Se Yeon. I'll try to comfort her.

145
00:14:26,883 --> 00:14:28,551
Thanks. I'll leave her to you.

146
00:14:28,551 --> 00:14:32,121
By the way, did something happen to Ye Rin?

147
00:14:32,121 --> 00:14:35,157
You don't look so good.

148
00:14:36,225 --> 00:14:38,761
It's because...

149
00:14:38,761 --> 00:14:41,330
yesterday, social workers came by from the Children's Welfare Services.

150
00:14:41,330 --> 00:14:43,699
Children's Welfare Services?

151
00:14:59,815 --> 00:15:02,051
Jang Se Yeon?

152
00:15:03,953 --> 00:15:07,690
How do you do? You're Jang Se Yeon, right?

153
00:15:08,658 --> 00:15:10,860
Yes I am.

154
00:15:11,961 --> 00:15:14,630
We're from the Children's Welfare Services.

155
00:15:15,231 --> 00:15:18,868
I would like to ask a few questions about your daughter.

156
00:15:23,940 --> 00:15:26,943
What do you want to know?

157
00:15:37,219 --> 00:15:39,588
What are you doing?

158
00:15:39,588 --> 00:15:42,124
There is a bruise on her arm.

159
00:15:42,124 --> 00:15:45,561
This is a needle mark. The nurse mistakenly pierced the needle in the wrong place.

160
00:15:45,561 --> 00:15:49,832
Ye Rin's mother, can we please talk for a few minutes?

161
00:15:57,273 --> 00:15:58,908
Ah..

162
00:15:58,908 --> 00:16:03,679
I heard a pesticide substance was found within Ye Rin's body.

163
00:16:07,283 --> 00:16:11,320
Her babysitter fed it to her little at a time.

164
00:16:11,320 --> 00:16:12,788
Babysitter?

165
00:16:13,789 --> 00:16:16,926
- Excuse me.- Don't worry, it's alright.

166
00:16:16,926 --> 00:16:20,863
Do you still have any more pesticide left?

167
00:16:23,499 --> 00:16:25,234
Say it!!

168
00:16:26,969 --> 00:16:30,606
No... not any more.

169
00:16:30,606 --> 00:16:33,743
Can you provide me the babysitter's contact number?

170
00:16:33,743 --> 00:16:39,148
I can, but why do you want it?

171
00:16:39,148 --> 00:16:42,918
You have insurance on Ye Rin, right?

172
00:16:42,918 --> 00:16:43,986
What?

173
00:16:43,986 --> 00:16:46,088
The pay-out amount is huge.

174
00:16:47,490 --> 00:16:50,993
Did you have insurance on Ye Rin?

175
00:16:51,894 --> 00:16:56,499
Yeong Dae wanted it so I filled out the application form but

176
00:16:56,499 --> 00:17:01,237
he told me that later he changed his mind and bought only mine and his.

177
00:17:01,237 --> 00:17:04,907
That was a good thing. You'll have some insurance money.

178
00:17:04,907 --> 00:17:09,912
And don't worry too much about the investigation. It's probably nothing.

179
00:17:20,656 --> 00:17:22,324
[Ye Rin's dad]

180
00:17:22,324 --> 00:17:27,763
[The number you called, has been disconnected. Please check the number and call again]

181
00:18:19,615 --> 00:18:22,751
It was inconceivable for us to think that

182
00:18:22,751 --> 00:18:27,990
Hwang Dong Seok would plant a dead man's fingerprint at the crime scene.

183
00:18:27,990 --> 00:18:31,994
Sorry to have bothered you.

184
00:18:31,994 --> 00:18:34,697
If you came to apologize, it's okay.

185
00:18:34,697 --> 00:18:37,766
I didn't have much time to think about it anyway.

186
00:18:37,766 --> 00:18:40,069
Oh, that's right...your daughter...

187
00:18:40,870 --> 00:18:44,874
To be honest, we came because of your daughter.

188
00:18:50,446 --> 00:18:52,348
To begin with

189
00:18:53,916 --> 00:19:01,090
after your husband passed away, you've received a huge pay-out from the insurance company.

190
00:19:01,090 --> 00:19:03,325
What's that got to do with my daughter?

191
00:19:03,325 --> 00:19:08,464
Ah..well...we received a request for an investigation.

192
00:19:08,464 --> 00:19:10,533
An Investigation?

193
00:19:10,533 --> 00:19:13,836
[You have a Life Insurance on Ye Rin, right?]

194
00:19:59,348 --> 00:20:02,551
[Life Insurance Application]

195
00:20:05,921 --> 00:20:07,356
What about this...

196
00:20:07,356 --> 00:20:12,027
Did Kim Yeong Dae filled this out himself?

197
00:20:12,895 --> 00:20:15,731
No, I did.

198
00:20:15,731 --> 00:20:20,936
Did you filled this out for your daughter as well?

199
00:20:20,936 --> 00:20:24,907
Yes. My husband thought it was a good idea.

200
00:20:28,244 --> 00:20:31,547
- Mine was also filled out.- I see.

201
00:20:33,015 --> 00:20:38,287
Your husband and Ye Rin's application were processed

202
00:20:38,287 --> 00:20:42,258
but there is no insurance under your name.

203
00:20:42,258 --> 00:20:44,860
That can't be right.

204
00:20:44,860 --> 00:20:48,864
My application was submitted at the same time.

205
00:20:48,864 --> 00:20:51,734
Oh, my husband set-up automatic of payments from his own account.

206
00:20:51,734 --> 00:20:54,837
But this record shows

207
00:20:54,837 --> 00:21:01,777
your insurance was terminated after one month, because there were no further payments made.

208
00:21:01,777 --> 00:21:03,912
That can't be right.

209
00:21:03,912 --> 00:21:06,782
[Insurance Contract]

210
00:21:10,919 --> 00:21:13,455
You might think this is odd but

211
00:21:13,455 --> 00:21:17,526
insurance does not cover pay-out on death of children.

212
00:21:17,526 --> 00:21:21,530
You know more about the insurance than I thought.

213
00:21:21,530 --> 00:21:24,967
I learned a thing or two about it while getting my husband's insurance pay-out.

214
00:21:24,967 --> 00:21:28,437
But the payments on treatments are paid by the insurance on a regular basis.

215
00:21:28,437 --> 00:21:31,307
That's what many criminals are after.

216
00:21:34,176 --> 00:21:37,446
I'm not saying that you are.

217
00:21:43,786 --> 00:21:48,023
Sa-Sang Hee...you're Sang Hee, right?

218
00:21:49,258 --> 00:21:51,794
Don't you remember me?

219
00:21:51,794 --> 00:21:55,030
We played together with Ye Rin at the playground.

220
00:21:55,030 --> 00:21:56,332
Ye Rin?

221
00:21:56,332 --> 00:21:58,701
Yeah, you..you remember me now, right?

222
00:21:58,701 --> 00:22:01,837
I used to push your swing...right?

223
00:22:01,837 --> 00:22:09,578
Sang Hee. Do you see that stick behind me? Can you hand that over to me?

224
00:22:09,578 --> 00:22:11,280
Come over here...

225
00:22:12,081 --> 00:22:13,649
Please...

226
00:22:28,897 --> 00:22:33,202
Let's untie this quickly so that we can go play with Ye Rin.

227
00:22:33,202 --> 00:22:35,671
Come here...come over here and help me.

228
00:22:40,642 --> 00:22:43,212
That's right...keep coming.

229
00:22:52,721 --> 00:22:57,025
You know Han Sang Hoon, right?

230
00:23:01,029 --> 00:23:05,100
Yes. His daughter and mine were friends so we got to know each other.

231
00:23:05,100 --> 00:23:08,670
It didn't seem like you were just casual friends.

232
00:23:10,939 --> 00:23:13,175
We were close..

233
00:23:13,175 --> 00:23:16,678
Lately, you've been distanced...what's the reason?

234
00:23:20,983 --> 00:23:24,520
Did you know that he was an insurance investigator from the beginning?

235
00:23:25,988 --> 00:23:28,891
No, I didn't know that at first.

236
00:23:28,891 --> 00:23:32,928
But you found out later.

237
00:23:34,129 --> 00:23:37,032
Is that why you stayed away from him?

238
00:23:40,169 --> 00:23:45,007
You know that he's been missing, right?

239
00:23:47,409 --> 00:23:49,211
Yes. I know.

240
00:23:49,211 --> 00:23:52,147
You knew when he was missing.

241
00:23:52,147 --> 00:23:55,851
Why do you keep asking me about him?

242
00:23:55,851 --> 00:24:01,056
We heard from his co-worker that he's been missing

243
00:24:01,056 --> 00:24:04,927
right after he'd gone to meet someone.

244
00:24:09,231 --> 00:24:11,266
Who?

245
00:24:11,967 --> 00:24:13,869
You.

246
00:25:32,247 --> 00:25:33,949
[Ah Yeon's Dad]

247
00:25:42,157 --> 00:25:45,827
Se Yeon, just listen.

248
00:25:45,827 --> 00:25:47,996
Don't say a word.

249
00:25:47,996 --> 00:25:51,934
Are the police there?

250
00:25:51,934 --> 00:25:56,538
If they are next to you, just say, hello.

251
00:25:56,538 --> 00:25:58,540
Hello?

252
00:25:58,540 --> 00:26:00,676
Very good.

253
00:26:01,610 --> 00:26:03,779
- Go out to the hallway to talk to me.- Ah...

254
00:26:03,779 --> 00:26:05,614
Don't say anything.

255
00:26:05,614 --> 00:26:08,584
Get out of there. Hurry.

256
00:26:09,685 --> 00:26:11,353
- Oh...

257
00:26:13,188 --> 00:26:16,792
Sorry. I need to take this call.

258
00:26:16,792 --> 00:26:18,660
Hello?

259
00:26:27,235 --> 00:26:29,271
- Is this really Sang Hoo?- Yes.

260
00:26:29,271 --> 00:26:32,240
I just ran away from Kim Yeong Dae.

261
00:26:32,240 --> 00:26:36,411
I found my cell phone but it's not working well.

262
00:26:36,411 --> 00:26:39,848
- Hello? - Where are you?

263
00:26:39,848 --> 00:26:44,453
I don't know. But right now, the most important thing is you.

264
00:26:45,954 --> 00:26:49,958
Listen carefully to what I'm telling you.

265
00:26:51,860 --> 00:26:54,496
You need to get out of there right now.

266
00:26:54,496 --> 00:26:57,833
I heard everything from Kim Yeong Dae while I was kidnapped by him.

267
00:26:57,833 --> 00:27:01,203
He is the one who called the police.

268
00:27:01,203 --> 00:27:05,574
We have to prove that I'm alive.

269
00:27:08,010 --> 00:27:14,316
Or else, you'll be accused of killing your husband for insurance money,

270
00:27:17,252 --> 00:27:20,389
and killing me to silence me.

271
00:27:20,389 --> 00:27:22,491
We need to move fast.

272
00:27:22,491 --> 00:27:25,961
If you get arrested, we'll lose the opportunity to reveal the truth.

273
00:27:26,728 --> 00:27:30,699
You need to get away from there immediately. You got it?

274
00:27:31,933 --> 00:27:36,038
Why is she talking so long?

275
00:27:36,972 --> 00:27:39,408
You think she ran away?

276
00:27:39,408 --> 00:27:43,211
Her daughter is here. How could she..

277
00:27:43,211 --> 00:27:46,381
She fed her daughter pesticide.

278
00:27:46,381 --> 00:27:49,251
Of course she could run away to evade getting caught.

279
00:27:52,587 --> 00:27:54,222
Wait here.

280
00:28:03,298 --> 00:28:05,200
Check over there.

281
00:28:12,074 --> 00:28:13,809
What should I do?

282
00:28:13,809 --> 00:28:16,078
I know where Kim Yeong Dae is.

283
00:28:16,078 --> 00:28:19,581
I'm got hurt a bit...

284
00:28:19,581 --> 00:28:21,149
Could you come here?

285
00:28:21,149 --> 00:28:23,585
Okay. I'll be right there.

286
00:28:34,129 --> 00:28:36,698
Oh, where are you going?

287
00:28:36,698 --> 00:28:41,937
Oh! Oh! Oh! Damn!

288
00:28:42,838 --> 00:28:46,274
Jang Se Yeon! Stop right there!

289
00:28:46,274 --> 00:28:48,910
Jang Se Yeon!

290
00:29:13,168 --> 00:29:15,837
Jang Se Yeon! Get out of the car!

291
00:29:15,837 --> 00:29:17,806
Jang Se Yeon! Don't do this!

292
00:29:17,806 --> 00:29:21,810
Jang Se Yeon! Get out! Get out!

293
00:29:21,810 --> 00:29:25,781
Jang Se Yeon!

294
00:29:38,727 --> 00:29:44,299
It'll be hard to get away by car. Taking the public transit will be better.

295
00:29:56,144 --> 00:29:58,413
No reception...

296
00:30:28,643 --> 00:30:29,911
Hey, isn't that Jang Se Yeon?

297
00:30:29,911 --> 00:30:33,481
Oh...oh...I think so.

298
00:30:33,481 --> 00:30:35,016
Get close to her.

299
00:30:35,016 --> 00:30:37,252
Stop the car.

300
00:30:39,821 --> 00:30:41,623
Jang Se Yeon!

301
00:30:56,104 --> 00:30:59,841
Get her! Go!

302
00:30:59,841 --> 00:31:03,912
Sorry! Sorry!

303
00:31:08,783 --> 00:31:11,353
- Get in there.- Okay.

304
00:31:57,332 --> 00:31:58,900
Jang Se Ye-

305
00:32:02,637 --> 00:32:05,340
You're making this bigger than it is! Get off!

306
00:33:02,864 --> 00:33:04,032
Hello?

307
00:33:04,032 --> 00:33:06,568
Were you able to turn the police away?

308
00:33:06,568 --> 00:33:11,239
- Yes.- Really? You did well. I thought they'd chase you all the way.

309
00:33:15,643 --> 00:33:17,712
They're not here. I've managed to turn them away.

310
00:33:17,712 --> 00:33:20,248
Good job.

311
00:33:20,248 --> 00:33:22,984
Very good...so now,

312
00:33:23,885 --> 00:33:27,389
go home, pick up your bank book and withdraw the insurance money!!

313
00:33:28,623 --> 00:33:31,059
You don't have much time, so hurry. The police will come after you again, soon.

314
00:33:31,059 --> 00:33:32,994
Who are you?

315
00:33:36,097 --> 00:33:37,999
Yeong Dae?

316
00:33:42,137 --> 00:33:47,308
There are many good folks in this world.

317
00:33:47,308 --> 00:33:50,311
What are you doing to me?

318
00:33:50,311 --> 00:33:54,015
Careful how you act...people are watching you.

319
00:33:54,015 --> 00:33:58,453
And what about you? What do you think you're doing to Ye Rin?

320
00:33:58,453 --> 00:34:02,590
What do you mean...Park Jeong Sim--

321
00:34:05,894 --> 00:34:08,563
You two are in this together. Right?

322
00:34:10,265 --> 00:34:15,570
The most important thing Ye Rin. She needs treatments. Don't you agree?

323
00:34:17,038 --> 00:34:19,074
I'm going to get you!

324
00:34:19,074 --> 00:34:22,310
How can you...you're too busy running away.

325
00:34:22,310 --> 00:34:25,380
And all that time, Ye Rin will be all alone.

326
00:34:25,380 --> 00:34:31,152
I just need to prove that you're alive. Then the ridiculous charges of killing a husband for insurance money will be--

327
00:34:31,152 --> 00:34:38,993
Hey, there's no proof of me being alive anywhere. Besides, in your situation, no one will believe you anyway.

328
00:34:41,296 --> 00:34:43,998
Hwa Yeong said she saw you.

329
00:34:43,998 --> 00:34:51,840
But she's your friend. They'll think that she's lying for a friend...

330
00:34:51,840 --> 00:34:58,179
Okay...Go on, tell the police that you're running away because you were duped by Kim Yeong Dae and that he is alive.

331
00:34:58,179 --> 00:35:02,650
Then, I'll get arrested shortly and you'll have to return

332
00:35:02,650 --> 00:35:06,087
all of the insurance money...20 million won. Oh I forgot,..

333
00:35:06,087 --> 00:35:08,790
you'll also be charged for insurance fraud. That means

334
00:35:08,790 --> 00:35:13,528
all of payments for Ye Rin's treatments will be cancelled.

335
00:35:13,528 --> 00:35:15,897
This is a good idea, right?

336
00:35:17,932 --> 00:35:23,438
Be reasonable. If you entrust me with the money

337
00:35:24,405 --> 00:35:27,909
I'll make sure Ye Rin gets her treatments.

338
00:35:30,879 --> 00:35:34,682
And what if I don't agree? What if I can't?

339
00:35:34,682 --> 00:35:37,552
Who do you think helped you at the hospital earlier?

340
00:35:37,552 --> 00:35:42,023
Stop..stop right there!

341
00:35:49,330 --> 00:35:54,936
Getting in the hospital is a piece of cake for me.

342
00:35:54,936 --> 00:35:57,038
What are you trying to say?

343
00:35:57,038 --> 00:35:59,941
What I'm saying is...

344
00:36:01,476 --> 00:36:08,850
getting rid of a child without a trace is not even work for me.

345
00:36:13,788 --> 00:36:18,393
If you ever hurt her...even as much as a strand of her hair

346
00:36:18,393 --> 00:36:22,197
I'll chase you down all the way to hell!

347
00:36:23,097 --> 00:36:29,003
Se Yeon..Se Yeon... do you really think that I'll hurt my own child?

348
00:36:29,938 --> 00:36:32,473
But did you know...

349
00:36:32,473 --> 00:36:38,813
even the animals will tare their offspring to pieces when driven to the edge?

350
00:36:43,585 --> 00:36:46,721
There's no time. Go and get the money.

351
00:37:00,635 --> 00:37:03,571
Se Yeon will never do that.

352
00:37:03,571 --> 00:37:07,108
Everything says the same thing.

353
00:37:07,108 --> 00:37:11,279
Family members and friends can do no wrong.

354
00:37:11,279 --> 00:37:14,015
But Se Yeon is different.

355
00:37:14,015 --> 00:37:17,285
Yes. I hear that a lot too.

356
00:37:18,753 --> 00:37:23,858
Anyway, in case you hear from her

357
00:37:23,858 --> 00:37:27,762
give us a call first. Okay?

358
00:37:29,364 --> 00:37:32,300
(Lim Dae Soo, Seoul Police Division)

359
00:37:35,536 --> 00:37:38,706
Excuse me. Where is the restroom?

360
00:37:55,623 --> 00:37:58,660
Hi Se Yeon. It's me. Where are you?

361
00:37:59,861 --> 00:38:04,599
The police came by. Let's meet up. Where are you?

362
00:39:57,612 --> 00:40:01,082
Eun Soo told me briefly about what has happened to you.

363
00:40:03,918 --> 00:40:05,820
Sae Yeon.

364
00:40:06,754 --> 00:40:09,791
Did you buy Yeong Dae and Ye Rin's life insurance?

365
00:40:13,361 --> 00:40:19,100
No matter what, I trust you. I'm sure you have your reason.

366
00:40:20,902 --> 00:40:26,607
So tell us straight out, how we can help you?

367
00:40:40,721 --> 00:40:42,757
So that jerk, Kim Yeong Dae

368
00:40:42,757 --> 00:40:47,228
schemed all this to steal the insurance money from you.

369
00:40:47,829 --> 00:40:51,833
It's not easy to kill a spouse and get the insurance money

370
00:40:52,500 --> 00:40:55,403
so, instead, he feined his own death

371
00:40:57,638 --> 00:41:00,608
Let's go and get the money from the bank first.

372
00:41:00,608 --> 00:41:04,579
What for? If you're worried about Ye Rin, just discharge her from the hospital.

373
00:41:04,579 --> 00:41:06,481
Ye Rin needs to receive treatments.

374
00:41:06,481 --> 00:41:11,352
That hospital is best known for surgeries and treatments.

375
00:41:11,352 --> 00:41:14,255
Are you going to give him the money?

376
00:41:14,255 --> 00:41:17,692
No. I'm going to use the money for bait.

377
00:41:18,759 --> 00:41:20,895
I'm going to get catch that jerk

378
00:41:23,998 --> 00:41:26,400
and protect my Ye Rin.

379
00:41:42,283 --> 00:41:44,886
I just arrived at the hospital.

380
00:42:04,872 --> 00:42:06,507
Are you alright?

381
00:42:06,507 --> 00:42:09,644
...oh...my fruit...

382
00:42:12,413 --> 00:42:14,782
...over there, behind you...

383
00:42:16,050 --> 00:42:17,785
...there too...

384
00:42:47,048 --> 00:42:52,119
After that, I caught ten criminals all by myself.

385
00:42:52,119 --> 00:42:55,556
Really? That's great. You're awesome!

386
00:42:55,556 --> 00:42:56,824
You're the best!

387
00:42:56,824 --> 00:42:58,659
Ahh...I'm embarrassed.

388
00:43:07,234 --> 00:43:10,037
And then?

389
00:43:10,037 --> 00:43:12,106
- I did some boxing too.- Boxing?- Wow...

390
00:43:12,106 --> 00:43:14,208
That's why I'm not afraid of any criminals.

391
00:43:14,208 --> 00:43:16,777
No wonder, the way you were chasing that apple...

392
00:43:16,777 --> 00:43:18,613
you were really brave.

393
00:43:18,613 --> 00:43:19,914
I'm very fast, huh.

394
00:43:19,914 --> 00:43:24,318
Ah, I have to go now. Thank you so much for today.

395
00:43:28,823 --> 00:43:31,792
Wait a minute! Your fruit basket...

396
00:43:32,760 --> 00:43:34,795
Hey!

397
00:43:34,795 --> 00:43:37,064
What are you doing?

398
00:43:37,064 --> 00:43:39,734
A young lady left her fruit basket.

399
00:43:39,734 --> 00:43:44,872
Fruit basket? What are you talking about!

400
00:43:44,872 --> 00:43:46,807
What lady?

401
00:43:58,019 --> 00:44:01,055
I told you to keep watch here.

402
00:44:01,055 --> 00:44:02,556
Wasn't it Jang Se Yeon?

403
00:44:02,556 --> 00:44:04,025
No.

404
00:44:04,592 --> 00:44:06,927
Then who?

405
00:44:12,233 --> 00:44:13,868
Did you get it?

406
00:44:16,804 --> 00:44:18,906
I think I bought the wrong one; I was running...

407
00:44:18,906 --> 00:44:20,708
Well. you were in a hurry.

408
00:44:20,708 --> 00:44:22,610
Let's go.

409
00:44:29,917 --> 00:44:32,820
oh, could you rewind a bit?

410
00:44:34,321 --> 00:44:36,791
Yes, right there. Stop.

411
00:44:44,832 --> 00:44:47,568
Stand by Jang Se Yeon's bank.

412
00:44:47,568 --> 00:44:49,870
I think she went to get the money.

413
00:45:03,718 --> 00:45:06,420
[Resident Registration]

414
00:45:07,521 --> 00:45:10,791
Please wait a moment while I take care of this.

415
00:45:38,552 --> 00:45:40,721
May I have your PIN?

416
00:45:57,004 --> 00:46:01,242
Oh my gosh!! Why are they on the phone for so long?

417
00:46:01,242 --> 00:46:05,346
I checked the home page...do you have something better?

418
00:46:05,346 --> 00:46:07,915
About 10 million won...

419
00:46:07,915 --> 00:46:12,153
Doesn't matter when it expires...how about the tax write-off?

420
00:46:12,153 --> 00:46:13,387
Damn!

421
00:46:13,387 --> 00:46:15,222
This is driving me crazy!

422
00:46:15,222 --> 00:46:18,759
Why don't you call the branch manager?

423
00:46:46,687 --> 00:46:48,789
It's all ready.

424
00:46:51,959 --> 00:46:54,028
But this is a huge sum

425
00:46:54,028 --> 00:46:57,164
my employee will escort you safely to your car.

426
00:46:57,164 --> 00:47:00,835
Oh, it's okay, thank you though.

427
00:47:02,469 --> 00:47:04,171
Thank you.

428
00:48:43,737 --> 00:48:45,439
Yeong Dae!

429
00:48:50,277 --> 00:48:51,946
Are you okay?

430
00:48:51,946 --> 00:48:52,980
Did you get his picture?

431
00:48:52,980 --> 00:48:54,848
It's didn't turn out clear.

432
00:48:55,916 --> 00:48:59,186
- Yeah, Eun Soo...number 3 exit!- Number 3?...

433
00:49:55,642 --> 00:49:57,177
- Hello? - Where are you?

434
00:49:57,177 --> 00:49:59,813
Okay. we'll head there now.

435
00:50:49,430 --> 00:50:53,200
- Did he get away?- I attached a tracer in the bag.

436
00:52:12,045 --> 00:52:13,814
Hey,

437
00:52:24,825 --> 00:52:27,060
Please untie me.

438
00:52:48,849 --> 00:52:50,450
[Hotel]

439
00:52:50,450 --> 00:52:53,620
[The second keyword is insurance fraud]

440
00:52:53,620 --> 00:52:57,758
[Insurance fraud is on the rise as we have reported previously]

441
00:52:57,758 --> 00:53:00,694
[This horrible scam has happened again]

442
00:53:00,694 --> 00:53:06,433
[This time, a little girl was fed a pesticide substance called Malathion to claim the insurance money]

443
00:53:06,433 --> 00:53:08,635
[Today, the culprit was caught]

444
00:53:08,635 --> 00:53:11,939
[Reporter Kim, please tell us what happened in detail]

445
00:53:11,939 --> 00:53:18,145
The way Ms. Jang's crime was caught is due to the hospitalization of her daughter Kim.

446
00:53:18,145 --> 00:53:22,716
Kim who was being examined for acute leukemia and during the exam...

447
00:53:22,716 --> 00:53:27,120
Pesticide with malic acid was found within her body.

448
00:53:27,120 --> 00:53:30,257
You're saying that the pesticide was found in her body, right?

449
00:53:30,257 --> 00:53:31,358
Yes, that's right.

450
00:53:31,358 --> 00:53:35,596
There was another incident involving pesticide for insurance scam before.

451
00:53:35,596 --> 00:53:40,133
There's a reason why this Ms. Jang incident is being brought to much attention this time is because...

452
00:53:40,133 --> 00:53:45,839
we have discovered that this isn't the first time Ms. Jang committed insurance fraud.

453
00:53:45,839 --> 00:53:48,408
Ms. Jang who is accused of killing her husband

454
00:53:48,408 --> 00:53:52,679
received a little over 2 billion won.

455
00:53:52,679 --> 00:53:56,717
But Ms. Jang's insurance fraud scam didn't end here.

456
00:53:56,717 --> 00:54:01,188
She poisoned her daughter this time and tried to take the insurance money.

457
00:54:01,188 --> 00:54:04,625
This is the second case of an insurance scam by a housewife.

458
00:54:04,625 --> 00:54:07,694
The police found out about it today.

459
00:54:07,694 --> 00:54:10,931
But I heard that she was able to get away unfortunately.

460
00:54:10,931 --> 00:54:12,266
Yes, that's right.

461
00:54:12,266 --> 00:54:18,105
When Ms. Jang realized she was being tracked down, she took the entire life insurance amount and ran away.

462
00:54:20,874 --> 00:54:26,546
The second "Mother" insurance fraud incident

463
00:54:36,523 --> 00:54:38,625
If that a**hole is still alive, we have to reveal the truth somehow.

464
00:54:38,625 --> 00:54:42,696
Se Yeon, do you have a recording of your conversation with Kim Yeong Dae?

465
00:54:44,231 --> 00:54:46,500
Yeah, I recorded our last conversation.

466
00:54:46,500 --> 00:54:48,268
Give it to me.

467
00:54:49,336 --> 00:54:54,074
But the voice was distorted, so you won't be able to tell if it's him.

468
00:54:55,442 --> 00:54:57,477
Hangook University Hospital

469
00:55:03,917 --> 00:55:07,187
Go home, get the bankbook, and go get the insurance money.

470
00:55:07,187 --> 00:55:11,425
- We don't have time. Don't let the police...  - I really can't tell...

471
00:55:14,928 --> 00:55:20,167
Don't worry. He's human being. I'm sure he left some type of trace somewhere whether it's CCTV or what.

472
00:55:28,942 --> 00:55:33,447
Right. If we reveal that a**hole is still alive, Se Yeon...

473
00:55:33,447 --> 00:55:37,184
You can clear your name of both murder and insurance fraud in one shot.

474
00:55:45,425 --> 00:55:47,627
What do you want?

475
00:55:48,829 --> 00:55:52,766
I'm the guardian of the patient in this room.

476
00:55:52,766 --> 00:55:57,237
What? If you're Ye Rin's guardian...

477
00:56:19,826 --> 00:56:21,528
Ye Rin...

478
00:56:23,230 --> 00:56:28,769
Ye Rin, daddy's here.

479
00:56:30,637 --> 00:56:32,139
Ye Rin...

480
00:56:33,740 --> 00:56:36,910
What's happening? Kim Yeong Dae is alive.

481
00:57:29,663 --> 00:57:31,832
Mistress

482
00:57:31,832 --> 00:57:36,136
My wife tried to drown me.

483
00:57:36,136 --> 00:57:39,539
You just have to reveal that Kim Yeong Dae is alive.

484
00:57:39,539 --> 00:57:42,642
In Sang Hoon's notebook... I'm sure there's a clue that leads to him.

485
00:57:42,642 --> 00:57:44,578
You'll have to help us.

486
00:57:44,578 --> 00:57:47,214
Before anything happens to Sang Hoon, let's find him first.

487
00:57:47,214 --> 00:57:49,616
We'll find Park Jeong Sim in that time.

488
00:57:49,616 --> 00:57:52,118
If I had ended it then...

489
00:57:52,118 --> 00:57:55,555
Things wouldn't be so dangerous like this.

490
00:58:01,828 --> 00:58:09,035
♫ Will you just hold me tight? ♫

491
00:58:10,837 --> 00:58:18,245
♫ Will you just quietly comfort me? ♫

492
00:58:19,746 --> 00:58:29,823
♫ Because I don’t care whether or not you know my heart ♫

