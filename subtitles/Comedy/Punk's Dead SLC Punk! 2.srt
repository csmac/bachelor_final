﻿1
00:00:29,646 --> 00:00:38,186
I didn't want to be the one to forget
I thought of everything I'd never regret

2
00:00:38,186 --> 00:00:46,956
A little time with you is all that I get
That’s all we need because it's all we can take

3
00:00:46,956 --> 00:00:55,662
One thing I never see the same when your 'round
I don't believe in him - his lips on ground

4
00:00:55,662 --> 00:01:04,475
I wanna take you to that place in the "Roche"
But no one gives us any time anymore

5
00:01:04,475 --> 00:01:13,138
He asks me once if I'd look in on his dog
You made an offer for it then you ran off

6
00:01:13,138 --> 00:01:21,832
I got this picture of us kids in my head
And all I hear is the last thing that you said

7
00:01:21,832 --> 00:01:31,065
"I listened to your problems now listen to mine"
I didn't want to anymore

8
00:01:31,065 --> 00:01:35,478
And we will never be alone again
'Cause it doesn't happen every day

9
00:01:35,478 --> 00:01:39,904
Kinda counted on you being a friend
Can I give it up or give it away

10
00:01:39,904 --> 00:01:44,174
Now I thought about what I wanna say
But I never really know where to go

11
00:01:44,174 --> 00:01:48,546
So I chained myself to a friend
'Cause I know it unlocks like a door

12
00:01:48,546 --> 00:01:52,852
And we will never be alone again
'Cause it doesn't happen every day

13
00:01:52,852 --> 00:01:57,553
Kinda counted on you being a friend
Can I give it up or give it away

14
00:01:57,553 --> 00:02:01,598
Now I thought about what I wanna say
But I never really know where to go

15
00:02:01,598 --> 00:02:04,745
So I chained myself to a friend

16
00:02:04,745 --> 00:02:08,430
Some more again

17
00:02:14,456 --> 00:02:22,867
It didn't matter what they wanted to see
He thought he saw someone that looked just like me

18
00:02:22,867 --> 00:02:31,657
The summer memory that just never dies
We worked too long and hard to give it no time

19
00:02:31,657 --> 00:02:40,441
He sees right through me, it's so easy with lies
Cracks in the road that I would try and disguise

20
00:02:40,441 --> 00:02:49,152
He runs the scissors at the seam in the wall
He cannot break it down or else he would fall

21
00:02:49,152 --> 00:03:06,099
One thousand lonely stars hiding in the cold
Take it, I don't wanna sing anymore

22
00:03:23,821 --> 00:03:27,455
"I listened to your problems now listen to
mine"

23
00:03:27,455 --> 00:03:33,133
I didn't want to anymore

24
00:03:33,133 --> 00:03:37,798
And we will never be alone again
'Cause it doesn't happen every day

25
00:03:37,798 --> 00:03:42,148
Kinda counted on you being a friend
Can I give it up or give it away

26
00:03:42,148 --> 00:03:46,511
Now I thought about what I wanna say
But I never really know where to go

27
00:03:46,511 --> 00:03:50,851
So I chained myself to a friend
'Cause I know it unlocks like a door

28
00:03:50,851 --> 00:03:55,212
And we will never be alone again
'Cause it doesn't happen every day

29
00:03:55,212 --> 00:03:59,767
Kinda counted on you being a friend
Can I give it up or give it away

30
00:03:59,767 --> 00:04:03,886
Now I thought about what I wanna say
But I never really know where to go

31
00:04:03,886 --> 00:04:08,039
So I chained myself to a friend
'Cause I know it unlocks like a door

32
00:04:08,039 --> 00:04:16,252
I don't understand, don't get upset
I'm not with you

33
00:04:16,252 --> 00:04:24,324
We’re swimming around,
It's all I do, when I'm with you

34
00:04:25,648 --> 00:04:30,182
And we will never be alone again
'Cause it doesn't happen every day

35
00:04:30,182 --> 00:04:34,568
Kinda counted on you being a friend
Can I give it up or give it away

36
00:04:34,568 --> 00:04:38,850
Now I thought about what I wanna say
But I never really know where to go

37
00:04:38,850 --> 00:04:43,235
So I chained myself to a friend
'Cause I know it unlocks like a door

38
00:04:43,235 --> 00:04:47,563
And we will never be alone again
'Cause it doesn't happen every day

39
00:04:47,563 --> 00:04:51,988
Kinda counted on you being a friend
Can I give it up or give it away

40
00:04:51,988 --> 00:04:56,279
Now I thought about what I wanna say
But I never really know where to go

41
00:04:56,279 --> 00:05:00,743
So I chained myself to a friend
'Cause I know it unlocks like a door

42
00:05:00,768 --> 00:05:07,068
Ali Moghadam
mr_ali_moghadam@yahoo.com