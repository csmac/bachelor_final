1
00:01:11,671 --> 00:01:13,263
Oh...

2
00:01:17,210 --> 00:01:19,201
So you're at a movie too.

3
00:01:20,580 --> 00:01:22,445
What are you eating?

4
00:01:24,217 --> 00:01:26,515
You know the noises
people make in cinemas...

5
00:01:26,586 --> 00:01:30,386
eating potato chips,
crumpling wrappers.

6
00:01:32,926 --> 00:01:36,259
I really can't stand it--

7
00:01:49,309 --> 00:01:51,539
Are they good?

8
00:01:51,611 --> 00:01:54,375
Sure. Curry-flavored.

9
00:01:55,315 --> 00:01:59,775
I'll kill you if you make that noise
once the movie starts.

10
00:02:00,587 --> 00:02:04,045
Understand?

11
00:02:06,659 --> 00:02:08,058
And...

12
00:02:08,128 --> 00:02:12,292
I also don't like
watch alarms going off.

13
00:02:12,365 --> 00:02:16,267
I don't want interruptions.

14
00:02:17,804 --> 00:02:21,763
They say when you die
you see something like a movie.

15
00:02:22,242 --> 00:02:25,700
A life kaleidoscoped into a few seconds

16
00:02:26,312 --> 00:02:29,042
I look forward to that movie.

17
00:02:29,115 --> 00:02:33,518
A man's last movie. I definitely
don't want it interrupted.

18
00:02:35,455 --> 00:02:38,720
"Darling, don't die!" and tears...

19
00:02:38,791 --> 00:02:40,884
I can do without that.

20
00:02:41,694 --> 00:02:45,790
Hey, our movie's starting.

21
00:02:48,868 --> 00:02:50,529
Tsutomu Yamazaki

22
00:02:50,603 --> 00:02:52,230
Nobuko Miyamoto

23
00:02:52,305 --> 00:02:53,738
Koji Yakusho

24
00:03:00,980 --> 00:03:04,575
in Juzo Itami's

25
00:03:12,859 --> 00:03:20,265
Tampopo (Dandelion)

26
00:03:27,674 --> 00:03:29,505
One fine day...

27
00:03:29,576 --> 00:03:32,670
I went out with an old man.

28
00:03:34,314 --> 00:03:36,179
He's studied noodles for 40 years.

29
00:03:36,649 --> 00:03:40,551
He was showing me the right way
to eat them.

30
00:03:45,992 --> 00:03:47,220
Master...

31
00:03:47,293 --> 00:03:50,421
soup first or noodles first?

32
00:03:50,897 --> 00:03:53,730
First, observe the whole bowl.

33
00:03:54,667 --> 00:03:56,157
Yes, sir.

34
00:03:56,236 --> 00:04:01,731
Appreciate its gestalt.
Savor the aromas.

35
00:04:03,009 --> 00:04:06,604
Jewels of fat glittering
on the surface.

36
00:04:08,581 --> 00:04:11,311
Shinachiku roots shining.

37
00:04:11,384 --> 00:04:15,616
Seaweed slowly sinking.

38
00:04:15,688 --> 00:04:19,146
Spring onions floating.

39
00:04:19,225 --> 00:04:23,753
Concentrate on the three pork slices...

40
00:04:23,830 --> 00:04:28,028
They play the key role,
but stay modestly hidden.

41
00:04:29,302 --> 00:04:32,999
First caress the surface...

42
00:04:34,040 --> 00:04:40,138
with the chopstick tips.

43
00:04:41,781 --> 00:04:43,305
What for?

44
00:04:43,783 --> 00:04:47,048
To express affection.

45
00:04:47,120 --> 00:04:49,213
I see.

46
00:04:49,289 --> 00:04:53,521
Then poke the pork.

47
00:04:53,593 --> 00:04:55,458
Eat the pork first?

48
00:04:55,928 --> 00:04:58,488
No. Just touch it.

49
00:04:58,564 --> 00:05:01,658
Caress it with the chopstick tips.

50
00:05:01,968 --> 00:05:03,765
Gently pick it up...

51
00:05:03,836 --> 00:05:09,240
and dip it into the soup
on the right of the bowl.

52
00:05:09,309 --> 00:05:12,301
What's important here is to...

53
00:05:12,745 --> 00:05:18,206
apologize to the pork by saying...

54
00:05:19,085 --> 00:05:21,246
"See you soon."

55
00:05:21,321 --> 00:05:23,619
"See you soon"?
What a fool.

56
00:05:23,690 --> 00:05:25,715
What a book!

57
00:05:25,792 --> 00:05:27,726
Makes me hungry for noodles.

58
00:05:27,794 --> 00:05:30,422
Hang on. Only two more hours.

59
00:05:30,897 --> 00:05:32,797
Let's finish and then eat.

60
00:05:32,865 --> 00:05:34,560
Shall we?

61
00:05:34,634 --> 00:05:35,931
Keep reading.

62
00:05:36,969 --> 00:05:40,837
Finally start eating, the noodles first.

63
00:05:47,914 --> 00:05:52,044
Oh, at this time...

64
00:05:52,118 --> 00:05:58,250
while slurping the noodles,
look at the pork.

65
00:05:58,324 --> 00:06:00,053
Yes.

66
00:06:01,861 --> 00:06:04,625
Eye it affectionately.

67
00:06:07,900 --> 00:06:12,132
The old man bit some shinachiku root
and chewed it awhile.

68
00:06:12,939 --> 00:06:15,931
Then he took some noodles.

69
00:06:17,043 --> 00:06:23,448
Still chewing noodles,
he took some more shinachiku.

70
00:06:26,119 --> 00:06:28,451
Then he sipped some soup.

71
00:06:31,224 --> 00:06:33,249
Three times.

72
00:06:33,659 --> 00:06:35,490
He sat up...

73
00:06:39,265 --> 00:06:40,892
sighed, picked up...

74
00:06:40,967 --> 00:06:44,198
one slice of pork as if making
a major decision in life...

75
00:06:44,270 --> 00:06:48,468
and lightly tapped it
on the side of the bowl.

76
00:06:48,541 --> 00:06:50,406
What for?

77
00:06:51,744 --> 00:06:54,338
To drain it. That's all.

78
00:07:00,520 --> 00:07:01,748
Let's stop.

79
00:07:01,821 --> 00:07:02,719
Why?

80
00:07:04,624 --> 00:07:07,525
Your foolish book is making me hungry.

81
00:07:07,593 --> 00:07:10,187
"Lai Lai Noodle Restaurant"

82
00:07:11,831 --> 00:07:14,299
Looks like a naruto and noritype.

83
00:07:14,367 --> 00:07:15,959
Yeah, shoyu -flavored.

84
00:07:16,035 --> 00:07:17,400
Kind of light.

85
00:07:22,208 --> 00:07:24,073
Hey, what's this?

86
00:07:34,187 --> 00:07:36,052
You okay?

87
00:07:37,356 --> 00:07:39,085
Do you live here?

88
00:07:41,360 --> 00:07:42,554
Cheer up.

89
00:08:04,884 --> 00:08:06,476
Welcome.

90
00:08:10,723 --> 00:08:11,712
Pork in mine.

91
00:08:11,791 --> 00:08:13,258
Special for me.

92
00:08:15,828 --> 00:08:17,489
That's a bad sign.

93
00:08:17,563 --> 00:08:18,655
Huh?

94
00:08:18,731 --> 00:08:20,494
It's not boiling.

95
00:08:30,376 --> 00:08:33,004
Hey, kid, you'll catch cold.

96
00:08:34,046 --> 00:08:36,241
Oh, you're home, Tabo.

97
00:08:36,315 --> 00:08:39,443
What's wrong? Got beat up again?

98
00:08:41,320 --> 00:08:45,723
Who was it this time, Ryuta?

99
00:08:46,526 --> 00:08:50,326
You're soaked. Go dry and change.

100
00:08:50,997 --> 00:08:54,057
I'll say it again. Leave this dump.

101
00:08:54,667 --> 00:08:58,296
Move in with me.
That kid won't be bullied any more.

102
00:09:04,243 --> 00:09:06,268
Come with me to Paris.

103
00:09:06,846 --> 00:09:10,043
I'll buy you furs, jewelry, anything.

104
00:09:11,083 --> 00:09:13,517
You're so stubborn.

105
00:09:13,586 --> 00:09:15,019
Pork for you.

106
00:09:15,788 --> 00:09:17,517
Special for you.

107
00:09:18,591 --> 00:09:20,525
Special - like hell.

108
00:09:20,593 --> 00:09:22,584
Why use smelly naruto ?

109
00:09:23,329 --> 00:09:28,392
You're out of date.
That's why business is bad.

110
00:09:28,467 --> 00:09:30,298
Why don't you quit?

111
00:09:30,369 --> 00:09:31,961
I'll help you.

112
00:09:32,038 --> 00:09:33,630
Shut up.

113
00:09:33,706 --> 00:09:36,174
What I do is my business.

114
00:09:36,242 --> 00:09:38,437
My shop has nothing to do with you.

115
00:09:38,511 --> 00:09:41,071
I'm just trying to be helpful.

116
00:09:41,447 --> 00:09:43,347
I'm offering to buy you out
of this crummy shop.

117
00:09:43,416 --> 00:09:47,682
Hey, you, be quiet.
We're trying to eat.

118
00:09:47,753 --> 00:09:48,981
Stay out of this.

119
00:09:49,055 --> 00:09:50,989
Hey, Ken.
I'm sorry.

120
00:09:51,057 --> 00:09:55,460
Stop it, Pisken.
Leave my customers alone.

121
00:09:55,528 --> 00:09:57,257
Customer!

122
00:09:57,663 --> 00:10:00,655
You don't need smelly truck drivers.

123
00:10:00,733 --> 00:10:02,132
- Stop it!
- Shut up!

124
00:10:06,305 --> 00:10:07,704
Hey, man.

125
00:10:07,773 --> 00:10:09,934
If you want to fight, try me.

126
00:10:18,551 --> 00:10:21,645
Okay, I think I will.

127
00:10:27,193 --> 00:10:28,854
Take the truck, Gun.

128
00:10:28,928 --> 00:10:29,860
But...

129
00:10:29,929 --> 00:10:32,796
I'm okay. Go ahead.
They're waiting.

130
00:10:35,167 --> 00:10:36,759
Okay. Good luck.

131
00:10:53,285 --> 00:10:55,845
Let's go outside.

132
00:11:54,213 --> 00:11:55,510
Good morning.

133
00:11:56,449 --> 00:11:58,178
Morning.

134
00:11:58,551 --> 00:11:59,711
Ouch.

135
00:12:00,553 --> 00:12:02,487
- Are you okay?
- I think so.

136
00:12:02,555 --> 00:12:04,352
How did I end up here?

137
00:12:04,423 --> 00:12:06,323
Don't you remember?

138
00:12:06,392 --> 00:12:08,553
I tried to put you to bed.

139
00:12:08,627 --> 00:12:10,595
But you wouldn't move.

140
00:12:51,437 --> 00:12:52,927
You fight good.

141
00:12:53,005 --> 00:12:55,473
No. I got beaten up.

142
00:12:55,541 --> 00:12:57,168
That's okay.

143
00:12:57,243 --> 00:12:59,711
There were five of them.
That's not fair.

144
00:12:59,779 --> 00:13:01,940
- Do you win?
- No, not me.

145
00:13:02,014 --> 00:13:03,504
Do you lose?

146
00:13:03,582 --> 00:13:05,641
Always. There are three of them.

147
00:13:05,718 --> 00:13:07,618
Ryuta, Yohei and Uchida.

148
00:13:07,686 --> 00:13:09,620
Three? That's not fair.

149
00:13:09,688 --> 00:13:11,622
But I don't run.

150
00:13:11,690 --> 00:13:14,557
A man should never run,
my dad used to say.

151
00:13:14,627 --> 00:13:16,424
Your dad was tough.

152
00:13:16,495 --> 00:13:18,395
Yes, and Mom is great too.

153
00:13:18,464 --> 00:13:20,455
She's a good cook.

154
00:13:23,669 --> 00:13:27,264
Yes. Haven't had
such good pickles in years.

155
00:13:27,339 --> 00:13:29,204
The best pickles in Japan.

156
00:13:29,508 --> 00:13:31,601
Her noodles too.

157
00:13:32,111 --> 00:13:35,376
Her noodles?
Her noodles are, well...

158
00:13:35,447 --> 00:13:37,972
I've got to go.

159
00:13:38,050 --> 00:13:39,517
- Got your notebook?
- Yes.

160
00:13:39,585 --> 00:13:41,109
- Pen?
- Yes.

161
00:13:41,187 --> 00:13:42,950
Is the denominator top or bottom?

162
00:13:43,022 --> 00:13:45,684
Top, of course.
I know more math than you do.

163
00:13:45,758 --> 00:13:47,350
Or is it?

164
00:13:47,426 --> 00:13:49,087
No. It's the bottom one. Bye.

165
00:13:49,161 --> 00:13:50,594
Leave his hat!

166
00:13:58,370 --> 00:14:01,066
Were my noodles that bad?

167
00:14:01,140 --> 00:14:03,768
No, I didn't mean that.

168
00:14:03,843 --> 00:14:06,175
Please. Be honest.

169
00:14:06,812 --> 00:14:10,270
Since my husband died,
I've been trying so hard.

170
00:14:10,349 --> 00:14:13,841
But I'm not confident. I'm never sure.

171
00:14:15,621 --> 00:14:18,055
Please tell me the truth.

172
00:14:20,226 --> 00:14:22,091
Oh, I haven't even told you my name.

173
00:14:22,428 --> 00:14:25,397
I'm Tampopo.

174
00:14:25,464 --> 00:14:27,364
- I'm Goro.
- My name is Gun.

175
00:14:28,467 --> 00:14:31,368
So, how are my noodles?

176
00:14:33,038 --> 00:14:34,528
Well...

177
00:14:34,607 --> 00:14:40,170
They've got sincerity,
but they lack guts, they're--

178
00:14:40,246 --> 00:14:41,838
Frankly, they're bad.

179
00:14:44,250 --> 00:14:45,911
Uh, Miss Tampopo.

180
00:14:45,985 --> 00:14:47,885
- No. Just Tampopo.
- Okay, Tampopo.

181
00:14:47,953 --> 00:14:49,614
Go make noodles.

182
00:14:49,688 --> 00:14:51,178
- Gun, you're the customer.
- Okay.

183
00:14:51,257 --> 00:14:52,952
No. Come in the door.

184
00:14:53,025 --> 00:14:54,890
Okay? We'll give it a try.

185
00:14:55,427 --> 00:14:59,227
Ready? Go!

186
00:15:01,333 --> 00:15:02,960
- Welcome!
- Stop!

187
00:15:03,035 --> 00:15:04,969
If you say "welcome," look at him.

188
00:15:05,037 --> 00:15:06,834
- If not, just keep working.
- Yes, sir.

189
00:15:06,906 --> 00:15:08,066
Carry on.

190
00:15:08,807 --> 00:15:10,138
Plain noodles.

191
00:15:10,209 --> 00:15:14,578
Now, look at the customer
while he's not looking.

192
00:15:17,783 --> 00:15:19,375
Wait a minute.

193
00:15:21,287 --> 00:15:23,050
What did you see?

194
00:15:23,122 --> 00:15:26,455
- He's young and cute--
- No, no. That's not the point.

195
00:15:26,825 --> 00:15:29,623
Is he in a hurry? Is he hungry?

196
00:15:29,695 --> 00:15:31,458
Is he a new customer?

197
00:15:31,530 --> 00:15:33,054
Is he drunk?

198
00:15:33,132 --> 00:15:36,693
- Is he a customer you want?
- I see.

199
00:15:36,769 --> 00:15:38,361
Now fix the noodles.

200
00:15:45,744 --> 00:15:47,678
Cutting the pork there is good.

201
00:15:47,746 --> 00:15:49,509
That's how my husband did it.

202
00:15:49,581 --> 00:15:51,742
- The slices are too thick.
- But thicker is better.

203
00:15:51,817 --> 00:15:53,478
No, not at all.

204
00:15:53,552 --> 00:15:55,417
Three 3mm slices are best.

205
00:15:58,490 --> 00:16:01,288
- No. That's too thin.
- I see.

206
00:16:10,803 --> 00:16:13,101
Now, quick,
look at the customer!

207
00:16:13,806 --> 00:16:17,401
Don't you want to see how he reacts?

208
00:16:17,476 --> 00:16:19,137
Yes, sir. I'm sorry.

209
00:16:19,211 --> 00:16:21,406
- What's he doing?
- Sipping soup.

210
00:16:21,480 --> 00:16:23,209
Isn't that strange?

211
00:16:23,983 --> 00:16:28,113
- Well...
- It's very strange.

212
00:16:28,187 --> 00:16:32,817
How could he sip the soup?
It's supposed to be boiling hot.

213
00:16:32,891 --> 00:16:33,983
I see.

214
00:16:34,059 --> 00:16:35,686
- A fatal flaw!
- Yes, sir.

215
00:16:35,761 --> 00:16:37,592
- The soup must be hot.
- Yes, sir.

216
00:16:42,668 --> 00:16:44,568
Wait. Wait, please...

217
00:16:44,636 --> 00:16:45,898
Please...

218
00:16:45,971 --> 00:16:47,666
Please be my teacher.

219
00:16:47,740 --> 00:16:49,264
Teacher?

220
00:16:49,341 --> 00:16:52,276
Please. I'll be a good student.

221
00:16:52,344 --> 00:16:57,646
Meeting you makes me want
to be a real noodle cook.

222
00:16:58,183 --> 00:17:01,277
For my son too.

223
00:17:01,353 --> 00:17:05,187
I'll do anything.
So please, please teach me.

224
00:17:05,557 --> 00:17:08,321
Teach you? But we're...

225
00:17:08,394 --> 00:17:10,259
Only when you're free.

226
00:17:10,963 --> 00:17:14,057
I'll keep pickles ready for you.

227
00:17:15,667 --> 00:17:18,033
One, two! One, two!

228
00:17:18,103 --> 00:17:19,832
"Closed Today"

229
00:17:22,541 --> 00:17:23,906
Come on!

230
00:17:23,976 --> 00:17:25,466
Don't be such a weakling.

231
00:17:25,544 --> 00:17:28,274
One, two! One, two!

232
00:17:39,091 --> 00:17:40,854
Okay, ladle the noodles out.

233
00:17:40,926 --> 00:17:42,723
Into six bowls.

234
00:17:42,795 --> 00:17:44,592
Get set. Go!

235
00:17:49,001 --> 00:17:50,662
Faster.

236
00:17:55,140 --> 00:17:57,199
What's that? Put it back.

237
00:17:57,276 --> 00:17:59,141
- Keep going.
- Yes, sir.

238
00:17:59,978 --> 00:18:02,003
Get set! Go!

239
00:18:06,485 --> 00:18:08,885
Get set! Go!

240
00:18:34,546 --> 00:18:37,242
Almost there!
3 minutes 15 seconds.

241
00:18:37,316 --> 00:18:39,181
- You have to beat 3 minutes.
- Yes, sir.

242
00:18:43,822 --> 00:18:45,847
Come on!
It takes muscles!

243
00:18:45,924 --> 00:18:47,516
Don't give up!

244
00:18:47,593 --> 00:18:49,823
You must be better than a man.

245
00:18:49,895 --> 00:18:51,487
Go! Go! Go!

246
00:18:52,297 --> 00:18:55,596
Okay, now some stretching exercises.

247
00:19:06,612 --> 00:19:08,739
Why am I doing this?

248
00:19:46,818 --> 00:19:49,013
This place is famous for seafood.

249
00:20:38,237 --> 00:20:40,205
Your order, sir?

250
00:20:44,443 --> 00:20:46,206
What would you like, sir?

251
00:20:46,278 --> 00:20:48,143
Just a moment.

252
00:20:49,381 --> 00:20:51,315
And you, sir?

253
00:20:51,383 --> 00:20:56,047
I'm not too hungry. Something light.

254
00:20:56,121 --> 00:20:58,749
Let me see...

255
00:21:03,895 --> 00:21:06,557
Sole meuniere, please.

256
00:21:06,965 --> 00:21:08,728
Some soup or salad, sir?

257
00:21:08,800 --> 00:21:11,030
Consomme. No salad.

258
00:21:11,103 --> 00:21:12,900
- A drink, sir?
- Beer.

259
00:21:12,971 --> 00:21:14,939
- Heineken.
- Yes, sir.

260
00:21:20,212 --> 00:21:21,873
And you, sir?

261
00:21:21,947 --> 00:21:23,812
I'll have sole too.

262
00:21:25,384 --> 00:21:27,011
Soup or a salad, sir?

263
00:21:27,085 --> 00:21:29,815
Consomme. No salad.

264
00:21:29,888 --> 00:21:32,220
- Something to drink, sir?
- Beer too.

265
00:21:37,162 --> 00:21:38,686
And you, sir?

266
00:21:38,764 --> 00:21:39,958
Well...

267
00:21:40,032 --> 00:21:42,159
Maybe I'll try the sole, too.

268
00:21:42,234 --> 00:21:44,293
- Soup or salad, sir?
- Well...

269
00:21:44,369 --> 00:21:45,996
Maybe consomme.

270
00:21:46,071 --> 00:21:48,096
- A drink, sir?
- Well...

271
00:21:48,874 --> 00:21:50,774
Maybe a beer.

272
00:21:56,081 --> 00:21:57,742
Same for me.

273
00:21:57,816 --> 00:21:59,283
Me, too.

274
00:22:06,825 --> 00:22:08,292
Sir?

275
00:22:11,496 --> 00:22:13,396
- And you, sir?
- One moment.

276
00:22:20,706 --> 00:22:22,298
So you have quenelle.

277
00:22:25,610 --> 00:22:27,544
Boudin style...

278
00:22:29,047 --> 00:22:32,983
That's quenelle prepared
in the shape of sausage?

279
00:22:33,051 --> 00:22:34,040
Yes, sir.

280
00:22:35,387 --> 00:22:39,619
I think Taille-Vent in France
serves this.

281
00:22:39,691 --> 00:22:41,352
You're well-informed.

282
00:22:41,426 --> 00:22:43,792
Our chef trained at Taille-Vent.

283
00:22:44,996 --> 00:22:46,930
Then it's served with caviar sauce?

284
00:22:46,998 --> 00:22:48,590
That's correct, sir.

285
00:22:50,102 --> 00:22:54,436
And the escargots wrapped in pastry.

286
00:22:55,140 --> 00:22:56,937
In fond-de-veau?

287
00:22:57,008 --> 00:23:01,502
Yes. Escargots and mushrooms
simmered in Madeira.

288
00:23:01,580 --> 00:23:03,980
And then stewed in fond-de-veau, sir.

289
00:23:04,049 --> 00:23:05,949
Yes. I'll have that.

290
00:23:06,017 --> 00:23:10,317
And apple-and-walnut salad.

291
00:23:10,389 --> 00:23:12,516
A perfect match, sir.

292
00:23:12,591 --> 00:23:14,525
A drink, sir?

293
00:23:14,593 --> 00:23:16,584
Yes.

294
00:23:16,895 --> 00:23:21,298
I think I feel like
some Corton Charlemagne.

295
00:23:24,703 --> 00:23:26,068
Do you have a 1981?

296
00:23:26,138 --> 00:23:28,197
I'll call the sommelier.

297
00:23:29,508 --> 00:23:31,066
Thank you.

298
00:24:13,318 --> 00:24:18,449
You use grated cheese
only on certain spaghetti dishes.

299
00:24:18,523 --> 00:24:22,721
This is spaghetti alle vongole. So...?

300
00:24:23,895 --> 00:24:26,420
That's right, no cheese.

301
00:24:26,498 --> 00:24:28,830
Fork and spoon, everybody.

302
00:24:30,569 --> 00:24:33,299
Spoon in your left hand.

303
00:24:33,705 --> 00:24:40,736
Catch three or four strings
of spaghetti with your fork...

304
00:24:40,812 --> 00:24:45,010
and gently turn it around on your spoon.

305
00:24:45,784 --> 00:24:48,344
You wind your spaghetti like this.

306
00:24:48,720 --> 00:24:53,589
And eat it silently.

307
00:25:07,973 --> 00:25:13,036
Never make any noise.

308
00:25:17,983 --> 00:25:20,383
No noise, whatsoever.

309
00:25:20,852 --> 00:25:26,256
Listen carefully to hear
if you make any noise.

310
00:25:27,425 --> 00:25:32,886
Sometimes people
don't know they make noise.

311
00:25:33,265 --> 00:25:34,823
Let's try it.

312
00:25:34,900 --> 00:25:36,925
Listen carefully.

313
00:25:41,239 --> 00:25:44,936
Even a very faint noise...

314
00:25:53,051 --> 00:25:58,318
...like this is taboo abroad.

315
00:29:14,419 --> 00:29:16,649
This shop is so near to yours...

316
00:29:17,122 --> 00:29:19,113
but look at the queue.

317
00:29:19,691 --> 00:29:22,717
If your noodles are good, you'll get
all the customers you want.

318
00:29:22,794 --> 00:29:24,523
That's right.

319
00:29:24,863 --> 00:29:28,492
This place is not so great.

320
00:29:28,933 --> 00:29:31,333
You've eaten here before?

321
00:29:31,402 --> 00:29:33,427
No, I can just tell.

322
00:29:36,508 --> 00:29:39,204
See? Too much wasted motion.

323
00:29:39,277 --> 00:29:43,111
And they talk too much.
Only their "welcome" has clout.

324
00:29:43,181 --> 00:29:45,376
Who ordered pork and wonton dumplings?

325
00:29:45,450 --> 00:29:47,748
See? They can't remember
who ordered what.

326
00:29:48,219 --> 00:29:50,084
You can beat them in a month.

327
00:29:51,689 --> 00:29:53,418
Tampopo, look.

328
00:29:58,463 --> 00:29:59,760
Watch closely.

329
00:30:00,498 --> 00:30:02,830
See how he changes the water.

330
00:30:03,434 --> 00:30:05,766
See how they switch positions?

331
00:30:05,837 --> 00:30:07,498
No wasted motion.

332
00:30:08,706 --> 00:30:10,606
And they're quiet too.

333
00:30:11,609 --> 00:30:15,340
This is a good place.

334
00:30:15,413 --> 00:30:17,677
Their customers feel good too.

335
00:30:17,749 --> 00:30:22,277
See? They keep on drinking
right down to the last drop.

336
00:30:23,988 --> 00:30:28,049
Look, when they return their bowls.

337
00:30:28,660 --> 00:30:32,027
Watch closely. The old man looks at
every empty bowl...

338
00:30:33,331 --> 00:30:36,789
to see if the soup is finished.

339
00:30:39,737 --> 00:30:41,762
It's the soup that animates the noodles.

340
00:30:41,840 --> 00:30:44,809
That's why he checks so carefully.

341
00:30:45,944 --> 00:30:51,974
He remembers exactly who orders
what and when.

342
00:30:52,050 --> 00:30:53,813
Right?

343
00:30:53,885 --> 00:30:56,080
That's part of being good.

344
00:30:58,122 --> 00:31:00,454
Another train just arrived. Watch...

345
00:31:04,796 --> 00:31:06,229
With pork.

346
00:31:08,433 --> 00:31:10,401
Plain noodles. No bean sprouts.

347
00:31:12,103 --> 00:31:13,695
With dumplings.

348
00:31:14,339 --> 00:31:15,931
Extra noodles.

349
00:31:17,842 --> 00:31:19,742
Shinachiku noodles, please.

350
00:31:19,811 --> 00:31:21,301
Garlic noodles.

351
00:31:21,379 --> 00:31:22,971
I want soft noodles.

352
00:31:24,449 --> 00:31:25,916
How much?

353
00:31:27,352 --> 00:31:29,047
480 yen.

354
00:31:31,055 --> 00:31:32,215
Thanks.

355
00:31:33,791 --> 00:31:35,782
Great, isn't he?

356
00:31:37,528 --> 00:31:40,224
See? He knows exactly who ordered what.

357
00:31:40,531 --> 00:31:43,466
I want fatty pork in mine.

358
00:31:44,235 --> 00:31:45,964
Here's my money.

359
00:31:50,575 --> 00:31:52,509
Plain, with firm noodles.

360
00:31:56,514 --> 00:31:58,539
Plain but no shinachiku.

361
00:31:59,183 --> 00:32:02,584
I can remember.
That guy ordered the pork noodles.

362
00:32:02,654 --> 00:32:06,112
And that guy gets straight noodles,
without bean sprouts.

363
00:32:06,190 --> 00:32:10,251
And he gets the large noodles, but...

364
00:32:10,328 --> 00:32:12,125
- Uh...
- What?

365
00:32:12,196 --> 00:32:15,597
The dumplings guy gets served
before Mr. Big Noodles.

366
00:32:15,667 --> 00:32:17,726
- Is that right?
- Yes.

367
00:32:17,802 --> 00:32:19,326
With dumplings.

368
00:32:19,404 --> 00:32:20,598
Large noodles.

369
00:32:20,672 --> 00:32:22,469
Shinachiku noodles.

370
00:32:22,540 --> 00:32:24,337
Garlic noodles.

371
00:32:24,409 --> 00:32:26,240
Soft noodles.
Fatty pork.

372
00:32:26,311 --> 00:32:27,642
Firm noodles.

373
00:32:27,712 --> 00:32:29,441
No shinachiku.

374
00:32:53,304 --> 00:32:57,638
This is it. This is it !

375
00:33:01,980 --> 00:33:05,381
Excuse me. Please!

376
00:33:05,450 --> 00:33:09,113
Please tell me your soup recipe.

377
00:33:09,187 --> 00:33:12,156
How to make my soup? Never!

378
00:33:12,557 --> 00:33:16,084
You're a professional.
I can tell by your eyes.

379
00:33:16,160 --> 00:33:19,027
I don't tell competitors my secrets.

380
00:33:19,097 --> 00:33:21,930
Please. I'll pay you for it.

381
00:33:24,235 --> 00:33:25,725
How much?

382
00:33:25,803 --> 00:33:27,737
Uh, 50,000 yen or so.

383
00:33:27,805 --> 00:33:29,170
Never!

384
00:33:29,240 --> 00:33:32,971
If you want to pay money,
you loan me one million yen...

385
00:33:33,044 --> 00:33:35,103
to be paid back in a year,
no interest...

386
00:33:35,179 --> 00:33:39,138
then I'll tell you
the secret of my soup free.

387
00:33:40,118 --> 00:33:41,983
One million yen...

388
00:33:46,324 --> 00:33:49,885
Don't lend him anything.

389
00:33:50,328 --> 00:33:54,355
He's crazy about the horses.
You'll never get it back.

390
00:33:55,500 --> 00:34:00,164
You give me 30,000 yen.
I give you the secret of his soup.

391
00:34:00,738 --> 00:34:04,037
My shop is next door.

392
00:34:04,108 --> 00:34:07,635
Tonight come back with the money.

393
00:34:21,459 --> 00:34:22,619
Hello.

394
00:34:36,307 --> 00:34:37,638
Come this way.

395
00:34:38,209 --> 00:34:40,769
- Where to?
- This way. This way.

396
00:34:41,212 --> 00:34:44,181
Where are we going?

397
00:34:44,715 --> 00:34:46,148
There, there.

398
00:34:48,052 --> 00:34:49,849
What's happening?

399
00:34:49,921 --> 00:34:52,185
You can't learn soup-making here.

400
00:35:02,033 --> 00:35:04,092
Look through here.

401
00:36:09,200 --> 00:36:10,633
How much?

402
00:36:13,638 --> 00:36:17,335
Hey, why didn't you finish it?

403
00:36:17,408 --> 00:36:19,205
Sorry, but I'm full.

404
00:36:19,277 --> 00:36:23,543
What? Why did you order it
if you're not hungry, you jerks?

405
00:36:26,083 --> 00:36:28,449
You're from Lai Lai, aren't you?

406
00:36:28,753 --> 00:36:33,520
Why did you come here? To steal
our secrets, you lousy bastards!

407
00:36:35,693 --> 00:36:39,288
We've been in this business
for 40 years.

408
00:36:39,597 --> 00:36:44,193
We don't need comments from beginners.

409
00:36:44,936 --> 00:36:47,029
Apologize right now.

410
00:36:47,371 --> 00:36:50,363
Or finish every drop of our soup
before you go.

411
00:36:55,613 --> 00:36:58,810
Now we can't leave
without a last word, can we?

412
00:36:59,317 --> 00:37:03,754
I didn't finish the soup
because I couldn't. It stinks!

413
00:37:03,821 --> 00:37:05,914
How dare you!

414
00:37:05,990 --> 00:37:09,221
The soup tastes like
overcooked pork bones.

415
00:37:09,660 --> 00:37:13,619
The vegetables you use to hide
the pork stink are too sweet.

416
00:37:14,198 --> 00:37:16,132
The kombu is too strong.

417
00:37:16,200 --> 00:37:19,328
Yeah, and you use
dried seguro sardines.

418
00:37:19,403 --> 00:37:22,133
Those fish are not fit to eat.
Their innards stink.

419
00:37:22,473 --> 00:37:23,735
How dare you!

420
00:37:23,808 --> 00:37:29,075
You stupid amateurs could never
appreciate my noodles!

421
00:37:29,146 --> 00:37:32,582
But people who eat noodles
are all amateurs.

422
00:37:32,650 --> 00:37:35,084
So why make noodles amateurs
can't appreciate?

423
00:37:35,152 --> 00:37:39,782
She's right. You let the dough sit
too long. It smells like soda.

424
00:37:39,857 --> 00:37:42,621
After it rains, go easy on the soda.

425
00:37:42,693 --> 00:37:44,820
And your overcooked pork is
like cardboard.

426
00:37:44,895 --> 00:37:49,355
Your shinachiku aren't pickled.
So they're soggy and bland.

427
00:37:51,469 --> 00:37:56,634
You goddamn big mouths!
You think you make better noodles!

428
00:37:58,109 --> 00:38:00,771
We just make normal noodles
in a normal way.

429
00:38:00,845 --> 00:38:04,747
Okay. We'll come try
your normal noodles.

430
00:38:05,316 --> 00:38:07,716
We'll be there in the morning.

431
00:38:08,552 --> 00:38:11,020
It'd better be good, or else.

432
00:38:18,763 --> 00:38:21,288
The pork is okay. Is the water boiling?

433
00:38:21,365 --> 00:38:22,332
Here.

434
00:38:22,400 --> 00:38:23,697
How's the soup?

435
00:38:26,170 --> 00:38:27,967
- Oh, God!
- What's wrong?

436
00:38:28,039 --> 00:38:29,370
I let the soup boil.

437
00:38:29,440 --> 00:38:31,305
- It's not clear.
- What can we do?

438
00:38:34,178 --> 00:38:36,840
- They're coming.
- Mom, they're here!

439
00:38:45,489 --> 00:38:50,688
Okay, let's have some of
your normal noodles.

440
00:38:50,761 --> 00:38:52,626
- Wait a minute.
- Wait?

441
00:38:53,998 --> 00:38:55,829
It won't take long.

442
00:38:55,900 --> 00:39:00,667
No, we want it now.
A promise is a promise. Get moving.

443
00:39:02,239 --> 00:39:03,831
What's that smell?

444
00:39:06,310 --> 00:39:08,801
Let me see the soup.

445
00:39:12,783 --> 00:39:14,341
Leave my mom alone!

446
00:39:21,392 --> 00:39:23,383
Don't! Stop it!

447
00:39:24,829 --> 00:39:26,694
Goro!

448
00:39:29,300 --> 00:39:30,631
Help!

449
00:39:30,701 --> 00:39:32,430
Goro, help!

450
00:39:50,788 --> 00:39:52,221
Oh, damn it!

451
00:40:12,276 --> 00:40:14,039
"Closed Today"

452
00:40:14,111 --> 00:40:16,477
Closed again, eh?

453
00:40:17,415 --> 00:40:19,975
It's not good for business.

454
00:40:24,021 --> 00:40:25,955
So you still can't get the soup right.

455
00:40:29,226 --> 00:40:31,285
We'll just have to ask the old master.

456
00:40:31,732 --> 00:40:34,030
.

457
00:40:34,068 --> 00:40:36,730
Goro, we're over here.
Come join us.

458
00:40:37,304 --> 00:40:38,896
There they are.

459
00:40:40,341 --> 00:40:41,865
Come on.

460
00:40:45,346 --> 00:40:47,780
Hello, hello.

461
00:40:49,216 --> 00:40:51,207
Come on.

462
00:40:53,420 --> 00:40:54,887
Hey!

463
00:40:55,889 --> 00:40:57,618
Come here.

464
00:41:02,963 --> 00:41:04,828
Come share our meal.

465
00:41:10,571 --> 00:41:12,971
This is our master.
He was a doctor.

466
00:41:13,040 --> 00:41:15,133
While he was selling noodles for fun...

467
00:41:15,209 --> 00:41:19,669
...his partner stole his wife
and practice.

468
00:41:21,181 --> 00:41:23,240
Now he's our resident gourmet.

469
00:41:24,084 --> 00:41:26,348
Nice to meet you.

470
00:41:26,420 --> 00:41:28,251
You came just in time.

471
00:41:28,322 --> 00:41:31,985
We just got our hands on some of
Carlton's boeuf bourgignon.

472
00:41:33,627 --> 00:41:36,562
It's slightly burnt,
so you can't call it perfect.

473
00:41:36,930 --> 00:41:40,866
French cooking is a constant battle
with burns.

474
00:41:42,970 --> 00:41:44,403
Ummm. Good.

475
00:41:46,173 --> 00:41:49,438
How's the sake?
Pretty good, isn't it?

476
00:41:50,244 --> 00:41:53,839
Yeah. But what is it?
I can't tell the brand.

477
00:41:55,516 --> 00:41:58,849
No, you can't. It's a blend.

478
00:41:58,919 --> 00:42:02,286
The base is a semi-dry Nada.

479
00:42:02,823 --> 00:42:05,121
But we blended it quite a bit.

480
00:42:05,192 --> 00:42:08,286
He's been studying this for 15 years.

481
00:42:08,796 --> 00:42:13,961
It's a pork cutlet from Kitaro.
But I can't recommend it.

482
00:42:14,034 --> 00:42:16,161
Their quality has gone down.

483
00:42:16,570 --> 00:42:21,439
They used to use good pork
from Kagoshima. But not now.

484
00:42:23,577 --> 00:42:27,343
Now they even use machines
to shred cabbage.

485
00:42:27,414 --> 00:42:29,746
No more soul in their food.

486
00:42:29,817 --> 00:42:32,786
It used to be good, though.

487
00:42:32,853 --> 00:42:35,253
They tend to be fussy.

488
00:42:35,322 --> 00:42:39,190
What's the story about this wine?

489
00:42:39,727 --> 00:42:43,060
You mean this Medoc,
a 1980 Chateau Pichon Lalande.

490
00:42:47,134 --> 00:42:54,700
The weather was bad in 1980,
and it was a bad year for Bordeaux.

491
00:42:56,610 --> 00:42:59,738
The other day I was in the alley
behind Chat Qui P�che...

492
00:42:59,813 --> 00:43:02,247
and found some empty bottles...

493
00:43:02,316 --> 00:43:07,117
including this particular
Chateau Pichon Lalande.

494
00:43:07,588 --> 00:43:11,854
There was still 5cm left.

495
00:43:11,925 --> 00:43:15,827
So I brought it home and decanted it.

496
00:43:17,398 --> 00:43:20,094
It's a real find.

497
00:43:23,570 --> 00:43:27,370
Light but firm...

498
00:43:28,008 --> 00:43:33,344
The tail that trailed down my throat
was so long.

499
00:43:36,617 --> 00:43:40,644
They live deeply, these vagabonds.

500
00:43:40,721 --> 00:43:43,690
You're not eating much, sonny.
Can I fix you something?

501
00:43:43,757 --> 00:43:46,248
What do you want?

502
00:43:46,326 --> 00:43:48,817
- A rice omelet.
- Rice omelet. Hmm.

503
00:43:48,896 --> 00:43:50,887
Okay, follow me.

504
00:46:30,891 --> 00:46:33,155
We want to borrow your master
for a while.

505
00:46:33,226 --> 00:46:34,887
So long.

506
00:46:34,962 --> 00:46:37,089
I'm so grateful, master.

507
00:46:37,164 --> 00:46:38,654
Good luck.

508
00:46:38,732 --> 00:46:41,530
Why don't we sing our master off.

509
00:46:47,574 --> 00:46:55,504
How precious

510
00:46:57,684 --> 00:47:04,112
Our teacher's teachings

511
00:47:07,294 --> 00:47:15,633
Time flies swiftly

512
00:47:18,338 --> 00:47:25,801
In this garden of learning

513
00:47:27,147 --> 00:47:36,078
So swiftly

514
00:47:38,291 --> 00:47:44,787
After all these years

515
00:47:49,202 --> 00:48:00,807
We must part

516
00:48:02,916 --> 00:48:09,412
Goodbye

517
00:50:01,468 --> 00:50:03,026
What did you catch?

518
00:50:03,103 --> 00:50:04,263
Oysters.

519
00:50:04,571 --> 00:50:06,095
Let me see.

520
00:50:19,486 --> 00:50:21,351
Sell me one.

521
00:50:22,455 --> 00:50:23,752
Sure.

522
00:50:24,457 --> 00:50:26,015
Shall I open it?

523
00:50:53,320 --> 00:50:54,082
Ouch!

524
00:51:14,808 --> 00:51:16,400
Here, let me help.

525
00:51:23,984 --> 00:51:25,679
It tickles.

526
00:52:51,004 --> 00:52:52,869
Anesthetic.

527
00:54:04,544 --> 00:54:06,444
God, what a huge abscess.

528
00:54:06,513 --> 00:54:09,107
What a stink. Didn't you smell it?

529
00:54:09,816 --> 00:54:11,647
A little bit.

530
00:54:11,718 --> 00:54:14,084
Whew! I thought I'd vomit.

531
00:54:15,889 --> 00:54:17,686
You can eat anything now.

532
00:54:17,757 --> 00:54:19,782
But start with something soft.

533
00:54:33,206 --> 00:54:34,798
"I only eat natural food"

534
00:54:39,379 --> 00:54:41,711
"Do not give me sweets or snacks"

535
00:54:41,781 --> 00:54:43,646
"A message from my mother"

536
00:54:51,991 --> 00:54:54,687
Do you want this?

537
00:55:01,701 --> 00:55:03,794
It's good.

538
00:55:04,771 --> 00:55:06,363
Here.

539
00:55:29,329 --> 00:55:30,626
Listen carefully.

540
00:55:30,697 --> 00:55:37,125
Noodles are synergetic things.
Every step must be perfectly built.

541
00:55:38,605 --> 00:55:40,505
Don't forget that.

542
00:55:40,573 --> 00:55:45,738
Let's review the basic rules of soup.

543
00:55:45,812 --> 00:55:51,273
Fowl spoils quickly.
So use only the freshest chicken.

544
00:55:51,351 --> 00:55:57,551
Both chicken and pork have strong
smells. So parboil them first.

545
00:55:57,624 --> 00:56:01,390
Then rinse them well in cold water.

546
00:56:01,461 --> 00:56:04,555
Don't cut the vegetables.

547
00:56:05,431 --> 00:56:07,558
The main point is heat.

548
00:56:08,101 --> 00:56:10,899
Heat releases the soul
of the ingredients.

549
00:56:10,970 --> 00:56:15,703
But never allow it to come
to a full boil like that.

550
00:56:16,376 --> 00:56:19,311
If you boil it,
the soup will never clear.

551
00:56:20,313 --> 00:56:26,445
And keep skimming the scum off.

552
00:56:29,155 --> 00:56:32,989
Today I have
a most interesting addition.

553
00:56:33,293 --> 00:56:35,887
Isn't he beautiful?

554
00:56:36,930 --> 00:56:38,363
This is...

555
00:56:57,350 --> 00:57:01,946
Sorry. It's just the way
it looked at me.

556
00:57:02,488 --> 00:57:06,982
Here's your 14 servings
of mori noodles.

557
00:57:16,936 --> 00:57:18,733
Welcome!

558
00:57:18,805 --> 00:57:20,796
How nice to see you, sir.

559
00:57:20,873 --> 00:57:23,774
Please, come this way.

560
00:57:43,162 --> 00:57:45,221
No shiroko , darling.

561
00:57:45,298 --> 00:57:47,425
Or kamonamban
or tempura soba either.

562
00:57:47,500 --> 00:57:50,162
They almost got you last time.

563
00:57:50,236 --> 00:57:52,170
I'm just going to the bank now.

564
00:57:52,238 --> 00:57:53,967
Come, Shohei.

565
00:58:13,626 --> 00:58:15,423
One kamonamban here.

566
00:58:15,495 --> 00:58:17,360
One tempura soba here.

567
00:58:17,730 --> 00:58:20,631
And one shiruko. All for you.