1
00:00:15,380 --> 00:00:17,500
<i>Kevin Gable,
please see the manager.</i>

2
00:00:17,500 --> 00:00:20,380
<i>Kevin Gable,
please see the manager.</i>

3
00:00:26,420 --> 00:00:28,420
You, uh --
you wanted to see me, Owen?

4
00:00:28,420 --> 00:00:29,710
"Owen"?

5
00:00:31,420 --> 00:00:33,670
Sorry. Mr. Durning.

6
00:00:33,670 --> 00:00:36,790
Exactly. Like the name
on the sign out front.

7
00:00:36,790 --> 00:00:38,710
That's right --
my dad owns the place.

8
00:00:38,710 --> 00:00:41,710
You've mentioned that
a couple times.

9
00:00:41,710 --> 00:00:43,170
Here's the sitch.

10
00:00:43,170 --> 00:00:45,710
I noticed on the security
monitor that you popped

11
00:00:45,710 --> 00:00:48,750
a cube of cheese from Rose,
the sample lady.

12
00:00:48,750 --> 00:00:49,920
It was a free --

13
00:00:48,750 --> 00:00:49,920
Don't deny it, chief.

14
00:00:49,920 --> 00:00:51,040
I see everything.

15
00:00:51,040 --> 00:00:52,540
I'm not denying it.
She asked me to try it.

16
00:00:52,540 --> 00:00:54,250
I don't know --
Is there a problem with that?

17
00:00:54,250 --> 00:00:56,120
Would there be a problem
if Homeless Joe came in here

18
00:00:56,120 --> 00:00:58,710
and started peeing
on the apricots?

19
00:00:58,710 --> 00:01:01,880
I didn't pee
on the cheese.

20
00:01:01,880 --> 00:01:04,290
Now, one more screw-up,
and it's your keys.

21
00:01:04,290 --> 00:01:05,750
The whole ring, chief.

22
00:01:07,290 --> 00:01:08,210
You feel me?

23
00:01:08,210 --> 00:01:09,670
I do.
I-I feel you.

24
00:01:09,670 --> 00:01:11,710
Then why are you
still here?

25
00:01:13,420 --> 00:01:15,460
I don't --
I don't know.

26
00:01:47,500 --> 00:01:48,960
Hey.

27
00:01:47,500 --> 00:01:48,960
Hey.

28
00:01:48,960 --> 00:01:50,710
Sorry I'm late.

29
00:01:50,710 --> 00:01:53,880
Did you know All American Burger
is right across the street?

30
00:01:53,880 --> 00:01:56,210
I pulled in there by mistake.
It's like muscle memory.

31
00:01:56,210 --> 00:01:58,500
My body was like --
just wanted to go there.

32
00:01:58,500 --> 00:02:00,620
Yeah. Well, I'm glad
you made it <i>here.</i>

33
00:02:00,620 --> 00:02:02,500
And I know your policy
on foreign food --

34
00:02:02,500 --> 00:02:03,750
If you can't read it,
you don't eat it.

35
00:02:03,750 --> 00:02:04,830
That's right.

36
00:02:04,830 --> 00:02:06,120
What's the, uh --
what's the deal here?

37
00:02:06,120 --> 00:02:09,210
Do I got to take off my shoes?
Do I got to bow to anybody?

38
00:02:09,210 --> 00:02:10,580
It's Vietnamese.
No.

39
00:02:10,580 --> 00:02:11,750
Okay, well,
just so you know,

40
00:02:11,750 --> 00:02:13,670
I'm not eating anything raw --
anything, okay?

41
00:02:13,670 --> 00:02:15,380
And I don't want to cook
my own food, you know,

42
00:02:15,380 --> 00:02:16,750
where they put the hot rock
in the middle there

43
00:02:16,750 --> 00:02:18,210
and they're like, you know,
"Get to work."

44
00:02:18,210 --> 00:02:21,120
I'm like, <i>"You </i>get to work.
That's what I'm paying you for."

45
00:02:21,120 --> 00:02:23,210
Honey, I know
all your food rules, okay?

46
00:02:23,210 --> 00:02:24,710
There's no rocks.
There's no raw food.

47
00:02:24,710 --> 00:02:26,080
There's
no sourdough bread.

48
00:02:26,080 --> 00:02:28,880
Why would I want my bread sour,
by the way? Seriously, I mean...

49
00:02:28,880 --> 00:02:30,580
You're gonna love it,
all right?

50
00:02:30,580 --> 00:02:32,500
Oh, great.
Look at this.

51
00:02:30,580 --> 00:02:32,500
What?

52
00:02:32,500 --> 00:02:35,880
I can see the sign
from All American.

53
00:02:35,880 --> 00:02:37,170
It says "urger."

54
00:02:37,170 --> 00:02:39,120
It says "urger," but I know
what they mean, and I know --

55
00:02:39,120 --> 00:02:40,880
We got to switch.
Switch with me.

56
00:02:40,880 --> 00:02:42,710
Yeah, yeah, yeah.

57
00:02:40,880 --> 00:02:42,710
Switch. Switch.

58
00:02:42,710 --> 00:02:45,710
It's like a lighthouse
calling you home.

59
00:02:46,790 --> 00:02:47,960
There we go.

60
00:02:49,380 --> 00:02:51,670
Well, I got to admit,
it was a nice surprise
to get your call.

61
00:02:51,670 --> 00:02:53,120
I thought you were gonna be
at the supermarket till 8:00.

62
00:02:53,120 --> 00:02:56,420
Oh, yeah, uh, cute story.
Um...

63
00:02:56,420 --> 00:02:57,960
There might have been
a little dustup

64
00:02:57,960 --> 00:03:00,290
involving
some cubes of cheese --

65
00:03:00,290 --> 00:03:01,540
that were complimentary,
by the way.

66
00:03:01,540 --> 00:03:03,790
Mm-hmm.
You quit?

67
00:03:03,790 --> 00:03:05,040
Yeah, I did.

68
00:03:05,040 --> 00:03:06,540
I also kissed Rose,
the sample lady --

69
00:03:06,540 --> 00:03:07,710
just to make a point.

70
00:03:09,210 --> 00:03:11,040
Wasn't sexy.
Her teeth shifted.

71
00:03:11,040 --> 00:03:13,540
It was weird.
I'm telling you.

72
00:03:13,540 --> 00:03:14,960
Should I be worried
about this?

73
00:03:14,960 --> 00:03:16,540
No! Trust me --
she's not gonna sue.

74
00:03:16,540 --> 00:03:19,540
She lit up like a candle.
I'm telling you.

75
00:03:19,540 --> 00:03:21,000
That's not what I mean.

76
00:03:21,000 --> 00:03:22,210
That's, like,
the fifth job

77
00:03:22,210 --> 00:03:24,250
you've walked out on
in two months.

78
00:03:24,250 --> 00:03:25,670
Honey, what's going on?

79
00:03:26,960 --> 00:03:28,580
I know we need extra money,
all right?

80
00:03:28,580 --> 00:03:31,330
But I just can't take these kids
bossing me around anymore.

81
00:03:31,330 --> 00:03:32,620
Okay, well,
don't worry about the money.

82
00:03:32,620 --> 00:03:33,750
We'll figure that out.

83
00:03:33,750 --> 00:03:35,790
But you need to find something
that you like,

84
00:03:35,790 --> 00:03:37,380
something
you're passionate about.

85
00:03:37,380 --> 00:03:39,880
Yeah, I would love that.
That's awesome.

86
00:03:39,880 --> 00:03:40,580
Oh.

87
00:03:39,880 --> 00:03:40,580
Oh.

88
00:03:40,580 --> 00:03:42,330
Thank you.

89
00:03:40,580 --> 00:03:42,330
Thank you.

90
00:03:51,080 --> 00:03:52,880
What am I
looking at here?

91
00:03:52,880 --> 00:03:54,290
It's pho.

92
00:03:54,290 --> 00:03:56,540
It's what?

93
00:03:54,290 --> 00:03:56,540
Pho.

94
00:03:56,540 --> 00:03:58,790
Well, pho looks like
a clogged sink.

95
00:04:00,540 --> 00:04:02,460
What are those chunks floating
around in there? What is that?

96
00:04:02,460 --> 00:04:04,040
It's chicken.
You love chicken.

97
00:04:04,040 --> 00:04:05,460
Did he make this outside?
What is this?

98
00:04:05,460 --> 00:04:07,750
Okay, babe,
it's seasoning!

99
00:04:07,750 --> 00:04:08,920
Just try it!

100
00:04:08,920 --> 00:04:10,460
Okay.

101
00:04:14,290 --> 00:04:15,620
Good?

102
00:04:26,080 --> 00:04:28,500
Hold the pho-one.

103
00:04:28,500 --> 00:04:29,880
Yeah!

104
00:04:29,880 --> 00:04:30,710
This is insane.

105
00:04:30,710 --> 00:04:31,790
This existed
this whole time,

106
00:04:31,790 --> 00:04:33,120
and you don't
tell me about it?

107
00:04:33,120 --> 00:04:34,420
Yeah, and wait
till you try the beef.

108
00:04:34,420 --> 00:04:36,420
This comes in beef?!

109
00:04:43,500 --> 00:04:45,120
Oh, hey, babe.

110
00:04:45,120 --> 00:04:46,920
Ugh. Never be
a school nurse.

111
00:04:46,920 --> 00:04:48,790
I had to send two kids home
with chicken pox,

112
00:04:48,790 --> 00:04:50,540
and I'm so paranoid
that I have it.

113
00:04:50,540 --> 00:04:53,540
I'm, like, itchy.
Will you look at my back?

114
00:04:53,540 --> 00:04:55,210
Your back is fine.

115
00:04:56,710 --> 00:04:58,750
Where do you keep
peanut worm?

116
00:04:58,750 --> 00:05:00,420
Um...

117
00:04:58,750 --> 00:05:00,420
Hey, hon,
where's our peanut worm?

118
00:05:00,420 --> 00:05:02,380
What...is happening?

119
00:05:02,380 --> 00:05:04,040
Let me get you caught up
to speed.

120
00:05:02,380 --> 00:05:04,040
Yeah.

121
00:05:04,040 --> 00:05:05,420
Remember that soup
that I loved last night?

122
00:05:05,420 --> 00:05:07,540
Yeah.

123
00:05:05,420 --> 00:05:07,540
Well, Mr. Chu here,
he made it.

124
00:05:07,540 --> 00:05:09,500
Oh. Oh, Mr. Chu,
hi.

125
00:05:09,500 --> 00:05:12,000
We -- I showed him my back.
I'll tell you later.

126
00:05:12,960 --> 00:05:14,380
All right, anyway,
I went down

127
00:05:14,380 --> 00:05:16,880
to the restaurant today
and...

128
00:05:16,880 --> 00:05:19,380
Well, long story short,
Mr. Chu works for me now.

129
00:05:19,380 --> 00:05:20,710
Uh, <i>with </i>you.

130
00:05:20,710 --> 00:05:22,540
This guy's got no filter.

131
00:05:22,540 --> 00:05:24,880
He's crazy.

132
00:05:22,540 --> 00:05:24,880
Um...

133
00:05:24,880 --> 00:05:26,120
Wait.
Working with you?

134
00:05:26,120 --> 00:05:27,920
What -- so you're opening
a Vietnamese restaurant?

135
00:05:27,920 --> 00:05:29,880
Well, that was
my first impulse,

136
00:05:29,880 --> 00:05:32,880
but then I cooled my jets
and put my business cap on.

137
00:05:32,880 --> 00:05:36,250
Chale, hit me up with it!

138
00:05:36,250 --> 00:05:39,380
90% of all new restaurants
go belly-up within a year.

139
00:05:39,380 --> 00:05:41,120
You know
what <i>doesn't </i>go belly-up?

140
00:05:41,120 --> 00:05:42,210
Food trucks.

141
00:05:42,210 --> 00:05:44,710
That feels like
it can't be true.

142
00:05:45,790 --> 00:05:46,790
Chale.

143
00:05:46,790 --> 00:05:48,540
A-Actually,
you're both right.

144
00:05:48,540 --> 00:05:52,040
But food trucks have
a much higher rate of success.

145
00:05:52,040 --> 00:05:55,920
And currently, there are none
on Long Island that sell pho.

146
00:05:55,920 --> 00:05:56,880
Thank you, Chale.

147
00:05:56,880 --> 00:05:58,210
However,
in my opinion --

148
00:05:58,210 --> 00:06:00,960
Okay, you know what? I will
point to you when I need you.

149
00:06:03,750 --> 00:06:06,330
So, yesterday,
you didn't know what pho was,

150
00:06:06,330 --> 00:06:08,210
and now we're in
the pho business?

151
00:06:08,210 --> 00:06:10,250
You told me to find something
I'm passionate about.

152
00:06:10,250 --> 00:06:12,420
Well, with this, I can be
my own boss, thanks to you.

153
00:06:12,420 --> 00:06:13,880
Okay, well,
that's great.

154
00:06:13,880 --> 00:06:15,710
But how much
is this gonna cost us?

155
00:06:15,710 --> 00:06:16,830
It's just 25 grand.

156
00:06:16,830 --> 00:06:18,210
What?!

157
00:06:18,210 --> 00:06:20,290
Honey!
We don't have 25 grand!

158
00:06:20,290 --> 00:06:22,880
No, no, but we can swing 5,
and for the rest,

159
00:06:22,880 --> 00:06:24,330
I brought in
some investors.

160
00:06:24,330 --> 00:06:27,380
Let's meet...the Sharks.

161
00:06:30,880 --> 00:06:32,250
Three retired
police officers

162
00:06:32,250 --> 00:06:34,750
searching for their slice
of the American dream.

163
00:06:34,750 --> 00:06:36,380
And my brother.

164
00:06:38,830 --> 00:06:40,500
So, they're
all into this?

165
00:06:40,500 --> 00:06:41,750
Not yet --
I got to pitch them.

166
00:06:41,750 --> 00:06:44,540
But if we get four yeses,
we're in business.

167
00:06:44,540 --> 00:06:45,670
Pho ready.

168
00:06:45,670 --> 00:06:47,710
Oh! Pho ready.
Let's do this.

169
00:06:47,710 --> 00:06:49,420
What? What?!

170
00:06:47,710 --> 00:06:49,420
Showtime.

171
00:06:49,420 --> 00:06:50,540
Showtime!

172
00:06:50,540 --> 00:06:52,540
Oh, Chale,
please don't encourage him.

173
00:06:52,540 --> 00:06:54,580
I just want him
to like me.

174
00:06:55,790 --> 00:06:58,080
Gentlemen, good afternoon.

175
00:06:58,080 --> 00:07:02,710
I'd like to talk to you about
a little place called Vietnam.

176
00:07:02,710 --> 00:07:05,620
Country of mystery.

177
00:07:05,620 --> 00:07:09,290
Once our enemy,
now our friend.

178
00:07:09,290 --> 00:07:10,580
And at last, we can enjoy

179
00:07:10,580 --> 00:07:14,000
one of their finest
achievements -- pho.

180
00:07:14,000 --> 00:07:15,210
Pho?

181
00:07:15,210 --> 00:07:17,500
Yes. Don't let
the weird name scare you.

182
00:07:17,500 --> 00:07:19,040
Think of it
as a warm blanket

183
00:07:19,040 --> 00:07:21,120
from our friends
from the Far East.

184
00:07:21,120 --> 00:07:22,670
What's all this green stuff?

185
00:07:22,670 --> 00:07:25,170
This guy's like a caveman,
I swear.

186
00:07:25,170 --> 00:07:27,670
It's seasoning.
Give it a shot.

187
00:07:27,670 --> 00:07:29,580
Man, this is
actually really good.

188
00:07:29,580 --> 00:07:31,580
Right?

189
00:07:29,580 --> 00:07:31,580
I love the hunks of meat
at the bottom.

190
00:07:31,580 --> 00:07:34,960
It's like a --
a treasure hunt.

191
00:07:31,580 --> 00:07:34,960
Yeah.

192
00:07:34,960 --> 00:07:38,880
It's like a reward for pushing
away all the green stuff.

193
00:07:38,880 --> 00:07:40,500
You know what? I don't
really mind the green stuff.

194
00:07:40,500 --> 00:07:44,750
Minty -- like a cross between
lettuce and brushing my teeth.

195
00:07:44,750 --> 00:07:46,540
Okay, well, guys,
that's great,

196
00:07:46,540 --> 00:07:48,000
but is there even
a business plan?

197
00:07:48,000 --> 00:07:49,580
Because you can't have
a business without a plan.

198
00:07:49,580 --> 00:07:51,040
I couldn't agree more.
You know what?

199
00:07:51,040 --> 00:07:53,120
Chale,
slap the math on her!

200
00:07:53,120 --> 00:07:55,330
The food-service industry

201
00:07:55,330 --> 00:07:59,170
generates $782 billion
a year.

202
00:07:59,170 --> 00:08:02,250
If you were to make
just 1% of 1% of that,

203
00:08:02,250 --> 00:08:04,040
you would all
be quite wealthy.

204
00:08:04,040 --> 00:08:06,540
Do you hear that? Okay?
This is not soup.

205
00:08:06,540 --> 00:08:08,500
This is a ticket
to happiness!

206
00:08:08,500 --> 00:08:10,120
And what does
that ticket cost?

207
00:08:10,120 --> 00:08:12,250
Just $5,000.

208
00:08:10,120 --> 00:08:12,250
$5,000.

209
00:08:12,250 --> 00:08:14,330
Per man.

210
00:08:14,330 --> 00:08:16,460
I don't know --
that's a lot of bacon.

211
00:08:16,460 --> 00:08:19,080
Mott,
can I ask you a question?

212
00:08:19,080 --> 00:08:20,210
Are you happy?

213
00:08:20,210 --> 00:08:21,670
I am.

214
00:08:21,670 --> 00:08:24,290
Mott, are you happy?

215
00:08:24,290 --> 00:08:25,620
Yeah, I'm -- I am.

216
00:08:25,620 --> 00:08:27,500
He's happy! He said he's happy.
Leave him alone.

217
00:08:27,500 --> 00:08:29,040
I'm just saying
I don't think he's happy.

218
00:08:29,040 --> 00:08:30,830
What?!

219
00:08:29,040 --> 00:08:30,830
Mott. Look at me.

220
00:08:35,210 --> 00:08:36,620
I want you to focus now.

221
00:08:36,620 --> 00:08:39,290
Are...you...happy?

222
00:08:39,290 --> 00:08:40,250
I'm <i>not </i>happy?

223
00:08:40,250 --> 00:08:42,460
Thank you
for admitting that.

224
00:08:42,460 --> 00:08:44,040
That was big, okay?

225
00:08:44,040 --> 00:08:46,290
Now, Duffy, please,
let's be honest.

226
00:08:46,290 --> 00:08:49,000
We know you're financially
dependent on your mom.

227
00:08:49,000 --> 00:08:51,080
Don't you want
to break free of that?

228
00:08:51,080 --> 00:08:52,420
Yes, I do.

229
00:08:52,420 --> 00:08:54,670
Then you got to do it.

230
00:08:54,670 --> 00:08:56,040
You know what? You're right.
I'm gonna do it.

231
00:08:56,040 --> 00:08:57,330
I'll do it right now.

232
00:08:56,040 --> 00:08:57,330
No, no, no, wait.

233
00:08:57,330 --> 00:09:01,210
You got to get the 5 grand
from Mommy first, and then...

234
00:09:01,210 --> 00:09:02,080
Kyle.

235
00:09:02,080 --> 00:09:03,210
Yeah. What?

236
00:09:03,210 --> 00:09:04,710
What's 5 grand
between brothers?

237
00:09:04,710 --> 00:09:07,380
I'll just pull it
from my fireman's pension fund.

238
00:09:07,380 --> 00:09:08,880
Smart move.

239
00:09:07,380 --> 00:09:08,880
Actually, it's not.

240
00:09:08,880 --> 00:09:11,500
There's, uh, crazy penalties
for early withdrawal.

241
00:09:11,500 --> 00:09:14,120
No, no, no, man, this whole deal
sounds too risky.

242
00:09:14,120 --> 00:09:16,540
'Cause it's a lot of risk.
It's a lot of risk.

243
00:09:16,540 --> 00:09:18,420
Yeah, but you know what?
We're betting on <i>ourselves.</i>

244
00:09:18,420 --> 00:09:19,540
That's
the difference here.

245
00:09:19,540 --> 00:09:20,830
You know what?
I just ran the numbers.

246
00:09:20,830 --> 00:09:22,380
I'm gonna have to take out
at least $7,000 to --

247
00:09:22,380 --> 00:09:24,210
Kyle,
nobody gives a crap.

248
00:09:24,210 --> 00:09:25,710
You know what --
I'm gonna take out $8,000,

249
00:09:25,710 --> 00:09:27,380
just to give myself
a little walking-around money.

250
00:09:27,380 --> 00:09:28,620
You know
what I'm saying?

251
00:09:28,620 --> 00:09:30,880
Duffy's in, right?
Kyle's in.

252
00:09:30,880 --> 00:09:32,170
Goody, it's up to you.

253
00:09:32,170 --> 00:09:34,250
Oh, man.
I'm in, too.

254
00:09:34,250 --> 00:09:36,040
Can't watch these guys
get rich without me.

255
00:09:36,040 --> 00:09:37,120
Yes!

256
00:09:36,040 --> 00:09:37,120
I'm in.

257
00:09:37,120 --> 00:09:38,830
All right, Mott,
you are the missing piece.

258
00:09:38,830 --> 00:09:40,210
What am I doing?

259
00:09:40,210 --> 00:09:42,500
I'll just take out the full
$10,000 and treat myself.

260
00:09:44,290 --> 00:09:46,710
Look,
I got seven kids.

261
00:09:46,710 --> 00:09:50,000
Yeah, and that's
seven mouths to feed.

262
00:09:50,000 --> 00:09:52,250
But...I want to be happy.
I'm in.

263
00:09:52,250 --> 00:09:54,120
No!

264
00:09:52,250 --> 00:09:54,120
We got
our four investors.

265
00:09:54,120 --> 00:09:56,880
Come on, honey.
I can't do this without you.

266
00:09:56,880 --> 00:09:58,000
Are we in?

267
00:09:58,000 --> 00:10:00,420
I -- Uh...

268
00:10:01,210 --> 00:10:03,040
Okay, fine! We're in.

269
00:10:03,040 --> 00:10:05,170
Awesome! 'Cause I already put
the down payment on the truck.

270
00:10:14,250 --> 00:10:17,250
Hey, folks.
Kevin Gable -- CEO/founder.

271
00:10:17,250 --> 00:10:19,250
I'd ask you how the soup is,
but that electric smile

272
00:10:19,250 --> 00:10:21,080
tells me everything
I need to know.

273
00:10:22,250 --> 00:10:24,620
Once again, Kevin Gable --
CEO/founder.

274
00:10:24,620 --> 00:10:27,040
Hey. There you go.
Enjoy.

275
00:10:27,040 --> 00:10:29,500
We have been so busy all day.
You're a hit, Dad.

276
00:10:29,500 --> 00:10:31,250
I told you.
You having fun, sweetheart?

277
00:10:31,250 --> 00:10:33,250
Yeah. Slinging soup in a hotbox
for no pay?

278
00:10:33,250 --> 00:10:35,580
Living the dream.

279
00:10:35,580 --> 00:10:36,620
That's my girl.

280
00:10:36,620 --> 00:10:38,750
Hey, Mr. CEO, you want
to chop some onions?

281
00:10:38,750 --> 00:10:40,460
Sorry, Mr. Chu,
but I'm a little busy out here

282
00:10:40,460 --> 00:10:41,880
putting a face
with the brand, okay?

283
00:10:41,880 --> 00:10:43,580
No, you come in here,
take out the trash.

284
00:10:43,580 --> 00:10:45,500
This guy
knows nothing about branding.

285
00:10:45,500 --> 00:10:46,540
You know what I'm saying?

286
00:10:46,540 --> 00:10:49,080
Anyway, Kevin Gable --
CEO and founder.

287
00:10:50,880 --> 00:10:53,460
Wow! Honey,
look at all these people!

288
00:10:53,460 --> 00:10:56,040
It's working.

289
00:10:53,460 --> 00:10:56,040
Okay,
you sound surprised.

290
00:10:56,040 --> 00:10:57,790
I <i>am. </i>I mean,
I'm not gonna lie.

291
00:10:57,790 --> 00:10:58,880
I went
and got more insurance

292
00:10:58,880 --> 00:11:00,080
in case we had to
torch this puppy.

293
00:11:00,080 --> 00:11:01,580
That's funny.

294
00:11:01,580 --> 00:11:03,710
No, I'm serious. I went and saw
Jeannie on the way over here.

295
00:11:03,710 --> 00:11:04,790
Okay.

296
00:11:04,790 --> 00:11:06,790
In the future,
next time I get a feeling,

297
00:11:06,790 --> 00:11:08,420
you might
want to ride with it.

298
00:11:08,420 --> 00:11:10,080
Okay, you're right,
you're right.

299
00:11:10,080 --> 00:11:12,790
So, what's a girl got to do
to get some pho around here?

300
00:11:12,790 --> 00:11:14,420
Oh, well, for <i>you...</i>

301
00:11:12,790 --> 00:11:14,420
Yeah?

302
00:11:14,420 --> 00:11:15,920
...you got to wait in line
and pay.

303
00:11:15,920 --> 00:11:17,750
'Cause it's -- it's a business.
I'm running a business.

304
00:11:17,750 --> 00:11:19,460
Mm-hmm.

305
00:11:17,750 --> 00:11:19,460
All right,
you're cute, kid.

306
00:11:19,460 --> 00:11:21,080
I'm gonna get you one.
Chicken or beef?

307
00:11:21,080 --> 00:11:22,380
Surprise me.

308
00:11:21,080 --> 00:11:22,380
Okay.

309
00:11:22,380 --> 00:11:24,670
With chicken.

310
00:11:22,380 --> 00:11:24,670
You got it.

311
00:11:24,670 --> 00:11:27,120
Hey, sweetheart, just take one
of these out to your mom, okay?

312
00:11:27,120 --> 00:11:28,460
Okay, yeah,
um, Dad, this lady

313
00:11:28,460 --> 00:11:30,040
wanted hers
with no scallions, so...

314
00:11:30,040 --> 00:11:31,380
Oh. Okay. Uh...

315
00:11:31,380 --> 00:11:32,420
Whoop! Sorry.

316
00:11:32,420 --> 00:11:34,250
Mr. Chu, uh,
this customer right here,

317
00:11:34,250 --> 00:11:35,920
she wanted this one
without scallions.

318
00:11:35,920 --> 00:11:37,380
No special orders.

319
00:11:37,380 --> 00:11:39,500
This is
my grandmother's recipe.

320
00:11:39,500 --> 00:11:41,290
No offense to your grandma,
but she's not here right now,

321
00:11:41,290 --> 00:11:42,580
and this customer <i>is.</i>

322
00:11:42,580 --> 00:11:44,290
So let's just --
let's make an exception.

323
00:11:44,290 --> 00:11:45,960
Okay.

324
00:11:54,580 --> 00:11:56,830
Ahh.
Scallions gone.

325
00:11:56,830 --> 00:11:57,960
Okay.

326
00:11:57,960 --> 00:12:00,750
Clearly, this is not
about the scallions, okay?

327
00:12:00,750 --> 00:12:02,380
Obviously,
you're on a power trip.

328
00:12:02,380 --> 00:12:05,250
Without me, you're just
a thick man in a rusty truck.

329
00:12:05,250 --> 00:12:06,330
Now you don't
like the truck?

330
00:12:06,330 --> 00:12:08,420
I like the truck.
I don't like <i>you.</i>

331
00:12:08,420 --> 00:12:10,540
You don't have to like me.
Just make the soup, okay?

332
00:12:10,540 --> 00:12:11,880
Otherwise,
I'll do it myself.

333
00:12:11,880 --> 00:12:13,420
<i>You </i>make the pho?

334
00:12:15,040 --> 00:12:16,750
It's not
that difficult, okay?

335
00:12:16,750 --> 00:12:19,040
It's basically Cup O' Noodles
with some meat floating around.

336
00:12:19,040 --> 00:12:20,830
Sorry, Grandma Chu.

337
00:12:20,830 --> 00:12:22,040
I don't need this!

338
00:12:22,040 --> 00:12:23,460
Oh!

339
00:12:22,040 --> 00:12:23,460
I quit!

340
00:12:23,460 --> 00:12:25,420
Oh, fine.

341
00:12:26,170 --> 00:12:28,040
No scallions, right, you said?
No scallions.

342
00:12:31,920 --> 00:12:33,380
Okay, uh, we will be
right with you.

343
00:12:33,380 --> 00:12:35,250
The secret's in
the slow cooking.

344
00:12:35,250 --> 00:12:36,460
Dad, we need it now!

345
00:12:36,460 --> 00:12:39,000
It's ready. Here we go,
just like Chu's.

346
00:12:39,000 --> 00:12:40,830
Tell me what you think.

347
00:12:58,290 --> 00:12:59,540
Okay.

348
00:12:59,540 --> 00:13:01,540
I called the investors meeting
a little early this month.

349
00:13:01,540 --> 00:13:03,210
Oh, and I know why.

350
00:13:03,210 --> 00:13:05,330
I drove by.
I saw the line.

351
00:13:05,330 --> 00:13:06,580
Ka-ching, ka-ching!

352
00:13:06,580 --> 00:13:10,040
Yes, and we are gonna continue
having those lines

353
00:13:10,040 --> 00:13:14,670
when we introduce
Kyle's Firehouse Chili!

354
00:13:15,500 --> 00:13:16,670
What?

355
00:13:17,670 --> 00:13:19,710
I thought we were here
to divvy up the profits.

356
00:13:19,710 --> 00:13:22,540
Yeah,
I want to be happy.

357
00:13:22,540 --> 00:13:23,960
You're <i>gonna </i>be happy --

358
00:13:23,960 --> 00:13:27,710
with Kyle's Firehouse Chili!

359
00:13:27,710 --> 00:13:30,920
Something wrong with you?
We sell pho.

360
00:13:30,920 --> 00:13:32,880
We <i>used </i>to. We're pivoting.

361
00:13:32,880 --> 00:13:34,580
Pivoting?

362
00:13:32,880 --> 00:13:34,580
Chale.

363
00:13:35,710 --> 00:13:38,080
"Pivoting" is a business term
to describe

364
00:13:38,080 --> 00:13:41,000
when a company changes
its strategic direction.

365
00:13:41,000 --> 00:13:43,120
Why are we changing <i>anything?
</i>We're doing great.

366
00:13:43,120 --> 00:13:43,920
Yeah.

367
00:13:43,920 --> 00:13:46,250
Hey, where's Mr. Chu.

368
00:13:48,830 --> 00:13:50,040
Chale?

369
00:13:51,380 --> 00:13:55,250
Mr. Chu has pivoted
away from the organization.

370
00:13:55,250 --> 00:13:57,250
So we'll get
a new pho guy.

371
00:13:58,540 --> 00:14:01,670
Uh, we tried that,
but, uh, apparently,

372
00:14:01,670 --> 00:14:05,420
we have been blackballed
by the entire pho community.

373
00:14:05,420 --> 00:14:06,620
What was it
they called me?

374
00:14:06,620 --> 00:14:08,170
Xe tai khi,

375
00:14:08,170 --> 00:14:10,420
which is Vietnamese
for "truck monkey."

376
00:14:10,420 --> 00:14:11,290
Yes.

377
00:14:14,040 --> 00:14:16,960
But don't worry,
because...

378
00:14:16,960 --> 00:14:19,460
No, no, don't point at him
and say "chili" again.

379
00:14:19,460 --> 00:14:20,670
I wasn't
gonna point at him.

380
00:14:20,670 --> 00:14:22,380
I was gonna point at <i>you --
</i>'cause everybody loves

381
00:14:22,380 --> 00:14:25,880
Goody's world-famous
egg salad sandwiches!

382
00:14:26,920 --> 00:14:28,040
Have you lost
your mind?

383
00:14:28,040 --> 00:14:29,420
Okay,
I want my money back.

384
00:14:29,420 --> 00:14:30,920
We got to sell the truck.

385
00:14:29,420 --> 00:14:30,920
Sell the truck.

386
00:14:30,920 --> 00:14:32,040
Yes.

387
00:14:30,920 --> 00:14:32,040
Whoa.
Wait a second.

388
00:14:32,040 --> 00:14:33,460
That is a decision
only the CEO can make,

389
00:14:33,460 --> 00:14:35,920
and that's me,
and I say we're pivoting.

390
00:14:35,920 --> 00:14:37,790
Well, wait a minute --
We are the investors,

391
00:14:37,790 --> 00:14:39,710
so technically,
you work for <i>us.</i>

392
00:14:37,790 --> 00:14:39,710
Mm.

393
00:14:39,710 --> 00:14:42,210
I don't think
it works that way. Chale?

394
00:14:42,210 --> 00:14:44,540
It works
<i>exactly </i>that way.

395
00:14:47,960 --> 00:14:49,880
What did you think
being a boss was?

396
00:14:49,880 --> 00:14:52,880
Just walking around
giving people orders?

397
00:14:52,880 --> 00:14:55,620
Kinda, yeah.

398
00:14:55,620 --> 00:14:57,040
You have to sell the truck.

399
00:14:57,040 --> 00:14:58,790
Now you're on the side
of the investors?

400
00:14:58,790 --> 00:15:00,290
Honey,
it's the only side!

401
00:15:00,290 --> 00:15:02,120
And, by the way,
<i>we </i>are the investors.

402
00:15:02,120 --> 00:15:04,000
We could lose
a lot of money.

403
00:15:04,000 --> 00:15:06,290
Who's gonna be stupid enough
to buy a used pho truck?

404
00:15:06,290 --> 00:15:09,540
You! <i>You </i>bought
a used pho truck!

405
00:15:09,540 --> 00:15:11,000
Look, I called the guy
who sold it to me.

406
00:15:11,000 --> 00:15:12,710
I tried to sell it to <i>him.
</i>He laughed at me.

407
00:15:12,710 --> 00:15:14,540
Yeah, I figured.

408
00:15:14,540 --> 00:15:18,210
But we do know a guy
who would want to buy it,

409
00:15:18,210 --> 00:15:20,500
who's already
<i>in </i>the pho business.

410
00:15:20,500 --> 00:15:22,120
Mr. Chu? No way.

411
00:15:22,120 --> 00:15:23,710
No, he said
he liked the truck.

412
00:15:23,710 --> 00:15:25,210
He knows
you guys were killing it.

413
00:15:25,210 --> 00:15:27,210
He called me a truck monkey!
He hates me!

414
00:15:27,210 --> 00:15:30,750
I know! I know,
but you can talk him into it.

415
00:15:30,750 --> 00:15:32,210
Honey,
when you want something,

416
00:15:32,210 --> 00:15:34,000
you are the most persuasive
person I know.

417
00:15:34,000 --> 00:15:35,040
Okay, that's not true.

418
00:15:35,040 --> 00:15:36,120
Oh, really?

419
00:15:36,120 --> 00:15:38,380
Night we met --
Mulcahy's.

420
00:15:38,380 --> 00:15:40,250
You're out
with your dumb friends,

421
00:15:40,250 --> 00:15:41,330
You're making noise,

422
00:15:41,330 --> 00:15:43,540
and you walk up to me
with your badge out,

423
00:15:43,540 --> 00:15:45,420
and you say, "Excuse me,

424
00:15:45,420 --> 00:15:49,210
I'm gonna have to arrest you
for being too beautiful."

425
00:15:50,210 --> 00:15:51,620
I was pretty smooth, huh?

426
00:15:51,620 --> 00:15:54,710
No, no! Smooth?
No, it wasn't smooth.

427
00:15:54,710 --> 00:15:56,670
You tripped
on the way over.

428
00:15:56,670 --> 00:15:58,420
You were eating a gyro,

429
00:15:58,420 --> 00:16:00,540
and then
when you introduced yourself,

430
00:16:00,540 --> 00:16:03,580
you spit on me -- here.

431
00:16:03,580 --> 00:16:06,040
But you won me over

432
00:16:06,040 --> 00:16:08,880
because you're so charming
and persuasive.

433
00:16:08,880 --> 00:16:12,580
Wow. I got to say, we remember
things completely different.

434
00:16:12,580 --> 00:16:14,290
What?!

435
00:16:12,580 --> 00:16:14,290
Yes!

436
00:16:14,290 --> 00:16:15,710
Okay.

437
00:16:14,290 --> 00:16:15,710
I was at the bar.

438
00:16:15,710 --> 00:16:17,670
I was hanging out,
minding my own business, right?

439
00:16:17,670 --> 00:16:19,710
Mm-hmm.

440
00:16:17,670 --> 00:16:19,710
I notice you
and your girlfriends

441
00:16:19,710 --> 00:16:20,960
are all checking me out.

442
00:16:20,960 --> 00:16:22,170
Oh!

443
00:16:20,960 --> 00:16:22,170
Yeah.

444
00:16:22,170 --> 00:16:24,040
You know how I know?
'Cause the temperature's rising.

445
00:16:24,040 --> 00:16:25,420
The back of my neck
feels like a --

446
00:16:25,420 --> 00:16:27,080
like a tater tot
under a heat lamp.

447
00:16:27,080 --> 00:16:28,580
Oooooh.

448
00:16:28,580 --> 00:16:31,210
And then I turned to you guys --
I remember.

449
00:16:31,210 --> 00:16:33,000
And I looked,
and I said,

450
00:16:33,000 --> 00:16:36,000
"That girl right there,
I'm going to marry her."

451
00:16:36,000 --> 00:16:37,790
And then I thought,
"You know what? Nah.

452
00:16:37,790 --> 00:16:39,960
I'm going for the tall,
skinny one instead."

453
00:16:41,790 --> 00:16:43,920
Best decision
I ever made.

454
00:16:43,920 --> 00:16:45,040
Bet your life.

455
00:16:49,420 --> 00:16:52,290
Okay, there he is.
Remember -- Mulcahy's.

456
00:16:52,290 --> 00:16:53,330
Yeah.

457
00:16:52,290 --> 00:16:53,330
Yeah.

458
00:16:53,330 --> 00:16:54,460
But don't spit on him.

459
00:16:54,460 --> 00:16:55,830
Gotcha.

460
00:16:57,830 --> 00:17:00,960
Mr. Chewbacca!
What up?

461
00:17:03,750 --> 00:17:06,120
I am busy.

462
00:17:06,120 --> 00:17:08,170
I thought about it,
and I just feel terrible

463
00:17:08,170 --> 00:17:10,790
about messing
with your grandmother's recipe.

464
00:17:10,790 --> 00:17:12,380
You know, and if --
if she found out,

465
00:17:12,380 --> 00:17:14,670
I figured she'd be
spinning in her grave.

466
00:17:14,670 --> 00:17:17,540
She's not in a grave.
She's in the kitchen.

467
00:17:18,750 --> 00:17:19,830
You keep her
in the kitchen?

468
00:17:19,830 --> 00:17:21,040
In an urn or something
like that?

469
00:17:21,040 --> 00:17:23,500
That would be disgusting,
right?

470
00:17:23,500 --> 00:17:25,380
Like in a chair,
eating lunch.

471
00:17:25,380 --> 00:17:26,380
She's not dead!

472
00:17:26,380 --> 00:17:27,380
Oh.

473
00:17:27,380 --> 00:17:28,830
How old is she?

474
00:17:28,830 --> 00:17:30,830
I don't know.
She's very old.

475
00:17:30,830 --> 00:17:32,920
Truck. The truck.

476
00:17:32,920 --> 00:17:34,880
Anyway, the point is,

477
00:17:34,880 --> 00:17:37,000
I have to place you
under arrest

478
00:17:37,000 --> 00:17:40,250
for having
some crazy-dreamy eyes.

479
00:17:43,960 --> 00:17:45,290
You know
what I'm talking about?

480
00:17:45,290 --> 00:17:47,120
Honestly, I just --
I just want to swim in them.

481
00:17:47,120 --> 00:17:49,000
I just want
to swim in them.

482
00:17:49,000 --> 00:17:52,120
But you're right -- I'm not cut
out for the food truck business.

483
00:17:52,120 --> 00:17:54,540
But <i>you --
</i>you have a gift.

484
00:17:54,540 --> 00:17:57,170
And all I'm here to do
is try to help you out

485
00:17:57,170 --> 00:17:58,710
and sell you my truck.

486
00:17:58,710 --> 00:18:01,210
I mean, come on.
It's good business.
You know that.

487
00:18:01,210 --> 00:18:03,250
You know,
it already says "pho" on it,

488
00:18:03,250 --> 00:18:04,670
so we're gonna
give you that for free.

489
00:18:04,670 --> 00:18:05,460
That's right.

490
00:18:05,460 --> 00:18:07,960
You know,
you're right.

491
00:18:07,960 --> 00:18:10,210
It's a very good
business.

492
00:18:07,960 --> 00:18:10,210
Mm-hmm.

493
00:18:10,210 --> 00:18:13,040
And that is why I bought
my <i>own </i>food truck today.

494
00:18:13,040 --> 00:18:14,380
See you in the streets.

495
00:18:19,540 --> 00:18:21,960
Okay. Well, I guess
we're stuck with the truck.

496
00:18:21,960 --> 00:18:24,040
Unless you know some other idiot
who's sitting around

497
00:18:24,040 --> 00:18:26,710
with a ton of money
burning a whole in his pocket.

498
00:18:30,830 --> 00:18:33,380
Hey, chief.
What brings you back?

499
00:18:33,380 --> 00:18:35,420
Let me ask you
a question, Owen.

500
00:18:35,420 --> 00:18:37,620
Are you happy?

501
00:18:39,210 --> 00:18:40,210
Wow.

502
00:18:40,210 --> 00:18:41,710
I got to say,
for an old guy,

503
00:18:41,710 --> 00:18:43,670
you're still able to make
a few good decisions.

504
00:18:43,670 --> 00:18:45,540
You're gonna love the truck.
Just make that out to "Gable."

505
00:18:45,540 --> 00:18:48,710
It's G-A-B-L-E.

506
00:18:48,710 --> 00:18:50,790
There you go.
Thanks, bud.

507
00:18:50,790 --> 00:18:52,040
All right.

508
00:18:52,040 --> 00:18:53,580
Yes!

509
00:19:23,830 --> 00:19:24,960
Hey, Mr. Gable.

510
00:19:24,960 --> 00:19:26,580
Uh, did you want
to be alone?

511
00:19:26,580 --> 00:19:28,580
No, no, grab a beer.
Sit down.

512
00:19:29,830 --> 00:19:31,420
I just wanted to say,
um...

513
00:19:31,420 --> 00:19:34,210
I know things didn't turn out
exactly as you expected,

514
00:19:34,210 --> 00:19:37,290
but, um,
I really admire you.

515
00:19:37,290 --> 00:19:38,790
You started a business.

516
00:19:38,790 --> 00:19:40,330
You made something
from nothing,

517
00:19:40,330 --> 00:19:42,250
and that is
a very hard thing to do.

518
00:19:42,250 --> 00:19:44,790
Well, you know,
I had a business for a day.

519
00:19:44,790 --> 00:19:46,710
You know,
I had some customers.

520
00:19:46,710 --> 00:19:48,000
For a moment,
it was pretty sweet.

521
00:19:48,000 --> 00:19:50,330
Oh.
You've been bitten by the bug.

522
00:19:51,620 --> 00:19:54,750
Entrepreneurs like you and me,
Mr. Gable,

523
00:19:54,750 --> 00:19:56,830
we dream big dreams
and we take big risks,

524
00:19:56,830 --> 00:19:59,620
and sometimes,
they don't work out.

525
00:19:59,620 --> 00:20:01,500
But we never give up.

526
00:20:01,500 --> 00:20:03,710
We just regroup
and come back stronger.

527
00:20:04,880 --> 00:20:06,580
Thank you, Chale.
I-I hope so.

528
00:20:06,580 --> 00:20:08,040
I speak from experience.

529
00:20:08,040 --> 00:20:10,500
I once had a business that I was
very passionate about.

530
00:20:10,500 --> 00:20:12,120
It --
it was a great idea.

531
00:20:12,120 --> 00:20:14,500
I put everything I had
into it.

532
00:20:14,500 --> 00:20:15,540
Didn't work out.

533
00:20:15,540 --> 00:20:16,710
What did you do?

534
00:20:16,710 --> 00:20:18,710
Well...

535
00:20:18,710 --> 00:20:19,790
Ohhh.

536
00:20:19,790 --> 00:20:21,170
I, uh...

537
00:20:22,670 --> 00:20:24,000
You moved in my garage,
didn't you?

538
00:20:24,000 --> 00:20:25,830
Yeah.

539
00:20:24,000 --> 00:20:25,830
Yes.

540
00:20:25,830 --> 00:20:28,830
And you live there rent-free
so you could "regroup."

541
00:20:30,080 --> 00:20:31,790
I probably should've
thought this all through

542
00:20:31,790 --> 00:20:33,380
before I started talking.

543
00:20:31,790 --> 00:20:33,380
Yeah.

544
00:20:33,380 --> 00:20:34,500
Um...

545
00:20:34,500 --> 00:20:35,960
I will leave you
to your thoughts.

546
00:20:35,960 --> 00:20:37,040
Okay.

547
00:20:37,040 --> 00:20:38,920
And leave the beer
to my thoughts, as well.

