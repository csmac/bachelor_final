1
00:02:25,854 --> 00:02:28,403
Very good!
Look straight at me.

2
00:02:28,774 --> 00:02:30,117
Okay, let's take five.

3
00:02:30,484 --> 00:02:33,488
Five minutes everyone!
See you back in 10.

4
00:02:51,880 --> 00:02:53,882
I'm so hungry!

5
00:03:17,155 --> 00:03:19,123
Let's switch to 12.

6
00:03:28,083 --> 00:03:30,336
Ready? All right, let's go.

7
00:03:33,171 --> 00:03:34,764
Very good!

8
00:03:35,132 --> 00:03:36,600
Nice.

9
00:03:36,967 --> 00:03:39,641
Remember, your chin...
Yeah! Very nice!

10
00:03:40,011 --> 00:03:41,388
Yeah, that's perfect!

11
00:03:41,763 --> 00:03:46,894
One, two, three!
The other side, again!

12
00:03:47,269 --> 00:03:50,739
Very good. Let's go!

13
00:05:08,600 --> 00:05:12,355
You're going to be a dad.
That should be fun.

14
00:05:12,729 --> 00:05:16,825
That's great.
So it'll be in... July?

15
00:05:17,192 --> 00:05:23,199
Well... I don't really know.
I'll have to check my calendar.

16
00:05:23,573 --> 00:05:29,501
<i>I'll call you and we can decide.
I'm busy with the party.</i>

17
00:05:29,871 --> 00:05:34,342
The girls are coming home. I guess
it's time to get this show on the road.

18
00:05:34,709 --> 00:05:40,182
- Or when she's in heat. She's lovely.
- A bit low-set.

19
00:05:40,549 --> 00:05:47,273
- It's never perfect.
- That's true. Nobody's perfect.

20
00:06:51,620 --> 00:06:54,624
Have you seen my purse?
The small one?

21
00:06:54,998 --> 00:06:57,751
No.

22
00:07:07,177 --> 00:07:11,853
- Where are you going?
- I'm going home.

23
00:07:29,532 --> 00:07:35,335
Elsa! Lovisa!
Come on, you two. Time to go!

24
00:07:35,705 --> 00:07:40,836
Do we take it easy
or do we go for it? Take it!

25
00:07:41,211 --> 00:07:46,684
<i>- I'll stay up here.
- Are you ready, or what?</i>

26
00:07:47,050 --> 00:07:49,803
- Huh? Hello!
- You're not dressed.

27
00:07:50,178 --> 00:07:53,603
- Grandma...
- So should I get the food?

28
00:07:53,974 --> 00:07:57,399
Get dressed!

29
00:08:03,608 --> 00:08:07,738
Sweden is so clean;
that's something you forget.

30
00:09:05,378 --> 00:09:09,884
- Hello, mom!
- My little sweetheart...

31
00:09:10,258 --> 00:09:13,432
It's great to see you.

32
00:09:22,479 --> 00:09:26,404
- It's so great to see you.
- Move your bag over there.

33
00:09:26,775 --> 00:09:30,075
- It's in the way where it is now.
- Oh, sorry.

34
00:09:30,445 --> 00:09:32,789
How's the party coming?

35
00:09:33,156 --> 00:09:36,330
Hello? Oh my...

36
00:09:40,622 --> 00:09:44,798
- Hey, sweetie!
- Hello! Darling little sister...

37
00:09:45,168 --> 00:09:49,014
- Good to see you.
- We'll put this away with you here.

38
00:09:49,380 --> 00:09:54,011
- You look great. What happened?
- Nothing. Why?

39
00:09:54,385 --> 00:09:59,016
- Did Janne finally go down on you?
- Cut it out...

40
00:09:59,390 --> 00:10:04,191
- You can feel great without good sex.
- Really? I didn't know that.

41
00:10:04,562 --> 00:10:08,032
- Mommy's tiny little sweetheart!
- Yes...

42
00:10:08,399 --> 00:10:13,906
God, that's a lot of luggage.
Katarina, you'll have to help out.

43
00:10:14,280 --> 00:10:17,375
<i>I don't know what to grab.
I guess I'll take...</i>

44
00:10:19,828 --> 00:10:26,461
- How nice! I've missed this house.
- Everything is the same here.

45
00:10:28,294 --> 00:10:31,764
Great. This place is so calming.

46
00:10:32,132 --> 00:10:36,182
I guess so...
Well, nothing ever happens here.

47
00:10:36,553 --> 00:10:41,354
You'd go nuts here,
you're used to the big city.

48
00:10:41,724 --> 00:10:48,323
That's true, but this is nice
for a while.

49
00:11:17,969 --> 00:11:21,769
Oh, stop it! Turn that off.

50
00:11:27,312 --> 00:11:33,740
- I'm not kidding. I'll beat you!
- Oh, I'm so scared...

51
00:11:34,110 --> 00:11:36,989
Oh my!

52
00:11:38,781 --> 00:11:41,534
Ouch!

53
00:11:45,371 --> 00:11:49,922
- This one is so beautiful.
- I think it's cheesy.

54
00:11:50,293 --> 00:11:57,097
- It's lovely. Don't you have dreams?
- What?

55
00:11:57,467 --> 00:12:03,270
- She hears poorly. Don't mention it.
- What, does she need a hearing aid?

56
00:12:03,640 --> 00:12:10,023
Oh God, you put this on? Come here.

57
00:12:47,267 --> 00:12:51,363
- Whoops...
- You OK?

58
00:12:55,775 --> 00:13:00,497
- Hello?
- Oh good, the food. Let's eat!

59
00:13:00,863 --> 00:13:03,958
Great... Got it.

60
00:13:04,325 --> 00:13:08,250
- Hello!
- Hey, Janne. It's been a while.

61
00:13:08,621 --> 00:13:11,170
- Here, have some wine.
- Thanks.

62
00:13:11,541 --> 00:13:15,591
Will you get some napkins
in the kitchen? This will be messy.

63
00:13:15,962 --> 00:13:20,684
- You want me to go?
- Yes, please.

64
00:13:21,050 --> 00:13:24,680
- I can't believe...
- Elin!

65
00:13:25,054 --> 00:13:30,652
I'm so out of it... Lova,
will you bring the punsch glasses?

66
00:13:31,019 --> 00:13:33,898
- What?
- The punsch glasses, please.

67
00:13:34,272 --> 00:13:39,278
- I don't recall mom ever cooking.
- No, it was rare.

68
00:13:39,652 --> 00:13:43,907
What on earth? I was slaving away
in the kitchen all the time.

69
00:13:44,282 --> 00:13:47,331
- Don't you remember?
- No!

70
00:13:47,702 --> 00:13:52,424
- So what did you make?
- Well, all sorts of things.

71
00:13:52,790 --> 00:13:57,637
- Such as?
- Regular meals. Lova, where are you?

72
00:13:58,004 --> 00:14:01,850
<i>Don't be childish. That's enough.
I'll have a piece of bread.</i>

73
00:14:09,098 --> 00:14:13,478
- Shake a paw!
- Shake a paw. Paw!

74
00:14:13,853 --> 00:14:16,652
- Sit.
- Stay! Look at this!

75
00:14:42,799 --> 00:14:47,726
- One, two, three, four, five.
- Mom?

76
00:14:49,180 --> 00:14:52,184
Why did you turn my room
into an office?

77
00:14:52,558 --> 00:14:57,064
The office?
That was never your room, was it?

78
00:14:57,438 --> 00:15:01,944
- Yes, it was.
- The little ones are asleep now.

79
00:15:02,318 --> 00:15:04,946
What are you doing?

80
00:15:08,324 --> 00:15:14,252
- Did you forget that it was my room?
- You lived in the servant's room.

81
00:15:14,622 --> 00:15:19,173
- Lova stayed in the office a while.
- No.

82
00:15:19,544 --> 00:15:24,220
- Sure. You usually stayed away.
- No, I didn't.

83
00:15:24,590 --> 00:15:28,561
- Didn't Lova stay in the office?
- I stayed in the servant's room...

84
00:15:28,928 --> 00:15:34,776
...for a month before Milan - true.
But my entire childhood...

85
00:15:35,143 --> 00:15:39,023
- ... was spent in the office.
- I'm sure you're right.

86
00:15:40,940 --> 00:15:46,947
- Check out the dork's room.

87
00:15:58,374 --> 00:16:02,379
Look, everything is still here.

88
00:16:09,635 --> 00:16:12,605
- What are you looking for?
- Nothing.

89
00:16:15,224 --> 00:16:19,695
My equestrian prizes!
I was pretty damn good.

90
00:16:21,606 --> 00:16:24,280
I was!

91
00:16:28,362 --> 00:16:32,708
Tomorrow is the big day.
Nothing can go wrong.

92
00:16:33,075 --> 00:16:38,707
Carelessness, laziness, and disorder
will not be accepted.

93
00:16:39,081 --> 00:16:42,711
- Katti, you know what to do?
- Yes.

94
00:16:43,085 --> 00:16:47,511
Janne can get some infrared heaters
in case it gets cold.

95
00:16:47,882 --> 00:16:52,638
- Really? OK, fine.
- Sure thing.

96
00:16:53,012 --> 00:16:55,686
- What did you say?
- Sure.

97
00:16:56,057 --> 00:17:00,187
- I never hear what he's saying.
- Mom!

98
00:17:00,561 --> 00:17:04,156
I can do the seating arrangement
and set the table.

99
00:17:04,524 --> 00:17:10,622
That won't work, darling. You know
you can't keep track of who's who.

100
00:17:10,988 --> 00:17:15,368
- Just make a list!
- I don't want to do a goddamn list.

101
00:17:15,743 --> 00:17:20,795
Lists are pretty good, actually.
I just know that I can...

102
00:17:21,165 --> 00:17:24,544
- I can't be bothered.
- Well, can I finish?

103
00:17:24,919 --> 00:17:28,924
- You're always going on and on.
- No, I don't.

104
00:17:29,298 --> 00:17:34,771
- Elin can't do just one thing.
- You fucking bitch!

105
00:17:35,137 --> 00:17:40,234
- Don't fight. I can't stand it.
- Neither can I.

106
00:17:40,601 --> 00:17:43,946
<i>I'll take the dogs out.</i>

107
00:17:44,313 --> 00:17:48,489
Come on, boy. Come here, Saga.
Come, come!

108
00:17:48,859 --> 00:17:54,912
Where are the leashes?
Come here, everybody!

109
00:17:58,160 --> 00:18:02,757
There, there. Let's go outside.

110
00:18:03,124 --> 00:18:06,094
There...

111
00:18:08,379 --> 00:18:11,178
<i>- Are you mad now?
- Yes, I am.</i>

112
00:18:11,549 --> 00:18:18,478
We just got here, and you're already
nagging about lists and crap.

113
00:18:20,516 --> 00:18:26,194
- She just leaves when it gets tough.
- Fucking whore, fucking whore...

114
00:18:26,564 --> 00:18:30,660
<i>- Yes, I'm mad. You're all idiots.
- Well, excuse me!</i>

115
00:18:31,027 --> 00:18:35,248
- When did you ever fix anything?
- I fixed these cookies.

116
00:18:35,615 --> 00:18:40,246
- You didn't fix them!
- I put them out, didn't I?

117
00:18:40,620 --> 00:18:46,343
- ... an ass-fuck, thank you very much.
- Janne!

118
00:18:46,709 --> 00:18:53,137
- What is he talking about?
- He gets weird when he's drunk.

119
00:18:53,507 --> 00:18:56,386
- Hello.
- Hello...

120
00:18:56,761 --> 00:19:00,686
Give me a hand here.
Why do you drink like this?

121
00:19:01,057 --> 00:19:06,154
- Fuck you, fuck you.
- I'll pretend I didn't hear that.

122
00:19:10,775 --> 00:19:14,746
Jesus! Goddamn it!

123
00:19:16,030 --> 00:19:19,284
No, no...

124
00:19:21,035 --> 00:19:27,759
- Try to help out, Janne.
- Time for you to have a shower.

125
00:19:29,168 --> 00:19:32,342
- He has no idea where he is.
- I usually don't either.

126
00:19:32,713 --> 00:19:36,468
But I don't usually
get hosed off for that.

127
00:19:38,302 --> 00:19:42,557
Jesus... Just wait a little.
Easy, easy!

128
00:19:56,028 --> 00:20:02,582
Oh, good! I'm absolutely exhausted.

129
00:20:02,952 --> 00:20:05,626
- I'm so tired.
- So was Janne.

130
00:20:05,996 --> 00:20:11,878
- Elin...
- I just meant that he went to bed.

131
00:20:12,253 --> 00:20:16,178
Here? That's unexpected.

132
00:20:16,549 --> 00:20:23,433
Not a word during dinner, and then
he goes to bed when he's not home.

133
00:20:31,355 --> 00:20:34,154
I have to go. Work.

134
00:20:37,361 --> 00:20:40,786
That's fine. Off you go!

135
00:20:41,157 --> 00:20:44,001
- Elin...
- Yes.

136
00:20:44,368 --> 00:20:48,339
Finish this.
You have to learn at some point.

137
00:20:48,706 --> 00:20:51,926
You put away all the dishes
before moving on.

138
00:20:52,293 --> 00:20:57,424
Yes. I'm pretty sure I can handle
putting things in the dishwasher.

139
00:20:57,798 --> 00:21:01,553
- Take care of it now then.
- Yes, sure.

140
00:21:13,981 --> 00:21:19,078
It sucked being at home.
My whole family was there.

141
00:21:21,530 --> 00:21:24,909
- I've missed you.
- Is that so?

142
00:21:25,284 --> 00:21:27,707
Definitely.

143
00:22:55,207 --> 00:22:59,383
- Hello...
- Good morning.

144
00:23:09,972 --> 00:23:13,021
- What do we do?
- Let's go, me and you.

145
00:23:13,392 --> 00:23:17,488
- Saint Tropez.
- Drinks are on me.

146
00:23:19,189 --> 00:23:21,738
Come on, you little psycho!

147
00:23:22,109 --> 00:23:25,158
Go after the ball.

148
00:23:28,532 --> 00:23:32,628
You know... I want the serve.

149
00:23:35,956 --> 00:23:38,880
<i>- Does it have to bounce first?
- Yes, it does.</i>

150
00:23:39,251 --> 00:23:43,051
- Can't we play like badminton?
- No, we can't.

151
00:24:06,528 --> 00:24:11,910
You must know what I'm talking about.
We're just passing each other.

152
00:24:23,545 --> 00:24:26,139
It's not enough for me.

153
00:24:28,300 --> 00:24:32,146
- I want more than this.
- OK, fine.

154
00:24:34,139 --> 00:24:37,894
I want more.
I don't want to live like this.

155
00:24:48,946 --> 00:24:53,793
Yes, there you are!
My lovely little dogs.

156
00:24:54,159 --> 00:24:57,083
There you are!

157
00:25:43,792 --> 00:25:48,218
- Hello!
- Well, there are a lot of us.

158
00:25:52,176 --> 00:25:57,353
- Hey! Don't sneak off, now.
- I've already finished my tasks.

159
00:25:57,723 --> 00:26:02,229
- Nice dress!
- Thanks. You too.

160
00:26:04,271 --> 00:26:09,243
Lova? Lova?

161
00:26:11,945 --> 00:26:14,915
Girls?

162
00:26:15,282 --> 00:26:19,788
Let's see, this will be your seat.
Darling, no!

163
00:26:20,162 --> 00:26:26,670
Your shoes don't go on the table.
Katarina, let's make standing fans.

164
00:26:27,044 --> 00:26:29,797
- Kisses!
- Kisses...

165
00:26:34,009 --> 00:26:38,515
- I usually do this.
- Not today, Elin. We don't have time.

166
00:26:38,889 --> 00:26:45,613
- Stop being silly.
- What do you mean? I'm not silly.

167
00:26:45,938 --> 00:26:50,910
- Shut up!
- Oh, sorry.

168
00:26:51,276 --> 00:26:55,406
- What's the matter?
- Nothing...

169
00:26:55,781 --> 00:27:01,254
Nothing's the matter.
Everything's fine...

170
00:27:03,705 --> 00:27:09,178
- Something's up with her.
- "Something's up. I'm silly. "

171
00:27:09,545 --> 00:27:13,595
- How the hell are you?
- Oh, so you're a doctor and a model?

172
00:27:13,966 --> 00:27:19,769
- Or neither, perhaps. Which is it?
- Fucking bitch!

173
00:28:14,234 --> 00:28:16,862
Come here.

174
00:28:24,369 --> 00:28:27,373
- Thanks.
- Thank you. Bye!

175
00:28:29,041 --> 00:28:31,590
- Come on, girls.
- Hello.

176
00:28:31,960 --> 00:28:36,136
- Would you come with me, please?
- Excuse me'?

177
00:28:40,344 --> 00:28:45,396
- Excuse me? How's the dessert done?
- We're not serving the dessert.

178
00:28:45,766 --> 00:28:49,236
- Why? Who is?
- I don't know. Isn't it a buffet?

179
00:28:49,603 --> 00:28:53,779
- Some things ought to be served.
- It'll work out fine.

180
00:28:54,149 --> 00:28:56,402
Can you take a break?

181
00:28:59,947 --> 00:29:03,952
Hello? Yes, speaking.

182
00:29:05,535 --> 00:29:08,960
- I'm sorry, Kattis.
- What the hell is wrong with you?

183
00:29:09,331 --> 00:29:15,384
You're above the rest of us.
Why did you even come home?

184
00:29:17,089 --> 00:29:20,093
Here, get in.

185
00:29:22,844 --> 00:29:27,850
- You're a real fuck-up!
- I didn't mean for this to happen.

186
00:29:28,225 --> 00:29:30,899
Yeah well, it did.

187
00:29:31,270 --> 00:29:34,319
So how was work last night?

188
00:29:36,233 --> 00:29:39,908
- You can't ride with us. Get out!
- Honestly?

189
00:29:40,279 --> 00:29:47,083
Get out of the car. Get out!
Get out of my car!

190
00:30:11,977 --> 00:30:18,280
I was at the crayfish party.
Why don't you sing that song again?

191
00:30:18,650 --> 00:30:22,530
Sing it to us. Go on!
'"Stina was sitting... "

192
00:30:35,751 --> 00:30:39,972
- So mom had a thing with a German?
- Yes, with several.

193
00:30:40,339 --> 00:30:43,434
- What were their names?
- Helmut.

194
00:30:43,800 --> 00:30:46,098
- All of them?
- No...

195
00:30:50,807 --> 00:30:52,901
Here's to Helmut!

196
00:31:01,985 --> 00:31:07,287
Hooray! Hooray! Hooray! Hooray!
Cheers!

197
00:31:07,657 --> 00:31:11,002
For Sigrid-

198
00:31:11,370 --> 00:31:15,000
- who turns 70 today.

199
00:31:16,708 --> 00:31:19,962
<i>I'll be darned - 70 years old!</i>

200
00:31:20,337 --> 00:31:24,763
Who would believe me,
looking at her today?

201
00:31:26,551 --> 00:31:31,273
- Well, my dear Ingrid...
- Oh no!

202
00:31:31,640 --> 00:31:37,192
- That funny! I'm not Ingrid.
- No, my mistake.

203
00:31:37,562 --> 00:31:42,409
- A slip of the tongue...
- You're worse than the King.

204
00:31:42,776 --> 00:31:48,829
I think... Wait, I don't know...
I think I'm having a heart attack.

205
00:31:55,580 --> 00:31:58,925
- What's going on?
- I don't know.

206
00:31:59,292 --> 00:32:03,513
What's going on? What's wrong?

207
00:32:03,880 --> 00:32:08,101
- What's wrong?
- I can't breathe.

208
00:32:09,428 --> 00:32:14,104
- Easy! Do you know where you are?
- Yes... Yes, I know.

209
00:32:15,767 --> 00:32:20,147
Don't hover over her, she needs air.

210
00:32:20,522 --> 00:32:23,526
Just stay calm and breathe.

211
00:32:26,987 --> 00:32:30,082
- There you go...
- Embarrassing.

212
00:32:39,583 --> 00:32:42,553
You'll feel much better after this.

213
00:32:52,929 --> 00:32:54,397
There!

214
00:33:33,386 --> 00:33:35,980
Mother...

215
00:33:36,348 --> 00:33:40,228
Happy 70th birthday!

216
00:33:40,602 --> 00:33:45,779
That's a big deal!
It's a big deal.

217
00:33:46,149 --> 00:33:52,373
We're so happy for you,
and we're proud to be here today.

218
00:34:00,163 --> 00:34:04,384
Having you for a mother
is truly something to be proud of.

219
00:34:04,751 --> 00:34:07,971
At least, I am.

220
00:34:09,464 --> 00:34:11,933
Mother...

221
00:34:14,719 --> 00:34:19,145
- You want things to be good.
- Things are good.

222
00:34:19,516 --> 00:34:24,488
For you to look at me,
at your daughters, and be proud.

223
00:34:24,813 --> 00:34:30,115
- Well, I am.
- That goes for Love and me.

224
00:34:45,750 --> 00:34:49,425
You want to be a good daughter.

225
00:34:50,839 --> 00:34:55,345
We want... That's what you want.

226
00:34:55,719 --> 00:35:00,316
We have had such a hard time
finding you a gift.

227
00:35:00,682 --> 00:35:05,984
It's hard to find something
for someone who has everything.

228
00:35:06,313 --> 00:35:10,363
But happy birthday!

229
00:35:11,943 --> 00:35:16,369
Thank you, sweetheart.
That's enough, it was great.

230
00:35:16,740 --> 00:35:19,368
- Cheers, everyone!
- Cheers!

231
00:35:19,743 --> 00:35:25,375
For our mom, who's turning 70!
That's insanely old...

232
00:38:30,099 --> 00:38:33,569
Bravo! Bravo!

233
00:38:57,126 --> 00:39:03,133
What the hell? There's a fire...
There's a fire. There's a damn fire!

234
00:39:03,508 --> 00:39:07,012
- What is going on?
- There's a fire!

235
00:39:07,387 --> 00:39:12,018
- Get a fire-extinguisher!
- Be careful, Janne!

236
00:39:12,392 --> 00:39:14,394
Janne!

237
00:39:26,239 --> 00:39:32,042
Come here, it'll be OK.
Daddy's going to take care of it.

238
00:39:38,626 --> 00:39:42,847
- You got it?
- Looking good. That's good!

239
00:39:45,174 --> 00:39:49,975
I think it's a damn...
It's just a suitcase!

240
00:39:50,346 --> 00:39:54,943
- Did you put it out?
- It was just a suitcase.

241
00:41:49,757 --> 00:41:53,227
- Hello? We're leaving now.
- No.

242
00:41:53,594 --> 00:41:57,644
<i>- Yes, we're staying with grandma.
- I don't want to.</i>

243
00:41:58,016 --> 00:42:01,646
- They don't want to.
- No, I can see that.

244
00:42:03,730 --> 00:42:10,614
Isn't it better if we do something
fun together? A road trip, an outing?

245
00:42:14,157 --> 00:42:17,787
- Bye-bye, daddy.
- Bye.

246
00:43:10,379 --> 00:43:15,931
- What's this?
- I'm cooking for us.

247
00:43:17,762 --> 00:43:21,812
Does it say potatoes with peel?
It's hard to read.

248
00:43:22,183 --> 00:43:26,780
- Put your glasses on.
- I am, sweetheart.

249
00:43:27,146 --> 00:43:31,822
Not my reading glasses...
It says potatoes, but that's fine.

250
00:43:32,193 --> 00:43:35,072
I'm sure you've rinsed them?

251
00:43:36,989 --> 00:43:40,960
- Yes, I think I did.
- Perhaps you should peel them.

252
00:43:41,327 --> 00:43:44,376
There are so many black spots.

253
00:43:44,747 --> 00:43:48,047
- Are you excited about the dog meet?
- Sorry?

254
00:43:48,417 --> 00:43:52,888
- Are you excited about the dog meet?
- I don't know about excited...

255
00:43:53,256 --> 00:43:57,978
<i>But it's tradition,
so I guess I'll go.</i>

256
00:43:58,344 --> 00:44:02,269
- Lova and I can care for the house.
- Sure.

257
00:44:04,767 --> 00:44:08,817
I'd like... I'm a bit tired
of all this running around.

258
00:44:11,190 --> 00:44:15,115
- Are you cooking?
- Yes.

259
00:44:15,486 --> 00:44:19,787
- Do you want my help?
- Sure. Peel these mini-onions.

260
00:44:20,158 --> 00:44:23,503
- The shallots?
- Right, that's what they're called.

261
00:44:29,417 --> 00:44:36,266
- Aren't you bored here?
- Yes, it's pretty boring.

262
00:44:41,596 --> 00:44:43,849
<i>I'll get it.</i>

263
00:44:48,144 --> 00:44:51,990
Don't you think we've seen enough
of each other?

264
00:45:03,492 --> 00:45:06,245
Grandma!

265
00:45:28,351 --> 00:45:32,948
- The soup was very good, Elin.
- Thanks.

266
00:45:33,314 --> 00:45:38,866
- Where is it from?
- A friend made it one night in Paris.

267
00:45:39,237 --> 00:45:43,367
Mom, is it OK if we stay here
for a while?

268
00:45:43,741 --> 00:45:48,463
- Here?
- She wants us to leave.

269
00:45:51,958 --> 00:45:55,428
- You don't want us here?
- She said so earlier.

270
00:45:55,795 --> 00:45:59,174
No, I didn't say that.

271
00:46:02,760 --> 00:46:06,731
What kind of spices did you use?

272
00:46:07,098 --> 00:46:10,728
You know sometimes
you're a fucking asshole!

273
00:46:11,102 --> 00:46:14,322
Either way, I have a trip planned.

274
00:46:14,689 --> 00:46:17,943
There's a dog meet.
I can't stay here.

275
00:46:18,317 --> 00:46:23,915
- I'm the one who should leave!
- Elin!

276
00:46:24,282 --> 00:46:29,004
I'm messed up, I'm the problem!
I should leave this damn house.

277
00:46:29,370 --> 00:46:36,049
- I'm the one with the problem, right?
- I don't want to hear this.

278
00:46:37,086 --> 00:46:40,260
Stop running,
and start caring about someone else!

279
00:46:40,631 --> 00:46:46,559
Kattis is in there, wanting to stay,
and you don't care worth a damn!

280
00:46:46,929 --> 00:46:52,277
- Stop it, Elin!
- The hell I will! She doesn't care.

281
00:46:54,312 --> 00:47:01,241
Katarina, you're welcome to stay here
for as long as you want.

282
00:47:03,112 --> 00:47:07,208
But you're leaving, aren't you?

283
00:47:07,575 --> 00:47:13,253
You have a very, very important life
somewhere far from here, don't you?

284
00:47:13,622 --> 00:47:20,096
Since when do you care
about what goes on here?

285
00:47:20,463 --> 00:47:26,061
I was 15, a fucking teenager,
when you sent me to Milan.

286
00:47:26,427 --> 00:47:30,148
- Hey...
- I was a child, and you sent me away!

287
00:47:30,514 --> 00:47:34,394
- You threw a child out of the house.
- Don't you blame me!

288
00:47:34,769 --> 00:47:37,818
Don't blame me
for what your life is like!

289
00:47:38,189 --> 00:47:41,113
I only blame you for being a psycho.

290
00:47:41,484 --> 00:47:45,034
I was 15 years old -
a fucking teenager!

291
00:47:45,404 --> 00:47:50,160
And you told me
I didn't have to come back.

292
00:48:25,152 --> 00:48:28,998
- You know something?
- What?

293
00:48:29,365 --> 00:48:32,744
I'm having an affair
with a guy at work.

294
00:48:39,375 --> 00:48:44,472
He's a medical intern
and a lot younger.

295
00:48:46,590 --> 00:48:51,847
- Oh, sorry. Do you want to know this?
- Yeah.

296
00:48:53,431 --> 00:48:58,437
- What, does Janne know?
- No. Are you crazy?

297
00:48:58,811 --> 00:49:03,533
Well, you stop caring
after a while, don't you?

298
00:49:03,899 --> 00:49:06,527
No...

299
00:49:13,284 --> 00:49:16,629
- Look!
- Yes...

300
00:49:19,373 --> 00:49:25,096
They're getting a bit old.
Perhaps we should trim them.

301
00:49:28,507 --> 00:49:35,186
Should we pick some
to put on the table? I love that.

302
00:49:38,184 --> 00:49:40,903
- Yes.
- Should we?

303
00:49:41,270 --> 00:49:44,114
You or I?

304
00:49:45,733 --> 00:49:50,204
Their stems are tough,
perhaps it's not a great idea.

305
00:49:50,571 --> 00:49:57,580
- Have you gained weight? I think so.
- No, I haven't!

306
00:49:58,913 --> 00:50:03,794
I don't know
if I should ask you about...

307
00:50:04,168 --> 00:50:08,389
Do Elsa and Lovisa need
some stuff as well?

308
00:50:08,756 --> 00:50:12,101
Should I bring something for them?

309
00:50:16,430 --> 00:50:19,650
This is so hard...

310
00:50:21,852 --> 00:50:25,152
Well, yes... Of course.

311
00:50:42,164 --> 00:50:45,794
Hey, I'm sure it'll be OK.

312
00:50:46,168 --> 00:50:49,547
Or, well, I don't know...

313
00:50:56,011 --> 00:51:00,858
- Why is she so mean? I don't get it.
- I don't know.

314
00:51:01,225 --> 00:51:07,323
She drugged me.
That's pretty mean too, right?

315
00:51:11,735 --> 00:51:17,708
Maybe I'm heading for a
collapse one of these days as well.

316
00:51:43,183 --> 00:51:45,311
Hello!

317
00:51:47,605 --> 00:51:49,903
- Come, sweetie!
- How are they?

318
00:51:50,274 --> 00:51:53,949
Good, except for little Minnie
who's been ill.

319
00:51:54,320 --> 00:51:59,952
She's been to the vet 7 times
this fall. It's awfully expensive.

320
00:52:00,326 --> 00:52:04,581
- But she's much better now.
- And how are things with you?

321
00:52:08,125 --> 00:52:12,972
- See you later!
- See you. Take care now!

322
00:52:13,339 --> 00:52:16,343
There you go, sweetie.

323
00:52:35,736 --> 00:52:39,115
- So V is forward?
- Yep, W.

324
00:52:39,490 --> 00:52:42,334
Why won't he die?
Why can't I shoot him?

325
00:52:42,701 --> 00:52:48,049
- Well, those are your friends.
- But I want to shoot them.

326
00:52:49,541 --> 00:52:55,219
I don't think... That's your team,
I don't think they'll die.

327
00:52:55,589 --> 00:53:01,016
But why don't you focus
on the enemy right there instead?

328
00:53:01,387 --> 00:53:05,233
- R... R... Hand grenade.
- Reload is done.

329
00:53:05,599 --> 00:53:09,649
You're throwing it at your friends,
but that's fine.

330
00:53:10,020 --> 00:53:14,696
- I can't kill them.
- No, but yourself. If you look...

331
00:53:17,111 --> 00:53:20,411
Hello? Right.

332
00:53:29,915 --> 00:53:31,633
What?

333
00:53:37,589 --> 00:53:40,843
Katti, tell me what happened.

334
00:54:08,162 --> 00:54:11,382
What are you talking about?
Just say what happened.

335
00:54:11,749 --> 00:54:16,755
<i>Elin, I'll handle this.
Why don't you go sit down?</i>

336
00:54:17,796 --> 00:54:23,803
- You'll fix it. You know this stuff.
- We have to wait for the results.

337
00:55:51,557 --> 00:55:56,734
<i>Let me know if you're hungry later.
I'll be right next door.</i>

338
00:55:57,104 --> 00:55:59,607
Thank you, Linn.

339
00:57:39,122 --> 00:57:43,719
Will you two stop for a little while?
Come over here.

340
00:57:54,179 --> 00:57:59,811
<i>Even if things between daddy and me
aren't the same as they used to be-</i>

341
00:58:00,185 --> 00:58:05,032
- we will always love you two
very, very much.

342
00:58:05,399 --> 00:58:09,245
We love you so much we almost break.

343
00:58:11,238 --> 00:58:14,538
And that will never change.

344
00:58:16,910 --> 00:58:19,129
No matter what.

345
00:58:19,496 --> 00:58:23,421
It's super-important
that you understand that.

346
00:58:33,093 --> 00:58:36,597
I get that you're tired of us, but...

347
00:58:43,353 --> 00:58:50,032
But I...
Nothing is really working right now.

348
00:58:52,404 --> 00:58:56,125
And I don't understand why.

349
00:58:59,119 --> 00:59:05,968
We miss you. It's not as much fun,
and they're so tiresome.

350
00:59:07,544 --> 00:59:10,388
I sort of thought that...

351
00:59:10,756 --> 00:59:16,889
That it'd be nice to come home
and all, but I just feel...

352
00:59:18,764 --> 00:59:24,066
I'm sorry I'm such a handful
all time and...

353
00:59:26,313 --> 00:59:31,114
Kattis is sort of...
She's always so good.

354
00:59:31,485 --> 00:59:34,329
I don't know...

355
01:00:58,488 --> 01:01:04,461
Elin 15 years old!
First model assignment

356
01:03:11,955 --> 01:03:14,253
Mom...

357
01:03:18,586 --> 01:03:20,930
Mom?

358
01:03:30,098 --> 01:03:36,777
You'd better apologize. Or at least
admit that what you did was wrong.

359
01:03:38,440 --> 01:03:41,785
You were wrong.

360
01:03:53,955 --> 01:03:57,505
You old hag...

361
01:04:17,812 --> 01:04:20,861
You're not the worst mom
in the world.

362
01:04:21,232 --> 01:04:24,782
I did mean it, but you're not.

363
01:04:26,112 --> 01:04:32,586
I was going to tell you when you
came back, but you didn't come back.

364
01:04:32,952 --> 01:04:37,583
Which is standard for you,
you old hag.

365
01:04:56,768 --> 01:05:02,366
Everything usually just works out,
but I don't think it will this time.

366
01:05:06,277 --> 01:05:09,030
Let's drink to that.

367
01:05:19,916 --> 01:05:23,170
So this is where you are?

368
01:05:29,092 --> 01:05:34,394
- This is a guy... What was your name?
- Tommy.

369
01:05:34,764 --> 01:05:39,065
- Hello, Tommy. We're leaving now.
- I don't want to.

370
01:05:41,187 --> 01:05:45,988
Come on. Let's sit down.

371
01:05:53,908 --> 01:05:58,209
Elin, please work with me!
I love you.

372
01:05:58,580 --> 01:06:05,259
- But you're no walk in the park.
- Neither are you.

373
01:06:13,136 --> 01:06:16,106
- Can I have a glass of white wine?
- Sure.

374
01:06:18,433 --> 01:06:21,186
- Thanks.
- You're welcome.

375
01:06:23,813 --> 01:06:28,364
<i>- Better?
- Yes, a little bit.</i>

376
01:06:37,243 --> 01:06:40,247
You know something?

377
01:06:41,164 --> 01:06:46,136
I was always jealous of you.
It's true!

378
01:06:46,503 --> 01:06:50,724
You never pretend to be
someone you're not.

379
01:06:51,841 --> 01:06:55,766
It's an extremely rare quality.

380
01:07:04,771 --> 01:07:07,149
Oh...

381
01:07:10,693 --> 01:07:14,072
It's Lova. Hello?

382
01:07:14,447 --> 01:07:19,749
- Yes, well you have...
- One, two, three!

383
01:07:27,585 --> 01:07:29,679
- Monkey face!
- Shut up!

384
01:07:30,046 --> 01:07:33,641
- Exactly!
- I can't stand my own crappy jokes.

385
01:07:34,008 --> 01:07:38,980
- What? They're awesome!
- It's OK. They're always crappy.

386
01:07:39,347 --> 01:07:43,318
- Ouch!
- Cigarette burn - hello!

387
01:07:43,685 --> 01:07:48,532
<i>I saved Elin when we got here.
Or when I got here, I mean.</i>

388
01:07:48,898 --> 01:07:52,528
She was about to go home
with a construction worker.

389
01:07:52,902 --> 01:07:56,702
- His name was Tommy...
- Classy!

390
01:07:57,073 --> 01:08:01,670
What? You think I'd find a guy here
that I'd take home and fuck?

391
01:08:02,036 --> 01:08:06,587
- Would you?
- No, I wouldn't.

392
01:08:06,958 --> 01:08:13,557
- Really?
- Really? Well, maybe. I don't know.

393
01:08:17,010 --> 01:08:19,980
- I have a confession to make.
- OK...

394
01:08:20,346 --> 01:08:26,820
I convinced mom to not celebrate
Christmas once, just to piss you off.

395
01:08:28,187 --> 01:08:32,033
- She really did. And you were so mad!
- Yes.

396
01:08:32,400 --> 01:08:36,325
- OK, well... guess what I did?
- You looked as if...

397
01:08:36,696 --> 01:08:42,294
It was my fault you had to cut
your hair. I put chewing gum in it.

398
01:08:42,660 --> 01:08:45,914
What? Goddamn you!

399
01:08:46,831 --> 01:08:50,881
I knew that it was you.
Such a bitchy thing to do.

400
01:08:51,252 --> 01:08:57,976
Oh, I used to call Social Services
and ask them to come get Lova...

401
01:08:58,343 --> 01:09:02,974
- ... because we can't afford her.
- Right, we were poor.

402
01:09:03,348 --> 01:09:08,445
<i>- Yes, we had one child too much.
- That is so mean.</i>

403
01:09:08,811 --> 01:09:14,193
She believed me! She did everything
I asked, she bought stuff.

404
01:09:14,567 --> 01:09:19,118
Yes, so that I could stay.
I didn't want to go to foster care.

405
01:09:19,489 --> 01:09:24,711
And I used to call Children's Rights
on speaker phone in front of Lova.

406
01:09:25,078 --> 01:09:29,458
- And she did it often too.
- God, you're awful!

407
01:09:29,832 --> 01:09:36,306
- And I believed her every time.
- Damn it, Elin...

408
01:09:36,673 --> 01:09:40,803
- I have something to confess as well.
- It's about goddamn time.

409
01:09:41,177 --> 01:09:44,807
- You're all talk.
- No, but it's best...

410
01:09:45,181 --> 01:09:47,775
- We'll listen. Sorry. What?
- I...

411
01:09:48,142 --> 01:09:50,861
I had my first period yesterday.

412
01:09:52,563 --> 01:09:59,071
- Well, I am a bit younger than some.
- I got blood in my panties.

413
01:09:59,445 --> 01:10:02,790
- What is it?
- Well, I just...

414
01:10:03,991 --> 01:10:10,294
I used to steel 500 kronor-bills
from mom's wallet.

415
01:10:12,709 --> 01:10:17,556
- That's the most pathetic thing ever.
- We all did, even I!

416
01:10:17,922 --> 01:10:22,052
- Still?
- Yes, still.

417
01:11:09,599 --> 01:11:14,901
It's really bad.
Completely fucked up.

418
01:11:18,483 --> 01:11:21,202
I don't know what I'm doing.

419
01:11:21,569 --> 01:11:26,826
I know that it seems as if I do,
but I'm completely clueless.

420
01:11:59,816 --> 01:12:04,492
Seriously... who the hell are you?

421
01:12:10,993 --> 01:12:15,544
Sorry! You're handsome
and everything, that's not it...

422
01:12:18,584 --> 01:12:21,383
Janne!

423
01:12:23,130 --> 01:12:25,599
Janne...

424
01:12:27,385 --> 01:12:30,059
Hello?

425
01:12:31,055 --> 01:12:33,979
Come on!

426
01:12:35,393 --> 01:12:40,490
- What are you doing?
- Nothing.

427
01:13:01,335 --> 01:13:06,637
- You see where he is...
- Teamwork, that's good.

428
01:13:07,008 --> 01:13:11,855
- Oh, will I get shot now?
- Yes, because you're just walking on.

429
01:13:12,221 --> 01:13:16,146
- There.
- There, right! OK then...

430
01:13:23,065 --> 01:13:29,949
Mom abandoned us, and now I...
I've abandoned my family.

431
01:13:32,074 --> 01:13:34,793
For you.

432
01:13:48,507 --> 01:13:55,140
God, I don't know what I'm doing.
You're such a jerk.

433
01:14:05,733 --> 01:14:09,954
Elin? Time to wake up.

434
01:14:34,095 --> 01:14:36,939
What time is it?

435
01:15:06,419 --> 01:15:10,845
Oh my... This is so nice!

436
01:15:11,215 --> 01:15:15,265
And dogs! A lot of them.

437
01:15:15,636 --> 01:15:17,889
Can I take you coat?

438
01:15:18,264 --> 01:15:22,986
Morn, do you want anything?
Can I get you something?

439
01:15:24,687 --> 01:15:29,443
- She just wants to be home.
- You don't know that.

440
01:15:31,068 --> 01:15:33,821
I can sit right there.

441
01:15:38,451 --> 01:15:42,627
- You live here. You know that, right?
- Yes, I know that.

442
01:15:42,997 --> 01:15:46,092
Well, that's good...

443
01:15:48,878 --> 01:15:53,384
It usually comes back with time.

444
01:15:53,758 --> 01:15:58,514
- It seems to come and go.
- That happens.

445
01:15:58,888 --> 01:16:05,271
We can help by telling her
about things, how it's been.

446
01:16:10,066 --> 01:16:16,449
Cerebral hemorrhage and stroke.
Don't get lost in the words.

447
01:16:17,698 --> 01:16:23,421
- Can I talk? Not about her.
- Absolutely.

448
01:16:25,372 --> 01:16:28,967
I've done a great deal of thinking.

449
01:16:31,629 --> 01:16:35,224
It's as if
I've woken up from a dream-

450
01:16:35,591 --> 01:16:42,099
- or as if I've been asleep
for many years, but now I get it.

451
01:16:43,390 --> 01:16:48,112
- Right, here you go.
- Thank you.

452
01:16:52,983 --> 01:16:59,707
- I feel that this is my fault.
- It's not just your fault.

453
01:17:00,074 --> 01:17:05,296
But I let everything
just fall apart... Yes.

454
01:17:06,664 --> 01:17:12,296
I've quit drinking.
It feels damn good, it really does.

455
01:17:12,670 --> 01:17:18,302
It's like a new kind of clarity.
My thoughts are clear. I'm me.

456
01:17:20,344 --> 01:17:23,223
It's an awakening.

457
01:17:24,431 --> 01:17:30,529
I can see... how it ended up like this
between you and me.

458
01:17:31,981 --> 01:17:35,485
And that's a good thing.

459
01:17:37,778 --> 01:17:43,035
I have to go. Take care of yourself.

460
01:19:06,075 --> 01:19:09,625
Where did you go?

461
01:19:09,995 --> 01:19:14,717
I should have never left.
I should have...

462
01:19:18,963 --> 01:19:23,434
I would have liked to...
I should have been there with you.

463
01:19:23,801 --> 01:19:29,308
- Not have let you go away.
- But...

464
01:19:29,682 --> 01:19:32,401
I should have...

465
01:19:34,853 --> 01:19:40,030
You were so little;
I should have been there with you.

466
01:19:40,401 --> 01:19:45,453
I didn't see you... How foolish.

467
01:19:49,660 --> 01:19:53,255
It was all so foolish.

468
01:22:31,655 --> 01:22:37,037
- Oh, sweetie. Can't you stay?
- I have to go home to work.

469
01:22:37,411 --> 01:22:41,632
<i>- I'll come back soon.
- My little sweetheart.</i>

470
01:22:41,999 --> 01:22:48,006
- I'm not that little anymore.
- No, you're really not.

471
01:22:48,380 --> 01:22:53,352
- Has anyone seen my flat iron?
- Are you leaving as well?

472
01:22:53,719 --> 01:22:57,895
Yes, I am. I realized
I want to get back to London.

473
01:22:58,265 --> 01:23:01,439
There are a bunch of things
I want to do there.

474
01:23:02,853 --> 01:23:07,734
<i>- I'll be staying for a while.
- That makes me happy.</i>

475
01:23:08,942 --> 01:23:13,698
Oh! Let's take a photo.

476
01:23:33,592 --> 01:23:37,938
- Why isn't it photographing?
- You have to push the button.

477
01:23:41,975 --> 01:23:44,023
There it is.

478
01:23:55,155 --> 01:24:00,958
- A bit closer.
- It's blinking.

479
01:27:25,198 --> 01:27:29,203
Subtitles by Linda Jansson
TC:subtitles

