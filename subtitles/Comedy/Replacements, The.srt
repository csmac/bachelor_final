{0}{26}-How's that arm?|-Good
{43}{73}What's that smell?
{90}{114}Wild yam
{119}{172}-That's nice|-you like that?
{210}{308}And so, boys and girls,|if anybody does have any firearms...
{314}{378}...you need to turn them in|as soon as possible.
{380}{412}No questions asked
{450}{478}Understood?
{642}{666}All right
{679}{728}Okay, coach, it's all yours
{906}{1006}Last Sunday, I saw a team on the field|play as hard as they could
{1011}{1055}to win the football game
{1065}{1157}We lost Not because|of effort or desire
{1170}{1266}but because of lack of leadership|and trust One's been resolved
{1270}{1362}But leadership means nothing if|we don't believe in each other
{1362}{1449}Players spend years together|before they develop trust
{1458}{1539}I'm asking you to do it in a week,|not reasonable but
{1544}{1602}these aren't reasonable times
{1626}{1722}I know you all have concerns|about this Sunday
{1805}{1842}But a real man
{1925}{1962}admits his fears
{2017}{2072}I'm asking you|to do that here, tonight
{2134}{2168}Who wants to start?
{2178}{2202}Let's talk about fears
{2228}{2262}Fears Fears
{2402}{2466}I'm scared of spiders, coach
{2499}{2586}-That's not what I meant|-I'm afraid of spiders too
{2595}{2659}Goddamn spiders freak me, too, fellas
{2694}{2730}Well, I didn't mean that--
{2734}{2807}Ever get one of those spiders|crawling up your arm?
{2813}{2858}-Crawling on you?|-Damn!
{2864}{2946}Thanks, Jumbo you can just|rock me to sleep tonight
{2972}{3090}Okay, that's great, but I'm talking|about what scares us on the field
{3114}{3161}Like spiders on the field?
{3176}{3234}Can we get beyond the spiders, please?
{3270}{3290}Bees
{3311}{3335}Bees?
{3341}{3378}-Bees?|-Bees
{3433}{3510}Anybody here afraid of anything|other than insects?
{3528}{3547}Come on
{3579}{3600}Quicksand
{3631}{3714}Shit, yeah!|Quicksand's a scary mother, man
{3717}{3798}It sucks you right in and even|if you scream, you get that muck--
{3810}{3846}I don't think that's it
{3886}{3931}That's not what he had in mind
{3941}{4002}-What's he talking about, then?|-Ask him
{4078}{4102}What's up, Shane?
{4174}{4205}you're playing
{4221}{4324}and you think everything is going|fine But then one thing goes wrong
{4329}{4348}And another
{4386}{4416}And another
{4422}{4531}you try to fight back, but the harder|you fight, the deeper you sink
{4554}{4584}Until you can't move
{4633}{4665}you can't breathe
{4707}{4756}because you're in over your head
{4803}{4842}Like quicksand
{5010}{5086}That's some deep shit|Some deep shit
{5178}{5208}Anything else?
{5264}{5312}-Going back to the mini-mart|-The shipping yard
{5322}{5349}The auto plant
{5370}{5394}Prison
{5418}{5447}yeah, all right
{5564}{5634}The truth is, you guys have been|given something
{5635}{5682}that every athlete dreams of
{5714}{5755}A second chance
{5778}{5808}And you're afraid of blowing it
{5835}{5859}We all are
{5865}{5934}But now our fear is shared|and we can overcome it together
{5946}{6030}Let's lose that fear this Sunday|and put it into San Diego!
{6050}{6078}Come on!
{6308}{6378}Another vicious hit by|the All-Pro Hank Morris.
{6378}{6426}That's his third of the game.
{6427}{6464}It's just me, dickhead
{6474}{6545}I tell you, he is really putting|a hurt on Falco.
{6551}{6618}I'm not sure how much more|of this abuse he can take
{6653}{6690}Huddle up!
{6692}{6724}Huddle up!
{6770}{6847}Same play, except let Morris by|Nobody touch him
{6867}{6930}-It's 16-0 already|-I said let him through!
{6994}{7063}Get some penetration, Morris!|Get some penetration!
{7074}{7110}Blue, 88!
{7440}{7470}Now, that is a hit!
{7577}{7592}Holy shit!
{7602}{7675}Let's haul ass, round boy!|Follow me! Follow me!
{7794}{7842}Holy shit! Holy shit!
{7914}{7943}Go!
{7963}{7991}Jump on his back!
{8010}{8058}Here it comes! I'm coming through!
{8082}{8106}Jump on his back!
{8227}{8279}Fumiko scores! Fumiko scores!
{8285}{8346}I love to see a fat guy score!
{8346}{8376}Why?
{8382}{8478}Because you get a fat guy spike,|and then you get a fat guy dance.
{8547}{8597}Pork rice!
{8730}{8761}That's not in the playbook
{8788}{8807}yeah
{8830}{8874}Maybe we should put it in
{8955}{8994}Kickoff!
{9715}{9810}So, Annabelle, what do you, like,|think of our friends?
{9825}{9858}I think
{9883}{9918}they're friendly
{9934}{9956}I know! Aren't they?
{9978}{10006}yeah
{10290}{10338}We'll stretch them right here
{10442}{10482}Green 95!
{10539}{10562}Green
{10604}{10629}95!
{10635}{10656}What the hell?
{10851}{10903}Start the ball! Get the ball off!
{10946}{10991}Hold the play clock
{11118}{11167}Start the ball on the play clock
{11346}{11394}False start on the offense
{11400}{11490}Number 72, number 77, number 60
{11490}{11562}number 61 , number 87 and number 53
{11567}{11596}Five yards
{11602}{11647}Still first down
{11658}{11754}That's bullshit! The one girl slapped|the other girl on the ass, Jimmy
{11755}{11782}you're killing me!
{11802}{11859}you want to see mine?|I got something for you
{12044}{12092}Here we go Don't throw it!
{12306}{12354}Damn it! They did it again!
{12377}{12440}Ray Smith with the interception.
{12450}{12498}First down, Washington.
{12561}{12600}-Nice going!|-you're killing me!
{12606}{12656}-Defense, get out there!|-Let's go
{12724}{12788}That one's been shaking her ass|for two minutes!
{12834}{12858}98 Shake
{12858}{12883}98 Shake!
{13001}{13028}Green 99!
{13563}{13590}Not bad, Falco
{13673}{13722}Touchdown, Brian Murphy.
{13776}{13866}Everybody in the stadium knows|that an onside kick is coming...
{13871}{13919}... with 55 seconds left on the clock.
{13925}{14034}With just 55 seconds left,|Washington has to recover the ball. ..
{14034}{14130}...call a time-out fast, so Gruff|will have a shot at a field goal.
{14279}{14322}Big Bateman ends up|with the ball!
{14328}{14351}Danny, go down!
{14374}{14417}No one's gonna tackle him.
{14421}{14455}He 's reversing his field!
{14465}{14504}He forgot the clock's running out.
{14513}{14568}Well, he's trying to|run out the clock.
{14585}{14656}Danny, go down!|you're using up the clock!
{14714}{14754}you won't see that every day
{14756}{14790}Time out! Time out!
{14802}{14878}Nice hit, Shane! Nice hit
{14922}{14946}What do you think?
{14958}{15018}We don't stop the clock, it's over
{15168}{15238}you just hold it, and I'll kick|the bloody piss out of it
{15304}{15328}What the hell?
{15332}{15354}What the hell!
{15375}{15400}Field goal!
{15579}{15680}McGinty's gonna let Gruff try|this field goal from 65 yards out!
{15690}{15737}I don't know if he has enough leg.
{15740}{15786}I think he's smoking on the field
{15788}{15844}Smoking? I'm sure you|just imagined that.
{15858}{15927}No, I saw the smoke and everything.
{16223}{16266}Vito! Look who's on TV!
{16270}{16350}He wants to keep his pub,|he's gonna stop blowing some kicks!
{16386}{16416}I'm telling you, that's him
{17034}{17111}It's straight enough.|If it's got the distance....
{17117}{17202}It has the distance! It's good!|Washington wins!
{17261}{17370}The 65-yard field goal attempt|is good by Nigel Gruff.
{17441}{17560}Final score:|Washington 17, San Diego 16.
{17731}{17802}-you the man! you the man!|-Oh, bollocks!
{17898}{17922}Come here!
{18018}{18066}Sorry, man Sorry, excuse me
{18075}{18149}All right Good game|We snuck by on that one
{18162}{18188}-I'll take it|-All right
{18198}{18256}I'll take it Congrats
{18360}{18424}-What do you think of the new team?|-It's great!
{18425}{18458}These guys are like us.
{18473}{18546}This strike ain 't about guys like me.|It's about them superstars...
{18551}{18642}... who want $8 million instead|of 7. To hell with them.
{18642}{18738}This is the most fun I've had|in football in years. Go Falco!
{18772}{18842}All right Let's go, baby, let's go!
{18847}{18940}-I love you, Annabelle|-I know you do, Alan We're closed
{18977}{19008}Oh, hey!
{19029}{19075}-you can come in|-Come on in, buddy!
{19080}{19120}you go
{19124}{19156}Out, out, out!
{19170}{19194}you're Shane Falco
{19218}{19298}-Way to go you guys kick ass!|-Thanks
{19303}{19386}-Way to go|-Bye! Take care
{19410}{19434}Hi
{19458}{19538}Sure it's okay? I don't want to get|you in trouble with your boss
{19590}{19634}No, she won't mind
{19650}{19686}Come on in you want a beer?
{19698}{19721}Sure
{19748}{19840}So, we were so tight|We had so much fun in here
{19844}{19920}Since I was five years old,|I been sitting on those stools
{19962}{20042}And then after he died, I took over
{20082}{20110}Is that you and him?
{20161}{20202}yeah
{20229}{20298}He was the biggest Washington fan|you have ever seen
{20302}{20397}Other kids got bedtime stories,|I got football stories
{20492}{20539}He used to talk all the time
{20544}{20621}about the glory days of football
{20633}{20672}Said how they were
{20682}{20720}gone forever
{20768}{20826}I wish he was here|to see you guys play
{20874}{20922}-I don't know about that|-I do
{20926}{20953}Good game today
{20970}{20994}Thanks
{21309}{21343}It's late
{21363}{21400}yeah
{21474}{21507}Us babies need our rest
{21522}{21549}yeah, you do
{21554}{21642}Between the guys on the field|and the guys in the bar
{21644}{21692}a girl's gotta keep her guard up
{21704}{21738}yeah, I imagine
{21739}{21771}yeah
{21838}{21888}you coming to the game on Sunday?
{21893}{21952}No, we don't travel with the team
{22037}{22097}Can I see you when I get back?
{22170}{22194}Sure
{22302}{22344}Good night, Annabelle
{22386}{22410}Good night
{23513}{23591}-What do you do here, John?|-You go for it. You have to.
{23609}{23707}But Falco's been shut down|by this defense all afternoon.
{23802}{23851}But I'll tell you this,|all it takes...
{23857}{23946}...is one big play to get him|back in the ball game.
{24004}{24066}And here goes Falco.
{24307}{24380}Falco scores! Falco scores!
{24506}{24623}Here's Falco He'll try to reverse|pivot, turn and pitch out here
{24630}{24726}But in doing so, his left guard,|Andre Jackson, is going to pull
{24738}{24809}and Falco will hit him with|the ball in the back
{24820}{24870}Then it's gonna flop|around the ground
{24882}{24958}Here comes Cochran, he'll kick it|Franklin's gonna pick it up
{24964}{25024}Franklin's gonna get hit right here
{25049}{25147}The ball will go flying in the air|Falco will pick it up
{25153}{25223}and the guy who started the play,|Falco
{25229}{25294}will end up with it|in the end zone for a touchdown!
{25300}{25370}-Welcome to strike football|-And the fabulous Falco
{25386}{25418}If they can win against Dallas...
{25424}{25529}...they'll do what Washington|hasn't done in over seven years:
{25529}{25553}Get to the playoffs.
{25816}{25840}Call time out! Time out!
{25845}{25867}Time out!
{25887}{25939}Time out Washington.
{25945}{26010}Franklin, come here,|come here, come on
{26011}{26034}I had it--
{26038}{26091}Shut up Give me the Stick-Em
{26134}{26202}-That's illegal|-Think you'll go to football jail?
{26213}{26273}Now, you know this don't look natural
{26275}{26370}Don't talk We'll run the same play|you'll catch the ball
{26380}{26428}I look like I jacked off|an elephant!
{26441}{26465}Say you understand
{26489}{26512}I understand
{26516}{26592}Go out there and catch the ball|for a change, all right?
{26761}{26816}Damn! This cup is stuck!
{26832}{26882}Gonna need some time on this one
{26888}{26960}Here we go Same play|Pro Right Switch 9--
{26970}{27002}Franklin Franklin!
{27008}{27066}I can't get the damn cup off!
{27081}{27118}you can do this
{27168}{27208}It's one on one Ready?
{27423}{27459}Ready!
{27465}{27501}Blue 89!
{27506}{27546}Blue 89!
{27935}{27981}And Franklin catches the ball!
{27987}{28056}Who would've thought?|He never catches a ball.
{28060}{28100}But he caught the ball!
{28106}{28169}That makes the score 21 to 20.
{28169}{28266}Conventional wisdom says kick|the extra point, tie the game...
{28266}{28348}...and go into overtime. Especially|with the playoffs on the line.
{28353}{28447}But Jimmy McGinty is anything|but a conventional guy
{28458}{28493}And McGinty says go for it
{28505}{28535}Let's go for the win!
{28579}{28637}I need a receiver!
{28653}{28724}4-22 y Cross, okay?|We gotta have it, gotta have it!
{28867}{28937}Ready? Go! Ready!
{29034}{29142}This kind of situation has not been|Falco's strong point in the past.
{29154}{29205}Falco rolls to his left and throws...
{29238}{29303}...right into the defender's hands.
{29309}{29400}And he drops it right into the unsure|hands of Clifford Franklin!
{29406}{29441}Washington wins!
{29441}{29536}What a lucky break for Shane Falco,|who threw a bad pass...
{29541}{29598}that could've blown the game|for Washington
{29971}{30003}Get over it
{30009}{30042}It's a win
{30066}{30102}Better lucky than good?
{30114}{30140}Right
{30343}{30376}Shane Falco
{30381}{30450}-Great game out there today|-Thanks
{30452}{30549}To what do you attribute this team's|sudden rise over these past few weeks?
{30555}{30646}you know, you should talk to Franklin|He's the hero today
{30670}{30718}Clifford Franklin|Terrific day today
{30724}{30788}yeah, today was a good day|for Clifford Franklin
{30793}{30844}Clifford Franklin can't wait|till tomorrow
{30858}{30912}Clifford Franklin gets|better looking every day
{30938}{31028}We're beginning to scratch the surface|of the talents of Clifford Franklin
{31034}{31109}Clifford Franklin has moves even|Clifford Franklin ain't seen yet
{31131}{31199}you showed us a few moves|today against Phoenix
{31205}{31266}but it could be a different story|against Dallas
{31267}{31325}That's the same story,|different chapter
{31338}{31422}The football is like a one-man cold|to Clifford Franklin
{31434}{31529}Clifford Franklin's the only one|catching it and coming down with it
{31704}{31733}Cheers, babe
{31744}{31792}I think we got it
{32106}{32143}-What are you up to?|-What?
{32154}{32231}I can't get excited about|the future of my team?
{32273}{32346}Okay, okay, I got some great news
{32346}{32420}Martel and Carr have crossed|the picket line
{32441}{32516}With a deal I made them swallow,|I can't not take them back
{32526}{32560}No
{32564}{32597}No what?
{32621}{32666}I'm sticking with Falco
{32671}{32714}Jimmy, come on
{32730}{32764}Have you read the newspaper?
{32802}{32874}The entire Dallas team has crossed|the picket line
{32877}{32933}Thanksgiving night we play|the world champions
{32950}{33025}We have a deal|No interference with my coaching
{33030}{33100}As long as the strike is on,|Falco is my quarterback
{33114}{33175}We have to beat Dallas|to get into the playoffs
{33190}{33260}Falco can't do that|you saw what happened yesterday
{33266}{33308}Jesus Christ!
{33316}{33400}Do you think he'll have|that luck against Dallas too?
{33400}{33464}-They'll murder him!|-He's just getting his game back
{33474}{33528}He falls apart when the game|is on the line!
{33534}{33594}That's been his rap|ever since the Sugar Bowl
{33642}{33742}21 other men put their faith in you to|lead them They'd be heartbroken
{33749}{33834}if you abandoned them before|the biggest game of their lives
{33892}{33960}you really are a son of a bitch,|you know that?
{34171}{34199}What are you doing?
{34205}{34266}Don't want to make the same mistake|I did in Phoenix
{34270}{34326}We might end up in that situation|with Dallas
{34353}{34410}They like to send their safeties
{34442}{34470}put the pressure on
{34506}{34530}yeah, coach?
{34558}{34590}It's over
{34602}{34626}Martel crossed
{34730}{34768}I'm sorry
{35132}{35206}''Chokes with the game on the line''|ls that what O'Neil said?
{35398}{35429}It's okay
{35450}{35488}It's better for the team, right?
{35503}{35541}I mean, Martel
{35563}{35634}he is the best|The guy's got it all
{35643}{35682}No
{35706}{35744}He doesn't have heart
{35802}{35826}you do
{35946}{35994}It's been a privilege
{36045}{36071}Thanks for believing
{36384}{36427}you give them hell on Thursday
{37076}{37150}I just wanted to tell you|before you left
{37186}{37218}I'm sorry
{37230}{37276}-Thanks|-No, really
{37298}{37362}I think it's terrible|what they do to you guys
{37434}{37518}They make you believe that|you're better than you really are
{37530}{37578}Then they pull the rug|out from under you
{37638}{37723}The cruelest thing that they can give|guys like you
{37744}{37768}is hope
{37904}{37965}you're a real class act, Martel
{38010}{38058}A real class act
{38091}{38130}She deserves better
{38170}{38254}you're a sinking ship|Don't drag her down with you
{38322}{38368}Take care of my guys
{38465}{38501}Come on, boys, let's go!
{38522}{38590}Nothing but water left in here|Let's go
{38595}{38634}I have a date
{38640}{38730}-But I love you, Annabelle|-I know you do, Alan
{38730}{38779}Bye, you guys, be careful!
{40107}{40149}Here he is!
{40155}{40202}Ladies and gentlemen
{40218}{40274}number 16 in your programs
{40279}{40342}number 1 in your hearts!
{40353}{40412}Shane Falco!
{40482}{40506}Falco! Falco!
{40675}{40704}Cheers God bless you
{40710}{40744}-Cheers|-Cheers
{40962}{40986}Are you all right?
{41066}{41094}I'm done
{41106}{41130}What?
{41145}{41188}Martel crossed
{41194}{41226}Bloody hell
{41266}{41308}When will you tell the guys?
{41322}{41371}-I don't want to ruin--|-No, no, wait
{41384}{41475}Cut that music! Cut it!|Turn the music off! Cut it!
{41489}{41536}Shane's got something to tell you
{41598}{41644}I've got good news
{41667}{41733}This Thursday you'll be with|the best quarterback in the league
{41738}{41782}Damn right Damn right
{41850}{41881}Martel
{41900}{41928}He crossed
{42027}{42066}Martel crossed
{42176}{42244}While you guys are getting pounded|by Dallas
{42280}{42304}just kidding
{42337}{42402}I'll be drinking beer on my boat
{42410}{42450}Kicking back
{42522}{42550}Sentinels
{42570}{42618}raise your glasses
{42629}{42669}This is to Shane Falco
{42698}{42726}He's our quarterback
{42742}{42786}he's our leader
{42810}{42880}but most of all, he's our friend|This is for you, man
{42891}{42914}To Falco!
{43009}{43048}Thanks
{43054}{43086}Thanks
{43124}{43157}It's been fun
{45184}{45256}Welcome back to Nextel Stadium|in our nation's capital.
{45262}{45339}I'm Pat Summerall and with me|as always is John Madden
{45352}{45426}It appears that the strike|is coming to a close
{45429}{45500}Martel will be resuming|the quarterback position tonight.
{45506}{45603}I gotta admit, I was looking forward|to seeing what Falco could do...
{45609}{45714}... with one more game. He made|amazing progress the past few weeks...
{45714}{45765}...under the tutelage|of Jimmy McGinty.
{45770}{45793}He really did.
{45799}{45906}But tonight it'll be Martel leading|Washington against Dallas.
{46123}{46181}I never thought I'd say this, but. ..
{46194}{46251}...l'm gonna kind of miss|those replacement games.
{46257}{46305}Bring back Falco!
{46314}{46410}Ladies and gentlemen, leading|your Washington Sentinels today...
{46416}{46494}... welcome back number 7,|Eddie Martel.
{46602}{46626}Go on, boys!
{47191}{47258}Come on, Martel|Get moving, get moving!
{47288}{47327}Maybe you should try scrambling
{47352}{47399}Brilliant|How about a quick kick?
{47838}{47874}Hey, Butler
{47946}{47994}-Red 38!|-Hey, man
{47994}{48075}If it ain't any trouble,|I'd love to get your autograph after
{48081}{48128}you want it, you got it, scab!
{48138}{48164}All right
{48620}{48694}Cochran fumbles the ball,|Dallas recovers.
{48700}{48766}First down and goal|on the two-yard line.
{48842}{48869}Hawk Nine Stay!
{48882}{48954}Bateman! Hawk Nine Stay!
{48958}{49014}Hawk Nine Stay!
{49041}{49075}I'm back!
{49206}{49248}Touchdown, Dallas.
{49338}{49401}They're quitting on you!|They're quitting on you!
{49601}{49625}Gold 41 !
{49938}{50010}Pass to Brian Murphy, incomplete.
{50023}{50062}Throw right, you're going left!
{50067}{50112}Right, right, right.
{50118}{50178}You went left,|I want you to go right!
{50202}{50228}No, right!
{50234}{50262}Right, you idiot!
{50309}{50354}Hey, easy!
{50848}{50923}Brian Murphy fumbled the ball.|Dallas recovered.
{51016}{51039}Nice hands
{51039}{51090}3-2 Magic Wreck, 0
{51096}{51143}What the hell was that about?
{51496}{51543}Touchdown, Dallas.
{52456}{52503}Gruff, wake up!
{52750}{52773}What the--?
{52778}{52839}Pass incomplete by Eddie Martel.
{52970}{53019}What the hell was that?
{53032}{53062}I'm talking to you!
{53066}{53104}I call the plays on the field
{53110}{53164}-That's not how I coach|-I don't give a shit
{53176}{53223}because that's the way I play
{53236}{53294}That's the end of the first half. ..
{53297}{53355}... with a score of Dallas 17,|Washington 0.
{53367}{53381}Coach McGinty!
{53391}{53446}What will Washington need to get back|into this game?
{53490}{53559}-Heart|-I'm sorry?
{53567}{53595}you gotta have heart
{53608}{53638}Can you elaborate?
{53708}{53738}Miles and miles of heart
{53781}{53862}There you have it,|in a word, from Coach McGinty...
{53872}{53946}Washington will need ''heart''|to get back into this--
{54042}{54111}I ran it like you said!|you just under threw me!
{54111}{54190}This isn't a track meet, asshole|you have to look for the ball!
{54212}{54261}I'll pull you off the field,|spoiled punk
{54304}{54409}Is O'Neil gonna side with a burnout|coach or someone who delivers the fans?
{54424}{54447}Son of a bitch!
{54474}{54547}What are you thinking about?|We got a game to play!
{54553}{54604}-Nobody can win with these losers!|-I can
{54712}{54773}Falco, great to see you Now, get|the hell out of my locker room!
{54792}{54836}-Coach?|-What took you so long?
{54855}{54878}Traffic
{54904}{54927}Suit up!
{54987}{55052}-O'Neil will fire your ass!|-Won't be the first time!
{55058}{55120}This is bullshit!|I'll end this right now
{55120}{55154}Come and get some!
{55253}{55295}you big fatty!
{55489}{55526}This doesn't change anything
{55529}{55585}I'm an All-Pro quarterback!|I got two Super Bowl rings!
{55599}{55647}you're no more than a replacement
{55653}{55681}yeah
{55720}{55758}I can live with that
{55816}{55892}My brothers, will somebody please get|this asshole out of here?
{55936}{55983}Get your hands off me, you gorilla!
{56230}{56318}Jimmy McGinty is anything|but a conventional guy
{56325}{56416}And McGinty says, with the playoffs|on the line, go for it.
{56416}{56472}Get to the playoffs.|Something Washington...
{56478}{56559}...has not been able to do|against Dallas in seven years.
{56906}{56934}Falco?
{56976}{57021}What the hell is he up to?
{57039}{57092}Look, here comes Falco!|Falco's back!
{57101}{57134}What happened to Martel?
{57139}{57190}I don't know,|but the way Falco's running...
{57195}{57282}...and that look he has in his eye,|he thinks he's gonna play.
{57391}{57414}I'm sorry
{57462}{57526}They're playing zone|and you can pick them apart
{57531}{57571}if you keep your eyes open--
{57577}{57687}He seems to be necking with that|cheerleader! That's what he's doing!
{57712}{57785}Players aren't supposed to be|fraternizing with cheerleaders.
{57792}{57855}yeah, but what are they gonna do?|Fire him?
{57869}{57927}-you give me strength|-you're late for work
{57986}{58014}Kick ass, Falco!
{58096}{58167}What's all the celebrating about?|We're down 17 to nothing
{58175}{58216}It's nasty out there
{58216}{58271}That's why girls|don't play the game, coach
{58277}{58383}Listen up This time tomorrow,|the strike will be officially over
{58396}{58480}Dallas made a big mistake|They haven't been afraid of you
{58480}{58555}They should be, because you have|a powerful weapon working for you
{58560}{58609}There is no tomorrow for you
{58623}{58677}And that makes you all|very dangerous people!
{58854}{58887}Kick ass on one Ready?
{59091}{59164}Clifford Franklin is|looking for a new ho
{59188}{59226}Ready for some pain?
{59232}{59281}-Bring it on|-It's coming
{59295}{59325}It's coming
{59330}{59372}See him coming in motion?
{59473}{59515}I've never seen anything like this!
{59521}{59548}Unbelievable!
{59584}{59668}There's at least five, six flags|out there. Hats, everything!
{59680}{59732}This is what you call|an old-fashioned melee
{59738}{59812}Unnecessary roughness,|number 16 and number 34
{59824}{59882}Fifteen yards, so that's, wait
{59896}{59943}4530
{60017}{60063}How many yards so far?
{60063}{60115}Who do they think|they're playing here?
{60280}{60312}Come on, let's go! Come on!
{60934}{60955}My knee!
{60978}{61001}It's my knee!
{61099}{61143}I think I broke it
{61250}{61288}Did I do it? Did I score?
{61288}{61350}-yeah, you did it|-God, it hurts It hurts!
{61360}{61384}I'm sorry
{61384}{61462}Don't be sorry Going out in front|of 80,000 people ain't bad
{61503}{61556}you finish what you started
{61696}{61730}White 24!
{62128}{62176}Blue 15! Hut, hut!
{62872}{62919}First down, Washington.
{62979}{63029}-Good job|-I got skills
{63039}{63110}-Feel like running the ball?|-Better give it to Wilkinson
{63136}{63159}He's going to jail
{63184}{63255}-I'll clear the way|-I'm right behind you
{63280}{63314}We need it, we need it!
{63616}{63711}A gain of 20 yards by Ray Smith.|First down.
{63868}{63929}I know you're tired,|I know you're hurting
{63934}{64003}I wish I could say something|classy and inspirational
{64010}{64047}But it wouldn't be our style
{64108}{64143}Pain heals
{64149}{64191}Chicks dig scars
{64200}{64263}Glory lasts forever
{64265}{64293}Right on, Shane
{64298}{64359}Right on, baby Right on!
{64363}{64410}Shotgun DC Right
{64415}{64487}Flip 90 Dig, on the center Ready?
{64873}{64959}We 're looking at a different team|here in the second half.
{64962}{65058}Absolutely. Washington is playing|like there's no tomorrow...
{65065}{65103}because there isn't
{65128}{65165}Touchdown, baby!
{65176}{65199}Son of a bitch!
{65249}{65301}Odd bullets, blitz coverage!
{65539}{65622}Daniel Bateman comes up with|a big stop on third and one.
{65632}{65657}That forces a Dallas punt.
{65663}{65781}Pat, with 28 seconds remaining,|Washington needs a decent return...
{65790}{65871}...in order to give Gruff|a shot to tie the game.
{66425}{66471}With 12 seconds remaining...
{66475}{66534}-It's yours, Nigel|-...a 32-yard field goal. ..
{66544}{66592}... will send this game into overtime.
{66592}{66654}32 yards is just|a chip shot for Gruff
{67048}{67071}you all right?
{67109}{67134}I'm sorry
{67144}{67215}I had the money but I pissed it|away at the track again
{67217}{67272}What? What are you talking about?
{67278}{67319}They'll take my pub away from me
{67325}{67365}It's all I got
{67384}{67421}Come on, Shane!
{67733}{67806}It's a fake! Falco still has it!|He 's running with it!
{68032}{68079}Touchdown, Falco!
{68402}{68474}Holding! Number 68 on the offense
{68488}{68545}10-yard penalty Repeat first down
{68550}{68591}What a terrible call!
{68597}{68632}Time out!
{68713}{68776}-you okay?|-I broke my arm
{68790}{68830}Cheers you saved my ass
{68848}{68871}Take care
{68878}{68943}you'll have to explain to me|what that was about
{68992}{69015}What's it gonna be?
{69040}{69072}I want the ball
{69098}{69135}Winners always do
{69160}{69194}Spread formation
{69379}{69409}Damn!
{69576}{69640}Sorry, Shane I'm sorry, everybody
{69640}{69711}No problem Just rip|someone's head off on this one
{69713}{69738}Consider it done
{69744}{69838}All right So besides me,|who really wants the ball?
{69880}{69911}yeah, you want it, Brian
{69918}{70008}Let's hook up|DC left, wide motion, 88 War
{70014}{70047}Gentlemen
{70144}{70222}It's been an honor to share|the field of battle with you
{70879}{70913}We ain't losing this game
{71425}{71447}Green 86!
{71570}{71615}Green 86!
{72475}{72543}Touchdown, Brian Murphy!
{72568}{72664}I knew it! I knew it all along!
{72664}{72746}Jimmy, you beautiful son of a bitch!|I knew you could do it!
{72760}{72855}Final score:|Washington 20, Dallas 17.
{72858}{72937}Washington goes to the playoffs!
{73168}{73191}you did it!
{73230}{73252}you did it!
{74863}{74967}When the replacement players|for the Sentinels left the stadium...
{74973}{75020}...there was no ticker-tape parade...
{75026}{75087}...no endorsement deals|for sneakers...
{75090}{75159}...or soda pop or breakfast cereal.
{75163}{75221}Just a locker to be cleaned out. ..
{75232}{75279}...and a ride home to catch.
{75282}{75328}But what they didn't know...
{75334}{75399}... was that their lives|would be changed forever...
{75405}{75473}...because they had been|part of something great.
{75484}{75555}And greatness, no matter how brief. ..
{75568}{75615}...stays with a man.
{75620}{75689}Every athlete dreams|of a second chance.
{75749}{75795}These men lived it.
