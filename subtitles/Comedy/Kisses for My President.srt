﻿1
00:00:00,790 --> 00:00:02,105
[LEE CHO HEE]

2
00:00:02,621 --> 00:00:03,826
[CHOI JI WOO]

3
00:00:04,472 --> 00:00:05,681
[LEE JOON GI]

4
00:00:06,632 --> 00:00:07,742
[PARK HAE JIN]

5
00:00:08,377 --> 00:00:09,617
[JI CHANG WOOK]

6
00:00:10,271 --> 00:00:11,386
[KAI]

7
00:00:12,212 --> 00:00:13,382
[TAEC YEON]

8
00:00:13,962 --> 00:00:15,136
[LEE JONG SUK]

9
00:00:15,862 --> 00:00:16,977
[LEE MIN HO]

10
00:00:16,977 --> 00:00:19,560
[7 First Kisses]

11
00:00:19,917 --> 00:00:21,752
[-Episode 4 -]
[Ji Chang Wook: Till the End of the World]

12
00:00:23,560 --> 00:00:29,235
We want to thank everyone for
shopping at Lotte Duty Free Shop.

13
00:00:30,260 --> 00:00:34,945
The hours of operation for Lotte Duty Free shop
is from 9:30 AM

14
00:00:34,945 --> 00:00:36,460
to 9:00 PM.

15
00:00:36,460 --> 00:00:37,560
[I'll come back for this.]
[Please keep it safe for now.]

16
00:00:37,825 --> 00:00:39,380
"I'll come back for this?"

17
00:01:20,709 --> 00:01:21,628
Oh my goodness!

18
00:01:21,983 --> 00:01:24,238
Ji Chang Wook?
He's Ji Chang Wook, right?

19
00:01:24,715 --> 00:01:28,430
- Where is it?
- What is going on this time?

20
00:01:28,430 --> 00:01:31,490
Ah! You mean tihs?

21
00:01:38,270 --> 00:01:40,305
I didn't want to get you involved again.

22
00:01:42,440 --> 00:01:43,590
I'm sorry.

23
00:01:47,896 --> 00:01:48,850
Oh my goodness!

24
00:01:53,890 --> 00:01:56,765
We knew there was a traitor among us.

25
00:01:57,060 --> 00:01:58,725
So, it was you.

26
00:01:59,345 --> 00:02:02,770
The secret agent that
sneaked into our organization.

27
00:02:02,770 --> 00:02:04,285
A secret agent?

28
00:02:06,780 --> 00:02:09,035
Seeing that you're having
a rendezvous with a retired agent,

29
00:02:09,035 --> 00:02:11,705
you must be pretty desperate.

30
00:02:12,125 --> 00:02:14,115
Who? Me?

31
00:02:14,760 --> 00:02:16,595
Am I a secret agent too?

32
00:02:17,015 --> 00:02:18,125
It's all over now.

33
00:02:18,970 --> 00:02:22,340
Hand over the USB with the blueprint!

34
00:02:35,297 --> 00:02:36,199
Freeze!

35
00:02:36,199 --> 00:02:37,613
No! No!

36
00:02:41,304 --> 00:02:43,654
Why...did you shoot me?

37
00:03:03,335 --> 00:03:05,770
Hey...I got shot.

38
00:03:14,580 --> 00:03:15,925
You still got skills.

39
00:03:17,340 --> 00:03:18,500
But sorry.

40
00:03:18,870 --> 00:03:22,035
I exposed your identity.
Let's go to a safe house now.

41
00:03:56,100 --> 00:03:57,030
I'm sorry.

42
00:03:58,890 --> 00:04:00,180
For putting you through this again.

43
00:04:04,505 --> 00:04:05,481
I'll go now.

44
00:04:11,421 --> 00:04:13,251
Oh no!

45
00:04:13,433 --> 00:04:14,948
Are you okay?

46
00:04:14,948 --> 00:04:15,902
I'm okay.

47
00:04:15,902 --> 00:04:17,825
You're not okay!

48
00:04:18,125 --> 00:04:20,425
Where? Did you get shot somewhere?

49
00:05:26,655 --> 00:05:28,355
I think I fell into their trap.

50
00:05:31,070 --> 00:05:33,005
It's been awhile since we last met...
I'm sorry.

51
00:05:36,990 --> 00:05:39,650
I'll get going now.

52
00:05:52,550 --> 00:05:53,990
- How's your current job working out?
- Why are you doing such a dangerous job?

53
00:05:57,265 --> 00:05:59,335
My job now is fine.

54
00:06:00,480 --> 00:06:03,830
But why are you doing something so dangerous?

55
00:06:09,130 --> 00:06:10,555
Those were my exact words to you

56
00:06:11,875 --> 00:06:14,165
when I was your team leader
at headquarters.

57
00:06:16,533 --> 00:06:17,495
I guess...

58
00:06:19,975 --> 00:06:20,925
I feel responsible?

59
00:06:22,885 --> 00:06:25,175
Because of this sense of responsibility
to protect someone,

60
00:06:27,675 --> 00:06:29,250
I said

61
00:06:29,895 --> 00:06:33,500
Ah...because you feel responsible.

62
00:06:35,705 --> 00:06:40,071
Was I a brave person?

63
00:06:40,960 --> 00:06:41,835
Brave?

64
00:06:43,210 --> 00:06:44,410
You were reckless.

65
00:06:45,965 --> 00:06:48,305
To the point I regretted hiring you.

66
00:06:49,940 --> 00:06:53,415
I had no idea I could be such a person...

67
00:06:53,415 --> 00:06:54,450
If I hadn't...

68
00:06:56,845 --> 00:06:57,820
If...

69
00:07:01,055 --> 00:07:02,575
If I hadn't fallen in love with you,

70
00:07:05,380 --> 00:07:07,440
we might still be working together.

71
00:07:19,540 --> 00:07:21,645
That's why I had to let you go.

72
00:07:22,680 --> 00:07:25,410
I'm sorry for getting you involved again.

73
00:07:28,530 --> 00:07:30,910
I didn't know who I could trust at headquarters,

74
00:07:32,920 --> 00:07:34,680
so I came looking for you.

75
00:07:47,261 --> 00:07:51,570
I won't show up in your life again.

76
00:08:03,040 --> 00:08:04,895
Please don't go...

77
00:08:18,730 --> 00:08:20,160
I mean...

78
00:08:21,160 --> 00:08:22,980
You're still bleeding.

79
00:08:23,300 --> 00:08:25,380
At least wait until
the bleeding stops.

80
00:08:45,840 --> 00:08:48,250
I think it was stupid of me to leave you.

81
00:08:48,970 --> 00:08:50,890
Even if I have to risk my life ,

82
00:08:52,460 --> 00:08:55,110
I'll protect you by your side.

83
00:08:56,520 --> 00:08:59,180
The person who'll risk his life to love me...

84
00:08:59,925 --> 00:09:01,110
is this person...

85
00:09:01,816 --> 00:09:03,000
Let's do it.

86
00:09:05,020 --> 00:09:06,175
Till the end of the world.

87
00:09:57,957 --> 00:09:59,921
Do I just keep missing out

88
00:10:00,370 --> 00:10:02,815
on kisses and men like this?

89
00:10:03,060 --> 00:10:05,470
What kind of reward is this?

90
00:10:07,396 --> 00:10:08,530
Min Soo Jin!

91
00:10:08,851 --> 00:10:09,841
Come you come here?

92
00:10:15,260 --> 00:10:18,015
These people are in a hurry to shop.

93
00:10:18,460 --> 00:10:21,100
They're VIP customers,

94
00:10:21,295 --> 00:10:23,155
- so I need you to interpret.
- Yes.

95
00:10:23,555 --> 00:10:25,790
- Interpret?
- Yes, interpret.

96
00:10:26,385 --> 00:10:27,865
Hello!

97
00:10:27,865 --> 00:10:29,780
Please help us.

98
00:10:34,925 --> 00:10:36,125
She's getting a call

99
00:10:46,750 --> 00:10:48,550
Why isn't she picking up her phone?

100
00:10:55,015 --> 00:10:56,975
[KAI]

101
00:10:57,566 --> 00:11:00,916
[7 First Kisses]

102
00:11:03,010 --> 00:11:04,640
Do you know how many times I called?

103
00:11:04,640 --> 00:11:06,325
It's so hard to go on a date with you.

104
00:11:06,730 --> 00:11:07,926
Date...

105
00:11:08,800 --> 00:11:11,945
Are you really going to say
you didn't know I liked you all this time?

106
00:11:12,510 --> 00:11:15,245
No! I'm a teacher, and you are a student!

107
00:11:15,995 --> 00:11:17,785
I don't want to call you teacher now.

108
00:11:17,785 --> 00:11:20,075
Then, what are you going to call me?

109
00:11:21,290 --> 00:11:22,040
You.