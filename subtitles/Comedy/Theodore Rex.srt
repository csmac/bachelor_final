﻿1
00:00:03,569 --> 00:00:05,285
(CLUB MUSIC PLAYING)

2
00:00:08,190 --> 00:00:11,460
ALL: Go! Go! Go! Go!

3
00:00:11,544 --> 00:00:13,137
Look out! You'll get soap on the floor!

4
00:00:13,793 --> 00:00:15,959
Oh, Rex, you burst my bubble!

5
00:00:16,043 --> 00:00:17,432
- Huh?
- (ALL EXCLAIM)

6
00:00:17,514 --> 00:00:19,722
- Eh, what a buzzkill.
- Sorry, I didn't...

7
00:00:19,805 --> 00:00:21,603
Huh! You're what they call
a party pooper!

8
00:00:21,686 --> 00:00:22,912
(SING-SONG) Party Pooper Rex!

9
00:00:23,036 --> 00:00:25,857
ALL: (SING-SONG)
Party Pooper Rex! Party Pooper Rex!

10
00:00:26,512 --> 00:00:27,697
BONNIE'S MOM: Bonnie! Bath time!

11
00:00:28,230 --> 00:00:29,414
BONNIE: (GASPS) Can I bring a toy?

12
00:00:29,538 --> 00:00:30,887
- MR. POTATO HEAD: I ain't no bath toy.
- (EXCLAIMS)

13
00:00:31,011 --> 00:00:35,590
BONNIE: Bath time!
Rex will be perfect for the search party.

14
00:00:38,904 --> 00:00:43,811
Cap'n's log: Received a distress
signal, but no sign of... (GASPS)

15
00:00:44,752 --> 00:00:46,469
Survivors! Son, squeak to me!

16
00:00:46,552 --> 00:00:48,350
Save yourself!

17
00:00:48,433 --> 00:00:49,577
What?

18
00:00:50,642 --> 00:00:51,867
(ROARING)

19
00:00:52,850 --> 00:00:53,953
No!

20
00:00:55,549 --> 00:00:57,511
(BOTH SCREAMING)

21
00:00:59,393 --> 00:01:01,192
(SCREAMS) Fire torpedoes!

22
00:01:01,275 --> 00:01:04,464
(IMITATES GUN FIRING)
No! I just want to have tea with you!

23
00:01:05,691 --> 00:01:07,041
Okay, trouble.
There's way too much water in here.

24
00:01:08,268 --> 00:01:10,272
- BONNIE: Aw!
- (WATER GURGLING)

25
00:01:10,436 --> 00:01:11,743
BONNIE'S MOM:
You want to flood the house?

26
00:01:11,827 --> 00:01:12,807
BONNIE: Yeah!

27
00:01:12,890 --> 00:01:15,179
BONNIE'S MOM: Well, how about
dinner at Grandma's instead?

28
00:01:15,507 --> 00:01:16,733
- Grandma's! (LAUGHING)
- (CHUCKLES) Sure.

29
00:01:17,838 --> 00:01:19,023
CAP'N: Welcome aboard!

30
00:01:19,106 --> 00:01:22,704
Yeah, baby! You were way
scarier than the last sea monster!

31
00:01:22,787 --> 00:01:23,891
No offense, Cuddles.

32
00:01:23,973 --> 00:01:25,158
No problem.

33
00:01:25,241 --> 00:01:27,039
Ha! What do they call you, sailor?

34
00:01:28,349 --> 00:01:30,639
Well, my friends call me...

35
00:01:30,802 --> 00:01:32,029
(SING-SONG) Party Pooper Rex!

36
00:01:32,766 --> 00:01:33,870
Um...

37
00:01:34,197 --> 00:01:36,446
Partysaurus Rex!

38
00:01:36,610 --> 00:01:37,632
- (TOYS EXCLAIMING)
- Avast thar!

39
00:01:37,714 --> 00:01:39,390
You're in the right place, baby!

40
00:01:39,474 --> 00:01:42,621
Because when the water's high,
the party's fly.

41
00:01:42,705 --> 00:01:43,685
Aye, aye!

42
00:01:43,767 --> 00:01:44,748
(ALL LAUGHING)

43
00:01:47,652 --> 00:01:48,838
(ALL GROAN)

44
00:01:49,370 --> 00:01:51,332
Oh! My lobster!

45
00:01:51,415 --> 00:01:53,091
Till tomorrow, my love.

46
00:01:53,255 --> 00:01:55,340
And when bath time's done?

47
00:01:56,160 --> 00:01:57,753
There's no more fun.

48
00:01:58,204 --> 00:01:59,839
We need water to move.

49
00:01:59,922 --> 00:02:02,088
Bath time's only 15 minutes a day!

50
00:02:02,172 --> 00:02:05,238
We could party all the time
if we could turn the water on ourselves.

51
00:02:05,320 --> 00:02:08,673
Aye, Drips. But no one's got arms
here, except for Barbara.

52
00:02:08,756 --> 00:02:09,737
How we doing, Babs?

53
00:02:10,555 --> 00:02:13,827
Weeks, months.
Scrubbing's been light, Cap'n.

54
00:02:14,604 --> 00:02:17,753
Oh, man! I wish
we could get the party started up in here.

55
00:02:18,694 --> 00:02:19,675
No more tears!

56
00:02:21,189 --> 00:02:23,601
Well, I've got arms.

57
00:02:23,766 --> 00:02:27,158
I could get this party started up in here.

58
00:02:27,242 --> 00:02:28,222
Without Bonnie?

59
00:02:28,305 --> 00:02:29,613
CAP'N: Topside?

60
00:02:29,695 --> 00:02:31,494
Huh? (MUMBLES)

61
00:02:31,904 --> 00:02:32,885
CAP'N: Shiver me timbers!

62
00:02:32,967 --> 00:02:33,948
(PARTY MUSIC PLAYING)

63
00:02:34,316 --> 00:02:37,384
No way! Yeah, Rex! That's it, baby!

64
00:02:37,547 --> 00:02:39,509
Let's pump this party!

65
00:02:39,634 --> 00:02:40,615
Partysaurus, you rock!

66
00:02:43,928 --> 00:02:44,867
(LAUGHING)

67
00:02:44,951 --> 00:02:46,258
CAP'N: Aye, aye, matey!

68
00:02:46,341 --> 00:02:47,689
Check me out! Oh, yeah!

69
00:02:47,772 --> 00:02:49,366
A thank ye to Rex!

70
00:02:50,635 --> 00:02:51,615
Wow!

71
00:02:52,066 --> 00:02:53,088
(GARGLES)

72
00:02:53,743 --> 00:02:55,133
- Thanks!
- Rex, baby!

73
00:02:55,215 --> 00:02:56,359
How about a little more bubbly?

74
00:02:56,443 --> 00:02:57,463
More? No, no, no, no!

75
00:02:57,547 --> 00:02:59,877
Party Pooper!

76
00:02:59,960 --> 00:03:04,048
I mean, why have a little
when you could have a lot?

77
00:03:04,704 --> 00:03:06,666
We really got a party up in here!

78
00:03:07,157 --> 00:03:08,465
- Partysaurus.
- (GIGGLING)

79
00:03:08,998 --> 00:03:10,183
Can you get some of us in?

80
00:03:10,266 --> 00:03:11,533
Are you crazy? There's...

81
00:03:11,697 --> 00:03:13,414
MR. POTATO HEAD: Pooper!

82
00:03:13,497 --> 00:03:17,544
Some of you? (CHUCKLES)
Why not all of you!

83
00:03:17,628 --> 00:03:19,099
(ALL LAUGHING)

84
00:03:20,326 --> 00:03:21,879
(CLUB MUSIC PLAYING)

85
00:03:25,153 --> 00:03:26,337
(BOTH SQUEAKING)

86
00:03:32,841 --> 00:03:33,903
Yeah, baby!

87
00:03:33,986 --> 00:03:35,253
All hands on deck!

88
00:03:39,467 --> 00:03:40,447
(LAUGHS)

89
00:03:42,329 --> 00:03:43,433
My lobster!

90
00:03:43,638 --> 00:03:44,660
(GURGLING)

91
00:03:44,742 --> 00:03:45,845
What's that racket?

92
00:03:48,178 --> 00:03:49,772
- What?
- The overflow drain?

93
00:03:50,386 --> 00:03:52,307
- Huh? No problem!
- Hey, whoa.

94
00:03:54,108 --> 00:03:57,379
Whoa, this dude really knows
how to party. Seriously... (GURGLING)

95
00:03:57,461 --> 00:03:59,301
He blocked the drain.

96
00:03:59,384 --> 00:04:01,141
We will overflow!

97
00:04:01,960 --> 00:04:03,187
BOTH: Awesome!

98
00:04:03,269 --> 00:04:04,577
Thanks, Partysaurus!

99
00:04:05,151 --> 00:04:09,198
Partysaurus doesn't worry about too
much soap and the tub overflow...

100
00:04:10,181 --> 00:04:13,083
Overflow? We'll flood the house!

101
00:04:13,657 --> 00:04:15,824
We're crossing the scum line!

102
00:04:16,765 --> 00:04:17,746
(EXCLAIMS)

103
00:04:18,728 --> 00:04:21,017
Too much water, everybody. Ahh!

104
00:04:22,246 --> 00:04:24,330
Aw, the floaters have all the fun!

105
00:04:24,413 --> 00:04:26,335
What up, fishes? What what!

106
00:04:26,498 --> 00:04:28,052
- He turned it up!
- Avast!

107
00:04:28,461 --> 00:04:30,178
No! Too much water!

108
00:04:30,588 --> 00:04:31,978
(GASPS) Help!

109
00:04:32,061 --> 00:04:33,532
Sir, this party is out of control.

110
00:04:33,697 --> 00:04:34,636
I know! There's too much...

111
00:04:34,719 --> 00:04:35,740
Out of control!

112
00:04:35,824 --> 00:04:36,804
(WHIMPERS)

113
00:04:39,341 --> 00:04:40,321
(GASPS)

114
00:04:41,794 --> 00:04:43,225
- Aw, man!
- (TOYS GROANING)

115
00:04:43,348 --> 00:04:45,802
Aww! You got to be kidding me.

116
00:04:45,966 --> 00:04:48,215
(SIGHS) That was close.

117
00:04:48,297 --> 00:04:49,728
(WHOOSHING)

118
00:04:49,810 --> 00:04:50,996
(GASPS)

119
00:04:54,064 --> 00:04:55,862
It's a perfect storm!

120
00:04:55,945 --> 00:04:57,212
ALL: Aye, aye!

121
00:04:59,708 --> 00:05:02,242
What are you doing?
My tail! No, not the...

122
00:05:03,062 --> 00:05:04,124
Oh!

123
00:05:05,679 --> 00:05:08,867
Overflow! Overflow!

124
00:05:08,951 --> 00:05:11,772
All hail the Partysaurus Rex!

125
00:05:11,854 --> 00:05:14,512
We're going to overflow!

126
00:05:15,289 --> 00:05:16,925
We're going over the top, baby!

127
00:05:18,357 --> 00:05:19,542
Secure the rigging!

128
00:05:22,160 --> 00:05:23,877
Man overboard!

129
00:05:23,960 --> 00:05:25,636
Yeah, it's been a while.

130
00:05:25,718 --> 00:05:28,089
Oh, Rex? Are you okay in...

131
00:05:35,247 --> 00:05:38,355
You guys missed it. I was a partysaurus!

132
00:05:38,437 --> 00:05:41,258
Party? You? (LAUGHS)
I'll believe it when I see it.

133
00:05:41,791 --> 00:05:42,771
Psst! I-yah, Partysaurus!

134
00:05:43,672 --> 00:05:47,639
We hear what ya done for the bath toy.
Can you hook us up, mon?

135
00:05:47,762 --> 00:05:48,988
ALL: Rex! Rex! Rex!

136
00:05:52,834 --> 00:05:53,855
Duty calls!

137
00:05:54,183 --> 00:05:55,164
(YELLS)

138
00:05:55,860 --> 00:05:57,291
Yeah, mon!

139
00:05:57,905 --> 00:06:00,930
(LAUGHS)

