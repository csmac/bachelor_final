1
00:00:35,880 --> 00:00:39,880
The polar regions are more hostile to
life than any other part of the Earth.

2
00:00:45,480 --> 00:00:49,920
Human beings have little natural
protection against the cold,

3
00:00:52,120 --> 00:00:54,919
so why, for thousands of years,

4
00:00:54,920 --> 00:00:57,720
have we endured the hardships that
come from living here?

5
00:00:59,720 --> 00:01:02,239
And what keeps us coming back today,

6
00:01:02,240 --> 00:01:04,840
to the farthest extremes
of our planet?

7
00:01:22,440 --> 00:01:26,640
Winter in the most northerly town
on Earth, Longyearbyen.

8
00:01:28,600 --> 00:01:32,320
Here, for three months of the year,
the sun never rises.

9
00:01:35,120 --> 00:01:39,159
Only the full moon, which never
sets as far north as this,

10
00:01:39,160 --> 00:01:41,560
sheds any light into the darkness.

11
00:01:55,400 --> 00:01:57,879
This town was built here in Svalbard,

12
00:01:57,880 --> 00:02:02,200
only 700 miles from the North Pole,
to support a mine.

13
00:02:03,920 --> 00:02:07,160
The Arctic is rich in coal,
oil and minerals.

14
00:02:14,200 --> 00:02:16,580
In Siberia, the Russian
Arctic, the mineral

15
00:02:16,581 --> 00:02:18,960
wealth has given rise
to large cities.

16
00:02:25,120 --> 00:02:28,760
This is Norilsk,
the coldest city on Earth.

17
00:02:32,280 --> 00:02:37,679
Temperatures regularly drop
to below minus 50 degrees Centigrade.

18
00:02:37,680 --> 00:02:40,340
Fuel freezes in the tanks
of the trucks, and

19
00:02:40,341 --> 00:02:43,000
has to be melted in a
rather alarming way.

20
00:02:49,560 --> 00:02:53,120
Ships are frozen into the rivers
for nine months of the year.

21
00:02:54,920 --> 00:02:57,559
The ice must be cut
away from their hulls

22
00:02:57,560 --> 00:03:02,160
because steel becomes brittle
and vulnerable to the thickening ice.

23
00:03:05,000 --> 00:03:08,180
Like all Arctic cities,
Norilsk depends on a

24
00:03:08,181 --> 00:03:11,360
power plant which heats
everybody's home.

25
00:03:12,400 --> 00:03:17,599
Waste heat from the plant even keeps
a lake ice-free all winter.

26
00:03:17,600 --> 00:03:20,740
Even at air temperatures
of minus 50, the

27
00:03:20,741 --> 00:03:23,880
Norilsk Walrus Club
come here every day.

28
00:03:39,120 --> 00:03:43,839
There is evidence that a dip in cold
water is good for the immune system,

29
00:03:43,840 --> 00:03:46,719
but when the water is
a degree above freezing,

30
00:03:46,720 --> 00:03:50,520
it's hard to imagine that the
benefits could outweigh the pain.

31
00:04:04,560 --> 00:04:07,700
Today's Arctic city
dwellers can lead an

32
00:04:07,701 --> 00:04:10,840
almost normal existence
thanks to technology.

33
00:04:12,000 --> 00:04:17,360
But towns and cities are very recent
arrivals in the polar landscape.

34
00:04:19,760 --> 00:04:23,679
The polar regions are the least
populated part of our planet.

35
00:04:23,680 --> 00:04:28,040
Most of the Arctic remains
empty of human beings.

36
00:04:32,200 --> 00:04:36,600
In the north, the Pole itself is
covered by a freezing ocean.

37
00:04:39,360 --> 00:04:43,800
Around it lie vast lands, of which
Siberia is the largest and coldest.

38
00:04:59,240 --> 00:05:03,159
Human beings first ventured onto
the great plains of Siberia,

39
00:05:03,160 --> 00:05:07,280
the tundra, thousands of years ago,
and some live here still.

40
00:05:17,360 --> 00:05:22,239
The Dolgan are one of the few tribes
who still live in much the same way

41
00:05:22,240 --> 00:05:24,600
as those first Arctic pioneers.

42
00:05:33,400 --> 00:05:35,319
They are reindeer people.

43
00:05:35,320 --> 00:05:41,279
Reindeer are one of few animals that can
endure these bitterly cold conditions,

44
00:05:41,280 --> 00:05:45,360
scraping a living by nibbling tiny
plants that survive beneath the snow.

45
00:05:50,200 --> 00:05:52,879
Nobody has ever totally
tamed reindeer,

46
00:05:52,880 --> 00:05:56,639
but today's animals are docile enough
to allow the Dolgan

47
00:05:56,640 --> 00:06:00,840
to drive them across the tundra in an
everlasting search for their food.

48
00:06:18,720 --> 00:06:24,120
This is a typical Dolgan village,
home to just two extended families.

49
00:06:36,000 --> 00:06:39,999
Here, in the coldest part of Arctic,
the only way to get water

50
00:06:40,000 --> 00:06:43,920
for nine months of the year is to
melt ice from the frozen rivers.

51
00:06:50,160 --> 00:06:54,079
At least there's no problem
preventing food from decay.

52
00:06:54,080 --> 00:06:56,280
Outside is one big deep-freeze.

53
00:07:00,040 --> 00:07:02,559
Survival is only possible
because of reindeer fur.

54
00:07:02,560 --> 00:07:05,839
It makes wonderfully warm clothing,

55
00:07:05,840 --> 00:07:08,240
though small children still
have to be sewn into

56
00:07:08,241 --> 00:07:10,640
their clothes to prevent
instant frostbite.

57
00:07:13,800 --> 00:07:17,880
The Dolgan even use reindeer fur to
insulate their huts.

58
00:07:20,920 --> 00:07:24,680
This is living at its most communal.

59
00:07:25,840 --> 00:07:28,640
Good relations with the in-laws are
essential.

60
00:07:30,920 --> 00:07:33,720
Reindeer are so valuable
that the people

61
00:07:33,721 --> 00:07:36,519
only eat them if they
have no other choice.

62
00:07:36,520 --> 00:07:40,320
Their favourite food is
raw fish from the frozen rivers.

63
00:07:43,400 --> 00:07:46,140
Every week or so, these
families have to travel

64
00:07:46,141 --> 00:07:48,880
to find new feeding
grounds for their herds.

65
00:07:51,760 --> 00:07:54,759
First, they round up their strongest
animals with lassoes,

66
00:07:54,760 --> 00:07:57,260
a skill that their
ancestors brought with

67
00:07:57,261 --> 00:07:59,760
them when they came north
from Central Asia.

68
00:08:07,560 --> 00:08:10,880
And then, literally, they move house.

69
00:08:27,600 --> 00:08:31,199
A whole Dolgan village can move
on in just a few hours.

70
00:08:31,200 --> 00:08:36,840
Over the year, they travel hundreds of
miles like this across the vast tundra.

71
00:08:57,600 --> 00:09:01,879
It was the herds of reindeer, wandering
over the lands of the Arctic,

72
00:09:01,880 --> 00:09:04,199
that brought the first Dolgan here.

73
00:09:04,200 --> 00:09:08,719
Other people, however,
took on an even greater challenge.

74
00:09:08,720 --> 00:09:13,080
They left the land and looked
for their food out on the frozen sea.

75
00:09:20,360 --> 00:09:25,480
Here in the shifting world of the sea
ice, they found sea mammals.

76
00:09:59,640 --> 00:10:02,340
Pasha leads a group of
Inuit men in Chukotka,

77
00:10:02,341 --> 00:10:05,040
the North Eastern
corner of Russia.

78
00:10:08,120 --> 00:10:11,639
The men have travelled many
hours from home in the bitter cold,

79
00:10:11,640 --> 00:10:14,000
fighting their way through
a dangerous maze.

80
00:10:18,800 --> 00:10:23,560
These hundred tonne ice floes could
crush their small boats like eggshells.

81
00:10:39,800 --> 00:10:44,960
The men have big families, and this is
the only way they have of feeding them.

82
00:10:48,440 --> 00:10:52,400
Pasha is looking for the puffs of
steamy air produced by their quarry.

83
00:10:53,960 --> 00:10:56,480
An animal that is bigger than
many Arctic whales.

84
00:11:04,160 --> 00:11:06,880
A two tonne seal with formidable
tusks.

85
00:11:13,200 --> 00:11:14,839
A walrus.

86
00:11:14,840 --> 00:11:18,880
It's heading for open water.
They must reach it before it dives.

87
00:11:24,760 --> 00:11:27,840
An angry walrus could easily
overturn the boats.

88
00:11:35,880 --> 00:11:39,159
The harpoon sticks firmly in the
walrus's thick layer of blubber,

89
00:11:39,160 --> 00:11:42,400
and floats attached to it prevent
the animal from diving.

90
00:12:00,320 --> 00:12:05,000
Pasha wants to kill quickly with
a single clean shot.

91
00:12:17,680 --> 00:12:20,620
The hunters are exhausted
after a long day,

92
00:12:20,621 --> 00:12:23,560
but they still have a
lot more work to do.

93
00:12:25,520 --> 00:12:30,280
This enormous prize will feed
everyone's family for weeks.

94
00:12:36,040 --> 00:12:40,280
It will take many hours to butcher.
Nothing will go to waste.

95
00:12:48,720 --> 00:12:52,160
The meat is parcelled up in bags
made of the animal's skin.

96
00:13:02,980 --> 00:13:04,780
Summer is almost here.

97
00:13:11,180 --> 00:13:14,379
This far north,
the seasons change fast,

98
00:13:14,380 --> 00:13:19,140
the sun is rising higher and
growing warmer with each passing day.

99
00:13:37,500 --> 00:13:41,539
The transformation from winter to
summer is so dramatic

100
00:13:41,540 --> 00:13:45,020
that it dominates the lives of all
who live here.

101
00:13:47,900 --> 00:13:51,099
The ice around the coast has
almost disappeared,

102
00:13:51,100 --> 00:13:55,580
and gone are the seals and walrus
that Pasha and his men relied on.

103
00:13:57,980 --> 00:14:00,860
They set off on
another search for food.

104
00:14:06,580 --> 00:14:09,420
Their destination is
an island in the bay.

105
00:14:16,700 --> 00:14:19,659
Their walrus skin boat is
an ancient design,

106
00:14:19,660 --> 00:14:23,660
light enough to carry high up
the beach so it doesn't drift away.

107
00:14:25,020 --> 00:14:28,660
THEY SPEAK RUSSIAN

108
00:14:35,580 --> 00:14:38,939
Once again, the men will have to work
as a team,

109
00:14:38,940 --> 00:14:42,660
but one of them will be taking
very serious risks.

110
00:14:47,900 --> 00:14:53,219
The lightest man in the group, Kolya,
is also the oldest.

111
00:14:53,220 --> 00:14:55,860
He will trust his life
to a length of old

112
00:14:55,861 --> 00:14:58,500
nylon rope and the
strength of his friends.

113
00:15:14,460 --> 00:15:19,739
These 100 metre high cliffs are home
to thousands of guillemots,

114
00:15:19,740 --> 00:15:21,780
and Kolya is after their eggs.

115
00:15:26,620 --> 00:15:29,900
He relies on the men above to
lower him to the right place.

116
00:15:33,740 --> 00:15:37,819
Kolya is tough
but his stress is obvious.

117
00:15:37,820 --> 00:15:41,420
HE SHOUTS IN RUSSIAN

118
00:15:44,940 --> 00:15:47,579
The men lower Kolya down to
the bottom of the cliff,

119
00:15:47,580 --> 00:15:50,540
and from there, he works his way back
up the crumbling rock face.

120
00:16:08,100 --> 00:16:09,180
Stop!

121
00:16:22,900 --> 00:16:25,939
This mission will produce no more
than about 50 eggs,

122
00:16:25,940 --> 00:16:29,220
but at least there's no need to
carry a packed lunch.

123
00:16:43,980 --> 00:16:48,460
Over the years, many men have fallen to
their deaths collecting seabird eggs.

124
00:16:50,500 --> 00:16:52,860
This is truly dangerous work.

125
00:17:04,260 --> 00:17:07,219
These Arctic peoples can't
grow crops.

126
00:17:07,220 --> 00:17:11,459
The frozen ground never thaws to
allow them to do so.

127
00:17:11,460 --> 00:17:15,819
They rely on animals for their food
so the chance to collect a few

128
00:17:15,820 --> 00:17:20,060
dozen eggs has to be taken,
even if it means risking your life.

129
00:17:36,620 --> 00:17:41,459
The change of season has transformed
the Arctic's coastline

130
00:17:41,460 --> 00:17:43,940
and inland, the difference
is just as extreme.

131
00:17:48,660 --> 00:17:51,380
July temperatures on
the tundra can be

132
00:17:51,381 --> 00:17:54,100
surprisingly high, over
30 degrees centigrade.

133
00:17:58,220 --> 00:18:01,579
Reindeer now move not just to
find fresh pasture,

134
00:18:01,580 --> 00:18:05,580
but also to avoid the summer
swarms of blood sucking flies.

135
00:18:08,140 --> 00:18:12,980
To keep their animals healthy, the local
herders are driving them to the sea.

136
00:18:15,340 --> 00:18:20,019
The cooler conditions on
the coast bring relief to the herds

137
00:18:20,020 --> 00:18:22,580
and the chance, every year,
for different tribes to meet.

138
00:18:26,020 --> 00:18:28,480
Pasha and his hunters
live close by.

139
00:18:28,481 --> 00:18:30,940
They've had word of
the herders' arrival.

140
00:18:36,940 --> 00:18:41,739
The hunters' cargo is highly
prized for the winter ahead,

141
00:18:41,740 --> 00:18:44,140
fat-rich walrus meat
that's been fermenting

142
00:18:44,141 --> 00:18:46,540
in the skin bags for two months.

143
00:18:48,460 --> 00:18:53,140
Today will see an exchange that has
taken place every summer for centuries.

144
00:18:54,660 --> 00:18:58,659
The herders barter reindeer
skins for walrus meat.

145
00:18:58,660 --> 00:19:04,220
Pooling their resources has helped these
communities to survive for so long.

146
00:19:09,780 --> 00:19:14,460
A fry-up of guillemot eggs is all the
better when shared with old friends.

147
00:19:17,700 --> 00:19:22,299
Summer brings a brief
chance for isolated peoples to meet.

148
00:19:22,300 --> 00:19:26,139
This is an opportunity to exchange
news, arrange weddings,

149
00:19:26,140 --> 00:19:29,099
and tell the latest jokes.

150
00:19:29,100 --> 00:19:33,220
HE SPEAKS RUSSIAN

151
00:19:37,760 --> 00:19:40,639
By August, the summer is over.

152
00:19:40,640 --> 00:19:44,519
Winter arrives only too swiftly,
but the peoples of the Arctic,

153
00:19:44,520 --> 00:19:48,559
who came here originally to hunt,
have devised ways to deal with

154
00:19:48,560 --> 00:19:52,280
the hostile and changing conditions
that have stood the test of time.

155
00:19:55,400 --> 00:19:58,080
Today, there's a new draw to the
Arctic.

156
00:20:05,600 --> 00:20:08,560
This is Greenland, a
territory of Denmark now

157
00:20:08,561 --> 00:20:11,520
known to be rich in oil
and precious metals.

158
00:20:23,640 --> 00:20:27,360
This sled team is
part of the Danish Special Forces.

159
00:20:29,320 --> 00:20:32,759
They're on one of the world's toughest
journeys, a 2,000 mile patrol

160
00:20:32,760 --> 00:20:36,640
to maintain Denmark's
claim to this valuable wilderness.

161
00:20:41,200 --> 00:20:44,180
But the patrol's mission
is only possible with

162
00:20:44,181 --> 00:20:47,160
the help of man's oldest
Arctic companion.

163
00:20:54,840 --> 00:20:57,320
Rasmus and Roland have spent the
summer months training

164
00:20:57,321 --> 00:21:01,079
and getting to know their team of
Greenland huskies.

165
00:21:01,080 --> 00:21:05,279
They need to have a very close bond
with every single dog.

166
00:21:05,280 --> 00:21:09,079
This is Roger and Armstrong,

167
00:21:09,080 --> 00:21:14,160
actually the oldest dog in the whole
patrol, but he's still going strong.

168
00:21:16,920 --> 00:21:20,799
The men are totally dependent on
the stamina of their dogs,

169
00:21:20,800 --> 00:21:25,400
which will keep on running all through
the bitter cold of the winter.

170
00:21:30,680 --> 00:21:35,200
This is the last time the team will
see the sun for two months.

171
00:21:40,120 --> 00:21:42,839
The most intelligent dogs
always lead,

172
00:21:42,840 --> 00:21:47,080
choosing the safest route, feeling
for hidden crevasses and thin ice.

173
00:21:57,640 --> 00:22:02,199
This is one of six teams that patrol
the whole of northeast Greenland,

174
00:22:02,200 --> 00:22:04,980
the only people in an
empty wilderness that is

175
00:22:04,981 --> 00:22:07,760
larger than France and
Great Britain combined.

176
00:22:11,880 --> 00:22:16,359
Conditions here are too extreme
for current mining technology,

177
00:22:16,360 --> 00:22:20,319
but some day, ways will be found of
digging out the huge mineral treasures

178
00:22:20,320 --> 00:22:22,560
that lie hidden within these
mountains.

179
00:22:25,280 --> 00:22:31,080
The patrol secures Denmark's claim to
do so simply by being here.

180
00:22:42,480 --> 00:22:44,880
But it's not the
prospect of getting rich

181
00:22:44,881 --> 00:22:47,279
that makes men sign
up for this patrol,

182
00:22:47,280 --> 00:22:49,720
it's the chance for the journey of a
lifetime.

183
00:22:50,760 --> 00:22:53,540
The team travel over
the ice for six

184
00:22:53,541 --> 00:22:56,320
months, covering up
to 40 miles in a day.

185
00:23:00,240 --> 00:23:03,760
Friendship and teamwork are essential
if they're to succeed.

186
00:23:05,760 --> 00:23:10,399
Their dogs can sleep outside no
matter how cold it gets.

187
00:23:10,400 --> 00:23:13,800
Rasmus and Roland have a
nice cosy tent.

188
00:23:17,920 --> 00:23:21,359
They have a few modern conveniences,
including a radio,

189
00:23:21,360 --> 00:23:25,679
with which they report their position
back to headquarters in Denmark

190
00:23:25,680 --> 00:23:27,720
and catch up on the latest news.

191
00:23:31,520 --> 00:23:33,759
Right now it's the section of money,

192
00:23:33,760 --> 00:23:38,519
what's new in the economy in
Denmark, the financial crisis

193
00:23:38,520 --> 00:23:42,000
and all the other things that we
actually don't care about out here.

194
00:23:43,480 --> 00:23:45,960
If you can cope with
the conditions, then

195
00:23:45,961 --> 00:23:48,439
winter in the Arctic
can be magical,

196
00:23:48,440 --> 00:23:52,640
especially when the greatest
light show on Earth is overhead.

197
00:24:02,280 --> 00:24:05,959
The first humans in the Arctic
believed the Northern Lights,

198
00:24:05,960 --> 00:24:09,799
or aurora borealis,
were dancing spirits.

199
00:24:09,800 --> 00:24:12,740
Now we know the lights are
caused by electrically

200
00:24:12,741 --> 00:24:15,679
charged particles
streaming from the sun,

201
00:24:15,680 --> 00:24:19,040
attracted by the magnetic
pull of the earth's poles.

202
00:24:31,760 --> 00:24:36,239
A big aurora storm contains enough
energy to knock out satellite

203
00:24:36,240 --> 00:24:40,639
communications and power supplies
across the northern hemisphere,

204
00:24:40,640 --> 00:24:44,000
so understanding the aurora
is vital.

205
00:24:49,880 --> 00:24:53,160
In Alaska,
rockets are used to study the Lights.

206
00:24:55,680 --> 00:24:59,159
A hundred miles up, at the edge
of outer space, the rockets

207
00:24:59,160 --> 00:25:02,800
release a cloud of glowing smoke
that's visible from earth.

208
00:25:07,920 --> 00:25:12,160
The smoke is blown by fierce winds
which are generated by the aurora.

209
00:25:16,120 --> 00:25:19,919
Mapping the movement of the smoke
helps scientists to understand

210
00:25:19,920 --> 00:25:23,800
how this unearthly spectacle
affects our atmosphere.

211
00:25:44,800 --> 00:25:49,879
They constantly monitor
the aurora to help protect us

212
00:25:49,880 --> 00:25:53,039
from its effects, so the rest of us
can simply enjoy the magic,

213
00:25:53,040 --> 00:25:57,000
just as the Arctic's first people must
have done, thousands of years ago.

214
00:26:12,880 --> 00:26:15,599
For all the many
peoples of the Arctic,

215
00:26:15,600 --> 00:26:18,240
the aurora is a
reminder of the sun's

216
00:26:18,241 --> 00:26:20,880
presence throughout the
dark days of winter.

217
00:26:28,620 --> 00:26:31,499
But when the sun
is below the horizon in the north,

218
00:26:31,500 --> 00:26:34,300
it's above it at the southern
end of our planet.

219
00:26:34,580 --> 00:26:37,340
Here, humanity's history has
been very different.

220
00:26:43,060 --> 00:26:46,499
Antarctica is far colder
than the Arctic,

221
00:26:46,500 --> 00:26:51,220
and 99% of its land is permanently
blanketed by ice.

222
00:26:53,020 --> 00:26:56,539
Antarctica is so utterly remote

223
00:26:56,540 --> 00:26:59,220
and inhospitable that no people
ever settled here.

224
00:27:02,780 --> 00:27:06,379
It was only 200 years ago that the
first human beings

225
00:27:06,380 --> 00:27:08,900
even glimpsed the vast continent.

226
00:27:16,620 --> 00:27:19,819
The first people who crossed
the Southern Ocean did

227
00:27:19,820 --> 00:27:23,819
so for the same reason that the first
people went to the Arctic Ocean,

228
00:27:23,820 --> 00:27:26,140
to hunt sea mammals.

229
00:27:38,660 --> 00:27:42,819
The populations of whales
and seals are only now beginning to

230
00:27:42,820 --> 00:27:46,460
recover from 150 years
of intensive hunting.

231
00:28:01,820 --> 00:28:04,680
But none of those hunters
ever tried to venture into

232
00:28:04,681 --> 00:28:07,540
the frigid interior of
the Antarctic continent.

233
00:28:09,540 --> 00:28:14,700
The first successful attempt to do that
was made only a hundred years ago.

234
00:28:16,900 --> 00:28:19,500
This hut was the base
for one of the most

235
00:28:19,501 --> 00:28:22,100
famous expeditions
in polar history.

236
00:28:28,940 --> 00:28:33,419
It was from here in 1911 that
Captain Scott and his team

237
00:28:33,420 --> 00:28:37,860
launched their attempt to be the
first people to reach the South Pole.

238
00:28:42,300 --> 00:28:44,899
The cold, dry conditions have
preserved the interior

239
00:28:44,900 --> 00:28:49,380
of the hut almost exactly
as the expedition members left it.

240
00:28:55,020 --> 00:28:57,900
Expedition photographer
Herbert Ponting

241
00:28:57,901 --> 00:29:00,780
captured the spirit of
the age of exploration.

242
00:29:05,860 --> 00:29:10,380
These first explorers borrowed
the techniques of the Arctic peoples.

243
00:29:11,620 --> 00:29:16,940
They wore fur gloves and boots
and burned seal blubber to keep warm.

244
00:29:19,020 --> 00:29:23,300
They built sleds based on a
traditional Inuit design.

245
00:29:29,140 --> 00:29:32,980
They even made their sleeping
bags from reindeer hide.

246
00:29:39,100 --> 00:29:44,739
Scott and his men sought the glory of
discovery in an untouched wilderness,

247
00:29:44,740 --> 00:29:47,419
and died in the attempt.

248
00:29:47,420 --> 00:29:51,179
But he, and those who followed him,
were the first to reveal

249
00:29:51,180 --> 00:29:54,340
the splendour of Antarctica to
the rest of the world.

250
00:30:02,500 --> 00:30:06,779
The lure of adventure still draws
intrepid travellers today.

251
00:30:06,780 --> 00:30:09,259
Like the first explorers,

252
00:30:09,260 --> 00:30:14,379
most modern visitors come during the
brief summer when the cold relents

253
00:30:14,380 --> 00:30:19,699
enough for the toughest icebreakers
to reach the edge of the continent,

254
00:30:19,700 --> 00:30:22,180
but most still need
a helicopter to go further.

255
00:30:31,340 --> 00:30:35,179
The scenery in Antarctica is
magnificent and dramatic,

256
00:30:35,180 --> 00:30:39,220
but what really attracts people
here is the wildlife.

257
00:30:41,540 --> 00:30:44,460
An emperor penguin colony is
a particular highlight.

258
00:30:48,820 --> 00:30:51,100
Because human beings
didn't arrive in the

259
00:30:51,101 --> 00:30:53,379
Antarctic until the
past few centuries,

260
00:30:53,380 --> 00:30:55,980
the animals have never
developed a fear of man.

261
00:31:00,260 --> 00:31:04,980
But very strict regulations govern how
close people can approach any wildlife.

262
00:31:06,860 --> 00:31:09,060
And when visitors
leave, they must take

263
00:31:09,061 --> 00:31:11,260
every trace of their
visit away with them.

264
00:31:39,060 --> 00:31:44,580
Since 1959, the whole of Antarctica has
been protected by international treaty.

265
00:31:46,380 --> 00:31:50,739
The nations of the world have agreed
that no country can claim Antarctica,

266
00:31:50,740 --> 00:31:52,900
or prospect for its oil or minerals.

267
00:31:55,980 --> 00:31:59,779
The only significant human activities
allowed here are those that

268
00:31:59,780 --> 00:32:02,540
extend our scientific knowledge.

269
00:32:06,580 --> 00:32:10,380
But unlocking Antarctica's secrets
requires some unusual tools.

270
00:32:28,620 --> 00:32:32,499
This brand new robot submarine has
been designed to go

271
00:32:32,500 --> 00:32:35,060
far beyond the limits of any human.

272
00:32:47,500 --> 00:32:50,320
Today, this diver is
putting the sub through

273
00:32:50,321 --> 00:32:53,140
its paces on one of
its very first dives.

274
00:32:54,460 --> 00:32:58,179
It's designed to be small
and nimble enough

275
00:32:58,180 --> 00:33:02,140
to explore the Antarctic seabed
without damaging it.

276
00:33:16,500 --> 00:33:20,859
The submarine's mission, as it
journeys into the unknown,

277
00:33:20,860 --> 00:33:24,220
is to map the seafloor and look for
species new to science.

278
00:33:39,780 --> 00:33:42,680
The seawater here is a
degree below zero, so even

279
00:33:42,681 --> 00:33:45,580
the toughest human diver
can't stay down for long.

280
00:33:49,180 --> 00:33:54,300
The submarine will explore deeper under
the ice than anyone has gone before.

281
00:33:58,940 --> 00:34:03,899
From the depths of the ocean to
the highest peaks of the land,

282
00:34:03,900 --> 00:34:06,680
new discoveries are being
made even in places

283
00:34:06,681 --> 00:34:09,460
which were first visited
over a century ago.

284
00:34:22,000 --> 00:34:24,780
Mount Erebus was an
irresistible draw to

285
00:34:24,781 --> 00:34:27,560
the legendary explorer
Ernest Shackleton.

286
00:34:28,760 --> 00:34:34,239
In 1908, his men became the first to
climb this active volcano.

287
00:34:34,240 --> 00:34:39,040
They soon discovered that this is the
coldest place on the Antarctic coast.

288
00:34:40,040 --> 00:34:42,160
Today's explorers
still have to guard

289
00:34:42,161 --> 00:34:44,279
against frostbite in
the height of summer

290
00:34:44,280 --> 00:34:47,960
when temperatures rarely creep
above minus 30 degrees centigrade.

291
00:34:53,920 --> 00:34:58,719
Shackleton's men had no idea
of the extraordinary spectacle

292
00:34:58,720 --> 00:35:01,040
that lay beneath their feet.

293
00:35:06,520 --> 00:35:09,119
Yep, OK, up on the wall there
somewhere now. Yeah.

294
00:35:09,120 --> 00:35:12,559
Under the ice and snow is a network
of caves,

295
00:35:12,560 --> 00:35:17,520
which only a handful of expert cavers
have ever dared to enter.

296
00:35:23,880 --> 00:35:29,080
This is the first scientific
expedition to explore them in detail.

297
00:35:34,520 --> 00:35:39,240
Here, there are ice formations that
occur nowhere else on Earth.

298
00:35:42,040 --> 00:35:46,160
Each cave contains its own unique
collection of structures.

299
00:36:13,680 --> 00:36:19,439
The team is mapping the caves to see how
their shape changes over the years.

300
00:36:19,440 --> 00:36:22,040
OK, flat side to here.

301
00:36:30,200 --> 00:36:35,239
That's 126.8 degrees for the angle.

302
00:36:35,240 --> 00:36:36,799
126.8. Correct.

303
00:36:36,800 --> 00:36:41,399
The steam leaking from vents
in the side of the volcano

304
00:36:41,400 --> 00:36:45,680
is constantly sculpting this labyrinth
that extends deep under the ice.

305
00:36:47,680 --> 00:36:51,439
When the hot breath of the volcano
hits the icy walls,

306
00:36:51,440 --> 00:36:55,160
the moisture in the air
freezes into beautiful shapes.

307
00:37:08,640 --> 00:37:13,399
Some of the crystals are so unusual
that the cavers are investigating

308
00:37:13,400 --> 00:37:16,880
a remote but tantalising
possibility about their formation.

309
00:37:19,560 --> 00:37:22,719
Could it be that some of these
extraordinary crystal shapes

310
00:37:22,720 --> 00:37:27,520
are formed by highly specialised
bacteria, living in the ice?

311
00:37:43,280 --> 00:37:45,639
Nobody yet knows the answer.

312
00:37:45,640 --> 00:37:49,479
This is just one of the many strange
mysteries that draw people to

313
00:37:49,480 --> 00:37:52,640
work in a place that is
so hostile to human life.

314
00:38:00,200 --> 00:38:05,039
While some scientists come to Erebus
to explore its bizarre ice caves,

315
00:38:05,040 --> 00:38:10,320
others visit the volcano to study
the innermost workings of our planet.

316
00:38:16,120 --> 00:38:19,999
Erebus is one of the most active
volcanoes on Earth,

317
00:38:20,000 --> 00:38:24,600
but even so, volcanologists work on
the very rim of its crater.

318
00:38:26,280 --> 00:38:31,079
They stand in the bitter cold,
while 100 metres below them

319
00:38:31,080 --> 00:38:34,560
is a lava lake where temperatures are
over a thousand degrees centigrade.

320
00:38:38,400 --> 00:38:41,280
This is a rare glimpse
of the molten rock

321
00:38:41,281 --> 00:38:44,160
that lies beneath
the earth's crust.

322
00:38:58,840 --> 00:39:02,319
But research here is looking
up as well as down,

323
00:39:02,320 --> 00:39:05,959
measuring how the gases that bubble
out of the volcano

324
00:39:05,960 --> 00:39:09,000
influence the make-up of the air we
breathe.

325
00:39:12,280 --> 00:39:17,519
Antarctica is the best place to
measure any changes in our atmosphere

326
00:39:17,520 --> 00:39:21,080
because it has the least polluted air
on Earth.

327
00:39:22,240 --> 00:39:27,280
It's also the perfect place to launch
more outward-looking missions.

328
00:39:33,120 --> 00:39:37,599
This balloon, made of material no
thicker than Clingfilm,

329
00:39:37,600 --> 00:39:40,600
will eventually grow to
be 300 metres tall.

330
00:39:43,720 --> 00:39:47,599
It will carry a device for detecting
cosmic rays,

331
00:39:47,600 --> 00:39:52,800
tiny particles from the beginning of
time that are only now reaching earth.

332
00:40:03,640 --> 00:40:08,639
The balloon will travel to the very
edge of outer space

333
00:40:08,640 --> 00:40:12,720
to gather clues about the formation
of the universe.

334
00:40:21,240 --> 00:40:26,160
Even today, very few ever make
the journey inland from the coast.

335
00:40:27,360 --> 00:40:31,800
We still know remarkably little
about the interior of the continent.

336
00:40:33,480 --> 00:40:38,719
The people on this plane are trying to
answer one of the fundamental questions.

337
00:40:38,720 --> 00:40:41,080
How much ice is there in Antarctica?

338
00:40:47,680 --> 00:40:50,039
They measure
the depth of the ice sheet,

339
00:40:50,040 --> 00:40:54,040
which is over 4,000 metres in places,
using radar.

340
00:40:55,960 --> 00:40:58,999
Their work will enable us to see

341
00:40:59,000 --> 00:41:02,280
how the volume of Antarctica's ice
changes in the future.

342
00:41:03,480 --> 00:41:07,000
It also makes it possible to map
a hidden landscape.

343
00:41:20,320 --> 00:41:25,999
This plane is following the same route
through the Trans-Antarctic Mountains

344
00:41:26,000 --> 00:41:29,119
that Captain Scott took a hundred
years ago.

345
00:41:29,120 --> 00:41:31,900
His team hauled their
sleds over a hundred

346
00:41:31,901 --> 00:41:34,680
miles up this glacier,
the Beardmore.

347
00:41:41,720 --> 00:41:46,039
Skirting seemingly endless crevasses,
with no map to guide them

348
00:41:46,040 --> 00:41:51,040
and no idea of what lay ahead, it was
a journey of extraordinary suffering.

349
00:42:02,080 --> 00:42:04,519
Their target lay beyond the
mountains,

350
00:42:04,520 --> 00:42:09,880
over 3,000 metres above sea level,
on the Antarctic Plateau.

351
00:42:16,480 --> 00:42:20,119
An unbroken sheet of ice,
larger than Western Europe,

352
00:42:20,120 --> 00:42:24,720
this is the coldest, the windiest,
the most lifeless place on Earth.

353
00:42:34,200 --> 00:42:38,559
Roald Amundsen's team narrowly
defeated Scott's to become

354
00:42:38,560 --> 00:42:44,800
the first people to reach the South
Pole on the 14th of December 1911.

355
00:42:45,960 --> 00:42:51,560
Nobody else successfully completed the
journey for nearly 50 years after that.

356
00:42:55,800 --> 00:43:00,399
But, since 1957, there has been
a permanent base at the South Pole.

357
00:43:00,400 --> 00:43:03,040
You can even land a plane on
the ice runway.

358
00:43:06,240 --> 00:43:09,060
The early explorers
would be astounded by

359
00:43:09,061 --> 00:43:11,880
the facilities at the
South Pole today.

360
00:43:15,160 --> 00:43:18,000
Construction work isn't
easy when the average

361
00:43:18,001 --> 00:43:20,840
summer temperature is minus
25 degrees Centigrade.

362
00:43:22,160 --> 00:43:25,719
But, despite the difficulties, the
most high-tech scientific research

363
00:43:25,720 --> 00:43:30,120
station ever built was
unveiled here in 2006.

364
00:43:31,880 --> 00:43:36,159
The brand new Amundsen-Scott South
Pole Station is designed to

365
00:43:36,160 --> 00:43:40,319
cope with the World's most
extreme conditions.

366
00:43:40,320 --> 00:43:45,679
The building's sloping edge deflects
the prevailing wind.

367
00:43:45,680 --> 00:43:49,239
Beneath, there are stilts that can
raise the whole building

368
00:43:49,240 --> 00:43:52,280
a further eight metres to keep
it above the accumulating snow.

369
00:44:01,080 --> 00:44:06,759
Living inside is as close to being on a
space-station as you can find on Earth.

370
00:44:06,760 --> 00:44:11,239
This base is totally self-sufficient,
the people are completely

371
00:44:11,240 --> 00:44:15,800
cut off from the outside world for
more than half the year over winter.

372
00:44:22,960 --> 00:44:27,320
The total darkness makes this the
perfect place to study the night sky.

373
00:44:35,320 --> 00:44:39,399
The group of star gazers will be
the most isolated community on our

374
00:44:39,400 --> 00:44:42,720
planet, but they will have
all their needs catered for.

375
00:44:48,320 --> 00:44:52,479
There is even a greenhouse where
fresh vegetables grow under

376
00:44:52,480 --> 00:44:57,160
artificial light all through
the darkest, coldest winter anywhere.

377
00:45:13,760 --> 00:45:18,720
The sun sets in March at the South Pole,
and won't rise again for six months.

378
00:45:24,840 --> 00:45:29,799
For a few days at this time of year,
high altitude clouds of ice crystals

379
00:45:29,800 --> 00:45:32,180
continue to catch the
sunlight, even when

380
00:45:32,181 --> 00:45:34,560
the sun itself is far
below the horizon.

381
00:45:52,160 --> 00:45:55,359
But soon all trace of the sun
disappears,

382
00:45:55,360 --> 00:46:00,560
and today's over-wintering scientists
remember the first explorers.

383
00:46:04,920 --> 00:46:08,959
These men, who endured
the winter in flimsy wooden huts,

384
00:46:08,960 --> 00:46:13,359
borrowed knowledge from the Arctic
pioneers before them,

385
00:46:13,360 --> 00:46:19,079
but they came here to study and explore,
rather than to hunt or exploit.

386
00:46:19,080 --> 00:46:24,080
They embodied the human spirit that has
enabled us to survive at the poles.

387
00:46:29,920 --> 00:46:33,599
Here, we are pushed to our limits,
but in being pushed, humanity has

388
00:46:33,600 --> 00:46:39,200
achieved the extraordinary
and opened up the last frontier.

389
00:46:39,225 --> 00:46:41,725
Subtitles by APOLLO
www.addic7ed.com

