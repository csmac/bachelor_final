﻿1
00:00:02,405 --> 00:00:04,044
(Sighs) All right. So let's
establish some ground rules.

2
00:00:04,165 --> 00:00:06,329
We sleep on our backs,
arms above the covers.

3
00:00:06,411 --> 00:00:07,960
We do not sleep on our sides...
(Sighs)

4
00:00:08,042 --> 00:00:12,791
As that could lead to another ugly spooning incident.
It was cold. It's human instinct.

5
00:00:12,858 --> 00:00:14,479
I don't want to hear about
any human instincts!

6
00:00:14,541 --> 00:00:17,257
Human instinct is what leads
to people eating other people.

7
00:00:18,434 --> 00:00:21,924
(Sighs) How did we end up in this hellhole?
It started innocently...

8
00:00:21,978 --> 00:00:23,751
(Sighs) The way a lot
of horror stories do,

9
00:00:25,688 --> 00:00:27,017
with Chinese food.
(Sighs)

10
00:00:28,391 --> 00:00:30,096
Well, maybe it's, like,
a little sliver of an almond?

11
00:00:30,194 --> 00:00:33,549
No, I'm telling you,
it's a fingernail.

12
00:00:34,759 --> 00:00:36,528
There's a fingernail in my food.
I'm gonna be sick.

13
00:00:36,630 --> 00:00:38,056
(Mouth full)
Want me to take it back?

14
00:00:38,121 --> 00:00:41,006
No, but I would love for you
to stop eating it.

15
00:00:41,081 --> 00:00:42,754
It's good,

16
00:00:42,837 --> 00:00:44,934
and what are the odds there's
another fingernail in there?

17
00:00:45,004 --> 00:00:50,275
9. The odds are 9.
Oh, and by the way,

18
00:00:50,357 --> 00:00:52,010
tomorrow night we have
dinner with my parents.

19
00:00:52,104 --> 00:00:53,917
They want to get to know you
a little bit better

20
00:00:53,983 --> 00:00:55,413
before we have
our engagement party.

21
00:00:55,486 --> 00:00:57,111
Everyone is gonna be asking
a lot of questions,

22
00:00:57,157 --> 00:01:01,529
so you know what that means... it's lyin' time.
Oh, man. I don't want to lie.

23
00:01:01,591 --> 00:01:03,784
We've known each other
for, like, five minutes.

24
00:01:03,847 --> 00:01:05,313
We're getting married
and having a baby,

25
00:01:05,370 --> 00:01:07,567
and for some reason,
my parents are okay with that.

26
00:01:07,649 --> 00:01:09,986
That is unbelievable.
We cannot risk losing that.

27
00:01:10,067 --> 00:01:12,111
They turn on my relationships like that.
(Snaps inaudibly)

28
00:01:12,181 --> 00:01:14,971
I forgot to tell you
that I can't snap.

29
00:01:15,045 --> 00:01:16,080
Do you still want to marry me?

30
00:01:17,170 --> 00:01:20,116
Of course, and your parents
aren't gonna turn on me.

31
00:01:20,180 --> 00:01:23,204
They love me. I am Casey,
giver of grandchildren.

32
00:01:24,807 --> 00:01:26,608
Well, they loved
my boyfriend mark, too,

33
00:01:26,660 --> 00:01:28,326
until he told them that
he didn't like dogs.

34
00:01:28,465 --> 00:01:30,193
Come on, dad.
This isn't fair.

35
00:01:30,261 --> 00:01:32,654
It's my house,
and I make the rules.

36
00:01:32,722 --> 00:01:34,593
Can someone please pass me
the yams?

37
00:01:35,442 --> 00:01:38,143
No yams for you, dog-hater.

38
00:01:39,440 --> 00:01:40,048
(Clatters)

39
00:01:40,174 --> 00:01:42,511
And after they found out
my prom date Billy Hicks

40
00:01:42,570 --> 00:01:45,001
got busted drinking in the park,
they turned on him, too.

41
00:01:50,235 --> 00:01:52,490
She covered him with
that guy from "Frasier"?

42
00:01:53,433 --> 00:01:55,060
Oh, yeah.
They loved that show.

43
00:01:55,148 --> 00:01:57,975
"Finally, a show that's as smart
as we are," they'd always say.

44
00:01:58,073 --> 00:02:01,872
But I can't lie. I'm terrible
at it. I get so anxious.

45
00:02:01,919 --> 00:02:04,364
I sweat. I call people
by weird nicknames.

46
00:02:04,432 --> 00:02:07,131
Nicknames? It's a thing I
do when I get nervous.

47
00:02:07,185 --> 00:02:10,403
When I panic, I get casual.
All right, well, that's okay,

48
00:02:10,442 --> 00:02:11,851
because I'm gonna be doing
most of the talking.

49
00:02:11,918 --> 00:02:13,440
(Mouth full) Now listen...
We need

50
00:02:13,509 --> 00:02:17,216
to come up with a better story of how we met.
Why? How we met is really romantic.

51
00:02:17,268 --> 00:02:18,362
You don't understand.

52
00:02:18,472 --> 00:02:20,361
The story of how
Ben and Maddie met is, like,

53
00:02:20,426 --> 00:02:22,824
the best story ever. They met
building houses for the poor,

54
00:02:22,891 --> 00:02:26,156
and my mom and dad... they met
chasing the same foul ball

55
00:02:26,231 --> 00:02:27,356
at yankee stadium.

56
00:02:27,431 --> 00:02:30,210
There was a huge pileup.
When the people cleared away,

57
00:02:30,286 --> 00:02:32,552
they were both left holding
the same ball.

58
00:02:34,088 --> 00:02:35,323
Wow.
Mm-hmm.

59
00:02:35,394 --> 00:02:37,061
That made me a little misty.

60
00:02:38,052 --> 00:02:39,870
So we need to come up with
something at least that good,

61
00:02:39,953 --> 00:02:41,655
and since we're making it up,
maybe even better.

62
00:02:41,715 --> 00:02:43,060
You got it, ladybug.

63
00:02:47,904 --> 00:02:49,944
I am already nervous.

64
00:02:51,279 --> 00:02:52,006
(Sighs)

65
00:02:52,129 --> 00:02:55,063
I wish I had just listened to
my instincts and I'd never lied.

66
00:02:55,145 --> 00:02:57,758
Did your hand just graze
my butt?

67
00:02:57,831 --> 00:03:00,108
(Chuckles)
I don't think so, bucko.

68
00:03:03,388 --> 00:03:06,176
(Man)
� I don't know where I'd be �

69
00:03:06,295 --> 00:03:09,837
� without someone to see �

70
00:03:09,910 --> 00:03:13,184
� this thing through �

71
00:03:13,814 --> 00:03:16,572
� I am such a mess �

72
00:03:16,631 --> 00:03:19,877
� even at my best �

73
00:03:19,965 --> 00:03:20,765
� I'm better with you �

74
00:03:20,800 --> 00:03:23,935
Sync by n17t01
www.addic7ed.com

75
00:03:24,022 --> 00:03:26,278
Man, you were so uncomfortable
at that dinner.

76
00:03:26,385 --> 00:03:29,914
Yeah, I was a little nervous.
At least I can control myself. (Sighs)

77
00:03:29,977 --> 00:03:31,508
I heard you fainted the first
time you met Maddie's parents.

78
00:03:31,585 --> 00:03:34,735
I did not faint!
I just fell off my chair

79
00:03:34,823 --> 00:03:39,040
and stayed on the ground for a while
because I was tired. Dude, I get it.

80
00:03:39,115 --> 00:03:41,334
Vicky and Joel are intimidating.
I felt sick to my stomach

81
00:03:41,400 --> 00:03:43,967
the whole time we were telling that
fake pedicab story. (Chuckles)

82
00:03:44,058 --> 00:03:46,273
Yeah, we just wanted
to get to know you.

83
00:03:46,335 --> 00:03:50,450
(Joel) We really only met you three weeks ago.
We don't even know the story of how you two met.

84
00:03:50,533 --> 00:03:51,818
It's the best story ever.

85
00:03:51,910 --> 00:03:55,783
The best?
I don't know about that.

86
00:03:55,865 --> 00:03:57,987
Ben and I met volunteering
for homes for humanity

87
00:03:58,053 --> 00:04:00,665
building houses for the poor.
Our story has been known

88
00:04:00,738 --> 00:04:02,874
to reduce people to tears.
(Chuckles) (Chuckles)

89
00:04:02,955 --> 00:04:04,686
Open on Maddie...
young, optimistic,

90
00:04:04,762 --> 00:04:06,557
ready to change the world.
Cut to Ben...

91
00:04:06,622 --> 00:04:09,983
hardworking, tough as the nails
he uses to build the house...

92
00:04:10,064 --> 00:04:12,975
hey! Hey.
It's about us, okay?

93
00:04:14,192 --> 00:04:16,062
So Casey was working
as a pedicab driver.

94
00:04:16,144 --> 00:04:17,608
Oh, neat.

95
00:04:17,694 --> 00:04:19,607
Was that a fun job for you?

96
00:04:19,707 --> 00:04:22,791
Sure was, pumpkin.
(Chuckles)

97
00:04:24,321 --> 00:04:28,229
Uh, Casey was taking
a passenger

98
00:04:28,317 --> 00:04:30,100
on a ride through the park,
and at the same time,

99
00:04:30,156 --> 00:04:32,467
I was on a different pedicab
going in the opposite direction,

100
00:04:32,561 --> 00:04:35,287
so as our pedicabs passed
each other, our eyes met,

101
00:04:35,379 --> 00:04:38,433
and the whole world just seemed
to freeze just for a second,

102
00:04:38,548 --> 00:04:41,268
and I knew. (Chuckles)
I just... I knew it.

103
00:04:41,344 --> 00:04:42,916
(Mouths word)
And I guess Casey knew, too,

104
00:04:43,006 --> 00:04:46,289
because he turned his pedicab
around and chased after me,

105
00:04:46,368 --> 00:04:48,428
and as he passed
a flower vendor,

106
00:04:48,507 --> 00:04:51,305
he reached out his hand and he
grabbed a dozen roses to give

107
00:04:51,392 --> 00:04:56,818
to the woman that he had just locked eyes with.
(Chuckles) Ohh. Casey. (Chuckles)

108
00:04:56,908 --> 00:04:59,301
Strong move, son. Strong move.

109
00:05:00,757 --> 00:05:03,587
So I took the flowers
and walked right up to her...

110
00:05:03,723 --> 00:05:05,773
oh, you didn't walk, honey.
You were on a pedicab.

111
00:05:05,873 --> 00:05:08,531
Right. Right.
(Chuckles)

112
00:05:08,617 --> 00:05:10,208
Is it hot in here, big man?

113
00:05:10,313 --> 00:05:14,678
So a huge crowd had gathered,
and they all started cheering

114
00:05:14,761 --> 00:05:16,704
and applauding
as we had our first kiss,

115
00:05:16,781 --> 00:05:19,150
and we've been together
ever since. (Chuckles)

116
00:05:19,229 --> 00:05:20,267
Oh, and there was a puppy.

117
00:05:20,358 --> 00:05:24,070
Oh. What a wonderful story.

118
00:05:24,155 --> 00:05:26,413
Certainly worthy
of the putney name.

119
00:05:26,496 --> 00:05:28,813
Mm. It's almost as good
as our story.

120
00:05:28,897 --> 00:05:31,141
(Chuckles) I think it's even better
than Ben and Maddie's story.

121
00:05:31,247 --> 00:05:33,828
I am so glad we did this
tonight.

122
00:05:33,920 --> 00:05:37,531
Casey, I had no idea you were
such a romantic. (Mia chuckles)

123
00:05:37,599 --> 00:05:40,101
(Gasps) You know what
Casey would love?

124
00:05:40,196 --> 00:05:42,506
Has Mia told you about
our house on the lake?

125
00:05:42,597 --> 00:05:46,931
<i>It has got to be the most
romantic place on earth.</i>

126
00:05:47,014 --> 00:05:50,009
We should all go there. I'd love to go.
Are you sure there's room for me?

127
00:05:50,083 --> 00:05:51,797
Of course. You two can take
the upstairs room.

128
00:05:51,889 --> 00:05:53,930
(Gasps) Maddie and Ben can
take the basement room.

129
00:05:54,029 --> 00:05:54,834
They'll be fine.

130
00:05:56,713 --> 00:05:59,155
The upstairs bedroom
is our room!

131
00:05:59,245 --> 00:06:00,802
We've been sleeping in there
for nine years,

132
00:06:00,891 --> 00:06:03,574
and now they just give it to Mia and Casey.
Why? (Door closes)

133
00:06:03,649 --> 00:06:05,012
'Cause of some stupid story
where he stole flowers

134
00:06:05,073 --> 00:06:07,881
and knows how to ride a bike?
And not even a real bike.

135
00:06:07,955 --> 00:06:09,302
It has three wheels,
like that's hard.

136
00:06:09,369 --> 00:06:12,595
I can't believe we get to sleep
in the upstairs bedroom.

137
00:06:12,659 --> 00:06:14,071
I knew that story was good,

138
00:06:14,144 --> 00:06:17,905
but I didn't know it was that good!
(Sighs) Is it that big a deal?

139
00:06:17,986 --> 00:06:19,540
Oh, honey, you have no idea.
(Door closes)

140
00:06:19,619 --> 00:06:22,010
The upstairs room has
a panoramic view of the lake,

141
00:06:22,086 --> 00:06:24,342
a huge bed with Fluffy pillows
like a hotel,

142
00:06:24,421 --> 00:06:25,816
and its own bathroom.

143
00:06:25,958 --> 00:06:31,097
Sleeping up there is like being queen of the lake.
And the basement room?

144
00:06:31,186 --> 00:06:32,354
It's hell down there.

145
00:06:32,439 --> 00:06:36,529
I'm not staying in the room where they gut the fish.
Well, they converted it.

146
00:06:36,624 --> 00:06:38,421
They didn't convert it.
They put a bed in it.

147
00:06:38,524 --> 00:06:40,772
(Keys jangle) Every time
Mia sleeps down there,

148
00:06:40,846 --> 00:06:42,619
she smells like the dumpster
behind the Sushi place.

149
00:06:44,334 --> 00:06:46,380
You want a glass of wine?
Two, please.

150
00:06:46,451 --> 00:06:50,353
Well, that's it. We're no longer
the number one couple.

151
00:06:50,450 --> 00:06:52,316
Once they got engaged
and pregnant,

152
00:06:52,408 --> 00:06:54,514
I knew this would happen, but I
thought we had more time!

153
00:06:54,653 --> 00:06:58,182
All right. Let's not get ahead of ourselves.
Oh, no, Ben, this is how it starts.

154
00:06:58,258 --> 00:07:00,353
First they get the best bedroom
at the lake house.

155
00:07:00,463 --> 00:07:02,752
At Thanksgiving,
I'm in the broken chair,

156
00:07:02,835 --> 00:07:04,032
and you're at the little table

157
00:07:04,127 --> 00:07:06,414
eating dark meat
and my mom's weird bean thing.

158
00:07:06,529 --> 00:07:09,925
Maddie, I think we'll be staying
in whatever bedroom we want.

159
00:07:10,017 --> 00:07:11,140
I have some information

160
00:07:11,216 --> 00:07:12,848
that will keep us
in the upstairs bedroom

161
00:07:12,945 --> 00:07:15,875
and me in all the white meat
I can handle.

162
00:07:18,814 --> 00:07:20,314
That last thing you said
was weird.

163
00:07:21,772 --> 00:07:23,869
I'm the little sister. I don't
get first dibs on anything,

164
00:07:23,982 --> 00:07:27,323
<i>but now we are
the number one couple.</i>

165
00:07:27,412 --> 00:07:29,603
<i>Ooh, it feels good.
(Chuckles)</i>

166
00:07:29,730 --> 00:07:32,482
Yeah, we get to choose where we
go on the next family vacation.

167
00:07:32,567 --> 00:07:33,827
Dollywood!

168
00:07:36,107 --> 00:07:37,063
I'll keep thinkin'.

169
00:07:38,087 --> 00:07:39,224
The reason they'll be
sleeping in the basement

170
00:07:39,307 --> 00:07:44,127
is because the story of how they met is a lie.
Really? How do you know?

171
00:07:44,216 --> 00:07:46,496
As a hotel manager,
it is my job to read people,

172
00:07:46,609 --> 00:07:49,749
and Mia's eye twitched
no less than three times

173
00:07:49,831 --> 00:07:51,497
as she told that story.

174
00:07:51,631 --> 00:07:53,543
Ohh! You're so good.

175
00:07:53,642 --> 00:07:57,502
Oh, I ain't done, baby.
Mia also touched her face a lot,

176
00:07:57,583 --> 00:07:59,272
which is
another classic indicator,

177
00:07:59,363 --> 00:08:01,584
and Casey kept calling
people weird names.

178
00:08:03,366 --> 00:08:04,481
I don't know exactly
what that means,

179
00:08:04,572 --> 00:08:07,055
but... it's got
to mean something.

180
00:08:08,181 --> 00:08:09,029
Tell me more.

181
00:08:09,129 --> 00:08:13,163
And the third,
perhaps most interesting tell

182
00:08:13,248 --> 00:08:14,241
that I discovered...

183
00:08:14,354 --> 00:08:18,577
pedicabs in Manhattan have
been on strike for six months.

184
00:08:18,940 --> 00:08:22,340
Ohh! (Chuckles)
So they made up a story,

185
00:08:22,419 --> 00:08:24,528
and they made one up
that was better than ours!

186
00:08:24,618 --> 00:08:27,352
Oh! How dare they?
She's gonna regret this.

187
00:08:27,438 --> 00:08:28,775
My parents don't like
being lied to.

188
00:08:28,875 --> 00:08:30,632
Whoa, whoa. Maddie,
you can't tell your parents.

189
00:08:30,750 --> 00:08:32,867
No, I would never do that
to her. I love my sister.

190
00:08:32,941 --> 00:08:34,774
I'm just gonna blackmail her,

191
00:08:34,867 --> 00:08:36,220
so we don't have to sleep
in the fish pit.

192
00:08:38,547 --> 00:08:40,984
Because sometimes...
Late at night...

193
00:08:42,451 --> 00:08:43,776
You can hear them screaming.

194
00:08:48,684 --> 00:08:50,729
Why are you flossing in bed?

195
00:08:51,434 --> 00:08:53,960
Well, why'd you leave a piece
of floss out for me?

196
00:08:54,604 --> 00:08:55,343
I didn't.

197
00:08:55,473 --> 00:08:58,748
Ah! Ah! Ah! (Gags)

198
00:08:58,863 --> 00:09:02,538
Why would you think I would do that?
Ugh. Well, maddie leaves me

199
00:09:02,641 --> 00:09:04,498
a piece of floss on
my bedside table every night.

200
00:09:04,620 --> 00:09:06,722
I wanna go home! I wanna
go home! I wanna go home.

201
00:09:06,829 --> 00:09:11,046
This is all Mia and Maddie's fault. (Sighs) Why
didn't we go with them when they went to check out

202
00:09:11,131 --> 00:09:12,707
the space for
the engagement party? You know,

203
00:09:12,802 --> 00:09:14,431
maybe we could've stopped
maddie from confronting Mia.

204
00:09:14,536 --> 00:09:17,776
This room is perfect for your
engagement party. (Chuckles)

205
00:09:17,868 --> 00:09:20,685
I am so excited. We can put the
dance floor there... (Chuckles)

206
00:09:20,772 --> 00:09:22,953
And we can put the hour bar here.
Hour bar?

207
00:09:23,034 --> 00:09:24,591
That's what your father
calls it.

208
00:09:24,671 --> 00:09:27,382
It's just open for an hour.
(Chuckles)

209
00:09:27,490 --> 00:09:29,816
These country club types
can drink,

210
00:09:29,893 --> 00:09:30,868
and you know your father...

211
00:09:30,990 --> 00:09:33,127
he won't even pay for me to get drunk.
(Chuckles)

212
00:09:33,562 --> 00:09:36,127
Oh, it doesn't matter. People
aren't coming here to drink.

213
00:09:36,257 --> 00:09:37,928
The real reason people are
coming is to meet Casey

214
00:09:38,017 --> 00:09:42,515
and hear the fantastical story
of how you two met. Fantastical?

215
00:09:42,659 --> 00:09:47,278
Did I say "fantastical"? I meant fantastic.
What's going on here, maddie?

216
00:09:47,376 --> 00:09:49,741
What's going on is that
Ben and I are gonna be sleeping

217
00:09:49,837 --> 00:09:52,593
in the upstairs bedroom at the lake house.
Oh. (Chuckles) No. You're not,

218
00:09:52,680 --> 00:09:56,193
because mom and dad just said Casey and I could have it.
That's because they didn't know that you lied to them.

219
00:09:56,298 --> 00:09:57,968
Pedicabs in New York...
(Scoffs)

220
00:09:58,062 --> 00:09:59,544
Have been on strike
for six months.

221
00:09:59,643 --> 00:10:04,026
No! Yes, so unless you want
mom and dad to turn on Casey,

222
00:10:04,118 --> 00:10:05,019
here's what's gonna happen.

223
00:10:05,121 --> 00:10:07,229
You guys can keep telling
your little pedicab thing,

224
00:10:07,324 --> 00:10:09,411
but your fake story can't
be better than our story.

225
00:10:09,538 --> 00:10:11,716
You're gonna lose the puppy.
Nobody grabbed roses,

226
00:10:11,790 --> 00:10:14,843
and no applauding crowd at the end.
But that's the big finish!

227
00:10:14,923 --> 00:10:17,128
Fine. You can have a small crowd.
100.

228
00:10:17,221 --> 00:10:18,646
12.
75.

229
00:10:18,759 --> 00:10:20,938
Nine. You're going in
the wrong direction!

230
00:10:21,025 --> 00:10:23,035
Keep talking.
It's gonna be four.

231
00:10:23,146 --> 00:10:24,861
Okay.
Can I at least have 15?

232
00:10:26,333 --> 00:10:28,732
Fine, but they're clapping
halfheartedly.

233
00:10:30,127 --> 00:10:34,436
She totally busted us. There was nothing I could do.
Well, great. Can we finally stop lying?

234
00:10:34,540 --> 00:10:37,827
No. We cannot stop.
I just fought tooth and nail

235
00:10:37,906 --> 00:10:40,679
to get 15 people to clap
halfheartedly. (Sighs)

236
00:10:40,815 --> 00:10:43,350
How bad is the downstairs room?

237
00:10:43,443 --> 00:10:45,242
Oh, I'll tell ya how bad it is.

238
00:10:45,355 --> 00:10:47,465
That room is always hot
when you want it cold,

239
00:10:47,550 --> 00:10:48,702
and it's always cold
when you want it hot.

240
00:10:48,792 --> 00:10:50,896
It knows what you want, and it
gives you the opposite. (Sighs)

241
00:10:51,012 --> 00:10:53,357
It'll drive you crazy, just like...
(Snaps inaudibly)

242
00:10:53,469 --> 00:10:54,194
Snap!

243
00:10:54,301 --> 00:10:56,843
Okay, I gotta tell you
something.

244
00:10:56,905 --> 00:10:59,748
I said I wouldn't say anything,
but now I have to,

245
00:10:59,825 --> 00:11:01,354
because I can't stand
seeing you so upset.

246
00:11:01,415 --> 00:11:05,250
Maddie and Ben's story
is a lie, too. What?

247
00:11:05,321 --> 00:11:06,652
You know how I have, like,

248
00:11:06,755 --> 00:11:08,624
a hundred half brothers and sisters?
Uh-huh.

249
00:11:08,692 --> 00:11:10,910
Well, one of them works
at homes for humanity,

250
00:11:11,004 --> 00:11:11,983
so I called him to say,

251
00:11:12,066 --> 00:11:13,612
"hey, you must know
my future sister-in-law,"

252
00:11:13,711 --> 00:11:20,330
so he looked 'em up. Guess what? (Whispers) They don't exist...
(Whispers) What? They don't exist?

253
00:11:20,421 --> 00:11:21,678
(Normal voice) Let me finish.

254
00:11:21,771 --> 00:11:23,668
(Lowered voice)
In the records.

255
00:11:24,818 --> 00:11:28,276
(Normal voice) They never worked there.
(High-pitched voice) Casey! (Normal voice) Oh, I love you!

256
00:11:28,370 --> 00:11:30,703
Oh, man. She was so happy.

257
00:11:30,812 --> 00:11:33,424
I remember she led me
into the bedroom, and we...

258
00:11:33,520 --> 00:11:36,639
whoa, whoa! Whoa, whoa, whoa.
New rule... no erotic stories.

259
00:11:37,771 --> 00:11:40,285
Don't worry. There's no story.
(Sighs) Good.

260
00:11:40,391 --> 00:11:43,054
'Cause the only thing left
for me to say was the word...

261
00:11:43,157 --> 00:11:44,470
(High-pitched voice)
"Boinked."

262
00:11:48,655 --> 00:11:51,194
And as the 100 people
applauded... no, wait!... 150...

263
00:11:51,298 --> 00:11:52,059
(Chuckles)

264
00:11:52,147 --> 00:11:54,495
Casey hands me the flowers,
and he says,

265
00:11:54,568 --> 00:11:57,485
"they're not as beautiful
as you, but they'll have to do,

266
00:11:57,558 --> 00:11:59,754
'cause I don't think anything in
the whole world is." (Chuckles)

267
00:11:59,839 --> 00:12:01,342
Isn't that right, honey?

268
00:12:01,432 --> 00:12:02,812
That's right, nutface.

269
00:12:07,535 --> 00:12:09,200
(Whispers) I am so sorry.

270
00:12:10,136 --> 00:12:12,135
(Chuckles) Man,
those nicknames were crazy.

271
00:12:12,215 --> 00:12:14,708
I can't believe you called
that woman Mrs. noodle-boobs.

272
00:12:14,821 --> 00:12:17,105
(Chuckles) Actually, that wasn't
a nickname. It's her real name.

273
00:12:17,200 --> 00:12:18,382
I was just pronouncing
it wrong.

274
00:12:18,509 --> 00:12:21,034
She's French, so the second "b"
is silent. It should've been...

275
00:12:21,112 --> 00:12:22,581
<i>(French accent)
Mrs. nood-leh-boo.</i>

276
00:12:24,566 --> 00:12:26,963
Just so you know,
I've been telling everyone

277
00:12:27,072 --> 00:12:29,400
that 150 people applauded
for us.

278
00:12:29,568 --> 00:12:30,664
You can't do that.

279
00:12:30,812 --> 00:12:33,094
Oh, yes, I can,

280
00:12:33,238 --> 00:12:36,562
because Casey's half brother
works at homes for humanity,

281
00:12:36,660 --> 00:12:39,897
and there is no record of you
or Ben ever working there.

282
00:12:40,058 --> 00:12:41,359
Well, it's a clerical error.

283
00:12:41,490 --> 00:12:43,364
Back then, there weren't
a lot of... computers.

284
00:12:43,481 --> 00:12:46,567
Oh, it is over, maddie.
I know your story is a lie.

285
00:12:46,685 --> 00:12:50,516
What's going on over there?
We know the story of how you guys met is a lie.

286
00:12:50,631 --> 00:12:51,716
Oh, what a bummer.

287
00:12:51,811 --> 00:12:54,645
I hate dark meat.

288
00:12:55,950 --> 00:12:56,565
Give it up, maddie,

289
00:12:56,643 --> 00:12:59,931
because you are talking to the new queen of the lake!
Yeah, well, I can still

290
00:13:00,003 --> 00:13:02,049
tell mom and dad that your story is a lie.
Oh, really? If you do,

291
00:13:02,139 --> 00:13:04,498
I'll punch you in the thigh with
my second knuckle out like this.

292
00:13:05,313 --> 00:13:08,034
If you do that, then maybe
I'll have to tell you the story

293
00:13:08,143 --> 00:13:11,305
of a young Doe-eyed deer
who loved his mother,

294
00:13:11,414 --> 00:13:13,555
but do you know
what happened to his mother?

295
00:13:13,696 --> 00:13:14,812
(Voice breaks)
Don't you "Bambi" me.

296
00:13:16,865 --> 00:13:19,463
This family is so weird.

297
00:13:20,814 --> 00:13:23,291
Oh, you haven't even seen
1% of it.

298
00:13:23,680 --> 00:13:24,462
(Chuckles)

299
00:13:24,651 --> 00:13:26,391
Wait till you see Vicky
drunk at the zoo.

300
00:13:28,262 --> 00:13:30,909
So what are we gonna do?
Nothing, because we both can destroy each other.

301
00:13:31,058 --> 00:13:35,112
Well, what about the upstairs bedroom? We split it.
You guys get holidays, and we get weekends.

302
00:13:35,200 --> 00:13:38,736
And we flip a coin for the cranberry festival.
Okay. You know what is funny?

303
00:13:38,821 --> 00:13:40,428
(Chuckles, lowered voice)
The only reason I lied

304
00:13:40,537 --> 00:13:43,905
is because your story is so good
and ours is so embarrassing.

305
00:13:44,019 --> 00:13:46,140
<i>Our story is
really embarrassing.</i>

306
00:13:46,224 --> 00:13:48,924
<i>That's why I lied. Aw.
(Chuckles) Aw.</i>

307
00:13:49,053 --> 00:13:52,679
Uh, some advice from someone
who's been lying for nine years.

308
00:13:52,790 --> 00:13:55,233
Mm-hmm. The thing is, you gotta
stay on Casey. To be honest,

309
00:13:55,314 --> 00:13:57,096
he didn't really tell it that
great at dinner the other night.

310
00:13:57,241 --> 00:13:59,297
Okay. You gotta get this
down, 'cause this is a lie

311
00:13:59,368 --> 00:14:02,838
you're gonna be telling for the rest of your lives.
Whoa. I don't know if Casey can do that.

312
00:14:02,927 --> 00:14:05,422
I'm sweating like crazy.
I called their dad "snack time."

313
00:14:05,539 --> 00:14:06,452
I'm out of control!

314
00:14:09,386 --> 00:14:11,569
And what about our baby? Are we
supposed to lie to our baby?

315
00:14:11,676 --> 00:14:12,874
(Chuckles) Oh, yeah.
You lie to your baby.

316
00:14:14,216 --> 00:14:15,756
You lie to everyone.
Forever.

317
00:14:17,144 --> 00:14:17,969
No.

318
00:14:18,125 --> 00:14:18,926
What?

319
00:14:19,043 --> 00:14:22,151
No, I... I can't do it.
Wh...

320
00:14:22,318 --> 00:14:25,930
Casey. (Lowered voice)
Casey, come here.

321
00:14:26,488 --> 00:14:28,285
(Normal voice) We're gonna
tell the truth. Really?

322
00:14:28,413 --> 00:14:29,821
We should've told the truth
from the beginning, but I just...

323
00:14:29,912 --> 00:14:32,316
I got so caught up in what
mom and dad would think

324
00:14:32,389 --> 00:14:34,333
and having a better story
than Ben and maddie,

325
00:14:34,448 --> 00:14:38,068
but none of that matters.
All that matters is me and you

326
00:14:38,167 --> 00:14:40,748
and the baby.
We can't lie to our baby.

327
00:14:40,888 --> 00:14:42,393
Thank you, Mia.

328
00:14:42,755 --> 00:14:44,685
Oh, my God. Mia.

329
00:14:44,806 --> 00:14:47,425
I just called you Mia without
adding a stupid nickname.

330
00:14:47,513 --> 00:14:49,572
I'm back!
(Chuckles) Okay.

331
00:14:49,697 --> 00:14:51,326
(Whispers) Yes!
Excuse me.

332
00:14:51,414 --> 00:14:53,068
I mean, what are you doing
anyway? There's not even music.

333
00:14:57,914 --> 00:15:00,160
Hello.
Hi, everyone. (Chuckles)

334
00:15:00,270 --> 00:15:03,517
I'm sure many of you, um,
think that you know the story

335
00:15:03,592 --> 00:15:06,475
about how Casey and I met,
but, um, that was a lie.

336
00:15:06,606 --> 00:15:08,426
I'm sorry, mom and dad,

337
00:15:08,522 --> 00:15:10,699
but Casey and I met
in the emergency room.

338
00:15:10,822 --> 00:15:13,439
Yeah, um, well,
I started the night

339
00:15:13,548 --> 00:15:15,236
at a male strip joint...
(Chuckles)

340
00:15:15,369 --> 00:15:17,525
Where I was watching
a wet briefs contest...

341
00:15:17,664 --> 00:15:19,325
That I was judging.

342
00:15:21,201 --> 00:15:23,711
Boy, she just went on and on.

343
00:15:23,836 --> 00:15:24,703
Yeah, that's
one of those stories

344
00:15:24,793 --> 00:15:26,005
where you really just need
the gist.

345
00:15:26,129 --> 00:15:28,368
Although it was nice to hear
that the judges at those things

346
00:15:28,457 --> 00:15:29,784
take their job seriously.

347
00:15:29,938 --> 00:15:33,262
Makes that hot bod trophy I won
in Daytona mean that much more.

348
00:15:33,369 --> 00:15:38,029
Is that what that bronzed thong
on your mantel is about? No.

349
00:15:40,987 --> 00:15:43,123
And as I made my way over
to congratulate the winner,

350
00:15:43,213 --> 00:15:45,454
a very talented dancer
named Apollo 13,

351
00:15:45,508 --> 00:15:48,715
I slipped on a pair
of his wet briefs.

352
00:15:48,780 --> 00:15:50,746
I fell, hit my head,
and I woke up in the hospital

353
00:15:50,830 --> 00:15:52,968
with a concussion...
(Chuckles) And, well...

354
00:15:53,066 --> 00:15:55,961
That's when I saw Casey.
(Chuckles)

355
00:15:56,120 --> 00:15:57,451
I was already at the hospital,

356
00:15:57,539 --> 00:15:59,729
because Dave, one
of the bassists in our band...

357
00:15:59,822 --> 00:16:01,420
we have seven...

358
00:16:01,558 --> 00:16:02,535
Uh...

359
00:16:02,680 --> 00:16:07,423
Uh, decided we should Pierce
our nipples.

360
00:16:07,932 --> 00:16:09,884
He has really long hair,
so when he was trying

361
00:16:09,970 --> 00:16:11,517
to Pierce mine, his hair
got stuck in the hole.

362
00:16:11,616 --> 00:16:14,183
Ugh. You went into a lot
of detail, too.

363
00:16:14,308 --> 00:16:16,567
Well, it freaked me out. His
hair got all wrapped in there,

364
00:16:16,668 --> 00:16:18,610
and he wouldn't cut it, because...
you're doing it again.

365
00:16:18,733 --> 00:16:20,211
Well, it started to ooze.

366
00:16:20,336 --> 00:16:24,994
All right. You know what? I'd
rather hear erotic stories. No!

367
00:16:27,367 --> 00:16:27,943
(Sighs)

368
00:16:28,066 --> 00:16:29,292
So we went to the hospital.
(Chuckles)

369
00:16:29,411 --> 00:16:31,603
I heard what was going on
in the room next to mine,

370
00:16:31,680 --> 00:16:35,388
so after Dave was removed...
From me... (Sighs)

371
00:16:37,210 --> 00:16:38,665
I walked over,

372
00:16:38,829 --> 00:16:40,832
and there was Mia.

373
00:16:41,080 --> 00:16:44,594
She was so beautiful
and all alone...

374
00:16:44,743 --> 00:16:45,694
(Chuckles)

375
00:16:45,805 --> 00:16:48,242
So I took her hand, and I told
her that I'd stay with her.

376
00:16:48,521 --> 00:16:52,382
(Guests) Aw. And he did, and we've
been together ever since. (Chuckles)

377
00:16:52,487 --> 00:16:53,665
(Guests) Aw.

378
00:16:53,766 --> 00:16:56,189
Maybe it's not as good
as the pedicab story,

379
00:16:56,287 --> 00:16:58,432
but we realized that,

380
00:16:58,523 --> 00:17:00,364
well, it doesn't matter
how we met.

381
00:17:00,499 --> 00:17:01,980
What matters is that we did.

382
00:17:02,468 --> 00:17:03,500
(Guests) Aw.

383
00:17:04,229 --> 00:17:06,566
That was
a weirdly sweet moment.

384
00:17:06,674 --> 00:17:09,278
Right up until maddie
ruined it.

385
00:17:09,367 --> 00:17:12,456
What Mia just said is
so true, even for us.

386
00:17:12,569 --> 00:17:14,458
It doesn't matter how
we met, Ben,

387
00:17:14,549 --> 00:17:15,743
it just matters that we met.

388
00:17:15,936 --> 00:17:20,186
No, that is not true. No.
Not if you met the way we did.

389
00:17:20,304 --> 00:17:22,803
(Chuckles) No, we have
to tell the truth, too.

390
00:17:22,913 --> 00:17:25,478
I-I can see why you would
say that. You're caught up,

391
00:17:25,586 --> 00:17:27,030
and your eyes have
that glassy look that tell me

392
00:17:27,140 --> 00:17:31,228
you're not gonna listen to anything I say. Great idea, Ben.
Let's tell everyone the truth right now.

393
00:17:31,332 --> 00:17:34,549
<i>Hey! (Laughs) Their story ended
romantically. Ours does not.</i>

394
00:17:34,644 --> 00:17:36,624
Let's just take a beat and think
about this for... everyone!

395
00:17:36,710 --> 00:17:38,438
Or just start talking.
Okay.

396
00:17:38,549 --> 00:17:41,889
(Chuckles) How about
that Mia and Casey, huh?

397
00:17:41,990 --> 00:17:42,719
(Chuckles)

398
00:17:45,101 --> 00:17:48,134
Uh, in this new spirit
of honesty

399
00:17:48,243 --> 00:17:51,006
and confessions
without repercussions,

400
00:17:51,120 --> 00:17:54,324
I also have something to say.

401
00:17:55,089 --> 00:17:58,502
Uh, Ben and I did not meet

402
00:17:58,617 --> 00:18:00,513
building houses for poor people.

403
00:18:00,980 --> 00:18:04,272
Uh, I was camping
and partying in the woods

404
00:18:04,396 --> 00:18:05,550
with a bunch of friends.

405
00:18:05,690 --> 00:18:07,672
I was at that wonderful age
before you realize

406
00:18:07,761 --> 00:18:09,756
that camping is stupid...
(Chuckles)

407
00:18:09,959 --> 00:18:14,362
And I got up in
the middle of the night to pee,

408
00:18:14,497 --> 00:18:16,062
so I stumbled
through the forest.

409
00:18:16,201 --> 00:18:18,528
Now in my defense,
I was super drunk... (Chuckles)

410
00:18:18,643 --> 00:18:21,381
And I went to pee on this rock,

411
00:18:21,514 --> 00:18:23,508
but guess what?

412
00:18:24,158 --> 00:18:25,308
It wasn't a rock.

413
00:18:26,151 --> 00:18:29,313
It was Ben. (Chuckles)

414
00:18:31,058 --> 00:18:32,134
And ever since that speech,

415
00:18:32,238 --> 00:18:34,718
we've been stuck
in the fish gutting room.

416
00:18:34,845 --> 00:18:36,824
Why do the girls get
the good room again?

417
00:18:36,950 --> 00:18:39,534
(Chuckles) Oh. Casey.

418
00:18:39,662 --> 00:18:42,074
Hey. Relationships are
a chess game.

419
00:18:42,174 --> 00:18:44,283
Now, sure, we've made
a sacrifice.

420
00:18:44,469 --> 00:18:46,945
We gave up our horsey piece
and the tall one.

421
00:18:47,098 --> 00:18:49,717
You don't play chess, do you?

422
00:18:49,831 --> 00:18:53,295
No, not even once,
but I do know

423
00:18:53,420 --> 00:18:55,059
that we've won this round,
because I guarantee you,

424
00:18:55,187 --> 00:18:57,802
those girls are feeling
really guilty.

425
00:18:58,117 --> 00:18:59,391
Ah.

426
00:18:59,780 --> 00:19:00,990
Nice. (Chuckles)

427
00:19:01,318 --> 00:19:02,597
I love being queen of the lake.

428
00:19:02,721 --> 00:19:04,467
Me, too.

429
00:19:04,657 --> 00:19:08,727
Ohh. And as your co-queen,
I can't believe

430
00:19:08,818 --> 00:19:10,013
we didn't think of this
years ago.

431
00:19:10,337 --> 00:19:12,544
I know.
Ohh.

432
00:19:12,668 --> 00:19:13,993
It's so hot in here.

433
00:19:17,095 --> 00:19:18,850
Keep your shirt on, Tarzan.

434
00:19:19,499 --> 00:19:20,541
We made a deal.

435
00:19:22,818 --> 00:19:24,733
You know, if Vicky and Joel
didn't have

436
00:19:24,851 --> 00:19:26,279
such an amazing
"how we met" story,

437
00:19:26,385 --> 00:19:27,549
then none of this
would've happened.

438
00:19:27,711 --> 00:19:31,367
I feel bad that the kids still
believe we met at yankee stadium

439
00:19:31,492 --> 00:19:33,348
both reaching for a foul ball.

440
00:19:33,482 --> 00:19:36,288
Should we have told them
how we really met?

441
00:19:36,391 --> 00:19:37,163
No.

442
00:19:37,808 --> 00:19:38,997
Of course not.

443
00:19:40,946 --> 00:19:43,478
But that was
an incredible night.

444
00:19:43,935 --> 00:19:47,266
(Vicky) You were so crazy
that night.

445
00:19:47,432 --> 00:19:49,318
(Joel) I'm still crazy.

446
00:19:49,541 --> 00:19:52,091
(Joel and Vicky chuckle)

447
00:19:52,224 --> 00:19:53,602
You're hearing this, right?

448
00:19:53,717 --> 00:19:55,289
I haven't heard anything.
I don't know anything.

449
00:19:55,420 --> 00:19:59,365
(Joel) Oh, remember this move?
It used to be your favorite.

450
00:19:59,470 --> 00:20:01,633
(Vicky) Oh, Joel.
(Giggles)

451
00:20:01,746 --> 00:20:04,505
Mmm. Oh. (Chuckles)

452
00:20:04,667 --> 00:20:05,659
Oh. (Chuckles)

453
00:20:06,638 --> 00:20:07,693
Mia was right!

454
00:20:07,820 --> 00:20:10,972
This room knows exactly
what you don't want

455
00:20:11,074 --> 00:20:12,601
and then gives it to you!

456
00:20:22,344 --> 00:20:23,183
Okay.

457
00:20:23,285 --> 00:20:26,873
All of the gentlemen's keys
are in cupid's bowl.

458
00:20:27,057 --> 00:20:28,402
Let's begin.

459
00:20:28,518 --> 00:20:30,919
Victoria, you're up first.

460
00:20:31,067 --> 00:20:34,187
<i>(Rock music playing)</i>

461
00:20:34,304 --> 00:20:36,448
(Keys jangle)

462
00:20:37,713 --> 00:20:40,201
Who's Joel?

463
00:20:40,430 --> 00:20:42,093
Right on! (Chuckles)

464
00:20:44,610 --> 00:20:47,178
Which room has a water bed?

465
00:20:47,359 --> 00:20:49,000
Sync by n17t01
www.addic7ed.com

