[INFORMATION]
[TITLE]
[AUTHOR]
[SOURCE]
[PRG]
[FILEPATH]
[DELAY]0
[CD TRACK]0
[COMMENT]
[END INFORMATION]
[SUBTITLE]
[COLF]&HFFFFFF,[STYLE]bd,[SIZE]24,[FONT]Tahoma
00:04:37.00,00:04:39.20
- It's fantastic![br]- Ha vinto?

00:04:39.28,00:04:41.72
360 feet! Yes!

00:04:42.68,00:04:44.68
Enzo!

00:04:44.76,00:04:48.92
(Enzo) Congratulations, Jacques.[br]It's wonderful. I'm happy for you.

00:04:49.00,00:04:50.59
Here.

00:04:50.92,00:04:52.44
Here.

00:04:52.51,00:04:55.36
- What is it?[br]- Little presents. Nothing much.

00:04:56.51,00:04:59.20
I knew it. I just knew it.

00:05:04.20,00:05:06.72
- Do you still collect them?[br]- Mm-hm.

00:05:06.80,00:05:08.80
You don't have that one, do you?

00:05:10.28,00:05:12.08
No.

00:05:12.16,00:05:14.44
It's beautiful. Thank you.

00:05:14.51,00:05:16.51
It's nothing.

00:05:16.59,00:05:18.59
I knew it. I told Roberto.

00:05:18.68,00:05:22.08
He's very good, that little Frenchman.[br]Very good.

00:05:22.16,00:05:25.80
- Is this a poem?[br]- No, it's a recipe for spaghetti frutti del mare.

00:05:25.88,00:05:28.48
Ha! Thank you.

00:05:30.16,00:05:34.88
You see? I was right in getting you out here.[br]You're a world champion, my friend.

00:05:46.00,00:05:47.00
It's a measuring tape.

00:05:51.26,00:05:53.46
You see? This is your record.

00:05:53.54,00:05:55.82
Three feet more than mine.

00:05:57.22,00:06:00.86
Looking at it from here,[br]it doesn't look like much, does it?

00:06:04.30,00:06:07.42
Keep your little measuring tape, my friend.

00:06:07.50,00:06:10.34
It will be a nice souvenir[br]when I beat you next.

00:08:04.78,00:08:06.78
(dolphin whistles)

00:08:22.66,00:08:24.66
(dolphin whistles)

00:08:40.82,00:08:42.34
Jacques?

00:11:05.22,00:11:06.62
God!

00:11:13.05,00:11:14.86
Did you have a nice night?

00:11:15.70,00:11:17.22
Yeah.

00:11:20.82,00:11:22.82
I was with the dolphin.

00:11:24.14,00:11:25.54
All night?

00:11:26.62,00:11:28.01
Yeah.

00:11:29.01,00:11:31.90
- With the dolphin?[br]- Yeah.

00:11:38.22,00:11:41.22
I gotta get back to New York.

00:11:45.78,00:11:50.18
Because I have my job,[br]I have my work and my... life.

00:12:04.90,00:12:07.18
Will you take me to the train station?

00:12:07.26,00:12:09.26
(clicking)

00:12:29.50,00:12:32.50
(thunder)

00:12:33.58,00:12:35.58
(train conductor's whistle)

00:12:41.30,00:12:44.58
(announcement in Italian)

00:12:49.70,00:12:51.30
Jacques!

00:12:51.78,00:12:54.54
If you're ever in New York,[br]give me a call, OK?

00:12:55.86,00:12:57.66
Jacques!

00:13:14.70,00:13:17.90
- Can I come in?[br]- No problem. I enjoy talking in my sleep.

00:13:25.86,00:13:28.62
Wake me up at 11.

00:13:42.14,00:13:44.14
(Enzo murmurs)

00:13:52.94,00:13:57.86
Why are you always talkin'? Nobody's[br]interested in what you're talkin' about.

00:13:57.98,00:14:01.98
So just stop talkin'! Other people[br]have things to say! Understand?

00:14:02.06,00:14:04.42
Stop talkin'! It's bullshit!

00:14:04.50,00:14:07.30
- She's gone?[br]- Yes, she's gone!

00:14:09.58,00:14:11.58
Hey, I'm gonna take care of you.

00:14:11.66,00:14:13.66
I've got a job, on an oil rig.

00:14:13.90,00:14:15.38
No sweat.

00:14:15.46,00:14:17.46
I'm taking you along. OK?

00:14:17.54,00:14:21.30
Come on, get your bags packed.[br]You're coming with me.

00:14:22.58,00:14:24.58
You stupid bastard.

00:14:26.78,00:14:28.78
(laughs)

00:14:28.86,00:14:31.62
We'll make a great team, the two of us.

00:14:31.70,00:14:33.70
You'll see.

00:14:33.78,00:14:35.78
OK?

00:14:39.54,00:14:41.74
(sighs)

00:15:25.86,00:15:27.86
(alarm)

00:15:45.90,00:15:47.90
(clinking)

00:15:53.42,00:15:55.62
100 feet.

00:15:57.30,00:15:59.26
(Enzo blows his nose)

00:16:01.54,00:16:03.54
You're still thinking about her.

00:16:03.62,00:16:06.82
Don't think about her any more.

00:16:06.90,00:16:09.34
There are so many women in the world.

00:16:09.46,00:16:12.82
Plenty of women everywhere. Right?

00:16:12.90,00:16:14.90
Smoking is absolutely forbidden.

00:16:14.98,00:16:16.90
It isn't lit yet.

00:16:17.54,00:16:20.34
You shouldn't even carry cigarettes on board.

00:16:21.94,00:16:24.70
Listen, we're not supposed to piss, either.

00:16:24.78,00:16:27.90
But that doesn't stop you[br]from carrying it on board.

00:16:28.38,00:16:30.38
The rules are the rules.

00:16:31.62,00:16:33.42
What's your name again?

00:16:34.22,00:16:35.78
Noireuter.

00:16:35.86,00:16:37.86
And where did you say you were from?

00:16:40.22,00:16:41.62
Brussels.

00:16:41.70,00:16:43.70
Hah!

00:16:43.78,00:16:46.50
Bella citta, bella citta.

00:16:46.58,00:16:48.58
200.

00:17:00.66,00:17:02.90
I was 17.

00:17:02.98,00:17:06.26
I loved her so much, I tried to die for her.

00:17:06.34,00:17:08.34
Two years later,...

00:17:08.42,00:17:10.66
..I couldn't even remember her name.

00:17:10.74,00:17:12.74
Let me tell you,...

00:17:12.82,00:17:16.14
..time erases everything.

00:17:16.22,00:17:18.22
I don't want to erase anything.

00:17:28.02,00:17:30.70
450 feet. End of descent.

00:17:33.02,00:17:35.94
(high-pitched)[br]We'll be left here for 50 minutes.

00:17:36.02,00:17:38.42
(high-pitched) What's with the voice?

00:17:38.50,00:17:41.34
It's nothing. Maybe the helium is up too high.

00:17:41.42,00:17:43.14
(blast of gas)

00:17:43.22,00:17:46.86
- (high-pitched) Does the voice come back?[br]- For others, yes.

00:17:46.94,00:17:49.46
For you... Who knows?

00:17:52.30,00:17:54.50
(high-pitched) Try speaking to me.

00:17:55.26,00:18:00.02
(high-pitched) I don't find this funny.[br]We were having a serious conversation!

00:18:01.42,00:18:03.42
(Enzo laughs) Mamma mia!

00:18:03.50,00:18:05.50
(Noireuter) It's OK for the helium.

00:18:06.90,00:18:09.22
Here, give me your finger.

00:18:09.70,00:18:11.70
Uh... Is that alcohol?

00:18:12.66,00:18:14.50
That's really forbidden!

00:18:14.58,00:18:16.98
Hey, do you have any other complaints?

00:18:17.06,00:18:20.10
Make a list and we'll stick it[br]on the porthole. OK?

00:18:20.18,00:18:24.62
- Suck your finger.[br]- Stop kidding around. It's...

00:18:24.70,00:18:27.98
It's dangerous at this pressure - alcohol.

00:18:28.06,00:18:30.46
Enzo, it's very deep down here.

00:18:30.54,00:18:32.54
Come on, suck your finger.

00:18:39.38,00:18:42.06
Ah, you'll be seeing mermaids everywhere.

00:18:42.14,00:18:44.14
(laughs giddily)

00:18:48.58,00:18:53.10
A thousand commanders and we get[br]the Belgian from Alcoholics Anonymous!

00:19:06.38,00:19:08.38
(Enzo and Jacques laughing)

00:19:08.86,00:19:10.74
Are we clear?

00:19:10.94,00:19:13.58
(Enzo laughing and mumbling incoherently)

00:19:19.14,00:19:21.58
(Jacques talks gibberish and giggles)

00:19:31.10,00:19:34.62
- (drunken laughter)[br]- Can you hear me?

00:19:36.18,00:19:38.18
(drunkenly humming a waltz)

00:20:03.38,00:20:05.38
(rustling)

00:20:11.26,00:20:13.58
I didn't get the fucking commercial.

00:20:26.90,00:20:29.46
So, did you see a therapist?

00:20:31.06,00:20:33.06
Yeah.

00:20:33.14,00:20:35.14
What did he say?

00:20:35.22,00:20:38.02
He said I have a decision to make.

00:20:38.10,00:20:40.10
The answer is within me.

00:20:43.26,00:20:45.26
And you paid for that?

00:20:45.34,00:20:47.34
Yeah.

00:20:50.74,00:20:52.74
What did you decide?

00:20:53.62,00:20:55.78
Nothing.

00:20:55.86,00:21:00.06
(sighs) Jesus, Jo,[br]you're driving me crazy with this already!

00:21:02.22,00:21:05.14
You don't wanna go shopping[br]or talk about my career...

00:21:05.22,00:21:07.30
..or do anything you used to wanna do!

00:21:07.38,00:21:11.26
All you wanna do is talk about this guy[br]that you don't even know!

00:21:11.34,00:21:13.62
Just make the fucking decision!

00:21:14.34,00:21:15.94
OK.

00:21:18.46,00:21:20.46
Heads - stay, tails - go.

00:21:20.54,00:21:22.14
OK?

00:21:32.34,00:21:33.82
Stay.

00:21:33.90,00:21:35.82
Good.

00:21:36.02,00:21:37.90
Bad. Very bad.

00:21:37.98,00:21:41.38
We weren't being cheated[br]by the Mafia anyway - except you.

00:21:41.46,00:21:44.22
Duffy, I know you think I'm a terrible person.

00:21:44.30,00:21:46.30
I'm not a terrible person.

00:21:49.34,00:21:51.34
I fell in love.

00:21:52.06,00:21:54.06
I mean, I really fell in love.

00:21:54.14,00:21:56.14
Do you understand?

00:21:56.22,00:21:59.50
I understand...[br]I could have you arrested for fraud.

00:21:59.58,00:22:03.58
I'd be happy to pay you back,[br]but I just don't have the money right now.

00:22:03.66,00:22:07.50
- If you dock my salary, say, $20 a week...[br]- Don't be cute, Johana.

00:22:07.58,00:22:11.22
- I guess this is where you fire me.[br]- You're right! You're fired.

00:22:11.30,00:22:12.82
Thanks.

00:22:14.34,00:22:17.30
All right, who is this guy? This lucky guy.

00:22:19.54,00:22:22.66
Remember Laurence in Peru?[br]It's one of his divers.

00:22:22.74,00:22:26.26
- The Popsicle stick?[br]- Yeah. No, it's a different guy.

00:22:29.10,00:22:32.10
Well, I'm happy for you.

00:22:34.50,00:22:36.50
Good luck.

00:22:45.62,00:22:47.62
(phone rings)

00:22:51.38,00:22:52.38
Hello.

00:22:53.30,00:22:54.90
Jacques?

00:22:55.26,00:22:57.26
It's Johana.

00:22:58.22,00:22:59.82
How are you?

00:23:01.50,00:23:03.10
Good.

00:23:03.58,00:23:05.18
I got fired.

00:23:05.26,00:23:07.26
I got this job with Enzo.

00:23:07.34,00:23:09.38
We got fired, too.

00:23:09.46,00:23:11.46
(laughs) Really?

00:23:14.74,00:23:16.74
It's strange.

00:23:22.86,00:23:24.86
Talk to me some more.

00:23:24.94,00:23:28.94
It's hard. I don't know what else to say.[br]You're so far away.

00:23:30.78,00:23:32.78
Tell me a story.

00:23:34.58,00:23:36.58
A story?

00:23:40.66,00:23:42.66
Do you know how it is...

00:23:43.54,00:23:46.98
Do you know what you're supposed to do[br]to meet a mermaid?

00:23:47.06,00:23:49.06
No.

00:23:50.50,00:23:52.82
You go down to the bottom of the sea...

00:23:53.54,00:23:56.38
..where the water isn't even blue any more...

00:23:56.46,00:23:58.46
..and the sky is only a memory...

00:23:58.54,00:24:01.34
..and you float there, in the silence,...

00:24:02.34,00:24:04.34
..and you stay there...

00:24:05.74,00:24:08.58
..and you decide... that you'll die for them.

00:24:08.66,00:24:11.02
Only then do they start comin' out.

00:24:11.10,00:24:15.50
They come, and they greet you,[br]and they judge the love you have for them.

00:24:16.50,00:24:18.50
If it's sincere,...

00:24:19.90,00:24:21.90
..if it's pure,...

00:24:23.46,00:24:25.46
..they'll be with you...

00:24:26.38,00:24:28.38
..and take you away for ever.

00:24:35.90,00:24:38.62
I like that story.

00:26:30.90,00:26:32.42
What is it?

00:26:34.46,00:26:36.46
I think I love you.

00:27:10.02,00:27:12.02
You live here?

00:27:12.10,00:27:14.50
Yeah. Sometimes.

00:27:29.42,00:27:31.94
- Who's that?[br]- That's my Uncle Louis.

00:27:39.10,00:27:41.18
I can't get him out of the bathtub.

00:27:41.26,00:27:43.26
Sorry.

00:27:50.78,00:27:52.90
(Louis gasps for breath)

00:27:57.18,00:27:58.70
Assassin!

00:28:00.50,00:28:02.90
Uncle Louis! This is my friend Johana!

00:28:02.98,00:28:04.62
Hi!

00:28:06.14,00:28:08.14
Johana is visiting me!

00:28:08.22,00:28:10.22
He's a bit deaf. He's 75.

00:28:10.30,00:28:13.22
It's very nice to meet you, Uncle Louis!

00:28:15.22,00:28:17.18
(moans)

00:28:17.26,00:28:19.78
(Louis takes deep breath and submerges)

00:28:26.50,00:28:28.82
( ''Ride Of The Valkyries'' by agner)

00:28:32.06,00:28:33.94
(music continues playing)

00:28:34.02,00:28:37.10
So, where are you from, Henrietta?

00:28:37.18,00:28:39.46
Johana! New York!

00:28:41.34,00:28:44.02
New York? What kind of name is that?

00:28:44.10,00:28:47.50
- No, I-I'm from New York.[br]- Oh, yeah!

00:28:48.54,00:28:51.30
(Louis) The best legs are from New York!

00:28:55.26,00:28:57.26
Uncle Louis...

00:28:59.02,00:29:01.02
Johana is my friend.

00:29:06.38,00:29:08.98
She's come to... stay with me...

00:29:10.86,00:29:12.86
..for a while.

00:29:12.94,00:29:16.02
Good. That way I'll see more of you.

00:29:16.10,00:29:18.10
Where are you gonna stay?

00:29:18.94,00:29:23.02
- Here.[br]- Here? In my apartment?

00:29:23.78,00:29:25.78
It's my apartment.

00:29:25.86,00:29:28.90
Liar! Cheat! Torturer!

00:29:28.98,00:29:32.02
Uncle Louis, go back to your bathtub.

00:29:32.10,00:29:35.14
I'm going to put a lock on the telephone.

00:29:38.34,00:29:40.78
Have some more wine, Uncle Louis.

00:29:43.18,00:29:45.62
Thank you, Henrietta.

00:29:46.18,00:29:48.18
(whispers) I'll change my name.

00:30:04.58,00:30:06.58
This is, uh...

00:30:06.66,00:30:08.46
..my room.

00:30:24.58,00:30:29.10
Enzo phoned. He's coming over next week[br]for another competition.

00:30:29.18,00:30:30.86
Great.

00:30:30.94,00:30:33.06
Are you gonna compete, too?

00:30:34.98,00:30:36.38
Yeah.

00:30:43.78,00:30:45.98
Are you sure you'll be all right here?

00:31:37.14,00:31:39.14
It's not like him to be late.

00:31:39.22,00:31:42.10
Of course, sometimes he doesn't show up at all.

00:31:43.02,00:31:45.02
Two dollars?!

00:31:45.50,00:31:49.26
You didn't even need to catch the fish!

00:31:49.38,00:31:52.02
It died of cancer!

00:31:52.50,00:31:54.70
(air horn)

00:31:54.78,00:31:56.50
Hello!

00:31:56.58,00:31:58.82
Enzo!

00:31:58.90,00:32:00.82
Jacques!

00:32:00.90,00:32:02.90
Johana!

00:32:04.70,00:32:06.74
Where did you get that?

00:32:06.82,00:32:09.66
That? This is not a ''that''.

00:32:09.74,00:32:13.22
This is Bonita Mariposa,[br]the famous Spanish actress,...

00:32:13.30,00:32:16.46
..who is going to be very careful[br]with the varnish...

00:32:16.54,00:32:20.18
- Hello, darling![br]- ..and who is spending a few days with us.

00:32:20.26,00:32:22.26
- Hi.[br]- I'm Bonita.

00:32:25.02,00:32:28.70
- Where is Mamma?[br]- You have eight hours before she gets here.

00:32:30.10,00:32:32.26
Va bene.

00:32:32.34,00:32:37.30
- ( ''Ride Of The Valkyries''plays loudly)[br]- He's really going totally deaf.

00:32:37.38,00:32:39.82
I'm not as deaf as you are blind!

00:32:40.94,00:32:44.58
You take care of your eyes,[br]and I'll take care of my ears!

00:32:45.02,00:32:47.30
I'm sorry, Uncle Louis. (laughs)

00:32:48.94,00:32:50.94
Come on, drink up, my boy!

00:32:51.02,00:32:53.02
Grazie.

00:32:57.78,00:32:59.78
(knocking)

00:32:59.86,00:33:03.82
Hey, tell me, what century are you in?

00:33:03.90,00:33:06.26
Second.

00:33:06.34,00:33:07.74
(knocking)

00:33:08.54,00:33:10.26
I'm glad you're here.

00:33:11.26,00:33:12.82
Thank you.

00:33:13.50,00:33:15.50
What's going on here?

00:33:15.58,00:33:18.14
We're listening to some music. With Enzo.

00:33:18.22,00:33:19.98
Come on in.

00:33:21.10,00:33:24.14
- This is Dr Laurence.[br]- Ah!

00:33:25.14,00:33:28.34
So, you must be the famous Enzo Molinari.

00:33:28.42,00:33:31.34
In the flesh! I have that honor.

00:33:36.74,00:33:39.58
(Johana) God... I'm getting wrinkles.

00:33:40.02,00:33:42.90
Mm-hm. And I'm getting pimples.

00:33:45.02,00:33:47.54
So, are you gonna live here permanently?

00:33:47.62,00:33:50.30
I don't know. Things have happened so fast.

00:33:50.38,00:33:52.38
We haven't even talked about it.

00:33:54.02,00:33:56.26
You're very much in love. I can tell.

00:33:58.30,00:34:00.30
Yeah, I am.

00:34:00.38,00:34:02.42
You have that look.

00:34:03.42,00:34:05.70
The look of a woman who wants a baby.

00:34:05.78,00:34:07.34
Baby?!

00:34:07.42,00:34:09.42
Oh, no... I...

00:34:09.50,00:34:11.94
I haven't even thought of having a baby.

00:34:12.02,00:34:16.22
God, we're just... starting[br]to get to know each other. It'd be really...

00:34:16.30,00:34:18.26
Do I really?

00:34:18.34,00:34:20.86
Well, maybe he's not quite ready, but...

00:34:21.62,00:34:23.62
..he's a nice guy, sensitive.

00:34:26.70,00:34:29.74
Well, I can't have a baby by myself.

00:34:29.82,00:34:32.26
Why not? I have a baby.

00:34:32.34,00:34:34.34
I loved his father.

00:34:34.42,00:34:36.58
We're not together any more but...

00:34:36.66,00:34:38.66
..I have his baby.

00:34:40.90,00:34:43.78
At the end, you do it alone anyway.

00:34:53.98,00:34:55.98
Enzo!

00:34:57.30,00:34:59.98
Cu! Cucu!

00:35:00.06,00:35:02.06
Via, via.

00:35:02.46,00:35:04.70
Enzo Molinari for classification.

00:35:09.86,00:35:11.50
Enzo!

00:35:14.06,00:35:16.02
Ciao.

00:35:16.10,00:35:18.10
What was his last time?

00:35:19.78,00:35:22.58
I don't know, but it was really long.

00:35:41.54,00:35:43.54
(clunk)

00:36:28.90,00:36:30.66
Hey, that's not bad.

00:36:30.74,00:36:32.90
Bravo, Enzo. Magnifiico. Bravissimo.

00:36:34.18,00:36:35.14
Enzo!

00:36:35.22,00:36:37.22
Vai, vai.

00:36:37.30,00:36:39.30
Very good, Enzo. Very good.

00:36:47.22,00:36:48.42
Aaargh!

00:36:51.86,00:36:54.22
So what? Anybody can have a bad day.

00:36:54.30,00:36:57.70
Well, I've had many,[br]when the sea doesn't want you.

00:36:57.78,00:37:01.86
It's never the sea.[br]It's Mamma, Roberto, that stupid actress!

00:37:03.78,00:37:05.78
Don't break my balls.

00:37:25.42,00:37:27.94
Anyway, I have something to tell you.

00:37:28.74,00:37:31.74
- I'm pregnant.[br]- (Sally) Argh! You're kidding!

00:37:31.82,00:37:33.62
I knew you were gonna do that!

00:37:33.70,00:37:35.90
Because... No, I'm kidding.

00:37:35.98,00:37:38.66
No. No, I wish I was.

00:37:42.94,00:37:45.90
I'll call you back tomorrow, OK? I promise.

00:37:45.98,00:37:48.10
(Johana hangs up) Hi.

00:37:59.90,00:38:01.94
Did you have a nice day?

00:38:02.98,00:38:05.14
Enzo had a bad day.

00:38:07.78,00:38:09.38
Did you dive?

00:38:14.06,00:38:16.06
No, it didn't feel right.

00:38:23.30,00:38:25.30
It's a cute baby, huh?

00:38:25.38,00:38:27.26
I love that baby.

00:38:42.86,00:38:44.66
(takes deep breath)

00:39:49.78,00:39:52.66
(cheering)

00:39:58.42,00:40:00.38
(low whistle)

00:40:00.46,00:40:02.86
Il campione delmondo!

00:40:08.42,00:40:09.62
Bravo, Enzo, bravo.

00:40:10.14,00:40:12.58
Let them try.

00:40:13.78,00:40:16.22
(men) Bravo!

00:40:41.06,00:40:43.50
(alarm)

00:41:26.66,00:41:28.66
(alarm)

00:41:42.14,00:41:44.58
(alarm)

00:41:46.74,00:41:48.54
Is he OK?

00:41:53.14,00:41:55.02
(man shouts ordering Japanese)

00:42:02.70,00:42:05.06
OK. Yes.

00:42:05.14,00:42:07.14
(shouts in Japanese)

00:42:08.30,00:42:10.30
Enzo!

00:42:12.82,00:42:14.82
(takes deep breath)

00:42:14.90,00:42:16.90
(men shout encouragement)

00:42:20.46,00:42:22.46
What are they doing?

00:42:23.06,00:42:25.06
I don't know.

00:42:30.50,00:42:32.30
(clears throat)

00:42:39.90,00:42:41.70
D'accord.

00:42:51.42,00:42:53.22
No!

00:42:54.30,00:42:56.30
(shouting in Japanese)

00:43:01.62,00:43:03.62
(shouting)

00:43:05.10,00:43:07.10
Great time!

00:43:07.18,00:43:09.18
Benissimo!

00:43:16.82,00:43:18.42
- Merci.[br]- Monsieur.

00:43:21.50,00:43:23.50
How was it?

00:43:24.82,00:43:26.82
It was dangerous.

00:43:26.90,00:43:28.90
380 feet,...

00:43:28.98,00:43:31.66
..four minutes and 50 seconds underwater.

00:43:31.74,00:43:34.02
Great.

00:43:37.38,00:43:39.14
Enzo!

00:43:39.22,00:43:41.42
Giacomino!

00:43:41.50,00:43:43.50
Congratulations!

00:43:43.70,00:43:45.42
Thank you. Thank you very much.

00:43:45.50,00:43:47.82
Thank you. Thank you very much.

00:43:48.54,00:43:50.02
Bravo, Enzo!

00:43:50.10,00:43:52.82
Mio fiiglio, bravo!

00:43:53.70,00:43:55.70
(clapping continues)

00:43:56.58,00:43:58.18
Now...

00:43:58.58,00:44:00.14
Now...

00:44:00.22,00:44:05.46
I am sure you heard some people suggest[br]it is too dangerous for us to continue.

00:44:05.54,00:44:08.26
Dangerous because[br]we don't know how to measure...

00:44:08.34,00:44:11.90
..the physiological consequences[br]of this type of dive.

00:44:11.98,00:44:16.46
And also because the scuba divers[br]are not used to going down below 300 feet.

00:44:18.06,00:44:20.06
- But...[br]- Cosa dice?

00:44:20.14,00:44:22.58
..we asked the free divers,...

00:44:22.70,00:44:24.78
..and they all insist...

00:44:24.86,00:44:27.50
..that the competition continue.

00:44:27.58,00:44:29.58
(clapping)

00:44:33.02,00:44:35.02
Bravo.

00:44:35.10,00:44:38.14
Good luck to you all. Good luck.

00:45:43.38,00:45:45.38
(breathes deeply)

00:45:49.74,00:45:51.74
One minute!

00:46:14.22,00:46:15.62
Three!

00:46:15.90,00:46:17.30
Two!

00:46:17.58,00:46:18.98
One!

00:47:37.38,00:47:39.38
You can't stay here!

00:48:05.86,00:48:07.42
Do you see him?

00:48:10.38,00:48:12.22
Hey! Do you see him?!

00:48:27.66,00:48:29.66
Jacques!

00:48:58.70,00:49:00.70
(clapping and cheering)

00:49:06.74,00:49:08.74
Bravo, Jacques! Bravo!

00:49:13.06,00:49:15.06
- (bleep)[br]- Thank you.

00:49:20.98,00:49:22.74
400 feet.

00:49:22.82,00:49:26.10
- 400 feet![br]- (cheering)

00:49:26.18,00:49:28.18
Hoo-hoo!

00:49:38.66,00:49:40.14
Bravo, Jacques!

00:49:41.54,00:49:43.34
You all right?

00:49:52.02,00:49:54.02
(Johana) I'll be right there!

00:49:54.34,00:49:56.34
Hi! Come on in.

00:49:56.98,00:49:58.98
No. I wanted to say goodbye.

00:49:59.06,00:50:01.26
- You're leaving?[br]- Yeah.

00:50:01.34,00:50:03.50
Could you please give this to Enzo?

00:50:05.30,00:50:06.90
OK.

00:50:08.70,00:50:10.70
It's no use.

00:50:11.66,00:50:15.74
Just tell him I said goodbye,[br]and give him a kiss for me. OK?

00:50:16.74,00:50:19.14
- Promise?[br]- Promise.

00:50:21.42,00:50:23.82
Maybe you shouldn't give up so easily.

00:50:25.14,00:50:27.14
Here's a present for you.

00:50:31.34,00:50:33.46
It's your baby.

00:50:34.74,00:50:36.34
Yeah.

00:50:37.74,00:50:40.26
Believe me, it will change your life,...

00:50:40.34,00:50:42.26
..only for the better.

00:50:45.34,00:50:47.50
You take care of yourself, Johana.

00:50:48.38,00:50:50.38
You, too.

00:50:50.46,00:50:52.02
Bye-bye.

00:50:52.10,00:50:53.70
Bye.

00:50:54.86,00:50:56.46
Bye.

00:51:42.10,00:51:44.10
(whistles)

00:51:55.10,00:51:58.22
(whistling and clicking)

00:52:01.66,00:52:04.74
(whistles)

00:52:07.10,00:52:08.90
Thank you.

00:52:09.90,00:52:11.50
Thank you.

00:52:16.06,00:52:18.06
(whistling continues)

00:52:55.94,00:52:58.46
Welcome to Greece, Jacques! How are you?

00:52:58.54,00:53:00.70
Welcome, Johana. Have a good trip?

00:53:00.78,00:53:04.06
OK, let me give you all the information.[br]Come with me.

00:53:19.78,00:53:21.78
Oh...

00:53:21.86,00:53:23.86
This is making me sick.

00:53:25.66,00:53:27.66
(sighs) Why?

00:53:29.22,00:53:31.30
Would you give me a glass of water?

00:53:35.74,00:53:37.74
(door opens)

00:53:52.10,00:53:54.10
Nervous about the competition?

00:53:54.62,00:53:56.14
No.

00:54:01.90,00:54:03.98
What's it feel like when you dive?

00:54:10.06,00:54:12.82
It's a feeling of slipping without falling.

00:54:14.82,00:54:17.26
The hardest thing is when you hit the bottom.

00:54:20.18,00:54:21.58
Why?

00:54:25.02,00:54:28.14
Because you have to find[br]a good reason to come back up.

00:54:29.14,00:54:31.22
And I have a hard time finding one.

00:54:38.70,00:54:40.70
Well, we have the same problem.

00:54:43.46,00:54:44.46
Why?

00:54:44.54,00:54:48.18
Because I have a hard time[br]finding a good reason to stay.

00:55:10.26,00:55:12.26
I found one.

00:55:18.62,00:55:21.54
Ciao, Enzo! Ciao.

00:55:25.98,00:55:27.66
Enzo.

00:55:27.74,00:55:29.54
Is he here?

00:55:31.18,00:55:32.58
Yeah.

00:55:35.10,00:55:36.70
Good.

00:56:18.26,00:56:20.98
It scares me when you look[br]at the sea like that.

00:56:27.38,00:56:29.74
I used to dive here when I was a boy.

00:56:33.02,00:56:35.70
I have something I want to talk to you about.

00:56:35.78,00:56:37.78
Not here.

00:56:47.10,00:56:48.50
OK.

00:56:59.74,00:57:01.74
Can we talk here?

00:57:21.22,00:57:23.22
Let's talk about my world.

00:57:25.66,00:57:27.46
My world is you.

00:57:33.22,00:57:35.22
I love you.

00:57:37.90,00:57:39.90
I wanna live with you.

00:57:41.38,00:57:43.38
I wanna have a baby with you.

00:57:45.06,00:57:47.06
I wanna have a house with you.

00:57:47.14,00:57:50.14
A car with you. A dog with you.

00:57:50.22,00:57:52.14
You know?

00:57:52.22,00:57:55.34
Jacques, I think I might be pregnant!

00:58:00.26,00:58:02.26
Did you hear me?

00:58:03.66,00:58:05.66
Jacques?

00:58:09.06,00:58:11.06
Jacques?

00:58:13.06,00:58:15.06
Jacques!

00:58:17.90,00:58:21.42
JAAAAAACQUES!

00:58:30.42,00:58:32.42
God...!

00:58:39.06,00:58:41.06
(Enzo playing piano)

00:58:49.66,00:58:51.66
Tomorrow is going to be a big day.

01:00:16.46,01:00:20.86
When I analyzed the data from[br]Mayol's last dive, it became apparent.

01:00:20.94,01:00:24.98
At the depths these men are diving,[br]it is a physiological impossibility...

01:00:25.06,01:00:28.66
..that they could retain enough oxygen[br]to make it to the surface.

01:00:28.74,01:00:33.90
The pressure is so strong, it stops the oxygen[br]from circulating through the body.

01:00:33.98,01:00:36.90
To attempt to break Mayol's record now...

01:00:36.98,01:00:38.98
..is simple suicide.

01:00:41.18,01:00:43.02
Do you understand?

01:00:44.34,01:00:46.33
Yes, I understand.

01:00:47.54,01:00:49.54
Today is just training.

01:00:51.66,01:00:54.78
- Molinari is a 17-time world champion.[br]- Three minutes!

01:00:54.86,01:00:56.86
Do you want to tell him he can't dive?

01:00:58.62,01:01:00.30
Ah.

01:01:02.49,01:01:04.62
Cancel the competition.

01:01:11.33,01:01:13.33
Two minutes!

01:01:18.57,01:01:20.38
Get Mayol in here.

01:01:24.10,01:01:25.90
(short, sharp breaths)

01:01:25.98,01:01:27.98
One minute!

01:01:29.06,01:01:31.06
(slower, deeper breaths)

01:01:42.78,01:01:45.18
They wanna cancel the competition.

01:01:45.26,01:01:47.26
For a while.

01:01:48.38,01:01:50.18
Why?

01:01:51.49,01:01:53.49
It's not safe, they say.

01:01:57.70,01:01:59.70
The doctors say...

01:02:00.98,01:02:03.41
..we can't go deeper and survive.

01:02:05.26,01:02:07.70
You say that because you have the title.

01:02:07.78,01:02:09.26
No.

01:02:09.33,01:02:11.33
I would never cheat you.

01:02:14.49,01:02:16.49
That's very nice of you.

01:02:23.90,01:02:26.82
Oh, Jesus! I told you to tell him not to go!

01:02:26.90,01:02:28.98
It's not my fault! I told him!

01:02:33.66,01:02:35.66
- No, no, no![br]- And then they die!

01:02:35.74,01:02:39.18
No! Ma che volete? E la prima[br]volta che organizzo un torneo!

01:03:43.54,01:03:46.18
(alarm)

01:03:46.26,01:03:48.02
No... No...

01:03:48.10,01:03:49.66
Wonderful!

01:03:56.86,01:03:58.38
(alarm stops)

01:04:00.74,01:04:02.30
Get away!

01:04:02.38,01:04:04.06
(Jacques) Will you get away?!

01:04:04.14,01:04:05.62
Goddamn it!

01:04:05.70,01:04:07.90
Get away!

01:04:07.98,01:04:10.98
- Give him some time! Leave him alone![br]- Out!

01:04:13.78,01:04:15.38
(Laurence) Clear off!

01:04:33.78,01:04:36.18
You were right.

01:04:38.57,01:04:40.14
About what?

01:04:41.54,01:04:43.54
It's much better down there.

01:04:47.38,01:04:49.38
It's a better place.

01:05:05.62,01:05:07.86
Push me back in the water.

01:05:08.98,01:05:10.98
No, I can't.

01:05:13.82,01:05:15.82
Jacques...

01:05:17.78,01:05:19.90
Take me back down.

01:05:27.26,01:05:29.02
Please.

01:05:47.54,01:05:49.54
(wails) No!

01:06:21.06,01:06:22.57
Oh, look!

01:07:05.90,01:07:07.78
Up with the arms... Over the top...

01:07:07.86,01:07:09.38
Come on, Jacques.

01:07:12.49,01:07:14.49
Let's get a heartbeat.

01:07:14.57,01:07:16.38
Come on, Jacques!

01:07:16.46,01:07:19.14
Come on, Jacques. Come on, Jacques!

01:07:19.22,01:07:21.62
Come back, Jacques!

01:07:21.70,01:07:23.66
Come on!

01:07:23.74,01:07:25.74
(distorted) Come on, Jacques!

01:07:43.33,01:07:45.70
(distorted voices)

01:07:49.33,01:07:51.74
Jacques! Stay with us!

01:08:11.62,01:08:13.62
(violent howl)

01:08:15.70,01:08:17.70
- (Jacques gags)[br]- Good. Good.

01:08:17.78,01:08:20.18
Good boy.

01:08:24.98,01:08:26.98
Very good.

01:08:45.94,01:08:47.94
Here.

01:08:52.06,01:08:54.06
Better to sleep.

01:09:13.46,01:09:15.46
We'll talk later.

01:09:18.33,01:09:20.33
You sleep now.

01:09:31.18,01:09:33.18
(whispers) I love you.

01:09:39.54,01:09:43.10
Doctor Laurence... This just arrived for you.

01:10:04.33,01:10:06.33
I wasn't on the boat.

01:10:12.33,01:10:14.33
The only time in 20 years,...

01:10:16.98,01:10:18.98
..and I wasn't on the boat.

01:10:22.78,01:10:24.57
I must go now.

01:10:27.74,01:10:29.74
La Mamma.

01:10:54.57,01:10:57.66
I know this isn't a good time for this but...

01:10:57.74,01:11:00.14
..I have the results of your test here.

01:11:01.14,01:11:02.86
Yes, you're pregnant.

01:11:13.41,01:11:15.41
Are you happy?

01:11:16.38,01:11:18.57
- Yes.[br]- Good.

01:11:18.66,01:11:20.66
I'm happy.

01:11:22.74,01:11:24.74
I'm scared,...

01:11:24.82,01:11:26.82
..but I'm happy.

01:11:28.06,01:11:29.86
So...

01:11:29.94,01:11:32.14
..no more ouzo.

01:11:33.33,01:11:35.33
No more cigarettes.

01:11:38.30,01:11:40.30
- You take care.[br]- Thank you.

01:14:56.02,01:14:58.02
(whispers) Jacques?

01:15:06.06,01:15:09.46
God... Laurence! Novelli!

01:15:09.54,01:15:11.74
What is it? Talk to me!

01:15:11.82,01:15:13.41
Dr Laurence!

01:15:13.49,01:15:14.49
Jacques!

01:15:15.22,01:15:17.22
Jacques!

01:15:18.22,01:15:20.22
OK. Wait a minute.

01:15:21.18,01:15:23.14
Let's talk to Laurence.

01:15:23.22,01:15:25.33
What are you doing?

01:15:25.41,01:15:27.41
Please, please don't do this.

01:15:27.49,01:15:29.30
Look, don't do this!

01:15:29.38,01:15:31.38
Why are you doing this?!

01:15:39.78,01:15:41.78
Jacques... Just talk to me!

01:16:28.06,01:16:29.86
I've got to go and see.

01:16:31.38,01:16:33.38
See what?

01:16:33.86,01:16:35.94
There's nothing to see, Jacques!

01:16:36.02,01:16:38.02
It's dark down there! It's cold!

01:16:38.10,01:16:40.10
You'll be alone!

01:16:40.18,01:16:43.57
And I'm here! I'm real! I exist!

01:17:06.57,01:17:08.57
Jacques...

01:17:08.66,01:17:10.66
I love you.

01:17:14.14,01:17:16.14
Jacques...

01:17:16.74,01:17:18.74
I'm pregnant.

01:17:26.14,01:17:28.18
Didn't you hear me?

01:18:14.57,01:18:16.57
(Johana sobs)

01:18:44.10,01:18:46.10
Go.

01:18:46.18,01:18:48.49
Go and see, my love.

01:21:03.94,01:21:05.94
No regrets

01:21:08.22,01:21:10.22
No tears

01:21:13.66,01:21:16.66
Only a strange feeling

01:21:18.66,01:21:21.66
Slipping without falling

01:21:23.90,01:21:27.49
I'm trying another world

01:21:28.26,01:21:31.86
here the water's not blue anymore

01:21:32.90,01:21:34.90
Another reality

01:21:38.18,01:21:40.78
Oh, my baby, I love you

01:21:43.33,01:21:45.33
My lady blue

01:21:48.18,01:21:51.26
I'm looking for something

01:21:53.30,01:21:55.30
That I'll never reach

01:21:59.02,01:22:02.33
I'll see eternity

01:22:17.62,01:22:19.62
No more sun

01:22:22.02,01:22:24.02
No more wind

01:22:27.57,01:22:30.46
Only a strange feeling

01:22:32.66,01:22:35.57
Living without moving

01:22:37.62,01:22:41.82
I'm trying another world

01:22:41.90,01:22:46.18
And the sky slowly fades in my mind

01:22:46.66,01:22:48.66
Just like a memory

01:22:52.26,01:22:54.26
No more reasons

01:22:56.94,01:22:59.14
No fears

01:23:02.62,01:23:05.54
Only this strange feeling

01:23:07.78,01:23:10.74
Giving without thinking

01:23:14.54,01:23:17.41
My baby, I love you

01:23:19.78,01:23:21.98
My lady blue

01:23:24.98,01:23:26.98
I'm looking for something

01:23:29.90,01:23:31.90
I'll never reach

01:23:35.38,01:23:37.38
Oooh

01:23:37.46,01:23:39.46
Baby, I love you

01:23:42.33,01:23:44.54
My lady blue

01:23:47.22,01:23:50.02
I'm searching for something

01:23:52.06,01:23:54.06
That I'll never reach

01:23:57.41,01:24:01.57
I'll see eternity

01:24:21.26,01:24:23.86
Subtitles by Visiontext

01:24:23.94,01:24:25.74
ENGLISH

