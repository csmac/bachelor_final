﻿1
00:00:31,800 --> 00:00:33,880
Mom?

2
00:00:40,800 --> 00:00:42,800
Mom?

3
00:00:48,200 --> 00:00:50,200
Christina?

4
00:01:07,720 --> 00:01:09,720
Listen.

5
00:01:11,880 --> 00:01:17,200
Sanna and I bought this for you.
It's from your home place.

6
00:01:30,200 --> 00:01:34,800
I've got nothing to do with those people.

7
00:01:38,000 --> 00:01:42,400
- Do you understand what she's singing?
- I don't like it.

8
00:01:45,640 --> 00:01:48,480
It's so screamy.

9
00:01:48,640 --> 00:01:52,600
And they steal, and lie.

10
00:01:53,960 --> 00:01:58,640
And they're nagging.

11
00:02:23,520 --> 00:02:25,920
We're here.

12
00:02:29,960 --> 00:02:33,080
It's your sister. We're going in.

13
00:02:35,080 --> 00:02:39,080
- We're not staying for long.
- No, alright.

14
00:02:43,560 --> 00:02:46,640
Her father's passing-

15
00:02:46,800 --> 00:02:52,560
- and the loss of her sister
was a painful period in her life.

16
00:02:56,560 --> 00:03:03,280
Joik became her way
of surviving, find strength...

17
00:03:05,000 --> 00:03:07,840
A way of escaping-

18
00:03:08,000 --> 00:03:12,760
-and come home.

19
00:03:14,320 --> 00:03:18,800
Home for the summer graze, -

20
00:03:18,960 --> 00:03:22,120
-that Njenna loved so much.

21
00:03:30,480 --> 00:03:34,800
- It's for grandma.
- Grandma.

22
00:03:34,960 --> 00:03:37,840
Grandma, please.

23
00:03:56,600 --> 00:03:58,920
Elle-Marja.

24
00:03:59,760 --> 00:04:02,120
My condolences.

25
00:04:02,280 --> 00:04:05,280
Why didn't you come...

26
00:04:05,440 --> 00:04:07,840
I don't understand.

27
00:04:08,000 --> 00:04:15,960
Every year she insisted on
branding your calves.

28
00:04:16,120 --> 00:04:20,080
- No, I don't understand.
- She kept your herd.

29
00:04:20,240 --> 00:04:23,640
- Yes, you do.
- I'm leaving now.

30
00:04:38,080 --> 00:04:40,560
What a nice gákti.

31
00:04:40,720 --> 00:04:44,160
- It's really nice.
- Thank you.

32
00:04:44,320 --> 00:04:48,120
- Yes, it's very nice.
- It's so beautiful.

33
00:04:50,920 --> 00:04:55,280
Look, grandma. Isn't it beautiful?

34
00:05:02,280 --> 00:05:05,800
Which she of course never had.

35
00:05:05,960 --> 00:05:08,720
We're going home now.

36
00:05:32,600 --> 00:05:36,880
We can stay with Jon-Olov
and your sister's family.

37
00:05:37,040 --> 00:05:41,080
Most of them are going straight
to the calf branding.

38
00:05:41,240 --> 00:05:45,080
- That would be so much fun for Sanna...
- No, we're going home, Olle.

39
00:05:45,240 --> 00:05:50,600
I'm too tired to drive now.
We're leaving tomorrow.

40
00:05:53,320 --> 00:05:56,120
No, I'm taking my handbag.

41
00:05:57,280 --> 00:05:59,480
Where are you going?

42
00:05:59,640 --> 00:06:03,160
- I'm checking in at the hotel.
- What?

43
00:06:04,200 --> 00:06:08,240
- I'll be fine on my own.
- Come on, mom.

44
00:06:09,640 --> 00:06:11,600
Mom?

45
00:06:14,640 --> 00:06:16,360
Grandma!

46
00:06:51,040 --> 00:06:55,400
- Where are you from?
- From Småland.

47
00:06:55,560 --> 00:07:02,440
- Are you here on vacation?
- Yes. No, I'm retired now.

48
00:07:02,600 --> 00:07:05,480
- Teacher.
- Ok.

49
00:07:05,640 --> 00:07:10,680
- The cleaners are back.
- Is that them?

50
00:07:10,840 --> 00:07:13,680
When we were up on the mountain yesterday -

51
00:07:13,840 --> 00:07:17,520
- a bunch of them made a lot of noise.

52
00:07:17,680 --> 00:07:22,920
That's not alright when you've paid
for peace and quiet.

53
00:07:23,080 --> 00:07:27,000
- Yes, they're quite noisy.
- Yes, they are.

54
00:07:27,160 --> 00:07:33,280
It's annoying that they think they
can ride motocross everywhere.

55
00:07:33,440 --> 00:07:36,760
- This is a nature reserve.
- Yes, it is.

56
00:07:37,760 --> 00:07:42,320
I though the Sami were people of nature.

57
00:08:05,400 --> 00:08:08,800
Mom? Christina?

58
00:08:11,120 --> 00:08:14,000
I know you're in there.

59
00:08:14,160 --> 00:08:19,240
We're invited to the calf branding.
There's a helicopter leaving soon.

60
00:08:20,240 --> 00:08:23,280
Mom, could you please come with us?

61
00:08:23,440 --> 00:08:28,040
So that I'm not standing there
like some clueless moron.

62
00:08:31,520 --> 00:08:33,520
Mom.

63
00:08:34,800 --> 00:08:37,240
I love you.

64
00:08:40,960 --> 00:08:43,360
Come on.

65
00:08:45,960 --> 00:08:49,200
Drink, row, read.

66
00:08:49,360 --> 00:08:52,440
Drink, row, read.

67
00:08:52,600 --> 00:08:55,520
I don't know anything further.

68
00:09:03,000 --> 00:09:05,920
Shall I talk to grandma?

69
00:09:06,080 --> 00:09:09,400
No, screw it. Let's go.

70
00:10:07,680 --> 00:10:10,120
Elle-Marja.

71
00:10:22,000 --> 00:10:24,120
Elle-Marja.

72
00:10:36,600 --> 00:10:39,000
Hold him.

73
00:10:57,400 --> 00:11:00,400
He's yours now.

74
00:11:31,040 --> 00:11:34,280
You should be on your way
a long time ago.

75
00:11:34,440 --> 00:11:36,440
Yes.

76
00:11:42,400 --> 00:11:44,800
Speak Sami.

77
00:12:16,840 --> 00:12:21,120
It's not long until Christmas.

78
00:12:21,280 --> 00:12:24,360
Your sister will be there too.

79
00:12:32,240 --> 00:12:37,640
This branding knife
belonged to your father.

80
00:12:39,360 --> 00:12:43,600
He would've wanted
you to have it.

81
00:12:50,600 --> 00:12:55,720
You can tell that your mother
can't take it anymore.

82
00:12:57,120 --> 00:13:01,880
- It won't take long.
- And then you'll be back?

83
00:13:06,280 --> 00:13:08,680
Yes.

84
00:13:21,400 --> 00:13:24,040
Go over to your big sister.

85
00:13:50,120 --> 00:13:54,400
You won't make it a single day
at boarding school with such behaviour.

86
00:13:56,400 --> 00:13:59,600
I don't wanna go.

87
00:14:02,240 --> 00:14:04,640
Close your eyes.

88
00:14:06,760 --> 00:14:09,240
Close them.

89
00:14:11,840 --> 00:14:15,680
Then imagine Norra Storfjället.

90
00:14:17,640 --> 00:14:22,880
- How does it sound?
- I can't remember.

91
00:14:23,040 --> 00:14:27,640
If you can joik it,
it feels like home.

92
00:14:27,800 --> 00:14:30,200
So joik.

93
00:15:05,680 --> 00:15:09,040
Njenna is crying aboard the boat.

94
00:15:10,080 --> 00:15:14,600
Njenna is crying aboard the boat.

95
00:15:27,200 --> 00:15:29,960
Do not joik at the school.

96
00:15:45,440 --> 00:15:47,840
Hurry up.

97
00:16:18,920 --> 00:16:21,320
One more time.

98
00:16:23,680 --> 00:16:26,720
Look. Tiny Lapp and Big Lapp.

99
00:16:27,720 --> 00:16:29,720
Good day!

100
00:16:31,080 --> 00:16:34,240
- Fucking Lapps.
- They've got to deal with the smell.

101
00:16:34,400 --> 00:16:38,200
Ask anyone.
You get a 100 to shoot one.

102
00:17:01,040 --> 00:17:04,120
Jevva.

103
00:17:04,280 --> 00:17:07,800
- I don't know the text ..
- Quiet.

104
00:17:07,960 --> 00:17:12,120
- A tiny, poor child...
- Louder and in proper Swedish.

105
00:17:12,280 --> 00:17:18,160
A tiny, poor child I am,
but happy nevertheless.

106
00:17:18,320 --> 00:17:21,760
My father...

107
00:17:21,920 --> 00:17:23,920
Hands.

108
00:17:26,840 --> 00:17:30,640
- She can tell that I don't know it.
- Quiet.

109
00:17:31,560 --> 00:17:36,400
I don't wanna hear any Sami. We're gonna
speak a language anyone understands.

110
00:17:37,440 --> 00:17:39,440
Hands.

111
00:17:46,280 --> 00:17:48,320
Elle-Marja.

112
00:17:56,160 --> 00:17:59,520
A tiny, poor child I am,
but happy nevertheless.

113
00:17:59,680 --> 00:18:02,360
I know my good Lord cares
about the young.

114
00:18:02,520 --> 00:18:06,760
He loves me, and he's devoted,
and his hand will guide me.

115
00:18:06,920 --> 00:18:11,800
His love is worth more than gold,
more than the entire world.

116
00:18:11,960 --> 00:18:14,480
Excellent.

117
00:18:15,320 --> 00:18:20,080
Elle-Marja is the one who gets to
welcome our guests from Uppsala.

118
00:18:26,800 --> 00:18:31,800
A tiny, poor child I am

119
00:18:31,960 --> 00:18:37,560
but happy nevertheless

120
00:18:37,720 --> 00:18:42,720
I know my good Lord cares

121
00:18:42,880 --> 00:18:47,880
about the young.

122
00:18:51,240 --> 00:18:53,200
Come on!

123
00:19:16,880 --> 00:19:21,600
- Do you think the King will come?
- No.

124
00:19:21,760 --> 00:19:26,240
- Why not?
- He doesn't live in Uppsala.

125
00:19:26,400 --> 00:19:30,080
- What about the Queen?
- In line.

126
00:19:32,520 --> 00:19:34,520
Let's go.

127
00:20:32,360 --> 00:20:35,280
You have to talk Swedish.

128
00:21:25,080 --> 00:21:27,080
Elle-Marja.

129
00:21:30,680 --> 00:21:33,040
Do you want some coffee?

130
00:22:24,560 --> 00:22:26,960
Do you like books?

131
00:22:35,640 --> 00:22:38,280
You can have it.

132
00:22:41,280 --> 00:22:46,360
I'm not supposed to,
but I think you should have it.

133
00:22:47,080 --> 00:22:49,320
Why not?

134
00:22:52,600 --> 00:22:55,080
Are you interested in poetry?

135
00:22:59,040 --> 00:23:01,640
Are you?

136
00:23:01,800 --> 00:23:03,800
Wait...

137
00:23:05,440 --> 00:23:08,640
Try reading this one.

138
00:23:08,800 --> 00:23:13,120
It was my favourite poem
when I was your age.

139
00:23:13,280 --> 00:23:15,280
That one.

140
00:23:22,960 --> 00:23:26,120
I'm longing for the country
that doesn't exist, -

141
00:23:26,280 --> 00:23:31,640
- because what does,
I'm tired of desiring.

142
00:23:31,800 --> 00:23:34,200
Isn't it beautiful?

143
00:23:51,960 --> 00:23:55,600
To become a teacher...

144
00:23:55,760 --> 00:23:59,440
- What do you have to know?
- Well...

145
00:24:00,880 --> 00:24:03,560
You have to know everything.

146
00:24:05,120 --> 00:24:09,280
You have to know a little about everything.

147
00:24:27,360 --> 00:24:30,120
Go back to your own bed.

148
00:24:32,120 --> 00:24:34,920
Go back to your own bed.

149
00:25:29,200 --> 00:25:32,200
Shall I read about Uppsala?

150
00:25:53,080 --> 00:25:59,320
On the left side of the river
lies the church, university -

151
00:25:59,480 --> 00:26:02,880
- the castle
with its dominating location, -

152
00:26:03,040 --> 00:26:06,040
- look Uppsala Slott...

153
00:26:08,960 --> 00:26:14,560
...and the library...

154
00:26:16,200 --> 00:26:20,000
...called Carolina Rediviva.

155
00:26:23,920 --> 00:26:29,120
- I want someone nicer.
- Your gákti is very nice.

156
00:26:33,880 --> 00:26:38,120
Welcome.
We would like to hand you a gift.

157
00:26:39,520 --> 00:26:43,200
It's a riegka
made from reindeer antlers.

158
00:26:43,360 --> 00:26:47,000
It's an old, Sami tradition.

159
00:26:47,720 --> 00:26:52,160
- What do you think?
- It sounds Swedish.

160
00:26:54,120 --> 00:26:58,160
Elle-Marja speaks Swedish...

161
00:27:00,920 --> 00:27:03,240
Stop it.

162
00:27:05,760 --> 00:27:07,920
Stop joking.

163
00:27:13,080 --> 00:27:18,200
- Elle-Marja speaks Swedish...
- Stop joking.

164
00:27:40,480 --> 00:27:43,160
Are you having visitors today too?

165
00:27:45,280 --> 00:27:48,200
I thought so.

166
00:28:14,960 --> 00:28:17,000
- Welcome.
- Thank you.

167
00:28:17,160 --> 00:28:21,760
- Emanuel Wennerberg.
- Christina Lajler.

168
00:28:21,920 --> 00:28:25,920
- Christina Lajler.
- Hedda Nordström.

169
00:28:27,520 --> 00:28:29,520
Elle-Marja.

170
00:28:36,480 --> 00:28:40,600
Welcome.
We would like to hand you a gift.

171
00:28:40,760 --> 00:28:47,600
It's a riegka,
which is an old, Sami tradition, -

172
00:28:47,760 --> 00:28:51,440
- made from reindeer antlers.

173
00:28:52,400 --> 00:28:55,000
Thank you very much, Elle-Marja.

174
00:29:05,600 --> 00:29:08,640
The clothes are so beautiful.

175
00:29:14,480 --> 00:29:18,880
It's so blonde.
Not very stiff.

176
00:29:29,560 --> 00:29:33,760
Let's take a picture of those
two holding hands.

177
00:29:33,920 --> 00:29:36,120
Of course.

178
00:29:36,280 --> 00:29:39,680
Elle-Marja. Come closer.

179
00:29:40,680 --> 00:29:43,080
And smile.

180
00:29:51,280 --> 00:29:53,640
Come in.

181
00:30:02,480 --> 00:30:06,280
- Take a seat.
- Elle-Marja, take it off.

182
00:30:35,000 --> 00:30:37,000
18,0.

183
00:30:37,880 --> 00:30:41,040
- What are you doing?
- So...

184
00:30:46,840 --> 00:30:49,160
15,5.

185
00:31:10,200 --> 00:31:12,600
Teeth are intact.

186
00:31:14,240 --> 00:31:16,320
What are you doing?

187
00:31:26,200 --> 00:31:28,600
3,2.

188
00:31:40,680 --> 00:31:43,080
4,4.

189
00:31:55,720 --> 00:31:58,840
Are both parents Lapps?

190
00:31:59,000 --> 00:32:00,960
Yes.

191
00:32:03,400 --> 00:32:07,760
Good. She can go over there
and take her clothes off.

192
00:32:07,920 --> 00:32:10,560
Stand on the bench.

193
00:32:12,960 --> 00:32:15,840
- Now...
- Elle-Marja.

194
00:32:41,560 --> 00:32:43,920
Take your clothes off.

195
00:32:47,560 --> 00:32:53,120
- Does she understand?
- Elle-Marja, be a good example.

196
00:32:59,880 --> 00:33:01,880
Elle-Marja!

197
00:33:23,760 --> 00:33:25,560
Good.

198
00:33:28,920 --> 00:33:32,320
Arms behind your head.

199
00:33:34,600 --> 00:33:36,600
Elle-Marja.

200
00:33:39,640 --> 00:33:42,960
Can she keep her hands behind
her head?

201
00:34:01,520 --> 00:34:03,520
Like that.

202
00:34:06,320 --> 00:34:09,120
Good. Take the picture.

203
00:34:15,960 --> 00:34:19,880
- Miss Lajler comes from Småland?
- Yes.

204
00:34:20,040 --> 00:34:22,680
- I was born there.
- Turn around.

205
00:34:22,840 --> 00:34:26,200
It's very beautiful.

206
00:34:42,440 --> 00:34:44,840
Njenna.

207
00:35:17,200 --> 00:35:21,200
Here comes the circus animal.

208
00:35:21,360 --> 00:35:26,400
They are less developed.
That's why they look like that.

209
00:35:27,440 --> 00:35:29,880
Take that back.

210
00:35:30,040 --> 00:35:32,400
- What?
- Take that back!

211
00:35:32,560 --> 00:35:37,240
- Did I say anything?
- Take back what you said!

212
00:35:37,400 --> 00:35:41,560
- There's a disease which makes...
- Take that back!

213
00:35:43,920 --> 00:35:46,120
Hold her!

214
00:35:46,280 --> 00:35:48,280
Let go!

215
00:35:51,920 --> 00:35:53,920
Let go!

216
00:35:56,960 --> 00:36:01,720
- You're as much of a Lapp as I!
- Turn the head!

217
00:36:33,120 --> 00:36:36,680
I've got the smell on my fingers now.

218
00:38:31,960 --> 00:38:33,960
Wait!

219
00:38:47,880 --> 00:38:51,440
Hello there!

220
00:38:51,600 --> 00:38:53,880
The dance...

221
00:38:57,320 --> 00:39:01,520
- Let's go.
- You can come if you want to!

222
00:39:03,360 --> 00:39:05,760
We're going now.

223
00:39:06,760 --> 00:39:11,440
Are you joining?
We'll help out with the laundry!

224
00:40:02,920 --> 00:40:06,120
- Go home.
- Where are you going?

225
00:40:10,920 --> 00:40:13,320
Go home, Njenna.

226
00:40:18,120 --> 00:40:20,920
Get down.

227
00:40:22,000 --> 00:40:26,560
Get down.
Down, Njenna. Get down.

228
00:42:18,760 --> 00:42:25,200
I've been thinking about Standard Swedish.
That's what you're speaking in Uppsala, right?

229
00:42:25,360 --> 00:42:29,400
It's two completely different things.

230
00:42:29,560 --> 00:42:32,680
- This and that.
- That's what I've been saying.

231
00:42:32,840 --> 00:42:35,720
I don't get it.

232
00:42:35,880 --> 00:42:39,840
That doesn't surprise me.

233
00:42:42,000 --> 00:42:44,000
Hey.

234
00:42:44,160 --> 00:42:45,960
Hey.

235
00:42:49,360 --> 00:42:51,360
Yes?

236
00:43:17,200 --> 00:43:19,200
Come.

237
00:43:20,720 --> 00:43:25,080
I'm not good at this
Or... I don't know...

238
00:43:49,160 --> 00:43:51,160
I'm sorry!

239
00:44:21,080 --> 00:44:23,160
What's your name?

240
00:44:24,200 --> 00:44:26,920
Can I guess?

241
00:44:27,080 --> 00:44:29,520
Something exotic?

242
00:44:30,560 --> 00:44:32,360
No.

243
00:44:33,400 --> 00:44:35,960
Christina.

244
00:44:38,440 --> 00:44:40,920
Are you called something exotic?

245
00:44:42,400 --> 00:44:45,160
Yes... Niklas Wikander.

246
00:44:46,040 --> 00:44:47,640
Nice.

247
00:44:49,200 --> 00:44:50,840
Thanks.

248
00:44:56,040 --> 00:44:58,680
What's Uppsala like?

249
00:45:01,320 --> 00:45:03,800
Better than this.

250
00:45:03,960 --> 00:45:06,960
I'm moving there.

251
00:45:09,160 --> 00:45:11,720
Wanna go for a walk?

252
00:45:12,720 --> 00:45:16,640
What do you mean? Go for a walk?

253
00:45:18,320 --> 00:45:21,240
Do you want me on top of Laxfjället?

254
00:45:21,400 --> 00:45:23,280
What?

255
00:45:23,440 --> 00:45:27,920
- On top of Laxfjället?
- What's going on there?

256
00:45:28,080 --> 00:45:32,840
That's where they're taking girls.

257
00:45:33,000 --> 00:45:35,680
And do what?

258
00:45:35,840 --> 00:45:38,320
Look at the view.

259
00:45:41,240 --> 00:45:43,560
How do you know?

260
00:45:45,440 --> 00:45:48,040
It's what I've heard.

261
00:46:21,720 --> 00:46:24,240
Wanna go for a swim?

262
00:46:25,280 --> 00:46:27,880
You don't like swimming?

263
00:46:37,400 --> 00:46:39,400
Elle-Marja.

264
00:46:44,760 --> 00:46:46,760
Where do you live?

265
00:46:46,920 --> 00:46:49,960
- What?
- Where do you live?

266
00:46:50,120 --> 00:46:53,480
- In Kebovägen. Why?
- I better go.

267
00:46:53,640 --> 00:46:56,120
Elle-Marja.

268
00:46:57,560 --> 00:47:02,840
- I had to tell...
- I don't understand, fucking Lapp.

269
00:48:00,720 --> 00:48:03,800
Do Elle-Marja want to say something?

270
00:49:04,120 --> 00:49:06,120
Sister?

271
00:49:16,680 --> 00:49:19,080
Answer me.

272
00:49:26,760 --> 00:49:29,880
I know you can hear me.

273
00:49:35,960 --> 00:49:37,960
Stop it.

274
00:49:42,280 --> 00:49:45,160
I can't stay.

275
00:49:55,360 --> 00:49:57,440
Sister?

276
00:50:47,280 --> 00:50:48,880
Njenna.

277
00:50:53,880 --> 00:50:56,280
Can we talk?

278
00:51:01,400 --> 00:51:03,200
Njenna, come here.

279
00:51:03,360 --> 00:51:07,200
Don't you have a Swede
to take care of you now?

280
00:51:07,360 --> 00:51:10,160
Go to him.

281
00:51:42,360 --> 00:51:44,440
Come in.

282
00:51:49,600 --> 00:51:52,440
I want to study more.

283
00:51:53,880 --> 00:51:56,680
- Alright?
- In Uppsala.

284
00:51:56,840 --> 00:52:01,960
You need papers for that.
Exams, certificates and so on.

285
00:52:02,880 --> 00:52:05,960
Could you write a paper for me?

286
00:52:06,120 --> 00:52:09,960
I'm sorry, but I can't.

287
00:52:12,840 --> 00:52:15,800
Why not?

288
00:52:17,560 --> 00:52:22,040
You can't keep on studying
after going to this school.

289
00:52:22,200 --> 00:52:24,240
Why not?

290
00:52:24,400 --> 00:52:28,080
You don't know the same things
as Swedish children do.

291
00:52:28,240 --> 00:52:32,920
- I've done everything right.
- Yes, but they're learning different things.

292
00:52:37,200 --> 00:52:43,080
Elle-Marja, you're very good,
but your family needs you here.

293
00:52:43,240 --> 00:52:48,960
- Only you know what it takes to be here.
- What if I don't wanna be here?

294
00:52:51,280 --> 00:52:54,760
There's nothing I can
do about that.

295
00:52:55,560 --> 00:52:58,600
Who's in charge, then?

296
00:53:01,720 --> 00:53:05,360
According to the scientific reports,
you people are not made for the city.

297
00:53:05,520 --> 00:53:10,000
Your brains are...
You don't have what it takes.

298
00:53:11,160 --> 00:53:15,160
You have to be up here,
or you'll die out.

299
00:53:20,120 --> 00:53:23,160
You could write a letter.

300
00:53:24,320 --> 00:53:26,120
Unfortunately.

301
00:54:07,120 --> 00:54:11,360
- Help!
- Quiet. Be quiet!

302
00:54:11,520 --> 00:54:15,120
Give me your belt. I'm leaving.

303
00:54:15,280 --> 00:54:18,080
Quiet.

304
00:54:18,240 --> 00:54:21,120
Be quiet.

305
00:54:21,280 --> 00:54:24,480
Are you seeing that Swede?

306
00:54:28,840 --> 00:54:33,600
Nobody likes you.
Nobody wants you here.

307
00:54:34,440 --> 00:54:37,240
You're only thinking about yourself.

308
00:54:38,240 --> 00:54:41,160
Nobody wants you here.

309
00:54:42,840 --> 00:54:46,480
You're a Lapp child
who doesn't understand anything.

310
00:54:46,640 --> 00:54:51,680
Your brains are tiny.
Nobody wants us.

311
00:54:53,040 --> 00:54:57,040
You're a bunch of idiots
who can't think for themselves.

312
00:59:27,080 --> 00:59:31,800
- Yes?
- Good evening. I'm here to see Niklas.

313
00:59:31,960 --> 00:59:34,960
He's already in Snärkes.

314
00:59:36,280 --> 00:59:38,240
In...?

315
00:59:38,400 --> 00:59:44,000
- Did Niklas go to Särmland-Närkes?
- I think so.

316
00:59:45,000 --> 00:59:48,760
But Niklas invited me.

317
00:59:49,880 --> 00:59:53,120
You'll find him at the dance.

318
01:00:15,240 --> 01:00:16,840
Yes?

319
01:00:17,920 --> 01:00:22,120
Sorry, but Niklas said that
I could come visiting.

320
01:00:24,120 --> 01:00:27,480
And that I could stay here.

321
01:00:32,520 --> 01:00:34,080
Alright.

322
01:00:39,120 --> 01:00:40,920
Elise.

323
01:00:43,200 --> 01:00:46,120
Just like in Für Elise.

324
01:00:46,280 --> 01:00:49,400
We were in the middle of eating dinner.

325
01:01:15,080 --> 01:01:18,240
I didn't quite catch your name.

326
01:01:19,840 --> 01:01:25,280
- Christina.
- How do you know Niklas?

327
01:01:26,680 --> 01:01:29,600
From a dance.

328
01:01:31,400 --> 01:01:33,520
At Udden.

329
01:01:35,800 --> 01:01:38,920
I'm from Småland, -

330
01:01:39,080 --> 01:01:43,240
- but my sister works in Norrland.

331
01:01:44,000 --> 01:01:49,560
- Gustav comes from Småland too.
- Yes, it's true.

332
01:01:49,720 --> 01:01:53,400
Where's your family living?

333
01:01:54,640 --> 01:01:58,040
In Småland. About -

334
01:01:58,200 --> 01:02:00,880
- everywhere.

335
01:02:25,880 --> 01:02:28,400
- Nice cake.
- That's good.

336
01:02:28,560 --> 01:02:32,720
We were going to serve
the same at the party.

337
01:03:22,440 --> 01:03:26,200
- We made a bed for you downstairs.
- Who's that?

338
01:03:26,360 --> 01:03:31,440
- Someone named Christina.
- Who? Who's that?

339
01:03:32,440 --> 01:03:38,920
Close the door.
You can't just invite people over.

340
01:04:02,760 --> 01:04:04,760
Hey.

341
01:04:07,520 --> 01:04:12,480
I came by train today.
I've got nowhere to stay.

342
01:04:12,640 --> 01:04:15,920
Then I remembered that you lived here.

343
01:04:18,040 --> 01:04:22,040
- I didn't mean to occupy your room.
- No problem.

344
01:04:26,960 --> 01:04:29,680
- Do you want one?
- No.

345
01:04:37,840 --> 01:04:41,240
- Sure?
- Yes.

346
01:04:48,080 --> 01:04:52,640
Can I stay for a couple of days?

347
01:04:55,400 --> 01:04:57,000
Yes...

348
01:05:04,000 --> 01:05:06,480
Could you ask?

349
01:05:07,680 --> 01:05:09,600
Yes.

350
01:05:10,600 --> 01:05:13,000
I'll ask.

351
01:07:14,000 --> 01:07:16,000
Sorry.

352
01:07:19,720 --> 01:07:22,520
What happened?

353
01:07:22,680 --> 01:07:26,320
- Nothing.
- Nothing?

354
01:07:28,600 --> 01:07:31,600
It was an accident.

355
01:07:31,760 --> 01:07:34,960
What kind of accident?

356
01:07:35,120 --> 01:07:37,520
An accident.

357
01:09:16,760 --> 01:09:19,000
What are you doing?

358
01:09:19,160 --> 01:09:21,320
Coffee-cheese.

359
01:09:23,360 --> 01:09:27,000
- It tastes good.
- What?

360
01:09:29,520 --> 01:09:33,960
I'm leaving it here.

361
01:09:35,040 --> 01:09:40,120
Maybe another time.
I prefer milk.

362
01:09:47,320 --> 01:09:50,120
Did you ask?

363
01:09:50,280 --> 01:09:53,360
Niklas, could you come here for a bit?

364
01:09:53,960 --> 01:09:56,360
I'll do it.

365
01:10:09,680 --> 01:10:15,000
We're worried that you haven't thought this
through with regards to Christina.

366
01:10:15,160 --> 01:10:17,480
The responsibility.

367
01:10:19,080 --> 01:10:21,920
I don't have any responsibility.

368
01:10:22,080 --> 01:10:26,480
Yes, if you...
If she becomes pregnant.

369
01:10:26,640 --> 01:10:29,480
Perhaps she is already?

370
01:10:30,480 --> 01:10:34,720
- No.
- Are you sure? Cause we heard...

371
01:10:35,720 --> 01:10:39,600
- Why is she here? Do you know?
- Yes, because she...

372
01:10:41,600 --> 01:10:47,280
- She needed a place to stay.
- You don't know what she wants.

373
01:10:49,960 --> 01:10:53,840
Thanks for visiting.
Make it short.

374
01:10:55,320 --> 01:10:58,400
- Where is she going...
- That's not your responsibility.

375
01:10:58,560 --> 01:11:03,600
It's for the best that she
goes back home.

376
01:11:04,840 --> 01:11:09,040
- They have resources.
- Who?

377
01:11:13,080 --> 01:11:15,280
The Lapps.

378
01:11:31,360 --> 01:11:35,920
I thought it was nice of
you to come.

379
01:11:37,880 --> 01:11:40,240
But...

380
01:12:05,960 --> 01:12:08,040
Sorry.

381
01:12:11,080 --> 01:12:13,120
That's what they're like.

382
01:12:13,280 --> 01:12:16,600
Can we still see each other?

383
01:12:19,160 --> 01:12:22,000
Tell them that I could work here.

384
01:12:23,880 --> 01:12:25,760
What?

385
01:12:26,760 --> 01:12:29,760
Tell them that I could work here.

386
01:12:34,160 --> 01:12:37,280
I could clean and so on.

387
01:15:32,920 --> 01:15:37,000
No pupils after the bells have chimed.

388
01:15:42,080 --> 01:15:44,040
Yes?

389
01:15:59,920 --> 01:16:03,960
- It's not the first time.
- No, it isn't.

390
01:16:04,120 --> 01:16:08,840
I don't have time for this.
What's her name?

391
01:16:10,400 --> 01:16:15,080
- What's your name?
- Christina Lajler.

392
01:16:16,120 --> 01:16:18,520
Christina Lajler.

393
01:16:29,360 --> 01:16:32,880
Girls, this is your new classmate.

394
01:16:50,680 --> 01:16:54,720
- My name is Christina Lajler.
- Yes.

395
01:18:16,480 --> 01:18:19,440
Five, six, seven, eight!

396
01:19:30,040 --> 01:19:32,480
Thank you. Stretch out.

397
01:19:36,040 --> 01:19:39,400
Have you not had PE before?

398
01:19:56,160 --> 01:19:59,520
Talk to Elsa.
Tell her to show you.

399
01:19:59,680 --> 01:20:02,280
Come back.

400
01:20:05,720 --> 01:20:08,080
Five, six, seven, eight!

401
01:20:19,560 --> 01:20:21,560
You?

402
01:20:22,760 --> 01:20:24,960
Come here.

403
01:20:44,440 --> 01:20:47,160
Are you using lipstick?

404
01:20:48,160 --> 01:20:51,000
Shall I apply some?

405
01:20:59,840 --> 01:21:02,240
Look up.

406
01:21:05,760 --> 01:21:07,560
And then...

407
01:21:15,800 --> 01:21:18,200
Then there's this.

408
01:21:25,080 --> 01:21:28,040
Have you lived in Germany?

409
01:21:30,320 --> 01:21:33,000
Ever been to Berlin?

410
01:21:45,520 --> 01:21:49,280
What do you need this for?

411
01:21:53,440 --> 01:21:57,400
What is it for?
Are you going after someone?

412
01:21:59,400 --> 01:22:03,760
What is it for?
Say something!

413
01:22:03,920 --> 01:22:08,320
If you're not using it,
I'll take it.

414
01:22:24,240 --> 01:22:27,400
Look at that dress. Look.

415
01:22:27,560 --> 01:22:32,120
Why does she insist on wearing
patterned clothes?

416
01:22:33,840 --> 01:22:35,840
It's hideous.

417
01:22:42,480 --> 01:22:45,400
Any plans for the weekend?

418
01:22:45,560 --> 01:22:49,360
- Wanna do something?
- Christina!

419
01:22:52,960 --> 01:22:55,360
The principal wants to talk with you.

420
01:23:07,280 --> 01:23:10,600
SEMESTER FEE

421
01:23:15,760 --> 01:23:21,080
200 KRONER FOR 2 SEMESTERS

422
01:23:54,600 --> 01:23:56,480
Niklas?

423
01:24:00,240 --> 01:24:02,720
There's a seat available here.

424
01:24:02,880 --> 01:24:06,280
Niklas? There's someone here to see you.

425
01:24:10,560 --> 01:24:12,560
Go on.

426
01:24:14,400 --> 01:24:17,800
Olof, I can't... Thank you.

427
01:24:21,360 --> 01:24:24,280
Do you want anything?

428
01:24:28,960 --> 01:24:31,360
Hello. Karin.

429
01:24:32,680 --> 01:24:34,680
Christina.

430
01:24:34,840 --> 01:24:39,320
Are you and Niklas old friends?

431
01:24:41,160 --> 01:24:43,600
Not that old.

432
01:24:48,560 --> 01:24:51,480
Where are you studying?

433
01:24:51,640 --> 01:24:55,320
In a regular school.

434
01:24:55,480 --> 01:24:59,800
Aren't you from Lappland?
The one Niklas met there?

435
01:25:04,280 --> 01:25:07,280
Which means you can joik... jodle...

436
01:25:07,440 --> 01:25:12,120
- Joik.
- For us.

437
01:25:14,120 --> 01:25:17,440
That would be great.

438
01:25:17,600 --> 01:25:20,640
- I've never heard it.
- Me neither.

439
01:25:20,800 --> 01:25:26,720
Ellen and I study antropology, so we're
doing some field work up there.

440
01:25:28,200 --> 01:25:33,160
Let it be a birthday present for Niklas.
A birthday joik.

441
01:25:34,200 --> 01:25:38,920
Niklas? You'd like a
birthday joik, wouldn't you?

442
01:25:41,040 --> 01:25:42,640
Yes...

443
01:25:46,240 --> 01:25:48,040
Yes.

444
01:25:48,200 --> 01:25:51,560
Turn the music off, will you?

445
01:25:51,720 --> 01:25:54,720
We're going to hear a birthday joik.

446
01:25:54,880 --> 01:25:57,440
- What is it for?
- Calling the animals.

447
01:25:57,600 --> 01:26:02,520
- No, it's a ritual.
- That would be great.

448
01:27:36,880 --> 01:27:39,000
Sorry.

449
01:27:44,080 --> 01:27:46,840
I thought it was nice.

450
01:27:47,000 --> 01:27:49,600
Perhaps a bit long.

451
01:27:53,840 --> 01:27:57,920
I need to borrow some money.

452
01:27:58,080 --> 01:28:02,280
I'm not pregnant.
It's for the school.

453
01:28:10,360 --> 01:28:14,240
Why don't you ask your
own parents?

454
01:28:14,400 --> 01:28:16,600
They're dead.

455
01:28:19,760 --> 01:28:21,760
They are?

456
01:28:27,440 --> 01:28:29,800
They are?

457
01:28:29,960 --> 01:28:34,640
My dad is.
Mom owns reindeer.

458
01:28:35,520 --> 01:28:38,080
But your name is Christina?

459
01:28:46,720 --> 01:28:49,080
Elle-Marja.

460
01:28:55,200 --> 01:28:58,240
Niklas, come here for a second.

461
01:31:02,640 --> 01:31:05,360
This is for you.

462
01:31:42,840 --> 01:31:47,240
- Come on.
- Not in melted water.

463
01:31:47,400 --> 01:31:53,080
- You're smelly.
- I can't smell anything.

464
01:31:54,120 --> 01:31:57,640
You can smell it yourself.

465
01:32:03,000 --> 01:32:05,000
Come on.

466
01:32:19,640 --> 01:32:23,560
Hold onto me.
Can I show you something?

467
01:32:23,720 --> 01:32:26,520
Lay down.

468
01:32:26,680 --> 01:32:28,720
Slowly.

469
01:32:46,920 --> 01:32:50,280
Lay down. Slowly.

470
01:32:51,400 --> 01:32:54,280
It's just like flying, isn't it?

471
01:32:58,240 --> 01:33:00,800
Isn't it lovely?

472
01:34:26,440 --> 01:34:28,440
Grandma.

473
01:34:34,880 --> 01:34:38,360
What am I supposed to do
with that much cream?

474
01:34:40,840 --> 01:34:43,240
And for what?

475
01:34:48,040 --> 01:34:49,880
Mom?

476
01:34:52,080 --> 01:34:54,600
Can we talk?

477
01:34:54,760 --> 01:34:57,720
Go ahead.

478
01:34:57,880 --> 01:35:02,440
- Can we go somewhere?
- Why can't we talk here?

479
01:35:02,600 --> 01:35:05,960
Or should we build
a nicer house?

480
01:35:09,080 --> 01:35:11,080
Read!

481
01:35:12,880 --> 01:35:15,040
Read!

482
01:35:22,400 --> 01:35:25,280
That's not your name.

483
01:35:27,920 --> 01:35:30,960
I'm not Elle-Marja anymore.

484
01:35:47,040 --> 01:35:51,520
I would like to sell all my reindeer
and those my father left me.

485
01:35:53,200 --> 01:35:56,760
We don't have the money, Elle-Marja.

486
01:35:56,920 --> 01:36:00,360
- We have daddy's silver belt.
- No.

487
01:36:01,120 --> 01:36:04,240
We could sell it to someone else.

488
01:36:12,000 --> 01:36:15,280
Watch out so you don't become Swedish.

489
01:36:17,640 --> 01:36:22,240
I'm not just sending you away.
Who's gonna watch out for your reindeer?

490
01:36:22,400 --> 01:36:25,640
I don't want to live with you.

491
01:36:25,800 --> 01:36:29,960
- I don't want to be a circus animal.
- Get out.

492
01:36:35,000 --> 01:36:37,000
Get out.

493
01:38:17,560 --> 01:38:19,760
She's over there.

494
01:41:20,200 --> 01:41:22,880
Forgive me.

