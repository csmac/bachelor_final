﻿1
00:00:07,180 --> 00:00:08,180
(Episode 99)

2
00:00:15,410 --> 00:00:16,680
How do others...

3
00:00:17,010 --> 00:00:19,380
go to their mums when they're in trouble?

4
00:00:20,310 --> 00:00:23,210
I can't even go in there because I don't want to worry Mum.

5
00:00:26,940 --> 00:00:28,340
What are you doing here?

6
00:00:30,010 --> 00:00:31,040
Mother.

7
00:00:32,010 --> 00:00:34,180
What are you doing here at this hour?

8
00:00:36,450 --> 00:00:37,680
Well...

9
00:00:39,310 --> 00:00:41,310
Did you have a fight with Jae Min?

10
00:00:43,480 --> 00:00:44,480
Yes.

11
00:00:45,680 --> 00:00:46,780
What about the kids?

12
00:00:47,510 --> 00:00:49,840
Jae Min is with them.

13
00:00:50,480 --> 00:00:52,340
What are you thinking?

14
00:00:52,940 --> 00:00:54,410
Mother, please.

15
00:00:54,680 --> 00:00:56,840
No matter how upset you are,

16
00:00:56,940 --> 00:00:59,810
you can't leave kids behind at this hour.

17
00:00:59,910 --> 00:01:02,140
I'll explain at home.

18
00:01:02,780 --> 00:01:04,840
- Mum will hear us. - What?

19
00:01:05,110 --> 00:01:06,980
She should know you're doing this.

20
00:01:07,080 --> 00:01:09,510
Mother, please.

21
00:01:09,610 --> 00:01:10,650
Oh, my.

22
00:01:11,180 --> 00:01:12,410
It's you, Mi So.

23
00:01:13,010 --> 00:01:14,480
Why are you here at this hour?

24
00:01:16,040 --> 00:01:17,080
Mum.

25
00:01:17,580 --> 00:01:19,480
- Yes? - Ms Ok.

26
00:01:19,810 --> 00:01:22,110
She came here after a fight with Jae Min.

27
00:01:22,810 --> 00:01:23,810
What?

28
00:01:23,840 --> 00:01:27,280
I couldn't go in because I didn't want to worry you.

29
00:01:27,580 --> 00:01:28,680
Come on in.

30
00:01:50,940 --> 00:01:53,710
What are you doing? Sit down.

31
00:01:54,940 --> 00:01:56,040
Okay.

32
00:01:59,080 --> 00:02:00,740
Well, tell us.

33
00:02:01,010 --> 00:02:02,110
What happened?

34
00:02:02,410 --> 00:02:05,480
We just argued about something trivial.

35
00:02:05,640 --> 00:02:08,910
Then why did you have to come here leaving your kids behind?

36
00:02:09,110 --> 00:02:12,340
She didn't bring anything. She doesn't mean to stay out long.

37
00:02:12,510 --> 00:02:15,680
I was so upset, so I came to see Mum.

38
00:02:15,810 --> 00:02:17,680
You should have come in right away.

39
00:02:17,810 --> 00:02:19,280
What were you doing outside?

40
00:02:20,380 --> 00:02:22,280
I didn't want to worry you.

41
00:02:22,480 --> 00:02:25,980
When your husband makes you upset, you should go to your mum.

42
00:02:26,080 --> 00:02:28,380
That's what everyone does.

43
00:02:29,010 --> 00:02:31,210
Are you telling her to talk behind my son's back?

44
00:02:31,510 --> 00:02:34,380
She should when he makes her upset, no matter how nice he usually is.

45
00:02:34,480 --> 00:02:35,880
Hey.

46
00:02:36,310 --> 00:02:37,880
You can't do this.

47
00:02:38,080 --> 00:02:40,580
Your daughter left home in the middle of the night.

48
00:02:40,710 --> 00:02:43,140
You should scold her and send her back home.

49
00:02:43,240 --> 00:02:44,880
She must have been really upset...

50
00:02:45,310 --> 00:02:48,080
if she left her kids behind at this hour.

51
00:02:48,180 --> 00:02:50,640
Are you saying it's okay to do this?

52
00:02:50,910 --> 00:02:52,940
That's not what I mean.

53
00:02:53,140 --> 00:02:56,510
Mi So just needs someone to talk to.

54
00:02:56,810 --> 00:02:59,710
Sleep here tonight and go home tomorrow morning.

55
00:02:59,810 --> 00:03:01,240
Ms Ok.

56
00:03:01,380 --> 00:03:02,780
Please let it pass this time.

57
00:03:03,280 --> 00:03:05,540
This is what mums are for.

58
00:03:05,980 --> 00:03:07,910
Fine. I get it.

59
00:03:08,580 --> 00:03:11,180
Have a great time, you two.

60
00:03:11,280 --> 00:03:12,810
A stranger should go home.

61
00:03:13,040 --> 00:03:15,410
I'll send her home tomorrow.

62
00:03:16,110 --> 00:03:17,740
Wait. Mother.

63
00:03:18,040 --> 00:03:19,440
I'll be right back.

64
00:03:19,710 --> 00:03:20,840
Mother.

65
00:03:27,810 --> 00:03:28,810
Mother.

66
00:03:29,440 --> 00:03:30,740
Mum.

67
00:03:31,040 --> 00:03:33,340
You can't leave like this.

68
00:03:33,610 --> 00:03:36,080
Why not? You two can have fun.

69
00:03:37,410 --> 00:03:38,410
I'm sorry.

70
00:03:39,280 --> 00:03:40,510
I was wrong.

71
00:03:40,940 --> 00:03:43,910
No matter how good Jae Min is at babysitting,

72
00:03:44,110 --> 00:03:46,010
you still shouldn't sleep out.

73
00:03:46,210 --> 00:03:49,180
I thought we'd have a huge fight if I was with him tonight.

74
00:03:49,740 --> 00:03:51,110
That's why I left.

75
00:03:51,680 --> 00:03:53,110
What is it?

76
00:03:55,210 --> 00:03:57,610
Did he do something at work?

77
00:03:58,440 --> 00:03:59,680
No.

78
00:03:59,880 --> 00:04:03,880
If not, what could possibly make you leave your kids and sleep out?

79
00:04:04,680 --> 00:04:07,110
I'll go home after tonight.

80
00:04:07,410 --> 00:04:08,940
He did do something, didn't he?

81
00:04:09,440 --> 00:04:10,710
You need to tell me.

82
00:04:12,580 --> 00:04:14,480
Let's go. I'll see you off.

83
00:04:16,310 --> 00:04:18,080
You can't leave your mum alone.

84
00:04:18,740 --> 00:04:19,740
Go on.

85
00:04:30,210 --> 00:04:32,810
Is Mi So over there?

86
00:04:32,940 --> 00:04:33,940
Yes.

87
00:04:34,240 --> 00:04:36,740
I'll take care of her tonight.

88
00:04:36,910 --> 00:04:39,810
Make sure you take care of your children.

89
00:04:40,240 --> 00:04:42,240
Okay. I'm sorry, Mother.

90
00:04:42,350 --> 00:04:44,880
Mi So's here. Goodbye.

91
00:04:50,180 --> 00:04:53,410
I completely forgot that Mi So found her mum.

92
00:04:55,580 --> 00:04:56,780
Dad.

93
00:04:58,180 --> 00:05:00,350
Did Mum go to Grandma's house?

94
00:05:00,980 --> 00:05:01,980
Yes.

95
00:05:02,280 --> 00:05:03,940
She's sleeping over tonight.

96
00:05:04,780 --> 00:05:05,940
That's a relief.

97
00:05:06,980 --> 00:05:07,980
Yes.

98
00:05:11,850 --> 00:05:14,380
Dad, whatever you did, you were very wrong.

99
00:05:15,610 --> 00:05:16,610
Why?

100
00:05:16,610 --> 00:05:19,780
You told Mum, "You have nowhere to go."

101
00:05:21,410 --> 00:05:22,710
Did you hear that?

102
00:05:22,810 --> 00:05:25,680
She probably wanted to leave more because she heard that.

103
00:05:36,680 --> 00:05:39,040
Oh, my. Leave it. I can do it myself.

104
00:05:39,140 --> 00:05:42,180
Just lie down. You must be tired.

105
00:05:45,110 --> 00:05:46,980
This is very comfortable, Mum.

106
00:05:47,110 --> 00:05:49,080
It's just an old shirt.

107
00:05:49,640 --> 00:05:52,680
I should get some casual clothes for you to wear.

108
00:05:52,840 --> 00:05:54,910
I can bring some from home next time.

109
00:05:55,480 --> 00:05:56,580
You know what?

110
00:05:57,780 --> 00:05:59,940
This smells like you. I love it.

111
00:06:03,140 --> 00:06:05,510
Were you surprised to see me?

112
00:06:05,740 --> 00:06:08,380
Why did you fight with Jae Min?

113
00:06:09,380 --> 00:06:10,810
Jae Min is...

114
00:06:11,840 --> 00:06:13,480
too righteous.

115
00:06:13,840 --> 00:06:17,110
Sometimes, that's too hard for me.

116
00:06:18,180 --> 00:06:19,780
No one would be happy...

117
00:06:19,880 --> 00:06:23,280
if he keeps picking on things he can overlook.

118
00:06:25,380 --> 00:06:28,410
He's just like your father.

119
00:06:29,510 --> 00:06:30,740
Was Dad like that?

120
00:06:30,880 --> 00:06:32,510
When he'd see a fight,

121
00:06:32,840 --> 00:06:34,640
he'd walk in to break it off.

122
00:06:35,040 --> 00:06:37,380
He'd also scold at impolite kids.

123
00:06:38,610 --> 00:06:41,610
I told him to not step in, but he'd never listen.

124
00:06:43,640 --> 00:06:46,580
I'm so glad you know how I feel. How should I put this?

125
00:06:47,210 --> 00:06:49,180
I feel very relieved.

126
00:06:50,980 --> 00:06:52,080
I am...

127
00:06:52,710 --> 00:06:54,280
always on your side.

128
00:06:55,080 --> 00:06:57,340
Even if you did wrong, I'll be on your side.

129
00:06:57,910 --> 00:06:58,910
So...

130
00:06:59,310 --> 00:07:01,980
if something bothers you, don't worry about it...

131
00:07:02,340 --> 00:07:03,840
and just come to me.

132
00:07:06,180 --> 00:07:07,740
So this is what it's like to have a mum.

133
00:07:09,080 --> 00:07:12,110
But you still shouldn't sleep out.

134
00:07:12,310 --> 00:07:14,440
You should stay home even if you two fight.

135
00:07:14,540 --> 00:07:16,310
You shouldn't walk out like this.

136
00:07:17,010 --> 00:07:18,680
This is just for tonight, okay?

137
00:07:20,710 --> 00:07:21,710
Okay.

138
00:07:24,510 --> 00:07:25,610
Let's go to sleep.

139
00:07:28,340 --> 00:07:30,280
What brings you here this late?

140
00:07:30,710 --> 00:07:33,340
Mi So is at her mum's house.

141
00:07:33,940 --> 00:07:35,040
How did you know?

142
00:07:35,410 --> 00:07:36,840
So you already knew?

143
00:07:37,780 --> 00:07:39,040
Mother called.

144
00:07:40,010 --> 00:07:41,440
Why did you two fight?

145
00:07:43,040 --> 00:07:46,240
What did you do wrong to make her leave her kids and walk out?

146
00:07:46,440 --> 00:07:47,810
It's nothing.

147
00:07:47,940 --> 00:07:49,040
Did you...

148
00:07:49,310 --> 00:07:50,740
do something at work?

149
00:07:50,910 --> 00:07:52,040
I didn't.

150
00:07:52,710 --> 00:07:56,240
I shouldn't have asked. I knew you wouldn't tell.

151
00:07:56,540 --> 00:07:59,610
You never start talking once you make up your mind.

152
00:07:59,840 --> 00:08:01,280
Yes. You shouldn't have come.

153
00:08:01,380 --> 00:08:03,410
Don't worry and go home and get some rest.

154
00:08:03,510 --> 00:08:04,940
I'm going to sleep over.

155
00:08:05,110 --> 00:08:06,310
Why?

156
00:08:06,440 --> 00:08:08,710
I will fix whatever you did wrong.

157
00:08:08,810 --> 00:08:10,980
There is nothing to fix here.

158
00:08:11,910 --> 00:08:14,210
Let's wait and see.

159
00:08:14,740 --> 00:08:17,080
I will find out what this is about.

160
00:08:23,740 --> 00:08:25,840
What's she doing? Is she a detective?

161
00:08:27,210 --> 00:08:29,110
This is driving me crazy.

162
00:08:50,580 --> 00:08:53,380
Ga Ram, you're sound asleep.

163
00:09:17,410 --> 00:09:18,410
Mum.

164
00:09:19,010 --> 00:09:21,510
Bang Geul. You're up so early.

165
00:09:21,780 --> 00:09:24,980
I was going to wake Dad up because you weren't home.

166
00:09:26,010 --> 00:09:27,010
Come here.

167
00:09:29,240 --> 00:09:30,950
I'm sorry, Bang Geul.

168
00:09:31,180 --> 00:09:34,380
I was feeling awful yesterday.

169
00:09:34,610 --> 00:09:36,480
So I slept over at Grandma's.

170
00:09:36,910 --> 00:09:38,140
What about us?

171
00:09:41,980 --> 00:09:44,480
My other grandma came over last night.

172
00:09:44,880 --> 00:09:46,950
She's sleeping in my room.

173
00:09:47,380 --> 00:09:48,380
Really?

174
00:09:49,950 --> 00:09:52,340
Mum, are you staying out tonight, too?

175
00:09:53,450 --> 00:09:55,740
No. I'm coming back tonight.

176
00:09:56,680 --> 00:09:58,280
Bang Geul, I'm going to work now.

177
00:09:58,710 --> 00:09:59,710
Okay.

178
00:09:59,980 --> 00:10:01,910
Smile today!

179
00:10:02,280 --> 00:10:04,110
Yes. Smile.

180
00:10:04,950 --> 00:10:06,240
Goodbye.

181
00:10:19,310 --> 00:10:20,840
- Honey. - Yes?

182
00:10:21,880 --> 00:10:25,280
Do you really not know why Jae Min and Mi So got into a fight?

183
00:10:25,840 --> 00:10:27,510
How would I know?

184
00:10:27,780 --> 00:10:30,480
Do you have something to do with Mr Oh, too?

185
00:10:31,080 --> 00:10:33,580
- Why do you ask? - Just because.

186
00:10:34,210 --> 00:10:35,910
You made a deal with Mr Oh...

187
00:10:36,480 --> 00:10:38,010
about my front money.

188
00:10:39,740 --> 00:10:41,910
I'm not at a place to have something with him.

189
00:10:41,980 --> 00:10:43,080
In my opinion,

190
00:10:43,680 --> 00:10:47,210
Mr Oh is at a completely different level than Dung Kim.

191
00:10:47,710 --> 00:10:48,950
You should be careful.

192
00:10:50,710 --> 00:10:52,910
Dad. I'm hungry.

193
00:10:53,610 --> 00:10:56,410
Are you? Wash up first. I'll bring your breakfast.

194
00:10:56,540 --> 00:10:57,540
Okay.

195
00:10:59,180 --> 00:11:01,310
Did Min Ho start studying Korean?

196
00:11:01,410 --> 00:11:02,940
Don't worry. I'm teaching him.

197
00:11:03,080 --> 00:11:05,510
I should've been more concerned about the basics.

198
00:11:05,840 --> 00:11:09,240
What good is prior learning if he doesn't know Korean well?

199
00:11:09,440 --> 00:11:11,910
How can he solve the difficult analytical problems?

200
00:11:12,010 --> 00:11:15,140
Many people do not realise that. They only want to push forward.

201
00:11:15,340 --> 00:11:16,810
No one looks back.

202
00:11:28,710 --> 00:11:30,710
Eun Sol, eat up.

203
00:11:35,410 --> 00:11:37,480
Is Mum angry, Dad?

204
00:11:38,810 --> 00:11:41,340
No. Let's eat, Eun Sol.

205
00:11:53,940 --> 00:11:55,180
Seung Ju's mum.

206
00:11:55,710 --> 00:11:59,040
Yes. Do you know where Da Woon's house is?

207
00:12:01,580 --> 00:12:04,440
Do you know anyone who knows?

208
00:12:06,080 --> 00:12:09,240
That's good news. Please find out where it is for me.

209
00:12:09,910 --> 00:12:10,910
Yes.

210
00:12:12,910 --> 00:12:14,310
Why do you ask?

211
00:12:15,610 --> 00:12:18,310
What will you do? Will you kneel down and apologise?

212
00:12:20,810 --> 00:12:22,480
That's how things were back then.

213
00:12:23,110 --> 00:12:26,410
There wasn't a hospital that gave maternity leave to nurses.

214
00:12:28,540 --> 00:12:29,640
What about now?

215
00:12:30,140 --> 00:12:33,440
Can people use maternity and a childcare leave?

216
00:12:34,180 --> 00:12:35,580
Can our nurses do that?

217
00:12:39,380 --> 00:12:41,110
Don't blame the society.

218
00:12:42,380 --> 00:12:44,380
This was your decision.

219
00:12:49,240 --> 00:12:50,240
When...

220
00:12:50,740 --> 00:12:53,010
Bang Geul's mum went through a similar incident,

221
00:12:53,710 --> 00:12:55,610
I couldn't imagine...

222
00:12:56,210 --> 00:12:57,880
that people could be that awful.

223
00:12:58,310 --> 00:13:02,580
I didn't realise my own husband was doing exactly the same thing.

224
00:13:03,410 --> 00:13:06,740
You only pretend to be a gentleman to people of power.

225
00:13:07,880 --> 00:13:08,880
You're...

226
00:13:09,480 --> 00:13:11,210
a terrible person.

227
00:13:23,880 --> 00:13:25,110
- You're here. - Yes.

228
00:13:25,480 --> 00:13:26,640
Where's Ga Ram?

229
00:13:27,510 --> 00:13:30,240
- My mum is with him. - Is she here?

230
00:13:31,410 --> 00:13:32,580
How's Mi So?

231
00:13:33,740 --> 00:13:36,740
She slept at her mother's place last night.

232
00:13:38,180 --> 00:13:39,180
I see.

233
00:13:40,610 --> 00:13:41,610
Well...

234
00:13:42,310 --> 00:13:44,480
I ran into Mi So yesterday.

235
00:13:45,340 --> 00:13:48,140
She asked me to tell her...

236
00:13:48,580 --> 00:13:50,640
if you ever tell me anything about Mr Oh.

237
00:13:53,340 --> 00:13:56,740
Do you mind if I ask what's going on?

238
00:13:57,940 --> 00:14:00,710
I'll tell you when I need your help.

239
00:14:01,340 --> 00:14:03,810
Bang Geul, your uncle is here. It's time for school.

240
00:14:07,940 --> 00:14:09,680
Oh, you're here.

241
00:14:09,880 --> 00:14:12,110
- Hello. - Let's go.

242
00:14:13,480 --> 00:14:15,710
- Where? - What do you think?

243
00:14:15,810 --> 00:14:17,880
I'm going to shared babysitting.

244
00:14:18,310 --> 00:14:19,480
Why?

245
00:14:19,540 --> 00:14:22,480
You babysit my grandkids. I should help out.

246
00:14:22,740 --> 00:14:25,140
Are you sure you have no other intentions?

247
00:14:27,780 --> 00:14:30,510
You guessed it right.

248
00:14:30,610 --> 00:14:31,610
Let's go, Bang Geul.

249
00:14:31,840 --> 00:14:33,280
Where's Min Ho?

250
00:14:33,380 --> 00:14:34,840
He's waiting in the lobby.

251
00:14:35,080 --> 00:14:36,080
Let's go.

252
00:14:40,510 --> 00:14:41,510
Let's go.

253
00:14:46,540 --> 00:14:47,710
Where's Ms Lee?

254
00:14:48,940 --> 00:14:51,710
She's at her mother's place.

255
00:14:52,510 --> 00:14:53,510
Why?

256
00:14:53,610 --> 00:14:55,280
She wanted to sleep at her mother's place.

257
00:14:55,780 --> 00:14:56,840
Get in.

258
00:15:01,110 --> 00:15:02,740
They had a fight because of me.

259
00:15:06,640 --> 00:15:09,980
I'm really okay. My goodness.

260
00:15:10,080 --> 00:15:11,880
Have some. It'll be tasty.

261
00:15:12,940 --> 00:15:14,580
You really didn't have to.

262
00:15:14,680 --> 00:15:17,180
Try it. It'll taste amazing.

263
00:15:17,610 --> 00:15:22,080
Gosh. I'm not hungry at all.

264
00:15:24,010 --> 00:15:25,810
Do you have any soup?

265
00:15:25,910 --> 00:15:28,240
I'm afraid I don't.

266
00:15:28,480 --> 00:15:30,340
It's boring to have only rice.

267
00:15:30,680 --> 00:15:33,210
Can't you whip up some egg soup?

268
00:15:33,310 --> 00:15:34,680
Egg soup? Sure.

269
00:15:35,280 --> 00:15:36,580
Let's see.

270
00:15:36,740 --> 00:15:37,740
Ms Lee.

271
00:15:38,340 --> 00:15:41,110
We don't have any kids here in the morning.

272
00:15:41,210 --> 00:15:42,710
You can go home and...

273
00:15:42,810 --> 00:15:43,940
Why did...

274
00:15:44,810 --> 00:15:46,640
- they fight? - Sorry?

275
00:15:46,880 --> 00:15:48,040
You don't know?

276
00:15:48,240 --> 00:15:49,940
I have no idea.

277
00:15:51,110 --> 00:15:54,010
Then can you try and ask Mi So about it?

278
00:15:54,840 --> 00:15:57,740
She won't tell me anything.

279
00:15:58,010 --> 00:16:00,840
Jae Min must have gotten in serious trouble at work.

280
00:16:01,640 --> 00:16:04,640
I see. I'll ask Mi So when I meet her.

281
00:16:21,110 --> 00:16:22,340
Have you had lunch yet?

282
00:16:23,180 --> 00:16:26,240
I've been at our stores until now. Can we eat together?

283
00:16:28,740 --> 00:16:30,210
I'm sorry.

284
00:16:31,140 --> 00:16:33,040
You must have been hurt when I said...

285
00:16:33,740 --> 00:16:35,080
you had nowhere to go.

286
00:16:36,540 --> 00:16:38,010
I was just joking.

287
00:16:38,810 --> 00:16:40,680
I got in trouble with Bang Geul for that, too.

288
00:16:41,840 --> 00:16:42,940
Mr Oh...

289
00:16:43,080 --> 00:16:45,980
How did your business trip go, Mr Oh?

290
00:16:47,540 --> 00:16:48,610
What should I do?

291
00:16:48,780 --> 00:16:50,440
I couldn't attend the disciplinary committee...

292
00:16:50,610 --> 00:16:52,540
because I suddenly had to go on a business trip.

293
00:16:53,210 --> 00:16:55,580
I heard the penalty turned out to be quite harsh.

294
00:16:56,340 --> 00:16:57,340
Yes.

295
00:16:57,440 --> 00:16:59,740
He regrets it very much.

296
00:17:00,310 --> 00:17:02,740
Even though you supported his idea,

297
00:17:02,980 --> 00:17:05,510
he should have informed you before carrying it out.

298
00:17:07,540 --> 00:17:09,110
You did the interview...

299
00:17:09,410 --> 00:17:11,810
to support him, right?

300
00:17:12,480 --> 00:17:13,880
I appreciate it.

301
00:17:14,310 --> 00:17:16,110
I'm glad you understand me.

302
00:17:16,810 --> 00:17:18,940
I was worried that Mr Kim would get me wrong.

303
00:17:20,340 --> 00:17:21,480
No way.

304
00:17:21,780 --> 00:17:26,180
We decided to go along with everything you say from now on.

305
00:17:26,610 --> 00:17:27,840
Don't worry.

306
00:17:37,910 --> 00:17:39,040
I hope...

307
00:17:39,910 --> 00:17:41,380
this will do for now.

308
00:17:43,980 --> 00:17:45,210
I'm sorry, Mi So.

309
00:17:46,340 --> 00:17:48,380
You had to apologise to Mr Oh for me.

310
00:17:52,880 --> 00:17:54,710
Please stop being mad at me.

311
00:17:55,180 --> 00:17:56,710
Then tell me the truth.

312
00:17:57,540 --> 00:18:00,580
What's Mr Oh's secret that I don't know about?

313
00:18:03,310 --> 00:18:04,740
You don't have to tell me.

314
00:18:05,480 --> 00:18:06,650
Mum said...

315
00:18:07,040 --> 00:18:09,940
I shouldn't keep it to myself and should always turn to her.

316
00:18:10,810 --> 00:18:12,080
I'm not going home tonight, either.

317
00:18:14,210 --> 00:18:15,240
Mi So.

318
00:18:32,440 --> 00:18:34,110
How can he just leave like that?

319
00:18:36,480 --> 00:18:38,510
I have to go home today.

320
00:18:48,810 --> 00:18:50,040
How did it go?

321
00:18:51,150 --> 00:18:53,210
The executives of Youngjin Industry...

322
00:18:54,150 --> 00:18:56,710
complained a lot about their president.

323
00:18:57,540 --> 00:18:59,880
They blamed him...

324
00:19:00,210 --> 00:19:02,040
for the company's bankruptcy.

325
00:19:02,480 --> 00:19:05,150
Is he really...

326
00:19:05,340 --> 00:19:07,310
registered at SS Global?

327
00:19:07,410 --> 00:19:08,480
I'm sure of it.

328
00:19:10,080 --> 00:19:12,080
Once we get our hands on the employee list, we'll find out.

329
00:19:12,180 --> 00:19:15,580
Even if we find his name on the list,

330
00:19:16,010 --> 00:19:18,240
how are you going to prove his relation to Mr Oh?

331
00:19:18,480 --> 00:19:20,440
I think I should meet Mr Park...

332
00:19:20,680 --> 00:19:22,650
who's in charge of SS Global.

333
00:19:23,340 --> 00:19:25,280
I should ask him about the president of Youngjin Industry.

334
00:19:25,540 --> 00:19:30,740
Jae Min. Why do you care so much about this matter?

335
00:19:30,840 --> 00:19:32,780
That's what I'm talking about.

336
00:19:34,180 --> 00:19:37,810
Hey. Haven't you guys talked it out yet?

337
00:19:38,240 --> 00:19:39,710
Talk it out?

338
00:19:40,380 --> 00:19:43,810
In Mr Kim's eyes, I'm just a fly.

339
00:19:44,040 --> 00:19:46,650
Would you have a conversation with a fly?

340
00:19:48,040 --> 00:19:50,080
I'm not going home for the time being.

341
00:19:50,280 --> 00:19:52,840
Please tell Mr Kim to take good care of the kids.

342
00:19:56,010 --> 00:19:58,440
Hey. Why is she so upset?

343
00:19:58,980 --> 00:20:01,580
She usually supports whatever you do.

344
00:20:01,580 --> 00:20:02,840
The thing is...

345
00:20:07,310 --> 00:20:08,680
It's driving me crazy.

346
00:20:09,540 --> 00:20:11,840
My goodness. You sort it out.

347
00:20:31,310 --> 00:20:32,310
Hi, Il Mok.

348
00:20:32,310 --> 00:20:33,440
(We're looking for missing kids.)

349
00:20:34,150 --> 00:20:36,540
We finally received some information about the missing kids.

350
00:20:36,910 --> 00:20:38,040
- Really? - Yes.

351
00:20:38,040 --> 00:20:39,480
Someone left a comment on my blog.

352
00:20:39,710 --> 00:20:41,980
She says the kid looks a lot like a kid at an orphanage in Gumi.

353
00:20:42,280 --> 00:20:43,580
That's great.

354
00:20:43,810 --> 00:20:46,240
I called the kid's parents. They're on their way to Gumi.

355
00:20:46,440 --> 00:20:47,580
My gosh.

356
00:20:48,210 --> 00:20:49,780
I hope it's the right kid.

357
00:20:50,340 --> 00:20:52,480
I really hope so.

358
00:20:52,650 --> 00:20:54,880
So do I. I'll keep you updated.

359
00:20:55,180 --> 00:20:56,180
Okay.

360
00:20:59,280 --> 00:21:00,440
What are you doing now?

361
00:21:00,880 --> 00:21:04,540
I'm telling my neighbours about the new information of the kid.

362
00:21:04,740 --> 00:21:06,240
When are you going to call Mi So?

363
00:21:06,780 --> 00:21:08,280
A bit later.

364
00:21:08,480 --> 00:21:09,840
Do it right now.

365
00:21:10,380 --> 00:21:14,650
Otherwise, I'll eat three meals here every day.

366
00:21:15,080 --> 00:21:17,040
I'll text her right now.

367
00:21:17,910 --> 00:21:19,240
Let's see.

368
00:21:26,410 --> 00:21:27,880
I have something to tell you.

369
00:21:28,180 --> 00:21:29,710
Meet me at the fried chicken place after work.

370
00:21:31,880 --> 00:21:33,940
Why does he want to meet me alone?

371
00:21:34,840 --> 00:21:36,040
(Meeting Room)

372
00:21:36,150 --> 00:21:37,310
Have you lost your mind?

373
00:21:40,310 --> 00:21:41,540
You want what?

374
00:21:42,040 --> 00:21:44,810
I'd like to take paternity leave.

375
00:21:46,440 --> 00:21:48,780
Do you have to copy everything others do?

376
00:21:49,340 --> 00:21:53,110
Do you want to take paternity leave because Jae Min did?

377
00:21:53,310 --> 00:21:55,140
I have no one to babysit my kids.

378
00:21:55,280 --> 00:21:58,280
Isn't it that you want to get paid while slacking at home?

379
00:21:58,540 --> 00:22:01,380
I barely managed to find a neighbour who can babysit today.

380
00:22:01,810 --> 00:22:03,340
If you persuade Mr Oh...

381
00:22:03,440 --> 00:22:06,540
This is why I was against Jae Min taking paternity leave.

382
00:22:06,780 --> 00:22:08,280
I knew there would be copycats.

383
00:22:08,380 --> 00:22:09,840
Copycats?

384
00:22:10,940 --> 00:22:13,240
Taking paternity leave is not a crime.

385
00:22:13,340 --> 00:22:16,040
You sound just like Jae Min.

386
00:22:17,080 --> 00:22:19,240
After criticising him so much,

387
00:22:19,410 --> 00:22:22,040
why do you try to copy him?

388
00:22:34,780 --> 00:22:37,080
We haven't found any kids yet,

389
00:22:37,310 --> 00:22:39,910
but please keep supporting us.

390
00:22:42,840 --> 00:22:44,940
I'll call you back.

391
00:22:47,940 --> 00:22:50,110
- Mi So. - Mr Kim.

392
00:22:50,410 --> 00:22:53,280
Mr Choi is in the meeting room right now.

393
00:22:53,440 --> 00:22:55,940
- Why? - His wife got in a car accident.

394
00:22:56,040 --> 00:22:58,840
He told Dung Kim that he wanted paternity leave.

395
00:22:59,040 --> 00:23:01,010
He got turned down right away.

396
00:23:01,380 --> 00:23:03,240
- What? - Shouldn't you...

397
00:23:03,680 --> 00:23:05,710
try to help him?

398
00:23:06,640 --> 00:23:07,840
I should.

399
00:23:10,480 --> 00:23:12,280
Stop being so angry, Mi So.

400
00:23:12,740 --> 00:23:14,310
Don't worry about me.

401
00:23:14,510 --> 00:23:16,440
Good luck with finding Mr Oh's fault.

402
00:23:22,040 --> 00:23:25,040
What should we do? She still seems very angry.

403
00:23:25,540 --> 00:23:28,240
What can I do? I need to make it right.

404
00:23:28,810 --> 00:23:31,240
You should just tell her that it's because of me.

405
00:23:31,510 --> 00:23:32,810
Never.

406
00:23:33,440 --> 00:23:36,810
If she finds out that you used Mother's name,

407
00:23:37,510 --> 00:23:39,410
she'll never see you again.

408
00:23:46,240 --> 00:23:48,480
Everyone is in a mess because of me.

409
00:23:50,910 --> 00:23:53,180
How long will I hide behind Mr Kim?

410
00:23:53,580 --> 00:23:55,240
I need to step up.

411
00:24:06,580 --> 00:24:07,780
I heard the story.

412
00:24:08,740 --> 00:24:10,280
You want to take paternity leave.

413
00:24:10,780 --> 00:24:11,940
Who says so?

414
00:24:13,910 --> 00:24:15,480
Your wife is ill.

415
00:24:15,880 --> 00:24:18,240
Of course you should take care of your children.

416
00:24:18,510 --> 00:24:20,240
Mind your own business.

417
00:24:22,140 --> 00:24:23,940
Like I said,

418
00:24:24,640 --> 00:24:27,810
I am going to help anyone who wants to take paternity leave.

419
00:24:29,510 --> 00:24:33,310
Think of your children. You need to make an effort.

420
00:24:33,740 --> 00:24:34,980
Paternity leave is...

421
00:24:35,340 --> 00:24:37,780
a right which every single employee has.

422
00:24:38,180 --> 00:24:40,940
You should apply for it at the human resources team.

423
00:24:41,040 --> 00:24:43,980
No, thanks. I do not want to be put to shame again.

424
00:24:44,540 --> 00:24:46,310
I'd rather quit.

425
00:25:00,640 --> 00:25:01,640
Mum.

426
00:25:01,780 --> 00:25:03,680
Are you going back home tonight?

427
00:25:04,840 --> 00:25:06,440
You can use me as an excuse.

428
00:25:06,740 --> 00:25:08,810
I'll be at home with the kids.

429
00:25:10,180 --> 00:25:11,180
Okay.

430
00:25:14,010 --> 00:25:15,110
Thank goodness.

431
00:25:15,710 --> 00:25:17,440
I didn't have a good excuse.

432
00:25:26,540 --> 00:25:27,580
What is it?

433
00:25:27,980 --> 00:25:29,610
I have something to discuss.

434
00:25:30,580 --> 00:25:32,840
To discuss? About what?

435
00:25:36,880 --> 00:25:37,980
I want to...

436
00:25:38,540 --> 00:25:41,640
tell everyone about this situation.

437
00:25:44,140 --> 00:25:46,610
Tell everyone? How will you do that?

438
00:25:46,710 --> 00:25:49,140
I will become a whistle-blower.

439
00:25:50,080 --> 00:25:52,410
I want to tell them what I have done wrong...

440
00:25:52,680 --> 00:25:55,280
and get Dung Kim and Mr Oh while I'm at it.

441
00:25:55,910 --> 00:25:57,340
What about you?

442
00:25:58,110 --> 00:26:02,080
Giving over Mother's name to an affiliated company is a felony.

443
00:26:02,340 --> 00:26:03,780
If they ask me to quit, I will.

444
00:26:04,080 --> 00:26:05,410
That was wrong of me.

445
00:26:05,510 --> 00:26:08,340
Fine. Let's say that you quit.

446
00:26:08,640 --> 00:26:10,040
What if you can't disclose Mr Oh's corruption?

447
00:26:12,210 --> 00:26:13,940
Please, wait a little more.

448
00:26:14,310 --> 00:26:17,080
We'll make a move once we have something.

449
00:26:17,210 --> 00:26:18,880
We can't move in vain.

450
00:26:19,610 --> 00:26:20,880
We may lose everything.

451
00:26:20,980 --> 00:26:22,940
I'm just so frustrated.

452
00:26:23,210 --> 00:26:25,110
I can't even face Mum,

453
00:26:25,340 --> 00:26:28,210
and I hate that you two are fighting because of me.

454
00:26:28,810 --> 00:26:31,380
What if something bad happens to Mum?

455
00:26:32,280 --> 00:26:34,180
What will I do then?

456
00:26:34,380 --> 00:26:36,980
Mr Oh won't make a rash move.

457
00:26:37,480 --> 00:26:40,380
He probably embezzled much money by an illegal use of her name.

458
00:26:41,080 --> 00:26:42,310
He won't give that up.

459
00:26:42,940 --> 00:26:45,310
We have to get as much information as we can...

460
00:26:45,710 --> 00:26:48,680
until Mr Oh makes a move first.

461
00:26:49,080 --> 00:26:51,910
I'm going to meet with Mr Park, the communicator with SS Global.

462
00:26:52,740 --> 00:26:55,140
Ye Eun, please.

463
00:26:55,840 --> 00:26:59,040
You really shouldn't make a rash move. Okay?

464
00:26:59,740 --> 00:27:00,810
Please.

465
00:27:02,940 --> 00:27:03,940
Okay.

466
00:27:06,180 --> 00:27:07,180
Honey.

467
00:27:09,080 --> 00:27:10,110
Mi So.

468
00:27:10,380 --> 00:27:12,710
Why are you two together?

469
00:27:13,810 --> 00:27:15,880
What's so weird about that?

470
00:27:17,080 --> 00:27:20,040
I'm glad you're here. I had something to tell you.

471
00:27:20,410 --> 00:27:22,740
What? What are you doing, Ye Eun?

472
00:27:26,080 --> 00:27:27,110
What is it?

473
00:27:32,410 --> 00:27:33,410
I'm sorry.

474
00:27:34,010 --> 00:27:35,380
- I have... - Ye Eun.

475
00:27:35,580 --> 00:27:36,780
- I... - Ye Eun.

476
00:27:38,240 --> 00:27:39,440
What's with you? What is it?

477
00:27:40,080 --> 00:27:43,510
I handed over Mum's name for SS Global to use.

478
00:27:46,040 --> 00:27:48,440
- What? - Mr Kim found that out.

479
00:27:48,740 --> 00:27:51,710
That's why he is after Mr Oh's corruption.

480
00:27:53,310 --> 00:27:54,910
Why did you do that?

481
00:27:55,040 --> 00:27:56,410
Why?

482
00:27:57,140 --> 00:27:58,310
I'm sorry.

483
00:28:06,180 --> 00:28:08,580
I doubt he'll tell me why they fought.

484
00:28:13,780 --> 00:28:17,080
Do you mind if I ask what's going on?

485
00:28:17,280 --> 00:28:20,040
I'll tell you when I need your help.

486
00:28:20,240 --> 00:28:22,140
What if they were going after Ye Eun...

487
00:28:22,940 --> 00:28:24,440
and not you?

488
00:28:24,640 --> 00:28:26,580
Did something happen between you and Dung Kim?

489
00:28:29,110 --> 00:28:31,180
Does it have something to do with her?

490
00:28:35,610 --> 00:28:37,240
Wait. Get up.

491
00:28:37,680 --> 00:28:39,010
Let's go outside and talk.

492
00:28:39,280 --> 00:28:40,810
- Get up. - Why?

493
00:28:40,980 --> 00:28:41,980
Why?

494
00:28:43,080 --> 00:28:45,640
It's Il Mok. We were going to meet here.

495
00:28:45,740 --> 00:28:46,980
- Get up. - Let's go.

496
00:28:47,340 --> 00:28:48,340
What?

497
00:28:48,510 --> 00:28:49,910
Hurry. Let's go.

498
00:28:51,310 --> 00:28:52,740
Welcome, Il Mok.

499
00:28:54,040 --> 00:28:55,140
Oh, hey.

500
00:28:56,780 --> 00:28:58,710
Why are you all here?

501
00:28:58,810 --> 00:29:01,280
Oh, it's because...

502
00:29:02,040 --> 00:29:03,410
Jae Min, why are you here?

503
00:29:03,880 --> 00:29:05,340
I come here often.

504
00:29:06,110 --> 00:29:07,280
What's going on?

