﻿1
00:00:01,000 --> 00:00:02,800
What's my favorite way to relax?

2
00:00:04,035 --> 00:00:05,270
Throw on my wireless headphones

3
00:00:05,304 --> 00:00:07,972
And disappear into
the nature channel.

4
00:00:15,113 --> 00:00:16,781
The fascinating thing about nature

5
00:00:16,815 --> 00:00:18,115
Is how, in the blink of an eye,

6
00:00:18,150 --> 00:00:21,852
It can go from a scene
of total tranquil beauty

7
00:00:21,887 --> 00:00:24,255
To raw, primal violence.

8
00:00:24,289 --> 00:00:27,024
Could you seriously not hear
me with those things on?

9
00:00:27,059 --> 00:00:28,926
It's noise-canceling technology.

10
00:00:28,961 --> 00:00:31,429
Give it a spin.
It's got - sweetie! Focus.

11
00:00:31,463 --> 00:00:33,164
Mom, why are you
freaking out on everyone?

12
00:00:33,198 --> 00:00:35,366
Because you are acting very
irresponsibly - all of you.

13
00:00:35,400 --> 00:00:37,635
Listen, honey,
Luke has a giant project

14
00:00:37,669 --> 00:00:39,403
Due tomorrow for school
that he hasn't even started,

15
00:00:39,438 --> 00:00:41,806
And Haley just informed
me she needs 40 cupcakes

16
00:00:41,840 --> 00:00:44,208
For her school fundraiser,
also due tomorrow.

17
00:00:44,243 --> 00:00:45,610
I'd like to point out

18
00:00:45,644 --> 00:00:48,980
I completed all my assignments
on my own and on time.

19
00:00:49,014 --> 00:00:50,147
Not now, Alex. Not now, Alex.

20
00:00:50,182 --> 00:00:52,183
Okay, look,
I will take the cupcakes.

21
00:00:52,217 --> 00:00:54,619
You do the project.
It's on Vincent Van Gogh. Done.

22
00:00:54,653 --> 00:00:56,187
Okay, I need you to
really stay on him,

23
00:00:56,221 --> 00:00:57,521
Keep him focused.

24
00:00:57,556 --> 00:00:58,789
All right.

25
00:00:58,824 --> 00:01:00,157
I'm... Actually gonna take
a different approach,

26
00:01:00,192 --> 00:01:02,526
But... Actually gonna
insist that you don't.

27
00:01:02,561 --> 00:01:05,329
Claire, I know you've got
your methods, but so do I.

28
00:01:05,364 --> 00:01:07,465
I'm sorry,
but I'm not a micromanager.

29
00:01:07,499 --> 00:01:09,467
Trust me, I can provide Luke

30
00:01:09,501 --> 00:01:12,603
With the tools and guidance
he needs without, uh,

31
00:01:12,638 --> 00:01:14,171
Smothering him.

32
00:01:14,206 --> 00:01:16,107
You think I smother our child?

33
00:01:16,141 --> 00:01:18,843
It's not your fault, honey.
"mother" is part of the word.

34
00:01:18,877 --> 00:01:21,512
You never hear of anyone
being "sfathered" to death.

35
00:01:21,546 --> 00:01:23,314
I know what Luke's problem is.

36
00:01:23,348 --> 00:01:24,849
He's got adhd.

37
00:01:24,883 --> 00:01:26,851
No. No, I don't!

38
00:01:26,885 --> 00:01:28,119
What is it?

39
00:01:28,153 --> 00:01:30,187
I'd tell you, but you'd wander off

40
00:01:30,222 --> 00:01:31,289
Before I got to the "h. "

41
00:01:31,323 --> 00:01:32,523
Just promise me

42
00:01:32,557 --> 00:01:34,892
That you will stay on him
until this project is done.

43
00:01:34,926 --> 00:01:37,495
I will, but you might have a
little more confidence in him.

44
00:01:37,529 --> 00:01:42,366
wah. Wah. No one can hear me now.

45
00:01:42,401 --> 00:01:44,302
Whoooo!

46
00:01:44,336 --> 00:01:48,105
Everybody is stupid, except me.

47
00:01:48,140 --> 00:01:51,309
Ha ha ha. I am funny.

48
00:02:04,689 --> 00:02:06,924
Is this the cutest
thing I've ever seen?

49
00:02:06,958 --> 00:02:08,002
You out here on the curb

50
00:02:08,026 --> 00:02:10,094
With your little telescope,
waiting for your dad.

51
00:02:10,129 --> 00:02:11,829
It's not cute.
It's - it's science.

52
00:02:12,064 --> 00:02:15,399
Tonight is the magnificent
lyrid meteor shower.

53
00:02:15,434 --> 00:02:18,136
It's where the planet geek
passes through the nerdy way.

54
00:02:18,170 --> 00:02:19,871
Oh, you know I think it's sweet.

55
00:02:19,905 --> 00:02:21,572
Every couple years,
Mitchell and his father,

56
00:02:21,607 --> 00:02:23,474
They go out and enjoy one
of these showers together.

57
00:02:23,509 --> 00:02:26,177
Well, I - I wouldn't
necessarily put it that way.

58
00:02:26,211 --> 00:02:28,012
I mean, yes,
me and my father enjoy it

59
00:02:28,046 --> 00:02:30,548
Because we - we don't share
a lot of the same interests.

60
00:02:30,582 --> 00:02:34,085
Like, um, I-I never went
to sports games with him.

61
00:02:34,119 --> 00:02:36,320
Probably because you call them "
sports games. "

62
00:02:37,956 --> 00:02:39,790
Just lose the "sports. "

63
00:02:39,825 --> 00:02:41,659
Just - just "games. "

64
00:02:41,693 --> 00:02:43,127
Oh, here comes your shower partner.

65
00:02:43,162 --> 00:02:44,228
Stop it!

66
00:02:44,263 --> 00:02:46,197
I'm kidding. Have fun
with your dad, okay?

67
00:02:46,231 --> 00:02:47,765
All right. See you later.

68
00:02:47,799 --> 00:02:49,767
Don't worry about me. I'm not.

69
00:02:49,801 --> 00:02:52,737
I'm just saying, if you were.

70
00:02:52,271 --> 00:02:53,905
Shotgun, sucker!

71
00:02:59,045 --> 00:03:01,445
Make sure the seat is
exactly the way you want it

72
00:03:01,551 --> 00:03:03,386
And feel free to play with any
of these controls up here.

73
00:03:03,420 --> 00:03:04,487
I have no idea what they do.

74
00:03:04,521 --> 00:03:07,723
<i>But I want mi car to be su car.</i>

75
00:03:07,758 --> 00:03:11,093
Cameron, are you okay?
You seem nervous.

76
00:03:11,128 --> 00:03:13,696
Uh, nervous? No, I'm... Not nervous.

77
00:03:15,899 --> 00:03:17,066
<i>I was nervous.</i>

78
00:03:17,100 --> 00:03:19,168
I mean, when Mitchell made
plans with his father,

79
00:03:19,202 --> 00:03:21,637
I figured, why not spend
the evening with Gloria?

80
00:03:21,672 --> 00:03:23,973
I've always wanted to be
good friends with her.

81
00:03:24,007 --> 00:03:25,908
<i>On paper, we should be
good friends.</i>

82
00:03:25,942 --> 00:03:28,377
Look at us - one spicy,
curvy diva -

83
00:03:28,412 --> 00:03:30,746
And Gloria.

84
00:03:30,781 --> 00:03:33,115
The problem is, I had a little,
minor setback

85
00:03:33,150 --> 00:03:35,050
That we had to overcome
from a couple weeks ago.

86
00:03:35,219 --> 00:03:37,954
I cannot stand hanging out with his ivy
league friends. Oh, they're the worst.

87
00:03:37,988 --> 00:03:39,455
<i>They're not that bad.
They are the worst.</i>

88
00:03:39,490 --> 00:03:40,024
They're not that bad.
Are you kidding me?

89
00:03:40,558 --> 00:03:41,692
Debbie can't go two minutes

90
00:03:41,726 --> 00:03:43,093
Without talking about
columbia university,

91
00:03:43,127 --> 00:03:45,362
And the little guy with
the lazy eye from harvard.

92
00:03:45,396 --> 00:03:46,663
Brown. Whatever.

93
00:03:47,198 --> 00:03:49,265
Honestly, I wish that tart
would go back to columbia

94
00:03:49,300 --> 00:03:51,501
And take her weird,
little brown friend with her.

95
00:03:59,443 --> 00:04:01,277
Cam, you need to relax.
You explained.

96
00:04:01,312 --> 00:04:03,279
Not very well.

97
00:04:03,314 --> 00:04:06,316
You know how awkward I get
when things get awkward.

98
00:04:06,350 --> 00:04:08,117
So when I was id "brown people,"

99
00:04:08,152 --> 00:04:10,553
I- I wasn't talking about your,
uh, brown people.

100
00:04:10,588 --> 00:04:14,190
I was talking about people
who go to a university,

101
00:04:14,225 --> 00:04:15,758
Um, not your people.

102
00:04:15,793 --> 00:04:18,862
Not - not that your people
couldn't go to college.

103
00:04:18,896 --> 00:04:21,965
Okay, now I'm hearing myself say "
your people" a lot, so...

104
00:04:24,802 --> 00:04:27,237
I'm just very excited
because this is, um -

105
00:04:27,271 --> 00:04:29,806
You know, this is our
first night out together,

106
00:04:29,840 --> 00:04:31,140
So it's our little date.

107
00:04:31,175 --> 00:04:32,275
Where are we going?

108
00:04:32,309 --> 00:04:34,444
Make sure your seat belt
is securely fastened

109
00:04:34,478 --> 00:04:36,779
And your tray tables are
in the upright position,

110
00:04:36,814 --> 00:04:39,382
Because I got us into palaiseau!

111
00:04:40,555 --> 00:04:41,950
fancy.

112
00:04:42,000 --> 00:04:43,111
I know, it's normally
a 4-week wait,

113
00:04:43,555 --> 00:04:45,455
But the maitre d' is an old
pilates brother of mine,

114
00:04:45,489 --> 00:04:46,656
So just one phone call.

115
00:04:46,690 --> 00:04:48,024
Okay.

116
00:04:49,058 --> 00:04:50,093
Palaiseau!

117
00:04:51,896 --> 00:04:53,730
<i>Ay, Cameron, you know what?</i>

118
00:04:53,764 --> 00:04:56,766
I don't know if I'm in the
mood for something so fancy.

119
00:04:56,800 --> 00:04:59,402
Can we just go somewhere,
like, more simple?

120
00:04:59,436 --> 00:05:01,971
Oh, of - yes. Yes, of course.

121
00:05:02,006 --> 00:05:06,442
We can - what - what do you
what did you have in mind?

122
00:05:06,477 --> 00:05:07,710
I don't know.

123
00:05:07,745 --> 00:05:11,948
How about a little Latin place
that I always used to go?

124
00:05:11,982 --> 00:05:13,316
That sounds fantastic. Okay.

125
00:05:13,350 --> 00:05:15,785
I would love to see
how your people eat.

126
00:05:15,819 --> 00:05:17,654
"your people"?

127
00:05:18,055 --> 00:05:19,655
What is wrong with me?

128
00:05:20,024 --> 00:05:21,391
Okay, it's egg-crackin' time.

129
00:05:21,425 --> 00:05:23,560
Now, I find the key to
cracking an egg properly -

130
00:05:23,594 --> 00:05:25,862
Mom, I'm not a child. I can do it.

131
00:05:25,896 --> 00:05:27,030
Okay.

132
00:05:27,064 --> 00:05:28,498
Okay. Okay.

133
00:05:28,532 --> 00:05:29,666
My mom's not dumb.

134
00:05:29,700 --> 00:05:32,335
You can't just ask her
to do something for you.

135
00:05:32,370 --> 00:05:36,306
You have to very carefully
put the cheese in the trap.

136
00:05:38,976 --> 00:05:40,977
Uh, oops. I got a little
shell in the bowl.

137
00:05:41,012 --> 00:05:42,912
And when -
when that happens, I find

138
00:05:42,947 --> 00:05:45,048
That I like to not dig it out - I got it.
It's okay. It's right here.

139
00:05:45,082 --> 00:05:46,750
Oh, that's - that's really gross.

140
00:05:46,784 --> 00:05:48,418
Okay, I'm gonna - I'll do this.

141
00:05:48,452 --> 00:05:50,654
You just... Mop up.

142
00:05:50,688 --> 00:05:52,689
And... Snap!

143
00:05:52,723 --> 00:05:54,858
Is it bad that I
feel sorry for her?

144
00:05:55,625 --> 00:05:57,360
So, how do I start?

145
00:05:57,394 --> 00:05:59,028
<i>What do you think?</i>

146
00:05:59,062 --> 00:06:01,997
I don't know. Mom usually
tells me what to do.

147
00:06:02,032 --> 00:06:04,867
Join the club. I'm kidding.
I love your mom.

148
00:06:04,901 --> 00:06:08,738
We are going to try a
new approach this time.

149
00:06:08,772 --> 00:06:10,573
Now, your goal is
to create a display

150
00:06:10,607 --> 00:06:12,408
About the life and art
of Van Gogh, right?

151
00:06:12,442 --> 00:06:13,642
Yeah.

152
00:06:13,677 --> 00:06:16,612
Okay, I got your poster
board right here,

153
00:06:16,646 --> 00:06:20,049
So pretend you're telling
the story of his life

154
00:06:20,083 --> 00:06:21,817
To someone who has
never heard of him.

155
00:06:21,852 --> 00:06:25,488
What do you put on that board?

156
00:06:25,522 --> 00:06:28,591
I see the wheels spinning,
the spark of creat-

157
00:06:28,625 --> 00:06:30,092
How do they get the
lead in pencils?

158
00:06:30,127 --> 00:06:31,494
Okay, let's try to stay on topic.

159
00:06:31,528 --> 00:06:33,129
Hey, Jay, look who thinks

160
00:06:33,163 --> 00:06:36,432
Mercury is the densest
planet in the solar system.

161
00:06:36,466 --> 00:06:39,802
I take it from your mocking
tone that I am incorrect.

162
00:06:39,836 --> 00:06:41,437
The densest planet is earth.

163
00:06:41,505 --> 00:06:44,540
Which makes you the densest
guy on the densest planet.

164
00:06:44,574 --> 00:06:45,875
Zing!

165
00:06:45,909 --> 00:06:47,009
Fun.

166
00:06:47,043 --> 00:06:49,078
You sure Mitchell won't
mind me tagging along?

167
00:06:49,112 --> 00:06:50,679
Are you kidding? He loves you.

168
00:06:50,714 --> 00:06:52,181
What are you worried about?

169
00:06:52,215 --> 00:06:54,150
I've never had a brother before.

170
00:06:54,184 --> 00:06:56,218
We never really hung out that much.

171
00:06:56,253 --> 00:06:58,421
Kiddo, you are overthinking this.

172
00:06:58,455 --> 00:07:00,189
I just don't want to
say the wrong thing.

173
00:07:00,223 --> 00:07:02,258
You can't. That's the
beauty of having a brother.

174
00:07:02,292 --> 00:07:04,560
Me and my brother were zinging
each other all the time,

175
00:07:04,594 --> 00:07:07,096
You know, like,
"what's going on, fat boy?"

176
00:07:07,130 --> 00:07:10,366
"nothing much, jackass.
" you know, things of that nature.

177
00:07:10,400 --> 00:07:13,269
It's - it's how
brothers express love.

178
00:07:13,303 --> 00:07:14,637
Hmm.

179
00:07:17,374 --> 00:07:19,208
Shotgun, sucker!

180
00:07:21,077 --> 00:07:23,512
yeah.

181
00:07:23,547 --> 00:07:26,849
I'm gonna... See the sights.

182
00:07:27,883 --> 00:07:29,684
Not if they see you first!

183
00:07:29,718 --> 00:07:32,454
Kind of missed with that one, kid.

184
00:07:33,557 --> 00:07:38,227
All right, this is all you
need to know right here.

185
00:07:38,261 --> 00:07:39,929
Apparently, the
pencil's in two pieces,

186
00:07:39,963 --> 00:07:41,864
And they glue th
together around the lead.

187
00:07:41,898 --> 00:07:44,733
What if we put all the stuff
about his life on this side

188
00:07:44,768 --> 00:07:47,369
And all the stuff about his
paintings on this side?

189
00:07:47,404 --> 00:07:49,371
Luke, buddy, that's fantastic.

190
00:07:49,406 --> 00:07:52,741
And the best part is you came
up with that by yourself.

191
00:07:52,776 --> 00:07:55,978
All it took was a little
bit of focus and, uh...

192
00:07:56,012 --> 00:07:57,847
This thing is really loose.

193
00:07:57,881 --> 00:08:00,783
I'm gonna go grab a screwdriver,
but you run with this.

194
00:08:00,817 --> 00:08:03,285
On your mark, get set, Van Gogh!

195
00:08:03,320 --> 00:08:04,620
Hey, dad.

196
00:08:04,654 --> 00:08:06,689
He kind of looks
like uncle Mitchell,

197
00:08:06,723 --> 00:08:08,524
If uncle Mitchell were insane.

198
00:08:10,961 --> 00:08:11,994
Aah!

199
00:08:12,028 --> 00:08:13,629
I got sprayed by a skunk!

200
00:08:13,663 --> 00:08:15,364
Oh! Ugh!

201
00:08:15,398 --> 00:08:16,632
Oh, you stink.

202
00:08:16,700 --> 00:08:19,535
And not just at astronomy! Blammo!

203
00:08:19,569 --> 00:08:21,573
What is happening?

204
00:08:26,224 --> 00:08:27,258
I love this place.

205
00:08:27,292 --> 00:08:29,727
I was a little bit worried,
because it's not exactly -

206
00:08:29,761 --> 00:08:32,630
I... Love... This place.

207
00:08:33,832 --> 00:08:35,933
I... Was... Nervous.

208
00:08:35,968 --> 00:08:38,569
I mean, there was a lot of
different food on the menu.

209
00:08:38,604 --> 00:08:40,371
And on the floor. And on the wall.

210
00:08:40,405 --> 00:08:42,741
Are we - are we sure we're not
exaggerating just a little bit?

211
00:08:42,875 --> 00:08:43,975
Were you there, Mitchell?

212
00:08:44,009 --> 00:08:45,243
Because I think I
would've recognized

213
00:08:45,277 --> 00:08:47,312
The only other white or gay person.

214
00:08:47,346 --> 00:08:49,447
I had already offended Gloria once.

215
00:08:49,482 --> 00:08:51,449
Not gonna do it again.

216
00:08:51,484 --> 00:08:52,784
<i>Hello. ÃÂ¿cÃÂ³mo estÃÂ¡?</i>

217
00:08:52,818 --> 00:08:54,052
Gloria!

218
00:08:54,086 --> 00:08:56,154
<i>Desde hace mucho que no vienes.</i>

219
00:08:56,188 --> 00:08:58,123
<i>Mi amigo, Cameron. Oh, welcome.</i>

220
00:08:58,157 --> 00:09:00,658
<i>So, carnitas diablos?</i>

221
00:09:00,693 --> 00:09:01,960
Ah, you remember!

222
00:09:01,994 --> 00:09:03,795
Uh, I'll just have the same thing.

223
00:09:03,829 --> 00:09:06,498
No, no, no, no, no, you should have
the chicken enchiladas. Mm-hmm.

224
00:09:06,532 --> 00:09:09,501
<i>Uh, no, I'll have the
carnitas diablos.</i>

225
00:09:09,535 --> 00:09:10,869
These are not for you.

226
00:09:10,903 --> 00:09:12,771
Excuse me? They're too spicy.

227
00:09:12,805 --> 00:09:14,773
And this girl is used to it.
Yeah, he's right.

228
00:09:14,807 --> 00:09:17,308
I can spice you under
the table any day,

229
00:09:17,343 --> 00:09:18,943
There, sweet thing.

230
00:09:18,978 --> 00:09:22,947
<i>Dos carnitas diablos, por favor.</i>

231
00:09:22,982 --> 00:09:25,984
Okay.

232
00:09:28,320 --> 00:09:29,954
Mitchell.

233
00:09:29,988 --> 00:09:31,455
Of course. What was I thinking?

234
00:09:32,691 --> 00:09:34,825
Dad, can we -
can we just go home, please?

235
00:09:34,860 --> 00:09:37,361
You've got to air out first
before I let you in that car.

236
00:09:37,396 --> 00:09:38,929
I'll never get the stink out.

237
00:09:38,964 --> 00:09:40,398
Hey, Jay, is that venus?

238
00:09:40,465 --> 00:09:44,035
'cause I can smell the clouds
of pure sulfuric acid from here.

239
00:09:44,102 --> 00:09:45,236
Slam!

240
00:09:45,270 --> 00:09:47,705
Okay, I-I - I'd really
like to go home now.

241
00:09:47,739 --> 00:09:48,206
We'll miss the meteor shower.

242
00:09:48,741 --> 00:09:50,909
The trouble is your clothes.
Just take them off.

243
00:09:50,976 --> 00:09:52,977
I think there's a blanket
in the trunk. You sure, dad?

244
00:09:53,012 --> 00:09:55,313
You're - you're not worried
I might stink up the blanket?

245
00:09:55,347 --> 00:09:56,447
Don't worry about it.

246
00:09:56,482 --> 00:09:58,316
We just use it to cover up the seat

247
00:09:58,350 --> 00:10:00,752
For when manny's all sweaty
after his tango class.

248
00:10:00,786 --> 00:10:03,521
If you don't sweat,
you're not doing it right.

249
00:10:09,628 --> 00:10:12,863
So, he was one of the best
boyfriends I ever had.

250
00:10:12,898 --> 00:10:14,432
But he was gay.

251
00:10:14,966 --> 00:10:17,134
Yeah, I figured that out
after the first month,

252
00:10:17,168 --> 00:10:18,635
But I stayed for a whole year

253
00:10:18,670 --> 00:10:20,437
Because the haircuts
were fantastic!

254
00:10:20,471 --> 00:10:24,041
it's funny, huh?

255
00:10:24,075 --> 00:10:26,243
Cameron, come on,
it's not that funny.

256
00:10:26,277 --> 00:10:27,778
Are you okay I'm fine.

257
00:10:27,812 --> 00:10:29,580
Are you sure? I love this place.

258
00:10:30,682 --> 00:10:32,283
But your head is running water.

259
00:10:32,317 --> 00:10:34,118
No, I don't think it is.

260
00:10:34,152 --> 00:10:37,288
I told you it was too spicy for you!
Look at your shirt!

261
00:10:37,322 --> 00:10:39,890
No, I just need to get a little bit of a
drink of something. No, no, no, no, no.

262
00:10:39,925 --> 00:10:41,792
Cameron, the water
makes it so worse.

263
00:10:41,827 --> 00:10:42,827
No!

264
00:10:43,629 --> 00:10:45,897
Oh, you're right.
That does make it worse.

265
00:10:45,931 --> 00:10:48,266
I feel like I ate the sun!

266
00:10:49,000 --> 00:10:51,736
And that is the secret
to no lumps. See?

267
00:10:52,271 --> 00:10:53,271
It's kind of fun.

268
00:10:53,305 --> 00:10:54,539
You make it fun, mom.

269
00:10:54,573 --> 00:10:57,041
Mmm. Listen to these
symptoms of adhd

270
00:10:57,076 --> 00:10:58,843
And tell me it's not Luke. Alex.

271
00:10:58,877 --> 00:11:01,779
"easily distracted by
irrelevant stimuli. "

272
00:11:11,123 --> 00:11:15,626
"often impulsively abandons
one task for another. "

273
00:11:21,467 --> 00:11:23,534
That's where I left those.

274
00:11:23,569 --> 00:11:26,437
"a tendency to act without
regard to consequences,

275
00:11:26,472 --> 00:11:30,074
Often at the expense
of personal safety. "

276
00:11:46,859 --> 00:11:48,500
"having accidents more often -"

277
00:11:48,611 --> 00:11:49,961
Okay, you know what?
I think that that's enough, Alex.

278
00:11:49,995 --> 00:11:51,062
We've - we've got it.

279
00:12:06,544 --> 00:12:09,479
Okay, no. No.

280
00:12:12,216 --> 00:12:14,652
Phil? Phil, honey, are you okay?

281
00:12:14,687 --> 00:12:15,087
Yeah.

282
00:12:15,821 --> 00:12:17,789
Remember those sunglasses
I couldn't find?

283
00:12:17,856 --> 00:12:19,390
Bingo!

284
00:12:21,226 --> 00:12:23,594
I asked you to do one thing
stay on top of Luke.

285
00:12:23,628 --> 00:12:25,796
For your information,
your son is hard at work

286
00:12:25,831 --> 00:12:28,432
On an awesome Van Gogh - got
to fix that - masterpiece.

287
00:12:28,467 --> 00:12:29,500
Wait till you see.

288
00:12:29,534 --> 00:12:31,268
You're gonna be so surprised.

289
00:12:31,303 --> 00:12:33,103
Oh, god, honey, I want nothing more

290
00:12:33,138 --> 00:12:35,239
Than to be surprised, really.

291
00:12:38,143 --> 00:12:40,211
Oh.

292
00:12:40,245 --> 00:12:41,912
I'm not surprised.

293
00:12:43,715 --> 00:12:44,282
Okay, that's it.

294
00:12:44,416 --> 00:12:45,717
I don't want to hear anything

295
00:12:45,751 --> 00:12:47,318
About your new method
of doing things.

296
00:12:47,353 --> 00:12:49,187
There's one thing that
works with these kids,

297
00:12:49,188 --> 00:12:51,055
And that is staying on top of them,

298
00:12:51,090 --> 00:12:52,890
Which, thanks to you, my friend,

299
00:12:52,925 --> 00:12:55,126
I will now be able
to do all night long.

300
00:13:02,634 --> 00:13:03,868
Not happy, dude.

301
00:13:03,902 --> 00:13:05,003
You're supposed to be working.

302
00:13:05,037 --> 00:13:07,222
<i>I am working.</i>
-Mr Potato Head
 Really??

303
00:13:07,473 --> 00:13:09,307
I know what you think, but - no,
I don't want to hear it.

304
00:13:09,341 --> 00:13:10,641
I'm going down to
clean up the garage.

305
00:13:10,676 --> 00:13:12,377
Then I'm coming back,
and I'm gonna stand over you

306
00:13:12,411 --> 00:13:14,545
Until this thing is done, okay?

307
00:13:21,987 --> 00:13:23,988
Breathe. Breathing only
makes the fire spread.

308
00:13:24,023 --> 00:13:25,056
Okay, then drink this milk.

309
00:13:25,790 --> 00:13:27,324
<i>Ay, cam!</i>

310
00:13:27,358 --> 00:13:30,094
Why did you have to
order that spicy dish?

311
00:13:30,128 --> 00:13:32,196
Because I'm - I'm a big idiot.

312
00:13:32,230 --> 00:13:34,798
And I wanted to have
this awesome night

313
00:13:34,833 --> 00:13:36,233
Between the two of us,

314
00:13:36,267 --> 00:13:38,035
Where we end up best friends,

315
00:13:38,069 --> 00:13:40,370
Uh, having lunch, buying shoes.

316
00:13:40,405 --> 00:13:41,371
Okay!

317
00:13:41,406 --> 00:13:44,508
Let's go have lunch!
Let's go buy shoes!

318
00:13:44,542 --> 00:13:45,843
Really?

319
00:13:45,877 --> 00:13:47,979
Yeah, why are you so surprised?
I don't know.

320
00:13:48,013 --> 00:13:51,049
I guess I just always feel like
I- I blow it with you, Gloria.

321
00:13:51,083 --> 00:13:52,884
Whether it's the colombian comment

322
00:13:52,918 --> 00:13:54,786
Or picking the wrong restaurant -

323
00:13:54,820 --> 00:13:55,954
The restaurant?

324
00:13:55,988 --> 00:13:58,256
That had nothing to do with you.

325
00:13:58,290 --> 00:14:01,760
I was just not in the mood
to go anywhere that fancy

326
00:14:01,794 --> 00:14:04,529
After the stupid thing
I did this morning.

327
00:14:04,533 --> 00:14:06,535
What did you do?

328
00:14:06,540 --> 00:14:08,475
I bought a dress.

329
00:14:09,768 --> 00:14:10,901
You monster.

330
00:14:10,936 --> 00:14:14,005
It was a very expensive
dress that I don't need.

331
00:14:14,039 --> 00:14:16,073
I felt so stupid
that when I got home,

332
00:14:16,108 --> 00:14:18,342
I couldn't even bring
it inside the house.

333
00:14:18,377 --> 00:14:19,677
I left it in the car.

334
00:14:19,711 --> 00:14:22,046
I totally get it.
I have a legendary hat story.

335
00:14:22,080 --> 00:14:24,215
I'll bet you look
fabulous in that dress.

336
00:14:24,249 --> 00:14:27,351
Yes, but anyone would
look fabulous in that dress.

337
00:14:28,353 --> 00:14:29,720
You find that blanket?

338
00:14:29,755 --> 00:14:31,989
No.

339
00:14:32,024 --> 00:14:34,458
This was all I could find. Shut up!

340
00:14:34,493 --> 00:14:35,860
Really shows off your shape.

341
00:14:35,894 --> 00:14:38,696
Okay. Spin around, cupcake.
Let's see the caboose.

342
00:14:40,230 --> 00:14:41,299
you get fries with that shake?

343
00:14:43,601 --> 00:14:44,935
Was that too far?

344
00:14:44,969 --> 00:14:46,537
There's a line, Jay.

345
00:14:46,571 --> 00:14:49,373
I better go mop this up.

346
00:14:49,407 --> 00:14:52,776
Oh, come on. Where's the...

347
00:14:55,446 --> 00:14:58,182
I'm sorry if things got a
little out of hand back there,

348
00:14:58,216 --> 00:15:00,717
But in our defense, look at you.

349
00:15:00,752 --> 00:15:03,220
I mean, smell you.

350
00:15:03,254 --> 00:15:05,522
This is a fantastic apology.

351
00:15:05,557 --> 00:15:06,657
Oh, don't be too hard on the kid.

352
00:15:06,692 --> 00:15:08,225
I was egging him on.

353
00:15:08,260 --> 00:15:11,095
If you're gonna be mad, be mad at me.

354
00:15:11,129 --> 00:15:12,730
Done.

355
00:15:12,764 --> 00:15:14,832
I was just happy to see
him laugh a little bit.

356
00:15:14,866 --> 00:15:16,667
You know, he's had
kind of a tough week.

357
00:15:16,702 --> 00:15:18,235
I got sprayed by a skunk,

358
00:15:18,270 --> 00:15:21,539
And I'm wearing a dress that
makes my hips look huge.

359
00:15:21,573 --> 00:15:24,175
I know he doesn't want
me to talk about it,

360
00:15:24,209 --> 00:15:26,911
But he didn't get invited
to this big party.

361
00:15:26,945 --> 00:15:30,915
Some kids he thought were his
friends think he's weird.

362
00:15:30,949 --> 00:15:32,717
Now, you know me on this.

363
00:15:32,751 --> 00:15:34,385
I'm no - I'm no good at it,
you know?

364
00:15:34,419 --> 00:15:36,520
I- I never know what to say.

365
00:15:36,555 --> 00:15:37,988
Well, that's true.

366
00:15:38,023 --> 00:15:41,092
<i>But maybe I raised a kid who
would know what to say.</i>

367
00:15:42,894 --> 00:15:44,795
Now, that's the only
reason I invited him along,

368
00:15:44,830 --> 00:15:46,430
You know, because
this astronomy stuff

369
00:15:46,465 --> 00:15:48,466
That's - that's our thing.

370
00:15:51,470 --> 00:15:53,170
Yeah, no, I'll - I'll talk to him.

371
00:15:53,205 --> 00:15:55,206
You know, you don't
look that ridiculous.

372
00:15:55,240 --> 00:15:57,341
Now, you - you actually
got the legs for it.

373
00:15:57,376 --> 00:15:58,709
Dad -

374
00:15:58,744 --> 00:16:01,078
No, I'm just saying, if you
were that type of a gay... Dad!

375
00:16:01,113 --> 00:16:02,680
...You'd probably do
all right for yourself.

376
00:16:02,714 --> 00:16:04,915
Come on!

377
00:16:06,318 --> 00:16:09,453
You know, I always had a sense
it was dangerous down here,

378
00:16:09,488 --> 00:16:12,390
But I guess it's actually
kind of charming, isn't it?

379
00:16:12,424 --> 00:16:13,824
No, it's not safe at all.

380
00:16:13,859 --> 00:16:17,294
I used to live down here,
you know? What?

381
00:16:17,329 --> 00:16:19,029
Yeah, that's why I come down here.

382
00:16:19,064 --> 00:16:22,133
Look there.
That was my old apartment.

383
00:16:22,167 --> 00:16:24,869
After I left Havier,
that's all I could afford.

384
00:16:25,403 --> 00:16:27,170
It's still part of me. Hmm.

385
00:16:27,205 --> 00:16:29,873
You have to remember
those things. You do.

386
00:16:29,908 --> 00:16:31,041
Like you when you go to your farm.

387
00:16:31,076 --> 00:16:33,377
You remember that?
I told you that a year ago.

388
00:16:33,412 --> 00:16:36,380
Of course I do. You're my friend,
you big idiot!

389
00:16:37,649 --> 00:16:40,351
You know, this part of
town might be very rough,

390
00:16:40,385 --> 00:16:42,386
But the people here, Cameron -

391
00:16:42,421 --> 00:16:43,654
The best.

392
00:16:45,290 --> 00:16:47,858
I'm pretty sure I had
wheels when I parked here.

393
00:16:47,893 --> 00:16:49,427
<i>Ay-yi-yi! Who did this?!</i>

394
00:16:49,461 --> 00:16:50,995
Oh, no, it's okay, Gloria.

395
00:16:51,029 --> 00:16:54,131
Who did this?!
You coward sons of bitches!

396
00:16:54,166 --> 00:16:56,934
It's okay, everybody!
I'm - I'm insured!

397
00:16:56,968 --> 00:16:58,803
What? You scared?!

398
00:16:58,837 --> 00:17:00,538
You scared to show your faces,
little girls?!

399
00:17:00,572 --> 00:17:03,240
No, no, it's all right,
everybody! Huh?! Cameron, wait in the car!

400
00:17:03,275 --> 00:17:06,444
Cameron! Gloria,
I think it's drivable!

401
00:17:06,478 --> 00:17:07,912
Gloria!

402
00:17:09,881 --> 00:17:11,249
Just so you know,
I'm going back up to Luke's room,

403
00:17:11,283 --> 00:17:12,783
And I'm not leaving
until he's finished.

404
00:17:12,818 --> 00:17:14,051
Thank you.

405
00:17:14,086 --> 00:17:17,255
And I'm really sorry for not
underestimating Luke enough.

406
00:17:17,289 --> 00:17:18,956
Well, that means a lot to me.

407
00:17:18,991 --> 00:17:22,393
The hardest part is he kind of
takes after me with all this. Hmm.

408
00:17:22,427 --> 00:17:24,795
Like my lack of focus
and your - my what?

409
00:17:24,830 --> 00:17:28,199
I just love you. Hmm?

410
00:17:31,637 --> 00:17:33,671
I did it. Buddy!

411
00:17:33,705 --> 00:17:35,640
Oh, my god.

412
00:17:35,674 --> 00:17:36,974
Do you like it?

413
00:17:37,009 --> 00:17:38,509
I- I-I love it.

414
00:17:38,543 --> 00:17:41,245
Did you this? All him.

415
00:17:41,280 --> 00:17:43,555
I used Mr. Potato Head's ears

416
00:17:43,748 --> 00:17:44,415
Because Van Gogh cut his ear off.

417
00:17:44,449 --> 00:17:46,684
And there's money,
because his paintings sell

418
00:17:46,718 --> 00:17:48,386
For, like, a bajillion dollars,

419
00:17:48,420 --> 00:17:50,921
Which is sad,
because he died broke.

420
00:17:50,956 --> 00:17:52,156
Yes.

421
00:17:52,190 --> 00:17:53,291
That -

422
00:17:53,325 --> 00:17:55,593
You were right. Don't apologize.

423
00:17:55,627 --> 00:17:57,628
I'm not apologizing.
Apology accepted.

424
00:17:57,663 --> 00:18:01,766
Luke, I-I really couldn't
be more proud of you,

425
00:18:01,833 --> 00:18:03,901
And I am so sorry I didn't
give you more credit.

426
00:18:03,935 --> 00:18:05,936
Thanks. Way to go, buddy.

427
00:18:07,906 --> 00:18:10,741
I got to go finish the cupcakes.

428
00:18:10,776 --> 00:18:12,176
no.

429
00:18:12,210 --> 00:18:13,444
She didn't.

430
00:18:13,478 --> 00:18:14,879
Sweetie, can you hand me those?

431
00:18:14,913 --> 00:18:15,946
Uh-huh.

432
00:18:15,981 --> 00:18:17,415
Yeah, I can talk.

433
00:18:18,750 --> 00:18:20,084
What are you doing?!

434
00:18:20,118 --> 00:18:21,686
Well, I showed you
how to make them,

435
00:18:22,220 --> 00:18:23,620
And now you can do it yourself.

436
00:18:25,156 --> 00:18:26,757
Come on, guys, let's go.

437
00:18:26,791 --> 00:18:31,128
Well, good, because
I really wanted to.

438
00:18:31,162 --> 00:18:32,529
Okay.

439
00:18:32,564 --> 00:18:34,198
Setting the oven to 700!

440
00:18:34,232 --> 00:18:37,301
I'm putting the eggs in the bowl!

441
00:18:37,335 --> 00:18:40,470
Oh, I got some shells
in them! Uh-oh!

442
00:18:40,505 --> 00:18:42,806
Are you serious?!

443
00:18:48,045 --> 00:18:49,112
Hey.

444
00:18:49,146 --> 00:18:50,747
Hey, Mitchell.

445
00:18:50,781 --> 00:18:53,116
So, I know you'd never, um...

446
00:18:54,218 --> 00:18:56,453
I know you'd never -
never believe this

447
00:18:56,487 --> 00:18:58,121
By looking at me right now,

448
00:18:58,156 --> 00:19:00,724
But, uh, I used to get
picked on at school, too.

449
00:19:00,758 --> 00:19:02,559
So, Jay told you?

450
00:19:02,593 --> 00:19:06,096
Yeah. Yeah, they would,
um - they'd call me weird.

451
00:19:06,130 --> 00:19:08,698
<i>I was weird - fun weird.</i>

452
00:19:08,733 --> 00:19:11,001
But I - this is the funny
thing about growing up.

453
00:19:11,035 --> 00:19:13,270
For years and years,
everybody's desperately afraid

454
00:19:13,304 --> 00:19:15,105
To be different,
you know, in any way.

455
00:19:15,139 --> 00:19:18,975
And then, suddenly,
almost overnight,

456
00:19:19,010 --> 00:19:22,445
<i>Everybody wants to be different.</i>

457
00:19:22,480 --> 00:19:26,583
And that is where we win.

458
00:19:26,617 --> 00:19:28,752
I'm sort of counting on that.

459
00:19:28,786 --> 00:19:32,255
I'm sorry I was picking on
you too much. No, it's -

460
00:19:32,290 --> 00:19:33,757
Jay said that's what brothers do.

461
00:19:33,791 --> 00:19:36,093
well, we don't have
to listen to him.

462
00:19:36,127 --> 00:19:37,394
Brothers do that, too.

463
00:19:37,429 --> 00:19:39,963
Okay, ladies,
finish up your tea party.

464
00:19:39,998 --> 00:19:41,265
You're missing the show.

465
00:19:41,299 --> 00:19:43,934
I can't believe I was
fighting over this guy, huh?

466
00:19:43,968 --> 00:19:45,602
Check it out. Oh, whoa.

467
00:19:45,637 --> 00:19:47,705
Check it out.

468
00:19:47,739 --> 00:19:50,240
Wow. Awesome.

469
00:19:54,112 --> 00:19:57,815
Why did he paint "starry night"?

470
00:19:57,849 --> 00:20:00,084
Maybe 'cause the sky is beautiful

471
00:20:00,118 --> 00:20:02,386
And everybody likes looking at it.

472
00:20:04,389 --> 00:20:07,024
And it reminds us that
something's up there,

473
00:20:07,058 --> 00:20:09,126
Watching over all of us.

474
00:20:09,160 --> 00:20:12,229
Aliens, who could
be here in a second

475
00:20:12,263 --> 00:20:15,065
To liquefy us and use us as fuel.

476
00:20:15,133 --> 00:20:17,735
So, wake up, people. We're next.

477
00:20:17,769 --> 00:20:19,770
Mom?

478
00:20:19,804 --> 00:20:20,953
You better get down here!

479
00:20:26,592 --> 00:20:28,293
Mmm! Oh, my god.

480
00:20:28,328 --> 00:20:31,563
Whoa! Delicious. Haley: Uh-huh.

481
00:20:31,597 --> 00:20:32,898
I am so proud.

482
00:20:32,932 --> 00:20:34,866
Oh, thanks, mom.
I'm just gonna wrap these up.

483
00:20:34,901 --> 00:20:36,468
Oh, you know what, sweetie?
You're gonna be late for school.

484
00:20:36,502 --> 00:20:38,036
Just go. I'll bring them by later.

485
00:20:38,071 --> 00:20:40,706
Are you sure? I'm done? Yeah,
get out of here, Betty Crocker.

486
00:20:40,740 --> 00:20:43,342
Okay. So proud of you!

487
00:20:44,644 --> 00:20:46,678
Ugh! Do we still have the
number for poison control?

488
00:20:46,713 --> 00:20:48,747
I love you, claire!
I've always loved you!

489
00:20:48,781 --> 00:20:51,683
My mouth is asleep
like at the dentist.

490
00:20:51,718 --> 00:20:53,185
Get over here, buddy.