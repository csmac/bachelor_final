1
00:02:03,466 --> 00:02:04,448
No...

2
00:02:07,179 --> 00:02:08,324
This other one.

3
00:04:37,834 --> 00:04:39,395
No! No!

4
00:05:52,106 --> 00:05:53,928
We'll soon have reinforcements
from Jaca.

5
00:05:54,347 --> 00:05:58,089
Fifty men or more.
Then we'll go head to head with Vidal.

6
00:05:59,082 --> 00:06:01,471
And then what?

7
00:06:02,154 --> 00:06:05,700
You kill him, they'll send another
just like him. And another...

8
00:06:06,250 --> 00:06:09,349
You're screwed, no guns,
no safe shelter...

9
00:06:09,451 --> 00:06:13,509
You need food, medicine.
You must take care of Mercedes.

10
00:06:13,738 --> 00:06:15,397
If you really love her,
you'd cross the border with her.

11
00:06:15,467 --> 00:06:17,801
- This is a lost cause.
- I'm staying here, doctor.

12
00:06:18,122 --> 00:06:19,050
There's no choice.

13
00:06:30,954 --> 00:06:32,067
You have to leave.

14
00:06:33,770 --> 00:06:36,585
Here's the key,
but you can't go down there now.

15
00:06:36,843 --> 00:06:38,981
It's exactly what he expects.

16
00:06:42,026 --> 00:06:42,954
Leave it to me.

17
00:06:46,858 --> 00:06:47,938
I'm a coward.

18
00:06:49,002 --> 00:06:51,424
No, you're not.

19
00:06:52,555 --> 00:06:53,732
Yes, I am.

20
00:06:54,346 --> 00:06:58,601
A coward... for living
next to that son of a bitch,

21
00:06:59,850 --> 00:07:02,533
doing his laundry, making his bed,
feeding him.

22
00:07:06,090 --> 00:07:08,479
What if the doctor's right
and we can't win?

23
00:07:12,619 --> 00:07:14,757
At least we'll make things
harder for that bastard.

24
00:09:04,939 --> 00:09:05,866
Captain Vidal...

25
00:09:07,882 --> 00:09:10,303
Her temperature's down.
I don't know how, but it is.

26
00:09:11,082 --> 00:09:11,976
But she still has a fever?

27
00:09:12,586 --> 00:09:15,368
Yes, but it's a good sign.
Her body is responding.

28
00:09:17,098 --> 00:09:18,026
Listen to me.

29
00:09:18,666 --> 00:09:21,568
If you have to choose,
save the baby.

30
00:09:21,738 --> 00:09:24,160
That boy will bear my name
and my father's name.

31
00:09:24,298 --> 00:09:25,246
Save him.

32
00:09:59,786 --> 00:10:00,713
Brother...

33
00:10:02,634 --> 00:10:03,528
Little brother...

34
00:10:05,674 --> 00:10:07,135
If you can hear me,

35
00:10:08,202 --> 00:10:10,274
things out here aren't too good.

36
00:10:12,074 --> 00:10:13,984
But soon you'll have to come out.

37
00:10:15,210 --> 00:10:16,771
You've made Mama very sick.

38
00:10:18,890 --> 00:10:22,370
I want to ask you one favor
for when you come out,

39
00:10:23,466 --> 00:10:24,546
just one:

40
00:10:25,834 --> 00:10:27,427
Don't hurt her.

41
00:10:28,586 --> 00:10:31,685
You'll meet her,
she's very pretty,

42
00:10:32,490 --> 00:10:34,662
even though sometimes she's sad
for days at a time.

43
00:10:35,722 --> 00:10:37,511
You'll see, when she smiles,

44
00:10:39,050 --> 00:10:40,479
you'll love her.

45
00:10:42,762 --> 00:10:45,925
Listen, if you do what I say,

46
00:10:46,218 --> 00:10:47,712
I'll make you a promise,

47
00:10:48,618 --> 00:10:52,099
I'll take you to my kingdom,
and I'll make you a prince.

48
00:10:53,066 --> 00:10:56,099
I promise you, a prince.

49
00:11:21,674 --> 00:11:24,095
I sounded the whistle,
but they wouldn't move.

50
00:11:24,298 --> 00:11:26,599
I tried to stop, but it was too late.

51
00:11:26,665 --> 00:11:29,000
The fireman and I
jumped outjust in time,

52
00:11:29,226 --> 00:11:30,568
but look at the mess they made.

53
00:11:33,610 --> 00:11:35,268
What did they steal
from the freight cars?

54
00:11:35,754 --> 00:11:37,478
They didn't open a single one.

55
00:11:37,738 --> 00:11:39,112
What the hell are you talking about?

56
00:11:39,242 --> 00:11:41,446
This whole mess...
They didn't open any of the cars.

57
00:11:41,642 --> 00:11:45,766
- They didn't take anything.
- Nothing? Are you sure?

58
00:11:46,122 --> 00:11:49,831
God only knows what they wanted,
other than to waste our time.

59
00:12:05,994 --> 00:12:07,685
They came out of nowhere, Captain.

60
00:12:10,890 --> 00:12:13,923
They have grenades,
they went up hill.

61
00:12:18,442 --> 00:12:21,126
Captain, we've surrounded
a small unit that fell behind.

62
00:12:21,354 --> 00:12:22,881
They took cover up on the hill.

63
00:12:56,522 --> 00:13:00,832
Go ahead, Serrano, don't be afraid,
this is the only decent way to die.

64
00:13:42,762 --> 00:13:43,907
Serrano!

65
00:14:00,425 --> 00:14:03,328
Let me see.

66
00:14:07,882 --> 00:14:08,929
Can you talk?

67
00:14:13,162 --> 00:14:14,089
Damn it!

68
00:14:31,914 --> 00:14:34,401
These are useless! They can't talk.

69
00:14:43,338 --> 00:14:45,891
Captain,
this one's still alive.

70
00:14:49,802 --> 00:14:51,111
Shot in the leg.

71
00:14:56,809 --> 00:14:58,118
- What's happened?
- They caught one.

72
00:14:58,218 --> 00:15:00,487
They took one of them alive.
And they're taking him to the storeroom.

73
00:15:05,482 --> 00:15:06,562
Mercedes!

74
00:15:07,369 --> 00:15:09,508
Pedro... Pedro...

75
00:15:26,250 --> 00:15:27,177
Mercedes?

76
00:15:28,074 --> 00:15:30,944
- I need to get into the storeroom.
- Not now.

77
00:15:31,434 --> 00:15:32,710
Move away!

78
00:15:58,698 --> 00:16:00,837
That's plenty, dear.

79
00:16:04,265 --> 00:16:05,694
Should I take this up?

80
00:16:24,746 --> 00:16:27,113
Here, half the dose.

81
00:16:27,594 --> 00:16:29,317
I don't think I need it.

82
00:16:29,610 --> 00:16:32,479
I feel better, much better.

83
00:16:32,746 --> 00:16:35,048
I don't understand.
But I'm glad.

84
00:16:38,250 --> 00:16:38,817
Mother...

85
00:16:52,969 --> 00:16:55,620
Damn, this cigarette is good!

86
00:16:56,361 --> 00:16:59,623
Real tobacco hard to find.

87
00:17:01,194 --> 00:17:03,496
G-G-Go to hell.

88
00:17:06,057 --> 00:17:09,887
Damn, Garces. We catch one
and he turns out to be a stutterer.

89
00:17:10,153 --> 00:17:12,193
We'll be here all night.

90
00:17:12,394 --> 00:17:14,281
As long as he talks.

91
00:17:19,913 --> 00:17:21,669
Garces is right.

92
00:17:22,473 --> 00:17:24,677
You'd do better
to tell us everything.

93
00:17:25,353 --> 00:17:28,582
But to make sure it happens,
I brought along a few tools.

94
00:17:30,825 --> 00:17:32,931
Just things you pick up
along the way.

95
00:17:33,130 --> 00:17:35,464
At first, I won't be able to trust you,

96
00:17:36,010 --> 00:17:39,326
but after I use this,
you'll own up to a few things.

97
00:17:41,290 --> 00:17:42,696
When we get to these,

98
00:17:43,690 --> 00:17:47,203
we'll have developed...
how can I put this-?

99
00:17:47,593 --> 00:17:50,081
A closer bond,
much like brothers.

100
00:17:51,466 --> 00:17:52,927
You'll see.

101
00:17:54,858 --> 00:17:56,451
And when we get to this one,

102
00:17:57,770 --> 00:18:00,104
I'll believe anything you tell me.

103
00:18:11,914 --> 00:18:13,157
I'll make you a deal.

104
00:18:13,226 --> 00:18:16,608
If you can count to three
without st-t-tuttering,

105
00:18:16,938 --> 00:18:18,083
you can go.

106
00:18:20,170 --> 00:18:22,985
Don't look at him, look at me.

107
00:18:23,242 --> 00:18:24,933
Above me, there's no one.

108
00:18:25,449 --> 00:18:27,272
- Garces!
- Yes, Captain?

109
00:18:27,530 --> 00:18:30,912
If I say this asshole can leave,
would anybody contradict me?

110
00:18:30,985 --> 00:18:32,545
No one, Captain.
He can leave.

111
00:18:33,258 --> 00:18:37,219
There you have it.
Count to three.

112
00:18:52,937 --> 00:18:55,458
- One.
- Good.

113
00:18:59,530 --> 00:19:00,457
Two.

114
00:19:02,154 --> 00:19:04,936
One more and you will be free.

115
00:19:21,161 --> 00:19:22,088
Shame.

116
00:19:33,513 --> 00:19:37,376
Your mother is much better,
Your Highness.

117
00:19:38,761 --> 00:19:44,577
Surely, you must be relieved.

118
00:19:45,065 --> 00:19:47,072
Yes, thank you.

119
00:19:47,466 --> 00:19:50,782
But things haven't turned out so well.

120
00:19:51,658 --> 00:19:52,705
No?

121
00:19:54,442 --> 00:19:57,922
- I had an accident.
- An accident?

122
00:19:58,090 --> 00:19:59,017
Yes.

123
00:20:07,913 --> 00:20:10,695
- You broke the rules!
- It was only two grapes!

124
00:20:10,793 --> 00:20:12,320
I thought no one would notice.

125
00:20:13,673 --> 00:20:16,641
- We've made a mistake!
- A mistake?

126
00:20:17,353 --> 00:20:20,931
You failed.
You can never return.

127
00:20:21,033 --> 00:20:24,546
- It was an accident!
- You cannot return.

128
00:20:24,650 --> 00:20:28,446
The moon will be full in three days.

129
00:20:29,002 --> 00:20:34,468
Your spirit shall forever remain
among the humans.

130
00:20:34,570 --> 00:20:38,279
You shall age like them,
you shall die like them

131
00:20:38,346 --> 00:20:42,274
and all memory of you
shall fade in time.

132
00:20:42,825 --> 00:20:46,655
And we'll vanish along with it.

133
00:20:47,273 --> 00:20:50,918
You will never see us again.

134
00:21:01,194 --> 00:21:04,772
Good day, doctor.
Sorry to wake you so early,

135
00:21:05,482 --> 00:21:07,140
but I think we need your help.

136
00:21:29,513 --> 00:21:31,007
My God, what have you done to him?

137
00:21:31,177 --> 00:21:32,257
Not much.

138
00:21:34,121 --> 00:21:35,845
But things are getting better.

139
00:21:43,434 --> 00:21:47,973
I like having you handy, Doctor.
It has its advantages.

140
00:21:49,450 --> 00:21:50,857
Serrano, stay here.

141
00:21:51,018 --> 00:21:52,032
I talked.

142
00:21:53,386 --> 00:21:54,563
Not much.

143
00:21:56,841 --> 00:22:00,136
- B-b-but I talked.
- I'm sorry.

144
00:22:00,841 --> 00:22:02,368
I'm so sorry.

145
00:22:09,225 --> 00:22:14,146
Kill me.
Kill me now, please.

146
00:22:29,673 --> 00:22:31,200
Son of a bitch.

147
00:22:51,433 --> 00:22:53,288
It'll take away the pain.

148
00:22:59,401 --> 00:23:00,808
It's almost over.

149
00:23:09,193 --> 00:23:10,469
Watch Dr. Ferreiro,

150
00:23:10,601 --> 00:23:12,008
- I'll be right there.
- Yes, Captain.

151
00:23:34,601 --> 00:23:36,128
You're not moving anymore.

152
00:23:37,578 --> 00:23:38,952
Are you sick?

153
00:23:42,025 --> 00:23:44,446
What are you doing down there?

154
00:23:57,482 --> 00:23:59,075
- Call him!
- Who?

155
00:23:59,177 --> 00:24:01,479
Who else, you imbecile?
Ferreiro!

156
00:24:24,585 --> 00:24:26,592
What the hell is this?

157
00:24:29,545 --> 00:24:31,072
No! No, no!

158
00:24:31,305 --> 00:24:34,753
Leave her.
Leave her alone, please!

159
00:24:36,393 --> 00:24:38,848
Look at this! Look what she
was hiding under your bed!

160
00:24:39,241 --> 00:24:40,386
What do you think of this?

161
00:24:44,809 --> 00:24:49,119
Ofelia, what is this thing doing
under the bed?

162
00:24:49,322 --> 00:24:53,096
It's a magic root
the faun gave me.

163
00:24:53,257 --> 00:24:55,745
This is all because of thatjunk
you let her read.

164
00:24:56,073 --> 00:24:57,001
Look what you've done!

165
00:24:57,129 --> 00:25:01,024
Please, leave us alone.
I'll talk to her, darling.

166
00:25:01,641 --> 00:25:04,456
Fine.
As you wish.

167
00:25:06,185 --> 00:25:08,771
He told me you would get better.
And you did.

168
00:25:09,033 --> 00:25:11,815
Ofelia, you have to listen
to your father.

169
00:25:13,097 --> 00:25:14,144
You have to stop all this.

170
00:25:14,793 --> 00:25:17,313
No. I want to leave this place!

171
00:25:17,641 --> 00:25:20,674
Please, take me away from here!
Let's just go, Please!

172
00:25:20,809 --> 00:25:23,013
Things are not that simple.

173
00:25:25,161 --> 00:25:26,917
You're getting older,

174
00:25:27,913 --> 00:25:31,142
soon you'll see that life
isn't like your fairy tales.

175
00:25:31,721 --> 00:25:33,826
The world is a cruel place.

176
00:25:35,145 --> 00:25:38,724
And you'll learn that,
even if it hurts.

177
00:25:40,265 --> 00:25:42,686
- No! No!
- Ofelia!

178
00:25:43,977 --> 00:25:48,734
Magic does not exist.
Not for you, me or anyone else.

179
00:26:00,073 --> 00:26:02,244
Mom! Help!

180
00:26:02,697 --> 00:26:04,071
Help!

181
00:26:04,905 --> 00:26:06,116
Help!

182
00:26:11,913 --> 00:26:12,840
Why did you do it?

183
00:26:15,081 --> 00:26:16,674
It was the only thing I could do.

184
00:26:17,961 --> 00:26:20,099
No. You could have obeyed me.

185
00:26:21,801 --> 00:26:23,972
I could have, but I didn't.

186
00:26:25,833 --> 00:26:28,254
It would have been better for you.
You know it.

187
00:26:29,609 --> 00:26:32,457
I don't understand.
Why didn't you obey me?

188
00:26:32,745 --> 00:26:38,626
To obey-just like that-for the sake
of obeying, without questioning...

189
00:26:40,297 --> 00:26:43,166
that's something only people
like you can do, Captain.

190
00:27:19,081 --> 00:27:20,358
Captain!

191
00:27:26,409 --> 00:27:28,678
Garces! Call the troop paramedic.

192
00:27:28,873 --> 00:27:31,262
- Get up immediately!
- Yes, Captain.

193
00:28:04,489 --> 00:28:06,278
Your wife is dead.

194
00:28:15,561 --> 00:28:18,910
Because the paths to the Lord
are inscrutable.

195
00:28:19,369 --> 00:28:21,157
Because the essence
of His forgiveness

196
00:28:21,225 --> 00:28:24,422
lies in His word
and in His mystery.

197
00:28:25,257 --> 00:28:28,127
Because although God
sends us the message,

198
00:28:28,265 --> 00:28:30,785
it is our task to decipher it.

199
00:28:31,625 --> 00:28:33,381
Because when we open our arms

200
00:28:33,545 --> 00:28:38,215
the earth takes in only a hollow
and senseless shell.

201
00:28:39,529 --> 00:28:42,343
Far away now is the soul
in its eternal glory.

202
00:28:42,921 --> 00:28:46,434
Because it is in pain
that we find the meaning of life.

203
00:28:47,209 --> 00:28:50,023
And the state of grace
that we lose when we are born.

204
00:28:51,305 --> 00:28:56,356
Because God, in His infinite wisdom,
puts the solution in our hands.

205
00:29:00,265 --> 00:29:02,654
And because it is only
in His physical absence,

206
00:29:02,825 --> 00:29:06,916
that the place He occupies
in our souls is reaffirmed.

207
00:29:27,977 --> 00:29:31,326
You knew
Dr. Ferreiro pretty well,
didn't you, Mercedes?

208
00:29:32,361 --> 00:29:35,176
We all knew him, sir.
Everyone around here.

209
00:29:37,065 --> 00:29:40,546
The stutterer spoke of an informer.
Here... at the mill.

210
00:29:41,672 --> 00:29:45,382
Can you imagine?
Right under my nose.

211
00:29:47,145 --> 00:29:50,440
Mercedes, please.

212
00:29:59,785 --> 00:30:01,159
What must you think of me?

213
00:30:02,377 --> 00:30:03,904
You must think that
I'm a monster.

214
00:30:04,425 --> 00:30:07,207
It doesn't matter
what someone like me thinks, sir.

215
00:30:16,969 --> 00:30:19,751
I want you to the storehouse
and bring me some more liquor, please.

216
00:30:20,009 --> 00:30:23,075
Yes, sir.
Goodnight, sir.

217
00:30:23,625 --> 00:30:27,421
Mercedes,
aren't you forgetting something?

218
00:30:28,425 --> 00:30:29,373
Sir?

219
00:30:34,217 --> 00:30:35,078
The key.

220
00:30:36,073 --> 00:30:38,309
I do have the only copy, don't I?

221
00:30:39,465 --> 00:30:40,414
Yes, sir.

222
00:30:41,897 --> 00:30:45,727
You know, there's an odd detail
that's been bothering me.

223
00:30:46,057 --> 00:30:47,911
Maybe it's not important, but...

224
00:30:48,168 --> 00:30:49,695
the day they broke into
the storehouse,

225
00:30:49,929 --> 00:30:52,166
with all those grenades
and explosives,

226
00:30:53,033 --> 00:30:54,462
the lock itself wasn't forced.

227
00:30:56,681 --> 00:31:00,129
As I said, it's probably
not important.

228
00:31:03,593 --> 00:31:04,837
Be very careful.

229
00:31:06,857 --> 00:31:08,035
Good night, sir.

230
00:31:49,129 --> 00:31:51,780
Ofelia, Ofelia!

231
00:31:52,040 --> 00:31:53,983
Ofelia, I'm leaving tonight.

232
00:31:54,504 --> 00:31:57,853
- Where to?
- I can't tell you. I can't tell you...

233
00:31:57,961 --> 00:32:00,863
- Take me with you.
- No, no. I can't.

234
00:32:00,969 --> 00:32:03,871
I can't, my child. But I'll come
back for you, I promise.

235
00:32:04,008 --> 00:32:05,088
Take me with you.

236
00:32:28,968 --> 00:32:30,212
I heard something.

237
00:32:34,984 --> 00:32:38,018
It's nothing, don't worry.

238
00:32:46,985 --> 00:32:47,933
Mercedes.

239
00:32:50,025 --> 00:32:50,952
Ofelia.

240
00:32:59,592 --> 00:33:01,153
How long have
you known about her?

241
00:33:06,345 --> 00:33:07,839
How long have you been
laughing at me?

242
00:33:08,072 --> 00:33:09,217
Little bitch!

243
00:33:10,504 --> 00:33:11,715
Watch her!

244
00:33:15,048 --> 00:33:16,706
And if anyone tries to get in,

245
00:33:17,641 --> 00:33:19,167
kill her first.

246
00:33:32,905 --> 00:33:33,832
Dry meat.

247
00:33:37,288 --> 00:33:38,237
Tobacco.

248
00:33:39,497 --> 00:33:42,148
If you had asked for it, I would have
given this to you, Mercedes.

249
00:33:49,064 --> 00:33:51,203
I want the names of whoever
wrote these letters.

250
00:33:52,873 --> 00:33:54,945
And I want them
in front of me, tomorrow.

251
00:33:55,305 --> 00:33:56,287
Yes, Captain.

252
00:33:59,049 --> 00:34:00,456
You can go, Garces.

253
00:34:01,961 --> 00:34:03,302
You're sure, Captain?

254
00:34:05,640 --> 00:34:08,291
For God's sake,
she's just a woman.

255
00:34:12,104 --> 00:34:14,177
That's what you've always thought.

256
00:34:15,305 --> 00:34:17,988
That's why I was able to get away with it.

257
00:34:18,216 --> 00:34:22,689
- I was invisible to you.
- Damn.

258
00:34:23,497 --> 00:34:27,141
You've found my weakness:
Pride.

259
00:34:28,489 --> 00:34:31,685
But it's your weak points
we're interested in.

260
00:34:33,961 --> 00:34:36,776
It's very simple: You will talk...

261
00:34:38,409 --> 00:34:39,391
And I have to know

262
00:34:41,289 --> 00:34:42,533
that everything you say is the truth.

263
00:34:43,720 --> 00:34:47,811
We have a few things here
strictly for that purpose.

264
00:34:49,673 --> 00:34:50,883
Nothing complicated.

265
00:34:53,609 --> 00:34:55,070
Things we learn on the job.

266
00:34:57,097 --> 00:34:58,984
At first...

267
00:35:15,400 --> 00:35:18,499
I'm not some old man!
Not a wounded prisoner!

268
00:35:18,569 --> 00:35:22,463
Mother Fucker!
Don't you dare touch the girl?

269
00:35:22,696 --> 00:35:24,387
Won't be the first pig I gutted.

270
00:35:46,921 --> 00:35:47,968
Look! He let her go.

271
00:35:50,376 --> 00:35:51,620
The fuck did you say?

272
00:35:59,304 --> 00:36:01,890
Get her!
Come on! Bring her to me!

273
00:36:02,313 --> 00:36:04,069
Bring her to me, damn it!

274
00:36:04,201 --> 00:36:05,630
Mount up!

275
00:37:05,993 --> 00:37:08,382
Lt'll be better if you come
with me without struggling.

276
00:37:09,000 --> 00:37:10,942
The Captain said that if you behave...

277
00:37:15,400 --> 00:37:17,091
Don't be a fool, sweetheart.

278
00:37:17,448 --> 00:37:18,943
If anyone's going to kill you...

279
00:37:20,968 --> 00:37:22,113
I'd rather it be me.

280
00:38:29,609 --> 00:38:34,343
I've decided to give you
one last chance.

281
00:38:39,400 --> 00:38:41,026
Do you promise to do what I say?

282
00:38:42,953 --> 00:38:47,011
Will you do everything I tell you,
without question?

283
00:38:48,744 --> 00:38:51,592
This is your last chance.

284
00:38:52,680 --> 00:38:55,680
Then listen to me.

285
00:38:56,104 --> 00:38:58,406
Fetch your brother

286
00:38:58,792 --> 00:39:01,345
and bring him to the labyrinth,

287
00:39:01,480 --> 00:39:05,703
as quickly as you can,
Your Highness.

288
00:39:05,928 --> 00:39:07,205
My brother?

289
00:39:07,432 --> 00:39:08,709
We need him.

290
00:39:08,840 --> 00:39:11,775
- But...
- No more questions.

291
00:39:14,664 --> 00:39:16,158
The door's locked.

292
00:39:17,128 --> 00:39:22,791
In that case,
create your own door.

293
00:42:04,520 --> 00:42:05,894
Captain, with your permission.

294
00:42:09,128 --> 00:42:10,110
Come quickly.

295
00:42:10,856 --> 00:42:13,988
- Now what?
- Serrano is back. He's wounded.

296
00:42:14,920 --> 00:42:16,229
Wounded?

297
00:43:00,552 --> 00:43:01,893
Where's Garces?

298
00:43:09,064 --> 00:43:10,013
How many were there?

299
00:43:10,536 --> 00:43:12,675
I don't know exactly, Captain.

300
00:43:13,096 --> 00:43:14,525
Fifty men, at least.

301
00:43:14,632 --> 00:43:18,080
The rest of the men didn't make it.
Our watch posts are not responding.

302
00:43:18,216 --> 00:43:21,664
- How many men we have left?
- Twenty, maybe less, sir.

303
00:43:32,040 --> 00:43:35,269
We're leaving.
Together.

304
00:43:36,264 --> 00:43:37,474
Don't be afraid.

305
00:43:38,312 --> 00:43:39,905
Nothing is going to happen to you.

306
00:43:46,024 --> 00:43:47,584
Put them on picket duty
at the tree line.

307
00:43:47,720 --> 00:43:49,760
If the rest of the squad gets back,
have it report immediately to me.

308
00:43:49,992 --> 00:43:53,341
Radio for reinforcements, now.

309
00:43:53,512 --> 00:43:54,756
Yes, Captain.

310
00:44:20,488 --> 00:44:21,665
Leave him!

311
00:45:05,864 --> 00:45:07,074
Ofelia!

312
00:46:30,664 --> 00:46:33,250
Quickly, Your Majesty,

313
00:46:33,511 --> 00:46:35,933
give him to me.

314
00:46:36,456 --> 00:46:39,423
The full moon is high in the sky,

315
00:46:39,527 --> 00:46:42,244
we can open the portal.

316
00:46:43,111 --> 00:46:44,672
Why is that in your hand?

317
00:46:46,120 --> 00:46:50,822
The portal will only open
if we offer the blood of an innocent.

318
00:46:51,112 --> 00:46:55,367
Just a drop of blood.
A pinprick, that's all.

319
00:46:55,464 --> 00:46:57,952
It's the final task.

320
00:46:58,248 --> 00:46:59,524
Hurry.

321
00:47:02,120 --> 00:47:05,087
You promised to obey me!

322
00:47:05,512 --> 00:47:07,650
- Give me the boy!
- No!

323
00:47:08,232 --> 00:47:10,271
My brother stays with me.

324
00:47:10,503 --> 00:47:16,167
You would give up your sacred rights
for this brat you barely know?

325
00:47:16,232 --> 00:47:18,915
Yes, I would.

326
00:47:20,168 --> 00:47:22,535
You would give up
your throne for him?

327
00:47:22,664 --> 00:47:27,072
He who has caused you
such misery, such humiliation?

328
00:47:27,592 --> 00:47:30,341
Yes, I would.

329
00:47:34,280 --> 00:47:39,747
As you wish, Your Highness.

330
00:47:54,824 --> 00:47:55,806
No!

331
00:48:52,103 --> 00:48:53,052
My son.

332
00:49:19,528 --> 00:49:21,121
Tell my son...

333
00:49:26,727 --> 00:49:28,735
Tell him what time his father died.

334
00:49:29,512 --> 00:49:30,755
- Tell him that I...
- No.

335
00:49:31,912 --> 00:49:33,603
He won't even know your name.

336
00:51:12,231 --> 00:51:14,173
Arise, my daughter.

337
00:51:17,352 --> 00:51:18,562
Come.

338
00:51:43,720 --> 00:51:44,897
Father.

339
00:51:45,384 --> 00:51:48,864
You have spilled your own blood
rather than that of an innocent.

340
00:51:49,607 --> 00:51:53,284
That was the final task,
and the most important.

341
00:52:03,367 --> 00:52:08,419
And you chose well,
Your Highness.

342
00:52:08,648 --> 00:52:10,241
Come here with me,

343
00:52:11,079 --> 00:52:13,119
and sit by your father's side.

344
00:52:13,575 --> 00:52:15,681
He's been waiting for you so long.

345
00:53:06,440 --> 00:53:10,051
And it is said the princess
returned to her father's kingdom.

346
00:53:10,951 --> 00:53:16,068
That she reigned there with justice
and a kind heart for many centuries.

347
00:53:16,935 --> 00:53:19,107
That she was loved by her people.

348
00:53:19,368 --> 00:53:23,874
And that she left behind
small traces of her time on earth,

349
00:53:24,936 --> 00:53:29,573
visible only to those
who know where to look.

