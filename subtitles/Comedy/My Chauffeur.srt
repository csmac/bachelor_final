﻿1
00:00:53,200 --> 00:00:55,009
We said 5:50 pm

2
00:00:55,160 --> 00:00:57,845
The plane is twenty minutes late.

3
00:00:58,000 --> 00:00:59,240
- Hi.
- Hi.

4
00:01:00,320 --> 00:01:03,403
I have to pick up my test
results before 7 p.m.

5
00:01:03,560 --> 00:01:08,407
- Can you call me as soon
as you have them? - Yes, don't worry.

6
00:01:10,800 --> 00:01:12,882
Guess where I found it?

7
00:01:14,640 --> 00:01:17,883
- In the car?
- Yeah. In the glove compartment.

8
00:01:18,040 --> 00:01:21,283
I think that it climbed in, in the garage.
But why didn't it climb into Pawel's car?

9
00:01:21,440 --> 00:01:23,169
He always leaves lots
of filth lying around.

10
00:01:23,320 --> 00:01:26,130
Mice don't like clean places,
do they?

11
00:01:28,160 --> 00:01:30,766
- You killed it?
- What could I do?

12
00:01:30,920 --> 00:01:34,129
Just imagine the mess if
a client found it.

13
00:01:36,200 --> 00:01:38,521
Do something with your hair.

14
00:01:39,480 --> 00:01:42,370
You know, if l spoke
a little better English,

15
00:01:42,520 --> 00:01:46,286
- I would have taken this run, but...
- Don't worry.

16
00:01:46,440 --> 00:01:47,851
See you.

17
00:01:49,600 --> 00:01:52,331
Bye- Bye bye.

18
00:07:14,880 --> 00:07:15,847
Yes?

19
00:07:16,000 --> 00:07:19,322
<i>- It's in the centre. 7 Wilcza Street.
- Thank you.</i>

20
00:07:41,760 --> 00:07:45,651
<i>The presented solution
on the diagram</i> 6

21
00:07:45,800 --> 00:07:49,168
<i>ls based on the basic
generator piston movement</i>

22
00:07:49,320 --> 00:07:51,641
<i>with forced translation vibrations</i>

23
00:07:51,800 --> 00:07:55,964
<i>accompanied by the piston movement
is used to steer the flow...</i>

24
00:07:56,120 --> 00:07:58,600
<i>the section of the
cylindric piston surface</i>

25
00:07:58,760 --> 00:08:03,766
<i>with the sinusoidal
rings marked with the line...</i>

26
00:08:04,480 --> 00:08:05,641
Hello?

27
00:08:08,320 --> 00:08:10,926
- <i>Ania?</i>
- Yes. What did he say?

28
00:08:12,160 --> 00:08:15,448
Don't worry. Everything is OK.
It'll be alright.

29
00:08:17,040 --> 00:08:18,644
Will they operate?

30
00:08:18,800 --> 00:08:21,644
<i>No, he doesn't operate
,-'just got some pills.</i>

31
00:08:21,800 --> 00:08:23,848
I need to take medicine.

32
00:08:24,000 --> 00:08:25,240
<i>That's all?</i>

33
00:08:26,840 --> 00:08:30,890
He said it'll now be easy to cure.
It's better than we thought.

34
00:08:31,040 --> 00:08:33,122
Just medicine? That's all?

35
00:08:33,280 --> 00:08:35,169
<i>He'll explain it to you
better than me.</i>

36
00:08:35,320 --> 00:08:37,766
And we need to talk about
that medicine.

37
00:08:38,520 --> 00:08:41,126
Dad, my client is back,
I need to go. Bye.

38
00:10:14,080 --> 00:10:16,765
What are you doing here?
You were meant to be at the Marriot?

39
00:10:16,920 --> 00:10:19,924
- How do you know?
- Your dad told me.

40
00:10:21,760 --> 00:10:25,526
- Are you going or waiting?
- I've an hour or two.

41
00:10:26,800 --> 00:10:29,485
Can you smell anything in the car?

42
00:10:32,040 --> 00:10:37,763
Yeah, you smell pretty well. Do you want
to go to the bar? It's on me.

43
00:10:37,920 --> 00:10:40,764
I don't have time, I need to study.

44
00:12:08,240 --> 00:12:09,526
What happened?

45
00:15:25,400 --> 00:15:27,926
- What are you up to?
- I'm cleaning the car.

46
00:15:28,480 --> 00:15:31,848
- Straight from the Kitchen.
Have you eaten. - No.

47
00:15:32,720 --> 00:15:35,883
I only have 10 minutes
I'm with the Spaniards today.

48
00:15:36,040 --> 00:15:38,884
They are doing a discotheques tour.

49
00:15:39,040 --> 00:15:40,849
You speak any spanish?

50
00:15:41,480 --> 00:15:42,925
I manage.

51
00:15:43,360 --> 00:15:46,762
Have I told you I know Marco
He is the Chef here.

52
00:15:46,920 --> 00:15:48,649
Sorry I don't have time.

53
00:15:50,560 --> 00:15:52,130
Yeaah...

54
00:15:52,520 --> 00:15:54,761
It needs to be served
in the restaurant for you. Doesn't it ?

55
00:15:54,920 --> 00:15:56,922
I'm not hungry.

56
00:15:58,200 --> 00:16:00,362
You lost my number?

57
00:16:01,240 --> 00:16:03,242
No. I'll call.

58
00:16:05,960 --> 00:16:08,406
So we go to the restaurant, ok?

59
00:16:11,520 --> 00:16:14,126
- And?
- Can you talk?

60
00:16:14,280 --> 00:16:16,931
<i>What do you want to tell me about
the medicine.</i>

61
00:16:17,080 --> 00:16:19,686
Nothing, there are side effects.

62
00:16:21,200 --> 00:16:25,330
- I wouldn't be able to drive.
- For how <i>long?</i>

63
00:16:25,720 --> 00:16:29,645
I'll be taking them till the end
of my life.

64
00:16:31,200 --> 00:16:32,690
<i>I'm so sorry.</i>

65
00:17:00,680 --> 00:17:03,365
Alright, alright.
Good evening...

66
00:17:03,520 --> 00:17:05,761
- Ania?
- Good evening...

67
00:17:07,400 --> 00:17:11,325
- How's your father?
- He's ok. Thank you.

68
00:17:12,600 --> 00:17:16,844
- Have you been here a long time?
- Half an hour or so.

69
00:17:17,000 --> 00:17:19,287
I've got a client. I'm in a hurry.

70
00:17:19,840 --> 00:17:22,730
Have you seen anyone
suspicious down there?

71
00:17:22,880 --> 00:17:25,724
No, I saw a couple of hotel guests.

72
00:17:25,880 --> 00:17:28,929
What? Somebody stole a bath robe?

73
00:17:35,400 --> 00:17:39,405
- Somebody lost a diamond necklace?
- No.

74
00:17:40,800 --> 00:17:43,531
We have a dead man in there.

75
00:17:46,240 --> 00:17:48,129
Ok, you can go.

76
00:17:49,520 --> 00:17:52,000
Send your father my regards.

77
00:17:56,400 --> 00:17:57,606
I will. Bye.

78
00:19:49,400 --> 00:19:50,606
<i>I'm sorry.</i>

79
00:19:50,760 --> 00:19:52,524
It's not your fault.

80
00:19:52,680 --> 00:19:53,920
<i>But don't worry.</i>

81
00:19:54,080 --> 00:19:56,970
<i>I'll do everything I can to
help you finish your studies.</i>

82
00:19:57,120 --> 00:20:00,567
<i>I thought of employing someone,
you know...</i>

83
00:20:00,720 --> 00:20:02,722
Yes, to make us go bankrupt
in a month!

84
00:20:02,880 --> 00:20:05,201
<i>We need to pay off the car.</i>

85
00:20:06,280 --> 00:20:10,649
Dad, I can't talk right now,
I've got a client on hold. Bye.

86
00:22:34,040 --> 00:22:35,769
Yes, I am his driver.

87
00:22:36,560 --> 00:22:41,566
<i>Yeah right. I can't believe it.
Is it really your job?</i>

88
00:22:41,720 --> 00:22:44,724
Yes. Just a second, he is not
in the car. I'll call him.

89
00:22:44,880 --> 00:22:46,245
<i>There is no point.</i>

90
00:22:46,400 --> 00:22:49,609
Since he likes to leave messages,
then I have one for him,

91
00:22:49,760 --> 00:22:51,603
that's your job, right?

92
00:22:52,600 --> 00:22:55,285
Tell this asshole,
he didn't get it, ok...

93
00:22:55,440 --> 00:22:56,601
Wait, he's coming.

94
00:22:56,760 --> 00:23:00,082
I am not a whore and I don't want
to see his face ever again,

95
00:23:00,240 --> 00:23:02,208
tell him that now he is
to leave me alone...

96
00:26:19,200 --> 00:26:20,690
You're not asleep?

97
00:26:20,840 --> 00:26:24,845
<i>I took my pills. I can't sleep.
Are ok? You were at the Bristol?</i>

98
00:26:25,000 --> 00:26:27,844
- <i>How do you know?</i>
- <i>Modrak called.</i>

99
00:26:28,920 --> 00:26:31,844
- What <i>did</i> he want?
- <i>He asked ff! talked with you?</i>

100
00:26:32,000 --> 00:26:36,801
<i>- Did he tell you exactly what happened?
- He's looking for some guy.</i>

101
00:26:36,960 --> 00:26:39,201
Have you thought it over,
for the job?

102
00:26:40,560 --> 00:26:43,962
Listen dad, I don't know,
I can't give you an answer like this.

103
00:26:44,120 --> 00:26:47,761
I'm taking the exam next week.
I only did it for my studies.

104
00:26:47,920 --> 00:26:52,448
<i>You know what happens if we stop,
even for one day.</i>

105
00:26:54,280 --> 00:26:56,521
You know I hate doing it.
I hate these people.

106
00:26:56,680 --> 00:26:59,650
<i>I don't either. ,-'just love the oars
Just like you.</i>

107
00:26:59,800 --> 00:27:04,806
Dad, listen. If Modrak calls back, could
you ask him more about what happened?

108
00:27:04,960 --> 00:27:11,127
<i>- Why would he call back?
- He will. I have to go. Bye.</i>

109
00:28:29,720 --> 00:28:32,963
Good evening. You're not allowed
to park here.

110
00:28:33,120 --> 00:28:35,521
Alright, I'll pick up my client
and I'm off.

111
00:28:35,680 --> 00:28:36,886
Alright.

112
00:28:37,040 --> 00:28:39,884
- Good night.
- Good night.

113
00:33:35,600 --> 00:33:37,409
<i>Pa we! Wore/ski speaking.</i>

114
00:33:37,560 --> 00:33:42,441
It's me. I am calling you with
a favor. It's about an address.

115
00:33:42,600 --> 00:33:44,443
<i>Ania! Are you lost?</i>

116
00:33:44,600 --> 00:33:46,284
The client is looking for a hotel...

117
00:33:46,440 --> 00:33:49,091
With toilets... made of metal,
does that say anything to you?

118
00:33:49,240 --> 00:33:53,245
<i>- What's going on in those restrooms?
- Please, I'm in a hurry.</i>

119
00:33:53,400 --> 00:33:56,370
<i>Nope, doesn't ring</i> a <i>bell</i>

120
00:33:58,960 --> 00:34:01,406
- Who <i>is</i> that?
- Someone in the street.

121
00:34:01,560 --> 00:34:04,769
Doesn't matter, if anything comes
to mind, call me...

122
00:34:04,920 --> 00:34:08,561
<i>- I'll send you an sms.
- Thanks. 'til next time.</i>

123
00:34:08,720 --> 00:34:09,607
Goodnight.

124
00:39:39,320 --> 00:39:42,164
What an idiot!
What a fucking idiot!

125
00:40:05,680 --> 00:40:07,045
Speak, you idiot!

126
00:43:36,120 --> 00:43:38,885
It's Chlodna street
corner of Elektoralna.

127
00:48:04,200 --> 00:48:06,123
Stop the car.

128
00:48:06,840 --> 00:48:08,763
But we're almost there.

129
00:48:08,920 --> 00:48:10,888
Stop here, now.

130
00:48:33,920 --> 00:48:35,046
Asshole.

131
00:51:06,360 --> 00:51:09,091
I am sorry to disturb you,
my name is Ania.

132
00:51:09,240 --> 00:51:13,245
<i>- I am calling you on behalf of Sacha.
- I don't know any Sacha.</i>

133
00:51:13,400 --> 00:51:16,927
- This is the number he gave me.
- He's <i>still in</i> Po/and?

134
00:51:17,080 --> 00:51:19,321
He's got serious problems.
He needs help.

135
00:51:19,480 --> 00:51:20,891
<i>He means money.</i>

136
00:51:21,040 --> 00:51:23,088
<i>The rest, I don't see how he
will give it back...</i>

137
00:51:23,240 --> 00:51:27,609
<i>I helped him once already.
I'm not interested.</i>

138
00:52:43,680 --> 00:52:44,806
Hello?

139
00:52:44,960 --> 00:52:47,440
I am sorry. But he answered.

140
00:52:47,600 --> 00:52:50,968
He told me you are an excellent driver.
He's rather nice.

141
00:52:51,120 --> 00:52:55,523
- I don't think so.
- You were right. Modrak called back.

142
00:52:55,680 --> 00:52:57,569
He thinks you may help
his investigation.

143
00:52:57,720 --> 00:52:59,245
I don't see how.

144
00:52:59,400 --> 00:53:02,085
<i>They are still looking for the guy.</i>

145
00:53:02,240 --> 00:53:06,211
The client has been beaten to death;
he refused to open the safe.

146
00:53:10,040 --> 00:53:13,442
Go to bed, Dad. I will be late. Bye.

147
00:54:04,240 --> 00:54:05,321
Yes.

148
00:54:05,480 --> 00:54:09,166
<i>Good evening, this is Modrak.
Your father gave me the number.</i>

149
00:54:09,320 --> 00:54:11,288
<i>I am sorry to disturb you.</i>

150
00:54:11,440 --> 00:54:12,521
Sir!

151
00:54:12,680 --> 00:54:14,045
<i>What's going on?</i>

152
00:54:14,200 --> 00:54:15,326
No, nothing.

153
00:54:16,080 --> 00:54:18,970
<i>I need to see you.
It'll only be two minutes.</i>

154
00:54:19,120 --> 00:54:20,724
I am on duty...

155
00:54:21,400 --> 00:54:22,845
<i>Where are you?</i>

156
00:54:26,160 --> 00:54:29,448
We keep on moving.
I don't know where we'll be next.

157
00:54:29,600 --> 00:54:32,046
<i>What district? What's the address?</i>

158
00:54:32,200 --> 00:54:34,851
- Wola. Mlynarska.
- I'm coming.

159
00:57:28,960 --> 00:57:29,847
Yes.

160
00:57:30,000 --> 00:57:33,447
<i>Modrak here. I'm almost there,
where are you?</i>

161
00:57:34,240 --> 00:57:35,890
We just left.

162
00:57:36,320 --> 00:57:38,402
- Where are you?
- On Chiodna.

163
00:57:38,560 --> 00:57:42,281
<i>- Listen, I can't talk.
- I'm losing network.</i>

164
00:58:52,320 --> 00:58:55,403
- You know this place?
- Are your two Spaniards inside?

165
00:58:55,560 --> 00:58:59,201
Yes. They stay exactly one hour
at each place.

166
00:58:59,360 --> 00:59:01,840
- I'm free tomorrow.
- So am I.

167
00:59:02,440 --> 00:59:05,046
- We could go out to dinner?
- No...

168
00:59:05,200 --> 00:59:07,646
- It's on me.
- We'll split it.

169
00:59:07,800 --> 00:59:08,881
Deal.

170
00:59:09,040 --> 00:59:10,849
Do you have a cigarette?

171
00:59:27,160 --> 00:59:30,130
Maybe I'll go and get
a pack of lights.

172
01:00:07,360 --> 01:00:10,807
Excuse me for insisting somewhat,
but it's urgent.

173
01:00:10,960 --> 01:00:12,485
I'm on duty...

174
01:00:15,560 --> 01:00:17,483
I told you :
I came across a colleague.

175
01:00:17,640 --> 01:00:22,043
A security guard walked by.
And a couple. In their fifties.

176
01:00:22,200 --> 01:00:24,168
- Strange.
- Why?

177
01:00:24,320 --> 01:00:26,971
No, nothing, I'm thinking aloud.

178
01:00:40,920 --> 01:00:42,251
You're sure?

179
01:00:42,880 --> 01:00:46,601
I have an impression
I've seen him before.

180
01:00:46,760 --> 01:00:49,730
He's a pickpocket, not a criminal.

181
01:00:49,880 --> 01:00:51,564
Did you find him?

182
01:00:53,960 --> 01:00:56,611
He has no clothes.

183
01:00:56,760 --> 01:01:00,207
The police thinks he's still
in the hotel.

184
01:01:01,720 --> 01:01:05,520
I think he's outside. He left.

185
01:01:25,320 --> 01:01:28,563
- That's one hell of a car.
- It's dad's.

186
01:01:31,320 --> 01:01:33,288
How long do I know you?

187
01:01:33,440 --> 01:01:34,965
A few years now.

188
01:01:37,000 --> 01:01:40,925
Yeah. And by then,
you didn't drive cars...

189
01:01:42,280 --> 01:01:43,725
You stole them.

190
01:01:43,880 --> 01:01:46,281
Yeah. I was young.

191
01:01:47,560 --> 01:01:48,800
I helped you.

192
01:01:49,120 --> 01:01:51,930
- You helped my father.
- That's true.

193
01:01:52,080 --> 01:01:57,007
- And now, I could help you.
- That's good to know. Excuse me...

194
01:02:00,800 --> 01:02:02,768
I can't miss a thing.

195
01:02:11,800 --> 01:02:17,842
The police might get in touch with you
within the next couple of days. Call me.

196
01:02:26,760 --> 01:02:27,966
Thanks.

197
01:02:36,200 --> 01:02:41,081
He called me a while ago, too.
They are looking for some guy.

198
01:02:41,640 --> 01:02:43,165
Homicide.

199
01:02:44,520 --> 01:02:47,649
- Did he show you any photos?
- No.

200
01:02:50,360 --> 01:02:51,805
Fucking asshole...

201
01:02:54,080 --> 01:02:57,209
Calm down, leave him alone.
He's with me. Leave him alone.

202
01:12:17,680 --> 01:12:19,523
What's going on? Who are you?

203
01:12:19,680 --> 01:12:24,242
I'm sorry, it's a misunderstanding.
We're on our way. Sorry.

204
01:13:53,000 --> 01:13:54,161
Idiot.

205
01:21:41,600 --> 01:21:44,365
Do you have anything to say, boy?