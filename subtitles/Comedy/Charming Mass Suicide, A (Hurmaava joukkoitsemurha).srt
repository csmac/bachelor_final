1
00:00:00,040 --> 00:00:04,000
TRANSLATED BY:
{\fnAndalus\fs38\b1\c&H09ACFF&\3c&H000000&}||killer_sub||

1
00:00:04,220 --> 00:00:06,460
I need to know everything you know

2
00:00:06,460 --> 00:00:07,824
about the Winter Hill Gang

3
00:00:07,960 --> 00:00:10,660
and specifically what you know about

4
00:00:10,660 --> 00:00:13,660
your former boss, and now fugitive

5
00:00:13,875 --> 00:00:15,475
James "Whitey" Bulger.

6
00:00:18,145 --> 00:00:19,345
Well let's start.

7
00:00:23,308 --> 00:00:24,668
In the beginning...

8
00:00:24,825 --> 00:00:26,665
Jim was a small town player.

9
00:00:27,848 --> 00:00:28,968
He's a very smart

10
00:00:28,968 --> 00:00:30,228
disciplined man.

11
00:00:30,623 --> 00:00:31,663
Take your shot.

12
00:00:31,792 --> 00:00:33,152
But make it your best,

13
00:00:33,152 --> 00:00:34,703
cause I get up, I eatcha.

14
00:00:36,140 --> 00:00:38,120
Then the next thing you know he's a damn kingpin!

15
00:00:38,741 --> 00:00:39,621
You know why?

16
00:00:39,640 --> 00:00:41,640
Because the FBI let it happen.

17
00:00:41,640 --> 00:00:42,640
I grew up with Jimmy

18
00:00:42,640 --> 00:00:44,460
and his brother Billy, the senator.

19
00:00:44,460 --> 00:00:46,780
And that is a bond that doesn't get broken.

20
00:00:46,780 --> 00:00:49,746
Your brother is wading into some very dark waters...

21
00:00:49,746 --> 00:00:52,042
Jimmy's business, is Jimmy's business.

22
00:00:52,042 --> 00:00:53,193
We all need friends.

23
00:00:53,883 --> 00:00:54,683
Even Jimmy.

24
00:00:55,420 --> 00:00:56,060
Even you.

25
00:00:59,526 --> 00:01:00,486
I can help you.

26
00:01:00,568 --> 00:01:02,328
Jimmy, and you can help me.

27
00:01:02,454 --> 00:01:03,654
It's an alliance.

28
00:01:05,560 --> 00:01:07,880
We'll get the FBI to fight our wars.

29
00:01:07,880 --> 00:01:09,479
And we do whatever we wanna do!

30
00:01:11,959 --> 00:01:12,759
To success.

31
00:01:14,639 --> 00:01:16,159
Just gettin' started.

32
00:01:24,320 --> 00:01:24,820
Boom!

33
00:01:29,199 --> 00:01:30,639
Bulger's playing us.

34
00:01:31,240 --> 00:01:32,580
Makin' a fool of the bureau...

35
00:01:32,786 --> 00:01:33,986
We're in too deep!

36
00:01:34,920 --> 00:01:35,960
And he knows it!

37
00:01:38,480 --> 00:01:39,280
God help us.

38
00:01:39,800 --> 00:01:41,760
How come no one has nailed Whitey Bulger?

39
00:01:41,760 --> 00:01:44,656
He seems to be involved in every crime in the city

40
00:01:44,656 --> 00:01:46,672
and yet the Bureau keeps sayin' he's clean.

41
00:01:46,672 --> 00:01:47,977
Well, what's Bulger DONE?

42
00:01:48,592 --> 00:01:49,632
What's he done?

43
00:01:50,780 --> 00:01:51,660
Everything!

44
00:01:58,310 --> 00:01:59,990
You cannot say that to me!

45
00:02:02,620 --> 00:02:04,220
You're changin', John.

46
00:02:10,239 --> 00:02:11,759
If he gives me his word?

47
00:02:12,040 --> 00:02:13,080
He will keep it.

