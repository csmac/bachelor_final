1
00:00:12,000 --> 00:00:16,920
For a year Tahrir Square has been
at the heart of a tumultuous
struggle for freedom.

2
00:00:18,360 --> 00:00:22,640
Last February, people from all over
Egypt united in their desire

3
00:00:22,640 --> 00:00:25,880
to bring down a dictator
and build a new country.

4
00:00:30,240 --> 00:00:34,280
This World has followed
three young revolutionaries.

5
00:00:34,280 --> 00:00:36,960
Ahmed's came from Cairo's
backstreets to fight for an Egypt

6
00:00:36,960 --> 00:00:40,520
where he can find work.

7
00:00:40,520 --> 00:00:43,320
For Gigi, it is a battle
for freedom.

8
00:00:45,240 --> 00:00:48,800
While Tahir wants to build
an Islamic state.

9
00:00:50,840 --> 00:00:52,760
GUN SHOTS

10
00:00:52,760 --> 00:00:57,120
Some dreams have been crushed.

11
00:00:57,120 --> 00:00:59,600
Others have come closer.

12
00:01:01,440 --> 00:01:05,440
It was a year which has
divided families.

13
00:01:08,520 --> 00:01:12,440
And a year that risked
dividing the nation.

14
00:01:16,160 --> 00:01:19,160
Once just another roundabout
in Cairo,

15
00:01:19,160 --> 00:01:23,320
Tahrir Square became
the crucible of the Arab Spring.

16
00:01:24,400 --> 00:01:28,320
Could Egypt become the model
for an Islamic democracy
in the Middle East?

17
00:01:28,320 --> 00:01:33,720
Or would the dreams evaporate
into intolerance
and another dictatorship?

18
00:02:07,640 --> 00:02:10,800
The battle begins on Police Day,
25th January, 2011.

19
00:02:12,920 --> 00:02:16,240
Inspired by the successful
uprising in Tunisia,

20
00:02:16,240 --> 00:02:19,880
unofficial opposition groups
called for a demonstration

21
00:02:19,880 --> 00:02:22,680
against corruption
and police brutality.

22
00:02:22,680 --> 00:02:26,560
The 25th January, I remember that
the first thing I did that day,

23
00:02:26,560 --> 00:02:29,400
I went to get another battery
for my BlackBerry.

24
00:02:31,520 --> 00:02:35,880
At 24, student, Gigi Ibrahim,
is a veteran of illegal protest.

25
00:02:37,520 --> 00:02:42,600
Going into residential areas
for people to join us.

26
00:02:42,600 --> 00:02:48,120
We started being tens, then grew into
hundreds and grew into thousands.

27
00:02:48,120 --> 00:02:52,000
But I didn't realize the impact
until I reached Tahrir.

28
00:02:52,000 --> 00:02:55,560
CHANTING

29
00:03:01,080 --> 00:03:02,480
I was so happy.

30
00:03:02,480 --> 00:03:07,160
I have this clip on video,
the exact moment

31
00:03:07,160 --> 00:03:08,960
I knew this was a revolution.

32
00:03:08,960 --> 00:03:12,640
We were in Tahrir and a huge wave

33
00:03:12,640 --> 00:03:17,920
of central security officers
started to run.

34
00:03:17,920 --> 00:03:22,200
The people first ran
as the normal reaction.

35
00:03:22,200 --> 00:03:25,360
But then they stopped,
some people started attacking.

36
00:03:25,360 --> 00:03:29,800
And then a wave of protestors
ran after the officers.

37
00:03:29,800 --> 00:03:34,120
And the officers actually ran away.

38
00:03:34,120 --> 00:03:37,440
SHE SCREAMS

39
00:03:41,320 --> 00:03:46,440
I'm like, "Let's go! We've waited
30 years for this!"

40
00:03:46,440 --> 00:03:54,080
This is the moment I physically saw
people not being afraid and running
after officers.

41
00:03:54,080 --> 00:03:57,520
SHE SHOUTS

42
00:04:03,840 --> 00:04:08,760
That night the thousands become
tens of thousands who begin

43
00:04:08,760 --> 00:04:10,920
to camp out in the square.

44
00:04:22,520 --> 00:04:24,880
Ahmed Hassan joins
the demonstration,

45
00:04:24,880 --> 00:04:29,600
angry after years of suffering
every day humiliations
at the hands of the state.

46
00:05:15,160 --> 00:05:19,280
Over the next 17 days,
as the confrontation escalates,

47
00:05:19,280 --> 00:05:21,680
hundreds are killed.

48
00:05:30,320 --> 00:05:34,400
Instead of leaving, the crowd grows.

49
00:05:34,400 --> 00:05:39,920
Ahmed is still there on February
10th and watches on
an improvised screen

50
00:05:39,920 --> 00:05:43,560
as President Mubarak appears on TV
to calm his people.

51
00:05:59,840 --> 00:06:05,320
The vast crowd, now made up of
every strand of Egyptian society,

52
00:06:05,320 --> 00:06:08,360
are united in their response,
waving their shoes,

53
00:06:08,360 --> 00:06:11,320
the ultimate sign of disrespect
in the Arab world.

54
00:06:29,680 --> 00:06:34,120
That night, the army, for years
a mainstay of the Mubarak regime,

55
00:06:34,120 --> 00:06:36,000
takes control.

56
00:06:36,000 --> 00:06:39,320
At 6pm, a general arrives
at the state TV building

57
00:06:39,320 --> 00:06:42,120
with a recorded message
from the vice president.

58
00:06:43,480 --> 00:06:45,760
He orders it to be broadcast
immediately.

59
00:07:06,040 --> 00:07:10,400
THEY ALL SHOUT OUT

60
00:07:10,400 --> 00:07:13,760
SHE LAUGHS WITH JOY

61
00:07:13,760 --> 00:07:19,440
THEY CHANT: Finish, Mubarak.
Finish, Mubarak. Finish, Mubarak!

62
00:07:21,920 --> 00:07:23,600
Finish, Mubarak!

63
00:07:30,120 --> 00:07:32,880
The army joins the protesters
on the square,

64
00:07:32,880 --> 00:07:36,360
promising to oversee a transition
to a democratic Egypt.

65
00:07:45,840 --> 00:07:49,280
The morning after, Gigi and fellow
revolutionaries celebrate

66
00:07:49,280 --> 00:07:51,600
by clearing up their mess.

67
00:07:51,600 --> 00:07:54,720
There is a sense
that a new era has begun.

68
00:07:56,160 --> 00:07:58,600
My dad, my dad. Hello.

69
00:08:09,280 --> 00:08:13,440
He's asking about what happens next.
He's asking me!

70
00:08:13,440 --> 00:08:16,120
My dad asking me. That never happens!

71
00:08:16,120 --> 00:08:18,720
Life has to go back to normal.

72
00:08:18,720 --> 00:08:21,800
Like, people have got
to go back to work.

73
00:08:21,800 --> 00:08:24,640
Now, we fight,

74
00:08:24,640 --> 00:08:28,480
we fight legitimately
for free and fair elections

75
00:08:28,480 --> 00:08:33,040
where the people are going
to choose democratically
for the first time ever.

76
00:08:33,040 --> 00:08:38,160
Now, the real hard work starts.
Really. It begins now.

77
00:08:47,960 --> 00:08:49,840
Across Cairo,

78
00:08:49,840 --> 00:08:52,240
Ahmed returns home
to the slum of Shubra,

79
00:08:52,240 --> 00:08:54,480
where unemployment runs at over 30%.

80
00:09:01,480 --> 00:09:05,440
He lives with his widowed mother,
Awatef, and younger brother, Amrr.

81
00:09:08,120 --> 00:09:10,920
They are glued to
the new debate shows on TV.

82
00:09:41,600 --> 00:09:45,040
Ahmed is convinced that
his life has changed for the better.

83
00:09:47,520 --> 00:09:50,480
TRANSLATION: God willing
I will find work.

84
00:09:50,480 --> 00:09:54,040
Now, after an employer finds out
that I was part of the revolution,

85
00:09:54,040 --> 00:09:58,280
he will never treat me badly
like before. Impossible.

86
00:10:05,240 --> 00:10:07,680
At his barber shop,
Ahmed is the local hero.

87
00:10:31,240 --> 00:10:36,320
They are conscious
the world has been watching.

88
00:11:03,480 --> 00:11:04,720
Every day at dawn,

89
00:11:04,720 --> 00:11:08,880
Ahmed's mother Awatef gets up
to purchase the vegetables
for her market stall.

90
00:11:10,400 --> 00:11:12,800
Not everyone supports
the revolution.

91
00:11:46,080 --> 00:11:49,920
Under Mubarak, the police
were seen as a constant menace.

92
00:11:49,920 --> 00:11:53,280
Harassment of a market trader had
sparked the revolution in Tunisia.

93
00:11:53,280 --> 00:11:58,920
TRANSLATION: That boy set fire
to himself. You'd never catch me
doing that. Poor kid!

94
00:12:01,560 --> 00:12:04,600
Government trucks would clear
the street by force.

95
00:12:10,120 --> 00:12:13,160
My friend, the baker,
suffered more than any of us.

96
00:12:13,160 --> 00:12:15,000
Since the day he moved in
by my stall,

97
00:12:15,000 --> 00:12:18,240
he's been dragged
to and from police station.

98
00:12:18,240 --> 00:12:20,840
They wouldn't let him go
till they took a �100 bribe.

99
00:12:22,480 --> 00:12:27,760
No-one dared to speak,
or they'd get beaten and get taken
to the police station too.

100
00:12:27,760 --> 00:12:29,600
That's how we used to live.

101
00:12:34,040 --> 00:12:37,320
This is the Nile River. Smelly.

102
00:12:37,320 --> 00:12:39,360
SHE LAUGHS

103
00:12:39,360 --> 00:12:42,080
I wish I had more time to come here
more often.

104
00:12:42,080 --> 00:12:45,640
My dad is here every single week.

105
00:12:45,640 --> 00:12:47,840
Gigi inhabits a different world.

106
00:12:47,840 --> 00:12:51,960
She is the American-educated
daughter of a wealthy industrialist.

107
00:12:51,960 --> 00:12:53,560
My dad is a business man, you know.

108
00:12:53,560 --> 00:12:57,440
He has benefited but, at the same
time, in the past few years

109
00:12:57,440 --> 00:13:00,320
he really has suffered also
from the regime.

110
00:13:08,000 --> 00:13:11,080
SHE LAUGHS
AND GREETS HER FAMILY

111
00:13:14,520 --> 00:13:16,320
She let me feel I am a failure.
GIGI: No!

112
00:13:16,320 --> 00:13:19,120
Honest, true. After this revolution,
how come we stand?

113
00:13:19,120 --> 00:13:21,600
I'm 55, 56 years old now.

114
00:13:21,600 --> 00:13:24,760
I stay with the Mubarak regime
for 30 years

115
00:13:24,760 --> 00:13:28,600
and I didn't even think,
think even to change.

116
00:13:28,600 --> 00:13:32,520
How many generations were useless
not to stand and say no?

117
00:13:32,520 --> 00:13:35,800
This is what I feel. I feel bad.

118
00:13:35,800 --> 00:13:39,160
Not everyone buys into the euphoria.

119
00:13:39,160 --> 00:13:41,920
Gigi's aunt and sister are worried.

120
00:13:41,920 --> 00:13:44,080
THEY ARGUE

121
00:14:28,760 --> 00:14:34,160
Before the revolution, religious
groups were ruthlessly suppressed.

122
00:14:34,160 --> 00:14:38,400
The only real opposition to Mubarak
was the illegal Muslim Brotherhood.

123
00:14:38,400 --> 00:14:42,200
Now they are in the open,
along with other Islamic groups

124
00:14:42,200 --> 00:14:44,640
such as the ultra-conservative
Salafis.

125
00:14:46,360 --> 00:14:50,080
24-year-old Tahir Yasin is a Salafi.

126
00:14:50,080 --> 00:14:52,400
HE SINGS TO HIMSELF

127
00:14:54,080 --> 00:14:57,240
He has been in prison seven times
from the age of 16

128
00:14:57,240 --> 00:14:59,240
for organising Qu'ran classes.

129
00:15:04,600 --> 00:15:08,280
For the first time in his life,
he is free to preach as he wishes.

130
00:16:10,400 --> 00:16:14,760
Tahir was held here, in
the state security prison at Giza.

131
00:16:14,760 --> 00:16:19,920
He claims that, like thousands of
others, he was routinely tortured.

132
00:17:06,680 --> 00:17:11,720
Tahir comes from a religious family
of teachers and scholars, Salafis,

133
00:17:11,720 --> 00:17:15,480
inspired by the Saudi Arabian model
of Islam.

134
00:17:46,120 --> 00:17:52,120
For Tahir, his family and friends,
the Mubarak regime had sold out
to the west.

135
00:17:52,120 --> 00:17:55,000
They were all imprisoned
for their beliefs.

136
00:18:43,400 --> 00:18:47,640
The fall of Mubarak after 30 years
has unsettled the whole country.

137
00:18:47,640 --> 00:18:52,480
The head of the new military
council, General Tantawi,
appeals for order.

138
00:19:12,200 --> 00:19:16,040
Gigi's father owns a clothing
factory on the outskirts of Cairo.

139
00:19:17,320 --> 00:19:21,040
Since the revolution, his workers,
like many all over Egypt,

140
00:19:21,040 --> 00:19:24,720
are demanding more rights
and better pay.

141
00:19:27,000 --> 00:19:29,040
He can't even visit his own factory.

142
00:19:31,680 --> 00:19:35,800
I don't go to the factory
because, if I go,

143
00:19:35,800 --> 00:19:38,800
maybe 80%, 70% will strike.

144
00:19:38,800 --> 00:19:42,360
If the workers stop working,
it's a disaster.

145
00:19:42,360 --> 00:19:46,320
Everyone thinks the new revolution,
that means, I'm getting a salary now

146
00:19:46,320 --> 00:19:50,320
1,000, I'll get 5,000.
This is what you call freedom!

147
00:19:51,800 --> 00:19:55,040
Gigi went to my factory and
she turned the people against me.

148
00:19:55,040 --> 00:19:58,880
My workers, turning against me
because of Gigi. They tell me,
"Ask your daughter!"

149
00:19:58,880 --> 00:20:02,560
I think she's a Communist.

150
00:20:05,760 --> 00:20:08,560
Gigi and her comrades
are using social networking

151
00:20:08,560 --> 00:20:11,920
to agitate across the country
and organise strikes.

152
00:20:17,200 --> 00:20:21,240
This is how exactly I would have
expected our revolution to go.

153
00:20:21,240 --> 00:20:26,320
There is a striking school
at least in every governance,

154
00:20:26,320 --> 00:20:28,600
doctors are also striking.

155
00:20:28,600 --> 00:20:32,040
The fall of Mubarak
is just the beginning.

156
00:20:32,040 --> 00:20:35,880
Gigi wants to destroy the old system
that made her father.

157
00:20:35,880 --> 00:20:38,640
This is the disagreement
that I've had with my dad.

158
00:20:38,640 --> 00:20:43,400
It's like, he wanted to engage
in the corrupted system

159
00:20:43,400 --> 00:20:46,600
to make his life and his family's
life better.

160
00:20:46,600 --> 00:20:48,600
Why am I OK with this?

161
00:21:12,840 --> 00:21:15,720
The revolution has done nothing
to help Ahmed.

162
00:21:15,720 --> 00:21:18,320
Even though he has a diploma
in telecommunications,

163
00:21:18,320 --> 00:21:20,200
he is still looking for work.

164
00:21:27,880 --> 00:21:32,040
TRANSLATION: When I go for jobs
that suit my qualifications,

165
00:21:32,040 --> 00:21:34,960
they only take people who bribe them
or who have contacts.

166
00:21:37,000 --> 00:21:40,720
I think it's high time for me
to repay my mother for everything.

167
00:21:48,240 --> 00:21:53,400
As a widow, Awatef put her sons
through college herself.

168
00:21:54,920 --> 00:22:01,080
TRANSLATION: So many times I haven't
eaten so I can feed my kids.

169
00:22:01,080 --> 00:22:04,760
I lay on the bed all night
with a stomach ache so painful
I couldn't sleep.

170
00:22:15,320 --> 00:22:19,280
Mubarak did nothing for his people.

171
00:22:19,280 --> 00:22:21,920
He never felt anything
for the suffering of the people.

172
00:22:21,920 --> 00:22:25,520
There are so many
much poorer than me.

173
00:22:30,040 --> 00:22:34,720
Despite his diploma,
the only work Ahmed has been offered
is in a clothing factory.

174
00:23:04,480 --> 00:23:07,240
TRANSLATION: He's scared of me

175
00:23:07,240 --> 00:23:11,360
because I was part of the revolution
that toppled the Mubarak regime.

176
00:23:13,920 --> 00:23:16,920
The revolutionaries are
the bravest men in Egypt.

177
00:23:19,840 --> 00:23:23,640
Ahmed decides to rejoin the
revolutionaries on Tahrir Square.

178
00:24:23,040 --> 00:24:26,080
MAN SPEAKS THROUGH MEGAPHONE

179
00:24:32,760 --> 00:24:34,880
For so long driven underground,

180
00:24:34,880 --> 00:24:39,160
politics is now on the streets
all over the country.

181
00:24:39,160 --> 00:24:45,280
Tahir has joined the new
Party of Light, born out of
his conservative Salafi movement.

182
00:25:13,960 --> 00:25:17,920
Under Mubarak the Salafis
operated as a secret charity.

183
00:25:26,360 --> 00:25:28,800
Now they work from district
to district,

184
00:25:28,800 --> 00:25:32,520
openly using donations to win over
possible supporters.

185
00:25:41,000 --> 00:25:45,720
Accompanied by one of his
party leaders, the purpose is
to get noticed.

186
00:26:09,400 --> 00:26:14,680
Tahir relishes the competition
with more established groups
like the Muslim Brotherhood.

187
00:26:44,840 --> 00:26:47,560
MAN LEADS CHANTING WITH LOUDSPEAKER

188
00:26:50,640 --> 00:26:55,800
On the weekend of July 28th,
the Salafis and the
Muslim Brotherhood come together

189
00:26:55,800 --> 00:26:59,840
to put on a show of force
and claim the revolution for Islam.

190
00:27:02,040 --> 00:27:07,680
Ahmed is concerned that
the Islamists have a different
agenda from the revolutionaries.

191
00:27:07,680 --> 00:27:11,560
By chance, he runs into Tahir.

192
00:27:59,720 --> 00:28:03,080
MAN LEADS CROWD
IN SHOUTS OF "ALLAHU AKBAR"

193
00:28:03,080 --> 00:28:07,600
Speakers call on the military to
fulfil their promise of elections

194
00:28:07,600 --> 00:28:09,360
and to try Mubarak.

195
00:29:01,680 --> 00:29:08,120
The next day, Ahmed's mother,
Awatef, sets out to fight.

196
00:29:12,240 --> 00:29:15,040
The Islamists have taken over
the square.

197
00:29:23,400 --> 00:29:26,360
She discovers that women no longer
have a place there.

198
00:29:36,000 --> 00:29:38,560
As she tries to make her way
through, the men object.

199
00:29:50,200 --> 00:29:52,800
Finally she has to meet Ahmed
on the edge of the square,

200
00:29:52,800 --> 00:29:55,480
but her problems are not over.

201
00:30:16,280 --> 00:30:18,760
SHE SPEAKS ARABIC

202
00:30:18,760 --> 00:30:23,680
TRANSLATION: "Why are you sitting on
the ground? You look like beggars."

203
00:30:23,680 --> 00:30:27,600
I was upset when he humiliated me.

204
00:30:27,600 --> 00:30:31,080
They were like Iranians,
not Egyptians.

205
00:30:31,080 --> 00:30:36,040
To be honest, I don't know anything
about Iran, but I hear they're evil.

206
00:30:36,040 --> 00:30:39,360
They don't act like normal people.
I don't like it.

207
00:30:41,160 --> 00:30:44,360
MUEZZIN CALLS

208
00:30:44,360 --> 00:30:49,120
If they come to power, will they let
women work?

209
00:30:49,120 --> 00:30:52,560
Will they give me handouts
so I can sit at home,

210
00:30:52,560 --> 00:30:56,200
or are they going to
make mincemeat of me for working?

211
00:31:07,200 --> 00:31:09,440
Within days of the demonstration,

212
00:31:09,440 --> 00:31:12,280
the military high command
fulfil one promise.

213
00:31:12,280 --> 00:31:17,240
They deliver Hosni Mubarak,
their former commander in chief,
for trial -

214
00:31:17,240 --> 00:31:22,240
the man who for 30 years had
been seen as crucial for maintaining
peace in the Middle East.

215
00:31:22,240 --> 00:31:27,560
He is accused of ordering
the killing of the protesters
in January.

216
00:31:27,560 --> 00:31:30,320
THEY CHANT

217
00:31:43,640 --> 00:31:48,160
Tahir watches the trial
with his mother and brother-in-law,

218
00:31:48,160 --> 00:31:50,360
along with the whole nation.

219
00:32:19,920 --> 00:32:24,800
Watching this makes me remember
all the chants, all the chants.

220
00:32:24,800 --> 00:32:28,640
"We will put you in the cage
when the revolution will come."

221
00:32:28,640 --> 00:32:32,680
It's like all of our hard work
are finally paying off.

222
00:32:32,680 --> 00:32:35,920
SHE SPEAKS ARABIC

223
00:32:40,600 --> 00:32:42,280
It's good.

224
00:32:43,840 --> 00:32:46,040
No, this is too fattening.

225
00:32:46,040 --> 00:32:51,200
Back at her sister's,
Gigi finds that even her aunt
has changed her tune.

226
00:33:01,560 --> 00:33:03,560
Success!

227
00:33:31,240 --> 00:33:37,120
At the same as putting Mubarak
on trial, the military
clear Tahrir Square of protesters

228
00:33:37,120 --> 00:33:39,560
and clamp down
on any form of dissent.

229
00:33:39,560 --> 00:33:42,640
Over 12,000 civilians have been
tried in military courts.

230
00:33:42,640 --> 00:33:46,640
Gigi has been briefly detained
by the military for filming
at a strike.

231
00:33:46,640 --> 00:33:51,320
As soon as she's released,
she's back in front of
the military courts,

232
00:33:51,320 --> 00:33:53,720
tweeting to her
30,000 followers.

233
00:33:53,720 --> 00:33:59,720
He was yelling and he was just like,
"I'm gonna kill you," and I'm
like... I'm looking at him

234
00:33:59,720 --> 00:34:05,560
and I'm just like, "I wanna know
why am I arrested, I need
a lawyer, don't touch my stuff."

235
00:34:05,560 --> 00:34:10,000
If you show them that you are weak
or afraid, they really crack on you,

236
00:34:10,000 --> 00:34:13,480
so I knew that I have
to hold it together.

237
00:34:13,480 --> 00:34:17,680
Filming on her phone, she and
a friend confront an army officer.

238
00:35:13,400 --> 00:35:16,960
Back home, Gigi's father is worried
for his daughter.

239
00:35:29,000 --> 00:35:31,320
Here is a good one.

240
00:35:31,320 --> 00:35:34,520
You don't know the bad side
of the country.

241
00:35:34,520 --> 00:35:40,000
You don't know the ugly face
of the army... Luckily... ..and
the ugly face of the government.

242
00:35:40,000 --> 00:35:43,040
She want it...quick.
This is the youth people.

243
00:35:43,040 --> 00:35:48,080
The youth, they are always...hurry.
They are always... They want it fast.

244
00:35:48,080 --> 00:35:52,320
This democratic, it take time.
You have to teach the people
how to think.

245
00:35:52,320 --> 00:35:56,160
He now wants the army
to remain in power.

246
00:35:56,160 --> 00:35:59,280
Something is going on,
we don't understand it really.

247
00:35:59,280 --> 00:36:01,280
It's not clear yet to everybody.

248
00:36:01,280 --> 00:36:06,400
But... It's the army trying to stay
in power. Well...and the army
HAVE to stay in power.

249
00:36:06,400 --> 00:36:08,560
Because we don't know the democracy.

250
00:36:08,560 --> 00:36:11,880
This is the first to time
to feel free, to feel this...

251
00:36:11,880 --> 00:36:13,880
SHE SPEAKS ARABIC
"Freedom"!

252
00:36:19,600 --> 00:36:25,520
Politically we just...arghh!..
completely disagree on everything.

253
00:36:25,520 --> 00:36:28,560
I mean, you've heard... It's...

254
00:36:28,560 --> 00:36:31,600
It's very difficult to talk about
it with him,

255
00:36:31,600 --> 00:36:34,840
cos it's very personal to me,
and it's my dad...

256
00:36:34,840 --> 00:36:38,280
'If I look at him in a political way
it will be very, very conflicting,

257
00:36:38,280 --> 00:36:42,760
'to the point that I would
have to choose,

258
00:36:42,760 --> 00:36:45,960
'and I never want to put myself
in this position.

259
00:36:45,960 --> 00:36:51,040
'If at some point I have to
choose...like...I might just not
choose my dad.'

260
00:36:58,480 --> 00:37:01,240
On September 26th,
the military council

261
00:37:01,240 --> 00:37:07,560
finally announce parliamentary
elections for the end of the year.

262
00:37:07,560 --> 00:37:15,000
All the polls point to a win
for the Muslim Brotherhood, who have
a presence all over the country.

263
00:37:17,360 --> 00:37:23,360
For Tahir's Salafi party, the
campaign is an opportunity to spread
their more conservative message.

264
00:37:26,480 --> 00:37:30,480
He is proud of their new
election posters.

265
00:38:03,000 --> 00:38:08,440
The Salafi election advert confronts
popular fears of their extremism.

266
00:38:51,760 --> 00:38:55,480
The message has a strong appeal
in the conservative countryside.

267
00:38:55,480 --> 00:39:00,280
Tahir travels out of Cairo
to secure the support of
an old friend and village mayor,

268
00:39:00,280 --> 00:39:03,280
Muhammed Abdul Sumed.

269
00:39:07,440 --> 00:39:11,440
Tahir invites his friend to
his wedding at the end of the year.

270
00:39:26,400 --> 00:39:27,680
I wait number four!

271
00:39:30,440 --> 00:39:35,000
Muhammed has been in power
for as long as Mubarak.

272
00:39:53,280 --> 00:39:57,480
This, to Tahir, is Salafi Egypt.

273
00:40:31,000 --> 00:40:34,320
October 6th is Army Day,
Egypt's annual holiday,

274
00:40:34,320 --> 00:40:39,480
which this year would dissolve
into violence that threatened
to derail the elections.

275
00:40:47,040 --> 00:40:49,720
As usual, the holiday
begins with celebrations

276
00:40:49,720 --> 00:40:53,400
of the army's victory
over Israel in 1973.

277
00:41:02,560 --> 00:41:06,200
TRANSLATION: I pray that I can
join the army.

278
00:41:06,200 --> 00:41:09,440
Maybe there are people
waiting for someone to say no,

279
00:41:09,440 --> 00:41:12,160
and we can all say no together.

280
00:41:14,520 --> 00:41:17,400
# Get in, just get in

281
00:41:17,400 --> 00:41:19,400
# Check out the trouble
we're in... #

282
00:41:19,400 --> 00:41:23,280
HARD ROCK PLAYS

283
00:41:23,280 --> 00:41:27,120
The holiday gives Gigi the chance
of a weekend on the Red Sea.

284
00:41:56,160 --> 00:41:58,600
Ohhh! Look at that!

285
00:41:58,600 --> 00:42:01,480
Oh, it's so beautiful.

286
00:42:01,480 --> 00:42:08,760
Maybe, maybe, maybe, off record, the
Salafi guys and all his family won't
be happy seeing this part of Gigi!

287
00:42:08,760 --> 00:42:12,440
That's never gonna happen,
they're never gonna ban the bikini.

288
00:42:12,440 --> 00:42:17,480
This is like...suppression of...
you know...freedom of expression!

289
00:42:19,160 --> 00:42:23,800
INTERVIEWER:
If the Salafis get power,
would you regret the revolution?

290
00:42:23,800 --> 00:42:27,240
It's not going to happen.

291
00:42:27,240 --> 00:42:31,320
I really, really, really believe
that it will never happen in Egypt.

292
00:42:31,320 --> 00:42:36,200
We will never be another Iran, where
there is an Islamic government,

293
00:42:36,200 --> 00:42:41,360
or the Salafis would get in power
or the Muslim Brotherhood
would get in power,

294
00:42:41,360 --> 00:42:44,080
because...
it just does not work in Egypt.

295
00:42:46,160 --> 00:42:52,000
Back in Cairo, a protest by
the Christian minority
against discrimination

296
00:42:52,000 --> 00:42:56,000
is broken up by soldiers
guarding the state TV station.

297
00:42:56,000 --> 00:43:00,600
It turns into the most
serious violence
since the January revolution.

298
00:43:17,640 --> 00:43:21,080
The authorities clamp down
on reporting.

299
00:43:21,080 --> 00:43:26,360
Independent TV stations are
broadcasting live clashes between
the demonstrators and the army

300
00:43:26,360 --> 00:43:30,000
when soldiers appear in the studio
to take them off air.

301
00:43:39,760 --> 00:43:43,240
State television appeals
to the Muslim majority,

302
00:43:43,240 --> 00:43:48,760
accusing the Christians of setting
out to destabilise the country.

303
00:44:12,680 --> 00:44:16,320
Friends call Ahmed to come down
to the demonstration.

304
00:44:16,320 --> 00:44:19,960
Using his own camera, he decides
to take on the role of reporter.

305
00:44:35,400 --> 00:44:38,040
Gigi is stranded at the beach.

306
00:44:49,640 --> 00:44:54,720
She is told the army
and Islamic groups are forming
an alliance to crush protest.

307
00:44:56,880 --> 00:45:04,080
Something seems to happen and people
are chanting, "Islam and the army
are one hand."

308
00:45:04,080 --> 00:45:09,360
From Democracy Now -
"Egyptian state TV is completely
distorting tonight's events,

309
00:45:09,360 --> 00:45:14,360
"airing interviews of soldiers
saying the Christians began
by beating and shooting them."

310
00:45:16,240 --> 00:45:19,240
27 protesters die.

311
00:45:19,240 --> 00:45:22,560
The authorities accuse
"invisible hands" for the deaths -

312
00:45:22,560 --> 00:45:27,600
until footage appears online
showing army vehicles
running over protestors.

313
00:45:42,040 --> 00:45:45,800
The dead are brought to the morgue
of the local hospital.

314
00:45:45,800 --> 00:45:51,360
Among them is one of
Ahmed's fellow fighters
from the January revolution.

315
00:46:18,840 --> 00:46:23,840
Another of his friends is injured.
They both feel the army
has betrayed them.

316
00:46:50,280 --> 00:46:52,720
CROWD CHANTS

317
00:46:58,880 --> 00:47:05,920
The military continue to blame the
protesters for the violence that is
putting the elections at risk.

318
00:47:05,920 --> 00:47:10,200
BOY RECITES VERSES

319
00:47:14,360 --> 00:47:17,640
At his weekly Qu'ran class,
Tahir is clear who is at fault.

320
00:48:16,400 --> 00:48:19,880
The demonstrators are now
labelled as enemies.

321
00:48:19,880 --> 00:48:22,120
When Ahmed returns to the barbers,

322
00:48:22,120 --> 00:48:26,680
his friend Imad from the Muslim
Brotherhood openly challenges him.

323
00:49:12,040 --> 00:49:14,280
ALL: Salaam.

324
00:49:50,680 --> 00:49:54,720
Find people, prepare the place,
get the car,

325
00:49:54,720 --> 00:49:58,000
get my suit, get her dress,
get the...

326
00:49:58,000 --> 00:50:01,720
Tahir has both an election
and a wedding to organise.

327
00:50:01,720 --> 00:50:03,840
Where are you?

328
00:50:36,200 --> 00:50:44,000
So many ambulances are going to
just pick up the injured...
injures from Tahrir Square.

329
00:50:44,000 --> 00:50:45,800
God be with us.

330
00:50:45,800 --> 00:50:47,920
SIREN IN DISTANCE

331
00:51:22,960 --> 00:51:27,040
OK. Who else? Ah, Wael,
my cousin Wael.

332
00:51:27,040 --> 00:51:31,800
For Tahir, the violence is now a
conspiracy to subvert the elections.

333
00:51:45,280 --> 00:51:50,720
With a week to the elections,
downtown Cairo is a battleground.

334
00:51:50,720 --> 00:51:55,840
The revolutionaries, who had
inspired the January uprising,

335
00:51:55,840 --> 00:52:00,240
now see the military council
as a brutal continuation
of the Mubarak regime.

336
00:52:00,240 --> 00:52:02,880
They want justice for those killed.

337
00:52:18,000 --> 00:52:21,400
Just a few streets away,
campaigning continues -

338
00:52:21,400 --> 00:52:27,160
the Muslim Brotherhood
and the Salafis are determined
that the elections go ahead.

339
00:52:27,160 --> 00:52:29,520
So is the military council.

340
00:52:42,320 --> 00:52:46,960
On 28th November, Egypt's first
ever democratic elections are held.

341
00:52:46,960 --> 00:52:51,640
CAMPAIGN SLOGANS THROUGH SPEAKER

342
00:52:55,320 --> 00:52:59,240
For so many, this is the triumph
of the revolution.

343
00:53:16,880 --> 00:53:22,080
Just a year ago, Tahir was living
under constant threat
of imprisonment

344
00:53:22,080 --> 00:53:24,520
for his religious activities.

345
00:53:24,520 --> 00:53:26,360
He voted!

346
00:53:26,360 --> 00:53:30,480
Now he is confident that the
elections will bring real change.

347
00:53:30,480 --> 00:53:36,000
You are not going to get down to
the street and find alcohol market.

348
00:53:36,000 --> 00:53:39,480
You are not going to find that,
in Sha'allah.

349
00:53:39,480 --> 00:53:43,480
The government is not
going to support that any more.

350
00:53:51,080 --> 00:53:54,800
Gigi spends the day filming and
tweeting as an unofficial monitor.

351
00:54:34,120 --> 00:54:36,160
She refuses to vote,

352
00:54:36,160 --> 00:54:40,520
because of the violence
and the continued detention
of her fellow protesters.

353
00:54:40,520 --> 00:54:45,720
Of course what we are
living under right now
is much worse than Mubarak days,

354
00:54:45,720 --> 00:54:50,680
but that doesn't mean that we want
Mubarak days to be back.

355
00:54:50,680 --> 00:54:54,920
No, this means that we're
that much closer to freedom.

356
00:54:57,280 --> 00:55:00,200
If Egypt become a Muslim state,

357
00:55:00,200 --> 00:55:04,560
Gigi might leave or she will fight
and she will go to jail.

358
00:55:04,560 --> 00:55:06,480
She doesn't understand yet.

359
00:55:06,480 --> 00:55:10,200
She still thinks she can change,
she can do more.

360
00:55:10,200 --> 00:55:12,440
Even if she lose her life.

361
00:55:12,440 --> 00:55:16,400
This is... For a father,
this is very scary for me.

362
00:55:20,360 --> 00:55:24,880
This is the revolution.
Freedom doesn't come easily.

363
00:55:24,880 --> 00:55:30,720
We have to pay in sacrifices
of blood, arrest, injures

364
00:55:30,720 --> 00:55:34,760
for us to win this battle
and at the end win this war.

365
00:55:42,600 --> 00:55:46,720
Ahmed is back on the square,
with no intention of voting.

366
00:56:39,560 --> 00:56:41,800
When the election results
are announced,

367
00:56:41,800 --> 00:56:46,440
as expected the Muslim Brotherhood
end up as the biggest party.

368
00:56:50,520 --> 00:56:54,480
Tahir's wedding turns into
a double celebration.

369
00:56:56,440 --> 00:57:01,680
His Salafi party have taken
almost a quarter of the vote.

370
00:57:01,680 --> 00:57:05,320
I feel like I am flying in the sky.

371
00:57:05,320 --> 00:57:08,560
His father wants an end to chaos.

372
00:57:17,760 --> 00:57:21,800
Tahir's plans for an Islamic Egypt
are a step closer

373
00:57:21,800 --> 00:57:25,480
and if the military attempt
to stand in the way

374
00:57:25,480 --> 00:57:30,600
the Salafis
and the Muslim Brotherhood now have
the strength to challenge them.

375
00:57:59,520 --> 00:58:01,800
WOMEN ULULATE

376
00:58:10,920 --> 00:58:18,560
For the Islamic parties, Egypt's
revolution has for the first time
brought the chance of real power.

377
00:58:18,560 --> 00:58:25,560
But for many of those who went
to Tahrir Square last January
to overthrow a dictatorship,

378
00:58:25,560 --> 00:58:28,400
the new Egypt has yet to be born.

379
00:58:54,440 --> 00:58:56,280
Subtitles by Red Bee Media Ltd

380
00:58:56,280 --> 00:58:58,280
E-mail subtitling@bbc.co.uk

