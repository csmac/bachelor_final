1
00:00:00,050 --> 00:00:08,100
<i> Timing & subtitles brought to you by Gone with the Shirt Team@Viki </i>

2
00:00:17,170 --> 00:00:20,480
♫<i> Extending across horizontally from a mysterious temple is a thousand mile sandstorm ♫</i>

3
00:00:20,480 --> 00:00:24,210
♫<i> Breaking through a thousand year of unchanging legend ♫</i>

4
00:00:24,210 --> 00:00:27,990
♫<i> One whistle and </i> ♫

5
00:00:27,990 --> 00:00:31,860
<i> ♫ here comes Heaven's ambush. ♫</i>

6
00:00:31,860 --> 00:00:38,280
♫<i> One side being blood thirsty, one side getting through one's trials ♫</i>

7
00:00:38,280 --> 00:00:46,870
♫<i> Wearing out one's obsession step by step, how do you explain that? ♫</i>

8
00:00:46,870 --> 00:00:50,720
♫<i> Empty and false life torrentially flows by; </i> ♫

9
00:00:50,720 --> 00:00:54,310
<i> ♫ Thoughts whistling and pattering ♫</i>

10
00:00:54,310 --> 00:01:01,790
♫<i> Jianghu deals with sharpness by pointing one's sword scabbard ♫</i>

11
00:01:01,790 --> 00:01:05,540
♫<i> Destiny follows laugh; </i> ♫

12
00:01:05,540 --> 00:01:09,060
<i> ♫ Heaven and earth attracts arrogance ♫</i>

13
00:01:09,060 --> 00:01:17,840
♫<i> Who says that one's reluctance to leave this world is not torture? When will gratitude and grudges end? ♫</i>

14
00:01:17,840 --> 00:01:25,150
♫<i> It's hard to predict a human heart; the soul is chasing daybreak ♫</i>

15
00:01:25,150 --> 00:01:33,850
♫<i> It's hard to predict what will happen in one's life; the soul disturbs today's morning ♫</i>

16
00:01:33,850 --> 00:01:40,160
<i>The Legend of Chusen</i>

17
00:01:40,160 --> 00:01:42,960
<i>Episode 8</i>

18
00:01:47,980 --> 00:01:54,860
Senior Sister! This is the green bean cake I made. I made it personally for you. Eat it!

19
00:02:20,130 --> 00:02:24,150
Zhang Xiao Fan, what are you doing outside the girls' dorm?

20
00:02:25,520 --> 00:02:29,310
I...I...was looking for my senior sister.

21
00:02:29,310 --> 00:02:32,170
She went out for something.

22
00:02:32,170 --> 00:02:33,830
Hey!

23
00:02:36,240 --> 00:02:43,050
Senior Sister Lu. Can you...do me a favor?

24
00:02:43,050 --> 00:02:44,630
No.

25
00:02:49,160 --> 00:02:53,290
Your Senior Sister left with Senior Brother Qi Hao to have their morning training already.

26
00:02:56,630 --> 00:03:03,680
Senior Sister. This food..please give it to her.

27
00:03:03,680 --> 00:03:08,090
Junior Brother Zhang. Don't you have to prepare for the martial arts competition?

28
00:03:08,090 --> 00:03:12,500
Are you so idle or you think that because you drew a that stick, you can be lucky throughout the competition?

29
00:03:12,500 --> 00:03:17,660
Everyone is doing their best, and what are you doing? You're still doing these pointless things.

30
00:03:19,210 --> 00:03:21,320
I'm sorry, Senior Sister.

31
00:03:48,960 --> 00:03:53,030
<i>Senior Brother Qi! You can just call me Ling Er from now on.</i>

32
00:03:54,830 --> 00:03:57,510
<i>Are you so idle</i>

33
00:03:57,510 --> 00:04:01,820
<i>Or do you feel like one picked spot can bring you luck the whole way?</i>

34
00:04:01,820 --> 00:04:06,180
<i>It's okay. Just treat it as getting experience this time.</i>

35
00:04:06,180 --> 00:04:08,190
<i>What can you do?</i>

36
00:04:08,190 --> 00:04:11,580
<i>You can't do martial arts. You can't do anything right!</i>

37
00:04:11,580 --> 00:04:14,340
<i>In the future, what are you preparing to do?</i>

38
00:04:56,560 --> 00:04:59,030
<i>-Zhang Xiao Fan<br>-Chu Yu Hong</i>

39
00:04:59,030 --> 00:05:03,310
Senior Brother. Who is this Chu Yu Hong?

40
00:05:03,310 --> 00:05:07,770
He's from Chang Yang Palace. I'm not sure if he's skilled though.

41
00:05:08,860 --> 00:05:12,780
Don't worry. Just try your best when you get up there.

42
00:05:12,780 --> 00:05:15,900
Senior Brothers, Senior Brothers! Let's make a bet.

43
00:05:15,900 --> 00:05:18,200
Let's see how likely Xiao Fan is to win this round!

44
00:05:18,200 --> 00:05:21,330
-Good idea! I bet that Xiao Fan loses!<br>-Okay!<br>-I bet on the opposite!

45
00:05:21,330 --> 00:05:23,430
-Okay!<br>-What are you guys doing?

46
00:05:23,430 --> 00:05:25,720
Xiao Fan is about to compete and you're here making him waver!

47
00:05:25,720 --> 00:05:29,900
Eldest Senior Brother, don't get angry! I was just joking!

48
00:05:29,900 --> 00:05:32,060
You were joking? Okay then!

49
00:05:32,060 --> 00:05:35,460
Since we're already betting, how about I place a bet too?

50
00:05:35,460 --> 00:05:37,060
Senior Brother Song, you-!

51
00:05:37,060 --> 00:05:38,870
You're doing this seriously? I'm going to tell Teacher!

52
00:05:38,870 --> 00:05:42,240
Eldest Senior Brother, you must not tell Teacher!

53
00:05:42,240 --> 00:05:45,980
Xiao Fan, I was just joking! You won't mind, right?

54
00:05:45,980 --> 00:05:47,920
I won't.

55
00:05:53,160 --> 00:05:56,870
Xiao Fan, I heard you came looking for me this morning!

56
00:05:56,870 --> 00:05:58,790
I-I didn't.

57
00:05:58,790 --> 00:06:01,340
You didn't?

58
00:06:01,340 --> 00:06:03,900
-Ling Er, let's go.<br>-Okay, I'm coming!

59
00:06:03,900 --> 00:06:09,110
Xiao Fan, I still have to compete today, so I can't watch you! I'll be leaving!

60
00:06:09,110 --> 00:06:11,220
I'm coming!

61
00:06:13,180 --> 00:06:16,660
I'm leaving too. Xiao Fan, work hard!

62
00:06:16,660 --> 00:06:20,130
Junior Brother, Teacher and Mistress don't want to put pressure on you. Just fight a bit.

63
00:06:20,130 --> 00:06:21,930
Winning or losing is not important.

64
00:06:21,930 --> 00:06:23,980
Okay.

65
00:06:23,980 --> 00:06:26,370
Work hard Xiao Fan!

66
00:06:27,910 --> 00:06:32,500
Xiao Fan, I'll say it again. Just do your best.

67
00:06:32,500 --> 00:06:34,090
I'm leaving.

68
00:06:52,450 --> 00:06:55,790
Is Da Zhu Palace out of people? There isn't even a single spectator!

69
00:06:55,790 --> 00:07:00,110
That's right! There isn't even a single person here, are they afraid of losing?

70
00:07:00,110 --> 00:07:03,700
-How shameful!<br>-Come down, come down!

71
00:07:07,940 --> 00:07:10,780
How shameful! Hurry and come down!

72
00:07:16,600 --> 00:07:20,670
-It's Senior Brother Chu!<br>-Senior Brother Chu!

73
00:07:25,160 --> 00:07:26,870
Junior Brother Zhang.

74
00:07:51,280 --> 00:07:54,370
What is that? A fire torch?

75
00:07:54,370 --> 00:07:57,240
A fire torch? A fire torch!

76
00:07:57,240 --> 00:08:00,170
Someone from Da Zhu Palace used a dice yesterday too!

77
00:08:00,170 --> 00:08:03,710
Why are the weapons from Da Zhu Palace so exotic?

78
00:08:21,930 --> 00:08:24,400
Make your move, Junior Brother Zhang.

79
00:08:56,110 --> 00:09:00,350
Senior Brother, who do you think will win this round?

80
00:09:00,350 --> 00:09:02,870
It's hard to tell.

81
00:09:12,890 --> 00:09:17,160
Good! Good!

82
00:09:17,160 --> 00:09:19,730
Junior Brother. Are you okay?

83
00:09:19,730 --> 00:09:24,270
I'm okay. Senior Brother, please put all your energy into it. Don't be merciful.

84
00:09:25,760 --> 00:09:27,800
I think I'd best-

85
00:09:30,320 --> 00:09:32,880
-Are you looking down on me?<br>-No.

86
00:09:34,760 --> 00:09:38,860
Very well then. I'll give it my all. You must be careful.

87
00:10:32,380 --> 00:10:34,850
<i>Mother!</i>

88
00:10:53,510 --> 00:10:56,870
Senior Brother Chu!

89
00:11:03,600 --> 00:11:06,400
Da Zhu Palace's Zhang Xiao Fan wins!

90
00:11:06,400 --> 00:11:09,000
What? He must have cheated!

91
00:11:09,000 --> 00:11:10,400
He must have cheated!

92
00:11:10,400 --> 00:11:13,400
He cheated! That isn't Qing Yun's martial arts!

93
00:11:13,400 --> 00:11:15,600
What kind of demon magic is that?

94
00:11:15,600 --> 00:11:17,800
It's demon magic! He cheated!

95
00:11:17,800 --> 00:11:20,800
What kind of demon magic did he use?

96
00:11:28,200 --> 00:11:31,000
Don't blame him!

97
00:11:31,000 --> 00:11:33,100
I'm the one who got complacent.

98
00:11:35,000 --> 00:11:39,700
Enough! If you aren't happy with anything, talk to the sect leader!

99
00:11:47,200 --> 00:11:50,100
What is that demonic object you hold?

100
00:11:51,400 --> 00:11:54,800
It's my treasure. It's not a demonic object!

101
00:12:28,200 --> 00:12:32,400
Good! Good! Good!

102
00:12:32,400 --> 00:12:35,600
Xue Qi, well fought! You're so amazing!

103
00:12:35,600 --> 00:12:38,600
Senior Sister Lu! Senior Sister Lu!

104
00:12:38,600 --> 00:12:41,700
-You're so amazing, Senior Sister Lu!<br>-Good, good!

105
00:12:43,000 --> 00:12:44,200
My apologies.

106
00:12:44,200 --> 00:12:46,400
Good! Good!

107
00:12:46,400 --> 00:12:48,000
Xue Qi is so amazing!

108
00:12:48,000 --> 00:12:52,200
Xue Qi! Xue-! Did she finish yet?

109
00:12:53,800 --> 00:12:55,400
My Xue Qi!

110
00:12:55,400 --> 00:12:57,600
Why am I so unlucky? I just finished a fight!

111
00:12:57,600 --> 00:13:00,600
You won too, and Xue Qi-

112
00:13:00,600 --> 00:13:03,600
Xiao Fan, what's up with your face?

113
00:13:05,200 --> 00:13:06,900
Zheng Shu Shu!

114
00:13:08,000 --> 00:13:10,300
Let's talk later!

115
00:13:14,500 --> 00:13:15,800
It's okay.

116
00:13:16,800 --> 00:13:18,800
Did your father go look for Senior Master Sect Leader?

117
00:13:18,800 --> 00:13:23,000
He looked for Junior Master Cang Song, he said he'd deal with it.

118
00:13:23,000 --> 00:13:26,400
That thief was so familiar with the treasure trove.

119
00:13:26,400 --> 00:13:29,800
Could he be someone like Boss Nian who's been here for a long time?

120
00:13:29,800 --> 00:13:32,200
It indeed can be some remaining disciples of Lie Xie Sect.

121
00:13:32,200 --> 00:13:34,600
But he can break through the enchantment my father set.

122
00:13:34,600 --> 00:13:38,100
That means he has a very high level of cultivation!

123
00:13:39,200 --> 00:13:41,400
We have to hurry and capture this thief.

124
00:13:41,400 --> 00:13:44,000
The fewer who know about this, the better.

125
00:13:44,000 --> 00:13:46,400
It shouldn't be publicized and we still haven't any clues yet.

126
00:13:46,400 --> 00:13:49,400
Then, do your father and Junior Master Cang Song have a plan?

127
00:13:49,400 --> 00:13:53,900
They're currently monitoring the martial arts competition. They can't clone themselves!

128
00:13:57,200 --> 00:13:59,800
That person didn't get what he wanted last night.

129
00:13:59,800 --> 00:14:02,400
Do you think he'll come again tonight?

130
00:14:02,400 --> 00:14:04,600
If he must have the Nine Rites Cauldron,

131
00:14:04,600 --> 00:14:08,200
then he will definitely come again tonight.

132
00:14:08,200 --> 00:14:12,000
Then let's wait for him tonight!

133
00:14:12,000 --> 00:14:14,900
Go with the flow?

134
00:14:19,000 --> 00:14:28,000
<i> Timing & subitles brought to you by Gone with the Shirt Team@Viki </i>

135
00:14:30,000 --> 00:14:31,800
Senior Brother Song.

136
00:14:31,800 --> 00:14:34,200
-Junior Brother Lin.<br>-Junior Brother Lin.

137
00:14:35,200 --> 00:14:36,600
Where is Xiao Fan?

138
00:14:37,600 --> 00:14:40,600
I don't know either. I haven't seen him since the competition this afternoon.

139
00:14:40,600 --> 00:14:42,000
You didn't watch him compete?

140
00:14:42,000 --> 00:14:44,000
We went to go support Junior Sister.

141
00:14:44,000 --> 00:14:46,200
That's right. How was Xiao Fan's fight?

142
00:14:46,200 --> 00:14:48,800
Xiao Fan is still a disciple of Da Zhu Palace!

143
00:14:48,800 --> 00:14:50,600
Is Junior Master Tian not going to ask about it at all?

144
00:14:50,600 --> 00:14:53,400
Enough, enough! Don't talk about that.

145
00:14:53,400 --> 00:14:57,600
Our Second Senior Brother just lost. Look over there.

146
00:14:57,600 --> 00:15:02,000
I say, Old Second! How can you lose like that?

147
00:15:02,000 --> 00:15:03,400
What did I say to you?

148
00:15:03,400 --> 00:15:08,100
If my teacher hears your words, he'll definitely get angry again!

149
00:15:09,200 --> 00:15:12,600
I heard that Xiao Fan triumphed at his competition today.

150
00:15:12,600 --> 00:15:13,600
Xiao Fan triumphed?

151
00:15:13,600 --> 00:15:17,300
Xiao Fan won? Xiao Fan won!

152
00:15:19,400 --> 00:15:24,000
Xiao Fan he-! My money! My money!

153
00:15:24,000 --> 00:15:25,000
What?

154
00:15:25,000 --> 00:15:26,600
I bet that Xiao Fan would lose-!

155
00:15:26,600 --> 00:15:29,700
Quiet! Hurry and tell Teacher!

156
00:16:10,300 --> 00:16:14,700
Senior Sister, you can be frank.

157
00:16:15,600 --> 00:16:17,600
You're full of tyrannical aura.

158
00:16:17,600 --> 00:16:21,000
At the martial arts competition, I'm afraid you'll be in danger.

159
00:16:21,000 --> 00:16:23,600
If you go on like this,

160
00:16:24,800 --> 00:16:28,400
to put it simple, watch yourself.

161
00:16:28,400 --> 00:16:30,400
Xue Qi! Xiao Fan!

162
00:16:30,400 --> 00:16:31,800
Xue Qi!

163
00:16:32,600 --> 00:16:34,600
This is my Heaven and Earth One Breath Bag.

164
00:16:34,600 --> 00:16:37,200
It's especially designed to hold different kinds of medicines and the seal is inherited in our family.

165
00:16:37,200 --> 00:16:39,800
-Hurry and switch it out.<br>-Okay!

166
00:16:47,400 --> 00:16:49,800
I can only maintain it for a moment. You must switch out the cauldron!

167
00:16:49,800 --> 00:16:52,480
Put it into the One Breath Bag.

168
00:17:06,800 --> 00:17:10,000
Hurry up, I can't keep it up!

169
00:17:21,000 --> 00:17:24,000
Thankfully I didn't use my hands last time!

170
00:17:27,800 --> 00:17:29,600
Let's find a place to hide it.

171
00:17:29,600 --> 00:17:31,100
Come on!

172
00:18:06,800 --> 00:18:09,600
Shu Shu, are you okay?

173
00:18:09,600 --> 00:18:12,600
My hand! Don't bother with me, hurry after him!

174
00:18:12,600 --> 00:18:16,900
Wait. I'll draw you a seal talisman of the One Breath Bag.

175
00:18:20,800 --> 00:18:23,000
Hurry and go after him!

176
00:19:19,200 --> 00:19:22,300
Zhang Xiao Fan, you only know how to cause trouble!

177
00:19:24,200 --> 00:19:29,000
Lu Xue Qi. You are your teacher's talented student. You are usually quite obedient.

178
00:19:29,000 --> 00:19:32,200
Why did you go create trouble with these two?

179
00:19:33,000 --> 00:19:36,200
Junior Master, this was my and Zheng Shu Shu's idea.

180
00:19:36,200 --> 00:19:40,000
-It has-<br>-Your idea? You only know how to ruin the plan early!

181
00:19:44,000 --> 00:19:45,400
Teacher, please investigate it well.

182
00:19:45,400 --> 00:19:48,800
That black-clothed man was really wild and bold. He even dared to target the Nine Rites Cauldron?!

183
00:19:48,800 --> 00:19:52,200
I was already alerted when someone sneaked into the treasure trove last time.

184
00:19:52,200 --> 00:19:55,300
Your Junior Master Zheng and I just added another enchantment.

185
00:19:55,300 --> 00:19:57,400
When someone touches it, we will be notified.

186
00:19:57,400 --> 00:20:00,000
And then I could use it to find that thief!

187
00:20:00,000 --> 00:20:01,800
But you all had to act smart!

188
00:20:01,800 --> 00:20:04,800
Do you think we have all gone foolish from old age?

189
00:20:07,300 --> 00:20:11,000
Okay. Hurry and put the Nine Rites Cauldron back in the treasure trove.

190
00:20:11,000 --> 00:20:13,800
Don't interfere in this matter anymore.

191
00:20:13,800 --> 00:20:15,200
Yes.

192
00:20:19,400 --> 00:20:20,600
Hurry and go!

193
00:20:20,600 --> 00:20:23,500
-Yes, Junior Master.<br>-Yes.

194
00:20:39,600 --> 00:20:41,900
How was it? Did you find it?

195
00:20:46,400 --> 00:20:49,600
Thank heavens! Thank heavens!

196
00:20:49,600 --> 00:20:52,900
-How is your injury?<br>-I'm okay!

197
00:20:56,000 --> 00:20:59,000
Xue Qi, are you caring about me?

198
00:21:00,200 --> 00:21:02,800
Xue Qi! Xue Qi!

199
00:21:02,800 --> 00:21:04,600
Did you think things through?

200
00:21:04,600 --> 00:21:06,900
You like me, right?

201
00:21:08,200 --> 00:21:10,000
She's shy!

202
00:21:10,000 --> 00:21:11,500
Yes.

203
00:21:14,700 --> 00:21:16,800
I heard that you won in your competition yesterday.

204
00:21:16,800 --> 00:21:19,000
I was so happy!

205
00:21:19,000 --> 00:21:21,400
I originally wanted to celebrate with you.

206
00:21:21,400 --> 00:21:24,600
I didn't think that this sort of thing would happen.

207
00:21:24,600 --> 00:21:26,000
That's because I was just lucky.

208
00:21:26,000 --> 00:21:28,800
If you saw how I won, you wouldn't say that.

209
00:21:28,800 --> 00:21:31,900
Why do you say that? You did very well.

210
00:21:32,700 --> 00:21:35,100
I will definitely do my best today too!

211
00:21:36,600 --> 00:21:40,200
Xiao Fan, I'll go support you this afternoon. Give a good performance!

212
00:21:40,200 --> 00:21:42,700
Don't like your teacher look down on you!

213
00:21:45,600 --> 00:21:48,600
I forgot to go to Teacher!

214
00:21:48,600 --> 00:21:51,400
Jing Yu, I'll go back first!

215
00:21:58,600 --> 00:22:01,200
Look at all of you!

216
00:22:01,200 --> 00:22:03,600
And you! As the oldest,

217
00:22:03,600 --> 00:22:05,090
you look very talented on the outside,

218
00:22:05,090 --> 00:22:08,300
always so handsome! You-!

219
00:22:09,000 --> 00:22:10,300
Teacher!

220
00:22:11,800 --> 00:22:13,600
Xiao Fan, hurry and come here!

221
00:22:13,600 --> 00:22:15,000
Xiao Fan!

222
00:22:15,000 --> 00:22:17,400
Teacher, Mistress.

223
00:22:22,760 --> 00:22:25,180
Teacher, Xiao Fan is back.

224
00:22:27,220 --> 00:22:30,440
Which house's young talent is this?

225
00:22:30,440 --> 00:22:33,790
Did he come to the wrong teacher?

226
00:22:33,790 --> 00:22:36,440
Teacher, I know I was wrong.

227
00:22:36,440 --> 00:22:40,370
Zhang Xiao Fan, you won once by luck.

228
00:22:41,520 --> 00:22:44,970
You don't need to return here. Go be independent!

229
00:22:44,970 --> 00:22:47,650
What a thing worth celebrating!

230
00:22:51,020 --> 00:22:56,250
And you all! Look at all of you!

231
00:22:56,250 --> 00:22:59,220
Don't compete anymore. Go back! You are so shameful here!

232
00:22:59,220 --> 00:23:00,920
Bu Yi!

233
00:23:05,610 --> 00:23:08,170
My heart!

234
00:23:14,600 --> 00:23:16,940
Get up, Xiao Fan.

235
00:23:16,940 --> 00:23:18,680
Hurry and get up!

236
00:23:19,990 --> 00:23:23,120
Your teacher is very angry right now. Don't put it in your heart.

237
00:23:23,120 --> 00:23:27,710
Compete well later. When you're done, go apologize to him.

238
00:23:27,710 --> 00:23:28,980
It's my fault.

239
00:23:28,980 --> 00:23:33,180
This isn't your fault. It's mostly because your senior brothers all lost yesterday.

240
00:23:33,180 --> 00:23:36,730
Ling Er is the only one left. So your teacher is very angry.

241
00:23:36,730 --> 00:23:41,710
Zhang Xiao Fan, you luckily won one round.

242
00:23:41,710 --> 00:23:46,440
It's okay! Senior Brothers, I'll get revenge for you!

243
00:23:46,440 --> 00:23:48,980
Who said we all lost? Isn't there still Xiao Fan?

244
00:23:48,980 --> 00:23:54,320
Mistress, Xiao Fan is lucky. He picked the empty slot in the first round, and won the second!

245
00:23:57,110 --> 00:24:00,390
Yes, Mistress. I was just lucky.

246
00:24:00,390 --> 00:24:03,140
How did you win?

247
00:24:03,140 --> 00:24:08,300
Halfway through, the opponent got diarrhea.

248
00:24:08,300 --> 00:24:11,100
You are too lucky!

249
00:24:27,040 --> 00:24:30,720
<i>[Zhang Xiao Fan versus Huang Lei]</i>

250
00:24:47,740 --> 00:24:51,600
Xiao Fan! We're going to watch Junior Sister!

251
00:24:51,600 --> 00:24:53,500
Fight well! You must be confident!

252
00:24:53,500 --> 00:24:55,380
Xiao Fan!

253
00:24:56,870 --> 00:25:00,160
-We're leaving!<br>-We're leaving!

254
00:26:04,480 --> 00:26:06,600
-Senior Brother!<br>-Senior Brother.

255
00:27:03,630 --> 00:27:09,090
<i>Did I win because of the two types of martial arts, or because of this black stick?</i>

256
00:27:10,150 --> 00:27:13,300
<i>Why can't I remember the move I used to win just now?</i>

257
00:27:14,530 --> 00:27:17,220
<i>Could it be this stick controlled me?</i>

258
00:27:36,680 --> 00:27:38,270
Xiao Fan.

259
00:27:39,840 --> 00:27:43,940
-Jing Yu.<br>-That wasn't a bad fight.

260
00:27:45,210 --> 00:27:47,300
Just lucky.

261
00:27:47,300 --> 00:27:50,740
You won twice in a row. How could it be just because of luck?

262
00:27:50,740 --> 00:27:55,300
Everyone is discussing that the martial arts of Da Zhu Palace is really profound.

263
00:27:55,300 --> 00:27:58,610
They don't even know how the opponent was defeated.

264
00:28:03,120 --> 00:28:06,330
Come. Let's spar.

265
00:28:06,330 --> 00:28:08,660
Let me see your martial arts.

266
00:28:12,450 --> 00:28:15,080
Why do I need to spar with you?

267
00:28:17,560 --> 00:28:23,560
You can't be afraid that you'll lose to me since you already used your ultimate move?

268
00:28:31,950 --> 00:28:36,280
I already told you that it was based on luck.

269
00:28:36,280 --> 00:28:39,770
But on the competition platform, you did very well.

270
00:28:39,770 --> 00:28:43,380
I won't fight you.

271
00:28:43,380 --> 00:28:46,640
You are my best friend.

272
00:28:46,640 --> 00:28:51,410
Xiao Fan. Did your teacher teach you nothing?

273
00:28:52,710 --> 00:28:55,370
I already said that I couldn't master it.

274
00:29:00,540 --> 00:29:04,750
Okay. Let's have a little spar.

275
00:30:21,520 --> 00:30:24,690
You see? I only won because I was lucky.

276
00:30:24,690 --> 00:30:28,770
Then what move did you use to win the past two rounds?

277
00:30:30,940 --> 00:30:33,140
I'm telling you! I don't have the ability to win!

278
00:30:33,140 --> 00:30:35,820
All I used was cheap tricks, okay?

279
00:30:38,680 --> 00:30:42,720
Zhang Xiao Fan, what kind of twisted skills did you learn?

280
00:30:42,720 --> 00:30:44,790
Are you going to keep it a secret even from me?

281
00:30:44,790 --> 00:30:47,440
I can't even master Qing Yun's basic magic skills!

282
00:30:47,440 --> 00:30:51,290
How can I learn twisted skills? You're overestimating me.

283
00:30:51,290 --> 00:30:53,750
You're lying, you're lying!

284
00:31:09,640 --> 00:31:12,640
Where did the magic treasure in your hand come from?

285
00:31:18,860 --> 00:31:20,970
I've never lied.

286
00:31:27,060 --> 00:31:31,400
I cannot tell you where this magic treasure came from.

287
00:31:46,040 --> 00:31:48,860
-The competition today was so intense!<br>-Yes.

288
00:31:48,860 --> 00:31:51,000
Senior Sister Lu won again, she's so amazing!

289
00:31:51,000 --> 00:31:53,400
I originally thought Junior Brother Huang could win.

290
00:31:53,400 --> 00:31:55,870
I wonder what that Zhang Xiao Fan used.

291
00:31:55,870 --> 00:31:59,120
He could win over Junior Brother Huang!

292
00:31:59,120 --> 00:32:02,000
-It's too strange.<br>-That's right.

293
00:32:17,600 --> 00:32:18,600
Why are you here?

294
00:32:18,600 --> 00:32:21,400
I'm waiting for Xue Qi.

295
00:32:28,000 --> 00:32:30,000
What's up with you?

296
00:32:32,400 --> 00:32:35,500
I'm telling you, Xue Qi she-

297
00:32:42,000 --> 00:32:45,600
Xiao Fan, how did you win today?

298
00:32:45,600 --> 00:32:48,300
Everyone is talking about you.

299
00:32:50,800 --> 00:32:55,500
Okay okay, I won't talk. I'll listen to you talk, okay?

300
00:33:04,400 --> 00:33:07,500
Xiao Fan, did you look on the notice board?

301
00:33:08,400 --> 00:33:11,400
The one you're fighting tomorrow is my eldest senior brother, Peng Chang.

302
00:33:11,400 --> 00:33:14,600
He has the highest cultivation among the disciples at Feng Hui Palace

303
00:33:14,600 --> 00:33:17,400
But don't worry. I've already gone to talk to him.

304
00:33:17,400 --> 00:33:19,800
He will be merciful.

305
00:33:20,600 --> 00:33:22,000
Just kill me.

306
00:33:22,000 --> 00:33:25,000
Why are you being so silly?

307
00:33:25,800 --> 00:33:30,600
Xiao Fan, what kind of martial arts did you use today?

308
00:33:30,600 --> 00:33:32,600
Did you learn it from Xiao Gui?

309
00:33:32,630 --> 00:33:35,460
How about you teach me too?

310
00:33:37,800 --> 00:33:42,600
Are there Qing Yun disciples who know martial arts from two sects?

311
00:33:42,600 --> 00:33:45,200
Martial arts from two sects?

312
00:33:45,200 --> 00:33:46,400
There shouldn't be.

313
00:33:46,400 --> 00:33:48,600
Did you forget? Qing Yun Sect's third rule.

314
00:33:48,600 --> 00:33:51,200
Secretly learning other martial arts is taboo here.

315
00:33:51,200 --> 00:33:56,000
Whoever is caught gets their martial arts destroyed, kicked out of Qing Yun, and then...

316
00:34:05,000 --> 00:34:07,600
<i>Zhang Xiao Fan, Peng Chang</i>

317
00:34:20,000 --> 00:34:22,800
Why did your Chao Yang Palace come to watch our Feng Hui Palace's competition?

318
00:34:22,800 --> 00:34:24,600
Of course we came to support Senior Brother Peng!

319
00:34:24,600 --> 00:34:28,100
He must defeat that Zhang Xiao Fan. He must defeat him!

320
00:34:29,000 --> 00:34:33,000
Senior Brother Peng, it's all up to you! You must not let that brat win!

321
00:34:33,000 --> 00:34:37,100
It's okay, Junior Brother Xiao Fan is also under our sect.

322
00:34:38,600 --> 00:34:40,700
Okay okay. I know.

323
00:34:40,700 --> 00:34:43,600
I will definitely be merciful. Don't worry.

324
00:34:43,600 --> 00:34:46,400
Senior Brother you're the best! You really are my senior brother!

325
00:34:46,400 --> 00:34:47,400
Of course!

326
00:34:47,400 --> 00:34:50,000
Xiao Fan! Work hard, Xiao Fan!

327
00:34:50,000 --> 00:34:51,900
Work hard, Xiao Fan!

328
00:34:51,910 --> 00:34:53,520
Xiao Fan!

329
00:34:54,200 --> 00:34:57,200
Junior Brother, which side are you on?

330
00:34:57,200 --> 00:35:00,900
N-n-no, we're all on the same side!

331
00:35:37,200 --> 00:35:40,800
Xiao Fan! Stop going easy on him!

332
00:35:41,600 --> 00:35:45,800
Junior Brother Zhang, you keep avoiding attacking. Are you looking down on me?

333
00:37:02,000 --> 00:37:04,000
He used that demonic magic again! That's not a Qing Yun skill!

334
00:37:04,000 --> 00:37:06,000
Yeah! Don't tell me that's demonic magic!

335
00:37:06,000 --> 00:37:09,100
Twisted magic! It must be demonic skills!

336
00:37:09,100 --> 00:37:10,600
Why did you lie that you don't know magic?

337
00:37:10,600 --> 00:37:12,800
Didn't I tell him to go easy on you?

338
00:37:12,800 --> 00:37:15,200
Why did you lie to me?

339
00:37:15,200 --> 00:37:17,600
Catch him, catch him!

340
00:37:17,600 --> 00:37:19,000
Hit him, hit him!

341
00:37:19,000 --> 00:37:21,000
Let me do it!

342
00:37:21,000 --> 00:37:25,400
Hit! Hit! Hit! Hit him!

343
00:37:25,400 --> 00:37:27,600
What are you doing? Is the big group bullying the small group?

344
00:37:27,600 --> 00:37:29,400
Let's talk, calm down!

345
00:37:29,400 --> 00:37:32,200
That's right! Retreat! I'm telling you, our Junior Brother Xiao Fan won!

346
00:37:32,200 --> 00:37:35,000
Are you guys not willing to take a loss?

347
00:37:35,000 --> 00:37:37,200
Retreat! Don't go overboard!

348
00:37:37,200 --> 00:37:40,800
Is the big group bullying the small? Retreat!

349
00:37:40,800 --> 00:37:44,000
-Calm down!<br>-Stop!

350
00:37:47,000 --> 00:37:49,600
What are you doing?

351
00:37:49,600 --> 00:37:51,800
What are you doing?

352
00:37:51,800 --> 00:37:53,300
Teacher!

353
00:37:54,000 --> 00:37:56,400
Do you want to be punished?

354
00:38:07,200 --> 00:38:09,800
Do rules still exist here?

355
00:39:20,800 --> 00:39:25,400
Old Seven, can you tell me

356
00:39:25,400 --> 00:39:29,300
how you beat Peng Chang?

357
00:39:33,800 --> 00:39:36,600
You don't want to tell me?

358
00:39:43,800 --> 00:39:45,300
Teacher.

359
00:39:46,800 --> 00:39:48,900
I...

360
00:39:58,800 --> 00:40:02,500
Teacher. I am stupid.

361
00:40:02,500 --> 00:40:06,000
In all these years, I have improved little.

362
00:40:06,000 --> 00:40:07,800
But recently,

363
00:40:07,800 --> 00:40:12,100
I suddenly discovered a magic treasure that matched me!

364
00:40:14,000 --> 00:40:19,200
Xiao Fan...you now is a dark horse.

365
00:40:19,200 --> 00:40:24,100
Amazing everyone with a single brilliant feat. So in the limelight.

366
00:40:26,800 --> 00:40:30,000
Teacher. No.

367
00:40:30,000 --> 00:40:34,400
Okay! You don't need to say anything.

368
00:40:34,400 --> 00:40:38,600
When I see you, when I see all of you-!

369
00:40:38,600 --> 00:40:41,300
My heart!

370
00:40:42,800 --> 00:40:45,400
Okay! I,

371
00:40:45,400 --> 00:40:49,600
don't want to know how you got that magic treasure.

372
00:40:49,600 --> 00:40:53,200
Do you know, that to be able to be matched with a magic treasure,

373
00:40:53,200 --> 00:40:56,900
that person's skills must be at least at the fourth level?

374
00:40:58,000 --> 00:40:59,800
But I remember

375
00:40:59,800 --> 00:41:02,600
that I have only taught you up to the second level of magic!

376
00:41:02,600 --> 00:41:05,200
Can you tell this ignorant and ill-informed Teacher of yours

377
00:41:05,200 --> 00:41:10,700
how you skipped the third level to hit the fourth level?

378
00:41:11,400 --> 00:41:13,000
Tell me!

379
00:41:24,800 --> 00:41:26,200
Please punish me, Teacher.

380
00:41:26,200 --> 00:41:28,200
Punish?

381
00:41:28,200 --> 00:41:31,700
Learning magic behind your teacher's back is Qing Yun's biggest taboo!

382
00:41:31,700 --> 00:41:35,200
A light punishment means reflection for ten years!

383
00:41:35,200 --> 00:41:37,800
A serious punishment means your skills are destroyed

384
00:41:37,800 --> 00:41:40,300
and you are kicked out of the sect!

385
00:41:43,000 --> 00:41:51,100
<i> Timing & subitles brought to you by Gone with the Shirt Team@Viki </i>

386
00:41:52,600 --> 00:41:55,800
♫<i> One thought became an obsession ♫</i>

387
00:41:55,800 --> 00:41:58,800
♫<i> The blue sea became yellow desert ♫</i>

388
00:41:58,800 --> 00:42:05,100
♫<i> I passed within reincarnations like a flying bird ♫</i>

389
00:42:05,100 --> 00:42:08,200
♫<i> Anchoring on my memories ♫</i>

390
00:42:08,200 --> 00:42:11,500
♫<i> Lighting a candle ♫</i>

391
00:42:11,500 --> 00:42:17,800
♫<i> Can that weak light illuminate sadness? ♫</i>

392
00:42:17,800 --> 00:42:21,000
♫<i> Listening to the rain dropping ♫</i>

393
00:42:21,000 --> 00:42:24,000
♫<i> Looking at some flowers blooming ♫</i>

394
00:42:24,000 --> 00:42:30,400
♫<i> Sighing why there are only silhouettes that were left from yesterday ♫</i>

395
00:42:30,400 --> 00:42:33,600
♫<i> Love chains that's hard to break ♫</i>

396
00:42:33,600 --> 00:42:36,700
♫<i> A simple life in one's eyes ♫</i>

397
00:42:36,700 --> 00:42:43,000
♫<i> A dream that no one can touch ♫</i>

398
00:42:43,000 --> 00:42:46,200
♫<i> I got drunk within one's lifetime ♫</i>

399
00:42:46,200 --> 00:42:49,700
♫<i> Suddenly, like a dream ♫</i>

400
00:42:49,710 --> 00:42:55,590
♫<i> When I opened my eyes, time has already interlaced ♫</i>

401
00:42:55,630 --> 00:42:58,830
♫<i> One feeling went on fire ♫</i>

402
00:42:58,830 --> 00:43:02,340
♫<i> One heart got bewitched ♫</i>

403
00:43:02,400 --> 00:43:11,000
♫<i> Who's going to be scared out of one's wits for whom?</i>

404
00:43:12,000 --> 00:43:18,200
♫<i> The pen of time can't draw us ♫</i>

405
00:43:18,200 --> 00:43:24,200
♫<i> Don't leave too much messages ♫</i>

406
00:43:24,200 --> 00:43:30,800
♫<i> One love blossoms; love is hard to copy ♫</i>

407
00:43:30,800 --> 00:43:37,000
♫<i> Use my lifetime to accompany you to squander ♫</i>

408
00:43:37,000 --> 00:43:43,000
♫<i> Be with you until even the north star has fallen down ♫</i>

409
00:43:55,500 --> 00:43:58,800
♫<i> One feeling went on fire ♫</i>

410
00:43:58,800 --> 00:44:02,400
♫<i> One heart got bewitched ♫</i>

411
00:44:02,400 --> 00:44:11,400
♫<i> Who's going to be scared out of one's wits for whom?</i>

412
00:44:11,400 --> 00:44:18,400
♫<i> The pen of time can't draw us ♫</i>

413
00:44:18,400 --> 00:44:24,100
♫<i> Don't leave too much messages ♫</i>

414
00:44:24,120 --> 00:44:30,700
♫<i> One love blossoms; love is hard to copy ♫</i>

415
00:44:30,700 --> 00:44:37,230
♫<i> Use my lifetime to accompany you to squander ♫</i>

416
00:44:37,230 --> 00:44:43,550
♫<i> Be with you until even the north star has fallen down ♫</i>

