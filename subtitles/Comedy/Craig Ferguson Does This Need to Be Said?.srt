1
00:01:04,167 --> 00:01:05,542
<i>We will win!</i>

2
00:01:07,000 --> 00:01:08,833
<i>Fight! We will win!</i>

3
00:05:27,167 --> 00:05:29,292
<i>Getting ready for the party!</i>

4
00:05:31,167 --> 00:05:33,292
<i>I take my daughter with me</i>

5
00:05:55,792 --> 00:05:59,000
Since going to the ball, you gotta dance like a princess.

6
00:05:59,542 --> 00:06:01,708
1, 2, 3 ...

7
00:06:04,333 --> 00:06:05,542
You'll dress how?

8
00:06:06,833 --> 00:06:08,917
- I'll get dressed ...
- With...

9
00:06:09,208 --> 00:06:10,542
A bucket on his head?

10
00:06:14,333 --> 00:06:15,792
Dress like a barrel.

11
00:06:16,708 --> 00:06:18,042
And mops to toe.

12
00:06:35,417 --> 00:06:36,667
Good afternoon.

13
00:06:40,542 --> 00:06:41,667
Hello, are you?

14
00:06:42,417 --> 00:06:44,000
Want to eat?

15
00:06:44,458 --> 00:06:45,917
I'm not hungry.

16
00:09:00,333 --> 00:09:01,625
 Ok

17
00:09:24,833 --> 00:09:25,833
<i>Franklin?</i>

18
00:09:26,208 --> 00:09:27,000
<i>It's me. I wanted</i>

19
00:09:28,875 --> 00:09:30,292
<i>you leave a message.</i>

20
00:09:31,333 --> 00:09:35,583
<i>It might be to talk? Without children.</i>

21
00:09:36,292 --> 00:09:38,875
<i>I would really like to ensure.</i>

22
00:09:41,000 --> 00:09:42,125
<i>I miss you.</i>

23
00:10:15,625 --> 00:10:17,417
Eva Khatchadourian?

24
00:10:35,667 --> 00:10:37,625
You've done a ton of stuff.

25
00:10:40,792 --> 00:10:43,458
Listen, Eva, no matter who you are

26
00:10:43,708 --> 00:10:47,292
and where you come from. If you can type and rank, I urge you.

27
00:10:50,208 --> 00:10:51,583
It's wonderful.

28
00:10:51,875 --> 00:10:52,875
Thank you.I'm sorry

29
00:10:56,292 --> 00:10:58,542
I will not disappoint you. Promised.

30
00:10:59,042 --> 00:11:00,375
Thank you very much.

31
00:11:00,792 --> 00:11:01,792
You're welcome.

32
00:11:02,542 --> 00:11:03,708
At Monday.

33
00:11:05,208 --> 00:11:06,292
Great, thank you.

34
00:11:28,417 --> 00:11:30,500
Looks like all is well with you.

35
00:11:30,792 --> 00:11:31,875
Are you satisfied?

36
00:11:32,875 --> 00:11:33,750
Excuse me

37
00:11:38,500 --> 00:11:41,667
I hope you burn in hell, bitch!

38
00:11:49,500 --> 00:11:52,583
- What happened?
- It's nothing.

39
00:11:52,875 --> 00:11:54,750
- I call for help.
- It's my fault.

40
00:11:55,042 --> 00:11:57,375
- It hit you!
Are you OK?

41
00:13:14,417 --> 00:13:16,000
<i>Eva, when you come home?</i>

42
00:13:20,042 --> 00:13:21,292
<i>I miss you.</i>

43
00:13:26,000 --> 00:13:27,333
<i>I love you.</i>

44
00:14:18,458 --> 00:14:21,167
<i>Promise me you will not go over.</i>

45
00:14:21,458 --> 00:14:22,667
<i>I will not leave ... more</i>

46
00:14:24,792 --> 00:14:26,000
<i>never.</i>

47
00:14:26,958 --> 00:14:28,958
<i>- You promise? - Yes!</i>

48
00:14:29,292 --> 00:14:31,583
<i>Promise me you will not go over.</i>

49
00:14:41,958 --> 00:14:43,000
It is safe?

50
00:14:43,708 --> 00:14:47,167
- No, the contrary.
We did! Homer took him.

51
00:14:51,000 --> 00:14:52,708
Are you sure?

52
00:15:29,417 --> 00:15:30,917
<i>I can not believe it!</i>

53
00:15:37,292 --> 00:15:39,708
<i>I feel it move.</i>

54
00:16:00,458 --> 00:16:01,292
<i>We managed.</i>

55
00:16:09,625 --> 00:16:11,875
We must classify it today.

56
00:16:12,708 --> 00:16:14,833
I'm not working this afternoon.

57
00:16:15,125 --> 00:16:16,250
I told you so.

58
00:16:16,542 --> 00:16:17,542
Shit, it's true.

59
00:16:17,833 --> 00:16:20,792
You will do when you can.

60
00:18:27,792 --> 00:18:29,292
- Hello, Eva.
.

61
00:20:10,333 --> 00:20:12,667
Let me go! Leave me alone!

62
00:20:12,958 --> 00:20:14,292
"Do not stop!

63
00:20:14,583 --> 00:20:18,500
Lady, help me! I have done nothing, damn!

64
00:20:18,792 --> 00:20:20,208
Help me! Pity!

65
00:20:20,500 --> 00:20:23,708
Let me go! Do not touch me!

66
00:20:31,292 --> 00:20:33,125
Do not contract.

67
00:20:34,500 --> 00:20:36,500
Do not hold back, Eva.

68
00:22:38,833 --> 00:22:40,375
It's time to get up.

69
00:22:42,250 --> 00:22:43,500
It's late.

70
00:22:44,375 --> 00:22:46,208
The day is almost over.

71
00:22:46,958 --> 00:22:47,958
Good afternoon.

72
00:22:48,250 --> 00:22:52,208
Do not get me, please. He has just fallen asleep.

73
00:22:57,333 --> 00:22:58,833
Cuckoo!

74
00:22:59,417 --> 00:23:00,875
Want to play?

75
00:23:06,667 --> 00:23:07,833
Everything is fine.

76
00:23:10,250 --> 00:23:12,417
You just rock him a little.

77
00:23:14,000 --> 00:23:15,958
You think I dramatizing?

78
00:23:18,500 --> 00:23:20,792
You just rock him a little.

79
00:23:21,292 --> 00:23:23,208
The rocking a little.

80
00:23:24,750 --> 00:23:26,542
My little boy!

81
00:25:09,583 --> 00:25:11,375
The cow! The twelve?

82
00:25:15,458 --> 00:25:18,750
- I take them anyway.
- But they are all broken.

83
00:25:50,417 --> 00:25:52,583
<i>You will be a handsome young man.</i>

84
00:25:53,750 --> 00:25:55,792
<i>The girls will crack.</i>

85
00:26:13,208 --> 00:26:14,833
<i>Do not wake up.</i>

86
00:26:18,958 --> 00:26:20,333
You can say "mama"?

87
00:26:20,625 --> 00:26:22,208
Ma .. man.

88
00:26:23,792 --> 00:26:24,875
Kevin?

89
00:26:26,750 --> 00:26:28,375
You say "ball"?

90
00:26:32,500 --> 00:26:33,458
Not? OK.

91
00:26:34,458 --> 00:26:37,208
I'll run it and you send him back to mom.

92
00:26:39,125 --> 00:26:40,333
Returns it to me.

93
00:26:44,250 --> 00:26:46,583
Returns it to me.

94
00:26:58,458 --> 00:26:59,917
You kick it back?

95
00:27:05,167 --> 00:27:09,833
Baby, he cried constantly, it may have damaged his hearing.

96
00:27:13,792 --> 00:27:15,542
He hears very well.

97
00:27:19,417 --> 00:27:21,333
He should talk. Not?

98
00:27:22,042 --> 00:27:25,292
I read somewhere that does not speak

99
00:27:25,583 --> 00:27:27,750
was a sign of autism.

100
00:27:30,250 --> 00:27:33,000
No symptoms suggest.

101
00:27:35,750 --> 00:27:37,292
Do not worry.

102
00:27:44,583 --> 00:27:46,625
It is not very reactive.

103
00:27:48,583 --> 00:27:50,792
But he's fine.

104
00:28:06,542 --> 00:28:08,208
Returns it to mom.

105
00:28:16,875 --> 00:28:19,083
I am sending. It was very good.

106
00:28:37,625 --> 00:28:40,458
<i>"E" is not very big small elephant</i>

107
00:28:40,750 --> 00:28:44,167
<i>Not really great small as a mouse</i>

108
00:28:44,458 --> 00:28:45,917
You can say "elephant"?

109
00:28:46,250 --> 00:28:47,458
Elephant.

110
00:28:55,583 --> 00:28:57,000
You can say "mama"?

111
00:28:59,125 --> 00:29:00,000
Mom.

112
00:29:01,292 --> 00:29:02,333
Say "mama".

113
00:31:59,875 --> 00:32:01,292
Trick or candy!

114
00:32:05,000 --> 00:32:06,792
We know you are there!

115
00:32:09,708 --> 00:32:11,375
We want candy!

116
00:32:11,958 --> 00:32:14,292
Madam, we want candy!

117
00:32:16,667 --> 00:32:18,208
Candy!

118
00:32:21,333 --> 00:32:23,167
<i>I do not like. I want it.</i>

119
00:32:23,667 --> 00:32:25,292
<i>I want more. I love it!</i>

120
00:32:26,667 --> 00:32:28,000
Mom was happy

121
00:32:28,333 --> 00:32:30,667
before Kevin arrived. You know?

122
00:32:32,250 --> 00:32:33,208
Candy!

123
00:32:33,708 --> 00:32:36,125
Now, mom wakes up every morning

124
00:32:37,500 --> 00:32:39,375
regretting not be very far!

125
00:32:53,042 --> 00:32:54,833
This elevator is too dangerous.

126
00:32:55,417 --> 00:32:58,917
It needs a garden to play ball, a swimming pool.

127
00:33:01,333 --> 00:33:02,792
But I love New York.

128
00:33:04,083 --> 00:33:05,292
Stop

129
00:33:05,583 --> 00:33:07,625
Sounds like a slogan for the city!

130
00:33:08,292 --> 00:33:12,167
There are priorities. He must take his childhood.

131
00:33:12,792 --> 00:33:14,417
Looking for houses if you want.

132
00:33:14,708 --> 00:33:16,792
I will not leave New York.

133
00:33:18,167 --> 00:33:20,833
Kevin, that's enough. Mom and Dad talking.

134
00:33:22,458 --> 00:33:23,417
Stop

135
00:33:28,000 --> 00:33:29,875
<i>There is a huge living room.</i>

136
00:33:30,625 --> 00:33:34,083
<i>The parents room has two huge walk-in closets.</i>

137
00:33:35,125 --> 00:33:37,083
<i>The bathroom is great.</i>

138
00:33:56,542 --> 00:33:57,542
Here we are.

139
00:33:57,833 --> 00:33:59,333
Our little castle!

140
00:34:37,250 --> 00:34:39,458
I'll get to eat.

141
00:34:40,333 --> 00:34:41,958
You want something?

142
00:34:57,583 --> 00:34:59,208
Wait ... Ms. K?

143
00:35:05,750 --> 00:35:07,000
"It's me,

144
00:35:07,333 --> 00:35:08,292
Soweto.

145
00:35:12,708 --> 00:35:13,792
Good afternoon.

146
00:35:16,542 --> 00:35:18,125
How do you go?

147
00:35:21,167 --> 00:35:22,875
I'm well, thank you.

148
00:35:25,125 --> 00:35:26,417
You look like.

149
00:35:27,958 --> 00:35:30,333
The doctors say I remarcherai.

150
00:35:31,708 --> 00:35:32,625
It's great.

151
00:35:36,417 --> 00:35:37,500
Take care of yourself.

152
00:35:39,500 --> 00:35:40,500
You too.

153
00:36:19,542 --> 00:36:21,500
Die!

154
00:36:26,500 --> 00:36:27,958
How do you do to jump?

155
00:36:30,542 --> 00:36:31,500
Die!

156
00:36:36,500 --> 00:36:37,958
Go on!

157
00:36:39,083 --> 00:36:39,958
Die!

158
00:36:51,833 --> 00:36:52,917
Kev, big boy!

159
00:36:53,417 --> 00:36:54,083
Come here.

160
00:36:56,208 --> 00:36:59,292
You see, Dad peed in the toilet.

161
00:37:00,958 --> 00:37:02,625
You can do it, too.

162
00:37:26,250 --> 00:37:28,958
You want to go there.

163
00:37:29,292 --> 00:37:32,625
You see? You click on the link and it opens by itself.

164
00:37:32,917 --> 00:37:34,375
And if it does not open,

165
00:37:35,500 --> 00:37:36,958
you go again.

166
00:37:37,500 --> 00:37:39,875
You must click on the left. You try?

167
00:37:41,500 --> 00:37:45,583
No, right click. That way, it's good.

168
00:37:46,750 --> 00:37:48,167
So, it opens.

169
00:37:48,750 --> 00:37:49,792
super gasoline

170
00:37:50,208 --> 00:37:52,125
Thank you. I understood.

171
00:37:53,125 --> 00:37:54,500
Thank you for your help.

172
00:38:11,958 --> 00:38:13,208
I pooped.

173
00:38:14,500 --> 00:38:15,708
What?

174
00:38:17,625 --> 00:38:18,792
You pooped?

175
00:38:20,583 --> 00:38:23,125
I come right away.

176
00:38:25,208 --> 00:38:27,125
Go to your room, I'm coming.

177
00:38:28,250 --> 00:38:30,000
We let mom sleep.

178
00:38:30,792 --> 00:38:31,958
Go ahead, my great.

179
00:39:08,292 --> 00:39:11,417
All these pieces of paper scrawled, this is stupid.

180
00:39:16,167 --> 00:39:18,708
Everyone must have his play.

181
00:39:19,042 --> 00:39:20,958
You have your room and that's mine.

182
00:39:21,292 --> 00:39:23,958
I can help you to make you an original room.

183
00:39:24,250 --> 00:39:25,667
How is that possible?"

184
00:39:28,250 --> 00:39:30,625
A room that reflects your personality.

185
00:39:32,458 --> 00:39:34,208
What personality?

186
00:39:36,708 --> 00:39:38,917
You know what I mean.

187
00:39:41,250 --> 00:39:42,375
This is stupid.

188
00:40:07,167 --> 00:40:08,667
It's great.

189
00:40:10,083 --> 00:40:11,875
You do not want me to do?

190
00:40:12,917 --> 00:40:14,542
And type ...

191
00:40:20,125 --> 00:40:21,792
If you take care of everything ...

192
00:40:22,667 --> 00:40:23,750
I could.

193
00:40:24,250 --> 00:40:25,292
We did! Homer took him.

194
00:40:29,500 --> 00:40:32,208
That sounds perfect.

195
00:40:33,667 --> 00:40:35,583
See you later. Goodbye.

196
00:41:58,000 --> 00:41:59,625
He is really sorry.

197
00:42:01,333 --> 00:42:04,000
He wanted to make you an original piece.

198
00:42:28,708 --> 00:42:31,083
You remember how you got that?

199
00:42:33,042 --> 00:42:35,667
This is the only sincere thing you've ever done.

200
00:42:38,250 --> 00:42:41,458
How we teach cats to relieve themselves?

201
00:42:41,750 --> 00:42:44,083
They put their noses in their shit.

202
00:42:46,542 --> 00:42:48,083
They like it.

203
00:42:48,625 --> 00:42:50,542
Then they go in their litter.

204
00:42:53,417 --> 00:42:54,708
Figures are reviewed.

205
00:42:55,750 --> 00:42:58,292
- What is it after 3?
9.

206
00:43:00,833 --> 00:43:03,542
- After 7?
- 71.

207
00:43:26,750 --> 00:43:28,542
Got it? We stop?

208
00:43:39,000 --> 00:43:39,917
Here

209
00:43:40,333 --> 00:43:43,458
Add me that since you are so smart.

210
00:43:58,417 --> 00:43:59,833
You did not do that!

211
00:44:02,333 --> 00:44:03,625
You did not do that!

212
00:44:25,000 --> 00:44:26,375
It amuses you?

213
00:44:59,542 --> 00:45:01,792
I can see the doctor alone.

214
00:45:18,250 --> 00:45:20,500
I have something to say.

215
00:45:21,708 --> 00:45:23,833
You have a very brave son.

216
00:45:54,917 --> 00:45:58,708
What Mom did, it was very bad,

217
00:46:00,625 --> 00:46:03,500
and she regrets it very much.

218
00:46:17,958 --> 00:46:19,292
Wait!

219
00:46:26,042 --> 00:46:27,250
Hi, Kev.

220
00:46:27,542 --> 00:46:29,083
What happened?

221
00:46:29,458 --> 00:46:31,083
I have broken my arm.

222
00:46:31,417 --> 00:46:32,458
 How ?

223
00:46:32,750 --> 00:46:34,458
I pooped in my bed

224
00:46:34,750 --> 00:46:36,917
Mom went to get the wipes.

225
00:46:37,208 --> 00:46:40,292
I fell off the changing table in my garbage truck.

226
00:46:40,583 --> 00:46:43,250
Mom took me to the doctor Goldbutt.

227
00:46:49,958 --> 00:46:51,333
That's my fault.

228
00:46:54,458 --> 00:46:55,750
Do not talk nonsense.

229
00:46:56,542 --> 00:46:58,958
You can not watch him constantly.

230
00:47:44,917 --> 00:47:46,042
Crazy!

231
00:47:48,167 --> 00:47:49,792
How you managed?

232
00:47:54,458 --> 00:47:56,333
Anyway, it worked.

233
00:48:11,708 --> 00:48:12,833
Expect.

234
00:48:24,833 --> 00:48:26,042
Are you OK?

235
00:48:45,292 --> 00:48:46,167
I love you.

236
00:49:54,792 --> 00:49:57,167
I do not like this music. Turn off.

237
00:50:09,417 --> 00:50:11,708
Honey, you can stop at the supermarket?

238
00:50:12,458 --> 00:50:13,917
I want to go.

239
00:50:15,792 --> 00:50:17,708
I must buy ...

240
00:50:26,583 --> 00:50:27,792
We're going home.

241
00:50:46,875 --> 00:50:47,833
Good evening.

242
00:50:48,750 --> 00:50:49,625
Best wishes!

243
00:50:50,417 --> 00:50:52,292
You took some great photos?

244
00:51:03,125 --> 00:51:05,083
Eva is a child.

245
00:51:07,042 --> 00:51:09,167
A nice little boy.

246
00:51:09,750 --> 00:51:11,417
Like all others.

247
00:52:05,458 --> 00:52:06,542
Mom is big.

248
00:52:12,917 --> 00:52:14,458
Counted when you tell me?

249
00:52:14,750 --> 00:52:16,542
Soon. Now.

250
00:52:18,750 --> 00:52:20,958
You took the decision without me.

251
00:52:21,750 --> 00:52:23,958
I'm supposed to say what, now?

252
00:52:31,667 --> 00:52:34,458
Daddy Bear puts small seed

253
00:52:35,000 --> 00:52:37,833
the mama bear, it becomes an egg.

254
00:52:38,417 --> 00:52:39,917
That's right, kiss?

255
00:52:43,583 --> 00:52:45,333
{0}Do you know what that{/0} {0}means?{/0}

256
00:52:46,583 --> 00:52:49,333
The boy puts his penis in the girl's pussy.

257
00:52:54,500 --> 00:52:58,000
You never wanted to have someone to play with?

258
00:52:59,500 --> 00:53:00,625
That you might like.

259
00:53:00,917 --> 00:53:02,417
And if I like it not?

260
00:53:03,083 --> 00:53:04,750
Then you'll get used.

261
00:53:05,500 --> 00:53:08,417
It can be used to something without necessarily loving.

262
00:53:08,708 --> 00:53:10,042
You're used to me.

263
00:53:16,417 --> 00:53:17,375
In a few months

264
00:53:17,667 --> 00:53:19,667
you get used to someone new.

265
00:53:25,417 --> 00:53:26,458
She is beautiful!

266
00:53:28,750 --> 00:53:30,458
Come see your little sister.

267
00:53:30,750 --> 00:53:32,333
This is your big brother.

268
00:53:32,833 --> 00:53:34,042
Kevin.

269
00:53:35,250 --> 00:53:36,917
Your big brother.

270
00:53:40,958 --> 00:53:42,125
This is Kevin.

271
00:53:45,583 --> 00:53:46,917
Do not do that.

272
00:53:49,417 --> 00:53:50,750
It's not nice.

273
00:53:51,042 --> 00:53:54,792
We go down and buy food. Come.

274
00:54:08,708 --> 00:54:13,333
<i>Ecuador? For 2 months? Them send someone else!</i>

275
00:54:14,250 --> 00:54:16,917
<i>I would really like to ensure.</i>

276
00:54:19,833 --> 00:54:22,042
<i>He cries too, and after he said nothing.</i>

277
00:54:22,333 --> 00:54:25,083
<i>He invents his own language and it bothers you.</i>

278
00:54:25,833 --> 00:54:28,292
<i>It has layers, it is not abnormal.</i>

279
00:54:29,625 --> 00:54:31,292
<i>This is not good for him.</i>

280
00:54:35,167 --> 00:54:36,625
<i>Unhook the fuck!</i>

281
00:54:37,750 --> 00:54:39,292
<i>Wins!</i>

282
00:54:51,083 --> 00:54:53,333
<i>Getting ready for the party!</i>

283
00:54:59,208 --> 00:55:00,167
Please!

284
00:55:00,500 --> 00:55:02,458
Give me to drink.

285
00:55:02,750 --> 00:55:05,333
I beg you.

286
00:55:05,875 --> 00:55:07,125
Please do.

287
00:55:19,458 --> 00:55:20,625
My heart!

288
00:55:50,875 --> 00:55:52,292
Sorry, Mom.

289
00:55:54,042 --> 00:55:55,750
Do not worry, darling.

290
00:55:56,417 --> 00:55:58,083
It was not your fault.

291
00:56:01,500 --> 00:56:03,208
"By my soul!" exclaimed Gilbert.

292
00:56:03,500 --> 00:56:06,083
"Are not you the Devil whether to aim well?"

293
00:56:06,417 --> 00:56:08,417
"No," said Robin, laughing.

294
00:56:08,708 --> 00:56:11,042
"I'm not as bad as that."

295
00:56:11,333 --> 00:56:12,792
And there went out another arrow.

296
00:56:13,083 --> 00:56:14,500
That's okay, darling?

297
00:56:21,167 --> 00:56:24,375
He took out another arrow and adjusted it on the rope.

298
00:56:24,667 --> 00:56:29,250
He drew back and threw the arrow next to the center of the target.

299
00:56:29,542 --> 00:56:32,417
The third time he bent the bow and sent the arrow

300
00:56:32,708 --> 00:56:35,375
right next to the other two in the middle.

301
00:56:35,667 --> 00:56:38,083
The feathers of three arrows

302
00:56:38,417 --> 00:56:41,417
were gathered as a large bouquet.

303
00:56:42,250 --> 00:56:45,708
A long murmur ran through the crowd.

304
00:56:46,000 --> 00:56:47,042
Hello, my grandfather.

305
00:56:49,167 --> 00:56:50,167
What's up?

306
00:56:51,708 --> 00:56:53,458
Go away. I'm tired.

307
00:56:58,542 --> 00:56:59,833
Okay. Sleep well.

308
00:57:01,750 --> 00:57:03,458
Continues to read, Mom.

309
00:57:10,667 --> 00:57:12,292
He threw 280 arrows

310
00:57:12,583 --> 00:57:15,667
with such speed and accuracy

311
00:57:15,958 --> 00:57:17,750
once planted,

312
00:57:18,042 --> 00:57:21,500
targets resembled the back of a hedgehog

313
00:57:21,792 --> 00:57:23,958
returned from a hunting dog.

314
00:58:14,042 --> 00:58:14,833
Hello Madam,

315
00:58:15,417 --> 00:58:17,292
I hope it does not bother you.

316
00:58:17,583 --> 00:58:18,792
In fact, if.

317
00:58:19,083 --> 00:58:21,167
We had some questions.

318
00:58:21,958 --> 00:58:22,958
About what?

319
00:58:24,250 --> 00:58:25,958
Where will you go when you die?

320
00:58:28,333 --> 00:58:30,292
I know very well.

321
00:58:30,583 --> 00:58:32,000
I'll live in hell.

322
00:58:32,292 --> 00:58:34,208
Eternal damnation.

323
00:58:34,500 --> 00:58:35,958
Thank you for your question.

324
00:58:45,292 --> 00:58:46,708
I'll help.

325
00:58:48,083 --> 00:58:50,333
I know how to dress himself. You can go out?

326
00:58:53,542 --> 00:58:54,458
Yes.

327
00:58:55,958 --> 00:58:59,667
Glad you are better. You want soup for lunch?

328
00:58:59,958 --> 00:59:01,042
I do not care.

329
00:59:01,792 --> 00:59:04,667
- A pop tart?
- I do not care a lot.

330
00:59:15,250 --> 00:59:17,125
Thank you, Dad. This is too cool.

331
00:59:17,417 --> 00:59:18,292
You're welcome.

332
00:59:18,583 --> 00:59:20,417
Seeks the middle.

333
00:59:21,583 --> 00:59:22,750
Almost.

334
00:59:26,042 --> 00:59:27,875
Seeks the middle.

335
01:00:28,083 --> 01:00:29,500
Bravo, Kevin.

336
01:00:30,167 --> 01:00:31,458
It is innate within you.

337
01:00:34,250 --> 01:00:35,333
It was perfect.

338
01:00:36,042 --> 01:00:37,000
Thank you, Dad.

339
01:01:18,917 --> 01:01:19,833
<i>That's how ...</i>

340
01:01:22,750 --> 01:01:23,833
<i>You wake</i>

341
01:01:24,792 --> 01:01:26,333
<i>and we watch TV.</i>

342
01:01:28,125 --> 01:01:31,208
<i>It gets into his car and listening to the radio.</i>

343
01:01:32,042 --> 01:01:35,625
<i>It goes quietly to his job or his school,</i>

344
01:01:36,417 --> 01:01:39,708
<i>but that, we do not hear about the news of 18h.</i>

345
01:01:41,542 --> 01:01:42,583
<i>Why?</i>

346
01:01:43,542 --> 01:01:45,583
<i>Because nothing happens.</i>

347
01:01:47,917 --> 01:01:49,917
<i>It comes home and puts it on TV.</i>

348
01:01:50,208 --> 01:01:51,917
<i>Or if it's a fantastic evening,</i>

349
01:01:52,250 --> 01:01:54,750
<i>we go out and goes to cinema.</i>

350
01:01:56,917 --> 01:02:01,250
It sucks so much that very often people on TV,

351
01:02:01,542 --> 01:02:04,917
those that one is watching, watching TV.

352
01:02:07,250 --> 01:02:09,375
And what do they look?

353
01:02:14,667 --> 01:02:16,208
People like me.

354
01:02:19,917 --> 01:02:22,417
<i>You, for example, what do you do?</i>

355
01:02:23,625 --> 01:02:24,917
<i>You look at me.</i>

356
01:02:28,625 --> 01:02:33,125
<i>They had already changed string if I was a model student.</i>

357
01:03:22,000 --> 01:03:24,667
With Kevin, we play with Christmas kidnapping.

358
01:03:28,833 --> 01:03:29,833
You are my friend.

359
01:03:31,333 --> 01:03:32,417
Stops, Celia.

360
01:03:32,708 --> 01:03:34,333
Give me a soda.

361
01:03:44,792 --> 01:03:46,167
No one, moron.

362
01:03:47,000 --> 01:03:48,000
A Root Beer.

363
01:03:49,833 --> 01:03:50,625
What?

364
01:03:55,667 --> 01:03:57,792
Thank you, Celia. I was thirsty.

365
01:04:01,542 --> 01:04:03,292
I tell you a secret?

366
01:04:03,625 --> 01:04:04,708
Approach

367
01:04:05,167 --> 01:04:06,667
Who else is thirsty?

368
01:04:07,167 --> 01:04:08,208
You know?

369
01:04:10,958 --> 01:04:13,042
The monster vacuum cleaner.

370
01:04:13,875 --> 01:04:15,167
Stop

371
01:04:19,083 --> 01:04:19,917
Hello.

372
01:04:23,083 --> 01:04:24,458
Look what I brought!

373
01:04:24,875 --> 01:04:27,000
- You just help me?
- I arrive.

374
01:05:49,250 --> 01:05:50,375
You dance?

375
01:05:52,000 --> 01:05:53,208
No thank you, Colin.

376
01:05:55,667 --> 01:05:57,125
Ok, a little dance.

377
01:05:58,125 --> 01:05:59,875
I love to dance.

378
01:06:11,500 --> 01:06:14,875
What excites you, kind of pretentious bitch?

379
01:06:17,708 --> 01:06:20,375
What want you crazy?

380
01:06:39,292 --> 01:06:41,292
Remains. Another glass timber.

381
01:07:04,458 --> 01:07:05,583
Are you happy?

382
01:07:06,667 --> 01:07:09,083
It will leave a carrot for the reindeer?

383
01:08:12,000 --> 01:08:15,125
I saw you at the bookstore today.

384
01:08:17,958 --> 01:08:18,875
No !

385
01:08:20,667 --> 01:08:22,458
Funny, I could have sworn ...

386
01:08:28,750 --> 01:08:31,208
In fact, you do something on Friday?

387
01:08:35,167 --> 01:08:36,208
Why?

388
01:08:39,542 --> 01:08:41,792
It could be something

389
01:08:42,500 --> 01:08:43,792
of fun.

390
01:08:45,542 --> 01:08:46,875
Like that?

391
01:08:48,583 --> 01:08:50,167
Go to a restaurant?

392
01:08:51,583 --> 01:08:54,000
Make a mini-golf course?

393
01:09:24,958 --> 01:09:26,667
You did not take a jacket?

394
01:09:28,667 --> 01:09:31,417
You always do everything possible not to be comfortable.

395
01:09:31,708 --> 01:09:32,958
Uncomfortable?

396
01:09:33,417 --> 01:09:34,708
With my mother?

397
01:09:46,167 --> 01:09:48,958
Large are always eating.

398
01:09:49,458 --> 01:09:53,292
This is not because of a slow metabolism or crap like that.

399
01:09:53,625 --> 01:09:54,708
This is the food.

400
01:09:55,000 --> 01:09:56,583
They eat anything.

401
01:09:56,875 --> 01:09:59,167
They eat too much and all the time.

402
01:10:03,667 --> 01:10:06,000
You can be very hard sometimes.

403
01:10:07,042 --> 01:10:08,583
You look great thing to say.

404
01:10:08,875 --> 01:10:10,750
Exactly!

405
01:10:12,292 --> 01:10:14,167
I wonder who I am.

406
01:10:32,875 --> 01:10:33,833
You won.

407
01:10:56,667 --> 01:10:58,458
And now, Mamounette?

408
01:10:59,583 --> 01:11:03,792
We go back, you put clothes more appropriate and we will dine.

409
01:11:05,417 --> 01:11:06,333
super gasoline

410
01:11:20,167 --> 01:11:21,625
Go to the restaurant.

411
01:11:24,417 --> 01:11:25,500
I was hungry.

412
01:11:26,500 --> 01:11:28,458
I'm growing.

413
01:11:37,833 --> 01:11:39,208
I will

414
01:11:40,667 --> 01:11:42,167
a salad of calamari,

415
01:11:43,625 --> 01:11:46,875
a steak and a bottle to the point of merlot.

416
01:11:48,375 --> 01:11:49,542
Immediately.

417
01:12:04,875 --> 01:12:06,333
It's going to school?

418
01:12:11,583 --> 01:12:12,667
I am Okay.

419
01:12:14,292 --> 01:12:16,083
Want my schedule?

420
01:12:18,292 --> 01:12:19,125
And your teachers?

421
01:12:19,958 --> 01:12:23,708
- There you ...
- I listen to anything, like music?

422
01:12:25,625 --> 01:12:30,208
Ask me if there is a hottie also interests me.

423
01:12:31,792 --> 01:12:33,042
This will bring you

424
01:12:33,750 --> 01:12:36,958
to say that it looks at me, but before the fuck,

425
01:12:37,250 --> 01:12:39,375
I must be sure you are ready.

426
01:12:41,000 --> 01:12:43,708
For dessert, you aborderas the subject of drugs.

427
01:12:44,000 --> 01:12:45,500
Gently, without scare me,

428
01:12:45,792 --> 01:12:47,167
by lying to me.

429
01:12:47,458 --> 01:12:50,167
You tell me your own experience.

430
01:12:53,208 --> 01:12:55,542
And when you whistled your bottle,

431
01:12:55,833 --> 01:13:00,167
you will give me the soft eyes and said, "It's good, you two!"

432
01:13:00,833 --> 01:13:03,500
You get closer you and me enlaceras

433
01:13:04,250 --> 01:13:05,750
shaking my little one.

434
01:14:53,458 --> 01:14:56,375
<i>YOU HAVE LOST</i>

435
01:15:11,333 --> 01:15:14,083
Your computer is broken?

436
01:15:14,458 --> 01:15:15,750
Yes.

437
01:15:16,250 --> 01:15:18,667
All of the office too.

438
01:15:19,500 --> 01:15:21,000
I earned it.

439
01:15:26,083 --> 01:15:28,375
Why did you do this stuff?

440
01:15:29,292 --> 01:15:30,708
I collect them.

441
01:15:34,250 --> 01:15:36,375
Bizarre, as a collection.

442
01:15:36,875 --> 01:15:38,792
I do not like stamps.

443
01:15:42,542 --> 01:15:43,458
What is the advantage?

444
01:15:47,208 --> 01:15:48,542
there aren't two, 

445
01:15:49,708 --> 01:15:51,083
That's the interest.

446
01:15:56,250 --> 01:15:58,375
It was easy to organize.

447
01:15:58,667 --> 01:16:00,542
Everyone brings a dish.

448
01:16:10,958 --> 01:16:13,000
They arrive. I leave you.

449
01:16:13,708 --> 01:16:15,125
Have a nice day.

450
01:16:17,250 --> 01:16:18,417
Goodbye, Mom.

451
01:16:20,542 --> 01:16:21,583
Merry Christmas!

452
01:16:39,042 --> 01:16:40,583
To you, Kevin.

453
01:16:42,083 --> 01:16:43,458
Guess what it is.

454
01:16:48,875 --> 01:16:50,292
This is an arc of seven series.

455
01:16:50,542 --> 01:16:52,875
The seller told me it was the best.

456
01:16:53,167 --> 01:16:54,333
Daddy!

457
01:16:55,208 --> 01:16:56,375
Thank you very much!

458
01:16:57,333 --> 01:16:58,625
It's great.

459
01:17:05,958 --> 01:17:08,458
What you eat in the morning?

460
01:17:10,625 --> 01:17:11,833
I don't actually know...

461
01:17:31,750 --> 01:17:33,333
You heard the impact?

462
01:17:51,708 --> 01:17:54,375
My dear, it's time to sleep.

463
01:18:04,417 --> 01:18:06,500
I thought I had closed the door.

464
01:18:09,292 --> 01:18:11,958
Kevin says I'm stupid, he's right.

465
01:18:13,542 --> 01:18:15,000
You're not stupid.

466
01:18:17,458 --> 01:18:19,750
Do not worry, we will find him.

467
01:18:20,458 --> 01:18:21,333
Put yourself to bed.

468
01:18:27,667 --> 01:18:28,958
You are well?

469
01:19:02,250 --> 01:19:03,333
Snuffles?

470
01:19:05,667 --> 01:19:06,792
Admit it.

471
01:19:07,042 --> 01:19:10,125
Mr. Snuffles went into the great pet in heaven.

472
01:19:10,417 --> 01:19:11,708
Do not say that.

473
01:19:12,000 --> 01:19:13,292
This is the truth.

474
01:19:13,958 --> 01:19:15,083
"My dear friends,

475
01:19:15,375 --> 01:19:17,708
"We are here to pray for Mr. Snuffles ...

476
01:19:20,417 --> 01:19:24,792
"... Who died in a tragic way ..."

477
01:19:25,708 --> 01:19:29,500
Mom and Dad have searched all night,

478
01:19:30,333 --> 01:19:32,000
and we did not find,

479
01:19:32,583 --> 01:19:35,667
because he went to live in the garden.

480
01:19:37,000 --> 01:19:41,208
He lives in the garden with all its new animal friends.

481
01:19:51,208 --> 01:19:52,542
Give your spoon.

482
01:19:54,292 --> 01:19:56,583
They make a big party tonight.

483
01:19:58,333 --> 01:20:00,000
With rodents,

484
01:20:00,292 --> 01:20:02,000
squirrels

485
01:20:02,250 --> 01:20:04,042
and raccoons.

486
01:20:04,583 --> 01:20:08,583
They eat nuts and berries ...

487
01:20:09,458 --> 01:20:10,792
And they'll do

488
01:20:11,125 --> 01:20:12,542
high ...

489
01:21:30,042 --> 01:21:32,292
Why you left out the Destop?

490
01:21:34,625 --> 01:21:35,750
I have not left.

491
01:21:36,542 --> 01:21:39,083
- I had stored.
- How is it released?

492
01:21:49,083 --> 01:21:50,167
Kevin.

493
01:21:51,250 --> 01:21:52,417
This is Kevin.

494
01:21:56,542 --> 01:21:58,792
You should go see a shrink.

495
01:23:18,000 --> 01:23:20,417
Your mother has something to tell you.

496
01:23:30,792 --> 01:23:32,167
I wanted ...

497
01:23:34,542 --> 01:23:37,125
thank you for calling the ambulance.

498
01:23:40,833 --> 01:23:41,750
and

499
01:23:47,542 --> 01:23:49,458
I fear that you feel

500
01:23:50,250 --> 01:23:52,292
Editor:

501
01:23:54,458 --> 01:23:55,583
Why?

502
01:23:57,542 --> 01:23:59,417
Because you had the monitor!

503
01:23:59,958 --> 01:24:02,000
You have nothing to reproach yourself.

504
01:24:03,542 --> 01:24:04,500
Are you OK?

505
01:24:05,417 --> 01:24:06,708
I blame myself for anything.

506
01:24:07,917 --> 01:24:10,417
She'll have a glass eye.

507
01:24:11,583 --> 01:24:14,000
We would like you to be nice to her.

508
01:24:14,250 --> 01:24:15,958
You do not insults.

509
01:24:18,458 --> 01:24:21,750
Do not you remember you were a kid, too?

510
01:24:22,667 --> 01:24:24,750
Celia is going to cash it.

511
01:24:32,500 --> 01:24:33,583
You do not like lychees.

512
01:24:35,125 --> 01:24:37,500
Say I ...

513
01:24:41,292 --> 01:24:42,875
I appreciate now.

514
01:25:40,750 --> 01:25:42,792
Pardon me, my angel.

515
01:26:04,458 --> 01:26:05,708
Thank you, Mom.

516
01:26:20,250 --> 01:26:21,958
You go back training?

517
01:26:22,292 --> 01:26:23,375
You know me!

518
01:26:23,667 --> 01:26:25,458
Celia, you just pick up the arrows?

519
01:26:51,167 --> 01:26:52,375
What do you do?

520
01:26:59,708 --> 01:27:02,042
We will finish the school year

521
01:27:04,417 --> 01:27:06,667
and arrange this summer.

522
01:27:09,958 --> 01:27:12,667
Child care, at least it's done.

523
01:27:15,000 --> 01:27:16,000
We did! Homer took him.

524
01:27:19,542 --> 01:27:20,792
You decided.

525
01:27:23,500 --> 01:27:25,542
there is nothing to see here.

526
01:27:28,417 --> 01:27:29,667
This is obvious.

527
01:27:41,792 --> 01:27:43,417
I want a glass of water.

528
01:27:47,542 --> 01:27:48,667
My great,

529
01:27:49,292 --> 01:27:52,708
we must put things in context.

530
01:27:56,667 --> 01:27:58,625
I know the background.

531
01:28:01,500 --> 01:28:03,208
It's me, the context.

532
01:28:21,417 --> 01:28:23,417
We will appeal.

533
01:28:24,125 --> 01:28:26,167
You lose your job, your house ...

534
01:28:26,417 --> 01:28:28,792
I always hated that house.

535
01:28:50,500 --> 01:28:52,875
A package for Kevin Khatcha ... Durian?

536
01:29:00,583 --> 01:29:02,708
- Warning, it's heavy.
We did! Homer took him.

537
01:29:05,417 --> 01:29:07,208
Best wishes!
- You too.

538
01:29:35,833 --> 01:29:38,125
You're going to do what? You do not bike.

539
01:29:39,125 --> 01:29:41,708
I bought cheap on the Internet.

540
01:29:42,000 --> 01:29:44,458
I will be selling them wheat.

541
01:29:45,458 --> 01:29:47,375
You think you're Donald Trump?

542
01:30:19,625 --> 01:30:20,917
Come with me.

543
01:30:27,417 --> 01:30:29,292
I'm back on my ship.

544
01:31:58,500 --> 01:31:59,917
Ready for the reverse?

545
01:32:12,417 --> 01:32:13,708
Pleased to meet you.

546
01:32:22,125 --> 01:32:23,542
<i>{0} I{/0} {0}am ready!{/0}</i>

547
01:32:23,833 --> 01:32:26,500
<i>- You ate your cereal? - Yes.</i>

548
01:32:28,542 --> 01:32:29,417
<i>And your bread?</i>

549
01:32:36,292 --> 01:32:37,750
You're a little feverish.

550
01:32:38,042 --> 01:32:39,292
You feel good?

551
01:32:40,042 --> 01:32:41,458
Better than ever.

552
01:32:44,750 --> 01:32:47,417
In three days you shall have 16 years.

553
01:32:48,167 --> 01:32:50,417
We should celebrate that. Sunday?

554
01:32:51,542 --> 01:32:52,417
I don't actually know...

555
01:32:53,708 --> 01:32:55,667
I may be taken elsewhere.

556
01:32:57,167 --> 01:32:59,208
The heading and we dress.

557
01:32:59,958 --> 01:33:01,125
I want to go to school.

558
01:33:01,417 --> 01:33:04,500
I know, but I'll take my shower and after we go.

559
01:33:04,792 --> 01:33:05,958
Say goodbye.

560
01:33:17,208 --> 01:33:20,833
<i>It might be to talk? Without children.</i>

561
01:33:22,208 --> 01:33:24,625
<i>I would really like to ensure.</i>

562
01:33:26,500 --> 01:33:28,375
Kevin Gladstone goes to school?

563
01:34:07,333 --> 01:34:09,083
<i>Unhook the fuck!</i>

564
01:34:51,750 --> 01:34:52,708
Have you seen Kevin?

565
01:43:34,292 --> 01:43:36,042
You did not look happy.

566
01:43:42,125 --> 01:43:44,042
I've had look happy?

567
01:43:47,583 --> 01:43:49,167
I'm almost 18 years.

568
01:43:53,958 --> 01:43:55,500
What's wrong?

569
01:43:55,792 --> 01:43:58,792
Go into the big leagues, it makes you afraid?

570
01:44:00,875 --> 01:44:02,042
Fear?

571
01:44:04,750 --> 01:44:07,542
You're already gone to such places?

572
01:44:17,042 --> 01:44:19,292
You did very well out.

573
01:44:20,917 --> 01:44:23,292
You were when you were considered minor

574
01:44:23,708 --> 01:44:26,292
for a moment of madness because of Prozac.

575
01:44:28,208 --> 01:44:30,292
You will be released within two years.

576
01:44:37,625 --> 01:44:40,625
You know what day it is? Why I am on a Monday?

577
01:44:43,250 --> 01:44:44,375
Obviously.

578
01:44:45,333 --> 01:44:47,125
This is the anniversary date.

579
01:44:49,208 --> 01:44:50,333
Two years!

580
01:44:53,250 --> 01:44:55,208
It gives you time to think.

581
01:45:09,458 --> 01:45:11,500
I want you to tell me

582
01:45:12,958 --> 01:45:14,000
Why?

583
01:45:34,500 --> 01:45:36,333
Before, I thought I knew ...

584
01:45:39,792 --> 01:45:41,500
But I do not know.

585
01:46:03,458 --> 01:46:04,583
It's time.

586
01:47:06,083 --> 01:47:08,375
Adaptation: Isabelle Juhasz

587
01:47:08,833 --> 01:47:11,167
Subtitling TVS - TITRA FILM
Translated To English - Produksiz

