1
00:00:10,040 --> 00:00:15,000
♫<i>Bleak winds roar in the wilderness</i>♫

2
00:00:15,000 --> 00:00:19,790
♫<i>Coldness numbs the senses</i>♫

3
00:00:19,790 --> 00:00:24,940
♫<i>The haze of obscurity haunts our path</i>♫

4
00:00:24,940 --> 00:00:29,970
♫<i>Darkness falls upon us</i>♫

5
00:00:29,970 --> 00:00:35,100
♫<i>Let pain pervade our flesh</i>♫

6
00:00:35,100 --> 00:00:39,990
♫<i>Let cold congeal our blood</i>♫

7
00:00:39,990 --> 00:00:45,300
♫<i>Let havoc crush us down to the ground</i>♫

8
00:00:45,300 --> 00:00:51,070
♫<i>But we will still be one</i>♫

9
00:01:04,620 --> 00:01:11,280
Over hundreds of years, the story of demons never ceased.

10
00:01:11,280 --> 00:01:17,860
But no one has ever met one, nevertheless knew what they looked like.

11
00:01:20,020 --> 00:01:26,590
They have always been described as being ridiculously beautiful and evil.

12
00:01:32,370 --> 00:01:38,060
They chased them while simultaneously tried to destroy them.

13
00:01:39,400 --> 00:01:45,180
They appeared in many dramas, movies, and dances yet also disappeared.

14
00:01:45,180 --> 00:01:52,730
People always wondered if they really did appear, what will they do?

15
00:01:58,030 --> 00:02:03,620
two two three four. Do it properly.

16
00:02:03,620 --> 00:02:09,520
Come. Let's do the next steps. 3, 2, 3, 4,

17
00:02:09,520 --> 00:02:13,130
5, 6...

18
00:02:20,890 --> 00:02:26,180
Wait a moment. Where are you guys from. We live beneath the bridge.

19
00:02:26,180 --> 00:02:29,350
What about your parents? My dad is pulling a rickshaw.

20
00:02:29,350 --> 00:02:36,380
My dad is helping someone fix his house. I don't have parents, I live with my uncle and aunt.

21
00:02:36,380 --> 00:02:39,430
Do you guys all like dancing? Yes.

22
00:02:39,430 --> 00:02:46,660
But learning dancing is very expensive, we barely have enough to eat. How would we have an opportunity.

23
00:02:47,760 --> 00:02:52,720
I'm the teacher. I can tell that you all really like dancing, right?

24
00:02:52,720 --> 00:02:58,400
Right! If you guys promise me to learn sincerely then I will let you guys learn okay?

25
00:02:58,400 --> 00:03:03,130
Okay! Then hurry and come in.

26
00:03:09,640 --> 00:03:16,960
Okay, everyone get ready. five, six, seven , eight.

27
00:03:16,960 --> 00:03:20,800
five six seven eight.

28
00:03:22,300 --> 00:03:28,060
Dr. Jiang especially invited me for some tea. What is concerning you?

29
00:03:29,440 --> 00:03:36,820
President Li is the older brother of Shang Hai's newspaper's Long Tou. I invited you over for some tea so that we can talk about our feelings.

30
00:03:36,820 --> 00:03:42,710
You're the one who's a Shanghai celebrity. If not because you were straightforward first,

31
00:03:42,710 --> 00:03:45,150
I dare not drink this tea.

32
00:03:45,150 --> 00:03:51,420
A few days ago, I performed a surgery. Many reporters came to cover it.

33
00:03:51,420 --> 00:03:55,210
I heard that it will be the weekend's front page.

34
00:03:55,210 --> 00:03:58,340
I especially came here to thank you.

35
00:03:58,340 --> 00:04:05,400
Dr. Jian's medical skills are exquisite.

36
00:04:05,400 --> 00:04:12,010
But about this weekend's front page, I think you have misunderstood.

37
00:04:12,010 --> 00:04:17,040
Is it possible that there can be some bigger news in Shang Hai?

38
00:04:19,080 --> 00:04:26,210
<i>Timing & subtitles brought to you by Gone with the Shirt Team @ Viki </i>

39
00:04:31,080 --> 00:04:36,510
Nie Qing Cheng. You know her too?

40
00:04:36,510 --> 00:04:43,380
I have met her a few times. Do you know her well?

41
00:04:43,380 --> 00:04:47,880
I don't even recognize her. I've only seen that picture of her.

42
00:04:47,880 --> 00:04:52,690
You also know that the situation outside is very chaotic. Either there's war or there's gang fighting for territories.

43
00:04:52,690 --> 00:04:57,760
I think it's probably best to write about something more mellow.

44
00:04:57,760 --> 00:05:01,710
That's why I decided to use this photograph as the headline for the weekend edition.

45
00:05:01,710 --> 00:05:06,720
This time, I can only apologize to you.

46
00:05:06,720 --> 00:05:11,800
It's nothing. I have to leave it to President Li for being so considerate.

47
00:05:11,800 --> 00:05:17,680
There will be plenty of time for us. Yes.

48
00:05:21,970 --> 00:05:24,930
So in conclusion, our profits for this month is good enough.

49
00:05:24,930 --> 00:05:29,950
Along with the president's unbiased-

50
00:05:29,950 --> 00:05:33,620
What I what isn't an unbiased view.

51
00:05:33,620 --> 00:05:36,620
I want exaggeration.

52
00:05:37,650 --> 00:05:43,020
- The more you are hasty, the more you won't get there.<br>- The matter depends on the individual.

53
00:05:52,820 --> 00:05:57,850
Or perhaps, there's one more option we can try.

54
00:05:58,960 --> 00:06:02,000
What method?

55
00:06:12,960 --> 00:06:16,390
Hello? Hello is this Mr. Luo?

56
00:06:16,390 --> 00:06:19,880
Yes and this is? I am Dr. Jiang.

57
00:06:19,880 --> 00:06:24,110
Dr. Jiang, is there anything you need? I want to order your medicine.

58
00:06:24,110 --> 00:06:28,970
Of course. Okay, then I'll order some.

59
00:06:30,430 --> 00:06:35,780
Doctor, this hasn't gone through inspection yet.

60
00:06:35,780 --> 00:06:39,460
So what if it hasn't. All the medicine that have been inspected have risks too.

61
00:06:39,460 --> 00:06:44,320
But you can't say that they didn't have an effect. This Luo Wen was advertising it so well.

62
00:06:44,320 --> 00:06:47,540
If there's really something bad that will happen, he also can't escape.

63
00:06:47,540 --> 00:06:53,080
- Okay already. Don't say anything anymore. Just do as I said.<br>- Okay.

64
00:07:07,590 --> 00:07:11,350
Hospital Director Jiang! Hospital Director Jiang! Your special drug was really so effective.

65
00:07:11,350 --> 00:07:14,990
My mother was sic for so many months already but after taking your medicine, she got well immediately.

66
00:07:14,990 --> 00:07:19,050
- Really so thank you.<br>- Thank you Hospital Director Jiang for curing my long time illness. Thank you. Thank you.

67
00:07:19,050 --> 00:07:23,360
That's right. I kept on coughing. I've been coughing for so many years. I've seen so many doctors but none worked.

68
00:07:23,360 --> 00:07:25,990
But after taking this special medicine, I'm fine already.

69
00:07:25,990 --> 00:07:29,970
- Thank you. Thank you. Your medicinal skills are really great.<br>- Thank you, Hospital Director Jiang.

70
00:07:29,970 --> 00:07:32,980
- Hurry and stand up.<br>- Thank you Hospital Director Jiang.

71
00:07:58,110 --> 00:08:01,840
Hospital Director, something bad has happened.

72
00:08:01,840 --> 00:08:04,580
Those who took the special medicine had adverse reactions.

73
00:08:04,580 --> 00:08:11,490
- Vomiting froth and having nonstop seizures. Someone has already reported it to the police.<br>- What?!

74
00:08:11,490 --> 00:08:14,090
What's going on?

75
00:08:21,160 --> 00:08:26,200
Hello. I'm looking for Luo Wen. Is he there?

76
00:08:26,200 --> 00:08:28,960
What?!

77
00:08:38,070 --> 00:08:41,110
- What happened?<br>- That guy has ran off already.

78
00:08:41,110 --> 00:08:46,820
- Looks like these people knew from the start that these drugs are problematic.<br>- Then what should we do now?

79
00:08:46,820 --> 00:08:51,360
We have been in very high profile these past days. Our industry colleagues have been watching our every move.

80
00:08:51,360 --> 00:08:54,530
I'm afraid that it would not be easy to bluff our way out of this.

81
00:08:54,530 --> 00:08:58,320
I think that you should leave now while the police are not here yet.

82
00:08:58,320 --> 00:09:01,990
Then I'll go in hiding first.

83
00:09:09,380 --> 00:09:12,740
Are you Jiang Xue Wu?

84
00:09:12,740 --> 00:09:15,900
- Take her away.<br>- Go.

85
00:09:15,900 --> 00:09:19,880
- Go and find Commander Ming. See if he can think of a way to save me.<br>- Yes.

86
00:09:19,880 --> 00:09:24,030
Hurry and go. Hurry.

87
00:09:27,110 --> 00:09:30,910
Ming Xia, wait for a moment. I have something to tell you.

88
00:09:30,910 --> 00:09:34,800
I've finished with my official duties. If it's a personal matter, let's just talk about it tomorrow.

89
00:09:34,800 --> 00:09:41,180
Today is Qing Cheng's birthday. Let me off already. Allow us two have our romantic couple's world.

90
00:09:41,180 --> 00:09:44,870
- Jiang Xue Wu was arrested.<br>- What's going on?

91
00:09:44,870 --> 00:09:50,990
- Reportedly, she was tricked and brought in fake medicines.<br>- How can she be so muddle-headed? How can she buy fake medicines?

92
00:09:50,990 --> 00:09:55,560
Now is not the time to say those. You also know that other than our district's police station,

93
00:09:55,560 --> 00:10:01,460
what methods does other police station use? Once a person enters, he most likely will be tortured.

94
00:10:01,460 --> 00:10:05,680
Then what should we do now? Do we get her a lawyer or what?

95
00:10:05,680 --> 00:10:10,310
That won't work too. I'm thinking that maybe we can gather some money

96
00:10:10,310 --> 00:10:14,650
- and see how many comlainants were there and ask them to withdraw their complaints.<br>- No.

97
00:10:14,650 --> 00:10:20,260
- Why?<br>- First, we don't know if these complainants will be willing.

98
00:10:20,260 --> 00:10:23,400
What if they are not willing? Wouldn't we have just wasted time?

99
00:10:23,400 --> 00:10:28,150
Second, they are victims to begin with. If we use money to buy them,

100
00:10:28,150 --> 00:10:32,550
isn't that intentionally breaking the law? I think that the best way now

101
00:10:32,550 --> 00:10:37,130
is to find those people who sold the fake drugs and see if they can be the one to bear a part of the crime.

102
00:10:37,130 --> 00:10:42,540
But those people have already ran off. In this vast world, where are we going to look?

103
00:10:42,540 --> 00:10:49,110
These people usually are under an organization. I know some people who had already turned over a new leaf. I'll see if I can find some clues.

104
00:10:49,110 --> 00:10:53,070
Time is pressing. Let's move.

105
00:10:58,910 --> 00:11:00,000
<i>How come he isn't here yet?</i>

