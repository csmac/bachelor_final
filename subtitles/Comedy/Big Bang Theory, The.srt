1
00:00:02,702 --> 00:00:05,104
Almost there.
You're doing great.

2
00:00:05,106 --> 00:00:06,872
<font color=#400000>(strained):
</font>Thanks for lifting my spirits.

3
00:00:06,874 --> 00:00:08,774
Next time, try lifting the box.

4
00:00:09,643 --> 00:00:11,577
Please hurry.

5
00:00:11,579 --> 00:00:13,179
MRS. WOLOWITZ: I don't need
a treadmill!

6
00:00:13,181 --> 00:00:15,881
The doctor says
you need to get exercise!

7
00:00:15,883 --> 00:00:18,083
I get plenty
of exercise!

8
00:00:18,085 --> 00:00:21,887
Crushing my will to live
isn't exercise!

9
00:00:21,889 --> 00:00:25,324
If she isn't gonna use it,
then why are we doing this?

10
00:00:25,326 --> 00:00:27,493
She'll use it.
All I need is to rig it

11
00:00:27,495 --> 00:00:30,663
with a fishing pole
and a HoneyBaked Ham.

12
00:00:30,665 --> 00:00:31,897
All right.

13
00:00:31,899 --> 00:00:33,032
Now what?

14
00:00:33,034 --> 00:00:35,167
We set it up
in Howie's old room.

15
00:00:35,169 --> 00:00:36,202
Do you know how to set it up?

16
00:00:36,204 --> 00:00:38,237
Please,
I'm an MIT-trained engineer.

17
00:00:38,239 --> 00:00:39,738
I think I can handle...

18
00:00:39,740 --> 00:00:41,140
Ma, look out!

19
00:00:41,142 --> 00:00:43,709
<font color=#400000>(Mrs. Wolowitz screams)</font>

20
00:00:43,711 --> 00:00:47,880
I told you this
thing would kill me!

21
00:00:47,882 --> 00:00:51,517
♪ Our whole universe
was in a hot, dense state ♪

22
00:00:51,519 --> 00:00:54,853
♪ Then nearly 14 billion years
ago expansion started... Wait! ♪

23
00:00:54,855 --> 00:00:56,488
♪ The Earth began to cool

24
00:00:56,490 --> 00:00:59,024
♪ The autotrophs began to drool,
Neanderthals developed tools ♪

25
00:00:59,026 --> 00:01:01,694
♪ We built the Wall
♪ <i>We built the pyramids</i> ♪

26
00:01:01,696 --> 00:01:04,363
♪ Math, Science, History,
unraveling the mystery ♪

27
00:01:04,365 --> 00:01:06,265
♪ That all started
with a big bang ♪

28
00:01:06,267 --> 00:01:08,081
♪ <i>Bang!</i> ♪

29
00:01:08,082 --> 00:01:12,082
<font color=#00FF00>♪ The Big Bang Theory 7x23 ♪</font>
<font color=#00FFFF>The Gorilla Dissolution</font>
Original Air Date on May 8, 2014

30
00:01:12,083 --> 00:01:17,083
== sync, corrected by <font color=#00FF00>elderman</font> ==
<font color=#00FFFF>@elder_man</font>

31
00:01:17,109 --> 00:01:21,278
So she's gonna be laid up
for at least six weeks.

32
00:01:21,280 --> 00:01:22,680
Poor Mrs. Wolowitz.

33
00:01:22,682 --> 00:01:24,281
Should we do something
for her?

34
00:01:24,283 --> 00:01:27,852
I know. Let's go see
the new Spider-Man movie.

35
00:01:28,954 --> 00:01:31,522
Sheldon, we're talking
about your friend's mother.

36
00:01:31,524 --> 00:01:32,590
She got hurt.

37
00:01:32,592 --> 00:01:34,692
I thought that subject
had run its course,

38
00:01:34,694 --> 00:01:36,327
so I changed it.

39
00:01:36,329 --> 00:01:39,830
It's called reading
the room, Amy.

40
00:01:39,832 --> 00:01:41,399
Hi.

41
00:01:41,401 --> 00:01:42,633
Hey, how were things on the set?

42
00:01:42,635 --> 00:01:44,035
Uh, pretty good, actually.

43
00:01:44,037 --> 00:01:45,836
So the movie's not
as bad as you thought?

44
00:01:45,838 --> 00:01:47,304
Oh, no, it is,

45
00:01:47,306 --> 00:01:48,873
but I decided,
instead of complaining about it,

46
00:01:48,875 --> 00:01:51,175
I'm just gonna go in every day
and give it my all.

47
00:01:51,177 --> 00:01:52,410
Good for you.
Thanks.

48
00:01:52,412 --> 00:01:54,078
There's no reason why
I shouldn't be

49
00:01:54,080 --> 00:01:56,681
the best bisexual go-go dancer
slowly transforming

50
00:01:56,683 --> 00:02:00,084
into a killer gorilla
anyone's ever seen.

51
00:02:00,086 --> 00:02:01,719
I don't know.

52
00:02:01,721 --> 00:02:04,255
The bisexual gorilla
go-go dancer in <i>Schindler's List</i>

53
00:02:04,257 --> 00:02:05,589
is tough to beat.

54
00:02:05,591 --> 00:02:07,558
Ah, very good.

55
00:02:07,560 --> 00:02:10,761
Because a gorilla go-go dancer
of any sexual preference

56
00:02:10,763 --> 00:02:13,497
would be out of place in a film
about the Holocaust.

57
00:02:13,499 --> 00:02:17,134
It only gets funnier
when you explain it, Sheldon.

58
00:02:17,136 --> 00:02:20,171
<font color=#400000>(chuckling):
</font>I know.

59
00:02:23,008 --> 00:02:26,510
<font color=#400000>(sighs)
</font> Okay, she's all settled
in the guest room.

60
00:02:26,512 --> 00:02:28,245
Maybe we should get
one of those machines

61
00:02:28,247 --> 00:02:30,114
to help her up the stairs.

62
00:02:30,116 --> 00:02:32,016
You mean a forklift?

63
00:02:32,784 --> 00:02:34,785
Howie...
I'm sorry.

64
00:02:34,787 --> 00:02:36,954
I just can't deal
with this right now.

65
00:02:36,956 --> 00:02:39,023
Taking care of your own mother?
How can you say that?

66
00:02:39,025 --> 00:02:41,892
Bernie, she's gonna be off
her feet for six to eight weeks.

67
00:02:41,894 --> 00:02:44,061
Are you prepared
to feed her, wash her

68
00:02:44,063 --> 00:02:45,863
and take her to the toilet?

69
00:02:45,865 --> 00:02:48,566
I would do it for my mother.
Yeah, of course

70
00:02:48,568 --> 00:02:51,068
you would--
you're a loving person.

71
00:02:51,070 --> 00:02:55,706
I'm what my people
would call a <i>putz.</i>

72
00:02:55,708 --> 00:02:58,209
Look, I'm not crazy
about the idea,

73
00:02:58,211 --> 00:03:00,111
but what other
choice do we have?

74
00:03:00,113 --> 00:03:01,145
We get a nurse.

75
00:03:01,147 --> 00:03:03,013
Preferably someone
from a Third World country

76
00:03:03,015 --> 00:03:06,650
who's used to suffering
and unpleasant smells.

77
00:03:08,053 --> 00:03:09,487
You'd hire a
total stranger

78
00:03:09,489 --> 00:03:11,856
to take care of the
woman who raised you?

79
00:03:11,858 --> 00:03:12,957
That's so cruel.

80
00:03:12,959 --> 00:03:15,359
Not if we pay them well

81
00:03:15,361 --> 00:03:18,395
and let them listen
to the music of their homeland.

82
00:03:20,098 --> 00:03:22,500
MRS. WOLOWITZ:
I need to tinkle!

83
00:03:22,502 --> 00:03:25,102
Sounds like a job
for a loving person.

84
00:03:26,872 --> 00:03:28,472
Would you like me
to play some Polish music

85
00:03:28,474 --> 00:03:29,974
while you carry her
to the toilet?

86
00:03:31,476 --> 00:03:33,244
You are a <i>putz.</i>

87
00:03:34,546 --> 00:03:36,914
As advertised.

88
00:03:40,886 --> 00:03:42,119
Thanks for coming
with me.

89
00:03:42,121 --> 00:03:45,156
Thanks for inviting me
after everyone else said no.

90
00:03:47,092 --> 00:03:48,859
Aren't you gonna get
3-D glasses?

91
00:03:48,861 --> 00:03:50,528
I brought my own.

92
00:03:50,530 --> 00:03:54,865
No sense in risking
bridge-of-nose herpes.

93
00:03:55,734 --> 00:03:57,001
Is that a real thing?

94
00:03:57,003 --> 00:03:59,837
Well, until they invent
nose condoms,

95
00:03:59,839 --> 00:04:02,006
I'm not finding out.

96
00:04:04,009 --> 00:04:06,110
Emily! Hey!

97
00:04:06,112 --> 00:04:07,578
Oh, hey, Raj.

98
00:04:07,580 --> 00:04:09,046
Uh, this is my friend, Sheldon.

99
00:04:09,048 --> 00:04:10,481
Sheldon, this is Emily.

100
00:04:11,283 --> 00:04:13,384
Oh, yes, you're
the dermatologist.

101
00:04:13,386 --> 00:04:17,188
I went for a walk yesterday
without sunscreen.

102
00:04:17,190 --> 00:04:20,991
Do you see anything
on my forehead

103
00:04:20,993 --> 00:04:23,994
that I should be
concerned about?

104
00:04:23,996 --> 00:04:27,064
Um, you know what?
I better go.

105
00:04:27,066 --> 00:04:28,332
My movie's about
to start.

106
00:04:28,334 --> 00:04:29,600
Are you here alone?

107
00:04:29,602 --> 00:04:30,601
No, not really.

108
00:04:30,603 --> 00:04:32,136
What do you mean,
"not really"?

109
00:04:32,138 --> 00:04:33,437
Hey, should we get
our seats?

110
00:04:33,439 --> 00:04:34,872
Yeah.

111
00:04:34,874 --> 00:04:37,174
It was nice seeing you.
Um, I'll call you later.

112
00:04:37,176 --> 00:04:38,342
Yeah, okay.

113
00:04:41,079 --> 00:04:43,848
That was awkward, right?

114
00:04:45,584 --> 00:04:46,984
Uh-huh.

115
00:04:46,986 --> 00:04:48,385
Is it because
she's dating you

116
00:04:48,387 --> 00:04:49,841
but was out with
that other fellow?

117
00:04:49,866 --> 00:04:50,755
Yes.

118
00:04:50,756 --> 00:04:53,324
Good. I thought she saw
something on my forehead.

119
00:04:56,428 --> 00:04:58,562
And... action.

120
00:04:58,564 --> 00:05:00,698
Please don't shut me out.

121
00:05:00,700 --> 00:05:01,665
Go away.

122
00:05:01,667 --> 00:05:04,668
Just go away.

123
00:05:04,670 --> 00:05:07,872
I swear, I will find a way
to turn you back.

124
00:05:07,874 --> 00:05:09,240
What gave you
the right

125
00:05:09,242 --> 00:05:12,943
to mix my DNA with that
of a killer gorilla?

126
00:05:14,079 --> 00:05:16,513
I was trying
to save your life.

127
00:05:16,515 --> 00:05:19,049
Life? What life?
Look at me! I'm a monster!

128
00:05:19,051 --> 00:05:22,519
And now I have blood on my
hands, or paws. I don't know.

129
00:05:26,124 --> 00:05:29,627
You can't give up.
I love you.

130
00:05:31,730 --> 00:05:33,597
I love you, too.

131
00:05:33,599 --> 00:05:36,400
But I'm afraid
I love killing more.

132
00:05:36,402 --> 00:05:39,270
Like, one day, I might
actually try and kill you!

133
00:05:39,272 --> 00:05:41,939
<font color=#400000>(shrieking)</font>

134
00:05:41,941 --> 00:05:45,075
And... cut.
All right.

135
00:05:47,846 --> 00:05:49,580
All right, let's set up
for the next scene.

136
00:05:49,582 --> 00:05:53,117
Actually, you know what?
Can we do one more?

137
00:05:53,119 --> 00:05:54,184
I think I could do it better.

138
00:05:54,186 --> 00:05:56,353
Let's just move on.
No one cares.

139
00:05:56,355 --> 00:05:57,888
Well, 
<font color=#400000>(scoffs)
</font> I care.
I mean, look,

140
00:05:57,890 --> 00:06:00,157
if we're gonna do this,
why not try and make it

141
00:06:00,159 --> 00:06:01,492
something
we're actually proud of?

142
00:06:01,494 --> 00:06:03,627
<font color=#400000>(clears throat)</font>

143
00:06:04,763 --> 00:06:05,729
Look, sweetie,

144
00:06:05,731 --> 00:06:06,964
this movie is garbage,

145
00:06:06,966 --> 00:06:08,499
but you should be happy
about that,

146
00:06:08,501 --> 00:06:11,468
'cause if it was a good movie,
you wouldn't be in it.

147
00:06:12,971 --> 00:06:13,971
Whoa, whoa, hang on.

148
00:06:13,973 --> 00:06:15,272
There's no need
to insult her.

149
00:06:15,274 --> 00:06:16,040
And who are you?

150
00:06:16,042 --> 00:06:17,174
I'm her boyfriend.

151
00:06:17,176 --> 00:06:18,375
Isn't she too hot for you?

152
00:06:18,377 --> 00:06:20,644
A little, yeah.

153
00:06:20,646 --> 00:06:22,579
Well, boyfriend, get off my set.

154
00:06:22,581 --> 00:06:24,181
You can't do that.
He's with me.

155
00:06:24,183 --> 00:06:26,650
You know what? You can get off
my set, too. You're fired.

156
00:06:26,652 --> 00:06:28,585
What? You can't fire me.
I'm the star.

157
00:06:28,587 --> 00:06:32,156
I'm the girl that goes bananas.
It says so on the poster.

158
00:06:32,158 --> 00:06:34,925
Yeah, but we just shot the last
scene where we see your face.

159
00:06:34,927 --> 00:06:40,264
So from now own, the star of
the movie is whoever wears this.

160
00:06:41,366 --> 00:06:43,267
Hey, if you're
gonna fire her,

161
00:06:43,269 --> 00:06:45,169
then you have to
fire me, too.

162
00:06:45,171 --> 00:06:49,273
Wow, that fell apart
really fast.

163
00:06:57,774 --> 00:06:59,608
Thanks for skipping
the movie.

164
00:06:59,610 --> 00:07:01,943
I couldn't sit in that theater
for two hours

165
00:07:01,945 --> 00:07:03,845
wondering about Emily
and that guy.

166
00:07:03,847 --> 00:07:05,647
Oh, quite all right.

167
00:07:05,649 --> 00:07:07,482
After my forehead
melanoma scare,

168
00:07:07,484 --> 00:07:09,651
I've learned not to sweat
the small stuff.

169
00:07:12,255 --> 00:07:13,655
Well, sorry,

170
00:07:13,657 --> 00:07:16,291
I don't have all the ingredients
to make chai tea.

171
00:07:16,293 --> 00:07:17,726
You don't have
to make me anything.

172
00:07:17,728 --> 00:07:20,162
No, I do.
You're upset about Emily

173
00:07:20,164 --> 00:07:21,096
and you're Indian.

174
00:07:21,098 --> 00:07:22,964
I need to make you
chai tea.

175
00:07:22,966 --> 00:07:28,703
Now, I have all the ingredients
except cardamom seeds.

176
00:07:28,705 --> 00:07:31,306
Do you happen
to have any on you?

177
00:07:31,308 --> 00:07:34,576
Sorry, I left them in my turban.

178
00:07:37,180 --> 00:07:39,681
Oh, I'll make
English breakfast tea.

179
00:07:39,683 --> 00:07:41,083
They destroyed
your culture.

180
00:07:41,085 --> 00:07:42,617
That's close enough.

181
00:07:45,755 --> 00:07:47,956
You know, I'm curious,

182
00:07:47,958 --> 00:07:51,626
why are you so upset about
seeing Emily with another man?

183
00:07:51,628 --> 00:07:54,796
Wouldn't you be upset if you saw
Amy out with someone else?

184
00:07:54,798 --> 00:07:55,964
Can't happen.

185
00:07:55,966 --> 00:07:58,166
We have an ironclad
relationship agreement

186
00:07:58,168 --> 00:08:00,469
which precludes her
from physical contact

187
00:08:00,471 --> 00:08:02,904
with anyone other than me.

188
00:08:02,906 --> 00:08:05,507
But you don't have sex
with her, either.

189
00:08:05,509 --> 00:08:08,043
<font color=#400000>(chuckles) 
</font>Slick, huh?

190
00:08:08,045 --> 00:08:11,313
To be truthful, Emily and I
haven't dated that long,

191
00:08:11,315 --> 00:08:13,815
and we never agreed to be
exclusive to each other.

192
00:08:13,817 --> 00:08:15,250
Have you had intercourse?

193
00:08:15,252 --> 00:08:16,384
No.

194
00:08:16,386 --> 00:08:17,686
Well, stick to your guns.

195
00:08:17,688 --> 00:08:19,321
There will be a lot of pressure.

196
00:08:25,161 --> 00:08:27,562
MRS. WOLOWITZ:
I'm hungry again!

197
00:08:29,665 --> 00:08:32,834
It's like the world's
fattest cuckoo clock.

198
00:08:36,772 --> 00:08:38,473
You know, you're
always talking about

199
00:08:38,475 --> 00:08:39,574
having a baby someday.

200
00:08:39,576 --> 00:08:41,610
This is exactly what
it's gonna be like.

201
00:08:41,612 --> 00:08:43,111
No, it's not.

202
00:08:43,113 --> 00:08:46,314
Come on. The constant
fussing, eating, pooping,

203
00:08:46,316 --> 00:08:47,315
burping, drooling.

204
00:08:47,317 --> 00:08:48,984
We're even waiting
for the day

205
00:08:48,986 --> 00:08:51,286
when she can finally
walk on her own.

206
00:08:52,421 --> 00:08:53,655
Maybe you're right.

207
00:08:53,657 --> 00:08:56,892
Anything she finds on the floor
goes right in her mouth.

208
00:09:01,931 --> 00:09:04,432
I'm just telling you now,
if we do have kids,

209
00:09:04,434 --> 00:09:06,635
don't expect me
to do all the work.

210
00:09:06,637 --> 00:09:09,504
Hey, I'm a very paternal person.

211
00:09:09,506 --> 00:09:12,574
I'd be excellent
at taking care of a baby.

212
00:09:12,576 --> 00:09:15,010
MRS. WOLOWITZ:
I'm still hungry!

213
00:09:15,012 --> 00:09:16,945
I'm coming, you big baby!

214
00:09:20,917 --> 00:09:23,685
You know, the only thing worse
than doing a movie

215
00:09:23,687 --> 00:09:26,721
where they glue monkey hair
to your ass

216
00:09:26,723 --> 00:09:28,089
is getting fired from a movie

217
00:09:28,091 --> 00:09:30,692
where they glue monkey hair
to your ass.

218
00:09:31,694 --> 00:09:34,362
Forget it, man,
it's crap.

219
00:09:34,364 --> 00:09:36,031
You just move on
to the next thing.

220
00:09:36,033 --> 00:09:37,766
Yeah, well, it's easy
for you to say.

221
00:09:37,768 --> 00:09:39,801
You used to be famous.

222
00:09:39,803 --> 00:09:42,871
Hey. I just lost
a job for you.

223
00:09:42,873 --> 00:09:45,707
You're right, I'm sorry,
you're famous.

224
00:09:45,709 --> 00:09:47,909
Penny, it's not
about being famous.

225
00:09:47,911 --> 00:09:49,945
It's about the art.

226
00:09:49,947 --> 00:09:51,980
It's about the passion
we have for our craft.

227
00:09:51,982 --> 00:09:53,915
<font color=#400000>(phone dings)</font>

228
00:09:53,917 --> 00:09:56,218
I have an audition
for <i>Sharknado 2!</i>

229
00:09:56,220 --> 00:09:58,053
If I book this,
I am totally gonna

230
00:09:58,055 --> 00:09:59,588
pay you back
for this beer.

231
00:10:02,725 --> 00:10:06,328
God, what am I
doing with my life?

232
00:10:06,330 --> 00:10:08,063
You having second thoughts
about acting?

233
00:10:08,065 --> 00:10:09,998
You were on set, you
saw what it was like.

234
00:10:10,000 --> 00:10:12,367
Yeah, but it's not
always that bad.

235
00:10:12,369 --> 00:10:13,501
Oh, really?

236
00:10:13,503 --> 00:10:15,136
What about when you did, uh,

237
00:10:15,138 --> 00:10:16,871
Anne Frank at that
cute little theater?

238
00:10:16,873 --> 00:10:19,307
It was over a bowling alley.

239
00:10:20,409 --> 00:10:22,210
Yeah, but...

240
00:10:22,212 --> 00:10:25,780
there was ample parking.

241
00:10:25,782 --> 00:10:27,048
Are you done?

242
00:10:27,050 --> 00:10:29,150
And you were so good
in the TV commercial.

243
00:10:29,152 --> 00:10:32,153
It was for hemorrhoid cream.

244
00:10:32,155 --> 00:10:34,189
And I got itchy and swollen

245
00:10:34,191 --> 00:10:36,324
just watching you.

246
00:10:39,528 --> 00:10:41,663
Leonard, you are really
not cheering me up.

247
00:10:41,665 --> 00:10:42,797
Come on.

248
00:10:42,799 --> 00:10:44,733
How can you be sad
when you're going home

249
00:10:44,735 --> 00:10:47,502
with all five-foot-six of this?

250
00:10:47,504 --> 00:10:50,572
Hmm. You think you're
five-foot-six, that's funny.

251
00:10:53,909 --> 00:10:55,877
I don't understand it.

252
00:10:55,879 --> 00:10:56,911
I'm a nice guy,

253
00:10:56,913 --> 00:10:58,446
I have a great job,

254
00:10:58,448 --> 00:10:59,648
I'm well-educated,

255
00:10:59,650 --> 00:11:01,516
come from a good family...

256
00:11:01,518 --> 00:11:03,585
Why don't women
want to be with me?

257
00:11:03,587 --> 00:11:04,519
<font color=#400000>(sighs)</font>

258
00:11:04,521 --> 00:11:06,788
An interesting
question.

259
00:11:08,190 --> 00:11:09,624
Well...

260
00:11:09,626 --> 00:11:11,760
good night.

261
00:11:11,762 --> 00:11:13,228
What?

262
00:11:13,230 --> 00:11:14,529
Don't send me home.

263
00:11:14,531 --> 00:11:15,930
I can't be alone
right now.

264
00:11:15,932 --> 00:11:17,799
That's your problem.

265
00:11:17,801 --> 00:11:19,000
You can't be alone.

266
00:11:19,002 --> 00:11:20,602
What do you mean?

267
00:11:22,605 --> 00:11:25,040
How many women have
you had dates with?

268
00:11:26,475 --> 00:11:27,442
Eleven.

269
00:11:27,444 --> 00:11:29,344
How many of
those women

270
00:11:29,346 --> 00:11:31,880
did you think would become
your perfect companion?

271
00:11:33,015 --> 00:11:34,182
Eleven.

272
00:11:35,251 --> 00:11:36,284
Wait.

273
00:11:36,286 --> 00:11:38,219
Do I count the 200-pound
Sailor Moon girl

274
00:11:38,221 --> 00:11:41,022
that Howard and I had
a threesome with at Comic Con?

275
00:11:43,959 --> 00:11:45,694
Sure.

276
00:11:46,529 --> 00:11:47,662
I'll stick with 11.

277
00:11:47,664 --> 00:11:50,031
She liked Howard better.

278
00:11:51,901 --> 00:11:54,235
Well, now do you
see the problem?

279
00:11:55,504 --> 00:11:56,771
Maybe.

280
00:11:56,773 --> 00:11:58,406
I... I don't know.

281
00:11:58,408 --> 00:11:59,641
You... It's late,
I-I should...

282
00:11:59,643 --> 00:12:01,843
I should go.

283
00:12:04,280 --> 00:12:05,413
<font color=#400000>(sighs)</font>

284
00:12:05,415 --> 00:12:06,381
Look,

285
00:12:06,383 --> 00:12:08,183
I-I do get what you're saying.

286
00:12:08,185 --> 00:12:09,317
Instead of desperately clinging

287
00:12:09,319 --> 00:12:11,186
to any woman
who will go out with me,

288
00:12:11,188 --> 00:12:13,788
I need to work on
my fear of being alone.

289
00:12:14,957 --> 00:12:17,592
I was trying to suggest
chemical castration, but...

290
00:12:19,829 --> 00:12:21,096
...it's my bedtime,

291
00:12:21,098 --> 00:12:22,464
so whatever gets you
out the door.

292
00:12:22,466 --> 00:12:24,666
Good night.

293
00:12:27,636 --> 00:12:29,738
Howie, I'm back!

294
00:12:29,740 --> 00:12:32,073
Shh-shh-shh,
I just got her to sleep.

295
00:12:32,075 --> 00:12:33,341
Sorry.

296
00:12:33,343 --> 00:12:34,409
What took you so long?

297
00:12:34,411 --> 00:12:36,010
The grocery store
is a few blocks away.

298
00:12:36,012 --> 00:12:37,345
They only had
regular yogurt.

299
00:12:37,347 --> 00:12:39,114
I had to go to
a different store

300
00:12:39,116 --> 00:12:41,316
to get the extra-fat
kind your mom likes.

301
00:12:42,218 --> 00:12:44,352
Then why do I smell coffee
on your breath?

302
00:12:44,354 --> 00:12:45,553
So what?

303
00:12:45,555 --> 00:12:47,155
After two days of
taking care of her,

304
00:12:47,157 --> 00:12:49,057
excuse me for stopping
to get a mocha.

305
00:12:49,059 --> 00:12:50,325
A mocha?

306
00:12:50,327 --> 00:12:53,194
Well, it must be nice
to be queen.

307
00:12:53,196 --> 00:12:54,696
Queen?

308
00:12:54,698 --> 00:12:56,664
I've been killing
myself here!

309
00:12:56,666 --> 00:12:57,732
Well, whose fault is that?

310
00:12:57,734 --> 00:12:59,401
I wanted to get a nurse,
but you were all,

311
00:12:59,403 --> 00:13:01,923
<font color=#400000>(high-pitched voice):
</font> "I'm nice.
I want to take care of people."

312
00:13:03,706 --> 00:13:05,440
I'm glad I got that mocha.

313
00:13:05,442 --> 00:13:07,242
And you know what
else I'm glad about?

314
00:13:07,244 --> 00:13:11,446
I bought you a brownie
and I ate it in the car!

315
00:13:13,949 --> 00:13:15,850
MRS. WOLOWITZ (moaning):
Howard!

316
00:13:15,852 --> 00:13:17,385
Thanks a lot.

317
00:13:17,387 --> 00:13:19,487
Now I have to go
rub her belly again.

318
00:13:23,225 --> 00:13:25,427
(knocking at door)

319
00:13:27,530 --> 00:13:28,563
Hey.

320
00:13:28,565 --> 00:13:30,198
Thanks for letting me come over.

321
00:13:30,200 --> 00:13:31,433
Of course.

322
00:13:31,435 --> 00:13:33,601
Please, come in.

323
00:13:34,937 --> 00:13:36,171
I just wanted to say

324
00:13:36,173 --> 00:13:38,773
how sorry I am about tonight,
and...

325
00:13:38,775 --> 00:13:40,742
I want to make sure
that we're okay.

326
00:13:40,744 --> 00:13:42,911
Uh, look, you and I haven't made

327
00:13:42,913 --> 00:13:44,813
any commitments to each other.
I know.

328
00:13:44,815 --> 00:13:46,915
I just felt like
I needed to explain.

329
00:13:46,917 --> 00:13:48,817
The guy I was with
did my last tattoo,

330
00:13:48,819 --> 00:13:50,251
and he's been asking
me out for months.

331
00:13:50,253 --> 00:13:52,720
I finally said yes
just to get it over with.

332
00:13:52,722 --> 00:13:54,389
It's okay.

333
00:13:54,391 --> 00:13:55,690
Really?

334
00:13:55,692 --> 00:13:58,426
Well, I mean, yeah,
it freaked me out a little,

335
00:13:58,428 --> 00:14:01,029
but that's my issue, not yours.

336
00:14:01,031 --> 00:14:02,964
Wow.

337
00:14:02,966 --> 00:14:04,799
If I saw you out
with another woman,

338
00:14:04,801 --> 00:14:06,501
I'd be pretty upset.

339
00:14:06,503 --> 00:14:09,437
Thank you.

340
00:14:10,439 --> 00:14:11,773
Not just for being upset,

341
00:14:11,775 --> 00:14:14,042
but for believing
that could happen.

342
00:14:17,279 --> 00:14:19,948
Just so you know,
I'm not seeing anyone else.

343
00:14:19,950 --> 00:14:22,116
Well, me, neither.

344
00:14:22,118 --> 00:14:23,218
Okay.

345
00:14:23,220 --> 00:14:25,220
Okay.
(laughs)

346
00:14:25,222 --> 00:14:26,721
Please.

347
00:14:26,723 --> 00:14:29,290
So, uh... you...
you have tattoos?

348
00:14:29,292 --> 00:14:30,258
Yeah.

349
00:14:30,260 --> 00:14:32,594
I don't.

350
00:14:32,596 --> 00:14:34,229
I have a hole
in my belly button

351
00:14:34,231 --> 00:14:37,265
that may or may not
have been a piercing.

352
00:14:37,267 --> 00:14:38,500
That's cool.

353
00:14:38,502 --> 00:14:40,568
It's a piercing.

354
00:14:41,804 --> 00:14:42,770
So, uh...

355
00:14:42,772 --> 00:14:44,138
how many tattoos?

356
00:14:44,140 --> 00:14:46,407
One on my shoulder,

357
00:14:46,409 --> 00:14:48,376
one not on my shoulder

358
00:14:48,378 --> 00:14:51,880
and one <i>really</i> not
on my shoulder.

359
00:14:53,349 --> 00:14:54,782
It's, uh...

360
00:14:54,784 --> 00:14:57,051
been a long time since
I've seen a girl's...

361
00:14:57,053 --> 00:14:58,953
really not her shoulder.

362
00:14:58,955 --> 00:15:00,288
(chuckles)

363
00:15:00,290 --> 00:15:03,124
Well...

364
00:15:03,126 --> 00:15:05,159
how about you show
me your piercing

365
00:15:05,161 --> 00:15:07,395
and I show you
my tattoos?

366
00:15:13,936 --> 00:15:15,603
But, uh, before
I take my shirt off,

367
00:15:15,605 --> 00:15:18,139
I just need like ten minutes
to do some crunches.

368
00:15:24,380 --> 00:15:26,381
Oh, my God.

369
00:15:26,383 --> 00:15:29,183
What a day.

370
00:15:29,185 --> 00:15:30,451
Can I get you anything?

371
00:15:30,453 --> 00:15:33,254
(sighs) No.

372
00:15:33,256 --> 00:15:35,924
I need to start making
some smart decisions.

373
00:15:35,926 --> 00:15:37,425
(exhales)
With your career?

374
00:15:37,427 --> 00:15:38,993
With my life.

375
00:15:38,995 --> 00:15:40,995
Like what?

376
00:15:40,997 --> 00:15:43,197
I don't know.

377
00:15:44,300 --> 00:15:46,000
We could get married.

378
00:15:46,002 --> 00:15:47,769
Come on, be serious.

379
00:15:47,771 --> 00:15:48,937
I am.

380
00:15:48,939 --> 00:15:50,538
Why?

381
00:15:50,540 --> 00:15:53,441
Because I'm
a-a "smart decision"?

382
00:15:53,443 --> 00:15:54,676
Well, yeah.

383
00:15:55,511 --> 00:15:58,046
So I'm like a bran muffin.

384
00:15:59,582 --> 00:16:00,682
What...

385
00:16:00,684 --> 00:16:01,816
No, that's not what I'm saying.

386
00:16:01,818 --> 00:16:03,084
No, it's exactly
what you're saying.

387
00:16:03,086 --> 00:16:04,619
I'm the boring thing
you're choosing

388
00:16:04,621 --> 00:16:06,054
because I'm good for you.

389
00:16:06,056 --> 00:16:07,155
What does it matter?

390
00:16:07,157 --> 00:16:08,489
The point is, I'm choosing you.

391
00:16:08,491 --> 00:16:09,524
Well, it matters a lot!

392
00:16:09,526 --> 00:16:11,092
I don't want to be
a bran muffin.

393
00:16:11,094 --> 00:16:12,660
I-I want to be

394
00:16:12,662 --> 00:16:15,196
a... Cinnabon, you know?

395
00:16:16,031 --> 00:16:18,566
A strawberry Pop-Tart.

396
00:16:19,468 --> 00:16:20,702
Something you're excited about

397
00:16:20,704 --> 00:16:23,304
even though it could
give you diabetes.

398
00:16:24,206 --> 00:16:26,708
Sweetie, you can be
any pastry you want.

399
00:16:26,710 --> 00:16:28,443
No, no. No, it's too late.

400
00:16:28,445 --> 00:16:30,478
I-I'm your
bran muffin.

401
00:16:30,480 --> 00:16:34,015
Probably fat-free
and good for your colon.

402
00:16:34,017 --> 00:16:35,116
You know what? Forget it.

403
00:16:35,118 --> 00:16:36,150
I never should've brought it up.

404
00:16:36,152 --> 00:16:38,853
You know I want to marry you,

405
00:16:38,855 --> 00:16:40,088
but you're
only doing this

406
00:16:40,090 --> 00:16:41,189
because you got fired

407
00:16:41,191 --> 00:16:42,390
and you're feeling
sorry for yourself.

408
00:16:42,392 --> 00:16:44,359
Okay, it may look that way,
but getting fired

409
00:16:44,361 --> 00:16:46,260
from that movie was the best
thing that could have happened

410
00:16:46,262 --> 00:16:47,662
to me, okay?
I finally realize

411
00:16:47,664 --> 00:16:49,063
I don't need to be famous

412
00:16:49,065 --> 00:16:50,698
or have some big career
to be happy.

413
00:16:50,700 --> 00:16:51,833
Then what <i>do</i> you need?

414
00:16:51,835 --> 00:16:54,736
You, you stupid Pop-Tart!

415
00:16:59,708 --> 00:17:01,576
Oh.

416
00:17:02,745 --> 00:17:05,680
Then I guess I'm in.

417
00:17:06,548 --> 00:17:07,448
Really?

418
00:17:07,450 --> 00:17:08,783
You "guess you're in"?

419
00:17:10,185 --> 00:17:11,753
Not like, "I guess I'm in."

420
00:17:11,755 --> 00:17:14,422
Like "I guess... I'm in!"

421
00:17:15,724 --> 00:17:16,924
Okay.

422
00:17:16,926 --> 00:17:18,960
Cool.

423
00:17:22,331 --> 00:17:23,898
So is that it?

424
00:17:23,900 --> 00:17:25,133
Are... are we engaged?

425
00:17:26,101 --> 00:17:27,802
Yeah, I think so.

426
00:17:29,605 --> 00:17:31,105
All right.

427
00:17:33,242 --> 00:17:34,809
What's wrong?

428
00:17:34,811 --> 00:17:36,477
I'm not sure.

429
00:17:36,479 --> 00:17:40,148
Just... feels
a little anticlimactic.

430
00:17:41,483 --> 00:17:43,685
Yeah, it kind of does, huh?

431
00:17:44,953 --> 00:17:46,921
Oh, I know.

432
00:17:48,724 --> 00:17:50,892
This might help.

433
00:17:54,697 --> 00:17:56,998
Where did you get a ring?

434
00:17:57,000 --> 00:17:58,633
I've...

435
00:17:58,635 --> 00:18:00,835
had it for a couple years,
not important.

436
00:18:06,208 --> 00:18:07,742
Penny...

437
00:18:07,744 --> 00:18:09,277
will you marry me?

438
00:18:10,846 --> 00:18:12,480
Oh, my God, yes.

439
00:18:12,482 --> 00:18:14,182
(laughing)

440
00:18:14,950 --> 00:18:16,751
(sighs happily)

441
00:18:16,753 --> 00:18:18,419
This would have been
so much more romantic

442
00:18:18,421 --> 00:18:20,588
if you didn't have
monkey hair on your finger.

443
00:18:33,934 --> 00:18:35,401
How you feeling?

444
00:18:35,403 --> 00:18:37,237
Last night was
a little rough,

445
00:18:37,239 --> 00:18:39,806
but I think we're
gonna get through this.

446
00:18:39,808 --> 00:18:41,107
I'm proud of us.

447
00:18:41,109 --> 00:18:42,976
Me, too.

448
00:18:42,978 --> 00:18:45,211
MRS. WOLOWITZ:
Where's my pancakes?!

449
00:18:45,213 --> 00:18:47,780
(accent):
Coming, Mrs. Wolowitz!

450
00:18:53,454 --> 00:18:54,554
You were right.

451
00:18:54,556 --> 00:18:56,155
Welcome to Team Putz.

452
00:18:56,157 --> 00:19:01,157
== sync, corrected by <font color=#00FF00>elderman</font> ==
<font color=#00FFFF>@elder_man</font>

