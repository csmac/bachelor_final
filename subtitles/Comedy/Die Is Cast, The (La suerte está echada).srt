1
00:00:19,950 --> 00:00:23,716
- What do you want?
- I got a writ I need you to sign.

2
00:00:23,888 --> 00:00:24,980
What kind of writ?

3
00:00:25,156 --> 00:00:28,182
I need the release of a prisoner
from San Luis to help me in a case.

4
00:00:28,359 --> 00:00:31,123
- Must be a big case.
- The target's a major counterfeiter...

5
00:00:31,295 --> 00:00:35,629
- ... involved in the murder of a federal agent.
- I never sign such writs.

6
00:00:41,138 --> 00:00:42,935
Why are you still here?

7
00:00:44,875 --> 00:00:46,137
I spent all morning on this.

8
00:00:46,310 --> 00:00:49,143
I would appreciate it if you would be
kind enough to look at it.

9
00:00:52,684 --> 00:00:55,152
Okay. Now I've looked at it.

10
00:00:55,587 --> 00:00:57,680
Look, I gotta have this guy out.

11
00:00:59,391 --> 00:01:03,191
Didn't you come in here about a week ago
and ask me to hold him without bail?

12
00:01:03,361 --> 00:01:06,159
Yes, and I'll assume full responsibility
for getting him back.

13
00:01:06,331 --> 00:01:09,926
I don't need the headache.

14
00:01:10,101 --> 00:01:11,762
Cody is an associate of Rick Masters.

15
00:01:11,937 --> 00:01:15,100
Masters has been making a mockery out
of you, me and this whole system.

16
00:01:15,273 --> 00:01:19,073
That doesn't change the fact that he's on
a no-bail hold awaiting arraignment.

17
00:01:23,648 --> 00:01:25,616
He killed my partner, man.

18
00:01:28,286 --> 00:01:31,551
The answer is still no.

19
00:01:39,664 --> 00:01:41,063
If I was one of your cronies...

20
00:01:41,233 --> 00:01:44,134
...you'd be spread-eagle on your desk
to do this for me.

21
00:01:47,172 --> 00:01:50,733
Don't say something you'll regret later.

22
00:01:52,210 --> 00:01:53,576
Come back here!

23
00:02:00,720 --> 00:02:02,745
Let me look at it again.

24
00:02:19,071 --> 00:02:22,302
If this prisoner escapes from custody...

25
00:02:22,475 --> 00:02:26,571
...I'll make you testify in open court
about how he made a fool out of you.

26
00:02:34,253 --> 00:02:36,244
Now get the hell out of here.

27
00:02:38,388 --> 00:02:41,551
If you cross me, I will dedicate
my life to putting you back in here...

28
00:02:42,893 --> 00:02:45,293
...and I will make sure you do
all five. You got that?

29
00:02:45,462 --> 00:02:46,451
You have my promise.

30
00:02:46,630 --> 00:02:49,861
I wanna know where Masters prints
and I want you to take me there now.

31
00:02:50,033 --> 00:02:52,627
Then we're going downtown
to swear out your statement.

32
00:02:53,470 --> 00:02:55,268
- Can I ask a favor?
- What?

33
00:02:55,440 --> 00:02:58,375
My daughter's in the hospital.
She's pretty sick.

34
00:02:58,543 --> 00:03:02,570
Do you think we could stop by
Santa Fe Hospital?

35
00:03:02,747 --> 00:03:05,215
It's on the way
to where I'm taking you.

36
00:03:05,383 --> 00:03:08,284
- You're pulling my dick.
- I swear, man. Check it out.

37
00:03:08,453 --> 00:03:10,444
- What's your daughter's name?
- Rozanne Brown.

38
00:03:11,322 --> 00:03:14,780
Lincoln 14-3-1
to Los Angeles base, over.

39
00:03:16,294 --> 00:03:17,761
Go ahead, 3-1.

40
00:03:17,929 --> 00:03:20,159
Yeah. Request you phone
Santa Fe Hospital.

41
00:03:20,331 --> 00:03:24,631
Find out if they have a patient
named Rozanne Brown, please. Over.

42
00:03:24,802 --> 00:03:26,167
Roger.

43
00:03:30,575 --> 00:03:32,338
You have any kids?

44
00:03:34,045 --> 00:03:37,242
Well, I tell you, it changes
the way you look at things.

45
00:03:37,415 --> 00:03:38,473
Yeah?

46
00:03:38,950 --> 00:03:41,817
I'm not looking to screw up anymore.

47
00:03:42,186 --> 00:03:46,350
Rozanne Brown is in room 306.
The elevators are over there.

48
00:03:46,524 --> 00:03:47,513
Thank you.

49
00:03:53,865 --> 00:03:56,494
You have to come up
to the room with me?

50
00:04:05,477 --> 00:04:06,944
Come here.

51
00:04:17,656 --> 00:04:21,422
- What happened to your daughter?
- She was in a park, and this little monkey-

52
00:04:34,807 --> 00:04:36,570
Rozanne Brown is a black woman.

53
00:04:36,742 --> 00:04:40,303
She's recuperating from a fall
she took from a bicycle near USC.

54
00:04:41,413 --> 00:04:44,348
She's married. Her husband is listed
as serving time...

55
00:04:44,516 --> 00:04:47,383
...for armed robbery
at San Luis Obispo Prison.

56
00:04:47,553 --> 00:04:48,645
Thanks.

57
00:04:48,821 --> 00:04:51,346
I think you ought to let us
take a look at you.

58
00:05:08,942 --> 00:05:11,467
I don't have a lot of time.
I'm in the middle of a trial.

59
00:05:11,644 --> 00:05:14,112
- What kind of trial?
- It's a dope case.

60
00:05:14,280 --> 00:05:17,272
A client got busted smuggling
50 pounds of cocaine.

61
00:05:17,450 --> 00:05:20,044
I should be able to get him off.
The search warrant's weak.

62
00:05:20,220 --> 00:05:21,778
Weak?

63
00:05:21,955 --> 00:05:24,617
The color of the house is listed as brown
on the warrant...

64
00:05:24,791 --> 00:05:28,056
...when, in fact, it's beige and yellow.

65
00:05:28,228 --> 00:05:29,855
You should be ashamed
of yourself.

66
00:05:30,029 --> 00:05:32,725
I don't make any apologies
for being an attorney.

67
00:05:32,899 --> 00:05:36,096
If I didn't accept the case, somebody else
would without a doubt.

68
00:05:36,269 --> 00:05:37,896
Without a doubt.

69
00:05:43,042 --> 00:05:44,907
It's too bad about Jim Hart.

70
00:05:51,150 --> 00:05:53,778
Masters has been calling me
in the middle of the night...

71
00:05:53,953 --> 00:05:57,719
...trying to order me around
like I was one of his mules.

72
00:05:58,391 --> 00:06:01,020
The other day he even had
the balls to threaten me.

73
00:06:01,195 --> 00:06:02,890
The man's an animal.

74
00:06:03,197 --> 00:06:05,665
Frankly, being house counsel
for Rick Masters...

75
00:06:05,833 --> 00:06:08,131
...doesn't sit very well with me.

76
00:06:08,302 --> 00:06:10,429
I've had it up to here,
as a matter of fact.

77
00:06:10,604 --> 00:06:12,128
Good for you.

78
00:06:14,909 --> 00:06:18,242
How bad do you and your friend
want him?

79
00:06:23,050 --> 00:06:25,177
I don't have to explain
what would happen...

80
00:06:25,352 --> 00:06:28,185
...if there was even a hint that
I set up one of my own clients.

81
00:06:28,355 --> 00:06:31,017
Aren't you afraid
you'll wind up on his hit list?

82
00:06:31,659 --> 00:06:34,992
- It crossed my mind.
- Your name will never come up.

83
00:06:35,162 --> 00:06:38,188
- You never met Rick, have you?
- No.

84
00:06:38,365 --> 00:06:39,957
I'll set up a meeting.

85
00:06:46,240 --> 00:06:47,901
Mr. Masters?

86
00:06:50,077 --> 00:06:52,443
How you doing? Ben Jessup.

87
00:06:52,613 --> 00:06:55,548
- My associate, Dr. George Victor.
- A pleasure. How do you do?

88
00:06:55,716 --> 00:06:57,741
Cut yourself shaving,
Mr. Jessup?

89
00:06:57,918 --> 00:07:01,355
No. As a matter of fact,
I got hit by a tennis ball.

90
00:07:01,523 --> 00:07:04,356
- You're in from Palm Springs, huh?
- Yeah.

91
00:07:04,526 --> 00:07:06,756
- What's the weather like there?
- It's really nice.

92
00:07:06,928 --> 00:07:08,953
We've been up here
the last few days.

93
00:07:09,864 --> 00:07:13,265
I've got a friend in Palm Springs,
Lenny Green.

94
00:07:13,435 --> 00:07:17,030
He owns the Oasis.
Do you know him?

95
00:07:17,205 --> 00:07:20,106
I got a friend in Hollywood,
Donald Duck. You know him?

96
00:07:22,077 --> 00:07:25,069
I understand you gentlemen
do some island banking.

97
00:07:25,246 --> 00:07:26,577
- That's right.
- Where?

98
00:07:26,748 --> 00:07:27,806
Cayman Islands.

99
00:07:27,983 --> 00:07:30,213
- Good business?
- Not bad.

100
00:07:30,385 --> 00:07:32,114
What sort of banking?

101
00:07:32,287 --> 00:07:34,312
We're a Dutch Antilles company.

102
00:07:34,489 --> 00:07:37,424
We loan money to various
enterprises here in the States.

103
00:07:37,592 --> 00:07:40,527
Loans aren't secured by real estate
or anything else down there.

104
00:07:40,962 --> 00:07:43,021
Hey, Rick, you got a phone call, man.

105
00:07:50,872 --> 00:07:52,601
Which one is it?

106
00:07:53,942 --> 00:07:55,239
There's nothing in there.

107
00:07:55,410 --> 00:07:57,742
Some tennis rackets
in the trunk...

108
00:07:57,912 --> 00:08:00,608
...men's clothing with Palm Springs
store labels...

109
00:08:00,782 --> 00:08:04,879
...some business letters with return
addresses in the Cayman Islands.

110
00:08:05,054 --> 00:08:06,521
What did the letters say?

111
00:08:06,689 --> 00:08:10,250
Something about "Please forward the stock
we discussed," or something like that.

112
00:08:10,426 --> 00:08:12,587
Who were the letters addressed to?

113
00:08:12,762 --> 00:08:15,560
Caribbean Banking Unlimited,
Dutch Antilles.

114
00:08:15,731 --> 00:08:19,758
Did you notice the names Jessup
or Victor on any of the letters?

115
00:08:19,935 --> 00:08:22,529
- No.
- Okay.

116
00:08:22,805 --> 00:08:24,363
Thanks.

117
00:08:24,807 --> 00:08:26,604
What kind of paper
are we talking about?

118
00:08:26,776 --> 00:08:29,939
Hundreds and 50s paper. We need
at least 10 different serial numbers.

119
00:08:30,112 --> 00:08:32,979
- How much?
- A million dollars.

120
00:08:37,186 --> 00:08:39,313
How you going to use it?

121
00:08:39,488 --> 00:08:41,683
What business is that of yours?

122
00:08:42,591 --> 00:08:45,458
It's always my business,
Mr. Jessup.

123
00:08:47,163 --> 00:08:49,427
Nothing will be passed up here.

124
00:08:49,598 --> 00:08:55,002
Our play involves a gentleman who wishes
to launder bonds, protect his tax position.

125
00:08:55,471 --> 00:08:58,440
My end is 20 percent.

126
00:08:58,607 --> 00:09:02,134
We never pay more than 10. We have
limitations that we have to abide by.

127
00:09:02,311 --> 00:09:05,941
Different serial numbers are
a real pain in the ass for me.

128
00:09:06,116 --> 00:09:10,644
I gotta make different plates and wear
rubber gloves during the whole operation.

129
00:09:10,821 --> 00:09:14,279
Have you ever tried to work
with rubber gloves on?

130
00:09:14,458 --> 00:09:16,551
Fifteen percent.

131
00:09:16,726 --> 00:09:19,251
I don't negotiate.

132
00:09:19,429 --> 00:09:21,693
I might if I knew you, but I don't.

133
00:09:21,865 --> 00:09:23,992
And I don't like what I see.

134
00:09:26,103 --> 00:09:27,695
All right.

135
00:09:29,272 --> 00:09:32,469
I start as soon as
I get a down payment.

136
00:09:32,642 --> 00:09:36,043
In this case, I'll take
30,000 up front...

137
00:09:36,213 --> 00:09:38,113
- ... and the rest on delivery.
- What?

138
00:09:38,281 --> 00:09:43,014
Everybody knows Rick Masters
won't go near a job without front money.

139
00:09:43,186 --> 00:09:47,919
You should also know that I've never
fucked a customer out of his front money.

140
00:09:48,558 --> 00:09:53,621
I've been coming to this gym three
or four times a week for five years.

141
00:09:53,797 --> 00:09:58,598
I'm an easy man to find.
My reputation speaks for itself.

142
00:09:58,768 --> 00:10:02,727
The fact is, that if you can't
come up with the front money...

143
00:10:02,906 --> 00:10:04,703
...you're not for real.

144
00:10:06,978 --> 00:10:09,742
No way I can get you
30 K to make a buy.

145
00:10:09,914 --> 00:10:12,041
You'd hear laughter
all the way from Washington.

146
00:10:12,216 --> 00:10:14,684
Masters beats the government
out of that much in a day.

147
00:10:14,852 --> 00:10:18,083
We've got a chance for a hand-to-hand
buy which he cannot beat in court.

148
00:10:18,256 --> 00:10:21,020
You're not the first agents
to get next to Masters.

149
00:10:21,192 --> 00:10:22,887
He always asks for big front money.

150
00:10:23,060 --> 00:10:25,494
Nobody ever approves it,
and he stays on the street.

151
00:10:25,663 --> 00:10:28,154
The limit for buys is 10 grand.
I don't make the rules.

152
00:10:28,332 --> 00:10:31,096
Look, you could get this
approved if you wanted to.

153
00:10:31,269 --> 00:10:34,397
302.5.

154
00:10:34,572 --> 00:10:37,063
- What?
-302.5.

155
00:10:37,241 --> 00:10:41,302
You violated section 302.5.
The manual says:

156
00:10:41,479 --> 00:10:44,471
"The agents must notify
the agent in charge"...

157
00:10:44,649 --> 00:10:47,618
...that's me, "of all
ongoing investigations. "

158
00:10:47,785 --> 00:10:50,481
You violated this section, and
I'm not gonna cover your ass.

159
00:10:50,655 --> 00:10:54,147
- Yeah, well, I'm not asking you to.
- You lost a federal prisoner!

160
00:10:54,325 --> 00:10:58,125
And I want Cody back!
Where the hell were you?

161
00:10:58,296 --> 00:11:00,696
- He wasn't with me, all right?
- Why not?

162
00:11:00,865 --> 00:11:03,197
Look, I lost him.
I'll get him back.

163
00:11:03,367 --> 00:11:05,995
Thank you, Gilbert.

164
00:11:06,037 --> 00:11:08,442
I think we should have offered
Masters the 10 grand.

165
00:11:08,614 --> 00:11:11,481
- I bet he'd have gone for it.
- Never happen, Johnny.

166
00:11:22,228 --> 00:11:23,991
Let me try something on you.

167
00:11:24,163 --> 00:11:25,425
Yeah, what?

168
00:11:25,598 --> 00:11:28,829
Ruth tells me there's a guy coming in
Thursday to buy stolen diamonds.

169
00:11:29,001 --> 00:11:32,630
He's gonna be carrying
$50,000 cash.

170
00:11:32,805 --> 00:11:34,170
So?

171
00:11:34,340 --> 00:11:36,774
So, what do you think?

172
00:11:36,943 --> 00:11:39,207
What do I think about what?

173
00:11:39,946 --> 00:11:43,643
The guy comes in Thursday,
Union Station.

174
00:11:43,816 --> 00:11:44,976
No muss, no fuss.

175
00:11:45,151 --> 00:11:48,177
If everything doesn't look like
a piece of cake, we just walk away.

176
00:11:51,591 --> 00:11:54,389
- Jesus! You gotta be kidding.
- When you came to me, I told you...

177
00:11:54,560 --> 00:11:57,893
...that I was gonna get Masters
and I didn't give a shit how I did it.

178
00:11:58,064 --> 00:12:01,158
- So now you want to commit a robbery?
- I wouldn't call it that.

179
00:12:01,334 --> 00:12:02,392
What would you call it?

180
00:12:02,568 --> 00:12:05,503
Taking down a douche bag
who's trying to break the law.

181
00:12:05,671 --> 00:12:08,572
- If it turns to shit?
- We say, "Fuck it," walk away.

182
00:12:10,911 --> 00:12:13,379
The front money is the only way
to get Masters to print.

183
00:12:13,547 --> 00:12:16,072
I don't give a shit. I won't pull a heist.
I don't care.

184
00:12:16,250 --> 00:12:17,877
- He's a fence.
- Who do you think I am?

185
00:12:18,051 --> 00:12:20,178
If he gets ripped off,
he can't call the police.

186
00:12:20,354 --> 00:12:23,755
Why don't you just blow his brains out?
That's what you want to do anyway.

187
00:12:23,924 --> 00:12:26,222
All I'm asking you to do is drive the car.

188
00:12:26,527 --> 00:12:29,894
Steal real money to buy counterfeit
money? How's that gonna look in court?

189
00:12:30,063 --> 00:12:32,327
- His word against ours.
- His lawyer is Bob Grimes.

190
00:12:32,499 --> 00:12:35,935
- It's Masters' word against ours.
- You got a couple of screws loose, pal.

191
00:12:38,472 --> 00:12:42,033
Listen, I say we go to Bateman
one more time.

192
00:12:42,209 --> 00:12:45,007
Oh, fuck Bateman. He's a pencil-neck.
He's out to save his ass.

193
00:12:45,179 --> 00:12:47,147
I think you're pushing it too fast. We can-

194
00:12:47,314 --> 00:12:49,646
It's happening Thursday.
We gotta go down in 24 hours.

195
00:12:49,816 --> 00:12:52,046
My father was a cop, brother's a cop.
You're ask-

196
00:12:52,219 --> 00:12:53,550
Give me a break!

197
00:12:53,720 --> 00:12:55,984
You got the wrong guy, okay?
You got the wrong guy.

198
00:12:56,156 --> 00:13:00,092
You're right I got the wrong guy, pal.
You ain't my partner.

199
00:13:00,260 --> 00:13:03,354
You ain't even my fucking friend.
Let me give you a piece of advice:

200
00:13:03,530 --> 00:13:05,020
You better get into protection...

201
00:13:05,199 --> 00:13:07,827
...because you ain't shit on
the street, you understand?

202
00:13:08,001 --> 00:13:09,628
You ain't got the nuts!

203
00:13:09,937 --> 00:13:11,199
Kiss my ass!

204
00:13:11,371 --> 00:13:13,305
Pussy motherfucker.

205
00:13:48,475 --> 00:13:50,443
How sure is this thing tomorrow?

206
00:13:51,812 --> 00:13:55,612
- You said you weren't interested.
- Well, now I am interested, okay?

207
00:13:55,782 --> 00:13:57,841
All I know is what I told you.

208
00:13:59,119 --> 00:14:02,452
He's on the number 11 Amtrak leaving
San Francisco 7:00 in the morning...

209
00:14:02,623 --> 00:14:04,955
...getting into Union Station
at 4:35.

210
00:14:06,426 --> 00:14:09,452
How do you remember that?
You wrote it down or what?

211
00:14:13,001 --> 00:14:15,026
Who's the seller?

212
00:14:15,203 --> 00:14:17,637
A guy I know.

213
00:14:17,805 --> 00:14:20,706
What did this guy you know
tell you?

214
00:14:20,875 --> 00:14:23,605
That a Chinaman comes down
from San Francisco...

215
00:14:23,778 --> 00:14:26,747
...buys diamonds, gold, whatever.

216
00:14:26,914 --> 00:14:28,245
What's his name?

217
00:14:29,684 --> 00:14:32,414
Ling. Thomas Ling.

218
00:14:33,888 --> 00:14:36,118
What's your end?

219
00:14:36,291 --> 00:14:39,419
- Nothing. Just a guy I know tells me things.
- A guy you know.

220
00:14:39,594 --> 00:14:43,030
A guy you know actually gives you
the train the buyer's coming in on?

221
00:14:43,464 --> 00:14:45,694
Of course not.

222
00:14:45,867 --> 00:14:48,199
I called Amtrak
and got his reservation.

223
00:14:49,771 --> 00:14:51,295
Why?

224
00:14:51,472 --> 00:14:53,064
Why did you do that?

225
00:14:57,378 --> 00:14:59,107
ID and $1.50.

226
00:15:11,693 --> 00:15:14,993
You were planning on having someone
else take him down, is that right?

227
00:15:17,132 --> 00:15:18,759
Thought about it.

228
00:15:20,402 --> 00:15:25,032
- Why are you suddenly interested?
- It fits some other shit that's happening.

229
00:15:25,207 --> 00:15:27,300
- You gonna bag him?
- I might.

230
00:15:27,476 --> 00:15:30,843
How you gonna do it if he's carrying
real cash and hasn't committed a crime?

231
00:15:31,013 --> 00:15:32,742
I can do whatever I want.

232
00:15:34,850 --> 00:15:37,751
How much is in this for me?

233
00:15:37,920 --> 00:15:39,717
How much of what?

234
00:15:39,888 --> 00:15:42,448
Don't shit me.
I know what you're gonna do.

235
00:15:42,624 --> 00:15:45,024
And they're gonna think I set it up.

236
00:15:47,529 --> 00:15:51,488
- All right. I'll give you 5 G's.
- Not enough.

237
00:15:54,803 --> 00:15:57,670
And my promise not
to throw you back in the joint.

238
00:16:39,716 --> 00:16:43,652
Mr. Thomas Ling, please come
to passenger services.

239
00:16:43,820 --> 00:16:48,189
Passenger Thomas Ling,
arriving on Amtrak 708...

240
00:16:48,358 --> 00:16:50,883
...please come
to passenger services.

241
00:16:54,497 --> 00:16:55,930
I'm Thomas Ling.

242
00:17:12,949 --> 00:17:14,746
- You know what this is?
- What's the game?

243
00:17:14,918 --> 00:17:16,545
- It's no game. Just walk.
- Why?

244
00:17:16,719 --> 00:17:19,814
Why? Because if you don't
I'll blow your fucking heart out.

245
00:17:52,723 --> 00:17:55,317
- Where's the key?
- I don't have it.

246
00:17:55,493 --> 00:17:57,654
He doesn't have it. What a guy.

247
00:18:46,011 --> 00:18:47,535
Over the chain.

248
00:18:49,247 --> 00:18:51,767
Come on, get over the chain!

249
00:18:51,900 --> 00:18:53,959
Grab the wall. Right there.

250
00:18:56,271 --> 00:18:57,704
Hold him.

251
00:19:03,011 --> 00:19:04,171
Damn it!

252
00:19:08,083 --> 00:19:09,778
Fucker!

253
00:19:15,223 --> 00:19:16,952
Come on, man. What the fuck?

254
00:19:17,125 --> 00:19:19,184
We got people all over the fucking place!

255
00:19:27,804 --> 00:19:29,431
That's very funny.

256
00:19:31,040 --> 00:19:33,907
Look, he doesn't have the money!
Let's get the fuck out of here!

257
00:19:34,077 --> 00:19:35,339
Where is it?

258
00:19:37,046 --> 00:19:40,209
Son of a bitch, move one more muscle
and I'll blow your brains out!

259
00:19:40,383 --> 00:19:43,011
You got that? Freeze!
Get down on your fucking knees!

260
00:19:43,186 --> 00:19:46,087
Put your hands
behind your fucking head!

261
00:19:46,255 --> 00:19:47,745
Goddamn it.

262
00:19:50,326 --> 00:19:52,624
You're wearing it, ain't you?

263
00:19:52,795 --> 00:19:54,228
Get your clothes off.

264
00:20:00,103 --> 00:20:02,298
Come on, two hands!
Take your shirt off!

265
00:20:05,208 --> 00:20:07,642
Ah, that's great.

266
00:20:07,810 --> 00:20:10,176
- That's it.
- Get your pants down.

267
00:20:10,346 --> 00:20:13,509
- What?! Let's get the fuck out of here!
- Get your pants down.

268
00:20:46,650 --> 00:20:48,515
Fuck. Shit.

269
00:20:53,457 --> 00:20:55,823
- You all right?
- Oh, Christ.

270
00:20:55,993 --> 00:20:58,791
Let's get out of here. Come on.

271
00:21:01,198 --> 00:21:04,167
Let's get out of here.
Come on. Come on!

272
00:21:08,172 --> 00:21:09,537
Come on!

273
00:21:58,823 --> 00:22:01,815
- You hit?
- I don't know. I don't think so.

274
00:22:36,896 --> 00:22:39,865
- Look out, for God's sake!
- Get them out of there, Johnny!

275
00:22:40,232 --> 00:22:42,860
- Move the truck!
- What are you doing? Trying to hurt-

276
00:22:43,035 --> 00:22:45,902
Shut up! Whose ever truck that is,
move it out of the way now!

277
00:22:46,071 --> 00:22:48,039
Back it up, Steve, back it up!

278
00:22:52,311 --> 00:22:53,471
Look out!

279
00:22:54,546 --> 00:22:56,013
Look out!

280
00:22:57,082 --> 00:22:59,550
- Get out of there!
- You fucking assholes!

281
00:23:17,670 --> 00:23:18,898
Go left!

282
00:24:26,606 --> 00:24:29,097
Hold on. Hold on!

283
00:24:48,696 --> 00:24:51,062
- I think we lost the mothers.
- I don't see them, man.

284
00:24:51,232 --> 00:24:53,792
I don't see them.
You fucking did it, man!

285
00:24:53,968 --> 00:24:55,265
- We made it, baby!
- We did it!

286
00:24:59,173 --> 00:25:02,074
Goddamn it, we did it!

287
00:25:02,243 --> 00:25:06,009
Oh, shit! You had me scared
out of my fucking mind, man.

288
00:25:06,180 --> 00:25:07,841
You had me scared out of my fucking-

289
00:25:11,752 --> 00:25:13,845
Sons of bitches!

290
00:25:14,021 --> 00:25:16,819
- Is it the same guy?
- It's two different guys.

291
00:25:16,991 --> 00:25:18,356
It's two Chevys!

292
00:25:25,366 --> 00:25:27,231
What the hell is going on?

293
00:25:27,401 --> 00:25:30,097
- Who the fuck are these guys?
- They're all over the place.

294
00:25:30,271 --> 00:25:31,830
Hang on. Hang on, Johnny!

295
00:25:44,019 --> 00:25:47,250
Piece of cake, huh? You son of a bitch!

296
00:25:50,892 --> 00:25:54,157
Cap one off, Johnny.
Get them off of me!

297
00:26:56,117 --> 00:26:58,051
They're all over the place.

298
00:27:02,390 --> 00:27:03,857
We're going this way.

299
00:27:08,163 --> 00:27:09,858
Get the fuck out of here!

300
00:27:10,031 --> 00:27:11,464
Fucking asshole!

301
00:27:17,172 --> 00:27:19,333
- Look out!
- Crazy fucking asshole.

302
00:27:19,641 --> 00:27:21,131
Hold it!

303
00:27:24,546 --> 00:27:26,173
Shut up!

304
00:27:36,592 --> 00:27:38,822
Get out of the way!
Get out of there!

305
00:27:53,609 --> 00:27:55,201
Oh, my God!

306
00:27:58,514 --> 00:28:00,539
Get out of the way!

307
00:28:01,150 --> 00:28:02,674
Get out of the way!

308
00:28:11,160 --> 00:28:12,627
Get out of the way!

309
00:28:16,098 --> 00:28:18,123
God, no!

310
00:28:31,647 --> 00:28:34,844
There's a minor tie-up on the north
Long Beach right near Henry Ford.

311
00:28:35,017 --> 00:28:38,681
A couple cars tangled there. Shouldn't
take too long to get this to the shoulder.

312
00:28:38,855 --> 00:28:41,346
It's a very simple affair,
no injuries involved.

313
00:28:41,625 --> 00:28:45,061
It shouldn't cost more than a few minutes
if you're heading north on the 710.

314
00:28:45,228 --> 00:28:47,992
I'm Stacey Binn
for Metro Traffic Control.

315
00:29:27,270 --> 00:29:29,602
What are we gonna do?!

316
00:29:30,807 --> 00:29:33,901
We're going to an auto-parts
store and get a new window.

317
00:29:35,045 --> 00:29:37,172
If we'd have totaled it,
we'd have been screwed.

318
00:29:37,347 --> 00:29:39,543
We'd never have gotten it back
to the motor pool.

319
00:29:39,717 --> 00:29:42,311
We lucked out, Johnny!
We lucked out!

320
00:30:02,206 --> 00:30:04,766
Listen, I'll have to get back to you, okay?

321
00:30:07,812 --> 00:30:11,441
- What happened? I called you all day.
- Clockwork, baby.

322
00:30:11,615 --> 00:30:14,140
Like fucking clockwork.

323
00:30:15,553 --> 00:30:16,781
Did you hear anything?

324
00:30:16,954 --> 00:30:19,445
My friend called. He said
the Chinaman never showed up.

325
00:30:19,623 --> 00:30:20,851
He sure didn't.

326
00:30:22,593 --> 00:30:24,424
God, 50,000 bucks.

327
00:30:24,595 --> 00:30:26,893
Fuck. Quintin Dailey got 30 points,
they said.

328
00:30:27,064 --> 00:30:30,727
That guy's unbelievable, man. Say all you
want about Michael Jordan. Great player.

329
00:30:30,901 --> 00:30:32,869
They'll know somebody
set the Chinaman up.

330
00:30:33,037 --> 00:30:35,505
Quintin Dailey's
got a gun like a howitzer, man.

331
00:30:35,673 --> 00:30:37,868
Thirty feet. Boom, boom, boom.

332
00:30:38,042 --> 00:30:40,773
He gets hot, he's fabulous.
Fucking Orlando Woolridge-

333
00:30:40,946 --> 00:30:43,779
I'm worried. I'm worried!

334
00:30:51,923 --> 00:30:54,221
The stars are God's eyes.

335
00:31:11,977 --> 00:31:13,945
What's the matter with you?

336
00:31:20,352 --> 00:31:24,413
The last item on the agenda
is a bulletin from the FBI.

337
00:31:24,589 --> 00:31:28,457
"On January 24th, FBI
Special Agent Raymond Fong...

338
00:31:28,627 --> 00:31:32,222
...of the bureau of San Francisco
field office was kidnapped and robbed...

339
00:31:32,397 --> 00:31:34,695
...of $50,000 in government funds.

340
00:31:34,866 --> 00:31:37,733
Fong, who was acting
in an undercover capacity...

341
00:31:37,903 --> 00:31:40,531
...as part of a bureau-sponsored
sting operation...

342
00:31:40,705 --> 00:31:44,836
...was abducted and murdered
shortly after arriving at Union Station.

343
00:31:45,011 --> 00:31:49,505
The suspects are described as white males,
30 to 35 years old...

344
00:31:49,682 --> 00:31:52,344
...one with black, the other
with brown hair.

345
00:31:52,518 --> 00:31:57,546
They eluded Fong's covering agents
and fled in a beige, late-model Chevrolet.

346
00:31:57,723 --> 00:31:59,247
Anyone having information...

347
00:31:59,425 --> 00:32:02,826
...contact the special agent
in charge, FBI, San Francisco. "

348
00:32:02,995 --> 00:32:06,863
- Is there a license on the vehicle?
- There's no plates on the vehicle.

349
00:32:07,333 --> 00:32:12,100
This is what happens when proper
covering procedures are not followed.

350
00:32:13,139 --> 00:32:15,300
We got an FBI agent killed.
You hear that?

351
00:32:15,475 --> 00:32:19,241
- What do you want me to do about it?
- It's just a matter of time before they ID us.

352
00:32:19,412 --> 00:32:21,744
- They got a good look.
- A face is shit without a name.

353
00:32:21,914 --> 00:32:23,711
They got a make on the car!

354
00:32:23,883 --> 00:32:28,980
They wouldn't have sent it out if they
had anything. They're grabbing at shit.

355
00:32:29,155 --> 00:32:32,249
Oh, Jesus. So, what are we
gonna do about the 50 grand?

356
00:32:32,425 --> 00:32:35,019
We'll make the buy from Masters,
just like we planned.

357
00:32:36,662 --> 00:32:40,223
Are you out of your fucking mind
or something?

358
00:32:40,399 --> 00:32:43,197
Ray. Hey, I want
a standup ashtray.

359
00:32:43,370 --> 00:32:46,100
These guys are stealing
all of my ashtrays.

360
00:32:48,942 --> 00:32:50,910
Just like we planned, amigo.

361
00:33:14,635 --> 00:33:16,227
Over here.

362
00:33:24,645 --> 00:33:26,670
Who are they?

363
00:33:26,847 --> 00:33:28,246
Who are you?

364
00:33:28,415 --> 00:33:30,110
This is Mr. Jessup...

365
00:33:30,284 --> 00:33:33,253
...whose name isn't really Jessup.

366
00:33:33,420 --> 00:33:37,481
He says he's from Palm Springs,
but doesn't have a tan.

367
00:33:40,360 --> 00:33:41,793
You're not wired, are you?

368
00:33:46,668 --> 00:33:48,329
Is this my package?

369
00:33:59,581 --> 00:34:01,139
Look okay?

370
00:34:09,791 --> 00:34:11,588
You're beautiful.

371
00:34:14,395 --> 00:34:16,386
When do I get delivery?

372
00:34:18,266 --> 00:34:21,326
How about Friday night?

373
00:34:21,502 --> 00:34:24,300
If I don't hear from you by Friday
I'm coming back to get this.

374
00:34:24,472 --> 00:34:26,337
That's understandable.

375
00:34:28,142 --> 00:34:29,666
Oh, Mr. Jessup...

376
00:34:32,614 --> 00:34:34,946
...like your work?

377
00:34:57,436 --> 00:34:59,996
Your only defense is to say
you were working undercover...

378
00:35:00,173 --> 00:35:02,573
...without the knowledge
of your supervisors.

379
00:35:02,742 --> 00:35:05,836
You were trying to get next to Masters
and things got out of hand...

380
00:35:06,012 --> 00:35:08,480
...and you intended
to return the money.

381
00:35:08,648 --> 00:35:10,878
The problem is, you'll have
to take the stand...

382
00:35:11,050 --> 00:35:14,383
...and the prosecutor
can ask you anything he wants.

383
00:35:14,554 --> 00:35:17,819
Frankly, I don't think
you can beat the case in court.

384
00:35:19,826 --> 00:35:23,057
Because I represent Masters
I can't get deeply involved in your case...

385
00:35:23,229 --> 00:35:26,289
- ... if you see what I mean.
- So, what should I do?

386
00:35:26,465 --> 00:35:29,866
You beat them to the punch with
the U.S. attorney and you make a deal.

387
00:35:30,036 --> 00:35:31,230
What kind of deal?

388
00:35:31,404 --> 00:35:35,534
You offer to plead guilty and
to testify against your partner.

389
00:35:35,708 --> 00:35:38,802
The FBI's not gonna want
a lot of publicity on this.

390
00:35:38,978 --> 00:35:41,708
I suspect they'll go along
with a guilty plea.

391
00:35:43,916 --> 00:35:46,316
How much time
would I have to do?

392
00:35:46,485 --> 00:35:48,784
I could probably get you off
with seven years.

393
00:35:48,956 --> 00:35:52,187
You won't have to do seven,
of course. Probably a year and a half.

394
00:35:53,827 --> 00:35:57,422
- But you can't get involved, right?
- Not directly.

395
00:36:00,968 --> 00:36:03,903
What would it cost
for your indirect involvement?

396
00:36:04,071 --> 00:36:06,005
Fifty thousand dollars.

397
00:36:14,014 --> 00:36:15,208
I know it's a tough call...

398
00:36:15,382 --> 00:36:18,681
...but it's one you're going
to have to make rather quickly.

399
00:36:22,456 --> 00:36:24,447
I can't hand up my partner.

400
00:36:25,959 --> 00:36:31,090
I can't do it, even if I have to go
to the joint.

401
00:37:14,042 --> 00:37:16,272
Who is it?

402
00:37:16,444 --> 00:37:17,604
Who's there?

403
00:37:34,929 --> 00:37:36,487
Honey?

404
00:38:13,135 --> 00:38:14,693
Shit, you okay, Carl?

405
00:38:16,338 --> 00:38:19,273
- Guess we all screw up, huh, Carl?
- How did you find me?

406
00:38:19,441 --> 00:38:22,342
Your bimbo girlfriend's listed
in the Screen Actors directory.

407
00:38:22,511 --> 00:38:25,810
- Carl, I swear I never saw this guy!
- Sit down, Claudia, and shut up!

408
00:38:25,981 --> 00:38:27,471
You know, you're a lucky man.

409
00:38:27,649 --> 00:38:30,413
I didn't tell the judge
you played hooky on me.

410
00:38:30,586 --> 00:38:32,451
Now you're gonna help me,
aren't you, Carl?

411
00:38:32,621 --> 00:38:35,351
Come on, asshole, let's go.
Move!

412
00:38:48,003 --> 00:38:49,994
Hi. Is Chance there?

413
00:38:52,174 --> 00:38:55,041
I want to talk to Chance.

414
00:38:55,210 --> 00:38:59,044
Is this 471-4421?

415
00:38:59,214 --> 00:39:01,148
This is John Vukovich. Where is he?

416
00:39:01,550 --> 00:39:04,280
There's no one here
by that name.

417
00:39:04,453 --> 00:39:06,182
Who is it?

418
00:39:06,355 --> 00:39:08,619
John Vukovich.

419
00:39:08,791 --> 00:39:10,691
John.

420
00:39:10,859 --> 00:39:13,191
Listen, I got Cody back.
I know where the plant is.

421
00:39:13,629 --> 00:39:16,928
We gotta talk. We gotta go to Bateman.
I can't live with this any longer.

422
00:39:17,099 --> 00:39:20,091
I talked to Masters. We're on tonight.

423
00:39:20,269 --> 00:39:24,399
- What are you talking about?
- We're on with Masters tonight.

424
00:39:40,556 --> 00:39:43,548
- Do you need me tonight?
- No, I don't think so.

425
00:39:45,561 --> 00:39:47,654
What time do you get off?

426
00:39:47,830 --> 00:39:49,627
- About 12:30.
- Okay.

427
00:39:49,798 --> 00:39:55,635
I got a few things to do, and then
I'll drop by and pick you up at the club.

428
00:39:55,805 --> 00:39:57,932
All right?

429
00:39:58,108 --> 00:39:59,598
You look beautiful.

430
00:40:09,486 --> 00:40:11,545
I got a little surprise for you.

431
00:41:27,757 --> 00:41:29,554
You packing?

432
00:41:29,726 --> 00:41:32,320
Check and see if you got
a wire on there, brother.

433
00:41:32,495 --> 00:41:34,588
- Let's see that bag.
- You don't touch me.

434
00:41:34,764 --> 00:41:36,994
- Look, dickhead!
- Keep your hands off me.

435
00:41:37,166 --> 00:41:39,896
If you want to get up them stairs
you gotta go through me.

436
00:41:40,069 --> 00:41:42,799
You go tell your little friend
that if he thinks I'm a fed-

437
00:41:42,972 --> 00:41:44,303
It's okay, Jack.

438
00:41:48,411 --> 00:41:50,276
Well, go on up, tough guy.

439
00:41:53,483 --> 00:41:56,418
So, what's in the briefcase, doctor?

440
00:42:08,999 --> 00:42:13,834
Hey, we're the ones who fronted the 30
grand and agreed to do this on your turf.

441
00:42:14,004 --> 00:42:16,529
Before you touch shit,
I want to see the funny money.

442
00:42:38,262 --> 00:42:39,854
Locker 38.

443
00:43:17,569 --> 00:43:18,934
Okay?

444
00:43:22,607 --> 00:43:24,370
You're beautiful.

445
00:43:33,218 --> 00:43:35,982
- You're under arrest! Turn around!
- Right there, asshole!

446
00:43:36,154 --> 00:43:38,179
Hands on top of the locker!

447
00:43:38,356 --> 00:43:41,086
Go on, grab the top
of the fucking locker!

448
00:43:41,259 --> 00:43:43,557
That's right! You're under arrest, moron!

449
00:43:45,597 --> 00:43:47,292
Go on, cuff the ape.

450
00:43:47,465 --> 00:43:51,128
- Don't move!
- How you doing, pal? Huh?

451
00:43:51,302 --> 00:43:55,261
This is from Jimmy Hart. From the desert.
Remember this? Suck on that for a while.

452
00:43:56,007 --> 00:43:58,805
- Freeze it up, pal!
- Jesus Christ!

453
00:44:07,853 --> 00:44:09,013
Oh, God.

454
00:44:12,525 --> 00:44:15,926
Talk to me. Talk to me, man.
Talk to me.

455
00:44:16,095 --> 00:44:18,029
Talk to me!

456
00:44:18,931 --> 00:44:20,091
Oh, God.

457
00:44:32,311 --> 00:44:35,041
You can't do this to me!

458
00:46:46,681 --> 00:46:49,878
Why didn't you take the deal
Grimes offered you?

459
00:46:52,586 --> 00:46:54,952
Wouldn't roll over on your partner, huh?

460
00:46:55,122 --> 00:46:56,419
Get up.

461
00:46:59,493 --> 00:47:01,620
Get up, you son of a bitch!

462
00:48:51,374 --> 00:48:54,309
You might want these.
They're very personal.

463
00:48:54,844 --> 00:48:56,243
Yes.

464
00:48:57,680 --> 00:49:01,377
I can't seem to find
any of his paintings.

465
00:49:01,551 --> 00:49:04,611
He told me he did
two large portraits of you.

466
00:49:04,787 --> 00:49:07,119
They might be worth
a lot of money.

467
00:49:07,290 --> 00:49:10,556
He used to burn a lot of things.
Maybe he burned them.

468
00:49:14,331 --> 00:49:17,596
I can't understand how
you stayed with him so long.

469
00:49:17,768 --> 00:49:20,794
Why did you work for him?

470
00:49:20,971 --> 00:49:22,563
It was just business.

471
00:50:29,974 --> 00:50:32,772
- Who is it?
- John Vukovich.

472
00:50:40,618 --> 00:50:43,416
- What do you want?
- Chance was my partner.

473
00:50:43,588 --> 00:50:47,217
- I know who you are. What do you want?
- Did you know he was dead?

474
00:50:47,392 --> 00:50:49,587
- I'm busy now.
- Open it.

475
00:51:04,309 --> 00:51:06,539
You going somewhere?

476
00:51:06,711 --> 00:51:08,303
I'm leaving the city.

477
00:51:11,182 --> 00:51:15,381
Well, there's a little matter of 20 grand
that belongs to the federal government.

478
00:51:15,554 --> 00:51:19,012
Chance said he left it with you.
We want it back.

479
00:51:19,191 --> 00:51:22,922
Look, part of that money was mine.
I had debts, people leaning on me.

480
00:51:25,131 --> 00:51:28,157
- I got ripped off for the rest-
- You set us up, didn't you, Ruth?

481
00:51:28,334 --> 00:51:30,199
You knew that Chinaman was FBI.

482
00:51:30,369 --> 00:51:33,065
What? Are you crazy?

483
00:51:33,239 --> 00:51:34,934
Come on. Don't shine me on.

484
00:51:35,107 --> 00:51:40,101
If you're gonna start by bullshitting, we're
gonna get off to a very bad relationship.

485
00:51:41,113 --> 00:51:43,206
What are you talking about?

486
00:51:44,550 --> 00:51:46,882
You're working for me now.

