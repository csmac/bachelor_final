1
00:02:51,487 --> 00:02:52,989
? COVER ME ?

2
00:02:54,991 --> 00:02:56,492
? WHEN I RUN ?

3
00:02:57,994 --> 00:02:59,495
? COVER ME ?

4
00:03:01,497 --> 00:03:02,498
? THROUGH THE FIRE ?

5
00:03:11,641 --> 00:03:12,141
? COVER ME ?

6
00:03:14,143 --> 00:03:15,645
? DARLING, PLEASE ?

7
00:03:35,665 --> 00:03:37,667
? WHEELS KEEP TURNING ?

8
00:03:42,672 --> 00:03:44,173
? SOMETHING'�S BURNIN'� ?

9
00:03:48,177 --> 00:03:50,680
? DON'�T LIKE IT,
BUT I GUESS I'�M LEARNIN'� ?

10
00:03:52,682 --> 00:03:53,683
? SHOCK ?

11
00:03:56,185 --> 00:03:57,186
? SHOCK ?

12
00:04:02,191 --> 00:04:03,693
? MONKEY ?

13
00:04:05,194 --> 00:04:06,696
? SHOCK ?

14
00:04:09,198 --> 00:04:10,199
? SHOCK ?

15
00:04:36,726 --> 00:04:39,228
JOHNS HOPKINS,
CANCER.

16
00:04:40,229 --> 00:04:42,231
[WHIMPERING]

17
00:04:51,741 --> 00:04:53,743
DEPARTMENT OF PSYCHOLOGY.

18
00:04:58,748 --> 00:05:00,249
? SHOCK THE MONKEY ?

19
00:05:01,784 --> 00:05:03,786
? SHOCK THE MONKEY
TONIGHT ???

20
00:05:11,294 --> 00:05:13,296
NO ONE'�S
GOING TO HURT YOU.

21
00:05:15,798 --> 00:05:18,301
TERI, COME AND MEET
YOUR NEW STUDENT.

22
00:05:27,944 --> 00:05:29,946
WE HAVE A TENDENCY
TO EXPECT--

23
00:05:32,198 --> 00:05:34,200
AS I WAS SAYING.

24
00:05:38,705 --> 00:05:39,706
HI.

25
00:05:45,211 --> 00:05:46,713
VIRGIL, LOOK.

26
00:06:03,229 --> 00:06:04,230
APPLE.

27
00:06:06,232 --> 00:06:08,234
YOU DON'�T WANT IT?

28
00:06:29,756 --> 00:06:30,757
APPLE.

29
00:06:37,764 --> 00:06:38,765
LOOK, LOOK.

30
00:06:53,146 --> 00:06:55,648
AH AH AH AH!

31
00:07:09,662 --> 00:07:11,164
MMM-MMM-MMM.

32
00:07:49,118 --> 00:07:50,620
GOOD. CLEAN. RIGHT.

33
00:08:02,014 --> 00:08:04,016
IT'�S WORK TIME.
COME ON.

34
00:08:08,137 --> 00:08:09,639
WHERE IS HE?

35
00:08:17,146 --> 00:08:19,148
GOOD. DOG OUT.

36
00:08:31,661 --> 00:08:32,662
GOOD.

37
00:08:42,555 --> 00:08:44,056
HELP WHAT?

38
00:08:45,675 --> 00:08:46,676
HELP FLY?

39
00:08:50,680 --> 00:08:52,064
YOU MEAN THIS?

40
00:09:27,967 --> 00:09:29,218
HEY, ENOUGH.

41
00:09:37,226 --> 00:09:40,730
HOW ABOUT IF I WRITE
YOUR NAME ON IT?

42
00:09:42,231 --> 00:09:44,734
THEN IT'�LL BE
VIRGIL'�S ALLIGATOR.

43
00:09:48,237 --> 00:09:51,240
NO. NO.
IT'�S NOT PLAY TIME.

44
00:10:30,913 --> 00:10:31,914
GOOD, GOOD!

45
00:10:34,417 --> 00:10:36,419
DR. CRISWELL.

46
00:10:55,054 --> 00:10:57,056
WAIT A MINUTE.

47
00:11:10,569 --> 00:11:11,570
IT'�S OK.

48
00:11:56,499 --> 00:11:58,000
I'�LL BUY HIM.

49
00:12:19,021 --> 00:12:21,023
HE WON'�T UNDERSTAND
WHAT'�S HAPPENING.

50
00:12:30,032 --> 00:12:34,036
HE'�LL BE WELL CARED
FOR...AND WELL LOVED.

51
00:14:07,079 --> 00:14:09,081
YOU'�RE
IN THE AIR FORCE NOW.

52
00:14:19,842 --> 00:14:21,844
[CHITTERING]

53
00:15:27,910 --> 00:15:30,412
GARRETT, TIME
TO SEE THE COLONEL.

54
00:15:59,575 --> 00:16:01,577
IN OTHER WORDS,
YOU DID IT TO GET LAID.

55
00:16:19,845 --> 00:16:21,347
CONCENTRATING
ON FOOTBALL.

56
00:16:34,610 --> 00:16:37,112
I RAN
A BETTING POOL.

57
00:16:47,623 --> 00:16:50,125
UNTIL YOU CAN LEARN
TO OBEY ORDERS.

58
00:16:51,744 --> 00:16:53,746
"EXPERIMENTAL PILOT
PERFORMANCE PROJECT."

59
00:16:57,132 --> 00:16:58,634
NO, SIR.

60
00:17:21,690 --> 00:17:23,692
[KNOCK ON DOOR]

61
00:17:28,197 --> 00:17:31,200
HOW DO YOU DO?
I'�M DR. CARROLL.

62
00:17:35,704 --> 00:17:37,206
YES, SIR.

63
00:18:02,364 --> 00:18:05,367
AND I HOPE THAT I CAN
BENEFIT THE PROGRAM.

64
00:18:10,739 --> 00:18:12,241
GOOD.

65
00:18:35,514 --> 00:18:37,516
NOT YOU. THEM.

66
00:19:05,177 --> 00:19:07,179
NUMBER 5,
RESTABILIZE YOUR APPROACH.

67
00:19:32,321 --> 00:19:33,822
THAT'�S RIGHT.

68
00:20:13,862 --> 00:20:15,364
LIKE EVERYBODY ELSE.
THANK YOU.

69
00:20:22,871 --> 00:20:25,874
[LOW-FLYING AIRCRAFT]

70
00:20:49,398 --> 00:20:51,400
HI THERE, FELLA.

71
00:20:52,651 --> 00:20:54,152
GRR!

72
00:20:58,040 --> 00:20:59,541
OK, LET'�S GO.

73
00:21:27,319 --> 00:21:29,321
WITH THIS LITTLE FELLA HERE.

74
00:21:30,822 --> 00:21:33,325
HE LOOKS JUST AS SCARED
AS YOU DO.

75
00:21:38,330 --> 00:21:39,331
OK.

76
00:21:44,586 --> 00:21:46,588
HOW YOU DOIN'�?

77
00:21:48,090 --> 00:21:51,093
UH, NO FUNNY STUFF, OK?

78
00:22:08,860 --> 00:22:10,862
BE A GOOD
LITTLE CHIMP.

79
00:22:12,364 --> 00:22:13,365
COME HERE.

80
00:22:19,371 --> 00:22:19,871
SHIT.

81
00:22:21,873 --> 00:22:22,874
HEY.

82
00:22:24,876 --> 00:22:26,878
[CLAPPING]

83
00:22:37,889 --> 00:22:39,141
GOOD BOY.

84
00:22:58,326 --> 00:23:00,328
PULL UP,
PULL UP.

85
00:23:11,840 --> 00:23:13,341
GIVE HIM A RAISIN.

86
00:23:53,248 --> 00:23:54,749
NOW GO UP.
GO UP.

87
00:23:56,251 --> 00:23:57,252
COME ON. UP.

88
00:24:07,262 --> 00:24:09,264
PULL BACK. PULL BACK.
THAT'�S GOOD.

89
00:24:28,783 --> 00:24:30,785
I'�VE NEVER
SEEN THIS BEFORE.

90
00:24:42,180 --> 00:24:43,682
UH...YOU GOT ME.

91
00:25:11,326 --> 00:25:12,827
I'�M SORRY.

92
00:25:29,344 --> 00:25:31,346
TURN YOUR HEAD
THIS WAY.

93
00:25:57,372 --> 00:25:58,373
PFFT! PFTT!

94
00:26:02,260 --> 00:26:03,261
PFFT!

95
00:26:28,286 --> 00:26:30,288
WHERE ARE YOU
TAKIN'� HIM?

96
00:26:51,059 --> 00:26:54,062
WHO MADE YOU
LORD OF THE APES?

97
00:26:56,564 --> 00:26:57,565
COME ON.

98
00:27:11,579 --> 00:27:13,698
DON'�T ASK,
"WHAT IS IT?"

99
00:27:44,863 --> 00:27:46,865
CAN I SEE
YOUR CIGARETTE?

100
00:28:15,643 --> 00:28:16,644
GOOD NIGHT, MAN.

101
00:28:26,020 --> 00:28:28,022
VIRGIL, HUH?

102
00:28:30,024 --> 00:28:32,026
YOU DON'�T LOOK
LIKE A VIRGIL.

103
00:28:33,528 --> 00:28:35,530
I THINK
THIS IS YOURS.

104
00:28:54,549 --> 00:28:56,551
SEE YOU LATER,
ALLIGATOR.

105
00:29:49,654 --> 00:29:54,659
HELPING PEOPLE HELP
EACH OTHER--THE UNITED WAY.

106
00:30:05,420 --> 00:30:06,421
HELP.

107
00:30:26,057 --> 00:30:27,559
HI.

108
00:30:30,311 --> 00:30:31,312
HELP.

109
00:30:33,948 --> 00:30:35,450
I DON'�T BELIEVE IT.

110
00:30:38,453 --> 00:30:39,454
HELP.

111
00:30:50,582 --> 00:30:51,583
OH, OUT. HELP OUT.

112
00:30:53,718 --> 00:30:54,719
YOU WANT TO GET OUT.

113
00:31:07,599 --> 00:31:09,601
HEY.

114
00:31:26,501 --> 00:31:28,503
NEW GIRL
IN THE NEIGHBORHOOD.

115
00:31:36,010 --> 00:31:38,012
I SHOULDN'�T EVEN
HAVE LETYOUOUT.

116
00:31:41,015 --> 00:31:43,017
WHAT IS THAT?

117
00:31:47,522 --> 00:31:49,524
FOOD.
THAT I CAN DO.

118
00:31:59,150 --> 00:32:00,151
HERE.

119
00:32:05,156 --> 00:32:06,658
YOU DON'�T WANT IT?

120
00:32:09,661 --> 00:32:10,662
WHAT?

121
00:32:14,165 --> 00:32:16,167
YEAH?
IT'�S A REFRIGERATOR.

122
00:32:26,561 --> 00:32:28,563
THAT'�S THE SIGN
FOR APPLE?

123
00:32:33,568 --> 00:32:34,569
WHAT?

124
00:32:39,824 --> 00:32:41,826
YOU WANT THIS?

125
00:32:45,580 --> 00:32:48,082
YOU WANT THE BRUSH?
OK. HERE.

126
00:33:29,490 --> 00:33:34,996
[LOW-FLYING AIRCRAFT]

127
00:33:54,148 --> 00:33:56,150
TIME TO GO TO BED.

128
00:34:06,277 --> 00:34:09,664
YEAH. FRIENDS.

129
00:34:11,282 --> 00:34:13,785
GOOD NIGHT, VIRGIL.

130
00:34:38,810 --> 00:34:41,312
AHH...AHH...

131
00:34:54,826 --> 00:34:57,328
"THE LEFT INDEX
TURNING SLIGHTLY.

132
00:35:30,862 --> 00:35:33,865
TO COMMUNICATE WITH ME.

133
00:35:38,619 --> 00:35:42,623
NO, VIRGIL, YOU GAVE
THE APPLE TO YOUR GIRLFRIEND.

134
00:35:54,135 --> 00:35:57,638
OK, FOOD.
COME ON, FOOD.

135
00:36:39,313 --> 00:36:41,816
NO, WE DON'�T.
THAT'�S ABSOLUTELY RIGHT.

136
00:37:17,602 --> 00:37:20,605
[LOUD CHATTERING]

137
00:37:22,607 --> 00:37:24,108
!
IT'�S YOUR FAULT

138
00:37:34,118 --> 00:37:36,120
THAT'�S RIGHT.
THAT'�S GOOD.

139
00:37:58,643 --> 00:37:59,644
PFFT PFFT!

140
00:38:18,663 --> 00:38:20,164
ETHEL.

141
00:38:26,787 --> 00:38:28,789
YEAH, BLUEBEARD.

142
00:38:31,292 --> 00:38:33,294
LOOKS LIKE YOU'�RE
JUST ABOUT READY, PAL.

143
00:38:42,937 --> 00:38:44,939
OH, FINE, SIR.

144
00:39:20,725 --> 00:39:21,726
GOOD.

145
00:39:34,855 --> 00:39:38,859
HEY.
OK. STOP FLYING.

146
00:39:44,865 --> 00:39:46,867
GOOD.

147
00:39:57,011 --> 00:40:00,514
HEY, CHIMP YEAGER,
GIVE ME FIVE.

148
00:40:05,519 --> 00:40:07,521
ENJOY IT.

149
00:40:13,027 --> 00:40:14,528
YOU DON'�T WANT IT?

150
00:40:25,656 --> 00:40:29,410
OUT. HURRY.

151
00:40:32,913 --> 00:40:34,415
OH.

152
00:40:39,420 --> 00:40:41,922
OOH OOH OOH!

153
00:40:58,439 --> 00:41:01,442
OK. BIG TIME.

154
00:41:07,448 --> 00:41:08,949
DON'�T GO TOO FAR.

155
00:41:58,299 --> 00:42:02,303
GARRETT, I NEED TO SPEAK
TO YOU FOR A MINUTE.

156
00:42:40,341 --> 00:42:41,842
OH, THAT'�S GREAT.

157
00:42:51,852 --> 00:42:53,354
KEEP UP THE GOOD WORK,
JIMMY.

158
00:42:56,106 --> 00:42:57,608
HEAR THAT?
WE'�RE PROMOTED.

159
00:43:22,633 --> 00:43:24,134
I DON'�T KNOW.

160
00:43:27,137 --> 00:43:29,139
OK, BLUEBEARD.

161
00:43:37,147 --> 00:43:39,149
OK. CHIN UP.

162
00:43:40,651 --> 00:43:42,152
GOOD BOY.

163
00:43:57,918 --> 00:44:00,421
HERE, GIVE ME
YOUR HAND.

164
00:44:39,460 --> 00:44:41,829
GARRETT, GO ON IN.

165
00:44:51,588 --> 00:44:54,591
TRAINER, CHECK YOUR PILOT IN
AT THE AIRCRAFT.

166
00:45:06,603 --> 00:45:08,105
GO ON.

167
00:46:20,394 --> 00:46:22,896
TRAINER, EVACUATE CHAMBER.

168
00:46:24,398 --> 00:46:27,901
TRAINER!
EVACUATE CHAMBER.

169
00:46:46,787 --> 00:46:48,789
HERE YOU GO.
PUT THESE ON.

170
00:47:17,568 --> 00:47:19,570
ROGER.
COMMENCING.

171
00:47:49,349 --> 00:47:50,984
SYSTEM LOADED.

172
00:48:04,748 --> 00:48:06,250
OPEN.

173
00:49:14,184 --> 00:49:18,572
RECEIVED MIDCRANIAL DOSE
OF 1,200 REMS.

174
00:49:37,841 --> 00:49:38,709
YO.

175
00:49:40,594 --> 00:49:42,846
LET'�S SEE THAT
LITTLE THING THERE.

176
00:49:48,852 --> 00:49:49,853
DON'�T WORRY,
YOU'�RE CLEAN.

177
00:50:00,981 --> 00:50:02,482
YUP.

178
00:50:58,038 --> 00:51:01,041
[LOW-FLYING AIRCRAFT]

179
00:51:17,941 --> 00:51:20,444
IN CASE WORLD WAR III
HAPPENS.

180
00:51:32,456 --> 00:51:33,957
IT'�S THE AIR FORCE.

181
00:51:54,978 --> 00:51:57,481
THAT'�S WHAT YOU
GOT TO DO.

182
00:52:15,499 --> 00:52:17,501
YOU'�LL FORGET ALL ABOUT
THIS PLACE, MAN.

183
00:52:23,006 --> 00:52:26,510
[LOW-FLYING AIRCRAFT]

184
00:52:58,542 --> 00:53:01,545
[WATER SPLASHES]

185
00:53:13,056 --> 00:53:14,558
GOOD NIGHT, JIMMY.

186
00:53:16,059 --> 00:53:17,310
GOOD NIGHT.

187
00:53:26,319 --> 00:53:27,320
YOU UNDERSTAND?

188
00:53:34,327 --> 00:53:37,831
VIRGIL. I SAID
COME BACK HERE.

189
00:53:39,332 --> 00:53:41,201
GIVE ME YOUR HAND.

190
00:53:45,705 --> 00:53:48,208
OK, GET ON THE SCALE.

191
00:53:54,214 --> 00:53:57,217
WOULD YOU GET
ON THE GODDAMN SCALE?!

192
00:54:10,230 --> 00:54:11,731
GET ON THE SCALE!

193
00:54:20,240 --> 00:54:23,743
GIVE ME THAT.
GIVE ME THAT!

194
00:54:25,245 --> 00:54:26,746
GODDAMN IT, VIRGIL!

195
00:55:27,190 --> 00:55:29,192
COME HERE.

196
00:56:04,344 --> 00:56:09,349
AAAAAAAAHHHH!

197
00:56:26,616 --> 00:56:29,252
AAAAAAAAHHHH!

198
00:56:31,621 --> 00:56:35,125
[ALL HOWLING]

199
00:56:52,525 --> 00:56:55,145
[ALL HOWLING]

200
00:57:04,954 --> 00:57:07,957
[TELEPHONE RINGS]

201
00:57:17,467 --> 00:57:20,470
BUT WE HAVE A FRIEND
IN COMMON--VIRGIL.

202
00:57:56,956 --> 00:57:58,958
NO, WAIT.

203
00:58:21,564 --> 00:58:22,565
OK.

204
00:58:51,594 --> 00:58:54,597
BLUE, JUNIORS,
AND RED, SENIORS...

205
00:59:11,114 --> 00:59:12,615
SIT DOWN.

206
01:00:10,173 --> 01:00:12,175
AND THE PAIN
WILL GO AWAY.

207
01:00:35,198 --> 01:00:37,700
WE'�LL GET YOU
UP IN THE AIR AGAIN SOON.

208
01:01:02,108 --> 01:01:04,110
WE'�VE, UH, GOT
SENATOR DANNIS COMING.

209
01:01:12,118 --> 01:01:13,870
? YOU BABY YOU,
BABY YOU... ???

210
01:01:21,244 --> 01:01:22,745
THANKS, JIMMY.

211
01:02:18,801 --> 01:02:19,802
EXCUSE ME.

212
01:02:21,938 --> 01:02:24,941
COULD I
BUY YOU A DRINK?

213
01:02:34,817 --> 01:02:36,319
TWO BEERS.

214
01:03:01,844 --> 01:03:02,845
YOUR NAME'�S JIMMY?

215
01:03:07,350 --> 01:03:09,852
YOU DID CALL ME,
RIGHT?

216
01:04:12,415 --> 01:04:15,418
I DON'�T MEAN TO BE RUDE,
SO, UH, EXCUSE ME.

217
01:05:20,983 --> 01:05:22,985
SHE GOT MARRIED,
YEAH. [CHUCKLES]

218
01:05:27,623 --> 01:05:29,625
GO, GO, GO ON.

219
01:05:57,019 --> 01:06:01,023
[WHIMPERING]

220
01:06:03,659 --> 01:06:05,661
AAAH! AH!

221
01:06:23,179 --> 01:06:24,180
GARRETT!

222
01:07:21,237 --> 01:07:22,738
[CONTINUES, INDISTINCT]

223
01:07:43,008 --> 01:07:45,010
[CONTINUES, INDISTINCT]

224
01:07:49,882 --> 01:07:51,884
COME ON, BUDDY.

225
01:07:53,385 --> 01:07:54,386
LET'�S GO.

226
01:08:01,527 --> 01:08:02,528
[WHIMPERING]

227
01:08:04,396 --> 01:08:06,899
AAAAH! AAH AAH!

228
01:08:08,400 --> 01:08:11,904
AH! AH! AH! AH!

229
01:08:15,407 --> 01:08:18,911
[WHIMPERING]

230
01:10:14,026 --> 01:10:15,527
HA HA HA HA!

231
01:10:57,319 --> 01:10:58,203
ROGER.

232
01:11:27,599 --> 01:11:29,101
OF 3,500 RADS...

233
01:11:33,238 --> 01:11:35,741
IF HE TOOK OFF
WITHIN 2.5 MILES
OF GROUND ZERO.

234
01:11:50,756 --> 01:11:54,259
WHY DOES THIS CHIMP
HAVE TO BE IRRADIATED?

235
01:13:22,347 --> 01:13:23,849
INITIATING
LANDING PATTERN.

236
01:13:46,989 --> 01:13:48,624
YOU DON'�T HAVE
ENOUGH TIME.

237
01:13:51,994 --> 01:13:53,996
GET HIM OUT
OF HERE, TONIGHT.

238
01:14:12,598 --> 01:14:14,233
UHH! UHH!

239
01:14:19,104 --> 01:14:21,490
OOH OOH OOH!

240
01:14:43,595 --> 01:14:45,597
WE'�LL COME OUT
THAT DOOR THERE.

241
01:14:51,220 --> 01:14:54,189
HEY, DON'�T WORRY ABOUT IT.
HE'�LL MAKE IT OVER.

242
01:14:57,693 --> 01:14:58,694
BYE.

243
01:15:35,847 --> 01:15:37,349
WATCH THE GAME.

244
01:15:53,749 --> 01:15:56,752
MISS? COME WITH ME,
PLEASE?

245
01:16:01,256 --> 01:16:03,508
THE BLUE DEVILS
ARE MORE EXPERIENCED...

246
01:16:15,020 --> 01:16:18,390
[ALARM]

247
01:16:21,643 --> 01:16:26,648
[MONKEYS CHITTERING]

248
01:16:52,424 --> 01:16:53,925
WHOO!

249
01:17:20,452 --> 01:17:21,453
AAAH!

250
01:17:31,963 --> 01:17:34,216
AAAAH!

251
01:17:39,971 --> 01:17:40,856
[SIZZLE]

252
01:17:58,740 --> 01:17:59,741
AAAH! AAH!

253
01:18:04,746 --> 01:18:05,747
AAAAAH!

254
01:18:22,147 --> 01:18:25,267
AAH! AAH!
AAH! AAH! AAH!

255
01:18:55,180 --> 01:18:58,183
GET THE DOOR!
SOMEBODY, GET THAT DOOR OPEN!

256
01:19:00,685 --> 01:19:02,187
LOOK LIVELY, EVERYBODY.

257
01:19:10,562 --> 01:19:13,515
WELL, WAKE HIM UP!
THIS IS DR. CARROLL.

258
01:19:23,492 --> 01:19:25,494
VIRGIL. VIRGIL.

259
01:19:26,495 --> 01:19:27,496
VIRGIL.

260
01:19:50,652 --> 01:19:52,154
IT'�S ME.

261
01:19:53,522 --> 01:19:54,406
COME ON.

262
01:20:11,423 --> 01:20:13,425
AHH AHH!

263
01:20:16,795 --> 01:20:18,296
PPFFT PPFFT!

264
01:20:21,683 --> 01:20:22,184
PPFFT!

265
01:20:28,306 --> 01:20:29,307
HOO! HOO!

266
01:20:37,065 --> 01:20:38,567
AAAAH!

267
01:21:19,241 --> 01:21:21,243
THERE ARE MONKEYS
ALL OVER THE PLACE, SIR.

268
01:21:54,526 --> 01:21:56,027
DAMN.

269
01:23:17,225 --> 01:23:18,226
OH, SHIT.

270
01:23:33,241 --> 01:23:35,744
[BEEPING]

271
01:23:59,150 --> 01:23:59,651
SPIKE, GET DOWN.

272
01:24:00,902 --> 01:24:04,272
SPIKE, PUT IT--
GET OFF, SPIKE.

273
01:24:05,523 --> 01:24:06,908
GET GOIN'�.
GET OUTTA HERE.

274
01:24:37,939 --> 01:24:39,941
GARRETT, GET OUT
OF THERE, MAN.

275
01:24:53,822 --> 01:24:54,572
2...

276
01:25:19,848 --> 01:25:21,349
NO CONTAMINATION
OUTSIDE.

277
01:25:35,830 --> 01:25:37,332
YEAH.

278
01:26:02,857 --> 01:26:04,726
UNTIL WHAT?

279
01:26:09,864 --> 01:26:11,366
GARRETT!

280
01:26:32,503 --> 01:26:35,506
GARRETT, YOU UNDERSTAND
WHAT I'�M SAYING?

281
01:27:49,464 --> 01:27:50,465
JIMMY.

282
01:28:36,010 --> 01:28:37,011
THAT'�S GOOD.
GOOD.

283
01:28:42,767 --> 01:28:44,268
OHHH...

284
01:28:47,271 --> 01:28:48,773
HOW ARE WE?

285
01:28:52,276 --> 01:28:55,279
NO RADIATION LEAKAGE
OUTSIDE THE FLIGHT CHAMBER.

286
01:29:05,540 --> 01:29:06,541
GOOD.

287
01:29:11,162 --> 01:29:12,663
GOOD, GOLIATH.

288
01:29:14,549 --> 01:29:16,050
THAT'�S GOOD.

289
01:29:19,420 --> 01:29:20,421
GOOD.

290
01:29:25,176 --> 01:29:27,678
THANK YOU, GOLIATH.

291
01:30:04,849 --> 01:30:06,851
ARE YOU BOTH OK?

292
01:30:17,979 --> 01:30:19,981
AND HELP GET THE OTHERS
BACK IN THEIR CAGES.

293
01:30:31,242 --> 01:30:32,743
HE NEVER SHOULD HAVE
BEEN BROUGHT HERE.

294
01:30:34,745 --> 01:30:36,247
SHE SHOULD
TAKE HIM HOME.

295
01:30:45,256 --> 01:30:47,258
IS THAT CLEAR?

296
01:30:54,765 --> 01:30:55,766
THAT'�S AN ORDER.

297
01:31:08,779 --> 01:31:10,281
YES, SIR.

298
01:31:15,786 --> 01:31:18,789
GO GET YOUR ANIMALS
BACK IN THEIR CAGES.

299
01:31:54,458 --> 01:31:56,961
COME ON.

300
01:32:00,715 --> 01:32:01,716
EASY.

301
01:32:10,725 --> 01:32:12,226
MMM MMM MMM!

302
01:32:14,228 --> 01:32:15,229
MMM! MMM!

303
01:32:35,116 --> 01:32:36,117
Teri: COME ON.

304
01:32:38,502 --> 01:32:40,004
UUH!

305
01:32:43,007 --> 01:32:44,508
COME ON.

306
01:32:46,010 --> 01:32:47,511
COME ON.

307
01:32:57,888 --> 01:32:59,390
[SIREN]

308
01:33:08,282 --> 01:33:09,784
COME ON.

309
01:33:18,909 --> 01:33:20,911
COME ON, GET
IN THERE, TROOPS.

310
01:33:28,919 --> 01:33:31,422
DON'�T WORRY.
I DO IT ALL THE TIME.

311
01:33:35,426 --> 01:33:38,429
[SIREN]

312
01:33:47,321 --> 01:33:48,322
FAR AS WE CAN GET.

313
01:33:53,327 --> 01:33:55,329
YOU'�RE MY COPILOT,
BUDDY.

314
01:34:13,214 --> 01:34:15,216
Teri: COME ON, GUYS!

315
01:34:20,354 --> 01:34:21,355
COME ON.
COME ON.

316
01:34:25,976 --> 01:34:26,977
THAT'�S IT, COME ON.

317
01:34:58,893 --> 01:34:59,894
WHOA.

318
01:35:19,530 --> 01:35:21,031
WITH YOUR HANDS
IN THE AIR.

319
01:35:30,541 --> 01:35:32,543
EXIT THE AIRCRAFT.

320
01:35:35,045 --> 01:35:37,047
LET'�S GO, HONEY.

321
01:35:40,184 --> 01:35:41,685
Man: COME ON,
LET'�S GO.

322
01:35:57,701 --> 01:35:58,702
NOW!

323
01:36:27,231 --> 01:36:28,732
OF THE ALL-CLEAR.
OVER.

324
01:36:30,234 --> 01:36:32,336
ROGER. COPY, 15.
HOLDING POSITION.

325
01:36:46,834 --> 01:36:47,835
WHY NOT?

326
01:37:02,550 --> 01:37:04,051
STANDING BY
FOR CONFIRMATION.

327
01:37:56,987 --> 01:37:58,989
HOLD SHORT. YOU ARE NOT
CLEARED FOR TAKEOFF.

328
01:38:22,630 --> 01:38:24,131
YOU ARE NOT CLEARED
FOR TAKEOFF. OVER.

329
01:38:28,135 --> 01:38:31,138
HOLD SHORT! YOU'�RE NOT
CLEARED FOR TAKEOFF! OVER!

330
01:38:34,642 --> 01:38:36,277
YOU BETTER ANSWER ME!

331
01:38:59,550 --> 01:39:01,051
GO, VIRGIL!

332
01:39:04,555 --> 01:39:05,055
YEAH!

333
01:39:21,939 --> 01:39:23,440
HEADING SOUTHBOUND
TOWARDS EVERGLADES. OVER.

334
01:39:29,697 --> 01:39:32,666
I'�LL CATCH '�EM
AT THE OUTER MARKER. OVER.

335
01:40:14,958 --> 01:40:16,460
HE'�S GOIN'� IN.

336
01:40:22,966 --> 01:40:24,952
DOWN IN THE EVERGLADES,
145 DEGREES AND...

337
01:41:11,131 --> 01:41:11,632
YES, SIR.

338
01:41:19,389 --> 01:41:20,891
AS CONFIDENTIAL...

339
01:41:33,403 --> 01:41:34,905
MMM MMM!

340
01:41:40,911 --> 01:41:42,913
MMM OOH OOH
UH UH...

341
01:41:44,915 --> 01:41:46,917
MMM MMM OOH OOH...

342
01:41:48,802 --> 01:41:51,305
CAN I SAY
SOMETHING TO HER?

343
01:41:53,307 --> 01:41:55,309
MAKE IT QUICK.

344
01:42:01,064 --> 01:42:02,566
LISTEN.

345
01:42:15,579 --> 01:42:16,580
AAH AAH AAAH...

346
01:42:23,587 --> 01:42:25,088
[WHIMPERING]

347
01:42:30,594 --> 01:42:32,596
WHAT'�S THE SIGN
FOR FREE?

348
01:42:38,101 --> 01:42:39,603
FREE.

349
01:42:45,108 --> 01:46:49,102
GO ON.

