1
00:00:32,873 --> 00:00:34,634
In lane 7,

2
00:00:34,744 --> 00:00:38,205
Suzuki, of Tadano
Boys High School.

3
00:00:38,545 --> 00:00:43,456
In lane 8, Tsuchiya
of Kitahara Academy.

4
00:00:48,398 --> 00:00:49,488
On your mark!

5
00:01:18,438 --> 00:01:19,398
No way!

6
00:01:20,569 --> 00:01:22,159
Hurry up and get out!

7
00:01:22,269 --> 00:01:23,139
Huh?

8
00:02:51,959 --> 00:02:54,389
The next routine.
It has already started!

9
00:03:47,667 --> 00:03:52,628
WATERBOYS

10
00:04:32,292 --> 00:04:34,922
Suzuki

11
00:04:57,220 --> 00:04:59,190
You monster!

12
00:05:06,073 --> 00:05:10,834
See for yourself, our swim team's so
pathetic, it's been this way awhile.

13
00:05:11,574 --> 00:05:16,206
We're down to one last senior,
a wimp named Suzuki.

14
00:05:16,586 --> 00:05:18,557
But it's time for him to quit.

15
00:05:19,417 --> 00:05:22,648
If nobody new comes along,
we won't have a swim team.

16
00:05:31,941 --> 00:05:34,672
Good morning.
I'm Sakuma Megumi.

17
00:05:34,872 --> 00:05:38,403
I'll be joining the staff today.

18
00:05:38,943 --> 00:05:42,445
I'm a newcomer here, and I look
forward to your cooperation.

19
00:05:42,585 --> 00:05:46,856
I know Tadano High is really
serious about sports.

20
00:05:48,026 --> 00:05:49,387
My own...

21
00:05:50,457 --> 00:05:52,518
My own training...

22
00:05:52,768 --> 00:05:53,658
Excuse me.

23
00:05:54,128 --> 00:05:57,069
My own training is that
I swam through college,

24
00:05:57,169 --> 00:06:00,110
so I'll be coaching the swim team.

25
00:06:00,210 --> 00:06:03,701
Unfortunately, it's a one-man
show right now,

26
00:06:03,812 --> 00:06:06,342
so if any of you are interested...

27
00:06:21,737 --> 00:06:26,269
Suzuki! Eliminated in the preliminaries?
Weren't you gonna quit?

28
00:06:26,379 --> 00:06:30,250
Shut up, you just got hurt and
quit basketball, right?

29
00:06:30,350 --> 00:06:34,882
I can't play in games anymore,
so of course I chose swimming.

30
00:06:39,963 --> 00:06:43,054
Ikeuchi from Karate,
Sakamoto from Gymnastics.

31
00:06:43,194 --> 00:06:45,635
Even Mochizuki from Track.

32
00:06:46,235 --> 00:06:50,677
All quitters with
nothing better to do.

33
00:06:52,047 --> 00:06:56,209
And that skinny nerd is Ota Yuichi
Guess why he signed up...

34
00:07:02,661 --> 00:07:06,592
And the kid drowning over there...
is Kanazawa Takashi.

35
00:07:07,062 --> 00:07:09,003
Just decided to learn how to swim!

36
00:07:11,234 --> 00:07:15,505
And that's Saotome.
Hey, he's got the hots for you.

37
00:07:15,605 --> 00:07:17,666
Cut it out, it makes me sick.

38
00:07:18,076 --> 00:07:20,407
We're really scraping
the bottom of the barrel, huh?

39
00:07:20,517 --> 00:07:23,448
Sniffing around like hyenas.

40
00:07:24,018 --> 00:07:27,509
What the hell!
I heard that!

41
00:07:32,000 --> 00:07:35,962
Wow, look how many
of you turned up.

42
00:07:36,502 --> 00:07:38,202
Her skin's so delicate.

43
00:07:40,243 --> 00:07:42,004
She's damn cute.

44
00:07:44,044 --> 00:07:45,915
Aren't you going to get in?

45
00:07:46,015 --> 00:07:47,075
Show us something.

46
00:07:47,185 --> 00:07:48,546
What's your favorite stroke?

47
00:07:48,656 --> 00:07:49,846
Let me rub your back.

48
00:07:49,956 --> 00:07:52,757
One thing we do
have is muscles!

49
00:07:53,227 --> 00:07:55,018
Yes... but...

50
00:07:55,428 --> 00:07:57,089
But what but?

51
00:07:58,129 --> 00:08:01,120
You wouldn't be interested.

52
00:08:01,600 --> 00:08:05,061
Because I...

53
00:08:06,972 --> 00:08:09,243
Miss Sakuma san!

54
00:08:12,084 --> 00:08:13,514
What's wrong?

55
00:08:17,325 --> 00:08:19,486
What'd they do to you?

56
00:08:19,696 --> 00:08:22,597
I wouldn't get
too near those guys.

57
00:08:24,498 --> 00:08:26,228
I'm sorry.

58
00:08:26,998 --> 00:08:31,810
I knew it, I just can't
teach at this school.

59
00:08:31,910 --> 00:08:33,271
Why not?

60
00:08:35,111 --> 00:08:36,602
I didn't...

61
00:08:37,352 --> 00:08:40,543
know they were sending me
to an all-boys school.

62
00:08:41,023 --> 00:08:44,184
Don't say that.
Girls, boys, who cares.

63
00:08:44,294 --> 00:08:48,596
That's right, miss!
We're all beginners here anyway.

64
00:08:48,696 --> 00:08:52,067
Tell us anything.
You can count on us.

65
00:08:52,307 --> 00:08:54,267
We want you to teach us, right?

66
00:08:54,367 --> 00:08:55,138
Yeah!

67
00:08:55,808 --> 00:08:57,138
Really?

68
00:09:07,922 --> 00:09:11,983
Real synchronized swimmers perform
in deep pools, but we'll manage.

69
00:09:12,293 --> 00:09:15,925
I can't believe I got
stuck on that tiny detail.

70
00:09:16,065 --> 00:09:19,036
All that chlorine must
have gone to my brain.

71
00:09:19,136 --> 00:09:23,237
The truth is, I got super depressed
when I heard this was a boys school.

72
00:09:23,347 --> 00:09:26,478
I've always dreamed of
coaching my own team.

73
00:09:30,519 --> 00:09:35,021
Why can't boys do it?
OK, I've made up my mind.

74
00:09:35,131 --> 00:09:38,322
Let's all go for it.
Synchro swim!

75
00:09:46,344 --> 00:09:49,005
Man, can you believe
the mess we're in?

76
00:09:49,115 --> 00:09:50,666
Don't start!

77
00:09:50,776 --> 00:09:53,337
I just wanted regular swimming...

78
00:09:53,447 --> 00:09:56,648
Liar, you don't care
if you swim or not.

79
00:09:56,758 --> 00:09:59,449
I do too! I have to
learn to stay afloat.

80
00:09:59,559 --> 00:10:03,120
Wow, scary! What are you
so pissed about, you geek!

81
00:10:03,230 --> 00:10:04,460
Don't call me a geek!

82
00:10:04,570 --> 00:10:06,261
Hey, stop it!

83
00:10:07,942 --> 00:10:09,272
Wait.

84
00:10:09,472 --> 00:10:11,963
How come you want to swim so bad?

85
00:10:12,143 --> 00:10:15,374
Because the math doesn't work.

86
00:10:15,644 --> 00:10:18,945
The relative human weight is less
than the volume of water,

87
00:10:19,055 --> 00:10:20,886
which has to create buoyancy.
Besides,

88
00:10:20,986 --> 00:10:25,387
the logic of fluid mechanics suggests
stroking water will propel one forward...

89
00:10:25,497 --> 00:10:27,728
And a 1, 2, 3, 4

90
00:10:27,828 --> 00:10:30,159
5, 6, 7, 8

91
00:10:30,269 --> 00:10:32,260
2, 2, 3, 4

92
00:10:32,900 --> 00:10:35,501
Those fools are
really gonna do it.

93
00:10:36,271 --> 00:10:38,211
Bunch of idiots!

94
00:10:42,213 --> 00:10:45,944
Synchro Swim Team, yes.

95
00:10:47,755 --> 00:10:51,626
Yes, yes, my Synchro
Swim Team.

96
00:10:51,766 --> 00:10:54,667
And a 1, 2, 3, 4

97
00:10:54,767 --> 00:10:57,968
5, 6, 7, 8

98
00:11:02,109 --> 00:11:04,080
Hey, Pavlov! Quiet.

99
00:11:15,664 --> 00:11:16,624
What's that?

100
00:11:16,724 --> 00:11:19,215
We premier at the
Tadano High School Festival.

101
00:11:19,335 --> 00:11:20,355
What!

102
00:11:20,465 --> 00:11:23,626
If we're going to practice,
might as well have an audience.

103
00:11:23,736 --> 00:11:26,637
I've left the date open
intentionally.

104
00:11:26,737 --> 00:11:30,609
And those letters should
be bolder, brighter.

105
00:11:30,709 --> 00:11:33,480
But leave the details for later.

106
00:11:33,580 --> 00:11:37,081
All right, here we go!

107
00:11:37,921 --> 00:11:41,522
Yup, we can do plenty
even with just 5 of you.

108
00:11:41,632 --> 00:11:44,923
I'm so happy I could just...

109
00:11:45,033 --> 00:11:48,054
My heart is pounding.

110
00:11:48,575 --> 00:11:49,535
I'm... ill...

111
00:11:49,635 --> 00:11:51,506
Must be the thrill.

112
00:12:02,619 --> 00:12:05,150
I mean, I thought
my period was late...

113
00:12:05,260 --> 00:12:08,521
And my stomach,
I thought I was bloated...

114
00:12:08,631 --> 00:12:12,432
Never dreamed I was 8 months!

115
00:12:16,774 --> 00:12:18,744
Oh, this is my husband.

116
00:12:21,445 --> 00:12:22,576
Hello.

117
00:12:23,886 --> 00:12:28,548
I'm so sorry, but I have
to take a little maternity leave.

118
00:12:28,658 --> 00:12:31,149
You've got to ace
that Tadano Fest, OK?

119
00:12:39,671 --> 00:12:43,202
Our annual Tadano Fest
is on the second weekend

120
00:12:43,313 --> 00:12:45,503
after summer vacation.

121
00:12:46,684 --> 00:12:50,985
It's the only time we ever
get girls from Sakuragi

122
00:12:51,085 --> 00:12:55,286
and the other all-girls
high schools to visit,

123
00:12:55,396 --> 00:12:58,187
so you guys better come up
with good ideas.

124
00:12:58,598 --> 00:13:02,659
This year, we want to nab
300 Sakuragi girls!

125
00:13:07,941 --> 00:13:10,972
Let's see...
Early on, the swim team

126
00:13:11,182 --> 00:13:14,383
put in a bid to perform
their synchronized swim.

127
00:13:14,483 --> 00:13:17,384
If you're still good for it,
I'll put it on the program.

128
00:13:17,484 --> 00:13:20,925
The basketball team gets the
pool for fly fishing.

129
00:13:21,155 --> 00:13:22,315
But, um...

130
00:13:24,626 --> 00:13:26,997
It's too late now, right?

131
00:13:27,637 --> 00:13:28,657
Late for what?

132
00:13:28,967 --> 00:13:32,909
We're stuck with the
synchronized thing, right?

133
00:13:33,009 --> 00:13:35,480
No, not at all.
The program's totally open.

134
00:13:35,650 --> 00:13:39,411
Oh, really?
In that case, we'll bow out.

135
00:13:39,751 --> 00:13:43,692
OK then, the basketball
team gets the pool as usual.

136
00:13:51,705 --> 00:13:54,866
Damn, check out those
Sakuragi High girls.

137
00:13:55,376 --> 00:13:57,237
That was a close call.

138
00:13:57,637 --> 00:14:01,338
We almost got stuck
synchro swimming.

139
00:14:01,448 --> 00:14:05,479
Besides, who the hell wants to
watch guys synchro swim?

140
00:14:05,589 --> 00:14:07,420
It only makes you barf.

141
00:14:07,520 --> 00:14:10,011
But what do we tell Sakuma...

142
00:14:10,121 --> 00:14:11,611
No sweat, man.

143
00:14:11,731 --> 00:14:15,593
She's busy havin' that baby.
She'll forget.

144
00:14:15,733 --> 00:14:19,534
What are we gonna do now?

145
00:14:19,874 --> 00:14:22,365
Let's study,
we still have to get into college.

146
00:14:22,875 --> 00:14:26,176
Oh, right.
We gotta study.

147
00:14:45,903 --> 00:14:46,733
Listen, I guess...

148
00:14:46,843 --> 00:14:49,834
Excellent!
Full panty sighting!

149
00:14:51,985 --> 00:14:54,916
I guess we can't really
synchro swim...

150
00:14:55,016 --> 00:14:59,217
Totally impossible, and besides,
they were never serious.

151
00:14:59,357 --> 00:15:03,088
Just a bunch of horny geeks.
And then she takes maternity leave.

152
00:15:03,198 --> 00:15:05,429
The worst.
You know they're gonna flake out.

153
00:15:05,529 --> 00:15:08,590
They're hopeless no matter what
they do, just hopeless.

154
00:15:08,870 --> 00:15:11,501
Let's bet whether they do it or not.

155
00:15:11,611 --> 00:15:13,002
Sure, 1,000 yen they won't.

156
00:15:13,112 --> 00:15:13,772
3,000 yen.

157
00:15:13,872 --> 00:15:17,173
Somebody bet on them
or we're not betting at all.

158
00:15:22,225 --> 00:15:23,775
What a shock.

159
00:16:00,607 --> 00:16:01,728
What?

160
00:16:09,120 --> 00:16:10,280
What the...

161
00:16:13,952 --> 00:16:17,863
Hey wait! We're gonna
use the pool after all.

162
00:16:17,963 --> 00:16:21,834
Too late, the fish guy already
put all the fish in.

163
00:16:21,934 --> 00:16:26,266
Don't be that way, man.
We're old basketball buddies.

164
00:16:26,476 --> 00:16:30,777
Basketball, huh!
You're such a total flake,

165
00:16:30,877 --> 00:16:34,708
you never made the team.
Now your teacher's run out on you!

166
00:16:35,589 --> 00:16:37,879
You didn't get injured in a game?

167
00:16:38,660 --> 00:16:41,491
The way he cut practice?
Never.

168
00:16:42,331 --> 00:16:44,762
Don't ever mention
our team again!

169
00:16:48,733 --> 00:16:53,735
Well, I don't know what
all the flap is about,

170
00:16:53,845 --> 00:16:58,606
but if you get all the fish out,
I can just take 'em home again.

171
00:17:01,487 --> 00:17:02,718
Go ahead.

172
00:17:04,018 --> 00:17:07,079
No way we can do that!

173
00:17:07,189 --> 00:17:12,151
Cool! If you guys catch all
the fish, the pool is yours.

174
00:17:13,301 --> 00:17:14,732
You swear!

175
00:17:23,714 --> 00:17:25,585
Goddammit!

176
00:17:33,128 --> 00:17:36,219
It's never gonna work.
Stop.

177
00:17:36,329 --> 00:17:38,769
Quit yakking and help.

178
00:17:38,869 --> 00:17:40,840
Some swim team!

179
00:17:47,342 --> 00:17:51,974
Emptying the pool is OK, but whose
gonna pay for the fresh water?

180
00:17:52,584 --> 00:17:55,485
No, I watched 'em do it before.

181
00:17:55,595 --> 00:17:59,026
This water meter's busted.
You can rig it.

182
00:18:02,967 --> 00:18:04,528
Get in, Sato.

183
00:18:04,638 --> 00:18:07,229
No way, you go!

184
00:18:08,439 --> 00:18:10,930
Better hurry or they'll die.

185
00:18:11,040 --> 00:18:13,481
Goddammit!

186
00:18:39,219 --> 00:18:40,810
Bunch of idiots.

187
00:18:44,551 --> 00:18:45,781
I'm going home.

188
00:18:49,663 --> 00:18:52,223
Man, you're a downer.

189
00:18:53,234 --> 00:18:55,665
If you just did it,
it'd be over.

190
00:18:55,775 --> 00:18:58,736
If you're gonna fart around,
you don't need me.

191
00:19:17,762 --> 00:19:19,392
What are you doing!

192
00:19:23,074 --> 00:19:24,434
Wait!
Don't run!

193
00:19:24,544 --> 00:19:26,105
Don't run!

194
00:20:09,439 --> 00:20:10,459
Look.

195
00:20:16,611 --> 00:20:18,982
Guess it's time we
called your parents.

196
00:20:19,082 --> 00:20:20,642
Oh, no, please.

197
00:20:20,783 --> 00:20:23,153
So, what are you
gonna do about it!

198
00:20:26,724 --> 00:20:29,755
Pay you back out of our
synchro take at the Festival.

199
00:20:30,826 --> 00:20:32,886
How can I count on that!

200
00:20:35,467 --> 00:20:39,199
Synchro? You mean
synchronized swimming?

201
00:20:39,809 --> 00:20:41,039
You guys?

202
00:20:41,139 --> 00:20:42,480
No, no, that's a matter of...

203
00:20:42,580 --> 00:20:43,700
Yes we are!

204
00:20:48,992 --> 00:20:53,053
For real? Are you guys good
enough to charge admission?

205
00:20:53,193 --> 00:20:54,784
We'll start practicing.

206
00:20:57,334 --> 00:21:01,066
Well, let's have a look, then.
And then we'll decide.

207
00:21:01,166 --> 00:21:03,897
You're on!
We'll show you in a week.

208
00:21:04,007 --> 00:21:05,167
Hey, Sato!

209
00:21:05,277 --> 00:21:06,938
A week, then.

210
00:21:07,878 --> 00:21:09,969
But the water.

211
00:21:10,149 --> 00:21:14,310
You guys emptied it.
I don't know how serious you are,

212
00:21:14,420 --> 00:21:18,251
but if you are,
figure it out yourselves.

213
00:21:18,832 --> 00:21:19,692
Sir.

214
00:21:19,792 --> 00:21:20,722
Yeah.

215
00:21:20,862 --> 00:21:23,193
How high's the water bill?

216
00:21:28,535 --> 00:21:29,835
About like that.

217
00:21:31,046 --> 00:21:34,607
Um, actually, your
math's a little off.

218
00:21:35,217 --> 00:21:38,088
25 meters long
by 13 meters wide,

219
00:21:38,188 --> 00:21:40,679
by 1.3 deep, so

220
00:21:41,819 --> 00:21:43,450
Actually, it's more.

221
00:21:43,930 --> 00:21:47,191
Synchronized Swim Tickets
-500 Yen

222
00:21:49,672 --> 00:21:51,362
Who's swimming?

223
00:21:51,472 --> 00:21:52,263
We are.

224
00:22:13,099 --> 00:22:15,530
Oh, I'm sorry, I really am.

225
00:22:19,171 --> 00:22:22,402
Can't sell one lousy ticket!

226
00:22:25,443 --> 00:22:30,145
Oh, I know, maybe you should
try selling them at that place.

227
00:22:30,255 --> 00:22:31,315
What!

228
00:22:34,526 --> 00:22:36,427
Huh...

229
00:22:37,227 --> 00:22:39,758
You could just buy one ticket.

230
00:22:39,868 --> 00:22:44,270
Just enough for the water bill.
Otherwise, we can't even practice.

231
00:22:46,880 --> 00:22:48,841
Will one of you promise

232
00:22:50,552 --> 00:22:53,643
to come work here
after you graduate?

233
00:23:07,437 --> 00:23:09,468
Why not start today.

234
00:23:14,409 --> 00:23:16,180
Just joking!

235
00:23:16,920 --> 00:23:21,682
Say ladies, these boys are
synchro artists for the Tadano Fest.

236
00:23:25,163 --> 00:23:28,624
You boys are so cute,
I'll buy 10.

237
00:23:28,734 --> 00:23:33,606
I run a host club, I'll buy
a stack, a whole stack.

238
00:23:37,947 --> 00:23:39,238
Thank goodness!

239
00:23:39,348 --> 00:23:40,008
He's so cute!

240
00:23:40,118 --> 00:23:42,109
Oh dear, he's crying.

241
00:23:42,689 --> 00:23:44,179
What are you crying about?

242
00:23:54,833 --> 00:23:55,763
Here goes.

243
00:24:02,775 --> 00:24:05,766
What are we
supposed to do?

244
00:24:08,817 --> 00:24:11,688
Bent knee pose
Bend one leg, and

245
00:24:11,788 --> 00:24:15,660
tuck the toes of the bent leg
behind the opposite knee.

246
00:24:16,930 --> 00:24:18,420
That's easy.

247
00:24:18,530 --> 00:24:22,902
Next, then, Ballet leg pose.
Straighten the leg into the air.

248
00:24:26,673 --> 00:24:31,275
Raise your hips and bend the
opposite leg for the Flamingo pose.

249
00:24:32,885 --> 00:24:34,946
Hey, easy, easy!

250
00:24:35,186 --> 00:24:38,587
Bent knee, Ballet leg,
Flamingo.

251
00:24:40,028 --> 00:24:44,159
Bent knee, Ballet leg,
Flamingo.

252
00:24:44,969 --> 00:24:49,301
Bent knee, Ballet leg,
Flamingo.

253
00:24:59,124 --> 00:25:02,085
Bent knee, Ballet leg, Flamin...

254
00:25:02,255 --> 00:25:03,485
Hey, wait!

255
00:25:04,696 --> 00:25:06,886
Bent knee, Ballet...

256
00:25:07,226 --> 00:25:09,957
I got a cramp!

257
00:25:45,209 --> 00:25:46,109
Huh?

258
00:25:50,891 --> 00:25:53,181
Outta the way!

259
00:25:53,292 --> 00:25:57,253
Outta the way!

260
00:26:01,904 --> 00:26:04,025
How's the synchro coming?

261
00:26:04,135 --> 00:26:06,506
Who knows, I sure don't.

262
00:26:06,606 --> 00:26:09,237
Are they serious, or what?

263
00:26:10,017 --> 00:26:11,347
What's the...

264
00:26:13,148 --> 00:26:15,139
Are you sure?

265
00:26:15,249 --> 00:26:16,189
Huh?

266
00:26:20,931 --> 00:26:21,861
Yeah.

267
00:26:28,773 --> 00:26:30,394
What's up with that?

268
00:26:33,215 --> 00:26:34,805
Let's check 'em out.

269
00:26:35,315 --> 00:26:37,806
I don't see 'em.

270
00:26:41,317 --> 00:26:42,418
Oh, no, they're here.

271
00:26:42,528 --> 00:26:44,928
What are they doing here!

272
00:26:45,059 --> 00:26:45,749
Oh, shit.

273
00:26:45,859 --> 00:26:47,589
We gotta do it.

274
00:26:47,699 --> 00:26:48,660
Already?

275
00:27:04,455 --> 00:27:06,616
Oh, shit, time out, time out.

276
00:27:10,557 --> 00:27:11,547
Dammit to hell!

277
00:27:54,481 --> 00:27:57,612
What, what happened!
How do I look?

278
00:27:57,722 --> 00:28:01,023
Who gives a damn!
What the hell are you doing!

279
00:28:01,123 --> 00:28:03,894
Why do you always flake out!

280
00:28:03,994 --> 00:28:08,236
Can it! You use this stupid thing
but you can't pose!

281
00:28:12,977 --> 00:28:16,849
What was that! You jerk!
Don't blame me!

282
00:28:17,749 --> 00:28:19,880
You weren't supposed
to jump in!

283
00:28:19,990 --> 00:28:21,210
But my string broke.

284
00:28:21,320 --> 00:28:23,481
Your wave crashed into me!

285
00:28:23,591 --> 00:28:26,022
Stop wearing
that dumb thing!

286
00:28:28,362 --> 00:28:30,693
Stop crying!
You queen!

287
00:28:33,704 --> 00:28:38,606
What the hell are you guys doing!
You can't do this at the Festival!

288
00:28:38,776 --> 00:28:40,806
As of today, the pool's off limits!

289
00:28:41,417 --> 00:28:43,477
So you'll pay my damages...

290
00:28:44,818 --> 00:28:47,189
I'm calling your parents now!

291
00:28:52,900 --> 00:28:54,231
Buncha losers!

292
00:28:54,361 --> 00:28:55,921
Stupid idiots!

293
00:29:05,645 --> 00:29:09,516
Your summer vacation
starts tomorrow,

294
00:29:09,656 --> 00:29:14,287
so take care of yourselves
and practice moderation.

295
00:29:14,728 --> 00:29:18,559
The Tadano Festival's
coming up after vacation,

296
00:29:18,659 --> 00:29:23,430
and I'm sure some of you will
come to school to prepare.

297
00:29:23,871 --> 00:29:24,961
However...

298
00:29:35,054 --> 00:29:39,256
For you seniors, it's
time to give up sports

299
00:29:39,356 --> 00:29:43,887
and study for
your college exams.

300
00:29:45,128 --> 00:29:49,259
For those of you planning
to take the Standardized Exam,

301
00:29:49,369 --> 00:29:53,640
as you see on the board, for
English, it's Standardized English,

302
00:29:53,740 --> 00:29:57,682
and for math, we recommend
Standardized Math.

303
00:29:57,782 --> 00:30:00,483
And as for Japanese...

304
00:30:14,707 --> 00:30:16,468
What is this!

305
00:30:16,578 --> 00:30:17,538
Bam!

306
00:30:20,679 --> 00:30:23,240
That's the trick.
Here.

307
00:30:23,450 --> 00:30:25,641
Oh, um, thank you...

308
00:30:31,123 --> 00:30:32,353
Oh, that...

309
00:30:48,978 --> 00:30:49,949
Here.

310
00:30:58,832 --> 00:31:00,732
Indirect kiss!

311
00:31:03,133 --> 00:31:05,734
I'm Shizuko from
Sakuragi High.

312
00:31:05,834 --> 00:31:06,604
Oh, I'm...

313
00:31:06,704 --> 00:31:08,475
You're Suzuki from
Tadano, right?

314
00:31:08,605 --> 00:31:10,335
What? How'd you...

315
00:31:10,776 --> 00:31:14,267
I've always had a crush on you.
Will you go out with me?

316
00:31:14,847 --> 00:31:15,747
What!

317
00:31:16,457 --> 00:31:19,619
Oh, um... okay.

318
00:31:21,029 --> 00:31:23,360
You guys are
getting all steamy.

319
00:31:23,460 --> 00:31:26,301
Busting the air conditioner.

320
00:31:26,801 --> 00:31:29,632
Ditch that weirdo,
come play with us.

321
00:31:29,842 --> 00:31:31,302
Don't!

322
00:31:31,442 --> 00:31:33,173
Let her go.
She's upset.

323
00:31:41,286 --> 00:31:43,216
Will you marry me?

324
00:31:45,287 --> 00:31:47,088
You're Suzuki
from Tadano, right?

325
00:31:47,198 --> 00:31:49,318
What? How'd you...

326
00:31:52,429 --> 00:31:53,700
Suzuki - Tadano High

327
00:31:54,170 --> 00:31:55,100
Karate!

328
00:31:59,742 --> 00:32:01,072
Nice!

329
00:32:01,642 --> 00:32:04,413
My brothers took me,
ever since I was little.

330
00:32:04,513 --> 00:32:08,425
I assumed all families
had tile cracking contests.

331
00:32:08,655 --> 00:32:11,746
But not likely, right?

332
00:32:13,496 --> 00:32:15,017
And you?

333
00:32:15,367 --> 00:32:16,327
Oh, synchro.

334
00:32:16,437 --> 00:32:17,227
What?

335
00:32:17,337 --> 00:32:20,999
Oh, synchro-songwriter club.

336
00:32:21,209 --> 00:32:23,700
That's so cool!

337
00:32:27,751 --> 00:32:29,111
Just keep walking.

338
00:32:29,211 --> 00:32:30,152
What?

339
00:32:31,122 --> 00:32:35,213
There's a really weird guy
behind us who's been staring.

340
00:32:35,954 --> 00:32:37,854
It is you, Suzuki.

341
00:32:37,964 --> 00:32:38,895
What?

342
00:32:38,995 --> 00:32:41,295
Don't recognize me out of drag?

343
00:32:43,736 --> 00:32:45,537
Oh, Mama san from the bar...

344
00:32:45,637 --> 00:32:46,767
You know him?

345
00:32:46,877 --> 00:32:47,807
Sort of.

346
00:32:47,907 --> 00:32:51,279
I see you're not wasting any time.
How are ticket sales?

347
00:32:51,379 --> 00:32:54,580
Oh, yes, well, so.

348
00:32:55,050 --> 00:32:58,251
Looking forward to
that Tadano Fest, take care.

349
00:33:01,762 --> 00:33:03,423
And who was that?

350
00:33:03,863 --> 00:33:05,293
Just someone I know.

351
00:33:05,493 --> 00:33:09,435
Huh.
What are you doing at the Festival?

352
00:33:09,905 --> 00:33:12,566
I wanna watch, can I come see?

353
00:33:12,676 --> 00:33:15,977
No, it's not really
set yet or anything.

354
00:33:16,547 --> 00:33:18,447
We might not even do it.

355
00:33:18,748 --> 00:33:20,308
Make up your mind.

356
00:33:20,418 --> 00:33:22,439
Like I said, I don't know...

357
00:33:22,549 --> 00:33:26,580
I myself plan
to try really hard, but...

358
00:33:32,832 --> 00:33:36,493
Like you're a guy and all,
but you synchro?

359
00:33:36,603 --> 00:33:39,264
I told you I quit!

360
00:33:39,374 --> 00:33:41,905
How way, way, uncool.

361
00:33:43,646 --> 00:33:48,557
Join our band for the Festival.
It's the best way to get girls.

362
00:33:48,657 --> 00:33:51,848
That's right, even
this jerk got a girlfriend.

363
00:33:51,958 --> 00:33:53,719
What was that!

364
00:34:14,156 --> 00:34:18,557
You don't like aquariums?
Why didn't you tell me?

365
00:34:18,657 --> 00:34:20,648
No, it's not really that...

366
00:34:21,168 --> 00:34:25,069
It's always something with you!
Make up your mind!

367
00:34:27,710 --> 00:34:30,801
I do have a tendency
to get carried away...

368
00:34:31,882 --> 00:34:33,282
It's no big deal, OK?

369
00:35:24,849 --> 00:35:26,319
Giddiyup.

370
00:35:27,920 --> 00:35:29,751
Giddiyup, Giddyup.

371
00:35:38,433 --> 00:35:40,664
Thank you.

372
00:35:47,476 --> 00:35:50,277
Thank you, thank you.

373
00:35:50,387 --> 00:35:53,008
Ladies and gentlemen,
boys and girls.

374
00:35:53,118 --> 00:35:55,519
Grandpas or grandmas.

375
00:35:55,619 --> 00:35:59,320
Today's show is now over.

376
00:36:01,831 --> 00:36:04,262
Dolphins are so cool!

377
00:36:13,145 --> 00:36:17,086
Ouch! Ouch, ouch, ouch.

378
00:36:21,728 --> 00:36:23,158
Um, excuse me.

379
00:36:23,258 --> 00:36:24,248
Huh?

380
00:36:25,329 --> 00:36:28,160
Oh, you're that synchro squirt.

381
00:36:29,500 --> 00:36:31,901
This area's closed to spectators.

382
00:36:32,441 --> 00:36:36,903
Please, teach us! How can
we put on a show like that!

383
00:36:37,013 --> 00:36:38,573
Are you crazy?

384
00:36:38,683 --> 00:36:42,114
I use dolphins.
Whole different universe from synchro.

385
00:36:42,214 --> 00:36:44,115
But I want to move like that.

386
00:36:44,225 --> 00:36:45,815
Ridiculous.

387
00:36:45,986 --> 00:36:49,117
I'll do anything!
I'll practice anything!

388
00:36:49,697 --> 00:36:53,098
The way we are...
is so pathetic.

389
00:36:55,539 --> 00:36:58,870
Moving freely in the
water is no mean feat.

390
00:36:59,610 --> 00:37:01,271
You got what it takes!?

391
00:37:01,811 --> 00:37:02,871
Yes...

392
00:37:04,352 --> 00:37:05,242
Probably.

393
00:37:05,812 --> 00:37:07,152
No probablys.

394
00:37:08,323 --> 00:37:12,844
Then promise you'll
do anything I say.

395
00:37:30,350 --> 00:37:31,751
Ota.

396
00:37:32,221 --> 00:37:33,451
Ota.

397
00:37:51,207 --> 00:37:54,378
Is that your best!
You can do better, I know it.

398
00:37:57,849 --> 00:38:00,150
Now your deltoids.

399
00:38:02,391 --> 00:38:05,852
Push yourself harder.
30 more seconds.

400
00:38:06,232 --> 00:38:08,633
Muscles! OK, let's go!

401
00:38:10,373 --> 00:38:11,964
Good, nice work-out.!

402
00:38:45,245 --> 00:38:48,306
Sato! Sato!

403
00:38:56,868 --> 00:38:57,919
Hey, man!

404
00:38:58,029 --> 00:38:59,899
What are you doing here!

405
00:39:00,400 --> 00:39:01,230
Let's swim, man.

406
00:39:01,340 --> 00:39:04,311
Are you nuts? There's no way.
The pool's off limits.

407
00:39:04,411 --> 00:39:07,772
We hijack the pool on Festival day.
We train at the aquarium.

408
00:39:07,882 --> 00:39:08,642
Aquarium?

409
00:39:08,742 --> 00:39:10,773
That fish guy's going to
teach us stuff.

410
00:39:10,883 --> 00:39:13,544
Our finest days of high school.

411
00:39:13,654 --> 00:39:18,526
It's so dorky, just give it up.
You'll wind up looking dumb.

412
00:39:26,568 --> 00:39:28,799
You really are the worst.

413
00:39:35,451 --> 00:39:37,682
I hate myself, too.

414
00:40:20,906 --> 00:40:22,807
Driver! Please!

415
00:41:08,172 --> 00:41:11,573
So you made it.
You synchro squirts!

416
00:41:21,156 --> 00:41:23,887
You make them leap?

417
00:41:24,187 --> 00:41:25,817
Of course not,
are you nuts?

418
00:41:31,529 --> 00:41:35,560
They get all excited when they
see me 'cause I mean food.

419
00:41:36,801 --> 00:41:39,672
There you go, there.

420
00:41:48,155 --> 00:41:51,056
Leave your stuff and come down.
Move it!

421
00:41:52,326 --> 00:41:55,697
Thank you for visiting
Sea World today.

422
00:41:55,797 --> 00:41:58,358
What is this?

423
00:41:59,668 --> 00:42:01,469
Dunno... ouch.

424
00:42:02,869 --> 00:42:04,740
What's this fish called?

425
00:42:04,840 --> 00:42:06,971
I'm sorry, I'm not sure.

426
00:42:07,681 --> 00:42:08,611
Idiot!

427
00:42:08,711 --> 00:42:12,653
What are you doing!
Hurry or we'll leave you here.

428
00:42:12,823 --> 00:42:14,983
Idiot! Idiot! Idiot!

429
00:42:20,925 --> 00:42:25,227
Polish the plate glass till it gleams.
And I mean spotless!

430
00:42:25,937 --> 00:42:29,098
You've got until morning,
when we open again.

431
00:42:29,238 --> 00:42:30,268
Um...

432
00:42:31,039 --> 00:42:32,939
"Anything I say," remember?

433
00:42:35,150 --> 00:42:38,281
What the hell's going on, Suzuki?

434
00:42:51,065 --> 00:42:56,007
Not me! I came here to practice
synchro, not wash windows!

435
00:43:02,649 --> 00:43:06,750
Wow, look at those eels.
They sure look tasty.

436
00:43:06,860 --> 00:43:08,091
Don't eat them.

437
00:43:08,191 --> 00:43:09,661
I won't.

438
00:43:09,891 --> 00:43:11,762
There's squid over here!

439
00:43:12,202 --> 00:43:13,723
And crab over there.

440
00:43:14,433 --> 00:43:16,834
This aquarium's just like a sushi bar.

441
00:43:16,934 --> 00:43:17,874
Yeah.

442
00:43:19,275 --> 00:43:24,236
Seafood Buffet
All you can eat

443
00:43:24,686 --> 00:43:28,878
All you can eat
Odllayee odllayee hee hoo

444
00:43:29,788 --> 00:43:33,519
Tuna belly, abalone, squid
All you can eat

445
00:43:33,629 --> 00:43:38,461
Ark shell, mackerel, salmon roe
and shrimp, All you can eat

446
00:43:42,802 --> 00:43:45,933
We did it! We really did it!

447
00:43:46,573 --> 00:43:50,135
Thanks squirts.
Looks good.

448
00:43:50,245 --> 00:43:52,735
Tomorrow you'll hit that tank.

449
00:43:53,856 --> 00:43:54,846
What?

450
00:44:49,594 --> 00:44:52,465
Finally...
We did it...

451
00:44:52,565 --> 00:44:55,086
We did it! We did it!

452
00:44:58,477 --> 00:45:00,568
Good morning
Welcome to Sea World.

453
00:45:00,678 --> 00:45:01,768
Outta here!

454
00:45:08,990 --> 00:45:10,451
Hey, hey.

455
00:45:10,921 --> 00:45:13,322
What's this weird fish called?

456
00:45:13,422 --> 00:45:14,822
An Elephant Nose.

457
00:45:14,932 --> 00:45:16,123
And that one?

458
00:45:16,233 --> 00:45:17,493
Tiger Oatfish.

459
00:45:17,593 --> 00:45:19,034
A Nautilus.

460
00:45:19,134 --> 00:45:20,104
A Dragonfish.

461
00:45:20,204 --> 00:45:24,406
Trinecties Maculatus, Melanotaenia.
Need a tour guide?

462
00:45:24,506 --> 00:45:26,066
Oh, no, that's alright.

463
00:45:26,376 --> 00:45:28,567
Something odd about those kids.

464
00:45:32,088 --> 00:45:36,780
It's already been two weeks.
How long do we have to do this?

465
00:45:37,120 --> 00:45:39,250
Summer vacation'll be over.

466
00:45:39,731 --> 00:45:42,822
Maybe he tricked us.

467
00:45:49,974 --> 00:45:52,445
Oops, I'm sorry, sorry.

468
00:45:55,316 --> 00:45:57,877
Dammit!

469
00:46:28,056 --> 00:46:30,427
This pool's for the dolphins!

470
00:46:36,539 --> 00:46:38,940
You said you'd coach us,
so we came.

471
00:46:39,040 --> 00:46:42,981
But it's hard labor everyday.
You're just a slave driver!

472
00:46:43,081 --> 00:46:45,842
Shut up! You cleaned all the tanks?

473
00:46:45,952 --> 00:46:46,843
Yes.

474
00:46:47,283 --> 00:46:51,844
What? All of 'em?
Even the fresh water fish?

475
00:46:51,954 --> 00:46:53,985
Yes, we're finished.

476
00:46:55,165 --> 00:46:57,956
You've got to tell
us what's going on.

477
00:46:58,066 --> 00:47:02,838
Do we just stare at fish and polish
tanks or are you going to teach us?

478
00:47:06,009 --> 00:47:07,309
Show me your arms.

479
00:47:12,581 --> 00:47:13,781
OK, next.

480
00:47:19,163 --> 00:47:22,254
OK, try the Bottle Nose Dolphin.

481
00:47:22,434 --> 00:47:23,124
Huh?

482
00:47:23,234 --> 00:47:25,135
Move like a dolphin.

483
00:47:28,106 --> 00:47:29,597
In the water, dammit!

484
00:47:31,747 --> 00:47:34,268
Scull with your hands to stay in place.

485
00:47:35,018 --> 00:47:38,009
If you can't do that, all
your training's been wasted!

486
00:47:38,449 --> 00:47:40,420
You call that training!

487
00:47:40,520 --> 00:47:44,481
Shut up! You don't like it, you
hustle your butts outta here!

488
00:48:00,447 --> 00:48:01,347
Damn.

489
00:48:02,988 --> 00:48:07,319
That doesn't even count as basic!
OK, next!

490
00:48:08,059 --> 00:48:12,020
After Bottle Nose Dolphin, do
Sea Otter, Squid and Flamingo.

491
00:48:12,401 --> 00:48:14,451
Finally, do the Nautilus Roll!

492
00:48:16,772 --> 00:48:19,003
Bottle Nose Dolphin.

493
00:48:19,173 --> 00:48:20,763
Sea Otter.

494
00:48:22,214 --> 00:48:23,904
Squid.

495
00:48:31,087 --> 00:48:32,577
Flamingo.

496
00:48:36,098 --> 00:48:37,529
Nautilus.

497
00:48:40,400 --> 00:48:41,330
You can swim!

498
00:48:41,430 --> 00:48:42,631
You did it!

499
00:48:42,741 --> 00:48:43,671
Yeah.

500
00:48:43,971 --> 00:48:45,602
We really were learning.

501
00:48:45,912 --> 00:48:47,932
What do we practice next?

502
00:48:48,042 --> 00:48:52,984
Huh? Lots of stuff.
What do you know?

503
00:49:00,226 --> 00:49:02,697
OK, here we are.
Get out.

504
00:49:03,127 --> 00:49:05,568
Slept like a log.

505
00:49:05,898 --> 00:49:07,159
Where are we?

506
00:49:07,269 --> 00:49:09,899
My butt hurts.

