﻿1
00:00:02,702 --> 00:00:04,169
- Da, da, da, ♪ da, da, da-ba-da,
da, da ♪ ba-let's go! Ba ♪

2
00:00:04,204 --> 00:00:06,038
♪ we'll be there ♪

3
00:00:06,072 --> 00:00:08,540
♪ a wink and a smile
and a great old time ♪

4
00:00:08,575 --> 00:00:10,943
♪ yeah, we'll be there ♪

5
00:00:10,977 --> 00:00:12,978
♪ wherever we are,
there's fun to be found ♪

6
00:00:13,013 --> 00:00:15,147
♪ we'll be there
when you turn that corner ♪

7
00:00:15,181 --> 00:00:16,215
♪ when you jump out the bush ♪

8
00:00:16,249 --> 00:00:18,217
♪ with a big bear smile ♪

9
00:00:18,251 --> 00:00:19,551
♪ we'll be there ♪

10
00:00:29,596 --> 00:00:32,398
Okay, I need two men
on that aerial ladder.

11
00:00:32,432 --> 00:00:34,112
Get the life net,
in case the animal falls.

12
00:00:44,444 --> 00:00:45,744
Hey, we almost there?

13
00:00:45,779 --> 00:00:47,780
Says that it's right here.

14
00:00:47,814 --> 00:00:49,248
Wup, wup, we passed it.

15
00:00:49,282 --> 00:00:50,516
Back up, back up.

16
00:00:50,550 --> 00:00:52,718
There it is.
I see it!

17
00:00:54,754 --> 00:00:56,455
Welcome to purrito, gentlemen.

18
00:00:56,489 --> 00:00:58,290
What can I get for you today?

19
00:00:58,324 --> 00:01:01,126
- Um, I'll have the veggie tacos,
please, and some extra salsa.

20
00:01:01,194 --> 00:01:02,561
Okay.

21
00:01:02,595 --> 00:01:05,697
- Ice bear
will have more free chips.

22
00:01:05,765 --> 00:01:07,299
And what about you, sir?

23
00:01:07,333 --> 00:01:09,768
-What can you tell me about

24
00:01:09,803 --> 00:01:12,271
this?! ♪

25
00:01:12,338 --> 00:01:14,573
- Ah, yes, our famous
burrito challenge.

26
00:01:14,607 --> 00:01:17,376
Succeed in eating our whole
array of burritos in one hour,

27
00:01:17,444 --> 00:01:19,378
and you'll get your photo
up on our wall.

28
00:01:19,446 --> 00:01:20,646
It can be pretty hard, though.

29
00:01:20,713 --> 00:01:22,181
I'd be lying if I told you

30
00:01:22,215 --> 00:01:23,682
there weren't gonna be
tears involved.

31
00:01:23,716 --> 00:01:25,756
So, I understand if you
don't want to do it anymore.

32
00:01:27,687 --> 00:01:29,221
Oh, that's cute.

33
00:01:29,289 --> 00:01:31,657
Oh, I'll do your
precious challenge.

34
00:01:31,691 --> 00:01:34,593
And I'll do it in half an hour.

35
00:01:34,627 --> 00:01:36,328
-Ice bear wants separate checks.

36
00:01:36,396 --> 00:01:39,731
Okay. Ready... Start!

37
00:01:39,766 --> 00:01:42,101
First up,
the fajita steak burrito.

38
00:01:43,369 --> 00:01:44,837
Omm!

39
00:01:44,871 --> 00:01:46,171
Another!

40
00:01:46,239 --> 00:01:47,573
-Jalapeño chicken burrito.

41
00:01:47,607 --> 00:01:48,841
Omm!

42
00:01:48,875 --> 00:01:51,076
-Baja Halibut burrito.

43
00:01:51,144 --> 00:01:52,611
Avocado delight. Pork wasabi.
Veggie party. Bacon and egg.

44
00:01:52,645 --> 00:01:55,147
Crazy meat.

45
00:01:55,181 --> 00:01:57,082
Cilantro overload.

46
00:02:01,254 --> 00:02:02,554
Omm! Finished!

47
00:02:02,622 --> 00:02:04,156
Finished!
Finished.

48
00:02:04,190 --> 00:02:06,859
Wow. 15 minutes.

49
00:02:06,893 --> 00:02:08,160
That's a new record!

50
00:02:08,194 --> 00:02:11,497
But there's still
one last burrito.

51
00:02:11,564 --> 00:02:13,265
Are you sure you want to do...

52
00:02:13,299 --> 00:02:14,867
That one?

53
00:02:14,901 --> 00:02:16,768
We have no choice.
Do it.

54
00:02:22,942 --> 00:02:25,110
Tortilla warmers!
Faster!

55
00:02:25,145 --> 00:02:26,378
Get 'em off the stove!
We need them over here,

56
00:02:26,412 --> 00:02:27,646
- laid out.
- Cilantro, hot sauce.

57
00:02:27,680 --> 00:02:28,881
Okay, spread 'em
with sour cream.

58
00:02:28,915 --> 00:02:30,582
-Tortillas! Tortillas!

59
00:02:30,617 --> 00:02:31,850
Spread them all the way.
We want every corner covered.

60
00:02:31,885 --> 00:02:35,721
- Meat, meat!
Tortillas, then meat!

61
00:02:35,755 --> 00:02:38,290
Tortillas!
Tortillas! Tortillas!

62
00:02:38,358 --> 00:02:40,626
This contest is a breeze!

63
00:02:40,660 --> 00:02:42,394
What's one more burrito
gonna do?

64
00:02:42,428 --> 00:02:44,563
Uh, grizz.
Hmm?

65
00:02:57,310 --> 00:02:58,944
Oooooh!

66
00:02:58,978 --> 00:03:01,580
Gimme, gimme, gimme.

67
00:03:01,614 --> 00:03:03,348
Ooh-eh.

68
00:03:06,486 --> 00:03:07,653
Mmm.

69
00:03:11,291 --> 00:03:14,159
Um... You doing okay
there, grizz?

70
00:03:14,194 --> 00:03:17,963
Yeah, it's just this burrito,
it's... So comforting.

71
00:03:17,997 --> 00:03:19,731
Nice and warm.

72
00:03:19,766 --> 00:03:21,200
Safe.

73
00:03:21,234 --> 00:03:22,868
Safe burrito.

74
00:03:22,902 --> 00:03:25,437
-Well? Eat it.

75
00:03:25,471 --> 00:03:27,306
N-no!
It's too perfect to eat!

76
00:03:27,340 --> 00:03:28,874
I will eat it!

77
00:03:28,908 --> 00:03:31,276
Stay away!
You stay away from burrito.

78
00:03:31,311 --> 00:03:32,544
Let's get outta here, guys.

79
00:03:40,720 --> 00:03:41,820
-Cash only.

80
00:03:45,258 --> 00:03:48,760
Uh... Are we really
gonna keep that thing?

81
00:03:48,795 --> 00:03:50,429
Of course, silly.

82
00:03:50,463 --> 00:03:52,464
What else would we do with
such a magnificent burrito?

83
00:03:52,532 --> 00:03:54,399
Uh... Not keep it?

84
00:03:54,434 --> 00:03:56,602
Hey, a theater!
We should watch a movie.

85
00:03:56,636 --> 00:03:58,403
Come on, come on, come on.

86
00:03:58,438 --> 00:03:59,771
It's your turn to pay, panda.

87
00:03:59,839 --> 00:04:01,840
Ahem, three ticke...

88
00:04:01,874 --> 00:04:04,276
Hello! Can you hear me?
- Yes.

89
00:04:04,310 --> 00:04:06,645
Great! Three tickets,
please.

90
00:04:06,679 --> 00:04:08,647
-You got four people there.

91
00:04:08,681 --> 00:04:10,782
Uh, no, we have three.

92
00:04:10,817 --> 00:04:13,218
- You'll have to pay
for that thing, as well.

93
00:04:13,253 --> 00:04:15,621
Uh, there must be
a misunderstanding here.

94
00:04:15,688 --> 00:04:17,823
You see, that's just a burrito.

95
00:04:17,890 --> 00:04:19,258
It can't watch movies.

96
00:04:19,325 --> 00:04:20,659
Hey, watch what you say, man.

97
00:04:20,693 --> 00:04:22,327
Burrito's gonna enjoy
this movie.

98
00:04:22,362 --> 00:04:23,595
Don't listen
to that silly panda.

99
00:04:23,630 --> 00:04:27,666
It's okay.

100
00:04:27,734 --> 00:04:30,402
Um, any discounts
for the burrito?

101
00:04:30,470 --> 00:04:32,337
Fine.

102
00:04:41,748 --> 00:04:44,616
♪ Whoo-hoo! All right! ♪
- what the...?

103
00:04:44,684 --> 00:04:47,352
♪ I think we gon' be chillin' ♪

104
00:04:47,420 --> 00:04:49,788
♪ whoo-hoo! All right! ♪

105
00:04:49,822 --> 00:04:51,062
♪ I think we gon' be chillin' ♪

106
00:04:52,692 --> 00:04:56,595
♪ We took a tour
down to market street ♪

107
00:04:56,629 --> 00:04:58,030
♪ ♪

108
00:04:58,064 --> 00:05:01,667
♪ then rode a cable car
and got a receipt ♪

109
00:05:01,701 --> 00:05:03,669
♪ -so, we're sitting

110
00:05:03,703 --> 00:05:06,271
outside of a studio and then,
somebody comes to me

111
00:05:06,306 --> 00:05:08,607
and asks me for a quarter
and I looked at burrito.

112
00:05:08,641 --> 00:05:10,609
♪ I said right on, my brother ♪

113
00:05:10,643 --> 00:05:12,811
♪ have a nice day ♪
- ahem.

114
00:05:12,845 --> 00:05:14,579
♪ Hey-ey! ♪

115
00:05:14,614 --> 00:05:17,916
♪ I think we're
gon' be chillin' ♪

116
00:05:17,950 --> 00:05:19,751
Whee-ee-ee!

117
00:05:19,786 --> 00:05:23,555
♪ I think we're
gon' be chillin' ♪

118
00:05:23,589 --> 00:05:24,790
♪ ♪

119
00:05:24,824 --> 00:05:28,427
♪ we're gon' be chillin' ♪

120
00:05:28,461 --> 00:05:31,897
♪ I think we're gonna be,
we're gonna be chillin' ♪

121
00:05:31,931 --> 00:05:35,767
♪ Whoo! All right ♪

122
00:05:35,802 --> 00:05:38,337
♪ I think we gon' be chillin' ♪

123
00:05:38,371 --> 00:05:40,706
♪ whoo-hoo! All right! ♪
- huh?

124
00:05:40,740 --> 00:05:43,442
♪ I think we gon' be chillin' ♪

125
00:05:43,476 --> 00:05:48,647
♪ I think we're gonna be,
we're gonna be chillin' ♪

126
00:05:48,681 --> 00:05:49,948
♪

127
00:05:49,982 --> 00:05:51,683
Ooh, shortcut.

128
00:05:51,751 --> 00:05:53,618
Hey, grizz, we're back!
We got your...

129
00:05:54,754 --> 00:05:56,888
Ohh!
What is that smell?

130
00:06:04,430 --> 00:06:05,497
Uh...

131
00:06:05,531 --> 00:06:07,666
Ice bear feels nauseous.

132
00:06:07,700 --> 00:06:08,934
Grizz, this is disgusting.

133
00:06:08,968 --> 00:06:10,435
Your burrito smells horrible!

134
00:06:10,470 --> 00:06:13,138
[Gasp] How could you
say that about burrito,

135
00:06:13,172 --> 00:06:14,740
and right in front of him?

136
00:06:14,774 --> 00:06:16,041
Panda, I'm shocked.

137
00:06:16,075 --> 00:06:17,642
<i>You're</i> shocked?

138
00:06:17,677 --> 00:06:19,344
I'm the one who
should be shocked!

139
00:06:19,379 --> 00:06:20,946
You never want to go
out with us anymore.

140
00:06:20,980 --> 00:06:23,648
You're never two feet
away from that burrito!

141
00:06:23,683 --> 00:06:24,950
It's creepy, man!

142
00:06:24,984 --> 00:06:26,385
Ice bear forced to agree.

143
00:06:26,419 --> 00:06:27,619
You guys just don't understand.

144
00:06:27,653 --> 00:06:30,055
Also, it's not "that burrito,"

145
00:06:30,089 --> 00:06:31,723
it's just burrito.

146
00:06:31,758 --> 00:06:33,392
You've changed, grizz.

147
00:06:33,426 --> 00:06:35,360
You're wrong!
Burrito's the best thing

148
00:06:35,395 --> 00:06:38,130
that ever happened to me,
so just leave us alone!

149
00:06:42,101 --> 00:06:45,904
I think it's worse
than we thought.

150
00:06:45,938 --> 00:06:47,773
I found this website
about people

151
00:06:47,807 --> 00:06:49,541
who get too attached
to their food.

152
00:06:49,575 --> 00:06:51,710
Look at all of these.

153
00:06:51,744 --> 00:06:54,112
This lady in Idaho
became obsessed

154
00:06:54,147 --> 00:06:56,014
with a bag of chips,

155
00:06:56,082 --> 00:06:58,383
someone in Japan married
their bowl of ramen,

156
00:06:58,451 --> 00:07:02,154
and this guy adopted
a jar of pickles.

157
00:07:02,188 --> 00:07:03,855
It just goes on and on!

158
00:07:03,923 --> 00:07:06,525
They forget about
their families and friends

159
00:07:06,559 --> 00:07:09,828
and sometimes just
become total hermits.

160
00:07:09,862 --> 00:07:11,863
I think one thing is certain.

161
00:07:11,898 --> 00:07:13,432
Ice bear sees
where this is going.

162
00:07:13,466 --> 00:07:18,036
We have to do something
about that burrito.

163
00:07:21,073 --> 00:07:23,108
Uh...

164
00:07:32,552 --> 00:07:34,753
burrito!

165
00:07:34,787 --> 00:07:38,824
Ah.

166
00:07:38,858 --> 00:07:40,659
Huh?

167
00:07:40,693 --> 00:07:41,793
What the...?

168
00:07:41,861 --> 00:07:42,861
Uh...

169
00:07:42,929 --> 00:07:44,629
burrito?

170
00:07:45,798 --> 00:07:47,532
Hey, come back here!

171
00:07:47,567 --> 00:07:48,667
Kidnappers!

172
00:07:48,701 --> 00:07:49,868
Don't hate us, please!

173
00:07:55,641 --> 00:07:57,943
Oh, it smells so bad!

174
00:07:57,977 --> 00:08:00,145
Ah! My hand
has juices on it!

175
00:08:05,651 --> 00:08:07,719
Ahh! Oh!

176
00:08:11,591 --> 00:08:13,024
Ow!

177
00:08:23,069 --> 00:08:25,470
We're just trying
to help you, grizz!

178
00:08:25,505 --> 00:08:27,472
This burrito
is driving you insane!

179
00:08:27,507 --> 00:08:30,041
Grizz...
Needs... Burrito.

180
00:08:30,076 --> 00:08:32,711
Uh, up the tower!

181
00:08:38,718 --> 00:08:41,653
Whoa-oa!

182
00:08:41,687 --> 00:08:43,922
Dudes, why are you doing this?

183
00:08:43,990 --> 00:08:47,158
I need to be with burrito.

184
00:08:47,193 --> 00:08:49,160
Burrito completes me!

185
00:08:49,195 --> 00:08:51,162
Grizz, please!
It's wrong to be

186
00:08:51,197 --> 00:08:53,832
dependent on something
so unnatural.

187
00:08:53,866 --> 00:08:55,667
We are doing this for you!

188
00:08:55,701 --> 00:08:56,868
Let me cradle burrito.

189
00:08:56,936 --> 00:08:58,270
We're concerned
because we love...

190
00:08:58,304 --> 00:08:59,938
Dude, stop it.
Not right now.

191
00:08:59,972 --> 00:09:01,306
Grizz, you need to understan...

192
00:09:01,340 --> 00:09:02,541
What?

193
00:09:02,608 --> 00:09:03,575
Ah-aah!

194
00:09:03,609 --> 00:09:07,746
Huh?

195
00:09:07,780 --> 00:09:10,148
Grizz! I'm sorry.
I'm so... wha-ah-oh!

196
00:09:10,182 --> 00:09:12,851
I'm so sorry!

197
00:09:12,885 --> 00:09:13,952
Uh-whoa!

198
00:09:13,986 --> 00:09:15,220
Grizz are you okay?!

199
00:09:15,254 --> 00:09:18,290
Oh, oh, my gosh! We...
We didn't mean to...

200
00:09:18,324 --> 00:09:20,892
We didn't think it would...
We could still fix it.

201
00:09:23,829 --> 00:09:27,265
Put this right there and...

202
00:09:27,300 --> 00:09:28,900
There. See, grizz?
It's not so bad.

203
00:09:28,935 --> 00:09:31,202
We could just put it
back togeth... whoa-oa!

204
00:09:35,841 --> 00:09:38,209
Oh! That was
so unpleasant.

205
00:09:38,244 --> 00:09:40,078
Whoa!

206
00:09:44,016 --> 00:09:46,251
Ha!

207
00:09:46,285 --> 00:09:47,319
Oh!

208
00:09:53,926 --> 00:09:55,627
Ooh!

209
00:09:57,897 --> 00:10:00,298
Ah, I am sorry if I was
acting weird, you guys.

210
00:10:00,366 --> 00:10:01,866
We were really worried there.

211
00:10:01,901 --> 00:10:04,669
Oh, don't worry about it.
I feel good as new.

212
00:10:04,737 --> 00:10:05,904
Come on, let's head home.

213
00:10:05,938 --> 00:10:08,773
Ice bear demands
everyone showers.

214
00:10:08,808 --> 00:10:11,810
Hm, I wonder why I was
so obsessed with that burrito.

215
00:10:11,877 --> 00:10:14,045
Ah, I guess it doesn't matter.

216
00:10:23,990 --> 00:10:26,358
<i>All cars, immediately.</i>

217
00:10:26,392 --> 00:10:28,159
Affirmative,
we've secured the area

218
00:10:28,194 --> 00:10:29,594
and fire and rescue is
on the scene.

219
00:10:30,863 --> 00:10:32,364
Okay, I need two men
on that aerial ladder.

220
00:10:32,398 --> 00:10:34,733
Get the life net,
in case the animal falls.

221
00:10:44,777 --> 00:10:46,778
It's okay, little guy.

222
00:10:46,846 --> 00:10:47,879
Grab on.

223
00:10:47,947 --> 00:10:49,147
You're safe now.

224
00:10:49,215 --> 00:10:51,416
♪ Please take me there ♪

225
00:10:51,450 --> 00:10:56,988
♪ I'm ready ♪

226
00:10:57,056 --> 00:11:00,191
I'm not sure how this
little fella got up there.

227
00:11:00,226 --> 00:11:02,093
He's holding my arm so tight.

228
00:11:02,161 --> 00:11:03,728
Cute little guy.

229
00:11:03,796 --> 00:11:05,930
I hope he doesn't get
too attached, heh.

