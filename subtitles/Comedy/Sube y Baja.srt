1
00:00:01,380 --> 00:00:05,420
The movements of my heart

2
00:00:06,460 --> 00:00:10,360
are ruled by sweet suggestion

3
00:00:11,880 --> 00:00:16,300
of a glance, of a vibration,

4
00:00:17,000 --> 00:00:21,040
and not by the words from your mouth

5
00:00:21,100 --> 00:00:26,960
You get it if you think that it's so easy

6
00:00:26,960 --> 00:00:28,920
to make me a suit

7
00:00:28,920 --> 00:00:33,720
without measuring the curves that draw

8
00:00:33,720 --> 00:00:36,860
the contours of my being

9
00:00:36,860 --> 00:00:42,040
To go up stairs and to know to stop

10
00:00:42,040 --> 00:00:48,400
to measure the exact time to start kissing

11
00:00:48,400 --> 00:00:52,880
To take the right path and to take care of me

12
00:00:53,180 --> 00:00:58,260
Only a gentleman passes here

13
00:00:58,260 --> 00:01:03,280
To make it through requires its system

14
00:01:03,280 --> 00:01:05,400
so that, in this matter

15
00:01:05,400 --> 00:01:10,220
it's words of affection that open the secret formulas,

16
00:01:10,220 --> 00:01:12,560
zippers and so on...

17
00:01:12,700 --> 00:01:17,640
What do they put in love

18
00:01:17,640 --> 00:01:22,240
that it climbs upward, higher and higher,

19
00:01:22,240 --> 00:01:27,060
growing to the tempo of the movement

20
00:01:27,060 --> 00:01:31,940
marked by two

21
00:01:40,340 --> 00:01:45,420
There are sages who will think here,

22
00:01:45,560 --> 00:01:50,040
"mystical that this be in the world"

23
00:01:50,800 --> 00:01:55,360
but it's the body that's the condensation

24
00:01:56,020 --> 00:02:00,320
of drives, nurture and passion.

25
00:02:00,320 --> 00:02:05,820
It's your actions that form a presence

26
00:02:05,820 --> 00:02:07,960
and it's in your absence

27
00:02:08,300 --> 00:02:12,100
that I'm without the strength that's in

28
00:02:12,300 --> 00:02:16,000
a person whole and real

29
00:02:16,000 --> 00:02:20,280
What do they put in love

30
00:02:20,280 --> 00:02:24,800
that it climbs upward, higher and higher,

31
00:02:24,800 --> 00:02:29,680
growing to the tempo of the movement

32
00:02:29,680 --> 00:02:35,680
marked by two

33
00:02:41,640 --> 00:02:46,360
What do they put in love

34
00:02:46,360 --> 00:02:50,580
that it climbs upward, higher and higher,

35
00:02:50,740 --> 00:02:55,780
growing to the tempo of the movement

36
00:02:55,780 --> 00:03:01,700
marked by two

37
00:03:08,040 --> 00:03:12,280
What do they put in love

38
00:03:12,280 --> 00:03:17,280
that it climbs upward, higher and higher,

39
00:03:17,280 --> 00:03:28,000
to the tempo of the movement marked by two

40
00:00:00,000 --> 00:00:00,000
