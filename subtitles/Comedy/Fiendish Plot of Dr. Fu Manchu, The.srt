1
00:04:25,290 --> 00:04:27,490
What's going on, Hop?

2
00:04:28,291 --> 00:04:30,720
You back again.

3
00:04:35,082 --> 00:04:38,632
Oh sick.
Find a good one.

4
00:04:39,013 --> 00:04:42,025
Ten Tigers of Kwang Tung.
This is gold.

5
00:04:43,545 --> 00:04:47,927
Oh.. Bride with White Hair.
Chinese, no subtitles?

6
00:04:47,928 --> 00:04:49,924
Hop, do you have any
early Shaw Brothers?

7
00:04:49,925 --> 00:04:51,500
There's a guy who does leopard
style, I don't want...

8
00:04:51,501 --> 00:04:57,701
Leopard style, dragon style.
Fight to the air, fight on water.

9
00:04:57,802 --> 00:05:02,120
Crouching Tiger, Sprinting Monkey.

10
00:05:02,121 --> 00:05:06,208
I know you.  I'm not a white boy
who knows no kung fu.

11
00:05:06,209 --> 00:05:09,509
Kick their asses.
Get the girls.

12
00:05:53,402 --> 00:05:55,981
Only storage back here.

13
00:05:56,782 --> 00:05:58,779
Where did you get this?

14
00:05:58,780 --> 00:06:05,803
It was here when my grandfather
open the store hundred years ago.

15
00:06:07,065 --> 00:06:10,900
He waited for a man
to come and pick it up.

16
00:06:11,904 --> 00:06:14,986
Return to rightful owner.

17
00:06:15,404 --> 00:06:17,926
Man never came.

18
00:06:17,927 --> 00:06:22,786
My father took over the shop,
waited too, long time.

19
00:06:22,787 --> 00:06:24,782
Now me.

20
00:06:24,783 --> 00:06:26,659
That's a Northern Temple
style Bold Staff.

21
00:06:27,360 --> 00:06:30,210
Same kind the 13 monks used
to save the Tang Emperor.

22
00:06:30,387 --> 00:06:34,046
I have seen him before.
That exact one.

23
00:06:34,195 --> 00:06:37,174
I had dreams about it.
You know why?

24
00:06:37,364 --> 00:06:41,754
You watch too much Hong Kong movies.
Come, come on.

25
00:06:42,777 --> 00:06:46,494
I will give you five awesome
Bruce Lee movies.

26
00:06:46,895 --> 00:06:50,795
For a very 'sick' price.
You are the man, Hop.

27
00:07:02,729 --> 00:07:04,814
Jason.

28
00:07:04,915 --> 00:07:07,239
Hey.
What's up?

29
00:07:07,240 --> 00:07:09,417
Nothing much. Just,
you know, cruising.

30
00:07:09,418 --> 00:07:11,955
You went down to Chinatown.
What's up with that?

31
00:07:11,956 --> 00:07:15,134
Oh, Kung Fu.
Are you serious?

32
00:07:15,135 --> 00:07:17,801
Not supposed to talk about it.
Not under the Kung Fu code.

33
00:07:17,899 --> 00:07:21,416
Hey look. The new kid is
making time with the hoods.

34
00:07:21,686 --> 00:07:24,199
J boy, you still riding
this loser cruiser?

35
00:07:24,200 --> 00:07:26,432
You need to get a motor
for this shit.

36
00:07:26,741 --> 00:07:29,979
What do we have here?
Just some bootlegs. No big deal.

37
00:07:29,980 --> 00:07:34,019
Enter The Dragon.
The Bride with White Hair.

38
00:07:34,020 --> 00:07:36,230
Yes, it's actually very good.

39
00:07:37,501 --> 00:07:40,484
Come on, let him go.
He ain't a dog. He knows Kung Fu.

40
00:07:41,236 --> 00:07:45,641
What? You think you are Chinese?
You hanging out  with that Chink with a sitar.

41
00:07:46,332 --> 00:07:48,112
What's up with you?
Let's see some moves.

42
00:07:48,113 --> 00:07:52,528
Come on, come on. Let's see some
Kung Fu. Kung Fu master. Huh?

43
00:07:53,338 --> 00:07:55,572
Want some Tae Kwan Do?

44
00:07:59,945 --> 00:08:04,053
Hey Lupo. He is hanging out with
that old Chink. Cash his checks.

45
00:08:08,015 --> 00:08:10,381
You have ties with the old man?

46
00:08:13,630 --> 00:08:15,943
Shut up and do it.

47
00:08:19,909 --> 00:08:22,585
It's late Lenny.
He's probably gone home.

48
00:08:28,207 --> 00:08:30,325
He is not here.
Let's just go.

49
00:08:30,326 --> 00:08:37,656
Who's there? Kung Fu boy.
You're back for more good priced movies?

50
00:08:43,821 --> 00:08:48,506
I told you. Best deal in China town.
Come on in.

51
00:08:49,999 --> 00:08:54,200
Want some tea?
Flush those scum out.

52
00:08:55,599 --> 00:08:58,026
Yankees winning. Very bad.

53
00:09:01,989 --> 00:09:04,427
Who's with you?
Friends?

54
00:09:04,428 --> 00:09:06,823
Where do you keep the cash?

55
00:09:11,823 --> 00:09:14,125
I said, where do you keep
the cash?

56
00:09:14,126 --> 00:09:18,326
Come on old man!
Stay here.

57
00:09:35,806 --> 00:09:37,636
Found the Jack's box.

58
00:09:45,094 --> 00:09:47,526
Jesus, Lupo.
You shot him

59
00:09:47,527 --> 00:09:49,759
Take it.

60
00:09:49,760 --> 00:09:53,660
Must return to rightful owner.
Run.

61
00:09:55,820 --> 00:09:59,369
You see that Yo?
Did you see that?

62
00:10:09,423 --> 00:10:11,620
Beat him with that stick.

63
00:10:20,743 --> 00:10:23,164
Look, cool man. Just don't...
Did you see that?

64
00:10:23,165 --> 00:10:25,435
Calm down, Lupo. Hey, chill man,
come on. Put the gun down.

65
00:10:25,436 --> 00:10:29,257
Shut up! Maybe I pulled the trigger,
but we are all in this together.

66
00:10:29,427 --> 00:10:31,967
But this little cockroach,
he ain't one of us.

67
00:11:02,624 --> 00:11:06,736
Have some tea.
It'll help you to recover.

68
00:11:43,600 --> 00:11:46,303
Excuse me,
I don't know where I am.

69
00:11:46,504 --> 00:11:49,974
I don't know how I got here.
I remember falling...

70
00:11:50,475 --> 00:11:53,975
Old lady found you
lying in the river.

71
00:11:54,276 --> 00:11:57,576
We all thought you were dead.

72
00:12:16,677 --> 00:12:19,250
Let's go! Let's go!

73
00:12:19,351 --> 00:12:21,851
Running away?
Help me!

74
00:12:26,052 --> 00:12:28,552
Please let her go!

75
00:12:34,466 --> 00:12:37,201
Quick, go and grab hold
of that guy!

76
00:12:48,296 --> 00:12:50,843
Stop, stop!

77
00:12:51,939 --> 00:12:54,835
Drop what you are holding!

78
00:12:58,636 --> 00:13:01,136
Try to run and I'll kill you.
Come here!

79
00:13:07,937 --> 00:13:11,337
What is that you are holding?
Where are you from?

80
00:13:11,690 --> 00:13:14,390
This? You want this?

81
00:13:15,883 --> 00:13:18,426
Drop the staff if you want your life.

82
00:13:20,862 --> 00:13:24,060
Please Sirs, some money
to this poor drunkard.

83
00:13:24,061 --> 00:13:25,882
Get lost!

84
00:13:57,004 --> 00:14:00,788
Where did you get that staff?
I don't know what you're saying.

85
00:14:02,206 --> 00:14:06,117
Where did you find it?
I can't understand you.

86
00:14:08,657 --> 00:14:11,280
That is because you
are not listening.

87
00:14:12,791 --> 00:14:14,736
Behind you!

88
00:14:46,448 --> 00:14:48,773
I don't know where I am

89
00:14:48,813 --> 00:14:51,987
Or how I got here. And who are
those guys who wanted to kill me.

90
00:14:52,087 --> 00:14:54,380
Whatever you did
back there was sick.

91
00:14:54,381 --> 00:14:56,917
No, Drunken Fist.

92
00:14:57,518 --> 00:15:00,218
See Crook Kung Fu of the South.

93
00:15:00,477 --> 00:15:03,695
I am Lu Yang, travelling scholar.

94
00:15:03,996 --> 00:15:06,102
What land do you come from monk?

95
00:15:06,103 --> 00:15:09,230
I'm not a monk, I am Jason.

96
00:15:09,235 --> 00:15:11,898
Jason Tripitikas, from South Boston.

97
00:15:13,635 --> 00:15:19,271
Is this a dream?
No, where you came from is the dream.

98
00:15:19,399 --> 00:15:21,908
Through the gate of 'No-Gate'.

99
00:15:22,125 --> 00:15:24,650
Is that like a worm-hole or something?

100
00:15:24,651 --> 00:15:30,347
No, it means you're either a Zen Master
or you're carrying something very special.

101
00:15:30,755 --> 00:15:35,429
This?  - Was in a pawn shop
waiting for a man to pick it up, and ...

102
00:15:36,018 --> 00:15:38,612
return it to its rightful owner.

103
00:15:38,807 --> 00:15:40,795
What?

104
00:15:40,996 --> 00:15:45,196
It has long been fore-told,
that a seeker will come.

105
00:15:45,197 --> 00:15:50,838
To return the staff and end
the reign of the Jade Warlord.

106
00:15:50,839 --> 00:15:52,790
Return the staff to who?

107
00:15:53,221 --> 00:15:55,141
The Monkey King.

108
00:15:55,142 --> 00:15:59,330
Born of stone, on the mountain
of fruit and flowers.

109
00:16:00,059 --> 00:16:02,180
With his weapon in hand.

110
00:16:02,400 --> 00:16:07,420
His 'Chi' became like fire.
His stick fighting like magic.

111
00:16:07,481 --> 00:16:09,673
He defied the order of the land.

112
00:16:10,089 --> 00:16:13,401
But the Jade Army
could not defeat him.

113
00:16:33,180 --> 00:16:36,382
Monkey King crushed every
soldier sent to stop him.

114
00:16:36,383 --> 00:16:39,867
But with his magical staff,
he was unbeatable.

115
00:16:40,503 --> 00:16:44,565
Word of his disobedience travelled
within the Forbidden Kingdom.

116
00:16:44,648 --> 00:16:48,845
To the Five Elements Mountains,
land of the Immortals.

117
00:16:48,896 --> 00:16:54,178
Once every 500 years, the Jade
Emperor hosts the Peach Banquet.

118
00:16:54,405 --> 00:16:58,649
It is here that the Heavenly Ministers
gather to celebrate the longevity

119
00:16:58,833 --> 00:17:02,029
and drink the elixir of immortality.

120
00:17:13,759 --> 00:17:18,041
Into the banquet, the Monkey King
crashed un-invited.

121
00:17:21,888 --> 00:17:25,316
The Jade Emperor was enchanted
by the Monkey King

122
00:17:25,350 --> 00:17:28,758
But the Jade Warlord
was not amused.

123
00:17:28,759 --> 00:17:30,493
As Master of the Army,

124
00:17:30,494 --> 00:17:34,178
the Warlord demanded the
Monkey King bow down to him.

125
00:17:42,891 --> 00:17:45,994
He's a bit unrefined, that's all.

126
00:17:46,491 --> 00:17:51,238
Give the naughty monkey
a title and let him go.

127
00:17:54,372 --> 00:17:57,365
Satisfied all was well
in Heaven and Earth.

128
00:17:57,384 --> 00:18:01,589
The Supreme Emperor left to
begin his 500 years meditation.

129
00:18:01,690 --> 00:18:06,254
Leaving the Jade Warlord in charge
by mandate of Heaven.

130
00:18:08,534 --> 00:18:11,606
The Warlord challenged
the Monkey King to a duel.

131
00:18:12,701 --> 00:18:15,082
High above Five Elements Mountain.

132
00:18:15,083 --> 00:18:19,339
In the Warlord's Palace,
the battle of Immortals was fought.

133
00:18:19,340 --> 00:18:23,069
To prove once and for all,
whose skill was supreme.

134
00:20:55,341 --> 00:20:58,426
Most excellent stick fighting, Wukong.

135
00:20:58,427 --> 00:21:02,463
But without your weapon you
are nothing but a lonely hermit.

136
00:21:03,331 --> 00:21:09,457
No more weapons, no more 'Chi' magic.
Fist against fist.

137
00:21:10,442 --> 00:21:14,393
The Monkey King was too trusting
and believed the Warlord's words.

138
00:21:14,394 --> 00:21:16,926
Laying down his magic weapon.

139
00:21:23,289 --> 00:21:27,807
Realizing he was been tricked, the Monkey King
casted the staff out into the Middle Kingdom.

140
00:21:45,388 --> 00:21:50,597
Martial art is based
on deception, my friend.

141
00:21:56,390 --> 00:21:59,254
Being immortal the
Monkey King could not be killed.

142
00:21:59,255 --> 00:22:03,172
Only trapped in stone, where he
waits for the seeker from the prophecy,

143
00:22:03,173 --> 00:22:07,046
to return to him his great weapon
and finally free him.

144
00:22:17,304 --> 00:22:22,579
That's what I heard anyway,
a long time ago.

145
00:22:22,580 --> 00:22:24,445
How long has he been in prison?

146
00:22:24,511 --> 00:22:27,862
Five hundred years, give
or take a few decades.

147
00:22:28,381 --> 00:22:32,772
They say when the Monkey King is
freed the Jade Emperor will return.

148
00:22:34,253 --> 00:22:35,555
How do I get home?

149
00:22:35,556 --> 00:22:39,619
You must return the staff
to Five Elements Mountain.

150
00:22:39,650 --> 00:22:42,050
You must free the Monkey King.

151
00:22:42,051 --> 00:22:44,306
I can't free the Monkey King.
I got to get home.

152
00:22:44,307 --> 00:22:48,701
Innkeeper, more wine.
Don't you think you had one too many?

153
00:22:49,473 --> 00:22:54,382
Wine is my inspiration.
In some parts, I'm known as a poet.

154
00:22:54,465 --> 00:22:56,213
Cheers!

155
00:22:59,194 --> 00:23:01,845
In other parts,
I am known as the beggar.

156
00:23:14,141 --> 00:23:18,000
Old wife.

157
00:23:18,107 --> 00:23:20,754
Where did this come from?
From them.

158
00:23:21,731 --> 00:23:24,120
Officers, look what they're using to pay.
What is this?

159
00:23:24,121 --> 00:23:26,572
What do we do now?
How good is your Kung Fu?

160
00:23:28,136 --> 00:23:31,569
He who speaks, does not know.
He who knows, does not speak.

161
00:23:31,570 --> 00:23:34,170
Surely you are masterful.

162
00:23:40,142 --> 00:23:42,620
Where did you get that raben?
It's a fake.

163
00:23:42,621 --> 00:23:45,200
You can find anything these days
on the Silk Road.

164
00:23:45,201 --> 00:23:47,717
Out of the way, old fool.

165
00:23:49,718 --> 00:23:51,600
Hand it over.

166
00:23:53,300 --> 00:23:56,400
Hand over the raben
or die.

167
00:24:08,331 --> 00:24:10,088
Go!

168
00:24:42,025 --> 00:24:43,948
Help!

169
00:25:01,503 --> 00:25:04,500
Protect yourself.
What?

170
00:26:14,496 --> 00:26:16,298
Let's go.

171
00:26:58,500 --> 00:27:00,280
Thank you.

172
00:27:07,745 --> 00:27:10,980
Such a skilled child.
Are you from Northern Mountains?

173
00:27:10,981 --> 00:27:13,581
She's Golden Sparrow from the south.

174
00:28:10,805 --> 00:28:14,386
She does not think the Drunkard
Immortal needed his life saved.

175
00:28:15,387 --> 00:28:17,187
You're immortal?

176
00:28:17,388 --> 00:28:19,688
What did you run from child?

177
00:28:19,689 --> 00:28:23,900
Bounty hunters trying to stop her from
reaching the Five Elements Mountain.

178
00:28:29,830 --> 00:28:32,101
I suggest you keep riding west.

179
00:28:32,102 --> 00:28:34,502
And only stop to water your horses.

180
00:28:34,503 --> 00:28:37,230
You are not coming?
The journey to Five Elements Mountain,

181
00:28:37,231 --> 00:28:40,398
crosses barrens and desserts,
unspeakable dangers.

182
00:28:40,399 --> 00:28:41,899
Worst of all, no wine.

183
00:28:41,900 --> 00:28:44,553
His elixir.
Every Immortal has one.

184
00:28:44,954 --> 00:28:47,073
I am very sorry.
Without wine I will perish.

185
00:28:47,074 --> 00:28:48,574
You must understand this.

186
00:28:48,575 --> 00:28:51,431
No.
You must understand this.

187
00:28:51,632 --> 00:28:53,432
This is insane!

188
00:28:53,533 --> 00:28:56,105
Do you wish to get home?
Yes.

189
00:28:57,722 --> 00:29:00,006
Then listen well.
If you die here,

190
00:29:00,007 --> 00:29:02,489
you were be found dead
in the world you left behind.

191
00:29:02,490 --> 00:29:04,698
Do you understand?

192
00:29:06,199 --> 00:29:10,199
My jug is getting low.
I must say goodbye. Bye-bye.

193
00:29:18,600 --> 00:29:21,235
Lu... Wait!

194
00:29:21,636 --> 00:29:26,237
Let him go. You are the one
to return staff, not him.

195
00:29:26,638 --> 00:29:28,465
How good is your Kung Fu?

196
00:29:28,466 --> 00:29:31,345
He's got no Kung Fu.
None!

197
00:29:31,346 --> 00:29:33,146
Lu, wait!

198
00:29:33,247 --> 00:29:37,073
Teach me.
Teach me to fight.

199
00:29:45,774 --> 00:29:49,211
Swing soft, cut harder,
at the same time.

200
00:29:50,112 --> 00:29:52,257
He's terrible.

201
00:29:56,023 --> 00:29:58,358
I've been weed whacking
for two days now.

202
00:29:58,375 --> 00:30:01,007
While you sit on the horse
like the King of England.

203
00:30:01,008 --> 00:30:02,492
When are you going
to teach me Kung Fu.

204
00:30:02,493 --> 00:30:05,411
You want to learn Kung Fu?
Ya.

205
00:30:09,012 --> 00:30:11,564
I'll teach you Kung Fu.

206
00:30:13,808 --> 00:30:16,364
That's call strike.
Tomorrow, teach you block.

207
00:30:16,365 --> 00:30:18,288
Let's go.

208
00:30:18,537 --> 00:30:22,006
Swing soft, cut hard.

209
00:30:40,973 --> 00:30:43,704
You think you'll teach me
the no shadow kick?

210
00:30:45,014 --> 00:30:47,665
Oh, and the Buddha Palm Technique.

211
00:30:49,330 --> 00:30:53,500
There's a guy in Virtua Fighter 2
who does the Buddha Palm Technique.

212
00:30:54,401 --> 00:30:58,860
And he does the Iron Elbow.

213
00:30:58,918 --> 00:31:01,445
And he does the
One Finger Death Touch.

214
00:31:01,446 --> 00:31:04,146
The cup's full. Stop!
Stop, it's full.

215
00:31:05,100 --> 00:31:09,063
Exactly. How can you fill
your cup, if already full?

216
00:31:09,484 --> 00:31:13,200
How can you learn Kung Fu,
if already know so much?

217
00:31:13,537 --> 00:31:17,908
Know Shadow Kick, Buddha Palm.
Empty your cup.

218
00:31:22,970 --> 00:31:25,950
Hopeless.
It is hopeless.

219
00:31:46,172 --> 00:31:52,700
It is said that music is a bridge
between earth and heaven.

220
00:31:53,747 --> 00:31:55,463
It's beautiful.

221
00:32:01,586 --> 00:32:03,800
It belong to our mother.

222
00:32:45,837 --> 00:32:47,828
My Lord

223
00:32:47,839 --> 00:32:50,831
How dare you...
can't you see I am busy?

224
00:32:51,843 --> 00:32:54,835
The devine staff of legend...

225
00:32:54,846 --> 00:32:57,838
It was seen in the Middle Kingdom...

226
00:32:57,849 --> 00:32:58,838
Impossible.

227
00:32:59,851 --> 00:33:03,844
The villagers are beginning to
whisper of prophecy...

228
00:33:05,857 --> 00:33:08,849
Mortals are always whispering of prophecy.

229
00:33:09,861 --> 00:33:11,852
It's their opium.

230
00:33:15,867 --> 00:33:19,860
What other offending news
do you have for me?

231
00:33:21,873 --> 00:33:23,864
That is all, My Lord.

232
00:33:32,884 --> 00:33:35,876
Summon the Witch...
The one born of wolves.

233
00:33:45,897 --> 00:33:47,888
Where were we?

234
00:34:02,193 --> 00:34:03,895
Lu Yang.

235
00:34:06,359 --> 00:34:08,127
Lu Yang!

236
00:34:12,180 --> 00:34:14,046
Lu Yang!

237
00:34:15,326 --> 00:34:17,363
Lu Yang!

238
00:34:19,990 --> 00:34:22,153
Sparrow!

239
00:35:05,907 --> 00:35:09,450
Jason, what happened?
He took it. The staff!

240
00:35:11,051 --> 00:35:13,980
The Jade Warlord has sent
a bounty hunter.

241
00:35:13,984 --> 00:35:16,066
We are doomed.

242
00:35:26,682 --> 00:35:29,700
That's his horse.
He must be inside.

243
00:35:30,191 --> 00:35:32,530
Why would he want to take
refuge in a temple?

244
00:35:32,531 --> 00:35:36,300
That's what I intend to find out.
Wait with the horses.

245
00:36:05,285 --> 00:36:09,888
Ahh...good to get off my feet.
Long way.

246
00:36:11,400 --> 00:36:13,289
So, where are you from?

247
00:36:13,290 --> 00:36:15,139
Shandong province?

248
00:36:15,970 --> 00:36:18,457
You look like the Shandong province type.

249
00:36:18,618 --> 00:36:20,881
You come here often.

250
00:36:22,248 --> 00:36:24,944
The Staff doesn't belong to you.

251
00:36:27,560 --> 00:36:30,841
You have to give to me
or somebody will get hurt.

252
00:37:47,360 --> 00:37:49,994
What kind of monk are you?

253
00:37:50,103 --> 00:37:52,537
Stealing from travellers.

254
00:37:55,144 --> 00:37:57,507
A silent one I see.

255
00:37:58,143 --> 00:38:00,295
Or deaf.

256
00:38:01,229 --> 00:38:04,033
I'm speaking to you monk.

257
00:39:38,234 --> 00:39:42,452
Praying Mantis very good.
For catching bugs.

258
00:39:42,453 --> 00:39:44,853
But not Tiger!

259
00:41:36,233 --> 00:41:39,548
You try to steal the Staff for the Warlord.
No, fool!

260
00:41:42,052 --> 00:41:45,408
I am on a mission to find
the seeker of the Staff.

261
00:42:03,310 --> 00:42:05,638
You found him.

262
00:42:17,408 --> 00:42:19,399
He's not even Chinese.

263
00:42:19,410 --> 00:42:22,402
We are all the same inside,
Aren't we Monk?

264
00:42:22,413 --> 00:42:23,402
Heaven help us.

265
00:42:25,416 --> 00:42:27,407
What, you find it a sin?

266
00:42:27,418 --> 00:42:30,410
Yes, it is sinful not to share.

267
00:42:31,422 --> 00:42:32,411
Cheers!

268
00:43:17,974 --> 00:43:20,511
How look have you been
searching for the Staff?

269
00:43:20,512 --> 00:43:23,112
As long as I can remember.

270
00:43:24,475 --> 00:43:28,468
An old man and a boy,
They had tea here, Yes?

271
00:43:28,479 --> 00:43:31,471
A Soldier Monk rides with them
Who else?

272
00:43:32,483 --> 00:43:34,474
I do not know
who you speak of...

273
00:43:51,502 --> 00:43:53,493
Men are such liars.

274
00:44:07,518 --> 00:44:08,507
Sparrow...

275
00:44:09,520 --> 00:44:10,509
Sparrow...

276
00:44:12,523 --> 00:44:14,514
...where do you nest?

277
00:44:32,393 --> 00:44:34,302
Jason, go dipper.

278
00:44:34,303 --> 00:44:38,854
Must taste bitter before sweet.
Horse stance, grow roots.

279
00:44:53,068 --> 00:44:56,000
Very good.
For taking a duck.

280
00:44:56,897 --> 00:44:58,497
Hey!

281
00:44:58,798 --> 00:45:02,467
Enough from you.
He's my student. Not yours.

282
00:45:02,768 --> 00:45:05,268
Two tigers cannot live
in the same mountain.

283
00:45:05,269 --> 00:45:07,954
Two masters cannot teach
the same student.

284
00:45:10,229 --> 00:45:12,000
Horse Stance!

285
00:45:12,604 --> 00:45:15,139
If he is ready to learn
the Kung Fu,

286
00:45:15,140 --> 00:45:18,553
he must develop the speed,
accuracy and power.

287
00:45:18,754 --> 00:45:24,079
Hey! I know that. It's the way
of the Intercepting Fist. Bruce Lee.

288
00:45:30,846 --> 00:45:34,768
I will punch. You'll block.
Ready?

289
00:45:37,038 --> 00:45:40,419
Ahh! What did I teach you
about the Snake?

290
00:45:40,883 --> 00:45:43,302
Now. You punch me.

291
00:45:44,079 --> 00:45:46,414
Look. See.

292
00:45:48,442 --> 00:45:51,081
Again. You punch him again.
Snake!

293
00:45:53,786 --> 00:45:57,178
It's not right for Snake.
Perhaps Eagle.

294
00:45:57,247 --> 00:45:59,870
Crane, Crane.

295
00:46:00,376 --> 00:46:02,111
Okay?

296
00:46:12,695 --> 00:46:15,309
I had enough! No more!

297
00:46:15,310 --> 00:46:20,019
No more of silent riddles
and no more of tea cups.

298
00:46:24,295 --> 00:46:27,196
First you show respect
to your teachers.

299
00:46:29,129 --> 00:46:33,402
So, what about the two tigers
and one mountain?

300
00:46:33,803 --> 00:46:36,403
We can kill each other
when it is over.

301
00:46:46,190 --> 00:46:50,902
Kung Fu. Hard work over time
to accomplish skill.

302
00:46:55,278 --> 00:46:57,916
A painter can have Kung Fu.

303
00:46:57,917 --> 00:47:01,822
Or the butcher who cuts meat
every day with such skill.

304
00:47:01,823 --> 00:47:04,423
His knife never touches bone.

305
00:47:08,310 --> 00:47:12,127
Learn the form,
but seek from this.

306
00:47:12,220 --> 00:47:15,674
Hear the song list,
learn it all,

307
00:47:15,880 --> 00:47:18,080
And forget it all

308
00:47:18,081 --> 00:47:22,281
Learn the way,
define your own way.

309
00:47:22,907 --> 00:47:25,111
A musician can have Kung Fu.

310
00:47:25,112 --> 00:47:28,150
Or the poet who paints
pictures with words.

311
00:47:28,151 --> 00:47:32,292
And mix emperors with his tubes
is Kung Fu.

312
00:47:32,694 --> 00:47:34,800
But do not name it my friend,

313
00:47:34,801 --> 00:47:39,139
For it is like water.
Nothing is softer than water.

314
00:47:39,140 --> 00:47:43,503
Yet it can overcome rock.
It does not fight.

315
00:47:46,731 --> 00:47:51,430
Formless, nameless,
the true master dwells within.

316
00:47:51,431 --> 00:47:54,480
Only you can free it.

317
00:48:54,957 --> 00:48:57,696
Behold, the tyranny of the Warlord.

318
00:48:58,279 --> 00:49:00,260
He must be stopped.

319
00:49:00,261 --> 00:49:04,229
He must be killed for his crimes
and his head put on a post.

320
00:49:04,892 --> 00:49:08,679
But we must not feel hatred
towards him or he wins.

321
00:49:10,738 --> 00:49:16,607
If he speaks compassion for this devil,
he should go back to his temple and pray.

322
00:49:16,804 --> 00:49:20,598
Our mission is not one of peace.

323
00:49:20,599 --> 00:49:22,999
Go back to your mother and father.

324
00:49:23,000 --> 00:49:25,106
You are but a child.

325
00:49:25,207 --> 00:49:26,913
They are dead.

326
00:49:28,747 --> 00:49:31,667
And she is not a child.

327
00:49:32,679 --> 00:49:34,837
Not anymore.

328
00:49:36,310 --> 00:49:42,045
Her father was a government official
who opposed the Warlord.

329
00:49:44,699 --> 00:49:49,773
To set an example, the Warlord
dispatched his troop into night.

330
00:49:54,211 --> 00:49:58,353
Legion upon legion poured out
of the Five Elements Mountain.

331
00:49:58,354 --> 00:50:01,570
Into the low lands
of the Middle Kingdom.

332
00:50:03,037 --> 00:50:06,696
Screams of innocent people
hung in the night air.

333
00:50:09,108 --> 00:50:14,397
When it was over, charred roofs
were all that remained.

334
00:50:14,503 --> 00:50:19,528
That, and a lonely child
hidden in a well by her mother.

335
00:50:19,783 --> 00:50:22,652
Ruthlessly murdered by an arrow,

336
00:50:24,189 --> 00:50:27,096
from the Warlord's bow.

337
00:50:31,115 --> 00:50:35,497
When she reaches the Warlord's palace,

338
00:50:35,876 --> 00:50:39,293
she will not offer him forgiveness.

339
00:50:39,515 --> 00:50:45,914
Monk, she will offer him this.

340
00:50:50,609 --> 00:50:55,292
A jade dart that can kill any mortal.

341
00:50:55,836 --> 00:50:59,075
Long has she practiced.

342
00:51:02,813 --> 00:51:06,606
Vengeance has a way
of rebounding upon themselves.

343
00:51:11,838 --> 00:51:14,359
Master of Sensitivity.

344
00:51:24,955 --> 00:51:25,944
It is them.

345
00:51:25,956 --> 00:51:27,947
They're headed towards the desert.

346
00:51:27,958 --> 00:51:31,951
They are between me and the River of Sand.

347
00:51:31,962 --> 00:51:34,954
Isn't it just like King Monkey...

348
00:51:35,966 --> 00:51:38,958
...summoning a boy to do a man's job?

349
00:51:59,394 --> 00:52:04,397
This is not a dessert.
That is the dessert.

350
00:52:08,467 --> 00:52:12,253
I hope you know where
you are going, crazy monk.

351
00:53:26,128 --> 00:53:29,825
A monk of the mission.
And where does he lead us.

352
00:53:29,826 --> 00:53:33,063
Across the dessert to nowhere.

353
00:53:56,890 --> 00:53:59,854
We are not going to make it
are we?

354
00:54:02,598 --> 00:54:05,486
And even if we do,
we'll still have to face the Jade army.

355
00:54:08,265 --> 00:54:10,807
What if I can't handle it?

356
00:54:15,732 --> 00:54:18,352
What if I freeze?

357
00:54:22,153 --> 00:54:24,912
Don't forget to breath.

358
00:54:56,161 --> 00:55:00,162
If we don't find water soon,
we will perish.

359
00:55:02,096 --> 00:55:06,753
Perhaps a Taoist immortal
can make rain.

360
00:55:07,806 --> 00:55:11,741
If he truly is a Taoist immortal.

361
00:56:33,395 --> 00:56:37,069
You are an insult to the name
of Buddha. Blasphemy.

362
00:56:40,070 --> 00:56:42,070
Look!

363
00:56:42,760 --> 00:56:45,370
I lead you to the Mountain.

364
00:56:46,371 --> 00:56:48,497
Over there!

365
00:57:54,442 --> 00:57:56,458
Can you see them?

366
00:57:56,459 --> 00:57:58,322
In the clouds.

367
00:57:58,512 --> 00:58:02,946
A two headed lion, right there.

368
00:58:03,109 --> 00:58:06,670
See.
Yes, I see it.

369
00:58:08,260 --> 00:58:12,611
See that one right there coming up
behind it like a wave.

370
00:58:14,095 --> 00:58:18,229
Looks like a green monster.
Green monster?

371
00:58:18,271 --> 00:58:20,039
You mean dragon.

372
00:58:20,348 --> 00:58:22,559
No, I mean family park.

373
00:58:22,960 --> 00:58:25,558
They call the left field fence,
the green monster.

374
00:58:26,161 --> 00:58:28,469
It's true.

375
00:58:30,367 --> 00:58:33,298
It does look like a dragon
doesn't it?

376
00:58:37,147 --> 00:58:41,538
Have you any family, back
in this land you come from?

377
00:58:42,849 --> 00:58:46,509
My mother.
Father?

378
00:58:47,477 --> 00:58:50,017
Never knew him.

379
00:58:50,767 --> 00:58:54,204
Do you ever long for him?

380
00:58:54,505 --> 00:58:56,605
Wonder who he was?

381
00:58:59,458 --> 00:59:05,998
Maybe the only thing I've ever been
good at is pretending that I don't.

382
00:59:08,480 --> 00:59:10,743
She's sorry.

383
00:59:14,114 --> 00:59:16,683
How romantic.

384
00:59:18,094 --> 00:59:21,412
Men will tell you what
they want to here, Sparrow.

385
00:59:21,513 --> 00:59:25,964
But in the end they
will leave you with nothing.

386
00:59:28,926 --> 00:59:32,635
You survived the river of sand.
Impressive.

387
00:59:32,756 --> 00:59:36,643
And you boy, so far from home.

388
00:59:36,737 --> 00:59:39,187
I can ensure your safe return.

389
00:59:39,188 --> 00:59:41,432
Just bring the weapon forward.

390
00:59:41,433 --> 00:59:43,371
I don't think so.

391
00:59:43,556 --> 00:59:45,614
Why do you want the staff?

392
00:59:46,415 --> 00:59:48,353
When I deliver the lost weapon,

393
00:59:48,454 --> 00:59:53,186
the Jade Warlord shall grant
me the elixir of immortality.

394
00:59:54,470 --> 00:59:58,597
An orphan girl, a lost traveller,
an old drunkard,

395
00:59:58,598 --> 01:00:03,938
and a monk who has failed
at the same task for half his life.

396
01:00:04,200 --> 01:00:09,614
Misfits following a misfit
in hope of rescuing a misfit.

397
01:00:29,773 --> 01:00:32,192
Kill them.

398
01:02:25,312 --> 01:02:29,949
Lu, you okay?
Just thirsty.

399
01:02:33,304 --> 01:02:35,077
Lu!

400
01:02:51,019 --> 01:02:54,500
That's okay. He's immortal, right?
I mean he is, right?

401
01:02:54,501 --> 01:02:57,555
Wine. He needs his wine.
There is nothing we can do.

402
01:02:57,556 --> 01:03:00,127
What are you saying?
We have to do something.

373
01:03:31,981 --> 01:03:37,514
I'm afraid the wound is too deep.
He will not survive it.

374
01:03:38,385 --> 01:03:42,703
He needs his wine.
He is one of the eight immortals.

375
01:03:42,804 --> 01:03:45,420
Wine is his elixir.
Please.

376
01:03:46,754 --> 01:03:52,304
We will send our walking monk.
Don't have a running monk?

377
01:04:13,070 --> 01:04:15,309
When I was your age.

378
01:04:15,542 --> 01:04:18,483
I was a scholar warrior in training.

379
01:04:19,310 --> 01:04:22,583
My arrow was good,
so too my Kung Fu.

380
01:04:23,707 --> 01:04:28,583
So I was chosen to take
the several examinations.

381
01:04:29,542 --> 01:04:37,232
To pass would place me in a very
short line of scholar immortals.

382
01:04:39,198 --> 01:04:44,382
I failed.
You're not immortal?

383
01:04:46,780 --> 01:04:51,933
If one does not attach himself
to people and desire,

384
01:04:53,880 --> 01:04:57,176
never shall his heart be broken.

385
01:04:58,319 --> 01:05:04,593
But then,
does he ever truly live?

386
01:05:06,134 --> 01:05:12,381
I would rather die a mortal
who has a care for someone.

387
01:05:13,776 --> 01:05:18,497
Than a man free
from his own death.

388
01:05:19,600 --> 01:05:23,465
I don't want to lose you.

389
01:05:24,503 --> 01:05:26,965
Forget about me.

390
01:05:28,962 --> 01:05:31,901
There is only one Elixir
of Immortality.

391
01:05:32,002 --> 01:05:37,927
It is the Emperor's mead of jade,
cinnabar and salts of mercury.

392
01:05:38,195 --> 01:05:42,489
It is forever stored in the realm
of the jade palace.

393
01:05:42,973 --> 01:05:47,662
High in the jade ferment.
Rarely touched by mortals.

394
01:05:47,829 --> 01:05:53,331
To reach it, one must take North Road
and cross the border of heaven and earth.

395
01:05:54,256 --> 01:05:58,096
Only to face the Jade Army.

396
01:05:59,147 --> 01:06:03,545
I'm sorry. There is no other way.

397
01:06:04,052 --> 01:06:06,476
Yes, there is.

398
01:06:07,067 --> 01:06:09,608
In two nights there should be no moon.

399
01:06:09,609 --> 01:06:12,497
We can take the south side of the fortress.

400
01:06:12,598 --> 01:06:15,199
Under cover of darkness.

401
01:06:15,200 --> 01:06:17,625
We have to go now.
No!

402
01:06:20,274 --> 01:06:22,598
We go now, we all die.

403
01:06:26,471 --> 01:06:29,730
There is more at stake.
An entire kingdom.

404
01:06:33,206 --> 01:06:39,545
You are our best hope.
We will advance in two nights.

405
01:06:39,546 --> 01:06:43,280
When the moon has ceased.
So be it!

406
01:07:27,890 --> 01:07:33,740
I should've met you long
ago in a tea house.

407
01:07:33,744 --> 01:07:41,990
She was lost and frightened.
Not the kind she would stand and fight with.

408
01:09:32,454 --> 01:09:35,900
You have made the most
excellent judgment.

409
01:09:51,980 --> 01:09:57,260
The seeker from the prophecy,
not quite what I'd expected.

410
01:09:57,446 --> 01:10:03,470
A man is dying back on Song Mountain.
I need the Elixir.

411
01:10:03,685 --> 01:10:06,737
Why should I give it to you?

412
01:10:06,738 --> 01:10:09,128
Because I brought you the Staff.

413
01:10:09,170 --> 01:10:12,978
The life of your friend over the
power to rule over the Kingdom?

414
01:10:13,319 --> 01:10:15,896
A most reasonable offer.

415
01:10:16,163 --> 01:10:21,559
This man, a good friend?
And a good teacher.

416
01:10:21,624 --> 01:10:25,372
The man who honours his teacher,
honours himself.

417
01:10:25,756 --> 01:10:30,966
However, there is a little bit
problem with your request.

418
01:10:31,141 --> 01:10:36,865
You see, I promised the Elixir
to someone else.

419
01:10:36,878 --> 01:10:40,252
She didn't bring you the Staff.
I did.

420
01:10:40,369 --> 01:10:42,889
The boy has a point.

421
01:10:44,856 --> 01:10:47,927
My Leech, you made a promised.

422
01:10:48,250 --> 01:10:51,985
There is but one way
to resolve such matters.

423
01:10:52,252 --> 01:10:55,868
A martial challenge to the death.

424
01:10:57,718 --> 01:11:00,056
With pleasure.

425
01:11:15,409 --> 01:11:18,697
The Elixir of Immortality

426
01:11:21,381 --> 01:11:25,390
The prize goes to the winner.

427
01:13:44,585 --> 01:13:46,530
Enough!

428
01:14:00,772 --> 01:14:05,858
The seeker from the prophecy.
I find that quite amusing.

429
01:14:06,598 --> 01:14:10,586
Do you really think even for
a minute, you stood a chance?

430
01:14:13,478 --> 01:14:16,006
I didn't think so.

431
01:15:10,712 --> 01:15:16,028
She will kill you witch.
Not if I kill you first, orphan bitch.

432
01:17:59,987 --> 01:18:01,976
Cheers!

433
01:18:19,441 --> 01:18:21,682
Remember what I taught you.

434
01:19:16,474 --> 01:19:18,389
The Statue!

435
01:19:22,387 --> 01:19:24,445
Use the Staff.

436
01:21:02,911 --> 01:21:04,972
My turn.

437
01:22:43,754 --> 01:22:46,990
My Lord.
Who are you?

438
01:22:46,991 --> 01:22:50,549
The youngest daughter
of the family you murdered.

439
01:22:51,999 --> 01:22:54,611
Come drink with me.

440
01:23:13,264 --> 01:23:15,146
No

441
01:23:18,289 --> 01:23:20,841
Sparrow, Sparrow

442
01:23:21,542 --> 01:23:25,875
Come on, come on.
You're going to be okay.

443
01:24:53,136 --> 01:24:57,419
Jade Warlord?
He's dead.

444
01:25:03,916 --> 01:25:05,959
I...

445
01:25:08,165 --> 01:25:10,738
I thank you.

446
01:26:01,812 --> 01:26:04,150
Is there anything you can do?

447
01:26:05,560 --> 01:26:09,406
Her Destiny was written
by her own hand.

448
01:26:10,450 --> 01:26:12,972
I am sorry.

449
01:26:44,990 --> 01:26:47,063
Traveller.

450
01:26:47,264 --> 01:26:53,036
You have come far through the gate of
No-Gate to fulfill the prophecy of the mortals.

451
01:26:53,284 --> 01:26:59,217
What do you desire, please?
I just want to go home.

452
01:27:00,896 --> 01:27:03,347
Very well.

453
01:27:07,418 --> 01:27:12,436
It is said, master and student,
walk their path side by side

454
01:27:12,437 --> 01:27:16,212
to share their destiny
until the paths go separate ways.

455
01:27:22,411 --> 01:27:27,822
I will never forget you.
I suggest that is what immortal truly means.

456
01:27:32,721 --> 01:27:38,684
You have freed me traveller.
Now, go free yourself.

457
01:29:19,542 --> 01:29:22,117
So, what is it going to be?

458
01:29:26,557 --> 01:29:28,616
You're going to shut up?

459
01:29:35,495 --> 01:29:38,786
Listen, piss end,
I'm not going to ask you again.

460
01:29:50,997 --> 01:29:53,590
Come on Maggie, get up, come on.

461
01:29:54,267 --> 01:29:57,395
You want some more?
You want some more?

462
01:29:57,623 --> 01:29:59,568
Come on.

463
01:30:35,236 --> 01:30:37,344
You don't have to do this.

464
01:31:10,399 --> 01:31:13,070
Take it easy, Sir.
You are going to be fine.

465
01:31:13,071 --> 01:31:16,000
Hop.
How is he doing?

466
01:31:16,001 --> 01:31:18,700
Missed his heart.
He will live.

467
01:31:20,055 --> 01:31:24,128
Of course, I'll live.
I am immortal.

468
01:31:26,005 --> 01:31:29,542
Did you return the Staff
to rightful owner?

469
01:31:30,206 --> 01:31:32,108
Yes, I did.

470
01:31:32,673 --> 01:31:35,294
You are the man.

471
01:32:12,050 --> 01:32:18,256
Is he going to be alright?
Looks like it.

472
01:32:18,457 --> 01:32:21,946
I saw what happened.
I walked across the street.

473
01:32:22,215 --> 01:32:24,787
You were very brave.

474
01:32:26,573 --> 01:32:28,709
See you.
Ya.

475
01:32:31,242 --> 01:32:33,109
Absolutely.

476
01:32:46,814 --> 01:32:48,565
As the legend just told,

477
01:32:48,566 --> 01:32:53,642
that the Monkey King began
his journey west, in search of truth.

478
01:32:53,843 --> 01:32:56,305
While the traveller
return to his world,

479
01:32:56,306 --> 01:33:01,092
to walk the path of the warrior
and find his own truth.

480
01:33:01,093 --> 01:33:06,300
As one tale ends,
so another begins.

