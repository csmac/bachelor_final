1
00:00:00,000 --> 00:00:02,724
<font color="#3399CC">Subtitles by </font><font color="ffffff">MemoryOnSmells</font>
<font color="#3399CC">http://UKsubtitles.ru.</font>

2
00:00:21,539 --> 00:00:25,179
Oh, I'm so glad you enjoyed your
cruise, Mother.

3
00:00:26,339 --> 00:00:29,219
Yes, you told me all about the
islanders.

4
00:00:30,859 --> 00:00:32,859
Please stop calling them that.

5
00:00:34,219 --> 00:00:37,139
Because we don't use that word any
more.

6
00:00:38,179 --> 00:00:41,339
I'd love to come and see you, but
I've got a cold.

7
00:00:41,379 --> 00:00:43,379
Atchoo! See?

8
00:00:43,419 --> 00:00:46,379
Yea... Actually, I should lie down.

9
00:00:46,419 --> 00:00:48,939
Really, I must be going, I'm very
ill.

10
00:00:48,979 --> 00:00:51,899
I'm hanging up, dear. Stop talking.

11
00:00:53,219 --> 00:00:55,219
Good lord, was that your mother?

12
00:00:55,259 --> 00:00:57,939
Don't tell me she managed to make it
back alive again?

13
00:00:57,979 --> 00:01:01,499
Next time, you should send her
somewhere really exotic,

14
00:01:01,539 --> 00:01:03,419
smother her with a pillow.

15
00:01:03,459 --> 00:01:06,019
How dare you talk about her like
that?

16
00:01:06,059 --> 00:01:10,259
Oh, please! All that money and the
nicest present she ever gave us

17
00:01:10,299 --> 00:01:12,579
was breaking her leg last Christmas.

18
00:01:12,619 --> 00:01:15,459
Well, I never spoke poorly of your
mother.

19
00:01:15,499 --> 00:01:17,699
Well, that's because my mother had
the decency

20
00:01:17,739 --> 00:01:20,099
to know the appropriate time to die.

21
00:01:20,139 --> 00:01:23,379
A characteristic you both don't
share, unfortunately,

22
00:01:23,419 --> 00:01:26,099
although the way you look this
morning,

23
00:01:26,139 --> 00:01:29,259
I'll be surprised if you make it to
teatime. Ooh, bugger.

24
00:01:29,299 --> 00:01:30,899
I've got to look my best.

25
00:01:30,939 --> 00:01:34,219
I've got this fan club screening of
my Doctor Who episode, this week.

26
00:01:34,259 --> 00:01:35,899
What can I do to look younger?

27
00:01:36,939 --> 00:01:38,939
Not go?

28
00:01:38,979 --> 00:01:40,979
You'd like that, wouldn't you?

29
00:01:41,019 --> 00:01:43,299
You've always been jealous of my
success.

30
00:01:43,339 --> 00:01:45,339
It is sad.

31
00:01:45,379 --> 00:01:47,019
I feel sad for you.

32
00:01:47,059 --> 00:01:50,139
You're a poor, sad, invisible,
little man.

33
00:01:52,899 --> 00:01:55,419
I think one of your teeth just fell
out.

34
00:01:56,459 --> 00:01:58,579
I'll get it.

35
00:01:58,619 --> 00:02:00,699
Any time I'm walking away from you,

36
00:02:00,739 --> 00:02:03,419
I can feel the blood start returning
to my veins.

37
00:02:04,939 --> 00:02:06,739
Hello, Ash.

38
00:02:06,779 --> 00:02:08,779
It's Ash.

39
00:02:08,819 --> 00:02:11,779
Our new neighbour.
I hope it's not too early.

40
00:02:11,819 --> 00:02:16,019
Of course not. Freddie, don't stand
so close to the boy.

41
00:02:16,059 --> 00:02:19,339
His breath is nothing to be alarmed
about, Ash,

42
00:02:19,379 --> 00:02:21,739
it's just his insides decaying.

43
00:02:22,779 --> 00:02:24,499
Would you like a cup of tea?

44
00:02:24,539 --> 00:02:26,539
That'd be great, thanks. Ah.

45
00:02:26,579 --> 00:02:30,179
I've got a bit of a problem. Ah?

46
00:02:30,219 --> 00:02:33,699
And... I don't have anyone else to
Mm.

47
00:02:35,339 --> 00:02:38,659
You remember that girl from back
home I told you I was seeing?

48
00:02:38,699 --> 00:02:40,699
Tracey? Eurgh! Her!

49
00:02:40,739 --> 00:02:42,739
Stupid name.

50
00:02:42,779 --> 00:02:45,219
Yes, of course we remember. How can
we help?

51
00:02:45,259 --> 00:02:47,259
She says she doesn't want to see me
any more,

52
00:02:47,299 --> 00:02:49,499
but I really like her
and I don't know what to do.

53
00:02:49,539 --> 00:02:51,539
She sounds like a complete idiot.

54
00:02:51,579 --> 00:02:53,579
Good riddance, I say.

55
00:02:53,619 --> 00:02:55,619
I never seem to know what girls
want.

56
00:02:57,699 --> 00:03:00,979
Women just want somebody
to make them feel special.

57
00:03:01,019 --> 00:03:03,699
A little kindness goes a long way.

58
00:03:04,739 --> 00:03:06,979
Oh, for Christ's sake, Violet!

59
00:03:07,019 --> 00:03:09,339
Why don't you ever call first?

60
00:03:10,739 --> 00:03:14,259
Er, Ash, you remember our friend,
Violet?

61
00:03:14,299 --> 00:03:17,179
Hi. So nice to see you again, Ash.

62
00:03:17,219 --> 00:03:19,939
Does it make you uncomfortable if I
tell you

63
00:03:19,979 --> 00:03:24,659
the musculature of your chest
reminds me of a cousin from my
youth?

64
00:03:28,139 --> 00:03:30,139
I-It makes me feel
very uncomfortable.

65
00:03:30,179 --> 00:03:32,619
I love how playful you are.

66
00:03:33,979 --> 00:03:38,539
Ash was just telling us about this
drug-addicted slag who's broken his
heart.

67
00:03:40,459 --> 00:03:42,459
She's actually a nurse.

68
00:03:42,499 --> 00:03:45,579
Well, maybe you should try dating
someone a bit older.

69
00:03:46,619 --> 00:03:49,219
Violet, stop turning your head to
flirt.

70
00:03:49,259 --> 00:03:51,059
Your neck keeps cracking.

71
00:03:52,219 --> 00:03:54,939
Ash was asking for advice about
women.

72
00:03:54,979 --> 00:03:56,979
Then, why did he come here?

73
00:03:58,059 --> 00:04:00,179
We don't like those kinds of jokes.

74
00:04:01,299 --> 00:04:03,219
Look, Ash, if this is a girl that
you like,

75
00:04:03,259 --> 00:04:05,099
you should just tell her how you
feel.

76
00:04:05,139 --> 00:04:07,299
Does that really work? Of course.

77
00:04:07,339 --> 00:04:11,299
You need to be direct. Women are
less forthcoming than men.

78
00:04:12,339 --> 00:04:14,339
Is that cotton?

79
00:04:15,859 --> 00:04:17,859
Why are you here, Violet?

80
00:04:17,899 --> 00:04:21,259
Oh, I suppose I thought we could
all spend the day together.

81
00:04:24,379 --> 00:04:27,219
We can't. We're shopping today.

82
00:04:27,259 --> 00:04:31,139
Freddie needs a new coat for his
Doctor Who screening. Ooh!

83
00:04:31,179 --> 00:04:36,139
Apparently, I have been voted the
tenth most popular villain of all
time.

84
00:04:37,979 --> 00:04:40,619
Does something like that even mean
anything?

85
00:04:40,659 --> 00:04:42,739
Er, I dunno.

86
00:04:42,779 --> 00:04:44,779
Of course it means something!

87
00:04:44,819 --> 00:04:48,019
And I don't need a new coat. The one
I've got is perfectly fine.

88
00:04:48,059 --> 00:04:50,419
It's not perfectly fine, you've had
it for ages.

89
00:04:50,459 --> 00:04:52,459
All right, stop spitting.

90
00:04:53,579 --> 00:04:56,539
So which villain did you play?

91
00:04:56,579 --> 00:04:59,699
I don't want to talk about it any
more. Everybody go home!

92
00:05:03,859 --> 00:05:06,219
Why is it all so bright and garish?

93
00:05:06,259 --> 00:05:09,139
It's like walking into Elton John's
dressing room.

94
00:05:09,179 --> 00:05:11,299
I don't like it here. Let's go.

95
00:05:11,339 --> 00:05:13,259
Nonsense, we're getting you a new
coat.

96
00:05:13,299 --> 00:05:16,019
Well, why do I need a new coat?
I just got this, last year.

97
00:05:16,059 --> 00:05:18,259
You did not get that last year.

98
00:05:18,299 --> 00:05:20,299
Well, recently.

99
00:05:20,339 --> 00:05:22,739
Look, here's a ticket stub for...

100
00:05:22,779 --> 00:05:24,779
Lawrence Of Arabia.

101
00:05:24,819 --> 00:05:27,419
Mm, all right, let's go. No. We're
not leaving. Oh...

102
00:05:30,059 --> 00:05:32,979
Well, maybe it was a little longer
than I remembered.

103
00:05:33,019 --> 00:05:35,859
Ah, now, what about this one?

104
00:05:35,899 --> 00:05:39,899
Oh, I like that. Oh, that would be
perfect for the screening.

105
00:05:39,939 --> 00:05:42,139
Everybody who's anybody's
going to be there.

106
00:05:42,179 --> 00:05:44,539
Who exactly? Doctor Who.

107
00:05:44,579 --> 00:05:46,979
Oh, God, has your memory gone now,
too?

108
00:05:48,179 --> 00:05:50,179
Oh, look, that can't be right.

109
00:05:50,219 --> 00:05:52,819
Excuse me. Excuse me!

110
00:05:52,859 --> 00:05:54,539
Arsehole.

111
00:05:57,379 --> 00:05:59,059
They make me nervous,

112
00:05:59,099 --> 00:06:01,779
all these young people skittering
about like mice,

113
00:06:01,819 --> 00:06:04,099
desperate to get back onto the
Internet.

114
00:06:07,059 --> 00:06:08,659
Let's just pay for it and go. No.

115
00:06:08,699 --> 00:06:12,619
Actually, I think we can probably
find something better elsewhere.

116
00:06:12,659 --> 00:06:14,659
No, let's go. Oh!

117
00:06:16,299 --> 00:06:18,299
Actually, that works.

118
00:06:21,459 --> 00:06:24,739
You know, when we filmed that Doctor
Who episode,

119
00:06:24,779 --> 00:06:26,779
I never dreamt it would become

120
00:06:26,819 --> 00:06:29,379
one of the most iconic roles of my
career.

121
00:06:29,419 --> 00:06:31,419
Or only.

122
00:06:33,219 --> 00:06:35,939
Well, I think it's quite exciting,
Freddie.

123
00:06:35,979 --> 00:06:37,579
Thank you, Penelope.

124
00:06:37,619 --> 00:06:41,219
Now, is this a television programme
we're talking about, or a book?

125
00:06:43,179 --> 00:06:45,179
It's a television programme. Ah.

126
00:06:45,219 --> 00:06:48,219
What's the matter with you?
How could it be a book?

127
00:06:49,299 --> 00:06:51,619
I'm sorry,
I'm only half-paying attention,

128
00:06:51,659 --> 00:06:54,339
but I got a lot right before this.

129
00:06:55,739 --> 00:06:57,739
You're doing fine, darling.

130
00:06:58,779 --> 00:07:01,619
Freddie's the actor, right,
or is it the other one?

131
00:07:01,659 --> 00:07:03,459
Stuart.

132
00:07:03,499 --> 00:07:07,899
My name is Stuart and we've known
each other for 50 years.

133
00:07:07,939 --> 00:07:11,539
Of course I know your name
S... Stuart.

134
00:07:12,859 --> 00:07:15,539
You're all invited to the screening.
Ooh.

135
00:07:15,579 --> 00:07:17,579
Except you.

136
00:07:17,619 --> 00:07:19,619
It's going to be quite the evening.

137
00:07:19,659 --> 00:07:21,779
Will there be a lot of single men?

138
00:07:21,819 --> 00:07:24,299
It's a science fiction fan club
event.

139
00:07:26,139 --> 00:07:28,899
They'll be single, but they'll be
disgusting.

140
00:07:28,939 --> 00:07:32,299
I have broad tastes, I am over 40.

141
00:07:33,339 --> 00:07:36,699
Oh, are you? Because I thought you
were still 39.

142
00:07:36,739 --> 00:07:39,059
Ooh.

143
00:07:39,099 --> 00:07:42,059
It is rather stressful,
coming here.

144
00:07:44,139 --> 00:07:46,139
Oh, Ash, come in.

145
00:07:46,179 --> 00:07:50,259
Ah. You remember our friend Violet?

146
00:07:50,299 --> 00:07:53,219
And Penelope, of course? Hi.

147
00:07:53,259 --> 00:07:56,579
Stuart, don't just stand there
like a lump, get Ash a drink.

148
00:07:56,619 --> 00:07:58,379
I haven't been offered a drink.

149
00:07:58,419 --> 00:08:00,499
Then get up and get one.

150
00:08:00,539 --> 00:08:02,499
I'll get you a drink, Penelope.

151
00:08:02,539 --> 00:08:04,739
Thank you, S... Stuart?

152
00:08:06,179 --> 00:08:10,059
So, what is it we can do for you,
Ash?

153
00:08:10,099 --> 00:08:13,859
More problems with the ladies?
I'm afraid so.

154
00:08:13,899 --> 00:08:15,899
They are a lot of work.

155
00:08:15,939 --> 00:08:18,619
Men, on the other hand, are simpler.

156
00:08:19,659 --> 00:08:22,979
Take Stuart and me, it's just
fun, fun, fun, all the time.

157
00:08:23,019 --> 00:08:24,899
Isn't that right, Stuart? Yes.

158
00:08:24,939 --> 00:08:27,139
It's like living in Disneyland.

159
00:08:28,299 --> 00:08:30,899
Well, I'm not a lot of work, Ash.

160
00:08:31,939 --> 00:08:33,939
Do you understand what I'm saying?

161
00:08:38,459 --> 00:08:40,459
I hope not.

162
00:08:41,899 --> 00:08:44,379
I just wish I knew
how to win Tracey back.

163
00:08:44,419 --> 00:08:46,459
Didn't you tell her how you felt?

164
00:08:46,499 --> 00:08:48,779
Yes, but she still doesn't want to
see me any more.

165
00:08:48,819 --> 00:08:51,859
What about a gift? Penelope and I
could take you shopping.

166
00:08:51,899 --> 00:08:54,659
That's a great idea. Stuart, what
about some lunch?

167
00:08:54,699 --> 00:08:57,739
No, I can't.
I've... got an appointment.

168
00:08:57,779 --> 00:09:01,019
I'll be gone all day.
Best of luck, Ash.

169
00:09:01,059 --> 00:09:04,459
Where the hell are you going?
I told you, I have an appointment.

170
00:09:04,499 --> 00:09:08,379
If you die before I come home,
I'll say goodbye now.

171
00:09:14,179 --> 00:09:16,179
Are all these girls prostitutes?

172
00:09:18,699 --> 00:09:21,219
No, that's just how they dress. Oh.

173
00:09:21,259 --> 00:09:23,739
Everyone's very proud of their
vaginas now.

174
00:09:25,859 --> 00:09:28,459
In our day,
you never discussed such things.

175
00:09:28,499 --> 00:09:30,499
I've never even seen mine.

176
00:09:35,179 --> 00:09:38,299
Why are we looking for a gift for
Tracey in the men's department?

177
00:09:38,339 --> 00:09:40,819
I thought I might get you
a little something first.

178
00:09:40,859 --> 00:09:44,019
Perhaps a T-shirt, or swimming
trunks...

179
00:09:45,179 --> 00:09:46,779
Oh, I don't know.

180
00:09:46,819 --> 00:09:49,979
Well, we won't know until you start
trying them on, will we?

181
00:09:51,019 --> 00:09:52,619
I like this one.

182
00:09:53,699 --> 00:09:55,699
I'm not sure. Just try it on.

183
00:09:55,739 --> 00:09:57,899
Oh, Violet, look. What is it?

184
00:09:57,939 --> 00:10:01,699
Isn't that... that... Stuart... I was
getting there.

185
00:10:02,819 --> 00:10:05,819
What's he doing, talking to that
man? I've no idea.

186
00:10:05,859 --> 00:10:07,659
Looks like he's flirting.

187
00:10:07,699 --> 00:10:09,819
We'd better go, I don't want him to
see us,

188
00:10:09,859 --> 00:10:11,659
and with such a younger man, too.

189
00:10:11,699 --> 00:10:13,699
Where's his self-respect?

190
00:10:13,739 --> 00:10:15,739
It's way too small. Oh.

191
00:10:15,779 --> 00:10:18,859
Well, let's just get a quick photo,
so we can be certain.

192
00:10:22,299 --> 00:10:27,139
Oh, I just checked on the dog. He's
looking really splendid for 20.

193
00:10:27,179 --> 00:10:30,419
I think I saw his tail move.

194
00:10:30,459 --> 00:10:32,779
He's having a good day. Yes.

195
00:10:32,819 --> 00:10:35,059
Where are you going, all tarted up?

196
00:10:35,099 --> 00:10:37,179
I told you, I'm going to see my
mother.

197
00:10:37,219 --> 00:10:39,219
Tell her I said hello,

198
00:10:39,259 --> 00:10:41,419
and to walk toward the light.

199
00:10:42,779 --> 00:10:45,379
I don't know why you have such a
problem with my mother.

200
00:10:45,419 --> 00:10:48,979
She's always been kind to you.
She sends you birthday cards.

201
00:10:49,019 --> 00:10:52,259
One card on my 30th birthday
and she signed it,

202
00:10:52,299 --> 00:10:54,379
'I think it's time
you found a wife.'

203
00:10:54,419 --> 00:10:57,059
Still, it was thoughtful.

204
00:10:57,099 --> 00:11:00,099
Now, make sure you take Balthazar
outside

205
00:11:00,139 --> 00:11:02,459
and hold him up while he does his
business,

206
00:11:02,499 --> 00:11:04,499
then you can put him back to bed.

207
00:11:04,539 --> 00:11:09,899
Oh, and remember to flip him over
onto the side that still has fur.

208
00:11:09,939 --> 00:11:12,339
All right, I'll remember.

209
00:11:16,099 --> 00:11:18,139
Oh, God. What was I supposed to do?

210
00:11:21,219 --> 00:11:23,219
Hello?

211
00:11:23,259 --> 00:11:25,699
Oh, hello, Mrs Bixby.

212
00:11:27,579 --> 00:11:30,259
No, this isn't Stuart, this is
Freddie.

213
00:11:31,739 --> 00:11:33,739
Yes, the actor.

214
00:11:35,019 --> 00:11:37,019
Yes, I'm still living here.

215
00:11:38,099 --> 00:11:40,539
Because I'm not looking for a wife.

216
00:11:40,579 --> 00:11:42,859
Anyway, Stuart should be there
shortly.

217
00:11:44,339 --> 00:11:46,339
No?

218
00:11:46,379 --> 00:11:48,379
Isn't he coming to see you?

219
00:11:49,499 --> 00:11:52,699
I see. Oh, I must be mistaken. Would
you excuse me?

220
00:11:52,739 --> 00:11:54,739
Splendid. Catching up, dear.

221
00:11:54,779 --> 00:11:56,779
Horrible, old cow.

222
00:12:00,859 --> 00:12:03,419
Oh, Vi, thank God you're there.
It's Freddie.

223
00:12:03,459 --> 00:12:05,859
You must come over right away.

224
00:12:05,899 --> 00:12:08,539
Oh, hang on. There's somebody at the
door.

225
00:12:11,339 --> 00:12:14,499
Are you always out there? I was just
popping round.

226
00:12:15,979 --> 00:12:17,979
What's wrong? It's Stuart.

227
00:12:18,019 --> 00:12:20,059
I think he's having an affair.

228
00:12:21,219 --> 00:12:24,219
Are you certain? He's been sneaking
around for days,

229
00:12:24,259 --> 00:12:27,419
and now he's lied about where he's
going. Oh, my!

230
00:12:27,459 --> 00:12:30,579
There's just one thing that doesn't
make any sense. What's that?

231
00:12:30,619 --> 00:12:32,739
Who the hell would want to shag
Stuart?

232
00:12:36,239 --> 00:12:40,119
I don't understand. How could Stuart
be seeing another man?

233
00:12:40,473 --> 00:12:43,273
I've been nothing but loving for 48
years.

234
00:12:44,353 --> 00:12:46,633
You might be getting upset over
nothing.

235
00:12:47,873 --> 00:12:49,873
Anyway, I should go.

236
00:12:49,913 --> 00:12:53,473
You just got here. Yes, but I was
looking for Stuart.

237
00:12:53,513 --> 00:12:55,513
Erm, well, he's not here, so...

238
00:12:55,553 --> 00:12:58,233
You're acting very strangely, Vi.

239
00:12:58,273 --> 00:13:00,193
You don't know something, do you?

240
00:13:00,200 --> 00:13:02,200
Of course not. I saw nothing.

241
00:13:04,000 --> 00:13:08,400
Oh. Hi, I was just coming by
to let you know that I got back
together with Tracey.

242
00:13:08,440 --> 00:13:11,520
She loved the gift. I owe you one.
I'll try and remember.

243
00:13:13,120 --> 00:13:15,440
Oh, that's wonderful, Ash.

244
00:13:15,480 --> 00:13:17,480
Is everything OK?

245
00:13:17,520 --> 00:13:20,080
I'm going through
a bit of a personal crisis.

246
00:13:20,120 --> 00:13:22,120
It's nothing I want to talk about.

247
00:13:26,800 --> 00:13:29,440
I guess I should also...

248
00:13:30,480 --> 00:13:34,200
Are you sure you're all right? You
see right through me, Ash.

249
00:13:38,360 --> 00:13:43,600
It's Stuart. I'm afraid he's seeing
somebody else. I don't believe that.

250
00:13:43,640 --> 00:13:46,920
Oh, Ash, you're so young. So naive.

251
00:13:46,960 --> 00:13:49,480
You've no idea what men are like.

252
00:13:50,520 --> 00:13:54,560
Well, I am a man, so
I probably have a bit of an idea.

253
00:13:54,600 --> 00:13:56,200
Mm.

254
00:13:56,240 --> 00:14:00,160
I'm going to have to consider very
carefully how I'm going to handle
this.

255
00:14:01,360 --> 00:14:03,360
You could just talk to him.

256
00:14:03,400 --> 00:14:05,000
I could.

257
00:14:05,040 --> 00:14:09,600
Or I could emotionally torture him
when he gets home

258
00:14:09,640 --> 00:14:12,720
and then follow him next time he
leaves the house.

259
00:14:13,760 --> 00:14:15,760
Yes, I think that's the winner.

260
00:14:19,600 --> 00:14:21,600
Would you like a biccie?

261
00:14:27,960 --> 00:14:29,960
Oh, still alive.

262
00:14:30,000 --> 00:14:32,560
Ooh. Good dog.

263
00:14:32,600 --> 00:14:34,600
Right, you can go back to sleep,
now.

264
00:14:36,240 --> 00:14:38,240
Oh, morning.

265
00:14:38,280 --> 00:14:41,400
I didn't hear you come in, last
night. Oh?

266
00:14:41,440 --> 00:14:43,440
How is your mother?

267
00:14:43,480 --> 00:14:45,880
Oh, fine. She sends her regards.

268
00:14:45,920 --> 00:14:48,240
Does she? Yes.

269
00:14:48,280 --> 00:14:51,280
She got some lovely colour on her
cruise.

270
00:14:51,320 --> 00:14:53,760
Is she a darker shade of grey now?

271
00:14:54,880 --> 00:14:56,880
Well, I must be going.

272
00:14:56,920 --> 00:14:59,920
Actually, I've got a doctor's
appointment.

273
00:14:59,960 --> 00:15:02,320
Do you? Yes. What's wrong with you?

274
00:15:02,360 --> 00:15:06,320
Aside from that visual horror show
your body has become...

275
00:15:07,480 --> 00:15:09,280
I'm just having them check

276
00:15:09,320 --> 00:15:12,560
that my stomach is still strong
enough to stand the sight of you.

277
00:15:13,960 --> 00:15:17,160
I'll be back in time to make it to
your screening.

278
00:15:17,200 --> 00:15:19,200
I'm surprised you even remembered,

279
00:15:19,240 --> 00:15:22,080
considering how many engagements
you've had lately.

280
00:15:22,120 --> 00:15:24,640
I'm suddenly living with Peaches
Geldof.

281
00:15:26,800 --> 00:15:29,480
Do you even know who that is?

282
00:15:29,520 --> 00:15:31,520
No.

283
00:15:31,560 --> 00:15:36,160
Now, if you want, I'll paint your
face on when I come home,

284
00:15:36,200 --> 00:15:39,760
so you'll at least resemble
what you used to look like.

285
00:15:43,720 --> 00:15:45,720
He's just left.

286
00:15:46,840 --> 00:15:48,840
I feel kind of weird about this,
Freddie.

287
00:15:48,880 --> 00:15:52,760
Ash, we helped you get back together
with that drug addict, didn't we?

288
00:15:52,800 --> 00:15:56,240
She's a nurse. So you keep saying,
now hurry, before we lose him.

289
00:15:56,280 --> 00:15:58,840
Wait! Wait! Wait! Wait!

290
00:15:58,880 --> 00:16:00,880
I've just got to poke the dog.

291
00:16:03,120 --> 00:16:05,120
Oh, he's still alive.

292
00:16:05,160 --> 00:16:06,840
Good boy.

293
00:16:06,880 --> 00:16:08,680
All right, let's go.

294
00:16:11,640 --> 00:16:13,840
I see you're back again with your
whore.

295
00:16:15,080 --> 00:16:17,400
Violet, what are you doing here? I
followed you.

296
00:16:17,440 --> 00:16:19,640
You followed me? Yes, I'm here to
warn you.

297
00:16:19,680 --> 00:16:21,600
Freddie knows you're having an
affair.

298
00:16:21,640 --> 00:16:25,560
How could you?
They've been together 48 years.

299
00:16:25,600 --> 00:16:29,440
It's been a hideous 48 years, but
it's not for us to judge.

300
00:16:29,480 --> 00:16:32,160
Well...? Answer me. I just want a
suit.

301
00:16:32,200 --> 00:16:34,200
You make me sick.

302
00:16:34,240 --> 00:16:37,680
You're buying him a suit? No, he's
buying a suit.

303
00:16:37,720 --> 00:16:40,480
I work here. Oh!

304
00:16:41,720 --> 00:16:44,200
It really looks quite lovely
on you.

305
00:16:44,240 --> 00:16:46,360
Why didn't you tell me?

306
00:16:46,400 --> 00:16:50,080
Well, I wanted to buy Freddie a new
coat for his screening tonight,

307
00:16:50,120 --> 00:16:52,120
but we couldn't afford it,

308
00:16:52,160 --> 00:16:55,720
and you know how he needs to feel
that he's the one supporting us.

309
00:16:55,760 --> 00:16:58,280
I didn't want him to know I'd taken
this job.

310
00:16:58,320 --> 00:17:00,000
Oh, Stuart.

311
00:17:01,040 --> 00:17:03,840
Look, there's Stuart, and he's with
some young man.

312
00:17:03,880 --> 00:17:05,880
Wait. Why is Violet with him?

313
00:17:05,920 --> 00:17:08,400
Please don't let her make me try on
Speedos again.

314
00:17:08,440 --> 00:17:10,440
What is Stuart doing?

315
00:17:11,560 --> 00:17:13,680
Freddie must never know.

316
00:17:13,720 --> 00:17:17,960
The important thing is that we let
him keep his dignity. Of course.

317
00:17:18,000 --> 00:17:21,720
Hey, Stu? I need you to clean the
staff room toilet.

318
00:17:21,760 --> 00:17:23,760
Oh, yes, Mr Harrison.

319
00:17:23,800 --> 00:17:26,080
What about your dignity?

320
00:17:26,120 --> 00:17:28,120
Oh, long gone.

321
00:17:29,160 --> 00:17:30,760
Oh, my God.

322
00:17:30,800 --> 00:17:35,400
Stuart's working here and he was too
embarrassed to let me know.

323
00:17:35,440 --> 00:17:38,640
How come? Well, look at me, I have
my career.

324
00:17:38,680 --> 00:17:40,680
He's just a shop girl.

325
00:17:44,760 --> 00:17:46,760
It's clear he's ashamed.

326
00:17:46,800 --> 00:17:50,440
After all,
my opinion means the world to him.

327
00:17:50,480 --> 00:17:53,720
Now, he can never know that I know.

328
00:17:53,760 --> 00:17:55,760
Do you understand? Of course.

329
00:17:55,800 --> 00:17:58,440
I think he's seen us.
What do we do?

330
00:17:58,480 --> 00:18:00,480
Follow my lead.

331
00:18:00,520 --> 00:18:02,240
So it's true!

332
00:18:02,280 --> 00:18:05,200
Freddie, what are you doing here?
What am I doing here?

333
00:18:05,240 --> 00:18:08,320
I'm very innocently buying swimming
trunks for Ash,

334
00:18:08,360 --> 00:18:11,080
when I think it's all too clear what
you're doing.

335
00:18:11,120 --> 00:18:13,960
It is? You're having an affair with
this whore.

336
00:18:14,000 --> 00:18:15,600
Hey!

337
00:18:15,640 --> 00:18:18,120
Oh, sorry. You're getting that a
lot, today.

338
00:18:18,160 --> 00:18:20,480
No, it's not what you think.

339
00:18:20,520 --> 00:18:24,240
I think it's very obvious to
everybody what's going on here.

340
00:18:24,280 --> 00:18:26,040
Wouldn't you say so, Ash?

341
00:18:27,080 --> 00:18:28,680
Not really.

342
00:18:28,720 --> 00:18:31,920
All right, yes, I was considering
having an affair.

343
00:18:31,960 --> 00:18:34,120
I knew it, Jezebel.

344
00:18:34,160 --> 00:18:36,160
Yeah, but I couldn't do it.

345
00:18:36,200 --> 00:18:41,040
I never had any real feelings
for... Gerard Wilkinson.

346
00:18:41,080 --> 00:18:42,880
I hope you can forgive me.

347
00:18:42,920 --> 00:18:45,800
It's going to take a very, very long
time,

348
00:18:45,840 --> 00:18:50,440
and I shall probably lash out at you
in cruel ways, so be prepared,

349
00:18:50,480 --> 00:18:53,120
you stinking pile of turd.

350
00:18:53,160 --> 00:18:56,000
Oh, thank you for being so
understanding.

351
00:18:58,320 --> 00:19:01,480
So Freddie still has absolutely no
idea? None.

352
00:19:01,520 --> 00:19:04,640
He still thinks that
I almost had an affair.

353
00:19:04,680 --> 00:19:07,760
Freddie! Hurry up, dear! We don't
want to be late!

354
00:19:07,800 --> 00:19:10,920
Don't rush me, you cheating slut!

355
00:19:14,040 --> 00:19:16,640
And he's being very decent
about it.

356
00:19:17,760 --> 00:19:20,880
You two have a twisted
relationship, you do know that.

357
00:19:20,920 --> 00:19:23,360
All couples are like this.

358
00:19:23,400 --> 00:19:26,640
You wouldn't understand, because
you're all alone.

359
00:19:28,680 --> 00:19:31,720
Oh, Ash! But I thought you were
bringing your girlfriend...

360
00:19:31,760 --> 00:19:35,240
It actually didn't work out, after
all. Oh, I'm sorry to hear that.

361
00:19:35,280 --> 00:19:37,440
Would you excuse me a moment?

362
00:19:37,480 --> 00:19:39,800
Violet, could you entertain Ash?

363
00:19:39,840 --> 00:19:42,360
You remember our friend, Violet?

364
00:19:44,560 --> 00:19:47,840
I don't understand, Ash. I thought
you were back together.

365
00:19:47,880 --> 00:19:50,920
We were, but then I saw what Stuart
did for Freddie -

366
00:19:50,960 --> 00:19:53,040
taking a job and not letting him
know.

367
00:19:53,080 --> 00:19:54,920
You knew Stuart was working there?

368
00:19:54,960 --> 00:19:56,560
I kind of figured it out.

369
00:19:56,600 --> 00:19:58,760
He did that because he wanted
Freddie to think

370
00:19:58,800 --> 00:20:01,520
he was still able to support them
both.

371
00:20:01,560 --> 00:20:04,720
His acting work isn't as frequent
as it used to be, I'm afraid.

372
00:20:04,760 --> 00:20:06,760
Yeah, I kind of figured that out
too.

373
00:20:06,800 --> 00:20:09,960
You're a little brighter than
I gave you credit for, Ash.

374
00:20:11,000 --> 00:20:13,320
Anyway, it made me realise
I wasn't ready

375
00:20:13,360 --> 00:20:16,440
to make that kind of sacrifice for
Tracey, so I ended it.

376
00:20:16,480 --> 00:20:18,520
I want the kind of love they have,
one day.

377
00:20:18,560 --> 00:20:20,680
Where is that miserable piece of
shit?

378
00:20:22,040 --> 00:20:25,240
I'm right here, you walking corpse.

379
00:20:27,400 --> 00:20:29,400
This is for you.

380
00:20:29,440 --> 00:20:32,760
If your hands can stop shaking long
enough, maybe you can open it.

381
00:20:36,560 --> 00:20:38,560
The coat I wanted!

382
00:20:38,600 --> 00:20:40,680
I thought you could wear it,
tonight.

383
00:20:41,760 --> 00:20:43,960
I am very proud of you.

384
00:20:45,000 --> 00:20:47,000
Tenth most popular villain.

385
00:20:48,520 --> 00:20:50,520
Thank you.

386
00:20:50,560 --> 00:20:52,560
Oh, you look quite dashing.

387
00:20:52,600 --> 00:20:55,320
And you don't look as revolting as
you usually do.

388
00:20:57,760 --> 00:20:59,400
Let's go.

389
00:20:59,440 --> 00:21:01,440
Let me just finish my last sip.

390
00:21:10,840 --> 00:21:12,240
There.

391
00:21:12,280 --> 00:21:15,240
Ash, I thought
you were bringing your girlfriend.

392
00:21:15,280 --> 00:21:16,880
We're not together any more.

393
00:21:16,920 --> 00:21:18,920
Did she take an overdose?

394
00:21:18,960 --> 00:21:21,560
No. She just wasn't the right girl.

395
00:21:21,600 --> 00:21:23,880
Well, it's important you find the
right one,

396
00:21:23,920 --> 00:21:27,720
or you could get stuck with some
useless moron for 1,000 years,
like I did.

397
00:21:27,760 --> 00:21:29,760
God, I hate you.

398
00:21:33,200 --> 00:21:36,080
It's your fault. How is it my fault?

399
00:21:36,120 --> 00:21:39,760
It was last week. You're in charge
of the diary - your fault.

400
00:21:42,560 --> 00:21:45,920
You said it was this week. Well,
I've got a lot going on.

401
00:21:45,960 --> 00:21:47,920
You have nothing going on.

402
00:21:47,960 --> 00:21:50,280
You stay at home all day, eating
chocolates.

403
00:21:50,320 --> 00:21:52,320
You'd think you could remember one
thing.

404
00:21:52,360 --> 00:21:54,040
I'm returning that coat. Good!

405
00:21:54,080 --> 00:21:55,880
I don't want anything from you.

406
00:21:57,360 --> 00:21:59,360
Tea? Yes, please.

407
00:22:00,856 --> 00:22:30,120
<font color="#3399CC">Subtitles by </font><font color="ffffff">MemoryOnSmells</font>
<font color="#3399CC">http://UKsubtitles.ru.</font>

