﻿1
00:00:31,603 --> 00:00:32,838
Keung!

2
00:00:34,825 --> 00:00:35,762
Uncle!

3
00:00:36,320 --> 00:00:37,088
I didn't think you were
picking me up!

4
00:00:37,088 --> 00:00:39,558
- Look at you!
- Uh oh!

5
00:00:40,822 --> 00:00:42,511
How is your English?

6
00:00:42,582 --> 00:00:46,095
My English is okay for simple things.

7
00:00:46,596 --> 00:00:49,397
- Is Amy's English good?
- Yes

8
00:00:49,647 --> 00:00:53,913
I was hoping you two could
take care of the supermarket.

9
00:00:53,963 --> 00:00:56,337
I know people who can
help you immigrate here.

10
00:00:56,339 --> 00:00:58,728
Like you and my father
running the supermarket

11
00:00:58,759 --> 00:01:01,029
for over 20 years? No thanks!

12
00:01:01,071 --> 00:01:02,681
I like Hong Kong.

13
00:01:02,737 --> 00:01:07,428
To be honest, if you say no, I will sell it.

14
00:01:07,446 --> 00:01:10,047
(Keung)
<i>You're my uncle,
I'll let you decide.</i>

15
00:01:10,666 --> 00:01:13,666
(Uncle Bill)
<i>Aren't you tired of being a cop?</i>

16
00:01:14,066 --> 00:01:17,066
(Keung)
<i>Cops deal with new things
everyday. It's exciting.</i>

17
00:01:17,150 --> 00:01:18,477
(Uncle Bill)
<i>Are you still a cop?</i>

18
00:01:18,483 --> 00:01:20,270
(Keung)
<i>I'm a Leutenant now.</i>

19
00:01:20,330 --> 00:01:21,430
(Keung)
<i>Very good.</i>

20
00:01:22,346 --> 00:01:27,760
(Keung)
<i>So, is my uncle's fiancé beautiful?</i>

21
00:01:27,760 --> 00:01:32,209
(Uncle Bill)
<i>Well, she's different. And beauty
is in the eye of the beholder.</i>

22
00:01:32,209 --> 00:01:35,000
(Uncle Bill)
<i>I can't wait for you to meet her.</i>

23
00:01:35,454 --> 00:01:38,454
Wow, Uncle Bill, this
place is really beautiful!

24
00:01:38,454 --> 00:01:40,863
(Uncle Bill)
<i>This area is of course beautiful.</i>

25
00:01:40,902 --> 00:01:43,079
(Keung)
<i>Is your supermarket in this area?</i>

26
00:01:43,082 --> 00:01:46,082
(Uncle Bill)
<i>If it were here, I would have it made!</i>

27
00:01:46,082 --> 00:01:49,101
<i>Our supermarket is in the Bronx.</i>

28
00:01:58,422 --> 00:01:59,223
(Police)
<i>Hey stop!</i>

29
00:02:00,049 --> 00:02:01,017
<i>He went that way!</i>

30
00:02:01,875 --> 00:02:02,375
<i>Stop!</i>

31
00:02:02,722 --> 00:02:05,191
Wow, Uncle Bill, this looks
like a really tough area.

32
00:02:05,191 --> 00:02:07,150
You'll get used to it after
you live here for awhile.

33
00:02:07,316 --> 00:02:10,316
It's important to mind your
own business in New York.

34
00:02:12,362 --> 00:02:13,954
Honey I'm home!

35
00:02:16,079 --> 00:02:17,756
She must be at the market.

36
00:02:17,774 --> 00:02:20,176
Uncle, your place is huge!

37
00:02:20,176 --> 00:02:22,244
It's more than 3,000 square feet.

38
00:02:22,244 --> 00:02:23,762
If I had a place like this in Hong Kong

39
00:02:23,762 --> 00:02:25,274
I wouldn't have to work anymore.

40
00:02:25,298 --> 00:02:28,829
You've never seen these pictures before.

41
00:02:29,002 --> 00:02:30,952
The supermarket in the old days.

42
00:02:31,072 --> 00:02:34,072
Your father was the lion,
and I was the Joker.

43
00:02:34,408 --> 00:02:37,733
Your father was a good
fighter back in the day.

44
00:02:38,186 --> 00:02:40,644
But his kung fu was no match
for the robber who shot him

45
00:02:40,806 --> 00:02:42,842
when he tried to retrieve
a lady's stolen purse.

46
00:02:43,063 --> 00:02:45,341
Let bygones be bygones.

47
00:02:45,341 --> 00:02:47,924
I'll wash my face and
take you to see the market.

48
00:02:48,078 --> 00:02:51,078
And you can meet your aunt-to-be.

49
00:02:58,272 --> 00:02:59,906
Hey Uncle, you still practice with this?

50
00:03:00,812 --> 00:03:02,419
No way. If your father hadn't made it

51
00:03:02,419 --> 00:03:05,593
I'd have thrown it away a long time ago!

52
00:03:07,525 --> 00:03:09,642
You still practice?

53
00:03:09,642 --> 00:03:11,130
Just sometimes.

54
00:03:45,415 --> 00:03:46,298
Cool trick!

55
00:03:47,227 --> 00:03:48,643
My nephew, call him Keung.

56
00:03:48,648 --> 00:03:51,602
Keung, your kung fu is
really good, Number 1.

57
00:03:51,602 --> 00:03:53,301
Keung's kung fu is good of course.

58
00:03:53,301 --> 00:03:55,015
Danny is my next-door neighbor.

59
00:03:55,015 --> 00:03:56,181
Don't you have to be in school?

60
00:03:56,181 --> 00:03:57,328
I'm on my way.

61
00:03:57,538 --> 00:03:58,838
- Let's help him downstairs.
- OK.

62
00:04:02,751 --> 00:04:03,745
Study hard.

63
00:04:03,745 --> 00:04:04,471
Let's go.

64
00:04:11,847 --> 00:04:13,509
This is for you.

65
00:04:16,181 --> 00:04:18,125
Don't play with it in class.

66
00:04:20,478 --> 00:04:22,300
- What was that?
- I gave him my old video game.

67
00:04:22,300 --> 00:04:24,665
That poor kid.

68
00:04:24,665 --> 00:04:27,424
He has only one sister,
and she neglects him.

69
00:04:29,268 --> 00:04:31,694
You see? Here we are. It's so close.

70
00:04:31,694 --> 00:04:33,354
This place is the result of more than 10 years

71
00:04:33,359 --> 00:04:34,776
of hard work by your father and me.

72
00:04:39,625 --> 00:04:41,413
Are you drawing a cartoon?

73
00:04:42,559 --> 00:04:45,705
Uncle Bill, why are you painting
this place if you're selling it?

74
00:04:47,136 --> 00:04:50,136
Fresh paint makes it look
better. I can impress the buyer.

75
00:04:50,750 --> 00:04:53,750
How come they haven't
painted this side yet?

76
00:04:53,789 --> 00:04:56,196
You go inside, I'll look around.

77
00:05:36,349 --> 00:05:37,857
Can I help you?

78
00:05:38,211 --> 00:05:39,684
I'm Keung.

79
00:05:39,732 --> 00:05:41,792
I'm Uncle Bill's nephew.

80
00:05:41,844 --> 00:05:43,408
Ah, you just arrived?

81
00:05:43,408 --> 00:05:44,504
Yes, I did.

82
00:05:44,504 --> 00:05:47,504
Here's a litle gift for my aunt-to-be.

83
00:05:48,420 --> 00:05:49,884
Not everyone here is your new aunt.

84
00:05:49,889 --> 00:05:52,980
She is Mrs Cheung, here
to arrange the wedding.

85
00:05:53,986 --> 00:05:56,401
Sorry.

86
00:06:20,038 --> 00:06:22,415
Are you surprised to see
your aunt to be is black?

87
00:06:22,415 --> 00:06:24,169
Yes, a little bit.

88
00:06:24,169 --> 00:06:27,434
Soon, the yellow, black, and
white people will be one.

89
00:06:27,434 --> 00:06:30,544
They will only be known
as the people of the earth.

90
00:06:32,625 --> 00:06:34,735
Wah, I'll be there in a minute.

91
00:06:34,890 --> 00:06:36,733
- Go and help your aunt
- OK.

92
00:06:39,484 --> 00:06:41,076
- Uncle Bill
- Hi, Wah.

93
00:06:41,076 --> 00:06:42,822
I've brought you a buyer.

94
00:06:43,454 --> 00:06:44,334
Elaine

95
00:06:44,760 --> 00:06:47,890
She's the one I told you who is very
interested in buying the market.

96
00:06:49,041 --> 00:06:51,884
Feel free to look around.

97
00:06:51,921 --> 00:06:52,903
OK.

98
00:06:53,317 --> 00:06:56,317
My shop is the only one
in these seven/eight blocks.

99
00:06:56,317 --> 00:06:58,536
We have a lot of regular customers.

100
00:06:58,536 --> 00:07:00,485
That makes for great business!

101
00:07:00,485 --> 00:07:02,486
Business is even better during the summer.

102
00:07:02,486 --> 00:07:04,151
Right.

103
00:07:07,700 --> 00:07:09,845
- Elaine, please come in and sit down.
- OK.

104
00:07:10,827 --> 00:07:12,612
Please sit down.

105
00:07:13,192 --> 00:07:14,904
Would you like something to drink?

106
00:07:14,904 --> 00:07:16,991
Oh, I'm fine, thank you.

107
00:07:16,991 --> 00:07:18,983
Ah, we can look outside through this mirror!

108
00:07:19,984 --> 00:07:22,984
The boss has to keep an eye
on everything. Right Uncle Bill?

109
00:07:23,264 --> 00:07:25,150
All the information is here.

110
00:07:25,150 --> 00:07:28,429
One has to work hard to make
money in a foreign country.

111
00:07:28,429 --> 00:07:31,429
I spent twenty years of my life on this shop.

112
00:07:47,680 --> 00:07:50,281
Why are you selling the market
if you are doing so well?

113
00:07:50,281 --> 00:07:52,265
Uncle Bill doesn't want to sell it.

114
00:07:52,265 --> 00:07:57,422
But his future wife wants to
buy a farm for raising horses.

115
00:08:00,580 --> 00:08:03,580
Race horses, you know?

116
00:08:14,555 --> 00:08:17,279
Uncle Bill, can you bring 
down the price a little?

117
00:08:17,279 --> 00:08:21,969
I've been honest with you. Last year
I turned down an offer of $220,000.

118
00:08:27,928 --> 00:08:30,258
I want to be nice to you.
My final offer is $220,000.

119
00:08:33,347 --> 00:08:34,907
You must be happy with that price?

120
00:08:34,907 --> 00:08:36,143
Are you?

121
00:08:37,558 --> 00:08:38,791
Not really.

122
00:08:38,791 --> 00:08:41,370
I would have to pay the 
legal fees and everything.

123
00:08:41,370 --> 00:08:43,142
They would add up to 
2 million Hong Kong dollars.

124
00:08:43,169 --> 00:08:45,606
What is two million HK dollars?

125
00:08:45,606 --> 00:08:48,098
That's only enough to buy 
a parking space in Hong Kong.

126
00:08:48,098 --> 00:08:49,254
Right.

127
00:08:50,890 --> 00:08:52,267
What?

128
00:08:52,267 --> 00:08:53,393
You don't believe me?

129
00:08:53,393 --> 00:08:55,802
No, I believe you.

130
00:08:55,802 --> 00:08:58,504
But what to do with existing employees?

131
00:08:58,504 --> 00:09:01,504
You can keep them. They are very reliable.

132
00:09:03,875 --> 00:09:06,875
If you buy this shop, you are the boss.

133
00:09:06,875 --> 00:09:08,993
If you don't want them,
you can dismiss them.

134
00:09:08,993 --> 00:09:12,916
No, I think they're very hard-working.

135
00:09:12,916 --> 00:09:14,716
Their salaries are low.

136
00:09:15,903 --> 00:09:16,853
I will think about it.

137
00:09:16,853 --> 00:09:19,046
Can you sign here to certify
I've come with a client?

138
00:09:19,046 --> 00:09:21,246
What is it?

139
00:09:22,284 --> 00:09:23,995
Uncle Bill

140
00:09:23,995 --> 00:09:25,910
This is my nephew, Keung.

141
00:09:25,910 --> 00:09:27,625
You just got here?

142
00:09:27,625 --> 00:09:28,990
He is Wah.

143
00:09:28,990 --> 00:09:32,234
Uncle Bill, somebody named
Stephen Lo just sent a car over.

144
00:09:32,234 --> 00:09:34,752
Yes, tell him to wait for me.

145
00:09:35,661 --> 00:09:37,720
You can sign them for me.

146
00:09:40,017 --> 00:09:41,161
Just a routine procedure.

147
00:09:41,161 --> 00:09:42,855
OK, no problem.

148
00:09:55,037 --> 00:09:56,876
I've stocked up a lot of goods.

149
00:09:58,859 --> 00:10:00,157
I've got everything here.

150
00:10:00,157 --> 00:10:03,157
This is the store room. People can live.

151
00:10:03,157 --> 00:10:04,894
Feel free to go up and have a look.

152
00:10:04,894 --> 00:10:06,798
Yes, it is very convenient.

153
00:10:16,788 --> 00:10:18,109
Good fortune to you.

154
00:10:18,193 --> 00:10:19,691
And to you also.

155
00:10:25,017 --> 00:10:26,464
Old customers. They have credits with me.

156
00:10:26,464 --> 00:10:28,738
Their business alone is enough
to keep the store running.

157
00:10:29,334 --> 00:10:31,008
Let me show you around. Come.

158
00:10:33,861 --> 00:10:37,750
You can see the graffiti all around.

159
00:10:37,750 --> 00:10:39,865
But our place is a lot cleaner.

160
00:10:39,865 --> 00:10:42,408
Everybody respects my
store because of its history.

161
00:10:45,150 --> 00:10:47,201
Take your time to think about it.

162
00:10:47,201 --> 00:10:49,389
Make sure you come to my wedding

163
00:10:49,389 --> 00:10:51,655
This is my only first marriage.

164
00:10:51,655 --> 00:10:52,932
I definitely will.

165
00:10:52,932 --> 00:10:53,865
You must come.

166
00:10:53,865 --> 00:10:55,025
We'd better go.

167
00:10:55,025 --> 00:10:56,390
- OK
- OK. Bye bye.

168
00:10:57,200 --> 00:10:59,446
Oh Stephen, come here!

169
00:11:04,318 --> 00:11:05,482
Wow, this car is beautiful!

170
00:11:05,482 --> 00:11:06,963
Yes, it's an antique.

171
00:11:16,452 --> 00:11:17,901
I told my boss that I took
the car to get a check-up.

172
00:11:17,904 --> 00:11:19,642
You'd better be careful with it!

173
00:11:19,642 --> 00:11:22,287
Don't worry. I rely on
it to pick up my bride.

174
00:11:22,287 --> 00:11:23,820
It means more to me than you.

175
00:11:23,820 --> 00:11:25,279
This is my nephew, Keung. Stephen.

176
00:11:25,279 --> 00:11:26,633
Yes, we just met.

177
00:11:27,314 --> 00:11:28,674
This is a little something for you. Take it.

178
00:11:28,674 --> 00:11:29,315
Oh, that's not necessary.

179
00:11:29,315 --> 00:11:30,896
Please take it. It'll bring luck.

180
00:11:30,896 --> 00:11:33,585
Thanks.

181
00:11:33,585 --> 00:11:35,325
Don't scrape the car.

182
00:11:35,325 --> 00:11:37,215
Don't worry. I'll sleep with it tonight.

183
00:13:56,345 --> 00:13:58,144
Uncle, your friend's car!

184
00:13:58,461 --> 00:14:02,145
Uncle, get up!

185
00:14:19,587 --> 00:14:20,208
Stop!

186
00:15:02,303 --> 00:15:03,542
Uncle!

187
00:15:05,192 --> 00:15:07,163
Uncle, open the door!

188
00:15:10,333 --> 00:15:12,701
Uncle, open the door!

189
00:15:14,071 --> 00:15:15,199
Uncle Bill!

190
00:15:17,895 --> 00:15:19,618
It's freezing!

191
00:15:24,202 --> 00:15:25,478
Uncle Bill!

192
00:16:06,164 --> 00:16:07,349
Can I get you something to drink?

193
00:16:07,349 --> 00:16:08,797
I've got it.

194
00:16:08,797 --> 00:16:10,050
My name is Keung.

195
00:16:10,050 --> 00:16:11,200
I heard the other day.

196
00:16:11,200 --> 00:16:13,759
You are macho Keung. Come, sit down.

197
00:16:13,982 --> 00:16:14,950
Thank you.

198
00:16:14,950 --> 00:16:17,525
I heard you wanted to buy the
supermarket. Have you decided?

199
00:16:18,596 --> 00:16:20,620
You're talking business now?

200
00:16:20,620 --> 00:16:23,620
No, I was just looking for 
something to talk about.

201
00:16:23,620 --> 00:16:25,154
But you sit next to me?

202
00:16:25,154 --> 00:16:28,154
You are the one I know best,
other than my uncle & aunt.

203
00:16:28,226 --> 00:16:29,563
Yes, same here.

204
00:16:30,629 --> 00:16:33,629
Do you think Uncle Bill will cut the price?

205
00:16:34,512 --> 00:16:35,832
If you are serious about it,

206
00:16:35,832 --> 00:16:38,052
We can lower the price a little.

207
00:16:38,452 --> 00:16:40,224
Can you make that decision?

208
00:16:40,279 --> 00:16:42,083
Half of the supermarket belongs to me.

209
00:16:43,076 --> 00:16:44,798
Thanks for coming!

210
00:16:44,879 --> 00:16:47,231
Uncle Bill, congratulations to us?

211
00:16:47,231 --> 00:16:48,809
To us?

212
00:16:49,169 --> 00:16:50,326
Yes, I want to buy your store.

213
00:16:50,473 --> 00:16:51,374
Really?

214
00:16:51,512 --> 00:16:53,168
Keung said he'd give me a good deal.

215
00:16:53,220 --> 00:16:55,445
Keung, you are great to 
fix the deal so quickly!

216
00:16:55,666 --> 00:16:56,750
Should I first pay the deposit?

217
00:16:56,906 --> 00:16:59,847
No need. Let's get it done quickly.

218
00:16:59,847 --> 00:17:02,265
- We can even skip to the lawyer's fee.
- Good!

219
00:17:02,749 --> 00:17:05,501
Uncle Bill, Uncle Bill, the
bride is waiting. Hurry up!

220
00:17:05,501 --> 00:17:07,332
It's a deal. I won't sell it to anyone else.

221
00:17:07,457 --> 00:17:08,926
Take care of the guests for me.

222
00:17:08,926 --> 00:17:10,252
You go ahead and do your thing.

223
00:17:10,729 --> 00:17:13,470
You buy it because I give
you a small discount?

224
00:17:13,470 --> 00:17:16,681
I was going to buy it anyway. But
it's even better with the discount.

225
00:17:16,681 --> 00:17:18,310
Good luck then.

226
00:19:44,592 --> 00:19:45,645
I'll be there in a minute.

227
00:19:46,106 --> 00:19:48,681
Ah-Keung, I have the stamped contract

228
00:19:48,735 --> 00:19:49,891
Remember to give it to Elaine.

229
00:19:49,891 --> 00:19:51,202
Take the money on the table.

230
00:19:51,202 --> 00:19:53,970
I'll be back in a week.
The whole house is yours.

231
00:19:56,046 --> 00:19:57,318
Ah-Keung, you're great!

232
00:19:59,514 --> 00:20:00,830
Uncle Bill, I'm ready.

233
00:20:00,830 --> 00:20:02,310
(Uncle Bill)
<i>It's OK. We're leaving now!</i>

234
00:20:16,137 --> 00:20:17,380
I have already signed this.

235
00:20:17,380 --> 00:20:18,240
Thanks.

236
00:20:18,240 --> 00:20:19,513
- The check.
- Thanks.

237
00:20:19,513 --> 00:20:22,007
Sign on the red one.

238
00:20:22,007 --> 00:20:23,194
OK.

239
00:20:23,194 --> 00:20:24,917
Would you give me a 20% discount card?

240
00:20:24,956 --> 00:20:27,956
What? Mine is only a small business.

241
00:20:28,234 --> 00:20:31,234
And what's the use of giving
you one? Aren't you leaving soon.

242
00:20:31,275 --> 00:20:32,728
And, when are you leaving?

243
00:20:33,194 --> 00:20:37,416
Uncle Bill said he would go back to Hong Kong with me after the honeymoon.

244
00:20:38,328 --> 00:20:39,886
Will you come back?

245
00:20:47,623 --> 00:20:49,036
Wait...

246
00:20:49,036 --> 00:20:51,497
He has already stolen a lot of chocolate.

247
00:20:57,372 --> 00:20:59,501
This is a tell-tale mirror!

248
00:21:01,587 --> 00:21:02,924
Don't worry.

249
00:21:02,924 --> 00:21:05,028
Let me settle the score with them.

250
00:22:00,003 --> 00:22:01,000
(man in Cantonese)
<i>What's going on?</i>

251
00:22:01,733 --> 00:22:02,798
Thank goodness you speak Cantonese.

252
00:22:03,240 --> 00:22:04,616
This guy wouldn't pay for his drinks.

253
00:22:04,898 --> 00:22:07,097
He stole some things, and is making a scene.

254
00:22:11,626 --> 00:22:13,188
You say we're stealing?!

255
00:22:14,180 --> 00:22:15,628
Thieves?

256
00:22:16,400 --> 00:22:18,036
Robbers?

257
00:22:24,821 --> 00:22:26,057
Shoplifting bastard.

258
00:22:56,065 --> 00:22:56,792
Ah-Keung!

259
00:23:30,259 --> 00:23:33,910
Stealing? Bullying women?

260
00:23:36,204 --> 00:23:38,058
Apologize to her!

261
00:23:45,324 --> 00:23:46,851
I'm sorry.

262
00:23:46,851 --> 00:23:48,298
I should say sorry instead.

263
00:23:50,642 --> 00:23:51,822
My arm!

264
00:23:51,822 --> 00:23:54,822
Don't show up here anymore,
or I'll beat you up each time.

265
00:23:57,670 --> 00:23:58,837
Watch your step.

266
00:23:59,768 --> 00:24:01,277
Thank you. Thank you.

267
00:24:13,315 --> 00:24:14,847
Are you OK? Did they hurt you?

268
00:24:15,079 --> 00:24:15,645
No.

269
00:24:21,385 --> 00:24:22,268
Ah-Keung...

270
00:24:23,210 --> 00:24:24,350
Were you hurt?

271
00:24:26,276 --> 00:24:28,790
How did you some of that? It was like...
(imitates martial arts noises)

272
00:24:32,258 --> 00:24:33,765
That's Chinese kung-fu.

273
00:24:36,670 --> 00:24:39,670
With Chinese kung fu,
remain calm while attacking.

274
00:24:39,670 --> 00:24:43,411
See if your opponent is unprepared. Then strike! But you have to act quickly.

275
00:24:43,411 --> 00:24:47,423
Aim for the sweet spot of their guard.
Hit there while covering here.

276
00:24:48,979 --> 00:24:51,333
Everyone has a weakness.

277
00:24:53,908 --> 00:24:56,435
Translate for them. I'll clean up.

278
00:25:03,554 --> 00:25:04,667
I'll tell you when I find out.

279
00:25:23,158 --> 00:25:24,367
Need some help?

280
00:25:24,807 --> 00:25:27,807
I have to do this myself. I
can't rely on you always.

281
00:25:41,693 --> 00:25:42,904
Thanks.

282
00:25:55,384 --> 00:25:56,907
It's okay, calm down.

283
00:25:57,537 --> 00:25:58,972
I can't get down!

284
00:25:58,972 --> 00:26:01,972
Of course you can, if you let go.

285
00:26:08,606 --> 00:26:09,842
- Thank you.
- You're welcome.

286
00:26:10,014 --> 00:26:11,275
I should get an electric one.

287
00:26:11,275 --> 00:26:14,275
That would be easier.
Go inside, I'll do it.

288
00:26:17,739 --> 00:26:18,798
Hey, wait!

289
00:26:19,117 --> 00:26:20,252
Your groceries.

290
00:26:21,170 --> 00:26:22,215
Here you go.

291
00:26:23,539 --> 00:26:25,237
- I'll put the contract in the bag.
- Thanks

292
00:26:25,237 --> 00:26:26,686
Oh, I forgot to pay.

293
00:26:26,686 --> 00:26:28,652
Don't worry about it. It's 
your payment for protection.

294
00:26:28,652 --> 00:26:30,537
Wow, then I should take more.

295
00:26:31,043 --> 00:26:32,350
Then I'll charge you.

296
00:26:32,350 --> 00:26:33,504
Hurry, lock it.

297
00:26:35,678 --> 00:26:37,788
This needs a new spring. Bye-Bye.

298
00:35:14,399 --> 00:35:15,382
I was attacked.

299
00:35:29,760 --> 00:35:30,957
How old is your sister?

300
00:35:38,426 --> 00:35:39,439
Where is she?

301
00:35:52,216 --> 00:35:53,551
What are you looking for?

302
00:35:59,125 --> 00:36:00,564
My uncle will kill me.

303
00:36:01,694 --> 00:36:02,749
Where you going?

304
00:36:13,082 --> 00:36:14,130
What happened?

305
00:36:16,367 --> 00:36:18,341
Two kids stole something this morning.

306
00:36:18,341 --> 00:36:20,486
Ben tried, but couldn't catch them.

307
00:36:20,961 --> 00:36:22,161
What happened to you?

308
00:36:22,161 --> 00:36:23,623
Uh, I fell last night.

309
00:36:26,965 --> 00:36:29,248
I apologize. I lost your contract.

310
00:36:30,953 --> 00:36:32,662
If both contracts of the
sellerand buyer are lost

311
00:36:32,662 --> 00:36:33,909
Can the deal be cancelled?

312
00:36:33,909 --> 00:36:34,809
Why?

313
00:36:34,809 --> 00:36:36,664
This whole thing was a fraud!

314
00:36:36,664 --> 00:36:37,959
Not tell me that people come and steal.

315
00:36:37,959 --> 00:36:40,959
And someone came this morning
to get their "protection money".

316
00:36:40,959 --> 00:36:43,508
I told him I didn't have it. He said
he'll come back this afternoon for it.

317
00:36:44,767 --> 00:36:46,994
Ben told me that you always
paid this money before.

318
00:36:46,994 --> 00:36:49,994
Why didn't you tell me this
before you sold it to me?!

319
00:36:51,262 --> 00:36:54,262
Look at the walls. They're
already filled with graffiti.

320
00:36:54,262 --> 00:36:56,007
Looks worse than your face!

321
00:37:03,799 --> 00:37:05,303
Good fortune to you!

322
00:37:14,261 --> 00:37:15,091
Stop!

323
00:37:49,769 --> 00:37:51,819
Remember, these people are bullies.

324
00:37:51,819 --> 00:37:53,867
You must act tough with them.

325
00:37:59,022 --> 00:38:00,871
It's them from yesterday!

326
00:38:20,335 --> 00:38:23,721
Where is he? Is he too
scared to show up? Tell us!

327
00:38:24,354 --> 00:38:26,600
- I don't know. He doesn't...
- Hold it, hold it.

328
00:38:35,996 --> 00:38:37,077
Do what you want!

329
00:38:38,732 --> 00:38:40,220
She said "no probrem".

330
00:38:44,865 --> 00:38:46,056
You made the right choice.

331
00:39:30,175 --> 00:39:31,777
Don't provoke them any further!

332
00:39:31,777 --> 00:39:32,566
Shut up bitch!

333
00:39:32,991 --> 00:39:33,491
Don't!

334
00:39:33,491 --> 00:39:33,991
What?!

335
00:40:04,193 --> 00:40:05,336
Are you okay?

336
00:51:21,552 --> 00:51:24,552
Some robbers came, but
the cops have them now.

337
00:51:25,104 --> 00:51:26,892
Oh, you're so heavy.

338
00:51:27,852 --> 00:51:29,537
It's alright now.

339
00:51:30,330 --> 00:51:32,101
Is your sister home?

340
00:51:32,101 --> 00:51:33,221
Probably not.

341
00:51:33,221 --> 00:51:34,050
I see.

342
00:51:35,031 --> 00:51:36,596
Let me get your wheelchair.

343
00:51:44,587 --> 00:51:46,239
Ah-Keung, come, come!

344
00:51:48,022 --> 00:51:49,720
What is it?

345
00:51:49,720 --> 00:51:51,799
Come! Ah-Keung, come, come on!

346
00:51:52,791 --> 00:51:53,553
What is it?

347
00:51:53,553 --> 00:51:54,256
There!

348
00:52:03,676 --> 00:52:05,030
You home?

349
00:52:13,932 --> 00:52:16,704
- <i>Keung, come bring my wheelchair.</i>
- OK.

350
00:52:17,691 --> 00:52:19,943
Your sister's never home. What does she do?

351
00:52:29,201 --> 00:52:32,201
I thought you said she was still in school.

352
00:52:34,905 --> 00:52:36,435
She has all kinds of jobs.

353
00:52:36,646 --> 00:52:40,996
Wow, so she works hard to support you.
That means she loves you very much.

354
00:52:40,996 --> 00:52:43,996
So just be a good brother to her.  

355
00:52:43,996 --> 00:52:46,625
Listen to her, and study hard.

356
00:52:57,590 --> 00:52:59,033
It's not your fault.

357
00:52:59,489 --> 00:53:01,291
Even if you legs don't work,
you can still do stuff, right?

358
00:53:13,900 --> 00:53:15,152
It's probably okay.

359
00:53:16,833 --> 00:53:17,949
I have a girlfriend.

360
00:53:17,949 --> 00:53:19,355
Break up with her!

361
00:53:21,167 --> 00:53:22,682
Why do you want me
to date your sister?

362
00:53:28,060 --> 00:53:30,965
I'm sure she has many boyfriends
who will take care of her.

363
00:54:05,638 --> 00:54:07,776
It's OK. Let's forget about the past.

364
00:54:11,852 --> 00:54:16,521
Oh, one night she called the cops
when there was a thief in the building.

365
00:54:20,283 --> 00:54:22,548
Let's try your new cushion.

366
00:54:23,769 --> 00:54:24,539
Come.

367
00:54:41,734 --> 00:54:43,231
Good fortune to you, girl.

368
00:54:45,263 --> 00:54:46,825
Don't!

369
00:56:17,721 --> 00:56:20,721
Our supermarket has a good reputation.

370
00:56:20,721 --> 00:56:22,820
Our suppliers include every big company.

371
00:56:22,820 --> 00:56:25,820
We're the only supermarket carrying
these items within seven/eight blocks.

372
00:56:25,820 --> 00:56:27,883
And we have many regular customers.

373
00:56:27,883 --> 00:56:31,033
And their business alone can
keep the supermarket going.

374
00:56:31,033 --> 00:56:33,014
I live upstairs. Feel free to look around.

375
00:56:33,014 --> 00:56:36,014
- OK, OK.
- This way

376
00:56:36,014 --> 00:56:38,516
Why sell such a thriving business?

377
00:56:38,516 --> 00:56:41,516
My boyfriend in Hong Kong
wants to get married.

378
00:56:41,516 --> 00:56:44,105
I don't actually want to sell it.

379
00:56:44,105 --> 00:56:46,793
How's the law & order in this area?

380
00:56:46,793 --> 00:56:48,401
Does anyone demand protection money?

381
00:56:48,401 --> 00:56:52,008
Protection fees? This is a foreign
society, no one demands protection fees.

382
00:56:54,928 --> 00:56:56,089
What's the price?

383
00:56:57,008 --> 00:56:59,525
Someone offered me $300,000.
But I refused the offer.

384
00:56:59,525 --> 00:57:03,794
But I like you guys. So I'll
sell it to you for that price.

385
00:57:03,794 --> 00:57:05,750
$300,000?

386
00:57:05,750 --> 00:57:08,750
You can't buy a parking space in
Hong Kong for that kind of money.

387
00:57:08,750 --> 00:57:09,607
That's true.

388
00:57:09,607 --> 00:57:11,499
We have a small warehouse.
I'll show you.

389
00:57:11,499 --> 00:57:12,530
- Good, good.
- OK.

390
00:57:17,629 --> 00:57:19,762
I only sold it to you for $220,000.

391
00:57:20,297 --> 00:57:23,523
I lost so much on renovations and repairs.

392
00:57:24,345 --> 00:57:26,990
I hope you will succeed. Then
give me back the money.

393
00:57:26,990 --> 00:57:27,920
<i>Keung!</i>

394
00:57:27,920 --> 00:57:28,898
Ah, Mr. Wah.

395
00:57:28,898 --> 00:57:31,898
I took some goods earlier,
and I came here to pay.

396
00:57:31,898 --> 00:57:34,138
Have a nice day! Bye-bye.

397
00:57:34,420 --> 00:57:37,420
Wow, so much graffiti everywhere!

398
00:57:37,420 --> 00:57:38,833
Yes, it's everywhere.

399
00:57:38,924 --> 00:57:43,925
Yes, on all the buildings. Except
ours, because everyone respects us!

400
00:57:47,911 --> 00:57:48,642
<i>We'll think about it.</i>

401
01:00:54,620 --> 01:00:55,604
You work as a dancer?

402
01:00:56,659 --> 01:00:57,650
Aren't you scared of the tiger?

403
01:01:06,618 --> 01:01:07,879
Do you like this job?

404
01:01:09,102 --> 01:01:10,128
What's wrong?

405
01:01:10,803 --> 01:01:12,852
Nothing. You dance very well.

406
01:01:48,300 --> 01:01:51,300
(Keung)
<i>Don't hang around those guys,
and spend more time wtih Danny.</i>

407
01:01:51,300 --> 01:01:52,911
He needs your care.

408
01:01:52,911 --> 01:01:56,178
Don't let the situation change
you. You have to change it.

409
01:02:42,728 --> 01:02:43,454
What would you like?

410
01:04:39,425 --> 01:04:40,818
Is Danny home?

411
01:05:00,097 --> 01:05:01,570
I've never trusted men.

412
01:05:02,449 --> 01:05:04,305
I thought you were different.

413
01:05:04,612 --> 01:05:05,584
What happened?

414
01:05:05,584 --> 01:05:07,798
The store was destroyed!
You ran off with their girl!

415
01:05:07,798 --> 01:05:11,019
You even told them that
you're the boss of the store!

416
01:05:11,019 --> 01:05:12,180
What are you trying to do to me?!

417
01:05:20,059 --> 01:05:20,846
Elaine!

418
01:05:22,863 --> 01:05:24,282
What's going on?

419
01:05:46,254 --> 01:05:46,909
Sorry.

420
01:05:49,903 --> 01:05:52,035
I'm the one who should say sorry.

421
01:05:52,411 --> 01:05:54,655
I lost my control just now.

422
01:05:55,302 --> 01:05:56,476
I shouldn't blame you.

423
01:05:58,321 --> 01:05:59,934
I should have known

424
01:06:01,045 --> 01:06:04,834
That a decent store sold at a
low price would have problems.

425
01:06:06,166 --> 01:06:09,470
<i>You said you were the 
boss to protect me.</i>

426
01:06:10,813 --> 01:06:12,244
I don't blame you.

427
01:06:12,868 --> 01:06:14,272
I'm grateful for your help.

428
01:06:15,668 --> 01:06:16,947
It's not your fault.

429
01:06:18,133 --> 01:06:19,870
It's mine.

430
01:06:21,155 --> 01:06:24,540
I shouldn't be so greedy.

431
01:06:26,242 --> 01:06:29,242
I thought I was getting a good deal.

432
01:06:29,299 --> 01:06:32,299
I should have just paid the protection fee.

433
01:06:32,757 --> 01:06:35,757
And turned a blind eye to people stealing.

434
01:06:36,847 --> 01:06:39,431
And everything would be okay.

435
01:06:40,268 --> 01:06:42,138
<i>However...</i>

436
01:06:44,302 --> 01:06:49,350
I thought you would protect me.

437
01:12:15,333 --> 01:12:17,080
I don't care if you want to listen.

438
01:12:17,080 --> 01:12:19,126
<i>Do you think you can always gang up?</i>

439
01:12:19,126 --> 01:12:22,126
Go around, beating people, stealing things, extoring people for the rest of your lives?

440
01:12:22,944 --> 01:12:25,944
Your so-called "home"
is just like garbage!

441
01:12:27,950 --> 01:12:31,532
Why do you have to be
the scum of society?

442
01:12:43,346 --> 01:12:46,346
I hope next time when we meet
we wont' be fighting each other.

443
01:12:46,346 --> 01:12:48,929
Instead we will be 
drinking tea together.

444
01:17:03,050 --> 01:17:04,414
Danny, are you okay?

445
01:17:17,468 --> 01:17:19,708
<i>Here, catch!</i>

446
01:17:26,264 --> 01:17:27,224
<i>Here, catch!</i>

447
01:17:50,207 --> 01:17:54,341
They're very different. The
big one alone is worth a house.

448
01:17:54,341 --> 01:17:55,088
Wow.

449
01:17:55,088 --> 01:17:56,343
Did they answer?

450
01:18:57,526 --> 01:18:58,934
Go to school and wait for me.

451
01:19:32,463 --> 01:19:34,483
<i>Is the quality okay?</i>

452
01:19:37,872 --> 01:19:39,615
Hey! Ah-Keung...

453
01:19:39,615 --> 01:19:42,287
Do you like it? The new
renovations are beautiful!

454
01:19:42,287 --> 01:19:45,287
I thought about it. Uncle Bill
was doing some things right.

455
01:19:47,598 --> 01:19:51,831
I'll pay the protection fees. There
are thefts in ever supermarket.

456
01:19:51,831 --> 01:19:53,875
We open again tomorrow.

457
01:19:53,875 --> 01:19:56,008
- Very beautiful.
- What's wrong? Stomach ache?

458
01:19:56,008 --> 01:19:57,817
Can we go somewhere quiet?

459
01:19:57,817 --> 01:19:59,855
Why a quiet place?

460
01:19:59,855 --> 01:20:02,002
Look...

461
01:20:04,737 --> 01:20:07,603
Wow, glass doesn't shine like that.

462
01:20:09,171 --> 01:20:11,187
Are these real?

463
01:20:11,365 --> 01:20:14,365
They're definitely real, stolen!

464
01:20:17,803 --> 01:20:19,062
Come on, let's go.

465
01:20:24,302 --> 01:20:25,567
What should I do?

466
01:20:25,615 --> 01:20:29,090
Why should I care after
what they did to the store?

467
01:20:29,090 --> 01:20:30,702
I'll take two or three
stones as compensation.

468
01:20:30,702 --> 01:20:33,702
Please don't. Peoples' lives are at stake here.

469
01:20:33,816 --> 01:20:36,816
If it's so serious, why not call the police?

470
01:20:36,816 --> 01:20:39,872
If I call the police, there'll be more
trouble. I don't know what to do.

471
01:20:41,023 --> 01:20:43,499
They won't notice if I took few pieces.

472
01:21:03,718 --> 01:21:05,204
What's wrong?

473
01:21:05,204 --> 01:21:07,242
He said to come to the supermarket.

474
01:21:07,242 --> 01:21:10,242
Ah, he knows about my new renovations.
That's why he told you to come.

475
01:21:10,242 --> 01:21:14,172
Don't worry. He's just trying to scare you.

476
01:21:14,770 --> 01:21:18,045
- Where are you going?
- Bathroom.

477
01:21:18,045 --> 01:21:19,840
You don't need diamonds in the bathroom.

478
01:21:22,739 --> 01:21:24,797
Come on, I'm not joking.

479
01:21:30,579 --> 01:21:32,093
Supermarket?

480
01:22:01,281 --> 01:22:03,626
Elaine, open up!

481
01:22:03,626 --> 01:22:05,309
Why?

482
01:22:05,309 --> 01:22:07,773
Come here, quick!

483
01:22:07,773 --> 01:22:09,090
I'm busy!

484
01:22:43,569 --> 01:22:45,252
Hey, Elaine, are you OK?

485
01:22:45,778 --> 01:22:49,316
No! No! I'm ruined, I have nothing!

486
01:22:49,316 --> 01:22:51,806
Don't be like this. I can see you.

487
01:22:51,806 --> 01:22:54,178
So what? You can see me,
everyone can see me!

488
01:22:54,178 --> 01:22:55,642
I don't care anymore!

489
01:22:55,642 --> 01:22:58,642
Don't worry, I'll fix it the damages.

490
01:22:58,642 --> 01:23:01,642
You asshole! It's all your fault!
You're trying to ruin me!

491
01:23:01,642 --> 01:23:05,896
Haven't you done enough to me?!
What do you want from me?!

492
01:23:05,896 --> 01:23:08,896
- I don't want anything!
- Do you want me to die?

493
01:23:08,896 --> 01:23:11,896
- You're killing me!
- Calm down, please!

494
01:23:14,119 --> 01:23:14,986
Oh, the phone.

495
01:23:48,003 --> 01:23:50,390
Quiet, I have to figure out
a way to save my friends.

496
01:23:50,390 --> 01:23:52,218
Save me first!

497
01:23:52,218 --> 01:23:54,384
I'll give you a diamond, okay?

498
01:23:54,384 --> 01:23:56,106
You'll give them all to me?

499
01:23:56,106 --> 01:23:58,294
No, only one. I need the rest.

500
01:23:58,294 --> 01:23:59,856
Just one...?

501
01:23:59,856 --> 01:24:01,777
I'll give you the biggest one.

502
01:24:01,777 --> 01:24:04,007
You promised! Protect it!

503
01:24:04,007 --> 01:24:06,126
I'll call the cops, they can help.

504
01:24:17,851 --> 01:24:19,115
What are you doing?

505
01:24:19,115 --> 01:24:20,502
Looking for diamonds!

506
01:25:00,589 --> 01:25:01,751
Father watch over me.

507
01:40:49,870 --> 01:40:50,898
Are you sure?

508
01:40:50,898 --> 01:40:52,177
Leave it to me.

509
01:42:05,126 --> 01:42:06,244
Get him! Over there!

510
01:42:08,770 --> 01:42:09,912
Look at that! Great!
