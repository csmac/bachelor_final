1
00:00:50,266 --> 00:00:51,790
Hey, Kevin, time to get up, buddy.

2
00:00:51,935 --> 00:00:53,562
- Gotta roll.
KEVIN: I know.

3
00:00:53,703 --> 00:00:55,136
- What time is it?
- Can you get up?

4
00:00:55,271 --> 00:00:56,499
- Four-thirty, man.
- Okay.

5
00:00:56,639 --> 00:00:59,904
- How long do we have?
- Five minutes. Living the dream, baby.

6
00:01:09,753 --> 00:01:12,654
- Nick. What's up, buddy? Gotta roll.
- Ugh.

7
00:01:12,789 --> 00:01:15,519
- Four-thirty, man. Get up. All right?
- I know.

8
00:01:15,658 --> 00:01:17,387
One more minute.

9
00:01:17,527 --> 00:01:19,654
All right, I'm up.

10
00:01:28,538 --> 00:01:30,836
Hey, Joe. Gotta get up, brother.
You gotta move.

11
00:01:30,974 --> 00:01:32,271
- Dude, go away, man.
- Gotta go.

12
00:01:32,408 --> 00:01:34,205
- No, I'm sleeping. I'm sleeping...
- Let's go.

13
00:01:34,344 --> 00:01:36,369
- Why did you hit me, man?
- Get up. Time to go.

14
00:01:43,086 --> 00:01:45,281
Okay, so here's today's schedule.
At 7 AM., you...

15
00:01:46,623 --> 00:01:48,887
Earthquake. Just kidding.

16
00:01:49,025 --> 00:01:51,118
- Ugh.
- Aftershock.

17
00:01:52,695 --> 00:01:54,925
- I'm sorry.
- I'm sorry. Thank you. Yes.

18
00:01:55,064 --> 00:01:56,793
Do you have any peach jelly?

19
00:01:56,933 --> 00:02:00,096
- Um, seven-thirty, we...
SERVER: Yes.

20
00:02:00,236 --> 00:02:02,500
- Ah, didn't see that.
... are leaving for rehearsals.

21
00:02:02,639 --> 00:02:06,939
At 8:12, "Burnin' Up"
with the anchors onstage.

22
00:02:07,076 --> 00:02:10,170
- Hey, Joe, are you listening?
- Yes. Pancakes, please.

23
00:02:10,313 --> 00:02:11,712
KEVIN:
Nine AM., post-show taping.

24
00:02:11,848 --> 00:02:13,713
<i>Nine-fifteen, finish
Good Morning, America.</i>

25
00:02:13,850 --> 00:02:18,344
Then we depart for Central Park
to finish our music video.

26
00:02:18,755 --> 00:02:20,723
We film it in 3-D.

27
00:02:20,857 --> 00:02:23,121
Three-thirty PM., CD listening party
at the Highland Ballroom.

28
00:02:23,259 --> 00:02:24,624
Psst. Hey.

29
00:02:24,761 --> 00:02:26,456
KEVIN: Perform about three songs.
And then 5 PM., interviews.

30
00:02:29,032 --> 00:02:30,863
How long was she standing there?

31
00:02:31,000 --> 00:02:32,558
Five seconds.

32
00:02:32,702 --> 00:02:35,466
She was... Joe, she was standing
there because you're like:

33
00:02:35,605 --> 00:02:37,732
"Can I have the jelly, please?

34
00:02:37,874 --> 00:02:39,501
Like, do...?
Do you have any waffles?"

35
00:02:39,642 --> 00:02:41,166
KEVIN
"Pancakes will do. "

36
00:02:41,311 --> 00:02:43,336
Dude, did you see her buy
the quake joke?

37
00:02:43,479 --> 00:02:44,844
That works every time.

38
00:02:44,981 --> 00:02:47,506
You've been doing
the earthquake thing since, like, third grade.

39
00:02:47,650 --> 00:02:49,618
Stop playing.
- What's wrong with earthquake?

40
00:02:49,752 --> 00:02:51,310
Sorry, Big Rob. Okay.

41
00:02:51,454 --> 00:02:52,785
- Stop doing that.
It's awesome.

42
00:02:52,922 --> 00:02:55,186
Guys, you're gonna play with
more earthquake games again.

43
00:02:55,325 --> 00:02:58,556
Three to 4, Kevin picks
up some new pajamas. Oh!

44
00:02:59,629 --> 00:03:01,460
You got... You showed him.

45
00:03:01,965 --> 00:03:04,866
"Whoo-hoo.
Aftershock. Can I have your number?"

46
00:03:05,001 --> 00:03:07,094
- What are you wearing?
- Yo, guys, we got to go.

47
00:03:07,237 --> 00:03:08,761
Are we late?
- Yes, let's move.

48
00:03:08,905 --> 00:03:10,236
- Where are we going?
- Thank you.

49
00:03:10,373 --> 00:03:11,965
KEVIN: Joe, just follow.
- Which way?

50
00:03:12,108 --> 00:03:13,166
This way, assuming?

51
00:03:13,309 --> 00:03:14,571
KEVIN: Living the dream.
- Let's go.

52
00:03:18,381 --> 00:03:19,678
This is a mess. Do you see this?

53
00:03:19,816 --> 00:03:21,613
This goes all the way
down to Broadway.

54
00:03:21,751 --> 00:03:23,946
- Unbelievable.
- Yes, I just beat my high score.

55
00:03:24,087 --> 00:03:25,179
We're gonna be late.

56
00:03:25,321 --> 00:03:28,620
Big Rob, this traffic's killing us.
We're on in another hour, guys.

57
00:03:29,158 --> 00:03:30,785
- There's nothing we can do.
- Helicopter?

58
00:03:30,927 --> 00:03:32,258
- Already on it.
Nice.

59
00:03:32,395 --> 00:03:34,363
- Big Rob.
KEVIN: Nicely done.

60
00:03:34,497 --> 00:03:36,761
- Yes, sir. Tenth and Broadway.
- That's so close.

61
00:03:36,900 --> 00:03:38,959
Yeah, but we're gonna
have to leave the car here.

62
00:03:39,102 --> 00:03:41,070
Guys, I put this car on my credit card,
and why are there fans here?

63
00:03:42,405 --> 00:03:44,430
We got trouble.
KEVIN: How did they find it was us?

64
00:03:44,574 --> 00:03:47,042
- Big Rob!
- No!

65
00:03:47,176 --> 00:03:48,541
Earthquake!

66
00:03:48,678 --> 00:03:50,976
KEVIN: Not now, Joe.
We have to get out of here.

67
00:03:51,114 --> 00:03:53,480
- Look, I can't get out, Big Rob.
Stay inside.

68
00:03:53,616 --> 00:03:55,413
Where's Big Rob going?

69
00:03:59,088 --> 00:04:00,715
I'm trying to get out of the car.

70
00:04:00,857 --> 00:04:02,791
Stand back.
KEVIN: Let's go through the roof.

71
00:04:02,926 --> 00:04:04,757
Let's try it. Ready?

72
00:04:05,528 --> 00:04:07,257
Kevin!

73
00:04:13,169 --> 00:04:14,898
It's Nick.

74
00:04:16,039 --> 00:04:17,529
Come on.

75
00:04:17,674 --> 00:04:21,235
- Big Rob, come on.
- Go, go. Save yourselves.

76
00:04:45,835 --> 00:04:47,769
Oh, my God.

77
00:05:24,741 --> 00:05:26,402
Go, go.

78
00:05:33,883 --> 00:05:35,180
Oh, no!

79
00:05:35,318 --> 00:05:37,582
Nick. Kevin.

80
00:05:43,993 --> 00:05:45,756
Open.

81
00:05:47,397 --> 00:05:48,728
I love you.
KEVIN: Joe.

82
00:06:29,972 --> 00:06:32,463
- How did you get here so fast?
- Don't worry about it. Get in.

83
00:06:46,923 --> 00:06:49,721
<i>Jonas! Jonas! Jonas!</i>

84
00:06:51,694 --> 00:06:56,427
<i>Jonas! Jonas! Jonas!</i>

85
00:06:56,566 --> 00:07:02,027
<i>Jonas! Jonas! Jonas!</i>

86
00:07:02,171 --> 00:07:06,164
<i>Jonas! Jonas! Jonas!</i>

87
00:11:06,782 --> 00:11:08,545
Joe, I love you!

88
00:11:52,395 --> 00:11:53,453
Sing it out.

89
00:11:58,734 --> 00:11:59,792
Come on.

90
00:12:23,692 --> 00:12:24,784
Sing it!

91
00:13:57,887 --> 00:13:59,548
How's everybody doing tonight?

92
00:14:02,658 --> 00:14:05,855
Welcome to the Burning Up Tour,
everybody.

93
00:14:07,129 --> 00:14:09,324
We are the Jonas Brothers.

94
00:14:13,135 --> 00:14:17,094
And I think it's time
that we get this party started.

95
00:15:59,942 --> 00:16:01,842
Listen, girl.

96
00:16:02,811 --> 00:16:04,711
You gotta be good.

97
00:16:05,781 --> 00:16:08,841
You see, I don't wanna hurt you.

98
00:16:08,984 --> 00:16:11,384
I wanna kiss you.

99
00:16:13,589 --> 00:16:15,557
Come on. Come on.

100
00:16:16,191 --> 00:16:18,557
Come on, Kev.

101
00:16:37,646 --> 00:16:38,704
Come on, come on.

102
00:16:54,730 --> 00:16:56,391
I'm gonna need your help.

103
00:16:56,532 --> 00:16:58,523
Sing it out with us, okay?

104
00:16:58,667 --> 00:16:59,895
Here we go.

105
00:17:13,048 --> 00:17:14,106
How about just you?

106
00:17:27,229 --> 00:17:28,526
Come on.

107
00:17:41,343 --> 00:17:42,537
One more time.
- Come on.

108
00:17:56,859 --> 00:17:59,760
Yeah!

109
00:18:06,201 --> 00:18:07,930
There are a lot of fans down there.

110
00:18:08,070 --> 00:18:10,061
Oh, my God. That's so crazy.
- In back.

111
00:18:10,205 --> 00:18:12,105
Look how far they're over there, guys.

112
00:18:12,241 --> 00:18:13,970
There's even more there.

113
00:18:14,777 --> 00:18:15,835
But go fresh.

114
00:18:15,978 --> 00:18:18,139
I really wanna accentuate
the eyes on Big Rob tonight.

115
00:18:18,280 --> 00:18:19,338
Uh-oh.

116
00:18:19,715 --> 00:18:22,309
Still coming in too. It's crazy.

117
00:18:22,451 --> 00:18:24,783
KEVIN:
Look, they're waving up here. See?

118
00:18:31,326 --> 00:18:33,624
<i>From the early days of Beatlemania,</i>

119
00:18:33,762 --> 00:18:36,663
<i>to the overnight sensations
of the '70s,</i>

120
00:18:36,799 --> 00:18:39,791
<i>to the choreographed
boy bands of the '90s.</i>

121
00:18:39,935 --> 00:18:44,838
<i>As long as there have been teen idols,
there have been teen-idol fanatics.</i>

122
00:18:44,973 --> 00:18:50,138
<i>But the Big Apple has never seen
dedication quite like this.</i>

123
00:18:55,417 --> 00:18:57,442
Twenty-four hours.

124
00:19:05,227 --> 00:19:06,353
Can I borrow this?

125
00:19:09,598 --> 00:19:10,656
Oh, my God!

126
00:19:16,038 --> 00:19:17,630
I can't believe it.

127
00:19:20,576 --> 00:19:22,840
Can you tell your mom you're okay?

128
00:19:23,178 --> 00:19:25,738
- I'm okay, Mom.
See? Everything is fine.

129
00:19:25,881 --> 00:19:27,508
Touching Joe was just, like, ahh!

130
00:19:27,649 --> 00:19:30,379
That's not fair. I didn't get to touch him.
He missed me.

131
00:19:33,555 --> 00:19:35,523
Okay. It's okay. It's okay.

132
00:19:35,657 --> 00:19:37,318
I was shaking the entire time.

133
00:19:38,460 --> 00:19:39,586
Oh, my God.

134
00:19:39,728 --> 00:19:40,922
Ready?

135
00:19:41,063 --> 00:19:42,928
They're bodacious.

136
00:19:43,065 --> 00:19:45,659
Seriously, I don't know what I'm
gonna do. I'm gonna throw up.

137
00:19:45,801 --> 00:19:48,861
Like, I've dreamt about this day
so many times.

138
00:19:49,705 --> 00:19:50,763
Oh, my God.

139
00:19:53,542 --> 00:19:56,170
Joe. Joe. Joe.
Kevin. Kevin.

140
00:19:57,079 --> 00:19:58,444
Kevin.

141
00:19:58,580 --> 00:20:00,480
We flew from Florida, just to see them.

142
00:20:00,616 --> 00:20:01,947
This used to say, "Florida. "

143
00:20:02,084 --> 00:20:05,952
- I love you, Joe! I love you!
- I love you!

144
00:20:06,088 --> 00:20:07,612
We love you.

145
00:20:12,327 --> 00:20:15,057
If the Jonas Brothers are gonna see this,
we really wanna meet you.

146
00:20:37,753 --> 00:20:39,015
I love you, Joseph!

147
00:23:44,439 --> 00:23:48,307
KEVIN:
What's going on, everybody?

148
00:23:50,412 --> 00:23:52,346
We are so happy to be with you tonight.

149
00:23:52,481 --> 00:23:54,278
Are you having fun so far?

150
00:23:56,818 --> 00:23:59,514
All right. You know what?

151
00:23:59,654 --> 00:24:03,090
I think I like that. Let's clap our hands.

152
00:24:04,426 --> 00:24:06,360
Are you with me?

153
00:24:07,729 --> 00:24:10,926
Okay. Joe, take it away, bro.

154
00:24:11,066 --> 00:24:14,433
Now, we're gonna have
a little drum competition right now.

155
00:24:14,569 --> 00:24:16,867
Between the two drummers up top.

156
00:24:17,005 --> 00:24:18,836
We're gonna see how many drumsticks

157
00:24:19,174 --> 00:24:21,506
Jack Lawless,
over here on drums, can catch.

158
00:24:21,643 --> 00:24:23,008
And Nick J's gonna throw them.

159
00:24:23,145 --> 00:24:25,272
But we're gonna need
your guys' help.

160
00:24:25,413 --> 00:24:27,313
I love you!
All right?

161
00:24:27,449 --> 00:24:29,110
So on the count of three

162
00:24:29,251 --> 00:24:31,242
he's gonna throw them.
We're gonna need your help.

163
00:24:31,386 --> 00:24:34,822
One, two, three.

164
00:24:36,691 --> 00:24:38,318
Oh! So close.

165
00:24:39,027 --> 00:24:42,656
I think I need you to scream
a little louder than that. Come on.

166
00:24:45,567 --> 00:24:48,934
One, two, three.

167
00:24:52,140 --> 00:24:53,368
There you go.

168
00:27:58,927 --> 00:28:01,896
Madison square Garden.
Greatest city in the world.

169
00:28:02,030 --> 00:28:05,363
The Jonas Brothers fans
never get to touch the Jonas Brothers.

170
00:28:05,500 --> 00:28:07,934
- We're the next best thing.
- We're the Fake Jonas Brothers.

171
00:28:08,069 --> 00:28:09,366
If you can't meet the real ones,

172
00:28:09,504 --> 00:28:12,132
- why not meet the fake ones?
- They're usually pretty pleased.

173
00:28:12,273 --> 00:28:15,003
Jonas Brothers are living the dream.
And we're dreaming the life.

174
00:28:18,480 --> 00:28:21,574
<i>Jonas! Jonas! Jonas!</i>

175
00:28:23,752 --> 00:28:25,151
- How are you, guys?
- I'm Fake Joe.

176
00:28:25,286 --> 00:28:26,480
- Hi.
- Hi, I'm Fake Nick.

177
00:28:26,621 --> 00:28:28,418
- Hi, Fake Nick.
- Nice to see you. All right.

178
00:28:28,556 --> 00:28:29,682
I'm Fake Kevin.

179
00:28:29,824 --> 00:28:31,086
Look, it's Zac Efron.

180
00:28:31,226 --> 00:28:32,955
Yeah, I can't believe
you came to the show.

181
00:28:33,094 --> 00:28:34,152
Fake Zac.

182
00:28:34,295 --> 00:28:36,263
- It's so good to see you, man.
- Fake Efron.

183
00:28:36,398 --> 00:28:38,593
<i>You were so awesome
in the Shanghai...</i>

184
00:28:38,733 --> 00:28:41,702
<i>No, not Shanghai.
What's it called with Jackie Chan?</i>

185
00:28:41,836 --> 00:28:43,531
<i>FAKE KEVIN: Rush Hour.
Rush Hour, man.</i>

186
00:28:43,671 --> 00:28:45,696
That was tight.
- That wasn't me.

187
00:28:45,840 --> 00:28:49,173
Kelly Clarkson doesn't even have
a Fake Kelly Clarkson.

188
00:28:49,310 --> 00:28:50,971
- Yeah.
- Yeah, well done.

189
00:28:51,112 --> 00:28:52,204
He's our fake dad.

190
00:28:52,347 --> 00:28:53,780
- Yeah. Whoo!
- Yeah.

191
00:28:53,915 --> 00:28:55,473
- Fake Kevin senior.
- He had Nick hair.

192
00:29:03,725 --> 00:29:06,626
You're from spain, huh?
Are the Jonas Brothers popular in spain?

193
00:29:06,761 --> 00:29:08,661
A little, a little.

194
00:29:10,665 --> 00:29:14,032
Could you say, "I love the Jonas Brothers
more than my wife" in spanish?

195
00:29:14,169 --> 00:29:15,693
Yes, I love.

196
00:29:17,739 --> 00:29:19,206
What?

197
00:29:20,742 --> 00:29:23,575
One, two, three.

198
00:31:37,579 --> 00:31:39,843
All right, now it's your turn.

199
00:32:01,936 --> 00:32:03,426
Come on, everybody!

200
00:32:27,695 --> 00:32:30,630
Ladies and gentlemen,
give it up for our friend Demi Lovato.

201
00:33:27,021 --> 00:33:28,886
Come on, girl.

202
00:35:36,484 --> 00:35:39,885
Ladies and gentlemen, Demi Lovato.

203
00:35:46,093 --> 00:35:48,323
AsslsTANT:
Come on, guys, come on.

204
00:35:48,696 --> 00:35:49,754
KEVIN:
Come on, guys, hurry up.

205
00:35:49,897 --> 00:35:53,458
<i>Jonas! Jonas! Jonas!</i>

206
00:35:54,435 --> 00:35:56,699
KEVIN:
Okay, guys, we have 30 seconds.

207
00:35:58,339 --> 00:36:00,500
- It's stuck in my hair.
- Whoa!

208
00:36:01,676 --> 00:36:04,008
- Hey.
AsslsTANT: Oh, man.

209
00:38:09,603 --> 00:38:11,127
Two, three.

210
00:39:36,991 --> 00:39:38,982
This song

211
00:39:42,430 --> 00:39:44,796
is for every broken heart,

212
00:39:48,636 --> 00:39:50,831
for every lost dream.

213
00:39:54,275 --> 00:39:56,334
For every high,

214
00:39:57,411 --> 00:39:59,345
and for every low.

215
00:40:04,552 --> 00:40:06,747
And for every person

216
00:40:08,456 --> 00:40:10,822
who's ever felt alone.

217
00:40:15,262 --> 00:40:17,059
And tonight,

218
00:40:19,066 --> 00:40:20,795
this song...

219
00:40:23,637 --> 00:40:25,696
This song's for you.

220
00:40:32,246 --> 00:40:34,612
Now, here's what I want you to do.

221
00:40:37,485 --> 00:40:39,715
Want you to clap your hands...

222
00:40:40,654 --> 00:40:42,212
There you go.

223
00:40:43,858 --> 00:40:47,487
- And I want you to sing this out with me
as loud as you can.

224
00:40:47,895 --> 00:40:49,556
With all of your heart,

225
00:40:49,697 --> 00:40:52,495
with all of your soul.
Ryan, would you join me?

226
00:41:02,109 --> 00:41:03,736
Sing it.

227
00:41:25,733 --> 00:41:27,325
Sing it.

228
00:41:37,645 --> 00:41:38,805
Just you.

229
00:41:48,489 --> 00:41:50,286
Two, three.

230
00:43:58,919 --> 00:44:01,046
I'm Joe Jonas.

231
00:44:33,921 --> 00:44:37,084
- One, two, three. Showtime.
All right.

232
00:48:08,735 --> 00:48:09,997
Thank you.

233
00:48:14,708 --> 00:48:17,199
I need to see you clap your hands
like this, come on.

234
00:48:19,813 --> 00:48:21,974
All the way to the top.

235
00:48:48,675 --> 00:48:51,166
- Are you ready?
- Yeah!

236
00:49:38,058 --> 00:49:39,992
Here we go! Yeah!

237
00:50:16,730 --> 00:50:18,391
Hey, Joe?

238
00:50:18,765 --> 00:50:20,460
You think they're ready?

239
00:52:24,624 --> 00:52:28,321
Here we go. We're about to leave
for Times square. Tsk, tsk, tsk.

240
00:52:28,461 --> 00:52:30,224
Whoo!

241
00:52:30,363 --> 00:52:32,991
This is gonna be real out of control.

242
00:52:33,133 --> 00:52:34,225
Do what we gotta do.

243
00:52:34,367 --> 00:52:37,302
Walk out. And that's...
You know what I mean?

244
00:52:37,437 --> 00:52:39,530
- Just stick to us like white on rice.
I will.

245
00:53:01,228 --> 00:53:03,458
- Oh, my gosh.
- It goes all the way.

246
00:53:03,597 --> 00:53:05,189
Oh, my goodness.

247
00:53:06,299 --> 00:53:08,767
DENlsE: I can't believe this.
Oh, my gosh.

248
00:53:14,641 --> 00:53:16,108
It's New Year's Eve.

249
00:53:16,243 --> 00:53:18,507
I've never seen anything like this.

250
00:53:18,645 --> 00:53:20,112
Oh, my gosh.

251
00:53:20,247 --> 00:53:21,737
It's the Jonas Brothers New Year.

252
00:53:21,882 --> 00:53:23,349
It's the Jonas Brothers New Year.

253
00:53:28,622 --> 00:53:32,615
This is incredible. Oh, my gosh,
there are so many people here.

254
00:53:40,500 --> 00:53:42,058
Excuse me.

255
00:53:45,071 --> 00:53:47,562
I love you, I'm here for you.

256
00:53:47,707 --> 00:53:49,140
I love you, Joe.

257
00:53:54,314 --> 00:53:57,181
- Whoa, whoa, whoa.
- Yeah, you can't go back there.

258
00:53:58,885 --> 00:54:00,819
Anytime, guys.
Here, have another one.

259
00:54:00,954 --> 00:54:02,785
Kevin.

260
00:54:06,059 --> 00:54:07,993
First copy.

261
00:54:08,495 --> 00:54:10,224
This is madness.

262
00:54:17,837 --> 00:54:21,466
Yeah!

263
00:54:25,478 --> 00:54:27,173
Thank you!

264
00:54:27,781 --> 00:54:30,079
Thank you, everybody!

265
00:54:30,784 --> 00:54:32,547
Thank you very much.

266
00:58:42,335 --> 00:58:45,896
Hey! Hey!

267
00:58:46,606 --> 00:58:49,905
Hey! Hey!

268
01:00:54,767 --> 01:00:55,825
Hey!

269
01:03:16,142 --> 01:03:17,769
Ladies and gentlemen,

270
01:03:18,845 --> 01:03:21,313
please give it up for our friend
Taylor swift, everybody!

271
01:06:44,617 --> 01:06:46,312
Come on, sing it with us!

272
01:07:18,551 --> 01:07:20,746
Ladies and gentlemen, Taylor swift!

273
01:07:21,487 --> 01:07:22,977
Thank you very much!

274
01:11:05,177 --> 01:11:07,441
Hot dogs.

275
01:11:19,825 --> 01:11:22,259
SOs.

276
01:12:07,973 --> 01:12:11,602
All right, keep clapping
those hands. Come on. Come on.

277
01:13:12,337 --> 01:13:14,498
One, two, three, yeah.

278
01:13:45,337 --> 01:13:46,497
Thank you!

279
01:14:54,940 --> 01:14:56,532
Oh, my gosh.

280
01:15:09,521 --> 01:15:11,318
That's enough. Let him go, please.

281
01:15:11,456 --> 01:15:12,980
Guys, move back.

282
01:15:14,493 --> 01:15:17,894
<i>Move that truck!
Move that truck! Move that truck!</i>

283
01:15:18,030 --> 01:15:19,463
I gotta move.

284
01:15:19,598 --> 01:15:24,058
<i>Move that truck! Move that truck!
Move that truck!</i>

285
01:15:28,507 --> 01:15:30,839
Clear the street. Clear the street.

286
01:15:33,679 --> 01:15:37,012
Behind the barricade, please.
Get behind the barricade.

287
01:15:44,957 --> 01:15:47,425
You, back. You can't be there.

288
01:15:59,538 --> 01:16:02,166
All right, everybody clap their hands
like this. Come on.

289
01:16:06,378 --> 01:16:08,209
Keep it up, all the way to the top.

290
01:17:25,357 --> 01:17:27,416
Come on, girl.

291
01:18:55,046 --> 01:18:58,345
Give it up for Big Rob!

292
01:19:37,456 --> 01:19:39,947
Ladies and gentlemen, Big Rob.

293
01:19:43,328 --> 01:19:45,489
We're gonna take this time
to introduce

294
01:19:45,630 --> 01:19:48,360
these amazing people behind us,
everybody,

295
01:19:48,500 --> 01:19:50,730
starting with my main man
Jack Lawless on drums.

296
01:19:50,869 --> 01:19:53,394
Jack, show them what's up.
Come on.

297
01:20:00,145 --> 01:20:03,171
Everybody, give it up
for Greg Garbowsky on the bass.

298
01:20:08,487 --> 01:20:12,218
KEVIN: I got Ryan Liestman
on keyboards.

299
01:20:17,229 --> 01:20:18,821
Give it up for my main man
John Taylor,

300
01:20:18,964 --> 01:20:21,728
our musical director, on guitar.

301
01:20:27,506 --> 01:20:31,499
Give it up one more time for our beautiful
and talented string section.

302
01:20:35,480 --> 01:20:38,108
Everybody, Big Rob one more time.
Come on.

303
01:20:40,652 --> 01:20:42,916
Thank you so much.
My name is Kevin Jonas.

304
01:20:43,054 --> 01:20:45,648
Thank you for having me tonight.

305
01:20:46,858 --> 01:20:50,692
One last time, my name is Nick Jonas.
Thank you so much for having us.

306
01:20:51,329 --> 01:20:54,298
Thank you, everybody. I am Joe Jonas.

307
01:20:55,667 --> 01:20:58,329
And we are the Jonas Brothers.

308
01:20:58,770 --> 01:21:00,237
One, two, three.

309
01:21:32,137 --> 01:21:34,605
KEVIN:
Hey, watch it, Joe, hey.

310
01:21:35,173 --> 01:21:38,006
Watch it, Brad. Hey. Hey.

311
01:25:11,689 --> 01:25:13,520
That was so cool.

312
01:25:13,658 --> 01:25:14,716
Let's watch it again.

313
01:25:14,859 --> 01:25:16,486
This 3-D's amazing.

314
01:25:16,628 --> 01:25:18,459
I can almost touch the audience.

315
01:25:18,596 --> 01:25:20,996
Big Rob,
do you have any more popcorn?

316
01:25:22,734 --> 01:25:26,465
- All gone.
- Oh, man.

317
01:25:26,938 --> 01:25:28,929
Hey, you in the red shirt, I see you.

318
01:25:29,073 --> 01:25:31,303
Can you...? Can you give me
a little popcorn, please?

319
01:25:33,304 --> 01:25:40,304
By BackFire

