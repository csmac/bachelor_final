1
00:00:01,380 --> 00:00:05,820
Let's travel all through the worldwide seas

2
00:00:05,820 --> 00:00:08,180
Raising our battle cry

3
00:00:08,180 --> 00:00:12,550
If you've made up your mind, let the signal for departure...

4
00:00:12,550 --> 00:00:14,740
Ring out

5
00:00:22,330 --> 00:00:25,620
We were able to escape from the set course

6
00:00:23,330 --> 00:00:28,840
Translation and timing:
Vegard Aune

7
00:00:25,620 --> 00:00:28,960
Along the boundary between sky and sea

8
00:00:29,260 --> 00:00:34,500
Now we're rowing our way

9
00:00:34,830 --> 00:00:40,760
To a hidden adventure at the dark bottom of the sea

10
00:00:35,470 --> 00:00:40,600
Partially based off
One Piece HQ's manga-translation

11
00:00:40,760 --> 00:00:43,980
Song-translation:
Cornman from Vegapunk Fansubs

12
00:00:40,980 --> 00:00:47,430
Doesn't it sound fun just thinking about it?

13
00:00:47,760 --> 00:00:52,190
Let's travel all through the worldwide seas

14
00:00:52,190 --> 00:00:54,490
Raising our battle cry

15
00:00:54,490 --> 00:00:58,870
If you've made up your mind, let the signal for departure...

16
00:00:58,870 --> 00:01:01,480
Ring out

17
00:01:08,590 --> 00:01:11,700
That feeling we had when we set sail

18
00:01:12,040 --> 00:01:15,210
Is something I'll never forget

19
00:01:15,560 --> 00:01:17,220
Because of that

20
00:01:17,220 --> 00:01:20,800
Nothing will be able to scare me

21
00:01:21,220 --> 00:01:26,970
Now, as the TRIP approaches, our racing heartbeats

22
00:01:27,310 --> 00:01:34,020
Engrave a rhythm of adventure in our shy hearts

23
00:01:34,020 --> 00:01:38,580
Get all the treasures

24
00:01:38,580 --> 00:01:40,830
While laughing out loud

25
00:01:40,830 --> 00:01:45,200
Give them all out at a big party

26
00:01:45,200 --> 00:01:47,870
Making a splash

27
00:02:00,770 --> 00:02:05,060
Fill your empty heart with dreams

28
00:02:05,060 --> 00:02:07,320
Spread your wings

29
00:02:07,320 --> 00:02:11,650
If you color your heart with the blowing wind

30
00:02:11,650 --> 00:02:13,770
You will move on

31
00:02:13,950 --> 00:02:18,280
Get all the treasures

32
00:02:18,280 --> 00:02:20,560
While laughing out loud

33
00:02:20,560 --> 00:02:24,870
Give them all out at a big party

34
00:02:24,870 --> 00:02:27,470
Making a splash

35
00:02:31,730 --> 00:02:32,420
Wealth...

36
00:02:32,710 --> 00:02:33,810
Fame...

37
00:02:33,810 --> 00:02:34,940
Power.

38
00:02:35,950 --> 00:02:38,530
The man who had obtained everything the world had to offer;

39
00:02:38,530 --> 00:02:39,790
The Pirate King...

40
00:02:39,790 --> 00:02:41,240
Gold Roger.

41
00:02:41,770 --> 00:02:43,900
The last words he spoke before his death...

42
00:02:44,190 --> 00:02:45,910
Sent people from all over the world...

43
00:02:45,910 --> 00:02:47,850
To the seas.

44
00:02:48,100 --> 00:02:49,950
My treasures?

45
00:02:50,490 --> 00:02:52,280
If you want them, they're yours!

46
00:02:52,280 --> 00:02:53,830
Look for them...

47
00:02:53,830 --> 00:02:56,660
I left it all at that place.

48
00:02:57,160 --> 00:03:01,690
Men then traveled to the Grand Line, in pursuit of romance.

49
00:03:02,290 --> 00:03:04,210
The world has now entered...

50
00:03:04,210 --> 00:03:05,940
The Great Age of Pirates!

51
00:03:13,460 --> 00:03:17,500
This guy is... One of the Seven Warlords of the Sea!

52
00:03:17,500 --> 00:03:20,760
The Tyrant... Bartholomew Kuma!

53
00:03:21,720 --> 00:03:25,050
If you were to go on a vacation, where would you like to go?

54
00:03:27,010 --> 00:03:28,930
Messing with me...

55
00:03:28,930 --> 00:03:32,310
I was overwhelmed by your huge reputation for a second...

56
00:03:32,310 --> 00:03:34,560
But I have no need to fear you...

57
00:03:35,060 --> 00:03:37,770
Negative Ghost!

58
00:03:37,770 --> 00:03:41,190
And... Special Hollow!

59
00:03:46,450 --> 00:03:51,460
With you snapping at me like that, I doubt you will tell me where Moria is...

60
00:03:54,120 --> 00:03:55,460
That girl...

61
00:03:55,460 --> 00:03:59,670
You are the "Burglar Cat", aren't you? One of Straw Hat's companions...

62
00:04:00,960 --> 00:04:06,390
Huh? What? This guy... He came over here in an instant?!

63
00:04:06,390 --> 00:04:08,010
He warped?

64
00:04:09,010 --> 00:04:12,770
Is he after Luffy? I have to let him know...

65
00:04:14,390 --> 00:04:18,020
I finally caught you... Moria!

66
00:04:22,400 --> 00:04:25,450
I've been chasing his... Shadow?!

67
00:04:25,780 --> 00:04:30,450
DAMMIT! HE TRICKED ME!

68
00:04:34,000 --> 00:04:39,040
Odz + Moria
The Ultimate Combination of Brawn and Brains

69
00:04:43,380 --> 00:04:47,180
Damn, he switched places with his shadow without me noticing!

70
00:04:51,060 --> 00:04:52,350
Why you...

71
00:04:58,060 --> 00:05:01,940
HEY! WAIT! MORIA'S SHADOW!

72
00:05:01,940 --> 00:05:04,110
DON'T JUST LEAVE ME HERE!

73
00:05:04,110 --> 00:05:07,490
WHERE AM I?!

74
00:05:07,490 --> 00:05:09,910
HEY!

75
00:05:14,080 --> 00:05:19,920
He got me! This is bad! I said I'd get back everyone's shadows!

76
00:05:19,920 --> 00:05:22,420
Without my shadow, I'm in trouble too!

77
00:05:22,420 --> 00:05:25,720
Wait, what happens if you don't have your shadow, again?

78
00:05:31,300 --> 00:05:34,810
AAAAAAAAAH!!!

79
00:05:34,810 --> 00:05:39,650
That's right! If we don't do anything, we'll be melted away by the sunlight!

80
00:05:40,730 --> 00:05:43,480
Once the sun rises, we'll be in big trouble!

81
00:05:44,530 --> 00:05:47,570
Damn, I have to hurry before the night ends!

82
00:05:49,820 --> 00:05:52,410
How could this happen?

83
00:05:54,080 --> 00:05:57,540
Anyway, I have to get to wherever Moria's shadow was going!

84
00:06:15,350 --> 00:06:20,900
Zoro, like I said, we've got no choice but to wait for Luffy to beat Moria!

85
00:06:21,940 --> 00:06:26,320
I've finally obtained the Black Blade, Shuusui...

86
00:06:26,900 --> 00:06:30,410
With a huge guy like this in front of us, there's no way I wouldn't try using it!

87
00:06:30,410 --> 00:06:32,450
Wah, you...

88
00:06:32,450 --> 00:06:33,740
Odz!

89
00:06:35,450 --> 00:06:36,620
Huh?

90
00:06:36,620 --> 00:06:39,870
Now, come! I'll fight you!

91
00:06:39,870 --> 00:06:41,880
Gotcha.

92
00:06:41,880 --> 00:06:43,750
You're gonna fight him alone?!

93
00:06:52,050 --> 00:06:55,390
Hey, Zoro! No matter how many times you try it, it's impossible for you alone!

94
00:06:55,390 --> 00:06:57,640
Get back here, and stick to the plan!

95
00:06:59,350 --> 00:07:04,020
Come to think about it, this sword is a lot heavier than Yubashiri...

96
00:07:04,020 --> 00:07:05,690
The black blade, Shuusui...

97
00:07:06,940 --> 00:07:08,570
Hey, Zoro! Out of the way!

98
00:07:09,490 --> 00:07:11,110
If it's this heavy, then...

99
00:07:14,410 --> 00:07:17,830
Awesome! He redirected the punch with sheer force!

100
00:07:18,450 --> 00:07:22,460
Three Sword Style... 108 Pound Cannon!

101
00:07:25,420 --> 00:07:26,340
It's huge!

102
00:07:29,050 --> 00:07:31,300
Damn, it missed him!

103
00:07:31,300 --> 00:07:33,220
He avoided that?!

104
00:07:33,220 --> 00:07:37,850
Shuusui's attack... Absorbed the power of the other two and released all of it in one shot!

105
00:07:38,930 --> 00:07:42,350
But there's too much random destruction around that hole...

106
00:07:43,270 --> 00:07:46,440
That's proof that I'm still not good enough at using this sword...

107
00:07:47,070 --> 00:07:52,740
It's really powerful, but it seems like this sword isn't very well-behaved.

108
00:07:58,620 --> 00:08:02,500
This is some blade you've given me... Swordsman Ryuuma!

109
00:08:05,040 --> 00:08:09,000
Did you do that? Seems like your weapon's pretty powerful.

110
00:08:09,420 --> 00:08:12,340
You bastard!

111
00:08:13,590 --> 00:08:16,760
I'll crush you!

112
00:08:18,810 --> 00:08:21,680
Die!

113
00:08:21,680 --> 00:08:22,850
Zoro's gonna be...!

114
00:08:22,850 --> 00:08:24,640
Is he alright?!

115
00:08:26,560 --> 00:08:30,110
You...

116
00:08:30,820 --> 00:08:34,860
I've gotta get further away from him, or all I can do is dodge!

117
00:08:38,740 --> 00:08:41,540
Yaaah!

118
00:08:48,540 --> 00:08:49,420
This is it!

119
00:08:49,420 --> 00:08:52,170
Three Sword Style 10-

120
00:08:52,170 --> 00:08:54,800
This should crush you too!

121
00:08:59,680 --> 00:09:01,010
Zoro!

122
00:09:06,600 --> 00:09:10,070
Heh heh heh... Now, I've flattened you, right?

123
00:09:10,730 --> 00:09:12,110
Huh?

124
00:09:13,030 --> 00:09:14,650
He ain't there...

125
00:09:14,650 --> 00:09:16,570
Where'd he go?

126
00:09:16,570 --> 00:09:20,870
Dammit, I can't tell with all this smoke...

127
00:09:23,410 --> 00:09:28,170
Three Sword Style 108 Pound Cannon!

128
00:09:30,750 --> 00:09:32,460
That was close...

129
00:09:32,460 --> 00:09:35,510
Hmph, you dodged again huh, damn monster!

130
00:09:35,970 --> 00:09:39,890
Little wimp, you just keep running away!

131
00:09:39,890 --> 00:09:42,930
Zoro! Hey, don't overdo it!

132
00:09:42,930 --> 00:09:48,810
Even if you DO beat him, we'll only get Luffy's shadow back!

133
00:09:48,810 --> 00:09:52,320
We have no idea where yours or Sanji's shadow is!

134
00:09:52,320 --> 00:09:57,200
But if Luffy beats Moria, all the shadows will return at once!

135
00:09:57,200 --> 00:10:00,780
Nobody has to die, and nobody gets hurt fighting a huge zombie like that!

136
00:10:00,780 --> 00:10:04,370
Let's trust Luffy and keep distracting this guy!

137
00:10:04,620 --> 00:10:10,080
I trust him, but he's got plenty of obstacles too.

138
00:10:10,080 --> 00:10:10,830
Huh?

139
00:10:10,830 --> 00:10:14,380
Trickery... An invisible man...

140
00:10:14,380 --> 00:10:18,010
A spirit-being... A shadow master...

141
00:10:18,010 --> 00:10:22,010
This island is full of people with abilities that mess you around.

142
00:10:22,010 --> 00:10:27,060
We don't know if the enemy will even give Luffy a chance to fight properly!

143
00:10:27,060 --> 00:10:28,850
You have a point.

144
00:10:28,850 --> 00:10:29,640
Yeah.

145
00:10:29,640 --> 00:10:39,030
If they delay Luffy until dawn... Him, me, the cook... All three of us will be unable to fight.

146
00:10:39,990 --> 00:10:44,620
But if we can return Luffy to normal before dawn...

147
00:10:44,620 --> 00:10:46,620
Then we'll make it, somehow!

148
00:10:46,620 --> 00:10:50,040
I don't think we even have thirty minutes left until dawn...

149
00:10:50,040 --> 00:10:55,500
But this sea is covered in mist... The sunlight won't reach everywhere.

150
00:10:56,340 --> 00:11:00,590
The sun's about to rise, huh? So the mist is our last hope once dawn comes...

151
00:11:00,590 --> 00:11:05,970
I've got to admit, I'm getting a really bad feeling about this.

152
00:11:15,610 --> 00:11:18,780
Ah, found you, little ones!

153
00:11:23,320 --> 00:11:24,280
What?

154
00:11:25,660 --> 00:11:27,790
What's with this shaking?

155
00:11:29,870 --> 00:11:31,120
What's this?

156
00:11:31,120 --> 00:11:36,000
The ship got caught up in another wierd current, like earlier!

157
00:11:36,340 --> 00:11:39,800
Huh? Oh! Hey, look at the sky!

158
00:11:39,800 --> 00:11:42,380
You can see the early morning-sky!

159
00:11:42,380 --> 00:11:45,140
The mist is starting to clear up!

160
00:11:45,140 --> 00:11:46,680
What's going on?!

161
00:11:46,680 --> 00:11:51,100
Could we have left the Florian Triangle?!

162
00:11:52,020 --> 00:11:56,150
It must be because Odz messed around with the helm before!

163
00:11:56,150 --> 00:12:00,820
Damn him... WHAT WAS THE POINT OF THAT?!

164
00:12:03,820 --> 00:12:05,950
This is a really bad situation...

165
00:12:05,950 --> 00:12:08,620
Thriller Bark has come out from the mist!

166
00:12:08,620 --> 00:12:10,370
We're doomed!

167
00:12:10,370 --> 00:12:13,750
The sun's about to rise! Then we'll die too!

168
00:12:13,750 --> 00:12:16,250
It's still too early to give up!

169
00:12:16,250 --> 00:12:19,590
Our "rays of hope" are fighting right now!

170
00:12:19,590 --> 00:12:22,130
Hey, look at that!

171
00:12:22,590 --> 00:12:25,760
Moria's shadow! It's flying towards the mansion!

172
00:12:25,760 --> 00:12:27,550
What about it?

173
00:12:27,550 --> 00:12:32,020
Idiot, don't you understand?! Straw Hat is chasing him...

174
00:12:32,020 --> 00:12:34,690
...So if he follows that shadow, he'll end up here!

175
00:12:34,690 --> 00:12:38,230
In other words, if we just wait patiently here...

176
00:12:38,230 --> 00:12:39,940
Ah, I get it!

177
00:12:39,940 --> 00:12:45,450
Okay, tell the captain and the others! We'll make sure to stop Straw Hat here!

178
00:12:45,450 --> 00:12:46,740
Yeah, I got it!

179
00:12:48,240 --> 00:12:52,290
Moria-sama! It's terrible!

180
00:12:52,290 --> 00:12:56,920
Thriller Bark has left the mist!

181
00:12:56,920 --> 00:12:58,790
It's Odz's fault!

182
00:13:03,670 --> 00:13:07,050
Moria-sama!

183
00:13:07,550 --> 00:13:11,890
I know, what about it?

184
00:13:11,890 --> 00:13:16,520
We're pirates! It doesn't matter where we sail.

185
00:13:17,150 --> 00:13:19,860
Uh, but...

186
00:13:19,860 --> 00:13:25,780
Anyway, I have an important guest right now, so leave!

187
00:13:25,780 --> 00:13:29,280
Right, "Tyrant" Kuma?

188
00:13:29,280 --> 00:13:34,580
The only one of the Warlords who actually obeys the government!

189
00:13:34,580 --> 00:13:41,540
I would say, "Stop distribution of this once it's been licensed", but that would be retarded, so...

190
00:13:41,540 --> 00:13:49,010
Please support the series by buying the DVDs when they become available.

191
00:13:49,760 --> 00:13:52,350
Right, "Tyrant" Kuma?

192
00:13:52,350 --> 00:13:57,350
The only one of the Warlords who actually obeys the government!

193
00:13:57,350 --> 00:14:03,110
The government propably appreciates having you around, but what are you up to?

194
00:14:03,110 --> 00:14:06,440
You're the one who worries me the most out of you guys.

195
00:14:06,440 --> 00:14:09,200
If you were to go on a vacation, where would you like to go?

196
00:14:09,200 --> 00:14:14,330
Hey, drop that! I know what your ability is.

197
00:14:14,330 --> 00:14:17,540
Did you come here to fight me?

198
00:14:17,540 --> 00:14:22,130
Well, speak up! Why are you here?

199
00:14:22,130 --> 00:14:24,550
I have an important message for you.

200
00:14:24,550 --> 00:14:25,960
Message?

201
00:14:25,960 --> 00:14:32,680
The successor to the demoted Warlord, Crocodile, has been chosen.

202
00:14:32,680 --> 00:14:36,140
Kishishishi... Why didn't you say so sooner?

203
00:14:36,140 --> 00:14:41,060
Which sea did they pick him up from, and who is it? They've got a lot of pirates to choose from.

204
00:14:41,060 --> 00:14:44,980
The successor's name is Marshall D. Teach.

205
00:14:44,980 --> 00:14:47,860
The man commonly known as Blackbeard.

206
00:14:47,860 --> 00:14:50,950
Blackbeard? Never heard of him.

207
00:14:50,950 --> 00:14:58,540
He used to be part of Whitebeard's first division. He has caused a lot of trouble in the world...

208
00:14:59,620 --> 00:15:01,120
Dark Whirlpool!

209
00:15:04,040 --> 00:15:05,840
Holy Flame, Unknown Fire!

210
00:15:07,010 --> 00:15:08,380
Damn!

211
00:15:15,050 --> 00:15:17,100
Die in darkness!

212
00:15:17,100 --> 00:15:19,480
Great Blaze Commandment!

213
00:15:25,570 --> 00:15:27,360
Flame Emperor!

214
00:15:46,790 --> 00:15:49,630
I guess the news haven't spread to the sea of mist...

215
00:15:49,630 --> 00:15:52,800
He was accepted after proving his power.

216
00:15:52,800 --> 00:15:56,300
So, what was his former bounty?

217
00:15:56,300 --> 00:15:57,510
Zero.

218
00:15:57,510 --> 00:16:05,230
I get it, an unknown value... I'm surprised the government noticed him, kishi...

219
00:16:05,230 --> 00:16:09,230
Well, it's good that they've filled the missing spot, right?

220
00:16:09,690 --> 00:16:13,700
Now, the world's equlibrium or whatever should be restored, right?

221
00:16:14,610 --> 00:16:21,410
No, I imagine it would be the exact opposite. Well, no matter.

222
00:16:21,410 --> 00:16:22,160
Hmm?

223
00:16:22,160 --> 00:16:26,460
The government is more worried about something else.

224
00:16:26,460 --> 00:16:33,800
Ever since the Enies Lobby-incident, they've been keeping a close eye on the Straw Hats's movements.

225
00:16:33,800 --> 00:16:40,970
If they followed the log from Water 7 to Fishman Island, there was a high chance they would end up here.

226
00:16:40,970 --> 00:16:45,140
So, do you understand what they're afraid of?

227
00:16:45,140 --> 00:16:46,560
Hmm?

228
00:16:46,940 --> 00:16:55,660
The government fears that another one of the Seven Warlords will fall at Straw Hat's hands.

229
00:16:57,410 --> 00:17:01,080
Are you kidding me?! They're worried about me?!

230
00:17:02,250 --> 00:17:07,920
You're telling me you came here to protect me from a bunch of puny little pirates?!

231
00:17:07,920 --> 00:17:12,010
If necessary, I would not object to assisting you.

232
00:17:12,630 --> 00:17:16,590
WHO THE HELL DO YOU THINK YOU'RE TALKING TO?!?!

233
00:17:21,220 --> 00:17:28,150
You seriously think I could be beaten by a tiny, inexperienced crew like that?!

234
00:17:28,150 --> 00:17:30,900
We can not be 100% sure of how a battle will end.

235
00:17:30,900 --> 00:17:37,820
During the Enies Lobby-incident... Who would have guessed that Rob Lucci would lose?

236
00:17:38,740 --> 00:17:45,660
JET GATLING!!

237
00:18:05,270 --> 00:18:13,440
So you're telling me that the government is so afraid of this crew that they're sending two Warlords to deal with them?! Huh?!

238
00:18:13,440 --> 00:18:19,320
I am only here to give the report. I have no specific orders to help you.

239
00:18:19,320 --> 00:18:25,200
Then take a good look at what's gonna happen, and tell the government this:

240
00:18:25,790 --> 00:18:34,380
"The Straw Hat Pirate crew, which caused you so much trouble, have all become part of Gecko Moria's Zombie army!"

241
00:18:34,380 --> 00:18:37,510
Kishishishishi!

242
00:18:40,180 --> 00:18:40,970
The mist...

243
00:18:41,930 --> 00:18:43,470
This is terrible.

244
00:18:43,470 --> 00:18:47,140
The mist was our only hope!

245
00:18:47,140 --> 00:18:54,190
Who managed to set things up so the mist cleared up now?! Now, the morning sun will hit us straight on!

246
00:18:54,190 --> 00:18:59,570
Damn, in this awful situation, we have to hurry and do something about Odz...

247
00:19:01,910 --> 00:19:06,620
Kishishishi... Unexpected, but the pure night sky is beautiful.

248
00:19:07,410 --> 00:19:10,500
M-Moria... It's Moria!

249
00:19:10,500 --> 00:19:12,670
Where?! Where?!

250
00:19:12,670 --> 00:19:14,590
Hey, quit joking!

251
00:19:14,590 --> 00:19:20,300
It's almost dawn. Do you guys have time to be standing around?

252
00:19:20,300 --> 00:19:23,760
Why are you here? What about Luffy?!

253
00:19:23,760 --> 00:19:27,010
Huh? I'm hearing Master's voice...

254
00:19:27,010 --> 00:19:30,350
Hey, wait, Usopp! Where did you say Moria was?

255
00:19:30,350 --> 00:19:34,560
He's right here! Look! His stomach! He's inside Odz's stomach!

256
00:19:34,560 --> 00:19:35,860
His stomach?

257
00:19:36,320 --> 00:19:37,900
There he is!

258
00:19:37,900 --> 00:19:42,910
Kishishishishishi...

259
00:19:44,120 --> 00:19:48,080
Damn that Hogback... Is this his doing, too?!

260
00:19:48,080 --> 00:19:50,000
What's Moria doing there?

261
00:19:50,000 --> 00:19:53,790
Then, did they manage to trick Luffy after all?

262
00:19:53,790 --> 00:19:55,170
Or did he...

263
00:19:55,170 --> 00:19:57,840
No way Luffy would lose!

264
00:19:58,550 --> 00:19:59,340
Right.

265
00:19:59,340 --> 00:20:04,760
Wierd... I heard Master's voice, but...

266
00:20:04,760 --> 00:20:11,020
Ah! There! A cockpit? Why's there a cockpit in my stomach?

267
00:20:11,020 --> 00:20:14,650
Kishishi... Huh? Hey!

268
00:20:14,650 --> 00:20:18,690
Heh, this is pretty well made... I wanna try ridin' it too...

269
00:20:19,110 --> 00:20:23,280
Hey! Moron! Don't put your fingers inside the cockpit!

270
00:20:23,280 --> 00:20:25,870
Yes, Master!

271
00:20:25,870 --> 00:20:30,160
This is awesome! I'm like a robot!

272
00:20:31,290 --> 00:20:33,040
I'm a Giant Robot!

273
00:20:37,670 --> 00:20:38,500
That was close!

274
00:20:38,500 --> 00:20:43,760
I'm the Giant Robot: umm... Odz Bomber! Nice to meetcha!

275
00:20:43,760 --> 00:20:45,640
What do you mean, Odz Bomber?!

276
00:20:45,640 --> 00:20:46,840
Odz Bomber?

277
00:20:46,840 --> 00:20:52,390
How can a zombie be a robot?! A robot needs to be more like.. Clang! Clang!

278
00:20:52,390 --> 00:20:57,440
Heh heh! Giant Robot... This increases the tension!

279
00:20:57,440 --> 00:21:01,480
Odz! Cut it out! Stop shaking!

280
00:21:01,900 --> 00:21:04,400
Yes, Master!

281
00:21:04,400 --> 00:21:09,580
Kishishishi... Now, I'll give you a chance to fight me!

282
00:21:09,580 --> 00:21:13,620
If you beat me, I'll release all the shadows!

283
00:21:13,620 --> 00:21:21,090
Feel free to attack all at once! However, if you don't beat Odz, you can't touch me!

284
00:21:21,960 --> 00:21:24,760
That bastard... That's cheap!

285
00:21:24,760 --> 00:21:30,260
If we don't beat Moria, we can't stop Odz... But Moria is inside Odz!

286
00:21:30,260 --> 00:21:33,310
Nah, our target just got a lot simpler.

287
00:21:33,310 --> 00:21:35,350
We've just gotta do this!

288
00:21:35,350 --> 00:21:40,400
Usopp! That tiny amount didn't work, so go find a mountain of salt!

289
00:21:40,650 --> 00:21:43,230
We'll purify Odz with that!

290
00:21:43,230 --> 00:21:46,240
We'll just weaken him as much as we can!

291
00:21:46,240 --> 00:21:48,820
Right! Leave this to us!

292
00:21:54,330 --> 00:21:55,620
Hurry!

293
00:21:55,620 --> 00:21:56,790
Alright, I got it!

294
00:21:56,790 --> 00:21:57,920
I'll go get it!

295
00:21:57,920 --> 00:22:03,050
I'm sure there was a kitched back in the mansion at the beginning! There should be more than enough salt there!

296
00:22:03,050 --> 00:22:07,470
More than enough, right? Salt, huh?

297
00:22:07,470 --> 00:22:15,140
Hey, Odz! Take out the Long-Nose and the route to the mansion! Don't let him get to the kitchen!

298
00:22:15,140 --> 00:22:17,600
Yes, Master!

299
00:22:18,190 --> 00:22:19,770
Usopp!

300
00:22:21,730 --> 00:22:24,480
What's- AAAAAAH!!!

301
00:22:27,950 --> 00:22:30,570
Damn! Usopp!

302
00:22:32,450 --> 00:22:34,330
Usopp!

303
00:22:34,330 --> 00:22:38,460
Kishishishishi...

304
00:22:38,460 --> 00:22:43,380
Damn! With Moria added into the mix, now Odz has some brains on his side too!

305
00:22:43,380 --> 00:22:47,760
Usopp! Where are you! Speak to me!

306
00:22:47,760 --> 00:22:49,130
He is fine!

307
00:22:54,310 --> 00:22:58,060
I thought we might need a lot of salt, so I went to fetch it!

308
00:22:58,060 --> 00:22:59,600
BROOK?!

309
00:22:59,600 --> 00:23:01,560
You saved me...

310
00:23:02,270 --> 00:23:04,400
I am your Brook!

311
00:23:10,110 --> 00:23:11,200
Who're you guys?

312
00:23:11,200 --> 00:23:12,990
We're the Risky Brothers!

313
00:23:12,990 --> 00:23:15,410
We're gonna give you an incredible power!

314
00:23:15,410 --> 00:23:18,910
What's that? Stop it, AAAGH!

315
00:23:18,910 --> 00:23:20,080
HOW'S THAT?!

316
00:23:20,080 --> 00:23:24,420
Yeah! The power is overflowing! I can't stop!

317
00:23:25,040 --> 00:23:26,090
Next time, on One Piece:

318
00:23:26,090 --> 00:23:29,510
A Crazy Strategy to Turn the Tables
The Creation of Nightmare Luffy

319
00:23:29,510 --> 00:23:31,800
I'm gonna be the Pirate King!

