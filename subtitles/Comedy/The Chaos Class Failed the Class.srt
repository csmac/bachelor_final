1
00:00:00,125 --> 00:00:07,925
Subs by Raceman and Traff
www.forom.com

2
00:00:08,050 --> 00:00:09,927
- Hey, man.
- Hey.

3
00:00:10,093 --> 00:00:11,762
You okay?

4
00:00:11,929 --> 00:00:14,264
Lina and I had our first date
a month ago today,

5
00:00:14,431 --> 00:00:17,017
and I want to get her
something to celebrate.

6
00:00:17,184 --> 00:00:19,019
I want to get her a trumpet.

7
00:00:19,186 --> 00:00:20,187
Really?

8
00:00:20,354 --> 00:00:24,399
I usually get'em
flowers or drunk.

9
00:00:25,692 --> 00:00:29,696
But if you score with the trumpet,
you let me know.

10
00:00:29,866 --> 00:00:30,941
On our first date, she told me

11
00:00:31,113 --> 00:00:33,991
she always wanted to play one,
but they're so expensive.

12
00:00:34,157 --> 00:00:38,123
Hey, you know what's not?
Drug store vodka.

13
00:00:38,289 --> 00:00:39,202
Ahh.

14
00:00:39,375 --> 00:00:43,872
I love a gift that makes a girl say,
"Where am I? What happened?"

15
00:00:44,045 --> 00:00:46,582
Heads up.

16
00:00:49,387 --> 00:00:51,423
Oh, was I supposed to--

17
00:00:51,596 --> 00:00:52,801
What's with the balls?

18
00:00:52,978 --> 00:00:54,932
Memorabilia convention.
And I'm running late.

19
00:00:55,098 --> 00:00:56,513
So I need you guys
to sign my name on 'em.

20
00:00:56,686 --> 00:00:57,091
Cool.

21
00:00:57,265 --> 00:01:00,890
I don't see anything
unethical about that.

22
00:01:01,068 --> 00:01:05,770
Is that sarcasm?
'Cause I do not get sarcasm.

23
00:01:07,358 --> 00:01:11,020
Which is surprising,
because you're so smart.

24
00:01:11,195 --> 00:01:13,611
I know!

25
00:01:14,746 --> 00:01:15,702
Where's the convention?

26
00:01:15,867 --> 00:01:16,780
Atlantic City.

27
00:01:16,955 --> 00:01:18,662
Oh, man,
it's a sweet deal.

28
00:01:18,828 --> 00:01:22,040
I just spend an hour chatting up the fans,
and then it's all the free drinks

29
00:01:22,206 --> 00:01:25,622
and free food
I can shove down my cake hole.

30
00:01:25,798 --> 00:01:28,380
You're charming.

31
00:01:28,547 --> 00:01:32,171
I appreciate you saying so.

32
00:01:33,396 --> 00:01:37,381
They actually pay you to eat, drink,
and get your ass kissed, wow.

33
00:01:37,558 --> 00:01:39,470
You want to come along?

34
00:01:39,647 --> 00:01:40,851
Are you messing with me?

35
00:01:41,015 --> 00:01:42,433
Messing with him about what?

36
00:01:42,606 --> 00:01:43,852
Oh, Dunc's coming to AC with me.

37
00:01:44,025 --> 00:01:48,151
I'm gonna shove free things down
my cake hole.

38
00:01:48,318 --> 00:01:51,150
So it's a fancy trip.

39
00:01:51,325 --> 00:01:53,150
How about you, champ?
You like Atlantic City?

40
00:01:53,326 --> 00:01:54,443
I've never been.

41
00:01:54,615 --> 00:01:57,153
What?
You've never been to AC?

42
00:01:57,325 --> 00:01:59,201
Well, that's it.
You're going.

43
00:01:59,366 --> 00:02:01,581
Red's coming.

44
00:02:01,746 --> 00:02:03,123
Um, can I talk to you
for a second?

45
00:02:03,298 --> 00:02:06,080
Yeah, sure, what's up?

46
00:02:06,257 --> 00:02:08,423
Thank God you're going.
I need you to do me a huge favor.

47
00:02:08,585 --> 00:02:09,370
Yeah, anything.

48
00:02:09,547 --> 00:02:10,962
You gotta make sure Yonk
doesn't gamble.

49
00:02:11,136 --> 00:02:13,302
Yeah, I'm not doing that.

50
00:02:13,468 --> 00:02:15,300
I'm begging you.
He's got a huge problem.

51
00:02:15,466 --> 00:02:18,720
The last time
he went to AC, $23,000.

52
00:02:18,887 --> 00:02:20,642
He won $23,000?

53
00:02:20,808 --> 00:02:25,641
Yeah, that's my problem.
My husband is a compulsive winner.

54
00:02:25,817 --> 00:02:30,100
Please?
Will you at least try? For me?

55
00:02:31,816 --> 00:02:32,691
Yeah.

56
00:02:32,865 --> 00:02:35,110
You know there's nothing
I wouldn't do for you.

57
00:02:35,286 --> 00:02:37,691
Thank you.

58
00:02:37,865 --> 00:02:39,701
- How are those balls coming?
- Great.

59
00:02:39,866 --> 00:02:43,873
Just knowing I'm helping you get richer
makes the time fly by.

60
00:02:44,036 --> 00:02:46,793
That's the spirit.

61
00:02:46,955 --> 00:02:50,370
I could do that all day.

62
00:03:14,986 --> 00:03:18,071
Mommy, somebody's ready
to show you her costume.

63
00:03:18,246 --> 00:03:19,361
Oprah!

64
00:03:19,538 --> 00:03:21,910
Ta-da!

65
00:03:23,628 --> 00:03:27,661
She's a...blind girl
drinking juice.

66
00:03:27,838 --> 00:03:31,081
Hello! Audrey Hepburn.
Breakfast at Tiffany's.

67
00:03:31,258 --> 00:03:34,082
Oh! I love it.

68
00:03:35,756 --> 00:03:37,921
Oh.

69
00:03:38,965 --> 00:03:40,220
I don't get it.

70
00:03:40,385 --> 00:03:44,470
You don't have to get it.
You just have to sell it.

71
00:03:44,646 --> 00:03:48,680
Absolutely.
I'm leaving right now.

72
00:03:48,977 --> 00:03:51,522
Guess what?
Steve Martinez just went into rehab.

73
00:03:51,698 --> 00:03:53,271
Yes!

74
00:03:53,446 --> 00:03:55,020
I'm gonna cover his spot
on the midday news today,

75
00:03:55,196 --> 00:03:58,943
and it's between me and Sandy Tanaka
to take over as weekend anchor.

76
00:03:59,115 --> 00:04:02,031
Oh!
Oh, my God.

77
00:04:04,037 --> 00:04:05,200
This is our moment.

78
00:04:05,375 --> 00:04:06,582
I know.

79
00:04:06,747 --> 00:04:09,950
Weekend  anchor!
Weekend anchor!

80
00:04:10,125 --> 00:04:12,161
Okay, all right,
let's not get ahead of ourselves.

81
00:04:12,338 --> 00:04:13,753
Sandy Tanaka is gonna be
tough to beat.

82
00:04:13,927 --> 00:04:14,121
Oh, yeah.

83
00:04:14,296 --> 00:04:16,880
With her Asian demographic
and her big fake boobs.

84
00:04:17,045 --> 00:04:18,713
Oh, you've noticed them,
have you?

85
00:04:18,886 --> 00:04:23,381
Oh, honey, relax.
You know I'm an ass man.

86
00:04:25,308 --> 00:04:28,061
Okay, come on,
you've gotta go change.

87
00:04:28,226 --> 00:04:31,352
I'm thinking the black Von Furstenberg
with the new Jimmy Choo's.

88
00:04:31,526 --> 00:04:34,231
Oh, no, I can barely walk in those.
They're so tall and spiky.

89
00:04:34,396 --> 00:04:35,942
Can I just wear these?

90
00:04:36,108 --> 00:04:38,610
Black suit, brown shoes?
Sure you can.

91
00:04:38,776 --> 00:04:39,730
"Hello, Sandy Tanaka.

92
00:04:39,907 --> 00:04:42,952
"Get your big fake rack down here.
You got the job."

93
00:04:43,117 --> 00:04:47,412
You're right, you're right.
What would I do without you?

94
00:04:47,576 --> 00:04:49,952
Clash.

95
00:04:50,746 --> 00:04:52,913
That was so awesome.

96
00:04:53,087 --> 00:04:54,833
All those players
I grew up watching.

97
00:04:55,006 --> 00:04:56,922
All those legends
in the same room.

98
00:04:57,085 --> 00:05:02,260
You know, I couldn't help thinking,
"They got so fat."

99
00:05:02,425 --> 00:05:05,682
You know, how I keep it off?
I only eat meat.

100
00:05:05,848 --> 00:05:08,803
Well, that's not a heart attack
waiting to happen.

101
00:05:08,977 --> 00:05:10,560
Well, let's hope you're right.

102
00:05:10,726 --> 00:05:12,981
Right, let's hit the tables.

103
00:05:13,148 --> 00:05:15,481
Oh, we're gambling, huh?

104
00:05:15,646 --> 00:05:17,772
Oh, I'm not.
I promised Nic I wouldn't gamble.

105
00:05:17,945 --> 00:05:19,860
But it's Big Rick here's
first time in AC.

106
00:05:20,026 --> 00:05:21,781
Thought he ought to have
a little taste of the action.

107
00:05:21,946 --> 00:05:27,610
Oh, I don't need to taste the action.
I assume it tastes like chicken.

108
00:05:27,787 --> 00:05:30,373
I'll tell you what I do.
I'll spot you 100 bucks each, all right?

109
00:05:30,536 --> 00:05:34,123
Just as a thank you for
signing my balls.

110
00:05:34,298 --> 00:05:37,670
How you doing, darling?

111
00:05:40,045 --> 00:05:42,590
Danny, I'm gonna do a walk and talk
along the fence there.

112
00:05:42,755 --> 00:05:45,422
And make sure you're wide enough
to see my shoes.

113
00:05:45,597 --> 00:05:49,013
Jack said he wanted you
in there with the animals.

114
00:05:49,186 --> 00:05:52,850
Actually in there with them?

115
00:05:53,028 --> 00:05:56,940
Well, if that's what Jack wants.

116
00:05:58,487 --> 00:06:02,362
Hey, guys!
Hello, little fella.

117
00:06:03,405 --> 00:06:05,571
Oh!

118
00:06:06,496 --> 00:06:09,493
Aren't you musty?

119
00:06:11,955 --> 00:06:15,791
All right, Dr. Doolittle.
We've live in 3, 2...

120
00:06:15,958 --> 00:06:18,001
Halloween is one again upon us.

121
00:06:18,178 --> 00:06:25,461
And here at the Upper Darby Autumn Festival,
there's a petting zoo your little goblins
won't want to miss.

122
00:06:25,638 --> 00:06:31,050
Already the festival's attracted a record...
number of...

123
00:06:31,646 --> 00:06:33,602
Oh!

124
00:06:33,768 --> 00:06:35,270
Oh!

125
00:06:35,436 --> 00:06:38,231
Get a hold of yourself, sweetie.
Now say it again.

126
00:06:38,396 --> 00:06:39,311
You did what?

127
00:06:39,487 --> 00:06:43,362
I  impaled a bunny with my shoe.

128
00:06:44,078 --> 00:06:45,483
Oh, my God.

129
00:06:45,655 --> 00:06:47,111
Okay, honey,
I'm coming to get you.

130
00:06:47,286 --> 00:06:48,783
Where are you?

131
00:06:48,955 --> 00:06:52,122
Boys,  please keep it down!
My wife is very upset.

132
00:06:52,288 --> 00:06:53,450
She just killed a bunny.

133
00:06:53,627 --> 00:06:57,042
- A bunny?!
- His wife?!

134
00:06:58,088 --> 00:06:59,412
It was so horrible.

135
00:06:59,586 --> 00:07:02,883
Oh, honey, I know.
But it's gonna be okay.

136
00:07:03,047 --> 00:07:06,211
And it's a holiday.
Maybe not that many people were watching.

137
00:07:06,387 --> 00:07:10,220
And Halloween got a little scarier today
at a Philadelphia petting zoo.

138
00:07:10,386 --> 00:07:14,603
We should warn you.
This footage may not be appropriate for children.

139
00:07:14,768 --> 00:07:20,852
Here at the Upper Darby Autumn Festival,
there's a petting zoo your little goblins
won't want to miss.

140
00:07:21,027 --> 00:07:23,480
Oh! Aah!

141
00:07:23,738 --> 00:07:26,322
Holy crap!

142
00:07:28,037 --> 00:07:30,992
I know.
It's already online.

143
00:07:31,156 --> 00:07:35,783
There's a petting zoo your little goblins
won't want to miss.

144
00:07:35,953 --> 00:07:38,201
Ugh!

145
00:07:48,137 --> 00:07:51,053
Guess who I am.

146
00:07:51,229 --> 00:07:54,302
Old-timey lesbian?

147
00:07:54,807 --> 00:07:56,182
Here's a hint.

148
00:07:56,356 --> 00:08:00,731
Uh, what am gonna do about
World War II?

149
00:08:04,866 --> 00:08:09,191
Oh, no, I can't believe
I've got Polio!

150
00:08:11,455 --> 00:08:16,993
Have you met my wife Eleanor?
She's got a face like a boat.

151
00:08:19,705 --> 00:08:22,122
Whoosh!

152
00:08:22,758 --> 00:08:25,042
Greetings, citizens.

153
00:08:25,215 --> 00:08:29,713
So what does the S stand for?
"Seriously"?

154
00:08:30,017 --> 00:08:32,173
I think you can
pull it off, Superman.

155
00:08:32,346 --> 00:08:36,552
Thank you...
wheelchair transvestite?

156
00:08:38,267 --> 00:08:41,023
So, Superman?
Really?

157
00:08:41,187 --> 00:08:42,641
- What?
- Oh, nothing.

158
00:08:42,818 --> 00:08:46,312
It's just kind of nice knowing
I could kick Superman's ass.

159
00:08:46,488 --> 00:08:47,150
Excuse me?

160
00:08:47,317 --> 00:08:49,361
Ignore her.
She's all hopped up on candy.

161
00:08:49,537 --> 00:08:54,402
Yeah, I ate so many Skittles
I peed the rainbow.

162
00:08:55,706 --> 00:08:58,411
Wow, you're actually claiming
that you could kick my ass?

163
00:08:58,586 --> 00:09:00,992
You guys, can we please go
to this party?

164
00:09:01,166 --> 00:09:03,040
I'm really sweating in this suit.

165
00:09:03,217 --> 00:09:04,332
You don't think that I could?

166
00:09:04,508 --> 00:09:07,631
Uh, well, I'm bigger than you.
I am stronger than you.

167
00:09:07,796 --> 00:09:09,171
I wrestled in high school.

168
00:09:09,347 --> 00:09:11,091
Oh, a wrestler, huh? Mm-hmm.

169
00:09:11,268 --> 00:09:15,091
Well, then it shouldn't be a problem
if somebody were to-- Hatcha!

170
00:09:15,265 --> 00:09:16,551
What the hell was that?

171
00:09:16,727 --> 00:09:19,390
Sorry.
What I mean to do was this!

172
00:09:19,566 --> 00:09:22,932
You are out of control.

173
00:09:23,437 --> 00:09:24,392
Face it.
You can't beat me.

174
00:09:24,575 --> 00:09:27,981
I've got special powers.

175
00:09:29,825 --> 00:09:31,652
Oh, look out, Superman!

176
00:09:31,826 --> 00:09:36,200
Ow! Ow! Let me go!
Let me go, let me go!

177
00:09:37,167 --> 00:09:40,202
All right, but first admit
I am the Man of Steel.

178
00:09:40,376 --> 00:09:42,083
Yeah, that's not gonna happen.

179
00:09:42,246 --> 00:09:43,663
Fine, I've got nothing but time.

180
00:09:43,835 --> 00:09:44,792
Okay, let go, come on.

181
00:09:44,966 --> 00:09:47,213
Not until you say
I am Superman.

182
00:09:47,376 --> 00:09:49,633
Hmm?

183
00:09:50,387 --> 00:09:52,842
I'm hot.

184
00:09:54,675 --> 00:09:57,513
Place your bets.

185
00:09:57,687 --> 00:09:59,931
Okay, here's what
I'm thinking.

186
00:10:00,107 --> 00:10:02,563
I keep the money...

187
00:10:02,727 --> 00:10:05,312
That's it.

188
00:10:05,895 --> 00:10:07,393
Come on, man,
you gotta play.

189
00:10:07,567 --> 00:10:10,403
Besides, it's my 100 bucks.
You know, you just keep the profits.

190
00:10:10,567 --> 00:10:13,233
Okay, okay.

191
00:10:14,575 --> 00:10:16,073
Hit me.

192
00:10:16,245 --> 00:10:19,360
And...not my money.

193
00:10:20,077 --> 00:10:21,622
It's to you, Rick.
Want a card?

194
00:10:21,787 --> 00:10:23,623
I don't know.
How do I do this?

195
00:10:23,795 --> 00:10:25,330
You gotta get a higher number
than the dealer.

196
00:10:25,508 --> 00:10:26,663
But you can't go over 21.

197
00:10:26,837 --> 00:10:30,882
Okay, I have 14.
What do you have?

198
00:10:32,175 --> 00:10:33,212
I can't tell you that, sir.

199
00:10:33,388 --> 00:10:36,091
Well, this is a nightmare.

200
00:10:36,267 --> 00:10:37,260
Ask for a card.

201
00:10:37,428 --> 00:10:40,262
Can I have another card,
please?

202
00:10:40,435 --> 00:10:41,051
21.

203
00:10:41,226 --> 00:10:42,351
21 is 21, right?

204
00:10:42,517 --> 00:10:43,972
- Yeah, man, it's 21!
- I won?

205
00:10:44,148 --> 00:10:46,932
Oh, my God!
I won $10.

206
00:10:47,108 --> 00:10:47,943
You having fun now?

207
00:10:48,108 --> 00:10:51,522
Yeah--not with the ring.

208
00:10:56,155 --> 00:10:57,993
-  Hi.
- Hey.

209
00:10:58,157 --> 00:11:00,862
We were on our way to a party.
We thought we'd see how she's doing.

210
00:11:01,038 --> 00:11:02,492
You didn't have to do that.

211
00:11:02,665 --> 00:11:04,703
That's what I said.

212
00:11:04,875 --> 00:11:06,330
Honey, look who's here.

213
00:11:06,505 --> 00:11:09,500
Hey, how are you?

214
00:11:09,717 --> 00:11:12,002
It's only the worst day of
my entire life.

215
00:11:12,176 --> 00:11:17,630
But I'm getting through it.
Apparently I drink Scotch now.

216
00:11:18,266 --> 00:11:19,300
Where did you see it?

217
00:11:19,477 --> 00:11:20,683
On the Internet.

218
00:11:20,845 --> 00:11:23,391
It--it's on the Internet?

219
00:11:23,557 --> 00:11:29,522
Um, yeah, it's, um, called
"Can I See This Bunny in a Size Eight?"

220
00:11:31,818 --> 00:11:35,983
Well, there you go.
It's everywhere.

221
00:11:36,328 --> 00:11:38,612
I told you
it was over.

222
00:11:38,785 --> 00:11:40,613
What's over?

223
00:11:40,788 --> 00:11:45,700
I was up for weekend anchor.
It was between me and Sandy Tanaka.

224
00:11:45,875 --> 00:11:50,251
Oh, we love Sandy Tanaka!
Why would I do that?

225
00:11:50,417 --> 00:11:52,713
There must be some way
to fix this.

226
00:11:52,887 --> 00:11:56,043
Well, hey, what about if you, uh,
you make a big apology on the air.

227
00:11:56,215 --> 00:11:58,461
You know, maybe go back
to the petting zoo.

228
00:11:58,638 --> 00:12:02,513
Maybe you should wear sneakers.

229
00:12:03,307 --> 00:12:05,142
I can't go back there.

230
00:12:05,308 --> 00:12:09,561
Sweetie, you've gotta give it a try.
You want it so much.

231
00:12:09,737 --> 00:12:14,313
Call Jack.
You can still get on the 11:00 news.

232
00:12:14,487 --> 00:12:16,773
Okay.

233
00:12:17,866 --> 00:12:20,862
Okay,
I'll do it.

234
00:12:23,867 --> 00:12:26,161
Ahoy.

235
00:12:27,287 --> 00:12:28,742
Unh, let me go!

236
00:12:28,916 --> 00:12:30,162
Let me go who?

237
00:12:30,335 --> 00:12:31,712
Oh, you are so lame!

238
00:12:31,875 --> 00:12:35,750
Hey, Sweaty Roosevelt,
help me out over here.

239
00:12:35,926 --> 00:12:39,710
I'm sorry,
but I've got Polio.

240
00:12:41,228 --> 00:12:44,383
You know, I think it's interesting
that you feel the need to be called Superman.

241
00:12:44,557 --> 00:12:46,262
I mean really, what is that--
Oh wait, ow, ow, ow!

242
00:12:46,437 --> 00:12:47,052
No, no, no,
I've got a cramp.

243
00:12:47,226 --> 00:12:48,313
Seriously, let me go.

244
00:12:48,478 --> 00:12:50,433
She's totally faking.
She always used to try this.

245
00:12:50,608 --> 00:12:51,931
Oh, not faking,
not faking!

246
00:12:52,105 --> 00:12:52,643
It really hurts!

247
00:12:52,817 --> 00:12:53,603
Just say it!

248
00:12:53,778 --> 00:12:55,652
Oh, my God,
will you stop being such an ass?

249
00:12:55,815 --> 00:12:56,653
Look for tears.

250
00:12:56,815 --> 00:13:00,573
She can fake the noises,
but she can't make tears.

251
00:13:00,736 --> 00:13:02,533
I can't see her face.

252
00:13:02,695 --> 00:13:04,702
Let me go!

253
00:13:04,865 --> 00:13:08,371
- Totally dry.
- Damn it!

254
00:13:09,795 --> 00:13:13,043
- 21.
- I can't lose!

255
00:13:14,337 --> 00:13:15,171
How much do I have now?

256
00:13:15,336 --> 00:13:16,332
You're up 700.

257
00:13:16,506 --> 00:13:17,713
$700?

258
00:13:17,886 --> 00:13:18,423
Yeah.

259
00:13:18,597 --> 00:13:20,210
Do you have any idea
how much money that is?

260
00:13:20,386 --> 00:13:22,670
Well, I once tipped
a dealer five grand.

261
00:13:22,845 --> 00:13:25,970
Do you have any idea
how much money that is?

262
00:13:26,136 --> 00:13:29,303
Hey, I just realized something.
I've never seen you have fun before.

263
00:13:29,476 --> 00:13:32,101
I know.
It's freaking me out.

264
00:13:32,275 --> 00:13:35,233
Place your bets.

265
00:13:35,395 --> 00:13:36,273
I'm betting it all.

266
00:13:36,448 --> 00:13:37,020
Oh, God.

267
00:13:37,198 --> 00:13:38,523
You gotta admire
the plums on this kid.

268
00:13:38,695 --> 00:13:42,322
Hear that?
Admire my plums.

269
00:13:42,657 --> 00:13:44,402
You're betting it all?

270
00:13:44,578 --> 00:13:47,703
I'm gonna get Lina the best trumpet
anyone's ever gotten.

271
00:13:47,875 --> 00:13:49,163
Hit me.

272
00:13:49,328 --> 00:13:50,702
Hit me.

273
00:13:50,875 --> 00:13:52,240
Hit  me.

274
00:13:52,416 --> 00:13:53,412
21!

275
00:13:53,588 --> 00:13:56,583
- 22.
- 22--what?

276
00:13:56,796 --> 00:13:57,502
Sorry, bud.

277
00:13:57,677 --> 00:13:59,212
Before you take away the chips--
Okay, you're taking away the chips.

278
00:13:59,386 --> 00:14:00,541
Why are you taking the chips,
Vivian?

279
00:14:00,715 --> 00:14:02,051
Vivian,
let's talk about this.

280
00:14:02,217 --> 00:14:03,302
It's gonna be okay, all right?

281
00:14:03,467 --> 00:14:06,593
How can it be okay?
I had $700.

282
00:14:06,765 --> 00:14:09,683
I didn't have to keep going.
What am I gonna get Lina now?

283
00:14:09,858 --> 00:14:13,431
She'll understand.
She shouldn't have to understand.

284
00:14:13,606 --> 00:14:16,021
What do I do?
How do I get that money back?

285
00:14:16,198 --> 00:14:16,943
You can't.

286
00:14:17,106 --> 00:14:20,770
Not unless you keep playing.

287
00:14:21,946 --> 00:14:23,730
I'm in!

288
00:14:23,906 --> 00:14:25,193
- We'll go for another 1,000.
- Yes!

289
00:14:25,365 --> 00:14:27,202
No! No! No, no, no, no!

290
00:14:27,368 --> 00:14:30,702
Look, we agreed 100 bucks,
that's all, okay?

291
00:14:30,877 --> 00:14:32,661
I lost my money.
We didn't keep going.

292
00:14:32,838 --> 00:14:34,203
'Cause you're a loser.
I'm a winner.

293
00:14:34,388 --> 00:14:37,123
I've got plums!

294
00:14:37,296 --> 00:14:39,042
You promised Nicole
you wouldn't gamble.

295
00:14:39,216 --> 00:14:39,920
I'm not gambling.

296
00:14:40,087 --> 00:14:43,381
I'm just helping a skinny, pale kid
buy some gal a trombone.

297
00:14:43,558 --> 00:14:44,882
- Trumpet.
- Whatever.

298
00:14:45,058 --> 00:14:49,263
Never one of them gonna
get you laid.

299
00:14:51,648 --> 00:14:54,182
- Just say I'm Superman.
- No!

300
00:14:54,355 --> 00:14:56,810
- My God, this is crazy! Ugh!

301
00:14:56,986 --> 00:14:57,812
You're grownups!

302
00:14:57,988 --> 00:15:01,022
Evan, you a doctor.
Doctors don't do this.

303
00:15:01,198 --> 00:15:06,073
And you... it's much less surprising,
but enough already!

304
00:15:06,248 --> 00:15:07,491
Two words will set you free.

305
00:15:07,657 --> 00:15:10,613
Oh, I got two words for you.

306
00:15:10,788 --> 00:15:13,332
Unless...

307
00:15:16,335 --> 00:15:18,750
Oh, you guys are enjoying this.

308
00:15:18,918 --> 00:15:19,833
What?!

309
00:15:20,005 --> 00:15:21,833
I'm thinking maybe you two--

310
00:15:22,008 --> 00:15:24,003
- Whoa, no, ew!
- No, I don't think so.

311
00:15:24,175 --> 00:15:24,632
Yeah, yeah.

312
00:15:24,808 --> 00:15:28,510
All I know is you both found a way
to spend the whole night wrapped around each other.

313
00:15:28,675 --> 00:15:29,931
She is so not my type.

314
00:15:30,098 --> 00:15:33,143
If anybody is into anybody,
it's obviously him.

315
00:15:33,306 --> 00:15:34,640
Uh, where do you get that?

316
00:15:34,817 --> 00:15:37,390
Uh, hello!

317
00:15:37,936 --> 00:15:39,312
That's padding.

318
00:15:39,487 --> 00:15:40,103
Really?

319
00:15:40,277 --> 00:15:41,312
Come on,
for two hours?

320
00:15:41,487 --> 00:15:43,690
Hey, I thought it might be
your one superpower.

321
00:15:43,865 --> 00:15:45,232
You know what?

322
00:15:45,407 --> 00:15:52,780
I'm gonna go to this party by myself
'cause I think you two kids need a little alone time.

323
00:15:54,998 --> 00:15:58,791
- You're Superman.
- Whatever.

324
00:15:59,917 --> 00:16:00,752
How do I look?

325
00:16:00,916 --> 00:16:03,541
Like a woman who's sorry
she killed a bunny.

326
00:16:03,716 --> 00:16:07,211
Any chance you could cry?

327
00:16:07,465 --> 00:16:09,212
I don't think so.

328
00:16:09,385 --> 00:16:12,431
Maybe we can get you there.
Picture me with another woman.

329
00:16:12,597 --> 00:16:15,102
Oh. We're kissing.
We're groping each other.

330
00:16:15,266 --> 00:16:17,142
Skirts are being
ripped off.

331
00:16:17,318 --> 00:16:20,603
- He said skirts.
- I heard it.

332
00:16:20,775 --> 00:16:22,692
Honey, I don't need to cry.
I can handle this.

333
00:16:22,856 --> 00:16:26,610
All right,
we're ready to go.

334
00:16:27,986 --> 00:16:29,862
Uh...

335
00:16:30,036 --> 00:16:32,150
We're live in 3, 2...

336
00:16:32,326 --> 00:16:36,070
Less than ten hours ago,
I was standing on this very spot

337
00:16:36,246 --> 00:16:41,792
surrounded by these glorious creatures
that I love so much.

338
00:16:41,967 --> 00:16:48,802
But then tragedy struck, taking the life
of a popular little fellow named...QTip.

339
00:16:50,135 --> 00:16:54,511
My heart goes out to bunnies
and bunny lovers everywhere

340
00:16:54,685 --> 00:16:58,891
over the loss
of this amazing rabbit.

341
00:16:59,526 --> 00:17:04,942
I hope you can forgive me,
and that I can forgive myself.

342
00:17:06,567 --> 00:17:12,531
Reporting live amongst my furry friends,
Holly Ellenbogen, News 9.

343
00:17:14,285 --> 00:17:16,070
And we're out.

344
00:17:16,247 --> 00:17:19,371
Please get these things away from me!
Get them away from me!

345
00:17:19,546 --> 00:17:21,913
Wait, sorry.
Now we're out.

346
00:17:22,086 --> 00:17:24,251
Oh!

347
00:17:26,678 --> 00:17:29,293
- We cannot catch a break.
- We need more money.

348
00:17:29,468 --> 00:17:31,132
Man, this is starting to get ugly.

349
00:17:31,305 --> 00:17:32,881
Starting to get ugly?

350
00:17:33,057 --> 00:17:36,303
You lost $15,000.
You have got to stop.

351
00:17:36,476 --> 00:17:38,392
Don't listen to him.
Gambling is fun.

352
00:17:38,558 --> 00:17:40,602
We are having fun!

353
00:17:40,768 --> 00:17:43,482
This is insane, man.
You have a problem.

354
00:17:43,648 --> 00:17:47,063
Look, I appreciate your concern.
But I'm a big boy.

355
00:17:47,237 --> 00:17:51,570
And nobody tells Yonk Allen
what to do.

356
00:17:53,037 --> 00:17:54,192
It's Nicole.

357
00:17:54,365 --> 00:17:57,110
What's she doing calling you?

358
00:17:57,286 --> 00:17:59,452
I'm supposed to make sure
you don't gamble.

359
00:17:59,628 --> 00:18:03,252
You ain't doing
a good job.

360
00:18:04,168 --> 00:18:04,912
Hey.

361
00:18:05,085 --> 00:18:06,672
Hey, how's he doing?

362
00:18:06,837 --> 00:18:11,673
Uh, let me go somewhere
where I can talk, okay?

363
00:18:12,347 --> 00:18:13,760
All right, look.
I'll tell you what.

364
00:18:13,926 --> 00:18:19,392
You just put the money away,
and... she doesn't need to know anything.

365
00:18:19,556 --> 00:18:21,051
What are you doing here, kid?

366
00:18:21,226 --> 00:18:23,061
What are you doing?

367
00:18:23,228 --> 00:18:27,813
You have this incredible woman,
and if I were married to her,

368
00:18:27,988 --> 00:18:33,361
man, I would never,
I would never risk screwing that up.

369
00:18:35,827 --> 00:18:36,862
You're right.

370
00:18:37,037 --> 00:18:39,203
No!

371
00:18:40,745 --> 00:18:42,660
Hey, he's, uh...

372
00:18:42,835 --> 00:18:44,291
He's doing good.
Everything's fine.

373
00:18:44,455 --> 00:18:47,043
Oh, that's great.
Thank you so much.

374
00:18:47,215 --> 00:18:48,752
Yeah, yeah,
it was no big deal.

375
00:18:48,925 --> 00:18:50,213
So where are you guys now?

376
00:18:50,388 --> 00:18:51,793
Uh...strip club.

377
00:18:51,965 --> 00:18:54,420
Oh, that's great.
Keep him there.

378
00:18:54,595 --> 00:18:57,012
Will do.

379
00:18:57,178 --> 00:18:57,973
Thanks, kid.

380
00:18:58,138 --> 00:19:01,221
Yeah.
Let's get out of here.

381
00:19:01,395 --> 00:19:02,763
No.

382
00:19:02,936 --> 00:19:03,852
What?

383
00:19:04,018 --> 00:19:07,063
I'm not stopping.
I want my salary for this week.

384
00:19:07,235 --> 00:19:08,643
Yeah, that's not happening.

385
00:19:08,817 --> 00:19:11,652
I worked for it.
It's mine, and I want it.

386
00:19:11,817 --> 00:19:12,520
No way, man.

387
00:19:12,696 --> 00:19:13,863
It's your rent,
it's your gas.

388
00:19:14,025 --> 00:19:14,782
You can't afford that.

389
00:19:14,948 --> 00:19:17,281
I don't care.
It's for Lina. It's my money.

390
00:19:17,448 --> 00:19:19,451
I just--

391
00:19:19,618 --> 00:19:23,372
Just once
I need to not lose.

392
00:19:30,797 --> 00:19:33,091
Thank  you.

393
00:19:33,257 --> 00:19:35,840
Good luck.

394
00:19:46,647 --> 00:19:48,770
Hit me.

395
00:19:48,937 --> 00:19:50,941
Hit me.

396
00:19:51,106 --> 00:19:53,813
Don't do it.

397
00:19:53,987 --> 00:19:56,401
Hit me.

398
00:19:59,497 --> 00:20:00,992
Oh, my God.

399
00:20:01,158 --> 00:20:04,663
This--
I don't know what to say.

400
00:20:04,836 --> 00:20:09,500
This is the most thoughtful,
perfect present I've ever gotten.

401
00:20:09,665 --> 00:20:11,330
Yay!

402
00:20:11,508 --> 00:20:16,001
Now let's see what
this baby can do, okay?

403
00:20:18,386 --> 00:20:20,843
Shut up!

404
00:20:23,016 --> 00:20:25,761
That's my neighbor in 2A.

405
00:20:25,938 --> 00:20:28,520
Excuse me.

406
00:20:30,145 --> 00:20:35,560
That's for the electric guitar!
How do you like that now?

407
00:20:43,578 --> 00:20:46,990
Thank you.
It's perfect.

