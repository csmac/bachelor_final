1
00:00:27,203 --> 00:00:29,104
It's warm.

2
00:00:29,205 --> 00:00:37,568
Isn't it strange? It's almost winter, but it feels like spring.

3
00:00:39,123 --> 00:00:44,828
Being like this, it seems like winter won't come.

4
00:00:46,386 --> 00:00:50,334
You can't fall asleep here. Get up.

5
00:00:51,755 --> 00:00:55,942
Why? I like it here.

6
00:01:00,956 --> 00:01:05,458
This isn't your place. Get up.

7
00:01:09,762 --> 00:01:13,971
You... got your memory back.

8
00:01:21,781 --> 00:01:23,040
Yes.

9
00:01:24,336 --> 00:01:28,943
I have to go back now.

10
00:02:28,896 --> 00:02:30,898
Anna.

11
00:02:40,060 --> 00:02:43,767
Are you okay? You've come back to your senses?

12
00:02:43,866 --> 00:02:46,521
I�m fine.

13
00:02:47,971 --> 00:02:49,975
Billy.

14
00:02:52,580 --> 00:02:57,085
Do you remember me?

15
00:03:00,541 --> 00:03:02,393
Do you know who you are?

16
00:03:04,447 --> 00:03:08,552
I'm Jo Anna.

17
00:03:09,402 --> 00:03:15,117
I've returned.

18
00:03:15,564 --> 00:03:17,367
You're back?

19
00:03:19,317 --> 00:03:22,573
Your memory's all back?

20
00:03:25,175 --> 00:03:27,031
Yes.

21
00:03:28,672 --> 00:03:33,628
I remember everything.

22
00:04:12,700 --> 00:04:14,004
Oppa.

23
00:04:16,657 --> 00:04:23,470
That woman's gone back to her rightful place.

24
00:04:25,873 --> 00:04:30,577
Oppa you too, go back to your rightful place.

25
00:05:14,972 --> 00:05:18,024
Are you really okay? You should go to the hospital...

26
00:05:18,072 --> 00:05:19,978
You changed rooms.

27
00:05:20,175 --> 00:05:22,930
When you died... no.

28
00:05:22,980 --> 00:05:28,538
When I thought you died, it was hard to stay in that room.

29
00:05:28,837 --> 00:05:32,594
But all of your things are there as before.

30
00:05:33,384 --> 00:05:37,485
They really are as I left them.

31
00:05:37,635 --> 00:05:39,937
Yes, everything's all there.

32
00:05:41,037 --> 00:05:44,142
I was just waiting for you to return.

33
00:05:57,112 --> 00:06:01,867
Remember how I asked you to remember
that I went back for what was precious to me?

34
00:06:01,922 --> 00:06:04,374
Do you remember?

35
00:06:12,108 --> 00:06:15,314
Sir, Jang Chul Soo...

36
00:06:18,216 --> 00:06:20,321
It�s Mr. Gong.

37
00:06:22,625 --> 00:06:26,979
Leave me, my head hurts. I want to be alone.

38
00:06:27,381 --> 00:06:29,484
Okay. Rest, Anna.

39
00:06:54,582 --> 00:06:55,593
What?

40
00:06:55,687 --> 00:06:59,540
Her memory has returned and Jang Chul Soo
has already come and gone?

41
00:06:59,642 --> 00:07:04,046
Then she'll know that you tried to abandon her.

42
00:07:04,548 --> 00:07:06,403
Anna doesn't know.

43
00:07:06,551 --> 00:07:10,055
Jang Chul Soo knows all about not being
able to find the boat or the phone.

44
00:07:10,106 --> 00:07:13,013
If she sees Jang Chul Soo, isn't it all over?

45
00:07:13,109 --> 00:07:16,064
I�ve already finished talking with Jang Chul Soo.

46
00:07:40,256 --> 00:07:44,514
I know I'm someone who should die.

47
00:07:45,368 --> 00:07:48,375
I also know I won�t be able to receive forgiveness.

48
00:07:48,523 --> 00:07:52,175
But.

49
00:07:55,282 --> 00:07:57,635
I really regret things now.

50
00:07:58,787 --> 00:08:00,740
I want to return everything to how it was.

51
00:08:01,042 --> 00:08:05,996
If you want things to return as before, start by being honest.

52
00:08:06,598 --> 00:08:08,852
It's because you don't know the real Anna.

53
00:08:08,900 --> 00:08:13,159
The woman who stayed with you wasn�t the
real Anna. She's such a frightening woman.

54
00:08:18,764 --> 00:08:23,721
If she knew everything, it's all over.

55
00:08:30,333 --> 00:08:37,193
Jang Chul Soo ssi, please cover everything up.

56
00:08:38,245 --> 00:08:43,852
You also bear responsibility for how things happened.

57
00:08:46,412 --> 00:08:51,068
You don't know how much I regret everything.

58
00:08:54,729 --> 00:09:07,547
So much that I'm pathetically asking you to lie like this...

59
00:09:10,248 --> 00:09:13,805
I want to return everything to how it was.

60
00:09:20,609 --> 00:09:23,663
This is where Anna has to return.

61
00:09:25,922 --> 00:09:28,421
For that to happen...

62
00:09:31,374 --> 00:09:33,427
Please help me.

63
00:09:48,802 --> 00:09:50,351
Anna.

64
00:09:52,260 --> 00:09:53,909
Her name was Anna.

65
00:09:56,563 --> 00:09:58,164
It's a strange name.

66
00:10:01,218 --> 00:10:04,675
But it's better than Na Sang Shil.

67
00:10:54,842 --> 00:10:56,194
Anna.

68
00:10:56,248 --> 00:10:57,947
I called Jang Chul Soo.

69
00:10:58,000 --> 00:11:02,664
Before bringing you back, I felt I had
to first tell you everything about the situation.

70
00:11:02,791 --> 00:11:09,450
Then that was all your doing?

71
00:11:10,100 --> 00:11:11,555
Yes.

72
00:11:11,605 --> 00:11:19,014
I wanted to sue Jang Chul Soo, but it wouldn�t look good to others.

73
00:11:19,114 --> 00:11:21,770
So I agreed to cover it up.

74
00:11:22,472 --> 00:11:24,673
I asked him to find my family.

75
00:11:24,733 --> 00:11:28,127
Why? He couldn�t even do that.

76
00:11:28,277 --> 00:11:31,782
Who knows if he even tried properly...

77
00:11:31,933 --> 00:11:37,594
In any case, this misunderstanding
was all because of Jang Chul Soo.

78
00:11:39,343 --> 00:11:41,451
Misunderstanding.

79
00:11:42,347 --> 00:11:45,154
So it was all a misunderstanding?

80
00:11:56,574 --> 00:11:59,471
I can't judge anything correctly right now.

81
00:11:59,623 --> 00:12:06,481
It's like I haven't quite awoken. I'm in a daze.

82
00:12:06,585 --> 00:12:11,293
Yes, you've gotten up from sleeping.
You were momentarily dreaming.

83
00:12:11,389 --> 00:12:16,298
Think of everything that happened as fantasy, not reality.

84
00:12:17,801 --> 00:12:30,470
If that's the case, the dream was so vivid... This reality is stranger.

85
00:12:31,122 --> 00:12:35,975
Things will become familiar quickly, since this is your place.

86
00:12:43,137 --> 00:12:44,389
Clear these away.

87
00:12:44,489 --> 00:12:45,939
Yes.

88
00:13:25,495 --> 00:13:26,495
Kids.

89
00:13:26,596 --> 00:13:28,449
Uncle!

90
00:13:28,506 --> 00:13:31,853
Uncle, you said you'd be gone a few days, but you're back early.

91
00:13:31,903 --> 00:13:33,612
Should we call ahjumma?

92
00:13:33,664 --> 00:13:37,813
No, don't call her. Even if you call, she can't come.

93
00:13:37,861 --> 00:13:39,066
Why?

94
00:13:41,909 --> 00:13:44,966
Ahjumma went home.

95
00:13:45,059 --> 00:13:47,661
If she goes home, she can't come here?

96
00:13:47,764 --> 00:13:53,678
Yes. Her home is so far away, she can't come anymore.

97
00:14:34,731 --> 00:14:36,786
What happened to the yacht?

98
00:14:36,888 --> 00:14:40,088
I sold it.

99
00:14:40,496 --> 00:14:45,446
It's the yacht where you had an accident. Keeping it was painful.

100
00:14:47,299 --> 00:14:52,963
If you'd kept the yacht, things wouldn't have taken so long.

101
00:14:53,008 --> 00:14:55,059
This is all Jang Chul Soo's doing.

102
00:14:55,209 --> 00:15:01,118
If he hadn't done all that to you, I wouldn't have found you so late.

103
00:15:02,021 --> 00:15:06,392
Yes. It's too late.

104
00:15:08,791 --> 00:15:14,458
When you're better, let's go back to America.

105
00:15:14,755 --> 00:15:18,816
Now we've returned to how things were before.

106
00:15:20,218 --> 00:15:22,722
Billy, don't be mistaken.

107
00:15:22,869 --> 00:15:27,327
I've returned to my place. I haven't returned to you.

108
00:15:30,782 --> 00:15:35,290
Whatever misunderstandings there were, you left me.

109
00:15:35,690 --> 00:15:41,757
Anna, I didn't do it on purpose. There was
a real misunderstanding. That...

110
00:15:41,847 --> 00:15:43,352
Stop.

111
00:15:43,451 --> 00:15:50,261
I'm not myself yet.

112
00:15:50,961 --> 00:16:00,726
After I settle things, whether it's defending
yourself or asking for forgiveness, do it then.

113
00:16:28,871 --> 00:16:33,529
Yes. I'm Jo Anna.

114
00:16:35,280 --> 00:16:36,985
I've returned.

115
00:17:43,379 --> 00:17:46,937
This is me.

116
00:18:05,982 --> 00:18:07,478
Pathetic people.

117
00:18:10,333 --> 00:18:12,038
Follow along.

118
00:18:12,102 --> 00:18:13,536
You've done this once before so you know.

119
00:18:13,638 --> 00:18:14,337
Yes!

120
00:18:14,439 --> 00:18:15,693
New ones!

121
00:18:16,392 --> 00:18:17,594
Picture frames!

122
00:18:18,243 --> 00:18:19,647
Fixtures!

123
00:18:20,952 --> 00:18:23,658
What else does she dislike?

124
00:18:43,787 --> 00:18:47,687
You're back already? I'm getting rid of things you dislike.

125
00:18:48,135 --> 00:18:50,489
Continue what you were doing.

126
00:19:05,012 --> 00:19:07,414
This is a big vacuum cleaner.

127
00:19:07,467 --> 00:19:08,618
Yes?

128
00:19:09,422 --> 00:19:12,321
If it's this big, does it pull in dirt better?

129
00:19:12,524 --> 00:19:13,574
Yes.

130
00:19:22,638 --> 00:19:24,992
So there were vacuum cleaners like this.

131
00:19:25,891 --> 00:19:29,146
Cleaning is a difficult task. You're working hard.

132
00:19:34,606 --> 00:19:36,363
Geez, what have you done?

133
00:19:41,665 --> 00:19:44,818
I'm sorry. I'm really sorry.

134
00:19:46,276 --> 00:19:47,724
Are you hurt?

135
00:19:48,223 --> 00:19:50,179
I'm okay.

136
00:19:52,632 --> 00:19:54,285
Then it's fine.

137
00:19:56,135 --> 00:19:58,541
Princess, where are you?

138
00:20:12,466 --> 00:20:13,612
What's the matter?

139
00:20:13,713 --> 00:20:18,070
Sir, it seems she hasn't completely returned.

140
00:20:28,999 --> 00:20:32,402
Why are you here? What's that?

141
00:20:32,902 --> 00:20:36,663
It's the wine you liked. Do you remember?

142
00:20:39,863 --> 00:20:45,977
Last Christmas, I made a mistake and you were really mad.

143
00:20:46,271 --> 00:20:48,277
You remember, right?

144
00:20:52,831 --> 00:21:03,045
That time, you were upset that I threw away the chair you liked,
so I went out at Christmas to find the same one.

145
00:21:03,798 --> 00:21:07,755
You remember how in the end, I found the same one, right?

146
00:21:07,851 --> 00:21:12,316
Stop asking if I remember things. I remember everything.

147
00:21:12,759 --> 00:21:16,465
Should I tell you one more thing I remember?

148
00:21:17,819 --> 00:21:21,875
I also remember you saying you wanted a divorce.

149
00:21:23,426 --> 00:21:31,737
I was really crazed then, since Solomon died right in front of me.

150
00:21:31,788 --> 00:21:33,441
I didn't do anything right either.

151
00:21:33,538 --> 00:21:40,749
If you'd killed Princess in front of me,
I couldn't have forgiven you either.

152
00:21:43,556 --> 00:21:52,473
If I'd said I was sorry then, none of this would have happened.

153
00:21:57,975 --> 00:22:00,779
Should we drink this now?

154
00:22:01,328 --> 00:22:03,983
Just because I said that doesn't mean I'm forgiving you.

155
00:22:08,097 --> 00:22:14,250
Okay. Still, drink this.

156
00:22:27,731 --> 00:22:29,289
Wine?

157
00:22:31,800 --> 00:22:38,105
Seeing this, I have a craving for rice wine.

158
00:22:38,202 --> 00:22:42,509
No. I'm not Na Sang Shil.

159
00:22:42,659 --> 00:22:45,622
Jo Anna didn't like things like rice wine.

160
00:22:47,410 --> 00:22:52,818
She didn't like anything.

161
00:22:56,885 --> 00:22:57,837
Jang Joon Suk.

162
00:22:57,885 --> 00:22:59,988
I hear you got the gold medal in sketching again.

163
00:23:00,088 --> 00:23:01,642
I was first place.

164
00:23:01,747 --> 00:23:04,151
Today I got 100 points.

165
00:23:04,247 --> 00:23:06,948
I know how to count to 50.

166
00:23:07,005 --> 00:23:08,702
You're all very impressive.

167
00:23:08,759 --> 00:23:12,506
Ahjumma said that when we have a right to be cocky,
we should do it to the fullest.

168
00:23:12,557 --> 00:23:17,014
She said that children earn their keep by making adults happy.

169
00:23:17,114 --> 00:23:22,920
Uncle said he's buying us something good to eat,
so we should go home early. Let's go.

170
00:23:22,983 --> 00:23:24,675
Okay, let's go.

171
00:23:43,862 --> 00:23:45,811
Go, Bang Gu.

172
00:23:47,516 --> 00:23:50,221
Take back the heart you gave me.

173
00:23:51,923 --> 00:23:53,676
Noonim.

174
00:23:53,773 --> 00:23:58,932
Were you intentionally using me so you could chase Chul Soo off?

175
00:24:00,535 --> 00:24:04,489
Is that why you gave me this book with the lead character Chul Soo?

176
00:24:08,845 --> 00:24:15,655
Your name might be Bang Gu (hole),
but you've been really precise and careful.

177
00:24:16,207 --> 00:24:17,809
It's Young Gu.

178
00:24:18,009 --> 00:24:20,364
And it wasn't like that, noonim.

179
00:24:20,462 --> 00:24:26,575
It doesn�t matter. We're enemies now.

180
00:24:27,873 --> 00:24:29,378
I will wait!

181
00:24:29,378 --> 00:24:34,728
Noonim, until you return, I'll stay here and wait for you!

182
00:24:36,467 --> 00:24:39,671
Noonim!!

183
00:25:01,947 --> 00:25:06,404
What? Why is this?

184
00:25:06,558 --> 00:25:11,613
I was pushing myself too much because of Jang Chul Soo.

185
00:25:16,669 --> 00:25:17,823
It's cold.

186
00:25:20,023 --> 00:25:22,579
Should I go in there and wait?

187
00:25:30,041 --> 00:25:32,943
I won't compromise my sincerity.

188
00:25:35,447 --> 00:25:36,851
Gotta pee.

189
00:25:48,019 --> 00:25:50,461
It's so cold.

190
00:25:50,962 --> 00:25:53,969
What if Bang Gu's really waiting?

191
00:26:29,974 --> 00:26:31,671
Bang Gu!

192
00:26:33,644 --> 00:26:36,803
Noonim, just a moment, just a minute!

193
00:26:36,847 --> 00:26:39,752
Don't come, just a minute!!

194
00:26:47,616 --> 00:26:51,117
Noonim! You can't just leave!

195
00:26:51,275 --> 00:26:53,274
Noonim!

196
00:26:55,523 --> 00:26:57,880
Why?! Why, why, why?!

197
00:26:57,979 --> 00:27:00,733
Of all the things in the world!

198
00:27:03,036 --> 00:27:05,540
I should've held it!

199
00:27:11,349 --> 00:27:16,708
Why is it so cold today?

200
00:28:10,251 --> 00:28:12,201
Wait a second!

201
00:28:12,357 --> 00:28:14,754
What happened to the trash earlier?

202
00:28:14,823 --> 00:28:19,964
I'm sorry, I threw away the broken glass. You said to clear it away.

203
00:28:20,065 --> 00:28:21,816
I'm not looking for that!

204
00:28:41,590 --> 00:28:43,292
This is it.

205
00:28:49,555 --> 00:28:51,211
Then, we'll go.

206
00:28:52,356 --> 00:28:56,863
I really...thank you.

207
00:29:13,703 --> 00:29:23,566
<i>This is something no one could have known.</i>

208
00:29:23,667 --> 00:29:32,230
<i>Something even fantasy couldn't have created.</i>

209
00:29:32,331 --> 00:29:39,642
<i>This cold and stopped heart.</i>

210
00:29:39,941 --> 00:29:42,701
If you keep disappearing, I can't contact you and it's bothersome.

211
00:29:42,801 --> 00:29:44,049
So take it wherever you go.

212
00:29:44,255 --> 00:29:47,963
<i>In confusion, in chaos, keeps searching for you.</i>

213
00:29:48,016 --> 00:29:58,279
<i>Though I say it's not, and try to stop you,
though I try to stop.</i>

214
00:29:58,377 --> 00:30:08,242
<i>Every night I only see your face in my dreams.</i>

215
00:30:08,298 --> 00:30:15,554
<i>I need you, I want you, I love you.</i>

216
00:30:15,650 --> 00:30:19,159
<i>I want to say these words.</i>

217
00:30:19,260 --> 00:30:27,669
<i>But I don't know why I hesitate today.</i>

218
00:30:27,770 --> 00:30:32,127
<i>Do you know I have to say this</i>

219
00:30:32,173 --> 00:30:38,241
<i>I've opened my heart like this.</i>

220
00:30:38,285 --> 00:30:44,600
<i>Turn to me now and come closer.</i>

221
00:30:51,804 --> 00:31:00,164
<i>I just keep getting angry for no reason.</i>

222
00:31:00,320 --> 00:31:04,926
<i>Where'd my reasonable self go?</i>

223
00:31:04,893 --> 00:31:09,699
The crazy queen is back. A storm will start soon.

224
00:31:10,143 --> 00:31:11,996
It is really that bad?

225
00:31:12,101 --> 00:31:13,601
Of course.

226
00:31:13,757 --> 00:31:15,902
I heard she's going back to US soon.

227
00:31:16,009 --> 00:31:18,258
During this time, don't ever appear in front of her.

228
00:31:24,682 --> 00:31:26,679
Hurry move and hide.

229
00:31:40,559 --> 00:31:44,916
Congrats on getting your position back.

230
00:31:46,868 --> 00:31:48,470
You don't look bad.

231
00:31:49,320 --> 00:31:53,678
You always used to advise me not to leave in the past.

232
00:31:53,777 --> 00:31:58,893
So you have to forget everything now, just like what you said.

233
00:32:01,142 --> 00:32:02,392
That's true.

234
00:32:02,893 --> 00:32:05,346
Now that you've returned to the side of such a rich husband,

235
00:32:05,695 --> 00:32:08,250
it's natural you'd forget those memories.

236
00:32:09,051 --> 00:32:14,857
Looking at you now, I can see
what your endless confidence was about.

237
00:32:14,959 --> 00:32:16,311
You understand it now.

238
00:32:17,416 --> 00:32:19,218
I heard you'll be leaving soon.

239
00:32:19,521 --> 00:32:21,317
I wish you happiness.

240
00:32:24,071 --> 00:32:25,725
Miss Oh Yu Kyeong

241
00:32:29,596 --> 00:32:34,256
It seems you think my endless confidence was about money,

242
00:32:34,752 --> 00:32:37,102
but you've got it wrong.

243
00:32:39,213 --> 00:32:41,317
That money...

244
00:32:41,414 --> 00:32:43,915
... isn't my husband's

245
00:32:44,765 --> 00:32:46,774
It's mine.

246
00:32:51,326 --> 00:32:54,878
It is so childish to show off my money to flower bundle.

247
00:32:55,734 --> 00:32:59,690
Might as well. There's nothing else to say.

248
00:33:07,448 --> 00:33:08,700
Where did you go to?

249
00:33:09,256 --> 00:33:11,759
I was bored, so I went for a walk.

250
00:33:11,808 --> 00:33:13,763
The employees very surprised to see you, right?

251
00:33:13,864 --> 00:33:17,429
They weren't. They were avoiding me.

252
00:33:18,130 --> 00:33:19,530
That's true.

253
00:33:19,578 --> 00:33:22,027
People always avoid me.

254
00:33:22,081 --> 00:33:23,338
That isn't true.

255
00:33:23,431 --> 00:33:27,391
If they knew you were still alive, so many
people will be happy and welcoming you warmly.

256
00:33:31,053 --> 00:33:34,410
They even had a party to celebrate your return.

257
00:33:35,162 --> 00:33:36,513
Really?

258
00:33:38,164 --> 00:33:40,868
The person who is waiting for my return...

259
00:33:41,421 --> 00:33:42,723
Who is it?

260
00:33:45,081 --> 00:33:49,337
That's true. You don't have many friends here.

261
00:33:49,687 --> 00:33:52,285
We shall have a party once we get back to US.

262
00:33:53,093 --> 00:33:55,038
I don't have any friends there either.

263
00:33:55,340 --> 00:33:58,095
I know clearly what kind of person I am.

264
00:33:58,549 --> 00:34:01,998
Billy, don't bother trying.

265
00:34:27,987 --> 00:34:34,344
The person waiting here isn't Jo Anna, but Na Sang Shil.

266
00:34:36,401 --> 00:34:38,707
There isn't Na Sang Shil.

267
00:34:39,854 --> 00:34:41,858
I am Jo Anna.

268
00:34:44,819 --> 00:34:46,463
When will unni return?

269
00:34:48,574 --> 00:34:51,722
You can't be late for a date.

270
00:34:53,328 --> 00:34:55,381
Unni, when are you coming back?

271
00:34:55,635 --> 00:34:57,830
Even if Ahjussi cries, you have to come back.

272
00:34:57,935 --> 00:35:00,234
Chul Soo oppa will cry.

273
00:35:00,487 --> 00:35:02,387
I will cry too.

274
00:35:13,552 --> 00:35:15,255
Hello!

275
00:35:15,309 --> 00:35:17,159
It's nice to meet you!

276
00:35:17,960 --> 00:35:19,970
I am waiting for unni.

277
00:35:20,063 --> 00:35:22,521
Please walk on your own.

278
00:35:35,185 --> 00:35:36,839
Hyung, but...

279
00:35:37,588 --> 00:35:40,392
Because they don't want you to find them,
they didn't do anything to you?

280
00:35:40,697 --> 00:35:42,796
There is enough evidence.

281
00:35:43,196 --> 00:35:44,496
Don't put it that way.

282
00:35:46,199 --> 00:35:48,810
So it is really ending then.

283
00:35:49,003 --> 00:35:53,710
Hyung, you will end it too, right?

284
00:35:56,570 --> 00:35:57,467
Hyung.

285
00:35:57,518 --> 00:35:58,518
Just forget about it.

286
00:35:58,625 --> 00:36:01,925
Just pretend that there wasn't anything from the beginning.

287
00:36:02,323 --> 00:36:04,978
Hyung, you go lose your memory as well.

288
00:36:05,886 --> 00:36:09,185
You go back first.

289
00:36:09,241 --> 00:36:11,143
I shall finish it up.

290
00:36:11,286 --> 00:36:12,990
Hyung, how about a drink?

291
00:36:13,139 --> 00:36:15,543
My mom went over to your place to take care of the kids.

292
00:36:15,844 --> 00:36:16,996
Really?

293
00:36:17,056 --> 00:36:19,799
Oh, Sang Shil unni isn't...

294
00:36:22,807 --> 00:36:23,904
I'm going.

295
00:36:31,716 --> 00:36:34,369
I'd rather he go crazy and drink some soju.

296
00:36:34,419 --> 00:36:36,630
Why is he pretending that nothing has happened?

297
00:36:49,592 --> 00:36:51,995
What is it, Duk Gu?

298
00:36:52,153 --> 00:36:55,117
<i>Hyung. I want to have a drink with you.</i>

299
00:36:55,266 --> 00:36:57,113
<i>Don't pretend that it isn't difficult.</i>

300
00:36:57,515 --> 00:36:58,615
<i>I'm hanging up.</i>

301
00:37:00,257 --> 00:37:02,072
<i>Even if it is difficult, bear with it.</i>

302
00:37:02,527 --> 00:37:04,422
That is love.

303
00:37:05,674 --> 00:37:07,476
Jang Chul Soo.

304
00:37:08,530 --> 00:37:10,433
I love you.

305
00:37:53,704 --> 00:38:00,115
Even if I can't bear to, I still have to answer her.

306
00:38:25,393 --> 00:38:27,200
It hurts so much now.

307
00:38:28,045 --> 00:38:30,250
It isn't me who feel hurts.

308
00:38:31,257 --> 00:38:34,218
I still have yet woken up from that "Na Sang Shil" fantasy.

309
00:38:34,768 --> 00:38:36,364
I just need to wake up.

310
00:38:38,378 --> 00:38:39,830
I...

311
00:38:41,785 --> 00:38:43,137
am Jo Anna.

312
00:38:52,602 --> 00:38:54,658
Why are you out here? It is cold.

313
00:39:01,611 --> 00:39:04,022
It's good that it's cold.

314
00:39:05,216 --> 00:39:07,522
I don't want to feel warm.

315
00:39:12,838 --> 00:39:15,191
Do you feel fine now?

316
00:39:16,097 --> 00:39:19,898
Billy, we'll go back tomorrow.

317
00:39:20,299 --> 00:39:22,554
I have nothing left over here.

318
00:39:25,456 --> 00:39:26,907
That was right of you.

319
00:39:27,015 --> 00:39:29,213
Once you get back, you'll forget it all.

320
00:39:38,616 --> 00:39:39,565
Hello.

321
00:39:39,917 --> 00:39:41,777
The order I've placed has reached already right?

322
00:39:41,826 --> 00:39:44,576
Oh, yes. Hang on.

323
00:39:54,521 --> 00:39:56,024
Have you eaten?

324
00:39:56,075 --> 00:39:57,675
If not, eat with us.

325
00:40:01,403 --> 00:40:04,254
It's alright, you go ahead.

326
00:40:09,863 --> 00:40:10,771
Anna.

327
00:40:10,870 --> 00:40:13,023
We didn't get a chance to eat together last time.

328
00:40:13,124 --> 00:40:16,230
So I specially prepared this for you.

329
00:40:28,693 --> 00:40:29,843
What is this?

330
00:40:30,401 --> 00:40:33,205
I specially invited a chef from Seoul.

331
00:40:33,398 --> 00:40:35,701
To give you the best tasting dishes.

332
00:40:39,117 --> 00:40:40,527
What?

333
00:40:40,620 --> 00:40:44,382
I was supposed to eat jjajangmyun with you.

334
00:40:44,982 --> 00:40:48,537
But I knocked it over, so we didn't get a chance to eat it.

335
00:40:50,284 --> 00:40:54,440
That's right. It was like that.

336
00:40:55,446 --> 00:40:57,000
Chairman, it is all ready.

337
00:41:08,015 --> 00:41:09,317
Hurry up eat.

338
00:41:09,415 --> 00:41:11,417
It was specially prepared to suit your taste.

339
00:41:11,515 --> 00:41:12,767
It will definitely taste good.

340
00:42:00,464 --> 00:42:03,776
The taste isn't there.

341
00:42:04,428 --> 00:42:07,426
Anna... What is wrong?

342
00:42:21,695 --> 00:42:23,657
Anna...

343
00:42:24,904 --> 00:42:26,711
Should I give you some time of your own?

344
00:43:27,090 --> 00:43:29,042
<i>So we'll have jjajangmyun the next time.</i>

345
00:43:29,042 --> 00:43:30,754
<i>And the next, we'll have dry jjajang.</i>

346
00:43:30,849 --> 00:43:34,455
If we're sick of it, we can eat SiChuan jjajangmyun.

347
00:43:35,109 --> 00:43:39,008
If it gets bored, jjajang rice.

348
00:43:39,214 --> 00:43:40,267
That would do.

349
00:43:40,361 --> 00:43:45,666
So if I were to end up with you, I have to
eat jjajangmyun for the rest of my life?

350
00:43:45,773 --> 00:43:46,921
You don't want to?

351
00:43:47,028 --> 00:43:53,031
Well, I don't know if you like it, but I like jjajangmyun.

352
00:45:05,605 --> 00:45:07,764
You found it after you threw it out?

353
00:45:17,923 --> 00:45:20,526
I couldn't throw it out.

354
00:45:25,333 --> 00:45:27,284
Over there...

355
00:45:27,841 --> 00:45:30,843
Seeing you smiling happily.

356
00:45:31,997 --> 00:45:33,995
I'd often feel insecure.

357
00:45:37,311 --> 00:45:40,217
I was afraid you would change drastically.

358
00:45:44,920 --> 00:45:46,573
But Anna...

359
00:45:46,678 --> 00:45:48,884
That isn't you.

360
00:45:49,677 --> 00:45:53,935
You couldn't remember being "Jo Anna" and lived
as "Na Sang Shil", how could that be real?

361
00:45:53,987 --> 00:45:56,395
It's just a fantasy which started from a lie.

362
00:45:56,638 --> 00:45:58,697
It isn't.

363
00:45:59,692 --> 00:46:01,851
If it is a fantasy...

364
00:46:02,049 --> 00:46:05,851
It wouldn't hurt that much.

365
00:46:13,221 --> 00:46:14,816
Billy.

366
00:46:16,669 --> 00:46:20,129
I'm in a lot of pain.

367
00:46:21,774 --> 00:46:23,978
My heart hurts so much.

368
00:46:25,732 --> 00:46:28,085
That I can't breathe.

369
00:46:31,539 --> 00:46:34,693
So we have to leave this place.

370
00:46:34,993 --> 00:46:37,905
Leave faraway,

371
00:46:38,052 --> 00:46:41,603
And avoid it all.

372
00:46:42,361 --> 00:46:44,259
We'll leave tomorrow.

373
00:46:45,308 --> 00:46:47,412
We'll be back to normal.

374
00:46:48,214 --> 00:46:49,465
I...

375
00:46:50,965 --> 00:46:53,976
I am unable to leave with you.

376
00:46:59,885 --> 00:47:02,483
The pain I'm feeling now...

377
00:47:04,946 --> 00:47:07,691
Makes my heart numb.

378
00:47:09,450 --> 00:47:10,996
You...

379
00:47:11,949 --> 00:47:18,624
Jo Anna has never said how she felt before, isn't that so?

380
00:47:20,373 --> 00:47:22,825
I'm sorry, Billy.

381
00:47:24,926 --> 00:47:26,530
Don't cry.

382
00:47:27,230 --> 00:47:29,139
You are Anna.

383
00:47:30,189 --> 00:47:34,243
Anna has never cried for someone else before.

384
00:47:37,708 --> 00:47:40,260
You are not crying now.

385
00:47:57,434 --> 00:48:00,090
The sincerity that you can give me...

386
00:48:03,598 --> 00:48:05,804
Is it only "I'm sorry"?

387
00:48:08,501 --> 00:48:10,360
I'm sorry...

388
00:48:13,565 --> 00:48:16,315
I really am sorry.

389
00:48:32,387 --> 00:48:35,091
Then...

390
00:48:36,656 --> 00:48:44,762
Will you return to where you left your heart?

391
00:48:46,317 --> 00:48:50,277
I am unable to go back to you.

392
00:48:51,676 --> 00:48:53,927
I will not go back there either.

393
00:48:54,907 --> 00:48:58,958
Na Sang Shil is unable to go back with me.

394
00:49:01,010 --> 00:49:04,720
Jo Anna is unable to go back there. Is it that way?

395
00:49:10,074 --> 00:49:12,477
Then you...

396
00:49:15,381 --> 00:49:18,236
Who are you now?

397
00:49:20,240 --> 00:49:22,443
I am both of them.

398
00:49:25,301 --> 00:49:27,251
But it feels weird.

399
00:49:29,555 --> 00:49:34,264
I don't have any confidence in either side.

400
00:50:08,107 --> 00:50:10,712
Good night, Jang Chul Soo.

401
00:50:36,250 --> 00:50:38,702
Were you waiting there?

402
00:50:41,410 --> 00:50:42,960
<i>I will find you.</i>

403
00:50:47,266 --> 00:50:49,668
No matter where you are.

404
00:50:49,720 --> 00:50:51,672
No matter how far you are.

405
00:50:52,674 --> 00:50:56,882
I will find you.

406
00:51:13,983 --> 00:51:16,182
What am I expecting?

407
00:51:18,396 --> 00:51:20,496
It is impossible to find me.

408
00:52:07,415 --> 00:52:09,168
What am I expecting?

409
00:52:10,777 --> 00:52:14,182
It is impossible to meet her.

410
00:52:37,619 --> 00:52:39,121
Na Sang Shil.

411
00:52:40,374 --> 00:52:42,428
I really am able to find you.

412
00:52:42,477 --> 00:52:44,380
What should I do?

413
00:52:46,899 --> 00:52:49,235
Do you want me to die?

414
00:53:53,536 --> 00:53:55,531
Jang Chul Soo.

415
00:53:56,884 --> 00:53:58,693
Were you here to find me?

416
00:54:02,945 --> 00:54:04,006
I...

417
00:54:04,810 --> 00:54:10,064
You knew I was waiting here, so you came to find me?

418
00:54:10,119 --> 00:54:11,767
I...

419
00:54:11,974 --> 00:54:14,827
I wasn't here to find you.

420
00:54:16,473 --> 00:54:18,379
This time as well...

421
00:54:19,179 --> 00:54:23,985
You just happened to pass by, and we happened to meet?

422
00:54:24,085 --> 00:54:24,891
Yea.

423
00:54:25,946 --> 00:54:26,714
That's it.

424
00:54:28,492 --> 00:54:29,176
Then...

425
00:54:30,316 --> 00:54:36,451
Isn't it better to pretend not to see me and continue walking?

426
00:54:38,259 --> 00:54:40,058
I should do that.

427
00:54:40,763 --> 00:54:42,561
But I am unable to.

428
00:54:44,513 --> 00:54:47,174
Up to now, to me...

429
00:54:47,170 --> 00:54:50,582
You are still Sang Shil.

430
00:54:51,780 --> 00:54:53,631
My name is not Sang Shil.

431
00:54:54,920 --> 00:54:56,578
You don't know, but my name is...

432
00:54:56,637 --> 00:54:58,444
Don't say it.

433
00:55:02,641 --> 00:55:07,201
Just that, it'd be better if I didn't know.

434
00:55:08,752 --> 00:55:10,056
You too...

435
00:55:10,862 --> 00:55:15,217
The weird name which I gave you, "Na Sang Shil"

436
00:55:15,665 --> 00:55:17,213
Forget about it too.

437
00:55:19,768 --> 00:55:24,024
It is better to forget unhealthy things.

438
00:55:24,125 --> 00:55:30,386
You'll feel better if you pretend that it never happened before.

439
00:55:30,585 --> 00:55:34,796
Can you do it?

440
00:55:34,896 --> 00:55:38,145
Oh, I can.

441
00:55:38,543 --> 00:55:41,098
So...

442
00:55:41,699 --> 00:55:44,605
Return to where you belong.

443
00:55:44,710 --> 00:55:47,957
And live happily.

444
00:55:51,816 --> 00:55:53,231
Alright.

445
00:55:54,284 --> 00:55:57,883
Since you put it that way...

446
00:56:02,540 --> 00:56:05,348
I will be well.

447
00:56:14,362 --> 00:56:18,469
Goodbye, Jang Chul Soo.

448
00:56:35,989 --> 00:56:37,744
Just go on.

449
00:56:46,803 --> 00:56:48,706
You jerk.

450
00:56:49,109 --> 00:56:50,816
Beggar!

451
00:56:55,517 --> 00:56:56,918
I...

452
00:56:57,919 --> 00:57:00,932
If I hold your hands now...

453
00:57:02,076 --> 00:57:04,137
I won't be able to let it go.

454
00:57:04,239 --> 00:57:06,592
So...

455
00:57:07,402 --> 00:57:09,447
Just leave now.

456
00:57:25,823 --> 00:57:29,124
I will forget the unhealthy me.

457
00:57:29,398 --> 00:57:31,552
But I won't forget this.

458
00:57:31,659 --> 00:57:33,359
So...

459
00:57:34,858 --> 00:57:36,659
Just for awhile.

460
00:57:37,969 --> 00:57:40,566
Just stay here for awhile.

461
00:58:16,084 --> 00:58:17,117
It is fine now.

462
00:58:20,319 --> 00:58:22,670
Now I can be able to forget it.

463
00:58:27,640 --> 00:58:30,994
Goodbye, Jang Chul Soo.

464
00:58:32,741 --> 00:58:34,798
And my name is...

465
00:58:36,496 --> 00:58:38,608
Jo Anna.

466
00:58:42,405 --> 00:58:44,213
This is a FREE fansub. Not for sale!
Get it for free @ d-addicts.com

467
00:58:44,316 --> 00:58:45,161
Main Translators: javabeans, iluxxx

468
00:58:45,259 --> 00:58:46,217
Spot Translator: javabeans

469
00:58:46,312 --> 00:58:47,168
Timer: debbii
Timing QC: theedqueen

470
00:58:47,266 --> 00:58:48,220
Editor & Final QC: theedqueen

471
00:58:48,314 --> 00:58:49,116
Coordinators: mily2, ay_link

472
00:58:49,222 --> 00:58:50,667
Brought to you by: WITH S2
Written In The Heavens Subbing Squad

