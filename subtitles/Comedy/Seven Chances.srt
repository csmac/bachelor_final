1
00:00:08,970 --> 00:00:12,670
SEVEN CHANCES

2
00:00:59,655 --> 00:01:03,755
One beautiful summer day,
when fragrant flowers were in bloom,

3
00:01:03,788 --> 00:01:07,453
Jimmie Shannon met Mary Jones,

4
00:01:07,454 --> 00:01:10,853
and he wanted to tell her
he loved her.

5
00:01:38,043 --> 00:01:42,943
When Autumn had cast its golden glow
and the flowers had faded and Died,

6
00:01:42,978 --> 00:01:46,943
He still wanted to tell her
he loved her.

7
00:02:06,636 --> 00:02:10,936
When winter came and the leaves
had fallen one by one ...

8
00:02:10,969 --> 00:02:14,034
... And the snow had covered the hills and valleys...

9
00:02:14,069 --> 00:02:16,934
... He still wanted to tell her
he loved her.

10
00:02:37,928 --> 00:02:41,693
However... when nature had changed again,

11
00:02:42,026 --> 00:02:45,726
Bringing forth spring-time with its
beautiful buds and blossoms...

12
00:02:46,925 --> 00:02:50,725
... He still wanted to tell her
he loved her.

13
00:03:12,417 --> 00:03:17,017
Jimmie was the junior partner of the
firm of "Meekin & Shannon",

14
00:03:17,050 --> 00:03:20,715
Brokers, that had been tricked into a
financial deal that meant disgrace...

15
00:03:20,716 --> 00:03:24,516
... And possibly prison...

16
00:03:24,548 --> 00:03:27,713
Unless they raised money quickly.

17
00:03:38,310 --> 00:03:43,310
A lawyer, unknown to the firm,
with good news for Jimmie.

18
00:03:46,807 --> 00:03:50,207
7-- And to my grandson, James Shannon,

19
00:03:50,208 --> 00:03:53,205
I hereby bequeath the balance of
my estate amounting to...

20
00:04:06,301 --> 00:04:10,501
"This man has some kind of
a legal paper with him !"

21
00:04:13,299 --> 00:04:15,799
"Maybe it's a summons !"

22
00:04:23,896 --> 00:04:26,096
"He's not in."

23
00:05:07,664 --> 00:05:10,884
"Let's go out to the
Country Club."

24
00:06:56,252 --> 00:07:01,652
And to my grandson, James Shannon, I hereby
bequeath the balance of my estate amounting to...

25
00:07:52,036 --> 00:07:56,036
"Your grandfather has left
you the sum of..."

26
00:08:15,828 --> 00:08:17,828
"Seven million dollars."

27
00:08:29,725 --> 00:08:31,125
"Providing..."

28
00:08:37,423 --> 00:08:44,023
"...you are married by seven o'clock on the
evening of your twenty seventh birthday."

29
00:08:48,120 --> 00:08:51,220
"When is your twenty-seventh birthday ?"

30
00:08:56,218 --> 00:08:58,018
"Today !"

31
00:09:06,215 --> 00:09:09,280
"There's plenty of time..."

32
00:09:09,315 --> 00:09:12,915
"You've got until seven o'clock
tonight to get married."

33
00:09:16,313 --> 00:09:19,213
"Maybe Mary might marry me."

34
00:10:23,593 --> 00:10:27,193
"Mary, you're the only
girl I have ever loved."

35
00:10:33,289 --> 00:10:35,289
"Will you marry me ?"

36
00:11:08,479 --> 00:11:11,379
"I must hurry back to the country club and..."

37
00:11:11,380 --> 00:11:14,377
"...tell Billy it's all fixed..."

38
00:11:14,378 --> 00:11:16,576
"...and we'll be married today."

39
00:11:19,376 --> 00:11:20,976
"Why today ?"

40
00:11:22,775 --> 00:11:25,540
"My grandfather left me a lot of money..."

41
00:11:25,574 --> 00:11:28,734
"...providing I marry some girl today."

42
00:11:37,570 --> 00:11:43,670
"I don't mean some girl... I can marry any girl !"

43
00:11:43,768 --> 00:11:49,868
"I... I mean, it don't matter who I marry, but
I must wed someone !"

44
00:12:33,554 --> 00:12:37,554
"He said he must wed someone, and
it might as well be me !"

45
00:13:18,141 --> 00:13:24,341
"...and so you see, dear, how much better
it is to give him a chance to explain."

46
00:13:39,036 --> 00:13:43,136
"Then, Mr. Shannon, you've got
to marry some other girl."

47
00:13:47,833 --> 00:13:50,733
"I couldn't marry any other girl."

48
00:14:00,730 --> 00:14:03,030
"But, you'll lose this fortune."

49
00:14:05,828 --> 00:14:08,528
"I don't want the money..."

50
00:14:08,528 --> 00:14:12,527
"... it has already cost me
the only girl I ever loved."

51
00:14:30,121 --> 00:14:33,121
<i>Dear Jimmie</i>

52
00:14:37,118 --> 00:14:39,118
<i>Jimmie Shannon</i>

53
00:14:41,118 --> 00:14:49,118
<i>Don't you dare marry anybody else.</i>

54
00:14:50,715 --> 00:14:53,515
<i>Mary.</i>

55
00:14:55,315 --> 00:15:02,715
<i>P.S.
I think I will be home all day.</i>

56
00:15:20,407 --> 00:15:25,107
"Take this to Mr. Shannon,
and ride for your life !"

57
00:15:45,198 --> 00:15:49,498
"... and we'll lose our reputation
our friends, Even our liberty..."

58
00:15:49,532 --> 00:15:52,662
"... unless you do marry !
... We're partners;"

59
00:15:52,697 --> 00:15:56,297
"... if you won't do it to save yourself,"

60
00:15:56,331 --> 00:16:01,096
"You must to save me !"

61
00:16:06,093 --> 00:16:07,993
"Who will I marry ?"

62
00:16:34,825 --> 00:16:37,885
"How many of these girls
do you know ?"

63
00:17:05,177 --> 00:17:07,477
"You have seven chances."

64
00:17:25,970 --> 00:17:27,870
"Would you marry me ?"

65
00:20:15,721 --> 00:20:18,221
<i>"Will you marry me ?"</i>

66
00:20:49,911 --> 00:20:52,911
"I'll propose to this one for you."

67
00:21:13,004 --> 00:21:15,804
"Have you ever thought of
getting married ?"

68
00:21:18,702 --> 00:21:20,202
"Constantly !"

69
00:21:23,501 --> 00:21:28,601
"Would you marry a man who
loves you better than his very life ?"

70
00:21:43,395 --> 00:21:47,195
"Wait a minute...
I'm proposing for him."

71
00:22:49,576 --> 00:22:51,576
"Who bats next ?"

72
00:23:07,071 --> 00:23:08,771
"Wrong party."

73
00:23:21,666 --> 00:23:25,731
"Meet me at the Broad Street church
at five o'clock..."

74
00:23:25,766 --> 00:23:30,466
"I'll have a bride there if it's
the last act of my life."

75
00:23:37,863 --> 00:23:42,763
"You try and have one there
too in case he fails."

76
00:23:45,659 --> 00:23:48,959
"In case two show up,
I'll marry the other one."

77
00:24:29,578 --> 00:24:33,078
On Duty
Miss Smith

78
00:25:13,734 --> 00:25:15,234
"Yours ?"

79
00:25:17,334 --> 00:25:20,434
"Are you going to
try to raise it ?"

80
00:25:57,622 --> 00:26:01,522
"Do you think anybody
would marry me ?"

81
00:28:08,285 --> 00:28:09,685
THE DAILY NEWS

82
00:28:09,686 --> 00:28:11,486
LEADING AFTERNOON PAPER LARGEST CIRCULATION

83
00:29:49,255 --> 00:29:51,755
STAGE - ENTRANCE

84
00:30:12,669 --> 00:30:18,669
Julian Eltinge (1881�1941)
Mr. William Dalton, one of the greatest drag performers
in the history of the American Theatre.

85
00:30:54,835 --> 00:30:57,135
WANTED, A BRIDE

86
00:30:57,235 --> 00:31:00,535
<i>James Shannon, Prominent Young
Broker Falls Heir...</i>

87
00:31:00,569 --> 00:31:03,599
<i>to $7,000,000 if He Is Married Today.
ALL HE NEEDS IS A BRIDE</i>

88
00:31:03,634 --> 00:31:06,634
<i>Girl Who Appears at Broad Street
Church In...</i>

89
00:31:06,634 --> 00:31:10,034
<i>...Bridal Costume By 5 O�Clock
Will Be the Lucky Winner.</i>

90
00:31:16,730 --> 00:31:21,130
By the time Jimmie had
reached the church...

91
00:31:21,163 --> 00:31:26,128
He had proposed to everything
in skirts, including a Scotchman.

92
00:32:09,814 --> 00:32:12,314
NIAGARA FALLS

93
00:32:14,413 --> 00:32:16,913
Destination: RENO

94
00:36:42,836 --> 00:36:47,101
"Ladies, you are evidently the
victims of some practical joker..."

95
00:36:47,134 --> 00:36:53,134
"I must ask you to leave the church
as quietly as possible."

96
00:38:07,306 --> 00:38:10,496
<i>Jimmie Shannon, Don't you dare marry anybody else.
Mary.
P.S.  I think I will be home all day.</i>

97
00:38:34,103 --> 00:38:35,703
Barbershop

98
00:38:36,903 --> 00:38:39,403
"What time is it ?"

99
00:39:04,073 --> 00:39:06,673
Clock Repairing

100
00:41:56,145 --> 00:41:58,810
"Bring the minister to Mary's house."

101
00:41:58,843 --> 00:42:01,843
"I'll try to get there somehow
before seven o'clock !"

102
00:43:46,313 --> 00:43:48,713
TURKISH BATHS

103
00:43:53,411 --> 00:43:55,911
LADIES DAY

104
00:46:36,965 --> 00:46:39,265
"You've killed him !"

105
00:49:26,114 --> 00:49:30,114
"I know a short cut...
we'll head him off !"

106
00:54:03,235 --> 00:54:04,835
"I'm too late !"

107
00:54:15,831 --> 00:54:18,831
"And we're not going to be married ?"

108
00:54:22,929 --> 00:54:28,029
"But Jimmie... Don't you think we could
be happy without the money ?"

109
00:54:33,527 --> 00:54:37,592
"Mary, there's nothing before me
but failure and disgrace,

110
00:54:37,625 --> 00:54:42,825
...and you mean too much to me
to let you share it !"

111
00:56:00,830 --> 00:56:04,930
The End
Typed by : Reza Fa.

