1
00:00:03,233 --> 00:00:04,480
What's this?

2
00:00:04,481 --> 00:00:06,076
I know this one.

3
00:00:06,077 --> 00:00:08,300
What is the world's cheapest puppet, Alex?

4
00:00:09,815 --> 00:00:12,849
Henry, save your sarcasm for your blog.

5
00:00:12,850 --> 00:00:14,824
At least there, no one will read it.

6
00:00:17,127 --> 00:00:18,602
It's my sock, dad. What's the problem?

7
00:00:18,603 --> 00:00:20,393
The problem is I found
it on your bathroom floor.

8
00:00:20,394 --> 00:00:21,738
Now, if you're gonna live with me,

9
00:00:21,739 --> 00:00:23,474
all I ask is that you
don't leave your bathroom

10
00:00:23,475 --> 00:00:26,996
looking like the
aftermath of a Roman orgy.

11
00:00:27,863 --> 00:00:30,060
Dad, I do a pretty good job
of cleaning up around here.

12
00:00:30,061 --> 00:00:31,912
I'm online right now looking for a job.

13
00:00:31,913 --> 00:00:32,779
Do you want me to look for a maid?

14
00:00:32,780 --> 00:00:33,858
A maid? I got a maid!

15
00:00:33,859 --> 00:00:35,511
Her name is Henry.

16
00:00:36,419 --> 00:00:38,894
Her wages are free room and board.

17
00:00:38,895 --> 00:00:40,219
And the only reason I keep her employed

18
00:00:40,220 --> 00:00:42,769
is I feel sorry for her father.

19
00:00:43,459 --> 00:00:44,718
All right, I get it.

20
00:00:44,719 --> 00:00:46,625
I will be a better cleaning lady.

21
00:00:49,313 --> 00:00:50,963
Hi, mom.

22
00:00:51,587 --> 00:00:54,289
Oh, wow, I'm sorry to hear that, mom.

23
00:00:54,290 --> 00:00:55,971
It'll be okay.

24
00:00:57,586 --> 00:01:00,283
I can't really talk right now.

25
00:01:00,284 --> 00:01:02,403
I'm not gonna do his
voice. He's right here, mom.

26
00:01:04,385 --> 00:01:06,100
I got to go.

27
00:01:06,101 --> 00:01:08,224
Good-bye, mom.

28
00:01:10,565 --> 00:01:12,909
- What's wrong with your mother?
- Nothing.

29
00:01:12,910 --> 00:01:14,726
She's been on a losing
streak with men lately.

30
00:01:14,727 --> 00:01:16,517
- For how long?
- Well, let's see,

31
00:01:16,518 --> 00:01:18,721
you guys split up 22 years ago, so...

32
00:01:18,722 --> 00:01:20,212
27 years.

33
00:01:21,002 --> 00:01:23,241
You're pretty sassy for a maid.

34
00:01:24,102 --> 00:01:25,529
Let's go to dinner.

35
00:01:26,031 --> 00:01:27,814
- It's 4:00.
- Right.

36
00:01:27,815 --> 00:01:30,992
It's time for the early-bird
special at the Hotcake Corral.

37
00:01:31,757 --> 00:01:33,079
What's your problem?

38
00:01:33,080 --> 00:01:35,502
I don't know... I just feel like
old people eat dinner at 4:00.

39
00:01:35,503 --> 00:01:37,836
Old people eat dinner at 5:00.

40
00:01:38,454 --> 00:01:41,160
Frugal people eat dinner at 4:00...

41
00:01:41,161 --> 00:01:43,310
to save money and to avoid the old people.

42
00:01:43,311 --> 00:01:44,695
Let's go.

43
00:01:55,183 --> 00:01:57,287
He becomes a cognac ad
whenever he puts on a suit.

44
00:01:57,288 --> 00:01:59,174
I'll never understand the connection.

45
00:02:00,753 --> 00:02:02,032
What are you doing here?

46
00:02:02,033 --> 00:02:02,839
And why do you look like

47
00:02:02,840 --> 00:02:05,121
you should be hanging
out the top of a limo?

48
00:02:06,413 --> 00:02:08,062
My girlfriend Janette
from short-term rentals

49
00:02:08,063 --> 00:02:08,925
is getting married today,

50
00:02:08,926 --> 00:02:10,638
and she made all of her bridesmaids

51
00:02:10,639 --> 00:02:11,802
pay for these hideous dresses

52
00:02:11,803 --> 00:02:13,635
and these super-expensive makeovers.

53
00:02:13,636 --> 00:02:15,029
The A.C. in the car is broken,

54
00:02:15,030 --> 00:02:16,498
so we have to stop every ten blocks

55
00:02:16,499 --> 00:02:18,664
and freeze her face to preserve it.

56
00:02:19,127 --> 00:02:20,591
I'm so mad I had to pay for this.

57
00:02:20,592 --> 00:02:22,524
Guess how much this
cost. Just take a guess.

58
00:02:22,525 --> 00:02:23,446
I have no idea.

59
00:02:23,447 --> 00:02:24,353
Just for fun... just take a guess.

60
00:02:24,354 --> 00:02:26,827
- You're not gonna believe it.
- $1,000.

61
00:02:26,828 --> 00:02:28,263
What?

62
00:02:29,398 --> 00:02:31,881
$1,000? Why would... that
is so like you, Henry,

63
00:02:31,882 --> 00:02:33,246
to purposely guess too high!

64
00:02:33,247 --> 00:02:34,414
You said it was crazy expensive.

65
00:02:34,415 --> 00:02:35,601
All right, what do you think, Ed?

66
00:02:35,602 --> 00:02:37,353
- $1.
- What?

67
00:02:40,802 --> 00:02:42,242
Why would you say $1?

68
00:02:42,243 --> 00:02:44,115
I didn't want to go over.

69
00:02:44,935 --> 00:02:49,261
No, no, no. $400 for one stupid day.

70
00:02:49,262 --> 00:02:51,048
It cost as much as Vince's suit.

71
00:02:53,620 --> 00:02:55,146
We get it. We get it.

72
00:02:55,865 --> 00:02:57,883
I'd like to stay and
chitchat about your makeup,

73
00:02:57,884 --> 00:02:59,202
but I got to take the maid to dinner.

74
00:02:59,203 --> 00:03:00,633
Let's go.

75
00:03:28,218 --> 00:03:29,637
You haven't taken me to the hotcake corral

76
00:03:29,638 --> 00:03:31,337
since I was a kid.

77
00:03:31,338 --> 00:03:32,808
It's really changed.

78
00:03:33,906 --> 00:03:34,737
How so?

79
00:03:34,738 --> 00:03:37,178
- I'm not talking to you.
- I'm not talking to you!

80
00:03:39,343 --> 00:03:41,823
- You mean the menu?
- The menu?

81
00:03:41,824 --> 00:03:43,957
It's the okay-to-be-gay corral.

82
00:03:47,469 --> 00:03:48,755
Ugh, it's mom.

83
00:03:48,756 --> 00:03:50,549
- I'll talk to her later.
- No, talk to her now.

84
00:03:50,550 --> 00:03:51,817
I don't want to deal with it now.

85
00:03:51,818 --> 00:03:53,033
She's going through another bad breakup.

86
00:03:53,034 --> 00:03:54,130
It'll be an hour-long rant

87
00:03:54,131 --> 00:03:55,906
about how she's being screwed over by men.

88
00:03:55,907 --> 00:03:57,115
You know, it's not your responsibility

89
00:03:57,116 --> 00:03:58,634
to take on your mother's problems.

90
00:03:58,635 --> 00:04:01,090
I'm not taking them on.
I'm just listening to them.

91
00:04:01,091 --> 00:04:03,307
Anyway, I'm sure she
can take care of herself.

92
00:04:03,308 --> 00:04:05,051
That'd be convenient, wouldn't it?

93
00:04:05,052 --> 00:04:06,429
Seeing as how you're the
cause of most of them.

94
00:04:06,430 --> 00:04:08,033
- I beg your pardon?
- What?

95
00:04:08,034 --> 00:04:10,247
Nothing.
- Really?

96
00:04:10,248 --> 00:04:11,265
'Cause I thought I heard you say,

97
00:04:11,266 --> 00:04:13,565
"seeing as you're the
cause of most of them. "

98
00:04:15,456 --> 00:04:17,444
Oh, sorry about this mess...

99
00:04:17,445 --> 00:04:19,812
wait. I remember you. I remember you.

100
00:04:19,813 --> 00:04:22,625
You're that nice homosexual from the DMV.

101
00:04:23,633 --> 00:04:25,194
Well, not anymore.

102
00:04:25,195 --> 00:04:26,987
I thought it was genetic.

103
00:04:31,711 --> 00:04:33,274
Henry, this is that nice man

104
00:04:33,275 --> 00:04:35,234
that I was telling you
about who fudged on my test

105
00:04:35,235 --> 00:04:37,910
so I could pass, and I
didn't lose my license.

106
00:04:37,911 --> 00:04:39,853
- That was very nice of you.
- Wasn't it?

107
00:04:39,854 --> 00:04:41,276
I got fired for it.

108
00:04:41,277 --> 00:04:43,263
- You're kidding.
- No, I'm not.

109
00:04:43,264 --> 00:04:44,985
That's terrible.

110
00:04:44,986 --> 00:04:46,189
Anyway, I'll have the short stack,

111
00:04:46,190 --> 00:04:48,890
the coffee, and the
side of Canadian bacon.

112
00:04:48,891 --> 00:04:50,306
My kid will have the same.

113
00:04:50,725 --> 00:04:52,536
Anything else you'd like to say to me?

114
00:04:52,537 --> 00:04:55,193
Yeah, I'm hungry, so
sooner rather than later.

115
00:04:56,250 --> 00:04:58,967
Did I mention I sacrificed my job

116
00:04:58,968 --> 00:05:02,369
so you could drive around
town spreading evil?

117
00:05:04,837 --> 00:05:07,935
Did I mention I was hungry,
so sooner rather than later?

118
00:05:09,167 --> 00:05:11,117
Dad, you're not gonna apologize?

119
00:05:11,118 --> 00:05:12,395
For what?

120
00:05:12,396 --> 00:05:13,911
I had a government job

121
00:05:13,912 --> 00:05:16,412
that's almost impossible
to get fired from,

122
00:05:16,413 --> 00:05:18,516
with full benefits and a pension,

123
00:05:18,517 --> 00:05:21,757
and now I'm packing syrup in a holster.

124
00:05:22,522 --> 00:05:23,741
I didn't know.

125
00:05:23,742 --> 00:05:26,473
So now would you like
to say something to me?

126
00:05:26,474 --> 00:05:29,627
Yeah, what kind of syrup are you packing?

127
00:05:31,841 --> 00:05:33,430
Boysenberry and maple.

128
00:05:33,431 --> 00:05:35,102
Aren't you forgetting something?

129
00:05:45,515 --> 00:05:47,207
They have to do that every time you ask.

130
00:05:47,208 --> 00:05:49,309
It makes me laugh every time.

131
00:05:50,443 --> 00:05:53,120
I'll have the boysenberry,
sooner rather than later.

132
00:05:55,594 --> 00:05:57,038
You're unbelievable.

133
00:05:57,039 --> 00:05:58,473
He just wanted you to apologize.

134
00:05:58,474 --> 00:06:01,697
Well, I want the aphids to
stay away from my tomato plants.

135
00:06:01,698 --> 00:06:04,006
I want a 12-inch cucumber.

136
00:06:04,007 --> 00:06:05,884
Holla!

137
00:06:10,519 --> 00:06:11,646
You know, they do that here

138
00:06:11,647 --> 00:06:12,945
every time I talk about gardening?

139
00:06:12,946 --> 00:06:14,490
It's very odd.

140
00:06:16,137 --> 00:06:17,491
The point is, I want a lot of things,

141
00:06:17,492 --> 00:06:18,719
but it doesn't mean I'm gonna get them.

142
00:06:18,720 --> 00:06:19,817
You should apologize.

143
00:06:19,818 --> 00:06:21,004
If not because he helped you out,

144
00:06:21,005 --> 00:06:22,193
because he's handling your food.

145
00:06:22,194 --> 00:06:24,260
He wouldn't do anything
to my food, would he?

146
00:06:24,261 --> 00:06:26,123
Here you go... apple pie.

147
00:06:26,124 --> 00:06:28,662
- I didn't order apple pie.
- It's on the house...

148
00:06:28,663 --> 00:06:31,009
special, just for you.

149
00:06:37,991 --> 00:06:40,902
There's no way I'm
putting that in my mouth.

150
00:06:40,903 --> 00:06:43,067
Holla!

151
00:06:50,019 --> 00:06:51,593
Unbelievable.

152
00:06:51,594 --> 00:06:52,618
Can't go to my favorite restaurant

153
00:06:52,619 --> 00:06:54,937
because some waiter is
gonna defile my food,

154
00:06:54,938 --> 00:06:56,649
for God knows what reason.

155
00:06:57,175 --> 00:06:59,003
Ed knows the reason!

156
00:06:59,004 --> 00:07:01,396
You got that guy fired, and you
won't go and say you're sorry.

157
00:07:01,397 --> 00:07:03,510
Ed doesn't know the reason!

158
00:07:05,316 --> 00:07:07,918
And nobody likes him
at that pancake house.

159
00:07:07,919 --> 00:07:09,322
Did you notice how they
all clinked their glasses

160
00:07:09,323 --> 00:07:11,108
when I said we should stiff him?

161
00:07:12,143 --> 00:07:13,556
Will you just go over
there and say you're sorry

162
00:07:13,557 --> 00:07:14,256
and get it over with?

163
00:07:14,257 --> 00:07:15,851
I've got nothing to apologize for!

164
00:07:15,852 --> 00:07:19,202
Dad, it is your fault. Just cop to it.

165
00:07:19,203 --> 00:07:21,593
Let me tell you a story that
happened to me in medical school.

166
00:07:21,594 --> 00:07:23,053
Maybe it'll make it clearer for you.

167
00:07:23,054 --> 00:07:25,216
A farmhand is working in a tobacco field

168
00:07:25,217 --> 00:07:27,049
and gets run over by a combine...

169
00:07:27,050 --> 00:07:28,737
louses up the whole machinery.

170
00:07:29,900 --> 00:07:32,297
So now, who's to blame...
the farmer, the farmhand,

171
00:07:32,298 --> 00:07:34,270
the guy who sold me the beer?

172
00:07:34,961 --> 00:07:38,603
Everyone knew the risks involved.

173
00:07:39,616 --> 00:07:41,472
Wow, that went a completely different way

174
00:07:41,473 --> 00:07:43,269
than I thought it was gonna go.

175
00:07:43,697 --> 00:07:44,398
Well, the point is,

176
00:07:44,399 --> 00:07:47,100
I may not be able to return to Kentucky...

177
00:07:50,269 --> 00:07:53,322
But you can bet your ass I
am going to figure out a way

178
00:07:53,323 --> 00:07:56,371
to eat my short stack
and turkey bacon in peace,

179
00:07:56,372 --> 00:07:58,496
sooner rather than later.

180
00:08:14,898 --> 00:08:15,826
Hi.

181
00:08:15,827 --> 00:08:17,126
Welcome to hotcake corral.

182
00:08:17,127 --> 00:08:19,019
What can I rustle up for you, partner?

183
00:08:19,020 --> 00:08:21,327
I'm new here, so I'd
like to try the coffee,

184
00:08:21,328 --> 00:08:23,653
the short stack, and the turkey bacon,

185
00:08:23,654 --> 00:08:25,691
sooner rather than later.

186
00:08:27,584 --> 00:08:29,837
Are you joking? I know it's you.

187
00:08:29,838 --> 00:08:31,298
It's not me.

188
00:08:32,093 --> 00:08:33,478
This is not Metropolis.

189
00:08:33,479 --> 00:08:36,829
Putting on a pair of
glasses is not a disguise.

190
00:08:38,117 --> 00:08:39,204
I don't know what you want from me.

191
00:08:39,205 --> 00:08:40,553
I just want you to apologize.

192
00:08:40,554 --> 00:08:42,115
Well, that's not gonna happen.

193
00:08:42,537 --> 00:08:45,707
I saw the look in your eyes
when you were telling me

194
00:08:45,708 --> 00:08:48,821
that you didn't have a
relationship with your son.

195
00:08:48,822 --> 00:08:50,396
It was sweet and vulnerable.

196
00:08:50,397 --> 00:08:53,441
It's the reason why I
cheated on the test for you.

197
00:08:53,442 --> 00:08:55,726
I know that, deep down inside,

198
00:08:55,727 --> 00:08:58,807
you are a closet nice person.

199
00:09:01,176 --> 00:09:03,678
So why don't you just
come out of the closet?

200
00:09:03,679 --> 00:09:05,640
Enough!

201
00:09:11,665 --> 00:09:13,698
If you do not apologize,

202
00:09:13,699 --> 00:09:15,857
you are no longer welcome
in this restaurant.

203
00:09:15,858 --> 00:09:16,985
Oh, really? You can't do that.

204
00:09:16,986 --> 00:09:18,511
- Just watch me.
- Oh, yeah.

205
00:09:18,512 --> 00:09:20,495
I know the law around here.

206
00:09:20,496 --> 00:09:22,286
What kind of syrup are you packing?

207
00:09:22,690 --> 00:09:23,847
- Mm.
- Oh.

208
00:09:23,848 --> 00:09:25,146
- I... I won't.
- Oh, yes, you do.

209
00:09:25,147 --> 00:09:26,341
- No, no, no, no.
- Oh, yes, you have to.

210
00:09:26,342 --> 00:09:27,603
- Not for you.
- I repeat...

211
00:09:27,604 --> 00:09:30,220
what kind of syrup are you packing?

212
00:09:30,895 --> 00:09:33,041
No! No syrup for you.

213
00:09:33,042 --> 00:09:34,746
Now, get the hell out of my restaurant.

214
00:09:34,747 --> 00:09:37,175
Whoa, whoa. Hold on, cowboy.

215
00:09:37,176 --> 00:09:38,655
Is there... is there a problem here?

216
00:09:38,656 --> 00:09:40,934
Yes, Lawrence, this
man won't take my order.

217
00:09:40,935 --> 00:09:42,682
I'm sorry. Do you know me?

218
00:09:44,057 --> 00:09:45,835
Whoa, Ed.

219
00:09:48,932 --> 00:09:50,792
I did not recognize you.

220
00:09:50,793 --> 00:09:52,239
What's... what's with the disguise?

221
00:09:52,240 --> 00:09:54,543
I'm sorry to be a bother, but
this man won't take my order.

222
00:09:54,544 --> 00:09:56,966
- He won't serve me.
- Why not?

223
00:09:56,967 --> 00:09:59,784
Tim, this is Ed Goodson. He's
one of our best customers.

224
00:09:59,785 --> 00:10:02,498
He likes his food
sooner rather than later.

225
00:10:03,767 --> 00:10:06,795
I'm sorry. I can't serve him.
It's a matter of principle.

226
00:10:06,796 --> 00:10:08,250
Uh, you will serve him.

227
00:10:08,251 --> 00:10:09,981
In fact, you will comp his meal.

228
00:10:09,982 --> 00:10:11,230
Sorry about this, Ed. No problem.

229
00:10:11,231 --> 00:10:12,627
No.

230
00:10:12,628 --> 00:10:14,944
This man does not deserve free food.

231
00:10:14,945 --> 00:10:16,361
I forbid it.

232
00:10:16,362 --> 00:10:18,104
- You forbid it?
- Yes.

233
00:10:18,105 --> 00:10:19,871
I think you're forgetting something, Tim.

234
00:10:19,872 --> 00:10:22,049
She's the sheriff.

235
00:10:26,241 --> 00:10:28,418
And now you've left me with no choice.

236
00:10:28,419 --> 00:10:31,930
Tim, I'm going to have to
ask you surrender your badge.

237
00:10:31,931 --> 00:10:34,753
- No, he didn't.
- Whoo, yes, he did.

238
00:10:36,284 --> 00:10:38,258
And your guns.

239
00:10:42,216 --> 00:10:45,415
And I don't want to see you
around these parts again.

240
00:10:52,031 --> 00:10:54,859
- You got him fired?
- I didn't do anything.

241
00:10:55,388 --> 00:10:58,583
He got himself fired.
He denied me sustenance.

242
00:10:59,771 --> 00:11:02,583
That's a violation of
the Geneva convention.

243
00:11:03,112 --> 00:11:04,648
Dad, I don't think the Geneva convention

244
00:11:04,649 --> 00:11:06,354
applies to pancake houses.

245
00:11:06,355 --> 00:11:08,765
It does to international pancake houses.

246
00:11:11,085 --> 00:11:13,344
You're unbelievable.

247
00:11:13,345 --> 00:11:16,203
Do you even care that you
just ruined this guy's life?

248
00:11:16,204 --> 00:11:18,319
Why do you care so much?

249
00:11:18,320 --> 00:11:19,216
Let's just say I have a soft spot

250
00:11:19,217 --> 00:11:20,364
for people you've screwed over.

251
00:11:20,365 --> 00:11:22,095
Excuse me?

252
00:11:22,796 --> 00:11:25,802
You wreck people's lives,
and then you walk away.

253
00:11:25,803 --> 00:11:27,072
What people?

254
00:11:27,073 --> 00:11:28,629
Who are you talking about?

255
00:11:29,115 --> 00:11:30,771
This is who I'm talking about.

256
00:11:30,772 --> 00:11:32,423
Blockbuster?

257
00:11:36,000 --> 00:11:37,491
I thought it was mom.

258
00:11:37,492 --> 00:11:38,814
What does your mother got to do with this?

259
00:11:38,815 --> 00:11:40,928
You think she keeps calling
me to tell me how happy she is?

260
00:11:40,929 --> 00:11:42,489
She's never gotten over
what you did to her.

261
00:11:42,490 --> 00:11:44,681
Henry, there are a lot of reasons

262
00:11:44,682 --> 00:11:46,920
- why a marriage doesn't work.
- Oh, come on.

263
00:11:46,921 --> 00:11:48,839
Sometimes the pig walks to slaughter

264
00:11:48,840 --> 00:11:51,269
because he knows it's
better for the farmer.

265
00:11:54,402 --> 00:11:56,491
Yeah, you're gonna have
to explain that one to me.

266
00:11:58,933 --> 00:12:01,420
Sometimes...

267
00:12:01,421 --> 00:12:04,936
The pig walks to slaughter...

268
00:12:04,937 --> 00:12:08,074
Because he knows it's
better for the farmer.

269
00:12:11,006 --> 00:12:12,179
Okay, I don't know if you're the pig.

270
00:12:12,180 --> 00:12:12,911
I don't know if you're the farmer,

271
00:12:12,912 --> 00:12:14,261
but I know one thing.

272
00:12:14,262 --> 00:12:16,109
Your marriage didn't work for one reason.

273
00:12:16,110 --> 00:12:18,331
You cheated on my mother.

274
00:12:19,263 --> 00:12:20,285
You're looking to say sorry for something,

275
00:12:20,286 --> 00:12:21,909
how about that, dad?

276
00:12:22,827 --> 00:12:26,184
And you owe blockbuster
$7 for transformers 2.

277
00:12:29,568 --> 00:12:31,442
That I'm sorry for.

278
00:12:38,194 --> 00:12:40,358
Oh, my God. That open house was so hot.

279
00:12:40,359 --> 00:12:42,799
How are we supposed to sell the
condo with no air-conditioning?

280
00:12:42,800 --> 00:12:44,943
It felt like my face was melting off.

281
00:12:46,432 --> 00:12:48,257
I think it has, honey.

282
00:12:48,651 --> 00:12:49,773
Why don't you just wash it off?

283
00:12:49,774 --> 00:12:51,102
I can't. It was too expensive.

284
00:12:51,103 --> 00:12:53,161
Sweetheart, you've already
got two days out of it, okay?

285
00:12:53,162 --> 00:12:55,585
Why don't you just... call it a face?

286
00:12:55,586 --> 00:12:57,124
I need it to last one more day.

287
00:12:57,125 --> 00:12:58,572
I got a call from K.P.I.L.

288
00:12:58,573 --> 00:13:00,494
They're doing a segment
on local real estate.

289
00:13:00,495 --> 00:13:01,813
Oh, my God, they want to interview us?

290
00:13:01,814 --> 00:13:03,647
Yes, they want to interview me.

291
00:13:03,648 --> 00:13:05,978
- Both of us?
- Yes, just me!

292
00:13:07,734 --> 00:13:09,340
I figure if I can make
it last one more night,

293
00:13:09,341 --> 00:13:11,536
then it'll practically
have paid for itself.

294
00:13:11,537 --> 00:13:12,815
I don't know, sweetheart.
I mean, don't you think

295
00:13:12,816 --> 00:13:14,359
it's starting to look a little...

296
00:13:14,901 --> 00:13:16,180
Batman villain-ish?

297
00:13:16,181 --> 00:13:18,347
I can touch it up. It was $400, Vince.

298
00:13:18,348 --> 00:13:21,533
Do you know what $400 is?
That's a month of olive garden.

299
00:13:23,096 --> 00:13:23,907
Okay.

300
00:13:23,908 --> 00:13:26,363
- So I have your support?
- Yeah, of course.

301
00:13:26,364 --> 00:13:27,783
Are you thinking about olive garden

302
00:13:27,784 --> 00:13:29,052
now that I've said "olive garden"?

303
00:13:29,053 --> 00:13:31,798
Wha... no! No.

304
00:13:31,799 --> 00:13:33,524
I'm thinking of you.

305
00:13:33,789 --> 00:13:34,968
Honey, I'm starting to tear up.

306
00:13:34,969 --> 00:13:37,256
Get me a napkin before my makeup gets bad.

307
00:13:40,545 --> 00:13:42,022
Hey, man. I didn't
even know you were here.

308
00:13:42,023 --> 00:13:43,354
What's going on?

309
00:13:44,201 --> 00:13:46,127
Dad and I got into a huge fight.

310
00:13:46,667 --> 00:13:48,773
Did you eat all the good cheese?

311
00:13:49,556 --> 00:13:51,476
No.

312
00:13:51,477 --> 00:13:53,377
Mm, would you be willing to say

313
00:13:53,378 --> 00:13:56,124
that you ate half of the good cheese?

314
00:13:57,277 --> 00:13:58,842
No.

315
00:14:00,422 --> 00:14:01,807
What happened?

316
00:14:01,808 --> 00:14:03,725
He just got this guy fired from the diner.

317
00:14:03,726 --> 00:14:04,998
Oh, wow.

318
00:14:04,999 --> 00:14:07,102
Food must've come later than sooner.

319
00:14:11,800 --> 00:14:12,840
What's it to you?

320
00:14:12,841 --> 00:14:14,528
It just brought up all these feelings

321
00:14:14,529 --> 00:14:16,243
about what he did to my mom.

322
00:14:17,076 --> 00:14:18,215
What are you talking about?

323
00:14:18,216 --> 00:14:19,745
He cheated on her.

324
00:14:20,726 --> 00:14:21,842
Is that what your mom told you?

325
00:14:21,843 --> 00:14:23,584
No, she didn't have to, everybody knows.

326
00:14:23,585 --> 00:14:24,656
That's why they got divorced.

327
00:14:24,657 --> 00:14:26,805
That's why she has 20 ex-boyfriends.

328
00:14:27,542 --> 00:14:29,235
21.

329
00:14:32,068 --> 00:14:34,116
Henry, I have to tell you something.

330
00:14:34,929 --> 00:14:37,242
But we're gonna need some cheese for this.

331
00:14:45,970 --> 00:14:47,881
Oh, my God. You?

332
00:14:47,882 --> 00:14:48,782
What do you want?

333
00:14:48,783 --> 00:14:50,282
Well, I'm sorry to show up unannounced.

334
00:14:50,283 --> 00:14:52,630
I hope I'm not interrupting

335
00:14:52,631 --> 00:14:55,328
some colorful game or sing-along.

336
00:14:57,435 --> 00:15:00,257
That's not what we do...

337
00:15:00,258 --> 00:15:01,998
all the time.

338
00:15:04,341 --> 00:15:05,728
How did you find me?

339
00:15:05,897 --> 00:15:07,809
Sheriff Lawrence gave me your address.

340
00:15:07,810 --> 00:15:09,688
I hate her.

341
00:15:10,957 --> 00:15:12,439
So what do you want?

342
00:15:12,440 --> 00:15:14,985
How much to call my son and tell him

343
00:15:14,986 --> 00:15:17,874
that I've apologized to you?

344
00:15:17,875 --> 00:15:19,225
How much?

345
00:15:19,226 --> 00:15:21,037
Why don't you just apologize?

346
00:15:21,038 --> 00:15:22,837
I'm not sorry.

347
00:15:24,235 --> 00:15:27,114
Okay, $1 million.

348
00:15:27,617 --> 00:15:30,677
Or you can say I'm sorry,
and I'll do it for free.

349
00:15:30,678 --> 00:15:33,143
- Will you take a check?
- Oh, come on.

350
00:15:34,217 --> 00:15:36,927
You drove all the way over here for this?

351
00:15:36,928 --> 00:15:38,753
Why is it so important to you?

352
00:15:38,754 --> 00:15:40,588
Because I can't say I'm sorry

353
00:15:40,589 --> 00:15:44,344
for the one thing my son
wants me to say I'm sorry for.

354
00:15:44,345 --> 00:15:46,165
Holla!

355
00:15:46,813 --> 00:15:49,196
That's not how it goes.

356
00:15:49,197 --> 00:15:51,303
What does he want you
to say you're sorry for?

357
00:15:51,304 --> 00:15:53,131
That's none of your business.

358
00:15:53,560 --> 00:15:56,070
Look, I... I'm... I'm sorry I came.

359
00:15:56,071 --> 00:15:59,230
Forgive me. I'll just... wait. Come back.

360
00:15:59,231 --> 00:16:02,004
I don't know why I always
get sucked in by you.

361
00:16:02,005 --> 00:16:03,779
Holla?

362
00:16:05,485 --> 00:16:07,340
Closer.

363
00:16:07,989 --> 00:16:09,276
Tell you what...

364
00:16:09,311 --> 00:16:10,809
I'll make the call if you tell me

365
00:16:10,810 --> 00:16:13,916
what you can't say you're sorry for.

366
00:16:15,846 --> 00:16:17,650
Cheating on his mother.

367
00:16:17,651 --> 00:16:20,081
You cheated on his mother?

368
00:16:20,082 --> 00:16:22,477
Dad didn't cheat on your mom.

369
00:16:22,478 --> 00:16:24,357
Your mom cheated on dad

370
00:16:24,358 --> 00:16:26,358
with the gravel guy.

371
00:16:26,359 --> 00:16:28,152
What? That makes no sense.

372
00:16:28,153 --> 00:16:30,571
Sure it does. We used to
have a circular driveway.

373
00:16:31,600 --> 00:16:33,265
I'll tell you, it was the
envy of the cul-de-sac.

374
00:16:33,266 --> 00:16:36,348
So much gravel and...
not about the gravel...

375
00:16:36,349 --> 00:16:37,580
About dad.

376
00:16:37,581 --> 00:16:39,458
Dad cheated on mom with Mrs. Fasinello.

377
00:16:39,459 --> 00:16:40,738
Mom found out about it, and she split.

378
00:16:40,739 --> 00:16:42,252
No, Henry, you were
too little to remember,

379
00:16:42,253 --> 00:16:43,250
but I used to hear all the fights.

380
00:16:43,251 --> 00:16:44,276
As a matter of fact, I remember the day

381
00:16:44,277 --> 00:16:45,529
that your mom jumped in the Chevy

382
00:16:45,530 --> 00:16:46,972
and peeled out of here.

383
00:16:46,973 --> 00:16:49,689
There was gravel flying everywhere.

384
00:16:51,767 --> 00:16:54,361
Why would dad take the heat
for something he didn't do?

385
00:16:54,362 --> 00:16:56,936
Because she was raising Henry,

386
00:16:56,937 --> 00:16:59,373
and if he found out the truth,

387
00:16:59,374 --> 00:17:00,760
he wouldn't respect her.

388
00:17:00,761 --> 00:17:03,397
And that was something
I found unacceptable.

389
00:17:03,398 --> 00:17:04,949
So you kept that lie alive

390
00:17:04,950 --> 00:17:07,804
just to protect his mother's integrity?

391
00:17:07,805 --> 00:17:09,488
That's quite a sacrifice.

392
00:17:09,489 --> 00:17:11,643
Well, she was a single mother
bringing up a five-year-old kid,

393
00:17:11,644 --> 00:17:15,342
so who was making the bigger sacrifice?

394
00:17:15,343 --> 00:17:16,710
Wow.

395
00:17:16,711 --> 00:17:19,592
How did you know it was the gravel guy?

396
00:17:19,593 --> 00:17:21,732
Well, he was there for six months.

397
00:17:24,069 --> 00:17:27,039
The driveway was eight inches thick.

398
00:17:28,392 --> 00:17:30,081
Holla!

399
00:17:32,248 --> 00:17:34,826
Mm-mm, maybe he was embarrassed?

400
00:17:35,414 --> 00:17:36,843
No, that's not it.

401
00:17:38,411 --> 00:17:40,136
He was protecting my mom.

402
00:17:42,338 --> 00:17:44,139
He was protecting me.

403
00:17:45,142 --> 00:17:47,149
Why would he do that?

404
00:17:49,165 --> 00:17:52,742
Because sometimes the
pig walks to slaughter

405
00:17:52,743 --> 00:17:55,032
'cause he knows it's
better for the farmer.

406
00:18:01,478 --> 00:18:03,329
I totally know what you mean.

407
00:18:06,591 --> 00:18:08,857
So you'll call and tell him I apologized?

408
00:18:08,858 --> 00:18:11,903
- Fine.
- You're a good man.

409
00:18:11,904 --> 00:18:15,190
Great. I'm a good man without a job.

410
00:18:15,191 --> 00:18:18,929
Okay, glee has been on
pause for 27 minutes.

411
00:18:22,928 --> 00:18:24,982
No one in there's gonna wait any longer.

412
00:18:24,983 --> 00:18:27,333
Oh, hi, Ed!
- Hi, sheriff.

413
00:18:27,334 --> 00:18:30,146
- I'll be in in a minute.
- Sooner rather than later.

414
00:18:30,147 --> 00:18:31,921
Right, Ed?

415
00:18:49,959 --> 00:18:52,504
If you're trying to kill me,
you got to squeeze harder.

416
00:18:56,088 --> 00:18:57,704
I just want to thank you.

417
00:18:57,705 --> 00:18:58,845
For what?

418
00:18:58,846 --> 00:19:01,131
Oh, you saw I hired the maid.

419
00:19:01,132 --> 00:19:04,184
- You hired a maid?
- I'm not a maid.

420
00:19:06,194 --> 00:19:09,886
I have a masters in
folklore and a nursing degree

421
00:19:09,887 --> 00:19:13,664
from the university of
Phoenix in Los Angeles.

422
00:19:14,694 --> 00:19:16,652
Coaster...

423
00:19:16,653 --> 00:19:18,854
Yeah, that's how that goes.

424
00:19:18,855 --> 00:19:21,057
Oh, my God, you gave Tim
a job. That's fantastic.

425
00:19:21,058 --> 00:19:23,208
Yeah, I'm living the dream.

426
00:19:25,131 --> 00:19:27,106
So if you're not thanking
me for hiring Tim,

427
00:19:27,107 --> 00:19:29,075
what are you thanking me for?

428
00:19:29,076 --> 00:19:31,619
Well, I just woke up today,

429
00:19:31,620 --> 00:19:34,397
and I felt really happy to be here.

430
00:19:34,398 --> 00:19:35,989
So thank you.

431
00:19:35,990 --> 00:19:37,307
Fine.

432
00:19:37,308 --> 00:19:39,494
Now, would you back it up about a foot?

433
00:19:39,495 --> 00:19:41,968
Your breath smells of hot garbage.

434
00:19:44,120 --> 00:19:46,042
Really happy to be here.

435
00:19:46,043 --> 00:19:49,052
Excuse me. What is this?

436
00:19:52,968 --> 00:19:55,383
I like this guy!

437
00:19:58,585 --> 00:20:00,606
Oh, okay, shh, shh. Here she is.

438
00:20:00,607 --> 00:20:02,737
This is Tigre Ramirez in Chula vista,

439
00:20:02,738 --> 00:20:05,114
where I'm getting some
insider home-buying tips

440
00:20:05,115 --> 00:20:08,936
at the local office of San
Diego realtor Bonnie Goodson.

441
00:20:12,001 --> 00:20:14,046
Thank you, Tigre.

442
00:20:14,047 --> 00:20:15,944
You know, looking for a real-estate agent

443
00:20:15,945 --> 00:20:17,382
can be a terrifying process.

444
00:20:17,383 --> 00:20:19,189
I've heard a lot of horror stories.

445
00:20:19,190 --> 00:20:21,588
Above all, it's important to get someone

446
00:20:21,589 --> 00:20:24,986
with a face you can trust.

