1
00:00:23,008 --> 00:00:29,079
Himalaya mountain range
Tibet, NEPAL

2
00:00:35,339 --> 00:00:39,492
Tibetan monastery
Tibet, NEPAL

3
00:01:12,571 --> 00:01:18,365
Mount Everest, North Face
Tibet, NEPAL

4
00:01:40,879 --> 00:01:46,404
Hot springs
Nagano, JAPAN

5
00:03:28,734 --> 00:03:33,794
Durbar Square
Kathmandu, NEPAL

6
00:04:08,067 --> 00:04:12,516
Pasupati temple
Kathmandu, NEPAL

7
00:04:35,570 --> 00:04:40,704
Swayambhu Temple
Kathmandu, NEPAL

8
00:04:46,937 --> 00:04:52,022
Bhaktapur: Hanuman Ghat
Kathmandu, NEPAL

9
00:05:13,559 --> 00:05:18,570
Pasupati Temple
Kathmandu, NEPAL

10
00:05:56,048 --> 00:06:00,761
Swayambhu Temple
Kathmandu, NEPAL

11
00:06:32,348 --> 00:06:37,120
Sadhu (Shiva devotee) praying
Kathmandu, NEPAL

12
00:07:04,494 --> 00:07:09,431
Wailing Wall
Jerusalem, ISRAEL

13
00:07:29,335 --> 00:07:34,556
The Whirling Dervishes, Galata Mevlevi Temple
Instambul, TURKEY

14
00:08:08,480 --> 00:08:13,375
Muhammad Shrine
Mecca, SAUDI ARABIA

15
00:08:26,474 --> 00:08:33,520
Emam Mosque
Isfahan, IRAN

16
00:08:50,024 --> 00:08:55,462
Church of the Holy Sepulchre
Jerusalem, ISRAEL

17
00:09:04,589 --> 00:09:09,673
Reims Cathedral
Reims, FRANCE

18
00:09:27,098 --> 00:09:32,300
Church of the Holy Sepulchre
Jerusalem, ISRAEL

19
00:09:47,952 --> 00:09:53,816
Light offerings, Jokang Temple
Lhasa, TIBET

20
00:10:20,263 --> 00:10:25,258
Swayambhu Temple
Kathmandu, NEPAL

21
00:10:36,518 --> 00:10:40,656
Oil fire
KUWAIT

22
00:10:48,547 --> 00:10:53,897
Ryoan-Ji Temple, Zen Karesansui rock garden
Kyoto, JAPAN

23
00:11:11,372 --> 00:11:14,549
Tokyo, JAPAN

24
00:11:15,029 --> 00:11:17,357
HONG-KONG

25
00:11:19,943 --> 00:11:22,419
Tokyo, JAPAN

26
00:11:34,576 --> 00:11:40,348
Big Sur
California, USA

27
00:11:53,222 --> 00:11:58,591
Uluwatu Temple
Bali, INDONESIA

28
00:12:02,035 --> 00:12:07,635
Rice fields
Bali, INDONESIA

29
00:12:24,145 --> 00:12:29,803
Preah Khan Temple
Angkor, CAMBODIA

30
00:12:32,385 --> 00:12:38,447
Angkor Wat
Angkor, CAMBODIA

31
00:12:45,013 --> 00:12:51,941
Angkor Wat
Angkor, CAMBODIA

32
00:12:57,497 --> 00:13:01,682
Prambanan Temple
Java, INDONESIA

33
00:13:03,053 --> 00:13:08,609
Borodour Temple
Java, INDONESIA

34
00:13:15,680 --> 00:13:20,226
Prambanan Temple
Java, INDONESIA

35
00:13:20,876 --> 00:13:25,061
Gunung Kawi, Tampak Siring Temple
Bali, INDONESIA

36
00:13:26,288 --> 00:13:30,473
Kecak (Balinese Monkey Chant)
Bali, INDONESIA

37
00:15:45,625 --> 00:15:50,388
Mount Bromo
Java, INDONESIA

38
00:16:25,313 --> 00:16:31,230
Puuoo National Park
HAWAII

39
00:17:10,123 --> 00:17:16,401
Haleakala National Park
Maui, HAWAII

40
00:18:10,304 --> 00:18:16,510
Arches National Park
Utah, USA

41
00:18:40,251 --> 00:18:45,302
Canyonlands National Park
Utah, USA

42
00:19:01,578 --> 00:19:07,235
Mesa Verde National Park
Colorado, USA

43
00:19:41,121 --> 00:19:46,374
Arches National Park
Utah, USA

44
00:20:10,519 --> 00:20:16,868
Galapagos Islands
ECUADOR

45
00:20:58,070 --> 00:21:03,843
Ayers Rock, Uluru National Park
AUSTRALIA

46
00:21:42,881 --> 00:21:48,365
Cave drawings, Kakadu National Park
Darwin, AUSTRALIA

47
00:22:43,753 --> 00:22:49,309
Al Aukre, Kayapo Village
BRAZIL

48
00:23:19,759 --> 00:23:25,099
Masaai Mara, Masaai tribe
KENIA

49
00:25:24,377 --> 00:25:29,645
Kayapo tribe
BRAZIL

50
00:26:11,659 --> 00:26:17,612
Pukamani ceremony (Funeral Dance), Tiwi Tribe
Bathurst Island, AUSTRALIA

51
00:26:49,813 --> 00:26:56,308
Flamingoes
Lake Magadi, KENYA

52
00:27:05,147 --> 00:27:10,469
Iguazu Falls
ARGENTINA

53
00:28:51,808 --> 00:28:58,572
Lake Natron
TANZANIA

54
00:30:11,814 --> 00:30:18,038
Serengeti
KENYA

55
00:30:27,328 --> 00:30:33,372
Thompson Gazelles, Masaai Mara
KENYA

56
00:30:56,057 --> 00:31:01,694
Rainforest
BRAZIL

57
00:31:55,589 --> 00:32:01,452
Kayapo Leader
BRAZIL

58
00:32:08,555 --> 00:32:14,080
Serra Pelada gold mine
BRAZIL

59
00:32:59,744 --> 00:33:05,381
Al Aukre
BRAZIL

60
00:33:15,669 --> 00:33:20,179
Kayapo tribe
BRAZIL

61
00:33:29,058 --> 00:33:34,555
Favela da Rocinha
Rio de Janeiro, BRAZIL

62
00:35:51,825 --> 00:35:57,181
Walled City, demolished in 1992
Kowloon, HONG-KONG

63
00:36:36,502 --> 00:36:42,280
Ciudad Blanca, Cemeterio General
(The White City, Cemetery)
Guyaquil, ECUADOR

64
00:37:09,199 --> 00:37:14,413
INDONESIA

65
00:37:19,910 --> 00:37:26,675
Gudang Garam Cigarette Factory
Kediri, Java, INDONESIA

66
00:38:22,062 --> 00:38:27,418
JR Shinjuku Station
Tokyo, JAPAN

67
00:39:17,309 --> 00:39:23,369
Green Plaza Capsule Hotel
Tokyo, JAPAN

68
00:39:36,758 --> 00:39:42,678
Japanese Buddist monk
Tokyo, JAPAN

69
00:41:36,977 --> 00:41:43,178
Belo Horizonte
Sao Paolo, BRAZIL

70
00:42:13,902 --> 00:42:20,526
Park Avenue
New-York, USA

71
00:43:07,599 --> 00:43:13,378
Great Mosque
Mecca, SAUDI ARABIA

72
00:43:22,961 --> 00:43:28,458
Shibuya Station
Tokyo, JAPAN

73
00:43:53,827 --> 00:44:00,592
Grand Central Terminal
New-York, USA

74
00:44:29,343 --> 00:44:35,121
JR Shinjuku Rail Station
Tokyo, JAPAN

75
00:45:24,449 --> 00:45:29,945
JVC Yokosuka Factory
Tokyo, JAPAN

76
00:46:07,160 --> 00:46:13,077
NMB Keyboard Factory
Bang Pa-In, Ayutthaya province, THAILAND

77
00:46:36,882 --> 00:46:43,662
Gudang Garam Cigarette Factory
Kediri, Java, INDONESIA

78
00:47:20,296 --> 00:47:27,186
World Trade Center, North Tower
New-York, USA

79
00:47:54,852 --> 00:48:00,539
Chicken farm

80
00:50:16,730 --> 00:50:23,073
Park Avenue
New-York, USA

81
00:50:48,992 --> 00:50:54,766
Silent scream, Buto dancer
Tokyo, JAPAN

82
00:51:29,257 --> 00:51:34,506
YEMEN

83
00:52:02,392 --> 00:52:07,969
City landfill
Calcutta, INDIA

84
00:54:07,605 --> 00:54:13,948
S�o Jo�o Avenue
S�o Paolo, BRAZIL

85
00:55:36,293 --> 00:55:42,745
INDIA

86
00:56:04,178 --> 00:56:09,099
CAMBODIA

87
00:56:44,531 --> 00:56:49,998
BRAZIL

88
00:57:05,964 --> 00:57:11,104
Bangkok, THAILAND

89
00:57:35,491 --> 00:57:41,069
Buto dance
Tokyo, JAPAN

90
00:58:45,808 --> 00:58:50,839
B-52 bombers, Aerospace Maintenance and Regeneration Center
Tucson, Arizona, USA

91
00:59:21,786 --> 00:59:26,707
Wailing Wall
Jerusalem, ISRAEL

92
00:59:43,876 --> 00:59:49,563
Oil fire
KUWAIT

93
01:01:18,687 --> 01:01:24,702
Highway 80
KUWAIT

94
01:02:01,665 --> 01:02:07,461
Bytom foundry
Bytom, POLAND

95
01:03:11,763 --> 01:03:18,980
Konzentrationslager
Auschwitz, POLAND

96
01:04:59,998 --> 01:05:06,013
Tuol Sleng Museum
Phnom Pen, CAMBODIA

97
01:06:16,820 --> 01:06:23,108
CAMBODIA

98
01:06:45,116 --> 01:06:52,361
Great Hall of The People, Tianenman Square
Beijing, CHINA

99
01:07:38,290 --> 01:07:45,467
People's Heros Monument, Tienanmen Square
Beijing, CHINA

100
01:07:54,010 --> 01:08:00,845
Terracotta Army, Mausoleum of the First Qin Emperor
Shaanxi province, CHINA

101
01:08:53,985 --> 01:09:00,649
Persepolis, IRAN

102
01:09:48,663 --> 01:09:56,352
Pyramids at Giza
Cairo, EGYPT

103
01:10:15,660 --> 01:10:21,128
Temple Of Ramses II
Luxor, EGYPT

104
01:10:47,100 --> 01:10:54,106
Angkor Wat
Angkor, CAMBODIA

105
01:11:23,837 --> 01:11:29,817
Angkor Ta Proum courtyard
Angkor, CAMBODIA

106
01:12:40,215 --> 01:12:46,196
Ganges River
INDIA

107
01:13:24,300 --> 01:13:31,134
Ghats
Varanasi, INDIA

108
01:13:44,121 --> 01:13:50,272
Dasaswameth Ghat
Varanasi, INDIA

109
01:14:00,524 --> 01:14:06,334
Uttar Pradesh
Varanasi, INDIA

110
01:14:34,527 --> 01:14:40,849
Benaras Ghat
Varanasi, INDIA

111
01:15:12,289 --> 01:15:20,149
Ganges River
INDIA

112
01:19:43,159 --> 01:19:50,848
Ryoan-Ji Temple
Kyoto, JAPAN

113
01:20:28,866 --> 01:20:35,487
Masaai Mara
KENYA

114
01:21:16,923 --> 01:21:24,185
The Whirling Dervishes, Galata Mevlevi Temple
Instambul, TURKEY

115
01:22:27,833 --> 01:22:33,813
Wailing Wall
Jerusalem, ISRAEL

116
01:22:44,706 --> 01:22:50,473
Church of the Holy Sepulchre
Jerusalem, ISRAEL

117
01:23:11,618 --> 01:23:16,317
Isfahan, IRAN

118
01:23:21,656 --> 01:23:29,131
The Ka'aba
Mecca, SAUDI ARABIA

119
01:24:14,198 --> 01:24:20,178
Grand Mosque
Mecca, SAUDI ARABIA

120
01:24:32,353 --> 01:24:37,479
Hagia Sophia
Instambul, TURKEY

121
01:24:37,480 --> 01:24:43,886
St. Peter's Basilica
VATICAN, ITALY

122
01:25:12,507 --> 01:25:20,623
Mausoleum of Shah-e-Cherach
Shiraz, IRAN

123
01:25:34,720 --> 01:25:41,554
Church of the Holy Sepulchre
Jerusalem, ISRAEL

124
01:25:55,438 --> 01:26:01,632
Funeral Lights on the Ganges
Varanasi, INDIA

125
01:26:21,281 --> 01:26:27,048
Ryoan-Ji Temple
Kyoto, JAPAN

126
01:26:49,261 --> 01:26:55,455
Angkor Wat
Angkor, CAMBODIA

127
01:28:27,083 --> 01:28:34,132
Temple Of Ramses II
Luxor, EGYPT

128
01:29:28,597 --> 01:29:36,927
The Turret Arch, Arches National Park
Utah, USA

