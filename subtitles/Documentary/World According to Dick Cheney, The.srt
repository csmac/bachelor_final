﻿1
00:00:06,010 --> 00:00:07,420
Subtitles by DramaFever

2
00:00:07,420 --> 00:00:09,450
Then...

3
00:00:09,450 --> 00:00:12,910
who's the culprit?

4
00:00:12,910 --> 00:00:14,450
The culprit?

5
00:00:23,510 --> 00:00:25,160
It's Young Joon.

6
00:00:28,030 --> 00:00:30,100
What... what did you say?

7
00:00:30,760 --> 00:00:33,850
The person who committed
murder on that night...

8
00:00:35,320 --> 00:00:40,990
was Sung Young Joon,
your younger brother.

9
00:00:40,990 --> 00:00:45,670
I felt bad for causing your death
through the car accident

10
00:00:45,670 --> 00:00:51,800
so I helped to hide the fact that
your brother was the murderer.

11
00:00:54,560 --> 00:00:56,440
Where do you think...

12
00:00:57,060 --> 00:01:01,610
Young Joon got the money to
attend medical school?

13
00:01:01,610 --> 00:01:04,060
I paid for his tuition.

14
00:01:04,060 --> 00:01:05,800
I did all that...

15
00:01:05,800 --> 00:01:09,790
because I felt bad for
what I had done to you.

16
00:01:09,790 --> 00:01:11,990
You... expect me to believe that?

17
00:01:12,980 --> 00:01:14,290
Well, I can't believe that.

18
00:01:14,290 --> 00:01:16,750
It'd be best for you to believe me...

19
00:01:18,010 --> 00:01:21,040
before everyone else
gets here, though.

20
00:01:25,790 --> 00:01:27,940
This is the transfer statement.

21
00:01:27,940 --> 00:01:32,110
Young Joon's tuition
and living expenses

22
00:01:32,110 --> 00:01:34,650
are all listed in there.

23
00:01:39,030 --> 00:01:40,670
<i>[Transfer Statement]</i>

24
00:01:41,560 --> 00:01:43,090
<i>[Chungho Foundation]</i>

25
00:01:43,090 --> 00:01:44,900
<i>[Sung Young Joon]</i>

26
00:01:44,910 --> 00:01:46,710
This can't be.

27
00:01:46,710 --> 00:01:48,560
<i>[Automatic Transfer
from Chungho Foundation]</i>

28
00:01:48,560 --> 00:01:50,000
<i>[Sung Young Joon]</i>

29
00:01:50,000 --> 00:01:52,310
This can't be!

30
00:02:12,670 --> 00:02:15,940
He was supposed to stay with
Chairman Cha, so where is he going?

31
00:02:17,870 --> 00:02:21,590
I'm sorry, but could you
wait here for a bit?

32
00:02:21,590 --> 00:02:22,790
All right.

33
00:02:22,790 --> 00:02:24,970
Thank you, I'll be right back.

34
00:02:30,140 --> 00:02:35,080
Geez, where did he go?
Driver Hwang's wife is here to see him.

35
00:02:47,800 --> 00:02:51,570
- Let's talk for a bit.
- The detective will be here soon.

36
00:02:51,570 --> 00:02:53,480
We can talk then.

37
00:02:53,480 --> 00:02:56,860
You said that Min Joon's uncle
didn't take the blame for the incident

38
00:02:56,860 --> 00:02:59,360
just for the money.

39
00:02:59,360 --> 00:03:02,810
And you wanted to know the other
reason why he agreed to it, correct?

40
00:03:10,020 --> 00:03:12,120
<i>Young Joon wasn't home then.</i>

41
00:03:12,120 --> 00:03:14,840
<i>He was with Tae Hoon's father.</i>

42
00:03:14,840 --> 00:03:17,350
<i>They seemed like
they were pretty close.</i>

43
00:03:19,060 --> 00:03:22,570
[Cardiology Department Sung Young Joon]

44
00:03:35,680 --> 00:03:38,030
Where were you
on that night?

45
00:03:38,030 --> 00:03:39,460
Huh?

46
00:03:39,460 --> 00:03:42,730
Where were you on
the night of my accident?

47
00:03:42,730 --> 00:03:44,640
Tell me.

48
00:03:44,640 --> 00:03:47,120
Tell me that you were
somewhere else, please!

49
00:03:47,120 --> 00:03:49,270
Why should I have to tell you that?

50
00:03:50,030 --> 00:03:51,700
What?

51
00:03:51,700 --> 00:03:55,080
I'm so damned sick and tired
of talking about that night!

52
00:03:55,080 --> 00:03:57,680
I have nothing to discuss
with you, so get out of here.

53
00:03:58,680 --> 00:04:02,090
- Sung... Sung Young Joon.
- I said get out!

54
00:04:04,010 --> 00:04:05,190
Did you...

55
00:04:06,310 --> 00:04:09,140
did you accept money from
Chairman Cha?

56
00:04:09,140 --> 00:04:12,420
Did you accept money for
tuition and living expenses from him?

57
00:04:12,420 --> 00:04:14,470
Did you really do that?

58
00:04:16,490 --> 00:04:20,950
The help I've agreed to give you...
Let's just keep that between us.

59
00:04:27,020 --> 00:04:28,200
No.

60
00:04:29,190 --> 00:04:31,240
No, huh?

61
00:04:31,240 --> 00:04:33,010
You really didn't?

62
00:04:33,010 --> 00:04:35,180
Why would I accept his money?

63
00:04:35,180 --> 00:04:37,960
Stop being so ridiculous,
and get out.

64
00:04:47,350 --> 00:04:49,440
What do you think is the reason?

65
00:04:49,440 --> 00:04:54,670
Min Joon's uncle did accept
money from me, of course.

66
00:04:54,670 --> 00:04:56,520
And yet...

67
00:04:56,520 --> 00:04:59,140
you don't think the money
is all there was to it?

68
00:05:00,250 --> 00:05:02,570
Well, you're right.

69
00:05:02,570 --> 00:05:05,190
That wasn't all there was to it.

70
00:05:06,680 --> 00:05:11,100
Where do you think Min Joon got the money
to open such a big restaurant?

71
00:05:11,100 --> 00:05:13,200
If he didn't have the money...

72
00:05:13,200 --> 00:05:16,670
he would've been working
in someone else's kitchen.

73
00:05:16,670 --> 00:05:22,900
I was told that Min Joon's uncle's
acquaintance invested in the restaurant.

74
00:05:24,510 --> 00:05:27,530
The investment for
Min Joon's restaurant...

75
00:05:27,530 --> 00:05:30,730
that was my money.

76
00:05:30,730 --> 00:05:33,740
What?
Is that true?

77
00:05:33,740 --> 00:05:35,730
Didn't he say that the money
was from "CEO Park?"

78
00:05:35,730 --> 00:05:38,890
You can go and ask him
yourself, if you'd like.

79
00:05:38,890 --> 00:05:40,040
But why?

80
00:05:40,040 --> 00:05:42,520
Why did you invest your
money into Min Joon?

81
00:05:42,520 --> 00:05:44,580
Because despite everything,
you're still his father?

82
00:05:44,580 --> 00:05:48,880
Because that was the promise
I made with Min Joon's uncle.

83
00:05:50,350 --> 00:05:52,680
Min Joon's uncle...

84
00:05:52,680 --> 00:05:54,880
really cared about
Min Joon quite a lot.

85
00:05:54,880 --> 00:05:59,520
The main condition he had
was that I take care of Min Joon.

86
00:05:59,520 --> 00:06:02,520
You do know that his restaurant
will close down...

87
00:06:02,520 --> 00:06:05,940
the moment I withdraw
my investment, right?

88
00:06:08,790 --> 00:06:12,030
If you don't want to be the cause
of Min Joon's ruin...

89
00:06:12,580 --> 00:06:14,990
then go back to Australia...

90
00:06:15,700 --> 00:06:17,960
immediately.

91
00:06:33,320 --> 00:06:35,680
Where have you been?

92
00:06:36,290 --> 00:06:38,660
Driver Hwang's wife
just disappeared, too.

93
00:06:38,660 --> 00:06:40,230
What is going on here?

94
00:06:41,280 --> 00:06:42,580
Hey, what about Chairman Cha?

95
00:06:42,580 --> 00:06:45,140
What did he say?
Did he admit to his crime?

96
00:06:47,160 --> 00:06:50,260
What happened? Why are you
acting like this all of the sudden?

97
00:06:52,110 --> 00:06:53,400
Ho Bang.

98
00:06:55,250 --> 00:06:57,390
I think that we'll have to
give up on this now.

99
00:06:57,390 --> 00:06:59,990
Hey, what are you talking about?
Why would we give up?

100
00:07:01,010 --> 00:07:03,290
Let's just forget about
all this, okay?

101
00:07:03,290 --> 00:07:07,850
Let's not investigate this further,
and just let it be.

102
00:07:08,890 --> 00:07:11,070
<i>I saw Young Joon earlier
this afternoon.</i>

103
00:07:11,070 --> 00:07:13,960
<i>He was with Tae Hoon's father.</i>

104
00:07:13,960 --> 00:07:16,470
<i>They seemed like
they were pretty close.</i>

105
00:07:16,470 --> 00:07:19,220
Tell me where you were.

106
00:07:20,590 --> 00:07:23,040
You went to the hospital, right?

107
00:07:37,350 --> 00:07:39,010
Detective!

108
00:07:41,790 --> 00:07:43,420
How did it go?

109
00:07:45,010 --> 00:07:46,750
Um...

110
00:07:47,300 --> 00:07:49,480
Did... something happen?

111
00:07:50,480 --> 00:07:52,430
What about my aunt?

112
00:07:52,430 --> 00:07:56,870
I'm sorry to tell you this, but...

113
00:08:01,800 --> 00:08:04,880
Oh, it's my aunt.
Just a moment.

114
00:08:06,020 --> 00:08:08,680
Aunt!
Where are you?

115
00:08:08,680 --> 00:08:10,750
Min Joon.

116
00:08:11,770 --> 00:08:14,870
I'm heading to the airport right now.

117
00:08:15,310 --> 00:08:18,020
The airport?
But, why?

118
00:08:18,690 --> 00:08:22,610
Sorry, but I need to hang up now.

119
00:08:24,130 --> 00:08:25,520
Aunt!

120
00:08:31,030 --> 00:08:34,180
Can you tell me what that was about?

121
00:08:34,650 --> 00:08:36,480
I'll be going now.

122
00:08:48,050 --> 00:08:49,950
Aunt!

123
00:09:00,940 --> 00:09:06,030
Why have you suddenly changed
your mind about testifying?

124
00:09:06,030 --> 00:09:09,610
You weren't like this
up until a short while ago.

125
00:09:11,220 --> 00:09:15,660
I couldn't bring myself to say anything
when I was actually facing your father.

126
00:09:16,260 --> 00:09:20,050
Your uncle and I were also
in the wrong, Min Joon.

127
00:09:20,050 --> 00:09:21,600
What?

128
00:09:21,600 --> 00:09:23,990
I'll be honest and
tell you everything.

129
00:09:27,140 --> 00:09:29,880
Your uncle...

130
00:09:29,880 --> 00:09:33,390
received money in exchange
for taking the blame.

131
00:09:35,020 --> 00:09:38,240
We were desperate for the money
to pay our daughter's hospital fees.

132
00:09:38,240 --> 00:09:41,050
We had to try anything we possibly could
for our only daughter.

133
00:09:41,650 --> 00:09:45,570
So we wanted to let her get
the surgery before she died.

134
00:09:45,570 --> 00:09:47,250
I'm sorry.

135
00:09:48,140 --> 00:09:51,540
Your uncle carried that
secret to his grave

136
00:09:51,540 --> 00:09:54,410
so I didn't think I had the right
to bring it up again.

137
00:09:55,670 --> 00:09:58,860
I do understand that you want to...

138
00:09:58,860 --> 00:10:02,520
make Chairman Cha
pay for his crimes.

139
00:10:02,520 --> 00:10:04,390
But I'm sorry.

140
00:10:04,390 --> 00:10:09,630
Just forget about it,
and live your own life happily.

141
00:10:10,170 --> 00:10:12,980
And I wish you success
with your business.

142
00:10:13,690 --> 00:10:15,790
I'm sorry.

143
00:10:24,790 --> 00:10:27,560
Is he not home yet?

144
00:10:27,560 --> 00:10:29,620
The lights are off, too.

145
00:10:38,580 --> 00:10:40,790
<i>Hae Sung!
Sung Hae Sung!</i>

146
00:10:44,040 --> 00:10:46,300
<i>Hae Sung!
Sung Hae Sung!</i>

147
00:10:50,260 --> 00:10:52,280
<i>He must not be home.</i>

148
00:10:53,010 --> 00:10:54,710
<i>Where did he go?</i>

149
00:10:56,690 --> 00:10:59,470
<i>Who's the culprit, then?</i>

150
00:11:00,990 --> 00:11:02,560
It's Young Joon.

151
00:11:30,800 --> 00:11:32,560
Here we go.

152
00:11:36,050 --> 00:11:37,310
Here it is!

153
00:11:37,310 --> 00:11:39,090
Wow!

154
00:11:41,070 --> 00:11:42,240
- Wow!
- Wow!

155
00:11:42,240 --> 00:11:44,020
- Eat up.
- Thank you for the meal!

156
00:11:44,020 --> 00:11:45,360
Thank you for the meal!

157
00:11:47,030 --> 00:11:48,500
How is it?

158
00:11:48,500 --> 00:11:50,080
Yum!

159
00:11:50,080 --> 00:11:53,290
The food you make
really is the best, Big Bro!

160
00:11:53,730 --> 00:11:56,350
Hey, Sung Hae Chul. Why didn't you
wash up before eating, though?

161
00:11:56,350 --> 00:11:57,520
Wow!

162
00:11:57,520 --> 00:12:00,520
You really are going to make
a great husband one day!

163
00:12:00,520 --> 00:12:01,780
Well, duh.

164
00:12:01,780 --> 00:12:05,880
You know that you have to study hard to
find someone as good looking as me?

165
00:12:05,880 --> 00:12:07,950
Sure, whatever.

166
00:12:09,230 --> 00:12:10,750
Young In.
Sung Young In!

167
00:12:10,750 --> 00:12:12,970
You're not leaving without
eating again, are you?

168
00:12:15,060 --> 00:12:18,130
What, and listen to you nag at me
all day because of it?

169
00:12:18,130 --> 00:12:20,220
- I'll eat, I'll eat.
- Eat up, it's delicious.

170
00:12:21,360 --> 00:12:22,770
Sung Hae Sung.

171
00:12:22,770 --> 00:12:27,210
The kids are going to treat you like an
outcast if you keep nagging at them!

172
00:12:27,750 --> 00:12:30,720
Why are you here?
Did you sleep in Young In's room again?

173
00:12:30,720 --> 00:12:33,880
Yeah, so that I could eat
the breakfast that you cooked.

174
00:12:35,780 --> 00:12:37,060
- Oh yeah, Hae Sung.
- Yeah?

175
00:12:37,060 --> 00:12:39,990
Close your restaurant today.
You and I need to go somewhere.

176
00:12:39,990 --> 00:12:42,000
You want me to close
the restaurant for today?

177
00:12:42,000 --> 00:12:44,810
- Why, where are we going?
- You'll see.

178
00:12:45,700 --> 00:12:47,490
He has a really masculine face

179
00:12:47,490 --> 00:12:51,640
so he looks handsome in different ways
based on what hairstyle he has.

180
00:12:51,640 --> 00:12:53,450
Please be mindful of that.

181
00:12:55,110 --> 00:12:57,130
This isn't too bad, either.

182
00:12:57,130 --> 00:12:59,600
But this won't do because
he looks too handsome like this.

183
00:13:05,800 --> 00:13:08,040
Perfect.
I like this.

184
00:13:11,150 --> 00:13:12,260
Excuse me.

185
00:13:12,260 --> 00:13:15,590
He has broad shoulders,
so he looks great in suits.

186
00:13:15,590 --> 00:13:17,250
Please be mindful of that.

187
00:13:17,250 --> 00:13:18,300
Yes, ma'am.

188
00:13:18,610 --> 00:13:20,840
Oh, I'm all set.

189
00:13:38,040 --> 00:13:39,850
How do I look?

190
00:13:39,850 --> 00:13:41,500
You look so cool!
So cool!

191
00:13:43,810 --> 00:13:45,650
Let's hurry and go.
We'll be late.

192
00:13:45,650 --> 00:13:47,610
But, where are we going?

193
00:13:47,610 --> 00:13:49,910
You'll see when we get there.
Let's hurry and go.

194
00:13:50,800 --> 00:13:52,490
The Hippocratic Oath.

195
00:13:54,020 --> 00:14:00,060
"I solemnly pledge to dedicate my life
to the service of humanity."

196
00:14:00,060 --> 00:14:05,230
"The health and well-being of my patient
will be my first consideration."

197
00:14:05,230 --> 00:14:11,760
"I make these promises solemnly,
freely and upon my honor."

198
00:14:22,010 --> 00:14:23,790
Hae Sung.

199
00:14:24,750 --> 00:14:26,020
Young Joon.

200
00:14:26,870 --> 00:14:30,190
Thanks to you, I was able to graduate
medical school.

201
00:14:35,020 --> 00:14:39,070
I'll become a great doctor.

202
00:14:39,070 --> 00:14:43,570
And I'll repay you for struggling
so much for my sake.

203
00:14:44,120 --> 00:14:46,050
Thanks, Big Bro.

204
00:14:48,240 --> 00:14:51,340
Don't be good to me,
be good to your patients instead!

205
00:14:52,690 --> 00:14:56,160
Looks like Big Bro is nagging again.

206
00:14:56,160 --> 00:14:58,480
Congratulations on graduating,
Young Joon!

207
00:14:58,480 --> 00:15:00,780
When it comes to nagging,
Sung Hae Sung can't be beat.

208
00:15:00,780 --> 00:15:02,930
It's been so long since all of us
have gotten together like this.

209
00:15:02,930 --> 00:15:04,350
We can get together
more often now.

210
00:15:04,350 --> 00:15:05,460
Yeah, let's do that.

211
00:15:05,460 --> 00:15:07,380
I'm hungry, Big Bro.

212
00:15:07,380 --> 00:15:09,470
Oh, really? Let's hurry and
go eat something, then.

213
00:15:09,470 --> 00:15:11,370
You have to eat jjajangmyun
on your graduation day.

214
00:15:11,370 --> 00:15:12,880
I'll buy. Let's go!

215
00:15:12,880 --> 00:15:13,880
- Really?
- Wow!

216
00:15:13,880 --> 00:15:15,690
- Let's go, let's go!
- Let's go!

217
00:15:20,230 --> 00:15:22,480
- What should we get?
- I want kkangpoongi.

218
00:15:27,260 --> 00:15:29,260
Hurry up!

219
00:15:29,260 --> 00:15:30,790
Coming!

220
00:16:25,350 --> 00:16:27,230
So it was just a dream, huh?

221
00:16:30,700 --> 00:16:32,550
Are you awake now?

222
00:16:37,010 --> 00:16:39,670
Hey, Jung Won was
worrying about you earlier

223
00:16:39,670 --> 00:16:41,480
so I told her that
it was nothing.

224
00:16:41,480 --> 00:16:43,460
All right.
Thanks.

225
00:16:46,250 --> 00:16:49,000
Go and get some
fresh air, or something.

226
00:16:52,660 --> 00:16:56,300
If you keep acting this way, Jung Won
will realize that something is up.

227
00:17:00,250 --> 00:17:01,670
Yeah.

228
00:17:02,360 --> 00:17:03,530
Yeah.

229
00:17:03,530 --> 00:17:07,370
I'll tell Jung Won to tell the folks at
the restaurant that you can't work today

230
00:17:07,370 --> 00:17:09,800
so go and get some fresh air
by yourself, or something.

231
00:17:09,800 --> 00:17:10,820
Okay?

232
00:18:12,020 --> 00:18:14,270
I did give him money.

233
00:18:14,860 --> 00:18:18,540
And in exchange for that,
your uncle went to prison.

234
00:18:18,540 --> 00:18:19,850
So what?

235
00:18:19,850 --> 00:18:21,740
What did you say to my aunt
to have her

236
00:18:21,740 --> 00:18:23,980
fly back to Australia
without even testifying?

237
00:18:23,980 --> 00:18:25,890
Why are you asking me that?

238
00:18:25,890 --> 00:18:28,310
Go and ask your aunt that!

239
00:18:29,210 --> 00:18:32,860
I have work to do, so stop saying
frustrating things and leave!

240
00:18:33,500 --> 00:18:35,580
Let me make this one thing clear.

241
00:18:35,580 --> 00:18:37,140
I vow...

242
00:18:37,140 --> 00:18:39,620
to make you pay for what you did.

243
00:18:41,040 --> 00:18:43,040
You punk!

244
00:18:43,040 --> 00:18:47,720
I'll make you keenly aware of the fact
that money can't solve everything.

245
00:18:47,720 --> 00:18:50,320
I'll make one thing very clear, too.

246
00:18:51,130 --> 00:18:53,250
Stop messing around.

247
00:18:53,250 --> 00:18:56,700
You'd better listen to me, because
I'm telling you this as your father!

248
00:18:57,350 --> 00:18:59,250
My father?

249
00:18:59,250 --> 00:19:02,450
<i>Fine.
I'll keep that in mind, Father.</i>

250
00:19:04,020 --> 00:19:05,860
"Father"?

251
00:19:05,860 --> 00:19:09,060
You'd better remember
my words too, Father.

252
00:19:19,150 --> 00:19:21,570
Is he, by any chance...

253
00:19:23,160 --> 00:19:25,360
<i>Mr. Cha Min Joon!</i>

254
00:19:30,020 --> 00:19:31,760
Have you been well, sir?

255
00:19:34,250 --> 00:19:36,480
You remember me, don't you?

256
00:19:36,480 --> 00:19:39,120
I met you at your restaurant.
I'm Jung Won's friend.

257
00:19:39,120 --> 00:19:41,200
Yes, I do remember.

258
00:19:42,090 --> 00:19:44,680
Your food was so good,
and I've been meaning to come back.

259
00:19:44,680 --> 00:19:46,570
It's quite nice to
run into you here.

260
00:19:49,030 --> 00:19:51,140
Oh, but what brings you by here?

261
00:19:51,650 --> 00:19:54,280
I had some business here.
Goodbye.

262
00:19:55,230 --> 00:19:56,680
Ah, okay then.
Goodbye.

263
00:20:00,580 --> 00:20:02,590
<i>How dare he show up here?</i>

264
00:20:02,590 --> 00:20:03,720
Why was he here?
Why?

265
00:20:03,720 --> 00:20:05,250
Tell me.

266
00:20:05,250 --> 00:20:07,410
Why did your son come here?

267
00:20:07,410 --> 00:20:10,810
Have you been keeping in touch
with him behind my back?

268
00:20:15,030 --> 00:20:18,470
I thought you said Tae Hoon
was the only son you had!

269
00:20:18,470 --> 00:20:20,410
How long has this been going on for?

270
00:20:21,060 --> 00:20:23,640
Since when has he been in touch
with you, and visiting you here?

271
00:20:23,640 --> 00:20:25,000
This was the first time.

272
00:20:25,690 --> 00:20:27,740
And we haven't been in touch
before that, either.

273
00:20:27,740 --> 00:20:31,000
Why did he come here if you two
hadn't been keeping in touch?

274
00:20:31,000 --> 00:20:33,220
There must have been a
reason why he came here!

275
00:20:33,220 --> 00:20:35,720
He stopped by just because.
Just because!

276
00:20:35,720 --> 00:20:37,730
And he won't be coming back, either!

277
00:20:37,730 --> 00:20:38,950
"Just because"?

278
00:20:39,500 --> 00:20:43,150
And you expect me
to believe that?

279
00:20:43,940 --> 00:20:45,330
I was far too naive.

280
00:20:45,330 --> 00:20:49,340
I actually believed you when you told me
that you didn't keep in touch with him.

281
00:20:49,340 --> 00:20:51,600
I completely fell for your lie.

282
00:20:51,600 --> 00:20:52,850
I'm telling the truth!

283
00:20:52,850 --> 00:20:56,010
Don't you feel sorry towards Tae Hoon,
who loves and respects you so much?

284
00:20:56,010 --> 00:20:57,420
Why are you bring up Tae Hoon?

285
00:20:57,420 --> 00:21:00,720
Why are you bringing
Tae Hoon up at a time like this? Huh?

286
00:21:03,800 --> 00:21:05,640
Father.

287
00:21:06,800 --> 00:21:09,290
What's going on here?

288
00:21:09,290 --> 00:21:11,350
You two don't look very happy.

289
00:21:14,040 --> 00:21:16,530
What were you talking about?

290
00:21:16,530 --> 00:21:19,250
Oh, it was nothing.

291
00:21:23,680 --> 00:21:26,900
<i>[Farming District (Last Stop)]</i>

292
00:21:41,540 --> 00:21:44,340
Hey!
Hey!

293
00:21:45,010 --> 00:21:47,760
This is the final stop!
Aren't you going to get off?

294
00:21:51,120 --> 00:21:53,440
Oh... um, yes.

295
00:22:38,890 --> 00:22:40,940
Oh dear!
Oh my!

296
00:22:40,940 --> 00:22:43,240
Get out of the way!
Get out of the way!

297
00:22:43,240 --> 00:22:46,070
Oh!
Oh no!

298
00:22:53,120 --> 00:22:54,570
The baby!

299
00:23:30,010 --> 00:23:32,210
Oh dear!
Oh my!

300
00:23:38,120 --> 00:23:39,640
Oh, it wasn't a baby.

301
00:23:51,250 --> 00:23:53,660
Oh dear!
Oh dear, this is terrible!

302
00:23:53,660 --> 00:23:54,780
Oh my!

303
00:23:54,970 --> 00:23:56,210
Oh no, what happened?

304
00:23:56,210 --> 00:23:58,690
Well, you see, my crane's
brakes were acting up.

305
00:23:58,690 --> 00:24:02,150
Oh dear, what do I do?
I almost killed him!

306
00:24:02,150 --> 00:24:04,270
You almost made me
lose all my corn!

307
00:24:04,270 --> 00:24:06,620
- Oh, I'm sorry. I'm sorry.
- Oh, no!

308
00:24:07,230 --> 00:24:09,670
Well, we should hurry
and call 911!

309
00:24:09,670 --> 00:24:11,590
Yes, let's hurry and call them!

310
00:24:15,800 --> 00:24:18,330
You think he should be sent to
a big hospital in Seoul?

311
00:24:18,330 --> 00:24:20,630
We did what we could,
but he's still unconscious

312
00:24:20,630 --> 00:24:23,340
so I think it'd be best if we transferred
him to a bigger hospital.

313
00:24:23,340 --> 00:24:25,300
Oh my.
What do I do?

314
00:24:25,300 --> 00:24:27,610
I'm done for.

315
00:24:28,130 --> 00:24:30,430
- Please prepare for the transfer.
- Yes, sir.

316
00:24:30,430 --> 00:24:34,250
Please calm down,
and take a seat here.

317
00:24:34,790 --> 00:24:36,260
Please wait here.

318
00:25:11,790 --> 00:25:14,830
Bring the hospital bed back there, and
be inconspicuous about it.

319
00:25:14,830 --> 00:25:16,110
- All right.
- Hurry, hurry!

320
00:25:16,110 --> 00:25:17,630
Yes, ma'am.

321
00:25:32,770 --> 00:25:34,750
The wound is gone!

322
00:25:35,500 --> 00:25:36,620
Yes!

323
00:25:59,530 --> 00:26:02,130
Oh.
Oh! Oh!

324
00:26:02,660 --> 00:26:04,070
Oh, Granny!

325
00:26:05,030 --> 00:26:07,060
Um, what happened?

326
00:26:08,650 --> 00:26:10,950
Here, drink this.

327
00:26:10,950 --> 00:26:13,810
You haven't even eaten anything,
but sleep for the past two days.

328
00:26:13,810 --> 00:26:15,290
What?
Two days?

329
00:26:15,290 --> 00:26:17,240
That's right, two days!

330
00:26:26,330 --> 00:26:29,620
I think I became dizzy
all of the sudden, and fainted.

331
00:26:29,620 --> 00:26:36,150
You stopped my crane with your
bare hands, and lost all your energy.

332
00:26:36,150 --> 00:26:38,970
That's why you were
unconscious for two days.

333
00:26:38,970 --> 00:26:40,330
Oh.

334
00:26:41,140 --> 00:26:43,030
But, where are we?

335
00:26:43,030 --> 00:26:44,530
We went to a hospital

336
00:26:44,530 --> 00:26:50,720
but I saw your wounds disappearing,
so I quickly brought you to my house.

337
00:26:50,720 --> 00:26:51,890
What?

338
00:26:53,080 --> 00:26:56,780
Other people won't be able
to understand your state of being.

339
00:26:56,780 --> 00:27:00,910
After all, you have to keep it a secret
that you're a "being who returned."

340
00:27:01,460 --> 00:27:03,860
Oh... how did you know that?

341
00:27:06,050 --> 00:27:11,690
Ten years ago, my husband also came
and left as fast as he'd come.

342
00:27:11,690 --> 00:27:13,010
Really?

343
00:27:13,010 --> 00:27:17,230
When I saw that you stopped
my crane with your bare hands

344
00:27:17,230 --> 00:27:18,610
I knew then what you were.

345
00:27:19,140 --> 00:27:23,860
My husband was also incredibly strong
when he came back.

346
00:27:23,860 --> 00:27:25,920
That's not the strength
of an ordinary human.

347
00:27:26,980 --> 00:27:31,940
At any rate, I became sure when I saw
your wounds disappearing at the hospital.

348
00:27:31,940 --> 00:27:34,920
Because the same thing
happened with my husband.

349
00:27:34,920 --> 00:27:37,720
When he used a lot
of his strength at once

350
00:27:37,720 --> 00:27:41,430
he was drained of his power,
and passed out for a day or two.

351
00:27:42,650 --> 00:27:44,320
Oh, I see.

352
00:27:44,320 --> 00:27:49,410
But, why did you come all the way
to the countryside, dressed like that?

353
00:27:51,560 --> 00:27:55,410
I just kind of somehow ended up here.

354
00:27:55,410 --> 00:27:56,960
Oh my.

355
00:27:56,960 --> 00:27:59,690
You must have a lot on your mind.

356
00:28:00,600 --> 00:28:03,570
Your face is full of worry.

357
00:28:04,340 --> 00:28:05,900
Yes.

358
00:28:05,900 --> 00:28:09,850
Well, we can take our time
discussing that later.

359
00:28:09,850 --> 00:28:13,950
Do you want to eat?
How about I get one of my chickens?

360
00:28:14,450 --> 00:28:18,020
Um, Granny, was I really
passed out for two whole days?

361
00:28:18,020 --> 00:28:19,140
Yeah!

362
00:28:19,780 --> 00:28:23,140
By the way, who's Jung Jung Won?

363
00:28:23,140 --> 00:28:25,260
You kept calling for her,
even in your sleep.

364
00:28:25,260 --> 00:28:26,520
Oh, well...

365
00:28:27,680 --> 00:28:29,720
Jung Won must be so worried.

366
00:28:30,580 --> 00:28:32,530
Can I make a phone call?

367
00:28:33,040 --> 00:28:34,860
Is she your girlfriend?

368
00:28:35,810 --> 00:28:39,410
Jung Won will get really mad at me
if I don't call her soon.

369
00:28:45,470 --> 00:28:48,730
Ignore me for two whole days now, huh?

370
00:28:48,730 --> 00:28:50,520
This is how you want to play this?

371
00:28:50,520 --> 00:28:53,410
Fine, go ahead, then!

372
00:29:02,480 --> 00:29:04,690
Maybe something has happened to him?

373
00:29:05,490 --> 00:29:07,670
He's probably okay, right?

374
00:29:08,260 --> 00:29:10,290
He's probably fine, right?

375
00:29:29,130 --> 00:29:30,570
Pardon me.

376
00:29:30,570 --> 00:29:33,800
Is there someone by the name
of Ms. Jung Jung Won here?

377
00:29:33,800 --> 00:29:36,190
Yes, that's me.

378
00:29:36,190 --> 00:29:37,820
Oh, I see.

379
00:29:40,450 --> 00:29:42,790
You know Mr. Sung Hae Sung, correct?

380
00:29:42,790 --> 00:29:46,860
Yes. Has something happened to Hae Sung?

381
00:29:46,860 --> 00:29:48,270
No.

382
00:29:48,270 --> 00:29:53,080
Mr. Sung Hae Sung is currently safe,
healthy, and doing quite well.

383
00:29:53,080 --> 00:29:56,260
He wanted me to tell you
not to worry about him.

384
00:29:56,260 --> 00:29:58,310
I'll be taking my leave now, then.

385
00:29:58,310 --> 00:30:00,120
Excuse me!

386
00:30:01,020 --> 00:30:02,860
What do you mean,
he's doing well?

387
00:30:02,860 --> 00:30:04,710
Where is he, and what is he doing?

388
00:30:09,330 --> 00:30:10,340
Excuse me!

389
00:30:10,340 --> 00:30:13,350
Just a moment!
Excuse me!

390
00:30:23,190 --> 00:30:24,460
Sung Hae Sung.

391
00:30:25,470 --> 00:30:27,720
You're healthy,
and doing well?

392
00:30:29,470 --> 00:30:32,320
So you're really going to be like this?

