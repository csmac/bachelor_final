1
00:03:21,667 --> 00:03:24,369
♪ Food, sex, sleep ♪

2
00:03:24,370 --> 00:03:26,638
♪ Money, health, flow ♪

3
00:03:26,639 --> 00:03:29,306
♪ Speed, love, nurse ♪

4
00:03:29,307 --> 00:03:31,609
♪ Peace, hair, clothes ♪

5
00:03:31,610 --> 00:03:34,279
♪ Dreams, space, fate ♪

6
00:03:34,280 --> 00:03:36,748
♪ Trouble, home, flower ♪

7
00:03:36,749 --> 00:03:39,049
♪ Push, murder, breeze ♪

8
00:03:39,050 --> 00:03:41,519
♪ Water, curves, tan ♪

9
00:03:41,520 --> 00:03:43,022
♪ Alone, mirror, truth ♪

10
00:03:44,022 --> 00:03:46,691
♪ Mother, risk, fall ♪

11
00:03:46,692 --> 00:03:49,293
♪ Shock, color, caress ♪

12
00:03:49,294 --> 00:03:50,394
♪ Hands ♪

13
00:03:50,395 --> 00:03:51,728
♪ Good ♪

14
00:03:51,729 --> 00:03:54,966
♪ Nasty, heat, please ♪

15
00:03:54,967 --> 00:03:57,567
♪ Wit, whims, sperm ♪

16
00:03:57,568 --> 00:03:59,004
♪ Just, you, fool ♪

17
00:04:00,004 --> 00:04:03,473
♪ Empty, laughs, skin ♪

18
00:04:03,474 --> 00:04:07,578
♪ Different, smell, third eye ♪

19
00:04:07,579 --> 00:04:10,347
♪ Hunt, time, fight ♪

20
00:04:23,329 --> 00:04:25,929
♪ Sacrifice ♪

21
00:04:25,930 --> 00:04:28,532
♪ Nicotine, youth ♪

22
00:04:28,533 --> 00:04:30,967
♪ Sane, lies, blue ♪

23
00:04:30,968 --> 00:04:33,504
♪ Info, clear, noise ♪

24
00:04:33,505 --> 00:04:36,641
♪ Reason, teasing ♪

25
00:04:36,642 --> 00:04:39,943
♪ Honesty, hard, soft ♪

26
00:04:39,944 --> 00:04:43,381
♪ Beauty, danger ♪

27
00:04:43,382 --> 00:04:44,849
♪ Curves, tan ♪

28
00:04:44,850 --> 00:04:47,451
♪ Alone, mirror, truth ♪

29
00:04:47,452 --> 00:04:49,886
♪ Mother, risk, fall ♪

30
00:04:49,887 --> 00:04:50,888
♪ Shock ♪

31
00:04:50,889 --> 00:04:54,958
♪ Pain, body, fame ♪

32
00:04:54,959 --> 00:04:58,395
♪ Hear me, free me ♪

33
00:04:58,396 --> 00:05:01,732
♪ Eat me, kill me ♪

34
00:05:01,733 --> 00:05:04,936
♪ Give me, form me ♪

35
00:05:04,937 --> 00:05:08,405
♪ Game me ♪

36
00:05:08,406 --> 00:05:09,941
♪ Heaven, cold ♪

37
00:05:09,942 --> 00:05:13,111
♪ Jealous, release ♪

38
00:05:13,112 --> 00:05:16,847
♪ Subtle, nature ♪

39
00:05:16,848 --> 00:05:19,917
♪ Shelter, energy ♪

40
00:05:19,918 --> 00:05:23,421
♪ Flavor, control ♪

41
00:05:23,422 --> 00:05:24,888
♪ Dirt, street ♪

42
00:05:24,889 --> 00:05:29,160
♪ Scream, ideas ♪

43
00:05:29,161 --> 00:05:32,729
♪ True, hell ♪

44
00:06:01,192 --> 00:06:02,092
Here, let's go.

45
00:07:36,822 --> 00:07:38,054
Open sesame.

46
00:07:38,055 --> 00:07:38,955
Boom.

47
00:12:24,375 --> 00:12:26,442
Big Blue's defense
step it up again.

48
00:12:26,443 --> 00:12:28,546
Look at that big sack
by Justin Tuck

49
00:12:28,547 --> 00:12:30,589
on the Giants' 7-yard line.

50
00:16:09,533 --> 00:16:12,273
Right, right, right.
Little more, little more.

51
00:16:15,273 --> 00:16:17,508
This show in New York
which we call

52
00:16:17,509 --> 00:16:21,744
"Shinohara:
Wham! Pow! Vroom!"

53
00:16:21,745 --> 00:16:23,747
is the idea of action

54
00:16:23,748 --> 00:16:27,451
and also "Vroom!"
for his motorcycle,

55
00:16:27,452 --> 00:16:30,387
because Shinohara has been
making sculpture, you know,

56
00:16:30,388 --> 00:16:32,523
since the beginning
of his artistic career.

57
00:16:32,524 --> 00:16:34,425
He's been famous
for, you know, years.

58
00:16:34,426 --> 00:16:37,529
But now is maybe
one of his key moments.

59
00:16:54,211 --> 00:16:55,646
In the 1960s,

60
00:16:55,647 --> 00:16:57,848
a Japanese painter
came to New York

61
00:16:57,849 --> 00:16:59,649
seeking to create art

62
00:16:59,650 --> 00:17:04,520
that would reflect the city's
prosperity and chaos.

63
00:17:04,521 --> 00:17:07,361
This artist is Ushio Shinohara.

64
00:17:10,361 --> 00:17:12,362
Early morning in SoHo:

65
00:17:12,363 --> 00:17:15,364
cardboard has been discarded
in the streets,

66
00:17:15,365 --> 00:17:18,235
and by walking a couple
of blocks from his loft,

67
00:17:18,236 --> 00:17:21,711
Shinohara can collect all
he needs for his day's work.

68
00:17:27,711 --> 00:17:30,114
At the moment, he is preparing
a new sculpture

69
00:17:30,115 --> 00:17:32,216
using great amounts
of cardboard

70
00:17:32,217 --> 00:17:35,756
while his wife, Noriko,
works on her own paintings.

71
00:17:46,463 --> 00:17:48,132
As a young man in Tokyo,

72
00:17:48,133 --> 00:17:49,466
he emerged as a leader

73
00:17:49,467 --> 00:17:51,768
in Japan's
avant-garde art movement

74
00:17:51,769 --> 00:17:55,172
with his imitation
of American pop art

75
00:17:55,173 --> 00:17:57,144
and action painting.

76
00:18:05,782 --> 00:18:08,184
The quality
of Ushio Shinohara's art

77
00:18:08,185 --> 00:18:10,386
is, to say the least,
in some doubt,

78
00:18:10,387 --> 00:18:11,688
but it does illustrate

79
00:18:11,689 --> 00:18:13,524
the rather frenetic spirit
of rebellion

80
00:18:13,525 --> 00:18:14,924
among some young people--

81
00:18:14,925 --> 00:18:17,460
and not only in Japan,
throughout the world.

82
00:18:26,336 --> 00:18:28,538
When he arrived
in the United States,

83
00:18:28,539 --> 00:18:31,642
he was at once in the limelight
of the mass media.

84
00:18:39,651 --> 00:18:41,852
It is said Shinohara
is the most famous

85
00:18:41,853 --> 00:18:45,788
of the poor and struggling
artists in New York.

86
00:18:45,789 --> 00:18:48,424
While his works
are exhibited across Japan,

87
00:18:48,425 --> 00:18:50,626
American collectors often say,

88
00:18:50,627 --> 00:18:54,864
"That's a wonderful image
but not my taste,"

89
00:18:54,865 --> 00:18:58,478
so most of Shinohara's pieces
have never sold.

90
00:19:40,478 --> 00:19:41,979
Really, what's most important

91
00:19:41,980 --> 00:19:43,947
is that moment when he created
the painting.

92
00:19:43,948 --> 00:19:46,583
This is a result,

93
00:19:46,584 --> 00:19:48,852
but--and a beautiful result--

94
00:19:48,853 --> 00:19:50,420
but also it's the action,

95
00:19:50,421 --> 00:19:51,954
so that's where--
the energy is in there.

96
00:19:51,955 --> 00:19:54,357
- You've met Ushio.
- Nice to meet you again.

97
00:19:54,358 --> 00:19:56,960
Pleasure seeing you.
This is wonderful.

98
00:19:56,961 --> 00:19:59,699
And he also made this
extraordinary new sculpture.

99
00:20:01,699 --> 00:20:05,750
- It gets very good mileage.
- Yeah, I'm sure.

100
00:20:19,750 --> 00:20:21,418
So you put some foam
on the top of it, right?

101
00:20:21,419 --> 00:20:22,929
Yeah.

102
00:20:31,929 --> 00:20:33,063
Two and a half!

103
00:23:44,521 --> 00:23:46,925
Hold on, please.
She's coming soon.

104
00:23:48,925 --> 00:23:50,960
Hi.
Good morning.

105
00:23:50,961 --> 00:23:52,998
Fine, thank you.

106
00:23:53,998 --> 00:23:56,688
Mm-hmm.
Okay, bye.

107
00:25:42,707 --> 00:25:44,341
How are you?

108
00:25:44,342 --> 00:25:47,210
Nice to see you.
Nice to see you.

109
00:25:47,211 --> 00:25:49,788
Congratulations on your show.

110
00:25:57,788 --> 00:26:01,924
Okay, and we'd also like
to talk also about

111
00:26:01,925 --> 00:26:03,993
the one we're gonna buy
for the Guggenheim Museum,

112
00:26:03,994 --> 00:26:05,694
which one it will be.

113
00:26:05,695 --> 00:26:08,163
Also, there's a piece
that he created.

114
00:26:08,164 --> 00:26:12,035
It's larger,
and in fact, this is the detail.

115
00:26:12,036 --> 00:26:14,396
I'm interested in one that
has, like, a real historical...

116
00:26:15,006 --> 00:26:16,972
No, no, no, no.
Yeah, exactly.

117
00:26:16,973 --> 00:26:18,941
Yeah, exactly.

118
00:26:18,942 --> 00:26:21,745
And they're interested--
we're just trying to get--

119
00:26:21,746 --> 00:26:23,412
You know, he needs support,

120
00:26:23,413 --> 00:26:25,815
and he needs to be, you know--
and he's getting it now,

121
00:26:25,816 --> 00:26:28,183
and so you're also very key,

122
00:26:28,184 --> 00:26:29,752
you know,
which is really wonderful.

123
00:26:29,753 --> 00:26:31,994
That's great.
Fantastic.

124
00:26:37,994 --> 00:26:38,894
That's good.

125
00:27:28,244 --> 00:27:29,764
- Can I get a picture with him?
- Okay.

126
00:27:35,286 --> 00:27:37,289
Fantastic. Fantastic.

127
00:27:39,289 --> 00:27:40,723
Fantastic.

128
00:27:40,724 --> 00:27:42,357
That is fabulous!

129
00:27:42,358 --> 00:27:45,194
It looks like a poppy field.

130
00:27:45,195 --> 00:27:47,463
Poppy field? Oh, yes!
Afghan poppy field.

131
00:27:47,464 --> 00:27:49,836
Looks like
an Afghan poppy field.

132
00:27:51,836 --> 00:27:53,769
<i>Yeah,Poppy Field.
It's definitely a poppy field.</i>

133
00:27:53,770 --> 00:27:55,238
Yeah.

134
00:27:55,239 --> 00:27:56,806
It's so important
he's not thinking.

135
00:27:56,807 --> 00:27:58,009
He's doing.
It's so visceral.

136
00:27:59,009 --> 00:28:00,343
It's so different from Pollock.

137
00:28:00,344 --> 00:28:02,245
Pollock was actually
consciously...

138
00:28:02,246 --> 00:28:04,780
It's very, very powerful.

139
00:28:04,781 --> 00:28:07,951
You know, I want to work
to try and find funders

140
00:28:07,952 --> 00:28:11,488
who can help us
acquire the work.

141
00:28:11,489 --> 00:28:15,132
We want one that has
some historical resonance.

142
00:28:21,132 --> 00:28:22,766
But what other ones exist?

143
00:28:22,767 --> 00:28:24,834
The Kamakura one
was photographed in the...

144
00:28:24,835 --> 00:28:25,735
<i>Art in America.</i>

145
00:28:26,002 --> 00:28:27,836
<i>It showed in Art in America.</i>

146
00:28:27,837 --> 00:28:29,439
Okay, so that's the kind
of thing that they like.

147
00:28:29,440 --> 00:28:30,874
That's the kind of thing
I need.

148
00:28:30,875 --> 00:28:32,508
Can you show me a picture
of that?

149
00:28:32,509 --> 00:28:35,545
- We have a copy of...
- We'll give it to you, yeah.

150
00:28:35,546 --> 00:28:37,791
Yeah, yeah,
let's go take a look.

151
00:28:47,791 --> 00:28:50,000
Noriko, I like this one.
I'd prefer this one.

152
00:28:57,000 --> 00:28:57,900
Oh.

153
00:29:01,806 --> 00:29:02,706
Oh, okay.

154
00:29:06,076 --> 00:29:07,876
But there--
maybe we can work it out.

155
00:29:07,877 --> 00:29:09,845
We have to be sure
that Ushio and Noriko benefit.

156
00:29:09,846 --> 00:29:10,886
Exactly.
No, no, that's--

157
00:29:16,019 --> 00:29:17,452
- But we can work this out.

158
00:29:17,453 --> 00:29:20,856
I mean, it's all--
we can negotiate this.

159
00:29:20,857 --> 00:29:22,325
<i>No, no, no,
arigato, arigato.</i>

160
00:29:22,326 --> 00:29:25,995
No, I appreciate that.
No, I appreciate that.

161
00:29:25,996 --> 00:29:27,529
Okay, so I need to do something.

162
00:29:27,530 --> 00:29:28,832
It's gonna take some time.

163
00:29:28,833 --> 00:29:30,232
I mean, I'm sorry.

164
00:29:30,233 --> 00:29:31,835
You've already been
so patient with me.

165
00:29:31,836 --> 00:29:34,437
It's just, you know,
we're just...

166
00:29:34,438 --> 00:29:36,843
I'm doing a start-up.

167
00:29:38,843 --> 00:29:41,011
Ready?
One. Two. Three.

168
00:30:55,952 --> 00:30:57,452
Yahoo!

169
00:33:53,297 --> 00:33:54,657
Uh, I'm okay.

170
00:34:25,628 --> 00:34:26,666
Hey.

171
00:34:50,887 --> 00:34:52,905
Can I have a little bit?

172
00:45:33,496 --> 00:45:34,996
- Hi.
- Hi.

173
00:46:07,996 --> 00:46:10,131
I like this very much.

174
00:46:10,132 --> 00:46:14,172
Even from afar I could see
something stronger here.

175
00:46:16,172 --> 00:46:18,056
Something strong is here.

176
00:46:34,056 --> 00:46:35,608
Very nice.

177
00:47:45,561 --> 00:47:46,545
Oh, thank you.

178
00:49:17,686 --> 00:49:19,050
Ooh, good smell.

179
00:52:27,210 --> 00:52:28,614
Huh.

180
00:55:19,949 --> 00:55:22,054
Hello?
How are you?

181
00:55:25,054 --> 00:55:30,924
I bought 15 pounds
of white pigment color.

182
00:55:30,925 --> 00:55:33,828
- 15 pounds?
- Yeah. It was--

183
00:55:33,829 --> 00:55:35,696
- Why?
- Why?

184
00:55:35,697 --> 00:55:37,000
I'm gonna have an exhibition,

185
00:55:38,000 --> 00:55:40,840
and now I'm painting
at the gallery.

186
00:55:45,840 --> 00:55:49,511
I'm using a big brush
and a small brush

187
00:55:49,512 --> 00:55:52,568
and big Cuties.

188
00:57:52,568 --> 00:57:54,045
Jealousy, jealousy.
Big jealousy.

189
01:01:40,496 --> 01:01:41,396
Mm.

190
01:07:27,608 --> 01:07:28,664
Celery, okay.

191
01:12:54,601 --> 01:12:57,069
both: Quick, quick, slow.

192
01:12:57,070 --> 01:12:59,872
Quick, quick, slow.

