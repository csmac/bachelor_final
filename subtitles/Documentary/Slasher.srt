1
00:00:00,088 --> 00:00:01,501
Previously on Slasher.

2
00:00:02,203 --> 00:00:04,936
Seven sins...six killings...

3
00:00:04,943 --> 00:00:07,242
Only one victims remains...

4
00:00:07,313 --> 00:00:09,746
Who is that unlucky soul?

5
00:00:09,982 --> 00:00:11,048
Don't! No!

6
00:00:11,186 --> 00:00:12,252
Please! No!

7
00:00:12,356 --> 00:00:13,455
Help me!

8
00:00:15,126 --> 00:00:16,158
Sarah.

9
00:00:16,228 --> 00:00:17,260
Welcome home.

10
00:00:17,296 --> 00:00:18,804
You glow.

11
00:00:19,004 --> 00:00:20,303
He thinks this is over.

12
00:00:20,407 --> 00:00:21,807
It's not even close to being over.

13
00:00:21,812 --> 00:00:23,279
This town has been cleansed.

14
00:00:23,981 --> 00:00:25,581
"And the Lord said in His heart,

15
00:00:25,583 --> 00:00:27,850
'Never again will I curse
the ground for man's sake,

16
00:00:28,619 --> 00:00:31,654
nor will I again destroy every
living thing as I have done'."

17
00:01:12,507 --> 00:01:13,706
Please!

18
00:01:15,710 --> 00:01:16,742
My God, Cam.

19
00:01:16,878 --> 00:01:18,211
What is wrong with you?

20
00:01:20,115 --> 00:01:21,180
Wake up, Cam.

21
00:01:21,583 --> 00:01:22,515
Wake up!

22
00:01:23,718 --> 00:01:24,584
- Suzy, would you...
- Shit, Alan!

23
00:01:24,719 --> 00:01:26,252
Calm down.

24
00:01:27,455 --> 00:01:28,621
Where am I?

25
00:01:28,756 --> 00:01:31,157
- You're sleepwalking.
- And you wanna know why?

26
00:01:31,160 --> 00:01:34,327
Because you were playing goddamn
videogames right before bed again.

27
00:01:34,462 --> 00:01:35,761
And I told you not to.

28
00:01:37,565 --> 00:01:39,966
If you would just frickin'
listen to me, we wouldn't

29
00:01:39,967 --> 00:01:42,368
have to go through this
every frickin' night.

30
00:01:43,271 --> 00:01:46,239
Oh, come on. You pissed yourself?

31
00:01:46,674 --> 00:01:49,742
You are too old for this.
What is wrong with you?

32
00:01:49,878 --> 00:01:51,410
I'm sorry, Mom. I didn't mean to.

33
00:01:51,546 --> 00:01:53,346
Well, you did, Cam. You did.

34
00:01:53,481 --> 00:01:55,648
And now I'm gonna have
to change all your sheets

35
00:01:55,783 --> 00:01:57,416
and wash them, and your pj's, too.

36
00:01:57,552 --> 00:02:00,753
And I'm gonna be lucky if I
get two hours of goddamn sleep

37
00:02:00,755 --> 00:02:01,787
because you are such a little baby!

38
00:02:03,574 --> 00:02:05,157
I oughta, uh, make you wear diapers.

39
00:02:05,460 --> 00:02:07,027
Would you like that?

40
00:02:07,028 --> 00:02:08,595
How would you like that?
Wearing diapers to bed.

41
00:02:09,998 --> 00:02:11,130
That's what I oughta do.

42
00:02:11,266 --> 00:02:12,498
I'm gonna make you wear diapers to bed.

43
00:02:12,634 --> 00:02:14,868
And to school, in front
of all your friends.

44
00:02:15,003 --> 00:02:16,453
To teach you a lesson

45
00:02:16,454 --> 00:02:17,904
because you are such a little baby!

46
00:02:22,477 --> 00:02:23,576
What the hell?

47
00:02:24,145 --> 00:02:25,478
Cam?

48
00:02:27,148 --> 00:02:28,348
Suzanne!

49
00:02:30,485 --> 00:02:31,885
Suzanne!

50
00:02:36,958 --> 00:02:38,391
What happened?

51
00:02:38,627 --> 00:02:40,360
Jesus, no.

52
00:02:40,495 --> 00:02:41,427
Suzanne.

53
00:02:42,430 --> 00:02:44,397
No! What have you done?

54
00:02:44,532 --> 00:02:45,365
No!

55
00:02:45,533 --> 00:02:46,900
Cameron!

56
00:02:54,948 --> 00:02:56,447
Oh, come on. It's smiling.

57
00:02:57,250 --> 00:02:58,416
It's wrong.

58
00:02:58,551 --> 00:03:00,184
No, it's not.

59
00:03:00,353 --> 00:03:02,020
- It...
- Look at it.

60
00:03:02,956 --> 00:03:05,390
Yeah, okay. Yeah, it's a smile.
- Mm-hm. Mm-hm.

61
00:03:05,998 --> 00:03:07,965
I bet you never thought
this was possible.

62
00:03:09,535 --> 00:03:10,634
Havin' fun...

63
00:03:11,304 --> 00:03:12,670
in Waterbury?

64
00:03:13,005 --> 00:03:14,605
Yeah, I guess I didn't.

65
00:03:20,279 --> 00:03:21,545
Sarah.

66
00:03:23,182 --> 00:03:25,482
I know we both lost people we love,

67
00:03:25,484 --> 00:03:26,550
this summer.

68
00:03:29,223 --> 00:03:30,156
But...

69
00:03:30,626 --> 00:03:32,626
Waterbury...

70
00:03:33,362 --> 00:03:35,028
It had a problem.

71
00:03:35,564 --> 00:03:37,564
Lots of problems,

72
00:03:37,566 --> 00:03:39,012
lots of secrets...

73
00:03:39,212 --> 00:03:42,480
There's a lot of darkness in this
town and it's not just The Executioner

74
00:03:55,065 --> 00:03:58,133
Will you two be helping me hand
out candy tomorrow or...?

75
00:04:00,037 --> 00:04:01,503
I don't know how many
trick or treaters we'll get.

76
00:04:01,505 --> 00:04:03,505
It's, uh... it's Sarah's birthday,

77
00:04:04,107 --> 00:04:05,140
tomorrow.

78
00:04:05,275 --> 00:04:06,374
Oh, right. Right.

79
00:04:07,444 --> 00:04:08,743
Happy Birthday.

80
00:04:10,047 --> 00:04:11,179
Thank you.

81
00:04:14,418 --> 00:04:17,485
All right, so, you know,
I should... go back to work.

82
00:04:18,121 --> 00:04:19,487
Remember, scary, okay?

83
00:04:19,623 --> 00:04:20,555
Chase those demons away.

84
00:04:24,795 --> 00:04:26,761
She seems in fine spirits.

85
00:04:28,532 --> 00:04:29,464
Yeah.

86
00:04:30,700 --> 00:04:32,133
She is.

87
00:04:40,732 --> 00:04:42,799
We'll take anything you've painted

88
00:04:43,135 --> 00:04:44,401
Money's no object.

89
00:04:44,736 --> 00:04:46,670
Really? Since when?

90
00:04:46,672 --> 00:04:49,406
Elise, I make six figures
drinking blood on the net.

91
00:04:49,541 --> 00:04:51,608
Yeah, I know you do. I wish you didn't.

92
00:04:51,743 --> 00:04:53,643
- Why? It's not cheating.
- Maybe not to you, it isn't.

93
00:04:53,779 --> 00:04:55,579
You know what, I'm just
gonna leave you, um...

94
00:05:00,652 --> 00:05:02,519
I haven't seen this
much black nail polish

95
00:05:02,654 --> 00:05:04,121
since I worked the make
up count in the '90s.

96
00:05:04,256 --> 00:05:05,522
I know. Can you believe this?

97
00:05:05,724 --> 00:05:08,191
And what is this, exactly?

98
00:05:08,560 --> 00:05:10,159
Happy Halloween?

99
00:05:10,796 --> 00:05:12,229
Speaking of...

100
00:05:13,132 --> 00:05:15,532
I wasn't gonna throw
one this year, but...

101
00:05:15,734 --> 00:05:17,334
- You're doing it for Justin?
- Yeah.

102
00:05:17,712 --> 00:05:20,646
It felt sort of wrong...
not celebrating it.

103
00:05:20,681 --> 00:05:21,248
You know...

104
00:05:24,224 --> 00:05:25,658
I... should make like Dracula and fly.

105
00:05:25,793 --> 00:05:28,127
But I will see you at The
A-Frame tomorrow night.

106
00:05:28,262 --> 00:05:30,062
- Hmm.
- Yes. Sarah?

107
00:05:30,197 --> 00:05:31,363
No, you know, I just want to stay in bed

108
00:05:31,365 --> 00:05:32,665
and eat my weight in candy.

109
00:05:33,003 --> 00:05:34,736
I'm not giving up on you.

110
00:05:34,738 --> 00:05:36,705
You will be doing the Monster
Ma with me tomorrow night.

111
00:05:36,707 --> 00:05:38,340
Yeah, you know what? Just dream big.

112
00:05:53,857 --> 00:05:56,258
Looking in fine form, son.

113
00:06:00,297 --> 00:06:02,197
Whattaya want?

114
00:06:05,836 --> 00:06:07,369
Talk for a minute?

115
00:06:10,641 --> 00:06:11,941
I can't even imagine...

116
00:06:12,476 --> 00:06:14,810
how upset you still must be.

117
00:06:16,814 --> 00:06:18,480
About June?

118
00:06:19,283 --> 00:06:21,450
That was three months ago, Dad.

119
00:06:21,586 --> 00:06:24,653
It took me years to get over
the death of your mother, Cameron.

120
00:06:25,226 --> 00:06:27,659
Mourning won't bring June back.

121
00:06:27,895 --> 00:06:29,528
Didn't bring back Mom.

122
00:06:30,798 --> 00:06:31,863
That's true.

123
00:06:31,999 --> 00:06:33,432
But...

124
00:06:36,237 --> 00:06:37,569
What?

125
00:06:39,807 --> 00:06:41,540
You and Sarah...

126
00:06:42,776 --> 00:06:45,711
I'm worried you might be
rushing into something...

127
00:06:47,448 --> 00:06:48,981
unseemly.

128
00:07:12,873 --> 00:07:13,939
Really?

129
00:07:15,809 --> 00:07:17,509
Tell me, Daddy.

130
00:07:18,045 --> 00:07:20,779
Are you this righteous when
you' down on your hands and knees

131
00:07:20,914 --> 00:07:22,948
being whipped and
flogged with a dog collar

132
00:07:22,950 --> 00:07:26,384
around that perverted little neck of yours?
...hm?

133
00:07:44,839 --> 00:07:46,606
Hey...

134
00:07:46,741 --> 00:07:47,774
You know, I'm...

135
00:07:47,909 --> 00:07:49,842
I'm headed downtown. I could...

136
00:07:49,844 --> 00:07:52,312
I could give you a ride
to the church, if you want.

137
00:07:52,914 --> 00:07:55,381
No, I'm fine. You go.

138
00:07:55,583 --> 00:07:56,849
All right.

139
00:07:57,452 --> 00:07:58,818
Suit yourself.

140
00:09:11,008 --> 00:09:13,876
I mean...I'm sorry, there's none
of my own work left.

141
00:09:14,545 --> 00:09:16,412
Let me guess, the
"vamps" got here first.

142
00:09:17,481 --> 00:09:18,981
Um, I do have a website,

143
00:09:18,983 --> 00:09:20,949
then when more become available,
you can buy them online.

144
00:09:24,624 --> 00:09:25,890
Could you... just
excuse me for a second?

145
00:09:29,028 --> 00:09:30,060
Hey.

146
00:09:31,497 --> 00:09:32,696
What are you doing here?

147
00:09:33,833 --> 00:09:35,699
What am I...?

148
00:09:35,701 --> 00:09:38,068
I've been calling for days.
You're not answering your phone.

149
00:09:38,070 --> 00:09:39,503
Because I don't want to talk to you.

150
00:09:39,505 --> 00:09:40,171
- It's your birthday, babe.
- Yeah.

151
00:09:41,941 --> 00:09:43,674
Best present you can give
me is just to leave me alone.

152
00:09:43,874 --> 00:09:44,483
Listen, babe.

153
00:09:45,820 --> 00:09:47,753
You wanted me... to remove
myself from the story.

154
00:09:47,888 --> 00:09:49,488
I did that.

155
00:09:49,490 --> 00:09:50,823
You wanted me to cut
ties with Lisa-Ann?

156
00:09:50,825 --> 00:09:51,590
I did that, too.

157
00:09:53,194 --> 00:09:55,761
All I am now is the editor
of a simple small town paper,

158
00:09:55,896 --> 00:09:57,062
and I'm fine with that.

159
00:10:20,503 --> 00:10:21,986
Sarah, you are the single
most important thing to me

160
00:10:22,122 --> 00:10:23,522
in the world.

161
00:10:24,725 --> 00:10:27,492
And you only came to that conclusion
after everything had gone to hell?

162
00:10:28,601 --> 00:10:31,035
Now, please. I'd like you to leave.

163
00:10:31,706 --> 00:10:33,428
Sarah.

164
00:10:47,307 --> 00:10:49,173
You know, some parents...

165
00:10:49,676 --> 00:10:51,642
It takes time to love their children.

166
00:10:52,712 --> 00:10:54,712
Your mother was like that.

167
00:10:55,748 --> 00:10:57,381
But me...

168
00:10:57,750 --> 00:10:59,650
The moment I saw you,

169
00:10:59,853 --> 00:11:01,652
my whole life finally made sense.

170
00:11:04,491 --> 00:11:06,624
Cam, was I a good father?

171
00:11:08,328 --> 00:11:10,561
Yeah, of course.

172
00:11:10,730 --> 00:11:13,598
Are you sure? 'Cause I worry.

173
00:11:14,701 --> 00:11:17,635
I worry I didn't do everything
I needed to do for you.

174
00:11:18,671 --> 00:11:19,770
Dad...

175
00:11:20,206 --> 00:11:22,807
you're
- you're acting really strange.

176
00:11:24,878 --> 00:11:28,279
I hoped you'd forget about that night.

177
00:11:29,315 --> 00:11:31,682
I thought it better just...

178
00:11:32,352 --> 00:11:34,218
just to move forward.

179
00:11:35,221 --> 00:11:38,756
But now I don't think that
was the right thing to do.

180
00:11:43,796 --> 00:11:45,630
I'm not angry at you, son.

181
00:11:45,765 --> 00:11:47,231
I'm not.

182
00:11:48,601 --> 00:11:50,234
I'm no angel.

183
00:11:50,770 --> 00:11:52,403
Far from it.

184
00:11:53,373 --> 00:11:55,840
And those people and their sins...

185
00:11:57,544 --> 00:11:58,843
Who knows, a 100 years ago,

186
00:11:59,179 --> 00:12:01,412
maybe the state would
have put 'em to death.

187
00:12:03,783 --> 00:12:06,284
But you broke man's law, Cam.

188
00:12:09,822 --> 00:12:11,689
More importantly, you broke God's law.

189
00:12:11,925 --> 00:12:13,391
Hmm.

190
00:12:14,594 --> 00:12:16,627
But I can help you with that.

191
00:12:17,630 --> 00:12:18,763
We can ask God,

192
00:12:18,765 --> 00:12:20,865
to forgive you, together.

193
00:12:26,606 --> 00:12:28,606
And what happens after that?

194
00:12:35,882 --> 00:12:37,248
I'm sorry.

195
00:12:44,757 --> 00:12:49,560
"Lord of grace and truth,
we confess our unworthiness

196
00:12:49,696 --> 00:12:52,630
to stand in Your
presence as Your children.

197
00:12:55,368 --> 00:12:56,767
We have sinned.

198
00:12:58,271 --> 00:13:01,939
Forgive... and heal us. "

199
00:13:06,980 --> 00:13:08,479
Cam!

200
00:13:09,282 --> 00:13:10,948
You know I'm terrible.

201
00:13:11,584 --> 00:13:13,517
I am not!

202
00:13:14,854 --> 00:13:15,720
Goddamnit.

203
00:13:16,990 --> 00:13:18,756
Goddamnit.

204
00:13:40,880 --> 00:13:42,413
Goddamnit.

205
00:13:44,284 --> 00:13:45,916
Forgive me, God.

206
00:13:52,058 --> 00:13:54,025
No one else needed to die.

207
00:14:08,105 --> 00:14:12,274
Tennyson wrote, "'Tis
better to have loved and lost

208
00:14:12,275 --> 00:14:14,874
than to never have loved at all.

209
00:14:14,875 --> 00:14:15,955
Well, guess what?

210
00:14:15,956 --> 00:14:19,715
Lord Tennyson had no idea
what he was talking about.

211
00:14:19,717 --> 00:14:22,417
Waterbury has faced many
losses this year. But...

212
00:14:22,553 --> 00:14:25,887
For me, my own personal
loss has been my wife.

213
00:14:26,357 --> 00:14:29,791
I didn't lose her to
The Executioner, however.

214
00:14:29,927 --> 00:14:31,760
I lost her to myself.

215
00:14:31,895 --> 00:14:33,795
The greatest sin, the cardinal sin...

216
00:14:33,931 --> 00:14:34,830
"... is pride.

217
00:14:36,400 --> 00:14:37,966
It's been the downfall
of many a man and woman,

218
00:14:37,968 --> 00:14:41,670
and now it's my own personal undoing. "

219
00:14:42,106 --> 00:14:43,071
Wow.

220
00:14:47,511 --> 00:14:48,910
God, if Sarah doesn't
take him back, I will.

221
00:14:49,913 --> 00:14:51,847
Don't you have any actual work to do?

222
00:14:56,754 --> 00:14:58,487
You know, it was
romantic, and it was brave.

223
00:14:58,622 --> 00:15:00,088
I do appreciate the sentiment.

224
00:15:01,925 --> 00:15:03,091
I didn't write that...

225
00:15:03,093 --> 00:15:05,660
thinking you'd magically
change your mind, Sarah.

226
00:15:05,662 --> 00:15:07,929
I wrote it so you'd hear me out.

227
00:15:07,931 --> 00:15:09,698
You know, what, yeah,
and I want to believe you

228
00:15:09,700 --> 00:15:11,500
in the spirit that it
was written in, but...

229
00:15:11,668 --> 00:15:13,735
You know you lied to me.

230
00:15:14,004 --> 00:15:15,470
For years.

231
00:15:15,606 --> 00:15:16,905
Yeah.

232
00:15:18,409 --> 00:15:20,942
I just... I just wish
we'd never come here.

233
00:15:22,179 --> 00:15:25,547
You know what? I'd still
be a li even if we had.

234
00:15:28,786 --> 00:15:30,152
I'm glad
- I'm glad we came here.

235
00:15:30,487 --> 00:15:32,888
Because... you wanted the truth, Sarah.

236
00:15:33,023 --> 00:15:34,523
And
- and you got it.

237
00:15:34,992 --> 00:15:36,825
Maybe more truth than you bargained for.

238
00:17:08,873 --> 00:17:10,640
Or we could do a piece on what to do

239
00:17:10,775 --> 00:17:12,174
with all your Halloween
crap the day after.

240
00:17:12,310 --> 00:17:14,043
Right? Like the
Jack-o'- lantern,

241
00:17:14,178 --> 00:17:16,145
all the candy you don't
want your kids to scarf down.

242
00:17:16,917 --> 00:17:18,250
Sound fun?

243
00:17:18,251 --> 00:17:19,584
- Whoa! Whoa!
- Get down on the ground!

244
00:17:19,586 --> 00:17:21,152
Nobody move!
- Get down on the ground!

245
00:17:21,154 --> 00:17:22,443
Cam, what's...? What...?

246
00:17:22,444 --> 00:17:23,733
Dylan Bennett, you are
under arrest for murder.

247
00:17:23,734 --> 00:17:25,023
What are you talking
about? There's a mistake.

248
00:17:25,025 --> 00:17:26,090
This is a mistake!

249
00:17:26,127 --> 00:17:27,693
You have the right to remain silent...

250
00:17:27,695 --> 00:17:33,411
You can refuse to anwer any questions...
Anything you say may be used against you in a court of law

251
00:17:33,425 --> 00:17:35,140
Call Sarah. Tell her to meet me at the station.

252
00:17:37,512 --> 00:17:39,345
- Thanks.
- No problem.

253
00:17:47,255 --> 00:17:48,688
I don't understand.

254
00:17:51,493 --> 00:17:54,360
Just take it easy. Okay?

255
00:17:54,496 --> 00:17:55,829
Breathe.

256
00:17:59,014 --> 00:18:00,347
You're gonna be okay.

257
00:18:02,784 --> 00:18:04,150
It's like he wanted me to find them.

258
00:18:05,821 --> 00:18:07,320
To punish me.

259
00:18:09,958 --> 00:18:12,092
Or maybe he wanted to be caught.

260
00:18:12,828 --> 00:18:14,160
I don't understand.

261
00:18:14,162 --> 00:18:15,261
Sarah...

262
00:18:17,065 --> 00:18:20,300
There's no telling what goes on
in the mind of a... a criminal.

263
00:18:27,791 --> 00:18:29,858
We married the wrong people.

264
00:18:32,863 --> 00:18:34,195
I should have waited for you.

265
00:18:35,866 --> 00:18:37,132
And now...

266
00:18:37,901 --> 00:18:39,701
I can never tell if...

267
00:18:40,103 --> 00:18:42,003
you felt the way I did.

268
00:18:44,441 --> 00:18:46,808
- I should have been more brave.
- No.

269
00:18:48,912 --> 00:18:51,012
You are brave, Cam. You're the...

270
00:18:52,249 --> 00:18:54,182
You're the bravest man I know.

271
00:19:07,264 --> 00:19:08,797
Sarah, maybe
- maybe we should...

272
00:19:08,932 --> 00:19:09,731
No.

273
00:20:12,007 --> 00:20:13,840
I was thinkin'...

274
00:20:14,309 --> 00:20:16,276
I'll go spend a few hours with Dylan.

275
00:20:16,845 --> 00:20:19,379
See what
- see what I can find out.

276
00:20:19,982 --> 00:20:22,282
And I thought, maybe afterwards...

277
00:20:22,417 --> 00:20:24,284
we come back here, order some food?

278
00:20:25,320 --> 00:20:27,320
I actually just need to be alone

279
00:20:32,327 --> 00:20:33,460
Well...

280
00:20:34,062 --> 00:20:35,428
It's been a long day.

281
00:20:37,466 --> 00:20:40,400
Why don't you call me if
you change your mind, okay?

282
00:21:06,049 --> 00:21:07,415
I need to talk to my lawyer.

283
00:21:07,417 --> 00:21:09,250
No, you need to tell
me why we found

284
00:21:09,252 --> 00:21:12,752
a box full of goddamn murder-scene souvenirs
in your house.

285
00:21:15,258 --> 00:21:17,458
Brenda Merritt's fingernails.

286
00:21:17,927 --> 00:21:20,561
Tell me, Dylan, did you
- you pull those out with pliers?

287
00:21:20,930 --> 00:21:22,363
No! Cam, listen to me.

288
00:21:22,498 --> 00:21:24,967
I didn't do anything to anybody,
Cam. You have the wrong guy.

289
00:21:25,068 --> 00:21:26,302
- What about my wife's hair, huh?

290
00:21:26,405 --> 00:21:27,604
What about your wife's hair?

291
00:21:27,676 --> 00:21:30,275
How'd you come by that?
You sick son of a bitch!

292
00:21:30,475 --> 00:21:33,475
Okay, I need to see my lawyer...now!

293
00:21:37,511 --> 00:21:40,910
Yeah...maybe I'll do that...

294
00:21:44,850 --> 00:21:46,250
tomorrow.

295
00:21:47,119 --> 00:21:48,519
You better get used to that chair

296
00:21:48,521 --> 00:21:49,553
because you're gonna be there all night.

297
00:21:49,555 --> 00:21:50,788
Cam, listen to me.

298
00:21:50,923 --> 00:21:51,488
Cam!

299
00:21:51,824 --> 00:21:53,423
Please... Hear me out!

300
00:25:29,128 --> 00:25:31,628
911. What is your emergency?

301
00:25:33,732 --> 00:25:36,333
Hello? Are you there?

302
00:25:38,337 --> 00:25:39,636
Hello?

303
00:26:32,424 --> 00:26:33,590
Merlin! Merlin! Cut!

304
00:26:35,294 --> 00:26:37,360
Uh, sorry, everyone.

305
00:26:37,362 --> 00:26:38,728
Uh, I didn't mean to stop
the dancing. Just one second.

306
00:26:38,864 --> 00:26:40,630
I just wanted to thank you all..

307
00:26:40,766 --> 00:26:42,566
so, so much for coming.

308
00:26:42,568 --> 00:26:44,868
- I really appreciate it.
- Whoo!

309
00:26:44,870 --> 00:26:45,802
Yeah!
- Um...

310
00:26:45,804 --> 00:26:47,704
You probably didn't know
this... I didn't, but...

311
00:26:49,474 --> 00:26:51,308
Halloween, actually started
as a celebration of life.

312
00:26:51,810 --> 00:26:55,946
We would, um, wear
costumes... to scare off death.

313
00:26:57,894 --> 00:26:59,493
I, uh...

314
00:27:01,864 --> 00:27:04,498
I lost the love of my life this year.

315
00:27:06,603 --> 00:27:07,768
And I've...

316
00:27:08,571 --> 00:27:10,838
I've felt very powerless, ever since.

317
00:27:13,776 --> 00:27:15,309
But tonight...

318
00:27:15,445 --> 00:27:16,744
Tonight I am giving...

319
00:27:16,879 --> 00:27:19,847
a great big middle
finger to death because...

320
00:27:21,284 --> 00:27:22,717
Because I'm still here.

321
00:27:23,453 --> 00:27:24,885
That's right. You bet.

322
00:27:24,887 --> 00:27:26,754
We are still here.

323
00:27:30,326 --> 00:27:31,792
So, Justin...

324
00:27:33,463 --> 00:27:35,863
Wherever you are, I really hope...

325
00:27:35,865 --> 00:27:38,799
that you can see us celebrating
your favorite holiday.

326
00:27:40,937 --> 00:27:42,570
As we raise our glass to you...

327
00:27:42,905 --> 00:27:43,804
Justin.

328
00:27:44,641 --> 00:27:46,474
Happy Halloween!

329
00:27:48,311 --> 00:27:49,810
Let's party!

330
00:27:49,812 --> 00:27:50,878
Merlin!

331
00:27:55,584 --> 00:27:56,283
Robin?
- Honey, honey, honey.

332
00:27:56,286 --> 00:27:57,518
I've got some very disturbing news.

333
00:27:57,520 --> 00:27:59,787
- Listen, is Cam here?
- I... yeah.

334
00:28:02,759 --> 00:28:04,625
Would you look at that?

335
00:28:04,627 --> 00:28:06,727
Listen, I don't believe you.
Dylan can't be The Execu...

336
00:28:06,729 --> 00:28:07,728
Yeah, he's not, okay?

337
00:28:07,864 --> 00:28:08,732
But even if he is,

338
00:28:08,932 --> 00:28:10,398
I want you to know
I'm here for you, okay?

339
00:28:10,400 --> 00:28:11,166
- You're a victim, too.
- Okay.

340
00:28:11,168 --> 00:28:11,933
Yeah, thanks.

341
00:28:19,543 --> 00:28:20,441
Hey!

342
00:28:20,944 --> 00:28:22,444
What are you doing here?

343
00:28:22,445 --> 00:28:23,945
I thought you were
gonna lay low tonight?

344
00:28:23,946 --> 00:28:25,446
Well, I thought you'd
be at the police station.

345
00:28:26,416 --> 00:28:29,083
I decided to let Dylan stew for a bit.

346
00:28:29,352 --> 00:28:32,287
He's
- he's lying through his teeth.

347
00:28:32,422 --> 00:28:34,355
Denied everything.

348
00:28:35,225 --> 00:28:36,925
But I'm gonna break him.

349
00:28:39,429 --> 00:28:41,229
Do you wanna go outside?

350
00:28:44,601 --> 00:28:45,967
The party's in here.

351
00:28:46,837 --> 00:28:48,269
Exactly.

352
00:28:51,074 --> 00:28:54,375
- Is everything okay, Sarah?
- Never better.

353
00:28:55,312 --> 00:28:56,477
Come on.

354
00:29:06,089 --> 00:29:07,889
Wait. Where are you taking me?

355
00:29:08,058 --> 00:29:09,490
What's with all the questions, huh?

356
00:29:10,327 --> 00:29:12,193
Aren't you supposed to
be interrogating, Dylan?

357
00:29:21,504 --> 00:29:23,338
No, no, wait.

358
00:29:24,541 --> 00:29:26,574
Are you sure, you're
okay with this, Sarah?

359
00:29:27,644 --> 00:29:30,011
You mean, go back inside?

360
00:29:30,513 --> 00:29:31,446
No.

361
00:29:31,448 --> 00:29:33,147
Then, shut up.

362
00:30:01,519 --> 00:30:02,385
What the hell's going on?

363
00:30:02,520 --> 00:30:03,653
Robin? No, no, Robin.

364
00:30:05,957 --> 00:30:06,555
Robin!

365
00:30:09,494 --> 00:30:10,359
No!

366
00:30:11,596 --> 00:30:13,629
Hey, what's going on down there?

367
00:30:19,607 --> 00:30:20,439
Sharma!

368
00:30:20,574 --> 00:30:22,408
Let me out of here!

369
00:30:29,317 --> 00:30:30,950
Your wife called.

370
00:30:32,086 --> 00:30:33,052
Pardon the outfit.

371
00:30:33,287 --> 00:30:34,420
I assure you, I am a lawyer.

372
00:30:34,689 --> 00:30:36,622
Can you tell him to let me out of here?

373
00:30:36,624 --> 00:30:38,590
I'm sorry, I can't do anything
until I can reach Chief Henry.

374
00:30:38,592 --> 00:30:40,492
I have brought with me
a writ of habeas corpus.

375
00:30:40,628 --> 00:30:41,660
Do you have any idea what that means?

376
00:30:43,331 --> 00:30:44,229
No.

377
00:30:44,598 --> 00:30:45,664
It means...

378
00:30:47,335 --> 00:30:49,435
either you charge my client with
an actual crime or you release him.

379
00:30:49,437 --> 00:30:51,003
You can't just keep him here.

380
00:30:51,005 --> 00:30:54,440
I don't have the
authority to do anything.

381
00:30:54,575 --> 00:30:55,607
Fine.

382
00:30:55,609 --> 00:30:57,676
I'll make a decision
for you. Mr. Bennett.

383
00:30:58,379 --> 00:30:59,278
Wha...?

384
00:30:59,413 --> 00:31:00,713
- Thank you.
- Wait, um...

385
00:31:01,048 --> 00:31:02,414
Good evening.

386
00:31:03,317 --> 00:31:04,650
Shit.

387
00:31:14,028 --> 00:31:15,494
Help!

388
00:31:42,056 --> 00:31:43,555
911. What's your emergency?

389
00:31:43,557 --> 00:31:44,656
Please, you've got to help me.

390
00:31:44,658 --> 00:31:46,025
The Executioner, he's here.

391
00:31:46,160 --> 00:31:47,593
I'm sorry?

392
00:31:47,594 --> 00:31:49,027
- Help!
- Are you there?

393
00:31:49,030 --> 00:31:52,231
This is Sarah Bennett! He's
outside! He's trying to kill me!

394
00:31:52,233 --> 00:31:53,532
Are you in danger?

395
00:31:54,402 --> 00:31:55,300
Ma'am?

396
00:31:59,373 --> 00:32:00,639
No!

397
00:32:11,685 --> 00:32:12,718
Open the door, Sarah.

398
00:32:17,091 --> 00:32:18,757
Open the goddamn door!

399
00:32:19,126 --> 00:32:21,060
Get out!

400
00:32:40,147 --> 00:32:41,080
Sarah!

401
00:32:41,649 --> 00:32:42,581
Dylan?

402
00:32:42,716 --> 00:32:44,616
Go! Get help now!

403
00:32:45,152 --> 00:32:46,085
No!

404
00:32:46,220 --> 00:32:48,120
No! Don't! Don't!

405
00:32:49,690 --> 00:32:51,390
No! Don't!

406
00:32:56,297 --> 00:32:57,196
No!

407
00:33:14,748 --> 00:33:17,116
This isn't what I wanted.

408
00:33:17,318 --> 00:33:19,318
This is the exact
opposite of what I wanted!

409
00:33:23,724 --> 00:33:25,524
I
- I ruined this!

410
00:33:27,727 --> 00:33:29,393
I ruin everything!

411
00:33:30,497 --> 00:33:32,830
I ruin everything!

412
00:33:45,211 --> 00:33:47,612
I'm gonna rest here, okay?

413
00:33:57,757 --> 00:33:58,623
Hey.

414
00:33:59,692 --> 00:34:00,591
Sarah.

415
00:34:02,228 --> 00:34:03,761
Did I tell you about the
first t I saw you, Sarah?

416
00:34:06,766 --> 00:34:08,699
Camp Motega.

417
00:34:09,702 --> 00:34:11,502
Your first day there.

418
00:34:13,506 --> 00:34:15,773
And you were sitting by the lake

419
00:34:15,909 --> 00:34:18,176
running your fingers through the water.

420
00:34:20,813 --> 00:34:21,913
In the air...

421
00:34:23,516 --> 00:34:25,483
I don't know, I guess it was...

422
00:34:25,818 --> 00:34:28,686
I guess it was dandelion
seeds or something, but...

423
00:34:29,856 --> 00:34:31,589
Ah, just these...

424
00:34:32,258 --> 00:34:33,858
white flecks...

425
00:34:34,460 --> 00:34:35,693
flowing...

426
00:34:36,663 --> 00:34:38,729
glowing in the sun.

427
00:34:46,839 --> 00:34:47,939
You don't think...

428
00:34:50,777 --> 00:34:53,244
I just...

429
00:34:54,447 --> 00:34:56,247
You know, I stopped.

430
00:34:57,584 --> 00:34:59,684
And I watched you.

431
00:35:01,354 --> 00:35:03,821
You were just so perfect.

432
00:35:05,692 --> 00:35:07,258
P...?

433
00:35:07,393 --> 00:35:08,726
Perfect?

434
00:35:09,796 --> 00:35:11,295
Yeah.
- No.

435
00:35:11,798 --> 00:35:15,299
You're just so... pure.

436
00:35:16,703 --> 00:35:19,303
And... innocent, you know?

437
00:35:20,240 --> 00:35:21,572
Perfect.

438
00:35:24,811 --> 00:35:26,310
Hmm.

439
00:35:28,781 --> 00:35:30,248
Well...

440
00:35:51,904 --> 00:35:53,404
I'm sorry.

441
00:36:23,870 --> 00:36:24,769
Sarah!

442
00:36:38,951 --> 00:36:40,751
What did you do?

443
00:36:53,900 --> 00:36:55,066
Sarah, don't!

444
00:36:55,568 --> 00:36:57,802
Don't, Sarah! Don't! Don't do it.

445
00:36:58,104 --> 00:36:59,937
Why? Why?

446
00:37:00,073 --> 00:37:01,672
Oh, why?

447
00:37:01,808 --> 00:37:03,741
Don't.

448
00:37:03,743 --> 00:37:05,476
Because you're not
a murderer. No, please.

449
00:37:05,478 --> 00:37:06,377
No!

450
00:37:09,015 --> 00:37:10,815
Please, Dylan.

451
00:37:11,017 --> 00:37:12,817
Please, I need you to help me.

452
00:37:13,119 --> 00:37:15,953
- No.
- Please... help.

453
00:37:16,089 --> 00:37:17,955
Please, Dylan!

454
00:37:18,358 --> 00:37:19,857
I need you, please.

455
00:37:24,097 --> 00:37:25,663
Just... please!

456
00:37:27,367 --> 00:37:29,834
No!

457
00:37:29,836 --> 00:37:30,901
No! No!

458
00:37:31,037 --> 00:37:31,902
No!

459
00:37:35,842 --> 00:37:38,609
I don't remember meeting you, Cam.

460
00:37:39,846 --> 00:37:40,978
That's not true.

461
00:37:41,381 --> 00:37:42,079
I remember camp.

462
00:37:43,449 --> 00:37:44,081
I remember becoming friends, but...

463
00:37:44,083 --> 00:37:47,084
I don't remember the
first time I saw you!

464
00:37:47,720 --> 00:37:49,420
It was that day by the lake.

465
00:37:49,555 --> 00:37:51,989
See, that's just...
It was just a Saturday.

466
00:37:52,759 --> 00:37:54,892
I wasn't... I wasn't innocent.

467
00:37:55,027 --> 00:37:57,495
I wasn't pure. I was 15!

468
00:37:59,465 --> 00:38:03,667
I didn't know who I was. But I do now.

469
00:38:10,977 --> 00:38:12,476
God!

470
00:38:12,478 --> 00:38:13,811
I'll tell you what I'll never forget.

471
00:38:14,847 --> 00:38:17,681
I'll never forget
seeing my grandmother...

472
00:38:18,151 --> 00:38:20,151
drifting in the lake.

473
00:38:20,953 --> 00:38:23,053
The life draining out of her.

474
00:38:38,070 --> 00:38:41,038
I'll never forget Justin's last breath.

475
00:38:42,074 --> 00:38:45,476
Cam, June worshipped
the ground you walked on.

476
00:38:47,213 --> 00:38:49,146
How she trusted you...

477
00:39:01,060 --> 00:39:02,760
The desecration of Verna McBride.

478
00:39:03,029 --> 00:39:04,962
What you did to Robin.

479
00:39:06,466 --> 00:39:08,999
Your father, Cam!

480
00:39:10,002 --> 00:39:12,102
Your father!

481
00:39:16,609 --> 00:39:19,210
Police! Open up!

482
00:39:25,218 --> 00:39:27,117
No...

483
00:39:37,763 --> 00:39:39,163
Never forget.

484
00:40:03,155 --> 00:40:05,856
You'd never guess, just lookin' at it.

485
00:40:06,125 --> 00:40:08,826
But you can feel it, right? There's...

486
00:40:08,961 --> 00:40:10,194
just something wrong.

487
00:40:12,865 --> 00:40:14,598
Three-story executive with bad juju.

488
00:40:15,167 --> 00:40:17,268
How 'bout we leave that
part out of the listing?

489
00:40:18,777 --> 00:40:20,643
How are you feeling?

490
00:40:21,681 --> 00:40:24,548
I look like Frankenstein
with my shirt off, but...

491
00:40:24,983 --> 00:40:26,149
I'll live.

492
00:40:27,185 --> 00:40:29,219
Okay, no, you can't
look at me like that.

493
00:40:29,555 --> 00:40:31,288
You're gonna make me
cry, and I hate crying.

494
00:40:34,593 --> 00:40:36,026
- You're welcome.
- I'm very sorry.

495
00:40:36,161 --> 00:40:38,094
Now, you stay out of trouble, okay?

496
00:40:38,297 --> 00:40:40,230
Don't move into any cursed homes.

497
00:40:40,566 --> 00:40:42,699
And don't... befriend
anyone with the last name

498
00:40:42,834 --> 00:40:44,167
"Henry," "Dahmer," or "Bundy. "

499
00:40:44,803 --> 00:40:46,069
Deal.

500
00:40:47,339 --> 00:40:48,772
I'm gonna miss you, Robin.

501
00:40:49,046 --> 00:40:52,647
- You take care of her!
- Don't worry about that.

502
00:40:52,783 --> 00:40:54,583
Just worry about selling this house.

503
00:40:54,785 --> 00:40:56,117
All right.

504
00:41:11,350 --> 00:41:13,216
We're gonna be okay, right?

505
00:41:14,653 --> 00:41:15,952
Better than okay.

506
00:41:35,865 --> 00:41:37,197
The house has five bedrooms.

507
00:41:37,333 --> 00:41:39,133
Which, I know, sounds
large for a family of three,

508
00:41:39,268 --> 00:41:42,403
but think about it, you could
both have your own offices.

509
00:41:42,738 --> 00:41:43,804
There's bedrooms for guests.

510
00:41:43,939 --> 00:41:46,906
Plus, if you wanted to, you
could open up your own B&B.

511
00:41:46,909 --> 00:41:49,309
People do it all the time.

512
00:41:49,311 --> 00:41:51,211
After you.

513
00:41:51,347 --> 00:41:54,915
The wood throughout is original,
as is all the stained glass.

514
00:42:03,259 --> 00:42:05,392
Here, kitty, kitty.

515
00:42:11,333 --> 00:42:13,967
You're such a pretty cat.

516
00:42:20,342 --> 00:42:24,178
You've got all new appliances,
granite countertop, backsplash.

517
00:42:24,313 --> 00:42:27,381
I love, love, love this little
breakfast nook over here.

518
00:42:30,252 --> 00:42:33,353
What do you say, Scotia?
You like this house?

519
00:42:34,256 --> 00:42:37,324
Yes, this house feels perfect.

