1
00:00:22,200 --> 00:00:27,030
Good evening, Good night.

2
00:00:27,720 --> 00:00:32,910
With bautiful pink splendor

3
00:00:33,600 --> 00:00:38,670
With lilies as bedspreads

4
00:00:39,360 --> 00:00:44,950
as fresh as the spring in May

5
00:00:45,560 --> 00:00:51,190
tomorrow morning,
when God permits

6
00:00:51,960 --> 00:00:57,110
You wake up from the dream,
so much layered

7
00:00:57,880 --> 00:01:03,470
Tomorrow morning,
when God permits

8
00:01:04,360 --> 00:01:10,870
Your dreams are over
and it's quiet

9
00:01:14,080 --> 00:01:15,390
Goodnight.

10
00:02:40,320 --> 00:02:41,230
You're it!

11
00:02:41,480 --> 00:02:44,670
One two three four ...

12
00:02:45,080 --> 00:02:45,750
Five!

13
00:03:58,080 --> 00:03:59,270
Lukas?

14
00:05:18,080 --> 00:05:20,190
10, 11, 12

15
00:05:21,240 --> 00:05:23,750
13, 14

16
00:05:24,440 --> 00:05:27,150
15, 16

17
00:05:27,720 --> 00:05:30,230
17, 18

18
00:05:31,320 --> 00:05:33,700
19, 20 ...

19
00:05:34,440 --> 00:05:35,500
Lukas?

20
00:05:50,440 --> 00:05:51,660
Lukas?

21
00:06:46,440 --> 00:06:47,910
Mom?

22
00:07:09,440 --> 00:07:10,390
Mom?

23
00:07:31,440 --> 00:07:33,270
Nice welcome.

24
00:07:44,720 --> 00:07:46,350
Look at your clothes.

25
00:07:47,880 --> 00:07:51,030
Nasty. Take them off now.

26
00:07:52,400 --> 00:07:53,620
Not here!

27
00:07:54,440 --> 00:07:58,390
Undress at the washing machine.
And then take a shower.

28
00:07:59,800 --> 00:08:00,990
Hurry up!

29
00:08:44,080 --> 00:08:45,270
Please.

30
00:08:45,440 --> 00:08:46,500
Thanks.

31
00:08:54,040 --> 00:08:56,070
Lukas wants some too.

32
00:08:59,320 --> 00:09:02,860
Then he can ask me himself.

33
00:09:04,720 --> 00:09:07,310
You've only made dinner for me.

34
00:09:08,240 --> 00:09:09,460
You know why.

35
00:09:24,720 --> 00:09:26,950
You should apologize.

36
00:09:44,400 --> 00:09:46,670
- Am I a person?
- No.

37
00:09:47,440 --> 00:09:49,670
- I am a thing?
- Yes.

38
00:09:50,040 --> 00:09:52,310
- I am a mobile phone?
- No.

39
00:09:55,040 --> 00:09:57,310
- I'm pants ?.
- No.

40
00:09:57,600 --> 00:09:59,670
- Can you wear me?
- No.

41
00:10:00,000 --> 00:10:02,230
- Can you eat me?
- No.

42
00:10:04,280 --> 00:10:07,110
- Do you need me every day?
- Yes.

43
00:10:16,400 --> 00:10:18,960
- I'm hair?
- No.

44
00:10:19,280 --> 00:10:20,750
It's a thing.

45
00:10:22,040 --> 00:10:24,150
It's necessary in everyday life.

46
00:10:25,400 --> 00:10:27,670
You use it almost every day.

47
00:10:28,160 --> 00:10:29,470
Is that so?

48
00:10:31,720 --> 00:10:33,630
You don't, but ...

49
00:10:35,920 --> 00:10:38,110
- A car?
- Yeah, okay.

50
00:10:39,280 --> 00:10:41,030
You guessed it.

51
00:11:00,280 --> 00:11:03,190
- Am I an animal?
- No.

52
00:11:03,720 --> 00:11:06,020
- I am a thing?
- No.

53
00:11:06,760 --> 00:11:08,110
Am I a person?

54
00:11:08,360 --> 00:11:09,670
Yes.

55
00:11:10,600 --> 00:11:13,270
- I'm a man?
- No.

56
00:11:14,320 --> 00:11:15,910
- Am I a woman?
- Yes.

57
00:11:16,080 --> 00:11:17,870
- An adult?
- Yes.

58
00:11:19,480 --> 00:11:21,070
I am still alive?

59
00:11:21,400 --> 00:11:22,430
Yes.

60
00:11:24,920 --> 00:11:26,230
Am I on TV?

61
00:11:26,560 --> 00:11:27,670
Yes.

62
00:11:27,960 --> 00:11:28,940
I am ...

63
00:11:30,040 --> 00:11:31,020
a TV presenter?

64
00:11:31,720 --> 00:11:32,700
Yes.

65
00:11:35,720 --> 00:11:38,310
- Am I on German TV?
- No.

66
00:11:38,960 --> 00:11:40,670
Austria.

67
00:11:41,040 --> 00:11:42,670
Am I famous?

68
00:11:46,400 --> 00:11:48,150
Well, a little.

69
00:11:50,720 --> 00:11:52,070
Barbara Karlich?

70
00:11:53,400 --> 00:11:54,510
No.

71
00:12:06,360 --> 00:12:07,670
A hint?

72
00:12:09,800 --> 00:12:11,950
You love animals.

73
00:12:16,920 --> 00:12:18,750
You love animals.

74
00:12:24,160 --> 00:12:25,270
Come on!

75
00:12:26,360 --> 00:12:28,110
You love animals.

76
00:12:30,920 --> 00:12:32,390
Another hint.

77
00:12:35,480 --> 00:12:37,940
You have two children.

78
00:12:40,400 --> 00:12:44,270
How should I know who has two
children? Do I know her?

79
00:12:44,440 --> 00:12:45,230
Yes.

80
00:14:36,480 --> 00:14:37,670
Lukas!

81
00:15:18,160 --> 00:15:19,140
Enough.

82
00:15:20,960 --> 00:15:21,750
We're done.

83
00:15:22,000 --> 00:15:23,710
- Come on!
- Mom!

84
00:15:24,000 --> 00:15:25,870
I have something to say.

85
00:15:26,040 --> 00:15:27,350
We were playing!

86
00:15:27,600 --> 00:15:28,740
I don't care.

87
00:15:28,920 --> 00:15:30,390
Daddy lets us play.

88
00:15:30,680 --> 00:15:32,950
I don't care what your
father does.

89
00:15:33,920 --> 00:15:35,750
This is important.

90
00:15:36,000 --> 00:15:38,030
I want you to look at me.

91
00:15:40,680 --> 00:15:43,030
- Alright then.
- Excuse me?

92
00:15:43,200 --> 00:15:44,340
Nothing.

93
00:15:46,960 --> 00:15:48,950
The doctor told me to rest.

94
00:15:49,480 --> 00:15:52,910
And I expect support.
There are some new rules.

95
00:15:54,400 --> 00:15:56,910
I want absolute silence
in the house.

96
00:15:57,160 --> 00:16:01,030
I need sleep. If it's
important, you knock.

97
00:16:02,200 --> 00:16:03,340
And no visitors.

98
00:16:03,680 --> 00:16:06,710
If someone asks,
tell them mom's sick.

99
00:16:07,120 --> 00:16:09,630
We keep the curtains closed.

100
00:16:09,960 --> 00:16:11,630
I need to avoid sunlight.

101
00:16:11,880 --> 00:16:14,470
You play alone in the
garden, very quiet.

102
00:16:14,720 --> 00:16:16,350
You take nothing inside.

103
00:16:16,520 --> 00:16:18,900
No branches, no animals ...

104
00:16:19,160 --> 00:16:20,430
What are you doing?

105
00:16:22,080 --> 00:16:23,750
We start over.

106
00:16:24,240 --> 00:16:25,350
Well?

107
00:16:25,520 --> 00:16:28,430
We need to keep this
family together.

108
00:16:28,800 --> 00:16:31,260
One for me and one of Luke.

109
00:16:31,960 --> 00:16:33,180
Very nice.

110
00:16:35,480 --> 00:16:38,310
I meant what I just said.

111
00:16:38,600 --> 00:16:40,670
We're sticking to this plan, huh?

112
00:16:41,480 --> 00:16:42,670
Good.

113
00:16:43,280 --> 00:16:45,030
Now it's bedtime.

114
00:16:49,560 --> 00:16:50,700
Goodnight.

115
00:17:00,680 --> 00:17:03,030
- She is so different.
- Well ...

116
00:17:03,360 --> 00:17:05,190
It's because of the surgery.

117
00:17:06,040 --> 00:17:07,470
Do you think?

118
00:17:08,080 --> 00:17:09,190
Yes.

119
00:17:09,440 --> 00:17:10,830
I don't know.

120
00:17:11,000 --> 00:17:17,950
How would you feel if you had
undergone such an operation?

121
00:17:19,320 --> 00:17:20,710
I don't know.

122
00:17:21,080 --> 00:17:23,460
She is not like our mother.

123
00:17:24,440 --> 00:17:26,430
Play it again.

124
00:17:35,720 --> 00:17:38,750
Loads of kisses from me.

125
00:17:40,440 --> 00:17:42,820
I can't wait to be back.

126
00:17:43,160 --> 00:17:47,310
Pull up your blankets
and close your eyes.

127
00:17:48,520 --> 00:17:51,990
Can you count the stars,

128
00:17:52,400 --> 00:17:55,670
shining in the night sky?

129
00:17:56,440 --> 00:18:00,110
Can you count the clouds,

130
00:18:00,680 --> 00:18:03,870
hovering over the fields?

131
00:18:04,480 --> 00:18:08,180
Oh Lord watch over them

132
00:18:08,680 --> 00:18:12,460
with eyes that never doze.

133
00:18:13,000 --> 00:18:16,620
He sees you and loves you.

134
00:18:17,240 --> 00:18:21,230
He sees you and loves you.

135
00:18:22,360 --> 00:18:23,910
Goodnight.

136
00:21:39,520 --> 00:21:41,390
Get mom.

137
00:22:28,800 --> 00:22:30,110
Mom?

138
00:23:13,760 --> 00:23:15,310
Lukas?

139
00:23:39,960 --> 00:23:41,020
Lukas?

140
00:24:04,800 --> 00:24:07,310
- Hello?
- Hi, good morning.

141
00:24:08,440 --> 00:24:12,430
That was a huge order.
So many at the same time.

142
00:24:12,720 --> 00:24:15,470
- Throwing a party?
- No.

143
00:24:17,000 --> 00:24:19,030
Who are all those pizzas for?

144
00:24:19,480 --> 00:24:22,510
Pepperoni, Pepperoni?
Is that your favorite?

145
00:24:24,600 --> 00:24:27,030
Pepperoni pizza. You must be
crazy about it.

146
00:24:27,960 --> 00:24:28,940
Yes.

147
00:24:29,120 --> 00:24:31,230
Enough for a whole year.

148
00:24:31,720 --> 00:24:34,550
So, it's all in there.

149
00:29:03,400 --> 00:29:04,950
Open the door.

150
00:29:08,000 --> 00:29:09,350
What is this ...

151
00:29:11,080 --> 00:29:12,550
Since when do we lock the door?

152
00:29:12,720 --> 00:29:13,910
Sorry.

153
00:30:03,360 --> 00:30:05,950
Why is there a lighter here?

154
00:30:06,720 --> 00:30:08,670
I wanted to burn some books.

155
00:30:09,440 --> 00:30:11,900
Don't be so smart.

156
00:30:27,160 --> 00:30:28,070
Stop!

157
00:30:30,440 --> 00:30:31,390
Let him go!

158
00:35:46,960 --> 00:35:49,070
OUR MARRIAGE

159
00:37:03,000 --> 00:37:08,470
FOR SALE: STYLISH RETREAT,
PRICE ON REQUEST

160
00:39:11,680 --> 00:39:12,660
Lukas!

161
00:39:16,800 --> 00:39:18,110
Leo?

162
00:39:18,280 --> 00:39:20,350
Time for the household!

163
00:39:33,880 --> 00:39:35,190
Leo?

164
00:40:04,040 --> 00:40:07,430
I have other concerns.
I'm not playing anymore.

165
00:40:07,600 --> 00:40:11,830
I have to make him stop.
He should understand.

166
00:40:13,360 --> 00:40:14,750
Hold on, sorry.

167
00:40:46,160 --> 00:40:47,750
Here neither.

168
00:40:50,160 --> 00:40:51,270
Leo?

169
00:41:03,800 --> 00:41:05,910
Lukas, look.

170
00:41:12,240 --> 00:41:14,030
What's wrong with him?

171
00:41:16,800 --> 00:41:17,990
Leo?

172
00:41:24,120 --> 00:41:27,110
I'm sure mom did this.

173
00:43:03,320 --> 00:43:04,230
Well?

174
00:43:09,960 --> 00:43:10,710
Good then.

175
00:43:15,600 --> 00:43:16,710
Are you nuts?

176
00:43:19,040 --> 00:43:19,710
Stop.

177
00:43:21,480 --> 00:43:22,510
What is going on?

178
00:43:23,520 --> 00:43:24,470
What's going on?

179
00:43:24,800 --> 00:43:26,670
- We want our mother back.
- What?

180
00:43:27,040 --> 00:43:28,670
We want our mother back.

181
00:43:30,240 --> 00:43:31,670
Are you nuts?

182
00:43:33,400 --> 00:43:35,350
Have you lost your mind?

183
00:43:36,480 --> 00:43:38,670
Clean this up
and go to your room!

184
00:43:39,720 --> 00:43:41,190
You are not our mother.

185
00:43:46,040 --> 00:43:47,670
Go to your room!

186
00:43:48,040 --> 00:43:49,630
Show us your birthmark.

187
00:43:49,800 --> 00:43:52,100
- Enough!
- Show us your birthmark.

188
00:43:52,360 --> 00:43:53,630
I'm tired of this.

189
00:43:55,440 --> 00:43:57,070
Anything else, perhaps?

190
00:44:15,000 --> 00:44:17,670
I want you to repeat ten times
that I'm your mother.

191
00:44:19,160 --> 00:44:20,670
- Open up!
- Say it.

192
00:44:21,080 --> 00:44:22,430
- You're my mother.
- More convincing.

193
00:44:22,600 --> 00:44:24,390
You're my mother.

194
00:44:24,560 --> 00:44:25,430
Look at me.

195
00:44:25,600 --> 00:44:27,830
You're my mother,
you're my mother ...

196
00:44:28,880 --> 00:44:29,630
Harder.

197
00:44:29,800 --> 00:44:31,830
You're my mother.

198
00:44:36,440 --> 00:44:37,990
Stop this nonsense!

199
00:44:38,160 --> 00:44:40,830
I'm not playing anymore.
Understood?

200
00:44:41,000 --> 00:44:43,560
There is only one breakfast
and one set of clothes.

201
00:44:43,720 --> 00:44:47,260
And promise me you're not
talking to your brother!

202
00:44:47,440 --> 00:44:48,190
No.

203
00:44:49,200 --> 00:44:50,630
Promise me.

204
00:44:53,600 --> 00:44:56,750
Otherwise I'm very angry!
Promise me!

205
00:44:59,680 --> 00:45:01,830
Give me your cell phone. Now!

206
00:45:11,440 --> 00:45:12,910
House arrest!

207
00:45:29,520 --> 00:45:32,230
She wants to pull us apart.

208
00:46:13,960 --> 00:46:15,070
Does that hurt?

209
00:46:16,120 --> 00:46:17,670
- Does that hurt?
- No.

210
00:46:19,280 --> 00:46:20,340
Does that hurt?

211
00:48:01,000 --> 00:48:02,950
Mama, please come back.

212
00:48:05,080 --> 00:48:07,070
I'll do anything you want.

213
00:48:08,160 --> 00:48:09,910
Please, come back.

214
00:48:14,240 --> 00:48:18,350
All I want is for you
to come back to us.

215
00:48:53,480 --> 00:48:54,540
Elias?

216
00:51:32,400 --> 00:51:34,510
Cut another bit away
at your ear.

217
00:51:34,880 --> 00:51:36,020
There?

218
00:51:45,320 --> 00:51:46,510
Better?

219
00:51:49,720 --> 00:51:51,150
Now we look the same.

220
00:51:51,560 --> 00:51:54,670
Now she can't keep us apart.

221
00:52:13,520 --> 00:52:15,550
It's me. Open the door.

222
00:52:18,720 --> 00:52:20,070
Are we friends again?

223
00:52:21,280 --> 00:52:22,670
I'm not angry anymore.

224
00:52:24,480 --> 00:52:25,990
Please open the door.

225
00:52:52,960 --> 00:52:53,990
Well?

226
00:53:08,040 --> 00:53:09,470
Are we friends again?

227
00:53:25,720 --> 00:53:27,150
I got something.

228
00:53:33,000 --> 00:53:33,830
Thanks.

229
00:53:34,960 --> 00:53:35,670
Thanks.

230
00:53:36,440 --> 00:53:38,350
- Nice isn't it?
- Yes.

231
00:53:39,800 --> 00:53:43,030
Can we go out and try?

232
00:56:53,280 --> 00:56:54,230
Yes?

233
00:57:04,600 --> 00:57:05,580
Yes?

234
00:57:09,960 --> 00:57:11,310
What is it?

235
00:57:12,000 --> 00:57:14,300
- Can you help us?
- Yes.

236
00:57:19,240 --> 00:57:20,460
What is it about?

237
00:57:23,400 --> 00:57:26,430
- Are you a priest?
- No, I'm the sexton.

238
00:57:28,320 --> 00:57:29,670
Where is the priest?

239
00:57:31,000 --> 00:57:32,350
He is not here.

240
00:57:36,280 --> 00:57:38,950
- Can we call him?
- Yes.

241
00:57:41,440 --> 00:57:43,670
Of course that's possible.

242
00:58:07,520 --> 00:58:11,510
You can go with us
to the police station?

243
00:58:13,040 --> 00:58:16,950
And talk to the agent
and explain to him?

244
00:58:17,280 --> 00:58:18,990
No problem.

245
00:59:24,040 --> 00:59:25,310
Open the door!

246
00:59:33,800 --> 00:59:36,360
Get out! Come one!

247
00:59:47,480 --> 00:59:48,510
Thanks.

248
00:59:55,880 --> 00:59:58,670
Don't you owe me any explanation?

249
01:00:04,200 --> 01:00:06,350
It was all a little bit too much.

250
01:00:08,160 --> 01:00:09,990
The accident. The divorce.

251
01:03:46,560 --> 01:03:48,150
What is this?

252
01:03:53,080 --> 01:03:54,300
What is this?

253
01:03:56,560 --> 01:03:58,270
Where is our mother?

254
01:04:02,400 --> 01:04:04,550
I'm going crazy. Crazy!

255
01:04:08,320 --> 01:04:10,030
And how am I going to get up?

256
01:04:11,080 --> 01:04:12,140
Not like this.

257
01:04:16,440 --> 01:04:18,150
Where is our mother?

258
01:04:20,880 --> 01:04:23,950
Lukas said: 'Where is our mother?'

259
01:04:26,080 --> 01:04:27,470
Let me go.

260
01:04:28,480 --> 01:04:29,350
No.

261
01:04:29,760 --> 01:04:31,230
It hurts. Untie me.

262
01:04:31,400 --> 01:04:32,350
No.

263
01:04:35,080 --> 01:04:36,950
Tell us. Where is our mother?

264
01:04:37,200 --> 01:04:38,510
I'm your mother!

265
01:04:42,440 --> 01:04:43,500
No ...

266
01:04:55,160 --> 01:04:56,670
Who is that?

267
01:05:04,080 --> 01:05:05,350
Where are the scissors?

268
01:05:05,600 --> 01:05:06,550
Who is she?

269
01:05:14,520 --> 01:05:15,950
A friend of mine.

270
01:05:17,320 --> 01:05:19,070
We always wear the same.

271
01:05:23,040 --> 01:05:25,070
Who is she really?

272
01:05:30,320 --> 01:05:31,270
Answer.

273
01:05:34,440 --> 01:05:35,660
Answer it, please.

274
01:06:13,080 --> 01:06:15,110
Hello, I'm from Vienna.

275
01:06:16,080 --> 01:06:19,510
What shall I say? It's the first
time I do this.

276
01:06:21,280 --> 01:06:23,350
I like cooking,
watching movies.

277
01:06:24,000 --> 01:06:27,510
But in the evening I also
like to watch TV ..

278
01:06:28,160 --> 01:06:29,380
Look.

279
01:06:41,760 --> 01:06:42,820
Go away!

280
01:06:44,800 --> 01:06:46,110
Help me.

281
01:06:53,720 --> 01:06:54,910
Stop!

282
01:06:56,360 --> 01:06:57,420
Stop!

283
01:06:58,560 --> 01:07:01,190
- I don't see it.
- What are you doing?

284
01:07:01,360 --> 01:07:02,500
Look again.

285
01:07:03,440 --> 01:07:06,350
What is this? What !?

286
01:07:06,880 --> 01:07:10,310
- In the video your eyes are brown.
- What?

287
01:07:11,440 --> 01:07:15,140
These are contact lenses, Christ!
They're in the bathroom.

288
01:07:33,520 --> 01:07:35,470
Why are you lying to me?

289
01:07:41,280 --> 01:07:44,150
We had agreed not to believe her?

290
01:07:44,440 --> 01:07:46,670
And? I've changed my mind.

291
01:07:47,080 --> 01:07:49,380
Do you believe her, you idiot?

292
01:07:49,560 --> 01:07:51,030
You're the idiot!

293
01:08:15,680 --> 01:08:16,950
Help!

294
01:08:23,760 --> 01:08:25,710
Where are those damn scissors?

295
01:08:26,720 --> 01:08:29,390
Do you hear me? Grab the scissors!

296
01:08:40,760 --> 01:08:42,030
Lukas?

297
01:08:49,600 --> 01:08:50,660
Elias ...

298
01:08:52,720 --> 01:08:53,830
Lukas?

299
01:08:56,240 --> 01:08:58,510
Come sit next to me.

300
01:08:59,560 --> 01:09:01,510
Come, Elias. Sit here.

301
01:09:05,120 --> 01:09:07,110
Come sit next to me, Elias.

302
01:09:11,200 --> 01:09:13,830
Sit down. Then we can talk.

303
01:09:16,440 --> 01:09:17,350
Listen.

304
01:09:23,160 --> 01:09:24,270
Elias ...

305
01:09:26,200 --> 01:09:28,350
You know I'm your mother.

306
01:09:44,320 --> 01:09:45,950
I can't untie myself.

307
01:09:46,200 --> 01:09:51,710
Go to the bathroom, grab a pair
of scissors and untie me.

308
01:09:56,560 --> 01:09:59,390
Nothing bad happened.
Untie me.

309
01:10:02,280 --> 01:10:05,820
I want you to repeat 10 times:
"I don't listen to my brother."

310
01:10:06,320 --> 01:10:08,830
I don't listen to my brother.
I ...

311
01:10:09,080 --> 01:10:10,350
Louder, please.

312
01:10:10,520 --> 01:10:13,710
I don't listen to my brother.
I don't listen ...

313
01:10:13,960 --> 01:10:15,310
I said louder.

314
01:10:17,080 --> 01:10:19,150
Our mother wouldn't do that.

315
01:10:19,320 --> 01:10:20,380
More convincing.

316
01:10:21,480 --> 01:10:22,510
Elias ...

317
01:10:25,080 --> 01:10:26,350
Sorry.

318
01:10:27,040 --> 01:10:27,950
Again.

319
01:10:28,440 --> 01:10:30,190
I'm sorry, Elias.

320
01:10:41,080 --> 01:10:42,350
Come on.

321
01:10:43,360 --> 01:10:45,150
Please? Untie me.

322
01:10:53,200 --> 01:10:54,830
What happened to your nose?

323
01:10:55,760 --> 01:10:58,060
Nothing. Nothing serious.

324
01:10:59,000 --> 01:11:00,430
See?

325
01:11:04,560 --> 01:11:08,470
If you let me go
I can make you breakfast.

326
01:11:09,480 --> 01:11:11,710
Untie my feet, they hurt.

327
01:11:17,720 --> 01:11:18,700
Lukas?

328
01:11:20,280 --> 01:11:22,950
I know it's difficult,
but we're gonna make it.

329
01:11:23,600 --> 01:11:26,030
Come on, untie my feet.

330
01:11:31,160 --> 01:11:34,350
Go ahead, then I'll make breakfast.

331
01:11:39,080 --> 01:11:40,300
Untie me.

332
01:11:43,160 --> 01:11:44,270
Yes go on.

333
01:11:48,600 --> 01:11:50,030
What are you doing?

334
01:11:59,560 --> 01:12:00,390
There.

335
01:12:02,760 --> 01:12:03,670
What?

336
01:12:08,560 --> 01:12:10,510
What is it?

337
01:12:14,400 --> 01:12:15,070
Stop.

338
01:12:17,680 --> 01:12:20,060
They have been removed
in the hospital.

339
01:12:21,080 --> 01:12:22,630
They are dangerous.

340
01:12:23,560 --> 01:12:24,830
Liar!

341
01:12:26,720 --> 01:12:28,030
They had to. They did not ...

342
01:12:28,680 --> 01:12:30,950
Admit that it's a lie.

343
01:12:33,000 --> 01:12:35,190
Do not make me go, please?

344
01:12:45,720 --> 01:12:46,700
Stop!

345
01:12:47,400 --> 01:12:48,380
Stop!

346
01:13:15,120 --> 01:13:17,070
Tell us where our mother is.

347
01:13:23,280 --> 01:13:25,310
Tell us where our mother is!

348
01:15:30,080 --> 01:15:31,220
The door is open.

349
01:15:32,080 --> 01:15:34,350
Hello? The Red Cross!

350
01:15:37,120 --> 01:15:38,150
Hello?

351
01:15:39,320 --> 01:15:41,910
Red Cross.
Care to make a donation?

352
01:15:42,400 --> 01:15:43,910
Is there anybody home?

353
01:15:47,520 --> 01:15:49,030
I'll go look.

354
01:15:52,600 --> 01:15:53,710
Hello?

355
01:15:54,200 --> 01:15:55,340
Red Cross!

356
01:15:59,240 --> 01:16:00,460
There is nobody.

357
01:16:04,480 --> 01:16:07,550
It wasn't locked, there must
be someone at home.

358
01:16:13,760 --> 01:16:14,900
Hello?

359
01:16:17,880 --> 01:16:19,310
Maybe they're upstairs.

360
01:16:26,200 --> 01:16:27,630
Red Cross!

361
01:16:30,960 --> 01:16:32,630
- Hello?
- Hey!

362
01:16:33,560 --> 01:16:34,230
Hello.

363
01:16:39,720 --> 01:16:41,070
Is your mom home?

364
01:16:42,160 --> 01:16:43,630
No, she's not home.

365
01:16:43,880 --> 01:16:46,110
She's not upstairs?
We heard something.

366
01:16:46,400 --> 01:16:48,270
No, that was our dog.

367
01:16:48,680 --> 01:16:50,270
- She's not home?
- No.

368
01:16:50,600 --> 01:16:51,710
It's our dog.

369
01:16:53,760 --> 01:16:55,990
When does she get back?

370
01:17:02,120 --> 01:17:03,670
She'll be home soon.

371
01:17:05,200 --> 01:17:06,390
Can we wait?

372
01:17:07,040 --> 01:17:08,390
Where can we wait?

373
01:17:19,480 --> 01:17:23,350
Let's see how much we raised.

374
01:17:23,520 --> 01:17:26,510
- The money also?
- I can count everything.

375
01:17:27,240 --> 01:17:29,830
- What a bad handwriting.
- Yes.

376
01:17:30,080 --> 01:17:35,070
Like a doctor's prescription.
Barely legible.

377
01:17:36,760 --> 01:17:38,990
You want to go to school again?

378
01:17:39,440 --> 01:17:41,390
- Well ...
- It starts soon.

379
01:17:46,200 --> 01:17:49,630
- Where did you go to school?
- In Vienna.

380
01:17:52,080 --> 01:17:54,430
It's a lot busier there, huh?

381
01:17:59,960 --> 01:18:02,710
We have already picked up a nice sum.

382
01:18:02,880 --> 01:18:08,670
If everyone continues to give
as much, we did well.

383
01:18:11,120 --> 01:18:13,990
We'll see how much more we get.

384
01:18:14,440 --> 01:18:16,710
What do you do all day?

385
01:18:17,160 --> 01:18:18,550
Not much.

386
01:18:29,040 --> 01:18:30,950
I gotta go to the bathroom.

387
01:19:01,720 --> 01:19:03,230
Is your mother back?

388
01:19:03,480 --> 01:19:04,700
Not yet.

389
01:19:05,120 --> 01:19:08,150
Is that your own money?

390
01:19:08,440 --> 01:19:09,470
No.

391
01:19:12,880 --> 01:19:17,390
- Does your mother approve?
- Sure.

392
01:19:18,600 --> 01:19:20,670
She always gives a lot herself.

393
01:19:22,880 --> 01:19:25,150
We don't want any trouble.

394
01:19:25,320 --> 01:19:26,910
You realize ...

395
01:19:28,400 --> 01:19:32,020
Normally we don't take
money from children.

396
01:19:32,200 --> 01:19:34,070
That's a lot of money.

397
01:19:34,240 --> 01:19:36,070
No, we can do it.

398
01:19:36,960 --> 01:19:39,420
Well, take a good look at how
beautiful this house is.

399
01:19:39,680 --> 01:19:42,190
They can afford it.

400
01:19:48,280 --> 01:19:52,630
As thanks for the donation,
I have here a plaster.

401
01:19:58,120 --> 01:19:59,230
Help!

402
01:20:00,760 --> 01:20:03,710
Help!

403
01:20:03,960 --> 01:20:06,910
Help!

404
01:20:29,200 --> 01:20:31,110
Stop!

405
01:23:37,120 --> 01:23:40,230
Please prove that
you're our mother.

406
01:23:50,720 --> 01:23:53,670
Ask her what my favorite song is.

407
01:23:54,880 --> 01:23:57,110
What is Lukas' favorite song?

408
01:24:07,240 --> 01:24:09,190
"Good evening, good night"?

409
01:24:42,400 --> 01:24:43,460
Don't!

410
01:24:59,960 --> 01:25:04,390
Can you count the stars,

411
01:25:04,880 --> 01:25:08,660
shining in the night sky?

412
01:25:09,720 --> 01:25:13,830
Can you count the clouds,

413
01:25:14,440 --> 01:25:18,190
hovering over the fields?

414
01:25:19,080 --> 01:25:23,270
Oh Lord watch over them.

415
01:25:23,760 --> 01:25:27,670
with eyes that never doze.

416
01:25:28,200 --> 01:25:31,710
He sees you and loves you.

417
01:25:32,320 --> 01:25:35,550
He sees you and loves you.

418
01:25:57,440 --> 01:25:59,430
Where is our mother?

419
01:27:03,120 --> 01:27:05,230
Don't you know yourself
when you have to urinate?

420
01:27:07,160 --> 01:27:09,150
We can't help you with that.

421
01:27:10,880 --> 01:27:11,940
Hello?

422
01:27:16,720 --> 01:27:17,990
Untie me.

423
01:28:16,600 --> 01:28:18,310
Now clean the bed.

424
01:28:18,760 --> 01:28:20,310
Take the sheets off.

425
01:28:23,080 --> 01:28:25,230
Come on, get them off.

426
01:30:31,720 --> 01:30:32,700
Elias?

427
01:30:33,800 --> 01:30:35,430
Where is our mother?

428
01:30:37,360 --> 01:30:38,750
Elias, come here.

429
01:30:40,320 --> 01:30:41,950
Where is she?

430
01:30:48,800 --> 01:30:49,630
All right.

431
01:30:51,680 --> 01:30:53,980
Elias, let's make a deal.

432
01:30:55,400 --> 01:30:57,070
I'll play the game again.

433
01:30:58,680 --> 01:31:00,710
I will talk again to Lukas.

434
01:31:02,160 --> 01:31:03,950
Lukas will live again.

435
01:31:09,320 --> 01:31:11,430
I'll make breakfast
for the both of you,

436
01:31:11,760 --> 01:31:15,230
ready your clothes.
We do everything again, as before.

437
01:31:15,400 --> 01:31:19,830
But you have to believe me
that I am your mother.

438
01:31:23,240 --> 01:31:25,390
And you honestly
want to do that?

439
01:31:30,760 --> 01:31:32,070
I promise.

440
01:31:34,080 --> 01:31:35,110
Elias ...

441
01:31:37,120 --> 01:31:39,990
It's not your fault that Lukas died.

442
01:31:42,080 --> 01:31:44,750
The accident was not your fault.

443
01:31:46,720 --> 01:31:48,830
Do you really believe her?

444
01:31:57,680 --> 01:31:58,660
She's lying.

445
01:32:05,080 --> 01:32:07,710
Let her prove that
she is our mother.

446
01:32:14,360 --> 01:32:15,830
What do I do now?

447
01:32:17,120 --> 01:32:18,950
What is Lukas now doing?

448
01:32:24,160 --> 01:32:26,190
But I can't see him!

449
01:32:26,600 --> 01:32:28,390
What's he doing now?

450
01:32:34,440 --> 01:32:35,710
Elias ...

451
01:32:39,360 --> 01:32:41,390
Mama would know.

452
01:32:42,880 --> 01:32:44,750
Mama could see him.

453
01:32:47,040 --> 01:32:49,190
She would know what he's doing.

454
01:32:59,320 --> 01:33:00,710
No, Elias. Don't!

455
01:33:02,920 --> 01:33:04,270
Elias, don't!

456
01:33:04,880 --> 01:33:06,670
Elias, do you hear me?

457
01:33:20,280 --> 01:33:22,310
Help!

458
01:35:24,160 --> 01:35:27,750
Oh Lord, watch over them.

459
01:35:28,320 --> 01:35:32,150
with eyes that never doze.

460
01:35:32,600 --> 01:35:36,350
He sees you and loves you.

461
01:35:36,800 --> 01:35:41,230
He sees you and loves you.

