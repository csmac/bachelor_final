1
00:00:05,967 --> 00:00:12,566
Only one creature has carved a life
for itself in every habitat on Earth.

2
00:00:13,807 --> 00:00:17,402
That creature is us.

3
00:00:18,407 --> 00:00:24,323
All over the world we still use our
ingenuity to survive in the wild places,

4
00:00:24,367 --> 00:00:29,885
far from the city lights,
face to face with raw nature.

5
00:00:31,767 --> 00:00:35,123
This is the Human Planet.

6
00:00:46,727 --> 00:00:51,482
Mountains are among
the most brutal environments on Earth.

7
00:00:54,567 --> 00:00:59,880
Weather here can shift
from tropical to arctic in just hours.

8
00:01:04,087 --> 00:01:07,124
And the higher you climb,
the tougher it gets...

9
00:01:08,327 --> 00:01:11,637
...until even oxygen is stripped away.

10
00:01:16,527 --> 00:01:19,917
But millions of people
live in the mountains...

11
00:01:24,167 --> 00:01:27,921
...either seeking refuge from conflict

12
00:01:27,967 --> 00:01:31,721
or exploiting resources
found nowhere else.

13
00:01:35,247 --> 00:01:37,807
And to survive they have had to adapt

14
00:01:37,847 --> 00:01:41,317
in the most surprising and ingenious ways.

15
00:01:43,767 --> 00:01:46,839
These are their stories.

16
00:01:46,887 --> 00:01:48,605
(DRAMATIC MUSIC)

17
00:01:54,167 --> 00:01:55,885
(MEWING WHOOP)

18
00:02:00,247 --> 00:02:01,475
(GELADA BARKS)

19
00:02:05,367 --> 00:02:06,595
(SQUEALS)

20
00:02:13,007 --> 00:02:17,398
(WIND HOWLS AND GUSTS)

21
00:02:26,327 --> 00:02:32,163
The Altai mountains in Mongolia
are among the most remote on Earth.

22
00:02:35,407 --> 00:02:39,400
And for the people
who live in this barren landscape,

23
00:02:39,447 --> 00:02:43,804
hunting is nearly impossible.

24
00:02:43,847 --> 00:02:46,919
Unless you have help.

25
00:02:51,767 --> 00:02:56,158
Sailau Jadik and his son Berik
are Kazakhs.

26
00:02:56,207 --> 00:03:01,076
And today they're in search
of the ultimate hunting partner.

27
00:03:02,447 --> 00:03:04,836
A golden eagle.

28
00:03:06,847 --> 00:03:10,203
(TRANSLATED FROM KAZAKH)

29
00:03:20,167 --> 00:03:24,126
These young birds
are almost ready to leave the nest.

30
00:03:27,807 --> 00:03:32,642
If 16-year-old Berik can collect one,
he will take his first step

31
00:03:32,687 --> 00:03:36,202
towards becoming a Kazakh hunter
like his father.

32
00:03:36,247 --> 00:03:39,205
(CHICKS CHEEP)

33
00:03:39,247 --> 00:03:41,283
It's a big first step.

34
00:03:58,767 --> 00:04:01,918
(EAGLE CHICKS CHEEP)

35
00:04:17,087 --> 00:04:22,161
Ever since the Kazakhs fled
into these mountains nearly 200 years ago,

36
00:04:22,207 --> 00:04:24,926
they have been stealing baby eagles.

37
00:04:32,047 --> 00:04:36,199
Eagles have eyes many times
more powerful than a human's

38
00:04:36,247 --> 00:04:39,080
and can spot prey
two kilometres away.

39
00:04:41,047 --> 00:04:44,084
If Berik can teach this eagle
to hunt for him,

40
00:04:44,127 --> 00:04:49,599
he will join the few hundred Kazakhs left
who can still do this.

41
00:05:03,127 --> 00:05:07,678
Berik calls his new eagle Balapan.

42
00:05:09,527 --> 00:05:14,078
If he gets it right,
Balapan will become his hunting partner.

43
00:05:17,007 --> 00:05:20,841
But training her will take five months.

44
00:05:34,567 --> 00:05:39,561
It's five months later, and time
for Berik and a fully grown Balapan

45
00:05:39,607 --> 00:05:42,246
to go on their first hunt together.

46
00:05:45,207 --> 00:05:48,279
They're after a Mongolian fox,

47
00:05:48,327 --> 00:05:54,800
an animal so elusive, only an eagle
stands a chance of catching it.

48
00:06:00,007 --> 00:06:05,764
But for Balapan to catch the fox, Berik
first has to take her to high ground.

49
00:06:09,127 --> 00:06:14,360
Here she'll have the perfect vantage point
to spot the slightest movement.

50
00:06:41,527 --> 00:06:43,040
(BALAPAN CALLS)

51
00:06:43,087 --> 00:06:48,115
Berik now hopes that Balapan's
hunting instinct will take over.

52
00:06:55,487 --> 00:06:57,478
(SAILAU SHOUTS)

53
00:06:57,527 --> 00:06:59,245
(WHOOPING)

54
00:07:12,007 --> 00:07:15,682
(MEWING WHOOP)

55
00:07:27,567 --> 00:07:28,966
(FOX GROWLS)

56
00:07:37,327 --> 00:07:39,887
Balapan has failed.

57
00:07:42,647 --> 00:07:45,036
For Berik, this is worrying.

58
00:07:45,087 --> 00:07:47,920
Does she have the killer instinct?

59
00:08:00,927 --> 00:08:05,364
Traditionally, Kazakh hunters
pair up with their eagles for seven years

60
00:08:05,407 --> 00:08:08,080
before setting them free,

61
00:08:08,127 --> 00:08:10,925
but Berik now has his doubts.

62
00:08:10,967 --> 00:08:12,002
(BALAPAN CALLS)

63
00:08:38,687 --> 00:08:43,044
As day breaks,
father and son return to the mountains.

64
00:08:46,727 --> 00:08:49,195
If Balapan can't catch a fox,

65
00:08:49,247 --> 00:08:54,321
Berik may have to let her go
and find another eagle to train.

66
00:09:06,807 --> 00:09:08,877
(SHOUTING)

67
00:09:25,847 --> 00:09:27,565
(MEWING WHOOP)

68
00:09:58,327 --> 00:10:02,923
Balapan has caught the fox,
just as she was trained to.

69
00:10:04,007 --> 00:10:07,079
She's now locked
in a fight to the death.

70
00:10:12,447 --> 00:10:13,596
(HORSE WHINNIES)

71
00:10:37,607 --> 00:10:40,917
Sailau kills the fox as quickly as he can.

72
00:10:45,087 --> 00:10:50,161
According to Kazakh tradition,
Balapan gets the fox's lungs.

73
00:10:53,407 --> 00:10:57,923
The fox's thick coat
will be used for winter clothing.

74
00:11:01,647 --> 00:11:07,677
Berik has proved himself
to be a successful Kazakh hunter.

75
00:11:27,007 --> 00:11:30,317
As long as they have lived
in the Altai Mountains,

76
00:11:30,367 --> 00:11:33,120
Kazakhs have relied on eagles.

77
00:11:36,287 --> 00:11:41,725
However, not all mountain people
get help from wild animals.

78
00:11:43,487 --> 00:11:46,957
On the edge of Africa's Great Rift Valley,

79
00:11:47,007 --> 00:11:52,001
geological upheaval has created
Ethiopia's Simien mountains.

80
00:11:54,287 --> 00:11:58,075
Here, giant cliffs form a natural fortress

81
00:11:58,127 --> 00:12:02,803
where for centuries people
have sought refuge from conflicts below.

82
00:12:07,367 --> 00:12:12,122
Getabit village is one of a hundred
perched in a landscape so vertical

83
00:12:12,167 --> 00:12:16,683
that the residents can only grow
their crops on tiny strips of land

84
00:12:16,727 --> 00:12:18,558
along the edges of cliffs.

85
00:12:18,607 --> 00:12:22,043
(VILLAGERS SING)

86
00:12:26,487 --> 00:12:31,083
But 700-foot precipices
are the least of their worries.

87
00:12:31,127 --> 00:12:34,517
Today their annual harvest is under way,

88
00:12:34,567 --> 00:12:39,038
and their grain is under attack
from a ravenous enemy.

89
00:12:41,007 --> 00:12:43,282
(SHRIEKING)

90
00:12:46,007 --> 00:12:51,684
These are gelada monkeys
and they love stealing the farmers'grain.

91
00:12:53,807 --> 00:13:00,326
Troops of up to 600 prowl the cliffs
surrounding Getabit village,

92
00:13:00,367 --> 00:13:04,121
led by males with fangs
larger than a lion's.

93
00:13:06,487 --> 00:13:08,478
They are cunning thieves.

94
00:13:09,487 --> 00:13:12,365
To defend their crops against the monkeys,

95
00:13:12,407 --> 00:13:18,482
the cliff farmers depend on their
children, such as 1 2-year-old Dereje.

96
00:13:18,527 --> 00:13:22,884
(IN TRANSLATION)

97
00:13:26,607 --> 00:13:28,040
(DEREJE WHOOPS)

98
00:13:44,767 --> 00:13:50,160
Because the area is next to
a national park, the gelada are protected.

99
00:14:09,247 --> 00:14:13,160
As night approaches,
the geladas stop raiding.

100
00:14:19,007 --> 00:14:23,478
But Dereje's crops are ripe for harvest,
and he knows that tomorrow

101
00:14:23,527 --> 00:14:26,724
the monkeys will attack
even more aggressively than before.

102
00:14:26,767 --> 00:14:32,444
So, with his two sisters and brother,
he camps by his fields.

103
00:14:56,167 --> 00:15:00,206
At night, temperatures
plummet below freezing.

104
00:15:01,687 --> 00:15:05,646
While the thick-furred geladas
have adapted to the cold,

105
00:15:05,687 --> 00:15:08,759
the humans
must huddle together for warmth.

106
00:15:13,687 --> 00:15:16,804
At dawn, the geladas attack.

107
00:15:16,847 --> 00:15:19,805
(GELADAS WHOOP)

108
00:15:19,847 --> 00:15:22,315
(DEREJE SHOUTS)

109
00:15:22,367 --> 00:15:28,522
The first strike comes from a few large
males, who target Dereje's haystacks.

110
00:15:28,567 --> 00:15:31,400
He drives them off,

111
00:15:31,447 --> 00:15:35,963
but the geladas are cunning -
these males were only a decoy.

112
00:15:40,967 --> 00:15:44,164
Out of sight
at the other end of Dereje's fields,

113
00:15:44,207 --> 00:15:47,756
the main army launches the real attack.

114
00:15:49,447 --> 00:15:53,360
A big troop like this
can strip a field in minutes.

115
00:16:03,087 --> 00:16:06,443
(MAN CONTINUES TO SHOUT WARNING)

116
00:16:18,367 --> 00:16:21,086
(DEREJE SHOUTS)

117
00:16:21,127 --> 00:16:22,606
If Dereje doesn't hurry,

118
00:16:22,647 --> 00:16:28,040
the food his family needs
to get through the winter will be gone.

119
00:16:33,727 --> 00:16:36,480
(GELADAS SHRIEK)

120
00:16:43,847 --> 00:16:45,917
(GELADAS SHRIEK AND BARK)

121
00:16:56,447 --> 00:17:01,077
Dereje's done it.
He's seen off the gelada.

122
00:17:03,447 --> 00:17:06,962
Finally, his crops are harvested.

123
00:17:07,007 --> 00:17:09,157
(SINGING)

124
00:17:30,207 --> 00:17:32,243
Dereje lives in the Simiens

125
00:17:32,287 --> 00:17:36,599
because his ancestors
sought refuge here centuries ago.

126
00:17:36,647 --> 00:17:41,198
But in some parts of the world,
people settle in mountains

127
00:17:41,247 --> 00:17:46,446
because this environment
has something they desperately want.

128
00:17:46,487 --> 00:17:52,881
(LOW RUMBLING EXPLOSIONS)

129
00:17:52,927 --> 00:17:57,796
Mountains are born
when continental plates collide.

130
00:17:59,447 --> 00:18:05,079
This massive upheaval often exposes
a wealth of valuable minerals.

131
00:18:06,087 --> 00:18:09,682
Nowhere more so than here in Indonesia,

132
00:18:09,727 --> 00:18:14,323
home to more active volcanic mountains
than any nation on Earth.

133
00:18:24,287 --> 00:18:26,482
Here, people risk their lives

134
00:18:26,527 --> 00:18:30,566
for a mineral vital
to several important industries.

135
00:18:32,487 --> 00:18:34,478
Sulphur.

136
00:18:36,407 --> 00:18:40,161
Hartomo and Sulaiman are sulphur miners.

137
00:18:41,647 --> 00:18:45,640
Today they're going where few others dare.

138
00:18:47,687 --> 00:18:50,997
Into the heart of an active volcano.

139
00:18:52,047 --> 00:18:54,515
(IN TRANSLATION)

140
00:19:10,687 --> 00:19:15,920
This is Ijen crater, one of the most
poisonous places on Earth.

141
00:19:18,127 --> 00:19:24,236
At its centre, a lake filled with
two-and-a-half million tonnes of acid.

142
00:19:31,807 --> 00:19:35,641
And out of the depths of the mountain
pour toxic gases

143
00:19:35,687 --> 00:19:40,681
that have claimed the lives
of 7 4 miners in the past 40 years.

144
00:19:50,967 --> 00:19:55,916
(SPEAKS IN LOCAL DIALECT)

145
00:20:16,687 --> 00:20:21,078
The hydrogen sulphide
that these men must breathe in

146
00:20:21,127 --> 00:20:23,357
is 40 times the safe level.

147
00:20:32,047 --> 00:20:35,517
Over time, it destroys their lungs.

148
00:20:38,687 --> 00:20:41,201
(COUGHING)

149
00:21:11,327 --> 00:21:13,636
Once they have enough sulphur,

150
00:21:13,687 --> 00:21:16,155
Hartomo and Sulaiman have to carry it

151
00:21:16,207 --> 00:21:19,563
200 metres straight up to the crater rim.

152
00:21:25,887 --> 00:21:29,516
Each man hefts 90 kilos,

153
00:21:29,567 --> 00:21:33,082
nearly one-and-a-half times
their own body weight.

154
00:21:37,247 --> 00:21:41,525
This work exacts a heavy price
on the miners'bodies.

155
00:22:15,687 --> 00:22:20,715
Sulaiman and Hartomo
are paid five dollars per load.

156
00:22:22,727 --> 00:22:26,686
The lives of miners have always been hard.

157
00:22:28,327 --> 00:22:31,125
But it's not just miners
who have it tough.

158
00:22:31,167 --> 00:22:35,558
For some mountain dwellers,
just finding food can be a challenge.

159
00:22:38,607 --> 00:22:44,796
In the South Pacific lies the world's
second largest island, New Guinea.

160
00:22:47,847 --> 00:22:53,080
This is one of the most biologically rich
mountain landscapes on Earth.

161
00:22:54,567 --> 00:22:58,480
With plenty of water,
and thousands of protected valleys,

162
00:22:58,527 --> 00:23:01,280
these mountains brim with life.

163
00:23:04,327 --> 00:23:09,355
Yet hunting for food, and in particular
meat, is surprisingly difficult.

164
00:23:17,887 --> 00:23:20,685
Marcus, Andrew and George are hunters

165
00:23:20,727 --> 00:23:23,446
from the Yangoru Boiken tribe.

166
00:23:23,487 --> 00:23:28,242
They and their families haven't eaten
any fresh meat for two weeks.

167
00:23:30,007 --> 00:23:32,760
But today they plan to solve the problem.

168
00:23:37,567 --> 00:23:41,401
They hope to trap giant fruit bats.

169
00:23:46,807 --> 00:23:49,275
(IN TRANSLATION)

170
00:24:02,607 --> 00:24:07,681
But to catch a giant bat
requires a giant bat trap.

171
00:24:10,207 --> 00:24:13,677
So these men are doing something radical.

172
00:24:13,727 --> 00:24:17,242
They're changing the shape
of the landscape itself.

173
00:24:23,247 --> 00:24:28,605
Along this ridge,
the men create a 70-foot wide doorway.

174
00:24:28,647 --> 00:24:34,244
A shortcut through the mountains
to the fruit trees beyond

175
00:24:34,287 --> 00:24:37,324
and a perfect place to ambush the bats.

176
00:25:04,207 --> 00:25:08,246
Like spiders,
the men spin a gigantic web.

177
00:25:11,967 --> 00:25:18,281
In the dark, this 1 30-foot high net
cannot be detected by the bats.

178
00:25:22,247 --> 00:25:25,159
Only two things now remain.

179
00:25:27,967 --> 00:25:29,923
Add the bat alert signal...

180
00:25:29,967 --> 00:25:31,958
(BATS CHIRP AND SQUEAK)

181
00:25:32,007 --> 00:25:34,237
...and pray the bats fly into their trap.

182
00:25:45,967 --> 00:25:48,925
(MEN CHATTER QUIETLY IN LOCAL DIALECT)

183
00:25:51,447 --> 00:25:54,803
Catching bats requires patience.

184
00:25:58,287 --> 00:26:01,677
In the past,
Marcus has spent weeks on the mountain

185
00:26:01,727 --> 00:26:04,241
and come home empty-handed.

186
00:26:06,567 --> 00:26:11,880
But if he does manage to catch
even a few, the effort will be worthwhile.

187
00:26:11,927 --> 00:26:13,758
(IN TRANSLATION)

188
00:26:32,767 --> 00:26:33,961
(BAT SQUEALS)

189
00:26:58,727 --> 00:27:02,800
It's a good start,
but with all the mouths to feed at home,

190
00:27:02,847 --> 00:27:05,600
they're going to need more than one bat.

191
00:27:10,887 --> 00:27:15,165
As the hours roll on,
their trap continues to catch bats.

192
00:27:21,007 --> 00:27:25,398
In the course of the night,
the men catch a total of 1 5 bats.

193
00:27:27,087 --> 00:27:30,841
They cook two now and save the rest.

194
00:27:33,647 --> 00:27:38,846
Their haul will provide their families
with enough protein for two weeks.

195
00:27:46,367 --> 00:27:49,643
Every part of the bat is edible,
down to the leather on the wings.

196
00:27:59,727 --> 00:28:01,797
For Andrew, Marcus and George,

197
00:28:01,847 --> 00:28:04,884
knowing every inch
of their mountain habitat

198
00:28:04,927 --> 00:28:07,805
enables them to feed their families.

199
00:28:13,287 --> 00:28:17,724
But in colder climes,
knowing every inch of your mountain

200
00:28:17,767 --> 00:28:21,316
can mean the difference
between life and death.

201
00:28:25,407 --> 00:28:29,923
In the Swiss Alps,
ten metres of snow can fall in a year.

202
00:28:37,167 --> 00:28:41,843
And for millions of skiers,
this is a brilliant reason to come here.

203
00:28:49,247 --> 00:28:53,240
But heavy snowfalls
can also pose a deadly threat.

204
00:28:55,887 --> 00:28:57,559
Avalanches.

205
00:29:00,687 --> 00:29:04,202
(ROARING RUMBLE)

206
00:29:13,007 --> 00:29:15,680
Travelling faster than a bullet train,

207
00:29:15,727 --> 00:29:20,039
a major avalanche
annihilates everything in its path.

208
00:29:21,887 --> 00:29:23,286
In any given winter,

209
00:29:23,327 --> 00:29:27,843
there can be 30,000 avalanches
in the Swiss Alps alone.

210
00:29:47,567 --> 00:29:51,526
Ski patrolman Martin Mathys
is an avalanche spotter.

211
00:29:51,567 --> 00:29:53,842
And today he has a big problem.

212
00:29:55,847 --> 00:30:01,080
In his hometown of Grindelwald,
there has been a massive snowfall.

213
00:30:01,127 --> 00:30:04,085
The risk of avalanche is now extreme.

214
00:30:04,127 --> 00:30:06,595
(MARTIN IN TRANSLATION)

215
00:30:37,047 --> 00:30:39,845
Towering 1,000 metres above Grindelwald

216
00:30:39,887 --> 00:30:42,526
is the notorious Black Horn ridge.

217
00:30:44,567 --> 00:30:48,799
Packed with snow,
it's a disaster waiting to happen.

218
00:30:50,007 --> 00:30:53,283
So Martin must set off
a controlled avalanche now

219
00:30:53,327 --> 00:30:55,966
before any more snow falls.

220
00:31:11,527 --> 00:31:16,726
To set off a controlled avalanche,
you need dynamite, plenty of it.

221
00:31:18,367 --> 00:31:24,397
Martin is taking 50 kilos,
enough to blow up several city blocks.

222
00:32:02,367 --> 00:32:06,918
Martin reaches the summit of
the Black Horn ridge, and sets a charge.

223
00:32:16,767 --> 00:32:19,201
(MARTIN DROWNED OUT BY HELICOPTER)

224
00:32:21,247 --> 00:32:23,238
(EXPLOSION)

225
00:32:27,967 --> 00:32:33,280
He triggers a mini avalanche,
shifting over ten tonnes of snow.

226
00:32:33,327 --> 00:32:37,525
But it's not enough.
He needs to go again.

227
00:32:52,047 --> 00:32:53,765
(EXPLOSION)

228
00:32:55,567 --> 00:32:59,003
(LOW RUMBLING)

229
00:33:09,247 --> 00:33:11,807
This time, Martin succeeds.

230
00:33:11,847 --> 00:33:16,284
This is the avalanche he needs
to make the mountain safe.

231
00:33:34,047 --> 00:33:37,164
High in the Alps,
mountain people have learnt

232
00:33:37,207 --> 00:33:41,405
to control the threat of avalanches
with modern technology.

233
00:33:42,487 --> 00:33:47,800
But there are mountains where
the forces of nature cannot be tamed.

234
00:33:53,527 --> 00:33:57,156
The Himalaya
is the highest mountain range on Earth.

235
00:33:59,447 --> 00:34:03,201
The world's tallest hundred mountains
are all here.

236
00:34:09,887 --> 00:34:14,563
And within these peaks
live 70 million people...

237
00:34:15,687 --> 00:34:20,397
...many at altitudes
that pose a threat to the human body.

238
00:34:26,567 --> 00:34:32,039
In the Doramba region of Nepal,
the residents face an insidious threat.

239
00:34:35,527 --> 00:34:40,601
Dangerously high levels of harmful
UVrays pierce the thin mountain air

240
00:34:40,647 --> 00:34:42,877
and burn people's eyes.

241
00:34:48,607 --> 00:34:50,837
And here in the village of Balau,

242
00:34:50,887 --> 00:34:54,800
65-year-old Teteeni
has paid a heavy price.

243
00:34:56,247 --> 00:34:57,566
She's blind.

244
00:34:57,607 --> 00:34:59,837
(TETEENI IN TRANSLATION)

245
00:35:24,367 --> 00:35:29,236
Teteeni is determined not to let blindness
interfere with her life,

246
00:35:29,287 --> 00:35:33,599
but simple tasks, such as fetching water,
now take longer

247
00:35:33,647 --> 00:35:35,478
and can be treacherous.

248
00:35:42,847 --> 00:35:45,919
Her blindness is caused by cataracts,

249
00:35:45,967 --> 00:35:51,280
a fogging of her lenses
exacerbated by the intense mountain sun.

250
00:35:53,767 --> 00:35:57,521
But isolated here in the Himalaya

251
00:35:57,567 --> 00:36:01,446
Teteeni has no access
to medical treatment.

252
00:36:06,447 --> 00:36:11,123
Fortunately, an answer to her prayers
may be just around the corner.

253
00:36:13,327 --> 00:36:14,726
From Kathmandu,

254
00:36:14,767 --> 00:36:18,646
Dr Sanduk Ruit has pioneered
a method of eye surgery

255
00:36:18,687 --> 00:36:22,282
that he brings to the remote corners
of the Himalaya.

256
00:36:26,807 --> 00:36:31,323
His mobile clinic
brings hope to thousands.

257
00:36:38,047 --> 00:36:41,926
And today,
Teteeni is setting off to join them.

258
00:36:48,167 --> 00:36:51,204
She has arranged
for the only transportation

259
00:36:51,247 --> 00:36:53,761
available to her in these mountains.

260
00:36:53,807 --> 00:36:59,484
A friend has offered to carry her
ten kilometres to the Doramba clinic.

261
00:37:00,767 --> 00:37:06,603
(MURMUR OF BACKGROUND CHATTER)

262
00:37:06,647 --> 00:37:11,846
While Dr Ruit's success rate is high,
there is still a strong chance

263
00:37:11,887 --> 00:37:15,926
that Teteeni's eyes
are too far gone to be saved.

264
00:37:17,447 --> 00:37:19,358
He makes no promises.

265
00:37:39,007 --> 00:37:43,319
Doramba's schoolhouse
is now an improvised operating theatre.

266
00:37:47,967 --> 00:37:54,281
It takes Dr Ruit just half an hour
to remove Teteeni's fogged lenses.

267
00:37:54,327 --> 00:37:56,283
He then replaces them

268
00:37:56,327 --> 00:38:00,764
with a synthetic lens
he manufactures himself.

269
00:38:02,687 --> 00:38:07,556
In the West,
this operation could cost $8,000.

270
00:38:09,967 --> 00:38:15,325
But, funded by charity, Dr Ruit doesn't
charge his patients a single rupee.

271
00:38:18,487 --> 00:38:23,242
With surgery now complete,
Teteeni can only wait.

272
00:38:37,687 --> 00:38:43,000
Just 24 hours after her operation,
Teteeni joins hundreds of patients

273
00:38:43,047 --> 00:38:48,758
waiting to have their bandages removed,
hopefully with her sight restored.

274
00:38:56,567 --> 00:39:00,480
For Teteeni, this is the moment of truth.

275
00:39:39,567 --> 00:39:42,400
(SINGING)

276
00:39:42,447 --> 00:39:47,919
For the first time in three years,
Teteeni can see.

277
00:40:28,567 --> 00:40:30,558
In the Himalayan foothills,

278
00:40:30,607 --> 00:40:35,727
modern medicine is helping prolong
the lives of the people who live here.

279
00:40:42,287 --> 00:40:45,404
But, as you climb higher
in these mountains,

280
00:40:45,447 --> 00:40:48,883
it's how to deal with death
that poses a problem.

281
00:40:58,167 --> 00:41:00,727
At over 4,000 metres,

282
00:41:00,767 --> 00:41:04,442
Dho Tarap is one of
the highest communities on Earth.

283
00:41:04,487 --> 00:41:10,119
(BELLS TOLL)

284
00:41:10,167 --> 00:41:14,240
Buddhists live here
in almost complete isolation.

285
00:41:19,487 --> 00:41:22,524
And when someone dies at this altitude,

286
00:41:22,567 --> 00:41:25,604
dealing with the corpse
is a real challenge.

287
00:41:29,687 --> 00:41:33,441
Last night
there was a death in the village.

288
00:41:37,167 --> 00:41:40,921
70-year-old Nombe-la passed away,

289
00:41:40,967 --> 00:41:45,279
and now his family are preparing his body
for a Buddhist funeral.

290
00:41:51,407 --> 00:41:56,276
The man in charge of this funeral
is Holy Lama Namgyal.

291
00:41:56,327 --> 00:42:00,525
(HE INTONES IN LOCAL DIALECT)

292
00:42:00,567 --> 00:42:03,559
(LAMA NAMGYAL'S NARRATION
IN TRANSLATION)

293
00:42:20,167 --> 00:42:24,046
Lama Namgyal
needs to hold the funeral soon

294
00:42:24,087 --> 00:42:29,036
because Nombe-la's corpse could attract
predators and spread disease.

295
00:42:31,687 --> 00:42:36,203
But when you live at the roof
of the world, your options are limited.

296
00:42:37,807 --> 00:42:41,561
Buddhists don't bury their dead.

297
00:42:45,487 --> 00:42:50,880
And at this altitude no trees grow,
so there is no wood for a cremation.

298
00:42:55,167 --> 00:43:00,639
The solution is a sacred ritual
older than Buddhism itself.

299
00:43:04,407 --> 00:43:06,967
A sky burial.

300
00:43:15,967 --> 00:43:18,879
To conduct the sky burial ritual,

301
00:43:18,927 --> 00:43:22,840
Lama Namgyal
needs the help of a specialist.

302
00:43:25,287 --> 00:43:28,597
(BHARMAY FURBA INTONES)

303
00:43:28,647 --> 00:43:32,037
Bharmay Furba is the undertaker.

304
00:43:34,927 --> 00:43:37,805
As a non-Buddhist, he is the only one

305
00:43:37,847 --> 00:43:42,079
who is allowed to carry out
this most difficult task.

306
00:44:07,127 --> 00:44:09,482
- (THEY CHANT)
- (BELLS RING)

307
00:44:09,527 --> 00:44:14,840
(RHYTHMIC PERCUSSIVE BEATS)

308
00:44:16,487 --> 00:44:21,038
The funeral procession heads
an hour-and-a-half up into the mountains

309
00:44:21,087 --> 00:44:24,875
to a sacred ledge
where sky burials have been performed

310
00:44:24,927 --> 00:44:27,680
for over 1,000 years.

311
00:44:31,767 --> 00:44:33,644
Here, they will rendezvous

312
00:44:33,687 --> 00:44:37,805
with the most efficient scavengers
in these mountains.

313
00:44:40,047 --> 00:44:41,958
Vultures.

314
00:44:49,447 --> 00:44:51,085
For millennia,

315
00:44:51,127 --> 00:44:55,040
Buddhists in these mountains
have relied on the griffon vulture

316
00:44:55,087 --> 00:44:57,920
and the lammergeier
to help them dispose of their dead.

317
00:45:02,407 --> 00:45:07,879
These birds swiftly consume a corpse
before it can spread disease.

318
00:45:11,447 --> 00:45:13,881
Buddhists see this as a sacred act,

319
00:45:13,927 --> 00:45:17,966
an offering that will sustain the life
of another being.

320
00:45:23,807 --> 00:45:28,198
For them, Nombe-la's corpse
is now an empty vessel.

321
00:45:30,167 --> 00:45:33,364
His soul has already migrated
to another realm.

322
00:45:45,527 --> 00:45:49,998
Nombe-la's sons pay their final respects
to their father.

323
00:45:52,607 --> 00:45:57,556
But they don't wish to be present
for what is about to take place.

324
00:45:57,607 --> 00:46:02,965
Because, in order for the vultures
to consume Nombe-la's corpse quickly,

325
00:46:03,007 --> 00:46:05,919
Bharmay must make it easier for them.

326
00:46:07,407 --> 00:46:09,125
(RHYTHMIC BEATS AND BELLS RING)

327
00:46:58,007 --> 00:46:59,360
(HORN PLAYS)

328
00:47:29,567 --> 00:47:34,880
Nombe-la's corpse has now gone
and cannot spread disease.

329
00:48:28,007 --> 00:48:32,956
To survive in the mountains,
you have to understand them.

330
00:48:39,767 --> 00:48:43,919
Mountain habitats can be brutal

331
00:48:43,967 --> 00:48:49,325
but if you use
your ingenuity, determination,

332
00:48:49,367 --> 00:48:52,757
resourcefulness and courage

333
00:48:52,807 --> 00:48:56,800
it is possible to make a life here
at the roof of the world.

334
00:49:15,247 --> 00:49:20,719
When the Human Planet mountains team
filmed the Mongolian eagle hunters,

335
00:49:20,767 --> 00:49:24,965
their search for a fox was like
finding a needle in a haystack.

336
00:49:25,007 --> 00:49:28,204
Keeping up with the hunters
also proved near impossible

337
00:49:28,247 --> 00:49:30,761
in the vast Mongolian landscape.

338
00:49:35,607 --> 00:49:39,600
The team are here
to film Sailau and his son Berik

339
00:49:39,647 --> 00:49:42,036
hunting with their eagle, Balapan.

340
00:49:43,727 --> 00:49:48,596
Cameraman Keith Partridge last met Berik
in June with his newborn chick.

341
00:49:49,807 --> 00:49:54,278
It's now November and bitterly cold -
an ideal time for hunting,

342
00:49:54,327 --> 00:49:58,559
when wolves, foxes and rabbits
all have thick winter coats.

343
00:49:59,967 --> 00:50:01,559
Unlike the wildlife,

344
00:50:01,607 --> 00:50:05,646
the old Russian trucks
are not well adapted to the cold

345
00:50:05,687 --> 00:50:08,679
so the drivers devise a novel solution.

346
00:50:10,927 --> 00:50:13,487
Keith opts for a different ride.

347
00:50:13,527 --> 00:50:14,960
KEITH: The fact is

348
00:50:15,007 --> 00:50:18,477
that I haven't even ridden a donkey
across Blackpool beach.

349
00:50:18,527 --> 00:50:20,165
And we've now got to go up there

350
00:50:20,207 --> 00:50:21,799
on one of these.

351
00:50:21,847 --> 00:50:23,838
Does my horse have a name?

352
00:50:23,887 --> 00:50:26,242
They don't have names?

353
00:50:28,887 --> 00:50:31,879
The Mongolians have more than 300 words

354
00:50:31,927 --> 00:50:34,680
just to describe the horse colours.

355
00:50:34,727 --> 00:50:35,716
Wow!

356
00:50:35,767 --> 00:50:40,921
You know that to make it go,
you slightly kick and you should say,

357
00:50:40,967 --> 00:50:43,037
- ''Shu!''
- OK, shall we follow Agii?

358
00:50:43,087 --> 00:50:44,600
Thank you.

359
00:50:44,647 --> 00:50:46,444
That's it. Shu!

360
00:50:46,487 --> 00:50:48,318
Shu!

361
00:50:49,487 --> 00:50:51,398
Shu!

362
00:50:51,447 --> 00:50:55,520
Eventually Keith finds the gears
and off he goes.

363
00:50:58,847 --> 00:51:01,839
After three hours, the trucks catch up,

364
00:51:01,887 --> 00:51:04,640
and Keith's only too happy
to leave his horse behind.

365
00:51:04,687 --> 00:51:06,200
(BALAPAN CHEEPS)

366
00:51:07,847 --> 00:51:08,962
What does he think?

367
00:51:16,887 --> 00:51:18,923
Except my nose? (CHUCKLES)

368
00:51:18,967 --> 00:51:20,719
Is it too big?

369
00:51:26,207 --> 00:51:28,960
There you go, no frostbite!

370
00:51:32,047 --> 00:51:35,119
Keith heads off
to test a small ''eagle-cam';

371
00:51:35,167 --> 00:51:38,603
for which Sailau has made a harness.

372
00:51:39,687 --> 00:51:43,566
There are cameras out there that
might give us a better picture quality

373
00:51:43,607 --> 00:51:45,199
but they're much bigger

374
00:51:45,247 --> 00:51:48,444
so we're playing this trade-off game
all the time

375
00:51:48,487 --> 00:51:50,955
between practicalities and quality.

376
00:51:51,007 --> 00:51:53,237
Anyway, we'll see how it goes.

377
00:51:56,687 --> 00:52:02,045
Sailau's eagle seems to fly quite happily
with the eagle-cam on her back.

378
00:52:02,087 --> 00:52:07,161
Encouraged by the first test,
they use a heavier high-definition camera.

379
00:52:08,967 --> 00:52:10,764
This will be amazing if this works.

380
00:52:10,807 --> 00:52:13,480
We don't even know
if the bird will take the weight.

381
00:52:13,527 --> 00:52:17,042
We might have overloaded the plane,
so to speak.

382
00:52:22,527 --> 00:52:26,918
The eagle flies beautifully.
Keith and the team head back to base,

383
00:52:26,967 --> 00:52:30,403
joining director Nic Brown
to view the results.

384
00:52:30,447 --> 00:52:31,960
It's a very tense moment.

385
00:52:32,007 --> 00:52:35,556
The locals join in for a bird's-eye view.

386
00:52:35,607 --> 00:52:39,395
They've never seen their eagles
quite like this before.

387
00:52:39,447 --> 00:52:41,005
- The hood's off!
- Hood's off.

388
00:52:41,047 --> 00:52:43,959
Off we go. Whoa!

389
00:52:44,007 --> 00:52:48,080
- How small, my God. Really wild!
- Look at his head!

390
00:52:52,567 --> 00:52:55,127
Wow, look at that banking round
with the head.

391
00:52:55,167 --> 00:52:59,797
Wow! That's pretty wicked, isn't it.

392
00:53:01,967 --> 00:53:03,002
The next day,

393
00:53:03,047 --> 00:53:07,359
the crew follow Berik and his young eagle
on their first fox hunt.

394
00:53:07,407 --> 00:53:09,477
The hunters shadow sweepers -

395
00:53:09,527 --> 00:53:13,361
men who flush the foxes
out into the open.

396
00:53:14,527 --> 00:53:17,519
The team must constantly move
from peak to peak

397
00:53:17,567 --> 00:53:20,320
to give the eagles the best chance
of seeing a fox.

398
00:53:20,367 --> 00:53:23,439
Sailau thinks that the fox
might be hiding

399
00:53:23,487 --> 00:53:27,605
over these small mounds
just behind these telegraph lines.

400
00:53:27,647 --> 00:53:31,481
He would like to go to one of those hills
and wait there.

401
00:53:31,527 --> 00:53:34,439
It seems to make sense
that we've got to head there.

402
00:53:34,487 --> 00:53:36,876
Well, you'd better be fast, mate,
cos he's just gone.

403
00:53:36,927 --> 00:53:37,916
There he goes.

404
00:53:37,967 --> 00:53:39,320
(LAUGHING)

405
00:53:40,927 --> 00:53:45,842
After packing up quickly,
the film crew race after the hunters.

406
00:53:46,887 --> 00:53:51,483
But as soon as Keith starts filming,
the plans seem to change yet again.

407
00:53:52,527 --> 00:53:55,997
Sailau's now moved off again
so, er, time to go.

408
00:53:56,047 --> 00:53:58,242
We've only been here for two minutes.

409
00:53:59,527 --> 00:54:01,882
This set the pattern of the day.

410
00:54:01,927 --> 00:54:05,602
As Sailau and Berik
move from peak to peak,

411
00:54:05,647 --> 00:54:11,358
so do the team, constantly playing
catch-up in the thin high-altitude air.

412
00:54:11,407 --> 00:54:15,446
Finally, near exhaustion,
they face a new problem.

413
00:54:17,727 --> 00:54:19,604
Where is Sailau?

414
00:54:20,767 --> 00:54:22,962
They call the director.

415
00:54:23,007 --> 00:54:25,805
Nic, Nic. This is Keith,
do you read? Over.

416
00:54:25,847 --> 00:54:27,883
NIC: Hi, yeah, how are you?

417
00:54:27,927 --> 00:54:32,796
We're on our third mountain range
of the day so far and still no luck, over.

418
00:54:32,847 --> 00:54:35,998
Which mountain range
are you on now? Over.

419
00:54:38,207 --> 00:54:39,799
ZUBIN: If only we knew!

420
00:54:39,847 --> 00:54:42,919
Behind us are the really big peaks
with no snow on.

421
00:54:42,967 --> 00:54:45,162
The terrain here is pretty wild, actually,

422
00:54:45,207 --> 00:54:47,482
and if anything's
going to be lurking about,

423
00:54:47,527 --> 00:54:50,599
I think I'd want to hide
around here somewhere.

424
00:54:50,647 --> 00:54:53,559
ZUBIN: Something's in front
of that telegraph pole. Is that a dog?

425
00:54:53,607 --> 00:54:54,835
KEITH: Let's get the lens on it.

426
00:55:00,327 --> 00:55:01,999
It's not a wolf, it's a cow!

427
00:55:02,047 --> 00:55:05,084
I never said it was a wolf,
I said it was moving.

428
00:55:05,127 --> 00:55:06,116
That is very true!

429
00:55:06,167 --> 00:55:08,920
Do I get the sense that delirium

430
00:55:08,967 --> 00:55:12,642
is beginning to set in
on the third mountain range of the day?

431
00:55:12,687 --> 00:55:15,076
Would you like some chocolate bars?

432
00:55:15,127 --> 00:55:20,281
Agii, every day, just at the point
where we are about to faint...

433
00:55:22,927 --> 00:55:25,395
You bust your teeth on them!

434
00:55:25,447 --> 00:55:29,486
I could ask you, Agii, if you could
keep them somewhere slightly warmer!

435
00:55:36,127 --> 00:55:38,038
They set off yet again.

436
00:55:38,087 --> 00:55:42,842
Keith and the team are beginning to wonder
if they'll ever film a hunt.

437
00:55:42,887 --> 00:55:45,082
But then, good news.

438
00:55:45,127 --> 00:55:46,879
We have seen a fox.

439
00:55:47,887 --> 00:55:50,401
It scarpered really fast
down that snowy slope.

440
00:55:50,447 --> 00:55:52,483
The guys are over there at the moment,

441
00:55:52,527 --> 00:55:55,121
trying to see where it's hunkered down.

442
00:55:55,167 --> 00:55:56,805
Once they're out on the snow,

443
00:55:56,847 --> 00:56:00,078
you can see them pretty easily cos it's
a little black dot, scarpering like hell.

444
00:56:00,127 --> 00:56:03,278
But, um, apart from that,
you see this place.

445
00:56:03,327 --> 00:56:07,286
It's utterly vast. It's like trying
to find a needle in a haystack.

446
00:56:07,327 --> 00:56:12,037
Suddenly there's a call from the valley
and the hunt is on.

447
00:56:13,327 --> 00:56:15,761
Everybody clear the front of frame,
please.

448
00:56:15,807 --> 00:56:16,796
Now!

449
00:56:17,847 --> 00:56:21,726
(BERIK MAKES MEWING WHOOPS)

450
00:56:34,927 --> 00:56:38,044
KEITH: I think the fox
has made a hasty escape.

451
00:56:39,407 --> 00:56:43,366
Both Balapan and the crew
have learned a lot on their first hunt.

452
00:56:45,767 --> 00:56:49,362
Still...the team
have yet to film a successful hunt.

453
00:56:49,407 --> 00:56:54,845
After several frustrating days,
the pressure to deliver is intense.

454
00:56:57,167 --> 00:57:01,843
We're fast running out of time, really.
It hasn't been looking very good.

455
00:57:01,887 --> 00:57:06,085
Just when the crew
are resigned to failure, a call is heard.

456
00:57:08,727 --> 00:57:14,882
Keith knows this is his and Balapan's
big chance to succeed.

457
00:57:17,127 --> 00:57:18,242
(MEWING WHOOP)

458
00:57:42,607 --> 00:57:44,882
The bird got it! My word.

459
00:57:47,047 --> 00:57:48,560
KEITH: When all those whoops go off,

460
00:57:48,607 --> 00:57:53,920
things just go from nought to a million
miles an hour in two or three seconds.

461
00:57:53,967 --> 00:57:57,323
Your heart races
when you are doing this sort of stuff,

462
00:57:57,367 --> 00:57:59,358
and you just have to respond
in a kind of a positive way.

463
00:57:59,407 --> 00:58:03,685
You know,just try and stay totally
focused on what you are here to do.

464
00:58:03,727 --> 00:58:07,037
When that adrenaline kicks in,
it's easy to get distracted,

465
00:58:07,087 --> 00:58:08,679
but you have to just think,

466
00:58:08,727 --> 00:58:10,797
''Right, now is the time
I've got to up my game,

467
00:58:10,847 --> 00:58:15,318
''really knuckle down and focus in
on getting the shot and making it work.''

468
00:58:15,367 --> 00:58:18,325
And also trying to build
some form of relationship

469
00:58:18,367 --> 00:58:20,642
with the people
that you are working with as well.

470
00:58:20,687 --> 00:58:22,086
So that they trust us

471
00:58:22,127 --> 00:58:26,643
and welcome us into what's
quite an intimate part of their lives.

