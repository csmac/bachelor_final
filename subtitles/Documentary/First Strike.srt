﻿1
00:00:08,741 --> 00:00:09,783
Jesus!

2
00:00:09,867 --> 00:00:10,951
Stonebridge?

3
00:00:11,035 --> 00:00:15,622
- I'm going in!
- No. Fall Back. Repeat. Fall Back!

4
00:00:16,207 --> 00:00:17,874
It's Connolly.

5
00:00:50,491 --> 00:00:52,117
Are you fucking kidding me?

6
00:00:54,036 --> 00:00:55,162
It was just a bit of fun.

7
00:00:55,705 --> 00:00:58,540
We had the charge set to explode
away from your body.

8
00:00:58,624 --> 00:01:00,000
Not that you knew that.

9
00:01:01,085 --> 00:01:02,461
Which was the fun part.

10
00:01:10,344 --> 00:01:11,386
Oh.

11
00:01:21,397 --> 00:01:24,483
No calls? You didn't even go
for your phone.

12
00:01:26,235 --> 00:01:28,570
You strapped a fucking bomb to my chest.

13
00:01:28,654 --> 00:01:29,988
Who am I gonna call?

14
00:01:30,698 --> 00:01:31,907
My mom?

15
00:01:32,533 --> 00:01:34,159
Ask her to defuse the fucking thing?

16
00:01:38,456 --> 00:01:40,081
You sick fuck!

17
00:01:40,708 --> 00:01:41,958
Scott's alive.

18
00:01:42,043 --> 00:01:43,794
Repeat, Scott's alive.

19
00:02:00,478 --> 00:02:02,896
- Well, you passed.
- Oh?

20
00:02:03,731 --> 00:02:07,192
And now it's time to claim your prize.

21
00:02:10,696 --> 00:02:12,197
Just the one, mind.

22
00:02:21,791 --> 00:02:23,834
Show him. Come on.

23
00:02:32,218 --> 00:02:33,760
Colonel.

24
00:02:41,602 --> 00:02:43,311
Look, Kate, one day you're gonna be
where I am

25
00:02:43,396 --> 00:02:46,398
and you're gonna have to make
these decisions for yourself.

26
00:02:46,691 --> 00:02:50,777
I knew Connolly would vet Scott
in the most extreme manner.

27
00:02:52,071 --> 00:02:56,992
I told Scott that he
was not to flinch in any way.

28
00:02:57,076 --> 00:02:59,369
Not to make a play,
not to call for help.

29
00:02:59,454 --> 00:03:01,037
Even if he thought he was going to die.

30
00:03:02,999 --> 00:03:05,000
Did you order him to go in?

31
00:03:05,626 --> 00:03:07,586
I gave him an informed choice.

32
00:03:08,755 --> 00:03:12,382
He volunteered, knowing exactly how
dangerous Daniel Connolly is.

33
00:03:14,260 --> 00:03:15,802
Do you have previous with Connolly?

34
00:03:18,681 --> 00:03:22,726
Northern Ireland was my first posting.

35
00:03:23,895 --> 00:03:26,188
I'd been there three days when

36
00:03:26,272 --> 00:03:28,773
Connolly broke into the home of
a delivery guy.

37
00:03:28,858 --> 00:03:31,735
He delivered groceries to the barracks.

38
00:03:34,030 --> 00:03:37,282
Connolly tied up the man's family
in the front room and told the guy

39
00:03:37,366 --> 00:03:41,703
that if he didn't drive a lorry full of
Semtex into the barracks

40
00:03:41,788 --> 00:03:44,247
that he would kill his family
in front of him.

41
00:03:44,832 --> 00:03:47,167
But if he did what he was told,

42
00:03:47,251 --> 00:03:50,962
Connolly promised to spare the lives of
his wife and children.

43
00:03:52,256 --> 00:03:57,219
The explosion killed five soldiers and
maimed dozens.

44
00:03:57,303 --> 00:03:59,262
I was first on the scene.

45
00:04:00,139 --> 00:04:01,556
And the man's family?

46
00:04:04,769 --> 00:04:08,688
Daniel Connolly is not a man to keep
his promises.

47
00:05:48,122 --> 00:05:49,623
They've moved to the compound.

48
00:05:56,756 --> 00:05:58,882
- Bank job?
- You don't need to know.

49
00:05:58,966 --> 00:06:00,717
You just need to do your job.

50
00:06:02,428 --> 00:06:04,221
Yeah, which would be what, exactly?

51
00:06:04,305 --> 00:06:06,264
To override the security.

52
00:06:09,936 --> 00:06:12,312
Do I get to see this place beforehand?

53
00:06:17,276 --> 00:06:18,485
Fuck.

54
00:06:21,531 --> 00:06:22,656
What about there?

55
00:06:22,740 --> 00:06:24,157
Not your concern.

56
00:06:24,242 --> 00:06:26,952
Cracking the access codes for here,
that is.

57
00:06:40,258 --> 00:06:42,050
You okay with that?

58
00:06:43,052 --> 00:06:45,303
Yeah, well, as far as a future-proof and

59
00:06:45,388 --> 00:06:49,141
impenetrable encryption
data storage unit goes, looks fine.

60
00:06:49,725 --> 00:06:51,309
I'll need to run some checks
at my place.

61
00:06:54,605 --> 00:06:55,772
What's this?

62
00:06:55,857 --> 00:06:58,024
Hugo, the guy before you.

63
00:06:58,735 --> 00:07:00,152
They're the codes he was working on.

64
00:07:05,199 --> 00:07:08,952
Hey. You can take the shiny stuff
and nothing else.

65
00:07:09,036 --> 00:07:11,413
I want you back at the bar in two.

66
00:07:50,703 --> 00:07:51,953
Stonebridge.

67
00:07:52,622 --> 00:07:54,456
Do you have a visual on Scott yet?

68
00:07:54,540 --> 00:07:56,124
- Affirmative.
- Is he okay?

69
00:08:05,843 --> 00:08:07,177
I think he'll come through.

70
00:08:08,429 --> 00:08:09,471
Fuck!

71
00:08:14,143 --> 00:08:15,310
Kate.

72
00:08:15,728 --> 00:08:17,646
I've been looking into it
like you asked.

73
00:08:18,356 --> 00:08:19,689
Read this.

74
00:09:40,438 --> 00:09:41,563
You look like shit.

75
00:09:42,899 --> 00:09:45,692
Yeah, they fucking blew me up, dickhead!

76
00:09:46,903 --> 00:09:48,487
Sorry I don't look fucking better.

77
00:10:01,417 --> 00:10:04,294
You know, I've met some
sick motherfuckers in my time,

78
00:10:04,378 --> 00:10:05,962
but Daniel Connolly...

79
00:10:06,047 --> 00:10:07,631
I'm fine by the way, thanks for asking.

80
00:10:07,715 --> 00:10:09,508
Sorry about that, mate.
How are you feeling, you all right?

81
00:10:09,592 --> 00:10:11,635
- Fuck you.
- I saw what happened back there.

82
00:10:11,719 --> 00:10:13,470
Oh, you saw that, did you?
Did you learn anything?

83
00:10:13,554 --> 00:10:15,180
- You're reckless, Scott.
- Excuse me?

84
00:10:15,264 --> 00:10:16,348
She's a fucking terrorist.

85
00:10:16,432 --> 00:10:18,725
You know fucking shit. Don't worry
about me, buddy, I'll do my job.

86
00:10:18,810 --> 00:10:19,976
Really? You're gonna put a
bullet in her?

87
00:10:20,061 --> 00:10:21,603
Yeah, if I have to, she's the bad guy.

88
00:10:21,687 --> 00:10:23,146
You're the one that's banging one
on the home team.

89
00:10:23,231 --> 00:10:25,315
Look, I told you, Kate and me
is none of your fucking business.

90
00:10:25,399 --> 00:10:27,484
It fucking is if it's gonna get
me killed out there!

91
00:10:27,777 --> 00:10:30,403
Sort your shit out, Michael.
For your sake.

92
00:10:30,488 --> 00:10:32,906
For Kate's, especially for Kerry's.

93
00:10:33,366 --> 00:10:35,408
Your wife? Remember her?

94
00:10:42,542 --> 00:10:43,750
You say Scott did this
from memory?

95
00:10:43,835 --> 00:10:45,669
And he only had eyes
on them for seconds.

96
00:10:45,753 --> 00:10:48,171
Looks like it relates to the same site.

97
00:10:48,256 --> 00:10:51,258
- One appears to overlay the other.
- Mmm.

98
00:10:51,342 --> 00:10:54,553
If this is an ATAT facility,
there'll be anti-ram barriers,

99
00:10:54,637 --> 00:10:56,179
acoustic infrared sensors.

100
00:10:56,264 --> 00:10:58,557
Ballistic-resistant entry points.
You name it.

101
00:10:58,641 --> 00:11:00,517
Not to mention more guards
than even Connolly could kill.

102
00:11:00,601 --> 00:11:04,146
This looks like some sort of
tunnel system running underneath.

103
00:11:04,230 --> 00:11:06,606
Scott said the maps looked much older

104
00:11:06,691 --> 00:11:08,775
so this structure could have been
there for years,

105
00:11:08,860 --> 00:11:10,569
decommissioned, forgotten about.

106
00:11:10,653 --> 00:11:14,906
Well, whatever and wherever this is,
Connolly's going in by the back door.

107
00:11:16,117 --> 00:11:17,284
Try again.

108
00:11:17,368 --> 00:11:18,702
All right.

109
00:11:19,245 --> 00:11:22,164
Route at 10,

110
00:11:22,248 --> 00:11:25,250
dot, 105,

111
00:11:27,336 --> 00:11:29,379
dot, 0,

112
00:11:29,464 --> 00:11:31,506
dot, 57.

113
00:11:32,091 --> 00:11:33,175
M-K

114
00:11:34,302 --> 00:11:36,511
D-I-R, dot.

115
00:11:40,057 --> 00:11:41,141
No. That's not right.

116
00:11:41,225 --> 00:11:42,267
Argh!

117
00:11:43,144 --> 00:11:44,186
Try again.

118
00:11:44,771 --> 00:11:45,812
All right, uh...

119
00:11:45,897 --> 00:11:49,024
These documents date back
to 2003. Now, take a look at this.

120
00:11:49,108 --> 00:11:51,902
- Excuse me, sir.
- No, not right now.

121
00:11:54,072 --> 00:11:56,531
Mahmood. Pakistani weapons scientist.

122
00:11:56,616 --> 00:11:59,201
We know what length Latif went to
capture and interrogate her.

123
00:11:59,285 --> 00:12:04,414
Now, it turns out that up until 2003
she's on ATAT's payroll.

124
00:12:04,499 --> 00:12:07,501
Mahmood and Bratton. Baghdad in 2003.

125
00:12:07,585 --> 00:12:09,669
Days before Porter got him out of there.

126
00:12:11,506 --> 00:12:13,715
Now, what if, right,

127
00:12:13,800 --> 00:12:17,803
Bratton and ATAT had intended on
planting a chemical weapons stock

128
00:12:17,887 --> 00:12:19,095
in Iraq,

129
00:12:19,180 --> 00:12:20,972
giving the Pentagon hawks
what they wanted

130
00:12:21,057 --> 00:12:23,308
and making a killing
when the war started?

131
00:12:26,104 --> 00:12:27,145
Shit.

132
00:12:27,230 --> 00:12:29,481
If what you're saying is right,
there's been a huge conspiracy here.

133
00:12:29,565 --> 00:12:31,358
Where some very influential people
go a long way

134
00:12:31,442 --> 00:12:33,944
to make sure none of this
ever comes out.

135
00:12:34,362 --> 00:12:35,445
Where are we?

136
00:12:35,530 --> 00:12:37,906
Scott saw the code that
Connolly's hacker used.

137
00:12:38,908 --> 00:12:40,534
Right, do we know what it was?

138
00:12:40,618 --> 00:12:43,245
Well, we have the data
and we have the device

139
00:12:43,329 --> 00:12:45,330
but we also need the
initialisation protocol

140
00:12:45,415 --> 00:12:47,958
that they used to hack into
ATAT's security system.

141
00:12:48,042 --> 00:12:50,043
Can we use it to hack?

142
00:12:50,128 --> 00:12:51,837
Only if we get the exact code.

143
00:12:51,921 --> 00:12:53,630
One wrong character,
it's completely useless.

144
00:12:53,714 --> 00:12:55,632
Oh, come on,
having a photographic memory

145
00:12:55,716 --> 00:12:57,551
and trying to remember all this shit's
really fucking hard

146
00:12:57,677 --> 00:12:58,718
with all you people talking.

147
00:12:59,554 --> 00:13:00,762
Shut the fuck up.

148
00:13:02,974 --> 00:13:05,433
The guy before you, he was working on...

149
00:13:05,518 --> 00:13:06,935
Try this.

150
00:13:08,187 --> 00:13:10,772
D-I-R,

151
00:13:13,192 --> 00:13:16,445
slash, root at 10,

152
00:13:18,489 --> 00:13:20,615
dot, 105,

153
00:13:22,702 --> 00:13:24,327
dot, 0,

154
00:13:24,746 --> 00:13:27,497
dot, 57.

155
00:13:30,251 --> 00:13:31,835
- We're in.
- Well done.

156
00:13:32,462 --> 00:13:33,837
Fuck, yeah.

157
00:13:34,046 --> 00:13:35,088
Did you see that?

158
00:14:12,460 --> 00:14:14,753
I've implanted a worm
on this hard drive.

159
00:14:14,837 --> 00:14:17,923
Scott needs to open the system
with the drive and the code,

160
00:14:18,007 --> 00:14:21,343
the worm will then allow me to remotely
bypass the proprietary firewall

161
00:14:21,427 --> 00:14:22,677
and work on the system from here.

162
00:14:22,762 --> 00:14:26,431
Right, only one problem, dude,
I don't speak geek.

163
00:14:26,516 --> 00:14:29,392
You just remember the code.
Leave the rest to me.

164
00:14:29,477 --> 00:14:32,813
- But you will need to buy me some time.
- Yeah, how much?

165
00:14:32,897 --> 00:14:34,272
A few seconds.

166
00:14:34,357 --> 00:14:37,526
Shouldn't be a problem for Damien Scott,
world-class bullshitter.

167
00:14:38,277 --> 00:14:39,611
So that opens the vault?

168
00:14:39,695 --> 00:14:43,448
No, hacking into the system will bypass
the base site security

169
00:14:43,533 --> 00:14:44,783
and avoid triggering the alarm.

170
00:14:44,867 --> 00:14:45,992
Right.

171
00:14:46,077 --> 00:14:48,078
All right. How do we get in the vault?

172
00:14:48,454 --> 00:14:50,622
Hmm, most likely a biometric key.

173
00:14:50,706 --> 00:14:52,040
Retinal scan, fingerprint match...

174
00:14:52,125 --> 00:14:54,709
From a high-ranking executive like...

175
00:14:54,794 --> 00:14:56,253
- Bratton.
- ...Kenneth Bratton.

176
00:14:56,337 --> 00:14:59,631
All right, I'm out of here.

177
00:15:06,097 --> 00:15:09,224
- Hello?
- It's Ken Bratton. I need to see you.

178
00:15:09,809 --> 00:15:11,434
I can't speak over the phone.

179
00:15:12,562 --> 00:15:14,563
We have to meet somewhere safe.

180
00:15:18,568 --> 00:15:20,944
- Looking forward to that drink together?
- What drink?

181
00:15:21,028 --> 00:15:23,155
She's here, got to go.

182
00:15:26,492 --> 00:15:28,034
- Hey.
- Hey.

183
00:15:29,328 --> 00:15:31,079
Where's everyone else?

184
00:15:31,164 --> 00:15:32,456
Busy.

185
00:15:32,832 --> 00:15:33,874
All right.

186
00:15:33,958 --> 00:15:35,375
What are you worried about?

187
00:15:36,586 --> 00:15:38,962
- I'm on orders to keep you relaxed.
- Hmm.

188
00:15:40,882 --> 00:15:43,925
Bratton's scared. He wants to meet in
a public place.

189
00:15:45,553 --> 00:15:47,429
- You think he's telling the truth?
- I do, yeah.

190
00:15:47,513 --> 00:15:49,389
This is about his children.

191
00:15:50,475 --> 00:15:52,642
You know Colonel,
if we're right and bring Bratton in,

192
00:15:52,727 --> 00:15:55,854
this puts an end to Connolly's plan.
You could pull Scott out.

193
00:15:56,397 --> 00:15:58,064
Captain, nothing would give
me greater pleasure

194
00:15:58,149 --> 00:16:00,108
than chopping Connolly off at the knees.

195
00:16:00,193 --> 00:16:04,112
But his plan needs to succeed if he's
to lead us to Latif.

196
00:16:04,197 --> 00:16:07,032
Connolly could be coming for
Bratton at any time.

197
00:16:07,116 --> 00:16:09,951
I want you to extract him
quickly and quietly

198
00:16:10,036 --> 00:16:13,038
so we can brief him and put him back to
work with Connolly

199
00:16:13,122 --> 00:16:14,831
under our protection.

200
00:16:14,916 --> 00:16:17,250
All right, you're his contact.
You're running point.

201
00:16:17,335 --> 00:16:19,044
- Yeah.
- Okay?

202
00:16:19,295 --> 00:16:20,462
Copy that.

203
00:16:20,963 --> 00:16:24,591
Kate, let me get in close.
You run point.

204
00:16:24,926 --> 00:16:27,636
- He asked for me.
- Yeah, I know he did but...

205
00:16:27,720 --> 00:16:29,054
But what?

206
00:16:30,473 --> 00:16:32,265
Scott told me about Connolly.

207
00:16:33,017 --> 00:16:34,267
If something goes wrong
or it's a trap...

208
00:16:34,352 --> 00:16:35,727
Michael, I don't need you protecting me.

209
00:16:35,812 --> 00:16:38,897
Look, I know you don't need me
protecting you.

210
00:16:39,190 --> 00:16:41,066
But I care about you.

211
00:16:46,864 --> 00:16:47,906
I know.

212
00:17:10,930 --> 00:17:12,764
Lovely family, Ken.

213
00:17:13,266 --> 00:17:16,101
I can't speak over the phone,
we have to meet.

214
00:17:17,311 --> 00:17:20,814
I just want to know that
my family are going to be safe.

215
00:17:27,989 --> 00:17:29,781
You wanted to see me, Ken?

216
00:17:31,492 --> 00:17:33,535
Darin, there's certain things
I need to tell you.

217
00:17:35,955 --> 00:17:38,373
Oh! Fuck me!

218
00:17:38,458 --> 00:17:41,126
Please, please, please will
you just stop, stop, stop,

219
00:17:47,175 --> 00:17:48,425
Oh, my God.

220
00:17:48,509 --> 00:17:49,551
Oh.

221
00:17:53,014 --> 00:17:54,055
Wow.

222
00:17:56,184 --> 00:17:57,225
That was amazing.

223
00:17:58,978 --> 00:18:00,770
You really know what you're doing, huh?

224
00:18:00,855 --> 00:18:02,898
I trained for two years.

225
00:18:03,566 --> 00:18:05,567
Pleasure and pain. That's me.

226
00:18:06,736 --> 00:18:08,278
You know, if you're that good
at something...

227
00:18:08,362 --> 00:18:10,781
Nobody wants to pay for a massage,

228
00:18:10,865 --> 00:18:13,366
not since the credit crunch.

229
00:18:15,244 --> 00:18:20,582
So, uh, you just become some
terrorist groupie, hmm?

230
00:18:22,001 --> 00:18:24,920
You don't know Daniel.

231
00:18:25,004 --> 00:18:27,255
You don't see what he stands for,
what he believes in.

232
00:18:30,718 --> 00:18:33,929
Anyway, live fast, die young.

233
00:18:34,013 --> 00:18:36,014
So what happens if you don't die?

234
00:18:38,976 --> 00:18:42,145
Who'd you rather see in the mirror
in 20 years' time?

235
00:18:43,648 --> 00:18:46,107
A happy masseuse with a couple of kids?

236
00:18:48,403 --> 00:18:52,697
Or just some cold-hearted soulless
bitch with black holes for eyes? Hmm?

237
00:18:53,366 --> 00:18:54,616
Fuck you.

238
00:18:58,788 --> 00:18:59,830
Hey.

239
00:19:05,336 --> 00:19:07,212
So how many people have you killed?

240
00:19:14,011 --> 00:19:16,346
Where the fuck are they?

241
00:19:31,612 --> 00:19:33,321
Visual on principal.

242
00:19:33,448 --> 00:19:35,782
Visual clear. Moving to transport.

243
00:19:35,867 --> 00:19:37,868
Copy that, initiating contact.

244
00:19:42,206 --> 00:19:43,248
Yeah?

245
00:19:43,332 --> 00:19:46,209
Walk to the end of the street,
take a right at the coffee shop.

246
00:19:46,294 --> 00:19:48,920
Wait for me at the windows outside.

247
00:19:59,682 --> 00:20:02,017
- Mr Bratton.
- Am I being followed?

248
00:20:02,101 --> 00:20:03,059
Relax, we're going to get out of here.

249
00:20:03,144 --> 00:20:05,187
- What about my family?
- They're fine. You need to come with me.

250
00:20:05,271 --> 00:20:07,063
Yeah, I have to know,
I have to know that they're safe.

251
00:20:07,148 --> 00:20:08,899
They'll be safe. We're 50 yards from
making that happen.

252
00:20:08,983 --> 00:20:10,484
- You just need to come with me.
- No!

253
00:20:10,568 --> 00:20:11,818
I need to know
that they are safe!

254
00:20:11,903 --> 00:20:13,445
Kenneth, I need to get
you into the car.

255
00:20:13,529 --> 00:20:14,821
Christ, how the hell
did I get into this?

256
00:20:14,906 --> 00:20:16,406
Is the principal secured?

257
00:20:16,491 --> 00:20:17,783
Negative, walking away.

258
00:20:17,867 --> 00:20:19,409
We'll talk about that,
come with me, get into the car.

259
00:20:19,494 --> 00:20:22,204
No! You have no idea the pressure
we were under.

260
00:20:22,288 --> 00:20:25,081
They wanted, no, God knows, they
needed to find chemical weapons in Iraq.

261
00:20:25,166 --> 00:20:26,249
Listen to me.

262
00:20:26,334 --> 00:20:27,375
It was never, never supposed to be.

263
00:20:27,460 --> 00:20:28,877
- Get into the car, Ken.
- Kate, bring him back.

264
00:20:28,961 --> 00:20:30,837
That's why, that's why I hid it.

265
00:20:30,922 --> 00:20:32,047
Principal is not secure.

266
00:20:32,131 --> 00:20:33,590
Repeat, principal is not secure.

267
00:20:33,674 --> 00:20:36,009
When I found out what this damn thing
could do to millions of people

268
00:20:36,094 --> 00:20:37,511
- with just a few drops...
- Okay, I'm listening.

269
00:20:37,595 --> 00:20:39,846
You're talking Sarin?
You're talking VX?

270
00:20:39,931 --> 00:20:41,848
- Yes, and you must understand...
- What the hell is going on?

271
00:20:47,271 --> 00:20:49,064
- Move!
- What the hell is going on?

272
00:21:00,201 --> 00:21:01,743
Move! Get down!

273
00:21:15,174 --> 00:21:16,633
Shit!

274
00:21:27,019 --> 00:21:28,979
Neve, we're moving now.

275
00:21:34,986 --> 00:21:37,988
It's true what they say. You can take
the girl out of Ireland...

276
00:21:50,668 --> 00:21:52,669
Connolly is after a chemical weapon
for Latif?

277
00:21:52,754 --> 00:21:55,338
Yes, Sarin VX. That's what Kate said.

278
00:21:55,423 --> 00:21:57,424
We have to get our hands on it first.

279
00:21:57,508 --> 00:21:58,842
Kate?

280
00:21:58,926 --> 00:22:01,219
We do not deflect from this mission.

281
00:22:01,304 --> 00:22:03,221
All right. We still have Scott
on the inside.

282
00:22:03,306 --> 00:22:05,557
And we have no way of knowing
where Kate is.

283
00:22:07,268 --> 00:22:08,768
That's not entirely true, Colonel.

284
00:22:15,735 --> 00:22:19,529
Maintaining lock on Scott's phone.
Heading east.

285
00:22:21,407 --> 00:22:23,325
Signal is slowing.

286
00:22:30,500 --> 00:22:33,043
You're not fucking serious. Come on...

287
00:22:37,924 --> 00:22:41,009
Oh, very fucking sneaky. Nice.

288
00:22:41,511 --> 00:22:43,512
Why'd you change the schedule?

289
00:22:44,263 --> 00:22:45,430
Are you getting cold feet?

290
00:22:50,937 --> 00:22:52,521
We've lost Scott's phone.

291
00:22:52,605 --> 00:22:53,980
I don't remember giving you an order

292
00:22:54,065 --> 00:22:56,233
to put a tracking device on
Captain Marshall.

293
00:22:57,151 --> 00:23:01,279
Give them the tracking reference and
then get out in the field and find Kate.

294
00:23:01,364 --> 00:23:02,405
Yes, ma'am.

295
00:23:06,953 --> 00:23:08,453
So what is this place?

296
00:23:09,664 --> 00:23:10,705
Patience.

297
00:23:18,131 --> 00:23:20,298
How are you holding up, Ken?

298
00:23:22,093 --> 00:23:23,552
I have a surprise for you.

299
00:23:41,571 --> 00:23:44,781
They've got my family!

300
00:23:45,616 --> 00:23:46,741
Now, come on, Ken.

301
00:23:47,452 --> 00:23:48,702
Remember Ramadi.

302
00:23:49,495 --> 00:23:51,163
Remember the lies I told for you.

303
00:23:52,165 --> 00:23:54,207
I put my neck on the block for you.

304
00:23:55,042 --> 00:23:56,251
And just because you've lost your balls

305
00:23:56,335 --> 00:23:58,211
doesn't mean you're gonna
take me down with you.

306
00:24:15,938 --> 00:24:18,106
We've got Marshall's signal.

307
00:24:18,191 --> 00:24:20,400
It's the same location as Scott.

308
00:24:21,277 --> 00:24:24,654
So, as we discussed.

309
00:24:24,739 --> 00:24:27,115
Ken's not leaving?

310
00:24:27,533 --> 00:24:29,493
Nobody's leaving.

311
00:24:50,264 --> 00:24:51,598
Now, I've got to ask.

312
00:24:52,892 --> 00:24:55,477
What's a nice girl like you
doing working for the colonial enemy?

313
00:24:56,062 --> 00:24:58,063
Spare me the prehistoric shite.

314
00:24:58,147 --> 00:24:59,856
You see, I'm thinking it's a
childhood thing

315
00:25:00,817 --> 00:25:02,150
that you've uh, never...

316
00:25:03,403 --> 00:25:05,612
Well, you know, playing soldiers
in the back garden

317
00:25:05,696 --> 00:25:08,240
with your brother... No, brothers.

318
00:25:09,534 --> 00:25:11,451
Always wanting to be one of the lads.

319
00:25:13,287 --> 00:25:14,746
Enough about me.

320
00:25:15,498 --> 00:25:17,499
Did you always want to
be a cold-blooded butcher?

321
00:25:17,708 --> 00:25:18,750
No,

322
00:25:19,919 --> 00:25:22,129
it wasn't my first calling.

323
00:25:22,797 --> 00:25:25,423
Don't tell me,
you want to become a priest.

324
00:25:26,467 --> 00:25:29,219
No. As a matter of fact,

325
00:25:31,222 --> 00:25:33,014
you know I've never told anyone
this before

326
00:25:35,393 --> 00:25:36,810
but I always wanted to be a jockey.

327
00:25:38,020 --> 00:25:39,104
On the level.

328
00:25:40,273 --> 00:25:44,985
When I was 15, I jumped school
and hopped on a bus to Fermanagh

329
00:25:45,069 --> 00:25:47,279
to work at a stable out there.

330
00:25:47,447 --> 00:25:49,865
You know, mucking out, grooming.

331
00:25:50,032 --> 00:25:52,742
I even got to ride a couple of times.

332
00:25:53,536 --> 00:25:55,245
It was the greatest week of my life.

333
00:25:58,458 --> 00:26:00,625
And then, on the Saturday,

334
00:26:01,461 --> 00:26:04,463
the head lad calls me in and says

335
00:26:04,881 --> 00:26:06,506
we have to let you go.

336
00:26:09,343 --> 00:26:10,844
Why was that?

337
00:26:13,598 --> 00:26:15,432
I frightened the horses.

338
00:26:23,441 --> 00:26:24,524
They 're on the move.

339
00:26:24,609 --> 00:26:26,067
Sending new co-ordinates.

340
00:26:30,615 --> 00:26:34,868
So basically, we're just gambling that
Marshall will be with Connolly.

341
00:26:34,952 --> 00:26:37,746
If she's still alive, it's because
Connolly has a use for her.

342
00:26:54,180 --> 00:26:56,348
We're getting transmission interference.

343
00:26:56,432 --> 00:26:59,226
Her signal's down.
I'm patching through last co-ordinates.

344
00:26:59,310 --> 00:27:01,603
Roger that, on my way.

345
00:27:02,688 --> 00:27:05,232
According to Captain Marshall's last
known position,

346
00:27:05,316 --> 00:27:08,276
I think the tunnel's under
this facility.

347
00:27:08,903 --> 00:27:10,028
All right, what is it?

348
00:27:10,113 --> 00:27:13,990
It's a factory making fertilisers,
agricultural chemicals.

349
00:27:14,075 --> 00:27:15,325
A shell company for ATAT?

350
00:27:15,410 --> 00:27:16,660
A hiding place.

351
00:30:08,624 --> 00:30:10,041
You're on.

352
00:30:53,586 --> 00:30:55,962
Mind giving me a bit of space?

353
00:30:57,590 --> 00:30:58,840
Please?

354
00:31:04,263 --> 00:31:08,391
D-I-R, breaker at 10, dot,

355
00:31:08,601 --> 00:31:11,478
105, dot, 0,

356
00:31:11,562 --> 00:31:13,522
dot, 50.

357
00:31:17,485 --> 00:31:18,693
Okay.

358
00:31:19,278 --> 00:31:21,530
Scott's online. Okay. Here we go.

359
00:31:24,909 --> 00:31:26,743
The worm's not loading.

360
00:31:28,454 --> 00:31:30,872
All right. Doors,

361
00:31:31,457 --> 00:31:32,499
access...

362
00:31:33,918 --> 00:31:35,001
Voilà...

363
00:31:35,086 --> 00:31:37,754
- What's the problem, Major?
- I don't understand it.

364
00:31:47,515 --> 00:31:48,890
Come on!

365
00:31:55,273 --> 00:31:56,648
Fuck.

366
00:31:56,732 --> 00:31:58,191
Fuck, it's not loading!

367
00:32:02,488 --> 00:32:05,157
Uh, I don't know what's going on.

368
00:32:07,702 --> 00:32:09,786
Activated! Okay, okay.

369
00:32:09,871 --> 00:32:11,329
The worm is uploading.

370
00:32:12,915 --> 00:32:18,211
Come on!

371
00:32:21,591 --> 00:32:22,716
Denied?

372
00:32:22,800 --> 00:32:24,718
Not denied. Not recognised.

373
00:32:24,802 --> 00:32:27,554
Come on, come on. Work, damn it. Work!

374
00:32:28,139 --> 00:32:29,723
Have we a problem?

375
00:32:29,807 --> 00:32:30,932
Come on!

376
00:32:31,017 --> 00:32:32,184
Damn it!

377
00:32:32,268 --> 00:32:34,227
The worm! It works! Okay.

378
00:32:34,312 --> 00:32:38,064
ID, password. Please.

379
00:32:38,149 --> 00:32:40,150
We're in.

380
00:32:43,780 --> 00:32:45,530
I've never doubted you.

381
00:32:46,574 --> 00:32:48,074
Watch him.

382
00:33:17,438 --> 00:33:20,148
It's a bit late to find your spine, Ken.

383
00:33:32,495 --> 00:33:34,162
This is nice.

384
00:33:56,269 --> 00:33:57,435
Open it.

385
00:34:37,727 --> 00:34:40,187
Do you have any idea what this is?

386
00:34:40,271 --> 00:34:43,523
I have no fucking clue. Pack it!

387
00:34:43,608 --> 00:34:46,401
What are you gonna do?
You gonna kill me?

388
00:34:46,486 --> 00:34:48,779
If that's what he tells me to do.

389
00:34:48,863 --> 00:34:50,238
I'm on his side, remember?

390
00:34:51,032 --> 00:34:54,701
He's got no side. All right?
It's just him against everyone else.

391
00:35:04,128 --> 00:35:06,254
He's killing everyone else.

392
00:35:08,049 --> 00:35:09,299
Think about it.

393
00:35:10,718 --> 00:35:13,845
Come on. As soon as I'm done here,
he's gonna kill me.

394
00:35:13,971 --> 00:35:17,682
Shut up. Shut the fuck up!

395
00:35:22,772 --> 00:35:23,897
That's it.

396
00:35:23,981 --> 00:35:26,149
Soon as you're done,
he's gonna kill you.

397
00:35:26,234 --> 00:35:27,400
Who are you?

398
00:35:27,777 --> 00:35:29,903
Who the fuck are you?

399
00:35:29,987 --> 00:35:31,446
What about my family?

400
00:35:31,531 --> 00:35:32,864
You'll never know.

401
00:35:37,286 --> 00:35:38,912
They've detected the worm.

402
00:35:46,421 --> 00:35:47,546
No!

403
00:35:52,135 --> 00:35:53,260
Fuck me.

404
00:35:54,971 --> 00:35:57,305
- You okay?
- Yeah, yeah, yeah, yeah.

405
00:35:58,141 --> 00:35:59,683
Grenade! Get down!

406
00:36:07,567 --> 00:36:09,568
Another grenade. Move!

407
00:36:13,281 --> 00:36:14,906
Connolly, drop your weapon!

408
00:36:16,701 --> 00:36:17,784
Come on, Scott!

409
00:36:17,869 --> 00:36:19,744
Capture, Michael.
Do not fucking kill him!

410
00:36:19,829 --> 00:36:22,205
He's up, he's up, come on!

411
00:36:29,714 --> 00:36:31,965
Connolly, stop! Don't move!

412
00:36:32,091 --> 00:36:35,510
Put the weapon down, Connolly!
Put it down!

413
00:36:36,053 --> 00:36:38,305
- Put the fucking weapon down.
- Put it down!

414
00:36:41,267 --> 00:36:42,517
Gently.

415
00:36:46,230 --> 00:36:47,898
- Now, get on the ground.
- On the ground.

416
00:36:48,733 --> 00:36:50,484
Connolly, get on your fucking knees!

417
00:36:50,568 --> 00:36:52,110
Get the fuck on the ground!

418
00:36:52,195 --> 00:36:54,446
Typical fucking British,
always giving orders.

419
00:36:55,698 --> 00:36:56,740
Get on the ground.

420
00:37:00,453 --> 00:37:02,370
I think you should look
in the van, lads,

421
00:37:02,455 --> 00:37:04,915
can't have too much insurance,
you know what I mean?

422
00:37:07,794 --> 00:37:09,336
You watch him.

423
00:37:11,130 --> 00:37:12,547
No, Michael, Michael!

424
00:37:12,799 --> 00:37:13,840
Shit.

425
00:37:14,217 --> 00:37:15,467
Kate...

426
00:37:17,386 --> 00:37:18,512
It's all right.

427
00:37:23,184 --> 00:37:24,351
Okay.

428
00:37:27,438 --> 00:37:28,688
You like that?

429
00:37:28,773 --> 00:37:31,733
You fucking piece of shit!

430
00:37:31,818 --> 00:37:33,735
I should have shot you when
I had the fucking chance.

431
00:37:38,449 --> 00:37:39,741
Some of my best work.

432
00:37:39,826 --> 00:37:42,702
Shut the fuck up! Shut up!

433
00:37:43,788 --> 00:37:45,038
We have an anti-handling device here.

434
00:37:45,123 --> 00:37:48,500
- There's a tilt switch, yeah?
- Yeah, that's right.

435
00:37:48,584 --> 00:37:52,045
That mercury moves, it will detonate.
You have to stay still.

436
00:37:52,672 --> 00:37:55,882
These charges are shaped to blow right
towards you. You feel anything else?

437
00:37:55,967 --> 00:38:00,095
Yeah, there's something, um...
Cold, uh...

438
00:38:00,179 --> 00:38:02,472
There's metals digging into my stomach.

439
00:38:02,557 --> 00:38:03,765
No, I can still cut through
this cable here...

440
00:38:03,850 --> 00:38:05,100
- No, it's wired to the bomb.
- No, it's all right,

441
00:38:05,184 --> 00:38:07,644
I can get this off of you, I just need
to see what's behind the cable.

442
00:38:07,728 --> 00:38:09,646
Besides we both know
there isn't any time.

443
00:38:11,941 --> 00:38:15,360
Switch it off,
switch the fucking thing off!

444
00:38:15,445 --> 00:38:17,863
Yeah, I can switch it off,
but you'll have to let me go.

445
00:38:17,947 --> 00:38:19,448
No fucking way!

446
00:38:19,532 --> 00:38:23,410
You've won. You got your
fucking apocalypse back.

447
00:38:23,494 --> 00:38:26,413
Now, what I'm saying is,
I can disable that timer remotely.

448
00:38:26,497 --> 00:38:27,581
That is bullshit!

449
00:38:27,665 --> 00:38:28,790
But I'll need a10-yard start!

450
00:38:28,916 --> 00:38:30,417
No, no, Michael!

451
00:38:33,045 --> 00:38:34,463
You think I haven't done this sort of
thing before?

452
00:38:34,547 --> 00:38:35,589
You fuck!

453
00:38:35,673 --> 00:38:37,841
Yeah, go ahead, shoot me!

454
00:38:40,094 --> 00:38:42,262
Go on! He will not fucking do it!

455
00:38:42,346 --> 00:38:43,430
Scott, we have to give her
a chance, mate.

456
00:38:43,514 --> 00:38:45,849
There is no fucking chance and
he's playing you!

457
00:38:45,933 --> 00:38:49,352
Come on, soldier.
What other choice do you have?

458
00:38:50,062 --> 00:38:51,813
- Or maybe she's not worth it!
- Fuck!

459
00:38:51,898 --> 00:38:53,607
Michael, Michael!

460
00:38:57,487 --> 00:38:59,529
Michael, he's a fucking liar!

461
00:39:00,323 --> 00:39:01,573
Tick-tock, lads.

462
00:39:01,657 --> 00:39:03,033
I can't let her die.

463
00:39:03,159 --> 00:39:04,618
- Tick-tock!
- He's not gonna do it.

464
00:39:04,702 --> 00:39:06,119
Michael!

465
00:39:07,455 --> 00:39:08,497
100 yards.

466
00:39:10,875 --> 00:39:12,167
Michael!

467
00:39:13,294 --> 00:39:14,377
It's all I need.

468
00:39:14,462 --> 00:39:17,047
- Michael, he won't fucking do it.
- Shut up, Scott!

469
00:39:17,924 --> 00:39:20,008
- Michael, he's fucking lying!
- Michael, no!

470
00:39:20,093 --> 00:39:22,636
- He'll not get the bomb off her!
- Shut up, Scott!

471
00:39:24,347 --> 00:39:25,639
Promise.

472
00:39:26,933 --> 00:39:28,099
Shit.

473
00:39:30,186 --> 00:39:31,228
Fuck!

474
00:39:36,567 --> 00:39:38,318
- We got the weapon.
- What have you done?

475
00:39:38,403 --> 00:39:39,986
- It's fine, we got the weapon.
- What have you done?

476
00:39:40,071 --> 00:39:41,113
He promised!

477
00:39:46,786 --> 00:39:48,370
He promised.

478
00:39:51,958 --> 00:39:53,875
- What have you done?
- He promised.

479
00:40:03,761 --> 00:40:05,137
Bitch!

480
00:40:12,895 --> 00:40:14,229
Time to go.

481
00:40:16,399 --> 00:40:18,024
Michael, look at me.

482
00:40:19,235 --> 00:40:20,444
Do you love me?

483
00:40:24,490 --> 00:40:26,533
Sergeant, you will obey this order!

484
00:40:27,618 --> 00:40:29,995
- I'm sorry, Kate.
- Michael, get the fuck out.

485
00:40:30,079 --> 00:40:31,538
You always had my back.

486
00:40:31,622 --> 00:40:33,331
Come on! Move!

487
00:40:33,416 --> 00:40:34,791
- I'm sorry... Kate!
- Move it!

488
00:40:35,209 --> 00:40:36,251
Come on!

489
00:40:39,130 --> 00:40:41,214
Kate!

490
00:40:45,928 --> 00:40:48,889
Kate! Kate!

491
00:41:42,693 --> 00:41:43,735
Yes?

492
00:41:43,903 --> 00:41:48,365
My friend, a thousand apologies.
Until now he had proved very reliable.

493
00:41:49,492 --> 00:41:53,286
The reputation of a man like Connolly
relies on results.

494
00:41:54,080 --> 00:41:56,498
Let him know I am
extremely disappointed.

495
00:41:57,375 --> 00:42:01,378
Of course. But I assure you this is
only a temporary setback.

496
00:42:02,380 --> 00:42:04,506
Crawford's contact is still in place.

497
00:42:04,590 --> 00:42:07,050
I know where the remaining stock
can be found.

498
00:43:14,118 --> 00:43:15,368
Fellas.

499
00:43:18,247 --> 00:43:19,456
What took you so long?

500
00:43:19,540 --> 00:43:21,750
It didn't seem like you were
going anywhere.

501
00:43:23,461 --> 00:43:25,003
You're right.

502
00:43:26,380 --> 00:43:29,174
I figured between you and Latif,

503
00:43:32,887 --> 00:43:34,221
I got tired of running.

504
00:43:40,937 --> 00:43:42,437
Sorry about your friend,

505
00:43:43,606 --> 00:43:45,106
but you know how it is.

506
00:44:01,165 --> 00:44:05,877
I had a dream about you, Eleanor.
So I did.

507
00:44:06,921 --> 00:44:08,255
I'd like it to be you.

508
00:44:08,714 --> 00:44:10,507
Soldier to Soldier.

509
00:44:13,970 --> 00:44:15,387
You were never a soldier!

