1
00:00:00,332 --> 00:00:01,460
Chelsea: Not too long ago,

2
00:00:01,461 --> 00:00:03,795
I pulled a real
bonehead move and got a dui.

3
00:00:03,796 --> 00:00:05,454
Since then,
my roommate Dee Dee

4
00:00:05,455 --> 00:00:07,048
has become
my designated driver.

5
00:00:07,049 --> 00:00:10,385
She's perfect for the job because
she doesn't like drinking,

6
00:00:10,386 --> 00:00:13,389
and her initials are,
well, "D.D."

7
00:00:16,928 --> 00:00:18,310
[Groans] I have to go
for to the courthouse

8
00:00:18,311 --> 00:00:20,765
to get my community service
assignment tomorrow.

9
00:00:20,766 --> 00:00:23,420
I hope they don't make you pick up
trash by the side of the road.

10
00:00:23,421 --> 00:00:24,421
Mmm.

11
00:00:24,422 --> 00:00:27,142
Oh, maybe they'd let you
dirty Skype the troops.

12
00:00:28,193 --> 00:00:29,576
Oh. I would be honored

13
00:00:29,577 --> 00:00:32,031
to share my foxhole
with the soldiers.

14
00:00:32,907 --> 00:00:34,668
All right, you guys.

15
00:00:34,669 --> 00:00:35,970
Go ahead
and try this.

16
00:00:37,823 --> 00:00:42,333
Oh, Rick, I'd hate to say this
about alcohol in any form, but...

17
00:00:42,334 --> 00:00:44,178
- Uhh.
- What?

18
00:00:44,179 --> 00:00:46,480
What do you mean, "uhh"?
It's great.

19
00:00:46,481 --> 00:00:47,564
It's lavender-infused
vodka

20
00:00:47,565 --> 00:00:50,091
with a hin cream
and a cactus liqueur.

21
00:00:50,092 --> 00:00:51,852
That's not a drink.

22
00:00:51,853 --> 00:00:54,611
It's a candle you buy
at a lesbian bookstore.

23
00:00:55,220 --> 00:00:57,358
I like it.
Thank you.

24
00:00:57,359 --> 00:00:59,360
It smells like
my Nana.

25
00:00:59,361 --> 00:01:02,429
And her roommate
Millie.

26
00:01:02,430 --> 00:01:04,981
Wait. I just
realized something.

27
00:01:07,042 --> 00:01:08,459
Hey, look, at least
I'm trying, OK?

28
00:01:08,460 --> 00:01:10,975
I'm gonna open up my own bar
one day, and I'm telling you,

29
00:01:10,976 --> 00:01:12,997
the central and north
Jersey mixology society,

30
00:01:12,998 --> 00:01:14,682
it's a great
networking opportunity.

31
00:01:14,683 --> 00:01:17,101
Yeah, so's sleeping with people.

32
00:01:17,102 --> 00:01:19,079
Why don't you
try that, Rick?

33
00:01:19,080 --> 00:01:21,155
Well, I do,
but unlike you,

34
00:01:21,156 --> 00:01:23,891
not enough
to fill a bar.

35
00:01:23,892 --> 00:01:26,561
Chelsea, you should
give him the recipe

36
00:01:26,562 --> 00:01:28,146
for your bang-bang.

37
00:01:28,147 --> 00:01:29,781
I don't need
your bang-bang,

38
00:01:29,782 --> 00:01:30,732
whatever that is.

39
00:01:30,733 --> 00:01:33,093
I'm sure I can come up
with something better.

40
00:01:33,094 --> 00:01:34,619
Oh, yeah?
Yeah.

41
00:01:34,620 --> 00:01:36,287
All right,
don't look.

42
00:01:36,288 --> 00:01:37,038
You too, fella.

43
00:01:37,039 --> 00:01:38,873
Hey, you know
straight vodka

44
00:01:38,874 --> 00:01:40,184
is not a cocktail,
Chelsea.

45
00:01:40,185 --> 00:01:42,978
And straight men don't
have your haircut, Rick.

46
00:01:47,729 --> 00:01:49,734
Ahem.

47
00:01:49,735 --> 00:01:51,419
Here you go.

48
00:01:55,847 --> 00:01:57,481
Mmm.
OK, that's good.

49
00:01:57,482 --> 00:01:58,515
Wait for it.

50
00:01:58,516 --> 00:02:02,039
Holy God.

51
00:02:02,040 --> 00:02:04,866
And the's
the second bang.

52
00:02:04,867 --> 00:02:06,189
Yeah, I admit it's
got a kick to it,

53
00:02:06,190 --> 00:02:09,243
but not sure it's up to
the standards of the CNJMS.

54
00:02:09,244 --> 00:02:11,840
I have TMJ, fyi.

55
00:02:11,841 --> 00:02:13,113
You can
make fun of me,

56
00:02:13,114 --> 00:02:14,414
but at least
I have a goal.

57
00:02:15,540 --> 00:02:16,831
Now, come on.
Seriously.

58
00:02:16,832 --> 00:02:18,438
You know,
Olivia wants to bea.

59
00:02:18,439 --> 00:02:20,099
And Todd's studying acting.
What do you want?

60
00:02:20,100 --> 00:02:22,101
Todd's studying acting?
That's great.

61
00:02:22,102 --> 00:02:24,323
Yeah. Well, not when
one man has the monopoly

62
00:02:24,324 --> 00:02:27,010
on every great role
for little people.

63
00:02:27,011 --> 00:02:30,150
- Peter Dinklage.
- Who?

64
00:02:30,151 --> 00:02:32,582
Peter Dinklage
from "game of thrones."

65
00:02:32,583 --> 00:02:35,364
Oh, he is cute.

66
00:02:39,781 --> 00:02:41,078
Do you know him?

67
00:02:41,079 --> 00:02:44,844
Not all little people
know each other, Olivia.

68
00:02:45,665 --> 00:02:48,669
But, yes, I... I do.

69
00:02:48,670 --> 00:02:50,367
Ah, come on.
You didn't answer my question.

70
00:02:50,368 --> 00:02:51,651
What do you think
you want?

71
00:02:51,652 --> 00:02:53,737
Hey, I am happy
in my life. You know,

72
00:02:53,738 --> 00:02:55,855
I love my friends.
I loy job.

73
00:02:55,856 --> 00:02:57,386
I don't really
want anything.

74
00:02:57,387 --> 00:02:59,511
Oh, come on.
There must be something you want.

75
00:03:00,578 --> 00:03:03,934
Well, last week,
something hit me, but...

76
00:03:03,935 --> 00:03:05,700
Forget it.

77
00:03:05,701 --> 00:03:07,503
No, what is it?
Come on. Tell us.

78
00:03:07,504 --> 00:03:09,888
Well, I was just
walking down the street,

79
00:03:09,889 --> 00:03:13,067
and suddenly I realized
there is something I want.

80
00:03:13,068 --> 00:03:15,310
Ohh.

81
00:03:15,311 --> 00:03:17,986
- Rick: You want a baby?
- Are you nuts?

82
00:03:19,983 --> 00:03:21,368
I want boots,

83
00:03:21,369 --> 00:03:26,605
$700 hand-crafted French
thigh-high leather boots.

84
00:03:26,606 --> 00:03:30,459
So your big goal
is a pair of boots.

85
00:03:30,460 --> 00:03:32,611
I wouldn't really
put it that way.

86
00:03:32,612 --> 00:03:33,495
It's more like...

87
00:03:33,496 --> 00:03:36,216
[Bubbly] My big goal's
a pair of boots.

88
00:03:43,611 --> 00:03:45,933
<font color="#00ffff">Sync & corrections by Rafael UPD</font>
<font color="#00ffff">www.Addic7ed.Com/</font>

89
00:03:50,433 --> 00:03:53,450
So, Melvin, I was talking
to your daughter yesterday.

90
00:03:53,451 --> 00:03:55,253
You know what
her big goal is?

91
00:03:55,254 --> 00:03:57,171
Pair of thigh-high
leather boots.

92
00:03:57,172 --> 00:03:59,423
Really?
That's your goal?

93
00:03:59,424 --> 00:04:01,926
- Yeah.
- Thigh-high boots?

94
00:04:01,927 --> 00:04:04,362
I'm proud of you,
honey.

95
00:04:05,297 --> 00:04:07,081
You would look
terrific in those.

96
00:04:07,082 --> 00:04:08,599
Thanks, dad.

97
00:04:08,600 --> 00:04:09,599
You're serious?

98
00:04:09,600 --> 00:04:11,469
Ah. She's
a leggy knockout.

99
00:04:11,470 --> 00:04:12,795
She's only
gonna be young once.

100
00:04:13,834 --> 00:04:16,102
My wife,
rest her soul,

101
00:04:16,103 --> 00:04:18,242
cause she anything was
always saving for late.

102
00:04:18,243 --> 00:04:20,094
What if there is
not a later?

103
00:04:20,095 --> 00:04:21,529
Have fun, Chelsea.

104
00:04:21,530 --> 00:04:23,364
You have a terrific
figure now,

105
00:04:23,365 --> 00:04:26,308
but time is not kind
to a busty girl.

106
00:04:28,056 --> 00:04:29,337
You know what?

107
00:04:29,338 --> 00:04:31,602
I'll buy you those boots.
What do you need?

108
00:04:31,603 --> 00:04:32,823
700 bucks.

109
00:04:32,824 --> 00:04:35,293
700 bucks?!
Mm-hmm.

110
00:04:35,294 --> 00:04:36,777
You're gonna
700 bucks for boots,

111
00:04:36,778 --> 00:04:40,572
your head's so far up your ass,
you won't be able to see 'em.

112
00:04:43,986 --> 00:04:45,186
Don't worry about it, dad.

113
00:04:45,187 --> 00:04:47,638
I'm gonna find a way
to get those boots on my own,

114
00:04:47,639 --> 00:04:50,332
because I don't need to rely
on anybody or anything.

115
00:04:50,333 --> 00:04:51,776
Chelsea, did you
forget I'm driving you

116
00:04:51,777 --> 00:04:53,783
to the courthouse
for your dui assignment?

117
00:04:53,784 --> 00:04:55,045
No, I did not.

118
00:04:55,046 --> 00:04:56,672
I made you a sandwich.
Did you remember to eat lunch

119
00:04:56,673 --> 00:04:58,006
no, I did not.

120
00:04:58,007 --> 00:04:59,183
And here's your wallet.
You left...

121
00:04:59,184 --> 00:05:02,470
All right,
let's just go.

122
00:05:05,528 --> 00:05:07,825
You know what? You don't have
to wait with me here, Dee.

123
00:05:07,826 --> 00:05:09,476
Are you kidding?
I wanna meet the clerk.

124
00:05:09,477 --> 00:05:11,117
Why?

125
00:05:11,118 --> 00:05:12,597
Why? I love everything
about a courthouse.

126
00:05:12,598 --> 00:05:13,748
When I was
a little girl,

127
00:05:13,749 --> 00:05:16,968
I used to love to play
bailiff with my dad.

128
00:05:16,969 --> 00:05:18,886
How do you play bailiff?

129
00:05:18,887 --> 00:05:20,404
Well, you stand quietly
by his desk,

130
00:05:20,405 --> 00:05:22,673
sometimes for hours,
while he does his work.

131
00:05:22,674 --> 00:05:23,758
That's the hard part.

132
00:05:23,759 --> 00:05:25,684
When he leaves the room,
you get to say,

133
00:05:25,685 --> 00:05:28,012
"all rise
for judge daddy."

134
00:05:28,013 --> 00:05:30,402
Then when he comes
back in, you get to say,

135
00:05:30,403 --> 00:05:32,666
"all rise
for judge daddy."

136
00:05:32,667 --> 00:05:35,792
- Was your dad a judge?
- No. He was a cartoonist.

137
00:05:37,200 --> 00:05:39,073
Hello, ladies.

138
00:05:39,074 --> 00:05:40,875
Now, that is
a handsome man.

139
00:05:40,876 --> 00:05:43,861
Oh, yeah. Imagine
without the glasses.

140
00:05:43,862 --> 00:05:44,578
Oh.

141
00:05:44,579 --> 00:05:46,975
I'm so sorry
to keep you waiting.

142
00:05:46,976 --> 00:05:49,194
I just came from
another budgetary meeting.

143
00:05:49,195 --> 00:05:51,268
I came this close
to losing my job today.

144
00:05:51,269 --> 00:05:52,536
Oh.

145
00:05:52,537 --> 00:05:54,255
5 other guys did.

146
00:05:54,256 --> 00:05:55,289
[Chuckles]

147
00:05:55,290 --> 00:05:56,356
But enough about me.

148
00:05:56,357 --> 00:05:57,857
Chelsea...
Yeah, that's me.

149
00:05:57,858 --> 00:06:01,345
You need to be assigned
your community service, huh?

150
00:06:01,346 --> 00:06:01,963
Mm-hmm.

151
00:06:01,964 --> 00:06:03,130
What would
make you happy?

152
00:06:03,131 --> 00:06:05,182
Uh, getting
my license back.

153
00:06:05,183 --> 00:06:07,268
Yeah, walking up
to the drive-through

154
00:06:07,269 --> 00:06:08,743
is getting
really embarrassing.

155
00:06:08,744 --> 00:06:11,055
No, no, I'm serious.
I need you to be happy,

156
00:06:11,056 --> 00:06:13,557
and then I need you to
fill out this online survey

157
00:06:13,558 --> 00:06:16,477
and rate my performance.
It'll just take you 5 minutes,

158
00:06:16,478 --> 00:06:18,086
and you could
save my pension.

159
00:06:18,087 --> 00:06:20,172
[Laughs]

160
00:06:20,173 --> 00:06:22,871
So don't make me beg.
[Laughs]

161
00:06:22,872 --> 00:06:25,552
I think my friend Dee Dee would
love to see you on your knees.

162
00:06:25,553 --> 00:06:27,054
Chelsea!

163
00:06:28,179 --> 00:06:29,907
Um, well, if I say
nice things about you,

164
00:06:29,908 --> 00:06:32,559
can I get out of
my community service completely?

165
00:06:32,560 --> 00:06:34,161
Mm, no.
I can't do that.

166
00:06:34,162 --> 00:06:36,843
But just pick
any dumb charity you want.

167
00:06:36,844 --> 00:06:39,650
I'll pretty much sign off on
anything that has a letterhead.

168
00:06:39,651 --> 00:06:42,026
I know, right.

169
00:06:42,899 --> 00:06:43,946
Lemon drop?

170
00:06:43,947 --> 00:06:45,956
Certainly.

171
00:06:45,957 --> 00:06:48,137
I volunteer tutoring
at-risk kids.

172
00:06:48,138 --> 00:06:49,715
Maybe Chelsea could
help me with that.

173
00:06:49,716 --> 00:06:51,912
Oh, yeah. And then she could
sign off on it for me, right?

174
00:06:51,913 --> 00:06:53,956
Absolutely! Yeah.
I'll just give you these forms.

175
00:06:53,957 --> 00:06:56,017
Fill these out.
But you do have to get them back.

176
00:06:56,018 --> 00:06:58,185
Otherwise, the sheriff'll
come after you.

177
00:06:58,186 --> 00:07:00,087
Unless he loses
his job, too.

178
00:07:03,022 --> 00:07:05,642
Um, OK. Well,
thank you, Mr...

179
00:07:05,643 --> 00:07:06,801
Mills.
Marty mills.

180
00:07:07,412 --> 00:07:08,837
Oh. My grandpa
died in a mill.

181
00:07:08,838 --> 00:07:11,303
I don't know
why I said that.

182
00:07:11,304 --> 00:07:13,951
Down, girl.

183
00:07:17,993 --> 00:07:19,123
Dee Dee, your boyfriend
at the courthouse

184
00:07:19,124 --> 00:07:20,607
said you could
just sign this for me.

185
00:07:20,608 --> 00:07:22,493
[Scoffs]
He's not my boyfriend.

186
00:07:22,494 --> 00:07:23,819
Then why are
you blushing?

187
00:07:23,820 --> 00:07:25,571
Maybe I have rosacea.
Get off my back.

188
00:07:26,776 --> 00:07:29,283
After you tutor for 2 hours,
I'll sign the paper.

189
00:07:29,284 --> 00:07:32,186
- Ok, how 'bout an hour?
- Don't play me, girl.

190
00:07:33,855 --> 00:07:35,122
I learned that here.

191
00:07:35,123 --> 00:07:37,358
Oh. All right, fine.

192
00:07:37,359 --> 00:07:38,959
Who am I
sued to tutor?

193
00:07:38,960 --> 00:07:40,778
Hmm. Oh, I know
Warren needs somebody.

194
00:07:40,779 --> 00:07:43,147
Warren, this is my friend Chelsea.
Chelsea, Warren.

195
00:07:43,148 --> 00:07:44,922
Chelsea's really excited
to jump in and help you,

196
00:07:44,923 --> 00:07:47,926
but it's her first time doing
community service, so be nice.

197
00:07:47,927 --> 00:07:50,470
Community service?
What'd you get arrested for?

198
00:07:50,471 --> 00:07:52,514
Being too hot?

199
00:07:53,344 --> 00:07:55,859
You didn't tell me this
was the gifted program.

200
00:07:55,860 --> 00:07:58,045
Here's Warren's
work package.

201
00:07:58,046 --> 00:07:58,963
Speaking
of packages...

202
00:07:58,964 --> 00:08:00,397
All right, stud,
dial it back.

203
00:08:01,728 --> 00:08:02,866
All right,

204
00:08:02,867 --> 00:08:05,469
let's see
what we got there.

205
00:08:05,470 --> 00:08:06,820
Pre-algebra?

206
00:08:06,821 --> 00:08:10,508
[Scoffs] "E" equals mc
that's never gonna happen.

207
00:08:10,509 --> 00:08:12,009
Oh, "lord of the flies."

208
00:08:12,010 --> 00:08:13,706
You're gonna
love this book.

209
00:08:13,707 --> 00:08:15,913
They kill a wild boar
and a smart kid with glasses.

210
00:08:15,914 --> 00:08:18,227
You know, I would kill a smart
kid with glasses for you.

211
00:08:18,228 --> 00:08:21,250
If you were my woman, I would
shower you with gold and diamonds.

212
00:08:22,124 --> 00:08:24,004
I'm glad you ended that
with diamonds.

213
00:08:25,665 --> 00:08:27,741
All right, do you wanna
read this book or not?

214
00:08:27,742 --> 00:08:29,061
Well, that depends.

215
00:08:29,062 --> 00:08:30,594
Can we read it
at your place?

216
00:08:31,913 --> 00:08:33,130
OK, dude, listen.

217
00:08:33,131 --> 00:08:37,225
You will never get
a woman like this

218
00:08:37,226 --> 00:08:39,503
if you don't graduate
from High School.

219
00:08:39,504 --> 00:08:40,254
Do you know why?

220
00:08:40,255 --> 00:08:41,956
Because I'm
an expensive woman.

221
00:08:41,957 --> 00:08:44,388
I want a pair
of $700 boots.

222
00:08:44,389 --> 00:08:46,844
Do you think your job at
the chicken processing plant

223
00:08:46,845 --> 00:08:48,153
is gonna pay for that?

224
00:08:49,737 --> 00:08:51,799
All right, get started
on your history homework.

225
00:08:51,800 --> 00:08:54,451
And, uh, I'll be right back.

226
00:08:56,777 --> 00:08:58,704
Hey, Dee, I got him
started on his reading,

227
00:08:58,705 --> 00:09:00,441
so I'm gonna
go on my break.

228
00:09:00,442 --> 00:09:01,775
Quick question.
The faculty lounge,

229
00:09:01,776 --> 00:09:04,508
is that a full bar
or just beer and wine?

230
00:09:04,509 --> 00:09:06,669
It's 2 hours, Chelsea.
There are no breaks.

231
00:09:06,670 --> 00:09:09,817
Come on. I just got my period.
I gotta go to the nurse.

232
00:09:12,841 --> 00:09:16,390
Welcome to fast times
at this sucks high.

233
00:09:17,626 --> 00:09:21,687
Hey. All right, so do you have
any questions about history

234
00:09:21,688 --> 00:09:23,522
or life or...
I don't know...

235
00:09:23,523 --> 00:09:26,300
[Cell phone ringtone]

236
00:09:26,301 --> 00:09:27,985
[Rings]

237
00:09:27,986 --> 00:09:30,754
[Rings]

238
00:09:30,755 --> 00:09:32,030
[Ringing]

239
00:09:32,031 --> 00:09:34,481
Hey, Warren,
why does your crotch

240
00:09:34,482 --> 00:09:35,826
have the same ringtone
as my phone?

241
00:09:36,961 --> 00:09:39,204
I wasn't
stealing your phone.

242
00:09:39,205 --> 00:09:40,938
Ah. Sure you weren't,
you little thief.

243
00:09:40,939 --> 00:09:43,354
We don't have a computer,
and I was just use it

244
00:09:43,355 --> 00:09:44,619
to do research on
the Louisiana purchase.

245
00:09:44,620 --> 00:09:48,628
Of course you were. And that's
why it's between your legs,

246
00:09:48,629 --> 00:09:50,799
so you can attach it to that
printer that you've got down there.

247
00:09:55,947 --> 00:09:58,303
This is
Nana's candle.

248
00:09:58,304 --> 00:09:59,808
Enjoy.

249
00:10:03,955 --> 00:10:05,147
Ahh. All right.

250
00:10:05,148 --> 00:10:07,174
Ahem.

251
00:10:07,175 --> 00:10:08,608
Hey, dude.
Yeah?

252
00:10:08,609 --> 00:10:10,027
What have you got
for ro2?

253
00:10:10,028 --> 00:10:11,679
Oh, see the drink
with the live goldfish

254
00:10:11,680 --> 00:10:12,963
and little castle in it?

255
00:10:12,964 --> 00:10:14,298
It's the aquariumdaiquiri.

256
00:10:14,299 --> 00:10:17,534
Are you kidding me?
You're trying too hard.

257
00:10:17,535 --> 00:10:18,919
OK. Chelsea.

258
00:10:18,920 --> 00:10:20,437
Hey. What's up?

259
00:10:20,438 --> 00:10:21,913
Just got your text.

260
00:10:21,914 --> 00:10:23,507
You didn't need
to text her, Todd.

261
00:10:23,508 --> 00:10:24,661
I can handle this.

262
00:10:24,662 --> 00:10:26,393
You put an animal
in a drink.

263
00:10:26,394 --> 00:10:27,645
You need
the bang-bang recipe.

264
00:10:27,646 --> 00:10:29,947
Whoa. Why would I give
you the bang-bang recipe?

265
00:10:29,948 --> 00:10:32,632
Because Rick's gonna split
the $1,500 with you.

266
00:10:32,633 --> 00:10:34,568
Todd.
Don't push me away

267
00:10:34,569 --> 00:10:36,636
I'm here to help.

268
00:10:36,637 --> 00:10:37,805
[Sighs]

269
00:10:37,806 --> 00:10:38,939
All right,
yeah, fine.

270
00:10:38,940 --> 00:10:40,207
If we win, you get
half the money.

271
00:10:40,208 --> 00:10:43,694
All right. First I need
2 shots of vodka.

272
00:10:43,695 --> 00:10:45,212
All right.

273
00:10:51,386 --> 00:10:53,387
All right,
let's get to work.

274
00:10:53,388 --> 00:10:54,138
OK.

275
00:10:54,139 --> 00:10:55,947
- See what's happening here?
- Hmm?

276
00:10:55,948 --> 00:10:57,975
We are gonna win
that contest.

277
00:10:57,976 --> 00:10:59,964
- OK.
- Because someone up there

278
00:10:59,965 --> 00:11:02,146
really wants to see
me in those boots.

279
00:11:02,147 --> 00:11:06,080
So God wants to see you
in thigh-high boots?

280
00:11:06,081 --> 00:11:07,268
Look, all I'm saying

281
00:11:07,269 --> 00:11:09,002
I wear a white t-shirt,
it rains.

282
00:11:09,003 --> 00:11:11,188
You do the math.

283
00:11:14,409 --> 00:11:15,609
Yay. We did it.

284
00:11:15,610 --> 00:11:17,259
The bang-bang
came through.

285
00:11:18,132 --> 00:11:20,574
Yeah. Well, you did it.
That's the thing.

286
00:11:20,575 --> 00:11:24,139
I'm just so tired of being,
like, average at everything.

287
00:11:24,140 --> 00:11:26,704
Like, in school, I was
an average student.

288
00:11:26,705 --> 00:11:28,188
I was kind of
an average athlete.

289
00:11:28,189 --> 00:11:29,731
Well, how are you in bed?

290
00:11:31,420 --> 00:11:33,485
I mean, I'll be honest.
I don't blow me away.

291
00:11:35,847 --> 00:11:37,234
Shut up.

292
00:11:37,235 --> 00:11:38,947
You've done a lot
of really cool things.

293
00:11:38,948 --> 00:11:40,700
Didn't you, like, model
when you were a kid?

294
00:11:40,701 --> 00:11:43,286
Yeah.
I was a husky model.

295
00:11:45,047 --> 00:11:46,451
Are you kidding?

296
00:11:46,452 --> 00:11:47,924
No. You know,
then I hit puberty,

297
00:11:47,925 --> 00:11:50,027
and I thinned out,
so all the jobs

298
00:11:50,028 --> 00:11:53,384
and the free
giant pants dried up.

299
00:11:53,385 --> 00:11:55,258
Did you ever
do underwear ads?

300
00:11:55,259 --> 00:11:57,346
If they were tasteful.
Ho ho ho!

301
00:11:57,347 --> 00:12:00,529
If you love me,
you would find those pictures,

302
00:12:00,530 --> 00:12:02,223
blow one up, and give it
to me for Christmas.

303
00:12:02,224 --> 00:12:05,197
Well, I'm glad you
think this is funny.

304
00:12:05,198 --> 00:12:07,564
Oh, shut up, Rick.
Come on.

305
00:12:07,565 --> 00:12:09,413
You're great-looking now.
Everyone loves you.

306
00:12:09,414 --> 00:12:11,480
And one day,
when you open your own bar,

307
00:12:11,481 --> 00:12:13,300
in tribute
to your childhood,

308
00:12:13,301 --> 00:12:16,193
you can call it
tgi fat-ass.

309
00:12:17,066 --> 00:12:18,789
Ah, go on.
Get out of here.

310
00:12:18,790 --> 00:12:20,841
Get your hooker boots.

311
00:13:00,169 --> 00:13:02,950
There are so many shows
about judges and lawyers.

312
00:13:02,951 --> 00:13:05,352
What about the clerks
and the secretaries

313
00:13:05,353 --> 00:13:07,271
and the midlevel
administrators?

314
00:13:07,272 --> 00:13:09,390
That's the show
I want to see.

315
00:13:09,391 --> 00:13:12,309
- You know what show I want to see?
- What?

316
00:13:12,310 --> 00:13:15,959
You kissing that clerk
at the courthouse.

317
00:13:15,960 --> 00:13:17,731
Can I tell you a secret?

318
00:13:17,732 --> 00:13:19,366
When he showed Chelsea
and me to the door,

319
00:13:19,367 --> 00:13:21,201
he smelled like
a heavenly mixture

320
00:13:21,202 --> 00:13:24,261
of stetson cologne
and copier.

321
00:13:25,137 --> 00:13:28,075
Honey, are there any
chunks of your childhood

322
00:13:28,076 --> 00:13:29,684
that you
don't remember?

323
00:13:32,517 --> 00:13:34,415
Whoo!
Whoo!

324
00:13:34,416 --> 00:13:37,275
What do you think?
Worth 700 bucks?

325
00:13:37,276 --> 00:13:38,252
Yes!

326
00:13:38,253 --> 00:13:39,902
Oh, my God.
I'd ask to borrow them.

327
00:13:39,903 --> 00:13:41,988
Except they would
chafe my armpits.

328
00:13:43,490 --> 00:13:45,041
Chelsea,
you look so great.

329
00:13:45,042 --> 00:13:47,076
You're gonna turn
so many heads in those.

330
00:13:47,848 --> 00:13:50,046
And hopefully
raise a few.

331
00:13:50,047 --> 00:13:51,998
Dee Dee, you have
got to try these on.

332
00:13:51,999 --> 00:13:53,415
They will change
your life.

333
00:13:53,416 --> 00:13:56,774
That's the way I feel about
my Nate Berkus Fanny pack.

334
00:13:56,775 --> 00:13:58,693
Oh, here.
Take a picture.

335
00:13:59,671 --> 00:14:03,092
Heh. Why were you googling
the Louisiana purchase?

336
00:14:03,864 --> 00:14:06,179
What?
Let me see that.

337
00:14:06,180 --> 00:14:07,263
Heh.
Oh, my God.

338
00:14:07,264 --> 00:14:09,916
Warren really was googling
the Louisiana purchase.

339
00:14:09,917 --> 00:14:12,518
Oh, yeah. I lend him my phone all
time. They don't have a computer.

340
00:14:12,519 --> 00:14:15,206
But I thought he was trying to
steal it. I called him a thief.

341
00:14:15,207 --> 00:14:17,490
Oh, don't worry. He's got
much bigger problems than you.

342
00:14:17,491 --> 00:14:19,984
His mom's in jail, and he has to
take care of his little brother.

343
00:14:20,756 --> 00:14:22,161
Oh, my God. Really?

344
00:14:22,162 --> 00:14:24,080
Mm-hmm. Don't worry.
Warren's fine.

345
00:14:24,081 --> 00:14:27,575
Tonight's about you
and your fancy new boots.

346
00:14:35,128 --> 00:14:38,079
I just bought this amazing
new boots.

347
00:14:38,080 --> 00:14:40,665
I should be feeling
good right now,

348
00:14:40,666 --> 00:14:44,216
not feeling bad about kids
who don't have computers.

349
00:14:44,217 --> 00:14:46,294
Nobody said
you had to.

350
00:14:47,886 --> 00:14:49,386
Chelsea, before you
have another drink,

351
00:14:49,387 --> 00:14:51,736
can you do that online
survey for Marty Mioh, right.

352
00:14:51,737 --> 00:14:53,851
Right.

353
00:14:53,852 --> 00:14:56,076
Look at me,
using a computer

354
00:14:56,077 --> 00:14:58,650
like I deserve to.

355
00:14:58,651 --> 00:15:01,609
The only thing I use this computer for
is to online shop

356
00:15:01,610 --> 00:15:05,823
and watch goats kicking
people in the nuts.

357
00:15:05,824 --> 00:15:07,990
My grandparents had a carrear

358
00:15:07,991 --> 00:15:11,017
until Kim jong-il
ate it.

359
00:15:13,621 --> 00:15:15,232
You know what?
Give me the computer.

360
00:15:15,233 --> 00:15:18,500
There's no way in heck Marty
was only "somewhat prom."

361
00:15:18,501 --> 00:15:20,705
God, poor little Warren,

362
00:15:20,706 --> 00:15:23,508
you know, with his mom
on death row

363
00:15:23,509 --> 00:15:26,294
and that ugly
little backpack.

364
00:15:26,295 --> 00:15:29,637
How is he ever gonna learn
about the Louisiana purchase

365
00:15:29,638 --> 00:15:32,467
if he doesn't have
a computer at school?

366
00:15:32,468 --> 00:15:34,752
He's gonna learn
from good tutors like you.

367
00:15:34,753 --> 00:15:36,685
Now, are you drunk enough
to sleep yet?

368
00:15:36,686 --> 00:15:40,439
Why? Is Marty mills
coming over?

369
00:15:40,440 --> 00:15:43,213
That's crazy.
I don't even know his number.

370
00:15:43,214 --> 00:15:46,030
[Gasps] We should
Facebook him.

371
00:15:46,031 --> 00:15:47,231
Does he have Facebook?

372
00:15:47,232 --> 00:15:49,817
Everybody has Facebook.

373
00:15:51,200 --> 00:15:54,072
Except little Warren.

374
00:16:02,114 --> 00:16:03,831
Ohh.

375
00:16:16,013 --> 00:16:18,596
Good morning,
sleepyhead.

376
00:16:18,597 --> 00:16:22,273
Or should I say,
good afternoon, alcoholic"?

377
00:16:23,523 --> 00:16:27,778
Oh, God. My boots even look good
in the morning.

378
00:16:29,612 --> 00:16:31,073
Why is
my credit card out?

379
00:16:31,074 --> 00:16:32,860
Oh, you went online
and bought the kids

380
00:16:32,861 --> 00:16:34,452
at the tutor center
a computer.

381
00:16:34,453 --> 00:16:36,125
What?

382
00:16:36,899 --> 00:16:38,315
Oh, crap.

383
00:16:38,316 --> 00:16:41,583
I have done some awful
things when I'm drunk,

384
00:16:41,584 --> 00:16:43,477
but buying a computer
for needy kids,

385
00:16:43,478 --> 00:16:44,545
that's a new low.

386
00:16:46,832 --> 00:16:47,881
Nonsense, Chelsea.

387
00:16:47,882 --> 00:16:50,342
Admit it.
You've got a big heart.

388
00:16:51,218 --> 00:16:54,165
Oh, look it.
Olivia has a big man.

389
00:16:54,800 --> 00:16:56,050
Thank you
for everything.

390
00:16:56,051 --> 00:16:58,642
Take a powerbar and a complimentary
toothbrush on your way out.

391
00:17:05,150 --> 00:17:07,785
Um, when did
that happen?

392
00:17:07,786 --> 00:17:08,869
I perked up
around 4 A.M.,

393
00:17:08,870 --> 00:17:11,622
walked across the hall,
and knocked on Duane's door.

394
00:17:11,623 --> 00:17:13,115
That was not Duane.

395
00:17:13,116 --> 00:17:14,959
I know. Duane moved out
2 weeks ago,

396
00:17:14,960 --> 00:17:17,244
but I have to say,
I think we traded up.

397
00:17:17,245 --> 00:17:21,165
All right, I need your advice
on a decision I have to make.

398
00:17:21,166 --> 00:17:22,883
I bought these boots,
but I also

399
00:17:22,884 --> 00:17:24,001
bought a computer
for needy kids,

400
00:17:24,002 --> 00:17:26,671
and I can't afford them both.
What do I do?

401
00:17:26,672 --> 00:17:30,198
Oh, I already told
the kids about the computer.

402
00:17:30,199 --> 00:17:32,543
I couldn't hold it in.
I'm sorry, Chelsea.

403
00:17:32,544 --> 00:17:33,877
Oh, it's OK.

404
00:17:33,878 --> 00:17:37,014
I know what
I have to do.

405
00:17:39,067 --> 00:17:42,144
God, this is the saddest I've ever
been pulling down a zipper.

406
00:17:49,611 --> 00:17:51,662
[Applause]

407
00:17:52,904 --> 00:17:55,115
You've gotta
feel good.

408
00:17:55,116 --> 00:17:57,168
Yeah. It does feel good.

409
00:17:57,169 --> 00:17:59,661
And it didn't even
require batteries.

410
00:18:03,792 --> 00:18:05,909
- You bought us the computer?
- Yeah.

411
00:18:05,910 --> 00:18:07,837
Aren't you worried
I'll steal it?

412
00:18:07,838 --> 00:18:11,006
I'm sorry. I got that
one completely wrong.

413
00:18:11,007 --> 00:18:13,967
Don't feel that bad,
'cause I was going to steal it.

414
00:18:13,968 --> 00:18:16,308
Really?

415
00:18:16,309 --> 00:18:18,847
Nah. I just didn't want
you to feel that bad.

416
00:18:18,848 --> 00:18:20,615
Really?

417
00:18:20,616 --> 00:18:22,268
Nah. I was
gonna steal it.

418
00:18:22,269 --> 00:18:24,353
I'm just
messing with you,

419
00:18:24,354 --> 00:18:26,383
but thanks for the computer.
You're welcome.

420
00:18:26,384 --> 00:18:27,899
You know how you
could pay me back?

421
00:18:27,900 --> 00:18:29,950
By hooking you up with
some of the insane chronic

422
00:18:29,951 --> 00:18:32,776
my cousin grows
by this garage?

423
00:18:32,777 --> 00:18:35,882
Well, I was gonna say
by reading a book,

424
00:18:35,883 --> 00:18:38,660
but if something were to
just appear in my purse...

425
00:18:38,661 --> 00:18:40,732
Hey, Chelsea...
I was kidding.

426
00:18:42,120 --> 00:18:44,186
Good news. We just
got a new volunteer,

427
00:18:44,187 --> 00:18:45,633
so since you were
so nice and generous,

428
00:18:45,634 --> 00:18:46,633
I'm gonna sign
your paper

429
00:18:46,634 --> 00:18:48,335
so you don't
have to come back.

430
00:18:48,336 --> 00:18:50,587
Oh. Thanks, Dee Dee.

431
00:18:50,588 --> 00:18:52,807
Absolutely.
You've gone above and beyond.

432
00:18:52,808 --> 00:18:55,133
Great.

433
00:18:57,260 --> 00:19:00,681
Ahem. You know, I think I might
hang out for a little bit.

434
00:19:00,682 --> 00:19:01,982
I think... I think
Warren needs me.

435
00:19:01,983 --> 00:19:03,851
Yeah?
Yeah, look at him.

436
00:19:03,852 --> 00:19:05,118
He's typing away.

437
00:19:05,119 --> 00:19:07,655
He's probably
looking up porn.

438
00:19:07,656 --> 00:19:09,711
You know someone's
gotta teach him

439
00:19:09,712 --> 00:19:10,899
how to clear
the browser history.

440
00:19:18,240 --> 00:19:20,205
All right, you guys.

441
00:19:20,206 --> 00:19:22,675
Taste the bacontini.

442
00:19:22,676 --> 00:19:24,343
Ooh.

443
00:19:27,681 --> 00:19:29,251
Oh, that is fantti

444
00:19:29,252 --> 00:19:30,377
yeah?

445
00:19:30,378 --> 00:19:33,268
You know, gin usually
make me mean,

446
00:19:33,269 --> 00:19:36,838
but the fat has
a calming effect.

447
00:19:36,839 --> 00:19:37,806
Good. Good.

448
00:19:37,807 --> 00:19:40,008
You know, I'm gonna enter
the greater New York

449
00:19:40,009 --> 00:19:41,843
event next week.

450
00:19:41,844 --> 00:19:44,813
Good luck,
you son of a bitch!

451
00:19:51,905 --> 00:19:56,041
Oh, hey. Uh, I heard you did
something nice for some kids today.

452
00:19:56,042 --> 00:19:57,159
[Scoffs]
Yeah, don't remind me.

453
00:19:57,160 --> 00:20:00,329
Yeah, well, I wanted to
do something nice for you.

454
00:20:00,330 --> 00:20:03,332
Oh, my God.
You bought me those boots?

455
00:20:03,333 --> 00:20:04,883
Open it up.

456
00:20:09,705 --> 00:20:12,758
[Laughs] Man!

457
00:20:18,481 --> 00:20:24,103
So this is the future
inventor of the bacontini.

458
00:20:24,104 --> 00:20:26,343
Yeah, I liked bacon
back then, too.

459
00:20:28,896 --> 00:20:31,772
<font color="#00ffff">Sync & corrections by Rafael UPD</font>
<font color="#00ffff">www.Addic7ed.Com/</font>

