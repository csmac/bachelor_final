1
00:00:01,766 --> 00:00:03,166
We just moved in

2
00:00:10,266 --> 00:00:12,033
Chanyeol

3
00:00:18,966 --> 00:00:20,9
Moon Gayoung

4
00:00:26,366 --> 00:00:27,733
D.O.

5
00:00:32,300 --> 00:00:33,466
Baekhyun

6
00:00:34,433 --> 00:00:36,000
Sehun

7
00:00:36,0 --> 00:00:38,9
Lay, Xiumin, Chen, Tao, Kai, Suho

8
00:00:40,666 --> 00:00:43,666
EXO Next Door

9
00:00:45,566 --> 00:00:47,94
[Episode 7]

10
00:00:50,033 --> 00:00:51,5
I said, what are you doing?

11
00:00:52,733 --> 00:00:56,5
I think you got
the wrong impression.

12
00:00:57,233 --> 00:00:60,033
That's right.
It's a misunderstanding.

13
00:00:60,033 --> 00:00:61,133
Didn't he make you cry?

14
00:00:61,133 --> 00:00:62,633
No, never!

15
00:00:63,3 --> 00:00:64,466
Tell him, Yeonhee.

16
00:00:66,566 --> 00:00:67,966
Yeah.

17
00:00:67,966 --> 00:00:71,666
It's been a long time and
I was just happy to see him.

18
00:00:71,666 --> 00:00:72,3
What?

19
00:00:73,3 --> 00:00:74,366
You know each other?

20
00:00:75,8 --> 00:00:78,233
You sure you're glad to see him?

21
00:00:79,9 --> 00:00:82,633
If you're done saying hello,
why don't you get going?

22
00:00:82,633 --> 00:00:83,433
What?

23
00:00:84,166 --> 00:00:86,266
Yes, of course.
I was leaving.

24
00:00:86,266 --> 00:00:88,733
It was nice to see you.
I'll send you a Line message.

25
00:00:88,733 --> 00:00:90,633
Goodbye. Goodbye.

26
00:00:96,166 --> 00:00:98,7
Yes. It's a 100% true story.

27
00:00:99,8 --> 00:00:101,833
Yes!

28
00:00:101,833 --> 00:00:103,233
I was totally defenseless.

29
00:00:104,4 --> 00:00:106,533
And they came out of nowhere.

30
00:00:110,633 --> 00:00:112,733
Something doesn't feel right.

31
00:00:112,733 --> 00:00:116,7
Do you think that guy was
taking Incheon Gal's money?

32
00:00:117,1 --> 00:00:119,466
Nah, they are too old for that.

33
00:00:119,466 --> 00:00:120,466
They know each other.

34
00:00:121,1 --> 00:00:123,333
I find it odd, also.

35
00:00:124,2 --> 00:00:126,0
He calls Incheon Gal by her name.

36
00:00:126,0 --> 00:00:129,433
And she was so happy to see him
that she cried?

37
00:00:130,633 --> 00:00:131,566
In that case...

38
00:00:133,166 --> 00:00:135,1
Maybe it's her ex.

39
00:00:139,533 --> 00:00:141,133
What? Cho Minhwan?

40
00:00:141,133 --> 00:00:142,266
Your first love?

41
00:00:143,4 --> 00:00:145,266
What first love?

42
00:00:145,266 --> 00:00:148,566
He's just a dark part of
my dull past.

43
00:00:148,566 --> 00:00:150,1
EXO played a part in

44
00:00:150,1 --> 00:00:154,066
shining a ray of light
into your dark love life.

45
00:00:154,066 --> 00:00:154,766
What?

46
00:00:155,666 --> 00:00:157,533
What on earth are you
talking about?

47
00:00:159,4 --> 00:00:161,666
A guy meets a girl who used to be
insignificant to him.

48
00:00:162,5 --> 00:00:167,6
And the most amazing guys
in the world are now protecting her.

49
00:00:167,6 --> 00:00:168,766
Question.

50
00:00:168,766 --> 00:00:171,6
What's the first thought
that crosses his mind?

51
00:00:172,933 --> 00:00:174,6
Drum rolls...

52
00:00:174,6 --> 00:00:176,8
She used to like me!

53
00:00:177,233 --> 00:00:178,0
So what?

54
00:00:178,666 --> 00:00:180,433
That was a long time ago.

55
00:00:181,3 --> 00:00:182,966
She used to be mine.

56
00:00:182,966 --> 00:00:184,833
He wants to mark his territory.

57
00:00:184,833 --> 00:00:189,433
He starts to think he can defeat
all the other incredible guys.

58
00:00:190,166 --> 00:00:194,566
Cho Minhwan will call you
within three days.

59
00:00:194,566 --> 00:00:196,2
I bet my pinky on it.

60
00:00:198,533 --> 00:00:202,266
Nah, no way.

61
00:00:214,433 --> 00:00:215,966
He won't call me.

62
00:00:217,366 --> 00:00:218,933
Incheon Gal!

63
00:00:218,933 --> 00:00:221,566
Yes. I'm coming!

64
00:00:227,066 --> 00:00:230,3
By the way,
how old is your brother?

65
00:00:230,3 --> 00:00:231,066
He's a high school freshman.

66
00:00:232,1 --> 00:00:233,5
That means...

67
00:00:234,7 --> 00:00:236,433
What? A high school student?

68
00:00:238,166 --> 00:00:239,366
Busted.

69
00:00:240,3 --> 00:00:244,466
You sneaky thing. You hid the fact
that you're in high school.

70
00:00:249,4 --> 00:00:253,366
You hid the fact that
you've got supernatural powers.

71
00:00:253,366 --> 00:00:255,133
Supernatural powers?

72
00:00:255,133 --> 00:00:256,366
Why did you approach me?

73
00:00:257,233 --> 00:00:258,3
What do you want?

74
00:00:259,533 --> 00:00:263,133
Your millstone drip coffee,
I can't forget it.

75
00:00:264,4 --> 00:00:266,666
Plus, we used to be friends.

76
00:00:270,633 --> 00:00:273,633
I can make coffee for you
any time.

77
00:00:274,866 --> 00:00:275,933
Just promise me one thing.

78
00:00:277,266 --> 00:00:281,266
Do not use your super power
when you're with me.

79
00:00:282,3 --> 00:00:285,933
That endless wackiness,
I'm addicted.

80
00:00:286,8 --> 00:00:288,6
Alright. Are we good now?

81
00:00:302,066 --> 00:00:304,2
That's your style?

82
00:00:306,033 --> 00:00:306,866
Hey, friend.

83
00:00:308,1 --> 00:00:311,066
Can I ask you
something about your sister?

84
00:00:311,866 --> 00:00:313,533
Sure. Ask away.

85
00:00:326,766 --> 00:00:331,166
He's not her ex,
He's her first love!

86
00:00:332,3 --> 00:00:334,866
Don't they say
you never forget your first love?

87
00:00:335,866 --> 00:00:337,5
So, that's his first love?

88
00:00:346,466 --> 00:00:348,9
Do you need something?

89
00:00:348,9 --> 00:00:350,733
Can't you clean better?

90
00:00:355,7 --> 00:00:357,266
I didn't clean there yet!

91
00:00:358,833 --> 00:00:360,533
Why are you so distracted?

92
00:00:361,4 --> 00:00:362,833
Thinking about your first love?

93
00:00:373,366 --> 00:00:375,166
These are girl's lines.

94
00:00:375,766 --> 00:00:377,566
I know. Try to act.

95
00:00:378,2 --> 00:00:380,733
Oh, Incheon Gal.
Come here.

96
00:00:383,1 --> 00:00:388,5
This is a job for the only girl
in our house.

97
00:00:389,366 --> 00:00:390,966
Our? Girl?

98
00:00:393,5 --> 00:00:394,966
What is this?

99
00:00:394,966 --> 00:00:399,733
It's my script.
I need someone to read it with me.

100
00:00:399,733 --> 00:00:401,7
I'd rather watch than participate.

101
00:00:404,966 --> 00:00:406,366
Do you have time?

102
00:00:407,033 --> 00:00:408,9
It will mean a lot to me
if you help me.

103
00:00:410,933 --> 00:00:415,766
Sure, I'll do it.
Where do I read?

104
00:00:416,8 --> 00:00:418,6
Start reading from here.

105
00:00:424,233 --> 00:00:426,2
[Comes close and hugs her]

106
00:00:426,2 --> 00:00:428,533
[Kisses her eyes, nose, and mouth]

107
00:00:432,966 --> 00:00:437,933
I can't look at your face.

108
00:00:441,533 --> 00:00:443,033
Be true to your feelings.

109
00:00:445,233 --> 00:00:446,133
Look at me.

110
00:00:447,833 --> 00:00:449,033
Look into my eyes.

111
00:00:453,566 --> 00:00:455,5
I can't.

112
00:00:455,5 --> 00:00:458,6
Can't you see the instructions
written in the parentheses?

113
00:00:460,7 --> 00:00:464,566
You have to act it out
so D.O. can get into character.

114
00:00:464,566 --> 00:00:466,566
D.O., start from your last line.

115
00:00:466,566 --> 00:00:469,366
Camera!
Ready, action.

116
00:00:473,1 --> 00:00:473,966
Look at me.

117
00:00:476,566 --> 00:00:477,566
Look into my eyes.

118
00:00:480,466 --> 00:00:484,833
When I look into your eyes,
I'm finally at peace.

119
00:08:41,01 --> 00:08:44,066
Ah, I've got to tell the others.

120
00:08:46,066 --> 00:08:50,7
Alright.
I'll tell Xiumin first.

121
00:08:59,866 --> 00:09:00,500
'Look at me.'

122
00:09:01,066 --> 00:09:02,033
'Look into my eyes.'

123
00:09:02,566 --> 00:09:05,900
D.O. read those lines
with a longing look.

124
00:09:05,900 --> 00:09:07,900
And Incheon Gal's reaction was...

125
00:09:07,900 --> 00:09:08,700
Was?

126
00:09:08,700 --> 00:09:11,700
I don't know
how to put in into words.

127
00:09:11,700 --> 00:09:14,333
Come on.
You got me all curious.

128
00:09:14,333 --> 00:09:17,133
And what about Chanyeol?

129
00:09:17,133 --> 00:09:20,466
He calls Incheon Gal
all day long.

130
00:09:20,466 --> 00:09:24,300
He says that name
dozens of times a day.

131
00:09:25,333 --> 00:09:28,966
I think he's just
making her life miserable.

132
00:09:28,966 --> 00:09:31,400
I thought so too.
But I was wrong.

133
00:09:31,400 --> 00:09:36,000
I have definite proof that
Chanyeol likes Incheon Gal.

134
00:09:37,766 --> 00:09:39,300
- Definite proof?
- Yup.

135
00:09:39,733--> 00:09:40,600
What is that?

136
00:09:42,100 --> 00:09:45,100
Are they in a love triangle?

137
00:09:45,100 --> 00:09:48,200
It's too long to explain
over the phone.

138
00:09:48,200 --> 00:09:50,300
Right.
This is an international call.

140
00:09:50,300 --> 00:09:52,666
I really want to know, though.

141
00:09:52,666 --> 00:09:56,566
Don't worry.
This call is completely free.

142
00:09:56,566 --> 00:09:57,600
Didn't you know that?

143
00:09:58,833 --> 00:10:01,233
We can talk all night long.

144
00:10:01,233 --> 00:10:03,100
Can you talk all night, too?

145
00:10:03,100 --> 00:10:07,533
Sure! Tell me everything.
Don't you dare leave anything out.

146
00:10:07,533 --> 00:10:11,566
Alright.
Let me go into detail, then.

147
00:10:12,433 --> 00:10:13,500
Oh man...

148
00:10:24,0 --> 00:10:26,066
I heard it with my own ears.

149
00:10:26,066 --> 00:10:26,766
Really?

150
00:10:27,333 --> 00:10:28,133
[Incheon Gal]

151
00:10:28,133 --> 00:10:29,7
What do you want now?

152
00:10:29,7 --> 00:10:30,5
Yes?

153
00:10:32,233 --> 00:10:34,266
You have to change your style.

154
00:10:34,266 --> 00:10:35,933
You're going to play hard to get.

155
00:10:35,933 --> 00:10:37,433
It's not you. I guarantee it!

156
00:10:37,433 --> 00:10:37,966
What about...

157
00:10:37,966 --> 00:10:40,366
You're saying
Incheon Gal likes one of us?

158
00:10:40,366 --> 00:10:43,533
It's natural that even thinking
about it makes you blush.

159
00:10:43,533 --> 00:10:44,966
That's not surprising at all.

160
00:10:44,966 --> 00:10:46,566
You're holding it upside down.

161
00:10:47,733 --> 00:10:51,133
The EXO member
she really likes is...

162
00:10:52,5 --> 00:10:55,566
Real love?
