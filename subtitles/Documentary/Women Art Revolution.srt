﻿1
00:00:00,000 --> 00:00:01,400
(The following content may not be suitable for viewers under 15.)

2
00:00:01,400 --> 00:00:02,900
(Viewer discretion is advised.)

3
00:00:04,444 --> 00:00:07,014
(Grand Performance Hall)

4
00:00:08,807 --> 00:00:10,447
Why did it have to be Kang Ji Woo?

5
00:00:11,077 --> 00:00:12,907
I worked so hard to be free of her.

6
00:00:13,207 --> 00:00:14,207
Why...

7
00:00:18,477 --> 00:00:19,577
(Episode 2)

8
00:00:19,577 --> 00:00:22,047
Oh my. Is it raining out?

9
00:00:22,047 --> 00:00:23,807
You're soaked.

10
00:00:25,677 --> 00:00:27,807
Can you take a look at something?

11
00:00:29,277 --> 00:00:32,047
That brat. Gosh.

12
00:00:33,607 --> 00:00:34,677
Hello?

13
00:00:34,947 --> 00:00:36,177
Hi, it's me.

14
00:00:36,307 --> 00:00:37,977
Hi, Ji Yoo.

15
00:00:38,277 --> 00:00:40,747
It's late. Why aren't you home?

16
00:00:41,477 --> 00:00:42,947
It's raining out.

17
00:00:43,047 --> 00:00:44,677
Should I bring you an umbrella?

18
00:00:44,677 --> 00:00:47,747
No. But it's Soon Bok's birthday today.

19
00:00:47,807 --> 00:00:48,807
What?

20
00:00:49,377 --> 00:00:51,877
My gosh. Where's my head?

21
00:00:52,177 --> 00:00:55,877
This old hag forgot her own daughter's birthday.

22
00:00:56,277 --> 00:00:58,107
If it weren't for you,

23
00:00:58,377 --> 00:01:01,377
Soon Bok would've had my head.

24
00:01:01,377 --> 00:01:04,247
Our friends and I want to throw her a surprise party.

25
00:01:04,477 --> 00:01:08,477
Would you tell her to bring me an umbrella?

26
00:01:08,947 --> 00:01:11,007
Keep the party a secret.

27
00:01:11,507 --> 00:01:14,307
Okay. I get it.

28
00:01:15,647 --> 00:01:18,247
- Why is she so late? - I know.

29
00:01:19,277 --> 00:01:20,777
She's coming, right?

30
00:01:24,407 --> 00:01:25,747
Where's Soon Bok?

31
00:01:26,847 --> 00:01:28,407
I'm starving.

32
00:01:30,147 --> 00:01:31,677
She should be here soon.

33
00:01:31,877 --> 00:01:33,347
Let's just eat first.

34
00:01:34,177 --> 00:01:35,877
Yes. She must not be coming.

35
00:01:36,947 --> 00:01:39,847
Then let's get it going and go home.

36
00:01:40,577 --> 00:01:44,007
We're here for Soon Bok's birthday. We should celebrate with her.

37
00:01:44,707 --> 00:01:45,707
Come on.

38
00:01:46,807 --> 00:01:47,807
Okay.

39
00:01:52,347 --> 00:01:55,207
It's always, "Ji Yoo" this and "Ji Yoo" that.

40
00:01:56,047 --> 00:01:58,447
Whose mum is she anyway?

41
00:03:27,047 --> 00:03:28,807
After all I did to get here.

42
00:03:30,847 --> 00:03:32,307
I can't go back to being Hong Soon Bok...

43
00:03:33,447 --> 00:03:35,247
because of her again.

44
00:04:31,207 --> 00:04:32,477
- Over there. - Okay.

45
00:04:36,407 --> 00:04:37,677
Hold on, please.

46
00:04:38,907 --> 00:04:39,977
Sorry.

47
00:04:40,747 --> 00:04:42,477
Sorry. That way.

48
00:04:43,477 --> 00:04:45,177
Sorry. Sorry.

49
00:04:46,107 --> 00:04:47,107
How embarrassing.

50
00:04:47,577 --> 00:04:48,777
You're sweating.

51
00:04:48,847 --> 00:04:49,907
- Are you okay? - Yes.

52
00:05:32,007 --> 00:05:33,007
Hello?

53
00:05:34,777 --> 00:05:35,777
Hello?

54
00:05:36,147 --> 00:05:37,947
Speak already.

55
00:05:38,947 --> 00:05:41,547
Who makes crank calls at this hour?

56
00:05:42,377 --> 00:05:43,877
If you won't talk, I'm hanging up.

57
00:05:50,607 --> 00:05:53,177
Why are they making crank calls?

58
00:05:54,877 --> 00:05:56,277
- I brought some fruit. - Okay.

59
00:05:56,277 --> 00:05:58,007
- Please have some. - What about Ji Yoo?

60
00:05:58,607 --> 00:06:01,607
A friend from the ballet is throwing a party at her home.

61
00:06:02,007 --> 00:06:03,807
She said she'll come home tomorrow.

62
00:06:04,307 --> 00:06:05,407
She's staying over?

63
00:06:05,407 --> 00:06:07,147
Saying she's sleeping at a friend's house...

64
00:06:07,447 --> 00:06:10,077
is the lie you tell so that you can stay out.

65
00:06:10,077 --> 00:06:13,007
Come on now. I've dated too, you know.

66
00:06:13,007 --> 00:06:14,307
Ji Yoo isn't you.

67
00:06:15,077 --> 00:06:18,377
Why are you accusing an innocent girl?

68
00:06:18,507 --> 00:06:22,077
Our Ji Yoo is smart, so don't you worry.

69
00:06:36,947 --> 00:06:37,947
What is it?

70
00:06:39,377 --> 00:06:40,377
I...

71
00:06:41,877 --> 00:06:43,407
I don't think I can do it.

72
00:06:44,807 --> 00:06:45,807
What?

73
00:06:47,247 --> 00:06:48,707
Yoo Kang Woo's girlfriend...

74
00:06:49,447 --> 00:06:51,407
is someone I know.

75
00:06:52,247 --> 00:06:53,247
So?

76
00:06:55,347 --> 00:06:56,347
I don't...

77
00:06:56,947 --> 00:06:58,877
I don't want to have anything to do with her again.

78
00:06:58,877 --> 00:07:00,877
Ever. Never again.

79
00:07:01,107 --> 00:07:02,107
Really?

80
00:07:02,777 --> 00:07:06,207
I guess you want to return to the sewer.

81
00:07:13,907 --> 00:07:15,177
It's pancreatic cancer.

82
00:07:16,177 --> 00:07:17,177
Sorry?

83
00:07:17,847 --> 00:07:20,677
But the cancer has spread...

84
00:07:20,907 --> 00:07:22,477
to other organs.

85
00:07:27,547 --> 00:07:28,907
You should go...

86
00:07:29,347 --> 00:07:32,207
to a big hospital to get a thorough test.

87
00:07:40,407 --> 00:07:42,077
That was so much fun.

88
00:07:44,247 --> 00:07:45,977
Hold on.

89
00:07:47,807 --> 00:07:49,747
Hello. Take a look.

90
00:07:50,907 --> 00:07:51,977
Do you like anything?

91
00:07:57,547 --> 00:07:58,647
How's this, Kang Woo?

92
00:08:01,347 --> 00:08:02,347
Is it for me?

93
00:08:06,047 --> 00:08:08,807
Ta-da. Your time is...

94
00:08:09,007 --> 00:08:10,047
all mine now.

95
00:08:20,647 --> 00:08:23,007
Kang Woo. Do you know what this is?

96
00:08:23,107 --> 00:08:24,107
Isn't that a bracelet?

97
00:08:25,147 --> 00:08:26,877
It isn't any bracelet.

98
00:08:27,147 --> 00:08:29,077
It's a bracelet that holds onto time.

99
00:08:32,077 --> 00:08:35,607
If you receive flowers, you put a flower on it.

100
00:08:36,047 --> 00:08:37,877
If you go to Paris on your honeymoon,

101
00:08:38,547 --> 00:08:40,547
you put the Eiffel Tower on.

102
00:08:42,047 --> 00:08:43,447
Looking at this,

103
00:08:43,447 --> 00:08:46,747
you remember the happy moments...

104
00:08:47,577 --> 00:08:48,607
since...

105
00:08:49,477 --> 00:08:51,047
you may forget with time.

106
00:08:54,377 --> 00:08:55,377
Let me see that.

107
00:09:26,107 --> 00:09:27,207
Give me your hand.

108
00:09:28,177 --> 00:09:29,177
Ta-da.

109
00:09:35,147 --> 00:09:36,547
I will fill this...

110
00:09:38,107 --> 00:09:39,107
with happiness.

111
00:09:41,777 --> 00:09:42,777
Just a minute.

112
00:09:46,877 --> 00:09:48,477
Surprise!

113
00:09:48,847 --> 00:09:50,307
First, the sand from here.

114
00:09:56,707 --> 00:09:57,707
It's pretty.

115
00:09:58,447 --> 00:09:59,547
It's pretty.

116
00:09:59,777 --> 00:10:01,107
Let's always remember...

117
00:10:02,447 --> 00:10:03,747
our promise.

118
00:10:07,647 --> 00:10:08,647
Please be...

119
00:10:12,277 --> 00:10:13,417
mine forever,

120
00:10:14,477 --> 00:10:15,477
Kang Ji Yoo.

121
00:10:24,447 --> 00:10:25,447
Surprise.

122
00:10:32,477 --> 00:10:34,147
I will always be...

123
00:10:35,247 --> 00:10:36,917
with you in all your times.

124
00:11:00,507 --> 00:11:01,507
We're ready.

125
00:11:01,847 --> 00:11:04,077
Okay. Bride and groom. Please look this way.

126
00:11:04,107 --> 00:11:05,107
- Okay. - Just one second.

127
00:11:18,547 --> 00:11:22,107
We walked this path so often when you were young. Remember?

128
00:11:22,747 --> 00:11:25,007
Of course. How could I forget?

129
00:11:25,777 --> 00:11:28,047
You'd carry me on your back...

130
00:11:28,047 --> 00:11:29,807
and go door to door,

131
00:11:30,247 --> 00:11:33,107
going, "Fresh fish for sale. Fresh fish for sale."

132
00:11:33,877 --> 00:11:37,077
So many times, I wasn't sure if it was your scent or the fish.

133
00:11:38,907 --> 00:11:41,577
My boy who wouldn't leave my back...

134
00:11:41,807 --> 00:11:43,377
has grown so big now.

135
00:11:48,647 --> 00:11:50,447
Mum, you get on my back now.

136
00:11:51,007 --> 00:11:52,507
No.

137
00:11:52,507 --> 00:11:54,977
Come on, get on. I'll carry you now.

138
00:11:55,607 --> 00:11:56,607
Come on.

139
00:12:02,877 --> 00:12:04,507
Oh my. Why are you so light?

140
00:12:05,047 --> 00:12:06,047
Were you dieting?

141
00:12:10,877 --> 00:12:17,247
My sweet love, did you forget me

142
00:12:18,247 --> 00:12:23,177
Did you forget me already

143
00:12:37,347 --> 00:12:38,347
Sun Ho.

144
00:12:38,977 --> 00:12:40,047
There's nothing more...

145
00:12:40,477 --> 00:12:42,547
that I could possibly want.

146
00:12:43,477 --> 00:12:45,007
Why not?

147
00:12:45,747 --> 00:12:47,677
You have to see me get married,

148
00:12:47,907 --> 00:12:50,177
have kids and live happily.

149
00:12:50,947 --> 00:12:52,607
Do you have someone you want to marry?

150
00:12:52,947 --> 00:12:54,977
Well... I guess you can say that.

151
00:12:55,477 --> 00:12:57,547
Really? Who is she?

152
00:12:58,147 --> 00:12:59,777
A ballet queen at my school.

153
00:13:00,407 --> 00:13:01,507
A ballet queen?

154
00:13:02,107 --> 00:13:04,477
Are you dating someone that amazing?

155
00:13:05,077 --> 00:13:06,077
What?

156
00:13:07,047 --> 00:13:09,907
Well, we're not dating exactly.

157
00:13:10,647 --> 00:13:11,747
I'm going to tell her soon.

158
00:13:12,207 --> 00:13:13,577
I practiced all year round.

159
00:13:14,147 --> 00:13:15,547
What?

160
00:13:15,777 --> 00:13:17,307
You haven't even told her how you feel?

161
00:13:17,977 --> 00:13:20,107
You should tell her right away.

162
00:13:20,547 --> 00:13:23,547
Love doesn't have legs, so it can't go to her.

163
00:13:23,947 --> 00:13:25,777
She won't know until you tell her.

164
00:13:26,047 --> 00:13:27,047
I know.

165
00:13:27,447 --> 00:13:29,407
You know I'm stupidly brave.

166
00:13:30,577 --> 00:13:34,077
But I'm a coward in front of her.

167
00:13:35,107 --> 00:13:37,047
It's so frustrating.

168
00:13:39,307 --> 00:13:42,607
My son must really like her a lot.

169
00:13:45,477 --> 00:13:47,877
So you have to get healthy quick.

170
00:13:48,707 --> 00:13:50,847
Then I'll bring her...

171
00:13:51,247 --> 00:13:52,507
and introduce her to you.

172
00:13:53,107 --> 00:13:54,107
Okay?

173
00:14:05,377 --> 00:14:06,377
Just a minute.

174
00:14:09,777 --> 00:14:12,377
Let me beat dust out of this blanket.

175
00:14:55,577 --> 00:14:56,577
I'm glad...

176
00:14:57,147 --> 00:14:59,577
it's nice and warm.

177
00:15:00,207 --> 00:15:01,207
Right?

178
00:15:04,407 --> 00:15:05,407
I...

179
00:15:05,977 --> 00:15:08,177
should've brought you somewhere nice,

180
00:15:08,847 --> 00:15:11,407
but these rooms all they have in this area.

181
00:15:14,047 --> 00:15:15,107
I like it.

182
00:15:15,607 --> 00:15:17,077
- Really? - Yes.

183
00:15:17,707 --> 00:15:19,377
As long as I'm with you,

184
00:15:20,207 --> 00:15:21,377
I don't care where I am.

185
00:15:51,807 --> 00:15:55,507
I guess you want to return to the sewer.

186
00:16:02,507 --> 00:16:04,577
What time will it be...

187
00:16:04,907 --> 00:16:07,147
if we get back...

188
00:16:07,847 --> 00:16:08,877
Excuse me.

189
00:16:16,047 --> 00:16:17,047
It's him, right?

190
00:16:17,147 --> 00:16:19,047
The student president that you like.

191
00:16:19,647 --> 00:16:21,047
I didn't say I like him.

192
00:16:21,107 --> 00:16:23,477
I just said he's the best.

193
00:16:26,007 --> 00:16:27,477
Do you have some time?

194
00:16:28,847 --> 00:16:30,377
He must be interested in you too.

195
00:16:31,477 --> 00:16:32,477
Go on.

196
00:16:47,347 --> 00:16:48,807
What is this?

197
00:16:49,507 --> 00:16:50,547
That's not for you.

198
00:16:52,777 --> 00:16:55,707
I'm too shy to give it to her myself. Give it to her for me.

199
00:16:56,247 --> 00:16:57,577
I heard you live with Ji Yoo.

200
00:16:58,677 --> 00:17:01,347
The kids said I can just ask you.

201
00:17:04,347 --> 00:17:06,747
Hey. What are you doing?

202
00:17:07,147 --> 00:17:08,907
Give it to her yourself!

203
00:17:09,007 --> 00:17:10,477
Don't bother me!

204
00:17:21,407 --> 00:17:22,707
If I can't go back,

205
00:17:23,407 --> 00:17:24,647
I'll have to face it head-on.

206
00:17:26,347 --> 00:17:27,347
Fine.

207
00:17:28,247 --> 00:17:29,247
Let's do it.

208
00:17:30,977 --> 00:17:33,207
I am no longer Hong Soon Bok,

209
00:17:34,207 --> 00:17:35,377
but Chae Seo Rin.

210
00:17:37,747 --> 00:17:38,747
Kang Ji Yoo,

211
00:17:39,907 --> 00:17:40,947
from now on,

212
00:17:42,747 --> 00:17:44,447
I'll take from you.

213
00:18:00,577 --> 00:18:01,577
It's me.

214
00:18:02,677 --> 00:18:05,407
I want to meet Yoo Kang Woo again.

215
00:18:06,807 --> 00:18:07,977
Where is the next location?

216
00:19:02,777 --> 00:19:03,777
How nice.

217
00:19:04,577 --> 00:19:05,747
One more time.

218
00:19:06,117 --> 00:19:08,407
No encores, please.

219
00:19:11,007 --> 00:19:13,177
We need to sleep early to go to Seoul tomorrow.

220
00:19:13,177 --> 00:19:15,147
Wait. Just a minute.

221
00:19:19,547 --> 00:19:20,547
Hold on.

222
00:19:29,277 --> 00:19:30,277
What is that?

223
00:19:31,617 --> 00:19:32,907
Your dad...

224
00:19:33,847 --> 00:19:36,077
gave this to me when he proposed.

225
00:19:38,007 --> 00:19:39,007
I'd like...

226
00:19:39,847 --> 00:19:41,677
you to hold onto this.

227
00:19:44,547 --> 00:19:45,547
Okay?

228
00:19:46,477 --> 00:19:48,077
I don't need it.

229
00:19:49,377 --> 00:19:50,947
Whenever you think of me,

230
00:19:51,847 --> 00:19:54,207
you should look at it.

231
00:19:57,147 --> 00:19:58,277
Go to sleep.

232
00:19:59,277 --> 00:20:00,677
We're leaving early in the morning.

233
00:20:18,307 --> 00:20:19,647
Dad? I have no such thing.

234
00:20:32,077 --> 00:20:33,077
What about Kang Woo?

235
00:20:33,677 --> 00:20:35,947
He's hanging out with friends and isn't coming home.

236
00:20:36,877 --> 00:20:37,877
What?

237
00:20:38,677 --> 00:20:40,477
He's still young.

238
00:20:40,877 --> 00:20:42,477
Friends are important at that age.

239
00:20:42,477 --> 00:20:43,777
Whatever.

240
00:20:47,307 --> 00:20:49,177
What's going on with the acquisition?

241
00:20:49,477 --> 00:20:52,847
Well, there is one plot that we haven't acquired yet.

242
00:20:53,177 --> 00:20:55,047
The owner refuses to sell...

243
00:20:55,547 --> 00:20:56,947
and is being stubborn.

244
00:20:57,477 --> 00:20:59,907
You're dragging your heels too much. It isn't like you.

245
00:21:01,177 --> 00:21:02,277
I'll rush it.

246
00:21:02,947 --> 00:21:03,947
Leave that...

247
00:21:04,707 --> 00:21:05,877
to Kang Woo.

248
00:21:06,077 --> 00:21:07,077
Dad.

249
00:21:07,507 --> 00:21:09,377
He's too young.

250
00:21:09,447 --> 00:21:10,907
He just got discharged from the army...

251
00:21:10,907 --> 00:21:13,507
and barely started working. We can't leave it to him.

252
00:21:13,777 --> 00:21:14,907
I want to test Kang Woo...

253
00:21:15,707 --> 00:21:18,147
to see how he does with this.

254
00:21:19,647 --> 00:21:21,077
So do as I say.

255
00:21:21,877 --> 00:21:22,977
Give him...

256
00:21:23,677 --> 00:21:25,677
all the documents tomorrow.

257
00:21:26,747 --> 00:21:27,947
Yes, Sir.

258
00:21:30,747 --> 00:21:31,907
I'm so annoyed.

259
00:21:36,907 --> 00:21:37,907
Gosh.

260
00:21:39,377 --> 00:21:43,277
My dad is so wrong.

261
00:21:43,577 --> 00:21:46,177
He let Kang Woo buy the paintings worth millions,

262
00:21:46,307 --> 00:21:49,077
and now he wants to give him the main business, too.

263
00:21:50,507 --> 00:21:52,647
I'm sure he has the best of intentions.

264
00:21:52,907 --> 00:21:56,347
Exactly. What is his "best of intentions"?

265
00:21:56,347 --> 00:21:57,747
You do all the work.

266
00:21:59,107 --> 00:22:00,407
Don't be too upset.

267
00:22:01,677 --> 00:22:02,677
I'm sorry.

268
00:22:04,577 --> 00:22:06,247
Don't be.

269
00:22:06,447 --> 00:22:08,177
I'm the one who's sorry.

270
00:22:09,107 --> 00:22:10,607
The great Yoo Jang Mi...

271
00:22:12,007 --> 00:22:14,647
is only a general manager's wife.

272
00:22:16,247 --> 00:22:18,607
I didn't marry you for your background.

273
00:22:18,807 --> 00:22:20,307
I married you because I like you.

274
00:22:21,007 --> 00:22:22,047
Just wait a little bit.

275
00:22:22,107 --> 00:22:24,977
I'll pull you up much higher.

276
00:22:25,947 --> 00:22:26,947
It's okay.

277
00:22:27,647 --> 00:22:29,647
Let's take it slowly.

278
00:22:30,347 --> 00:22:33,877
If I keep trying, he'll acknowledge me one day.

279
00:22:35,047 --> 00:22:37,247
How are you such an angel?

280
00:22:37,477 --> 00:22:40,747
It isn't easy to be smart and kind.

281
00:22:41,207 --> 00:22:42,877
I chose the best husband.

282
00:22:55,807 --> 00:22:58,107
I've got you.

283
00:23:03,347 --> 00:23:04,347
Did you do something wrong?

284
00:23:05,307 --> 00:23:08,307
Why are you sneaking in?

285
00:23:09,677 --> 00:23:11,677
Didn't you go out?

286
00:23:11,677 --> 00:23:13,177
How could I go out?

287
00:23:13,477 --> 00:23:14,977
I was so worried.

288
00:23:15,047 --> 00:23:18,377
What if my sister went away with some man?

289
00:23:18,547 --> 00:23:21,577
I was so worried that I couldn't do anything.

290
00:23:21,577 --> 00:23:24,177
What... What are you talking about?

291
00:23:24,177 --> 00:23:26,077
What... What man?

292
00:23:26,177 --> 00:23:28,177
See? You're stuttering.

293
00:23:28,277 --> 00:23:30,547
I must have guessed right.

294
00:23:30,547 --> 00:23:32,277
Stop it.

295
00:23:33,447 --> 00:23:34,447
Are you back?

296
00:23:34,907 --> 00:23:36,707
Yes, Mum.

297
00:23:37,107 --> 00:23:39,047
Mum. I was right.

298
00:23:39,177 --> 00:23:41,077
She went away with a guy.

299
00:23:41,377 --> 00:23:42,977
I did not.

300
00:23:43,577 --> 00:23:46,177
- No, right? - No.

301
00:23:46,777 --> 00:23:47,877
I mean it.

302
00:23:48,277 --> 00:23:50,847
Your dad was worried. Don't stay out so late.

303
00:23:51,407 --> 00:23:52,407
Okay.

304
00:23:57,177 --> 00:23:58,177
I know I'm right.

305
00:23:58,807 --> 00:24:00,207
I think she's lying.

306
00:24:00,647 --> 00:24:02,207
Ji Yoo isn't like you.

307
00:24:02,507 --> 00:24:05,147
Worry about yourself. Don't accuse innocent people.

308
00:24:05,407 --> 00:24:06,907
If you fail the college entrance exam,

309
00:24:07,177 --> 00:24:08,547
I'm kicking you out.

310
00:24:08,907 --> 00:24:11,607
How did I suddenly become the target?

311
00:24:33,177 --> 00:24:34,277
It's the land documents.

312
00:24:34,447 --> 00:24:37,677
The land owner's contact information is there, so you should call.

313
00:24:38,347 --> 00:24:39,347
Thank you.

314
00:24:40,107 --> 00:24:41,577
It won't be easy.

315
00:24:42,107 --> 00:24:45,847
To be honest, I don't want to follow Dad's oppressive way...

316
00:24:45,977 --> 00:24:46,977
of doing business.

317
00:24:48,177 --> 00:24:51,207
Try your own way then instead of the chairman's.

318
00:24:51,777 --> 00:24:53,047
Don't be oppressive.

319
00:24:53,707 --> 00:24:55,877
Be soft and a gentleman.

320
00:25:08,307 --> 00:25:09,307
Hello.

321
00:25:09,577 --> 00:25:11,907
If someone looks for Yoo Kang Woo, please send them to me.

322
00:25:12,307 --> 00:25:13,307
- I will. - Thank you.

323
00:25:16,207 --> 00:25:17,447
(My love Ji Yoo)

324
00:25:18,647 --> 00:25:21,477
- Hi, Ji Yoo. - Are you still working?

325
00:25:21,477 --> 00:25:23,207
Yes. I have a meeting.

326
00:25:23,207 --> 00:25:25,777
Then we can't meet today? I'm sad.

327
00:25:25,847 --> 00:25:27,547
No. I'll be as quick as possible.

328
00:25:28,347 --> 00:25:29,907
I really don't want to do this.

329
00:25:33,147 --> 00:25:34,147
Do you want to come here?

330
00:25:51,977 --> 00:25:54,647
Mosung Group's Yoo Kang Woo?

331
00:25:54,777 --> 00:25:57,977
Then you're the land owner?

332
00:26:00,077 --> 00:26:01,507
Yes, I am.

333
00:26:02,647 --> 00:26:04,447
What a coincidence.

334
00:26:05,277 --> 00:26:06,677
This is our third time, right?

335
00:26:07,507 --> 00:26:09,447
They say three coincidences mean it's fate.

336
00:26:12,947 --> 00:26:14,877
It's nice to meet you. I'm Chae Seo Rin.

337
00:26:15,807 --> 00:26:17,047
Hello. I'm Yoo Kang Woo.

338
00:26:18,607 --> 00:26:19,607
Please.

339
00:26:19,947 --> 00:26:21,047
I'm surprised.

340
00:26:22,047 --> 00:26:24,007
I was expecting a gangster...

341
00:26:24,007 --> 00:26:27,007
or the agent of one.

342
00:26:29,147 --> 00:26:30,707
You were way off.

343
00:26:34,377 --> 00:26:36,507
I was prepared to get beaten up.

344
00:26:36,847 --> 00:26:38,007
I guess I won't be.

345
00:26:38,447 --> 00:26:41,007
You never know, so don't let your guard down.

346
00:26:43,007 --> 00:26:44,677
It belonged to my grandfather.

347
00:26:44,947 --> 00:26:46,747
It was put under my name this year.

348
00:26:47,307 --> 00:26:48,307
I see.

349
00:26:51,407 --> 00:26:53,807
- My love - I'm going out.

350
00:26:54,047 --> 00:26:56,607
Oh my. Oh my.

351
00:26:57,607 --> 00:26:59,077
Are you going on a date?

352
00:26:59,077 --> 00:27:00,407
- Yes. - Oh my.

353
00:27:04,347 --> 00:27:07,277
I wonder what my Soon Bok is doing?

354
00:27:09,147 --> 00:27:10,177
Ma'am.

355
00:27:11,607 --> 00:27:14,577
Every time I see you leave home,

356
00:27:15,307 --> 00:27:18,247
I'm reminded of Soon Bok leaving for school.

357
00:27:19,477 --> 00:27:21,907
I've been looking for her, and it's been four years.

358
00:27:22,207 --> 00:27:24,547
I don't even know if she's dead or alive.

359
00:27:27,347 --> 00:27:30,047
I'm sure she's fine.

360
00:27:30,377 --> 00:27:31,677
I know she'll call.

361
00:27:32,107 --> 00:27:34,707
My gosh. My gosh. I'm sorry.

362
00:27:36,177 --> 00:27:38,947
What am I doing? I'm being silly.

363
00:27:38,947 --> 00:27:40,477
You should go.

364
00:27:42,047 --> 00:27:43,047
Okay.

365
00:27:44,007 --> 00:27:45,907
- I'll see you later. - Okay.

366
00:27:50,807 --> 00:27:54,907
Our company plans to build a shopping mall for the residents.

367
00:27:55,277 --> 00:27:58,847
Once the mall opens, it will create job opportunities and...

368
00:28:05,577 --> 00:28:08,377
How does Yoo Kang Woo treat you?

369
00:28:09,547 --> 00:28:11,077
The girlfriend is too high a wall.

370
00:28:12,747 --> 00:28:13,747
It's iron-clad.

371
00:28:15,547 --> 00:28:17,677
Iron walls are meant to be taken down.

372
00:28:19,507 --> 00:28:20,507
Take this.

373
00:28:22,707 --> 00:28:23,707
What is this?

374
00:28:25,807 --> 00:28:27,407
It'll be a ladder...

375
00:28:27,547 --> 00:28:29,207
that will let you climb past the iron wall.

376
00:28:29,907 --> 00:28:33,047
Yoo Kang Woo will come to you himself very soon.

377
00:28:35,877 --> 00:28:37,177
Let me get to the point.

378
00:28:39,547 --> 00:28:40,547
I will not sell.

379
00:28:42,477 --> 00:28:44,577
I will not sell. Ever.

380
00:28:57,777 --> 00:28:59,907
You told me not to let my guard down, right?

381
00:29:02,247 --> 00:29:03,977
Don't be so sure of yourself either.

382
00:29:05,407 --> 00:29:07,047
As in, you'll make me sell somehow?

383
00:29:08,977 --> 00:29:12,347
Honestly, I don't care whether or not anything gets built there.

384
00:29:13,447 --> 00:29:16,177
I just need to bring my dad the contract.

385
00:29:17,247 --> 00:29:18,607
Is it a gift for your father?

386
00:29:18,877 --> 00:29:20,047
Not really a gift.

387
00:29:21,347 --> 00:29:22,507
It's more like a test paper.

388
00:29:24,207 --> 00:29:25,207
I am...

389
00:29:25,877 --> 00:29:27,177
being tested right now.

390
00:29:31,947 --> 00:29:34,607
Okay, since I was honest first,

391
00:29:35,647 --> 00:29:36,947
it's your turn.

392
00:29:38,207 --> 00:29:39,277
What do you want?

393
00:29:42,777 --> 00:29:45,277
Would you do anything I want?

394
00:29:47,377 --> 00:29:48,377
Tell me.

395
00:29:52,707 --> 00:29:53,807
Let's have a drink.

396
00:30:02,407 --> 00:30:04,207
After having a few drinks together,

397
00:30:04,507 --> 00:30:06,347
I may be able to tell you...

398
00:30:07,447 --> 00:30:09,277
exactly what I want.

399
00:30:10,377 --> 00:30:12,777
Oh. I'm sorry,

400
00:30:13,777 --> 00:30:15,907
but my girlfriend is coming here.

401
00:30:22,077 --> 00:30:25,147
Wait. I'll buy drinks another time...

402
00:30:25,477 --> 00:30:26,507
No. There's no need.

403
00:30:27,147 --> 00:30:29,607
I think we're done here.

404
00:30:31,447 --> 00:30:33,407
I didn't say I'm giving up yet.

405
00:30:34,847 --> 00:30:37,277
I'll give you a call again.

406
00:32:02,147 --> 00:32:03,847
(Secrets of Women)

407
00:32:04,147 --> 00:32:05,177
Do you know something?

408
00:32:05,507 --> 00:32:08,007
Women and wine are the same.

409
00:32:08,107 --> 00:32:09,907
Why can't I reach you?

410
00:32:09,907 --> 00:32:12,477
Ji Yoo. I can finally breathe again.

411
00:32:12,477 --> 00:32:14,107
I want to introduce someone to you.

412
00:32:14,107 --> 00:32:16,147
Delivery man. Designated driver. And your mum's nurse.

413
00:32:16,147 --> 00:32:17,147
Will you be okay?

414
00:32:17,207 --> 00:32:19,677
What do you think of me as a woman?