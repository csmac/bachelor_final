1
00:01:03,530 --> 00:01:04,622
Everybody ready?

2
00:01:04,731 --> 00:01:05,755
MAN: Ready.

3
00:01:05,865 --> 00:01:07,833
Stage right, flying away.

4
00:01:53,046 --> 00:01:54,946
Nah. Sounds good.

5
00:01:58,785 --> 00:01:59,979
I'm going to go...

6
00:02:00,086 --> 00:02:01,383
Can you go back
by the school?

7
00:02:02,088 --> 00:02:03,350
They�re widening the road.

8
00:02:03,456 --> 00:02:04,753
The trees are
gone in front of it,

9
00:02:04,858 --> 00:02:06,917
and the golf course
across the road

10
00:02:07,026 --> 00:02:08,584
is being converted
to a subdivision.

11
00:02:08,795 --> 00:02:10,353
Yeah, okay.
I'll go back by the school

12
00:02:10,463 --> 00:02:11,862
or go down by
the school road and go,

13
00:02:11,965 --> 00:02:13,330
and then go out
to the main drag,

14
00:02:13,433 --> 00:02:14,866
and then go down
towards the house.

15
00:02:18,838 --> 00:02:20,430
This is Omemee.

16
00:02:20,874 --> 00:02:24,275
This is a little...
This is a town
in North Ontario.

17
00:02:25,411 --> 00:02:26,673
That's the mill that...

18
00:02:26,779 --> 00:02:28,770
That used to be
the mill down there,

19
00:02:28,882 --> 00:02:30,873
where I used to
catch fish and...

20
00:02:30,984 --> 00:02:33,179
When I was just
five years old or whatever.

21
00:02:33,286 --> 00:02:38,485
That's where I was
one of the town's
premier fishermen.

22
00:02:38,825 --> 00:02:41,191
We used to have a wagon.
And I would carry...

23
00:02:41,327 --> 00:02:45,593
Take my wagon down,
and fill it full of fish
and take all the fish home.

24
00:02:45,765 --> 00:02:48,666
Turtles, fish, you name it,
I had them in my wagon,

25
00:02:48,768 --> 00:02:51,362
and I pulled my wagon
through town.

26
00:02:51,971 --> 00:02:53,666
It was pretty cool.

27
00:02:53,840 --> 00:02:56,968
One of my biggest memories

28
00:02:57,076 --> 00:02:58,668
is the place called
Skinny Reel's,

29
00:02:58,811 --> 00:02:59,835
which is not there anymore,

30
00:02:59,946 --> 00:03:05,907
but it's a place where
there were little
wooden boxes of daisies,

31
00:03:06,019 --> 00:03:09,921
and they�d be out
on the sidewalk,
all these colored daisies.

32
00:03:10,056 --> 00:03:13,321
And I'd, you know...

33
00:03:14,494 --> 00:03:16,325
I'd just look at them.

34
00:03:17,063 --> 00:03:18,462
This is George Street, here.

35
00:03:18,565 --> 00:03:20,999
This is...
The school used to
be right down here.

36
00:03:21,100 --> 00:03:23,330
Now it's a park
or something.

37
00:03:25,939 --> 00:03:28,271
But it was a big
three-story school

38
00:03:28,374 --> 00:03:31,434
with a large fire escape
up the side of it.

39
00:03:31,544 --> 00:03:33,239
There was this tube

40
00:03:33,346 --> 00:03:35,473
for the kids to get out
if it caught on fire.

41
00:03:35,582 --> 00:03:37,174
I don't know how it
would ever catch on fire.

42
00:03:37,283 --> 00:03:38,409
It was brick.

43
00:03:38,585 --> 00:03:42,112
So we'd get... Climb up
inside the fire escape,

44
00:03:42,222 --> 00:03:43,280
all the way to the top,

45
00:03:43,389 --> 00:03:45,949
and then ride down the tube
and come shooting out
the bottom.

46
00:03:46,059 --> 00:03:47,959
That was a lot of fun.

47
00:03:48,661 --> 00:03:50,720
This house here,
this is
the reverend's house.

48
00:03:50,863 --> 00:03:54,390
This is where
the reverend used to be.

49
00:03:54,734 --> 00:03:57,328
And right here, over here,

50
00:03:57,437 --> 00:04:00,031
across the street,
is the Coronation Hall.

51
00:04:00,740 --> 00:04:03,106
This is the Village of Omemee
Coronation Hall.

52
00:04:03,209 --> 00:04:04,801
This is where my dad...

53
00:04:04,911 --> 00:04:06,071
There's a hall in there,

54
00:04:06,179 --> 00:04:07,737
holds about 100 people,

55
00:04:07,847 --> 00:04:09,337
a stage and everything.

56
00:04:09,449 --> 00:04:10,643
My dad appeared there

57
00:04:10,750 --> 00:04:11,944
in a minstrel show.

58
00:04:12,418 --> 00:04:15,285
And he was the only white guy.
He was the white guy.

59
00:04:15,388 --> 00:04:18,221
Had a,
kind of a beige jacket.

60
00:04:18,324 --> 00:04:20,815
And everybody else
was in blackface.

61
00:04:21,194 --> 00:04:24,857
All these, you know,
Irish Canadians in blackface.

62
00:04:26,666 --> 00:04:29,692
And there's a picture of me
with my cowboy outfit on,

63
00:04:29,802 --> 00:04:31,463
standing right here.

64
00:04:31,904 --> 00:04:34,998
I think I killed a turtle
right in front of the place

65
00:04:35,108 --> 00:04:39,067
by sticking
a firecracker up its ass
and lighting it.

66
00:04:41,014 --> 00:04:43,539
As children, you know,
young people will do.

67
00:04:43,650 --> 00:04:46,642
So, my environmental roots
are not that deep.

68
00:04:47,587 --> 00:04:49,111
And my green roots.

69
00:04:49,222 --> 00:04:51,053
But this is a new version.

70
00:04:51,157 --> 00:04:52,590
The old church is behind it.

71
00:04:52,692 --> 00:04:54,284
Hey, buddy.
Yeah, thank you.

72
00:04:54,394 --> 00:04:56,954
And this writer
who was a friend of my dad's,
with his three daughters,

73
00:04:57,063 --> 00:04:58,963
they lived in that house.

74
00:04:59,065 --> 00:05:00,896
But they were
all too old for me.

75
00:05:01,000 --> 00:05:02,763
I don't know
where Bob's going.

76
00:05:02,869 --> 00:05:05,497
I think he's
taking me somewhere.

77
00:05:06,806 --> 00:05:10,207
That's my brother, Bob,
up there in that old Cadillac.

78
00:05:11,210 --> 00:05:12,837
Oh, no,
I know where he's going now.

79
00:05:12,979 --> 00:05:14,742
This is right...
Now, this is...

80
00:05:14,847 --> 00:05:19,079
My dad, of course,
was very famous in Canada

81
00:05:19,185 --> 00:05:21,210
'cause he was a great writer.

82
00:05:21,321 --> 00:05:24,757
And this is the school
they named after my dad,
right here.

83
00:05:24,857 --> 00:05:27,018
This is
Scott Young Public School.

84
00:05:27,393 --> 00:05:30,760
And I was here with my dad
and everybody else

85
00:05:30,863 --> 00:05:34,959
when they dedicated
the school to him
and opened it up.

86
00:05:35,068 --> 00:05:38,765
It's a pretty cool school,
actually.
It's made out of stone.

87
00:05:38,871 --> 00:05:40,338
And it's nice.

88
00:05:42,008 --> 00:05:46,274
Now,
there used to be an arena,
a rink around here somewhere.

89
00:05:46,379 --> 00:05:48,904
I'm a little confused
as to where everything is.

90
00:05:49,015 --> 00:05:51,540
It just seems to be in
a different place now
than it was before.

91
00:05:51,984 --> 00:05:55,010
That's probably
because I was so small.

92
00:05:55,121 --> 00:05:56,645
Everything looked different.

93
00:05:57,123 --> 00:06:00,422
Just taking our way,
winding our way back
through the countryside,

94
00:06:00,526 --> 00:06:03,427
following my brother here,
who's taking us on a...

95
00:06:03,696 --> 00:06:05,323
He's the scout.

96
00:06:05,431 --> 00:06:06,591
So, he'll take us

97
00:06:06,699 --> 00:06:09,190
and go all the way
to Massey Hall, I think.

98
00:06:09,836 --> 00:06:13,203
Or just get a shot of the car
going to Massey Hall.

99
00:06:13,539 --> 00:06:15,029
You never know.

100
00:13:32,511 --> 00:13:35,036
We just passed
Goof Whitney's house.

101
00:13:35,581 --> 00:13:36,809
Goof was older than me,

102
00:13:36,916 --> 00:13:38,008
and he used to give me, like,

103
00:13:38,117 --> 00:13:41,780
he'd give me a nickel
to go up and say, you know,

104
00:13:42,521 --> 00:13:45,149
"You have a fat ass,"
or something to an old lady.

105
00:13:45,257 --> 00:13:47,452
You know... So l...

106
00:13:47,560 --> 00:13:50,723
You know,
he'd give me a nickel,
I'd do anything.

107
00:13:52,031 --> 00:13:55,125
And he also convinced me
that eating tar
was a good idea.

108
00:13:55,301 --> 00:13:58,270
I should try it.
It's a lot like chocolate.

109
00:13:58,370 --> 00:14:01,897
Said it's harsh at first,
but it turns into chocolate.

110
00:14:02,508 --> 00:14:05,272
So I tried eating
some tar off the road.

111
00:14:08,180 --> 00:14:12,344
That was the beginning of
mu close relationship
with cars, I think.

112
00:14:14,253 --> 00:14:19,316
When I do my listening,
I only listen in cars.

113
00:14:19,425 --> 00:14:22,485
I don't give a shit
if I'm listening on
a speaker this big.

114
00:14:22,595 --> 00:14:26,031
I can tell
if I like the music

115
00:14:27,266 --> 00:14:28,995
by listening to it
in a car.

116
00:27:27,178 --> 00:27:28,839
This is beautiful.

117
00:27:29,481 --> 00:27:33,542
Really, this whole,
this is so peaceful out here.

118
00:27:34,386 --> 00:27:37,378
When I come up here,
I just start thinking about...

119
00:27:37,489 --> 00:27:39,081
And it feels so good,
you know,

120
00:27:39,224 --> 00:27:43,092
just to sit there at the lake
and let the sun hit you,

121
00:27:43,194 --> 00:27:47,597
and watch the loons
and listen to
the honkers flying around,

122
00:27:47,699 --> 00:27:50,759
Look at the perch
and the sunfish in the water,

123
00:27:51,970 --> 00:27:56,464
see the fishermen out there
in their little boats.

124
00:27:57,575 --> 00:27:58,872
Very cool.

125
00:28:00,078 --> 00:28:04,344
Caesarea, township of Scugog.

126
00:28:05,183 --> 00:28:07,151
So, you know, like,
my brother is going at a pace

127
00:28:07,252 --> 00:28:09,618
that's absolutely perfect.
You know what I mean?

128
00:28:09,721 --> 00:28:12,485
There's nothing about the pace
that's rushed,

129
00:28:12,590 --> 00:28:14,581
and yet he's going
to beautiful places

130
00:28:14,693 --> 00:28:16,627
and he's just going
and there's...

131
00:28:16,795 --> 00:28:19,958
He's not going too slow,
he's not going too fast.

132
00:39:12,583 --> 00:39:13,880
These trees.

133
00:39:13,984 --> 00:39:18,011
Yeah, there was
a huge weeping willow tree,
which is where that stump is.

134
00:39:18,322 --> 00:39:20,586
Seeing the house
after it had burned down,

135
00:39:20,758 --> 00:39:23,226
and looking through
the window

136
00:39:23,327 --> 00:39:25,352
and seeing the floor
with the holes in it

137
00:39:25,463 --> 00:39:27,556
and everything
burned and everything...

138
00:39:27,665 --> 00:39:29,030
This is pretty well...

139
00:39:29,133 --> 00:39:30,828
It was right here.

140
00:39:31,202 --> 00:39:33,568
I would say
it was on this lawn.

141
00:39:33,671 --> 00:39:35,263
This lawn was...

142
00:39:36,307 --> 00:39:38,639
My mother's pride and joy,
I think, was this lawn.

143
00:39:38,776 --> 00:39:41,677
The house across the street
used to be the Knoph's place.

144
00:39:41,779 --> 00:39:43,679
K-N-O-P-H.
Yeah. Something like that.

145
00:39:43,781 --> 00:39:47,478
That was the greens keeper
and his couple of
daughters he had there.

146
00:39:47,585 --> 00:39:49,576
Yeah, that's right, yeah.

147
00:39:55,292 --> 00:39:56,850
We're back here.

148
00:39:57,628 --> 00:39:59,323
I haven't been in
here in a long time.

149
00:39:59,430 --> 00:40:00,727
I don't know...

150
00:40:00,831 --> 00:40:03,459
I remember,
I used to have
a pup tent.

151
00:40:03,567 --> 00:40:04,591
Yeah.

152
00:40:04,702 --> 00:40:08,798
So, I lived in
this pup tent out here
during the summer,

153
00:40:08,906 --> 00:40:13,002
and my dad would call
out of the back door
to wake me up,

154
00:40:13,110 --> 00:40:15,010
and I'd have to stick
my hand out of the tent

155
00:40:15,112 --> 00:40:17,376
to let him know
that I was awake.

156
00:40:17,481 --> 00:40:20,712
And I slept on a cot
in a little pup tent

157
00:40:20,818 --> 00:40:23,651
so I could be out
closer to my chickens.

158
00:40:24,288 --> 00:40:26,552
I think that's why
I was out there.

159
00:40:30,528 --> 00:40:32,621
There's garbage back there.

160
00:40:32,730 --> 00:40:36,826
This is the property line
with our neighbors.

161
00:40:37,168 --> 00:40:40,069
Every once in a while,
a fox would get into these,

162
00:40:40,237 --> 00:40:41,727
that chicken house.
Yeah.

163
00:40:41,839 --> 00:40:45,240
I remember coming out here
and sitting on top
of the chicken house

164
00:40:45,376 --> 00:40:47,674
with a shotgun
when I was a kid,

165
00:40:47,778 --> 00:40:48,904
waiting for the fox
to show up.

166
00:40:50,114 --> 00:40:52,048
It didn't show up.
No.

167
00:40:53,551 --> 00:40:54,848
Cool.
Yeah.

168
00:41:08,332 --> 00:41:09,299
MAN: Oh, man.

169
00:41:09,400 --> 00:41:10,367
Fuck.

170
00:41:11,502 --> 00:41:13,231
It's all gone.

171
00:41:17,208 --> 00:41:18,766
It's in my head.

172
00:41:19,543 --> 00:41:23,206
That's why
you don't have to worry
when you lose friends.

173
00:41:23,848 --> 00:41:25,315
'Cause they�re
still in your head,

174
00:41:25,416 --> 00:41:27,281
still in your heart.

175
00:53:40,951 --> 00:53:43,112
Well, we didn't get
off at Yonge Street.

176
00:53:43,220 --> 00:53:44,517
This is Bayview.

177
00:53:51,795 --> 00:53:55,390
We're in
a different area now.
Crossed over.

178
00:54:01,671 --> 00:54:03,400
We're pretty close.

179
00:54:03,506 --> 00:54:05,474
Certainly doesn't look
anything like it did before,

180
00:54:05,575 --> 00:54:07,372
but I can smell it.

181
00:54:08,945 --> 00:54:10,503
There it is.

182
00:54:11,548 --> 00:54:14,142
On the left,
middle of the next block.

183
01:13:33,175 --> 01:13:36,576
MAN 1: Stand by, house lights.
Stand by, house music forward.

184
01:13:42,351 --> 01:13:43,784
MAN 2: That's for all
the living work.

185
01:14:06,308 --> 01:14:07,741
This is good.

186
01:14:08,043 --> 01:14:09,237
MAN: I agree.

