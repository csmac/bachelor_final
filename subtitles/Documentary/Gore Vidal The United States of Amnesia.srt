﻿1
00:00:00,500 --> 00:00:02,500
<i>Previously on United
States of Tara...</i>

2
00:00:02,525 --> 00:00:04,525
<i>I just woke up... again...</i>

3
00:00:04,550 --> 00:00:06,050
<i>next to that woman
from the grocery store</i>

4
00:00:06,075 --> 00:00:08,075
Pammy's got just what you need.

5
00:00:08,100 --> 00:00:09,333
Uh... I can't do this.

6
00:00:09,334 --> 00:00:11,068
It's always the disease's
fault, isn't it?

7
00:00:11,102 --> 00:00:12,970
Never someone just
being an asshole!

8
00:00:13,004 --> 00:00:14,838
Marshall and Courtney...
game on.

9
00:00:14,873 --> 00:00:17,775
Marshall says he's
straight now?

10
00:00:17,809 --> 00:00:19,610
How is your beard, anyway?
You guys boning?

11
00:00:19,644 --> 00:00:22,112
How am I doing?

12
00:00:22,147 --> 00:00:23,514
Lynda P. Frazier.

13
00:00:23,548 --> 00:00:26,450
Your check bounced.
You owe me money.

14
00:00:26,484 --> 00:00:29,620
Princess Valhalla Hawkwind.

15
00:00:29,654 --> 00:00:31,355
Damn, girl.

16
00:00:31,389 --> 00:00:33,457
We should know our neighbors
a little better, right?

17
00:00:33,491 --> 00:00:35,492
- Ted Mayo. This is my partner, Hany.
- Hi.

18
00:00:35,527 --> 00:00:37,227
The Hubbard house
is officially ours!

19
00:00:37,262 --> 00:00:38,962
Escrow just closed.

20
00:00:38,997 --> 00:00:40,731
You need a real carpenter.

21
00:00:40,765 --> 00:00:42,065
How much money did you
give him for materials?

22
00:00:42,100 --> 00:00:44,535
Oh, boy, oh, boy, oh, boy.

23
00:00:44,569 --> 00:00:46,303
<i>You know what? I think you
have been sully-rigged.</i>

24
00:00:46,337 --> 00:00:47,905
Give me back my money.

25
00:00:50,208 --> 00:00:51,408
Buck, I love you.

26
00:00:53,077 --> 00:00:54,311
I love you.

27
00:00:54,345 --> 00:00:56,113
All I've ever done
is be good to you.

28
00:00:56,147 --> 00:00:58,248
That's all I've ever done...
just be good to you.

29
00:01:15,033 --> 00:01:17,968
<i>Open up the sky.</i>

30
00:01:18,002 --> 00:01:21,605
<i>This mess is getting high.</i>

31
00:01:21,639 --> 00:01:26,410
<i>It's windy, and our
family needs a ride.</i>

32
00:01:28,480 --> 00:01:33,217
<i>I know we'll be just fine.</i>

33
00:01:33,251 --> 00:01:38,789
<i>When we learn to
love the ride.</i>

34
00:01:39,958 --> 00:01:44,862
<i>I know we'll be fine.</i>

35
00:01:44,896 --> 00:01:51,802
<i>When we learn to
love the ride.</i>

36
00:01:51,836 --> 00:01:56,340
<i>I know we'll be just fine.</i>

37
00:01:56,374 --> 00:02:00,878
<i>When we learn to
love the ride.</i>

38
00:02:15,860 --> 00:02:18,595
The ants are back
in the kitchen.

39
00:02:26,771 --> 00:02:28,071
What happened to your neck?

40
00:02:28,105 --> 00:02:29,472
A door-to-door werewolf.

41
00:02:29,507 --> 00:02:30,740
Give me that crowbar.

42
00:02:32,810 --> 00:02:35,779
What the fuck happened last night?
Huh?

43
00:02:35,813 --> 00:02:37,948
Who was that?

44
00:02:37,982 --> 00:02:39,249
I don't know. I...

45
00:02:39,283 --> 00:02:41,051
Oh, you know.

46
00:02:43,120 --> 00:02:45,188
Her name is Pammy.

47
00:02:48,359 --> 00:02:50,727
I saw that look on her face.

48
00:02:50,761 --> 00:02:52,529
She's in love with you.

49
00:02:52,563 --> 00:02:53,663
It's nothing.

50
00:02:53,698 --> 00:02:55,498
It's... it's Buck.
It's not me.

51
00:02:55,533 --> 00:02:57,867
That is not enough.

52
00:02:57,902 --> 00:02:59,269
You know Buck.

53
00:02:59,303 --> 00:03:01,338
You've known Buck forever,
the stuff that he...

54
00:03:01,372 --> 00:03:02,572
No, it's you!

55
00:03:02,606 --> 00:03:04,140
You were better.

56
00:03:04,175 --> 00:03:06,276
You fucking told me you
were better... for months!

57
00:03:06,310 --> 00:03:07,844
You said you weren't
transitioning.

58
00:03:07,878 --> 00:03:08,845
I didn't know!

59
00:03:08,879 --> 00:03:11,114
You fucking knew!

60
00:03:13,651 --> 00:03:16,920
I guess...

61
00:03:16,954 --> 00:03:18,922
I guess a part of me...

62
00:03:18,956 --> 00:03:20,790
Yeah. That's why I'm mad.

63
00:03:20,825 --> 00:03:23,093
'Cause you lied to me... not
Buck, not fucking Alice.

64
00:03:23,127 --> 00:03:25,628
You fucking lied to me!

65
00:03:28,232 --> 00:03:30,467
So...

66
00:03:30,501 --> 00:03:32,035
What do we do?

67
00:03:32,069 --> 00:03:34,437
We don't do anything.

68
00:03:34,472 --> 00:03:36,639
You fucking get help.
Find a therapist.

69
00:03:36,674 --> 00:03:37,807
Okay. I will.

70
00:03:37,842 --> 00:03:39,042
Yeah.

71
00:03:39,076 --> 00:03:41,144
You will.

72
00:03:43,314 --> 00:03:45,081
Give me that.

73
00:03:48,819 --> 00:03:50,086
Why?

74
00:03:50,121 --> 00:03:52,422
'Cause I told you to.

75
00:04:11,175 --> 00:04:13,043
No. Wait, wait, wait, wait.

76
00:04:13,077 --> 00:04:15,945
Not... not in here.
Not in this house.

77
00:04:45,876 --> 00:04:47,210
Hey, Kate.

78
00:04:47,244 --> 00:04:48,344
Tara.

79
00:04:48,379 --> 00:04:51,081
No more "mom," huh?
Now I'm just "Tara"?

80
00:04:51,115 --> 00:04:53,083
I don't know why you'd
be upset, Tara.

81
00:04:53,117 --> 00:04:55,151
I've called you many names
over our time together.

82
00:04:55,186 --> 00:04:56,753
Okay. Stupid ant!

83
00:04:56,787 --> 00:04:58,747
I'm sure what happened last
night was confusing...

84
00:04:58,756 --> 00:05:01,224
Not to me. I'm used to it.

85
00:05:01,258 --> 00:05:04,394
Is there a name you'd rather
be called, Tara, than "Tara"?

86
00:05:04,428 --> 00:05:07,297
That's fun, Kate. Good stuff.

87
00:05:07,331 --> 00:05:08,965
Morning.

88
00:05:08,999 --> 00:05:10,600
I'm off to work.

89
00:05:10,634 --> 00:05:12,602
Someone around here needs
to be a responsible adult.

90
00:05:12,636 --> 00:05:16,005
Also, you have grass
all over your back.

91
00:05:17,608 --> 00:05:21,444
So, I found this killer wedding
dress by badgley mischka,

92
00:05:21,479 --> 00:05:23,413
but it's really expensive.

93
00:05:23,447 --> 00:05:25,348
But it's awesome!

94
00:05:25,382 --> 00:05:27,350
But I need my
chocolate fountain.

95
00:05:27,384 --> 00:05:29,185
I'm not getting married without
my chocolate fountain.

96
00:05:29,220 --> 00:05:31,020
And I don't care what
Nick's family says...

97
00:05:31,055 --> 00:05:32,689
We are dancing down the aisle,

98
00:05:32,723 --> 00:05:34,657
and we are all
wearing sunglasses.

99
00:05:34,692 --> 00:05:36,893
Well, that sounds like
every little girl's dream.

100
00:05:38,429 --> 00:05:40,497
So...

101
00:05:40,531 --> 00:05:42,031
Last night?

102
00:05:42,066 --> 00:05:44,167
Huh?

103
00:05:44,201 --> 00:05:46,336
You and Max work it out?

104
00:05:46,370 --> 00:05:48,371
Charmy, I really don't
want to talk about it.

105
00:05:49,406 --> 00:05:52,041
Oh, you know what I
realized this morning?

106
00:05:52,076 --> 00:05:56,412
My bedroom window overlooks
the Hubbards' backyard.

107
00:05:57,948 --> 00:06:02,185
That's quite a view.

108
00:06:07,324 --> 00:06:09,392
A good therapist in
Overland Park? Please.

109
00:06:09,426 --> 00:06:11,027
I can't even find a good latte.

110
00:06:11,061 --> 00:06:12,662
That one place...

111
00:06:12,696 --> 00:06:14,631
That café attached to
the Christmas store?

112
00:06:14,665 --> 00:06:17,033
With that little queen
behind the counter?

113
00:06:18,636 --> 00:06:20,436
Spiritual pride in a barista?
I won't brook it! I won't!

114
00:06:20,471 --> 00:06:21,838
He won't brook it.

115
00:06:21,872 --> 00:06:24,440
So, what's the problem?

116
00:06:26,143 --> 00:06:27,544
Let's just say

117
00:06:27,578 --> 00:06:31,080
I've been seriously
without help for a while,

118
00:06:31,115 --> 00:06:33,149
and I've... I've...
I've been okay,

119
00:06:33,184 --> 00:06:36,686
but I think I actually
need help again.

120
00:06:36,720 --> 00:06:38,421
Just got some stuff
going on with me.

121
00:06:39,456 --> 00:06:41,958
Or... Max.

122
00:06:41,992 --> 00:06:44,027
With me and Max.

123
00:06:44,061 --> 00:06:46,129
Well, you're not gonna
find anybody around here.

124
00:06:46,163 --> 00:06:47,263
Ugh!

125
00:06:47,298 --> 00:06:49,065
Hany and I went through
a rocky period

126
00:06:49,099 --> 00:06:50,266
a few years back...

127
00:06:50,301 --> 00:06:52,135
Erectile dysfunction.

128
00:06:52,169 --> 00:06:54,971
Let's just say it...
it was me.

129
00:06:55,005 --> 00:06:56,539
It was him.

130
00:06:56,574 --> 00:06:59,342
We went to quack after
quack after quack.

131
00:06:59,376 --> 00:07:00,677
It was a duck pond.

132
00:07:00,711 --> 00:07:03,780
So, uh, shrinkwise, I'm fucked?

133
00:07:03,814 --> 00:07:04,948
You may be.

134
00:07:04,982 --> 00:07:06,849
But in the meantime...

135
00:07:08,152 --> 00:07:10,920
Take this for a spin.

136
00:07:10,955 --> 00:07:13,056
Written by my old therapist.

137
00:07:13,090 --> 00:07:18,628
Oh, the much-lauded
Shoshana Schoenbaum, huh?

138
00:07:18,662 --> 00:07:20,830
It's simple stuff, but
she's very down-to-earth.

139
00:07:20,864 --> 00:07:22,432
Till you find the right doctor.

140
00:07:22,466 --> 00:07:24,067
Yeah, I'll give it a look.

141
00:07:24,101 --> 00:07:25,668
Just don't crack the spine.

142
00:07:25,703 --> 00:07:26,769
He hates that.

143
00:07:26,804 --> 00:07:28,605
I hate that.

144
00:07:31,442 --> 00:07:32,775
Hey, buddy.

145
00:07:32,810 --> 00:07:35,778
Just in time for a little
Billy Jack action.

146
00:07:37,915 --> 00:07:41,050
"When I see this child."

147
00:07:41,085 --> 00:07:43,920
"Who is so special to us."

148
00:07:43,954 --> 00:07:45,855
I kind of have to
get to school.

149
00:07:45,889 --> 00:07:48,191
"We call him God's
little ray of sunshine,"

150
00:07:48,225 --> 00:07:52,962
"so demeaned by this
idiotic moment of yours..."

151
00:07:52,997 --> 00:07:55,431
"I just go berserk!"

152
00:08:02,940 --> 00:08:05,074
Oh, I fucked my back.

153
00:08:05,109 --> 00:08:07,577
I thought Billy Jack
was about nonviolence.

154
00:08:07,611 --> 00:08:09,212
I don't know.

155
00:08:13,150 --> 00:08:15,551
So...

156
00:08:15,586 --> 00:08:16,953
I guess Buck's out.

157
00:08:18,422 --> 00:08:21,157
Yeah, Buck's out.

158
00:08:21,191 --> 00:08:22,425
You and mom okay?

159
00:08:22,459 --> 00:08:24,527
Yeah. We're dealing with it.

160
00:08:24,561 --> 00:08:27,463
I thought you were
working with that guy.

161
00:08:27,498 --> 00:08:29,365
Sully? No, not anymore.

162
00:08:29,400 --> 00:08:31,534
Motherfucker tried
to rip me off.

163
00:08:31,568 --> 00:08:35,104
So... You just... Let
the guy take our money?

164
00:08:35,139 --> 00:08:37,006
No, I took care of it.

165
00:08:37,041 --> 00:08:38,007
How?

166
00:08:39,310 --> 00:08:41,377
I kind of kicked the
shit out of him.

167
00:08:42,746 --> 00:08:44,614
Does, um...

168
00:08:44,648 --> 00:08:46,149
Does mom know?

169
00:08:46,183 --> 00:08:47,216
No.

170
00:08:47,251 --> 00:08:49,052
Let's keep it that
way, okay, bro?

171
00:08:54,024 --> 00:08:55,892
I was just out of art school

172
00:08:55,926 --> 00:08:57,560
and realized that the crap I
was making wasn't selling.

173
00:08:57,594 --> 00:08:59,028
Mm-hmm.

174
00:08:59,063 --> 00:09:01,364
Little less talking,
little more painting.

175
00:09:01,398 --> 00:09:03,666
- So I started making princess paintings.
- Oh, God.

176
00:09:03,701 --> 00:09:05,268
My arm's fucking killing me!

177
00:09:05,302 --> 00:09:06,569
Quit complaining.

178
00:09:06,603 --> 00:09:08,604
I mean, at first,
I hated myself.

179
00:09:08,639 --> 00:09:09,906
Starting to hate you!

180
00:09:09,940 --> 00:09:11,874
But then I realized, you know,

181
00:09:11,909 --> 00:09:13,976
at least my paintings
were selling.

182
00:09:14,011 --> 00:09:16,245
And every now and then, I could
afford a whopper... with cheese.

183
00:09:16,280 --> 00:09:17,246
Sword!

184
00:09:19,016 --> 00:09:21,584
So, what are her superpowers?

185
00:09:21,618 --> 00:09:24,220
Does she burn things
with her eyes?

186
00:09:24,254 --> 00:09:26,089
Does she hear good?

187
00:09:26,123 --> 00:09:27,557
Does she fly?

188
00:09:27,591 --> 00:09:29,359
Oh, yeah. She flies.

189
00:09:29,393 --> 00:09:30,927
What about sex?

190
00:09:30,961 --> 00:09:32,962
Do I use my body like a weapon,

191
00:09:32,996 --> 00:09:34,831
like Poison Ivy on Batman?

192
00:09:34,865 --> 00:09:36,766
I mean, I'm pretty fuckable.

193
00:09:36,800 --> 00:09:38,368
No. The princess don't fuck.

194
00:09:38,402 --> 00:09:40,169
Because she won't?

195
00:09:40,204 --> 00:09:42,271
Because she can't.

196
00:09:43,307 --> 00:09:44,407
Got it.

197
00:09:44,441 --> 00:09:46,909
Like Barbie... no holes.

198
00:09:51,582 --> 00:09:53,716
Hey, Max, I was just...

199
00:10:30,988 --> 00:10:33,156
I figure we could do it there.

200
00:10:33,190 --> 00:10:36,159
Sure. Yeah. Makes sense.

201
00:10:36,193 --> 00:10:37,460
I-I mean, we could...

202
00:10:37,494 --> 00:10:40,229
No, it... this seems right.

203
00:10:40,264 --> 00:10:42,965
Classic.

204
00:10:43,000 --> 00:10:44,834
Yeah, it's, uh...

205
00:10:44,868 --> 00:10:46,736
It's what you'd
expect, you know?

206
00:10:46,770 --> 00:10:48,271
A bed.

207
00:10:49,740 --> 00:10:53,910
What'd you tell Ms. Watkins
to get out of school?

208
00:10:53,944 --> 00:10:55,978
I told her I had an ortho.

209
00:10:56,013 --> 00:10:57,680
You?

210
00:10:57,714 --> 00:11:01,017
I went with explosive diarrhea.

211
00:11:04,054 --> 00:11:06,355
That's a lot of condoms.

212
00:11:07,724 --> 00:11:09,659
Where did you get all that?

213
00:11:09,693 --> 00:11:11,127
Planned parenthood.

214
00:11:11,161 --> 00:11:13,529
My mom's nightstand.

215
00:11:13,564 --> 00:11:15,531
My cousin Graham.

216
00:11:15,566 --> 00:11:19,368
So, any of these
calling your name?

217
00:11:19,403 --> 00:11:21,471
Um, that one.

218
00:11:23,073 --> 00:11:24,540
No, I...

219
00:11:26,410 --> 00:11:28,377
No.

220
00:11:30,347 --> 00:11:32,348
Yeah, sure. That...
that's fine.

221
00:11:32,382 --> 00:11:34,250
Yeah?

222
00:11:34,284 --> 00:11:36,118
Yeah.

223
00:11:50,267 --> 00:11:53,970
The... the... um,
the other way.

224
00:11:54,004 --> 00:11:57,874
I think it's
supposed to be hard.

225
00:12:00,110 --> 00:12:01,777
I bet if you wanted to,

226
00:12:01,812 --> 00:12:04,080
you could make some pretty
good coin off this thing

227
00:12:04,114 --> 00:12:05,715
and pay off your debt.

228
00:12:05,749 --> 00:12:10,419
You know... mugs and
t-shirts and key chains.

229
00:12:10,454 --> 00:12:11,420
Sure.

230
00:12:11,455 --> 00:12:12,755
And hats and stickers.

231
00:12:12,789 --> 00:12:14,423
And you could put
out a magazine

232
00:12:14,458 --> 00:12:16,259
with pictures and a centerfold.

233
00:12:16,293 --> 00:12:17,994
Oh, no. Not a centerfold.

234
00:12:18,028 --> 00:12:19,929
She's a subject, not an object.

235
00:12:21,465 --> 00:12:22,598
Mm-hmm.

236
00:12:24,201 --> 00:12:26,536
But you know what?
You're right.

237
00:12:26,570 --> 00:12:29,105
I mean, who the fuck is
gonna see the painting?

238
00:12:31,341 --> 00:12:33,342
Let's make a movie.

239
00:12:35,812 --> 00:12:37,547
<i>"From birth,"</i>

240
00:12:37,581 --> 00:12:40,583
<i>"each of us sets out to
discover who we really are."</i>

241
00:12:40,617 --> 00:12:44,554
<i>"But when trauma occurs,
we create ways to survive."</i>

242
00:12:44,588 --> 00:12:46,455
<i>"Often, we split into pieces"</i>

243
00:12:46,490 --> 00:12:49,692
<i>"and abandon the most
vulnerable parts of ourselves,"</i>

244
00:12:49,726 --> 00:12:53,362
<i>"boxing them up in tidy
packages and tossing them away."</i>

245
00:12:53,397 --> 00:12:56,065
<i>"Later, we spend our lives
looking for these parcels,"</i>

246
00:12:56,099 --> 00:12:58,200
<i>"hoping to meet someone
who can help us"</i>

247
00:12:58,235 --> 00:13:02,238
<i>"find these lost splintered
pieces of ourself."</i>

248
00:13:22,125 --> 00:13:26,095
<i>Charmaine, dear, shut
that door this instant!</i>

249
00:13:29,633 --> 00:13:32,802
<i>All that rain will
Buckle the hardwood!</i>

250
00:13:35,372 --> 00:13:38,341
Tara, what in the name
of all that is Holy

251
00:13:38,375 --> 00:13:40,576
have you done with your poncho?

252
00:13:40,611 --> 00:13:42,411
I don't know, Mimi.

253
00:13:42,446 --> 00:13:45,114
Don't play games with me.

254
00:13:56,960 --> 00:14:00,296
That thing you did...

255
00:14:00,330 --> 00:14:02,698
That was you c-coming, right?

256
00:14:02,733 --> 00:14:04,400
Uh... Yeah.

257
00:14:04,434 --> 00:14:05,568
Big-time.

258
00:14:05,602 --> 00:14:06,669
Did you?

259
00:14:06,703 --> 00:14:08,704
Yeah. Totally.

260
00:14:08,739 --> 00:14:09,872
A lot.

261
00:14:11,508 --> 00:14:14,176
So many times.

262
00:14:16,980 --> 00:14:19,415
So...

263
00:14:19,449 --> 00:14:23,919
I don't know if you're,
like, all worn out...

264
00:14:23,954 --> 00:14:26,656
But I could go again.

265
00:14:28,659 --> 00:14:30,092
Oh.

266
00:14:30,127 --> 00:14:31,360
Okay, yeah.

267
00:14:31,395 --> 00:14:33,896
Yeah, if... if you want to.

268
00:14:35,732 --> 00:14:39,568
You're not in any... Pain
or anything, are you?

269
00:14:41,538 --> 00:14:43,673
I don't think so.

270
00:14:46,510 --> 00:14:47,943
We could go back to school.

271
00:14:47,978 --> 00:14:49,745
I do have a chemistry test.

272
00:15:08,732 --> 00:15:10,266
What?

273
00:15:10,300 --> 00:15:11,801
I got help.

274
00:15:11,835 --> 00:15:13,436
I found somebody.

275
00:15:14,671 --> 00:15:16,105
Okay.

276
00:15:16,139 --> 00:15:18,140
Remember that woman that Ted
was telling us about...

277
00:15:18,175 --> 00:15:20,242
His therapist in New York?

278
00:15:20,277 --> 00:15:22,678
Well, when I was there this morning,
he gave me one of her books.

279
00:15:22,713 --> 00:15:25,347
I've been reading it all day.
It's so good.

280
00:15:25,382 --> 00:15:28,150
And it says that people...
all people...

281
00:15:28,185 --> 00:15:30,052
have ways of being
not integrated,

282
00:15:30,087 --> 00:15:32,688
and that... and that we
jettison parts of ourselves

283
00:15:32,723 --> 00:15:34,023
into these parcels.

284
00:15:34,057 --> 00:15:35,725
Come on. Speak English.

285
00:15:35,759 --> 00:15:37,860
It's a book. A
book is not help.

286
00:15:39,629 --> 00:15:41,030
We need a new doctor.

287
00:15:41,064 --> 00:15:43,399
Well, I got in touch with her.

288
00:15:47,304 --> 00:15:49,338
Did you call her?

289
00:15:49,372 --> 00:15:51,440
Yeah, I called her. She's
Ted's old therapist.

290
00:15:51,475 --> 00:15:54,877
Max, she's fucking brilliant.
She has a mind like...

291
00:15:56,646 --> 00:15:58,581
She loves my story.

292
00:15:58,615 --> 00:16:00,116
She's in New York.

293
00:16:00,150 --> 00:16:01,984
Well, she's happy to start
with phone therapy.

294
00:16:02,018 --> 00:16:03,452
We might Skype.

295
00:16:03,487 --> 00:16:05,087
Skype?

296
00:16:05,122 --> 00:16:08,190
And she can travel. She's,
like, this famous author.

297
00:16:08,225 --> 00:16:11,360
I don't trust these...
Small-town locals.

298
00:16:11,394 --> 00:16:12,895
I'm too...

299
00:16:12,929 --> 00:16:13,896
What?

300
00:16:13,930 --> 00:16:15,631
What are you?

301
00:16:19,236 --> 00:16:21,036
It's Kate.

302
00:16:21,071 --> 00:16:22,037
Yep.

303
00:16:23,540 --> 00:16:25,841
You got to be kidding me.

304
00:16:25,876 --> 00:16:28,110
W-what's wrong with the car?

305
00:16:28,145 --> 00:16:29,645
Oka... no, no, no, no.

306
00:16:29,679 --> 00:16:31,119
Don't start it again.
You flooded it.

307
00:16:31,148 --> 00:16:32,469
Where are you? We'll
come get you.

308
00:16:33,750 --> 00:16:34,784
Where?

309
00:16:47,297 --> 00:16:50,499
Wow. This is colorful.

310
00:16:55,205 --> 00:16:57,439
Smells like pee.

311
00:17:01,011 --> 00:17:02,645
Kate!

312
00:17:03,814 --> 00:17:05,881
We're here! Let's go!

313
00:17:08,418 --> 00:17:10,019
Bye! Bye-bye, Kate.

314
00:17:10,053 --> 00:17:11,520
Goodbye, goodbye!

315
00:17:12,522 --> 00:17:14,223
Oh. Hi.

316
00:17:14,257 --> 00:17:15,791
What are you wearing?

317
00:17:15,826 --> 00:17:18,227
It's my Princess
Valhalla costume.

318
00:17:18,261 --> 00:17:21,230
You're not walking around like that.
Come here.

319
00:17:22,332 --> 00:17:25,234
Ugh. That fucking car.

320
00:17:25,268 --> 00:17:28,037
Stupid piece of American shit.

321
00:17:28,071 --> 00:17:30,973
Yeah, that's who's at fault here...
Detroit.

322
00:17:33,476 --> 00:17:34,443
Ugh.

323
00:17:44,054 --> 00:17:46,388
That sign was the
only thing left

324
00:17:46,423 --> 00:17:48,724
when I found this place.

325
00:17:48,758 --> 00:17:52,361
They made copper plating,
doorknobs, drawer pulls.

326
00:17:52,395 --> 00:17:54,029
How long have you lived here?

327
00:17:54,064 --> 00:17:55,865
About a year.

328
00:17:55,899 --> 00:17:58,567
But you don't want
to know that.

329
00:17:58,602 --> 00:18:00,236
I don't?

330
00:18:02,539 --> 00:18:05,374
I've noticed that when people
walk into a new place,

331
00:18:05,408 --> 00:18:07,176
they get nervous,

332
00:18:07,210 --> 00:18:10,212
so they start asking
questions about numbers.

333
00:18:10,247 --> 00:18:12,982
What do you really
want to know?

334
00:18:19,022 --> 00:18:20,489
Hey.

335
00:18:20,523 --> 00:18:22,491
You, uh, you ready to go?

336
00:18:22,525 --> 00:18:24,894
Yeah.

337
00:18:28,164 --> 00:18:30,733
I'm gonna give this to you.

338
00:18:33,970 --> 00:18:36,005
It's Kate's.

339
00:18:42,979 --> 00:18:45,347
Look, we're not talking about it anymore.
End of discussion.

340
00:18:45,382 --> 00:18:48,150
You can't ground me. I
was there for my job.

341
00:18:48,184 --> 00:18:50,352
I've had it up to
here with you guys.

342
00:18:50,387 --> 00:18:52,421
Who's the parent
around here, anyway?

343
00:18:52,455 --> 00:18:53,489
No one.

344
00:18:53,523 --> 00:18:55,324
Jesus God, girl.

345
00:18:55,358 --> 00:18:58,761
Could you cut me some
fucking slack for once?

346
00:18:58,795 --> 00:19:00,029
Maybe I'll move out.

347
00:19:00,063 --> 00:19:01,864
Maybe I'll move in with Lynda.

348
00:19:01,898 --> 00:19:04,266
How come you never
told us she was black?

349
00:19:04,301 --> 00:19:05,935
What does that have
to do with anything?

350
00:19:05,969 --> 00:19:07,636
Wake up, Fox News.

351
00:19:07,671 --> 00:19:10,039
There's a new sheriff in town.
His name is Barack Obama.

352
00:19:10,073 --> 00:19:11,941
I think it's a cool thing.

353
00:19:11,975 --> 00:19:13,409
Oh, great. Now it's
cool that she's black.

354
00:19:13,443 --> 00:19:14,510
That's even worse.

355
00:19:14,544 --> 00:19:16,378
Okay, here's the deal...

356
00:19:16,413 --> 00:19:18,747
You're done with the car. You're
done with going downtown.

357
00:19:18,782 --> 00:19:20,683
You're done with that woman... having
nothing to do with her being black.

358
00:19:20,717 --> 00:19:22,618
What do you guys
even do together?

359
00:19:22,652 --> 00:19:25,287
Bottom line... you're grounded
until we say otherwise.

360
00:19:25,322 --> 00:19:27,623
You know what, dad... if you are
so desperate to ground somebody,

361
00:19:27,657 --> 00:19:29,591
you should be grounding mom!

362
00:19:29,626 --> 00:19:31,627
She's the one that has been
pretending to be somebody else

363
00:19:31,661 --> 00:19:34,063
with that crazy lady from the
ice-skating rink, doing God knows what!

364
00:19:34,097 --> 00:19:36,031
Coming from the girl
in the winged hat

365
00:19:36,066 --> 00:19:37,266
and the princess costume.

366
00:19:37,300 --> 00:19:38,467
Stop! Okay?

367
00:19:38,501 --> 00:19:40,269
Look, Kate, your
mom's getting help.

368
00:19:40,303 --> 00:19:43,205
We found an expert in New
York who's gonna help her.

369
00:19:43,239 --> 00:19:44,707
New York?

370
00:19:44,741 --> 00:19:47,009
Yeah. They don't know what
they're doing around here.

371
00:19:47,043 --> 00:19:49,778
Come on. Let's just give the
new doc a chance, all right?

372
00:19:49,813 --> 00:19:51,580
Hey, kids!

373
00:19:52,682 --> 00:19:54,616
So...

374
00:19:54,651 --> 00:19:56,352
We've got some big news.

375
00:19:56,386 --> 00:19:57,553
We're pregnant!

376
00:19:57,587 --> 00:19:58,988
What the fuck, Nick?

377
00:19:59,022 --> 00:19:59,989
I'm sorry.

378
00:20:00,023 --> 00:20:01,523
You fucking ruined it!

379
00:20:01,558 --> 00:20:02,725
Okay. You go.

380
00:20:02,759 --> 00:20:04,193
No. What am I
supposed to say now?

381
00:20:07,163 --> 00:20:09,431
I... it's gone. The
moment's gone.

382
00:20:09,466 --> 00:20:10,966
Hey, guys...

383
00:20:11,001 --> 00:20:12,668
Congratulations, Charmaine.

384
00:20:12,702 --> 00:20:13,936
Thank you.

385
00:20:13,970 --> 00:20:15,904
If that baby knows
what's good for it,

386
00:20:15,939 --> 00:20:17,606
it'll tie the umbilical cord
around its neck and take a leap.

387
00:20:17,640 --> 00:20:19,308
What the hell, Kate?

388
00:20:23,246 --> 00:20:24,213
Anyway...

389
00:20:25,648 --> 00:20:28,417
Uh, Max, I want to
talk to you sometime

390
00:20:28,451 --> 00:20:31,020
maybe about... Maybe
buying this place.

391
00:20:31,054 --> 00:20:34,590
For me and Charmaine and...
Our baby.

392
00:20:34,624 --> 00:20:36,725
We're so excited to have
our own little family,

393
00:20:36,760 --> 00:20:38,827
but Charmaine said she
can't imagine doing it

394
00:20:38,862 --> 00:20:40,963
anywhere but here... right
next door to you guys.

395
00:20:40,997 --> 00:20:43,265
Aw.

396
00:20:44,567 --> 00:20:45,768
Hey! Ahhh...

397
00:20:45,802 --> 00:20:48,637
All right. Great, great.
Good. Goddamn.

398
00:20:48,671 --> 00:20:50,205
I guess I'm a little confused

399
00:20:50,240 --> 00:20:52,041
about why you're
living with us.

400
00:20:52,075 --> 00:20:54,443
I mean, aren't you supposed
to be "re-virginating"?

401
00:20:54,477 --> 00:20:57,212
Well, it must have happened
before I moved out of Nick's.

402
00:21:00,784 --> 00:21:04,119
I love the light in here!

403
00:21:04,154 --> 00:21:07,222
This is gonna be
the baby's room.

404
00:21:07,257 --> 00:21:09,291
Okay, obviously, this
junk has to go...

405
00:21:23,039 --> 00:21:24,473
Oh, my God!

406
00:21:24,507 --> 00:21:26,175
I know... a porch swing!

407
00:21:26,209 --> 00:21:28,110
So then Nick and I can
rock the baby together.

408
00:21:28,144 --> 00:21:29,178
Hey, Char?

409
00:21:29,212 --> 00:21:31,280
Yeah?

410
00:21:33,450 --> 00:21:35,984
You're gonna be a great mother.

411
00:21:42,525 --> 00:21:44,259
Hey, dad, where's mom?

412
00:21:44,294 --> 00:21:45,934
Oh, she's next door,
but don't bother her.

413
00:21:45,962 --> 00:21:48,330
She's got a phone session
with her new therapist.

414
00:21:48,364 --> 00:21:50,666
She found somebody we're
kind of excited about.

415
00:21:51,935 --> 00:21:52,968
Cool.

416
00:21:53,002 --> 00:21:54,169
Yeah.

417
00:21:54,204 --> 00:21:56,572
Hey, I know, uh, I've
been kind of busy

418
00:21:56,606 --> 00:21:58,540
and a little preoccupied,

419
00:21:58,575 --> 00:22:00,335
but how's things going
with you and Courtney?

420
00:22:03,046 --> 00:22:05,147
You know, uh...

421
00:22:05,181 --> 00:22:07,149
We're...

422
00:22:07,183 --> 00:22:08,851
It's...

423
00:22:08,885 --> 00:22:11,286
I'm not prying, okay?
It's none of my business.

424
00:22:11,321 --> 00:22:13,589
I'm gonna run down to grandstand
and grab some burgers.

425
00:22:13,623 --> 00:22:14,823
You want anything?

426
00:22:16,092 --> 00:22:18,026
I'm gay.

427
00:22:24,367 --> 00:22:25,667
Good.

428
00:22:29,672 --> 00:22:31,740
So, you want anything?

429
00:22:31,774 --> 00:22:34,209
Yeah, um...

430
00:22:34,244 --> 00:22:36,545
Maybe I'll just come
with you to grandstand.

431
00:22:36,579 --> 00:22:38,413
Cool.

432
00:22:44,654 --> 00:22:47,289
I think you should tell mom
about that guy you beat up.

433
00:23:03,506 --> 00:23:06,008
How was your session?

434
00:23:06,042 --> 00:23:09,845
Really, um... Intense.

435
00:23:09,879 --> 00:23:11,380
Yeah?

436
00:23:13,449 --> 00:23:15,217
Marshall came out
to me tonight.

437
00:23:15,251 --> 00:23:17,553
Officially?

438
00:23:17,587 --> 00:23:19,655
Oh, Marshmallow.

439
00:23:19,689 --> 00:23:21,256
How'd you take I?

440
00:23:21,291 --> 00:23:22,658
I don't know.

441
00:23:22,692 --> 00:23:26,795
I-I guess it gave me...
Some hope.

442
00:23:31,100 --> 00:23:33,402
I'm just glad you
found someone.

443
00:23:35,872 --> 00:23:37,573
Me too.

444
00:23:55,592 --> 00:23:57,259
Tara?

445
00:23:57,293 --> 00:23:58,560
Yeah?

446
00:24:00,630 --> 00:24:03,599
I did a bad thing.

447
00:24:42,071 --> 00:24:44,339
Tara?

448
00:24:55,752 --> 00:24:59,054
Excuse me? Do you mind? I'm
finishing up a session.

449
00:25:01,858 --> 00:25:03,258
Hold on, Dolores.

450
00:25:03,293 --> 00:25:04,993
Can I help you?

451
00:25:05,028 --> 00:25:09,097
<i>It must be something
psychological.</i>

