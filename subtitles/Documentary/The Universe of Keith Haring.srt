﻿1
00:00:01,120 --> 00:00:06,153
[Star of the Universe Episode 3]

2
00:02:35,200 --> 00:02:36,900
I want to die.

3
00:03:46,920 --> 00:03:51,921
Have you ever thought about what
the world would be like without Wooju?

4
00:03:52,125 --> 00:03:57,600
Are you nuts? Focus
3,2... Go!

5
00:04:01,795 --> 00:04:05,896
Row A, seat 37.
I'm going to see Wooju in person!

6
00:04:08,098 --> 00:04:10,840
That was awesome. Awesome.

7
00:04:11,950 --> 00:04:13,000
Damn!

8
00:04:13,050 --> 00:04:16,085
- I got distracted.
- Bad luck.

9
00:04:16,100 --> 00:04:18,601
You were distracted during
the holiest of holy ticketing hour?

10
00:04:18,620 --> 00:04:22,087
This is the first time
he's had an album flop.

11
00:04:23,100 --> 00:04:26,836
I mean, he's just a man.
He could have just had a low moment.

12
00:04:26,900 --> 00:04:30,711
It's fine if he thinks that way,
as long as he doesn't act on it.

13
00:04:32,500 --> 00:04:33,469
Is he really going to die?

14
00:04:36,005 --> 00:04:39,907
You have insider information,
tell me the truth...

15
00:04:40,900 --> 00:04:43,500
Does he died before or after the concert?

16
00:04:44,900 --> 00:04:48,269
Even if I knew, how am I supposed
to sit by and watch him go?

17
00:04:49,300 --> 00:04:51,844
This job is the worst.

18
00:04:54,900 --> 00:04:58,604
Let's just do it then!
It's not like he can see us!

19
00:05:00,400 --> 00:05:05,200
You know how he is. He hates
clingy fans more than anything.

20
00:05:05,220 --> 00:05:08,044
Then what if he does something
he shouldn't?

21
00:05:08,050 --> 00:05:10,235
You always says he's your 
only connection to the living.

22
00:05:10,300 --> 00:05:13,700
Are you just going to let him die
if that's what's going to happen?

23
00:05:13,800 --> 00:05:15,701
Let's just follow him around.

24
00:05:15,800 --> 00:05:19,835
- You can do it because you're a freelancer...
- You can just take some sick days~!

25
00:05:19,900 --> 00:05:22,902
"Sick days?"
I'm dead.

26
00:05:32,800 --> 00:05:35,068
Hello? Helloo, sir.

27
00:05:36,070 --> 00:05:40,914
I was fell of my bike
and hurt my hip...

28
00:05:40,970 --> 00:05:44,770
It's really hard to get around...

29
00:05:45,800 --> 00:05:51,034
I'm sorry for waking you up.
Good night~

30
00:06:09,100 --> 00:06:12,779
Like clockwork, he wakes up 
ever morning at 6:30 am.

31
00:06:14,860 --> 00:06:18,800
And he runs the same empty route every day.

32
00:06:18,901 --> 00:06:23,567
He's never missed a workout
since his debut.

33
00:06:35,069 --> 00:06:38,160
For lunch, he eats the organic
lunches his fans send in.

34
00:06:38,263 --> 00:06:44,341
He always eats 2500 calories. Exactly.

35
00:06:58,000 --> 00:06:59,743
He doesn't even touch sweets.
36
00:07:00,100 --> 00:07:03,201
Instant food?
What's the point?

37
00:07:03,300 --> 00:07:06,167
That smell. You forget?

38
00:07:07,690 --> 00:07:08,625
Sorry.

39
00:07:10,700 --> 00:07:13,943
All of those comments were by students.
You really want to sue them?

40
00:07:16,900 --> 00:07:18,901
Yeah. All of them.

41
00:07:19,400 --> 00:07:21,628
Did you even have to ask?

42
00:07:23,100 --> 00:07:25,703
Bolder than I thought.

43
00:07:30,127 --> 00:07:34,193
What!? He might slip and fall in the shower!

44
00:07:35,200 --> 00:07:36,901
We can only protect him so much.

45
00:07:43,303 --> 00:07:48,068
It's been a few days but, he's clean.
Nothing dubious at all.

46
00:07:48,100 --> 00:07:53,368
Beer, cigarettes, women... I mean
he lives alone and he doesn't even watch porn.

47
00:07:53,426 --> 00:07:56,995
If there's nothing to kill him then...

48
00:07:57,098 --> 00:07:58,934
You don't think...?

49
00:08:00,000 --> 00:03:55,860
- Maybe an overdose?
- Hey, no way.

50
00:08:03,000 --> 00:08:09,935
Michael Jackson, Prince...that's how
all the big artist go.

51
00:08:11,800 --> 00:08:16,301
I mean, I thought his latest single
was just too... finished.

52
00:08:17,188 --> 00:08:20,422
What if he has needle marks?

53
00:08:26,000 --> 00:08:28,535
See? Amethysts, germanium ang titanium.


54
00:08:32,000 --> 00:08:33,067
Hyung.

55
00:08:34,865 --> 00:08:37,466
"Healthy jade chair mat."

56
00:08:42,011 --> 00:08:43,612
I hear jade is good for men.

57
00:08:46,600 --> 00:08:48,735
I don't think his health is a problem.

58
00:08:51,800 --> 00:08:53,134
Again!

59
00:08:55,700 --> 00:08:56,901
Let's do it again.

60
00:09:02,210 --> 00:09:03,500
Again.

61
00:09:06,000 --> 00:09:07,334
Again.

62
00:09:08,100 --> 00:09:09,035
Again.

63
00:09:44,500 --> 00:09:46,234
Let's stop here.

64
00:09:46,300 --> 00:09:47,901
Let's call it a day.

65
00:09:48,210 --> 00:09:54,500
We can keep going. And you could look my way,
we're singing a duet after all.

66
00:09:57,000 --> 00:10:01,334
Go home and get some rest.
Good work today.

67
00:10:04,100 --> 00:10:05,735
Water.

68
00:10:07,800 --> 00:10:11,934
- Hey, do you know how hard it was
to get that thug in here?
- "Thug!?"

69
00:10:12,000 --> 00:10:14,201
Of music!
Thug of music!

70
00:10:14,210 --> 00:10:19,500
- Wasn't that to much!? It's not trot music.
- What do you mean? It sounded fine!
 
71
00:10:19,600 --> 00:10:20,334
You want me to get the car?

72
00:10:22,300 --> 00:10:26,935
Like you've done anything.
You've never even said thank you to the staff!

73
00:10:27,000 --> 00:10:30,934
Forget that. If you're not going to retire,
then at least finish the song.

74
00:10:35,200 --> 00:10:37,201
Like love songs are hard...

75
00:10:43,210 --> 00:10:44,500
I'll write one.

76
00:10:49,010 --> 00:10:53,334
Hey! You're standing in a studio
that Wooju built and paid for!

77
00:10:53,400 --> 00:10:55,935
- What do you know about music?
- President!

78
00:10:57,100 --> 00:11:02,134
You want me to poke your eyes out?
Who are you speaking like that?!

79
00:11:04,200 --> 00:11:08,201
But he is a son of a bitch when he's working.

80
00:11:08,210 --> 00:11:09,100
More like a puppy...

81
00:11:34,200 --> 00:11:37,634
I only gave you the prescription
because I'm worried about you.

82
00:11:37,800 --> 00:11:42,935
Sleeping pills tend to form habits.
You should try to sleep without them.

83
00:11:42,940 --> 00:11:44,534
You're like a ghost...

84
00:11:44,900 --> 00:11:46,901
It's a woman.

85
00:11:49,010 --> 00:11:51,500
I'm sure sleep is hard,
with your single having tanked.

86
00:11:54,100 --> 00:11:56,834
Having someone next to you
would be the best cure.

87
00:11:56,900 --> 00:12:00,535
- You should at least get a dog.
- You make as much sense as a dog.

88
00:12:00,600 --> 00:12:03,134
Just because you talk like a dog
doesn't mean I'll adopt you.

89
00:12:03,200 --> 00:12:06,801
Hey, I have someone I'm interested in.

90
00:12:07,210 --> 00:12:10,500
And at least I'm trying,
you don't think you're a little strange?

91
00:12:10,900 --> 00:12:16,034
Locking yourself away in that big house.
You'll end up with ghosts.

92
00:12:17,100 --> 00:12:18,935
I'm Hang up.

93
00:12:19,100 --> 00:12:20,934
What Ghosts..

181
00:12:55,000 --> 00:13:000,000
-Sinc & corrected by KieKie Rizkya-

