1
00:00:14,680 --> 00:00:18,400
Listen, Baxter!
You can have them now if you like!

2
00:00:18,800 --> 00:00:21,200
Since dead men can't talk!

3
00:00:31,360 --> 00:00:34,720
It seems they didn't like my joke.

4
00:00:35,040 --> 00:00:36,920
We'll let them go home in a while.

5
00:02:17,600 --> 00:02:22,400
This bad boy will be our guest only until
Baxter leaves San Miguel for the frontier.

6
00:02:22,800 --> 00:02:24,200
Put him in the cellar.

7
00:02:30,000 --> 00:02:33,640
Esteban, good work!
With Antonio as our prisoner,

8
00:02:34,000 --> 00:02:37,960
the Baxters will have to give in to us.
Now we can make any kind of demand!

9
00:02:38,360 --> 00:02:40,600
Don Miguel! Don Miguel!

10
00:02:45,480 --> 00:02:47,720
What has happened? Talk, will you!

11
00:02:48,040 --> 00:02:50,480
I don't know who,
but somebody shot at me.

12
00:02:50,600 --> 00:02:52,520
Marisol!

13
00:02:53,400 --> 00:02:56,480
Marisol! Where are you, Marisol?!

14
00:03:10,360 --> 00:03:15,560
Strange... how you always manage
to be in the right place at the right time.

15
00:03:21,560 --> 00:03:26,560
It's nothing serious,
a headache that will pass. She'll sleep.

16
00:03:30,720 --> 00:03:35,480
Keep your eyes open. She's not to move
from that room, you hear?

17
00:03:41,440 --> 00:03:43,880
I wouldn't mention I brought her here.

18
00:03:44,040 --> 00:03:47,240
I wouldn't want the Rojos to think
I was on your side.

19
00:03:47,600 --> 00:03:50,200
Don't be worried. I'm rich enough

20
00:03:50,400 --> 00:03:54,200
to appreciate the men
that my money can buy.

21
00:03:54,360 --> 00:03:56,200
I've been to talk to the Rojos.

22
00:03:56,560 --> 00:04:00,520
They'll give back Antonio
in exchange for Marisol in the morning.

23
00:04:02,720 --> 00:04:05,520
Very soon, you're going to be rich.

24
00:04:06,720 --> 00:04:08,080
Uh-huh.

25
00:04:10,040 --> 00:04:13,680
Yeah, that's not gonna break my heart.

26
00:05:52,320 --> 00:05:56,800
Where is she?
You said I'd see her! Where is she?

27
00:05:57,040 --> 00:05:59,200
I want to see her!

28
00:05:59,840 --> 00:06:01,920
Come back here, Jesus. Come here.

29
00:06:02,200 --> 00:06:07,600
I want to see her!
Why can't I look at her? I want to see her!

30
00:06:26,520 --> 00:06:28,600
Go. Go to him, Marisol.

31
00:06:52,560 --> 00:06:53,400
Mama!

32
00:06:57,000 --> 00:06:59,000
- Mama!
- Jesus!

33
00:07:06,200 --> 00:07:07,000
Mama!

34
00:07:10,160 --> 00:07:12,760
- Jesus!
- Mama! Mama!

35
00:07:18,120 --> 00:07:19,320
Mama!

36
00:08:00,720 --> 00:08:02,640
Julio.

37
00:08:03,200 --> 00:08:04,640
Marisol.

38
00:08:05,160 --> 00:08:07,000
Get Julio.

39
00:08:17,960 --> 00:08:20,600
You were warned to get outta town.

40
00:08:20,760 --> 00:08:23,480
Now I'm gonna make you pay
once and for all.

41
00:08:25,120 --> 00:08:26,800
Let him be.

42
00:09:04,200 --> 00:09:06,840
You better get goin'. Ramon's waitin'.

43
00:09:11,000 --> 00:09:14,360
And you, get that kid home
where he belongs.

44
00:09:59,600 --> 00:10:01,600
A pig, that Ramon!

45
00:10:02,440 --> 00:10:04,760
Tell me, what's Ramon
got to do with them?

46
00:10:04,920 --> 00:10:07,120
You weren't told, eh?

47
00:10:09,200 --> 00:10:11,440
In these parts, the story is very old.

48
00:10:11,600 --> 00:10:15,760
A happy little family until trouble
comes along. Trouble is Ramon.

49
00:10:16,000 --> 00:10:19,440
Claiming the husband cheated at cards,
which wasn't true,

50
00:10:19,880 --> 00:10:22,400
he gets the wife
to live with him as hostage.

51
00:10:22,680 --> 00:10:23,680
The husband?

52
00:10:23,840 --> 00:10:26,960
Him, there's nothing for him to do.

53
00:10:27,200 --> 00:10:32,560
The Rojos threatened to kill his boy,
Jesus. He was forced to accept things.

54
00:10:34,360 --> 00:10:36,360
That Ramon is smart.

55
00:10:37,800 --> 00:10:39,920
Where are you going?

56
00:10:43,560 --> 00:10:45,920
To the Rojos, to look for a job.

57
00:10:48,680 --> 00:10:49,960
Poo!

58
00:10:51,640 --> 00:10:56,640
I'm glad we have the Americano with us.
It's the best thing to happen to us.

59
00:10:56,920 --> 00:11:01,880
If either government starts an enquiry,
we'll need every man we can get.

60
00:11:03,240 --> 00:11:08,400
To be at war with the Baxters now is
worse than sitting on a case of dynamite.

61
00:11:08,680 --> 00:11:11,200
- Where should we put this?
- Put it over there.

62
00:11:18,960 --> 00:11:20,960
Come on, everybody. Drink up!

63
00:11:51,760 --> 00:11:53,800
Good shooting. Very good shooting.

64
00:11:54,000 --> 00:11:58,760
To kill a man, you shoot for his heart,
and a Winchester is the best weapon.

65
00:11:59,080 --> 00:12:02,400
That's very nice, but I'll stick with my .45.

66
00:12:02,720 --> 00:12:06,120
When a man with a .45
meets a man with a rifle,

67
00:12:06,400 --> 00:12:09,200
the man with the pistol
will be a dead man.

68
00:12:09,440 --> 00:12:13,320
That's an old Mexican proverb,
and it's true.

69
00:12:13,840 --> 00:12:15,680
You believe that?

70
00:12:17,160 --> 00:12:21,440
Paquito, take five men
and escort Marisol to the small house.

71
00:12:30,800 --> 00:12:35,360
- Everything is prepared.
- Do not worry. I will return tomorrow.

72
00:12:35,760 --> 00:12:37,600
The wagons are ready. We can leave.

73
00:12:37,760 --> 00:12:39,200
Everyone!

74
00:12:39,400 --> 00:12:41,600
Enjoy yourselves during my absence!

75
00:12:56,960 --> 00:12:59,600
You heard Ramon,
let's have a good time.

76
00:13:24,720 --> 00:13:26,600
He weighs over a ton.

77
00:13:26,960 --> 00:13:30,480
Of course, with all he poured into himself.

78
00:14:42,360 --> 00:14:43,680
Papa,

79
00:14:43,960 --> 00:14:46,760
did you not tell me
no-one could go see Mama?

80
00:14:48,880 --> 00:14:50,880
That's right.

81
00:14:51,160 --> 00:14:55,440
Then, why does that man see her
when I cannot?

82
00:14:56,840 --> 00:15:00,080
- I want Mama!
- Come away from the door. Quiet.

83
00:15:01,840 --> 00:15:04,880
We cannot show ourselves.
You must stay away.

84
00:15:14,800 --> 00:15:15,800
Hello.

85
00:15:35,360 --> 00:15:37,720
There's somebody shooting
at the small house.

86
00:15:38,080 --> 00:15:41,080
Saddle the horses immediately!
Come on, let's go!

87
00:16:08,520 --> 00:16:09,120
Look out!

88
00:16:55,360 --> 00:17:00,800
Take this money. It's enough to live on
for a while. Now, get across the border.

89
00:17:01,160 --> 00:17:03,600
Get as far away from San Miguel
as possible.

90
00:17:03,840 --> 00:17:06,200
How may we thank you
for what you are doing?

91
00:17:06,520 --> 00:17:09,360
Don't try. Just get goin'
before the Rojos get here.

92
00:17:13,600 --> 00:17:15,400
Why do you do this for us?

93
00:17:16,520 --> 00:17:22,200
Why? I knew someone like you once.
No-one was there to help. Get movin'.

94
00:17:25,520 --> 00:17:26,920
Get movin'.

95
00:17:27,880 --> 00:17:29,840
Get outta here!

96
00:18:10,400 --> 00:18:12,320
What do you see, Chico?

97
00:18:12,640 --> 00:18:15,360
They're all dead. They massacred them.

98
00:18:15,600 --> 00:18:19,440
- They kidnapped Marisol!
- This looks like the work of the Baxters.

99
00:18:19,760 --> 00:18:23,600
Quick! Let's get back before
they attack our house in the town.

100
00:20:13,240 --> 00:20:15,720
Chico, give out the ammunition.

101
00:20:15,840 --> 00:20:19,400
Esteban, spread your men
around the house. Keep your eyes open.

102
00:20:19,720 --> 00:20:24,880
Miguel, Paco, behind the house.
Manolo and Alvaro, come with me.

103
00:20:25,040 --> 00:20:26,560
Stay alert!

104
00:20:26,640 --> 00:20:30,040
The way they killed those men,
there must be a lot of them.

105
00:20:37,720 --> 00:20:39,760
It's a warm evening.

106
00:20:42,920 --> 00:20:47,120
One of our wagons lost a wheel
so we had to turn back here to get help.

107
00:20:48,960 --> 00:20:54,080
One of the men tells me...
the Baxters attacked the small house.

108
00:20:56,240 --> 00:20:58,320
Do you know about it?

109
00:21:01,480 --> 00:21:03,600
Tell me what you know.

110
00:20:58,760 --> 00:21:00,440
Oh!

111
00:21:17,600 --> 00:21:19,160
Well?

112
00:21:20,120 --> 00:21:22,520
Where did you hide Marisol?

113
00:21:24,520 --> 00:21:26,080
Rubio.

114
00:21:37,320 --> 00:21:38,680
Bring him over here.

115
00:21:58,240 --> 00:22:02,320
You are a stubborn idiot. Tell us
where Marisol is and it'll all be ended.

116
00:22:02,480 --> 00:22:06,960
In a week, you'll be back in shape,
and you can go wherever you want.

117
00:22:14,760 --> 00:22:16,840
That's enough for today.

118
00:22:17,120 --> 00:22:20,240
Sooner or later he'll talk.
It's just a matter of time.

119
00:22:20,480 --> 00:22:23,000
He mustn't escape or die.
Otherwise, do as you like.

120
00:22:27,840 --> 00:22:31,200
Wait till he comes to.
This way he hardly feels anything.

121
00:22:53,840 --> 00:22:59,200
I have a poker hand waiting for me.
Now I am stuck here! I can't get back to it!

122
00:22:59,600 --> 00:23:03,800
This is more amusing than cards.
Just be sure he does not die,

123
00:23:04,000 --> 00:23:07,240
and also make sure
he regrets the day he was born.

124
00:23:09,240 --> 00:23:13,760
The gringo is asleep, I imagine.
That's all right. I'll enjoy waking him up.

125
00:23:34,840 --> 00:23:36,800
Esteban, what was that noise?

126
00:23:37,000 --> 00:23:40,040
I do not know.
I heard a yell and then a crash.

127
00:24:11,000 --> 00:24:12,760
It's useless to hide!

128
00:24:14,240 --> 00:24:16,640
You are finished playing the smart boy.

129
00:24:18,640 --> 00:24:21,320
Go up and look in the loft!

130
00:24:21,920 --> 00:24:24,800
And take a look behind the barrels!

131
00:24:35,360 --> 00:24:37,600
Let's get out of here!

132
00:24:46,800 --> 00:24:48,720
Hurry! Hurry!

133
00:24:49,080 --> 00:24:51,840
- Come on!
- You, come along with me!

134
00:25:06,600 --> 00:25:11,960
Surround the town! Block the streets
leading out! Find him for me!

135
00:25:13,120 --> 00:25:16,560
- Esteban, search the whole town!
- Follow me!

136
00:25:17,440 --> 00:25:20,760
Get him back right now!
You've got to at any cost!

137
00:25:21,000 --> 00:25:24,400
Look in the stables, in every corral,
in the store!

138
00:25:24,720 --> 00:25:28,680
Look for him in the church!
Go in there! Search every inch of it!

139
00:25:29,040 --> 00:25:31,840
Search everywhere! Rubio, in here!

140
00:25:33,240 --> 00:25:37,800
If somebody is hiding him,
burn their house down and kill them!

141
00:25:38,080 --> 00:25:39,960
You, come here!

142
00:25:40,080 --> 00:25:43,800
Look for him near the Baxters',
but take him alive. I want him alive!

143
00:25:44,200 --> 00:25:45,600
Alive!

144
00:25:46,800 --> 00:25:48,920
Rubio,... in the bar.

145
00:26:18,400 --> 00:26:21,400
He says he knows nothing.
He's being smart.

146
00:26:21,760 --> 00:26:23,800
Bring him here to me.

147
00:26:25,360 --> 00:26:27,360
I swear I'm telling the truth.

148
00:26:27,520 --> 00:26:30,160
Well,... you know nothing, huh?

149
00:26:30,600 --> 00:26:32,640
Nothing! I have not seen him!

150
00:26:32,760 --> 00:26:35,160
Rubio, you take care of him!

151
00:27:14,680 --> 00:27:17,080
Where have you hidden him?

152
00:27:17,920 --> 00:27:20,160
I... didn't... hide...

153
00:27:24,640 --> 00:27:27,720
You are the right friend
for that filthy Americano.

154
00:27:28,040 --> 00:27:30,000
You'll end up the same way.

155
00:27:30,240 --> 00:27:32,240
We looked everywhere.

156
00:27:32,400 --> 00:27:36,200
In his cellar, in the attic, on the roof,
but there is no trace.

157
00:27:37,080 --> 00:27:39,520
I couldn't find anybody in the back room.

158
00:27:39,880 --> 00:27:44,000
He has taken refuge with the Baxters.
The Americano isn't stupid.

159
00:27:44,320 --> 00:27:47,560
He knows that's the best place to hide.

160
00:27:49,200 --> 00:27:53,200
The Baxters, eh?
Then better go and get him.

161
00:28:14,060 --> 00:28:15,780
Curse them!

162
00:28:21,220 --> 00:28:22,860
Piripero!

163
00:28:23,180 --> 00:28:24,500
Eh?

164
00:28:31,180 --> 00:28:33,140
- Piripero!
- Hm?

165
00:28:43,500 --> 00:28:46,500
- Come here.
- I can't see anybody.

166
00:28:47,020 --> 00:28:49,460
- Come here.
- Dios mio!

167
00:28:57,500 --> 00:29:00,060
What are you doing in there?

168
00:29:00,540 --> 00:29:03,020
Never mind. Get me outta here.

169
00:29:03,100 --> 00:29:05,380
But you're not dead yet.

170
00:29:05,500 --> 00:29:10,180
I will be, if you don't get me
outta here quick. Get the lid down.

171
00:30:32,980 --> 00:30:34,020
Rubio.

172
00:30:55,180 --> 00:30:57,460
I see some signs of life.

173
00:30:57,820 --> 00:31:01,300
Let's be ready
when they decide to come out.

174
00:31:02,780 --> 00:31:07,180
Don't shoot!
We surrender! Don't shoot!

175
00:31:10,340 --> 00:31:12,820
Don't shoot! We're coming out!

176
00:31:15,860 --> 00:31:17,860
I surrender! Hold back your fire!

177
00:31:18,140 --> 00:31:20,860
Don't shoot! We're surrendering!

178
00:31:48,980 --> 00:31:51,780
Stop. I wanna take a look at this.

179
00:32:03,500 --> 00:32:07,060
Ramon! Don't shoot!
We're comin' out.

180
00:32:10,380 --> 00:32:12,060
We surrender!

181
00:32:12,220 --> 00:32:15,660
Listen, you've won! It's enough!
I'll get outta town!

182
00:32:15,900 --> 00:32:17,660
I'll do whatever you like!

183
00:32:17,980 --> 00:32:20,420
- You promise that?
- I swear it, Ramon!

184
00:32:20,620 --> 00:32:22,300
You won't try any tricks?

185
00:32:22,540 --> 00:32:27,500
None. No, no tricks. I said before,
I give you my word, we'll leave!

186
00:32:27,980 --> 00:32:31,540
Are you sure? You had better
ask permission of your wife.

187
00:32:32,700 --> 00:32:34,580
Maybe she won't be too happy!

188
00:32:48,260 --> 00:32:50,900
Antonio! John!

189
00:33:16,100 --> 00:33:17,700
Murderers.

190
00:33:18,940 --> 00:33:21,100
They had no guns!

191
00:33:21,460 --> 00:33:24,260
Murderers! I hope you rot in hell!

192
00:33:24,540 --> 00:33:27,580
May you all die spitting blood!
Curse you for this!

193
00:33:27,860 --> 00:33:30,020
Murderers!

194
00:33:52,180 --> 00:33:54,300
Let's go. The show's over anyway.

195
00:33:58,300 --> 00:34:01,300
Keep looking for him!
Search among those bodies!

196
00:34:02,660 --> 00:34:05,060
The filthy Americano
has to be somewhere.

197
00:36:17,780 --> 00:36:18,660
Silvanito?

198
00:36:23,900 --> 00:36:25,740
Where's Silvanito?

199
00:36:28,740 --> 00:36:30,580
Any news today?

200
00:36:30,700 --> 00:36:34,860
I have to tell you some bad news.
I hate to tell you this, Joe, but...

201
00:36:35,100 --> 00:36:36,900
Where is Silvanito?

202
00:36:37,940 --> 00:36:41,940
He was captured this morning
by those men of Ramon's.

203
00:36:42,140 --> 00:36:44,820
They grabbed him on his way here.

204
00:36:45,060 --> 00:36:49,780
He was bringing in some provisions.
He is being tortured, but he won't talk.

205
00:36:50,180 --> 00:36:51,260
He's stubborn.

206
00:36:51,300 --> 00:36:55,540
Whatever they do, he won't say a word,
even if it means his life.

207
00:36:55,980 --> 00:36:58,660
Listen to me.
They'll never be able to force him.

208
00:37:09,860 --> 00:37:13,540
Now, old fool, loosen up your tongue.

209
00:37:16,100 --> 00:37:17,100
No.

210
00:37:17,460 --> 00:37:18,900
Rubio!

211
00:37:20,460 --> 00:37:24,100
Get back to your shop.
You're liable to have business there.

212
00:37:24,500 --> 00:37:27,060
Very good! That's what I wanted to hear.

213
00:37:27,220 --> 00:37:32,220
I brought a little surprise for you.
I know how much you will need it.

214
00:37:32,460 --> 00:37:35,860
It was hard to get my hands on it.
I got it by using my head.

215
00:37:36,180 --> 00:37:39,900
No-one can resist two bottles of wine.

216
00:37:40,060 --> 00:37:42,900
I thought you might need that pistol.

217
00:37:43,220 --> 00:37:45,820
And I brought another present.

218
00:37:46,620 --> 00:37:48,180
It's dynamite.

219
00:37:48,340 --> 00:37:50,620
I stole it from the Rojos.

220
00:37:50,940 --> 00:37:54,500
The moment's come
for you to light the fuse

221
00:37:54,900 --> 00:37:58,380
and send it back to them.

222
00:38:14,540 --> 00:38:16,540
Still lots of light.

223
00:38:20,100 --> 00:38:21,740
Rubio, here.

224
00:38:27,140 --> 00:38:31,540
I shouldn't like to ruin the rifle.
I could never find another like this one.

225
00:38:32,940 --> 00:38:35,700
Later, Ramon.
Let's try it with this one now.

226
00:40:32,500 --> 00:40:34,020
Gringo!

227
00:40:40,700 --> 00:40:42,700
I heard you wanted to see me.

228
00:40:43,260 --> 00:40:45,060
The Americano's dead.

229
00:40:47,180 --> 00:40:49,220
Cut the old man down.

230
00:41:06,700 --> 00:41:10,500
What's wrong, Ramon?
You losing your touch?

231
00:41:18,660 --> 00:41:20,300
Are you afraid, Ramon?

232
00:41:46,060 --> 00:41:49,580
The heart, Ramon. Don't forget the heart.

233
00:41:52,380 --> 00:41:54,500
Aim for the heart or you'll never stop me.

234
00:43:28,220 --> 00:43:31,340
When a man with a .45
meets a man with a rifle,

235
00:43:31,540 --> 00:43:33,940
you said the man with the pistol's
a dead man.

236
00:43:35,700 --> 00:43:37,860
Let's see if that's true.

237
00:44:00,540 --> 00:44:02,660
Go ahead. Load up and shoot.

238
00:46:01,060 --> 00:46:04,100
Hey. Hey, listen, Joe.

239
00:46:05,500 --> 00:46:07,540
Listen, Joe, l... I...

240
00:46:09,700 --> 00:46:11,540
Oh, Joe... Joe...

241
00:46:43,060 --> 00:46:46,620
I guess your government'll be glad
to see that gold back.

242
00:46:46,940 --> 00:46:50,540
And you? You don't want to be here
when they get it, eh?

243
00:46:52,700 --> 00:46:55,540
You mean,
the Mexican government on one side,

244
00:46:55,900 --> 00:47:00,980
maybe the Americans on the other,
and me, right smack in the middle?

245
00:47:01,300 --> 00:47:03,300
Uh-uh. Too dangerous.

246
00:47:04,260 --> 00:47:05,900
So long.

247
00:47:06,060 --> 00:47:07,460
Adios.

