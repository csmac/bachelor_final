1
00:10:17,416 --> 00:10:18,542
Good morning, guys.

2
00:10:18,651 --> 00:10:20,175
Good morning!

3
00:10:24,156 --> 00:10:26,147
Under these
particular circumstances,

4
00:10:26,258 --> 00:10:29,250
we can easily discover
many shortcomings.

5
00:10:29,562 --> 00:10:35,660
They complain about us every day.
Watch the way you operate.

6
00:10:36,135 --> 00:10:39,764
Also, watch those knobs for controlling
temperature and those for steam.

7
00:10:40,806 --> 00:10:42,433
Don't laugh.

8
00:10:42,742 --> 00:10:44,209
Don't talk.

9
00:10:46,245 --> 00:10:51,683
You, stand behind him. Four in a row.
Stand straight. You, come to this row.

10
00:10:52,685 --> 00:10:59,113
Regarding the unqualified material,
our group, 2B, did the worst.

11
00:11:00,359 --> 00:11:03,328
Every time we worked on 7562,
if I didn't remind you,

12
00:11:03,429 --> 00:11:08,867
then nobody would label the bad items,
so they got messed up.

13
00:11:09,769 --> 00:11:11,634
They were scattered everywhere.

14
00:11:11,737 --> 00:11:16,470
Besides this, the worst thing,
there were no labels for them.

15
00:11:23,382 --> 00:11:28,752
So when the products were moved,
the bad ones went out as well.

16
00:11:29,789 --> 00:11:33,350
I'm not a god.
I'm a human being, just like you.

17
00:11:33,459 --> 00:11:36,394
I am only 1 person and you are 20.

18
00:11:37,897 --> 00:11:41,492
When those materials were rejected,
the quality controller complained a lot.

19
00:11:42,234 --> 00:11:45,226
This is the bad habit that exists
in our group, 2B.

20
00:13:37,016 --> 00:13:41,453
It should be good. No problem.
He is shooting now.

21
00:21:49,107 --> 00:21:51,507
We haven't sorted
these recycling materials in time.

22
00:21:51,610 --> 00:21:53,908
Now there's too much to do.

23
00:21:54,012 --> 00:21:57,914
You wouldn't start working
until you were told to.

24
00:21:59,084 --> 00:22:01,882
You don't want to work?

25
00:26:01,359 --> 00:26:03,418
It's not useable anymore.

26
00:26:04,362 --> 00:26:07,456
So after you sort them out,
will you smash all of them?

27
00:26:07,599 --> 00:26:09,260
Yes. All will be smashed.

28
00:29:48,753 --> 00:29:53,713
First, welcome to our company.
This is the headquarter office.

29
00:29:54,459 --> 00:29:58,919
During the past 1 3 years, our company
has been expanding steadily.

30
00:30:00,965 --> 00:30:07,734
Our product is sold in more than 60
countries and territories around the world.

31
00:30:09,741 --> 00:30:12,676
Our products cover from the origin

32
00:30:12,777 --> 00:30:16,679
to the terminal of low voltage
electrical safeguards,

33
00:30:16,981 --> 00:30:22,283
which includes the
TSM1 Smart Breaker,

34
00:30:22,387 --> 00:30:25,879
Smart and Multi-functional Breaker...

35
00:30:28,026 --> 00:30:33,293
and the TSM21 Breaker,
TSM Small Breaker,

36
00:30:33,398 --> 00:30:37,892
and the TSY Power Supply Protector.

37
00:31:06,030 --> 00:31:09,488
My name is Tan Yanfang.

38
00:31:10,001 --> 00:31:11,593
How long have you been working here?

39
00:31:11,703 --> 00:31:13,671
Six years.

40
00:31:16,007 --> 00:31:19,909
I can assemble 400 units every day
without overtime.

41
00:31:30,922 --> 00:31:33,789
The shell of the TSM Small Breaker

42
00:31:34,492 --> 00:31:38,428
is made of the best quality materials
in the world,

43
00:31:38,563 --> 00:31:44,433
including specialized parts
imported from Holland.

44
00:31:46,704 --> 00:31:53,041
The products are mainly for...
Excuse me...

45
00:31:54,512 --> 00:31:56,980
I feel so nervous.

46
00:31:57,582 --> 00:31:58,810
That's okay.

47
00:31:59,250 --> 00:32:01,514
Can I use...

48
00:32:02,086 --> 00:32:04,680
Can I use my notes?

49
00:32:04,789 --> 00:32:06,689
You can read. That's fine.

50
00:32:07,759 --> 00:32:11,559
We will keep updating our marketing,
focussing on technical research,

51
00:32:12,730 --> 00:32:14,789
to serve our customers best.

52
00:32:17,235 --> 00:32:19,226
Professional, high-quality,
and large-scale work

53
00:32:19,337 --> 00:32:22,170
will make our company
an industry leader.

54
00:32:24,776 --> 00:32:28,177
Our corporate goal is to be exceptional.
Professionalism is our principle.

55
00:36:49,640 --> 00:36:52,666
Changes here are huge.

56
00:36:53,144 --> 00:36:55,908
My father told me when
he came here in the early '90s,

57
00:36:56,013 --> 00:37:00,973
there were no ships being built.
Now it's getting richer here.

58
00:37:06,924 --> 00:37:09,586
My mother is a welder, just like me.
My father is an assembler.

59
00:37:10,261 --> 00:37:14,425
I failed to get into high school
and my uncle worked here.

60
00:37:14,532 --> 00:37:16,932
That's why I came here to work.

61
00:49:21,892 --> 00:49:24,884
There are lots of coal mines here.

62
00:49:28,899 --> 00:49:33,029
Coal is mined on a large scale
to meet the energy needs of our country,

63
00:49:33,136 --> 00:49:36,628
because our economy
is developing very fast.

64
00:49:36,740 --> 00:49:42,406
But it brings some
environmental problems.

65
00:49:43,981 --> 00:49:47,417
Did you just say
that large-scale coal mining

66
00:49:47,551 --> 00:49:52,750
is to meet China's demands
in her economic development?

67
00:49:53,623 --> 00:49:56,558
We came here for that.

68
00:49:57,427 --> 00:50:00,555
I have to let you know
that he's going to publish a book

69
00:50:00,664 --> 00:50:04,065
about the development
of Chinese industry...

70
00:50:06,770 --> 00:50:15,109
and we understand that China
needs a lot of energy for development.

71
00:50:16,146 --> 00:50:18,080
No shooting. No shooting.

72
00:50:18,615 --> 00:50:19,877
He's just taking a picture.

73
00:50:20,017 --> 00:50:20,984
Don't shoot, guys.

74
00:50:22,652 --> 00:50:23,949
- Excuse me.
-Yeah.

75
00:50:24,054 --> 00:50:28,047
As far as I understand,
the problem right now...

76
00:50:28,158 --> 00:50:30,353
- I mean this is my personal idea...
- Okay.

77
00:50:30,460 --> 00:50:35,727
is that the court itself
has the very regulation

78
00:50:35,899 --> 00:50:41,098
that no media is allowed
to come to this.

79
00:50:41,204 --> 00:50:44,071
No I understand that.
But my understanding is,
Tianjin Harbour Goods Distribution Centre

80
00:50:44,174 --> 00:50:45,937
from Mr. Chendling this morning

81
00:50:46,043 --> 00:50:48,477
- I think, maybe he...
-You know? Yeah, no, no. That's okay.

82
00:50:48,578 --> 00:50:53,743
Maybe Mr. Chendling does not know
that there is a certain regulation that...

83
00:50:56,586 --> 00:50:58,417
I understand it's quite windy today.

84
00:51:00,490 --> 00:51:02,287
It's very dirty, very dirty.

85
00:51:02,526 --> 00:51:04,858
I don't think it's a good day
to make beautiful pictures.

86
00:51:05,362 --> 00:51:07,956
It's very gloomy here.

87
00:51:10,333 --> 00:51:17,671
But through his camera lens, through
his eyes, it will appear beautiful.

88
00:51:19,209 --> 00:51:21,109
For example, even this industrial
waste, it's kind of like garbage.

89
00:51:21,211 --> 00:51:24,942
Still it appears beautiful
through his camera.

90
00:51:25,582 --> 00:51:28,107
Take a look at these pictures.

91
00:51:29,553 --> 00:51:33,011
This book is a retrospective
of his past 20 years' work.

92
00:51:35,358 --> 00:51:38,020
Most of the pictures
were taken in Canada.

93
00:51:38,128 --> 00:51:40,119
Here are some pictures of mines.

94
00:51:42,265 --> 00:51:47,897
The book we are making
needs a picture...

95
00:51:50,874 --> 00:51:53,468
Actually, our concern is that it will
cause some negative influence.

96
00:51:54,544 --> 00:51:56,375
Let's just go to the site
and have a look.

97
00:55:39,202 --> 00:55:42,865
Workers, before you go to the site,

98
00:55:43,006 --> 00:55:47,875
please check that you are wearing
safety equipment or proper clothing.

99
00:55:48,545 --> 00:55:51,946
Please check if
there's any potential danger

100
00:55:52,048 --> 00:55:55,950
and if you are clear about today's work.

101
00:56:00,357 --> 00:56:07,923
Today's highest temperature:
26 to 27 centigrade; lowest: 1 5 to 1 6.

102
00:56:19,075 --> 00:56:20,599
- Old Master? How are you?
- Yes.

103
00:56:20,710 --> 00:56:23,440
- Are you working here?
-Yes.

104
00:56:23,546 --> 00:56:25,810
How about the work? Is it hard?

105
00:56:26,082 --> 00:56:28,516
Of course it's hard.

106
00:56:29,519 --> 00:56:31,510
- 20 Yuan.
- What?

107
00:56:33,590 --> 00:56:35,683
- 20, uh, 30 Yuan.

108
00:56:36,559 --> 00:56:39,187
So, 30 Yuan a day? Oh.

109
00:56:43,233 --> 00:56:45,793
What do you think of this project?

110
00:56:45,902 --> 00:56:49,963
Are you proud of your involvement
on such a huge project?

111
00:56:50,740 --> 00:56:52,867
I'm just a general labourer here.

112
00:56:53,276 --> 00:56:55,938
We are just working for our boss,
getting paid here.

113
00:57:00,617 --> 00:57:04,485
We have three shifts a day:
day shift, mid shift and night shift.

114
00:57:04,788 --> 00:57:06,949
How long have you
been working here?

115
00:57:07,190 --> 00:57:08,179
I've been working here nine years.

116
00:57:08,525 --> 00:57:10,550
Nine years? Wow.

117
00:57:10,960 --> 00:57:18,025
It's just work. It's all for our country.

118
00:58:06,850 --> 00:58:11,514
The project was officially launched
on December 14, 1 994.

119
00:58:11,855 --> 00:58:16,019
It has been almost ten years.

120
00:58:16,259 --> 00:58:21,788
It will take 1 7 years to finish,
ending in 2009.

121
00:58:23,132 --> 00:58:30,163
This is the biggest dam
that has ever been made in the world

122
00:58:30,273 --> 00:58:33,674
and probably ever will be made.

123
00:58:33,810 --> 00:58:36,438
There are three main functions
of the dam.

124
00:58:36,546 --> 00:58:39,913
The first is to avoid floods.
That's the most crucial function.

125
00:58:40,016 --> 00:58:43,543
The second
is to generate electricity.

126
00:58:43,686 --> 00:58:46,712
The third
is to improve transportation.

127
00:58:48,791 --> 00:58:50,418
That's it.

128
00:59:04,040 --> 00:59:09,137
The full length of the dam
is 2,309 metres.

129
00:59:09,979 --> 00:59:15,576
There will be
26 generators installed...

130
00:59:16,819 --> 00:59:25,284
as well as 6 underground generators,
as back-up, which makes 32 total.

131
00:59:27,096 --> 00:59:33,729
Each one generates 700,000 kW,
with a total capacity of 22,400,000 kW

132
00:59:33,836 --> 00:59:40,833
which supplies 84.7 billion kWh per year.

133
01:00:03,266 --> 01:00:10,172
How many people have to relocate
due to the construction of the dam?

134
01:00:10,740 --> 01:00:17,612
According to the statistics
we got in the 1 992 census, 830,000,

135
01:00:17,714 --> 01:00:23,675
but considering population growth,
the actual amount is around 1,1 00,000.

136
01:00:26,155 --> 01:00:29,022
Are these people content
with their new homes?

137
01:00:30,560 --> 01:00:34,189
Do not ask me this question.
I'm not in charge of this.

138
01:01:18,574 --> 01:01:20,872
Yangtze RiverCities,
China, 2002

139
01:06:19,108 --> 01:06:21,838
It's a very broad view.

140
01:06:22,411 --> 01:06:24,675
It's hard to see the details.

141
01:10:57,586 --> 01:11:06,494
Because our country was poor in the
past, people's life standards were low.

142
01:11:08,464 --> 01:11:13,492
Compared with the old city,
the new city has been improved

143
01:11:13,769 --> 01:11:16,294
regarding the scale
and construction quality.

144
01:11:19,975 --> 01:11:27,677
Generally speaking, life quality
has been improved as well.

145
01:11:28,751 --> 01:11:34,553
Because people feel happy
about the fresh environment.

146
01:12:43,992 --> 01:12:45,653
You have so much free time
that you can wash your dog?

147
01:12:52,134 --> 01:12:55,399
These houses were
basically old houses.

148
01:12:55,504 --> 01:13:00,339
We were old neighbours.
We've been neighbours for decades.

149
01:13:02,711 --> 01:13:11,016
My father and mother grew up here...

150
01:13:13,856 --> 01:13:16,620
and I was born here.

151
01:13:17,426 --> 01:13:18,950
I'm more than 60 years old now.

152
01:14:23,091 --> 01:14:26,583
Go further. You will find
an interesting place to shoot.

153
01:15:09,171 --> 01:15:13,767
They came door to door
to persuade you to move, one by one;

154
01:15:14,643 --> 01:15:17,077
sometimes they
deceived people into going.

155
01:15:17,179 --> 01:15:18,806
Sometimes they coaxed them.

156
01:16:04,526 --> 01:16:11,955
Often they beat people up.
One guy here got broken bones.

