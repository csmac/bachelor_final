1
00:00:19,520 --> 00:00:22,717
(Emilie Simon)
"All is white"

2
00:00:23,040 --> 00:00:24,878
All is white

3
00:00:24,880 --> 00:00:26,996
I think it snowed
in the night

4
00:00:27,240 --> 00:00:30,755
Everything is cold
So cold outside

5
00:00:31,000 --> 00:00:34,754
I listen to the wind
All alone

6
00:00:34,960 --> 00:00:38,635
I know it means
the storm will come

7
00:00:38,880 --> 00:00:42,793
I want to live in paradise

8
00:00:43,000 --> 00:00:46,959
I want to live in the South

9
00:00:47,160 --> 00:00:50,835
I want to live in paradise

10
00:00:51,080 --> 00:00:54,675
At the south of the earth tonight

11
00:00:59,480 --> 00:01:03,314
All is white
I know it snowed in the night

12
00:01:03,520 --> 00:01:07,274
Everything is cold
So cold outside

13
00:01:07,520 --> 00:01:10,956
I try to be calm
It's a lonely trip

14
00:01:11,440 --> 00:01:15,194
Listen to my eyes
Listen to my lips

15
00:01:15,440 --> 00:01:19,035
I want to live in paradise

16
00:01:19,240 --> 00:01:23,233
I want to live in the South

17
00:01:23,440 --> 00:01:27,115
I want to live in paradise

18
00:01:27,360 --> 00:01:31,035
At the south of the earth tonight

19
00:02:36,120 --> 00:02:38,315
Once upon a time a garden

20
00:02:39,240 --> 00:02:43,153
A fertile world where life was a given

21
00:02:43,560 --> 00:02:47,235
A long time ago - before winter

22
00:02:47,760 --> 00:02:50,638
But one day, everything was blanked

23
00:02:51,280 --> 00:02:53,077
plants, animals

24
00:02:53,880 --> 00:02:54,995
Those who could flee

25
00:02:55,280 --> 00:02:58,636
did so, but our ancestors
decided to stay.

26
00:02:58,960 --> 00:02:59,995
At any cost

27
00:03:00,520 --> 00:03:03,114
And to resist tothis frost
which was taking everything

28
00:03:06,000 --> 00:03:07,479
Time has passed

29
00:03:07,680 --> 00:03:10,114
100 times, Earth has changed its face

30
00:03:10,760 --> 00:03:13,115
We are still there

31
00:03:13,800 --> 00:03:15,597
We stand, light sentinels.

32
00:03:16,800 --> 00:03:18,119
Here is our story.

33
00:03:50,880 --> 00:03:52,279
Where are the others?

34
00:04:49,280 --> 00:04:51,396
I feel dizzy
leaving the Ocean.

35
00:04:51,600 --> 00:04:54,319
Here we are, our two feets
in the snow,

36
00:04:54,520 --> 00:04:56,033
in this country
too large for us.

37
00:04:56,240 --> 00:04:57,355
Are we all here?

38
00:04:58,160 --> 00:04:59,559
Everybody's here?

39
00:05:27,080 --> 00:05:29,719
A day passes,
and another...

40
00:05:30,160 --> 00:05:31,434
and the third day...

41
00:05:35,400 --> 00:05:37,595
We gathered all here today,

42
00:05:37,840 --> 00:05:39,956
from the straightest to the
hunchbakcs, all of us

43
00:05:40,520 --> 00:05:42,078
Waiting for the signal

44
00:05:43,640 --> 00:05:44,595
The signal...

45
00:05:44,880 --> 00:05:45,995
...comes at last.

46
00:06:16,360 --> 00:06:18,316
In this great day,
One third into the year,

47
00:06:18,560 --> 00:06:21,597
when the moon and the sun
meet at half day,

48
00:06:21,800 --> 00:06:25,076
we, swimmers, stand up
and start to walk.

49
00:06:33,440 --> 00:06:36,195
Our kind is forever shared between

50
00:06:36,280 --> 00:06:38,396
the ocean which feeds us

51
00:06:38,840 --> 00:06:40,478
and on the other side

52
00:06:40,800 --> 00:06:43,997
beyond the great plain
the ice covered ocean,

53
00:06:44,240 --> 00:06:45,434
...a meeting.

54
00:06:46,240 --> 00:06:48,037
We've got a rendez vous.

55
00:06:51,360 --> 00:06:53,157
Now standing up

56
00:06:53,440 --> 00:06:55,556
we walk, like nomads,

57
00:06:55,800 --> 00:06:57,916
we walk one day,
two days

58
00:06:58,160 --> 00:07:00,037
ten days, twenty days.

59
00:07:00,280 --> 00:07:02,475
The same number of nights
the same number of cold nights

60
00:07:03,360 --> 00:07:05,476
Here comes the time
of the first walk

61
00:07:05,680 --> 00:07:07,796
The walk of the long caravans

62
00:07:44,840 --> 00:07:47,195
Above us, the sky's
paths are shining

63
00:07:47,960 --> 00:07:51,475
The sun, the stars draw
eternal maps

64
00:07:51,680 --> 00:07:54,513
Under our feets, Earth's
heart humms...

65
00:07:54,720 --> 00:07:57,518
Humms its beautiful
magnetic fields

66
00:08:15,280 --> 00:08:19,378
Each year,
our trek takes a new turn

67
00:08:19,400 --> 00:08:21,675
Icebergs are like giant tramps

68
00:08:21,920 --> 00:08:23,353
They sleep wherever winter catches
up with them!

69
00:08:23,560 --> 00:08:25,915
Sometimes we have to make long detours

70
00:08:26,120 --> 00:08:27,838
Going around the sleeping titans.

71
00:08:28,080 --> 00:08:30,275
But even the oldest amongst us

72
00:08:30,560 --> 00:08:33,518
cannot remember ever being lost

73
00:09:48,880 --> 00:09:51,838
Despite the cold
engulfing the plain

74
00:09:52,200 --> 00:09:54,077
despite the dreamy mountains' walls

75
00:09:54,320 --> 00:09:56,595
caravans from all 4 corners of the horizon

76
00:09:56,840 --> 00:09:59,559
are gathering - nearly
the same day

77
00:10:00,200 --> 00:10:02,634
at the same time, at the same place.

78
00:10:03,320 --> 00:10:04,594
As if by magic.

79
00:10:11,200 --> 00:10:14,317
10 days pass
and thousands of steps

80
00:10:14,640 --> 00:10:17,837
And another 10 days
another thousands of steps

81
00:10:18,920 --> 00:10:20,194
...and one morning

82
00:10:21,400 --> 00:10:23,516
Our people gets
to the end of the trip

83
00:10:23,720 --> 00:10:27,633
Here are the stone doors
that witnessed our birth

84
00:10:27,920 --> 00:10:29,558
Since the dawn of times

85
00:11:07,440 --> 00:11:10,477
It's here, on the Oamock (river?)
that we meet

86
00:11:10,680 --> 00:11:12,398
every year to give life.

87
00:11:13,160 --> 00:11:16,118
No place is safer

88
00:11:16,360 --> 00:11:18,555
Protected by granite walls

89
00:11:18,760 --> 00:11:20,398
and the ice beneath our feet

90
00:11:20,640 --> 00:11:23,393
will not break until summer.

91
00:11:58,360 --> 00:12:01,033
No more order, no more steps
we all freely roam the crowd

92
00:12:01,240 --> 00:12:03,196
...looking for our soulmate.

93
00:12:03,440 --> 00:12:05,795
Love songs cover the Oamock

94
00:12:06,000 --> 00:12:08,434
We marry for the whole year

95
00:12:08,800 --> 00:12:10,358
to a single partner.

96
00:12:36,680 --> 00:12:39,478
Every yearit's the same.
As soon as we arrive...

97
00:12:39,760 --> 00:12:40,954
...it's slapping time.

98
00:12:41,200 --> 00:12:42,918
More females than males!

99
00:12:43,160 --> 00:12:46,596
Females fierciously fight for us :-)

100
00:12:49,160 --> 00:12:51,594
A new couple?
They intervene.

101
00:12:51,840 --> 00:12:55,515
A married male is a lost male...

102
00:12:55,840 --> 00:12:58,718
So they dive in,
push their way in.

103
00:13:01,680 --> 00:13:04,319
Other couples wait.

104
00:14:14,800 --> 00:14:15,755
Those who, like us

105
00:14:16,040 --> 00:14:17,837
have already found
each other...

106
00:14:18,080 --> 00:14:21,197
...isolate themselves in the intimacy
of songs and parades.

107
00:14:23,520 --> 00:14:26,876
Our nuptial dance
is opening the big winter's bal

108
00:14:27,520 --> 00:14:31,513
(Emilie Simon)
"The Frozen World"

109
00:14:31,760 --> 00:14:34,433
Won't you open for me

110
00:14:35,200 --> 00:14:38,715
The door to your ice world

111
00:14:39,320 --> 00:14:43,279
To your white desert

112
00:14:47,200 --> 00:14:50,078
I just want to stare

113
00:14:50,520 --> 00:14:53,637
Out over these snowfields

114
00:14:54,520 --> 00:14:58,035
Until we are one again

115
00:14:58,440 --> 00:15:02,035
We belong to

116
00:15:02,240 --> 00:15:05,994
the frozen world

117
00:15:09,200 --> 00:15:11,555
When the ice

118
00:15:11,920 --> 00:15:14,957
begins to thaw

119
00:15:15,600 --> 00:15:18,876
Becomes the sea

120
00:15:27,520 --> 00:15:30,080
You will see

121
00:15:30,840 --> 00:15:35,595
How beautiful we can be

122
00:15:58,160 --> 00:16:01,038
You will see

123
00:16:01,400 --> 00:16:04,278
How beautiful we

124
00:16:05,000 --> 00:16:07,036
can be

125
00:16:09,680 --> 00:16:12,274
Everything is cold

126
00:16:12,600 --> 00:16:16,275
At the end of the planet

127
00:16:17,160 --> 00:16:21,438
In our white desert

128
00:16:24,880 --> 00:16:28,156
The sun kissed the ice

129
00:16:28,720 --> 00:16:31,598
It glistens for me

130
00:16:32,480 --> 00:16:35,153
And we are one again

131
00:16:36,600 --> 00:16:42,554
We belong to the frozen world

132
00:16:47,240 --> 00:16:49,037
When the ice

133
00:16:49,960 --> 00:16:52,793
begins to thaw

134
00:16:53,360 --> 00:16:56,875
Becomes the sea

135
00:17:05,280 --> 00:17:08,158
You will see

136
00:17:08,760 --> 00:17:11,513
How beautiful we

137
00:17:11,840 --> 00:17:14,274
can be

138
00:17:26,520 --> 00:17:29,353
We are going to dance
for the longest of winters

139
00:17:29,560 --> 00:17:33,379
We'll lose and find
each other for 9 months

140
00:17:33,414 --> 00:17:37,198
If our ballet is harmonious,
we shall survive

141
00:18:25,920 --> 00:18:28,593
Too bad for those who
couldn't follow

142
00:18:28,800 --> 00:18:29,994
those who are erring, lost,

143
00:18:30,240 --> 00:18:32,037
those who are late or weak.

144
00:18:32,240 --> 00:18:35,994
At the desert's mercy,
far from the Oamock's granite walls

145
00:18:36,240 --> 00:18:40,119
Without the groups' strength,
the solitary is condemned.

146
00:19:18,800 --> 00:19:21,473
Those the march forgot slowly fade.

147
00:19:21,680 --> 00:19:25,355
Here, white reigns -
what dies

148
00:19:25,680 --> 00:19:26,795
...is erased.

149
00:20:00,640 --> 00:20:03,519
Already, winter's first tears come.

150
00:20:03,760 --> 00:20:07,196
Soft tears, like
souvenirs from the sea

151
00:20:11,000 --> 00:20:12,433
Love was proclaimed,

152
00:20:13,120 --> 00:20:14,473
Love was made,

153
00:20:15,480 --> 00:20:17,596
all those around us keep quiet.

154
00:20:19,640 --> 00:20:23,918
In the warmth of our bodies,
a promise of life starts

155
00:21:16,920 --> 00:21:18,797
A moon passes
and we wait.

156
00:21:19,040 --> 00:21:21,156
A moon passes
and we wait.

157
00:21:26,080 --> 00:21:28,514
Another moon
and we wait.

158
00:21:28,720 --> 00:21:31,359
Another moon
and we wait.

159
00:21:39,080 --> 00:21:42,356
Comes the 3rd moon,
the time has come.

160
00:22:03,200 --> 00:22:07,398
With our feathers, we have to cover life
the moment it appears

161
00:22:07,840 --> 00:22:08,875
...hide it.

162
00:22:09,480 --> 00:22:10,799
Cold is everywhere

163
00:22:12,080 --> 00:22:15,834
This mini heart's tremor,
hidden in its shell

164
00:22:16,040 --> 00:22:19,157
is so vulnerable in
this huge ice land.

165
00:22:19,400 --> 00:22:24,437
Here, you need much more life
to create life.

166
00:22:30,120 --> 00:22:33,475
From now on, each of our move
aims at one single thing:

167
00:22:34,640 --> 00:22:35,834
keep him alive.

168
00:22:39,000 --> 00:22:40,479
I'm worn out.

169
00:22:42,760 --> 00:22:45,638
I need to go back to the sea,
eat to keep

170
00:22:45,880 --> 00:22:47,518
my body awake.

171
00:22:47,920 --> 00:22:51,276
Yet, before, I need to
give the egg to his father.

172
00:22:55,560 --> 00:22:57,835
Life contained in the egg
won't resist the frost's bite

173
00:22:58,040 --> 00:23:00,156
No more than a few seconds

174
00:23:00,400 --> 00:23:03,517
Some couples, like this one
- they must be young -

175
00:23:03,760 --> 00:23:04,954
are too nervous

176
00:23:23,280 --> 00:23:25,999
Frost bites and breaks

177
00:23:26,200 --> 00:23:27,633
the treasure of the hurried,

178
00:23:27,840 --> 00:23:29,796
the clumsy, the unlukcy.

179
00:24:01,840 --> 00:24:04,035
Their dance is over,

180
00:24:04,280 --> 00:24:05,474
their year is lost.

181
00:24:05,880 --> 00:24:08,440
Now without an aim,
they'll go,

182
00:24:08,640 --> 00:24:10,278
in an errand towards the ocean.

183
00:24:11,640 --> 00:24:14,837
100 times, we patiently repeat
the dance...

184
00:24:15,080 --> 00:24:16,638
...before passing the egg.

185
00:24:24,440 --> 00:24:27,034
Again and again,
we repeat the litany

186
00:24:27,240 --> 00:24:28,798
of our songs,
of our charms.

187
00:24:29,080 --> 00:24:31,640
Again and again,
we repeat the litany

188
00:24:31,840 --> 00:24:33,796
of our songs,
of our charms.

189
00:24:35,560 --> 00:24:37,596
In our dance's balancing act

190
00:24:37,800 --> 00:24:39,677
lies the perfect movement

191
00:24:39,880 --> 00:24:42,633
This little step, this little step.

192
00:26:20,680 --> 00:26:21,556
I'm leaving,

193
00:26:22,160 --> 00:26:23,718
I have to walk to the ocean.

194
00:26:23,960 --> 00:26:26,315
I'll stay with our egg,
I will cover it.

195
00:26:26,520 --> 00:26:29,353
My belly's warmth will
protect until your return.

196
00:26:30,040 --> 00:26:32,156
Go back to the ocean
and its food feasts.

197
00:26:32,400 --> 00:26:34,755
And make sure you're back in time,

198
00:26:34,960 --> 00:26:36,518
too feed our soon-to-born baby.

199
00:26:36,760 --> 00:26:37,795
We'll wait for you.

200
00:26:50,000 --> 00:26:53,675
It's time for the 2nd walk,
the march at dawn.

201
00:26:54,760 --> 00:26:57,194
For the first time,
dancers separate.

202
00:27:00,440 --> 00:27:01,634
I'm starving.

203
00:27:02,240 --> 00:27:05,516
Our food is over there,
beyond the desert.

204
00:27:05,720 --> 00:27:08,757
Yet only here,
on the Oamock's solid ice...

205
00:27:09,000 --> 00:27:11,594
can our baby be born.

206
00:27:34,320 --> 00:27:37,357
We'll go to the sea,
they'll stay here.

207
00:27:38,480 --> 00:27:40,596
They'll walk,
we'll wait.

208
00:27:40,840 --> 00:27:41,636
Two month...

209
00:27:41,880 --> 00:27:43,916
of misery,
and fight to try to...

210
00:27:44,200 --> 00:27:45,713
...preserve life.

211
00:29:21,040 --> 00:29:24,237
For the last few days,
the sky above darkens.

212
00:29:26,640 --> 00:29:29,518
The sun, the mountains,
the settings change.

213
00:29:30,080 --> 00:29:32,674
It's like hearing winter
preparing its first blow.

214
00:30:26,800 --> 00:30:30,395
We look like circus artists
with this egg on our feet.

215
00:30:30,600 --> 00:30:34,718
We're now a single body
rolling itself

216
00:30:35,400 --> 00:30:37,595
like a storm roll.

217
00:30:37,880 --> 00:30:38,835
We'll hold.

218
00:32:56,840 --> 00:32:59,798
Same winter -
we walk again and again

219
00:33:00,120 --> 00:33:02,953
Cruel irony, on the ocean's back

220
00:33:03,160 --> 00:33:05,116
we're looking for the door.

221
00:33:21,040 --> 00:33:23,474
Here, under the ice, fishes swim.

222
00:33:25,880 --> 00:33:28,633
Here, on the ice, the storm blows.

223
00:33:36,440 --> 00:33:39,113
The starving walkers suffer.

224
00:34:17,640 --> 00:34:19,073
The smell of water...

225
00:34:20,480 --> 00:34:22,118
We can smell the water.

226
00:34:23,040 --> 00:34:24,598
The entrance isn't far.

227
00:34:55,160 --> 00:34:56,354
Here we are.

228
00:34:57,160 --> 00:34:58,275
The ocean at last!

229
00:35:52,080 --> 00:35:53,399
Caress of the water,

230
00:35:53,920 --> 00:35:57,276
everywhere, the light shows
the way to the abyss.

231
00:35:57,520 --> 00:36:00,717
Over there, past the blue
gateway to the deep,

232
00:36:00,920 --> 00:36:02,353
...the table is ready.

233
00:36:02,680 --> 00:36:04,477
"Song of the sea"

234
00:36:09,080 --> 00:36:11,674
See

235
00:36:14,720 --> 00:36:19,077
how I fly

236
00:36:22,360 --> 00:36:25,955
In the sea

237
00:36:28,800 --> 00:36:32,759
Like in the sky

238
00:36:38,360 --> 00:36:41,033
You set me free

239
00:37:47,160 --> 00:37:49,799
Sometimes, the wind leaves a bit
of fresh snow for us

240
00:37:50,000 --> 00:37:51,513
...it quenches our thirst.

241
00:37:51,720 --> 00:37:53,915
3 months without any food!

242
00:37:54,880 --> 00:37:58,475
We defy winter and reach
life's limits.

243
00:37:59,440 --> 00:38:03,035
Some pass the border,
they fall asleep, slowly.

244
00:38:04,480 --> 00:38:07,040
White spreads little by little
over their body,

245
00:38:07,320 --> 00:38:08,878
which disappear forever.

246
00:38:56,960 --> 00:38:58,154
Nightime

247
00:38:58,800 --> 00:39:01,997
...we crawl under the storms,
cold rules.

248
00:39:02,200 --> 00:39:03,519
It has no name.

249
00:39:04,040 --> 00:39:06,679
We hold without failing, under
the mother of all blizzards

250
00:39:06,880 --> 00:39:09,599
melded together, a single body,
like the scale

251
00:39:09,840 --> 00:39:11,558
...of turltes are fitted.

252
00:39:14,840 --> 00:39:17,718
Night's daughters wear their
best veils.

253
00:39:17,960 --> 00:39:18,756
They run

254
00:39:19,000 --> 00:39:21,673
...under the stars,
dance to the wind's song.

255
00:39:21,920 --> 00:39:25,435
Be courageous, enslaved people,
this dance is going

256
00:39:25,640 --> 00:39:29,474
To the sky's limit,
looking for the sun who forgot us.

257
00:39:56,120 --> 00:39:56,916
How soft is

258
00:39:57,160 --> 00:39:58,479
the sea's water!

259
00:39:59,160 --> 00:40:02,357
We enjoy the ocean
as long as we can.

260
00:40:03,600 --> 00:40:08,116
But on the Oamock, life
will soon need us.

261
00:40:56,080 --> 00:40:59,436
With a single jawbite,
the monster erased two lives

262
00:40:59,720 --> 00:41:03,235
...the life of the trapped mother
and that of her baby

263
00:41:03,440 --> 00:41:05,158
who will never be fed.

264
00:41:23,280 --> 00:41:25,840
We are still petrified
by terror.

265
00:41:26,960 --> 00:41:29,394
Around us, the night
has the smell of frost

266
00:41:29,880 --> 00:41:32,155
we remember the ice's noise

267
00:41:32,400 --> 00:41:34,914
so different from the ocean's songs

268
00:41:56,880 --> 00:41:57,995
It's time for the return

269
00:41:58,440 --> 00:42:01,955
time for the 3rd march,
the walk of the moon.

270
00:42:03,200 --> 00:42:06,476
In the endless night,
we find our way to our land of cold.

271
00:42:08,720 --> 00:42:10,836
We bring back lots of food
to feed our babies.

272
00:42:11,120 --> 00:42:13,475
People are waiting for us.

273
00:42:13,680 --> 00:42:14,999
We have to hurry.

274
00:43:19,000 --> 00:43:20,877
Our gaping night takes forever

275
00:43:21,160 --> 00:43:23,754
...and get bogged,
a 100 days long night,

276
00:43:23,960 --> 00:43:26,315
filled with cold
and turmoil.

277
00:43:27,360 --> 00:43:31,433
Yet... this morning...
something...

278
00:43:31,720 --> 00:43:32,914
...sparkles in the air.

279
00:43:35,320 --> 00:43:38,198
The light is back.

280
00:44:06,440 --> 00:44:08,795
We have vainquished winter.

281
00:44:40,760 --> 00:44:41,795
Life...

282
00:44:42,440 --> 00:44:46,638
so small, so beautiful, unreal
in this cold that erodes us.

283
00:44:46,840 --> 00:44:49,400
We reached
the summit of our torment.

284
00:44:49,600 --> 00:44:52,717
From now on, each day
will bring a stronger sun,

285
00:44:53,880 --> 00:44:56,758
which frees the youngs
from their shells.

286
00:44:58,640 --> 00:44:59,755
Where are they?

287
00:45:01,080 --> 00:45:02,354
But where are they?

288
00:45:20,840 --> 00:45:23,434
How long have we
been going around like that?

289
00:45:24,280 --> 00:45:26,874
Dawn's first glow has
awaken winter.

290
00:45:27,080 --> 00:45:28,877
It enrages, it increases.

291
00:45:29,120 --> 00:45:31,395
Cold is so strong that it finally...

292
00:45:31,720 --> 00:45:32,994
...froze the wind.

293
00:45:33,520 --> 00:45:36,557
It tighten its grip on us,
locks us stronger,

294
00:45:36,800 --> 00:45:38,438
...clenches stronger.

295
00:45:39,120 --> 00:45:42,556
So we turn, so as not
to be too exposed to the cold

296
00:45:42,760 --> 00:45:44,034
...our back to the cold.

297
00:45:44,760 --> 00:45:48,196
Heat exists, at the turtle's hearth,
hidden under the heads.

298
00:45:48,880 --> 00:45:52,316
At our feet, babies appear,
always more plentiful.

299
00:46:07,160 --> 00:46:08,195
Where are they?

300
00:46:15,280 --> 00:46:17,396
As soon as they hatch out,
the babies are hungry.

301
00:46:17,680 --> 00:46:20,274
The mothers have to
come back!

302
00:46:20,480 --> 00:46:23,313
In this cold,
their energy fades so quickly.

303
00:47:06,600 --> 00:47:10,593
And I am exhaused,
4 months without any food.

304
00:47:10,800 --> 00:47:11,835
I am empty.

305
00:47:12,080 --> 00:47:15,675
Barely enough to walk
another 20 days.

306
00:47:16,120 --> 00:47:20,033
20 days... the eternity
that separates us from the ocean.

307
00:47:20,560 --> 00:47:23,677
So this evening, if his mother
hasn't come back,

308
00:47:24,000 --> 00:47:26,753
...I'll have to go,
abandon my child.

309
00:47:42,080 --> 00:47:46,198
But before we give up,
there's still a chance.

310
00:47:49,040 --> 00:47:51,554
The hidden part,
a few crumbles...

311
00:47:51,760 --> 00:47:54,115
...preserved deep inside my body,

312
00:47:54,320 --> 00:47:56,276
...despite these months of starvation.

313
00:47:58,680 --> 00:48:02,878
A few more hours of life.
I had kept them...

314
00:48:03,480 --> 00:48:06,517
...for you,
in expecation of this moment.

315
00:48:07,960 --> 00:48:11,794
Who will win?
Life or winter?

316
00:48:15,080 --> 00:48:17,958
For some,
it's already too late.

317
00:48:38,600 --> 00:48:41,319
We walk, and in us,
life shrieks:

318
00:48:41,520 --> 00:48:43,158
Hurry up!
Hurry up!

319
00:48:43,480 --> 00:48:47,519
He, she, is born, needing you,
He, she will not hold for long, now.

320
00:49:18,320 --> 00:49:21,995
Frightened for no reason,
a lot of wasted time.

321
00:50:06,160 --> 00:50:07,559
We push our speed.

322
00:50:10,080 --> 00:50:12,958
The wind brings puffs
of familiar scents.

323
00:50:13,160 --> 00:50:15,754
The colony's not far,
faster!

324
00:50:21,680 --> 00:50:22,954
The Oamock, at last!

325
00:50:24,880 --> 00:50:26,233
Who will I find?

326
00:50:27,040 --> 00:50:30,157
Is my baby dead?
Is he or she still alive?

327
00:51:25,520 --> 00:51:27,078
Where in this crowd are you?

328
00:51:28,600 --> 00:51:29,635
Where are you?

329
00:51:43,320 --> 00:51:46,198
This voice...
This chant coming towards me...

330
00:51:46,680 --> 00:51:48,875
This baby calling,
is he or she mine?

331
00:51:49,080 --> 00:51:50,638
Come on, sing!

332
00:52:27,320 --> 00:52:28,435
You did hold!

333
00:52:30,480 --> 00:52:31,993
You came back in time!

334
00:52:32,640 --> 00:52:33,755
You are still here.

335
00:52:35,200 --> 00:52:39,113
And you, I only meet you now,
my marvelous son of the winter,

336
00:52:39,880 --> 00:52:40,995
...my little baby.

337
00:52:54,360 --> 00:52:58,478
Do you remember the dance,
our songs in the times of love?

338
00:52:58,800 --> 00:52:59,676
Here they are again...

339
00:52:59,960 --> 00:53:02,315
...renewed to celebrate
your return.

340
00:53:17,040 --> 00:53:20,476
I must take our child,
and you must leave hastilly.

341
00:53:21,040 --> 00:53:23,918
A short moment,
a precious moment.

342
00:53:25,040 --> 00:53:26,996
Within one step, we'll be separated,

343
00:53:27,240 --> 00:53:31,358
...Soon we'll be 1 000 winters apart,
let's make the most of it!

344
00:54:01,400 --> 00:54:02,674
Another instant.

345
00:54:04,360 --> 00:54:05,918
Youngster, listen carefully
to my voice

346
00:54:06,160 --> 00:54:09,277
...thanks to my voice
you'll recognise me one day.

347
00:54:34,000 --> 00:54:37,037
I'd like to be able to
promise I'll come back.

348
00:54:39,600 --> 00:54:41,158
It's my turn to leave.

349
00:54:41,480 --> 00:54:44,074
My strength desert me,
I have to complete...

350
00:54:44,280 --> 00:54:46,555
...the most dangerous
part of the trip:

351
00:54:46,800 --> 00:54:48,358
the march of the starved ones.

352
00:54:48,680 --> 00:54:50,159
The ultimate march,

353
00:54:50,560 --> 00:54:54,314
the march which decimates us
every year.

354
00:55:58,120 --> 00:56:00,475
Here it comes at last - renewal.

355
00:56:01,920 --> 00:56:04,559
Slowly, light blossoms
on the Oamock.

356
00:56:18,320 --> 00:56:22,233
Like the sun, our youngs
grow stronger day by day.

357
00:56:22,720 --> 00:56:23,835
But summer is still far.

358
00:56:24,160 --> 00:56:27,516
No way we'll let our youngs out.

359
00:56:27,840 --> 00:56:30,274
They are so frail,
They need...

360
00:56:30,600 --> 00:56:31,999
our warmth.

361
00:56:32,840 --> 00:56:34,637
Around us,

362
00:56:34,920 --> 00:56:36,035
frost lurks.

363
00:57:23,320 --> 00:57:25,914
10 days pass by
then 10 more days.

364
00:57:27,880 --> 00:57:30,758
And one beautiful morning...

365
00:57:32,000 --> 00:57:35,276
I am going for my first walk
by myself!

366
00:57:36,120 --> 00:57:37,473
Oh, it's like a nettle...

367
00:57:37,760 --> 00:57:38,875
under the paw (clawed feet)!

368
00:57:41,680 --> 00:57:44,035
I'm cold!
It's funny, it tickles!

369
00:57:47,440 --> 00:57:51,035
Here, my first walk,
the march of the free chick!

370
00:58:09,880 --> 00:58:12,758
Cold is bringing them back
under our paws.

371
00:58:13,000 --> 00:58:14,877
But they made...

372
00:58:15,160 --> 00:58:18,118
...the great jump.
A page of their life has been turned.

373
00:59:03,600 --> 00:59:06,478
We haven't seen snow running like that.

374
00:59:06,680 --> 00:59:09,319
Adults sqeeze together,
they are worrying.

375
00:59:09,520 --> 00:59:11,397
This wind is no bearer of good news.

376
00:59:11,960 --> 00:59:13,598
The dying winter is kicking us...

377
00:59:13,880 --> 00:59:15,438
...with it last blizzard.

378
00:59:15,680 --> 00:59:18,353
We think cold lost
its arrogance,

379
00:59:18,640 --> 00:59:21,279
...but our youngs think otherwise.
They'll have to be strong.

380
00:59:21,560 --> 00:59:23,596
The time of their initiation has come.

381
00:59:23,840 --> 00:59:26,957
It's the dreaded moment of
their first turmoil.

382
00:59:27,240 --> 00:59:31,233
Can't you hear my storm coming
Stones falling on to you

383
00:59:34,840 --> 00:59:38,913
Can't you feel the earth shaking
Big dark clouds forming now

384
00:59:41,080 --> 00:59:43,913
And I hope you're satisfied

385
00:59:44,920 --> 00:59:47,639
I hope you're satisfied

386
00:59:48,400 --> 00:59:50,197
I hope you're satisfied

387
00:59:51,360 --> 00:59:53,157
To see the wind

388
00:59:53,440 --> 00:59:55,874
blow over me

389
00:59:57,920 --> 01:00:00,195
Can't you hear my sky shouting

390
01:00:00,520 --> 01:00:02,238
Close, chasing after you

391
01:00:05,280 --> 01:00:09,398
Deep, dark fear building up
It's too strong for you

392
01:00:12,080 --> 01:00:15,117
And I hope you're satisfied

393
01:00:16,400 --> 01:00:17,958
I hope you're satisfied

394
01:00:19,480 --> 01:00:22,517
I hope you're satisfied

395
01:00:23,360 --> 01:00:25,078
To see the wind

396
01:00:27,920 --> 01:00:30,195
Blow

397
01:00:30,560 --> 01:00:32,596
over me

398
01:00:38,080 --> 01:00:40,674
Can't you hear my storm coming

399
01:00:41,640 --> 01:00:43,198
Stones falling

400
01:00:45,520 --> 01:00:48,318
Big dark clouds

401
01:00:48,600 --> 01:00:50,238
forming now

402
01:01:45,880 --> 01:01:50,396
The wind leaves, on its trail,
mothers looking for their child.

403
01:01:51,040 --> 01:01:53,156
Trembling lost chicks.

404
01:01:55,720 --> 01:01:59,156
And a harvest of lost lives,
swept away by the cold.

405
01:02:49,880 --> 01:02:52,838
All these efforts broken
by a gust of wind.

406
01:03:00,280 --> 01:03:01,838
Some find it unbearable.

407
01:03:02,080 --> 01:03:03,638
They need a child, at any price.

408
01:03:03,840 --> 01:03:07,037
Life has to keep on growing
on their feet.

409
01:03:07,600 --> 01:03:11,354
So, they perpetrate the most
inconceivable act.

410
01:03:11,680 --> 01:03:14,114
In a moment of crazyness,
the steal...

411
01:03:14,440 --> 01:03:15,839
...someone else's child.

412
01:03:59,280 --> 01:04:03,159
That was just! I thought they
would crush me!

413
01:04:03,480 --> 01:04:04,595
You saved me!

414
01:04:06,880 --> 01:04:08,279
It feels good to be close to you.

415
01:04:58,760 --> 01:05:01,433
Happy birthday, short legs!
One month ago...

416
01:05:01,720 --> 01:05:04,075
...we were hatching out of our shells,
now we run.

417
01:05:04,280 --> 01:05:05,156
Not bad, innit?

418
01:05:05,400 --> 01:05:08,517
We don't even need our mothers
to get warm.

419
01:05:08,920 --> 01:05:11,036
Sometimes, we'd really like to...

420
01:05:11,360 --> 01:05:13,396
...go under the warm duvet.

421
01:05:21,600 --> 01:05:25,434
There's always one or two
who slouch in the pockets.

422
01:05:25,760 --> 01:05:28,513
They pretend to be cold,
but for them, same system:

423
01:05:28,720 --> 01:05:31,757
Stand up, your two paws
on the ice, and walk!

424
01:05:32,320 --> 01:05:34,038
When time's up,
time is up!

425
01:05:34,360 --> 01:05:36,476
Now, we are
in-de-pen-dent!

426
01:05:46,080 --> 01:05:48,275
When we are cold,
there's the child care centre.

427
01:05:48,680 --> 01:05:51,717
Not bad, we aggregate
and keep warm.

428
01:05:52,640 --> 01:05:54,517
A bit like our fathers
making a big turtle.

429
01:05:54,720 --> 01:05:56,597
But it's hard to stay in place.

430
01:06:47,880 --> 01:06:51,077
I'm going to have to leave you alone,
I'll be back quickly.

431
01:06:51,480 --> 01:06:54,358
I need to go and fish
to nourish you.

432
01:06:54,680 --> 01:06:58,116
You'll stay here,
your father should be there soon.

433
01:06:59,480 --> 01:07:01,357
You'll recognise his chant.

434
01:07:01,720 --> 01:07:03,073
He'll recognise yours.

435
01:07:11,840 --> 01:07:14,354
I'm not too fond of
staying alone.

436
01:07:16,200 --> 01:07:17,838
Wait! I'm a little bit afraid.

437
01:08:47,120 --> 01:08:47,996
Alert!

438
01:08:48,320 --> 01:08:51,153
The long wings are coming!
Hurry, to the day care centre!

439
01:08:51,440 --> 01:08:54,000
Oh! What about those ones,
left alone over there!

440
01:10:55,960 --> 01:10:59,236
It's over for that one.
She'll never see the sea.

441
01:11:49,000 --> 01:11:51,116
Here they come!
The fathers are back!

442
01:11:51,960 --> 01:11:53,916
Is my father with them?

443
01:12:37,200 --> 01:12:40,954
Yes, it's him, I'm know it.
Yes, it's you, I know it.

444
01:13:05,400 --> 01:13:07,277
Adults keep on going and coming back.

445
01:13:07,520 --> 01:13:08,555
They have two sides:

446
01:13:08,800 --> 01:13:11,473
The white one, it's the full bellies
coming back.

447
01:13:11,680 --> 01:13:14,558
The black one, empty bellies leaving.

448
01:13:14,800 --> 01:13:17,758
As for us, front or back,
we are grey.

449
01:13:18,000 --> 01:13:19,513
We are always hungry!

450
01:13:41,720 --> 01:13:43,915
Sometimes come days
we can't even dream of.

451
01:13:44,200 --> 01:13:46,634
For weeks, we have
fed our kid...

452
01:13:46,880 --> 01:13:49,553
...separated, never meeting up.

453
01:13:49,840 --> 01:13:53,116
And here we are, reunited
by the finest of all hasards.

454
01:13:59,840 --> 01:14:02,957
Our kid is strong,
our kid is lovely.

455
01:14:05,600 --> 01:14:07,955
On the wake of the last march,

456
01:14:08,160 --> 01:14:11,197
let's promise to meet
for next season...

457
01:14:11,440 --> 01:14:14,238
...to dance again,
maybe tie the knot...

458
01:14:14,520 --> 01:14:16,158
...for another winter.

459
01:14:41,040 --> 01:14:43,634
It's been a few days,
it's always sunny...

460
01:14:43,920 --> 01:14:46,593
...and there's no night.
The icefield groans,

461
01:14:46,880 --> 01:14:48,996
...it looks like it wants to leave.

462
01:15:03,200 --> 01:15:04,679
It's time to depart.

463
01:15:05,360 --> 01:15:08,318
The ocean calls me,
it will call you soon.

464
01:15:08,560 --> 01:15:11,597
To life, little son of winter,
to life!

465
01:15:28,680 --> 01:15:31,274
There ends our last march.

466
01:15:31,560 --> 01:15:34,597
The separation march,
that of the return...

467
01:15:34,880 --> 01:15:38,236
...to the sea. We leave
the Oamock for this year.

468
01:15:38,480 --> 01:15:42,473
The time of the couples has passed away,
the dance is finished.

469
01:15:42,680 --> 01:15:45,717
The dancers leave their separate way.

470
01:16:13,160 --> 01:16:13,956
We return to...

471
01:16:14,200 --> 01:16:16,236
the water for the year's most
beautiful moons.

472
01:16:16,480 --> 01:16:19,836
Three months of ocean,
Three months of summer,

473
01:16:20,120 --> 01:16:21,439
...of swimming and pleasure!

474
01:16:27,480 --> 01:16:29,596
These last few days,
we feel strange.

475
01:16:29,880 --> 01:16:32,394
We don't even want to eat,
maybe...

476
01:16:32,680 --> 01:16:34,398
...we are becoming adults?

477
01:17:01,080 --> 01:17:03,878
Time for us to answer the call.

478
01:17:04,080 --> 01:17:06,355
Like our parents came
from the sea,

479
01:17:06,600 --> 01:17:09,273
...it's our turn
to walk towards the ocean.

480
01:17:09,480 --> 01:17:12,199
We leave the Oamock
to join our people...

481
01:17:12,480 --> 01:17:14,755
...in the gentle deep.

482
01:17:57,520 --> 01:17:59,476
Little ice walkers,
here comes the time...

483
01:17:59,800 --> 01:18:01,836
...of metamorphosis,
we became...

484
01:18:02,160 --> 01:18:05,755
...children of the ocean,
amongst children of the ocean.

485
01:18:28,280 --> 01:18:32,114
One day, far away,
this famous day of 1/3 of the year

486
01:18:32,320 --> 01:18:35,357
...when the sun and the moon
meet in the sky,

487
01:18:35,600 --> 01:18:36,919
...we'll have to jump...

488
01:18:37,160 --> 01:18:39,355
...out of the sea and become
walkers again.

489
01:18:39,600 --> 01:18:42,160
Faithfull to our forefathers' oath,

490
01:18:42,360 --> 01:18:44,635
...we'll come back here
to dance for life...

491
01:18:44,840 --> 01:18:48,719
...during the biggest winter,
and perpetuate...

492
01:18:48,920 --> 01:18:50,638
...the march of the Emperor.

493
01:18:57,680 --> 01:19:01,877
Subs CEDRA PRODUCTlONS
Translated by Patrick


