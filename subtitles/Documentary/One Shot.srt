1
00:00:00,640 --> 00:00:17,640
<b><font color=#FF8080>Sub by: TlTELBlLD, Berlin, 2014</font>
<font color=#00FF00>Timing By Oom St@r, Langsa</font>
<font color=#00FFFF>Resync To BRrip English By JoelzR,
Pariscell-Beureunuen</font></b>

2
00:00:26,640 --> 00:00:30,517
Our species was once close to perfection.

3
00:00:30,780 --> 00:00:33,552
We knew diseases nor pain.

4
00:00:33,620 --> 00:00:38,576
But contact with people was the
beginning of the end for my people.

5
00:00:40,520 --> 00:00:45,451
Our only chance of survival
was to quickly attack.

6
00:00:47,700 --> 00:00:50,290
However, to provide,
instead of the solution,

7
00:00:50,600 --> 00:00:53,518
ushered in a long war in our attack

8
00:00:53,640 --> 00:00:58,393
people brought to my planet, Cerulean.

9
00:01:27,780 --> 00:01:29,400
Kyle. wake up.

10
00:01:31,663 --> 00:01:33,400
Matthews. Matthews. - Hurry up.

11
00:01:34,679 --> 00:01:39,313
We have to go. Stand up. We have to go.

12
00:02:38,638 --> 00:02:40,438
Contact the left. - We must go.

13
00:04:46,525 --> 00:04:51,363
MILITAIR RUIMTESTATION MAGELLAN

14
00:04:51,634 --> 00:04:54,130
This situation has top priority.

15
00:04:54,200 --> 00:04:57,169
EX-3 is ambushed.

16
00:04:57,200 --> 00:04:59,595
Niles, play the recording
on one channel mode.

17
00:05:00,660 --> 00:05:05,217
We are attacked. Mayday. Mayday.

18
00:05:05,280 --> 00:05:08,590
Magellan, you hear me? We are attacked.

19
00:05:08,660 --> 00:05:10,413
Why is the link broken?

20
00:05:10,680 --> 00:05:13,136
Sergeant Green was hit by a bazooka.

21
00:05:13,200 --> 00:05:17,137
Precise position?
- 40 km north of their target.

22
00:05:17,193 --> 00:05:20,113
Bring it into focus.
- The satellite is failed.

23
00:05:20,200 --> 00:05:23,169
How many dead? - Four confirmed.

24
00:05:23,200 --> 00:05:25,192
Green, Smith, Knight, Steele.

25
00:05:25,200 --> 00:05:27,193
Two missing. - Names?

26
00:05:27,200 --> 00:05:29,579
Matthews and Carpenter.
- Call up their files.

27
00:05:32,714 --> 00:05:37,128
Matthews, Kyle. Born 04/07/03, Idaho.
Scandinavian-German ancestors.

28
00:05:37,200 --> 00:05:40,154
In het leger sinds 12.05.21.
Special Forces.

29
00:05:40,200 --> 00:05:42,173
Small arms, nurse.

30
00:05:42,200 --> 00:05:45,510
Pilot's license and certification.
37 hits. Highly decorated.

31
00:05:45,644 --> 00:05:49,962
I know him. He is one of
our best schuipschutters.

32
00:05:50,000 --> 00:05:53,913
Believe me, 37 hits is a low estimate.
And Carpenter?

33
00:05:54,200 --> 00:05:59,155
EX3-Luitenant Jason Carpenter,
29.9.01, New Jersey.

34
00:05:59,200 --> 00:06:00,180
Ltalian Hispanic ancestry.

35
00:06:00,200 --> 00:06:05,652
Ln the army since 03/06/21, Special Forces.
Distinguished as high.

36
00:06:05,780 --> 00:06:07,597
Satellite images in five seconds.

37
00:06:13,720 --> 00:06:15,376
Zoom out. Bring the enemy image.

38
00:06:16,675 --> 00:06:20,585
Troops Transport. Five kilometers away.
Fast approaching.

39
00:06:20,712 --> 00:06:24,588
Search Matthews and Carpenter.
Let them out of there.

40
00:09:16,700 --> 00:09:19,392
Hou is taai.

41
00:09:55,620 --> 00:09:56,900
We have him. Wait for my signal.

42
00:10:21,684 --> 00:10:23,538
A subject is moving quickly
towards the hills.

43
00:10:59,680 --> 00:11:02,111
Who is that? - That's Matthews.

44
00:11:02,180 --> 00:11:03,409
Follow him.

45
00:11:20,660 --> 00:11:21,574
Where did he go?

46
00:11:21,740 --> 00:11:24,452
Bring that rock formation image.

47
00:11:25,620 --> 00:11:26,656
Composition?

48
00:11:27,693 --> 00:11:33,185
Calciet, CaCO3. En dolomiet, CaMg [Co3] 2.

49
00:11:33,200 --> 00:11:35,392
Smart man. - What do you mean?

50
00:11:35,712 --> 00:11:41,207
Die Rots blocker hittesensoren.
They can not locate him.

51
00:11:41,700 --> 00:11:43,596
What do we do now?

52
00:11:43,742 --> 00:11:46,252
Search Carpenter. - And Matthews?

53
00:11:46,636 --> 00:11:50,391
We know where he is. He
trusts that we know.

54
00:17:21,520 --> 00:17:22,720
What was that? - What channel?

55
00:17:24,212 --> 00:17:25,571
Two. On the main screen.

56
00:17:25,660 --> 00:17:29,497
What have you seen?
- A thermal. Rinse back.

57
00:17:35,520 --> 00:17:37,518
There. - Is it one of them?

58
00:17:37,760 --> 00:17:39,592
He greets us alone.

59
00:17:40,740 --> 00:17:45,150
I send the rescue bird. Target
area between LZ 06 and LZ 09.

60
00:17:45,200 --> 00:17:48,537
It is too dangerous. Keep them
ready until we know more.

61
00:17:48,600 --> 00:17:52,132
How long do we have received?
- Another 60 minutes.

62
00:17:52,205 --> 00:17:55,523
I want to see as long as
possible as much as possible.

63
00:18:52,713 --> 00:18:56,384
Enemies approaching Matthews' position.

64
00:19:47,620 --> 00:19:48,712
What happened?

65
00:19:50,712 --> 00:19:52,502
Three enemies less.

66
00:21:31,620 --> 00:21:32,559
What is that?

67
00:21:32,720 --> 00:21:35,391
It looks like a civilian vehicle.

68
00:21:35,640 --> 00:21:40,590
Give me the scan results.
- They are online now.

69
00:21:41,720 --> 00:21:42,813
Who is that?

70
00:21:42,880 --> 00:21:45,236
The vehicle is Omar Karzchet.

71
00:21:45,260 --> 00:21:48,152
He is a Leckchat, a kind of frost.

72
00:21:48,220 --> 00:21:52,219
He secular and religious power.

73
00:21:52,240 --> 00:21:53,532
What's he doing there?

74
00:21:53,722 --> 00:21:57,433
Keep an eye on them. Return to Matthews.

75
00:23:56,700 --> 00:23:58,380
What will we do?

76
00:23:59,820 --> 00:24:01,552
We must leave here.

77
00:24:06,720 --> 00:24:09,330
Come. That concerns us nothing.

78
00:30:46,716 --> 00:30:48,396
Do not cry.

79
00:30:55,802 --> 00:30:58,458
Do not be afraid.

80
00:33:20,700 --> 00:33:24,493
How long will this injustice continue?

81
00:36:39,580 --> 00:36:41,378
What is the situation?

82
00:36:41,740 --> 00:36:44,399
We have every day a few hours no picture,

83
00:36:44,803 --> 00:36:48,102
every time the third moon is in the way.

84
00:36:48,120 --> 00:36:51,431
Any news? - Depends on how you look.

85
00:36:51,700 --> 00:36:53,478
Let's see then.

86
00:36:54,740 --> 00:36:59,220
Three kilometers from Matthews shelter.

87
00:36:59,240 --> 00:37:01,418
What should I see?

88
00:37:01,620 --> 00:37:04,454
I saw something move. Did you see it?

89
00:37:04,820 --> 00:37:06,551
Please zoom back in. And rinse

90
00:37:10,635 --> 00:37:14,394
That's our man. - But what is he up to?

91
00:37:15,722 --> 00:37:17,596
He explores something. But what?

92
00:37:18,640 --> 00:37:20,478
He goes into the ravine and we lost him.

93
00:37:21,540 --> 00:37:22,633
Wait.

94
00:37:23,706 --> 00:37:27,382
There is a thermal in the
ravine by Matthews shelter.

95
00:37:27,840 --> 00:37:29,236
But I kept it for a scout.

96
00:37:30,680 --> 00:37:32,536
Does he have a taken hostage?

97
00:39:15,714 --> 00:39:17,365
The man in the red vehicle.

98
00:39:20,800 --> 00:39:22,214
I spoke to him.

99
00:39:22,280 --> 00:39:24,551
I promised him to send you back.

100
00:39:30,620 --> 00:39:32,318
I thought so.

101
00:39:35,713 --> 00:39:38,547
I know you speak the language of the earth.

102
00:39:47,760 --> 00:39:48,740
Please.

103
00:39:53,680 --> 00:39:55,520
Please do not bring me back.

104
00:39:57,816 --> 00:39:59,408
He will murder me.

105
00:40:02,627 --> 00:40:04,619
I will not return.

106
00:40:06,732 --> 00:40:08,451
I promise you.

107
00:40:10,705 --> 00:40:12,344
Dank them.

108
00:40:17,534 --> 00:40:18,514
Who is he?

109
00:40:21,721 --> 00:40:25,537
You are safe here. No one will hurt you.

110
00:40:28,704 --> 00:40:30,496
I hear him.

111
00:40:31,800 --> 00:40:34,935
I'm his property.

112
00:40:35,000 --> 00:40:37,495
And he is my owner.

113
00:40:38,815 --> 00:40:40,647
That's different with us.

114
00:41:01,511 --> 00:41:02,482
Close your eyes.

115
00:41:08,700 --> 00:41:10,532
Close your eyes.

116
00:41:21,648 --> 00:41:24,563
Think of someone you really love.

117
00:41:26,700 --> 00:41:28,631
How the sun shines on his face.

118
00:41:31,611 --> 00:41:34,451
And his smile when he knows you're there.

119
00:41:48,536 --> 00:41:49,516
Dank them.

120
00:41:54,740 --> 00:41:57,532
You can look at me best.

121
00:41:57,740 --> 00:42:00,496
Sorry, but this is forbidden.

122
00:42:01,700 --> 00:42:05,452
Who forbids it? You God?

123
00:42:05,620 --> 00:42:07,516
Or men like you owner?

124
00:42:10,745 --> 00:42:14,425
Why would God give your eyes and
take away the freedom to see?

125
00:42:15,723 --> 00:42:19,558
This site should you do what you want.

126
00:42:19,698 --> 00:42:22,474
I am your protector.

127
00:42:26,620 --> 00:42:29,139
Maybe I am mistaken.

128
00:42:29,200 --> 00:42:31,595
Maybe I'm too ugly to look at.

129
00:42:51,760 --> 00:42:53,311
We have something in common.

130
00:42:54,680 --> 00:42:56,497
We both escaped death.

131
00:42:58,720 --> 00:43:00,571
But death surrounds us.

132
00:43:03,660 --> 00:43:05,458
Do you know this area?

133
00:43:07,720 --> 00:43:10,518
25 kilometers southwest of
Bulkav there is a people basis.

134
00:43:11,813 --> 00:43:14,806
My men and I were on our way there.

135
00:43:14,900 --> 00:43:16,698
Do you know where it is?

136
00:43:18,780 --> 00:43:21,476
I'm from Belahzerkva.

137
00:43:23,821 --> 00:43:27,337
My owner has been driving to get here.

138
00:43:29,580 --> 00:43:31,572
I do not know where we are.

139
00:43:42,700 --> 00:43:47,111
We can not stay here. We have
hardly any food and water

140
00:43:47,140 --> 00:43:49,694
and we need to create
at least two ambushes.

141
00:44:44,640 --> 00:44:46,535
The heat signal came from these ruins.

142
00:46:02,680 --> 00:46:04,280
Pas op.

143
00:46:12,640 --> 00:46:16,431
You must Commandant, see this.
- What is it?

144
00:46:16,720 --> 00:46:20,433
Someone has slain six fighters with a fall.

145
00:47:00,460 --> 00:47:03,591
There is movement. - Main.

146
00:47:04,640 --> 00:47:07,397
There. I see him.

147
00:47:08,725 --> 00:47:12,480
The signal is weak. He
camouflaged himself well.

148
00:47:30,678 --> 00:47:32,370
Ls dat Matthews?

149
00:47:32,780 --> 00:47:34,312
We think so.

150
00:47:34,680 --> 00:47:36,531
He hid the bodies.

151
00:47:36,722 --> 00:47:39,521
We Once the coast is clear she mountains.

152
00:47:41,620 --> 00:47:45,500
Al heard from Carpenter?
- No, he's like a ghost.

153
00:47:45,660 --> 00:47:51,132
We thought to see something a few
times, but he will not be found.

154
00:47:51,200 --> 00:47:54,694
Keep trying. We bring the boys back home.

155
00:47:59,640 --> 00:48:01,393
Listen well.

156
00:48:01,760 --> 00:48:04,711
You have something of mine. I will find it.

157
00:48:04,740 --> 00:48:07,377
And I will kill you all.

158
00:48:46,660 --> 00:48:48,456
Lk heet Mirra.

159
00:48:53,780 --> 00:48:54,860
Kyle.

160
00:49:03,658 --> 00:49:07,334
Do you hate the rock?

161
00:49:08,760 --> 00:49:11,431
I hunt enemy soldiers.

162
00:49:13,611 --> 00:49:16,585
I know exactly where and
how many there are.

163
00:49:17,660 --> 00:49:20,420
Each notch is one less worry.

164
00:49:20,803 --> 00:49:24,555
Maybe for you, but not for their families.

165
00:49:29,712 --> 00:49:33,484
If I do not kill them, they kill me.

166
00:49:34,797 --> 00:49:36,595
Is it that simple?

167
00:49:38,620 --> 00:49:40,477
It's my job to shoot.

168
00:49:41,700 --> 00:49:44,672
Unfortunately for them
I am very good at it.

169
00:49:46,655 --> 00:49:48,306
After all I've seen...

170
00:49:48,700 --> 00:49:51,513
I saw my best friend die before my eyes.

171
00:49:51,620 --> 00:49:57,378
And to know that I could be next...
Nothing's worse than that.

172
00:49:57,635 --> 00:49:59,550
Why do you do it?

173
00:50:03,599 --> 00:50:05,389
I have kind studied.

174
00:50:06,700 --> 00:50:09,454
I know more than you realize.

175
00:50:09,679 --> 00:50:11,538
Echt? - I am.

176
00:50:11,700 --> 00:50:14,693
I your culture, know your traditions,

177
00:50:15,800 --> 00:50:17,490
your self-destructive nature.

178
00:50:19,505 --> 00:50:21,105
Mens.

179
00:50:21,702 --> 00:50:25,515
I know you have the corpse
of the woman Ceruleaanse.

180
00:50:25,700 --> 00:50:28,198
You must be a sadist.

181
00:50:28,200 --> 00:50:32,279
Like the rest of your species, you
have no respect for the dead.

182
00:50:32,300 --> 00:50:34,437
What can you care about the body?

183
00:50:35,634 --> 00:50:39,494
Her owner is an influential
man of noble birth.

184
00:50:39,700 --> 00:50:42,431
You're stupid if you trust him.

185
00:50:43,648 --> 00:50:46,488
I want to give her a proper burial.

186
00:50:46,704 --> 00:50:48,300
I am willing to do business.

187
00:50:49,800 --> 00:50:51,656
You have nothing I want.

188
00:50:51,776 --> 00:50:53,415
I have something.

189
00:50:53,647 --> 00:50:57,465
A metal object that you
hang around your neck.

190
00:50:57,641 --> 00:50:59,553
That's a fair exchange, right?

191
00:50:59,800 --> 00:51:02,634
What do you have to keep it a corpse?

192
00:51:02,700 --> 00:51:06,472
There is no body, just a woman Ceruleaanse

193
00:51:06,800 --> 00:51:09,437
which was almost executed by its own owner.

194
00:51:11,100 --> 00:51:13,312
Depending lies, mens.

195
00:51:13,700 --> 00:51:15,532
You do not believe me.

196
00:51:15,800 --> 00:51:18,440
I do not deliver her killers.

197
00:51:18,648 --> 00:51:21,540
And do not try to find me.

198
00:51:21,704 --> 00:51:23,477
I will find you.

199
00:51:24,624 --> 00:51:26,555
Do not wait on you.

200
00:51:41,711 --> 00:51:43,601
Stay with me if you want to stay alive.

201
00:51:51,800 --> 00:51:54,599
Where did you get your nature
knowledge and our language learned?

202
00:51:55,732 --> 00:51:59,567
My mother took me in when I was little.

203
00:52:01,700 --> 00:52:02,719
Ln the secret?

204
00:52:04,678 --> 00:52:09,372
When I was six, I found an old book.

205
00:52:09,720 --> 00:52:13,154
I took it home, opened the

206
00:52:13,180 --> 00:52:16,479
and tried to pronounce the words.

207
00:52:16,740 --> 00:52:19,311
My mother found me with the book.

208
00:52:19,740 --> 00:52:24,593
I see the fear in her eyes before
me when she waned me the book.

209
00:52:24,720 --> 00:52:27,519
I had her promise never to tell my owner.

210
00:52:28,680 --> 00:52:32,594
A few days later she
gave me secretly a note.

211
00:52:34,740 --> 00:52:39,156
The note was the human alphabet

212
00:52:39,220 --> 00:52:41,476
and Ceruleaanse translation.

213
00:52:41,700 --> 00:52:44,420
I soon learned it from my mind.

214
00:52:45,640 --> 00:52:51,498
My mother found many ways
to give me secretly lesson.

215
00:52:51,760 --> 00:52:55,557
When I was in was good,
they gave me the book.

216
00:52:56,803 --> 00:53:01,343
I read it over and over again
until I knew it by heart.

217
00:53:01,800 --> 00:53:06,719
I was obsessed with all people book

218
00:53:06,720 --> 00:53:09,380
paper or quote that I could find.

219
00:53:09,740 --> 00:53:12,494
And you managed to keep
this secret for your own?

220
00:53:13,680 --> 00:53:17,438
He has everything found and destroyed.

221
00:53:21,740 --> 00:53:24,214
See the notes from my mother fires

222
00:53:25,740 --> 00:53:29,319
did more than the beating
he gave me later pain.

223
00:53:33,620 --> 00:53:35,415
I was so ashamed

224
00:53:36,780 --> 00:53:38,495
because I had my owner brought shame.

225
00:53:38,802 --> 00:53:42,432
He recently discovered that
I've never stopped it.

226
00:53:42,760 --> 00:53:45,456
Maybe I deserve death.

227
00:53:47,660 --> 00:53:51,378
I know what it means to kill someone.

228
00:53:53,520 --> 00:53:57,539
Every time you kill someone
dies a part of yourself.

229
00:53:57,800 --> 00:54:00,390
Soon there is nothing good about it.

230
00:54:01,700 --> 00:54:03,434
Do not forget that Mirra.

231
00:54:04,800 --> 00:54:07,535
What your owner did is unforgivable.

232
00:54:10,780 --> 00:54:12,218
I protect and defend.

233
00:54:13,680 --> 00:54:15,514
What he did is murder.

234
00:54:16,740 --> 00:54:19,578
We must go. The sun rises later on.

235
00:55:45,760 --> 00:55:47,597
How's your shoulder?

236
00:55:57,780 --> 00:55:59,192
Here, take it.

237
00:55:59,260 --> 00:56:02,458
I can not. We are different.

238
00:56:03,640 --> 00:56:07,372
My system bumped the previous
tablet that you gave me.

239
00:56:07,800 --> 00:56:11,215
It is a medicine for the pain.

240
00:56:11,280 --> 00:56:14,560
For you, but our bodies are not.
Tolerate your resources

241
00:56:14,720 --> 00:56:16,512
We heal themselves.

242
00:56:17,740 --> 00:56:21,498
Previously it was faster, but
since we have contact with people

243
00:56:22,620 --> 00:56:26,350
began to deteriorate our systems.

244
00:56:26,659 --> 00:56:28,810
How to cure you then?

245
00:56:32,620 --> 00:56:33,593
Door magie.

246
00:56:33,620 --> 00:56:35,036
Magic?

247
00:56:38,680 --> 00:56:42,491
I'm very disappointed in you, Antarah.

248
00:56:42,700 --> 00:56:46,338
How have I failed?

249
00:56:47,640 --> 00:56:51,330
Where is the body?

250
00:56:51,740 --> 00:56:55,251
We have yet to find the man.

251
00:56:55,720 --> 00:57:00,060
Ldioot. The body in the ravine.

252
00:57:00,120 --> 00:57:02,194
The body that I forbade you to touch.

253
00:57:02,210 --> 00:57:04,486
The man has hair.

254
00:57:04,720 --> 00:57:08,354
He claims that she's alive.

255
00:57:08,700 --> 00:57:11,199
If you've caught that human brood,

256
00:57:11,280 --> 00:57:13,497
the woman. please bring me

257
00:57:23,620 --> 00:57:25,571
Why did you save me?

258
00:57:26,680 --> 00:57:28,411
You remind me of someone.

259
00:57:30,760 --> 00:57:34,534
Someone a second chance
deserved but never received.

260
00:57:38,740 --> 00:57:40,809
I wish you were my own.

261
00:57:46,680 --> 00:57:50,554
Would not you like me? - That's not it.

262
00:58:00,680 --> 00:58:03,194
We married. Did you read about that?

263
00:58:03,280 --> 00:58:04,594
Yes, something.

264
00:58:05,800 --> 00:58:10,694
Were you ever married? - Yes. Ever.

265
00:58:10,740 --> 00:58:13,479
Is there something wrong?

266
00:58:17,700 --> 00:58:22,594
I am sorry I will not talk about it.

267
00:58:23,680 --> 00:58:26,494
She is the woman I see before
me when I close my eyes.

268
00:58:28,580 --> 00:58:29,670
Sarah.

269
00:58:31,740 --> 00:58:35,520
We were heading to the doctor
when the invasion began.

270
00:58:36,680 --> 00:58:38,797
We would have a son.

271
00:58:42,660 --> 00:58:44,652
It's been a long time ago.

272
00:58:48,760 --> 00:58:50,431
When you helped me...

273
00:58:52,740 --> 00:58:54,614
When you asked me to close my eyes...

274
00:58:56,760 --> 00:58:58,431
I thought of my mother.

275
00:59:00,712 --> 00:59:02,424
It's weird.

276
00:59:03,720 --> 00:59:07,272
I now understand something
she once told me.

277
00:59:07,283 --> 00:59:11,174
She told me that we are free to choose,

278
00:59:11,260 --> 00:59:14,329
but that we are bound by
what is selected. his

279
00:59:17,780 --> 00:59:20,478
My choices have led me here.

280
00:59:23,626 --> 00:59:25,578
If I had never taken that book,

281
00:59:27,740 --> 00:59:29,652
I would have never met.

282
01:01:01,620 --> 01:01:02,698
Do you see that?

283
01:01:03,760 --> 01:01:05,592
On the main screen.

284
01:01:05,702 --> 01:01:07,558
Why do they make him just wondering?

285
01:01:07,721 --> 01:01:10,331
Is alive he is worth more dead.

286
01:01:12,708 --> 01:01:14,488
Launch the bird.

287
01:01:17,700 --> 01:01:21,650
Lndrukwekkend weapon. A SL9.

288
01:01:21,680 --> 01:01:25,437
Incredibly accurate even at 2100 meters.

289
01:01:25,700 --> 01:01:30,140
I heard de.408 bullets fly faster

290
01:01:30,220 --> 01:01:33,657
and with ball. over een.50 caliber power

291
01:01:34,663 --> 01:01:36,402
Why are you here, man?

292
01:01:36,700 --> 01:01:39,499
Your rebellion is almost over.

293
01:01:40,660 --> 01:01:44,337
Have you so easily fooled by
the propaganda of the few?

294
01:01:44,700 --> 01:01:48,452
You are mistaken in me, my
people and our beliefs.

295
01:01:48,680 --> 01:01:51,170
You essentially change our
prejudices and ideals.

296
01:01:51,200 --> 01:01:54,174
Your ideals are worth fighting.

297
01:01:54,240 --> 01:01:59,553
But those ideals are only a
mask of a hypocritical nation.

298
01:02:01,760 --> 01:02:04,429
A chilling weapon, right?

299
01:02:05,700 --> 01:02:11,470
The size and shape of the
blade made for stabbing,

300
01:02:11,720 --> 01:02:14,337
threading and kill.

301
01:02:14,700 --> 01:02:18,493
I know the wounds that makes such a weapon.

302
01:02:18,707 --> 01:02:21,225
After almost a year on
your planet have fought

303
01:02:21,740 --> 01:02:23,159
I came home.

304
01:02:23,203 --> 01:02:27,223
I found my child lying in the street,

305
01:02:27,260 --> 01:02:30,590
stabbed by a soldier or other people.

306
01:02:31,700 --> 01:02:32,780
That was not me.

307
01:02:34,700 --> 01:02:40,196
My wife with the same weapon slaughtered.
She was like an animal eroded

308
01:02:40,200 --> 01:02:43,334
and then shot through her head.

309
01:02:49,700 --> 01:02:53,174
I promise I will not rest

310
01:02:53,200 --> 01:02:56,593
is wiped out to the last man.

311
01:02:56,720 --> 01:02:59,335
Please.

312
01:03:00,680 --> 01:03:03,354
His wife and child are murdered too.

313
01:03:15,701 --> 01:03:20,378
You are brave. You risk your
life for this man after all.

314
01:03:20,722 --> 01:03:23,498
He has not caused your suffering.

315
01:03:23,700 --> 01:03:25,351
He understands it. He made it himself.

316
01:03:25,720 --> 01:03:27,357
He speaks the truth.

317
01:03:30,711 --> 01:03:33,468
Why else has he saved me?

318
01:03:37,640 --> 01:03:39,460
I see truth in your eyes.

319
01:03:40,620 --> 01:03:45,376
You're not the darkness
that all people see in us.

320
01:03:45,760 --> 01:03:48,411
You fight for our freedom.

321
01:03:50,684 --> 01:03:54,522
You'll live, I promise you.

322
01:03:54,720 --> 01:03:59,478
But the bloodshed that this
man-made must be punished.

323
01:04:01,620 --> 01:04:03,418
No.

324
01:05:20,632 --> 01:05:24,343
Hit it, soldier. I give coverage.

325
01:05:28,700 --> 01:05:30,380
Keep going right.

326
01:05:34,612 --> 01:05:35,492
Naar links.

327
01:05:44,657 --> 01:05:47,411
There are a lot of mines in the open field.

328
01:05:47,780 --> 01:05:49,392
You almost said "boom!"

329
01:05:51,700 --> 01:05:53,490
I know a shelter. Hurry up.

330
01:06:08,700 --> 01:06:10,612
I thought you were dead.

331
01:06:11,640 --> 01:06:12,630
Me?

332
01:06:13,720 --> 01:06:17,350
I'm like an STD. From
me you will not print.

333
01:06:22,683 --> 01:06:25,403
Both life. Matthews has someone with him.

334
01:06:25,640 --> 01:06:28,355
Give me the AZ-12 pilot. I want an arrival.

335
01:06:28,680 --> 01:06:30,372
Ze Zeiden net 50 minutes.

336
01:06:30,700 --> 01:06:34,198
That was 30 minutes ago. I
want to talk to him now.

337
01:06:34,260 --> 01:06:39,373
The rescue bird waiting for orders.
- The pilot is on the line.

338
01:06:39,700 --> 01:06:42,160
Dit is commandant Gibson.
Wees realistisch.

339
01:06:42,220 --> 01:06:44,191
Can you get there undetected?

340
01:06:44,220 --> 01:06:47,158
I want to go there, sir, but
the flak is too strong.

341
01:06:47,200 --> 01:06:50,499
The probability that we lose the bird.
Is large

342
01:06:50,694 --> 01:06:53,448
Break the surgery, but stay ready.

343
01:06:54,760 --> 01:06:57,210
I knew just to escape
before the missile insloeg.

344
01:06:57,700 --> 01:07:01,455
And da's just as well,
otherwise I have been dead.

345
01:07:01,700 --> 01:07:04,420
That explosion was definitely your work?

346
01:07:08,721 --> 01:07:10,552
Who is the woestijnkoningin?

347
01:07:12,678 --> 01:07:14,538
I am responsible for her.

348
01:07:15,637 --> 01:07:18,477
Your safety is my first priority.

349
01:07:18,712 --> 01:07:20,829
We have lost enough men.

350
01:07:21,713 --> 01:07:24,270
I'll take you home. Clear?

351
01:07:24,275 --> 01:07:25,489
I am, sir.

352
01:07:27,712 --> 01:07:30,202
Where are you going? - The shelter monitor.

353
01:07:30,240 --> 01:07:33,529
I will find the position of the
enemy and give us firepower.

354
01:07:40,700 --> 01:07:42,592
Is he safe?

355
01:07:44,720 --> 01:07:46,359
Can we trust him?

356
01:07:46,780 --> 01:07:48,358
One hundred percent.

357
01:08:22,640 --> 01:08:23,620
Kyle?

358
01:08:30,680 --> 01:08:31,658
Alles in orde, Mirra.

359
01:08:32,640 --> 01:08:34,338
It's just Jason.

360
01:09:14,678 --> 01:09:17,418
I give them a taste of their own medicine.

361
01:09:19,701 --> 01:09:22,500
This baby shows very Cerulea shake.

362
01:09:22,713 --> 01:09:24,484
Have a flight?

363
01:09:24,780 --> 01:09:28,457
I know where to go, but I
know of no safe route.

364
01:09:29,692 --> 01:09:33,483
See those mountains in the west? - Yes.

365
01:09:33,720 --> 01:09:36,194
We follow that mountain range to the south.

366
01:09:36,260 --> 01:09:39,439
Thus we should be able to come unseen.

367
01:09:40,665 --> 01:09:41,845
That sounds good.

368
01:09:43,579 --> 01:09:47,592
Here, take it. It is the
last, so be careful with it.

369
01:09:47,703 --> 01:09:50,434
For you, not your girlfriend.

370
01:09:51,680 --> 01:09:53,718
Their drones follow a standard route.

371
01:09:54,780 --> 01:09:56,460
They come every half hour passed.

372
01:09:59,700 --> 01:10:04,353
When the coast is clear, we go to the border.
But take this first.

373
01:10:04,700 --> 01:10:07,340
We explore the area to be sure.

374
01:10:08,700 --> 01:10:11,157
500 meters north is a ridge.

375
01:10:11,200 --> 01:10:15,360
There you have three hours of shade.
I explore the south side.

376
01:10:16,623 --> 01:10:19,340
Check.
- Gecheckt.

377
01:10:19,400 --> 01:10:21,932
Always stay cool.

378
01:10:27,700 --> 01:10:31,471
More food we have. Eat
every few hours a bit.

379
01:10:33,700 --> 01:10:35,453
Ga weg them?

380
01:10:36,646 --> 01:10:41,466
We need to explore the
area and avoid the heat.

381
01:10:41,715 --> 01:10:44,753
Stay in the shade. We are back soon.

382
01:11:48,549 --> 01:11:50,441
I have just arrived, you hear me?

383
01:11:51,622 --> 01:11:53,315
I hear you.

384
01:11:55,703 --> 01:11:57,538
Remember our second broadcast?

385
01:11:57,700 --> 01:12:02,376
We were surrounded, and
you put your gun on quiet

386
01:12:02,800 --> 01:12:06,353
and they popped one by one,
head shot after head shot.

387
01:12:07,700 --> 01:12:10,374
What did you say after
all those head shots?

388
01:12:10,700 --> 01:12:13,472
Headless no headaches. - Exactly.

389
01:12:14,700 --> 01:12:16,476
Headless no headaches.

390
01:12:17,700 --> 01:12:22,134
I know of no one better
with a rifle than you.

391
01:12:22,200 --> 01:12:25,319
Unfortunately, I sit in the wilderness
without my survival weapon.

392
01:12:27,621 --> 01:12:31,476
Okay, time to focus. Stay alert.

393
01:12:31,700 --> 01:12:33,354
Understood.

394
01:12:44,700 --> 01:12:47,451
There's something moving. - Commander?

395
01:12:47,605 --> 01:12:51,123
We're going to get them. First
the bomber, then the bird.

396
01:12:51,200 --> 01:12:54,510
We have a further 30 minutes sight.
- I have the AZ12-Pilot.

397
01:12:54,700 --> 01:12:57,499
Estimated time of arrival?
- Over 40 minutes, sir.

398
01:12:58,760 --> 01:13:00,577
Good. Good luck, soldier.

399
01:13:00,640 --> 01:13:05,312
Make sure to touch. Commander, over.

400
01:13:05,712 --> 01:13:10,150
Say the rescue bird that
there is an AZ12 bomber go.

401
01:13:10,220 --> 01:13:13,360
The area is dangerous
and there are comrades.

402
01:15:31,512 --> 01:15:33,026
I am.

403
01:15:36,686 --> 01:15:40,566
Why did you leave the shelter?
- There were soldiers.

404
01:15:41,700 --> 01:15:43,498
Come on.

405
01:15:50,722 --> 01:15:52,120
Jason, can you hear me?

406
01:15:56,802 --> 01:15:58,434
Jason. Do you hear me?

407
01:16:25,548 --> 01:16:27,405
It is teeming with enemies.

408
01:16:31,732 --> 01:16:33,583
We have a strange job.

409
01:16:35,722 --> 01:16:37,673
We need to kill without question.

410
01:16:38,693 --> 01:16:41,447
When we get home says society:

411
01:16:42,780 --> 01:16:47,235
"Thanks for the killing, but
remember that killing is bad."

412
01:16:47,800 --> 01:16:50,659
As if we could just turn a knob.

413
01:16:51,680 --> 01:16:54,514
Only soldiers understand
what we are undergoing.

414
01:16:55,711 --> 01:16:57,680
We offeren alles op,

415
01:16:58,645 --> 01:17:00,557
mind, body and soul.

416
01:17:00,760 --> 01:17:04,317
Yes, sacrifices must be made.

417
01:17:10,620 --> 01:17:11,454
Dank them.

418
01:17:14,712 --> 01:17:16,392
I think not.

419
01:17:22,646 --> 01:17:25,217
It's like when you fly.

420
01:17:25,280 --> 01:17:27,629
First you put your own mask on.

421
01:17:27,630 --> 01:17:29,429
Do not worry about her.

422
01:17:31,700 --> 01:17:34,474
The struggle for survival is a key

423
01:17:34,740 --> 01:17:38,200
to the true potential of man.

424
01:17:38,260 --> 01:17:42,679
We chained ourselves with
fear and our shortcomings,

425
01:17:42,740 --> 01:17:45,300
but only when death
stares us in the eyes...

426
01:17:46,780 --> 01:17:48,472
That is the turning point.

427
01:17:49,760 --> 01:17:53,471
I saw it for the first time
in Jersey when I was eleven.

428
01:17:54,686 --> 01:17:58,304
I just bought some junk food in a store.

429
01:17:58,700 --> 01:18:02,594
When a homeless person came
in and he smelled of death.

430
01:18:03,705 --> 01:18:06,539
He walked to the cashier, grabbed a gun

431
01:18:06,700 --> 01:18:08,415
and shot him through the head.

432
01:18:09,763 --> 01:18:11,241
I stood rooted to the ground.

433
01:18:12,726 --> 01:18:15,246
He took the money and ran towards me.

434
01:18:15,260 --> 01:18:19,456
His eyes were cold as death.
Without repentance.

435
01:18:20,711 --> 01:18:23,371
He grabbed my junk and left.

436
01:18:23,678 --> 01:18:26,418
I did not know why he did not kill me.

437
01:18:26,740 --> 01:18:29,411
But now I understand.

438
01:18:30,700 --> 01:18:33,156
Every thought, moral or not,

439
01:18:33,220 --> 01:18:37,500
has to bow to the sheer survival instinct.

440
01:18:37,620 --> 01:18:43,297
Are difficult choices
when that happens, easy.

441
01:18:43,360 --> 01:18:44,313
And the impossible...

442
01:18:46,700 --> 01:18:49,298
Is suddenly possible.

443
01:18:54,667 --> 01:18:57,498
Do not worry. The poison is not fatal.

444
01:18:58,700 --> 01:19:00,551
It's incredible what grows here.

445
01:19:05,640 --> 01:19:07,432
What are you planning?

446
01:19:12,700 --> 01:19:16,457
I know you can not make tough decisions.

447
01:19:16,621 --> 01:19:18,480
But I do.

448
01:19:29,674 --> 01:19:31,525
You'll betray our position.

449
01:19:31,640 --> 01:19:33,420
That's what you already did.

450
01:19:49,760 --> 01:19:51,214
Kyle.

451
01:20:46,600 --> 01:20:47,920
Kyle.

452
01:21:07,700 --> 01:21:10,552
How does it feel to betray your people?

453
01:21:20,611 --> 01:21:22,503
We gotta get outta here. Right now

454
01:21:30,700 --> 01:21:32,453
Like them.

455
01:21:43,700 --> 01:21:45,498
What happened?

456
01:21:47,700 --> 01:21:48,680
Sir?

457
01:21:50,705 --> 01:21:51,737
Carpenter is dood.

458
01:21:54,700 --> 01:21:57,369
Only Matthews and wife were with him.

459
01:21:56,711 --> 01:22:00,466
I know.
- Another minute to satellite contact.

460
01:22:01,804 --> 01:22:04,384
The bomber does not make it
before we lose the picture.

461
01:22:05,700 --> 01:22:07,412
Call the bomber back.

462
01:22:10,800 --> 01:22:13,680
Matthews just need to save
themselves until we sight again.

463
01:22:14,700 --> 01:22:17,310
Lnformeer me when we picture.

464
01:22:30,700 --> 01:22:32,492
Ln the storm we escape them.

465
01:22:51,700 --> 01:22:52,690
Kyle?

466
01:25:54,700 --> 01:25:56,478
When did you sleep last?

467
01:25:56,740 --> 01:25:58,680
Not everyone needs sleep sir.

468
01:26:00,711 --> 01:26:03,624
What happened between
Carpenter and Matthews?

469
01:26:03,740 --> 01:26:07,580
Matthews is a good soldier,
he can explain to us.

470
01:26:07,760 --> 01:26:11,575
I have freak. - Send it to my screen.

471
01:26:27,680 --> 01:26:30,514
The civilian vehicle a few days ago.

472
01:26:31,695 --> 01:26:34,264
They are Matthews and his
traveling companion.

473
01:26:34,320 --> 01:26:37,457
They are close to our base at LZ 17.

474
01:26:43,640 --> 01:26:45,452
Hands high.

475
01:26:50,700 --> 01:26:52,557
We do not want trouble.

476
01:26:53,640 --> 01:26:55,438
Stay there.

477
01:23:25,640 --> 01:23:27,393
I got her back.

478
01:26:57,660 --> 01:27:01,391
Do not lie, man. I know everything.

479
01:27:01,760 --> 01:27:06,397
You saved her for her fate.
She does not deserve to live.

480
01:27:07,700 --> 01:27:10,476
Here you will be hands chopped off for.

481
01:27:10,700 --> 01:27:11,390
Enough.

482
01:27:11,420 --> 01:27:16,372
I will drag you with fun
romp through my village.

483
01:27:48,620 --> 01:27:50,256
Scum.

484
01:27:50,635 --> 01:27:53,591
I will soon be your naughty eyes protrude.

485
01:28:04,740 --> 01:28:06,220
Kyle.

486
01:28:07,700 --> 01:28:10,596
I need to ask him something,
now I can talk freely.

487
01:28:20,760 --> 01:28:23,558
Why did you want me, your property, kill?

488
01:28:23,720 --> 01:28:28,172
You have taught me and cared
for me when I was sick.

489
01:28:28,200 --> 01:28:30,474
I remember that, and more.

490
01:28:30,800 --> 01:28:36,470
But you're blind. You were no longer
mine when you were resisting me.

491
01:28:36,702 --> 01:28:40,718
You let the poison of men
flowing in your veins.

492
01:28:40,760 --> 01:28:44,498
I ask you: How could you do this to me?

493
01:28:45,760 --> 01:28:49,458
Take your gun and destroy
what you have dishonored.

494
01:28:49,704 --> 01:28:51,539
Do not listen to him, Mirra.

495
01:28:54,700 --> 01:28:57,331
I see it mensengif works fast.

496
01:28:58,760 --> 01:29:03,440
Let yourself be influenced by that foolish?

497
01:29:03,700 --> 01:29:07,535
I'm sorry, Mirra. He deserves death.

498
01:29:07,700 --> 01:29:10,693
If I do not kill him, he kills us both.

499
01:29:11,787 --> 01:29:14,456
Is killing the only answer?

500
01:29:17,960 --> 01:29:21,498
If you kill someone dies
a part of yourself.

501
01:29:21,711 --> 01:29:24,326
Soon there is nothing good about it.

502
01:29:25,780 --> 01:29:27,397
There is still good in you.

503
01:29:41,720 --> 01:29:43,489
I want you to know.

504
01:29:45,740 --> 01:29:49,477
What you have done to me is unforgivable.

505
01:29:53,740 --> 01:29:55,411
But I forgive you anyway.

506
01:30:53,700 --> 01:30:56,132
There is another person.

507
01:30:56,200 --> 01:30:58,657
A kilometer away. It can be a sniper.

508
01:30:59,435 --> 01:31:01,488
The rescue bird is almost there.

509
01:32:11,715 --> 01:32:15,629
- Take Matthews on board. Bring him home.
-

510
01:32:38,514 --> 01:32:46,514
<b><font color=#FF8080>Sub by: TlTELBlLD, Berlin, 2014</font>
<font color=#00FF00>Timing By Oom St@r, Langsa</font>
<font color=#00FFFF>Resync To BRrip English By JoelzR,
Pariscell-Beureunuen</font></b>