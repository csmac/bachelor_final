1
00:00:39,956 --> 00:00:42,959
[BIRDS CHIRPING]

2
00:00:52,969 --> 00:00:54,971
[HORSES WHINNYING]

3
00:01:15,992 --> 00:01:18,495
[WHINNYING CONTINUES]

4
00:01:24,000 --> 00:01:26,503
Easy, boys.

5
00:01:29,506 --> 00:01:32,008
Easy, now.

6
00:01:33,510 --> 00:01:35,512
[??????]

7
00:01:37,514 --> 00:01:39,015
What the hell?

8
00:01:46,523 --> 00:01:48,525
[??????]

9
00:02:00,036 --> 00:02:01,037
[RUMBLING]

10
00:02:05,542 --> 00:02:08,044
Sir, we have a breach
in the perimeter.

11
00:02:15,051 --> 00:02:16,052
Lock us down.

12
00:02:48,585 --> 00:02:50,587
Then I guess
we better clear the halls.

13
00:02:56,593 --> 00:02:58,094
Go!

14
00:03:33,129 --> 00:03:34,130
Evacuate! Now!

15
00:03:48,645 --> 00:03:49,646
Come get some.

16
00:03:54,651 --> 00:03:55,652
.
Get in

17
00:03:59,155 --> 00:04:00,156
Good man.

18
00:04:14,671 --> 00:04:16,673
[??????]

19
00:04:21,177 --> 00:04:22,178
[CHUCKLES]

20
00:04:46,703 --> 00:04:48,204
Moreattitude?

21
00:04:52,709 --> 00:04:54,711
[KORN'S "FIGHT THE POWER
" PLAYING]

22
00:05:05,722 --> 00:05:07,724
["FIGHT THE POWER"
CONTINUES PLAYING]

23
00:06:58,334 --> 00:07:00,336
Quick and clean...
and contained.

24
00:07:19,355 --> 00:07:20,857
.
that cover
s half the world

25
00:07:33,736 --> 00:07:36,372
[??????]

26
00:07:48,885 --> 00:07:50,386
Attorney-client
privilege.

27
00:08:01,397 --> 00:08:03,900
[OVER SPEAKERS]
Good to see you again,
lieutenant.

28
00:08:13,910 --> 00:08:16,412
I'm inmate 3655 now.

29
00:08:17,914 --> 00:08:19,916
What you doing here, Gibbons?
What you want?

30
00:08:23,419 --> 00:08:25,421
And why would I do
a damn thing for you?

31
00:09:05,962 --> 00:09:08,965
The Darius I remember always
liked to throw the first punch.

32
00:09:14,470 --> 00:09:15,471
New speech, huh?

33
00:09:46,502 --> 00:09:48,004
Hey.

34
00:09:52,008 --> 00:09:53,509
Come on, let's go.

35
00:10:08,524 --> 00:10:10,026
Why don't I get
those assignments, huh?

36
00:10:26,042 --> 00:10:28,044
Full recon.
Wrap it tight.

37
00:10:33,049 --> 00:10:35,051
A-Block to exercise yard.

38
00:10:39,055 --> 00:10:41,557
Time to take the dog
out for a walk.

39
00:11:02,078 --> 00:11:03,579
[??????]

40
00:11:17,093 --> 00:11:19,095
!
Get backup up here

41
00:11:25,601 --> 00:11:26,102
Get him!

42
00:11:31,107 --> 00:11:32,108
Laundry!
West door!

43
00:11:53,629 --> 00:11:55,631
[MAN SPEAKING INDISTINCTLY
OVER PA]

44
00:12:02,138 --> 00:12:03,639
[INMATES SHOUTING]

45
00:12:10,146 --> 00:12:11,147
He's nowhere
 near the wall.

46
00:12:17,153 --> 00:12:18,154
All right, man!

47
00:12:47,683 --> 00:12:48,684
Come back!

48
00:12:54,190 --> 00:12:56,692
[HELICOPTER BLADES WHIRRING]

49
00:12:58,194 --> 00:13:00,196
[??????]

50
00:13:08,204 --> 00:13:09,205
[BULLETS ZIPPING]

51
00:13:16,212 --> 00:13:17,713
You were late.

52
00:13:19,215 --> 00:13:21,217
sir.
You sure know
how to pick 'em,

53
00:13:28,224 --> 00:13:31,227
By the time he wakes up,
we'll be long gone.

54
00:14:09,265 --> 00:14:10,766
?
You know what I'm sayin'

55
00:14:17,273 --> 00:14:18,774
[MOANING]

56
00:14:27,783 --> 00:14:29,285
.
You got it

57
00:14:39,295 --> 00:14:40,796
What I want when I want.

58
00:14:51,307 --> 00:14:53,809
and my personal favorite,
grand theft chopper.

59
00:15:12,328 --> 00:15:13,329
[SOFTLY]
College boy?

60
00:15:14,830 --> 00:15:16,332
Done.

61
00:15:38,354 --> 00:15:39,855
We need a place to lay low.

62
00:15:59,875 --> 00:16:01,877
This is the safest place
in town?

63
00:16:44,920 --> 00:16:49,425
?? Yo, what you think?
It's Dead Celeb ??

64
00:17:09,445 --> 00:17:11,447
I run the chop shop now.

65
00:17:12,448 --> 00:17:13,449
Where's Lo?

66
00:17:18,454 --> 00:17:19,955
[??????]

67
00:17:28,964 --> 00:17:30,466
I don't blend.

68
00:17:44,980 --> 00:17:46,482
Try 20.

69
00:17:47,983 --> 00:17:50,986
[??????]

70
00:17:58,994 --> 00:18:00,496
Nine years.

71
00:18:12,508 --> 00:18:15,010
Hot shower, place to crash.

72
00:18:24,520 --> 00:18:27,022
And I'm not gonna le
t you wreck it this time.

73
00:18:29,024 --> 00:18:30,526
So...

74
00:18:32,528 --> 00:18:34,029
e?
what's in it for m

75
00:18:50,546 --> 00:18:52,548
Somethin' nasty.

76
00:19:58,113 --> 00:20:00,115
[??????]

77
00:20:15,631 --> 00:20:18,133
can you make that
into a personal weapon?

78
00:20:19,134 --> 00:20:20,636
Gimme an hour.

79
00:20:33,649 --> 00:20:35,150
Tupac.

80
00:20:37,102 --> 00:20:39,655
[??????]

81
00:20:51,166 --> 00:20:53,168
Full stealth, total silence.

82
00:20:54,169 --> 00:20:56,171
You're gonna wanna be
real delicate here.

83
00:21:02,177 --> 00:21:03,178
Subtle.

84
00:21:10,185 --> 00:21:12,187
We got an attack,
topside.

85
00:21:14,189 --> 00:21:16,692
Everybody else, follow me.

86
00:21:24,700 --> 00:21:25,701
three minutes.

87
00:21:35,711 --> 00:21:37,212
is northwest corner,
sector four.

88
00:21:43,218 --> 00:21:44,219
t!
Hey! Wai

89
00:21:52,227 --> 00:21:54,730
Remove the first
of three drives facing you.

90
00:22:05,240 --> 00:22:07,242
It wasn't an attack,
it was a diversion.

91
00:22:11,747 --> 00:22:12,748
Now.

92
00:22:17,252 --> 00:22:19,755
[WHOOSHING]

93
00:22:45,781 --> 00:22:47,783
Okay, X. Boat dock,
quarter mile, dead ahead.

94
00:22:53,789 --> 00:22:55,791
Block every road
in a 10-mile radius.

95
00:23:54,850 --> 00:23:55,851
[MEN YELLING]

96
00:24:11,366 --> 00:24:12,868
[THE GRUSOMES' "I PLAY YOU LOS
E" PLAYING]

97
00:24:56,912 --> 00:24:59,414
?
?? Cross that line
And I'm-a rip you dead ?

98
00:25:06,922 --> 00:25:08,924
[HORNS HONKING,
TIRES SCREECHING]

99
00:25:21,937 --> 00:25:24,940
??
?? Cross that line
And I'm-a rip you to shreds

100
00:25:30,445 --> 00:25:31,947
[ENGINE REVVING]

101
00:26:27,002 --> 00:26:29,504
and another 10 for
breaking the general's jaw.

102
00:26:35,510 --> 00:26:36,511
e.
Oh, that's cut

103
00:26:46,521 --> 00:26:47,522
Okay, reel it back.

104
00:26:50,525 --> 00:26:52,027
.
Blow it up

105
00:26:54,529 --> 00:26:57,032
.
Gibbons and Stone
on a comeback tour

106
00:27:10,545 --> 00:27:13,048
[DOGS BARKING]

107
00:28:02,597 --> 00:28:04,099
[FLOOR CREAKS]

108
00:28:38,133 --> 00:28:40,135
MAN:
I'm disappointed,
Auggie.

109
00:28:43,138 --> 00:28:45,140
.
You should've seen it comin'

110
00:28:45,640 --> 00:28:46,641
[GIBBONS GRUNTS]

111
00:28:54,149 --> 00:28:56,151
es.
You were always
taking liberti

112
00:29:09,164 --> 00:29:10,665
[GIBBONS GRUNTS]

113
00:29:12,167 --> 00:29:13,668
I bet you do.

114
00:29:16,671 --> 00:29:18,173
d it?
Didn't heal too well, di

115
00:29:32,187 --> 00:29:34,189
e?
What's that,
just more friendly fir

116
00:29:36,191 --> 00:29:38,693
Some men die so others may live.

117
00:29:57,712 --> 00:29:59,214
h.
Save the speec

118
00:30:02,217 --> 00:30:05,720
it.
whatever you're up to,
you're not gettin' away with

119
00:30:07,722 --> 00:30:10,725
Because Darius Stone
is gonna stop me?

120
00:30:33,248 --> 00:30:35,750
[GRUNTS]

121
00:30:38,253 --> 00:30:39,754
Make it look like an accident.

122
00:30:40,255 --> 00:30:41,756
Zero exposure.

123
00:31:12,954 --> 00:31:15,456
Sir, they found Gibbons.

124
00:31:39,981 --> 00:31:41,983
Back to you in the studio.

125
00:31:44,485 --> 00:31:45,486
let's get to work.

126
00:31:56,497 --> 00:31:59,500
Gibbons said
it wasonthe hard drive.

127
00:32:01,002 --> 00:32:03,004
What're you doing?

128
00:32:15,516 --> 00:32:17,018
Not that old school.

129
00:32:44,545 --> 00:32:45,546
Fire.

130
00:32:48,049 --> 00:32:50,051
[??????]

131
00:33:13,074 --> 00:33:14,575
They're gonna
kill me next.

132
00:33:29,590 --> 00:33:32,093
Okay. See, now that's
a start. Not bad.

133
00:33:40,601 --> 00:33:42,603
.
We go way back

134
00:33:51,612 --> 00:33:53,114
You found him.

135
00:34:00,121 --> 00:34:03,124
[??????]

136
00:34:38,159 --> 00:34:40,161
te.
This'll only take a minu

137
00:35:10,191 --> 00:35:12,693
but I don't trust
anybody right now.

138
00:35:24,705 --> 00:35:26,707
gun instructional program
for kids, and--

139
00:36:03,744 --> 00:36:05,746
od.
try a cookout with free fo

140
00:36:21,762 --> 00:36:23,764
I'll call you about that.

141
00:36:29,770 --> 00:36:31,772
[NORMAL]
Tell me about you
and the captain.

142
00:36:40,281 --> 00:36:41,282
Military?

143
00:36:54,295 --> 00:36:56,297
Well, Reverend...

144
00:36:57,798 --> 00:37:00,801
you think you can lay your hands
on a tuxedo?

145
00:37:02,303 --> 00:37:07,308
[PLAYING WILD, BAROQUE MELODY]

146
00:38:15,376 --> 00:38:16,877
One way to find out.

147
00:38:59,920 --> 00:39:00,921
Three days.

148
00:39:15,436 --> 00:39:16,437
[SNAP]

149
00:39:23,944 --> 00:39:24,945
MAN: Hey!

150
00:39:32,453 --> 00:39:33,954
Black guy, white tux.

151
00:39:41,462 --> 00:39:43,464
[??????]

152
00:39:48,469 --> 00:39:49,470
Had enough of the party?

153
00:39:50,471 --> 00:39:52,473
[TIRES SQUEAL]

154
00:39:55,476 --> 00:39:56,977
Hold on.

155
00:39:58,479 --> 00:39:59,480
[HONKING]

156
00:40:14,495 --> 00:40:16,997
Well, half of them
are still enlisted.

157
00:40:23,003 --> 00:40:25,005
Every single one of them
just disappeared.

158
00:40:42,022 --> 00:40:44,024
Not bad for a senator's aide.

159
00:40:57,037 --> 00:40:58,539
Really?

160
00:41:13,554 --> 00:41:16,056
Sounds a lot like
where I grew up.

161
00:41:20,060 --> 00:41:22,563
This is nothin' like
where I grew up.

162
00:41:24,565 --> 00:41:27,067
Let's get you out of
those clothes, shall we?

163
00:41:35,075 --> 00:41:37,077
Not really my style.

164
00:41:55,095 --> 00:41:57,097
Anything at all.

165
00:41:59,099 --> 00:42:01,101
How about
some fries and a shake?

166
00:42:09,610 --> 00:42:13,113
The things I can't do
for my country.

167
00:42:24,124 --> 00:42:26,126
[CLANGING IN DISTANCE]

168
00:42:27,628 --> 00:42:30,130
[??????]

169
00:42:36,136 --> 00:42:39,640
[CLANGING CONTINUES]

170
00:42:45,646 --> 00:42:48,649
Charlie?

171
00:42:58,158 --> 00:43:00,160
Charlie.

172
00:43:07,668 --> 00:43:10,671
[CLANGING]

173
00:43:16,176 --> 00:43:18,178
e?
Pettibon

174
00:43:18,679 --> 00:43:20,681
[SIRENS WAILING]

175
00:43:22,182 --> 00:43:24,685
ch.
She set me up. Bit

176
00:43:31,692 --> 00:43:33,694
Come out with your hands
behind your head.

177
00:44:11,231 --> 00:44:13,734
I'm comin' in unarmed!

178
00:44:18,739 --> 00:44:20,240
All right, close that door.

179
00:44:24,745 --> 00:44:25,245
Hands.

180
00:44:48,769 --> 00:44:49,770
Now, turn around.

181
00:45:17,297 --> 00:45:18,298
Who set you up?

182
00:45:20,300 --> 00:45:20,801
Deckert.

183
00:45:30,811 --> 00:45:32,312
Too close to what?

184
00:45:43,824 --> 00:45:45,325
Never seen him bef
ore in my life.

185
00:45:47,327 --> 00:45:49,329
sy.
Ea

186
00:45:51,331 --> 00:45:53,333
That sure looks
a lot like you.

187
00:46:18,859 --> 00:46:21,361
it's time for you to go.

188
00:46:23,363 --> 00:46:24,865
CAPTAIN:
Where is he?

189
00:46:33,874 --> 00:46:36,376
[??????]

190
00:46:43,383 --> 00:46:44,885
[BEEPING]

191
00:46:55,395 --> 00:46:56,897
[BEEPING INTENSIFYING]

192
00:48:21,481 --> 00:48:22,482
Right.

193
00:48:41,501 --> 00:48:43,503
[??????]

194
00:48:56,016 --> 00:48:58,018
You know,
you don't have to wait up.

195
00:49:01,521 --> 00:49:02,522
[CHUCKLES]

196
00:49:04,024 --> 00:49:05,525
What's so specia
l about this one?

197
00:49:22,542 --> 00:49:24,544
w.
Everything's more expensive no

198
00:49:29,049 --> 00:49:31,051
I could see that.

199
00:49:33,053 --> 00:49:34,554
Come here.

200
00:49:48,568 --> 00:49:51,071
And I still
get my hands dirty.

201
00:50:14,094 --> 00:50:16,596
You remember all that damage
we did in the back seat?

202
00:50:22,602 --> 00:50:23,603
Not for me.

203
00:50:25,605 --> 00:50:27,107
ge?
Ready to do
some more dama

204
00:50:43,623 --> 00:50:46,626
.
It's an aircra
ft carrier, man

205
00:50:47,627 --> 00:50:49,629
e.
Looks like we goin' upstat

206
00:50:58,638 --> 00:51:01,641
[??????]

207
00:51:51,691 --> 00:51:55,195
[MEN CHATTERING]

208
00:52:18,718 --> 00:52:20,720
Daddy's little girl.

209
00:52:55,255 --> 00:52:56,256
Captain?

210
00:53:13,773 --> 00:53:15,275
get that far. I was
hoping maybe you did.

211
00:53:26,286 --> 00:53:27,787
t us.
No, no, no.
Don't worry abou

212
00:53:29,789 --> 00:53:31,658
You know you should'
ve killed that bitch!

213
00:53:42,802 --> 00:53:47,307
Liebo, get men fore and aft.
Push him down here.

214
00:54:02,822 --> 00:54:04,324
[ENGINE RUMBLING]

215
00:54:11,831 --> 00:54:13,833
!
MAN:
Seal the hatch

216
00:54:51,371 --> 00:54:52,872
Fire!

217
00:55:02,882 --> 00:55:04,884
No, but yours is gonna take you
straight to hell.

218
00:55:09,389 --> 00:55:10,390
Stand by.

219
00:55:13,893 --> 00:55:14,894
Now!

220
00:55:30,910 --> 00:55:33,413
.
Keep this up and you ain't gon
nahave nothin' left, hillbilly

221
00:55:38,418 --> 00:55:40,420
You and I have
unfinished business, homeboy.

222
00:55:50,430 --> 00:55:51,431
Fire.

223
00:55:57,937 --> 00:55:59,439
All right, come on
! Let's go!

224
00:56:09,449 --> 00:56:11,451
Hey!
What the hell is he doing?

225
00:56:23,463 --> 00:56:25,965
Where the hell you going?!
Over there! That way!

226
00:56:51,491 --> 00:56:52,992
Get that tank topside now!

227
00:56:56,496 --> 00:56:57,497
Move, move, move!

228
00:56:58,998 --> 00:57:01,501
Bravo Squad to flight deck. Now!

229
00:57:20,019 --> 00:57:21,521
Locked on.

230
00:57:29,529 --> 00:57:30,029
Fire now.

231
00:57:42,041 --> 00:57:43,042
Goddamn it!

232
00:57:47,547 --> 00:57:49,048
[GUNFIRE]

233
00:58:20,079 --> 00:58:23,082
Dive Team Number Three,
report any visual.

234
00:58:42,101 --> 00:58:44,604
Maybe then we can turn some of
these enemies into allies.

235
00:58:47,106 --> 00:58:50,109
I know this doesn't
particularly thrill you, George.

236
00:59:00,119 --> 00:59:03,122
It goes without saying,
Mr. President.

237
00:59:18,137 --> 00:59:21,641
XXX:
Downstairs. Your car.
Come alone.

238
00:59:26,145 --> 00:59:28,147
[ALARM CHIRPS]

239
00:59:32,151 --> 00:59:33,653
L]
[TIRES SQUEA

240
00:59:39,659 --> 00:59:41,160
Take a look.

241
01:00:15,194 --> 01:00:18,197
This man is makin' a move
on the U.S. Capitol.

242
01:00:22,201 --> 01:00:23,202
is?
See th

243
01:00:28,207 --> 01:00:30,209
.
He's running security for
the State of the Union tonight

244
01:00:35,214 --> 01:00:36,215
Oh, that's beautiful.

245
01:00:41,220 --> 01:00:44,724
Look, these boys don't
play defense, just offense.

246
01:00:50,730 --> 01:00:52,231
rong?
What if you're w

247
01:01:02,742 --> 01:01:04,744
ES]
[DOOR CLOS

248
01:01:14,587 --> 01:01:16,589
.
All of them served
with Gibbons and Stone

249
01:01:20,593 --> 01:01:22,095
ng.
I think
we got somethi

250
01:01:31,604 --> 01:01:35,108
Anything the Defense Departmen
t needs to know?

251
01:01:36,609 --> 01:01:38,611
r.
I'm sure it's nothing
you don't already know, si

252
01:01:58,631 --> 01:02:00,133
dy.
Well, you know,
we never did ID his bo

253
01:02:02,635 --> 01:02:04,137
Terrible thing,
accident like that.

254
01:02:09,142 --> 01:02:10,643
[??????]

255
01:02:13,646 --> 01:02:15,148
what did Darius St
one say to you?

256
01:02:27,160 --> 01:02:28,661
Sounds like Stone.

257
01:02:33,666 --> 01:02:35,168
.
if you hear anything..

258
01:02:37,170 --> 01:02:38,671
I'll know who to chase down.

259
01:02:42,675 --> 01:02:44,177
Sir, could you
tell me something?

260
01:02:49,182 --> 01:02:51,184
How do you pick the men
who protect the president?

261
01:03:06,699 --> 01:03:07,200
Good luck, sir.

262
01:03:29,222 --> 01:03:31,724
U.
You wanna richen her up, you'
re gonna have to remap the EC

263
01:03:38,731 --> 01:03:40,733
t.
I didn't always wear a sui

264
01:03:46,739 --> 01:03:49,242
Can we talk?

265
01:04:00,253 --> 01:04:01,754
Look at this.

266
01:04:25,778 --> 01:04:28,281
Figure we gotta go
outside the box on this one.

267
01:04:39,792 --> 01:04:42,295
we're gonna need
some brothers we can trust.

268
01:05:05,818 --> 01:05:06,819
No joke.

269
01:05:31,344 --> 01:05:32,845
.
on the same bloc
k as the White House

270
01:05:44,357 --> 01:05:47,860
ng.
'Cause if Deckert takes 
ovfreedom won't be free for lo

271
01:05:55,868 --> 01:05:57,870
Yeah. The freedom part
was a bit much, though.

272
01:06:11,884 --> 01:06:12,885
I'll do what I can.

273
01:07:04,437 --> 01:07:05,938
[GLOVE HUMMING]

274
01:07:24,457 --> 01:07:25,958
se.
Lola.
Come on, man, plea

275
01:07:34,967 --> 01:07:36,969
.
across country
in civilian trucks

276
01:07:39,472 --> 01:07:40,473
?? Yeah! ??

277
01:07:58,991 --> 01:08:00,493
[BOTH LAUGHING]

278
01:08:01,994 --> 01:08:04,497
?? Yeah, yeah, yeah
Yeah, yeah ??

279
01:08:06,499 --> 01:08:08,501
Let's give '
em a little assistance.

280
01:08:37,029 --> 01:08:38,531
Move the level,
the brakes get stuck.

281
01:08:48,040 --> 01:08:49,542
We'll say it was
75 black guys.

282
01:09:12,064 --> 01:09:13,566
So, what you think, D-Train?

283
01:09:15,067 --> 01:09:16,068
This enough bang for you?

284
01:09:23,075 --> 01:09:25,077
[HELICOPTER WHIRRING]

285
01:10:08,120 --> 01:10:11,123
And you've never been 
able to see beyond yourself.

286
01:10:26,639 --> 01:10:28,140
What can one man do?

287
01:10:30,643 --> 01:10:33,646
[??????]

288
01:10:37,650 --> 01:10:40,653
Follow me!
Stay and squat!

289
01:11:21,193 --> 01:11:24,196
[??????]

290
01:11:29,702 --> 01:11:34,206
[CHEERING & APPLAUSE]

291
01:11:39,712 --> 01:11:42,214
[REPORTERS CHATTERING]

292
01:11:54,727 --> 01:11:56,729
chief among them,
the new military bill.

293
01:12:06,739 --> 01:12:09,241
the president
of the United States.

294
01:12:24,256 --> 01:12:28,761
that the state of our union
is strong.

295
01:12:41,273 --> 01:12:44,276
?? Come on ??

296
01:12:54,286 --> 01:12:57,790
if we are to truly flourish
and survive.

297
01:13:03,796 --> 01:13:04,797
[ENGINE ROARS]
Hold on.

298
01:13:16,308 --> 01:13:19,311
We must use understanding...

299
01:13:21,313 --> 01:13:22,815
and compassion...

300
01:13:27,319 --> 01:13:30,322
and compromise to win battles...

301
01:13:47,339 --> 01:13:50,843
?
?? Brothers and sisterrrrrrs ?

302
01:13:57,850 --> 01:13:58,851
There's no such thing
as a dead end.

303
01:14:30,382 --> 01:14:32,384
ha!
Now, that's what
I'm talkin' about! Ha-

304
01:14:33,886 --> 01:14:35,387
it.
Oh, sh

305
01:14:37,389 --> 01:14:39,892
I think you might wann
a back up right about now.

306
01:14:44,897 --> 01:14:45,898
!
Go, go, go, go, go

307
01:14:49,902 --> 01:14:50,903
Everybody out now!

308
01:15:16,929 --> 01:15:18,931
Get those jacks outta there.

309
01:15:52,965 --> 01:15:55,968
I will ask you tonight
to support this new--

310
01:16:06,979 --> 01:16:09,481
Okay, let's put these du
bs to work.

311
01:16:12,484 --> 01:16:14,486
You sure you know
how to drive this thing?

312
01:16:15,988 --> 01:16:16,989
[ALARMS BLARING]

313
01:16:18,490 --> 01:16:20,492
Just tryin' to get
a feel for her.

314
01:16:21,994 --> 01:16:23,996
Yeah! I could
get used to this.

315
01:16:34,006 --> 01:16:36,508
Just following
protocol, sir.

316
01:16:49,021 --> 01:16:50,022
We're in position.

317
01:17:00,032 --> 01:17:01,533
[PEOPLE SCREAMING]

318
01:17:06,538 --> 01:17:08,040
All units respond immediately.

319
01:17:20,052 --> 01:17:22,054
[GUNFIRE, PEOPLE SCREAMING]

320
01:17:34,066 --> 01:17:35,067
Is this you, George?

321
01:17:36,068 --> 01:17:37,569
Are you doing this?

322
01:17:46,578 --> 01:17:48,080
with the blood of patriots."

323
01:17:59,091 --> 01:18:00,592
in?
What world are you living

324
01:18:05,597 --> 01:18:06,598
Get rid of him.

325
01:18:11,603 --> 01:18:12,604
Fire in the hole.

326
01:18:22,614 --> 01:18:23,615
One Team, twelve.

327
01:18:31,623 --> 01:18:33,125
Sergeant.

328
01:18:48,140 --> 01:18:50,142
Let's finish this.

329
01:19:14,166 --> 01:19:15,167
We'll be with you
in 30 seconds--

330
01:19:25,677 --> 01:19:26,678
I said, drop it.

331
01:19:29,181 --> 01:19:30,682
[MUMBLES]

332
01:19:32,184 --> 01:19:33,185
[YELLS]

333
01:19:42,694 --> 01:19:45,197
or a "Happy to see you,
Stone" or nothin'.

334
01:19:50,702 --> 01:19:52,704
I told you you should've
killed that bitch.

335
01:20:07,719 --> 01:20:09,721
It's over.

336
01:20:10,722 --> 01:20:12,724
Things not going
according to plan?

337
01:20:20,232 --> 01:20:21,733
rain.
We'll fall back to the t

338
01:20:35,247 --> 01:20:36,748
[GUNFIRE]

339
01:20:39,751 --> 01:20:41,253
s!
Go after him!
We'll take care of thi

340
01:21:18,957 --> 01:21:20,959
SANFORD: Do what he says.

341
01:21:23,461 --> 01:21:24,462
[GUNFIRE]
No!

342
01:21:25,463 --> 01:21:26,464
Drive.

343
01:21:35,473 --> 01:21:38,977
Toby, I need a ride,
somethin' fast.

344
01:21:55,994 --> 01:21:56,995
Go get him.

345
01:22:04,502 --> 01:22:06,004
Heard you needed a ride.

346
01:22:16,014 --> 01:22:17,515
[ENGINE REVS]

347
01:22:35,533 --> 01:22:37,535
Neither am I. Let's go.

348
01:22:47,045 --> 01:22:49,047
[??????]

349
01:23:06,064 --> 01:23:07,565
[HORN BLARING]

350
01:23:41,599 --> 01:23:42,600
There it is.

351
01:23:46,104 --> 01:23:47,605
Okay, get me closer.

352
01:23:56,114 --> 01:23:58,116
Faster.

353
01:24:08,126 --> 01:24:09,627
m.
Sit down, Ji

354
01:24:14,132 --> 01:24:15,633
Fuck you.

355
01:24:19,137 --> 01:24:22,640
Okay, let's see
what half a million buys.

356
01:24:48,166 --> 01:24:51,169
Maybe you can't, but he can.

357
01:25:39,717 --> 01:25:41,719
[??????]

358
01:26:12,250 --> 01:26:14,252
.
You might get your own holiday

359
01:26:33,771 --> 01:26:35,273
[YELLS]

360
01:27:07,805 --> 01:27:08,806
[YELLS]

361
01:27:45,343 --> 01:27:48,346
Hillbilly,
you need to lighten up.

362
01:27:56,854 --> 01:27:58,856
Then we won't be
needing you anymore.

363
01:28:02,860 --> 01:28:05,363
ve.
Mo

364
01:28:21,879 --> 01:28:22,880
All right, here we go.

365
01:28:38,896 --> 01:28:39,897
.
I don't think so

366
01:28:59,417 --> 01:29:00,418
Can't hold it steady.

367
01:29:01,419 --> 01:29:02,920
Down.

368
01:29:12,430 --> 01:29:14,932
I think of you
every time I chew steak.

369
01:29:18,436 --> 01:29:20,938
And I thought of you
every night I spent in prison.

370
01:29:32,450 --> 01:29:34,952
Go! Run! Aah!

371
01:29:43,461 --> 01:29:44,962
Sir!

372
01:29:46,464 --> 01:29:47,465
p.
You're gonna have to jum

373
01:30:06,484 --> 01:30:07,985
[STEELE YELLS]

374
01:30:20,498 --> 01:30:21,499
Almost in.

375
01:30:31,008 --> 01:30:32,510
I've been waitin' for 
this for 10 years!

376
01:30:37,014 --> 01:30:38,015
Well, keep waiting.

377
01:30:45,022 --> 01:30:47,525
Your turn to do the dying,
general.

378
01:31:42,079 --> 01:31:43,581
id?
What's that you sa

379
01:31:54,091 --> 01:31:55,593
And then some.

380
01:31:56,594 --> 01:31:59,096
[??????]

381
01:33:21,178 --> 01:33:22,179
Mm.

382
01:33:25,182 --> 01:33:26,684
s?
So, what's thi

383
01:33:28,185 --> 01:33:30,187
You out?

384
01:33:39,196 --> 01:33:41,699
[??????]

385
01:33:48,706 --> 01:33:51,709
That ought to keep me going
another nine years.

386
01:34:05,222 --> 01:34:08,225
The second-best ride of my life.

387
01:34:26,744 --> 01:34:27,745
D...

388
01:34:28,245 --> 01:34:30,247
n'.
you forgot somethi

389
01:34:52,269 --> 01:34:54,772
?? What is done is what is done
It's all for the fun ??

390
01:40:34,611 --> 01:40:36,113
?? Dis dat block ??

391
01:40:40,117 --> 01:40:41,618
?? Dis dat block ??

392
01:40:45,622 --> 01:40:47,124
?? Dis dat block ??

393
01:40:52,129 --> 01:40:54,131
?? Dat block ??

