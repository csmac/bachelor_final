﻿1
00:00:00,200 --> 00:00:03,201
-SupunWe- | supun@zoom.lk | supunwee@facebook.com

2
00:00:03,201 --> 00:00:06,089
I think it's safe to say the maze trials
were a complete success.

3
00:00:08,136 --> 00:00:11,344
It's to soon to say,
but they could be the key to everything.

4
00:00:13,039 --> 00:00:15,640
It's time now to begin phase two.

5
00:00:16,076 --> 00:00:17,082
Welcome to the Scorch!

6
00:00:20,077 --> 00:00:22,580
The world outside is hanging on
by a very thin thread.

7
00:00:24,129 --> 00:00:25,525
Beyond this door

8
00:00:25,733 --> 00:00:26,818
lies the beginning

9
00:00:27,026 --> 00:00:28,361
of your new lives.

10
00:00:29,266 --> 00:00:31,489
- Hey, Thomas!
- We weren't he only maze.

11
00:00:33,259 --> 00:00:35,118
What do you remember about Wicked?

12
00:00:35,368 --> 00:00:37,467
I remember they sent me to the maze.

13
00:00:38,095 --> 00:00:40,504
I remember watching my friends die
in front of me.

14
00:00:40,754 --> 00:00:43,335
- Don't you want to understand?
- Understand what?

15
00:00:43,543 --> 00:00:44,795
Why this all happened?

16
00:00:45,628 --> 00:00:47,255
I just need to know,

17
00:00:47,644 --> 00:00:49,132
whose side are you on?

18
00:00:51,171 --> 00:00:53,470
- They're hiding something.
- Come on, you don't know that!

19
00:00:54,303 --> 00:00:55,430
They lied to us.

20
00:00:55,596 --> 00:00:57,613
We never escaped.
It's all just been part of their plan.

21
00:00:57,932 --> 00:00:59,476
What do they want from us?

22
00:01:01,833 --> 00:01:03,897
Thomas!
The maze is one thing.

23
00:01:04,483 --> 00:01:07,150
But you kids won't last one day
out in the Scorch.

24
00:01:15,299 --> 00:01:16,301
What the hell?

25
00:01:19,094 --> 00:01:20,719
The course of your lives

26
00:01:20,969 --> 00:01:23,667
will determine the course of humanity.

27
00:01:25,241 --> 00:01:26,342
Into light,

28
00:01:26,592 --> 00:01:27,604
or darkness.

29
00:01:28,420 --> 00:01:43,914
-SupunWe- | supun@zoom.lk | supunwee@facebook.com

