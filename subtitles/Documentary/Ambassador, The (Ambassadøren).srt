1
00:13:18,308 --> 00:13:21,365
You should not touch weapons.

2
00:13:22,413 --> 00:13:24,992
You should not touch diamonds.

3
00:13:26,635 --> 00:13:29,429
And don't touch the wives of the staff.

4
00:13:31,583 --> 00:13:33,633
Everything besides that is green.

5
00:14:41,470 --> 00:14:44,470
As soon as you have decided,
Mr. Mads will contact Europe.

6
00:14:53,359 --> 00:14:56,235
Now we're talking about the contract.

7
00:14:58,304 --> 00:15:01,379
--We have already fixed the primary deal.
--You can have a lot of diamonds.

8
00:15:03,718 --> 00:15:07,582
It's about the water. It has rained so much that...

9
00:15:08,575 --> 00:15:09,475
--Did you understand that?
--No.

10
00:15:10,281 --> 00:15:16,523
--He's saying that the work with one of the
mines has progressed so much

11
00:15:16,523 --> 00:15:22,367
--that when the water disappears it is just about
digging up the diamonds as quickly as possible.

12
00:15:22,900 --> 00:15:26,104
He needs a sum of 10 million CAF.

13
00:15:27,227 --> 00:15:30,705
That would suffice to get the diamonds up
in a couple of weeks.

14
00:15:30,881 --> 00:15:38,283
Actually I'd prefer to cooperate with
businessmen, but I do trust you.

15
00:17:20,193 --> 00:17:24,234
--Who framed this picture? Done down here?
--Yes.

16
00:17:24,416 --> 00:17:26,984
--Slanted...
--Very slanted.

17
00:17:35,003 --> 00:17:37,242
--You got the business cards, right?
--Yeah, I'll go get them.

18
00:18:01,708 --> 00:18:04,450
--A gin tonic? Vodka tonic?
--Vodka.

19
00:18:05,989 --> 00:18:09,244
I am the representative of the secretary of state
and I am in charge of domestic security issues.

20
00:18:10,261 --> 00:18:14,801
I am responsible for areas like espionage and counterintelligence.

21
00:18:15,221 --> 00:18:19,500
--And he has been in the French Foreign Legion?
--Yes, for 26 years.

22
00:18:20,277 --> 00:18:22,677
He was in the Foreign Legion for 26 years.

23
00:18:24,917 --> 00:18:31,543
France gave me citizenship in 1986
as a thank you for my duty.

24
00:18:31,669 --> 00:18:32,569
--They confiscated my passport in 2006.
--Aha! That says it all.

25
00:18:37,533 --> 00:18:43,524
In December 2006 I was prosecuted for mercenary.

26
00:18:43,797 --> 00:18:48,623
France considers the CAR as its piggy bank.

27
00:18:49,513 --> 00:18:56,279
They consider everything underground as
belonging to France. Nobody else.

28
00:18:57,061 --> 00:19:01,819
It is like savings that
we are not allowed to touch.

29
00:19:02,061 --> 00:19:05,732
It belongs to France, and that seems
to be the opinion still, he says.

30
00:19:06,109 --> 00:19:12,018
And underground there is copper, iron
manganese, cobolt, uranium, cinnabar...

31
00:19:13,933 --> 00:19:17,299
He says that they have all sorts
of minerals underground.

32
00:19:17,397 --> 00:19:19,019
We know where --

33
00:19:20,005 --> 00:19:26,208
-- but every time we ask for a 
 contribution to start the mining...

34
00:19:28,677 --> 00:19:32,008
-- ... there are problems.
--Are they obstructing you?

35
00:19:32,061 --> 00:19:33,561
But how? What do they do?

36
00:19:34,000 --> 00:19:39,029
When one wants to prevent someone from running,
they may put a stone in the runner's shoe.

37
00:19:39,613 --> 00:19:40,513
--[France] puts stones in their shoes.
-- If one wants to prevent a country's development...

38
00:19:44,357 --> 00:19:50,289
... all its resources will be used to hunt rebels.
Then the country ceases to develop.

39
00:27:00,501 --> 00:27:04,794
--The bulldozer or the 10 millions?
--The millions.

40
00:27:12,405 --> 00:27:15,947
We thought about making a preliminary investment--

41
00:27:15,947 --> 00:27:19,317
-- with a man named Gilbert Dalkia.
Do you know him?

42
00:27:21,109 --> 00:27:23,089
Yes.

43
00:27:25,547 --> 00:27:28,085
Do you think it is a good idea?

44
00:27:28,185 --> 00:27:33,312
It depends whether you are controlling
him or vice versa.

45
00:27:33,412 --> 00:27:40,312
What should we do to control him in order
to prevent him from taking control?

46
00:27:42,641 --> 00:27:48,224
Here you need to know what relations
people have with one another.

47
00:27:48,324 --> 00:27:55,307
If he believes that you know people who are
mightier than himself, he will contain himself.

48
00:27:55,407 --> 00:28:00,936
But if he believes otherwise
you will lose everything.

49
00:28:01,036 --> 00:28:04,233
Then we end up losing everything.

50
00:30:02,734 --> 00:30:06,442
Mr. Cortzen, perhaps you can ...?

51
00:30:11,925 --> 00:30:14,080
[Minister for public administration]

52
00:30:50,837 --> 00:30:54,571
You educate people so that they become productive.
Even pygmees.

53
00:30:54,671 --> 00:30:59,472
He says that you are touching an issue
that is close to his heart--

54
00:30:59,572 --> 00:31:05,749
-- that is thinking of the human development,
and the fact that you educate.

55
00:31:05,849 --> 00:31:13,231
That is very important, especially with the
pygmees, that you seek to highen their level.

56
00:31:22,278 --> 00:31:28,668
I am currently moving to a city where the
indigenous people are pygmees.

57
00:31:28,768 --> 00:31:35,412
They are in the are where I live, so it
is easy to get hold of them if you want.

58
00:31:35,512 --> 00:31:40,525
I can try to arrange a meeting with
the city leaders.

59
00:32:18,872 --> 00:32:24,288
The baguettes. This is a frenchman.
And this is a Chinese.

60
00:32:24,388 --> 00:32:30,200
--Chinese have narrow eyes.
--And the frenchman's hair should be more spiky.

61
00:32:30,300 --> 00:32:35,360
Then the Chinese should have smaller eyes.
But it's pretty, isn't it?

62
00:32:54,277 --> 00:33:01,357
With this contract, if M. Dalkia finds
 diamonds that he wishes to export--

63
00:33:01,357 --> 00:33:05,221
--he can take them out legally,
using the contract.

64
00:33:05,221 --> 00:33:07,909
Then I can take the stones to Europe.

65
00:33:14,473 --> 00:33:16,873
Definitely.

66
00:34:33,336 --> 00:34:37,063
They dance to conjure up the spirits--

67
00:34:37,163 --> 00:34:38,408
-- in the shape of a whirlwind.

68
00:34:43,347 --> 00:34:49,106
They have made a whirlwind by taking
off their clothes and swinging them.

69
00:34:51,691 --> 00:34:52,691
--Now you will get a chance to 
meet them.

70
00:40:58,457 --> 00:41:02,584
And then he gave me a driver's license--

71
00:41:02,684 --> 00:41:07,368
--indicating that I live on
Clay Street in Monrovia.

72
00:41:40,194 --> 00:41:43,232
We have a Danish businessman--

73
00:41:43,332 --> 00:41:49,785
--who for many years has
shown an interest in Liberia.

74
00:41:49,885 --> 00:41:52,576
We find it appropriate to apply --

75
00:41:52,676 --> 00:42:00,046
-- for the [inaud.] privilege of serving
as our honoary consule in Bangui in CAR.

76
00:43:22,003 --> 00:43:26,795
Put your phone over there. Then you'll have
an excuse if it starts ringing.

77
00:45:17,904 --> 00:45:25,135
--Mr. Gilbert! How is it going?
--Very well. Long time no see!

78
00:46:05,934 --> 00:46:10,853
--Is it good?
--It is not good.

79
00:46:17,052 --> 00:46:23,384
Companionship and law is not in
my competency.

80
00:46:33,122 --> 00:46:36,344
Can you ask Girad who of these two
will be my assistant?

81
00:46:36,444 --> 00:46:39,983
It is him.
He represents the chieftan.

82
00:46:48,635 --> 00:46:52,083
--They move about in pairs. Never alone.

83
00:46:53,998 --> 00:47:00,241
Can they both participate on Saturday?

84
00:47:16,748 --> 00:47:19,798
Come on, let's go to the boat.

85
00:48:14,460 --> 00:48:19,826
When Bokassa was overthrown,
the French reinstalled Dacko

86
00:48:19,926 --> 00:48:26,818
-- and by their order he gave the
people permission to plunder at will.

87
00:48:31,306 --> 00:48:35,426
The French soldiers had orders to
just stand idly.

88
00:48:35,526 --> 00:48:42,122
THey were only to protect the French
embassy and Dacko's residence.

89
00:48:42,224 --> 00:48:45,832
--So there was violence?
--Everything was destroyed.

90
00:48:45,932 --> 00:48:49,417
Did the French want it to collapse?

91
00:48:49,517 --> 00:48:52,107
-- Yes.
--But why?

92
00:48:54,023 --> 00:49:01,343
If the country kept Bokassa
it would be the Switzerland of Africa today.

93
00:49:01,429 --> 00:49:07,873
Instead of exporting raw material,
we were to export refined goods.

94
00:49:07,973 --> 00:49:16,249
But when I talk to other business diplomats
I often hear the same story.

95
00:49:16,349 --> 00:49:20,729
The companies that try to establish
fly the country immediately.

96
00:49:20,829 --> 00:49:26,033
The French taught the people a nasty
habit, that is corruption.

97
00:49:26,133 --> 00:49:29,553
When a minister is exposed as corrupt--

98
00:49:29,653 --> 00:49:35,289
-- he automatically gets visa, passport
and residency in France.

99
00:52:12,521 --> 00:52:15,786
--My business card.
--Thanks.

100
00:52:15,886 --> 00:52:21,186
Why is it heating up in Birao? Now and then
 the trampling of boots can be heard.

101
00:52:21,286 --> 00:52:29,914
-- because there is oil. And the forces...

102
00:52:30,014 --> 00:52:35,274
The forces... that fight for the
oil from Birao...

103
00:52:35,374 --> 00:52:37,410
... they are creating the problems.

104
00:52:37,510 --> 00:52:46,153
It is not a Central African problem,
it is other forces standing behind.

105
00:52:46,253 --> 00:52:52,010
-- And who are these forces?
--Forces that you know very well.

106
00:53:49,420 --> 00:53:56,556
Two French planes, one in the morning, one
in the evening, fly over CAR every day.

107
00:53:56,656 --> 00:54:00,460
Two military reconnaissance planes.

108
00:54:00,560 --> 00:54:06,476
For three years we have requested
insight into their reports, but we'll never get it.

109
00:54:06,576 --> 00:54:09,868
-- Where are the planes headed?
--They just overfly the country.

110
00:54:09,968 --> 00:54:15,989
They take photos and sweep the area
with heat and movement sensors.

111
00:54:16,089 --> 00:54:19,300
They know who is moving there but
they never tell us anything.

112
00:54:19,400 --> 00:54:23,076
The column that attacked Birao
consisted of 27 vehicles --

113
00:54:23,176 --> 00:54:28,876
--and came out of Sudan, 400 kilometers
away, three days before the attack.

114
00:54:28,976 --> 00:54:37,860
The French knew about it, but they
never told us. There is your stone in the shoe.

115
00:54:37,960 --> 00:54:41,516
Hello! How is it going?

116
00:54:50,870 --> 00:54:56,259
--Is that a government soldier?
--Yes. All of them.

117
00:55:21,890 --> 00:55:26,786
--How much time do we have left?
--An hour and ten minutes.

118
00:55:37,203 --> 00:55:39,299
Let's go.

119
00:55:49,357 --> 00:55:53,540
-- What is that?
--The mine.

120
00:55:59,452 --> 00:56:02,951
There it is.

121
00:56:05,827 --> 00:56:07,737
The diamonds.

122
00:56:28,713 --> 00:56:31,208
My workforce.

123
00:56:37,070 --> 00:56:38,913
Like this.

124
00:56:39,013 --> 00:56:46,521
The biggest stones in CAR are from here.

125
00:56:50,312 --> 00:56:53,279
I love you... The mine.

126
00:57:12,533 --> 00:57:18,129
--We need to keep the time.
--It is OK. We have 35 minutes to go.

127
00:57:18,229 --> 00:57:21,153
Then let's take a greeting round.

128
00:57:43,447 --> 00:57:47,807
How long will it take to go to the plane?

129
00:58:02,488 --> 00:58:05,907
--Don't force her!
--No one is forcing her.

130
00:58:06,007 --> 00:58:08,499
--Leave her.
--Open!

131
00:58:08,599 --> 00:58:14,147
How can you say that we are
forcing her?

132
00:58:14,247 --> 00:58:22,331
Should we go? -- No! She wants me to go.
Don't ridicule us in front of the white men.

133
00:58:39,921 --> 00:58:42,296
The plane. Yes, that is it.

134
01:01:16,364 --> 01:01:23,624
99 percent of all matches sold here
come from Cameroun.

135
01:01:25,109 --> 01:01:26,261
-- The brand Le Boxeur?
--Yes.

136
01:01:26,361 --> 01:01:31,770
And the real owner--

137
01:01:31,870 --> 01:01:36,496
--is a Lebanese with a French
diplomatic passport--

138
01:01:36,596 --> 01:01:42,080
--who in secrecy works for the
previous French tobacco monopoly.

139
01:01:42,180 --> 01:01:46,308
Can I expect him trying to hamper
me if I try to open a factory here?

140
01:01:46,408 --> 01:01:52,253
It depends on your will to
go through with this.

141
01:02:43,533 --> 01:02:50,260
Charles Massi was a minister. He
exploited his position--

142
01:02:50,360 --> 01:02:55,866
--to embezzle state money, 
procure weapons and start a rebellion.

143
01:02:59,188 --> 01:03:04,439
He tried to carry out a coup 
d'etat with the help of France.

144
01:03:06,837 --> 01:03:11,175
If he disappears he disappears.

145
01:03:12,412 --> 01:03:15,661
No one is exactly
crying over that.

146
01:03:31,397 --> 01:03:40,374
He looks like a person of 
progression, as a pioneer.

147
01:03:40,474 --> 01:03:45,076
And I think the most important
thing to remember about the CAR is--

148
01:03:45,176 --> 01:03:49,870
--that it is a young country with
many possibilities.

149
01:04:23,201 --> 01:04:26,447
--Thank you for the visit.
--Thank you.

150
01:06:01,190 --> 01:06:07,630
Cortzen Group will pay to Gilbert Dalkia
a sum of 10 million CAF.

151
01:06:07,730 --> 01:06:12,460


152
01:06:12,560 --> 01:06:19,679
Additional five millions?
I need to translate this into Danish.

153
01:06:19,779 --> 01:06:23,462
--Are you aware that they want us to
pay five millions in addition?

154
01:06:23,562 --> 01:06:24,762
--How much is that?

155
01:06:24,862 --> 01:06:29,143
Besides the 10 millions you
have already agreed on--

156
01:06:29,243 --> 01:06:32,673
--you have consented to
paying five millions extra--

157
01:06:32,773 --> 01:06:38,457
--to facilitate Mr. Gilbert's paying of taxes and fees.

158
01:06:39,937 --> 01:06:46,178
That was included in the ten millions.
How much can we afford to pay him?

159
01:06:46,278 --> 01:06:50,830
--I need to have a look at that. I cannot say anything conclusively.

160
01:06:50,930 --> 01:06:52,307
--But we need to conclude this now.
--Yes, yes, but --

161
01:06:52,407 --> 01:06:58,380
-- it is completely ridiculous that they
now claim five millions that was never discussed.

162
01:07:01,240 --> 01:07:07,608
I heard that if we went to Zako yesterday--

163
01:07:07,708 --> 01:07:11,252
--we could have bought 17 carat
diamonds worth 90 million.

164
01:07:11,352 --> 01:07:16,161
With five million we can
get hold of a large stone.

165
01:07:16,261 --> 01:07:19,963
Or what? So pay me.

166
01:07:20,063 --> 01:07:26,879
He is talking bullshit. He says that we
can find diamonds for 80 millions, blah blah.

167
01:07:26,979 --> 01:07:31,146
I feel like telling him "Shut up. If you have
that much money you can pay yourself."

168
01:07:31,246 --> 01:07:37,386
It is bullshit. It is all bullshit. But he wants
ten millions now. Otherwise there is no agreement.

169
01:07:40,991 --> 01:07:46,639
The only thing I want to say that when there
are this many changes to a contract--

170
01:07:46,739 --> 01:07:52,344
--it is quite common to re-read it thoroughly
and think about it before signing it.

171
01:07:52,444 --> 01:07:57,540
I know that we're in Africa and there is
momentum now, I'm just saying that--

172
01:07:57,640 --> 01:08:02,438
--I would never sign this if I were you.
I simply would not.

173
01:09:20,551 --> 01:09:27,742
I found one stone wirth 34 billion.
But they took it from me.

174
01:09:42,285 --> 01:09:47,895
Sodiam and Catadiam was oned 
by a Lebanese, Hassan Al-Baqaash--

175
01:09:47,995 --> 01:09:53,029
-- who by the way is on USA's list
of known terrorists--

176
01:09:53,129 --> 01:09:55,446
--because he has supported Hamas.

177
01:09:56,423 --> 01:10:01,167
--Do Gilbert and Sodiam still cooperate?
--Yes.

178
01:10:27,535 --> 01:10:30,850
When he has left, I am to meet the president.

179
01:10:40,027 --> 01:10:42,152
--Who?
--Him.

180
01:10:42,252 --> 01:10:48,048
No, it is for MPs.
With this I conduct diamond politics.

181
01:10:48,148 --> 01:10:51,488
When I become a member
I can negotiate for him.

182
01:10:56,502 --> 01:10:59,803
When I get a diplomatic passport...

183
01:11:43,337 --> 01:11:46,811
The brother of the consule of Senegal.

184
01:11:50,126 --> 01:11:51,717
No problem.

185
01:11:51,817 --> 01:11:58,889
I'm thinking of sending Liberia's
consule-to-be in Bangui to you.

186
01:11:58,989 --> 01:12:04,529
Treat him as you would me. Thank you!

187
01:12:04,629 --> 01:12:09,575
Thank you, your excellence. Bye.

188
01:12:09,848 --> 01:12:12,865
Cortzen Group dispurses a sum
of 10 million franc--

189
01:12:12,965 --> 01:12:20,338
--according to the agreement that
was signed in Bangui on November 21 2010.

190
01:15:16,500 --> 01:15:23,699
How can it be that when
we were to sign the contract--

191
01:15:24,319 --> 01:15:29,369
--then you tell us that we
must sign the other contract?

192
01:16:26,200 --> 01:16:29,295
I have made four copies.

193
01:16:29,395 --> 01:16:34,908
One for the notary, one for Mr. Gilbert, one for the archive.

194
01:16:46,630 --> 01:16:52,001
I said I cannot do anything about it.
And by the way it is no problem.

195
01:22:33,342 --> 01:22:35,998
--Gilbert! How are you doing?
--Well, thanks!

196
01:23:38,716 --> 01:23:45,324
If they find those and the papers
aren't in order, you will be fined 50-100 millions.

197
01:24:16,654 --> 01:24:20,881
There's one there if you lift your foot.

198
01:24:31,991 --> 01:24:34,593
--Look under your shoes.

199
01:26:21,778 --> 01:26:27,121
Just stuff them down like this,
then it is done and he can go.

200
01:26:53,303 --> 01:26:55,723
And so many!

201
01:26:55,662 --> 01:26:59,626
And there are more where
they come from.

