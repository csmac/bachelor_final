1
00:00:20,300 --> 00:00:25,590
<i>When I open my eyes, Irie-kun is right next to me.</i>

2
00:00:25,590 --> 00:00:29,210
<i>I'm so happy!</i>

3
00:00:32,710 --> 00:00:38,960
<i>Perhaps... I am the most happiest person in the world?</i>

4
00:00:41,590 --> 00:00:44,480
<i>It's already been 2 and a half years since the marriage,</i>

5
00:00:44,480 --> 00:00:50,580
<i>and still my love for Irie-kun reaches a new record everyday!</i>

6
00:00:57,210 --> 00:00:59,300
Good morning.

7
00:01:00,650 --> 00:01:04,010
Don't look at me with your swollen face.

8
00:01:04,010 --> 00:01:06,890
It's not good to look at.

9
00:01:09,210 --> 00:01:12,610
<i>Forget about what he said now.</i>

10
00:01:14,860 --> 00:01:17,150
<i>I love you.</i>

11
00:01:19,320 --> 00:01:21,200
Good morning.

12
00:01:21,200 --> 00:01:23,670
Good morning, Kotoko-chan.

13
00:01:23,670 --> 00:01:27,020
You could have slept a bit more since it's the spring holidays.

14
00:01:27,020 --> 00:01:29,290
I'm your unchanging Daughter-in-law...

15
00:01:29,290 --> 00:01:31,570
Oh my.

16
00:01:32,620 --> 00:01:35,920
<i>Nice Mother-in-law!</i>

17
00:01:39,300 --> 00:01:40,670
Good morning, Mama.

18
00:01:40,670 --> 00:01:41,660
And Kotoko-chan.

19
00:01:41,660 --> 00:01:42,610
Good morning.

20
00:01:42,610 --> 00:01:44,750
Good morning, Father-in-law.

21
00:01:44,750 --> 00:01:46,390
Shall I make you some coffee?

22
00:01:46,390 --> 00:01:50,310
Oh~! Thank you, Kotoko-chan!

23
00:01:52,190 --> 00:01:54,460
<i>Nice Father-in-law!</i>

24
00:01:54,460 --> 00:01:55,640
Kotoko!

25
00:01:55,640 --> 00:01:59,120
You washed my shirt with my student handbook in a pocket.

26
00:02:00,460 --> 00:02:01,660
You noticed that?

27
00:02:01,660 --> 00:02:05,630
It won't fit into a pocket now!

28
00:02:08,990 --> 00:02:12,510
<i>Forget about him, too.</i>

29
00:02:13,580 --> 00:02:15,530
- Good morning<br>- Good morning!

30
00:02:15,530 --> 00:02:17,400
Kotoko, can you make me some coffee?

31
00:02:17,400 --> 00:02:18,660
Of course~!

32
00:02:18,660 --> 00:02:21,580
<i>And... And...</i>

33
00:02:21,580 --> 00:02:26,190
<i>The coolest person in the world - my husband!</i>

34
00:02:26,190 --> 00:02:32,440
<i>This happiness will never end... was what I thought.</i>

35
00:02:32,440 --> 00:02:34,830
<i>Until that time.</i>

36
00:02:35,650 --> 00:02:39,980
Oh, I forgot to mention,

37
00:02:39,980 --> 00:02:44,440
but Rika-chan will stay with us for a while starting tomorrow.

38
00:02:44,440 --> 00:02:46,180
Rika?<br>- Rika will?

39
00:02:46,180 --> 00:02:47,950
So she came back.

40
00:02:47,950 --> 00:02:51,460
That's right! In 7 years!

41
00:02:51,460 --> 00:02:54,310
Who is Rika-chan?

42
00:02:54,310 --> 00:02:57,800
That's right. You didn't know, right?

43
00:02:57,800 --> 00:03:01,180
She is Naoki and Yuki's cousin.

44
00:03:01,180 --> 00:03:03,520
You're incomparable to her.

45
00:03:03,520 --> 00:03:06,760
She is really smart and pretty.

46
00:03:07,850 --> 00:03:11,790
Until she went to America because of her dad's job 7 years ago,

47
00:03:11,790 --> 00:03:15,490
she used to come here and play with these two.

48
00:03:15,490 --> 00:03:17,590
Why will she stay here for a while?

49
00:03:17,590 --> 00:03:22,240
Oh, her family will come back in May,

50
00:03:22,240 --> 00:03:26,860
but Rika-chan will go to a high school here in April and came back by herself.

51
00:03:26,860 --> 00:03:29,870
So, for a month, until her family comes back here,

52
00:03:29,870 --> 00:03:33,100
we agreed to let her stay here with us.

53
00:03:33,100 --> 00:03:37,490
That's how it is. So Rika is coming back.

54
00:03:40,050 --> 00:03:45,150
<i>What kind of girl is Rika-chan who makes woman hater Yuki-kun this happy?</i>

55
00:03:45,150 --> 00:03:49,230
As I recall, Rika-chan used to win

56
00:03:49,230 --> 00:03:52,250
piano competitions and cooking competitions, right?

57
00:03:52,250 --> 00:03:55,410
She is cheerful and nice, isn't she?

58
00:03:55,410 --> 00:04:00,150
Yes, she has been bright ever since she was little.

59
00:04:01,300 --> 00:04:04,590
So Rika-chan is like a perfect person.

60
00:04:04,590 --> 00:04:09,190
So different from idiot Kotoko.

61
00:04:09,190 --> 00:04:13,120
Think of her as your little sister. Please take care of her.

62
00:04:13,120 --> 00:04:15,160
Okay.

63
00:04:26,200 --> 00:04:28,020
So heavy!

64
00:04:29,100 --> 00:04:31,450
It feels like my arms are being torn.

65
00:04:31,450 --> 00:04:34,990
I'm almost there.

66
00:04:39,310 --> 00:04:40,550
I'm sorry!

67
00:04:40,550 --> 00:04:43,370
No, I'm sorry!

68
00:04:46,880 --> 00:04:48,200
Sorry.<br>- Sorry!

69
00:04:48,200 --> 00:04:50,330
Thank you.

70
00:04:55,190 --> 00:04:59,760
So you were coming to our house, right?

71
00:05:01,000 --> 00:05:05,100
Are you Rika-chan? You're Rika-chan, right?

72
00:05:05,100 --> 00:05:07,860
No, I'm not.

73
00:05:07,860 --> 00:05:09,220
You're not...

74
00:05:09,220 --> 00:05:11,890
I'm sorry.

75
00:05:16,510 --> 00:05:18,450
Thank you.

76
00:05:18,450 --> 00:05:20,340
I'm so sorry.

77
00:05:20,340 --> 00:05:22,570
It's okay.

78
00:05:24,200 --> 00:05:27,510
Perhaps you're Yuki's friend?

79
00:05:27,510 --> 00:05:30,620
What?

80
00:05:30,620 --> 00:05:33,320
Excuse me, then!

81
00:05:33,320 --> 00:05:36,200
Wait a minute!

82
00:05:42,210 --> 00:05:44,330
I'm home.

83
00:05:44,330 --> 00:05:46,420
Welcome back.

84
00:05:47,300 --> 00:05:48,770
Seriously.

85
00:05:48,770 --> 00:05:50,550
Look in front of you.

86
00:05:50,550 --> 00:05:52,980
I was.

87
00:05:52,980 --> 00:05:56,070
You suddenly ran into me.

88
00:05:57,490 --> 00:06:01,600
<i>She seems to be about the same age as Yuki-kun.</i>

89
00:06:01,600 --> 00:06:03,020
Onii-chan!

90
00:06:03,020 --> 00:06:05,110
Oh, Yuki.

91
00:06:05,110 --> 00:06:06,660
Yuki, there was...

92
00:06:06,660 --> 00:06:11,050
Rika. Rika is here!

93
00:06:11,050 --> 00:06:13,300
Hey Rika!

94
00:06:21,790 --> 00:06:26,590
Wow. She is so cute!

95
00:06:27,590 --> 00:06:29,560
Naoki!

96
00:06:40,610 --> 00:06:43,190
Naok, you've become more handsome again.

97
00:06:43,190 --> 00:06:47,120
You too. You've become prettier as well.

98
00:06:50,680 --> 00:06:53,800
<i>What is this?!</i>

99
00:06:58,860 --> 00:07:03,740
♫KISS what I've longed for♫

100
00:07:03,740 --> 00:07:07,530
♫the complicated kiss from you♫

101
00:07:07,530 --> 00:07:15,770
♫Mischievous Kiss♫

102
00:07:27,620 --> 00:07:30,480
♫On the sloping path♫

103
00:07:30,480 --> 00:07:35,260
♫The wind passes through. If I look up at the sky♫

104
00:07:35,260 --> 00:07:39,070
♫The sun is shining brightly♫

105
00:07:39,070 --> 00:07:41,890
♫With an innocent expression♫

106
00:07:41,890 --> 00:07:50,010
♫I catch up and naturally our eyes meet SPARK♫

107
00:07:50,010 --> 00:07:52,850
♫No matter if I cry or laugh, or if things are difficult♫

108
00:07:52,850 --> 00:07:55,660
♫Even if I want to escape reality♫

109
00:07:55,660 --> 00:07:58,520
♫This feeling is the only thing♫

110
00:07:58,520 --> 00:08:02,890
♫I can't control♫

111
00:08:02,890 --> 00:08:04,890
♫KISS the greedy kiss♫

112
00:08:04,890 --> 00:08:06,450
♫The kiss I remember♫

113
00:08:06,450 --> 00:08:09,140
♫The kiss that awakened me♫

114
00:08:09,140 --> 00:08:13,170
♫Mischievous Kiss♫

115
00:08:14,390 --> 00:08:16,370
♫KISS the kiss I've waited for♫

116
00:08:16,370 --> 00:08:17,860
♫The kiss that betrayed me♫

117
00:08:17,860 --> 00:08:20,460
♫It started with a kiss♫

118
00:08:20,460 --> 00:08:25,300
♫Mischievous Kiss♫

119
00:08:25,300 --> 00:08:28,170
♫Oh Kiss♫

120
00:08:28,170 --> 00:08:30,890
♫Oh Kiss♫

121
00:08:37,750 --> 00:08:47,180
<i>Mischievous Kiss 2 ~Love in TOKYO~</i>

122
00:08:48,120 --> 00:08:50,920
Thank you for doing this for me today.

123
00:08:50,920 --> 00:08:54,390
I'm so happy nobody has changed.

124
00:08:54,390 --> 00:08:57,730
Let's toast for Rika-chan who came back safely

125
00:08:57,730 --> 00:09:02,540
and stays here as a part of our family for a while.

126
00:09:02,540 --> 00:09:06,840
Cheers!

127
00:09:06,840 --> 00:09:08,470
Thank you.<br>- Yes, welcome back.

128
00:09:08,470 --> 00:09:10,450
Cheers! Thank you.

129
00:09:10,450 --> 00:09:13,450
Thank you.

130
00:09:13,450 --> 00:09:15,390
Nice to meet you, Kotoko-san.

131
00:09:15,390 --> 00:09:16,840
Nice to meet you.

132
00:09:16,840 --> 00:09:19,530
So you go to school from tomorrow, right?

133
00:09:19,530 --> 00:09:23,640
Yes, I'm going to Sumiregaoka Gakuin.<br>(gakuin: school)

134
00:09:23,640 --> 00:09:25,430
Sumiregaoka Gakuin?

135
00:09:25,430 --> 00:09:27,540
The one that's very famous and for the intelligent...?

136
00:09:27,540 --> 00:09:30,620
Yeah, well because it's kind of in my neighbourhood.

137
00:09:30,620 --> 00:09:33,380
<i>It's not the kind of school that you can enter...</i>

138
00:09:33,380 --> 00:09:36,110
<i>just because it's in you neighbourhood right...</i>

139
00:09:36,110 --> 00:09:39,910
I heard you scored full marks for the transfer admission test.

140
00:09:39,910 --> 00:09:41,070
Full marks?!

141
00:09:41,070 --> 00:09:45,170
Kotoko, you should learn from Rika.

142
00:09:45,990 --> 00:09:49,050
Ah, but Kotoko-san is more amazing, don't you think?

143
00:09:49,050 --> 00:09:51,520
Which part of Kotoko? Better than you?

144
00:09:51,520 --> 00:09:57,670
Well, I think capturing Naoki's heart is <br>a hundred times harder than getting full marks on a test.

145
00:09:57,670 --> 00:10:01,360
Now that you mention it, that seems true!

146
00:10:01,360 --> 00:10:04,420
Oniichan is stubborn.

147
00:10:04,420 --> 00:10:08,420
We were so surprised to learn Naoki got married.

148
00:10:08,420 --> 00:10:14,340
Kotoko-san is great to win the heart of Naoki who's been known as woman hater.

149
00:10:14,340 --> 00:10:16,690
I did try my best.

150
00:10:16,690 --> 00:10:20,470
But you approached him, right?

151
00:10:20,470 --> 00:10:21,720
Yes.

152
00:10:21,720 --> 00:10:25,610
I thought so. Otherwise...

153
00:10:27,280 --> 00:10:33,970
Bummer. I should've claimed him before going to America.

154
00:10:33,970 --> 00:10:41,430
<i>I know she is his cousin, but she is getting too clingy.</i>

155
00:10:42,410 --> 00:10:44,060
Rika-chan!

156
00:10:44,060 --> 00:10:47,960
Enough with touching Naoki!

157
00:10:47,960 --> 00:10:50,370
Why don't you eat my special lasagna?

158
00:10:50,370 --> 00:10:52,750
Wow, it looks delicous!

159
00:10:52,750 --> 00:10:55,470
I missed your cooking so much!

160
00:10:55,470 --> 00:10:57,000
Here you go.

161
00:10:58,360 --> 00:11:02,210
<i> Subtitles and timing by the Mischievous Kisser Team @vik.com </i>

162
00:11:03,060 --> 00:11:04,960
Isn't that...

163
00:11:04,960 --> 00:11:08,300
a serious crisis?

164
00:11:08,300 --> 00:11:10,610
I thought so.

165
00:11:10,610 --> 00:11:14,110
She's a lively high schooler.

166
00:11:14,110 --> 00:11:18,640
Face, style and brain. They're all perfect, right?

167
00:11:18,640 --> 00:11:20,970
-Yes.<br>- And...

168
00:11:20,970 --> 00:11:24,360
They're cousins who grew up together.

169
00:11:24,360 --> 00:11:29,160
She has a past with Irie-san that you <br>don't know anything about.

170
00:11:29,160 --> 00:11:31,920
They hugged each other suddenly...

171
00:11:31,920 --> 00:11:33,880
"Naoki~!"

172
00:11:33,880 --> 00:11:36,070
She called his name so casually.

173
00:11:36,910 --> 00:11:40,040
She's not bad. Such an admirable enemy.

174
00:11:40,040 --> 00:11:41,890
What do you mean an enemy?

175
00:11:41,890 --> 00:11:43,930
She is, isn't she?

176
00:11:43,930 --> 00:11:46,260
To you,

177
00:11:46,260 --> 00:11:48,110
and to me, too.

178
00:11:48,110 --> 00:11:52,790
But there is no way that she would have feelings for Irie-kun.

179
00:11:52,790 --> 00:11:53,720
Yeah. Probably.

180
00:11:53,720 --> 00:11:56,240
It's obvious! What are you saying?

181
00:11:56,240 --> 00:12:00,260
Hey, we're talking about Irie-san here!

182
00:12:01,030 --> 00:12:03,750
-Are you sure?<br>- I am absolutely sure.

183
00:12:06,340 --> 00:12:08,570
This cream parfait is delicious.

184
00:12:08,570 --> 00:12:10,570
<br>

185
00:12:10,570 --> 00:12:15,490
She really is cheerful and seems like <br>a good girl, doesn't she?

186
00:12:15,490 --> 00:12:20,450
It's just, I feel a bit left out.

187
00:12:20,450 --> 00:12:23,480
Like I'm being alienated.

188
00:12:23,480 --> 00:12:25,630
I see.

189
00:12:26,680 --> 00:12:30,620
You're biggest problem this time...

190
00:12:31,540 --> 00:12:33,000
is his family.

191
00:12:33,000 --> 00:12:33,880
What?

192
00:12:33,880 --> 00:12:36,540
Up until now they were always on your side,

193
00:12:36,540 --> 00:12:39,710
but now they're all on her side.

194
00:12:39,710 --> 00:12:42,530
Don't exaggerate.

195
00:12:42,530 --> 00:12:44,250
You're too soft.

196
00:12:44,250 --> 00:12:47,480
This is no time to be talking so complacently!

197
00:12:47,480 --> 00:12:51,310
While everyone is making such a fuss of her,

198
00:12:51,310 --> 00:12:53,590
Irie-san...

199
00:12:53,590 --> 00:12:56,690
may be taken from you before you know it.

200
00:12:56,690 --> 00:12:58,920
N-No way.

201
00:12:58,920 --> 00:13:03,550
You will be completely forgotten.

202
00:13:07,510 --> 00:13:11,490
Moto-chan!

203
00:13:11,490 --> 00:13:14,610
Thanks for the dessert.

204
00:13:14,610 --> 00:13:16,080
What?

205
00:13:22,100 --> 00:13:24,500
Kotoko-san.

206
00:13:24,500 --> 00:13:26,320
Rika-chan.

207
00:13:26,320 --> 00:13:30,740
So many guys have been trying to pick me up until now.

208
00:13:30,740 --> 00:13:32,600
Pick you up?

209
00:13:32,600 --> 00:13:35,050
Guys here are so immature.

210
00:13:35,050 --> 00:13:37,620
Is that okay?

211
00:13:37,620 --> 00:13:43,480
The guys in my class aren't great either. <br>Not one of them is good-looking.

212
00:13:44,880 --> 00:13:48,970
I grew up with Naoki, so no one else <br>measures up.

213
00:13:48,970 --> 00:13:51,230
I have high standards.

214
00:13:51,810 --> 00:13:56,760
Poor things. Being compared to Irie-kun.

215
00:13:56,760 --> 00:13:59,560
Hey, Kotoko-san...

216
00:14:00,670 --> 00:14:02,890
Naoki...

217
00:14:02,890 --> 00:14:05,430
Could you give him to me?

218
00:14:05,430 --> 00:14:06,650
What?

219
00:14:08,630 --> 00:14:10,770
It's fine, right?

220
00:14:10,770 --> 00:14:13,390
Give him to me.

221
00:14:14,010 --> 00:14:17,870
R-Rika-chan...that's...

222
00:14:19,500 --> 00:14:24,380
Just look at your face! I'm kidding.

223
00:14:26,580 --> 00:14:30,930
Kidding. Just kidding.

224
00:14:40,240 --> 00:14:45,550
Maybe I should just let it go...

225
00:14:50,980 --> 00:14:54,070
Who is it? Mother might be doing laundry?

226
00:14:54,070 --> 00:14:55,440
Kotoko-san.

227
00:14:55,440 --> 00:14:58,870
Rika-chan! What is it?

228
00:14:58,870 --> 00:15:01,150
Let's take a bathe together!

229
00:15:01,150 --> 00:15:03,400
Rika-chan..that's a little...

230
00:15:03,400 --> 00:15:05,660
We're both girls, so it's fine!

231
00:15:05,660 --> 00:15:08,220
That's a bit...

232
00:15:15,010 --> 00:15:17,810
She has a great figure.

233
00:15:25,130 --> 00:15:27,610
Hey, Kotoko-san...

234
00:15:27,610 --> 00:15:31,900
you're the one who proposed, aren't you?

235
00:15:31,900 --> 00:15:34,440
Of course not.

236
00:15:34,440 --> 00:15:36,960
Really?

237
00:15:36,960 --> 00:15:40,720
<i>I always seem to end up following Rika-chan's pace.</i>

238
00:15:40,720 --> 00:15:44,010
<i>No, no, no! Don't get caught up!</i>

239
00:15:44,010 --> 00:15:46,950
<i>I have to be confident! confident!</i>

240
00:15:46,950 --> 00:15:49,500
<i>She's just a high school girl.</i>

241
00:15:49,500 --> 00:15:53,630
<i>And I am Irie-kun's wife!</i>

242
00:15:54,340 --> 00:15:57,360
Can I ask one more question?

243
00:15:57,360 --> 00:15:59,390
What is it?

244
00:15:59,390 --> 00:16:03,360
Is Naoki good at kissing?

245
00:16:03,360 --> 00:16:05,060
What?!

246
00:16:08,420 --> 00:16:10,580
Well...

247
00:16:10,580 --> 00:16:14,030
I don't know...

248
00:16:14,030 --> 00:16:18,180
I've really only kissed Irie-kun, so...

249
00:16:18,180 --> 00:16:20,660
Really?

250
00:16:20,660 --> 00:16:21,570
<br>

251
00:16:21,570 --> 00:16:25,110
Then you wouldn't really know.

252
00:16:25,110 --> 00:16:29,990
I had lots of boyfriends while I was in the US,

253
00:16:29,990 --> 00:16:32,330
so I kissed quite a bit.

254
00:16:32,330 --> 00:16:34,220
<i>No.</i>

255
00:16:34,220 --> 00:16:36,530
<i>I feel dizzy now.</i>

256
00:16:36,530 --> 00:16:40,940
I think I'm done for now.

257
00:16:46,820 --> 00:16:48,680
But,

258
00:16:48,680 --> 00:16:52,220
the best kisser so far

259
00:16:52,920 --> 00:16:54,820
is Naoki.

260
00:16:54,820 --> 00:16:56,420
<br>

261
00:17:00,230 --> 00:17:03,450
Naoki's really good...

262
00:17:03,450 --> 00:17:05,800
at kissing.

263
00:17:07,900 --> 00:17:10,100
<i>What did she say?</i>

264
00:17:10,100 --> 00:17:14,420
I'm jealous. You can kiss Naoki whenever.

265
00:17:14,420 --> 00:17:18,840
I really want to kiss him one more time.

266
00:17:18,840 --> 00:17:21,860
But I don't think it was that kind of kiss.

267
00:17:21,860 --> 00:17:26,380
Like the kind of kiss you give your childhood friend on their cheek.

268
00:17:26,380 --> 00:17:29,770
I was in 5th grade and Naoki was a junior in high school.

269
00:17:29,770 --> 00:17:33,200
It was on the lips, of course.

270
00:17:33,200 --> 00:17:35,410
What?!

271
00:17:35,410 --> 00:17:39,540
I think it was both of our first kisses.

272
00:17:44,970 --> 00:17:48,890
This can't be true!!!

273
00:17:49,900 --> 00:17:51,440
Kotoko-san?!

274
00:17:51,440 --> 00:17:54,590
<i>Irie-kun's first kiss...</i>

275
00:17:54,590 --> 00:17:59,030
<i>wasn't with me but with Rika?</i>

276
00:18:00,670 --> 00:18:03,260
Kotoko-san!

277
00:18:03,260 --> 00:18:06,900
<i>Somebody...tell me it's a lie...</i>

278
00:18:06,900 --> 00:18:09,540
<i>Irie-kun...Irie-kun...</i>

279
00:18:09,540 --> 00:18:11,970
<i>Irie...</i>

280
00:18:11,970 --> 00:18:13,970
<br>

281
00:18:18,760 --> 00:18:23,580
<i>Kotoko-san, I started dating Naoki.</i>

282
00:18:23,580 --> 00:18:25,090
<i>What?</i>

283
00:18:25,090 --> 00:18:31,470
<i>What are you saying?! Irie-kun's married to me!</i>

284
00:18:31,470 --> 00:18:35,230
<i>No matter how you look at it, I and Naoki look better.</i>

285
00:18:35,230 --> 00:18:39,950
<i>And I kissed Naoki first.</i>

286
00:18:39,950 --> 00:18:42,440
<i>-Right, Naoki?<br>-Yeah.</i>

287
00:18:42,440 --> 00:18:45,100
<i>That's right.</i>

288
00:18:45,100 --> 00:18:49,820
<i>Naoki and I, we were both our first kiss.</i>

289
00:18:50,580 --> 00:18:54,480
<i>Hey, Naoki.</i>

290
00:18:54,480 --> 00:18:56,470
<i>Let's kiss in front of her.</i>

291
00:18:56,470 --> 00:18:57,220
<i>Sure.</i>

292
00:18:57,220 --> 00:19:03,840
<i>Stop it!</i>

293
00:19:08,350 --> 00:19:12,040
What? It was a dream?

294
00:19:19,600 --> 00:19:21,960
I slept in!

295
00:19:27,930 --> 00:19:30,280
Oh, good morning, Kotoko-san.

296
00:19:30,280 --> 00:19:32,830
Are you alright?

297
00:19:32,830 --> 00:19:36,040
You fainted last night. You could have slept some more.

298
00:19:36,040 --> 00:19:39,180
Oh, I borrowed your apron.

299
00:19:42,520 --> 00:19:46,660
Naoki, you like omelet. Do you want some?

300
00:19:46,660 --> 00:19:47,850
-Yes.<br>- Me too.

301
00:19:47,850 --> 00:19:51,480
-Okay, leave it to me<br> -Rika-chan, it's okay! I will do it!

302
00:19:51,480 --> 00:19:53,220
But I am better at making this.

303
00:19:53,220 --> 00:19:55,060
You don't know until you do it, right?

304
00:19:55,060 --> 00:19:58,340
It's okay. You can rest, Kotoko-san.

305
00:19:58,340 --> 00:20:00,700
What's up, you two?

306
00:20:00,700 --> 00:20:04,750
I said I will do it!

307
00:20:37,660 --> 00:20:43,600
You want to know about Rika?<br>- You would know, right?

308
00:20:44,560 --> 00:20:50,470
Yeah. We were close since we were young.

309
00:20:50,470 --> 00:20:53,790
So we three were like siblings.

310
00:20:56,950 --> 00:21:03,710
Does Rika-chan like...

311
00:21:04,450 --> 00:21:06,890
Irie-kun?

312
00:21:07,920 --> 00:21:10,040
Not sure.

313
00:21:11,680 --> 00:21:16,840
But I really like Rika.

314
00:21:16,840 --> 00:21:20,230
When I told her I wanted her to stay here forever and made a scene,

315
00:21:20,230 --> 00:21:24,370
Rika said this to me before;

316
00:21:25,550 --> 00:21:33,280
If she gets married to my brother, then she will become my real sister.

317
00:21:35,530 --> 00:21:36,880
Marriage?

318
00:21:36,880 --> 00:21:42,060
Yeah. I wanted her to be my sister, so...

319
00:21:42,060 --> 00:21:47,200
She asked for me to help to get her married to my brother.

320
00:21:47,200 --> 00:21:51,980
And I promised her...

321
00:21:54,080 --> 00:22:00,760
<i>Kotoko-san, can you give Naoki to me?</i>

322
00:22:02,660 --> 00:22:08,260
<i>She liked Irie-kun since she was young.</i>

323
00:22:08,260 --> 00:22:12,060
<i>Is she serious about it?</i>

324
00:22:24,190 --> 00:22:27,370
Alright. This should do.

325
00:22:27,370 --> 00:22:29,990
<br>

326
00:22:43,220 --> 00:22:46,540
Wow, Kotoko-san. You're early.

327
00:22:49,450 --> 00:22:51,970
You too.

328
00:23:04,130 --> 00:23:09,250
What happened this morning?<br>- You guys are getting along!

329
00:23:09,250 --> 00:23:12,570
That's great!

330
00:23:16,510 --> 00:23:17,350
Good morning.

331
00:23:17,350 --> 00:23:20,250
Irie-kun!<br>- Naoki!

332
00:23:21,880 --> 00:23:24,240
Naoki! This is really good. Eat it.

333
00:23:24,240 --> 00:23:27,000
Irie-kun! I made this myself. Have it.

334
00:23:27,000 --> 00:23:28,820
I am not going to eat all that in the morning.

335
00:23:28,820 --> 00:23:31,960
No way. Please~

336
00:23:31,960 --> 00:23:35,140
So cruel. I made this for you.

337
00:23:36,520 --> 00:23:40,640
Seriously. What should I have first?

338
00:23:40,640 --> 00:23:44,600
Fish, fish. Here you go.

339
00:23:54,050 --> 00:23:55,670
So bad.

340
00:23:55,670 --> 00:23:58,090
Naoki, eat this too.

341
00:24:04,220 --> 00:24:07,160
It's really good.<br> - Really?

342
00:24:07,160 --> 00:24:08,820
<br>

343
00:24:08,820 --> 00:24:12,200
Kotoko-chan. Don't be discouraged!

344
00:24:14,300 --> 00:24:17,900
Why?

345
00:24:19,130 --> 00:24:21,400
Kotoko. Coffee.

346
00:24:21,400 --> 00:24:23,590
I will bring you some.

347
00:24:23,590 --> 00:24:25,400
It's okay.

348
00:24:26,650 --> 00:24:30,430
I like her coffee.

349
00:24:34,350 --> 00:24:38,170
I will bring it to you!

350
00:24:52,890 --> 00:24:54,750
Kotoko-san.

351
00:24:54,750 --> 00:24:59,550
Rika-chan, What's up? What about school?

352
00:25:00,810 --> 00:25:02,870
I...

353
00:25:03,930 --> 00:25:06,290
like Naoki.

354
00:25:08,810 --> 00:25:12,670
I liked him from the start.

355
00:25:12,670 --> 00:25:15,070
Give Naoki back.

356
00:25:16,310 --> 00:25:19,210
What are you saying?

357
00:25:19,210 --> 00:25:22,970
She is serious. She is serious about this.

358
00:25:23,880 --> 00:25:26,720
If I didn't go to America,

359
00:25:26,720 --> 00:25:30,150
Naoki wouldn't have married you.

360
00:25:30,150 --> 00:25:32,260
How would you know that?

361
00:25:32,260 --> 00:25:36,870
Naoki liked me!

362
00:25:37,680 --> 00:25:42,520
We exchanged a kiss, too.

363
00:25:42,520 --> 00:25:46,600
Naoki was holding me tight.

364
00:25:47,660 --> 00:25:50,520
We were in a good mood.

365
00:25:50,520 --> 00:25:55,040
That doesn't matter. I don't know how he felt at that time.

366
00:25:55,040 --> 00:25:58,340
All I could do was watch him.

367
00:25:58,340 --> 00:26:03,760
But Naoki slowly started looking at me,<br>and asked me to marry him.

368
00:26:03,760 --> 00:26:05,580
What do you mean he slowly started looking at you?

369
00:26:17,540 --> 00:26:19,220
What is this?

370
00:26:23,130 --> 00:26:26,520
Give that back! Give that back!

371
00:26:28,900 --> 00:26:31,120
This thing.

372
00:26:33,130 --> 00:26:35,710
Enough!

373
00:26:36,370 --> 00:26:38,170
Rika!

374
00:26:38,170 --> 00:26:40,390
Naoki!

375
00:26:47,860 --> 00:26:49,460
Naoki, it hurts.

376
00:26:49,460 --> 00:26:51,820
Rika, let me see your leg.

377
00:26:53,780 --> 00:26:57,860
Irie-kun...<br>- Enough!

378
00:26:57,860 --> 00:27:00,160
What are you trying to do against a high school student?

379
00:27:00,160 --> 00:27:02,400
You have been acting weird, recently.

380
00:27:02,400 --> 00:27:06,480
Can you stand up? I will take you home.

381
00:27:20,000 --> 00:27:27,020
<i>Subtitles brought to you by The Mischievous Kisser Team @ Viki</i>

382
00:27:41,210 --> 00:27:43,550
That's not it...

383
00:28:00,670 --> 00:28:04,170
Kotoko-chan!

384
00:28:08,090 --> 00:28:10,910
So you're Kotoko-chan's friends.

385
00:28:10,910 --> 00:28:11,690
Yes.

386
00:28:11,690 --> 00:28:13,770
Thank you.

387
00:28:13,770 --> 00:28:15,350
I'm sorry.

388
00:28:15,350 --> 00:28:21,250
She's been crying since the morning, saying she injured a kid named Rika so she can't go home.

389
00:28:21,250 --> 00:28:24,760
We tried to comfort her and went out for a drink but...

390
00:28:28,400 --> 00:28:31,970
Hey, hurry and get off, you're too heavy!

391
00:28:31,970 --> 00:28:35,370
Really... What are you doing?

392
00:28:35,370 --> 00:28:36,730
Irie-kun!

393
00:28:39,110 --> 00:28:41,040
No, it's no problem.

394
00:28:44,440 --> 00:28:46,530
Hey, let's go.

395
00:28:46,530 --> 00:28:52,470
No!

396
00:28:52,470 --> 00:28:57,360
You hate me, don't you?

397
00:29:14,670 --> 00:29:19,710
Let me go!

398
00:29:21,340 --> 00:29:28,140
But you, you like...

399
00:29:28,140 --> 00:29:34,470
Rika-chan... Rika-chan... more than me!

400
00:29:36,090 --> 00:29:37,830
Seriously.

401
00:29:37,830 --> 00:29:40,310
Did kotoko-san fall asleep?

402
00:29:47,450 --> 00:29:50,250
I don't want you to apologize.

403
00:29:51,830 --> 00:29:57,650
Don't taunt her too much.

404
00:29:57,650 --> 00:29:59,490
What? When did I?

405
00:29:59,490 --> 00:30:01,390
Good night.

406
00:30:13,610 --> 00:30:15,110
Hey, it's done!

407
00:30:17,180 --> 00:30:19,930
Eat a lot!

408
00:30:26,030 --> 00:30:30,010
Naoki, this looks good. Try it.

409
00:30:30,010 --> 00:30:32,000
Thanks.

410
00:30:38,280 --> 00:30:39,800
Is it good?

411
00:30:39,800 --> 00:30:42,380
Yeah. It's good.

412
00:30:44,100 --> 00:30:48,100
It's good when I eat here!

413
00:30:48,100 --> 00:30:50,460
Irie-chan. You're eating too much!

414
00:30:50,460 --> 00:30:52,230
It's okay.

415
00:30:52,230 --> 00:30:54,000
Kotoko, bring some beer.

416
00:30:54,000 --> 00:30:55,850
Yes.

417
00:30:56,570 --> 00:30:58,630
I am coming too.

418
00:31:19,190 --> 00:31:21,600
Kotoko-san.

419
00:31:21,600 --> 00:31:25,400
When did you start liking Naoki?

420
00:31:25,400 --> 00:31:31,380
It was first year in high school, so 8 years ago I guess.

421
00:31:33,130 --> 00:31:35,260
That's pretty long, huh?

422
00:31:35,260 --> 00:31:40,100
For me, I liked him when I was two, so it has been 15 years.

423
00:31:46,800 --> 00:31:48,970
Rika-chan.

424
00:31:49,840 --> 00:31:55,190
You told me to give Naoki back last time, right?

425
00:32:02,520 --> 00:32:05,990
Hey, is beer ready ye...

426
00:32:09,210 --> 00:32:11,370
This is dangerous.

427
00:32:13,810 --> 00:32:18,060
I can never do that.

428
00:32:18,060 --> 00:32:20,260
Never.

429
00:32:22,580 --> 00:32:25,250
Okay.

430
00:32:25,250 --> 00:32:28,280
But that's your opinion, right?

431
00:32:28,280 --> 00:32:29,380
What?

432
00:32:29,380 --> 00:32:31,740
I wonder what Naoki thinks.

433
00:32:31,740 --> 00:32:35,680
If Naoki says he likes me,

434
00:32:35,680 --> 00:32:38,650
you can give him to me, right?

435
00:32:38,650 --> 00:32:42,100
Sure. You better ask him, then.

436
00:32:42,100 --> 00:32:44,750
So...

437
00:32:44,750 --> 00:32:46,260
You were listening, Naoki?

438
00:32:46,260 --> 00:32:47,940
What?

439
00:32:49,100 --> 00:32:52,770
Yeah, somewhat.

440
00:32:58,660 --> 00:33:03,030
I have always liked you.

441
00:33:03,030 --> 00:33:05,990
I don't understand how you like her!

442
00:33:05,990 --> 00:33:08,440
I suit you better.

443
00:33:08,440 --> 00:33:11,500
Then why? Why did you marry her?

444
00:33:11,500 --> 00:33:15,090
Do you know how shocked I was when I heard it, while in America?

445
00:33:16,610 --> 00:33:18,540
Then...

446
00:33:21,420 --> 00:33:23,580
Then...

447
00:33:23,580 --> 00:33:26,510
You shouldn't have gone to America!

448
00:33:26,510 --> 00:33:28,880
Why did you stay away from the one you loved for so long then?

449
00:33:28,880 --> 00:33:30,740
I had no choice!

450
00:33:30,740 --> 00:33:32,970
I was only in primary school at that time, my parents...

451
00:33:32,970 --> 00:33:36,030
That doesn't matter!

452
00:33:39,000 --> 00:33:40,870
If I were you,

453
00:33:40,870 --> 00:33:43,930
I wouldn't have gone to America.

454
00:33:43,930 --> 00:33:46,300
For me,

455
00:33:47,800 --> 00:33:52,050
I was okay with just looking at Irie-kun.

456
00:33:53,420 --> 00:33:57,710
You might have loved him longer, but...

457
00:34:07,920 --> 00:34:09,530
Then...

458
00:34:10,770 --> 00:34:14,350
I don't know how Irie-kun feels though.

459
00:34:16,360 --> 00:34:18,770
About that...

460
00:34:19,580 --> 00:34:20,940
Then...

461
00:34:21,830 --> 00:34:24,150
Kotoko-san!

462
00:34:31,900 --> 00:34:36,870
That's why I fell in love with her.

463
00:34:39,150 --> 00:34:42,250
She can't do anything right,

464
00:34:42,250 --> 00:34:45,080
unlike you, but

465
00:34:45,980 --> 00:34:48,790
I like that power of her.

466
00:34:55,350 --> 00:34:57,620
You like her over me?

467
00:34:59,750 --> 00:35:02,550
Yeah. Over you.

468
00:35:06,380 --> 00:35:09,140
Okay.

469
00:35:10,060 --> 00:35:12,890
If it were Kotoko-san,

470
00:35:12,890 --> 00:35:16,150
she wouldn't have gone to America, I guess.

471
00:35:19,000 --> 00:35:21,180
I guess so.

472
00:35:51,660 --> 00:35:54,220
Why are you crying?

473
00:37:01,500 --> 00:37:04,530
I chose you.

474
00:37:04,530 --> 00:37:07,090
So be more confident.

475
00:37:36,460 --> 00:37:37,980
Rika.

476
00:37:57,910 --> 00:37:59,850
You betrayer.

477
00:37:59,850 --> 00:38:01,530
Hm?

478
00:38:01,530 --> 00:38:03,650
You betrayer!

479
00:38:03,650 --> 00:38:06,060
Didn't you promise me?

480
00:38:06,060 --> 00:38:09,170
That you would help me become your sister!

481
00:38:09,170 --> 00:38:13,740
That you would help me get married to Naoki!

482
00:38:13,740 --> 00:38:16,440
This is your fault!

483
00:38:17,930 --> 00:38:20,620
You're such an idiot!

484
00:38:20,620 --> 00:38:23,350
Idiot! Idiot! Idiot!

485
00:38:25,850 --> 00:38:27,610
Sorry.

486
00:38:33,160 --> 00:38:34,920
Sorry.

487
00:38:35,690 --> 00:38:37,520
Rika.

488
00:38:46,020 --> 00:38:48,400
Uncle, Aunt.

489
00:38:57,050 --> 00:38:58,320
Come again.

490
00:38:58,320 --> 00:39:00,020
Of course!

491
00:39:00,020 --> 00:39:04,270
I will come with a boyfriend much cooler than Naoki next time and hang out.

492
00:39:04,270 --> 00:39:08,270
Well, there is no one like that.

493
00:39:11,260 --> 00:39:14,220
My, my. These two girls.

494
00:39:15,900 --> 00:39:17,610
Rika.

495
00:39:18,900 --> 00:39:20,750
See you.<br> - Yeah.

496
00:39:28,100 --> 00:39:29,880
That time?

497
00:39:29,880 --> 00:39:32,660
The day I left for America.

498
00:39:32,660 --> 00:39:36,660
When Naoki and I kissed for the first time.

499
00:39:43,210 --> 00:39:46,270
Then do you want me to show you?

500
00:39:47,230 --> 00:39:49,060
No!

501
00:39:52,790 --> 00:39:56,220
This is what happened 7 years ago.

502
00:39:57,980 --> 00:40:00,060
Sorry.

503
00:40:04,600 --> 00:40:07,420
Dramatize?

504
00:40:11,140 --> 00:40:14,550
But the the kiss doesn't change, right?

505
00:40:17,930 --> 00:40:22,130
So that's how it was...

506
00:40:26,700 --> 00:40:28,620
Here.

507
00:40:32,040 --> 00:40:34,850
This is... for me?

508
00:40:34,850 --> 00:40:36,390
I had no choice.

509
00:40:36,390 --> 00:40:39,670
I will recognize you as Naoki's wife.

510
00:40:42,890 --> 00:40:44,440
See you again.

511
00:40:44,440 --> 00:40:46,150
Be careful.<br>- Seeya.

512
00:40:46,150 --> 00:40:48,110
See you!<br>- Bye!

513
00:40:48,110 --> 00:40:51,290
Rika-chan! See you again!

514
00:40:51,290 --> 00:40:54,440
Bye, bye!

515
00:40:56,340 --> 00:41:00,210
Seriously. She is such a noisy person.

516
00:41:10,340 --> 00:41:13,380
Bye bye, Naoki.

517
00:41:23,850 --> 00:41:29,390
<i>In fact, I'm the one who really understands how Rika-chan feels.</i>

518
00:41:29,390 --> 00:41:36,500
<i>Rika-chan, next time we meet, we must become closer alright?<br>[Sorry about it--Rika]</i>

519
00:43:04,140 --> 00:43:05,640
<i>#10 Naoki-san, please take good care of Kotoko</i>

520
00:43:05,640 --> 00:43:07,470
<i>You like Yuki-kun right?</i>

521
00:43:07,470 --> 00:43:08,960
<i>Those cold eyes!</i>

522
00:43:08,960 --> 00:43:10,980
<i>I'll support you!</i>

523
00:43:10,980 --> 00:43:13,260
<i>Next week is the death anniversary of Kotoko's mother, right?</i>

524
00:43:13,260 --> 00:43:14,980
<i>-This year I'll go with you too.<br>-What?!</i>

525
00:43:14,980 --> 00:43:16,670
<i>It's...it's ok, you don't have to go out of your way to!</i>

526
00:43:16,670 --> 00:43:17,990
<i>Is there something you're hiding from me?</i>

527
00:43:17,990 --> 00:43:20,300
<i>You'll see when we arrive...</i>

528
00:43:22,590 --> 00:43:26,920
<i>Thank you for your warm reception.</i>

529
00:43:28,280 --> 00:43:33,730
<i>But...the character for 'ki' in Naoki has been spelt wrongly...</i>

