﻿1
00:00:01,060 --> 00:00:04,800
You want to break up with me?
Can't you not?

2
00:00:04,800 --> 00:00:09,070
Please, can you break up with me tomorrow instead?

3
00:00:21,390 --> 00:00:26,010
Vivian... Vivian!

4
00:00:31,490 --> 00:00:34,590
You said Vivian broke up with you?

5
00:00:34,590 --> 00:00:38,070
Hey, Mo Cheng Zhen, don't you know

6
00:00:38,070 --> 00:00:40,270
that tomorrow is our birthday?

7
00:00:40,270 --> 00:00:41,850
What?!

8
00:00:42,930 --> 00:00:46,910
You broke up with your girlfriend on the day before our birthday?!

9
00:00:51,510 --> 00:00:55,210
<i>Happy birthday.</i>

10
00:01:02,250 --> 00:01:05,870
♫ <i>What can I do if I lose you now?</i> ♫

11
00:01:05,870 --> 00:01:09,470
♫ <i>I have to protect you every day now</i> ♫

12
00:01:09,470 --> 00:01:12,930
♫ <i>If I lose you, I won't be able to breathe</i> ♫

13
00:01:12,930 --> 00:01:16,810
♫ <i>So let me just gather up my courage for you</i> ♫

14
00:01:16,810 --> 00:01:20,550
♫ <i>What can I do if I lose you now?</i> ♫

15
00:01:20,550 --> 00:01:24,190
♫ <i>Even if the world ends, I have to protect you</i> ♫

16
00:01:24,190 --> 00:01:28,110
♫ <i>There's only you and me left in this world</i> ♫

17
00:01:28,110 --> 00:01:30,930
♫ <i>You and me</i> ♫

18
00:01:45,770 --> 00:01:49,210
♫ <i>Looking at our childhood albums</i> ♫

19
00:01:49,210 --> 00:01:52,930
♫ <i>The days pass by, one by one</i> ♫

20
00:01:52,930 --> 00:01:57,430
♫ <i>I don't think there's any way I can let you go</i> ♫

21
00:01:57,430 --> 00:02:02,970
♫ <i>Whoa... The time has already passed</i> ♫

22
00:02:02,970 --> 00:02:06,670
♫ <i>Our story has become a timeless legend</i> ♫

23
00:02:06,670 --> 00:02:10,230
♫ <i>You said you didn't want to leave me</i> ♫

24
00:02:10,230 --> 00:02:15,550
♫ <i>Then I'll hold on tightly, and I won't let go</i> ♫

25
00:02:15,550 --> 00:02:19,170
♫ <i>What can I do if I lose you now?</i> ♫

26
00:02:19,170 --> 00:02:22,710
♫ <i>I have to protect you every day now</i> ♫

27
00:02:22,710 --> 00:02:26,270
♫ <i>If I lose you, I won't be able to breathe</i> ♫

28
00:02:26,270 --> 00:02:30,010
♫ <i>So let me just gather up my courage for you</i> ♫

29
00:02:30,010 --> 00:02:34,010
♫ <i>What can I do if I lose you now?</i> ♫

30
00:02:34,010 --> 00:02:37,450
♫ <i>Even if the world ends, I have to protect you</i> ♫

31
00:02:37,450 --> 00:02:41,330
♫ <i>There's only you and me left in this world</i> ♫

32
00:02:41,330 --> 00:02:44,490
♫ <i>You and me</i> ♫

33
00:02:44,490 --> 00:02:46,810
<i>Happy birthday.</i>

34
00:02:46,810 --> 00:02:50,790
Love or Spend
Episode 01

35
00:02:51,890 --> 00:02:59,940
<i>Subtitles By The... Team @ Viki</i>

36
00:03:10,540 --> 00:03:12,990
Say that one more time.

37
00:03:14,110 --> 00:03:16,450
I can say it as many times as you want, but that won't change it.

38
00:03:28,610 --> 00:03:32,320
I broke up with Vivian.

39
00:03:40,440 --> 00:03:42,450
What are you playing at?

40
00:03:42,450 --> 00:03:45,100
Give me your phone! I'll plead with her!

41
00:03:45,100 --> 00:03:48,060
Give me your phone!
Get up, where's your phone?

42
00:03:48,060 --> 00:03:51,760
Mo Cheng Zhen, it's not possible.

43
00:03:51,760 --> 00:03:53,640
There's no hope.

44
00:03:54,380 --> 00:03:56,490
She already left with someone else.

45
00:04:03,070 --> 00:04:06,080
Of the nine break-ups in my life, this was the most...

46
00:04:08,110 --> 00:04:11,060
The most tragic.

47
00:04:19,920 --> 00:04:23,900
<i>She and I have always been inseparable friends.</i>

48
00:04:23,900 --> 00:04:27,690
<i>Chen Zhen and I met when we were kids.</i>

49
00:04:27,690 --> 00:04:30,370
<i>We ate together, slept together,</i>

50
00:04:30,370 --> 00:04:35,760
<i>bathed together.
We even went to school together, hand in hand.</i>

51
00:04:35,760 --> 00:04:38,700
<i>We were in the same kindergarten and elementary school classes.</i>

52
00:04:38,700 --> 00:04:42,450
<i>We went to the same high school, and both got into the same college.</i>

53
00:04:42,450 --> 00:04:47,360
<i>Just as everyone saw us, we were childhood friends.</i>

54
00:04:48,310 --> 00:04:51,250
So, we...

55
00:04:51,250 --> 00:04:53,710
No way!

56
00:04:53,710 --> 00:04:56,580
Of course I know we can't!
What I was going to say was...

57
00:04:56,580 --> 00:05:00,940
What should we do right now?!

58
00:05:04,540 --> 00:05:07,710
Who else knows about this?

59
00:05:09,550 --> 00:05:13,440
While I was despairing, Cheng Han called me.

60
00:05:13,440 --> 00:05:16,740
I told him a little bit about it.

61
00:05:17,500 --> 00:05:18,810
You...

62
00:05:21,530 --> 00:05:25,040
He's your brother, my good friend.

63
00:05:25,790 --> 00:05:28,010
There's probably nothing to be worried about.

64
00:05:31,130 --> 00:05:33,820
I'm worried about Cheng Han's mom, and his mom's best friend!

65
00:05:33,820 --> 00:05:36,050
Your mom!

66
00:05:37,740 --> 00:05:39,640
You can't go to my house!

67
00:05:39,640 --> 00:05:41,390
- What are you doing?
- What are you doing?

68
00:05:41,390 --> 00:05:43,530
Get up! Phone...!

69
00:05:43,530 --> 00:05:45,710
It's not there.
Stop grabbing at random places!

70
00:05:46,800 --> 00:05:50,280
<i>Oppa, pick up the phone!</i>

71
00:05:50,280 --> 00:05:53,180
<i>Oppa, pick up the phone!</i>

72
00:05:53,180 --> 00:05:57,130
Oppa still wants to sleep.

73
00:05:57,130 --> 00:06:00,220
<i>Oppa, pick up the phone!</i>

74
00:06:00,860 --> 00:06:04,540
Is it Zheng Xi again?

75
00:06:04,540 --> 00:06:09,490
He's so annoying.
If you don't have a girlfriend, go find another one.

76
00:06:09,490 --> 00:06:12,780
Are you saying that Zheng Xi and his girlfriend broke up?

77
00:06:12,780 --> 00:06:15,970
Yeah, Zheng Xi and his...

78
00:06:15,970 --> 00:06:19,470
Wait, why does this voice sound like...

79
00:06:20,540 --> 00:06:21,610
Mom!

80
00:06:21,610 --> 00:06:25,160
That's right, it's me, your mom, Ms. Huang Ji Xia.

81
00:06:25,160 --> 00:06:26,710
No, Mom, listen to me.

82
00:06:26,710 --> 00:06:28,870
I was just sleeping, and I had a dream.

83
00:06:28,870 --> 00:06:31,020
So I was just talking in my sleep.

84
00:06:31,020 --> 00:06:33,230
Got it?

85
00:06:48,100 --> 00:06:50,690
Mom, it hurts.

86
00:06:52,040 --> 00:06:55,850
This kid can tolerate quite a bit.

87
00:06:55,850 --> 00:06:59,290
I already told you that I'm very tight-lipped.

88
00:06:59,290 --> 00:07:01,880
I won't tell anyone that it was you who told me.

89
00:07:01,880 --> 00:07:06,330
That's not it, Mrs. Pei.
I don't know, I really don't know anything.

90
00:07:06,330 --> 00:07:12,460
Actually, there was a certain spot that was really effective with my husband.

91
00:07:12,460 --> 00:07:13,880
Oh, really?

92
00:07:16,380 --> 00:07:18,990
Then, Yong Jin...

93
00:07:18,990 --> 00:07:23,870
He's fine. At first though, there was a little...
But everything was fine after a week.

94
00:07:23,870 --> 00:07:26,570
What? Then...

95
00:07:26,570 --> 00:07:28,010
Mom.

96
00:07:28,950 --> 00:07:31,350
No! Mom!

97
00:07:33,010 --> 00:07:34,930
I'm the only son in the Mo family.

98
00:07:34,930 --> 00:07:37,130
Have you really thought this over clearly?

99
00:07:43,330 --> 00:07:47,210
Mr. Pei Zheng Xi was the unfortunate witness to Vivian kissing someone else, and then he was dumped!

100
00:07:48,850 --> 00:07:52,050
That's wonderful!

101
00:08:00,050 --> 00:08:01,430
<i>[Mo Cheng Han]</i>

102
00:08:03,790 --> 00:08:06,830
Give it to me!

103
00:08:06,830 --> 00:08:08,410
Get away.

104
00:08:09,910 --> 00:08:11,450
Cheng Han, what are you doing?

105
00:08:13,410 --> 00:08:15,150
But you're just now calling back.
Are you awake?

106
00:08:15,150 --> 00:08:18,910
Zheng Xi, listen to me.
While I was sleeping, I unintentionally–

107
00:08:18,910 --> 00:08:21,850
Listen to me first.
You have to keep it a secret, don't–

108
00:08:21,850 --> 00:08:24,490
Get to the point!

109
00:08:24,490 --> 00:08:26,990
Mo Cheng Han, I'm warning you.

110
00:08:26,990 --> 00:08:30,290
If you dare tell anyone about Pei Zheng Xi and Vivian breaking up,

111
00:08:30,290 --> 00:08:31,810
I'll...!

112
00:08:32,350 --> 00:08:36,630
I'll tell all the girls about the time you peed your pants in middle school.

113
00:08:36,630 --> 00:08:42,010
Sis, can't you just let me off a bit? Just tell the story of the time I fell into a trench when I was in elementary school, okay?

114
00:08:42,810 --> 00:08:44,550
Wait, Cheng Han.

115
00:08:44,550 --> 00:08:47,890
By that, you can't mean...

116
00:08:56,650 --> 00:08:58,670
Wait, did you hear that?

117
00:08:58,670 --> 00:09:00,090
Yeah.

118
00:09:00,090 --> 00:09:02,670
Cheers!

119
00:09:03,750 --> 00:09:07,870
It's the Queen Mother and the Royal Consort Cai Ling.

120
00:09:08,890 --> 00:09:11,590
Cheng Han oppa, hurry and come join us

121
00:09:11,590 --> 00:09:13,270
as we open a bottle of champagne to celebrate!

122
00:09:13,270 --> 00:09:15,130
Come and celebrate!

123
00:09:15,130 --> 00:09:18,070
Let's drink!

124
00:09:18,070 --> 00:09:23,030
Mo Cheng Han, I'm severing my ties with you!

125
00:09:24,050 --> 00:09:25,950
So, what should we do?

126
00:09:25,950 --> 00:09:27,650
What should we do?

127
00:09:27,650 --> 00:09:30,610
This is all your fault!

128
00:09:30,610 --> 00:09:32,870
If you hadn't promised our moms 5 years ago,

129
00:09:32,870 --> 00:09:36,230
our brains wouldn't be hurting so much now!

130
00:09:36,230 --> 00:09:40,250
Well, who was the one who gave the opportunity to suggest such an idea?

131
00:09:40,250 --> 00:09:43,470
You could have not agreed to it.
Why did you agree?!

132
00:09:43,470 --> 00:09:45,430
Sure, just blame me. It's not your fault at all.

133
00:09:45,430 --> 00:09:47,850
Of course!

134
00:09:47,850 --> 00:09:50,010
- Weird...
- Why did you agree to it?

135
00:09:50,010 --> 00:09:52,430
You figure it out.

136
00:09:54,870 --> 00:09:56,190
Daughter.

137
00:09:57,130 --> 00:10:02,230
Cheng Zhen, whether you get promoted or not

138
00:10:02,230 --> 00:10:07,510
depends a bit on luck sometimes.

139
00:10:07,510 --> 00:10:10,110
But I was depending on luck.

140
00:10:10,110 --> 00:10:14,490
I worked really, really hard at the office.

141
00:10:14,490 --> 00:10:17,890
But every time someone gets promoted, it's a man.

142
00:10:17,890 --> 00:10:20,270
How am I supposed to accept that?

143
00:10:21,770 --> 00:10:24,930
Why don't you think of it in a different way?

144
00:10:24,930 --> 00:10:29,590
A woman needs to get married and have kids.

145
00:10:29,590 --> 00:10:33,310
You should put in all your effort to run the family.

146
00:10:33,310 --> 00:10:36,610
That's more practical, right?

147
00:10:37,450 --> 00:10:39,590
That's not what Chengzhen thinks.

148
00:10:39,590 --> 00:10:43,150
She's always wanted to be recognized for her ability at work.

149
00:10:43,990 --> 00:10:46,410
Here, drink some water.

150
00:10:46,410 --> 00:10:50,630
After you drink, you should try to get in a good mood.
It's our birthday today, after all.

151
00:10:56,590 --> 00:11:00,990
Eat some bread. It's your favorite - wheat bread.
You haven't eaten all day.

152
00:11:04,270 --> 00:11:05,950
Add some butter.

153
00:11:10,650 --> 00:11:12,590
Now, have a bit of soup.

154
00:11:21,550 --> 00:11:25,350
Alright, I've decided.

155
00:11:25,350 --> 00:11:27,870
You two, get married.

156
00:11:29,450 --> 00:11:31,690
Where did that come from?

157
00:11:31,690 --> 00:11:34,770
It was already decided when they were 5 years old.

158
00:11:34,770 --> 00:11:37,650
Everyone knows about it.

159
00:11:37,650 --> 00:11:41,010
Zheng Xi, you said you wanted to protect our Cheng Zhen.

160
00:11:41,010 --> 00:11:44,110
And you said you'd definitely marry her.

161
00:11:44,110 --> 00:11:47,410
Mom, when did that happen?
Don't make stuff up.

162
00:11:47,410 --> 00:11:51,290
What do you mean, it didn't happen?
You were 5 years old.

163
00:11:51,290 --> 00:11:53,750
Don't either of you have any recollection of it at all?

164
00:11:53,750 --> 00:11:57,190
Auntie, wait a minute.
How can you hold us to the promises of a 5 year-old?

165
00:11:57,190 --> 00:12:00,090
Plus, I don't remember it at all.

166
00:12:01,250 --> 00:12:03,330
I knew you would say that!

167
00:12:03,330 --> 00:12:06,890
That's why we recorded the evidence.

168
00:12:06,890 --> 00:12:09,610
Really? Where is it?

169
00:12:09,610 --> 00:12:11,970
Pei Yong Jin, where is it?

170
00:12:14,470 --> 00:12:17,070
- Do we have that?
- Yes, you put it inside the drawer.

171
00:12:17,070 --> 00:12:19,870
- No, I open that drawer every day.
- Yes, we do.

172
00:12:19,870 --> 00:12:22,450
Alright, that's enough. Mom...

173
00:12:22,450 --> 00:12:24,510
It sounds like you're pushing us to get married.

174
00:12:24,510 --> 00:12:26,910
You want me to marry Cheng Zhen, but then what happens to my girlfriend?

175
00:12:26,910 --> 00:12:31,730
Also, have you asked Cheng Zhen?
Isn't that being too disrespectful of her?

176
00:12:31,730 --> 00:12:34,010
That's right, I'm not respecting you two.

177
00:12:34,010 --> 00:12:36,330
And that's because you don't even respect yourselves.

178
00:12:36,330 --> 00:12:39,170
You can't even keep your promises!

179
00:12:39,170 --> 00:12:42,610
How can I respect a son like you?

180
00:12:42,610 --> 00:12:45,750
Ji Xia, I'm so sorry.

181
00:12:45,750 --> 00:12:48,490
How could I have raised such a son, who doesn't keep his promises?

182
00:12:48,490 --> 00:12:50,590
I've really let you down.

183
00:12:50,590 --> 00:12:54,370
Let me kneel down to you.

184
00:12:54,370 --> 00:12:56,790
Mom!

185
00:12:56,790 --> 00:12:59,450
Mom! Mrs. Pei!

186
00:13:00,810 --> 00:13:03,310
Zheng Xi, look at this.

187
00:13:03,310 --> 00:13:07,170
How did you let things turn into this?
Hurry and say something to make your mom happy.

188
00:13:07,170 --> 00:13:12,170
Alright... Alright, Mom, just tell me exactly what it is you want me to do.

189
00:13:12,170 --> 00:13:14,150
I'll do it.

190
00:13:15,450 --> 00:13:18,370
No one forced to you say that.

191
00:13:18,370 --> 00:13:19,990
No one forced me.

192
00:13:20,670 --> 00:13:25,570
Okay, since you have a girlfriend right now...

193
00:13:25,570 --> 00:13:30,790
Then, if you two are both single on your birthday,

194
00:13:30,790 --> 00:13:33,830
you two will sincerely date,

195
00:13:33,830 --> 00:13:35,530
and then get married.

196
00:13:35,530 --> 00:13:37,490
What?

197
00:13:38,270 --> 00:13:44,690
<i>If we both just happen to be single on our birthday,</i>

198
00:13:44,690 --> 00:13:46,870
<i>we will sincerely date,</i>

199
00:13:46,870 --> 00:13:49,490
<i>and then get married.</i>

200
00:13:59,310 --> 00:14:04,210
<i>Are you recording? Is it clear?</i>

201
00:14:04,210 --> 00:14:08,090
Good thing you came up with this strange idea.

202
00:14:08,770 --> 00:14:12,510
Otherwise, who would know how long we'd have to wait.

203
00:14:12,510 --> 00:14:14,670
Cheers.

204
00:14:14,670 --> 00:14:19,390
But they really are made for each other.

205
00:14:19,390 --> 00:14:23,190
One day, they will understand that.

206
00:14:23,190 --> 00:14:25,870
There really isn't anyone else

207
00:14:25,870 --> 00:14:28,930
who suits them better than each other.

208
00:14:28,930 --> 00:14:31,730
That's right! Ji Xia,

209
00:14:31,730 --> 00:14:35,550
we're going to really become

210
00:14:35,550 --> 00:14:39,450
one family!

211
00:14:39,450 --> 00:14:42,110
Cheers!

212
00:15:02,110 --> 00:15:03,310
- That–
- That–

213
00:15:04,370 --> 00:15:05,830
- Was–
- Was–

214
00:15:16,850 --> 00:15:22,730
It's 8 PM right now. There's 24 hours left until our birthday party tomorrow.

215
00:15:22,730 --> 00:15:25,190
Let's face this together.
We'll each find someone.

216
00:15:25,190 --> 00:15:27,890
Let's see if we can get through tomorrow.

217
00:15:29,270 --> 00:15:32,370
Fine, let's find someone.
Who's afraid?

218
00:16:19,280 --> 00:16:20,420
Pei Yongjin,

219
00:16:20,420 --> 00:16:24,750
I told Jixia in high school that we'd become family.

220
00:16:24,750 --> 00:16:29,000
I never thought it would really become a reality.

221
00:16:30,500 --> 00:16:34,540
Hubby...

222
00:16:34,540 --> 00:16:37,100
Hubby...

223
00:16:37,100 --> 00:16:40,300
Hubby, do you remember

224
00:16:40,300 --> 00:16:43,730
when Chengzhen was 11 years old,

225
00:16:43,730 --> 00:16:49,210
her father left Chenghan in the park.

226
00:16:49,210 --> 00:16:51,730
And Chenghan was scared, and cried loudly.

227
00:16:51,730 --> 00:16:55,220
You were the one who carried him back, right?

228
00:16:55,220 --> 00:16:57,250
Right?

229
00:17:01,040 --> 00:17:04,280
Here. Endure it.

230
00:17:09,240 --> 00:17:10,730
I know

231
00:17:10,730 --> 00:17:14,620
that Jixia always tells me she's okay,

232
00:17:14,620 --> 00:17:19,570
but then every time, she hides and cries.

233
00:17:19,570 --> 00:17:24,740
How can I leave her to bear such a big burden by herself?

234
00:17:24,740 --> 00:17:26,150
Right?

235
00:17:26,150 --> 00:17:28,510
Cailing,

236
00:17:28,510 --> 00:17:31,830
from your personality,

237
00:17:31,830 --> 00:17:35,390
you wouldn't interfere with what kind of girl our son dates.

238
00:17:36,220 --> 00:17:39,670
So why is it that this time you put in such effort to match the two of them up?

239
00:17:39,670 --> 00:17:42,250
There was a bigger reason, right?

240
00:17:43,210 --> 00:17:45,320
For Jixia?

241
00:17:49,870 --> 00:17:53,500
You know, Jixia and I,

242
00:17:53,500 --> 00:17:59,730
we're just like sisters, but when it comes down to it, we're just friends.

243
00:17:59,730 --> 00:18:01,740
If something happened,

244
00:18:01,740 --> 00:18:06,550
how much can friends do?

245
00:18:06,550 --> 00:18:09,580
If we became family,

246
00:18:09,580 --> 00:18:12,630
Zhengxi could take care of Chengzhen for us,

247
00:18:12,630 --> 00:18:15,770
take care of Jixia, take care of all of them.

248
00:18:15,770 --> 00:18:18,200
Am I right?

249
00:18:18,200 --> 00:18:25,840
And haven't you always treated Chengzhen and Chenghan as your own kids?

250
00:18:25,840 --> 00:18:29,400
That's because you care for them, and I care for you. Of course I should do that much.

251
00:18:29,400 --> 00:18:33,320
Hubby, you're so wonderful.

252
00:18:33,320 --> 00:18:36,810
I love you the most! Kiss!

253
00:18:36,810 --> 00:18:40,460
- Alright...
- I'm so happy today!

254
00:18:41,620 --> 00:18:46,330
- Okay, okay...
- I love you the most!

255
00:18:47,900 --> 00:18:51,190
Be careful. Why did you drink so much wine?

256
00:18:52,100 --> 00:18:53,820
Be careful!

257
00:18:56,350 --> 00:18:59,900
Chengzhen, you're back?

258
00:19:01,390 --> 00:19:05,590
Did you know that Zhengxi broke up with his girlfriend?

259
00:19:06,860 --> 00:19:08,940
Okay.

260
00:19:11,490 --> 00:19:15,500
Chengzhen, let me tell you.

261
00:19:15,500 --> 00:19:19,060
I've always worried a lot about you.

262
00:19:19,850 --> 00:19:25,920
Everyone says that a daughter's future is just like her mother's past.

263
00:19:28,010 --> 00:19:29,390
Mom.

264
00:19:29,390 --> 00:19:34,130
I was worried that you would follow in my footsteps.

265
00:19:38,030 --> 00:19:40,400
Chengzhen,

266
00:19:40,400 --> 00:19:45,640
anything can happen in this world.

267
00:19:45,640 --> 00:19:48,050
I just hope

268
00:19:49,800 --> 00:19:55,390
that, at least, there can be someone to walk this path with you.

269
00:19:55,390 --> 00:19:59,790
To cry with you, to laugh with you.

270
00:20:04,440 --> 00:20:07,000
Having you is enough.

271
00:20:07,000 --> 00:20:09,070
Didn't we already agree?

272
00:20:09,070 --> 00:20:13,970
I would stay with you until we're old.

273
00:20:16,650 --> 00:20:18,810
No matter how you put it,

274
00:20:20,720 --> 00:20:23,550
my farewell will come before yours.

275
00:20:23,550 --> 00:20:26,940
I'll reach the end before you.

276
00:20:26,940 --> 00:20:30,450
Mom, don't talk about that stuff.

277
00:20:37,070 --> 00:20:39,270
Ms. Huang!

278
00:22:15,540 --> 00:22:18,650
Hello, is this Assistant Manager Luo?

279
00:22:18,650 --> 00:22:22,980
I'm Mo Chengzhen from King of Scissor.
Sorry to disturb you so suddenly.

280
00:22:22,980 --> 00:22:26,530
<i>It's very noisy here, I'll call you back in a moment.</i>

281
00:22:32,040 --> 00:22:36,170
How can my luck be so bad?
Well, so what if it doesn't start out great.

282
00:22:55,680 --> 00:22:57,070
Hey.

283
00:22:58,020 --> 00:23:01,650
You still have the yearbooks from elementary school to college?

284
00:23:01,650 --> 00:23:03,470
Why?

285
00:23:03,470 --> 00:23:06,810
I'm worried you won't have enough people from your business cards.

286
00:23:09,060 --> 00:23:11,020
Thanks for your concern.

287
00:23:11,020 --> 00:23:16,030
But please, before you worry about someone else, take care of yourself first.

288
00:23:17,700 --> 00:23:23,680
Between the two of us, the one who doesn't need to keep worrying about this situation is me.

289
00:23:24,480 --> 00:23:27,750
Pei Zhengxi, you've already taken care of it?

290
00:23:28,850 --> 00:23:33,690
I just called Peipei, and she said she'd also really like to meet up.

291
00:23:33,690 --> 00:23:35,660
Really?

292
00:23:35,660 --> 00:23:38,070
I just called Assistant Manager Luo,

293
00:23:38,070 --> 00:23:41,850
from 8mm Beauty Salon.

294
00:23:41,850 --> 00:23:45,630
But he seemed to be busy, and said he'd call back.

295
00:23:46,700 --> 00:23:48,360
Let's have a toast.

296
00:23:48,360 --> 00:23:52,440
No matter what, I still have Peipei to help me out.

297
00:23:58,080 --> 00:24:00,370
Wait, let me check my phone.

298
00:24:01,930 --> 00:24:03,070
What?

299
00:24:03,070 --> 00:24:07,360
He's decided to return tomorrow, and I have to pick him up at the airport?

300
00:24:16,460 --> 00:24:19,340
I didn't realize Assistant Manager Luo was so considerate.

301
00:24:20,930 --> 00:24:23,880
Good luck to you. Bye.

302
00:24:52,030 --> 00:24:55,770
Youxin, what are you doing here?

303
00:25:15,000 --> 00:25:17,120
I know you're the most considerate.

304
00:25:17,120 --> 00:25:20,090
Every year, you play <i>Happy Birthday</i> for me.

305
00:25:21,400 --> 00:25:24,390
But it's already late.
Why haven't you gone to sleep?

306
00:25:24,390 --> 00:25:28,270
If you want to play the song, it's not too late to play it tomorrow.

307
00:25:28,270 --> 00:25:32,250
But Mom said I have to play the <i>Bridal Chorus</i> tomorrow.

308
00:25:33,710 --> 00:25:36,330
Brother, goodnight!

309
00:25:36,330 --> 00:25:37,780
Okay!

310
00:25:37,780 --> 00:25:40,780
Goodnight.
Happy birthday.

311
00:26:22,470 --> 00:26:26,220
So? Where are you and Assistant Manager Luo going for your date?

312
00:26:26,220 --> 00:26:27,770
What?

313
00:26:28,870 --> 00:26:32,320
Didn't Assistant Manager Luo text you to pick him up at the airport?

314
00:26:32,320 --> 00:26:35,360
You're not planning to grab some afternoon tea or something as well?

315
00:26:35,360 --> 00:26:36,860
What?

316
00:26:39,800 --> 00:26:43,230
- That text...?
- Yeah.

317
00:26:44,030 --> 00:26:48,050
You... Are you set?

318
00:26:49,740 --> 00:26:52,770
Peipei invited me out for lunch.

319
00:26:52,770 --> 00:26:54,180
Is that so?

320
00:26:54,180 --> 00:26:58,560
Then, good luck on your date.

321
00:26:58,560 --> 00:27:01,170
What's that face supposed to mean?

322
00:27:01,170 --> 00:27:06,070
Nothing. You also know that Peipei's laugh sounds just like a rooster.

323
00:27:08,920 --> 00:27:10,000
Hey...

324
00:27:10,000 --> 00:27:11,800
Hey, no one's perfect.

325
00:27:11,800 --> 00:27:14,790
That's right, Peipei's laugh is indeed very unusual.

326
00:27:14,790 --> 00:27:18,540
But she's very pretty.
If you can, why don't you find a man then?

327
00:27:30,330 --> 00:27:34,290
That client was really crafty. He kept asking questions that I had no way to answer.

328
00:27:34,290 --> 00:27:35,260
Really?

329
00:27:35,260 --> 00:27:38,200
Maybe I haven't studied enough.

330
00:27:39,480 --> 00:27:43,190
There's a very suitable candidate in front of my eyes. Do you want to think it over?

331
00:27:43,190 --> 00:27:44,880
Who?

332
00:27:47,170 --> 00:27:49,110
He Liyang?

333
00:27:50,010 --> 00:27:52,020
You've got to be kidding.

334
00:27:52,020 --> 00:27:55,540
- He's my–
- I know that he's your assistant.

335
00:27:57,320 --> 00:28:00,900
Look at his deep eyes,

336
00:28:00,900 --> 00:28:04,950
his wide shoulders, his sturdy chest.

337
00:28:04,950 --> 00:28:08,510
And there's his bright and sunny smile.

338
00:28:08,510 --> 00:28:11,640
Don't try to match up people so randomly.

339
00:28:11,640 --> 00:28:12,800
Good morning, managers.

340
00:28:12,800 --> 00:28:14,630
- Good morning.
- Good morning.

341
00:28:18,910 --> 00:28:21,000
Don't mess around.

342
00:28:22,340 --> 00:28:23,440
Please, you first.

343
00:28:23,440 --> 00:28:25,600
Thank you, Manager Pei.

344
00:28:27,880 --> 00:28:29,770
Get in.

345
00:28:33,710 --> 00:28:36,110
Be careful. Are you okay?

346
00:28:36,110 --> 00:28:38,810
I'm fine, sorry.

347
00:28:38,810 --> 00:28:40,860
What's the matter?

348
00:29:03,870 --> 00:29:06,850
- I'm sorry.
- It's okay.

349
00:29:17,990 --> 00:29:20,240
I'm sorry.

350
00:29:20,240 --> 00:29:22,360
It's okay.

351
00:29:49,250 --> 00:29:51,980
Is there something bothering you?

352
00:29:51,980 --> 00:29:54,760
There's nothing.

353
00:29:58,220 --> 00:29:59,340
Yes.

354
00:29:59,340 --> 00:30:03,340
I just called each of them, and they said they could come.

355
00:30:42,830 --> 00:30:46,790
<i>There's a very suitable candidate in front of my eyes. Do you want to think it over?</i>

356
00:30:54,280 --> 00:30:57,970
Who else is there?

357
00:31:01,420 --> 00:31:02,810
Miss,

358
00:31:02,810 --> 00:31:07,650
your shoes are very pretty.
Are they new? Size 25, right?

359
00:31:08,880 --> 00:31:13,420
Excuse me, didn't you ask me the same thing two weeks ago?

360
00:31:13,420 --> 00:31:18,410
And these are the same pair of shoes.

361
00:31:18,410 --> 00:31:21,160
But they've already got wrinkles.

362
00:31:21,160 --> 00:31:22,940
Really?

363
00:31:23,830 --> 00:31:26,340
Just like you.

364
00:31:26,340 --> 00:31:29,870
You were very pretty before, but now you're getting wrinkles.

365
00:31:30,710 --> 00:31:34,030
What's on your mind?
Can I help you?

366
00:31:34,730 --> 00:31:36,870
No...

367
00:31:36,870 --> 00:31:39,150
You're still saying no?

368
00:31:39,150 --> 00:31:45,100
You were just going through your phone, with your brows furrowed, saying you were looking for something.

369
00:31:45,910 --> 00:31:47,990
I was looking for...

370
00:31:50,430 --> 00:31:53,430
I need a man.

371
00:31:53,430 --> 00:31:56,060
Tonight.

372
00:32:16,240 --> 00:32:18,240
I already set up a date with Peipei.

373
00:32:19,120 --> 00:32:21,130
Peipei?

374
00:32:21,130 --> 00:32:23,220
Peipei's...

375
00:32:23,220 --> 00:32:24,750
Oh!

376
00:32:24,750 --> 00:32:28,280
You mean the one who's laugh sounds like this?

377
00:32:28,280 --> 00:32:29,780
Her?

378
00:32:29,780 --> 00:32:32,900
You two were almost together, right?

379
00:32:32,900 --> 00:32:35,520
Hey, Zhengxi...

380
00:32:54,300 --> 00:32:56,700
Hi, Zhengxi.

381
00:33:09,040 --> 00:33:11,990
Zhengxi, it's been a long time.

382
00:33:11,990 --> 00:33:14,350
How is it that you haven't changed at all?

383
00:33:16,060 --> 00:33:19,750
Your smile...also hasn't changed at all.

384
00:33:23,060 --> 00:33:26,160
Last night, when you said you wanted to meet up...

385
00:33:26,160 --> 00:33:28,340
I was so happy!

386
00:33:28,340 --> 00:33:29,720
Really?

387
00:33:29,720 --> 00:33:34,230
I was thinking that we hadn't seen each other in a long time, so it wouldn't be bad to meet up.

388
00:33:36,460 --> 00:33:39,920
But, what I wanted to say was

389
00:33:39,920 --> 00:33:43,460
I don't want to be just friends.

390
00:33:43,460 --> 00:33:47,110
Peipei, don't act like this.
We're in a public place.

391
00:33:48,490 --> 00:33:50,940
Really, don't.

392
00:33:54,060 --> 00:33:57,100
Oops, I'm sorry.

393
00:34:13,830 --> 00:34:19,750
8mm's Assistant Manager Luo just got married yesterday?

394
00:34:21,110 --> 00:34:25,620
I'm doomed...

395
00:34:25,620 --> 00:34:31,250
Assistant Manager Luo? I remember I told you he wasn't bad, and that you should grab him.

396
00:34:31,250 --> 00:34:34,070
But you kept saying you didn't need that.

397
00:34:34,070 --> 00:34:36,120
I never thought that

398
00:34:36,120 --> 00:34:40,770
there would be a day where Zhengxi and I were both single.

399
00:34:40,770 --> 00:34:43,930
Since it's all fake anyway, and your search is so tough,

400
00:34:43,930 --> 00:34:46,460
why not just pair up with Zhengxi?

401
00:34:46,460 --> 00:34:50,430
Go home, and put on a nice show for your moms, and then it'll be done with.

402
00:34:50,430 --> 00:34:53,090
Me and Pei Zhengxi? Impossible!

403
00:34:53,090 --> 00:34:55,170
We're good friends.

404
00:34:55,170 --> 00:34:58,550
Pairing us up together

405
00:34:58,550 --> 00:35:04,270
makes me feel very uncomfortable.
It's not possible.

406
00:35:04,270 --> 00:35:06,770
It matters how you feel?

407
00:35:07,590 --> 00:35:12,730
Hey, are you seriously looking for a man this time?

408
00:35:12,730 --> 00:35:13,980
Of course.

409
00:35:13,980 --> 00:35:18,100
How seriously? This much, this much, this much?

410
00:35:18,100 --> 00:35:22,030
Just as seriously as when I'm making a presentation to clients.

411
00:35:30,400 --> 00:35:33,090
<i>No, no, I didn't lie.</i>

412
00:35:33,090 --> 00:35:35,690
<i>I really want to find someone to help me through tonight.</i>

413
00:35:35,690 --> 00:35:40,670
<i>If it goes smoothly, I won't keep rejecting love.</i>

414
00:35:42,710 --> 00:35:45,360
So, this time,

415
00:35:45,360 --> 00:35:49,480
you're looking for a man you can spend every day with, for the rest of your life?

416
00:35:49,480 --> 00:35:53,140
And not just someone you play with and use, and then get rid of?

417
00:35:55,180 --> 00:36:00,060
Of course I can't just play with someone.

418
00:36:02,710 --> 00:36:08,180
- Hey, there's a decent candidate that might be able to help you.
- Really?!

419
00:36:10,280 --> 00:36:12,450
His name is Victor.

420
00:36:12,450 --> 00:36:17,990
He's flying into Taiwan from the US tonight.
I'll help you set something up.

421
00:36:17,990 --> 00:36:19,280
You can meet at work.

422
00:36:19,280 --> 00:36:21,050
Really?!

423
00:36:21,050 --> 00:36:22,960
Really.

424
00:36:25,740 --> 00:36:27,480
President, thank you!

425
00:36:27,480 --> 00:36:31,730
You always treat me the best!

426
00:36:31,730 --> 00:36:33,360
No need to act cute.

427
00:36:33,360 --> 00:36:36,110
My pride is helping a beautiful girl in need.

428
00:36:37,420 --> 00:36:39,850
I'll take care of it, don't worry.

429
00:36:39,850 --> 00:36:42,080
I'll help you right away.

430
00:36:42,080 --> 00:36:44,150
Thank you, President!

431
00:36:44,150 --> 00:36:47,020
Ah Hao, send a car

432
00:36:47,020 --> 00:36:49,730
to the airport to pick someone up.

433
00:36:49,730 --> 00:36:52,320
It's all taken care of then, right?

434
00:36:58,700 --> 00:37:02,400
I almost forgot about the airport!

435
00:37:34,220 --> 00:37:38,390
Strange... Where's Chengzhen?

436
00:37:38,390 --> 00:37:41,520
Why don't I see her?

437
00:37:41,520 --> 00:37:44,040
Did I go the wrong way?

438
00:37:57,970 --> 00:38:00,980
Hey! Mister, in the front!

439
00:38:00,980 --> 00:38:02,270
Hey!

440
00:38:02,270 --> 00:38:06,070
Mister, in the yellow sweater that looks like a banana!

441
00:38:22,840 --> 00:38:24,240
May I help you?

442
00:38:24,240 --> 00:38:26,160
Help me?

443
00:38:26,160 --> 00:38:28,110
As if I would really say "help me."

444
00:38:28,110 --> 00:38:33,350
Just now, your luggage just hit my foot.
Shouldn't you say somthing?

445
00:38:33,350 --> 00:38:36,010
Oh, Miss, are you alright?

446
00:38:36,010 --> 00:38:39,490
Your luggage is so big and heavy.
Do you think I'd be alright?

447
00:38:39,490 --> 00:38:42,120
I'm sorry, I didn't notice.

448
00:38:42,120 --> 00:38:45,180
Your cart hit my foot and you didn't notice?

449
00:38:45,180 --> 00:38:48,960
I'm really very sorry.
If you'd like, I can compensate you for it.

450
00:38:48,960 --> 00:38:50,580
Compensate?

451
00:38:50,580 --> 00:38:54,970
Okay, ask my foot what kind of compensation it needs.

452
00:39:31,540 --> 00:39:35,450
Okay, ask my foot what kind of compensation it needs.

453
00:39:42,040 --> 00:39:43,820
Hey! What are you doing?

454
00:39:43,820 --> 00:39:48,170
You asked me to ask your foot. I should check to see the condition, let me see.

455
00:39:48,170 --> 00:39:50,680
Hey! Are you trying to hit on me?

456
00:39:50,680 --> 00:39:53,240
That's not it at all.

457
00:39:53,240 --> 00:39:55,090
You're so annoying.

458
00:40:08,240 --> 00:40:10,370
Pervert.

459
00:40:17,350 --> 00:40:22,500
<i>Youting, I've got an important meeting right now, so I could only send a car to pick you up.</i>

460
00:40:22,500 --> 00:40:27,460
When you leave, take note.
The license plate is 3883.

461
00:40:27,460 --> 00:40:31,260
Called a car for me? I guess that's fine.

462
00:40:47,490 --> 00:40:51,400
3-8-8... 3?

463
00:40:51,400 --> 00:40:53,850
Wow, it's that one.

464
00:40:53,850 --> 00:40:56,050
Chengzhen is being too kind.

465
00:40:56,050 --> 00:40:59,560
She got such an expensive car to pick me up.

466
00:41:05,770 --> 00:41:08,790
Hey, where's the driver?

467
00:41:50,880 --> 00:41:54,200
Hey!

468
00:42:07,470 --> 00:42:09,560
Hello, Stanley?

469
00:42:09,560 --> 00:42:13,750
Something's come up over here.

470
00:42:19,830 --> 00:42:21,070
Hey, Chenghan.

471
00:42:26,580 --> 00:42:31,120
She just called and said she had to go to Nanbu.

472
00:42:31,120 --> 00:42:35,070
She even took the day off from work tomorrow.

473
00:42:39,990 --> 00:42:41,560
Come in.

474
00:42:43,490 --> 00:42:47,760
President, where's Victor? Is he still not here?

475
00:42:47,760 --> 00:42:49,750
Something's come up, so he can't make it.

476
00:42:49,750 --> 00:42:51,420
Then...!

477
00:42:51,420 --> 00:42:55,180
Then what am I supposed to do about tonight?

478
00:43:10,440 --> 00:43:12,200
- You–
- You–

479
00:44:17,990 --> 00:44:22,030
There's only an hour left.

480
00:44:22,030 --> 00:44:25,730
Where am I supposed to find a boyfriend?

481
00:44:28,230 --> 00:44:32,470
Manager, I wanted to go over the price for Yixuan Salon.

482
00:44:32,470 --> 00:44:36,790
Take a look at this price.

483
00:44:36,790 --> 00:44:40,840
<i>There's a very suitable candidate in front of my eyes. Do you want to think it over?</i>

484
00:44:46,720 --> 00:44:48,240
Manager, you...

485
00:44:48,240 --> 00:44:51,870
<i>Look at his deep eyes,</i>

486
00:44:51,870 --> 00:44:56,960
<i>his wide shoulders, his sturdy chest.</i>

487
00:44:56,960 --> 00:45:00,810
<i>And there's his bright and sunny smile.</i>

488
00:45:14,120 --> 00:45:15,920
I'm sorry.

489
00:45:27,000 --> 00:45:30,760
<i>My mom went to work.
Chenghan said he'd leave before noon.</i>

490
00:45:30,760 --> 00:45:35,530
<i>If you leave the room after noon, it should be safe.</i>

491
00:45:41,380 --> 00:45:46,660
<i>Being close to your memories
is a kind of loneliness.</i>

492
00:45:49,510 --> 00:45:56,410
<i>Don’t Say Goodbye - by Popu Lady</i>

493
00:45:57,980 --> 00:45:59,810
<i>I don't want to be alone.</i>

494
00:46:01,100 --> 00:46:07,720
♫ <i>Such a familiar laugh, and yet it feels so cold</i> ♫

495
00:46:07,720 --> 00:46:16,050
♫ <i>Quietly watching you, when suddenly,
my heart starts to beat anxiously</i> ♫

496
00:46:16,050 --> 00:46:23,010
♫ <i>I'd rather be worse off,
than have to guess at what you're hiding</i> ♫

497
00:46:23,010 --> 00:46:30,500
♫ <i>Or maybe I just need to close my eyes,
and wait for your embrace</i> ♫

498
00:46:30,500 --> 00:46:32,860
♫ <i>Once upon a time, your breathing</i> ♫

499
00:46:32,860 --> 00:46:37,950
♫ <i>was beside my ear,
wrapped around the future that was set</i> ♫

500
00:46:37,950 --> 00:46:45,290
♫ <i>But will your next words be 'I love you more'
or will you have no response at all?</i> ♫

501
00:46:45,290 --> 00:46:49,030
♫ <i>I don't want to cry and have to guess</i> ♫

502
00:46:49,030 --> 00:46:52,700
♫ <i>I don't want an answer based off a hunch</i> ♫

503
00:46:52,700 --> 00:46:56,800
♫ <i>You borrowed all of my tomorrows</i> ♫

504
00:46:56,800 --> 00:47:00,090
♫ <i>I don't want you to give them back</i> ♫

505
00:47:00,090 --> 00:47:07,490
♫ <i>If this is a mistake, and I'm just scared of losing you</i> ♫

506
00:47:07,490 --> 00:47:13,300
♫ <i>Please hold me tightly, kiss me, and tell me what to do</i> ♫

507
00:47:13,300 --> 00:47:18,640
♫ <i>so that we won't have to say goodbye</i> ♫

508
00:47:20,640 --> 00:47:28,020
♫ <i>I don't want to say goodbye</i> ♫

