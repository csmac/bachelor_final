1
00:00:03,720 --> 00:00:06,039
Die, die, die!

2
00:00:06,040 --> 00:00:07,639
Can we just watch Flog It?

3
00:00:07,640 --> 00:00:09,639
No, I'm teabagging this
camper I just pwned.

4
00:00:09,640 --> 00:00:12,559
But they're flogging. Tom, I've just
killed a man on the battlefield

5
00:00:12,560 --> 00:00:15,639
and now, to humiliate him, I'm going
to put my testicles in his mouth.

6
00:00:15,640 --> 00:00:18,159
Itchy, are you seeing this?
Who's Itchy?

7
00:00:18,160 --> 00:00:21,159
Some 11-year-old from Guyana.
No, don't retreat. Treat, treat!

8
00:00:21,160 --> 00:00:23,799
So, you're molesting a virtual
corpse? Itchy, you do it.

9
00:00:23,800 --> 00:00:25,439
And now telling a child to.

10
00:00:25,440 --> 00:00:27,439
Medic, medic!

11
00:00:27,440 --> 00:00:30,159
Everything all right?
Can I get a can of Lilt, please?

12
00:00:30,160 --> 00:00:35,199
Oh, hello, JuicySniper9,
prepare to be died. You're sick.

13
00:00:35,200 --> 00:00:37,879
Oh, no, stop killing
children on the internet!

14
00:00:37,880 --> 00:00:39,479
Grow up!

15
00:00:39,480 --> 00:00:41,719
Argh!

16
00:00:41,720 --> 00:00:43,319
No.

17
00:00:43,320 --> 00:00:45,799
Don't you dare teabag me,
you little bitch!

18
00:00:45,800 --> 00:00:47,639
No!

19
00:00:47,640 --> 00:00:48,839
Thanks.

20
00:00:48,840 --> 00:00:50,680
No!

21
00:00:56,880 --> 00:00:59,000
This is Flat News.

22
00:01:02,080 --> 00:01:04,359
Momentous developments in
lift talk this morning,

23
00:01:04,360 --> 00:01:06,959
as I kind of asked gorgeous
next-door neighbour Sophie out,

24
00:01:06,960 --> 00:01:08,119
kind of, sort of, in a way,

25
00:01:08,120 --> 00:01:10,039
we'll see what happens,
fingers crossed.

26
00:01:10,040 --> 00:01:12,479
Busy this evening?
Old lift, isn't it? Oh, this?

27
00:01:12,480 --> 00:01:14,680
Yeah, it's a rickety old twat.

28
00:01:15,680 --> 00:01:19,079
What to do say about this evening?
Chicken. Chicken?

29
00:01:19,080 --> 00:01:21,679
Yeah,
would you like to go for chicken?

30
00:01:21,680 --> 00:01:23,399
What, just you and me?

31
00:01:23,400 --> 00:01:26,359
Yeah, well, there'll presumably
be other people in the restaurant,

32
00:01:26,360 --> 00:01:30,719
but... OK, that, Tom,
you seem really nice

33
00:01:30,720 --> 00:01:34,439
and I love our weird conversations
in the lift and everything.

34
00:01:34,440 --> 00:01:37,319
I've been hurt by guys
in the past...

35
00:01:37,320 --> 00:01:40,319
and you didn't come to my party,
and you berated those homeless guys,

36
00:01:40,320 --> 00:01:42,639
and you burnt down my flat.

37
00:01:42,640 --> 00:01:45,639
I just, I don't know,
I need someone sweet and caring.

38
00:01:45,640 --> 00:01:49,000
I don't know, I just really don't
want to get hurt again.

39
00:01:51,200 --> 00:01:53,479
So, that's a maybe?

40
00:01:53,480 --> 00:01:56,439
Sophie possibly going on
a date with me, huge news.

41
00:01:56,440 --> 00:01:59,320
With high hopes, we now
go to Naz with the weather.

42
00:02:00,480 --> 00:02:03,079
Naz? The weather?

43
00:02:03,080 --> 00:02:05,799
It's bleak.

44
00:02:05,800 --> 00:02:09,160
Oh, come on. What happened
that can be that bad?

45
00:02:10,320 --> 00:02:12,319
Naz?

46
00:02:12,320 --> 00:02:15,119
For the last two years,
Naz has been fighting

47
00:02:15,120 --> 00:02:18,159
on the front-line of one
of the world's bloodiest

48
00:02:18,160 --> 00:02:19,720
first-person shooters.

49
00:02:22,280 --> 00:02:25,000
This is his story.

50
00:02:27,280 --> 00:02:29,239
War isn't easy.

51
00:02:29,240 --> 00:02:31,279
You get headaches, thumb cramp,

52
00:02:31,280 --> 00:02:33,959
one tiny mistake
in how you're sitting,

53
00:02:33,960 --> 00:02:35,959
you're looking at a dead leg.

54
00:02:35,960 --> 00:02:40,639
Naz was out on a routine patrol.
Operation, just shoot everyone.

55
00:02:40,640 --> 00:02:43,319
Little did he know the peril
he faced.

56
00:02:43,320 --> 00:02:47,359
This guy, JuicySniper9,
he just got under my skin.

57
00:02:47,360 --> 00:02:49,999
You're dead.

58
00:02:50,000 --> 00:02:51,960
Obviously, I retaliated.

59
00:02:53,560 --> 00:02:56,279
He was just too clever.

60
00:02:56,280 --> 00:02:59,479
JuicySniper9 shot and killed Naz.

61
00:02:59,480 --> 00:03:02,640
As soon as he killed me,
I knew I was dead.

62
00:03:03,720 --> 00:03:05,759
Nothing prepares you for death.

63
00:03:05,760 --> 00:03:08,239
Apart from the other 100
or so times I died that day.

64
00:03:08,240 --> 00:03:10,439
No, don't knock me over. Oh!

65
00:03:10,440 --> 00:03:12,399
It's all right, guys, I've got it.

66
00:03:12,400 --> 00:03:14,919
Oh! Take that, you... Oh!

67
00:03:14,920 --> 00:03:18,399
A dead body lying in a war zone
is usually respected,

68
00:03:18,400 --> 00:03:21,480
but JuicySniper9 showed
no such decorum.

69
00:03:23,160 --> 00:03:26,279
He teabagged me.

70
00:03:26,280 --> 00:03:29,280
'Don't you dare teabag me! No!'

71
00:03:31,360 --> 00:03:33,320
He put his balls in my mouth.

72
00:03:36,560 --> 00:03:38,800
You just don't forget that.

73
00:03:40,120 --> 00:03:43,800
The only way to move forward
is to get my revenge.

74
00:03:44,840 --> 00:03:46,920
Revenge is sweet...

75
00:03:48,280 --> 00:03:49,999
...but my balls are not.

76
00:03:50,000 --> 00:03:51,759
Made you a cuppa, mate. Argh!

77
00:03:51,760 --> 00:03:53,800
Oh, Goddamn it, Mikey!

78
00:03:58,880 --> 00:04:01,959
Have you heard the news?
Can you believe those Russians?

79
00:04:01,960 --> 00:04:05,959
It's not yours, Russia.
It's just not yours.

80
00:04:05,960 --> 00:04:07,519
Was there a reason you came, or...?

81
00:04:07,520 --> 00:04:09,959
Yes, could you look
after my nephew today, please?

82
00:04:09,960 --> 00:04:12,519
Ah, no. I don't really have
the qualifications.

83
00:04:12,520 --> 00:04:16,039
You sound just like my doctor.
Please, I've got a busy day.

84
00:04:16,040 --> 00:04:19,599
Hi. Yo. Already here, are you?
OK. Forcing my hand.

85
00:04:19,600 --> 00:04:22,479
Please? I've got such a busy day.
No.

86
00:04:22,480 --> 00:04:25,359
This one usually takes them,
but she's busy, too.

87
00:04:25,360 --> 00:04:26,959
Hello, Sophie.

88
00:04:26,960 --> 00:04:30,639
How, Tom. Oh, are you looking
after him today?

89
00:04:30,640 --> 00:04:33,560
You guys look really
cute together.

90
00:04:34,640 --> 00:04:37,679
I'll take him. Oh, good. Thank you.

91
00:04:37,680 --> 00:04:41,079
Kieran is such a good boy.
Deep down.

92
00:04:41,080 --> 00:04:43,440
Deep, deep down.
Thanks, bye.

93
00:04:46,360 --> 00:04:51,319
OK, JuicySniper9,
how can I find you?

94
00:04:51,320 --> 00:04:53,359
This is my office.

95
00:04:53,360 --> 00:04:56,559
Sh-h-h! I'm planning my revenge.

96
00:04:56,560 --> 00:05:00,280
So many clues,
but how to connect them?

97
00:05:09,680 --> 00:05:12,239
Connections, connections,
connections.

98
00:05:12,240 --> 00:05:15,319
JuicySniper number nine,
First attacks by the fuel depot,

99
00:05:15,320 --> 00:05:17,079
then by the ammo crate. Connection?

100
00:05:17,080 --> 00:05:19,079
Both storage containers
full of something.

101
00:05:19,080 --> 00:05:21,279
What else is full of something?
Balls. Argh, no!

102
00:05:21,280 --> 00:05:24,759
Concentrate. JuicySniper, likes to
snipe, also enjoys sugary beverages,

103
00:05:24,760 --> 00:05:27,999
such as orange juice. What shape
are oranges? Balls. Oh, no! Think!

104
00:05:28,000 --> 00:05:32,759
JuicySniper number nine, of course,
9+9- 10... And what've you got?

105
00:05:32,760 --> 00:05:34,919
A huge pair of balls. Argh!

106
00:05:34,920 --> 00:05:38,239
God, damn it!
All my deductions are balls.

107
00:05:38,240 --> 00:05:42,239
I don't see... You do see, but
you don't observe, my dear Mikey.

108
00:05:42,240 --> 00:05:44,119
It's el-mel-entary. What?

109
00:05:44,120 --> 00:05:46,399
It's emel... Hemel Hempsteady,
it's Humpty Dumpty.

110
00:05:46,400 --> 00:05:49,879
Oh! It's elementary.
It means very complicated.

111
00:05:49,880 --> 00:05:51,359
Can you, please, get out of...

112
00:05:51,360 --> 00:05:53,319
Stop talking, I'm going to
my mind phallus.

113
00:05:53,320 --> 00:05:55,759
You mean palace.
Exactly, my phallus palace.

114
00:05:55,760 --> 00:05:58,000
Now, get me a Tizer.

115
00:06:31,040 --> 00:06:32,999
I'm hungry.

116
00:06:33,000 --> 00:06:35,439
Fetch me a bap! I fancy a BLT.

117
00:06:35,440 --> 00:06:38,759
Bacon, lettuce and Tizer. OK!

118
00:06:38,760 --> 00:06:41,600
This reveals absolutely nothing.

119
00:06:44,720 --> 00:06:46,999
Revenge is hard.

120
00:06:47,000 --> 00:06:49,839
Why are we playing here?
There's a park the other side.

121
00:06:49,840 --> 00:06:52,039
There is,
but this is a great spot for catch.

122
00:06:52,040 --> 00:06:54,719
Because it's next to Sophie's flat.
Basically, yeah.

123
00:06:54,720 --> 00:06:56,479
I want to play PlayStation.

124
00:06:56,480 --> 00:06:59,079
Well, right now, we're playing
a real time strategy game

125
00:06:59,080 --> 00:07:01,279
called
Make The Pretty Girl Like Tom.

126
00:07:01,280 --> 00:07:03,839
You're so old. Ready?
I don't really do catch,

127
00:07:03,840 --> 00:07:06,439
I'm more into football
and being liked.

128
00:07:06,440 --> 00:07:08,759
That's the problem with kids
these days,

129
00:07:08,760 --> 00:07:12,999
they just don't do what
they're told. Come on, let's play.

130
00:07:13,000 --> 00:07:15,120
Hee-hee!

131
00:07:18,360 --> 00:07:22,520
This is The Square, where simple
tasks become epic challenges.

132
00:07:23,560 --> 00:07:25,319
Catch.

133
00:07:25,320 --> 00:07:31,079
Tom, you must take the ball and play
catch with this horrible brat,

134
00:07:31,080 --> 00:07:34,519
to convince Sophie you're a sweet,
caring guy.

135
00:07:34,520 --> 00:07:38,359
Can your steady hand take you to
a romantic chicken dinner?

136
00:07:38,360 --> 00:07:41,480
Are we playing or what? Yeah!

137
00:07:42,800 --> 00:07:46,359
Yeah.

138
00:07:46,360 --> 00:07:49,879
Ha-ha-ha!
Hee-hee-ha!

139
00:07:49,880 --> 00:07:54,199
Look, if we go back inside, I'll
help you out with Sophie. Trust me.

140
00:07:54,200 --> 00:07:59,159
I'm experienced. What, in crayons?
Skirt, tail, dame-bombs.

141
00:07:59,160 --> 00:08:01,759
I've had more girls
than you've had haircuts -

142
00:08:01,760 --> 00:08:03,839
but not as many
as I've had haircuts,

143
00:08:03,840 --> 00:08:05,559
cos this boy got to look good.

144
00:08:05,560 --> 00:08:08,239
And my mum's a hairdresser.
Look, just do my plan, eh?

145
00:08:08,240 --> 00:08:10,599
For a little bit, mm?
High-five?

146
00:08:10,600 --> 00:08:14,519
I don't really do high-fives,
I'm more into handshakes

147
00:08:14,520 --> 00:08:16,479
and having friends.

148
00:08:16,480 --> 00:08:17,920
Mini high-five?

149
00:08:20,960 --> 00:08:23,159
See, we're having fun.

150
00:08:23,160 --> 00:08:24,760
Ha-ha-ha!

151
00:08:27,000 --> 00:08:30,279
Sorry about the teabagging,
bruv. Yeah.

152
00:08:30,280 --> 00:08:31,879
I thought this might cheer you up.

153
00:08:31,880 --> 00:08:34,959
Spicy beef, pepperoni, meatballs.

154
00:08:34,960 --> 00:08:39,759
Argh, balls! Sorry, sorry,
pork spheres. Ah!

155
00:08:39,760 --> 00:08:42,599
I'm sorry, man. I'm just still
a bit sore about it, OK?

156
00:08:42,600 --> 00:08:44,880
But I plan to get my revenge.

157
00:08:45,880 --> 00:08:49,119
The man who seeks revenge must
dig two graves.

158
00:08:49,120 --> 00:08:52,919
What, like teabag him twice? There
can be no satisfaction in revenge.

159
00:08:52,920 --> 00:08:57,679
It's like nachos, yeah? Eat one,
not enough. Eat two, not enough.

160
00:08:57,680 --> 00:09:02,159
Whole bowl, still hungry.
And the Bar Mitzvah's out of crisps.

161
00:09:02,160 --> 00:09:04,119
Yeah, but it's consuming me

162
00:09:04,120 --> 00:09:06,999
and the only way to end it is
if he consumes my balls.

163
00:09:07,000 --> 00:09:10,239
Listen to yourself, you're obsessed.
Yeah, you're damn right, I am!

164
00:09:10,240 --> 00:09:12,039
And I'm not going to stop, Carl!

165
00:09:12,040 --> 00:09:14,039
Not until I get my two
pounds of fleshy balls

166
00:09:14,040 --> 00:09:15,399
bouncing around his face

167
00:09:15,400 --> 00:09:17,720
and drowning him in a rain
of 1,000 teabags.

168
00:09:19,120 --> 00:09:22,639
Sh-h-h! You're running through
a golden field, free,

169
00:09:22,640 --> 00:09:25,599
where you roam amongst the corn,
frolicking with the fawns

170
00:09:25,600 --> 00:09:27,239
and other animal people.

171
00:09:27,240 --> 00:09:28,399
Oh, little animal people.

172
00:09:28,400 --> 00:09:31,319
You arrive at a lake, where you
meet the old boatman. Hello.

173
00:09:31,320 --> 00:09:34,639
He takes you to the cave
of forgiveness. He's a nice man.

174
00:09:34,640 --> 00:09:38,079
Underneath the dripping limestones,
you release your anger -

175
00:09:38,080 --> 00:09:44,039
and into the calming waters
sink your two furious pebbles.

176
00:09:44,040 --> 00:09:46,999
Actually, they're more like rocks.
Sleep!

177
00:09:47,000 --> 00:09:49,399
Your anger has now left you.

178
00:09:49,400 --> 00:09:53,519
And in this place,
only the desire to spread peace

179
00:09:53,520 --> 00:09:55,039
and love to those around you.

180
00:09:55,040 --> 00:09:59,439
Peace and love to those around me,
that's my mission now.

181
00:09:59,440 --> 00:10:02,039
Also, I've got some doughballs.
Urgh!

182
00:10:02,040 --> 00:10:04,639
A lot of food is balls, isn't it?

183
00:10:04,640 --> 00:10:06,799
Oh!

184
00:10:06,800 --> 00:10:09,159
Top tip, if you're trying to
impress a girl,

185
00:10:09,160 --> 00:10:11,239
you might not want to make it
quite so obvious

186
00:10:11,240 --> 00:10:12,519
that you can't play sport.

187
00:10:12,520 --> 00:10:14,959
I played cricket
for Berkshire Under 13s.

188
00:10:14,960 --> 00:10:16,759
People used to call me
Rocket Rosenthal.

189
00:10:16,760 --> 00:10:19,520
Is that because of your
massive nose? You shut up.

190
00:10:23,000 --> 00:10:25,159
Maybe I should come a little closer.

191
00:10:25,160 --> 00:10:27,479
The game has simplified -

192
00:10:27,480 --> 00:10:31,199
you are now one step closer
to the little shit.

193
00:10:31,200 --> 00:10:34,240
OK, Rocket, give me
one with a little heat.

194
00:10:36,200 --> 00:10:38,640
Get ready for take off.

195
00:10:48,480 --> 00:10:50,479
Less hope remaining.

196
00:10:50,480 --> 00:10:54,079
Argh! Argh, you threw it too hard
on purpose! Why didn't you catch it?

197
00:10:54,080 --> 00:10:56,759
Why did you do that?

198
00:10:56,760 --> 00:10:58,639
Is he all right? Shall I come down?

199
00:10:58,640 --> 00:11:01,480
No, no! He's fine.

200
00:11:03,080 --> 00:11:06,679
Ha-ha-ha. Yeah, he's fine.

201
00:11:06,680 --> 00:11:08,760
Thank God you can't throw properly.

202
00:11:12,720 --> 00:11:14,800
Still want to play outside?

203
00:11:29,200 --> 00:11:31,799
What's going on? Thomas!

204
00:11:31,800 --> 00:11:34,999
OK, well, I hated that.
What is this hate?

205
00:11:35,000 --> 00:11:37,799
My mission is to spread peace
and love to all around.

206
00:11:37,800 --> 00:11:40,399
For, as the tranquil Buddha says...

207
00:11:40,400 --> 00:11:42,479
Ahhh... don't be a dick.

208
00:11:42,480 --> 00:11:44,119
Being pretty weird, mate.

209
00:11:44,120 --> 00:11:47,879
Please, call me
Priest or Mother Naz. No.

210
00:11:47,880 --> 00:11:49,359
Oh, look, a precious child.

211
00:11:49,360 --> 00:11:51,639
Don't hug the kid.
Don't call me a kid.

212
00:11:51,640 --> 00:11:53,640
The force is strong in this one.
What the...?

213
00:11:55,080 --> 00:11:57,799
Can we get the real Naz back? At
this rate, I think I prefer Mikey.

214
00:11:57,800 --> 00:12:01,039
Please, Tom, we are all
equal in the eyes of the universe.

215
00:12:01,040 --> 00:12:03,519
Although this priest could do
with some holy water.

216
00:12:03,520 --> 00:12:05,360
Mike, get me a Tango! With Ice!

217
00:12:07,840 --> 00:12:09,399
OK, can you just look after the kid?

218
00:12:09,400 --> 00:12:12,039
I've got to pick an outfit out,
in case Sophie says yes.

219
00:12:12,040 --> 00:12:15,319
You, sit down
and think about what you've done.

220
00:12:15,320 --> 00:12:16,960
Hm!

221
00:12:18,480 --> 00:12:21,039
Hey there, little fella,
having a pleasant day?

222
00:12:21,040 --> 00:12:25,359
Yeah, I think
Tom's my new best mate. He's great.

223
00:12:25,360 --> 00:12:27,119
Isn't he? Isn't he!

224
00:12:27,120 --> 00:12:31,119
I'm trying to make him look good in
front of Sophie. Spreading the love.

225
00:12:31,120 --> 00:12:32,320
Maybe I could help.

226
00:12:34,120 --> 00:12:39,239
You can. Speak, little prophet.
Listen, I know chicks.

227
00:12:39,240 --> 00:12:42,839
If I see a girl I like, trust me,
by the end of the night,

228
00:12:42,840 --> 00:12:46,479
I'll be holding her hand.
This impresses Mother Naz.

229
00:12:46,480 --> 00:12:50,639
Tom's too nice. Dames appreciate it
when a guy's rough around the edges.

230
00:12:50,640 --> 00:12:53,280
You know, treat them mean,
keep them keen. Hm.

231
00:12:54,760 --> 00:12:57,159
So, what can I do?

232
00:12:57,160 --> 00:12:59,359
Tom's a dick. What?

233
00:12:59,360 --> 00:13:03,119
Yeah, he is not a nice guy.
The worst.

234
00:13:03,120 --> 00:13:07,559
One of the worst guys. I thought
you guys were best friends?

235
00:13:07,560 --> 00:13:09,039
No, that's a complete lie.

236
00:13:09,040 --> 00:13:13,159
I only live with him
because he controls my money. OK.

237
00:13:13,160 --> 00:13:18,599
He forces me to work for him,
like a sort of slave.

238
00:13:18,600 --> 00:13:24,679
Erm... And he called you fat.
Like a fat potato.

239
00:13:24,680 --> 00:13:30,319
What? Not a hot potato, he was very
clear about that. Like a fat potato.

240
00:13:30,320 --> 00:13:34,199
Oh. OK, well, thanks for telling me.

241
00:13:34,200 --> 00:13:35,279
He...

242
00:13:35,280 --> 00:13:38,720
He also called you a smelly,
ugly, fuck pig.

243
00:13:40,280 --> 00:13:42,600
I just thought you should...

244
00:13:46,240 --> 00:13:49,239
Are these a bit much?
Glad tidings, brother.

245
00:13:49,240 --> 00:13:53,679
Where have you been? I've been
to see Sophie, your one true love.

246
00:13:53,680 --> 00:13:58,639
OK. And let's just say, all is well.
She thinks you're a complete dick.

247
00:13:58,640 --> 00:14:00,239
What?

248
00:14:00,240 --> 00:14:02,039
I said you'd called her
a host of names,

249
00:14:02,040 --> 00:14:05,679
ranging from bulbous vegetables
to pongy swine.

250
00:14:05,680 --> 00:14:08,879
Are you serious? She even cried.

251
00:14:08,880 --> 00:14:13,039
Well, go back there and take it
all back. I'm not your slave, Tom.

252
00:14:13,040 --> 00:14:16,679
Although that's not what Sophie
thinks. Why would you do that?

253
00:14:16,680 --> 00:14:18,639
Why would you do that?!

254
00:14:18,640 --> 00:14:20,919
Treat them mean, keep them keen.

255
00:14:20,920 --> 00:14:23,919
Honestly,
he's like Tinder in child form.

256
00:14:23,920 --> 00:14:25,959
You are going back to Auntie Aoife!
I'm sorry.

257
00:14:25,960 --> 00:14:27,199
You're in so much trouble.

258
00:14:27,200 --> 00:14:29,599
You are genuinely a little devil
child, with a black soul

259
00:14:29,600 --> 00:14:32,079
and a frustratingly handsome face.
Weird thing to say.

260
00:14:32,080 --> 00:14:34,959
I don't care, I resent it and
I resent you. You sound like my dad.

261
00:14:34,960 --> 00:14:38,559
Oh, do I? Oh, well... Oh.

262
00:14:38,560 --> 00:14:43,479
Does he say that? Does he say that
to you? He used to, before he left.

263
00:14:43,480 --> 00:14:45,559
Oh.

264
00:14:45,560 --> 00:14:48,839
Well. He walked out on us
when I was small.

265
00:14:48,840 --> 00:14:52,439
I haven't seen him in eight years.

266
00:14:52,440 --> 00:14:56,719
I guess I just feel the need to hurt
those that are put in charge of me.

267
00:14:56,720 --> 00:14:58,440
Before they can hurt me.

268
00:15:01,480 --> 00:15:03,080
Yeah.

269
00:15:04,280 --> 00:15:09,199
I was lying about all those girls,
I've never even held a hand.

270
00:15:09,200 --> 00:15:13,120
I just like the attention, you know?
I mean, what does that say about me?

271
00:15:15,360 --> 00:15:18,839
I'm the same. I was just using you
to get Sophie's attention.

272
00:15:18,840 --> 00:15:21,559
I mean, what does that say about me?

273
00:15:21,560 --> 00:15:24,279
I'm sorry. No, I'm sorry.

274
00:15:24,280 --> 00:15:28,119
And, while we're being honest,
my cricket nickname wasn't Rocket.

275
00:15:28,120 --> 00:15:32,680
It was Tom Can't-Throw-senthal.
You really can't throw. I know.

276
00:15:34,360 --> 00:15:38,639
Don't worry, you're a nice guy.
I'm sure Sophie will see that.

277
00:15:38,640 --> 00:15:43,159
Oh, cheers, man. Trust me,
you'll be holding hands in no time.

278
00:15:43,160 --> 00:15:46,080
Mini high-five? Mega high-five.

279
00:15:49,280 --> 00:15:53,319
Oh, hello.

280
00:15:53,320 --> 00:15:56,560
Everything all right?
Yeah, we're good. Busy day, is it?

281
00:16:06,040 --> 00:16:09,879
What? I heard Naz came round
earlier. Yeah, he did.

282
00:16:09,880 --> 00:16:11,999
Anything he said was not true.

283
00:16:12,000 --> 00:16:13,999
So, you don't
think I'm a fat potato?

284
00:16:14,000 --> 00:16:16,279
No, no, never.
Or a smelly, ugly...

285
00:16:16,280 --> 00:16:20,679
No, none of those things.
Then why would he say it?

286
00:16:20,680 --> 00:16:23,679
Listen, Sophie, I've liked you

287
00:16:23,680 --> 00:16:26,439
ever since you've moved in,
and Naz knows that.

288
00:16:26,440 --> 00:16:29,279
What happened earlier was just
his way of trying to convince you

289
00:16:29,280 --> 00:16:33,679
that I'm a nice guy.
He said you keep him as a slave.

290
00:16:33,680 --> 00:16:35,359
Oh, it baffles me, too.

291
00:16:35,360 --> 00:16:37,679
It's just taken me
too long to say

292
00:16:37,680 --> 00:16:41,959
that I really enjoy
living across from you, and...

293
00:16:41,960 --> 00:16:44,639
I know that chicken this
evening is a maybe,

294
00:16:44,640 --> 00:16:47,200
but I really, sincerely
hope that you say yes.

295
00:16:51,200 --> 00:16:53,640
Sophie? Erm...

296
00:16:55,000 --> 00:16:57,399
Hare, Hare Christmas.

297
00:16:57,400 --> 00:16:59,800
Go away, will you? No smelly-smelly?
No smelly-smelly.

298
00:17:01,600 --> 00:17:03,639
Is that a slave outfit?

299
00:17:03,640 --> 00:17:06,159
It's not fair for you
to keep him in robes.

300
00:17:06,160 --> 00:17:08,919
Just no idea why he's wearing that.

301
00:17:08,920 --> 00:17:11,759
I'll knock on your door
in about an hour?

302
00:17:11,760 --> 00:17:13,519
Chicken?

303
00:17:13,520 --> 00:17:14,840
Chicken.

304
00:17:19,920 --> 00:17:22,919
How do I look? You look good, man.

305
00:17:22,920 --> 00:17:24,799
Oh, thanks, Champ.

306
00:17:24,800 --> 00:17:27,679
Chicken!

307
00:17:27,680 --> 00:17:31,399
Wow, you're a gifted gamesman.
Here you are, little buddy.

308
00:17:31,400 --> 00:17:34,479
Do you play? No, no, I gave up war.

309
00:17:34,480 --> 00:17:37,879
No, I live peacefully
and treat people only with respect.

310
00:17:37,880 --> 00:17:40,639
Mikey, that beef isn't going
to Wellington itself.

311
00:17:40,640 --> 00:17:42,999
Personally, I could never
give up sniping.

312
00:17:43,000 --> 00:17:45,479
It's one of my greatest
pleasures in life.

313
00:17:45,480 --> 00:17:47,799
Apart from juice.

314
00:17:47,800 --> 00:17:49,439
Obviously.

315
00:17:49,440 --> 00:17:51,520
And humiliating my victims.

316
00:17:52,720 --> 00:17:55,679
Connections, connections,
connections.

317
00:17:55,680 --> 00:17:58,239
Personally, I could never
give up sniping.

318
00:17:58,240 --> 00:18:00,359
Sniper.

319
00:18:00,360 --> 00:18:01,759
He likes to snipe.

320
00:18:01,760 --> 00:18:05,399
It's one of my greatest pleasures
in life. Apart from juice.

321
00:18:05,400 --> 00:18:07,999
Juicy.
Also enjoys sugary beverages.

322
00:18:08,000 --> 00:18:09,759
And humiliating my victims.

323
00:18:09,760 --> 00:18:13,559
Balls. Oh, no, concentrate! Sniper
number nine. He's just too clever.

324
00:18:13,560 --> 00:18:16,720
Don't you dare teabag me,
little bitch!

325
00:18:18,520 --> 00:18:21,279
I know your filthy secret.

326
00:18:21,280 --> 00:18:23,799
You're The Stig.
What is wrong with you?

327
00:18:23,800 --> 00:18:27,159
No, I'm JuicySniper9.
What? I did it.

328
00:18:27,160 --> 00:18:29,400
It was me, I'm the teabagger.

329
00:18:30,960 --> 00:18:34,359
I'm going to beat you to death
with the tranquil Buddha.

330
00:18:34,360 --> 00:18:36,719
No, no, no. He's the JuicySniper!

331
00:18:36,720 --> 00:18:40,360
Mother Naz go crazy! Argh! Sh-h-h!

332
00:18:42,200 --> 00:18:45,839
Don't be a dick. Let it go, Naz.

333
00:18:45,840 --> 00:18:48,919
Sh-h-h, Naz. I know he's an
annoying kid. Oh, God, I know.

334
00:18:48,920 --> 00:18:50,919
But he's like this for a reason, OK?

335
00:18:50,920 --> 00:18:54,239
His dad abandoned him and he has to
hang out with Aoife all the time.

336
00:18:54,240 --> 00:18:57,360
He's a good kid. OK?
Sometimes good people do bad things.

337
00:18:59,440 --> 00:19:00,599
That's Sophie.

338
00:19:00,600 --> 00:19:03,439
Remember when you went round to her
house and called her a smelly,

339
00:19:03,440 --> 00:19:05,959
ugly pig? No, it was a smelly,
ugly... It doesn't matter.

340
00:19:05,960 --> 00:19:09,199
That was a bad thing to do, right?
But you're not a bad guy, are you?

341
00:19:09,200 --> 00:19:12,559
You're great guy. In fact,
I think you're the best guy.

342
00:19:12,560 --> 00:19:14,440
I love you, man.

343
00:19:15,480 --> 00:19:18,399
I love you, too, man.

344
00:19:18,400 --> 00:19:21,280
Do you think you've got
it in you to forgive Kieran?

345
00:19:22,520 --> 00:19:24,239
Friends?

346
00:19:24,240 --> 00:19:25,959
Friends.

347
00:19:25,960 --> 00:19:28,680
Super-mega high-five?
Super-mega high-five.

348
00:19:29,880 --> 00:19:31,879
Oh!

349
00:19:31,880 --> 00:19:34,400
Oh, that little shit!

350
00:19:38,680 --> 00:19:40,239
No hope remaining.

351
00:19:40,240 --> 00:19:42,079
Oh, my God!

352
00:19:42,080 --> 00:19:45,239
Why would you... why would you
do that?

353
00:19:45,240 --> 00:19:48,160
Kieran,
your dad's here to pick you up.

354
00:19:57,680 --> 00:20:00,839
I mean, I actually had a date
with her. I had a date with her.

355
00:20:00,840 --> 00:20:03,439
To think, she was worried about
getting hurt emotionally.

356
00:20:03,440 --> 00:20:07,039
Yeah. And then you punched her
in the face.

357
00:20:07,040 --> 00:20:09,439
Yeah.

358
00:20:09,440 --> 00:20:12,079
That bloody kid. I mean, why?

359
00:20:12,080 --> 00:20:13,639
Why would he do all that?

360
00:20:13,640 --> 00:20:15,640
Hm.

361
00:20:19,560 --> 00:20:21,719
Is that a pizza cutter?

362
00:20:21,720 --> 00:20:24,560
Ow!

363
00:20:26,560 --> 00:20:30,880
I, myself, was supposed to be taking
my nephew to Thorpe Park today.

364
00:20:32,600 --> 00:20:36,079
Hang on, is it cool for me
to eat this? Not really.

365
00:20:36,080 --> 00:20:38,360
It's some little dickhead's.

366
00:20:41,200 --> 00:20:43,400
Probably fair enough.

367
00:20:46,560 --> 00:20:49,920
Coming up,
I try to apologise to Sophie.

368
00:20:53,200 --> 00:20:55,439
Don't.

369
00:20:55,440 --> 00:20:58,439
Mikey makes a totally
unreasonable request.

370
00:20:58,440 --> 00:21:02,239
Oh, can you pass me a Vimto, please?
What am I, your bloody slave?

371
00:21:02,240 --> 00:21:05,199
Oh, is there a Vimto going?
Yes, master.

372
00:21:05,200 --> 00:21:08,399
And Naz takes up less
a violent video game.

373
00:21:08,400 --> 00:21:10,719
No, Luigi, what's wrong with you?

374
00:21:10,720 --> 00:21:14,399
Get your balls out of my face!
How are you doing this, Luigi?

375
00:21:14,400 --> 00:21:17,439
How are you doing this?!

376
00:21:17,440 --> 00:21:19,480
This is Flat News.

