1
00:00:20,496 --> 00:00:21,888
<i>Grievous in retreat!</i>

2
00:00:22,908 --> 00:00:27,470
<i>Before the battleship Malevolence could
destroy an Outer Rim clone medical base,</i>

3
00:00:27,590 --> 00:00:32,114
<i>a Republic strike force, under
the command of Jedi Anakin Skywalker,</i>

4
00:00:32,234 --> 00:00:36,167
<i>crippled the warship,
disabling its dreaded ion cannon.</i>

5
00:00:39,614 --> 00:00:43,000
<i>All the Jedi relentlessly persue
the Malevolence.</i>

6
00:01:05,682 --> 00:01:08,356
Commander, what is the damage level
to the enemy ship?

7
00:01:08,642 --> 00:01:10,861
She's lost a primary shield
and stabilizes.

8
00:01:10,981 --> 00:01:14,111
But the ship is so massive, it can take
all the fire our cannons can give it.

9
00:01:14,462 --> 00:01:16,682
We must summon reinforcements.

10
00:01:16,802 --> 00:01:18,444
That's why I'm here, Master Plo.

11
00:01:18,564 --> 00:01:20,926
Anakin, were you able to contact
Master Luminara?

12
00:01:21,166 --> 00:01:24,624
Yes, Master. She's busy with a fleet
of separatits reinforcements nearby.

13
00:01:24,931 --> 00:01:27,362
She won't be able to give us support
until she's turned them away.

14
00:01:27,483 --> 00:01:29,571
Then we'll have to make due
with what we have.

15
00:01:40,335 --> 00:01:42,515
We've lost our primary weapon.

16
00:01:42,925 --> 00:01:45,184
The hyperdrive has been disabled.

17
00:01:46,047 --> 00:01:46,808
General?

18
00:01:46,927 --> 00:01:49,397
General, the forward engines
are shutting down.

19
00:01:49,926 --> 00:01:51,689
Impossible!

20
00:01:56,908 --> 00:01:59,449
- Admiral, status report.
- They're not attempting to jump

21
00:01:59,569 --> 00:02:01,991
to hyperspace.
Their hyperdrive must be damaged.

22
00:02:02,429 --> 00:02:03,674
This is our chance.

23
00:02:03,794 --> 00:02:06,913
All ships, target the bridge,
maximum firepower.

24
00:02:16,627 --> 00:02:20,592
It has not sustained extensive damage.

25
00:02:21,069 --> 00:02:23,193
<i>General, I have arranged a trap</i>

26
00:02:23,312 --> 00:02:25,723
<i>which will give you an advantage
over the jedi.</i>

27
00:02:25,883 --> 00:02:28,308
I assure you, that is unnecessary.

28
00:02:28,469 --> 00:02:30,899
<i>Do not compound your failure this day</i>

29
00:02:31,019 --> 00:02:34,022
<i>by allowing our prize warship to fall.</i>

30
00:02:34,183 --> 00:02:38,319
My lord, they will never capture me
or this ship.

31
00:02:38,613 --> 00:02:42,469
<i>Heading towards you
is a very important galactic senator.</i>

32
00:02:43,095 --> 00:02:47,619
<i>With her as your hostage,
they will call off their attack.</i>

33
00:02:48,027 --> 00:02:50,486
As you wish, my lord.

34
00:02:53,764 --> 00:02:55,546
My lady, are you sure

35
00:02:55,665 --> 00:02:58,996
the information
from chancellor Palpatine is reliable?

36
00:02:59,115 --> 00:03:01,143
Yes, it was secretly given to him

37
00:03:01,263 --> 00:03:03,744
by the supreme executive
of the banking clan himself.

38
00:03:04,169 --> 00:03:06,192
If they leave the separatist alliance,

39
00:03:06,313 --> 00:03:08,515
it will go a long way
to shortening this war.

40
00:03:10,080 --> 00:03:11,760
We're approaching the system now.

41
00:03:16,140 --> 00:03:17,315
My goodness!

42
00:03:18,185 --> 00:03:19,602
This isn't right!

43
00:03:19,721 --> 00:03:22,883
That's a droid warship.
We're in the middle of a battle!

44
00:03:23,730 --> 00:03:26,725
Sir, we're scanning a small ship
off our bow.

45
00:03:26,845 --> 00:03:30,203
Good.
Our hostage has arrived.

46
00:03:34,991 --> 00:03:38,062
Master, I'm picking up a signal
near the enemy vessel.

47
00:03:38,330 --> 00:03:40,171
Something just came out
of hyperspace.

48
00:03:40,330 --> 00:03:42,089
- Enemy reinforcements?
- No.

49
00:03:42,516 --> 00:03:45,051
It looks like a Naboo ship.

50
00:03:45,656 --> 00:03:47,284
Gunners, stand down.

51
00:03:47,517 --> 00:03:49,672
What in blazes
are they doing out here?

52
00:03:49,793 --> 00:03:51,265
Ahsoka, contact that ship.

53
00:03:54,248 --> 00:03:58,121
<i>- Naboo cruiser, identify yourself.
- This is senator Amidala.</i>

54
00:03:58,241 --> 00:04:00,232
Padme?
What are you doing out here?

55
00:04:00,392 --> 00:04:03,891
<i>I was sent on a special mission.
The senate was told the banking clan</i>

56
00:04:04,011 --> 00:04:06,696
<i>- wanted to negotiate a treaty.
- Get out of there as fast as you can.</i>

57
00:04:07,452 --> 00:04:09,728
Activate the tractor beam.

58
00:04:12,056 --> 00:04:15,579
- Have we been hit?
- I'm afraid it's much worse than that.

59
00:04:18,127 --> 00:04:19,334
Padme, what's happening?

60
00:04:19,494 --> 00:04:22,336
<i>I'm being pulled inside
the droid cruiser by a tractor beam.</i>

61
00:04:29,865 --> 00:04:31,971
<i>I will not be made
a separatist bargaining chip.</i>

62
00:04:32,254 --> 00:04:36,892
<i>Continue your attack.
You must destroy this monstrous ship!</i>

63
00:04:41,272 --> 00:04:44,232
Admiral,
order our ships to stop firing.

64
00:04:57,039 --> 00:04:59,507
It does not look safe out there,
my lady.

65
00:04:59,782 --> 00:05:03,626
I see now this whole thing was a trap,
and I'm afraid I walked right into it.

66
00:05:03,910 --> 00:05:07,152
Sir, the Republic cruisers
have halted their attack.

67
00:05:07,619 --> 00:05:09,854
Jedi are so predictable.

68
00:05:10,280 --> 00:05:11,949
Get the repair team up here.

69
00:05:12,069 --> 00:05:14,370
Continue working on the hyperdrive.

70
00:05:14,572 --> 00:05:16,909
I'm going down to the main hangar.

71
00:05:23,330 --> 00:05:26,909
This ship must be returned
to Count Dooku intact.

72
00:05:33,831 --> 00:05:36,265
There is no room for failure.

73
00:05:36,386 --> 00:05:38,118
<i>Rail jet departing.</i>

74
00:05:43,533 --> 00:05:45,900
Come on,
I've overloaded the power system.

75
00:05:46,020 --> 00:05:47,336
We're doomed.

76
00:05:49,309 --> 00:05:53,177
- Where do you think you're going?
- Somebody has to save her skin.

77
00:05:53,297 --> 00:05:55,594
I thought you might say that.

78
00:05:56,363 --> 00:06:00,625
I'm afraid Master Skywalker
is taking a terrible risk.

79
00:06:00,861 --> 00:06:02,537
You get used to it.

80
00:06:06,473 --> 00:06:08,481
Come on, 3PO, hurry.

81
00:06:08,752 --> 00:06:11,817
Hurrying, my lady.
I'm not sure this is such a good idea.

82
00:06:18,129 --> 00:06:20,721
<i>Welcome to rail
jet substation 9-4.</i>

83
00:06:20,841 --> 00:06:22,261
<i>Mind the gap.</i>

84
00:06:22,990 --> 00:06:24,196
<i>Mind the gap.</i>

85
00:06:25,108 --> 00:06:26,165
<i>Mind the gap.</i>

86
00:06:26,457 --> 00:06:28,423
You two, come with me.

87
00:06:38,628 --> 00:06:41,388
Looks like the engines are set
to destroy themselves.

88
00:06:42,563 --> 00:06:44,052
Get out of my way!

89
00:06:44,281 --> 00:06:45,935
Hey, that's just rude!

90
00:06:54,103 --> 00:06:55,949
Watch what you're doing
with that thing.

91
00:07:04,179 --> 00:07:05,295
Sound the alarm!

92
00:07:05,414 --> 00:07:07,443
We have stowaways on board.

93
00:07:15,846 --> 00:07:19,504
I trust you've already formulated
a brilliant plan to rescue the senator.

94
00:07:19,719 --> 00:07:21,827
As a matter of fact, I have.

95
00:07:21,946 --> 00:07:23,596
But do we have a plan "B"?

96
00:07:23,756 --> 00:07:25,765
Every operation
needs a backup, Anakin.

97
00:07:26,127 --> 00:07:30,185
I don't have a backup... yet, but I do
have a plan for getting on that ship.

98
00:07:30,626 --> 00:07:31,625
Really?

99
00:07:32,008 --> 00:07:34,157
The enemy sensors
are obviously damaged,

100
00:07:34,276 --> 00:07:37,337
so we'll sneak in behind them
and dock at the emergency airlock.

101
00:07:37,645 --> 00:07:38,908
That's your plan?

102
00:07:39,029 --> 00:07:40,467
Just fly there, land,

103
00:07:40,587 --> 00:07:42,948
hope they don't spot us,
and walk in the door?

104
00:07:43,626 --> 00:07:44,533
Basically.

105
00:07:45,627 --> 00:07:47,369
Brilliant. Let's get going.

106
00:08:03,627 --> 00:08:06,438
Pardon me, but might
I suggest we keep moving?

107
00:08:06,557 --> 00:08:08,764
I think I hear battle droids
approaching.

108
00:08:08,924 --> 00:08:11,380
But we also need
to contact the fleet.

109
00:08:11,743 --> 00:08:14,353
If I can just get
this comm panel working.

110
00:08:17,816 --> 00:08:20,485
My lady, I'm afraid I was right.

111
00:08:26,825 --> 00:08:28,183
<i>- General?
- What?</i>

112
00:08:28,416 --> 00:08:31,375
<i>The damage to the hyperdrive
was not as bad as we first thought.</i>

113
00:08:31,495 --> 00:08:33,789
<i>We should be able
to get under way again shortly.</i>

114
00:08:34,065 --> 00:08:36,042
I must inform Count Dooku.

115
00:08:36,340 --> 00:08:37,405
Stay here.

116
00:08:37,698 --> 00:08:39,295
Continue the search.

117
00:08:39,541 --> 00:08:42,520
Find the stowaway
and bring them to me.

118
00:08:42,845 --> 00:08:44,166
Roger, roger.

119
00:09:04,998 --> 00:09:07,238
If they spot us,
we'll be pulverized.

120
00:09:07,517 --> 00:09:09,493
They're too busy repairing the ship.

121
00:09:09,613 --> 00:09:11,618
They don't have time to notice us.

122
00:09:11,999 --> 00:09:14,703
Subtlety has never been
one of your strong points, Anakin.

123
00:09:15,244 --> 00:09:18,083
Everything I know
I learned from you, Master.

124
00:09:18,445 --> 00:09:20,669
If only that were true.

125
00:09:31,080 --> 00:09:32,721
- What was that?
- What?

126
00:09:33,056 --> 00:09:35,083
That noise.
Didn't you hear it?

127
00:09:35,458 --> 00:09:38,602
It sounded like a ship docking
at one of the emergency airlocks.

128
00:09:39,189 --> 00:09:42,523
Your circuits are loose.
No one's crazy enough to do that.

129
00:09:43,133 --> 00:09:44,900
Anakin, you're crazy.

130
00:09:45,223 --> 00:09:47,278
Spinning is not flying.

131
00:09:47,618 --> 00:09:51,031
- But it's a good trick.
- We do not want to be spotted.

132
00:09:51,350 --> 00:09:53,826
- I knew it! It's them!
- No.

133
00:09:57,665 --> 00:09:58,705
You stay here, R2.

134
00:10:00,547 --> 00:10:03,710
<i>Another bold strategy
by Skywalker, I presume.</i>

135
00:10:03,962 --> 00:10:07,188
- That's my master.
- Once they make it off that ship,

136
00:10:07,308 --> 00:10:09,803
we'll need reinforcements
to finish off the enemy.

137
00:10:10,460 --> 00:10:11,926
<i>I am on my way, Master Plo.</i>

138
00:10:12,086 --> 00:10:14,894
We're receiving a transmission
from inside the Malevolence.

139
00:10:15,197 --> 00:10:16,472
I believe it's the senator.

140
00:10:18,598 --> 00:10:19,273
Yes.

141
00:10:19,392 --> 00:10:22,338
<i>Master, we've found the senator.
I'm patching her through.</i>

142
00:10:22,458 --> 00:10:23,688
<i>- Padme?
- Anakin.</i>

143
00:10:23,988 --> 00:10:25,593
<i>Are you all right?
Where are you?</i>

144
00:10:25,713 --> 00:10:28,983
On the lower levels.
I'm fine, but I don't know for how long.

145
00:10:29,196 --> 00:10:32,238
- Droids are everywhere.
- Obi-Wan and I are on board too.

146
00:10:32,601 --> 00:10:34,365
What?
What are you doing here?

147
00:10:34,617 --> 00:10:36,614
We came to get you off this ship.

148
00:10:36,734 --> 00:10:38,411
Ahsoka,
how can we get to the senator?

149
00:10:39,083 --> 00:10:40,937
According to our scans,
there seems to be

150
00:10:41,057 --> 00:10:43,375
a larger open area
in the center of the ship

151
00:10:43,494 --> 00:10:45,584
It should be halfway
between the two of you.

152
00:10:46,059 --> 00:10:48,128
We're on our way.
Did you hear that, Padme?

153
00:10:48,471 --> 00:10:49,677
I'll be there.

154
00:10:55,329 --> 00:10:58,939
General, we just detected
an unauthorized communication

155
00:10:59,059 --> 00:11:01,725
- coming from within the ship.
- What did it say?

156
00:11:02,297 --> 00:11:03,297
Well...

157
00:11:04,080 --> 00:11:05,453
We don't know.

158
00:11:05,890 --> 00:11:08,261
We didn't catch it in time.

159
00:11:08,515 --> 00:11:11,419
Monitor all internal communications.

160
00:11:11,894 --> 00:11:15,279
I want that senator on this bridge.

161
00:11:22,030 --> 00:11:25,273
- I don't see her, Anakin.
- She's here, master. I sense it.

162
00:11:31,069 --> 00:11:32,838
This is where we're supposed
to meet them.

163
00:11:34,679 --> 00:11:36,129
He's probably late again.

164
00:11:36,249 --> 00:11:38,738
But we do have company
of another sort.

165
00:11:39,591 --> 00:11:40,805
Blast them!

166
00:11:43,990 --> 00:11:44,907
There!

167
00:11:53,855 --> 00:11:55,296
Look, Jedi.

168
00:11:55,416 --> 00:11:57,028
- Fire!
- No, wait.

169
00:11:59,841 --> 00:12:01,741
I knew that was a bad idea.

170
00:12:03,412 --> 00:12:04,254
Jump!

171
00:12:06,146 --> 00:12:07,656
Who, me?

172
00:12:13,298 --> 00:12:14,139
Padme!

173
00:12:27,097 --> 00:12:28,476
There they are.

174
00:12:28,719 --> 00:12:29,518
Fire!

175
00:12:34,259 --> 00:12:36,316
- The bridge is out.
- Jump to me.

176
00:12:37,388 --> 00:12:39,276
You have to trust me.

177
00:12:43,233 --> 00:12:45,826
- I've got you.
- Nice catch.

178
00:12:46,361 --> 00:12:48,078
I'll fetch the droid.

179
00:12:52,492 --> 00:12:54,710
The things you do to get me alone.

180
00:12:55,231 --> 00:12:56,357
Stop talking.

181
00:13:02,398 --> 00:13:03,843
What is going on?

182
00:13:12,322 --> 00:13:15,188
Stop me, please!

183
00:13:15,654 --> 00:13:17,706
Blast,
that's not good.

184
00:13:18,778 --> 00:13:21,170
Anakin, I got separated
from your droid.

185
00:13:21,290 --> 00:13:22,010
3PO.

186
00:13:22,861 --> 00:13:24,128
I'll take care of it.

187
00:13:24,248 --> 00:13:27,033
- We'll meet you back at the Twilight.
- No, we can't leave yet.

188
00:13:27,192 --> 00:13:30,119
I overheard Grievous.
Their hyperdrive is almost repaired.

189
00:13:30,279 --> 00:13:32,234
I'm already headed
in that direction,

190
00:13:32,354 --> 00:13:35,499
so I'll make certain
that the hyperdrive stays offline.

191
00:13:37,964 --> 00:13:41,671
We'll see about that, Jedi.
Come with me.

192
00:13:42,031 --> 00:13:45,926
I'm getting you out of here.
R2, I need you to help me find 3PO.

193
00:13:46,086 --> 00:13:47,540
He's on the rail jet.

194
00:13:47,941 --> 00:13:50,097
I know. I know. He does.

195
00:13:50,460 --> 00:13:53,266
Look, just find him for me,
and I'll be there soon.

196
00:13:58,636 --> 00:14:02,390
Someone stop this contraption,
please!

197
00:14:08,462 --> 00:14:10,568
I suppose I did ask for that.

198
00:14:32,923 --> 00:14:34,105
Hello, there.

199
00:14:41,369 --> 00:14:42,939
General Kenobi,

200
00:14:43,283 --> 00:14:47,528
did you really think I would leave
the hyperdrive unguarded?

201
00:14:47,687 --> 00:14:49,236
Anything is possible.

202
00:14:49,357 --> 00:14:51,322
You haven't exactly
impressed me today.

203
00:14:53,056 --> 00:14:54,200
Kill him.

204
00:15:25,095 --> 00:15:26,479
That was impressive.

205
00:15:27,694 --> 00:15:29,225
Guard the hyperdrive!

206
00:15:29,469 --> 00:15:31,961
I will deal with this Jedi myself!

207
00:15:41,307 --> 00:15:43,364
Obi-Wan? Come in, Obi-Wan.

208
00:15:43,484 --> 00:15:46,323
<i>Anakin,
I'm afraid Grievous is on to us.</i>

209
00:15:46,443 --> 00:15:47,920
Yeah, we noticed.

210
00:15:50,123 --> 00:15:52,007
<i>We'll rendezvous
back at the Twilight.</i>

211
00:15:52,127 --> 00:15:54,119
<i>The fleet must engage the...</i>

212
00:15:54,239 --> 00:15:56,303
Obi-Wan? Come in. Obi-Wan!

213
00:15:56,728 --> 00:15:58,347
- What's wrong?
- They're jamming us.

214
00:16:08,451 --> 00:16:11,859
- That might buy us some time.
- I suppose you have a plan.

215
00:16:12,286 --> 00:16:13,381
Follow me.

216
00:16:14,991 --> 00:16:18,429
I do believe I'm lost...
in enemy territory...

217
00:16:18,909 --> 00:16:20,076
and all alone.

218
00:16:20,923 --> 00:16:23,232
Don't shoot!
I surrender!

219
00:16:24,904 --> 00:16:26,409
R2-D2, my

220
00:16:26,905 --> 00:16:29,084
you are a sight for short circuits.

221
00:16:30,886 --> 00:16:33,067
Master Anakin sent you to find me?

222
00:16:33,289 --> 00:16:34,668
What kept you then?

223
00:16:38,816 --> 00:16:41,333
The General is demanding
a status report.

224
00:16:41,454 --> 00:16:43,868
Is the hyperdrive repaired yet?

225
00:16:43,988 --> 00:16:45,266
It's almost done.

226
00:16:45,985 --> 00:16:47,800
I'll give him the good news.

227
00:17:01,826 --> 00:17:04,953
Ever since I've known you,
you've been playing with droids.

228
00:17:05,231 --> 00:17:09,065
I used to put them together.
Now I only take them apart.

229
00:17:09,397 --> 00:17:10,625
So where do we start?

230
00:17:11,283 --> 00:17:14,849
First we need to get rid of these droids
so they won't know we were here.

231
00:17:14,969 --> 00:17:18,216
I'm gonna hotwire the ship,
give Grievous a little surprise.

232
00:17:18,884 --> 00:17:21,594
I guess I'll clean up
the droids then.

233
00:17:25,279 --> 00:17:28,476
Our ships are in attack position.
Any word from master Skywalker?

234
00:17:28,813 --> 00:17:31,996
No, the droids are jamming
our transmissions.

235
00:17:32,238 --> 00:17:34,231
We need to give him more time.

236
00:17:34,391 --> 00:17:36,309
I'm not sure we can.

237
00:18:20,613 --> 00:18:23,028
That ought to do it.
How's the housecleaning going?

238
00:18:23,700 --> 00:18:25,264
Done.
Let's get out of here.

239
00:18:30,261 --> 00:18:32,300
I guess repairs are finished.

240
00:18:32,420 --> 00:18:35,348
- Prepare to charge up the hyperdrive.
- Roger, roger.

241
00:18:40,653 --> 00:18:43,090
R2, are you quite certain
the ship is in this direction?

242
00:18:44,942 --> 00:18:46,716
This way
looks potentially dangerous.

243
00:18:47,802 --> 00:18:49,971
I know the whole place is dangerous!

244
00:18:50,493 --> 00:18:54,009
I suggest we stay here
and let master Anakin find us.

245
00:18:54,633 --> 00:18:57,187
3PO, what are you doing?
Don't just stand there.

246
00:18:57,623 --> 00:18:59,147
Let's get back to the ship.

247
00:18:59,986 --> 00:19:02,836
- Power up the engines, R2.
- Hold the ship!

248
00:19:09,599 --> 00:19:11,372
I'll contact the fleet.

249
00:19:12,663 --> 00:19:14,287
R2, release the docking clamp.

250
00:19:36,873 --> 00:19:40,396
- Time for some clever tricks, Anakin.
- That's what I was thinking.

251
00:19:48,105 --> 00:19:50,030
All batteries, open fire!

252
00:19:58,263 --> 00:20:01,025
You know, we have guns.
You can shoot back any time.

253
00:20:01,145 --> 00:20:03,021
- I was just about to...
- I got it.

254
00:20:09,315 --> 00:20:11,050
She seems to know her way around.

255
00:20:16,833 --> 00:20:19,907
<i>General, the hyperdrive
has been completely repaired.</i>

256
00:20:20,027 --> 00:20:23,966
<i>- Should we retreat to friendly space?
- Engage the hyperdrive.</i>

257
00:20:24,085 --> 00:20:27,566
I'll meet you at our secret base
in sector four.

258
00:20:28,247 --> 00:20:31,409
You heard the General.
Prepare to make the jump to hyperspace.

259
00:20:31,529 --> 00:20:32,529
Yes, Sir.

260
00:20:44,701 --> 00:20:47,336
- Nice shot, senator.
- Beginner's luck.

261
00:20:48,722 --> 00:20:51,275
Pardon me, Sir,
but R2's scan of the enemy's ship

262
00:20:51,395 --> 00:20:53,349
indicates their hyperdrive
is activating.

263
00:20:53,469 --> 00:20:55,092
- Don't worry about it.
- What?

264
00:20:55,445 --> 00:20:58,485
Coordinates are locked.
Hyperdrive is engaging.

265
00:21:00,131 --> 00:21:02,183
I think we have a problem.

266
00:21:02,344 --> 00:21:05,062
<i>General, there's something wrong
with the hyperdrive.</i>

267
00:21:05,444 --> 00:21:07,981
I thought the hyperdrive was fixed.

268
00:21:08,922 --> 00:21:11,943
The navicomputer is heading us
right into the moon!

269
00:21:12,287 --> 00:21:14,737
Fools, reset the navicomputer!

270
00:21:15,073 --> 00:21:17,292
Quick, reset the navicomputer!

271
00:21:19,804 --> 00:21:23,746
<i>General, we await the Malevolence
at the rendezvous point.</i>

272
00:21:24,198 --> 00:21:26,332
<i>Have you made your escape yet?</i>

273
00:21:28,569 --> 00:21:29,856
No, reset it...

274
00:21:30,567 --> 00:21:31,394
General?

275
00:21:32,494 --> 00:21:33,493
General?

276
00:21:38,955 --> 00:21:41,429
Transmission has been cut off, Sir.

277
00:21:41,590 --> 00:21:43,932
I think the General did it himself.

278
00:21:44,092 --> 00:21:46,044
We're gonna die! Abandon ship!

279
00:21:51,976 --> 00:21:54,109
I imagine you had something
to do with that.

280
00:21:55,117 --> 00:21:57,070
All part of the plan, Master.

