1
00:03:46,315 --> 00:03:48,526
Dick Laurent is dead.

2
00:05:36,013 --> 00:05:38,975
You don't mind that
I'm not coming tonight?

3
00:05:42,060 --> 00:05:43,770
What are you going to do?

4
00:05:45,690 --> 00:05:48,066
Stay home, read.

5
00:05:51,654 --> 00:05:52,572
Read?

6
00:06:03,708 --> 00:06:04,876
Read?

7
00:06:07,129 --> 00:06:08,756
Read what, Renee?

8
00:06:15,763 --> 00:06:18,850
It's nice to know
I can still make you laugh.

9
00:06:19,893 --> 00:06:22,102
I like to laugh, Fred.

10
00:06:28,485 --> 00:06:30,695
That's why I married you.

11
00:06:35,450 --> 00:06:38,538
You can wake me up when
you get home if you want to.

12
00:10:20,019 --> 00:10:21,187
What is that?

13
00:10:27,693 --> 00:10:29,737
A videotape.

14
00:10:32,615 --> 00:10:35,159
I found it outside on the steps.

15
00:10:38,997 --> 00:10:40,582
Who's it from?

16
00:10:41,834 --> 00:10:43,085
I don't know.

17
00:10:45,212 --> 00:10:47,422
There isn't anything on the envelope.

18
00:10:51,135 --> 00:10:53,429
Does it say anything on the tape?

19
00:10:59,060 --> 00:11:00,645
No. Nothing.

20
00:11:03,940 --> 00:11:06,234
Well, see what's on it.

21
00:11:22,960 --> 00:11:23,794
Come on.

22
00:12:03,586 --> 00:12:06,255
Must be from a real estate agent.

23
00:12:08,341 --> 00:12:09,592
Maybe.

24
00:15:39,520 --> 00:15:41,315
It's OK.

25
00:15:42,399 --> 00:15:44,109
It's OK.

26
00:15:45,277 --> 00:15:46,862
It's OK.

27
00:16:37,623 --> 00:16:39,917
I had a dream last night.

28
00:16:44,589 --> 00:16:46,007
You were inside the house...

29
00:16:55,517 --> 00:16:57,477
You were calling my name...

30
00:17:02,066 --> 00:17:02,983
Fred!

31
00:17:08,572 --> 00:17:09,323
Fred!

32
00:17:11,784 --> 00:17:12,952
Where are you.

33
00:17:19,250 --> 00:17:21,210
I couldn't find you.

34
00:17:39,730 --> 00:17:41,273
Then there you were...

35
00:17:42,608 --> 00:17:44,110
... Iying in bed.

36
00:17:45,360 --> 00:17:47,070
It wasn't you.

37
00:17:47,947 --> 00:17:51,701
It looked like you, but it wasn't.

38
00:18:14,140 --> 00:18:17,645
Fred, are you all right?

39
00:19:24,549 --> 00:19:26,676
You're up early.

40
00:19:27,844 --> 00:19:29,721
That dog woke me.

41
00:19:40,732 --> 00:19:43,360
Who the hell owns that dog?

42
00:19:54,871 --> 00:19:56,248
What's that?

43
00:20:01,253 --> 00:20:03,298
Another videotape.

44
00:20:04,299 --> 00:20:05,216
Yes.

45
00:20:18,688 --> 00:20:20,565
Don't you want to watch it?

46
00:20:22,317 --> 00:20:23,485
I guess so.

47
00:20:40,586 --> 00:20:42,380
Well, don't you want to watch it?

48
00:20:43,422 --> 00:20:44,423
Yeah.

49
00:20:56,478 --> 00:20:58,021
It's the same thing.

50
00:21:03,944 --> 00:21:05,195
No, it isn't.

51
00:21:13,997 --> 00:21:14,663
Fred?

52
00:21:29,137 --> 00:21:30,722
What?

53
00:21:42,651 --> 00:21:45,196
We have to call the police.

54
00:21:46,155 --> 00:21:47,324
That's right. Yes.

55
00:21:53,078 --> 00:21:58,834
Someone broke in and taped us
while we slept. Isn't that enough?

56
00:22:01,421 --> 00:22:03,547
7035 Hollis.

57
00:22:05,133 --> 00:22:07,594
Near the observatory.

58
00:22:09,387 --> 00:22:10,055
Yes.

59
00:22:12,098 --> 00:22:13,600
We'll be here.

60
00:22:18,523 --> 00:22:19,690
So?

61
00:22:21,234 --> 00:22:23,194
They're sending 2 detectives out.

62
00:22:39,795 --> 00:22:40,963
That's it.

63
00:22:42,047 --> 00:22:43,297
What do you think.

64
00:22:43,882 --> 00:22:46,093
I really don't know.

65
00:22:55,436 --> 00:22:58,189
Let's check the hallway and the bedroom.

66
00:23:21,547 --> 00:23:23,090
This is the bedroom?

67
00:23:24,340 --> 00:23:27,595
You sleep here in this room.
Both of yous.

68
00:23:31,140 --> 00:23:33,017
This is our bedroom.

69
00:23:36,228 --> 00:23:38,022
There's no other bedroom?

70
00:23:39,900 --> 00:23:41,150
No.

71
00:23:43,402 --> 00:23:47,782
I mean, I use it as a practice room.

72
00:23:48,742 --> 00:23:50,160
It's soundproof.

73
00:23:50,493 --> 00:23:51,828
You're a musician?

74
00:23:53,330 --> 00:23:54,080
Yeah.

75
00:23:54,707 --> 00:23:56,292
What's your axe?

76
00:23:56,500 --> 00:23:59,044
Tenor. Tenor-Saxophone.

77
00:24:00,670 --> 00:24:03,424
- Do you...
- No... Tone deaf.

78
00:24:04,925 --> 00:24:06,552
Do you own a video camera?

79
00:24:06,761 --> 00:24:07,428
No.

80
00:24:09,680 --> 00:24:11,265
Fred hates them.

81
00:24:18,439 --> 00:24:21,442
I like to remember things my own way.

82
00:24:22,735 --> 00:24:24,153
What do you mean by that?

83
00:24:27,241 --> 00:24:31,537
How I remembered them...
Not necessarily the way they happened.

84
00:24:35,916 --> 00:24:37,709
You have an alarm system?

85
00:24:37,919 --> 00:24:40,588
Yes. Actually, we do.

86
00:24:41,214 --> 00:24:44,008
But we haven't been using it lately.

87
00:24:45,176 --> 00:24:46,177
Why not?

88
00:24:49,222 --> 00:24:51,432
It kept going off...

89
00:24:52,642 --> 00:24:56,145
For some reason. False alarms.

90
00:24:57,939 --> 00:24:59,899
Might want to try and use it again.

91
00:25:00,775 --> 00:25:01,943
Yeah.

92
00:25:04,947 --> 00:25:05,948
OK?

93
00:25:09,618 --> 00:25:13,038
We're going to check the windows and doors,
see if anybody tried to break in.

94
00:26:27,033 --> 00:26:30,870
-We'll keep watch over the house.
-We'll do the best we can.

95
00:26:32,872 --> 00:26:36,209
-If anything else happens, you'll call us.
-We will.

96
00:26:38,169 --> 00:26:41,423
-Thanks, guys.
-It's what we do.

97
00:27:16,418 --> 00:27:20,255
Hey, Andy! Welcome to my party.

98
00:27:21,173 --> 00:27:23,550
-You look ravishing.
-Thank you.

99
00:27:38,316 --> 00:27:38,984
Fred!

100
00:27:41,068 --> 00:27:42,153
Please!

101
00:27:44,197 --> 00:27:45,448
Please!

102
00:27:47,658 --> 00:27:48,743
Hey!

103
00:28:05,552 --> 00:28:08,389
-2 double scotches, neat.
-OK.

104
00:28:59,776 --> 00:29:02,571
We've met before, haven't we?

105
00:29:06,116 --> 00:29:07,742
I don't think so.

106
00:29:10,412 --> 00:29:13,082
Where was it you think we met?

107
00:29:14,541 --> 00:29:17,002
At your house. Don't you remember?

108
00:29:20,422 --> 00:29:22,300
No. No, I don't.

109
00:29:23,676 --> 00:29:25,094
Are you sure?

110
00:29:25,928 --> 00:29:27,471
Of course.

111
00:29:28,973 --> 00:29:33,270
As a matter of fact, I'm there right now.

112
00:29:38,108 --> 00:29:41,695
What do you mean? You're where right now?

113
00:29:43,363 --> 00:29:46,033
At your house.

114
00:29:49,745 --> 00:29:52,123
That's fucking crazy, man!

115
00:30:00,673 --> 00:30:02,550
Call me!

116
00:30:08,848 --> 00:30:10,476
Dial your number!

117
00:30:14,605 --> 00:30:15,773
Go ahead!

118
00:30:28,035 --> 00:30:30,871
I told you I was here.

119
00:30:34,333 --> 00:30:35,918
How'd you do that?

120
00:30:39,505 --> 00:30:41,215
Ask me!

121
00:30:47,556 --> 00:30:49,057
How'd you get inside my house?

122
00:30:49,266 --> 00:30:55,106
You invited me. It is not my custom
to go where I'm not wanted.

123
00:30:56,232 --> 00:30:57,315
Who are you?

124
00:30:58,484 --> 00:30:59,485
Hm.

125
00:31:05,241 --> 00:31:07,368
Give me back my phone.

126
00:31:18,838 --> 00:31:21,717
It's been a pleasure talking to you.

127
00:31:39,610 --> 00:31:42,864
-I thought you were getting me a drink.
-Hang on just a minute.

128
00:31:43,823 --> 00:31:48,620
Andy, who's the guy on the stairs?
Guy in black?

129
00:31:49,996 --> 00:31:53,249
I don't know his name. He's
a friend of Dick Laurent, I think.

130
00:31:58,254 --> 00:31:59,882
Dick Laurent?

131
00:32:00,883 --> 00:32:03,093
Yeah. I believe so.

132
00:32:09,391 --> 00:32:11,518
But Dick Laurent is dead, isn't he?

133
00:32:14,147 --> 00:32:15,481
He is?

134
00:32:16,899 --> 00:32:20,945
I didn't think you knew Dick.
How do you know he's dead?

135
00:32:24,240 --> 00:32:25,867
I don't.

136
00:32:27,035 --> 00:32:28,663
I don't know him.

137
00:32:29,412 --> 00:32:31,374
Dick can't be dead.

138
00:32:32,040 --> 00:32:34,167
Who told you he was dead?

139
00:32:34,751 --> 00:32:38,256
Who, honey?
Who's dead?

140
00:32:40,841 --> 00:32:42,467
Let's go home.

141
00:32:42,677 --> 00:32:44,971
-But...
-Now. We're leaving now.

142
00:32:48,307 --> 00:32:50,268
We never should have come here in the first place.

143
00:32:57,734 --> 00:33:00,737
So, how'd you meet that asshole Andy anyway?

144
00:33:04,658 --> 00:33:06,618
It was a long time ago.

145
00:33:09,120 --> 00:33:11,498
We met at a place called Moke's.

146
00:33:13,166 --> 00:33:15,043
We became friends.

147
00:33:16,129 --> 00:33:17,754
He told me about a job.

148
00:33:18,797 --> 00:33:19,966
What job?

149
00:33:23,177 --> 00:33:24,888
I don't remember.

150
00:33:26,890 --> 00:33:29,976
Anyway...Andy's OK.

151
00:33:33,813 --> 00:33:36,107
Well, He's got some pretty fucked-up friends.

152
00:33:46,159 --> 00:33:47,620
Hey!..
Hey!..

153
00:33:49,205 --> 00:33:50,623
Stay in the car.

154
00:35:19,424 --> 00:35:21,385
I told you to stay in the car.

155
00:35:21,593 --> 00:35:24,930
Why? What is it?

156
00:35:25,722 --> 00:35:28,517
Why did you make me stay out here?

157
00:35:31,145 --> 00:35:34,815
I'll tell you why, because I thought someone was inside the house.

158
00:35:37,735 --> 00:35:40,197
Was there?

159
00:35:42,156 --> 00:35:44,034
No, of course not.

160
00:38:29,916 --> 00:38:30,832
Fred?

161
00:38:38,633 --> 00:38:41,261
Fred, where are you?

162
00:41:36,570 --> 00:41:37,236
Renee!

163
00:41:39,740 --> 00:41:40,407
Renee!

164
00:41:41,073 --> 00:41:43,118
Sit down, Killer!

165
00:41:47,789 --> 00:41:49,666
I didn't kill her.

166
00:41:52,711 --> 00:41:54,838
Tell me I didn't kill her.

167
00:42:08,436 --> 00:42:12,982
We, the jury, find the defendant
guilty of murder in the first degree.

168
00:42:19,490 --> 00:42:24,118
Fred Madison, the jury having found
you guilty of murder in the first degree,

169
00:42:24,370 --> 00:42:28,040
it is my order that you be put to death in the electric chair.

170
00:42:41,470 --> 00:42:45,850
Make yourself to home, fella.

171
00:42:54,025 --> 00:42:55,735
Stick your hand out, chief.

172
00:45:10,460 --> 00:45:11,628
Something wrong?

173
00:45:17,134 --> 00:45:18,719
It's...my head.

174
00:45:44,871 --> 00:45:46,206
You sleeping OK?

175
00:45:47,207 --> 00:45:47,874
No.

176
00:45:49,752 --> 00:45:51,879
I can't sleep.

177
00:46:41,514 --> 00:46:43,140
You'll sleep now.

178
00:46:44,768 --> 00:46:47,062
Take him back to his cell.

179
00:46:48,730 --> 00:46:49,814
Let's go.

180
00:47:02,035 --> 00:47:02,704
Guard!

181
00:47:07,165 --> 00:47:07,834
Guard!

182
00:47:20,638 --> 00:47:21,306
Guard!

183
00:47:27,312 --> 00:47:27,896
Hey!

184
00:47:31,566 --> 00:47:32,734
What do you want?

185
00:47:34,486 --> 00:47:37,406
Aspirin.
My head.

186
00:47:40,075 --> 00:47:41,869
Hey! Hey!

187
00:47:54,840 --> 00:47:58,929
Shit. That wife killer's looking pretty fucked up.

188
00:47:59,596 --> 00:48:01,181
Which one?

189
00:49:48,251 --> 00:49:49,502
Pete!

190
00:49:50,421 --> 00:49:51,963
Please don't go!

191
00:49:53,506 --> 00:49:54,174
No. Pete!

192
00:49:55,216 --> 00:49:56,552
Pete!..Pete!..Pete!
Wait a minute!

193
00:51:25,520 --> 00:51:27,981
Fuck me!

194
00:51:37,700 --> 00:51:39,242
All right. What's the situation?

195
00:51:39,452 --> 00:51:42,537
I'm not entirely certain, captain.
You'll have to see for yourself.

196
00:51:58,638 --> 00:51:59,639
Right here.

197
00:52:05,145 --> 00:52:06,939
That's not Fred Madison.

198
00:52:09,191 --> 00:52:10,984
No, Sir. It's not.

199
00:52:15,405 --> 00:52:18,158
-Who is it?
-I couldn't say, Sir.

200
00:52:20,328 --> 00:52:21,578
Captain Luneau?

201
00:52:22,330 --> 00:52:23,413
Yeah, Mike?

202
00:52:24,414 --> 00:52:25,166
Captain...

203
00:52:25,959 --> 00:52:28,418
This is some spooky shit we got here.

204
00:52:38,138 --> 00:52:41,141
Well, Gentlemen, we know who the stranger is.

205
00:52:42,726 --> 00:52:45,813
His name is Peter Raymond Dayton.
24 years old.

206
00:52:46,563 --> 00:52:49,900
Arrested 5 years ago for auto theft

207
00:52:50,609 --> 00:52:53,320
for which he was put on probation for 1 year.

208
00:52:54,405 --> 00:53:01,371
He lives with his parents. William and
Candace Dayton, at 814 Garland Avenue.

209
00:53:53,425 --> 00:53:54,092
So...

210
00:54:35,844 --> 00:54:38,138
-Here.
-Thanks, Lou.

211
00:56:30,090 --> 00:56:32,758
Where the fuck have you been, man?

212
00:56:33,385 --> 00:56:35,011
-Hey! -Hiya. -Hi.
-Hey, guy.

213
00:56:36,263 --> 00:56:38,807
-Hey, man.
-What's up man?

214
00:56:39,557 --> 00:56:42,102
Well, you look like shit.

215
00:56:42,310 --> 00:56:43,812
Yeah. What happened?

216
00:56:45,563 --> 00:56:47,524
Just haven't been feeling so good.

217
00:56:47,732 --> 00:56:49,693
You're not contagious, are you?

218
00:56:49,902 --> 00:56:51,487
No. Uh-uh. Don't think so.

219
00:56:51,695 --> 00:56:53,656
All right. Well, let's go for a drive then.

220
00:56:53,863 --> 00:56:56,617
Different kinds of fruits grow and ripen.

221
00:56:57,243 --> 00:57:00,162
It takes many strawberries to fill a bucket.

222
00:57:00,371 --> 00:57:03,292
But it's worth it when you know that...

223
00:57:03,584 --> 00:57:04,959
-Come on.
-Hoh.

224
00:57:05,418 --> 00:57:06,752
Come on. I'll lead.

225
00:57:07,086 --> 00:57:10,591
...and a glass of fresh, cold milk from grandmother's cow.

226
00:57:13,010 --> 00:57:13,844
See ya later.

227
00:57:14,802 --> 00:57:18,473
-I'm gonna go out with these clowns for a while.
-That should do you some good.

228
00:57:19,850 --> 00:57:24,313
-Good night. -See ya.
-Good night.

229
00:57:53,384 --> 00:57:55,094
What's happening to you?

230
00:57:56,846 --> 00:57:58,724
What happened to your face?

231
00:58:00,476 --> 00:58:03,312
-I don't know?
-What do you mean, you don't know?

232
00:58:04,480 --> 00:58:06,858
You've been acting strange lately.

233
00:58:07,399 --> 00:58:08,776
Like the other night.

234
00:58:11,404 --> 00:58:12,572
What night?

235
00:58:13,238 --> 00:58:15,533
The last time I saw you.

236
00:58:16,451 --> 00:58:17,910
I don't remember.

237
00:58:29,382 --> 00:58:31,092
You still care about me?

238
00:58:57,702 --> 00:58:58,369
Pete!

239
00:59:01,373 --> 00:59:03,249
-Arnie!
-Where you been?

240
00:59:03,500 --> 00:59:06,044
-How you doing?
-It's good you're back...

241
00:59:06,295 --> 00:59:09,047
...Wonderful.
-Good to see you, man.

242
00:59:09,257 --> 00:59:09,923
Listen, man, a lot of people...

243
00:59:10,173 --> 00:59:13,510
...gonna be real happy that you're back, including me.

244
00:59:13,760 --> 00:59:15,345
Well, it's good to be back.

245
00:59:15,555 --> 00:59:17,682
Mr. Smith has been waiting for you.

246
00:59:17,889 --> 00:59:19,225
Yeah, I'll take care of him.

247
00:59:19,434 --> 00:59:22,060
And Mr. Eddy called.

248
00:59:22,270 --> 00:59:26,481
-Can I call him and tell him to come in?
-Yeah. Call him. Tell him I'm back.

249
00:59:27,150 --> 00:59:30,111
-And, uh, you know, I'm ready to work.
-You're ready to work?
-Yeah.

250
00:59:30,320 --> 00:59:32,697
-Well, let's go to work.
-All right, Buddy.

251
00:59:36,327 --> 00:59:37,911
Petey's back!

252
00:59:55,847 --> 00:59:57,432
There's 9 People down here.

253
00:59:58,390 --> 01:00:03,188
And you're gonna ask 7 of them.
If you get that price from one of them,...

254
01:00:03,772 --> 01:00:06,316
...I'll let you ask the other 2.

255
01:00:15,867 --> 01:00:17,285
Hey, Pete!

256
01:00:20,622 --> 01:00:21,623
Pete!

257
01:00:24,585 --> 01:00:25,586
Where's Pete?

258
01:00:26,963 --> 01:00:28,297
He's back there!

259
01:00:28,506 --> 01:00:29,340
Mr. Eddy!

260
01:00:31,133 --> 01:00:32,218
Hey!

261
01:00:36,264 --> 01:00:37,723
What happened?

262
01:00:38,600 --> 01:00:40,142
-Somebody giving you trouble?
-No. No trouble.

263
01:00:40,352 --> 01:00:44,106
'Cause if somebody's giving you trouble, Pete,...

264
01:00:44,356 --> 01:00:47,109
...I can take care of the problem...
Like that!

265
01:00:47,317 --> 01:00:51,697
-No. I'm fine, Mr. Eddy.
-I mean it, Pete. Like...that!

266
01:00:51,947 --> 01:00:54,324
Thank you. Really. I'm fine.
So, uh, what do you want,...

267
01:00:54,533 --> 01:00:58,370
...just a regular tune-up?
-I want you to take a ride with me.

268
01:00:58,997 --> 01:01:01,206
I don't like the sound of something.

269
01:01:01,416 --> 01:01:05,502
-OK. I got to check in with the boss.
-It's OK with Arnie. Come on, let's go.

270
01:01:40,749 --> 01:01:43,043
Pull it over, but keep it running.

271
01:02:04,481 --> 01:02:06,943
Best goddamn ears in town.

272
01:02:21,792 --> 01:02:23,502
Give that a try.

273
01:02:29,758 --> 01:02:34,888
Beautiful...Smooth as shit from a duck's ass.

274
01:02:37,683 --> 01:02:40,853
-Let's take a ride.
-Whatever you say, Mr. Eddy.

275
01:02:54,701 --> 01:02:56,744
It's a beautiful day.

276
01:03:04,086 --> 01:03:06,045
-You did a great job, Pete.
-Yeah.

277
01:03:07,423 --> 01:03:10,134
Well, you know I like working on this car, Mr. Eddy.

278
01:03:27,360 --> 01:03:31,113
Shit. That cocksucker doing what I think he's doing?

279
01:04:02,605 --> 01:04:04,148
This is where mechanical excellence...

280
01:04:04,358 --> 01:04:08,027
...and 1400 horsepower pays off.

281
01:04:43,523 --> 01:04:45,401
Ahh...Oh, oh, God!

282
01:04:50,613 --> 01:04:55,160
Don't you ever fucking tailgate!

283
01:04:55,661 --> 01:04:57,538
-Ever!
-Tell him you won't tailgate.

284
01:04:57,746 --> 01:04:58,580
Ever!

285
01:04:58,998 --> 01:05:02,251
-I won't ever...
-Do you know how many fucking car lengths it takes...

286
01:05:02,502 --> 01:05:05,671
...to stop a car at 35 miles an hour?!

287
01:05:05,879 --> 01:05:09,384
6 fucking car lengths!
That's 106 fucking feet, Mister!

288
01:05:10,343 --> 01:05:12,971
If I had to stop suddenly, you would've hit me!

289
01:05:13,179 --> 01:05:17,725
I want you to get a fucking driver's manual!
I want you to study that motherfucker!

290
01:05:17,976 --> 01:05:20,270
And I want you to obey the goddamn rules!

291
01:05:20,478 --> 01:05:24,399
50 fucking thousand people were
killed on the highway last

292
01:05:24,649 --> 01:05:27,527
Because of fucking assholes like you!

293
01:05:28,069 --> 01:05:34,160
-Tell me you're going to get a manual!
-I will ge-get a manual.

294
01:05:36,871 --> 01:05:38,581
Fucking ldiot!

295
01:05:38,831 --> 01:05:41,917
Oh!.Aah!.Oh, Gooood!

296
01:06:21,250 --> 01:06:23,128
I'm sorry about that, Pete.

297
01:06:23,962 --> 01:06:27,799
But tailgating is one thing I cannot tolerate.

298
01:06:29,342 --> 01:06:31,219
Yeah, I can see that.

299
01:06:42,064 --> 01:06:42,982
Wait a minute!

300
01:06:48,445 --> 01:06:50,824
-Thanks, Mr. Eddy.
-No, thank you.

301
01:06:51,366 --> 01:06:53,410
I'm bringing the Caddy by tomorrow.

302
01:06:55,704 --> 01:06:57,205
You like pornos?

303
01:06:57,414 --> 01:06:59,873
-Pornos?
-Give you a boner?

304
01:07:01,042 --> 01:07:03,837
Uh, No, No thanks. No.

305
01:07:04,462 --> 01:07:05,964
Suit yourself, champ.

306
01:07:07,215 --> 01:07:11,219
-Well, Uh, I-I'll see you then.
-You will.

307
01:07:17,809 --> 01:07:19,312
Damn!

308
01:07:27,320 --> 01:07:30,407
Lou, you recognize that guy?

309
01:07:32,701 --> 01:07:35,162
Yeah...Laurent.

310
01:08:46,444 --> 01:08:50,282
-What do you want?
-Want to go for a drive?

311
01:08:52,075 --> 01:08:53,410
I don't know.

312
01:08:55,161 --> 01:08:56,871
Get in, Baby!

313
01:09:19,021 --> 01:09:19,855
Come here!

314
01:10:02,982 --> 01:10:06,654
-Why don't you like me?
-I do like you.

315
01:10:07,488 --> 01:10:08,656
How much?

316
01:11:18,061 --> 01:11:19,438
Oh, Pete!

317
01:11:25,820 --> 01:11:26,571
Sheila!

318
01:11:29,198 --> 01:11:30,700
Sheila!

319
01:12:13,411 --> 01:12:15,288
What'd you change it for?

320
01:12:16,331 --> 01:12:17,499
I like that.

321
01:12:18,833 --> 01:12:20,168
Well, I don't.

322
01:12:30,096 --> 01:12:31,555
I like that!

323
01:13:06,800 --> 01:13:09,679
I'm leaving the Caddy like I told you.

324
01:13:09,928 --> 01:13:12,682
-Think you'll get a chance to give her the once-over today?
-Sure.

325
01:13:13,391 --> 01:13:16,311
Uh, you want to pick it up later on
...or in the morning?

326
01:13:17,020 --> 01:13:20,440
Well, if you think you can finish it,
I'll be back later today.

327
01:13:20,732 --> 01:13:24,402
-It'll be done.
-You're my man, Pete.

328
01:15:11,848 --> 01:15:15,686
Don't let that thing upset you.

329
01:15:42,255 --> 01:15:45,259
Holy smokes!

330
01:16:11,994 --> 01:16:13,080
Hey!

331
01:16:15,414 --> 01:16:17,042
I'm Alice Wakefield.

332
01:16:17,875 --> 01:16:19,293
Pete Dayton.

333
01:16:21,463 --> 01:16:24,716
-I was here earlier.
-Yeah, I remember.

334
01:16:31,891 --> 01:16:34,185
How'd you like to take me to dinner?

335
01:16:36,145 --> 01:16:37,938
I don't know.

336
01:16:43,903 --> 01:16:45,071
OK.

337
01:16:46,614 --> 01:16:48,826
Why don't I take you to dinner?

338
01:16:51,577 --> 01:16:55,666
Look, I don't think this is a very good idea.

339
01:17:03,299 --> 01:17:04,800
Do you have a phone?

340
01:17:07,970 --> 01:17:08,971
Yeah.

341
01:17:10,014 --> 01:17:12,141
It's right...lt's right there.

342
01:17:13,017 --> 01:17:15,770
I have to call myself another taxi.

343
01:17:26,364 --> 01:17:28,325
Hello? Van Nuys?

344
01:17:28,783 --> 01:17:32,620
Can I have the number for Vanguard Cab?

345
01:17:46,218 --> 01:17:50,348
Hello. Yes, I need a Taxi.
Arnie's garage, the corner of fifth...

346
01:17:51,224 --> 01:17:54,727
Hello. Yeah, we're not going to need that cab.
Thanks.

347
01:18:11,454 --> 01:18:14,457
Maybe we should just skip dinner.

348
01:18:48,033 --> 01:18:49,494
Take my clothes off!

349
01:18:53,747 --> 01:18:56,709
Fucker gets more pussy than a toilet seat.

350
01:19:47,721 --> 01:19:50,724
-I want more.
-Me, too.

351
01:19:55,646 --> 01:20:01,486
-Can I call you?
-Yeah. Call me at home. I'll give you the number.

352
01:20:07,950 --> 01:20:08,951
OK, Baby.

353
01:20:37,481 --> 01:20:42,696
Hey! Up here.Come on up, Baby.
I already got the room.

354
01:21:17,481 --> 01:21:20,277
I'll get it.
Hello?

355
01:21:21,193 --> 01:21:24,363
Meow, meow.It's me.

356
01:21:24,613 --> 01:21:26,116
Hey, Baby.

357
01:21:27,659 --> 01:21:30,370
I can't see you tonight.

358
01:21:33,832 --> 01:21:35,000
OK.

359
01:21:36,710 --> 01:21:39,004
I have to go somewhere with Mr. Eddy.

360
01:21:40,214 --> 01:21:41,590
Sure.

361
01:21:43,676 --> 01:21:48,806
I think he suspects something. We have to be careful.

362
01:21:51,893 --> 01:21:53,519
I miss you.

363
01:21:57,441 --> 01:21:58,358
Pete?

364
01:21:59,860 --> 01:22:01,235
Me, too.

365
01:22:03,112 --> 01:22:05,157
I'll call you again.

366
01:23:43,718 --> 01:23:45,346
What a fucking job.

367
01:23:46,180 --> 01:23:47,890
His or ours, Lou?

368
01:23:49,932 --> 01:23:51,018
Ours, Hank.

369
01:24:16,586 --> 01:24:19,130
-Hey.
-Sit down a minute!

370
01:24:20,090 --> 01:24:22,759
-What's up?
-Sit down!

371
01:24:25,428 --> 01:24:27,138
You don't look so good.

372
01:24:27,765 --> 01:24:31,017
No, I just...just have a headache.

373
01:24:32,478 --> 01:24:34,063
What's going on?

374
01:24:35,690 --> 01:24:37,565
The police called us.

375
01:24:39,819 --> 01:24:40,736
What'd they want?

376
01:24:40,987 --> 01:24:46,576
They want to know if we had a chance
to find out what happened to you the other night.

377
01:24:47,368 --> 01:24:50,455
And they want to know
if you remembered anything.

378
01:24:54,459 --> 01:24:56,837
But I don't remember anything.

379
01:24:58,505 --> 01:25:00,799
What'd you tell them?

380
01:25:06,346 --> 01:25:08,640
We're not going to say anything...

381
01:25:09,182 --> 01:25:11,476
...about that night to the police.

382
01:25:14,439 --> 01:25:16,816
We saw you that night, Pete.

383
01:25:20,278 --> 01:25:22,655
You came home with your friend Sheila.

384
01:25:24,908 --> 01:25:25,658
Sheila?

385
01:25:29,078 --> 01:25:31,623
Yeah. There was a man with you.

386
01:25:32,665 --> 01:25:36,503
What is this? I mean, why didn't you tell me anything?

387
01:25:41,800 --> 01:25:43,302
Who's the man?

388
01:25:44,928 --> 01:25:48,265
I've never seen him before in my life.

389
01:25:53,730 --> 01:25:55,440
What happened to me?

390
01:25:58,443 --> 01:26:01,696
Please, please, Dad, Tell me!

391
01:26:20,549 --> 01:26:21,717
Hey, Pete! Hey!

392
01:26:23,135 --> 01:26:24,220
Hey!

393
01:26:35,940 --> 01:26:36,691
Hey!

394
01:26:37,901 --> 01:26:38,735
Mr. Eddy.

395
01:26:49,538 --> 01:26:51,665
Hey, Pete!

396
01:26:52,582 --> 01:26:54,626
-How you doing?
-I'm OK.

397
01:26:54,836 --> 01:26:57,839
I'm sure you noticed that girl
who was with me the other day.

398
01:26:58,089 --> 01:27:01,259
Good looking Blonde.
She stayed in the car.

399
01:27:02,217 --> 01:27:07,015
Her name is Alice.I swear,
I love that girl to death.

400
01:27:09,643 --> 01:27:13,939
If I ever found out
somebody was making out with her...

401
01:27:16,358 --> 01:27:18,235
I'd take this

402
01:27:18,485 --> 01:27:22,656
and I'd shove it so far up his ass,
it would come out his mouth.

403
01:27:22,864 --> 01:27:26,285
-And then you know what I'd do?
-What?

404
01:27:27,162 --> 01:27:29,997
I'd blow his fucking brains out.

405
01:27:37,130 --> 01:27:41,176
Hey. You're looking good.

406
01:27:42,217 --> 01:27:44,094
What you been up to?

407
01:27:53,938 --> 01:27:55,023
I'll get it.

408
01:27:57,192 --> 01:27:57,860
Hello?

409
01:28:00,112 --> 01:28:06,618
Meet me at the Starlight Hotel
on Sycamore in 20 minutes.

410
01:28:12,166 --> 01:28:13,335
He'll kill us.

411
01:28:17,254 --> 01:28:18,673
Are you...

412
01:28:21,176 --> 01:28:22,968
...positive that he knows?

413
01:28:23,178 --> 01:28:26,181
I'm not positive, but he knows.

414
01:28:28,767 --> 01:28:34,606
So...What are we supposed to do?

415
01:28:37,651 --> 01:28:39,194
I don't know.

416
01:29:10,269 --> 01:29:15,608
If we could just get some money,
we could go away together.

417
01:29:20,196 --> 01:29:21,906
I know a guy.

418
01:29:24,451 --> 01:29:27,704
He pays girls to party with him.

419
01:29:28,412 --> 01:29:30,206
He's always got a lot of cash.

420
01:29:31,791 --> 01:29:33,668
He'd be easy to rob.

421
01:29:35,211 --> 01:29:39,842
Then we'd have the money.
We could go away.

422
01:29:41,844 --> 01:29:44,138
We could be together.

423
01:29:52,688 --> 01:29:54,482
Have you partied with him?

424
01:30:01,030 --> 01:30:02,448
Did you like it?

425
01:30:04,575 --> 01:30:05,660
No.

426
01:30:06,661 --> 01:30:08,080
It was part of the deal.

427
01:30:09,373 --> 01:30:10,457
What deal?

428
01:30:14,753 --> 01:30:17,298
-He works for Mr. Eddy.
-Yeah?

429
01:30:18,090 --> 01:30:21,259
-And what's he do?
-He makes films for Mr. Eddy.

430
01:30:22,094 --> 01:30:25,431
-Pornos?
-Yeah.

431
01:30:26,557 --> 01:30:29,101
How did you get in with these
fucking people, Alice?

432
01:30:29,310 --> 01:30:33,147
-Pete...
-No. Pete. I want to know how it happened.

433
01:30:37,902 --> 01:30:40,029
It was a long time ago.

434
01:30:41,781 --> 01:30:45,994
I met this guy at a place called Moke's.
We became friends.

435
01:30:47,663 --> 01:30:49,707
He told me about a job.

436
01:30:55,087 --> 01:30:56,422
In pornos?

437
01:30:59,634 --> 01:31:04,847
No. Just a job. I didn't know what.

438
01:31:07,892 --> 01:31:11,562
He made an appointment for me to see a man.

439
01:31:12,647 --> 01:31:14,232
I went to this place.

440
01:31:16,901 --> 01:31:18,778
They made me wait there forever.

441
01:31:21,282 --> 01:31:23,658
There was a guy guarding the door.

442
01:31:28,164 --> 01:31:31,042
In another room, there was this
other guy lifting weights.

443
01:31:38,257 --> 01:31:40,134
I started getting nervous.

444
01:31:42,553 --> 01:31:45,014
When it got dark...

445
01:31:46,182 --> 01:31:49,435
...they brought me into this other room.

446
01:34:13,545 --> 01:34:16,006
Why didn't you just leave?

447
01:34:24,264 --> 01:34:26,475
You liked it, huh?

448
01:34:30,105 --> 01:34:35,568
If you want me to go away,
I'll go away.

449
01:34:41,116 --> 01:34:43,326
I don't want you to go away.

450
01:34:44,160 --> 01:34:45,620
I don't want you to go away.

451
01:34:56,381 --> 01:34:58,342
I love you, Alice.

452
01:35:02,555 --> 01:35:05,099
-Should I call Andy?
-Andy?

453
01:35:07,018 --> 01:35:10,021
That's his name, Andy.
Our ticket out of here.

454
01:35:11,022 --> 01:35:13,774
Yeah. Call him.

455
01:35:16,652 --> 01:35:18,864
I'll set it up for tomorrow night.

456
01:35:19,531 --> 01:35:21,908
You meet me at his place at 11:00.

457
01:35:22,325 --> 01:35:27,122
Don't drive, take the bus.
Make sure no one follows you.

458
01:35:27,831 --> 01:35:34,004
His address is easy to remember.
It's 2224 Deep Dell Place.

459
01:35:34,213 --> 01:35:37,216
It's a white Stucco job
on the south side of the street.

460
01:35:37,467 --> 01:35:42,263
I'll be upstairs with Andy.
The back door will be open.

461
01:35:42,638 --> 01:35:46,392
Go through the kitchen into the living room.
There's a bar there.

462
01:35:46,727 --> 01:35:51,023
At 11:15, I'll send Andy down to fix me a drink.

463
01:35:51,230 --> 01:35:54,984
And when he does,
you crack him on the head, OK?

464
01:35:58,989 --> 01:35:59,655
OK.

465
01:36:29,813 --> 01:36:32,524
-You're fucking somebody else, aren't you?
-Sheila!

466
01:36:32,732 --> 01:36:35,110
You fuck me whenever you want.
Sheila! Sheila, stop it!

467
01:36:35,319 --> 01:36:37,529
-You don't call. Who is she?
-Stop it!

468
01:36:37,738 --> 01:36:40,699
-What's the bitch's name?
-I'm sorry.
-Oh, you're sorry?

469
01:36:40,908 --> 01:36:42,535
-Go home!
-You're sorry?
-Sheila, stop it!

470
01:36:42,785 --> 01:36:44,829
You're sorry, you piece of shit!

471
01:36:45,037 --> 01:36:46,497
-You're sorry?
-Go home!

472
01:36:46,706 --> 01:36:48,833
-Fuck you!..Fuck you!
-Sheila, stop, Sheila!
-Fuck you!..Fuck you!

473
01:36:49,125 --> 01:36:51,669
-Fuck you!..Fuck you!
-Sheila...Sheila.

474
01:36:52,003 --> 01:36:56,215
Sheila! Let's both go in and talk about this quietly.

475
01:36:56,632 --> 01:36:58,217
Sheila, come on.

476
01:37:00,679 --> 01:37:02,973
-You are different.
-Sheila!

477
01:37:03,306 --> 01:37:05,600
Tell him!..Tell him!

478
01:37:05,809 --> 01:37:08,645
Sheila, don't. Don't.

479
01:37:10,105 --> 01:37:17,071
I don't care anymore anyway. I'm sorry, Mr. Dayton.

480
01:37:17,655 --> 01:37:23,744
I won't bother you, or any member
of your family ever again.

481
01:37:38,426 --> 01:37:39,510
Pete?

482
01:37:42,555 --> 01:37:44,183
There's a man on the phone.

483
01:37:45,476 --> 01:37:48,020
He's called a couple of times tonight.

484
01:37:48,729 --> 01:37:49,813
Who is it?

485
01:37:51,940 --> 01:37:53,568
He won't give his name.

486
01:38:12,797 --> 01:38:13,963
Hello?

487
01:38:14,714 --> 01:38:17,258
Hey, Pete, how you doing?

488
01:38:19,594 --> 01:38:23,891
-Who is this?
-You know who it is.

489
01:38:27,395 --> 01:38:28,771
Mr. Eddy?

490
01:38:29,729 --> 01:38:32,734
Yeah. How you doing, Pete?

491
01:38:33,526 --> 01:38:37,447
-OK.
-You're doing OK? That's good, Pete.

492
01:38:39,449 --> 01:38:45,789
-Look, it's late Mr. Eddy, I...
-I'm really glad to know you're doing OK.

493
01:38:47,999 --> 01:38:54,005
-You're sure you're OK? Everything all right?
-Yeah.

494
01:38:56,049 --> 01:38:59,469
I'm really glad to know you're doing good, Pete. Hey!

495
01:39:00,595 --> 01:39:02,557
I want you to talk to a friend of mine.

496
01:39:08,229 --> 01:39:10,773
We've met before, haven't we?

497
01:39:12,233 --> 01:39:13,651
I don't think so.

498
01:39:15,862 --> 01:39:17,906
Where is it you think we've met?

499
01:39:18,239 --> 01:39:22,160
At your house. Don't you remember?

500
01:39:25,914 --> 01:39:29,835
No. No, I don't.

501
01:39:31,295 --> 01:39:34,089
In the East, the far East...

502
01:39:34,298 --> 01:39:39,846
...when a person is sentenced to death,
they're sent to a place where they can't escape.

503
01:39:40,471 --> 01:39:47,604
Never knowing when an executioner may step up behind them
and fire a bullet into the back of their head.

504
01:39:48,937 --> 01:39:50,732
What's going on?

505
01:39:51,399 --> 01:39:54,027
It's been a pleasure talking to you.

506
01:39:54,986 --> 01:40:00,450
Pete, I just wanted to jump on and
tell you that I'm really glad you're doing OK.

507
01:43:16,239 --> 01:43:17,406
You got him.

508
01:43:18,616 --> 01:43:19,784
Alice!

509
01:44:17,094 --> 01:44:17,929
Wow!

510
01:44:21,724 --> 01:44:23,434
We killed him.

511
01:44:32,528 --> 01:44:34,655
You killed him.

512
01:44:37,782 --> 01:44:38,450
Alice?

513
01:44:44,248 --> 01:44:45,499
Alice?

514
01:44:46,959 --> 01:44:48,502
What do we do?

515
01:44:49,921 --> 01:44:51,631
What do we do?

516
01:44:52,924 --> 01:44:54,634
We have to get the stuff.

517
01:44:57,220 --> 01:44:58,763
We have to get out of here.

518
01:45:14,487 --> 01:45:15,655
Ah, fuck!

519
01:45:52,110 --> 01:45:53,111
Is that you?

520
01:45:54,821 --> 01:45:56,324
Are both of them you?

521
01:46:06,000 --> 01:46:07,335
That's me.

522
01:46:10,088 --> 01:46:12,549
Honey, are you all right?

523
01:46:17,596 --> 01:46:19,180
Where's the bathroom?

524
01:46:20,640 --> 01:46:23,519
It's upstairs, down the hall.

525
01:47:21,079 --> 01:47:22,873
Did you want to talk to me?

526
01:47:25,834 --> 01:47:28,837
Did you want to ask me, why?

527
01:48:26,773 --> 01:48:27,941
What's the matter?

528
01:48:31,694 --> 01:48:33,196
Don't you trust me, Pete?

529
01:48:42,080 --> 01:48:44,208
Stick this in your pants.

530
01:48:51,548 --> 01:48:53,844
I know a fence.

531
01:48:56,512 --> 01:49:01,308
He'll give us money, get us passports
in exchange for all this shit

532
01:49:02,226 --> 01:49:03,477
and Andy's car...

533
01:49:04,937 --> 01:49:07,691
And then we can go anywhere.

534
01:49:23,082 --> 01:49:24,416
Come on, Baby!

535
01:49:40,933 --> 01:49:42,186
Pete, you drive!

536
01:49:58,035 --> 01:50:00,579
Where the fuck are we going, Alice?

537
01:50:03,207 --> 01:50:05,251
Where the fuck are we going?

538
01:50:08,088 --> 01:50:10,298
We have to go to the desert, Baby.

539
01:50:12,842 --> 01:50:15,303
The fence I told you about...

540
01:50:19,307 --> 01:50:20,850
He's at his cabin.

541
01:51:35,179 --> 01:51:36,180
Come on!

542
01:52:24,188 --> 01:52:26,065
We'll have to wait.

543
01:52:47,087 --> 01:52:49,798
Why me, Alice? Why choose me?

544
01:52:57,598 --> 01:52:59,809
You still want me, don't you, Pete?

545
01:53:03,646 --> 01:53:05,774
More than ever.

546
01:55:07,109 --> 01:55:08,277
I want you.

547
01:55:17,411 --> 01:55:18,747
I want you.

548
01:55:37,307 --> 01:55:38,475
I want you.

549
01:55:40,477 --> 01:55:41,645
I want you.

550
01:56:00,998 --> 01:56:05,170
You'll never have me.

551
01:57:09,987 --> 01:57:10,989
Here I am.

552
01:58:02,710 --> 01:58:09,050
-Where's Alice?
-Alice who? Her name is Renee.

553
01:58:09,300 --> 01:58:13,054
If she told you her name is Alice, she's lying.

554
01:58:17,183 --> 01:58:21,437
And your name?
What the fuck is your name?!

555
02:01:44,275 --> 02:01:44,942
Renee?

556
02:01:55,369 --> 02:01:57,456
You're coming with me.

557
02:04:00,293 --> 02:04:03,004
What do you guys want?

558
02:05:18,082 --> 02:05:20,043
Now you can hand it back!

559
02:05:29,846 --> 02:05:32,055
You and me, Mister...

560
02:05:35,225 --> 02:05:39,272
We can really outugly them sumbitches...

561
02:05:40,230 --> 02:05:41,940
Can't we?

562
02:06:37,959 --> 02:06:40,419
Ed...Take a look at this!

563
02:06:47,385 --> 02:06:49,344
Yeah. That's her all right.

564
02:06:50,763 --> 02:06:54,267
That's Fred Madison's wife with Dick Laurent.

565
02:06:54,684 --> 02:06:56,645
And Mr. Dent-Head over there.

566
02:06:58,271 --> 02:07:00,815
We got Pete Dayton's prints all over this place.

567
02:07:01,775 --> 02:07:03,568
You know what I think, Ed?

568
02:07:05,403 --> 02:07:07,364
What is it, AI? What do you think?

569
02:07:08,782 --> 02:07:11,535
I think there's no such thing as a bad coincidence.

570
02:08:15,060 --> 02:08:16,603
Dick Laurent is dead.

