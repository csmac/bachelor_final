1
00:00:00,807 --> 00:00:03,557
<i>Accessing archive data...</i>

2
00:00:03,961 --> 00:00:07,578
<i>The Renegade continues
to inspire the public...</i>

3
00:00:08,169 --> 00:00:10,270
Standing up to Tesler
showed those programs

4
00:00:10,304 --> 00:00:13,273
that they don't have
to sit idly by.

5
00:00:13,307 --> 00:00:16,109
They can choose to fight.

6
00:00:16,143 --> 00:00:17,544
<i>...while Beck meets someone else</i>

7
00:00:17,578 --> 00:00:20,313
<i>who knows that Tron is alive.</i>

8
00:00:20,348 --> 00:00:23,483
You're not the first program
Tron trained to fight back.

9
00:00:23,518 --> 00:00:25,485
I am.

10
00:00:25,520 --> 00:00:29,556
Surprise.
I'm the first Renegade.

11
00:00:30,967 --> 00:00:34,483
<b>1x14 - Tagged</b>

12
00:01:26,314 --> 00:01:31,451
<i>Halt, program! 
Violation penal code 113.</i>

13
00:01:48,169 --> 00:01:50,370
- There he is!
- No, over here!

14
00:01:50,904 --> 00:01:52,405
Split up.

15
00:01:59,513 --> 00:02:03,149
You're fast, Renegade.
But not fast enough.

16
00:02:05,453 --> 00:02:07,120
Wait, you're not...

17
00:02:07,154 --> 00:02:08,188
[ groans ]

18
00:02:23,516 --> 00:02:26,410
<i><font color="cyan"> sync & correction by f1nc0
~ addic7ed.com ~ </font></i>

19
00:02:43,671 --> 00:02:47,674
- My fan club strikes again.
- You mean my fan club. 

20
00:02:47,708 --> 00:02:51,477
Hey, you made a joke.
That's progress.

21
00:02:51,512 --> 00:02:52,712
What did you find out?

22
00:02:52,746 --> 00:02:55,481
Not much. The soldiers
still think it's me.

23
00:02:55,516 --> 00:02:57,717
They haven't even figured out
that there's more than one.

24
00:02:57,751 --> 00:02:59,819
So when do we bring them in?

25
00:02:59,853 --> 00:03:01,621
We don't.

26
00:03:01,655 --> 00:03:04,290
- Not yet.
- What do you mean?

27
00:03:04,325 --> 00:03:07,360
This is what we've been
waiting for... recruits.

28
00:03:07,394 --> 00:03:08,594
We have to build ranks eventually,

29
00:03:08,629 --> 00:03:10,697
and they obviously
believe in the cause.

30
00:03:10,731 --> 00:03:14,500
There's more to the uprising
than acts of vandalism.

31
00:03:14,535 --> 00:03:16,002
You know that.

32
00:03:16,387 --> 00:03:18,188
Well, you do now.

33
00:03:18,222 --> 00:03:20,523
Yeah, because you gave me
the chance to do something,

34
00:03:20,558 --> 00:03:23,259
make a difference.
I'm not sure if you noticed,

35
00:03:23,294 --> 00:03:24,494
but I'm kinda on my own out there.

36
00:03:24,528 --> 00:03:29,199
- I'm just asking you to wait.
- I'm done waiting.

37
00:03:29,233 --> 00:03:30,533
As long as I'm wearing this suit,

38
00:03:30,568 --> 00:03:32,702
you have to start
letting me make decisions.

39
00:03:34,155 --> 00:03:37,123
These programs need to know
what they're fighting for.

40
00:03:37,158 --> 00:03:40,026
They need to be willing to die for it.

41
00:03:40,061 --> 00:03:43,029
You want to make a decision,
make one.

42
00:03:43,064 --> 00:03:45,432
But be prepared to deal
with the consequences.

43
00:03:48,069 --> 00:03:51,404
And you're walking away.
Big surprise.

44
00:04:04,385 --> 00:04:06,086
Where is it?

45
00:04:06,120 --> 00:04:09,089
<i>- Identify.
- I told you, I didn't see anything.</i>

46
00:04:09,123 --> 00:04:12,092
I want every program
on this train detained.

47
00:04:12,126 --> 00:04:14,260
No one leaves until
I've questioned them.

48
00:04:14,295 --> 00:04:15,462
Yes, sir.

49
00:04:22,103 --> 00:04:23,236
Well?

50
00:04:23,270 --> 00:04:25,305
Open it!

51
00:04:27,341 --> 00:04:29,476
I got it!

52
00:04:29,510 --> 00:04:31,277
[ trilling ]
[ gasps ]

53
00:04:34,115 --> 00:04:36,449
[ gasps ]

54
00:04:36,484 --> 00:04:38,218
Get down!

55
00:04:38,252 --> 00:04:40,053
[ grunts ]

56
00:04:45,459 --> 00:04:47,193
[ screams ]

57
00:04:59,073 --> 00:05:00,039
[ screams ]

58
00:05:04,345 --> 00:05:06,212
Triumph!

59
00:05:06,247 --> 00:05:08,448
I wish I could've seen their
faces when the countdown hit zero.

60
00:05:08,482 --> 00:05:11,151
I wish I could've seen
my masterpiece.

61
00:05:11,185 --> 00:05:12,318
[ clears throat ]

62
00:05:12,353 --> 00:05:14,087
Our masterpiece.

63
00:05:14,121 --> 00:05:16,422
Next time let's hang
out at the scene longer.

64
00:05:16,457 --> 00:05:18,258
It's more fun when they chase us.

65
00:05:18,292 --> 00:05:19,425
That's how we get caught.

66
00:05:19,460 --> 00:05:21,427
If we wanna
keep this operation running,

67
00:05:21,462 --> 00:05:23,196
we stick to my plan.

68
00:05:25,399 --> 00:05:26,432
[ gasps ]

69
00:05:26,467 --> 00:05:28,401
<i>- Now what?
- Run!</i>

70
00:05:34,074 --> 00:05:37,277
[ all groan ]

71
00:05:47,321 --> 00:05:49,122
Huh?

72
00:05:49,156 --> 00:05:52,425
<i>Do you know how fast you could've
been derezzed for what you've done?</i>

73
00:05:52,459 --> 00:05:56,262
<i>Vandalizing city property,
trespassing,</i>

74
00:05:56,297 --> 00:05:59,999
<i>instigating disobedience
by spreading lies.</i>

75
00:06:00,034 --> 00:06:03,036
- They're not lies.
- Hey, be quiet.

76
00:06:03,070 --> 00:06:05,104
And it's not vandalism, it's free code.

77
00:06:05,139 --> 00:06:08,374
For the record, I have no idea

78
00:06:08,409 --> 00:06:11,911
what you or these two
are talking about.

79
00:06:11,946 --> 00:06:14,080
Hey, hey!
Whoa!

80
00:06:17,084 --> 00:06:19,953
Oh, that.

81
00:06:19,987 --> 00:06:21,921
Okay.

82
00:06:21,956 --> 00:06:24,357
So you believe that Tron's alive?

83
00:06:24,391 --> 00:06:27,126
- I do.
- And you?

84
00:06:27,161 --> 00:06:32,265
- I let my work speak for itself.
- Then so be it.

85
00:06:32,299 --> 00:06:33,933
[ screams ]
Wait!

86
00:06:33,968 --> 00:06:35,235
No, no, no!
You can't!

87
00:06:44,078 --> 00:06:48,114
- Tron?
- So you're not still gonna derez us?

88
00:06:48,148 --> 00:06:51,150
Duh, he's here
to help us. Right?

89
00:06:52,186 --> 00:06:53,186
[ all grunt ]

90
00:06:53,220 --> 00:06:57,357
Actually, you're going to help me.
But first I have to trust you.

91
00:07:03,264 --> 00:07:07,934
Name's Moog.
And this here's Rasket.

92
00:07:07,968 --> 00:07:10,236
It's pronounced "Ras-kay."

93
00:07:10,271 --> 00:07:15,041
And finally, the heart
and soul of the Jolly Tricksters.

94
00:07:15,075 --> 00:07:16,175
Mara.

95
00:07:17,945 --> 00:07:21,915
Nice to see you again, Tron.

96
00:07:28,312 --> 00:07:31,280
Wait.
You two know each other?

97
00:07:31,315 --> 00:07:32,381
- No.
- Yes.

98
00:07:32,416 --> 00:07:36,152
He saved my life.
More than once.

99
00:07:36,186 --> 00:07:39,055
Well, of course he did.
He's impersonating Tron.

100
00:07:39,089 --> 00:07:41,257
[ groans ]
I mean, he is Tron.

101
00:07:41,291 --> 00:07:46,262
We're both... all three of us...
huge fans of your work.

102
00:07:46,296 --> 00:07:49,098
Uh, thanks.

103
00:07:49,133 --> 00:07:51,434
I've brought all of
you here for a reason.

104
00:07:51,468 --> 00:07:53,102
There's a revolution coming,

105
00:07:53,137 --> 00:07:55,271
and I need recruits to fight by my side.

106
00:07:55,305 --> 00:07:58,274
So...
so what do you say?

107
00:07:58,308 --> 00:07:59,442
Are you up for the challenge?

108
00:07:59,476 --> 00:08:02,245
- Absolutely!
- Of course!

109
00:08:02,279 --> 00:08:06,249
That was easy.
But our task ahead won't be.

110
00:08:06,283 --> 00:08:08,835
I just wanna say that I've been waiting

111
00:08:08,836 --> 00:08:11,387
for an opportunity like
this for a while now,

112
00:08:11,421 --> 00:08:16,125
a chance to show my commitment
to the revolution, to you.

113
00:08:16,160 --> 00:08:20,062
That's very flattering, really.

114
00:08:20,097 --> 00:08:22,131
Look, you've all been through a lot.

115
00:08:22,166 --> 00:08:24,133
Let's call it for now,
then meet back here

116
00:08:24,168 --> 00:08:27,436
at the start of next cycle
for further instructions.

117
00:08:29,339 --> 00:08:31,207
Class dismissed.

118
00:08:31,241 --> 00:08:33,376
Let's go, guys.
Let's bounce.

119
00:08:35,179 --> 00:08:40,049
Whatever you want us to do, I want
you to know that I'm not afraid.

120
00:08:44,054 --> 00:08:48,457
<i>Remember when you first approached me?
I said I wasn't afraid.</i>

121
00:08:48,492 --> 00:08:51,460
But how did you really
know I was the right choice?

122
00:08:51,495 --> 00:08:55,231
I didn't.
You kept coming back.

123
00:08:55,265 --> 00:08:59,235
The more I pushed,
the harder you fought.

124
00:08:59,269 --> 00:09:01,938
Was I your first choice?

125
00:09:01,972 --> 00:09:04,073
Yes.

126
00:09:04,107 --> 00:09:07,210
Really?
There was no one before me?

127
00:09:10,347 --> 00:09:13,983
Why do you ask?

128
00:09:14,017 --> 00:09:16,319
Is there something you'd
like to tell me, Beck?

129
00:09:16,353 --> 00:09:19,305
No. I was just wondering if you were

130
00:09:19,306 --> 00:09:22,258
afraid you made the wrong choice...

131
00:09:22,292 --> 00:09:24,093
with me.

132
00:09:24,127 --> 00:09:26,362
You're forgetting, when I found you,

133
00:09:26,396 --> 00:09:30,199
you were already waging your
own war against the occupation.

134
00:09:30,234 --> 00:09:32,401
I just steered you down the right path.

135
00:09:35,305 --> 00:09:38,274
<i>The right path...
what is it?</i>

136
00:09:38,308 --> 00:09:41,010
<i>Defacing trains and billboards?</i>

137
00:09:41,044 --> 00:09:44,247
<i>Or striking a significant blow
against the enemy's stronghold?</i>

138
00:09:44,281 --> 00:09:47,083
Whoa.
That's Tesler's ship.

139
00:09:47,117 --> 00:09:49,185
And you three are going
to help me infiltrate it.

140
00:09:49,219 --> 00:09:51,153
On that ship is a data key.

141
00:09:51,188 --> 00:09:54,123
Tesler's ship is the
biggest canvas in Argon.

142
00:09:54,157 --> 00:09:56,025
We hit that, we'll make history.

143
00:09:56,059 --> 00:09:59,395
Whoa, whoa, whoa. Nobody's
hitting Tesler's ship but me.

144
00:09:59,429 --> 00:10:02,064
In fact, you won't be anywhere near it.

145
00:10:02,099 --> 00:10:03,399
Every fifth cycle,

146
00:10:03,433 --> 00:10:05,368
the ship docks with
the Argon refueling tower.

147
00:10:05,402 --> 00:10:07,136
That's where you three come in.

148
00:10:07,170 --> 00:10:08,371
While I circle by light jet,

149
00:10:08,405 --> 00:10:10,139
you're going to do
what you're good at...

150
00:10:10,173 --> 00:10:12,208
bombard the area with your free code.

151
00:10:12,242 --> 00:10:14,343
In order to draw the guards away,

152
00:10:14,378 --> 00:10:17,179
that way you can sneak
onto the ship undetected.

153
00:10:17,214 --> 00:10:19,415
Do you like
how I put that together?

154
00:10:19,449 --> 00:10:21,350
So we're just your distraction.

155
00:10:21,385 --> 00:10:25,454
Where's the fun in that?
We want in on the thrills.

156
00:10:25,489 --> 00:10:28,257
This isn't about thrills.
I'm trying to gather intel.

157
00:10:28,292 --> 00:10:31,994
And if you can't understand that,
then maybe I've made a mistake.

158
00:10:32,029 --> 00:10:35,331
Tron, wait.
I can't believe what I'm hearing.

159
00:10:35,365 --> 00:10:38,000
Do you know who we're talking to?

160
00:10:38,035 --> 00:10:41,304
This is the program who
inspired you to create,

161
00:10:41,338 --> 00:10:45,141
and inspired you to help
Rasket realize his mission.

162
00:10:45,175 --> 00:10:46,342
Uh, "Ras-kay."

163
00:10:46,376 --> 00:10:48,077
[ sighs ]
Whatever.

164
00:10:48,111 --> 00:10:51,247
He's risking his life out
there and he needs our help.

165
00:10:51,281 --> 00:10:55,084
I don't know about you guys,
but I'm giving it to him.

166
00:10:58,255 --> 00:11:01,991
Us, too.
When do we start?

167
00:11:19,142 --> 00:11:22,111
Right on time.
Be careful.

168
00:11:32,422 --> 00:11:34,256
<i>Security zone compromised.</i>

169
00:11:34,291 --> 00:11:36,325
Alert commander Paige.

170
00:11:55,045 --> 00:11:59,248
<i>Stand clear of docking.
Prepare for refuel.</i>

171
00:12:05,956 --> 00:12:10,259
<i>Perpetrators at large.
Report or be derezzed.</i>

172
00:12:12,129 --> 00:12:14,096
Hey, where are you two going?

173
00:12:14,131 --> 00:12:16,966
<i>The plan is to meet
Tron at the warehouse.</i>

174
00:12:17,000 --> 00:12:19,235
Change of plans, baby.

175
00:12:41,123 --> 00:12:45,928
<i>All personnel remain in docking area
until refueling is complete.</i>

176
00:13:12,155 --> 00:13:13,956
You're alone.
[ gasps ]

177
00:13:13,990 --> 00:13:16,992
- Why?
- I tried to stop them.

178
00:13:17,027 --> 00:13:19,929
Stop them?
Mara, where are they?

179
00:13:19,963 --> 00:13:22,198
[ sighs ]

180
00:13:23,366 --> 00:13:24,934
<i>There.</i>

181
00:13:24,968 --> 00:13:26,202
What were they thinking?

182
00:13:26,236 --> 00:13:29,271
They weren't.
I usually handle that part.

183
00:13:29,306 --> 00:13:32,942
This is as far as you go.
I'll take care of the rest.

184
00:13:32,976 --> 00:13:35,244
Uh, try again.

185
00:13:35,278 --> 00:13:37,146
I'm the one who talked
them into joining you.

186
00:13:37,180 --> 00:13:38,981
Besides, I know their plan.

187
00:13:39,015 --> 00:13:41,317
Without me, you'll
never find them in time.

188
00:13:41,351 --> 00:13:44,119
And I won't take no
for an answer.

189
00:13:44,154 --> 00:13:47,289
[ sighs ]
I was afraid of that.

190
00:13:51,394 --> 00:13:54,964
[ laughs ]
Zed is never going to believe this!

191
00:13:54,998 --> 00:13:56,932
Hmm.

192
00:13:58,034 --> 00:13:59,969
No sign of the Renegade.

193
00:14:00,003 --> 00:14:02,171
Or whoever struck by
the refueling tower.

194
00:14:02,205 --> 00:14:04,273
Unacceptable. Keep looking.

195
00:14:04,307 --> 00:14:07,009
This "Tron lives" blasphemy must stop.

196
00:14:07,043 --> 00:14:10,913
Then you definitely
shouldn't look outside, sir.

197
00:14:10,947 --> 00:14:11,997
[ growls ]

198
00:14:11,998 --> 00:14:15,150
Where is that coming from?

199
00:14:15,185 --> 00:14:20,322
Far as I can tell, from us.

200
00:14:20,357 --> 00:14:23,158
[ alarm sounds ]

201
00:14:23,193 --> 00:14:25,160
Perhaps we better abscond.

202
00:14:25,195 --> 00:14:28,330
Aw, I was just getting
in the zone.

203
00:14:30,400 --> 00:14:32,368
<i>Security alert.</i>

204
00:14:33,937 --> 00:14:35,137
<i>Security alert.</i>

205
00:14:36,940 --> 00:14:38,307
<i>Security alert.</i>

206
00:14:39,342 --> 00:14:41,176
<i>Security alert.</i>

207
00:14:41,211 --> 00:14:42,244
[ grunts ]

208
00:14:44,948 --> 00:14:47,150
[ laughs ]

209
00:14:48,184 --> 00:14:50,319
What are you two grinning for?

210
00:14:50,353 --> 00:14:52,221
[ both grunt ]

211
00:15:00,397 --> 00:15:03,165
I'm disappointed that
you're not the Renegade, 

212
00:15:03,199 --> 00:15:06,135
but that doesn't mean we
still can't have fun together.

213
00:15:15,211 --> 00:15:18,080
Moog, what are we walking on?

214
00:15:18,114 --> 00:15:22,117
I think you mean...
"who" are we walking on.

215
00:15:26,289 --> 00:15:28,223
- Did he scream?
- Yes, sir.

216
00:15:33,963 --> 00:15:38,033
That's the problem with these
programs, they can't hold it together.

217
00:15:40,336 --> 00:15:41,303
[ grunts ]

218
00:15:43,173 --> 00:15:45,340
[ grunts ]

219
00:15:48,878 --> 00:15:51,213
A little privacy, please.

220
00:15:55,118 --> 00:15:57,086
[ alarm sounds ]

221
00:15:57,120 --> 00:15:59,088
<i>Security breach contained.</i>

222
00:15:59,122 --> 00:16:04,193
<i>General Tesler, please report to
prison level for interrogation.</i>

223
00:16:04,227 --> 00:16:07,029
<i>While you're able to
talk, why not tell me</i>

224
00:16:07,030 --> 00:16:09,832
<i>the location of your
leader, the Renegade!</i>

225
00:16:09,866 --> 00:16:12,935
Look, we don't know!
You have to believe us, okay?!

226
00:16:12,969 --> 00:16:15,037
Where's the fun in that?

227
00:16:15,071 --> 00:16:19,908
[ screaming ]

228
00:16:22,145 --> 00:16:26,215
Please! We're sorry!
We're just artists!

229
00:16:26,249 --> 00:16:30,085
Oh, so you work with your hands.

230
00:16:30,120 --> 00:16:34,256
[ screaming ]

231
00:16:34,290 --> 00:16:37,926
[ grunting ]

232
00:16:39,029 --> 00:16:41,096
[ screams ]

233
00:16:41,131 --> 00:16:42,097
[ groans ]

234
00:16:47,237 --> 00:16:48,270
Get him!

235
00:16:48,304 --> 00:16:51,206
[ alarm sounds ]

236
00:16:53,276 --> 00:16:57,079
They're sealing us in.
We'll be trapped.

237
00:16:57,113 --> 00:16:58,313
[ gasps ]

238
00:17:00,183 --> 00:17:04,787
I'll handle the Renegade.
You get the others.

239
00:17:04,821 --> 00:17:09,024
You three get that gate open...
while I deal with them.

240
00:17:09,059 --> 00:17:15,898
[ grunting ]

241
00:17:18,868 --> 00:17:23,072
- You don't give up, do you?
- No, and I never will.

242
00:17:26,976 --> 00:17:29,111
You're gonna wanna hurry, Mara.

243
00:17:29,145 --> 00:17:31,113
I can't!
There's not enough time!

244
00:17:31,147 --> 00:17:34,283
Then what are we waiting for?!
Catch!

245
00:17:34,317 --> 00:17:36,151
No, wait!
What about Tron?

246
00:17:36,186 --> 00:17:40,923
What about him?
He's fine. He's Tron.

247
00:17:40,957 --> 00:17:45,260
[ grunting ]

248
00:17:54,304 --> 00:17:57,172
We need to go now!

249
00:17:57,207 --> 00:17:58,173
[ all gasp ]

250
00:18:01,177 --> 00:18:04,847
I was wrong! It's not more
fun when they chase us!

251
00:18:04,881 --> 00:18:07,082
I thought you were
all about the thrills.

252
00:18:09,885 --> 00:18:11,921
Huh?

253
00:18:12,956 --> 00:18:14,123
[ grunts ]

254
00:18:16,159 --> 00:18:18,026
[ grunts ]

255
00:18:31,875 --> 00:18:38,147
[ grunting ]

256
00:18:38,181 --> 00:18:39,882
[ both scream ]

257
00:18:49,259 --> 00:18:51,059
[ panting ]

258
00:19:06,876 --> 00:19:08,911
[ cheering ]

259
00:19:12,148 --> 00:19:14,116
<i>Mara, go!</i>

260
00:19:16,219 --> 00:19:18,787
[ groans ]

261
00:19:25,061 --> 00:19:26,028
[ grunts ]

262
00:19:38,107 --> 00:19:42,244
Huh?
Oh.

263
00:19:48,284 --> 00:19:52,054
- We messed up.
- Yeah, we messed up bad.

264
00:19:52,088 --> 00:19:53,989
And thanks
for coming back for us.

265
00:19:54,023 --> 00:19:56,992
- It's not just me you have to thank.
- You're welcome.

266
00:19:57,026 --> 00:20:00,929
That right path you mentioned...
it's out there, isn't it?

267
00:20:00,964 --> 00:20:03,932
It's Argon.
That's what you've been fighting for.

268
00:20:03,967 --> 00:20:05,267
It's more than Argon.

269
00:20:05,301 --> 00:20:07,836
You're taking the fight
all the way to Clu.

270
00:20:07,871 --> 00:20:10,272
You're fighting so this stops.

271
00:20:10,306 --> 00:20:14,877
Then join me. Let me put
your talents to good use.

272
00:20:14,911 --> 00:20:18,146
The uprising needs you.
And so do I.

273
00:20:18,181 --> 00:20:20,148
We're not good enough.
No way.

274
00:20:20,183 --> 00:20:22,050
We barely survived up there.

275
00:20:22,085 --> 00:20:24,920
I don't think we'll be
as lucky a second time.

276
00:20:26,856 --> 00:20:30,893
Your cause is right,
but we're not your army.

277
00:20:30,927 --> 00:20:37,065
Hey, it's a good thing Tron lives.
Argon needs you.

278
00:20:41,170 --> 00:20:46,008
I'm still in. No matter the
risks, I'm ready for this.

279
00:20:48,878 --> 00:20:51,179
[ sighs ]
I'm not.

280
00:20:51,214 --> 00:20:53,181
What does that mean?

281
00:20:53,216 --> 00:20:56,251
It means without your friends
you're of no help to me.

282
00:20:56,286 --> 00:20:58,987
<i>Go home.</i>

283
00:21:03,159 --> 00:21:06,962
It was the only way
to get her to leave.

284
00:21:06,996 --> 00:21:10,999
She was the one true
believer in the cause.

285
00:21:11,034 --> 00:21:14,269
But I wasn't prepared
for the consequences.

286
00:21:14,304 --> 00:21:17,239
Then you made the right decision.

287
00:21:17,273 --> 00:21:21,843
<i>If she's really willing to
risk everything for the cause,</i>

288
00:21:21,878 --> 00:21:25,981
<i>she'll still be there...
when you're ready.</i>

289
00:21:30,779 --> 00:21:33,978
<i><font color="cyan"> sync & correction by f1nc0
~ addic7ed.com ~ </font></i>

