1
00:00:02,553 --> 00:00:04,245
Max, look out,
you're so tired

2
00:00:04,246 --> 00:00:05,691
you're spilling batter
all over yourself.

3
00:00:05,692 --> 00:00:06,914
Ugh.

4
00:00:06,915 --> 00:00:08,249
I knew it was only
a matter of time

5
00:00:08,250 --> 00:00:11,531
before I became
a battered woman.

6
00:00:11,532 --> 00:00:13,658
This is
a huge disappointment.

7
00:00:13,659 --> 00:00:15,618
I heard that.

8
00:00:15,619 --> 00:00:17,787
You don't even know
what I'm referring to.

9
00:00:17,788 --> 00:00:20,998
Oh, I just assumed we were
talking about everything.

10
00:00:20,999 --> 00:00:22,680
I'm referring
to our expensive

11
00:00:22,681 --> 00:00:24,098
new cupcake website.

12
00:00:24,099 --> 00:00:25,503
Not one job offer.

13
00:00:25,504 --> 00:00:27,819
You want a website that will make you
feel better about yourself?

14
00:00:27,820 --> 00:00:30,600
Go to
models-falling-down.com

15
00:00:32,259 --> 00:00:35,763
number one:
Klum goes boom.

16
00:00:35,764 --> 00:00:37,598
[Knocking at door]

17
00:00:37,599 --> 00:00:39,851
It's 3:00 A.M.
Who would be coming over now?

18
00:00:39,852 --> 00:00:42,149
I don't know,
my dealer, my other dealer?

19
00:00:42,150 --> 00:00:45,203
That guy who always asks
if I know where my dealer is?

20
00:00:47,190 --> 00:00:48,790
- Who is it?
- You can't handle

21
00:00:48,791 --> 00:00:50,570
what's on the other
side of that door.

22
00:00:50,571 --> 00:00:52,527
Yesterday,
I saw a guy on a stoop

23
00:00:52,528 --> 00:00:53,711
frenching his cat.

24
00:00:53,712 --> 00:00:56,097
I can handle anything.

25
00:01:01,019 --> 00:01:03,437
Oleg, what are you
doing here?

26
00:01:03,438 --> 00:01:06,461
And you put the, "oh, no,"
in kimono.

27
00:01:06,462 --> 00:01:08,493
Your upstairs neighbor
Sophie

28
00:01:08,494 --> 00:01:10,339
is allowing me
to have sex with her.

29
00:01:10,340 --> 00:01:12,864
And I came down to borrow
some sensual oils.

30
00:01:12,865 --> 00:01:15,449
Preferably,
ones that you can eat.

31
00:01:16,845 --> 00:01:19,515
How did you
get out of apartment?

32
00:01:19,516 --> 00:01:21,405
You know the rules.

33
00:01:21,406 --> 00:01:24,525
I came down
to borrow sensual oils.

34
00:01:24,526 --> 00:01:26,410
Yeah, but no peppermint.

35
00:01:26,411 --> 00:01:28,379
It makes his tongue swell.

36
00:01:28,380 --> 00:01:29,797
And let's face it,

37
00:01:29,798 --> 00:01:33,696
his tongue is the best part
about him.

38
00:01:33,697 --> 00:01:37,271
You're right,
I can't handle that.

39
00:01:37,272 --> 00:01:40,208
[Rock music]

40
00:01:40,209 --> 00:01:45,129
<font color="#00ffff">Sync & corrections by Rafael UPD</font>
<font color="#00ffff">www.addic7ed.com/</font>

41
00:01:53,361 --> 00:01:54,884
those nice smelling men
in booth two

42
00:01:54,885 --> 00:01:57,498
just asked me
if I was on the down-low.

43
00:01:57,499 --> 00:01:58,971
What does this mean?

44
00:01:58,972 --> 00:02:00,584
Uh, brother,

45
00:02:00,585 --> 00:02:03,392
I'm gonna need
a little more context.

46
00:02:03,393 --> 00:02:05,039
They said,
"are you on the down-low,

47
00:02:05,040 --> 00:02:08,272
because we have a friend
who is a big-time rice queen."

48
00:02:08,273 --> 00:02:11,067
Again, coming up dry.

49
00:02:11,068 --> 00:02:13,236
Rice queen, is it, like,
a Korean dairy queen?

50
00:02:13,237 --> 00:02:15,216
Kind of.

51
00:02:16,584 --> 00:02:17,907
Here's table seven's check,
Earl.

52
00:02:17,908 --> 00:02:19,575
Max,
the guys at booth two

53
00:02:19,576 --> 00:02:21,536
just asked Han
if was on the down-low.

54
00:02:21,537 --> 00:02:23,538
They think he's gay.

55
00:02:23,539 --> 00:02:24,842
That's a step up.

56
00:02:24,843 --> 00:02:28,229
Usually people think
he's a lesbian.

57
00:02:32,087 --> 00:02:33,130
Here you go, Sophie.

58
00:02:33,131 --> 00:02:35,152
Uh, Max, Caroline.

59
00:02:35,153 --> 00:02:37,093
Yeah, about last night,

60
00:02:37,094 --> 00:02:41,097
I hope we can keep that
just between us girls.

61
00:02:41,098 --> 00:02:43,127
I'm sure we have all
done things in the dark

62
00:02:43,128 --> 00:02:46,811
late at night with men
that we'd like to forget.

63
00:02:46,812 --> 00:02:48,883
Why stop at late at night
in the dark?

64
00:02:48,884 --> 00:02:50,301
How about early morning?

65
00:02:50,302 --> 00:02:53,150
Or a nooner at
the port authority bus terminal?

66
00:02:53,151 --> 00:02:54,652
Oh, good.

67
00:02:54,653 --> 00:02:56,737
And you too understand,
Caroline?

68
00:02:56,738 --> 00:02:58,292
Actually, no, sorry.

69
00:02:58,293 --> 00:02:59,677
I haven't ever done
anything with a man

70
00:02:59,678 --> 00:03:01,562
- that I'm ashamed of.
- Oh, yeah.

71
00:03:01,563 --> 00:03:03,230
But your father did
steal millions

72
00:03:03,231 --> 00:03:06,539
and millions of dollars.

73
00:03:06,540 --> 00:03:08,966
So... don't forget that.

74
00:03:12,941 --> 00:03:14,474
Han just sat
your favorite gays.

75
00:03:14,475 --> 00:03:16,911
Max, their names
are Steven and Michael.

76
00:03:16,912 --> 00:03:19,343
And it's not very P.C. To reduce
people to a stereotype.

77
00:03:19,344 --> 00:03:23,501
- Hey, girl.
- What's up, care bear?

78
00:03:23,502 --> 00:03:25,085
Yeah, they're not
stereotypes.

79
00:03:25,086 --> 00:03:26,954
What would you say
if they called you

80
00:03:26,955 --> 00:03:28,769
"the big-boob waitress from
the wrong side of the tracks

81
00:03:28,770 --> 00:03:30,563
who had sex
with everyone in high school"?

82
00:03:30,564 --> 00:03:32,343
I'd say,
"that's why I love the gays.

83
00:03:32,344 --> 00:03:34,233
They get me."

84
00:03:34,234 --> 00:03:35,713
Come over and say hello.

85
00:03:35,714 --> 00:03:37,181
They're the first
and practically the only

86
00:03:37,182 --> 00:03:39,447
people that ever hired us
to do a cupcake job.

87
00:03:39,448 --> 00:03:40,935
Do I have to?

88
00:03:40,936 --> 00:03:42,950
Can't I just stand here
and judge from afar?

89
00:03:42,951 --> 00:03:45,453
Come on, and let's not
mention how bad we're doing.

90
00:03:45,454 --> 00:03:47,191
We want them to think
we're a big success,

91
00:03:47,192 --> 00:03:48,908
so they'll recommend us
for their friends' events.

92
00:03:48,909 --> 00:03:51,500
Yeah, because you know the gays,
they party about everything.

93
00:03:51,501 --> 00:03:54,865
And there's
another stereotype.

94
00:03:54,866 --> 00:03:57,548
- Hi, guys.
- Happy leap year!

95
00:03:57,549 --> 00:03:59,453
F.Y.I., people
are still talking about

96
00:03:59,454 --> 00:04:01,427
those cupcakes you made
for our wedding shower.

97
00:04:01,428 --> 00:04:03,324
- Everyone.
- How's business?

98
00:04:03,325 --> 00:04:05,209
Fabulous, amazing,
killing it.

99
00:04:05,210 --> 00:04:07,892
Or it's killing us.
We're not quite sure.

100
00:04:07,893 --> 00:04:10,030
I'll go grab some menus
for you girls.

101
00:04:10,031 --> 00:04:11,498
- Max.
- Sorry.

102
00:04:11,499 --> 00:04:14,732
I'll go grab some menus
for you ladies.

103
00:04:14,733 --> 00:04:17,276
Love her... so Madeleine Stowe
in <i>revenge.</i>

104
00:04:17,277 --> 00:04:18,611
Oh, she is.

105
00:04:18,612 --> 00:04:20,307
- I don't watch that.
- We love it.

106
00:04:20,308 --> 00:04:21,725
It's all about
a young blonde woman

107
00:04:21,726 --> 00:04:23,227
who's out to seek revenge
on everyone

108
00:04:23,228 --> 00:04:24,742
for destroying her family.

109
00:04:24,743 --> 00:04:27,348
Yeah, too close to home.

110
00:04:28,883 --> 00:04:30,351
- Max, guess what.
- Caroline, you're not

111
00:04:30,352 --> 00:04:32,019
supposed to tell me
I'm on <i>intervention</i>

112
00:04:32,020 --> 00:04:34,251
until the actual
intervention.

113
00:04:34,252 --> 00:04:36,420
I just got offered $600

114
00:04:36,421 --> 00:04:38,058
to stay at Steven
and Michael's apartment

115
00:04:38,059 --> 00:04:39,493
and watch their dogs
while they're out of town

116
00:04:39,494 --> 00:04:41,895
- for two days.
- Why would anyone pay $600

117
00:04:41,896 --> 00:04:43,969
to watch dogs
for two days?

118
00:04:43,970 --> 00:04:45,399
Well, because, you know,
they want a certain

119
00:04:45,400 --> 00:04:46,733
amount of attention
to be paid to the...

120
00:04:46,734 --> 00:04:50,476
Just say it, they're gay and they think
their dogs are their children.

121
00:04:50,477 --> 00:04:52,603
Max, you have to stop
doing this.

122
00:04:52,604 --> 00:04:54,480
I'll stop
when they stop.

123
00:04:54,481 --> 00:04:56,126
This is so exciting.

124
00:04:56,127 --> 00:04:58,245
It'll be like a paid vacation
at an amazing hotel.

125
00:04:58,246 --> 00:05:00,653
And I need to get as far away
from here as possible.

126
00:05:00,654 --> 00:05:03,280
- Oh, where do they live?
- Four blocks away.

127
00:05:03,281 --> 00:05:05,185
Well, have a good time.

128
00:05:05,186 --> 00:05:06,971
And while you're
four blocks away

129
00:05:06,972 --> 00:05:08,722
having your vacation,
I'll be home

130
00:05:08,723 --> 00:05:11,108
maybe taking a little
masturbacation.

131
00:05:13,194 --> 00:05:14,645
What are you
talking about?

132
00:05:14,646 --> 00:05:16,430
I wouldn't go on vacation
without you.

133
00:05:16,431 --> 00:05:17,982
We'll split the dog money
to cover the shifts

134
00:05:17,983 --> 00:05:19,505
we take off
from the diner.

135
00:05:19,506 --> 00:05:21,118
Well, thanks,
but I don't do that thing

136
00:05:21,119 --> 00:05:22,550
where you take off work.

137
00:05:22,551 --> 00:05:24,771
"Hi, I'm Max, I can't go

138
00:05:24,772 --> 00:05:26,929
"to a fancy apartment
and have fun.

139
00:05:26,930 --> 00:05:28,292
"I'd rather work
in a steel mill

140
00:05:28,293 --> 00:05:30,858
and sing
Bruce Springsteen songs."

141
00:05:32,206 --> 00:05:34,214
Max, when was the last time
you had a vacation?

142
00:05:34,215 --> 00:05:35,438
Never.

143
00:05:35,439 --> 00:05:37,017
Unless you count
the summer I hid

144
00:05:37,018 --> 00:05:40,109
under the front porch
from my mom's new boyfriend.

145
00:05:40,110 --> 00:05:41,388
We really need
to get out of here.

146
00:05:41,389 --> 00:05:42,923
This is more than just
spring break.

147
00:05:42,924 --> 00:05:44,258
It's a break
from the diner,

148
00:05:44,259 --> 00:05:46,644
from the cupcake business,
from being poor,

149
00:05:46,645 --> 00:05:48,534
from everything
depressing right now.

150
00:05:48,535 --> 00:05:51,015
Things aren't
that depressing.

151
00:05:51,016 --> 00:05:53,631
[Clattering]

152
00:05:56,804 --> 00:05:58,335
[Thud]

153
00:05:58,336 --> 00:06:00,507
[Clattering stops]

154
00:06:11,870 --> 00:06:13,184
Oh.

155
00:06:13,185 --> 00:06:16,290
Yeah, well,
I'm sure we have

156
00:06:16,291 --> 00:06:20,210
all done things with men
in kitchens that...

157
00:06:20,211 --> 00:06:21,962
We are not proud of.

158
00:06:33,021 --> 00:06:34,324
[Elevator dings]

159
00:06:36,344 --> 00:06:38,995
Spring break
is the greatest time ever.

160
00:06:38,996 --> 00:06:40,947
You just go crazy
and do shots

161
00:06:40,948 --> 00:06:43,047
and throw all the rules
out the window.

162
00:06:43,048 --> 00:06:44,751
Oh, I've done that.

163
00:06:44,752 --> 00:06:47,537
But when you're poor,
you just call it Tuesday.

164
00:06:47,538 --> 00:06:49,623
[Hums]

165
00:06:50,591 --> 00:06:52,926
Why are you
walking like that?

166
00:06:52,927 --> 00:06:54,678
This is my
spring break walk.

167
00:06:54,679 --> 00:06:56,396
These shoulders
have shimmied like this

168
00:06:56,397 --> 00:06:58,131
all the way
from Majorca to Ibiza.

169
00:06:58,132 --> 00:06:59,766
Try it, Max.
Shake a little.

170
00:06:59,767 --> 00:07:01,384
- Shake.
- That's cool.

171
00:07:01,385 --> 00:07:04,020
I'll just wait till
I get the DTS later.

172
00:07:05,756 --> 00:07:08,642
Maybe I don't
have the right key.

173
00:07:08,643 --> 00:07:10,032
Oh, hi.

174
00:07:10,033 --> 00:07:12,326
Sorry, I think we might
be at the wrong door.

175
00:07:12,327 --> 00:07:14,161
That's a bummer
for the door.

176
00:07:14,162 --> 00:07:15,782
It's over there, 12d.

177
00:07:15,783 --> 00:07:17,498
We're house-sitting
for a few days.

178
00:07:17,499 --> 00:07:18,874
Cool, I'm Brendon.

179
00:07:18,875 --> 00:07:21,121
Hey, Brendon.
I'm Ashley.

180
00:07:21,122 --> 00:07:24,324
- And...
- I'm surprised.

181
00:07:24,325 --> 00:07:26,293
- And also Max.
- I'd shake your hands,

182
00:07:26,294 --> 00:07:27,794
but I kind of stink.

183
00:07:27,795 --> 00:07:29,546
I've been at my microbrewery
most of the day,

184
00:07:29,547 --> 00:07:32,012
so I am sorry
if I smell like yeast.

185
00:07:32,013 --> 00:07:34,390
Hey, that's my apology.

186
00:07:34,391 --> 00:07:37,470
Whoa, all right, cool.

187
00:07:37,471 --> 00:07:40,062
Well, I will see
you guys around, yeah?

188
00:07:40,063 --> 00:07:42,509
Yeah, totes.

189
00:07:45,212 --> 00:07:47,480
You have something
you want to tell me, Ashley?

190
00:07:47,481 --> 00:07:49,566
I never use Caroline channing
on vacation

191
00:07:49,567 --> 00:07:50,967
for security reasons.

192
00:07:50,968 --> 00:07:52,519
I was almost abducted
at gunpoint

193
00:07:52,520 --> 00:07:54,243
for a ransom
in Mexico city.

194
00:07:54,244 --> 00:07:55,655
Wow, gunpoint.

195
00:07:55,656 --> 00:07:57,705
Were you using the word,
"totes" at the time?

196
00:07:57,706 --> 00:08:01,083
Can you take out the paper
that has the alarm code on it?

197
00:08:01,084 --> 00:08:03,997
[Alarm beeps]

198
00:08:03,998 --> 00:08:06,422
Wow, this place
is amazing.

199
00:08:06,423 --> 00:08:09,336
It's, like, the cover
of <i>gay gay gay magazine.</i>

200
00:08:11,217 --> 00:08:12,922
Max, you have to stop
reducing them

201
00:08:12,923 --> 00:08:14,490
to that gay stereotype.

202
00:08:14,491 --> 00:08:17,127
Now, read me
the security code.

203
00:08:17,128 --> 00:08:21,145
It's..."Liza."

204
00:08:21,146 --> 00:08:23,016
A lot of different
types of people

205
00:08:23,017 --> 00:08:25,524
like Liza Minnelli.

206
00:08:25,525 --> 00:08:26,853
[Alarm stops]

207
00:08:26,854 --> 00:08:28,722
[Gasps]

208
00:08:28,723 --> 00:08:32,448
Oh, look,
it's a poodle in a tutu.

209
00:08:32,449 --> 00:08:35,528
That's not gay, right?

210
00:08:35,529 --> 00:08:37,530
Lots of...
Lots of different

211
00:08:37,531 --> 00:08:41,951
types of people
have a poodle in a tutu...

212
00:08:41,952 --> 00:08:45,002
Named Barbra Streisand.

213
00:08:45,003 --> 00:08:49,476
Oh, there's the book
of instructions.

214
00:08:49,477 --> 00:08:52,092
"Hey, girl.
Hey, care bear.

215
00:08:52,093 --> 00:08:54,798
"Have fun with Barbra Streisand
and Wynonna Judd.

216
00:08:54,799 --> 00:08:56,549
"Wynonna is a little
temperamental

217
00:08:56,550 --> 00:08:58,418
"and tends to stay
in the bedroom.

218
00:08:58,419 --> 00:09:01,554
"We didn't have time to purge
her anal gland before left.

219
00:09:01,555 --> 00:09:03,812
Instructions on page two."

220
00:09:03,813 --> 00:09:06,815
I'm starting
to understand the $600.

221
00:09:06,816 --> 00:09:09,262
"Don't worry, it's easy.
We do it all the time."

222
00:09:09,263 --> 00:09:11,547
No wonder Wynonna's
so temperamental.

223
00:09:11,548 --> 00:09:13,433
"Help yourself
to anything in the fridge.

224
00:09:13,434 --> 00:09:16,033
There are portable saunas
in the closet."

225
00:09:16,034 --> 00:09:17,387
Portable saunas?

226
00:09:17,388 --> 00:09:20,004
Love it, what is it?

227
00:09:23,694 --> 00:09:25,845
This is so cool.

228
00:09:25,846 --> 00:09:27,711
I'm hot
like I'm at the beach,

229
00:09:27,712 --> 00:09:30,798
but I'm not stepping
in any used condoms.

230
00:09:30,799 --> 00:09:32,035
Yeah, these are great.

231
00:09:32,036 --> 00:09:33,403
My friend
Candice travelstead

232
00:09:33,404 --> 00:09:35,552
used one to make weight
for prom.

233
00:09:35,553 --> 00:09:37,123
Are you done with that?

234
00:09:37,124 --> 00:09:39,092
Yeah, switch?

235
00:09:39,093 --> 00:09:40,293
On spring break,
I always loved

236
00:09:40,294 --> 00:09:41,600
spending the day
at the beach.

237
00:09:41,601 --> 00:09:44,144
Just the chilling
and the girl talk.

238
00:09:44,145 --> 00:09:45,915
Yeah.
Tell me again,

239
00:09:45,916 --> 00:09:48,565
about the sound the anal gland
made when you squeezed it.

240
00:09:48,566 --> 00:09:49,936
Please, stop.

241
00:09:49,937 --> 00:09:51,587
I already told you twice.

242
00:09:51,588 --> 00:09:53,654
And now, wynonna Judd
won't even look at me.

243
00:09:53,655 --> 00:09:56,476
Well, not without
her lawyer in the room.

244
00:09:56,477 --> 00:09:59,076
I'm hungry.
What do you eat on vacation?

245
00:09:59,077 --> 00:10:00,780
Anything we want.

246
00:10:00,781 --> 00:10:02,565
Let's see.

247
00:10:04,268 --> 00:10:07,103
I bet their refrigerator is
stocked with all kinds of pates

248
00:10:07,104 --> 00:10:09,989
and cheeses
and those parm crisp things.

249
00:10:09,990 --> 00:10:11,624
By the way, nice suit.

250
00:10:11,625 --> 00:10:13,460
You look like someone should
be pouring a 40 on you

251
00:10:13,461 --> 00:10:15,178
in a rap video.

252
00:10:15,179 --> 00:10:18,114
Thanks.

253
00:10:18,115 --> 00:10:19,466
There's nothing.

254
00:10:19,467 --> 00:10:22,141
I mean, come on,
what kind of gays are they?

255
00:10:22,142 --> 00:10:24,685
All anal gland
and no cheese.

256
00:10:24,686 --> 00:10:26,923
I just expected
everything to be better.

257
00:10:26,924 --> 00:10:28,647
You know,
champagne on ice,

258
00:10:28,648 --> 00:10:30,343
thousand
thread-count sheets.

259
00:10:30,344 --> 00:10:32,860
I mean, the towels...
They're not even bath sheets.

260
00:10:32,861 --> 00:10:34,314
Wait, the towels
aren't good?

261
00:10:34,315 --> 00:10:36,739
Because I already stole two.

262
00:10:36,740 --> 00:10:38,017
They're fine,
they're just not

263
00:10:38,018 --> 00:10:39,324
what I was expecting.

264
00:10:39,325 --> 00:10:40,904
What's happening
over there?

265
00:10:40,905 --> 00:10:43,370
You could use
a little shoulders.

266
00:10:43,371 --> 00:10:45,308
You're right,
enough, we're on vacay.

267
00:10:45,309 --> 00:10:47,110
Come on, let's go get dressed
and find someplace to eat.

268
00:10:47,111 --> 00:10:49,112
Are you crazy?
I am not leaving this thing.

269
00:10:49,113 --> 00:10:51,795
This is the happiest
I've ever been in my life.

270
00:10:51,796 --> 00:10:53,867
Okay, we'll order in,
pretend it's room service.

271
00:10:53,868 --> 00:10:56,258
They must have a menu drawer
here somewhere.

272
00:10:56,259 --> 00:10:57,504
Plus, we'll never
find a restaurant

273
00:10:57,505 --> 00:10:59,386
with a portable sauna
section.

274
00:10:59,387 --> 00:11:01,591
"Food network party tonight

275
00:11:01,592 --> 00:11:03,676
"to celebrate
the release of a cookbook.

276
00:11:03,677 --> 00:11:05,100
Food and drink provided."

277
00:11:05,101 --> 00:11:07,714
Free foodie food,
free foodie drinks?

278
00:11:07,715 --> 00:11:10,049
- Yay, let's go.
- No, I like it in here.

279
00:11:10,050 --> 00:11:11,815
It's amazing.

280
00:11:11,816 --> 00:11:13,503
Ashley wants to go.

281
00:11:13,504 --> 00:11:15,522
Ashley wants to go.

282
00:11:15,523 --> 00:11:17,613
Fine,
but can I bring the dog?

283
00:11:17,614 --> 00:11:19,025
I've always wanted
to be one of those girls

284
00:11:19,026 --> 00:11:20,727
who brings a tiny dog
to a public place

285
00:11:20,728 --> 00:11:22,445
and just
looks at everyone, like,

286
00:11:22,446 --> 00:11:24,953
"yeah, I have a tiny dog,
so what?"

287
00:11:24,954 --> 00:11:26,816
You can be whoever
you want to be.

288
00:11:26,817 --> 00:11:28,101
You're on vacation.

289
00:11:28,102 --> 00:11:29,702
I'm gonna go invite
Brendon to come.

290
00:11:29,703 --> 00:11:31,710
You gotta have a crush
on spring break.

291
00:11:31,711 --> 00:11:34,123
Well,
I'm sitting in my crush.

292
00:11:34,124 --> 00:11:35,923
Hey, shouldn't you
put something on?

293
00:11:35,924 --> 00:11:37,808
Uh, it's spring break.

294
00:11:42,283 --> 00:11:44,300
Aah!

295
00:11:46,587 --> 00:11:49,055
Barbra, get help.

296
00:11:49,056 --> 00:11:51,841
Go, Barbra Streisand, go.

297
00:11:56,330 --> 00:11:57,880
Hey,
look at the book title,

298
00:11:57,881 --> 00:12:01,448
<i>306 degrees of heaven:
Bacon.</i>

299
00:12:01,449 --> 00:12:05,071
Heaven bacon.
That's good wordplay.

300
00:12:05,072 --> 00:12:06,739
It's okay.
Not much of a crowd.

301
00:12:06,740 --> 00:12:08,372
Party's a little
disappointing.

302
00:12:08,373 --> 00:12:10,827
Bacon truffle,
Barbra Streisand?

303
00:12:10,828 --> 00:12:13,085
Mm?

304
00:12:13,086 --> 00:12:17,233
Yeah, I'm feeding my tiny dog
a truffle, so what?

305
00:12:17,234 --> 00:12:19,068
Miss, miss.

306
00:12:19,069 --> 00:12:20,801
Excuse me,
what are these things for?

307
00:12:20,802 --> 00:12:22,261
Oh, it holds your drink
while you get food.

308
00:12:22,262 --> 00:12:24,924
- Both: No!
- Yeah.

309
00:12:24,925 --> 00:12:27,432
Yeah, these are great when
you see them for the first time.

310
00:12:27,433 --> 00:12:29,178
For me,
it was on a yacht

311
00:12:29,179 --> 00:12:30,394
in the Mediterranean.

312
00:12:30,395 --> 00:12:32,415
You know what else
these are good for?

313
00:12:32,416 --> 00:12:34,857
Frees up your shoulders
to shimmy.

314
00:12:34,858 --> 00:12:37,276
You shimmy.
I'm gonna walk over here

315
00:12:37,277 --> 00:12:41,655
and find us a bigger, shinier
something to do later.

316
00:12:41,656 --> 00:12:43,092
Later?
We're going home

317
00:12:43,093 --> 00:12:44,944
- to our amazing apartment.
- Or not.

318
00:12:44,945 --> 00:12:46,396
Come on, Max,
it's spring break.

319
00:12:46,397 --> 00:12:47,981
Let's not get tied down.

320
00:12:47,982 --> 00:12:49,399
I'll be over there.

321
00:12:49,400 --> 00:12:51,100
Yeah, we'll be over there.

322
00:12:51,101 --> 00:12:52,785
Oh.

323
00:12:54,121 --> 00:12:57,004
Hi. You're not allowed
to have dog in here.

324
00:12:57,005 --> 00:12:58,922
Unless he wants
to buy the book.

325
00:12:58,923 --> 00:13:01,592
What are you,
some kind of foodie bouncer?

326
00:13:01,593 --> 00:13:03,677
Worse.
I wrote the book.

327
00:13:03,678 --> 00:13:05,470
Yeah, nice sleeves.

328
00:13:05,471 --> 00:13:09,052
You get those in
the tattoo gold rush of '09?

329
00:13:09,053 --> 00:13:10,392
I'm Zeke.
What's your name?

330
00:13:10,393 --> 00:13:12,672
I'm Max.
This is my tiny dog.

331
00:13:12,673 --> 00:13:15,105
I take her everywhere,
so what?

332
00:13:15,106 --> 00:13:17,900
Hey, Max, you wanna try
my thick-cut bacon?

333
00:13:17,901 --> 00:13:20,847
Maybe. Let's just
take things slow.

334
00:13:22,349 --> 00:13:23,633
I'm telling you,
it serves

335
00:13:23,634 --> 00:13:25,351
the best
vegan stew anywhere.

336
00:13:25,352 --> 00:13:27,117
You really should put it
in your blog.

337
00:13:27,118 --> 00:13:29,489
Oh, if I write one more
article about something vegan,

338
00:13:29,490 --> 00:13:32,775
all of my devoted
10,000 readers will hold hands

339
00:13:32,776 --> 00:13:35,542
and jump collectively
out a window.

340
00:13:35,543 --> 00:13:38,814
And I know you like to find
what's new and what's next.

341
00:13:38,815 --> 00:13:40,047
Chocolate bacon?

342
00:13:40,048 --> 00:13:43,286
I just need
a minute over there.

343
00:13:43,287 --> 00:13:45,719
Hi, couldn't help
but overhear.

344
00:13:45,720 --> 00:13:47,256
If you're looking
for what's new,

345
00:13:47,257 --> 00:13:48,991
I found these
two adorable girls

346
00:13:48,992 --> 00:13:50,390
who run a cupcake business

347
00:13:50,391 --> 00:13:51,844
out of their home
in Williamsburg.

348
00:13:51,845 --> 00:13:53,630
I know how that sounds,
but trust me,

349
00:13:53,631 --> 00:13:56,146
totally fresh
and not at all overexposed.

350
00:13:56,147 --> 00:13:58,357
And you're someone
I should listen to because...?

351
00:13:58,358 --> 00:14:00,520
Sorry,
didn't introduce myself.

352
00:14:00,521 --> 00:14:03,320
Ashley Emerson,
style editor for <i>Elle.</i>

353
00:14:03,321 --> 00:14:05,530
Oh.

354
00:14:05,531 --> 00:14:07,009
If you're interested,
I think I might have

355
00:14:07,010 --> 00:14:08,177
a card in here somewhere.

356
00:14:08,178 --> 00:14:10,160
Was holding on to it
for Martha.

357
00:14:10,161 --> 00:14:13,449
Mr. Bacon action figure.

358
00:14:13,450 --> 00:14:15,582
A world of bacon sampler.

359
00:14:15,583 --> 00:14:17,737
And I want to apologize for
the food network trucker hat,

360
00:14:17,738 --> 00:14:19,188
which has nothing
to do with bacon

361
00:14:19,189 --> 00:14:21,023
and everything to do
with me being a sellout.

362
00:14:21,024 --> 00:14:22,125
Guess what.

363
00:14:22,126 --> 00:14:23,590
Ashley just gave our card

364
00:14:23,591 --> 00:14:26,829
to a woman who's such a bitch
she must be important.

365
00:14:26,830 --> 00:14:28,081
She's a food blogger.

366
00:14:28,082 --> 00:14:29,215
It's a long shot,

367
00:14:29,216 --> 00:14:30,800
but we need to get
some exposure.

368
00:14:30,801 --> 00:14:32,385
Whoa, we are on vacation.

369
00:14:32,386 --> 00:14:35,388
No business talk.
That was the deal, right?

370
00:14:35,389 --> 00:14:39,475
This is Zeke, chef-slash-
author-slash-corporate sellout.

371
00:14:39,476 --> 00:14:41,650
We were, uh...

372
00:14:41,651 --> 00:14:43,396
We were thinking about
going back to the apartment

373
00:14:43,397 --> 00:14:45,815
- to hang out.
- Oh, sounds fun.

374
00:14:45,816 --> 00:14:47,406
Can I see you alone
for a second?

375
00:14:47,407 --> 00:14:48,851
Well, not alone.

376
00:14:48,852 --> 00:14:51,535
I bring my tiny dog
everywhere, so what?

377
00:14:51,536 --> 00:14:54,390
Come on, I heard
of an after-party in Soho

378
00:14:54,391 --> 00:14:56,325
and an after-after-party
in dumbo.

379
00:14:56,326 --> 00:14:57,577
So, let's go-ho.

380
00:14:57,578 --> 00:15:00,002
Have fun and get
this vacation started.

381
00:15:00,003 --> 00:15:02,699
I'm holding a tiny dog
at a bacon-book party

382
00:15:02,700 --> 00:15:05,168
and laughing with a guy
who wrote a book about bacon.

383
00:15:05,169 --> 00:15:08,176
I think my vacation's
already started.

384
00:15:08,177 --> 00:15:09,789
All right,
well, have fun.

385
00:15:09,790 --> 00:15:11,387
I'm gonna go par-tay.

386
00:15:11,388 --> 00:15:14,627
Eh, did your thing, Ash.

387
00:15:23,773 --> 00:15:25,151
I don't want
to say that I'm hot.

388
00:15:25,152 --> 00:15:28,738
But the bacon
in my pocket is done.

389
00:15:28,739 --> 00:15:31,540
We said turns were
five minutes, you bastards.

390
00:15:32,841 --> 00:15:34,976
Oh, hi.
More sauna fun.

391
00:15:34,977 --> 00:15:36,996
Max, can I see you
in the hall for a second?

392
00:15:36,997 --> 00:15:40,949
But it's my turn
to sweat my balls off.

393
00:15:40,950 --> 00:15:44,235
Don't think the clock's
not running while I'm out there.

394
00:15:44,236 --> 00:15:45,954
We're going
on a real vacation.

395
00:15:45,955 --> 00:15:47,655
How quickly
can you pack a bag?

396
00:15:47,656 --> 00:15:49,207
And don't worry about
appropriate evening wear.

397
00:15:49,208 --> 00:15:50,542
Every four seasons
has a Gucci

398
00:15:50,543 --> 00:15:52,470
near the Bulgari store
in the lobby.

399
00:15:52,471 --> 00:15:55,163
None of those words
made any sense.

400
00:15:55,164 --> 00:15:57,099
I think you've had
a spring break stroke.

401
00:15:57,100 --> 00:15:59,384
No, seriously, there's a car
waiting downstairs

402
00:15:59,385 --> 00:16:01,136
to take us
to a private plane.

403
00:16:01,137 --> 00:16:02,637
We can be at Teterboro
in 20 minutes

404
00:16:02,638 --> 00:16:03,888
and Tahiti in 8 hours.

405
00:16:03,889 --> 00:16:06,541
The Greek
is paying for everything.

406
00:16:06,542 --> 00:16:08,510
What did they give you?

407
00:16:08,511 --> 00:16:10,654
What's the last thing
you can remember?

408
00:16:10,655 --> 00:16:12,313
Nothing.

409
00:16:12,314 --> 00:16:13,866
I just had some cristal.

410
00:16:13,867 --> 00:16:16,327
Some cristal meth?

411
00:16:16,328 --> 00:16:18,069
No, I'm just
really excited.

412
00:16:18,070 --> 00:16:20,738
I went to the after-party
and this shipping magnate.

413
00:16:20,739 --> 00:16:22,323
Don't worry,
it's not gonna be sexual.

414
00:16:22,324 --> 00:16:24,058
More like a father thing.

415
00:16:24,059 --> 00:16:26,754
Anyway, let's go... finally
got us a good vacation.

416
00:16:26,755 --> 00:16:28,839
But we're having
a good vacation.

417
00:16:28,840 --> 00:16:30,565
Max, don't take this
personally,

418
00:16:30,566 --> 00:16:32,233
but you don't know what
a good vacation is

419
00:16:32,234 --> 00:16:33,803
because you've
never been anywhere.

420
00:16:33,804 --> 00:16:35,888
I mean, you walked
into that apartment

421
00:16:35,889 --> 00:16:39,007
and your face lit up, like...
Like it was amazing.

422
00:16:39,008 --> 00:16:41,092
And that's because
you don't know that it's not.

423
00:16:41,093 --> 00:16:43,628
Trust me.
I've been everywhere.

424
00:16:43,629 --> 00:16:47,358
And I'm telling you,
this is the worst vacation ever.

425
00:16:47,359 --> 00:16:50,585
So let's ditch the bacon freak
and the beer bro,

426
00:16:50,586 --> 00:16:53,087
get on that plane, spend
one great day at the beach,

427
00:16:53,088 --> 00:16:55,449
and then come back
and make $8 an hour.

428
00:16:55,450 --> 00:16:57,141
That sounds fun.

429
00:16:57,142 --> 00:16:58,776
You kind of skipped over
the part where we get abducted

430
00:16:58,777 --> 00:17:00,778
and Nancy grace
covers the year-long search

431
00:17:00,779 --> 00:17:02,289
for our bodies.

432
00:17:02,290 --> 00:17:03,931
[Phone beeps]

433
00:17:03,932 --> 00:17:05,584
It must be Constantine.
He's gonna be upset.

434
00:17:05,585 --> 00:17:08,269
You never
keep a Greek waiting.

435
00:17:08,270 --> 00:17:09,904
Oh, my God, Max,
we just got an email

436
00:17:09,905 --> 00:17:12,407
on our website from that
food blogger at the book party.

437
00:17:12,408 --> 00:17:14,659
She said if we drop off four
cupcakes by tomorrow morning,

438
00:17:14,660 --> 00:17:17,629
she'll consider writing about
them for her column that day.

439
00:17:17,630 --> 00:17:19,390
Well, what's it gonna be,
Caroline or Ashley?

440
00:17:19,391 --> 00:17:22,584
Cupcakes or Tahiti?
Your call.

441
00:17:23,685 --> 00:17:25,479
Is the Greek
still waiting?

442
00:17:25,480 --> 00:17:27,388
He just pulled away.

443
00:17:27,389 --> 00:17:29,525
I'm sure he'll come back here
for Ashley sometime.

444
00:17:29,526 --> 00:17:32,236
And when that happens,
I wouldn't want to be her.

445
00:17:32,237 --> 00:17:34,864
Well, after tomorrow,
you won't be.

446
00:17:34,865 --> 00:17:35,990
What can I do?

447
00:17:35,991 --> 00:17:37,732
You mean after
your two personalities

448
00:17:37,733 --> 00:17:39,702
meet and kill each other?

449
00:17:39,703 --> 00:17:41,603
Max, I told you
I needed a break.

450
00:17:41,604 --> 00:17:44,206
I just didn't know it was
gonna be a psychotic one.

451
00:17:44,207 --> 00:17:45,690
You really need
to chill.

452
00:17:45,691 --> 00:17:47,108
You can't
keep freaking out

453
00:17:47,109 --> 00:17:48,443
because you're not where
you want to be...

454
00:17:48,444 --> 00:17:50,445
On vacation
or in our business.

455
00:17:50,446 --> 00:17:53,549
I mean, it's life.
Lower your expectations.

456
00:17:53,550 --> 00:17:55,634
This microbrew
is actually really good.

457
00:17:55,635 --> 00:17:57,652
Tastes better
in the batter.

458
00:17:57,653 --> 00:18:00,055
Hey, crumble up some of that
maple bacon for me.

459
00:18:00,056 --> 00:18:01,839
Sorry about
that stuff I said

460
00:18:01,840 --> 00:18:03,726
about you never having
gone anywhere.

461
00:18:03,727 --> 00:18:05,326
Why?
I never have gone anywhere

462
00:18:05,327 --> 00:18:07,178
or done anything.

463
00:18:07,179 --> 00:18:08,930
And now I'm thinking
that's kind of a good thing,

464
00:18:08,931 --> 00:18:11,049
because I can still
get excited and make a face

465
00:18:11,050 --> 00:18:12,850
when something's new to me.

466
00:18:12,851 --> 00:18:14,153
'Cause, basically,
everything is.

467
00:18:14,154 --> 00:18:15,779
That must be nice.

468
00:18:15,780 --> 00:18:19,241
All right, here,
taste it.

469
00:18:19,242 --> 00:18:22,694
My beer-batter, maple bacon,
spring break cupcake.

470
00:18:25,664 --> 00:18:29,376
Oh, my God, you made
the "something new" face.

471
00:18:29,377 --> 00:18:32,036
I did, because
you made something new.

472
00:18:32,037 --> 00:18:35,406
- It's delicious.
- Do the face again.

473
00:18:42,665 --> 00:18:45,768
Oh, there they are,
the world travelers.

474
00:18:45,769 --> 00:18:47,978
Shouldn't people be throwing
confetti or something?

475
00:18:47,979 --> 00:18:49,704
I'm all out.

476
00:18:49,705 --> 00:18:53,424
I snorted mine
in the power outage of '65.

477
00:18:53,425 --> 00:18:54,860
How was your vacation?

478
00:18:54,861 --> 00:18:57,178
Well, it turned out
to be more of a daycation,

479
00:18:57,179 --> 00:18:59,046
but we had a great time,
didn't we, Max?

480
00:18:59,047 --> 00:19:00,565
Yes, we did.

481
00:19:00,566 --> 00:19:02,868
Max, things sure are dull
around here without you.

482
00:19:02,869 --> 00:19:04,552
Everyone walking around
all P.C.,

483
00:19:04,553 --> 00:19:07,047
nobody called me black.
Hell.

484
00:19:08,665 --> 00:19:10,825
I haven't left once
since you've been gone.

485
00:19:10,826 --> 00:19:13,879
When it comes to this job,
you are my vacation.

486
00:19:13,880 --> 00:19:15,563
Aw.

487
00:19:15,564 --> 00:19:17,732
And that, Earl,
is why I got

488
00:19:17,733 --> 00:19:20,418
all five
of these gift bags for you.

489
00:19:23,005 --> 00:19:25,072
Welcome, again,
to the Williamsburg diner.

490
00:19:25,073 --> 00:19:26,934
This way, please.

491
00:19:26,935 --> 00:19:30,878
I have looked up "down-low"
and "rice queen" on Wikipedia.

492
00:19:30,879 --> 00:19:33,080
And, no, I'm not
a secret homosexual

493
00:19:33,081 --> 00:19:35,609
or a man who chases
only Asian men.

494
00:19:35,610 --> 00:19:38,704
I'm great, I'm straight.
Get used to it.

495
00:19:40,864 --> 00:19:42,523
Hi, guys.
How was your vacation?

496
00:19:42,524 --> 00:19:44,577
Great. Did you have fun
at our place?

497
00:19:44,578 --> 00:19:46,620
Totes.
Your saunas are sick.

498
00:19:46,621 --> 00:19:49,707
I know.
Could they be any gayer?

499
00:19:49,708 --> 00:19:52,099
No.

500
00:19:52,100 --> 00:19:54,652
Just one thing.
Wynonna's still a little upset

501
00:19:54,653 --> 00:19:56,380
about the anal gland event.

502
00:19:56,381 --> 00:19:57,989
What?

503
00:19:57,990 --> 00:20:00,992
I told you she wouldn't
know it was a joke.

504
00:20:04,660 --> 00:20:07,281
How could she not know
it was a joke?

505
00:20:07,282 --> 00:20:10,728
I mean, who would ask someone
to squeeze their dog's pooper?

506
00:20:10,729 --> 00:20:12,813
We're paying you
another 50.

507
00:20:12,814 --> 00:20:15,306
Dude, she went
second knuckle deep

508
00:20:15,307 --> 00:20:19,445
in your beagle's back door
for only another 50?

509
00:20:19,446 --> 00:20:20,904
150.

510
00:20:20,905 --> 00:20:22,797
Thank you.

511
00:20:22,798 --> 00:20:24,315
And just one other thing.

512
00:20:24,316 --> 00:20:26,634
Our business is actually
not that great right now,

513
00:20:26,635 --> 00:20:28,352
so if you could recommend us
to any of your friends,

514
00:20:28,353 --> 00:20:30,164
- that would really help.
- Good for you.

515
00:20:30,165 --> 00:20:32,916
I'm sure business will
pick up after that blog blurb.

516
00:20:32,917 --> 00:20:35,276
- Wait, blog blurb?
- About your beer-battered

517
00:20:35,277 --> 00:20:36,744
maple bacon
spring break cupcake.

518
00:20:36,745 --> 00:20:38,005
We just read about it.

519
00:20:38,006 --> 00:20:40,424
Yeah, in that
foodie bitch's blog.

520
00:20:40,425 --> 00:20:41,884
Max, it worked.

521
00:20:41,885 --> 00:20:43,761
Let's go look at the blog
on Han's computer.

522
00:20:43,762 --> 00:20:45,554
Excuse us.
We'll be right back.

523
00:20:45,555 --> 00:20:48,506
We got a little break
and a big break.

524
00:20:57,332 --> 00:20:59,333
Yeah,
I did it again.

525
00:20:59,334 --> 00:21:01,769
Yeah.

526
00:21:07,526 --> 00:21:09,176
Welcome back.

527
00:21:09,177 --> 00:21:12,005
You missed the best
two days of my life.

528
00:21:19,254 --> 00:21:22,141
<font color="#00ffff">Sync & corrections by Rafael UPD</font>
<font color="#00ffff">www.addic7ed.com/</font>

