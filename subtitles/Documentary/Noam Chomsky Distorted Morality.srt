﻿1
00:00:04,815 --> 00:00:06,852
One day, there was a journalist
that I liked the most,

2
00:00:07,276 --> 00:00:08,061
and he said things like this,

3
00:00:08,805 --> 00:00:10,973
"Helping the weak is not justice"

4
00:00:10,828 --> 00:00:12,828
"but common sense to journalists"

5
00:00:12,853 --> 00:00:13,620
(Distorted)

6
00:00:14,068 --> 00:00:16,295
You gotta see me before you die

7
00:00:16,744 --> 00:00:18,264
We can put him in jail
only if there's evidence

8
00:00:18,681 --> 00:00:20,079
Just tell him a sentence.

9
00:00:20,271 --> 00:00:21,119
Don't give up 'till the end.

10
00:00:22,748 --> 00:00:24,062
The truth won't be covered with darkness
Don't get confused

11
00:00:24,286 --> 00:00:25,527
Swaying people is not your
duty as a journalist.

12
00:00:25,366 --> 00:00:27,622
Cover up the case with a bigger case.

13
00:00:28,046 --> 00:00:30,302
No matter who comes, the game's over.

14
00:00:30,693 --> 00:00:32,774
I can never possibly forgive them.

15
00:00:33,318 --> 00:00:35,638
(SBS, Monday-Tuesday Drama: Distorted)

16
00:00:36,110 --> 00:00:37,542
First Air on July 24th

17
00:00:37,918 --> 00:00:40,557
Subtitles by KOCOWA TV

