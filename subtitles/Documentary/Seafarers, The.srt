1
00:00:07,735 --> 00:00:09,944
(Narrator)
'This is a story simple but dramatic.

2
00:00:10,071 --> 00:00:13,782
'A story about the men
who crew our ships, the seafarers.

3
00:00:15,034 --> 00:00:19,037
'Every seafarer will recognise
in what follows something of himself.

4
00:00:19,163 --> 00:00:22,540
'Familiar scenes, the faces
of the men who share his calling,

5
00:00:22,667 --> 00:00:27,420
'his way of life
and maybe something more.

6
00:00:27,546 --> 00:00:29,673
'Call it a dream fulfilled.'

7
00:00:32,510 --> 00:00:34,511
I'm Don Hollenbeck.

8
00:00:34,637 --> 00:00:37,347
Joseph Conrad wrote
a lot of things about the sea,

9
00:00:37,473 --> 00:00:39,766
and among other things
he wrote this:

10
00:00:40,643 --> 00:00:45,814
"The true piece of God begins
at any spot 1,000 miles from land."

11
00:00:46,649 --> 00:00:49,150
Here's something else
I came across the other day

12
00:00:49,276 --> 00:00:51,069
that interested me very much.

13
00:00:51,195 --> 00:00:54,072
"Ever since the days of sailing ships,

14
00:00:54,198 --> 00:00:58,034
"seafaring has drawn to it
men of many different types,

15
00:00:58,160 --> 00:01:00,995
"and yet they have one thing
in common, these men.

16
00:01:01,122 --> 00:01:06,960
"It's the quality that makes men
want to live not in a city or in a town,

17
00:01:07,086 --> 00:01:08,712
"but in the world.

18
00:01:08,838 --> 00:01:11,172
"It's a true spirit of independence

19
00:01:11,674 --> 00:01:15,844
"and it's always been deeply rooted
in the seafaring tradition."

20
00:01:16,846 --> 00:01:19,639
And now to go on with this little essay,

21
00:01:19,765 --> 00:01:22,976
"Nobody knows better
than a seafaring man

22
00:01:23,102 --> 00:01:26,479
"that any man,
however independent he is,

23
00:01:26,605 --> 00:01:29,232
"isn't entirely independent.

24
00:01:29,358 --> 00:01:32,152
"He's a member of something larger.

25
00:01:32,278 --> 00:01:37,407
"Of a family,
of a community, of a nation.

26
00:01:37,533 --> 00:01:40,326
"Or bring it back to seafaring.

27
00:01:40,453 --> 00:01:42,370
"He's a member of a crew.

28
00:01:42,496 --> 00:01:45,081
"A crew of men like himself,

29
00:01:45,207 --> 00:01:49,210
"banded together for one
essential common purpose."

30
00:01:50,212 --> 00:01:53,131
Now, the picture
you're going to see now

31
00:01:53,257 --> 00:01:56,092
is the story of men
who are banded together

32
00:01:56,218 --> 00:01:59,220
for one essential common purpose.

33
00:01:59,346 --> 00:02:03,016
This is a vital story in their daily lives
and their achievements.

34
00:02:03,142 --> 00:02:08,062
It's a proud story made possible
by the deeds of seafaring men.

35
00:02:08,189 --> 00:02:11,191
It's the story of the SIU.

36
00:03:04,370 --> 00:03:07,497
(Hollenbeck) 'From Vancouver
down the long Pacific coast,

37
00:03:07,623 --> 00:03:10,458
'around the great curve
of the Gulf of Mexico,

38
00:03:10,584 --> 00:03:14,128
'up the rim of the Atlantic
from Tampa to Halifax,

39
00:03:14,255 --> 00:03:17,841
'in every major north-American port
close by the waterfront,

40
00:03:17,967 --> 00:03:21,970
'there is a headquarters
of the Seafarers' International Union.

41
00:03:24,557 --> 00:03:28,351
'This is headquarters for the SIU
Atlantic and Gulf district.

42
00:03:29,854 --> 00:03:31,521
'More than just a building,

43
00:03:31,647 --> 00:03:35,733
'a solidly impressive symbol
of what seafarers have achieved.

44
00:03:36,944 --> 00:03:38,695
'And like every SIU headquarters,

45
00:03:38,821 --> 00:03:42,240
'the heart of this one
is its hiring hall,

46
00:03:42,366 --> 00:03:45,827
'run by seafarers for seafarers.'

47
00:03:46,704 --> 00:03:48,496
...one second cook.

48
00:03:53,669 --> 00:03:55,253
Is there any more?

49
00:03:56,255 --> 00:03:59,549
Have a book, group one,
April 16th.

50
00:03:59,675 --> 00:04:01,175
Book, group one, April 16th...

51
00:04:01,302 --> 00:04:03,553
(Hollenbeck)
'In this SIU hall, as in others,

52
00:04:03,679 --> 00:04:06,389
'seafarers can pick berths
on ships of many types,

53
00:04:06,515 --> 00:04:08,725
'going almost anywhere in the world,

54
00:04:08,851 --> 00:04:12,854
'Antwerp, Cape Town,
London, Marseilles or Singapore.

55
00:04:14,607 --> 00:04:15,857
'You name it.

56
00:04:15,983 --> 00:04:19,444
'Picking his destination
is the right of every seafarer.

57
00:04:22,406 --> 00:04:25,867
'And sometimes
more than a right - a duty.

58
00:04:25,993 --> 00:04:31,331
'In the great seafaring tradition,
a duty voluntarily assumed.

59
00:04:36,003 --> 00:04:38,880
'Every true seafarer respects courage,

60
00:04:39,006 --> 00:04:41,883
'the courage of men
of all national origins and religions

61
00:04:42,009 --> 00:04:46,054
'that has created seafaring tradition
and built the SIU.

62
00:04:55,564 --> 00:04:58,191
'A seafarer coming ashore
in any United States' port

63
00:04:58,317 --> 00:05:01,819
'no longer needs to feel cut adrift,
as many once did.

64
00:05:12,623 --> 00:05:14,749
'He has a place to go.

65
00:05:16,835 --> 00:05:18,836
'A union hall built by seafarers

66
00:05:18,963 --> 00:05:22,465
'to meet the needs
and suit the tastes of seafarers.

67
00:05:24,093 --> 00:05:26,970
'Big, efficient and safeguarding
the rights and interests

68
00:05:27,096 --> 00:05:29,305
'of those who go to sea,

69
00:05:29,431 --> 00:05:32,642
'but never forgetting that the machines
and the files and the figures

70
00:05:32,768 --> 00:05:35,979
'are there to serve the seafarers
and not the other way around.

71
00:05:36,105 --> 00:05:39,774
'Staffed by experts, but under the
supervision of men who know what it is

72
00:05:39,900 --> 00:05:44,821
'to top a boom, to batten down
a hatch, to weigh an anchor.

73
00:06:13,434 --> 00:06:15,893
'Financial statements
posted weekly in every port

74
00:06:16,020 --> 00:06:17,937
'for seafarers to read.

75
00:06:18,814 --> 00:06:22,692
'And a SIU print shop turns out
regular bulletins and reports,

76
00:06:22,818 --> 00:06:26,529
'airmailed to seafarers on
SIU-contracted ships around the world.

77
00:06:34,079 --> 00:06:36,080
(♪ Music over dialogue)

78
00:06:51,430 --> 00:06:55,391
'A good seafarers' headquarters
has to begin where others leave off.

79
00:06:55,517 --> 00:06:58,061
'For seafarers coming ashore
in the port away from home,

80
00:06:58,187 --> 00:07:00,646
'it ought to be and is a second home

81
00:07:00,773 --> 00:07:03,691
'where no man off a ship
is a stranger to the rest,

82
00:07:03,817 --> 00:07:06,569
'and where the things he needs
are close at hand,

83
00:07:06,695 --> 00:07:08,780
'like this SIU cafeteria,

84
00:07:08,906 --> 00:07:13,034
'open to the public but mainly
for the convenience of seafarers.

85
00:07:34,556 --> 00:07:38,184
'Lots of space,
lots of good, inexpensive food.

86
00:07:43,649 --> 00:07:45,566
(♪ Music over dialogue)

87
00:08:05,879 --> 00:08:08,840
'The galley well stocked
and well scrubbed.

88
00:08:10,551 --> 00:08:14,053
'A pleasant sight after any voyage
is the SIU barber shop.

89
00:08:14,179 --> 00:08:17,682
'Another of the many conveniences
at SIU headquarters.

90
00:08:26,692 --> 00:08:31,696
'The SIU Sea Chest,
which supports ship and shore gear.

91
00:08:31,822 --> 00:08:36,534
'Takes orders, makes deliveries
from SIU warehouses in SIU trucks,

92
00:08:36,660 --> 00:08:39,579
'maintains this headquarters store
for those ashore

93
00:08:39,705 --> 00:08:41,622
'with some refitting to do.

94
00:08:56,138 --> 00:08:58,181
'The SIU's port of call,

95
00:08:58,307 --> 00:09:00,600
'from the figurehead
on the ship-shaped bar

96
00:09:00,726 --> 00:09:02,518
'to the faces of men
you have shipped with,

97
00:09:02,644 --> 00:09:04,770
'a friendly spot for seafarers ashore

98
00:09:04,897 --> 00:09:08,774
'to spend some time to relax
and meet shipmates.

99
00:09:12,821 --> 00:09:14,697
'Or to bring some friends.

100
00:09:16,742 --> 00:09:18,201
(♪ Music over dialogue)

101
00:09:29,588 --> 00:09:31,631
'But the hiring hall
through which every job

102
00:09:31,757 --> 00:09:34,508
'on every SIU ship import is filled

103
00:09:34,635 --> 00:09:36,177
'is the centre of it all.

104
00:09:36,303 --> 00:09:40,598
'Here, seafarers get their jobs
not as a favour but as a right,

105
00:09:40,724 --> 00:09:43,309
'in the order
in which they apply for them.

106
00:09:49,942 --> 00:09:53,986
'When a seafarer registers for a berth,
he gets a registration card.

107
00:09:54,112 --> 00:09:56,614
'The card filled out
right in front of him

108
00:09:56,740 --> 00:10:00,576
'is stamped with a number marked
with the date of his application.

109
00:10:00,702 --> 00:10:02,245
'Then this information,

110
00:10:02,371 --> 00:10:05,581
name, job classification,
date and number,

111
00:10:05,707 --> 00:10:09,252
'is typed onto a list,
which is posted in full view.'

112
00:10:26,395 --> 00:10:32,149
On the vessel, US petroleum T2 tanker.
Going Far East from Boston.

113
00:10:32,276 --> 00:10:35,611
(Hollenbeck) 'When two or more
seafarers throw in for the same job,

114
00:10:35,737 --> 00:10:37,697
'the one with the registration card

115
00:10:37,823 --> 00:10:40,199
'showing the earlier date
and lower number gets it,

116
00:10:40,325 --> 00:10:44,787
'and any seafarer in the hall
can check these for himself.'

117
00:10:45,664 --> 00:10:47,748
And three boats...

118
00:10:50,085 --> 00:10:52,670
Wednesday, April 22nd.

119
00:10:54,339 --> 00:10:58,092
Berth one.
Berth group one, April 22nd.

120
00:10:58,760 --> 00:11:01,804
(Hollenbeck) 'What's more,
a seafarer can pass up a berth

121
00:11:01,930 --> 00:11:04,557
'because he would rather work
another type of ship,

122
00:11:04,683 --> 00:11:06,642
'or one headed for his favourite ports

123
00:11:06,768 --> 00:11:09,770
'without losing his place
on top of the list.'

124
00:11:18,322 --> 00:11:20,156
Two-deck maintenance.

125
00:11:22,743 --> 00:11:24,702
Do you want to stay over there?

126
00:11:32,169 --> 00:11:34,837
All right, group one.

127
00:11:35,922 --> 00:11:38,716
April 24th and April 25th.

128
00:11:40,469 --> 00:11:42,720
Berth group one, April 25th.

129
00:11:47,225 --> 00:11:49,935
(Hollenbeck) 'Shipping call
is every hour on the hour,

130
00:11:50,062 --> 00:11:54,106
'but there's no standing in line
or idling on a sidewalk in between.

131
00:11:54,232 --> 00:11:56,025
'A seafarer waiting for a ship

132
00:11:56,151 --> 00:11:59,070
'doesn't have to kill time
in SIU headquarters.

133
00:11:59,196 --> 00:12:00,905
'He can enjoy it.

134
00:12:06,787 --> 00:12:08,788
'He can shoot a game of pool.

135
00:12:33,105 --> 00:12:36,524
'He can shoot the breeze,
write a letter, read a book.

136
00:12:49,246 --> 00:12:51,163
'Or play some cards.

137
00:12:56,878 --> 00:12:59,797
'He can try his hand
at one of these games.

138
00:13:11,852 --> 00:13:13,686
'Or take a look at works of art

139
00:13:13,812 --> 00:13:17,898
'by fellow seafarers - carvings
and paintings done aboard ship.

140
00:13:23,572 --> 00:13:26,115
'Some good enough for any gallery.

141
00:13:34,583 --> 00:13:39,128
'And for those staying ashore awhile,
there's an SIU school.'

142
00:13:41,882 --> 00:13:44,800
(Tutor) Well, we've pretty much
covered that subject.

143
00:13:44,926 --> 00:13:46,844
All of us have had our say.

144
00:13:46,970 --> 00:13:49,305
And we come to the next question.

145
00:13:49,431 --> 00:13:53,017
That is, what does a union
mean to you?

146
00:13:53,935 --> 00:13:55,311
George?

147
00:13:55,437 --> 00:13:58,439
I'd say protection on the job,
security.

148
00:13:58,565 --> 00:14:00,858
It means the boss
can't shove me around.

149
00:14:00,984 --> 00:14:03,068
Anyone else? Benny?

150
00:14:03,195 --> 00:14:05,863
The big thing is
how you take home pay.

151
00:14:05,989 --> 00:14:10,117
Overtime, along with the penalty pay,
it buys a load of groceries.

152
00:14:10,994 --> 00:14:13,954
Any other ideas? Yes, Paul.

153
00:14:14,581 --> 00:14:16,499
I look at it this way,

154
00:14:16,625 --> 00:14:19,793
I see other guys doing things together,

155
00:14:19,920 --> 00:14:21,378
I want to be part of it.

156
00:14:23,632 --> 00:14:25,883
(Hollenbeck) 'Being part of its group,

157
00:14:26,009 --> 00:14:29,512
'the SIU provided benefits of many kinds
to the seafarer and his family,

158
00:14:29,638 --> 00:14:32,848
'when he is likely to need them most.

159
00:14:32,974 --> 00:14:35,434
'All of them financed
by company contributions

160
00:14:35,560 --> 00:14:37,853
'required by SIU contracts

161
00:14:37,979 --> 00:14:41,899
'and costing seafarers not a cent.

162
00:14:42,025 --> 00:14:45,945
'You can sum these benefits up
in one word: security.

163
00:14:46,071 --> 00:14:47,947
'The security seafarers gain

164
00:14:48,073 --> 00:14:51,325
'by working together
in their common interest.

165
00:15:08,051 --> 00:15:09,510
(♪ Music over dialogue)

166
00:15:19,271 --> 00:15:22,064
'For example, there's
the maternity benefit of $200

167
00:15:22,190 --> 00:15:25,192
'to help a seafarer and his family.

168
00:15:28,488 --> 00:15:30,155
(♪ Music over dialogue)

169
00:15:49,217 --> 00:15:52,136
'Plus, a $25 bond for baby.

170
00:15:55,307 --> 00:15:57,308
'And later on,
the children of seafarers

171
00:15:57,434 --> 00:16:00,644
'have a chance
at SIU college scholarships.

172
00:16:03,064 --> 00:16:05,399
'Seafarers become eligible
for vacation pay

173
00:16:05,525 --> 00:16:07,943
'after every 90 days
they have worked.

174
00:16:08,069 --> 00:16:12,489
'And in the SIU, as every seafarer
knows, there is no red tape.

175
00:16:12,616 --> 00:16:17,077
'This whole operation
takes five minutes or a little more.

176
00:16:35,639 --> 00:16:38,182
'The SIU never loses sight of the fact

177
00:16:38,308 --> 00:16:40,976
'that it exists to serve
its seafaring members

178
00:16:41,102 --> 00:16:44,938
'in every way it can
wherever they may be.

179
00:16:45,065 --> 00:16:48,567
'That is why seafarers created the SIU.

180
00:16:58,286 --> 00:17:01,497
'Every hospitalised seafarer knows,
at least once a week,

181
00:17:01,623 --> 00:17:05,209
'an SIU representative
will come by to say hello

182
00:17:05,335 --> 00:17:07,544
'to fill personal requests.

183
00:17:22,477 --> 00:17:24,144
(♪ Music over dialogue)

184
00:17:30,235 --> 00:17:35,197
'And pay the SIU's weekly
hospital benefit of $15 in cash.

185
00:17:53,299 --> 00:17:57,052
'And this goes on for as long
as a seafarer is hospitalised,

186
00:17:57,178 --> 00:18:00,139
'weeks, months or years.

187
00:18:06,187 --> 00:18:09,565
'For disabled seafarers
or those too old to go to sea,

188
00:18:09,691 --> 00:18:14,528
'the SIU weekly disability benefit
of $25 comes in handy.

189
00:18:17,699 --> 00:18:21,076
'Seafarers' gains through the SIU
have been won step by step

190
00:18:21,202 --> 00:18:25,456
'in victories on ships of Isthmian,
City Service and other lines,

191
00:18:25,582 --> 00:18:28,375
'through the solidarity
of seafarers themselves,

192
00:18:28,501 --> 00:18:30,753
'backed by the SIU.

193
00:18:30,879 --> 00:18:35,257
'Of one SIU victory on a tanker fleet,
a senate committee said,

194
00:18:35,383 --> 00:18:37,760
"'It is amazing that any union
could survive

195
00:18:37,886 --> 00:18:41,305
"'this heavily financed,
lawyer-led attack."

196
00:18:47,479 --> 00:18:50,189
'But winning through
the democratic process

197
00:18:50,315 --> 00:18:52,649
'of collective bargaining
is an SIU habit,

198
00:18:52,776 --> 00:18:57,571
'as these headlines from its newspaper
the "Seafarers' Log" show.

199
00:19:11,711 --> 00:19:15,798
'The "Seafarers' Log", often called
the country's best labour paper,

200
00:19:15,924 --> 00:19:18,759
'is airmailed to SIU ships everywhere,

201
00:19:18,885 --> 00:19:22,638
'keeping seafarers informed
of SIU progress.

202
00:19:22,764 --> 00:19:25,140
'Progress such as this.

203
00:19:25,266 --> 00:19:29,728
'Shipping lines under contract
up from eight to 84.

204
00:19:29,854 --> 00:19:35,734
'SIU assets up from less
than $100,000 to close to $3 million.

205
00:19:37,362 --> 00:19:41,323
'Cash benefits to seafarers
now running into millions.

206
00:19:52,961 --> 00:19:57,214
'Seafarers' earnings tripled
through SIU contracts.

207
00:19:57,340 --> 00:19:58,966
'Impressive, isn't it?

208
00:20:04,472 --> 00:20:07,683
'With the best contracts
in the maritime industry,

209
00:20:07,809 --> 00:20:12,312
'the SIU makes sure that seafarers
get the full benefit of those contracts.

210
00:20:12,438 --> 00:20:15,524
'An SIU representative
goes aboard all incoming ships

211
00:20:15,650 --> 00:20:17,192
'to represent the crew

212
00:20:17,318 --> 00:20:20,195
'in ironing out all grievances,
claims and disputes

213
00:20:20,321 --> 00:20:22,281
'before the payoff.

214
00:20:41,342 --> 00:20:44,303
'This SIU representation
assures every seafarer

215
00:20:44,429 --> 00:20:46,889
'that he will be paid off in full.

216
00:20:47,015 --> 00:20:49,600
'A seafarers' union, in doing its job,

217
00:20:49,726 --> 00:20:53,395
'must meet and service
its members aboard ship.

218
00:21:09,787 --> 00:21:13,165
'The SIU brings its services
to its members,

219
00:21:13,291 --> 00:21:16,418
'not waiting for the members
to come to it.

220
00:21:33,061 --> 00:21:35,938
'Working on board ship,
every seafarer knows that his job,

221
00:21:36,064 --> 00:21:39,691
'his rights on his job
and his rights and interests ashore

222
00:21:39,817 --> 00:21:44,821
'are fully protected through
his cooperation with other seafarers,

223
00:21:44,948 --> 00:21:48,492
'not only in his crew but in his union.

224
00:22:08,888 --> 00:22:11,306
'This explains why SIU organisers,

225
00:22:11,432 --> 00:22:14,267
'when they approach
members of unorganised crews,

226
00:22:14,394 --> 00:22:15,978
'get a friendly welcome.

227
00:22:27,824 --> 00:22:30,575
'And real cooperation
in providing SIU support

228
00:22:30,702 --> 00:22:33,996
'for those who want the benefits
of SIU membership.

229
00:22:34,122 --> 00:22:37,457
'Membership in the union that
represents seafarers effectively,

230
00:22:37,583 --> 00:22:42,754
'because it is controlled by seafarers
in the democratic way.

231
00:22:42,880 --> 00:22:45,549
'All officers of the SIU are elected.

232
00:22:45,675 --> 00:22:48,552
'No one can be
a candidate for SIU office,

233
00:22:48,678 --> 00:22:51,555
'unless he has spent
at least three years at sea,

234
00:22:51,681 --> 00:22:56,059
'a guarantee that he will understand
seafarers' problems.

235
00:22:58,521 --> 00:23:00,355
'On ship and on shore,

236
00:23:00,481 --> 00:23:04,651
'all SIU policies are decided
by the majority vote of seafarers

237
00:23:04,777 --> 00:23:08,113
'with plenty of time for discussion.

238
00:23:10,491 --> 00:23:12,492
'Every SIU meeting
begins with a statement

239
00:23:12,618 --> 00:23:15,537
'reminding seafarers of the rights
guaranteed them

240
00:23:15,663 --> 00:23:18,331
'in the SIU Constitution,

241
00:23:18,458 --> 00:23:21,960
'calling on them
to exercise those rights.

242
00:23:24,797 --> 00:23:26,465
'The right of any seafarer

243
00:23:26,591 --> 00:23:30,010
'to nominate himself or any
other seafarer as a meeting officer,

244
00:23:30,136 --> 00:23:32,137
'or to any committee.

245
00:24:03,503 --> 00:24:07,589
'The right of any seafarer
to speak his mind on any issue.

246
00:24:21,854 --> 00:24:23,980
'The right to vote on every issue.

247
00:24:24,107 --> 00:24:27,609
'The right to straight-talk
from SIU officers.

248
00:24:27,735 --> 00:24:29,444
'Listen to Paul Hall,

249
00:24:29,570 --> 00:24:32,697
'Secretary Treasurer
of the SIU Atlantic and Gulf district.'

250
00:24:38,871 --> 00:24:42,791
Fellow chairmen,
officers of the meeting,

251
00:24:42,917 --> 00:24:44,584
fellow seafarers,

252
00:24:44,710 --> 00:24:47,796
you have heard
in the previous part of this meeting,

253
00:24:47,922 --> 00:24:50,465
our organiser makes the report,

254
00:24:50,591 --> 00:24:54,010
as we have petitioned
a very large unorganised company

255
00:24:54,137 --> 00:24:57,055
for the purpose
of representing their employees

256
00:24:57,181 --> 00:24:58,932
for collective bargaining reasons.

257
00:25:00,143 --> 00:25:02,144
You know, as I do,

258
00:25:02,270 --> 00:25:05,564
that the seamen
in that particular company

259
00:25:05,690 --> 00:25:07,858
are wont to vote in our favour.

260
00:25:08,693 --> 00:25:10,777
Those men are going to vote
in our favour

261
00:25:10,903 --> 00:25:13,905
because they know
what kind of a union that we are

262
00:25:14,031 --> 00:25:18,952
in all the things that we represent,
in all the things that we want to do.

263
00:25:19,078 --> 00:25:22,414
They know, for example,
that in this union

264
00:25:22,540 --> 00:25:26,084
we have the most fair,
democratic system of hiring

265
00:25:26,210 --> 00:25:29,129
ever known to any industry.

266
00:25:29,255 --> 00:25:31,423
They know, too,

267
00:25:31,549 --> 00:25:33,466
that we have in this organisation,

268
00:25:33,593 --> 00:25:37,179
the most democratic constitution
in the trade union movement,

269
00:25:37,305 --> 00:25:40,599
whereby no decision can be made
to affect our people,

270
00:25:40,725 --> 00:25:42,684
unless that decision is made

271
00:25:42,810 --> 00:25:45,979
on the floor of meetings
just such as these.

272
00:25:46,105 --> 00:25:48,857
They know, too,
that this organisation

273
00:25:48,983 --> 00:25:51,610
has the finest agreements
ever negotiated

274
00:25:51,736 --> 00:25:53,612
in the entire maritime history.

275
00:25:53,738 --> 00:25:56,489
They know that the seamen
under those agreements

276
00:25:56,616 --> 00:25:58,825
are the highest-paid seamen
in the world.

277
00:25:58,951 --> 00:26:01,870
They know, too,
of our many broad benefits

278
00:26:01,996 --> 00:26:04,831
that we have
in all the payments we make

279
00:26:04,957 --> 00:26:08,877
for hospital benefits,
vacation benefits.

280
00:26:09,003 --> 00:26:11,713
They know, too,
of disability regardless of age,

281
00:26:11,839 --> 00:26:14,007
when you can't work
in this organisation,

282
00:26:14,133 --> 00:26:18,011
you are put on a pension
so that you will be taken care of.

283
00:26:18,137 --> 00:26:21,848
They know of all the death benefits.
They know the maternity benefits.

284
00:26:21,974 --> 00:26:24,851
They know of the fact that
we have scholarship benefits.

285
00:26:26,312 --> 00:26:29,022
The men in that particular fleet
also realise

286
00:26:29,148 --> 00:26:32,234
that we have a tremendous
economic strength here,

287
00:26:32,360 --> 00:26:35,695
an economic strength that
enables us to be protected

288
00:26:35,821 --> 00:26:39,199
against the boss,
if such need arises.

289
00:26:39,325 --> 00:26:43,745
They know, too, that while we do
possess such large economic strength

290
00:26:43,871 --> 00:26:45,956
that we do not abuse that strength.

291
00:26:47,124 --> 00:26:49,584
(Hollenbeck)
'The SIU's economic strength

292
00:26:49,710 --> 00:26:51,878
'used effectively but without abuse

293
00:26:52,004 --> 00:26:54,589
'has meant much to seafarers,

294
00:26:54,715 --> 00:26:58,551
'security,
a higher standard of living,

295
00:26:58,678 --> 00:27:01,888
'a position of respect
in their communities.

296
00:27:26,080 --> 00:27:28,581
'Today, every seafarer
beginning a voyage

297
00:27:28,708 --> 00:27:30,959
'goes up the gang plank
secure in the knowledge

298
00:27:31,085 --> 00:27:33,628
'that he and his family
can depend on the protection

299
00:27:33,754 --> 00:27:35,547
'of a great organisation.

300
00:27:35,673 --> 00:27:37,799
'An organisation
the seafarers themselves,

301
00:27:37,925 --> 00:27:41,886
'with their own intelligence and effort,
have brought into being.'

302
00:27:48,227 --> 00:27:50,603
You've seen the story
of how the seafarers,

303
00:27:50,730 --> 00:27:53,565
conscious of their rights
and their responsibility,

304
00:27:53,691 --> 00:27:55,650
get together
for their common objectives,

305
00:27:55,776 --> 00:27:57,986
dignity, security,

306
00:27:58,112 --> 00:28:02,866
a better way of life for themselves
and for their families.

307
00:28:02,992 --> 00:28:07,120
This is the story of the SIU.