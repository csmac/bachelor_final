1
00:00:02,580 --> 00:00:07,608
I am Sakuma Manabu,
of Kaio Academy.

2
00:00:08,360 --> 00:00:13,627
I am here to play table tennis
against Tsukimoto!

3
00:00:13,932 --> 00:00:16,400
Aren't you worried about Sakuma?

4
00:00:19,471 --> 00:00:25,273
He knows non-competition games
are against club rules.

5
00:00:25,410 --> 00:00:27,874
If he plays one and loses,
he's off the team.

6
00:00:27,875 --> 00:00:30,338
If he plays one and loses,
he's off the team.

7
00:00:37,322 --> 00:00:40,621
I'll play, but you'll lose.

8
00:00:41,193 --> 00:00:44,060
Listen to this!

9
00:00:46,731 --> 00:00:48,221
He'll lose.

10
00:00:49,201 --> 00:00:51,692
What did you say?

11
00:00:52,938 --> 00:00:55,099
Sakuma's going to lose.

12
00:00:55,974 --> 00:00:57,339
Tsukimoto's in there!

13
00:00:57,442 --> 00:00:59,034
Not now!

14
00:00:59,144 --> 00:01:01,374
But he's so cool!

15
00:01:02,113 --> 00:01:04,274
What's happening?

16
00:01:04,449 --> 00:01:06,246
Yuck! It's Hoshino!

17
00:01:07,452 --> 00:01:08,851
Want some sour chips?

18
00:01:09,454 --> 00:01:10,944
They're excellent.

19
00:01:11,056 --> 00:01:12,489
'Suppamucho'.

20
00:01:12,591 --> 00:01:15,424
I thought you quit ping-pong.

21
00:01:15,527 --> 00:01:17,495
I didn't quit living.

22
00:01:19,130 --> 00:01:20,825
Well?

23
00:01:22,801 --> 00:01:25,429
Give this to Tsukimoto for me.

24
00:01:25,537 --> 00:01:27,630
Roger.

25
00:01:28,473 --> 00:01:29,701
After I check it.

26
00:01:29,808 --> 00:01:31,605
Why?!

27
00:01:32,577 --> 00:01:35,774
'Do you have a girlfriend?...

28
00:01:35,881 --> 00:01:37,212
No!

29
00:01:37,315 --> 00:01:40,307
Hattori Mako, Grade 10C!

30
00:01:42,387 --> 00:01:44,378
'Mako Hattori'!

31
00:01:49,327 --> 00:01:51,056
Change serve!

32
00:01:52,797 --> 00:01:54,958
To Kaio Academy.

33
00:02:05,176 --> 00:02:06,700
Damn it!!

34
00:02:07,312 --> 00:02:10,679
Where did I go wrong?

35
00:02:16,821 --> 00:02:19,312
I can't watch this!

36
00:02:20,292 --> 00:02:21,088
Sorry.

37
00:02:21,192 --> 00:02:23,626
Demon! Leave Smile alone!

38
00:02:23,728 --> 00:02:25,320
But he's lousy!

39
00:02:25,430 --> 00:02:28,194
He'll get better with practice.

40
00:02:28,300 --> 00:02:30,768
Practice won't help him!

41
00:02:30,869 --> 00:02:32,359
I don't care.

42
00:02:34,873 --> 00:02:38,138
21-7. Game, Tsukimoto.

43
00:02:43,515 --> 00:02:46,143
Aw, shit!

44
00:02:52,090 --> 00:02:53,819
Why?!

45
00:02:56,328 --> 00:02:59,297
Why is it you and not me?!

46
00:03:03,768 --> 00:03:05,759
I've tried!

47
00:03:07,505 --> 00:03:13,774
Ten, a hundred, ten thousand
times harder than you have!

48
00:03:17,349 --> 00:03:23,481
To impress Mr Kazama...
to beat Peco...

49
00:03:25,290 --> 00:03:28,282
I've lived and breathed
table tennis!

50
00:03:31,730 --> 00:03:34,494
So why is it you?!

51
00:03:35,700 --> 00:03:38,168
You just don't have the talent.

52
00:03:44,209 --> 00:03:45,540
What?!

53
00:03:45,677 --> 00:03:50,341
That's all it is. It's nothing to
start screaming over.

54
00:03:56,121 --> 00:03:58,646
You can go now.

55
00:04:27,986 --> 00:04:30,386
Bye-bye.

56
00:04:44,936 --> 00:04:46,198
Hey!

57
00:04:47,839 --> 00:04:50,899
Where do you think
you're going?

58
00:04:51,009 --> 00:04:54,911
Where do you think
you're going I said!

59
00:04:56,848 --> 00:04:59,214
Wherever the hell I want to!

60
00:04:59,317 --> 00:05:02,650
Where am I going, huh?

61
00:05:03,588 --> 00:05:05,317
Tell me!

62
00:05:27,212 --> 00:05:30,739
Kazama

63
00:05:53,171 --> 00:05:57,073
Rise to Buddha, old buddy.

64
00:06:18,129 --> 00:06:20,324
Home it!

65
00:06:26,638 --> 00:06:30,574
We were just warming up.

66
00:06:32,010 --> 00:06:36,845
I asked for some better balls
to practise with, didn't I?

67
00:06:37,949 --> 00:06:42,545
Oh, yeah... they're on order.

68
00:06:42,654 --> 00:06:44,952
You said that last week.

69
00:06:45,790 --> 00:06:50,159
Look, we can't spend the whole
club budget on you.

70
00:06:51,829 --> 00:06:53,854
What else are we buying?

71
00:07:00,939 --> 00:07:04,466
Hey wait! Tsukimoto!

72
00:07:11,949 --> 00:07:13,814
There's Peco!

73
00:07:16,520 --> 00:07:18,488
Why don't you come any more?

74
00:07:18,589 --> 00:07:21,023
Come and teach us!

75
00:07:21,926 --> 00:07:24,724
Girls don't go for ping-pong players.

76
00:07:28,265 --> 00:07:30,927
There's a guy
there who chops!

77
00:07:32,436 --> 00:07:33,562
A chopper?

78
00:07:34,305 --> 00:07:35,397
The guy with the glasses.

79
00:07:35,506 --> 00:07:36,200
Smile?

80
00:07:36,307 --> 00:07:37,569
Nope.

81
00:07:37,675 --> 00:07:40,974
But an old friend's an old friend.

82
00:07:41,078 --> 00:07:42,545
There!

83
00:07:43,948 --> 00:07:45,347
Gee!

84
00:08:00,131 --> 00:08:01,928
Idiot!

85
00:08:02,033 --> 00:08:04,900
That chop wouldn't cut tofu.

86
00:08:05,636 --> 00:08:07,103
You're right.

87
00:08:15,212 --> 00:08:18,545
See? I'm teaching myself.

88
00:08:18,649 --> 00:08:21,447
I'm totally impressed.

89
00:08:31,162 --> 00:08:36,657
I had to drop out of school but
I want to give it my best shot.

90
00:08:43,841 --> 00:08:48,801
But this watch-and-wait
kind of game...

91
00:08:48,913 --> 00:08:52,110
...just doesn't suit me.

92
00:09:02,693 --> 00:09:05,423
Some birds can't fly.

93
00:09:48,038 --> 00:09:50,131
Keep playing.

94
00:10:04,421 --> 00:10:06,389
You've got the gift.

95
00:10:06,791 --> 00:10:09,726
I'm talking about you, Peco.

96
00:10:11,529 --> 00:10:13,224
Do I know you?

97
00:10:19,336 --> 00:10:22,464
You've got an instinct
for the game.

98
00:10:24,208 --> 00:10:25,937
Bullshit.

99
00:10:27,511 --> 00:10:32,676
The fact is that you beat me
and Smile beat you.

100
00:10:32,783 --> 00:10:35,183
So do something about it.

101
00:10:35,286 --> 00:10:37,186
Go screw yourself.

102
00:10:40,691 --> 00:10:43,683
There are things
only a hack player knows.

103
00:10:51,368 --> 00:10:53,734
I'll say this once.

104
00:10:55,873 --> 00:10:58,364
Run till you puke blood.

105
00:10:59,176 --> 00:11:01,542
Practise till you piss blood.

106
00:11:03,747 --> 00:11:06,147
You'll feel better.

107
00:11:09,453 --> 00:11:11,045
If you don't...

108
00:11:12,790 --> 00:11:18,285
...you'll let down the ones
who've always admired you...

109
00:11:18,395 --> 00:11:20,386
...me and Smile.

110
00:11:53,731 --> 00:11:56,063
Haven't seen him.

111
00:12:00,337 --> 00:12:03,568
Coaching is more than just
teaching technique.

112
00:12:03,707 --> 00:12:04,799
Aw, shuddup.

113
00:12:06,810 --> 00:12:13,443
It's trial and error.
I've bust my ass over him.

114
00:12:16,387 --> 00:12:19,618
That's why he took off.

115
00:12:21,859 --> 00:12:26,728
If there's no love in it,
you'd better leave him alone.

116
00:12:43,347 --> 00:12:44,939
Young man!

117
00:12:48,485 --> 00:12:53,320
Don't do it, son.
It's not worth it.

118
00:12:54,692 --> 00:13:00,062
I had a bad day at the track.
All I've got is Y60...

119
00:13:00,164 --> 00:13:02,155
...but I'm not giving up.

120
00:13:02,333 --> 00:13:05,825
What'll your parents think
if you die?

121
00:13:08,539 --> 00:13:09,767
Die?

122
00:13:10,140 --> 00:13:15,908
Yeah. You'll never amount
to anything if you're dead.

123
00:13:16,013 --> 00:13:17,742
I'm not going to die.

124
00:13:17,848 --> 00:13:19,179
No?

125
00:13:19,783 --> 00:13:21,045
I'm going to fly.

126
00:13:21,151 --> 00:13:23,517
Yeah? Great!

127
00:13:23,988 --> 00:13:26,354
I can touch the moon
no problem!

128
00:13:26,457 --> 00:13:28,322
That's the spirit!

129
00:13:43,207 --> 00:13:46,040
Did I make you do that?!

130
00:13:54,752 --> 00:13:55,844
Are you strong?

131
00:14:02,826 --> 00:14:05,454
...taste iron in blood.

132
00:14:12,169 --> 00:14:13,966
Hey, Peco...

133
00:14:15,839 --> 00:14:17,238
... do you believe in heroes?

134
00:15:39,496 --> 00:15:41,020
You're still here?

135
00:15:45,268 --> 00:15:47,133
A coach isn't a coach...

136
00:15:49,506 --> 00:15:51,303
...without a player.

137
00:15:52,676 --> 00:15:54,667
Next time I'll kick your ass.

138
00:16:38,054 --> 00:16:39,180
Now I remember...

139
00:16:42,692 --> 00:16:44,785
...the one time...

140
00:16:49,065 --> 00:16:51,397
...that Smile smiled.

141
00:17:09,252 --> 00:17:10,981
This isn't a barber shop.

142
00:17:13,590 --> 00:17:19,392
Show me again, will you?
Right from how you grip the racket.

143
00:17:41,284 --> 00:17:44,583
5:04...no good! Do it again!

144
00:17:45,489 --> 00:17:46,956
Granny!

145
00:17:47,390 --> 00:17:49,153
What?

146
00:17:50,494 --> 00:17:55,227
I said from how you grip
the racket, didn't I?

147
00:17:55,332 --> 00:17:56,663
Yep.

148
00:17:57,467 --> 00:18:00,698
When do I start hitting?
I've been running for two weeks.

149
00:18:00,937 --> 00:18:03,872
Until you beat three minutes.

150
00:18:03,974 --> 00:18:05,498
Do it again.

151
00:18:21,491 --> 00:18:22,719
Nice day.

152
00:18:25,228 --> 00:18:27,219
Don't you have...

153
00:18:29,399 --> 00:18:35,133
...someone to pack you a lunch
on a sunny day like this?

154
00:18:35,238 --> 00:18:37,172
I've got a girlfriend.

155
00:18:39,743 --> 00:18:43,941
Just kidding. You don't have to
be surprised like that.

156
00:18:46,316 --> 00:18:47,943
Wanna go out?

157
00:18:51,187 --> 00:18:52,916
Tsukimoto!

158
00:18:57,060 --> 00:19:00,894
Hey, Tsukimoto! Hi!

159
00:19:01,565 --> 00:19:04,898
Isn't this fun, Mr Tsukimoto?

160
00:19:07,337 --> 00:19:12,798
Are you having fun?
If you're having fun, smile!

161
00:19:12,909 --> 00:19:14,774
I would if I was.

162
00:19:16,780 --> 00:19:19,271
You would, huh?

163
00:19:19,382 --> 00:19:21,543
Damn right!

164
00:19:35,599 --> 00:19:39,330
Stop puking!
You'll hurt my feelings!

165
00:19:41,938 --> 00:19:46,102
I'm your honey! Get up here
and put your arms around me.

166
00:19:48,411 --> 00:19:50,276
Stop puking!

167
00:19:51,881 --> 00:19:53,246
Butterfly Joe?

168
00:19:54,451 --> 00:19:59,115
Yep. 30 years ago,
he was one of the best.

169
00:20:01,024 --> 00:20:04,050
Was he really good?

170
00:20:05,061 --> 00:20:07,120
Not just good.

171
00:20:07,530 --> 00:20:10,988
He was beautiful to watch.

172
00:20:11,568 --> 00:20:17,564
No one could touch him. He was
the king of Japanese table tennis.

173
00:20:19,843 --> 00:20:24,109
He was a shoo-in
for the national team...

174
00:20:24,914 --> 00:20:26,313
But...

175
00:20:28,618 --> 00:20:36,650
...I had a friend I'd hit against
ever since I could remember.

176
00:20:40,964 --> 00:20:43,558
He was really good...

177
00:20:45,268 --> 00:20:50,296
...but if we were both
in good shape...

178
00:20:51,041 --> 00:20:53,168
...I'd beat him.

179
00:21:06,556 --> 00:21:08,888
A knee injury...

180
00:21:11,361 --> 00:21:14,057
They said to take
six months off.

181
00:21:15,432 --> 00:21:17,696
So you defaulted?

182
00:21:21,104 --> 00:21:22,298
I would have.

183
00:21:25,842 --> 00:21:31,041
But it was my friend
who had the bad knee.

184
00:21:39,089 --> 00:21:40,750
I don't get it.

185
00:21:42,058 --> 00:21:45,585
You could've played him deep
then hit to his backhand.

186
00:21:53,870 --> 00:21:55,804
Could you do that?

187
00:22:00,043 --> 00:22:05,208
Would you take advantage
of a friend's injury...

188
00:22:07,050 --> 00:22:10,042
...and risk ending his career?

189
00:22:17,994 --> 00:22:19,791
Could you do that?

190
00:22:23,566 --> 00:22:25,966
That was my last game.

191
00:22:35,211 --> 00:22:36,178
Let's go.

192
00:22:36,279 --> 00:22:37,678
Coach...

193
00:22:39,749 --> 00:22:42,684
...I've always wanted
to ride that thing.

194
00:22:58,334 --> 00:23:00,666
Well? Did I do it?

195
00:23:01,371 --> 00:23:04,067
I did? I didn't?

196
00:23:04,174 --> 00:23:05,266
Back to the dojo!

197
00:23:10,246 --> 00:23:11,338
I did it!

198
00:23:11,514 --> 00:23:14,779
Three minutes!
Am I awesome or what!

199
00:23:14,884 --> 00:23:17,182
You're great.

200
00:23:17,287 --> 00:23:19,585
We make a great couple!

201
00:23:19,689 --> 00:23:23,147
I love you a whole bunch!

202
00:23:32,735 --> 00:23:33,997
Here...

203
00:23:34,270 --> 00:23:36,864
What you've been waiting for.

204
00:23:40,977 --> 00:23:45,141
You're losing it, Granny.
There's rubber on the back, too.

205
00:23:46,916 --> 00:23:53,185
I'm not losing it. That means
you can turn it over to hit.

206
00:24:02,365 --> 00:24:04,856
I thought you'd forgotten.

207
00:24:04,968 --> 00:24:09,837
We'd just come to the end of
last year's budget.

208
00:24:10,740 --> 00:24:13,402
Plus I didn't know
where to order them.

209
00:24:14,377 --> 00:24:17,346
Will you hit against me
for a bit?

210
00:24:17,447 --> 00:24:23,977
You'd better ask Coach Koizumi.
The Inter-High's getting close.

211
00:24:24,087 --> 00:24:25,349
I'll go easy.

212
00:24:46,442 --> 00:24:47,704
These are good.

213
00:24:49,279 --> 00:24:50,405
It's not the same.

214
00:24:53,016 --> 00:24:54,677
Now for some spin...

215
00:25:41,531 --> 00:25:42,998
Damn!

216
00:26:15,965 --> 00:26:17,296
That's enough!

217
00:26:18,835 --> 00:26:20,496
No!

218
00:26:49,999 --> 00:26:54,459
Tsukimoto! Shouldn't you
be warming up?

219
00:26:56,739 --> 00:26:58,764
Is he listening?

220
00:27:00,309 --> 00:27:02,300
Is he listening?

221
00:27:14,590 --> 00:27:16,319
You'll be late!

222
00:27:16,426 --> 00:27:18,792
Who's my first game?

223
00:27:19,095 --> 00:27:22,792
Don't leave on me
when you find out.

224
00:27:32,442 --> 00:27:34,876
Why do I have to draw China first?

225
00:27:36,079 --> 00:27:38,013
I seriously want to go home!

226
00:27:42,385 --> 00:27:44,376
I'm not losing it.

227
00:27:52,795 --> 00:27:56,253
Placement, control, speed...

228
00:27:56,365 --> 00:27:59,027
...Tsukimoto's in another league.

229
00:27:59,135 --> 00:28:00,466
Hmm?

230
00:28:01,637 --> 00:28:04,003
Oh, Kong Wenge...

231
00:28:04,107 --> 00:28:08,942
He's gotten better this year, too.
He's headed for the top.

232
00:28:09,245 --> 00:28:11,236
I wonder...

233
00:28:14,183 --> 00:28:15,912
He could lose here.

234
00:28:17,820 --> 00:28:19,082
16-20.

235
00:28:19,188 --> 00:28:20,655
Yes!

236
00:28:21,190 --> 00:28:22,748
Alright!

237
00:28:28,097 --> 00:28:30,588
You remember?

238
00:28:30,800 --> 00:28:37,899
The first time we played,
you didn't win a point.

239
00:28:38,875 --> 00:28:44,472
Thanks, Kong.
I learned something from you.

240
00:28:46,048 --> 00:28:50,417
You taught me how to fly.

241
00:29:09,138 --> 00:29:09,763
There!

242
00:29:14,076 --> 00:29:15,475
The back side!

243
00:29:15,945 --> 00:29:17,776
Game! Hoshino!

244
00:29:20,283 --> 00:29:21,944
That's my boy!

245
00:29:22,251 --> 00:29:26,551
Check it out, China.
You might catch me in 300 years!

246
00:29:27,924 --> 00:29:30,484
Hey! You're in a game.

247
00:29:31,627 --> 00:29:32,958
Sorry.

248
00:29:36,933 --> 00:29:39,299
Get a move on, will you?!

249
00:29:39,835 --> 00:29:43,271
Hurry up, Muko,
or I'll leave you here!

250
00:29:48,544 --> 00:29:53,004
Hey, Manabu...there's
a swimming tournament, too.

251
00:29:53,115 --> 00:29:56,983
You can go jump in
with them, then.

252
00:29:57,520 --> 00:30:00,148
What's the matter?

253
00:30:00,423 --> 00:30:03,221
I just said there's swimming.

254
00:30:03,326 --> 00:30:05,556
Swimmers have meets,
not tournaments!

255
00:30:08,464 --> 00:30:11,058
I should've gone to Europe.

256
00:30:11,667 --> 00:30:14,158
Sweden, or Germany...

257
00:30:19,542 --> 00:30:21,601
But I chose Japan...

258
00:30:23,179 --> 00:30:25,670
Say hello to Kazama.

259
00:30:33,756 --> 00:30:38,284
Hoshino
Kong

260
00:30:38,728 --> 00:30:40,389
I could cry...

261
00:30:40,496 --> 00:30:42,225
What?

262
00:30:42,598 --> 00:30:46,329
Did your friend win?

263
00:30:51,040 --> 00:30:53,531
Hello, Mr Kazama.

264
00:30:56,012 --> 00:30:57,536
It's Sakuma.

265
00:30:58,180 --> 00:30:59,647
Sakuma Manabu.

266
00:31:06,322 --> 00:31:08,051
I'm not going to let you...

267
00:31:08,924 --> 00:31:10,653
...show up my school!

268
00:31:16,332 --> 00:31:20,200
How have you been, Sakuma?

269
00:31:25,308 --> 00:31:26,673
What?

270
00:31:29,745 --> 00:31:30,734
I'm doing alright.

271
00:31:38,554 --> 00:31:43,048
Mr Sanada never quits, does he?

272
00:31:49,932 --> 00:31:53,959
If there's one guy he doesn't
want to lose to, it's Tsukimoto.

273
00:31:58,174 --> 00:31:59,300
Sakuma...

274
00:32:01,644 --> 00:32:05,546
...competition is there to show
who's best.

275
00:32:11,153 --> 00:32:14,748
If you say so.

276
00:32:16,792 --> 00:32:19,317
It doesn't matter who says so.

277
00:32:24,066 --> 00:32:28,901
Now that I've quit table tennis,
I finally understand...

278
00:32:32,475 --> 00:32:37,344
...why you shut yourself away
in here.

279
00:32:40,516 --> 00:32:41,642
Funny, huh?

280
00:32:48,591 --> 00:32:53,722
Who do you play for anyway?

281
00:32:59,502 --> 00:33:03,734
Myself, of course.

282
00:33:05,074 --> 00:33:07,542
You're kidding, of course.

283
00:33:10,913 --> 00:33:13,040
If you mean that...

284
00:33:15,718 --> 00:33:17,515
...I was wasting my time.

285
00:33:20,256 --> 00:33:21,883
Be seeing you.

286
00:33:21,991 --> 00:33:23,356
Sakuma!

287
00:33:26,495 --> 00:33:30,431
Do you feel betrayed?

288
00:33:36,639 --> 00:33:38,504
I sympathize.

289
00:33:49,718 --> 00:33:53,586
What's the matter?
Have you got diarrhea?

290
00:33:56,025 --> 00:34:01,429
There's more than diarrhea
that drives a man to the toilet.

291
00:34:02,331 --> 00:34:03,628
What do you mean?

292
00:34:03,732 --> 00:34:05,029
Stay there.

293
00:34:08,037 --> 00:34:09,732
I need to cry a little.

294
00:34:15,044 --> 00:34:19,242
Game, set! Hoshino,
Katase High School.

295
00:34:36,866 --> 00:34:39,528
Semi-final Round

296
00:34:42,972 --> 00:34:46,635
Game, set! Tsukimoto,
Katase High School.

297
00:34:59,255 --> 00:35:03,214
You're in the final!
Way to go, Smile!

298
00:35:03,325 --> 00:35:06,123
Uh...'Tsukimoto'.

299
00:35:06,228 --> 00:35:07,559
Where's Peco?

300
00:35:09,198 --> 00:35:13,658
In the medical room.
His knee's pretty bad.

301
00:35:18,340 --> 00:35:20,774
Aren't you worried about him?

302
00:35:21,143 --> 00:35:24,044
The man's a machine!

303
00:35:24,947 --> 00:35:29,748
I like it... how cool he is.

304
00:35:32,521 --> 00:35:36,287
No problem, baby. Check it out.

305
00:35:40,996 --> 00:35:45,956
I had it retaped, and I got a
shot of pain-killer.

306
00:35:46,669 --> 00:35:50,070
You don't know what
a bad knee can do.

307
00:35:50,172 --> 00:35:53,733
Excuse me... it's time for
your semi-final.

308
00:35:55,044 --> 00:35:55,703
I'm coming.

309
00:35:55,811 --> 00:35:56,937
No, he's not.

310
00:36:04,119 --> 00:36:10,183
Listen, Peco... I know how bad
you want to play Kazama.

311
00:36:10,292 --> 00:36:16,060
But if you want to be the best
in the world someday...

312
00:36:16,799 --> 00:36:22,465
...you've got to have the guts
to raise the white flag.

313
00:36:34,950 --> 00:36:37,441
Smile's calling me.

314
00:36:49,231 --> 00:36:52,689
He's been waiting
for a long time.

315
00:36:55,904 --> 00:37:00,341
For ages and ages...

316
00:37:02,344 --> 00:37:04,005
...he's believed in me.

317
00:37:08,550 --> 00:37:12,213
I knew it,
but I pretended I didn't.

318
00:37:14,623 --> 00:37:18,354
It scared me.
I covered my ears against it.

319
00:37:22,564 --> 00:37:25,192
But he's waiting for me.

320
00:37:30,205 --> 00:37:31,536
Peco...

321
00:37:36,945 --> 00:37:38,970
...I love you, kid.

322
00:37:43,052 --> 00:37:46,613
Stand back! Stand back!
Competitor coming through!

323
00:37:49,992 --> 00:37:54,725
Stand back, stand back! There's
a player coming through.

324
00:38:21,090 --> 00:38:23,354
Do you believe in heroes?

325
00:38:27,196 --> 00:38:28,720
Heroes?

326
00:38:35,504 --> 00:38:37,028
You OK?

327
00:38:39,208 --> 00:38:40,675
Your knee...

328
00:38:43,946 --> 00:38:47,677
I don't know if it'll hold up.

329
00:38:49,852 --> 00:38:52,377
But you're a high-flier.

330
00:38:54,389 --> 00:38:57,552
Way high up there.

331
00:38:58,026 --> 00:39:01,427
So I'll just climb your back,
stand on your shoulders and...

332
00:39:02,531 --> 00:39:03,691
...fly.

333
00:39:04,399 --> 00:39:07,232
A hero shows up in the clutch...

334
00:39:08,237 --> 00:39:13,265
...and saves me, no matter
how deep a hole I'm in.

335
00:39:15,244 --> 00:39:16,939
That's a hero.

336
00:39:17,179 --> 00:39:18,976
Here goes!

337
00:39:20,749 --> 00:39:22,410
After I beat you,
go out and get...

338
00:39:24,853 --> 00:39:25,945
...a haircut.

339
00:39:26,789 --> 00:39:30,486
Go get some hair.

340
00:39:40,335 --> 00:39:46,205
I really need to take a piss!

341
00:40:16,939 --> 00:40:19,203
He jumped over to his backhand.

342
00:40:20,242 --> 00:40:23,302
He won't admit that Hoshino's good.

343
00:40:24,513 --> 00:40:26,276
Amazing!

344
00:40:29,384 --> 00:40:31,318
Nice shot.

345
00:40:32,955 --> 00:40:34,445
But...

346
00:40:36,124 --> 00:40:40,823
...you won't be there forever
looking down at everyone.

347
00:40:42,531 --> 00:40:44,362
'Enter the hero.'

348
00:40:46,735 --> 00:40:48,635
I'm the hero.

349
00:40:51,874 --> 00:40:53,899
It's me.

350
00:40:54,910 --> 00:40:56,571
He comes back.

351
00:40:57,379 --> 00:40:59,472
My name is...

352
00:40:59,581 --> 00:41:01,105
...Hoshino...

353
00:41:01,450 --> 00:41:02,576
...Yutaka!

354
00:41:08,056 --> 00:41:08,605
Remember that!

355
00:41:08,606 --> 00:41:09,155
Remember that!

356
00:41:41,657 --> 00:41:43,488
15-2.

357
00:41:44,459 --> 00:41:46,051
What's wrong...

358
00:41:47,229 --> 00:41:48,560
...'hero'?

359
00:41:50,465 --> 00:41:51,989
What's wrong?

360
00:41:54,303 --> 00:41:56,032
Aren't you the hero?

361
00:41:58,473 --> 00:41:59,804
Can't you fly?!

362
00:42:11,453 --> 00:42:13,978
He thinks he's fated to win.

363
00:42:16,458 --> 00:42:18,323
Maybe for Kazama...

364
00:42:18,427 --> 00:42:20,952
...table tennis is pain.

365
00:42:29,571 --> 00:42:32,165
There's a strength in that.

366
00:42:33,875 --> 00:42:38,278
Looks like Peco made him mad.

367
00:42:44,453 --> 00:42:45,579
The back!

368
00:42:46,221 --> 00:42:47,188
Use the back!

369
00:42:52,527 --> 00:42:53,323
Right!

370
00:42:59,001 --> 00:43:00,298
Kid!

371
00:43:08,643 --> 00:43:14,843
Don't go pulling
those cheap trick shots on me!

372
00:43:18,820 --> 00:43:20,788
It sucks!

373
00:43:25,894 --> 00:43:29,421
First game's yours, Dragon boy.

374
00:43:35,504 --> 00:43:37,631
Love you, man.

375
00:43:37,739 --> 00:43:40,139
Game, Kazama!

376
00:44:29,624 --> 00:44:31,023
Does it hurt?

377
00:44:36,765 --> 00:44:38,562
You're a nice guy.

378
00:44:40,168 --> 00:44:43,296
So why not give me this game?

379
00:44:47,209 --> 00:44:48,403
Clown!

380
00:44:48,710 --> 00:44:49,972
You asked!

381
00:44:57,519 --> 00:44:59,077
Right?

382
00:44:59,921 --> 00:45:01,513
Left?

383
00:45:02,157 --> 00:45:05,649
Stop thinking, Peco!

384
00:45:11,199 --> 00:45:13,099
Not that way!

385
00:45:16,438 --> 00:45:21,398
I can't keep this up!!

386
00:45:29,985 --> 00:45:31,612
Oh, man...

387
00:45:32,754 --> 00:45:35,018
...here we go again!

388
00:45:35,323 --> 00:45:37,291
Ow!

389
00:45:43,465 --> 00:45:46,263
Shit!

390
00:45:53,542 --> 00:45:55,442
Help me!

391
00:45:57,479 --> 00:45:58,639
Please!

392
00:46:08,223 --> 00:46:10,623
Enter the hero!

393
00:46:12,727 --> 00:46:14,957
Enter the hero!

394
00:46:17,065 --> 00:46:19,795
Enter the hero!

395
00:46:23,939 --> 00:46:26,339
Is he good?

396
00:46:27,776 --> 00:46:29,141
Yeah.

397
00:46:30,946 --> 00:46:33,779
Table tennis incarnate.

398
00:46:33,882 --> 00:46:35,975
You can have fun here.

399
00:46:37,319 --> 00:46:39,250
Your knee's no problem.
Listen to it.

400
00:46:39,251 --> 00:46:41,181
Your knee's no problem.
Listen to it.

401
00:46:48,330 --> 00:46:49,388
Can you fly?

402
00:47:01,376 --> 00:47:03,640
What's going on?

403
00:47:05,080 --> 00:47:06,206
Don't ask me.

404
00:47:06,781 --> 00:47:07,543
Hey!

405
00:47:08,783 --> 00:47:10,512
I'm ready to roll!

406
00:47:12,320 --> 00:47:13,252
What?

407
00:47:19,594 --> 00:47:22,654
The speed of sound!!

408
00:47:23,164 --> 00:47:25,029
The speed of light!!

409
00:47:25,133 --> 00:47:25,690
Yes!

410
00:47:25,800 --> 00:47:26,459
And beyond.

411
00:47:30,438 --> 00:47:31,769
Faster!

412
00:47:32,474 --> 00:47:33,668
Back!!

413
00:47:34,676 --> 00:47:36,268
I get it...

414
00:47:47,822 --> 00:47:49,585
The hero...

415
00:49:02,831 --> 00:49:04,526
Game, Hoshino!

416
00:49:37,899 --> 00:49:39,890
Kazama's in trouble.

417
00:49:41,236 --> 00:49:42,828
Maybe.

418
00:49:49,644 --> 00:49:50,941
Don't fall back.

419
00:49:51,513 --> 00:49:52,878
Play up!

420
00:49:54,382 --> 00:49:55,542
Shut up!

421
00:49:56,117 --> 00:49:57,175
You bother me!

422
00:50:04,092 --> 00:50:08,358
Kazama's smiling!

423
00:50:15,336 --> 00:50:18,737
Hoshino's not sticking to one pattern.

424
00:50:21,743 --> 00:50:26,442
It's like he's playing
for the sheer joy of it.

425
00:50:27,515 --> 00:50:28,641
Yeah.

426
00:50:29,250 --> 00:50:31,582
Playing someone like that...

427
00:50:35,490 --> 00:50:37,117
...is fantastic.

428
00:51:29,177 --> 00:51:33,113
Match point, Dragon.

429
00:51:40,655 --> 00:51:42,122
You're going to win?

430
00:51:51,933 --> 00:51:53,366
What the hell.

431
00:51:55,169 --> 00:51:56,466
It was fun.

432
00:52:00,141 --> 00:52:02,200
I like it here.

433
00:52:12,854 --> 00:52:14,378
Bring me here again...

434
00:52:16,257 --> 00:52:17,815
...'hero'.

435
00:53:39,374 --> 00:53:41,604
You guys haven't
changed a bit!

436
00:53:41,709 --> 00:53:43,267
You sure have.

437
00:53:43,378 --> 00:53:47,508
That kid with
the bangs was cool!

438
00:53:49,584 --> 00:53:50,846
Yeah.

439
00:53:51,586 --> 00:53:54,749
You're not jealous?

440
00:53:54,856 --> 00:53:56,881
You're no fun!

441
00:53:56,991 --> 00:54:00,620
Final:
Tsukimoto vs. Hoshino

442
00:54:01,829 --> 00:54:07,131
And now, the 2001 Inter-High
Men's Table Tennis Final.

443
00:54:13,207 --> 00:54:15,072
Captain Ota...

444
00:54:16,944 --> 00:54:19,412
...is Hoshino's knee OK?

445
00:54:20,248 --> 00:54:21,715
He's coming.

446
00:54:24,419 --> 00:54:25,443
I see.

447
00:54:25,553 --> 00:54:28,283
So who should we cheer for?

448
00:54:28,389 --> 00:54:31,552
Both of them of course!

449
00:54:31,693 --> 00:54:34,526
You cheer whenever there's a point.

450
00:54:36,831 --> 00:54:38,264
But...

451
00:54:41,269 --> 00:54:44,295
You can yell, Ota.

452
00:54:46,541 --> 00:54:49,339
Tsukimoto!

453
00:54:52,714 --> 00:54:54,375
What's with you?

454
00:54:55,783 --> 00:54:59,310
Tsukimoto... I probably
don't have to say this...

455
00:54:59,420 --> 00:55:00,512
I'll play to win.

456
00:55:03,257 --> 00:55:07,284
I'm not like you, 'Butterfly Joe'.

457
00:55:13,601 --> 00:55:20,131
You're the best player
I've ever seen.

458
00:55:29,183 --> 00:55:33,916
Stand aside! Stand aside!
Competitor coming through!

459
00:55:44,298 --> 00:55:44,889
Viva!

460
00:55:44,999 --> 00:55:45,829
You're late.

461
00:55:45,933 --> 00:55:48,060
Don't be like that.

462
00:55:48,503 --> 00:55:51,063
I'm here to kick your ass.

463
00:56:02,316 --> 00:56:03,840
The final match!

464
00:56:03,951 --> 00:56:10,015
Hoshino, Katase High,
vs. Tsukimoto, Katase High!

465
00:56:32,079 --> 00:56:36,880
I'm getting too old
to stand and watch.

466
00:56:38,352 --> 00:56:39,683
Yeah.

467
00:56:52,233 --> 00:56:55,862
And it doesn't matter
who wins.

468
00:57:03,377 --> 00:57:08,713
Get Hoshino to a hospital
when this is over, OK?

469
00:57:11,018 --> 00:57:13,282
I know.

470
00:57:40,648 --> 00:57:42,172
Go, go, Hoshino!

471
00:57:42,483 --> 00:57:44,075
Push, push, Hoshino!

472
00:57:44,185 --> 00:57:45,812
Go, go, Tsukimoto!

473
00:57:45,920 --> 00:57:47,581
Push, push, Tsukimoto!

474
00:58:15,616 --> 00:58:16,844
Let's go, buddy!

475
00:58:19,253 --> 00:58:20,668
Welcome back, hero.

476
00:58:20,669 --> 00:58:22,084
Welcome back, hero.

477
00:58:32,633 --> 00:58:33,429
Smile...

478
00:58:34,602 --> 00:58:39,232
...don't squeeze the ball
when you serve.

479
00:58:40,174 --> 00:58:43,940
And hold it up
above the table.

480
00:58:44,345 --> 00:58:47,007
It has to bounce here first.

481
00:58:47,415 --> 00:58:49,144
Try it.

482
00:58:49,250 --> 00:58:50,911
OK.

483
00:58:53,120 --> 00:58:54,712
I'll try it.

484
01:00:19,073 --> 01:00:21,507
Don't swing so far.

485
01:00:23,110 --> 01:00:26,705
Take the ball, gently...

486
01:00:26,814 --> 01:00:30,272
Bring your arm down, and hit.

487
01:00:30,618 --> 01:00:34,554
Hit it well forward...

488
01:00:35,122 --> 01:00:36,783
...as it's rising.

489
01:00:39,727 --> 01:00:43,720
OK? Can you do that?
You try it.

490
01:00:44,698 --> 01:00:46,723
Yeah, yeah, that's right.

491
01:01:18,132 --> 01:01:19,929
Thank you, Peco.

492
01:01:20,734 --> 01:01:22,634
'Mr Peco', please.

493
01:01:23,037 --> 01:01:25,471
'Mr Peco', please.

494
01:01:55,269 --> 01:01:57,829
Enter the hero!

