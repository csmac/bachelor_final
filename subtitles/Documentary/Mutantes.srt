1
00:00:39,172 --> 00:00:45,839
A fiIm by
Virginie Despentes

2
00:02:31,117 --> 00:02:32,846
Summer 2005.

3
00:02:33,019 --> 00:02:36,785
I cross the U.S. From west to
east, from Seattle to New York,

4
00:02:36,956 --> 00:02:39,754
through Los Angeles
and San Francisco.

5
00:02:39,926 --> 00:02:43,987
I meet with activists,
photographers, artists,

6
00:02:44,164 --> 00:02:47,600
fiImmakers, sex workers
and theorists,

7
00:02:47,734 --> 00:02:51,431
who in the 80s began a
revolutionary movement:

8
00:02:52,805 --> 00:02:55,740
A pro-sex feminism,
pro-whores,

9
00:02:55,909 --> 00:02:59,436
porn that is feminist,
S&M, lesbian.

10
00:03:05,985 --> 00:03:07,418
When we speak of feminism,

11
00:03:07,587 --> 00:03:10,317
we often assume
abolitionist feminism.

12
00:03:10,490 --> 00:03:12,355
It treats any pornographic image

13
00:03:12,525 --> 00:03:16,393
as an attack on the dignity of women
and calls for its censureship.

14
00:03:16,563 --> 00:03:20,021
It sees all prostitution as
merchandizing of the human body

15
00:03:20,233 --> 00:03:22,360
and calls
for its criminalization.

16
00:03:24,837 --> 00:03:27,863
Pro-sex feminism has the
opposite strategy:

17
00:03:28,041 --> 00:03:30,509
The body, pleasure,
pornography,

18
00:03:30,677 --> 00:03:35,011
and sex work can be political
tools which one must seize.

19
00:03:35,315 --> 00:03:38,409
Here, the discussion of those
directly involved

20
00:03:38,585 --> 00:03:41,019
outweighs the word
of the experts.

21
00:03:42,889 --> 00:03:44,948
Annie Sprinkle,
Norma Jean Almodovar,

22
00:03:45,124 --> 00:03:46,853
Carol Queen or Scarlot Harlot

23
00:03:47,026 --> 00:03:49,927
have all written about
prostitution in the first person.

24
00:03:50,597 --> 00:03:53,361
Freed from censorship
and victimization,

25
00:03:53,533 --> 00:03:56,900
they claim the right to use
their bodies as they see fit,

26
00:03:57,070 --> 00:03:59,800
including for sex work.

27
00:26:49,240 --> 00:26:54,177
The concept of obscenity is a
totalitarian tool to reduce women,

28
00:26:54,346 --> 00:26:57,213
ultimately, to a piece of flesh.

29
00:26:57,382 --> 00:27:00,283
What is that in sex,
especially women's sex,

30
00:27:00,452 --> 00:27:02,716
what is it in the human
sexual act

31
00:27:03,455 --> 00:27:05,685
that is so shameful?

32
00:27:05,857 --> 00:27:08,758
It's more than the ugliness,
it's... a rejection.

33
00:27:08,927 --> 00:27:11,088
People want to vomit,
people...

34
00:27:11,262 --> 00:27:16,632
or they're fine with it,
with lust, etc...

35
00:27:16,868 --> 00:27:18,597
or in a salacious way.

36
00:27:18,803 --> 00:27:22,364
But to finally find
the simple humanity,

37
00:27:22,540 --> 00:27:25,668
just the beauty,
with ugliness mixed in,

38
00:27:25,844 --> 00:27:28,369
with people as they are,

39
00:27:28,546 --> 00:27:32,073
it's in this delicate balance,
finally, that it's touching.

40
00:27:32,584 --> 00:27:34,415
There are few people
who recognize it,

41
00:27:34,586 --> 00:27:38,044
and who, if not the fiImmakers
can show it

42
00:27:38,189 --> 00:27:41,818
with human material, that is
to say, with the true sexes?

43
00:30:18,750 --> 00:30:20,980
I see absolutely no
objective argument

44
00:30:21,152 --> 00:30:24,053
that allows one to say that
porn is degrading to women.

45
00:30:30,829 --> 00:30:34,697
I had my first professional
shoot in December.

46
00:30:34,866 --> 00:30:37,892
The following ApriI I had my
first cover of Hot Vid�o.

47
00:30:38,069 --> 00:30:41,630
After three months, I fell face
to face with myself in the street,

48
00:30:41,806 --> 00:30:44,570
and it's true that the effect
is quite striking.

49
00:30:55,220 --> 00:30:59,156
Being a porn star,
for me it was

50
00:30:59,324 --> 00:31:03,556
to make a fool of myself maybe
once or twice a month on a set.

51
00:31:04,028 --> 00:31:06,997
It took maybe 10 hours of my
life per month. It was nothing.

52
00:31:07,165 --> 00:31:08,757
What does it mean,
"pornography?"

53
00:31:08,933 --> 00:31:11,663
It's a look focused
on the human sexual act.

54
00:31:11,903 --> 00:31:15,862
It carries a prejudice

55
00:31:16,574 --> 00:31:19,737
and ethics outside
the Judeo-Christian norms

56
00:31:19,911 --> 00:31:22,880
because of nudity, obscenity,

57
00:31:23,514 --> 00:31:25,812
an act of indignity to humans.

58
00:31:25,984 --> 00:31:29,818
It's an indignity to humans
even to believe these things.

59
00:31:29,988 --> 00:31:34,823
Society states that the woman
is a vault of dignity

60
00:31:34,993 --> 00:31:38,861
that is emptied when she's
committing sexual acts.

61
00:31:51,242 --> 00:31:53,767
I think people are perverts,
frustrated,

62
00:31:53,945 --> 00:31:55,936
and that's their problem,
whatever.

63
00:31:56,114 --> 00:31:58,742
Because ultimately, the people
who butt fuck, etc...

64
00:31:59,050 --> 00:32:01,848
Sometimes I've been insulted
by guys in the street.

65
00:32:02,020 --> 00:32:06,252
And I said, "Wait, if you find
what I'm doing so dirty,

66
00:32:06,357 --> 00:32:08,518
"it's because you've
already seen what I do.

67
00:32:08,693 --> 00:32:10,251
"You must be interested.

68
00:32:10,528 --> 00:32:13,156
"You see, you're curious
about it, so you look.

69
00:32:13,331 --> 00:32:16,789
"If people like you didn't watch,
there'd be no girls like me."

70
00:32:52,236 --> 00:32:56,730
Judgment is strong because people
won't recognize the fascination,

71
00:32:56,908 --> 00:32:59,103
the strength of the fascination.

72
00:32:59,277 --> 00:33:04,180
And it's necessarily the object
of their desire that causes desire.

73
00:33:04,382 --> 00:33:07,044
And because it's repressed,
it's wrong... I don't know.

74
00:33:07,218 --> 00:33:09,186
People are completely twisted.

75
00:33:09,354 --> 00:33:12,118
At least, much more than me
when I was doing porn.

76
00:53:26,036 --> 00:53:28,527
"Queer" is first of all
an insult, in fact.

77
00:53:28,705 --> 00:53:31,799
Actually it's very important,
it really is an insult.

78
00:53:32,309 --> 00:53:36,302
It's an insult that means
fag, dyke, asshole,

79
00:53:36,479 --> 00:53:39,846
deranged, sexual sicko, whore.

80
00:53:40,016 --> 00:53:44,919
It indicates a sexual deviation,

81
00:53:45,021 --> 00:53:48,252
a dissent from the sexual norm.

82
00:53:48,658 --> 00:53:51,650
And so that insult has been

83
00:53:51,828 --> 00:53:56,925
reclaimed by a set of micro-
communities in the United States.

84
00:53:58,735 --> 00:54:05,766
In the late 1980s, they turned
this insult on its head.

85
00:54:06,009 --> 00:54:07,476
It's quite incredible,

86
00:54:07,644 --> 00:54:15,050
this hijacking of an insult
thrown at you.

87
00:54:15,285 --> 00:54:18,186
You pick it up and actually
start working with the injury,

88
00:54:18,321 --> 00:54:19,948
and an identity develops.

89
00:54:42,479 --> 00:54:44,379
There was a huge surge

90
00:54:44,547 --> 00:54:48,210
in psychoanalytic and sociological
discourse on femininity

91
00:54:48,385 --> 00:54:51,912
as construction,
as parody, as performance.

92
00:54:52,455 --> 00:54:55,913
Besides, the seminal text

93
00:54:56,226 --> 00:54:58,717
was by a psychoanalyst
named Jean Rivi�re.

94
00:54:58,895 --> 00:55:03,491
It became the basis of
all analyses, Queer analyses.

95
00:55:03,900 --> 00:55:09,600
They actually began to speak of
femininity just as a masquerade.

96
00:55:09,773 --> 00:55:15,302
This performative or
parodic analysis...

97
00:55:15,478 --> 00:55:19,380
Well, it wouldn't happen the same
way in relation to masculinity.

98
00:55:19,482 --> 00:55:21,177
That's to say on the one hand

99
00:55:21,351 --> 00:55:24,149
there's a deconstructive swell
concerning femininity,

100
00:55:24,254 --> 00:55:27,189
and on the other hand,
a huge silence about masculinity.

101
00:55:27,357 --> 00:55:31,088
Masculinity appears as
a natural absolute,

102
00:55:31,261 --> 00:55:33,092
and no one, in fact,
thinks about it.

103
00:57:10,493 --> 00:57:14,156
I don't like to make hierarchies,
in fact,

104
00:57:14,330 --> 00:57:16,298
about gender, sexuality,
saying there are...

105
00:57:16,499 --> 00:57:18,729
well there are butches,
there are drag queens.

106
00:57:18,835 --> 00:57:20,803
Then there are transgenders
and transsexuals.

107
00:57:20,937 --> 00:57:25,271
I think they have different ways
of reclaiming a multiplicity

108
00:57:25,442 --> 00:57:27,103
of the technologies of gender.

109
00:57:27,310 --> 00:57:30,370
I refer the concept
of Teresa Lorite,

110
00:57:30,547 --> 00:57:32,310
these kinds of technologies
of gender.

111
00:57:33,116 --> 00:57:35,710
And these technologies in fact,
they are often performative,

112
00:57:35,819 --> 00:57:39,255
that is,
they are only parodies or...

113
00:57:39,422 --> 00:57:41,515
It's works about representation,

114
00:57:41,691 --> 00:57:47,220
but sometimes they're biopolitical
technologies in a wider sense.

115
00:57:47,397 --> 00:57:50,798
And so we can work equally well
with photography as with fiIm,

116
00:57:50,934 --> 00:57:52,902
with a performance,
with a hormone.

117
00:57:53,002 --> 00:57:55,266
And for me they're different...
different ways

118
00:57:55,438 --> 00:57:58,839
of actually reclaiming the technologi
of production of the genre.

119
00:58:40,049 --> 00:58:43,177
Contemporary society,
in any case from the 1950s,

120
00:58:43,353 --> 00:58:47,255
is a society in which
bio-women are...

121
00:58:47,557 --> 00:58:50,890
systematically pumped
with estrogen.

122
00:58:51,060 --> 00:58:54,223
Every girl, in fact, from
the age of 15, 16 years

123
00:58:54,397 --> 00:59:00,427
wiII take the piII and be fiIled
with estrogen and hence hormones,

124
00:59:00,603 --> 00:59:03,766
in line with creating
femininity in her body.

125
00:59:04,607 --> 00:59:09,476
To my knowledge, there are very
few boys with access to estrogen,

126
00:59:09,646 --> 00:59:14,811
and it's not because it's hard
to find estrogen in our society.

127
00:59:14,984 --> 00:59:17,111
N the other hand,
testosterone,

128
00:59:17,287 --> 00:59:20,745
and here's a radical
political shift,

129
00:59:20,924 --> 00:59:24,792
testosterone continues
to be a hormone

130
00:59:25,094 --> 00:59:26,618
and also a political metaphor,

131
00:59:26,796 --> 00:59:31,392
the use of which is stiII
extremely restricted.

132
00:59:31,834 --> 00:59:40,936
That's to say that testosterone
is controlled, prescribed today

133
00:59:41,110 --> 00:59:43,977
either medically or for the
production of transsexuality

134
00:59:44,080 --> 00:59:47,277
in the body of bio-woman. That is,
for transformation into a man, period

135
01:01:00,456 --> 01:01:04,222
Now, pro-sex feminism
has crossed the Atlantic.

136
01:01:04,861 --> 01:01:07,295
These European inheritors
combine punk rock,

137
01:01:07,463 --> 01:01:10,227
gender politics and
trans/queer/dyke

138
01:01:10,333 --> 01:01:13,302
to make visible the
dissident sexual practices.

139
01:01:14,137 --> 01:01:17,595
They refuse that the state or the
cultural industry decide for them

140
01:01:17,774 --> 01:01:20,038
what is presentable
and what is not.

141
01:01:24,347 --> 01:01:28,477
Mainstream pornography imposes
on us restrictive requirements

142
01:01:28,651 --> 01:01:30,983
of what is a man
and what is a woman,

143
01:01:31,154 --> 01:01:34,851
who does what to whom, what
angle, how many participants.

144
01:01:36,259 --> 01:01:38,921
Pornographic representation
is a war.

145
01:01:39,095 --> 01:01:42,292
To seize it is to
change the imaginary,

146
01:01:42,465 --> 01:01:44,933
to construct identities of resistance

147
01:01:45,702 --> 01:01:50,435
That image is experimental,
funny, edgy or exciting.

148
01:01:50,606 --> 01:01:54,906
Nly one instruction: Do not
leave intact any regulatory code.

149
01:06:39,862 --> 01:06:44,697
The Postporn movement is a new
stage of the feminist revolution.

150
01:06:45,568 --> 01:06:47,866
By staging a deviant sexuality

151
01:06:48,037 --> 01:06:50,267
that doesn't address
the hetero male eye,

152
01:06:50,439 --> 01:06:52,964
one chips away in the brain
of the beholder

153
01:06:53,075 --> 01:06:55,669
the resistances that others
had implanted.

154
01:06:57,380 --> 01:07:00,975
Postpornography undermines
the castrating fiIters

155
01:07:01,150 --> 01:07:03,141
that define virility:

156
01:07:03,352 --> 01:07:05,377
A man does not touch
the cock of another man,

157
01:07:05,554 --> 01:07:08,421
and his anus should
always remain locked.

158
01:07:11,060 --> 01:07:14,427
In Barcelona, the new warriors
of Postporn are punks,

159
01:07:14,597 --> 01:07:16,792
insecure,
feminist and politicized.

160
01:07:16,966 --> 01:07:19,764
Armed with diIdos, mustaches,
fisting, prostheses

161
01:07:19,935 --> 01:07:22,233
and post-movida
comic characters,

162
01:07:22,405 --> 01:07:25,841
they claim all that mainstream
pornography forbids.

163
01:07:26,108 --> 01:07:30,169
Here, Mad Max
confronts Judith Butler.

164
01:07:39,989 --> 01:07:43,618
In 200, at MACBA, the Museum of
Contemporary Art in Barcelona,

165
01:07:43,793 --> 01:07:47,729
the marathon Postporno invited
artists, activists, marginals

166
01:07:47,897 --> 01:07:50,058
and theorists to work together

167
01:07:50,232 --> 01:07:54,100
on an investigation
around pornography.

168
01:07:54,303 --> 01:07:57,329
Postporn had reached
its critical mass.

169
01:08:47,690 --> 01:08:50,853
Ah, follow, follow.

170
01:08:51,293 --> 01:08:54,262
Fuck me, fuck <i>me. - I'm going
to make you work a bit.

171
01:08:54,430 --> 01:08:56,193
I'll make you wait.

172
01:08:56,499 --> 01:08:58,729
I'll make you wait.
- Smash my ass.

173
01:08:58,901 --> 01:09:00,926
Now, I'll break your ass.

174
01:09:01,137 --> 01:09:02,934
I'll split you in two.

175
01:09:03,105 --> 01:09:05,232
Nine years ago,
when I arrived in Barcelona,

176
01:09:05,441 --> 01:09:10,140
a Postporn movement
began to hatch.

177
01:09:10,279 --> 01:09:12,645
Queer, trans-gay-lesbian,

178
01:09:12,848 --> 01:09:16,045
I was a part of it
from the beginning,

179
01:09:16,218 --> 01:09:18,516
and it continues to expand.

180
01:09:18,888 --> 01:09:21,652
There have been many results.

181
01:09:21,824 --> 01:09:24,987
It implements
different politics,

182
01:09:25,094 --> 01:09:26,561
much more aggressive.

183
01:09:39,842 --> 01:09:44,575
When we started Girlswholikeporno,
it was very militant queer,

184
01:09:44,747 --> 01:09:48,342
very involved in the queer
movement in Barcelona.

185
01:09:48,517 --> 01:09:51,418
We were in the background:
Queeruption, Judith Butler...

186
01:09:53,722 --> 01:09:55,781
Confusion of gender,

187
01:09:55,991 --> 01:09:59,825
against dichotomies:
Man/woman, hetero/homo.

188
01:09:59,995 --> 01:10:03,260
That was the birthplace of the
project, Girlswholikeporno.

189
01:10:25,387 --> 01:10:30,381
I find it politically very
interesting and contentious

190
01:10:30,693 --> 01:10:35,323
that women reappropriate
the stigma of the whore.

191
01:10:35,764 --> 01:10:38,631
In Spanish, the words
"bitch" and "fox"

192
01:10:38,934 --> 01:10:41,198
are often used as pejoratives

193
01:10:41,370 --> 01:10:44,464
to describe women,
especially free women.

194
01:10:44,807 --> 01:10:47,901
I like to call myself
or my girlfriends

195
01:10:48,077 --> 01:10:50,307
"sluts" or "bitches,"

196
01:10:50,479 --> 01:10:53,676
and I find that usage
very liberating.

197
01:10:53,849 --> 01:10:56,579
It's no longer to argue,
"I'm good, I'm not a whore."

198
01:10:56,685 --> 01:10:58,949
Kay, I'm a whore.
It's my decision.

199
01:11:00,022 --> 01:11:02,991
<i>On a beautiful spring morning,</i>

200
01:11:03,726 --> 01:11:06,718
<i>there was once a prostitute</i>

201
01:11:06,895 --> 01:11:08,487
<i>looking for clients,</i>

202
01:11:09,198 --> 01:11:11,723
<i>in order to make some cash.</i>

203
01:11:12,901 --> 01:11:15,734
<i>She looked and looked,</i>

204
01:11:16,205 --> 01:11:17,832
<i>Until at last,</i>

205
01:11:18,307 --> 01:11:20,707
<i>after several attempts,</i>

206
01:11:20,876 --> 01:11:22,867
<i>appeared a gypsy,</i>

207
01:11:23,045 --> 01:11:26,071
<i>his briefcase</i>
<i>stuffed with euros.</i>

208
01:11:30,853 --> 01:11:34,084
<i>When they arrive</i>
<i>at their destination,</i>

209
01:11:34,256 --> 01:11:35,587
<i>the whore</i>

210
01:11:35,758 --> 01:11:39,091
<i>begins to put the gypsy</i>
<i>in a position</i>

211
01:11:39,361 --> 01:11:41,955
<i>that is vulnerable</i>
<i>and defenseless.</i>

212
01:11:43,432 --> 01:11:47,493
<i>She presents him to</i>
<i>her friend, the bitch,</i>

213
01:11:47,670 --> 01:11:49,194
<i>and offers him</i>

214
01:11:49,371 --> 01:11:52,101
<i>what appears to be</i>
<i>a bunch of grapes,</i>

215
01:11:52,274 --> 01:11:53,866
<i>ready to eat.</i>

216
01:11:54,143 --> 01:11:55,701
<i>By now,</i>

217
01:11:56,378 --> 01:11:59,438
<i>the Gypsy is caught</i>
<i>between the claws</i>

218
01:11:59,682 --> 01:12:02,116
<i>of these two unwise women.</i>

219
01:12:03,085 --> 01:12:05,110
<i>The poor victim</i>

220
01:12:05,220 --> 01:12:09,247
<i>cannot or doesn't want to</i>
<i>imagine what awaits him.</i>

221
01:12:10,759 --> 01:12:14,024
Don't ask me how it happened,
but we've put together

222
01:12:14,129 --> 01:12:16,825
a lot of people who share
the same interests,

223
01:12:17,433 --> 01:12:20,129
who are friends,
are all productive,

224
01:12:20,302 --> 01:12:24,295
and there's a way of life...

225
01:12:25,574 --> 01:12:30,102
ur leisure time and our time
of political activity has merged.

226
01:12:30,279 --> 01:12:34,477
We formed a network,
compact and powerful,

227
01:12:34,650 --> 01:12:37,881
that's giving us good results
at this time.

228
01:12:38,053 --> 01:12:43,218
This network also influences
our individual work.

229
01:12:43,392 --> 01:12:49,422
Everything produced is the
result of ongoing cooperation.

230
01:13:21,663 --> 01:13:24,928
F course, our work
nourishes feminism.

231
01:13:25,100 --> 01:13:28,069
Even if we've never fought
in feminist groups,

232
01:13:28,237 --> 01:13:30,000
it is influenced,

233
01:13:30,105 --> 01:13:33,768
especially by feminist artists
since the 1970s.

234
01:13:35,344 --> 01:13:38,939
Ur main influence is mainly
from within the queer discourse,

235
01:13:39,047 --> 01:13:41,015
but this is not incompatible.

236
01:13:41,183 --> 01:13:44,050
N the contrary,
it forms a whole.

237
01:13:45,721 --> 01:13:47,814
Let me take your picture.

238
01:13:50,292 --> 01:13:54,092
Do what you want.
Do you have a light?

239
01:13:57,166 --> 01:13:59,259
Are you from around here?

240
01:13:59,435 --> 01:14:01,232
Wait, don't move.

241
01:14:01,403 --> 01:14:02,870
Give me a light, bastard!

242
01:14:02,971 --> 01:14:05,906
No photos.
Give me a light...

243
01:14:06,942 --> 01:14:08,671
I'm doing it, okay?
Where are you going?

244
01:14:08,844 --> 01:14:10,471
In my house, fuck...

245
01:14:10,646 --> 01:14:12,341
I want to fuck, too...

246
01:14:12,581 --> 01:14:13,570
Go fuck your mother.

247
01:14:13,749 --> 01:14:14,738
Your mother!

248
01:14:14,883 --> 01:14:16,475
She fucks like hell.

249
01:14:29,832 --> 01:14:32,392
Call me when you
know how to eat pussy!

250
01:14:33,001 --> 01:14:36,903
It's part of the relationship
between the two of us,

251
01:14:37,072 --> 01:14:40,303
a desire to play
and try things,

252
01:14:40,476 --> 01:14:43,877
especially in connection with
photos, to explore sexuality.

253
01:14:44,213 --> 01:14:49,048
Before discovering Postporn,
it was, at first, a game.

254
01:14:49,218 --> 01:14:52,153
Then, our readings enabled us
to situate ourselves.

255
01:14:52,321 --> 01:14:55,950
I used a diIdo before I knew
what it means.

256
01:15:20,849 --> 01:15:23,613
I developed myself
against masculinity.

257
01:15:23,785 --> 01:15:25,116
Because of my history,

258
01:15:25,387 --> 01:15:28,686
at some point a rejection
of "bio-men."

259
01:15:28,857 --> 01:15:31,121
And to find new meaning
in this rejection,

260
01:15:31,293 --> 01:15:34,558
and to invent other types
of practices,

261
01:15:34,730 --> 01:15:37,460
I enrolled in feminism.

262
01:15:38,734 --> 01:15:41,202
I'll shit on everything

263
01:15:41,370 --> 01:15:45,033
with my crazy bitch voice.

264
01:15:49,177 --> 01:15:52,374
Finally, I have a pussy.

265
01:15:53,215 --> 01:15:54,807
I didn't choose it,

266
01:15:54,983 --> 01:15:57,042
but it does not displease me.

267
01:15:57,352 --> 01:15:59,946
I am a capricious kid
who wants it all.

268
01:16:00,822 --> 01:16:03,120
Perpetually dissatisfied.

269
01:16:03,292 --> 01:16:05,453
It's possible that today
these tools,

270
01:16:05,627 --> 01:16:07,857
these weapons are destroyed.

271
01:16:08,030 --> 01:16:09,588
It is clear that the
lesbian scenes

272
01:16:09,765 --> 01:16:12,757
aim get rid of all the
old stereotypes of femininity.

273
01:16:13,602 --> 01:16:16,662
We can say that the scenes are
the sexualization of power,

274
01:16:16,838 --> 01:16:19,068
the sexualization
of strategic relationships.

275
01:16:19,241 --> 01:16:23,678
What struck me in the scenes is
how it unfurls to see beneath her.

276
01:16:24,613 --> 01:16:26,672
The game in the scene
is very interesting,

277
01:16:26,848 --> 01:16:29,078
because although its time has
a strategic relationship,

278
01:16:29,251 --> 01:16:30,616
it is always fluid.

279
01:16:30,852 --> 01:16:32,547
There are roles, of course,

280
01:16:32,721 --> 01:16:35,781
but everyone knows very well that
these roles can be reversed.

281
01:16:35,958 --> 01:16:39,655
Sometimes when the games begin,
one is the master,

282
01:16:39,828 --> 01:16:41,352
the other the slave,

283
01:16:41,530 --> 01:16:44,931
and at the end the slave
becomes the master.

284
01:16:45,734 --> 01:16:49,329
What seduces me about Postporn
is its experimental side,

285
01:16:49,504 --> 01:16:52,598
trying to talk about sex,

286
01:16:53,275 --> 01:16:55,869
of sex and our desires,

287
01:16:56,044 --> 01:16:57,944
with other codes.

288
01:17:00,749 --> 01:17:03,616
Sometimes it works,
sometimes it doesn't.

289
01:17:03,785 --> 01:17:05,446
Sometimes
it is incomprehensible,

290
01:17:05,554 --> 01:17:09,354
sometimes you read the opposite
of what you expected.

291
01:17:09,658 --> 01:17:12,252
But this line of research

292
01:17:12,427 --> 01:17:14,987
is what really
interests me in Postporn.

293
01:17:15,397 --> 01:17:17,729
I find it very exciting.

294
01:17:17,899 --> 01:17:19,366
For me, Postporn...

295
01:17:19,534 --> 01:17:23,197
I believe my own sexuality
when I make Postporn.

296
01:17:23,372 --> 01:17:27,399
I do not reproduce it,
I create it.

297
01:17:27,743 --> 01:17:29,734
It's like a moment of creation.

298
01:17:30,078 --> 01:17:31,909
To break the barriers and say:

299
01:17:32,080 --> 01:17:35,948
"Now I think I want,
that which I invent?"

300
01:17:37,853 --> 01:17:41,118
Yes, what was very important
for both of us

301
01:17:41,289 --> 01:17:44,156
is that sexuality
is not an essence,

302
01:17:44,593 --> 01:17:48,654
but a creative activity,
buiIt little by little,

303
01:17:48,830 --> 01:17:52,061
from which we create
concepts such as gender.

304
01:17:53,969 --> 01:17:56,233
The aim of Postporn

305
01:17:56,405 --> 01:17:59,602
is not only to excite,
like mainstream porn,

306
01:17:59,775 --> 01:18:00,935
but to construct

307
01:18:01,109 --> 01:18:03,737
and deconstruct
majority discourses.

308
01:18:04,179 --> 01:18:05,942
That's a creation.

309
01:18:06,114 --> 01:18:08,639
Yes, sexuality is
an artistic creation.

310
01:18:09,151 --> 01:18:12,552
Previously, we weren't represented
in mainstream pornography.

311
01:18:12,788 --> 01:18:14,585
Or were represented by others.

312
01:18:14,756 --> 01:18:18,192
So we took the reins
to make our own porn,

313
01:18:18,326 --> 01:18:22,786
adapted to our physical realities,
our practices and our tastes.

314
01:19:18,820 --> 01:19:24,258
The punk subculture seeks
to liberate itself from

315
01:19:24,493 --> 01:19:29,294
established production structures
and production companies

316
01:19:29,464 --> 01:19:31,955
and create its own
independent labels,

317
01:19:32,134 --> 01:19:35,331
its own media,

318
01:19:35,937 --> 01:19:38,906
to organize its own festivals,

319
01:19:39,074 --> 01:19:41,508
its own events,

320
01:19:41,676 --> 01:19:43,371
conferences, etc...

321
01:19:43,545 --> 01:19:46,912
All this with a reduced budget.

322
01:19:47,249 --> 01:19:48,682
It gives strength

323
01:19:48,850 --> 01:19:51,216
to notice that four
or five colleagues

324
01:19:51,386 --> 01:19:53,286
can achieve so many things.

325
01:19:53,455 --> 01:19:57,323
We must reclaim
our sexual representations,

326
01:19:57,726 --> 01:19:59,523
in order to stop hiding.

327
01:19:59,694 --> 01:20:02,561
I am not trying to
escape patriarchy

328
01:20:02,731 --> 01:20:04,995
to enter the convent of feminists,

329
01:20:05,167 --> 01:20:09,331
where I cannot show a breast
or speak sexually.

330
01:20:09,805 --> 01:20:12,296
Giving visibility to our pleasure

331
01:20:12,474 --> 01:20:14,374
and to our twisted sexuality

332
01:20:14,543 --> 01:20:17,307
seems essential to me
in order to emancipate us

333
01:20:17,479 --> 01:20:19,743
and control the way
we're represented.