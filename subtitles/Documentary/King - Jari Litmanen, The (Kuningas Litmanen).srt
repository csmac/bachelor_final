1
00:00:04,100 --> 00:00:06,100
“The King Loves” is the story about

2
00:00:06,300 --> 00:00:13,000
Prince Wang Won, attractive woman Eun San and prince Wang Won’s only friend, Wang Rin.

3
00:00:13,180 --> 00:00:17,330
The drama traces the turbulent love triangle and the romance.

4
00:00:22,000 --> 00:00:27,600
I played “Wang Won” in this drama. To introduce more,

5
00:00:27,600 --> 00:00:31,400
he’s the crown prince during the Goryeo Dynasty.

6
00:00:31,600 --> 00:00:39,700
It’s about King Chungseon’s story before he ruled the country.

7
00:00:41,290 --> 00:00:47,120
He is a profound character, and has both good and evil w

8
00:00:49,000 --> 00:00:54,000
I played the unpredictable character Eun San who is a daughter of the wealthiest merchant in the Goryeo Dynasty.

9
00:00:54,500 --> 00:01:01,200
She is loved by Wang Won and Wang Rin.

10
00:01:10,500 --> 00:01:11,700
What did you think of me?

11
00:01:11,900 --> 00:01:13,500
For me, at first,

12
00:01:13,610 --> 00:01:19,490
we just ran into each other a few times.

13
00:01:19,510 --> 00:01:24,100
But we didn’t have a chance to work together or to talk.

14
00:01:25,200 --> 00:01:29,700
Before getting to know him,

15
00:01:29,720 --> 00:01:34,500
he looked very handsome and acted well.

16
00:01:35,700 --> 00:01:39,100
So I was curious how he was in person.

17
00:01:39,190 --> 00:01:41,150
When meeting him,

18
00:01:41,200 --> 00:01:49,900
he is much more masculine than I thought and he is still professional.

19
00:01:49,930 --> 00:01:50,580
Kind

20
00:01:50,660 --> 00:01:51,870
I’m about to say that.

21
00:01:52,000 --> 00:01:54,700
He is kind and very very

22
00:01:54,830 --> 00:01:55,660
Delicate

23
00:01:55,730 --> 00:01:58,630
He is delicate, and took care of me a lot.

24
00:01:59,300 --> 00:02:07,500
He was helpful during the filming and I learned a lot from him,

25
00:02:07,620 --> 00:02:14,500
so he was a wonderful male partner for me.

26
00:02:16,000 --> 00:02:20,100
Thanks. I didn’t know you thought of me like that.

27
00:02:20,300 --> 00:02:29,000
Before filming the drama, she debuted in Girls’ Generation before me.

28
00:02:29,300 --> 00:02:37,000
And she was working actively when I was a trainee.

29
00:02:40,460 --> 00:02:48,890
Her popularity and power of influence were amazing from the beginning.

30
00:02:48,950 --> 00:02:54,820
Of course, her charms are great.

31
00:02:59,600 --> 00:03:03,700
When I heard she would be my partner in the drama,

32
00:03:03,850 --> 00:03:05,220
[when hearing],

33
00:03:05,300 --> 00:03:11,100
I felt blessed to have the honor of working with such a celebrity.

34
00:03:11,400 --> 00:03:12,400
Thanks a lot.

