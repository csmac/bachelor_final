

159
00:11:43,135 --> 00:11:48,198
If you learn where
he is, I beg you, let me know.

160
00:11:49,575 --> 00:11:53,341
I just want to make sure he is OK.

161
00:11:53,412 --> 00:11:59,146
He won't let me know by phone
because you kill people with the phone.

162
00:11:59,218 --> 00:12:03,917
The Russians killed the president
of Chechnya with the phone,

163
00:12:05,791 --> 00:12:10,421
and you Americans have killed
people with the phone.

164
00:12:10,496 --> 00:12:15,661
This, a telephone,
has become a lethal weapon!

169
00:12:31,117 --> 00:12:34,450
I would like to tell you something,

170
00:12:34,520 --> 00:12:36,954
the hungry

171
00:12:37,022 --> 00:12:41,015
who cannot find bread to eat

172
00:12:41,093 --> 00:12:43,960
in Afghanistan

173
00:12:44,029 --> 00:12:47,055
they wouldn't accept
these $25 million of yours

174
00:12:47,133 --> 00:12:51,467
to turn in a Muslim they know
is waging jihad for them.

176
00:12:54,640 --> 00:12:57,404
You say so.


177
00:12:57,476 --> 00:13:00,104
America says so. America says so.

178
00:13:00,179 --> 00:13:02,875
America says.

181
00:13:11,657 --> 00:13:15,115
The seed that was sown by America,

182
00:13:15,194 --> 00:13:18,493
a seed of spitefulness, hate,

183
00:13:18,564 --> 00:13:21,328
aggression and killing,

184
00:13:21,400 --> 00:13:26,337
cannot disappear in the short term.

185
00:13:28,407 --> 00:13:32,969
There has to be another foreign
policy to uproot this seed.

190
00:13:44,924 --> 00:13:48,883
The Americans
are known to be fighting

191
00:13:48,961 --> 00:13:52,021
the Egyptians, Palestinians and Iraqis.

193
00:13:54,733 --> 00:13:57,293
Yes.

195
00:14:02,608 --> 00:14:04,633
You want to occupy Egypt.

197
00:14:06,045 --> 00:14:08,878
We're not that well versed in politics

198
00:14:08,948 --> 00:14:11,815
but that's what we hear on TV and stuff.

200
00:14:13,285 --> 00:14:16,982
They are good people, but
we reject the American foreign policy.

207
00:14:39,278 --> 00:14:42,543
It's about oppression, because
Egypt is a developing country,

208
00:14:42,615 --> 00:14:45,550
so, like, the people who are
oppressed are kind of more...

209
00:14:45,618 --> 00:14:48,678
So it develops more terrorists,
and that's it.

210
00:14:48,754 --> 00:14:54,090
These would be
the minority, the rotten type.

211
00:14:54,159 --> 00:14:55,990
Every society has the same.

212
00:14:56,061 --> 00:14:58,256
Every society has good and bad.

271
00:18:37,483 --> 00:18:39,815
The United States is
the main problem in the region, frankly.

272
00:18:39,885 --> 00:18:44,584
We love the American people, but
we hate the American government.

273
00:18:44,656 --> 00:18:47,489
We pray to God
to destroy you

274
00:18:47,559 --> 00:18:50,722
because of what's happening.

277
00:18:56,368 --> 00:18:59,633
OK, I don't want you to be there
when it happens.

278
00:18:59,705 --> 00:19:01,138
Have a smell.

298
00:20:16,949 --> 00:20:20,350
While we are
living in these troubled times,

299
00:20:21,153 --> 00:20:24,919
we need to be merciful.

300
00:20:24,990 --> 00:20:28,926
So get mercy through praises
and prayers upon Him...

301
00:20:30,495 --> 00:20:33,658
...because He is mercy.

302
00:20:33,732 --> 00:20:37,691
And He puts compassion in your heart.

350
00:23:50,595 --> 00:23:53,962
He might be
somewhere here in Morocco.

351
00:23:58,270 --> 00:23:59,601
How should I know?

355
00:24:10,549 --> 00:24:12,176
Centuries.

357
00:24:15,654 --> 00:24:20,648
It is not from Islam
to kill innocent people

358
00:24:20,725 --> 00:24:25,355
or to blow yourself up.

359
00:24:26,731 --> 00:24:28,699
I'm disgusted by that.

361
00:24:32,170 --> 00:24:33,535
We worry about that.

362
00:24:33,605 --> 00:24:37,939
We don't want them to get involved
with that kind of crowd.

363
00:24:38,009 --> 00:24:42,105
That's why we have to take care
of them financially.

364
00:24:47,819 --> 00:24:50,413
Americans are
causing all the trouble in the world.

366
00:24:51,656 --> 00:24:54,352
But this is my opinion
of the American government,

367
00:24:54,426 --> 00:24:55,757
not the American people.

368
00:24:55,827 --> 00:24:58,455
These are all
the problems that Bush created.

369
00:24:58,530 --> 00:25:00,395
I heard that they want
to eradicate Islam.

372
00:25:14,112 --> 00:25:16,103
Osama bin Laden,
look how many people he's killed,

373
00:25:16,181 --> 00:25:18,513
how many have died.
And what has he accomplished?

403
00:27:06,858 --> 00:27:10,350
Islam obligates us to be
this way, to be brothers with everyone.

405
00:27:14,699 --> 00:27:18,931
You should take care of your wife.

407
00:27:21,039 --> 00:27:24,133
If your relationship is based
on love and good values,

408
00:27:24,209 --> 00:27:26,939
you will have good children.

411
00:27:31,483 --> 00:27:32,472
It's like a sardine.

413
00:27:33,652 --> 00:27:35,517
I'm glad. Eat with good health.

415
00:27:40,258 --> 00:27:43,284
I want my kids to get an education.

416
00:27:43,361 --> 00:27:47,127
The people who aren't educated won't
accomplish anything in their life.

419
00:27:55,940 --> 00:27:59,671
She teases me. Sometimes
she and the kids gang up on me.

421
00:28:01,946 --> 00:28:04,244
When you have your child
they will do the same to you.

423
00:28:09,554 --> 00:28:12,523
You can't get well educated living here,

424
00:28:12,590 --> 00:28:14,080
because of the poverty.

428
00:28:24,369 --> 00:28:27,827
Those people do not belong to Islam.

429
00:28:27,906 --> 00:28:32,138
Even the terrorism that happened
here was masterminded abroad.

430
00:28:32,210 --> 00:28:35,805
The people who blew themselves
up were tempted,

431
00:28:35,880 --> 00:28:38,974
some tempted by money
and some by Paradise.

432
00:28:39,050 --> 00:28:43,783
It is much harder for this generation
than it was for my generation.

433
00:28:55,500 --> 00:28:59,869
Violence is birthed
by the economic dead ends

434
00:28:59,938 --> 00:29:01,530
that confront our youth

435
00:29:01,606 --> 00:29:05,599
who happen to be Muslim
with an Islamic outlook.

438
00:29:12,684 --> 00:29:17,018
I believe that there is an enormous
anger with American policy.

439
00:29:17,088 --> 00:29:20,785
It's too bad it's the American people
who have to pay for that anger.

440
00:29:20,859 --> 00:29:25,626
And so we have the obligation
not to fail our future,

441
00:29:25,697 --> 00:29:28,996
we have to reach out
to each other,

442
00:29:29,067 --> 00:29:32,366
ignore official political lines

443
00:29:32,437 --> 00:29:37,204
and create unofficial bridges
between civilian societies

444
00:29:37,275 --> 00:29:38,572
in order to bring about change.

446
00:29:42,881 --> 00:29:45,816
If it were truly
Osama bin Laden, who did that,

447
00:29:45,884 --> 00:29:47,715
then he dealt us a bad hand.

448
00:29:47,786 --> 00:29:51,916
He legitimized the American
presence in the Middle East.

449
00:29:51,990 --> 00:29:55,653
He was the reason for the adoption
of all the anti-terrorist laws

450
00:29:55,727 --> 00:29:59,128
that have pushed us further
under the yoke

451
00:29:59,197 --> 00:30:02,166
of our own local political leaders.

452
00:30:02,233 --> 00:30:07,364
I was astonished on many levels.
It's horrible.

460
00:30:50,849 --> 00:30:54,182
I don't agree with Al-Qaeda.

462
00:30:55,386 --> 00:30:57,911
I'll leave that alone.
I don't agree with them at all.

463
00:30:57,989 --> 00:31:01,186
It is none of their business
to do anything for us.

464
00:31:01,259 --> 00:31:03,022
We can do for ourselves.

465
00:31:03,094 --> 00:31:07,690
We are fighting to make our homeland
free. It's none of their business.

474
00:31:43,668 --> 00:31:47,502
Ours is
not a religious conflict.

475
00:31:47,572 --> 00:31:51,565
We don't fight the Jews
because they are Jews.

476
00:31:51,643 --> 00:31:54,703
No, we fight them
because they occupy our lands.

477
00:31:54,779 --> 00:31:58,875
And Al-Qaeda, it holds
all of the West as an enemy.

480
00:32:05,156 --> 00:32:11,061
Any Arab leader, all Arab movements
have adopted the Palestinian issue

481
00:32:11,129 --> 00:32:16,431
because they know the Palestinian issue
is what triggers the emotions and

482
00:32:16,501 --> 00:32:19,197
nationalistic feelings
in all Arab and Muslim people.

485
00:32:27,345 --> 00:32:28,972
Without the Palestinian issue,

486
00:32:29,047 --> 00:32:33,711
they won't get any attention

487
00:32:33,785 --> 00:32:37,118
on the global level.

488
00:32:37,188 --> 00:32:40,351
It is strategic on their part
to adopt the Palestinian issue.

492
00:33:04,182 --> 00:33:05,979
If this
was happening in Israel,

493
00:33:06,050 --> 00:33:08,280
America and all the other
countries would protest it.

494
00:33:08,353 --> 00:33:10,878
The peace of life, we don't have it.


495
00:33:10,955 --> 00:33:14,789
And we are living in very big prison.

496
00:33:14,859 --> 00:33:19,262
I wish we had somebody
like Osama bin Laden in Palestine.

497
00:33:19,330 --> 00:33:22,857
Just to destroy the state of Israel.

503
00:33:40,585 --> 00:33:44,544
It's not only overlooking us,
but we are always under their foot.

504
00:33:45,023 --> 00:33:47,617
They can come and take over
my home, where I live, by force.

505
00:33:47,692 --> 00:33:51,184
What can I do? I can't do anything.

576
00:38:41,852 --> 00:38:45,117
The suspicious object
is here, past the garbage can.

577
00:38:45,189 --> 00:38:47,248
Listen up.
Good evening to everyone.

578
00:38:47,325 --> 00:38:48,986
There is
a suspicious object there.

579
00:38:49,060 --> 00:38:52,359
I am asking everyone
who is in this area of Allenby

580
00:38:52,430 --> 00:38:55,092
and the area of the boardwalk
to move away from the area.

581
00:39:03,107 --> 00:39:06,634
The man on the hotel balcony,
get inside.

582
00:39:10,281 --> 00:39:13,409
Everyone is requested to be
behind the police gates.

583
00:39:14,552 --> 00:39:17,453
Pay attention. This is the bomb squad.

584
00:39:17,521 --> 00:39:20,251
I am about to conduct a controlled
explosion on the suspicious object.

587
00:39:27,498 --> 00:39:30,524
Don't stand near the windows.

588
00:39:46,951 --> 00:39:51,217
Three... two... one. Go.

596
00:40:35,966 --> 00:40:38,628
About what?

597
00:40:38,702 --> 00:40:40,363
Political?

600
00:40:47,845 --> 00:40:49,005
Yes.

604
00:41:01,225 --> 00:41:03,056
Get the hell out of here!

608
00:41:12,870 --> 00:41:14,667
- Go off!
- Get out of here!

609
00:41:14,738 --> 00:41:15,727
This is not your land.

611
00:41:18,142 --> 00:41:20,633
Get the hell out of here!

612
00:41:20,711 --> 00:41:23,612
Get out of here!

614
00:41:29,920 --> 00:41:31,785
I'm going to break this.

616
00:41:40,030 --> 00:41:45,263
So what you see here, majority
of people here don't think like them.

618
00:41:49,406 --> 00:41:52,933
Out of here, get out of here,
get out of here!

620
00:41:55,813 --> 00:41:58,008
Go off! Go off!

621
00:41:58,082 --> 00:42:00,414
Go off!

623
00:42:03,921 --> 00:42:05,980
Jewish?

625
00:42:08,325 --> 00:42:11,692
No Jewish.

636
00:43:05,316 --> 00:43:06,806
We are worshipping Allah.

644
00:43:37,548 --> 00:43:41,644
9/11 breaks the hearts
of every human being,

658
00:44:31,235 --> 00:44:35,672
Al-Qaeda attacked New York
and Washington on September 11th.

659
00:44:35,739 --> 00:44:40,972
Because Al-Qaeda can't fight the U.S. on
its soil, they drew them to Arabic land

660
00:44:41,045 --> 00:44:42,945
so that they will fight them here.

663
00:44:48,886 --> 00:44:54,483
After Afghanistan, Al-Qaeda
didn't have anywhere to operate.

664
00:44:54,558 --> 00:44:56,048
If the United States
hadn't entered Iraq,

665
00:44:56,126 --> 00:44:58,993
Al-Qaeda wouldn't be
the same as they are now.

666
00:44:59,063 --> 00:45:01,258
It would have ended
by the end of Afghanistan.

667
00:45:01,331 --> 00:45:03,561
The psychology of the Arabs and Muslims

668
00:45:03,634 --> 00:45:06,432
is to side
with whoever fights the occupier.

671
00:45:13,811 --> 00:45:15,142
Nothing whatsoever.

672
00:45:15,212 --> 00:45:18,477
Zarqawi was killed
and nothing happened.

673
00:45:18,549 --> 00:45:21,814
And if bin Laden and Zawahiri
got killed, nothing will happen.

674
00:45:21,885 --> 00:45:25,412
Because Al-Qaeda has become
an intercontinental idea,

675
00:45:25,489 --> 00:45:27,719
the same as globalization.

684
00:45:54,985 --> 00:45:58,944
O God, one leader
to lead jihad for your sake.

685
00:45:59,022 --> 00:46:01,047
To liberate the land of Palestine.

686
00:46:01,125 --> 00:46:03,059
And the land of Iraq
from the Christians.

687
00:46:03,127 --> 00:46:04,560
O God, the strong and noble one.

688
00:46:04,628 --> 00:46:07,028
O God, go after the Christians.

689
00:46:07,097 --> 00:46:09,088
O God, make wars in their homes.

690
00:46:09,166 --> 00:46:11,760
O God, release your armies upon them.

691
00:46:11,835 --> 00:46:15,066
O God, make the land of Palestine
a graveyard for the Jews.

692
00:46:15,139 --> 00:46:18,802
O God, make the land of Iraq
a graveyard for the Christians.

695
00:46:25,315 --> 00:46:28,250
Well, look at what
you did in Iraq and Afghanistan.

698
00:46:34,358 --> 00:46:36,451
Was it an act of aggression or not?

700
00:46:38,662 --> 00:46:41,324
You went there with an ambulance
or with a tank?

702
00:46:46,870 --> 00:46:51,432
If one man in a cave
was able to do this...

703
00:46:51,508 --> 00:46:53,999
What if all Muslims were
to resist against America

704
00:46:54,077 --> 00:46:55,840
under the leadership of one man?

706
00:46:59,716 --> 00:47:02,241
One single verse from the Koran
and they will come.

708
00:47:04,555 --> 00:47:07,023
These things get planned.
Don't disregard it as unlikely.

709
00:47:07,090 --> 00:47:10,992
What if we were to burn the refineries,
just to deprive you of the oil?

789
00:51:57,481 --> 00:51:58,539
Eighteen.

792
00:52:03,186 --> 00:52:05,586
Great.

793
00:52:05,655 --> 00:52:08,886
We have no worries or troubles.

795
00:52:12,796 --> 00:52:15,765
Actually, we did not study

796
00:52:15,832 --> 00:52:19,632
the United States specifically.

798
00:52:24,474 --> 00:52:29,571
I have no definite opinion...
no specific opinion.

805
00:52:50,267 --> 00:52:51,291
No answer.

807
00:52:55,172 --> 00:52:58,335
Their view is...

808
00:52:58,408 --> 00:53:04,404
Americans always want to dominate...

809
00:53:05,715 --> 00:53:10,345
No, that was the wrong answer...

813
00:53:25,135 --> 00:53:27,000
Stop your cameras.

824
00:54:30,934 --> 00:54:34,131
Osama bin Laden?
No, I have no leads.

842
00:55:44,007 --> 00:55:46,407
Where can
I marry Osama bin Laden?

843
00:55:49,479 --> 00:55:51,344
I don't.

844
00:55:52,215 --> 00:55:54,911
Please make sure that
you have the correct number.

845
00:55:56,753 --> 00:55:58,050
Your call cannot be completed as dialed.

851
00:56:20,477 --> 00:56:24,538
We would like to ask you
some questions about Osama bin Laden.

857
00:56:39,796 --> 00:56:42,697
He's in the basement.

861
00:56:54,244 --> 00:56:56,235
Where is your car?

880
00:58:15,258 --> 00:58:17,852
Yes.

881
00:58:18,761 --> 00:58:20,422
No.

885
00:58:33,676 --> 00:58:38,511
I don't believe that is solid proof.
America wins all the Oscar awards.

886
00:58:38,581 --> 00:58:42,347
It owns the best studios
and technologies in the world.

887
00:58:42,418 --> 00:58:46,354
We've seen the animals,
the lion, the dog and the pig,

888
00:58:46,422 --> 00:58:49,585
we've seen them in many movies
talking like humans.

889
00:58:50,293 --> 00:58:53,820
I feel very uncomfortable
with this conversation.

893
00:59:04,807 --> 00:59:09,267
They go because they feel
like America...

894
00:59:09,345 --> 00:59:11,506
you feel like...

895
00:59:11,581 --> 00:59:13,742
She's trying all kinds
of ways to dominate

896
00:59:13,816 --> 00:59:16,148
and impose her dominance
on the entire world,

897
00:59:16,219 --> 00:59:18,210
especially on the Muslim world.

899
00:59:22,425 --> 00:59:23,983
Yes, of course.

901
00:59:25,428 --> 00:59:29,057
These youth went first to Afghanistan.

902
00:59:29,132 --> 00:59:33,034
Then America went into Iraq,
and they followed.

904
00:59:35,071 --> 00:59:37,164
A lot.

906
00:59:43,580 --> 00:59:45,309
Unfortunately, nobody came back.

921
01:01:14,237 --> 01:01:16,000
No, not on the path...

922
01:01:16,072 --> 01:01:17,972
- Really?
- Really.

925
01:01:26,416 --> 01:01:30,978
This is where
Osama's headquarters was.

926
01:01:31,054 --> 01:01:33,887
There are large caves on this
side and the other side too.

927
01:01:33,956 --> 01:01:39,826
They were full of ammunition, weapons,
and food such as tea, sugar and rice.

928
01:01:39,896 --> 01:01:42,524
Look, up there, should we go?

946
01:03:00,843 --> 01:03:03,903
Our plan
and goal is that Tora Bora

947
01:03:03,980 --> 01:03:06,710
should become an international
tourism center.

948
01:03:06,783 --> 01:03:10,742
So we want to build a grand
amusement park in Tora Bora.

950
01:03:11,988 --> 01:03:12,977
Everything will be built.

952
01:03:14,123 --> 01:03:15,112
Everything will be built.

953
01:03:15,191 --> 01:03:16,954
Absolutely.

954
01:03:17,026 --> 01:03:20,086
Yes. Yes.

958
01:03:38,748 --> 01:03:42,081
No one
has done anything for us.

959
01:03:42,151 --> 01:03:45,951
All the international aid
goes to those at the top.

960
01:03:46,022 --> 01:03:47,011
It never makes its way down to us.

962
01:03:49,425 --> 01:03:50,653
No, we bought it
in the black market!

963
01:03:50,726 --> 01:03:54,162
No, we buy it in the black market.

964
01:03:55,231 --> 01:03:59,497
They haven't helped us with land,
money or to fix our tent or anything.

965
01:04:01,204 --> 01:04:04,264
Look at this troublemaker!
When we're traveling at night,

966
01:04:04,340 --> 01:04:08,504
we have to constantly check the camel to
make sure we didn't leave him somewhere.

968
01:04:09,946 --> 01:04:12,847
Yeah, all the time!

969
01:04:12,915 --> 01:04:14,439
We've lost them many times!

970
01:04:14,517 --> 01:04:17,452
Our lives already passed. We are
concerned about our children.

971
01:04:17,520 --> 01:04:20,318
They should be able
to have a better life.

979
01:04:47,483 --> 01:04:49,576
No one is helping this place,

980
01:04:49,652 --> 01:04:51,415
and if there is,
no one is distributing it.

981
01:04:53,956 --> 01:04:59,417
There are more than 800 houses
here but no school or hospital.

982
01:05:01,964 --> 01:05:04,762
We are living the same now
as we were under the Taliban.

983
01:05:04,834 --> 01:05:07,894
America must hold
our government accountable.

984
01:05:07,970 --> 01:05:12,634
America must ask why didn't you pave
the streets with the money we gave you.

985
01:05:14,944 --> 01:05:18,072
All those houses were destroyed
by foreign countries.

986
01:05:18,147 --> 01:05:21,014
Why don't they rebuild those houses?

987
01:05:21,083 --> 01:05:23,551
They haven't done
anything positive here.

1014
01:07:16,565 --> 01:07:18,999
God know,
if we catch him, we will tear him apart.

1015
01:07:19,068 --> 01:07:22,196
Osama bin Laden's Afghan's enemies.

1016
01:07:24,874 --> 01:07:26,341
Who is Osama?

1017
01:07:26,409 --> 01:07:29,640
He is the one who destroyed
those buildings in America.

1018
01:07:29,712 --> 01:07:31,873
Fuck him.
And fuck America.

1020
01:07:36,986 --> 01:07:39,887
Pakistan.

1021
01:07:39,955 --> 01:07:41,718
Pakistan.

1023
01:07:44,427 --> 01:07:46,418
It is very obvious to the world

1024
01:07:46,495 --> 01:07:47,587
that he is hiding in Pakistan.

1025
01:07:47,663 --> 01:07:50,928
You should find him
so we have peace in the world.

1027
01:07:55,104 --> 01:07:57,163
Tribal area.

1033
01:08:17,093 --> 01:08:18,720
You will find him.

1038
01:08:33,309 --> 01:08:34,298
I'll miss my family.

1042
01:08:42,718 --> 01:08:46,085
God knows
what will happen if you go there.

1043
01:08:46,155 --> 01:08:48,988
Something terrible will happen to you.

1044
01:08:49,992 --> 01:08:52,051
You're finished.

1045
01:08:52,128 --> 01:08:54,153
The Taliban will cut your head.

1046
01:08:54,230 --> 01:08:56,460
Yeah.

1093
01:12:28,277 --> 01:12:29,938
Where's Osama?

1114
01:13:59,935 --> 01:14:01,732
This person is a civilian

1115
01:14:01,804 --> 01:14:03,772
from America and wants
to ask you some questions.

1116
01:14:03,839 --> 01:14:06,899
His name is Morgan.

1119
01:14:12,047 --> 01:14:13,446
What do you think,

1120
01:14:13,515 --> 01:14:14,914
who is happy with battles?
No one is happy with battles.

1121
01:14:14,983 --> 01:14:17,952
We are not happy.

1122
01:14:18,020 --> 01:14:21,217
We don't like the war.
We want a peaceful life.

1123
01:14:21,290 --> 01:14:24,555
We are neither your friends nor the
Taliban's. We just want a simple life.

1126
01:14:29,398 --> 01:14:34,233
The most important problem
is the lack of water.

1127
01:14:34,303 --> 01:14:37,431
Before the base drilled their well

1128
01:14:37,506 --> 01:14:40,566
we didn't have any water problems.

1145
01:15:55,317 --> 01:15:57,785
Thank you
and please tell your commander

1146
01:15:57,853 --> 01:15:58,911
the father of Taza Gui
says peace.

1168
01:17:14,863 --> 01:17:16,387
He's in Afghanistan.

1169
01:17:16,465 --> 01:17:18,160
Osama bin Laden is here.

1170
01:17:18,233 --> 01:17:19,222
Afghanistan.

1171
01:17:19,301 --> 01:17:21,394
Osama bin Laden...

1172
01:17:21,470 --> 01:17:23,301
Which animal is that?

1175
01:17:31,146 --> 01:17:33,444
He is
a great soldier of the Muslims.

1176
01:17:33,515 --> 01:17:36,177
He is fighting for righteousness.

1177
01:17:36,251 --> 01:17:38,082
He's a diamond.

1178
01:17:38,153 --> 01:17:40,144
Osama is exploiting the root causes.

1179
01:17:40,222 --> 01:17:44,090
If we find the root causes,
we'll solve everything.

1180
01:17:44,159 --> 01:17:47,822
- Osama bin Laden is nothing, sir.
- He is only one! One person!

1182
01:17:53,235 --> 01:17:56,966
All the people like the wrestling.

1183
01:17:57,039 --> 01:18:01,305
Because wrestling is fair.

1184
01:18:01,376 --> 01:18:02,570
Fixed?

1185
01:18:02,644 --> 01:18:04,908
Maybe.

1186
01:18:04,980 --> 01:18:07,710
You are right. I know. You are right.

1187
01:18:07,783 --> 01:18:11,719
But when it is for a belt,

1188
01:18:11,787 --> 01:18:13,379
then it is not fixed.

