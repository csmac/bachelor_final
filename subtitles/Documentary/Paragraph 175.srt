{725}{837}We have to|see this romantically,
{837}{902}because in such drastic times|one tends to be romantic.
{930}{997}When bombs fall...
{1035}{1108}...and explode nearby...
{1123}{1233}...one looks|to others for closeness...
{1270}{1425}...and one forgets the bombs,|the war and the stalled train.
{1450}{1499}One is just close to others.
{1523}{1650}One does what everyone does|when they are close.
{1673}{1715}That's what one does.
{1766}{1850}You're not going to tell me|that while the bombs were falling...
{1850}{1903}...you made love on the train?
{1903}{1939}But of course I did!
{1967}{2018}But of course!
{2018}{2109}You didn't get that?|You are slow, darling.
{2135}{2182}You are slow.
{2207}{2282}Subtitles downloaded from www.OpenSubtitles.org
{5724}{5796}Hello! Karl, greetings,|it's Klaus!
{5863}{5910}Today is not convenient?
{5948}{6060}Should I just stop by so we can|see each other again?
{6143}{6221}Then I'll be there around three.
{6221}{6323}If you want to get rid of me,|just kick me out, that's fine.
{6339}{6440}I'm excited! Till later!
{8669}{8763}Oh, I've already talked so much|about the concentration camps.
{8852}{8921}You know,|it's more than fifty years ago.
{8964}{9059}There have been so many|other, better impressions...
{9076}{9141}...than such shitty shameful deeds.
{9325}{9379}Look, with time they've all|been torn out.
{9379}{9448}Why do you tear them out?
{9448}{9486}I throw them away.
{9508}{9578}Those are uncomfortable memories.
{9591}{9701}I've spoken about it before,
{9719}{9777}I don't want to anymore.
{9777}{9849}That's in the past for me.
{10167}{10239}I don't have much time|for this movie.
{10239}{10301}More than half a day,|I refuse.
{10301}{10376}I'm here|but let's make it short.
{10404}{10440}Very short.
{10447}{10494}Otherwise, I can't.
{10513}{10549}Okay,|let me, let me.
{10627}{10679}I'm not dead yet!
{11063}{11160}I swore never to shake hands|with a German again.
{11160}{11199}And here you are.
{11199}{11242}It's terrible.
{11249}{11308}You can't understand this,
{11308}{11368}because you're not from|the same generation.
{11368}{11451}This is the difficulty|between us today.
{11471}{11551}You're trying hard|to understand me.
{11551}{11621}And I'm trying not to hurt you.
{11655}{11719}Because it's difficult to talk|about that time.
{12620}{12688}This is the Schwanenburg.
{12812}{12899}It was a dance club.
{12899}{12961}A normal bar...
{12961}{13106}...but on certain days|it was rented by homosexuals.
{13443}{13511}Then there was much joy,
{13511}{13580}and even more screaming.
{13647}{13760}There was homosexual dancing,
{13760}{13894}and once in a while,|just to get the queens going,
{13894}{13971}someone would shout,|"The police are coming!"
{13971}{14043}Everyone would hike up|their skirts and run.
{14084}{14172}But the police|never really came.
{14268}{14328}Today, it's hard to imagine...
{14357}{14435}...how wild it was in Berlin...
{14479}{14590}...after the 1914 to 1918 war.
{14599}{14654}Everything went topsy-turvy.
{14742}{14850}Men danced together|and so did women.
{14884}{14972}in Berlin, those were|the golden years.
{15013}{15075}I think in all of Berlin|you were free,
{15075}{15111}you could do|what you wanted.
{15141}{15231}We had three|very well-known clubs.
{15244}{15292}One was in the north...
{15292}{15349}...where proletarian girls came.
{15349}{15442}Usually in their|Sunday-best costume...
{15442}{15476}...their smoking costume.
{15541}{15601}I was a bit scared, I must say.
{15648}{15715}If you have never seen...
{15744}{15828}...boyish and|masculine lesbians...
{15828}{15878}...and such a heap of them.
{15967}{16026}I was surprised,|I had to get used to it.
{16048}{16092}And funnily enough,
{16104}{16151}I saw one woman...
{16171}{16247}...which looked a little bit|like Marlene Dietrich.
{16247}{16316}Anyway, I wanted|to get to know her.
{16316}{16376}But she didn't|care for me, of course.
{16387}{16447}I was a silly little girl.
{16447}{16512}But she is the one...
{16512}{16579}...I saw occasionally later on...
{16579}{16643}...who saved my life,
{16643}{16695}because she was the one|who sent me this permit.
{16695}{16748}She went to England before.
{21613}{21738}Sports became the center|of my life.
{21901}{21995}I had an athletics teacher...
{21995}{22083}...a blond Jewish teacher.
{22089}{22126}Oh, my!
{22132}{22214}So slim and strong and beautiful.
{22253}{22375}One day we were|showering together...
{22375}{22424}...and I jumped on him.
{22455}{22579}Exactly the opposite of the|pederastic teacher...
{22579}{22634}...I jumped on him!
{22687}{22771}I ran home to my mother|and said,
{22771}{22860}"Mother, today|I had my first man!"
{23465}{23503}You were born here?
{23516}{23554}Born here.
{23586}{23638}But not in this apartment?
{23647}{23714}No. One floor below.
{26552}{26630}Have you forgotten|the boys, Heinz?
{26641}{26702}- I haven't forgotten.|- You haven't forgotten.
{26790}{26888}Back in here, off to the side,
{26941}{26979}was the couch.
{27011}{27070}Now it's hidden|under the boxes.
{27107}{27170}- This was your room, right?|- Yes.
{27232}{27274}The boys' room.
{27358}{27488}It was all done up with|boy scout souvenirs and photos,
{27571}{27636}with the guitar|hanging on the wall.
{28460}{28567}I was the regional leader...
{28580}{28634}...at the end.
{28727}{28774}Arrest.
{28843}{28927}My group and I|could only exist...
{28957}{29024}...for another six months.
{29083}{29154}Then the Hitler Youth|moved in on us,
{29170}{29209}with brass knuckles...
{29287}{29326}...and other weapons.
{29332}{29431}There was a lot of resistance.
{29487}{29535}But they were stronger...
{29650}{29691}...and in numbers...
{29746}{29804}...they were superior.
{30866}{30942}My mother came|from a Christian family,
{30951}{30995}my father is Jewish.
{31002}{31041}My mother converted.
{31066}{31160}When Hitler came,|things began to change.
{31224}{31342}German boys in the Hitler Youth|had to have uniforms.
{31373}{31466}Their parents had to rush off|and buy them.
{31477}{31572}Within four months|the entire class turned brown.
{31590}{31653}With little black shorts|and brown shirts.
{31695}{31754}After four months of Hitler...
{31778}{31834}...someone would|raise their hand:
{31834}{31885}"Can I sit somewhere else?"
{31885}{31923}"But why?"
{31923}{31989}"It smells like garlic here!"
{32029}{32098}I didn't even know|what garlic was.
{32098}{32145}We never ate it at home.
{32145}{32242}Now that I am older,|I know the advantages of garlic.
{32244}{32326}That was the first insinuation|about the Jews.
{32326}{32353}Jews are garlic-eaters.
{32396}{32445}"Can I sit in the back?
{32445}{32493}"It stinks of|sweaty Jewish feet here!"
{32493}{32539}That was pretty obvious.
{32566}{32685}And within two months,
{32707}{32771}I was sitting alone|in the first row.
{32800}{32866}They had all|withdrawn from me.
{32866}{32916}In the truest sense|of the word.
{33146}{33224}We call it the|Yom Kippur Jews.
{33304}{33407}They were not very religious,|my family.
{33407}{33482}But we did hold|the holy holidays.
{33491}{33563}That's why|we call ourselves...
{33563}{33617}the Yom Kippur Jews.
{33747}{33802}The time was very bad...
{33825}{33875}...people couldn't get work.
{33879}{33946}And the inflation|was a horrible thing.
{33996}{34055}At first we didn't believe it.
{34089}{34135}We laughed about him.
{34158}{34227}That such a person|like Hitler...
{34243}{34302}...that the people would|stay behind him.
{34466}{34510}Promises, promises.
{34530}{34565}They believed it.
{35078}{35170}We began to talk about|Hitler in Alsace.
{35181}{35261}But only in whispers...
{35347}{35428}...we knew that preparations|were being made.
{35474}{35587}My older brothers were drafted|into the French army.
{35587}{35654}We lived in fear of war.
{35891}{35983}I remember|one speech I heard...
{35983}{36029}...on the radio:
{36029}{36092}"Do you want|butter or guns?"
{36118}{36186}And the people cried,|"GUNS!"
{36212}{36279}And at that,|my father became afraid.
{36842}{36880}Turn a little.
{36880}{36954}No, turn the camera this way.
{36954}{36992}But then I can't see you.
{36992}{37039}That's not necessary,|I want a photo of you.
{37039}{37063}C'mon!
{40853}{40943}Someone took me|to a gay bar.
{40978}{41090}He wanted to show me|how it was for Christian gays.
{41090}{41146}So he dragged me to a bar.
{41198}{41338}There was an incredible|atmosphere of fear.
{41368}{41451}They kept looking to see|who's coming in now.
{41490}{41562}Some of them had told me...
{41562}{41644}...things used to be|happy and carefree,
{41644}{41724}but now they|were being persecuted.
{41724}{41807}it didn't seem like|persecution to me,
{41807}{41874}since the bar was still open.
{41874}{41951}But they said this bar is|only open to round us up.
{41956}{42039}They did this again later|with the Jews...
{42039}{42134}...they'd let them keep|their meeting places...
{42150}{42202}...so they could|snatch them up.
{45802}{45846}In this picture,|you can see my father.
{45931}{46008}This is my grandfather's house.
{46047}{46136}We lived on the first floor,
{46136}{46237}and my grandparents lived|on the second floor.
{46286}{46379}I didn't fully understand|the situation...
{46379}{46440}...it didn't register.
{46488}{46557}But I also|didn't take it seriously.
{46612}{46666}And then there|were my parents,
{46666}{46732}I couldn't abandon them.
{46889}{46924}This is me.
{46959}{47014}Always adorable,|with a sweet smile.
{47045}{47111}What's interesting|in this picture is...
{47111}{47220}...there is absolutely nothing|Jewish about this face.
{47254}{47302}He's a little Christian.
{47324}{47376}No? Absolutely Christian.
{47454}{47579}My mother's family was Prussian.|Devout Christians. Evangelicals.
{47588}{47664}For my aunts and for my|family it was terrible...
{47664}{47771}...they said, "Oh my God, he's|Jewish and he's gay!"
{47771}{47834}"Either way,|he'll be persecuted!"
{47834}{47890}"This cannot end well."
{47899}{47987}We once had a plan|to go to Shanghai.
{48012}{48059}It cost $1,000 each.
{48059}{48146}Where would|we have gotten $4,000?
{48187}{48314}The family was sure|they could take care of us.
{48314}{48379}No one knew|what was coming.
{52561}{52608}I have a very good intuition.
{52608}{52651}And I had a feeling...
{52651}{52725}...something horrible|is going to happen.
{52759}{52817}I made a decision,|I go off in the country.
{52817}{52854}And I did.
{52905}{52972}When I was on the farm,|what did we do?
{52972}{53015}We were singing.
{53015}{53067}Hebrew.|Hebrew songs.
{53114}{53171}We were invaded by the Nazis...
{53171}{53225}...and brought to prison.
{53282}{53355}But not all Germans were...
{53381}{53435}...aggressive and nasty.
{53447}{53501}The wife of the policeman...
{53520}{53566}...left the doors open on purpose.
{53566}{53602}And we all escaped.
{53678}{53743}The farm was partly burned down.
{53761}{53866}I went in and, a miracle,
{53903}{53949}I found my passport...
{53966}{54029}...in this muddle and glass...
{54029}{54073}...without hurting myself.
{54089}{54136}I went on the bicycle...
{54136}{54215}...and off I tried to go to Berlin.
{54315}{54380}And this is where it happens...
{54380}{54458}...that the postman came on the|bicycle from the other side...
{54487}{54572}...and said, "Fraulein,|I have a love letter for you."
{54609}{54670}I opened it,|and there was the...
{54670}{54752}...English papers which let me|come over to England.
{54805}{54850}I could hardly believe it.
{54857}{54950}Had I ever missed this letter,
{54965}{55034}then I would have gone...
{55034}{55078}...with my parents|to Auschwitz.
{57742}{57809}One morning the police called.
{57809}{57859}What could this|have to do with me?
{57859}{57901}Nothing.
{57958}{58042}I told them I couldn't come,
{58042}{58093}I was too busy working.
{58101}{58202}Ten minutes later,|the police called again.
{58217}{58289}I repeated that|I'd come by soon.
{58289}{58369}But they said it was very urgent.
{58389}{58449}I thought,|"What could they want?"
{58469}{58558}So I went to the police...
{58558}{58624}...and they showed me a letter.
{58624}{58671}"Here, read this," they said.
{58673}{58750}"Bavarian Political Police."
{58826}{58919}What did that|have to do with me?
{58919}{59018}"You are suspected|of being homosexual."
{59134}{59238}"You are hereby under arrest."
{59265}{59312}What could I do?
{59381}{59443}Off I went to Dachau...
{59463}{59521}...without a trial...
{59521}{59575}...directly to Dachau.
{59784}{59888}I spent a year and a half|in Dachau...
{59969}{60076}...without really knowing why.
{60273}{60386}The Germans came to Alsace|in 1940.
{60410}{60470}I don't say the Nazis,|the Germans!
{60470}{60530}And the Germans found|the police files.
{60621}{60730}They saw our names|on these lists...
{60730}{60766}...lists of homosexuals.
{60830}{60919}They were probably|watching us...
{60919}{61018}...how we live, where we go,|what we do.
{61238}{61329}And one day I had to go|to the Gestapo,
{61329}{61381}with 12 friends.
{61478}{61557}I ended up at the camp|in Schirmeck.
{61610}{61707}At the time, it was the|Schirmeck Internment Camp,
{61707}{61794}a "protective custody" camp.
{61872}{61935}If, for example,|someone got drunk,
{61959}{62034}and sang the French|national anthem in the street...
{62034}{62093}...this is during the|German occupation...
{62102}{62186}...he'd be sent to Schirmeck.
{62186}{62247}There were also communists,|resistance fighters,
{62292}{62343}and my 12 friends...
{62343}{62403}...who were arrested|in a round-up...
{62424}{62485}...in May 1941.
{63458}{63542}After I was released|from Dachau,
{63542}{63609}I went on a trip.
{63662}{63729}I think I was being watched...
{63729}{63798}...this woman was|always behind me.
{63833}{63905}I left my hotel|to go eat...
{63930}{63987}...and a hustler|came up to me.
{64048}{64145}I realized|I was being followed.
{64214}{64291}He pulled me|into some bushes.
{64371}{64424}I said,|"Someone is following us."
{64424}{64494}"No," he said.|"No one is there."
{64534}{64610}And that's when it happened.
{64652}{64704}"You are under arrest."
{64799}{64916}I was taken somewhere,|to some prison, for trial.
{64980}{65030}I didn't understand anything.
{65133}{65183}While I was there...
{65183}{65270}...almost all the|homosexuals...
{65270}{65329}...were transported|to Mauthausen...
{65360}{65415}...and nearly all of them...
{65698}{65750}...were killed.
{66279}{66339}Again, I came to a|concentration camp.
{66416}{66498}This time it was Buchenwald.
{66622}{66702}At first it was "homo"...
{66702}{66749}...or rather "Paragraph 175"...
{66779}{66851}...written in big letters...
{66851}{66930}...on the back of the jacket.
{66933}{66978}As I remember.
{66978}{67021}Later, it was...
{67046}{67086}...a pink triangle.
{67350}{67414}At that time,|the transports began.
{67444}{67510}Every day|we said goodbye to someone.
{67542}{67602}That's when I encountered...
{67602}{67670}...the Jewish|Zionist underground,
{67670}{67746}that existed in this|great capital, Berlin.
{67782}{67862}Those who remained,|joined together...
{67883}{68011}...they understood,|soon it would be their turn.
{68026}{68075}I found them shelter.
{68075}{68161}I even let them|stay in my attic.
{68218}{68310}I met this beautiful|blond Jew.
{68324}{68419}He invited me|to spend the night.
{68419}{68463}He said,|"Let's play chess."
{68539}{68586}We sat on his bed...
{68599}{68665}...and we played chess.
{68674}{68754}We did the other thing too,|of course. We had to.
{68790}{68862}Then we slept|for a few hours.
{68894}{68965}In the morning,|the Gestapo came.
{68974}{69014}They checked...
{69014}{69076}...he and his mother|were on the list.
{69076}{69115}I showed my ID...
{69159}{69194}...not on the list.
{69194}{69246}They could have taken me.
{69286}{69349}They took him and his mother...
{69391}{69446}...to the train station...
{69458}{69509}...and sent them to Auschwitz.
{69622}{69674}It had a different value then,
{69674}{69738}a night of love.
{72330}{72378}There was a hierarchy...
{72452}{72522}...from strongest to weakest.
{72540}{72600}There was no doubt...
{72600}{72659}...that the weakest|in the camps...
{72735}{72779}...were the homosexuals.
{72779}{72843}All the way on the bottom.
{72936}{72986}I wasn't even eighteen.
{73006}{73063}Arrested, tortured, beaten.
{73090}{73133}Without any defense,
{73182}{73219}without a trial.
{73219}{73258}Nothing.|I was all alone.
{73293}{73374}I don't even mention being|sodomized, being raped.
{73494}{73538}It happened in front of me...
{73555}{73682}...and 300 prisoners, 300.
{73682}{73740}The death of Jo. My friend.
{73754}{73833}He was condemned to die,|eaten by dogs,
{73833}{73882}German dogs,|German shepherds.
{73882}{73926}- Where?|- in Schirmeck.
{73946}{74002}And that, I can never forget.
{74294}{74374}I was just glad that|I landed in a regular prison.
{74398}{74533}That was a gift,|so to speak.
{74574}{74652}It prolonged my life.
{74652}{74724}Had I been taken to|a concentration camp,
{74724}{74766}I'd no longer be alive.
{76014}{76070}I wanted to be with men!
{76211}{76269}I took photos of everyone,
{76351}{76418}and right away|I had lots of friends.
{76617}{76736}Military was honor,|dignity and justice.
{76759}{76818}What the Nazis would|change it into,
{76818}{76886}you didn't know before.
{76909}{76985}You were always|a little proud...
{77002}{77053}...of this militarism,
{77053}{77113}even if you were|a homosexual.
{77306}{77401}This is my father's|Jewish family in Vienna.
{77412}{77452}Which of them survived?
{77567}{77638}This one survived,|and this one.
{77651}{77698}All the others were killed.
{77734}{77761}Auschwitz.
{77812}{77885}During that time|I found my first big love...
{77898}{77930}...Manfred.
{77976}{78043}it was like a dramatic love.
{78087}{78219}Then one time, I went to|spend the night at his house.
{78242}{78277}His brother was there.
{78294}{78333}"Where is Manfred?"
{78349}{78419}He said, "Our whole family|was arrested today."
{78499}{78558}So I went to Manfred's boss.
{78606}{78642}I say,|"They've picked up Manfred.
{78664}{78739}"His whole family|is being held...
{78739}{78798}"...in my old school building!"
{78819}{78868}"Do you have courage?"|he says,
{78868}{78926}this great big German guy.
{78926}{78972}"Yes, I have courage."
{78994}{79066}He says,|"My son is your size,
{79066}{79117}"he has a|Hitler Youth uniform.
{79117}{79179}"Put it on and|get Manfred out."
{79222}{79284}I went in and said,|"Heil Hitler!
{79312}{79371}"I must see|the officer in charge."
{79395}{79535}So this Gestapo guy says,|"You'll bring him back, right?"
{79535}{79593}I say,|"What else? He's a Jew!"
{79593}{79688}And I walk with Manfred,|out of my school building.
{79734}{79781}After 20 or 30 meters...
{79781}{79841}...I still remember|the exact spot...
{79874}{79974}I give him 20 marks.|"Go to my uncle's place.
{79983}{80082}"I'll call him and|meet you there later."
{80082}{80123}He stops...
{80156}{80225}and he says, very calmly
{80267}{80314}"I can't come with you, Gad.
{80342}{80422}"If I leave my|sick family now,
{80422}{80494}"I'll never be free again.
{80505}{80558}"I have to go with them.
{80558}{80629}"I'm the only strong one."
{80672}{80716}Without saying goodbye,
{80728}{80769}he turns around,
{80781}{80844}and walks back,|into my school building.
{81010}{81091}I walked in the other direction.
{81100}{81161}I wasn't able to think,
{81161}{81235}but I knew|that something was...
{81262}{81336}...forever broken.
{81596}{81639}The singing forest.
{81698}{81742}That gave us all...
{81804}{81845}...goosebumps.
{82158}{82206}What was the singing forest?
{82302}{82413}In the ground, there were holes.
{82490}{82562}Concrete holes.
{82669}{82712}Everyone...
{82739}{82797}...who was sentenced...
{82802}{82860}...would be lifted up...
{82889}{82925}...onto the hook.
{83192}{83247}in the Jewish barracks...
{83276}{83324}...it was similar.
{83359}{83414}But they were twisted...
{83438}{83521}...in addition to the hanging.
{83601}{83687}That's what was prepared|for the Jews.
{83820}{83881}The howling and screaming...
{83890}{83937}...were inhuman.
{83997}{84074}The singing forest.
{84250}{84300}inexplicable.
{84312}{84403}Beyond human comprehension.
{84479}{84549}And much remains untold.
{86878}{87012}"On this site was the first|Nazi camp in occupied Alsace."
{87460}{87521}Here where I now live was|barrack number thirteen.
{87573}{87594}During the war.
{87611}{87655}All this was the camp.
{87681}{87711}Right here.
{87761}{87820}What sorts of|prisoners were there?
{87820}{87870}It was mostly|political prisoners.
{87870}{87927}You had homosexuals,|unemployed...
{87927}{87987}...I don't know|who all was in there.
{88009}{88094}These days, do people|talk about the camp?
{88118}{88198}Oh, the young people|don't even know.
{88198}{88241}They didn't see it.
{88241}{88286}By the time they arrived,
{88286}{88347}there was nothing,|it was all torn down.
{88347}{88445}Only us old folks saw things,|when we were kids.
{88515}{88618}Was it known then that|homosexuals went to the camps?
{88662}{88734}It was known,|but how...I don't know.
{88734}{88797}Maybe through the newspapers.
{88873}{88958}Anything can be talked around.
{89050}{89112}Was it known that Jews were|exterminated there?
{89351}{89452}The big concentration camps|in Austria.
{89524}{89576}Wasn't that frightening?
{89623}{89690}Yes and no.
{89698}{89793}I think people|become indifferent very fast.
{89891}{89959}When things go on for years...
{89998}{90097}in the beginning|they were just camps.
{90126}{90218}That these camps|became death camps...
{90271}{90333}...wasn't known|in the beginning.
{90638}{90689}So now you see...
{90698}{90790}...why I did not speak|for 40 years?
{90909}{90978}My ass still bleeds.|Even today.
{90986}{91131}The Nazis stuck 25 centimeters|of wood up my ass.
{91157}{91244}Do you think I can talk|about that?
{91244}{91299}That it is good for me?
{91299}{91389}This is too much|for my nerves, Klaus!
{91389}{91434}I can't do this anymore.
{91466}{91524}I am ashamed for humanity.
{91570}{91609}Ashamed.
{91701}{91793}How long were you|in concentration camps?
{91793}{91819}Altogether?
{91819}{91858}I added it up once.
{91858}{91917}I think eight and a quarter years.
{91958}{92038}What did you do|when you got back?
{92038}{92075}When I came home?
{92184}{92276}I worked in|the family store...
{92276}{92346}...that my brother|was running.
{92346}{92400}My father|had already died.
{92506}{92576}Did you tell your brother|or mother...
{92576}{92646}...what happened in|the camps?
{92710}{92778}I never spoke|with my mother about it.
{92799}{92883}I could have talked|to my father, but...
{92938}{92970}Why not?
{93037}{93068}Shame.
{93163}{93228}My mother|never said anything.
{93267}{93302}It's all about...
{93366}{93419}...patiently carrying|one's burden.
{93682}{93734}Shame, about what?
{93831}{93886}You mean my mother?
{93955}{94062}Maybe it was|from compassion,
{94062}{94122}so she wouldn't offend me,
{94122}{94198}or make it even harder on me.
{94252}{94319}Not even one word from her.
{94515}{94576}Today, it is hard to imagine,
{94576}{94661}that you survived|these horrible years,
{94661}{94694}and came back and--
{94737}{94831}Couldn�t talk to|anybody about it?
{94831}{94894}Yes, I never spoke|to anyone about it.
{94958}{95014}Would you have liked|to talk to someone?
{95041}{95126}Maybe,|maybe with my father.
{95659}{95731}And later,|could you speak with others?
{95771}{95802}Never.
{95819}{95898}Nobody wanted|to hear about it.
{95898}{95994}If you would just mention|one of those words...
{95994}{96069}"Leave me alone|with this stuff.
{96197}{96274}"It's over now|and done with."
{96695}{96759}Now for me too...
{96806}{96840}...it's all over.
{96930}{97019}in September,|I'll be 93.
{97030}{97130}>> Napisy pobrane z http://napisy.org <<|>>>>>>>> nowa wizja napis�w <<<<<<<<
{97155}{97230}Download Movie Subtitles Searcher from www.OpenSubtitles.org
