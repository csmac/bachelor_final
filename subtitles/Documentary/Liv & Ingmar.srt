﻿1
00:00:00,000 --> 00:00:01,698
Subs created by: David Coleman.

2
00:00:01,699 --> 00:00:03,795
One week till our sweet 16.

3
00:00:03,796 --> 00:00:06,810
After four years of being apart, we
are finally together on our birthday.

4
00:00:06,811 --> 00:00:08,335
Yaysies!

5
00:00:08,336 --> 00:00:11,004
I know! We get to blow out
our candles together again.

6
00:00:11,005 --> 00:00:14,200
The three Rooney girls,
together again and ready to...

7
00:00:14,201 --> 00:00:17,058
<i>♪Party! Woo woo woo,
party! I said now party!</i>

8
00:00:17,059 --> 00:00:19,926
Don't turn around. You don't want
to know what Mom's doing behind you.

9
00:00:19,927 --> 00:00:25,671
I don't need to. I can feel the
desperation on the back of my neck.

10
00:00:25,672 --> 00:00:30,007
All right, fine. Guess I'll just
save my moves for the par-tay!

11
00:00:30,008 --> 00:00:32,804
Back that party up over here, Care Bear.

12
00:00:32,805 --> 00:00:35,118
Here we go.

13
00:00:35,119 --> 00:00:39,145
<i>♪Left left left, to
the right right right...</i>

14
00:00:39,146 --> 00:00:40,856
Anyway.

15
00:00:40,857 --> 00:00:45,297
So for our party, I had a few thoughts.

16
00:00:48,622 --> 00:00:50,782
I had...

17
00:00:52,443 --> 00:00:54,723
A thought.

18
00:00:54,823 --> 00:01:00,051
Ahem, okay, so I'm thinking
red carpet, designer gowns,

19
00:01:00,052 --> 00:01:05,572
we enter cirque-du-soleil
style, spiraling in on silks.

20
00:01:05,898 --> 00:01:10,272
I'm thinking peanut shells on the
floor, steaks the size of our heads,

21
00:01:10,273 --> 00:01:15,673
we enter hungry-man style
in our casual eating pants.

22
00:01:15,808 --> 00:01:20,800
Ahem. Maddie, this is our
big 1-6. It needs to be epic.

23
00:01:20,801 --> 00:01:23,220
I know! And I'm thinking epic.

24
00:01:23,221 --> 00:01:27,668
There's this new Cowboy-jail-themed
restaurant, The Hoosegow...

25
00:01:27,669 --> 00:01:30,069
Wow! Ha ha.

26
00:01:30,497 --> 00:01:34,333
We have very different ideas of epic.

27
00:01:34,334 --> 00:01:36,135
You know what's epic?

28
00:01:36,136 --> 00:01:38,571
Mini-golf birthday parties!

29
00:01:38,572 --> 00:01:41,165
They worked for years for you two.

30
00:01:41,166 --> 00:01:44,975
- Ages six to 11, done!
- Yes yes, Dad's right.

31
00:01:44,976 --> 00:01:47,179
Maddie, you loved it
because it was sporty.

32
00:01:47,180 --> 00:01:49,862
And, Liv, there was a Castle, so
you could dress up like a Princess.

33
00:01:49,863 --> 00:01:54,186
And I loved it because you two were
together celebrating your birthday.

34
00:01:54,187 --> 00:01:56,177
So, Rooney girls, let's get ready to...

35
00:01:56,178 --> 00:02:00,225
<i>♪Par-tay! Woo woo woo,
party! I said now party!</i>

36
00:02:00,226 --> 00:02:02,606
Mom! Mom, Mom.

37
00:02:02,607 --> 00:02:04,297
Stop.

38
00:02:04,298 --> 00:02:06,278
Hater.

39
00:02:09,137 --> 00:02:14,019
As fun as mini-golf was,
we are turning 16 now, so...

40
00:02:14,020 --> 00:02:15,894
We can plan our own party.

41
00:02:15,895 --> 00:02:18,839
So let's just talk this out
because nothing's been set in stone.

42
00:02:18,840 --> 00:02:24,480
Liv, I picked up the silks
for your epic party entrance.

43
00:02:25,253 --> 00:02:30,593
Epically bad decision to
bring them home on my bike.

44
00:02:31,192 --> 00:02:33,791
The silks have already been ordered?

45
00:02:33,792 --> 00:02:36,397
You've already planned our
entire party, haven't you?

46
00:02:36,398 --> 00:02:38,561
Well, it's not like I
could leave it up to you!

47
00:02:38,562 --> 00:02:40,357
I mean, casual eating pants?!

48
00:02:40,358 --> 00:02:41,951
Spiraling in on silks?!

49
00:02:41,952 --> 00:02:43,175
You're being impossible!

50
00:02:43,176 --> 00:02:45,240
Ugh, I wish we could just
have separate birthdays!

51
00:02:45,241 --> 00:02:47,761
- Ugh!
- Ugh!

52
00:02:58,152 --> 00:03:00,732
A little help?

53
00:03:02,754 --> 00:03:04,205
<i>♪Better in stereo.</i>

54
00:03:04,206 --> 00:03:06,287
<i>♪B... b... better in stereo.</i>

55
00:03:06,288 --> 00:03:08,399
<i>- ♪ I'm up with the sunshine.
- Let's go.</i>

56
00:03:08,400 --> 00:03:10,474
<i>- ♪I lace up my high tops.
- Oh no.</i>

57
00:03:10,475 --> 00:03:12,840
<i>- ♪ Slam dunk.
- Ready or not.</i>

58
00:03:12,841 --> 00:03:15,012
<i>♪Yeah, show me what you got.</i>

59
00:03:15,013 --> 00:03:17,156
<i>- ♪ I'm under the spotlight.
- Holler.</i>

60
00:03:17,157 --> 00:03:19,446
<i>♪I dare you, come on and follow.</i>

61
00:03:19,447 --> 00:03:23,120
<i>♪You dance to your own
beat. I'll sing the melody.</i>

62
00:03:23,121 --> 00:03:25,661
<i>♪When you say yea-ah-ah.</i>

63
00:03:25,662 --> 00:03:27,574
<i>♪I say no-oh-oh.</i>

64
00:03:27,575 --> 00:03:30,274
<i>♪When you say stop.</i>

65
00:03:30,275 --> 00:03:32,338
<i>♪All I want to do is go, go, go.</i>

66
00:03:32,339 --> 00:03:35,344
<i>♪You, you, the other half of me, me.</i>

67
00:03:35,345 --> 00:03:37,519
<i>♪The half I'll never be.</i>

68
00:03:37,520 --> 00:03:41,090
<i>♪The half that drives me crazy.</i>

69
00:03:41,091 --> 00:03:44,290
<i>♪You, you, the better half of me, me.</i>

70
00:03:44,291 --> 00:03:46,787
<i>♪The half I'll always need.</i>

71
00:03:46,788 --> 00:03:48,542
<i>♪But we both know.</i>

72
00:03:48,543 --> 00:03:52,083
<i>♪We're better in stereo...</i>

73
00:03:54,804 --> 00:03:58,310
My twins can't have separate
parties on their sweet 16th.

74
00:03:58,311 --> 00:04:02,132
So while you two were at
Maddie's basketball game...

75
00:04:02,133 --> 00:04:05,596
I came up with a "mompromise".

76
00:04:05,597 --> 00:04:08,297
So take a look.

77
00:04:09,707 --> 00:04:12,047
Dazzleberry!

78
00:04:12,186 --> 00:04:14,286
Awesome!

79
00:04:14,738 --> 00:04:19,718
Hold on, you haven't seen
the best part. Turn around.

80
00:04:19,719 --> 00:04:22,510
Two themes, one party.

81
00:04:22,511 --> 00:04:25,871
It's a red carpet rodeo!

82
00:04:26,477 --> 00:04:28,574
Hey, why isn't this thing working?

83
00:04:28,575 --> 00:04:31,469
So, Maddie honey, you
wanted to go to The Hoosegow.

84
00:04:31,470 --> 00:04:35,040
So we're gonna have a Western
barbecue right here in the house.

85
00:04:35,041 --> 00:04:36,806
- Joey.
- Mom, I don't want to say it.

86
00:04:36,807 --> 00:04:38,472
Oh, come on, say it.

87
00:04:38,473 --> 00:04:40,633
Yee-haw.

88
00:04:41,981 --> 00:04:46,433
And, Liv, you wanted a
cirque-du-soleil, red carpet gala.

89
00:04:46,434 --> 00:04:48,825
I bring you Parker-azzi!

90
00:04:48,826 --> 00:04:53,151
Liv Rooney, Liv Rooney!
Who are you wearing?

91
00:04:53,152 --> 00:04:55,266
Boom boom boom!

92
00:04:55,267 --> 00:04:57,187
Send.

93
00:04:57,493 --> 00:05:00,097
Mom, this is great! And I
think we're halfway there.

94
00:05:00,098 --> 00:05:03,488
The half that doesn't
look like a barn. So...

95
00:05:03,489 --> 00:05:07,929
I think we should clear out these hay bales
and, uh, ooh, all the barbecuey stuff.

96
00:05:07,930 --> 00:05:10,810
And I just really want to, you
know, class this place up, you know?

97
00:05:10,811 --> 00:05:13,109
I think we're gonna bring in
some French Contortionists...

98
00:05:13,110 --> 00:05:16,388
Liv, why are you throwing
out everything that I like?

99
00:05:16,389 --> 00:05:19,066
I thought this was our party.

100
00:05:19,067 --> 00:05:21,522
Honey, it's time to come clean.

101
00:05:21,523 --> 00:05:24,092
We've kept the secret for too long.

102
00:05:24,093 --> 00:05:25,681
No, Pete! I'm not telling them...

103
00:05:25,682 --> 00:05:29,522
They were born on separate days.

104
00:05:32,394 --> 00:05:36,097
<i>♪Camptown ladies sing
that song, doo-dah doo-dah.</i>

105
00:05:36,098 --> 00:05:39,818
This karaoke thing works great.

106
00:05:40,102 --> 00:05:41,646
Who's next?

107
00:05:41,647 --> 00:05:44,695
- What did you just say?
- We have different birthdays?

108
00:05:44,696 --> 00:05:49,733
Girls, your Mother and I
have something to tell you.

109
00:05:49,734 --> 00:05:51,954
Go ahead.

110
00:05:53,971 --> 00:05:59,871
Liv and Maddie, the day you were born
was truly the greatest day of my life.

111
00:06:00,021 --> 00:06:03,141
Well, that feels nice.

112
00:06:04,129 --> 00:06:07,184
But it was actually the
"days" you were born.

113
00:06:07,185 --> 00:06:11,690
You see, Liv, you were born
on the fifth at 11:56 P.M.

114
00:06:11,691 --> 00:06:14,037
But, Maddie honey, you were born...

115
00:06:14,038 --> 00:06:16,397
At 12:02 on the...

116
00:06:16,398 --> 00:06:17,559
Sixth.

117
00:06:17,560 --> 00:06:22,174
But the whole reason that five is my lucky
number is because I was born on the fifth.

118
00:06:22,175 --> 00:06:24,635
I'm a five.

119
00:06:24,645 --> 00:06:27,825
Apparently I'm a zero.

120
00:06:27,956 --> 00:06:33,656
Wait, so Maddie's never
celebrated on her actual birthday?

121
00:06:33,967 --> 00:06:37,627
What kind of parents are you?

122
00:06:38,815 --> 00:06:43,141
This is preposterous. I
say again preposterous.

123
00:06:43,142 --> 00:06:47,533
If they can do this to her, what
does that mean for everybody else?

124
00:06:47,534 --> 00:06:49,250
Am I six? Am I 11?

125
00:06:49,251 --> 00:06:52,011
I could be nine!

126
00:06:55,698 --> 00:06:58,218
- Hey.
- Hey.

127
00:07:00,737 --> 00:07:03,332
Maddie honey, I am so sorry.

128
00:07:03,333 --> 00:07:07,370
Every year your birthday would roll
around and I would just get so excited

129
00:07:07,371 --> 00:07:10,512
at the two of you sitting
together blowing out your candles.

130
00:07:10,513 --> 00:07:15,583
Then why did you use those trick
candles that didn't blow out?

131
00:07:15,584 --> 00:07:18,853
We wanted the moment to last forever.

132
00:07:18,854 --> 00:07:22,691
Plus a joint birthday
party is double the fun.

133
00:07:22,692 --> 00:07:24,955
It's also half the work.

134
00:07:24,956 --> 00:07:30,293
See, I'm trying to be completely
honest with you from now on.

135
00:07:30,294 --> 00:07:33,769
Don't try and be funny. I'm
still trying to be mad at you.

136
00:07:33,770 --> 00:07:37,142
Oh, honey, I'm so sorry. I thought
I was doing the right thing.

137
00:07:37,143 --> 00:07:40,045
You know, when you and Liv were
little, you were inseparable.

138
00:07:40,046 --> 00:07:43,506
As your Mother, it just felt wrong
to take that special day away.

139
00:07:43,507 --> 00:07:46,882
Are you seriously trying to take credit
for being a good Mother right now?

140
00:07:46,883 --> 00:07:49,819
Yes, I tried to slip that in.

141
00:07:49,820 --> 00:07:52,281
Now just tell me what you
want to do for your birthday

142
00:07:52,282 --> 00:07:54,957
and your Father and I will make
it happen... sky's the limit.

143
00:07:54,958 --> 00:07:59,796
Okay, there is a budget. See,
I'm trying to be totally honest.

144
00:07:59,797 --> 00:08:01,698
Thanks, Mom, but...

145
00:08:01,699 --> 00:08:04,917
Honestly the last thing I feel like doing
right now is celebrating my birthday.

146
00:08:04,918 --> 00:08:06,968
I don't even think I want to
have a party at all this year.

147
00:08:06,969 --> 00:08:09,030
And I'm sure Liv feels
the exact same way.

148
00:08:09,031 --> 00:08:10,420
Well, think about it.

149
00:08:10,421 --> 00:08:14,269
In the meantime I made your
favorite dinner; Chicken pot pie.

150
00:08:14,270 --> 00:08:16,847
Okay, I didn't make it.
It was frozen from a box.

151
00:08:16,848 --> 00:08:21,288
Woo, the truth really will set you free!

152
00:08:22,866 --> 00:08:25,055
Okay, everyone find a seat.

153
00:08:25,056 --> 00:08:27,409
Maddie's off on a jog,
but I bought us some time.

154
00:08:27,410 --> 00:08:31,596
She always jogs till the end of
her play list so I added six songs.

155
00:08:31,597 --> 00:08:32,723
Hey, Joey!

156
00:08:32,724 --> 00:08:36,348
I got a spot for you right here.

157
00:08:36,349 --> 00:08:41,869
I am going to say yes
because I am scared to say no.

158
00:08:45,511 --> 00:08:49,104
Okay, you are all here because you
are Maddie's best friends and family

159
00:08:49,105 --> 00:08:51,924
and I want you to help me to
throw a surprise party for her.

160
00:08:51,925 --> 00:08:54,509
You have to keep it a secret.

161
00:08:54,510 --> 00:08:57,922
I know that won't be
a problem for you two.

162
00:08:57,923 --> 00:09:00,665
Mom said Maddie doesn't
even want a party this year.

163
00:09:00,666 --> 00:09:04,206
Also I cannot feel my ribs.

164
00:09:05,041 --> 00:09:07,081
I can!

165
00:09:08,769 --> 00:09:11,395
Maddie says she doesn't want a
party, but I know my sister better.

166
00:09:11,396 --> 00:09:15,268
This birthday shocker has knocked her
for a loop and we are going to fix it.

167
00:09:15,269 --> 00:09:18,306
- That's very sweet.
- We'll do anything to help.

168
00:09:18,307 --> 00:09:23,097
Of course you will. You're
totally on the hook for this.

169
00:09:23,098 --> 00:09:26,753
Well, I'm in. I want Maddie's sweet
16 to be as amazing as it can be.

170
00:09:26,754 --> 00:09:29,727
- Careful.
- Right, sorry.

171
00:09:29,728 --> 00:09:33,599
Willow, Diggie, Stains, I want you to
take Maddie to The Hoosegow Saturday night

172
00:09:33,600 --> 00:09:35,812
while the rest of us get the
house ready for the party.

173
00:09:35,813 --> 00:09:37,572
Don't bring her home
until midnight though,

174
00:09:37,573 --> 00:09:39,487
because that's when her
actual birthday begins.

175
00:09:39,488 --> 00:09:41,736
Oh, midnight? Isn't that
awfully late for a party?

176
00:09:41,737 --> 00:09:43,356
You mean like...

177
00:09:43,357 --> 00:09:46,297
16 years late, Mom?

178
00:09:46,365 --> 00:09:49,425
Midnight party... fun.

179
00:09:54,188 --> 00:09:56,213
Hey, Mads, what are you doing?

180
00:09:56,214 --> 00:10:00,687
I am turning all my
lucky fives into sixes.

181
00:10:00,688 --> 00:10:06,588
Turns out the first 15 years of
my life I stuck fives everywhere.

182
00:10:06,729 --> 00:10:09,009
Everywhere.

183
00:10:10,533 --> 00:10:13,189
Identity crisis... I can relate.

184
00:10:13,190 --> 00:10:17,226
I remember the tough transition
from Joseph to Joe to Joey...

185
00:10:17,227 --> 00:10:20,374
With a brief stint as baby "J".

186
00:10:20,375 --> 00:10:22,577
Then ultimately back to Joey.

187
00:10:22,578 --> 00:10:25,814
- Wasn't there a J-Dog in there somewhere?
- Do not bring that up!

188
00:10:25,815 --> 00:10:27,735
Okay.

189
00:10:29,586 --> 00:10:31,262
Maddie and Joey Rooney?

190
00:10:31,263 --> 00:10:33,588
Character from my nightmares?

191
00:10:33,589 --> 00:10:35,749
Delivery.

192
00:10:37,925 --> 00:10:40,610
"You are cordially invited
to the cirque-du-soleil

193
00:10:40,611 --> 00:10:45,383
red carpet gala celebrating
the sweet 16 of Liv Rooney".

194
00:10:45,384 --> 00:10:48,230
Liv's throwing a party for herself?!

195
00:10:48,231 --> 00:10:50,560
The invitations
pretzel-butt was handing out

196
00:10:50,561 --> 00:10:53,137
were for a fake party to
throw Maddie off the scent.

197
00:10:53,138 --> 00:10:56,710
My job was to twist the
knife ever so delicately.

198
00:10:56,711 --> 00:10:59,428
I have a gentle touch.

199
00:10:59,429 --> 00:11:00,820
Wow.

200
00:11:00,821 --> 00:11:04,543
Liv took this whole birthday
scandal a lot better than you did.

201
00:11:04,544 --> 00:11:07,064
A lot better.

202
00:11:07,096 --> 00:11:09,316
A... lot.

203
00:11:10,241 --> 00:11:13,181
I can't believe she just
dumped me to do her own thing!

204
00:11:13,182 --> 00:11:15,793
Yeah. Does that make you mad?

205
00:11:15,794 --> 00:11:17,561
I bet I'd be mad.

206
00:11:17,562 --> 00:11:20,982
I'd be, like, really mad.

207
00:11:21,670 --> 00:11:23,302
You know what?

208
00:11:23,303 --> 00:11:27,674
If Liv's throwing a party for herself,
I'm throwing a party for myself.

209
00:11:27,675 --> 00:11:29,876
I'm going to The Hoosegow.

210
00:11:29,877 --> 00:11:33,264
Oh okay, here comes Maddie.
Remember we have plans on Sunday.

211
00:11:33,265 --> 00:11:35,686
Hey, guys, do you want
to go out for my birthday?

212
00:11:35,687 --> 00:11:37,330
Hoosegow on Sunday, what do you say?

213
00:11:37,331 --> 00:11:39,854
Can't, we have plans on Sunday.

214
00:11:39,855 --> 00:11:41,358
What plans?

215
00:11:41,359 --> 00:11:46,039
I... I've got a, um,
uh... a drum circle.

216
00:11:47,594 --> 00:11:50,414
Washing my ferret.

217
00:11:51,382 --> 00:11:53,662
Book Club?

218
00:11:54,901 --> 00:11:57,349
You're in a Book Club?

219
00:11:57,350 --> 00:12:00,773
Sure. Yeah, you know, every
month we pick a book and then...

220
00:12:00,774 --> 00:12:05,027
We watch the movie and talk about it.

221
00:12:05,028 --> 00:12:07,781
Well, we're available Saturday.

222
00:12:07,782 --> 00:12:11,064
Saturday? Aren't you
guys going to Liv's party?

223
00:12:11,065 --> 00:12:14,059
Not invited. Guess we
didn't make the cut.

224
00:12:14,060 --> 00:12:18,140
Liv's loss... we're the cool kids.

225
00:12:19,423 --> 00:12:23,928
Liv's throwing a party for herself
and she didn't even invite my friends.

226
00:12:23,929 --> 00:12:25,565
You know what?

227
00:12:25,566 --> 00:12:28,702
I am free Saturday. I'm
going to The Hoosegow.

228
00:12:28,703 --> 00:12:30,172
Yes!

229
00:12:30,173 --> 00:12:33,807
Snap! The jaws of my
traptastic plan had closed.

230
00:12:33,808 --> 00:12:35,402
Maddie hated me, hello...

231
00:12:35,403 --> 00:12:38,523
<i>♪Best sister... ever!</i>

232
00:12:40,499 --> 00:12:42,309
There you go, little lady.

233
00:12:42,310 --> 00:12:45,051
Now you are welcome to
as many return visits

234
00:12:45,052 --> 00:12:47,738
to the salad bar as you
want at The Hoosegow.

235
00:12:47,739 --> 00:12:52,479
But only under the
watchful eye of the law.

236
00:12:52,777 --> 00:12:56,814
"Watchful eye"? Hardly! I filled
my whole purse with baby corn.

237
00:12:56,815 --> 00:12:58,783
Don't mess with Sheriff Tim, Willow.

238
00:12:58,784 --> 00:13:02,684
He's packing ketchup and mustard.

239
00:13:02,987 --> 00:13:05,678
Dude, whatever Liv is doing
at her stupid red carpet party

240
00:13:05,679 --> 00:13:07,998
cannot be nearly as cool as this.

241
00:13:07,999 --> 00:13:09,843
I mean I'd much rather
be at a place where you

242
00:13:09,844 --> 00:13:11,563
can throw your peanut
shells on the floor.

243
00:13:11,564 --> 00:13:14,144
And rib bones.

244
00:13:16,735 --> 00:13:19,704
Am I not supposed to do that?

245
00:13:19,705 --> 00:13:21,872
Always the lady, Stains.

246
00:13:21,873 --> 00:13:24,573
Always the lady.

247
00:13:25,715 --> 00:13:27,812
Ooh, guys, they're gonna
arrest someone again.

248
00:13:27,813 --> 00:13:29,496
You are under arrest!

249
00:13:29,497 --> 00:13:31,967
Cattle rustling, stagecoach robbery!

250
00:13:31,968 --> 00:13:34,154
And spitting in the presence of a lady.

251
00:13:34,155 --> 00:13:35,615
Ooh!

252
00:13:35,616 --> 00:13:39,336
- Lock him up!
- Lock him up!

253
00:13:40,937 --> 00:13:44,117
Oh, I gotta take this.

254
00:13:46,004 --> 00:13:47,831
This is jailhouse one. Over.

255
00:13:47,832 --> 00:13:49,734
- Diggie, how's it going?
- It's great.

256
00:13:49,735 --> 00:13:52,343
We're just about to order dessert.
She doesn't suspect a thing.

257
00:13:52,344 --> 00:13:56,277
Dessert?! You can't be on dessert
already. It's barely 10:00!

258
00:13:56,278 --> 00:13:58,940
You need to keep her out
of the house until midnight.

259
00:13:58,941 --> 00:14:01,130
I mean, yeah, I'll do what
I can, but I didn't expect

260
00:14:01,131 --> 00:14:04,008
service to be this fantastic
at a restaurant like this.

261
00:14:04,009 --> 00:14:06,270
Diggie, I am trying to do
something nice for my sister.

262
00:14:06,271 --> 00:14:10,323
Do not make me regret trusting you.

263
00:14:10,324 --> 00:14:15,544
The fate of my plan rests
on someone named Diggie.

264
00:14:16,059 --> 00:14:18,819
- We gotta stall for time.
- Not a prob.

265
00:14:18,820 --> 00:14:24,400
Stains is doing the rotisserie
chicken stockade challenge.

266
00:14:26,963 --> 00:14:31,644
If you can eat a whole chicken in
under an hour, you get a free T-shirt.

267
00:14:31,645 --> 00:14:34,659
Come on, come on, come on!

268
00:14:34,660 --> 00:14:36,505
Wipe.

269
00:14:36,506 --> 00:14:42,406
Uh, you know what? Let's just
hose you off when this is over.

270
00:14:45,659 --> 00:14:48,132
A one-layer cake? Really?

271
00:14:48,133 --> 00:14:53,867
You think that's gonna make up
for 16 years of fibs and fraud?

272
00:14:53,868 --> 00:14:57,503
I was going to put little purple
frosting flowers on the corners.

273
00:14:57,504 --> 00:15:00,921
That will impress... no one.

274
00:15:00,922 --> 00:15:03,709
Well, I suppose I could
do a few more layers.

275
00:15:03,710 --> 00:15:08,390
Oh yeah! Maybe one
for each year you lied.

276
00:15:15,152 --> 00:15:20,294
Wow, this mini-golf course
actually looks pretty good.

277
00:15:20,295 --> 00:15:24,398
I'm glad someone's paying attention
to what Liv's trying to pull off.

278
00:15:24,399 --> 00:15:27,792
- Let's see the windmill spin.
- Oh, it doesn't actually spin.

279
00:15:27,793 --> 00:15:32,506
Yeah, no, it's decorative, like the
treadmill in Mom and Dad's bedroom.

280
00:15:32,507 --> 00:15:34,542
Are you kidding me?

281
00:15:34,543 --> 00:15:37,985
A crummy cake and a
non-spinning windmill?

282
00:15:37,986 --> 00:15:41,383
Liv is trying is recreate the
mini-golf parties of Maddie's youth.

283
00:15:41,384 --> 00:15:44,105
And if we're gonna do this,
we're gonna do this right.

284
00:15:44,106 --> 00:15:46,998
I guess we could take apart
one of my remote-controlled cars

285
00:15:46,999 --> 00:15:49,324
and use the motor from
that to make it spin.

286
00:15:49,325 --> 00:15:51,855
Yeah, we could do that. We can
also play duck, duck, goose.

287
00:15:51,856 --> 00:15:55,576
Am I the only one that cares?

288
00:15:56,298 --> 00:16:00,891
We could take apart the riding mower
and use the motor from that bad boy.

289
00:16:00,892 --> 00:16:02,436
Now we're talking.

290
00:16:02,437 --> 00:16:06,964
Birthday magic, monkeys.
Let's go, let's go!

291
00:16:06,965 --> 00:16:08,846
Where are you going?

292
00:16:08,847 --> 00:16:11,169
It's already two hours past my bedtime.

293
00:16:11,170 --> 00:16:17,070
If I don't take a nap before the party,
I'm going to be a real cranky-pants.

294
00:16:18,453 --> 00:16:20,011
Good job, Stains.

295
00:16:20,012 --> 00:16:24,872
I have never been more
impressed or disgusted.

296
00:16:25,526 --> 00:16:29,662
And you know you didn't have to eat
the bones to win the shirt, right?

297
00:16:29,663 --> 00:16:32,908
When I commit, I commit, man.

298
00:16:32,909 --> 00:16:36,669
We hear it's your birthday, little lady.

299
00:16:36,670 --> 00:16:39,340
Uh, it's not my actual
birthday till midnight.

300
00:16:39,341 --> 00:16:44,084
Well, blow out the candle.
It's somebody's birthday.

301
00:16:44,085 --> 00:16:46,658
Go ahead, Maddie, make a wish.

302
00:16:46,659 --> 00:16:48,927
Guys, I can't.

303
00:16:48,928 --> 00:16:51,778
Sheriff Tim was right.
It is somebody's birthday.

304
00:16:51,779 --> 00:16:53,039
It's Liv's.

305
00:16:53,040 --> 00:16:56,210
And the only wish that popped
into my head was that...

306
00:16:56,211 --> 00:16:58,848
I wish we were blowing out
our candles together, so.

307
00:16:58,849 --> 00:17:03,889
That's so sweet. Let's
get you home right now.

308
00:17:05,333 --> 00:17:06,624
Uh...

309
00:17:06,625 --> 00:17:07,790
Get up.

310
00:17:07,791 --> 00:17:10,387
Well, are you sure? We've...

311
00:17:10,388 --> 00:17:13,331
Barely been here for four hours.

312
00:17:13,332 --> 00:17:15,858
You are under arrest... cattle rustling.

313
00:17:15,859 --> 00:17:17,910
Wait! You're arresting the wrong bandit.

314
00:17:17,911 --> 00:17:19,984
This girl here, she's
the cattle rustler.

315
00:17:19,985 --> 00:17:22,285
Yeah, man, lock her up!

316
00:17:22,286 --> 00:17:24,949
Lock her up, lock her up!

317
00:17:24,950 --> 00:17:27,618
- Lock her up!
- Guys!

318
00:17:27,619 --> 00:17:31,750
Guys, let me out! I need to get
to Liv's party before it's over!

319
00:17:31,751 --> 00:17:33,427
Good move, Diggie.

320
00:17:33,428 --> 00:17:35,780
I'm smooth under pressure.

321
00:17:35,781 --> 00:17:41,681
There's a new Sheriff in town and his name
is Diggie and he's the meanest Sheriff on..

322
00:17:44,055 --> 00:17:45,455
We're outta here.

323
00:17:45,456 --> 00:17:48,756
Well... go! Maddie, wait.

324
00:17:53,175 --> 00:17:57,135
Mr. & Mrs. Joey and Willow Rooney.

325
00:18:01,383 --> 00:18:06,423
It's midnight! Seriously,
Diggie, 16 wrong turns?

326
00:18:07,328 --> 00:18:11,298
And I've never heard of that law that
says you have to go 5mph past a graveyard?

327
00:18:11,299 --> 00:18:15,199
That's just showing respect, man.

328
00:18:15,788 --> 00:18:18,066
Wait a second, I don't hear music.

329
00:18:18,067 --> 00:18:20,387
And the red carpet is
rolled up. You guys!

330
00:18:20,388 --> 00:18:23,688
I missed Liv's birthday!

331
00:18:25,241 --> 00:18:26,863
Su...

332
00:18:26,864 --> 00:18:31,184
Upper was really great at The Hoosegow.

333
00:18:32,439 --> 00:18:35,319
Where is everybody?

334
00:18:37,967 --> 00:18:40,127
Surprise!

335
00:18:41,717 --> 00:18:46,577
We missed saying surprise.
Are you kidding me?

336
00:18:48,658 --> 00:18:51,276
Happy sweet 16, Maddie.

337
00:18:51,277 --> 00:18:53,228
Wait, this is all for me?

338
00:18:53,229 --> 00:18:55,403
- Where is your red carpet party?
- Oh.

339
00:18:55,404 --> 00:18:57,923
There was no party.
I totally played you.

340
00:18:57,924 --> 00:19:02,174
Don't feel bad though, I am a day older.

341
00:19:02,175 --> 00:19:04,095
Ahem.

342
00:19:04,983 --> 00:19:07,794
Hey, remember us, the
people you were screaming

343
00:19:07,795 --> 00:19:10,246
at the whole way past the graveyard?

344
00:19:10,247 --> 00:19:13,816
You guys were all in on this?

345
00:19:13,817 --> 00:19:16,451
Liv, mini-golf?

346
00:19:16,452 --> 00:19:18,824
It's just like old times.

347
00:19:18,825 --> 00:19:22,120
We got so wrapped up in which kind
of party we each wanted that...

348
00:19:22,121 --> 00:19:25,459
We forgot the best part
is doing it together.

349
00:19:25,460 --> 00:19:28,940
<i>♪Someone made you a cake!</i>

350
00:19:29,627 --> 00:19:32,760
Guys, a basketball cake?

351
00:19:32,761 --> 00:19:33,775
This is amazing.

352
00:19:33,776 --> 00:19:36,369
16 layers of butter-cream and guilt.

353
00:19:36,370 --> 00:19:39,706
Mama's done her time. We're moving on.

354
00:19:39,707 --> 00:19:42,173
Now the Rooney girls
are ready to get their...

355
00:19:42,174 --> 00:19:44,347
<i>♪A-party on, a-party on.</i>

356
00:19:44,348 --> 00:19:46,580
<i>♪Whoop whoop, their party on.</i>

357
00:19:46,581 --> 00:19:48,581
<i>♪Party on, whoop whoop...</i>

358
00:19:48,582 --> 00:19:51,222
Ahem! Mom, Mom.

359
00:19:51,277 --> 00:19:54,555
We've forgiven you. You
can stop punishing us.

360
00:19:54,556 --> 00:19:59,070
Maddie, we even got
you a number six jersey,

361
00:19:59,071 --> 00:20:02,630
because we figured you'd
want to change your number.

362
00:20:02,631 --> 00:20:07,062
Actually I've decided to
stick with the number five.

363
00:20:07,063 --> 00:20:11,503
That's the day my best friend was born.

364
00:20:11,572 --> 00:20:14,565
So, Liv, come over here. Help
me blow out all these candles.

365
00:20:14,566 --> 00:20:17,016
Uh, okay, one sec. I want
to get a video of this.

366
00:20:17,017 --> 00:20:21,010
I think I left my phone in the garage
when I was grabbing decorations.

367
00:20:21,011 --> 00:20:24,352
Video? Then we've gotta
have the windmill going.

368
00:20:24,353 --> 00:20:27,113
Dad, turn it on.

369
00:20:34,497 --> 00:20:37,765
- Guys, I love it.
- We did a good thing today, boys.

370
00:20:37,766 --> 00:20:42,446
See what happens when
you listen to Parker?

371
00:20:45,283 --> 00:20:49,363
That thing kicks up quite a breeze.

372
00:20:54,451 --> 00:20:57,211
Oh no, the cake!

373
00:21:00,757 --> 00:21:03,856
We have to turn this thing off!

374
00:21:03,857 --> 00:21:08,777
Don't worry, birthday
girl, your hero is here.

375
00:21:20,134 --> 00:21:22,411
Hey, superhero...

376
00:21:22,412 --> 00:21:25,592
The plug's right here.

377
00:21:32,389 --> 00:21:34,309
Whoa.

378
00:21:37,694 --> 00:21:41,534
Oh, nice work on the cake, Mom!

379
00:21:46,700 --> 00:21:48,381
Oh, hey Parker, check it out.

380
00:21:48,382 --> 00:21:51,151
Liv got us a present for
helping out with the party.

381
00:21:51,152 --> 00:21:54,752
It's nice to be appreciated.

382
00:21:55,879 --> 00:21:58,039
Surprise!

383
00:22:01,130 --> 00:22:03,479
Dude, that was awesome.
Nice work, pretzel-butt.

384
00:22:03,480 --> 00:22:05,479
Up top!

385
00:22:05,480 --> 00:22:06,674
Yeah!

386
00:22:06,675 --> 00:22:10,413
Hey, can you fit in my underwear
drawer? I want to freak out my Mom.

387
00:22:10,414 --> 00:22:12,694
Follow me!

388
00:22:12,809 --> 00:22:18,709
<font color="#00FFFF">Sync & corrections by P2Pfiend.
</font> <font color="#00FF40">Www.Addic7ed.Com.</font>

