﻿1
00:06:44,036 --> 00:06:48,140
Here you go, there she be.

2
00:06:48,574 --> 00:06:49,575
Awesome.

3
00:10:04,537 --> 00:10:07,038
It's gnarly.

4
00:10:07,907 --> 00:10:10,175
Man that's massive.

5
00:14:00,539 --> 00:14:06,744
The ridge looks stable but it is
going to be gnarly trying to
figure that shit out.

6
00:14:07,079 --> 00:14:09,113
Looks like pow.

7
00:14:09,315 --> 00:14:10,882
What's that?

8
00:14:10,884 --> 00:14:12,217
Looks like pow.

9
00:14:12,384 --> 00:14:15,954
Yeah, looks awesome.

10
00:14:18,591 --> 00:14:21,793
Let's keep going.

11
00:15:13,946 --> 00:15:16,648
I don't think this is going to
go.

12
00:15:19,852 --> 00:15:20,853
No good?

13
00:15:22,321 --> 00:15:29,327
I think the spines might have to
wait another day.

14
00:15:44,009 --> 00:15:53,084
Oh man this is so not going to
go it is like full on trench
warfare up here.

15
00:15:53,086 --> 00:15:55,887
This seems like a pretty good
way to get worked.

16
00:21:47,139 --> 00:21:49,740
It's getting warm.

17
00:21:55,013 --> 00:22:03,454
Um, I think we have to make a
decision we probably got another
hour if we're lucky?

18
00:22:03,622 --> 00:22:04,989
You're going to bail?

19
00:22:05,157 --> 00:22:07,758
I don't know it's right on the
edge.

20
00:22:11,296 --> 00:22:13,497
I think we should go down.

21
00:22:13,665 --> 00:22:15,399
I'm going to go for it.

22
00:22:19,204 --> 00:22:21,939
I don't know if it's a good call
but...

23
00:22:21,941 --> 00:22:25,609
I'll see you down there.

24
00:22:25,611 --> 00:22:26,911
Okay.

25
00:30:34,465 --> 00:30:38,035
Jimmy do you copy?

26
00:30:38,236 --> 00:30:42,906
Jimmy I'm not reading you? Do
you copy?

27
00:34:50,688 --> 00:34:54,257
You going to drop it bud?

28
00:34:54,425 --> 00:35:00,597
I'm going to take my skins off,
put my skis on and have a look.

29
00:35:00,798 --> 00:35:02,833
Alright man, keep me posted.

30
00:35:03,000 --> 00:35:04,101
Copy.

31
00:49:38,976 --> 00:49:48,017
I'm just going to check your
pupils here... alright, good.

32
00:50:12,843 --> 00:50:13,844
Holy shit.

33
00:50:25,555 --> 00:50:27,055
What was I thinking?

34
00:59:46,549 --> 00:59:47,749
Woo, go Angel!

35
01:00:54,684 --> 01:01:01,456
Hahaha dancing with it! Way to
hang on in your sluff bud.

36
01:03:19,695 --> 01:03:23,265
These moments make everything
worth it.

37
01:05:05,701 --> 01:05:07,051
Woo, magnificent.

38
01:15:04,533 --> 01:15:08,035
Jimmy do you copy?

39
01:15:09,505 --> 01:15:10,605
Yeah I copy.

40
01:15:13,275 --> 01:15:17,445
Uh, I am up here on the
ridge, I don't know it is not
looking so good.

41
01:15:17,447 --> 01:15:22,283
I am going to bail and down
climb, it is just to warm,
these cornices are sketchy.

42
01:15:22,285 --> 01:15:27,054
Alright keep me posted, be
careful.

43
01:15:27,222 --> 01:15:30,791
10-4 see you back at the camp.

