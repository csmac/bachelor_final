﻿1
01:59:55,401 --> 02:00:00,737
♪

2
02:00:00,807 --> 02:00:04,675
♪ It's in the way you are ♪

3
02:00:04,744 --> 02:00:09,044
♪ You've got a way with me ♪

4
02:00:11,351 --> 02:00:16,152
♪ Somehow you got me
to believe ♪

5
02:00:18,658 --> 02:00:24,324
♪ In everything
that I could be ♪

6
02:00:24,397 --> 02:00:26,388
♪ I gotta say ♪

7
02:00:26,466 --> 02:00:29,958
♪ You really got a way ♪

8
02:00:30,036 --> 02:00:34,473
♪ It's in the way you want me ♪

9
02:00:36,876 --> 02:00:41,813
♪ It's in the way you hold me ♪

10
02:00:44,083 --> 02:00:49,077
♪ The way you show me
just what love's ♪

11
02:00:49,155 --> 02:00:51,385
♪ Made of ♪

12
02:00:51,457 --> 02:00:58,158
♪ It's in the way we make love ♪

13
02:00:58,231 --> 02:01:01,723
♪ Oh, how I adore you ♪

14
02:01:01,801 --> 02:01:05,100
♪ Like no one before you ♪

15
02:01:05,171 --> 02:01:09,938
♪ I love you
just the way you are ♪

16
02:01:10,009 --> 02:01:14,571
♪ It's in the way you want me ♪

17
02:01:16,516 --> 02:01:21,886
♪ Oh, it's in the way
you hold me ♪

18
02:01:24,123 --> 02:01:29,026
♪ The way you show me
just what love's ♪

19
02:01:29,095 --> 02:01:31,325
♪ Made of ♪

20
02:01:31,397 --> 02:01:37,097
♪ It's in the way we make love ♪

21
02:01:38,771 --> 02:01:45,540
♪ It's just the way you are ♪

