1
00:00:06,400 --> 00:00:20,400
<font color=#00FFFF>davidseanghia</font>
<font color=#FFFF00>.: www.phudeviet.org :.</font>

1
00:00:25,400 --> 00:00:28,400
Không quân!
Chúng tôi cần hỗ trợ trên không!

2
00:00:29,520 --> 00:00:32,680
"Ở yên đấy, Sierra 2-0,
chúng tôi đang đến."

3
00:00:34,840 --> 00:00:35,840
Aaargh!

4
00:00:40,360 --> 00:00:44,080
Cấp cứu! Tôi đây! Sẽ ổn thôi!
Nằm yên đấy!

5
00:00:47,320 --> 00:00:49,560
Trả lời, Không quân!

6
00:00:49,560 --> 00:00:52,680
Ở đâu thế?!
Hỗ trợ trên không ở chỗ đéo nào thế?!

7
00:00:52,680 --> 00:00:55,880
"Fast Air đang đến, Sierra 2-0."

8
00:01:22,680 --> 00:01:28,320
Viện binh của địch
đang đến từ hướng Nam!

9
00:01:28,320 --> 00:01:33,560
Viện binh của địch!
Cần hỗ trợ trên không ngay!

10
00:01:33,560 --> 00:01:36,120
"Saxon 4-3, một phút nữa đến."

11
00:01:36,120 --> 00:01:39,600
Josefa!

12
00:01:49,280 --> 00:01:52,200
"30 giây nữa."

13
00:01:52,200 --> 00:01:56,920
Đã đánh dấu mục tiêu!
Đã đánh dấu mục tiêu!

14
00:01:56,920 --> 00:02:01,040
Delta Oscar Bravo 7-4 Papa Alpha!
Xác nhận!

15
00:02:01,040 --> 00:02:05,280
"Xác nhận mục tiêu,
Delta Oscar Bravo 7-4 Papa Alpha."

16
00:02:12,520 --> 00:02:14,640
"Đã khóa mục tiêu, Sierra 2-0."

17
00:02:17,560 --> 00:02:19,600
"Hàng phỏng trym đây."

18
00:02:19,600 --> 00:02:21,880
Tuyệt!

19
00:02:31,760 --> 00:02:34,720
Đậu móa. Đến kìa!

20
00:02:41,120 --> 00:02:45,400
Người đẹp của anh!

21
00:02:52,240 --> 00:02:55,400
Có sự cố trong hệ thống dẫn hướng.

22
00:02:55,400 --> 00:02:58,760
Các tên lửa của ta
bắn trúng quân Mỹ bốn lần

23
00:02:58,760 --> 00:03:01,520
trong tháng qua.

24
00:03:01,520 --> 00:03:04,760
RAF Fast Air JDAM lẽ ra
phải tấn công một cơ sở của Taliban.

25
00:03:04,760 --> 00:03:06,800
Thay vào đó lại tấn công
vào Lính thủy đánh bộ Mỹ,

26
00:03:06,800 --> 00:03:10,520
một vùng rộng 600 đầy mục tiêu.
Nhiều lính đã hy sinh

27
00:03:10,520 --> 00:03:13,000
và người Mỹ muốn câu trả lời.

28
00:03:13,000 --> 00:03:16,360
Bộ chỉ huy không quân đã nhận thấy
một vết truy cập điện tử.

29
00:03:16,360 --> 00:03:20,360
- Có kẻ trên mặt đất đã bẻ khóa mật mã.
- Không thể thế được.

30
00:03:20,360 --> 00:03:25,920
- Tất cả hệ thống đều chống xâm nhập 100%.
- Hết rồi.

31
00:03:25,920 --> 00:03:29,840
Nếu Taliban có thể xâm nhập hệ thống
và đổi hướng các tên lửa của ta thì

32
00:03:29,840 --> 00:03:34,960
nghiêm trọng đấy.
Cần có câu trả lời và cách giải quyết ngay.

33
00:03:34,960 --> 00:03:38,600
Ta đang thua te mỏ ở Helmand.
Tình hình đã thay đổi

34
00:03:38,600 --> 00:03:41,880
và chúng ta không biết tại sao.
Có thể ta chỉ đoán là

35
00:03:41,880 --> 00:03:45,960
một loại thủ lĩnh mới nổi.
Hắn phải bị ngăn lại ngay.

36
00:03:45,960 --> 00:03:48,120
Hắn đang làm loạn chiến dịch.

37
00:04:45,560 --> 00:04:49,000
Gerald Baxter.
Gerald Baxter ở Iraq năm 2003

38
00:04:49,000 --> 00:04:53,000
là một nhà thầu hỗ trợ kỹ thuật,
thường dân, không phải quân nhân.

39
00:04:53,000 --> 00:04:56,400
Một kỹ sư phần mềm định vị tên lửa.
Nhưng hắn đã thất bại.

40
00:04:56,400 --> 00:05:00,400
Hắn chịu trách nhiệm một vụ đánh bom
vào một ngôi làng.

41
00:05:00,400 --> 00:05:03,520
Phụ nữ và trẻ em đã thiệt mạng.

42
00:05:03,520 --> 00:05:07,040
Baxter được chẩn đoán
không đủ tâm lý để phục vụ

43
00:05:07,040 --> 00:05:10,600
tại những vùng xung đột.
Quay về Anh, nơi hắn được chẩn đoán

44
00:05:10,600 --> 00:05:14,160
và nhập viện với chứng trầm cảm
hậu chấn thương. Chúng tôi nghĩ

45
00:05:14,160 --> 00:05:17,240
hắn giờ có thể đang ở Afghanistan.
Tất cả hồ sơ về Baxter

46
00:05:17,240 --> 00:05:20,880
dừng lại vào Tháng 9/2005.
Hắn lặn mất tăm kể từ ngày đó.

47
00:05:20,880 --> 00:05:23,800
Tập trung vào những gì
chúng ta có về hắn.

48
00:05:23,800 --> 00:05:28,280
John, nhiệm vụ của anh là vào
và bắt Gerald Baxter.

49
00:05:28,280 --> 00:05:30,520
Vậy tôi sẽ vào vai
một tên buôn vũ khí?

50
00:05:30,520 --> 00:05:34,040
Ba giai đoạn tiêu chuẩn của
định vị tên lửa là gì?

51
00:05:34,040 --> 00:05:39,080
Tôi muốn một phiên dịch và
một kẻ có liên hệ với Taliban.

52
00:05:39,080 --> 00:05:43,640
- Tăng tốc, đoạn giữa, điểm đến.
- Lẽ ra anh phải biết điều này.

53
00:05:50,440 --> 00:05:54,720
- Đọc hai kiểu điều khiển radar.
- Ra lệnh.

54
00:05:56,720 --> 00:05:59,920
- Khóa?
- Chưa, mới là tia dẫn đường.

55
00:05:59,920 --> 00:06:03,640
- Nó hả?
- John, anh sẽ phải cẩn thận.

56
00:06:03,640 --> 00:06:07,280
Ý em là hãy cẩn thận.

57
00:06:07,280 --> 00:06:09,480
Anh sẽ cẩn thận.

58
00:07:24,640 --> 00:07:29,400
Đó là loại đạn lõi thép
FMJ 7.62 x 0.39 mm.

59
00:07:32,680 --> 00:07:35,120
170 triệu viên...

60
00:07:36,720 --> 00:07:39,960
..85 đô cho 1000 viên.

61
00:07:45,040 --> 00:07:48,920
Thôi nào, Hàng Ukraina đấy,
tiêu chuẩn châu Âu.

62
00:07:58,760 --> 00:08:00,760
Thủ lĩnh không quan tâm.

63
00:08:11,120 --> 00:08:13,240
Được thôi.

64
00:08:18,280 --> 00:08:22,920
Tôi có đồng nghiệp có liên hệ với
mạng Arafel ở Chandrigar.

65
00:08:22,920 --> 00:08:27,000
Họ có thể truy cập vào
phần mềm mã hóa LTD

66
00:08:27,000 --> 00:08:31,000
dùng cho đời mới nhất của
tên lửa định vị bằng laser, Brimstone.

67
00:08:31,000 --> 00:08:34,480
Một thiên đường cho hacker.

68
00:08:36,640 --> 00:08:39,760
Những vũ khí này sẽ được nối mạng
với lực lượng ISAF

69
00:08:39,760 --> 00:08:44,320
trong khoảng ba tháng nữa. Điều khiển chúng
thì các người cũng điều khiển luôn cuộc chiến.

70
00:08:44,320 --> 00:08:46,520
Thủ lĩnh sẽ muốn chắc chắn

71
00:08:46,520 --> 00:08:49,640
anh là người như anh nói, Wallace.

72
00:08:50,720 --> 00:08:54,360
Quay về khách sạn rồi
ông sẽ liên lạc sau.

73
00:09:10,200 --> 00:09:13,560
Làm cho xong đi.

74
00:09:13,560 --> 00:09:15,840
Oof!

75
00:09:49,400 --> 00:09:51,480
- Frank!
- Hey, Hugh.

76
00:09:51,480 --> 00:09:54,560
Hân hạnh.
Frank Arlington, đây là Trung úy Thompson.

76
00:09:54,580 --> 00:09:55,160
Chào anh.

77
00:09:55,160 --> 00:09:58,000
Cục an ninh đặc biệt Hoa Kỳ
biệt phái đến Luôn Đôn.

78
00:09:58,000 --> 00:10:01,520
Tôi là liên lạc cho chiến dịch
"quân ta bắn nhầm quân mình".

79
00:10:01,520 --> 00:10:04,800
Không thể để chuyện này
có thể tồi tệ hơn nữa.

80
00:10:04,800 --> 00:10:08,560
Tổng thống sẽ trình lên nhiều đề xuất
trước Quốc hội vào tuần tới

81
00:10:08,560 --> 00:10:11,840
nên chúng ta cần xử lý
tình hình này càng nhanh càng tốt.

82
00:10:11,840 --> 00:10:16,600
Tôi đến để giúp, liên lạc, kết nối.
Bất cứ mọi chuyện.

83
00:10:16,600 --> 00:10:18,880
Rất vui khi được biết,
nhưng để tôi làm anh yên tâm là

84
00:10:18,880 --> 00:10:22,520
chúng tôi đã kiểm soát tình hình.
Mời đi lối này.

85
00:10:51,880 --> 00:10:56,800
Gerald Baxter. Chúng tôi không chắc
sao hắn đến được Afghanistan.

86
00:10:56,800 --> 00:10:59,760
Có thể hắn bị bắt ngoài ý muốn
hoặc cũng có thể không.

87
00:10:59,760 --> 00:11:04,080
- Anh định cứu hay xử hắn?
- Chúng tôi muốn hắn còn sống.

88
00:11:04,080 --> 00:11:06,880
Chúng tôi cần biết sao hắn có thể
chuyển hướng tên lửa của chúng tôi.

89
00:11:06,880 --> 00:11:09,520
Hắn có đưa kiến thức của hắn
cho Taliban hay không?

90
00:11:09,520 --> 00:11:12,520
Cộng với tin của hắn về
chiến lược của Taliban có thể vô giá.

91
00:11:12,520 --> 00:11:16,160
- Anh định làm thế nào?
- Chúng tôi đã cài người vào đó.

92
00:11:16,160 --> 00:11:19,000
- Ai? Anh ấy đâu?
- Này.

93
00:11:19,000 --> 00:11:22,400
Chúng tôi không vui về chuyện này
dù ở Langley hay ở Nhà Trắng.

94
00:11:22,400 --> 00:11:25,640
Việc đe dọa chức Tổng thống
ở Afghanistan sẽ chỉ có ích

95
00:11:25,640 --> 00:11:29,200
nếu anh cập nhật cho tôi
mọi bước tiến của kế hoạch.

96
00:11:29,200 --> 00:11:31,520
Rõ chứ?

97
00:11:32,800 --> 00:11:36,880
- Thông tin này sẽ được cùng chia sẻ?
- Không đâu.

98
00:11:39,080 --> 00:11:41,960
Nhưng hãy nhớ là chúng tôi sẽ
theo đuổi đến cùng

99
00:11:41,960 --> 00:11:44,600
một cách mạnh mẽ đấy.

100
00:11:45,640 --> 00:11:48,160
"Có một người da trắng bị bắt cóc."

101
00:11:48,160 --> 00:11:52,760
- "Vài tên Tăng li ba trong xe."
- "Nghe rõ, chờ lệnh."

102
00:11:52,760 --> 00:11:56,720
"Có thể là người châu Âu.
Chờ tin xác nhận."

103
00:11:56,720 --> 00:11:59,960
- "Nghe rõ, Delta Bravo."
- Không được động đậy.

104
00:12:05,800 --> 00:12:09,520
"Phải, là một người châu Âu."
"Xác nhận và nghe rõ."

105
00:12:09,520 --> 00:12:12,920
"Cảnh sát Afghan đang kiểm tra vụ bắt cóc."

106
00:12:12,920 --> 00:12:16,160
"Chúng đã hối lộ cho họ."

107
00:12:16,160 --> 00:12:19,680
"Chẳng khó chút nào.
Chúng đang di chuyển."

108
00:12:19,680 --> 00:12:22,600
- "Nghe rõ, chờ lệnh."
- Động đậy là chết.

109
00:12:22,600 --> 00:12:26,400
"Ra hiệu đi, Delta Bravo.
Chắc là một vụ bắt cóc, có thể là manh mối."

110
00:12:26,400 --> 00:12:29,520
"Rõ, sẽ quan sát và theo dõi.
Delta Bravo ngưng."

111
00:13:00,880 --> 00:13:02,720
Josefa!

112
00:13:02,720 --> 00:13:04,880
Gì thế?

113
00:13:14,920 --> 00:13:18,680
Josef Mohammed.
Tên anh là gì?

114
00:13:20,200 --> 00:13:23,720
- Tom Wallace.
- Chào mừng đến Chateau Taliban, Tom Wallace.

115
00:13:25,360 --> 00:13:27,720
Bọn tôi đang đợi anh.

116
00:13:54,640 --> 00:13:57,480
Ổn chứ anh bạn?
Anh có gì thế?

117
00:13:58,800 --> 00:14:03,800
- Anh đã biết tôi có gì.
- Hệ thống Arafel, Chandrigar?

118
00:14:03,800 --> 00:14:07,000
Họ đang thiết kế phần mềm dẫn đường
cho tên lửa Brimstones mới.

118
00:14:07,010 --> 00:14:07,920
Thì sao chứ?

119
00:14:07,920 --> 00:14:11,200
Tôi truy cập vào được mã hóa LTD
của mấy quả tên lửa đó.

120
00:14:11,200 --> 00:14:13,360
Điều khiển mọi di chuyển của chúng.

121
00:14:26,160 --> 00:14:28,360
Tommy Wallace!

122
00:14:28,360 --> 00:14:31,400
Tên nước ngoài cặn bã, buôn vũ khí.

123
00:14:31,400 --> 00:14:35,000
Anh có một phút lập trình
hệ thống định vị tên lửa.

124
00:14:35,000 --> 00:14:37,800
- Thời gian của anh bắt đầu... ngay!
- Không, tôi -

125
00:14:37,800 --> 00:14:41,240
Tên của hai hệ thống điều khiển
lập trình mọi định vị tên lửa.

126
00:14:41,240 --> 00:14:44,200
- Cao độ và đường bay.
- Hai loại radar định vị.

127
00:14:44,200 --> 00:14:47,040
Ra lệnh và tia dẫn hướng.

128
00:14:47,040 --> 00:14:49,200
Tên người duy nhất...

129
00:14:50,280 --> 00:14:53,440
...đoạt giải Nobel văn chương
chơi bóng ở Cúp thế giới.

130
00:14:54,960 --> 00:14:56,800
Albert Camus.

131
00:14:58,680 --> 00:15:02,480
Tên lửa Brimstone hiện tại
đang dùng loại nào kể trên?

132
00:15:02,480 --> 00:15:05,960
Không phải kỹ thuật.
Vận hành tại trạm, không phải radar.

133
00:15:05,960 --> 00:15:10,120
Dùng phương pháp so sánh biên độ đơn mạch.

134
00:15:11,600 --> 00:15:15,160
- Anh làm cho ai?
- Tập đoàn Tom Wallace.

135
00:15:15,160 --> 00:15:16,680
Được rồi.

136
00:15:18,200 --> 00:15:21,080
- Đi với tôi.
- Không đâu.

137
00:15:22,240 --> 00:15:24,760
Tôi đi xa đủ rồi.

138
00:15:24,760 --> 00:15:28,360
Anh muốn mã thì đi với tôi.
Cứ dẫn theo bảo vệ nếu thích.

139
00:15:28,360 --> 00:15:32,480
Nhưng tôi muốn đảm bảo chúng
không phải một đám cuồng tín.

140
00:15:32,480 --> 00:15:36,880
- Tôi không đi du lịch mà là làm ăn.
- Coi chừng mồm miệng đấy.

141
00:15:36,880 --> 00:15:41,040
Tôi không tin anh đến nỗi
sẽ đi theo anh, họ cũng thế,

142
00:15:41,040 --> 00:15:44,800
họ xem anh là kẻ đồi bại,
thối nát và đạo đức suy đồi.

143
00:15:44,800 --> 00:15:48,560
Tôi tình cờ nghe được tin
về một người duy nhất ở nước này

144
00:15:48,560 --> 00:15:52,560
nói về việc đáng giá
khi mua thứ khỉ này.

145
00:15:52,560 --> 00:15:55,920
Một tên cặn bã như anh,
không đảng để

146
00:15:55,920 --> 00:15:58,840
liếm giày một người như...

147
00:15:58,840 --> 00:15:59,680
Một người như ai?

147
00:15:59,840 --> 00:16:01,580
- Hả?
- Anh nói gì thế?

148
00:16:01,680 --> 00:16:04,040
Sếp anh là ai?

149
00:16:04,040 --> 00:16:06,280
Được rồi.

150
00:16:06,280 --> 00:16:09,520
Xin lỗi anh bạn.
Tôi phải xử anh rồi.

151
00:16:12,040 --> 00:16:13,480
Coi chừng!

152
00:16:24,920 --> 00:16:27,520
Josef! Quỳ xuống!

153
00:16:30,600 --> 00:16:33,800
- Quỳ xuống ngay!
- Đừng bắn!

154
00:16:33,800 --> 00:16:37,080
Quỳ xuống! Giơ tay lên!

155
00:16:41,200 --> 00:16:44,160
Lý lịch của Gerald Baxter, 2005.

156
00:16:44,160 --> 00:16:48,200
Không chỉ hắn là kẻ duy nhất biến mất
mà còn cả gia đình hắn. Vợ, con hắn.

157
00:16:48,200 --> 00:16:51,440
- Không tìm thấy họ.
- Cả gia đình? Chắc chứ?

158
00:16:51,440 --> 00:16:53,240
- Không dấu vết?
- Hoàn toàn không.

158
00:16:53,440 --> 00:16:54,240
Các nhân dạng mới.

159
00:16:55,640 --> 00:16:59,640
- Tôi dự là tổ chức có nhúng tay vào.
- Nhưng nếu MI6 đưa Baxter

160
00:16:59,640 --> 00:17:02,120
quay lại chiến trường sau năm 2005

161
00:17:02,120 --> 00:17:05,240
dù biết hắn bị tâm lý bất ổn,
thì ý tôi là...

162
00:17:05,240 --> 00:17:09,160
nếu người Mỹ đưa Baxter ra tòa...

163
00:17:09,160 --> 00:17:12,000
...thì Chính phủ Anh sẽ bị liên lụy.

164
00:17:12,000 --> 00:17:16,200
Cứ xem vợ Baxter mất tích
nhưng hãy tìm cô ta.

165
00:17:16,200 --> 00:17:19,960
MI5 và 6. Ta cần phải biết Gerald Baxter
có trong biên chế của họ không?

166
00:17:29,200 --> 00:17:32,680
Đem đám tù nhân vào đây!

167
00:17:32,680 --> 00:17:35,160
Chuyển tù nhân vào!

168
00:17:36,360 --> 00:17:38,760
Tôi muốn nói chuyện với các anh!

169
00:17:38,760 --> 00:17:41,480
Chúng ta cùng phe mà!

170
00:17:45,200 --> 00:17:48,360
Chúng tôi là nạn nhân bị bắt cóc!
Chúng tôi là nhân viên cứu trợ!

171
00:17:48,360 --> 00:17:52,800
Chúng tôi cứu bọn trẻ khỏi chết đói!
Các anh không thể làm thế này!

172
00:18:11,000 --> 00:18:13,000
- Tên của anh.
- Tom Wallace.

173
00:18:17,680 --> 00:18:20,880
- Tên của anh.
- Josef Mohammed. Tôi yêu cầu luật sư!

174
00:18:20,880 --> 00:18:23,760
- Tôi yêu cầu được gọi điện ngay!
- Tôi cần thấy mặt anh.

175
00:18:23,760 --> 00:18:27,120
Bỏ tay xuống.

176
00:18:27,120 --> 00:18:29,840
Bỏ tay xuống!
Để chúng tôi thấy mặt.

177
00:18:32,560 --> 00:18:34,760
Vừa chặn được tin này của Mỹ.

178
00:18:34,760 --> 00:18:39,120
Hai người nước ngoài bị bắt sau
một cuộc tấn công bất ngờ vào một căn cứ Taliban.

179
00:18:39,120 --> 00:18:42,080
Họ đang được giữ tại
Căn cứ lính thủy Mỹ, Victor.

179
00:18:42,120 --> 00:18:42,880
Khỉ gió!

180
00:18:42,880 --> 00:18:46,800
- MI5 và 6 nói gì?
- Họ bảo chẳng có lý do gì

181
00:18:46,800 --> 00:18:50,800
để giữ Gerald Baxter trong biên chế.
- Họ nói dối. Cứ ép tiếp.

182
00:18:50,800 --> 00:18:53,440
Cần đưa Porter và Baxter ra khỏi đó.

183
00:18:53,440 --> 00:18:57,240
- Có lẽ anh cần phải gọi vài sếp lớn.
- Có thể.

184
00:18:57,240 --> 00:19:00,480
- Đề nghị Arlington giao họ.
- Có thể.

185
00:19:06,960 --> 00:19:10,960
Nhân viên tình báo Mỹ sẽ lo mọi chuyện,
theo tiêu chuẩn thông thường.

186
00:19:10,960 --> 00:19:14,600
Chúng tôi sẽ thông tin
cho Chính quyền Anh quốc.

187
00:19:14,600 --> 00:19:18,920
Rất tiếc, yêu cầu bị từ chối.
Trừ khi anh có thông tin quan trọng

188
00:19:18,920 --> 00:19:21,440
sẵn lòng chia sẻ với chúng tôi.

189
00:19:23,400 --> 00:19:25,800
Frank, người mà anh đang giữ

190
00:19:25,800 --> 00:19:29,640
tự xưng mình là Josef Mohammed...

191
00:19:29,640 --> 00:19:33,040
- Chúng tôi tin hắn là Gerald Baxter.
- Tốt.

192
00:19:33,040 --> 00:19:38,320
Nếu Gerald Baxter chịu trách nhiệm
cho cái chết của 22 lính thủy Mỹ

193
00:19:38,320 --> 00:19:42,640
thì theo các điều khoản của Hiệp ước,
hắn phải bị xét xử ở Mỹ.

194
00:19:45,560 --> 00:19:48,120
Còn gì nữa?

195
00:19:48,120 --> 00:19:50,600
- Người bị bắt còn lại.
- Xin lỗi?

196
00:19:53,600 --> 00:19:57,720
- Người cùng bị bắt với Baxter là...
- Hắn bị bắt một mình.

197
00:19:59,600 --> 00:20:04,400
Frank, theo thông tin từ Lính thủy Mỹ,
đã bắt được hai người.

198
00:20:04,400 --> 00:20:08,200
- Anh ta là điệp viên không chính thức của chúng tôi.
- Vậy ư?

199
00:20:08,200 --> 00:20:10,520
Thêm trở ngại từ chối nhỉ?

200
00:20:11,600 --> 00:20:14,680
- Baxter bị bắt một mình.
- Đậu móa,

201
00:20:14,680 --> 00:20:17,000
tôi đã thấy mấy tấm hình đó!

202
00:20:19,560 --> 00:20:22,520
Gerald Baxter... bị bắt một mình.

203
00:20:25,760 --> 00:20:28,040
Tôi là John Porter.

204
00:20:28,040 --> 00:20:31,440
34487566, mã số 807.

205
00:20:31,440 --> 00:20:34,400
Anh có thể muốn viết lại.

206
00:20:35,600 --> 00:20:38,680
Tù nhân người Anh mà
các anh đang giữ là của tôi.

207
00:20:38,680 --> 00:20:43,160
- Người Anh nào?
- Các anh giỡn cái đéo gì nhiều thế?

208
00:20:43,160 --> 00:20:45,360
Thằng khốn ở đó.

209
00:20:46,760 --> 00:20:50,400
Tên hắn là Gerald Baxter.

210
00:20:50,400 --> 00:20:53,520
Hắn chịu trách nhiệm
cho cái chết của...

211
00:20:55,160 --> 00:20:56,440
Gì thế?

212
00:20:56,440 --> 00:20:57,720
Uh-huh.

213
00:20:58,760 --> 00:21:00,920
Được rồi.

214
00:21:05,600 --> 00:21:08,520
- Hắn được R4 chỉ định.
- R4? Cái quái gì...

215
00:21:11,720 --> 00:21:14,920
- Chào mừng đến 1984.
- Tôi là công dân Anh.

216
00:21:14,920 --> 00:21:18,160
Tôi muốn gọi cho luật sư,
tôi muốn nói chuyện với Đại sứ quán!

217
00:21:18,160 --> 00:21:20,360
Nghe chưa?
Chúng tôi có quyền!

218
00:21:20,360 --> 00:21:23,360
Chúng tôi là người của Chính phủ Anh!

219
00:21:23,360 --> 00:21:25,440
Vào!

220
00:21:25,440 --> 00:21:27,480
Mấy người là bọn quái nào thế?!

221
00:21:27,480 --> 00:21:31,280
Tôi không hiểu.
Người Mỹ có lý do để giữ Baxter

222
00:21:31,280 --> 00:21:34,800
nhưng còn Porter?
Sao họ từ chối sự tồn tại của anh ấy?

223
00:21:42,000 --> 00:21:44,520
Họ sẽ giết anh ấy sao?

224
00:22:13,720 --> 00:22:16,840
Nghe này, tôi có quan hệ với nước này.

225
00:22:16,840 --> 00:22:19,080
Tôi có quyền chi nhiều tiền.

226
00:22:19,080 --> 00:22:21,800
Giết thằng hề này rồi ra giá đi!

227
00:22:35,240 --> 00:22:36,720
Hay nhỉ.

228
00:22:41,120 --> 00:22:43,920
Nhảm nhí, thật ngạc nhiên.

229
00:22:43,920 --> 00:22:47,920
Giống như phim nhỉ.
Sao làm được thế, vụ boom, boom?

230
00:22:49,920 --> 00:22:54,600
Cám ơn.
Lần thứ hai anh cứu mạng tôi nhỉ?

231
00:22:54,600 --> 00:22:57,320
Tưởng anh sẽ nghĩ
tôi đang cứu mạng mình chứ.

232
00:22:58,360 --> 00:23:00,880
Tôi hiểu rõ ràng chuyện đang xảy ra.

233
00:23:02,600 --> 00:23:06,000
Chúng ta có tình cảm!

234
00:23:06,000 --> 00:23:09,360
Shh. Gì thế?

235
00:23:16,480 --> 00:23:19,160
Khỉ thật. Bồ câu.
Chúng đang tới tóm chúng ta.

236
00:23:19,160 --> 00:23:22,600
Đó là dấu hiệu nguy hiểm.
hãy đi khỏi đây.

237
00:23:22,600 --> 00:23:25,040
Gỡ còng cho tôi!

238
00:23:25,040 --> 00:23:27,840
- Sao người Mỹ muốn anh chết?
- Ai giở trò khỉ thế?

239
00:23:27,840 --> 00:23:30,040
Sao chúng muốn anh chết?!

240
00:23:33,200 --> 00:23:36,080
- Tôi làm cho họ.
- Gì hả?!

241
00:23:36,080 --> 00:23:38,480
Giờ thì không. Từng thôi.

242
00:23:40,360 --> 00:23:44,200
- Người Mỹ đưa tôi vào Pakistan.
- Tại sao? Anh đâu phải quân nhân.

243
00:23:44,200 --> 00:23:47,800
Vì họ thích phép màu tôi đưa vào
phần mềm thiết kế cũ,

244
00:23:47,800 --> 00:23:51,520
hệ thống định vị tên lửa, xâm nhập,
anh nói thì tôi sẽ có thể làm được.

245
00:23:51,520 --> 00:23:54,280
Rồi tôi bỏ về và bị bắt cóc,

246
00:23:54,280 --> 00:23:57,360
bị trói vào một bộ tản nhiệt
trong suốt 18 tháng.

247
00:23:57,360 --> 00:24:00,000
Giờ người Mỹ muốn tôi chết vì
tôi biết quá nhiều chuyện.

248
00:24:00,000 --> 00:24:03,240
- Những bí mật như việc họ làm ở Pakistan.
- Gì chứ?

249
00:24:06,040 --> 00:24:08,440
- Danni.
- "Tôi tìm thấy gia đình Baxter."

249
00:24:08,540 --> 00:24:09,440
Ở đâu??

250
00:24:09,440 --> 00:24:13,520
"Connecticut. Thủ tục cấp tốc
khi cấp quyền công dân Mỹ."

251
00:24:13,520 --> 00:24:15,600
Làm tốt lắm.

252
00:24:18,520 --> 00:24:20,640
CIA.

253
00:24:20,640 --> 00:24:24,800
- CIA?
- Tôi là dân thường. Hỗ trợ kỹ thuật.

254
00:24:24,800 --> 00:24:26,960
CIA.

255
00:24:26,960 --> 00:24:30,080
Anh cải sang đạo Hồi?
Đã xảy ra chuyện gì? 

256
00:24:30,080 --> 00:24:33,560
Đúng không? Đạo Hồi?
Allah schmallah.

257
00:24:33,560 --> 00:24:36,040
Ramadama. Còn khuya.

258
00:24:36,040 --> 00:24:39,200
Không. Quên nó đi.
Đi khỏi chỗ quái này ngay!

259
00:24:39,200 --> 00:24:43,440
Cải thành gì chứ? Anh bị sao thế?
Anh đi theo ai?

260
00:24:44,560 --> 00:24:48,320
Tôi cải thành Zahir Sharq.
Biết là gì không?

261
00:24:48,320 --> 00:24:51,080
Zahir Sharq?

262
00:24:51,080 --> 00:24:54,960
Zahir Sharq là người của anh.
Là bố tương lai của quốc gia này.

263
00:24:54,960 --> 00:24:58,720
Mọi chương trình tái xây dựng ở đây
đều phải lại quả cho Zahir Sharq.

264
00:24:58,720 --> 00:25:01,700
- Tiền bảo kê.
- Tiền bảo kê? Thuế đấy.

265
00:25:02,200 --> 00:25:05,720
Đó là người nhìn xa trông rộng.
Ông sẽ lấy mật mã tên lửa của anh.

266
00:25:05,720 --> 00:25:08,280
Sẽ trả hậu lắm.

267
00:25:08,280 --> 00:25:10,640
Không có ý hối anh,

268
00:25:10,640 --> 00:25:14,000
nhưng người Mỹ có tai mắt
trên trời, trong núi và mặt đất.

269
00:25:14,000 --> 00:25:17,640
- Nếu không đi nhanh thì chết đấy.
- Bình tĩnh.

270
00:25:17,640 --> 00:25:20,760
Đây là giao kèo.
Đi đâu đây?

271
00:25:22,240 --> 00:25:25,360
10 dặm theo đường.
Sau đó đi bộ.

272
00:25:25,360 --> 00:25:27,840
Đi đâu?
Hang ổ của Sharq?

273
00:25:27,840 --> 00:25:31,880
Hang? Người như Zahir Sharq còn khuya
mới trốn trong hang, đồ Anh điên.

274
00:25:31,880 --> 00:25:34,520
Tên Anh điên này
vừa cứu mạng anh đấy,

275
00:25:34,520 --> 00:25:37,120
vậy sao anh không cho tôi biết
hắn đang ở đâu?

276
00:25:37,120 --> 00:25:39,440
Pakistan.

277
00:25:40,640 --> 00:25:42,480
Hay thật!

278
00:25:42,480 --> 00:25:45,800
Nhưng phải làm theo cách của tôi.
Đừng giở trò khỉ, hiểu chưa?

279
00:25:45,800 --> 00:25:48,360
Đừng khích tôi.

280
00:25:52,160 --> 00:25:55,360
Đi đến chỗ Sharq chứ?!

281
00:26:06,400 --> 00:26:10,760
Ví thắng lợi ngoài mong muốn.

282
00:26:10,760 --> 00:26:15,040
Và không có sự can thiệp
của Ủy ban tình báo chung.

283
00:26:21,480 --> 00:26:24,120
Chúng tôi đang nghiên cứu.

284
00:26:24,120 --> 00:26:27,720
Gerald Baxter là một kỹ thuật viên
hỗ trợ kỹ thuật thông thường

285
00:26:27,720 --> 00:26:30,480
được Langley tuyển mộ.

286
00:26:30,480 --> 00:26:34,800
Gia đình được cho nhân dạng mới.
Nhập cư vào Mỹ

287
00:26:34,800 --> 00:26:37,560
còn nhanh hơn một khoa học gia
về tên lửa của Đức quốc xã.

288
00:26:37,560 --> 00:26:41,280
Tổ chức của anh biết Gerald Baxter
bị chứng tâm thần bất ổn.

289
00:26:41,280 --> 00:26:45,560
Vậy mà vẫn tuyển mộ hắn rồi
quăng hắn về lại Pakistan.

290
00:26:45,560 --> 00:26:49,200
Anh chịu trách nhiệm cho cái chết
của chính lính thủy của mình.

291
00:26:49,200 --> 00:26:53,320
22 lính và anh muốn Gerald Baxter chết

292
00:26:53,320 --> 00:26:57,360
và cả Porter. Che đậy chuyện
và đổ lỗi cho chúng tôi.

293
00:27:03,920 --> 00:27:07,880
- Anh muốn gì?
- Họ.

294
00:27:07,880 --> 00:27:10,760
Chính phủ Mỹ chịu trách nhiệm cho chuyện này.

295
00:27:10,760 --> 00:27:14,560
Tôi không quan tâm anh làm gì
nhưng đừng giở trò với chúng tôi.

296
00:27:14,560 --> 00:27:17,040
Đổi lại thì chúng tôi có gì?

297
00:27:17,040 --> 00:27:20,120
Sự thật sẽ không được công bố.

298
00:27:20,120 --> 00:27:23,280
Tôi sẽ bảo Porter và Baxter câm như hến.

299
00:27:24,440 --> 00:27:28,920
- Nếu anh chưa chôn họ ở sa mạc Afghan.
- Rất nhộn đấy.

300
00:27:33,200 --> 00:27:37,160
- Anh không có tin gì từ họ?
- Không.

301
00:27:40,480 --> 00:27:43,400
Anh cũng không có họ để trao đổi?

302
00:27:43,400 --> 00:27:46,160
Không.

303
00:27:48,640 --> 00:27:51,760
John Porter trời đánh.

304
00:27:51,760 --> 00:27:56,040
Được rèn luyện ở phương Bắc.
Một người thông minh.

305
00:27:57,120 --> 00:27:59,600
Anh ấy trốn khỏi anh rồi chứ?

306
00:27:59,600 --> 00:28:02,720
Frank, việc anh ấy về nhà
chỉ còn là vấn đề thời gian.

307
00:28:03,760 --> 00:28:06,440
Hãy nghĩ đến đề nghị của tôi.

308
00:28:08,800 --> 00:28:11,560
Hugh, có vài người trong chính phủ

309
00:28:11,560 --> 00:28:15,280
nghĩ bọn anh là rắc rối
hơn là một giải pháp.

310
00:28:15,280 --> 00:28:18,200
Chuyện tình Anh-Mỹ kết thúc rồi

311
00:28:18,200 --> 00:28:21,600
và có một chiến dịch "tự lo"
nằm trên bàn ở Nhà Trắng.

312
00:28:21,600 --> 00:28:26,320
Nếu tôi là anh, tôi sẽ nghĩ lại
trước khi đưa ra quyết định gì.

313
00:28:40,760 --> 00:28:44,680
- "Gọi Langley, mã bốn."
- "Vâng. Đang kết nối."

314
00:28:47,680 --> 00:28:50,200
Shh, shh.

315
00:28:52,360 --> 00:28:54,880
Cúi xuống. Nhanh.

316
00:29:02,520 --> 00:29:04,800
An toàn.

317
00:29:04,800 --> 00:29:09,440
Này, xấu trai. Đã thu xếp xong.

318
00:29:09,440 --> 00:29:13,080
- Anh được gặp Zahir Sharq.
- Tuyệt! Còn bao xa, Joe?

319
00:29:13,080 --> 00:29:17,440
- Không xa và tên tôi là Josuf.
- Tên thật của anh là gì?

320
00:29:17,440 --> 00:29:19,720
- Josuf Mohammed.
- Nhảm nhí.

321
00:29:19,720 --> 00:29:23,460
Anh không phải Ả Rập, Afghan hay Pakistani.
Anh đến từ...

321
00:29:23,520 --> 00:29:24,160
Cowdenbeath.

322
00:29:27,760 --> 00:29:30,400
MI5 và 6 đã phản hồi.

323
00:29:30,400 --> 00:29:33,640
- Họ thẳng thừng từ chối mọi điều về Gerald Baxter.
- Tốt.

324
00:29:33,640 --> 00:29:38,080
Tất cả đều do người Mỹ.
Porter và Baxter đã thoát khỏi họ.

325
00:29:38,080 --> 00:29:40,120
Sếp?

326
00:29:43,280 --> 00:29:47,640
Kia là Pakistan.
Còn kia là Afghanistan.

327
00:29:47,640 --> 00:29:51,720
- Biên giới đâu, Tommy? Chỉ biên giới đi.
- Sao thế?

328
00:29:51,720 --> 00:29:55,280
Vì chúng là đất của các bộ lạc

329
00:29:55,280 --> 00:29:58,600
từ cả ngàn năm.
Dân ở đâu chả quan tâm đếch gì

330
00:29:58,600 --> 00:30:01,440
đến lãnh thổ quốc gia
hay luật pháp quốc tế.

331
00:30:01,440 --> 00:30:04,160
Liên Xô còn bịa chuyện để xâm chiếm

332
00:30:04,160 --> 00:30:07,520
thay vì qua lại với Afghanistan.

333
00:30:07,520 --> 00:30:10,360
Tôi không đến đây để nghe lịch sử.

334
00:30:10,360 --> 00:30:12,600
Tôi chỉ muốn làm ăn, bán vài khẩu súng.

335
00:30:12,600 --> 00:30:16,640
Anh đi theo Zahir Sharq.

336
00:30:16,640 --> 00:30:20,760
vì anh nghĩ hắn có thể giải quyết
cuộc chiến loạn xà ngầu này.

337
00:30:22,200 --> 00:30:24,800
- Tôi thì không.
- Thật sao?

338
00:30:24,800 --> 00:30:26,720
Anh là ai?

339
00:30:26,720 --> 00:30:30,920
Ngài doanh nhân,
tôi sẽ cho anh biết tôi là ai.

340
00:30:32,240 --> 00:30:35,720
Thấy nó không?
Đồ của Lực lượng đặc nhiệm.

341
00:30:35,720 --> 00:30:38,760
Mua một cái nếu anh đến Afghanistan,

342
00:30:38,760 --> 00:30:41,320
vũ khí roi da hủy diệt hàng loạt.

343
00:30:42,520 --> 00:30:46,000
Nhìn đi. Con gái tôi sẽ đó.

344
00:30:46,000 --> 00:30:49,120
Lúc đó, con bé lên sáu.

345
00:30:49,120 --> 00:30:52,640
Nó nhắc tôi về lý do mình đến đây.

346
00:30:52,640 --> 00:30:55,320
Con bé đang cố giúp tôi.

347
00:30:57,680 --> 00:31:00,120
Mang kết thúc cho mọi chuyện này.

348
00:31:01,280 --> 00:31:05,200
'Gerald Baxter chịu trách nhiệm vì
sự cố ném bom vào một ngôi làng

349
00:31:05,200 --> 00:31:07,720
ở Samara.
Nhiều phụ nữ và trẻ em đã thiệt mang.'

350
00:31:07,720 --> 00:31:12,280
Chúng ta dính vào việc đó.

351
00:31:12,280 --> 00:31:14,960
- Máy không không người lái.
- Khỉ gió!

352
00:31:14,960 --> 00:31:17,200
"Đang dò tìm khu vực."

353
00:31:17,200 --> 00:31:19,320
Chạy!

354
00:31:20,360 --> 00:31:22,200
"Chuyển sang tầm nhiệt."

355
00:31:24,480 --> 00:31:29,200
Đây là chỗ cuối cùng của họ,
Căn cứ Lính thủy Victor của Mỹ.

356
00:31:29,200 --> 00:31:31,760
Chẳng khác gì mò kim đáy bể.

357
00:31:31,760 --> 00:31:33,920
Tôi muốn một đội đến đó
để đưa họ ra.

358
00:31:33,920 --> 00:31:36,440
Dùng Tình báo điện tử tìm họ.

359
00:31:36,440 --> 00:31:39,480
Ta phải tìm ra họ trước người Mỹ.
Hiểu chứ?

360
00:31:40,880 --> 00:31:42,640
"Đang quét IR."

361
00:31:42,640 --> 00:31:46,080
Vào đó nhanh!

362
00:31:49,240 --> 00:31:52,480
Đứng im, đừng cục cựa.
Nó sẽ nghĩ chúng ta là đá.

363
00:31:53,600 --> 00:31:55,400
Tôi là đá.

364
00:31:55,400 --> 00:31:57,760
"Khu November Zulu 2356 an toàn."

365
00:32:01,440 --> 00:32:02,720
Đừng động đậy!

366
00:32:08,360 --> 00:32:10,760
Xin lỗi anh bạn.

367
00:32:11,800 --> 00:32:16,560
Bọn Mỹ chó đẻ!
Chết mịa hết đi!

368
00:32:18,960 --> 00:32:21,160
Tôi sẽ không bao giờ gặp lại
con gái nhỏ của mình nữa.

369
00:32:25,000 --> 00:32:27,080
Bố!

370
00:32:30,280 --> 00:32:35,800
Tôi không đáng phải chết cho đến khi
ngừng những chuyện giết chóc này lại!

371
00:32:36,880 --> 00:32:40,000
Tôi phải khiến nó dừng lại.

372
00:32:40,000 --> 00:32:42,960
Có thể anh đòi hỏi quá nhiều ở mình.

373
00:32:47,040 --> 00:32:49,480
Có thể anh đòi hỏi quá ít ở mình.

374
00:32:49,480 --> 00:32:53,920
Zahir Sharq sẽ làm nó dừng lại.

375
00:32:53,920 --> 00:32:56,360
Ông là người như thế.

376
00:33:01,000 --> 00:33:03,440
Ông là người duy nhất có thể.

377
00:33:05,480 --> 00:33:08,360
Đi nào. Sharq đang chờ.

378
00:33:38,040 --> 00:33:40,320
Công chúa, xe đang chờ.

379
00:34:20,640 --> 00:34:24,120
Salam Alaikum.

380
00:34:34,160 --> 00:34:39,120
Chẳng giống an ninh nội địa
chút nào nhỉ?

381
00:34:39,120 --> 00:34:43,200
Nếu không có trong danh sách
thì anh sẽ không được vào.

382
00:34:47,320 --> 00:34:52,320
Đây đâu phải Afghanistan.
Cũng chẳng phải Pakistan.

383
00:34:52,320 --> 00:34:57,600
Anh phải gọi nó là...Sharqistan?

384
00:34:57,600 --> 00:35:00,400
Tài năng của anh không có giới hạn sao?

385
00:35:08,440 --> 00:35:12,320
Salam Alaikum.

386
00:35:12,320 --> 00:35:15,680
Lính đánh thuê.
Giỏi nhất đấy.

387
00:35:42,080 --> 00:35:45,520
Taliban đang thăm chúng ta.

388
00:35:45,520 --> 00:35:50,920
Zaman Qalzai, chuyên gia moi tiền.

389
00:35:50,920 --> 00:35:55,480
- Tưởng đó là việc các anh hay làm nhiều.
- Không, Sharq đã đổi luật.

390
00:35:57,400 --> 00:35:59,840
Ông đâu phải một tên Taliban cuồng tín.

391
00:35:59,840 --> 00:36:03,560
Quay về khách sạn,
chúng tôi sẽ liên lạc sau.

392
00:36:03,560 --> 00:36:08,240
- Ông là một chính trị gia.
- Khi chính trị thất bại thì ai thắng?

393
00:36:11,080 --> 00:36:15,560
Một gã có nhiều súng lớn, ngốc thật.

394
00:36:19,640 --> 00:36:23,120
Josuf.

395
00:36:27,600 --> 00:36:32,640
Trong vùng đất bộ lạc, không có gì
như vẻ ngoài của nó, anh Wallace.

396
00:36:32,640 --> 00:36:36,400
- Ông Sharq.
- Gã đó đã đòi 20,000 đô

397
00:36:36,400 --> 00:36:40,000
từ một Công ty cung cấp điện tử
của Pháp ở Lashkar Gah.

398
00:36:40,000 --> 00:36:44,280
Nhưng tôi đã có giao kèo
với Công ty đó rồi.

399
00:36:44,280 --> 00:36:47,080
Tôi hiểu. Sự bảo vệ.

400
00:36:47,080 --> 00:36:52,720
Thuế ngầm cho một chính phủ ngầm.

401
00:36:52,720 --> 00:36:56,080
- Trà?
- Không, cám ơn.

402
00:36:56,080 --> 00:37:00,440
Cần kiên định trong kinh doanh,

403
00:37:00,440 --> 00:37:02,800
luôn giữ giá ổn định.

404
00:37:02,800 --> 00:37:06,600
Nên tôi sẽ trả lại tiền mà
hắn đã lấy của Công ty đó.

405
00:37:06,600 --> 00:37:10,600
Ông xài tiền kiếm được
từ việc đánh thuế trên các vũ khí đó.

406
00:37:10,600 --> 00:37:14,000
Đó là lý do tôi đến đây.
Ông có muốn bộ mã hóa không?

407
00:37:14,000 --> 00:37:18,600
- Có chứ.
- Hãy thỏa thuận rồi để tôi quay về Kabul.

408
00:37:18,600 --> 00:37:23,240
- Cứ từ từ đã, anh Wallace.
- Tôi là một doanh nhân.

409
00:37:23,240 --> 00:37:26,920
- Phải.
- Một doanh nhân nào đó.

410
00:37:26,920 --> 00:37:30,240
Hắn đúng là một tên xấu xa.

411
00:37:30,240 --> 00:37:33,240
Đã cứu mạng tôi hai lần.

412
00:37:33,240 --> 00:37:38,600
Hắn là một tên mạnh khỏe, sung sức,
tháo vát, tàn nhẫn, thông minh.

413
00:37:38,600 --> 00:37:42,080
Một kẻ thức thời.

414
00:37:42,080 --> 00:37:47,760
Hắn là một lính đặc nhiệm Anh quốc.

415
00:37:47,760 --> 00:37:53,240
Rất tốt, Josuf.
Thậm chí còn giỏi hơn một tên buôn vũ khí.

416
00:37:53,240 --> 00:37:57,520
Tommy Wallace, lòi đuôi cáo rồi đấy.

417
00:38:13,640 --> 00:38:17,240
- Họ muốn tin tức.
- Họ?

418
00:38:17,240 --> 00:38:22,560
Dẹp mấy trò đấu trí đi.
Họ muốn tin tức.

419
00:38:22,560 --> 00:38:26,120
Tôi đang cố giúp anh.

420
00:38:26,120 --> 00:38:30,200
Tại sao? Anh nghĩ tôi là
bạn duy nhất của anh ở đây hả?

421
00:38:30,200 --> 00:38:34,720
Và anh có thể đã nhầm,
cũng như tôi không phải là lính.

422
00:38:34,720 --> 00:38:38,360
Không, anh là lính.

423
00:38:38,360 --> 00:38:41,600
- Tôi sao?
- Phải.

424
00:38:44,600 --> 00:38:47,880
Chứng minh đi.

425
00:38:52,240 --> 00:38:55,120
Dah, dah, dah, dah!

426
00:38:55,120 --> 00:38:58,880
Bản đồ. Tiền đô.

427
00:38:58,880 --> 00:39:05,720
Ghi chú. Tặng một phần thưởng cho
bất cứ ai đưa anh về với người của anh.

428
00:39:05,720 --> 00:39:08,240
Anh muốn chuyện này?

429
00:39:16,160 --> 00:39:19,400
Gia đình anh nhớ anh.

430
00:39:23,240 --> 00:39:26,360
Đến lúc về nhà, Gerry.

431
00:39:29,880 --> 00:39:34,400
- Gerry không còn, hiểu chưa?
- Sharq đang lợi dụng anh, Gerry.

432
00:39:34,400 --> 00:39:38,440
Đã bảo tôi không phải Gerry!

433
00:39:57,520 --> 00:40:00,960
'Tôi sẽ không bao giờ
gặp lại con gái nhỏ của mình nữa.'

434
00:40:00,960 --> 00:40:03,840
- 'Hay mẹ nó.'
- Họ nhớ anh, Gerry.

435
00:40:42,240 --> 00:40:45,680
- "Chào Sharq."
- "Chào."

436
00:40:45,680 --> 00:40:49,560
Tôi có thứ anh muốn và
anh có thứ tôi muốn.

437
00:40:49,560 --> 00:40:52,000
Tôi sẽ đưa cho anh hai người,

438
00:40:52,000 --> 00:40:57,280
nhưng tôi muốn quyền truy cập
thông tin và phần cứng.

439
00:40:57,280 --> 00:41:00,680
Được, tôi sẽ báo cho người của tôi.

440
00:41:00,680 --> 00:41:04,480
- Tôi chắc họ sẽ quan tâm đến thỏa thuận.
- Tốt.

441
00:41:24,640 --> 00:41:36,120
<font color=#00FFFF>davidseanghia</font>
<font color=#FFFF00>.: www.phudeviet.org :.</font>