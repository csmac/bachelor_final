1
00:00:07,257 --> 00:00:09,459
(Episode 8)

2
00:00:13,196 --> 00:00:15,632
How dare she talk about your mother?

3
00:00:15,632 --> 00:00:17,667
Who does she think she is?

4
00:00:17,968 --> 00:00:19,703
Don't worry, Jae Yi.

5
00:00:19,703 --> 00:00:22,472
We'll just deny everything if something goes wrong.

6
00:00:22,572 --> 00:00:25,108
I'll go find the reporter and sue her.

7
00:00:25,442 --> 00:00:27,511
I'll find a solution no matter what.

8
00:00:27,711 --> 00:00:29,980
Reputation is very important for an actress.

9
00:00:31,281 --> 00:00:34,151
You went through a lot to hide your identity.

10
00:00:34,751 --> 00:00:36,253
It's because of that jerk.

11
00:00:37,120 --> 00:00:38,588
You get hurt again.

12
00:00:43,560 --> 00:00:45,262
(Is Han Jae Yi Jee Hye Won's daughter?)

13
00:00:47,531 --> 00:00:48,532
(She's the only one to survive.)

14
00:00:48,532 --> 00:00:49,633
(Is it really true?)

15
00:00:52,869 --> 00:00:54,838
(Han Jae Yi is found...)

16
00:00:56,273 --> 00:00:58,108
(Han Jae Yi is found to be Jee Hye Won's daughter.)

17
00:01:02,012 --> 00:01:04,581
It's what the inmates wait for the most.

18
00:01:04,581 --> 00:01:05,982
Time to exercise.

19
00:01:07,250 --> 00:01:11,121
It's the most free time to the inmates.

20
00:01:12,422 --> 00:01:14,791
In case of emergency,

21
00:01:14,791 --> 00:01:17,060
guards are placed everywhere.

22
00:01:20,430 --> 00:01:22,332
After the exercise,

23
00:01:22,332 --> 00:01:24,367
we count the number of inmates.

24
00:01:24,568 --> 00:01:25,769
The counting...

25
00:01:25,769 --> 00:01:28,472
is similarly conducted as in the military.

26
00:01:28,972 --> 00:01:31,308
- Count. - 1, 2, 3, 4.

27
00:01:31,308 --> 00:01:32,742
5, 6 present.

28
00:01:32,742 --> 00:01:33,810
Count.

29
00:01:33,810 --> 00:01:35,512
- 1, 2. - 3, 4.

30
00:01:35,512 --> 00:01:37,180
5, 6 present.

31
00:01:37,180 --> 00:01:39,549
- Count. - 1, 2, 3, 4.

32
00:01:39,549 --> 00:01:41,151
5, 6 present.

33
00:01:41,451 --> 00:01:44,020
Most inmates are put into work.

34
00:01:45,922 --> 00:01:47,791
Only death row inmates...

35
00:01:48,191 --> 00:01:50,160
have a choice to participate or not.

36
00:02:00,170 --> 00:02:01,872
Goodness.

37
00:02:03,840 --> 00:02:05,142
Do it gently.

38
00:02:05,142 --> 00:02:07,377
Clean this up.

39
00:02:10,280 --> 00:02:11,748
Do it properly.

40
00:02:13,850 --> 00:02:15,152
Let's move on.

41
00:02:19,322 --> 00:02:20,357
Do it gently.

42
00:02:33,770 --> 00:02:35,672
Number 2837, where are you going?

43
00:02:37,741 --> 00:02:38,942
Come back to your place.

44
00:02:40,577 --> 00:02:42,312
I heard you.

45
00:02:43,880 --> 00:02:46,850
Do they think we're monkeys in a cage? Darn it.

46
00:02:53,690 --> 00:02:54,991
(I never sold my daughter's death.)

47
00:02:56,193 --> 00:02:57,761
Don't buy it.

48
00:02:57,761 --> 00:03:00,197
Don't ever buy it.

49
00:03:05,001 --> 00:03:06,269
(Yoon Hui Jae's family agrees to share profits...)

50
00:03:07,337 --> 00:03:09,372
(Yoon Hui Jae's family agrees to share profits from the book.)

51
00:03:10,407 --> 00:03:12,042
Welcome.

52
00:03:13,143 --> 00:03:14,511
What's good here?

53
00:03:14,511 --> 00:03:15,879
- "What's good here?" - Yes.

54
00:03:15,879 --> 00:03:17,981
Is it your first time here?

55
00:03:17,981 --> 00:03:19,149
Yes.

56
00:03:19,149 --> 00:03:21,651
Everything is good here.

57
00:03:21,918 --> 00:03:24,921
Today around 3pm, Kim Sang Chul, the representative...

58
00:03:24,921 --> 00:03:27,591
for the victims' families of serial killer Yoon Hui Jae,

59
00:03:27,591 --> 00:03:30,327
- What are you doing? Hurry up. - applied for an injunction...

60
00:03:30,327 --> 00:03:33,196
- Are you distracted again? - to prohibit sales.

61
00:03:33,697 --> 00:03:36,032
- Hey. - Regarding his autobiography,

62
00:03:36,032 --> 00:03:37,667
Reporter Park...

63
00:03:37,667 --> 00:03:39,936
acts as a proxy for Yoon's rights for sales.

64
00:03:39,936 --> 00:03:43,506
And she claims that details of his criminal acts...

65
00:03:43,506 --> 00:03:45,642
and his victims were already announced in public.

66
00:03:45,642 --> 00:03:48,078
- What kind of reporter is that? - He denied the victims' opinions.

67
00:03:48,311 --> 00:03:50,180
- She must be insane. - Also Reporter Park...

68
00:03:50,180 --> 00:03:53,083
still intends to give copies of his autobiography...

69
00:03:53,083 --> 00:03:55,151
to his family and the victims' families.

70
00:03:55,151 --> 00:03:57,220
She hasn't changed her plan.

71
00:03:57,320 --> 00:03:58,688
Many people...

72
00:03:58,688 --> 00:04:01,691
are still interested by Yoon's autobiography.

73
00:04:01,691 --> 00:04:03,927
As he applied for an injunction to prohibit sales,

74
00:04:03,927 --> 00:04:06,463
- we expect a conflict between... - Hello.

75
00:04:06,463 --> 00:04:07,831
the publishing company and the victims' families.

76
00:04:08,231 --> 00:04:09,933
This is Han Ji Yoo from MBS News.

77
00:04:10,767 --> 00:04:13,336
What's wrong? Who called?

78
00:04:14,571 --> 00:04:17,807
It's nothing. Someone called the wrong number.

79
00:04:24,547 --> 00:04:27,751
- Hello? - Who do you think you are?

80
00:04:27,851 --> 00:04:30,287
- How dare she hang up on me? - Just hang up.

81
00:04:30,520 --> 00:04:31,821
Who is this?

82
00:04:31,821 --> 00:04:33,556
How dare you curse at us?

83
00:04:33,556 --> 00:04:35,158
Are you Yoon Hui Jae's wife?

84
00:04:35,292 --> 00:04:37,460
Do you make profit from that autobiography?

85
00:04:37,460 --> 00:04:39,529
You piece of trash.

86
00:04:39,763 --> 00:04:41,698
Is it nice to earn money as his family?

87
00:04:41,898 --> 00:04:43,033
Are you happy...

88
00:04:43,033 --> 00:04:45,402
that you make money off of the victims?

89
00:04:45,869 --> 00:04:47,037
Chugae Island?

90
00:04:47,470 --> 00:04:50,540
Did you think no one would find you hiding on that island?

91
00:04:50,807 --> 00:04:53,810
I'll find you on behalf of the victims' families.

92
00:04:53,810 --> 00:04:55,378
I'll burn your house down.

93
00:04:55,378 --> 00:04:58,081
- Fine, go ahead. - What?

94
00:04:58,081 --> 00:05:00,850
Neither my son nor I...

95
00:05:00,850 --> 00:05:03,820
earned a cent from that book written by that crazy man.

96
00:05:03,820 --> 00:05:05,588
Go ahead and burn our house.

97
00:05:05,588 --> 00:05:07,757
What on earth?

98
00:05:07,757 --> 00:05:09,392
Stop cursing.

99
00:05:09,392 --> 00:05:11,428
Who are you to curse at me?

100
00:05:11,428 --> 00:05:13,363
Or curse at my children?

101
00:05:13,530 --> 00:05:16,599
My children did nothing wrong, you punk!

102
00:05:21,438 --> 00:05:22,539
Mom.

103
00:05:23,073 --> 00:05:24,541
Are you okay?

104
00:05:25,208 --> 00:05:26,309
So Jin.

105
00:05:27,377 --> 00:05:28,411
Your brother.

106
00:05:29,412 --> 00:05:31,481
Call Do Jin right now.

107
00:05:32,449 --> 00:05:35,652
If anyone visits him before his graduation,

108
00:05:36,786 --> 00:05:38,421
he shouldn't see them.

109
00:05:39,489 --> 00:05:43,093
Tell him not to pick up calls from unknown numbers.

110
00:05:43,860 --> 00:05:44,928
Okay?

111
00:05:45,862 --> 00:05:47,931
- Do it now. - Okay.

112
00:05:56,306 --> 00:05:58,541
Hey, did something happen?

113
00:05:58,641 --> 00:06:00,477
You look pale.

114
00:06:00,477 --> 00:06:02,879
Of course not.

115
00:06:03,446 --> 00:06:05,081
It's nothing.

116
00:06:05,882 --> 00:06:08,218
Why would I look pale?

117
00:06:08,952 --> 00:06:10,720
I'm completely fine.

118
00:06:13,423 --> 00:06:14,391
No...

119
00:06:15,759 --> 00:06:17,360
He's not picking up.

120
00:06:17,560 --> 00:06:19,062
What do we do?

121
00:06:26,703 --> 00:06:27,771
Today around 3pm,

122
00:06:27,771 --> 00:06:30,073
Kim Sang Chul, the representative for the victims' families...

123
00:06:30,073 --> 00:06:32,942
- of serial killer Yoon Hui Jae... - He wrote an autobiography?

124
00:06:32,942 --> 00:06:34,110
applied for an injunction...

125
00:06:34,110 --> 00:06:35,612
to prohibit the sales...

126
00:06:35,612 --> 00:06:39,082
of Yoon Hui Jae's autobiography from being sold.

127
00:06:39,082 --> 00:06:41,050
This autobiography outlines...

128
00:06:41,050 --> 00:06:43,219
the details of his crimes against the victims,

129
00:06:43,219 --> 00:06:46,589
so there was a lot of controversy even before its publication.

130
00:06:46,589 --> 00:06:49,626
The victim families found...

131
00:06:49,626 --> 00:06:51,661
the autobiography to be misusing...

132
00:06:51,661 --> 00:06:53,997
the deaths of the victims,

133
00:06:53,997 --> 00:06:56,900
and have performed a protest against the sale of the book.

134
00:06:57,100 --> 00:06:59,402
Until the court decides on the verdict...

135
00:06:59,402 --> 00:07:01,237
of the injunction,

136
00:07:01,237 --> 00:07:06,042
the victim families say they will continue their protest.

137
00:07:06,409 --> 00:07:09,846
The details of his crimes were distorted in favor of him...

138
00:07:19,088 --> 00:07:21,057
Yes, sir. I can't get through...

139
00:07:23,660 --> 00:07:24,928
What about Nak Won?

140
00:07:27,030 --> 00:07:28,932
I'll be there right away.

141
00:07:59,329 --> 00:08:00,396
Do Jin.

142
00:08:00,396 --> 00:08:02,398
I thought you weren't coming.

143
00:08:09,072 --> 00:08:10,173
You're early.

144
00:08:11,908 --> 00:08:12,942
Let's go inside.

145
00:08:16,346 --> 00:08:17,447
Wait.

146
00:08:28,391 --> 00:08:29,692
Actually,

147
00:08:30,260 --> 00:08:31,961
I'm not feeling well today.

148
00:08:31,961 --> 00:08:33,796
Could we take today off?

149
00:08:34,998 --> 00:08:37,100
I'm sorry. You made your way here.

150
00:08:38,902 --> 00:08:39,969
Sure.

151
00:08:43,506 --> 00:08:44,507
I'm sorry.

152
00:10:24,807 --> 00:10:26,142
Don't die.

153
00:10:30,980 --> 00:10:32,382
Stay alive.

154
00:10:34,150 --> 00:10:35,518
You must stay alive...

155
00:10:36,319 --> 00:10:38,021
no matter what.

156
00:10:44,761 --> 00:10:46,596
It's not your fault.

157
00:10:47,997 --> 00:10:51,567
So please don't die. Never.

158
00:10:58,141 --> 00:10:59,509
If you die,

159
00:11:01,411 --> 00:11:04,080
I won't ever forgive you.

160
00:11:05,882 --> 00:11:07,150
So...

161
00:11:08,418 --> 00:11:09,552
promise me.

162
00:11:11,821 --> 00:11:12,889
Promise me.

163
00:11:16,192 --> 00:11:17,260
Promise me...

164
00:11:18,161 --> 00:11:19,729
that you will stay alive.

165
00:11:25,068 --> 00:11:27,470
Tell me you will endure it...

166
00:11:30,406 --> 00:11:32,041
just like your name.

167
00:11:39,749 --> 00:11:41,217
Tell me...

168
00:11:44,520 --> 00:11:46,723
that you will keep this promise.

169
00:11:50,026 --> 00:11:51,527
Answer me, Na Moo.

170
00:13:31,961 --> 00:13:33,529
The air in here...

171
00:13:34,197 --> 00:13:35,798
feels different today.

172
00:13:37,967 --> 00:13:40,770
Did we have a visitor?

173
00:13:41,470 --> 00:13:44,607
There were a few visitors earlier.

174
00:13:47,210 --> 00:13:49,111
I haven't watched a movie like this in a while.

175
00:13:49,645 --> 00:13:52,982
I miss my family more than ever today.

176
00:13:53,549 --> 00:13:55,418
I'm not sure if I can say it's fortunate,

177
00:13:55,418 --> 00:13:58,221
but at least your oldest son is here with you.

178
00:14:00,489 --> 00:14:03,526
That punk is just like me.

179
00:14:03,826 --> 00:14:05,761
He gets hot easily, so he doesn't wear a jacket...

180
00:14:06,429 --> 00:14:08,030
even in the winter.

181
00:14:08,631 --> 00:14:10,233
However, my wife...

182
00:14:11,000 --> 00:14:14,737
has cold hands and feet. It's because she works a lot.

183
00:14:15,271 --> 00:14:16,639
My daughter...

184
00:14:17,240 --> 00:14:20,276
won't have breakfast if there isn't a hot soup on the table.

185
00:14:21,177 --> 00:14:22,311
Also,

186
00:14:24,680 --> 00:14:26,282
my youngest...

187
00:14:32,622 --> 00:14:33,990
My youngest...

188
00:14:37,360 --> 00:14:39,262
My son Na Moo...

189
00:14:42,098 --> 00:14:43,332
wouldn't go to bed...

190
00:14:44,000 --> 00:14:45,501
if I wasn't around.

191
00:14:46,502 --> 00:14:48,871
He was worried about me whenever I came home late.

192
00:14:50,806 --> 00:14:53,509
He would stay up all night studying while waiting for me.

193
00:14:55,211 --> 00:14:57,046
Once he heard the gates,

194
00:14:58,281 --> 00:15:00,349
he would sneak over quietly...

195
00:15:00,950 --> 00:15:03,119
without waking up the family.

196
00:15:04,487 --> 00:15:06,956
He would tip toe to the door...

197
00:15:07,757 --> 00:15:09,725
while rubbing his eyes...

198
00:15:10,226 --> 00:15:11,827
to welcome me home.

199
00:15:13,496 --> 00:15:14,797
"Father."

200
00:15:15,631 --> 00:15:17,133
"You're late."

201
00:15:18,301 --> 00:15:19,502
That's what he would say.

202
00:15:22,538 --> 00:15:25,007
I miss that sweet face.

203
00:15:26,776 --> 00:15:29,111
Just because I wanted to see that face,

204
00:15:30,613 --> 00:15:33,449
I went home late on purpose at times.

205
00:15:36,152 --> 00:15:37,720
What do you think?

206
00:15:39,288 --> 00:15:40,389
My youngest kid...

207
00:15:41,490 --> 00:15:42,992
is a good son, isn't he?

208
00:15:51,600 --> 00:15:53,102
Please stop.

209
00:15:54,337 --> 00:15:55,972
Stop it!

210
00:16:05,781 --> 00:16:09,452
I wonder if he eats regularly.

211
00:16:10,419 --> 00:16:12,221
When he gets fixated,

212
00:16:12,621 --> 00:16:15,091
he forgets to eat, you see.

213
00:16:18,127 --> 00:16:21,630
You should quit smoking. It's bad for you for health.

214
00:16:22,031 --> 00:16:24,266
You should live for a long time.

215
00:16:31,440 --> 00:16:33,109
Isn't life...

216
00:16:35,111 --> 00:16:37,079
just great?

217
00:16:41,951 --> 00:16:45,121
Actress Jee Hye Won who was killed...

218
00:16:45,121 --> 00:16:47,990
in 2006 by serial killer Yoon Hui Jae...

219
00:16:48,090 --> 00:16:51,660
has been one of the most-searched names in the past few hours.

220
00:16:52,461 --> 00:16:55,231
That's because Yoon Hui Jae revealed that...

221
00:16:55,231 --> 00:16:57,400
Jee's daughter followed in her mother's footsteps...

222
00:16:57,400 --> 00:16:59,301
to become an actress, in an interview...

223
00:16:59,301 --> 00:17:01,670
with a weekly current affairs magazine.

224
00:17:01,871 --> 00:17:04,740
People are trying to find out who she is...

225
00:17:04,740 --> 00:17:07,376
- by looking online. - It's too cold for March.

226
00:17:07,376 --> 00:17:09,945
Where the tow truck?

227
00:17:12,081 --> 00:17:14,950
I'm getting really sick of you lot.

228
00:17:15,017 --> 00:17:17,319
- Will you just go to bed? - Sir.

229
00:17:17,720 --> 00:17:20,523
I'll do it. You get some rest.

230
00:17:20,523 --> 00:17:22,992
No, you have to film tomorrow.

231
00:17:22,992 --> 00:17:24,660
I can't let an actress do this.

232
00:17:24,660 --> 00:17:27,430
I'll deal with tomorrow when tomorrow comes.

233
00:17:27,430 --> 00:17:28,631
Do as I say.

234
00:17:28,631 --> 00:17:30,232
I can't do that.

235
00:17:30,232 --> 00:17:33,102
You only just left the hospital.

236
00:17:33,102 --> 00:17:35,571
I'm an expert at this so get in the car.

237
00:17:35,571 --> 00:17:37,706
I can't let you do this.

238
00:17:38,007 --> 00:17:40,576
Your hands are freezing cold.

239
00:17:41,410 --> 00:17:44,113
It's March and look at you.

240
00:17:44,113 --> 00:17:45,881
I said I'm okay.

241
00:17:45,881 --> 00:17:47,116
No, no.

242
00:17:47,249 --> 00:17:48,751
Use the blanket in there.

243
00:18:01,897 --> 00:18:03,199
Are you okay?

244
00:18:04,467 --> 00:18:06,001
Do you need help?

245
00:18:06,001 --> 00:18:07,970
No, it's okay.

246
00:18:08,137 --> 00:18:09,572
Our car's just old.

247
00:18:09,572 --> 00:18:11,340
The tow truck is on its way.

248
00:18:11,340 --> 00:18:13,709
It's quite cold. Will you be okay?

249
00:18:14,043 --> 00:18:16,278
Yes, I'm well wrapped up.

250
00:18:16,545 --> 00:18:17,646
Thank you.

251
00:18:17,646 --> 00:18:19,348
Take care then.

252
00:18:19,348 --> 00:18:21,650
- We will. Thank you. - Let's go.

253
00:18:28,057 --> 00:18:29,191
Good girl.

254
00:18:56,418 --> 00:18:57,419
Nak Won.

255
00:18:58,120 --> 00:18:59,188
Hey.

256
00:18:59,722 --> 00:19:00,789
Moo Won.

257
00:19:01,991 --> 00:19:03,559
How come you're here?

258
00:19:03,826 --> 00:19:05,161
What about training?

259
00:19:05,261 --> 00:19:06,395
Are you okay?

260
00:19:06,862 --> 00:19:09,532
You bet. Whose sister am I?

261
00:19:14,336 --> 00:19:16,138
(Reporter Kim Sang Ho)

262
00:19:28,250 --> 00:19:29,618
I put my name...

263
00:19:30,386 --> 00:19:32,021
on the injunction against the book.

264
00:19:32,188 --> 00:19:35,157
Kim Ji Young's father called me last week.

265
00:19:37,927 --> 00:19:39,261
I see.

266
00:19:39,995 --> 00:19:42,731
I'm sorry I didn't tell you sooner.

267
00:19:43,666 --> 00:19:44,700
But...

268
00:19:45,100 --> 00:19:47,002
You were worried for me.

269
00:19:48,771 --> 00:19:51,640
It's okay. You did it and that works with me.

270
00:19:52,241 --> 00:19:53,842
We knew this would happen...

271
00:19:54,577 --> 00:19:55,911
when you started acting.

272
00:19:56,712 --> 00:19:57,746
Right?

273
00:19:58,380 --> 00:19:59,481
Yes.

274
00:20:02,117 --> 00:20:04,420
I'm just annoyed that...

275
00:20:04,787 --> 00:20:07,122
he talked about Mom and me.

276
00:20:07,523 --> 00:20:08,791
That's all.

277
00:20:10,359 --> 00:20:12,561
That's really all there is to it.

278
00:20:18,200 --> 00:20:21,036
Why did you come all the way here over that?

279
00:20:21,737 --> 00:20:23,205
What if you're expelled?

280
00:20:23,205 --> 00:20:25,207
I'll have to support you then.

281
00:20:32,481 --> 00:20:33,549
To be honest,

282
00:20:34,850 --> 00:20:37,419
today was a really, really long day.

283
00:20:38,420 --> 00:20:40,055
It felt so long,

284
00:20:41,290 --> 00:20:43,325
as if I was dreaming.

285
00:20:46,428 --> 00:20:48,597
Seeing you brought me back to reality.

286
00:20:56,572 --> 00:20:58,707
I'm so glad you came over.

287
00:21:01,076 --> 00:21:03,579
I didn't have a nice day at all.

288
00:21:07,016 --> 00:21:08,050
Moo Won.

289
00:21:08,550 --> 00:21:09,618
What?

290
00:21:11,020 --> 00:21:12,688
Do you think...

291
00:21:14,757 --> 00:21:16,825
we did what we should?

292
00:21:21,497 --> 00:21:23,399
We got out alive.

293
00:21:33,575 --> 00:21:35,377
(From Yoon Hui Jae)

294
00:21:35,377 --> 00:21:37,212
(To my darling Ok Hee)

295
00:21:42,551 --> 00:21:44,787
(To my darling Ok Hee)

296
00:21:47,189 --> 00:21:48,957
We survived,

297
00:21:50,526 --> 00:21:52,428
so we have to go on...

298
00:21:53,595 --> 00:21:55,597
living each day as it comes.

299
00:22:01,637 --> 00:22:03,706
(To Na Moo. Are you okay?)

300
00:22:06,408 --> 00:22:09,411
(I miss you.)

301
00:22:15,851 --> 00:22:17,386
I miss you.

302
00:22:19,421 --> 00:22:20,723
Na Moo.

303
00:22:22,691 --> 00:22:24,927
I want to see you.

304
00:22:34,670 --> 00:22:36,171
We all...

305
00:22:37,606 --> 00:22:40,142
survived the fiery pit.

306
00:22:47,616 --> 00:22:49,318
(Forest)

307
00:23:22,451 --> 00:23:23,719
What's this?

308
00:23:25,421 --> 00:23:27,523
You're mean to the end, Yoon Na Moo.

309
00:23:30,559 --> 00:23:31,727
Do you still...

310
00:23:32,561 --> 00:23:34,129
like me that much?

311
00:23:41,837 --> 00:23:43,405
You'll feel sorry for me...

312
00:23:45,441 --> 00:23:47,209
for the rest of your life.

313
00:23:50,446 --> 00:23:52,448
You'll feel so guilty that...

314
00:23:53,882 --> 00:23:56,385
you'll be hurt whenever you see me.

315
00:23:58,687 --> 00:24:00,989
You'll suffer like crazy on your own.

316
00:24:01,890 --> 00:24:04,126
What more do you want?

317
00:24:07,463 --> 00:24:08,430
What about you?

318
00:24:13,101 --> 00:24:14,436
How about you?

319
00:24:18,941 --> 00:24:19,942
Nak Won.

320
00:24:25,781 --> 00:24:27,249
Can you...

321
00:24:29,918 --> 00:24:30,953
still...

322
00:24:33,722 --> 00:24:35,557
smile at me?

323
00:25:11,560 --> 00:25:13,629
Just to survive,

324
00:25:15,931 --> 00:25:17,566
we go on living.

325
00:25:41,390 --> 00:25:44,192
(Come and Hug Me)

326
00:25:44,192 --> 00:25:46,562
You have a visitor today. The one who visits often.

327
00:25:46,562 --> 00:25:48,630
Your father wants to see you a lot.

328
00:25:48,630 --> 00:25:49,831
Will you come with me?

329
00:25:49,831 --> 00:25:51,066
How dare you publish a book like this?

330
00:25:51,066 --> 00:25:52,801
- Who are you? - I'm his son.

331
00:25:52,801 --> 00:25:55,137
Everything in my father's autobiography is a lie.

332
00:25:55,137 --> 00:25:57,706
There's no truth in that book.

333
00:25:57,706 --> 00:25:59,641
Who is he? He fell in love with a girl.

334
00:25:59,641 --> 00:26:01,243
He destroyed our family.

335
00:26:01,243 --> 00:26:02,711
It seems like...

336
00:26:02,711 --> 00:26:05,447
there's a secret love story between you two.

337
00:26:05,447 --> 00:26:08,550
Graduated with honors? You're a police officer?

338
00:26:08,550 --> 00:26:11,486
You wanted to be with her knowing that your father is a monster.

339
00:26:11,486 --> 00:26:13,155
That's your fault.

