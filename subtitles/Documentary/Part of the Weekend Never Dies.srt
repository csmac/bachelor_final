1
00:03:25,400 --> 00:03:28,358
Explain to me what you do.

2
00:03:29,680 --> 00:03:32,752
Basically, anything we like,
under the name of Soulwax.

3
00:03:32,960 --> 00:03:34,518
Come on.

4
00:03:48,560 --> 00:03:52,155
First of all, can you tell
the audience what's the difference

5
00:03:52,360 --> 00:03:54,510
between 2ManyDJs

6
00:03:54,720 --> 00:03:56,073
Soulwax

7
00:03:56,280 --> 00:03:57,508
and Radio Soulwax?

8
00:04:14,000 --> 00:04:15,149
That's how simple it is.

9
00:04:15,360 --> 00:04:18,636
Soulwax? It's like
French Canadian or something.

10
00:04:18,840 --> 00:04:25,916
It's like a hemp store or like
a place that sells dragon sticks.

11
00:04:26,640 --> 00:04:30,633
OK, so 2ManyDJs
are Dave, my brother,

12
00:04:30,840 --> 00:04:32,956
and me, DJing

13
00:04:33,160 --> 00:04:35,833
and playing other people's music.

14
00:04:37,360 --> 00:04:39,749
Soulwax is a rock band

15
00:04:41,640 --> 00:04:44,438
which includes
my brother Dave, again,

16
00:04:45,720 --> 00:04:47,312
me

17
00:04:48,280 --> 00:04:50,236
Steve on drums

18
00:04:51,640 --> 00:04:53,710
and Stefaan, our bass player.

19
00:04:54,960 --> 00:04:55,949
We're a rock band.

20
00:05:00,200 --> 00:05:05,228
And then Nite Versions
is Soulwax remixing itself

21
00:05:05,440 --> 00:05:07,351
and playing the remixes live.

22
00:05:07,560 --> 00:05:10,313
Then we thought,
"What if we do everything together

23
00:05:10,520 --> 00:05:13,512
and we do one night
called Radio Soulwax?

24
00:05:15,800 --> 00:05:18,155
"We can do it all together."
It's confusing.

25
00:05:18,360 --> 00:05:23,559
It's almost Goa. It's like
a trance name. A bad trance name.

26
00:06:41,200 --> 00:06:44,749
You couldn't just go to Nite Versions.
It would have been lunacy.

27
00:06:46,760 --> 00:06:49,718
2ManyDJs had gone off
DJing around the world,

28
00:06:49,920 --> 00:06:52,593
but the rock band was
in a time capsule.

29
00:06:52,880 --> 00:06:56,156
Every time I see Dave or Steph,
the first thing I say,

30
00:06:56,360 --> 00:06:59,557
before 'hello', is,
<i>"Any minute now</i> is a great record."

31
00:06:59,760 --> 00:07:03,912
And then they went and decided
to ditch <i>Any minute now</i>

32
00:07:04,120 --> 00:07:08,830
and do a different version
of the album. And I was utterly...

33
00:07:13,560 --> 00:07:14,879
...disappointed.

34
00:07:15,080 --> 00:07:18,038
Oh, fuck. Fuck.

35
00:07:18,240 --> 00:07:23,553
Nite Versions, to me,
rocked as hard as the rock band did.

36
00:07:23,760 --> 00:07:28,311
Soulwax, because it's
a live band but it sounds like...

37
00:07:28,520 --> 00:07:30,317
...punk

38
00:07:30,520 --> 00:07:31,999
electronic

39
00:07:32,200 --> 00:07:36,318
possibly the best thing I've ever seen
in my whole entire life.

40
00:07:36,520 --> 00:07:39,478
- It was diabolical.
- Beyond diabolical.

41
00:07:40,000 --> 00:07:42,150
It's like cocaine
without the big ideas.

42
00:07:42,360 --> 00:07:45,796
One of them dared me to go onstage
naked, so I thought, "Fuck it."

43
00:07:46,000 --> 00:07:48,230
He thought nothing of it.

44
00:08:03,320 --> 00:08:08,997
To make the dance record
out of the gate would have felt

45
00:08:09,200 --> 00:08:13,990
like capitulating to the pressure
of the new success as a DJ duo

46
00:08:14,200 --> 00:08:17,272
in a world in which dance
and rock are no longer separate

47
00:08:17,480 --> 00:08:20,950
like they were when
the Soulwax records were made.

48
00:08:21,160 --> 00:08:29,431
And then when the massive pressure
of making the Soulwax album proper

49
00:08:29,640 --> 00:08:32,200
was released, and the album came out,

50
00:08:32,400 --> 00:08:35,676
to be like, "Want to make
some remixes? Nite versions?"

51
00:08:35,880 --> 00:08:39,998
in our studio, without much pressure;
that was how to bridge the gap.

52
00:08:42,760 --> 00:08:45,593
I remember that.
It was, "Want to DJ in August?"

53
00:08:45,800 --> 00:08:48,394
"No, the record's
gonna be done in one week."

54
00:08:48,600 --> 00:08:51,558
OK, December.
"Hey, you want to come?"

55
00:08:51,760 --> 00:08:54,194
"Can't, man, the record.
One more week."

56
00:08:54,400 --> 00:08:56,356
March. "What are you doing?"

57
00:08:56,560 --> 00:08:59,711
"One more week.
It's just the final touches."

58
00:08:59,920 --> 00:09:03,549
"We hate half the songs, it's crap.
We have to redo them."

59
00:09:03,760 --> 00:09:05,432
God, I hated watching that.

60
00:09:19,720 --> 00:09:24,430
Soulwax started because we're big fans
of Monster Magnet and Kyuss

61
00:09:24,640 --> 00:09:28,394
and all that. Masters of Reality.
That's why we started playing music.

62
00:09:28,600 --> 00:09:31,558
And we were already interested
in a lot of other things

63
00:09:31,760 --> 00:09:34,513
and it came out of it.
That's where we come from.

64
00:09:34,720 --> 00:09:36,950
We toured, against everyone's advice.

65
00:09:37,160 --> 00:09:41,073
We would do support for Muse
and all these bands for months,

66
00:09:41,280 --> 00:09:45,558
and we'd get really bored because
at 11 o' clock the show was over.

67
00:09:45,760 --> 00:09:47,796
"OK, cool, what do we do now?"

68
00:09:48,000 --> 00:09:51,788
Most times we'd go to clubs
and ask the DJs if we could take over

69
00:09:52,000 --> 00:09:57,233
because we didn't like their stuff.
That's how we started doing 2ManyDJs.

70
00:09:57,440 --> 00:10:00,671
We really started from being
on tour and being bored.

71
00:10:01,920 --> 00:10:04,753
...changed the world
with his impossible puzzles.

72
00:10:04,960 --> 00:10:09,750
2ManyDJs, live

73
00:10:09,960 --> 00:10:14,238
and, introducing the album
<i>Nite Versions</i> by Soulwax

74
00:10:14,440 --> 00:10:17,796
on November 22nd at Salon 21.

75
00:10:18,000 --> 00:10:22,516
Special guests: No Somos Machos
pero Somos Muchos.

76
00:10:24,680 --> 00:10:29,196
Soulwax and 2ManyDJs:
Nite Versions Live.

77
00:10:29,400 --> 00:10:32,153
A presentation to the rhythm of...

78
00:10:33,360 --> 00:10:39,151
Bits of the album are sounds
they gave us when we did a remix

79
00:10:39,360 --> 00:10:40,998
of <i>NYExcuse.</i>

80
00:10:41,200 --> 00:10:45,751
And we stole them. Other people steal
them from us now, which is funny.

81
00:10:45,960 --> 00:10:48,713
The drum sounds from <i>NYExcuse</i>

82
00:10:48,920 --> 00:10:54,040
which we used maybe
ten times, I think, so...

83
00:10:54,240 --> 00:10:58,358
Soulwax's beats, at the very least,
are on the Justice album.

84
00:11:07,520 --> 00:11:11,798
You're supposed to do something
you can look at, I think, as a DJ.

85
00:11:13,440 --> 00:11:15,078
It's not really exciting.

86
00:11:16,000 --> 00:11:16,955
They're just...

87
00:11:17,160 --> 00:11:17,672
Godfathers.

88
00:11:17,880 --> 00:11:19,598
I mean they're...

89
00:11:19,800 --> 00:11:20,789
Pioneers.

90
00:11:21,280 --> 00:11:22,508
Yeah, I mean it's...

91
00:11:22,600 --> 00:11:27,390
If there was a word that could combine
'pioneer' and 'godfather'...

92
00:11:28,840 --> 00:11:33,072
With 2ManyDJs, sometimes,
I think we did it so much

93
00:11:33,280 --> 00:11:35,430
that we wanted to be in a band again.

94
00:11:35,640 --> 00:11:39,428
And we started being Soulwax again.
Then we made Nite Versions

95
00:11:39,640 --> 00:11:43,758
and everything fell into place.
We can do everything!

96
00:11:43,960 --> 00:11:48,158
DJ, play with the band, invite
our friends and do the whole night.

97
00:11:48,360 --> 00:11:51,830
That is what I'm talking about.
Tiger-striped ice cream cones.

98
00:12:03,680 --> 00:12:05,910
- Are you filming?
- Yeah.

99
00:12:06,120 --> 00:12:08,680
- Can I dance?
- Yeah.

100
00:12:12,920 --> 00:12:14,672
Say something about Soulwax.

101
00:12:14,880 --> 00:12:18,953
I love you Soulwax,
you make all my dreams come true.

102
00:13:50,800 --> 00:13:54,634
Radio Soulwax, when did you
decide to start doing that?

103
00:13:54,840 --> 00:13:59,072
The blueprint for it is super-simple.
It's Nite Versions as a band live,

104
00:13:59,280 --> 00:14:03,831
and 2ManyDJs as whatever,
headliner DJ spot,

105
00:14:04,040 --> 00:14:08,318
and everything in between is filled up
with whatever band we love...

106
00:14:08,520 --> 00:14:13,150
One live band and then two other DJs
who play in-between all these things.

107
00:14:13,360 --> 00:14:16,989
And how did you find
being the promoters?

108
00:14:17,200 --> 00:14:21,352
It was hard on Dave and me, 'cause
we're there from the first band or DJ.

109
00:14:21,560 --> 00:14:25,189
You invite the people, so you try
and make the effort to be there.

110
00:14:25,400 --> 00:14:29,712
You're there for the sound check,
then you play with Nite Versions,

111
00:14:29,920 --> 00:14:34,630
then you come off, and 2ManyDJs
is always the end of the night.

112
00:14:34,840 --> 00:14:38,469
So we'd have to DJ again, and then
everybody on the tour would go crazy

113
00:14:38,680 --> 00:14:44,152
so we would be the soundtrack
for their decadent lifestyle.

114
00:14:45,920 --> 00:14:49,549
It's kind of fun but it's tiring,
'cause you would stop at 4am,

115
00:14:49,760 --> 00:14:53,958
like, "Wow, I've been here from 6.
It's been... Wow. This is..."

116
00:14:54,160 --> 00:14:56,515
Then off to the next city
and you do it again.

117
00:14:57,000 --> 00:15:03,030
O-U-L...
- W.

118
00:15:03,240 --> 00:15:04,559
A-X.

119
00:15:04,760 --> 00:15:08,594
- The drummer is amazing.
- Man, drums live...

120
00:15:08,800 --> 00:15:11,189
I was right up there.
Where were you?

121
00:15:11,400 --> 00:15:14,358
So many fucking people out there.

122
00:15:14,560 --> 00:15:19,714
Literally the best night ever.
I don't do this many drugs, but...

123
00:15:19,920 --> 00:15:21,512
And this bloke was there...

124
00:15:21,720 --> 00:15:25,395
Good career move? Probably not.
Probably lose my job on Monday.

125
00:15:25,600 --> 00:15:27,556
He was going to kiss me.

126
00:15:27,760 --> 00:15:30,479
I had to strangle him,
like, "Get the fuck off!"

127
00:15:30,680 --> 00:15:32,352
She's really nice.

128
00:15:32,560 --> 00:15:36,792
- I was terrified!
- Then he grabbed my arse, like, "Ow!"

129
00:17:37,760 --> 00:17:38,954
Couldn't stop dancing.

130
00:17:43,120 --> 00:17:45,759
I've never taken drugs
in my whole life.

131
00:17:47,920 --> 00:17:49,319
Ever.

132
00:17:52,320 --> 00:17:54,515
Who gives a fuck?

133
00:17:54,720 --> 00:17:58,554
That is so cool! That guy is so cool,
because he likes music.

134
00:18:15,200 --> 00:18:20,115
Sorry to interrupt. I'd like to say
I've been in the business

135
00:18:20,320 --> 00:18:25,440
and in electro
for more than 15 years now.

136
00:18:25,640 --> 00:18:27,312
Sorry.

137
00:18:41,000 --> 00:18:43,753
This is Steph when he DJs.

138
00:18:48,440 --> 00:18:50,396
And this is Dave.

139
00:18:52,840 --> 00:18:55,559
So it's like sine and cosine.

140
00:18:59,840 --> 00:19:02,479
I think Steph's taste...

141
00:19:02,680 --> 00:19:04,750
...drives a lot of it.

142
00:19:04,960 --> 00:19:09,158
Which ultimately...
Which I've kind of come to realise

143
00:19:09,360 --> 00:19:12,033
is pretty much a producer's role.

144
00:19:12,240 --> 00:19:13,798
You know, very much so.

145
00:19:15,320 --> 00:19:16,958
And Dave,

146
00:19:17,560 --> 00:19:20,791
Dave's ability with
the machines, and also this taste,

147
00:19:21,000 --> 00:19:24,595
is... fantastic.

148
00:19:27,640 --> 00:19:32,316
They'll be the first
to criticise each other.

149
00:19:32,520 --> 00:19:37,878
But if there's somebody else
threatening them

150
00:19:38,080 --> 00:19:40,514
or having conflict with them,

151
00:19:40,720 --> 00:19:45,396
then they'll be the first
to defend their brother.

152
00:19:46,640 --> 00:19:48,949
Neither could survive
without the other.

153
00:19:49,160 --> 00:19:53,631
He put the bottle on the table and
it popped out and went into his eye.

154
00:19:53,840 --> 00:19:57,310
If you're the boyfriend of one,
you have to deal with the others too

155
00:19:57,520 --> 00:19:59,476
so it doesn't make much difference.

156
00:19:59,680 --> 00:20:02,797
This guy's like sausage, right?

157
00:20:03,000 --> 00:20:06,356
Meat in the big grinder,
music. It's sausage.

158
00:20:06,560 --> 00:20:09,279
"Oh, cool, yeah, let's try this!
Let's mic these drums."

159
00:20:09,520 --> 00:20:10,669
We can do this...

160
00:20:11,760 --> 00:20:13,876
And left to his own devices,

161
00:20:14,080 --> 00:20:17,993
it can... I mean it's
kind of a never-ending flow.

162
00:20:18,200 --> 00:20:19,838
And Steph comes in and...

163
00:20:22,440 --> 00:20:25,432
It slices the sausage
and ties it in a knot.

164
00:20:25,640 --> 00:20:30,589
And there you have a sausage. Before
that it was just a long tube of meat.

165
00:20:30,800 --> 00:20:32,233
Goodnight, you've been great.

166
00:20:32,440 --> 00:20:36,638
- We have a little present for you.
- Oh, sorry.

167
00:20:36,840 --> 00:20:40,469
You're going to see on the monitor
what we have for you.

168
00:20:40,680 --> 00:20:44,389
They want your autograph after
the show. Let me push the button.

169
00:20:44,600 --> 00:20:47,319
OK, this is the present for you.

170
00:20:47,520 --> 00:20:51,479
A little kiss for the camera.
This way, girls. That's it.

171
00:20:51,680 --> 00:20:53,636
Do you like that kind of girls?

172
00:20:55,520 --> 00:20:58,990
- Can they cook?
- Yeah, she can cook.

173
00:20:59,200 --> 00:21:01,475
Probably peanut butter sandwich,

174
00:21:01,680 --> 00:21:05,070
but she can do something else
that's so much better...

175
00:21:05,280 --> 00:21:07,635
I'm a little bit afraid
of them, though.

176
00:21:07,840 --> 00:21:10,877
Thank you very much for being here.
It's a pleasure to have you.

177
00:21:11,080 --> 00:21:14,356
They're on live tomorrow
at Salon 21:

178
00:21:14,560 --> 00:21:17,950
Soulwax and 2ManyDJs.
You've been watching <i>Calibre 45.</i>

179
00:21:18,160 --> 00:21:20,151
See you there.

180
00:21:39,400 --> 00:21:42,836
In duos there's always
one manic one and one calm one.

181
00:21:43,040 --> 00:21:47,192
I think Dave is the calm one
and Steph is the manic one.

182
00:21:47,400 --> 00:21:50,358
They're brothers,
so they have exactly the same...

183
00:21:50,560 --> 00:21:54,633
There's one taste, and then
they just fight endlessly about it.

184
00:21:54,840 --> 00:21:56,398
That's a good T-shirt.

185
00:21:56,600 --> 00:22:00,195
It's true! It took me a while
to figure you guys out.

186
00:22:00,400 --> 00:22:02,630
On the surface
you're wildly different,

187
00:22:02,840 --> 00:22:07,550
but you're different because
you fill the gap the other one leaves.

188
00:22:07,760 --> 00:22:12,356
So if one of you's upset, the other
one's like, "What's the big deal?"

189
00:22:12,560 --> 00:22:17,429
And if one of you thinks
that something's really good,

190
00:22:17,640 --> 00:22:19,596
the other one thinks it's overrated.

191
00:22:19,800 --> 00:22:24,874
I figured that out in Ghent,
which is where it all gets magnified.

192
00:22:25,080 --> 00:22:28,834
You were really stressed out about
something with the record.

193
00:22:29,040 --> 00:22:31,634
Something was just not good enough.

194
00:22:31,840 --> 00:22:34,798
Steph was like,
"Dave's getting so upset about this.

195
00:22:35,000 --> 00:22:36,638
We need to finish it."

196
00:22:36,840 --> 00:22:43,075
There was this long thing like,
"Dude, just don't worry about it.

197
00:22:43,280 --> 00:22:45,714
"I don't care! It's fine."

198
00:22:45,920 --> 00:22:48,559
And then we were
walking from the B&B...

199
00:22:48,760 --> 00:22:52,355
...and I'm walking with Dave
and we're talking about things,

200
00:22:52,560 --> 00:22:56,633
Steph is slightly behind us,
and Dave and I are walking ahead.

201
00:22:56,840 --> 00:23:01,914
We're taking a slight turn and Steph
goes, "Why would you go this way?

202
00:23:02,120 --> 00:23:04,190
"What's wrong with you?"
Dave was like...

203
00:23:04,400 --> 00:23:10,111
"This is where I naturally went,
I didn't know there was a right way."

204
00:23:10,320 --> 00:23:12,993
"You know there's a..."
You were so mad!

205
00:23:13,200 --> 00:23:16,158
It went on for the whole walk.
And it suddenly clocked:

206
00:23:16,360 --> 00:23:19,830
you guys just flipped positions.
You walked out of the studio

207
00:23:20,040 --> 00:23:24,352
and you took over the upset guy and
you took over the unflappable guy.

208
00:23:25,800 --> 00:23:29,349
And... something else
with something else.

209
00:23:29,560 --> 00:23:33,758
It's not real taste, it's not like
you differ that much, it's just...

210
00:23:33,960 --> 00:23:36,793
Otherwise you'd
just sit there, like, "Yeah."

211
00:23:37,000 --> 00:23:41,152
If this ends up in the movie
then it's a really boring movie.

212
00:23:42,640 --> 00:23:47,998
We might be struggling for material,
Steph. I'm sorry it's come to this.

213
00:23:48,200 --> 00:23:51,033
If we are using this,
I'm sorry that it came to this.

214
00:26:48,560 --> 00:26:50,755
I don't like going on tour.

215
00:26:50,960 --> 00:26:54,157
On average we've been home
one out of six weeks.

216
00:26:54,360 --> 00:26:59,434
I think a lot of our friends have
given up trying to keep up with us.

217
00:27:48,680 --> 00:27:52,309
A really massive strength of the tour

218
00:27:52,520 --> 00:27:58,914
is that everybody knows each other
and everyone's a fan of each other.

219
00:27:59,120 --> 00:28:02,590
At least, I like to think so.

220
00:28:06,520 --> 00:28:11,150
But it wasn't a scene,
it was just sort of...

221
00:28:11,360 --> 00:28:17,708
It was just coincidental that all of
us were emerging at the same time.

222
00:28:17,920 --> 00:28:22,232
And it just so happened
we had similar tastes in music.

223
00:28:22,440 --> 00:28:25,989
And we met each other
and we all became friends.

224
00:28:26,200 --> 00:28:33,231
And I think that was because we made
effort to actually do things together.

225
00:28:33,440 --> 00:28:36,716
What's wrong with saying "scene"?
It is a scene.

226
00:28:36,920 --> 00:28:39,718
It wasn't a scene
like everyone lived in the same city,

227
00:28:39,920 --> 00:28:44,869
but we lived in this other city,
the city of seeing the same people

228
00:28:45,080 --> 00:28:47,435
no matter what or continent you're on.

229
00:28:47,640 --> 00:28:50,518
It's a bit like a family.

230
00:28:50,720 --> 00:28:55,350
Which grows and grows, and
travels and travels all over the world

231
00:28:55,560 --> 00:28:58,518
and I think Radio Soulwax
has become like a culture now.

232
00:29:44,960 --> 00:29:50,159
Honestly I don't know how it happens,
how physically a human being

233
00:29:50,360 --> 00:29:56,276
can withstand so much
touring and performing.

234
00:29:58,080 --> 00:30:01,072
Every night,
night after night, for years on end.

235
00:30:01,280 --> 00:30:02,759
It's pretty remarkable.

236
00:30:02,960 --> 00:30:09,877
To do five live shows,
and then put a different hat on

237
00:30:10,080 --> 00:30:13,629
and go out and be a DJ afterwards...

238
00:30:13,840 --> 00:30:17,515
I can't imagine
what that would be like.

239
00:30:17,720 --> 00:30:21,952
I kind of... It must have knocked
seven shades of shit out of them

240
00:30:22,160 --> 00:30:24,799
doing it every night
for how long they did it for.

241
00:30:25,000 --> 00:30:26,558
Tell us about your father.

242
00:30:26,760 --> 00:30:30,196
It was, I think,
'67 when he started DJing.

243
00:30:30,400 --> 00:30:34,154
We grew up amongst
thousands of records, which is good.

244
00:30:34,360 --> 00:30:38,035
I got to spend a lot of time
in my dad's collection.

245
00:30:38,240 --> 00:30:41,471
All the valuable things
are all stolen by me now.

246
00:30:45,680 --> 00:30:47,716
Will you say something about Stefaan?

247
00:30:47,920 --> 00:30:51,595
I don't know...
He's so quiet, Stefaan.

248
00:30:51,800 --> 00:30:59,639
He's like... In a way he's like
the third Dewaele brother.

249
00:31:00,520 --> 00:31:02,795
But the silent one.

250
00:31:03,000 --> 00:31:08,597
And he does a lot of work
in the studio with them,

251
00:31:08,800 --> 00:31:12,031
on the computer,
programming and stuff,

252
00:31:12,240 --> 00:31:13,753
but he never speaks.

253
00:31:13,960 --> 00:31:18,351
Which would be your favourite band?
Maybe the song, or...

254
00:31:18,560 --> 00:31:21,757
Just to name one band, it's torture.

255
00:31:21,960 --> 00:31:22,676
Say it.

256
00:31:22,880 --> 00:31:27,556
ELO.
No, that's a band I'm not really into.

257
00:31:27,760 --> 00:31:31,469
- You're not really into ELO?
- I like a few songs...

258
00:31:34,640 --> 00:31:37,996
- Come on, <i>Don't bring me down.</i>
- No, thank you.

259
00:31:40,960 --> 00:31:43,190
<i>Evil woman?</i>

260
00:31:59,120 --> 00:32:01,111
Good drum break?

261
00:32:03,280 --> 00:32:08,400
My friend from New York sent me
a text: "Hey, I'm in bed with Baker."

262
00:32:08,600 --> 00:32:11,353
This girl that we both like.

263
00:32:12,040 --> 00:32:12,995
Morning!

264
00:32:13,200 --> 00:32:19,116
Was it like there was an ad?
"We need a drummer."

265
00:32:25,040 --> 00:32:26,553
They were like, "Can you drink?"

266
00:32:26,760 --> 00:32:29,797
Taste that.
That's sunshine in a glass.

267
00:32:30,000 --> 00:32:31,069
"Can you do drugs?"

268
00:32:33,600 --> 00:32:36,068
- OK, good point...
- "Do you like to dance?"

269
00:32:39,120 --> 00:32:40,075
"How about girls?"

270
00:32:46,640 --> 00:32:51,156
"What would you do in a bathroom
with a female presenter from Mexico?"

271
00:35:06,320 --> 00:35:10,871
Ghent's such a weird place.
On the one hand you guys are like...

272
00:35:11,080 --> 00:35:15,870
You're in a city, so you can
go see art and designers go there,

273
00:35:16,080 --> 00:35:18,196
record stores from all over the world,

274
00:35:18,400 --> 00:35:21,517
but on the other hand,
it's like a tiny town you grew up in

275
00:35:21,720 --> 00:35:25,679
where you guys are football stars.
You're kind of half-time heroes there.

276
00:35:25,880 --> 00:35:29,190
You're kind of like, "Remember me?
I got that big pass in 1988?

277
00:35:29,400 --> 00:35:33,439
"Won the whole state championships.
That's right, baby."

278
00:35:33,640 --> 00:35:38,270
So it a weird place to be from,
for an American.

279
00:35:38,480 --> 00:35:42,029
- You want to go to a hotel?
- Yeah.

280
00:35:48,920 --> 00:35:52,754
Those kids said nothing to that guy.
He just started telling them things.

281
00:35:54,720 --> 00:35:56,995
If you're underage
and you're homeless,

282
00:35:57,200 --> 00:36:00,510
you're arrested for...
Just right there.

283
00:36:00,720 --> 00:36:03,712
Hanging on the streets,
that's against the law.

284
00:36:03,920 --> 00:36:07,037
- They call this a free country.
- What's free about it?

285
00:36:07,240 --> 00:36:11,119
Go outside your car without your ID
and see how free you are.

286
00:36:41,840 --> 00:36:44,035
Not looking...

287
00:36:53,520 --> 00:36:54,475
Check this out.

288
00:36:55,560 --> 00:36:58,632
Four people can play.

289
00:36:58,840 --> 00:37:00,193
Deluxe version.

290
00:37:07,320 --> 00:37:09,197
I mean it!

291
00:37:16,360 --> 00:37:18,874
We were into complex worlds.

292
00:37:19,080 --> 00:37:21,799
I'm just following
Soulwax around. I have to.

293
00:37:22,000 --> 00:37:26,152
I will hunt you down and kill you
when I get out of jail.

294
00:37:26,360 --> 00:37:31,354
- What's your name?
- John. Or the naked guy,

295
00:37:42,840 --> 00:37:46,196
I know you're jaded because
you've been here a thousand times,

296
00:37:46,400 --> 00:37:47,833
but I haven't.

297
00:37:50,680 --> 00:37:53,638
Now I'm coming to kick everybody out.

298
00:38:54,080 --> 00:38:56,150
C alert.

299
00:39:09,560 --> 00:39:12,313
You've got twelve
band members on there?

300
00:39:12,520 --> 00:39:15,034
Who are the rest
of the people on there?

301
00:39:15,240 --> 00:39:18,994
We're going to keep all
the passports here and look around.

302
00:39:59,560 --> 00:40:03,872
You're going to play your remix album
where you remixed all the big stars,

303
00:40:04,080 --> 00:40:07,993
big electro acts,
Kylie Minogue, Justice, Daft Punk...

304
00:40:08,200 --> 00:40:10,555
How do you choose your artists?

305
00:40:24,880 --> 00:40:29,158
No, no, I don't want to talk
about that. I'm really pissed off.

306
00:40:29,360 --> 00:40:32,079
They're not finished
with the fucking remix for us

307
00:40:32,280 --> 00:40:34,236
and they did a remix for Justice.

308
00:40:34,440 --> 00:40:38,274
Soulwax remixes,
the drums always go...

309
00:40:41,600 --> 00:40:42,749
Always.

310
00:40:49,360 --> 00:40:51,157
It's that noise...

311
00:40:53,560 --> 00:40:57,189
You know? That horrible noise

312
00:40:57,400 --> 00:41:01,359
that makes your head hurt.
But at the same time it's the...

313
00:41:01,560 --> 00:41:05,599
What's that thing for making
holes in teeth? It hurts.

314
00:41:05,800 --> 00:41:07,119
Horrible.

315
00:41:22,360 --> 00:41:24,954
I love the Justice one.
I play it every night.

316
00:41:25,600 --> 00:41:28,831
I finally figured out
that you have to play it at the end

317
00:41:29,040 --> 00:41:33,158
so that that long annoying pause,
which for me is so obnoxious

318
00:41:33,360 --> 00:41:34,998
and I would never do that...

319
00:41:59,960 --> 00:42:02,269
Just the other night
I finally nailed it.

320
00:42:02,480 --> 00:42:07,474
Every set, I was playing it
and everyone looks at you like...

321
00:42:07,680 --> 00:42:14,119
"Did you screw up?" And you're like,
"No. Just calm down, Mr Promoter,

322
00:42:14,320 --> 00:42:19,758
I got this under control,
this is Soulwax. Just cool your jets."

323
00:42:19,960 --> 00:42:23,430
But now I learnt. In Munich,
I just played it late enough

324
00:42:23,640 --> 00:42:27,189
so that when the break comes
it's believable that the night's over.

325
00:42:27,400 --> 00:42:32,394
So then it gives time for them to
chant your name, throw things at you,

326
00:42:32,600 --> 00:42:34,272
cheer, lights gone,

327
00:42:34,480 --> 00:42:38,951
and you just totally relax, man.
You just sit back and like...

328
00:42:39,160 --> 00:42:42,311
Start putting away my records,
shake a few hands...

329
00:43:20,120 --> 00:43:23,874
I think every DJ should be forced
to be in a punk band for a year.

330
00:43:26,320 --> 00:43:30,233
'Cause DJing,
next to video game tester,

331
00:43:30,440 --> 00:43:36,879
may be the most ridiculous, simplest,
most overpaid job on the planet.

332
00:43:37,080 --> 00:43:38,593
Excuse me.

333
00:43:38,800 --> 00:43:40,995
- We have an artist pass...
- Straight on.

334
00:43:41,200 --> 00:43:43,156
It's an artist pass.

335
00:43:43,360 --> 00:43:46,352
I think before 2ManyDJs existed

336
00:43:46,560 --> 00:43:52,669
I didn't really care about dance music
or DJing or that whole culture.

337
00:43:52,880 --> 00:43:58,113
And I think what they did
is kind of helped people

338
00:43:58,320 --> 00:44:02,313
who are used to playing in bands
and stuff to get into dance music

339
00:44:02,520 --> 00:44:05,876
and realise that dance music
can be interesting

340
00:44:06,080 --> 00:44:10,835
and just as exciting as pop,
prog, rock or any other genre.

341
00:44:11,040 --> 00:44:11,995
Or nu rave.

342
00:44:12,200 --> 00:44:17,558
Absolutely insane. Upside down.

343
00:44:17,760 --> 00:44:21,912
People going... Just having to scrape
people from the walls or the ceilings

344
00:44:22,120 --> 00:44:23,075
or the floors.

345
00:44:23,280 --> 00:44:26,352
- OK.
- OK, I'll do it.

346
00:44:26,560 --> 00:44:28,198
Let me get out for a sec.

347
00:44:29,960 --> 00:44:32,918
They kind of destroy it.
A lot of the time they really...

348
00:44:33,120 --> 00:44:36,430
For years I didn't want
to play after them, it was annoying.

349
00:44:36,640 --> 00:44:41,031
I didn't want to... It's true. I don't
know exactly how to build a groove

350
00:44:41,240 --> 00:44:48,555
after Blur mixed with <i>Ace of Spades</i>
with like Aretha Franklin on top.

351
00:44:48,760 --> 00:44:52,833
But yeah, they made the job
for all the boring guys harder.

352
00:44:58,200 --> 00:45:03,194
That was like a deeply bizarre...

353
00:45:05,600 --> 00:45:08,956
...like stadium rock concert DJ set.

354
00:45:09,160 --> 00:45:13,199
People went... It was like
there's 8,000 people here,

355
00:45:13,400 --> 00:45:16,119
I don't know
how you would DJ to 8,000 people.

356
00:45:16,320 --> 00:45:17,992
Who've just watched a band.

357
00:45:18,800 --> 00:45:22,031
Everybody went crazy
and I was really psyched about it

358
00:45:22,240 --> 00:45:26,392
and I was really like,
"I get this! This is fucking nuts."

359
00:48:13,560 --> 00:48:19,112
Someone took a picture of me
in the crowd, screaming and jumping,

360
00:48:19,320 --> 00:48:21,993
and I think it's really an image

361
00:48:22,200 --> 00:48:28,196
of the power Dave and Steph can throw
at the audience when they are DJing.

362
00:49:01,800 --> 00:49:07,432
I've actually seen
a lot of 2ManyDJs shows,

363
00:49:07,640 --> 00:49:10,598
like just as a fan.

364
00:49:10,800 --> 00:49:13,758
And people are just mad,
they're just like...

365
00:49:15,280 --> 00:49:17,953
It's amazing.

366
00:49:18,160 --> 00:49:21,550
- Are they on drugs?
- Totally on drugs.

367
00:49:35,960 --> 00:49:38,030
- Is it like this?
- Yes.

368
00:50:09,000 --> 00:50:10,718
You can edit all this, can't you?

369
00:50:50,320 --> 00:50:52,276
You fucker! Fuck off!

370
00:51:04,480 --> 00:51:10,350
It's always a good start when somehow,
inevitably, something goes wrong;

371
00:51:10,560 --> 00:51:17,477
the power goes out or the promoter
gets angry and turns the sound off.

372
00:51:17,680 --> 00:51:21,593
You know,
there's always some incident.

373
00:51:21,800 --> 00:51:26,078
And the music just stops
for a couple of minutes.

374
00:51:26,280 --> 00:51:31,912
And there's this pregnant pause,
there's tension.

375
00:51:32,120 --> 00:51:36,750
Are they going to go on or not?
Is this it? Are they going to finish?

376
00:51:36,960 --> 00:51:39,713
Then the music starts
and the crowd goes crazy,

377
00:51:39,920 --> 00:51:44,232
tearing out their eyeballs and their
hair, punching each other in the face,

378
00:51:44,440 --> 00:51:47,079
and, you know, it's a good trick.

379
00:52:54,760 --> 00:52:58,230
...naked. But once you're naked,
"What do I do now?"

380
00:52:58,440 --> 00:53:02,956
There was a cocktail bar with
chocolate sauce and strawberry sauce.

381
00:53:03,880 --> 00:53:05,438
Covered in this crap

382
00:53:05,640 --> 00:53:08,950
and I just jumped
into the crowd and just...

383
00:53:09,160 --> 00:53:12,789
I remember...
I feel so sorry for this girl.

384
00:53:13,000 --> 00:53:16,913
She must have been no older than 14,
and I just remember losing my balance

385
00:53:17,120 --> 00:53:21,636
and then I got up and my cock
was just banging against her face.

386
00:53:21,840 --> 00:53:24,513
And she was so traumatised.

387
00:53:24,720 --> 00:53:26,995
She was about... She was so small.

388
00:54:40,800 --> 00:54:42,756
Thank you very much.

389
00:54:42,960 --> 00:54:45,315
Love!

390
00:54:48,000 --> 00:54:51,072
Thank you for your nice act!

391
00:54:56,160 --> 00:54:59,072
2ManyDJs are cool!

392
00:55:00,560 --> 00:55:01,549
Soulwax!

393
00:55:03,800 --> 00:55:09,158
This is my night to myself.
I was like, "I want to go to the gym,

394
00:55:09,360 --> 00:55:11,874
make some dinner and just hang out."

395
00:55:12,080 --> 00:55:15,197
And he was like, "You have
to come to the show with me."

396
00:55:15,400 --> 00:55:22,556
And I said, "OK, I'll go,
if you promise and swear to Christ

397
00:55:22,760 --> 00:55:25,433
that you'll go to
The Favourite Sons on Saturday."

398
00:55:25,640 --> 00:55:30,714
I always play the same song,
over and over again.

399
00:55:30,920 --> 00:55:32,876
He's getting better and better.

400
00:55:33,080 --> 00:55:36,755
...like out of his arse,
and then calls you.

401
00:55:36,960 --> 00:55:38,916
- I heard about that.
- Awesome.

402
00:55:39,120 --> 00:55:42,715
And then he realised,
in the middle of the conversation,

403
00:55:42,920 --> 00:55:46,595
that he would be so fucked
if he tried to do it.

404
00:55:46,800 --> 00:55:50,110
And he was just like,
"Hold on five minutes."

405
00:55:50,320 --> 00:55:52,914
And then Sean called you
while he was on hold...

406
00:55:53,120 --> 00:55:54,872
Blah, blah, blah, Soulwax...

407
00:55:55,080 --> 00:55:58,038
Everybody gets it,
because don't have to get it.

408
00:55:58,240 --> 00:55:59,593
You have to fucking dance.

409
00:55:59,800 --> 00:56:01,916
We were in Mexico a few years ago

410
00:56:02,120 --> 00:56:06,557
and they were playing remixes of a hip
hop song that nobody would dare do.

411
00:56:06,760 --> 00:56:08,512
Everybody started to dance.

412
00:56:08,720 --> 00:56:10,233
Yes, but...

413
00:56:12,560 --> 00:56:14,790
We should rephrase the question.

414
00:56:15,000 --> 00:56:20,233
"Which guy in the band
have you fucked?"

415
00:56:20,440 --> 00:56:23,955
This one, this one, this one...

416
00:57:37,040 --> 00:57:39,554
And Soulwax were still touring.

417
00:57:39,760 --> 00:57:42,911
And still touring.
And they're still touring.

418
00:57:43,120 --> 00:57:45,076
It's never-ending.

419
00:58:42,000 --> 00:58:45,231
So, <i>NYExcuse...</i> I hate that song.

420
00:58:46,680 --> 00:58:50,673
I hate the sound of that song.
And every time I'm some place

421
00:58:50,880 --> 00:58:53,633
and I can hear the beginning,

422
00:58:53,840 --> 00:58:57,674
my skin starts to crawl and I can't...
I have to get away.

423
00:58:57,880 --> 00:59:02,829
But the story behind it
is very sweet and clever.

424
00:59:03,040 --> 00:59:08,034
They had a song
that was just instrumental

425
00:59:08,240 --> 00:59:13,314
and Steph had this idea
of having a female vocal on it.

426
00:59:13,520 --> 00:59:18,071
I was living in New York, and he was
living in Belgium, recording in London

427
00:59:18,280 --> 00:59:21,317
so we just didn't get
to see each other very much.

428
00:59:21,520 --> 00:59:26,878
So the song was an excuse
for him to fly to New York,

429
00:59:27,080 --> 00:59:29,913
paid for by the label,
so that we could see each other.

430
00:59:30,120 --> 00:59:34,079
And then we did the song, we recorded
at Plantain with James Murphy...

431
00:59:34,280 --> 00:59:37,317
Steph's there, I'm there,
got some time, got the studio,

432
00:59:37,520 --> 00:59:39,954
Nancy's there,
got a mic set up, checked it,

433
00:59:40,160 --> 00:59:43,755
got the compressor, pre-amp...

434
00:59:45,720 --> 00:59:46,675
Hm...

435
00:59:47,680 --> 00:59:48,635
What do we do?

436
00:59:48,840 --> 00:59:52,150
And then we came with the lyrics:

437
00:59:52,360 --> 00:59:54,476
"This excuse that we're making,

438
00:59:54,680 --> 00:59:56,796
is it good enough
for what you're paying?"

439
00:59:57,000 --> 00:59:58,956
That's where it comes from.

440
01:01:55,480 --> 01:01:57,436
You're fantastic.

441
01:01:57,640 --> 01:02:00,916
We're Soulwax, the Nite Versions.

442
01:02:01,120 --> 01:02:07,070
Thanks to everyone who played tonight
and all the other nights.

443
01:02:07,280 --> 01:02:08,235
And don't forget. You pay!!!

444
01:05:08,236 --> 01:05:09,236
Subtitles by LeapinLar

