﻿1
00:00:00,874 --> 00:00:04,246
Russia, isolated from the world for years.

2
00:00:04,885 --> 00:00:08,631
Now a free pass reveals secret places.

3
00:00:10,532 --> 00:00:13,300
In southwestern Caucasus lies removed.

4
00:00:14,509 --> 00:00:18,239
A compact region filled with a rich fauna.

5
00:00:20,180 --> 00:00:23,831
Here live animals of all colors, shapes and sizes.

6
00:00:26,781 --> 00:00:30,621
Snowy hills and dry deserts stand together,

7
00:00:30,721 --> 00:00:32,796
each with their own vieþuitoare.

8
00:00:35,569 --> 00:00:39,289
Sea border between Europe and Asia, Caucasus.

10
00:01:09,839 --> 00:01:14,028
Russia spans 11 time zones and two continents

11
00:01:14,128 --> 00:01:16,485
is a bridge between East and West.

12
00:01:16,585 --> 00:01:19,139
With 140 million inhabitants,

13
00:01:19,239 --> 00:01:21,761
Much of its territory is inhospitable or inaccessible.

14
00:01:23,400 --> 00:01:27,507
Placed between the Black Sea and the Caspian is the Caucasus.

15
00:01:28,314 --> 00:01:32,907
The size of California, has lots of climates and vieþuitoare.

16
00:01:37,073 --> 00:01:40,596
Green hills and alpine meadows,

17
00:01:40,696 --> 00:01:46,610
ºesuri small sand dune arid and make room for feral mountains.

18
00:01:50,218 --> 00:01:54,469
The landscapes here rival those of any region on Earth.

19
00:01:56,601 --> 00:02:01,847
In northern hills heights lives a species found only here.

20
00:02:02,626 --> 00:02:04,782
Caucasian chamois.

21
00:02:07,164 --> 00:02:13,321
A subspecies of those in Europe can run on terrain 50 km / h

22
00:02:13,421 --> 00:02:15,866
And can jump two meters.

23
00:02:18,746 --> 00:02:23,479
Supradimensionaþi lungs compensate for the thin air,

24
00:02:23,579 --> 00:02:27,134
and their blood is adapted to high altitudes and cold.

25
00:02:30,117 --> 00:02:35,263
But deforestation, poaching and fires threaten their existence.

26
00:02:37,866 --> 00:02:42,502
The remaining copies can retire inaccessible regions,

27
00:02:42,602 --> 00:02:47,163
at the height of 2,500 m, where few dare to venture.

28
00:02:59,681 --> 00:03:02,531
They are not the only ones who face these mountains abrupþi.

29
00:03:05,053 --> 00:03:09,665
Chamois share capped with ture.

30
00:03:11,833 --> 00:03:16,000
On the slopes, Warm males during the mating season.

31
00:04:23,951 --> 00:04:30,000
Turi males away from females living on cliffs and steep slopes.

32
00:04:32,201 --> 00:04:34,059
The females live somewhat downhill.

33
00:04:37,712 --> 00:04:41,854
Before the reunion, tighten measured forces to establish hierarchy.

34
00:04:52,284 --> 00:04:57,595
The horns grow two or three rings per year and can reach 90 cm.

35
00:05:03,905 --> 00:05:06,977
From their vegetarian food lacks essential minerals,

36
00:05:07,077 --> 00:05:11,123
So that eating ground in places where the soil gives them what they need.

37
00:05:19,330 --> 00:05:22,416
Soon they will descend in search of females.

38
00:05:26,865 --> 00:05:29,823
Discovered coasts will have to roam the hills.

39
00:05:31,295 --> 00:05:35,641
Females and kids prefer the lower slopes, wooded valleys next.

40
00:05:36,704 --> 00:05:40,151
The danger spotted mountainside rout them.

41
00:05:44,629 --> 00:05:48,084
It's a pair of brown Syrian Bears in search of food.

42
00:05:51,431 --> 00:05:57,859
They coat the open and are most easily Bears that roam the Caucasus.

43
00:06:00,110 --> 00:06:03,036
Hunting has decimated the population.

44
00:06:03,136 --> 00:06:06,460
Even at high altitudes, they are threatened by poachers.

45
00:06:08,625 --> 00:06:14,083
Caucasus Mountains belºug provides all of grassland and wood

46
00:06:14,183 --> 00:06:17,079
plus the spectacular natural wonders.

47
00:06:20,412 --> 00:06:24,820
Over 2,000 gheþari ºlefuiesc high valleys.

48
00:06:34,578 --> 00:06:41,113
Mountains massive span 1500 kilometers, from northwest to southeast.

49
00:06:41,213 --> 00:06:47,208
The highest peaks are 5,000 m, the mass of their time controlling the region.

50
00:06:51,893 --> 00:06:56,000
Clouds dial in the west, above the Black Sea, are împinºi up.

51
00:06:58,957 --> 00:07:04,304
These barrier issue over 4.5 m annual precipitation,

52
00:07:05,839 --> 00:07:08,343
seven times more than in London.

53
00:07:11,793 --> 00:07:17,305
A coast is dry and without vegetation, the other is wet and green.

54
00:07:25,109 --> 00:07:29,334
Here grow more than 6,500 species of plants.

55
00:07:29,434 --> 00:07:32,313
A quarter of them can not be found elsewhere in the world.

56
00:07:34,518 --> 00:07:37,000
Bears do not miss any.

57
00:07:38,184 --> 00:07:42,880
They spend more time in wooded areas and meadows coming out for food.

58
00:07:44,678 --> 00:07:47,406
Their food is made up more of vegetation.

59
00:07:52,599 --> 00:07:57,000
As you descend, the landscape becomes more forested.

60
00:07:57,916 --> 00:08:03,614
When preparing for hibernation, bears may eat 40 kg per day.

61
00:08:07,218 --> 00:08:10,712
The vast territory is divided for different types of Brown Bears.

62
00:08:13,511 --> 00:08:15,882
But populations are threatened.

63
00:08:20,718 --> 00:08:23,451
Hunting and cherestegii and they come here.

64
00:08:29,371 --> 00:08:33,036
Currently, the region live less than 3000 Bears.

65
00:08:45,139 --> 00:08:49,619
Humid forests of the northwest, which housed Bears and attract cherestegii,

66
00:08:49,719 --> 00:08:53,648
They are full of pine, chestnut, oak and beech trees.

67
00:09:00,830 --> 00:09:05,933
Under the canopy of greatness, in the sun, it's a battlefield.

68
00:09:11,506 --> 00:09:16,825
Mistreþii, often hanging out, tighten settle disputes by fighting.

69
00:09:18,014 --> 00:09:20,442
This time it comes to access to females.

70
00:09:23,956 --> 00:09:28,504
Build and tanks, mistreþii can reach 350 kg.

71
00:09:29,255 --> 00:09:33,000
The two pairs of corner ascuþiþi are some effective weapons.

72
00:09:37,608 --> 00:09:43,102
Battles can keep tough 20 minutes but in the end there is a winner.

73
00:09:47,276 --> 00:09:51,688
Elsewhere, a male enjoying the company of females.

74
00:09:52,822 --> 00:09:55,000
And he already earned the right to mating.

75
00:10:00,000 --> 00:10:04,014
Flocks can have over 30 boar,

76
00:10:04,114 --> 00:10:08,005
one led by the matriarch, with piglets of different ages.

77
00:10:12,689 --> 00:10:16,450
The strongest boar can pair with up to eight sows.

78
00:10:23,484 --> 00:10:28,262
In trees, a green woodpecker chicks, asking for food.

79
00:10:31,480 --> 00:10:33,713
Parents share the load tighten.

80
00:10:35,515 --> 00:10:37,799
Carefully regurgitates food.

81
00:10:46,911 --> 00:10:49,710
After being fed, the baby wants more.

82
00:10:58,983 --> 00:11:03,426
Everywhere forests and wet meadows bloom,

83
00:11:03,526 --> 00:11:06,381
Some plant species can be spotted and in the gardens of Europe.

84
00:11:12,941 --> 00:11:18,354
But these are not Maral garden ornaments. They came to graze.

85
00:11:20,394 --> 00:11:22,405
And they are in danger.

86
00:11:22,941 --> 00:11:25,558
In the Georgian mountains are protect.

87
00:11:26,684 --> 00:11:30,357
Russian across the border are easy prey for hunters.

88
00:11:35,102 --> 00:11:37,907
But not all hunters carry firearms.

89
00:11:43,932 --> 00:11:47,737
Eurasian lynx is the largest of lincºi.

90
00:11:47,837 --> 00:11:52,194
30,000 copies caroling Russia, several than anywhere else.

91
00:11:53,623 --> 00:11:56,319
It's one thing not to Maralal liniºtitor.

92
00:12:00,445 --> 00:12:03,625
But now this young mother is not in the hunt.

93
00:12:07,250 --> 00:12:11,131
Tighten lead pups six weeks in a first trip,

94
00:12:11,901 --> 00:12:14,000
Whether you like it or not.

95
00:12:18,211 --> 00:12:23,007
They have not left the den where they were born and are eager to explore.

96
00:12:35,751 --> 00:12:40,000
It's normal calving two young mother and being able to grow without problems.

97
00:12:49,206 --> 00:12:54,362
Now harm, but more than one year will be hunted desãvârºiþi.

98
00:12:55,860 --> 00:12:58,548
Their table will be the image.

99
00:13:04,012 --> 00:13:06,438
So far received only milk.

100
00:13:06,538 --> 00:13:11,565
The mother will breastfeed three to four months and will gradually receive meat and hunting.

101
00:13:16,352 --> 00:13:19,849
It grows alone, until they leave at the age of one year.

102
00:13:25,748 --> 00:13:29,081
Râºii males play no role in raising chickens.

103
00:13:38,330 --> 00:13:44,041
Tufts of fur captures any sound to their ears fine.

104
00:13:56,000 --> 00:13:58,625
Le smeared camouflage

105
00:13:58,725 --> 00:14:02,000
and 11 weeks, the cats will be difficult to detect.

106
00:14:06,453 --> 00:14:10,858
Puppies play, and the mother's careful hazards or table at the break.

107
00:14:15,669 --> 00:14:18,136
Râºii hunt at dawn and sunset,

108
00:14:18,236 --> 00:14:22,916
but the need for milk makes hunting day to eat more.

109
00:14:33,024 --> 00:14:39,082
Râºilor hearing is phenomenal. Can detect the movements of 65 m,

110
00:14:39,182 --> 00:14:42,719
their vision sharp and can spot a mouse, 75 m.

111
00:14:45,216 --> 00:14:50,161
They can easily bring down big deer, but prefer smaller prey.

112
00:14:54,526 --> 00:14:58,840
With so many eyes and ears atâþia waking attack is difficult.

113
00:15:05,435 --> 00:15:09,328
Puppies and deer are targets better if you can get close enough.

114
00:15:16,612 --> 00:15:18,025
But not today.

115
00:15:19,635 --> 00:15:24,332
No matter, the place offers enough opportunities for it and chicken

116
00:15:24,432 --> 00:15:28,119
Even though the area is gradually transforming from deforestation and agriculture.

117
00:15:39,131 --> 00:15:45,306
Caucasus has only 440,000 square kilometers, but is full of contrasts.

118
00:15:46,537 --> 00:15:51,000
Just 400 km north-east, the landscape becomes smooth.

119
00:15:51,711 --> 00:15:53,868
This is the Caspian Depression.

120
00:15:55,450 --> 00:15:59,183
Russia's minimum height is 30 m below sea level.

121
00:16:04,020 --> 00:16:09,000
In these vast meadows where only 30 cm of precipitation falls annually,

122
00:16:09,100 --> 00:16:11,462
pools of water are vital.

123
00:16:20,026 --> 00:16:24,764
In nesfârºita high grass, lives an animal puppet with a face.

124
00:16:26,898 --> 00:16:28,649
Saiga antelope.

125
00:16:32,658 --> 00:16:36,857
Oversized nose filters dust dry plains

126
00:16:36,957 --> 00:16:39,769
And heats the cold air when the temperature drops.

127
00:16:47,230 --> 00:16:52,657
Once counting million, now there are only 18,000.

128
00:16:56,237 --> 00:17:00,000
The collapse of the USSR left them unprotected in the face of poachers.

129
00:17:08,603 --> 00:17:14,672
Chinese medicine uses them today and horns to treat fever.

130
00:17:25,095 --> 00:17:27,709
Studies have shown that, if protected,

131
00:17:27,809 --> 00:17:31,573
antelope population may increase by 60% in one year.

132
00:17:37,281 --> 00:17:40,726
Aºadar, someday, huge flocks could reoccur.

133
00:17:50,691 --> 00:17:54,365
While cleans after antelope dung beetles,

134
00:17:54,465 --> 00:17:59,545
Spring steppe turns into a sea of ​​pink flowers.

135
00:18:03,447 --> 00:18:07,000
A nuns moving to a new place of hunting.

136
00:18:10,000 --> 00:18:14,444
Perfect camouflage, waiting for insects attracted to the flowers.

137
00:18:18,024 --> 00:18:21,037
Grass rãtãceºte another inhabitant of the Caucasus.

138
00:18:22,076 --> 00:18:25,412
Long-eared hedgehog the size of a hamster higher.

139
00:18:27,103 --> 00:18:29,792
He watched carefully by a ºopârlã agama.

140
00:18:32,205 --> 00:18:37,000
This piece of land is the background of the life and death struggle.

141
00:18:38,794 --> 00:18:42,629
A grasshopper gives everything he can, but does not have any chance.

142
00:18:49,093 --> 00:18:51,783
Finally, just will stay on his feet.

143
00:18:52,246 --> 00:18:57,542
His remaining leg is amputated, becoming an easy prey for someone else.

144
00:19:02,196 --> 00:19:05,529
Chitin beetle wrapper saves him,

145
00:19:05,629 --> 00:19:07,760
while ºopârla collect debris.

146
00:19:12,128 --> 00:19:16,449
Another Hedgehog pursues prey, guided by his ears fine.

147
00:19:23,287 --> 00:19:29,000
A Centipede, armed with two veninoºi corner, care must be attacked.

148
00:19:40,729 --> 00:19:46,130
Despite the onslaught of the insect, the little warrior retains lunch.

149
00:19:58,000 --> 00:20:01,837
Turning our south, land is barren and dry and more.

150
00:20:02,715 --> 00:20:05,584
There are lakes with salt, not snow

151
00:20:07,558 --> 00:20:10,192
And tall grasses that hide you.

152
00:20:15,387 --> 00:20:19,522
With only 90 cm height and weighing just 2 kg

153
00:20:19,622 --> 00:20:23,396
Crane has the lowest small size of the family.

154
00:20:26,459 --> 00:20:31,591
They mate for life and chicken to be fast to keep up.

155
00:20:34,721 --> 00:20:37,639
Meanwhile, the father înhaþã a delicious snack.

156
00:20:41,529 --> 00:20:46,000
Headed for the water source, represented by a salt lake.

157
00:20:52,954 --> 00:20:55,706
No chance for a cool dip.

158
00:20:55,806 --> 00:21:01,551
With temperatures above 40 ° C, the water evaporates, leaving behind salt.

159
00:21:04,285 --> 00:21:07,445
It's a dry, hot and inhospitable.

160
00:21:15,652 --> 00:21:19,612
Under a harsh sun, salt parents looking crustaceans.

161
00:21:26,828 --> 00:21:30,800
Let tighten hot chicks on shore, enduring heat înãbuºitoare.

162
00:21:40,492 --> 00:21:45,664
In this compact region, the landscape can change suddenly.

163
00:21:47,590 --> 00:21:49,629
Just 50 km south,

164
00:21:49,729 --> 00:21:53,935
Caspian Depression disappears and is replaced by a new landscape.

165
00:21:54,035 --> 00:21:56,372
North-Caucasian steppes.

166
00:21:57,526 --> 00:22:01,279
On the green hills, sheep and goats bring shepherds to graze.

167
00:22:03,145 --> 00:22:06,618
The strong sun plants grow even in this dry area.

168
00:22:11,125 --> 00:22:15,556
Here broaºtele þestoase out and enjoy the warmth and flowers.

169
00:22:18,286 --> 00:22:21,798
And after winter hibernation, and must regain his strength.

170
00:22:30,914 --> 00:22:35,049
In April and May males tighten resistance increase, for the mating season.

171
00:22:43,287 --> 00:22:45,520
Courtship happens in stages.

172
00:22:46,643 --> 00:22:48,705
First tracking.

173
00:22:53,892 --> 00:22:56,270
Then blow convincing.

174
00:22:59,115 --> 00:23:02,190
Usually followed by a surprisingly fast tracking.

175
00:23:10,484 --> 00:23:12,678
And some blows stronger.

176
00:23:21,040 --> 00:23:25,094
Eventually, the male courtship ends and earn their right.

177
00:23:39,294 --> 00:23:42,508
A mating can fertilize more eggs,

178
00:23:42,608 --> 00:23:45,012
which will be submitted at a time, three to fourth.

179
00:23:52,124 --> 00:23:54,617
But female seems to be impressive offering.

180
00:24:15,683 --> 00:24:19,207
La o aruncãturã de bãþ,
e o priveliºte superbã.

181
00:24:19,307 --> 00:24:21,413
A miniature desert, an oasis.

182
00:24:23,297 --> 00:24:27,776
Near mountains înzãpeziþi area of ​​30 square kilometers of dunes

183
00:24:27,876 --> 00:24:31,000
It has its own climate and desert fauna.

184
00:24:31,930 --> 00:24:34,365
Sarykum called "golden sands"

185
00:24:34,465 --> 00:24:39,655
Desert was formed by centuries of erosion neighboring mountains.

186
00:24:42,071 --> 00:24:45,170
A pair of ºopârle agama face off.

187
00:24:48,482 --> 00:24:51,632
These energetic reptiles are not afraid of fighting,

188
00:24:52,899 --> 00:24:54,515
even against ºerpilor.

189
00:24:59,652 --> 00:25:02,314
But today taken a different tack.

190
00:25:09,631 --> 00:25:12,823
Habitat strange old adãposteºte knowledge,

191
00:25:12,923 --> 00:25:14,642
As long eared hedgehog's.

192
00:25:19,000 --> 00:25:22,489
HUNT night and day in the cool hours.

193
00:25:22,589 --> 00:25:25,272
Their long ears helps to thermoregulation.

194
00:25:28,708 --> 00:25:32,062
They are more and more iuþi ºireþi than those in Europe

195
00:25:32,162 --> 00:25:35,319
And can travel up to 9 km in the road for food.

196
00:25:43,702 --> 00:25:48,045
The best defense is to stay under the ground or have a hard shell.

197
00:25:55,675 --> 00:25:58,000
Prevesteºte grass struggles danger.

198
00:26:00,000 --> 00:26:03,025
It is time for birds to keep it as best I can.

199
00:26:04,282 --> 00:26:07,075
And for those who have the opportunity to hide.

200
00:26:21,060 --> 00:26:26,568
In summer, sandstorms blowing with winds over 100 km / h.

201
00:26:34,054 --> 00:26:37,718
The flocks caught at the open do not have shelters.

202
00:26:38,692 --> 00:26:40,588
Other animals put them underground.

203
00:27:35,794 --> 00:27:39,000
The storm's arrival as suddenly as they end.

204
00:27:46,000 --> 00:27:47,796
Life resumes.

205
00:27:52,731 --> 00:27:55,711
But it's a wind that some harms.

206
00:27:58,935 --> 00:28:03,163
Together, the rocks, eagles build nests pleºuvi tighten measures.

207
00:28:04,463 --> 00:28:07,624
They can live 40 years and often mate for life.

208
00:28:09,544 --> 00:28:13,848
Females rarely leave their chicks, so that males should be without corpses.

209
00:28:15,361 --> 00:28:21,081
Adult eagles can soar seven hours. And hunt using sight, smell.

210
00:28:21,181 --> 00:28:24,292
I can eat 5 kilograms of meat at one meal.

211
00:28:25,911 --> 00:28:30,294
On the edge of the dune, a steppe eagle found a dead goat in the storm.

212
00:28:33,401 --> 00:28:36,640
Eagle is strong, but opening carcass may last.

213
00:28:47,934 --> 00:28:51,607
Starve will arrive a few days whether it will keep to herself.

214
00:28:52,813 --> 00:28:56,021
Already arriving coþofene that put patience to the test.

215
00:29:03,131 --> 00:29:07,192
But there are so courageous. Eagles gathered chase them.

216
00:29:10,590 --> 00:29:14,600
Whether he likes it or not, the first eagle feast will not single.

217
00:29:22,146 --> 00:29:24,322
Eagles glimpse shake,

218
00:29:24,422 --> 00:29:28,302
but with weak beaks, you have to wait opening carcass.

219
00:29:29,043 --> 00:29:32,599
If too much flies, eagles shall eat it all.

220
00:29:39,824 --> 00:29:44,258
With an empty stomach, male eagle glides towards his family.

221
00:29:50,203 --> 00:29:54,148
The puppy, eager for food, will stay hungry today.

222
00:29:59,484 --> 00:30:03,765
The female can not give food, but at least they can provide shade.

223
00:30:08,298 --> 00:30:13,304
If you survive, barely four months will fly like his parents.

224
00:30:18,690 --> 00:30:23,394
For thousands of years, shepherds have crossed this region, called Dagestan.

225
00:30:26,166 --> 00:30:28,284
Not only that pass by.

226
00:30:29,290 --> 00:30:32,257
A community-installed colorful and reside summer.

227
00:30:36,674 --> 00:30:39,476
Bee eaters, birds þipãtoare African

228
00:30:39,576 --> 00:30:44,000
And dug holes 1.5m nests in sandy slopes.

229
00:30:48,639 --> 00:30:53,305
They came looking pair and thickets roar of chirps.

230
00:30:57,406 --> 00:31:02,534
Social birds, make their large colonies, but males do not tolerate competition.

231
00:31:11,929 --> 00:31:15,000
I can eat more than 200 bee or wasp day.

232
00:31:15,440 --> 00:31:17,539
The first time hit to kill,

233
00:31:18,793 --> 00:31:21,348
then shake to get rid of the poison.

234
00:31:29,923 --> 00:31:34,124
Well crushing an insect's a nice gift for female.

235
00:31:51,926 --> 00:31:57,000
When they reached the gifts will lean, signaling he's ready.

236
00:32:08,760 --> 00:32:13,706
After mating can result ten eggs, submitted at the end of the nest.

237
00:32:16,770 --> 00:32:19,880
The whole community increases young, very unusual.

238
00:32:25,955 --> 00:32:29,646
The distant hills are furiºeazã a strange animal.

239
00:32:30,510 --> 00:32:34,269
ªopârla without feet long by one meter, not a snake.

240
00:32:34,659 --> 00:32:38,145
This may break tighten the tail when it is threatened.

241
00:32:38,784 --> 00:32:41,828
It makes bucãþele as glass, bedazzle enemies.

242
00:32:44,378 --> 00:32:48,000
Eyelids and ears ºopârlã reveal his identity.

243
00:32:48,322 --> 00:32:54,359
ªerpii not have. It has the features of a ºopârle, except degenerate feet.

244
00:32:57,398 --> 00:33:01,479
Another ciudãþenie is that some lay eggs, others give birth to live young.

245
00:33:05,355 --> 00:33:11,000
To the east of this varied landscape is home to the largest inland sea in the world.

246
00:33:11,431 --> 00:33:12,840
Caspian Sea.

247
00:33:14,876 --> 00:33:19,474
With an area of ​​422,000 square kilometers, it is larger than Germany.

248
00:33:22,072 --> 00:33:25,376
Along its western banks is found another surprise.

249
00:33:27,437 --> 00:33:30,171
Here, Earth clocoteºte and boil.

250
00:33:34,841 --> 00:33:38,480
Over 400 Bãlþi muddy beats on methane.

251
00:33:46,470 --> 00:33:51,000
Occur along a tectonic plate where the heat comes out of the Earth's crust.

252
00:33:55,915 --> 00:34:00,253
Mind volcanoes forms noroioºi let sunshine strengthened.

253
00:34:05,800 --> 00:34:10,789
In other parts, stufãriºul discreet hide jungle cats.

254
00:34:20,816 --> 00:34:24,581
Despite the name, jungle cats prefer wet areas.

255
00:34:24,681 --> 00:34:28,183
In Russia, it is believed that only 500 still alive.

256
00:34:31,211 --> 00:34:33,645
This caught the eye of quail.

257
00:35:17,294 --> 00:35:21,105
But one fly, and the other into the water,

258
00:35:21,205 --> 00:35:24,763
cats must seek another specialty dining.

259
00:35:28,949 --> 00:35:30,539
There are many other options.

260
00:35:31,066 --> 00:35:35,023
Millions of birds pass through the Caucasus pausing migrations.

261
00:36:12,923 --> 00:36:17,040
800 km away in the mountains of western împãduriþi,

262
00:36:17,140 --> 00:36:19,182
animals are preparing for the fall.

263
00:36:29,357 --> 00:36:31,542
Mistreþii s-au îngrãºat.

264
00:36:36,630 --> 00:36:39,477
Herd multiplied by a new generation.

265
00:36:46,457 --> 00:36:50,544
With only a few weeks already piglets energy

266
00:36:50,644 --> 00:36:53,540
that will turn into some aggressive adult boar.

267
00:37:03,924 --> 00:37:07,936
Each sow gave birth to 12 piglets dungaþi,

268
00:37:08,036 --> 00:37:12,454
but many will not catch maturity becoming prey animal feed.

269
00:37:16,015 --> 00:37:19,051
The piglets will be breast-feeding up to four months

270
00:37:19,151 --> 00:37:21,843
but will not leave until the next mating.

271
00:37:25,454 --> 00:37:28,737
Sows tighten recognize piglets after guiþ,

272
00:37:28,837 --> 00:37:33,124
But if a mother pig and finds not, it will suck from other sows.

273
00:37:39,000 --> 00:37:43,591
When piglets resting, sitting next to each other to keep warm.

274
00:37:43,691 --> 00:37:45,797
There were still quite thick fur.

275
00:37:57,477 --> 00:38:01,482
Elsewhere in the mountains, another family has different activities.

276
00:38:11,034 --> 00:38:14,885
Two days ago, my mother laughed killed a deer and fed to chickens.

277
00:38:18,318 --> 00:38:21,000
Holt has already provided some meals

278
00:38:21,100 --> 00:38:25,000
and now it is again brought out of hiding for another lunch.

279
00:38:29,583 --> 00:38:32,063
There will be complaints from starving chickens.

280
00:38:33,000 --> 00:38:37,440
In five months of age, apprentice hunters rarely receive milk.

281
00:38:40,280 --> 00:38:43,559
For several months, tighten mother watching in action,

282
00:38:43,659 --> 00:38:47,706
studying them fast attack tactics and feeding on prey.

283
00:38:56,645 --> 00:39:01,543
A mature laughter gobble 2.5kg of meat at a meal,

284
00:39:01,643 --> 00:39:04,292
So this chamois will reach a time.

285
00:39:11,060 --> 00:39:13,581
The chicks will stay with the mother until spring,

286
00:39:13,681 --> 00:39:17,199
Then, one year, will go in search of their own territories.

287
00:39:24,454 --> 00:39:29,221
When you are tired, mother covering the carcass until the next meal.

288
00:39:37,469 --> 00:39:40,912
Cover leftovers with great attention,

289
00:39:41,012 --> 00:39:44,630
Althea to prevent inhabitants of the forest to steal his book.

290
00:39:57,958 --> 00:40:02,000
At the foot of the Caucasus mountains of natural rezervaþia,

291
00:40:02,100 --> 00:40:05,681
slowly traverses a huge green slopes.

292
00:40:07,549 --> 00:40:10,228
A male bison.

293
00:40:11,740 --> 00:40:15,353
The old man weighs nearly a tonne solitary animal.

294
00:40:20,127 --> 00:40:24,882
In the past, there were many bison Caucasus Mountains Caucasian

295
00:40:24,982 --> 00:40:27,412
o subspecie a zimbrului european.

296
00:40:32,229 --> 00:40:35,684
The last wild specimen was killed in 1927.

297
00:40:40,818 --> 00:40:45,316
Zimbrii zoo were multiply and release in nature.

298
00:40:53,230 --> 00:40:57,000
This small population represents some of their descendants.

299
00:41:00,097 --> 00:41:03,935
Few have the chance to see these shy and rare animals.

300
00:41:04,035 --> 00:41:06,510
Their finding may take days to follow.

301
00:41:10,866 --> 00:41:14,264
Today, about 500 roam these hills.

302
00:41:14,364 --> 00:41:18,260
Family threatened by multiplying the population is unstable

303
00:41:18,360 --> 00:41:21,740
And despite protection, attracts poachers.

304
00:41:28,590 --> 00:41:31,505
Fortunately, some instinct of reproduction.

305
00:41:34,383 --> 00:41:37,584
Rolling in urine, tighten applying perfume.

306
00:41:39,095 --> 00:41:42,359
Once he entered the fur should be irresistible.

307
00:41:50,262 --> 00:41:52,582
Maybe I should be hung from a replica.

308
00:41:59,659 --> 00:42:03,479
But not all the Caucasus mountains are inviting places to live.

309
00:42:10,608 --> 00:42:16,193
Zimþate peaks of mountains are as hostile as the moon.

310
00:42:20,883 --> 00:42:23,595
Even plants can barely survive,

311
00:42:23,695 --> 00:42:26,463
So it's hard to believe that an animal lives here.

312
00:42:27,073 --> 00:42:28,359
And yet I do.

313
00:42:37,144 --> 00:42:39,719
With singuranþã that defies gravity

314
00:42:39,819 --> 00:42:42,791
ibex mountain goats are masters of these rocks.

315
00:42:45,554 --> 00:42:47,767
Nu has to cãzãturi.

316
00:42:52,799 --> 00:42:54,627
It's mating season.

317
00:42:55,951 --> 00:42:59,135
Males live in groups of four or five

318
00:42:59,235 --> 00:43:01,613
but now everyone is on their own.

319
00:43:06,036 --> 00:43:10,630
When induce, use their long horns in heavy fighting.

320
00:43:13,204 --> 00:43:16,079
Find herds of up to 50 females.

321
00:43:25,208 --> 00:43:30,586
If you find any, try to isolate it with extravagant movements.

322
00:43:35,262 --> 00:43:40,000
Try Hail, rub heads and feet, and raises.

323
00:43:51,749 --> 00:43:55,750
But if does not move quickly, competitors take advantage of opportunities.

324
00:44:00,294 --> 00:44:02,646
Suddenly, the race is open to all.

325
00:44:17,525 --> 00:44:21,012
Finally, a lucky male obtains its right.

326
00:44:32,269 --> 00:44:36,610
Multiplying is the only time when males and females meet.

327
00:44:37,220 --> 00:44:40,245
Silverback will mate with several females.

328
00:44:45,381 --> 00:44:48,753
This game with high stakes keep more than one month

329
00:44:48,853 --> 00:44:52,882
over some mountains with altitudes of 4,200 m.

330
00:44:55,367 --> 00:44:59,681
Five months later, the next generation will be born goats

331
00:44:59,781 --> 00:45:02,791
Cath cliffs capable from day one.

332
00:45:10,406 --> 00:45:13,915
On the highest peaks, there are no signs of life.

333
00:45:17,456 --> 00:45:22,641
Snows stretch as far as the eye can see and often have 7 m thick.

334
00:45:29,212 --> 00:45:34,554
Quiet is broken only by the sound of the wind and avalanºelor.

335
00:45:39,817 --> 00:45:45,057
In the Caucasus mountains abrupþii most avalanches occur in Russia.

336
00:45:49,915 --> 00:45:54,000
LOWER speeds of up to 250 km / h.

337
00:45:57,528 --> 00:46:02,000
It represents a major threat to all vietãþile mountains.

338
00:46:12,096 --> 00:46:15,457
Summary Russia's Caucasus region,

339
00:46:15,557 --> 00:46:18,942
stretched, unknown, wild.

340
00:46:22,045 --> 00:46:24,000
And mysterious.

341
00:46:28,402 --> 00:46:33,581
Secrets and surprises are piled in a limited space.

342
00:46:35,420 --> 00:46:38,524
Sea border between Europe and Asia,

343
00:46:39,010 --> 00:46:41,257
this is Caucasus.



