1
00:00:07,500 --> 00:00:11,800
King Leopold's Ghost

2
00:05:57,500 --> 00:06:01,320
"This wonderful African cake"

3
00:06:48,870 --> 00:06:52,000
Diary, 1875

4
00:06:54,999 --> 00:06:58,800
"Stanley shoots negroes as if they were monkeys"

5
00:06:59,100 --> 00:07:00,000
Diary, 1875

6
00:08:37,000 --> 00:08:39,450
Author "King Leopold's Ghost"

7
00:09:19,840 --> 00:09:23,000
Letter to Col. Strauch, 1881

8
00:09:43,150 --> 00:09:46,800
"Ivory", they sighed, and it sounded like a prayer.

9
00:10:23,100 --> 00:10:25,000
Letter to H.M.Stanley, 1881

10
00:11:02,000 --> 00:11:05,000
Letter to H.M.Stanley, 1881

11
00:11:41,500 --> 00:11:44,000
Former colonial official, Congo

12
00:12:31,500 --> 00:12:34,500
Letter to the US - President, 1883

13
00:13:05,600 --> 00:13:09,800
Letter from Leopold's advisor,
undated

14
00:13:37,000 --> 00:13:40,000
"The independent states of Congo"

15
00:14:10,000 --> 00:14:11,500
Senate enactment

16
00:14:11,700 --> 00:14:13,700
Philantropist King Leopold

17
00:14:14,001 --> 00:14:15,400
to protect

18
00:14:15,600 --> 00:14:17,200
Humanitarian objectives

19
00:14:25,900 --> 00:14:27,000
Benevolent venture

20
00:14:27,250 --> 00:14:28,500
Kanaan for modern Israelites

21
00:14:29,501 --> 00:14:30,501
Free state founded

22
00:14:31,550 --> 00:14:33,000
Leopold fights slave trade

23
00:14:36,400 --> 00:14:40,750
Speech to NY Chamber of Commerce, 1879

24
00:17:42,000 --> 00:17:44,000
Former policeman, Congo

25
00:18:30,000 --> 00:18:33,000
Former Senator, DRC

26
00:18:50,000 --> 00:18:53,000
"Crimes against humanity"

27
00:19:57,200 --> 00:20:01,000
Open letter to Leopold II, 1890

28
00:20:43,000 --> 00:20:47,000
Open letter to the US - President, 1890

29
00:21:12,000 --> 00:21:16,000
Open letter to the US - Secretary Of State, 1890

30
00:21:30,500 --> 00:21:34,000
"Beasts of burden with thin monkey - legs"

31
00:21:45,000 --> 00:21:48,000
Letter to the Belgian Prime Minister, 1890

32
00:21:55,000 --> 00:21:58,000
Newspaper interview, 1906

33
00:24:17,000 --> 00:24:20,000
Author "Assassination of Lumumba"

34
00:25:38,100 --> 00:25:41,500
Letter to the governor-general of Congo, 1890

35
00:26:19,500 --> 00:26:25,000
Reverend mother's letter to colonial offical, 1895

36
00:26:35,000 --> 00:26:39,500
"The most abominable prey of human history"

37
00:27:45,300 --> 00:27:48,000
Unknown missionary

38
00:28:21,500 --> 00:28:24,000
Eldest of village Luebo

39
00:29:34,500 --> 00:29:36,500
Diary, 1892

40
00:29:41,000 --> 00:29:44,501
Wire to British State Department, 1899

41
00:30:18,000 --> 00:30:21,000
Diary, 1895

42
00:32:51,500 --> 00:32:54,500
Diary, 1899

43
00:35:51,000 --> 00:35:54,500
"The horror, the horror"

44
00:36:38,600 --> 00:36:41,000
Writer

45
00:38:36,000 --> 00:38:41,300
"A secret society of murderers"

46
00:41:06,480 --> 00:41:10,000
Printer for W. Sheppard

47
00:41:56,000 --> 00:42:00,000
"Shameful, shameful, a despicable system"

48
00:42:50,001 --> 00:42:54,001
Report #32 to British State Dept., 1903

49
00:43:52,400 --> 00:43:56,000
Ilanga from village Waniendo

50
00:43:56,500 --> 00:43:59,500
Journalised by Edgar Canisius, 1899

51
00:46:31,300 --> 00:46:34,300
Germans whipping women in Cameroon

52
00:46:34,800 --> 00:46:37,100
England's wicked war in South Africa

53
00:46:37,101 --> 00:46:39,200
French aggression in Africa

54
00:48:25,500 --> 00:48:29,000
Report #32 to British State Dept., 1903

55
00:50:30,000 --> 00:50:33,000
"A cry for justice and mercy sounds from Congo"

56
00:51:39,200 --> 00:51:43,001
Never to be published for research

57
00:53:18,400 --> 00:53:21,500
Letter to advisors

58
00:55:23,700 --> 00:55:27,000
"This wonderful African cake"

59
00:56:51,700 --> 00:56:55,001
Former colonial offical, Congo

60
01:05:25,200 --> 01:05:28,002
Former minister, DRC

61
01:07:06,000 --> 01:07:09,000
"A secret society of murderers"

62
01:11:37,500 --> 01:11:40,000
We must fight,

63
01:11:40,200 --> 01:11:45,100
even make a deal with the devil,
for the unity of Congo.

64
01:11:45,500 --> 01:11:49,100
Congo is indivisible!

65
01:15:34,700 --> 01:15:36,700
Development expert

66
01:23:22,000 --> 01:23:25,000
Former vice president, DRC

