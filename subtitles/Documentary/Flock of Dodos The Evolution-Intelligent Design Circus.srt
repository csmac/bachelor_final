1
00:01:28,000 --> 00:01:31,868
<i>My God.
Brother, what have you done?</i>

2
00:01:47,618 --> 00:01:52,612
Yet again we arrive to witness
this monster's aftermath.

3
00:01:54,691 --> 00:01:57,819
I will not tolerate this
any longer, Marcus.

4
00:01:58,027 --> 00:02:00,222
Your brother must be stopped.

5
00:02:00,797 --> 00:02:02,458
It ends tonight.

6
00:02:02,899 --> 00:02:06,163
We must move quickly,
before they turn.

7
00:02:09,204 --> 00:02:10,899
Is he still here?

8
00:02:11,506 --> 00:02:12,973
Yes.

9
00:02:13,175 --> 00:02:15,972
Viktor, he must not be harmed.

10
00:02:16,177 --> 00:02:18,304
I gave you my word, did I not?

11
00:02:18,512 --> 00:02:21,413
But William must be controlled.

12
00:02:24,651 --> 00:02:25,948
Burn the bodies.

13
00:02:26,152 --> 00:02:28,086
Search the outbuildings.

14
00:02:28,288 --> 00:02:30,256
Form two groups.
You, douse them with fuel.

15
00:02:30,457 --> 00:02:31,924
- Yes, sir.
- Marcus.

16
00:02:33,026 --> 00:02:34,754
Stay with me.

17
00:03:15,263 --> 00:03:17,094
Give me the torch.

18
00:03:21,703 --> 00:03:23,193
They're turning!

19
00:03:25,439 --> 00:03:26,667
They're turning!

20
00:05:02,227 --> 00:05:05,287
- Retreat to the woods.
- I'll stay and fight. You need my help.

21
00:05:05,630 --> 00:05:07,221
I need you alive.

22
00:05:07,431 --> 00:05:08,728
If you die, we all die.

23
00:05:08,932 --> 00:05:10,058
Now, go.

24
00:05:10,801 --> 00:05:12,098
Go!

25
00:05:36,925 --> 00:05:38,721
- We found him.
- And?

26
00:05:38,926 --> 00:05:41,417
- We need more men.
- Find Amelia.

27
00:05:50,670 --> 00:05:52,729
Amelia, we found William.

28
00:06:19,129 --> 00:06:20,858
Surround him!

29
00:06:23,900 --> 00:06:25,765
Take him down.

30
00:06:34,076 --> 00:06:35,441
No.

31
00:06:37,479 --> 00:06:39,207
Leave him be.

32
00:06:42,650 --> 00:06:44,345
Stop this, you're killing him.

33
00:06:47,722 --> 00:06:48,882
More!

34
00:07:13,378 --> 00:07:14,811
William.

35
00:07:15,013 --> 00:07:16,241
Marcus!

36
00:07:18,249 --> 00:07:19,647
He was not to be harmed.

37
00:07:20,084 --> 00:07:22,143
Place him in my charge
as we agreed...

38
00:07:22,353 --> 00:07:24,321
...or you will pay for your deceit.

39
00:07:24,521 --> 00:07:27,183
And you will learn your place.

40
00:07:27,391 --> 00:07:30,359
Your sympathy for this beast
is foolish.

41
00:07:30,560 --> 00:07:33,324
Your brother is entirely
beyond control.

42
00:07:33,529 --> 00:07:35,053
It will be done my way.

43
00:07:35,264 --> 00:07:38,722
You know well the consequences
if you murder me...

44
00:07:38,935 --> 00:07:40,162
...or William.

45
00:07:40,369 --> 00:07:43,805
If you so much as speak
his name again...

46
00:07:44,006 --> 00:07:46,372
...you will have chosen
that future for him.

47
00:07:54,882 --> 00:07:58,841
What is thy will, my lord?

48
00:07:59,687 --> 00:08:03,053
Imprisonment for all time.

49
00:08:03,923 --> 00:08:06,517
Far from you.

50
00:08:16,135 --> 00:08:20,572
<i>For six centuries I was a loyal soldier
of the Vampire clan.</i>

51
00:08:22,974 --> 00:08:24,771
<i>But I was betrayed.</i>

52
00:08:26,210 --> 00:08:29,304
<i>The war was not as it had seemed.</i>

53
00:08:29,680 --> 00:08:34,378
<i>In one night, the lies that had united
our kind had been exposed.</i>

54
00:08:35,052 --> 00:08:37,316
<i>Kraven, our second-in-command...</i>

55
00:08:37,521 --> 00:08:40,012
<i>... had formed a secret alliance
with Lucian...</i>

56
00:08:40,223 --> 00:08:42,213
<i>... ruler of the Werewolf clan...</i>

57
00:08:42,691 --> 00:08:44,921
<i>... to overthrow Viktor, our leader.</i>

58
00:08:46,429 --> 00:08:49,421
<i>But Kraven's lust for power
and domination had failed.</i>

59
00:08:51,766 --> 00:08:54,929
<i>Viktor was not the savior
I had been led to believe.</i>

60
00:08:58,106 --> 00:08:59,539
<i>He had betrayed us all.</i>

61
00:09:06,547 --> 00:09:09,710
<i>Soon the hunt will be on
for his killer.</i>

62
00:09:13,252 --> 00:09:15,948
<i>I have but one ally left:</i>

63
00:09:16,155 --> 00:09:17,747
<i>Michael...</i>

64
00:09:17,957 --> 00:09:20,585
<i>... the human descendant
of Corvinus.</i>

65
00:09:21,494 --> 00:09:23,825
<i>Neither Vampire nor Lycan...</i>

66
00:09:24,029 --> 00:09:25,826
<i>... but a hybrid.</i>

67
00:09:28,533 --> 00:09:31,161
<i>It's only a matter of time
before we're found.</i>

68
00:09:53,722 --> 00:09:56,782
<i>My only hope now
is to awaken Marcus...</i>

69
00:09:56,992 --> 00:09:59,358
<i>... our last remaining elder...</i>

70
00:09:59,561 --> 00:10:01,222
<i>... and expose the truth...</i>

71
00:10:01,430 --> 00:10:05,866
<i>... before Kraven tries to murder him
while he's still in hibernation.</i>

72
00:10:07,735 --> 00:10:11,068
<i>Kraven knows
he's no match for him awake.</i>

73
00:10:48,872 --> 00:10:50,100
Perfect.

74
00:11:23,704 --> 00:11:25,568
This thing's been dead for weeks.

75
00:11:25,772 --> 00:11:28,434
I thought Lycans went back
to their human form when they die.

76
00:11:28,641 --> 00:11:31,132
They do. It's been given
a serum to stop regression...

77
00:11:31,344 --> 00:11:33,141
...so it can be studied.

78
00:11:33,346 --> 00:11:34,539
How can you tell?

79
00:11:42,554 --> 00:11:44,419
Not quite your department, I guess.

80
00:11:44,622 --> 00:11:47,989
I just killed them. I didn't pay
much attention to their anatomy.

81
00:11:49,193 --> 00:11:51,286
- How long can we stay here?
- Not long.

82
00:11:51,495 --> 00:11:53,986
These safe houses are
linked together on one mainframe...

83
00:11:54,198 --> 00:11:56,757
...with motion sensors revealing
which ones are active.

84
00:11:56,967 --> 00:11:59,299
Someone could have
picked us up already.

85
00:12:07,309 --> 00:12:09,504
There's only about an hour
till daylight.

86
00:12:09,912 --> 00:12:12,142
Can you make it back
to the mansion before sunrise?

87
00:12:12,348 --> 00:12:15,010
- Just.
- Okay, let's get what we need and go.

88
00:12:15,416 --> 00:12:17,213
No.

89
00:12:18,486 --> 00:12:20,317
I'm going alone.

90
00:12:39,639 --> 00:12:41,300
Open it.

91
00:14:26,736 --> 00:14:27,929
Marcus?

92
00:14:28,136 --> 00:14:32,971
The blood memories
of this wretched creature...

93
00:14:34,008 --> 00:14:39,274
...have shown me that your treachery
knows no bounds.

94
00:14:40,280 --> 00:14:43,113
My lord, I can explain.

95
00:14:43,550 --> 00:14:46,815
Why would I listen to your lies...

96
00:14:47,020 --> 00:14:49,954
...when the journey to the truth...

97
00:14:53,659 --> 00:14:56,423
...is so much sweeter?

98
00:15:04,002 --> 00:15:06,971
<i>I kept his secrets,
cleaned up the mess.</i>

99
00:15:08,039 --> 00:15:09,300
You'll go before Viktor...

100
00:15:09,774 --> 00:15:11,833
...and tell him
exactly what I tell you to!

101
00:15:13,277 --> 00:15:15,609
You just concentrate on your part.

102
00:15:26,055 --> 00:15:27,249
Please.

103
00:15:28,091 --> 00:15:30,388
I can assist you.

104
00:15:30,592 --> 00:15:32,753
Oh, you already have.

105
00:15:39,767 --> 00:15:40,995
If I can plead my case...

106
00:15:41,202 --> 00:15:43,295
...there's a chance
you'll be granted sanctuary.

107
00:15:43,504 --> 00:15:46,405
Right now, you'll be killed on sight.
I'm not prepared to risk it.

108
00:15:46,607 --> 00:15:47,972
What am I supposed to do, wait?

109
00:15:48,176 --> 00:15:50,837
Kraven may still have men with him.
You're not going alone.

110
00:15:51,044 --> 00:15:52,909
You're not as strong
as you might think.

111
00:15:53,113 --> 00:15:55,707
- What?
- Michael, you're unique.

112
00:15:55,916 --> 00:15:57,941
There's never been a hybrid before.

113
00:15:58,151 --> 00:16:01,711
However ambivalent you feel about it,
your powers could be limitless.

114
00:16:01,921 --> 00:16:03,149
But you depend on blood.

115
00:16:03,656 --> 00:16:05,453
You need to feed.

116
00:16:05,658 --> 00:16:09,185
Without it, you'll be growing weaker
by the second. Use the time for that.

117
00:16:09,594 --> 00:16:11,391
Jesus Christ.

118
00:16:12,363 --> 00:16:14,160
And what if I don't?

119
00:16:14,432 --> 00:16:15,865
What if I can't?

120
00:16:16,067 --> 00:16:17,967
Normal food could be lethal.

121
00:16:18,169 --> 00:16:20,966
If you don't anticipate your cravings,
you will attack humans.

122
00:16:21,171 --> 00:16:24,299
And believe me, you don't want that
on your conscience.

123
00:16:25,142 --> 00:16:26,905
There really is
no going back, Michael.

124
00:16:29,513 --> 00:16:31,207
I'm sorry.

125
00:16:31,681 --> 00:16:33,842
Look, I understand what you did.

126
00:16:34,050 --> 00:16:35,813
I'm grateful.

127
00:16:36,886 --> 00:16:38,979
You saved my life.

128
00:16:39,589 --> 00:16:40,850
I wasn't ready to die.

129
00:16:45,427 --> 00:16:47,418
I don't know.

130
00:16:47,629 --> 00:16:48,755
Everything's changed.

131
00:16:49,264 --> 00:16:51,697
I probably need a minute
to make it all fit in my head.

132
00:16:51,899 --> 00:16:53,992
It's a lot to process all at once.

133
00:16:56,003 --> 00:16:58,870
Look, go. I'll be here.

134
00:16:59,440 --> 00:17:01,930
You just make sure you come back.

135
00:18:27,185 --> 00:18:28,447
Sir.

136
00:18:28,653 --> 00:18:31,087
The innocent who witnessed...

137
00:18:31,289 --> 00:18:32,585
...they've been silenced?

138
00:18:33,023 --> 00:18:35,321
But otherwise unharmed,
as ordered.

139
00:18:39,563 --> 00:18:42,123
- Show me what you have.
- Yes, sir.

140
00:18:47,403 --> 00:18:49,030
Two Death Dealers were killed.

141
00:18:49,238 --> 00:18:51,103
We found no Lycan bodies.

142
00:18:51,341 --> 00:18:53,740
Apparently they were using
a new kind of ammunition...

143
00:18:53,942 --> 00:18:55,842
...some sort of UV round.

144
00:18:57,045 --> 00:18:58,137
Amelia?

145
00:19:00,916 --> 00:19:02,144
No one survived.

146
00:19:02,351 --> 00:19:04,716
It seems Kraven's men
might have been present...

147
00:19:04,919 --> 00:19:07,752
...but they did nothing to prevent it.

148
00:19:08,055 --> 00:19:09,784
And Viktor?

149
00:19:30,075 --> 00:19:33,305
And no trace of Marcus
amongst the ashes?

150
00:19:33,511 --> 00:19:36,207
It seems he destroyed
his own coven, sir.

151
00:19:37,448 --> 00:19:39,575
It was never his coven.

152
00:20:04,406 --> 00:20:05,737
<i>We need to go.</i>

153
00:20:49,280 --> 00:20:51,305
Give me a moment.

154
00:23:09,673 --> 00:23:11,573
There you are.

155
00:24:13,864 --> 00:24:15,422
How you doing?

156
00:24:36,785 --> 00:24:38,343
Thank you.

157
00:26:00,361 --> 00:26:02,760
You've got to get away from me.

158
00:26:04,230 --> 00:26:05,595
Get away from me!

159
00:26:17,809 --> 00:26:19,106
Hello?

160
00:26:25,449 --> 00:26:26,814
There.

161
00:26:29,086 --> 00:26:30,280
<i>Michael Corvin.</i>

162
00:26:30,488 --> 00:26:31,784
Get the men up there. Now.

163
00:26:42,564 --> 00:26:44,327
Get the fuck away from me.

164
00:27:10,624 --> 00:27:11,851
Stop. Stop.

165
00:27:17,596 --> 00:27:18,824
Shit.

166
00:28:44,841 --> 00:28:47,810
Michael. Oh, shit.

167
00:28:54,783 --> 00:28:57,479
- Here. Take it.
- No.

168
00:28:57,686 --> 00:28:59,711
Michael, you'll die.

169
00:29:25,311 --> 00:29:27,074
Shit.

170
00:29:27,380 --> 00:29:29,109
Can you move?

171
00:29:33,519 --> 00:29:34,746
We need to go.

172
00:29:55,738 --> 00:29:56,932
Marcus.

173
00:29:57,140 --> 00:29:59,608
I know what you've done, Selene.

174
00:30:00,243 --> 00:30:02,734
Viktor deserved his fate
and Kraven was no better.

175
00:30:02,946 --> 00:30:08,076
Kraven has already reaped
the rewards of his own misdeeds.

176
00:30:08,650 --> 00:30:10,083
And Viktor...

177
00:30:10,485 --> 00:30:12,953
Viktor deserved his fate...

178
00:30:13,155 --> 00:30:14,554
...many times over.

179
00:30:16,224 --> 00:30:18,192
A terrible business...

180
00:30:18,392 --> 00:30:20,223
...the slaying of your family.

181
00:30:21,429 --> 00:30:23,420
Yet so much effort was spent...

182
00:30:23,631 --> 00:30:25,894
...to conceal this matter from me.

183
00:30:28,001 --> 00:30:31,095
What do you suppose
Viktor had to hide?

184
00:30:32,105 --> 00:30:35,370
Or perhaps it is you, Selene...

185
00:30:37,343 --> 00:30:41,006
...as the last
of your wretched family...

186
00:30:41,780 --> 00:30:44,078
...who has something to hide.

187
00:30:53,691 --> 00:30:55,124
Go!

188
00:31:30,591 --> 00:31:31,819
Get in.

189
00:32:39,753 --> 00:32:43,382
Dead or alive,
you will give me what I want.

190
00:33:56,456 --> 00:33:58,117
You okay?

191
00:34:01,960 --> 00:34:04,292
- He's a hybrid, isn't he?
- Yes.

192
00:34:05,464 --> 00:34:07,762
He wanted this. Why?

193
00:34:08,934 --> 00:34:10,525
I don't know.

194
00:34:10,735 --> 00:34:12,930
But we have another problem.

195
00:34:18,142 --> 00:34:19,336
Turn off up here.

196
00:34:25,148 --> 00:34:27,139
Shit. All right, get down.

197
00:34:28,251 --> 00:34:30,218
Keep your foot on the gas,
but stay down.

198
00:34:46,034 --> 00:34:47,831
Hold on.

199
00:35:02,548 --> 00:35:04,175
Stay down.

200
00:35:58,799 --> 00:36:00,266
Come on.

201
00:36:17,850 --> 00:36:19,317
Give me your hand.

202
00:36:19,518 --> 00:36:21,042
Jesus Christ.

203
00:36:22,687 --> 00:36:24,518
There's really no need.

204
00:36:24,722 --> 00:36:26,349
I'll be right back.

205
00:36:43,039 --> 00:36:44,233
All right, let me see.

206
00:36:49,178 --> 00:36:50,770
See?

207
00:36:50,980 --> 00:36:52,571
No need.

208
00:40:40,720 --> 00:40:41,948
<i>Area secure, sir.</i>

209
00:42:09,700 --> 00:42:11,065
Sun's setting.

210
00:42:13,103 --> 00:42:14,502
What's wrong?

211
00:42:21,410 --> 00:42:24,106
The supplies were taken.
Used weapons were left behind.

212
00:42:24,547 --> 00:42:27,675
<i>The tavern events occurred before
dawn, so they couldn't have got far.</i>

213
00:42:27,884 --> 00:42:30,613
Remain airborne for the moment.
I'm sure they'll reappear...

214
00:42:30,819 --> 00:42:33,413
<i>- ... in good time.
- Yes, sir.</i>

215
00:42:37,726 --> 00:42:40,558
I've seen this before,
when I was a child.

216
00:42:40,761 --> 00:42:42,285
I've held it.

217
00:42:42,496 --> 00:42:43,986
When it was open, like this.

218
00:42:44,198 --> 00:42:45,426
How is that possible?

219
00:42:47,701 --> 00:42:49,429
I don't know.

220
00:42:51,771 --> 00:42:53,329
But I know someone that might.

221
00:42:54,107 --> 00:42:56,075
Andreas Tanis.

222
00:42:56,276 --> 00:42:59,109
He was the official historian
of the covens.

223
00:43:01,613 --> 00:43:05,344
He fell from favor after documenting
what Viktor considered malicious lies.

224
00:43:05,550 --> 00:43:08,280
Of course, as it turns out,
he was probably telling the truth.

225
00:43:08,487 --> 00:43:09,783
What happened to him?

226
00:43:09,987 --> 00:43:12,080
He was exiled over 300 years ago.

227
00:43:12,723 --> 00:43:14,190
Three hundred years?

228
00:43:14,392 --> 00:43:16,155
What makes you think
we'll find him now?

229
00:43:16,560 --> 00:43:17,993
I was the one who exiled him.

230
00:43:57,531 --> 00:43:59,123
Looks like a monastery.

231
00:43:59,333 --> 00:44:01,357
It used to be.
More like a prison now.

232
00:44:02,034 --> 00:44:04,468
Tanis has been hiding there
since Viktor's order.

233
00:44:04,670 --> 00:44:07,195
We'll be the first people
he's seen in centuries.

234
00:44:19,150 --> 00:44:21,174
- That's odd.
- What?

235
00:44:21,752 --> 00:44:24,346
I don't remember this gate
being here before.

236
00:44:26,657 --> 00:44:27,988
Take this.

237
00:45:33,350 --> 00:45:34,749
Shit.

238
00:45:58,373 --> 00:45:59,499
Go back!

239
00:46:01,943 --> 00:46:03,170
Gotcha.

240
00:46:06,213 --> 00:46:07,271
Fuck.

241
00:48:41,453 --> 00:48:43,580
I knew it was you, Selene.

242
00:48:44,089 --> 00:48:47,353
The stench of Viktor's blood
still lingers in your veins.

243
00:48:47,558 --> 00:48:48,991
Tanis.

244
00:48:49,193 --> 00:48:50,922
I see your aim hasn't improved.

245
00:48:53,197 --> 00:48:55,392
You haven't changed.

246
00:48:55,600 --> 00:48:57,294
You don't scare me, Selene.

247
00:48:58,401 --> 00:49:00,232
Well, we're gonna
have to work on that.

248
00:49:10,412 --> 00:49:11,743
We need to talk.

249
00:49:15,918 --> 00:49:18,477
Your exile seems a bit more
comfortable than I remember.

250
00:49:19,420 --> 00:49:21,285
How does a Vampire
have Lycan bodyguards?

251
00:49:21,823 --> 00:49:22,915
A gift.

252
00:49:23,124 --> 00:49:24,682
From a most persuasive client.

253
00:49:25,893 --> 00:49:27,257
Lucian.

254
00:49:29,029 --> 00:49:30,519
Why would Lucian
wanna protect you?

255
00:49:32,232 --> 00:49:33,756
Because he was trading with them.

256
00:49:35,902 --> 00:49:37,926
UV rounds.

257
00:49:38,137 --> 00:49:40,765
How long have you been
in the business of killing your own?

258
00:49:41,140 --> 00:49:44,041
I've done what's necessary
to survive.

259
00:49:44,243 --> 00:49:48,542
My decision was made easy the day
your precious Viktor betrayed me.

260
00:49:48,747 --> 00:49:51,580
Betrayal was something
he did very well.

261
00:49:52,951 --> 00:49:55,647
Viktor's dead. I killed him.

262
00:49:55,854 --> 00:49:57,252
You?

263
00:49:57,454 --> 00:49:59,183
Kill Viktor?

264
00:50:03,260 --> 00:50:05,228
No, I think not.

265
00:50:07,463 --> 00:50:09,363
Oh, unless you've learned the truth.

266
00:50:14,737 --> 00:50:17,297
So your eyes are finally open.

267
00:50:18,073 --> 00:50:20,337
Isn't it interesting how the truth...

268
00:50:20,542 --> 00:50:22,874
...is even harder to absorb
than light?

269
00:50:23,078 --> 00:50:25,239
You know, I tried to stop him,
of course.

270
00:50:25,447 --> 00:50:28,381
A travesty,
committing such a horrible crime.

271
00:50:29,450 --> 00:50:31,645
And then turning you.

272
00:50:31,852 --> 00:50:33,945
That was too much to take.

273
00:50:34,188 --> 00:50:36,486
My protests are why he put me here.

274
00:50:37,491 --> 00:50:39,549
Careful with that, dear.

275
00:50:39,759 --> 00:50:40,919
Makes a terrible bang.

276
00:50:42,295 --> 00:50:43,819
Open the blades and they're active.

277
00:50:44,564 --> 00:50:46,498
Good to know.

278
00:50:50,602 --> 00:50:53,070
Viktor put you here for a reason,
but I doubt it was...

279
00:50:53,272 --> 00:50:55,570
...because you had moral qualms.

280
00:50:55,774 --> 00:50:59,766
- What do you know?
- Very little of anything, I'm afraid.

281
00:51:06,117 --> 00:51:09,381
Well, then, perhaps I'm mistaken
and there's no use for you at all.

282
00:51:10,654 --> 00:51:12,747
Marcus was after this. Why?

283
00:51:57,497 --> 00:52:00,294
Some history is based on truth...

284
00:52:00,499 --> 00:52:02,592
...and others on deception.

285
00:52:04,436 --> 00:52:07,997
Viktor was not the first of our kind,
as you were led to believe.

286
00:52:08,206 --> 00:52:10,139
He was once human...

287
00:52:10,341 --> 00:52:12,502
...the ruler of these lands.

288
00:52:14,044 --> 00:52:15,807
<i>Marcus...</i>

289
00:52:16,480 --> 00:52:18,380
<i>... he's the one.</i>

290
00:52:20,150 --> 00:52:21,447
<i>The source.</i>

291
00:52:22,719 --> 00:52:24,687
<i>The first true Vampire.</i>

292
00:52:25,221 --> 00:52:27,587
So the legend is true.

293
00:52:28,258 --> 00:52:31,522
Toward the end of his ruthless life...

294
00:52:32,594 --> 00:52:36,690
<i>... when the next breath meant more
to Viktor than silver or gold...</i>

295
00:52:36,899 --> 00:52:39,766
<i>... Marcus came with an offer...</i>

296
00:52:39,968 --> 00:52:43,698
<i>... a reprieve from sickness
and death.</i>

297
00:52:44,705 --> 00:52:46,229
Immortality.

298
00:52:46,441 --> 00:52:49,968
And in return, Viktor was to use
his army-turned-immortal to help him.

299
00:52:51,011 --> 00:52:52,638
To do what?

300
00:52:52,846 --> 00:52:55,076
To defeat
the very first Werewolves...

301
00:52:55,282 --> 00:52:58,274
...a dangerous
and infectious breed...

302
00:52:58,485 --> 00:53:01,078
...created by Marcus' own
flesh and blood:

303
00:53:03,122 --> 00:53:04,680
His twin brother, William.

304
00:53:06,759 --> 00:53:08,351
But these weren't
the Lycans we know.

305
00:53:08,727 --> 00:53:10,956
Disgusting though
your brethren may be...

306
00:53:11,162 --> 00:53:13,130
...they at least are evolved.

307
00:53:13,331 --> 00:53:15,595
No, these were raging monsters.

308
00:53:15,800 --> 00:53:19,065
Never able to take
human form again.

309
00:53:19,270 --> 00:53:22,432
It was only later generations
that learned to channel their rage.

310
00:53:22,639 --> 00:53:26,336
And William's appetite for destruction
and rampage was insatiable.

311
00:53:27,644 --> 00:53:29,043
He had to be stopped.

312
00:53:29,246 --> 00:53:31,679
And so once Viktor's army
was turned...

313
00:53:31,881 --> 00:53:34,247
...the legions of Vampires
under his control...

314
00:53:34,450 --> 00:53:37,681
...tracked down
and destroyed the animals...

315
00:53:37,887 --> 00:53:39,184
...then captured William...

316
00:53:39,822 --> 00:53:41,255
...and locked him away.

317
00:53:41,823 --> 00:53:43,950
Viktor's prisoner for all time.

318
00:53:46,628 --> 00:53:47,856
Why let him live?

319
00:53:48,063 --> 00:53:52,328
For the very same reason that Viktor
never conspired against Marcus: Fear.

320
00:53:52,766 --> 00:53:54,757
He was warned that,
should Marcus be killed...

321
00:53:55,269 --> 00:53:58,261
...all those in his bloodline
would follow him to the grave.

322
00:53:59,373 --> 00:54:01,273
So in Viktor's mind...

323
00:54:01,475 --> 00:54:04,204
...William's death would mean
the end for all Lycans...

324
00:54:04,410 --> 00:54:05,502
...his slaves.

325
00:54:07,580 --> 00:54:09,013
Yes.

326
00:54:10,250 --> 00:54:12,342
Yes, a clever deception...

327
00:54:12,551 --> 00:54:15,281
...but one Viktor was hardly willing
to put to the test.

328
00:54:16,655 --> 00:54:18,623
And so Marcus was protected...

329
00:54:18,991 --> 00:54:21,050
...at all costs.

330
00:54:24,362 --> 00:54:25,556
Yes, here we are.

331
00:54:31,335 --> 00:54:33,802
- Vampires?
- Mortals.

332
00:54:34,971 --> 00:54:37,963
Men loyal to Alexander Corvinus.

333
00:54:39,843 --> 00:54:41,140
The father of us all.

334
00:54:49,184 --> 00:54:50,515
What's this?

335
00:54:54,355 --> 00:54:55,947
You should know.

336
00:54:57,225 --> 00:54:58,954
It's William's prison.

337
00:55:01,229 --> 00:55:04,925
The prison your father
was commissioned to build.

338
00:55:10,504 --> 00:55:13,597
So you've been the one keeping
a watch over this for me, have you?

339
00:55:13,806 --> 00:55:16,070
I've been searching everywhere
for this, my darling.

340
00:55:21,080 --> 00:55:22,274
What is it?

341
00:55:22,982 --> 00:55:24,175
What's wrong?

342
00:55:25,383 --> 00:55:28,250
She now understands
why her family was killed.

343
00:55:31,222 --> 00:55:33,587
But that was many years later.

344
00:55:34,892 --> 00:55:36,757
The winter of Lucian's escape.

345
00:55:36,961 --> 00:55:38,952
Your father knew too much.

346
00:55:39,163 --> 00:55:40,926
Or too much for Viktor to risk...

347
00:55:41,465 --> 00:55:43,194
...especially when Lucian...

348
00:55:44,200 --> 00:55:47,499
...had the key to William's cell.

349
00:55:48,538 --> 00:55:50,403
And I'm the map.

350
00:55:51,274 --> 00:55:52,764
Yes.

351
00:55:53,809 --> 00:55:58,303
The only one still living
who has seen its location.

352
00:55:59,247 --> 00:56:02,045
Oh, Viktor realized you'd be too young
to remember explicitly...

353
00:56:02,250 --> 00:56:05,480
...but Marcus knows
that the memory...

354
00:56:05,686 --> 00:56:09,315
...and therefore the exact location
of William's prison...

355
00:56:09,824 --> 00:56:11,655
...is hidden away in your blood.

356
00:56:13,027 --> 00:56:15,358
Why is Marcus looking
for him now...

357
00:56:15,562 --> 00:56:17,120
...after all this time?

358
00:56:17,530 --> 00:56:18,690
That I cannot answer.

359
00:56:29,975 --> 00:56:31,533
But I do know someone...

360
00:56:31,810 --> 00:56:34,573
...who could stop him.
Perhaps I could arrange a meeting.

361
00:56:36,214 --> 00:56:40,548
In exchange for your discretion,
of course.

362
00:56:41,853 --> 00:56:43,320
Of course.

363
00:57:38,370 --> 00:57:41,271
Tanis. You seem anxious.

364
00:57:42,040 --> 00:57:44,270
Why do you flee
the very sight of me?

365
00:57:49,747 --> 00:57:50,907
Please...

366
00:57:52,016 --> 00:57:53,483
...sit.

367
00:57:54,852 --> 00:57:57,615
There's no need
for this to be unpleasant.

368
00:57:58,822 --> 00:58:01,290
I've always rather enjoyed
your company.

369
00:58:07,262 --> 00:58:09,389
Now you're being rude.

370
00:58:10,733 --> 00:58:12,166
Sorry.

371
00:58:20,374 --> 00:58:22,467
Viktor struck two keys.

372
00:58:23,077 --> 00:58:24,442
What do you know of them?

373
00:58:26,580 --> 00:58:28,104
Keys?

374
00:58:29,082 --> 00:58:31,983
I don't know of any keys.

375
00:58:47,599 --> 00:58:49,464
Oh, yes.

376
00:58:49,667 --> 00:58:51,157
Yes, those keys.

377
00:58:51,569 --> 00:58:54,504
- Yes?
- Well, one was...

378
00:58:56,040 --> 00:58:58,803
One was kept in plain sight...

379
00:58:59,009 --> 00:59:01,204
...draped around
his daughter's neck...

380
00:59:01,979 --> 00:59:04,447
...right there for you to see.
- And the other?

381
00:59:07,151 --> 00:59:10,119
- Kept with Viktor at all times.
- Where?

382
00:59:11,187 --> 00:59:12,916
Within him.

383
00:59:14,257 --> 00:59:15,884
Beneath the flesh.

384
00:59:22,330 --> 00:59:24,230
Please. Please.

385
00:59:24,432 --> 00:59:25,592
I beg you. Please.

386
00:59:25,834 --> 00:59:27,665
Please, I beg you.

387
00:59:44,250 --> 00:59:46,810
- Who could stop him.
Perhaps I could arrange a meeting.

388
00:59:47,020 --> 00:59:49,487
Go to pier 17.
Ask for Lorenz Macaro.

389
01:00:13,877 --> 01:00:16,107
How do we know Tanis
isn't setting us up?

390
01:00:16,313 --> 01:00:18,746
He's not brave enough
to set me up.

391
01:00:44,571 --> 01:00:45,902
You can go.

392
01:00:56,382 --> 01:00:58,043
You're familiar with this, then?

393
01:01:05,991 --> 01:01:07,253
Intimately.

394
01:01:14,565 --> 01:01:16,430
You're Alexander Corvinus.

395
01:01:21,738 --> 01:01:26,072
There was a time
that I was known by that name.

396
01:01:27,477 --> 01:01:30,809
By any name,
I am still your forefather.

397
01:01:36,952 --> 01:01:40,250
How have you stayed hidden
all these years?

398
01:01:40,454 --> 01:01:42,820
For centuries I've stood by
and watched the havoc...

399
01:01:43,024 --> 01:01:47,984
...my sons have wrought upon
each other and upon humanity.

400
01:01:49,263 --> 01:01:51,059
Not the legacy I prayed for...

401
01:01:51,264 --> 01:01:53,494
...the morning I watched them
enter this world.

402
01:01:54,801 --> 01:01:56,962
And a tiresome duty...

403
01:01:57,170 --> 01:02:00,333
...keeping the war contained,
cleaning up the mess...

404
01:02:01,373 --> 01:02:04,672
...hiding my family's
unfortunate history.

405
01:02:04,877 --> 01:02:07,175
- Couldn't you have stopped it?
- Yes.

406
01:02:08,480 --> 01:02:10,539
Could you kill your own sons?

407
01:02:10,748 --> 01:02:13,046
You know what Marcus will do.

408
01:02:13,251 --> 01:02:16,311
If he finds me,
he finds William's prison.

409
01:02:16,521 --> 01:02:18,648
You need to help us stop him.

410
01:02:19,991 --> 01:02:23,824
You are asking me
to help you kill my son?

411
01:02:24,027 --> 01:02:25,392
You?

412
01:02:25,595 --> 01:02:27,062
A Death Dealer?

413
01:02:27,464 --> 01:02:29,227
How many innocents did you kill...

414
01:02:29,433 --> 01:02:32,196
...in the six-century quest
to avenge your family?

415
01:02:32,401 --> 01:02:34,767
Spare me
your self-righteous declarations.

416
01:02:34,971 --> 01:02:39,999
You are no different from Marcus
and even less noble than William.

417
01:02:40,209 --> 01:02:42,938
At least he cannot control
his savagery.

418
01:02:43,144 --> 01:02:46,204
Anything I've done
can be laid at your feet.

419
01:02:46,414 --> 01:02:49,508
Hundreds of thousands have died
because of your inability to accept...

420
01:02:49,718 --> 01:02:52,709
...that your sons are monsters,
that they create monsters.

421
01:02:53,587 --> 01:02:55,054
You could've stopped all of this.

422
01:02:55,255 --> 01:02:56,984
Do not come groveling to me...

423
01:02:57,191 --> 01:03:00,854
...simply because you are weaker
than your adversary.

424
01:03:01,061 --> 01:03:03,893
You know the devastation William
caused before he was captured.

425
01:03:04,097 --> 01:03:05,428
He cannot be set free.

426
01:03:54,242 --> 01:03:56,802
Wa... No, wait.
You're no match for him.

427
01:04:19,732 --> 01:04:20,926
Soon, brother.

428
01:04:55,630 --> 01:04:57,257
No.

429
01:05:09,176 --> 01:05:10,643
Father.

430
01:05:11,078 --> 01:05:12,136
No.

431
01:05:13,581 --> 01:05:16,105
They've gone.
There, there, my child.

432
01:05:16,316 --> 01:05:17,783
You are safe now.

433
01:06:06,461 --> 01:06:08,929
No. Michael.

434
01:06:09,697 --> 01:06:10,925
Look at me.

435
01:06:13,234 --> 01:06:14,792
Michael.

436
01:06:15,335 --> 01:06:16,529
Shit.

437
01:06:42,460 --> 01:06:44,189
Please.

438
01:06:45,930 --> 01:06:47,864
Please.

439
01:06:56,839 --> 01:06:58,397
Come on.

440
01:06:58,942 --> 01:07:00,603
Come on.

441
01:07:04,214 --> 01:07:06,148
Fuck.

442
01:07:52,423 --> 01:07:53,685
Hello, Father.

443
01:07:54,325 --> 01:07:57,316
You are unwelcome in my presence.

444
01:08:00,163 --> 01:08:04,099
The predictable heart
that never thaws.

445
01:08:04,768 --> 01:08:07,998
Pity it beats within such a fool.

446
01:08:09,005 --> 01:08:10,768
The eldest of the immortals...

447
01:08:11,274 --> 01:08:15,176
...yet you have made no attempt
to seize your destiny.

448
01:08:15,378 --> 01:08:20,747
We are oddities of nature, you and I.
Nothing more.

449
01:08:20,949 --> 01:08:23,474
This is a world for humanity.

450
01:08:23,685 --> 01:08:27,814
And that petty sentiment explains
why you rejected your sons?

451
01:08:28,022 --> 01:08:30,490
Why you stood by
for over half a millennium...

452
01:08:30,691 --> 01:08:34,218
...as William suffered alone
in darkness?

453
01:08:34,762 --> 01:08:36,730
No, Father.

454
01:08:37,398 --> 01:08:40,764
I have no respect
for your petty sentiment.

455
01:08:42,635 --> 01:08:45,468
Viktor's key. Where is it?

456
01:08:45,672 --> 01:08:48,401
Whatever plan you have
for William is futile.

457
01:08:48,607 --> 01:08:51,735
- You cannot control your brother.
- Oh, I am stronger now.

458
01:08:51,943 --> 01:08:55,344
And our bond is greater than you
have ever wanted to acknowledge.

459
01:08:55,547 --> 01:08:56,844
You're wrong.

460
01:08:57,049 --> 01:09:00,245
Soon you'll be drowning in Lycans,
just like before.

461
01:09:00,451 --> 01:09:02,112
Oh, not Lycans, Father...

462
01:09:02,520 --> 01:09:04,385
...or Vampires.

463
01:09:04,588 --> 01:09:09,081
A new race, created in
the image of their maker...

464
01:09:10,193 --> 01:09:11,820
...their new god:

465
01:09:14,831 --> 01:09:16,799
Me.

466
01:09:28,711 --> 01:09:31,178
And a true god...

467
01:09:35,883 --> 01:09:37,510
...has no father.

468
01:10:13,650 --> 01:10:15,481
You will fail.

469
01:10:51,351 --> 01:10:53,080
- Get the kit.
- No.

470
01:11:00,460 --> 01:11:03,690
Please, sir, let us help you.

471
01:11:04,196 --> 01:11:06,187
The time has come, my friend.

472
01:11:07,833 --> 01:11:09,323
Find the girl.

473
01:11:10,769 --> 01:11:12,827
Bring her to me.

474
01:11:18,342 --> 01:11:19,639
No, wait. Wait.

475
01:11:23,313 --> 01:11:26,441
If you want Marcus,
you'll need Alexander's help.

476
01:11:32,755 --> 01:11:34,222
Don't leave him here.

477
01:11:54,908 --> 01:11:58,571
- Did he get the pendant?
- Yes.

478
01:11:59,379 --> 01:12:02,006
He's too powerful for you alone.

479
01:12:02,582 --> 01:12:04,641
You are the only one
older than he is...

480
01:12:04,851 --> 01:12:06,819
...the only one stronger.

481
01:12:07,019 --> 01:12:09,385
You could have killed him yourself.

482
01:12:10,590 --> 01:12:13,581
No matter what he's become...

483
01:12:15,026 --> 01:12:16,391
...he is my son.

484
01:12:22,733 --> 01:12:25,497
You are the last hope left.

485
01:12:26,437 --> 01:12:29,736
There is only one way
to defeat him.

486
01:12:37,080 --> 01:12:39,105
Quickly, now...

487
01:12:39,315 --> 01:12:43,307
...before there is no more legacy
left in my veins.

488
01:12:51,827 --> 01:12:53,987
What will I become?

489
01:12:55,796 --> 01:12:57,423
The future.

490
01:13:40,137 --> 01:13:42,071
Go now.

491
01:17:29,576 --> 01:17:31,043
We're getting close.

492
01:18:26,295 --> 01:18:28,525
Head back around
towards the river.

493
01:18:40,574 --> 01:18:42,667
I don't see a way inside.

494
01:18:44,945 --> 01:18:47,175
There used to be
a river entrance just there.

495
01:18:47,380 --> 01:18:49,404
It must be submerged now.

496
01:18:49,615 --> 01:18:51,082
Looks like we're getting wet.

497
01:19:27,450 --> 01:19:28,974
Closer.

498
01:19:34,623 --> 01:19:36,181
Selene.

499
01:20:49,390 --> 01:20:50,687
William.

500
01:21:06,205 --> 01:21:07,695
Wait.

501
01:21:12,744 --> 01:21:14,211
No, William. Stop.

502
01:21:14,746 --> 01:21:16,338
Be still, brother.

503
01:21:16,548 --> 01:21:18,015
It's me.

504
01:21:21,686 --> 01:21:24,347
I'd no sooner harm you than myself.

505
01:21:56,851 --> 01:21:58,045
Which way?

506
01:22:26,010 --> 01:22:27,807
Is this it?

507
01:22:32,116 --> 01:22:34,015
He's already here.

508
01:23:07,815 --> 01:23:09,544
What is that?

509
01:23:09,750 --> 01:23:12,947
Nothing. Let's go.

510
01:23:24,030 --> 01:23:26,156
We're too late.

511
01:23:53,756 --> 01:23:55,347
- After him.
- Get behind him.

512
01:24:04,766 --> 01:24:06,392
Impressive.

513
01:25:15,330 --> 01:25:16,819
<i>Michael, you're unique.</i>

514
01:25:19,333 --> 01:25:21,961
<i>There's never been
a hybrid before.</i>

515
01:25:23,103 --> 01:25:24,866
<i>Your powers could be limitless.</i>

516
01:25:42,321 --> 01:25:44,551
Hold it! Oh, fuck.

517
01:25:48,292 --> 01:25:51,056
- Where is he?
- He went right through there.

518
01:25:52,763 --> 01:25:55,527
All we have are UV rounds.
They won't take him down.

519
01:25:55,733 --> 01:25:58,200
No, but they'll slow him down.

520
01:26:44,477 --> 01:26:46,104
Let's go.

521
01:26:46,646 --> 01:26:47,840
You all right?

522
01:28:04,749 --> 01:28:06,444
Jesus Christ!

523
01:29:58,418 --> 01:29:59,976
But these weren't
the Lycans we know.

524
01:30:00,187 --> 01:30:01,518
No, these were raging monsters.

525
01:30:01,721 --> 01:30:03,847
Unable to take human form
ever again.

526
01:30:54,335 --> 01:30:56,200
What are you waiting for?
Shoot them.

527
01:30:56,403 --> 01:30:59,099
I can't. Not without taking her out.

528
01:30:59,306 --> 01:31:00,500
I can't get a shot.

529
01:31:52,254 --> 01:31:53,482
Michael.

530
01:32:29,788 --> 01:32:31,847
Take her down.

531
01:33:04,152 --> 01:33:05,119
Cut us loose!

532
01:33:08,889 --> 01:33:10,550
We're going down!

533
01:34:50,181 --> 01:34:51,614
William!

534
01:35:02,258 --> 01:35:06,160
I knew Viktor made a mistake
by keeping you as a pet.

535
01:35:06,696 --> 01:35:10,062
He should have killed you
with the rest of your family.

536
01:37:58,417 --> 01:38:01,476
<i>An unknown chapter lies ahead.</i>

537
01:38:01,686 --> 01:38:04,951
<i>The lines that had divided the clans
have now been blurred.</i>

538
01:38:05,156 --> 01:38:08,091
<i>Chaos and infighting are inevitable.</i>

539
01:38:08,293 --> 01:38:11,853
<i>All that is certain is that darkness
is still ahead.</i>

540
01:38:12,062 --> 01:38:15,793
<i>But for now, for the first time...</i>

541
01:38:16,000 --> 01:38:18,833
<i>... I look into the light with new hope.</i>

subtitled by ren