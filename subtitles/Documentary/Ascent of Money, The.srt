1
00:02:18,800 --> 00:02:20,093
In the beginning

2
00:02:20,866 --> 00:02:22,293
There was only The Word,

3
00:02:23,193 --> 00:02:27,631
And with The Word He spoke into being
the realm of Heaven and its angels.

4
00:02:28,991 --> 00:02:30,990
And He spoke into being
the sun, the moon and the stars.

5
00:02:32,831 --> 00:02:34,829
And then the Earth,
its oceans and its beasts.

6
00:02:36,670 --> 00:02:38,668
And finally He spoke into being
His masterpiece:

7
00:02:40,509 --> 00:02:42,506
Us.

8
00:02:42,427 --> 00:02:44,425
And we were His children
to love, keep and protect.

9
00:02:46,266 --> 00:02:50,344
Placed above all others, we were
His most cherished creation,

10
00:02:51,104 --> 00:02:53,104
most beloved on Earth
and in Heaven.

11
00:02:53,984 --> 00:02:55,983
This was The Word as it
was spoken to the prophets,

12
00:02:57,821 --> 00:02:59,822
and then it was
handed down to us.

13
00:03:00,701 --> 00:03:04,060
The Word of God
our Father in Heaven.

14
00:03:05,540 --> 00:03:09,379
And a thousand generations
found shelter in that Word

15
00:03:13,217 --> 00:03:15,217
So here we are tonight,

16
00:03:16,137 --> 00:03:19,495
The Word
becomes flesh and blood.

17
00:03:21,855 --> 00:03:25,293
Our flesh and blood.
And I got news.

18
00:03:25,693 --> 00:03:29,691
The Lord, He didn't keep his
Word, No. He backed out on it.

19
00:03:30,531 --> 00:03:35,050
He abandoned His kids a long time ago,
and He ain't payin' child support.

20
00:03:35,330 --> 00:03:39,369
- Am I right, Maggie ?
- You got that right, preacher boy.

21
00:03:42,087 --> 00:03:45,967
Brothers and sisters,
our Father is a deadbeat dad !

22
00:03:46,886 --> 00:03:48,886
- Amen to that.

23
00:03:49,765 --> 00:03:51,765
That's right. And we can all
forget about the "divine plan, "

24
00:03:53,643 --> 00:03:55,643
because there
is no divine plan.

25
00:03:55,564 --> 00:03:57,563
It's a blind universe
out there, friends.

26
00:03:58,441 --> 00:04:02,481
Our lives are nothing but
a chain of unrelated accidents.

27
00:04:03,241 --> 00:04:05,242
- Get used to it !
- Get used to it !

28
00:04:06,121 --> 00:04:08,121
Bad things happen to good people
for no reason. Get used to it.

29
00:04:09,959 --> 00:04:11,959
Get used to it ! - Good things they
happen to bad people for no reason.

30
00:04:14,757 --> 00:04:16,757
- Get used to it ! -
Get used to it ! - Say it again !

31
00:04:17,676 --> 00:04:19,676
- Get used to it !
- I can't hear you !
- Get used to it !

32
00:04:21,515 --> 00:04:23,515
- Do you want salvation ?
- We want salvation !

33
00:04:23,435 --> 00:04:25,434
- Do you want it now ?
- We want it now !

34
00:04:25,355 --> 00:04:28,113
God is dead !

35
00:04:30,193 --> 00:04:33,871
God is not dead.
He just doesn't give a damn.

36
00:05:21,700 --> 00:05:25,020
Please help me. Please.
Oh, God.

37
00:05:29,347 --> 00:05:31,080
No.

38
00:07:14,891 --> 00:07:17,075
Praise the Lord.

39
00:07:18,167 --> 00:07:19,608
It's done.

40
00:07:22,839 --> 00:07:27,651
No. No, I did what you told me.

41
00:07:27,651 --> 00:07:30,532
The blasphemer is dead.

42
00:07:34,704 --> 00:07:37,479
Oh. Oh.

43
00:07:39,624 --> 00:07:39,624
Oh.

44
00:07:42,330 --> 00:07:44,930
- Oh, but I couldn't.

45
00:07:44,930 --> 00:07:46,624
-...pushed me out of the way before...

46
00:07:49,692 --> 00:07:51,585
I'm sorry.

47
00:07:55,143 --> 00:07:57,291
I can find him again.

48
00:08:01,386 --> 00:08:03,933
- I can!
No.

49
00:08:03,933 --> 00:08:06,840
Yes. Yes.

50
00:08:07,640 --> 00:08:10,120
Please.

51
00:08:15,546 --> 00:08:17,122
No. Don't go.

52
00:08:17,122 --> 00:08:19,575
Don't leave me alone.

53
00:08:20,080 --> 00:08:21,733
Please.

54
00:08:23,761 --> 00:08:25,347
Please.

55
00:08:38,017 --> 00:08:44,162
Ohh.

56
00:10:05,373 --> 00:10:07,373
Hey, Joseph.

57
00:10:08,253 --> 00:10:10,253
L-282's mom
wants his corneas back.

58
00:10:11,132 --> 00:10:13,133
- Did you give her the
"gift of sight" rap ? - Yep.

59
00:10:13,052 --> 00:10:15,056
Says they were harvested
without her consent.

60
00:10:16,936 --> 00:10:18,935
- She wants her kid to go to the
next world intact. - Mm.

61
00:10:19,817 --> 00:10:21,817
"Take heed,
you senseless ones.

62
00:10:21,737 --> 00:10:23,737
Does He who
formed the eye not see ?"

63
00:10:24,617 --> 00:10:26,618
- Huh ?
- Psalm 94.

64
00:10:27,498 --> 00:10:29,498
Deal with it, Carl.

65
00:10:30,419 --> 00:10:33,940
All right.
Let's see what we have here.

66
00:10:35,219 --> 00:10:38,220
Ahhh. Leadpoisoning.

67
00:10:40,061 --> 00:10:43,861
Hmm. Okay, Rasputin.
What army did you piss off ?

68
00:10:58,306 --> 00:11:00,307
Allright.
Let's do a liver temp. Thermometer.

69
00:11:01,186 --> 00:11:03,187
Okay.

70
00:11:03,106 --> 00:11:05,110
Do the honors.

71
00:11:06,029 --> 00:11:08,027
Okay.

72
00:11:07,949 --> 00:11:09,949
Eh. The liver's
that away, Magellan.

73
00:11:10,829 --> 00:11:14,670
There you go. T.O.D.
approximately 8:00 P.M., huh ?

74
00:11:15,631 --> 00:11:17,631
All right. See what else
we got on our John Doe here.

75
00:11:19,511 --> 00:11:21,513
Um...

76
00:11:21,393 --> 00:11:23,392
"Danyael Rosales."

77
00:11:24,273 --> 00:11:26,274
Danyael.
Funny spelling, huh ?

78
00:11:27,154 --> 00:11:31,075
"Father unknown.
Mother, Valerie Rosales, deceased."

79
00:11:33,915 --> 00:11:37,117
Valerie Rosales.
Oh, Jesus.

80
00:11:46,439 --> 00:11:48,439
It's starting again.

81
00:11:48,359 --> 00:11:50,360
It all comes back,
over and over again and again.

82
00:11:51,240 --> 00:11:54,881
- Never stops, never ends.
- What do you mean. ?

83
00:11:55,121 --> 00:11:58,642
Look. Look
at that burn pattern. Look.

84
00:11:58,962 --> 00:12:00,962
- It's like...
- Go on.

85
00:12:01,802 --> 00:12:05,645
It's like she got hit with napalm
and never moved.

86
00:12:06,644 --> 00:12:10,084
All right.
These are from the scene.

87
00:12:10,483 --> 00:12:12,486
Okay.

88
00:12:12,405 --> 00:12:16,968
You get hit with a blast like that,
don't you curl up into a tighter ball,

89
00:12:17,246 --> 00:12:19,249
expose less skin ?

90
00:12:20,127 --> 00:12:22,128
- Are you asking me ?
- I've asked myself a thousand times.

91
00:12:23,967 --> 00:12:27,608
- I don'tknow.
- Look at the body position.

92
00:12:29,770 --> 00:12:31,770
I'm lookin.

93
00:12:31,690 --> 00:12:34,291
Closer.

94
00:12:34,571 --> 00:12:37,171
Closer.

95
00:12:38,412 --> 00:12:41,932
It's like she was
shielding something.

96
00:12:42,253 --> 00:12:44,773
Yes.

97
00:12:45,134 --> 00:12:47,135
Yeah.

98
00:12:47,055 --> 00:12:49,935
That something...

99
00:12:50,896 --> 00:12:52,894
was herchild

100
00:12:52,855 --> 00:12:56,097
She was
protecting her child.

101
00:12:57,657 --> 00:13:00,738
You gave him your life.

102
00:13:01,498 --> 00:13:04,619
That still wasn't enough.

103
00:13:25,942 --> 00:13:27,940
So you, uh, personally
knew the victim, huh ?

104
00:13:27,861 --> 00:13:30,580
We go back.

105
00:13:30,780 --> 00:13:34,219
I know him from
before the big bang.

106
00:13:34,619 --> 00:13:36,617
The big bang ?

107
00:13:37,497 --> 00:13:41,337
- When exactly was the big bang ?
- The beginning.

108
00:14:00,610 --> 00:14:02,609
- Good night, Joseph.

109
00:14:02,489 --> 00:14:04,490
Yeah.
Good night, Kyle.

110
00:14:21,763 --> 00:14:23,762
All right. Look. I'm going to let you go,
but I may have to call you back...

111
00:14:25,603 --> 00:14:27,601
later on down the line
as a material witness.

112
00:14:28,482 --> 00:14:32,041
Gabriel... Is there no last name ?
I mean,

113
00:14:33,320 --> 00:14:35,319
is it like the Artist Formerly
Known As Yanni or something ?

114
00:14:37,158 --> 00:14:39,157
- Sting.
- I thought it was Yanni.

115
00:14:40,037 --> 00:14:42,037
Shows you how far out oftouch I am.
Is this... this address current, "Gabriel" ?

116
00:14:43,876 --> 00:14:47,556
I can tell you
in two words: un-fortunately.

117
00:14:49,635 --> 00:14:51,634
Is that with a hyphen ?

118
00:14:52,513 --> 00:14:55,633
- Slash.
- Date of birth ?

119
00:14:57,353 --> 00:14:59,352
There's no date of birth
here. Why is that ?

120
00:15:00,231 --> 00:15:04,750
I asked the woman at the Department
of Motor Vehicles that same question.

121
00:15:05,071 --> 00:15:08,867
She told me not to bother her
on a coffee break.

122
00:15:11,788 --> 00:15:13,787
D.M.V.

123
00:15:14,667 --> 00:15:16,666
All right. Why don't you tell me ?
How old are you ?

124
00:15:18,546 --> 00:15:21,304
Thirty-nine.

125
00:15:23,303 --> 00:15:25,302
Hmm.

126
00:15:25,222 --> 00:15:28,502
H-How old do I look ?
Be cruel.

127
00:15:32,941 --> 00:15:36,418
I'll just put down "39"
instead, huh ?

128
00:15:38,174 --> 00:15:39,121
You know what?

129
00:15:39,680 --> 00:15:44,014
Fact is I'm old enough
to know better, Sparky.

130
00:15:47,312 --> 00:15:51,937
- What'd you call me ? - That's what they
called you when you were a kid, right ?

131
00:15:54,133 --> 00:15:56,133
Yeah. You did good

132
00:16:13,328 --> 00:16:15,327
- Can you buzz me in, Clark ?
No. Not before that.

133
00:16:20,085 --> 00:16:24,285
- Where's your morgue ?
- That's off-limits to the public, sir.

134
00:16:24,884 --> 00:16:27,724
A.Z.Jones, FBl.

135
00:16:31,641 --> 00:16:35,800
That'll be two floors down.
Tunnel access to the sub-basement.

136
00:16:53,754 --> 00:16:55,753
I'm gonna have to ask you
to identify the body, ma'am.

137
00:16:57,594 --> 00:16:59,592
I'm also gonna have to ask you
wait just a little bit longer here.

138
00:17:00,473 --> 00:17:04,391
- There'll be somebody along
to help you. - Thank you.

139
00:17:53,377 --> 00:17:55,375
I always knew
we'd see each other again,

140
00:17:56,296 --> 00:17:58,294
but I never thought
you'd look so...

141
00:17:59,175 --> 00:18:01,854
different.

142
00:18:02,054 --> 00:18:05,731
We become the thing we fear.
I let myself go.

143
00:18:06,852 --> 00:18:10,211
Obviously.
Or I wouldn't be here.

144
00:18:13,568 --> 00:18:16,689
- Ah.
- I need the heart.

145
00:18:17,408 --> 00:18:21,566
You, more than anyone, know what he is.
He's their last hope.

146
00:18:22,246 --> 00:18:24,245
I never know
what side you're on, Zophael.

147
00:18:25,124 --> 00:18:29,445
Well, that should be infinitely clear
to you now. Come back to us.

148
00:18:30,925 --> 00:18:34,404
We can retake
what's rightfully ours.

149
00:18:34,763 --> 00:18:38,801
We can make it like it was
before the monkeys. Remember ?

150
00:18:39,561 --> 00:18:41,560
Or has this place completely
befouled your senses ?

151
00:18:41,480 --> 00:18:45,039
I like it here.
I even learned to drive.

152
00:18:52,996 --> 00:18:54,996
You know what
your one flaw is, Gabriel ?

153
00:18:55,916 --> 00:19:00,154
You only see what you want to see.
Have you ever looked around ?

154
00:19:00,715 --> 00:19:05,073
Our Creator poured all His grace
into this world, but do you see it ?

155
00:19:05,513 --> 00:19:08,951
- Do you see it anywhere ?
- Not yet.

156
00:19:11,310 --> 00:19:15,189
Destroying Heaven
is not what I was up to, Zophael.

157
00:19:16,110 --> 00:19:19,508
Not even Lucifer,
is that arrogant?

158
00:19:22,827 --> 00:19:25,386
Go on.

159
00:19:25,706 --> 00:19:28,785
Out of my way... monkey.

160
00:21:45,221 --> 00:21:48,499
- I had to know.

161
00:22:07,334 --> 00:22:10,533
Ah. Thank you
for not jamming.

162
00:22:35,244 --> 00:22:38,324
Where did he go, Joseph ?

163
00:22:39,084 --> 00:22:42,125
- What ?
- Where is he ?

164
00:23:13,902 --> 00:23:16,994
No. You don't just stand
there straight-faced

165
00:23:16,994 --> 00:23:19,313
and tell me he walked
right out of here.

166
00:23:19,313 --> 00:23:21,968
- You don't do that.
- I'm sorry.

167
00:23:22,391 --> 00:23:24,390
Sorry ? How am I
supposed to take that, huh ?

168
00:23:26,190 --> 00:23:30,230
Probably the same way I have
for a very long time. Badly.

169
00:23:31,028 --> 00:23:33,026
I just lost the only thing
I cared about...

170
00:23:33,906 --> 00:23:35,905
in this whole
fucked-up universe.

171
00:23:36,785 --> 00:23:40,305
You haven't seen "badly,"
sir. Not yet.

172
00:24:09,774 --> 00:24:13,377
What's it like
to wake up as dead meat...

173
00:24:13,616 --> 00:24:15,616
with the buzzards circling ?

174
00:24:16,495 --> 00:24:18,495
Do I know you ?

175
00:24:18,455 --> 00:24:21,936
I know you from
before you were born.

176
00:24:25,177 --> 00:24:27,897
Both times.

177
00:24:33,859 --> 00:24:36,379
Huh ?

178
00:24:58,863 --> 00:25:00,863
"Zophael. Spy of God."

179
00:25:01,744 --> 00:25:03,743
"Bridge between rebel and
loyal factions within Heaven.

180
00:25:04,623 --> 00:25:08,545
His true allegiance is unclear
from religious texts."

181
00:25:11,385 --> 00:25:14,745
My kingdom
for a straight answer.

182
00:27:12,647 --> 00:27:15,487
Pyriel. Pyriel.

183
00:27:17,445 --> 00:27:20,526
Who the hell is Pyriel ?

184
00:27:35,729 --> 00:27:38,691
He died in my arms.

185
00:27:46,331 --> 00:27:50,533
T.O.D. was 8:00 P.M. That
was taken six hours later.

186
00:27:51,133 --> 00:27:55,094
Look. I still don't believe what
you're tryin' to tell me.

187
00:28:02,693 --> 00:28:06,296
I was afraid of that.
All right. Let's go.

188
00:28:07,495 --> 00:28:10,056
Where ?

189
00:28:11,335 --> 00:28:15,696
There are things that ghosts
and civil servants just shouldn't know.

190
00:28:20,017 --> 00:28:22,818
No. You drive.

191
00:31:45,013 --> 00:31:47,012
Embryos and idiots.

192
00:31:47,894 --> 00:31:50,814
I hate this place.

193
00:32:51,384 --> 00:32:53,385
"ln the final hour shall come one born...

194
00:32:54,264 --> 00:32:56,265
with the heart ofan angel
and the soul of a man."

195
00:32:58,145 --> 00:33:00,146
Ever hear
of a nephalim ?

196
00:33:00,065 --> 00:33:03,906
- A what ? - Take your
vitamins, Vincent. Good boy.

197
00:33:05,827 --> 00:33:07,829
Nephalim.
Half angel, half man.

198
00:33:08,750 --> 00:33:10,748
Old Testament's chock-full of 'em.
They're very nasty guys.

199
00:33:12,588 --> 00:33:14,587
And very, very hard
to kill.

200
00:33:15,468 --> 00:33:17,468
Am I right or am I right,
Boris. ?

201
00:33:17,388 --> 00:33:20,829
Are you saying
Danyael's a nephalim ?

202
00:33:23,189 --> 00:33:25,192
Am I ?
I guess I am.

203
00:33:26,031 --> 00:33:29,231
Do you really
believe that ?

204
00:33:32,791 --> 00:33:35,353
Look.

205
00:33:37,592 --> 00:33:39,593
I've had four
gutted hermaphrodites...

206
00:33:40,472 --> 00:33:44,034
burn to black pitch
right under my nose.

207
00:33:44,353 --> 00:33:46,354
I've had one cop,
my best friend,

208
00:33:46,273 --> 00:33:50,196
driven insane by the angels
shrieking in his head...

209
00:33:51,074 --> 00:33:53,076
before somehow
spontaneously combusting...

210
00:33:53,956 --> 00:33:55,957
in a madhouse he had mistaken
for a monastery.

211
00:33:56,876 --> 00:33:58,875
A pretty young woman, now dead
knocked up by a stranger...

212
00:34:00,716 --> 00:34:02,716
who left her three months, pregnant
in only 48 hours.

213
00:34:03,598 --> 00:34:05,598
And just yesterday, a young man,
allegedly her son,

214
00:34:07,397 --> 00:34:09,398
shot up six ways to sun down,,
crawled out of a drawer...

215
00:34:10,319 --> 00:34:13,558
and waltzed out
like Lazarus.

216
00:34:14,159 --> 00:34:18,280
So, yeah, I'm pretty much open
to a buffet of possibilities.

217
00:34:18,960 --> 00:34:21,841
Any suggestions ?

218
00:34:22,840 --> 00:34:25,320
No.

219
00:34:56,704 --> 00:34:58,687
What's the "D" for ?

220
00:34:59,603 --> 00:35:02,422
Uh, "doughnut."

221
00:35:03,001 --> 00:35:06,359
I'll take one of those
with the colored fragments on it.

222
00:35:08,555 --> 00:35:12,396
Did you see a young man about this tall,
dark hair, dark brown jacket,

223
00:35:12,609 --> 00:35:13,847
come in here earlier ?

224
00:35:15,500 --> 00:35:18,981
Look. That'll be
63 cents for the doughnut...

225
00:35:20,302 --> 00:35:23,742
and about, uh, 50 bucks
for the recollection.

226
00:35:27,275 --> 00:35:30,941
- What kind of an answer is that ?
- A minimum wage answer.

227
00:35:34,700 --> 00:35:36,181
You answer my question,

228
00:35:36,181 --> 00:35:38,301
or I'll personally see to it that
you spend the next millennium...

229
00:35:38,301 --> 00:35:40,608
chained to a damp wall
wondering just what it is...

230
00:35:40,608 --> 00:35:44,901
that's been winding its way up through
your bowels for the last 750 years.

231
00:35:48,675 --> 00:35:49,768
Cool.

232
00:36:05,295 --> 00:36:08,136
Look. The dude came in here
about a half an hour ago...

233
00:36:08,336 --> 00:36:11,523
and scarfed down about two dozen mixed
in less than five minutes.

234
00:36:11,523 --> 00:36:13,841
He was, like, on this serious
sugar rush, you know what I mean. ?

235
00:36:13,841 --> 00:36:16,307
Spontaneous tissue regeneration
tends to do that.

236
00:36:16,962 --> 00:36:18,948
- What ?
- Where is he now ?

237
00:36:19,821 --> 00:36:22,283
Oh.

238
00:36:37,056 --> 00:36:39,041
Hiding in shit...

239
00:36:39,915 --> 00:36:41,901
just like a monkey.

240
00:37:01,877 --> 00:37:03,862
No matter how many times you tell them,
they never seem to learn.

241
00:37:05,690 --> 00:37:09,263
You must always,
always remove the heart.

242
00:37:56,324 --> 00:37:59,103
Hey, hey, hey !

243
00:38:22,138 --> 00:38:24,123
- What are you ?
- Judgment.

244
00:38:38,379 --> 00:38:41,358
Don't push your luck.

245
00:38:46,005 --> 00:38:48,585
Ah. Ah ?

246
00:38:51,722 --> 00:38:54,384
Crazy me.

247
00:38:54,582 --> 00:38:56,566
Nice coat.

248
00:38:56,487 --> 00:38:58,472
Killing you
would be so easy.

249
00:38:58,434 --> 00:39:02,286
Yeah ? Fried food can kill me.
A mugger can kill me.

250
00:39:03,200 --> 00:39:05,184
You're not so special
down here, "Jones."

251
00:39:06,060 --> 00:39:08,045
How do you bear
what you've become, Gabriel ?

252
00:39:08,959 --> 00:39:12,571
You, who were once exalted
above all others.

253
00:39:12,770 --> 00:39:14,756
You been with a woman,
Zophael ? It's like dying.

254
00:39:15,628 --> 00:39:19,125
You moan and cry out.
You get to a spot...

255
00:39:19,441 --> 00:39:21,428
that has you
begging for release.

256
00:39:22,341 --> 00:39:25,598
Once,
I was an angel of death.

257
00:39:28,020 --> 00:39:31,593
Now, I die every day
when I have the cash.

258
00:39:33,778 --> 00:39:37,192
Enough about me.
You must be scared.

259
00:39:40,449 --> 00:39:43,308
What if you lose ?

260
00:39:44,262 --> 00:39:48,114
You must be scared you might
become like me, right ?

261
00:39:50,022 --> 00:39:52,524
Yes.

262
00:41:03,686 --> 00:41:07,609
My dying memory is being in your arms.

263
00:41:10,451 --> 00:41:13,974
I remember slipping away
and thinking,

264
00:41:15,254 --> 00:41:19,016
God, how lucky I was
to hold you one last time.

265
00:41:20,058 --> 00:41:23,219
But I... I kept slipping...

266
00:41:26,781 --> 00:41:28,782
until all around me
there were just bodies.

267
00:41:29,663 --> 00:41:32,626
I don't understand.

268
00:41:55,680 --> 00:41:57,683
No.

269
00:41:58,604 --> 00:42:00,605
Maggie, get out of here.

270
00:42:01,485 --> 00:42:04,728
- Get out !
- No, Danyael ! No !

271
00:42:11,092 --> 00:42:13,092
Maggie, get out of here.
- No ! No, Danyael ! Please.

272
00:42:13,973 --> 00:42:17,495
- Please don't go.
- Maggie, I said go !

273
00:42:58,283 --> 00:43:01,366
911.

274
00:43:16,576 --> 00:43:18,577
- How does it feel, Danyael,

275
00:43:19,457 --> 00:43:22,860
to know that
you're almost perfect ?

276
00:43:37,750 --> 00:43:41,792
- If not for the monkey inside you.

277
00:44:06,609 --> 00:44:09,932
- What am I to you. ?

278
00:44:12,412 --> 00:44:14,414
What is this ?

279
00:44:15,295 --> 00:44:19,138
I said, "What is this ?"
And why am I drawn to it ?

280
00:44:20,099 --> 00:44:22,701
Pyriel.

281
00:44:25,901 --> 00:44:28,423
Who ?

282
00:44:29,745 --> 00:44:32,747
The whited sepulcher.

283
00:44:33,587 --> 00:44:36,390
The next God.

284
00:45:17,897 --> 00:45:19,900
Help me, please.

285
00:45:20,779 --> 00:45:22,781
Come closer.

286
00:45:23,701 --> 00:45:25,702
I tried
to reason with him.

287
00:45:26,543 --> 00:45:28,545
I tried everything
I could.

288
00:45:28,464 --> 00:45:32,067
- Help's on the way.
- Good. That's good.

289
00:45:50,639 --> 00:45:53,761
There now.
That's better.

290
00:45:54,481 --> 00:45:57,563
Please.
Let me explain.

291
00:46:08,891 --> 00:46:10,894
- Looking for these ?

292
00:46:13,735 --> 00:46:15,736
- Take the car. You can have it.
- I'm afraid that won't do.

293
00:46:16,951 --> 00:46:19,630
After you.

294
00:46:21,750 --> 00:46:23,748
Yes, yes, I know.

295
00:46:24,094 --> 00:46:26,857
You keep a gun under  the front seat of
your car ever since you were robbed ...

296
00:46:27,043 --> 00:46:31,549
in that dark  cul-de-sac in that
very, very bad part of town.

297
00:46:32,347 --> 00:46:36,106
But the idea of actually
using it is a bit, what. ?

298
00:46:37,186 --> 00:46:39,864
Repulsive ?

299
00:46:40,063 --> 00:46:43,942
So therefore you
keep the bullets stashed safely...

300
00:46:44,863 --> 00:46:47,742
in the glove box.

301
00:46:49,662 --> 00:46:53,060
I'm not here to hurt you.
Promise.

302
00:47:23,370 --> 00:47:27,447
Earth angel, Earth angel .Hmm.

303
00:48:13,354 --> 00:48:16,833
Is this the best you can do ?

304
00:48:18,100 --> 00:48:22,255
- Do we have a problem ?
- Look, why do you even need me, huh ?

305
00:48:22,933 --> 00:48:24,932
Can't drive.

306
00:48:25,810 --> 00:48:29,046
I believe this
can go faster.

307
00:48:29,644 --> 00:48:32,680
Sure.
Whatever you say.

308
00:48:46,943 --> 00:48:50,338
We really don't
have time for this !

309
00:49:09,991 --> 00:49:12,588
Monkey.

310
00:49:16,742 --> 00:49:18,739
Good trick with the car.
I'll have to remember that.

311
00:49:20,578 --> 00:49:22,574
Jerk the wheel
to the right, and... wow.

312
00:49:23,493 --> 00:49:25,491
I swear to God,
I'll use this.

313
00:49:26,329 --> 00:49:28,327
I'm sure you will.

314
00:49:28,246 --> 00:49:30,244
- But why don't we just talk instead ?
- Don't !

315
00:49:32,082 --> 00:49:34,080
I said don't !

316
00:50:07,634 --> 00:50:09,630
- Fact. I am an angel.

317
00:50:10,511 --> 00:50:12,507
I'm also your friend.

318
00:50:12,634 --> 00:50:16,387
We can both help each other, but you're
going to have to be willing to listen to me.

319
00:50:16,422 --> 00:50:18,300
Are you ready
to listen ?

320
00:50:19,180 --> 00:50:21,175
I usually measure time in eons,
but in this particular case,

321
00:50:22,055 --> 00:50:24,051
every second counts.

322
00:50:24,011 --> 00:50:28,166
- Please. I told you, I'm not
here to hurt you.

323
00:50:39,392 --> 00:50:43,027
We both see things
the same way, you and I.

324
00:50:43,227 --> 00:50:45,224
A world abandoned
by the Creator.

325
00:50:45,103 --> 00:50:47,101
A universe in chaos.

326
00:50:48,020 --> 00:50:50,018
Danyael Rosales saw it too.
He saw it more clearly than any of us.

327
00:50:51,854 --> 00:50:53,852
But he's
lost his way now.

328
00:50:54,731 --> 00:50:59,324
He's been deceived by my brothers, who
still cling to old ways, old truths...

329
00:50:59,565 --> 00:51:01,562
who still harbor
some cold, empty faith...

330
00:51:02,441 --> 00:51:04,438
that our beloved Father
shall somehow return...

331
00:51:04,358 --> 00:51:06,354
and deliver us
from this wasteland,

332
00:51:07,233 --> 00:51:10,189
this killing field.

333
00:51:12,068 --> 00:51:15,783
But that will never,
never happen, Magdalena.

334
00:51:16,862 --> 00:51:19,939
You know it. I know it.

335
00:51:20,697 --> 00:51:24,252
And most ofall,
Danyael Rosales knew it.

336
00:51:24,571 --> 00:51:26,569
- Until now.
- Why ?

337
00:51:26,449 --> 00:51:29,963
Because he's being
driven by forces...

338
00:51:30,284 --> 00:51:32,280
beyond his control.

339
00:51:32,201 --> 00:51:36,115
- Where's he going ?
- He's going to stop our savior,

340
00:51:37,033 --> 00:51:39,031
the only one who can set things
right again with Heaven and Earth.

341
00:51:39,910 --> 00:51:42,507
Savior ?

342
00:51:46,622 --> 00:51:49,579
His name is Pyriel.

343
00:51:50,497 --> 00:51:53,692
It means "light of Heaven."

344
00:51:54,331 --> 00:51:56,329
He came here
in the first war,

345
00:51:57,208 --> 00:51:59,205
led the army ofGod that cast Lucifer
to a far more distant shore.

346
00:52:01,083 --> 00:52:04,159
And here he's remained.

347
00:52:04,918 --> 00:52:06,914
- Is he evil ?
- No, no.

348
00:52:07,754 --> 00:52:09,751
Evil is the realm
of darkness.

349
00:52:10,629 --> 00:52:12,627
Pyriel is the light.

350
00:52:13,546 --> 00:52:15,543
He is the shining beacon
that will lead us...

351
00:52:16,422 --> 00:52:18,419
from an eternity
of rotting despair...

352
00:52:19,299 --> 00:52:22,693
unless Danyael's there
to stop him.

353
00:52:23,173 --> 00:52:25,170
- Why would he do something like that ?
- Shh.

354
00:52:27,009 --> 00:52:29,685
Magdalena.

355
00:52:29,885 --> 00:52:33,920
We've always communicated
with your kind through voices.

356
00:52:34,678 --> 00:52:38,192
Voices. Voices
that make saints weep...

357
00:52:38,552 --> 00:52:40,549
and zealots kill.

358
00:52:40,469 --> 00:52:42,468
Voices that drive an angry mob
to incinerate a mother...

359
00:52:44,305 --> 00:52:46,302
desperately
shielding her little boy.

360
00:52:47,182 --> 00:52:51,015
It is those same voices
that now speak to Danyael.

361
00:52:53,892 --> 00:52:55,890
Danyael would never
listen to them.

362
00:52:55,810 --> 00:52:57,806
Whatever was human
in Danyael Rosales...

363
00:52:57,727 --> 00:52:59,723
died before he rose
from that morgue.

364
00:53:00,642 --> 00:53:02,640
He is no longer
your Danyael.

365
00:53:02,559 --> 00:53:04,559
You know it.

366
00:53:05,436 --> 00:53:07,434
You felt it.

367
00:53:07,354 --> 00:53:10,071
Didn't you ?

368
00:53:19,858 --> 00:53:22,973
Why should I
believe you ?

369
00:53:23,732 --> 00:53:26,688
'Cause you have to.

370
00:53:56,871 --> 00:53:58,856
Everywhere I go,
you're there.

371
00:53:58,777 --> 00:54:02,034
I close my eyes,
you're there.

372
00:54:07,354 --> 00:54:10,532
running from
his dead mama.

373
00:54:12,159 --> 00:54:14,144
Was the boy thinking...
Was he thinking about destiny ?

374
00:54:15,017 --> 00:54:17,004
Do you think he'll live
to see another day ?

375
00:54:17,877 --> 00:54:19,862
So many
who want him dead

376
00:54:19,782 --> 00:54:23,358
Yet he gets by.
Does he stop to wonder why. ?

377
00:54:29,354 --> 00:54:31,338
Does he think he might
be here for a purpose. ?

378
00:54:31,260 --> 00:54:33,245
Does he wonder
what that purpose is ?

379
00:54:34,120 --> 00:54:37,493
I do every moment
of my existence.

380
00:54:42,735 --> 00:54:46,827
Before you were born, I tried
to rip you from your mama's womb.

381
00:54:47,461 --> 00:54:49,447
I failed.
The Sword of Heaven...

382
00:54:50,359 --> 00:54:53,854
could not still
this tiny mortal heart.

383
00:54:54,172 --> 00:54:56,832
How come. ?

384
00:54:59,930 --> 00:55:01,916
It's not being here
that perplexes me.

385
00:55:02,789 --> 00:55:04,774
It's the not knowing.

386
00:55:05,647 --> 00:55:09,024
How come you ?
What could you mean ?

387
00:55:09,460 --> 00:55:12,319
Do you know who ?

388
00:55:13,312 --> 00:55:17,004
- Nothing.
- Clearly the facts say otherwise.

389
00:55:19,982 --> 00:55:21,968
You're the Word.

390
00:55:21,889 --> 00:55:23,875
Whatever it is you're destined
to commit upon this Earth,

391
00:55:25,701 --> 00:55:29,078
it's His will,
it's what He wants.

392
00:55:30,468 --> 00:55:32,452
No angel,
however powerful,

393
00:55:32,374 --> 00:55:34,357
can be anything
but the messenger.

394
00:55:34,278 --> 00:55:37,733
Danyael, you're a message,
and now...

395
00:55:39,083 --> 00:55:41,069
for the first time in a gazillion years,
l get to know what it is.

396
00:55:42,896 --> 00:55:46,271
I get to know
what that message is.

397
00:56:10,442 --> 00:56:12,441
Okay, I did my time
in Sunday school.

398
00:56:13,362 --> 00:56:15,361
If this Pyriel's like
the Second Coming or something,

399
00:56:17,200 --> 00:56:19,198
how come I don't
know anything about him ?

400
00:56:20,078 --> 00:56:23,636
Maybe you just
chose the wrong religion.

401
00:57:10,102 --> 00:57:13,181
I dreamed of you.

402
00:57:23,578 --> 00:57:25,577
I'm Mary.

403
00:57:25,537 --> 00:57:28,616
I know the enemy ghost.

404
00:57:33,175 --> 00:57:36,813
Last night I dreamed
the end of one history...

405
00:57:38,013 --> 00:57:41,213
and the beginning
of another.

406
00:57:41,851 --> 00:57:45,731
The darkness was met by the coming
of a great warrior.

407
00:57:48,569 --> 00:57:51,448
I dreamed ofyou.

408
00:57:54,368 --> 00:57:57,567
You look smaller
in person.

409
00:58:04,965 --> 00:58:08,164
<i>% Together
Flying for life %</i>

410
00:58:22,857 --> 00:58:24,204
Tell me something.

411
00:58:25,216 --> 00:58:27,311
Do you still
love your God ?

412
00:58:30,204 --> 00:58:31,870
The truth.

413
00:58:34,754 --> 00:58:37,833
As much
as He loves me.

414
00:58:55,909 --> 00:59:00,187
Say, ma'am, I wonder if you
could tell me how to get to Gila Flats.

415
00:59:03,555 --> 00:59:07,517
Yeah, it's, um... It's over
on the Hualapai reservation.

416
00:59:08,308 --> 00:59:10,290
I don't suppose you could
be just a tad more specific,

417
00:59:12,114 --> 00:59:14,648
Madge.

418
00:59:16,907 --> 00:59:18,886
Where's your friend ?

419
00:59:18,808 --> 00:59:20,788
Oh, you
have a long memory.

420
00:59:21,660 --> 00:59:25,543
Rachel is, uh...
under the weather. She's bedridden.

421
00:59:26,453 --> 00:59:28,435
But I'll tell her
you inquired.

422
00:59:29,267 --> 00:59:32,594
- What do you want ?
- Everything.

423
00:59:33,070 --> 00:59:36,795
But to begin, I
need some coffee, fresh-brewed,

424
00:59:37,864 --> 00:59:39,846
Sweet N Low,
non-dairy creamer,

425
00:59:40,715 --> 00:59:42,697
small grape fruit juice,
three eggs, yolks firm, not hard,

426
00:59:44,519 --> 00:59:46,502
bacon, crisp, hash browns,
dry wheat toast, butter on the side...

427
00:59:47,372 --> 00:59:50,938
and a portion of your
famous blackberry jam.

428
00:59:54,068 --> 00:59:57,159
It's gonna
be a long day.

429
00:59:59,307 --> 01:00:01,304
Remember, we're here to stop him,
that's all. And nobody gets hurt.

430
01:00:02,183 --> 01:00:04,180
- Go up the hill and turn.
- I suppose you caught his scent.

431
01:00:06,057 --> 01:00:09,055
Something like that.

432
01:00:19,484 --> 01:00:22,041
There.

433
01:00:23,319 --> 01:00:26,714
You've done well.
I'm proud of you.

434
01:00:28,152 --> 01:00:31,390
- Do not lose him.
- I won't !

435
01:00:44,496 --> 01:00:47,770
- Okay. Now what ?
- Go faster.

436
01:00:48,288 --> 01:00:51,365
What's that. ?

437
01:00:56,959 --> 01:00:59,637
lgnore it.

438
01:01:15,257 --> 01:01:18,414
- Don't let up.
- I won't !

439
01:01:22,929 --> 01:01:25,606
Turn left.

440
01:01:25,805 --> 01:01:28,483
Hard left.

441
01:01:38,271 --> 01:01:40,268
I don't see him.

442
01:01:40,228 --> 01:01:42,946
He's there.

443
01:01:44,065 --> 01:01:46,621
Jesus !

444
01:01:51,775 --> 01:01:53,772
- Faster.
- We'll run him over !

445
01:01:54,652 --> 01:01:56,648
- Exactly.
- I can't do that !

446
01:01:57,528 --> 01:01:59,526
I told you
what's at stake here.

447
01:01:59,447 --> 01:02:03,400
That wasn't the deal.
Nobody was supposed to get hurt.

448
01:02:05,239 --> 01:02:07,237
How did you think we were going to
stop him, Magdalena ? With a prayer ?

449
01:02:07,157 --> 01:02:10,472
I don't know,
but not like this !

450
01:02:17,704 --> 01:02:19,702
He's almost at the destination.
We have to kill him.

451
01:02:21,539 --> 01:02:23,538
- We have no choice.
- I do.

452
01:02:24,416 --> 01:02:28,973
- I'm warning you. - Whoeveryou are,
whateveryou are, I'm not afraid ofyou.

453
01:02:30,211 --> 01:02:33,007
You should be.

454
01:03:19,232 --> 01:03:21,230
She's still
in there, Danyael.

455
01:03:22,108 --> 01:03:24,985
Dying, I suspect.

456
01:03:25,944 --> 01:03:29,182
Not that
that really matters.

457
01:04:36,101 --> 01:04:39,577
So, this is what
it must feel like...

458
01:04:39,977 --> 01:04:41,976
to be human.

459
01:04:42,855 --> 01:04:45,530
Not quite.

460
01:04:54,361 --> 01:04:56,959
Maggie.

461
01:05:05,908 --> 01:05:08,944
- Danyael.
- I'm here.

462
01:05:09,744 --> 01:05:11,742
Look behind you. - What ?

463
01:05:12,620 --> 01:05:15,176
Aaah !

464
01:05:27,044 --> 01:05:29,600
Aaah !

465
01:06:23,736 --> 01:06:27,252
This is what it feels like
to be human.

466
01:07:06,047 --> 01:07:09,243
- Bring her back.
- I can't.

467
01:07:09,883 --> 01:07:13,439
- You have to.
- What are you afraid of ?

468
01:07:17,593 --> 01:07:21,310
- Nothing.
- Why so desperate to get her back ?

469
01:07:22,388 --> 01:07:26,344
'Cause you know what Heaven's
gonna be like for her...

470
01:07:27,183 --> 01:07:30,577
if you don't finish
what you start.

471
01:08:24,835 --> 01:08:27,711
Are you an angel ?

472
01:08:31,587 --> 01:08:33,584
Once upon a time.

473
01:08:34,464 --> 01:08:38,100
- I don't want to die.
-  No.

474
01:08:44,091 --> 01:08:46,847
Who are you ?

475
01:08:47,928 --> 01:08:50,564
Gabriel.

476
01:08:53,682 --> 01:08:55,679
Hold my hand, Gabriel.

477
01:08:56,559 --> 01:08:59,154
Please.

478
01:09:02,310 --> 01:09:04,308
Oh, my God.

479
01:09:04,269 --> 01:09:07,425
I'm so afraid.

480
01:09:08,105 --> 01:09:10,982
Don't be afraid.

481
01:09:16,775 --> 01:09:20,290
- Where is Danyael ?
- He's fighting...

482
01:09:22,527 --> 01:09:25,124
for you.

483
01:10:52,861 --> 01:10:54,859
The child
of divine fornication.

484
01:10:55,738 --> 01:10:57,736
What do you want
from me ?

485
01:10:58,614 --> 01:11:00,612
I have come
to help the monkeys perish...

486
01:11:01,491 --> 01:11:03,490
by mutual slaughter...

487
01:11:03,409 --> 01:11:06,926
and then sow the Earth
with a better seed.

488
01:11:10,161 --> 01:11:12,837
Genocide.

489
01:11:13,037 --> 01:11:16,113
It happens
now and then.

490
01:11:27,460 --> 01:11:30,458
Turn away, nephalim.

491
01:11:31,297 --> 01:11:33,293
Blight not...

492
01:11:33,214 --> 01:11:35,211
and keep warm.

493
01:11:36,091 --> 01:11:38,087
Fuck you.

494
01:11:38,008 --> 01:11:41,406
There is no such thing
as destiny.

495
01:11:41,884 --> 01:11:45,119
You above all
should know that.

496
01:11:45,719 --> 01:11:47,717
What am I doing here ?

497
01:11:47,637 --> 01:11:51,034
The one thing your kind
excels at...

498
01:11:55,349 --> 01:11:57,904
dying.

499
01:12:33,783 --> 01:12:36,341
Aaah !

500
01:15:36,371 --> 01:15:38,368
It's not a mindless,
indifferent,

501
01:15:39,246 --> 01:15:42,321
blind universe, Danyael.

502
01:15:45,039 --> 01:15:47,836
It never was.

503
01:15:48,874 --> 01:15:51,711
Get used to it.

504
01:16:15,764 --> 01:16:18,281
Shh.

505
01:16:31,185 --> 01:16:33,183
ln the end there's still
The Word everywhere...

506
01:16:35,021 --> 01:16:37,019
in Heaven and its angels,,
the Earth and the stars,

507
01:16:38,856 --> 01:16:42,412
even in the darkest part
of the human soul.

508
01:16:43,690 --> 01:16:47,166
It was there
The Word burned brightest.

509
01:16:47,526 --> 01:16:49,523
And for a moment,
I was blinded

