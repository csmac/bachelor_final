﻿1
00:00:00,000 --> 00:00:00,000
<i>[One Percent of Something]</i>

2
00:00:00,000 --> 00:00:00,000
KoreanDramaX.com

3
00:00:07,600 --> 00:00:11,300
<i>[One Percent of Something]</i>

4
00:00:21,500 --> 00:00:30,299
<i>[Episode 2: Match Point.
I Don't Play Losing Games]</i>

5
00:00:36,899 --> 00:00:39,200
- Goodbye!
- Goodbye!

6
00:01:13,200 --> 00:01:16,299
Is Ms. Kim Da Hyun here, by any chance?

7
00:01:16,299 --> 00:01:17,799
She left not too long ago.

8
00:01:17,799 --> 00:01:20,500
I can get in touch with her
if it's something urgent.

9
00:01:20,500 --> 00:01:24,799
Um, no. You don't need to do that.

10
00:01:24,799 --> 00:01:26,700
It's not urgent.

11
00:01:42,799 --> 00:01:48,400
Grandpa, I've come all the way here.
Thus, I've done all that I could.

12
00:01:48,400 --> 00:01:51,900
This just goes to show
that we're not meant to be.

13
00:01:57,000 --> 00:02:01,300
Oh, my wallet!
Darn it!

14
00:02:01,300 --> 00:02:04,199
Man, not again!

15
00:02:34,800 --> 00:02:38,599
<i>This... is just a coincidence.
It's not fate.</i>

16
00:02:44,800 --> 00:02:48,500
My grandpa wants us to get married.

17
00:02:48,500 --> 00:02:51,000
That doesn't seem like it
has anything to do with me.

18
00:02:51,000 --> 00:02:52,599
Yes, it does.

19
00:02:52,599 --> 00:02:55,099
Because you're the one
that my grandfather chose.

20
00:02:55,800 --> 00:02:58,000
In our family,
whatever that man says is law.

21
00:02:58,000 --> 00:03:00,699
Then you and your grandpa
should talk it over

22
00:03:00,699 --> 00:03:03,400
instead of burdening others
with your family problems!

23
00:03:04,300 --> 00:03:06,500
You shouldn't be acting
as if this isn't your problem

24
00:03:06,500 --> 00:03:09,900
since he wants us to get married.

25
00:03:09,900 --> 00:03:12,699
You. And me. This marriage
will involve the two of us.

26
00:03:12,699 --> 00:03:14,199
What do you mean, this marriage?

27
00:03:14,199 --> 00:03:17,699
I told you!
I have no idea who your grandpa is!

28
00:03:17,699 --> 00:03:18,699
Your grandpa and you
have nothing to do with me!

29
00:03:18,699 --> 00:03:20,900
I just can't bring myself
to believe that!

30
00:03:23,699 --> 00:03:25,500
Don't believe me, then.
Whatever.

31
00:03:27,199 --> 00:03:29,699
What are you doing?
I'm not done talking.

32
00:03:30,099 --> 00:03:32,000
- Let's go.
- Where?

33
00:03:32,000 --> 00:03:33,900
I want the three of us to talk.

34
00:03:33,900 --> 00:03:34,900
What?

35
00:03:34,900 --> 00:03:37,000
You said that you can't bring yourself
to believe what I'm saying.

36
00:03:37,000 --> 00:03:38,599
But you see, I feel the exact same way.

37
00:03:38,599 --> 00:03:41,599
I have no interest in your grandfather,
or in your money, either.

38
00:03:41,599 --> 00:03:44,400
So let's talk.
All three of us.

39
00:03:48,099 --> 00:03:49,500
Will that really be okay?

40
00:03:49,500 --> 00:03:51,900
I'd like to ask you the same thing.

41
00:04:10,899 --> 00:04:12,899
Hey, Ms. Kim!

42
00:04:17,300 --> 00:04:18,800
Um, thank you.

43
00:04:27,500 --> 00:04:28,899
You're not getting in?

44
00:04:28,899 --> 00:04:31,699
Just tell me the address.
I'll take a cab there.

45
00:04:31,699 --> 00:04:33,300
What's the point? We're going to see
the same person, anyway.

46
00:04:33,300 --> 00:04:35,199
It's a dangerous world, you know.
How am I supposed to trust you?

47
00:04:35,199 --> 00:04:37,300
Who knows what you'll do?

48
00:04:47,300 --> 00:04:49,699
What are you doing?
Aren't you going to call?

49
00:04:49,699 --> 00:04:51,651
Call?
Call who?

50
00:04:51,651 --> 00:04:54,300
Your grandpa, of course!

51
00:04:54,300 --> 00:04:57,699
Even if he may be an
accomplice in your scam.

52
00:04:57,699 --> 00:04:58,699
Scam?

53
00:04:58,699 --> 00:05:01,800
Sorry. I shouldn't just say
things like that outright, should I?

54
00:05:10,300 --> 00:05:11,899
Where are you right now?

55
00:05:12,399 --> 00:05:13,800
Why do you care?

56
00:05:13,800 --> 00:05:18,800
The woman you introduced me to wants
to have a three-way conversation.

57
00:05:18,800 --> 00:05:23,399
A three-way conversation?
Why would I need to participate in that?

58
00:05:23,399 --> 00:05:26,300
Who else would she want to talk to?
She says she doesn't know who you are!

59
00:05:26,300 --> 00:05:29,500
<i>You take care of it.
That's your mission.</i>

60
00:05:30,899 --> 00:05:32,500
Hello?

61
00:05:33,000 --> 00:05:34,399
What the...

62
00:05:35,100 --> 00:05:38,800
Lee Jae In.
It's your turn to suffer.

63
00:05:38,800 --> 00:05:42,000
Not everything in the world will
always go your way, you know.

64
00:05:44,800 --> 00:05:48,899
What is it? He freaked out when
you said that I was coming, right?

65
00:05:48,899 --> 00:05:52,199
You should do some pre-planning
if you want to pull off a scam!

66
00:05:52,199 --> 00:05:54,600
Why do you keep going on about a scam?

67
00:05:54,600 --> 00:05:56,000
Why can't you believe me?

68
00:05:56,000 --> 00:05:59,699
You don't believe me at all,
so why should I believe you?

69
00:05:59,699 --> 00:06:03,899
Go and ask anyone on the street if this
kind of situation sounds normal to them.

70
00:06:03,899 --> 00:06:05,899
Are you implying that I'm crazy?

71
00:06:05,899 --> 00:06:08,800
At least you understand that much.

72
00:06:19,699 --> 00:06:23,199
Wait, what are you doing?

73
00:06:23,199 --> 00:06:24,199
I'm not done talking to you!

74
00:06:24,199 --> 00:06:25,699
I'm done talking to you, though!

75
00:06:27,899 --> 00:06:31,300
Do you want me to wrap my arms
around you and force you inside?

76
00:06:39,899 --> 00:06:43,199
I'll seriously call the cops!
What are you doing?

77
00:06:44,199 --> 00:06:46,500
I feel like we need
a real heart-to-heart.

78
00:06:46,500 --> 00:06:48,300
I have nothing to discuss
with the likes of you.

79
00:06:59,699 --> 00:07:01,000
What's this?

80
00:07:07,300 --> 00:07:10,300
<i>[Lee Jae In - Businessman CEO
of SH Alpegia Hotel]</i>

81
00:07:14,600 --> 00:07:16,399
Do you believe me now?

82
00:07:17,100 --> 00:07:20,100
Wait, what?

83
00:07:28,600 --> 00:07:30,699
I'll admit that you two
do look a bit similar.

84
00:07:30,699 --> 00:07:33,100
It's not that we look alike!
That's really me!

85
00:07:35,300 --> 00:07:36,500
Geez.

86
00:07:39,899 --> 00:07:42,800
Was the person calling you... the CEO?

87
00:07:42,800 --> 00:07:44,300
Yeah.

88
00:07:46,899 --> 00:07:51,399
But, um, do you really have
a debt to repay to that teacher?

89
00:07:51,399 --> 00:07:53,199
I told you, yes!

90
00:07:53,199 --> 00:07:56,100
I'm not exactly sure what kind
of debt that may be, but...

91
00:07:56,100 --> 00:07:58,199
can't you repay her in another way?

92
00:07:58,199 --> 00:08:01,500
Throwing CEO Lee at her
isn't paying back her kindness.

93
00:08:01,500 --> 00:08:03,899
You're making her into your enemy.

94
00:08:04,399 --> 00:08:07,699
- Is my grandson really that bad?
- Um, of course not!

95
00:08:07,699 --> 00:08:10,800
The fact that he's not
lacking at all is the problem!

96
00:08:10,800 --> 00:08:11,899
In assholeish-ness, anyway.

97
00:08:11,899 --> 00:08:13,799
That's why he needs Da Hyun.

98
00:08:13,799 --> 00:08:16,799
You're well aware of the fact that
Jae In is a real piece of work, right?

99
00:08:16,799 --> 00:08:20,000
And the women he dated
are all just like him!

100
00:08:23,799 --> 00:08:27,399
All he cares about is money.

101
00:08:27,399 --> 00:08:30,700
I can't let him live like that forever.

102
00:08:32,299 --> 00:08:34,700
And I'm running out of time.

103
00:08:34,700 --> 00:08:42,600
So that chairman left me his fortune,
but I have to marry you to get it.

104
00:08:42,600 --> 00:08:45,000
That's what you're telling me, right?

105
00:08:45,000 --> 00:08:47,500
More or less.

106
00:08:47,500 --> 00:08:48,799
Yeah.

107
00:08:51,299 --> 00:08:52,899
Um, I mean, yes.

108
00:08:53,899 --> 00:08:56,500
I don't particularly like
this condition.

109
00:08:58,100 --> 00:09:01,000
Do you know how much our
company's shares cost right now?

110
00:09:01,000 --> 00:09:04,200
The dividends you'd get per year
that you're apparently not interested in

111
00:09:04,200 --> 00:09:06,700
are more than what you'd make on your
salary in the next ten years, Teach!

112
00:09:06,700 --> 00:09:08,100
Teach-er.

113
00:09:09,200 --> 00:09:14,899
You've been calling me "Teach" or "Woman"
all this time. Call me "Teacher."

114
00:09:14,899 --> 00:09:17,299
Teach, Teacher, whatever.
It's all just a way of referring to you.

115
00:09:17,299 --> 00:09:19,000
It'd be much better if we didn't
waste time talking about that

116
00:09:19,000 --> 00:09:20,799
and talked about what to
do about all of this, instead.

117
00:09:20,799 --> 00:09:22,600
That's your opinion.

118
00:09:24,100 --> 00:09:26,299
I'm starting to wonder which
one of us is the crazy one here.

119
00:09:26,299 --> 00:09:29,932
There's so much money at stake here!
But you don't care?

120
00:09:29,932 --> 00:09:31,100
Look here!

121
00:09:31,700 --> 00:09:34,700
Um... what was your name, again?

122
00:09:37,200 --> 00:09:40,000
- Lee Jae In.
- Yes, Mr. Lee Jae In.

123
00:09:40,000 --> 00:09:45,399
It's not like my house is getting
knocked down tomorrow, or I'm starving.

124
00:09:45,399 --> 00:09:48,299
I'm getting by just fine even without
this nuisance of an inheritance

125
00:09:48,299 --> 00:09:50,799
so why would I need to get married?

126
00:09:50,799 --> 00:09:51,799
And to you, of all people?

127
00:09:51,799 --> 00:09:54,500
Because if you don't marry me,
it'll get really annoying.

128
00:09:54,500 --> 00:09:56,799
Do you think that you'll
be able to handle that?

129
00:09:56,799 --> 00:09:58,600
What... do you mean by that?

130
00:09:58,600 --> 00:10:05,700
You'll get incredible power at SH Group,
as long as you just marry someone.

131
00:10:05,700 --> 00:10:08,799
Tons of people will try to seize this
chance, and swarm over like flies.

132
00:10:08,799 --> 00:10:12,299
It'd be fine to just choose
someone among that crowd, I guess, but--

133
00:10:12,299 --> 00:10:14,299
Oh, that doesn't sound bad.

134
00:10:14,299 --> 00:10:20,100
But you know, you definitely
won't be the one I choose to marry.

135
00:10:21,600 --> 00:10:26,700
Your grandfather sure is strong-willed.

136
00:10:26,700 --> 00:10:31,799
It'll take more than his wealth
to cover up your nasty temper.

137
00:10:31,799 --> 00:10:36,399
That's why he's going out of his way like
this to try and give you a chance at it.

138
00:10:37,200 --> 00:10:39,700
By adding you into the deal
as a free gift.

139
00:10:40,299 --> 00:10:42,000
<i>Some cold water, please.</i>

140
00:10:42,000 --> 00:10:43,299
With ice.

141
00:10:43,299 --> 00:10:45,399
And please put some ice in it.

142
00:10:45,399 --> 00:10:47,100
<i>Yes, sir.</i>

143
00:10:49,000 --> 00:10:52,100
Stop stomping around.
It's driving me crazy.

144
00:10:52,100 --> 00:10:53,500
Are things not going well
with the teacher, or something?

145
00:10:53,500 --> 00:10:55,100
Is she too much to handle?

146
00:10:55,100 --> 00:10:56,899
That doesn't even begin to cover it.

147
00:10:56,899 --> 00:10:58,899
She's literally pouring bucketloads
of salt into my wound!

148
00:10:58,899 --> 00:11:01,200
She's a real pro at pissing people off!

149
00:11:01,200 --> 00:11:06,100
She's really cunning, sly,
and is super brazen.

150
00:11:06,600 --> 00:11:10,600
So in other words, she's smart,
clever, and fearless?

151
00:11:10,600 --> 00:11:11,799
Hey!

152
00:11:14,500 --> 00:11:15,799
Thank you.

153
00:11:23,600 --> 00:11:25,399
Sheesh.

154
00:11:28,299 --> 00:11:31,200
Do you know what
that wily fox said to me?

155
00:11:31,200 --> 00:11:32,700
No, what'd she say?

156
00:11:32,700 --> 00:11:34,100
She called me a "free gift."

157
00:11:35,899 --> 00:11:37,200
Free gift!

158
00:11:39,200 --> 00:11:42,399
Wait, so what happened in the end?

159
00:11:42,399 --> 00:11:45,700
She said I can do whatever,
but she doesn't want any part in it.

160
00:11:45,700 --> 00:11:48,100
She's not interested
in marriage or money.

161
00:11:48,100 --> 00:11:52,000
Wait, then...
what are you going to do?

162
00:11:52,000 --> 00:11:55,799
What else? Just keep pressuring her
until she crumbles!

163
00:11:55,799 --> 00:11:58,100
Have you ever seen me lose to anyone?

164
00:12:12,600 --> 00:12:15,200
<i>[Talking About Management
by Lee Gyu Chul]</i>

165
00:12:16,100 --> 00:12:19,899
Do you know me?
Why did you do this to me?

166
00:12:19,899 --> 00:12:23,899
Why didn't you just give me money,
instead? That would've been better.

167
00:12:24,799 --> 00:12:27,399
Why'd you have to throw
your grandson into the deal, too?

168
00:12:32,799 --> 00:12:35,000
Oh my gosh!
What brings you here?

169
00:12:35,899 --> 00:12:37,799
Why aren't you wearing a hat?

170
00:12:38,600 --> 00:12:40,500
I'm not famous enough to be doing that.

171
00:12:40,500 --> 00:12:42,100
Nobody recognizes me.

172
00:12:42,100 --> 00:12:44,700
No, you're definitely famous!

173
00:12:44,700 --> 00:12:46,700
You're starting to be talked
about on the web now, too!

174
00:12:46,700 --> 00:12:48,000
Hurry up and put a hat on.

175
00:12:48,000 --> 00:12:50,600
You're the only one who
thinks that, Teacher.

176
00:12:50,600 --> 00:12:52,299
No, no.
Put this on.

177
00:12:53,600 --> 00:12:57,799
And I told you, don't call me "teacher."
I'm the president of your fanclub!

178
00:12:57,799 --> 00:12:59,700
Keep it down!

179
00:13:03,200 --> 00:13:05,100
You read all the books
from last time, right?

180
00:13:05,100 --> 00:13:08,200
The GED exam is coming up,
so let's finish these up by then.

181
00:13:08,200 --> 00:13:11,799
Don't worry. I'm diligently
listening to online lectures, too.

182
00:13:11,799 --> 00:13:13,899
Good!
Let's go.

183
00:13:14,500 --> 00:13:20,000
Oh yeah. As the president, I'll buy you
something yummy on behalf of your fans.

184
00:13:20,000 --> 00:13:22,700
Let's go!
Let's go!

185
00:13:42,299 --> 00:13:46,100
Sorry, but I don't play losing games.

186
00:13:59,799 --> 00:14:01,200
That's it for today!

187
00:14:01,200 --> 00:14:06,700
Don't make your parents worry by
studying too much at home, okay?

188
00:14:06,700 --> 00:14:09,299
And make sure to listen
to a lot of classical music!

189
00:14:09,299 --> 00:14:11,500
And to those taking the special class,
make your way to the other classroom.

190
00:14:11,500 --> 00:14:14,200
And, to all my friends in
the third grade Green class...

191
00:14:14,200 --> 00:14:15,200
Today, you all did a...

192
00:14:15,200 --> 00:14:17,899
- Great job!
- Great job!

193
00:14:17,899 --> 00:14:20,299
Great job!

194
00:14:20,299 --> 00:14:21,899
Clean up after yourselves, and let's go!

195
00:14:35,100 --> 00:14:36,700
Is he not here yet?

196
00:14:38,500 --> 00:14:41,399
- No, I am.
- Oh my gosh!

197
00:14:46,899 --> 00:14:50,799
Geez, he scared me!

198
00:14:55,200 --> 00:14:56,700
Thank you.

199
00:15:00,600 --> 00:15:02,000
Why do you keep calling me out?

200
00:15:02,000 --> 00:15:07,500
I told you last time, but I really have
no interest in you, or the SH Group.

201
00:15:07,500 --> 00:15:08,600
Deal with that mess among yourselves.

202
00:15:08,600 --> 00:15:10,799
That's not possible.

203
00:15:12,799 --> 00:15:15,799
Do you know what a three-legged race is?

204
00:15:15,799 --> 00:15:20,299
You and I have been tied
together by my grandpa.

205
00:15:21,899 --> 00:15:23,200
With what?

206
00:15:23,700 --> 00:15:25,000
Marriage.

207
00:15:26,399 --> 00:15:29,000
I thought you said that
you wouldn't go through with it!

208
00:15:29,600 --> 00:15:31,000
Do you want to do it, Teach?

209
00:15:31,000 --> 00:15:33,799
- Are you joking?
- That's a relief.

210
00:15:33,799 --> 00:15:36,500
Thankfully, Grandpa has backed off
from the idea of marriage.

211
00:15:36,500 --> 00:15:38,100
He said that he'd rewrite
his will in six months

212
00:15:38,100 --> 00:15:42,100
if the two of us consort with
one another, for real.

213
00:15:42,600 --> 00:15:44,399
Consort with one another?

214
00:15:44,899 --> 00:15:48,200
What do I have to gain from that?

215
00:15:48,200 --> 00:15:54,500
I told you already! This is the best way
for both of us to escape this mess.

216
00:15:54,500 --> 00:15:57,299
I barely managed to get
him to agree to this, as is!

217
00:15:57,299 --> 00:15:59,299
So, am I supposed to be
thankful about that?

218
00:15:59,299 --> 00:16:01,799
Are you saying that you'll
get married after all, then?

219
00:16:06,100 --> 00:16:07,500
Want to?

220
00:16:13,100 --> 00:16:14,700
This is a joke, right?

221
00:16:14,700 --> 00:16:17,299
You don't need to glare at me like that.
I don't want to marry you, either.

222
00:16:17,299 --> 00:16:19,500
But, why do you keep asking to see me?

223
00:16:19,500 --> 00:16:21,799
He said that he'll be okay with it
even if we don't get married.

224
00:16:21,799 --> 00:16:25,500
However, we must really date,
as a man and woman

225
00:16:25,500 --> 00:16:27,500
- for the next six months--
- Excuse me.

226
00:16:27,500 --> 00:16:29,600
I don't know what your
grandfather's thoughts may be

227
00:16:29,600 --> 00:16:32,899
but I have no intention of dating you.

228
00:16:38,100 --> 00:16:40,500
Want me to tell you how much SH Group
makes in sales every year?

229
00:16:40,500 --> 00:16:42,299
Do you want me to tell you
how much my monthly salary is?

230
00:16:42,299 --> 00:16:44,700
- Why would I?
- Exactly.

231
00:16:44,700 --> 00:16:47,400
Why should I care about
SH Group's yearly profits

232
00:16:47,400 --> 00:16:49,500
when I get confused
about my own yearly salary?

233
00:16:50,000 --> 00:16:53,500
You have the potential to
inherit a huge fortune, here.

234
00:16:53,500 --> 00:16:55,099
You really don't care?

235
00:16:56,799 --> 00:17:00,000
If I really am inheriting a
huge fortune, I am interested.

236
00:17:00,000 --> 00:17:05,200
But if I have to marry you to get
that money, I'll have to refuse.

237
00:17:05,200 --> 00:17:07,000
It's not like I'm starving, here.

238
00:17:07,000 --> 00:17:11,000
I don't really want to get married just
to inherit someone else's fortune.

239
00:17:11,000 --> 00:17:14,000
And I don't want to twist up my
life over something like this, either.

240
00:17:14,000 --> 00:17:17,000
So talk it over with
your grandfather nicely.

241
00:17:17,000 --> 00:17:20,400
And tell him that I'm
completely opposed to this.

242
00:17:25,799 --> 00:17:27,200
So you really do mean it.

243
00:17:28,799 --> 00:17:33,500
Yes. I've really meant it
since the start.

244
00:18:10,299 --> 00:18:16,099
It kind of is a shame, but it's not
like I'd get all that money anyway.

245
00:18:16,099 --> 00:18:20,400
Those people seem super wicked.
Who knows what they're planning?

246
00:18:26,500 --> 00:18:27,700
Yeah, Mom?

247
00:18:29,200 --> 00:18:31,400
All right, all right.

248
00:18:31,400 --> 00:18:34,200
Wow, I'm going to end up meeting all
the herbalists in Korea, at this rate!

249
00:18:36,500 --> 00:18:41,700
No, no. I'm just saying that I'll
definitely end up marrying an herbalist.

250
00:18:41,700 --> 00:18:42,900
Yeah.

251
00:18:51,799 --> 00:18:54,700
And I don't want to twist up my
life over something like this, either.

252
00:18:54,700 --> 00:18:58,299
So talk it over with
your grandfather nicely.

253
00:18:58,299 --> 00:19:01,700
And tell him that I'm
completely opposed to this.

254
00:19:03,099 --> 00:19:08,299
<i>Then SH Group will become Tae Ha's.
Of course, that includes SH Mall.</i>

255
00:19:08,299 --> 00:19:11,099
<i>There's no reason why I have to
make you my successor.</i>

256
00:19:14,900 --> 00:19:17,400
Listen, Teacher.

257
00:19:19,599 --> 00:19:25,099
You're making things complicated
for me by acting like this.

258
00:19:28,599 --> 00:19:29,900
Jae In is quiet, right?

259
00:19:29,900 --> 00:19:31,500
Yes, sir.

260
00:19:31,500 --> 00:19:35,000
So he got rejected.
What a relief. I knew that would happen.

261
00:19:35,000 --> 00:19:37,099
No way.
The CEO, rejected?

262
00:19:37,099 --> 00:19:39,500
I'm growing to hate him
more and more, too.

263
00:19:39,500 --> 00:19:41,099
So I'm sure Da Hyun thinks
even less of him than I do.

264
00:19:42,599 --> 00:19:47,400
All right then, when should
I tell Tae Ha about this?

265
00:19:48,099 --> 00:19:49,400
Call Attorney Park.

266
00:19:50,299 --> 00:19:52,200
N-now, sir?

267
00:19:53,599 --> 00:19:54,700
This is bad.

268
00:19:59,799 --> 00:20:02,599
Dongyang Hotel's restaurant.
Noh Hyun Woo.

269
00:20:03,500 --> 00:20:05,500
Aw man, I hate this so much.

270
00:20:05,500 --> 00:20:08,200
It's the weekend, and
the weather is so nice, too.

271
00:20:08,200 --> 00:20:13,299
And yet, I have to waste it by going to a
marriage meeting with some stranger.

272
00:20:17,799 --> 00:20:19,299
All right, then.
Let's go.

273
00:20:38,099 --> 00:20:40,500
- Did I keep you waiting?
- No, I just got here.

274
00:20:40,500 --> 00:20:42,900
Wow, it sure is hard to see my son!

275
00:20:55,500 --> 00:21:00,000
I heard your grandpa called you over.
How did he blackmail you this time?

276
00:21:00,700 --> 00:21:04,000
Nothing much.
I can figure it out.

277
00:21:09,200 --> 00:21:14,400
Are you... keeping in touch with
your father's sister, in Canada?

278
00:21:18,000 --> 00:21:20,500
- Um, Mr. Noh?
- Oh, yes!

279
00:21:20,500 --> 00:21:22,700
- Hello, Ms. Da Hyun.
- Yes, hello.

280
00:21:22,700 --> 00:21:27,299
Yes. I called her once last week.
She seems to be much healthier now.

281
00:21:27,299 --> 00:21:29,000
I see.

282
00:21:29,000 --> 00:21:30,680
- There wasn't any traffic?
- No. It was great.

283
00:21:30,680 --> 00:21:34,700
Oh, really? It seemed like
there was some unexpected traffic.

284
00:21:35,299 --> 00:21:38,000
Do you know them?
Are they your friends?

285
00:21:38,900 --> 00:21:41,599
Um, yes.
Somewhat.

286
00:21:42,299 --> 00:21:45,599
- I heard that you're an herbalist.
- Oh, yes.

287
00:21:45,599 --> 00:21:49,200
Looks like it's a marriage meeting.
They look good together.

288
00:21:52,900 --> 00:21:55,000
You're not going to get married?

289
00:21:55,000 --> 00:21:58,000
I'm getting tired of waiting
for you to get married!

290
00:21:58,000 --> 00:21:59,799
I'll get to it when the time comes.

291
00:21:59,799 --> 00:22:02,000
Hurry up and come back to
SH Group, already.

292
00:22:03,200 --> 00:22:05,500
You're the Chairman's eldest grandson.

293
00:22:09,500 --> 00:22:10,700
I know that.

294
00:22:10,700 --> 00:22:15,099
But I don't want to pop in there just
because I'm the chairman's grandson.

295
00:22:15,099 --> 00:22:16,200
I'll go back to SH Group
when the time comes.

296
00:22:16,200 --> 00:22:20,500
You're not just Chairman Lee's grandson.

297
00:22:20,500 --> 00:22:22,900
My father is the Chairman
of Daehan Electric.

298
00:22:22,900 --> 00:22:26,900
Your grandfather and I would
provide you with anything you need.

299
00:22:26,900 --> 00:22:29,799
- Please, eat.
- You know that, right?

300
00:22:32,799 --> 00:22:37,500
Um, Mother.
I think I'll have to go.

301
00:22:39,400 --> 00:22:41,400
Is there a problem?

302
00:22:41,400 --> 00:22:45,400
Yes.
A huge problem.

303
00:22:45,400 --> 00:22:49,700
All right, then.
I'll go see Madam Kang, since I'm here.

304
00:22:49,700 --> 00:22:51,599
Take care of your problem, then.

305
00:22:59,900 --> 00:23:03,500
Are you really close with that PD
from the broadcasting company?

306
00:23:03,500 --> 00:23:06,099
Yes. We went to school together,
so we keep in touch.

307
00:23:06,099 --> 00:23:08,299
- Oh, really?
- Yes.

308
00:23:08,299 --> 00:23:09,900
Ms. Kim Da Hyun?

309
00:23:10,500 --> 00:23:11,700
Oh my! Huh?

310
00:23:11,700 --> 00:23:14,000
Mr. Lee?
What are you doing here?

311
00:23:14,000 --> 00:23:16,599
I thought that you and I still
had some loose ends to tie up.

312
00:23:17,099 --> 00:23:19,500
So isn't it cheating for you
to be here, doing this?

313
00:23:19,500 --> 00:23:21,299
Cheating?
What are you talking about?

314
00:23:24,500 --> 00:23:26,500
We talked about this
more than enough last time!

315
00:23:26,500 --> 00:23:28,400
- I told you, I don't want to marry--
- Exactly!

316
00:23:29,599 --> 00:23:32,099
Let's push back our wedding
for now, just as you said.

317
00:23:32,099 --> 00:23:33,799
I'd be fine with us getting
married tomorrow, but...

318
00:23:36,200 --> 00:23:38,099
Oh, yes.
I'm sorry.

319
00:23:38,099 --> 00:23:41,599
When you date someone,
tiny misunderstandings pile up, and...

320
00:23:41,599 --> 00:23:43,500
Well, at any rate,
dating sure is tough.

321
00:23:43,500 --> 00:23:44,799
Um, we're not like that at all.

322
00:23:44,799 --> 00:23:47,000
What are you doing?

323
00:23:47,000 --> 00:23:50,299
What do you mean? Do you want
to get married, then, like Grandpa said?

324
00:23:50,299 --> 00:23:52,799
I thought we didn't have to!
I thought we just had to date--

325
00:23:55,799 --> 00:23:57,900
I don't think I should be here.

326
00:24:00,099 --> 00:24:02,700
- Wait!
- Have a safe trip home!

327
00:24:07,000 --> 00:24:10,400
Shall we start over, then?

328
00:24:12,099 --> 00:24:13,799
Oh, I'll move over there.

329
00:24:15,900 --> 00:24:19,200
Oh my. He didn't touch his food at all.

330
00:24:26,299 --> 00:24:28,299
Do you want to finish that
before we start talking?

331
00:24:33,200 --> 00:24:35,200
Are you out of your mind?
Why did you do that?

332
00:24:35,200 --> 00:24:38,500
No, I'm not.
Like I said, I want to start over.

333
00:24:38,500 --> 00:24:44,000
Start what over?
When did we ever start anything?

334
00:24:44,000 --> 00:24:46,200
Well, we can just get started
right now, then.

335
00:24:46,200 --> 00:24:50,400
Hey, wait!
Wait!

336
00:24:51,400 --> 00:24:56,500
<i>You were dating someone? Why didn't
you bring him over sooner, then?</i>

337
00:24:57,799 --> 00:25:02,500
I told you, no! I don't know!
I don't even know the guy!

338
00:25:02,500 --> 00:25:04,599
Well, it's not true
that you don't know me.

339
00:25:07,700 --> 00:25:10,799
Well, um, a guy who's slightly crazy--

340
00:25:12,000 --> 00:25:18,299
No way!
Well, no, he's not dangerous.

341
00:25:18,299 --> 00:25:20,900
All right. All right. I'll go on
another marriage meeting next week.

342
00:25:20,900 --> 00:25:22,799
There. Happy now?

343
00:25:22,799 --> 00:25:25,900
I'm telling you, it's not true!

344
00:25:25,900 --> 00:25:27,900
Okay. Bye.

345
00:25:31,099 --> 00:25:34,000
Think of an excuse
that'll make me forgive you.

346
00:25:34,000 --> 00:25:35,900
I have no intention of doing that.

347
00:25:37,299 --> 00:25:39,200
Why do you have to go out of
your way to date another man?

348
00:25:39,200 --> 00:25:41,000
And you're even going on
marriage meetings to find someone!

349
00:25:41,000 --> 00:25:43,500
You can just solve everything
by going out with me.

350
00:25:43,500 --> 00:25:46,700
Because that's complicated!
And it's weird!

351
00:25:46,700 --> 00:25:49,500
I'll make it uncomplicated!
And what's so weird about it?

352
00:25:49,500 --> 00:25:53,500
You need a man, and I need you.

353
00:25:54,799 --> 00:25:56,599
Teach...er.

354
00:25:57,599 --> 00:26:01,099
Don't people say that that
you're insanely stubborn?

355
00:26:02,099 --> 00:26:05,099
- Sometimes.
- That's not a compliment!

356
00:26:06,099 --> 00:26:09,700
Fine.
Let's give it a shot.

357
00:26:11,000 --> 00:26:14,200
He wants me to give Tae Ha
a copy of the will, too?

358
00:26:17,200 --> 00:26:18,500
By when?

359
00:26:18,500 --> 00:26:20,599
Like, yesterday.
Right now.

360
00:26:26,400 --> 00:26:31,400
Hey, Deputy Han. What did the
American Chamber of Commerce say?

361
00:26:31,400 --> 00:26:35,500
The CEO?
Well, um, the situation is... um.

362
00:26:35,500 --> 00:26:39,000
You know the phrase,
"hopeless endeavor?" Like that.

363
00:26:39,000 --> 00:26:41,099
I feel like I'm going to go insane.

364
00:26:41,599 --> 00:26:44,200
Okay. Call me later, okay?

365
00:26:52,099 --> 00:26:53,799
I'll say this again.

366
00:26:53,799 --> 00:26:58,099
What my grandpa wants is for us
to see each other for six months.

367
00:26:58,099 --> 00:27:02,599
This is the best way.
And it's the most ideal option we have.

368
00:27:02,599 --> 00:27:04,500
In my opinion, it's not the
most ideal option "we" have.

369
00:27:04,500 --> 00:27:06,799
It seems like this benefits you
more than anyone else.

370
00:27:06,799 --> 00:27:10,299
You'll inherit the fortune
just for dating me.

371
00:27:10,299 --> 00:27:12,200
I have to give up on
getting stocks and a husband.

372
00:27:12,200 --> 00:27:15,099
And to make matters worse,
I have to tolerate you for six months.

373
00:27:17,599 --> 00:27:19,900
You're quite clever.

374
00:27:19,900 --> 00:27:21,799
Well, I haven't ever had
someone tell me that I'm dumb.

375
00:27:21,799 --> 00:27:23,500
You said that you had no
interest in the fortune, though!

376
00:27:23,500 --> 00:27:25,799
And I'm not that interested
in a husband, either.

377
00:27:25,799 --> 00:27:28,500
Also, a deal is supposed
to be fair for both parties!

378
00:27:32,099 --> 00:27:38,099
Well, you know, you're not
losing out in any way.

379
00:27:38,099 --> 00:27:40,200
If everything gets
resolved well in six months

380
00:27:40,200 --> 00:27:44,000
I'll give you a sum equivalent to three
years worth of dividends, in cash.

381
00:27:44,000 --> 00:27:45,799
It'll be much easier for you to date me

382
00:27:45,799 --> 00:27:48,500
than to get tangled up with
stocks and director meetings.

383
00:27:48,500 --> 00:27:51,799
Oh, and you won't have to go on marriage
meetings to meet weird men, either.

384
00:27:51,799 --> 00:27:53,900
Really, now?

385
00:27:53,900 --> 00:27:56,700
I feel like this option is actually a lot
more complicated than the first one.

386
00:27:56,700 --> 00:28:00,299
If I get married, I don't need
to accept cash from you.

387
00:28:00,299 --> 00:28:03,500
And the people who get tangled up with me
because of my shares will be nice to me.

388
00:28:03,500 --> 00:28:06,799
I'll have tons of money, and have a lot
of shares. And I'll have a husband, too.

389
00:28:06,799 --> 00:28:09,900
So why should I choose the difficult
path instead of this easy path?

390
00:28:10,400 --> 00:28:16,200
Oh. As for that man from earlier,
he wasn't a weird person.

391
00:28:17,900 --> 00:28:21,900
Fine. What do you want, then, Teach?

392
00:28:21,900 --> 00:28:25,900
Let me make this clear now.
I'm not marrying you.

393
00:28:25,900 --> 00:28:29,000
Thanks. I feel exactly the same way.

394
00:28:29,000 --> 00:28:31,799
- Firstly--
- "Firstly?"

395
00:28:31,799 --> 00:28:35,200
You have multiple things that you want?
Aren't you being too greedy?

396
00:28:35,200 --> 00:28:39,000
Firstly, call me Ms. Kim Da Hyun,
or Teacher.

397
00:28:39,000 --> 00:28:41,200
I've mentioned this
several times already.

398
00:28:41,200 --> 00:28:42,799
That's easy enough, right?

399
00:28:42,799 --> 00:28:44,500
Damn it.

400
00:28:44,500 --> 00:28:46,900
And please watch your language.

401
00:28:48,900 --> 00:28:54,299
Fine, Ms. Kim Da Hyun.
Teacher Kim Da Hyun.

402
00:28:59,700 --> 00:29:01,500
This is Lee Jae In.

403
00:29:02,099 --> 00:29:05,400
Negotiate with AmCham Korea
regarding the meeting, first!

404
00:29:05,400 --> 00:29:09,900
If you miss this chance, you'd better
have a letter of resignation for me.

405
00:29:16,200 --> 00:29:17,500
What is it?

406
00:29:17,900 --> 00:29:23,099
He... told me to write
my letter of resignation.

407
00:29:23,099 --> 00:29:26,500
Did he finally realize that you're
working for the chairman, too?

408
00:29:29,299 --> 00:29:32,500
Wait. Were you the one
who told him about that?

409
00:29:32,500 --> 00:29:35,700
Are you out of your mind?
Who knows what'll happen if he finds out?

410
00:29:36,299 --> 00:29:40,400
You know, I'm his friend and all,
but he's super scary.

411
00:29:40,400 --> 00:29:42,200
Me too.
Me too.

412
00:29:42,200 --> 00:29:46,400
Why is it that everyone in his family
is such an asshole?

413
00:29:46,900 --> 00:29:48,299
I know, right?

414
00:29:48,299 --> 00:29:50,200
I'm genuinely curious.

415
00:29:54,799 --> 00:29:57,900
So, what do you want?
Money? SH Group?

416
00:29:57,900 --> 00:30:03,299
Or something else? Just name it.
You have to tell me for us to negotiate.

417
00:30:03,299 --> 00:30:04,700
You can give me anything I want?

418
00:30:05,200 --> 00:30:09,200
Looks like you're loaded, then.
Just how much can you offer me?

419
00:30:09,200 --> 00:30:12,700
Hmm, it's much easier to talk to
you now that you're being frank.

420
00:30:13,299 --> 00:30:15,400
Well, then, tell me.
What is it that you want?

421
00:30:15,400 --> 00:30:19,700
A brand new technology system
for my school's library.

422
00:30:20,500 --> 00:30:23,200
There were a lot of budget cuts
from the Board of Education this year.

423
00:30:23,200 --> 00:30:25,299
And it'd be nice if you threw in
a lot of nice books, to boot.

424
00:30:25,299 --> 00:30:28,500
And a nice indoor gym, so the kids
can play in there on rainy days.

425
00:30:28,500 --> 00:30:30,299
And it'd be nice to have
a school bus, too.

426
00:30:30,299 --> 00:30:33,700
Oh, and it'd be nice to have a big
mirror in the multipurpose room, too.

427
00:30:33,700 --> 00:30:36,900
I'm the academic adviser for
the dance club, you see.

428
00:30:37,900 --> 00:30:38,900
Anything else?

429
00:30:38,900 --> 00:30:42,099
Can you get me some stars from the sky,
so we can make the science room brighter?

430
00:30:42,099 --> 00:30:43,500
That'll be hard, since I think
that the SH Space Program

431
00:30:43,500 --> 00:30:47,000
hasn't broken ground yet.

432
00:30:47,000 --> 00:30:49,000
- Anything else?
- Um, excuse me.

433
00:30:49,000 --> 00:30:51,200
That was a joke.

434
00:30:51,200 --> 00:30:55,000
Well, I'm not joking.
Is that all you want?

435
00:30:57,299 --> 00:31:00,900
The school will take care of everything
else, so I don't need anything.

436
00:31:00,900 --> 00:31:04,500
But can you get my Ji Su
a better management company?

437
00:31:04,500 --> 00:31:06,400
What?
Who?

438
00:31:06,400 --> 00:31:09,700
He's a fantastic singer, and actor!

439
00:31:09,700 --> 00:31:11,799
On top of that, he's so handsome!

440
00:31:11,799 --> 00:31:15,200
If you just help him out a little,
he'll definitely become a star.

441
00:31:15,900 --> 00:31:18,200
His management company
is kind of crappy--

442
00:31:18,200 --> 00:31:20,599
I mean, um, it's pretty tyrannical.

443
00:31:20,599 --> 00:31:23,799
He's pretty much stuck in a slave
contract, so he can't even leave.

444
00:31:23,799 --> 00:31:25,700
It might even take a lawsuit.

445
00:31:25,700 --> 00:31:27,700
I mean, how could they ask him for 300
million won for breaking the contract?

446
00:31:27,700 --> 00:31:29,700
Isn't that ridiculous?

447
00:31:30,500 --> 00:31:34,500
Um, does your attorney know
how to handle cases like these?

448
00:31:34,500 --> 00:31:35,900
Is that really all you want?

449
00:31:36,500 --> 00:31:37,900
- You mean it?
- Yes!

450
00:31:37,900 --> 00:31:39,299
I'm not joking this time.

451
00:31:39,299 --> 00:31:43,099
This is a lot more important to me
than some capricious will.

452
00:31:44,099 --> 00:31:47,200
A young kid's life depends on this.

453
00:31:47,200 --> 00:31:49,200
That's what you think,
Teach...

454
00:31:49,200 --> 00:31:52,000
er.

455
00:31:53,000 --> 00:31:55,700
What else?
Is that all?

456
00:31:55,700 --> 00:32:01,900
No. If you'd just do that for me,
I can agree to anything you want.

457
00:32:01,900 --> 00:32:05,500
Not including getting married
to you, though, just like you said.

458
00:32:05,500 --> 00:32:07,200
Okay.

459
00:32:07,200 --> 00:32:10,000
The contract is in place.
Don't change your mind later.

460
00:32:10,000 --> 00:32:12,500
Fine.
Let's write up a contract.

461
00:32:12,500 --> 00:32:13,900
What?

462
00:32:18,299 --> 00:32:19,799
Goodbye.

463
00:32:20,299 --> 00:32:21,799
Teacher!

464
00:32:28,799 --> 00:32:29,799
Oh!

465
00:32:29,799 --> 00:32:32,000
- Are you doing this on purpose?
- What?

466
00:32:32,000 --> 00:32:34,799
Why do you always leave things behind?

467
00:32:47,304 --> 00:32:57,304
KoreanDramaX.com

468
00:33:08,500 --> 00:33:12,400
<i>If I just date him, Ji Su's issue
with his company will be solved.</i>

469
00:33:12,400 --> 00:33:15,099
<i>She's a fox.
And a cunning one, at that.</i>

470
00:33:15,099 --> 00:33:16,500
<i>Don't make a copy of that last page.</i>

471
00:33:16,500 --> 00:33:17,500
<i>You were three minutes late.</i>

472
00:33:17,500 --> 00:33:19,299
<i>I guess you really do
have a lot of money.</i>

473
00:33:19,299 --> 00:33:21,799
<i>You forgot about all of that overnight.</i>

474
00:33:21,799 --> 00:33:24,500
<i>I'm not sure.
It's my first time seeing them.</i>

475
00:33:24,500 --> 00:33:26,500
<i>Does Jae In have a girlfriend?</i>

476
00:33:27,000 --> 00:33:30,900
<i>Don't act like you know me.
Don't even look my way.</i>

477
00:33:30,900 --> 00:33:33,299
<i>I thought you said that you weren't
embarrassed of me, at the very least.</i>

478
00:33:33,299 --> 00:33:35,200
<i>Close your eyes.</i>

479
00:33:35,200 --> 00:33:37,400
<i>And I will kiss you.</i>

480
00:33:37,400 --> 00:33:40,299
<i>[One Percent of Something]</i>

