1
00:00:33,208 --> 00:00:37,508
- Stan, where'd you get that black eye?
- Nothing, I mean nowhere.

2
00:00:37,678 --> 00:00:41,118
- Your sister beat you up again?
- No!

3
00:00:41,288 --> 00:00:43,148
Your sister kicked your ass!

4
00:00:43,318 --> 00:00:46,988
She's pissed she got headgear.
She takes it out on me.

5
00:00:47,158 --> 00:00:49,528
You get your butt kicked by a girl.

6
00:00:49,698 --> 00:00:53,958
I'd never let a woman kick my ass.
If she tried anything, I'd be:

7
00:00:54,128 --> 00:00:58,328
"Get your bitch-ass back in the
kitchen and make me some pie!"

8
00:00:58,508 --> 00:01:00,408
My parents don't believe me.

9
00:01:00,568 --> 00:01:03,698
They think she's innocent.
She's an evil bitch.

10
00:01:03,878 --> 00:01:10,108
Be a man, Stan. Just say, "Hey, woman!
You shut your mouth and make babies!"

11
00:01:10,618 --> 00:01:12,408
What's that elephant doing?

12
00:01:12,588 --> 00:01:15,488
You mean this one?
He's my new pet elephant.

13
00:01:15,658 --> 00:01:19,148
- Where'd you get that?
- Mail order from Africa.

14
00:01:19,328 --> 00:01:22,228
They said it'd take 6 weeks,
but it only took 3!

15
00:01:22,398 --> 00:01:24,258
- Wow, that's cool!
- It's not!

16
00:01:24,428 --> 00:01:28,728
Mom won't let him in the house!
His poop is bigger than our couch.

17
00:01:28,898 --> 00:01:33,898
That's why my mom got me a potbellied
pig because its poop is small.

18
00:01:35,608 --> 00:01:38,478
Yeah, but pigs aren't smart
like elephants.

19
00:01:39,478 --> 00:01:43,278
Hey! Wait a minute!
What is that thing?

20
00:01:44,748 --> 00:01:47,048
This is the new retarded kid.

21
00:01:47,218 --> 00:01:51,448
Oh, I'm sorry, little girl.
But you still can't get on.

22
00:01:51,618 --> 00:01:55,218
You have to take the special-ed bus!

23
00:01:56,828 --> 00:02:00,358
Looks like you're not welcome
anywhere. See you!

24
00:02:00,698 --> 00:02:05,398
If a woman ever gave me crap,
I'd say, "Hey, you go do my laundry!"

25
00:02:05,568 --> 00:02:08,408
- Sit down, kid!
- Yes, ma'am.

26
00:02:09,108 --> 00:02:14,808
Children, our friend Mr. Hat is going
to tell us all about genetic engineering.

27
00:02:14,978 --> 00:02:19,918
That's right, Mr. Garrison, genetic
engineering is an exciting new science.

28
00:02:20,088 --> 00:02:23,888
You can splice DNA from some
animals and make them better.

29
00:02:24,058 --> 00:02:25,848
- Mr. Garrison?
- Yes, Kyle.

30
00:02:26,028 --> 00:02:28,618
Can you make an elephant smaller?

31
00:02:28,798 --> 00:02:31,418
Well, yes, I suppose you could.

32
00:02:31,598 --> 00:02:36,728
You could splice elephant genes with
a dog or cat or potbellied pig genes.

33
00:02:36,898 --> 00:02:42,138
Combine my elephant with a potbellied
pig and make potbellied elephants.

34
00:02:42,578 --> 00:02:45,768
- Smart like elephants, small like pigs.
- Cool!

35
00:02:45,948 --> 00:02:48,808
- I want a potbellied elephant.
- I'll pay $50.

36
00:02:48,978 --> 00:02:49,968
That's stupid.

37
00:02:50,148 --> 00:02:53,018
Shut up!
We can genetical engineer anything.

38
00:02:53,188 --> 00:02:58,088
I bet I could clone a human being
before you cross an elephant and a pig.

39
00:02:58,258 --> 00:03:00,278
- I'll bet you can't!
- Watch me!

40
00:03:00,458 --> 00:03:05,858
Well, spank my ass and call me Charlie.
Two A+ students in a cloning war.

41
00:03:06,028 --> 00:03:10,558
Genetic engineering lets us correct
God's horrible mistakes.

42
00:03:10,738 --> 00:03:11,998
Like German people.

43
00:03:12,168 --> 00:03:16,968
You might visit the genetic-engineering
ranch outside of town for help.

44
00:03:17,138 --> 00:03:21,238
You could use this for your
science-fair projects next month.

45
00:03:22,118 --> 00:03:25,378
- Genetic-engineering ranch? Sweet!
- We need a pig.

46
00:03:25,548 --> 00:03:29,008
- We can use Cartman's pig.
- Leave Fluffy out of this!

47
00:03:29,188 --> 00:03:31,318
We won't hurt her.
We need blood.

48
00:03:31,488 --> 00:03:35,718
You're not using Fluffy's blood!
I'll kick you in the nuts!

49
00:03:35,898 --> 00:03:36,918
Kyle, no!

50
00:03:37,098 --> 00:03:39,388
Can I talk to you for a minute?

51
00:03:39,568 --> 00:03:43,998
I couldn't help notice that black eye.
Are there problems at home?

52
00:03:44,168 --> 00:03:46,568
- Yes.
- Oh, dear.

53
00:03:46,738 --> 00:03:51,678
Here, sit down, have some cocoa,
and tell Mr. Hat all about it.

54
00:03:51,838 --> 00:03:57,208
I'm your friend Mr. Hat. You can
tell me anything. Now, who hit you?

55
00:03:57,378 --> 00:04:01,408
- Is it your father or your mother?
- Neither, it's my sister.

56
00:04:01,588 --> 00:04:04,918
Your sister? For Pete's sake,
don't be such a wuss!

57
00:04:05,088 --> 00:04:10,288
Stop wasting Mr. Hat's time with little
problems and give me back my cocoa!

58
00:04:14,728 --> 00:04:17,788
Let's open the phone lines
for questions.

59
00:04:17,968 --> 00:04:19,298
Hey!

60
00:04:21,908 --> 00:04:24,338
- Hi, Shelly.
- You looking at my headgear?

61
00:04:24,508 --> 00:04:25,878
What headgear?

62
00:04:26,048 --> 00:04:29,708
- You looking at my headgear?
- I didn't really notice.

63
00:04:29,878 --> 00:04:34,748
- You little liar!
- I think it looks terrific! It matches...

64
00:04:50,538 --> 00:04:52,268
- Ready to go?
- Go where?

65
00:04:52,438 --> 00:04:57,138
The genetics ranch. To splice
the pig's genes with my elephant.

66
00:04:57,308 --> 00:04:59,538
Nobody's splicing from Fluffy!

67
00:04:59,708 --> 00:05:03,908
- I swear I'm gonna kill you, Stan!
- Why is your sister so mean?

68
00:05:04,078 --> 00:05:07,678
If some sissy chick tried
to kick my ass, I'd be like:

69
00:05:07,848 --> 00:05:13,988
"Hey, missy, why don't you go knit me a
sweater before I slap you in the face!"

70
00:05:14,158 --> 00:05:16,718
Who said that?

71
00:05:21,868 --> 00:05:23,628
This must be it.

72
00:05:23,798 --> 00:05:27,358
Looks like nobody's home.
We should come another time.

73
00:05:27,538 --> 00:05:30,808
We're splicing Fluffy
and my elephant together.

74
00:05:30,978 --> 00:05:33,238
I won't let them hurt you.

75
00:05:33,408 --> 00:05:35,898
- It's just a pig.
- Quit being a baby.

76
00:05:36,078 --> 00:05:40,518
- I don't get my ass kicked by a girl!
- At least I'm not a pig-fucker!

77
00:05:40,688 --> 00:05:44,248
I'm taking my pig. Screw you guys!
I'm going home!

78
00:05:44,418 --> 00:05:46,318
This whole idea is stupid!

79
00:05:46,488 --> 00:05:50,118
What would you know?
You never get higher than a D!

80
00:05:50,298 --> 00:05:53,928
Why don't you go to San Francisco
with the other Jews?

81
00:05:54,098 --> 00:05:56,928
There's no Jews
in San Francisco, retard!

82
00:05:57,098 --> 00:05:59,088
I'll kick you in the nuts!

83
00:05:59,568 --> 00:06:01,798
Can I help you?

84
00:06:03,708 --> 00:06:07,078
Yeah. We wanna crossbreed
an elephant with a pig.

85
00:06:07,248 --> 00:06:10,698
Brilliant idea.
Huge, elephant-sized pigs.

86
00:06:10,878 --> 00:06:14,908
No, little potbellied elephants
that people can keep as pets.

87
00:06:15,088 --> 00:06:18,318
That's an even better idea.
Come on in.

88
00:06:18,488 --> 00:06:22,548
I'm so pleased that you're interested
in genetic engineering.

89
00:06:22,728 --> 00:06:24,628
Nobody will hurt you.

90
00:06:24,798 --> 00:06:28,388
Thanks to genetic engineering,
there will be an end...

91
00:06:28,568 --> 00:06:31,698
...to hunger, disease,
pollution, even war.

92
00:06:31,868 --> 00:06:34,668
I created things
to make the world better.

93
00:06:34,838 --> 00:06:38,868
For instance,
here's a monkey with four asses.

94
00:06:39,478 --> 00:06:43,708
- How does that make the world better?
- My four-assed ostrich.

95
00:06:43,878 --> 00:06:45,578
My four-assed mongoose.

96
00:06:45,748 --> 00:06:48,618
Have anything
besides four-assed animals?

97
00:06:48,788 --> 00:06:52,088
I suppose so. Yes, over here.

98
00:06:52,258 --> 00:06:56,518
I have rats spliced with ducks,
gorillas spliced with mosquitoes.

99
00:06:56,698 --> 00:07:01,718
Here, I have rabbits spliced with fish
to make little bunny fish.

100
00:07:01,968 --> 00:07:04,658
These bunny ears are
tied on with strings!

101
00:07:04,838 --> 00:07:08,068
Here, Swiss cheese spliced
with chalk and a beard.

102
00:07:08,238 --> 00:07:12,908
- What about our potbellied elephant?
- Well, I'm sorry, children.

103
00:07:13,078 --> 00:07:18,068
Pig and elephant DNA won't splice.
Have you heard that song by Loverboy?

104
00:07:18,248 --> 00:07:19,238
Which song?

105
00:07:23,718 --> 00:07:27,058
Maybe I can help you add
a few asses to that swine.

106
00:07:27,228 --> 00:07:29,288
Keep your hands off of Fluffy!

107
00:07:29,458 --> 00:07:33,798
It's amazing what we can do
with a little blood sample these days.

108
00:07:34,328 --> 00:07:37,828
- What? Excuse me.
- Why are you taking Stan's blood?

109
00:07:37,998 --> 00:07:41,058
Pardon me, I tripped.
Could I have some hair?

110
00:07:41,238 --> 00:07:45,338
- Watch out, genetic engineers are crazy!
- Come on, Fluffy!

111
00:07:47,708 --> 00:07:51,438
- This gas is hurting my anus.
- That's pretty fresh.

112
00:07:51,618 --> 00:07:53,408
I sure am hungry.

113
00:07:53,588 --> 00:07:56,678
Any of you blokes know
what's for lunch today?

114
00:07:58,158 --> 00:08:02,218
- Lunchy munchies?
- Go away, Pip, nobody likes you!

115
00:08:02,398 --> 00:08:04,328
What kind of a name is Pip?

116
00:08:04,498 --> 00:08:08,258
My father's family name being Pirrip
and my name, Phillip...

117
00:08:08,428 --> 00:08:11,628
Would you shut up?
Nobody gives a rat's ass!

118
00:08:11,798 --> 00:08:14,398
- Yeah, go away, Pip.
- Right.

119
00:08:15,008 --> 00:08:17,028
French people piss me off.

120
00:08:17,208 --> 00:08:21,508
Dumb asses! You give up
on your stupid science-fair project?

121
00:08:21,678 --> 00:08:26,678
- No, we're already halfway done.
- Then all you've got is a stupid pig.

122
00:08:26,848 --> 00:08:29,978
- Probably a gay pig.
- It's more than you've got!

123
00:08:30,158 --> 00:08:34,178
Wrong! We've already
got our human clone under way.

124
00:08:34,688 --> 00:08:37,558
Oh, my God! They cloned a foot!

125
00:08:37,798 --> 00:08:39,988
Hey, I'll kick your ass!

126
00:08:41,898 --> 00:08:46,238
By Friday, we'll clone a whole human.
Good luck with your pig.

127
00:08:46,408 --> 00:08:47,838
Hello, children.

128
00:08:48,008 --> 00:08:49,528
- How you doing?
- Bad.

129
00:08:49,708 --> 00:08:50,698
Why bad?

130
00:08:50,878 --> 00:08:54,398
A pig and an elephant's
genes won't splice.

131
00:08:54,578 --> 00:08:57,308
Of course they won't splice, children.

132
00:08:57,478 --> 00:09:00,478
Haven't you ever heard
that song by Loverboy?

133
00:09:02,658 --> 00:09:08,528
- A pig-elephant? That's not a bad idea.
- I told you guys.

134
00:09:08,688 --> 00:09:12,818
Imagine a pint-sized elephant
that you could keep in the house.

135
00:09:12,998 --> 00:09:16,328
Children, we could make
a fortune with this.

136
00:09:16,498 --> 00:09:18,088
You hear? We'll be rich!

137
00:09:18,268 --> 00:09:20,898
Forget about that genetic engineering.

138
00:09:21,068 --> 00:09:26,438
If you want to combine a pig and an
elephant, get them to make sweet love.

139
00:09:26,778 --> 00:09:28,768
An elephant won't make love
to a pig.

140
00:09:28,948 --> 00:09:32,178
My pig won't wanna make love
to that elephant!

141
00:09:32,348 --> 00:09:35,318
Sure they would,
if you get them in the mood.

142
00:09:35,488 --> 00:09:39,818
- How do we do that?
- Do what I do, get them good and drunk.

143
00:09:40,558 --> 00:09:42,958
- Wanna come over?
- We've got work to do.

144
00:09:43,128 --> 00:09:45,858
An elephant takes a while
to get drunk.

145
00:09:46,028 --> 00:09:49,988
- Just for a little bit?
- Your sister gonna kick your ass?

146
00:09:50,168 --> 00:09:52,158
Shut up, Cartman!

147
00:09:52,638 --> 00:09:57,268
- Yeah, she's just a girl.
- Lf a girl tried to kick my ass, I'd be:

148
00:09:57,438 --> 00:10:02,898
"Why don't you stop dressing me like a
mailman and making me dance for you...

149
00:10:03,078 --> 00:10:05,878
...while you smoke crack
in your bedroom...

150
00:10:06,048 --> 00:10:11,008
...and have sex with some guy
I don't even know on my dad's bed."

151
00:10:13,358 --> 00:10:18,418
- What the hell are you talking about?
- I'm saying you're a little wuss.

152
00:10:18,598 --> 00:10:21,188
Use family love
as a weapon against her.

153
00:10:21,368 --> 00:10:26,928
Next time she kicks your ass, tell her,
"Shelly, you're my sister. I love you."

154
00:10:27,108 --> 00:10:31,128
- Sick, she's my sister!
- Try it. We gotta get Cartman's pig.

155
00:10:31,308 --> 00:10:34,108
You don't! Leave Fluffy out of this!

156
00:10:34,278 --> 00:10:36,108
- Come on, Kenny.
- Kyle, no!

157
00:10:36,278 --> 00:10:41,648
No elephant is gonna make love
to Fluffy! I will kick you in the nuts!

158
00:10:42,288 --> 00:10:43,308
Crap!

159
00:10:44,188 --> 00:10:46,848
And now back to Jesus and Pals...

160
00:10:47,028 --> 00:10:52,328
- Are you staring at my neck brace?
- No, I mean, yes? What neck brace?

161
00:10:53,628 --> 00:10:56,568
Before you beat my face
into a bloody pulp...

162
00:10:56,738 --> 00:11:01,228
...I just want you to know
that you're my sister and I love you.

163
00:11:06,508 --> 00:11:11,278
Someday I'll be bigger than you.
You'll wish you'd never done this.

164
00:11:11,448 --> 00:11:16,618
You'll never be bigger
than me, Stan. Never!

165
00:11:16,958 --> 00:11:22,758
Beautiful, it's absolutely beautiful.
My son, I think we've finally done it.

166
00:11:22,928 --> 00:11:26,388
Yes, we have, Dad.
My very own human clone.

167
00:11:26,568 --> 00:11:31,258
- Hope it's not a gay clone.
- Yeah. That's so stupid.

168
00:11:31,438 --> 00:11:33,168
Come on, keep drinking.

169
00:11:33,338 --> 00:11:37,498
I wonder how drunk he needs to be
to make sweet love to the pig.

170
00:11:40,048 --> 00:11:42,708
- Damn it! This won't work!
- Hello, children.

171
00:11:42,878 --> 00:11:47,508
I'm checking to see how our little
entrepreneurial adventure is going.

172
00:11:47,688 --> 00:11:50,248
They're both drunk
but won't have sex!

173
00:11:50,418 --> 00:11:54,688
Children, you can't stick a drunk pig
with a drunk elephant...

174
00:11:54,858 --> 00:11:57,728
...and expect them
to do the mattress mambo.

175
00:11:57,898 --> 00:12:02,728
You need to set the mood. Let me
show you boys what I'm talking about.

176
00:12:32,028 --> 00:12:35,398
Ladies and gentlemen,
Mr. Elton John!

177
00:12:48,408 --> 00:12:49,748
Thank you, Elton.

178
00:12:57,158 --> 00:12:58,648
Look, it's working!

179
00:12:59,888 --> 00:13:05,348
Now, children, gather around
and watch the wonders of life.

180
00:13:05,528 --> 00:13:08,428
The beauty of mother nature.

181
00:13:09,568 --> 00:13:11,868
- Sick!
- Fluffy!

182
00:13:13,038 --> 00:13:17,298
Now I know how all those
white women must have felt.

183
00:13:18,078 --> 00:13:21,738
How luscious,
our creature has come to fruition.

184
00:13:21,908 --> 00:13:25,978
- Dad, you're the best.
- Oh, my God! He only has one ass!

185
00:13:26,148 --> 00:13:29,348
He's of no use to me.
I'll have to burn the room.

186
00:13:30,718 --> 00:13:34,678
This entire experiment
is turning out very bad.

187
00:13:34,858 --> 00:13:36,048
Me bad?

188
00:13:38,898 --> 00:13:41,728
He's out of control.
We have to destroy him!

189
00:13:41,898 --> 00:13:44,228
But he's our science-fair project!

190
00:13:46,668 --> 00:13:50,868
- He's too dangerous, son.
- But, Dad, I want a human clone!

191
00:13:51,038 --> 00:13:52,598
Son, no!

192
00:13:55,948 --> 00:14:01,218
You've made a horrible mistake. You put
the people of South Park in jeopardy!

193
00:14:01,388 --> 00:14:04,718
They're all stupid anyway.
Come on, guys, let's go.

194
00:14:04,888 --> 00:14:06,518
They're all gay.

195
00:14:07,428 --> 00:14:09,488
Aren't they ever gonna wake up?

196
00:14:09,658 --> 00:14:13,028
They will.
It's gonna be one ugly sight.

197
00:14:13,198 --> 00:14:18,068
- I thought nature was beautiful.
- When does it go from beautiful to ugly?

198
00:14:18,238 --> 00:14:21,598
Usually about 9:30 a.m., children.

199
00:14:22,808 --> 00:14:24,398
Here we go.

200
00:14:29,108 --> 00:14:34,808
There's nothing worse than getting
drunk and waking up next to a pig.

201
00:14:36,358 --> 00:14:39,588
- Or an elephant.
- How do we know if she's pregnant?

202
00:14:39,758 --> 00:14:42,418
We might not know
for a couple of days.

203
00:14:42,598 --> 00:14:46,188
Couple of days? Terrance
will have his clone tomorrow.

204
00:14:46,368 --> 00:14:49,558
Good job, Einstein!
Why don't we build a rocket?

205
00:14:49,738 --> 00:14:51,568
Thank Buddha I found you.

206
00:14:51,738 --> 00:14:54,668
Tell me,
have you seen anything odd lately?

207
00:14:54,838 --> 00:14:57,068
An elephant have sex with a pig.

208
00:14:57,238 --> 00:15:01,908
- No, I said odd.
- You're that crazy cracker from the hill.

209
00:15:02,078 --> 00:15:07,638
Sir, if making mutant animals spliced
with humans is crazy, then...

210
00:15:11,918 --> 00:15:15,588
Never mind.
There's been an incident at the ranch.

211
00:15:15,758 --> 00:15:21,698
I created a large mutant clone of that
little boy there, and he's broken free.

212
00:15:22,998 --> 00:15:26,198
- A version of me?
- Bigger than a regular clone?

213
00:15:26,368 --> 00:15:31,968
He's dangerous. His brain is identical
to yours. I need you to help me find him.

214
00:15:36,548 --> 00:15:39,538
Stan, are you wearing a different hat?

215
00:15:41,288 --> 00:15:46,318
Hey! I know a certain young
man who's itching for detention!

216
00:15:48,688 --> 00:15:51,388
How big is he?
I bet he weighs 400 pounds!

217
00:15:51,558 --> 00:15:54,928
Come on, Stan, don't you know
where you would go?

218
00:15:55,398 --> 00:15:56,528
Oh, my God!

219
00:15:58,338 --> 00:16:04,108
It appears that the destructive creature
is 8-year-old Stan Marsh of South Park.

220
00:16:04,278 --> 00:16:07,638
Asked why he was wreaking
havoc on his hometown...

221
00:16:07,808 --> 00:16:13,148
...the little boy replied simply,
"Me Stan..."

222
00:16:13,318 --> 00:16:14,908
Back to the studio.

223
00:16:15,088 --> 00:16:18,178
Police request
that if you see this boy...

224
00:16:18,358 --> 00:16:21,658
...you immediately kill him
and burn his body.

225
00:16:21,828 --> 00:16:23,588
Now back to Jesus and Pals.

226
00:16:23,758 --> 00:16:27,158
The way is paved with gold
for ye who seek truth...

227
00:16:30,768 --> 00:16:32,028
Jesus!

228
00:16:33,968 --> 00:16:37,968
My evil clone is destroying the town.
We have to find him.

229
00:16:38,138 --> 00:16:40,738
You boys watch
The X Files too much.

230
00:16:40,908 --> 00:16:44,078
There's no such thing
as genetic clone...

231
00:16:48,448 --> 00:16:51,008
- Come on, let's go!
- There you are!

232
00:16:51,188 --> 00:16:56,958
You tore up my entire gun shop! You
better have a good explanation for this!

233
00:16:57,128 --> 00:16:59,788
It wasn't me, it was my evil clone.

234
00:16:59,968 --> 00:17:04,728
What the hell has gotten into you?
You have got severe lunchroom duty!

235
00:17:04,898 --> 00:17:07,528
I'm gonna have a word
with your father!

236
00:17:07,708 --> 00:17:12,338
- Wait till your father hears about this.
- Wait, Stan, there he goes.

237
00:17:12,508 --> 00:17:13,498
Stop!

238
00:17:16,548 --> 00:17:20,808
- He recognizes you, dude.
- That's good, dude. Just calm down.

239
00:17:25,058 --> 00:17:27,288
What should we do with him?

240
00:17:27,788 --> 00:17:32,088
Stan? How would you like to go home
and meet your sister?

241
00:17:36,098 --> 00:17:39,898
Shelly has a wire in her mouth
and a metal plate on her back.

242
00:17:40,068 --> 00:17:43,938
When you see Shelly, kick her ass.
Shelly very bad!

243
00:17:44,108 --> 00:17:47,078
- Me bad?
- No, Shelly bad. You good.

244
00:17:51,118 --> 00:17:53,408
He's tearing up the house.
Stop him!

245
00:18:03,128 --> 00:18:05,758
What the hell do you want?

246
00:18:11,698 --> 00:18:17,228
- I'm lusciously sorry for everything.
- They got our clone. He belongs to us.

247
00:18:17,408 --> 00:18:21,398
This beast is a disgrace
to genetic engineers everywhere.

248
00:18:21,578 --> 00:18:24,608
I'm sorry I've caused you
such inconvenience.

249
00:18:24,778 --> 00:18:27,408
I tried to play God and I failed.

250
00:18:27,848 --> 00:18:30,188
Daddy, no!

251
00:18:30,358 --> 00:18:35,048
All I wanted was to genetically
engineer something useful. I failed.

252
00:18:35,228 --> 00:18:38,198
Perhaps we shouldn't toy
with God's creations.

253
00:18:38,358 --> 00:18:43,058
Perhaps we should leave nature alone
to its simple one-assed schematics.

254
00:18:43,238 --> 00:18:46,798
You bastards, this isn't over!
Wait until tomorrow!

255
00:18:48,168 --> 00:18:51,838
Oh, my God! They killed Kenny!
You bastard!

256
00:18:52,508 --> 00:18:56,108
Mom and Dad are home.
It's a disaster! You gotta help.

257
00:18:56,278 --> 00:18:58,718
I ain't helping. I wanna eat pie.

258
00:18:58,878 --> 00:19:01,818
- You can't leave me here alone!
- Watch me.

259
00:19:01,988 --> 00:19:05,218
We have to find out
if Cartman's pig is pregnant.

260
00:19:05,388 --> 00:19:10,848
- Thanks a lot!
- Oh, boy, you are gonna get it now.

261
00:19:11,628 --> 00:19:14,288
It isn't fair.
Everybody hates me.

262
00:19:14,468 --> 00:19:18,958
The whole town wants me killed.
Mom and Dad are gonna send me away.

263
00:19:19,138 --> 00:19:23,068
I don't wanna be sent away.
I wanna stay here!

264
00:19:24,708 --> 00:19:27,678
What in God's name
have you been doing?

265
00:19:27,848 --> 00:19:30,208
Everybody in town
is upset with you.

266
00:19:30,378 --> 00:19:32,938
What's going on?
Are you on drugs?

267
00:19:33,548 --> 00:19:39,048
It's not Stan's fault.
It wasn't Stan. He was...

268
00:19:39,558 --> 00:19:41,818
He was with me the whole time.

269
00:19:43,198 --> 00:19:46,428
Well, we're sorry
we jumped to conclusions.

270
00:19:46,598 --> 00:19:49,998
Oh, honey.
Please forgive us, son.

271
00:19:51,838 --> 00:19:56,498
Shelly, you saved my life.
And yet you've done so much more.

272
00:19:56,678 --> 00:20:00,698
You taught me the meaning of family.
Families don't get along.

273
00:20:00,878 --> 00:20:05,608
When the forces of evil descend,
we conquer them by sticking together.

274
00:20:14,758 --> 00:20:18,918
Everyone, let's give Casey
and his weed a big hand.

275
00:20:19,598 --> 00:20:22,258
We're ready to see
your science project.

276
00:20:22,428 --> 00:20:26,028
Our pig hasn't given birth.
She should any time now.

277
00:20:26,208 --> 00:20:27,598
I guess you get an F.

278
00:20:27,768 --> 00:20:31,728
Terrance, I know the class
can hardly wait to see yours.

279
00:20:31,908 --> 00:20:37,108
Boys, Mr. Garrison, fellow students,
for our science-fair project...

280
00:20:37,278 --> 00:20:41,718
...Bill Fossey and I spawned a creature
genetically superior to man.

281
00:20:41,888 --> 00:20:45,408
I present to you
the five-assed monkey.

282
00:20:45,918 --> 00:20:48,118
Mr. Hat, isn't it beautiful?

283
00:20:50,928 --> 00:20:54,798
Wait, wait, the pig just gave birth!
It had a baby!

284
00:20:55,828 --> 00:20:59,698
- What's it look like?
- Does it look like pig or elephant?

285
00:21:03,238 --> 00:21:06,178
It kind of looks like Mr. Garrison.

286
00:21:06,348 --> 00:21:11,438
Isn't that an amazing coincidence?
What are the odds of that?

287
00:21:13,248 --> 00:21:15,778
You boys get first prize.

288
00:21:18,518 --> 00:21:20,818
That'll do, pig.

