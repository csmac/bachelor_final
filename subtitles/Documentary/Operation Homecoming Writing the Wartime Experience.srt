1
00:00:27,903 --> 00:00:32,782
WlNTER lN WARTlME

2
00:00:37,121 --> 00:00:40,498
holland, january 1 945

3
00:03:48,979 --> 00:03:50,646
Theo, Theo.

4
00:03:51,481 --> 00:03:52,690
Brains.

5
00:04:41,531 --> 00:04:42,573
You turn right!

6
00:05:28,078 --> 00:05:34,458
Herrvan Beusekom.

7
00:05:34,626 --> 00:05:38,212
-HerrAuer, how are you?
-Fine. Please sit down.

8
00:05:40,424 --> 00:05:43,342
We found this boy near the crashed plane.

9
00:05:44,428 --> 00:05:48,055
l expect better manners from a mayor's son.

10
00:05:51,476 --> 00:05:54,562
You're right. lt won't happen again.

11
00:05:54,646 --> 00:05:56,105
Michiel?

12
00:05:58,942 --> 00:06:00,192
Michiel!

13
00:06:05,615 --> 00:06:07,116
He wasn't alone.

14
00:06:08,326 --> 00:06:10,119
Who was the other boy?

15
00:06:17,878 --> 00:06:20,713
He says he was on his own.

16
00:06:35,312 --> 00:06:38,230
You shouldn't upset the Germans now.

17
00:06:38,482 --> 00:06:41,108
l'm supposed to suck up to them too?

18
00:06:50,452 --> 00:06:52,119
BlCYCLES

19
00:06:56,958 --> 00:06:58,459
NAZl

20
00:07:04,424 --> 00:07:05,466
Schafter.

21
00:07:35,539 --> 00:07:38,249
-What's that?
-lt was in the plane.

22
00:07:38,875 --> 00:07:40,543
Did you say anything?

23
00:07:40,710 --> 00:07:41,710
No.

24
00:07:41,878 --> 00:07:43,212
Theo!

25
00:07:44,548 --> 00:07:46,382
You have to help Dad.

26
00:08:13,743 --> 00:08:15,578
l'm so glad you're here.

27
00:08:17,247 --> 00:08:19,915
-You had to go?
-Yes, raids for the work camps.

28
00:08:30,927 --> 00:08:32,928
-How are you, Michiel?
-Fine.

29
00:08:33,096 --> 00:08:36,098
-Can he sleep in my room?
-l don't know if Uncle Ben wants to.

30
00:08:45,275 --> 00:08:47,401
Can l put this in the attic?

31
00:08:47,819 --> 00:08:49,069
l'll do that.

32
00:09:09,090 --> 00:09:10,758
FREE HOLLAND

33
00:09:27,442 --> 00:09:30,152
-How is school?
-lt's closed.

34
00:09:45,877 --> 00:09:47,628
The law of the what?

35
00:09:50,465 --> 00:09:52,174
The lion's den.

36
00:09:59,891 --> 00:10:01,934
No, l can't help you.

37
00:10:03,061 --> 00:10:04,853
You know where they are.

38
00:10:06,648 --> 00:10:08,357
You can't ask me that.

39
00:10:09,526 --> 00:10:14,446
l bring a case of ration cards
and you want them thrown in the river?

40
00:10:16,241 --> 00:10:20,577
You let your own people starve
because you have to be neutral!

41
00:10:22,789 --> 00:10:24,123
Weakling.

42
00:10:28,962 --> 00:10:30,129
Michiel?

43
00:10:30,380 --> 00:10:31,714
Can you help me?

44
00:10:31,881 --> 00:10:34,550
No time. l have to go to Caesar.

45
00:10:40,890 --> 00:10:42,057
-Hi, Dirk.
-Hi.

46
00:10:42,309 --> 00:10:45,811
-Can l have a word?
-No time, he has to go to Caesar.

47
00:10:49,649 --> 00:10:50,733
Sucker.

48
00:11:04,372 --> 00:11:06,540
Thanks for keeping
your mouth shut yesterday.

49
00:11:07,375 --> 00:11:10,210
-Did you get in trouble?
-No, not much.

50
00:11:19,262 --> 00:11:21,597
Can you deliver this if l'm not back tonight?

51
00:11:22,557 --> 00:11:23,766
Why shouldn't you be back?

52
00:11:24,392 --> 00:11:27,936
lf l'm not, take this letter
to Bertus van Gelder.

53
00:11:28,104 --> 00:11:30,814
-You can do that.
-Not if it goes wrong.

54
00:11:32,650 --> 00:11:34,443
What are you going to do?

55
00:11:37,030 --> 00:11:39,698
Swear not to tell anyone. No one.

56
00:11:43,036 --> 00:11:47,289
We're taking out the ammunition depot
at Lagezande.

57
00:13:34,105 --> 00:13:36,190
One, two...

58
00:13:40,195 --> 00:13:41,236
Let's try it.

59
00:13:44,324 --> 00:13:45,407
May l?

60
00:13:48,870 --> 00:13:51,788
-What is it?
-Soup! Can't you taste it?

61
00:13:52,332 --> 00:13:55,292
Watch out or l'll shop you to the Germans.

62
00:13:57,545 --> 00:13:58,712
Michiel.

63
00:14:00,673 --> 00:14:02,466
Come with your mother.

64
00:14:03,593 --> 00:14:04,927
Left.

65
00:14:05,220 --> 00:14:06,386
Turn.

66
00:14:07,514 --> 00:14:08,972
Backwards.

67
00:15:30,179 --> 00:15:31,513
Dad!

68
00:15:46,070 --> 00:15:47,738
Let me look.

69
00:15:57,874 --> 00:15:59,207
Now it's my turn.

70
00:16:05,048 --> 00:16:07,507
Why do they want the neighbors?

71
00:16:58,476 --> 00:17:00,143
You laugh with the Germans.

72
00:17:02,522 --> 00:17:03,605
Laugh?

73
00:17:12,615 --> 00:17:14,491
Wasn't Dirk here today?

74
00:17:15,785 --> 00:17:17,327
Did he say anything?

75
00:17:19,288 --> 00:17:20,288
No.

76
00:17:20,790 --> 00:17:22,457
He took part in a raid.

77
00:17:22,959 --> 00:17:25,711
The Germans were waiting for them.

78
00:17:25,878 --> 00:17:28,797
Two dead and Dirk's been arrested.

79
00:17:30,967 --> 00:17:32,175
Michiel.

80
00:17:33,886 --> 00:17:35,011
No.

81
00:17:35,805 --> 00:17:38,265
No, he didn't say anything.

82
00:18:03,583 --> 00:18:05,208
What a fuss.

83
00:18:18,097 --> 00:18:20,348
Theo's your best friend, isn't he?

84
00:18:22,685 --> 00:18:26,730
And do you know his brother Dirk well?

85
00:18:30,193 --> 00:18:33,153
lt was pretty tough, wasn't it?

86
00:18:39,494 --> 00:18:41,995
You know what will happen to him now?

87
00:18:46,626 --> 00:18:52,339
lf l notice you getting involved in the war,
l'll knock your head off.

88
00:18:52,507 --> 00:18:53,673
Clear?

89
00:19:18,950 --> 00:19:20,116
Hey, Bertus.

90
00:19:21,536 --> 00:19:24,496
l have something for you.
Dirk Knopper came yesterday...

91
00:19:31,087 --> 00:19:32,087
Ah, Michiel.

92
00:19:40,888 --> 00:19:44,057
Caesar needs a new horseshoe, right front.

93
00:19:44,725 --> 00:19:46,059
End of the afternoon.

94
00:19:46,477 --> 00:19:48,103
Can't you do it earlier?

95
00:19:48,187 --> 00:19:50,605
End of the afternoon. Schafter was first.

96
00:20:01,742 --> 00:20:02,951
Michiel!

97
00:20:05,079 --> 00:20:08,373
-You heard about Dirk?
-He's been arrested?

98
00:20:08,541 --> 00:20:12,878
l have to stay with Grandma.
Mom thinks it's too dangerous.

99
00:20:14,755 --> 00:20:18,425
You're lucky with your dad.
They won't harm you.

100
00:22:06,409 --> 00:22:10,912
Dagdaler Woods. Old hunting cabin.
60 meters east.

101
00:23:58,354 --> 00:24:00,021
He's been arrested.

102
00:26:49,692 --> 00:26:52,860
When l come back, l'll whistle this.

103
00:28:05,100 --> 00:28:08,269
No, Bertus just ran away. That was stupid.

104
00:28:08,771 --> 00:28:10,855
lf you run, they're allowed to shoot you?

105
00:28:10,939 --> 00:28:14,484
l mean he didn't have a chance.

106
00:28:14,860 --> 00:28:16,069
What shall we do?

107
00:28:17,529 --> 00:28:19,447
We can't just let this happen.

108
00:28:21,325 --> 00:28:25,286
-Who's in the resistance here?
-Not again, Ben. l can't help.

109
00:28:25,454 --> 00:28:27,914
You don't want to.
You'd rather talk to Krauts.

110
00:28:28,916 --> 00:28:31,793
l want everyone to survive the war safe.

111
00:28:31,960 --> 00:28:36,005
lf you know how, go ahead.
But not in my village.

112
00:28:36,548 --> 00:28:38,174
"Your" village?

113
00:28:38,342 --> 00:28:42,053
You have no power. You proved that today.

114
00:28:56,610 --> 00:28:57,819
Michiel.

115
00:29:15,045 --> 00:29:16,921
Where did you come from?

116
00:29:45,117 --> 00:29:46,784
Where did you get that?

117
00:29:46,869 --> 00:29:50,037
Rule one in wartime: keep your mouth shut.

118
00:30:00,424 --> 00:30:02,049
Not hungry?

119
00:30:02,134 --> 00:30:03,926
lt's German. lt might be poisoned.

120
00:30:37,586 --> 00:30:38,794
Uncle Ben.

121
00:31:37,604 --> 00:31:41,107
The Germans at the ferry drink tea
every day at 3:00.

122
00:32:19,855 --> 00:32:22,231
An acquaintance has to cross tomorrow.

123
00:32:22,649 --> 00:32:23,858
l sail all day.

124
00:32:24,568 --> 00:32:26,402
He has to cross at 3:00.

125
00:32:28,864 --> 00:32:29,864
What do you have for me?

126
00:32:31,116 --> 00:32:32,700
What do you mean?

127
00:32:32,868 --> 00:32:34,201
As long as it's edible.

128
00:32:35,579 --> 00:32:36,746
Okay.

129
00:32:39,207 --> 00:32:40,708
See you tomorrow.

130
00:33:08,570 --> 00:33:10,571
You can have more later.

131
00:33:11,907 --> 00:33:14,283
That's Michiel, our youngest.

132
00:33:14,618 --> 00:33:18,162
Michiel, this is Aunt Cora and...

133
00:33:19,081 --> 00:33:21,707
Janneke. Janneke from Amsterdam.

134
00:33:22,334 --> 00:33:24,752
Will you cut some firewood?

135
00:33:28,840 --> 00:33:29,965
Michiel.

136
00:33:31,426 --> 00:33:34,095
Why do l always have to? She never does...

137
00:33:34,262 --> 00:33:37,390
l'm already busy. You can do something.

138
00:33:59,204 --> 00:34:01,080
Michiel. Give me a hand.

139
00:34:04,584 --> 00:34:06,085
Hold that leg.

140
00:34:08,547 --> 00:34:13,092
l've seen Grandpa do it.
But l'm making a mess.

141
00:34:14,010 --> 00:34:15,094
Watch this.

142
00:34:17,931 --> 00:34:19,056
Right.

143
00:34:22,978 --> 00:34:26,647
Hang it for a few days and we have a feast.

144
00:34:27,274 --> 00:34:29,442
With grateful thanks to the Nazis.

145
00:34:29,526 --> 00:34:30,651
Meat is meat, Michiel.

146
00:34:31,778 --> 00:34:33,571
Or would you rather have soup again?

147
00:34:52,090 --> 00:34:56,552
Jesus Christ and God the father...

148
00:35:14,571 --> 00:35:16,739
Great, we lost our room.

149
00:35:16,907 --> 00:35:18,783
l never heard ofAunt Cora before.

150
00:35:19,534 --> 00:35:20,576
But Hanneke?

151
00:35:21,787 --> 00:35:22,828
Janneke.

152
00:37:25,577 --> 00:37:27,202
Hey! Michiel.

153
00:37:31,666 --> 00:37:33,083
Erica.

154
00:37:42,344 --> 00:37:45,095
Michiel, now l want to know
where we're going.

155
00:37:45,263 --> 00:37:47,348
-To a patient.
-Who?

156
00:37:49,809 --> 00:37:52,311
l don't have time for your games.

157
00:37:53,438 --> 00:37:56,732
-Michiel!
-The less you know, the better.

158
00:37:56,900 --> 00:37:58,609
Well...

159
00:38:02,697 --> 00:38:04,698
You're a long way from home.

160
00:38:09,955 --> 00:38:11,497
We ran out of wood.

161
00:38:25,887 --> 00:38:29,056
l'm going back. Look for someone else!

162
00:38:52,706 --> 00:38:55,082
ls there water and a bowl here?

163
00:38:59,379 --> 00:39:01,046
This is going to hurt.

164
00:39:58,271 --> 00:39:59,813
Come on, let's go.

165
00:40:24,130 --> 00:40:25,214
Of course.

166
00:40:29,302 --> 00:40:31,804
-Does Dad know about this?
-No, of course not!

167
00:40:34,015 --> 00:40:36,100
But he has to know.

168
00:40:38,311 --> 00:40:41,855
-What if they find him?
-They won't if you keep quiet.

169
00:40:42,023 --> 00:40:44,775
-Michiel...
-l take care of Jack and you shut up.

170
00:40:54,744 --> 00:40:56,245
More hot water?

171
00:40:57,705 --> 00:40:59,373
What do you want of me?

172
00:41:19,602 --> 00:41:21,436
What are you up to?

173
00:41:23,648 --> 00:41:25,983
Hey, what did l tell you?

174
00:41:30,155 --> 00:41:32,489
-You won't?
-l didn't say that.

175
00:41:42,709 --> 00:41:44,418
l'll take care of it.

176
00:41:59,934 --> 00:42:01,768
What are you doing here?

177
00:42:04,439 --> 00:42:06,315
Jack is my patient.

178
00:42:06,774 --> 00:42:08,775
Jack is mine! l'll take care of him.

179
00:42:08,943 --> 00:42:10,777
You want him to die?

180
00:42:16,159 --> 00:42:17,159
How is he?

181
00:42:18,661 --> 00:42:20,412
The fever has dropped.

182
00:42:21,623 --> 00:42:23,332
What are you going to do?

183
00:42:23,416 --> 00:42:25,083
None of your business.

184
00:42:28,087 --> 00:42:29,129
lf l can help...

185
00:42:29,297 --> 00:42:31,131
Get lost, that helps.

186
00:42:38,473 --> 00:42:40,349
The ferry leaves at 3:00.

187
00:42:49,817 --> 00:42:52,027
-"Bastard."
-"Bastard"?

188
00:42:55,573 --> 00:42:58,325
Your sister is a real "bastard."

189
00:47:03,905 --> 00:47:04,988
Michiel?

190
00:47:06,407 --> 00:47:08,325
Are you okay?

191
00:47:09,535 --> 00:47:11,328
Help set the table.

192
00:47:11,412 --> 00:47:12,537
lt's Erica's turn.

193
00:47:12,622 --> 00:47:13,830
You can help though.

194
00:47:18,836 --> 00:47:20,253
How is Jack?

195
00:47:21,839 --> 00:47:24,257
The woods are full of Krauts!

196
00:47:27,053 --> 00:47:28,553
ls he safe?

197
00:47:35,061 --> 00:47:36,561
Where is Jack?

198
00:47:38,189 --> 00:47:39,898
l lost him.

199
00:47:48,449 --> 00:47:50,283
Van Beusekom speaking.

200
00:47:53,037 --> 00:47:58,583
Yes.

201
00:47:59,961 --> 00:48:01,711
Call me when you know more.

202
00:48:10,263 --> 00:48:11,972
They found a dead soldier.

203
00:48:12,974 --> 00:48:15,517
-Where?
-Dagdaler Woods.

204
00:48:17,436 --> 00:48:19,104
-A German?
-Yes.

205
00:48:19,814 --> 00:48:22,148
He'd been there for some time. Shot.

206
00:48:25,319 --> 00:48:26,987
Now they want the culprit.

207
00:48:35,496 --> 00:48:36,580
Michiel.

208
00:48:39,875 --> 00:48:42,586
What do you know about that dead German?

209
00:48:43,254 --> 00:48:44,296
Nothing.

210
00:48:49,594 --> 00:48:51,428
You will be careful?

211
00:48:58,185 --> 00:49:00,687
lf there's anything l can do...

212
00:49:03,566 --> 00:49:05,066
There's nothing.

213
00:49:10,948 --> 00:49:12,991
The package is on its way.

214
00:50:00,081 --> 00:50:03,124
We'll go to Zwolle
when the Germans leave the woods.

215
00:50:04,043 --> 00:50:06,086
Tomorrow or maybe the day after.

216
00:50:13,260 --> 00:50:17,430
They found a German in the woods. Dead.

217
00:52:28,896 --> 00:52:30,063
What happened?

218
00:52:35,903 --> 00:52:38,363
Has he asked about me?

219
00:52:41,951 --> 00:52:43,076
What did you talk about?

220
00:52:43,327 --> 00:52:44,911
Just things.

221
00:53:18,988 --> 00:53:20,446
l'll help you.

222
00:53:36,547 --> 00:53:37,964
Give me the razor.

223
00:54:07,286 --> 00:54:10,246
Try it yourself. Carefully.

224
00:54:11,123 --> 00:54:12,665
Don't cut yourself.

225
00:54:16,086 --> 00:54:17,420
Not too hard.

226
00:54:19,715 --> 00:54:21,507
And hold it at an angle.

227
00:54:28,515 --> 00:54:30,892
We have to do it differently.

228
00:55:46,260 --> 00:55:47,427
Dad.

229
00:55:57,771 --> 00:55:59,439
Look after yourself.

230
00:56:42,274 --> 00:56:44,650
l have to know. He's my husband!

231
00:56:50,282 --> 00:56:51,741
Where's Uncle Ben?

232
00:56:53,077 --> 00:56:55,620
There's no time to cry. Do something!

233
00:56:56,205 --> 00:56:57,538
They won't say where he is.

234
00:56:59,625 --> 00:57:00,666
Michiel, please.

235
00:57:00,834 --> 00:57:03,002
We can't leave him. Do something!

236
00:57:03,087 --> 00:57:04,921
We don't know where he is.

237
00:57:05,005 --> 00:57:08,132
Vught, Amersfoort. Prison in Deventer.

238
00:57:08,300 --> 00:57:09,967
He's at the town hall.

239
00:57:11,011 --> 00:57:12,678
Here? ln the village?

240
00:57:33,367 --> 00:57:35,034
Stay polite, Michiel.

241
00:57:35,536 --> 00:57:37,495
That helps best.

242
00:57:43,001 --> 00:57:44,043
Good morning.

243
00:57:44,169 --> 00:57:47,755
l would like to see my husband.

244
00:57:48,799 --> 00:57:50,299
Van Beusekom.

245
00:57:50,467 --> 00:57:51,759
l'm sorry. l can't let you in.

246
00:57:52,052 --> 00:57:56,180
l have brought clothes and food.

247
00:57:56,348 --> 00:57:57,723
Thank you. l'll make sure...

248
00:57:57,850 --> 00:58:00,893
l want to give it to him myself.

249
00:58:01,979 --> 00:58:03,020
Please.

250
00:58:03,522 --> 00:58:05,022
l'm sorry. l really can't let you in.

251
00:58:08,026 --> 00:58:09,735
Stay there.

252
00:58:09,903 --> 00:58:12,864
-l have to see my husband.
-lmpossible.

253
00:58:12,948 --> 00:58:15,700
He hasn't done anything. He hasn't!

254
00:58:15,784 --> 00:58:17,577
l know he hasn't done anything.

255
00:58:17,744 --> 00:58:19,078
Don't be so difficult!

256
00:58:19,163 --> 00:58:20,997
Johan! Johan!

257
00:58:23,750 --> 00:58:27,128
All l ask is to speak to him.

258
00:58:27,212 --> 00:58:28,462
Johan!

259
00:58:39,600 --> 00:58:41,601
The mayor's son again.

260
00:58:43,437 --> 00:58:45,771
l can't remember inviting you.

261
00:58:49,318 --> 00:58:50,568
Get lost!

262
00:58:59,870 --> 00:59:01,579
Well?

263
00:59:02,456 --> 00:59:05,208
-Bomb the town hall.
-Dad will like that.

264
00:59:05,292 --> 00:59:07,752
-Then we have to attack.
-Come on!

265
00:59:07,836 --> 00:59:11,547
What else? We have to do something.
We can't just leave him!

266
00:59:14,218 --> 00:59:16,719
-l'll see what l can do.
-l'll go with you.

267
00:59:16,845 --> 00:59:19,472
-No.
-No. Stay with your mother. Look after her.

268
00:59:19,932 --> 00:59:21,474
Be careful.

269
00:59:32,694 --> 00:59:33,986
Uncle Ben.

270
00:59:41,536 --> 00:59:43,162
l'll do my best.

271
00:59:43,997 --> 00:59:45,206
l promise.

272
00:59:55,008 --> 00:59:56,342
Uncle Ben.

273
01:00:11,316 --> 01:00:12,858
Mom, where is Erica?

274
01:00:15,862 --> 01:00:16,862
Mom?

275
01:00:23,996 --> 01:00:25,663
Erica!

276
01:00:57,279 --> 01:00:58,529
They're not.

277
01:01:03,952 --> 01:01:06,620
No. Dad will be free. Uncle Ben will help.

278
01:01:08,206 --> 01:01:10,333
-All on his own?
-Who says he is alone?

279
01:01:13,420 --> 01:01:16,380
Let's go home. Mom is alone.

280
01:01:36,735 --> 01:01:38,235
No. That's not necessary.

281
01:01:39,279 --> 01:01:40,654
You'll get shot.

282
01:02:09,601 --> 01:02:11,519
He'll be freed tomorrow.

283
01:03:12,914 --> 01:03:15,374
l did all l could.

284
01:03:20,338 --> 01:03:21,338
Michiel?

285
01:04:02,839 --> 01:04:04,840
Right turn!

286
01:04:06,593 --> 01:04:08,093
Order arms.

287
01:04:19,814 --> 01:04:22,775
Let go of me. l know who did it.

288
01:06:03,710 --> 01:06:04,835
Are you coming?

289
01:06:11,134 --> 01:06:12,259
Michiel?

290
01:06:26,066 --> 01:06:27,566
ls he coming too?

291
01:06:27,734 --> 01:06:28,901
Of course.

292
01:06:29,402 --> 01:06:31,945
-Then l won't go.
-Michiel.

293
01:06:32,113 --> 01:06:33,822
lt's his fault.

294
01:06:34,783 --> 01:06:37,826
lt was the Germans. Not Uncle Ben.

295
01:06:59,474 --> 01:07:01,975
God save the Queen

296
01:08:06,291 --> 01:08:07,332
Jesus.

297
01:08:40,366 --> 01:08:41,867
That's for Jack.

298
01:08:45,789 --> 01:08:47,873
He has to be moved, Michiel.

299
01:08:50,710 --> 01:08:52,419
-l want to ask Uncle Ben...
-No.

300
01:08:54,714 --> 01:08:58,926
Tomorrow he can take the ferry at 3:00.
That's safe.

301
01:09:01,221 --> 01:09:02,971
He has to go to Zwolle.

302
01:09:11,272 --> 01:09:12,481
Sorry.

303
01:09:27,163 --> 01:09:28,247
Michiel.

304
01:10:55,168 --> 01:10:56,668
Can we help maybe?

305
01:11:04,052 --> 01:11:05,552
Claus, you take the left.

306
01:11:09,390 --> 01:11:10,849
Push it under.

307
01:11:14,520 --> 01:11:16,855
One, two and push!

308
01:11:28,284 --> 01:11:30,953
Come on out. You have to help!

309
01:11:31,120 --> 01:11:33,413
He doesn't speak German.

310
01:11:43,925 --> 01:11:46,635
Ah! Schafter! Give us a hand!

311
01:11:56,270 --> 01:11:59,982
-What's wrong with the wheel?
-No idea how to fit it.

312
01:12:02,527 --> 01:12:04,027
Let me have a go.

313
01:12:11,911 --> 01:12:13,412
Lower it slowly.

314
01:13:22,356 --> 01:13:23,899
l decide when we go.

315
01:13:58,935 --> 01:13:59,976
Come on.

316
01:17:46,370 --> 01:17:49,205
Germans shot him. Not you.

317
01:19:19,588 --> 01:19:26,010
Sarah!

318
01:19:31,434 --> 01:19:32,934
lnside, quick.

319
01:20:42,713 --> 01:20:45,048
British military, here in the shed?

320
01:20:45,841 --> 01:20:47,342
A kid from the RAF?

321
01:20:50,596 --> 01:20:52,597
He has to leave as quickly as possible.

322
01:20:53,599 --> 01:20:55,558
What's the plan?

323
01:20:58,354 --> 01:20:59,562
Michiel?

324
01:21:04,109 --> 01:21:05,944
Who's in charge?

325
01:21:07,863 --> 01:21:09,113
Me.

326
01:21:11,784 --> 01:21:13,451
You didn't involve anyone else?

327
01:21:18,457 --> 01:21:21,251
You don't know what you're doing, do you?

328
01:21:25,798 --> 01:21:28,967
From now on, throw snowballs or bully girls.

329
01:21:29,760 --> 01:21:31,010
l'll take over.

330
01:22:09,341 --> 01:22:11,009
Erica, Jack's gone.

331
01:22:30,195 --> 01:22:31,571
Michiel.

332
01:22:43,959 --> 01:22:46,169
There are checks everywhere.

333
01:23:00,684 --> 01:23:02,477
What? Piss off then!

334
01:23:34,385 --> 01:23:35,551
Bastard.

335
01:23:42,685 --> 01:23:45,269
Ah, here you are.

336
01:23:50,192 --> 01:23:52,735
Fortunately it's nearly all over.

337
01:23:54,863 --> 01:23:56,531
Dirk should never have involved you.

338
01:24:02,121 --> 01:24:03,871
See you later. Okay?

339
01:25:02,431 --> 01:25:04,515
l never mentioned Dirk.

340
01:25:34,087 --> 01:25:36,130
ln the Name of the German People.

341
01:27:09,808 --> 01:27:10,975
-Michiel!
-Bastard.

342
01:27:14,062 --> 01:27:15,563
Michiel, put it down.

343
01:27:15,647 --> 01:27:17,523
"Dirk should never have involved you."

344
01:27:19,526 --> 01:27:21,444
l never mentioned Dirk.

345
01:27:21,612 --> 01:27:23,613
l didn't tell anyone.

346
01:27:23,780 --> 01:27:27,325
He told me about the raid. He was arrested.

347
01:27:27,993 --> 01:27:30,369
He also talked about Bertus.
He was arrested, too.

348
01:27:35,375 --> 01:27:37,460
Look, he works for the Germans.

349
01:28:42,359 --> 01:28:44,694
The Russians will be here soon.

350
01:28:47,447 --> 01:28:50,574
Then we'll see who's occupier
and who's liberator.

351
01:29:04,423 --> 01:29:05,423
Michiel.

352
01:29:08,010 --> 01:29:09,176
Can l have a cigarette?

353
01:29:50,469 --> 01:29:53,220
Where's that resistance of yours?

354
01:30:41,603 --> 01:30:42,686
Hey, Michiel.

355
01:30:45,732 --> 01:30:47,650
Michiel, l want to make a deal.

356
01:30:48,568 --> 01:30:50,611
Let me go and l'll leave Jack alone.

357
01:30:50,695 --> 01:30:53,239
Oh, just like Dirk and Bertus?

358
01:30:53,406 --> 01:30:55,366
Terrible things happen in all wars.

359
01:30:55,742 --> 01:30:56,867
And Dad?

360
01:31:05,544 --> 01:31:07,211
l had your father free.

361
01:31:07,379 --> 01:31:09,547
-Shut up!
-Really.

362
01:31:10,590 --> 01:31:12,383
l arranged it.

363
01:31:17,139 --> 01:31:18,889
They'd take someone else at random.

364
01:31:19,808 --> 01:31:22,476
But your father refused.

365
01:31:31,069 --> 01:31:32,820
lt was his choice.

366
01:32:40,430 --> 01:32:42,306
Think about it, Michiel.

367
01:32:43,892 --> 01:32:46,060
l'm a bastard, but l'm your uncle.

368
01:32:48,647 --> 01:32:50,522
l always protected you.

369
01:32:54,444 --> 01:32:56,612
Your father would've helped me.

370
01:33:00,367 --> 01:33:05,913
Stand up.

371
01:33:31,022 --> 01:33:32,022
Germans.

372
01:35:33,478 --> 01:35:34,770
Mom!

373
01:35:55,834 --> 01:35:56,834
Michiel!

374
01:36:09,639 --> 01:36:10,681
Here you are.

375
01:36:14,018 --> 01:36:15,519
Why don't you go outside?

376
01:36:34,164 --> 01:36:35,873
Why don't you come to the party?

377
01:36:39,335 --> 01:36:40,335
You want a go?

