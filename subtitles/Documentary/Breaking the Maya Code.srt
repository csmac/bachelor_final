﻿1
00:00:00,200 --> 00:00:05,240
A girl on a chair with a glass of wine.
It's as if she's on a date.

2
00:00:05,320 --> 00:00:11,280
- Who are you talking to?
- It must be L. H.

3
00:00:13,040 --> 00:00:14,720
No!

4
00:00:14,800 --> 00:00:19,680
- You see all my fault.
- Do not keep an eye on me all the time.

5
00:00:19,760 --> 00:00:23,880
Can you do one thing for me?
Follow me away for the weekend.

6
00:00:25,240 --> 00:00:27,880
How are you, my friend?

7
00:00:27,960 --> 00:00:31,000
- Who is she?
- Sofia. She is new.

8
00:00:31,080 --> 00:00:36,520
- You arranged imagination parties, huh?
- I have not done anything.

9
00:00:36,600 --> 00:00:38,920
It is not him.

10
00:00:39,000 --> 00:00:43,600
- You have destroyed yourself.
- I beat a fucking monster.

11
00:00:43,680 --> 00:00:47,560
- You beat a sitting marked fairway.
- I have nothing, Mike.

12
00:00:47,640 --> 00:00:53,640
Hunter sometimes hangs up deer
and to empty them of blood.

13
00:00:53,720 --> 00:00:59,200
- It was probably what they did.
- Crawl forest at the site.

14
00:01:06,080 --> 00:01:11,040
"The line between good and evil
runs through every human soul. "

15
00:02:11,720 --> 00:02:14,320
Wake up, you pig.

16
00:02:14,400 --> 00:02:16,880
Wake up.

17
00:02:22,360 --> 00:02:27,520
- No no no!
- Shut up. Do not move.

18
00:02:27,600 --> 00:02:30,320
Listen.

19
00:02:33,960 --> 00:02:38,080
It gets you,
if you do not start talking.

20
00:02:38,160 --> 00:02:41,440
- Okay.
- It was Amir, huh?

21
00:02:41,520 --> 00:02:44,360
He warned you
to point him out.

22
00:02:44,440 --> 00:02:47,120
No, he did not.

23
00:02:47,200 --> 00:02:50,600
- Who was it?
- No.

24
00:02:52,680 --> 00:02:56,360
Careful. There was none.

25
00:02:56,440 --> 00:03:01,480
- I did it myself.
- Shut up.

26
00:03:01,560 --> 00:03:05,600
It was well Amir
which you rented the premises to?

27
00:03:05,680 --> 00:03:08,960
- Yes.
- It was ...

28
00:03:09,040 --> 00:03:11,880
No!

29
00:03:11,960 --> 00:03:16,720
Shut up!
Breathe in and start talking.

30
00:03:16,800 --> 00:03:20,840
Give me the truth now. Talk!

31
00:03:39,040 --> 00:03:42,480
Are you having breakfast?

32
00:03:42,560 --> 00:03:46,000
Let's talk a little?

33
00:03:47,120 --> 00:03:54,160
- I loved your mother and I know ...
- I know. We need to hold on.

34
00:03:54,240 --> 00:03:59,200
- We can talk about it.
- We can not change anything.

35
00:03:59,280 --> 00:04:01,880
It is as it is.

36
00:04:01,960 --> 00:04:05,640
And I do not hate you.

37
00:04:05,720 --> 00:04:11,040
I just have to make
how I'm going to like you again.

38
00:04:14,320 --> 00:04:18,520
I need some time for it.
I study day today.

39
00:04:18,600 --> 00:04:22,760
I'm going to Jessica's summer retreat,
so I will not be home.

40
00:04:22,840 --> 00:04:27,600
- When will you come home, then?
- Tomorrow or on Sunday.

41
00:04:30,800 --> 00:04:34,040
You may well Text,
so that I know that everything is fine?

42
00:04:34,120 --> 00:04:36,480
Yes.

43
00:04:40,440 --> 00:04:46,720
Hello. Yes, I go home now.

44
00:04:46,800 --> 00:04:49,320
So see you soon.

45
00:04:50,680 --> 00:04:52,960
Goodbye.

46
00:05:06,240 --> 00:05:08,840
Göran?

47
00:05:39,240 --> 00:05:42,800
Seal off here
and get technical here at once.

48
00:05:56,960 --> 00:05:59,400
Hello.

49
00:05:59,880 --> 00:06:04,080
- Are you ready for an awesome weekend?
- I can hardly wait.

50
00:06:04,160 --> 00:06:09,800
Good. For now we'll check it out
from mobiles and Computers

51
00:06:09,880 --> 00:06:13,120
-and be ouppkopplade all weekend.

52
00:06:13,200 --> 00:06:16,400
It sounds absolutely wonderful.

53
00:06:16,480 --> 00:06:20,640
You have not told anybody, huh?
Can I trust it?

54
00:06:20,720 --> 00:06:24,400
- Yes, you can trust it.
- Can I do it? Good.

55
00:07:54,760 --> 00:07:57,960
- Hello.
- Hello.

56
00:08:00,360 --> 00:08:05,480
Do you want to buy a bottle of champagne?
So we can get to know each other.

57
00:08:05,560 --> 00:08:09,520
I think
you are completely magnificent.

58
00:08:12,040 --> 00:08:15,160
- You are an artist.
- Yes.

59
00:08:15,240 --> 00:08:18,160
I'd like to paint you.

60
00:08:19,640 --> 00:08:23,760
- You're kidding?
- An artist always tells the truth.

61
00:08:23,840 --> 00:08:30,240
From the depths of his soul. Otherwise
one is a deceiver and useless.

62
00:08:30,320 --> 00:08:33,160
Therefore, I want to paint you.

63
00:08:33,240 --> 00:08:38,360
It would give me great pleasure.
It should never deny.

64
00:08:43,600 --> 00:08:46,000
Do not you agree?

65
00:08:47,000 --> 00:08:51,600
- I want twice as much.
- There will be.

66
00:09:02,760 --> 00:09:08,200
- How did you know where I lived?
- I followed you.

67
00:09:08,280 --> 00:09:12,560
- Why you do not live at home?
- She threw me out.

68
00:09:13,800 --> 00:09:17,720
- Why did not you say something?
- That I drink, and play?

69
00:09:17,800 --> 00:09:20,920
You knew well already.

70
00:09:21,000 --> 00:09:26,640
We have to sell the house. I have not
been a good husband or man.

71
00:09:29,600 --> 00:09:34,840
So you steal to pay
mortgages and gambling debts?

72
00:09:34,920 --> 00:09:40,680
No, because I'm
a simple fucking thief. Grip me.

73
00:09:45,080 --> 00:09:48,760
- You do well that you get caught?
- I do not care anymore.

74
00:09:48,840 --> 00:09:53,520
Do you feel that life is shit now,
what happens when you get caught?

75
00:09:53,600 --> 00:09:57,000
We are doing this ... Henry!

76
00:09:57,080 --> 00:10:01,520
Somehow
we fix this with the home loan.

77
00:10:01,600 --> 00:10:04,320
You can of course not stay.

78
00:10:04,400 --> 00:10:07,760
Meanwhile, move in with us.

79
00:10:07,840 --> 00:10:10,760
We have an extra room.

80
00:10:11,960 --> 00:10:14,800
- Okay.
- Good.

81
00:10:19,040 --> 00:10:22,800
- See you at home, then.
- You ...

82
00:10:22,880 --> 00:10:26,240
It was not the first time
I beat the bastard.

83
00:10:26,320 --> 00:10:29,160
What?

84
00:10:29,240 --> 00:10:32,920
I wanted to give back
for all the crap they gave us.

85
00:10:33,000 --> 00:10:38,080
So when Hammar fired me was
the end of a fucking shit.

86
00:10:38,160 --> 00:10:43,600
World downfall
begins with small, small things.

87
00:10:43,680 --> 00:10:47,440
But Henry ... Do not go, then! Damn!

88
00:10:52,600 --> 00:10:56,960
- I have not done anything.
- You cut open her wrists ...

89
00:10:57,040 --> 00:11:02,720
... And let her bleed to death.
You do not want to hear what you did.

90
00:11:04,040 --> 00:11:06,080
Stop!

91
00:11:07,480 --> 00:11:09,160
Hello.

92
00:11:09,240 --> 00:11:12,480
He killed probably not Sophia,
but he's hiding something.

93
00:11:12,560 --> 00:11:15,480
Yes, he's a pimp.

94
00:11:15,560 --> 00:11:19,040
Without witnesses, we can not
keep him much longer.

95
00:11:19,120 --> 00:11:23,080
Josephine
he has a residence permit?

96
00:11:23,160 --> 00:11:26,040
I check.

97
00:11:27,120 --> 00:11:31,680
- His case is under investigation.
- We use it.

98
00:11:31,760 --> 00:11:35,200
- Give me a copy of it.
- Absolutely.

99
00:11:43,040 --> 00:11:44,840
Hello.

100
00:11:44,920 --> 00:11:49,560
- Was it difficult to find?
- No not at all.

101
00:11:49,640 --> 00:11:55,200
- Is that the key to the house?
- Yes. I'll show you a little bit.

102
00:11:58,280 --> 00:12:01,640
I do not see the inside.

103
00:12:03,720 --> 00:12:05,800
Nähä?

104
00:12:07,280 --> 00:12:12,280
I am an artist,
and it is natural I am looking for.

105
00:12:12,360 --> 00:12:16,520
- And the silence, of course.
- Yeah.

106
00:12:16,600 --> 00:12:22,160
- You take cash, I hope.
- Yes, I do.

107
00:12:22,240 --> 00:12:26,120
- This should be enough.
- Even money, I hope.

108
00:12:26,200 --> 00:12:31,680
Good.
Well, then you get to have it so fun out here.

109
00:12:31,760 --> 00:12:34,520
Thanks, I will.

110
00:12:56,120 --> 00:13:00,920
- How are things?
- Now we get this over with.

111
00:13:01,000 --> 00:13:03,600
I have things to do.

112
00:13:03,680 --> 00:13:07,120
Amir Alsuwaidi ...

113
00:13:07,200 --> 00:13:10,520
You came from Syria
24 months ago ...

114
00:13:10,600 --> 00:13:15,640
... And waiting clearance
for a residence permit.

115
00:13:15,720 --> 00:13:18,520
- I have a right to be here.
- You are here now ...

116
00:13:18,600 --> 00:13:22,560
... But not if we link up
a murdered girl.

117
00:13:22,640 --> 00:13:26,240
It was not me who killed Sophia.

118
00:13:26,320 --> 00:13:31,080
You are the pimp. The
who presented the murderer of her.

119
00:13:31,160 --> 00:13:33,960
According to whom?

120
00:13:35,480 --> 00:13:38,440
I know that.

121
00:13:38,520 --> 00:13:43,080
<I> I rented the premises to Amir.
He organized sex clubs. </ I>

122
00:13:43,160 --> 00:13:50,400
<I> I did not get involved,
for he is dangerous. </ i>

123
00:13:53,480 --> 00:13:57,480
I will die if I go home.

124
00:13:57,560 --> 00:14:02,080
- You can not send me back.
- I think so, actually.

125
00:14:02,160 --> 00:14:06,160
Yes. It only takes a phone call.

126
00:14:06,240 --> 00:14:09,560
- What do you know?
- Everything.

127
00:14:09,640 --> 00:14:14,520
I arrange parties
where people live out their fantasies.

128
00:14:14,600 --> 00:14:17,960
And then they return to everyday life.

129
00:14:18,040 --> 00:14:24,720
- And Sofia?
- She was always fun to be with.

130
00:14:28,440 --> 00:14:32,640
- There was a customer ...
- What's his name?

131
00:14:32,720 --> 00:14:36,000
I have never seen his face.

132
00:14:36,080 --> 00:14:40,640
- Come on.
- It's true!

133
00:14:40,720 --> 00:14:43,960
He is always hidden.

134
00:14:44,040 --> 00:14:47,800
He takes the money,
then I will arrange the imagination.

135
00:14:47,880 --> 00:14:51,680
How did he find you? What?

136
00:14:51,760 --> 00:14:56,200
- A friend recommended him.
- What's his name?

137
00:14:58,360 --> 00:15:02,160
Her name is Asha.

138
00:15:03,840 --> 00:15:07,080
Asha Warsame. She's my girl.

139
00:15:07,160 --> 00:15:10,480
- She could not have known.
- Is she a fantasy girl?

140
00:15:10,560 --> 00:15:16,200
- No. My girl.
- Where does she live? Write down the address.

141
00:15:16,280 --> 00:15:23,600
We go there, but it takes
she tells who the stranger is.

142
00:15:23,680 --> 00:15:28,480
- That's all I know!
- I doubt it. Tell me.

143
00:15:30,920 --> 00:15:36,360
- Then you promise not to tell me.
- I can not promise.

144
00:15:36,440 --> 00:15:40,080
I need to know what you know.

145
00:15:40,160 --> 00:15:44,080
A corrupt police.

146
00:15:44,160 --> 00:15:48,680
- The interests well you?
- Write down his name.

147
00:15:48,760 --> 00:15:51,640
Write it down.

148
00:16:00,120 --> 00:16:02,480
Göran Hansson?

149
00:16:02,560 --> 00:16:06,600
He comes and lies
with my girl without paying.

150
00:16:06,680 --> 00:16:10,760
Fucking scum.
And he was interested in Sofia.

151
00:16:10,840 --> 00:16:16,480
- Can you prove this?
- He sells movies to me.

152
00:16:16,560 --> 00:16:19,000
Sex tapes.

153
00:16:31,160 --> 00:16:33,560
Are you satisfied?

154
00:16:34,200 --> 00:16:39,720
So you kept him from a rooftop,
where people could see you?

155
00:16:39,800 --> 00:16:44,480
- You crossed the line.
- I did not throw him down.

156
00:16:44,560 --> 00:16:47,800
It was the only way to get the info.

157
00:16:47,880 --> 00:16:52,040
That's not how we work.
You went over the limit.

158
00:16:55,040 --> 00:16:59,480
I know. Talk to you later.

159
00:16:59,560 --> 00:17:02,480
- What is it?
- We have a problem.

160
00:17:02,560 --> 00:17:06,280
Amir revealed a corrupt police.

161
00:17:07,280 --> 00:17:11,960
- From here?
- Yes. Göran Hansson.

162
00:17:12,040 --> 00:17:14,960
Is it true? Do you have proof?

163
00:17:15,040 --> 00:17:21,080
- We'll check it out now.
- He used to work well with you?

164
00:17:21,160 --> 00:17:27,200
- But you never noticed anything?
- No, he seemed schyst.

165
00:17:27,280 --> 00:17:34,600
If he is corrupt
he probably looking for something.

166
00:17:34,680 --> 00:17:40,560
Go. Find out
if this is really true.

167
00:17:43,400 --> 00:17:47,520
See you inside.
I'll just have coffee.

168
00:17:51,000 --> 00:17:54,280
- It is the first time.
- Like what?

169
00:17:54,360 --> 00:17:58,200
I feel
that you are hiding something for me.

170
00:17:59,240 --> 00:18:01,920
Do you want to tell me?

171
00:18:02,000 --> 00:18:05,200
- No.
- Okay.

172
00:18:17,560 --> 00:18:20,760
<I> This is Hanna. Say something. </ I>

173
00:18:20,840 --> 00:18:26,800
It is the father. It's really important
you call. It has happened a thing.

174
00:19:01,320 --> 00:19:04,600
- I thought we had a deal?
- What?

175
00:19:04,680 --> 00:19:06,760
Yes ...

176
00:19:06,840 --> 00:19:11,360
Give it to me.
Come on - it's more fun that way.

177
00:19:13,160 --> 00:19:15,000
So.

178
00:19:22,320 --> 00:19:28,560
- Do we have anything on the container?
- Blood consistent with Sofia Salas.

179
00:19:28,640 --> 00:19:33,720
- What do we have?
- He was at a gas station an hour ago.

180
00:19:33,800 --> 00:19:37,200
- Do you have his file?
- No remarks.

181
00:19:37,280 --> 00:19:40,600
There is no Naivasha at the address.

182
00:19:40,680 --> 00:19:43,560
- So Amir lied.
- She lied to him.

183
00:19:43,640 --> 00:19:49,480
- She must go to find.
- We can ask Amir again.

184
00:19:49,560 --> 00:19:53,480
We release him.
L.H. might be looking him up.

185
00:19:53,560 --> 00:19:59,040
- Should we use him as bait?
- Sometimes you have to take risks.

186
00:19:59,120 --> 00:20:02,320
Here are the cameras outside.

187
00:20:05,720 --> 00:20:11,520
- He's somebody in your car.
- I'll find another angle. Here.

188
00:20:17,120 --> 00:20:19,560
Damn.

189
00:20:42,600 --> 00:20:46,640
He bought food, so he lives nearby.

190
00:20:46,720 --> 00:20:51,520
I want a list of
all cabins nearby.

191
00:20:51,600 --> 00:20:54,560
You can reach me on the phone.

192
00:20:58,440 --> 00:21:01,320
Eklund! I'll go with you.

193
00:21:01,400 --> 00:21:05,240
It is not necessary.
Help them to find Asha.

194
00:21:05,320 --> 00:21:06,880
No.

195
00:21:06,960 --> 00:21:11,320
- This is personal.
- Yes, and when you find Goran ...

196
00:21:11,400 --> 00:21:16,440
... You will go over the limit.
And I do it better than you.

197
00:21:16,520 --> 00:21:18,960
Come on.

198
00:21:40,200 --> 00:21:44,480
- What do you think?
- It is absolutely fantastic.

199
00:21:45,960 --> 00:21:50,240
I pack up.
Feel the water, so we swim ago.

200
00:22:09,360 --> 00:22:13,840
- What do I do now, then?
- Have a seat.

201
00:22:18,920 --> 00:22:21,840
And so we can show the breast.

202
00:22:22,960 --> 00:22:25,880
Not the left - the right.

203
00:22:32,120 --> 00:22:35,800
- Will it be good?
- Perfect.

204
00:22:44,000 --> 00:22:47,040
Should we put in the time, then?

205
00:22:47,120 --> 00:22:49,840
Well, so here it is ...

206
00:22:51,960 --> 00:22:54,560
You are sitting on an Inn.

207
00:22:54,640 --> 00:23:01,240
You have an admirer. He is a little
drunk but think highly of you.

208
00:23:01,320 --> 00:23:06,200
- He has given you the wine glass.
- So I'll just sit here?

209
00:23:09,000 --> 00:23:14,720
You laugh at him
because you like his attention.

210
00:23:14,800 --> 00:23:19,760
- So we look a little that way.
- What do you do?

211
00:23:19,840 --> 00:23:22,880
- What the hell is that?
- For the portrait.

212
00:23:22,960 --> 00:23:25,800
Hell, no.
This does not feel good.

213
00:23:25,880 --> 00:23:31,160
- Do not touch me.
- I'll just paint by you.

214
00:23:37,520 --> 00:23:40,040
- Come on!
- No.

215
00:23:40,120 --> 00:23:44,280
- Come on, Hannah.
- It's too cold.

216
00:23:45,920 --> 00:23:48,640
- Wait.
- No, you are wet.

217
00:23:48,720 --> 00:23:53,400
- Yes, we were supposed to swim, of course.
- I do not want.

218
00:24:49,440 --> 00:24:53,120
Have you made them yourself?

219
00:24:53,200 --> 00:24:55,880
Yes.

220
00:24:55,960 --> 00:24:58,960
- Yes I have.
- Why?

221
00:24:59,520 --> 00:25:02,080
In order to ...

222
00:25:03,080 --> 00:25:06,600
To occasionally
is a pain better than another.

223
00:25:09,360 --> 00:25:11,800
- Do you know?
- No?

224
00:25:14,480 --> 00:25:18,480
- I have something to show you too.
- What kind of something?

225
00:25:19,960 --> 00:25:22,920
That is my secret.

226
00:25:32,480 --> 00:25:35,200
Staircase.

227
00:25:35,280 --> 00:25:40,080
- Where are we going?
- Trust me.

228
00:25:42,480 --> 00:25:46,040
Goran bought gasoline and food.

229
00:25:46,120 --> 00:25:51,800
The treasurer remember him.
But he had never seen him before.

230
00:25:51,880 --> 00:25:57,360
Hey, there are 42 cabins where.
We go through the owners now.

231
00:25:57,440 --> 00:26:00,240
- I have him.
- Jonsson has something.

232
00:26:00,320 --> 00:26:06,240
I have checked up George's family.
His mother named Anja Holgersson.

233
00:26:06,320 --> 00:26:12,960
There is a cabin owned by her
south of you, at the lake.

234
00:26:13,040 --> 00:26:17,680
We might have him. Hurry up.

235
00:26:27,480 --> 00:26:31,200
- Are you ready?
- Yes, I am prepared.

236
00:26:53,720 --> 00:26:56,400
You know what I want to do?

237
00:26:58,600 --> 00:27:02,240
I want to film us when we love.

238
00:27:03,560 --> 00:27:06,360
I want to do everything ...

239
00:27:06,440 --> 00:27:09,160
... As we ever dreamed of doing.

240
00:27:16,960 --> 00:27:19,600
And I want to film it.

241
00:27:19,680 --> 00:27:22,600
I would like to
that we can have it forever.

242
00:27:22,680 --> 00:27:25,760
I want to do everything
that makes you feel alive.

243
00:27:25,840 --> 00:27:29,000
- Why?
- Because I want you.

244
00:27:29,080 --> 00:27:33,040
I want to have you forever.

245
00:27:33,120 --> 00:27:36,160
Whatever happens in the future.

246
00:27:41,160 --> 00:27:44,200
No, I do not like it here.

247
00:27:44,280 --> 00:27:48,800
- I can not do this.
- Wait!

248
00:27:48,880 --> 00:27:52,560
It makes very painful. Let me go.

249
00:27:53,760 --> 00:27:57,040
You said you would do
everything that I asked for.

250
00:27:57,120 --> 00:28:00,320
Yes, but I can not do this.

251
00:28:01,360 --> 00:28:03,640
Oh well.

252
00:28:04,640 --> 00:28:08,800
But ... I do not understand.

253
00:28:08,880 --> 00:28:12,360
You wanted
that we would try again.

254
00:28:12,440 --> 00:28:17,040
- You've pushed for this.
- Not for this.

255
00:28:17,120 --> 00:28:19,680
There you damn certain!

256
00:28:19,760 --> 00:28:26,440
But take your clothes
and go from here. It is perfectly okay.

257
00:28:37,680 --> 00:28:39,760
So ...

258
00:28:40,760 --> 00:28:43,720
You, like this:

259
00:28:43,800 --> 00:28:48,160
We actually have something that is good.

260
00:28:48,240 --> 00:28:51,880
We have,
as you might have to work on.

261
00:28:51,960 --> 00:28:55,720
But if you go from here now ...

262
00:28:55,800 --> 00:28:59,600
... Then there's no more you and me.

263
00:28:59,680 --> 00:29:02,600
Then you are dead to me.

264
00:29:02,680 --> 00:29:04,760
Death.

265
00:29:06,240 --> 00:29:12,320
Thank you. A home owners saw a man
assaulting a woman in the woods.

266
00:29:12,400 --> 00:29:16,800
- Send it to violence.
- She was dressed as a peasant maid.

267
00:29:19,120 --> 00:29:21,960
Text me the address.

268
00:31:34,160 --> 00:31:36,200
Hello?

269
00:31:36,280 --> 00:31:39,000
Can you hear me?

270
00:32:10,600 --> 00:32:13,040
Drop the knife.

271
00:32:15,480 --> 00:32:18,480
I do not want to shoot you!

272
00:32:49,560 --> 00:32:52,240
Camera equipment.

273
00:33:00,320 --> 00:33:03,240
Where is she?

274
00:33:03,320 --> 00:33:07,520
Where is Hanna? Where is she?

275
00:33:07,600 --> 00:33:10,800
Reply!

276
00:33:13,160 --> 00:33:16,520
No no.

277
00:33:16,600 --> 00:33:20,400
- Where is Hanna?
- Calm down.

278
00:33:20,480 --> 00:33:23,200
- It's okay.
- Okay. Stop.

279
00:33:24,960 --> 00:33:31,560
- Where is she?
- She has stuck from here.

280
00:33:51,640 --> 00:33:53,920
Hello?

281
00:33:54,000 --> 00:33:57,640
- Where are you?
<I> - I'm with Jessica. </ I>

282
00:33:59,440 --> 00:34:03,880
I know you were with Goran.
Are you okay?

283
00:34:03,960 --> 00:34:07,560
Yes, please, I'm fine.
I can not talk about it.

284
00:34:07,640 --> 00:34:12,280
<I> - Should I pick you up? </ I>
- No. Can we do this later?

285
00:34:27,040 --> 00:34:30,080
<I> SexFilm are Amir was talking about ... </ i>

286
00:34:30,160 --> 00:34:33,120
<I> We found them on George's computer. </ I>

287
00:34:34,400 --> 00:34:39,200
A whole lot of ill movies
of young girls.

288
00:34:39,280 --> 00:34:44,720
<I> - interrogated him about Sofia? </ I>
- Nothing binds him to her ...

289
00:34:44,800 --> 00:34:47,480
... L.H.. or something of the other murders.

290
00:34:47,560 --> 00:34:53,320
We take him for bribery, illegal
threat, gross misconduct, purchases of sexual services.

291
00:34:53,400 --> 00:34:58,760
<I> - How's Jonsson? </ I>
- He looks to be okay. Okay.

292
00:34:58,840 --> 00:35:04,960
We should not be seen when you're done?
To go through everything ...

293
00:35:05,040 --> 00:35:08,760
No, I do not want it.

294
00:35:28,880 --> 00:35:33,640
- Are you okay?
- Yes. It's Jonsson, who is injured.

295
00:35:33,720 --> 00:35:36,760
- How are you, then?
- It's okay.

296
00:35:36,840 --> 00:35:40,240
There is a lot of evidence.

297
00:35:41,800 --> 00:35:44,080
All right.

298
00:36:07,280 --> 00:36:12,000
- Can I get photos then?
- I send them away.

299
00:36:23,680 --> 00:36:28,400
- Is that OK?
- We sew it, so it heals nicely.

300
00:36:33,800 --> 00:36:36,800
What happened?

301
00:36:36,880 --> 00:36:39,720
Nothing.

302
00:37:27,440 --> 00:37:31,080
<I> Henry Renberg.
Leave a message. </ I>

303
00:37:31,160 --> 00:37:35,040
Henry, do not you call back?

304
00:37:35,120 --> 00:37:37,240
You ...

305
00:37:40,080 --> 00:37:43,600
Everyone makes errors.

306
00:37:43,680 --> 00:37:46,560
It is human.

307
00:37:47,880 --> 00:37:50,640
We can do this - I promise.

308
00:37:50,720 --> 00:37:53,800
We fix it,
as we have always done.

309
00:38:22,880 --> 00:38:26,040
How did you know?

310
00:38:42,560 --> 00:38:45,680
- Do you hate me yet?
- Not yet.

311
00:38:52,760 --> 00:38:57,160
Okay ... A few years ago
I was working undercover.

312
00:38:57,240 --> 00:39:00,840
I was looking for
an arms dealer in the Bronx.

313
00:39:00,920 --> 00:39:06,240
He was horrible,
but he had a beautiful sister.

314
00:39:06,320 --> 00:39:11,720
She had long, red hair.
I'm a sucker for redheads girls.

315
00:39:11,800 --> 00:39:14,960
We fell in love.

316
00:39:15,040 --> 00:39:18,080
She was fantastic.

317
00:39:18,160 --> 00:39:23,520
I told him I was a policeman,
and she accepted it.

318
00:39:23,600 --> 00:39:26,880
She chose me and not her brother.

319
00:39:26,960 --> 00:39:30,000
And the evening
when we would grab him,

320
00:39:30,080 --> 00:39:36,240
- She wanted that we did it at
her, so he did not do anything.

321
00:39:36,320 --> 00:39:40,840
The Task Force was outside
waiting for my signal.

322
00:39:40,920 --> 00:39:43,680
They were waiting for my signal.

323
00:39:43,760 --> 00:39:50,560
But they screwed up. He saw them,
and all drew their weapons.

324
00:39:50,640 --> 00:39:54,880
It was chaos,
and both he and she were shot.

325
00:39:54,960 --> 00:39:59,320
There was blood from her head.
Much blood.

326
00:40:01,120 --> 00:40:06,000
There was so much that I ...
tried to push it back.

327
00:40:09,640 --> 00:40:12,720
The last thing she said was my name.

328
00:40:14,160 --> 00:40:16,640
Then she died.

329
00:40:17,960 --> 00:40:20,920
She died there.

330
00:40:22,680 --> 00:40:27,920
In my part of the world
you should take care of people.

331
00:40:28,000 --> 00:40:34,640
I also got to learn
not to forget my own.

332
00:40:34,720 --> 00:40:38,120
But there, I forgot.

333
00:40:38,200 --> 00:40:41,400
Maggie was mine.

334
00:40:44,640 --> 00:40:47,640
And I did not take care of her.

335
00:40:50,480 --> 00:40:54,800
Now I no longer own.

336
00:40:54,880 --> 00:40:58,280
In this way, mister, I no.

337
00:41:00,800 --> 00:41:04,080
There was little about me.

338
00:41:04,320 --> 00:41:08,880
There was little about this and about us.

339
00:41:15,160 --> 00:41:18,400
Do you hate me now?

340
00:41:20,280 --> 00:41:24,840
Tell me how I do not get it there
to a farewell ...

341
00:41:24,920 --> 00:41:29,120
... I promise
To really give it a chance.

342
00:41:29,200 --> 00:41:32,280
I do not know how ...

343
00:41:33,520 --> 00:41:37,720
... You do not
can get it at a farewell.

344
00:41:39,800 --> 00:41:42,840
Thanks for the whiskey.

345
00:42:58,120 --> 00:43:02,000
- You killed me.
- You are wrong.

346
00:43:03,960 --> 00:43:08,680
I knew you would die
but did it anyway.

347
00:43:08,760 --> 00:43:11,480
Do not you understand?

348
00:43:12,800 --> 00:43:15,560
I am the police.

349
00:43:37,640 --> 00:43:41,360
Translation: Mark Smith
www.btistudios.com

