1
00:00:37,480 --> 00:00:38,779
Is it 15 minutes already?

2
00:00:38,780 --> 00:00:40,100
20 minutes, actually.

3
00:00:42,720 --> 00:00:45,600
I want you to become Khun Jane’s assistant.

4
00:00:47,800 --> 00:00:50,40
And don’t come to work late again.

5
00:00:51,540 --> 00:00:52,360
Ok.

6
00:00:53,360 --> 00:00:54,200
Sanrak,

7
00:00:54,380 --> 00:00:56,000
If you have any queries about work,

8
00:00:56,80 --> 00:00:57,519
please ask Khun Jane.

9
00:00:58,20 --> 00:00:59,540
But if you feel uneasy,

10
00:01:00,40 --> 00:01:01,800
you can come to me.

11
00:01:04,440 --> 00:01:06,000
I leave her to you, Khun Jane.

12
00:01:06,380 --> 00:01:07,699
Yes. I will.

13
00:01:07,980 --> 00:01:10,580
Well, excuse me, then.

14
00:01:21,540 --> 00:01:23,400
I suggest you to lock the door next time.

15
00:01:26,520 --> 00:01:28,140
It was me who forgot to lock it.

16
00:01:28,520 --> 00:01:30,160
Don’t blame him.

17
00:01:54,640 --> 00:01:55,840
Like what he said.

18
00:01:55,840 --> 00:01:57,640
From now, you are my assistant.

19
00:01:57,640 --> 00:01:59,160
Just observe me today.

20
00:01:59,160 --> 00:02:01,60
And I let you do some work tomorrow.

21
00:02:01,140 --> 00:02:03,500
So, the first rule you have to keep in mind is…

22
00:02:03,860 --> 00:02:05,400
try to catch up with me.

23
00:02:09,780 --> 00:02:12,000
Please sum this up for me by 10 am tomorrow.

24
00:02:13,920 --> 00:02:16,160
This is for our special feature. Please edit as this brief.

25
00:02:19,80 --> 00:02:21,160
So many typos, please recheck.

26
00:02:21,940 --> 00:02:23,859
Please print this out 5 more copies.

27
00:02:41,720 --> 00:02:42,620
Hey,

28
00:02:43,500 --> 00:02:46,320
how come you got home this late?

29
00:02:46,760 --> 00:02:48,340
And look at you.

30
00:02:51,80 --> 00:02:53,20
Have you been to a military training or something?

31
00:02:54,700 --> 00:02:57,440
The military training may wear out your body.

32
00:02:57,820 --> 00:03:00,880
But the work training wears out my mind.

33
00:03:03,200 --> 00:03:06,280
My girl, Sanrak can do it.

34
00:03:07,300 --> 00:03:08,240
Right?

35
00:03:10,440 --> 00:03:11,840
That’s right.

36
00:03:12,220 --> 00:03:14,200
I can do it.

37
00:03:14,360 --> 00:03:15,880
I’m talented.

38
00:03:16,720 --> 00:03:17,680
Su! (Fight on)

39
00:03:17,740 --> 00:03:18,840
Su!

40
00:03:19,60 --> 00:03:20,60
Su!

41
00:03:20,260 --> 00:03:21,920
I’m exhausted.

42
00:03:23,20 --> 00:03:24,740
Where’s everyone?

43
00:03:24,740 --> 00:03:25,760
It’s really quiet.

44
00:03:25,760 --> 00:03:26,780
There.

45
00:03:27,140 --> 00:03:28,420
She’s right there.

46
00:03:29,120 --> 00:03:30,160
Sandee?

47
00:03:34,920 --> 00:03:35,760
Pete, you asshole.

48
00:03:35,760 --> 00:03:37,299
You will sure get some piece of me tomorrow!

49
00:03:43,560 --> 00:03:44,840
What are you doing?

50
00:03:45,200 --> 00:03:46,560
Are you tired today?

51
00:03:51,220 --> 00:03:52,880
A bit.

52
00:03:54,840 --> 00:03:57,240
Don’t forget to eat. You are so thin.

53
00:04:04,600 --> 00:04:06,200
Hello, Thew?

54
00:04:06,200 --> 00:04:07,380
What’s up?

55
00:04:07,380 --> 00:04:08,180
Come again?

56
00:04:08,960 --> 00:04:10,600
What are you saying?

57
00:04:11,340 --> 00:04:14,120
The signal at my place is not so good.

58
00:04:14,560 --> 00:04:15,480
What?

59
00:04:15,700 --> 00:04:16,680
Hello?

60
00:04:18,120 --> 00:04:18,800
What?

61
00:04:18,800 --> 00:04:19,720
Hello. Hello.

62
00:04:19,720 --> 00:04:21,240
Thew, I can’t hear you?

63
00:04:21,420 --> 00:04:23,000
Let’s text instead.

64
00:04:23,000 --> 00:04:23,940
Hello. Hello.

65
00:04:23,940 --> 00:04:25,780
What? I can’t hear you well.

66
00:04:25,960 --> 00:04:27,380
Helloooo.

67
00:04:32,540 --> 00:04:34,240
Listen, my neighbors.

68
00:04:34,400 --> 00:04:35,620
Hey, uncle!

69
00:04:36,500 --> 00:04:37,320
Look at my sister.

70
00:04:37,340 --> 00:04:38,820
She is finally asked out by a guy.

71
00:04:40,640 --> 00:04:42,800
She talked so sweetly to her guy.

72
00:04:42,980 --> 00:04:43,780
Enough.

73
00:04:43,780 --> 00:04:44,820
Are you crazy?

74
00:04:44,820 --> 00:04:46,99
No one is asking me out.

75
00:04:46,120 --> 00:04:47,720
Could that guy be Thada?

76
00:04:48,220 --> 00:04:50,460
You also told me that Thada came to pick her up the other day.

77
00:04:50,820 --> 00:04:52,80
Wait a minute.

78
00:04:52,140 --> 00:04:53,140
P’Chacha.

79
00:04:53,160 --> 00:04:55,720
Why did you tell her about Thada?

80
00:04:55,720 --> 00:04:56,580
Huh?

81
00:04:57,000 --> 00:04:58,720
I didn’t know you treat that as a secret.

82
00:04:58,960 --> 00:04:59,820
Oh?

83
00:05:00,60 --> 00:05:01,660
Isn’t he just a friend.

84
00:05:01,980 --> 00:05:03,960
A friend and a secret…

85
00:05:04,860 --> 00:05:07,240
A secret and a friend…

86
00:05:07,260 --> 00:05:08,360
Yeah?

87
00:05:08,420 --> 00:05:09,560
How can they be together?

88
00:05:09,560 --> 00:05:10,640
I’m confused.

89
00:05:10,640 --> 00:05:11,520
Gosh!

90
00:05:11,560 --> 00:05:13,340
Nothing is going on.

91
00:05:14,40 --> 00:05:15,000
This is so annoying.

92
00:05:15,40 --> 00:05:16,220
Stop talking nonsense.

93
00:05:16,240 --> 00:05:17,700
Her world looks pink, don’t you think?

94
00:05:19,580 --> 00:05:21,340
Why does she feel annoyed?

95
00:05:21,600 --> 00:05:23,60
Did I say something wrong?

96
00:05:24,80 --> 00:05:26,140
She is annoyed because there must be something going on.

97
00:05:28,180 --> 00:05:29,660
Hey, Uncle.

98
00:05:29,920 --> 00:05:32,80
My sister finally got asked out.

99
00:05:32,80 --> 00:05:32,859
I mean it!

100
00:05:32,860 --> 00:05:33,980
A guy is hitting on her.

101
00:05:34,440 --> 00:05:35,560
Gosh, I’m happy.

102
00:05:35,560 --> 00:05:38,80
Let’s eavesdrop her on the phone together.

103
00:05:38,200 --> 00:05:39,840
Let’s come in.

104
00:05:47,340 --> 00:05:48,619
Excuse me.

105
00:05:52,60 --> 00:05:53,800
Omo, so premium.

106
00:05:53,800 --> 00:05:55,660
You are like Belgium chocolate, you know?

107
00:05:55,660 --> 00:05:56,620
Good morning.

108
00:05:56,620 --> 00:05:57,680
I’m here to see Sandee.

109
00:06:01,460 --> 00:06:03,99
Are you the guy who called her last night?

110
00:06:03,100 --> 00:06:04,560
Did Sandee mention me to you?

111
00:06:04,660 --> 00:06:06,500
Something like that, dear.

112
00:06:07,440 --> 00:06:09,520
Hey, Sandee.

113
00:06:10,000 --> 00:06:11,720
You seem to have premium guys picking you up every day, dear.

114
00:06:11,720 --> 00:06:12,700
What is going on?

115
00:06:12,700 --> 00:06:13,300
This man is gorgeous.

116
00:06:13,300 --> 00:06:14,840
He’s handsome like an angel.

117
00:06:15,780 --> 00:06:16,679
Thew.

118
00:06:16,820 --> 00:06:18,219
Why are you here?

119
00:06:19,560 --> 00:06:20,560
Well, yesterday…

120
00:06:20,580 --> 00:06:22,99
you haven’t given me your answer.

121
00:06:22,340 --> 00:06:24,179
So I’m here to get it.

122
00:06:28,620 --> 00:06:30,680
What if my answer is “No, you can’t pick me up.”?

123
00:06:31,760 --> 00:06:35,39
Then, you would become a very mean person, you know?

124
00:06:36,180 --> 00:06:37,640
Hey, Sandee.

125
00:06:37,640 --> 00:06:38,760
Sandee, listen.

126
00:06:38,760 --> 00:06:40,700
How come it can be this coincident?

127
00:06:40,860 --> 00:06:45,80
Don’t worry. Sandee is not a mean person.

128
00:06:52,560 --> 00:06:53,760
Go on…

